USING "rage_builtins.sch"
USING "globals.sch"

USING "net_prints.sch"
USING "net_stat_system.sch"

USING "net_activity_selector_groups_common.sch"
USING "net_mission_joblist.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   net_activity_selector_groups_contact_missions.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Group-Specific functionality used by Contact Missions.
//
//		NOTES			:	CONTACT MISSION RANK and COMPLETED CONTENT IDs
//							There is a hidden contact mission rank stored as a stat. Also stored as stats is a number of ContentIDs
//								for Contact Missions played at the current Contact Mission Rank. When all Contact Missions at a CM Rank
//								have been played the CM Rank will increase by one. If the CM Rank is equal to or less than the player's
//								real rank then Contact Missions at the CM Rank will be offered to the player. If the CM Rank is more
//								then Contact Mission's will pause until the real rank catches up. The CM Rank should never be greater
//								than one more than the real rank.
//
//							PLAYED CONTACT MISSIONS
//							All CMs of an earlier rank are classed as 'played'. All CMs of a higher rank are classed as 'not played'.
//								The STRING STATS containing the ContentIDs are used to determine played missions at the current rank.
//								If the player is on a contact mission then it should be classed as 'played' whether it was through a
//								contact invite or a player invite - this can only be at the current rank for now.
//								To record future contact missions as 'played' we need to use the played flag, but this won't allow us
//								to get more than one run-through (unless the played flag is set per character) before all are classed
//								as played, meaning the game can't jump around in debug - so I'm going to ignore 'future played' CMs
//								for now.
//
//							31/1/14: ROOT CONTENTID HASH - for recording which missions have been played already
//							Switching over from using the contentID Hash to the RootContentID Hash to record in stats which missions
//								between the Min CM Rank and the player's Rank have been played. This will preserve this information
//								as updates are made to the mission.
//							NOTE: The 'active CMs' broadcast data still uses the contentID because it is for comparing current content
//								that has been cloud loaded rather than any historical checks.
//
//							17/4/14: CNC MISSIONS
//								These will make decisions based on Gang Rank and won't read from and store the CM STATS, instead they'll
//								refer to Dave's 'CnC Missions Complete At Current Rank' and the player's gang rank.
//								THIS ALSO ENSURES ONLY FREEMODE USES THE CM STATS AND MINIMUM CM RANK STAT AND SO PREVENTS ERRORS.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      Function to Store Local Array data into the appropriate STAT (used by converter and in-game)
// ===========================================================================================================

// PURPOSE:	Store the Local Data into the appropriate STAT
//
// INPUT PARAMS:		paramArrayPos		The array position of the data within the local copy of the stats storage
//
// NOTES:	Assumes all pre-checks have been made and the pre- and post- output has been done
PROC Store_Local_Array_Data_Into_Stat(INT paramArrayPos)
	SWITCH (paramArrayPos)
		CASE 0	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_0, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_0]")		#ENDIF	BREAK
		CASE 1	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_1, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_1]")		#ENDIF	BREAK
		CASE 2	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_2, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_2]")		#ENDIF	BREAK
		CASE 3	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_3, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_3]")		#ENDIF	BREAK
		CASE 4	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_4, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_4]")		#ENDIF	BREAK
		CASE 5	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_5, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_5]")		#ENDIF	BREAK
		CASE 6	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_6, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_6]")		#ENDIF	BREAK
		CASE 7	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_7, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_7]")		#ENDIF	BREAK
		CASE 8	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_8, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_8]")		#ENDIF	BREAK
		CASE 9	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_9, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_9]")		#ENDIF	BREAK
		CASE 10	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_10, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_10]")	#ENDIF	BREAK
		CASE 11	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_11, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_11]")	#ENDIF	BREAK
		CASE 12	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_12, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_12]")	#ENDIF	BREAK
		CASE 13	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_13, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_13]")	#ENDIF	BREAK
		CASE 14	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_14, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_14]")	#ENDIF	BREAK
		CASE 15	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_15, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_15]")	#ENDIF	BREAK
		CASE 16	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_16, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_16]")	#ENDIF	BREAK
		CASE 17	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_17, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_17]")	#ENDIF	BREAK
		CASE 18	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_18, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_18]")	#ENDIF	BREAK
		CASE 19	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_19, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_19]")	#ENDIF	BREAK
		CASE 20	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_20, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_20]")	#ENDIF	BREAK
		CASE 21	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_21, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_21]")	#ENDIF	BREAK
		CASE 22	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_22, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_22]")	#ENDIF	BREAK
		CASE 23	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_23, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_23]")	#ENDIF	BREAK
		CASE 24	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_24, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_24]")	#ENDIF	BREAK
		CASE 25	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_25, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_25]")	#ENDIF	BREAK
		CASE 26	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_26, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_26]")	#ENDIF	BREAK
		CASE 27	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_27, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_27]")	#ENDIF	BREAK
		CASE 28	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_28, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_28]")	#ENDIF	BREAK
		CASE 29	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_29, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_29]")	#ENDIF	BREAK
		CASE 30	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_30, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_30]")	#ENDIF	BREAK
		CASE 31	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_31, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_31]")	#ENDIF	BREAK
		CASE 32	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_32, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_32]")	#ENDIF	BREAK
		CASE 33	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_33, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_33]")	#ENDIF	BREAK
		CASE 34	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_34, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_34]")	#ENDIF	BREAK
		CASE 35	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_35, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_35]")	#ENDIF	BREAK
		CASE 36	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_36, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_36]")	#ENDIF	BREAK
		CASE 37	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_37, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_37]")	#ENDIF	BREAK
		CASE 38	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_38, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_38]")	#ENDIF	BREAK
		CASE 39	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_39, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_39]")	#ENDIF	BREAK
		CASE 40	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_40, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_40]")	#ENDIF	BREAK
		CASE 41	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_41, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_41]")	#ENDIF	BREAK
		CASE 42	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_42, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_42]")	#ENDIF	BREAK
		CASE 43	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_43, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_43]")	#ENDIF	BREAK
		CASE 44	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_44, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_44]")	#ENDIF	BREAK
		CASE 45	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_45, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_45]")	#ENDIF	BREAK
		CASE 46	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_46, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_46]")	#ENDIF	BREAK
		CASE 47	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_47, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_47]")	#ENDIF	BREAK
		CASE 48	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_48, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_48]")	#ENDIF	BREAK
		CASE 49	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_49, g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])	#IF IS_DEBUG_BUILD	NET_PRINT(" [MP_STAT_CONTACT_MISS_HASH_49]")	#ENDIF	BREAK
	ENDSWITCH

ENDPROC



// ===========================================================================================================
//      Hardcoded Convertion Function To Change contentIDs stored as STATS into their rootContentIDs
//		For the Valentine's Pack we changed to storing the rootContentIDs for Played Contact Missions
//			so that the mission could be updated without forcing the player to replay it.
//		This is a hardcoded routine to check all the contentIDs that were LIVE at the time of the changeover
//			and manually update them to their rootContentID equivalents so that player's didn't have to
//			replay all their played COntact Missions again.
//		Initial Implementation has this running everytime the STATs are stored in the local arrays but I'll
//			update this by storing a STAT that indicates the conversion has already been done so that it
//			isn't called everytime.
// ===========================================================================================================

// PURPOSE:	A hardcoded big switch statement to check if a stored STAT is equal to a contentID and to switch it to the rootContentID
//
// INPUT PARAMS:		paramArrayPos		The array position of the data within the local copy of the stats storage
PROC Convert_Stat_From_ContentID_To_RootContentID_If_Required(INT paramArrayPos)

	// This is a big switch statement checking if the array contains an original contentID and swapping over to its rootContentID
	INT theCurrentData	= g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos]
	INT theNewData		= 0
	
	SWITCH (theCurrentData)
		CASE 1269828147			theNewData = -1787073287		BREAK
		CASE -409534769			theNewData = 702766520			BREAK
		CASE 929719754			theNewData = -1300643348		BREAK
		CASE -1286358667		theNewData = -694362413			BREAK
		CASE -1192912009		theNewData = 132112382			BREAK
		CASE 1448338924			theNewData = 910306827			BREAK
		CASE 888600919			theNewData = 1278524759			BREAK
		CASE 1690765936			theNewData = -695098066			BREAK
		CASE 1181788561			theNewData = 262729509			BREAK
		CASE 1285855404			theNewData = -809031922			BREAK
		CASE -1060673764		theNewData = 593739370			BREAK
		CASE 754341743			theNewData = -1216550376		BREAK
		CASE 1240025430			theNewData = -1065105963		BREAK
		CASE 816202569			theNewData = 226570875			BREAK
		CASE -1830609646		theNewData = -994383302			BREAK
		CASE 974554972			theNewData = -1091967073		BREAK
		CASE -454970619			theNewData = 685973649			BREAK
		CASE 1667081992			theNewData = -1523283847		BREAK
		CASE 90092881			theNewData = 169729038			BREAK
		CASE 446225197			theNewData = 2106887052			BREAK
		CASE -1289291365		theNewData = -1453669064		BREAK
		CASE 1716121053			theNewData = 896181331			BREAK
		CASE -1664034716		theNewData = 1566962013			BREAK
		CASE -1455102196		theNewData = 586995422			BREAK
		CASE 1579592983			theNewData = 496084940			BREAK
		CASE 127553317			theNewData = -567278563			BREAK
		CASE -972560845			theNewData = 631962877			BREAK
		CASE 1506488840			theNewData = 1047979906			BREAK
		CASE -1728505841		theNewData = 520058584			BREAK
		CASE -987990651			theNewData = 618013948			BREAK
		CASE -328824107			theNewData = -684360719			BREAK
		CASE -83629759			theNewData = -225003632			BREAK
		CASE -1140605000		theNewData = 42332035			BREAK
		CASE 1461077444			theNewData = 1339459711			BREAK
		CASE 419732112			theNewData = -1987279508		BREAK
		CASE -387819802			theNewData = 989345120			BREAK
		CASE -1058965662		theNewData = 381359152			BREAK
		CASE 1201197145			theNewData = -700668065			BREAK
		CASE -1389957443		theNewData = -239189674			BREAK
		CASE 465578364			theNewData = 1807814894			BREAK
		CASE 1787190421			theNewData = -1011782942		BREAK
		CASE -1030781877		theNewData = -1561373566		BREAK
		CASE -475956235			theNewData = -1908823349		BREAK
		CASE -1403012732		theNewData = -741395393			BREAK
		CASE -902073884			theNewData = -1786907022		BREAK
		CASE -1082702901		theNewData = -1829253772		BREAK
		CASE -447141445			theNewData = -1239180175		BREAK
		CASE 773059902			theNewData = 749216744			BREAK
		CASE 993455805			theNewData = 551894033			BREAK
		CASE 2063666235			theNewData = -1535187829		BREAK
		CASE -1733874887		theNewData = 946590548			BREAK
		CASE -336533873			theNewData = -1905702301		BREAK
		CASE 1961109516			theNewData = -1440037902		BREAK
		CASE -1876219313		theNewData = 802599894			BREAK
		CASE -1553003144		theNewData = -847626561			BREAK
		CASE -1429325700		theNewData = 1585111308			BREAK
		CASE -391876017			theNewData = 1402589281			BREAK
		CASE -1551764808		theNewData = 163191282			BREAK
		CASE -460271559			theNewData = 367577012			BREAK
		CASE -76796997			theNewData = -654327895			BREAK
		CASE -1706931882		theNewData = -877761254			BREAK
		CASE -869791756			theNewData = -1492599272		BREAK
		CASE -157240823			theNewData = -1961125567		BREAK
		CASE 109072708			theNewData = -1980732912		BREAK
		CASE 209914443			theNewData = -1123673972		BREAK
		CASE -1132415669		theNewData = 1167917655			BREAK
		CASE 1821185690			theNewData = -1237355636		BREAK
		CASE -233516334			theNewData = 1589875898			BREAK
		CASE -1242776915		theNewData = 908832902			BREAK
		CASE 616333880			theNewData = 1005709588			BREAK
		CASE -1446230624		theNewData = 670486799			BREAK
		CASE 1779976782			theNewData = 2081865403			BREAK
		CASE 956366152			theNewData = 1671018532			BREAK
		CASE -1475966812		theNewData = 1815925099			BREAK
		CASE -1184037872		theNewData = -516011999			BREAK
		CASE 1592869468			theNewData = -1204946727		BREAK
		CASE -508289342			theNewData = -1769790346		BREAK
		CASE -115227763			theNewData = -742157261			BREAK
		CASE 1848244698			theNewData = -1114585673		BREAK
		CASE 1341425332			theNewData = -1784821215		BREAK
		CASE 462342316			theNewData = 2135520142			BREAK
		CASE -614509095			theNewData = 250600326			BREAK
		CASE -830091743			theNewData = -450735064			BREAK
		CASE 501754400			theNewData = 1226708017			BREAK
		
		DEFAULT
			EXIT
	ENDSWITCH
	
	// If it reaches here it has found a contentID that needs updated
	g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos] = theNewData
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Updated Array: ")
		NET_PRINT_INT(paramArrayPos)
		NET_PRINT("    Existing ContentID: ")
		NET_PRINT_INT(theCurrentData)
		NET_PRINT("    Updated To RootContentID: ")
		NET_PRINT_INT(theNewData)
	#ENDIF
	
	// Update the appropriate STAT
	Store_Local_Array_Data_Into_Stat(paramArrayPos)
	
	#IF IS_DEBUG_BUILD
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To check the local array of all played missions for contentIDs and convert them to rootContentIDs - these will then get saved next time stats are saved
PROC Convert_All_Stored_Stat_ContentIDs_To_RootContentIDs()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Convert_All_Stored_Stat_ContentIDs_To_RootContentIDs") NET_NL()
	#ENDIF

	INT tempLoop = 0
	REPEAT MAX_CM_CONTENTIDS_PLAYED tempLoop
		IF (g_CMsPlayed.cmrmHashContentIdsComplete[tempLoop] != 0)
			Convert_Stat_From_ContentID_To_RootContentID_If_Required(tempLoop)
		ENDIF
	ENDREPEAT
	
ENDPROC




// ===========================================================================================================
//      Contact Mission Rank Access Functions
// ===========================================================================================================

// PURPOSE:	Clear the Contact Mission RootContentID Local Storage
PROC Clear_All_Contact_Mission_RootContentIDs_Local_Storage()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Clear All Contact Mission RootContentIDs Local Storage") NET_NL()
	#ENDIF

	INT tempLoop = 0
	REPEAT MAX_CM_CONTENTIDS_PLAYED tempLoop
		g_CMsPlayed.cmrmHashContentIdsComplete[tempLoop] = 0
	ENDREPEAT
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Contact Mission Rank Stats variables
PROC Clear_Contact_Mission_Rank_Stats_Local_Storage()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Clear Contact Mission Rank Details Local Storage") NET_NL()
	#ENDIF

	g_CMsPlayed.cmrmMinRank = 0
	
	Clear_All_Contact_Mission_RootContentIDs_Local_Storage()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Delete the Contact Missions Played for a specified rank and below from local storage
//
// INPUT PARAMS:		paramRank			Contact Missions Played at specified rank and below should be deleted from local storage
//
// NOTES:	31/1/14 - Updated to search on the rootContentID Hash instead of the contentID Hash
PROC Clear_Contact_Mission_RootContentIDs_Local_Storage_For_Rank_And_Below(INT paramRank)

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Clear Played Contact Missions from Local Storage for this Rank and below: ") NET_PRINT_INT(paramRank) NET_NL()
	#ENDIF

	// This may be quite processor intensive - it needs to go through all played contact missions and compare rootContentIDs with the stored contact missions
	BOOL	keepSearchingCMArray	= FALSE
	INT 	cmLoop					= 0
	INT 	playedArrayLoop			= 0
	INT		rootContentIdHash		= 0
	
	// Need to mark all the deleted entries so they can be cleared out after the intial testing loop
	BOOL toBeDeleted[MAX_CM_CONTENTIDS_PLAYED]
	REPEAT MAX_CM_CONTENTIDS_PLAYED playedArrayLoop
		toBeDeleted[playedArrayLoop] = FALSE
	ENDREPEAT
	
	BOOL entriesNeedDeleted = FALSE
	
	playedArrayLoop = 0
	
	WHILE (playedArrayLoop < MAX_CM_CONTENTIDS_PLAYED)
		rootContentIdHash = g_CMsPlayed.cmrmHashContentIdsComplete[playedArrayLoop]
		
		IF (rootContentIdHash != 0)
			// ...there is a played contact mission hash value stored in this position
			// Find it on the Contact Mission array so that the array position can be compared
			keepSearchingCMArray	= TRUE
			cmLoop					= 0
			
			IF (g_numLocalMPCMs = 0)
				keepSearchingCMArray = FALSE
			ENDIF
			
			WHILE (keepSearchingCMArray)
				IF (g_sLocalMPCMs[cmLoop].lcmRootContentIdHash = rootContentIdHash)
					// ..found the contact mission, so check the rank
					IF (g_sLocalMPCMs[cmLoop].lcmRank <= paramRank)
						// ...found a mission that should be deleted
						toBeDeleted[playedArrayLoop]	= TRUE
						entriesNeedDeleted				= TRUE
						
						#IF IS_DEBUG_BUILD
							NET_PRINT("...KGM MP [ActSelect][ContactMission]: Found mission at rank. Deleting. RootContentIdHash: ") NET_PRINT_INT(rootContentIdHash) NET_NL()
						#ENDIF
					ENDIF
					
					keepSearchingCMArray = FALSE
				ENDIF
				
				IF (keepSearchingCMArray)
					cmLoop++
					IF (cmLoop >= g_numLocalMPCMs)
						// ...failed to find the mission, delete it because it must be obsolete - but only if the data looks good
						IF (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
						AND (g_numLocalMPCMs > 0)
							toBeDeleted[playedArrayLoop]	= TRUE
							entriesNeedDeleted				= TRUE
							
							#IF IS_DEBUG_BUILD
								NET_PRINT("...KGM MP [ActSelect][ContactMission]: RootContentID Not Found. Treat as Obsolete. Deleting. RootContentIdHash: ") NET_PRINT_INT(rootContentIdHash) NET_NL()
							#ENDIF
						ENDIF
						
						keepSearchingCMArray = FALSE
					ENDIF
				ENDIF
			ENDWHILE
		ENDIF
		
		playedArrayLoop++
	ENDWHILE
	
	// Nothing more to do if no entries need deleted
	IF NOT (entriesNeedDeleted)
		EXIT
	ENDIF
	
	// Shuffle the array to cover any deleted entries
	INT	theToPos	= 0
	INT theFromPos	= 0
	
	WHILE (theFromPos < MAX_CM_CONTENTIDS_PLAYED)
		IF NOT (toBeDeleted[theFromPos])
			IF (theToPos != theFromPos)
				g_CMsPlayed.cmrmHashContentIdsComplete[theToPos] = g_CMsPlayed.cmrmHashContentIdsComplete[theFromPos]
			ENDIF
			
			theToPos++
		ENDIF
		
		theFromPos++
	ENDWHILE
	
	// Clear out any data from now unused positions at the end of the array
	WHILE (theToPos < MAX_CM_CONTENTIDS_PLAYED)
		g_CMsPlayed.cmrmHashContentIdsComplete[theToPos] = 0
		
		theToPos++
	ENDWHILE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear all contact mission contentID Stats
PROC Clear_All_Contact_Mission_ContentID_Stats()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Clear ALL Contact Mission ContentID Hash Stats") NET_NL()
	#ENDIF

	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_0, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_1, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_2, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_3, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_4, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_5, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_6, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_7, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_8, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_9, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_10, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_11, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_12, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_13, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_14, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_15, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_16, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_17, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_18, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_19, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_20, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_21, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_22, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_23, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_24, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_25, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_26, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_27, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_28, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_29, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_30, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_31, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_32, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_33, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_34, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_35, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_36, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_37, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_38, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_39, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_40, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_41, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_42, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_43, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_44, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_45, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_46, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_47, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_48, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_49, 0)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear all contact mission Rank Details Stats
// NOTES:	This is intended to be used for a new character only.
PROC Clear_All_Contact_Mission_Rank_Details_Stats()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Clear ALL Contact Mission Rank Details Stats - this should only be called for a new character") NET_NL()
	#ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_RANK, 0)
	
	Clear_All_Contact_Mission_ContentID_Stats()
	
	// Make sure these changes get saved out
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Clear_All_Contact_Mission_Rank_Details_Stats()") NET_NL()
	#ENDIF

// KGM 8/7/13: Probably no longer specifically required - should get handled within mission end general saves
//	REQUEST_SAVE(STAT_SAVETYPE_CONTACTS)
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set Rank Update Standard Timeout
PROC Set_Next_Rank_Update_Standard_Timeout()

	g_cmRankUpdateTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), CONTACT_MISSION_RANK_CHECK_STANDARD_DELAY_msec)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
		NET_PRINT_TIME()
		NET_PRINT("Setting Standard Rank Timeout Time: ")
		NET_PRINT(GET_TIME_AS_STRING(g_cmRankUpdateTimeout))
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set Rank Update Quick Retest Timeout
PROC Set_Next_Rank_Update_Quick_Retest_Timeout()

	g_cmRankUpdateTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), CONTACT_MISSION_RANK_CHECK_QUICK_RETEST_msec)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
		NET_PRINT_TIME()
		NET_PRINT("Setting Quick Retest Rank Timeout Time: ")
		NET_PRINT(GET_TIME_AS_STRING(g_cmRankUpdateTimeout))
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if Rank Update timeout has expired
FUNC BOOL Has_Rank_Update_Timeout_Expired()
	RETURN (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_cmRankUpdateTimeout))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Contact Mission Rank Info from Stats into Local Storage
PROC Get_Contact_Mission_Rank_Stats_Into_Local_Storage()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Get Contact Mission Stats Details Into Local Storage") NET_NL()
	#ENDIF
	
	// Retrieve the player's Contact Mission Minimum Rank
	g_CMsPlayed.cmrmMinRank = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_RANK)
	
	// Retrieve the player's completed Contact Mission ContentIDs at the current Contact Mission rank
	g_CMsPlayed.cmrmHashContentIdsComplete[0] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_0)
	g_CMsPlayed.cmrmHashContentIdsComplete[1] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_1)
	g_CMsPlayed.cmrmHashContentIdsComplete[2] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_2)
	g_CMsPlayed.cmrmHashContentIdsComplete[3] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_3)
	g_CMsPlayed.cmrmHashContentIdsComplete[4] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_4)
	g_CMsPlayed.cmrmHashContentIdsComplete[5] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_5)
	g_CMsPlayed.cmrmHashContentIdsComplete[6] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_6)
	g_CMsPlayed.cmrmHashContentIdsComplete[7] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_7)
	g_CMsPlayed.cmrmHashContentIdsComplete[8] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_8)
	g_CMsPlayed.cmrmHashContentIdsComplete[9] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_9)
	g_CMsPlayed.cmrmHashContentIdsComplete[10] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_10)
	g_CMsPlayed.cmrmHashContentIdsComplete[11] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_11)
	g_CMsPlayed.cmrmHashContentIdsComplete[12] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_12)
	g_CMsPlayed.cmrmHashContentIdsComplete[13] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_13)
	g_CMsPlayed.cmrmHashContentIdsComplete[14] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_14)
	g_CMsPlayed.cmrmHashContentIdsComplete[15] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_15)
	g_CMsPlayed.cmrmHashContentIdsComplete[16] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_16)
	g_CMsPlayed.cmrmHashContentIdsComplete[17] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_17)
	g_CMsPlayed.cmrmHashContentIdsComplete[18] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_18)
	g_CMsPlayed.cmrmHashContentIdsComplete[19] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_19)
	g_CMsPlayed.cmrmHashContentIdsComplete[20] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_20)
	g_CMsPlayed.cmrmHashContentIdsComplete[21] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_21)
	g_CMsPlayed.cmrmHashContentIdsComplete[22] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_22)
	g_CMsPlayed.cmrmHashContentIdsComplete[23] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_23)
	g_CMsPlayed.cmrmHashContentIdsComplete[24] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_24)
	g_CMsPlayed.cmrmHashContentIdsComplete[25] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_25)
	g_CMsPlayed.cmrmHashContentIdsComplete[26] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_26)
	g_CMsPlayed.cmrmHashContentIdsComplete[27] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_27)
	g_CMsPlayed.cmrmHashContentIdsComplete[28] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_28)
	g_CMsPlayed.cmrmHashContentIdsComplete[29] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_29)
	g_CMsPlayed.cmrmHashContentIdsComplete[30] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_30)
	g_CMsPlayed.cmrmHashContentIdsComplete[31] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_31)
	g_CMsPlayed.cmrmHashContentIdsComplete[32] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_32)
	g_CMsPlayed.cmrmHashContentIdsComplete[33] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_33)
	g_CMsPlayed.cmrmHashContentIdsComplete[34] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_34)
	g_CMsPlayed.cmrmHashContentIdsComplete[35] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_35)
	g_CMsPlayed.cmrmHashContentIdsComplete[36] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_36)
	g_CMsPlayed.cmrmHashContentIdsComplete[37] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_37)
	g_CMsPlayed.cmrmHashContentIdsComplete[38] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_38)
	g_CMsPlayed.cmrmHashContentIdsComplete[39] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_39)
	g_CMsPlayed.cmrmHashContentIdsComplete[40] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_40)
	g_CMsPlayed.cmrmHashContentIdsComplete[41] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_41)
	g_CMsPlayed.cmrmHashContentIdsComplete[42] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_42)
	g_CMsPlayed.cmrmHashContentIdsComplete[43] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_43)
	g_CMsPlayed.cmrmHashContentIdsComplete[44] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_44)
	g_CMsPlayed.cmrmHashContentIdsComplete[45] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_45)
	g_CMsPlayed.cmrmHashContentIdsComplete[46] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_46)
	g_CMsPlayed.cmrmHashContentIdsComplete[47] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_47)
	g_CMsPlayed.cmrmHashContentIdsComplete[48] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_48)
	g_CMsPlayed.cmrmHashContentIdsComplete[49] = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_49)
	
	// 3/2/14: A function to convert existing played missions (stored using a contentID) into their rootContentID equivalent
	Convert_All_Stored_Stat_ContentIDs_To_RootContentIDs()
	
	#IF IS_DEBUG_BUILD
		Debug_Output_Contact_Mission_Rank_Details()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Move the Contact Mission Rank Info from Local Storage into Stats
PROC Store_Contact_Mission_Rank_Stat_From_Local_Storage()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Storing Contact Mission Minimum Rank Stat: CM Min Rank ")
		NET_PRINT_INT(g_CMsPlayed.cmrmMinRank)
		NET_NL()
	#ENDIF

	// Store the player's Contact Mission Minimum Rank
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_RANK, g_CMsPlayed.cmrmMinRank)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Move a Contact Mission Completed RootContentID into Stat
//
// INPUT PARAMS:		paramArrayPos		The Array Position for the stat
//
// NOTES:	This should only get called by the local storage contentID function
//			31/1/14 - Updated to search on the rootContentID Hash instead of the contentID Hash
PROC Store_One_Contact_Mission_Completed_RootContentID_Stat_From_Local_Storage(INT paramArrayPos)

	IF (paramArrayPos >= MAX_CM_CONTENTIDS_PLAYED)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]: Store_One_Contact_Mission_Completed_RootContentID_Stat_From_Local_Storage() - ERROR: Illegal ArrayPos: ")
			NET_PRINT_INT(paramArrayPos)
			NET_PRINT(" (MAX = ")
			NET_PRINT_INT(MAX_CM_CONTENTIDS_PLAYED)
			NET_PRINT(")")
			NET_NL()
		#ENDIF
	
		EXIT
	ENDIF

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Storing Contact Mission Completed RootContentID Hash [")
		NET_PRINT_INT(g_CMsPlayed.cmrmHashContentIdsComplete[paramArrayPos])
		NET_PRINT("] - as Stat: ")
	#ENDIF
	
	// Retrieve the player's completed Contact Mission ContentIDs at the current Contact Mission rank
	Store_Local_Array_Data_Into_Stat(paramArrayPos)
	
	#IF IS_DEBUG_BUILD
		NET_NL()
	#ENDIF
	
	// Clear out any that come after that array position, so arrayPos 0 needs to make arrayPos 1-9 all clear
	SWITCH (paramArrayPos)
		CASE 0	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_1, g_CMsPlayed.cmrmHashContentIdsComplete[1])	FALLTHRU
		CASE 1	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_2, g_CMsPlayed.cmrmHashContentIdsComplete[2])	FALLTHRU
		CASE 2	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_3, g_CMsPlayed.cmrmHashContentIdsComplete[3])	FALLTHRU
		CASE 3	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_4, g_CMsPlayed.cmrmHashContentIdsComplete[4])	FALLTHRU
		CASE 4	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_5, g_CMsPlayed.cmrmHashContentIdsComplete[5])	FALLTHRU
		CASE 5	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_6, g_CMsPlayed.cmrmHashContentIdsComplete[6])	FALLTHRU
		CASE 6	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_7, g_CMsPlayed.cmrmHashContentIdsComplete[7])	FALLTHRU
		CASE 7	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_8, g_CMsPlayed.cmrmHashContentIdsComplete[8])	FALLTHRU
		CASE 8	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_9, g_CMsPlayed.cmrmHashContentIdsComplete[9])	FALLTHRU
		CASE 9	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_10, g_CMsPlayed.cmrmHashContentIdsComplete[10])	FALLTHRU
		CASE 10	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_11, g_CMsPlayed.cmrmHashContentIdsComplete[11])	FALLTHRU
		CASE 11	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_12, g_CMsPlayed.cmrmHashContentIdsComplete[12])	FALLTHRU
		CASE 12	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_13, g_CMsPlayed.cmrmHashContentIdsComplete[13])	FALLTHRU
		CASE 13	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_14, g_CMsPlayed.cmrmHashContentIdsComplete[14])	FALLTHRU
		CASE 14	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_15, g_CMsPlayed.cmrmHashContentIdsComplete[15])	FALLTHRU
		CASE 15	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_16, g_CMsPlayed.cmrmHashContentIdsComplete[16])	FALLTHRU
		CASE 16	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_17, g_CMsPlayed.cmrmHashContentIdsComplete[17])	FALLTHRU
		CASE 17	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_18, g_CMsPlayed.cmrmHashContentIdsComplete[18])	FALLTHRU
		CASE 18	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_19, g_CMsPlayed.cmrmHashContentIdsComplete[19])	FALLTHRU
		CASE 19	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_20, g_CMsPlayed.cmrmHashContentIdsComplete[20])	FALLTHRU
		CASE 20	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_21, g_CMsPlayed.cmrmHashContentIdsComplete[21])	FALLTHRU
		CASE 21	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_22, g_CMsPlayed.cmrmHashContentIdsComplete[22])	FALLTHRU
		CASE 22	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_23, g_CMsPlayed.cmrmHashContentIdsComplete[23])	FALLTHRU
		CASE 23	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_24, g_CMsPlayed.cmrmHashContentIdsComplete[24])	FALLTHRU
		CASE 24	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_25, g_CMsPlayed.cmrmHashContentIdsComplete[25])	FALLTHRU
		CASE 25	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_26, g_CMsPlayed.cmrmHashContentIdsComplete[26])	FALLTHRU
		CASE 26	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_27, g_CMsPlayed.cmrmHashContentIdsComplete[27])	FALLTHRU
		CASE 27	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_28, g_CMsPlayed.cmrmHashContentIdsComplete[28])	FALLTHRU
		CASE 28	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_29, g_CMsPlayed.cmrmHashContentIdsComplete[29])	FALLTHRU
		CASE 29	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_30, g_CMsPlayed.cmrmHashContentIdsComplete[30])	FALLTHRU
		CASE 30	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_31, g_CMsPlayed.cmrmHashContentIdsComplete[31])	FALLTHRU
		CASE 31	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_32, g_CMsPlayed.cmrmHashContentIdsComplete[32])	FALLTHRU
		CASE 32	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_33, g_CMsPlayed.cmrmHashContentIdsComplete[33])	FALLTHRU
		CASE 33	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_34, g_CMsPlayed.cmrmHashContentIdsComplete[34])	FALLTHRU
		CASE 34	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_35, g_CMsPlayed.cmrmHashContentIdsComplete[35])	FALLTHRU
		CASE 35	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_36, g_CMsPlayed.cmrmHashContentIdsComplete[36])	FALLTHRU
		CASE 36	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_37, g_CMsPlayed.cmrmHashContentIdsComplete[37])	FALLTHRU
		CASE 37	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_38, g_CMsPlayed.cmrmHashContentIdsComplete[38])	FALLTHRU
		CASE 38	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_39, g_CMsPlayed.cmrmHashContentIdsComplete[39])	FALLTHRU
		CASE 39	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_40, g_CMsPlayed.cmrmHashContentIdsComplete[40])	FALLTHRU
		CASE 40	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_41, g_CMsPlayed.cmrmHashContentIdsComplete[41])	FALLTHRU
		CASE 41	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_42, g_CMsPlayed.cmrmHashContentIdsComplete[42])	FALLTHRU
		CASE 42	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_43, g_CMsPlayed.cmrmHashContentIdsComplete[43])	FALLTHRU
		CASE 43	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_44, g_CMsPlayed.cmrmHashContentIdsComplete[44])	FALLTHRU
		CASE 44	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_45, g_CMsPlayed.cmrmHashContentIdsComplete[45])	FALLTHRU
		CASE 45	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_46, g_CMsPlayed.cmrmHashContentIdsComplete[46])	FALLTHRU
		CASE 46	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_47, g_CMsPlayed.cmrmHashContentIdsComplete[47])	FALLTHRU
		CASE 47	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_48, g_CMsPlayed.cmrmHashContentIdsComplete[48])	FALLTHRU
		CASE 48	SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTACT_MISS_HASH_49, g_CMsPlayed.cmrmHashContentIdsComplete[49])	FALLTHRU
		CASE 49	BREAK
	ENDSWITCH

	// Ensure this change gets saved out
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Store_One_Contact_Mission_Completed_RootContentID_Stat_From_Local_Storage()") NET_NL()
	#ENDIF

// KGM 8/7/13: Probably no longer specifically required - should get handled within mission end general saves
//	REQUEST_SAVE(STAT_SAVETYPE_CONTACTS)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Copy all Contact Mission RootContentIDs from Local Storage into Stats ready for saving
PROC Copy_All_Contact_Mission_RootContentID_Stats_From_Local_Storage()
	INT temploop = 0
	
	REPEAT MAX_CM_CONTENTIDS_PLAYED tempLoop
		Store_One_Contact_Mission_Completed_RootContentID_Stat_From_Local_Storage(tempLoop)
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Advance to the next Contact Mission Rank unless it is already higher than the Real Rank
// NOTES:	This automatically clears out RootContentID local storage and STATS and updates the CM Rank STAT
PROC Advance_To_Next_Contact_Mission_Rank_And_Set_Rank_Check_Timeout()
	INT gameRank = GET_PLAYER_RANK(PLAYER_ID())
	
	// If the Contact Mission Rank is already higher than the real rank, then do nothing
	IF (g_CMsPlayed.cmrmMinRank > gameRank)
		EXIT
	ENDIF

	// Otherwise, clear the local storage RootContentIDs and the RootContentIDs as STATS
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Advance to next Contact Mission Minimum Rank. [Current CM Minimum Rank: ")
		NET_PRINT_INT(g_CMsPlayed.cmrmMinRank)
		NET_PRINT("] [Player's game Rank: ")
		NET_PRINT_INT(gameRank)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
	Clear_Contact_Mission_RootContentIDs_Local_Storage_For_Rank_And_Below(g_CMsPlayed.cmrmMinRank)
	Copy_All_Contact_Mission_RootContentID_Stats_From_Local_Storage()
	
	// Advance the Rank
	g_CMsPlayed.cmrmMinRank++
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      - Contact Mission Minimum Rank advanced to: ")
		NET_PRINT_INT(g_CMsPlayed.cmrmMinRank)
		NET_NL()
	#ENDIF
	
	// Update the Rank Stat
	Store_Contact_Mission_Rank_Stat_From_Local_Storage()
	
	// Add the next Rank Update check timeout - use the Quick Retest time in case there are no missions at the new rank
	Set_Next_Rank_Update_Quick_Retest_Timeout()

	// Ensure all these changes get saved out
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Advance_To_Next_Contact_Mission_Rank_And_Set_Rank_Check_Timeout()") NET_NL()
	#ENDIF
	
// KGM 8/7/13: Probably no longer specifically required - should get handled within mission end general saves
//	REQUEST_SAVE(STAT_SAVETYPE_CONTACTS)	//Removed as this is caught in the corona and after the mission ends: Brenda (KEITH: Put back because this means contact mission stats don't get cleared out when the rank advances which is necessary)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the rootContentID Hash from the local Contact Mission storage variables based on the contentID Hash
//
// INPUT PARAMS:		paramContentIdHash		The Hash of the contentID
// RETURN VALUE:		INT						The Hash of the associated RootContentID
//
// NOTES:	31/1/14: Function added when making the change to store rootContentID instead of contentID as 'played' stats
FUNC INT Get_RootContentIdHash_From_ContentIdHash(INT paramContentIdHash)

	INT tempLoop = 0
	
	REPEAT g_numLocalMPCMs tempLoop
		IF (g_sLocalMPCMs[tempLoop].lcmContentIdHash = paramContentIdHash)
			// ...found the contentID Hash, so return the rootContentID Hash
			RETURN (g_sLocalMPCMs[tempLoop].lcmRootContentIdHash)
		ENDIF
	ENDREPEAT

	// Failed to find the contentID Hash, so return 0
	RETURN (0)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store a RootContentID in the local storage array
//
// INPUT PARAMS:		paramContentID			The contentID to store
//
// NOTES:	This automatically updates the RootContentID STAT
// NOTES:	31/1/14 - Updated to store the rootContentID Hash instead of the contentID Hash
PROC Store_One_Contact_Mission_ContentID_In_Local_Storage(STRING paramContentID)

	IF (IS_STRING_NULL_OR_EMPTY(paramContentID))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]: Store_Contact_Mission_ContentID_In_Local_Storage() - ERROR: ContentID is NULL or EMPTY") NET_NL()
			SCRIPT_ASSERT("Store_Contact_Mission_ContentID_In_Local_Storage() - ERROR: ContentID is NULL or EMPTY. IGNORE. Tell Keith")
		#ENDIF
		
		EXIT
	ENDIF

	INT tempLoop			= 0
	INT contentIDHash		= GET_HASH_KEY(paramContentID)
	
	// 31/1/14: Now need to retrieve the rootContentID Hash and store it instead of the contentID Hash
	INT rootContentIdHash	= Get_RootContentIdHash_From_ContentIdHash(contentIDHash)
	
	IF (rootContentIdHash = 0)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]: Store_Contact_Mission_ContentID_In_Local_Storage() - ERROR: rootContentIdHash is 0") NET_NL()
			NET_PRINT("           For Info: ContentID passed in: ") NET_PRINT(paramContentID) NET_PRINT("  contentId Hash: ") NET_PRINT_INT(contentIDHash) NET_NL()
			SCRIPT_ASSERT("Store_Contact_Mission_ContentID_In_Local_Storage() - ERROR: rootContentIdHash is 0. IGNORE. Tell Keith")
		#ENDIF
		
		EXIT
	ENDIF
	
	REPEAT MAX_CM_CONTENTIDS_PLAYED tempLoop
		IF (g_CMsPlayed.cmrmHashContentIdsComplete[tempLoop] = 0)
			// ...found a free slot, so store this rootContentID hash
			g_CMsPlayed.cmrmHashContentIdsComplete[tempLoop] = rootContentIdHash
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [ActSelect][ContactMission]: Found a free slot for this rootContentID Hash. Slot: ")
				NET_PRINT_INT(tempLoop)
				NET_PRINT(" [")
				NET_PRINT(paramContentID)
				NET_PRINT(" - HASH: ")
				NET_PRINT_INT(contentIDHash)
				NET_PRINT("] - RootContentID Hash: ")
				NET_PRINT_INT(rootContentIdHash)
				NET_NL()
				Debug_Output_Contact_Mission_Rank_Details()
			#ENDIF
			
			// Update the stat
			Store_One_Contact_Mission_Completed_RootContentID_Stat_From_Local_Storage(tempLoop)
			
			EXIT
		ELSE
			// ...slot isn't free, so ignore duplicate rootContentIDs hashes - in theory this shouldn't happen, so assert
			IF (rootContentIdHash = g_CMsPlayed.cmrmHashContentIdsComplete[tempLoop])
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [ActSelect][ContactMission]: Store_Contact_Mission_ContentID_In_Local_Storage(): ERROR - RootContentID Hash already stored: ")
					NET_PRINT(paramContentID)
					NET_PRINT(" - HASH: ")
					NET_PRINT_INT(contentIDHash)
					NET_PRINT("] - RootContentID Hash: ")
					NET_PRINT_INT(rootContentIdHash)
					NET_NL()
					Debug_Output_Contact_Mission_Rank_Details()
					SCRIPT_ASSERT("Store_Contact_Mission_ContentID_In_Local_Storage(): ERROR - RootContentID Hash already stored. See Console Log. Tell Keith.")
				#ENDIF
				
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Store_Contact_Mission_ContentID_In_Local_Storage(): ERROR - ContentID array is full")
		NET_NL()
		Debug_Output_Contact_Mission_Rank_Details()
		SCRIPT_ASSERT("Store_Contact_Mission_ContentID_In_Local_Storage(): ERROR - ContentID array is full. See Console Log. Tell Keith.")
	#ENDIF
	
	// Advance to the next Contact Mission Rank
	Advance_To_Next_Contact_Mission_Rank_And_Set_Rank_Check_Timeout()

	#IF IS_DEBUG_BUILD
		Debug_Output_Local_Activity_Selector_Details_After_Refresh()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a Contact Mission filename is saved as a 'played at this rank' stat
//
// INPUT PARAMS:		paramContentID			The ContentID to be checked
// RETURN VALUE:		BOOL					TRUE if in stat array, otherwise FALSE
//
// NOTES:	31/1/14 - Updated to search on the rootContentID Hash instead of the contentID Hash
FUNC BOOL Is_This_Mission_ContentID_In_Local_Storage(STRING paramContentID)

	IF (IS_STRING_NULL_OR_EMPTY(paramContentID))
		RETURN FALSE
	ENDIF

	INT tempLoop		= 0
	INT contentIdHash	= GET_HASH_KEY(paramContentID)
	
	// 31/1/14: Now need to compare the rootContentID Hash instead of the contentID Hash
	INT rootContentIdHash	= Get_RootContentIdHash_From_ContentIdHash(contentIDHash)
	
	IF (rootContentIdHash = 0)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]: Store_Contact_Mission_ContentID_In_Local_Storage() - ERROR: rootContentIdHash is 0") NET_NL()
			NET_PRINT("           For Info: ContentID passed in: ") NET_PRINT(paramContentID) NET_PRINT("  contentId Hash: ") NET_PRINT_INT(contentIDHash) NET_NL()
			SCRIPT_ASSERT("Is_This_Mission_ContentID_In_Local_Storage() - ERROR: rootContentIdHash is 0. IGNORE. Tell Keith")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	REPEAT MAX_CM_CONTENTIDS_PLAYED tempLoop
		IF (g_CMsPlayed.cmrmHashContentIdsComplete[tempLoop] = 0)
			// ...there are no more stored 'played' filenames to check
			RETURN FALSE
		ENDIF
		
		IF (g_CMsPlayed.cmrmHashContentIdsComplete[tempLoop] = rootContentIdHash)
			// ...found it
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	// Array all checked
	RETURN FALSE

ENDFUNC





// ===========================================================================================================
//      Contact Mission 'Missions Player By Contact' Functions
// ===========================================================================================================

// PURPOSE:	Increment the number of missions played by a contact by 1
//
// INPUT PARAMS:		paramHeistContactID			The Heist Contact ID
PROC Increment_Number_Of_Missions_Player_For_Contact(g_eFMHeistContactIDs paramHeistContactID)
	g_cmsPlayedPerContact.cmppcPlayed[paramHeistContactID]++
ENDPROC





// ===========================================================================================================
//      Contact Mission Widget Functions
// ===========================================================================================================

// PURPOSE:	Debug refresh the 'played' flags after widget use
// NOTES:	31/1/14 - Updated to search on the rootContentID Hash instead of the contentID Hash
#IF IS_DEBUG_BUILD
PROC Debug_Update_Contact_Mission_Played_Flag_After_Widget_Use()

	g_eFMHeistContactIDs	heistContact	= NO_FM_HEIST_CONTACT
	INT						cmMinRank		= g_CMsPlayed.cmrmMinRank
	INT						tempLoop		= 0
			
	REPEAT MAX_FM_HEIST_CONTACTS tempLoop
		g_cmsPlayedPerContact.cmppcPlayed[tempLoop] = 0
	ENDREPEAT
	
	// Find all 'played' at a lower rank than the new rank
	REPEAT g_numLocalMPCMs tempLoop
		IF (g_sLocalMPCMs[tempLoop].lcmRank < cmMinRank)
			g_sLocalMPCMs[tempLoop].lcmPlayed = TRUE
			
			heistContact = g_sLocalMPCMs[tempLoop].lcmContact
			Increment_Number_Of_Missions_Player_For_Contact(heistContact)
		ELSE
			g_sLocalMPCMs[tempLoop].lcmPlayed = FALSE
		ENDIF
	ENDREPEAT
	
	// Add all others set as 'played' in the stats
	INT 	playedArrayLoop 		= 0
	INT 	rootContentIdHash		= 0
	INT		cmLoop					= 0
	BOOL	keepSearchingCMArray	= FALSE
	
	WHILE (playedArrayLoop < MAX_CM_CONTENTIDS_PLAYED)
		rootContentIdHash = g_CMsPlayed.cmrmHashContentIdsComplete[playedArrayLoop]
		
		IF (rootContentIdHash != 0)
			// ...there is a played contact mission hash value stored in this position
			// Find it on the Contact Mission array so that the array position can be compared
			keepSearchingCMArray	= TRUE
			cmLoop					= 0
			
			IF (g_numLocalMPCMs = 0)
				keepSearchingCMArray = FALSE
			ENDIF
			
			WHILE (keepSearchingCMArray)
				IF (g_sLocalMPCMs[cmLoop].lcmRootContentIdHash = rootContentIdHash)
					// ..found the contact mission, so mark it as played
					g_sLocalMPCMs[cmLoop].lcmPlayed = TRUE
					
					heistContact = g_sLocalMPCMs[cmLoop].lcmContact
					Increment_Number_Of_Missions_Player_For_Contact(heistContact)
					
					keepSearchingCMArray = FALSE
				ENDIF
				
				IF (keepSearchingCMArray)
					cmLoop++
					IF (cmLoop >= g_numLocalMPCMs)
						keepSearchingCMArray = FALSE
					ENDIF
				ENDIF
			ENDWHILE
		ENDIF
		
		playedArrayLoop++
	ENDWHILE

	NET_PRINT("...KGM MP [ActSelect][ContactMission]: Updated Played CMs after widget use for this CM Min Rank: ") NET_PRINT_INT(g_CMsPlayed.cmrmMinRank) NET_NL()
	Debug_Output_Local_Activity_Selector_Details_After_Refresh()

ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Setup Contact Mission widgets
#IF IS_DEBUG_BUILD
PROC Create_Contact_Mission_Widgets()


	g_WIDGET_cmRank					= 0
	g_WIDGET_setCMRank 				= FALSE
	g_WIDGET_nextCMRank				= FALSE
	g_WIDGET_forceCMPlayerTimeout	= FALSE

	START_WIDGET_GROUP("Contact Missions")
		ADD_WIDGET_INT_SLIDER("Change Contact Mission Minimum Rank",									g_WIDGET_cmRank, 0, 100, 1)
		ADD_WIDGET_BOOL("Set Contact Mission Minimum Rank to above value (if Game Rank allows it)?",	g_WIDGET_setCMRank)
		ADD_WIDGET_BOOL("Move to next Contact Mission Minimum Rank (if Game Rank allows it)?",			g_WIDGET_nextCMRank)
		ADD_WIDGET_BOOL("Force Contact Mission Delay Timeout?",											g_WIDGET_forceCMPlayerTimeout)
	STOP_WIDGET_GROUP()
		
ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintain Contact Mission widgets
#IF IS_DEBUG_BUILD
PROC Maintain_Contact_Mission_Widgets()


	IF (g_WIDGET_nextCMRank)
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: WIDGET USED: Advance to next CM Rank") NET_NL()
	
		g_WIDGET_nextCMRank = FALSE
		Advance_To_Next_Contact_Mission_Rank_And_Set_Rank_Check_Timeout()
		Debug_Update_Contact_Mission_Played_Flag_After_Widget_Use()
	ENDIF

	IF (g_WIDGET_setCMRank)
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: WIDGET USED: Change to this CM Rank: ") NET_PRINT_INT(g_WIDGET_cmRank) NET_NL()
	
		g_WIDGET_setCMRank = FALSE
		g_CMsPlayed.cmrmMinRank = g_WIDGET_cmRank - 1
		
		IF (g_CMsPlayed.cmrmMinRank > GET_PLAYER_RANK(PLAYER_ID()))
			 g_CMsPlayed.cmrmMinRank = GET_PLAYER_RANK(PLAYER_ID())
		ENDIF
		
		Clear_All_Contact_Mission_RootContentIDs_Local_Storage()
		Clear_All_Contact_Mission_ContentID_Stats()
		Advance_To_Next_Contact_Mission_Rank_And_Set_Rank_Check_Timeout()
		Debug_Update_Contact_Mission_Played_Flag_After_Widget_Use()
	ENDIF

	IF (g_WIDGET_forceCMPlayerTimeout)
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: WIDGET USED: Force Player Offer Mission Timeout") NET_NL()
	
		g_WIDGET_forceCMPlayerTimeout = FALSE
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcTimeout = GET_NETWORK_TIME()
	ENDIF

ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if SHIFT-F or debug Rank Increase has been used and mark any ForcedAtRank missions as played
#IF IS_DEBUG_BUILD
PROC Maintain_Debug_Skipping_ForcedAtRank_CMs()


	// Ignore until initial cloud load complete
	IF NOT (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
		EXIT
	ENDIF

	IF (g_DEBUG_SkippedToRank = 0)
		EXIT
	ENDIF
	
	// Player must have debug ranked up or SHIFT-F'd into the game, store the new rank then clear it out for next check
	INT newRank = g_DEBUG_SkippedToRank
	g_DEBUG_SkippedToRank = 0
	
	NET_PRINT("...KGM MP [ActSelect][ContactMission]: Player has debug Ranked Up or SHIFT-F'd in - class any ForcedAtRank missions as Played up to rank: ")
	NET_PRINT_INT(newRank)
	NET_NL()
	
	// Nothing to do if the minimum CM rank is greater than the player rank
	IF (g_CMsPlayed.cmrmMinRank > newRank)
		EXIT
	ENDIF
	
	// Go through all contact missions and check for any within the rank range that isn't already marked as played
	INT tempLoop = 0
	TEXT_LABEL_23	contentID	= ""
	
	REPEAT g_numLocalMPCMs tempLoop
		IF (g_sLocalMPCMs[tempLoop].lcmForcedAtRank)
			IF NOT (g_sLocalMPCMs[tempLoop].lcmPlayed)
				IF (g_sLocalMPCMs[tempLoop].lcmRank >= g_CMsPlayed.cmrmMinRank)
				AND (g_sLocalMPCMs[tempLoop].lcmRank <= newRank)
					NET_PRINT("      Found an unplayed ForcedAtRank mission within the appropriate rank range ")
					NET_PRINT_INT(g_CMsPlayed.cmrmMinRank)
					NET_PRINT("..")
					NET_PRINT_INT(newRank)
					NET_PRINT(" - ContentID: ")
					contentID = Get_FileName_For_FM_Cloud_Loaded_Activity(g_sLocalMPCMs[tempLoop].lcmCreatorID, g_sLocalMPCMs[tempLoop].lcmVariation)
					NET_PRINT(contentID)
					NET_NL()
					IF NOT (Is_This_Mission_ContentID_In_Local_Storage(contentID))
						NET_PRINT("      - STORING AS PLAYED") NET_NL()
						Store_One_Contact_Mission_ContentID_In_Local_Storage(contentID)
					ENDIF
					g_sLocalMPCMs[tempLoop].lcmPlayed = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC
#ENDIF




// ===========================================================================================================
//      Contact Mission Delay Functions (using Tunables)
// ===========================================================================================================

// PURPOSE:	Return a slightly randomised Contact Mission Server Delay
//
// RETURN VALUE:		TIME_DATATYPE			A slightly randomised server timeout
//
// NOTES:	The Tunables are in seconds, so need to convert to milliseconds
FUNC TIME_DATATYPE Get_CM_Randomised_Server_Delay_msec()

	INT serverDelayMsec = (g_sMPTunables.ContactMissionServerDelay_secs * 1000)
	
	// Should the tunable value be 0, make it 5 minutes
	IF (serverDelayMsec = 0)
		serverDelayMsec = (60000 * 5)
	ENDIF
	
	RETURN (Get_Random_Time_Offset(serverDelayMsec))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return a slightly randomised Contact Mission Regular Delay Between New Missions Being Offered
//
// RETURN VALUE:		TIME_DATATYPE			A slightly randomised regular timeout
//
// NOTES:	The Tunables are in seconds, so need to convert to milliseconds
FUNC TIME_DATATYPE Get_CM_Randomised_Regular_Delay_msec()

	INT regularDelayMsec = (g_sMPTunables.ContactMissionRegularDelay_secs * 1000)
	
	// Should the tunable value be 0, make it 10 minutes
	IF (regularDelayMsec = 0)
		regularDelayMsec = (60000 * 10)
	ENDIF
	
	RETURN (Get_Random_Time_Offset(regularDelayMsec))

ENDFUNC

/// PURPOSE:
///    If the timeout for the invites is less than the tunable (~5minutes) then we adjust the timeout to match this.
///    Usually called after a forced invite has been sent
PROC Delay_CM_Adversary_Invite_System()

	// Grab the current timeout and the time between now and then
	TIME_DATATYPE nextInvite = GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcTimeout
	INT timeDifference = ABSI(GET_TIME_DIFFERENCE(nextInvite, GET_NETWORK_TIME()))
	
	// If less than the tunable, we should delay our invite system
	IF timeDifference <= g_sMPTunables.iMinContactMessageDelay
		PRINTLN(".KGM [ActSelect][ContactMission][Adversary] Delay_CM_Adversary_Invite_System - Delay our invite system by: ", g_sMPTunables.iMinContactMessageDelay)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), g_sMPTunables.iMinContactMessageDelay)
	ENDIF
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return a slightly randomised Contact Mission Initial Delay Between New Missions Being Offered
//
// RETURN VALUE:		TIME_DATATYPE			A slightly randomised initial timeout
//
// NOTES:	The Tunables are in seconds, so need to convert to milliseconds
FUNC TIME_DATATYPE Get_CM_Randomised_Initial_Delay_msec()
	
	INT initialDelayMsec = (g_sMPTunables.ContactMissionInitialDelay_secs * 1000)
	
	// Should the tunable value be 0, make it 3 minutes
	IF (initialDelayMsec = 0)
		initialDelayMsec = (60000 * 3)
	ENDIF
	
	RETURN (Get_Random_Time_Offset(initialDelayMsec))

ENDFUNC




// ===========================================================================================================
//      Client CM History Functions
// ===========================================================================================================

// Store details of this mission in the history
//
// INPUT PARAMS:		paramContentID			The ContentID for this mission
//
// NOTES:	31/1/14: It's fine for this function to continue to use contentID Hash instead of rootContentID Hash because it isn't doing anything with the 'played mission' STATs storage
PROC Store_Contact_Mission_In_History(TEXT_LABEL_31 paramContentID)

	IF (IS_STRING_NULL_OR_EMPTY(paramContentID))
		EXIT
	ENDIF

	// Find the array position for this mission
	INT		contentIdHash	= GET_HASH_KEY(paramContentID)
	INT		arrayPos		= 0
	BOOL	keepSearching	= TRUE
	
	WHILE (keepSearching)
		IF (g_sLocalMPCMs[arrayPos].lcmContentIdHash = contentIdHash)
			keepSearching = FALSE
		ELSE
			arrayPos++
			
			IF (arrayPos >= g_numLocalMPCMs)
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [ActSelect][ContactMission]: HISTORY: Failed to find ContentID: ")
					NET_PRINT(paramContentID)
					NET_PRINT(" (Hash: ")
					NET_PRINT_INT(contentIdHash)
					NET_PRINT(")")
					NET_NL()
				#ENDIF
			
				EXIT
			ENDIF
		ENDIF
	ENDWHILE
	
	// Got the array position
	// Store the contact of this mission as the most recent contact
	g_eFMHeistContactIDs theContact = g_sLocalMPCMs[arrayPos].lcmContact
	
	IF (theContact != NO_FM_HEIST_CONTACT)
	AND (theContact != MAX_FM_HEIST_CONTACTS)
		g_cmLocalHistory.chcmRecentContact.crcmHeistContact	= theContact
		g_cmLocalHistory.chcmRecentContact.crcmFrameTimeout	= MISSION_IN_PROGRESS_FRAME
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]: HISTORY: Storing Most Recent Contact: ")
			NET_PRINT_INT(ENUM_TO_INT(theContact))
			NET_PRINT(" - Mission Still In Progress")
			NET_NL()
		#ENDIF
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this Heist contact is the contact for the recently played Contact Mission
//
// INPUT PARAMS:		paramHeistContact		The Heist Contact to check for
// RETURN VALUE:		BOOL					TRUE if this is the contact for the very recently played contact mission, otherwise FALSE
FUNC BOOL Is_This_The_Most_Recent_CM_Contact(g_eFMHeistContactIDs paramHeistContact)


	IF (paramHeistContact = NO_FM_HEIST_CONTACT)
		RETURN FALSE
	ENDIF

	RETURN (paramHeistContact = g_cmLocalHistory.chcmRecentContact.crcmHeistContact)
	
ENDFUNC




// ===========================================================================================================
//      General Contact Mission Array Access Functions
// ===========================================================================================================

// PURPOSE:	To check if this Contact Mission is 'Forced At Rank'
//
// INPUT PARAMS:		paramContentID			The contentID to search for
// RETURN VALUE:		BOOL					TRUE if 'forced at rank', otherwise FALSE
//
// NOTES:	31/1/14: It's fine for this function to continue to use contentID Hash instead of rootContentID Hash because it isn't doing anything with the 'played mission' STATs storage
FUNC BOOL Is_This_CM_Forced_At_Rank(TEXT_LABEL_23 paramContentID)

	IF (IS_STRING_NULL_OR_EMPTY(paramContentID))
		RETURN FALSE
	ENDIF
	
	IF (g_numLocalMPCMs = 0)
		RETURN FALSE
	ENDIF

	INT 	contentIdHash			= GET_HASH_KEY(paramContentID)
	INT		cmLoop					= 0
	
	REPEAT g_numLocalMPCMs cmLoop
		IF (g_sLocalMPCMs[cmLoop].lcmContentIdHash = contentIdHash)
			// ..found the contact mission, so check if it is 'forced at rank'
			RETURN (g_sLocalMPCMs[cmLoop].lcmForcedAtRank)
		ENDIF
	ENDREPEAT
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if there is an Unplayed 'Forced At Rank' mission available to offer to the player
//
// RETURN VALUE:		INT			The CM array position of the unplayed 'forced at rank' mission, or ILLEGAL_CONTACT_MISSION_SLOT if none
FUNC INT Get_ArrayPos_Of_Unplayed_Forced_At_Rank_Mission_Available_To_Player()

	INT tempLoop	= 0
	INT minRank		= g_CMsPlayed.cmrmMinRank
	INT maxRank		= GET_PLAYER_RANK(PLAYER_ID())
	
	REPEAT g_numLocalMPCMs tempLoop
		IF (g_sLocalMPCMs[tempLoop].lcmForcedAtRank)
			IF NOT (g_sLocalMPCMs[tempLoop].lcmPlayed)
				IF (g_sLocalMPCMs[tempLoop].lcmRank >= minRank)
				AND (g_sLocalMPCMs[tempLoop].lcmRank <= maxRank)
					RETURN tempLoop
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN ILLEGAL_CONTACT_MISSION_SLOT

ENDFUNC




// ===========================================================================================================
//      Active CMs Broadcast to other Players
// ===========================================================================================================

// PURPOSE:	Update the Active CMs broadcast array based on the local active CMs array
// NOTES:	31/1/14: It's fine for this function to continue to use contentID Hash instead of rootContentID Hash because it isn't doing anything with the 'played mission' STATs storage
PROC Update_Active_CMs_On_Broadcast_Array()

	INT 			tempLoop		= 0
	INT 			contentIdHash	= 0
	TEXT_LABEL_23	contentID		= ""
	
	REPEAT MAX_CONTACT_MISSION_INVITES_AT_ONCE tempLoop
		contentID		= g_lcacmActiveCMs[tempLoop].lcacmMissionIdData.idCloudFilename
		contentIdHash	= 0
		
		IF NOT (IS_STRING_NULL_OR_EMPTY(contentID))
			contentIdHash = GET_HASH_KEY(contentID)
		ENDIF
		
		// Store on Broadcast array
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcActiveContentIdHashes[tempLoop] = contentIdHash
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		Debug_Output_PlayerBD_Active_CMs()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the PlayerBD ContentID Hash value at an array position for a player
//
// INPUT PARAMS:		paramPlayerID			PlayerID data is being gathered for
//						paramArrayPos			Array Position on the Broadcast array where the contentID hash needs retrieved
// RETURN VALUE:		INT						contentID Hash
FUNC INT Return_Active_CM_ContentID_Hash_For_Player_At_ArrayPos(PLAYER_INDEX paramPlayerID, INT paramArrayPos)

	IF NOT (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
		RETURN 0
	ENDIF
	
	RETURN (GlobalplayerBD[NATIVE_TO_INT(paramPlayerID)].playerActivitySelector.aspbContactMission.cmcActiveContentIdHashes[paramArrayPos])

ENDFUNC



// ===========================================================================================================
//      Client Active CMs Array
// ===========================================================================================================

// PURPOSE:	Add details of a current Contact Mission being offered to the player onto the Client Active CMs array
//
// INPUT PARAMS:		paramHeistContactID		The Contact for this CM as a Heist Contact ID
//						paramPreviouslyPlayed	TRUE if the mission was previosuly played, FLASE if it was a new unplayed mission
// REFERENCE PARAMS:	refMissionIdData		The Mission Id Data for the Contact Mission
//
// NOTES:	Assume any 'number allowed due to rank' checks were made before this was issued, so just find an empty slot to store the details
PROC Add_Details_To_Client_Active_CM_Array(MP_MISSION_ID_DATA &refMissionIdData, g_eFMHeistContactIDs paramHeistContactID, BOOL paramPreviouslyPlayed)

	INT tempLoop = 0
	
	REPEAT MAX_CONTACT_MISSION_INVITES_AT_ONCE tempLoop
		IF (g_lcacmActiveCMs[tempLoop].lcacmContact = NO_FM_HEIST_CONTACT)
			// ...found a free slot, so store the details
			g_lcacmActiveCMs[tempLoop].lcacmContact				= paramHeistContactID
			g_lcacmActiveCMs[tempLoop].lcacmMissionIdData		= refMissionIdData
			g_lcacmActiveCMs[tempLoop].lcacmPreviouslyCompleted	= paramPreviouslyPlayed
			g_lcacmActiveCMs[tempLoop].lcacmForcedAtRank		= Is_This_CM_Forced_At_Rank(refMissionIdData.idCloudFilename)
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [ActSelect][ContactMission]: Storing Current CM details in Active arraypos: ")
				NET_PRINT_INT(tempLoop)
				NET_NL()
				
				Debug_Output_Active_CMs_Array()
			#ENDIF
	
			// Update the Player Broadcast array of active ContentIDs so they are available to other players in the session
			Update_Active_CMs_On_Broadcast_Array()
			
			EXIT
		ENDIF
	ENDREPEAT
	
	// Failed to find a free space - this should have been checked for upfront so should never happen
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Failed to find a free space on the Client Active CMs array")
		NET_NL()
		Debug_Output_Active_CMs_Array()
		SCRIPT_ASSERT("Add_Details_To_Client_Active_CM_Array() - No free spaces. See Console Log. Tell Keith.")
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if there are any missions on the active CM Array
//
// RETURN VALUE:		BOOL				TRUE if there are any previously unplayed missions in the array, otherwise FALSE
FUNC BOOL Are_There_Any_Previously_Unplayed_Missions_In_The_Active_CM_Array()

	INT tempLoop = 0
	
	REPEAT MAX_CONTACT_MISSION_INVITES_AT_ONCE tempLoop
		IF (g_lcacmActiveCMs[tempLoop].lcacmContact != NO_FM_HEIST_CONTACT)
			// ...found a mission on the array
			IF NOT (g_lcacmActiveCMs[tempLoop].lcacmPreviouslyCompleted)
				// ...and this mission was not a previously completed mission being offered again to the player
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is in the active CM Array
//
// INPUT PARAMS:		paramContentID		The ContentID
// RETURN VALUE:		BOOL				TRUE if it is in the array, otherwise FALSE
FUNC BOOL Is_Mission_In_The_Active_CM_Array(STRING paramContentID)

	IF (IS_STRING_NULL_OR_EMPTY(paramContentID))
		RETURN FALSE
	ENDIF

	INT tempLoop = 0
	
	REPEAT MAX_CONTACT_MISSION_INVITES_AT_ONCE tempLoop
		IF (g_lcacmActiveCMs[tempLoop].lcacmContact != NO_FM_HEIST_CONTACT)
			// ...found a slot containing an Active CM, so compare content IDs
			IF (ARE_STRINGS_EQUAL(paramContentID, g_lcacmActiveCMs[tempLoop].lcacmMissionIdData.idCloudFilename))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is in the active CM Array
//
// INPUT PARAMS:		paramHeistContact	The FM Heist ContactID
// RETURN VALUE:		BOOL				TRUE if it is in the array, otherwise FALSE
FUNC BOOL Is_HeistContact_In_The_Active_CM_Array(g_eFMHeistContactIDs paramHeistContact)

	IF (paramHeistContact = NO_FM_HEIST_CONTACT)
		RETURN FALSE
	ENDIF

	INT tempLoop = 0
	
	REPEAT MAX_CONTACT_MISSION_INVITES_AT_ONCE tempLoop
		IF (g_lcacmActiveCMs[tempLoop].lcacmContact = paramHeistContact)
			// ...found a slot containing this Heist Contact
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Remove the mission if it is on the active CM Array
//
// INPUT PARAMS:		paramContentID		The ContentID
PROC Remove_This_Mission_If_On_Active_CM_Array(STRING paramContentID)
	IF (IS_STRING_NULL_OR_EMPTY(paramContentID))
		EXIT
	ENDIF

	INT		theToPos		= 0
	INT		theFromPos		= 0
	
	#IF IS_DEBUG_BUILD
		BOOL	foundMission	= FALSE
	#ENDIF
	
	WHILE (theFromPos < MAX_CONTACT_MISSION_INVITES_AT_ONCE)
		IF (g_lcacmActiveCMs[theFromPos].lcacmContact != NO_FM_HEIST_CONTACT)
			// ...found a slot containing an Active CM, so compare content IDs
			IF (ARE_STRINGS_EQUAL(paramContentID, g_lcacmActiveCMs[theFromPos].lcacmMissionIdData.idCloudFilename))
				// ...found the mission, so it will be removed
				#IF IS_DEBUG_BUILD
					foundMission = TRUE
				
					NET_PRINT("...KGM MP [ActSelect][ContactMission]: Removing this Content ID from Active CMs Array: ")
					NET_PRINT(paramContentID)
					NET_NL()
				#ENDIF
			ELSE
				// ...shuffle the array if necessary
				IF (theToPos != theFromPos)
					g_lcacmActiveCMs[theToPos].lcacmContact				= g_lcacmActiveCMs[theFromPos].lcacmContact
					g_lcacmActiveCMs[theToPos].lcacmMissionIdData		= g_lcacmActiveCMs[theFromPos].lcacmMissionIdData
					g_lcacmActiveCMs[theToPos].lcacmPreviouslyCompleted	= g_lcacmActiveCMs[theFromPos].lcacmPreviouslyCompleted
					g_lcacmActiveCMs[theToPos].lcacmForcedAtRank		= g_lcacmActiveCMs[theFromPos].lcacmForcedAtRank
				ENDIF
					
				theToPos++
			ENDIF
		ENDIF
		
		theFromPos++
	ENDWHILE
	
	// Clear any remaining array positions
	WHILE (theToPos < MAX_CONTACT_MISSION_INVITES_AT_ONCE)
		g_lcacmActiveCMs[theToPos].lcacmContact	= NO_FM_HEIST_CONTACT
		Clear_MP_MISSION_ID_DATA_Struct(g_lcacmActiveCMs[theToPos].lcacmMissionIdData)
		g_lcacmActiveCMs[theToPos].lcacmPreviouslyCompleted	= FALSE
		g_lcacmActiveCMs[theToPos].lcacmForcedAtRank		= FALSE
		
		theToPos++
	ENDWHILE
	
	// Update the Player Broadcast array of active ContentIDs so they are available to other players in the session
	Update_Active_CMs_On_Broadcast_Array()
	
	// Debug output if the content ID wasn't found on the array
	#IF IS_DEBUG_BUILD
		IF NOT (foundMission)
			NET_PRINT("...KGM MP [ActSelect][ContactMission]: Content ID wasn't on Active CMs Array: ")
			NET_PRINT(paramContentID)
			NET_NL()
		ENDIF
	#ENDIF
	
	// Output the updated array contents
	#IF IS_DEBUG_BUILD
		Debug_Output_Active_CMs_Array()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the maximum number of CMs that the player can be actively invited to at this rank
//
// RETURN VALUE:		INT			The number of CMs
FUNC INT Get_Max_CMs_At_This_Rank()

	// Use the missionsatcoords stored version of the player rank
	INT playerRank	= g_sAtCoordsControlsMP.matccPlayerRank
	INT maxAtOnce	= 0
	
	IF (playerRank >= MIN_CM_RANK_ALLOWING_3_CM_MAX)
		maxAtOnce = 3
	ELIF (playerRank >= MIN_CM_RANK_ALLOWING_2_CM_MAX)
		maxAtOnce = 2
	ELIF (playerRank >= MIN_CM_RANK_ALLOWING_1_CM_MAX)
		maxAtOnce = 1
	ENDIF
	
	RETURN (maxAtOnce)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player is allowed another Contact Mission at the current rank
//
// RETURN VALUE:		BOOL			TRUE if another CM is allowed at the current rank, otherwise FALSE
FUNC BOOL Is_Player_Allowed_To_Be_Offered_Another_CM_At_This_Rank()

	INT maxAtThisRank	= Get_Max_CMs_At_This_Rank()
	INT tempLoop		= 0
	
	REPEAT maxAtThisRank tempLoop
		IF (g_lcacmActiveCMs[tempLoop].lcacmContact = NO_FM_HEIST_CONTACT)
			// Found an empty slot within the valid array locations at this current rank, so allow another CM Invite
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if there is a 'Forced At Rank' mission already on the Active CM Array
//
// RETURN VALUE:		BOOL			TRUE if there is a 'forced at rank' mission on the array, FALSE if not
FUNC BOOL Is_There_A_Forced_At_Rank_Mission_On_Active_CM_Array()

	INT maxAtThisRank	= Get_Max_CMs_At_This_Rank()
	INT tempLoop		= 0
	
	REPEAT maxAtThisRank tempLoop
		IF (g_lcacmActiveCMs[tempLoop].lcacmContact != NO_FM_HEIST_CONTACT)
			// Found an entry on hte array, so check if it is a 'forced at rank' entry
			IF (g_lcacmActiveCMs[tempLoop].lcacmForcedAtRank)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC



// ===========================================================================================================
//      General Contact Mission Array Access Functions
// ===========================================================================================================

// PURPOSE:	Clear the local Contact Mission struct ready for a refresh of the data
PROC Clear_Local_Contact_Missions()

	// KGM 17/4/14: CnC needs this too

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Clear Local Contact Mission Data") NET_NL()
	#ENDIF
	
	g_structLocalContactMissions	emptyLocalCM
	INT								tempLoop		= 0
	
	REPEAT MAX_CONTACT_MISSIONS tempLoop
		g_sLocalMPCMs[tempLoop] = emptyLocalCM
	ENDREPEAT
	
	g_numLocalMPCMs = 0

ENDPROC


FUNC BOOL CAN_CONTACT_MISSION_BE_OFFERED_FROM_ROOT_CONTENT_ID(INT iRootContentIDHash)
	IF IS_THIS_MISSION_A_CASINO_HEIST_MISSION(iRootContentIDHash)
	OR GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(iRootContentIDHash)
	#IF FEATURE_HEIST_ISLAND
	OR IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(iRootContentIDHash)
	#ENDIF
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get a random Contact Mission matching these details
//
// INPUT PARAMS:		paramContactID		The ContactID (or NO_CHARACTER to ignore contactID)
//						paramPlayerRank		The player's current rank (passed in to avoid cyclic headers) - NOTE: For CnC this should be the Player Gang Rank
//						paramRankUse		How Rank should be used when comparing missions
//						paramUseClosest		[DEFAULT = FALSE] TRUE means use the closest available mission rather than a randomly chosen mission, FALSE means use random as usual
//						paramNumRandom		[DEFAULT = 1] When using closest missions to player, this allows a random chose from the closest few
// RETURN VALUE:		INT					Array Position of Contact Mission matching these details, or ILLEGAL_CONTACT_MISSION_SLOT if none
//
// NOTES:	31/1/14: It's fine for this function to continue to use contentID Hash instead of rootContentID Hash because it isn't doing anything with the 'played mission' STATs storage
FUNC INT Get_Random_Contact_Mission_Matching_These_Details(enumCharacterList paramContactID, INT paramPlayerRank, g_eContactMissionSelection paramRankUse, BOOL paramUseClosest = FALSE, INT paramNumRandom = 1)

	IF (paramNumRandom < 1)
		paramNumRandom = 1
	ENDIF

	INT tempLoop				= 0
	INT	numPossibleCMs			= 0
	
	INT	possibleCMs[MAX_CONTACT_MISSIONS]
	
	// Gather a list of all possible activities
	REPEAT MAX_CONTACT_MISSIONS tempLoop
		IF (Do_Contact_Missions_Slot_Details_Match(tempLoop, paramContactID, paramPlayerRank, paramRankUse))
			// ...found a match
			possibleCMs[numPossibleCMs] = tempLoop
			numPossibleCMs++
		ENDIF
	ENDREPEAT
	
	// Return an activity
	IF (numPossibleCMs = 0)
		RETURN (ILLEGAL_CONTACT_MISSION_SLOT)
	ENDIF

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Choosing a Contact Mission from list of potential CMs matching these details:") NET_NL()
		NET_PRINT("   ")
		IF (paramContactID != NO_CHARACTER)
			NET_PRINT("   [CONTACT: ") NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sCharacterSheetAll[paramContactID].label)) NET_PRINT("]")
		ENDIF
		
		NET_PRINT("   [RANK INFO: MAX RANK: ") 
		
		IF paramRankUse = CM_RANK_USE_NO_PLAYER_RECORD_BELOW_CM_RANK
			NET_PRINT_INT(g_CMsPlayed.cmrmMinRank) 
		ELSE
			NET_PRINT_INT(paramPlayerRank) 
		ENDIF
		NET_PRINT("  MIN RANK: ")
		SWITCH (paramRankUse)
			CASE CM_RANK_USE_UNPLAYED_BETWEEN_CM_MIN_AND_CURRENT_RANKS
				NET_PRINT_INT(g_CMsPlayed.cmrmMinRank)
				NET_PRINT("] - ")
				NET_PRINT("Look for UNPLAYED MISSIONS only")
				BREAK
			
			CASE CM_RANK_USE_NO_PLAYER_RECORD_BELOW_CM_RANK
				NET_PRINT_INT(0)
				NET_PRINT("] - ")
				NET_PRINT("Look for 'NO PLAYER RECORD' MISSIONS only")
				BREAK
			
			CASE CM_RANK_USE_PREVIOUSLY_PLAYED_BELOW_CURRENT_RANK
				NET_PRINT_INT(0)
				NET_PRINT("] - ")
				NET_PRINT("Look for PLAYED MISSIONS only")
				BREAK
				
			DEFAULT
				NET_PRINT("UNKNOWN RANK USE")
				BREAK
		ENDSWITCH
		NET_NL()
		IF (paramUseClosest)
			NET_PRINT("  [CHOOSE CLOSEST TO PLAYER IF POSSIBLE, randomise closest ")
			NET_PRINT_INT(paramNumRandom)
			NET_PRINT("]")
		ELSE
			NET_PRINT("  [CHOOSE RANDOMLY]")
		ENDIF
		NET_NL()
	#ENDIF
	
	// If using current rank then weed out all the played missions, and those currently being offered, and missions from the most recent contact
	INT		theTo		= 0
	INT		theFrom		= 0
	
	IF (paramRankUse = CM_RANK_USE_UNPLAYED_BETWEEN_CM_MIN_AND_CURRENT_RANKS)
		INT						theMax			= numPossibleCMs
		INT						cmCreatorID		= -1
		INT						cmVariation		= -1
		INT						cmOnHours		= MATC_ALL_DAY
		BOOL					cmIsPlayed		= FALSE
		BOOL					okForOnHours	= FALSE
		g_eFMHeistContactIDs	cmContactID		= NO_FM_HEIST_CONTACT
		TEXT_LABEL_23			cmFilename		= ""
		
		REPEAT theMax theFrom
			cmCreatorID	= g_sLocalMPCMs[possibleCMs[theFrom]].lcmCreatorID
			cmVariation	= g_sLocalMPCMs[possibleCMs[theFrom]].lcmVariation
			cmIsPlayed	= g_sLocalMPCMs[possibleCMs[theFrom]].lcmPlayed
			cmContactID	= g_sLocalMPCMs[possibleCMs[theFrom]].lcmContact
			cmOnHours	= g_sLocalMPCMs[possibleCMs[theFrom]].lcmOnHours
			cmFilename	= Get_FileName_For_FM_Cloud_Loaded_Activity(cmCreatorID, cmVariation)
			
			IF (Can_Invite_Be_Displayed_At_This_Hour(cmOnHours))
			AND (Will_Invite_Be_Ok_To_Display_Next_Hour(cmOnHours))
				okForOnHours = TRUE
			ENDIF
			
			IF NOT (cmIsPlayed)
			AND NOT (Is_Mission_In_The_Active_CM_Array(cmFilename))
			AND NOT (Is_This_The_Most_Recent_CM_Contact(cmContactID))
			AND NOT (Is_This_CM_Contact_Offering_A_Heist(cmContactID))
			AND (okForOnHours)
			AND CAN_CONTACT_MISSION_BE_OFFERED_FROM_ROOT_CONTENT_ID(g_sLocalMPCMs[possibleCMs[theFrom]].lcmRootContentIdHash)
				// ...not played and available, so keep in the possibleCM list
				possibleCMs[theTo] = possibleCMs[theFrom]
				theTo++
			ELSE
				#IF IS_DEBUG_BUILD
					NET_PRINT("      IGNORE: ")
					IF (cmIsPlayed)
						NET_PRINT("Already Played    (")
					ELIF (Is_This_The_Most_Recent_CM_Contact(cmContactID))
						NET_PRINT("Recent CM Contact (")
					ELIF (Is_This_CM_Contact_Offering_A_Heist(cmContactID))
						NET_PRINT("CM Contact May Be Offering Heist (")
					ELIF NOT (okForOnHours)
						NET_PRINT("Closed/Closing    (")
					ELIF NOT CAN_CONTACT_MISSION_BE_OFFERED_FROM_ROOT_CONTENT_ID(g_sLocalMPCMs[possibleCMs[theFrom]].lcmRootContentIdHash)
						NET_PRINT("Root Content id (")
					ELSE
						NET_PRINT("Already Invited   (")
					ENDIF
					NET_PRINT(cmFilename)
					NET_PRINT(":")
					NET_PRINT_INT(GET_HASH_KEY(cmFilename))
					NET_PRINT(") ")
					Debug_Output_Local_Contact_Missions_Slot_Details_On_One_Line(possibleCMs[theFrom])
					NET_NL()
				#ENDIF
			ENDIF
		ENDREPEAT
		
		// Update the numPossibleCMs after all the played and invited missions have been weeded out
		numPossibleCMs = theTo
	ENDIF
	
	// This should find missions below the CM minimum and return those missions that are unplayed.
	IF (paramRankUse = CM_RANK_USE_NO_PLAYER_RECORD_BELOW_CM_RANK)
		INT						theMax			= numPossibleCMs
		INT						cmOnHours		= MATC_ALL_DAY
		BOOL					okForOnHours	= FALSE
		BOOL					cmIsPlayed		= FALSE
		g_eFMHeistContactIDs	cmContactID		= NO_FM_HEIST_CONTACT
		
		REPEAT theMax theFrom		
			IF g_sLocalMPCMs[possibleCMs[theFrom]].lcmCreatorID = FMMC_ROCKSTAR_CREATOR_ID
				cmIsPlayed	= IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[g_sLocalMPCMs[possibleCMs[theFrom]].lcmVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED)
			ELSE
				SCRIPT_ASSERT("[ContactList] The mission is not marked as Rockstar Created so we can't get the played value. Need to update Get_Random_Contact_Mission_Matching_These_Details()")
			ENDIF
			
			cmOnHours	= g_sLocalMPCMs[possibleCMs[theFrom]].lcmOnHours
			cmContactID	= g_sLocalMPCMs[possibleCMs[theFrom]].lcmContact
			
			IF (Can_Invite_Be_Displayed_At_This_Hour(cmOnHours))
			AND (Will_Invite_Be_Ok_To_Display_Next_Hour(cmOnHours))
				okForOnHours = TRUE
			ENDIF
			
			IF NOT (cmIsPlayed)
			AND NOT (Is_This_The_Most_Recent_CM_Contact(cmContactID))
			AND NOT (Is_This_CM_Contact_Offering_A_Heist(cmContactID))
			AND (okForOnHours)
			AND CAN_CONTACT_MISSION_BE_OFFERED_FROM_ROOT_CONTENT_ID(g_sLocalMPCMs[possibleCMs[theFrom]].lcmRootContentIdHash)
				// ...'played' but not a 'one play' mission, so keep in the possibleCM list
				possibleCMs[theTo] = possibleCMs[theFrom]
				theTo++
			ELSE
				#IF IS_DEBUG_BUILD
					IF (cmIsPlayed)
						NET_PRINT("      IGNORE: THIS ONE HAS BEEN PLAYED - ")
					ELIF (Is_This_The_Most_Recent_CM_Contact(cmContactID))
						NET_PRINT("      IGNORE: RECENT CM CONTACT - ")
					ELIF (Is_This_CM_Contact_Offering_A_Heist(cmContactID))
						NET_PRINT("      IGNORE: CM CONTACT MAY BE OFFERING HEIST - ")
					ELIF NOT (okForOnHours)
						NET_PRINT("      IGNORE: CLOSED or CLOSING - ")
					ELIF NOT CAN_CONTACT_MISSION_BE_OFFERED_FROM_ROOT_CONTENT_ID(g_sLocalMPCMs[possibleCMs[theFrom]].lcmRootContentIdHash)
						NET_PRINT("      IGNORE: ROOT CONTENT ID - ")
					ELSE
						NET_PRINT("      IGNORE: UNKNOWN IGNORE REASON - ")
					ENDIF
					Debug_Output_Local_Contact_Missions_Slot_Details_On_One_Line(possibleCMs[theFrom])
					NET_NL()
				#ENDIF
			ENDIF
		ENDREPEAT
		
		// Update the numPossibleCMs
		numPossibleCMs = theTo
	ENDIF
	
	// If using previous rank missions then weed out all unplayed missions and all those that are One Play only - they shouldn't get offered again
	// NOTE: Changes to this function need reflected in net_activity_selector_public::Do_Any_Played_Contact_Missions_Match_These_Details()
	IF (paramRankUse = CM_RANK_USE_PREVIOUSLY_PLAYED_BELOW_CURRENT_RANK)
		INT						theMax			= numPossibleCMs
		INT						cmOnHours		= MATC_ALL_DAY
		BOOL					okForOnHours	= FALSE
		BOOL					cmIsOnePlay		= FALSE
		BOOL					cmIsPlayed		= FALSE
		g_eFMHeistContactIDs	cmContactID		= NO_FM_HEIST_CONTACT
		
		REPEAT theMax theFrom
			cmIsOnePlay	= g_sLocalMPCMs[possibleCMs[theFrom]].lcmOnePlay
			cmIsPlayed	= g_sLocalMPCMs[possibleCMs[theFrom]].lcmPlayed
			cmOnHours	= g_sLocalMPCMs[possibleCMs[theFrom]].lcmOnHours
			cmContactID	= g_sLocalMPCMs[possibleCMs[theFrom]].lcmContact
			
			IF (Can_Invite_Be_Displayed_At_This_Hour(cmOnHours))
			AND (Will_Invite_Be_Ok_To_Display_Next_Hour(cmOnHours))
				okForOnHours = TRUE
			ENDIF
			
			IF (cmIsPlayed)
			AND NOT (cmIsOnePlay)
			AND NOT (Is_This_The_Most_Recent_CM_Contact(cmContactID))
			AND NOT (Is_This_CM_Contact_Offering_A_Heist(cmContactID))
			AND (okForOnHours)
			AND CAN_CONTACT_MISSION_BE_OFFERED_FROM_ROOT_CONTENT_ID(g_sLocalMPCMs[possibleCMs[theFrom]].lcmRootContentIdHash)
				// ...'played' but not a 'one play' mission, so keep in the possibleCM list
				possibleCMs[theTo] = possibleCMs[theFrom]
				theTo++
			ELSE
				#IF IS_DEBUG_BUILD
					IF NOT (cmIsPlayed)
						NET_PRINT("      IGNORE: NOT PLAYED YET - ")
					ELIF (Is_This_The_Most_Recent_CM_Contact(cmContactID))
						NET_PRINT("      IGNORE: RECENT CM CONTACT - ")
					ELIF (Is_This_CM_Contact_Offering_A_Heist(cmContactID))
						NET_PRINT("      IGNORE: CM CONTACT MAY BE OFFERING HEIST - ")
					ELIF NOT (okForOnHours)
						NET_PRINT("      IGNORE: CLOSED or CLOSING - ")
					ELIF NOT CAN_CONTACT_MISSION_BE_OFFERED_FROM_ROOT_CONTENT_ID(g_sLocalMPCMs[possibleCMs[theFrom]].lcmRootContentIdHash)
						NET_PRINT("      IGNORE: ROOT CONTENT ID - ")
					ELSE
						NET_PRINT("      IGNORE: ONE PLAY MISSION - ")
					ENDIF
					Debug_Output_Local_Contact_Missions_Slot_Details_On_One_Line(possibleCMs[theFrom])
					NET_NL()
				#ENDIF
			ENDIF
		ENDREPEAT
		
		// Update the numPossibleCMs after all the One Play missions have been weeded out
		numPossibleCMs = theTo
	ENDIF
	
	IF (numPossibleCMs = 0)
		RETURN (ILLEGAL_CONTACT_MISSION_SLOT)
	ENDIF
	
	BOOL					useClosest 							= paramUseClosest
	FLOAT					unknownDistance						= 88888.8
	FLOAT					activeContactAdditionalDistance		= 20000.0
	FLOAT					cmWithOtherPlayerAdditionalDistance	= 12000.0
	FLOAT					thisDistance						= 0.0
	VECTOR					thisLocation						= << 0.0, 0.0, 0.0 >>
	g_eFMHeistContactIDs	thisFMHeistContact					= NO_FM_HEIST_CONTACT
	
	// Clear out a distances array
	FLOAT cmDistances[MAX_CONTACT_MISSIONS]
	REPEAT MAX_CONTACT_MISSIONS tempLoop
		cmDistances[tempLoop] = unknownDistance
	ENDREPEAT
	
	// Gather the distances from the player for the ones remaining
	VECTOR playerPos = << 0.0, 0.0, 0.0 >>
	IF (IS_NET_PLAYER_OK(PLAYER_ID(), FALSE))
		playerPos = GET_PLAYER_COORDS(PLAYER_ID())

		#IF IS_DEBUG_BUILD
			NET_PRINT("      Gathering mission distances closest to coords: ")
			NET_PRINT_VECTOR(playerPos)
			NET_NL()
		#ENDIF
		
		REPEAT numPossibleCMs tempLoop
			thisLocation		= Get_Contact_Mission_Start_Location_For_Slot(possibleCMs[tempLoop])
			thisFMHeistContact	= g_sLocalMPCMs[possibleCMs[tempLoop]].lcmContact
					
			#IF IS_DEBUG_BUILD
				NET_PRINT("         ")
				NET_PRINT_INT(possibleCMs[tempLoop])
				NET_PRINT(": ")
				NET_PRINT_VECTOR(thisLocation)
			#ENDIF
					
			IF NOT (ARE_VECTORS_ALMOST_EQUAL(thisLocation, << 0.0, 0.0, 0.0 >>))
				thisDistance = GET_DISTANCE_BETWEEN_COORDS(thisLocation, playerPos)
						
				#IF IS_DEBUG_BUILD
					NET_PRINT("   (")
					NET_PRINT_FLOAT(thisDistance)
					NET_PRINT("m)")
					NET_PRINT(" [ContentId Hash: ")
					NET_PRINT_INT(g_sLocalMPCMs[possibleCMs[tempLoop]].lcmContentIdHash)
					NET_PRINT("]")
					NET_PRINT(" [RootContentId Hash: ")
					NET_PRINT_INT(g_sLocalMPCMs[possibleCMs[tempLoop]].lcmRootContentIdHash)
					NET_PRINT("]")
				#ENDIF
				
				// If this mission's contact already has an active CM Invite then increase the distance so this mission becomes less favourable
				IF (Is_HeistContact_In_The_Active_CM_Array(thisFMHeistContact))
					thisDistance += activeContactAdditionalDistance
						
					#IF IS_DEBUG_BUILD
						NET_PRINT(" - Invite from Contact already Active. Adding ")
						NET_PRINT_FLOAT(activeContactAdditionalDistance)
						NET_PRINT("m")
						NET_PRINT("   (= ")
						NET_PRINT_FLOAT(thisDistance)
						NET_PRINT("m)")
					#ENDIF
				ENDIF
			ELSE
				thisDistance = 99999.9
						
				#IF IS_DEBUG_BUILD
					NET_PRINT("   (Too Close To Origin)")
				#ENDIF
			ENDIF
			
			// Store the distance
			cmDistances[tempLoop] = thisDistance
			
			#IF IS_DEBUG_BUILD
				NET_NL()
			#ENDIF
		ENDREPEAT
	ELSE
		// ...player is not ok, so just use random selection
		useClosest = FALSE
		
		IF (paramUseClosest)
			#IF IS_DEBUG_BUILD
				NET_PRINT("      Player is not OK, so using random selection, not closest selection")
				NET_NL()
			#ENDIF
		ENDIF
	ENDIF
	
	// Need to take active CMs on other player's phones into account

	// I'll do this by adding distance for every match found - the more times the same mission is found the less favourable it becomes
	CONST_INT	MAX_OTHER_PLAYER_ACTIVE_CMS		(NUM_NETWORK_PLAYERS * MAX_CONTACT_MISSION_INVITES_AT_ONCE)
	
	// ...gather a list of contentIDs active with other players
	// NOTE 31/1/14: This is current contentIDs, not rootContentIDs - there was no reason to swap over to rootContentIDs for the currently Active Missions information
	INT otherPlayerHashes[MAX_OTHER_PLAYER_ACTIVE_CMS]
	INT numOtherHashes = 0
	PLAYER_INDEX thisPlayer = INVALID_PLAYER_INDEX()
	INT hashesLoop = 0
	INT thisContentIdHash = 0
	
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		thisPlayer = INT_TO_PLAYERINDEX(tempLoop)
		
		IF (thisPlayer != PLAYER_ID())
			IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
				REPEAT MAX_CONTACT_MISSION_INVITES_AT_ONCE hashesLoop
					thisContentIdHash = Return_Active_CM_ContentID_Hash_For_Player_At_ArrayPos(thisPlayer, hashesLoop)
					IF (thisContentIdHash != 0)
						otherPlayerHashes[numOtherHashes] = thisContentIdHash
						numOtherHashes++
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDREPEAT
	
	// ...find matching contentIDs in the potential missions list and add a distance to make it more unfavourable
	BOOL	keepSearching						= FALSE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Making Active Contact Missions Invites with other players less favourable") NET_NL()
		IF (numOtherHashes = 0)
			NET_PRINT("      - NO OTHER PLAYERS HAVE ACTIVE CM INVITES") NET_NL()
		ENDIF
		
		INT foundAtPosition = ILLEGAL_ARRAY_POSITION
	#ENDIF
	
	hashesLoop = 0
	WHILE (hashesLoop < numOtherHashes)
		keepSearching	= TRUE
		tempLoop		= 0
	
		#IF IS_DEBUG_BUILD
			NET_PRINT("      Checking Other Player's CM: ") NET_PRINT_INT(otherPlayerHashes[hashesLoop])
			
			foundAtPosition = ILLEGAL_ARRAY_POSITION
		#ENDIF
		
		WHILE (keepSearching)
			IF (otherPlayerHashes[hashesLoop] = g_sLocalMPCMs[possibleCMs[tempLoop]].lcmContentIdHash)
				// ...found a match
				keepSearching = FALSE
				cmDistances[tempLoop] += cmWithOtherPlayerAdditionalDistance
				
				#IF IS_DEBUG_BUILD
					foundAtPosition = tempLoop
				#ENDIF
			ENDIF
			
			IF (keepSearching)
				tempLoop++
				
				IF (tempLoop >= numPossibleCMs)
					keepSearching = FALSE
				ENDIF
			ENDIF
		ENDWHILE
		
		#IF IS_DEBUG_BUILD
			IF (foundAtPosition = ILLEGAL_ARRAY_POSITION)
				NET_PRINT(" - not found")
			ELSE
				NET_PRINT(" - FOUND: Increasing distance by ")
				NET_PRINT_FLOAT(cmWithOtherPlayerAdditionalDistance)
				NET_PRINT("m to make it less favourable.")
				NET_NL()
				NET_PRINT("         Now: ")
				NET_PRINT_FLOAT(cmDistances[tempLoop])
				NET_PRINT("m  [Coords: ")
				NET_PRINT_VECTOR(Get_Contact_Mission_Start_Location_For_Slot(possibleCMs[tempLoop]))
				NET_PRINT("]")
			ENDIF
			
			NET_NL()
		#ENDIF
	
		hashesLoop++
	ENDWHILE
	
	// Sort the array into distance order
	FLOAT	holdDistance	= 0.0
	INT		holdPossibleCM	= 0
	
	IF (useClosest)
		BOOL somethingChanged = TRUE
		
		WHILE (somethingChanged)
			somethingChanged = FALSE
			
			theTo	= 0
			theFrom	= theTo + 1
			
			WHILE (theFrom < numPossibleCMs)
				IF (cmDistances[theFrom] < cmDistances[theTo])
					// Need to swap the order
					somethingChanged = TRUE
					
					holdDistance	= cmDistances[theTo]
					holdPossibleCM	= possibleCMs[theTo]
					
					cmDistances[theTo]	= cmDistances[theFrom]
					possibleCMs[theTo]	= possibleCMs[theFrom]
					
					cmDistances[theFrom]	= holdDistance
					possibleCMs[theFrom]	= holdPossibleCM
				ENDIF
				
				theFrom++
				theTo++
			ENDWHILE
		ENDWHILE

		// Output the sorted array
		#IF IS_DEBUG_BUILD
			NET_PRINT("      Sorted mission distances closest to coords: ")
			NET_PRINT_VECTOR(playerPos)
			NET_NL()
		#ENDIF
		
		REPEAT numPossibleCMs tempLoop
			#IF IS_DEBUG_BUILD
				NET_PRINT("         ")
				NET_PRINT_INT(possibleCMs[tempLoop])
				NET_PRINT(": ")
				NET_PRINT_FLOAT(cmDistances[tempLoop])
				NET_NL()
			#ENDIF
		ENDREPEAT
	ENDIF
	
	// Work out how many distances are below the 'with other player' threshold
	// NOTE: the threshold value is fairly arbitrary - it's just a big distance
	INT priorityMissions = 0
	
	REPEAT numPossibleCMs tempLoop
		IF (cmDistances[tempLoop] < cmWithOtherPlayerAdditionalDistance)
			priorityMissions++
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Number of 'priority' missions - these haven't had their distances modified so are better candidates: ")
		NET_PRINT_INT(priorityMissions)
		NET_NL()
	#ENDIF
	
	// Choose an appropriate mission
	INT chosenCM		= 0
	INT maxToRandomise	= 0
	
	IF (numPossibleCMs > 1)
		maxToRandomise = numPossibleCMs
		IF (useClosest)
			IF (paramNumRandom < maxToRandomise)
				maxToRandomise = paramNumRandom
			ENDIF
			
			// Reduce the 'randomiser' value if a few of the missions are better candidates than the others (ie: their contact hasn't already offered a mission)
			IF (priorityMissions > 0)
			AND (priorityMissions < maxToRandomise)
				maxToRandomise = priorityMissions
			ENDIF
		ENDIF
	
		// Use a random CM from the list
		chosenCM = GET_RANDOM_INT_IN_RANGE(0, maxToRandomise)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		REPEAT numPossibleCMs tempLoop
			NET_PRINT("      ")
			Debug_Output_Local_Contact_Missions_Slot_Details_On_One_Line(possibleCMs[tempLoop])
			IF (chosenCM = tempLoop)
				NET_PRINT("   [CHOSEN CONTACT MISSION - ")
				IF (useClosest)
					NET_PRINT("CLOSEST TO PLAYER")
					IF (maxToRandomise > 1)
						NET_PRINT(", randomised from nearest ")
						NET_PRINT_INT(maxToRandomise)
					ENDIF
					NET_PRINT("]")
				ELSE
					NET_PRINT("RANDOMLY SELECTED]")
				ENDIF
			ENDIF
			NET_NL()
		ENDREPEAT
	#ENDIF
	
	RETURN (possibleCMs[chosenCM])

ENDFUNC


// PURPOSE:	Get a random Contact Mission matching these details
//
// INPUT PARAMS:		paramContactID		The ContactID (or NO_CHARACTER to ignore contactID)
//						paramPlayerRank		The player's current rank (passed in to avoid cyclic headers) - NOTE: For CnC this should be the Player Gang Rank
//						paramRankUse		How Rank should be used when comparing missions
//						paramUseClosest		[DEFAULT = FALSE] TRUE means use the closest available mission rather than a randomly chosen mission, FALSE means use random as usual
//						paramNumRandom		[DEFAULT = 1] When using closest missions to player, this allows a random chose from the closest few
// RETURN VALUE:		INT					Array Position of Contact Mission matching these details, or ILLEGAL_CONTACT_MISSION_SLOT if none
//
// NOTES:	Priority Order as follows:
//				- Unplayed Contact Missions between Min CM Rank and Current Player Rank
//				- Contact Missions without a Player Record under the Min CM Rank
//				- Previously Played Contact Missions under the Current Player Rank
FUNC INT Get_Random_Contact_Mission_For_Contact(enumCharacterList paramContactID, INT paramPlayerRank, BOOL paramUseClosest = FALSE, INT paramNumRandom = 1)

	INT thisCMArrayPos = ILLEGAL_CONTACT_MISSION_SLOT
	g_eContactMissionSelection thisCheckTypeID = CM_RANK_USE_UNPLAYED_BETWEEN_CM_MIN_AND_CURRENT_RANKS
	
	// Search for: Unplayed Contact Missions between Min CM Rank and Current Player Rank
	PRINTLN(".KGM [ActSelect][ContactMission]: Searching for unplayed BETWEEN Min CM Rank and Current Player Rank")
	
	thisCMArrayPos = Get_Random_Contact_Mission_Matching_These_Details(paramContactID, paramPlayerRank, thisCheckTypeID, paramUseClosest, paramNumRandom)
	IF (thisCMArrayPos != ILLEGAL_CONTACT_MISSION_SLOT)
		PRINTLN(".KGM [ActSelect][ContactMission]: ...FOUND. Returning CM Array Position: ", thisCMArrayPos)
		RETURN (thisCMArrayPos)
	ENDIF
	PRINTLN(".KGM [ActSelect][ContactMission]: ...NOT FOUND.")
	
	// Search for: Contact Missions without a Player Record under the Min CM Rank
	thisCheckTypeID = CM_RANK_USE_NO_PLAYER_RECORD_BELOW_CM_RANK
	PRINTLN(".KGM [ActSelect][ContactMission]: Searching for unplayed BELOW Min CM Rank (ie: NEW missions with no player record)")
	
	thisCMArrayPos = Get_Random_Contact_Mission_Matching_These_Details(paramContactID, paramPlayerRank, thisCheckTypeID, paramUseClosest, paramNumRandom)
	IF (thisCMArrayPos != ILLEGAL_CONTACT_MISSION_SLOT)
		PRINTLN(".KGM [ActSelect][ContactMission]: ...FOUND. Returning CM Array Position: ", thisCMArrayPos)
		RETURN (thisCMArrayPos)
	ENDIF
	PRINTLN(".KGM [ActSelect][ContactMission]: ...NOT FOUND.")
	
	// Search for: Previously Played Contact Missions under the Current Player Rank
	thisCheckTypeID = CM_RANK_USE_PREVIOUSLY_PLAYED_BELOW_CURRENT_RANK
	PRINTLN(".KGM [ActSelect][ContactMission]: Searching for previously played below Current Player Rank")
	
	thisCMArrayPos = Get_Random_Contact_Mission_Matching_These_Details(paramContactID, paramPlayerRank, thisCheckTypeID, paramUseClosest, paramNumRandom)
	IF (thisCMArrayPos != ILLEGAL_CONTACT_MISSION_SLOT)
		PRINTLN(".KGM [ActSelect][ContactMission]: ...FOUND. Returning CM Array Position: ", thisCMArrayPos)
		RETURN (thisCMArrayPos)
	ENDIF
	PRINTLN(".KGM [ActSelect][ContactMission]: ...NOT FOUND.")
	
	PRINTLN(".KGM [ActSelect][ContactMission]: There are NO available Contact Missions for this contact")
	RETURN (ILLEGAL_CONTACT_MISSION_SLOT)
	
ENDFUNC

FUNC INT GET_CONTACT_MISSION_ARRAY_POS_FROM_ROOT_CONTENT_ID_HASH(INT iRootContentIDHash)
	INT iCMArrayPos = -1

	INT iLoop
	REPEAT MAX_CONTACT_MISSIONS iLoop
		IF g_sLocalMPCMs[iLoop].lcmRootContentIdHash = iRootContentIDHash
			iCMArrayPos = iLoop
			BREAKLOOP
		ENDIF
	ENDREPEAT

	RETURN iCMArrayPos
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if there are any unplayed contact missions at the specified rank
//
// INPUT PARAMS:		paramRank			The specific rank to check
// RETURN VALUE:		BOOL				TRUE if there are unplayed contact missions at the rank, otherwise FALSE
FUNC BOOL Are_There_Unplayed_Contact_Missions_At_Rank(INT paramRank)

	INT tempLoop = 0

	REPEAT g_numLocalMPCMs tempLoop
		IF (g_sLocalMPCMs[tempLoop].lcmRank = paramRank)
			IF NOT (g_sLocalMPCMs[tempLoop].lcmPlayed)
				// ...found an unplayed mission at the specified rank
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// There are no unplayed missions at the specified rank
	RETURN FALSE

ENDFUNC




// ===========================================================================================================
//      General Contact Mission Functions
// ===========================================================================================================

// PURPOSE:	Get a Player's Contact Mission Stage
//
// INPUT PARAMS:		paramPlayerID						The Player Index for the client
// RETURN VALUE:		g_eContactMissionStagesClient		The current Client Contact Mission Stage
FUNC g_eContactMissionStagesClient Get_Player_Contact_Mission_Stage(PLAYER_INDEX paramPlayerID)
	RETURN (GlobalplayerBD[NATIVE_TO_INT(paramPlayerID)].playerActivitySelector.aspbContactMission.cmcStage)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set this player's Contact Mission stage
//
// INPUT PARAMS:		paramCMStage						The new Contact Mission Stage for this player
PROC Set_My_Contact_Mission_Stage(g_eContactMissionStagesClient paramCMStage)

	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcStage = paramCMStage

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Change Player's Contact Mission Stage to: ")
		NET_PRINT(Convert_Client_Contact_Mission_Stage_To_String(paramCMStage))
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Gather the MissionID Data for a Contact Mission and store it as the Chosen Contact Mission data
//
// INPUT PARMS:			paramSlotCM							The Contact Mission Slot
PROC Gather_And_Store_MissionID_Data_For_Contact_Mission(INT paramSlotCM)

	MP_MISSION_ID_DATA theMissionIdData = Gather_And_Get_MissionID_Data_For_Contact_Mission(paramSlotCM)

	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcMissionIdData = theMissionIdData
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Gathered MissionID Data for Contact Mission.")
		NET_NL()
		NET_PRINT("      Slot ")
		Debug_Output_Local_Contact_Missions_Slot_Details_On_One_Line(paramSlotCM)
		NET_NL()
		NET_PRINT("           [")
		Debug_Output_MissionID_Data_For_Chosen_Contact_Mission_On_One_Line()
		NET_PRINT("]")
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a player's Contact Mission Player Timeout has expired
//
// INPUT PARAMS:		paramPlayerID						The Player Index for the client
// RETURN VALUE:		BOOL								TRUE if the player's timeout has expired, FALSE if not
FUNC BOOL Has_Client_Contact_Mission_Player_Timeout_Expired(PLAYER_INDEX paramPlayerID)
	RETURN (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GlobalplayerBD[NATIVE_TO_INT(paramPlayerID)].playerActivitySelector.aspbContactMission.cmcTimeout))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get a player's Contact Mission MissionID Data for Chosen Contact Mission
//
// INPUT PARAMS:		paramPlayerID						The Player Index for the client
// RETURN VALUE:		MP_MISSION_ID_DATA					The MissionID Data for this Contact Mission
FUNC MP_MISSION_ID_DATA Get_Player_MissionID_Data_For_Chosen_CM(PLAYER_INDEX paramPlayerID)
	RETURN (GlobalplayerBD[NATIVE_TO_INT(paramPlayerID)].playerActivitySelector.aspbContactMission.cmcMissionIdData)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get a player's ContactID for Chosen Contact Mission
//
// INPUT PARAMS:		paramPlayerID						The Player Index for the client
// RETURN VALUE:		g_eFMHeistContactIDs					The MissionID Data for this Contact Mission
FUNC g_eFMHeistContactIDs Get_Player_Heist_ContactID_For_Chosen_CM(PLAYER_INDEX paramPlayerID)
	RETURN (GlobalplayerBD[NATIVE_TO_INT(paramPlayerID)].playerActivitySelector.aspbContactMission.cmcContact)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store Contact Mission details based on Contact Mission slot
//
// INPUT PARAMS:		paramSlotCM				The Contact Mission slot containing the mission to be offered
//						paramPrevious			TRUE if this is from the pool of previously completed missions, FALSE if it is a new mission
PROC Store_Contact_Mission_Based_On_Contact_Mission_Slot(INT paramSlotCM, BOOL paramPrevious)

	g_eFMHeistContactIDs thisHeistContactID = Get_Contact_Mission_ContactID_For_Slot(paramSlotCM)
	
	Gather_And_Store_MissionID_Data_For_Contact_Mission(paramSlotCM)
	
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcContact					= thisHeistContactID
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcMissionIdData			= Get_Player_MissionID_Data_For_Chosen_CM(PLAYER_ID())
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcPreviouslyCompleted		= paramPrevious
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Storing Details of this Contact Mission: ")
		MP_MISSION_ID_DATA	theMissionIdData	= Get_Player_MissionID_Data_For_Chosen_CM(PLAYER_ID())
		TEXT_LABEL_63		theMissionName		= Get_Mission_Name_For_FM_Cloud_Loaded_Activity(theMissionIdData)
		NET_PRINT(theMissionName)
		NET_PRINT("  [")
		NET_PRINT(theMissionIdData.idCloudFilename)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player should be offered a previously completed CM instead of a new CM
//
// RETURN VALUE:		BOOL			TRUE if offered a previously completed CM, otherwise FALSE
//
// NOTES:	A previous CM should only be given if the player's CM Rank is higher than the player's Game Rank
//			The check for the player being allowed another CM at the current rank should have already taken place, so assume it's ok to offer another
FUNC BOOL Check_If_Player_Should_Be_Offered_A_Previously_Completed_CM()

	INT playerRank = GET_PLAYER_RANK(PLAYER_ID())
	
	IF (g_CMsPlayed.cmrmMinRank <= playerRank)
		RETURN FALSE
	ENDIF
	
	// The CM Rank must be greater than the player's rank, so randomly choose one of the previous CMs to give to the player
	INT thisCMArrayPos = Get_Random_Contact_Mission_Matching_These_Details(NO_CHARACTER, playerRank, CM_RANK_USE_PREVIOUSLY_PLAYED_BELOW_CURRENT_RANK)
	IF (thisCMArrayPos = ILLEGAL_CONTACT_MISSION_SLOT)
		// ...no CMs were offered, so try again after a short delay
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcTimeout	= Get_Random_Time_Offset(MP_CONTACT_MISSION_SHORT_PLAYER_DELAY_msec)
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]: NO PREVIOUSLY PLAYED CMs TO OFFER: Try again after short delay.")
			NET_NL()
			Debug_Output_Active_CMs_Array()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Found a mission, so store the details as a Contact Mission
	Store_Contact_Mission_Based_On_Contact_Mission_Slot(thisCMArrayPos, TRUE)
	
	// Ask the server if it is okay to send an invite for this mission
	Set_My_Contact_Mission_Stage(CONTACT_MISSION_CLIENT_STAGE_REQUEST_TO_SEND_INVITE)
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Try to setup a 'forced at rank' mission
//
// INPUT PARAMS:		paramArrayPosCM			Array position on the Contact Missions array
PROC Try_To_Setup_Unplayed_Forced_At_Rank_Mission(INT paramArrayPosCM)

	// Delay if this was the contact for the most recent CM
	g_eFMHeistContactIDs theHeistContact = Get_Contact_Mission_ContactID_For_Slot(paramArrayPosCM)
	
	IF (Is_This_The_Most_Recent_CM_Contact(theHeistContact))
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcTimeout	= Get_Random_Time_Offset(MP_CONTACT_MISSION_SHORT_PLAYER_DELAY_msec)
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]: DON'T OFFER 'FORCED AT RANK' CM: Contact was the contact for the most recently played CM")
			NET_NL()
			Debug_Output_Active_CMs_Array()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Delay if the contact hasn't yet made their unlock phonecall
	enumCharacterList theContactID = Get_Contact_Mission_ContactID_As_CharacterList_For_Slot(paramArrayPosCM)
	IF NOT (HAS_PLAYER_UNLOCKED_CONTACT(PLAYER_ID(), theContactID))
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcTimeout	= Get_Random_Time_Offset(MP_CONTACT_MISSION_SHORT_PLAYER_DELAY_msec)
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]: DON'T OFFER 'FORCED AT RANK' CM: Player hasn't yet received intro phonecall from character: ")
			NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sCharacterSheetAll[theContactID].label))
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Delay if the mission is closed or closing
	INT theOnHours = g_sLocalMPCMs[paramArrayPosCM].lcmOnHours
	IF NOT (Can_Invite_Be_Displayed_At_This_Hour(theOnHours))
	OR NOT (Will_Invite_Be_Ok_To_Display_Next_Hour(theOnHours))
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcTimeout	= Get_Random_Time_Offset(MP_CONTACT_MISSION_SHORT_PLAYER_DELAY_msec)
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]: DON'T OFFER 'FORCED AT RANK' CM: Mission is closed or about to close: ")
			NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sCharacterSheetAll[theContactID].label))
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// KGM 24/8/13: Hardcoded Delay - Has Gerald unlocked 'Meth'd Up'
	// TO REMOVE AS HARDCODED: Need a way to tie up a mission with an FMMC_TYPE being unlocked, or something
	IF (theContactID = CHAR_MP_GERALD)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcTimeout	= Get_Random_Time_Offset(MP_CONTACT_MISSION_SHORT_PLAYER_DELAY_msec)
		
		IF NOT (IS_FM_TYPE_UNLOCKED(FMMC_TYPE_GERALD_SANDY_SHORES))
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [ActSelect][ContactMission]: DON'T OFFER 'FORCED AT RANK' CM: HARDCODED RESTRICTION: Gerald hasn't sent 'Meth'd Up' txtmsg")
				NET_NL()
			#ENDIF
			
			EXIT
		ENDIF
	ENDIF
	
	// Clear the Active CM array (but leave the mission setup and the invite active - neither of these will get setup again after a transition but let the player still choose them for now)
	INT tempLoop = 0
	
	REPEAT MAX_CONTACT_MISSION_INVITES_AT_ONCE tempLoop
		g_lcacmActiveCMs[tempLoop].lcacmContact	= NO_FM_HEIST_CONTACT
		Clear_MP_MISSION_ID_DATA_Struct(g_lcacmActiveCMs[tempLoop].lcacmMissionIdData)
		g_lcacmActiveCMs[tempLoop].lcacmPreviouslyCompleted	= FALSE
		g_lcacmActiveCMs[tempLoop].lcacmForcedAtRank		= FALSE
	ENDREPEAT
		
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: About to setup 'FORCED AT RANK' CM - active CM array has been cleared (but invite and mission still remain for now)")
		NET_NL()
		Debug_Output_Active_CMs_Array()
	#ENDIF
	
	// Store the details as a Contact Mission
	Store_Contact_Mission_Based_On_Contact_Mission_Slot(paramArrayPosCM, FALSE)
	
	// Ask the server if it is okay to send an invite for this mission
	Set_My_Contact_Mission_Stage(CONTACT_MISSION_CLIENT_STAGE_REQUEST_TO_SEND_INVITE)
	
ENDPROC

/// PURPOSE:
///    Gets the next invite type for the contact mission selector.
FUNC INT Get_Next_Selector_Contact_Invite_Type()
	
	INT forcedAtRankArrayPos = Get_ArrayPos_Of_Unplayed_Forced_At_Rank_Mission_Available_To_Player()
	IF (forcedAtRankArrayPos != ILLEGAL_CONTACT_MISSION_SLOT)
		PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary] Get_Next_Selector_Contact_Invite_Type - Selected invite type: SELECTOR_NEXT_INVITE_IS_CONTACT_MISSION because there is a forced at rank Contact Mission: Slot: ", forcedAtRankArrayPos)
		RETURN SELECTOR_NEXT_INVITE_IS_CONTACT_MISSION
	ENDIF
	
	NETWORK_SEED_RANDOM_NUMBER_GENERATOR(GET_CLOUD_TIME_AS_INT()/g_sMPTunables.iNjvsSyncWindow)
	INT inviteTypeNo = NETWORK_GET_RANDOM_INT_RANGED(0, 100)
	
	IF (inviteTypeNo < g_sMPTunables.iPercentage_Adversary_Invite_Is_Sent)
		PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary] Get_Next_Selector_Contact_Invite_Type - Selected invite type: SELECTOR_NEXT_INVITE_IS_ADVERSARY (Tunable = ", g_sMPTunables.iPercentage_Adversary_Invite_Is_Sent, ". Random No = ", inviteTypeNo, ")")
		RETURN SELECTOR_NEXT_INVITE_IS_ADVERSARY
	ENDIF
	
	PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary] Get_Next_Selector_Contact_Invite_Type - Selected invite type: SELECTOR_NEXT_INVITE_IS_CONTACT_MISSION (Tunable = ", g_sMPTunables.iPercentage_Adversary_Invite_Is_Sent, ". Random No = ", inviteTypeNo, ")")
	RETURN SELECTOR_NEXT_INVITE_IS_CONTACT_MISSION
ENDFUNC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Returns a valud Id type that is used within Bobby's system based on the adversary type passed
FUNC STRING Get_Contact_Mission_Adversary_Debug_Name(g_eContactMissionAdversaryTypes adversaryType)
	
	SWITCH adversaryType
		CASE CM_ADVERSARY_TYPE_COME_OUT_TO_PLAY			RETURN "CM_ADVERSARY_TYPE_COME_OUT_TO_PLAY"
		CASE CM_ADVERSARY_TYPE_SIEGE_MENTALITY			RETURN "CM_ADVERSARY_TYPE_SIEGE_MENTALITY"
		CASE CM_ADVERSARY_TYPE_HASTA_LA_VISTA			RETURN "CM_ADVERSARY_TYPE_HASTA_LA_VISTA"
		CASE CM_ADVERSARY_TYPE_HUNTING_PACK				RETURN "CM_ADVERSARY_TYPE_HUNTING_PACK"
		CASE CM_ADVERSARY_TYPE_CROSS_THE_LINE			RETURN "CM_ADVERSARY_TYPE_CROSS_THE_LINE"
		CASE CM_ADVERSARY_TYPE_SHEPHERD					RETURN "CM_ADVERSARY_TYPE_SHEPHERD"
		CASE CM_ADVERSARY_TYPE_RELAY					RETURN "CM_ADVERSARY_TYPE_RELAY"
		CASE CM_ADVERSARY_TYPE_SPEED_RACE				RETURN "CM_ADVERSARY_TYPE_SPEED_RACE"
		CASE CM_ADVERSARY_TYPE_EBC						RETURN "CM_ADVERSARY_TYPE_EBC"
		CASE CM_ADVERSARY_TYPE_HUNT_DARK				RETURN "CM_ADVERSARY_TYPE_HUNT_DARK"
		CASE CM_ADVERSARY_TYPE_RUNNING_BACK				RETURN "CM_ADVERSARY_TYPE_RUNNING_BACK"
		CASE CM_ADVERSARY_TYPE_EXTRACTION				RETURN "CM_ADVERSARY_TYPE_EXTRACTION"
		CASE CM_ADVERSARY_TYPE_BEAST_V_SLASHER			RETURN "CM_ADVERSARY_TYPE_BEAST_V_SLASHER"
		CASE CM_ADVERSARY_TYPE_DROP_ZONE				RETURN "CM_ADVERSARY_TYPE_DROP_ZONE"
		CASE CM_ADVERSARY_TYPE_BE_MY_VALENTINE			RETURN "CM_ADVERSARY_TYPE_BE_MY_VALENTIN"
		CASE CM_ADVERSARY_TYPE_DAVID_AND_GOLIATH		RETURN "CM_ADVERSARY_TYPE_DAVID_AND_GOLIATH"
		CASE CM_ADVERSARY_TYPE_SUMO						RETURN "CM_ADVERSARY_TYPE_SUMO"		
		CASE CM_ADVERSARY_TYPE_RUGBY					RETURN "CM_ADVERSARY_TYPE_RUGBY"	
		CASE CM_ADVERSARY_TYPE_IN_AND_OUT				RETURN "CM_ADVERSARY_TYPE_IN_AND_OUT"
		CASE CM_ADVERSARY_TYPE_POWER_PLAY				RETURN "CM_ADVERSARY_TYPE_POWER_PLAY"
		CASE CM_ADVERSARY_TYPE_TRADE_PLACES				RETURN "CM_ADVERSARY_TYPE_TRADE_PLACES"
		CASE CM_ADVERSARY_TYPE_DEADLINE					RETURN "CM_ADVERSARY_TYPE_DEADLINE"
		CASE CM_ADVERSARY_TYPE_LOSTVDAMNED				RETURN "CM_ADVERSARY_TYPE_LOSTVDAMNED"
		CASE CM_ADVERSARY_TYPE_SLIPSTREAM				RETURN "CM_ADVERSARY_TYPE_SLIPSTREAM"
		CASE CM_ADVERSARY_TYPE_KILL_QUOTA				RETURN "CM_ADVERSARY_TYPE_KILL_QUOTA"
	ENDSWITCH
	
	RETURN "UNKNOWN"
ENDFUNC

#ENDIF

/// PURPOSE:
///    Returns a valud Id type that is used within Bobby's system based on the adversary type passed
FUNC INT Get_Contact_Mission_Adversary_Id_From_Type(g_eContactMissionAdversaryTypes adversaryType)
	
	SWITCH adversaryType
		CASE CM_ADVERSARY_TYPE_COME_OUT_TO_PLAY			RETURN ciROCKSTAR_CREATED_NEW_VS_MISSION_ComeOutToPlay
		CASE CM_ADVERSARY_TYPE_SIEGE_MENTALITY			RETURN ciROCKSTAR_CREATED_NEW_VS_MISSION_SiegeMentality
		CASE CM_ADVERSARY_TYPE_HASTA_LA_VISTA			RETURN ciROCKSTAR_CREATED_NEW_VS_MISSION_HastaLaVista
		CASE CM_ADVERSARY_TYPE_HUNTING_PACK				RETURN ciROCKSTAR_CREATED_NEW_VS_MISSION_HuntingPack
		CASE CM_ADVERSARY_TYPE_CROSS_THE_LINE			RETURN ciROCKSTAR_CREATED_NEW_VS_MISSION_CrossTheLine
		CASE CM_ADVERSARY_TYPE_SHEPHERD					RETURN ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SHEPHERD
		CASE CM_ADVERSARY_TYPE_RELAY					RETURN ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RELAY
		CASE CM_ADVERSARY_TYPE_SPEED_RACE				RETURN ciROCKSTAR_CREATED_2_NEW_VS_MISSION_SPEEDRACE
		CASE CM_ADVERSARY_TYPE_EBC						RETURN ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EBC
		CASE CM_ADVERSARY_TYPE_HUNT_DARK				RETURN ciROCKSTAR_CREATED_2_NEW_VS_MISSION_HUNTDARK
		CASE CM_ADVERSARY_TYPE_RUNNING_BACK				RETURN ciROCKSTAR_CREATED_2_NEW_VS_MISSION_RUNNINGBACK
		CASE CM_ADVERSARY_TYPE_EXTRACTION				RETURN ciROCKSTAR_CREATED_2_NEW_VS_MISSION_EXTRACTION
		CASE CM_ADVERSARY_TYPE_BEAST_V_SLASHER			RETURN ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BVS1
		CASE CM_ADVERSARY_TYPE_DROP_ZONE				RETURN ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DROP_ZONE
		CASE CM_ADVERSARY_TYPE_BE_MY_VALENTINE			RETURN ciROCKSTAR_CREATED_2_NEW_VS_MISSION_BE_MY_VALENTIN
		CASE CM_ADVERSARY_TYPE_DAVID_AND_GOLIATH		RETURN ciROCKSTAR_CREATED_2_NEW_VS_MISSION_DAVID_AND_GOLIATH
	ENDSWITCH
	
	RETURN ILLEGAL_AT_COORDS_ID
ENDFUNC

/// PURPOSE:
///    Returns the relevant tunable value for the Adversary Mode Reminders
FUNC INT Get_Contact_Mission_Adversary_Reminder_Tunable(g_eContactMissionAdversaryTypes adversaryType)
	
	
	
	SWITCH adversaryType
		CASE CM_ADVERSARY_TYPE_COME_OUT_TO_PLAY			RETURN g_sMPTunables.iTURN_ON_COME_OUT_TO_PLAY_NUMBER_OF_OCCURRENCES
		CASE CM_ADVERSARY_TYPE_SIEGE_MENTALITY			RETURN g_sMPTunables.iTURN_ON_SEIGE_MENTALITY_NUMBER_OF_OCCURRENCES
		CASE CM_ADVERSARY_TYPE_HASTA_LA_VISTA			RETURN g_sMPTunables.iTURN_ON_HASTA_LA_VISTA_NUMBER_OF_OCCURRENCES
		CASE CM_ADVERSARY_TYPE_HUNTING_PACK				RETURN g_sMPTunables.iTURN_ON_HUNTING_PACK_NUMBER_OF_OCCURRENCES
		CASE CM_ADVERSARY_TYPE_CROSS_THE_LINE			RETURN g_sMPTunables.iTURN_ON_CROSS_THE_LINE_NUMBER_OF_OCCURRENCES
		CASE CM_ADVERSARY_TYPE_SHEPHERD					RETURN g_sMPTunables.iTURN_ON_OFFENSE_DEFENSE_NUMBER_OF_OCCURRENCES
		CASE CM_ADVERSARY_TYPE_RELAY					RETURN g_sMPTunables.iTURN_ON_RELAY_NUMBER_OF_OCCURRENCES
		CASE CM_ADVERSARY_TYPE_SPEED_RACE				RETURN g_sMPTunables.iTURN_ON_KEEP_THE_PACE_NUMBER_OF_OCCURRENCES
		CASE CM_ADVERSARY_TYPE_EBC						RETURN g_sMPTunables.iTURN_ON_EVERY_BULLET_COUNTS_NUMBER_OF_OCCURRENCES
		CASE CM_ADVERSARY_TYPE_HUNT_DARK				RETURN g_sMPTunables.iTURN_ON_SLASHER_NUMBER_OF_OCCURRENCES
		CASE CM_ADVERSARY_TYPE_RUNNING_BACK				RETURN g_sMPTunables.iTURN_ON_RUNNING_BACK_NUMBER_OF_OCCURRENCES
		CASE CM_ADVERSARY_TYPE_EXTRACTION				RETURN g_sMPTunables.iTURN_ON_COME_EXTRACTION_NUMBER_OF_OCCURRENCES
		CASE CM_ADVERSARY_TYPE_BEAST_V_SLASHER			RETURN g_sMPTunables.iTURN_ON_COME_BEASTVSSLASHER_NUMBER_OF_OCCURRENCES
		CASE CM_ADVERSARY_TYPE_DROP_ZONE				RETURN g_sMPTunables.iTURN_ON_COME_DROPZONE_NUMBER_OF_OCCURRENCES
		CASE CM_ADVERSARY_TYPE_BE_MY_VALENTINE			RETURN g_sMPTunables.iTURN_ON_BE_MY_VALENTINE_NUMBER_OF_OCCURRENCES
		CASE CM_ADVERSARY_TYPE_DAVID_AND_GOLIATH		RETURN g_sMPTunables.idavids_vs_goliath_number_of_occurrences
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Returns the relevant tunable value for the Adversary Mode Reminders
FUNC MP_INT_STATS Get_Contact_Mission_Adversary_Reminder_Stat(g_eContactMissionAdversaryTypes adversaryType)
	
	SWITCH adversaryType
		CASE CM_ADVERSARY_TYPE_COME_OUT_TO_PLAY			RETURN MP_STAT_HELPCOMEOUTTOPLAY
		CASE CM_ADVERSARY_TYPE_SIEGE_MENTALITY			RETURN MP_STAT_HELPSEIGEMENTALITY
		CASE CM_ADVERSARY_TYPE_HASTA_LA_VISTA			RETURN MP_STAT_HELPHASTALAVISTA
		CASE CM_ADVERSARY_TYPE_HUNTING_PACK				RETURN MP_STAT_HELPHUNTINGPACK
		CASE CM_ADVERSARY_TYPE_CROSS_THE_LINE			RETURN MP_STAT_HELPCROSSTHELINE
		CASE CM_ADVERSARY_TYPE_SHEPHERD					RETURN MP_STAT_HELPOFFENSEDEFENSE
		CASE CM_ADVERSARY_TYPE_RELAY					RETURN MP_STAT_HELPRELAY
		CASE CM_ADVERSARY_TYPE_SPEED_RACE				RETURN MP_STAT_HELPKEEPTHEPACE
		CASE CM_ADVERSARY_TYPE_EBC						RETURN MP_STAT_HELPEVERYBULLETCOUNTS
		CASE CM_ADVERSARY_TYPE_HUNT_DARK				RETURN MP_STAT_HELPSLASHER
		CASE CM_ADVERSARY_TYPE_RUNNING_BACK				RETURN MP_STAT_HELPRUNNINGBACK
		CASE CM_ADVERSARY_TYPE_EXTRACTION				RETURN MP_STAT_HELPEXTRACTION
		CASE CM_ADVERSARY_TYPE_BEAST_V_SLASHER			RETURN MP_STAT_HELPBEASTVSSLASHER
		CASE CM_ADVERSARY_TYPE_DROP_ZONE				RETURN MP_STAT_HELPDROPZONE
		CASE CM_ADVERSARY_TYPE_BE_MY_VALENTINE			RETURN MP_STAT_HELPBEMYVALEN
		CASE CM_ADVERSARY_TYPE_DAVID_AND_GOLIATH		RETURN MP_STAT_HELPDAVE_GOLIATH
	ENDSWITCH
	
	PRINTLN("Get_Contact_Mission_Adversary_Reminder_Stat - Requesting a STAT that does not exist for Adversary Mode Reminder: ", ENUM_TO_INT(adversaryType))
	SCRIPT_ASSERT("Get_Contact_Mission_Adversary_Reminder_Stat - Requesting a STAT that does not exist for Adversary Mode Reminder")
	RETURN INT_TO_ENUM(MP_INT_STATS, -1)
ENDFUNC


/// PURPOSE:
///    Returns the relevant name for the Adversary Mode Reminders
FUNC STRING Get_Contact_Mission_Adversary_Reminder_Name(g_eContactMissionAdversaryTypes adversaryType)
	
	SWITCH adversaryType
		CASE CM_ADVERSARY_TYPE_COME_OUT_TO_PLAY			RETURN "ADV_COP"
		CASE CM_ADVERSARY_TYPE_SIEGE_MENTALITY			RETURN "ADV_SEI"
		CASE CM_ADVERSARY_TYPE_HASTA_LA_VISTA			RETURN "ADV_HLV"
		CASE CM_ADVERSARY_TYPE_HUNTING_PACK				RETURN "ADV_HPK"
		CASE CM_ADVERSARY_TYPE_CROSS_THE_LINE			RETURN "ADV_CTL"
		CASE CM_ADVERSARY_TYPE_SHEPHERD					RETURN "ADV_OFF"
		CASE CM_ADVERSARY_TYPE_RELAY					RETURN "ADV_REL"
		CASE CM_ADVERSARY_TYPE_SPEED_RACE				RETURN "ADV_KTP"
		CASE CM_ADVERSARY_TYPE_EBC						RETURN "ADV_EBC"
		CASE CM_ADVERSARY_TYPE_HUNT_DARK				RETURN "ADV_SLA"
		CASE CM_ADVERSARY_TYPE_RUNNING_BACK				RETURN "ADV_RUN"
		CASE CM_ADVERSARY_TYPE_EXTRACTION				RETURN "ADV_EXT"
		CASE CM_ADVERSARY_TYPE_BEAST_V_SLASHER			RETURN "ADV_BVS"
		CASE CM_ADVERSARY_TYPE_DROP_ZONE				RETURN "ADV_DRO"
		CASE CM_ADVERSARY_TYPE_BE_MY_VALENTINE			RETURN "ADV_BMV"
		CASE CM_ADVERSARY_TYPE_DAVID_AND_GOLIATH		RETURN "ADV_DVG"
		CASE CM_ADVERSARY_TYPE_SUMO						RETURN "ADV_SUMO"
		CASE CM_ADVERSARY_TYPE_RUGBY					RETURN "ADV_IBI"	
		CASE CM_ADVERSARY_TYPE_IN_AND_OUT				RETURN "ADV_INANDOUT"
		CASE CM_ADVERSARY_TYPE_POWER_PLAY				RETURN "ADV_POWER"
		CASE CM_ADVERSARY_TYPE_TRADE_PLACES				RETURN "ADV_TRADE"
		CASE CM_ADVERSARY_TYPE_ENTOURAGE				RETURN "ADV_ENTOURAGE"
		CASE CM_ADVERSARY_TYPE_DEADLINE	    			
			IF g_sMPTunables.bTURN_ON_DEADLINER_JOB
				RETURN ""
			ENDIF
			
			RETURN "ADV_DEADL"
		BREAK
		CASE CM_ADVERSARY_TYPE_LOSTVDAMNED  			RETURN "ADV_LVD"
		CASE CM_ADVERSARY_TYPE_SLIPSTREAM   			RETURN "ADV_SLIPS"
		CASE CM_ADVERSARY_TYPE_KILL_QUOTA   			RETURN "ADV_KQUOTA"
		CASE CM_ADVERSARY_TYPE_TURF_WAR  				RETURN "ADV_TW"
		CASE CM_ADVERSARY_TYPE_VEHICLE_VENDETTA   		RETURN "ADV_VEHVEN"
		CASE CM_ADVERSARY_TYPE_POINTLESS   				RETURN "ADV_PNTLS"
		CASE CM_ADVERSARY_TYPE_JUGGERNAUT   			RETURN "ADV_JUG"
		CASE CM_ADVERSARY_TYPE_RESURRECTION				RETURN "ADV_RESUR"
		CASE CM_ADVERSARY_TYPE_LANDGRAB					RETURN "VS_SR_LG"
		CASE CM_ADVERSARY_TYPE_TINY_RACERS				RETURN "ADV_TRACERS"
		CASE CM_ADVERSARY_TYPE_OVERTIME					RETURN "ADV_OVRT"
		CASE CM_ADVERSARY_TYPE_DAWN_RAID				RETURN "VS_DAWNRAID"
		CASE CM_ADVERSARY_TYPE_POWER_MAD				RETURN "ADV_POWM"
		CASE CM_ADVERSARY_TYPE_OVERTIME_RUMBLE			RETURN "ADV_OVRR"
		CASE CM_ADVERSARY_TYPE_VEHICLE_WARFARE				RETURN "ADVPROMO_MOTORW"
		CASE CM_ADVERSARY_TYPE_BOMBUSHKA_RUN				RETURN "ADVPROMO_BOMBUS"
		CASE CM_ADVERSARY_TYPE_STOCKPILE					RETURN "ADVPROMO_STOCKP"
		CASE CM_ADVERSARY_TYPE_CONDEMNED					RETURN "ADVPROMO_CONDEM"
		CASE CM_ADVERSARY_TYPE_AIR_SHOOTOUT					RETURN "ADVPROMO_DOGFIG"
		CASE CM_ADVERSARY_TYPE_HARD_TARGET					RETURN "ADV_HRDTRG"
		CASE CM_ADVERSARY_TYPE_SLASHERS						RETURN "ADV_SLSRS"
		CASE CM_ADVERSARY_TYPE_HOSTILE_TAKEOVER				RETURN "ADV_HTO"
		CASE CM_ADVERSARY_TYPE_AIR_QUOTA					RETURN "ADV_AQTA"
		CASE CM_ADVERSARY_TYPE_VENETIAN_JOB					RETURN "ADV_PR_VJOB"
		CASE CM_ADVERSARY_TYPE_TRAP_DOOR					RETURN "ADV_PR_TDOR"
		CASE CM_ADVERSARY_TYPE_SUMO_REMIX					RETURN "ADV_PR_SUMORUN"
		CASE CM_ADVERSARY_TYPE_HUNTING_PACK_REMIX			RETURN "ADV_PR_STNPKC"
		CASE CM_ADVERSARY_TYPE_TRADING_PLACES_REMIX			RETURN "ADV_PR_TPR"
		CASE CM_ADVERSARY_TYPE_RUNNING_BACK_REMIX			RETURN "ADV_PR_RUNBR"
	ENDSWITCH
	
	PRINTLN("Get_Contact_Mission_Adversary_Reminder_Name - Requesting a Name for an adversary mode that does not exist: ", ENUM_TO_INT(adversaryType))
	SCRIPT_ASSERT("Get_Contact_Mission_Adversary_Reminder_Name - Requesting a Name for an adversary mode that does not exist")
	RETURN ""
ENDFUNC

FUNC BOOL Is_Adversary_Reminder_Tunable_Requesting_A_Reset(g_eContactMissionAdversaryTypes adversaryType)
	
	INT iTunable = Get_Contact_Mission_Adversary_Reminder_Tunable(adversaryType)
	MP_INT_STATS adversaryStat = Get_Contact_Mission_Adversary_Reminder_Stat(adversaryType)
	
	// Invalid stat, exit
	IF adversaryStat = INT_TO_ENUM(MP_INT_STATS, -1)
		RETURN FALSE
	ENDIF
	
	INT iLocalStat = GET_MP_INT_CHARACTER_STAT(adversaryStat)
	
	INT iBit
	// The below loop is over the last 4 bits of the above ints. These bits act as RESET flags:
	//	If bit 28 is set and locally it isn't, we flatten our stats and start afresh with whatever the tunable value is.
	//	NOTE: Fionn / James should have knowledge of the state of these values so we progressively set these reset flags until we have used the stat 4times.
	FOR iBit = 28 TO 31 STEP 1
		
		// If the tunable is indicating a reset, have we locally done this?
		IF IS_BIT_SET(iTunable, iBit)
		AND NOT IS_BIT_SET(iLocalStat, iBit)
			
			//...no, return TRUE
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Get the rank that the adversary type is unlocked at
FUNC INT Get_Contact_Mission_Adversary_Type_Rank(g_eContactMissionAdversaryTypes adversaryType)
	
	SWITCH adversaryType
		CASE CM_ADVERSARY_TYPE_COME_OUT_TO_PLAY			
		CASE CM_ADVERSARY_TYPE_SIEGE_MENTALITY			
		CASE CM_ADVERSARY_TYPE_HASTA_LA_VISTA			
		CASE CM_ADVERSARY_TYPE_HUNTING_PACK				
		CASE CM_ADVERSARY_TYPE_CROSS_THE_LINE
			RETURN g_sMPTunables.iOldAdModeInvitesRank
		CASE CM_ADVERSARY_TYPE_SHEPHERD					
		CASE CM_ADVERSARY_TYPE_RELAY					
		CASE CM_ADVERSARY_TYPE_SPEED_RACE				
		CASE CM_ADVERSARY_TYPE_EBC						
		CASE CM_ADVERSARY_TYPE_HUNT_DARK				
		CASE CM_ADVERSARY_TYPE_RUNNING_BACK
		CASE CM_ADVERSARY_TYPE_EXTRACTION
		CASE CM_ADVERSARY_TYPE_BEAST_V_SLASHER
		CASE CM_ADVERSARY_TYPE_DROP_ZONE
		CASE CM_ADVERSARY_TYPE_BE_MY_VALENTINE
		CASE CM_ADVERSARY_TYPE_DAVID_AND_GOLIATH
			RETURN g_sMPTunables.iAdModeInvitesRank
	ENDSWITCH
	
	RETURN 1
ENDFUNC

/// PURPOSE:
///    Check if the adversary type is valid and can be sent to the local player
FUNC BOOL Is_Contact_Mission_Adversary_Type_Active(g_eContactMissionAdversaryTypes adversaryType)

	// JA: 2559405 - Do not send Adversary invites until the invite is complete
	IF NOT HAS_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS(PLAYER_ID())
		RETURN FALSE
	ENDIF

	INT iPlayerRank = GET_PLAYER_RANK(PLAYER_ID())
	INT iAdversaryRank = Get_Contact_Mission_Adversary_Type_Rank(adversaryType)

	SWITCH adversaryType
		CASE CM_ADVERSARY_TYPE_COME_OUT_TO_PLAY
		CASE CM_ADVERSARY_TYPE_SIEGE_MENTALITY
		CASE CM_ADVERSARY_TYPE_HASTA_LA_VISTA
		CASE CM_ADVERSARY_TYPE_HUNTING_PACK
		CASE CM_ADVERSARY_TYPE_CROSS_THE_LINE
		CASE CM_ADVERSARY_TYPE_SHEPHERD
		CASE CM_ADVERSARY_TYPE_RELAY
		CASE CM_ADVERSARY_TYPE_SPEED_RACE
		CASE CM_ADVERSARY_TYPE_BE_MY_VALENTINE
		CASE CM_ADVERSARY_TYPE_DAVID_AND_GOLIATH
			IF iPlayerRank >= iAdversaryRank
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE CM_ADVERSARY_TYPE_HUNT_DARK
		CASE CM_ADVERSARY_TYPE_EBC
		CASE CM_ADVERSARY_TYPE_RUNNING_BACK
		CASE CM_ADVERSARY_TYPE_EXTRACTION
		CASE CM_ADVERSARY_TYPE_BEAST_V_SLASHER
		CASE CM_ADVERSARY_TYPE_DROP_ZONE
			// Only if permitted by Bobby
			IF LOW_FLOW_SHOULD_THIS_ADVERSORY_MODE_INVITE_BE_SENT(Get_Contact_Mission_Adversary_Id_From_Type(adversaryType))
			AND iPlayerRank >= iAdversaryRank
				RETURN TRUE
			ENDIF
		BREAK		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Allows the selector to get the current weighting for each adversary mode
/// PARAMS:
///    adversaryType - The mode being queried
FUNC FLOAT Get_Contact_Mission_Adversary_Type_Weighting(g_eContactMissionAdversaryTypes adversaryType)
	
	SWITCH adversaryType
		CASE CM_ADVERSARY_TYPE_COME_OUT_TO_PLAY			RETURN g_sMPTunables.fCome_Out_To_Play_Invite_Weight
		CASE CM_ADVERSARY_TYPE_SIEGE_MENTALITY			RETURN g_sMPTunables.fSiege_Mentality_Invite_Weight
		CASE CM_ADVERSARY_TYPE_HASTA_LA_VISTA			RETURN g_sMPTunables.fHasta_La_Vista_Invite_Weight
		CASE CM_ADVERSARY_TYPE_HUNTING_PACK				RETURN g_sMPTunables.fHunting_Pack_Invite_Weight
		CASE CM_ADVERSARY_TYPE_CROSS_THE_LINE			RETURN g_sMPTunables.fCross_The_Line_Invite_Weight
		CASE CM_ADVERSARY_TYPE_SHEPHERD					RETURN g_sMPTunables.fShepherd_Invite_Weight
		CASE CM_ADVERSARY_TYPE_RELAY					RETURN g_sMPTunables.fRelay_Invite_Weight
		CASE CM_ADVERSARY_TYPE_SPEED_RACE				RETURN g_sMPTunables.fSpeed_Race_Invite_Weight
		CASE CM_ADVERSARY_TYPE_EBC						RETURN g_sMPTunables.fEBC_Invite_Weight
		CASE CM_ADVERSARY_TYPE_HUNT_DARK				RETURN g_sMPTunables.fHunt_Dark_Invite_Weight
		CASE CM_ADVERSARY_TYPE_RUNNING_BACK				RETURN g_sMPTunables.fRunning_Back_Invite_Weight
		CASE CM_ADVERSARY_TYPE_EXTRACTION				RETURN g_sMPTunables.fextraction_invite_weight
		CASE CM_ADVERSARY_TYPE_BEAST_V_SLASHER			RETURN g_sMPTunables.fbeastvslasher_invite_weight
		CASE CM_ADVERSARY_TYPE_DROP_ZONE				RETURN g_sMPTunables.fdropzone_invite_weight
		CASE CM_ADVERSARY_TYPE_BE_MY_VALENTINE			RETURN g_sMPTunables.fBE_MY_VALENTINE_INVITE_WEIGHT
		CASE CM_ADVERSARY_TYPE_DAVID_AND_GOLIATH		RETURN g_sMPTunables.fdavids_vs_goliath_invite_weight
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

///// PURPOSE:
/////    Returns TRUE if the adversary mode has a weighting which is adjusted during a specific timed event
//FUNC BOOL Is_Contact_Mission_Adversary_Type_Weighting_Time_Flexible(g_eContactMissionAdversaryTypes adversaryType)
//	SWITCH adversaryType
//		CASE CM_ADVERSARY_TYPE_HUNT_DARK
//			IF g_sMPTunables.bTURN_ON_HALLOWEEN_JOBS
//				RETURN TRUE
//			ENDIF
//		BREAK
//		CASE CM_ADVERSARY_TYPE_EBC						
//		CASE CM_ADVERSARY_TYPE_RUNNING_BACK
//			IF g_sMPTunables.bTURN_ON_TH
//				RETURN TRUE
//			ENDIF
//		BREAK
//		CASE CM_ADVERSARY_TYPE_EXTRACTION
//			IF g_sMPTunables.bturn_on_extraction
//			OR g_sMPTunables.bturn_on_extraction_dropzone
//				RETURN TRUE
//			ENDIF
//		BREAK
//		CASE CM_ADVERSARY_TYPE_BEAST_V_SLASHER
//			IF g_sMPTunables.bturn_on_beastvslasher
//				RETURN TRUE
//			ENDIF
//		BREAK
//		CASE CM_ADVERSARY_TYPE_DROP_ZONE
//			IF g_sMPTunables.bturn_on_dropzone
//			OR g_sMPTunables.bturn_on_extraction_dropzone
//				RETURN TRUE
//			ENDIF
//		BREAK
//	ENDSWITCH
//
//	RETURN FALSE
//ENDFUNC

/// PURPOSE:
///    Get an adjusted weighting if the adversary mode is event specific
FUNC FLOAT Get_Contact_Mission_Adversary_Type_Weighting_Time_Flexible(g_eContactMissionAdversaryTypes adversaryType, FLOAT totalWeighting, FLOAT overriddenWeighting)
	SWITCH adversaryType
		CASE CM_ADVERSARY_TYPE_HUNT_DARK
			IF g_sMPTunables.bTURN_ON_HALLOWEEN_JOBS
				RETURN (totalWeighting)						// For Halloween, this mission is 50% weighting when event is active
			ENDIF
		BREAK
		
		CASE CM_ADVERSARY_TYPE_EBC			
		CASE CM_ADVERSARY_TYPE_RUNNING_BACK
			IF g_sMPTunables.bTURN_ON_TH
				IF overriddenWeighting != -1
					RETURN (overriddenWeighting)
				ELSE
					RETURN (totalWeighting / 2)					// For Thankgiving, this mission is 25% weighting when event is active
				ENDIF
			ENDIF
		BREAK
		CASE CM_ADVERSARY_TYPE_EXTRACTION
			IF g_sMPTunables.bturn_on_extraction
				RETURN (totalWeighting)						// When extraction is active, the weighting is 50%
			ENDIF
			IF g_sMPTunables.bturn_on_extraction_dropzone
				IF overriddenWeighting != -1
					RETURN (overriddenWeighting)
				ELSE
					RETURN (totalWeighting / 2)					// If both modes are active, return 25%
				ENDIF
			ENDIF
		BREAK
		CASE CM_ADVERSARY_TYPE_BEAST_V_SLASHER
			IF g_sMPTunables.bturn_on_beastvslasher
				RETURN (totalWeighting)						// When Beast v Slasher is active, the weighting is 50%
			ENDIF
		BREAK
		CASE CM_ADVERSARY_TYPE_DROP_ZONE
			IF g_sMPTunables.bturn_on_dropzone
				RETURN (totalWeighting)						// When Drop Zone is active, the weighting is 50%
			ENDIF
			IF g_sMPTunables.bturn_on_extraction_dropzone
				IF overriddenWeighting != -1
					RETURN (overriddenWeighting)
				ELSE
					RETURN (totalWeighting / 2)					// If both modes are active, return 25%
				ENDIF
			ENDIF
		BREAK
			
	ENDSWITCH

	PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary] Unknwon / Invalid adversary mode for timed event weighting: ", Get_Contact_Mission_Adversary_Debug_Name(adversaryType))
	SCRIPT_ASSERT("...KGM MP [ActSelect][ContactMission][Adversary] Unknwon adversary mode for timed event weighting")
	RETURN 0.0
ENDFUNC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Returns TRUE if there is an available Adversary mode to the player with current weighting and game progress
FUNC BOOL Are_There_Any_Valid_Adversary_Modes_Available()
	
	FLOAT adversaryWeighting
	//FLOAT totalWeighting
	//FLOAT overriddenWeighting = -1
	g_eContactMissionAdversaryTypes adversaryType
	
		
	INT loop
	// Loop over each of our adversary modes
	REPEAT CM_ADVERSARY_TYPE_MAX loop
	
		adversaryType = INT_TO_ENUM(g_eContactMissionAdversaryTypes, loop)
	
		// Is the mode active the player?
		IF Is_Contact_Mission_Adversary_Type_Active(adversaryType)
		
			//...yes, check the weighting is greater than 0.
			adversaryWeighting = Get_Contact_Mission_Adversary_Type_Weighting(adversaryType)
//			
//			IF Is_Contact_Mission_Adversary_Type_Weighting_Time_Flexible(adversaryType)
//				adversaryWeighting = Get_Contact_Mission_Adversary_Type_Weighting_Time_Flexible(adversaryType, totalWeighting, overriddenWeighting)
//				overriddenWeighting = adversaryWeighting
//			ENDIF
			
			IF adversaryWeighting > 0
			
				//...There is an available adversary mode, proceed
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

// PURPOSE:	Look for a suitable Contact Mission to offer to this player
PROC Try_To_Find_Contact_Mission_To_Offer_To_Player()
	#IF IS_DEBUG_BUILD
	IF (GET_COMMANDLINE_PARAM_EXISTS("sc_ForcedRankInviteDebug"))
		CPRINTLN(DEBUG_AMBIENT, "[2703037] [Try_To_Find_Contact_Mission_To_Offer_To_Player] Calling...")
	ENDIF
	#ENDIF

	// Are there any Contact Missions?
	IF (g_numLocalMPCMs = 0)
		#IF IS_DEBUG_BUILD
		IF (GET_COMMANDLINE_PARAM_EXISTS("sc_ForcedRankInviteDebug"))
			CPRINTLN(DEBUG_AMBIENT, "[2703037] [Try_To_Find_Contact_Mission_To_Offer_To_Player] g_numLocalMPCMs = 0")
		ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	// If the invite flow playlist is active...
	IF g_sMPTunables.bNPCFlowInvitePlaylistActive
		// And there's no forced missions to be played, like Meth'd up, then exit and don't send any invites from this system
		INT forcedAtRankArrayPos = Get_ArrayPos_Of_Unplayed_Forced_At_Rank_Mission_Available_To_Player()
		IF (forcedAtRankArrayPos = ILLEGAL_CONTACT_MISSION_SLOT)
			#IF IS_DEBUG_BUILD
			IF (GET_COMMANDLINE_PARAM_EXISTS("sc_ForcedRankInviteDebug"))
				CPRINTLN(DEBUG_AMBIENT, "[2703037] [Try_To_Find_Contact_Mission_To_Offer_To_Player] (forcedAtRankArrayPos = ILLEGAL_CONTACT_MISSION_SLOT)")
			ENDIF
			#ENDIF
			
			EXIT
		ENDIF
	ENDIF	

	// Delay if the player is on a mission
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		// ...there is a focus mission, so try again after a short delay
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcTimeout	= Get_Random_Time_Offset(MP_CONTACT_MISSION_SHORT_PLAYER_DELAY_msec)
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]: DON'T OFFER CM: Player is on a mission")
			NET_NL()
			Debug_Output_Active_CMs_Array()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Delay if the player is spectating
	IF (IS_A_SPECTATOR_CAM_ACTIVE())
		// ...player is spectating, so try again after a short delay
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcTimeout	= Get_Random_Time_Offset(MP_CONTACT_MISSION_SHORT_PLAYER_DELAY_msec)
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]: DON'T OFFER CM: Player is spectating")
			NET_NL()
			Debug_Output_Active_CMs_Array()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Delay if the post mission cleanup is active
    IF (IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID()))
		// ...post mission cleanup, so try again after a short delay
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcTimeout	= Get_Random_Time_Offset(MP_CONTACT_MISSION_SHORT_PLAYER_DELAY_msec)
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]: DON'T OFFER CM: Post Mission Cleanup is ongoing")
			NET_NL()
			Debug_Output_Active_CMs_Array()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Check if there is already a 'forced at rank' mission offered to the player - nothing else should be offered until it is played
	IF (Is_There_A_Forced_At_Rank_Mission_On_Active_CM_Array())
		// ...a 'forced at rank' mission is on the active CM array, so block any other missions from being offered until it has been played
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcTimeout	= Get_CM_Randomised_Regular_Delay_msec()
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]: DON'T OFFER ANOTHER CM: There is an actively offered 'Forced At Rank' CM")
			NET_NL()
			Debug_Output_Active_CMs_Array()
		#ENDIF
		
		EXIT
	ENDIF
	
	
	INT nextSelectorInviteType = Get_Next_Selector_Contact_Invite_Type()
	
	// If there are no Adversary modes available, revert back to a contact mission invite
	IF (nextSelectorInviteType = SELECTOR_NEXT_INVITE_IS_ADVERSARY)
		IF NOT Are_There_Any_Valid_Adversary_Modes_Available()
			PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary]: Adversary invite not valid as there are no modes available to the player")
			nextSelectorInviteType = SELECTOR_NEXT_INVITE_IS_CONTACT_MISSION
		ENDIF
	ENDIF
		
	IF (nextSelectorInviteType = SELECTOR_NEXT_INVITE_IS_ADVERSARY)
		
		// Move our stage to the invite stage for adversary modes.
		Set_My_Contact_Mission_Stage(CONTACT_MISSION_CLIENT_STAGE_SEND_INVITE_ADVERSARY)
	ELSE
		// There isn't a 'forced at rank' mission already offered, so check if there is one currently unplayed
		INT forcedAtRankArrayPos = Get_ArrayPos_Of_Unplayed_Forced_At_Rank_Mission_Available_To_Player()
		IF (forcedAtRankArrayPos != ILLEGAL_CONTACT_MISSION_SLOT)

			// ...there is an unplayed 'forced at rank' mission available to the player, so set it up
			Try_To_Setup_Unplayed_Forced_At_Rank_Mission(forcedAtRankArrayPos)
			
			EXIT
		ENDIF
		
		// Is the player allowed to be offered any more at the current CM Rank?
		IF NOT (Is_Player_Allowed_To_Be_Offered_Another_CM_At_This_Rank())
			// ...not allowed to be offered another CM at this rank, so add a delay before trying again
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcTimeout	= Get_CM_Randomised_Regular_Delay_msec()
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [ActSelect][ContactMission]: DON'T OFFER ANOTHER CM: Player can't have another active CM Invite at this rank: ")
				NET_PRINT_INT(GET_PLAYER_RANK(PLAYER_ID()))
				NET_NL()
				Debug_Output_Active_CMs_Array()
			#ENDIF
			
			EXIT
		ENDIF
		
		// Check if the player is ahead of schedule for receiving new CMs and offer a previously completed one instead
		IF (Check_If_Player_Should_Be_Offered_A_Previously_Completed_CM())
			EXIT
		ENDIF

		// Player shouldn't be offered a previously completed CM, so randomly choose a Contact Mission of the player's rank
		// KGM 5/8/13: Allow randomly choosing one of the closest to the player
		INT		playerRank			= 0
		BOOL	findClosestToPlayer	= TRUE
		INT		numRandomised		= 4
		

		playerRank = GET_PLAYER_RANK(PLAYER_ID())

	
		INT thisCMArrayPos = Get_Random_Contact_Mission_Matching_These_Details(NO_CHARACTER, playerRank, CM_RANK_USE_UNPLAYED_BETWEEN_CM_MIN_AND_CURRENT_RANKS, findClosestToPlayer, numRandomised)
		IF (thisCMArrayPos = ILLEGAL_CONTACT_MISSION_SLOT)
			// ...no CMs were offered, so try again after a short delay
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcTimeout	= Get_Random_Time_Offset(MP_CONTACT_MISSION_SHORT_PLAYER_DELAY_msec)
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [ActSelect][ContactMission]: CURRENTLY NO CMs TO OFFER: Try again after short delay. CM Min Rank: ")
				NET_PRINT_INT(g_CMsPlayed.cmrmMinRank)
				NET_NL()
				Debug_Output_Active_CMs_Array()
			#ENDIF
			
			EXIT
		ENDIF
	
		// Ensure this contact has made their intro phonecall to the player
		enumCharacterList theContactID = Get_Contact_Mission_ContactID_As_CharacterList_For_Slot(thisCMArrayPos)
		IF NOT (HAS_PLAYER_UNLOCKED_CONTACT(PLAYER_ID(), theContactID))
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [ActSelect][ContactMission]: Rejecting chosen contact mission. Player hasn't yet received intro phonecall from character: ")
				NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sCharacterSheetAll[theContactID].label))
				NET_NL()
			#ENDIF
			
			EXIT
		ENDIF
		
		// Found a mission, so store the details as a Contact Mission
		Store_Contact_Mission_Based_On_Contact_Mission_Slot(thisCMArrayPos, FALSE)
		
		// Ask the server if it is okay to send an invite for this mission
		Set_My_Contact_Mission_Stage(CONTACT_MISSION_CLIENT_STAGE_REQUEST_TO_SEND_INVITE)
	ENDIF
ENDPROC




// ===========================================================================================================
//      Client Initialisation and Maintenance Routines
// ===========================================================================================================
// -----------------------------------------------------------------------------------------------------------
//     Client Maintenance - Contact Missions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Setup Contact Mission and send an invite from the contact to the player
//
// INPUT PARAMS:			paramSendInvite				[DEFAULT = TRUE] TRUE if an invite should be sent, FALSE if not
//							paramAddToStats				[DEFAULT = FALSE] TRUE if the NPC Invite stats should get updated, FALSE if not
//							paramReAdding				[DEFAULT = FALSE] TRUE if the mission is being re-added after a transition, FALSE if being setup normally
//							paramUseOnHours				[DEFAULT = TRUE] TRUE if the mission should obey the OnHours, FALSE if it shoudl remain active all day
// REFERENCE PARAMS:		refMissionIdData			The MissionID Data for the Contact Mission
PROC Setup_Contact_Mission_From_CM_Data_And_Send_Invite(MP_MISSION_ID_DATA &refMissionIdData, BOOL paramSendInvite = TRUE, BOOL paramAddToStats = FALSE, BOOL paramReAdding = FALSE, BOOL paramUseOnHours = TRUE)

	// Register the Contact Mission with Missions At Coords
	g_eMPMissionSource		theSource			= MP_MISSION_SOURCE_MISSION_FLOW_BLIP
	VECTOR					theCoords			= Get_Coordinates_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	g_structMatCBlipMP		theBlipDetails		= Gather_Blip_Details_For_Shared_Mission(refMissionIdData)
	g_structMatCCoronaMP	theCorona			= Gather_Corona_Details_For_Shared_Mission(refMissionIdData)
	g_structMatCOptionsMP	theOptions			= Gather_Optional_Flags_For_Shared_Mission(refMissionIdData)

	INT atCoordsID = Register_MP_Mission_At_Coords(theSource, theCoords, theBlipDetails, theCorona, theOptions, refMissionIdData)
	
	IF (atCoordsID = ILLEGAL_AT_COORDS_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]: ") NET_PRINT_TIME() NET_PRINT(" Setup_Contact_Mission_From_CM_Data_And_Send_Invite(): FAILED TO REGISTER WITH MISSIONS AT COORDS") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: ")
		NET_PRINT_TIME()
		NET_PRINT(" Setup_Contact_Mission_From_CM_Data_And_Send_Invite(): Successfully registered with Missions At Coords RegID: ")
		NET_PRINT_INT(atCoordsID)
		NET_NL()
	#ENDIF
	
	// Ensure the Contact Mission is unlocked whatever the player's rank
	Temporarily_Unlock_MissionsAtCoords_Mission_By_Filename(refMissionIdData.idCloudFilename, TRUE)
	
	// Update the Contact Mission variables after registered
	// To Do: Store the At Coords ID for the mission (if needed)
	
	// Send the invite?
	IF NOT (paramSendInvite)
		EXIT
	ENDIF
	
	INT						theFMMissionType		= Convert_MissionID_To_FM_Mission_Type(refMissionIdData.idMission)
	INT						theSubtype				= Get_SubType_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	enumCharacterList		theContact				= Get_Contact_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	TEXT_LABEL_63			theMissionName			= Get_Mission_Name_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	TEXT_LABEL_63			theMissionDescription	= ""
	INT						theOnHours				= MATC_ALL_DAY
	
	IF (paramUseOnHours)
		theOnHours = Get_On_Hours_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	ENDIF
	
	Store_Invite_Details_For_Joblist_From_NPC(theFMMissionType, theSubtype, theCoords, refMissionIdData.idCreator, theContact, theMissionName, theMissionDescription, refMissionIdData.idCloudFilename, paramReAdding, theOnHours)
	
	// Should this update the playstats
	// NOTE: This should be TRUE if it is a new contact mission request only
    // Add to NPC Playlist stats if appropriate
    IF (paramAddToStats)
        TEXT_LABEL  npcNameAsTL = Get_NPC_Name(theContact)
        TEXT_LABEL_31 npcNameForStats = GET_FILENAME_FOR_AUDIO_CONVERSATION(npcNameAsTL)
        PLAYSTATS_NPC_INVITE(npcNameForStats)
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [ActSelect][ContactMission]: Calling PLAYSTATS_NPC_INVITE() with: ")
            NET_PRINT(npcNameForStats)
            NET_NL()
        #ENDIF
    ENDIF

ENDPROC

/// PURPOSE:
///    Find the mission in the rockstar created array and send the invite
FUNC BOOL Set_Message_Invite_From_Root_Content_Id_Hash(INT iRootContentID, enumCharacterList eCharacter = CHAR_MARTIN, BOOL jliQuickMatch = FALSE, INT iStartLoop = -1, INT iEndLoop = -1)

	UNUSED_PARAMETER(iStartLoop)
	UNUSED_PARAMETER(iEndLoop)
	
	IF iRootContentID != -1
	AND iRootContentID != 0	
	
		INT iArray = GET_CONTENT_ID_INDEX(iRootContentID)

		//If we got it then send the message
		IF iArray >=0
			CPRINTLN(DEBUG_NET_AMBIENT, "[LOWFLOW] SET_MESSAGE_INVITE_FROM_ROOT_CONTENT_ID_HASH - Store_Invite_Details_For_Joblist_From_NPC - ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArray].tlMissionName, " -  ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArray].tlName)
			Store_Invite_Details_For_Joblist_From_NPC(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArray].iType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArray].iSubType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArray].vStartPos, FMMC_ROCKSTAR_CREATOR_ID, eCharacter,
	        											g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArray].tlMissionName, "", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArray].tlName, DEFAULT, DEFAULT, jliQuickMatch)
	
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets a specific mission within the adversary type passed and sends the invite to the player
PROC Setup_Adversary_Invite_And_Send_Invite(g_eContactMissionAdversaryTypes adversaryType)

	INT adversaryId = Get_Contact_Mission_Adversary_Id_From_Type(adversaryType)
	
	// Ensure the returned adversary Id is valid
	IF (adversaryId = ILLEGAL_AT_COORDS_ID)
		PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary] Setup_Adversary_Invite_And_Send_Invite - Invalid adversary Id: ", adversaryId)
		EXIT
	ENDIF
	
	// Get the mission root content id for the adversary type
	INT missionRootCID = GET_RANDOM_ROCKSTAR_MISSION_NEW_VS_MISSION_FROM_MODE(adversaryId)

	IF (missionRootCID = 0)
		PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary] Setup_Adversary_Invite_And_Send_Invite - missionRootCID: ", missionRootCID)
		EXIT
	ENDIF
	
	PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary] Setup_Adversary_Invite_And_Send_Invite - Send invite for mission with RCID: ", missionRootCID)
	// Find the mission and send the invite
	Set_Message_Invite_From_Root_Content_Id_Hash(missionRootCID)
ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Setup Contact Mission and send an invite from the contact to the player
//
// INPUT PARAMS:		paramSendInvite			[DEFAULT = TRUE] TRUE if an invite should be sent, FALSE if not
//						paramAddToStats			[DEFAULT = FALSE] TRUE if the NPC Invite stats should update, otherwise FALSE
//						paramReAdding			[DEFAULT = FALSE] TRUE if this mission is being re-added after transition, otherwise FALSE
//						paramUseOnHours			[DEFAULT = TRUE] TRUE if the Invite should obey the OnHours, otherwise FALSE
PROC Setup_Contact_Mission_For_FM_Contact_Mission_Group_And_Send_Invite(BOOL paramSendInvite = TRUE, BOOL paramAddToStats = FALSE, BOOL paramReAdding = FALSE, BOOL paramUseOnHours = TRUE)
	MP_MISSION_ID_DATA theMissionIdData	= Get_Player_MissionID_Data_For_Chosen_CM(PLAYER_ID())
	Setup_Contact_Mission_From_CM_Data_And_Send_Invite(theMissionIdData, paramSendInvite, paramAddToStats, paramReAdding, paramUseOnHours)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	When there is no Contact Mission setup, check if there is a preset mission setup - if so, then the player must just have launched a new transition session
PROC Maintain_Contact_Mission_Client_Stage_No_Mission()

	// KGM NOTE: This is triggered by Dave's function call that then uses Try_To_Find_Contact_Mission_To_Offer_To_Player()

// KGM 30/5/13: COMMENTING THIS OUT FOR NOW - I'LL NEED SOME INFO BUT THE MISSION ITSELF SHOULD GET RE-LAUNCHED USING STANDARD FUNCTIONALITY AFTER THE TRANSITION
//	IF NOT (g_sLocalTransitionSafeCMData.asfmcmtTransitionInProgress)
//		EXIT
//	ENDIF
//	
//	// InCorona Transition is in progress - so setup the required mission again
//	#IF IS_DEBUG_BUILD
//		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Found PreSet Contact Mission - Player must have been performing InCorona transition, so setup the mission again")
//		NET_NL()
//	#ENDIF
//	
//	INT presetCMArrayPos = Find_Matching_Activity_On_Pending_Activities(g_sLocalTransitionSafeCMData.asfmcmtCloudFilename, MP_CONTACT_MISSION_GENERIC_COORDS)
//	IF (presetCMArrayPos = ILLEGAL_PENDING_ACTIVITIES_SLOT)
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("      BUT: Couldn't find Cloud Filename in Pending Activities array - try again later. Cloud Filename: ")
//			NET_PRINT(g_sLocalTransitionSafeCMData.asfmcmtCloudFilename)
//			NET_NL()
//		#ENDIF
//		
//		EXIT
//	ENDIF
//	
//	// Found the Preset Contact Mission - so set up the Mission again
//	#IF IS_DEBUG_BUILD
//		NET_PRINT("      Found Cloud Filename in Pending Activities array, so setting up the Contact Mission again to allow immediate triggering")
//		NET_NL()
//	#ENDIF
//	
//	Store_Contact_Mission_Based_On_Pending_Activities_Slot(presetCMArrayPos)
//	
//	BOOL sendInvite = FALSE
//	Setup_Contact_Mission_For_FM_Contact_Mission_Group_And_Send_Invite(sendInvite)
//	
//	// Clear out the preset details
//	g_sLocalTransitionSafeCMData.asfmcmtTransitionInProgress	= FALSE
//	g_sLocalTransitionSafeCMData.asfmcmtCloudFilename			= ""
//	
//	// Move on to Invite Sent stage
//	Set_My_Contact_Mission_Stage(CONTACT_MISSION_CLIENT_STAGE_TEMP_INVITE_SENT)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Setup the mission then send an invite to the mission
PROC Maintain_Contact_Mission_Client_Stage_Send_Invite()

	// Setup the mission
	BOOL sendInvite = TRUE
	BOOL addToStats = TRUE
	BOOL reAdding	= FALSE
	BOOL useOnHours	= TRUE
	Setup_Contact_Mission_For_FM_Contact_Mission_Group_And_Send_Invite(sendInvite, addToStats, reAdding, useOnHours)

	// Move to the TEMP Invite Sent stage
	Set_My_Contact_Mission_Stage(CONTACT_MISSION_CLIENT_STAGE_INVITE_SENT)
	
	// TEMP: Store the preset mission details here (when the invite sent - need to do this when the player in the corona instead)
// KGM 30/5/13: COMMENTING THIS OUT FOR NOW - I'LL NEED SOME INFO BUT THE MISSION ITSELF SHOULD GET RE-LAUNCHED USING STANDARD FUNCTIONALITY AFTER THE TRANSITION
//	INT						theSlot				= Get_Player_Contact_Mission_Pending_Activities_Slot(PLAYER_ID())
//	MP_MISSION_ID_DATA		theMissionIdData	= Get_Pending_Activities_MissionID_Data_For_Slot(theSlot)
//	
//	g_sLocalTransitionSafeCMData.asfmcmtTransitionInProgress	= TRUE
//	g_sLocalTransitionSafeCMData.asfmcmtCloudFilename			= theMissionIdData.idCloudFilename

ENDPROC

/// PURPOSE:
///    Processes the player being sent an Adversary invite rather than a Contact mission. Bases the selection of weights
PROC Maintain_Contact_Mission_Client_Stage_Send_Adversary_Invite()

	PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary] Maintain_Contact_Mission_Client_Stage_Send_Adversary_Invite()")

	FLOAT totalWeighting = 0
	FLOAT adversaryWeighting
	//FLOAT overriddenWeighting = -1
	g_eContactMissionAdversaryTypes adversaryType
	g_eContactMissionAdversaryTypes chosenAdversaryType = CM_ADVERSARY_TYPE_NULL
		
	INT loop
	// Get our total weighting for all valid adversary modes
	REPEAT CM_ADVERSARY_TYPE_MAX loop
	
		adversaryType = INT_TO_ENUM(g_eContactMissionAdversaryTypes, loop)
	
		IF Is_Contact_Mission_Adversary_Type_Active(adversaryType)
		
			// Grab the weighting for this adversary type
			adversaryWeighting = Get_Contact_Mission_Adversary_Type_Weighting(adversaryType)
		
//			IF Is_Contact_Mission_Adversary_Type_Weighting_Time_Flexible(adversaryType)
//				adversaryWeighting = Get_Contact_Mission_Adversary_Type_Weighting_Time_Flexible(adversaryType, totalWeighting, overriddenWeighting)
//				overriddenWeighting = adversaryWeighting
//				
//				PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary] 			- Overridding weighting for mode: ", Get_Contact_Mission_Adversary_Debug_Name(adversaryType), " with weighting: ", adversaryWeighting)
//			ENDIF
		
			totalWeighting += adversaryWeighting
			PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary] 			- For adversary mode: ", Get_Contact_Mission_Adversary_Debug_Name(adversaryType), " adding weight of: ", adversaryWeighting)
			PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary] 				- Total Weight: ", totalWeighting)
		ENDIF
	ENDREPEAT
	
	NETWORK_SEED_RANDOM_NUMBER_GENERATOR(GET_CLOUD_TIME_AS_INT()/g_sMPTunables.iNjvsSyncWindow)
	INT iTotalWeight = ROUND(totalWeighting*100)
	FLOAT randomSelection = (NETWORK_GET_RANDOM_INT_RANGED(0, iTotalWeight) / 100.0)
	
	PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary] 		- totalWeighting = ", totalWeighting)
	PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary]  		- randomSelection = ", randomSelection)
	
	totalWeighting = 0
	//overriddenWeighting = -1
	
	REPEAT CM_ADVERSARY_TYPE_MAX loop
	
		adversaryType = INT_TO_ENUM(g_eContactMissionAdversaryTypes, loop)
		
		IF Is_Contact_Mission_Adversary_Type_Active(adversaryType)
		
			adversaryWeighting = Get_Contact_Mission_Adversary_Type_Weighting(adversaryType)
//			IF Is_Contact_Mission_Adversary_Type_Weighting_Time_Flexible(adversaryType)
//				adversaryWeighting = Get_Contact_Mission_Adversary_Type_Weighting_Time_Flexible(adversaryType, totalWeighting, overriddenWeighting)
//				overriddenWeighting = adversaryWeighting
//			ENDIF
			
			totalWeighting += adversaryWeighting
			
			PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary] 			- Checking for adversary mode: ", Get_Contact_Mission_Adversary_Debug_Name(adversaryType), " adding weight of: ", adversaryWeighting)
			PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary] 				- TotalWeighting: ", totalWeighting)
			PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary] 				- RandomSelection: ", randomSelection)

			IF randomSelection < totalWeighting
			
				// Save out the type we are going to be sending
				chosenAdversaryType = adversaryType
				loop = ENUM_TO_INT(CM_ADVERSARY_TYPE_MAX)	// Exit
			ENDIF
		ENDIF
	ENDREPEAT
		
	// Set our delay before another invite
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcTimeout	= Get_CM_Randomised_Regular_Delay_msec()
	
	// Move back to waiting for another mission
	Set_My_Contact_Mission_Stage(CONTACT_MISSION_CLIENT_STAGE_NO_MISSION)
		
	IF (chosenAdversaryType = CM_ADVERSARY_TYPE_NULL)
		PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary] chosenAdversaryType = CM_ADVERSARY_TYPE_NULL. Do not send any invites. Suggests all available modes have weighting = 0")
		EXIT
	ENDIF

	PRINTLN("...KGM MP [ActSelect][ContactMission][Adversary] Maintain_Contact_Mission_Client_Stage_Send_Adversary_Invite() Invite for mode: ", Get_Contact_Mission_Adversary_Debug_Name(adversaryType))

	Setup_Adversary_Invite_And_Send_Invite(chosenAdversaryType)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	After an invite is sent, stores the 'current CM' and clears them to allow another 'current CM' if the player can receive more
PROC Maintain_Contact_Mission_Client_Stage_Invite_Sent()

	// Add current CM details onto active CMs array
	MP_MISSION_ID_DATA		currentMPID			= GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcMissionIdData
	g_eFMHeistContactIDs	currentContactID	= GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcContact
	BOOL					previouslyPlayed	= GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcPreviouslyCompleted
	
	Add_Details_To_Client_Active_CM_Array(currentMPID, currentContactID, previouslyPlayed)

	// Clear out the 'current CM' details
	Clear_MP_MISSION_ID_DATA_Struct(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcMissionIdData)
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcContact	= NO_FM_HEIST_CONTACT
	
	// Introduce a decent delay before allowing another CM for this player
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcTimeout	= Get_CM_Randomised_Regular_Delay_msec()

	// Move back to waiting for another mission
	Set_My_Contact_Mission_Stage(CONTACT_MISSION_CLIENT_STAGE_NO_MISSION)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// KGM 6/8/13: Obsolete - we'll use the 'played' flag instead of waiting for an invite to be accepted
//// PURPOSE:	Waits for the JOblist to indicate the invite has been accepted
//PROC Maintain_Contact_Mission_Client_Stage_Wait_For_Invite_Accept()
//
//	// Has the player become reserved for a Contact Mission by accepting the contact invite?
//	IF NOT (g_sCMInviteAccept.ciReservedForJob)
//		EXIT
//	ENDIF
//	
//	// Player has become reserved for a mission from a Contact - is it this Contact Mission's Contact?
//	g_eFMHeistContactIDs	theHeistContact		= Get_Player_Heist_ContactID_For_Chosen_CM(PLAYER_ID())
//	enumCharacterList		theContact			= Convert_FM_Heist_ContactID_To_CharacterListID(theHeistContact)
//	
//	IF (g_sCMInviteAccept.ciInvitingContact != theContact)
//		EXIT
//	ENDIF
//
//	// Correct contact, so compare cloud filenames
//	IF (IS_STRING_NULL_OR_EMPTY(g_sCMInviteAccept.ciCloudFilename))
//		EXIT
//	ENDIF
//	
//	MP_MISSION_ID_DATA	theMissionIdData	= Get_Player_MissionID_Data_For_Chosen_CM(PLAYER_ID())
//	TEXT_LABEL_23		theContentID		= theMissionIdData.idCloudFilename
//	
//	IF (IS_STRING_NULL_OR_EMPTY(theContentID))
//		EXIT
//	ENDIF
//	
//	IF NOT (ARE_STRINGS_EQUAL(g_sCMInviteAccept.ciCloudFilename, theContentID))
//		EXIT
//	ENDIF
//	
//	// An invite has just been accepted for the correct job, so clear the Invite details, mark the mission as 'played', and move on
//	#IF IS_DEBUG_BUILD
//		NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
//		NET_PRINT_TIME()
//		NET_PRINT("Invite for this Contact Mission has been accepted. Filename: ")
//		NET_PRINT(theContentID)
//		NET_NL()
//	#ENDIF
//		
//	// ...clear the invite details
//	g_sCMInviteAccept.ciInvitingContact		= NO_CHARACTER
//	g_sCMInviteAccept.ciCloudFilename		= ""
//	g_sCMInviteAccept.ciReservedForJob		= FALSE
//	
//	// ...mark the mission as played (and update the saved stats)
//	Store_One_Contact_Mission_ContentID_In_Local_Storage(theContentID)
//	
//	// ...move on to holding stage
//	Set_My_Contact_Mission_Stage(CONTACT_MISSION_CLIENT_STAGE_WAIT_FOR_ON_MISSION)
//	
//ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Contact Mission 'first play' flag
//
// INPUT PARAMS:		paramContentID			The contact mission's contentID
//
// NOTES:	This is used by Rowan to decide whether to give full cash or half cash at the end of the mission.
//			Assume ContentID is not NULL - this should have been pre-checked
//			31/1/14: It's fine for this function to continue to use contentID Hash instead of rootContentID Hash because it isn't doing anything with the 'played mission' STATs storage
PROC Set_Contact_Mission_First_Play_Status(TEXT_LABEL_31 paramContentID)

	// RULES:	If there is any doubt, set to FALSE (half cash)
	//			If the mission is beyond this player's current rank, set to FALSE (half cash - the fact the mission is played won't be recorded if beyond current rank)
	//			If the mission is within the current rank then set the flag based on the 'played' flag associated with this mission
	//			If the mission is not found, then leave as FALSE (better to give half cash than have a full cash exploit of some description)
	
	// By default, set to FALSE
	g_cpcmPlaying.cpcmFirstPlay = FALSE
	
	// Search for this mission in the list of Contact Missions
	INT 			tempLoop					= 0
	INT 			missionRank					= 0
	INT				playerRank					= GET_PLAYER_RANK(PLAYER_ID())
	INT				thisContentIdHash			= GET_HASH_KEY(paramContentID)
	INT				holdFirstPlayCID			= g_cpcmHashMostRecentFirstPlayContentID
	BOOL			holdFirstPlayFullCashGiven	= g_cpcmMostRecentFirstPlayFullCashGiven
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
		NET_PRINT_TIME()
		NET_PRINT("Setting Current CM First Play Status: ")
	#ENDIF
	
	// Always, clear out the 'most recent' First Play variables
	g_cpcmHashMostRecentFirstPlayContentID	= 0
	g_cpcmMostRecentFirstPlayFullCashGiven	= FALSE
	
	REPEAT g_numLocalMPCMs tempLoop
		missionRank = Get_Contact_Mission_Rank_For_Slot(tempLoop)
		
		// The 'first play' flag can only be TRUE if the mission is within the active ranks
		IF (missionRank >= g_CMsPlayed.cmrmMinRank)
		AND (missionRank <= playerRank)
			// ..within the active ranks, so check if the contentID Hash values are the same
			IF (thisContentIdHash = g_sLocalMPCMs[tempLoop].lcmContentIdHash)
				// ...found the mission, so check if it is 'played'
				IF (g_sLocalMPCMs[tempLoop].lcmPlayed)
					// KGM 13/4/14: It has been played, so check if this mission is a replay of the most recent First Play mission and it
					//		didn't get a reward (ie: because it failed) and allow full cash again
					IF (holdFirstPlayCID = thisContentIdHash)
					AND NOT (holdFirstPlayFullCashGiven)
						// ...Although played, this mission was the most recent CM and it was failed on it's first play, so full cash wasn't given - so allow full cash this time again
						g_cpcmPlaying.cpcmFirstPlay				= TRUE
						g_cpcmHashMostRecentFirstPlayContentID	= thisContentIdHash
						
						#IF IS_DEBUG_BUILD
							NET_PRINT("FIRST PLAY = FALSE (already played) - BUT cash reward wasn't given. Treating FIRST PLAY as TRUE again") NET_NL()
						#ENDIF
					ELSE
						g_cpcmPlaying.cpcmFirstPlay = FALSE
						
						#IF IS_DEBUG_BUILD
							NET_PRINT("FIRST PLAY = FALSE (already played)") NET_NL()
						#ENDIF
					ENDIF
				ELSE
					g_cpcmPlaying.cpcmFirstPlay				= TRUE
					g_cpcmHashMostRecentFirstPlayContentID	= thisContentIdHash
					
					#IF IS_DEBUG_BUILD
						NET_PRINT("FIRST PLAY = TRUE (within active ranks and not played)") NET_NL()
					#ENDIF
				ENDIF
				
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
				
	// Mission not found, or not within active ranks
	g_cpcmPlaying.cpcmFirstPlay = FALSE
					
	#IF IS_DEBUG_BUILD
		NET_PRINT("FIRST PLAY = FALSE (mission not found, or not within active ranks)") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if this contentID is at the player's current CM Rank
//
// INPUT PARAMS:		paramContentID			The contact mission's contentID
// RETURN VALUE:		BOOL					TRUE if at the player's current rank, FALSE if not
FUNC BOOL Is_This_Contact_Mission_Within_Players_Active_CM_Ranks(TEXT_LABEL_31 paramContentID)

	// Go through all stored Contact Missions and compare content IDs for the ones between the player's CM Min Rank and player's Game Rank
	INT 			tempLoop	= 0
	INT 			missionRank	= 0
	INT				cmCreatorID	= ILLEGAL_CREATOR_ID
	INT 			cmVariation	= NO_MISSION_VARIATION
	INT				playerRank	= GET_PLAYER_RANK(PLAYER_ID())
	TEXT_LABEL_31	cmFilename	= ""
	
	REPEAT g_numLocalMPCMs tempLoop
		missionRank = Get_Contact_Mission_Rank_For_Slot(tempLoop)
		
		// Check if mission rank matches player's active ranks then compare content IDs
		IF (missionRank >= g_CMsPlayed.cmrmMinRank)
		AND (missionRank <= playerRank)
			cmCreatorID	= g_sLocalMPCMs[tempLoop].lcmCreatorID
			cmVariation	= g_sLocalMPCMs[tempLoop].lcmVariation
			cmFilename	= Get_FileName_For_FM_Cloud_Loaded_Activity(cmCreatorID, cmVariation)
			
			IF (ARE_STRINGS_EQUAL(cmFilename, paramContentID))
				// ...found a matching contentID within the player's active CM Ranks
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player is actively playing a contact mission
// NOTES:	31/1/14: This function doesn't need to change for rootContentID Hashes. Any sub-functions accepting the contentID have been updated as appropriate.
PROC Maintain_Client_Missions_Played()

	// Don't do this on this frame if the initial cloud load hasn't completed
	IF NOT (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
		EXIT
	ENDIF

	BOOL	wasPlayingCM	= g_cpcmPlaying.cpcmOnCM
	BOOL	isPlayingCM		= Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID())
	
	// Is there a change in state?
	IF (wasPlayingCM = isPlayingCM)
		EXIT
	ENDIF
	
	// There is a change of state so if no longer on a CM then reset the flag
	IF NOT (isPlayingCM)
		// Reset the 'on CM' flag
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
			NET_PRINT_TIME()
			NET_PRINT("Player is no longer playing a Contact Mission")
			NET_NL()
		#ENDIF
		
		g_cpcmPlaying.cpcmOnCM		= FALSE
		
		// Clear the 'first play' flag
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
			NET_PRINT_TIME()
			NET_PRINT("Clearing the 'First Play' flag when Contact Mission over")
			NET_NL()
		#ENDIF
		
		g_cpcmPlaying.cpcmFirstPlay	= FALSE
		
		EXIT
	ENDIF
	
	// Player is now playing a CM, so set the 'first play' flag to FALSE by default. If there is an error then only half cash will be rewarded.
	g_cpcmPlaying.cpcmFirstPlay = FALSE

	// Ensure the contact mission details are available - use the mission triggerer data since it sets the 'currently on CM' function's data
	TEXT_LABEL_31 cmContentID = g_sTriggerMP.mtMissionData.mdID.idCloudFilename
	IF (IS_STRING_NULL_OR_EMPTY(cmContentID))
		EXIT
	ENDIF
	
	// Store the change of state
	g_cpcmPlaying.cpcmOnCM = TRUE
	
	// Player is now on a contact mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
		NET_PRINT_TIME()
		NET_PRINT("Player is now playing a Contact Mission: ")
		NET_PRINT(cmContentID)
		NET_PRINT(":")
		NET_PRINT_INT(GET_HASH_KEY(cmContentID))
		NET_NL()
	#ENDIF
	
	// Store the flag that indicates whether or not this is the Contact Mission's 'first play' based on a few rules (see function for rules)
	Set_Contact_Mission_First_Play_Status(cmContentID)
	
	// Store this contact mission in the history
	// NOTE: Currently only deals with the contact of the most recently played mission
	Store_Contact_Mission_In_History(cmContentID)
	
	// If this CM is on the active CMs array then remove it
	Remove_This_Mission_If_On_Active_CM_Array(cmContentID)
	
	// Only record this contact mission if it is within the player's active CM Ranks
	IF NOT (Is_This_Contact_Mission_Within_Players_Active_CM_Ranks(cmContentID))
		// ...not within player's active CM ranks, so nothing more to do
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
			NET_PRINT_TIME()
			NET_PRINT("Contact Mission is not unplayed between player's CM Min Rank and player's Game Rank, so nothing more to do: ")
			NET_PRINT(cmContentID)
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Add it to the 'played' STATS if not already stored
	IF NOT (Is_This_Mission_ContentID_In_Local_Storage(cmContentID))
		Store_One_Contact_Mission_ContentID_In_Local_Storage(cmContentID)
	ELSE
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
			NET_PRINT_TIME()
			NET_PRINT("Contact Mission is already stored as string stats: ")
			NET_PRINT(cmContentID)
			NET_NL()
		#ENDIF
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Contact Mission Minimum Rank needs updated
PROC Maintain_Client_Rank_Update_Check()

	// Nothing to do until timeout
	IF NOT (Has_Rank_Update_Timeout_Expired())
		EXIT
	ENDIF
	
	// Timeout has occurred
	// Ensure the initial cloud data is available - if not do a short delay before trying rank update again
	IF NOT (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
			NET_PRINT_TIME()
			NET_PRINT("Rank Update Timeout has occurred, but initial cloud data isn't available. Try again soon.")
			NET_NL()
		#ENDIF
		
		Set_Next_Rank_Update_Quick_Retest_Timeout()
		EXIT
	ENDIF
	
	// Ensure a cloud refresh isn't in progress - if it is do a short delay before trying rank update again
	IF (Is_Cloud_Loaded_Mission_Data_Being_Refreshed())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
			NET_PRINT_TIME()
			NET_PRINT("Rank Update Timeout has occurred, but a cloud refresh is in progress. Try again soon.")
			NET_NL()
		#ENDIF
		
		Set_Next_Rank_Update_Quick_Retest_Timeout()
		EXIT
	ENDIF
	
	// Ensure contact missions have been stored - if none have this may be the sign of a problem and we don't want to progress the CM Rank
	IF (g_numLocalMPCMs = 0)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
			NET_PRINT_TIME()
			NET_PRINT("Rank Update Timeout has occurred, but there are no stored Contact Missions. Try again soon.")
			NET_NL()
		#ENDIF
		
		Set_Next_Rank_Update_Quick_Retest_Timeout()
		EXIT
	ENDIF
	
	// Don't do CM Rank update if there is a Focus Mission - the save won't occur on mission
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
			NET_PRINT_TIME()
			NET_PRINT("Rank Update Timeout has occurred, but there is a Focus Mission. Try again soon.")
			NET_NL()
		#ENDIF
		
		Set_Next_Rank_Update_Quick_Retest_Timeout()
		EXIT
	ENDIF
	
	// If the CM Min Rank is already greater than the Player Rank then an update check is not needed
	INT playerRank = GET_PLAYER_RANK(PLAYER_ID())
	IF (g_CMsPlayed.cmrmMinRank > playerRank)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
			NET_PRINT_TIME()
			NET_PRINT("Rank Update Timeout has occurred, but ignore for now: CM Min Rank [")
			NET_PRINT_INT(g_CMsPlayed.cmrmMinRank)
			NET_PRINT("] is higher than Game Rank [")
			NET_PRINT_INT(playerRank)
			NET_PRINT("]")
			NET_NL()
		#ENDIF
		
		Set_Next_Rank_Update_Standard_Timeout()
		EXIT
	ENDIF
	
	// A Rank update check is needed
	// Check if there are any unplayed Contact Missions remaining at this rank
	// NOTE: use the 'Get Random' function for the current rank - this will return an array position for any unplayed contact mission at the current rank,
	//		or ILLEGAL_CONTACT_MISSION_SLOT if there are none or all have been played
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
		NET_PRINT_TIME()
		NET_PRINT("CM MIN Rank Update Timeout has occurred. Check for unplayed missions at CM Min Rank.")
		NET_NL()
	#ENDIF
	
	IF (Are_There_Unplayed_Contact_Missions_At_Rank(g_CMsPlayed.cmrmMinRank))
		// ...there are still unplayed CMs at the minimum rank
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
			NET_PRINT_TIME()
			NET_PRINT("Ignore CM Rank Update for now: there are still unplayed missions at the current CM Min Rank.")
			NET_NL()
		#ENDIF
		
		Set_Next_Rank_Update_Standard_Timeout()
		EXIT
	ENDIF
	
	// There are no unplayed CMs remaining at the current CM Min Rank
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
		NET_PRINT_TIME()
		NET_PRINT("There are no unplayed Contact Missions at the current CM Min Rank. Advancing to next rank. Current CM Min Rank: ")
		NET_PRINT_INT(g_CMsPlayed.cmrmMinRank)
		NET_NL()
	#ENDIF
	
	Advance_To_Next_Contact_Mission_Rank_And_Set_Rank_Check_Timeout()

	#IF IS_DEBUG_BUILD
		Debug_Output_Local_Activity_Selector_Details_After_Refresh()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the most recent client on the history list
PROC Maintain_Client_Most_Recent_CM_Contact()
	// Is there a most recent contact?
	IF (g_cmLocalHistory.chcmRecentContact.crcmHeistContact = NO_FM_HEIST_CONTACT)
		EXIT
	ENDIF
	
	// There is a most recent contact
	// Was the mission still classed as 'in progress' last frame?
	IF (g_cmLocalHistory.chcmRecentContact.crcmFrameTimeout = MISSION_IN_PROGRESS_FRAME)
		// ...mission was still in progress, so check if still in progress
		IF (Is_There_A_MissionsAtCoords_Focus_Mission())
			EXIT
		ENDIF
		
		// No longer in progress, so setup the frame counter expiry value
		g_cmLocalHistory.chcmRecentContact.crcmFrameTimeout = GET_FRAME_COUNT() + MOST_RECENT_CONTACT_TIMEOUT_frame
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]: HISTORY: Mission not active, so setting expiry frame for most recent contact: ")
			NET_PRINT_INT(ENUM_TO_INT(g_cmLocalHistory.chcmRecentContact.crcmHeistContact))
			NET_PRINT("  Expiry Frame: ")
			NET_PRINT_INT(g_cmLocalHistory.chcmRecentContact.crcmFrameTimeout)
			NET_PRINT("  [current frame: ")
			NET_PRINT_INT(GET_FRAME_COUNT())
			NET_PRINT("]")
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Mission wasn't 'in progress' last frame, so check for frame expiry
	IF (GET_FRAME_COUNT() < g_cmLocalHistory.chcmRecentContact.crcmFrameTimeout)
		EXIT
	ENDIF
	
	// Clean up the most recent contact
	g_cmLocalHistory.chcmRecentContact.crcmHeistContact = NO_FM_HEIST_CONTACT
	g_cmLocalHistory.chcmRecentContact.crcmFrameTimeout = 0
		
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: HISTORY: Most recent contact frame has expired. Cleaning up.")
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain any items on the CM History
PROC Maintain_Client_CM_History()
	// Maintain the most recent contact
	Maintain_Client_Most_Recent_CM_Contact()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Activity Selector Client Contact Missions Group
PROC Maintain_MP_Activity_Selector_Contact_Mission_Group_Client()


	SWITCH (Get_Player_Contact_Mission_Stage(PLAYER_ID()))
		CASE CONTACT_MISSION_CLIENT_STAGE_NO_MISSION
			Maintain_Contact_Mission_Client_Stage_No_Mission()
			BREAK
			
		CASE CONTACT_MISSION_CLIENT_STAGE_REQUEST_TO_SEND_INVITE
			// Jump to 'Send Invite' - We're not involving the server anymore
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [ActSelect][ContactMission]: Ignoring any communication with the server - jumping directly to 'send invite' on client") NET_NL()
			#ENDIF
			
			Set_My_Contact_Mission_Stage(CONTACT_MISSION_CLIENT_STAGE_SEND_INVITE)
			BREAK
			
		CASE CONTACT_MISSION_CLIENT_STAGE_SEND_INVITE
			Maintain_Contact_Mission_Client_Stage_Send_Invite()
			BREAK
			
		CASE CONTACT_MISSION_CLIENT_STAGE_SEND_INVITE_ADVERSARY
			Maintain_Contact_Mission_Client_Stage_Send_Adversary_Invite()
			BREAK
			
		CASE CONTACT_MISSION_CLIENT_STAGE_INVITE_SENT
			// Stores the details in the local globals then clears the 'current invite' array ready for another invite
			Maintain_Contact_Mission_Client_Stage_Invite_Sent()
			BREAK
			
		DEFAULT
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [ActSelect][ContactMission]: UNKNOWN CONTACT MISSION CLIENT STAGE.") NET_NL()
				SCRIPT_ASSERT("Maintain_MP_Activity_Selector_Contact_Mission_Group_Client() - ERROR: Trying to process Unknown Contact Mission Client Stage. Add to Switch statement. Tell Keith")
			#ENDIF
			
			BREAK
	ENDSWITCH
	
	// Maintain the Rank Update scheduling
	Maintain_Client_Rank_Update_Check()
	
	// Maintain the Missions Played checking
	Maintain_Client_Missions_Played()
	
	// Maintain the Local CM History
	Maintain_Client_CM_History()
	
	#IF IS_DEBUG_BUILD
		Maintain_Contact_Mission_Widgets()
		Maintain_Debug_Skipping_ForcedAtRank_CMs()
	#ENDIF

ENDPROC




// ===========================================================================================================
//      Local Data Storage Routines
// ===========================================================================================================



// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Update the number of missions played per contact if this mission has been played
//
// INPUT PARAMS:			paramRank					The activity's rank
//							paramRootContentIdHash		The RootContentID Hash value
// RETURN VALUE:			BOOL						TRUE if the contact mission should be classed as 'played', otherwise FALSE
//
// NOTES:	31/1/14 - Updated to search on the rootContentID Hash instead of the contentID Hash
FUNC BOOL Get_Played_Status_For_Contact_Mission(INT paramRank, INT paramRootContentIdHash)


	// I need the player's game rank, but I'm going to use the MissionsAtCoords stored version (directly through the global) to prevent re-calculation
	INT playerRank = g_sAtCoordsControlsMP.matccPlayerRank

	// Not Played if the mission rank is higher than the player's CM Rank
	IF (paramRank > playerRank)
		RETURN FALSE
	ENDIF
	
	// Played if the mission rank is less than the CM Min Rank
	INT cmMinRank = g_CMsPlayed.cmrmMinRank
	IF (paramRank < cmMinRank)
		RETURN TRUE
	ENDIF
	
	INT tempLoop		= 0
	
	REPEAT MAX_CM_CONTENTIDS_PLAYED tempLoop
		IF (g_CMsPlayed.cmrmHashContentIdsComplete[tempLoop] != 0)
			IF (g_CMsPlayed.cmrmHashContentIdsComplete[tempLoop] = paramRootContentIdHash)
				// This mission is stored in the Played Stats, so return 'Played'
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Not found, so not played
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Add Details of a Contact Mission to the local Contact Mission array
//
// INPUT PARAMS:			paramContact			The activity's Contact (or NO_CHARACTER)
//							paramRank				The activity's rank
// REFERENCE PARAMS:		refMissionIdData		Mission Identification Data
//
// NOTES:	Assumes the array was cleared prior to registration and that there are no duplicates, so tag this entry on at the end
//			Also update the number of missions played per contact
//			31/1/14 - Updated to also store the rootContentID Hash as well as the contentID Hash and uses the rootContentID Hash for played mission's STAT storage
//				to preserve the data if the mission updates (since the contentID will change, but the rootContentID won't change)
PROC Add_Contact_Mission_To_Local_Activity_Selector(MP_MISSION_ID_DATA &refMissionIdData, enumCharacterList paramContact, INT paramRank)

	// KGM 18/4/14: CnC needs this too, but some functionality will be either CnC-specific or Freemode-specific, decided within other functions

	// If there are no contact mission details stored yet then clear out the number of contact missions played per contact struct
	IF (g_numLocalMPCMs = 0)
		// KGM 18/4/14: Num Missions Player Per Contact not required for CnC, but no harm clearing it out
		INT tempLoop = 0
		REPEAT MAX_FM_HEIST_CONTACTS tempLoop
			g_cmsPlayedPerContact.cmppcPlayed[tempLoop] = 0
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
			NET_PRINT_TIME()
			NET_PRINT("Clearing out the number of Contact Missions Played per Contact")
			NET_NL()
		#ENDIF
	ENDIF
	
	
	IF g_numLocalMPCMs >= MAX_CONTACT_MISSIONS
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
			NET_PRINT_TIME()
			NET_PRINT("g_numLocalMPCMs >= MAX_CONTACT_MISSIONS. IGNORING.")
			NET_NL()
		#ENDIF
		EXIT
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(refMissionIdData.idCloudFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
			NET_PRINT_TIME()
			NET_PRINT("Request to store Contact Mission with NULL or EMPTY contentID. IGNORING.")
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	INT rootContentIdHash = Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: ContentID: ")
		NET_PRINT(refMissionIdData.idCloudFilename)
		NET_PRINT("          contentIdHash: ")
		NET_PRINT_INT(GET_HASH_KEY(refMissionIdData.idCloudFilename))
		NET_PRINT("          RootContentIdHash: ")
		NET_PRINT_INT(rootContentIdHash)
		NET_NL()
	#ENDIF
	
	IF (rootContentIdHash = 0)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
			NET_PRINT_TIME()
			NET_PRINT("Request to store Contact Mission but RootContentIDHash = 0. IGNORING.")
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	IF GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(rootContentIdHash)
	OR CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(rootContentIdHash)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
			NET_PRINT_TIME()
			NET_PRINT("Request to store Contact Mission but GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(RootContentIDHash). IGNORING.")
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	BOOL					isPlayed			= Get_Played_Status_For_Contact_Mission(paramRank, rootContentIdHash)
	INT						contentIdHash		= GET_HASH_KEY(refMissionIdData.idCloudFilename)
	BOOL					isOnePlay			= Get_One_Play_Status_FM_Cloud_Loaded_Activity(refMissionIdData)
	BOOL					isForcedAtRank		= Get_Forced_At_Rank_Status_FM_Cloud_Loaded_Activity(refMissionIdData)
	INT						theOnHours			= Get_On_Hours_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	g_eFMHeistContactIDs	heistContactID		= Convert_CharacterListID_To_FM_Heist_ContactID(paramContact)

	g_sLocalMPCMs[g_numLocalMPCMs].lcmContact			= heistContactID
	g_sLocalMPCMs[g_numLocalMPCMs].lcmRank				= paramRank
	g_sLocalMPCMs[g_numLocalMPCMs].lcmContentIdHash		= contentIdHash
	g_sLocalMPCMs[g_numLocalMPCMs].lcmRootContentIdHash	= rootContentIdHash
	g_sLocalMPCMs[g_numLocalMPCMs].lcmCreatorID			= refMissionIdData.idCreator
	g_sLocalMPCMs[g_numLocalMPCMs].lcmVariation			= refMissionIdData.idVariation
	g_sLocalMPCMs[g_numLocalMPCMs].lcmOnHours			= theOnHours
	g_sLocalMPCMs[g_numLocalMPCMs].lcmPlayed			= isPlayed
	g_sLocalMPCMs[g_numLocalMPCMs].lcmOnePlay			= isOnePlay
	g_sLocalMPCMs[g_numLocalMPCMs].lcmForcedAtRank		= isForcedAtRank
	
	// TODO: Check that this is OK	
	/*
	IF g_sLocalMPCMs[g_numLocalMPCMs].lcmCreatorID = FMMC_ROCKSTAR_CREATOR_ID
		g_sLocalMPCMs[g_numLocalMPCMs].lcmPlayed	= IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[g_sLocalMPCMs[g_numLocalMPCMs].lcmVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED)
	ELSE
		SCRIPT_ASSERT("[ContactList] Is not played but will be using the alternative play decider")
	ENDIF
	*/
	#IF IS_DEBUG_BUILD
		//store CM mission names for debug use
		g_sMPDebugCMs[g_numLocalMPCMs].CMname	= Get_Mission_Name_For_FM_Cloud_Loaded_Activity(refMissionIdData)
		
		// I can utilise the Pending Activities debug function for output since the same details are being output
		NET_PRINT("...KGM MP [ActSelect][ContactMission]: Adding: ")
		Debug_Output_Pending_Activities_Details_Passed_As_Parameters_On_One_Line(refMissionIdData, MP_CONTACT_MISSION_GENERIC_COORDS, paramContact, paramRank)
		IF (isPlayed) AND NOT IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[g_sLocalMPCMs[g_numLocalMPCMs].lcmVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED)
			NET_PRINT("  - BELOW CM THRESHOLD BUT NOT ACTUALLY PLAYED")
		ELIF IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[g_sLocalMPCMs[g_numLocalMPCMs].lcmVariation].iBitSet, ciROCKSTAR_CREATED_MISSION_HAS_BEEN_PLAYED)
			NET_PRINT("  - REALLY PLAYED")
		ELIF (isPlayed)
			NET_PRINT("  - PLAYED")
		ENDIF
		
		IF (isOnePlay)
			NET_PRINT("   [ONE PLAY ONLY]")
		ENDIF
		TEXT_LABEL_63 onHoursString = Convert_Hours_From_Bitfield_To_TL(theOnHours)
        NET_PRINT("   [ON HOURS: ")	NET_PRINT(onHoursString) NET_PRINT("]")
		NET_NL()
	#ENDIF
	
	// Update the number of missions played per contact
	IF (isPlayed)
		Increment_Number_Of_Missions_Player_For_Contact(heistContactID)
	ENDIF
	
	// If this mission is in the Active CM transition-safe array then re-add it to the joblist
	// NOTE: This may not be the way we end up doing this
	IF (Is_Mission_In_The_Active_CM_Array(refMissionIdData.idCloudFilename))
		// ...in the active list, so re-add the mission and to the joblist
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
			NET_PRINT_TIME()
			NET_PRINT("Found newly setup mission on the Active CM list, so set it up again on MatC and Joblist: ")
			NET_PRINT(refMissionIdData.idCloudFilename)
			NET_NL()
		#ENDIF
		
		// BUT if this is outside the OnHours then delete from the active array instead
		IF (Can_Invite_Be_Displayed_At_This_Hour(theOnHours))
			// ...set up the mission again
			Setup_Contact_Mission_From_CM_Data_And_Send_Invite(refMissionIdData, TRUE, FALSE, TRUE)
		ELSE
			// ...remove the mission from the active list
			#IF IS_DEBUG_BUILD
				NET_PRINT("      BUT: mission is currently outside it's On Hours, so delete from the active array instead. Current Hour: ")
				NET_PRINT_INT(GET_CLOCK_HOURS())
				NET_NL()
			#ENDIF
		
			Remove_This_Mission_If_On_Active_CM_Array(refMissionIdData.idCloudFilename)
		ENDIF
		
		// Update the Player Broadcast array of active ContentIDs so they are available to other players in the session
		Update_Active_CMs_On_Broadcast_Array()
	ENDIF
	
	g_numLocalMPCMs++

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check for a matching entry in the Active CM array and delete it
//
// INPUT PARAMS:		paramContactID			The Mission Contact
//						paramMissionName		The Mission Name string
//
// NOTES:	This was added so that the Invite could be deleted on the Joblist
PROC Contact_Mission_Invite_Deleted(enumCharacterList paramContactID, TEXT_LABEL_63 paramMissionName)

	IF (paramContactID = NO_CHARACTER)
		EXIT
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramMissionName))
		EXIT
	ENDIF

	INT						tempLoop			= 0
	TEXT_LABEL_31			thisContentID		= ""
	TEXT_LABEL_63			thisMissionName		= ""
	MP_MISSION_ID_DATA		thisMissionIdData
	g_eFMHeistContactIDs	thisHeistContactID	= NO_FM_HEIST_CONTACT

	REPEAT MAX_CONTACT_MISSION_INVITES_AT_ONCE tempLoop
		IF (g_lcacmActiveCMs[tempLoop].lcacmContact != NO_FM_HEIST_CONTACT)
			thisHeistContactID = Convert_CharacterListID_To_FM_Heist_ContactID(paramContactID)
			
			IF (thisHeistContactID = g_lcacmActiveCMs[tempLoop].lcacmContact)
				thisMissionIdData	= g_lcacmActiveCMs[tempLoop].lcacmMissionIdData
				thisMissionName		= Get_Mission_Name_For_FM_Cloud_Loaded_Activity(thisMissionIdData)
				
				IF NOT (IS_STRING_NULL_OR_EMPTY(thisMissionName))
					IF (ARE_STRINGS_EQUAL(thisMissionName, paramMissionName))
						thisContentID = thisMissionIdData.idCloudFilename
						
						#IF IS_DEBUG_BUILD
							NET_PRINT("...KGM MP [ActSelect][ContactMission]:")
							NET_PRINT_TIME()
							NET_PRINT("Found matching mission for deleted Invite on Active CM Array: ")
							NET_PRINT(paramMissionName)
							NET_PRINT(" [ContentID: ")
							NET_PRINT(thisMissionIdData.idCloudFilename)
							NET_PRINT("] - REMOVING")
							NET_NL()
						#ENDIF
						
						Remove_This_Mission_If_On_Active_CM_Array(thisContentID)
						
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC




