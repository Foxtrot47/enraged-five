//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_SCRIPT_MANAGEMENT.sch																			//
// Description: Header file containing functionality to launch the AM_MP_PEDS.sc child script. Script termination		//
//				conditions are handled within the SHOULD_PED_SCRIPT_TERMINATE() function inside AM_MP_PEDS.sc			//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		23/05/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
USING "net_peds_creation.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ SCRIPT LAUNCHING ╞═══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Maintains the launching of AM_MP_PEDS.sc
///    Script termination conditions are handled within the SHOULD_PED_SCRIPT_TERMINATE() function inside AM_MP_PEDS.sc
/// PARAMS:
///    ePedLocation - The ped location type. The ped data is populated dependant on the location type.
///    bPedScriptLaunched - Referenced state on whether the ped script has launched.
///    iScriptInstance - Script instance of AM_MP_PEDS.sc to launch. Based on the simple interior script ID for interior peds. Constant defined for non simple interior peds, e.g. PED_SCRIPT_INSTANCE_ISLAND.
///    bQuerySimpleInteriorState - Adds a conditional check for the simple interior state before launching AM_MP_PEDS.sc
///    bConditionalLaunch - Doesn't launch the script automatically. Further conditions are checked before launching.
PROC MAINTAIN_PED_SCRIPT_LAUNCHING(PED_LOCATIONS ePedLocation, BOOL &bPedScriptLaunched, INT iScriptInstance, BOOL bQuerySimpleInteriorState = TRUE, BOOL bConditionalLaunch = FALSE)
	
	// Conditional launching
	IF (bConditionalLaunch AND NOT SHOULD_PED_SCRIPT_LAUNCH(ePedLocation))
		bPedScriptLaunched = FALSE
		EXIT
	ENDIF
	
	// M menu launching
	IF g_bTurnOnPeds
		IF NOT IS_THREAD_ACTIVE(g_PedsThreadID)
			bPedScriptLaunched = FALSE
		ENDIF
	ENDIF
	
	// Default launching
	IF NOT bPedScriptLaunched
		BOOL bLaunchScript = FALSE
		
		IF NOT IS_THREAD_ACTIVE(g_PedsThreadID)
		AND NOT NETWORK_IS_SCRIPT_ACTIVE("AM_MP_PEDS", iScriptInstance, TRUE)
		AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("AM_MP_PEDS")) < 1
		AND NOT NETWORK_IS_ACTIVITY_SESSION()
			bLaunchScript = TRUE
			
			IF (bQuerySimpleInteriorState)
			AND (g_iSimpleInteriorState = SIMPLE_INT_STATE_INITIALISE)
				bLaunchScript = FALSE
			ENDIF
			
			IF (bLaunchScript)	
				REQUEST_SCRIPT("AM_MP_PEDS")
				IF HAS_SCRIPT_LOADED("AM_MP_PEDS")
					SCRIPT_PARENT_DATA Data
					Data.ePedLocation = ePedLocation
					Data.iParentScriptNameHash = GET_HASH_OF_THIS_SCRIPT_NAME()
					Data.iScriptInstance = iScriptInstance
					g_bInitPedsCreated = FALSE // make sure this is set first thing so there is no time for this to be true while the script initializes
					g_PedsThreadID = START_NEW_SCRIPT_WITH_ARGS("AM_MP_PEDS", Data, SIZE_OF(SCRIPT_PARENT_DATA), ACTIVITY_CREATOR_INT_STACK_SIZE)
					SET_SCRIPT_AS_NO_LONGER_NEEDED("AM_MP_PEDS")
					bPedScriptLaunched = TRUE
					#IF IS_DEBUG_BUILD
					PRINTLN("[AM_MP_PEDS] MAINTAIN_PED_SCRIPT_LAUNCHING - Lauching AM_MP_PEDS script - Parent script: ", GET_THIS_SCRIPT_NAME(), " Parent script Hash: ", Data.iParentScriptNameHash, " Ped Location: ", DEBUG_GET_PED_LOCATION_STRING(Data.ePedLocation), " Script Instance: ", iScriptInstance)
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#ENDIF	// FEATURE_HEIST_ISLAND
