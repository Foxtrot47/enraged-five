USING "net_help_text_appender.sch"

USING "net_arena_vehicle_help_text.sch"


/// PURPOSE: 
///    Contains bit sets that keep track of
///    which vehicle help messages have been displayed
STRUCT VEHICLE_HELP_TEXT_BIT_SETS_STRUCT
	
	INT iArenaWarVehicleModHelpDisplayCount[COUNT_OF( ARENA_VEHICLE_MOD_HELP)]
	INT iArenaWarVehicleGeneralHelpBS
	
	
ENDSTRUCT


/// PURPOSE:
///    Displays vehicle help text that displays when the player is not in
///    a vehicle. 
///    Call on foot help displaying functions from within this function.
/// PARAMS:
///    sRegisteredBS - struct holding all registered bit sets
///    viCurrentVehicle - the players current vehicle
///    eCurrentVehicleModel - the current vehicles model
/// RETURNS:
///    TRUE if a help message is being displayed
FUNC BOOL DISPLAY_ON_FOOT_VEHICLE_HELP_TEXT(VEHICLE_HELP_TEXT_BIT_SETS_STRUCT& sRegisteredBS,
VEHICLE_INDEX viCurrentVehicle, MODEL_NAMES eCurrentVehicleModel)
	
	// May not be needed, but here for convenience
	UNUSED_PARAMETER(viCurrentVehicle)
	UNUSED_PARAMETER(eCurrentVehicleModel)
	
	// Arena war on foot help text
	IF TRY_DISPLAY_ARENA_ON_FOOT_VEHICLE_HELP(sRegisteredBS.iArenaWarVehicleGeneralHelpBS)
		RETURN TRUE
	ENDIF
	
	
	// Register other general vehicle help here in the same manner
	
	
	RETURN FALSE
	
ENDFUNC


/// PURPOSE:
///    Displays general vehicle help text for the vehicle the player is in.
///    Call general help displaying functions from within this function.
/// PARAMS:
///    sRegisteredBS - struct holding all registered bit sets
///    viCurrentVehicle - the players current vehicle
///    eCurrentVehicleModel - the current vehicles model
/// RETURNS:
///    TRUE if a help message is being displayed
FUNC BOOL DISPLAY_GENERAL_VEHICLE_HELP_TEXT(VEHICLE_HELP_TEXT_BIT_SETS_STRUCT& sRegisteredBS,
VEHICLE_INDEX viCurrentVehicle, MODEL_NAMES eCurrentVehicleModel)
	
	// May not be needed, but here for convenience
	UNUSED_PARAMETER(viCurrentVehicle)
	UNUSED_PARAMETER(eCurrentVehicleModel)
	
	// Arena war vehicles general help text
	IF TRY_DISPLAY_HELP_TRANSFORM_ARENA_CONTENDER(sRegisteredBS.iArenaWarVehicleGeneralHelpBS,  eCurrentVehicleModel)
		RETURN TRUE
	ENDIF
	
	
	// Register other general vehicle help here in the same manner
	
	
	RETURN FALSE
	
ENDFUNC


/// PURPOSE:
///    Displays vehicle help text specific to the mods installed on the vehicle the player is in.
///    Call mod help displaying functions from within this function.
/// PARAMS:
///    sRegisteredBS - struct holding all registered bit sets
///    viCurrentVehicle - the players current vehicle
///    eCurrentVehicleModel - the current vehicles model
/// RETURNS:
///    TRUE if a help message is being displayed 
FUNC BOOL DISPLAY_VEHICLE_MOD_HELP_TEXT(VEHICLE_HELP_TEXT_BIT_SETS_STRUCT& sRegisteredBS,
VEHICLE_INDEX viCurrentVehicle, MODEL_NAMES eCurrentVehicleModel)
	
	// May not be needed, but here for convenience
	UNUSED_PARAMETER(viCurrentVehicle)
	UNUSED_PARAMETER(eCurrentVehicleModel)
	
	// Arena war vehicles mod help text
	INT iArenaHelpCount = COUNT_OF(ARENA_VEHICLE_MOD_HELP)
	IF TRY_DISPLAY_APPENDED_HELP_TEXTS(iArenaHelpCount,
	sRegisteredBS.iArenaWarVehicleModHelpDisplayCount, &GET_ARENA_VEHICLE_MODS_HELP)
		RETURN TRUE
	ENDIF	
	
	
	// Register other vehicle mod help here in the same manner
	
	
	RETURN FALSE
	
ENDFUNC

