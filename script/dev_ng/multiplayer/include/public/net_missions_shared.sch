USING "rage_builtins.sch"
USING "globals.sch"

USING "net_events.sch"
USING "net_prints.sch"

USING "net_missions_shared_public.sch"
USING "net_missions_shared_cloud_data.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   net_missions_shared.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Provides access to an array of identification data for missions that can be stored for later activation.
//							The missions can be stored in groups with a bitfield recording which players have requested storage.
//							This is not the cloud-loaded header data for these missions, just the details needed to identify where to
//							retrieve the cloud-loaded header data. These details are required for missions that aren't yet registered
//							with the Mission At Coords system but which will need to be in future. The original use of this array was
//							for the Heists and the Contact Point missions. Each potential heist mission is stored on this array and
//							when the player enters their heist corona they get a free choice of which mission from the player's own set
//							of heist missions is going to be launched. The list is drawn from this array, and only when a mission is
//							chosen does it get reigistered with the Missions At Coords system of all players. Similarly, all missions
//							at a Contact Point have their identification details registered here and, from the available missions, one
//							is chosen at random and registered with the Missions At Coords system for all players to play.
//
//		NOTES			:	ARRAY LAYOUT - KGM 24/2/13
//							The array position of an entry is fixed until deleted, so the array is non-continguous.
//
//							MISSONS SHARED renamed as PENDING ACTIVITIES - KGM 28/3/13
//							Renamed as Pending Activites to avoid confusion with Cloud-Loaded Missions Shared array and also to better
//								represent teh purpose of this mission data being stored
//
//							ON-THE-FLY GROUPINGS - KGM 29/3/13
//							A Group is one or more missions within range of a group focus vector. The focus vector will be the start
//							coordinates of the first mission stored and this vector will then become the start coordinates for any
//							missions within a short range of this vector.
//
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      PRIVATE GROUP ACCESS FUNCTIONS
// ===========================================================================================================

// PURPOSE:	Check if a Group contains any missions
//
// INPUT PARAMS:			paramGroupArrayPos		Pending Activities Group Array pos
// RETURN VALUE:			BOOL					TRUE if the group contains missions, FALSE otherwise
FUNC BOOL Does_This_Pending_Activities_Group_Slot_Contain_Any_Missions(INT paramGroupArrayPos)
	RETURN (GlobalServerBD_MissionsShared.groups[paramGroupArrayPos].msgNumInGroup > 0)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Look for an existing group position that gets overlapped by this mission
//
// INPUT PARAMS:			paramLocation			The location of this mission
// RETURN VALUE:			INT						The Group array position containing a matching location, or ILLEGAL_PENDING_ACTIVITIES_SLOT if no match
FUNC INT Get_Group_Array_Position_For_This_Location(VECTOR paramLocation)

	// Ignore a location at the origin
	VECTOR theOrigin	= << 0.0, 0.0, 0.0 >>
	
	IF (ARE_VECTORS_ALMOST_EQUAL(paramLocation, theOrigin))
		RETURN ILLEGAL_PENDING_ACTIVITIES_SLOT
	ENDIF

	// Find an existing nearby group location
	INT	tempLoop	= 0
	
	REPEAT MAX_PENDING_ACTIVITIES_GROUPS tempLoop
		IF (Does_This_Pending_Activities_Group_Slot_Contain_Any_Missions(tempLoop))
			IF (ARE_VECTORS_ALMOST_EQUAL(Get_Group_Coords_For_Group_Slot(tempLoop), paramLocation, PENDING_ACTIVITIES_GROUP_RADIUS_m))
				// Found a matching vector
				RETURN tempLoop
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Not found
	RETURN ILLEGAL_PENDING_ACTIVITIES_SLOT

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Find a free group slot
//
// RETURN VALUE:			INT						A free Group array position, or ILLEGAL_PENDING_ACTIVITIES_SLOT if none available
FUNC INT Find_Free_Pending_Activities_Group_Slot()

	// Find an existing nearby group location
	INT	tempLoop	= 0
	
	REPEAT MAX_PENDING_ACTIVITIES_GROUPS tempLoop
		IF NOT (Does_This_Pending_Activities_Group_Slot_Contain_Any_Missions(tempLoop))
			// Found a free slot
			RETURN tempLoop
		ENDIF
	ENDREPEAT
	
	// Not found
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared]: Find_Free_Pending_Activities_Group_Slot() ERROR - Failed to find a free slot on the array") NET_NL()
		Debug_Output_Pending_Activities_Groups()
		SCRIPT_ASSERT("Find_Free_Pending_Activities_Group_Slot() - ERROR: Failed to find free slot on Pending Activities array. Increase MAX_PENDING_ACTIVITIES_GROUPS. See Console Log. Tell Keith.")
	#ENDIF

	RETURN ILLEGAL_PENDING_ACTIVITIES_SLOT

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Decrement the number of missions in the passed-in slot, setting back to origin if the slot is empty
//
// INPUT PARAMS:			paramGroupArrayPos			The GroupSlot, or ILLEGAL_PENDING_ACTIVITIES_SLOT if no group slot
PROC Decrement_Number_Of_Missions_In_Group(INT paramGroupArrayPos)

	// Has a group slot been passed in?
	IF (paramGroupArrayPos = ILLEGAL_PENDING_ACTIVITIES_SLOT)
		EXIT
	ENDIF
	
	// Ignore this if there are no entries in the slot
	IF NOT (Does_This_Pending_Activities_Group_Slot_Contain_Any_Missions(paramGroupArrayPos))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared]: Decrement_Number_Of_Missions_In_Group() ERROR - There are no entries in this Group: ") NET_PRINT_INT(paramGroupArrayPos) NET_NL()
			Debug_Output_Pending_Activities_Groups()
			SCRIPT_ASSERT("Decrement_Number_Of_Missions_In_Group() - ERROR: There are no entries in this Group. See Console Log. Tell Keith.")
		#ENDIF
	
		EXIT
	ENDIF
	
	// Decrement the number of entries
	GlobalServerBD_MissionsShared.groups[paramGroupArrayPos].msgNumInGroup--
	
	// If there are no entries left for this group, then clean up the slot
	IF NOT (Does_This_Pending_Activities_Group_Slot_Contain_Any_Missions(paramGroupArrayPos))
		GlobalServerBD_MissionsShared.groups[paramGroupArrayPos].msgGroupLocation	= << 0.0, 0.0, 0.0 >>
		GlobalServerBD_MissionsShared.groups[paramGroupArrayPos].msgNumInGroup		= 0
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MShared]: (SERVER) Decrement_Number_Of_Missions_In_Group() Group Slot: ")
		NET_PRINT_INT(paramGroupArrayPos)
		NET_PRINT("  Location: ")
		NET_PRINT_VECTOR(GlobalServerBD_MissionsShared.groups[paramGroupArrayPos].msgGroupLocation)
		NET_PRINT("     - Total Missions: ")
		NET_PRINT_INT(GlobalServerBD_MissionsShared.groups[paramGroupArrayPos].msgNumInGroup)
		NET_NL()
		Debug_Output_Pending_Activities_Groups()
	#ENDIF

ENDPROC




// ===========================================================================================================
//      PRIVATE ACCESS FUNCTIONS
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Array Data Access Routines
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the MissionID Data for a specific Pending Activities slot
FUNC MP_MISSION_ID_DATA Get_Pending_Activities_MissionID_Data_For_Slot(INT paramSlot)
	RETURN (GlobalServerBD_MissionsShared.activities[paramSlot].paMissionIdData)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the CreatorID for a specific Pending Activities slot
//
// INPUT PARAMS:			paramSlot					Pending Activities slot
// RETURN VALUE:			INT							CreatorID in slot
FUNC INT Get_Pending_Activities_CreatorID_For_Slot(INT paramSlot)
	RETURN (GlobalServerBD_MissionsShared.activities[paramSlot].paMissionIdData.idCreator)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the missionVariation for a specific Pending Activities slot
//
// INPUT PARAMS:			paramSlot					Pending Activities slot
// RETURN VALUE:			INT							Variation in slot
FUNC INT Get_Pending_Activities_Variation_For_Slot(INT paramSlot)
	RETURN (GlobalServerBD_MissionsShared.activities[paramSlot].paMissionIdData.idVariation)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the specified player is of a high enough rank to trigger the activity in this Pending Activities slot
//
// INPUT PARAMS:			paramPlayerID				The PlayerID
//							paramSlot					Pending Activities slot
// RETURN VALUE:			INT							Rank in slot
FUNC BOOL Is_Player_Of_High_Enough_Rank_For_Pending_Activity_In_Slot(PLAYER_INDEX paramPlayerID, INT paramSlot)

	INT playerRank = GET_PLAYER_RANK(paramPlayerID)
	
	IF (playerRank = 0)
		playerRank = GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(paramPlayerID), FALSE)
	ENDIF

	RETURN (playerRank >= Get_Pending_Activities_Rank_For_Slot(paramSlot))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if there are any registered players for this Pending Activity
//
// INPUT PARAMS:			paramSlot				The slot on the Pending Activities array
// RETURN VALUE:			BOOL					TRUE if there are registered players, FALSE if no registered players
FUNC BOOL Are_There_Any_Registered_Players_For_Pending_Activity(INT paramSlot)

	IF (GlobalServerBD_MissionsShared.activities[paramSlot].paPlayersReg = ALL_PLAYER_BITS_CLEAR)
		// ...there are no registered players
		RETURN FALSE
	ENDIF
	
	// There are still registered players
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Register a player with a shared mission
//
// INPUT PARAMS:		paramSlot			The slot containing the shared mission
//						paramPlayerID		The Player Index of the player to be registered
PROC Register_Player_With_Pending_Activity(INT paramSlot, PLAYER_INDEX paramPlayerID)

	IF NOT (Is_Pending_Activities_Slot_In_Use(paramSlot))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared]: Register_Player_With_Pending_Activity - ERROR: Passed in array position is not in use. Ignoring. Slot: ") NET_PRINT_INT(paramSlot) NET_NL()
			SCRIPT_ASSERT("Register_Player_With_Pending_Activity(): ERROR - Passed in array position is not in use. Tell Keith.")
		#ENDIF
	
		EXIT
	ENDIF

	IF NOT (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared]: Register_Player_With_Pending_Activity - ERROR: Player is NOT ok. Ignoring.") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	INT playerIndexAsInt = NATIVE_TO_INT(paramPlayerID)
	
	#IF IS_DEBUG_BUILD
		IF (IS_BIT_SET(GlobalServerBD_MissionsShared.activities[paramSlot].paPlayersReg, playerIndexAsInt))
			NET_PRINT("...KGM MP [MShared]: Register_Player_With_Pending_Activity - Player is already registered. Ignoring. Player: ")
			NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
			NET_NL()
			NET_PRINT("      ")
			Debug_Output_Pending_Activities_Slot_Details_On_One_Line(paramSlot)
			NET_NL()
			Debug_Output_Player_Names_From_Bitfield(GlobalServerBD_MissionsShared.activities[paramSlot].paPlayersReg)
			
			EXIT
		ENDIF
	#ENDIF
	
	// Register the player
	SET_BIT(GlobalServerBD_MissionsShared.activities[paramSlot].paPlayersReg, playerIndexAsInt)

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared]: Registered Player with Pending Activity. Slot: ")
		NET_PRINT_INT(paramSlot)
		NET_PRINT(" Player: ")
		NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
		NET_NL()
		NET_PRINT("      ")
		Debug_Output_Pending_Activities_Slot_Details_On_One_Line(paramSlot)
		NET_NL()
		Debug_Output_Player_Names_From_Bitfield(GlobalServerBD_MissionsShared.activities[paramSlot].paPlayersReg)
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Remove a player as a registered player for this Pending Activity
//
// INPUT PARAMS:		paramSlot			Pending Activity slot
//						paramPlayerID		PlayerID of player registering the activity
PROC Remove_Player_As_Registered_With_Pending_Activity(INT paramSlot, PLAYER_INDEX paramPlayerID)

	// Set the player bit for this player
	INT	playerBit	= NATIVE_TO_INT(paramPlayerID)

	// Nothing to do if the player is not registered
	IF NOT (IS_BIT_SET(GlobalServerBD_MissionsShared.activities[paramSlot].paPlayersReg, playerBit))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared]: (SERVER) Remove_Player_As_Registered_With_Pending_Activity() - INFO: Player is not registered for the activity") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	CLEAR_BIT(GlobalServerBD_MissionsShared.activities[paramSlot].paPlayersReg, playerBit)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared]: (Server) Unregister Player for activity: ")
		NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
		NET_PRINT(" (Bit: ")
		NET_PRINT_INT(playerBit)
		NET_PRINT(") - REMAINING REGISTERED PLAYERS (Bitfield value: ")
		NET_PRINT_INT(GlobalServerBD_MissionsShared.activities[paramSlot].paPlayersReg)
		NET_PRINT(")")
		NET_NL()
		Debug_Output_Player_Names_From_Bitfield(GlobalServerBD_MissionsShared.activities[paramSlot].paPlayersReg)
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Look for a free slot on the Pending Activities array
//
// RETURN VALUE:			INT						The array position of a free slot, or ILLEGAL_PENDING_ACTIVITIES_SLOT if array full
//
// NOTES:	This function checks server BD and can be used by both client and server 
FUNC INT Find_Free_Slot_On_Pending_Activities()

	INT tempLoop = 0
	
	REPEAT MAX_PENDING_ACTIVITIES tempLoop
		IF NOT (Is_Pending_Activities_Slot_In_Use(tempLoop))
			// ...found a free slot
			RETURN tempLoop
		ENDIF
	ENDREPEAT
	
	// Failed to find a free slot
	RETURN ILLEGAL_PENDING_ACTIVITIES_SLOT

ENDFUNC




// ===========================================================================================================
//      PUBLIC ACCESS FUNCTIONS
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Missions Data Access
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Look for an existing mission that matches the mission details
//
// INPUT PARAMS:			paramCoords				Activity Location
// REFERENCE PARAMS:		paramCloudFilename		The Cloud Filename
// RETURN VALUE:			INT						The array position of the matching details, or ILLEGAL_PENDING_ACTIVITIES_SLOT
//
// NOTES:	This function checks server BD and can be used by both client and server 
FUNC INT Find_Matching_Activity_On_Pending_Activities(TEXT_LABEL_23 &refCloudFilename, VECTOR paramCoords)

	INT tempLoop = 0
	
	REPEAT MAX_PENDING_ACTIVITIES tempLoop
		IF (Is_Pending_Activities_Slot_In_Use(tempLoop))
			IF	(ARE_VECTORS_ALMOST_EQUAL(GlobalServerBD_MissionsShared.activities[tempLoop].paLocation, paramCoords))
			AND (ARE_STRINGS_EQUAL(GlobalServerBD_MissionsShared.activities[tempLoop].paMissionIdData.idCloudFilename, refCloudFilename))
				// ...found matching data
				RETURN tempLoop
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find a match
	RETURN ILLEGAL_PENDING_ACTIVITIES_SLOT

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get a random pending Activities matching these details
//
// INPUT PARAMS:		paramGroupArrayPos	The Group Array position (or ILLEGAL_PENDING_ACTIVITIES_GROUP to ignore Group Array)
//						paramContactID		The ContactID (or NO_CHARACTER to ignore contactID)
//						paramPlayerBits		Search for all missions from each player in bitfield (or ALL_PLAYER_BITS_CLEAR to ignore players)
//						paramMinRank		The Minimum Rank - Inclusive (or MIN_PENDING_ACTIVITIES_RANK)
//						paramMaxRank		The maximum Rank - Inclusive (or MAX_PENDING_ACTIVITIES_RANK)
// RETURN VALUE:		INT					Array Position of pending activity matching these details, or ILLEGAL_PENDING_ACTIVITIES_SLOT if none
FUNC INT Get_Random_Pending_Activity_Matching_These_Details(INT paramGroupArrayPos, enumCharacterList paramContactID, INT paramPlayerBits, INT paramMinRank, INT paramMaxRank)

	INT tempLoop				= 0
	INT	numPossibleActivities	= 0
	
	INT	possibleActivities[MAX_PENDING_ACTIVITIES]
	
	// Gather a list of all possible activities
	REPEAT MAX_PENDING_ACTIVITIES tempLoop
		IF (Do_Pending_Activities_Slot_Details_Match(tempLoop, paramGroupArrayPos, paramContactID, paramPlayerBits, paramMinRank, paramMaxRank))
			// ...found a match
			possibleActivities[numPossibleActivities] = tempLoop
			numPossibleActivities++
		ENDIF
	ENDREPEAT
	
	// Return an activity
	IF (numPossibleActivities = 0)
		RETURN (ILLEGAL_PENDING_ACTIVITIES_SLOT)
	ENDIF
	
	INT randomActivity = 0
	
	IF (numPossibleActivities = 1)
		randomActivity = 0
	ELSE
		randomActivity = GET_RANDOM_INT_IN_RANGE(0, numPossibleActivities)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared]: Choosing a random activity from list of potential activities matching these details:") NET_NL()
		NET_PRINT("   ")
		IF (paramGroupArrayPos != ILLEGAL_PENDING_ACTIVITIES_GROUP)
			NET_PRINT("   [GROUP SLOT: ") NET_PRINT_INT(paramGroupArrayPos) NET_PRINT("]")
		ENDIF
		IF (paramContactID != NO_CHARACTER)
			NET_PRINT("   [CONTACT: ") NET_PRINT(GET_STRING_FROM_TEXT_FILE(GLOBAL_CHARACTER_SHEET_GET_LABEL(paramContactID))) NET_PRINT("]")
		ENDIF
		IF (paramPlayerBits != ALL_PLAYER_BITS_CLEAR)
			NET_PRINT("   [PLAYER BITS: ") NET_PRINT_INT(paramPlayerBits) NET_PRINT("]")
		ENDIF
		NET_PRINT("   [VALID RANKS: ") NET_PRINT_INT(paramMinRank) NET_PRINT(" - ") NET_PRINT_INT(paramMaxRank) NET_PRINT("]")
		NET_NL()
		REPEAT numPossibleActivities tempLoop
			NET_PRINT("      ")
			Debug_Output_Pending_Activities_Slot_Details_On_One_Line(possibleActivities[tempLoop])
			IF (randomActivity = tempLoop)
				NET_PRINT("   [CHOSEN ACTIVITY]")
			ENDIF
			NET_NL()
		ENDREPEAT
	#ENDIF
	
	RETURN (possibleActivities[randomActivity])

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the next mission slot that contains data that matches the input parameters, or ILLEGAL_PENDING_ACTIVITIES_SLOT if no data
//
// INPUT PARAMS:		paramStartSlot		[DEFAULT = NEW_PENDING_ACTIVITIES_SEARCH] Missions Shared start slot for the search [THIS SLOT IS IGNORED] - use NEW_PENDING_ACTIVITIES_SEARCH to begin at the start of the array
//						paramPlayerBits		[DEFAULT = ALL_PLAYER_BITS_CLEAR] Search for all missions from each player in bitfield
//						paramAllowWrap		[DEFAULT = FALSE] TRUE if the search can loop round to the start of the array again, otherwise FALSE
// RETURN VALUE:		INT					A slot containing matching mission details, or ILLEGAL_PENDING_ACTIVITIES_SLOT if none
FUNC INT Get_Next_Missions_Shared_Slot_For_These_Details(	INT		paramStartSlot	= NEW_PENDING_ACTIVITIES_SEARCH,
															INT		paramPlayerBits	= ALL_PLAYER_BITS_CLEAR,
															BOOL	paramAllowWrap	= FALSE)

	// Anything to search for?
	IF	(paramPlayerBits	= ALL_PLAYER_BITS_CLEAR)
		// ...no
		RETURN ILLEGAL_PENDING_ACTIVITIES_SLOT
	ENDIF
	
	// Prepare the search
	INT		thisSearchSlot	= paramStartSlot
	INT		endSearch		= MAX_PENDING_ACTIVITIES
	BOOL	hasWrapped		= FALSE
	
	WHILE (TRUE)
		thisSearchSlot++
		
		// End of search?
		IF (thisSearchSlot >= endSearch)
			IF (hasWrapped)
				// ...search has already wrapped, so no missions found
				RETURN ILLEGAL_PENDING_ACTIVITIES_SLOT
			ENDIF
			
			// Allowed to wrap?
			IF NOT (paramAllowWrap)
				// ...search isn't allowed to wrap, so no missions found
				RETURN ILLEGAL_PENDING_ACTIVITIES_SLOT
			ENDIF
			
			// Wrap the search
			hasWrapped		= TRUE
			thisSearchSlot	= 0
			endSearch		= paramStartSlot + 1
		ENDIF
		
		// Check this slot for matching details
		IF (Do_Pending_Activities_Slot_Details_Match(thisSearchSlot, ILLEGAL_PENDING_ACTIVITIES_GROUP, NO_CHARACTER, paramPlayerBits, MIN_PENDING_ACTIVITIES_RANK, MAX_PENDING_ACTIVITIES_RANK))
			RETURN thisSearchSlot
		ENDIF
	ENDWHILE
	
	// Won't reach here
	RETURN ILLEGAL_PENDING_ACTIVITIES_SLOT

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the previous mission slot that contains data that matches the input parameters, or ILLEGAL_PENDING_ACTIVITIES_SLOT if no data
//
// INPUT PARAMS:		paramStartSlot		[DEFAULT = NEW_PENDING_ACTIVITIES_SEARCH] Missions Shared start slot for the search [THIS SLOT IS IGNORED] - use NEW_PENDING_ACTIVITIES_SEARCH to begin at the end of the array
//						paramPlayerBits		[DEFAULT = ALL_PLAYER_BITS_CLEAR] Search for all missions from each player in bitfield
//						paramAllowWrap		[DEFAULT = FALSE] TRUE if the search can loop round to the end of the array again, otherwise FALSE
// RETURN VALUE:		INT					A slot containing matching mission details, or ILLEGAL_PENDING_ACTIVITIES_SLOT if none
FUNC INT Get_Previous_Missions_Shared_Slot_For_These_Details(	INT		paramStartSlot	= NEW_PENDING_ACTIVITIES_SEARCH,
																INT		paramPlayerBits	= ALL_PLAYER_BITS_CLEAR,
																BOOL	paramAllowWrap	= FALSE)

	// Anything to search for?
	IF	(paramPlayerBits	= ALL_PLAYER_BITS_CLEAR)
		// ...no
		RETURN ILLEGAL_PENDING_ACTIVITIES_SLOT
	ENDIF
	
	// Used better-named NEW_PENDING_ACTIVITIES_SEARCH as an input value for consistency, but it should actually be MAX_PENDING_ACTIVITIES - so change it if necessary
	IF (paramStartSlot = NEW_PENDING_ACTIVITIES_SEARCH)
		paramStartSlot = MAX_PENDING_ACTIVITIES
	ENDIF
	
	// Prepare the search
	INT		thisSearchSlot	= paramStartSlot
	INT		endSearch		= -1
	BOOL	hasWrapped		= FALSE
	
	WHILE (TRUE)
		thisSearchSlot--
		
		// End of search?
		IF (thisSearchSlot <= endSearch)
			IF (hasWrapped)
				// ...search has already wrapped, so no missions found
				RETURN ILLEGAL_PENDING_ACTIVITIES_SLOT
			ENDIF
			
			// Allowed to wrap?
			IF NOT (paramAllowWrap)
				// ...search isn't allowed to wrap, so no missions found
				RETURN ILLEGAL_PENDING_ACTIVITIES_SLOT
			ENDIF
			
			// Wrap the search
			hasWrapped		= TRUE
			thisSearchSlot	= MAX_PENDING_ACTIVITIES - 1
			endSearch		= paramStartSlot - 1
		ENDIF
		
		// Check this slot for matching details
		IF (Do_Pending_Activities_Slot_Details_Match(thisSearchSlot, ILLEGAL_PENDING_ACTIVITIES_GROUP, NO_CHARACTER, paramPlayerBits, MIN_PENDING_ACTIVITIES_RANK, MAX_PENDING_ACTIVITIES_RANK))
			RETURN thisSearchSlot
		ENDIF
	ENDWHILE
	
	// Won't reach here
	RETURN ILLEGAL_PENDING_ACTIVITIES_SLOT

ENDFUNC




// ===========================================================================================================
//      EVENT BROADCAST AND PROCESS FUNCTIONS
// ===========================================================================================================

// PURPOSE:	Get the locally known Host as player bits (or 'all_players' if host is unknown)
//
// RETURN VALUE:			INT			PlayerBit for host
//
// NOTES:	This will use g_sPALocalClient.lcpaHost if known
FUNC INT Get_Known_Pending_Activities_Host_Player_Bit()

	IF (g_sPALocalClient.lcpaHost = INVALID_PLAYER_INDEX())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared]: Get_Known_Pending_Activities_Host_Player_Bit(): Locally Known Host is invalid. Returning ALL_PLAYERS") NET_NL()
		#ENDIF
		
		RETURN (ALL_PLAYERS())
	ENDIF
	
	IF NOT (IS_NET_PLAYER_OK(g_sPALocalClient.lcpaHost, FALSE))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared]: Get_Known_Pending_Activities_Host_Player_Bit(): Locally Known Host is not OK. Returning ALL_PLAYERS") NET_NL()
		#ENDIF
		
		RETURN (ALL_PLAYERS())
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared]: Get_Known_Pending_Activities_Host_Player_Bit(): Locally Known Host is ")
		NET_PRINT(GET_PLAYER_NAME(g_sPALocalClient.lcpaHost))
		NET_NL()
	#ENDIF
		
	RETURN (SPECIFIC_PLAYER(g_sPALocalClient.lcpaHost))

ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Event: Add Activity To Pending Activities
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventAddActivityToPendingActivities
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	MP_MISSION_ID_DATA			MissionIdData			// The Mission Identification Data
	VECTOR						Location				// The Activity Location - this can be different from it's cloud-loaded start pos
	enumCharacterList			Contact					// The Activity's Contact (or NO_CHARACTER)
	INT							Rank					// The Activity's Rank
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Broadcast details of an activity to be stored on the Pending Activities array
//
// INPUT PARAMS:			paramCoords				The mission's coords
//							paramContact			The contact ID
//							paramRank				The Mission Rank
// REFERENCE PARAMS:		refMissionIdData		Mission Identification Data
PROC Broadcast_Add_Activity_To_Pending_Activities(MP_MISSION_ID_DATA &refMissionIdData, VECTOR paramCoords, enumCharacterList paramContact, INT paramRank)
	
	// Store the Broadcast data
	m_structEventAddActivityToPendingActivities Event
	
	Event.Details.Type				= SCRIPT_EVENT_ADD_ACTIVITY_TO_PENDING_ACTIVITIES
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.MissionIdData				= refMissionIdData
	Event.Location					= paramCoords
	Event.Contact					= paramContact
	Event.Rank						= paramRank
	
	// Broadcast the event to the known host
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_Known_Pending_Activities_Host_Player_Bit())

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared]: ") NET_PRINT_TIME() NET_PRINT(" Broadcast_Add_Activity_To_Pending_Activities: ")
		Debug_Output_Pending_Activities_Details_Passed_As_Parameters_On_One_Line(refMissionIdData, paramCoords, paramContact, paramRank)
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process a request to add an activity to the Pending Activities array
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Add_Activity_To_Pending_Activities(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MShared]: Process_Event_Add_Activity_To_Pending_Activities - IGNORED (not the host)") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MShared]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Add_Activity_To_Pending_Activities") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventAddActivityToPendingActivities theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Add_Activity_To_Pending_Activities(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Output details
	#IF IS_DEBUG_BUILD
		NET_PRINT("      ")
		Debug_Output_Pending_Activities_Details_Passed_As_Parameters_On_One_Line(theEventData.MissionIdData, theEventData.Location, theEventData.Contact, theEventData.Rank)
		NET_NL()
	#ENDIF
	
	// Find a Group Array Position
	VECTOR	groupLocation	= theEventData.Location
	INT		groupArrayPos	= Get_Group_Array_Position_For_This_Location(theEventData.Location)
	
	IF (groupArrayPos != ILLEGAL_PENDING_ACTIVITIES_SLOT)
		groupLocation = Get_Group_Coords_For_Group_Slot(groupArrayPos)
	ENDIF
	
	// Ignore if the activity already exists
	INT existingSlot = Find_Matching_Activity_On_Pending_Activities(theEventData.MissionIdData.idCloudFilename, groupLocation)
	IF (existingSlot != ILLEGAL_PENDING_ACTIVITIES_SLOT)
		// ...details already exist
		#IF IS_DEBUG_BUILD
			NET_PRINT("      EXISTS: Registering Player with existing entry with matching details. Slot: ") NET_PRINT_INT(existingSlot) NET_NL()
		#ENDIF
		
		// Register this player on existing activity
		Register_Player_With_Pending_Activity(existingSlot, theEventData.Details.FromPlayerIndex)
		
		EXIT
	ENDIF
	
	// Find a free slot on the Pending Activities array
	INT freeSlot = Find_Free_Slot_On_Pending_Activities()
	IF (freeSlot = ILLEGAL_PENDING_ACTIVITIES_SLOT)
		// ...failed to find a free slot
		#IF IS_DEBUG_BUILD
			NET_PRINT("      ERROR: Ignoring Mission. Failed to find a free slot on the array") NET_NL()
//			Debug_Output_Pending_Activities()
			SCRIPT_ASSERT("Process_Event_Add_Activity_To_Pending_Activities() - ERROR: Failed to find free slot on Pending Activities array. Increase MAX_PENDING_ACTIVITIES. See Console Log. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Update the Group Details
	IF (groupArrayPos = ILLEGAL_PENDING_ACTIVITIES_SLOT)
		// ...this is a new group
		groupArrayPos = Find_Free_Pending_Activities_Group_Slot()
		
		// If valid group, store this location as the new group's focus position and set the number of missions to 0
		IF (groupArrayPos != ILLEGAL_PENDING_ACTIVITIES_SLOT)
			GlobalServerBD_MissionsShared.groups[groupArrayPos].msgGroupLocation	= groupLocation
			GlobalServerBD_MissionsShared.groups[groupArrayPos].msgNumInGroup		= 1
			
			#IF IS_DEBUG_BUILD
				NET_NL() NET_PRINT("...KGM MP [MShared]: (SERVER) Storing Activity as first activity for Group Slot: ")
				NET_PRINT_INT(groupArrayPos)
				NET_PRINT("  Location: ")
				NET_PRINT_VECTOR(groupLocation)
				NET_NL()
			#ENDIF
		ENDIF
	ELSE
		// Increase the number of missions associated with this group
		GlobalServerBD_MissionsShared.groups[groupArrayPos].msgNumInGroup++
			
		#IF IS_DEBUG_BUILD
			NET_NL() NET_PRINT("...KGM MP [MShared]: (SERVER) Group Slot: ")
			NET_PRINT_INT(groupArrayPos)
			NET_PRINT("  Location: ")
			NET_PRINT_VECTOR(groupLocation)
			NET_PRINT("     - Total Missions: ")
			NET_PRINT_INT(GlobalServerBD_MissionsShared.groups[groupArrayPos].msgNumInGroup)
			NET_NL()
		#ENDIF
	ENDIF
	
	// Found a free slot, so store the mission details
	GlobalServerBD_MissionsShared.activities[freeSlot].paInUse				= TRUE
	GlobalServerBD_MissionsShared.activities[freeSlot].paMissionIdData		= theEventData.MissionIdData
	GlobalServerBD_MissionsShared.activities[freeSlot].paLocation			= groupLocation
	GlobalServerBD_MissionsShared.activities[freeSlot].paContact			= theEventData.Contact
	GlobalServerBD_MissionsShared.activities[freeSlot].paRank				= theEventData.Rank
	GlobalServerBD_MissionsShared.activities[freeSlot].paGroupSlot			= groupArrayPos
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Storing New Details on Pending Activities array. Slot: ") NET_PRINT_INT(freeSlot) NET_NL()
	#ENDIF
		
	// Register this player on existing mission
	Register_Player_With_Pending_Activity(freeSlot, theEventData.Details.FromPlayerIndex)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Ready To Send Pending Activities - Received by Server from Players
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Server is being told that a player is ready to send Pending Activities
//
// INPUT PARAMS:		paramPlayerID			The Player with data ready to send to server
PROC Process_Event_Ready_To_Send_Pending_Activities(PLAYER_INDEX paramPlayerID)

	// Ignore if not the server
	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MShared]: Process_Event_Ready_To_Send_Pending_Activities - IGNORED (not the host). From: ")
			NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
			NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL()
		NET_PRINT("...KGM MP [MShared]: ")
		NET_PRINT_TIME()
		NET_PRINT("(SERVER) Process_Event_Ready_To_Send_Pending_Activities from ")
		NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
		NET_NL()
	#ENDIF
	
	// Set the player's bit on the Ready To Send bitfield
	INT playerBit = NATIVE_TO_INT(paramPlayerID)
	SET_BIT(GlobalServerBD_MissionsShared.playerBitsReadyToSend, playerBit)
	
	// Output the current players Ready To Send
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Current List Of Players Ready To Send Data to Server:") NET_NL()
		Debug_Output_Player_Names_From_Bitfield(GlobalServerBD_MissionsShared.playerBitsReadyToSend)
	#ENDIF

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Ok To Send Pending Activities - Sent By Server to a Specific Player
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Informs a player that it is ok to start sending Pending Activity data to the server
//
// INPUT PARAMS:		paramHostPlayerID			The current Host PlayerID
PROC Process_Event_Ok_To_Send_Pending_Activities(PLAYER_INDEX paramHostPlayerID)
	
	#IF IS_DEBUG_BUILD
		NET_NL()
		NET_PRINT("...KGM MP [MShared]: ")
		NET_PRINT_TIME()
		NET_PRINT(" Process_Event_Ok_To_Send_Pending_Activities from Host: ")
		NET_PRINT(GET_PLAYER_NAME(paramHostPlayerID))
		NET_NL()
	#ENDIF
	
	// Set the flag to be ready by the FMMC_Launcher initialisation routine
	g_sPALocalClient.lcpaStartSendingData	= TRUE
	
	// Store the Host player
	g_sPALocalClient.lcpaHost = paramHostPlayerID

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Pending Activities All Sent - Received by Server from Player
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Server is being told that a player has sent all their Pending Activities
//
// INPUT PARAMS:		paramPlayerID			The Player with data ready to send to server
PROC Process_Event_All_Pending_Activities_Sent(PLAYER_INDEX paramPlayerID)

	#IF NOT IS_DEBUG_BUILD
		UNUSED_PARAMETER(paramPlayerID)
	#ENDIF

	// Ignore if not the server
	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MShared]: Process_Event_All_Pending_Activities_Sent - IGNORED (not the host). From: ")
			NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
			NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL()
		NET_PRINT("...KGM MP [MShared]: ")
		NET_PRINT_TIME()
		NET_PRINT(" (SERVER) Process_Event_All_Pending_Activities_Sent from ")
		NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
		NET_PRINT(" - Clearing Timeout timer so a new player can be selected")
		NET_NL()
	#ENDIF
	
	// Clear the Ready To Send timer so that a new player will be chosen next update if there is one waiting to send data
	GlobalServerBD_MissionsShared.currentPlayerReadyToSendTimeout = GET_NETWORK_TIME()
	
	#IF IS_DEBUG_BUILD
		Debug_Output_Pending_Activities()
		Debug_Output_Pending_Activities_Groups()
	#ENDIF

ENDPROC




// ===========================================================================================================
//      Server Initialisation and Maintenance Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//     Server Initialisation
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear one Pending Activity slot
//
// INPUT PARAMS:		paramSlot			The array position on the Pending Activities array to clear
PROC Clear_One_Pending_Activities_Slot(INT paramSlot)

	g_structPendingActivities	emptyMission
	GlobalServerBD_MissionsShared.activities[paramSlot] = emptyMission
	
	Clear_MP_MISSION_ID_DATA_Struct(GlobalServerBD_MissionsShared.activities[paramSlot].paMissionIdData)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Initialise the Missions Shared array into a default starting state
PROC Initialise_MP_Missions_Shared_Server()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared]: Initialise_MP_Missions_Shared_Server") NET_NL()
	#ENDIF
	
	// Clear the player bits for players ready to register their Pending Activities
	GlobalServerBD_MissionsShared.playerBitsReadyToSend				= ALL_PLAYER_BITS_CLEAR
	GlobalServerBD_MissionsShared.currentPlayerReadyToSendTimeout	= GET_NETWORK_TIME()
	
	INT tempLoop = 0
	
	// Clear the Missions BD array
	REPEAT MAX_PENDING_ACTIVITIES tempLoop
		Clear_One_Pending_Activities_Slot(tempLoop)
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("          Pending Activities Array Size: ")
		NET_PRINT_INT((SIZE_OF(g_structPendingActivities) * MAX_PENDING_ACTIVITIES))
		NET_PRINT("   [for Info: size of an INT is: ")
		NET_PRINT_INT(SIZE_OF(tempLoop))
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
	// Clear the Groups BD array
	g_structPendingActivityGroups	emptyGroup
	
	REPEAT MAX_PENDING_ACTIVITIES_GROUPS tempLoop
		GlobalServerBD_MissionsShared.groups[tempLoop] = emptyGroup
		GlobalServerBD_MissionsShared.groups[tempLoop].msgGroupLocation		= << 0.0, 0.0, 0.0 >>
	ENDREPEAT
	
	// Initialise the Missions Shared Cloud Data array
	Initialise_MP_Missions_Shared_Cloud_Data_Server()
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("          Pending Activities Groups Array Size: ")
		NET_PRINT_INT((SIZE_OF(g_structPendingActivityGroups) * ENUM_TO_INT(MAX_PENDING_ACTIVITIES_GROUPS)))
		NET_PRINT("   [for Info: size of an INT is: ")
		NET_PRINT_INT(SIZE_OF(tempLoop))
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
	// This is local global server data that doesn't need to be preserved on host migration - initialise it to a decent starting default
	g_structPAServerControlData	emptyServerControlStruct
	g_sPACloudControlServer = emptyServerControlStruct

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//     Server Maintenance
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the 'Ready To Send' timer has expired
//
// RETURN VALUE:		BOOL			TRUE if the timer has expired, otherwise FALSE
FUNC BOOL Has_Current_Players_Ready_To_Send_Timer_Expired()
	RETURN (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GlobalServerBD_MissionsShared.currentPlayerReadyToSendTimeout))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the 'Ready To Send' timer
PROC Set_Ready_To_Send_Timer_Timeout()
	GlobalServerBD_MissionsShared.currentPlayerReadyToSendTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), PENDING_ACTIVITIES_DATA_SEND_TIMEOUT_msec)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the list of players currently waiting to send data to the server
PROC Maintain_Players_Waiting_To_Send_MP_Pending_Activities()

	// Are there any players waiting to send?
	IF (GlobalServerBD_MissionsShared.playerBitsReadyToSend = ALL_PLAYER_BITS_CLEAR)
		EXIT
	ENDIF

	// Has the current sending player's timeout expired?
	IF NOT (Has_Current_Players_Ready_To_Send_Timer_Expired())
		EXIT
	ENDIF
	
	// Current player's timeout has expired, so choose another player from the list
	INT				thisPlayerBit	= 0
	PLAYER_INDEX	thePlayer		= INVALID_PLAYER_INDEX()
	
	REPEAT NUM_NETWORK_PLAYERS thisPlayerBit
		IF (IS_BIT_SET(GlobalServerBD_MissionsShared.playerBitsReadyToSend, thisPlayerBit))
			// ...found a player ready to send data
			thePlayer = INT_TO_PLAYERINDEX(thisPlayerBit)
			
			// If the player is OK and in the Running state then the data can be sent
			IF (IS_NET_PLAYER_OK(thePlayer, FALSE))
				// Remove the player from the list
				CLEAR_BIT(GlobalServerBD_MissionsShared.playerBitsReadyToSend, thisPlayerBit)
			
				// Let the player know it is ok to send the Pending Activity data
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MShared]: ")
					NET_PRINT_TIME()
					NET_PRINT("(SERVER) Let player know it is ok to start sending Pending Activities data: ")
					NET_PRINT(GET_PLAYER_NAME(thePlayer))
					NET_NL()
				#ENDIF
				
				BROADCAST_GENERAL_EVENT(GENERAL_EVENT_OK_TO_SEND_PENDING_ACTIVITIES, SPECIFIC_PLAYER(thePlayer))
				
				// Restart the timer
				Set_Ready_To_Send_Timer_Timeout()

				EXIT
			ELSE
				// The player is not OK and in the running state, so delay this player if ok but not in the running state, otherwise clear the bit because the player has gone
// KEITH TEMP 12/3/14: Don't do this check - it seemed to fail a lot and will be redundant when I make the data client-based
//				IF NOT (IS_NET_PLAYER_OK(thePlayer, FALSE, FALSE))
//					// Remove the player from the list, must have gone from the game
//					CLEAR_BIT(GlobalServerBD_MissionsShared.playerBitsReadyToSend, thisPlayerBit)
//				
//					// Let the player know it is ok to send the Pending Activity data
//					#IF IS_DEBUG_BUILD
//						NET_PRINT("...KGM MP [MShared]: ")
//						NET_PRINT_TIME()
//						NET_PRINT("(SERVER) Player seems to have gone from the game so don't wait for data. Player Bit: ")
//						NET_PRINT_INT(thisPlayerBit)
//						NET_NL()
//					#ENDIF
//				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the next scheduled pending activity on server
PROC Maintain_MP_Pending_Activities_Data_Server()

	// Get the next activity to maintain
	IF (g_sPACloudControlServer.paccNextInSchedule >= MAX_PENDING_ACTIVITIES)
		g_sPACloudControlServer.paccNextInSchedule = 0
	ENDIF
	
	INT arrayPosToMaintain	= g_sPACloudControlServer.paccNextInSchedule
	
	// Always, increment to the next mission for the next frame's maintenance
	g_sPACloudControlServer.paccNextInSchedule++
	
	// Does this array position have data to maintain?
	IF NOT (Is_Pending_Activities_Slot_In_Use(arrayPosToMaintain))
		// Nothing to do
		EXIT
	ENDIF	
	
	// This array position contains data, so check if any registered players for the mission are no longer in the game
	INT				tempLoop			= 0
	INT				regPlayersBitset	= GlobalServerBD_MissionsShared.activities[arrayPosToMaintain].paPlayersReg
	PLAYER_INDEX	thisPlayerID		= INVALID_PLAYER_INDEX()
	
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(regPlayersBitset, tempLoop))
			// ...found a registered player
			thisPlayerID = INT_TO_PLAYERINDEX(tempLoop)
			
			IF NOT (IS_NET_PLAYER_OK(thisPlayerID, FALSE))
				// Player is no longer in the game, so unregister the player
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MShared]: Player registered for Pending Activity slot has left the game. Slot: ")
					NET_PRINT_INT(arrayPosToMaintain)
					NET_NL()
				#ENDIF
				
				Remove_Player_As_Registered_With_Pending_Activity(arrayPosToMaintain, thisPlayerID)
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Are there any players still registered with the mission
	IF NOT (Are_There_Any_Registered_Players_For_Pending_Activity(arrayPosToMaintain))
		// ...there are no registered players remaining, so remove from the Group Slot and delete the entry
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared]: (SERVER) Pending Activity slot now has no registered players, so clearing the slot. Slot: ")
			NET_PRINT_INT(arrayPosToMaintain)
			NET_NL()
		#ENDIF
		
		INT groupSlot = GlobalServerBD_MissionsShared.activities[arrayPosToMaintain].paGroupSlot
		Decrement_Number_Of_Missions_In_Group(groupSlot)
		
		Clear_One_Pending_Activities_Slot(arrayPosToMaintain)
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("   Updated Pending Activities Groups Array Details") NET_NL()
			Debug_Output_Pending_Activities_Groups()
		#ENDIF
		
		EXIT
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Maintain the Mission Shared array
PROC Maintain_MP_Missions_Shared_Server()
	
	// Maintain the array of players waiting to send Pending Activity data to the server
	Maintain_Players_Waiting_To_Send_MP_Pending_Activities()
	
	// Maintain the Pending Activities Data Array
	Maintain_MP_Pending_Activities_Data_Server()
	
	// Maintain the Missions Shared Cloud Data array
	Maintain_MP_Missions_Shared_Cloud_Data_Server()

ENDPROC




// ===========================================================================================================
//      Client Initialisation and Maintenance Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//     Client Initialisation
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Initialise any Missions Shared client data
PROC Initialise_MP_Missions_Shared_Client()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared]: Initialise_MP_Missions_Shared_Client") NET_NL()
	#ENDIF
	
	// Initialise the Missions Shared Cloud Data array
	Initialise_MP_Missions_Shared_Cloud_Data_Client()
	
	// This is local global server data that doesn't need to be preserved on host migration - initialise it to a decent starting default in case this machine becomes host
	g_structPAServerControlData	emptyServerControlStruct
	g_sPACloudControlServer = emptyServerControlStruct
	
	// Clear the local global client data
	g_structLocalClientPendingActivities emptyLocalClientPA
	
	g_sPALocalClient			= emptyLocalClientPA
	g_sPALocalClient.lcpaHost	= INVALID_PLAYER_INDEX()
	
	// Also clear out the generated cloud header data struct
	Clear_Generated_Cloud_Header_Data()

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//     Client Maintenance
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Maintain the Mission Shared client functionality
PROC Maintain_MP_Missions_Shared_Client()
	
	// Maintain the Missions Shared Cloud Data array
	Maintain_MP_Missions_Shared_Cloud_Data_Client()

ENDPROC







