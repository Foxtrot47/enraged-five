USING "rage_builtins.sch"
USING "globals.sch"
USING "title_update_globals.sch"
USING "net_include.sch"

#IF IS_DEBUG_BUILD
//	USING "Net_Missions_At_Coords_Debug.sch"
	USING "fm_mission_info.sch"
#ENDIF

 

// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Missions_At_Coords_Public_Checks.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Handles cyclic-header free public access to the MP Mission At Coords routines.
//		NOTES			:	Directly accesses global array data rather than through functions.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      Mission At Coords Data Refresh Public Access Routines
// ===========================================================================================================

// PURPOSE:	Check if all cloud-loaded data has been downloaded
FUNC BOOL Is_Mission_At_Coords_Cloud_Loaded_Data_Being_Downloaded()
	RETURN (g_sAtCoordsControlsMP.matccDownloadInProgress)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a cloud-loaded data refresh is ongoing - this also encompasses when the data is being downloaded
FUNC BOOL Is_Cloud_Loaded_Mission_Data_Being_Refreshed()
	RETURN (g_sAtCoordsControlsMP.matccRefreshInProgress)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the initial storage of cloud-loaded data has been received
FUNC BOOL Is_Initial_Cloud_Loaded_Mission_Data_Ready()
	RETURN (g_sAtCoordsControlsMP.matccInitialDataReady)
ENDFUNC




// added by Neil for optimisations
PROC Insert_Into_Nearest_Coords_Array(SPAWN_SEARCH_CORONA &paramToInsert, SPAWN_SEARCH_CORONA &NearestCoords[], INT iSlot)
	//SPAWN_SEARCH_CORONA SavedAtCoords
	gSavedAtCoords =  NearestCoords[iSlot]
	NearestCoords[iSlot] = paramToInsert
	IF (iSlot+1 < COUNT_OF(NearestCoords))
		Insert_Into_Nearest_Coords_Array(gSavedAtCoords, NearestCoords, iSlot+1)
	ENDIF
ENDPROC

// added by Neil for optimisations
PROC Insert_Into_Nearest_Angled_Area_Array(SPAWN_SEARCH_GANG_ATTACK &paramToInsert, SPAWN_SEARCH_GANG_ATTACK &NearestGangAttack[], INT iSlot)
	//SPAWN_SEARCH_GANG_ATTACK SavedAtAngledArea
	gSavedAtAngledArea =  NearestGangAttack[iSlot]
	NearestGangAttack[iSlot] = paramToInsert
	IF (iSlot+1 < COUNT_OF(NearestGangAttack))
		Insert_Into_Nearest_Angled_Area_Array(gSavedAtAngledArea, NearestGangAttack, iSlot+1)
	ENDIF
ENDPROC

// added by Neil for optimisations
PROC Get_Nearest_Mission_Coronas_And_Gang_Attacks_To_Point(VECTOR paramCoords, SPAWN_SEARCH_CORONA &NearestCoords[], SPAWN_SEARCH_GANG_ATTACK &NearestGangAttack[])


	INT iNearestLoop	
	INT iBigLoop
	INT iBigLoop2
	SPAWN_SEARCH_CORONA InsertCorona
	SPAWN_SEARCH_GANG_ATTACK InsertAngledArea
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("Get_Nearest_Mission_Coronas_And_Gang_Attacks_To_Point - called with point ") NET_PRINT_VECTOR(paramCoords) NET_NL()
	#ENDIF
	
	// make sure the current nearest coords and angled areas are zero'd
	REPEAT COUNT_OF(NearestCoords) iNearestLoop
		NearestCoords[iNearestLoop] = InsertCorona	
	ENDREPEAT
	REPEAT COUNT_OF(NearestGangAttack) iNearestLoop
		NearestGangAttack[iNearestLoop] = InsertAngledArea	
	ENDREPEAT	

	
	// get neareast coords first
	iNearestLoop = 0
	iBigLoop = 0
	REPEAT g_numAtCoordsMP iBigLoop
		REPEAT COUNT_OF(NearestCoords) iNearestLoop
			IF NOT (IS_BIT_SET(g_sAtCoordsMP[iBigLoop].matcOptionBitflags, MATC_BITFLAG_AREA_TRIGGERED))
				IF (VMAG(NearestCoords[iNearestLoop].vCoords) = 0.0)
				OR (VDIST(g_sAtCoordsMP[iBigLoop].matcCoords, paramCoords) < VDIST(NearestCoords[iNearestLoop].vCoords, paramCoords))
					
					InsertCorona.vCoords = g_sAtCoordsMP[iBigLoop].matcCoords
					InsertCorona.fRadius =  g_sAtCoordsMP[iBigLoop].matcCorona.triggerRadius
					
					Insert_Into_Nearest_Coords_Array(InsertCorona, NearestCoords, iNearestLoop)
					iNearestLoop = COUNT_OF(NearestCoords) 
				ENDIF
			ENDIF
		ENDREPEAT	
	ENDREPEAT
	
	iNearestLoop = 0
	iBigLoop = 0
	REPEAT ciMAX_V2_CORONAS iBigLoop
		REPEAT COUNT_OF(NearestCoords) iNearestLoop
			IF (VMAG(NearestCoords[iNearestLoop].vCoords) = 0.0)
			OR (VDIST(g_sMPTunables.iFmCoronaPlaylistLocation[iBigLoop], paramCoords) < VDIST(NearestCoords[iNearestLoop].vCoords, paramCoords))
				
				InsertCorona.vCoords = g_sMPTunables.iFmCoronaPlaylistLocation[iBigLoop]
				InsertCorona.fRadius =  3.0
				
				Insert_Into_Nearest_Coords_Array(InsertCorona, NearestCoords, iNearestLoop)
				iNearestLoop = COUNT_OF(NearestCoords) 
			ENDIF
		ENDREPEAT	
	ENDREPEAT

	iNearestLoop = 0
	iBigLoop = 0
	iBigLoop2 = 0
	REPEAT ciMAX_V2_PROFESSIONAL_CORONAS iBigLoop
		REPEAT ciMAX_V2_PROFESSIONAL_CORONAS_LOCATIONS iBigLoop2
			REPEAT COUNT_OF(NearestCoords) iNearestLoop
				IF (VMAG(NearestCoords[iNearestLoop].vCoords) = 0.0)
				OR (VDIST(g_sMPTunables.iFmCoronaPlaylistProfessionalLocation[iBigLoop][iBigLoop2], paramCoords) < VDIST(NearestCoords[iNearestLoop].vCoords, paramCoords))
					
					InsertCorona.vCoords = g_sMPTunables.iFmCoronaPlaylistProfessionalLocation[iBigLoop][iBigLoop2]
					InsertCorona.fRadius =  3.0
					
					Insert_Into_Nearest_Coords_Array(InsertCorona, NearestCoords, iNearestLoop)
					iNearestLoop = COUNT_OF(NearestCoords) 
				ENDIF
			ENDREPEAT	
		ENDREPEAT
	ENDREPEAT
	
	// print all nearby coords
	#IF IS_DEBUG_BUILD
		REPEAT COUNT_OF(NearestCoords) iNearestLoop
			NET_PRINT("Get_Nearest_Mission_Coronas_And_Gang_Attacks_To_Point -  NearestCoords[") NET_PRINT_INT(iNearestLoop) 
			NET_PRINT("] = ") NET_PRINT_VECTOR(NearestCoords[iNearestLoop].vCoords) NET_PRINT(", ") NET_PRINT_FLOAT(NearestCoords[iNearestLoop].fRadius) NET_NL()
		ENDREPEAT
	#ENDIF

	// get nearest angled areas
	VECTOR vCentre
	VECTOR vCentreNearest
	iNearestLoop = 0
	iBigLoop = 0
	REPEAT g_numMatcAngledAreas iBigLoop
		REPEAT COUNT_OF(NearestGangAttack) iNearestLoop
			
			vCentre = (g_sMatCAngledAreasMP[iBigLoop].matcaaMin + g_sMatCAngledAreasMP[iBigLoop].matcaaMax) * 0.5		  
			vCentreNearest = (NearestGangAttack[iNearestLoop].vMin + NearestGangAttack[iNearestLoop].vMax) * 0.5
			
			IF (VMAG(NearestGangAttack[iNearestLoop].vMin) = 0.0)
			OR (VDIST(vCentre, paramCoords) < VDIST(vCentreNearest, paramCoords))
				
				InsertAngledArea.vMin = g_sMatCAngledAreasMP[iBigLoop].matcaaMin
				InsertAngledArea.vMax = g_sMatCAngledAreasMP[iBigLoop].matcaaMax
				InsertAngledArea.fWidth = g_sMatCAngledAreasMP[iBigLoop].matcaaWidth
				InsertAngledArea.vBlip = g_sMatCAngledAreasMP_TitleUpdate[iBigLoop].matcaaBlipPos
				
				Insert_Into_Nearest_Angled_Area_Array(InsertAngledArea, NearestGangAttack, iNearestLoop)
				iNearestLoop = COUNT_OF(NearestGangAttack)
			ENDIF			
		ENDREPEAT
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		REPEAT COUNT_OF(NearestGangAttack) iNearestLoop
			NET_PRINT("Get_Nearest_Mission_Coronas_And_Gang_Attacks_To_Point -  NearestGangAttack[") NET_PRINT_INT(iNearestLoop) 
			NET_PRINT("] = ") NET_PRINT_VECTOR(NearestGangAttack[iNearestLoop].vMin) NET_PRINT(", ") 
			NET_PRINT_VECTOR(NearestGangAttack[iNearestLoop].vMax) NET_PRINT(", ")
			NET_PRINT_FLOAT(NearestGangAttack[iNearestLoop].fWidth) 
			NET_PRINT(", vBlip=") NET_PRINT_VECTOR(NearestGangAttack[iNearestLoop].vBlip)
			NET_NL()
		ENDREPEAT
	#ENDIF	

ENDPROC


// added by Neil for optimisations
FUNC BOOL Is_Vector_In_Any_Corona_Triggering_Range_Using_Nearest_Arrays(VECTOR paramCoords, SPAWN_SEARCH_CORONA &NearestCoords[], SPAWN_SEARCH_GANG_ATTACK &NearestGangAttack[], FLOAT paramLeeway = CORONA_GENERAL_COSMETIC_LEEWAY_m)
	
	FLOAT	checkingRadius	= 0.0
	INT		tempLoop		= 0
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Is_Vector_In_Any_Corona_Triggering_Range_Using_Nearest_Arrays() - Coords : ")
		NET_PRINT_VECTOR(paramCoords)
		NET_PRINT("  [Leeway beyond triggering radius: ")
		NET_PRINT_FLOAT(paramLeeway)
		NET_PRINT("]")
	#ENDIF
	
	// For the first pass, check only missions with coronas
	REPEAT COUNT_OF(NearestCoords) tempLoop
		// ...mission is triggered at a corona, so check if the vector is in the corona
		checkingRadius = NearestCoords[tempLoop].fRadius + paramLeeway
	
		IF (VDIST(NearestCoords[tempLoop].vCoords, paramCoords) < checkingRadius)
			// Found a mission with coords within tolerance range
			#IF IS_DEBUG_BUILD
				NET_NL()
				NET_PRINT("          ")
				NET_PRINT("Found nearby mission: ")
				NET_PRINT_VECTOR(NearestCoords[tempLoop].vCoords)
			//	Debug_Output_Missions_At_Coords_Slot_In_One_Line(tempLoop)
				NET_NL()
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	// For the second pass, check the angled areas array
	VECTOR	theMin		= << 0.0, 0.0, 0.0 >>
	VECTOR	theMax		= << 0.0, 0.0, 0.0 >>
	FLOAT	theWidth	= 0.0

	
	REPEAT COUNT_OF(NearestGangAttack) tempLoop
		theMin		= NearestGangAttack[tempLoop].vMin
		theMax		= NearestGangAttack[tempLoop].vMax
		theWidth	= NearestGangAttack[tempLoop].fWidth

		
		IF (IS_POINT_IN_ANGLED_AREA(paramCoords, theMin, theMax, theWidth))
		
			// Found the point is inside an area-triggered mission
			#IF IS_DEBUG_BUILD
				NET_NL()
				NET_PRINT("          ")
				NET_PRINT("Found nearby angled area: ")
				NET_PRINT_VECTOR(theMin)
				NET_PRINT(" ")
				NET_PRINT_VECTOR(theMax)
				NET_PRINT(" ")
				NET_PRINT_FLOAT(theWidth)
				NET_NL()
			#ENDIF
				
			RETURN TRUE
			
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT(" - ")
		NET_PRINT("NO NEARBY MISSION")
		NET_NL()
	#ENDIF
	
	RETURN FALSE

ENDFUNC

// ===========================================================================================================
//      Mission At Coords Checking for existing missions
//		CURRENTLY EXPENSIVE FUNCTIONS
// ===========================================================================================================

// PURPOSE:	Check if a vector is in a corona triggering area
//
// INPUT PARAMS:			paramCoords			The coords to be checked
//							paramLeeway			[DEFAULT = CORONA_GENERAL_COSMETIC_LEEWAY_m] An additional distance beyond the triggering radius of the corona that should still return TRUE
// RETURN VALUE:			BOOL				TRUE if within the triggering range of a corona, otherwise FALSE
FUNC BOOL Expensive_Is_Vector_In_Any_Corona_Triggering_Range(VECTOR paramCoords, FLOAT paramLeeway = CORONA_GENERAL_COSMETIC_LEEWAY_m)
	
	FLOAT	checkingRadius	= 0.0
	INT		tempLoop		= 0
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Expensive_Is_Vector_In_Any_Corona_Triggering_Range() - Coords : ")
		NET_PRINT_VECTOR(paramCoords)
		NET_PRINT("  [Leeway beyond triggering radius: ")
		NET_PRINT_FLOAT(paramLeeway)
		NET_PRINT("]")
	#ENDIF
	
	// For the first pass, check only missions with coronas
	REPEAT g_numAtCoordsMP tempLoop
		IF NOT (IS_BIT_SET(g_sAtCoordsMP[tempLoop].matcOptionBitflags, MATC_BITFLAG_AREA_TRIGGERED))
			// ...mission is triggered at a corona, so check if the vector is in the corona
			checkingRadius = g_sAtCoordsMP[tempLoop].matcCorona.triggerRadius + paramLeeway
		
			IF (ARE_VECTORS_ALMOST_EQUAL(g_sAtCoordsMP[tempLoop].matcCoords, paramCoords, checkingRadius))
				// Found a mission with coords within tolerance range
				#IF IS_DEBUG_BUILD
					NET_NL()
					NET_PRINT("          ")
					NET_PRINT("Found nearby mission: ")
				//	Debug_Output_Missions_At_Coords_Slot_In_One_Line(tempLoop)
					NET_NL()
				#ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// For the second pass, check the angled areas array
	VECTOR	theMin		= << 0.0, 0.0, 0.0 >>
	VECTOR	theMax		= << 0.0, 0.0, 0.0 >>
	FLOAT	theWidth	= 0.0
	
	REPEAT g_numMatcAngledAreas tempLoop
		theMin		= g_sMatCAngledAreasMP[tempLoop].matcaaMin
		theMax		= g_sMatCAngledAreasMP[tempLoop].matcaaMax
		theWidth	= g_sMatCAngledAreasMP[tempLoop].matcaaWidth
		
		IF (IS_POINT_IN_ANGLED_AREA(paramCoords, theMin, theMax, theWidth))
			// Found the point is inside an area-triggered mission
			#IF IS_DEBUG_BUILD
				NET_NL()
				NET_PRINT("          ")
				NET_PRINT("Found nearby mission with this RegID: ")
				NET_PRINT_INT(g_sMatCAngledAreasMP[tempLoop].matcaaRegID)
				NET_NL()
			#ENDIF
				
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT(" - ")
		NET_PRINT("NO NEARBY MISSION")
		NET_NL()
	#ENDIF
	
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if there are any missions available at the player's current rank
//
// INPUT PARAMS:		paramRank			The Rank to search for
// RETURN VALUE:		BOOL				TRUE if there are any missions on the map at the player's rank, FALSE if not
FUNC BOOL Expensive_Check_For_MissionsAtCoords_Missions_Of_Rank(INT paramRank)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Expensive Check for any stored missions of rank:")
		NET_PRINT_INT(paramRank)
		NET_PRINT(": ")
	#ENDIF
	
	INT					tempLoop			= 0
	MP_MISSION_ID_DATA	theMissionIdData
	
	REPEAT g_numAtCoordsMP tempLoop
		theMissionIdData = g_sAtCoordsMP[tempLoop].matcMissionIdData
		
		IF (theMissionIdData.idMission = eFM_MISSION_CLOUD)
			//-- To break cyclic header, had to go straight to Fm_mission_info function. Won't work for tennis etc.
			IF (paramRank =  Get_Rank_For_FM_Cloud_Loaded_Activity(theMissionIdData)) 
				#IF IS_DEBUG_BUILD
					NET_PRINT("YES - found mission in slot: ")
					NET_PRINT_INT(tempLoop)
					NET_NL()
				#ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("NO MISSIONS AT THIS RANK")
		NET_NL()
	#ENDIF
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a vector is in or near an active Gang Attack area
//
// INPUT PARAMS:			paramCoords			The coords to be checked
//							paramRadius			[DEFAULT = ACTIVE_GANG_ATTACK_DEFAULT_RADIUS_m] The radius to check between the vector and the Gang Attack blip coord
// RETURN VALUE:			BOOL				TRUE if there is an active Gang Attack within range of the vector, otherwise FALSE
//
// NOTES:	Details of active Gang Attacks in a session are stored on ServerBD
//			This routine uses raw data to avoid cyclic header issues.
FUNC BOOL Expensive_Is_Vector_Near_An_Active_Gang_Attack(VECTOR paramCoords, FLOAT paramRadius = ACTIVE_GANG_ATTACK_DEFAULT_RADIUS_m)

	// Is there an active Gang Attack in this session?
	IF (GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasNumActive = 0)
		RETURN FALSE
	ENDIF
	
	// There is an active Gang Attack in this session - these are stored on the server but not contiguously, so there will be gaps
	// As a pre-step, loop through the active Gang Attacks and store their ContentID Hashes in a contiguous array.
	INT activeGA[MAX_ACTIVE_GANG_ATTACKS]
	INT numActiveGA = 0

	INT tempLoop = 0
	BOOL continueSearch = TRUE
	
	WHILE (continueSearch)
		// Is there active Gang Attack data is this array position?
		IF (GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasActive[tempLoop].agasSharedMissionSlot != NO_ACTIVE_GANG_ATTACK_SLOT)
			// There is active Gang Attack Data, so store a hash of the ContentID
			activeGA[numActiveGA] = GET_HASH_KEY(GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasActive[tempLoop].agasCloudFilename)
			
			numActiveGA++
			IF (numActiveGA = GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasNumActive)
				continueSearch = FALSE
			ENDIF
		ENDIF
		
		tempLoop++
		IF (tempLoop >= MAX_ACTIVE_GANG_ATTACKS)
			continueSearch = FALSE
		ENDIF
	ENDWHILE
	
	// We should have found at least one active GA in the session, but do a sanity check
	IF (numActiveGA = 0)
		// Shouldn't really happen, but just in case
		RETURN FALSE
	ENDIF
	
	// Loop through all GangAttacks stored on MissionsAtCoords and check if they are within range and active
	VECTOR blipCoords = << 0.0, 0.0, 0.0 >>
	VECTOR checkCoords = paramCoords
	
	INT activeLoop = 0
	MP_MISSION_ID_DATA	theMissionIdData
	INT thisContentIdHash = 0
	
	REPEAT g_numAtCoordsMP tempLoop
		IF (IS_BIT_SET(g_sAtCoordsMP[tempLoop].matcOptionBitflags, MATC_BITFLAG_AREA_TRIGGERED))
			// ...mission is area triggered, so this will be a Gang Attack
			blipCoords = g_sAtCoordsMP[tempLoop].matcCoords
			
			// Change the checkCoords to use the same ground height as the blip
			checkCoords.z = blipCoords.z
			
			IF (GET_DISTANCE_BETWEEN_COORDS(blipCoords, checkCoords) < paramRadius)
				// The checking vector is in range of this GangAttack, so check if this GangAttack is active
				theMissionIdData = g_sAtCoordsMP[tempLoop].matcMissionIdData
				thisContentIdHash = Get_Default_Corona_Options_For_FM_Cloud_Loaded_Activity_HASH(theMissionIdData)
				
				REPEAT numActiveGA activeLoop
					IF (thisContentIdHash = activeGA[activeLoop])
						// This GA is active
						RETURN TRUE
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE

ENDFUNC



// ===========================================================================================================
//      PUBLIC ACCESS TO TRANSITION FUNCTIONS
// ===========================================================================================================

// PURPOSE:	The Mission Is Being Played Again
PROC Mission_Shared_Mission_Being_Played_Again()

	IF (IS_STRING_NULL_OR_EMPTY(g_sTriggerMP.mtMissionData.mdID.idCloudFilename))
		#IF IS_DEBUG_BUILD
		PRINTLN("[TS] * ...KGM MP [MShared][CloudData]: Mission_Shared_Mission_Being_Played_Again() - ERROR: MissionTrigger Filename is NULL or EMPTY")
		#ENDIF
		EXIT
	ENDIF
	
	// KGM 22/11/13: Only set the Copy Mission use to 'Play Again' if the copy data is the same as the current mission
	// (This is to fix a bug where: Player walking into own UGC/Bookmark, chooses 'Host' so Shared Data gets stored as Copy data, Walk Out, JIP another player already playing
	//		own UGC, On vote screen, Players choose to Replay dragging spectator on with them, the setup routine doesnt' find the mission data locally but the 'play again'
	//		flag is set so it tries instead to use the Copy Shared Mission data which is for player's own UGC rather than the UGC being spectated)
	IF NOT (IS_STRING_NULL_OR_EMPTY(g_sCopyRegSharedMissionData.crsmdHeaderData.tlName))
		IF (ARE_STRINGS_EQUAL(g_sCopyRegSharedMissionData.crsmdHeaderData.tlName, g_sTriggerMP.mtMissionData.mdID.idCloudFilename))
			// ...the Copy Shared Mission data is the same as teh current mission, so setup the 'use of copy data' indicator as 'Copy'
			g_eUseOfCopyRegSharedMissionData = COPY_MISSION_USE_IS_PLAY_AGAIN
		ELSE
			// ...the Copy Shared Mission data is NOT the same as the current mission, so clear the 'use of copy data' indicator to prevent trying to use these details to trigger the mission
			g_eUseOfCopyRegSharedMissionData = COPY_MISSION_NOT_REQUIRED
		ENDIF
	ENDIF

	// Set the flags for 'Play Again' and grab the current filename
	g_sRandomMission.tmIsRandomMission			= FALSE
	g_sRandomMission.tmRandomMissionFilename	= g_sTriggerMP.mtMissionData.mdID.idCloudFilename
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: Change Shared Mission Data Use if Copy Data to: Play Again - used on re-initialisation if Header Data is no longer locally available on this player's machine.") NET_NL()
		NET_PRINT("g_sCopyRegSharedMissionData.crsmdHeaderData.tlName = ") NET_PRINT(g_sCopyRegSharedMissionData.crsmdHeaderData.tlName) NET_NL()
		NET_PRINT("g_sTriggerMP.mtMissionData.mdID.idCloudFilename = ")NET_PRINT(g_sTriggerMP.mtMissionData.mdID.idCloudFilename) NET_NL()
		NET_PRINT("      Also filling the 'Random' variables with data - Play Again and Random both use RESTART_RANDOM transition: ")
		NET_PRINT(g_sRandomMission.tmRandomMissionFilename)
		IF (g_eUseOfCopyRegSharedMissionData = COPY_MISSION_USE_IS_PLAY_AGAIN)
		NET_NL()
			NET_PRINT("      The Copy of the most recent Shared Mission Data is also for this mission, so leaving the Copy Use indicator as Play Again") NET_NL()
		ELSE
			NET_PRINT("      However, the Copy of the most recent Shared Mission Data is not this mission, so ensuring the Copy Use indicator is clear") NET_NL()
		ENDIF
		NET_NL()
	#ENDIF
					
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	This mission has been randomly chosen to be played after the transition - need to store it somewhere to prioeritise it getting setup again
//
// INPUT PARAMS:		paramCloudFilename		The Cloud Filename of the mission to be setup again after transition
//
// NOTES:	This isn't strictly related to 'SHARED MISSION' routines, but the 'played again' function above gets called from the same place and there is nothing else similar so far
PROC Mission_Shared_Next_Mission_Randomly_Chosen(TEXT_LABEL_23 paramCloudFilename)

	IF (IS_STRING_NULL_OR_EMPTY(paramCloudFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared][CloudData]: Mission_Shared_Next_Mission_Randomly_Chosen() - ERROR: Filename is NULL or EMPTY") NET_NL()
			SCRIPT_ASSERT("Mission_Shared_Next_Mission_Randomly_Chosen() - ERROR: Filename is NULL or EMPTY")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Store the details of this transition mission
	g_sRandomMission.tmIsRandomMission			= TRUE
	g_sRandomMission.tmRandomMissionFilename	= paramCloudFilename
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: This Randomly chosen mission is being stored to be priority launched after transition: ")
		NET_PRINT(paramCloudFilename)
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Random and Play Again data
PROC Clear_Random_And_Play_Again_Data()
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: Clearing the Random and Play Again Data (because player may have gone on mission).")
		NET_NL()
		NET_PRINT("      Current Contents:")
		NET_NL()
		NET_PRINT("         Random/Play Again Filename      : ")
		NET_PRINT(g_sRandomMission.tmRandomMissionFilename)
		NET_NL()
		NET_PRINT("         g_eUseOfCopyRegSharedMissionData: ")
		SWITCH (g_eUseOfCopyRegSharedMissionData)
			CASE COPY_MISSION_USE_IS_PLAY_AGAIN		NET_PRINT("COPY_MISSION_USE_IS_PLAY_AGAIN")		BREAK
			CASE COPY_MISSION_NOT_REQUIRED			NET_PRINT("COPY_MISSION_NOT_REQUIRED")			BREAK
			DEFAULT									NET_PRINT("UNKNOWN COPY USE")					BREAK
		ENDSWITCH
		NET_NL()
	#ENDIF

	// Clear it all
	g_eUseOfCopyRegSharedMissionData = COPY_MISSION_NOT_REQUIRED
	
	g_sRandomMission.tmIsRandomMission			= FALSE
	g_sRandomMission.tmRandomMissionFilename	= ""

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Indicate that the Strand Mission Part One Data needs to be used to setup the corona again
//
// INPUT PARAMS:		paramCloudFilename		The Cloud Filename of the Strand Mission Part One expected to be stored
//
// NOTES:	This isn't strictly related to 'SHARED MISSION' routines, but is similar to the above functions
PROC Quick_Restart_Strand_Mission_Part_One(TEXT_LABEL_23 paramCloudFilename)

	IF (IS_STRING_NULL_OR_EMPTY(paramCloudFilename))
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [Heist]: Quick_Restart_Strand_Mission_Part_One() - ERROR: Filename is NULL or EMPTY")
			SCRIPT_ASSERT("Quick_Restart_Strand_Mission_Part_One() - ERROR: Filename is NULL or EMPTY")
		#ENDIF
		
		EXIT
	ENDIF
	
	PRINTLN(".KGM [Heist]: Request that the stored Strand Mission Part One Header Data be used to setup the Part One Corona for Quick Restart: ", paramCloudFilename)
	
	// Ensure the expected mission is stored
	IF NOT (g_sStrandMissionPartOne.smp1hdInUse)
		PRINTLN(".KGM [Heist]: ...but there is no Strand Mission Part One Header Data stored.")
		EXIT
	ENDIF
	
	IF NOT (ARE_STRINGS_EQUAL(paramCloudFilename, g_sStrandMissionPartOne.smp1hdHeaderData.tlName))
		PRINTLN(".KGM [Heist]: ...but Strand Mission Part One Header Data stored is for a different mission. Stored ContentID: ", g_sStrandMissionPartOne.smp1hdHeaderData.tlName)
		EXIT
	ENDIF
	
	// Request the corona be setup again
	g_sSetUpStrandMissionPartOne = TRUE

ENDPROC



// ===========================================================================================================
//      Mission At Coords Retained Gang Attack Blip Public Access Routines
// ===========================================================================================================

// PURPOSE:	Remove the Retained Gang Attack blip (this should get called when the Gang Attack kicks off properly)
PROC Remove_Retained_Gang_Attack_Blip()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Remove_Retained_Gang_Attack_Blip() - external request to remove the retained Gang Attack blip") NET_NL()
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	g_removeRetainedGABlip = TRUE

ENDPROC




// ===========================================================================================================
//      Mission At Coords Retained Gang Attack Blip Public Access Routines
// ===========================================================================================================

// PURPOSE: Gather a total of the new missions unlocked at the current rank
//
// INPUT PARAMS:		paramRank			The Rank to search for
// REFERENCE PARAMS:	refUnlocks			The totals of activity types unlocked at the specified rank
// RETURN VALUE:		BOOL				TRUE if data got generated, FALSE if it didn't.
//
// NOTES:	To avoid cyclic header issues, the unlock flags within the reference param needs to be set before calling this function.
FUNC BOOL Gather_Details_Of_Missions_At_This_Rank(INT paramRank, g_structMatcUnlocks &refUnlocks)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Expensive Check: Gathering Details of Missions At This Rank:")
		NET_PRINT_INT(paramRank)
		NET_NL()
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		IF NOT (refUnlocks.areUnlockChecksDone)
			NET_PRINT("      BUT: Unlock checks haven't been done. Call Fill_Missions_At_This_Rank_Unlocks() first") NET_NL()
			SCRIPT_ASSERT("Gather_Details_Of_Missions_At_This_Rank() - ERROR: Need to fill the unlock flags. Tell Keith.")
		
			RETURN FALSE
		ENDIF
		
		NET_PRINT("      Unlocked Content: ")
		IF (refUnlocks.areMissionsUnlocked)			NET_PRINT(" [MISSIONS]")		ENDIF
		IF (refUnlocks.areRacesUnlocked)			NET_PRINT(" [RACES]")			ENDIF
		IF (refUnlocks.areDeathmatchesUnlocked)		NET_PRINT(" [DEATHMATCHES]")	ENDIF
		IF (refUnlocks.areSurvivalsUnlocked)		NET_PRINT(" [SURVIVALS]")		ENDIF
		IF (refUnlocks.areGangAttacksUnlocked)		NET_PRINT(" [GANG ATTACKS]")	ENDIF
		IF (refUnlocks.areBaseJumpsUnlocked)		NET_PRINT(" [BASE JUMPS]")		ENDIF
		NET_NL()
	#ENDIF
	
	IF (Is_Cloud_Loaded_Mission_Data_Being_Refreshed())
		#IF IS_DEBUG_BUILD
			NET_PRINT("      BUT: Mission Data is being delayed") NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	INT					tempLoop			= 0
	MP_MISSION_ID_DATA	theMissionIdData
	
	// Clear out the totals struct
	g_structMatcUnlocks emptyTotals
	refUnlocks = emptyTotals
	
	// There's no good generic way to do this without creating a return struct that's huge, so will be using hardcoded relationships
	REPEAT g_numAtCoordsMP tempLoop
		theMissionIdData = g_sAtCoordsMP[tempLoop].matcMissionIdData
		
		// Is this activity of the correct rank?
		IF (paramRank = Get_Rank_For_FM_Cloud_Loaded_Activity(theMissionIdData))
			SWITCH (theMissionIdData.idMission)
				CASE eFM_MISSION_CLOUD
					// Gather individual totals for 'standard missions (includes Random Events and Versus Missions)', HEISTS, CONTACT MISSIONS, LTS, and CTF missions
					// Missions Unlocked?
					IF (refUnlocks.areMissionsUnlocked)
						// ...update the numbers of appropriate (anything uncategorised should be added to the standard missions total)
						IF (Is_FM_Cloud_Loaded_Activity_A_Contact_Mission(theMissionIdData))
							// ...ignore CMs - they're not stored until offered
						ELIF (Is_FM_Cloud_Loaded_Activity_A_CTF_Mission(theMissionIdData))
							refUnlocks.numUnlockedCTF++
						ELIF (Is_FM_Cloud_Loaded_Activity_A_Heist(theMissionIdData))
							// ...ignore Heists - they're not stored until offered
						ELIF (Is_FM_Cloud_Loaded_Activity_Heist_Planning(theMissionIdData))
							// ...ignore Heist Planning missions - they're not stored until offered
						ELIF (Is_FM_Cloud_Loaded_Activity_A_LTS_Mission(theMissionIdData))
							refUnlocks.numUnlockedLTS++
						ELSE
							refUnlocks.numUnlockedMissions++
						ENDIF
					ELSE
						// ...found an activity at rank where the job type isn't generally unlocked yet
						refUnlocks.rankJobsNotYetUnlocked = TRUE
					ENDIF
					BREAK
					
				CASE eFM_DEATHMATCH_CLOUD
					// Any type of deathmatch
					IF (refUnlocks.areDeathmatchesUnlocked)
						// ...update the numbers
						refUnlocks.numUnlockedDeathmatches++
					ELSE
						// ...found an activity at rank where the job type isn't generally unlocked yet
						refUnlocks.rankJobsNotYetUnlocked = TRUE
					ENDIF
					BREAK
					
				CASE eFM_RACE_CLOUD
					// Any type of Race
					IF (refUnlocks.areRacesUnlocked)
						// ...update the numbers
						refUnlocks.numUnlockedRaces++
					ELSE
						// ...found an activity at rank where the job type isn't generally unlocked yet
						refUnlocks.rankJobsNotYetUnlocked = TRUE
					ENDIF
					BREAK
					
				CASE eFM_SURVIVAL_CLOUD
					// Survivals
					IF (refUnlocks.areSurvivalsUnlocked)
						// ...update the numbers
						refUnlocks.numUnlockedSurvivals++
					ELSE
						// ...found an activity at rank where the job type isn't generally unlocked yet
						refUnlocks.rankJobsNotYetUnlocked = TRUE
					ENDIF
					BREAK
					
				CASE eFM_GANG_ATTACK_CLOUD
					// Gang Attacks
					IF (refUnlocks.areGangAttacksUnlocked)
						// ...update the numbers
						refUnlocks.numUnlockedGangAttacks++
					ELSE
						// ...found an activity at rank where the job type isn't generally unlocked yet
						refUnlocks.rankJobsNotYetUnlocked = TRUE
					ENDIF
					BREAK
					
				CASE eFM_BASEJUMP_CLOUD
					// BaseJumps
					IF (refUnlocks.areBaseJumpsUnlocked)
						// ...update the numbers
						refUnlocks.numUnlockedBaseJumps++
					ELSE
						// ...found an activity at rank where the job type isn't generally unlocked yet
						refUnlocks.rankJobsNotYetUnlocked = TRUE
					ENDIF
					BREAK
					
				DEFAULT
					// Ignore anything else
					BREAK
			ENDSWITCH
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Gathered These Details for missions unlocked at this rank:") 					NET_NL()
		NET_PRINT("      LTS                  : ")	NET_PRINT_INT(refUnlocks.numUnlockedLTS)				NET_NL()
		NET_PRINT("      CTF                  : ")	NET_PRINT_INT(refUnlocks.numUnlockedCTF)				NET_NL()
		NET_PRINT("      Other Missions       : ")	NET_PRINT_INT(refUnlocks.numUnlockedMissions)			NET_NL()
		NET_PRINT("      Deathmatches         : ")	NET_PRINT_INT(refUnlocks.numUnlockedDeathmatches)		NET_NL()
		NET_PRINT("      Races                : ")	NET_PRINT_INT(refUnlocks.numUnlockedRaces)				NET_NL()
		NET_PRINT("      Survivals            : ")	NET_PRINT_INT(refUnlocks.numUnlockedSurvivals)			NET_NL()
		NET_PRINT("      Gang Attacks         : ")	NET_PRINT_INT(refUnlocks.numUnlockedGangAttacks)		NET_NL()
		NET_PRINT("      BaseJumps            : ")	NET_PRINT_INT(refUnlocks.numUnlockedBaseJumps)			NET_NL()
		NET_PRINT("      At Rank, Not Unlocked: ")
		IF (refUnlocks.rankJobsNotYetUnlocked)
			NET_PRINT("YES (there are activities at this rank with a type that hasn't yet been unlocked") NET_NL()
		ELSE
			NET_PRINT("NO (all activities at this rank have already had their types unlocked") NET_NL()
		ENDIF
	#ENDIF
	
	// Data has been gathered
	RETURN TRUE

ENDFUNC



// PURPOSE:	Get the 'Played' status for this mission
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
FUNC BOOL Has_MissionsAtCoords_Activity_Been_Played(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_MISSION_HAS_BEEN_PLAYED))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To set a random blip of a specified type to flash
//
// INPUT PARAMS:			paramMissionId			The type of job
//							paramMustBeUnplayed		[DEFAULT = FALSE] TRUE means it must be unplayed, FALSE means it doesn't matter if it has been played or not
//							paramDuration			[DEFAULT = MATC_BLIP_FLASH_TIME_msec] The duration of the flash in msec
//							paramNumBlipsToFlash	[DEFAULT = 1] The number of blips to flash
PROC Set_MissionsAtCoords_Mission_Blips_Of_Type_To_Flash(MP_MISSION paramMissionId, BOOL paramMustBeUnplayed = FALSE, INT paramDuration = MATC_BLIP_FLASH_TIME_msec, INT paramNumBlipsToFlash = 1)

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Request to flash blips") NET_NL()
		DEBUG_PRINTCALLSTACK()
	#ENDIF

	CONST_INT	MAX_BLIPS_TO_FLASH				5
	
	INT			tempLoop	= 0
	BLIP_INDEX	thisBlip	= NULL
	BOOL		acceptBlip	= FALSE
	
	BLIP_INDEX	theBlips[MAX_BLIPS_TO_FLASH]
	
	REPEAT MAX_BLIPS_TO_FLASH tempLoop
		theBlips[tempLoop] = NULL
	ENDREPEAT
	
	INT numBlipsStored = 0
	
	REPEAT g_numAtCoordsMP tempLoop
		IF (numBlipsStored < MAX_BLIPS_TO_FLASH)
			// ...check for jobs of the appropriate type
			IF (g_sAtCoordsMP[tempLoop].matcMissionIdData.idMission = paramMissionId)
				// ...found mission type
				thisBlip = g_sAtCoordsMP[tempLoop].matcBlipIndex
				
				IF (thisBlip != NULL)
					IF (GET_BLIP_INFO_ID_DISPLAY(thisBlip) != DISPLAY_NOTHING)
						// ...this blip is on display
						acceptBlip = FALSE
						IF (paramMustBeUnplayed)
							// ...must be unplayed
							IF NOT (Has_MissionsAtCoords_Activity_Been_Played(tempLoop))
								// ...accept - hasn't been played
								acceptBlip = TRUE
							ENDIF
						ELSE
							// ...accept - doesn't have to be unplayed
							acceptBlip = TRUE
						ENDIF
						
						// Store on the blip array sequentially until full, then randomly replace an existing blip
						IF (acceptBlip)
							theBlips[numBlipsStored] = thisBlip
							numBlipsStored++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF (numBlipsStored = 0)
		EXIT
	ENDIF
	
	// Flash the appropriate number of blips
	IF (numBlipsStored < paramNumBlipsToFlash)
		paramNumBlipsToFlash = numBlipsStored
	ENDIF
	
	REPEAT paramNumBlipsToFlash tempLoop
		SET_BLIP_FLASHES(theBlips[tempLoop], TRUE)
		SET_BLIP_FLASH_TIMER(theBlips[tempLoop], paramDuration)
	ENDREPEAT
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	This is essentially a copy of 'Is_There_A_MissionsAtCoords_Focus_Mission()' using raw data
//
// RETURN VALUE:			BOOL		TRUE if there is a Focus Mission (and therefore the mission is being triggered by Matc), FALSE if not
FUNC BOOL Is_Mission_Being_Triggered_By_Matc()
	RETURN (g_sAtCoordsBandsMP[MATCB_FOCUS_MISSION].matcbNumInBand > 0)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the trigger location of the focus mission
// NOTES:	KGM 6/11/14: Added so that the Mission Triggerer can store the trigger coords for a Part One Strand Mission in case a Part Two quick restarts at Part One
//			This function accesses raw data
FUNC VECTOR Get_Focus_Mission_Coordinates()

	VECTOR theReturn = << 0.0, 0.0, 0.0 >>
	
	// Is there a focus mission?
	IF NOT (Is_Mission_Being_Triggered_By_Matc())
		PRINTLN(".KGM [At Coords][Heist]: Get_Focus_Mission_Coordinates() - ERROR: There is no focus mission. Returning: ", theReturn)
		RETURN (theReturn)
	ENDIF

	// Get the focus mission slot (should be 0)
	INT focusSlot = g_sAtCoordsBandsMP[MATCB_FOCUS_MISSION].matcbFirstSlot
	
	theReturn = g_sAtCoordsMP[focusSlot].matcCoords
	
	PRINTLN(".KGM [At Coords][Heist]: Get_Focus_Mission_Coordinates() - Returning: ", theReturn)
	RETURN (theReturn)

ENDFUNC


FUNC BOOL SHOULD_BLOCK_All_MissionsAtCoords_Missions_BECAUSE_OF_ENTRY_TYPE()
	IF g_iMissionEnteryType != ciMISSION_ENTERY_TYPE_INVALID
	AND g_iMissionEnteryType != ciMISSION_ENTERY_TYPE_V2_CORONA_ON_CALL
	AND g_iMissionEnteryType != ciMISSION_ENTERY_TYPE_V2_PROFESSIONAL_CORONA_ON_CALL
	AND g_iMissionEnteryType != ciMISSION_ENTERY_TYPE_V2_ADVERSARY_CORONA_ON_CALL
	AND g_iMissionEnteryType != ciMISSION_ENTERY_TYPE_ON_CALL
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


// ===========================================================================================================
//      Mission At Coords Functionally Block All Missions
//		(Here to avoid cyclic headers)
// ===========================================================================================================

// PURPOSE:	Block all missions and hide all coronas/blips (or unblock them again)
//
// INPUT PARAMS:		paramBlock		TRUE if all should be blocked, FALSE if all should be unblocked again
PROC Block_All_MissionsAtCoords_Missions(BOOL paramBlock)
	
	IF SHOULD_BLOCK_All_MissionsAtCoords_Missions_BECAUSE_OF_ENTRY_TYPE()
	AND paramBlock
		PRINTLN("[TS] * .KGM [At Coords]: Block_All_MissionsAtCoords_Missions() - IGNORE: g_iMissionEnteryType != ciMISSION_ENTERY_TYPE_INVALID")
		DEBUG_PRINTCALLSTACK()
		EXIT
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		NET_PRINT(".KGM [At Coords]: Block_All_MissionsAtCoords_Missions() called with ")
		IF (paramBlock)
			NET_PRINT(" TRUE (block all missions) - see callstack")
		ELSE
			NET_PRINT(" FALSE (unblock all missions) - see callstack")
		ENDIF
		NET_NL()
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF (paramBlock = g_matcBlockAndHideAllMissions)
		PRINTLN(".KGM [At Coords]: Block_All_MissionsAtCoords_Missions() - IGNORE: State hasn't changed")
		EXIT
	ENDIF
	
	g_matcBlockAndHideAllMissions = paramBlock
	
	// Clear the one mission exception controls
	g_matcUnblockOneMissionHash = 0
	g_matcUnblockOneMissionTimeout = 0
	
	// Immediately Hide/Show all blips
	// KGM 3/6/15: This was causing cyclic headers so is now done here: Maintain_All_Blips_To_Match_All_Coronas()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	ONLY WHEN ALL MISSIONS ARE BLOCKED USING THE ABOVE FUNCTION, Allow ONE mission to become unblocked
//
// INPUT PARAMS:			paramContentID		The ContentID for the mission being allowed to trigger
//
// NOTES:	This was added to allow pause menu triggering while in an FM Event as an exception when all other missions are functionally blocked.
//			THIS EXCEPTION WILL AUTO-DISABLE AFTER A SHORT PERIOD OF TIME.
PROC Unblock_One_MissionsAtCoords_Mission(TEXT_LABEL_23 paramContentID)

	IF (IS_STRING_NULL_OR_EMPTY(paramContentID))
		PRINTLN(".KGM [At Coords]: Unblock_One_MissionsAtCoords_Mission() - IGNORE: ContentID NULL or EMPTY")
		EXIT
	ENDIF
	
	// Only allow this if ALL missions are currently blocked
	IF NOT (g_matcBlockAndHideAllMissions)
		PRINTLN(".KGM [At Coords]: Unblock_One_MissionsAtCoords_Mission() - IGNORE: Only works when all other missions to be blocked using Block_All_MissionsAtCoords_Missions()")
		EXIT
	ENDIF
	
	// Store the details and set an auto-disable timer
	INT HashCID = GET_HASH_KEY(paramContentID)
	g_matcUnblockOneMissionHash = HashCID
	
	g_matcUnblockOneMissionTimeout = GET_GAME_TIMER() + MATC_UNBLOCK_ONE_MISSION_ACTIVE_TIME_msec
	
	PRINTLN(".KGM [At Coords]: Unblock_One_MissionsAtCoords_Mission() - Unblocking this mission: ", paramContentID, "   Hash: ", g_matcUnblockOneMissionHash)

ENDPROC



// ===========================================================================================================
//      Mission At Coords Functionally Block Missions in a specific area
// ===========================================================================================================

// PURPOSE: KGM 17/6/15 [BUG 2316703]: Switch on a temporary disabled area (this blocks coronas and Gang Attacks in the area)
PROC Set_MissionsAtCoords_Local_Disabled_Area(VECTOR paramPosition, FLOAT fDisableRange = MATC_LOCAL_DISABLED_AREA_RANGE_m)

	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [At Coords]: Set_MissionsAtCoords_Local_Disabled_Area() called.")
		PRINTLN(".KGM [At Coords]: 							Adding disable area at: 	", paramPosition)
		PRINTLN(".KGM [At Coords]: 							With range: 				", paramPosition)
		
		IF (g_matcHasLocalDisabledArea)
			PRINTLN(".KGM [At Coords]: ...NOTE: This replaces an existing disabled area at ", g_matcLocalDisabledArea)
		ENDIF
		
		DEBUG_PRINTCALLSTACK()
	#ENDIF

	g_matcLocalDisabledArea = paramPosition
	g_matcHasLocalDisabledArea = TRUE
	g_matcLocalDisabledAreaRange = fDisableRange
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: KGM 17/6/15 [BUG 2316703]: Switch off a temporary disabled area (this allows any blocked coronas or Gang Attacks in the area to be triggered again)
PROC Clear_MissionsAtCoords_Local_Disabled_Area()

	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [At Coords]: Clear_MissionsAtCoords_Local_Disabled_Area(): ", g_matcLocalDisabledArea)
		
		DEBUG_PRINTCALLSTACK()
	#ENDIF

	g_matcLocalDisabledArea = << 0.0, 0.0, 0.0 >>
	g_matcHasLocalDisabledArea = FALSE
	g_matcLocalDisabledAreaRange = MATC_LOCAL_DISABLED_AREA_RANGE_m
	
ENDPROC



// ===========================================================================================================
//      Mission At Coords Functionally Block Specific Gang Attacks (ie: on FM Mission)
// ===========================================================================================================

// PURPOSE: KGM 22/7/15 [BUG 2382702]: Temporarily block this specific Gang Attack
//
// INPUT PARAMS:		paramRootContentID		The rootContentID of the GangAttack being specifically blocked
PROC Block_MissionsAtCoords_Specific_Gang_Attack(STRING paramRootContentID)

	IF (IS_STRING_NULL_OR_EMPTY(paramRootContentID))
		PRINTLN(".KGM [At Coords]: Block_MissionsAtCoords_Specific_Gang_Attack() - IGNORED: rootContentID is NULL or Empty")
		DEBUG_PRINTCALLSTACK()
		EXIT
	ENDIF
	
	INT hashRCID = GET_HASH_KEY(paramRootContentID)
	
	INT tempLoop = 0
	
	// Loop through all Gang Attacks looking for this RCID
	REPEAT g_numAtCoordsMP tempLoop
		IF (IS_BIT_SET(g_sAtCoordsMP[tempLoop].matcOptionBitflags, MATC_BITFLAG_AREA_TRIGGERED))
			// ...this mission is a Gang Attack, so compare rootContentIDs
			IF (Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(g_sAtCoordsMP[tempLoop].matcMissionIdData) = hashRCID)
				IF NOT (IS_BIT_SET(g_sAtCoordsMP[tempLoop].matcStateBitflags, MATC_BITFLAG_HIDDEN_GANG_ATTACK))
					SET_BIT(g_sAtCoordsMP[tempLoop].matcStateBitflags, MATC_BITFLAG_HIDDEN_GANG_ATTACK)
		
					#IF IS_DEBUG_BUILD
						PRINTLN(".KGM [At Coords]: Block_MissionsAtCoords_Specific_Gang_Attack() - FOUND: rootContentID [", paramRootContentID, "] - Blocking Gang Attack. RegID: ", g_sAtCoordsMP[tempLoop].matcRegID)
						PRINTSTRING(".KGM [At Coords]: Gang Attack Specifically Hidden. REGID: ")
						PRINTINT(g_sAtCoordsMP[tempLoop].matcRegID)
						PRINTSTRING(" - Block_MissionsAtCoords_Specific_Gang_Attack()")
						PRINTNL()
						DEBUG_PRINTCALLSTACK()
					#ENDIF
				ELSE
					PRINTLN(".KGM [At Coords]: Block_MissionsAtCoords_Specific_Gang_Attack() - FOUND: rootContentID [", paramRootContentID, "] - BUT Gang Attack already blocked. RegID: ", g_sAtCoordsMP[tempLoop].matcRegID)
				ENDIF
				
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN(".KGM [At Coords]: Block_MissionsAtCoords_Specific_Gang_Attack() - FAILED: rootContentID [", paramRootContentID, "] - NOT FOUND")

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: KGM 22/7/15 [BUG 2382702]: Unblock this specific Gang Attack if temporarily blocked
//
// INPUT PARAMS:		paramRootContentID		The rootContentID of the GangAttack being unblocked
PROC Unblock_MissionsAtCoords_Specific_Gang_Attack(TEXT_LABEL_23 paramRootContentID)

	IF (IS_STRING_NULL_OR_EMPTY(paramRootContentID))
		PRINTLN(".KGM [At Coords]: Unblock_MissionsAtCoords_Specific_Gang_Attack() - IGNORED: rootContentID is NULL or Empty")
		DEBUG_PRINTCALLSTACK()
		EXIT
	ENDIF
	
	INT hashRCID = GET_HASH_KEY(paramRootContentID)
	
	INT tempLoop = 0
	
	// Loop through all Gang Attacks looking for this RCID
	REPEAT g_numAtCoordsMP tempLoop
		IF (IS_BIT_SET(g_sAtCoordsMP[tempLoop].matcOptionBitflags, MATC_BITFLAG_AREA_TRIGGERED))
			// ...this mission is a Gang Attack, so compare rootContentIDs
			IF (Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(g_sAtCoordsMP[tempLoop].matcMissionIdData) = hashRCID)
				IF (IS_BIT_SET(g_sAtCoordsMP[tempLoop].matcStateBitflags, MATC_BITFLAG_HIDDEN_GANG_ATTACK))
					CLEAR_BIT(g_sAtCoordsMP[tempLoop].matcStateBitflags, MATC_BITFLAG_HIDDEN_GANG_ATTACK)
		
					#IF IS_DEBUG_BUILD
						PRINTLN(".KGM [At Coords]: Unblock_MissionsAtCoords_Specific_Gang_Attack() - FOUND: rootContentID [", paramRootContentID, "] - Unblocking Gang Attack. RegID: ", g_sAtCoordsMP[tempLoop].matcRegID)
						PRINTSTRING(".KGM [At Coords]: Gang Attack No Longer Specifically Hidden. REGID: ")
						PRINTINT(g_sAtCoordsMP[tempLoop].matcRegID)
						PRINTSTRING(" - Unblock_MissionsAtCoords_Specific_Gang_Attack()")
						PRINTNL()
						DEBUG_PRINTCALLSTACK()
					#ENDIF
				ELSE
					PRINTLN(".KGM [At Coords]: Unblock_MissionsAtCoords_Specific_Gang_Attack() - FOUND: rootContentID [", paramRootContentID, "] - BUT Gang Attack already not blocked. RegID: ", g_sAtCoordsMP[tempLoop].matcRegID)
				ENDIF
				
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN(".KGM [At Coords]: Block_MissionsAtCoords_Specific_Gang_Attack() - FAILED: rootContentID [", paramRootContentID, "] - NOT FOUND")

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: KGM 22/7/15 [BUG 2382702]: Unblock ALL temporarily blocked Gang Attacks
PROC Unblock_All_MissionsAtCoords_Specific_Gang_Attacks()
	
	PRINTLN(".KGM [At Coords]: Unblock_All_MissionsAtCoords_Specific_Gang_Attacks()")
	DEBUG_PRINTCALLSTACK()
						
	INT tempLoop = 0
	
	#IF IS_DEBUG_BUILD
		BOOL unblockedGA = FALSE
	#ENDIF
	
	// Loop through all Gang Attacks unblocking any that are blocked
	REPEAT g_numAtCoordsMP tempLoop
		IF (IS_BIT_SET(g_sAtCoordsMP[tempLoop].matcOptionBitflags, MATC_BITFLAG_AREA_TRIGGERED))
			// ...this mission is a Gang Attack, so unblock if blocked
			IF (IS_BIT_SET(g_sAtCoordsMP[tempLoop].matcStateBitflags, MATC_BITFLAG_HIDDEN_GANG_ATTACK))
				CLEAR_BIT(g_sAtCoordsMP[tempLoop].matcStateBitflags, MATC_BITFLAG_HIDDEN_GANG_ATTACK)
		
				#IF IS_DEBUG_BUILD
					PRINTSTRING(".KGM [At Coords]: Gang Attack No Longer Specifically Hidden. REGID: ")
					PRINTINT(g_sAtCoordsMP[tempLoop].matcRegID)
					PRINTSTRING(" - Unblock_All_MissionsAtCoords_Specific_Gang_Attacks()")
					PRINTNL()
				
					unblockedGA = TRUE
				#ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		IF NOT (unblockedGA)
			PRINTLN(".KGM [At Coords]: Unblock_All_MissionsAtCoords_Specific_Gang_Attacks() - NO BLOCKED GANG ATTACKS FOUND")
		ENDIF
	#ENDIF
	
ENDPROC

