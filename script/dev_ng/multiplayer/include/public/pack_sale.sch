USING "rage_builtins.sch"
USING "net_script_tunables_new.sch"

ENUM PACK_CLOTHES_SALE_BS	
	PCS_BS_INVALID					=-1,
	PCS_BS_BEACH_BUM				= 0,										
	PCS_BS_BUSINESS 				= 1,
	PCS_BS_FESTIVE_SURPRISE 		= 2,
	PCS_BS_VALENTINES				= 3,
	PCS_BS_HIGH_LIFE				= 4,						
	PCS_BS_IM_NOT_A_HIPSTER			= 5,				
	PCS_BS_HEISTS_2015				= 6,
	PCS_BS_INDEPENDENCE_DAY			= 7,
	PCS_BS_FLIGHT_SCHOOL			= 8,
	PCS_BS_LTS_CREATOR				= 9,
	PCS_BS_ILL_GOTTEN_GAINS_I 		= 10,			
	PCS_BS_ILL_GOTTEN_GAINS_II		= 11,
	PCS_BS_EDITOR_AND_FM_EVENTS		= 12,
	PCS_BS_LOWRIDERS				= 13,		
	PCS_BS_HALLOWEEN				= 14,
	PCS_BS_LOWRIDER_2				= 15,
	PCS_BS_APARTMENTS				= 16,
	PCS_BS_FESTIVE_SURPRISE_2015	= 17,
	PCS_BS_FESTIVE_SURPRISE_2016	= 18,
	PCS_BS_BE_MY_VALENTINE			= 19,
	PCS_BS_FINANCE_AND_FELONY		= 20,			
	PCS_BS_CUNNING_STUNTS			= 21,														
	PCS_BS_BIKERS					= 22,												
	PCS_BS_IMPORT_EXPORT			= 23,														
	PCS_BS_CUNNING_STUNTS_SPECIAL	= 24,						
	PCS_BS_GUNRUNNING				= 25,										
	PCS_BS_SMUGGLERS_RUN			= 26,								
	PCS_BS_HEISTS_2017				= 27,								
	PCS_BS_FESTIVE_SURPRISE_2017	= 28,
	PCS_BS_BUSINESS_BATTLES			= 29,
	PCS_BS_ARENA_WARS				= 30,
	PCS_BS_FESTIVE_SURPRISE_2018	= 31,
	PCS_BS_VINEWOOD					= 32,
	PCS_BS_CASINO_HEIST				= 33,
	
	// If you add a new variant so that PCS_BS_NUM_PACKS (below) is equal to a multiple of 32 please increase
	// CONST_INT PACK_CLOTHES_SALE_BS_COUNT by 1 (found in mp_globals_tunables.sch).
	//
	// Please also note that this means the tunable that neesd to be set will be
	// "PACK_CLOTHES_SALE_[PACK_CLOTHES_SALE_BS_COUNT-1]" and the value required to set these items on sale will
	// be 1 (see PCS_BS_BEACH_BUM).
	PCS_BS_NUM_PACKS		
ENDENUM

FUNC BOOL GET_TUNABLE_PACK_SALE_VALUE(PACK_CLOTHES_SALE_BS ePack)
	INT iPack = ENUM_TO_INT(ePack)
	INT iBitset = iPack / 32
	INT iBit = iPack % 32

	IF iBitset < PACK_CLOTHES_SALE_BS_COUNT
		RETURN IS_BIT_SET(g_sMPTunables.iBSPackClothesSale[iBitset], iBit)
	ENDIF
	
	PRINTLN("[GET_TUNABLE_PACK_SALE_VALUE] Couldn't find PACK_CLOTHES_SALE_BS value ", iPack)
	RETURN FALSE
ENDFUNC 

PROC SET_TUNABLE_PACK_SET_VALUE(PACK_CLOTHES_SALE_BS ePack)
	INT iPack = ENUM_TO_INT(ePack)
	INT iBitset = iPack / 32
	INT iBit = iPack % 32
	
	IF iBitset < PACK_CLOTHES_SALE_BS_COUNT
		SET_BIT(g_sMPTunables.iBSPackClothesSale[iBitset], iBit)
		EXIT
	ENDIF
	PRINTLN("[SET_TUNABLE_PACK_SET_VALUE] Couldn't find PACK_CLOTHES_SALE_BS value ", iPack)
ENDPROC
PROC CLEAR_TUNABLE_PACK_SET_VALUE(PACK_CLOTHES_SALE_BS ePack)
	INT iPack = ENUM_TO_INT(ePack)
	INT iBitset = iPack / 32
	INT iBit = iPack % 32
	
	IF iBitset < PACK_CLOTHES_SALE_BS_COUNT
		CLEAR_BIT(g_sMPTunables.iBSPackClothesSale[iBitset], iBit)
		EXIT
	ENDIF
	PRINTLN("[CLEAR_TUNABLE_PACK_SET_VALUE] Couldn't find PACK_CLOTHES_SALE_BS value ", iPack)
ENDPROC

//NOTE: Need to add to this when new pack is added! Just Like FUNC STRING GET_SHOP_CONTENT_FOR_MENU(STRING sLabel, BOOL bUseSecondaryLabel = FALSE)
FUNC PACK_CLOTHES_SALE_BS GET_PACK_CLOTHES_SALE_ENUM_FROM_STRING(STRING sLabel)
	INT iParamHash = GET_HASH_KEY(sLabel)
	
	IF iParamHash = 0
		RETURN PCS_BS_INVALID
	ENDIF
	
	INT iLabelHash = GET_HASH_KEY(GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(sLabel, 0, 6))
	
	SWITCH iLabelHash
		CASE HASH("CLO_BB") 		
		CASE HASH("TAT_BB") 		
			RETURN PCS_BS_BEACH_BUM	
	
		
		CASE HASH("CLO_BU") 		
		CASE HASH("TAT_BU") 		
			RETURN PCS_BS_BUSINESS
		
		
		CASE HASH("CLO_XM") 		
		CASE HASH("CLO_X2") 		
		CASE HASH("TAT_X2") 		
			RETURN PCS_BS_FESTIVE_SURPRISE
		
		CASE HASH("CLO_VA") 	
			RETURN PCS_BS_VALENTINES
		
		CASE HASH("CLO_HI") 		
			RETURN PCS_BS_HIGH_LIFE
		
		CASE HASH("CLO_HP") 				
		CASE HASH("TAT_HP") 				
		CASE HASH("BRD_HP") 			
			RETURN PCS_BS_IM_NOT_A_HIPSTER
		
		CASE HASH("CLO_HS")
			RETURN PCS_BS_HEISTS_2015
		
		CASE HASH("CLO_IN")			
			RETURN PCS_BS_INDEPENDENCE_DAY
		
		CASE HASH("CLO_PI") 				
			RETURN PCS_BS_FLIGHT_SCHOOL
		
		CASE HASH("CLO_LT") 			
			RETURN PCS_BS_LTS_CREATOR
		
		CASE HASH("CLO_LX") 				
		CASE HASH("TAT_LX") 			
			IF (iParamHash = HASH("CLO_LXM_DEC_30"))	// Fake Dix Gold T-Shirt
			OR (iParamHash = HASH("CLO_LXM_DEC_33"))	// Fake Le Chien No2 T-Shirt
			OR (iParamHash = HASH("CLO_LXM_DEC_38"))	// Fake Sessanta Nova T-Shirt
			
			OR (iParamHash = HASH("CLO_LXF_DEC_30"))	// Fake Dix Gold T-Shirt
			OR (iParamHash = HASH("CLO_LXF_DEC_33"))	// Fake Le Chien No2 T-Shirt
			OR (iParamHash = HASH("CLO_LXF_DEC_38"))	// Fake Sessanta Nova T-Shirt
				RETURN PCS_BS_ILL_GOTTEN_GAINS_II
			ENDIF
			
			RETURN PCS_BS_ILL_GOTTEN_GAINS_I
		
		CASE HASH("CLO_VE") 				
			SWITCH iParamHash
				CASE HASH("CLO_VEM_DEC_44") // Nelson In Naples
				CASE HASH("CLO_VEM_DEC_45") // The Many Wives of Alfredo Smith
				CASE HASH("CLO_VEM_DEC_46") // An American Divorce
				CASE HASH("CLO_VEM_DEC_48") // Capolavoro
				CASE HASH("CLO_VEF_DEC_44") // Nelson In Naples
				CASE HASH("CLO_VEF_DEC_45") // The Many Wives of Alfredo Smith
				CASE HASH("CLO_VEF_DEC_46") // An American Divorce
				CASE HASH("CLO_VEF_DEC_48") // Capolavoro
					RETURN PCS_BS_INVALID
			ENDSWITCH
			RETURN PCS_BS_EDITOR_AND_FM_EVENTS
		
		CASE HASH("CLO_L2") 				
		CASE HASH("TAT_L2") 			
			RETURN PCS_BS_ILL_GOTTEN_GAINS_II
		
		CASE HASH("CLO_S1") 				
		CASE HASH("TAT_S1") 				
			RETURN PCS_BS_LOWRIDERS
		
		CASE HASH("CLO_HA") 			
		CASE HASH("TAT_HA") 			
			RETURN PCS_BS_HALLOWEEN
		
		CASE HASH("CLO_S2")
		CASE HASH("TAT_S2") 			
			RETURN PCS_BS_LOWRIDER_2
		
		CASE HASH("CLO_AP") 				
		CASE HASH("TAT_AP") 			
			RETURN PCS_BS_APARTMENTS
		
		CASE HASH("CLO_X3") 				
		CASE HASH("TAT_X3") 			
			IF (iParamHash = HASH("CLO_X3F_DEC_00"))
			OR (iParamHash = HASH("CLO_X3M_DEC_00"))
				RETURN PCS_BS_INVALID
			ENDIF
			
			RETURN PCS_BS_FESTIVE_SURPRISE_2015
		
		CASE HASH("CLO_X4")
			RETURN PCS_BS_FESTIVE_SURPRISE_2016
		
		CASE HASH("CLO_V2") 		
			RETURN PCS_BS_BE_MY_VALENTINE
		
		CASE HASH("CLO_EX") 		
			RETURN PCS_BS_FINANCE_AND_FELONY
		
		CASE HASH("CLO_ST")
		CASE HASH("TAT_ST")
			RETURN PCS_BS_CUNNING_STUNTS
		
		CASE HASH("CLO_BI")
		CASE HASH("TAT_BI")
			RETURN PCS_BS_BIKERS
		
		CASE HASH("CLO_IE")
		CASE HASH("TAT_IE")
			RETURN PCS_BS_IMPORT_EXPORT
		
		CASE HASH("CLO_GR")
		CASE HASH("TAT_GR")
			RETURN PCS_BS_GUNRUNNING
		
		CASE HASH("CLO_SM")
		CASE HASH("TAT_SM")
			RETURN PCS_BS_SMUGGLERS_RUN
		
		CASE HASH("CLO_AR")
		CASE HASH("TAT_AR")
			RETURN PCS_BS_SMUGGLERS_RUN
	
		CASE HASH("CLO_H2")
		CASE HASH("TAT_H2")		// "Doomsday Heist"
			RETURN PCS_BS_HEISTS_2017
		
		CASE HASH("CLO_X1")
		CASE HASH("TAT_X1")		// "Festive Surprise 2017"
			RETURN PCS_BS_FESTIVE_SURPRISE_2017
		
		CASE HASH("CLO_AW")
		CASE HASH("TAT_AW")
			RETURN PCS_BS_ARENA_WARS
			
		CASE HASH("CLO_X5")
		CASE HASH("TAT_X5")		// "Festive Surprise 2018"
			RETURN PCS_BS_FESTIVE_SURPRISE_2018
			
		CASE HASH("CLO_VW")
		CASE HASH("TAT_VW")
			RETURN PCS_BS_VINEWOOD
			
		CASE HASH("CLO_H3")
		CASE HASH("TAT_H3")		// "Casino Heist"
			RETURN PCS_BS_CASINO_HEIST
		
	ENDSWITCH
	RETURN PCS_BS_INVALID
ENDFUNC
