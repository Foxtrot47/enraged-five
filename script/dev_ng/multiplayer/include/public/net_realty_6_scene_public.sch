USING "SceneTool_public.sch"

USING "cutscene_public.sch"
USING "rc_helper_functions.sch"

#IF IS_DEBUG_BUILD
USING "net_realty_scene_debug.sch"
#ENDIF

USING "net_cutscene.sch"
USING "net_spawn.sch"

ENUM enumNET_REALTY_6_SCENEPans
	NET_REALTY_6_SCENE_PAN_null,
	NET_REALTY_6_SCENE_PAN_MAX
ENDENUM

ENUM enumNET_REALTY_6_SCENECuts
	NET_REALTY_6_SCENE_CUT_shot = 0,
	NET_REALTY_6_SCENE_CUT_MAX
ENDENUM

ENUM enumNET_REALTY_6_SCENEMarkers
	NET_REALTY_6_SCENE_MARKER_null,
	NET_REALTY_6_SCENE_MARKER_MAX
ENDENUM

ENUM enumNET_REALTY_6_SCENEPlacers
	NET_REALTY_6_SCENE_PLACER_startCoords,
	NET_REALTY_6_SCENE_PLACER_driveTo,
	NET_REALTY_6_SCENE_PLACER_MAX
ENDENUM

STRUCT STRUCT_NET_REALTY_6_SCENE
	structSceneTool_Pan		mPans[NET_REALTY_6_SCENE_PAN_MAX]
	structSceneTool_Cut		mCuts[NET_REALTY_6_SCENE_CUT_MAX]
	
	structSceneTool_Marker	mMarkers[NET_REALTY_6_SCENE_MARKER_MAX]
	structSceneTool_Placer	mPlacers[NET_REALTY_6_SCENE_PLACER_MAX]
	
	BOOL					bEnablePans[NET_REALTY_6_SCENE_PAN_MAX]
	VECTOR 					vDoorPos1
	VECTOR 					vDoorPos2
	VECTOR 					vDoorPos3
	FLOAT					fExitDelay
	BOOL 					bSetCarLockedInvulnerable
ENDSTRUCT

CONST_INT DRIVE_OUT_FAKE_PEDS	4

CONST_INT FAKE_CAR_EXIT_GARAGE_WARP_POSSIBLE		0
CONST_INT FAKE_CAR_EXIT_GARAGE_WARP_DONE			1
CONST_INT FAKE_CAR_EXIT_GARAGE_ADDED_CUST_SPAWN		2

STRUCT DRIVE_OUT_GARAGE_CS_STRUCT
	VEHICLE_INDEX vehClone
	PED_INDEX pedClones[DRIVE_OUT_FAKE_PEDS]
	INT iPedsInCar
	INT iBS
ENDSTRUCT

FUNC BOOL Private_Get_NET_REALTY_6_SCENE(INT iBuildingID, STRUCT_NET_REALTY_6_SCENE& scene	#IF ACTIVATE_PROPERTY_CS_DEBUG	,	BOOL bIgnoreAssert = FALSE	#ENDIF	)
	SWITCH iBuildingID
		CASE 0	RETURN FALSE	BREAK
		
		//Luke Howard, 08-09-2013 (#1627391)
		CASE MP_PROPERTY_BUILDING_1	//High apt 1,2,3,4
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vPos = <<0.0000, 0.0000, 0.0000>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vRot = <<0.0000, 0.0000, 0.0000>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vPos = <<0.0000, 0.0000, 0.0000>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vRot = <<0.0000, 0.0000, 0.0000>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fFov = 0.0000
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fShake = 0.0000
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fDuration = 0.0000
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vPos = <<-793.3801, 321.0642, 85.8677>>
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vRot = <<2.3849, -0.0000, 164.3360>>
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fFov = 50.0000
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fShake = 0.2500
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos = <<-795.8162, 322.7209, 84.7015>>
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].fRot = 179.8355
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos = <<-795.6993, 301.1959, 84.7075>>
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].fRot = 179.8355
			scene.vDoorPos1 = <<-798.967651,313.991486,84.705750>> 
			scene.vDoorPos2 = <<-795.849182,314.308655,84.701675>>
			scene.vDoorPos3 = <<-792.878845,314.504852,84.699097>>
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke Howard, 08-09-2013 (#1627391)
		CASE MP_PROPERTY_BUILDING_3	//High apt 7,8
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vPos = <<-1452.4291, -495.9077, 38.5501>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vRot = <<-22.7824, 0.0000, -9.0439>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vPos = <<-1452.4291, -495.9077, 38.5501>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vRot = <<-22.7824, 0.0000, -9.0439>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fFov = 50.0000
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fShake = 0.2500
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fDuration = 6.5000
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vPos = <<-1454.0414, -510.0159, 31.4477>>
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vRot = <<3.9470, -0.0000, 19.6017>>
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fFov = 50.0000
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fShake = 0.2500
//			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos = <<-1450.4836, -511.1783, 30.8522>>
//			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].fRot = 35.0000
//			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos = <<-1458.9091, -498.2391, 31.9241>>
//			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].fRot = 322.5000
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos = <<-1450.5439, -511.1783, 30.8522>>
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].fRot = 35.0000
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos = <<-1458.9189, -496.1591, 31.9241>>
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].fRot = 322.5000
			scene.vDoorPos1 = <<-282.026947,-993.643066,23.303766>> 
			scene.vDoorPos2 = <<-283.157166,-996.661743,23.483461>>
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke Howard, 08-09-2013 (#1627391)
		CASE MP_PROPERTY_BUILDING_4	//High apt 9,10,11
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vPos = <<-810.3313, -418.4649, 35.9807>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vRot = <<-25.1259, 0.0000, 112.0883>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vPos = <<-810.3313, -418.4649, 35.9807>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vRot = <<-25.1259, 0.0000, 112.0883>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fFov = 50.0000
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fShake = 0.2500
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fDuration = 6.5000
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vPos = <<-810.4244, -428.7455, 36.2140>>
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vRot = <<4.3253, 0.0000, 128.9352>>
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fFov = 50.0000
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fShake = 0.2500
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos = <<-812.6725, -423.6420, 33.5552>>
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].fRot = 153.7500
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos = <<-828.6069, -448.3151, 35.6399>>
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].fRot = 75.0000
			scene.vDoorPos1 = <<-819.302307,-438.809143,35.626488>> 
			scene.vDoorPos2 = <<-821.377380,-435.120636,35.794361>> 
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke Howard, 08-09-2013 (#1627391)
		CASE MP_PROPERTY_BUILDING_6	//High apt 14,15
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vPos = <<-884.4767, -350.0993, 35.9389>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vRot = <<-1.6140, 0.0000, -148.2025>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vPos = <<-884.4767, -350.0993, 35.9389>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vRot = <<-1.6140, 0.0000, -148.2025>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fFov = 50.0000
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fShake = 0.2500
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fDuration = 6.5000
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vPos = <<-879.5704, -351.3455, 34.9938>>
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vRot = <<5.7711, -0.0000, -169.6314>>
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fFov = 50.0047
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fShake = 0.2500
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos = <<-882.8714, -350.0053, 33.6581>>
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].fRot = 207.3600
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos = <<-871.5641, -372.2396, 37.9079>>
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].fRot = 207.0000
			scene.vDoorPos1 = <<-876.418823,-358.469299,34.920353>> 
			scene.vDoorPos2 = <<-879.781738,-360.224945,34.828461>> 
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke Howard, 08-09-2013 (#1627391)
		CASE MP_PROPERTY_BUILDING_7	//High apt 16,17
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vPos = <<-624.2532, 53.5243, 44.6045>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vRot = <<-2.7979, 0.0000, -114.5529>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vPos = <<-624.2532, 53.5243, 44.6045>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vRot = <<-2.7979, 0.0000, -114.5529>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fFov = 50.0000
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fShake = 0.2500
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fDuration = 6.5000
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vPos = <<-622.4495, 55.0172, 44.2430>>
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vRot = <<0.0871, 0.0000, 78.4050>>
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fFov = 50.0000
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fShake = 0.2500
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos = <<-620.2952, 58.3260, 42.7365>>
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].fRot = 100.3200
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos = <<-645.2898, 57.5593, 43.0006>>
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].fRot = 89.8800
			scene.vDoorPos1 = <<-629.773926,59.266457,42.724987>>
			scene.vDoorPos2 = <<-629.822937,55.832752,42.724960>> 
			scene.vDoorPos3 = <<-629.732056,52.372917,42.725010>> 
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke Howard, 08-09-2013 (#1627391)
		CASE MP_PROPERTY_BUILDING_5	//High apt 12,13
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vPos = <<-41.7185, -620.0758, 35.5620>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vRot = <<0.5557, 0.0000, -101.1061>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vPos = <<-41.7185, -620.0758, 35.5620>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vRot = <<0.5557, 0.0000, -101.1061>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fFov = 50.0000
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fShake = 0.2500
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fDuration = 6.5000
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vPos = <<-36.1208, -618.8893, 35.3674>>
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vRot = <<-3.3754, 0.0307, -135.9231>>
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fFov = 50.0000
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fShake = 0.2500
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos = <<-38.2102, -620.6124, 34.0848>>
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].fRot = 251.6400
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos = <<-23.1606, -625.4481, 34.7032>>
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].fRot = 251.6400
			scene.vDoorPos1 = <<-33.880001,-621.538513,34.038071>> 
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke Howard, 08-09-2013 (#1627391)
		CASE MP_PROPERTY_BUILDING_2          //High apt 5,6
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vPos = <<-242.7861, -1004.2469, 30.7049>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vRot = <<-14.4323, 0.0000, 66.8627>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vPos = <<-242.7861, -1004.2469, 30.7049>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vRot = <<-14.4323, 0.0000, 66.8627>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fFov = 50.0000
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fShake = 0.2500
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fDuration = 6.5000
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vPos = <<-288.3694, -991.2920, 24.4412>>
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vRot = <<0.2085, -0.0000, -121.1736>>
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fFov = 50.0430
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fShake = 0.2500
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos = <<-290.5520, -994.1109, 23.1368>>
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].fRot = 250.0000
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos = <<-265.1293, -1003.4581, 26.1927>>
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].fRot = 248.7600
			scene.vDoorPos1 = <<-282.026947,-993.643066,23.303766>> 
			scene.vDoorPos2 = <<-283.157166,-996.661743,23.483461>>
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//#1617991
//		CASE MP_PROPERTY_BUILDING_16	//Med apt 9		//Commented out for bug 1629904
//			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vPos = <<-759.1165, -750.4486, 30.3282>>
//			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vRot = <<-26.3503, -0.0000, 144.9728>>
//			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vPos = <<-759.1165, -750.4486, 30.3282>>
//			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vRot = <<-26.3503, -0.0000, 144.9728>>
//			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fFov = 50.0000
//			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fShake = 0.2500
//			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fDuration = 6.5000
//			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vPos = <<-786.2479, -798.5568, 22.7781>>
//			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vRot = <<-2.0878, -0.0000, -176.9901>>
//			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fFov = 50.0000
//			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fShake = 0.2500
//			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos = <<-787.0496, -800.7537, 19.6203>>
//			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].fRot = 180.0000
//			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos = <<-785.5596, -802.9417, 19.6625>>
//			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].fRot = 180.0000
//			scene.bEnablePans[0] = FALSE
//			scene.fExitDelay = 0.0000
//			RETURN TRUE
//		BREAK
		
		//Luke Howard, 20/09/2013 15:38 (#1642223)
		CASE MP_PROPERTY_BUILDING_23	//Low apt 7
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vPos = <<-1612.9268, -457.2170, 42.0234>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vRot = <<-28.5862, 0.0000, -33.6129>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vPos = <<-1612.9268, -457.2170, 42.0234>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vRot = <<-28.5862, 0.0000, -33.6129>>
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fFov = 50.0000
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fShake = 0.2500
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fDuration = 6.5000
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vPos = <<-1598.1476, -442.8423, 38.1364>>
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vRot = <<1.2301, 0.0000, 124.0840>>
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fFov = 50.0010
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fShake = 0.2500
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos = <<-1599.0793, -439.5631, 37.2166>>
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].fRot = 138.7500
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos = <<-1613.1024, -456.1613, 37.0932>>
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].fRot = 139.5000
			scene.vDoorPos1 = <<-1606.561279,-445.916870,37.216648>>
			scene.vDoorPos2 = <<-1603.390503,-448.632263,37.216648>> 
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//#1623715 - placeholder
//		CASE MP_PROPERTY_BUILDING_19
//			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vPos = <<-802.0515, -981.4214, 13.9060>>
//			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vRot = <<3.9901, -0.0000, -143.3916>>
//			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vPos = <<-802.0515, -981.4214, 13.9060>>
//			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vRot = <<3.9901, -0.0000, -143.3916>>
//			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fFov = 50.0000
//			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fShake = 0.2500
//			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fDuration = 6.5000
//			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vPos = <<-800.6073, -980.9600, 14.5789>>
//			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vRot = <<2.5651, -0.0000, -148.8183>>
//			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fFov = 50.0000
//			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fShake = 0.2500
//			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos = <<-798.3298, -983.8885, 12.9190>>
//			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].fRot = 225.0000
//			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos = <<-795.6094, -986.3440, 12.8848>>
//			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].fRot = 225.0000
//			scene.bEnablePans[0] = FALSE
//			scene.fExitDelay = 0.0000
//			RETURN TRUE
//		BREAK
		
		//#1625322 - placeholder
		CASE MP_PROPERTY_BUILDING_20
			RETURN FALSE
		BREAK
		
		//#1635108 - placeholder
		CASE MP_PROPERTY_BUILDING_49
			RETURN FALSE
		BREAK
		
		//#1639542 - placeholder
		CASE MP_PROPERTY_BUILDING_56
			RETURN FALSE
		BREAK
		
		DEFAULT
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vPos = GET_GAMEPLAY_CAM_COORD()
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mStart.vRot = GET_GAMEPLAY_CAM_ROT()
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vPos = GET_GAMEPLAY_CAM_COORD()
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vRot = GET_GAMEPLAY_CAM_ROT()
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fFov = GET_GAMEPLAY_CAM_FOV()
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fShake = 0.25
			scene.mPans[NET_REALTY_6_SCENE_PAN_null].fDuration = 6.500
			scene.bEnablePans[NET_REALTY_6_SCENE_PAN_null] = FALSE
			
			scene.fExitDelay = 0.0000
			
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vPos = scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vPos
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].mCam.vRot = scene.mPans[NET_REALTY_6_SCENE_PAN_null].mEnd.vRot
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fFov = scene.mPans[NET_REALTY_6_SCENE_PAN_null].fFov
			scene.mCuts[NET_REALTY_6_SCENE_CUT_shot].fShake = scene.mPans[NET_REALTY_6_SCENE_PAN_null].fShake
			
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos = GET_MP_PROPERTY_BUILDING_WORLD_POINT(iBuildingID)
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].fRot = 0
			
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos = GET_MP_PROPERTY_BUILDING_WORLD_POINT(iBuildingID)
			scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].fRot = 0
			
			scene.mMarkers[NET_REALTY_6_SCENE_PAN_null].vPos = scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos			+ <<1.0, 0.0, 0.0>>
			
			#IF ACTIVATE_PROPERTY_CS_DEBUG
			IF NOT bIgnoreAssert
				TEXT_LABEL_63 str
				str = "Private_Get_NET_REALTY_6_SCENE() invalid ID "
				str += GET_STRING_FROM_STRING(Private_Get_Net_Realty_Building_Name(iBuildingID), GET_LENGTH_OF_LITERAL_STRING("MP_PROPERTY_"), GET_LENGTH_OF_LITERAL_STRING(Private_Get_Net_Realty_Building_Name(iBuildingID)))
				//SCRIPT_ASSERT(str)
				PRINTLN(str)
			ENDIF
			#ENDIF
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


ENUM GARAGE_CAR_LEAVE_CUTSCENE_STAGE
	GARAGE_CAR_LEAVE_CUTSCENE_INIT = 0,
	GARAGE_CAR_LEAVE_CUTSCENE_TRIGGER_CUT,
	GARAGE_CAR_LEAVE_CUTSCENE_WAIT_FOR_OPEN_DOOR,
	GARAGE_CAR_LEAVE_CUTSCENE_RE_TASK,
	GARAGE_CAR_LEAVE_CUTSCENE_WAIT_FOR_EXIT,
	
	GARAGE_CAR_WARP_TO_UNOCCUPIED_AREA,
	GARAGE_CAR_SET_IN_UNOCCUPIED_AREA,
	
	GARAGE_CAR_LEAVE_CUTSCENE_COMPLETE
ENDENUM

FUNC BOOL NET_REALTY_6_SCENE_IS_DOOR_BLOCKED(STRUCT_NET_REALTY_6_SCENE &scene)
	FLOAT fRadius = 2.0
	IF NOT ARE_VECTORS_EQUAL(<<0,0,0>>,scene.vDoorPos1)
		IF IS_POSITION_OCCUPIED(
						scene.vDoorPos1,
						fRadius,
						FALSE, TRUE, FALSE, FALSE, FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	IF NOT ARE_VECTORS_EQUAL(<<0,0,0>>,scene.vDoorPos2)
		IF IS_POSITION_OCCUPIED(
						scene.vDoorPos2,
						fRadius,
						FALSE, TRUE, FALSE, FALSE, FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	IF NOT ARE_VECTORS_EQUAL(<<0,0,0>>,scene.vDoorPos1)
		IF IS_POSITION_OCCUPIED(
						scene.vDoorPos3,
						fRadius,
						FALSE, TRUE, FALSE, FALSE, FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//BOOL bRollingStart

PROC SET_CUTSCENE_COMPLETE(GARAGE_CAR_LEAVE_CUTSCENE_STAGE &garageCarLeaveCutStage
		,STRUCT_NET_REALTY_6_SCENE &scene, DRIVE_OUT_GARAGE_CS_STRUCT &data  )
	INT i
	IF scene.bSetCarLockedInvulnerable
		CLEANUP_MP_CUTSCENE(DEFAULT,FALSE)
		IF DOES_ENTITY_EXIST(data.vehClone)
			DELETE_VEHICLE(data.vehClone)
		ENDIF
		REPEAT data.iPedsInCar i
			IF DOES_ENTITY_EXIST(data.pedClones[i])
				DELETE_PED(data.pedClones[i])
			ENDIF
		ENDREPEAT
	ENDIF
	CLEAR_BIT(mpPropIntRequestExtDoor.iBS,MP_PROP_INT_REQUEST_EXT_DOOR_OPEN)
	garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_COMPLETE
ENDPROC

PROC DO_GARAGE_CAR_LEAVE_WARP(INT iPropertyEntered,DRIVE_OUT_GARAGE_CS_STRUCT &fakeExitData)
	IF IS_BIT_SET(fakeExitData.iBS,FAKE_CAR_EXIT_GARAGE_WARP_POSSIBLE)
	AND NOT IS_BIT_SET(fakeExitData.iBS,FAKE_CAR_EXIT_GARAGE_WARP_DONE)
		IF NOT IS_BIT_SET(fakeExitData.iBS,FAKE_CAR_EXIT_GARAGE_ADDED_CUST_SPAWN)

			USE_CUSTOM_SPAWN_POINTS(TRUE,FALSE,FALSE)
			ADD_CUSTOM_SPAWN_POINT(mpProperties[iPropertyEntered].garage.vCarExitLoc[0],mpProperties[iPropertyEntered].garage.fCarExitHeading[0],mpProperties[iPropertyEntered].garage.fCarExitWeight[0])
			ADD_CUSTOM_SPAWN_POINT(mpProperties[iPropertyEntered].garage.vCarExitLoc[1],mpProperties[iPropertyEntered].garage.fCarExitHeading[1],mpProperties[iPropertyEntered].garage.fCarExitWeight[1])
			ADD_CUSTOM_SPAWN_POINT(mpProperties[iPropertyEntered].garage.vCarExitLoc[2],mpProperties[iPropertyEntered].garage.fCarExitHeading[2],mpProperties[iPropertyEntered].garage.fCarExitWeight[2])
			ADD_CUSTOM_SPAWN_POINT(mpProperties[iPropertyEntered].garage.vCarExitLoc[3],mpProperties[iPropertyEntered].garage.fCarExitHeading[3],mpProperties[iPropertyEntered].garage.fCarExitWeight[3])
			ADD_CUSTOM_SPAWN_POINT(mpProperties[iPropertyEntered].garage.vCarExitLoc[4],mpProperties[iPropertyEntered].garage.fCarExitHeading[4],mpProperties[iPropertyEntered].garage.fCarExitWeight[4])
			SET_BIT(fakeExitData.iBS,FAKE_CAR_EXIT_GARAGE_ADDED_CUST_SPAWN)
		ELSE
			IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS,FALSE,FALSE,FALSE,TRUE,FALSE,FALSE,TRUE,TRUE,2000)
				#IF IS_DEBUG_BUILD
				PRINTLN("DO_GARAGE_CAR_LEAVE_WARP: NETWORK_FADE_IN_ENTITY -14353 ")
				#ENDIF
				NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE)
				CLEAR_CUSTOM_SPAWN_POINTS()
				SET_BIT(fakeExitData.iBS,FAKE_CAR_EXIT_GARAGE_WARP_DONE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_GARAGE_CAR_LEAVE_THIS_CUTSCENE(STRUCT_NET_REALTY_6_SCENE &scene,
		GARAGE_CAR_LEAVE_CUTSCENE_STAGE &garageCarLeaveCutStage,
		INT &iGarageCutsceneBitset, INT &iGarageCutTimer,
		CAMERA_INDEX &camGarageCutscene, INT iPropertyEntered, PLAYER_INDEX ownerID, INT &iOwnerProgress, VEHICLE_INDEX &vehiclePlayerIsIn
		, DRIVE_OUT_GARAGE_CS_STRUCT &data  )
	
	BOOL bRESET_GARAGE_CAR_LEAVE_CUTSCENE = FALSE
	
	CONST_INT GARAGE_CUT_SKIP_TIME_SHORT			5000
	CONST_INT GARAGE_CUT_SKIP_TIME_LONG				10000
	
	CONST_INT GARAGE_CUT_BS_INTERPOLATE_STARTED		0
	CONST_INT GARAGE_CUT_BS_CONTROL_RETURNED		1
	
	//VEHICLE_INDEX vehiclePlayerIsIn
	INT i
	PED_INDEX pedTemp
	MODEL_NAMES modelTemp
	
	#IF IS_DEBUG_BUILD
	FLOAT fDistance
	#ENDIF
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		DO_GARAGE_CAR_LEAVE_WARP(iPropertyEntered,data)
		VECTOR vMP_PROPERTY_BUILDING_3_driveto = <<-1455.0319, -490.8658, 33.5>>//vMP_PROPERTY_BUILDING_3_driveto = <<-1455.1458, -490.9381, 32.8011>>
		BOOL bMP_PROPERTY_BUILDING_3 = FALSE
		IF VDIST2(scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos, <<-1450.4836, -511.1783, 30.8522>>) < 10.0
			bMP_PROPERTY_BUILDING_3 = TRUE
		ENDIF
		IF ownerID = PLAYER_ID()
			iOwnerProgress = ENUM_TO_INT(garageCarLeaveCutStage)
		ENDIF
		SWITCH garageCarLeaveCutStage
			CASE GARAGE_CAR_LEAVE_CUTSCENE_INIT
				IF GET_PROPERTY_SIZE_TYPE(iPropertyEntered) = PROP_SIZE_TYPE_LARGE_APT //GET_PROPERTY_BUILDING(iPropertyEntered) = MP_PROPERTY_BUILDING_1
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
						vehiclePlayerIsIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
						IF IS_VEHICLE_DRIVEABLE(vehiclePlayerIsIn)
							PRINTLN("DO_GARAGE_CAR_LEAVE_THIS_CUTSCENE- grabbing clones")
							IF ownerID = PLAYER_ID()
								SET_VEHICLE_DOORS_LOCKED(vehiclePlayerIsIn,VEHICLELOCK_CANNOT_ENTER)
								SET_ENTITY_CAN_BE_DAMAGED(vehiclePlayerIsIn,FALSE)
							ENDIF
							//Clones
							CREATE_CUTSCENE_VEHICLE_CLONE(data.vehClone, vehiclePlayerIsIn, GET_ENTITY_COORDS(vehiclePlayerIsIn)-<<0,0,10>>, GET_ENTITY_HEADING(vehiclePlayerIsIn))

							data.iPedsInCar = 0
							REPEAT DRIVE_OUT_FAKE_PEDS i
								IF i < GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(vehiclePlayerIsIn))
									IF NOT IS_VEHICLE_SEAT_FREE(vehiclePlayerIsIn, INT_TO_ENUM(VEHICLE_SEAT, -1 + i))
										pedTemp = GET_PED_IN_VEHICLE_SEAT(vehiclePlayerIsIn, INT_TO_ENUM(VEHICLE_SEAT, -1 + i))
										
										IF DOES_ENTITY_EXIST(pedTemp)
											modelTemp = GET_ENTITY_MODEL(pedTemp)
											
											IF IS_PED_MALE(pedTemp) 
												data.pedClones[i] = CREATE_PED(PEDTYPE_CIVMALE, modelTemp, GET_ENTITY_COORDS(pedTemp) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(pedTemp), FALSE, FALSE)
											ELSE
												data.pedClones[i] = CREATE_PED(PEDTYPE_CIVFEMALE, modelTemp, GET_ENTITY_COORDS(pedTemp) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(pedTemp), FALSE, FALSE)
											ENDIF
											CLONE_PED_TO_TARGET(pedTemp, data.pedClones[i])
											
											data.iPedsInCar++
											TASK_ENTER_VEHICLE(data.pedClones[i], data.vehClone, 1, INT_TO_ENUM(VEHICLE_SEAT, -1 + i), DEFAULT, ECF_WARP_PED)
										ENDIF
									ENDIF
								ENDIF
							ENDREPEAT
							IF IS_VEHICLE_DRIVEABLE(data.vehClone)
								SET_VEHICLE_LIGHTS(data.vehClone, SET_VEHICLE_LIGHTS_ON)
							ENDIF
							
							garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_TRIGGER_CUT
							iGarageCutTimer = GET_GAME_TIMER()
							
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_TRIGGER_CUT")
							#ENDIF
						ENDIF
					ENDIF
				ELSE
				garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_TRIGGER_CUT
								
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_TRIGGER_CUT")
				#ENDIF
				ENDIF
			BREAK
			
			CASE GARAGE_CAR_LEAVE_CUTSCENE_TRIGGER_CUT
				IF GET_PROPERTY_SIZE_TYPE(iPropertyEntered) = PROP_SIZE_TYPE_LARGE_APT //GET_PROPERTY_BUILDING(iPropertyEntered) = MP_PROPERTY_BUILDING_1
					IF (GET_GAME_TIMER() - iGarageCutTimer) < 5000
						REPEAT data.iPedsInCar i
							IF DOES_ENTITY_EXIST(data.pedClones[i])
								IF NOT IS_PED_INJURED(data.pedClones[i])
									IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(data.pedClones[i])
										PRINTLN("DO_GARAGE_CAR_LEAVE_THIS_CUTSCENE Waiting on streaming for ped #",i )
										RETURN FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(vehiclePlayerIsIn)
					
							IF IS_NET_PLAYER_OK(ownerID)
								IF ownerID != PLAYER_ID()
								AND iOwnerProgress <= ENUM_TO_INT(GARAGE_CAR_LEAVE_CUTSCENE_TRIGGER_CUT)
									PRINTLN("CDM: Waiting for owner to finish init")
									RETURN FALSE
								ENDIF
							ENDIF

							//  //  //  //  //  //  //  //  // 
							
													//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						
							iGarageCutsceneBitset = 0
						
							iGarageCutTimer = GET_GAME_TIMER()
							IF ownerID = PLAYER_ID()
								SET_VEHICLE_DOORS_LOCKED(vehiclePlayerIsIn,VEHICLELOCK_CANNOT_ENTER)
								SET_ENTITY_CAN_BE_DAMAGED(vehiclePlayerIsIn,FALSE)
								SET_ENTITY_VISIBLE_IN_CUTSCENE(vehiclePlayerIsIn,FALSE,TRUE)
								PRINTLN("DO_GARAGE_CAR_LEAVE_THIS_CUTSCENE setting vehicle as invisible in cutscene")
//								SET_ENTITY_COORDS(vehiclePlayerIsIn, scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos)
//								SET_ENTITY_HEADING(vehiclePlayerIsIn, scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].fRot)
//								NETWORK_FADE_IN_ENTITY(vehiclePlayerIsIn,TRUE)
//								PRINTLN("DO_GARAGE_CAR_LEAVE_THIS_CUTSCENE: setting vehicle position to be: ", scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos)
							ENDIF
							IF NOT IS_PLAYER_IN_CUTSCENE(PLAYER_ID())
								SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE,TRUE)
								START_MP_CUTSCENE(TRUE)
							ENDIF
							SET_BIT(mpPropIntRequestExtDoor.iBS,MP_PROP_INT_REQUEST_EXT_DOOR_OPEN)
							IF IS_VEHICLE_DRIVEABLE(data.vehClone)
								SET_ENTITY_COORDS(data.vehClone, scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos)
								SET_ENTITY_HEADING(data.vehClone, scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].fRot)
							ENDIF
							
							scene.bSetCarLockedInvulnerable = TRUE
							
							camGarageCutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
							SET_CAM_ACTIVE(camGarageCutscene, TRUE)
							SET_CAM_PARAMS(	camGarageCutscene,
									<<-800.4786, 308.6443, 88.1763>>, 
									<<-17.3271, -0.0000, -26.6875>>, 
									50.0083)
							SHAKE_CAM(camGarageCutscene, "HAND_SHAKE", 0.25)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							//SceneTool_ExecuteCut(scene.mCuts[NET_REALTY_6_SCENE_CUT_shot], camGarageCutscene)
							
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_WAIT_FOR_OPEN_DOOR")
							#ENDIF
							
							IF NOT IS_SCREEN_FADED_IN()
							AND NOT IS_SCREEN_FADING_IN()
								DO_SCREEN_FADE_IN(1000)
							ENDIF
	//						bRollingStart = FALSE
							
							garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_WAIT_FOR_OPEN_DOOR
						ELSE
							bRESET_GARAGE_CAR_LEAVE_CUTSCENE = (TRUE)
						ENDIF
					ELSE
						bRESET_GARAGE_CAR_LEAVE_CUTSCENE = (TRUE)
					ENDIF
					
					
				ELSE
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(vehiclePlayerIsIn)
				
						IF IS_NET_PLAYER_OK(ownerID)
							IF ownerID != PLAYER_ID()
							AND iOwnerProgress <= ENUM_TO_INT(GARAGE_CAR_LEAVE_CUTSCENE_INIT)
								PRINTLN("CDM: Waiting for owner to finish init")
								RETURN FALSE
							ENDIF
						ENDIF
						//  #1638192 //  //  //  //  //  //
						FLOAT fRadius
						fRadius = VDIST(scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos, scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos)
						
						IF (fRadius > 4) fRadius = 4 ENDIF
						IF (fRadius < 2) fRadius = 2 ENDIF
						
						IF IS_POSITION_OCCUPIED(
								scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos,
								fRadius,
								FALSE, TRUE, FALSE, FALSE, FALSE, vehiclePlayerIsIn)
						//IF ownerID = PLAYER_ID()
						OR NET_REALTY_6_SCENE_IS_DOOR_BLOCKED(scene)
							//SCRIPT_ASSERT("area '6' occupied!")
							
							SET_PLAYER_IN_PROPERTY(FALSE,TRUE,FALSE)
							SETUP_SPECIFIC_SPAWN_LOCATION(mpProperties[iPropertyEntered].garage.vCarExitLoc[0],mpProperties[iPropertyEntered].garage.fCarExitHeading[0], 150, FALSE)
							
							IF IS_NEW_LOAD_SCENE_ACTIVE()
								garageCarLeaveCutStage = GARAGE_CAR_SET_IN_UNOCCUPIED_AREA
								
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarLeaveCutStage = GARAGE_CAR_SET_IN_UNOCCUPIED_AREA (fRadius: ", fRadius, ")")
								#ENDIF
							ELSE
								garageCarLeaveCutStage = GARAGE_CAR_WARP_TO_UNOCCUPIED_AREA
								
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarLeaveCutStage = GARAGE_CAR_WARP_TO_UNOCCUPIED_AREA (fRadius: ", fRadius, ")")
								#ENDIF
							ENDIF
							RETURN FALSE
						ELSE
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarLeaveCutStage = unoccupied (fRadius: ", fRadius, ")")
						ENDIF
						//  //  //  //  //  //  //  //  // 
						
												//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
						iGarageCutsceneBitset = 0
					
						iGarageCutTimer = GET_GAME_TIMER()
						IF ownerID = PLAYER_ID()
							SET_VEHICLE_DOORS_LOCKED(vehiclePlayerIsIn,VEHICLELOCK_CANNOT_ENTER)
							SET_ENTITY_CAN_BE_DAMAGED(vehiclePlayerIsIn,FALSE)
							SET_ENTITY_COORDS(vehiclePlayerIsIn, scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos)
							SET_ENTITY_HEADING(vehiclePlayerIsIn, scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].fRot)
							PRINTLN("DO_GARAGE_CAR_LEAVE_THIS_CUTSCENE: setting vehicle position to be: ", scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos)
						ENDIF
						
						scene.bSetCarLockedInvulnerable = TRUE
						
//						camGarageCutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//						SET_CAM_ACTIVE(camGarageCutscene, TRUE)
//						SET_CAM_PARAMS(	camGarageCutscene,
//								<<-796.1625, 320.7180, 87.4069>>,
//								<<7.0379, 0.0327, -179.3550>>, 
//								50.0083)
//						SHAKE_CAM(camGarageCutscene, "HAND_SHAKE", 0.25)
//						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						SceneTool_ExecuteCut(scene.mCuts[NET_REALTY_6_SCENE_CUT_shot], camGarageCutscene)
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_WAIT_FOR_OPEN_DOOR")
						#ENDIF
						
						IF NOT IS_SCREEN_FADED_IN()
						AND NOT IS_SCREEN_FADING_IN()
							DO_SCREEN_FADE_IN(1000)
						ENDIF
//						bRollingStart = FALSE
						
						garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_WAIT_FOR_OPEN_DOOR
					ELSE
						bRESET_GARAGE_CAR_LEAVE_CUTSCENE = (TRUE)
					ENDIF
				ELSE
					bRESET_GARAGE_CAR_LEAVE_CUTSCENE = (TRUE)
				ENDIF
				
				ENDIF
			BREAK
			
			CASE GARAGE_CAR_LEAVE_CUTSCENE_WAIT_FOR_OPEN_DOOR
				IF GET_PROPERTY_SIZE_TYPE(iPropertyEntered) = PROP_SIZE_TYPE_LARGE_APT //GET_PROPERTY_BUILDING(iPropertyEntered) = MP_PROPERTY_BUILDING_1
					
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(vehiclePlayerIsIn)
						AND IS_VEHICLE_DRIVEABLE(data.vehClone)
							IF NOT IS_PED_INJURED(data.pedClones[0])
								TASK_VEHICLE_DRIVE_TO_COORD(data.pedClones[0], data.vehClone,
									scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos, 5.0, DRIVINGSTYLE_STRAIGHTLINE,
									GET_ENTITY_MODEL(data.vehClone), DRIVINGMODE_PLOUGHTHROUGH, 1.0, 100.0)
							ENDIF
							
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH()
							
//							IF GET_PROPERTY_SIZE_TYPE(iPropertyEntered) = PROP_SIZE_TYPE_LARGE_APT
//							OR iPropertyEntered = PROPERTY_LOW_APT_7
//								RENDER_SCRIPT_CAMS(FALSE, TRUE, 6000)
//								
//								#IF IS_DEBUG_BUILD
//								CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === RENDER_SCRIPT_CAMS(FALSE, TRUE, 6000)	//property: ", iPropertyEntered)
//								#ENDIF
//							ELSE
//								RENDER_SCRIPT_CAMS(FALSE, FALSE)
//								
//								#IF IS_DEBUG_BUILD
//								CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === RENDER_SCRIPT_CAMS(FALSE, FALSE)	//property: ", iPropertyEntered)
//								#ENDIF
//							ENDIF
							
									
							iGarageCutTimer = GET_GAME_TIMER()
							IF NOT bMP_PROPERTY_BUILDING_3
								garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_WAIT_FOR_EXIT
								#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_WAIT_FOR_EXIT")
								#ENDIF
							ELSE
								garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_RE_TASK
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_RE_TASK")
								#ENDIF
							ENDIF
						ELSE
							bRESET_GARAGE_CAR_LEAVE_CUTSCENE = (TRUE)
						ENDIF
					ELSE
						bRESET_GARAGE_CAR_LEAVE_CUTSCENE = (TRUE)
					ENDIF
				ELSE
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(vehiclePlayerIsIn)
						
						IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_SHORT
							IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
							AND ownerID = PLAYER_ID()
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								SET_ENTITY_COORDS(vehiclePlayerIsIn, scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos)
								SET_ENTITY_HEADING(vehiclePlayerIsIn, scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].fRot)
							ENDIF
							iGarageCutTimer = GET_GAME_TIMER()
							SET_CUTSCENE_COMPLETE(garageCarLeaveCutStage   ,scene ,data  )
							RETURN FALSE
						ENDIF
						
						
//						IF IS_GARAGE_DOOR_UNLOCKED()
//							IF IS_GARAGE_DOOR_OPEN()
							
								IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
								AND ownerID = PLAYER_ID()
									TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), vehiclePlayerIsIn,
											scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos, 5.0, DRIVINGSTYLE_STRAIGHTLINE,
											GET_ENTITY_MODEL(vehiclePlayerIsIn), DRIVINGMODE_PLOUGHTHROUGH, 1.0, 100.0)
								ENDIF
								
								SET_GAMEPLAY_CAM_RELATIVE_HEADING()
								SET_GAMEPLAY_CAM_RELATIVE_PITCH()
								
								IF GET_PROPERTY_SIZE_TYPE(iPropertyEntered) = PROP_SIZE_TYPE_LARGE_APT
								OR iPropertyEntered = PROPERTY_LOW_APT_7
									IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehiclePlayerIsIn),scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos) >= 5
										iGarageCutTimer = GET_GAME_TIMER()
										SET_CUTSCENE_COMPLETE(garageCarLeaveCutStage   ,scene ,data  )
										PRINTLN("=== CUTSCENE === garageCarLeaveCutStage - SET_CUTSCENE_COMPLETE(garageCarLeaveCutStage  #IF IS_DEBUG_BUILD #IF FEATURE_HEIST_PLANNING ,scene ,data #ENDIF #ENDIF ) - Q")
										RETURN FALSE
									ENDIF
									RENDER_SCRIPT_CAMS(FALSE, TRUE, 6000)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === RENDER_SCRIPT_CAMS(FALSE, TRUE, 6000)	//property: ", iPropertyEntered)
									#ENDIF
								ELSE
									RENDER_SCRIPT_CAMS(FALSE, FALSE)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === RENDER_SCRIPT_CAMS(FALSE, FALSE)	//property: ", iPropertyEntered)
									#ENDIF
								ENDIF
								
								iGarageCutTimer = GET_GAME_TIMER()
								IF NOT bMP_PROPERTY_BUILDING_3
									garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_WAIT_FOR_EXIT
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_WAIT_FOR_EXIT")
									#ENDIF
								ELSE
									garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_RE_TASK
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_RE_TASK")
									#ENDIF
								ENDIF
//							ENDIF
//						ENDIF
					ELSE
						bRESET_GARAGE_CAR_LEAVE_CUTSCENE = (TRUE)
					ENDIF
				ELSE
					bRESET_GARAGE_CAR_LEAVE_CUTSCENE = (TRUE)
				ENDIF
				
				ENDIF
			BREAK
			
			CASE GARAGE_CAR_LEAVE_CUTSCENE_RE_TASK
				IF GET_PROPERTY_SIZE_TYPE(iPropertyEntered) = PROP_SIZE_TYPE_LARGE_APT //GET_PROPERTY_BUILDING(iPropertyEntered) = MP_PROPERTY_BUILDING_1
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(vehiclePlayerIsIn)
						AND IS_VEHICLE_DRIVEABLE(data.vehClone)
							SET_BIT(data.iBS,FAKE_CAR_EXIT_GARAGE_WARP_POSSIBLE)
							IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_LONG
								iGarageCutTimer = GET_GAME_TIMER()
								CLEANUP_MP_CUTSCENE(DEFAULT,FALSE)
								SET_CUTSCENE_COMPLETE(garageCarLeaveCutStage   ,scene ,data  )
								RETURN FALSE
							ENDIF
							IF VDIST(GET_ENTITY_COORDS(data.vehClone), scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos) <= 4
								//CLEAR_PED_TASKS(PLAYER_PED_ID())
								//TASK_GO_TO_COORD_ANY_MEANS(PLAYER_PED_ID(),vMP_PROPERTY_BUILDING_3_driveto,)
								IF NOT IS_PED_INJURED(data.pedClones[0])
									TASK_VEHICLE_DRIVE_TO_COORD(data.pedClones[0], data.vehClone,
											vMP_PROPERTY_BUILDING_3_driveto, 5.0, DRIVINGSTYLE_NORMAL,
											GET_ENTITY_MODEL(data.vehClone), DRIVINGMODE_PLOUGHTHROUGH, 1.0, 100.0)
								ENDIF
								PRINTLN("=== CUTSCENE === garageCarLeaveCutStage = giving building 3 second drive task")
							
							ELSE
								PRINTLN("=== CUTSCENE === garageCarLeaveCutStage = waiting for vehicle to reach first placement for building 3")
								RETURN FALSE
							ENDIF
						
							iGarageCutTimer = GET_GAME_TIMER()
							garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_WAIT_FOR_EXIT
						ELSE
							bRESET_GARAGE_CAR_LEAVE_CUTSCENE = (TRUE)
						ENDIF
					ELSE
						bRESET_GARAGE_CAR_LEAVE_CUTSCENE = (TRUE)
					ENDIF
				ELSE
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(vehiclePlayerIsIn)
						IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_LONG
							IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
							AND ownerID = PLAYER_ID()
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								SET_ENTITY_COORDS(vehiclePlayerIsIn, scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos)
								SET_ENTITY_HEADING(vehiclePlayerIsIn, scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].fRot)
							ENDIF
							iGarageCutTimer = GET_GAME_TIMER()
							SET_CUTSCENE_COMPLETE(garageCarLeaveCutStage   ,scene ,data  )
							RETURN FALSE
						ENDIF
						IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
						AND ownerID = PLAYER_ID()
							IF VDIST(GET_ENTITY_COORDS(vehiclePlayerIsIn), scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos) <= 4
								//CLEAR_PED_TASKS(PLAYER_PED_ID())
								//TASK_GO_TO_COORD_ANY_MEANS(PLAYER_PED_ID(),vMP_PROPERTY_BUILDING_3_driveto,)
								TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), vehiclePlayerIsIn,
											vMP_PROPERTY_BUILDING_3_driveto, 5.0, DRIVINGSTYLE_NORMAL,
											GET_ENTITY_MODEL(vehiclePlayerIsIn), DRIVINGMODE_PLOUGHTHROUGH, 1.0, 100.0)
								PRINTLN("=== CUTSCENE === garageCarLeaveCutStage = giving building 3 second drive task")
							
							ELSE
								PRINTLN("=== CUTSCENE === garageCarLeaveCutStage = waiting for vehicle to reach first placement for building 3")
								RETURN FALSE
							ENDIF
						ENDIF
					
						iGarageCutTimer = GET_GAME_TIMER()
						garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_WAIT_FOR_EXIT
					ELSE
						bRESET_GARAGE_CAR_LEAVE_CUTSCENE = (TRUE)
					ENDIF
				ELSE
					bRESET_GARAGE_CAR_LEAVE_CUTSCENE = (TRUE)
				ENDIF
				
				ENDIF
			BREAK
			
			CASE GARAGE_CAR_LEAVE_CUTSCENE_WAIT_FOR_EXIT
				IF GET_PROPERTY_SIZE_TYPE(iPropertyEntered) = PROP_SIZE_TYPE_LARGE_APT //GET_PROPERTY_BUILDING(iPropertyEntered) = MP_PROPERTY_BUILDING_1
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(vehiclePlayerIsIn)
						AND IS_VEHICLE_DRIVEABLE(data.vehClone)	
							SET_BIT(data.iBS,FAKE_CAR_EXIT_GARAGE_WARP_POSSIBLE)
							IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_LONG
								iGarageCutTimer = GET_GAME_TIMER()
								SET_CUTSCENE_COMPLETE(garageCarLeaveCutStage   ,scene ,data  )
								RETURN FALSE
							ENDIF
							

							
							CONST_FLOAT fMIN_FORWARD_SPEED	5.0
							
							IF (NOT bMP_PROPERTY_BUILDING_3 AND VDIST2(GET_ENTITY_COORDS(data.vehClone), scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos) < (4.0*4.0))
							OR (bMP_PROPERTY_BUILDING_3 AND VDIST(GET_ENTITY_COORDS(data.vehClone), vMP_PROPERTY_BUILDING_3_driveto) <= 4)


								IF GET_ENTITY_SPEED(data.vehClone) < fMIN_FORWARD_SPEED
									SET_VEHICLE_FORWARD_SPEED(data.vehClone,fMIN_FORWARD_SPEED)
									PRINTLN("=== CUTSCENE === garageCarLeaveCutStage = SET_VEHICLE_FORWARD_SPEED -  1")
								ENDIF
								//ENDIF
								IF IS_BIT_SET(data.iBS,FAKE_CAR_EXIT_GARAGE_WARP_DONE)
									IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()

										IF GET_ENTITY_SPEED(data.vehClone) < fMIN_FORWARD_SPEED
											SET_VEHICLE_FORWARD_SPEED(data.vehClone,fMIN_FORWARD_SPEED)
											PRINTLN("=== CUTSCENE === garageCarLeaveCutStage = SET_VEHICLE_FORWARD_SPEED - 2")		
										ENDIF
	
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === SET_CUTSCENE_COMPLETE(garageCarLeaveCutStage  #IF IS_DEBUG_BUILD #IF FEATURE_HEIST_PLANNING ,scene ,data #ENDIF #ENDIF )")
										#ENDIF
										iGarageCutTimer = GET_GAME_TIMER()
										CLEANUP_MP_CUTSCENE(DEFAULT,FALSE)
										SET_CUTSCENE_COMPLETE(garageCarLeaveCutStage   ,scene ,data  )

									ENDIF
								ELSE
									PRINTLN("=== CUTSCENE === garageCarLeaveCutStage = waiting for warp to be done")
								ENDIF
							ELSE
								IF bMP_PROPERTY_BUILDING_3		
									IF NOT IS_PED_INJURED(data.pedClones[0])
										IF NOT (GET_SCRIPT_TASK_STATUS(data.pedClones[0],SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) = PERFORMING_TASK
										OR GET_SCRIPT_TASK_STATUS(data.pedClones[0],SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) = WAITING_TO_START_TASK)
											TASK_VEHICLE_DRIVE_TO_COORD(data.pedClones[0], data.vehClone,
													vMP_PROPERTY_BUILDING_3_driveto, 5.0, DRIVINGSTYLE_NORMAL,
													GET_ENTITY_MODEL(data.vehClone), DRIVINGMODE_AVOIDCARS, 1.0, 100.0)
											PRINTLN("=== CUTSCENE === garageCarLeaveCutStage = RE-giving building 3 second drive task")		
										ENDIF		
									ENDIF
									#IF IS_DEBUG_BUILD
										fDistance = VDIST(GET_ENTITY_COORDS(data.vehClone), vMP_PROPERTY_BUILDING_3_driveto)
										PRINTLN("=== CUTSCENE === garageCarLeaveCutStage = distance between car and drive to point is = ",fDistance)
									#ENDIF
								ENDIF
							ENDIF
						ELSE
							bRESET_GARAGE_CAR_LEAVE_CUTSCENE = (TRUE)
						ENDIF
					ELSE
						bRESET_GARAGE_CAR_LEAVE_CUTSCENE = (TRUE)
					ENDIF
				ELSE
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(vehiclePlayerIsIn)
						
						IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_LONG
							IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
							AND ownerID = PLAYER_ID()
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								SET_ENTITY_COORDS(vehiclePlayerIsIn, scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos)
								SET_ENTITY_HEADING(vehiclePlayerIsIn, scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].fRot)
							ENDIF
							iGarageCutTimer = GET_GAME_TIMER()
							SET_CUTSCENE_COMPLETE(garageCarLeaveCutStage   ,scene ,data  )
							RETURN FALSE
						ENDIF
						

						
						CONST_FLOAT fMIN_FORWARD_SPEED	5.0
						
						IF (NOT bMP_PROPERTY_BUILDING_3 AND VDIST2(GET_ENTITY_COORDS(vehiclePlayerIsIn), scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos) < (4.0*4.0))
						OR (bMP_PROPERTY_BUILDING_3 AND VDIST(GET_ENTITY_COORDS(vehiclePlayerIsIn), vMP_PROPERTY_BUILDING_3_driveto) <= 4)

							IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
							AND ownerID = PLAYER_ID()
								IF GET_ENTITY_SPEED(vehiclePlayerIsIn) < fMIN_FORWARD_SPEED
									SET_VEHICLE_FORWARD_SPEED(vehiclePlayerIsIn,fMIN_FORWARD_SPEED)
									PRINTLN("=== CUTSCENE === garageCarLeaveCutStage = SET_VEHICLE_FORWARD_SPEED -  1")
								ENDIF
							ENDIF
							//ENDIF

								IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
									//IF bMP_PROPERTY_BUILDING_3
										
									//	SCRIPT_ASSERT("ignore control for building 3")
										
									//ELSE
									//IF NOT (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) = PERFORMING_TASK
									//OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) = WAITING_TO_START_TASK)
											IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
											AND ownerID = PLAYER_ID()
												IF GET_ENTITY_SPEED(vehiclePlayerIsIn) < fMIN_FORWARD_SPEED
													SET_VEHICLE_FORWARD_SPEED(vehiclePlayerIsIn,fMIN_FORWARD_SPEED)
													PRINTLN("=== CUTSCENE === garageCarLeaveCutStage = SET_VEHICLE_FORWARD_SPEED - 2")		
												ENDIF
											ENDIF
										//ENDIF
										
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === SET_CUTSCENE_COMPLETE(garageCarLeaveCutStage  #IF IS_DEBUG_BUILD #IF FEATURE_HEIST_PLANNING ,scene ,data #ENDIF #ENDIF )")
										#ENDIF
										iGarageCutTimer = GET_GAME_TIMER()
										SET_CUTSCENE_COMPLETE(garageCarLeaveCutStage   ,scene ,data  )
									//ENDIF
								ENDIF
//							ENDIF
						ELSE
							IF bMP_PROPERTY_BUILDING_3
								IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
								AND ownerID = PLAYER_ID()							
									IF NOT (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) = PERFORMING_TASK
									OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) = WAITING_TO_START_TASK)
										TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), vehiclePlayerIsIn,
												vMP_PROPERTY_BUILDING_3_driveto, 5.0, DRIVINGSTYLE_NORMAL,
												GET_ENTITY_MODEL(vehiclePlayerIsIn), DRIVINGMODE_AVOIDCARS, 1.0, 100.0)
										PRINTLN("=== CUTSCENE === garageCarLeaveCutStage = RE-giving building 3 second drive task")		
									ENDIF		
								ENDIF
								#IF IS_DEBUG_BUILD
									fDistance = VDIST(GET_ENTITY_COORDS(vehiclePlayerIsIn), vMP_PROPERTY_BUILDING_3_driveto)
									PRINTLN("=== CUTSCENE === garageCarLeaveCutStage = distance between car and drive to point is = ",fDistance)
								#ENDIF
							ENDIF
						ENDIF
					ELSE
						bRESET_GARAGE_CAR_LEAVE_CUTSCENE = (TRUE)
					ENDIF
				ELSE
					bRESET_GARAGE_CAR_LEAVE_CUTSCENE = (TRUE)
				ENDIF
				
				ENDIF
			BREAK
			
			CASE GARAGE_CAR_WARP_TO_UNOCCUPIED_AREA
			CASE GARAGE_CAR_SET_IN_UNOCCUPIED_AREA
	//			IF IS_SCREEN_FADED_OUT()
					BOOL bWarpWhenBlocked
					bWarpWhenBlocked = FALSE
					
					IF garageCarLeaveCutStage = GARAGE_CAR_SET_IN_UNOCCUPIED_AREA
						PRINTLN("=== CUTSCENE === garageCarLeaveCutStage - IN HERE-")
						bWarpWhenBlocked = TRUE
						IF (NOT IS_NEW_LOAD_SCENE_ACTIVE() OR IS_NEW_LOAD_SCENE_LOADED())
							PRINTLN("=== CUTSCENE === garageCarLeaveCutStage - IF (NOT IS_NEW_LOAD_SCENE_ACTIVE() OR NOT IS_NEW_LOAD_SCENE_LOADED()) -")
						ELSE
							PRINTLN("=== CUTSCENE === garageCarLeaveCutStage - waiting for new load scene to be finished before setting player coords -")
							RETURN FALSE
						ENDIF
					ENDIF
					IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_AT_SPECIFIC_COORDS_IF_POSSIBLE, FALSE, FALSE, FALSE, bWarpWhenBlocked, FALSE, FALSE, TRUE)
					
						IF NOT IS_SCREEN_FADED_IN()
						AND NOT IS_SCREEN_FADING_IN()
							DO_SCREEN_FADE_IN(500)
						ENDIF
						
						
						IF NOT IS_BIT_SET(iGarageCutsceneBitset, GARAGE_CUT_BS_CONTROL_RETURNED)
							
													//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//							IF bDisabledRadar
//								DISPLAY_RADAR(TRUE)
//								bDisabledRadar = FALSE
//							ENDIF
							SET_BIT(iGarageCutsceneBitset, GARAGE_CUT_BS_CONTROL_RETURNED)
						ENDIF
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
								vehiclePlayerIsIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								CLEAR_VEHICLE_ROUTE_HISTORY(vehiclePlayerIsIn)
							ENDIF
						ENDIF
						iGarageCutTimer = GET_GAME_TIMER()
						SET_CUTSCENE_COMPLETE(garageCarLeaveCutStage   ,scene ,data  )
						PRINTLN("=== CUTSCENE === garageCarLeaveCutStage - SET_CUTSCENE_COMPLETE(garageCarLeaveCutStage  #IF IS_DEBUG_BUILD #IF FEATURE_HEIST_PLANNING ,scene ,data #ENDIF #ENDIF ) - C")
					ELSE
						PRINTLN("=== CUTSCENE === garageCarLeaveCutStage - Trying to warp to spawn location-")
					ENDIF
	//			ENDIF
			BREAK
				
			CASE GARAGE_CAR_LEAVE_CUTSCENE_COMPLETE
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) = PERFORMING_TASK
						OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) = WAITING_TO_START_TASK
							IF GET_GAME_TIMER() - iGarageCutTimer < 7000
								PRINTLN("=== CUTSCENE === garageCarLeaveCutStage - waiting for drive to coord task to be cleared")
								RETURN FALSE
							ENDIF
						ELSE
							CLEAR_VEHICLE_ROUTE_HISTORY(vehiclePlayerIsIn)
						ENDIF
					ENDIF
				ENDIF
				RETURN TRUE
			BREAK
		
		ENDSWITCH
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === DO_GARAGE_CAR_LEAVE_CUTSCENE over player is not OK.")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF bRESET_GARAGE_CAR_LEAVE_CUTSCENE
	
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === RESET_GARAGE_CAR_LEAVE_CUTSCENE()")
		#ENDIF

		
		//BOOL bClearTasks = TRUE
		BOOL bTurnOffRenderScriptCams = TRUE
		
		IF bTurnOffRenderScriptCams
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		ENDIf
		
		IF DOES_CAM_EXIST(camGarageCutscene)
			DESTROY_CAM(camGarageCutscene)
		ENDIF
		
		iGarageCutsceneBitset = 0
		
								//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos
		NET_SET_PLAYER_CONTROL(	PLAYER_ID(),TRUE)
		
		IF scene.bSetCarLockedInvulnerable
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_DRIVEABLE(vehiclePlayerIsIn)	
					IF NETWORK_HAS_CONTROL_OF_ENTITY(vehiclePlayerIsIn)
						SET_VEHICLE_DOORS_LOCKED(vehiclePlayerIsIn,VEHICLELOCK_UNLOCKED)
						SET_ENTITY_CAN_BE_DAMAGED(vehiclePlayerIsIn,TRUE)
					ENDIF
					scene.bSetCarLockedInvulnerable = FALSE
				ENDIF
			ENDIF
			CLEANUP_MP_CUTSCENE(DEFAULT,FALSE)
		ENDIF
		IF scene.bSetCarLockedInvulnerable
			CLEANUP_MP_CUTSCENE(DEFAULT,FALSE)
			IF DOES_ENTITY_EXIST(data.vehClone)
				DELETE_VEHICLE(data.vehClone)
			ENDIF
			REPEAT data.iPedsInCar i
				IF DOES_ENTITY_EXIST(data.pedClones[i])
					DELETE_PED(data.pedClones[i])
				ENDIF
			ENDREPEAT
		ENDIF
	//	#IF IS_DEBUG_BUILD
	//		bDebugLaunchGarageCarLeaveCutscene = FALSE
	//		bDebugSeenGarageCarLeaveCutscene = FALSE
	//	#ENDIF
		
		garageCarLeaveCutStage = GARAGE_CAR_LEAVE_CUTSCENE_INIT
		
		RETURN TRUE //1704824
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF ACTIVATE_PROPERTY_CS_DEBUG
USING "NET_REALTY_6_SCENE_debug.sch"
#ENDIF
