

USING "Net_hud_activating.sch"
USING "net_above_head_display.sch"
USING "net_scoring_Common.sch"


USING "Cellphone_public.sch"
USING "shared_hud_displays.sch"
USING "menu_public.sch"
USING "Net_Entity_Icons.sch"
USING "screen_gang_on_mission.sch"
USING "freemode_header.sch"




/// PURPOSE:
///    Just a normal timer. returns true when time has expired
/// PARAMS:
///    StartTimer - Fixed start time
///    TimeAmount - how long you want the timer to run for.
/// RETURNS:
/////    true when the time has elapsed. False if not.
//FUNC BOOL DO_LOCAL_TIMER(INT StartTimer, INT TimeAmount)
//
//	INT CurrentTimer = GET_THE_NETWORK_TIMER()
//	IF CurrentTimer - StartTimer > TimeAmount
//		RETURN TRUE
//	ENDIF
//	RETURN FALSE
//	
//ENDFUNC

/// PURPOSE:
///    Function to let the mission creator and MPHUD communicate with each other
/// PARAMS:
///    ExitOption - Passes in the option the player made while in the mission creator so the hud can decide what to do.
PROC SET_MISSION_CREATOR_EXIT_STATE(MISSIONCREATORHUD ExitOption)
	IF NOT IS_BIT_SET(MPGlobalsHud.iMissionCreatorHud, ENUM_TO_INT(ExitOption))
		SET_BIT(MPGlobalsHud.iMissionCreatorHud, ENUM_TO_INT(ExitOption))
	ENDIF
ENDPROC



// KGM 19/6/12 - Gather and return a list of MP_MISSIONS based on the joblist
// NOTE: This can get called every frame - I have some stuff in here that only re-generates the list at appropriate times
PROC GET_JOBLIST_MISSIONS(MP_MISSION &paramMissions[MAX_MP_JOBLIST_ENTRIES])

	// Set this TRUE every frame it is on-screen, it'll be cleared by the joblist processing functions
	g_sJoblistUpdate.missionsOnScreen = TRUE
	
	// Clear the return array
	INT jobLoop = 0
	REPEAT MAX_MP_JOBLIST_ENTRIES jobLoop
		paramMissions[jobLoop] = eNULL_MISSION
	ENDREPEAT
	
	INT numMissions = 0
	
	REPEAT MAX_MP_JOBLIST_ENTRIES jobLoop
		IF NOT (g_sJobListMP[jobLoop].jlState = NO_MP_JOBLIST_ENTRY)
			paramMissions[numMissions] = INT_TO_ENUM(MP_MISSION, g_sJobListMP[jobLoop].jlMissionAsInt)
			numMissions++
		ENDIF
	ENDREPEAT
	
ENDPROC



FUNC RECT GET_TODOBOX_DESCRIPTION_BOX_SIZE(RECT& aRect, INT NumberOfLines)

	RECT result = aRect 

	SWITCH NumberOfLines
	
		CASE 0 
			result.h = 0
			result.y = 0
		BREAK
		
		CASE 1
			result.y = 0.308 
			result.h = 0.038
		BREAK
		
		CASE 2
			result.y = 0.319 
			result.h = 0.061 
		BREAK
		
		CASE 3
			result.y = 0.333
			result.h = 0.089
		BREAK
		CASE 4
			result.y = 0.346
			result.h = 0.113
		BREAK
		CASE 5
			result.y = 0.359 
			result.h = 0.139
		BREAK
		CASE 6
			result.y = 0.371
			result.h = 0.164
		BREAK
		CASE 7
			result.y = 0.384
			result.h = 0.189
		BREAK
		CASE 8
			result.y = 0.396
			result.h = 0.213
		BREAK
		CASE 9
			result.y = 0.408
			result.h = 0.237
		BREAK
		CASE 10
			result.y = 0.420
			result.h = 0.262
		BREAK
	
	ENDSWITCH


	RETURN result

ENDFUNC

ENUM TODORECTS
	TODO_TITLEBOX = 0,
	TODO_DIVTITLEBODY_LINE,
	TODO_BODYBOX
ENDENUM

ENUM TODOTEXT
	TODO_TITLETEXT = 0,
	TODO_BODYTEXT,
	TODO_DIST_VALUE_TEXT
ENDENUM

//BOOL SetupToDO
//INT Widget_NumberMissions
//FLOAT Widget_WordWrap
//FLOAT Widget_TodoDistance 

//RECT TODO_Rects[3]
//TEXT_PLACEMENT TODO_TEXT[10]
//TEXT_STYLE aTodoTextStyle

//FLOAT Widget_Text_YStep 


PROC INIT_TODO_LIST(RECT& Rects[], TEXT_PLACEMENT& Texts[] )
	Rects[TODO_TITLEBOX].x = 0.848
	Rects[TODO_TITLEBOX].y = 0.270
	Rects[TODO_TITLEBOX].w = GENERIC_SLOT_WIDTH
	Rects[TODO_TITLEBOX].h = 0.038
	Rects[TODO_TITLEBOX].r = 0
	Rects[TODO_TITLEBOX].g = 0
	Rects[TODO_TITLEBOX].b = 0
	Rects[TODO_TITLEBOX].a = 204
	

	//Divider line between title box and mission box
	Rects[TODO_DIVTITLEBODY_LINE].x = 0.848
	Rects[TODO_DIVTITLEBODY_LINE].y = 0.289
	Rects[TODO_DIVTITLEBODY_LINE].w = GENERIC_SLOT_WIDTH
	Rects[TODO_DIVTITLEBODY_LINE].h = 0.002
	Rects[TODO_DIVTITLEBODY_LINE].r = 160
	Rects[TODO_DIVTITLEBODY_LINE].g = 160
	Rects[TODO_DIVTITLEBODY_LINE].b = 160
	Rects[TODO_DIVTITLEBODY_LINE].a = 204
	
	Rects[TODO_BODYBOX].x = 0.848
	Rects[TODO_BODYBOX].y = 0.308
	Rects[TODO_BODYBOX].w = GENERIC_SLOT_WIDTH
	Rects[TODO_BODYBOX].h = 0.038
	Rects[TODO_BODYBOX].r = 0
	Rects[TODO_BODYBOX].g = 0
	Rects[TODO_BODYBOX].b = 0
	Rects[TODO_BODYBOX].a = 204
	
	Texts[TODO_TITLETEXT].x = 0.755
	Texts[TODO_TITLETEXT].y = 0.260
	
	Texts[TODO_BODYTEXT].x = 0.755
	Texts[TODO_BODYTEXT].y = 0.295
	
	Texts[TODO_DIST_VALUE_TEXT].x = 0.5
	Texts[TODO_DIST_VALUE_TEXT].y = 0.295

ENDPROC


STRUCT MISSION_TODO_HUD
	MP_MISSION aMission
	INT StrandIndex
	STRING missionName
	STRING sLiteralString
	FLOAT Distance
	
ENDSTRUCT

// KGM 19/12/12: Function moved to net_mission_info.sch so it is accessible to other scripts
//FUNC STRING GET_FM_MISSION_NAME_TEXT_LABEL(INT iMission)

FUNC STRING GET_ACT_STRING_FROM_TL(STRING textLabel)
	RETURN textLabel
ENDFUNC

FUNC STRING GET_JOB_LIST_MP_MISSION_NAME_TEXT_LABEL(MP_MISSION mission)
	RETURN GET_FM_MISSION_NAME_TEXT_LABEL(ENUM_TO_INT(mission))
ENDFUNC

FUNC STRING GET_ACTIVITY_NAME(INT iActivityIndex)
	SWITCH iActivityIndex
		CASE MP_ACT_ID_HEIST					RETURN "MP_ACT_HEIST"		
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_ACTIVITY_NAME_FROM_FMMC_TYPE(INT iFMMCType)
	IF iFMMCType = iFMMCType
	ENDIF
//	SWITCH iFMMCType
//		CASE FMMC_TYPE_MISSION        		RETURN "MP_ACT_MIS"  
//		CASE FMMC_TYPE_DEATHMATCH      	    RETURN "MP_ACT_DM"
//		CASE FMMC_TYPE_RACE            	   	RETURN "MP_ACT_RACE"
//		CASE FMMC_TYPE_SURVIVAL          	    RETURN "MP_ACT_SUR"
//		CASE FMMC_TYPE_MISSION_CTF      	   	RETURN "MP_ACT_MISSION"
//		CASE FMMC_TYPE_IMPROMPTU_DM	        RETURN "MP_ACT_IMPRO"
//		CASE FMMC_TYPE_GANGHIDEOUT        	RETURN "MP_ACT_HIDE"
//		CASE FMMC_TYPE_TRIATHLON          	RETURN "MP_ACT_TRI"
//		CASE FMMC_TYPE_BASE_JUMP          	RETURN "MP_ACT_BASEJ"
//		CASE FMMC_TYPE_MG					RETURN "MP_ACT_MINI"
//		CASE FMMC_TYPE_MG_GOLF				RETURN "MP_ACT_GOLF"
//		CASE FMMC_TYPE_MG_TENNIS			RETURN "MP_ACT_TEN"
//		CASE FMMC_TYPE_MG_SHOOTING_RANGE	RETURN "MP_ACT_SR"
//		CASE FMMC_TYPE_MG_DARTS				RETURN "MP_ACT_DART"
//		CASE FMMC_TYPE_MG_ARM_WRESTLING		RETURN "MP_ACT_AW"	
//		CASE FMMC_TYPE_IMPORT_EXPORT		RETURN "MP_ACT_IMPEX"
//		CASE FMMC_TYPE_SECURITY_VAN			RETURN "MP_ACT_SV"
//		CASE FMMC_TYPE_STUNT_JUMPS			RETURN "MP_ACT_SV"	
//		CASE FMMC_TYPE_CINEMA				RETURN "MP_ACT_CIN"
//		CASE FMMC_TYPE_STRIP_CLUB			RETURN "MP_ACT_SC"
//		CASE FMMC_TYPE_BOUNTIES				RETURN "MP_ACT_BOUNTY"	
//		CASE FMMC_TYPE_HOLD_UPS				RETURN "MP_ACT_HU"
//		CASE FMMC_TYPE_CRATE_DROP			RETURN "MP_ACT_CD"
//		CASE FMMC_TYPE_RACE_TO_POINT		RETURN "MP_ACT_RACETP"	
//		CASE FMMC_TYPE_MG_RANGE_GRID		RETURN "MP_ACT_SRG"	
//		CASE FMMC_TYPE_MG_RANGE_RANDOM		RETURN "MP_ACT_SRR"	
//		CASE FMMC_TYPE_MG_RANGE_COVERED		RETURN "MP_ACT_SRC"	
//	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC BOOL ADD_NAMED_ACTIVITY_TO_DISPLAY_LIST(VECTOR vCoords,STRING sActivityName, STRING sLiteralName)
	INT i
	IF NOT IS_STRING_NULL_OR_EMPTY(sActivityName)
		REPEAT MP_ACT_MAX i
			IF NOT ARE_STRINGS_EQUAL(MPGlobalsAmbience.availableAct[i].tlTextLabel, sActivityName)
				IF IS_STRING_NULL_OR_EMPTY(MPGlobalsAmbience.availableAct[i].tlTextLabel) 
					MPGlobalsAmbience.availableAct[i].bValid = TRUE
					MPGlobalsAmbience.availableAct[i].vCoords = vCoords
					MPGlobalsAmbience.availableAct[i].tlTextLabel = sActivityName
					MPGlobalsAmbience.availableAct[i].tlLiteralMissionName = sLiteralName
					RETURN TRUE
				ENDIF
			ELSE
				IF NOT ARE_VECTORS_EQUAL(MPGlobalsAmbience.availableAct[i].vCoords,vCoords)
					MPGlobalsAmbience.availableAct[i].bValid = TRUE
					MPGlobalsAmbience.availableAct[i].vCoords = vCoords
					MPGlobalsAmbience.availableAct[i].tlTextLabel = sActivityName
					MPGlobalsAmbience.availableAct[i].tlLiteralMissionName= sLiteralName
					RETURN TRUE
				ELSE
					//already in job list
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN FALSE
ENDFUNC

//FUNC BOOL ADD_NAMED_ACTIVITY_TO_DISPLAY_LIST_SLOW(VECTOR vCoords,STRING sActivityName, STRING sLiteralName)
//	IF NOT IS_STRING_NULL_OR_EMPTY(sActivityName)
//		IF NOT ARE_STRINGS_EQUAL(MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].tlTextLabel, sActivityName)
//			IF IS_STRING_NULL_OR_EMPTY(MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].tlTextLabel) 
//				MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].bValid = TRUE
//				MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].vCoords = vCoords
//				MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].tlTextLabel = sActivityName
//				MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].tlLiteralMissionName = sLiteralName
//				RETURN TRUE
//			ENDIF
//		ELSE
//			IF NOT ARE_VECTORS_EQUAL(MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].vCoords,vCoords)
//				MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].bValid = TRUE
//				MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].vCoords = vCoords
//				MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].tlTextLabel = sActivityName
//				MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].tlTextLabel = sLiteralName
//				RETURN TRUE
//			ELSE
//				//already in job list
//				RETURN TRUE
//			ENDIF
//		ENDIF
//	ENDIF
//	RETURN FALSE
//ENDFUNC

//PROC REMOVE_ACTIVITY_FROM_DISPLAY_LIST_SLOW(VECTOR vCoords,STRING sActivityName, BOOL bAllWithThisName = FALSE)
//	IF NOT IS_STRING_NULL_OR_EMPTY(sActivityName)
//		IF ARE_STRINGS_EQUAL(MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].tlTextLabel,sActivityName)
//			IF bAllWithThisName
//			OR ARE_VECTORS_EQUAL(MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].vCoords,vCoords)
//				MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].bValid = FALSE
//				MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].vCoords = <<0,0,0>>
//				MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].tlTextLabel = ""
//				MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].tlLiteralMissionName = ""
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

PROC UPDATE_ACTIVITY_ON_DISPLAY_LIST_EACH_FRAME(VECTOR vCoords,STRING sActivityName, STRING sLiteralName)
	IF ARE_STRINGS_EQUAL(MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].tlTextLabel, sActivityName)
		IF ARE_STRINGS_EQUAL(MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].tlLiteralMissionName,sLiteralName)
			MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].bValid = TRUE
			MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].vCoords = vCoords
			MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].tlTextLabel = sActivityName
			MPGlobalsAmbience.availableAct[MPGlobalsAmbience.iAvailActSlowLoop].tlLiteralMissionName = sLiteralName
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_ACTIVITY_FROM_DISPLAY_LIST(VECTOR vCoords,STRING sActivityName, BOOL bAllWithThisName = FALSE)
	INT i
	IF NOT IS_STRING_NULL_OR_EMPTY(sActivityName)
		REPEAT MP_ACT_MAX i
			IF ARE_STRINGS_EQUAL(MPGlobalsAmbience.availableAct[i].tlTextLabel,sActivityName)
				IF bAllWithThisName
				OR ARE_VECTORS_EQUAL(MPGlobalsAmbience.availableAct[i].vCoords,vCoords)
					MPGlobalsAmbience.availableAct[i].bValid = FALSE
					MPGlobalsAmbience.availableAct[i].vCoords = <<0,0,0>>
					MPGlobalsAmbience.availableAct[i].tlTextLabel = ""
					MPGlobalsAmbience.availableAct[i].tlLiteralMissionName = ""
					//EXIT
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC FLOAT GET_DISTANCE_TO_ACTIVITY(INT iIndex)
	FLOAT Distance
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF NOT ARE_VECTORS_EQUAL(MPGlobalsAmbience.availableAct[iIndex].vCoords,<<0,0,0>>)
			Distance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),MPGlobalsAmbience.availableAct[iIndex].vCoords)
		ELSE
			Distance = -99
		ENDIF
	ENDIF
	RETURN Distance
ENDFUNC


FUNC BOOL CAN_DISPLAY_TODO_LIST()

	//1166695 disabled list.
	RETURN FALSE
	#IF IS_DEBUG_BUILD
		BOOL bPrint
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(TodoBoxDebugTimer, 6000)
			bPrint = TRUE
		ENDIF
		
	#ENDIF
	
	IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		#IF IS_DEBUG_BUILD	
			IF bPrint
				NET_NL()NET_PRINT("CAN_DISPLAY_TODO_LIST = FALSE - IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_SCRIPT_HUD_DISABLED(HUDPART_TODOBOX)
		#IF IS_DEBUG_BUILD	
			IF bPrint
				NET_NL()NET_PRINT("CAN_DISPLAY_TODO_LIST = FALSE - IS_SCRIPT_HUD_DISABLED(HUDPART_TODOBOX)")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

PROC DISPLAY_TODO_LIST()
	
	IF CAN_DISPLAY_TODO_LIST()
	

		FLOAT Main_YStep = 0.025	
		INT i, j
		INT NumberOfAvailableMissions = 0
	//	FLOAT DistanceAmount
		
	//	IF SetupToDO = FALSE
	//		
	//		INIT_TODO_LIST()
	//		
	//		START_WIDGET_GROUP("TODO BOX")
	//			CREATE_A_RECT_PLACEMENT_WIDGET(TODO_Rects[TODO_TITLEBOX], "TODO_Rects[TODO_TITLEBOX]")
	//			CREATE_A_RECT_PLACEMENT_WIDGET(TODO_Rects[TODO_DIVTITLEBODY_LINE], "TODO_Rects[TODO_DIVTITLEBODY_LINE]")
	//			CREATE_A_RECT_PLACEMENT_WIDGET(TODO_Rects[TODO_BODYBOX], "TODO_Rects[TODO_BODYBOX]")
	//			ADD_WIDGET_INT_SLIDER("Widget_NumberMissions", Widget_NumberMissions, -10, 10, 1)
	//			ADD_WIDGET_FLOAT_SLIDER("Widget_Text_YStep", Widget_Text_YStep, 0, 1, 0.001)
	//			ADD_WIDGET_FLOAT_SLIDER("Widget_WordWrap", Widget_WordWrap, -1, 1, 0.001)
	//			ADD_WIDGET_FLOAT_SLIDER("Widget_TodoDistance", Widget_TodoDistance, 0, 10, 0.1)
	//			CREATE_A_TEXT_PLACEMENT_WIDGET(TODO_TEXT[TODO_TITLETEXT], "TODO_TEXT[TODO_TITLETEXT]")
	//			CREATE_A_TEXT_PLACEMENT_WIDGET(TODO_TEXT[TODO_BODYTEXT], "TODO_TEXT[TODO_BODYTEXT]")
	//			CREATE_A_TEXT_PLACEMENT_WIDGET(TODO_TEXT[TODO_DIST_VALUE_TEXT], "TODO_TEXT[TODO_DIST_VALUE_TEXT]")
	//			
	//		STOP_WIDGET_GROUP()
	//		
	//		SetupToDO = TRUE
	//	ENDIF


	//	IF IS_PLAYER_ON_ANY_FM_MISSION(playerID)
	//		INT iPlayerGBD = NATIVE_TO_INT(playerID)
	//		IF GlobalplayerBD_FM[iPlayerGBD].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
	//		OR GlobalplayerBD_FM[iPlayerGBD].iCurrentMissionType = FMMC_TYPE_MISSION
	//
	//		ENDIF
	//	ENDIF
	//	
	//	IF NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerGBD].currentMissionData.iBitSet, ciMissionIsNotJoinable)
	//	
	//	ENDIF
	//	
	//	GlobalplayerBD_FM[iPlayerGBD].currentMissionData.iFMCreatorID
	//	GlobalplayerBD_FM[iPlayerGBD].currentMissionData.missionVariation
		
		
		MPGlobalsAmbience.iAvailActSlowLoop++
		IF MPGlobalsAmbience.iAvailActSlowLoop >= MP_ACT_MAX
			MPGlobalsAmbience.iAvailActSlowLoop = 0
		ENDIF
		RECT TODO_Rects[3]
		TEXT_PLACEMENT TODO_TEXT[10]
		TEXT_STYLE aTodoTextStyle
		
		MP_MISSION joblistArray[MAX_MP_JOBLIST_ENTRIES]
		GET_JOBLIST_MISSIONS(joblistArray)


		INIT_TODO_LIST(TODO_Rects,TODO_TEXT)
		
		INT iTeam = GET_PLAYER_TEAM(PLAYER_ID())
		FLOAT iDistance
		
		SET_RECT_TO_TEAM_COLOUR(TODO_Rects[TODO_TITLEBOX], iTeam)
		
		SET_STANDARD_SMALL_HUD_TEXT(aTodoTextStyle)
		aTodoTextStyle.WrapEndX = 0.94
	//	aTodoTextStyle.WrapEndX += Widget_WordWrap
	//	NumberOfAvailableMissions += Widget_NumberMissions

		SET_TEXT_STYLE(aTodoTextStyle)
		
		
		//increased this by MP_ACT_MAX for all activities. not all will be active at one time so could probably reduce this if needed.
		CONST_INT MAX_MISSIONS_TODO 36 //(ENUM_TO_INT(CNC_FLOW_MAX_STRANDS)+MAX_MP_JOBLIST_ENTRIES)
		
		CONST_INT MAX_DISPLAYED_ENTRIES 10

		
		MISSION_TODO_HUD MissionDetails[MAX_MISSIONS_TODO]
		MISSION_TODO_HUD SwappingDetails
		
		FOR i = 0 TO (MAX_MISSIONS_TODO-1)
			MissionDetails[i].Distance = -999999
		ENDFOR
		
		VECTOR vPlayerLoc
		
		

		IF IS_NET_PLAYER_OK(PLAYER_ID())
			vPlayerLoc = GET_PLAYER_COORDS(PLAYER_ID())
			FOR I = 0 TO g_numMPJobListInvitations -1
				IF NumberOfAvailableMissions<  MAX_MISSIONS_TODO
					IF g_sJLInvitesMP[I].jliActive
					 	MissionDetails[NumberOfAvailableMissions].missionName = GET_ACTIVITY_NAME_FROM_FMMC_TYPE(g_sJLInvitesMP[I].jliType)
						MissionDetails[NumberOfAvailableMissions].sLiteralString = GET_ACT_STRING_FROM_TL(g_sJLInvitesMP[I].jliMissionName)
						MissionDetails[NumberOfAvailableMissions].Distance = GET_DISTANCE_BETWEEN_COORDS(vPlayerLoc,g_sJLInvitesMP[I].jliCoords)
						NumberOfAvailableMissions++
					ENDIF
				ENDIF
			ENDFOR	
		ENDIF
		FOR I = 0 TO MAX_MP_JOBLIST_ENTRIES-1
			 IF NOT (joblistArray[I] = eNULL_MISSION)
			 	IF NumberOfAvailableMissions<  MAX_MISSIONS_TODO
				 	MissionDetails[NumberOfAvailableMissions].aMission = joblistArray[I]
					MissionDetails[NumberOfAvailableMissions].StrandIndex = I
					MissionDetails[NumberOfAvailableMissions].missionName = GET_JOB_LIST_MP_MISSION_NAME_TEXT_LABEL(joblistArray[I])
					MissionDetails[NumberOfAvailableMissions].Distance = -99
					NumberOfAvailableMissions++
				ENDIF
			 ENDIF
		ENDFOR
		//ELSE
			
		//ENDIF
		
		REPEAT MP_ACT_MAX i 
			IF NumberOfAvailableMissions<  MAX_MISSIONS_TODO
				IF MPGlobalsAmbience.availableAct[i].bValid = TRUE
				
					iDistance= GET_DISTANCE_TO_ACTIVITY(i)
					
					IF iDistance = -99
					OR iDistance > 0
						MissionDetails[NumberOfAvailableMissions].missionName = GET_ACT_STRING_FROM_TL(MPGlobalsAmbience.availableAct[i].tlTextLabel)
						MissionDetails[NumberOfAvailableMissions].sLiteralString = GET_ACT_STRING_FROM_TL(MPGlobalsAmbience.availableAct[i].tlLiteralMissionName)
						MissionDetails[NumberOfAvailableMissions].Distance = iDistance
						NumberOfAvailableMissions++
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		FOR I = 0 TO NumberOfAvailableMissions-1
			FOR J = I+1 TO NumberOfAvailableMissions
				IF MissionDetails[I].Distance > MissionDetails[J].Distance
					SwappingDetails = MissionDetails[I]
					MissionDetails[I] = MissionDetails[J]
					MissionDetails[J] = SwappingDetails
				ENDIF
			ENDFOR
		ENDFOR
		
		//All items are ordered at this stage so just display top entries up to max number possible
		IF NumberOfAvailableMissions > MAX_DISPLAYED_ENTRIES
			NumberOfAvailableMissions = MAX_DISPLAYED_ENTRIES
		ENDIF
		
		IF NumberOfAvailableMissions > 0
		
			FLOAT OldValue = TODO_TEXT[TODO_BODYTEXT].y
			
			FOR I = 1 TO NumberOfAvailableMissions
				IF MissionDetails[i].Distance != -999999
					IF NOT IS_STRING_NULL_OR_EMPTY(MissionDetails[i].sLiteralString)
						DRAW_TEXT_WITH_STRING(TODO_TEXT[TODO_BODYTEXT], aTodoTextStyle,MissionDetails[I].missionName,MissionDetails[i].sLiteralString)
					ELSE
						DRAW_TEXT(TODO_TEXT[TODO_BODYTEXT], aTodoTextStyle, MissionDetails[I].missionName)
					ENDIF
					
					IF MissionDetails[i].Distance <> -99
						IF MissionDetails[i].Distance < 0.0
							MissionDetails[i].Distance = 0.0
						ENDIF
						DRAW_TEXT_WITH_FLOAT(TODO_TEXT[TODO_DIST_VALUE_TEXT], aTodoTextStyle,"TIMER_METER" ,MissionDetails[i].Distance,1,FONT_RIGHT)
					ELSE
						DRAW_TEXT_WITH_ALIGNMENT(TODO_TEXT[TODO_DIST_VALUE_TEXT], aTodoTextStyle, "HUD_PHONE", FALSE, TRUE)
					ENDIF
					TODO_TEXT[TODO_BODYTEXT].y += Main_YStep
					TODO_TEXT[TODO_DIST_VALUE_TEXT].y += Main_YStep

		//			TODO_TEXT[TODO_BODYTEXT].y += Widget_Text_YStep
		//			TODO_TEXT[TODO_DIST_VALUE_TEXT].y += Widget_Text_YStep
				ENDIF
				
			ENDFOR
			
			TODO_TEXT[TODO_BODYTEXT].y = OldValue
			
			DRAW_RECTANGLE(TODO_Rects[TODO_TITLEBOX])
			DRAW_RECTANGLE(TODO_Rects[TODO_DIVTITLEBODY_LINE])
			
			TODO_Rects[TODO_BODYBOX] = GET_TODOBOX_DESCRIPTION_BOX_SIZE(TODO_Rects[TODO_BODYBOX], NumberOfAvailableMissions)
			DRAW_RECTANGLE(TODO_Rects[TODO_BODYBOX])
			
			SET_TEXT_BLACK(aTodoTextStyle)
			DRAW_TEXT(TODO_TEXT[TODO_TITLETEXT], aTodoTextStyle, "HUD_TODOLIST")
			SET_TEXT_WHITE(aTodoTextStyle)
		ENDIF
		
	ENDIF

	

ENDPROC




FUNC SPRITE_PLACEMENT GET_RANK_SPRITE_POSITION_AND_SCALE(OVERHEADSTRUCT& OverHeadDetails , PED_INDEX aPlayerPed, SPRITE_PLACEMENT aSprite)
	
	SPRITE_PLACEMENT TempSprite = aSprite

	FLOAT Scaler = GET_SCALER_FROM_DISTANCE(OverHeadDetails, aPlayerPed)
	
	IF Scaler > 1
		TempSprite.w = TempSprite.w / Scaler
		TempSprite.h = TempSprite.h / Scaler		
	ENDIF

	RETURN TempSprite

ENDFUNC

//
FUNC BOOL SHOULD_PLAYER_SEE_THIS_PLAYERS_NAME(PLAYER_INDEX PlayerID, PLAYER_INDEX OtherPlayerID)
	
	ENTITY_INDEX AnEnt
	
	IF NETWORK_IS_IN_TUTORIAL_SESSION()
		IF NOT NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PlayerID, OtherPlayerID)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_ON_TUTORIAL(OtherPlayerID)
		RETURN FALSE
	ENDIF
	
	IF IS_CINEMATIC_CAM_RENDERING() = TRUE
		RETURN FALSE
	ENDIF
	
	
//	IF IS_PLAYER_COP(PlayerID)
//		IF IS_PLAYER_CUFFED(OtherPlayerID)
//		OR IS_PLAYER_IN_CUSTODY(OtherPlayerID)
//			RETURN FALSE
//		ENDIF
//	ENDIF
	
	IF ARE_PLAYERS_ON_SAME_TEAM(PlayerID, OtherPlayerID)
		RETURN TRUE
	ENDIF
	
	
	
	IF IS_PLAYER_TARGETTING_ANYTHING(PlayerID)
		IF GET_PLAYER_TARGET_ENTITY(PlayerId, AnEnt)
			IF IS_ENTITY_A_PED(AnEnt)
				IF NOT IS_ENTITY_DEAD(AnEnt)
					IF IS_PED_A_PLAYER(GET_PED_INDEX_FROM_ENTITY_INDEX(AnEnt))
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_ENTITY_PLAYER_IS_FREE_AIMING_AT(PlayerId, AnEnt)
		IF IS_ENTITY_A_PED(AnEnt)
			IF NOT IS_ENTITY_DEAD(AnEnt)
				IF IS_PED_A_PLAYER(GET_PED_INDEX_FROM_ENTITY_INDEX(AnEnt))
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
//	IF IS_PLAYER_COP(PlayerID)
//		IF IS_PLAYER_CRIMINAL(OtherPlayerID)
			IF GET_PLAYER_WANTED_LEVEL(OtherPlayerID) > 0
				RETURN TRUE
			ENDIF
//		ENDIF
//	ENDIF
	

	
	RETURN FALSE
ENDFUNC


FUNC OVERHEAD_OFFSETS GET_PLAYERS_OVERHEAD_OFFSET(PLAYER_INDEX aPlayer)
			
	IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(aPlayer))
		VEHICLE_INDEX aVeh = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(aPlayer))
		
		IF NOT IS_ENTITY_DEAD(aVeh)
			IF IS_PED_ON_ANY_BIKE(GET_PLAYER_PED(aPlayer)) 
				IF GET_PED_IN_VEHICLE_SEAT(aVeh, VS_DRIVER) = GET_PLAYER_PED(aPlayer)
					RETURN OO_BIKE_DRIVER
				ENDIF
				IF GET_PED_IN_VEHICLE_SEAT(aVeh, VS_FRONT_RIGHT) = GET_PLAYER_PED(aPlayer)
					RETURN OO_BIKE_PASSENGER
				ENDIF
			ENDIF
			
			IF GET_ENTITY_MODEL(aVeh) = SEASHARK
			OR GET_ENTITY_MODEL(aVeh) = SEASHARK2
				IF GET_PED_IN_VEHICLE_SEAT(aVeh, VS_DRIVER) = GET_PLAYER_PED(aPlayer)
					RETURN OO_BIKE_DRIVER
				ENDIF
				IF GET_PED_IN_VEHICLE_SEAT(aVeh, VS_FRONT_RIGHT) = GET_PLAYER_PED(aPlayer)
					RETURN OO_BIKE_PASSENGER
				ENDIF
			ENDIF
			
			IF GET_PED_IN_VEHICLE_SEAT(aVeh, VS_DRIVER) = GET_PLAYER_PED(aPlayer)
				RETURN OO_DRIVER
			ENDIF
			
			IF GET_PED_IN_VEHICLE_SEAT(aVeh, VS_FRONT_RIGHT) = GET_PLAYER_PED(aPlayer)
				RETURN OO_FRONT_RIGHT
			ENDIF
			
			IF GET_PED_IN_VEHICLE_SEAT(aVeh, VS_BACK_LEFT) = GET_PLAYER_PED(aPlayer)
				RETURN OO_BACK_LEFT
			ENDIF
			
			IF GET_PED_IN_VEHICLE_SEAT(aVeh, VS_BACK_RIGHT) = GET_PLAYER_PED(aPlayer)
				RETURN OO_BACK_RIGHT
			ENDIF
	
		ENDIF
		
	ENDIF
	
	RETURN OO_NONE
	
ENDFUNC

FUNC BOOL SHOULD_DPADDOWN_BE_ACTIVE(PLAYER_INDEX aPLAYER)

	#IF IS_DEBUG_BUILD
//		IF g_DebugDpadDownSet = FALSE
//			g_DebugDpadDownGlobalPrint = GET_THE_NETWORK_TIMER()
//			g_DebugDpadDownSet = TRUE
//		ENDIF
		BOOL Printout
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(g_DebugDpadDownGlobalPrint, 6000) 
			Printout = TRUE
		ENDIF
	#ENDIF

	IF IS_PHONE_ONSCREEN()
		#IF IS_DEBUG_BUILD
		IF Printout
			NET_PRINT(" >>> >>> SHOULD_DPADDOWN_BE_ACTIVE = FALSE - IS_PHONE_ONSCREEN") NET_NL()
			
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
//	IF IS_TOPLEFT_INFOBOX_ONSCREEN()
//		#IF IS_DEBUG_BUILD
//			IF Printout
//				NET_PRINT(" >>> >>> SHOULD_DPADDOWN_BE_ACTIVE = FALSE - IS_TOPLEFT_INFOBOX_ONSCREEN") NET_NL()
//				
//			ENDIF
//		#ENDIF
//		RETURN FALSE
//	ENDIF
	IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		#IF IS_DEBUG_BUILD
			IF Printout
				NET_PRINT(" >>> >>> SHOULD_DPADDOWN_BE_ACTIVE = FALSE - IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)") NET_NL()
				
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
//	IF NOT CAN_MISSION_OVERLAY_BE_DISPLAYED()
//	IF IS_SCRIPT_HUD_DISABLED(HUDPART_STATBOX)
//		#IF IS_DEBUG_BUILD
//			IF GET_THE_NETWORK_TIMER() - g_DebugDpadDownGlobalPrint > 6000
//				NET_PRINT(" >>> >>> SHOULD_DPADDOWN_BE_ACTIVE = FALSE - IS_SCRIPT_HUD_DISABLED(HUDPART_STATBOX)") NET_NL()
//				g_DebugDpadDownGlobalPrint = GET_THE_NETWORK_TIMER()
//			ENDIF
//		#ENDIF
//		RETURN FALSE
//	ENDIF
	IF IS_SCRIPT_HUD_DISABLED(HUDPART_ALL_OVERHEADS) 
		#IF IS_DEBUG_BUILD
			IF Printout
				NET_PRINT(" >>> >>> SHOULD_DPADDOWN_BE_ACTIVE = FALSE - IS_SCRIPT_HUD_DISABLED(HUDPART_ALL_OVERHEADS)") NET_NL()
				
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
//	IF IS_MP_OVERHEAD_STATS_ACTIVE_FOR_PLAYER(aPLAYER) = FALSE
	IF IS_SCRIPT_HUD_DISABLED(HUDPART_THISPLAYER_OVERHEADS, aPLAYER) 
		#IF IS_DEBUG_BUILD
			IF Printout
				NET_PRINT(" >>> >>> SHOULD_DPADDOWN_BE_ACTIVE = FALSE - IS_SCRIPT_HUD_DISABLED(HUDPART_THISPLAYER_OVERHEADS, aPLAYER) ") NET_NL()
				
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		#IF IS_DEBUG_BUILD
			IF Printout
				NET_PRINT(" >>> >>> SHOULD_DPADDOWN_BE_ACTIVE = FALSE - NETWORK_IS_IN_MP_CUTSCENE") NET_NL()
				
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		#IF IS_DEBUG_BUILD
			IF Printout
				NET_PRINT(" >>> >>> SHOULD_DPADDOWN_BE_ACTIVE = FALSE - IS_PAUSE_MENU_ACTIVE") NET_NL()
				
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF

	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_AWARDS_BE_ACTIVE()

	#IF IS_DEBUG_BUILD
		BOOL bPrint
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(AwardDebugTimer, 6000)
			bPrint = TRUE
		ENDIF
		
	#ENDIF

	IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		#IF IS_DEBUG_BUILD	
			IF bPrint
				NET_NL()NET_PRINT("SHOULD_AWARDS_BE_ACTIVE = FALSE - IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)")
			ENDIF
		#ENDIF	
		RETURN FALSE
	ENDIF
	
	
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		#IF IS_DEBUG_BUILD	
			IF bPrint
				NET_NL()NET_PRINT("SHOULD_AWARDS_BE_ACTIVE = FALSE - NETWORK_IS_IN_MP_CUTSCENE")
			ENDIF
		#ENDIF	
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		#IF IS_DEBUG_BUILD	
			IF bPrint
				NET_NL()NET_PRINT("SHOULD_AWARDS_BE_ACTIVE = FALSE - IS_PAUSE_MENU_ACTIVE")
			ENDIF
		#ENDIF	
		RETURN FALSE
	ENDIF

	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_RANK_BE_ACTIVE(BOOL bAllowInCutscene = FALSE)
	
	#IF IS_DEBUG_BUILD
		BOOL bPrint
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(RankDebugTimer, 6000)
			bPrint = TRUE
		ENDIF
		
	#ENDIF
	
	IF MPGlobalsAmbience.bFMIntroCutDemandingDpadDownActive
		#IF IS_DEBUG_BUILD	
			IF bPrint
			NET_NL()NET_PRINT("SHOULD_RANK_BE_ACTIVE = TRUE - MPGlobalsAmbience.bFMIntroCutDemandingDpadDownActive = TRUE")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_SCRIPT_HUD_DISABLED(HUDPART_RANKBAR)
		#IF IS_DEBUG_BUILD	
			IF bPrint
		NET_NL()NET_PRINT("SHOULD_RANK_BE_ACTIVE = FALSE - IS_SCRIPT_HUD_DISABLED(HUDPART_RANKBAR)")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF gdisablerankupmessage // for disabling rank ups when going into the activities and cutscenes
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("SHOULD_RANK_BE_ACTIVE = FALSE gdisablerankupmessage ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	IF g_bCelebrationScreenIsActive = TRUE
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("SHOULD_RANK_BE_ACTIVE = FALSE: g_bCelebrationScreenIsActive ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_bBlockRankuphudendofactivity = TRUE
		#IF IS_DEBUG_BUILD	
			IF bPrint
			NET_NL()NET_PRINT("SHOULD_RANK_BE_ACTIVE = FALSE - g_bBlockRankuphudendofactivity = TRUE")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	
//	IF IS_DPADDOWN_SET_AS_ACTIVE() = FALSE
//		#IF IS_DEBUG_BUILD	
//			IF bPrint
//		NET_NL()NET_PRINT("SHOULD_RANK_BE_ACTIVE = FALSE - IS_DPADDOWN_SET_AS_ACTIVE() = FALSE")
//			ENDIF
//		#ENDIF
//		RETURN FALSE
//	ENDIF
	
//	IF IS_DPADDOWN_DISABLED_THIS_FRAME()
//		#IF IS_DEBUG_BUILD	
//			IF bPrint
//		NET_NL()NET_PRINT("SHOULD_RANK_BE_ACTIVE = FALSE - IS_DPADDOWN_DISABLED_THIS_FRAME() = TRUE")
//			ENDIF
//		#ENDIF
//		RETURN FALSE
//	ENDIF
	
	IF g_b_OnLeaderboard
		#IF IS_DEBUG_BUILD	
			IF bPrint
				NET_NL()NET_PRINT("SHOULD_RANK_BE_ACTIVE = FALSE - g_b_OnLeaderboard")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_b_OnHordeWaveBoard
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("SHOULD_RANK_BE_ACTIVE = FALSE: g_b_OnHordeWaveBoard = TRUE ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	// Disable for race intro (902231)
	IF g_b_OnRaceIntro
		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("SHOULD_RANK_BE_ACTIVE = FALSE: g_b_OnRaceIntro = TRUE ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_TRANSITION_ACTIVE()
			#IF IS_DEBUG_BUILD	
			IF bPrint
				NET_NL()NET_PRINT("SHOULD_RANK_BE_ACTIVE = FALSE - IS_TRANSITION_ACTIVE")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	
	IF g_b_OnResults = TRUE
		#IF IS_DEBUG_BUILD	
			IF bPrint
				NET_NL()NET_PRINT("SHOULD_RANK_BE_ACTIVE = FALSE - g_b_OnResults")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		#IF IS_DEBUG_BUILD	
			IF bPrint
				NET_NL()NET_PRINT("SHOULD_RANK_BE_ACTIVE = FALSE - IS_PAUSE_MENU_ACTIVE")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_AN_ANIMAL()
		#IF IS_DEBUG_BUILD	
			IF bPrint
				NET_NL()NET_PRINT("SHOULD_RANK_BE_ACTIVE = FALSE - IS_PLAYER_AN_ANIMAL")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF bAllowInCutscene = FALSE
		
		IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
			#IF IS_DEBUG_BUILD	
				IF bPrint
					NET_NL()NET_PRINT("SHOULD_RANK_BE_ACTIVE = FALSE - IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)")
				ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
		
		
		IF NETWORK_IS_IN_MP_CUTSCENE()
			#IF IS_DEBUG_BUILD	
				IF bPrint
					NET_NL()NET_PRINT("SHOULD_RANK_BE_ACTIVE = FALSE - NETWORK_IS_IN_MP_CUTSCENE")
				ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
		
		
		
		
	ENDIF
	
	RETURN TRUE
	
ENDFUNC



//PROC DISPLAY_NORMAL_OVERHEAD(PLAYER_INDEX LocalPlayer, PLAYER_INDEX OtherPlayer, OVERHEAD_OFFSETS anOffset)
//	IF SHOULD_PLAYER_SEE_THIS_PLAYERS_NAME(LocalPlayer, OtherPlayer)
//		PROCESS_PLAYER_NAME_DISPLAY(OtherPlayer, anOffset)
//	ENDIF
//ENDPROC

FUNC BOOL SHOULD_DISPLAY_OTHER_PLAYER_STATS_WITH_DPADDOWN_ON(PLAYER_INDEX PlayerID, PLAYER_INDEX OtherPlayerID)
	
	IF IS_NET_PLAYER_BLIPPED(OtherPlayerID)
		RETURN TRUE
	ENDIF

	IF SHOULD_PLAYER_SEE_THIS_PLAYERS_NAME(PlayerID, OtherPlayerID)
		RETURN TRUE
	ENDIF
	
	IF OtherPlayerID <> PLAYER_ID()
		//Add check so block enemy names unless targetting.
		IF ARE_PLAYER_IN_SAME_VEHICLE(PlayerID, OtherPlayerID)
			IF IS_CINEMATIC_CAM_RENDERING() = FALSE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC





//
//FUNC INT GET_NEXT_FREE_GLOBALTICKER_SLOT(GLOBALTICKERDETAIL& TheArray[])
//	INT RESULT
//	INT I
//	FOR I = 0 TO MAX_NUM_GLOBALTICKER_QUEUE-1
//		IF TheArray[I].Ticker_type = GLOBALTICKERENUM_NONE
//			RESULT = I
//			I = MAX_NUM_GLOBALTICKER_QUEUE
//		ENDIF
//	ENDFOR
//	RETURN RESULT
//ENDFUNC
//
//FUNC INT GET_NEXT_GLOBALTICKER_TO_SHOW(GLOBALTICKERDETAIL& TheArray[])
//
//	INT RESULT = -1
//	INT I
//	FOR I = 0 TO MAX_NUM_GLOBALTICKER_QUEUE-1
//		IF TheArray[I].Ticker_type <> GLOBALTICKERENUM_NONE
//			RESULT = I
//			I = MAX_NUM_GLOBALTICKER_QUEUE
//		ENDIF
//	ENDFOR
//	RETURN RESULT
//
//ENDFUNC
//
//
//PROC RESET_A_GLOBALTICKER_TO_SHOW(GLOBALTICKERDETAIL& TheArray[], INT iSlot)
//	GLOBALTICKERDETAIL anEmptyStruct
//	TheArray[iSlot] = anEmptyStruct
//ENDPROC
//
//PROC RESET_ALL_GLOBALTICKERS_TO_SHOW(GLOBALTICKERDETAIL& TheArray[])
//	INT I
//	FOR I = 0 TO MAX_NUM_GLOBALTICKER_QUEUE-1
//		RESET_A_GLOBALTICKER_TO_SHOW(TheArray,I)
//	ENDFOR
//ENDPROC
//
//PROC MOVE_ALL_GLOBALTICKERARRAY_UP_ONE(GLOBALTICKERDETAIL& TheArray[])
//
//	GLOBALTICKERDETAIL anEmptyStruct
//	GLOBALTICKERDETAIL aMovingStruct
//	
//	INT I
//	RESET_A_GLOBALTICKER_TO_SHOW(TheArray,0)
//	FOR I = 1 TO MAX_NUM_GLOBALTICKER_QUEUE-1
//		aMovingStruct = TheArray[I] 
//		TheArray[I-1] = aMovingStruct
//		TheArray[I] = anEmptyStruct
//	ENDFOR
//			
//ENDPROC
//
//PROC PRINT_GLOBAL_TICKER(GLOBALTICKERENUM TickerType, STRING Ticker_Text)
//	
//	INT iSlot = GET_NEXT_FREE_GLOBALTICKER_SLOT(MPGlobalsHudPosition.GlobalXPTickerArray)
//	MPGlobalsHudPosition.GlobalXPTickerArray[iSlot].Ticker_Type = TickerType
//	MPGlobalsHudPosition.GlobalXPTickerArray[iSlot].Ticker_Title = Ticker_Text
//	
//ENDPROC
//
//
//
//ENUM GLOBALTICKERDRAWING
//	GLOBALTICKERDRAWING_LISTENING = 0,
//	GLOBALTICKERDRAWING_FADEIN,
//	GLOBALTICKERDRAWING_DRAW,
//	GLOBALTICKERDRAWING_FADEOUT,
//	GLOBALTICKERDRAWING_FINISHED
//ENDENUM
//
//STRUCT GlobalXPStruct
//
//	TEXT_STYLE RockstarLogo
//	TEXT_STYLE TickerTextStyle
//	
//	INT TickerToShow = -1
//	GLOBALTICKERDRAWING DrawTickerStages = GLOBALTICKERDRAWING_LISTENING
//	TEXT_PLACEMENT RockstarLogoPlacement
//	TEXT_PLACEMENT TickerPlacement
//	INT GlobalXP_FadeInRate = 40
//	INT GlobalXP_FadeOutRate = 40
//	INT GlobalXP_DrawTimer
//	INT GlobalXP_DrawForHowLong = 7000
//	BOOL InitGlobalTickers
//
//
//	#IF IS_DEBUG_BUILD	
//	
////	FLOAT Widget_TickerOffsetX 
////	FLOAT Widget_TickerOffsetY
//	BOOL Widget_tickerHold
//	#ENDIF
//
//ENDSTRUCT
//
//PROC RESET_GLOBALTICKER_DATA(GlobalXPStruct& DisplayStruct)
//	DisplayStruct.TickerToShow = -1
//	DisplayStruct.DrawTickerStages = GLOBALTICKERDRAWING_LISTENING
//	DisplayStruct.InitGlobalTickers = FALSE
//ENDPROC
//
//
//FUNC BOOL DISPLAY_GLOBAL_XP_TICKER(GLOBALTICKERDETAIL& TickerArray[], GlobalXPStruct& DisplayStruct)
//
//
//
//	IF DisplayStruct.InitGlobalTickers = FALSE
//
//		DisplayStruct.RockstarLogoPlacement.x = 0.160
//		DisplayStruct.RockstarLogoPlacement.y = 0.740
//
//		DisplayStruct.RockstarLogo.aFont = 
//		DisplayStruct.RockstarLogo.XScale = 0.202 
//		DisplayStruct.RockstarLogo.YScale = 0.5
//		DisplayStruct.RockstarLogo.r = 255
//		DisplayStruct.RockstarLogo.g = 255
//		DisplayStruct.RockstarLogo.b = 255
//		DisplayStruct.RockstarLogo.a = 0
//		DisplayStruct.RockstarLogo.drop = DROPSTYLE_OUTLINEONLY
//		
//		DisplayStruct.TickerTextStyle.aFont = FONT_STANDARD
//		DisplayStruct.TickerTextStyle.XScale = 0.202 
//		DisplayStruct.TickerTextStyle.YScale = 0.311
//		DisplayStruct.TickerTextStyle.r = 255
//		DisplayStruct.TickerTextStyle.g = 255
//		DisplayStruct.TickerTextStyle.b = 255
//		DisplayStruct.TickerTextStyle.a = 0
//		DisplayStruct.TickerTextStyle.drop = DROPSTYLE_DROPSHADOWONLY
//		DisplayStruct.TickerTextStyle.wrapStartX = 0
//		DisplayStruct.TickerTextStyle.wrapEndX = 0
//		
//		DisplayStruct.InitGlobalTickers = TRUE
//	ENDIF
//	
////	#IF IS_DEBUG_BUILD
//	//
//	//		START_WIDGET_GROUP("GLOBAL XP TICKER")
//	//			
//	//			CREATE_A_TEXT_STYLE_WIGET(DisplayStruct.RockstarLogo, "RockstarLogo")
//	//			CREATE_A_TEXT_STYLE_WIGET(DisplayStruct.TickerTextStyle, "TickerTextStyle")
//	//		
//	//			CREATE_A_TEXT_PLACEMENT_WIDGET(DisplayStruct.RockstarLogoPlacement, "RockstarLogoPlacement")
//	//					
//	//			ADD_WIDGET_INT_SLIDER("GlobalXP_FadeInRate", DisplayStruct.GlobalXP_FadeInRate, 0, 255, 1)
//	//			ADD_WIDGET_INT_SLIDER("GlobalXP_FadeOutRate", DisplayStruct.GlobalXP_FadeOutRate, 0, 255, 1)
//	//			ADD_WIDGET_INT_SLIDER("GlobalXP_DrawForHowLong", DisplayStruct.GlobalXP_DrawForHowLong, 0, 50000, 100)
//	//			ADD_WIDGET_FLOAT_SLIDER("Widget_TickerOffsetX", DisplayStruct.Widget_TickerOffsetX, 0, 1, 0.001)
//	//			ADD_WIDGET_FLOAT_SLIDER("Widget_TickerOffsetY", DisplayStruct.Widget_TickerOffsetY, 0, 1, 0.001)
//	//			ADD_WIDGET_BOOL("Widget_tickerHold", DisplayStruct.Widget_tickerHold)
//	//		STOP_WIDGET_GROUP()
//	//		
//	//		
//	//		DisplayStruct.InitGlobalTickers = TRUE
//	//		
//	//	ENDIF
////	#ENDIF
//	
//	DisplayStruct.TickerPlacement = DisplayStruct.RockstarLogoPlacement
//	
//	DisplayStruct.TickerPlacement.x += 0.013
//	DisplayStruct.TickerPlacement.y += 0.010
//	
////	#IF IS_DEBUG_BUILD
////	DisplayStruct.TickerPlacement.x += DisplayStruct.Widget_TickerOffsetX
////	DisplayStruct.TickerPlacement.y += DisplayStruct.Widget_TickerOffsetY
////	#ENDIF
//
//	SWITCH DisplayStruct.DrawTickerStages
//	
//		CASE GLOBALTICKERDRAWING_LISTENING
//		
//			DisplayStruct.TickerToShow = GET_NEXT_GLOBALTICKER_TO_SHOW(TickerArray)
//			IF DisplayStruct.TickerToShow > -1
//				DisplayStruct.DrawTickerStages = GLOBALTICKERDRAWING_FADEIN
//			ENDIF
//			
//		
//		BREAK
//		
//		CASE GLOBALTICKERDRAWING_FADEIN
//			IF DisplayStruct.RockstarLogo.a < 255
//				DisplayStruct.RockstarLogo.a += DisplayStruct.GlobalXP_FadeInRate
//				IF DisplayStruct.RockstarLogo.a > 255
//				 	DisplayStruct.RockstarLogo.a = 255
//				ENDIF
//			ELSE
//				DisplayStruct.RockstarLogo.a = 255
//			ENDIF
//			
//			IF DisplayStruct.TickerTextStyle.a < 255
//				DisplayStruct.TickerTextStyle.a += DisplayStruct.GlobalXP_FadeInRate
//				IF DisplayStruct.TickerTextStyle.a > 255
//				 	DisplayStruct.TickerTextStyle.a = 255
//				ENDIF
//			ELSE
//				DisplayStruct.TickerTextStyle.a = 255
//			ENDIF
//			
//			
//			IF DisplayStruct.RockstarLogo.a = 255
//			AND DisplayStruct.TickerTextStyle.a = 255
//				DisplayStruct.GlobalXP_DrawTimer = GET_THE_NETWORK_TIMER()
//				DisplayStruct.DrawTickerStages = GLOBALTICKERDRAWING_DRAW
//			ENDIF
//			
//		BREAK
//		
//		CASE GLOBALTICKERDRAWING_DRAW
//			#IF IS_DEBUG_BUILD
//				IF DisplayStruct.Widget_tickerHold
//					BREAK
//				ENDIF
//			#ENDIF
//		
//			IF GET_THE_NETWORK_TIMER() - DisplayStruct.GlobalXP_DrawTimer > DisplayStruct.GlobalXP_DrawForHowLong
//				DisplayStruct.DrawTickerStages = GLOBALTICKERDRAWING_FADEOUT
//			ENDIF
//		
//		BREAK
//		
//		CASE GLOBALTICKERDRAWING_FADEOUT
//
//			
//			IF DisplayStruct.RockstarLogo.a > 0
//				DisplayStruct.RockstarLogo.a -= DisplayStruct.GlobalXP_FadeOutRate
//				IF DisplayStruct.RockstarLogo.a < 0
//				 	 DisplayStruct.RockstarLogo.a = 0
//				ENDIF
//			ELSE
//				DisplayStruct.RockstarLogo.a = 0
//			ENDIF
//			
//			IF DisplayStruct.TickerTextStyle.a > 0
//				DisplayStruct.TickerTextStyle.a -= DisplayStruct.GlobalXP_FadeOutRate
//				IF DisplayStruct.TickerTextStyle.a < 0
//				 	 DisplayStruct.TickerTextStyle.a = 0
//				ENDIF
//			ELSE
//				DisplayStruct.TickerTextStyle.a = 0
//			ENDIF
//	
//			
//			IF DisplayStruct.RockstarLogo.a = 0
//			AND DisplayStruct.TickerTextStyle.a = 0
//				DisplayStruct.DrawTickerStages = GLOBALTICKERDRAWING_FINISHED
//			ENDIF
//			
//		BREAK
//		
//		CASE GLOBALTICKERDRAWING_FINISHED
//			
//			MOVE_ALL_GLOBALTICKERARRAY_UP_ONE(TickerArray)
//			RESET_GLOBALTICKER_DATA(DisplayStruct)
//			RETURN TRUE
//		BREAK
//	
//	ENDSWITCH
//	
//	DISPLAYING_SCRIPT_HUD(HUDPART_ROCKSTARTICKER, FALSE)
//	IF IS_SCRIPT_HUD_DISABLED(HUDPART_ROCKSTARTICKER) = FALSE
//		IF DisplayStruct.TickerToShow > -1
//		AND DisplayStruct.DrawTickerStages > GLOBALTICKERDRAWING_LISTENING
//			DISPLAYING_SCRIPT_HUD(HUDPART_ROCKSTARTICKER, TRUE)
//			DRAW_TEXT_WITH_ALIGNMENT(DisplayStruct.RockstarLogoPlacement, DisplayStruct.RockstarLogo, "HUD_ROCKSTAR", FALSE, FALSE)
//			DRAW_TEXT_WITH_ALIGNMENT(DisplayStruct.TickerPlacement, DisplayStruct.TickerTextStyle, TickerArray[DisplayStruct.TickerToShow].Ticker_Title, FALSE, FALSE)
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
//	
//ENDFUNC




PROC DPADDOWN_ONE_TIME_ACTIVE(BOOL IsActive)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL()NET_PRINT("DPADDOWN_ONE_TIME_ACTIVE: IsActive = ")NET_PRINT_BOOL(IsActive)
	#ENDIF
	HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE (IsActive)
ENDPROC

FUNC BOOL IS_DPADDOWN_ACTIVE()

	#IF IS_DEBUG_BUILD
		BOOL PrintDebug
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(DPadDownCheckTimer, 6000)
			PrintDebug = TRUE
		ENDIF
		
	#ENDIF

	IF MPGlobalsAmbience.bFMIntroCutDemandingDpadDownActive
		#IF IS_DEBUG_BUILD
			IF PrintDebug = TRUE
				NET_NL()NET_PRINT("IS_DPADDOWN_ACTIVE: MPGlobalsAmbience.bFMIntroCutDemandingDpadDownActive")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	
	//If the custom menu is on screen
	IF IS_CUSTOM_MENU_ON_SCREEN()
		#IF IS_DEBUG_BUILD
			IF PrintDebug = TRUE
				NET_NL()NET_PRINT("IS_DPADDOWN_ACTIVE: IS_CUSTOM_MENU_ON_SCREEN()")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		#IF IS_DEBUG_BUILD
			IF PrintDebug = TRUE
				NET_NL()NET_PRINT("IS_DPADDOWN_ACTIVE: NETWORK_IS_IN_MP_CUTSCENE()")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		#IF IS_DEBUG_BUILD
			IF PrintDebug = TRUE
				NET_NL()NET_PRINT("IS_DPADDOWN_ACTIVE: IS_PHONE_ONSCREEN()")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_DPADDOWN_SET_AS_ACTIVE() = FALSE
		#IF IS_DEBUG_BUILD
			IF PrintDebug = TRUE
				NET_NL()NET_PRINT("IS_DPADDOWN_ACTIVE: IS_DPADDOWN_SET_AS_ACTIVE() = FALSE")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_DPADDOWN_DISABLED_THIS_FRAME()
		#IF IS_DEBUG_BUILD
			IF PrintDebug = TRUE
				NET_NL()NET_PRINT("IS_DPADDOWN_ACTIVE: IS_DPADDOWN_DISABLED_THIS_FRAME() = TRUE")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF GET_PAUSE_MENU_STATE() = PM_READY	
		#IF IS_DEBUG_BUILD
			IF PrintDebug = TRUE
				NET_NL()NET_PRINT("IS_DPADDOWN_ACTIVE: GET_PAUSE_MENU_STATE() = PM_READY")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_INTERACTING_WITH_BOARD)
		#IF IS_DEBUG_BUILD
			IF PrintDebug = TRUE
				NET_NL()NET_PRINT("IS_DPADDOWN_ACTIVE: GANG_OPS_BD_GENERAL_BITSET_INTERACTING_WITH_BOARD")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF IS_PLAYER_INTERACTING_WITH_CASINO_HEIST_PLANNING_BOARDS(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			IF PrintDebug = TRUE
				NET_NL()NET_PRINT("IS_DPADDOWN_ACTIVE: IS_PLAYER_INTERACTING_WITH_CASINO_HEIST_PLANNING_BOARDS")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	#ENDIF
	
	RETURN TRUE

ENDFUNC

// Speirs dpad leaderboard ///////////////////////////////
PROC NUM_DPAD_LBD_PLAYERS_IS(INT iThisMuch) // Freemode is done in SET_ACTIVE_PLAYERS
	IF g_i_NumPlayersForLbd <> iThisMuch
		g_i_NumPlayersForLbd = iThisMuch
	ENDIF
ENDPROC

///////////////////////////////////////////////////////////

FUNC BOOL SHOULD_DPADDOWN_SKIP_FIRST()
	IF NETWORK_IS_ACTIVITY_SESSION()
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_SimulateSinglePlayerHUD)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_DPADDOWN_CHECK()
	
	BOOL bPrint = FALSE
	
	#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 200 = 0
			bPrint = TRUE
		ENDIF
	#ENDIF
	
	
	IF GET_DPADDOWN_ACTIVATION_STATE() != DPADDOWN_NONE 
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(MPGlobalsHud.iOnMissionOverlayTimer, DPAD_DOWN_DISPLAY_TIME)
			SET_DPADDOWN_ACTIVATION_STATE(DPADDOWN_NONE)
			DPADDOWN_ONE_TIME_ACTIVE(FALSE)
//			OVERRIDE_AREA_VEHICLE_STREET_NAME_TIME_OFF()
		ENDIF
	ENDIF
	
	// If we are in an active state and the timer has expired, reset.
	IF MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState != DPADDOWN_NONE
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(MPGlobalsHud_TitleUpdate.stGamertagsTimer, DPAD_DOWN_GAMERTAGS_DISPLAY_TIME)
			MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState = DPADDOWN_NONE
			
		ENDIF
	ENDIF
	
	IF IS_DPADDOWN_ACTIVE() = FALSE
		IF  GET_DPADDOWN_ACTIVATION_STATE() != DPADDOWN_NONE
			DPADDOWN_ONE_TIME_ACTIVE(FALSE)
//			OVERRIDE_AREA_VEHICLE_STREET_NAME_TIME_OFF()
			SET_DPADDOWN_ACTIVATION_STATE(DPADDOWN_NONE)
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_DPADDOWN_DISABLED_THIS_FRAME()
		IF bPrint
			NET_NL()NET_PRINT("MAINTAIN_DPADDOWN_CHECK: IS_DPADDOWN_DISABLED_THIS_FRAME")
		ENDIF
		IF  GET_DPADDOWN_ACTIVATION_STATE() != DPADDOWN_NONE
			DPADDOWN_ONE_TIME_ACTIVE(FALSE)
			SET_DPADDOWN_ACTIVATION_STATE(DPADDOWN_NONE)
		ENDIF
		IF MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState != DPADDOWN_NONE
			MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState = DPADDOWN_NONE
		ENDIF
		RETURN FALSE 
	ENDIF
	
	IF IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID())
		IF bPrint
			NET_NL()NET_PRINT("MAINTAIN_DPADDOWN_CHECK: IS_AIMING_THROUGH_SNIPER_SCOPE")
		ENDIF
		IF  GET_DPADDOWN_ACTIVATION_STATE() != DPADDOWN_NONE
			DPADDOWN_ONE_TIME_ACTIVE(FALSE)
			SET_DPADDOWN_ACTIVATION_STATE(DPADDOWN_NONE)
		ENDIF
		IF MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState != DPADDOWN_NONE
			MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState = DPADDOWN_NONE
		ENDIF
		RETURN FALSE 
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_MULTIPLAYER_INFO)
		
		IF g_dPadDownButtonPress = FALSE
			g_dPadDownButtonPress = TRUE
		ENDIF
		
		//So Les can press Dpaddown repidly and flick through all states while rank bar is loading. 
		SWITCH GET_DPADDOWN_ACTIVATION_STATE()
			CASE DPADDOWN_NONE
//			 	OVERRIDE_AREA_VEHICLE_STREET_NAME_TIME_ON(DPAD_DOWN_DISPLAY_TIME)
				g_dPadSecondPage = FALSE
				IF SHOULD_DPADDOWN_SKIP_FIRST()
					SET_DPADDOWN_ACTIVATION_STATE(DPADDOWN_SECOND)
				ELSE
					SET_DPADDOWN_ACTIVATION_STATE(DPADDOWN_FIRST)
				ENDIF
			BREAK
			CASE DPADDOWN_FIRST
				IF DOES_DPAD_LBD_NEED_SECOND_PAGE()
					IF NOT g_dPadSecondPage
						REFRESH_DPAD(TRUE)
						g_dPadSecondPage = TRUE
						IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
							SET_CONTROL_VALUE_NEXT_FRAME(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL, 1) // B*2201778 Ensure the leaderboard gets refreshed if pressing the keyboard key to cycle multiplayer info
						ENDIF
					ELSE
						g_dPadSecondPage = FALSE
						SET_DPADDOWN_ACTIVATION_STATE(DPADDOWN_SECOND)
					ENDIF
				ELSE
					SET_DPADDOWN_ACTIVATION_STATE(DPADDOWN_SECOND)
				ENDIF
			BREAK
			CASE DPADDOWN_SECOND
//				OVERRIDE_AREA_VEHICLE_STREET_NAME_TIME_OFF()
				g_dPadSecondPage = FALSE
				SET_DPADDOWN_ACTIVATION_STATE(DPADDOWN_NONE)
				DPADDOWN_ONE_TIME_ACTIVE(FALSE)
			BREAK
		ENDSWITCH
		
		SWITCH MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState 
			CASE DPADDOWN_NONE
				NET_PRINT("[WJK] - overheads - gamertag dpad down state in state NONE, going to state FIRST.")NET_NL()
				MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState = DPADDOWN_FIRST
			BREAK
			CASE DPADDOWN_FIRST
				NET_PRINT("[WJK] - overheads - gamertag dpad down state in state FIRST, going to state SECOND.")NET_NL()
				MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState = DPADDOWN_SECOND
			BREAK
			CASE DPADDOWN_SECOND
				NET_PRINT("[WJK] - overheads - gamertag dpad down state in state SECOND, going to state NONE.")NET_NL()
				MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState = DPADDOWN_NONE
			BREAK
		ENDSWITCH
		
	ENDIF
	
	
	IF g_dPadDownButtonPress
		
		SWITCH GET_DPADDOWN_ACTIVATION_STATE()
			CASE DPADDOWN_NONE
				IF NOT IS_PAUSE_MENU_ACTIVE()
					IF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID()) = FALSE
						DPADDOWN_ONE_TIME_ACTIVE(FALSE)
					ENDIF
					RESET_NET_TIMER(MPGlobalsHud.iOnMissionOverlayTimer)
				ENDIF
			BREAK
			CASE DPADDOWN_FIRST
				IF NOT IS_PAUSE_MENU_ACTIVE()
					
					RESET_NET_TIMER(MPGlobalsHud.iOnMissionOverlayTimer)
				ENDIF
			BREAK
			CASE DPADDOWN_SECOND
				IF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID()) = FALSE
					DPADDOWN_ONE_TIME_ACTIVE(TRUE)
				ENDIF
				
				RESET_NET_TIMER(MPGlobalsHud.iOnMissionOverlayTimer)
			BREAK
		ENDSWITCH
		
		SWITCH MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState 
			CASE DPADDOWN_NONE
				IF NOT IS_PAUSE_MENU_ACTIVE()
					RESET_NET_TIMER(MPGlobalsHud_TitleUpdate.stGamertagsTimer)
				ENDIF
			BREAK
			CASE DPADDOWN_FIRST
				IF NOT IS_PAUSE_MENU_ACTIVE()
					RESET_NET_TIMER(MPGlobalsHud_TitleUpdate.stGamertagsTimer)
				ENDIF
			BREAK
			CASE DPADDOWN_SECOND
				IF NOT IS_PAUSE_MENU_ACTIVE()
					RESET_NET_TIMER(MPGlobalsHud_TitleUpdate.stGamertagsTimer)
				ENDIF
			BREAK
		ENDSWITCH
		
		g_dPadDownButtonPress = FALSE
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC





PROC SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(BOOL Set)

	IF g_b_ShowRadarNames = FALSE
	AND Set = TRUE
		#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			NET_NL()NET_PRINT("SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS = ")NET_PRINT_BOOL(Set)
		#ENDIF

		g_b_ShowRadarNames = Set
		DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(Set)
	ENDIF
	
	IF g_b_ShowRadarNames = TRUE
	AND Set = FALSE
		#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			NET_NL()NET_PRINT("SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS = ")NET_PRINT_BOOL(Set)
		#ENDIF

		g_b_ShowRadarNames = Set
		DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(Set)
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Updates the radar zoom so the boundary of the radar is slightly further than the furthest away remote player.
/// PARAMS:
///    bInstant - ask Rowan, he wrote this.
PROC CONTROL_RADAR_ZOOM_FREEMODE()//(BOOL bInstant = FALSE)

	IF network_is_activity_session() = FALSE
		EXIT
	ENDIF
	
//	IF NOT g_b_Do_RadarZoom
//		g_b_Do_RadarZoom = TRUE
//	ENDIF

//	FLOAT Radar_fZoomLevel
//
//	IF NOT bInstant
//		
//		IF g_fOldFurthestTargetDist > g_fFurthestTargetDist
//			IF (g_fOldFurthestTargetDist - g_fFurthestTargetDist) > 5
//				g_fOldFurthestTargetDist= g_fOldFurthestTargetDist-cfRADAR_ZOOM_SPEED
//			ENDIF
//		ELSE
//			IF (g_fFurthestTargetDist - g_fOldFurthestTargetDist) > 5
//				g_fOldFurthestTargetDist= g_fOldFurthestTargetDist+cfRADAR_ZOOM_SPEED
//			ENDIF
//		ENDIF
//		
//		Radar_fZoomLevel = g_fOldFurthestTargetDist/cfRADAR_ZOOM_ADJUST
//		
//		// Cap the zoom level
//		IF (Radar_fZoomLevel >= cfRADAR_ZOOM_CAP)
//			Radar_fZoomLevel = cfRADAR_ZOOM_CAP
//		ENDIF
//		
//		SET_RADAR_ZOOM_TO_DISTANCE(Radar_fZoomLevel)
//		
//	ELSE
//	
//		g_fOldFurthestTargetDist = g_fFurthestTargetDist
//		Radar_fZoomLevel = g_fOldFurthestTargetDist/cfRADAR_ZOOM_ADJUST
//		SET_RADAR_ZOOM_TO_DISTANCE(Radar_fZoomLevel)
//		
//	ENDIF
	
ENDPROC

PROC RESET_CONTROL_MAP_ZOOM()
//	IF g_b_Do_RadarZoom
//		g_b_Do_RadarZoom = FALSE
//	ENDIF
	
//	g_fFurthestTargetDist = 50.0
//	g_fOldFurthestTargetDist = 50.0
//	g_fPartDis = 0
//	g_fHeightMapHeight = -1.0

ENDPROC

PROC SHOW_STREET_AND_VEHICLE_INFO_HUD_THIS_FRAME()
	PRINTLN("[ST_DEBUG] [UI] Vehicle/Street info being shown this frame")
	SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
	SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
	SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
ENDPROC

PROC DISABLE_RADAR_MAP(BOOL bState)
	PRINTLN("DISABLE_RADAR_MAP = ", bState)
	g_b_DisableRadarMap = bState
	DEBUG_PRINTCALLSTACK()
ENDPROC
FUNC BOOL IS_RADAR_MAP_DISABLED()
	RETURN g_b_DisableRadarMap
ENDFUNC
PROC MAINTAIN_STREET_AND_VEHICLE_INFO_DISPLAY()
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_HUD_SPECIAL)
		IF NOT IS_RADAR_MAP_DISABLED()
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		AND NOT IS_SELECTOR_CAM_ACTIVE()
		AND NOT IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID())
		AND g_sSelectorUI.eUIStateCurrent != SELECTOR_UI_AUTOSWITCH_HINT
		AND g_b_Private_FMeventsHidingCodeUIThisFrame = FALSE
			SHOW_STREET_AND_VEHICLE_INFO_HUD_THIS_FRAME()
		ENDIF
		
	ENDIF
	
	g_b_Private_FMeventsHidingCodeUIThisFrame = FALSE
	
ENDPROC

PROC RUN_RADAR_MAP()
	IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_INTERACTING_WITH_BOARD)
	OR IS_PLAYER_INTERACTING_WITH_CASINO_HEIST_PLANNING_BOARDS(PLAYER_ID())
	OR g_sNetHeistPlanningGenericClientData.sStateData.bInteract
		PRINTLN("[radarandlbd] - interacting with planning board")
		EXIT
	ENDIF

	IF NOT IS_RADAR_MAP_DISABLED() //added for flight school to disable control of dpad down.
//		g_PlayerBlipsData.bBigMapIsActive = FALSE
		IF IS_SCRIPT_HUD_DISABLED(HUDPART_MAP) = FALSE
			
			IF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
	
				IF NOT IS_PHONE_ONSCREEN(TRUE)
				AND NOT IS_CUSTOM_MENU_ON_SCREEN() 
					SWITCH g_i_PlaneDpadIsActive
						CASE 0
							IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_HUD_SPECIAL)
								RESET_CONTROL_MAP_ZOOM()
								g_i_PlaneDpadIsActive++
								NET_NL()NET_PRINT("RUN_RADAR_MAP: In Plane Turn On Map ")
								IF GET_MP_BOOL_PLAYER_STAT(MPPLY_TOGGLE_OFF_BIG_RADAR) = FALSE
									DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(TRUE)
									SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(TRUE)
								ENDIF
							
								SET_BIGMAP_ACTIVE(TRUE, FALSE) //SET_BIGMAP_ACTIVE(TRUE, TRUE)
								g_PlayerBlipsData.bBigMapIsActive = TRUE
								DPADDOWN_ONE_TIME_ACTIVE(TRUE)
								
							ENDIF
						BREAK
						CASE 1
							CONTROL_RADAR_ZOOM_FREEMODE()
							IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_HUD_SPECIAL)
								RESET_CONTROL_MAP_ZOOM()
								NET_NL()NET_PRINT("RUN_RADAR_MAP: In Plane Turn off Map ")
								g_i_PlaneDpadIsActive--
								SET_BIGMAP_ACTIVE(FALSE, FALSE)
								RESET_CONTROL_MAP_ZOOM()
								DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE)
								SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE)
								g_PlayerBlipsData.bBigMapIsActive = FALSE
								DPADDOWN_ONE_TIME_ACTIVE(FALSE)
							ENDIF
						BREAK
						CASE 2
							
						BREAK
					
					ENDSWITCH
				ELSE
					IF g_i_PlaneDpadIsActive = 1
						NET_NL()NET_PRINT("RUN_RADAR_MAP: External happenings, In Plane Turn off Map ")
						g_i_PlaneDpadIsActive--
						SET_BIGMAP_ACTIVE(FALSE, FALSE)
						DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE)
						RESET_CONTROL_MAP_ZOOM()
						SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE)
						g_PlayerBlipsData.bBigMapIsActive = FALSE
						DPADDOWN_ONE_TIME_ACTIVE(FALSE)
						RESET_CONTROL_MAP_ZOOM()
					ENDIF
				ENDIF
				
			
			
			ELIF GET_PACKED_STAT_BOOL(PACKED_MP_TOGGLE_EXPANDED_RADAR) OR GET_PROFILE_SETTING(PROFILE_DIAPLAY_BIG_RADAR) = 1
				
												
//				IF GET_MP_BOOL_PLAYER_STAT(MPPLY_TOGGLE_OFF_BIG_RADAR) = FALSE OR GET_PROFILE_SETTING(PROFILE_DIAPLAY_BIG_RADAR_NAMES) = 1
//					SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(TRUE)
//				ELSE
//					DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE)	
//					SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE)
//				ENDIF
				
				CONTROL_RADAR_ZOOM_FREEMODE()
				
				SWITCH GET_DPADDOWN_ACTIVATION_STATE() 
					CASE DPADDOWN_NONE
					
						IF g_b_ShowRadarNames
							DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE)	
							SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE)
						ENDIF
					BREAK
					
					CASE DPADDOWN_FIRST
						IF g_b_ShowRadarNames = FALSE
							DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(TRUE)	
							SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(TRUE)
						ENDIF
					BREAK
					
					CASE DPADDOWN_SECOND
						IF g_b_ShowRadarNames = FALSE
							DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(TRUE)	
							SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(TRUE)
						ENDIF
					
					BREAK
					
				ENDSWITCH
				
				
				
				#IF IS_DEBUG_BUILD
					IF g_b_DisplayPrintsForRadarMap
						NET_NL()NET_PRINT("RUN_RADAR_MAP: SET_BIGMAP_ACTIVE(TRUE, FALSE) GET_PACKED_STAT_BOOL(PACKED_MP_TOGGLE_EXPANDED_RADAR) = ")NET_PRINT_BOOL(GET_PACKED_STAT_BOOL(PACKED_MP_TOGGLE_EXPANDED_RADAR))
						NET_NL()NET_PRINT("RUN_RADAR_MAP: SET_BIGMAP_ACTIVE(TRUE, FALSE) GET_PROFILE_SETTING(PROFILE_DIAPLAY_BIG_RADAR) = ")NET_PRINT_INT(GET_PROFILE_SETTING(PROFILE_DIAPLAY_BIG_RADAR))
					ENDIF
				#ENDIF
				
				SET_BIGMAP_ACTIVE(TRUE, FALSE)
				g_PlayerBlipsData.bBigMapIsActive = TRUE
				
			ELSE
			
				IF CAN_DISPLAY_DPADDOWN_PARTS()
				
					SWITCH GET_DPADDOWN_ACTIVATION_STATE() 
						CASE DPADDOWN_NONE
						
							#IF IS_DEBUG_BUILD
							IF g_b_DisplayPrintsForRadarMap
								NET_NL()NET_PRINT("RUN_RADAR_MAP: DPADDOWN_NONE SET_BIGMAP_ACTIVE(FALSE, FALSE) ")
							ENDIF
							#ENDIF
						
							RESET_CONTROL_MAP_ZOOM()
							SET_BIGMAP_ACTIVE(FALSE, FALSE)
							DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE)
							SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE)
							RESET_CONTROL_MAP_ZOOM()
							g_PlayerBlipsData.bBigMapIsActive = FALSE
						BREAK
						CASE DPADDOWN_FIRST
							#IF IS_DEBUG_BUILD
							IF g_b_DisplayPrintsForRadarMap
							NET_NL()NET_PRINT("RUN_RADAR_MAP: DPADDOWN_FIRST SET_BIGMAP_ACTIVE(FALSE, FALSE) ")
							ENDIF
							#ENDIF
							
							RESET_NET_TIMER(g_st_PlaneDpadNameTimer)
							SET_BIGMAP_ACTIVE(FALSE, FALSE) //SET_BIGMAP_ACTIVE(TRUE, FALSE)
							DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE)
							SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE)
							RESET_CONTROL_MAP_ZOOM()
							g_PlayerBlipsData.bBigMapIsActive = FALSE
						BREAK
						CASE DPADDOWN_SECOND
							CONTROL_RADAR_ZOOM_FREEMODE()
							
							#IF IS_DEBUG_BUILD
							IF g_b_DisplayPrintsForRadarMap
							NET_NL()NET_PRINT("RUN_RADAR_MAP: DPADDOWN_SECOND SET_BIGMAP_ACTIVE(TRUE, FALSE) ")
							ENDIF
							#ENDIF														
							IF HAS_NET_TIMER_EXPIRED(g_st_PlaneDpadNameTimer, 4000)
								DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE)
								SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE)
							ELSE
								IF GET_MP_BOOL_PLAYER_STAT(MPPLY_TOGGLE_OFF_BIG_RADAR) = FALSE
									DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(TRUE)
									SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(TRUE)
								ENDIF
							ENDIF
							CONTROL_RADAR_ZOOM_FREEMODE()
							SET_BIGMAP_ACTIVE(TRUE, FALSE) //SET_BIGMAP_ACTIVE(TRUE, TRUE)
							g_PlayerBlipsData.bBigMapIsActive = TRUE
						BREAK
					ENDSWITCH
				ELSE
					#IF IS_DEBUG_BUILD
					IF g_b_DisplayPrintsForRadarMap
					NET_NL()NET_PRINT("RUN_RADAR_MAP: CAN_DISPLAY_DPADDOWN_PARTS = FALSE ")
					ENDIF
					#ENDIF
					RESET_CONTROL_MAP_ZOOM()
					SET_BIGMAP_ACTIVE(FALSE, FALSE)
					RESET_CONTROL_MAP_ZOOM()
					g_PlayerBlipsData.bBigMapIsActive = FALSE
				ENDIF
			ENDIF 
		ELSE
			RESET_CONTROL_MAP_ZOOM()
			SET_BIGMAP_ACTIVE(FALSE, FALSE)
			RESET_CONTROL_MAP_ZOOM()
			g_PlayerBlipsData.bBigMapIsActive = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC RUN_CASH_DISPLAYS(DPADDOWN_ACTIVATION& aState_LastFrame )
	
	IF aState_LastFrame <> GET_DPADDOWN_ACTIVATION_STATE() 
		SWITCH GET_DPADDOWN_ACTIVATION_STATE() 
			CASE DPADDOWN_NONE
				
				IF NOT HAS_NET_TIMER_STARTED(st_ScriptTimers_BIG_MESSAGE_CashDisplay)
					REMOVE_MULTIPLAYER_WALLET_CASH()
					REMOVE_MULTIPLAYER_BANK_CASH()
					NET_NL()NET_PRINT("[FAKECASH]RUN_CASH_DISPLAYS: REMOVE DISPLAY ")
				ENDIF
			BREAK
			
			CASE DPADDOWN_FIRST
			CASE DPADDOWN_SECOND
				IF NOT HAS_NET_TIMER_STARTED(st_ScriptTimers_BIG_MESSAGE_CashDisplay)
					NET_NL()NET_PRINT("[FAKECASH]RUN_CASH_DISPLAYS: ACTIVATE DISPLAY ")
					SET_MULTIPLAYER_WALLET_CASH()
					SET_MULTIPLAYER_BANK_CASH()
				ENDIF
			BREAK
			
		ENDSWITCH
		aState_LastFrame = GET_DPADDOWN_ACTIVATION_STATE() 
	ENDIF
	
		
	


ENDPROC
				
PROC RUN_PLAYER_VEHICLE_ICON()
	IF MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState != DPADDOWN_NONE
		IF IS_SCRIPT_HUD_DISABLED(HUDPART_VEHICLE) = FALSE
			IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PERSONAL_VEHICLE_ID())		
						DRAW_SPRITE_ON_OBJECTIVE_VEHICLE_THIS_FRAME(PERSONAL_VEHICLE_ID())
					ENDIF 
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD

PROC SETUP_REWARD_WIDGETS()

	START_WIDGET_GROUP("REWARD BANNERS")
		ADD_WIDGET_BOOL("Turn on widgets", g_bDebugDisplayRewardBanner)
		ADD_WIDGET_INT_SLIDER("Which Team", g_iActivateRewardsTeam, 0, 4, 1)
		START_WIDGET_GROUP("AWARDS")
			ADD_WIDGET_BOOL("AWARD BRONZE", g_bActivateRewardBanner[0])
			ADD_WIDGET_BOOL("AWARD SILVER", g_bActivateRewardBanner[1])
			ADD_WIDGET_BOOL("AWARD GOLD", g_bActivateRewardBanner[2])
			ADD_WIDGET_BOOL("AWARD PLATINUM", g_bActivateRewardBanner[3])
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("MEDALS")
			ADD_WIDGET_BOOL("MEDALS BRONZE", g_bActivateRewardBanner[4])
			ADD_WIDGET_BOOL("MEDALS SILVER", g_bActivateRewardBanner[5])
			ADD_WIDGET_BOOL("MEDALS GOLD", g_bActivateRewardBanner[6])
			ADD_WIDGET_BOOL("MEDALS PLATINUM", g_bActivateRewardBanner[7])
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("TATTOOS")
			ADD_WIDGET_BOOL("TATTOO LEVEL 1", g_bActivateRewardBanner[8])
			ADD_WIDGET_BOOL("TATTOO LEVEL 2", g_bActivateRewardBanner[9])
			ADD_WIDGET_BOOL("TATTOO LEVEL 3", g_bActivateRewardBanner[10])
			ADD_WIDGET_BOOL("TATTOO LEVEL 4", g_bActivateRewardBanner[11])
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("PATCHES")
			ADD_WIDGET_BOOL("PATCHE LEVEL 1", g_bActivateRewardBanner[12])	
			ADD_WIDGET_BOOL("PATCHE LEVEL 2", g_bActivateRewardBanner[13])	
			ADD_WIDGET_BOOL("PATCHE LEVEL 3", g_bActivateRewardBanner[14])	
			ADD_WIDGET_BOOL("PATCHE LEVEL 4", g_bActivateRewardBanner[15])	
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("KIT")
			ADD_WIDGET_BOOL("KIT", g_bActivateRewardBanner[16])		
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("ABILITY")
			ADD_WIDGET_BOOL("ABILITY FULL UNLOCK", g_bActivateRewardBanner[17])	
			ADD_WIDGET_BOOL("ABILITY FIRST STAT INCREASE", g_bActivateRewardBanner[18])	
			ADD_WIDGET_BOOL("ABILITY SECOND STAT INCREASE", g_bActivateRewardBanner[19])	
			ADD_WIDGET_BOOL("ABILITY THIRD STAT INCREASE", g_bActivateRewardBanner[20])	
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("CAR MOD")
			ADD_WIDGET_BOOL("CAR MOD", g_bActivateRewardBanner[21])		
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("CLOTHES")
			ADD_WIDGET_BOOL("CLOTHES", g_bActivateRewardBanner[22])		
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("HEALTH")
			ADD_WIDGET_BOOL("HEALTH STAT LEVEL 1", g_bActivateRewardBanner[23])	
			ADD_WIDGET_BOOL("HEALTH STAT LEVEL 2", g_bActivateRewardBanner[24])	
			ADD_WIDGET_BOOL("HEALTH STAT LEVEL 3", g_bActivateRewardBanner[25])	
			ADD_WIDGET_BOOL("HEALTH STAT LEVEL 4", g_bActivateRewardBanner[26])	
			ADD_WIDGET_BOOL("HEALTH STAT LEVEL 5", g_bActivateRewardBanner[27])	
			ADD_WIDGET_BOOL("HEALTH STAT LEVEL 6", g_bActivateRewardBanner[28])	
			ADD_WIDGET_BOOL("HEALTH STAT LEVEL 7", g_bActivateRewardBanner[29])	
			ADD_WIDGET_BOOL("HEALTH STAT LEVEL 8", g_bActivateRewardBanner[30])	
			ADD_WIDGET_BOOL("HEALTH STAT LEVEL 9", g_bActivateRewardBanner[31])	
			ADD_WIDGET_BOOL("HEALTH STAT LEVEL 10", g_bActivateRewardBanner[32])	
			ADD_WIDGET_BOOL("HEALTH FULL", g_bActivateRewardBanner[33])	
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("HEIST")
			ADD_WIDGET_BOOL("HEIST", g_bActivateRewardBanner[34])		
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("VEHICLES")
			ADD_WIDGET_BOOL("VEHICLES", g_bActivateRewardBanner[35])	
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("WEAPON")
			ADD_WIDGET_BOOL("WEAPON", g_bActivateRewardBanner[36])	
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("WEAPON ADDON")
			ADD_WIDGET_BOOL("WEAPON ADDON", g_bActivateRewardBanner[37])	
		STOP_WIDGET_GROUP()
		
	
	STOP_WIDGET_GROUP()


ENDPROC

#ENDIF


