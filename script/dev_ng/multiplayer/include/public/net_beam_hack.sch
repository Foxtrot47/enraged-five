USING "timer_public.sch"
USING "rage_builtins.sch"
USING "globals.sch"
USING "net_include.sch"
USING "freemode_header.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "selector_public.sch"
USING "timer_public.sch"
USING "net_beam_hack_support.sch"

CONST_INT ciBH_HACK_TIME 300000
TWEAK_INT ciBH_MAX_LIVES 2

CONST_FLOAT cfBH_SAME_WALL_HIT_TOLERANCE 0.0006
CONST_FLOAT cfBH_FLOAT_TOLERANCE 0.001
CONST_INT ciBH_END_DELAY 3000
CONST_INT ciBH_NODE_ROTATE_SPEED 10
CONST_FLOAT cfBH_NODE_SPEED_MOD 4.25
CONST_FLOAT cfBH_BG_WIDTH 1.2
CONST_FLOAT cfBH_BG_HEIGHT 0.675
CONST_FLOAT cfBH_BG_XPOS 0.5
CONST_FLOAT cfBH_BG_YPOS 0.5075
CONST_INT ciBH_NODE_FLASH_MIN 0
CONST_INT ciBH_NODE_FLASH_MAX 85
CONST_INT ciBH_BHSEL_NODE_FLASH_MIN 0
CONST_INT ciBH_BHSEL_NODE_FLASH_MAX 125
CONST_INT ciBH_BHTUT_NODE_FLASH_MIN 0
CONST_INT ciBH_BHTUT_NODE_FLASH_MAX 200
CONST_FLOAT cfBH_TEMP_INNER_WALL_SIZE 0.112
CONST_INT ciBH_FIREWALL_PENALTY_TIME 1500
CONST_FLOAT cfBH_SineSpeed 8.5
CONST_FLOAT cfBH_SineMod 0.001
CONST_FLOAT cfBH_CURSOR_SIZE 0.075
CONST_FLOAT cfBH_CURSOR_THICKNESS 0.0015
CONST_FLOAT cfBH_CURSOR_SINE_SPEED 0.5
CONST_FLOAT cfBH_CURSOR_SINE_MOD_MAX 0.015
CONST_FLOAT cfBH_CURSOR_SINE_MOD_MIN 0.001
CONST_INT ciBH_FIREWALL_FRAME_OFFSET 58
CONST_INT ciBH_FIREWALL_FRAME_MAX 80
CONST_INT ciBH_PACKET_FRAME_MAX 80
CONST_INT ciBH_PACKET_FRAME_EXPLODE 60
CONST_INT ciBH_CURSOR_FRAME_MAX 8
CONST_INT ciBH_BEAM_FX_MAX_FRAME 4
CONST_FLOAT cfBH_LivesPosX 1.09
CONST_FLOAT cfBH_TimePosX 0.852
CONST_FLOAT cfBH_TextPosY 0.086
CONST_FLOAT cfBH_LivesPosX2 1.090
CONST_FLOAT cfBH_TimePosX2 0.851
CONST_FLOAT cfBH_TextPosY2 0.061
CONST_FLOAT cfBH_UINumScale 0.693
CONST_FLOAT cfBH_UITextScale 0.401
CONST_INT ciBH_MAX_PROXY_SIDES 8
CONST_INT ciBH_PROXY_ANGLE 45
CONST_INT ciBH_PROXY_SWITCH_TIME 750
CONST_INT ciBH_FIREWALL_TIME 2000
CONST_INT ciBH_FINALPACKET_DELAY_TIME 1000
CONST_FLOAT cfBH_ROTNODE_SIZE 0.095
CONST_FLOAT cfBH_PACKET_SIZE 0.085
CONST_INT ciBH_FIREWALL_HELP_TEXT_TIME 10000
CONST_INT ciBH_ACCURACY_HELP_TEXT_TIME 7500
CONST_INT ciBH_PROXY_HELP_TEXT_TIME 7500
CONST_FLOAT cfBH_BOTTOM_SPEED -5.2
CONST_FLOAT cfBH_MIDDLE_SPEED 3.667
CONST_FLOAT cfBH_TOP_SPEED 5.76
CONST_INT ciBH_BOTTOM_TRIG 1
CONST_INT ciBH_MIDDLE_TRIG 1
CONST_INT ciBH_TOP_TRIG 0
CONST_FLOAT cfBH_BOTTOM_MIN 245.0
CONST_FLOAT cfBH_MIDDLE_MIN 250.0
CONST_FLOAT cfBH_TOP_MIN 255.0
CONST_FLOAT cfBH_BOTTOM_MAX 12.0
CONST_FLOAT cfBH_MIDDLE_MAX 13.5
CONST_FLOAT cfBH_TOP_MAX 50.0
CONST_INT ciBHCURSOR_RED 188
CONST_INT ciBHCURSOR_GREEN 255
CONST_INT ciBHCURSOR_BLUE 255
CONST_FLOAT cfBH_NODEVIBRADIUS 0.019
CONST_INT ciBH_PROXY_CANCEL_LOOP_COUNT 3
CONST_INT ciBH_ROTATE_INTERVAL 166

ENUM BEAMHACK_TUTORIAL_STATE
	BHTUT_AIM_NODE,
	BHTUT_DESELECT_NODE,
	BHTUT_SELECT_NODE,
	BHTUT_AIM_NODE_2,
	BHTUT_AIM_NODE_3,
	BHTUT_FIREWALL_WARNING,
	BHTUT_REGULAR_TEXT
ENDENUM

CONST_INT ciBEAMHACKBS_UPDATE_BEAM			0
CONST_INT ciBEAMHACKBS_HITTING_PROXY		1
CONST_INT ciBEAMHACKBS_PLAYED_PROXY_HELP	2

ENUM BEAM_HACK_STATE
	BEAM_HACK_INIT = 0,
	BEAM_HACK_LOAD_LEVEL,
	BEAM_HACK_PLAY,
	BEAM_HACK_PASS,
	BEAM_HACK_FAIL,
	BEAM_HACK_CLEANUP,
	BEAM_HACK_EDITOR
ENDENUM

STRUCT BEAM_HACK_GAMEPLAY_DATA
	INT iTimeDuration = ciBH_HACK_TIME
	SCRIPT_TIMER tdHackTime
	INT iLives
	INT iLevel = 0
	INT iPacketsRemaining = ciBH_MAX_PACKETS
	INT iFirewallBS = 0
	INT iPacketBS = 0
	INT iPacketFrame[ciBH_MAX_PACKETS]
	INT iFirewallFrame[ciBH_MAX_FIREWALLS]
	INT iFirewallTime[ciBH_MAX_FIREWALLS]
	INT iBeamLength = 0
	INT iSelectedNode = 0
	VECTOR_2D vCursor
	INT iBS = 0
	INT iPacketHit = -1
	INT iFirewallHit = -1
	FLOAT fCursorSize
	INT iCursorNode = -1
	INT iProxyAngle = 0
	SCRIPT_TIMER tdFireWallTimer
	INT iFrameTimeCounter
	INT iUpdateFrames
	INT iCursorFrame
	INT iBeamFXFrame = 1
	SCRIPT_TIMER tdFinalPacketDelayTimer
	INT iBeamLength_Cached = 0
	//Audio
	INT sBeamHack_BackgroundLoopSndID = -1
	INT sBeamHack_CursorLoopSndID = -1
	INT sBeamHack_BlueChargeLoopSndID = -1
	INT sBeamHack_RedChargeLoopSndID = -1
	INT sBeamHack_RotateMirrorLoopSndID = -1
	BEAMHACK_TUTORIAL_STATE eTutStage = BHTUT_AIM_NODE
	SCRIPT_TIMER tdTutFirewallHelp
	SCRIPT_TIMER tdTutAccuracyHelp
	SCRIPT_TIMER tdProxyHelp
	SCRIPT_TIMER tdRotateTimer
	FLOAT fDesiredAngle
	SCRIPT_TIMER tdBeamEndDelay
	RGBA_COLOUR_STRUCT rgbaNodeRotate
	RGBA_COLOUR_STRUCT rgbaNodeRotateSelected
	RGBA_COLOUR_STRUCT rgbaNodeStatic
	RGBA_COLOUR_STRUCT rgbaInnerWall
	RGBA_COLOUR_STRUCT rgbaOuterWall
	RGBA_COLOUR_STRUCT rgbaPacket
	RGBA_COLOUR_STRUCT rgbaFireWall
	RGBA_COLOUR_STRUCT rgbaProxy
	RGBA_COLOUR_STRUCT rgbaBeam
	RGBA_COLOUR_STRUCT rgbaBeamStart
	RGBA_COLOUR_STRUCT rgbaSprite
	RGBA_COLOUR_STRUCT rgbaNodeCursor
	BEAM_HACK_STATE eBeamHackState = BEAM_HACK_INIT
	INT iHacksComplete = 0
ENDSTRUCT

BEAM_HACK_GAMEPLAY_DATA sBeamHackGameplayData

ENUM SHAPE_TYPES
	ST_INNER_WALL,
	ST_NODE,
	ST_ROTATING_NODE,
	ST_PACKET,
	ST_BEAM,
	ST_FIREWALL,
	ST_PROXY,
	ST_MAX_SHAPES
ENDENUM

PROC BEAMHACK_END_LOOPING_SOUNDS()
	IF sBeamHackGameplayData.sBeamHack_BackgroundLoopSndID >= 0
		IF NOT HAS_SOUND_FINISHED(sBeamHackGameplayData.sBeamHack_BackgroundLoopSndID)
			STOP_SOUND(sBeamHackGameplayData.sBeamHack_BackgroundLoopSndID)
		ENDIF
		RELEASE_SOUND_ID(sBeamHackGameplayData.sBeamHack_BackgroundLoopSndID)
		sBeamHackGameplayData.sBeamHack_BackgroundLoopSndID = -1
		PRINTLN("[BeamHack] Stopping background sound now")
	ENDIF
	
	IF sBeamHackGameplayData.sBeamHack_BlueChargeLoopSndID >= 0
		IF NOT HAS_SOUND_FINISHED(sBeamHackGameplayData.sBeamHack_BlueChargeLoopSndID)
			STOP_SOUND(sBeamHackGameplayData.sBeamHack_BlueChargeLoopSndID)
		ENDIF
		RELEASE_SOUND_ID(sBeamHackGameplayData.sBeamHack_BlueChargeLoopSndID)
		sBeamHackGameplayData.sBeamHack_BlueChargeLoopSndID = -1
		PRINTLN("[BeamHack] Stopping sBeamHack_BlueChargeLoopSndID now")
	ENDIF
	
	IF sBeamHackGameplayData.sBeamHack_RedChargeLoopSndID >= 0
		IF NOT HAS_SOUND_FINISHED(sBeamHackGameplayData.sBeamHack_RedChargeLoopSndID)
			STOP_SOUND(sBeamHackGameplayData.sBeamHack_RedChargeLoopSndID)
		ENDIF
		RELEASE_SOUND_ID(sBeamHackGameplayData.sBeamHack_RedChargeLoopSndID)
		sBeamHackGameplayData.sBeamHack_RedChargeLoopSndID = -1
		PRINTLN("[BeamHack] Stopping sBeamHack_RedChargeLoopSndID now")
	ENDIF
	
	IF sBeamHackGameplayData.sBeamHack_CursorLoopSndID >= 0
		IF NOT HAS_SOUND_FINISHED(sBeamHackGameplayData.sBeamHack_CursorLoopSndID)
			STOP_SOUND(sBeamHackGameplayData.sBeamHack_CursorLoopSndID)
		ENDIF
		RELEASE_SOUND_ID(sBeamHackGameplayData.sBeamHack_CursorLoopSndID)
		sBeamHackGameplayData.sBeamHack_CursorLoopSndID = -1
		PRINTLN("[BeamHack] Stopping sBeamHack_CursorLoopSndID now")
	ENDIF
	
	IF sBeamHackGameplayData.sBeamHack_RotateMirrorLoopSndID >= 0
		IF NOT HAS_SOUND_FINISHED(sBeamHackGameplayData.sBeamHack_RotateMirrorLoopSndID)
			STOP_SOUND(sBeamHackGameplayData.sBeamHack_RotateMirrorLoopSndID)
		ENDIF
		RELEASE_SOUND_ID(sBeamHackGameplayData.sBeamHack_RotateMirrorLoopSndID)
		sBeamHackGameplayData.sBeamHack_RotateMirrorLoopSndID = -1
		PRINTLN("[BeamHack] Stopping sBeamHack_RotateMirrorLoopSndID now")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_XM17_Silo_Laser_Hack_Minigame_Scene")
		STOP_AUDIO_SCENE("DLC_XM17_Silo_Laser_Hack_Minigame_Scene")
		PRINTLN("[BeamHack] Stopping DLC_XM17_Silo_Laser_Hack_Minigame_Scene now")
	ENDIF
ENDPROC

PROC RESET_BEAM_HACK_GAMEPLAY_DATA(BOOL bForNextLevel = FALSE)
	IF !bForNextLevel
		sBeamHackGameplayData.iLives = ciBH_MAX_LIVES
		REINIT_NET_TIMER(sBeamHackGameplayData.tdHackTime)
		sBeamHackGameplayData.iHacksComplete = 0
		BEAMHACK_END_LOOPING_SOUNDS()
	ENDIF
	RESET_NET_TIMER(sBeamHackGameplayData.tdBeamEndDelay)
	sBeamHackGameplayData.iPacketsRemaining = sBeamLevel.iMaxPackets 
	INT i
	FOR i = 0 TO ciBH_MAX_PACKETS - 1
		sBeamHackGameplayData.iPacketFrame[i] = 0
	ENDFOR
	FOR i = 0 TO ciBH_MAX_FIREWALLS - 1
		sBeamHackGameplayData.iFirewallFrame[i] = 0
		sBeamHackGameplayData.iFirewallTime[i] = 0
	ENDFOR
	sBeamHackGameplayData.iFirewallBS = 0
	sBeamHackGameplayData.iPacketBS = 0
	sBeamHackGameplayData.iSelectedNode = 0
	sBeamHackGameplayData.fCursorSize = cfBH_CURSOR_SIZE * 1.175
	sBeamHackGameplayData.vCursor = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vNodes[0][0], sBeamLevel.vNodes[0][1]), 2.0)
	IF NOT IS_VECTOR_2D_ZERO(sBeamLevel.vNodes[0][0])
		sBeamHackGameplayData.vCursor = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vNodes[0][0], sBeamLevel.vNodes[0][1]), 2.0)
	ELSE
		sBeamHackGameplayData.vCursor = INIT_VECTOR_2D(APPLY_ASPECT_MOD_TO_X(cfSCREEN_CENTER), cfSCREEN_CENTER)
	ENDIF
	sBeamHackGameplayData.iBeamLength = 0
	sBeamHackGameplayData.iBS = 0
	SET_BIT(sBeamHackGameplayData.iBS, ciBEAMHACKBS_UPDATE_BEAM)
	sBeamHackGameplayData.iPacketHit = -1
	sBeamHackGameplayData.iFirewallHit = -1
	sBeamHackGameplayData.iProxyAngle = 0
	REINIT_NET_TIMER(sBeamHackGameplayData.tdFireWallTimer)
	sBeamHackGameplayData.iFrameTimeCounter = 0
	sBeamHackGameplayData.iUpdateFrames = 0
	sBeamHackGameplayData.iCursorFrame = 0
	sBeamHackGameplayData.iBeamFXFrame = 1
	sBeamHackGameplayData.eTutStage = BHTUT_AIM_NODE
	sBeamHackGameplayData.fDesiredAngle = 0.0
ENDPROC

//#######################################################################
//##########################DEBUG FUNCTIONS##############################
//#######################################################################

#IF IS_DEBUG_BUILD

//Debug Variables
STRUCT BEAM_HACK_EDITOR_STRUCT
	INT iSelectedShape = 0
	SHAPE_TYPES eShapeType = ST_INNER_WALL
ENDSTRUCT

STRUCT BEAM_HACK_DEBUG_STRUCT
	BEAM_HACK_EDITOR_STRUCT sBeamHackEditorData
	BOOL bDebugCreatedWidgets = FALSE
	BOOL bDebugDrawDebugLines = FALSE
	BOOL bDebugDrawGrid = FALSE
	BOOL bLoadLevel
	INT iLevelToLoad = 0
	WIDGET_GROUP_ID widgetBeamMinigame
ENDSTRUCT

BEAM_HACK_DEBUG_STRUCT sBeamHackDebug

PROC DRAW_PLAY_AREA_DEBUG()
	//Black background
	DRAW_RECT(cfSCREEN_CENTER, cfSCREEN_CENTER, 1, 1, 0,0,0,255)
ENDPROC

PROC DRAW_BEAMHACK_DEBUG_SCREEN_GRID()
	INT i, j
	VECTOR_2D v2dW0, v2dW1
	VECTOR_2D v2dH0, v2dH1
	FOR i = 0 TO 9
		FOR j = 0 TO 16
			v2dW0.x = sBeamLevel.vOuterWalls[1].x + (cfGRID_SIZE * j)
			v2dW0.y = sBeamLevel.vOuterWalls[1].y 
			v2dW1.x = v2dW0.x
			v2dW1.y = sBeamLevel.vOuterWalls[3].y
			DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(v2dW0, v2dW1, sBeamHackGameplayData.rgbaOuterWall.iR, sBeamHackGameplayData.rgbaOuterWall.iG, sBeamHackGameplayData.rgbaOuterWall.iB, sBeamHackGameplayData.rgbaOuterWall.iA)
		ENDFOR
		v2dH0.x = sBeamLevel.vOuterWalls[1].x
		v2dH0.y = sBeamLevel.vOuterWalls[1].y + (cfGRID_SIZE * i)
		v2dH1.x = sBeamLevel.vOuterWalls[2].x
		v2dH1.y = v2dH0.y
		DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(v2dH0, v2dH1, sBeamHackGameplayData.rgbaOuterWall.iR, sBeamHackGameplayData.rgbaOuterWall.iG, sBeamHackGameplayData.rgbaOuterWall.iB, sBeamHackGameplayData.rgbaOuterWall.iA)
	ENDFOR
ENDPROC

PROC DRAW_LEVEL_DEBUG_LINES()
	INT i,j
	
	FOR i = 1 TO sBeamHackGameplayData.iBeamLength
		DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(vBeam[i - 1], vBeam[i], sBeamHackGameplayData.rgbaBeam.iR, sBeamHackGameplayData.rgbaBeam.iG, sBeamHackGameplayData.rgbaBeam.iB, sBeamHackGameplayData.rgbaBeam.iA)
	ENDFOR
	
	FOR i = 0 TO sBeamLevel.iMaxNodes - 1
		INT iR = sBeamHackGameplayData.rgbaNodeStatic.iR, iG = sBeamHackGameplayData.rgbaNodeStatic.iG, iB = sBeamHackGameplayData.rgbaNodeStatic.iB, iA = sBeamHackGameplayData.rgbaNodeStatic.iA
		IF i = sBeamHackGameplayData.iSelectedNode
			iR = sBeamHackGameplayData.rgbaNodeRotateSelected.iR
			iG = sBeamHackGameplayData.rgbaNodeRotateSelected.iG
			iB = sBeamHackGameplayData.rgbaNodeRotateSelected.iB
			iA = sBeamHackGameplayData.rgbaNodeRotateSelected.iA
		ELIF i < sBeamLevel.iMaxRotatingNodes
			iR = sBeamHackGameplayData.rgbaNodeRotate.iR
			iG = sBeamHackGameplayData.rgbaNodeRotate.iG
			iB = sBeamHackGameplayData.rgbaNodeRotate.iB
			iA = sBeamHackGameplayData.rgbaNodeRotate.iA
		ENDIF
		DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vNodes[i][0], sBeamLevel.vNodes[i][1], iR, iG, iB, iA)
		IF i >= sBeamLevel.iMaxRotatingNodes
			VECTOR_2D vNodeWallPoint = INIT_VECTOR_2D(sBeamLevel.vNodes[i][1].x, sBeamLevel.vNodes[i][0].y)
			DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vNodes[i][0], vNodeWallPoint, iR, iG, iB, iA)
			DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(vNodeWallPoint, sBeamLevel.vNodes[i][1], iR, iG, iB, iA)
		ENDIF
	ENDFOR
	
	FOR i = 0 TO sBeamLevel.iMaxPackets - 1
		IF sBeamHackGameplayData.iPacketFrame[i] <= ciBH_PACKET_FRAME_EXPLODE
			INT k
			FOR j = 0 TO ciBH_PACKET_POINTS - 1
				k = j + 1
				IF k >= ciBH_PACKET_POINTS
					k = 0
				ENDIF
				DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vPackets[i][j], sBeamLevel.vPackets[i][k], sBeamHackGameplayData.rgbaPacket.iR, ROUND((sBeamHackGameplayData.iPacketFrame[i] / TO_FLOAT(ciBH_PACKET_FRAME_EXPLODE)) * 255), sBeamHackGameplayData.rgbaPacket.iB, sBeamHackGameplayData.rgbaPacket.iA)
			ENDFOR
		ENDIF
	ENDFOR
	
	FOR i = 0 TO sBeamLevel.iMaxFirewalls - 1
		IF NOT IS_BIT_SET(sBeamHackGameplayData.iFirewallBS, i)
			INT k
			FOR j = 0 TO ciBH_FIREWALL_POINTS - 1
				k = j + 1
				IF k >= ciBH_FIREWALL_POINTS
					k = 0
				ENDIF
				DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vFirewalls[i][j], sBeamLevel.vFirewalls[i][k], sBeamHackGameplayData.rgbaFireWall.iR, sBeamHackGameplayData.rgbaFireWall.iG, sBeamHackGameplayData.rgbaFireWall.iB, sBeamHackGameplayData.rgbaFireWall.iA)
			ENDFOR
		ENDIF
	ENDFOR
	
	FOR i = 0 TO sBeamLevel.iMaxProxies - 1
		INT k
		FOR j = 0 TO ciBH_PROXY_POINTS - 1
			k = j + 1
			IF k >= ciBH_PROXY_POINTS
				k = 0
			ENDIF
			DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vProxies[i][j], sBeamLevel.vProxies[i][k], sBeamHackGameplayData.rgbaProxy.iR, sBeamHackGameplayData.rgbaProxy.iG, sBeamHackGameplayData.rgbaProxy.iB, sBeamHackGameplayData.rgbaProxy.iA)
		ENDFOR
	ENDFOR
	
	FOR i = 0 TO sBeamLevel.iMaxInnerWalls - 1
		INT k
		FOR j = 0 TO ciBH_INNER_WALL_POINTS - 1
			k = j + 1
			IF k >= ciBH_INNER_WALL_POINTS
				k = 0
			ENDIF
			DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vInnerWalls[i][j], sBeamLevel.vInnerWalls[i][k], sBeamHackGameplayData.rgbaInnerWall.iR, sBeamHackGameplayData.rgbaInnerWall.iG, sBeamHackGameplayData.rgbaInnerWall.iB, sBeamHackGameplayData.rgbaInnerWall.iA)
		ENDFOR
	ENDFOR
	
	FOR i = 0 TO ciBH_MAX_OUTER_WALLS - 1
		j = i + 1
		IF j >= ciBH_MAX_OUTER_WALLS
			j = 0
		ENDIF
		DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vOuterWalls[i], sBeamLevel.vOuterWalls[j], sBeamHackGameplayData.rgbaOuterWall.iR, sBeamHackGameplayData.rgbaOuterWall.iG, sBeamHackGameplayData.rgbaOuterWall.iB, sBeamHackGameplayData.rgbaOuterWall.iA)
	ENDFOR
	
	//Laser start
	DRAW_RECT(APPLY_ASPECT_MOD_TO_X(vBeam[0].x), vBeam[0].y, 0.01 / fAspectRatio, 0.01, sBeamHackGameplayData.rgbaBeamStart.iR, sBeamHackGameplayData.rgbaBeamStart.iG, sBeamHackGameplayData.rgbaBeamStart.iB, sBeamHackGameplayData.rgbaBeamStart.iA)
ENDPROC

PROC DRAW_CURSOR_POSITION()
	FLOAT fXCursor, fYCursor
	fXCursor = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
	fYCursor = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
	FLOAT fX = 0.5 - ((0.5 - fXCursor) * fAspectRatio)
	TEXT_LABEL_63 tl23 = ""
	
	tl23 += FLOOR(fXCursor)
	tl23 += "."
	tl23 += ROUND((fXCursor - FLOOR(fXCursor)) * 1000)
	tl23 += " ("
	tl23 += FLOOR(fX)
	tl23 += "."
	tl23 += ROUND((fX - FLOOR(fX)) * 1000)
	tl23 += ") , "
	tl23 += FLOOR(fYCursor)
	tl23 += "."
	tl23 += ROUND((fYCursor - FLOOR(fYCursor)) * 1000)
	
	DRAW_DEBUG_TEXT_2D(tl23, <<fXCursor, fYCursor - 0.01, 0>>, 255, 255, 255, 255)
ENDPROC

PROC SET_BEAM_HACK_PARENT_WIDGET_GROUP(WIDGET_GROUP_ID parentWidgetGroup)
	sBeamHackDebug.widgetBeamMinigame = parentWidgetGroup
ENDPROC

PROC PROCESS_BEAM_HACK_WIDGETS()
	IF NOT sBeamHackDebug.bDebugCreatedWidgets
		SET_CURRENT_WIDGET_GROUP(sBeamHackDebug.widgetBeamMinigame)
		START_WIDGET_GROUP("     BEAM HACK")
			ADD_WIDGET_BOOL("sBeamHackDebug.bDebugDrawDebugLines", sBeamHackDebug.bDebugDrawDebugLines)
			ADD_WIDGET_BOOL("sBeamHackDebug.bDebugDrawGrid", sBeamHackDebug.bDebugDrawGrid)
			ADD_WIDGET_INT_SLIDER("ciBH_MAX_LIVES", ciBH_MAX_LIVES, 0, 100, 1)
			ADD_WIDGET_INT_SLIDER("sBeamHackDebug.iLevelToLoad", sBeamHackDebug.iLevelToLoad, 0, ciBH_MAX_LEVELS - 1, 1)
			ADD_WIDGET_BOOL("sBeamHackDebug.bLoadLevel", sBeamHackDebug.bLoadLevel)
		STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(sBeamHackDebug.widgetBeamMinigame)
		sBeamHackDebug.bDebugCreatedWidgets = TRUE
	ENDIF
ENDPROC

FUNC STRING GET_STRING_FROM_BEAM_HACK_STATE(BEAM_HACK_STATE eState)
	SWITCH eState
		CASE BEAM_HACK_INIT			RETURN "BEAM_HACK_INIT"
		CASE BEAM_HACK_LOAD_LEVEL	RETURN "BEAM_HACK_LOAD_LEVEL"
		CASE BEAM_HACK_PLAY			RETURN "BEAM_HACK_PLAY"
		CASE BEAM_HACK_PASS			RETURN "BEAM_HACK_PASS"
		CASE BEAM_HACK_FAIL			RETURN "BEAM_HACK_FAIL"
		CASE BEAM_HACK_CLEANUP		RETURN "BEAM_HACK_CLEANUP"
	ENDSWITCH		
	RETURN "GET_STRING_FROM_BEAM_HACK_STATE - No Such State"
ENDFUNC

PROC DRAW_LEVEL_FOR_EDITOR()	
	TEXT_LABEL_15 tl15
	//Laser start
	IF sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_BEAM
		DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(vBeam[0], vBeam[1], sBeamHackGameplayData.rgbaBeam.iR, sBeamHackGameplayData.rgbaBeam.iG, sBeamHackGameplayData.rgbaBeam.iB, sBeamHackGameplayData.rgbaBeam.iA)
		DRAW_RECT(APPLY_ASPECT_MOD_TO_X(vBeam[0].x), vBeam[0].y, 0.01 / fAspectRatio, 0.01, sBeamHackGameplayData.rgbaBeamStart.iR, sBeamHackGameplayData.rgbaBeamStart.iG, sBeamHackGameplayData.rgbaBeamStart.iB, sBeamHackGameplayData.rgbaBeamStart.iA)
	ELSE
		DRAW_RECT(APPLY_ASPECT_MOD_TO_X(vBeam[0].x), vBeam[0].y, 0.01 / fAspectRatio, 0.01, sBeamHackGameplayData.rgbaBeamStart.iR, sBeamHackGameplayData.rgbaBeamStart.iG, sBeamHackGameplayData.rgbaBeamStart.iB, sBeamHackGameplayData.rgbaBeamStart.iA)
	ENDIF
	
	INT i,j, k
	FOR i = 0 TO ciBH_MAX_OUTER_WALLS - 1
		j = i + 1
		IF j >= ciBH_MAX_OUTER_WALLS
			j = 0
		ENDIF
		DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vOuterWalls[i], sBeamLevel.vOuterWalls[j], sBeamHackGameplayData.rgbaOuterWall.iR, sBeamHackGameplayData.rgbaOuterWall.iG, sBeamHackGameplayData.rgbaOuterWall.iB, sBeamHackGameplayData.rgbaOuterWall.iA)
	ENDFOR

	FOR i = 0 TO sBeamLevel.iMaxInnerWalls - 1
		FOR j = 0 TO ciBH_INNER_WALL_POINTS - 1
			IF NOT (sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_INNER_WALL
			AND sBeamHackDebug.sBeamHackEditorData.iSelectedShape = i)
				k = j + 1
				IF k >= ciBH_INNER_WALL_POINTS
					k = 0
				ENDIF
				DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vInnerWalls[i][j], sBeamLevel.vInnerWalls[i][k], sBeamHackGameplayData.rgbaInnerWall.iR, sBeamHackGameplayData.rgbaInnerWall.iG, sBeamHackGameplayData.rgbaInnerWall.iB, sBeamHackGameplayData.rgbaInnerWall.iA)
			ENDIF
		ENDFOR
		tl15 = GET_STRING_FROM_INT(i)
		VECTOR_2D vCentre = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vInnerWalls[i][0], sBeamLevel.vInnerWalls[i][2]), 2.0)
		DRAW_DEBUG_TEXT_2D(tl15, <<APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, 0>>, sBeamHackGameplayData.rgbaInnerWall.iR, sBeamHackGameplayData.rgbaInnerWall.iG, sBeamHackGameplayData.rgbaInnerWall.iB, sBeamHackGameplayData.rgbaInnerWall.iA)
	ENDFOR
	IF sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_INNER_WALL
		FOR j = 0 TO ciBH_INNER_WALL_POINTS - 1
			k = j + 1
			IF k >= ciBH_INNER_WALL_POINTS
				k = 0
			ENDIF
			INT iR = 255, iG = 0, iB = 255, iA = 255
			DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][j], sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][k], iR, iG, iB, iA)
		ENDFOR
	ENDIF
	
	FOR i = 0 TO sBeamLevel.iMaxRotatingNodes - 1
		IF NOT (sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_ROTATING_NODE
		AND sBeamHackDebug.sBeamHackEditorData.iSelectedShape = i)
			DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vNodes[i][0], sBeamLevel.vNodes[i][1], sBeamHackGameplayData.rgbaNodeRotate.iR, sBeamHackGameplayData.rgbaNodeRotate.iG, sBeamHackGameplayData.rgbaNodeRotate.iB, sBeamHackGameplayData.rgbaNodeRotate.iA)
		ENDIF
		tl15 = GET_STRING_FROM_INT(i)
		VECTOR_2D vCentre = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vNodes[i][0], sBeamLevel.vNodes[i][1]), 2.0)
		DRAW_DEBUG_TEXT_2D(tl15, <<APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, 0>>, sBeamHackGameplayData.rgbaNodeRotate.iR, sBeamHackGameplayData.rgbaNodeRotate.iG, sBeamHackGameplayData.rgbaNodeRotate.iB, sBeamHackGameplayData.rgbaNodeRotate.iA)
	ENDFOR
	IF sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_ROTATING_NODE
		INT iR = 255, iG = 0, iB = 255, iA = 255
		DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], iR, iG, iB, iA)
	ENDIF
	
	FOR i = sBeamLevel.iMaxRotatingNodes TO sBeamLevel.iMaxNodes - 1
		IF NOT (sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_NODE
		AND sBeamHackDebug.sBeamHackEditorData.iSelectedShape + sBeamLevel.iMaxRotatingNodes = i)
			DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vNodes[i][0], sBeamLevel.vNodes[i][1], sBeamHackGameplayData.rgbaNodeStatic.iR, sBeamHackGameplayData.rgbaNodeStatic.iG, sBeamHackGameplayData.rgbaNodeStatic.iB, sBeamHackGameplayData.rgbaNodeStatic.iA)
			VECTOR_2D vNodeWallPoint = INIT_VECTOR_2D(sBeamLevel.vNodes[i][1].x, sBeamLevel.vNodes[i][0].y)
			DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vNodes[i][0], vNodeWallPoint, sBeamHackGameplayData.rgbaNodeStatic.iR, sBeamHackGameplayData.rgbaNodeStatic.iG, sBeamHackGameplayData.rgbaNodeStatic.iB, sBeamHackGameplayData.rgbaNodeStatic.iA)
			DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(vNodeWallPoint, sBeamLevel.vNodes[i][1], sBeamHackGameplayData.rgbaNodeStatic.iR, sBeamHackGameplayData.rgbaNodeStatic.iG, sBeamHackGameplayData.rgbaNodeStatic.iB, sBeamHackGameplayData.rgbaNodeStatic.iA)
		ENDIF
		tl15 = GET_STRING_FROM_INT(i)
		VECTOR_2D vCentre = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vNodes[i][0], sBeamLevel.vNodes[i][1]), 2.0)
		DRAW_DEBUG_TEXT_2D(tl15, <<APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, 0>>, sBeamHackGameplayData.rgbaNodeStatic.iR, sBeamHackGameplayData.rgbaNodeStatic.iG, sBeamHackGameplayData.rgbaNodeStatic.iB, sBeamHackGameplayData.rgbaNodeStatic.iA)
	ENDFOR
	IF sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_NODE
		INT iR = 255, iG = 0, iB = 255, iA = 255
		DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape + sBeamLevel.iMaxRotatingNodes][0], sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape + sBeamLevel.iMaxRotatingNodes][1], iR, iG, iB, iA)
		VECTOR_2D vNodeWallPoint = INIT_VECTOR_2D(sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape + sBeamLevel.iMaxRotatingNodes][1].x, sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape + sBeamLevel.iMaxRotatingNodes][0].y)
		DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape + sBeamLevel.iMaxRotatingNodes][0], vNodeWallPoint, iR, iG, iB, iA)
		DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(vNodeWallPoint, sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape + sBeamLevel.iMaxRotatingNodes][1], iR, iG, iB, iA)
	ENDIF
	
	FOR i = 0 TO sBeamLevel.iMaxPackets - 1
		IF NOT (sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_PACKET
		AND sBeamHackDebug.sBeamHackEditorData.iSelectedShape = i)
			FOR j = 0 TO ciBH_PACKET_POINTS - 1
				k = j + 1
				IF k >= ciBH_PACKET_POINTS
					k = 0
				ENDIF
				DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vPackets[i][j], sBeamLevel.vPackets[i][k], sBeamHackGameplayData.rgbaPacket.iR, sBeamHackGameplayData.rgbaPacket.iG, sBeamHackGameplayData.rgbaPacket.iB, sBeamHackGameplayData.rgbaPacket.iA)
			ENDFOR
		ENDIF
		tl15 = GET_STRING_FROM_INT(i)
		VECTOR_2D vCentre = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vPackets[i][0], sBeamLevel.vPackets[i][2]), 2.0)
		DRAW_DEBUG_TEXT_2D(tl15, <<APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, 0>>, sBeamHackGameplayData.rgbaPacket.iR, sBeamHackGameplayData.rgbaPacket.iG, sBeamHackGameplayData.rgbaPacket.iB, sBeamHackGameplayData.rgbaPacket.iA)
	ENDFOR
	IF sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_PACKET
		FOR j = 0 TO ciBH_PACKET_POINTS - 1
			k = j + 1
			IF k >= ciBH_PACKET_POINTS
				k = 0
			ENDIF
			INT iR = 255, iG = 0, iB = 255, iA = 255
			DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][j], sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][k], iR, iG, iB, iA)
		ENDFOR
	ENDIF
	
	FOR i = 0 TO sBeamLevel.iMaxFirewalls - 1
		IF NOT (sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_FIREWALL
		AND sBeamHackDebug.sBeamHackEditorData.iSelectedShape = i)
			FOR j = 0 TO ciBH_FIREWALL_POINTS - 1
				k = j + 1
				IF k >= ciBH_FIREWALL_POINTS
					k = 0
				ENDIF
				DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vFirewalls[i][j], sBeamLevel.vFirewalls[i][k], sBeamHackGameplayData.rgbaFireWall.iR, sBeamHackGameplayData.rgbaFireWall.iG, sBeamHackGameplayData.rgbaFireWall.iB, sBeamHackGameplayData.rgbaFireWall.iA)
			ENDFOR
		ENDIF
		tl15 = GET_STRING_FROM_INT(i)
		VECTOR_2D vCentre = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vFirewalls[i][0], sBeamLevel.vFirewalls[i][2]), 2.0)
		DRAW_DEBUG_TEXT_2D(tl15, <<APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, 0>>, sBeamHackGameplayData.rgbaFireWall.iR, sBeamHackGameplayData.rgbaFireWall.iG, sBeamHackGameplayData.rgbaFireWall.iB, sBeamHackGameplayData.rgbaFireWall.iA)
	ENDFOR
	IF sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_FIREWALL
		FOR j = 0 TO ciBH_FIREWALL_POINTS - 1
			k = j + 1
			IF k >= ciBH_FIREWALL_POINTS
				k = 0
			ENDIF
			INT iR = 255, iG = 0, iB = 255, iA = 255
			DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][j], sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][k], iR, iG, iB, iA)
		ENDFOR
	ENDIF
	
	FOR i = 0 TO sBeamLevel.iMaxProxies - 1
		IF NOT (sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_PROXY
		AND sBeamHackDebug.sBeamHackEditorData.iSelectedShape = i)
			FOR j = 0 TO ciBH_PROXY_POINTS - 1
				k = j + 1
				IF k >= ciBH_PROXY_POINTS
					k = 0
				ENDIF
				DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vProxies[i][j], sBeamLevel.vProxies[i][k], sBeamHackGameplayData.rgbaProxy.iR, sBeamHackGameplayData.rgbaProxy.iG, sBeamHackGameplayData.rgbaProxy.iB, sBeamHackGameplayData.rgbaProxy.iA)
			ENDFOR
		ENDIF
		tl15 = GET_STRING_FROM_INT(i)
		VECTOR_2D vCentre = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vProxies[i][0], sBeamLevel.vProxies[i][2]), 2.0)
		DRAW_DEBUG_TEXT_2D(tl15, <<APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, 0>>, sBeamHackGameplayData.rgbaProxy.iR, sBeamHackGameplayData.rgbaProxy.iG, sBeamHackGameplayData.rgbaProxy.iB, sBeamHackGameplayData.rgbaProxy.iA)
	ENDFOR
	IF sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_PROXY
		FOR j = 0 TO ciBH_PROXY_POINTS - 1
			k = j + 1
			IF k >= ciBH_PROXY_POINTS
				k = 0
			ENDIF
			INT iR = 255, iG = 0, iB = 255, iA = 255
			DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][j], sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][k], iR, iG, iB, iA)
		ENDFOR
	ENDIF
ENDPROC

PROC SHIFT_POINT(VECTOR_2D &p, FLOAT fMove, BOOL bVertical)
	IF bVertical
		p.y += fMove
	ELSE
		p.x += fMove
	ENDIF
ENDPROC

TYPEDEF PROC TRANSFORM_EDGE(VECTOR_2D &p0, VECTOR_2D &p1, FLOAT fMove, BOOL bVertical)

PROC SCALE_EDGE(VECTOR_2D &p0, VECTOR_2D &p1, FLOAT fMove, BOOL bVertical)
	SHIFT_POINT(p0, -fMove, bVertical)
	SHIFT_POINT(p1, fMove, bVertical)
ENDPROC

PROC MOVE_EDGE(VECTOR_2D &p0, VECTOR_2D &p1, FLOAT fMove, BOOL bVertical)
	SHIFT_POINT(p0, fMove, bVertical)
	SHIFT_POINT(p1, fMove, bVertical)
ENDPROC

PROC FLIP_POINTS(VECTOR_2D &p0, VECTOR_2D &p1, FLOAT fMove, BOOL bVertical)
	UNUSED_PARAMETER(fMove)
	IF bVertical
		FLOAT fTemp = p0.y
		p0.y = p1.y
		p1.y = fTemp
	ELSE
		FLOAT fTemp = p0.x
		p0.x = p1.x
		p1.x = fTemp
	ENDIF
ENDPROC

FUNC INT GET_ACTUAL_MAX_FROM_SHAPE_TYPE(SHAPE_TYPES eShapeType)
	SWITCH eShapeType
		CASE ST_INNER_WALL 		RETURN ciBH_MAX_INNER_WALL_RECTS
		CASE ST_NODE			RETURN ciBH_MAX_NODES - ciBH_MAX_ROTATING_NODES
		CASE ST_ROTATING_NODE	RETURN ciBH_MAX_ROTATING_NODES
		CASE ST_PACKET			RETURN ciBH_MAX_PACKETS
		CASE ST_FIREWALL		RETURN ciBH_MAX_FIREWALLS
		CASE ST_PROXY			RETURN ciBH_MAX_PROXIES
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC INT GET_CURRENT_MAX_FROM_SHAPE_TYPE(SHAPE_TYPES eShapeType)
	SWITCH eShapeType
		CASE ST_INNER_WALL 		RETURN sBeamLevel.iMaxInnerWalls
		CASE ST_NODE			RETURN sBeamLevel.iMaxNodes - sBeamLevel.iMaxRotatingNodes
		CASE ST_ROTATING_NODE	RETURN sBeamLevel.iMaxRotatingNodes
		CASE ST_PACKET			RETURN sBeamLevel.iMaxPackets
		CASE ST_FIREWALL		RETURN sBeamLevel.iMaxFirewalls
		CASE ST_PROXY			RETURN sBeamLevel.iMaxProxies
	ENDSWITCH
	RETURN 0
ENDFUNC

PROC PROCESS_LEVEL_SELECT()
	IF sBeamHackDebug.bLoadLevel
		IF sBeamHackDebug.iLevelToLoad != sBeamHackGameplayData.iLevel
			TEXT_LABEL_23 tlDict = GET_BEAMHACK_TEXTURE_DICT_FOR_LEVEL(sBeamHackDebug.iLevelToLoad)
			REQUEST_STREAMED_TEXTURE_DICT(tlDict)
			IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tlDict)
				PRINTLN("[JS][BeamHack] PROCESS_LEVEL_SELECT Not Loaded ", tlDict)
				EXIT
			ENDIF
			tlDict = GET_BEAMHACK_TEXTURE_DICT_FOR_LEVEL(sBeamHackGameplayData.iLevel)
			SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(tlDict)
		ENDIF
		PRINTLN("[JS][BeamHack] - UPDATE_BEAM_HACK_STATE - Loading ", sBeamHackDebug.iLevelToLoad, ". Dumping current screen to logs in case this was an accident")
		DUMP_LEVEL_TO_LOGS()
		RESET_BEAM_HACK_GAMEPLAY_DATA()
		PRINTLN("[JS][BeamHack] - PROCESS_BEAM_HACK_DEBUG - Going to BEAM_HACK_LOAD_LEVEL")
		sBeamHackGameplayData.eBeamHackState = BEAM_HACK_LOAD_LEVEL
		sBeamHackGameplayData.iLevel = sBeamHackDebug.iLevelToLoad
		sBeamHackDebug.bLoadLevel = FALSE
	ENDIF
ENDPROC

PROC PROCESS_BEAM_HACK_EDITOR()
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_N, KEYBOARD_MODIFIER_SHIFT, "")
		PRINTLN("[JS][BeamHack] - UPDATE_BEAM_HACK_STATE - SHIFT + N pressed. Dumping current screen to logs in case this was an accident")
		DUMP_LEVEL_TO_LOGS()
		LOAD_BLANK_LEVEL()
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_U, KEYBOARD_MODIFIER_SHIFT, "")
		IF sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_PACKET
			INT j = 0
			PRINTLN("[BeamHack] TAKING A PACKET DUMP - PAcket: ", sBeamHackDebug.sBeamHackEditorData.iSelectedShape)
			PRINTLN("")
			PRINTLN("")
			PRINTLN("//Packets")
			FOR j = 0 TO ciBH_PACKET_POINTS - 1
				PRINTLN("sBeamLevel.vPackets[",sBeamHackDebug.sBeamHackEditorData.iSelectedShape,"][",j,"] = INIT_VECTOR_2D(",sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][j].x,", ",sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][j].y,")")
			ENDFOR
			PRINTLN("")
		ENDIF
	ENDIF
	
	DRAW_CURSOR_POSITION()
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_X)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
	TRANSFORM_EDGE ptTransform = &MOVE_EDGE
	IF sBeamHackDebug.sBeamHackEditorData.eShapeType != ST_BEAM
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
			sBeamHackDebug.sBeamHackEditorData.iSelectedShape--
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
			sBeamHackDebug.sBeamHackEditorData.iSelectedShape++
		ENDIF
	ENDIF
	IF sBeamHackDebug.sBeamHackEditorData.iSelectedShape >= GET_CURRENT_MAX_FROM_SHAPE_TYPE(sBeamHackDebug.sBeamHackEditorData.eShapeType)
		sBeamHackDebug.sBeamHackEditorData.iSelectedShape = 0
	ELIF sBeamHackDebug.sBeamHackEditorData.iSelectedShape < 0 
		sBeamHackDebug.sBeamHackEditorData.iSelectedShape = GET_CURRENT_MAX_FROM_SHAPE_TYPE(sBeamHackDebug.sBeamHackEditorData.eShapeType) - 1
	ENDIF
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
		sBeamHackDebug.sBeamHackEditorData.eShapeType = INT_TO_ENUM(SHAPE_TYPES, ENUM_TO_INT(sBeamHackDebug.sBeamHackEditorData.eShapeType) + 1)
		sBeamHackDebug.sBeamHackEditorData.iSelectedShape = 0
	ENDIF
	IF sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_MAX_SHAPES
		sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_INNER_WALL
	ENDIF
	IF sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_INNER_WALL
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
			ptTransform = &SCALE_EDGE
		ENDIF
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			CALL ptTransform(sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], -cfGRID_HALF_SIZE, TRUE)
			CALL ptTransform(sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][3], sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][2], -cfGRID_HALF_SIZE, TRUE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			CALL ptTransform(sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], cfGRID_HALF_SIZE, TRUE)
			CALL ptTransform(sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][3], sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][2], cfGRID_HALF_SIZE, TRUE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			CALL ptTransform(sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][3], -cfGRID_HALF_SIZE, FALSE)
			CALL ptTransform(sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][2], -cfGRID_HALF_SIZE, FALSE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			CALL ptTransform(sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][3], cfGRID_HALF_SIZE, FALSE)
			CALL ptTransform(sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], sBeamLevel.vInnerWalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][2], cfGRID_HALF_SIZE, FALSE)
		ENDIF
	ELIF sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_NODE
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
			ptTransform = &FLIP_POINTS
		ENDIF
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			CALL ptTransform(sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape + sBeamLevel.iMaxRotatingNodes][0], sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape + sBeamLevel.iMaxRotatingNodes][1], -cfGRID_HALF_SIZE, TRUE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			CALL ptTransform(sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape + sBeamLevel.iMaxRotatingNodes][0], sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape + sBeamLevel.iMaxRotatingNodes][1], cfGRID_HALF_SIZE, TRUE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			CALL ptTransform(sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape + sBeamLevel.iMaxRotatingNodes][0], sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape + sBeamLevel.iMaxRotatingNodes][1], -cfGRID_HALF_SIZE, FALSE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			CALL ptTransform(sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape + sBeamLevel.iMaxRotatingNodes][0], sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape + sBeamLevel.iMaxRotatingNodes][1], cfGRID_HALF_SIZE, FALSE)
		ENDIF
	ELIF sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_ROTATING_NODE
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
			ptTransform = &FLIP_POINTS
		ENDIF
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			CALL ptTransform(sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], -cfGRID_HALF_SIZE, TRUE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			CALL ptTransform(sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], cfGRID_HALF_SIZE, TRUE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			CALL ptTransform(sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], -cfGRID_HALF_SIZE, FALSE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			CALL ptTransform(sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vNodes[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], cfGRID_HALF_SIZE, FALSE)
		ENDIF
	ELIF sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_PACKET
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			CALL ptTransform(sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], -cfGRID_HALF_SIZE, TRUE)
			CALL ptTransform(sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][3], sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][2], -cfGRID_HALF_SIZE, TRUE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			CALL ptTransform(sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], cfGRID_HALF_SIZE, TRUE)
			CALL ptTransform(sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][3], sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][2], cfGRID_HALF_SIZE, TRUE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			CALL ptTransform(sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][3], -cfGRID_HALF_SIZE, FALSE)
			CALL ptTransform(sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][2], -cfGRID_HALF_SIZE, FALSE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			CALL ptTransform(sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][3], cfGRID_HALF_SIZE, FALSE)
			CALL ptTransform(sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], sBeamLevel.vPackets[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][2], cfGRID_HALF_SIZE, FALSE)
		ENDIF
	ELIF sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_FIREWALL
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			CALL ptTransform(sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], -cfGRID_HALF_SIZE, TRUE)
			CALL ptTransform(sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][3], sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][2], -cfGRID_HALF_SIZE, TRUE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			CALL ptTransform(sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], cfGRID_HALF_SIZE, TRUE)
			CALL ptTransform(sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][3], sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][2], cfGRID_HALF_SIZE, TRUE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			CALL ptTransform(sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][3], -cfGRID_HALF_SIZE, FALSE)
			CALL ptTransform(sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][2], -cfGRID_HALF_SIZE, FALSE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			CALL ptTransform(sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][3], cfGRID_HALF_SIZE, FALSE)
			CALL ptTransform(sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], sBeamLevel.vFirewalls[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][2], cfGRID_HALF_SIZE, FALSE)
		ENDIF
	ELIF sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_PROXY
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			CALL ptTransform(sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], -cfGRID_HALF_SIZE, TRUE)
			CALL ptTransform(sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][3], sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][2], -cfGRID_HALF_SIZE, TRUE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			CALL ptTransform(sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], cfGRID_HALF_SIZE, TRUE)
			CALL ptTransform(sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][3], sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][2], cfGRID_HALF_SIZE, TRUE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			CALL ptTransform(sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][3], -cfGRID_HALF_SIZE, FALSE)
			CALL ptTransform(sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][2], -cfGRID_HALF_SIZE, FALSE)
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			CALL ptTransform(sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][0], sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][3], cfGRID_HALF_SIZE, FALSE)
			CALL ptTransform(sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][1], sBeamLevel.vProxies[sBeamHackDebug.sBeamHackEditorData.iSelectedShape][2], cfGRID_HALF_SIZE, FALSE)
		ENDIF
	ELIF sBeamHackDebug.sBeamHackEditorData.eShapeType = ST_BEAM
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
				vBeam[1] = INIT_VECTOR_2D(vBeam[0].x, -1.0)
			ELSE
				CALL ptTransform(vBeam[0], vBeam[1], -cfGRID_HALF_SIZE, TRUE)
			ENDIF
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
				vBeam[1] = INIT_VECTOR_2D(vBeam[0].x, 2.0)
			ELSE
				CALL ptTransform(vBeam[0], vBeam[1], cfGRID_HALF_SIZE, TRUE)
			ENDIF
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
				vBeam[1] = INIT_VECTOR_2D(-1.0, vBeam[0].y)
			ELSE
				CALL ptTransform(vBeam[0], vBeam[1], -cfGRID_HALF_SIZE, FALSE)
			ENDIF
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
				vBeam[1] = INIT_VECTOR_2D(2.0, vBeam[0].y)
			ELSE
				CALL ptTransform(vBeam[0], vBeam[1], cfGRID_HALF_SIZE, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_BEAM_HACK_DEBUG()
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	IF NOT sBeamHackDebug.bDebugCreatedWidgets
		PROCESS_BEAM_HACK_WIDGETS()
	ENDIF
	PROCESS_LEVEL_SELECT()
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_D, KEYBOARD_MODIFIER_SHIFT, "")
		DUMP_LEVEL_TO_LOGS()
	ENDIF
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_T, KEYBOARD_MODIFIER_SHIFT, "")
		sBeamHackDebug.bDebugDrawDebugLines = !sBeamHackDebug.bDebugDrawDebugLines
	ENDIF
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_E, KEYBOARD_MODIFIER_SHIFT, "")
		IF sBeamHackGameplayData.eBeamHackState = BEAM_HACK_PLAY
			PRINTLN("[JS][BeamHack] - PROCESS_BEAM_HACK_DEBUG - Going to BEAM_HACK_EDITOR")
			sBeamHackGameplayData.eBeamHackState = BEAM_HACK_EDITOR
		ELIF sBeamHackGameplayData.eBeamHackState = BEAM_HACK_EDITOR
			RESET_BEAM_HACK_GAMEPLAY_DATA()
			PRINTLN("[JS][BeamHack] - PROCESS_BEAM_HACK_DEBUG - Going to BEAM_HACK_PLAY")
			sBeamHackGameplayData.eBeamHackState = BEAM_HACK_PLAY
		ENDIF
	ENDIF
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_NONE, "")
		PRINTLN("[JS][BeamHack] - PROCESS_BEAM_HACK_DEBUG - J SKIP - Going to BEAM_HACK_PASS")
		sBeamHackGameplayData.eBeamHackState = BEAM_HACK_PASS
	ENDIF
ENDPROC

#ENDIF

FUNC BOOL IS_BEAMHACK_TUTORIAL()
	RETURN sBeamHackGameplayData.iLevel = 999
ENDFUNC

PROC BEAMHACK_PROCESS_LOOPING_SOUNDS()
	IF sBeamHackGameplayData.sBeamHack_BackgroundLoopSndID = -1
		sBeamHackGameplayData.sBeamHack_BackgroundLoopSndID = GET_SOUND_ID()
	ENDIF
	IF sBeamHackGameplayData.sBeamHack_CursorLoopSndID = -1
		sBeamHackGameplayData.sBeamHack_CursorLoopSndID = GET_SOUND_ID()
	ENDIF
	IF sBeamHackGameplayData.sBeamHack_BlueChargeLoopSndID = -1
		sBeamHackGameplayData.sBeamHack_BlueChargeLoopSndID = GET_SOUND_ID()
	ENDIF
	IF sBeamHackGameplayData.sBeamHack_RedChargeLoopSndID = -1
		sBeamHackGameplayData.sBeamHack_RedChargeLoopSndID = GET_SOUND_ID()
	ENDIF
	IF sBeamHackGameplayData.sBeamHack_RotateMirrorLoopSndID = -1
		sBeamHackGameplayData.sBeamHack_RotateMirrorLoopSndID = GET_SOUND_ID()
	ENDIF

	IF HAS_SOUND_FINISHED(sBeamHackGameplayData.sBeamHack_BackgroundLoopSndID)
		PLAY_SOUND_FRONTEND(sBeamHackGameplayData.sBeamHack_BackgroundLoopSndID, "Background", "dlc_xm_silo_laser_hack_sounds")
		PRINTLN("[BeamHack] Playing background sound now")
	ENDIF
	
	IF HAS_SOUND_FINISHED(sBeamHackGameplayData.sBeamHack_BlueChargeLoopSndID)
		PLAY_SOUND_FRONTEND(sBeamHackGameplayData.sBeamHack_BlueChargeLoopSndID, "Blue_Target_Charge", "dlc_xm_silo_laser_hack_sounds")
		PRINTLN("[BeamHack] Playing sBeamHack_BlueChargeLoopSndID now")
	ENDIF
	
	IF HAS_SOUND_FINISHED(sBeamHackGameplayData.sBeamHack_RedChargeLoopSndID)
		PLAY_SOUND_FRONTEND(sBeamHackGameplayData.sBeamHack_RedChargeLoopSndID, "Red_Target_Charge", "dlc_xm_silo_laser_hack_sounds")
		PRINTLN("[BeamHack] Playing sBeamHack_RedChargeLoopSndID now")
	ENDIF
	
	IF HAS_SOUND_FINISHED(sBeamHackGameplayData.sBeamHack_CursorLoopSndID)
		PLAY_SOUND_FRONTEND(sBeamHackGameplayData.sBeamHack_CursorLoopSndID, "Cursor_Move", "dlc_xm_silo_laser_hack_sounds")
		PRINTLN("[BeamHack] Playing sBeamHack_CursorLoopSndID now")
	ENDIF
	
	IF HAS_SOUND_FINISHED(sBeamHackGameplayData.sBeamHack_RotateMirrorLoopSndID)
		PLAY_SOUND_FRONTEND(sBeamHackGameplayData.sBeamHack_RotateMirrorLoopSndID, "Rotate_Mirror", "dlc_xm_silo_laser_hack_sounds")
		PRINTLN("[BeamHack] Playing sBeamHack_RotateMirrorLoopSndID now")
	ENDIF
	
	IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_XM17_Silo_Laser_Hack_Minigame_Scene")
		START_AUDIO_SCENE("DLC_XM17_Silo_Laser_Hack_Minigame_Scene")
	ENDIF
ENDPROC

PROC BEAMHACK_RESET_LOOPING_VALUES()
	SET_VARIABLE_ON_SOUND(sBeamHackGameplayData.sBeamHack_CursorLoopSndID, "moveSpeed", 0.0)
	SET_VARIABLE_ON_SOUND(sBeamHackGameplayData.sBeamHack_RotateMirrorLoopSndID, "moveSpeed", 0.0)
	
	SET_VARIABLE_ON_SOUND(sBeamHackGameplayData.sBeamHack_BlueChargeLoopSndID, "Control", 0.0)
	SET_VARIABLE_ON_SOUND(sBeamHackGameplayData.sBeamHack_RedChargeLoopSndID, "Control", 0.0)
ENDPROC

PROC DRAW_PLAY_AREA()
	//Background sprite
	DRAW_RECT_FROM_CORNER(0.0, 0.0, 1.0, 1.0, 0, 0, 0, 255)
	TEXT_LABEL_23 tlDict = GET_BEAMHACK_TEXTURE_DICT_FOR_LEVEL(sBeamHackGameplayData.iLevel)
	TEXT_LABEL_23 tlTx = GET_BEAMHACK_TEXTURE_BG_FOR_LEVEL(sBeamHackGameplayData.iLevel)
	DRAW_SPRITE(tlDict, tlTx, cfBH_BG_XPOS, cfBH_BG_YPOS, cfBH_BG_WIDTH / fAspectRatio, cfBH_BG_HEIGHT, 0.0, sBeamHackGameplayData.rgbaSprite.iR, sBeamHackGameplayData.rgbaSprite.iG, sBeamHackGameplayData.rgbaSprite.iB, sBeamHackGameplayData.rgbaSprite.iA)
ENDPROC

TYPEDEF FUNC FLOAT TRIG_FUNC(FLOAT f)

FUNC FLOAT BHSIN(FLOAT f)
	RETURN SIN(f)
ENDFUNC
FUNC FLOAT BHCOS(FLOAT f)
	RETURN COS(f)
ENDFUNC
FUNC FLOAT BHTAN(FLOAT f)
	RETURN TAN(f)
ENDFUNC

FUNC BOOL IS_GAME_PAUSED_DUE_TO_FIREWALL_HIT()
	IF sBeamHackGameplayData.iFirewallHit = -1
		RETURN FALSE
	ELIF sBeamHackGameplayData.iFirewallTime[sBeamHackGameplayData.iFirewallHit] < ciBH_FIREWALL_TIME
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC DRAW_BEAM()
	IF NOT IS_GAME_PAUSED_DUE_TO_FIREWALL_HIT()
		INT iR, iG, iB
		IF sBeamHackGameplayData.iFirewallHit = -1
		AND sBeamHackGameplayData.iLives >= 0
			iR = sBeamHackGameplayData.rgbaBeam.iR
			iG = sBeamHackGameplayData.rgbaBeam.iG
			iB = sBeamHackGameplayData.rgbaBeam.iB
		ELSE
			iR = 255
			iG = 0
			iB = 0
		ENDIF
		
		TRIG_FUNC fTrigTop
		TRIG_FUNC fTrigMiddle
		TRIG_FUNC fTrigBottom
//		IF ciBH_BOTTOM_TRIG = 0
//			fTrigBottom = &BHSIN
//		ELIF ciBH_BOTTOM_TRIG = 1
			fTrigBottom = &BHCOS
//		ELIF ciBH_BOTTOM_TRIG = 2
//			fTrigBottom = &BHTAN
//		ENDIF
//		IF ciBH_MIDDLE_TRIG = 0
//			fTrigMiddle = &BHSIN
//		ELIF ciBH_MIDDLE_TRIG = 1
			fTrigMiddle = &BHCOS
//		ELIF ciBH_MIDDLE_TRIG = 2
//			fTrigMiddle = &BHTAN
//		ENDIF
//		IF ciBH_TOP_TRIG = 0
			fTrigTop = &BHSIN
//		ELIF ciBH_TOP_TRIG = 1
//			fTrigTop = &BHCOS
//		ELIF ciBH_TOP_TRIG = 2
//			fTrigTop = &BHTAN
//		ENDIF
		
		FLOAT fMaxScaler = 1.0
		IF sBeamHackGameplayData.iPacketHit > -1
		OR sBeamHackGameplayData.iFirewallHit > -1
			fMaxScaler = 1.5
		ENDIF
		
		INT iABottom = ROUND((CALL fTrigBottom(TO_FLOAT(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sBeamHackGameplayData.tdHackTime.Timer))) * cfBH_BOTTOM_SPEED) * (cfBH_BOTTOM_MAX * fMaxScaler)) + cfBH_BOTTOM_MIN)
		INT iAMid = ROUND((CALL fTrigMiddle(TO_FLOAT(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sBeamHackGameplayData.tdHackTime.Timer))) * cfBH_MIDDLE_SPEED) * (cfBH_MIDDLE_MAX * fMaxScaler)) + cfBH_MIDDLE_MIN)
		INT iATop = ROUND((CALL fTrigTop(TO_FLOAT(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sBeamHackGameplayData.tdHackTime.Timer))) * cfBH_TOP_SPEED) * (cfBH_TOP_MAX * fMaxScaler)) + cfBH_TOP_MIN)

		INT i
		FOR i = 1 TO sBeamHackGameplayData.iBeamLength
			VECTOR_2D vCentre = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(vBeam[i - 1], vBeam[i]), 2.0)
			FLOAT fDist = VECTOR_2D_DIST(vBeam[i - 1], vBeam[i])
			FLOAT fRotation = ATAN2(vBeam[i].y - vBeam[i - 1].y, vBeam[i].x - vBeam[i - 1].x)
			DRAW_SPRITE_ARX("MPBeamHack", "Beam_Glow_Tapered", APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, 0.064 / fAspectRatio, fDist, fRotation + 90.0, iR, iG, iB, iABottom)
			DRAW_SPRITE_ARX("MPBeamHack", "beam_middle", APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, 0.064 / fAspectRatio, fDist, fRotation + 90.0, iR, iG, iB, iAMid)
			DRAW_SPRITE_ARX("MPBeamHack", "beam_top", APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, 0.064 / fAspectRatio, fDist, fRotation + 90.0, iR, iG, iB, iATop)
		ENDFOR
		IF sBeamHackGameplayData.iBeamLength > 0
			sBeamHackGameplayData.iBeamFXFrame += sBeamHackGameplayData.iUpdateFrames
			IF sBeamHackGameplayData.iBeamFXFrame > ciBH_BEAM_FX_MAX_FRAME
				sBeamHackGameplayData.iBeamFXFrame = 1
			ENDIF
			TEXT_LABEL_15 tl15 = "beam_spark_"
			tl15 += sBeamHackGameplayData.iBeamFXFrame
			VECTOR_2D vBeamDir = NORMALISE_VECTOR_2D(SUBTRACT_VECTOR_2D(vBeam[sBeamHackGameplayData.iBeamLength], vBeam[sBeamHackGameplayData.iBeamLength - 1]))
			VECTOR_2D vStartPoint = ADD_VECTOR_2D(vBeam[sBeamHackGameplayData.iBeamLength], (MULTIPLY_VECTOR_2D(vBeamDir, -0.016)))
			VECTOR_2D vCentreFX = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(vBeam[sBeamHackGameplayData.iBeamLength], vStartPoint), 2.0)
			FLOAT fRotation = ATAN2(vBeam[sBeamHackGameplayData.iBeamLength].y - vStartPoint.y, vBeam[sBeamHackGameplayData.iBeamLength].x - vStartPoint.x)
			DRAW_SPRITE_ARX("MPBeamHack", tl15, APPLY_ASPECT_MOD_TO_X(vCentreFX.x), vCentreFX.y, 0.064 / fAspectRatio, 0.064, fRotation + 90.0, iR, iG, iB, iABottom)
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_CURSOR()
	IF sBeamHackGameplayData.iSelectedNode = -1
		DRAW_SPRITE("MPBeamHack", "Cursor_0", APPLY_ASPECT_MOD_TO_X(sBeamHackGameplayData.vCursor.x), sBeamHackGameplayData.vCursor.y, sBeamHackGameplayData.fCursorSize / fAspectRatio, sBeamHackGameplayData.fCursorSize, 0.0, sBeamHackGameplayData.rgbaSprite.iR, sBeamHackGameplayData.rgbaSprite.iG, sBeamHackGameplayData.rgbaSprite.iB, sBeamHackGameplayData.rgbaSprite.iA)
	ELSE
		DRAW_SPRITE("MPBeamHack", "Cursor_0", APPLY_ASPECT_MOD_TO_X(sBeamHackGameplayData.vCursor.x), sBeamHackGameplayData.vCursor.y, sBeamHackGameplayData.fCursorSize / fAspectRatio, sBeamHackGameplayData.fCursorSize, 0.0, ciBHCURSOR_RED, ciBHCURSOR_GREEN, ciBHCURSOR_BLUE, (ROUND((SIN(TO_FLOAT((ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sBeamHackGameplayData.tdHackTime.Timer)))) * cfBH_CURSOR_SINE_SPEED) * 30.0) + 225.0)))
	ENDIF
ENDPROC

PROC DRAW_LEVEL()
	
	FLOAT fNodeXOffset = COS(TO_FLOAT((ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sBeamHackGameplayData.tdHackTime.Timer)))) * (CONST_PI * 2)) * cfBH_NODEVIBRADIUS
	FLOAT fNodeYOffset = SIN(TO_FLOAT((ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sBeamHackGameplayData.tdHackTime.Timer)))) * (CONST_PI * 2)) * cfBH_NODEVIBRADIUS
	FLOAT fSINPulse = SIN(TO_FLOAT(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sBeamHackGameplayData.tdHackTime.Timer))) * 0.5)
	
	INT i
	FLOAT fRotation
	
	//###Background Elements###
	//Rotating Nodes
	FOR i = 0 TO sBeamLevel.iMaxRotatingNodes - 1
		FLOAT fDist = cfBH_ROTNODE_SIZE
		IF IS_BEAMHACK_TUTORIAL()
		AND sBeamHackGameplayData.eTutStage = BHTUT_SELECT_NODE
			IF i = 1
				fDist += (fSINPulse * (cfBH_ROTNODE_SIZE * 0.25))
			ENDIF
		ENDIF
		VECTOR_2D vCentre = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vNodes[i][0], sBeamLevel.vNodes[i][1]), 2.0)
		fRotation = ATAN2(sBeamLevel.vNodes[i][1].y - sBeamLevel.vNodes[i][0].y, sBeamLevel.vNodes[i][1].x - sBeamLevel.vNodes[i][0].x)
		DRAW_SPRITE("MPBeamHackFG", "mirror_base", APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, cfBH_ROTNODE_SIZE / fAspectRatio, cfBH_ROTNODE_SIZE, 0.0, sBeamHackGameplayData.rgbaSprite.iR, sBeamHackGameplayData.rgbaSprite.iG, sBeamHackGameplayData.rgbaSprite.iB, sBeamHackGameplayData.rgbaSprite.iA)
		DRAW_SPRITE("MPBeamHackFG", "mirror_shadow", APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, fDist / fAspectRatio, fDist, fRotation, sBeamHackGameplayData.rgbaSprite.iR, sBeamHackGameplayData.rgbaSprite.iG, sBeamHackGameplayData.rgbaSprite.iB, sBeamHackGameplayData.rgbaSprite.iA)
	ENDFOR

	//Packets
	FOR i = 0 TO sBeamLevel.iMaxPackets - 1
		IF sBeamHackGameplayData.iPacketFrame[i] < 61
			FLOAT fSize = cfBH_PACKET_SIZE
			VECTOR_2D vCentre = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vPackets[i][1], sBeamLevel.vPackets[i][3]), 2.0)
			IF sBeamHackGameplayData.iPacketFrame[i] > 0
				vCentre.x += (fNodeXOffset * (fSize * ((TO_FLOAT(sBeamHackGameplayData.iPacketFrame[i]) / TO_FLOAT(ciBH_PACKET_FRAME_EXPLODE)) + 0.25)))
				vCentre.y += (fNodeXOffset * (fSize * ((TO_FLOAT(sBeamHackGameplayData.iPacketFrame[i]) / TO_FLOAT(ciBH_PACKET_FRAME_EXPLODE)) + 0.25)))
			ENDIF
			DRAW_SPRITE("MPBeamHackFG", "Node_Shatter_BG_60", APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, fSize / fAspectRatio, fSize, 0.0, sBeamHackGameplayData.rgbaSprite.iR, sBeamHackGameplayData.rgbaSprite.iG, sBeamHackGameplayData.rgbaSprite.iB, sBeamHackGameplayData.rgbaSprite.iA)
		ENDIF
	ENDFOR
	
	//Firewalls
	FOR i = 0 TO sBeamLevel.iMaxFirewalls - 1
		IF NOT IS_BIT_SET(sBeamHackGameplayData.iFirewallBS, i)
		AND sBeamHackGameplayData.iFirewallFrame[i] + ciBH_FIREWALL_FRAME_OFFSET < 61
			VECTOR_2D vCentre = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vFirewalls[i][1], sBeamLevel.vFirewalls[i][3]), 2.0)
			vCentre.y += (fNodeYOffset * (cfGRID_SIZE * (TO_FLOAT(sBeamHackGameplayData.iFirewallTime[i]) / TO_FLOAT(ciBH_FIREWALL_TIME))))
			DRAW_SPRITE("MPBeamHackFG", "Firewall_Shatter_BG_58", APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, cfGRID_SIZE / fAspectRatio, cfGRID_SIZE, 0.0, sBeamHackGameplayData.rgbaSprite.iR, sBeamHackGameplayData.rgbaSprite.iG, sBeamHackGameplayData.rgbaSprite.iB, sBeamHackGameplayData.rgbaSprite.iA)
		ENDIF
	ENDFOR

	//###Beam###
	DRAW_BEAM()
	
	//###Foreground Elements###
	//Rotating Nodes
	FOR i = 0 TO sBeamLevel.iMaxRotatingNodes - 1
		FLOAT fDist = cfBH_ROTNODE_SIZE
		INT iR = 0, iG = 0, iB = 0, iA = 0
		IF IS_BEAMHACK_TUTORIAL()
		AND sBeamHackGameplayData.eTutStage = BHTUT_SELECT_NODE
			IF i = 1
				fDist += fSINPulse * (cfBH_ROTNODE_SIZE * 0.25)
				iR = sBeamHackGameplayData.rgbaBeam.iR
				iG = sBeamHackGameplayData.rgbaBeam.iG
				iB = sBeamHackGameplayData.rgbaBeam.iB
				iA = ciBH_BHTUT_NODE_FLASH_MIN + ROUND(fSINPulse * ciBH_BHTUT_NODE_FLASH_MAX)
			ELSE
				iR = sBeamHackGameplayData.rgbaSprite.iR
				iG = sBeamHackGameplayData.rgbaSprite.iG
				iB = sBeamHackGameplayData.rgbaSprite.iB
				iA = 0
			ENDIF
		ELIF sBeamHackGameplayData.iSelectedNode = -1
		AND sBeamHackGameplayData.iCursorNode = -1
			iR = sBeamHackGameplayData.rgbaSprite.iR
			iG = sBeamHackGameplayData.rgbaSprite.iG
			iB = sBeamHackGameplayData.rgbaSprite.iB
			iA = ciBH_NODE_FLASH_MIN + ROUND(fSINPulse * ciBH_NODE_FLASH_MAX)
		ELIF sBeamHackGameplayData.iSelectedNode = -1
		AND sBeamHackGameplayData.iCursorNode = i
			iR = sBeamHackGameplayData.rgbaBeam.iR
			iG = sBeamHackGameplayData.rgbaBeam.iG
			iB = sBeamHackGameplayData.rgbaBeam.iB
			iA = ciBH_NODE_FLASH_MIN + ROUND(fSINPulse * ciBH_NODE_FLASH_MAX)
		ELIF sBeamHackGameplayData.iSelectedNode = i
			iR = sBeamHackGameplayData.rgbaBeam.iR
			iG = sBeamHackGameplayData.rgbaBeam.iG
			iB = sBeamHackGameplayData.rgbaBeam.iB
			iA = ciBH_BHSEL_NODE_FLASH_MIN + ROUND(fSINPulse * ciBH_BHSEL_NODE_FLASH_MAX)
		ELSE
			iR = sBeamHackGameplayData.rgbaSprite.iR
			iG = sBeamHackGameplayData.rgbaSprite.iG
			iB = sBeamHackGameplayData.rgbaSprite.iB
			iA = 0
		ENDIF
		VECTOR_2D vCentre = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vNodes[i][0], sBeamLevel.vNodes[i][1]), 2.0)
		fRotation = ATAN2(sBeamLevel.vNodes[i][1].y - sBeamLevel.vNodes[i][0].y, sBeamLevel.vNodes[i][1].x - sBeamLevel.vNodes[i][0].x)
		DRAW_SPRITE("MPBeamHackFG", "mirror_highlight", APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, fDist / fAspectRatio, fDist, fRotation, sBeamHackGameplayData.rgbaSprite.iR, sBeamHackGameplayData.rgbaSprite.iG, sBeamHackGameplayData.rgbaSprite.iB, sBeamHackGameplayData.rgbaSprite.iA)
		DRAW_SPRITE("MPBeamHackFG", "mirror_white", APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, fDist / fAspectRatio, fDist, fRotation, iR, iG, iB, iA)
	ENDFOR

	//Packets
	FOR i = 0 TO sBeamLevel.iMaxPackets - 1
		IF sBeamHackGameplayData.iPacketFrame[i] < ciBH_PACKET_FRAME_MAX
			IF sBeamHackGameplayData.iPacketFrame[i] > ciBH_PACKET_FRAME_EXPLODE
				sBeamHackGameplayData.iPacketFrame[i] += sBeamHackGameplayData.iUpdateFrames
				IF NOT IS_BIT_SET(sBeamHackGameplayData.iPacketBS, i)
					PRINTLN("[JS][BeamHack] - DRAW_LEVEL - BACKUP -Packet ", i, " has been destroyed. Packets Remaining: ", sBeamHackGameplayData.iPacketsRemaining)
					SET_BIT(sBeamHackGameplayData.iPacketBS, i)
					sBeamHackGameplayData.iPacketsRemaining--
					PLAY_SOUND_FRONTEND(-1, "Blue_Target_Explode", "dlc_xm_silo_laser_hack_sounds")
				ENDIF
				IF sBeamHackGameplayData.iPacketFrame[i] > ciBH_PACKET_FRAME_MAX
					sBeamHackGameplayData.iPacketFrame[i] = ciBH_PACKET_FRAME_MAX
				ENDIF
			ENDIF
			FLOAT fSize = cfBH_PACKET_SIZE
			IF sBeamHackGameplayData.iPacketFrame[i] > ciBH_PACKET_FRAME_EXPLODE
				fSize = cfBH_PACKET_SIZE * 2
			ENDIF
			VECTOR_2D vCentre = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vPackets[i][1], sBeamLevel.vPackets[i][3]), 2.0)
			TEXT_LABEL_23 tl23 = "Node_Shatter_FG_"
			IF sBeamHackGameplayData.iPacketFrame[i] < 24
				tl23 += 24
			ELIF sBeamHackGameplayData.iPacketFrame[i] < 60
			AND (sBeamHackGameplayData.iPacketFrame[i] & 1) = 1
				tl23 += sBeamHackGameplayData.iPacketFrame[i] - 1
			ELIF sBeamHackGameplayData.iPacketFrame[i] > 63
			AND sBeamHackGameplayData.iPacketFrame[i] <= 75
			AND (sBeamHackGameplayData.iPacketFrame[i] & 1) = 0
				tl23 += sBeamHackGameplayData.iPacketFrame[i] - 1
			ELIF sBeamHackGameplayData.iPacketFrame[i] > 75
				tl23 += 75
			ELSE
				tl23 += sBeamHackGameplayData.iPacketFrame[i] 
			ENDIF
			IF sBeamHackGameplayData.iPacketFrame[i] <= ciBH_PACKET_FRAME_EXPLODE
			AND sBeamHackGameplayData.iPacketFrame[i] > 0
			 	vCentre.x += fNodeXOffset * (fSize * ((TO_FLOAT(sBeamHackGameplayData.iPacketFrame[i]) / TO_FLOAT(ciBH_PACKET_FRAME_EXPLODE)) + 0.25))
				vCentre.y += fNodeYOffset * (fSize * ((TO_FLOAT(sBeamHackGameplayData.iPacketFrame[i]) / TO_FLOAT(ciBH_PACKET_FRAME_EXPLODE)) + 0.25))
			ENDIF
			DRAW_SPRITE("MPBeamHackFG", tl23, APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, fSize / fAspectRatio, fSize, 0.0, sBeamHackGameplayData.rgbaSprite.iR, sBeamHackGameplayData.rgbaSprite.iG, sBeamHackGameplayData.rgbaSprite.iB, sBeamHackGameplayData.rgbaSprite.iA)
		ENDIF
	ENDFOR
	
	//Firewalls
	FOR i = 0 TO sBeamLevel.iMaxFirewalls - 1
		IF NOT IS_BIT_SET(sBeamHackGameplayData.iFirewallBS, i)
			VECTOR_2D vCentre = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vFirewalls[i][1], sBeamLevel.vFirewalls[i][3]), 2.0)
			FLOAT fSize = cfGRID_SIZE
			TEXT_LABEL_23 tl23 = "Firewall_Shatter_FG_"
			IF sBeamHackGameplayData.iFirewallFrame[i] + ciBH_FIREWALL_FRAME_OFFSET > 60
				fSize = cfGRID_SIZE * 2
			ELSE
				vCentre.y += (fNodeYOffset * (fSize * (TO_FLOAT(sBeamHackGameplayData.iFirewallTime[i]) / TO_FLOAT(ciBH_FIREWALL_TIME))))
			ENDIF
			IF sBeamHackGameplayData.iFirewallFrame[i] + ciBH_FIREWALL_FRAME_OFFSET = 64
				tl23 += 63
			ELIF sBeamHackGameplayData.iFirewallFrame[i] + ciBH_FIREWALL_FRAME_OFFSET = 66
				tl23 += 65
			ELIF sBeamHackGameplayData.iFirewallFrame[i] + ciBH_FIREWALL_FRAME_OFFSET = 73
				tl23 += 72
			ELIF sBeamHackGameplayData.iFirewallFrame[i] + ciBH_FIREWALL_FRAME_OFFSET = 75
				tl23 += 74
			ELIF sBeamHackGameplayData.iFirewallFrame[i] + ciBH_FIREWALL_FRAME_OFFSET > 76
				tl23 += 76
			ELSE
				tl23 += sBeamHackGameplayData.iFirewallFrame[i] + ciBH_FIREWALL_FRAME_OFFSET
			ENDIF	
			INT iColour = 255
			IF sBeamHackGameplayData.iFirewallFrame[i] = 0
				iColour -= ROUND((sBeamHackGameplayData.iFirewallTime[i] / TO_FLOAT(ciBH_FIREWALL_TIME)) * 150)
			ENDIF
			
			DRAW_SPRITE("MPBeamHackFG", tl23, APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, fSize / fAspectRatio, fSize, 0.0, sBeamHackGameplayData.rgbaSprite.iR, iColour, iColour, sBeamHackGameplayData.rgbaSprite.iA)
		ENDIF
	ENDFOR
	
	//Proxies
	FOR i = 0 TO sBeamLevel.iMaxProxies - 1
		VECTOR_2D vCentre = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vProxies[i][0], sBeamLevel.vProxies[i][2]), 2.0)
		TEXT_LABEL_23 tl23 = "Prism_FG_"
		tl23 += ABSI(sBeamHackGameplayData.iProxyAngle)
		DRAW_SPRITE("MPBeamHackFG", tl23, APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, (cfGRID_SIZE) / fAspectRatio, cfGRID_SIZE, 0.0, sBeamHackGameplayData.rgbaSprite.iR, sBeamHackGameplayData.rgbaSprite.iG, sBeamHackGameplayData.rgbaSprite.iB, sBeamHackGameplayData.rgbaSprite.iA)
	ENDFOR
	
	//Inner Walls
	TEXT_LABEL_23 tlDict = GET_BEAMHACK_TEXTURE_DICT_FOR_LEVEL(sBeamHackGameplayData.iLevel)
	TEXT_LABEL_23 tlTx = GET_BEAMHACK_TEXTURE_FG_FOR_LEVEL(sBeamHackGameplayData.iLevel)
	DRAW_SPRITE(tlDict, tlTx, 0.5, 0.5, 1.0 * fAspectRatioMod, 1.0, 0.0, sBeamHackGameplayData.rgbaSprite.iR, sBeamHackGameplayData.rgbaSprite.iG, sBeamHackGameplayData.rgbaSprite.iB, sBeamHackGameplayData.rgbaSprite.iA)

	DRAW_SPRITE("MPBeamHack", "border", 0.5, 0.5, 1.0 * fAspectRatioMod, 1.0, 0.0, sBeamHackGameplayData.rgbaSprite.iR, sBeamHackGameplayData.rgbaSprite.iG, sBeamHackGameplayData.rgbaSprite.iB, sBeamHackGameplayData.rgbaSprite.iA)
	
ENDPROC

FUNC BOOL IS_BEAM_INTERSECTING_EDGE(VECTOR_2D vBeamOrigin, VECTOR_2D vBeamDirection, VECTOR_2D v0, VECTOR_2D v1, FLOAT &fDistance)
	VECTOR_2D vSegment = SUBTRACT_VECTOR_2D(v1, v0)
	VECTOR_2D vSegmentPerp = GET_LEFT_PERPENDICULAR_VECTOR_2D(vSegment)
	FLOAT fPerpDot = DOT_PRODUCT_VECTOR_2D(vBeamDirection, vSegmentPerp)
	IF IS_FLOAT_IN_RANGE(fPerpDot, -cfBH_FLOAT_TOLERANCE, cfBH_FLOAT_TOLERANCE)
		fDistance = MAX_FLOAT
		RETURN FALSE
	ENDIF
	VECTOR_2D vD = SUBTRACT_VECTOR_2D(v0, vBeamOrigin)
	fDistance = DOT_PRODUCT_VECTOR_2D(vSegmentPerp, vD) / fPerpDot
	FLOAT fS = DOT_PRODUCT_VECTOR_2D(GET_LEFT_PERPENDICULAR_VECTOR_2D(vBeamDirection), vD) / fPerpDot
	RETURN (fDistance > cfBH_SAME_WALL_HIT_TOLERANCE AND fDistance <= MAX_FLOAT AND fS >= 0.0 AND fS <= 1)
ENDFUNC

FUNC INT GET_QUADRANT_FROM_VECTOR(VECTOR_2D v)
	IF (v.x > -cfBH_FLOAT_TOLERANCE AND v.y <= 0.0)
		RETURN 1
	ELIF (v.x <= -cfBH_FLOAT_TOLERANCE AND v.y <= 0.0)
		RETURN 2
	ELIF (v.x <= -cfBH_FLOAT_TOLERANCE AND v.y > 0.0)
		RETURN 3
	ELSE 
		RETURN 4
	ENDIF
ENDFUNC

FUNC BOOL WORTH_CHECKING_EDGE(VECTOR_2D vBeamOrigin, VECTOR_2D v0, VECTOR_2D v1, INT iDirQuadrant)
	IF iDirQuadrant = 1 OR iDirQuadrant = 2 //Heading up
		IF v0.y > vBeamOrigin.y
		AND v1.y > vBeamOrigin.y
			RETURN FALSE
		ENDIF
	ELIF iDirQuadrant = 3 OR iDirQuadrant = 4 //Heading Down
		IF v0.y < vBeamOrigin.y
		AND v1.y < vBeamOrigin.y
			RETURN FALSE
		ENDIF
	ENDIF
	IF iDirQuadrant = 1 OR iDirQuadrant = 4 //Heading Right
		IF v0.x < vBeamOrigin.x
		AND v1.x < vBeamOrigin.x
			RETURN FALSE
		ENDIF
	ELIF iDirQuadrant = 2 OR iDirQuadrant = 3 //Heading Left
		IF v0.x > vBeamOrigin.x
		AND v1.x > vBeamOrigin.x
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC GET_DIAGONAL_EDGE_TO_CHECK(INT iQuadrant, INT &iPoint0, INT &iPoint1)
	iPoint0 = 0
	iPoint1 = 0
	IF iQuadrant = 1
	OR iQuadrant = 3
		iPoint0 = 1
		iPoint1 = 3
	ELIF iQuadrant = 2
	OR iQuadrant = 4
		iPoint0 = 0
		iPoint1 = 2
	ENDIF
ENDPROC

PROC GET_EDGE_TO_CHECK(INT iEdge, INT iQuadrant, INT &iPoint0, INT &iPoint1, BOOL bInvert = FALSE)
	iPoint0 = 0
	iPoint1 = 0
	IF bInvert //For Outer Walls
		IF iQuadrant = 1
			iQuadrant = 3
		ELIF iQuadrant = 2
			iQuadrant = 4
		ELIF iQuadrant = 3
			iQuadrant = 1
		ELIF iQuadrant = 4
			iQuadrant = 2
		ENDIF
	ENDIF
	
	IF iQuadrant = 1
		IF iEdge = 0
			iPoint0 = 0
			iPoint1 = 1
		ELIF iEdge = 1
			iPoint0 = 3
			iPoint1 = 0
		ENDIF
	ELIF iQuadrant = 2
		IF iEdge = 0
			iPoint0 = 2
			iPoint1 = 3
		ELIF iEdge = 1
			iPoint0 = 3
			iPoint1 = 0
		ENDIF
	ELIF iQuadrant = 3
		IF iEdge = 0
			iPoint0 = 1
			iPoint1 = 2
		ELIF iEdge = 1
			iPoint0 = 2
			iPoint1 = 3
		ENDIF
	ELIF iQuadrant = 4
		IF iEdge = 0
			iPoint0 = 0
			iPoint1 = 1
		ELIF iEdge = 1
			iPoint0 = 1
			iPoint1 = 2
		ENDIF
	ENDIF
	//PRINTLN("[JS][BeamHack] - GET_EDGE_TO_CHECK -  iEdge = ", iEdge, " iQuadrant = ", iQuadrant, " iPoint0 = ", iPoint0, " iPoint1 = ", iPoint1)
ENDPROC

PROC PROCESS_BEAM()
	
	INT iBeam
	CLEAR_BIT(sBeamHackGameplayData.iBS, ciBEAMHACKBS_HITTING_PROXY)
	sBeamHackGameplayData.iBeamLength = 0
	INT iLastProxyHit = -1
	INT iBeamIndexProxyLastHit = -1
	FOR iBeam = 1 TO ciBH_MAX_BEAM_SEGMENTS - 1
		FLOAT fShortestDistance = MAX_FLOAT
		FLOAT fTempDistance = MAX_FLOAT
		VECTOR_2D vNormal
		VECTOR_2D vBeamDirection = NORMALISE_VECTOR_2D(SUBTRACT_VECTOR_2D(vBeam[iBeam], vBeam[iBeam - 1]))
		INT iDirQuadrant = GET_QUADRANT_FROM_VECTOR(vBeamDirection)
		BOOL bCrossed = FALSE
		BOOL bNode = FALSE
		INT iProxy = -1
		sBeamHackGameplayData.iPacketHit = -1
		sBeamHackGameplayData.iFirewallHit = -1
		SHAPE_TYPES eLastHitShape = ST_MAX_SHAPES
		INT iLastHitIndex = -1
		INT i = 0, j = 0, k = 0
		VECTOR_2D vEdge0, vEdge1
		//Nodes
		FOR i = 0 TO sBeamLevel.iMaxNodes - 1
			IF WORTH_CHECKING_EDGE(vBeam[iBeam - 1], sBeamLevel.vNodes[i][0], sBeamLevel.vNodes[i][1], iDirQuadrant)
				IF IS_BEAM_INTERSECTING_EDGE(vBeam[iBeam - 1], vBeamDirection, sBeamLevel.vNodes[i][0], sBeamLevel.vNodes[i][1], fTempDistance)
					PRINTLN("[JS][BeamHack] - PROCESS_BEAM - Intersecting node ", i," Distance: ", fTempDistance)
					IF fTempDistance < fShortestDistance AND fTempDistance <= MAX_FLOAT
						fShortestDistance = fTempDistance
						vEdge0 = sBeamLevel.vNodes[i][0]
						vEdge1 = sBeamLevel.vNodes[i][1]
						bCrossed = TRUE
						bNode = TRUE
						iLastHitIndex = i
						eLastHitShape = ST_ROTATING_NODE
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		//Node Walls
		FOR i = sBeamLevel.iMaxRotatingNodes TO sBeamLevel.iMaxNodes - 1
			VECTOR_2D vNodeWallPoint = INIT_VECTOR_2D(sBeamLevel.vNodes[i][1].x, sBeamLevel.vNodes[i][0].y)
			IF WORTH_CHECKING_EDGE(vBeam[iBeam - 1], sBeamLevel.vNodes[i][0], vNodeWallPoint, iDirQuadrant)
				IF IS_BEAM_INTERSECTING_EDGE(vBeam[iBeam - 1], vBeamDirection, sBeamLevel.vNodes[i][0], vNodeWallPoint, fTempDistance)
					PRINTLN("[JS][BeamHack] - PROCESS_BEAM - Intersecting node wall (1) ", i," Distance: ", fTempDistance)
					IF fTempDistance < fShortestDistance AND fTempDistance <= MAX_FLOAT
						fShortestDistance = fTempDistance
						vEdge0 = sBeamLevel.vNodes[i][0]
						vEdge1 = vNodeWallPoint
						bCrossed = TRUE
						bNode = FALSE
						iLastHitIndex = i
						eLastHitShape = ST_NODE
					ENDIF
				ENDIF
			ENDIF
			
			IF WORTH_CHECKING_EDGE(vBeam[iBeam - 1], vNodeWallPoint, sBeamLevel.vNodes[i][1], iDirQuadrant)
				IF IS_BEAM_INTERSECTING_EDGE(vBeam[iBeam - 1], vBeamDirection, vNodeWallPoint, sBeamLevel.vNodes[i][1], fTempDistance)
					PRINTLN("[JS][BeamHack] - PROCESS_BEAM - Intersecting node wall (2) ", i," Distance: ", fTempDistance)
					IF fTempDistance < fShortestDistance AND fTempDistance <= MAX_FLOAT
						fShortestDistance = fTempDistance
						vEdge0 = vNodeWallPoint
						vEdge1 = sBeamLevel.vNodes[i][1]
						bCrossed = TRUE
						bNode = FALSE
						iLastHitIndex = i
						eLastHitShape = ST_NODE
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		//Packets
		FOR i = 0 TO sBeamLevel.iMaxPackets - 1
			IF NOT IS_BIT_SET(sBeamHackGameplayData.iPacketBS, i)	
				GET_DIAGONAL_EDGE_TO_CHECK(iDirQuadrant, j, k)
				IF WORTH_CHECKING_EDGE(vBeam[iBeam - 1], sBeamLevel.vPackets[i][j], sBeamLevel.vPackets[i][k], iDirQuadrant)
					IF IS_BEAM_INTERSECTING_EDGE(vBeam[iBeam - 1], vBeamDirection, sBeamLevel.vPackets[i][j], sBeamLevel.vPackets[i][k], fTempDistance)
						PRINTLN("[JS][BeamHack] - PROCESS_BEAM - Intersecting Packet ", i," Distance: ", fTempDistance)
						IF fTempDistance < fShortestDistance AND fTempDistance <= MAX_FLOAT
							fShortestDistance = fTempDistance
							bCrossed = TRUE
							bNode = FALSE
							sBeamHackGameplayData.iPacketHit = i
							iLastHitIndex = i
							eLastHitShape = ST_PACKET
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		//Firewalls
		FOR i = 0 TO sBeamLevel.iMaxFirewalls - 1
			IF NOT IS_BIT_SET(sBeamHackGameplayData.iFirewallBS, i)
				GET_DIAGONAL_EDGE_TO_CHECK(iDirQuadrant, j, k)
				IF WORTH_CHECKING_EDGE(vBeam[iBeam - 1], sBeamLevel.vFirewalls[i][j], sBeamLevel.vFirewalls[i][k], iDirQuadrant)
					IF IS_BEAM_INTERSECTING_EDGE(vBeam[iBeam - 1], vBeamDirection, sBeamLevel.vFirewalls[i][j], sBeamLevel.vFirewalls[i][k], fTempDistance)
						PRINTLN("[JS][BeamHack] - PROCESS_BEAM - Intersecting Firewall ", i," Distance: ", fTempDistance)
						IF fTempDistance < fShortestDistance AND fTempDistance <= MAX_FLOAT
							fShortestDistance = fTempDistance
							bCrossed = TRUE
							bNode = FALSE
							sBeamHackGameplayData.iPacketHit = -1
							sBeamHackGameplayData.iFirewallHit = i
							iLastHitIndex = i
							eLastHitShape = ST_FIREWALL
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		//Proxies
		FOR i = 0 TO sBeamLevel.iMaxProxies - 1
			GET_DIAGONAL_EDGE_TO_CHECK(iDirQuadrant, j, k)
			IF WORTH_CHECKING_EDGE(vBeam[iBeam - 1], sBeamLevel.vProxies[i][j], sBeamLevel.vProxies[i][k], iDirQuadrant)
				IF IS_BEAM_INTERSECTING_EDGE(vBeam[iBeam - 1], vBeamDirection, sBeamLevel.vProxies[i][j], sBeamLevel.vProxies[i][k], fTempDistance)
					PRINTLN("[JS][BeamHack] - PROCESS_BEAM - Intersect between proxy ", i, " edge ", j,", ", k, " Distance: ", fTempDistance)
					IF fTempDistance < fShortestDistance AND fTempDistance <= MAX_FLOAT
						fShortestDistance = fTempDistance
						bCrossed = TRUE
						bNode = FALSE
						iProxy = i
						sBeamHackGameplayData.iPacketHit = -1
						sBeamHackGameplayData.iFirewallHit = -1
						iLastHitIndex = i
						eLastHitShape = ST_PROXY
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		//Inner Walls
		FOR i = 0 TO sBeamLevel.iMaxInnerWalls - 1
			GET_DIAGONAL_EDGE_TO_CHECK(iDirQuadrant, j, k)
			IF WORTH_CHECKING_EDGE(vBeam[iBeam - 1], sBeamLevel.vInnerWalls[i][j], sBeamLevel.vInnerWalls[i][k], iDirQuadrant)
				IF IS_BEAM_INTERSECTING_EDGE(vBeam[iBeam - 1], vBeamDirection, sBeamLevel.vInnerWalls[i][j], sBeamLevel.vInnerWalls[i][k], fTempDistance)
					PRINTLN("[JS][BeamHack] - PROCESS_BEAM - Intersect between inner wall ", i, " edge ", j,", ", k, " Distance: ", fTempDistance)
					IF fTempDistance < fShortestDistance AND fTempDistance <= MAX_FLOAT
						fShortestDistance = fTempDistance
						bCrossed = TRUE
						bNode = FALSE
						iProxy = -1
						sBeamHackGameplayData.iPacketHit = -1
						sBeamHackGameplayData.iFirewallHit = -1
						iLastHitIndex = i
						eLastHitShape = ST_INNER_WALL
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		//Outer Walls
		IF NOT bCrossed
			FOR i = 0 TO (ciBH_MAX_OUTER_WALLS / 2) - 1
				GET_EDGE_TO_CHECK(i, iDirQuadrant, j, k, TRUE)
				IF IS_BEAM_INTERSECTING_EDGE(vBeam[iBeam - 1], vBeamDirection, sBeamLevel.vOuterWalls[j], sBeamLevel.vOuterWalls[k], fTempDistance)
					PRINTLN("[JS][BeamHack] - PROCESS_BEAM - Intersect between outer walls edge ", j,", ", k, " Distance: ", fTempDistance)
					IF fTempDistance < fShortestDistance AND fTempDistance <= MAX_FLOAT
						fShortestDistance = fTempDistance
						bCrossed = TRUE
						bNode = FALSE
						iProxy = -1
						sBeamHackGameplayData.iPacketHit = -1
						sBeamHackGameplayData.iFirewallHit = -1
						iLastHitIndex = i
						eLastHitShape = ST_MAX_SHAPES
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		IF fShortestDistance < MAX_FLOAT
			vBeam[iBeam] = ADD_VECTOR_2D(vBeam[iBeam - 1], MULTIPLY_VECTOR_2D(vBeamDirection, fShortestDistance))
		ENDIF
		sBeamHackGameplayData.iBeamLength++
		IF NOT bNode
		AND iProxy = -1
			IF iLastHitIndex > -1
				IF eLastHitShape = ST_INNER_WALL
					INT iWallCheck
					FOR iWallCheck = 0 TO (ciBH_INNER_WALL_POINTS / 2)
						GET_EDGE_TO_CHECK(iWallCheck, iDirQuadrant, j, k)
						IF WORTH_CHECKING_EDGE(vBeam[iBeam - 1], sBeamLevel.vInnerWalls[iLastHitIndex][j], sBeamLevel.vInnerWalls[iLastHitIndex][k], iDirQuadrant)
							IF IS_BEAM_INTERSECTING_EDGE(vBeam[iBeam - 1], vBeamDirection, sBeamLevel.vInnerWalls[iLastHitIndex][j], sBeamLevel.vInnerWalls[iLastHitIndex][k], fTempDistance)
								PRINTLN("[JS][BeamHack] - PROCESS_BEAM - BETTER intersect between inner wall ", iLastHitIndex, " edge ", j,", ", k, " Distance: ", fTempDistance)
								IF fTempDistance < fShortestDistance AND fTempDistance <= MAX_FLOAT
									fShortestDistance = fTempDistance
									bCrossed = TRUE
									bNode = FALSE
									iProxy = -1
									sBeamHackGameplayData.iPacketHit = -1
									sBeamHackGameplayData.iFirewallHit = -1
									iLastHitIndex = i
									eLastHitShape = ST_INNER_WALL
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					IF fShortestDistance < MAX_FLOAT
						vBeam[iBeam] = ADD_VECTOR_2D(vBeam[iBeam - 1], MULTIPLY_VECTOR_2D(vBeamDirection, fShortestDistance))
					ENDIF
				ELIF eLastHitShape = ST_FIREWALL
					INT iWallCheck
					FOR iWallCheck = 0 TO (ciBH_FIREWALL_POINTS / 2)
						GET_EDGE_TO_CHECK(iWallCheck, iDirQuadrant, j, k)
						IF WORTH_CHECKING_EDGE(vBeam[iBeam - 1], sBeamLevel.vFirewalls[iLastHitIndex][j], sBeamLevel.vFirewalls[iLastHitIndex][k], iDirQuadrant)
							IF IS_BEAM_INTERSECTING_EDGE(vBeam[iBeam - 1], vBeamDirection, sBeamLevel.vFirewalls[iLastHitIndex][j], sBeamLevel.vFirewalls[iLastHitIndex][k], fTempDistance)
								PRINTLN("[JS][BeamHack] - PROCESS_BEAM - BETTER intersect between Firewall ", iLastHitIndex," Distance: ", fTempDistance)
								IF fTempDistance < fShortestDistance AND fTempDistance <= MAX_FLOAT
									fShortestDistance = fTempDistance
									bCrossed = TRUE
									bNode = FALSE
									sBeamHackGameplayData.iPacketHit = -1
									sBeamHackGameplayData.iFirewallHit = iLastHitIndex
									iLastHitIndex = iLastHitIndex
									eLastHitShape = ST_FIREWALL
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					IF fShortestDistance < MAX_FLOAT
						vBeam[iBeam] = ADD_VECTOR_2D(vBeam[iBeam - 1], MULTIPLY_VECTOR_2D(vBeamDirection, fShortestDistance))
					ENDIF
				ELIF eLastHitShape = ST_PACKET
					INT iWallCheck
					FOR iWallCheck = 0 TO (ciBH_PACKET_POINTS / 2)
						GET_EDGE_TO_CHECK(iWallCheck, iDirQuadrant, j, k)
						IF WORTH_CHECKING_EDGE(vBeam[iBeam - 1], sBeamLevel.vPackets[iLastHitIndex][j], sBeamLevel.vPackets[iLastHitIndex][k], iDirQuadrant)
							IF IS_BEAM_INTERSECTING_EDGE(vBeam[iBeam - 1], vBeamDirection, sBeamLevel.vPackets[iLastHitIndex][j], sBeamLevel.vPackets[iLastHitIndex][k], fTempDistance)
								PRINTLN("[JS][BeamHack] - PROCESS_BEAM - BETTER intersect between Packet ", iLastHitIndex," Distance: ", fTempDistance)
								IF fTempDistance < fShortestDistance AND fTempDistance <= MAX_FLOAT
									fShortestDistance = fTempDistance
									bCrossed = TRUE
									bNode = FALSE
									sBeamHackGameplayData.iPacketHit = iLastHitIndex
									iLastHitIndex = iLastHitIndex
									eLastHitShape = ST_PACKET
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					IF fShortestDistance < MAX_FLOAT
						vBeam[iBeam] = ADD_VECTOR_2D(vBeam[iBeam - 1], MULTIPLY_VECTOR_2D(vBeamDirection, fShortestDistance))
					ENDIF
				ENDIF
			ENDIF
			BREAKLOOP
		ELIF iBeam < ciBH_MAX_BEAM_SEGMENTS - 1
			IF bNode
				vNormal = NORMALISE_VECTOR_2D(GET_LEFT_PERPENDICULAR_VECTOR_2D(SUBTRACT_VECTOR_2D(vEdge0, vEdge1)))
				vBeam[iBeam + 1] = SUBTRACT_VECTOR_2D(vBeamDirection, MULTIPLY_VECTOR_2D(vNormal, (2 * DOT_PRODUCT_VECTOR_2D(vBeamDirection, vNormal))))
				vBeam[iBeam + 1] = ADD_VECTOR_2D(vBeam[iBeam], vBeam[iBeam + 1])
			ELIF iProxy != -1
				IF (iBeam + 1) < ciBH_MAX_BEAM_SEGMENTS - 1
					IF iLastProxyHit = iProxy
					AND iBeamIndexProxyLastHit = (iBeam - ciBH_PROXY_CANCEL_LOOP_COUNT)
						PRINTLN("[JS][Beamhack] - PROCESS_BEAM - Infinite proxy loop - BAIL")
						BREAKLOOP
					ENDIF
					VECTOR_2D vProxyCentre = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vProxies[iProxy][0], sBeamLevel.vProxies[iProxy][2]), 2.0)
					vBeam[iBeam + 1] = vProxyCentre
					vBeam[iBeam + 2] = ROTATE_VECTOR_2D_AROUND_VECTOR_2D(vProxyCentre, INIT_VECTOR_2D(vProxyCentre.x, 0), TO_FLOAT(sBeamHackGameplayData.iProxyAngle))
					SET_BIT(sBeamHackGameplayData.iBS, ciBEAMHACKBS_HITTING_PROXY)
					iLastProxyHit = iProxy
					iBeamIndexProxyLastHit = iBeam
					sBeamHackGameplayData.iBeamLength++
					iBeam++
				ELSE
					BREAKLOOP
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC VECTOR_2D GET_MIDPOINT_FROM_NODE(INT iNode)
	RETURN DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vNodes[iNode][0], sBeamLevel.vNodes[iNode][1]), 2.0)
ENDFUNC

FUNC INT GET_NODE_FROM_CURSOR()
	INT i
	FOR i = 0 TO sBeamLevel.iMaxRotatingNodes - 1
		IF VECTOR_2D_DIST2(GET_MIDPOINT_FROM_NODE(i), sBeamHackGameplayData.vCursor) < POW(cfGRID_HALF_SIZE, 2.0)
			RETURN i			
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

PROC KEEP_BEAM_HACK_CURSOR_IN_BOUNDS(FLOAT& fX, FLOAT& fY)

	IF fX > sBeamLevel.vOuterWalls[2].x
		fX = sBeamLevel.vOuterWalls[2].x
	ENDIF
	
	IF fX < sBeamLevel.vOuterWalls[0].x
		fX = sBeamLevel.vOuterWalls[0].x
	ENDIF
	
	IF fY > sBeamLevel.vOuterWalls[0].y
		fY = sBeamLevel.vOuterWalls[0].y
	ENDIF
	
	IF fY < sBeamLevel.vOuterWalls[2].y
		fY = sBeamLevel.vOuterWalls[2].y
	ENDIF
ENDPROC

PROC PROCESS_BEAM_HACK_CURSOR()
	
	FLOAT fCursorMovement = 0.0
	
	sBeamHackGameplayData.fCursorSize = cfBH_CURSOR_SIZE
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		sBeamHackGameplayData.vCursor.x = (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X) - 0.1) * fAspectRatio //Hack to get thsi working in all aspect ratios - not perfect
		sBeamHackGameplayData.vCursor.y = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
	ELSE
		FLOAT fX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
		FLOAT fY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
		IF fX > 0.1 OR fX < -0.1
			sBeamHackGameplayData.vCursor.x = sBeamHackGameplayData.vCursor.x +@ (fX / cfANALOGUE_SPEED_REDUCTION)
		ENDIF
		IF fY > 0.1 OR fY < -0.1
			sBeamHackGameplayData.vCursor.y = sBeamHackGameplayData.vCursor.y +@ (fY / cfANALOGUE_SPEED_REDUCTION)
		ENDIF
		
		fCursorMovement = ABSF(fX) + ABSF(fY)
		
		IF fCursorMovement > 1
			fCursorMovement = 1
		ENDIF
	ENDIF
	KEEP_BEAM_HACK_CURSOR_IN_BOUNDS(sBeamHackGameplayData.vCursor.x, sBeamHackGameplayData.vCursor.y)
	sBeamHackGameplayData.iCursorNode = GET_NODE_FROM_CURSOR()
	IF sBeamHackGameplayData.iCursorNode > -1
		sBeamHackGameplayData.iCursorFrame += sBeamHackGameplayData.iUpdateFrames
		IF sBeamHackGameplayData.iCursorFrame > ciBH_CURSOR_FRAME_MAX
			sBeamHackGameplayData.iCursorFrame = 0
		ENDIF
		FLOAT fSineValue = TO_FLOAT((ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sBeamHackGameplayData.tdHackTime.Timer))))
		sBeamHackGameplayData.fCursorSize += (SIN(fSineValue * cfBH_CURSOR_SINE_SPEED) * cfBH_CURSOR_SINE_MOD_MAX) + cfBH_CURSOR_SINE_MOD_MIN
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OR (IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT))
			sBeamHackGameplayData.iSelectedNode = sBeamHackGameplayData.iCursorNode
			sBeamHackGameplayData.fCursorSize = cfBH_CURSOR_SIZE * 1.175
			sBeamHackGameplayData.vCursor = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vNodes[sBeamHackGameplayData.iCursorNode][0], sBeamLevel.vNodes[sBeamHackGameplayData.iCursorNode][1]), 2.0)
			PLAY_SOUND_FRONTEND(-1, "Node_Select", "dlc_xm_silo_laser_hack_sounds")
		ENDIF
	ELSE
		sBeamHackGameplayData.iCursorFrame = 0
	ENDIF
	
	SET_VARIABLE_ON_SOUND(sBeamHackGameplayData.sBeamHack_CursorLoopSndID, "moveSpeed", fCursorMovement)
	
ENDPROC

PROC ROTATE_MIRROR_NODE_BY_DEGREES(INT iNode, FLOAT fAngle)
	VECTOR_2D vCenter = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sBeamLevel.vNodes[iNode][0], sBeamLevel.vNodes[iNode][1]), 2.0)
	sBeamLevel.vNodes[iNode][0] = ROTATE_VECTOR_2D_AROUND_VECTOR_2D(vCenter, sBeamLevel.vNodes[iNode][0], fAngle)
	sBeamLevel.vNodes[iNode][0].x = CLAMP(sBeamLevel.vNodes[iNode][0].x, vCenter.x - cfBH_ROTNODE_SIZE, vCenter.x + cfBH_ROTNODE_SIZE)
	sBeamLevel.vNodes[iNode][0].y = CLAMP(sBeamLevel.vNodes[iNode][0].y, vCenter.y - cfBH_ROTNODE_SIZE, vCenter.y + cfBH_ROTNODE_SIZE)
	sBeamLevel.vNodes[iNode][1] = ROTATE_VECTOR_2D_AROUND_VECTOR_2D(vCenter, sBeamLevel.vNodes[iNode][1], fAngle)
	sBeamLevel.vNodes[iNode][1].x = CLAMP(sBeamLevel.vNodes[iNode][1].x, vCenter.x - cfBH_ROTNODE_SIZE, vCenter.x + cfBH_ROTNODE_SIZE)
	sBeamLevel.vNodes[iNode][1].y = CLAMP(sBeamLevel.vNodes[iNode][1].y, vCenter.y - cfBH_ROTNODE_SIZE, vCenter.y + cfBH_ROTNODE_SIZE)
ENDPROC

PROC PROCESS_ROTATABLE_NODES()
	IF sBeamHackGameplayData.iSelectedNode > -1
		FLOAT fX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
		INT iTimeInterval = ciBH_ROTATE_INTERVAL
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
			iTimeInterval = ciBH_ROTATE_INTERVAL / 2
		ENDIF
		IF fX > 0.1 OR fX < -0.1
			IF NOT HAS_NET_TIMER_STARTED(sBeamHackGameplayData.tdRotateTimer)
				IF fX > 0.0
					sBeamHackGameplayData.fDesiredAngle += 5
				ELSE
					sBeamHackGameplayData.fDesiredAngle += -5
				ENDIF
				REINIT_NET_TIMER(sBeamHackGameplayData.tdRotateTimer)
			ELIF HAS_NET_TIMER_EXPIRED(sBeamHackGameplayData.tdRotateTimer, iTimeInterval)
				RESET_NET_TIMER(sBeamHackGameplayData.tdRotateTimer)
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(sBeamHackGameplayData.tdRotateTimer)
				RESET_NET_TIMER(sBeamHackGameplayData.tdRotateTimer)
			ENDIF
		ENDIF
		SET_VARIABLE_ON_SOUND(sBeamHackGameplayData.sBeamHack_RotateMirrorLoopSndID, "moveSpeed", ABSF(fX))
		
		IF sBeamHackGameplayData.fDesiredAngle != 0.0
			FLOAT fAngle = 0.0 +@ sBeamHackGameplayData.fDesiredAngle / (TO_FLOAT(iTimeInterval) / 1000)
			sBeamHackGameplayData.fDesiredAngle -= fAngle
			ROTATE_MIRROR_NODE_BY_DEGREES(sBeamHackGameplayData.iSelectedNode, fAngle)
			SET_BIT(sBeamHackGameplayData.iBS, ciBEAMHACKBS_UPDATE_BEAM)
		ENDIF
		
		IF NOT IS_BEAMHACK_TUTORIAL()
		OR sBeamHackGameplayData.eTutStage > BHTUT_AIM_NODE
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			OR (IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT))
				IF sBeamHackGameplayData.fDesiredAngle > cfBH_FLOAT_TOLERANCE
				OR sBeamHackGameplayData.fDesiredAngle < -cfBH_FLOAT_TOLERANCE
					ROTATE_MIRROR_NODE_BY_DEGREES(sBeamHackGameplayData.iSelectedNode, sBeamHackGameplayData.fDesiredAngle)
					SET_BIT(sBeamHackGameplayData.iBS, ciBEAMHACKBS_UPDATE_BEAM)
					sBeamHackGameplayData.fDesiredAngle = 0.0
				ENDIF
				sBeamHackGameplayData.iSelectedNode = -1
				PLAY_SOUND_FRONTEND(-1, "Node_Release", "dlc_xm_silo_laser_hack_sounds")
				SET_VARIABLE_ON_SOUND(sBeamHackGameplayData.sBeamHack_RotateMirrorLoopSndID, "moveSpeed", 0.0)
			ENDIF
		ENDIF
	ELSE
		PROCESS_BEAM_HACK_CURSOR()
	ENDIF
ENDPROC

PROC PROCESS_PACKETS(HACKING_CONTROLLER_STRUCT &sControllerStruct)

	SET_VARIABLE_ON_SOUND(sBeamHackGameplayData.sBeamHack_BlueChargeLoopSndID, "Control", 0)
	SET_VARIABLE_ON_SOUND(sBeamHackGameplayData.sBeamHack_RedChargeLoopSndID, "Control", 0)
	
	IF sBeamHackGameplayData.iPacketHit != -1
		sBeamHackGameplayData.iPacketFrame[sBeamHackGameplayData.iPacketHit] += sBeamHackGameplayData.iUpdateFrames
		
		FLOAT fPacketAmount = ( TO_FLOAT(sBeamHackGameplayData.iPacketFrame[sBeamHackGameplayData.iPacketHit]) / TO_FLOAT(ciBH_PACKET_FRAME_MAX) )
		PRINTLN("[BeamHackSnd] fPacketAmount: ", fPacketAmount)
		SET_VARIABLE_ON_SOUND(sBeamHackGameplayData.sBeamHack_BlueChargeLoopSndID, "Control", fPacketAmount)
		
		IF sBeamHackGameplayData.iPacketFrame[sBeamHackGameplayData.iPacketHit] > ciBH_PACKET_FRAME_MAX
			sBeamHackGameplayData.iPacketFrame[sBeamHackGameplayData.iPacketHit] = ciBH_PACKET_FRAME_MAX
		ENDIF
		IF sBeamHackGameplayData.iPacketFrame[sBeamHackGameplayData.iPacketHit] > ciBH_PACKET_FRAME_EXPLODE
		AND NOT IS_BIT_SET(sBeamHackGameplayData.iPacketBS, sBeamHackGameplayData.iPacketHit)	
			PRINTLN("[JS][BeamHack] - PROCESS_PACKETS - Packet ", sBeamHackGameplayData.iPacketHit, " has been destroyed. Packets Remaining: ", sBeamHackGameplayData.iPacketsRemaining)
			SET_BIT(sBeamHackGameplayData.iPacketBS, sBeamHackGameplayData.iPacketHit)
			sBeamHackGameplayData.iPacketHit = -1
			sBeamHackGameplayData.iPacketsRemaining--
			SET_BIT(sBeamHackGameplayData.iBS, ciBEAMHACKBS_UPDATE_BEAM)
			SET_BIT(sControllerStruct.iBS, ciGOHACKBS_PACKET_HIT)
			PLAY_SOUND_FRONTEND(-1, "Blue_Target_Explode", "dlc_xm_silo_laser_hack_sounds")
		ENDIF
	ELIF sBeamHackGameplayData.iFirewallHit != -1
		sBeamHackGameplayData.iFirewallTime[sBeamHackGameplayData.iFirewallHit] += FLOOR(GET_FRAME_TIME() * 1000)
		
		FLOAT fFirewallAmount = ( TO_FLOAT(sBeamHackGameplayData.iFirewallTime[sBeamHackGameplayData.iFirewallHit]) / TO_FLOAT(ciBH_FIREWALL_TIME) )
		PRINTLN("[BeamHackSnd] fFirewallAmount: ", fFirewallAmount)
		SET_VARIABLE_ON_SOUND(sBeamHackGameplayData.sBeamHack_RedChargeLoopSndID, "Control", fFirewallAmount)
		
		SET_CONTROL_LIGHT_EFFECT_COLOR(PLAYER_CONTROL, 255, 0, 0)
		PRINTLN("[JS][BeamHack] - PROCESS_PACKETS - Firewall ", sBeamHackGameplayData.iFirewallHit, " was hit! Time elapsed: ", sBeamHackGameplayData.iFirewallTime[sBeamHackGameplayData.iFirewallHit])
		IF sBeamHackGameplayData.iFirewallTime[sBeamHackGameplayData.iFirewallHit] >= ciBH_FIREWALL_TIME
			CLEAR_BIT(sBeamHackGameplayData.iBS, ciBEAMHACKBS_UPDATE_BEAM)
			IF NOT IS_BIT_SET(sBeamHackGameplayData.iFirewallBS, sBeamHackGameplayData.iFirewallHit)
				sBeamHackGameplayData.iFirewallFrame[sBeamHackGameplayData.iFirewallHit] += sBeamHackGameplayData.iUpdateFrames
				IF sBeamHackGameplayData.iFirewallFrame[sBeamHackGameplayData.iFirewallHit] > ciBH_FIREWALL_FRAME_MAX - ciBH_FIREWALL_FRAME_OFFSET
					sBeamHackGameplayData.iFirewallFrame[sBeamHackGameplayData.iFirewallHit] = ciBH_FIREWALL_FRAME_MAX - ciBH_FIREWALL_FRAME_OFFSET
				ENDIF
			ENDIF
			IF NOT HAS_NET_TIMER_STARTED(sBeamHackGameplayData.tdFireWallTimer)
				REINIT_NET_TIMER(sBeamHackGameplayData.tdFireWallTimer)
				SET_CONTROL_SHAKE(PLAYER_CONTROL, 100, 100)
				sBeamHackGameplayData.iLives--
				SET_BIT(sControllerStruct.iBS, ciGOHACKBS_FIREWALL_HIT)
				PLAY_SOUND_FRONTEND(-1, "Red_Target_Explode", "dlc_xm_silo_laser_hack_sounds")
			ELSE
				IF sBeamHackGameplayData.iFirewallFrame[sBeamHackGameplayData.iFirewallHit] >= ciBH_FIREWALL_FRAME_MAX - ciBH_FIREWALL_FRAME_OFFSET
				AND NOT IS_BIT_SET(sBeamHackGameplayData.iFirewallBS, sBeamHackGameplayData.iFirewallHit)
					SET_BIT(sBeamHackGameplayData.iFirewallBS, sBeamHackGameplayData.iFirewallHit)
				ENDIF	
				IF HAS_NET_TIMER_EXPIRED(sBeamHackGameplayData.tdFireWallTimer, ciBH_FIREWALL_PENALTY_TIME)
					sBeamHackGameplayData.iFirewallHit = -1
					IF sBeamHackGameplayData.iLives >= 0
						SET_BIT(sBeamHackGameplayData.iBS, ciBEAMHACKBS_UPDATE_BEAM)
					ENDIF
					RESET_NET_TIMER(sBeamHackGameplayData.tdFireWallTimer)
				ENDIF
			ENDIF
		ELSE
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 100, 30)
		ENDIF
	ELSE
		CLEAR_CONTROL_LIGHT_EFFECT(PLAYER_CONTROL)
	ENDIF
ENDPROC

PROC UPDATE_BEAM_HACK_STATE(BEAM_HACK_STATE eNewState)
	#IF IS_DEBUG_BUILD
	PRINTLN("[JS][BeamHack] - UPDATE_BEAM_HACK_STATE - Updating state from ",GET_STRING_FROM_BEAM_HACK_STATE(sBeamHackGameplayData.eBeamHackState)," to ",GET_STRING_FROM_BEAM_HACK_STATE(eNewState))
	#ENDIF
	sBeamHackGameplayData.eBeamHackState = eNewState
ENDPROC

PROC PROCESS_BEAM_HACK_EVERY_FRAME_MISC(HACKING_CONTROLLER_STRUCT &sControllerStruct)
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
	SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
	DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	Pause_Objective_Text()
	DISABLE_SELECTOR_THIS_FRAME()
	DISABLE_DPADDOWN_THIS_FRAME()
	THEFEED_HIDE_THIS_FRAME()
	IF NOT IS_SKYSWOOP_AT_GROUND() 
		UPDATE_BEAM_HACK_STATE(BEAM_HACK_CLEANUP)
	ENDIF
	#IF USE_REPLAY_RECORDING_TRIGGERS
		DISABLE_REPLAY_RECORDING_UI_THIS_FRAME()
	#ENDIF
	REPLAY_PREVENT_RECORDING_THIS_FRAME()
	IF NOT IS_PAUSE_MENU_ACTIVE()
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	ENDIF
	CLEAR_BIT(sBeamHackGameplayData.iBS, ciBEAMHACKBS_UPDATE_BEAM)
	HACKING_GET_CUSTOM_SCREEN_ASPECT_RATIO()
	HACKING_GET_ASPECT_RATIO_MODIFIER_FROM_ASPECT_RATIO()
	IF IS_BIT_SET(sControllerStruct.iBS, ciGOHACKBS_FIREWALL_HIT)
		CLEAR_BIT(sControllerStruct.iBS, ciGOHACKBS_FIREWALL_HIT)
	ENDIF
	IF IS_BIT_SET(sControllerStruct.iBS, ciGOHACKBS_PACKET_HIT)
		CLEAR_BIT(sControllerStruct.iBS, ciGOHACKBS_PACKET_HIT)
	ENDIF
	HACKING_HELP_SCALEFORM_EVERY_FRAME()
ENDPROC

PROC PROCESS_BEAMHACK_UI()
	//Time Remaining
	//Text
	SET_TEXT_SCALE(cfBH_UITextScale, cfBH_UITextScale)
	SET_TEXT_COLOUR(211, 55, 229, 255)
	SET_TEXT_CENTRE(TRUE)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("BEAM_TRT")
	END_TEXT_COMMAND_DISPLAY_TEXT(APPLY_ASPECT_MOD_TO_X(cfBH_TimePosX2), cfBH_TextPosY2)
	//Number
	INT iTimeDif = 0
	INT iMinutes = 0
	INT iSeconds = 0
	IF HAS_NET_TIMER_STARTED(sBeamHackGameplayData.tdHackTime)
		iTimeDif = sBeamHackGameplayData.iTimeDuration - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sBeamHackGameplayData.tdHackTime)
		iMinutes = iTimeDif/60000
		iSeconds = iTimeDif/1000 % 60
	ENDIF
	SET_TEXT_SCALE(cfBH_UINumScale, cfBH_UINumScale)
	SET_TEXT_COLOUR(211, 55, 229, 255)
	SET_TEXT_CENTRE(TRUE)
	TEXT_LABEL_15 tl15 = "BEAM_TR"
	IF iSeconds < 10
		tl15 += "S"
	ENDIF
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(tl15)
		ADD_TEXT_COMPONENT_INTEGER(iMinutes)
		ADD_TEXT_COMPONENT_INTEGER(iSeconds)
	END_TEXT_COMMAND_DISPLAY_TEXT(APPLY_ASPECT_MOD_TO_X(cfBH_TimePosX), cfBH_TextPosY)
	//Lives
	//Text
	SET_TEXT_SCALE(cfBH_UITextScale, cfBH_UITextScale)
	SET_TEXT_COLOUR(211, 55, 229, 255)
	SET_TEXT_CENTRE(TRUE)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("BEAM_LVT")
	END_TEXT_COMMAND_DISPLAY_TEXT(APPLY_ASPECT_MOD_TO_X(cfBH_LivesPosX2), cfBH_TextPosY2)
	//Number
	SET_TEXT_SCALE(cfBH_UINumScale, cfBH_UINumScale)
	SET_TEXT_COLOUR(211, 55, 229, 255)
	SET_TEXT_CENTRE(TRUE)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("BEAM_LV")
		ADD_TEXT_COMPONENT_INTEGER(sBeamHackGameplayData.iLives)
	END_TEXT_COMMAND_DISPLAY_TEXT(APPLY_ASPECT_MOD_TO_X(cfBH_LivesPosX), cfBH_TextPosY)
	//Help
	TEXT_LABEL_15 tl15Help
	IF NOT IS_BEAMHACK_TUTORIAL()
	OR sBeamHackGameplayData.eTutStage = BHTUT_REGULAR_TEXT
		IF sBeamHackGameplayData.iLevel = 0
		AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sBeamHackGameplayData.tdHackTime) < 10000
			tl15Help = "BEAMTUT_4"
		ELSE
			IF sBeamHackGameplayData.iSelectedNode > -1
				tl15Help = "BEAM_HLPb"
			ELSE
				tl15Help = "BEAM_HLPa"
			ENDIF
		ENDIF
	ELSE
		tl15Help = "BEAMTUT_"
		tl15Help += ENUM_TO_INT(sBeamHackGameplayData.eTutStage)
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		AND (sBeamHackGameplayData.eTutStage = BHTUT_DESELECT_NODE
		OR sBeamHackGameplayData.eTutStage = BHTUT_SELECT_NODE)
			tl15Help += "p"
		ENDIF
	ENDIF
	IF (IS_BIT_SET(sBeamHackGameplayData.iBS, ciBEAMHACKBS_HITTING_PROXY)
	OR HAS_NET_TIMER_STARTED(sBeamHackGameplayData.tdProxyHelp))
	AND NOT IS_BIT_SET(sBeamHackGameplayData.iBS, ciBEAMHACKBS_PLAYED_PROXY_HELP)
		IF NOT HAS_NET_TIMER_STARTED(sBeamHackGameplayData.tdProxyHelp)
			REINIT_NET_TIMER(sBeamHackGameplayData.tdProxyHelp)
			tl15Help = "BEAM_HLPc"
		ELIF HAS_NET_TIMER_EXPIRED(sBeamHackGameplayData.tdProxyHelp, ciBH_PROXY_HELP_TEXT_TIME)
			SET_BIT(sBeamHackGameplayData.iBS, ciBEAMHACKBS_PLAYED_PROXY_HELP)
			RESET_NET_TIMER(sBeamHackGameplayData.tdProxyHelp)
		ELSE
			tl15Help = "BEAM_HLPc"
		ENDIF
	ENDIF
	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tl15Help)
		CLEAR_HELP()
	ENDIF
	PRINT_HELP_NO_SOUND(tl15Help)
	
	//Scaleform
	IF sBeamHackGameplayData.iSelectedNode > -1
		ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_CANCEL, "BHSF_QT") //Quit
		ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_Y, "BHSF_SPR") //Speed Up Rotation
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			IF NOT IS_BEAMHACK_TUTORIAL()
			OR sBeamHackGameplayData.eTutStage > BHTUT_AIM_NODE
				ADD_MENU_HELP_KEY_INPUT(INPUT_CURSOR_ACCEPT, "BHSF_DSEL") //Deselect Node
			ENDIF
		ELSE
			IF NOT IS_BEAMHACK_TUTORIAL()
			OR sBeamHackGameplayData.eTutStage > BHTUT_AIM_NODE
				ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_ACCEPT, "BHSF_DSEL") //Deselect Node
			ENDIF
		ENDIF
		ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_AXIS_X, "BHSF_ROT") //Rotate Node
	ELSE
		ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_CANCEL, "BHSF_QT") //Quit
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			ADD_MENU_HELP_KEY_INPUT(INPUT_CURSOR_ACCEPT, "BHSF_SEL")//Select Node
			ADD_MENU_HELP_KEY_INPUT(INPUT_CURSOR_X, "BHSF_MCUR")//Move Cursor
		ELSE
			ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_ACCEPT, "BHSF_SEL")//Select Node
			ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_AXIS_X, "BHSF_MCUR")//Move Cursor
		ENDIF
	ENDIF
ENDPROC

PROC CLEAN_UP_BEAM_HACK_MINIGAME(HACKING_CONTROLLER_STRUCT &sControllerStruct, BOOL bFinalCleanup = FALSE)
	DEBUG_PRINTCALLSTACK()
	Unpause_Objective_Text()
	BEAMHACK_END_LOOPING_SOUNDS()
	g_FMMC_STRUCT.bDisablePhoneInstructions = FALSE
	MP_FORCE_TERMINATE_INTERNET_CLEAR()
	//Renable notifications
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bIsHacking = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
	//Disable multiple displays/blinders.
	SET_MULTIHEAD_SAFE(FALSE, TRUE)
	ENABLE_INTERACTION_MENU()
	IF NOT bFinalCleanup
		IF IS_SKYSWOOP_AT_GROUND()
		AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
			IF NETWORK_IS_GAME_IN_PROGRESS()
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ELSE
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
		ENDIF
	ENDIF
	TEXT_LABEL_23 tl23 = "MPBeamHack_lvl"
	tl23 += sBeamHackGameplayData.iLevel
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(tl23)
	
	RESET_NET_TIMER(sBeamHackGameplayData.tdBeamEndDelay)
	UPDATE_BEAM_HACK_STATE(BEAM_HACK_INIT)
	sControllerStruct.iBS = 0
ENDPROC

PROC INIT_BEAM_HACK_COLOURS()
	INIT_RGBA_STRUCT(sBeamHackGameplayData.rgbaNodeRotate, 150, 150, 150)
	INIT_RGBA_STRUCT(sBeamHackGameplayData.rgbaNodeRotateSelected, 225, 0, 40)
	INIT_RGBA_STRUCT(sBeamHackGameplayData.rgbaNodeStatic, 0, 191, 255)
	INIT_RGBA_STRUCT(sBeamHackGameplayData.rgbaInnerWall, 50, 205, 50)
	INIT_RGBA_STRUCT(sBeamHackGameplayData.rgbaOuterWall, 50, 205, 50)
	INIT_RGBA_STRUCT(sBeamHackGameplayData.rgbaPacket, 200, 0, 0)
	INIT_RGBA_STRUCT(sBeamHackGameplayData.rgbaFireWall, 218, 112, 214)
	INIT_RGBA_STRUCT(sBeamHackGameplayData.rgbaProxy, 0, 0, 214)
	INIT_RGBA_STRUCT(sBeamHackGameplayData.rgbaBeam, 171, 252, 255)
	INIT_RGBA_STRUCT(sBeamHackGameplayData.rgbaBeamStart, 0, 255, 127)
	INIT_RGBA_STRUCT(sBeamHackGameplayData.rgbaSprite, 255, 255, 255, 255)
	INIT_RGBA_STRUCT(sBeamHackGameplayData.rgbaNodeCursor, 255, 0, 0)
ENDPROC

PROC UPDATE_BEAMHACK_TUTORIAL_STATE(BEAMHACK_TUTORIAL_STATE eNewState)
	#IF IS_DEBUG_BUILD
	PRINTLN("[JS][BeamHack] - UPDATE_BEAMHACK_TUTORIAL_STATE - Updating state from ",ENUM_TO_INT(sBeamHackGameplayData.eTutStage)," to ",ENUM_TO_INT(eNewState))
	#ENDIF
	sBeamHackGameplayData.eTutStage = eNewState
ENDPROC

PROC PROCESS_BEAMHACK_TUTORIAL_STAGE()
	SWITCH sBeamHackGameplayData.eTutStage	
		CASE BHTUT_AIM_NODE
			IF IS_BIT_SET(sBeamHackGameplayData.iPacketBS, 0) //First packet destroyed
				UPDATE_BEAMHACK_TUTORIAL_STATE(BHTUT_DESELECT_NODE)
			ENDIF
		BREAK
		
		CASE BHTUT_DESELECT_NODE
			IF sBeamHackGameplayData.iSelectedNode = -1 //Node Released
				UPDATE_BEAMHACK_TUTORIAL_STATE(BHTUT_SELECT_NODE)
			ENDIF
		BREAK
		
		CASE BHTUT_SELECT_NODE
			IF sBeamHackGameplayData.iSelectedNode = 1 //Next node selected
				UPDATE_BEAMHACK_TUTORIAL_STATE(BHTUT_AIM_NODE_2)
			ELIF sBeamHackGameplayData.iSelectedNode != -1 //Wrong node selected
				UPDATE_BEAMHACK_TUTORIAL_STATE(BHTUT_DESELECT_NODE)
			ENDIF
		BREAK
		
		CASE BHTUT_AIM_NODE_2
			IF IS_BIT_SET(sBeamHackGameplayData.iPacketBS, 2) //Nodes Destroyed
			OR IS_BIT_SET(sBeamHackGameplayData.iPacketBS, 3)
			OR IS_BIT_SET(sBeamHackGameplayData.iPacketBS, 4)
				UPDATE_BEAMHACK_TUTORIAL_STATE(BHTUT_AIM_NODE_3)
			ENDIF
		BREAK
		
		CASE BHTUT_AIM_NODE_3
			IF HAS_NET_TIMER_STARTED(sBeamHackGameplayData.tdTutAccuracyHelp)
				IF HAS_NET_TIMER_EXPIRED(sBeamHackGameplayData.tdTutAccuracyHelp, ciBH_ACCURACY_HELP_TEXT_TIME)
					IF IS_BIT_SET(sBeamHackGameplayData.iPacketBS, 2) //Nodes Destroyed
					AND IS_BIT_SET(sBeamHackGameplayData.iPacketBS, 3)
					AND IS_BIT_SET(sBeamHackGameplayData.iPacketBS, 4)
						UPDATE_BEAMHACK_TUTORIAL_STATE(BHTUT_REGULAR_TEXT)
					ENDIF
				ENDIF
			ELSE
				REINIT_NET_TIMER(sBeamHackGameplayData.tdTutAccuracyHelp)
			ENDIF
		BREAK
		
		CASE BHTUT_FIREWALL_WARNING
			IF HAS_NET_TIMER_STARTED(sBeamHackGameplayData.tdTutFirewallHelp)
				IF HAS_NET_TIMER_EXPIRED(sBeamHackGameplayData.tdTutFirewallHelp, ciBH_FIREWALL_HELP_TEXT_TIME)
					UPDATE_BEAMHACK_TUTORIAL_STATE(BHTUT_REGULAR_TEXT)
				ENDIF
			ELSE
				REINIT_NET_TIMER(sBeamHackGameplayData.tdTutFirewallHelp)
			ENDIF
		BREAK
		
		CASE BHTUT_REGULAR_TEXT
			IF HAS_NET_TIMER_STARTED(sBeamHackGameplayData.tdTutFirewallHelp)
				RESET_NET_TIMER(sBeamHackGameplayData.tdTutFirewallHelp)
			ENDIF
			IF sBeamHackGameplayData.iFirewallHit != -1
				UPDATE_BEAMHACK_TUTORIAL_STATE(BHTUT_FIREWALL_WARNING)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC CLEAN_UP_BEAM_HACK_MINIGAME_ASSETS()

	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPBeamhack")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPBeamhackFG")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPBeamHack_lvl0")

ENDPROC

FUNC BOOL LOAD_BEAM_HACK_MINIGAME_ASSETS()

	REQUEST_STREAMED_TEXTURE_DICT("MPBeamHack")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPBeamHack")
		PRINTLN("[JS][BeamHack][LOAD_BEAM_HACK_MINIGAME_ASSETS] Not Loaded MPBeamHack")
		RETURN FALSE	
	ENDIF
	REQUEST_STREAMED_TEXTURE_DICT("MPBeamHack_lvl0")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPBeamHack_lvl0")
		PRINTLN("[JS][BeamHack][LOAD_BEAM_HACK_MINIGAME_ASSETS] Not Loaded MPBeamHack_lvl0")
		RETURN FALSE
	ENDIF
	REQUEST_STREAMED_TEXTURE_DICT("MPBeamHackFG")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPBeamHackFG")
		PRINTLN("[JS][BeamHack][LOAD_BEAM_HACK_MINIGAME_ASSETS] Not Loaded MPBeamHackFG")
		RETURN FALSE
	ENDIF
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_Silo_Laser_Hack")
		PRINTLN("[JS][BeamHack][LOAD_BEAM_HACK_MINIGAME_ASSETS] Not Loaded audio bank for beam hack")
		RETURN FALSE
	ENDIF

	RETURN TRUE

ENDFUNC

PROC PROCESS_BEAM_HACK_MINIGAME(HACKING_CONTROLLER_STRUCT &sControllerStruct, INT iNumPlays, INT iTimerOverride = -1, INT iRequiredHacks = 1)
	#IF IS_DEBUG_BUILD
	PROCESS_BEAM_HACK_DEBUG()
	#ENDIF
	IF sBeamHackGameplayData.eBeamHackState != BEAM_HACK_CLEANUP
		PROCESS_BEAM_HACK_EVERY_FRAME_MISC(sControllerStruct)
	ENDIF
	INT iScreenX, iScreenY
	GET_ACTUAL_SCREEN_RESOLUTION(iScreenX, iScreenY)
	IF NOT g_bHideHackingVisuals
	AND sBeamHackGameplayData.eBeamHackState >= BEAM_HACK_PLAY
		DRAW_PLAY_AREA()
	ENDIF
	
	IF iTimerOverride != -1
		sBeamHackGameplayData.iTimeDuration = iTimerOverride
	ELIF iTimerOverride != ciBH_HACK_TIME
		sBeamHackGameplayData.iTimeDuration = ciBH_HACK_TIME
	ENDIF
	
	SWITCH sBeamHackGameplayData.eBeamHackState
		CASE BEAM_HACK_INIT
			BEAM_HACK_GAMEPLAY_DATA sEmpty
			sBeamHackGameplayData = sEmpty
			INIT_BEAM_HACK_COLOURS()
			LOAD_MENU_ASSETS()
			
			IF iNumPlays < ciBH_MAX_LEVELS
				sBeamHackGameplayData.iLevel = iNumPlays
			ELSE
				SET_RANDOM_SEED(NATIVE_TO_INT(GET_NETWORK_TIME()))
				sBeamHackGameplayData.iLevel = GET_RANDOM_INT_IN_RANGE(1, ciBH_MAX_LEVELS)
			ENDIF
			
			TEXT_LABEL_23 tl23
			tl23 = GET_BEAMHACK_TEXTURE_DICT_FOR_LEVEL(sBeamHackGameplayData.iLevel)
			REQUEST_STREAMED_TEXTURE_DICT(tl23)
			IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
				PRINTLN("[JS][BeamHack] Not Loaded ", tl23)
				EXIT
			ENDIF
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				DISABLE_INTERACTION_MENU() 
			ELSE
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			
			//Turn on blinders for multihead displays.
			SET_MULTIHEAD_SAFE(TRUE, TRUE)
			g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE
			
			//Prevent notifications from appearing during the minigame.
			//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bIsHacking = TRUE
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
			CLEAR_BIT(sControllerStruct.iBS, ciGOHACKBS_QUIT)		
			
			UPDATE_BEAM_HACK_STATE(BEAM_HACK_LOAD_LEVEL)
		BREAK
		CASE BEAM_HACK_LOAD_LEVEL
			POPULATE_LEVEL_DATA(sBeamHackGameplayData.iLevel)
			
			BOOL bFirstHackDone
			bFirstHackDone = iRequiredHacks > 1 AND sBeamHackGameplayData.iHacksComplete < iRequiredHacks AND sBeamHackGameplayData.iHacksComplete > 0
			IF bFirstHackDone
				DRAW_SPRITE("MPBeamHack", "pass", 0.5, 0.5, 1.0 * fAspectRatioMod, 1.0, 0.0, sBeamHackGameplayData.rgbaSprite.iR, sBeamHackGameplayData.rgbaSprite.iG, sBeamHackGameplayData.rgbaSprite.iB, sBeamHackGameplayData.rgbaSprite.iA)
				DRAW_BIG_HACKING_TEXT("BEAM_P", 17, 165, 141, 255, 1.75, 1.75, 0.5, 0.4)
				tl23 = GET_BEAMHACK_TEXTURE_DICT_FOR_LEVEL(sBeamHackGameplayData.iLevel)
				REQUEST_STREAMED_TEXTURE_DICT(tl23)
				IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
					PRINTLN("[JS][BeamHack] Not Loaded (repeat) ", tl23)
					EXIT
				ENDIF
			ENDIF
			RESET_BEAM_HACK_GAMEPLAY_DATA(bFirstHackDone)

			//Run beam once
			PROCESS_BEAM()
			UPDATE_BEAM_HACK_STATE(BEAM_HACK_PLAY)
		BREAK
		CASE BEAM_HACK_PLAY
		
			IF IS_BEAMHACK_TUTORIAL()
				PROCESS_BEAMHACK_TUTORIAL_STAGE()
			ENDIF
		
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				PRINTLN("[BeamHack] Player has backed out by pressing CANCEL")
				HACKING_STORE_END_TIME(sControllerStruct, sBeamHackGameplayData.tdHackTime, sBeamHackGameplayData.iTimeDuration)
				SET_BIT(sControllerStruct.iBS, ciGOHACKBS_QUIT)
			ENDIF
		
			#IF IS_DEBUG_BUILD
			IF sBeamHackDebug.bDebugDrawDebugLines
				DRAW_LEVEL_DEBUG_LINES()
			ENDIF
			IF sBeamHackDebug.bDebugDrawGrid
				DRAW_BEAMHACK_DEBUG_SCREEN_GRID()
			ENDIF
			#ENDIF
			
			BEAMHACK_PROCESS_LOOPING_SOUNDS()
			
			sBeamHackGameplayData.iFrameTimeCounter += ROUND(GET_FRAME_TIME() * 1000)
			sBeamHackGameplayData.iUpdateFrames = FLOOR(sBeamHackGameplayData.iFrameTimeCounter / 33.0)
			sBeamHackGameplayData.iFrameTimeCounter = sBeamHackGameplayData.iFrameTimeCounter - 33 * sBeamHackGameplayData.iUpdateFrames
			
			IF IS_BIT_SET(sBeamHackGameplayData.iBS, ciBEAMHACKBS_HITTING_PROXY)
			AND NOT IS_GAME_PAUSED_DUE_TO_FIREWALL_HIT()
				SET_BIT(sBeamHackGameplayData.iBS, ciBEAMHACKBS_UPDATE_BEAM)
			ENDIF
			IF NOT IS_GAME_PAUSED_DUE_TO_FIREWALL_HIT()
				PROCESS_ROTATABLE_NODES()
			ENDIF
			PROCESS_PACKETS(sControllerStruct)
			IF sBeamLevel.iMaxProxies > 0
			AND NOT IS_GAME_PAUSED_DUE_TO_FIREWALL_HIT()
				sBeamHackGameplayData.iProxyAngle = ((NATIVE_TO_INT(GET_NETWORK_TIME()) / ciBH_PROXY_SWITCH_TIME) % ciBH_MAX_PROXY_SIDES) * -45
			ENDIF
			IF IS_BIT_SET(sBeamHackGameplayData.iBS, ciBEAMHACKBS_UPDATE_BEAM)
			AND sBeamHackGameplayData.iPacketsRemaining > 0
				PROCESS_BEAM()
				
				IF sBeamHackGameplayData.iBeamLength <> sBeamHackGameplayData.iBeamLength_Cached
					PLAY_SOUND_FRONTEND(-1, "Hit_Mirror", "dlc_xm_silo_laser_hack_sounds")
					PRINTLN("[BeamHackSnd] Playing bounce sound because beam length has changed!")
					sBeamHackGameplayData.iBeamLength_Cached = sBeamHackGameplayData.iBeamLength
				ENDIF
			ENDIF
			
			IF NOT g_bHideHackingVisuals
				DRAW_LEVEL()
			ENDIF
			
			IF NOT IS_GAME_PAUSED_DUE_TO_FIREWALL_HIT()
				DRAW_CURSOR()
			ENDIF
			PROCESS_BEAMHACK_UI()
			
			IF NOT IS_GAME_PAUSED_DUE_TO_FIREWALL_HIT()
				IF sBeamHackGameplayData.iPacketsRemaining <= 0
					IF NOT HAS_NET_TIMER_STARTED(sBeamHackGameplayData.tdFinalPacketDelayTimer)
						START_NET_TIMER(sBeamHackGameplayData.tdFinalPacketDelayTimer)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sBeamHackGameplayData.tdFinalPacketDelayTimer, ciBH_FINALPACKET_DELAY_TIME)
							HACKING_STORE_END_TIME(sControllerStruct, sBeamHackGameplayData.tdHackTime, sBeamHackGameplayData.iTimeDuration)
							UPDATE_BEAM_HACK_STATE(BEAM_HACK_PASS)
						ENDIF
					ENDIF
				ELIF sBeamHackGameplayData.iLives < 0
				OR HAS_NET_TIMER_EXPIRED(sBeamHackGameplayData.tdHackTime, sBeamHackGameplayData.iTimeDuration)
					HACKING_STORE_END_TIME(sControllerStruct, sBeamHackGameplayData.tdHackTime, sBeamHackGameplayData.iTimeDuration)
					UPDATE_BEAM_HACK_STATE(BEAM_HACK_FAIL)
				ENDIF
			ELSE
				BEAMHACK_RESET_LOOPING_VALUES()
			ENDIF
			IF LOAD_MENU_ASSETS()
				DRAW_MENU_HELP_SCALEFORM(iScreenX)
			ENDIF
		BREAK
		CASE BEAM_HACK_PASS
			IF NOT HAS_NET_TIMER_STARTED(sBeamHackGameplayData.tdBeamEndDelay)
				REINIT_NET_TIMER(sBeamHackGameplayData.tdBeamEndDelay)
				
				PLAY_SOUND_FRONTEND(-1, "Pass", "dlc_xm_silo_laser_hack_sounds")
				PLAY_SOUND_FROM_ENTITY(-1, "Pass_Remote", PLAYER_PED_ID(), "dlc_xm_silo_laser_hack_sounds", TRUE, 30)
				
				BEAMHACK_RESET_LOOPING_VALUES()
			ELSE
				IF HAS_NET_TIMER_EXPIRED(sBeamHackGameplayData.tdBeamEndDelay, ciBH_END_DELAY)
					sBeamHackGameplayData.iHacksComplete++
					IF iRequiredHacks > 1 AND sBeamHackGameplayData.iHacksComplete < iRequiredHacks
						tl23 = "MPBeamHack_lvl"
						tl23 += sBeamHackGameplayData.iLevel
						SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(tl23)
						sBeamHackGameplayData.iLevel++
						IF sBeamHackGameplayData.iLevel >= ciBH_MAX_LEVELS
							sBeamHackGameplayData.iLevel = 1
						ENDIF
						DRAW_SPRITE("MPBeamHack", "pass", 0.5, 0.5, 1.0 * fAspectRatioMod, 1.0, 0.0, sBeamHackGameplayData.rgbaSprite.iR, sBeamHackGameplayData.rgbaSprite.iG, sBeamHackGameplayData.rgbaSprite.iB, sBeamHackGameplayData.rgbaSprite.iA)
						DRAW_BIG_HACKING_TEXT("BEAM_P", 17, 165, 141, 255, 1.75, 1.75, 0.5, 0.4)
						UPDATE_BEAM_HACK_STATE(BEAM_HACK_LOAD_LEVEL)
					ELSE
						UPDATE_BEAM_HACK_STATE(BEAM_HACK_CLEANUP)
						SET_BIT(sControllerStruct.iBS, ciGOHACKBS_PASSED)
					ENDIF
				ELSE
					DRAW_SPRITE("MPBeamHack", "pass", 0.5, 0.5, 1.0 * fAspectRatioMod, 1.0, 0.0, sBeamHackGameplayData.rgbaSprite.iR, sBeamHackGameplayData.rgbaSprite.iG, sBeamHackGameplayData.rgbaSprite.iB, sBeamHackGameplayData.rgbaSprite.iA)
					DRAW_BIG_HACKING_TEXT("BEAM_P", 17, 165, 141, 255, 1.75, 1.75, 0.5, 0.4)
				ENDIF
			ENDIF
		BREAK
		CASE BEAM_HACK_FAIL
			IF NOT HAS_NET_TIMER_STARTED(sBeamHackGameplayData.tdBeamEndDelay)
				REINIT_NET_TIMER(sBeamHackGameplayData.tdBeamEndDelay)
				
				PLAY_SOUND_FRONTEND(-1, "Fail", "dlc_xm_silo_laser_hack_sounds")
				PLAY_SOUND_FROM_ENTITY(-1, "Fail_Remote", PLAYER_PED_ID(), "dlc_xm_silo_laser_hack_sounds", TRUE, 30)
				
				BEAMHACK_RESET_LOOPING_VALUES()
			ELSE
				IF HAS_NET_TIMER_EXPIRED(sBeamHackGameplayData.tdBeamEndDelay, ciBH_END_DELAY)
					SET_BIT(sControllerStruct.iBS, ciGOHACKBS_FAILED)
					UPDATE_BEAM_HACK_STATE(BEAM_HACK_CLEANUP)
				ELSE
					DRAW_SPRITE("MPBeamHack", "fail", 0.5, 0.5, 1.0 * fAspectRatioMod, 1.0, 0.0, sBeamHackGameplayData.rgbaSprite.iR, sBeamHackGameplayData.rgbaSprite.iG, sBeamHackGameplayData.rgbaSprite.iB, sBeamHackGameplayData.rgbaSprite.iA)
					DRAW_BIG_HACKING_TEXT("BEAM_F", 157, 35, 20, 255, 1.75, 1.75, 0.5, 0.4)
				ENDIF
			ENDIF
		BREAK
		CASE BEAM_HACK_CLEANUP
			CLEAN_UP_BEAM_HACK_MINIGAME(sControllerStruct)
		BREAK
		#IF IS_DEBUG_BUILD
		CASE BEAM_HACK_EDITOR
			DRAW_LEVEL_FOR_EDITOR()
			PROCESS_BEAM_HACK_EDITOR()
		BREAK
		#ENDIF
		
	ENDSWITCH
ENDPROC

