// name:        net_scoring_CnC.sch
// description: Common scoring/ranking/cash and xp functions.


USING "NET_XP_FUNC.sch"
USING "PM_MissionCreator_Public.sch"
USING "Transition_Crews.sch"
USING "Commands_Money.sch"



/// PURPOSE:
///    Gets the total cash of playerID
/// PARAMS:
///    playerID - A valid player ID
/// RETURNS:
///    The cash value held by iPlayer
//FUNC INT GET_PLAYER_CnC_CASH(PLAYER_INDEX playerID)
//
//	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
//		SCRIPT_ASSERT("GET_PLAYER_CnC_CASH is called from the wrong mode. Use GET_PLAYER_CASH instead ")
//	ENDIF
//
////BC: 04/07/2012: Commented out all these extra checks as the player was sometimes getting their current cash to be 0. These are not needed now from what I can see. 
//	//BC: Added this check due to a array overrun because playerId was -1
//	IF NATIVE_TO_INT(playerID) > -1
//		IF (playerID = PLAYER_ID())
//			RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_CASH)
//		ELSE	
//			IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
//				RETURN GlobalplayerBD_CNC[NATIVE_TO_INT(playerID)].scoreData.iCash
//			ELSE
//				NET_NL()NET_PRINT("GET_PLAYER_CnC_CASH cannot Get players Cash ")NET_PRINT_INT(NATIVE_TO_INT(playerID))NET_PRINT(" they are not in the GBD slot. ")
//				RETURN 0
//			ENDIF
//		ENDIF
//	ENDIF
//
//	NET_NL()NET_PRINT("GET_PLAYER_CnC_CASH cannot Get players Cash ")NET_PRINT_INT(NATIVE_TO_INT(playerID))NET_PRINT(" their player index is not valid. ")
//	RETURN 0
//	
//ENDFUNC
// 


/// PURPOSE:
/////    Set the XPcashof the local player. (WILL) update online storage. Call after team selection only. Caps invalid iCash values to MIN_CnC_CASH and MAX_CnC_CASH.
///// PARAMS:
/////    iCash - The new cash value for the player.
//PROC SET_LOCAL_PLAYER_CnC_CASH_DO_NOT_USE(INT iCash)
//	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
//		SCRIPT_ASSERT("SET_LOCAL_PLAYER_CnC_CASH is called from the wrong mode. Use SET_LOCAL_PLAYER_CASH instead ")
//	ENDIF
//
//	GlobalplayerBD_CNC[NATIVE_TO_INT(PLAYER_ID())].scoreData.iCash = iCash		
//	#IF IS_DEBUG_BUILD
//		DEBUG_PRINTCALLSTACK()
//		NET_NL()NET_PRINT("SET_LOCAL_PLAYER_CnC_CASH - Setting the player Cash = ")NET_PRINT_INT(iCash)
//	#ENDIF
//	SET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_CASH, iCash)
//ENDPROC

/// PURPOSE:
/////    sets the current cash to the value stored in the tus. Used in game cnc initialisation
//PROC INITIALISE_LOCAL_PLAYER_CNC_CASH()
//
//	SET_LOCAL_PLAYER_CnC_CASH_DO_NOT_USE(GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_CASH))
//ENDPROC

/// PURPOSE:
/////    Give (or take) xp to the local player. Can be +ve or -ve. Updates online storage. Call after team selection only. Half of positive values is given to the Team Cash Stat.
///// PARAMS:
/////    iXPToGive - Value of XP to give.
//PROC GIVE_LOCAL_PLAYER_CnC_CASH(INT iAmount = 0, INT iMultiplier = 1, BOOL bDisplayTicker = TRUE, FLOAT fPercentageToTeamSafe = 10.0)
//	
//	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
//		SCRIPT_ASSERT("GIVE_LOCAL_PLAYER_CnC_CASH is called from the wrong mode. Use GIVE_LOCAL_PLAYER_CASH instead ")
//	ENDIF
//	
//	INT iCash
//	INT iPlayerCash 
//	INT iTeamCash
//	
//	/*
//	TEXT_LABEL_7 tl
//	PRINTSTRING(" start of GIVE_LOCAL_PLAYER_CnC_CASH stack: ")
//	tl = GET_CURRENT_STACK_SIZE()
//	PRINTSTRING(tl)
//	PRINTNL()
//	*/
//	
//	IF iMultiplier < 1
//		iMultiplier = 1
//	ENDIF
//	//Check if the scripter has passed in an amount
//	//IF iOverrideAmount = 0
//	//   iCash = GET_INT_FROM_CASH_XP_ENUM(CashXpType)*iMultiplier
//	//ELSE
//		iCash = iAmount*iMultiplier			
//	//ENDIF
//	
//	// KGM 24/7/12: The amount of cash now gets a SCRIPT TUNABLE modifier applied
//	// (A SCRIPT TUNABLE is a global variable with a value that can be optionally modified by the cloud allowing us to tweak variables post-release)
//	IF iCash > 0
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("...KGM MP: GIVE_LOCAL_PLAYER_CnC_CASH() - Applying CASH_MULTIPLIER script tunable, function called by: ")
//			NET_PRINT(GET_THIS_SCRIPT_NAME())
//			NET_NL()
//			NET_PRINT("           Original Cash Value: ") NET_PRINT_INT(iCash)
//			NET_PRINT("   CASH_MULTIPLIER: ") NET_PRINT_FLOAT(g_sMPTunables.cashMultiplier)
//		#ENDIF
//		FLOAT modifiedCashAsFloat = TO_FLOAT(iCash) * g_sMPTunables.cashMultiplier
//		iCash = ROUND(modifiedCashAsFloat)
//		#IF IS_DEBUG_BUILD
//			NET_PRINT(" GIVE_LOCAL_PLAYER_CnC_CASH:  Modified Cash Value: ") NET_PRINT_INT(iCash)
//			NET_NL()
//		#ENDIF
//	ELSE
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("  GIVE_LOCAL_PLAYER_CnC_CASH: DO NOT MODIFY Cash Value: ") NET_PRINT_INT(iCash)
//			NET_NL()
//		#ENDIF
//	ENDIF
//	// END SCRIPT TUNABLE modification
//
//	
//	//Check fPercentageToTeamSafe is okay 
//	IF fPercentageToTeamSafe > 100
//		fPercentageToTeamSafe = 100
//		NET_SCRIPT_ASSERT("GIVE_LOCAL_PLAYER_CnC_CASH_END_OF_MISSION - fPercentageToTeamSafe > 100")
//	ELIF fPercentageToTeamSafe < 0
//		fPercentageToTeamSafe = 0
//		NET_SCRIPT_ASSERT("GIVE_LOCAL_PLAYER_CnC_CASH_END_OF_MISSION - fPercentageToTeamSafe < 0")
//	ENDIF
//	
//	//Check if Cash going to Team Safe has been unlocked by the Flow
////	IF NOT CNC_FLOW_Get_flow_Flag_State(ciCNC_FLOW_CASH_TUTORIAL_COMPLETED)
////		fPercentageToTeamSafe = 0
////		NET_PRINT(" -- GIVE_LOCAL_PLAYER_CnC_CASH() - ciCNC_FLOW_CASH_TUTORIAL_COMPLETED = FALSE - fPercentageToTeamSafe = 0") NET_NL()
////	ENDIF
//	
//	//Team Cash
//	IF iCash > 0
//		IF fPercentageToTeamSafe > 0
//			iTeamCash = CEIL(iCash * (fPercentageToTeamSafe/100))
//			BROADCAST_CHANGE_SESSION_STAT(SESSION_STAT_GANG_CASH,iTeamCash,0,GET_PLAYER_TEAM(PLAYER_ID()))
//			NET_PRINT(" -- GIVE_LOCAL_PLAYER_CnC_CASH() - CASH SHARE TO GANG SAFE = $") NET_PRINT_INT(iTeamCash) NET_NL()
//		ENDIF
//	ENDIF
//	
//	//Player Cash
//	IF iCash > 0
//		FLOAT iPercentageToPlayer = (100 - fPercentageToTeamSafe)
//		iPlayerCash = FLOOR(iCash * (iPercentageToPlayer/100))
//	ELSE
//		iPlayerCash = iCash
//	ENDIF
//	NET_NL()NET_PRINT(" -- GIVE_LOCAL_PLAYER_CnC_CASH() - Players Current Cash = $")NET_PRINT_INT(GET_PLAYER_CnC_CASH(PLAYER_ID()))NET_NL()
//	NET_NL()NET_PRINT(" -- GIVE_LOCAL_PLAYER_CnC_CASH() - Cash to give = $")NET_PRINT_INT(iPlayerCash)
//	SET_LOCAL_PLAYER_CnC_CASH_DO_NOT_USE(GET_PLAYER_CnC_CASH(PLAYER_ID()) + iPlayerCash)
//	GlobalplayerBD_CNC[NATIVE_TO_INT(PLAYER_ID())].scoreData.iLastCashReward = iPlayerCash
//	INCREMENT_BY_MP_FLOAT_CHARACTER_STAT(MP_STAT_MONEY_MADE_FROM_MISSIONS,TO_FLOAT(iPlayerCash) )
//	IF bDisplayTicker = TRUE
//		PRINT_CASH_TICKER(iPlayerCash, IS_PLAYER_COP(PLAYER_ID()))
//	ENDIF
//ENDPROC
//
/// PURPOSE:
///    Give (or take) xp to the local player. Can be +ve or -ve. Updates online storage. Call after team selection only.
/// PARAMS:
///    iXPToGive - Value of XP to give.
//PROC GIVE_LOCAL_PLAYER_CnC_CASH_END_OF_MISSION(INT iAmount = 0, INT iMultiplier = 1, BOOL bDisplayTicker = TRUE, FLOAT fPercentageToTeamSafe = 10.0)
//	
//	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
//		SCRIPT_ASSERT("GIVE_LOCAL_PLAYER_CnC_CASH_END_OF_MISSION is called from the wrong mode. Use GIVE_LOCAL_PLAYER_CASH_END_OF_MISSION instead ")
//	ENDIF
//	// KGM 24/7/12: This is currently identical to the normal 'give cash' function so I'm going to re-direct to there. We'll leave this function in case they separate again in future.
//	GIVE_LOCAL_PLAYER_CnC_CASH(iAmount, iMultiplier, bDisplayTicker, fPercentageToTeamSafe)
//ENDPROC

// ---------------------- XP ----------------------------------






//
//
///// PURPOSE:
/////    sets the current cash to the value stored in the tus. Used in game cnc initialisation
//PROC INITIALISE_LOCAL_PLAYER_CNC_XP()
//	SET_LOCAL_PLAYER_CnC_XP_DO_NOT_USE(GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_XP))
//ENDPROC


FUNC INT GET_PLAYER_RANK(PLAYER_INDEX aPlayer)
	//RETURN GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(aPlayer, bRunQuicklaunchCheck), bRunQuicklaunchCheck)
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(aPlayer)].scoreData.iRank  // did this for optimisations - it gets set in MAINTAIN_PLAYERS_GLOBAL_BROADCAST_DATA()
ENDFUNC

PROC REFRESH_LOCAL_PLAYER_RANK(INT ISlot = -1)
//	INT Rank = GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID(), FALSE), FALSE)
//	SET_LOCAL_PLAYER_RANK(Rank)
		
	INT FMRANK = GET_FM_RANK_FROM_XP_VALUE(GET_STAT_CHARACTER_FM_XP(ISlot), TRUE)
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_RANK_FM, FMRANK, ISlot)

	SET_MP_INT_CHARACTER_STAT(MP_STAT_NEXT_RANKXP_FM, GET_XP_NEEDED_FOR_FM_RANK(FMRANK+1, FALSE, TRUE),ISlot)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_PREV_RANKXP_FM, GET_XP_NEEDED_FOR_FM_RANK(FMRANK, FALSE, TRUE), ISlot )
	
	SET_MP_INT_PLAYER_STAT(MPPLY_CURRENT_CREW_RANK, GET_RANK_FROM_XP_VALUE(GET_PLAYER_LOCAL_CREW_XP_VALUE_FROM_PLAYER_ID(PLAYER_ID())))
	//SET_MP_INT_PLAYER_STAT(MPPLY_NEXT_CREW_RANK, GET_MP_INT_PLAYER_STAT(MPPLY_CURRENT_CREW_RANK) + 1)
	
	
ENDPROC

PROC REFRESH_LOCAL_PLAYER_RANK_TRANSITION(INT ISlot = -1)

	//Need to do this before FM is running, can't use BD data. 
	INT FMRANK = GET_FM_RANK_FROM_XP_VALUE(GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_XP_FM, iSLOT, TRUE), TRUE)
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_RANK_FM, FMRANK, ISlot)

	SET_MP_INT_CHARACTER_STAT(MP_STAT_NEXT_RANKXP_FM, GET_XP_NEEDED_FOR_FM_RANK(FMRANK+1, FALSE, TRUE),ISlot)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_PREV_RANKXP_FM, GET_XP_NEEDED_FOR_FM_RANK(FMRANK, FALSE, TRUE), ISlot )
	
	SET_MP_INT_PLAYER_STAT(MPPLY_CURRENT_CREW_RANK, GET_RANK_FROM_XP_VALUE(GET_PLAYER_LOCAL_CREW_XP_VALUE_FROM_PLAYER_ID(PLAYER_ID())))
	//SET_MP_INT_PLAYER_STAT(MPPLY_NEXT_CREW_RANK, GET_MP_INT_PLAYER_STAT(MPPLY_CURRENT_CREW_RANK) + 1)
	
	
ENDPROC



FUNC TEXT_LABEL_63 RETURN_PERSONAL_RANK_STRING(INT iRank, INT iTeam)
TEXT_LABEL_63 rankName
	rankName = "RANK_G"
	IF iTeam = TEAM_FREEMODE
	OR iTeam = TEAM_SCTV
		rankName += "fm"
	ELSE
		rankName += iTeam
	ENDIF
	rankName += "_"
	rankName += GET_RANK_AS_NUMBER_FROM_0_TO_20(iRank)
	RETURN rankName
ENDFUNC





/// PURPOSE:
///    Gets the text label of the players current rank
/// PARAMS:
///    iRank - Int value of rank to get name of
///    bCop - Bool true if name is cop rank
/// RETURNS:
///    The name of the specified rank
FUNC TEXT_LABEL_63 GET_RANK_TEXTLABEL(INT iRank, INT iTeam, BOOL UsePlayerCrewXPForCrewRank = TRUE, MULTIPLAYER_SETTING_TITLE eTypeOverride = -1)
	TEXT_LABEL_63 rankName
	
	MULTIPLAYER_SETTING_TITLE eSetting = GET_MP_CHARACTER_TITLE_SETTING()
	
	IF eTypeOverride != INT_TO_ENUM(MULTIPLAYER_SETTING_TITLE, -1)
		eSetting = eTypeOverride
	ENDIF
	
	INT CutDownRank
	INT CrewRank
	SWITCH eSetting
		CASE MP_SETTING_TITLE_CREW
			
			IF UsePlayerCrewXPForCrewRank
				CrewRank = GET_RANK_FROM_XP_VALUE(GET_PLAYER_LOCAL_CREW_XP_VALUE_FROM_PLAYER_ID(PLAYER_ID()))
				iRank = CrewRank
			ENDIF
			IF iRank > 999
				iRank = 999
			ENDIF
			CutDownRank = FLOOR(TO_FLOAT(iRank)/10.0)
			
			
			//Return the normal gang one if the string is empty. 
			rankName = g_Private_LocalPlayerCrew_RankTitle[CutDownRank]
			
			
			RETURN rankName
			 
		BREAK
		
		CASE MP_SETTING_TITLE_PERSONAL
			RETURN RETURN_PERSONAL_RANK_STRING(iRank, iTeam)
			
		BREAK
	
	ENDSWITCH
		
	RETURN rankName
		

ENDFUNC







///// PURPOSE:
/////    Get the rank of a specific player.
///// PARAMS:
/////    playerID - A valid player ID.
///// RETURNS:
/////    The rank of iPlayer. Cap at MAX_CnC_RANK
//FUNC INT GET_PLAYER_CnC_RANK( PLAYER_INDEX playerID) 
//	
//	#IF IS_DEBUG_BUILD
//	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
//		SCRIPT_ASSERT("GET_PLAYER_CnC_RANK is called from the wrong mode. Use GET_PLAYER_RANK instead ")
//	ENDIF
//	#ENDIF
//	
//	INT iXP
//	
//	iXP = GET_PLAYER_CNC_XP(playerID)
//		
//	IF iXP < 0
//		RETURN 0
//	ENDIF
//	
//	RETURN GET_CNC_RANK_FROM_XP_VALUE(iXP)
//	
//ENDFUNC




//FREEMODE!!




/// PURPOSE:
///    Get the rank of a specific player.
/// PARAMS:
///    playerID - A valid player ID.
/// RETURNS:
///    The rank of iPlayer. Cap at MAX_CnC_RANK
FUNC INT GET_PLAYER_FM_RANK( PLAYER_INDEX playerID)

	INT iXP = GET_PLAYER_FM_XP(playerID)
	IF iXP < 0
		RETURN 0
	ENDIF
	RETURN GET_FM_RANK_FROM_XP_VALUE(iXP)
ENDFUNC


/// PURPOSE:
///    Gets the local player's VC amount
/// PARAMS:
///    bIncludeBank - includes the players bank account value
/// RETURNS:
///    The player's VC amount
FUNC INT GET_LOCAL_PLAYER_VC_AMOUNT(BOOL bIncludeBank)
	//If the player has more than SCRIPT_MAX_INT32 in cash, then this function may overflow and return a negative value. However, both NETWORK_GET_VC_BANK_BALANCE
	//and NETWORK_GET_VC_WALLET_BALANCE appear to be capped at SCRIPT_MAX_INT32. Therefore, it is only when this function is called with TRUE that it may overflow,
	//as an addition is attempted. NETWORK_CAN_SPEND_MONEY can be used to catch values over SCRIPT_MAX_INT32 by ensuring the player can still spend cash, but in
	//cases where GET_LOCAL_PLAYER_VC_AMOUNT is called with FALSE, it is not necessary.
	INT iAmount
	IF bIncludeBank
		iAmount = NETWORK_GET_VC_BANK_BALANCE()
	ENDIF
	iAmount += NETWORK_GET_VC_WALLET_BALANCE()
	RETURN iAmount
ENDFUNC


/// PURPOSE:
///    To get the split of money to take from bank versus wallet for purchasing an item (Bank is taken first)
/// PARAMS:
///    iAmountToTake - the amount to take
///    BankAmount - the amount to take from bank
///    WalletAmount - the amount to take from cash/
/// RETURNS:
///    True is all the money can be taken from bank
FUNC BOOL GET_VC_CASH_SPLIT(INT iAmountToTake, INT& iBankAmount, INT& iWalletAmount)
	IF NETWORK_GET_VC_BANK_BALANCE() > iAmountToTake
		iBankAmount = iAmountToTake
		RETURN TRUE
	ELSE
		iBankAmount = NETWORK_GET_VC_BANK_BALANCE()
		iWalletAmount = iAmountToTake - iBankAmount
	ENDIF
	RETURN FALSE
ENDFUNC




/// PURPOSE:
///    Gets the total cash of playerID
/// PARAMS:
///    playerID - A valid player ID
/// RETURNS:
///    The cash value held by iPlayer
FUNC INT GET_PLAYER_FM_CASH(PLAYER_INDEX playerID)


//BC: 04/07/2012: Commented out all these extra checks as the player was sometimes getting their current cash to be 0. These are not needed now from what I can see. 
//	IF (g_TransitionData.iTransitionState = TRANSITION_STATE_INACTIVE)
		//BC: Added this check due to a array overrun because playerId was -1
	IF NATIVE_TO_INT(playerID) > -1
		IF (playerID = PLAYER_ID())
			RETURN NETWORK_GET_VC_WALLET_BALANCE()
		ELSE	
			IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
				RETURN GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].scoreData.iCash
			ELSE
				NET_NL()NET_PRINT("GET_PLAYER_FM_CASH cannot Get players Cash ")NET_PRINT_INT(NATIVE_TO_INT(playerID))NET_PRINT(" they are not in the GBD slot. ")
				RETURN 0
			ENDIF
		ENDIF
	ENDIF

	NET_NL()NET_PRINT("GET_PLAYER_FM_CASH cannot Get players Cash ")NET_PRINT_INT(NATIVE_TO_INT(playerID))NET_PRINT(" their player index is not valid. ")
	RETURN 0
	
ENDFUNC
 


/// PURPOSE:
///    Gets the total cash of playerID
/// PARAMS:
///    playerID - A valid player ID
/// RETURNS:
///    The cash value held by iPlayer
FUNC INT GET_PLAYER_CASH(PLAYER_INDEX playerID)
	INT Result
	
	#IF IS_DEBUG_BUILD
	IF GET_QUICKLAUNCH_STATE() != QUICKLAUNCH_EMPTY
		RETURN Result
	ENDIF
	#ENDIF
	
	Result = 	GET_PLAYER_FM_CASH(playerID)

	RETURN Result
	
ENDFUNC

ENUM CASH_TUNABLE_ENUM
	eNoCashTunable,
	eStandardCashTunable
ENDENUM

//PURPOSE: Multiplies the passed in Cash value by the Tunable set by the enum.
FUNC INT MULTIPLY_CASH_BY_TUNABLE(INT iCash, CASH_TUNABLE_ENUM eCashTunable = eStandardCashTunable)
	FLOAT modifiedCashAsFloat
	
	NET_PRINT("  --> MULTIPLY_CASH_BY_TUNABLE() - Cash = ")NET_PRINT_INT(iCash)NET_NL()
	
	SWITCH eCashTunable
		CASE eNoCashTunable
			#IF IS_DEBUG_BUILD DEBUG_PRINTCALLSTACK() NET_PRINT("  --> MULTIPLY_CASH_BY_TUNABLE() - eNoCashTunable - Cash = ") NET_PRINT_INT(iCash) NET_NL() #ENDIF
		BREAK
		
		CASE eStandardCashTunable
			IF iCash > 0
				#IF IS_DEBUG_BUILD 
				NET_PRINT("  --> MULTIPLY_CASH_BY_TUNABLE() - Applying STANDARD script tunable - Function called by: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
				NET_PRINT("  --> MULTIPLY_CASH_BY_TUNABLE() - Original Cash Value: ") NET_PRINT_INT(iCash) NET_PRINT("   Multiplier: ") NET_PRINT_FLOAT(g_sMPTunables.cashMultiplier) NET_NL()
				#ENDIF
				
				modifiedCashAsFloat = TO_FLOAT(iCash) * g_sMPTunables.cashMultiplier
				iCash = ROUND(modifiedCashAsFloat)
				
				#IF IS_DEBUG_BUILD DEBUG_PRINTCALLSTACK() NET_PRINT("  --> MULTIPLY_CASH_BY_TUNABLE() - MODIFIED CASH VALUE - Cash = ") NET_PRINT_INT(iCash) NET_NL() #ENDIF
			ELSE
				#IF IS_DEBUG_BUILD DEBUG_PRINTCALLSTACK() NET_PRINT("  --> MULTIPLY_CASH_BY_TUNABLE() - DO NOT MODIFY CASH VALUE - Cash = ") NET_PRINT_INT(iCash) NET_NL() #ENDIF
			ENDIF
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD DEBUG_PRINTCALLSTACK() NET_PRINT("  --> MULTIPLY_CASH_BY_TUNABLE() - eCashTunable NOT VALID - Cash = ") NET_PRINT_INT(iCash) NET_NL() #ENDIF
		BREAK
	ENDSWITCH
	
	RETURN iCash
ENDFUNC

/// PURPOSE:
///    Give (or take) xp to the local player. Can be +ve or -ve. Updates online storage. Call after team selection only. Half of positive values is given to the Team Cash Stat.
/// PARAMS:
///    iXPToGive - Value of XP to give.
PROC GIVE_LOCAL_PLAYER_FM_CASH(INT iAmount = 0, INT iMultiplier = 1, BOOL bDisplayTicker = TRUE, FLOAT fPercentageToTeamSafe = 0.0)
	
	INT iCash
	INT iPlayerCash 
	
	/*
	TEXT_LABEL_7 tl
	PRINTSTRING(" start of GIVE_LOCAL_PLAYER_CnC_CASH stack: ")
	tl = GET_CURRENT_STACK_SIZE()
	PRINTSTRING(tl)
	PRINTNL()
	*/
	
	IF iMultiplier < 1
		iMultiplier = 1
	ENDIF
	//Check if the scripter has passed in an amount
	//IF iOverrideAmount = 0
	//   iCash = GET_INT_FROM_CASH_XP_ENUM(CashXpType)*iMultiplier
	//ELSE
		iCash = iAmount*iMultiplier			
	//ENDIF
	
	// KGM 24/7/12: The amount of cash now gets a SCRIPT TUNABLE modifier applied
	// (A SCRIPT TUNABLE is a global variable with a value that can be optionally modified by the cloud allowing us to tweak variables post-release)
	/*IF iCash > 0
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP: GIVE_LOCAL_PLAYER_FM_CASH() - Applying CASH_MULTIPLIER script tunable, function called by: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			NET_PRINT("           Original Cash Value: ") NET_PRINT_INT(iCash)
			NET_PRINT("   CASH_MULTIPLIER: ") NET_PRINT_FLOAT(g_sMPTunables.cashMultiplier)
		#ENDIF
		FLOAT modifiedCashAsFloat = TO_FLOAT(iCash) * g_sMPTunables.cashMultiplier
		iCash = ROUND(modifiedCashAsFloat)
		#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			NET_PRINT("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ") 
			NET_PRINT("!!!!!!!!!!!!!!!!!!!!!!GIVE_LOCAL_PLAYER_FM_CASH:   MODIFIED CASH VALUES: ") NET_PRINT_INT(iCash)
			NET_PRINT("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ") 
			NET_NL()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			NET_PRINT("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ") 
			NET_PRINT("!!!!!!!!!!!!!!!!!!!!!!GIVE_LOCAL_PLAYER_FM_CASH:   DO NOT MODIFY CASH VALUES: ") NET_PRINT_INT(iCash)
			NET_PRINT("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ") 
			NET_NL()
		#ENDIF
	ENDIF*/
	// END SCRIPT TUNABLE modification

	
	//Check fPercentageToTeamSafe is okay 
	/*IF fPercentageToTeamSafe > 100
		fPercentageToTeamSafe = 100
		NET_SCRIPT_ASSERT("GIVE_LOCAL_PLAYER_FM_CASH - fPercentageToTeamSafe > 100")
	ELIF fPercentageToTeamSafe < 0
		fPercentageToTeamSafe = 0
		NET_SCRIPT_ASSERT("GIVE_LOCAL_PLAYER_FM_CASH - fPercentageToTeamSafe < 0")
	ENDIF*/
	//NEVER GIVE SHARE TO TEAM FOR FREEMODE
	fPercentageToTeamSafe = 0
	
	//Player Cash
	IF iCash > 0
		FLOAT iPercentageToPlayer = (100 - fPercentageToTeamSafe)
		iPlayerCash = FLOOR(iCash * (iPercentageToPlayer/100))
	ELSE
		iPlayerCash = iCash
	ENDIF
	//NET_NL()NET_PRINT(" -- GIVE_LOCAL_PLAYER_FM_CASH() - Players Current Cash = $")NET_PRINT_INT(GET_PLAYER_FM_CASH(PLAYER_ID()))NET_NL()
	NET_NL()NET_PRINT(" -- GIVE_LOCAL_PLAYER_FM_CASH() - Cash to give = $")NET_PRINT_INT(iPlayerCash)
	//SET_LOCAL_PLAYER_FM_CASH_DO_NOT_USE(GET_PLAYER_FM_CASH(PLAYER_ID()) + iPlayerCash)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].scoreData.iLastCashReward = iPlayerCash
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].scoreData.iCash += iPlayerCash
	//INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_MADE_FROM_MISSIONS,iPlayerCash )
	IF bDisplayTicker = TRUE
		PRINT_CASH_TICKER(iPlayerCash, FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Give (or take) xp to the local player. Can be +ve or -ve. Updates online storage. Call after team selection only.
/// PARAMS:
///    iXPToGive - Value of XP to give.
PROC GIVE_LOCAL_PLAYER_FM_CASH_END_OF_MISSION(INT iAmount = 0, INT iMultiplier = 1, BOOL bDisplayTicker = TRUE, FLOAT fPercentageToTeamSafe = 0.0)
	
	
	// KGM 24/7/12: This is currently identical to the normal 'give cash' function so I'm going to re-direct to there. We'll leave this function in case they separate again in future.
	GIVE_LOCAL_PLAYER_FM_CASH(iAmount, iMultiplier, bDisplayTicker, fPercentageToTeamSafe)
ENDPROC



// ---------------------- XP ----------------------------------







/// PURPOSE:
///    sets the current cash to the value stored in the tus. Used in game cnc initialisation
PROC INITIALISE_LOCAL_PLAYER_FM_XP()
	INT I
	FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
		g_Private_CHARACTER_TEAM[I] = GET_PACKED_STAT_INT(PACKED_CHAR_TEAM, I)
	ENDFOR
	#IF IS_DEBUG_BUILD
	IF g_Private_ACTIVE_CHARACTER != GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR)
		NET_NL()NET_PRINT("[SET_ACTIVE_CHARACTER_SLOT] 1 g_Private_ACTIVE_CHARACTER = ")NET_PRINT_INT(g_Private_ACTIVE_CHARACTER)
	ENDIF
	#ENDIF
	g_Private_ACTIVE_CHARACTER = GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR)

	//Reverted back to using the stat as when you delete the character Neil's global was giving the old XP value back. 
	
	BOOL HaveHeavilyAccessedStatsBeenInitialised = g_bHeavilyAccessedStatsInitialised
	
	IF HaveHeavilyAccessedStatsBeenInitialised
		g_bHeavilyAccessedStatsInitialised = FALSE
	ENDIF
	
	PLAYSTATS_AWARD_XP(GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_XP_FM, -1, TRUE), ENUM_TO_INT(XPTYPE_INITIALISE), ENUM_TO_INT(XPCATEGORY_INITIALISE_RP))			
	SET_LOCAL_PLAYER_FM_XP_DO_NOT_USE((GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_XP_FM, -1, TRUE)), XPCATEGORY_INITIALISE_RP)
	
	g_bHeavilyAccessedStatsInitialised = HaveHeavilyAccessedStatsBeenInitialised
		

ENDPROC


PROC REFRESH_LOCAL_PLAYER_FM_RANK()
	INT Rank = GET_FM_RANK_FROM_XP_VALUE(GET_PLAYER_FM_XP(PLAYER_ID()))
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_RANK_FM, Rank)
ENDPROC



/// PURPOSE:
///    Give (or take) xp to the local player. Can be +ve or -ve. Updates online storage. Call after team selection only. Also returns the amnount of xp to give.
/// PARAMS:
///    iXPToGive - Value of XP to give.
FUNC INT GIVE_LOCAL_PLAYER_FM_XP(XP_TYPE eXpType, STRING tl_23XpType, XPTYPE anXPType, XPCATEGORY anXPCategory, INT iOverrideAmount = 0, INT iMultiplier = 1, INT iGlobalXPValue = -1, BOOL bDoFreeAimBonus = FALSE)
	
	
	INT RESULT

	
	#IF IS_DEBUG_BUILD
		NET_NL()
		NET_PRINT("GIVE_LOCAL_PLAYER_FM_XP ")
	#ENDIF
	
	RESULT = PRIVATE_GIVE_PLAYER_XP(eXpType, GAMEMODE_FM, tl_23XpType, iOverrideAmount, iMultiplier, FALSE, iGlobalXPValue, TRUE, anXPType, anXPCategory, bDoFreeAimBonus)
	RETURN RESULT
	
	
ENDFUNC


FUNC INT GIVE_LOCAL_PLAYER_FM_XP_WITH_ANIMATION(XP_TYPE eXpType, ENTITY_INDEX xpEntityID, STRING tl_23XpType, XPTYPE anXPType, XPCATEGORY anXPCategory, INT iOverrideAmount = 0, INT iMultiplier = 1, INT iGlobalXPValue = -1, STRING stXPDesc = NULL, BOOL bDoFreeAimBonus = FALSE, INT iDisplayDurationMS = 0)

	

	INT iXPValue = GIVE_LOCAL_PLAYER_FM_XP(eXpType, tl_23XpType,anXPType,anXPCategory, iOverrideAmount, iMultiplier, iGlobalXPValue, bDoFreeAimBonus)
	
	#IF IS_DEBUG_BUILD
		IF bDisableXPAnimation
			RETURN iXPValue
		ENDIF
	#ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_SimulateSinglePlayerHUD)
		RETURN iXPValue
	ENDIF
	
//	IF anXPCategory != XPCATEGORY_ACTION_HEADSHOTS		//#1627008
//		
//		//additional output for headshot!
//		IF DOES_ENTITY_EXIST(xpEntityID)
//			IF IS_ENTITY_A_PED(xpEntityID)
//				
//				PED_INDEX xpPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(xpEntityID)
//				
//				IF IS_PED_DEAD_OR_DYING(xpPedID, FALSE)
//				
//					IF NOT (WAS_PED_KILLED_BY_STEALTH(xpPedID) OR WAS_PED_KILLED_BY_TAKEDOWN(xpPedID))
//						
//						SWITCH GET_PED_CAUSE_OF_DEATH(xpPedID)
//							CASE WEAPONTYPE_PISTOL
//							CASE WEAPONTYPE_COMBATPISTOL
//							CASE WEAPONTYPE_APPISTOL
//							
//							CASE WEAPONTYPE_MICROSMG
//							CASE WEAPONTYPE_SMG
//							
//							CASE WEAPONTYPE_ASSAULTRIFLE
//							CASE WEAPONTYPE_CARBINERIFLE
//							CASE WEAPONTYPE_ADVANCEDRIFLE
//							
//							CASE WEAPONTYPE_MG
//							CASE WEAPONTYPE_COMBATMG
//							
//							CASE WEAPONTYPE_PUMPSHOTGUN
//							CASE WEAPONTYPE_SAWNOFFSHOTGUN
//							CASE WEAPONTYPE_ASSAULTSHOTGUN
//							
//							CASE WEAPONTYPE_SNIPERRIFLE
//							CASE WEAPONTYPE_HEAVYSNIPER
//							CASE WEAPONTYPE_REMOTESNIPER
//							
//		//					CASE WEAPONTYPE_GRENADELAUNCHER
//		//					CASE WEAPONTYPE_GRENADELAUNCHER_SMOKE
//		//					CASE WEAPONTYPE_RPG
//		//					CASE WEAPONTYPE_MINIGUN
//		//					CASE WEAPONTYPE_STINGER
//							
//		//					CASE WEAPONTYPE_GRENADE
//		//					CASE WEAPONTYPE_SMOKEGRENADE
//		//					CASE WEAPONTYPE_BZGAS
//		//					CASE WEAPONTYPE_STICKYBOMB
//		//					CASE WEAPONTYPE_MOLOTOV
//		//					CASE WEAPONTYPE_BALL
//		//					CASE WEAPONTYPE_FLARE
//							
//		//					CASE WEAPONTYPE_STUNGUN
//		//					CASE WEAPONTYPE_FIREEXTINGUISHER
//		//					CASE WEAPONTYPE_PETROLCAN
//		//					CASE WEAPONTYPE_DIGISCANNER
//		//					CASE WEAPONTYPE_AIRSTRIKE_ROCKET
//		//					CASE WEAPONTYPE_WATER_CANNON
//							
//		//					CASE WEAPONTYPE_ELECTRIC_FENCE
//							
//		//					CASE WEAPONTYPE_KNIFE
//		//					CASE WEAPONTYPE_NIGHTSTICK
//		//					CASE WEAPONTYPE_HAMMER
//		//					CASE WEAPONTYPE_BAT
//		//					CASE WEAPONTYPE_CROWBAR
//		//					CASE WEAPONTYPE_GOLFCLUB
//							
//		//					CASE WEAPONTYPE_VEHICLE_WEAPON_TANK				    			
//		//					CASE WEAPONTYPE_VEHICLE_SPACE_ROCKET				    		
//		//					CASE WEAPONTYPE_VEHICLE_PLAYER_LASER				    		
//		//					CASE WEAPONTYPE_VEHICLE_PLAYER_BULLET				    		
//		//					CASE WEAPONTYPE_VEHICLE_ROTORS				    				
//		//					CASE WEAPONTYPE_VEHICLE_PLAYER_BUZZARD				    		
//		//					CASE WEAPONTYPE_PASSENGER_ROCKET				    			
//		//					CASE WEAPONTYPE_VEHICLE_ROCKET				    				
//							
//		//					CASE WEAPONTYPE_OBJECT				    				
//		//					CASE WEAPONTYPE_BRIEFCASE				    			
//		//					CASE WEAPONTYPE_BRIEFCASE_02				    		
//							
//		//					CASE GADGETTYPE_PARACHUTE				    			
//							
//		//					CASE WEAPONTYPE_AMMO_RPG				    			
//		//					CASE WEAPONTYPE_AMMO_TANK				    			
//		//					CASE WEAPONTYPE_AMMO_SPACE_ROCKET				    	
//		//					CASE WEAPONTYPE_AMMO_PLAYER_LASER				    	
//		//					CASE WEAPONTYPE_AMMO_ENEMY_LASER				    	
//		//					CASE WEAPONTYPE_AMMO_GRENADE_LAUNCHER				    
//		//					CASE WEAPONTYPE_AMMO_GRENADE_LAUNCHER_SMOKE				
//							
//		//					CASE WEAPONTYPE_RAMMEDBYVEHICLE				    		
//		//					CASE WEAPONTYPE_RUNOVERBYVEHICLE				    	
//							
//		//					CASE WEAPONTYPE_EXPLOSION				    			
//							
//		//					CASE WEAPONTYPE_FALL				    				    
//							
//							CASE WEAPONTYPE_DLC_PISTOL50				    			
//							CASE WEAPONTYPE_DLC_ASSAULTSMG				    			
//							CASE WEAPONTYPE_DLC_HEAVYRIFLE				    			
//							CASE WEAPONTYPE_DLC_BULLPUPSHOTGUN				    		
//							CASE WEAPONTYPE_DLC_ASSAULTMG				    			
//							CASE WEAPONTYPE_DLC_ASSAULTSNIPER				    		
//		//					CASE WEAPONTYPE_DLC_PROGRAMMABLEAR				    		
//		//					CASE WEAPONTYPE_DLC_RUBBERGUN				    			
//		//					CASE WEAPONTYPE_DLC_LOUDHAILER				    			
//								PED_BONETAG entityDamageBonetag
//								IF GET_PED_LAST_DAMAGE_BONE(xpPedID, entityDamageBonetag)
//									IF entityDamageBonetag = BONETAG_HEAD
//										TEXT_LABEL_31 tlName
//										BOOL bLiteralString
//										
//										tlName = "HEADSHOT"				//"XP_HEADSHOT"
//										bLiteralString = TRUE			//FALSE
//										
//					//					SCRIPT_ASSERT(tlName)
//					//					SAVE_STRING_TO_DEBUG_FILE("\"")
//					//					SAVE_STRING_TO_DEBUG_FILE(tlName)
//					//					SAVE_STRING_TO_DEBUG_FILE("\"")
//					//					SAVE_NEWLINE_TO_DEBUG_FILE()
//										
//										STORE_ANIMATION_WITH_STRING(GET_PED_BONE_COORDS(xpPedID, BONETAG_HEAD, <<0,0,0>>), tlName, bLiteralString)
//									ENDIF
//								ENDIF
//							BREAK
//							
//							DEFAULT
//								//
//							BREAK
//						ENDSWITCH
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
	
	IF anXPCategory = XPCATEGORY_COMPLETED_SHOWER OR anXPCategory = XPCATEGORY_COMPLETED_ROLLERCOASTER
		
		//special output for shower!
		IF DOES_ENTITY_EXIST(xpEntityID)
			IF IS_ENTITY_A_PED(xpEntityID)
				PED_INDEX xpPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(xpEntityID)
		
				STORE_XP_ANIMATION(GET_PED_BONE_COORDS(xpPedID, BONETAG_HEAD, <<0,0,0>>), iXPValue, HUD_ANIM_XP, stXPDesc, iDisplayDurationMS)
			ENDIF
		ENDIF
	ELSE
		SET_XP_REWARD_FOR_ENTITY(xpEntityID, iXPValue, stXPDesc, iDisplayDurationMS)
	ENDIF
	
	RETURN iXPValue
	
ENDFUNC

FUNC INT GIVE_LOCAL_PLAYER_FM_XP_WITH_ANIM_AT_VECTOR(XP_TYPE eXpType, VECTOR vPosition, STRING tl_23XpType, XPTYPE anXPType, XPCATEGORY anXPCategory, INT iOverrideAmount = 0, INT iMultiplier = 1, INT iGlobalXPValue = -1, BOOL bDoFreeAimBonus = FALSE)

	

	INT iXPValue = GIVE_LOCAL_PLAYER_FM_XP(eXpType, tl_23XpType,  anXPType, anXPCategory, iOverrideAmount, iMultiplier, iGlobalXPValue, bDoFreeAimBonus)
	
	#IF IS_DEBUG_BUILD
		IF bDisableXPAnimation
			RETURN iXPValue
		ENDIF
	#ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_SimulateSinglePlayerHUD)
		RETURN iXPValue
	ENDIF
	
	STORE_XP_ANIMATION(vPosition, iXPValue)
	
	RETURN iXPValue
	
ENDFUNC

/// PURPOSE:
///    Give (or take) xp to the local player. Can be +ve or -ve. Updates online storage. Call after team selection only. Call for the end of the mission
/// PARAMS:
///    iXPToGive - Value of XP to give.
FUNC INT GIVE_LOCAL_PLAYER_FM_XP_END_OF_MISSION(XP_TYPE eXpType, STRING tl_23XpType, XPTYPE anXPType, XPCATEGORY anXPCategory, INT iOverrideAmount = 0, INT iMultiplier = 1, BOOL isDebug = FALSE, BOOL bDoFreeAimBonus = FALSE)
	
	INT iXp

	
	#IF IS_DEBUG_BUILD
		NET_NL()
		NET_PRINT("GIVE_LOCAL_PLAYER_FM_XP_END_OF_MISSION ")
	#ENDIF
	iXp = PRIVATE_GIVE_PLAYER_XP(eXpType, GAMEMODE_FM, tl_23XpType, iOverrideAmount,iMultiplier, FALSE, FALSE, -1, isDebug, anXPType, anXPCategory, bDoFreeAimBonus)

	RETURN iXp
//	GIVE_LOCAL_PLAYER_FM_XP(tl_23XpType, iOverrideAmount,iMultiplier, bMissionXP,isDebug, iGlobalXPValue  )
ENDFUNC






FUNC INT GIVE_LOCAL_PLAYER_XP(XP_TYPE eXpType, STRING tl_23XpType, XPTYPE anXPType, XPCATEGORY anXPCategory , INT iOverrideAmount = 0, INT iMultiplier = 1, INT iGlobalXPValue = -1, BOOL bDoFreeAimBonus = FALSE)

	RETURN GIVE_LOCAL_PLAYER_FM_XP(eXpType, tl_23XpType, anXPType, anXPCategory , iOverrideAmount, iMultiplier, iGlobalXPValue, bDoFreeAimBonus)

ENDFUNC


//PROC GIVE_LOCAL_PLAYER_XP_END_OF_MISSION(XP_TYPE eXpType, STRING tl_23XpType, INT iOverrideAmount = 0, INT iMultiplier = 1, BOOL isDebug = FALSE)
//	GIVE_LOCAL_PLAYER_FM_XP_END_OF_MISSION(eXpType, tl_23XpType, iOverrideAmount, iMultiplier, isDebug )
//ENDPROC
//
////

PROC GIVE_LOCAL_PLAYER_CASH_END_OF_MISSION(INT iAmount = 0, INT iMultiplier = 1, BOOL bDisplayTicker = TRUE, FLOAT fPercentageToTeamSafe = 10.0)

	GIVE_LOCAL_PLAYER_FM_CASH_END_OF_MISSION( iAmount, iMultiplier, bDisplayTicker, fPercentageToTeamSafe )

ENDPROC



PROC GIVE_LOCAL_PLAYER_CASH(INT AmountChange, BOOL bDisplayTicker = FALSE)
	
	
	GIVE_LOCAL_PLAYER_FM_CASH(AmountChange)
	IF bDisplayTicker = TRUE
		PRINT_CASH_TICKER(AmountChange)
	ENDIF


ENDPROC

FUNC INT GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(XP_TYPE eXpType, ENTITY_INDEX xpEntityID, STRING tl_23XpType, XPTYPE anXPType, XPCATEGORY anXPCategory, INT iOverrideAmount = 0, INT iMultiplier = 1, INT iGlobalXPValue = -1, STRING stXPDesc = NULL, BOOL bDoFreeAimBonus = FALSE, INT iDisplayDurationMS = 0)
	
	RETURN GIVE_LOCAL_PLAYER_FM_XP_WITH_ANIMATION(eXpType, xpEntityID, tl_23XpType, anXPType,anXPCategory, iOverrideAmount, iMultiplier, iGlobalXPValue, stXPDesc, bDoFreeAimBonus, iDisplayDurationMS)

ENDFUNC

/// PURPOSE: Allows script to provide a description to the next XP being awarded
///    iParamInt1 is optional INT that always appears before XP (ie. Survived for 3s - 150 XP)
PROC SET_NEXT_XP_DESCRIPTION(STRING tl15Desc, INT iXPAmount, INT iParamInt1 = -1)

	NET_PRINT("SET_NEXT_XP_DESCRIPTION - Label: ") NET_PRINT(tl15Desc) NET_PRINT(" and XP: ") NET_PRINT_INT(iXPAmount) NET_NL()

	g_sXPDescription.tlXPDesc = tl15Desc
	g_sXPDescription.iXPAmount = iXPAmount
	g_sXPDescription.iParamInt = iParamInt1
	g_sXPDescription.bDisplayText = TRUE
ENDPROC

// Cleans up XP description struct
PROC RESET_XP_DESCRIPTION()
	g_sXPDescription.tlXPDesc = ""
	g_sXPDescription.iXPAmount = 0
	g_sXPDescription.iParamInt = -1
	g_sXPDescription.bDisplayText = FALSE
ENDPROC


//EOF
