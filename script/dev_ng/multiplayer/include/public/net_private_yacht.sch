USING "globals.sch"
USING "net_include.sch"
USING "net_spawn_debug.sch"
USING "net_events.sch"

#IF IS_DEBUG_BUILD
USING "shared_debug.sch"
#ENDIF

USING "net_yacht_scene_public.sch"
USING "transition_saving.sch"



CONST_INT YACHT_HELP_TEXT_DOCKING_VEHICLE						0
CONST_INT YACHT_HELP_TEXT_AIR_DEFENCE_WARNING					1
CONST_INT YACHT_HELP_TEXT_AIR_DEFENCE_SHOTS						2
CONST_INT YACHT_HELP_TEXT_AIR_DEFENCE_MISSION					3
CONST_INT YACHT_HELP_TEXT_AIR_DEFENCE_WANTED_LEVEL				4
CONST_INT YACHT_HELP_TEXT_AIR_DEFENCE_DISABLE_WEAPONS			5
CONST_INT YACHT_HELP_TEXT_AIR_DEFENCE_DISABLE_WEAPONS_WHEEL		6


CONST_INT YACHT_HELP_TEXT_SEAT_SIT			0
CONST_INT YACHT_HELP_TEXT_SEAT_STAND		1
CONST_INT YACHT_DO_SIT_DOWN					2
CONST_INT YACHT_DO_STAND_UP					3


CONST_INT YACHT_DIALOGUE_AIR_DEF_ACTIVE				0
CONST_INT YACHT_DIALOGUE_AIR_DEF_FIRST_SHOT			1
CONST_INT YACHT_DIALOGUE_AIR_DEF_SWIMNEAR			2
CONST_INT YACHT_DIALOGUE_AIR_DEF_THIRD_SHOT			3
CONST_INT YACHT_DIALOGUE_AIR_DEF_CAPTAIN_SETUP		4
CONST_INT YACHT_DIALOGUE_AIR_DEF_ATTACKED_YACHT		5
CONST_INT YACHT_DIALOGUE_AIR_DEF_ATTACK_PASSENGER	6
CONST_INT YACHT_DIALOGUE_AIR_DEF_REASSURE_PASSENGER	7
CONST_INT YACHT_DIALOGUE_AIR_DEF_INTRO_PASSENGER	8
CONST_INT YACHT_DIALOGUE_AIR_DEF_PASSENGER_NOW 		9

// display positions
// default
STRUCT DISPLAY_INFO_STRUCT
	FLOAT	fInfoDisplayPosX			=	0.0 
	FLOAT	fInfoDisplayPosY			=	-0.014
	FLOAT 	fInfoDisplaySizeX			=	0.0
	FLOAT 	fInfoDisplaySizeY			=	0.01
	FLOAT	fInfoDisplayScaleX			=	0.67 
	FLOAT	fInfoDisplayScaleY			=	0.67 
	INT		iInfoDisplayTime			=	9000 
	INT		iInfoDisplayFadeTime		=	1000
	FLOAT	fInfoDisplayMaxWidth		=	0.17
	FLOAT	fInfoDisplayMaxWidth_4_3	=	0.14
	FLOAT 	fMaxInfoDisplayWidth 		= 	0.17
	SCRIPT_TIMER safeDisplayInfoTimer
	STRING sSavedPlayerName
	BOOL bUserContentIsBlocked = FALSE
ENDSTRUCT

ENUM PRIVATE_YACHT_COMPONENTS
	PYC_BASE = 0,
//	PYC_BRIDGE,
	PYC_INTERIOR
//	PYC_OPTION_1,
//	PYC_OPTION_1_rA,
//	PYC_OPTION_1_rB,
//	PYC_OPTION_2, 
//	PYC_OPTION_2_rA,
//	PYC_OPTION_2_rB,
//	PYC_OPTION_3,
//	PYC_OPTION_3_rA,
//	PYC_OPTION_3_rB
ENDENUM

//STRUCT YACHT_DATA
//	INT iNavMeshBlockingObject = -1
//	//OBJECT_INDEX ObjectID
//	YACHT_APPEARANCE Appearance
//	OBJECT_INDEX OptionObjectID[6]
//	OBJECT_INDEX JacuzziObjectID
//	OBJECT_INDEX LightingObjectID
//	OBJECT_INDEX RailingObjectID
//	OBJECT_INDEX RadarObjectID[3]
//	OBJECT_INDEX FlagObjectID
//	OBJECT_INDEX PoleObjectID
//	OBJECT_INDEX HullObjectID
//	BOOL bObjectsCreated
//	BOOL bHullObjectsCreated
//	BOOL bColourSet
//	BOOL bUpdateObjects
//	BOOL bCreateObjects
//	
//ENDSTRUCT


STRUCT YACHT_DIALOGUE_STRUCT
	structPedsForConversation captainConversation
ENDSTRUCT

STRUCT PRIVATE_YACHT_LOCAL_DATA
	
	// should really move into yacht_data so we can set initial values.
	INT iYachtSpawnState[NUMBER_OF_PRIVATE_YACHTS]
	INT iYachtHullState[NUMBER_OF_PRIVATE_YACHTS]
	INT iVehicleSpawnState[NUMBER_OF_PRIVATE_YACHT_VEHICLES]
	//INT iYachtDummyState[NUMBER_OF_PRIVATE_YACHTS]
	//INT iYachtFadeTimer[NUMBER_OF_PRIVATE_YACHTS]
	//OBJECT_INDEX oYachtDummies[NUMBER_OF_PRIVATE_YACHTS]
	//OBJECT_INDEX oYachtDummiesMast[NUMBER_OF_PRIVATE_YACHTS]
	BOOL bDoneIPLRemovalCheck[NUMBER_OF_PRIVATE_YACHTS]
	BLIP_INDEX YachtBlipID[NUMBER_OF_PRIVATE_YACHTS]
	VEHICLE_INDEX StoredVehicleID[NUMBER_OF_PRIVATE_YACHT_VEHICLES]
	#IF IS_DEBUG_BUILD
		BOOL bWarpToYachtCoords[NUMBER_OF_PRIVATE_YACHTS]
		BOOL bBlipIsActive[NUMBER_OF_PRIVATE_YACHTS]
		BOOL bActivateThisYacht[NUMBER_OF_PRIVATE_YACHTS]
		BOOL bShouldYachtBeActive[NUMBER_OF_PRIVATE_YACHTS]	
	#ENDIF
	
	YACHT_DATA YachtData[NUMBER_OF_PRIVATE_YACHTS]
	
	TIME_DATATYPE VehicleSpawnTimer[NUMBER_OF_PRIVATE_YACHT_VEHICLES]
	
	TIME_DATATYPE WarpTimer
	
	INT iClientPlayerStagger
	INT iClientYachtStagger
	INT iServerYachtStagger
	INT iVehicleStagger
	INT iSeatStagger
	INT iBuoyStagger
	INT iDockingStagger
	#IF IS_DEBUG_BUILD
	INT iDebugStagger
	#ENDIF	
	
	INT iBS_PlayersOnMyYacht
	INT iBS_PlayersNearMyYacht
	
	INT iYachtToWarpFrom
	
	PLAYER_INDEX WarpOwnerID
	
	
	BLIP_INDEX YachtVehicleBlipID[NUMBER_OF_PRIVATE_YACHT_VEHICLES]
	
	#IF IS_DEBUG_BUILD
		WIDGET_GROUP_ID wgYachtWidget
		FLOAT fForceAppliedWidget
		FLOAT fForceAppliedHeadingWidget
	#ENDIF
	
	INT HelpTextBitset
	INT DockingStages
	INT VehicleDocking
	INT iStoredAllowIntoYV[NUMBER_OF_PRIVATE_YACHT_VEHICLES]
	BOOL bDockingInputReceived
	INT bDockingInputLoopedNumber
	SCRIPT_TIMER st_DockingCooldownTimer
	
	INT bSeatHelpTextBitset[NUM_SEAT_ON_YACHT]
	BOOL bSeatInputReceived
	INT bSeatInputLoopedNumber
	
	BOOL bAreBuoysCreated
	INT bYachtWithBuoys
	OBJECT_INDEX oBouy[NUMBER_OF_PRIVATE_YACHT_BUOY]
	

	BOOL bCreatedAirDefence[NUMBER_OF_PRIVATE_YACHTS]
	INT bCreatedAirDefence_OwnYachtLastFrameBS //Based on players actual owning a yacht
	INT iAirDefenceSettingLastFrame[NUMBER_OF_PRIVATE_YACHTS] //Based on Yacht as we get the owner of the yacht setting
	BOOL bAirDefencePlayerInSessionLastFrame[NUM_NETWORK_PLAYERS] //Based on players actual setting
	SCRIPT_TIMER st_AirDefenceWarningShotTimer
	INT i_inYachtIDAreaHelpText = -1
	
	INT i_inYachtIDMissionHelpText = -1
	BOOL HasLoadedPTFXWarningShot
	INT iAirDefenceSphereIndex[NUMBER_OF_PRIVATE_YACHTS]
	BOOL bAirDefence_WillAttackMe[NUMBER_OF_PRIVATE_YACHTS]
	INT bAirDefence_BSRunDialogueWarning[NUMBER_OF_PRIVATE_YACHTS]
	SCRIPT_TIMER st_AirDefenceDialogueTimer[NUMBER_OF_PRIVATE_YACHTS]
	SCRIPT_TIMER st_AirDefenceDialogueAttackTimer[NUMBER_OF_PRIVATE_YACHTS]
	SCRIPT_TIMER st_AirDefenceDialogueAggroTimer
	SCRIPT_TIMER st_AirDefenceDialogueAttackingTimer
	INT i_inYachtIDAreaWarningShot = -1
	INT bTurnOffYachtAirDefForMissionPreviousSetting
	INT bTurnOffYachtAirDefForMissionPlayer = -1
	
	#IF IS_DEBUG_BUILD
	BOOL bAirDefence_WillAttackMe_LastFrame[NUMBER_OF_PRIVATE_YACHTS]
	#ENDIF
	INT bAirDefence_Setting_LastFrame[NUMBER_OF_PRIVATE_YACHTS]
	BOOL bAirDefence_InAreaAndVehicle_LastFrame[NUMBER_OF_PRIVATE_YACHTS]
	BOOL bRefreshAirDefenceDueToMove
	SCRIPT_TIMER st_AirDefenceWarmUpTimer[NUMBER_OF_PRIVATE_YACHTS]
	
	// Move Yacht Scene
	STRUCT_NET_YACHT_SCENE sMoveScene
	INT iCutStage
	CAMERA_INDEX hCam0, hCam1
	#IF IS_DEBUG_BUILD
		structSceneTool_Launcher rag_net_yacht_scene
	#ENDIF
	
	BOOL bWantedLevelIsSuppressed
	INT iStoredMaxWantedLevel = -1

	INT iAssignYachtLastFrame
	BOOL bHasChangedYachtAssignment
	
	YACHT_APPEARANCE AppearanceLastFrame
	YACHT_APPEARANCE AppearanceHasChanged
	
	INT iYachtToFade = -1
	INT iYachtFadeState = -1
	INT iYachtFadeOldOption = -1
	TIME_DATATYPE YachtFadeTimer
	
	INT iAlreadyProcessedYachtThisFrame
	
	SCRIPT_TIMER st_AnchorAllBoatsTimer
	INT iYachtIDWeAreDockingOn
	
	YACHT_DIALOGUE_STRUCT yachtDialogueStruct

	INT iLockStateForVehicles
	TIME_DATATYPE LockStateTimer

	BOOL bResetVehicleColours
	
	BOOL bDialogueSwitchedtoDLC
	BOOL bCleanupAllStoredYachtVehicles
	
	SCRIPT_TIMER iYachtHornTimer
	INT iYachtHornSoundID = -1
	#IF IS_DEBUG_BUILD
	INT iTotalYachtObjectsCreated = 0
	INT iMaxYachtObjectsCreated = 0
	#ENDIF

	BOOL bPlayerIsDead

	INT iWarpBehindYacht = -1
	INT iWarpBehindYachtState = -1
	VECTOR vWarpBehindCoords
	FLOAT fWarpBehindHeading

	BOOL bDoneWarpHelpCheck
	
	BOOL bRunnningWarmUp[NUMBER_OF_PRIVATE_YACHTS]

ENDSTRUCT

PROC SET_LOCAL_PLAYER_BD_DESIRED_YACHT_ID(INT iDesiredYachtID)
	DEBUG_PRINTCALLSTACK()
	
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iDesiredYachtID = iDesiredYachtID
	
	INT iPlayerHash = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
	
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iPlayerHash = iPlayerHash
	
	CPRINTLN(DEBUG_YACHT, "SET_LOCAL_PLAYER_BD_DESIRED_YACHT_ID called with ", iDesiredYachtID, ", iPlayerHash = ", iPlayerHash, " player id ", NATIVE_TO_INT(PLAYER_ID()))
ENDPROC

PROC CLEAR_LOCAL_PLAYER_BD_DESIRED_YACHT_ID()
	DEBUG_PRINTCALLSTACK()
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iDesiredYachtID = -1
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iPlayerHash = -1
ENDPROC

PROC SET_LOCAL_PLAYER_BD_DESIRED_YACHT_LOCATION(VECTOR vDesiredCoords)
	DEBUG_PRINTCALLSTACK()
	IF VMAG(vDesiredCoords) < 0.01
		vDesiredCoords.z = 1.0	
	ENDIF
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.vDesiredCoords = vDesiredCoords
	INT iPlayerHash = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iPlayerHash = iPlayerHash
	
	CPRINTLN(DEBUG_YACHT, "SET_LOCAL_PLAYER_BD_DESIRED_YACHT_LOCATION called with <<", vDesiredCoords.x, ", ", vDesiredCoords.y, ", ", vDesiredCoords.z, ">>, iPlayerHash = ", iPlayerHash, " player id ", NATIVE_TO_INT(PLAYER_ID()))
ENDPROC

PROC CLEAR_LOCAL_PLAYER_BD_DESIRED_YACHT_LOCATION()
	DEBUG_PRINTCALLSTACK()
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.vDesiredCoords = <<0.0, 0.0, 0.0>>
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iPlayerHash = -1
ENDPROC

PROC SET_LOCAL_PLAYER_BD_YACHT_OPTION(INT iOption)
	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_YACHT, "SET_LOCAL_PLAYER_BD_YACHT_OPTION called with ", iOption)
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.Appearance.iOption = iOption
ENDPROC

PROC SET_LOCAL_PLAYER_BD_YACHT_TINT(INT iTint)
	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_YACHT, "SET_LOCAL_PLAYER_BD_YACHT_TINT called with ", iTint)
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.Appearance.iTint = iTint
ENDPROC

PROC SET_LOCAL_PLAYER_BD_YACHT_LIGHTING(INT iLighting)
	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_YACHT, "SET_LOCAL_PLAYER_BD_YACHT_LIGHTING called with ", iLighting)
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.Appearance.iLighting = iLighting
ENDPROC

PROC SET_LOCAL_PLAYER_BD_YACHT_RAILING(INT iRailing)
	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_YACHT, "SET_LOCAL_PLAYER_BD_YACHT_RAILING called with ", iRailing)
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.Appearance.iRailing = iRailing
ENDPROC

PROC SET_LOCAL_PLAYER_BD_YACHT_ACCESS(INT iAccess)
	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_YACHT, "SET_LOCAL_PLAYER_BD_YACHT_ACCESS called with ", iAccess)
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iAccess = iAccess
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_DOING_FADE_SCENE_FOR_YACHT()
	RETURN MPGlobalsPrivateYacht.bPlayerDoingYachtFade
ENDFUNC

PROC SET_LOCAL_PLAYER_BD_YACHT_FLAG(INT iFlag)
	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_YACHT, "SET_LOCAL_PLAYER_BD_YACHT_FLAG called with ", iFlag)
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.Appearance.iFlag = iFlag
ENDPROC

PROC SET_LOCAL_PLAYER_BD_YACHT_NAME(STRING sName)
	DEBUG_PRINTCALLSTACK()
	INT iHash = GET_HASH_KEY(sName)
	CPRINTLN(DEBUG_YACHT, "SET_LOCAL_PLAYER_BD_YACHT_NAME called with ", sName, " hash = ", iHash)
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.Appearance.sName = sName
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.Appearance.iNameAsHash = iHash
ENDPROC


#IF IS_DEBUG_BUILD
PROC PRINTLN_YACHT_APPEARANCE(YACHT_APPEARANCE &Appearance)
	CPRINTLN(DEBUG_YACHT, "Yacht Appearance - iOption = ", Appearance.iOption, ", iTint = ", Appearance.iTint, ", iLighting = ", Appearance.iLighting, ", iRailing = ", Appearance.iRailing, ", iFlag = ", Appearance.iFlag, ", sName = ", Appearance.sName, ", iNameAsHash = ", Appearance.iNameAsHash)
ENDPROC
#ENDIF

PROC MAKE_YACHT_APPEARANCE_SAFE(YACHT_APPEARANCE &Appearance)
	#IF IS_DEBUG_BUILD
	BOOL bTriggered = FALSE
	
	IF (g_bMakeDefaultYachtFullyPimped)
		Appearance.iOption = 2
		Appearance.iTint = 15
		Appearance.iLighting = 7
		Appearance.iRailing = 1
		Appearance.iFlag = 45
		EXIT
	ENDIF
	
	#ENDIF
	IF (Appearance.iOption < 0)
		Appearance.iOption = 0
		CPRINTLN(DEBUG_YACHT, "MAKE_YACHT_APPEARANCE_SAFE - iOption = 0")
		#IF IS_DEBUG_BUILD
		bTriggered = TRUE
		#ENDIF
	ENDIF
	IF (Appearance.iOption > 2)
		Appearance.iOption = 2
		CPRINTLN(DEBUG_YACHT, "MAKE_YACHT_APPEARANCE_SAFE - iOption = 2")
		#IF IS_DEBUG_BUILD
		bTriggered = TRUE
		#ENDIF
	ENDIF
	IF (Appearance.iTint < 0)
		Appearance.iTint = 0
		CPRINTLN(DEBUG_YACHT, "MAKE_YACHT_APPEARANCE_SAFE - iTint = 0")
		#IF IS_DEBUG_BUILD
		bTriggered = TRUE
		#ENDIF
	ENDIF	
	IF (Appearance.iTint > 15)
		Appearance.iTint = 15
		CPRINTLN(DEBUG_YACHT, "MAKE_YACHT_APPEARANCE_SAFE - iTint = 15")
		#IF IS_DEBUG_BUILD
		bTriggered = TRUE
		#ENDIF
	ENDIF	
	IF (Appearance.iLighting < 0)
		Appearance.iLighting = 0
		CPRINTLN(DEBUG_YACHT, "MAKE_YACHT_APPEARANCE_SAFE - iLighting = 0")
		#IF IS_DEBUG_BUILD
		bTriggered = TRUE
		#ENDIF
	ENDIF	
	IF (Appearance.iLighting > 7)
		Appearance.iLighting = 7
		CPRINTLN(DEBUG_YACHT, "MAKE_YACHT_APPEARANCE_SAFE - iLighting = 7")
		#IF IS_DEBUG_BUILD
		bTriggered = TRUE
		#ENDIF
	ENDIF
	IF (Appearance.iRailing < 0)
		Appearance.iRailing = 0
		CPRINTLN(DEBUG_YACHT, "MAKE_YACHT_APPEARANCE_SAFE - iRailing = 0")
		#IF IS_DEBUG_BUILD
		bTriggered = TRUE
		#ENDIF
	ENDIF	
	IF (Appearance.iRailing > 1)
		Appearance.iRailing = 1
		CPRINTLN(DEBUG_YACHT, "MAKE_YACHT_APPEARANCE_SAFE - iRailing = 2")
		#IF IS_DEBUG_BUILD
		bTriggered = TRUE
		#ENDIF
	ENDIF
	IF (Appearance.iFlag < 0)
		Appearance.iFlag = 0
		CPRINTLN(DEBUG_YACHT, "MAKE_YACHT_APPEARANCE_SAFE - iRailing = 0")
		#IF IS_DEBUG_BUILD
		bTriggered = TRUE
		#ENDIF
	ENDIF	
	IF (Appearance.iFlag > 45)
		Appearance.iFlag = 45
		CPRINTLN(DEBUG_YACHT, "MAKE_YACHT_APPEARANCE_SAFE - iRailing = 2")
		#IF IS_DEBUG_BUILD
		bTriggered = TRUE
		#ENDIF
	ENDIF		
	#IF IS_DEBUG_BUILD
	IF (bTriggered)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_BD_YACHT_APPEARANCE(YACHT_APPEARANCE Appearance)
	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_YACHT, "SET_LOCAL_PLAYER_BD_YACHT_RAILING called with: ")
	#IF IS_DEBUG_BUILD
	PRINTLN_YACHT_APPEARANCE(Appearance)
	#ENDIF
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.Appearance = Appearance
	MAKE_YACHT_APPEARANCE_SAFE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.Appearance)
ENDPROC

FUNC BOOL DOES_LOCAL_PLAYER_STAT_OWN_PRIVATE_YACHT()
	RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_INV_YACHT_MODEL_0) != 0
ENDFUNC

FUNC VECTOR GET_COORD_OFFSET_FOR_PRIVATE_YACHT(INT iYacht)
	VECTOR vReturn = <<PRIVATE_YACHT_MAIN_COORDS_OFFSET_X, PRIVATE_YACHT_MAIN_COORDS_OFFSET_Y, PRIVATE_YACHT_MAIN_COORDS_OFFSET_Z>>
	RotateVec(vReturn, <<0.0, 0.0, (g_PrivateYachtDetails[iYacht].fInteriorHeading + PRIVATE_YACHT_MAIN_HEADING_OFFSET) >>)
	RETURN vReturn
ENDFUNC

FUNC VECTOR GET_COORDS_OF_PRIVATE_YACHT(INT iYachtID)
	#IF IS_DEBUG_BUILD
		IF (g_bUseDebugOffsetsForYacht)
			g_PrivateYachtDetails[iYachtID].vCoords = g_PrivateYachtDetails[iYachtID].vInteriorCoords + GET_COORD_OFFSET_FOR_PRIVATE_YACHT(iYachtID)	
		ENDIF
	#ENDIF	
	RETURN g_PrivateYachtDetails[iYachtID].vCoords
ENDFUNC


FUNC FLOAT GET_HEADING_OF_PRIVATE_YACHT(INT iYachtID)
	#IF IS_DEBUG_BUILD
		IF (g_bUseDebugOffsetsForYacht)
			g_PrivateYachtDetails[iYachtID].fHeading = g_PrivateYachtDetails[iYachtID].fInteriorHeading + PRIVATE_YACHT_MAIN_HEADING_OFFSET	
		ENDIF
	#ENDIF
	RETURN g_PrivateYachtDetails[iYachtID].fHeading
ENDFUNC

FUNC INT GET_PLAYERS_DESIRED_YACHT_ID(PLAYER_INDEX PlayerID)
	INT iPlayerHash = NETWORK_HASH_FROM_PLAYER_HANDLE(PlayerID)
	
	IF (GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.iPlayerHash = iPlayerHash)
		RETURN GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.iDesiredYachtID
	ENDIF
	
	CPRINTLN(DEBUG_YACHT, "GET_PLAYERS_DESIRED_YACHT_ID - player hash doesn't match stored value ", GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.iPlayerHash, " iPlayerHash = ", iPlayerHash)
	CPRINTLN(DEBUG_YACHT, "GET_PLAYERS_DESIRED_YACHT_ID - GlobalplayerBD[", NATIVE_TO_INT(PlayerID) ,"].PrivateYachtDetails.iDesiredYachtID = ", GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.iDesiredYachtID)
	
	RETURN -1
ENDFUNC

FUNC VECTOR GET_PLAYERS_DESIRED_YACHT_COORDS(PLAYER_INDEX PlayerID)	
	INT iPlayerHash = NETWORK_HASH_FROM_PLAYER_HANDLE(PlayerID)	
	IF (GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.iPlayerHash = iPlayerHash)
		RETURN GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.vDesiredCoords
	ENDIF
	CPRINTLN(DEBUG_YACHT, "GET_PLAYERS_DESIRED_YACHT_COORDS - player hash doesn't match stored value ", GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.iPlayerHash, " iPlayerHash = ", iPlayerHash)
	CPRINTLN(DEBUG_YACHT, "GET_PLAYERS_DESIRED_YACHT_COORDS - GlobalplayerBD[", NATIVE_TO_INT(PlayerID) ,"].PrivateYachtDetails.vDesiredCoords = ", GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.vDesiredCoords)

	RETURN <<0,0,0>>
ENDFUNC

PROC SET_LOCAL_PLAYER_OWNS_PRIVATE_YACHT(BOOL bSet, INT iOption = -1, INT iTint = -1, INT iLighting = -1, INT iRailing = -1, INT iFlag = -1, INT iSlot = -1)
	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_YACHT, "SET_LOCAL_PLAYER_OWNS_PRIVATE_YACHT called with ", bSet, ", ", iOption, ", ", iTint, ", ", iLighting, ", ", iRailing, ", ", iFlag)
	CPRINTLN(DEBUG_YACHT, "SET_LOCAL_PLAYER_OWNS_PRIVATE_YACHT called with iSlot = ", iSlot, " GET_ACTIVE_CHARACTER_SLOT() ", GET_ACTIVE_CHARACTER_SLOT(), " ")	

	VECTOR vPlayerCoords

	IF iSlot = -1
		iSlot = GET_ACTIVE_CHARACTER_SLOT()
	ENDIF

	IF bSet
		SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_PROPERTY,PROPERTY_YACHT_APT_1_BASE, iSlot)
		IF iSlot = GET_ACTIVE_CHARACTER_SLOT()
			IF (VMAG(GET_PLAYERS_DESIRED_YACHT_COORDS(PLAYER_ID())) < 0.01)
				
				INT iLastYachtID = 0
				iLastYachtID = GET_MP_INT_CHARACTER_STAT(MP_STAT_YACHTPREFERREDAREA)
				iLastYachtID += -1 // the stat default is zero, so we use this as NULL
				PRINTLN("SET_LOCAL_PLAYER_OWNS_PRIVATE_YACHT - iLastYachtID = ", iLastYachtID)	
				IF (iLastYachtID != -1)
					vPlayerCoords = GET_COORDS_OF_PRIVATE_YACHT(iLastYachtID)
				ELSE			
					vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
					CPRINTLN(DEBUG_YACHT, "SET_LOCAL_PLAYER_OWNS_PRIVATE_YACHT vDesiredCoords not set so using players current coords. ", vPlayerCoords)	
				ENDIF
				SET_LOCAL_PLAYER_BD_DESIRED_YACHT_LOCATION(vPlayerCoords)
			ENDIF
		ENDIF
	ELSE
		SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_PROPERTY,0, iSlot)
		SET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_FLAG, 0, iSlot)
		SET_MP_LONG_STRING_CHARACTER_STAT(MP_STAT_YACHT_NAME, MP_STAT_YACHT_NAME2, "", iSlot)
	
		SET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_MOD, 0, iSlot)
		SET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_FIXTURE, 0, iSlot)
		SET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_LIGHTING, 0, iSlot)
		SET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_COLOR, 0, iSlot)
		
		CLEAR_LOCAL_PLAYER_BD_DESIRED_YACHT_LOCATION()
		CLEAR_LOCAL_PLAYER_BD_DESIRED_YACHT_ID()
	ENDIF	
	IF iSlot = GET_ACTIVE_CHARACTER_SLOT()
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.bOwnsPrivateYacht = bSet
	
		// SANITY CHECK THE VALUES
		// PC uses option values from 0-5 to handle price variants so we need to fix up index.
		IF iOption < 0 OR iOption > 5
			iOption = 0
		ENDIF
		IF iOption >= 3 AND iOption <= 5
			iOption -= 3
		ENDIF

		SET_LOCAL_PLAYER_BD_YACHT_OPTION(iOption)
		SET_LOCAL_PLAYER_BD_YACHT_TINT(iTint)
		SET_LOCAL_PLAYER_BD_YACHT_LIGHTING(iLighting)
		SET_LOCAL_PLAYER_BD_YACHT_RAILING(iRailing)
		SET_LOCAL_PLAYER_BD_YACHT_FLAG(iFlag)
	ENDIF
ENDPROC

FUNC BOOL DOES_PLAYER_OWN_PRIVATE_YACHT(PLAYER_INDEX PlayerID)
	RETURN GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.bOwnsPrivateYacht
ENDFUNC

FUNC BOOL IS_PRIVATE_YACHT_ID_VALID(INT iYachtID)
	IF (iYachtID > -1)
	AND (iYachtID < NUMBER_OF_PRIVATE_YACHTS)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC INT GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_INDEX PlayerID)
	RETURN GlobalServerBD_BlockC.PYPlayerDetails[NATIVE_TO_INT(PlayerID)].iPrivateYachtAssignedToPlayer
ENDFUNC

FUNC INT GET_PRIVATE_YACHT_RESERVED_FOR_PLAYER(PLAYER_INDEX PlayerID)
	RETURN GlobalServerBD_BlockC.PYPlayerDetails[NATIVE_TO_INT(PlayerID)].iPrivateYachtReservedForPlayer
ENDFUNC


FUNC PLAYER_INDEX GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(INT iYacht)
	RETURN INT_TO_NATIVE(PLAYER_INDEX, GlobalServerBD_BlockC.PYYachtDetails[iYacht].iPlayerAssignedToPrivateYacht)
ENDFUNC

FUNC PLAYER_INDEX GET_PLAYER_RESERVED_PRIVATE_YACHT(INT iYacht)
	RETURN INT_TO_NATIVE(PLAYER_INDEX, GlobalServerBD_BlockC.PYYachtDetails[iYacht].iPlayerPrivateYachtiIsReservedFor)
ENDFUNC

FUNC INT GET_OPTION_OF_PRIVATE_YACHT(INT iYacht)
	RETURN GlobalServerBD_BlockC.PYYachtDetails[iYacht].Appearance.iOption
ENDFUNC

FUNC INT GET_TINT_OF_PRIVATE_YACHT(INT iYacht)
	RETURN GlobalServerBD_BlockC.PYYachtDetails[iYacht].Appearance.iTint
ENDFUNC

FUNC INT GET_LIGHTING_OF_PRIVATE_YACHT_x(INT iYacht)
	RETURN GlobalServerBD_BlockC.PYYachtDetails[iYacht].Appearance.iLighting
ENDFUNC

FUNC INT GET_RAILING_OF_PRIVATE_YACHT(INT iYacht)
	RETURN GlobalServerBD_BlockC.PYYachtDetails[iYacht].Appearance.iRailing
ENDFUNC

FUNC INT GET_ACCESS_OF_PRIVATE_YACHT(INT iYachtID)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		PLAYER_INDEX OwnerID = GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)
		IF NOT (OwnerID = INVALID_PLAYER_INDEX())
			RETURN GlobalplayerBD[NATIVE_TO_INT(OwnerID)].PrivateYachtDetails.iAccess	
		ENDIF
	ENDIF
	RETURN -1
ENDFUNC

FUNC BOOL DOES_LOCAL_PLAYER_HAVE_ACCESS_TO_YACHT(INT iYachtID)
	INT iAccess = GET_ACCESS_OF_PRIVATE_YACHT(iYachtID)
	IF (iAccess > -1)
		PLAYER_INDEX OwnerID = GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)

		IF NOT (OwnerID = INVALID_PLAYER_INDEX())

			// if local player is owner they always have access
			IF (OwnerID = PLAYER_ID())
				RETURN(TRUE)
			ENDIf
			
			BOOL bSameCrew = ARE_PLAYERS_ON_SAME_ACTIVE_CLAN(PLAYER_ID(), OwnerID)
			GAMER_HANDLE GamerHandle = GET_GAMER_HANDLE_PLAYER(OwnerID)					
			BOOL bFriends = NETWORK_IS_FRIEND(GamerHandle)
		
			BOOL bSameCar 
			IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(OwnerID))
				IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(OwnerID))
					VEHICLE_INDEX VehicleID = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(OwnerID))
					IF NOT IS_ENTITY_DEAD(VehicleID)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), VehicleID)
							bSameCar = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			BOOL bSameGang = GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), OwnerID)
			
		
			SWITCH iAccess
				CASE 0 // everyone
					RETURN(TRUE)
				BREAK
				CASE 1 // crew only
					IF (bSameCrew)
						RETURN(TRUE)
					ENDIF
				BREAK
				CASE 2 // friends			
					IF (bFriends)
						RETURN(TRUE)	
					ENDIF
				BREAK
				CASE 3 // crew + friends
					IF (bSameCrew)
					OR (bFriends)
						RETURN(TRUE)
					ENDIF
				BREAK
				CASE 4 // same gang
					IF (bSameGang)
						RETURN(TRUE)
					ENDIF
				BREAK
				CASE 5 // associate (any of the above)
					IF (bSameCrew)
					OR (bFriends)
					OR (bSameCar)
					OR (bSameGang)
						RETURN(TRUE)
					ENDIF
				BREAK
				CASE 6 // noone
					RETURN(FALSE)	
				BREAK
				
//				CASE 5 // a passenger
//					IF (bSameCar)
//						RETURN(TRUE)
//					ENDIF
//				BREAK
				
				
			ENDSWITCH
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC INT GET_FLAG_OF_PRIVATE_YACHT(INT iYacht)
	RETURN GlobalServerBD_BlockC.PYYachtDetails[iYacht].Appearance.iFlag
ENDFUNC

FUNC TEXT_LABEL_63 GET_NAME_OF_PRIVATE_YACHT(INT iYacht)
	RETURN GlobalServerBD_BlockC.PYYachtDetails[iYacht].Appearance.sName
ENDFUNC

FUNC INT GET_NAME_OF_PRIVATE_YACHT_AS_HASH(INT iYacht)
	RETURN GlobalServerBD_BlockC.PYYachtDetails[iYacht].Appearance.iNameAsHash	
ENDFUNC

PROC GET_APPEARANCE_OF_YACHT(INT iYacht, YACHT_APPEARANCE &Appearance)
	Appearance = GlobalServerBD_BlockC.PYYachtDetails[iYacht].Appearance
	MAKE_YACHT_APPEARANCE_SAFE(Appearance)
ENDPROC

FUNC PLAYER_INDEX GET_LAST_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(INT iYacht)
	RETURN INT_TO_NATIVE(PLAYER_INDEX, GlobalServerBD_BlockC.PYYachtDetails[iYacht].iLastPlayerAssignedToPrivateYacht)
ENDFUNC

FUNC BOOL HAS_PLAYER_BEEN_ASSIGNED_PRIVATE_YACHT(PLAYER_INDEX PlayerID)
	RETURN IS_PRIVATE_YACHT_ID_VALID( GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PlayerID) )
ENDFUNC

FUNC BOOL HAS_PLAYER_RESERVED_PRIVATE_YACHT(PLAYER_INDEX PlayerID)
	RETURN IS_PRIVATE_YACHT_ID_VALID( GET_PRIVATE_YACHT_RESERVED_FOR_PLAYER(PlayerID) )
ENDFUNC

FUNC INT LOCAL_PLAYER_PRIVATE_YACHT_ID()
	RETURN GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID())
ENDFUNC

#IF IS_DEBUG_BUILD
PROC OUTPUT_YACHT_DETAILS_TO_DEBUG_FILE(INT iYachtID)

	SAVE_STRING_TO_DEBUG_FILE("g_PrivateYachtDetails[")
	SAVE_INT_TO_DEBUG_FILE(iYachtID)
	SAVE_STRING_TO_DEBUG_FILE("].vInteriorCoords = ")
	SAVE_VECTOR_TO_DEBUG_FILE(g_PrivateYachtDetails[iYachtID].vInteriorCoords)
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	SAVE_STRING_TO_DEBUG_FILE("g_PrivateYachtDetails[")
	SAVE_INT_TO_DEBUG_FILE(iYachtID)
	SAVE_STRING_TO_DEBUG_FILE("].fInteriorHeading = ")
	SAVE_FLOAT_TO_DEBUG_FILE(g_PrivateYachtDetails[iYachtID].fInteriorHeading)
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	INT i
	REPEAT NUMBER_OF_PRIVATE_YACHT_IPLS i
		SAVE_STRING_TO_DEBUG_FILE("g_PrivateYachtDetails[")
		SAVE_INT_TO_DEBUG_FILE(iYachtID)
		SAVE_STRING_TO_DEBUG_FILE("].strIPL[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("] = '")
		SAVE_STRING_TO_DEBUG_FILE(g_PrivateYachtDetails[iYachtID].strIPL[i])
		SAVE_STRING_TO_DEBUG_FILE("'")
		SAVE_NEWLINE_TO_DEBUG_FILE()	
	ENDREPEAT

ENDPROC

PROC OUTPUT_ALL_YACHT_DETAILS_TO_DEBUG_FILE()
	INT i
	IF OPEN_DEBUG_FILE() 
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("// **************** PRIVATE YACHT DETAILS ******************")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		REPEAT NUMBER_OF_PRIVATE_YACHTS i
			OUTPUT_YACHT_DETAILS_TO_DEBUG_FILE(i)	
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
	
		CLOSE_DEBUG_FILE()
	ENDIF

ENDPROC

#ENDIF

PROC FILL_YACHT_MODEL_TRANSLATION(INT iYachtID, VECTOR &vCoords, FLOAT &fHeading)
	SWITCH iYachtID
		CASE 0 
		vCoords = <<-3542.82,1488.25,5.42995>>
		fHeading 	= -123.045
		BREAK
		CASE 1
		vCoords = <<-3148.38,2807.55,5.42995>>
		fHeading = 91.955
		BREAK
		CASE 2 
		vCoords = <<-3280.5,2140.51,5.42995>>
		fHeading = 86.955
		BREAK
		CASE 3 
		vCoords = <<-2814.49,4072.74,5.42995>>
		fHeading = -108.045
		BREAK
		CASE 4 
		vCoords = <<-3254.55,3685.68,5.42995>>
		fHeading = 81.955
		BREAK
		CASE 5 
		vCoords = <<-2368.44,4697.87,5.42995>>
		fHeading = -133.045
		BREAK
		CASE 6 
		vCoords = <<-3205.34,-219.01,5.42995>>
		fHeading = 176.955
		BREAK
		CASE 7 
		vCoords = <<-3448.25,311.502,5.42995>>
		fHeading = -83.045
		BREAK
		CASE 8 
		vCoords = <<-2697.86,-540.612,5.42995>>
		fHeading = 146.955
		BREAK
		CASE 9 
		vCoords = <<-1995.73,-1523.69,5.42997>>
		fHeading = -38.045
		BREAK
		CASE 10 
		vCoords = <<-2117.58,-2543.35,5.42995>>
		fHeading = 36.955
		BREAK
		CASE 11
		vCoords = <<-1605.07,-1872.47,5.42995>>
		fHeading = -68.045
		BREAK
		CASE 12
		vCoords = <<-753.082,-3919.07,5.42995>>
		fHeading = 11.955
		BREAK
		CASE 13
		vCoords = <<-351.061,-3553.32,5.42995>>
		fHeading = -123.045
		BREAK
		CASE 14
		vCoords = <<-1460.54,-3761.47,5.42995>>
		fHeading = 161.955
		BREAK
		CASE 15
		vCoords = <<1546.89,-3045.63,5.42995>>
		fHeading = -118.045
		BREAK
		CASE 16
		vCoords = <<2490.89,-2428.85,5.42995>>
		fHeading = -168.045
		BREAK
		CASE 17
		vCoords = <<2049.79,-2821.62,5.42995>>
		fHeading = 31.955
		BREAK
		CASE 18
		vCoords = <<3029.02,-1495.7,5.42995>>
		fHeading = -108.045
		BREAK
		CASE 19
		vCoords = <<3021.25,-723.39,5.42995>>
		fHeading = 81.955
		BREAK
		CASE 20
		vCoords = <<2976.62,-1994.76,5.42995>>
		fHeading = -133.045
		BREAK
		CASE 21
		vCoords = <<3404.51,1977.04,5.42995>>
		fHeading = -103.045
		BREAK
		CASE 22
		vCoords = <<3411.1,1193.44,5.42995>>
		fHeading = 31.955
		BREAK
		CASE 23
		vCoords = <<3784.8,2548.54,5.42995>>
		fHeading = 86.955
		BREAK
		CASE 24
		vCoords = <<4225.03,3988,5.42995>>
		fHeading = 61.955
		BREAK
		CASE 25
		vCoords = <<4250.58,4576.57,5.42995>>
		fHeading = 111.955
		BREAK
		CASE 26
		vCoords = <<4204.36,3373.7,5.42995>>
		fHeading = 81.955
		BREAK
		CASE 27
		vCoords = <<3751.68,5753.5,5.42995>>
		fHeading = 136.955
		BREAK
		CASE 28
		vCoords = <<3490.11,6305.79,5.42995>>
		fHeading = 156.955
		BREAK
		CASE 29
		vCoords = <<3684.85,5212.24,5.42995>>
		fHeading = -58.045
		BREAK
		CASE 30
		vCoords = <<581.595,7124.56,5.42995>>
		fHeading = 121.955
		BREAK
		CASE 31
		vCoords = <<2004.46,6907.16,5.42997>>
		fHeading = 6.955
		BREAK
		CASE 32
		vCoords = <<1396.64,6860.2,5.42995>>
		fHeading = 176.955
		BREAK
		CASE 33
		vCoords = <<-1170.69,5980.68,5.42995>>
		fHeading = 91.955
		BREAK
		CASE 34
		vCoords = <<-777.487,6566.91,5.42995>>
		fHeading = 26.955
		BREAK
		CASE 35
		vCoords = <<-381.774,6946.96,5.42995>>
		fHeading = 71.955
		BREAK
		CASE 36
		vCoords = <<3615.5232,-4779.0210,5.4337>>
		fHeading = 0.0
		BREAK
		CASE 37
		vCoords = <<52.6177,-3293.0867,5.4337>>
		fHeading = -90.0
		BREAK
		CASE 38
		vCoords = <<-3537.2012,736.4623,5.4337>>
		fHeading = 180.0
		BREAK
		CASE 39
		vCoords = <<-1748.0023,5328.7461,5.4337>>
		fHeading = 170.0
		BREAK
		#IF FEATURE_FIXER
		CASE 40
		vCoords = <<-3262.1250, -1586.9724, 5.4337>>
		fHeading = 167.5
		BREAK
		CASE 41
		vCoords = <<-850.5552, -4818.5889, 5.4337>>
		fHeading = 32.50
		BREAK
		#ENDIF
	ENDSWITCH
ENDPROC

// apa_mpapyacht_bar1_shell coord
FUNC MP_PROP_OFFSET_STRUCT GET_BASE_YACHT_INTERIOR_LOCATION(INT iYachtID = -1)
	MP_PROP_OFFSET_STRUCT tempStruct
	SWITCH iYachtID
		DEFAULT
		CASE -1
//			tempStruct.vLoc = <<-1573.0980,-4085.7703,9.7027>> //<<-1573.0981, -4085.8059, 9.7851>>  //this is at -1573.0980,-4085.7703,9.7027  apa_mpapyacht_bar1_shell coordinates
//			tempStruct.vRot = <<0,0,162.00>>
			tempStruct.vLoc = <<-1478.4360,-3753.5378,9.7027>>                           
			tempStruct.vRot = <<0,0,-18>>
		BREAK
		CASE 0               
		tempStruct.vLoc = <<-3555.1155,1473.0128,9.7027>>  
		tempStruct.vRot = <<0,0,57.0>>
		BREAK

		CASE 1                
		tempStruct.vLoc = <<-3147.0488,2827.0879,9.7027>>                          
		tempStruct.vRot = <<0,0,-88.0>>
		BREAK

		CASE 2                
		tempStruct.vLoc = <<-3277.4729,2159.8499,9.7027>>                       
		tempStruct.vRot = <<0,0,-93.0>>
		BREAK

		CASE 3                
		tempStruct.vLoc = <<-2822.4194,4054.8396,9.7027>>                           
		tempStruct.vRot = <<0,0,72.0>>
		BREAK

		CASE 4                
		tempStruct.vLoc = <<-3249.8491,3704.6814,9.7027>>                              
		tempStruct.vRot = <<0,0,-98.0>>
		BREAK

		CASE 5                
		tempStruct.vLoc = <<-2383.1934,4685.0034,9.7027>> 
		tempStruct.vRot = <<0,0,47.0>>
		BREAK

		CASE 6                
		tempStruct.vLoc = <<-3224.6863,-215.9825,9.7027>>                          
		tempStruct.vRot = <<0,0,-3.0>>
		BREAK

		CASE 7                
		tempStruct.vLoc = <<-3447.8765,291.9275,9.7027>>                           
		tempStruct.vRot = <<0,0,97.0>>
		BREAK

		CASE 8                
		tempStruct.vLoc = <<-2713.0979,-528.3185,9.7027>>                          
		tempStruct.vRot = <<0,0,-33.0>>
		BREAK

		CASE 9                
		tempStruct.vLoc = <<-1981.6182,-1537.2692,9.7027>>                          
		tempStruct.vRot = <<0,0,142.0>>
		BREAK

		CASE 10                
		tempStruct.vLoc = <<-2100.8169,-2533.2332,9.7027>>                          
		tempStruct.vRot = <<0,0,-143.0>>
		BREAK

		CASE 11                
		tempStruct.vLoc = <<-1599.6425,-1891.2773,9.7027>>                          
		tempStruct.vRot = <<0,0,112.0>>
		BREAK

		CASE 12                
		tempStruct.vLoc = <<-733.6151,-3916.9846,9.7027>>                          
		tempStruct.vRot = <<0,0,-168.0>>
		BREAK

		CASE 13                
		tempStruct.vLoc = <<-363.3534,-3568.5601,9.7027>>                          
		tempStruct.vRot = <<0,0,57.0>>
		BREAK

		CASE 14     
		tempStruct.vLoc = <<-1478.4360,-3753.5378,9.7027>>                           
		tempStruct.vRot = <<0,0,-18>>
		//tempStruct.vLoc = <<-1481.24,-3753.1,9.7027>>                            
		//tempStruct.vRot = <<0,0,-18.0>>
		BREAK

		CASE 15                
		tempStruct.vLoc = <<1535.9740,-3061.8774,9.7027>>                           
		tempStruct.vRot = <<0,0,62.0>>
		BREAK

		CASE 16                
		tempStruct.vLoc = <<2471.4185,-2430.9297,9.7027>>                           
		tempStruct.vRot = <<0,0,12.0>>
		BREAK

		CASE 17                
		tempStruct.vLoc = <<2067.3708,-2813.0103,9.7027>>                           
		tempStruct.vRot = <<0,0,-148.0>>
		BREAK

		CASE 18                
		tempStruct.vLoc = <<3021.0881,-1513.6022,9.7027>>                              
		tempStruct.vRot = <<0,0,72.0>>
		BREAK

		CASE 19                
		tempStruct.vLoc = <<3025.9556,-704.3854,9.7027>>                                
		tempStruct.vRot = <<0,0,-98.0>>
		BREAK

		CASE 20                
		tempStruct.vLoc = <<2961.8629,-2007.6315,9.7027>>                           
		tempStruct.vRot = <<0,0,47.0>>
		BREAK

		CASE 21                
		tempStruct.vLoc = <<3398.1694,1958.5214,9.7027>>                             
		tempStruct.vRot = <<0,0,77.0>>
		BREAK

		CASE 22
		tempStruct.vLoc = <<3428.6812,1202.0597,9.7027>>                             
		tempStruct.vRot = <<0,0,-148.0>>
		BREAK

		CASE 23                
		tempStruct.vLoc = <<3787.8298,2567.8838,9.7027>>                               
		tempStruct.vRot = <<0,0,-93.0>>
		BREAK

		CASE 24                
		tempStruct.vLoc = <<4235.9463,4004.2522,9.7027>>                             
		tempStruct.vRot = <<0,0,-118.0>>
		BREAK

		CASE 25                
		tempStruct.vLoc = <<4245.1514,4595.3750,9.7027>>                               
		tempStruct.vRot = <<0,0,-68.0>>
		BREAK

		CASE 26                
		tempStruct.vLoc = <<4209.0571,3392.7053,9.7027>>                                   
		tempStruct.vRot = <<0,0,-98.0>>
		BREAK

		CASE 27                
		tempStruct.vLoc = <<3738.8098,5768.2524,9.7027>>                             
		tempStruct.vRot = <<0,0,-43.0>>
		BREAK

		CASE 28                
		tempStruct.vLoc = <<3472.9656,6315.2451,9.7027>>                             
		tempStruct.vRot = <<0,0,-23.0>>
		BREAK

		CASE 29                
		tempStruct.vLoc = <<3693.4683,5194.6587,9.7027>>                             
		tempStruct.vRot = <<0,0,122.0>>
		BREAK

		CASE 30                
		tempStruct.vLoc = <<572.9806,7142.1382,9.7027>>                             
		tempStruct.vRot = <<0,0,-58.0>>
		BREAK

		CASE 31                
		tempStruct.vLoc = <<2024.0360,6907.5361,9.7027>>                             
		tempStruct.vRot = <<0,0,-173.0>>
		BREAK

		CASE 32                
		tempStruct.vLoc = <<1377.2958,6863.2305,9.7027>>                             
		tempStruct.vRot = <<0,0,-3.0>>
		BREAK

		CASE 33                
		tempStruct.vLoc = <<-1169.3605,6000.2139,9.7027>>                                 
		tempStruct.vRot = <<0,0,-88.0>>
		BREAK

		CASE 34                
		tempStruct.vLoc = <<-759.2205,6573.9551,9.7027>>                           
		tempStruct.vRot = <<0,0,-153.0>>
		BREAK

		CASE 35                
		tempStruct.vLoc = <<-373.8432,6964.8599,9.7027>>                           
		tempStruct.vRot = <<0,0,-108.0>>
		BREAK
		
		CASE 36
		tempStruct.vLoc = <<3634.9990, -4781.0171, 9.7065>>
		tempStruct.vRot = <<0.0, 0.0, -179.95>>
		BREAK
		
		CASE 37
		tempStruct.vLoc = <<50.6219, -3312.5625, 9.7065>>
		tempStruct.vRot = <<0.0, 0.0, 90.05>>
		BREAK
		
		CASE 38
		tempStruct.vLoc = <<-3556.6770, 738.4581, 9.7065>>
		tempStruct.vRot = <<0.0, 0.0, 0.05>>
		BREAK
		
		CASE 39
		tempStruct.vLoc = <<-1766.8353, 5334.0933, 9.7065>>
		tempStruct.vRot = <<0.0, 0.0, -9.95>>
		BREAK
		
		#IF FEATURE_FIXER
		CASE 40
		tempStruct.vLoc = <<-3280.7068, -1580.8092, 9.7065>>
		tempStruct.vRot = <<0.0, 0.0, -12.45>>
		BREAK
		
		CASE 41
		tempStruct.vLoc = <<-833.0568, -4809.8076, 9.7065>>
		tempStruct.vRot = <<0.0, 0.0, -147.45>>
		BREAK
		#ENDIF
	ENDSWITCH
	
	RETURN tempStruct
ENDFUNC

PROC SET_PRIVATE_YACHT_VEHICLES_INITIAL_VALUES_LEVEL_3()
	
	CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] SET_PRIVATE_YACHT_VEHICLES_INITIAL_VALUES_LEVEL_3 - called ")
	DEBUG_PRINTCALLSTACK()
	
	// heli
	gPrivateYachtSpawnVehicleDetails[0].vOffsetCoords = <<-0.2508, -32.3, 0.8727>>
	gPrivateYachtSpawnVehicleDetails[0].fOffsetHeading = 0.0
	gPrivateYachtSpawnVehicleDetails[0].ModelName = SUPERVOLITO2
	gPrivateYachtSpawnVehicleDetails[0].fClearRadius = 3.5
	gPrivateYachtSpawnVehicleDetails[0].iRespawnTime = 300000
	gPrivateYachtSpawnVehicleDetails[0].iLivery = -1
	gPrivateYachtSpawnVehicleDetails[0].fSpawnInDistance = 400.0	
	gPrivateYachtSpawnVehicleDetails[0].iFlags = ENUM_TO_INT(PYVF_DONT_SPAWN_IF_PLAYER_IN_TAXI_HELI) 
	
	// boat
	gPrivateYachtSpawnVehicleDetails[1].vOffsetCoords = <<11.25, -55.8404, -11>> //<<6.8638, -62.5456, -11.0>> 
	gPrivateYachtSpawnVehicleDetails[1].fOffsetHeading = 340.8939 //132.5474 //136.3450
	gPrivateYachtSpawnVehicleDetails[1].ModelName = DINGHY4
	gPrivateYachtSpawnVehicleDetails[1].fClearRadius = 2.4
	gPrivateYachtSpawnVehicleDetails[1].fSpawnInDistance = 300.0	
	gPrivateYachtSpawnVehicleDetails[1].iFlags = ENUM_TO_INT(PYVF_SET_ANCHOR)
	
	// jetskii
	gPrivateYachtSpawnVehicleDetails[2].vOffsetCoords = <<6.75, -63, -11>> //<<0.0, -62.2, -11.0>>
	gPrivateYachtSpawnVehicleDetails[2].fOffsetHeading = 206.26
	gPrivateYachtSpawnVehicleDetails[2].ModelName = SEASHARK3
	gPrivateYachtSpawnVehicleDetails[2].fClearRadius = 1.4
	gPrivateYachtSpawnVehicleDetails[2].fSpawnInDistance = 200.0	
	gPrivateYachtSpawnVehicleDetails[2].iFlags = ENUM_TO_INT(PYVF_SET_ANCHOR)
	
	// jetskii
	gPrivateYachtSpawnVehicleDetails[3].vOffsetCoords = <<4.5, -63, -11>> //<<-2.8, -62.2, -11.0>>
	gPrivateYachtSpawnVehicleDetails[3].fOffsetHeading = 206.26
	gPrivateYachtSpawnVehicleDetails[3].ModelName = SEASHARK3
	gPrivateYachtSpawnVehicleDetails[3].fClearRadius = 1.4
	gPrivateYachtSpawnVehicleDetails[3].fSpawnInDistance = 200.0	
	gPrivateYachtSpawnVehicleDetails[3].iFlags = ENUM_TO_INT(PYVF_SET_ANCHOR)
	
	// boat
	gPrivateYachtSpawnVehicleDetails[4].vOffsetCoords = <<-11.25, -55.8404, -11>> //<<6.8638, -62.5456, -11.0>> 
	gPrivateYachtSpawnVehicleDetails[4].fOffsetHeading = 20 //132.5474 //136.3450
	gPrivateYachtSpawnVehicleDetails[4].ModelName = TORO2
	gPrivateYachtSpawnVehicleDetails[4].fClearRadius = 2.4
	gPrivateYachtSpawnVehicleDetails[4].fSpawnInDistance = 300.0	
	gPrivateYachtSpawnVehicleDetails[4].iFlags = ENUM_TO_INT(PYVF_SET_ANCHOR)
	
	// jetskii
	gPrivateYachtSpawnVehicleDetails[5].vOffsetCoords = <<-4.5, -63, -11>>
	gPrivateYachtSpawnVehicleDetails[5].fOffsetHeading = 153.7463
	gPrivateYachtSpawnVehicleDetails[5].ModelName = SEASHARK3
	gPrivateYachtSpawnVehicleDetails[5].fClearRadius = 1.4
	gPrivateYachtSpawnVehicleDetails[5].fSpawnInDistance = 200.0	
	gPrivateYachtSpawnVehicleDetails[5].iFlags = ENUM_TO_INT(PYVF_SET_ANCHOR)
	
	// jetskii
	gPrivateYachtSpawnVehicleDetails[6].vOffsetCoords = <<-6.75, -63, -11>>
	gPrivateYachtSpawnVehicleDetails[6].fOffsetHeading = 153.7463
	gPrivateYachtSpawnVehicleDetails[6].ModelName = SEASHARK3
	gPrivateYachtSpawnVehicleDetails[6].fClearRadius = 1.4
	gPrivateYachtSpawnVehicleDetails[6].fSpawnInDistance = 200.0	
	gPrivateYachtSpawnVehicleDetails[6].iFlags = ENUM_TO_INT(PYVF_SET_ANCHOR)
	
	
	gPrivateYachtDockingOffsets[0].vOffsetCoords = gPrivateYachtSpawnVehicleDetails[1].vOffsetCoords
	gPrivateYachtDockingOffsets[0].fOffsetHeading = gPrivateYachtSpawnVehicleDetails[1].fOffsetHeading
	gPrivateYachtDockingOffsets[0].fClearRadius = gPrivateYachtSpawnVehicleDetails[1].fClearRadius
	
	gPrivateYachtDockingOffsets[1].vOffsetCoords = gPrivateYachtSpawnVehicleDetails[2].vOffsetCoords
	gPrivateYachtDockingOffsets[1].fOffsetHeading = gPrivateYachtSpawnVehicleDetails[2].fOffsetHeading
	gPrivateYachtDockingOffsets[1].fClearRadius = gPrivateYachtSpawnVehicleDetails[2].fClearRadius
	
	gPrivateYachtDockingOffsets[2].vOffsetCoords = gPrivateYachtSpawnVehicleDetails[3].vOffsetCoords
	gPrivateYachtDockingOffsets[2].fOffsetHeading = gPrivateYachtSpawnVehicleDetails[3].fOffsetHeading
	gPrivateYachtDockingOffsets[2].fClearRadius = gPrivateYachtSpawnVehicleDetails[3].fClearRadius
	
	gPrivateYachtDockingOffsets[3].vOffsetCoords = gPrivateYachtSpawnVehicleDetails[4].vOffsetCoords
	gPrivateYachtDockingOffsets[3].fOffsetHeading = gPrivateYachtSpawnVehicleDetails[4].fOffsetHeading
	gPrivateYachtDockingOffsets[3].fClearRadius = gPrivateYachtSpawnVehicleDetails[4].fClearRadius
	
	gPrivateYachtDockingOffsets[4].vOffsetCoords = gPrivateYachtSpawnVehicleDetails[5].vOffsetCoords
	gPrivateYachtDockingOffsets[4].fOffsetHeading = gPrivateYachtSpawnVehicleDetails[5].fOffsetHeading
	gPrivateYachtDockingOffsets[4].fClearRadius = gPrivateYachtSpawnVehicleDetails[5].fClearRadius
	
	gPrivateYachtDockingOffsets[5].vOffsetCoords = gPrivateYachtSpawnVehicleDetails[6].vOffsetCoords
	gPrivateYachtDockingOffsets[5].fOffsetHeading = gPrivateYachtSpawnVehicleDetails[6].fOffsetHeading
	gPrivateYachtDockingOffsets[5].fClearRadius = gPrivateYachtSpawnVehicleDetails[6].fClearRadius
	
	//Always available dock
	gPrivateYachtDockingOffsets[6].vOffsetCoords = <<0, -63, -11.9254>>
	gPrivateYachtDockingOffsets[6].fOffsetHeading =180.2068
	gPrivateYachtDockingOffsets[6].fClearRadius = 2.4

ENDPROC


FUNC VECTOR GET_BUOY_POSITION_OFFSET_TO_YACHTS(INT WhichBuoy)
	/*
	SWITCH WhichBuoy
		CASE 0	RETURN <<12.2472, -61.6128, -11.1776>> BREAK
		CASE 1	RETURN <<7.4788, -59.6480, -10.9966>> BREAK
		CASE 2	RETURN <<5.1550, -60.5007, -10.7919>> BREAK
		CASE 3	RETURN <<2.4168, -60.8726, -10.9442>> BREAK
		CASE 4	RETURN <<-2.4168, -60.8726, -10.9442>> BREAK
		CASE 5	RETURN <<-5.1550, -60.5007, -10.7919>> BREAK
		CASE 6	RETURN <<-7.4788, -59.6480, -10.9966>>  BREAK
		CASE 7	RETURN <<-12.2472, -61.6128, -11.1776>>  BREAK
	ENDSWITCH 
	*/
	
	IF WhichBuoy > -1 AND WhichBuoy < NUMBER_OF_PRIVATE_YACHT_BUOY
		RETURN g_PrivateYachtBuoysCoords[WhichBuoy]
	ENDIF

	RETURN <<0,0,0>>
ENDFUNC

FUNC MODEL_NAMES GET_BUOY_MODEL(INT iYachtID, INT WhichBuoy)

	SWITCH GET_OPTION_OF_PRIVATE_YACHT(iYachtID)
	
		CASE 0 
		DEFAULT
			SWITCH WhichBuoy
				CASE 0	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 1	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 2	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 3	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 4	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 5	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 6	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 7	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
			ENDSWITCH
		BREAK
		CASE 1 
			SWITCH WhichBuoy
				CASE 0	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 1	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 2	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 3	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 4	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 5	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 6	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 7	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH WhichBuoy
				CASE 0	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 1	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 2	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 3	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 4	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 5	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 6	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
				CASE 7	RETURN APA_PROP_YACHT_FLOAT_1A BREAK
			ENDSWITCH
		BREAK
	
	ENDSWITCH

	RETURN APA_PROP_YACHT_FLOAT_1A
ENDFUNC

FUNC INT GET_BUOY_FLAG(INT iYachtID, INT WhichBuoy)

	SWITCH GET_OPTION_OF_PRIVATE_YACHT(iYachtID)
	
		CASE 0 
		DEFAULT
			SWITCH WhichBuoy
				CASE 0	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR)  BREAK
				CASE 1	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
				CASE 2	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR)+ENUM_TO_INT(PYVF_DONT_SPAWN) BREAK
				CASE 3	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
				CASE 4	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
				CASE 5	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR)+ENUM_TO_INT(PYVF_DONT_SPAWN) BREAK
				CASE 6	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
				CASE 7	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
			ENDSWITCH
		BREAK
		CASE 1 
			SWITCH WhichBuoy
				CASE 0	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
				CASE 1	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
				CASE 2	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR)+ENUM_TO_INT(PYVF_DONT_SPAWN) BREAK
				CASE 3	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
				CASE 4	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
				CASE 5	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR)+ENUM_TO_INT(PYVF_DONT_SPAWN) BREAK
				CASE 6	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
				CASE 7	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH WhichBuoy
				CASE 0	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
				CASE 1	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
				CASE 2	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
				CASE 3	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
				CASE 4	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
				CASE 5	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
				CASE 6	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
				CASE 7	RETURN ENUM_TO_INT(PYVF_SET_ANCHOR) BREAK
			ENDSWITCH
		BREAK
	
	ENDSWITCH

	RETURN 0
ENDFUNC


PROC SET_PRIVATE_YACHT_VEHICLES_INITIAL_VALUES_LEVEL_2()
	
	CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] SET_PRIVATE_YACHT_VEHICLES_INITIAL_VALUES_LEVEL_2 - called ")
	DEBUG_PRINTCALLSTACK()
	SET_PRIVATE_YACHT_VEHICLES_INITIAL_VALUES_LEVEL_3()
	
	gPrivateYachtSpawnVehicleDetails[3].iFlags+= ENUM_TO_INT(PYVF_DONT_SPAWN)
	gPrivateYachtSpawnVehicleDetails[5].iFlags+= ENUM_TO_INT(PYVF_DONT_SPAWN)
	
	//Heli
	gPrivateYachtSpawnVehicleDetails[0].ModelName = SWIFT2
	gPrivateYachtSpawnVehicleDetails[0].iLivery = -1
	// boat
	gPrivateYachtSpawnVehicleDetails[1].ModelName = DINGHY4
	// jetskii
	gPrivateYachtSpawnVehicleDetails[2].ModelName = SEASHARK3
	// boat
	gPrivateYachtSpawnVehicleDetails[4].ModelName = SPEEDER2
	// jetskii
	gPrivateYachtSpawnVehicleDetails[6].ModelName = SEASHARK3

ENDPROC



PROC SET_PRIVATE_YACHT_VEHICLES_INITIAL_VALUES_LEVEL_1()
	
	CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] SET_PRIVATE_YACHT_VEHICLES_INITIAL_VALUES_LEVEL_1 - called ")
	DEBUG_PRINTCALLSTACK()
	SET_PRIVATE_YACHT_VEHICLES_INITIAL_VALUES_LEVEL_3()
	
	//Heli
	gPrivateYachtSpawnVehicleDetails[0].iFlags+= ENUM_TO_INT(PYVF_DONT_SPAWN)
	// jetskii
	gPrivateYachtSpawnVehicleDetails[3].iFlags+= ENUM_TO_INT(PYVF_DONT_SPAWN)
	// boat
	gPrivateYachtSpawnVehicleDetails[4].iFlags+= ENUM_TO_INT(PYVF_DONT_SPAWN)
	// jetskii
	gPrivateYachtSpawnVehicleDetails[5].iFlags+= ENUM_TO_INT(PYVF_DONT_SPAWN)
	// jetskii
	gPrivateYachtSpawnVehicleDetails[6].iFlags+= ENUM_TO_INT(PYVF_DONT_SPAWN)
	
	
	
	// boat
	gPrivateYachtSpawnVehicleDetails[1].ModelName = TROPIC2
	// jetskii
	gPrivateYachtSpawnVehicleDetails[2].ModelName = SEASHARK3


ENDPROC


FUNC VECTOR GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(INT iYachtID, VECTOR vOffset)
	RETURN GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS ( GET_COORDS_OF_PRIVATE_YACHT(iYachtID), GET_HEADING_OF_PRIVATE_YACHT(iYachtID), vOffset )
ENDFUNC

PROC INIT_YACHT_COORDS()
	
	CPRINTLN(DEBUG_YACHT, "INIT_YACHT_COORDS called...")

	// make sure the coords of the yachts are set first.
	INT i
	REPEAT NUMBER_OF_PRIVATE_YACHTS i
	
		// set the actual coords and headings for the yachts
		g_PrivateYachtDetails[i].vCoords = g_PrivateYachtDetails[i].vInteriorCoords + GET_COORD_OFFSET_FOR_PRIVATE_YACHT(i)
		g_PrivateYachtDetails[i].fHeading = g_PrivateYachtDetails[i].fInteriorHeading + PRIVATE_YACHT_MAIN_HEADING_OFFSET
		
		// set the blip coords
		g_PrivateYachtDetails[i].vBlipCoords = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(i, <<-0.1665, -38.7851, -5.5231>> )
				
	ENDREPEAT	
ENDPROC

PROC INIT_PRIVATE_YACHT_DATA()
	CPRINTLN(DEBUG_YACHT, "initialising data...")
	
	INT i
	MP_PROP_OFFSET_STRUCT PropStruct
	
	REPEAT NUMBER_OF_PRIVATE_YACHTS i
		PropStruct = GET_BASE_YACHT_INTERIOR_LOCATION(i)
		
		g_PrivateYachtDetails[i].vInteriorCoords = PropStruct.vLoc
		g_PrivateYachtDetails[i].fInteriorHeading = PropStruct.vRot.z

		FILL_YACHT_MODEL_TRANSLATION(i, g_PrivateYachtDetails[i].vModelCoords, g_PrivateYachtDetails[i].fModelHeading)

	ENDREPEAT
	
	// base ipl
	g_PrivateYachtDetails[0].strIPL[0] = "apa_yacht_grp01_1"
	g_PrivateYachtDetails[1].strIPL[0] = "apa_yacht_grp01_2"
	g_PrivateYachtDetails[2].strIPL[0] = "apa_yacht_grp01_3"
	g_PrivateYachtDetails[3].strIPL[0] = "apa_yacht_grp02_1"
	g_PrivateYachtDetails[4].strIPL[0] = "apa_yacht_grp02_2"
	g_PrivateYachtDetails[5].strIPL[0] = "apa_yacht_grp02_3"
	g_PrivateYachtDetails[6].strIPL[0] = "apa_yacht_grp03_1"
	g_PrivateYachtDetails[7].strIPL[0] = "apa_yacht_grp03_2"
	g_PrivateYachtDetails[8].strIPL[0] = "apa_yacht_grp03_3"
	g_PrivateYachtDetails[9].strIPL[0] = "apa_yacht_grp04_1"
	g_PrivateYachtDetails[10].strIPL[0] = "apa_yacht_grp04_2"
	g_PrivateYachtDetails[11].strIPL[0] = "apa_yacht_grp04_3"
	g_PrivateYachtDetails[12].strIPL[0] = "apa_yacht_grp05_1"
	g_PrivateYachtDetails[13].strIPL[0] = "apa_yacht_grp05_2"
	g_PrivateYachtDetails[14].strIPL[0] = "apa_yacht_grp05_3"
	g_PrivateYachtDetails[15].strIPL[0] = "apa_yacht_grp06_1"
	g_PrivateYachtDetails[16].strIPL[0] = "apa_yacht_grp06_2"
	g_PrivateYachtDetails[17].strIPL[0] = "apa_yacht_grp06_3"
	g_PrivateYachtDetails[18].strIPL[0] = "apa_yacht_grp07_1"
	g_PrivateYachtDetails[19].strIPL[0] = "apa_yacht_grp07_2"
	g_PrivateYachtDetails[20].strIPL[0] = "apa_yacht_grp07_3"
	g_PrivateYachtDetails[21].strIPL[0] = "apa_yacht_grp08_1"
	g_PrivateYachtDetails[22].strIPL[0] = "apa_yacht_grp08_2"
	g_PrivateYachtDetails[23].strIPL[0] = "apa_yacht_grp08_3"
	g_PrivateYachtDetails[24].strIPL[0] = "apa_yacht_grp09_1"
	g_PrivateYachtDetails[25].strIPL[0] = "apa_yacht_grp09_2"
	g_PrivateYachtDetails[26].strIPL[0] = "apa_yacht_grp09_3"
	g_PrivateYachtDetails[27].strIPL[0] = "apa_yacht_grp10_1"
	g_PrivateYachtDetails[28].strIPL[0] = "apa_yacht_grp10_2"
	g_PrivateYachtDetails[29].strIPL[0] = "apa_yacht_grp10_3"
	g_PrivateYachtDetails[30].strIPL[0] = "apa_yacht_grp11_1"
	g_PrivateYachtDetails[31].strIPL[0] = "apa_yacht_grp11_2"
	g_PrivateYachtDetails[32].strIPL[0] = "apa_yacht_grp11_3"
	g_PrivateYachtDetails[33].strIPL[0] = "apa_yacht_grp12_1"
	g_PrivateYachtDetails[34].strIPL[0] = "apa_yacht_grp12_2"
	g_PrivateYachtDetails[35].strIPL[0] = "apa_yacht_grp12_3"
	g_PrivateYachtDetails[36].strIPL[0] = "sum_lost_yacht"
	g_PrivateYachtDetails[37].strIPL[0] = "h4_islandx_yacht_01"
	g_PrivateYachtDetails[38].strIPL[0] = "h4_islandx_yacht_02"
	g_PrivateYachtDetails[39].strIPL[0] = "h4_islandx_yacht_03"
	g_PrivateYachtDetails[40].strIPL[0] = "sf_yacht_01"
	g_PrivateYachtDetails[41].strIPL[0] = "sf_yacht_02"
	
	// other ipls
	TEXT_LABEL_63 str
	REPEAT NUMBER_OF_PRIVATE_YACHTS i
	
//		// bridge
//		str = g_PrivateYachtDetails[i].strIPL[0]
//		str += "_bridge"
//		g_PrivateYachtDetails[i].strIPL[ENUM_TO_INT(PYC_BRIDGE)] = str
		
		// interior
		str = g_PrivateYachtDetails[i].strIPL[0]
		str += "_int"
		g_PrivateYachtDetails[i].strIPL[ENUM_TO_INT(PYC_INTERIOR)] = str
		
//		// option 1
//		str = g_PrivateYachtDetails[i].strIPL[0]
//		str += "_option1"
//		g_PrivateYachtDetails[i].strIPL[ENUM_TO_INT(PYC_OPTION_1)] = str
//		
//		// option 1 rA
//		str = g_PrivateYachtDetails[i].strIPL[0]
//		str += "_option1_rA"
//		g_PrivateYachtDetails[i].strIPL[ENUM_TO_INT(PYC_OPTION_1_rA)] = str
//		
//		// option 1 rB
//		str = g_PrivateYachtDetails[i].strIPL[0]
//		str += "_option1_rB"
//		g_PrivateYachtDetails[i].strIPL[ENUM_TO_INT(PYC_OPTION_1_rB)] = str
//		
//		// option 2
//		str = g_PrivateYachtDetails[i].strIPL[0]
//		str += "_option2"
//		g_PrivateYachtDetails[i].strIPL[ENUM_TO_INT(PYC_OPTION_2)] = str
//		
//		// option 2 rA
//		str = g_PrivateYachtDetails[i].strIPL[0]
//		str += "_option2_rA"
//		g_PrivateYachtDetails[i].strIPL[ENUM_TO_INT(PYC_OPTION_2_rA)] = str
//		
//		// option 2 rB
//		str = g_PrivateYachtDetails[i].strIPL[0]
//		str += "_option2_rB"
//		g_PrivateYachtDetails[i].strIPL[ENUM_TO_INT(PYC_OPTION_2_rB)] = str	
//		
//		// option 3
//		str = g_PrivateYachtDetails[i].strIPL[0]
//		str += "_option3"
//		g_PrivateYachtDetails[i].strIPL[ENUM_TO_INT(PYC_OPTION_3)] = str
//		
//		// option 3 rA
//		str = g_PrivateYachtDetails[i].strIPL[0]
//		str += "_option3_rA"
//		g_PrivateYachtDetails[i].strIPL[ENUM_TO_INT(PYC_OPTION_3_rA)] = str
//		
//		// option 3 rB
//		str = g_PrivateYachtDetails[i].strIPL[0]
//		str += "_option3_rB"
//		g_PrivateYachtDetails[i].strIPL[ENUM_TO_INT(PYC_OPTION_3_rB)] = str			
		
	ENDREPEAT
	
	
	// shore spawn points
	g_PrivateYachtDetails[0].ShoreSpawn[0].vCoords = <<-3215.5286, 1337.0327, 1.2217>>
	g_PrivateYachtDetails[0].ShoreSpawn[0].fHeading = 233.0511
	g_PrivateYachtDetails[0].ShoreSpawn[1].vCoords = <<-3101.0115, 1437.8131, 16.7490>>
	g_PrivateYachtDetails[0].ShoreSpawn[1].fHeading = 68.4861
	g_PrivateYachtDetails[0].ShoreSpawn[2].vCoords = <<-3262.4160, 1343.0431, 0.0>>
	g_PrivateYachtDetails[0].ShoreSpawn[2].fHeading = 64.1521
	
	g_PrivateYachtDetails[1].ShoreSpawn[0].vCoords = <<-2907.3110, 3083.4292, 2.1057>>
	g_PrivateYachtDetails[1].ShoreSpawn[0].fHeading = 315.0847
	g_PrivateYachtDetails[1].ShoreSpawn[1].vCoords = <<-2917.6414, 3057.1531, 2.3254>>
	g_PrivateYachtDetails[1].ShoreSpawn[1].fHeading = 139.5540
	g_PrivateYachtDetails[1].ShoreSpawn[2].vCoords = <<-2959.7612, 3069.2905, 0.0>>
	g_PrivateYachtDetails[1].ShoreSpawn[2].fHeading = 140.7879	

	g_PrivateYachtDetails[2].ShoreSpawn[0].vCoords = <<-2781.9832, 2347.4463, 1.4625>>
	g_PrivateYachtDetails[2].ShoreSpawn[0].fHeading = 301.6382
	g_PrivateYachtDetails[2].ShoreSpawn[1].vCoords = <<-2817.1616, 2320.7517, 1.6080>>
	g_PrivateYachtDetails[2].ShoreSpawn[1].fHeading = 76.4715
	g_PrivateYachtDetails[2].ShoreSpawn[2].vCoords = <<-2846.7288, 2354.8430, 0.0>>
	g_PrivateYachtDetails[2].ShoreSpawn[2].fHeading = 109.8094
	
	g_PrivateYachtDetails[3].ShoreSpawn[0].vCoords = <<-2498.8440, 3752.3284, 14.2509>>
	g_PrivateYachtDetails[3].ShoreSpawn[0].fHeading = 217.6760
	g_PrivateYachtDetails[3].ShoreSpawn[1].vCoords = <<-2502.9124, 3719.5156, 13.1249>>
	g_PrivateYachtDetails[3].ShoreSpawn[1].fHeading = 55.3889
	g_PrivateYachtDetails[3].ShoreSpawn[2].vCoords = <<-2638.5295, 3864.1230, 0.0>>
	g_PrivateYachtDetails[3].ShoreSpawn[2].fHeading = 59.3129
	
	g_PrivateYachtDetails[4].ShoreSpawn[0].vCoords = <<-3043.0234, 3440.0017, 2.8783>>
	g_PrivateYachtDetails[4].ShoreSpawn[0].fHeading = 267.8972 
	g_PrivateYachtDetails[4].ShoreSpawn[1].vCoords = <<-2972.3223, 3456.3098, 8.8206>>
	g_PrivateYachtDetails[4].ShoreSpawn[1].fHeading = 40.0795
	g_PrivateYachtDetails[4].ShoreSpawn[2].vCoords = <<-3107.2778, 3475.4226, 0.0>>
	g_PrivateYachtDetails[4].ShoreSpawn[2].fHeading = 33.9091
	
	g_PrivateYachtDetails[5].ShoreSpawn[0].vCoords = <<-2408.2214, 4243.0859, 9.0072>>
	g_PrivateYachtDetails[5].ShoreSpawn[0].fHeading = 291.9838
	g_PrivateYachtDetails[5].ShoreSpawn[1].vCoords = <<-2418.3411, 4218.9063, 8.7096>>
	g_PrivateYachtDetails[5].ShoreSpawn[1].fHeading = 345.1725
	g_PrivateYachtDetails[5].ShoreSpawn[2].vCoords = <<-2392.8477, 4424.4688, 0.0>>
	g_PrivateYachtDetails[5].ShoreSpawn[2].fHeading = 24.1730	
	
	g_PrivateYachtDetails[6].ShoreSpawn[0].vCoords = <<-3093.1008, 192.3021, 5.8948>>
	g_PrivateYachtDetails[6].ShoreSpawn[0].fHeading = 282.9320
	g_PrivateYachtDetails[6].ShoreSpawn[1].vCoords = <<-3078.0696, 154.1856, 9.8672>>
	g_PrivateYachtDetails[6].ShoreSpawn[1].fHeading = 122.1543
	g_PrivateYachtDetails[6].ShoreSpawn[2].vCoords = <<-3135.2104, 65.4845, 0.0>>
	g_PrivateYachtDetails[6].ShoreSpawn[2].fHeading = 109.4958
	
	g_PrivateYachtDetails[7].ShoreSpawn[0].vCoords = <<-3067.8333, 182.1174, 10.4800>>
	g_PrivateYachtDetails[7].ShoreSpawn[0].fHeading = 316.2727
	g_PrivateYachtDetails[7].ShoreSpawn[1].vCoords = <<-3092.8096, 432.5182, 1.3684>>
	g_PrivateYachtDetails[7].ShoreSpawn[1].fHeading = 85.3024
	g_PrivateYachtDetails[7].ShoreSpawn[2].vCoords = <<-3211.3191, 290.4250, 0.0>>
	g_PrivateYachtDetails[7].ShoreSpawn[2].fHeading = 92.1519
	
	g_PrivateYachtDetails[8].ShoreSpawn[0].vCoords = <<-2702.6440, -104.7734, 4.2172>>
	g_PrivateYachtDetails[8].ShoreSpawn[0].fHeading = 353.8816
	g_PrivateYachtDetails[8].ShoreSpawn[1].vCoords = <<-2823.5728, -29.9384, 3.1771>>
	g_PrivateYachtDetails[8].ShoreSpawn[1].fHeading = 178.7297
	g_PrivateYachtDetails[8].ShoreSpawn[2].vCoords = <<-2734.0925, -154.0857, 0.0>>
	g_PrivateYachtDetails[8].ShoreSpawn[2].fHeading = 141.6168
	
	g_PrivateYachtDetails[9].ShoreSpawn[0].vCoords = <<-1463.4625, -1393.5692, 1.5368>>
	g_PrivateYachtDetails[9].ShoreSpawn[0].fHeading = 287.3892
	g_PrivateYachtDetails[9].ShoreSpawn[1].vCoords = <<-1424.5920, -1404.7458, 3.1770>>
	g_PrivateYachtDetails[9].ShoreSpawn[1].fHeading = 100.2350
	g_PrivateYachtDetails[9].ShoreSpawn[2].vCoords = <<-1534.5277, -1438.4449, 0.0>>
	g_PrivateYachtDetails[9].ShoreSpawn[2].fHeading = 121.4151
	
	g_PrivateYachtDetails[10].ShoreSpawn[0].vCoords = <<-1239.7362, -2066.0000, 13.2375>>
	g_PrivateYachtDetails[10].ShoreSpawn[0].fHeading = 297.0490
	g_PrivateYachtDetails[10].ShoreSpawn[1].vCoords = <<-1332.8740, -2123.4058, 13.1281>>
	g_PrivateYachtDetails[10].ShoreSpawn[1].fHeading = 75.7300
	g_PrivateYachtDetails[10].ShoreSpawn[2].vCoords = <<-1402.5754, -2094.5222, 0.0>>
	g_PrivateYachtDetails[10].ShoreSpawn[2].fHeading = 70.1691
	
	g_PrivateYachtDetails[11].ShoreSpawn[0].vCoords = <<-1224.0404, -1777.9192, 2.2519>>
	g_PrivateYachtDetails[11].ShoreSpawn[0].fHeading = 335.8759
	g_PrivateYachtDetails[11].ShoreSpawn[1].vCoords = <<-1217.1301, -1833.2988, 1.5737>>
	g_PrivateYachtDetails[11].ShoreSpawn[1].fHeading = 123.5309
	g_PrivateYachtDetails[11].ShoreSpawn[2].vCoords = <<-1277.2784, -1875.2473, 0.0>>
	g_PrivateYachtDetails[11].ShoreSpawn[2].fHeading = 118.8218
	
	g_PrivateYachtDetails[12].ShoreSpawn[0].vCoords = <<108.4327, -3325.7793, 5.0000>>
	g_PrivateYachtDetails[12].ShoreSpawn[0].fHeading = 0.5520
	g_PrivateYachtDetails[12].ShoreSpawn[1].vCoords = <<134.3913, -2804.3577, 5.0002>> 
	g_PrivateYachtDetails[12].ShoreSpawn[1].fHeading = 223.5200
	g_PrivateYachtDetails[12].ShoreSpawn[2].vCoords = <<98.6531, -3324.3389, 0.0>>
	g_PrivateYachtDetails[12].ShoreSpawn[2].fHeading = 167.3062
	
	g_PrivateYachtDetails[13].ShoreSpawn[0].vCoords = <<108.5783, -3258.3467, 5.0000>>
	g_PrivateYachtDetails[13].ShoreSpawn[0].fHeading = 359.6605
	g_PrivateYachtDetails[13].ShoreSpawn[1].vCoords = <<149.4426, -2791.0391, 5.0002>>
	g_PrivateYachtDetails[13].ShoreSpawn[1].fHeading = 218.7591
	g_PrivateYachtDetails[13].ShoreSpawn[2].vCoords = <<94.8648, -3249.4863, 0.0>>
	g_PrivateYachtDetails[13].ShoreSpawn[2].fHeading = 164.5406
	
	g_PrivateYachtDetails[14].ShoreSpawn[0].vCoords = <<-406.9930, -2851.1216, 5.0004>>
	g_PrivateYachtDetails[14].ShoreSpawn[0].fHeading = 315.8702
	g_PrivateYachtDetails[14].ShoreSpawn[1].vCoords = <<-380.5807, -2820.3872, 5.0003>>
	g_PrivateYachtDetails[14].ShoreSpawn[1].fHeading = 160.4800
	g_PrivateYachtDetails[14].ShoreSpawn[2].vCoords = <<-456.5881, -2906.1570, 0.0>>
	g_PrivateYachtDetails[14].ShoreSpawn[2].fHeading = 162.6687
	
	g_PrivateYachtDetails[15].ShoreSpawn[0].vCoords = <<1292.6520, -3088.6887, 4.9066>> 
	g_PrivateYachtDetails[15].ShoreSpawn[0].fHeading = 60.2816
	g_PrivateYachtDetails[15].ShoreSpawn[1].vCoords = <<1288.9417, -3074.0293, 4.9340>> 
	g_PrivateYachtDetails[15].ShoreSpawn[1].fHeading = 258.5226
	g_PrivateYachtDetails[15].ShoreSpawn[2].vCoords = <<1308.6777, -3083.9841, 0.0>> 
	g_PrivateYachtDetails[15].ShoreSpawn[2].fHeading = 343.9506

	g_PrivateYachtDetails[16].ShoreSpawn[0].vCoords = <<1206.3674, -2669.0615, 3.0010>> 
	g_PrivateYachtDetails[16].ShoreSpawn[0].fHeading = 77.4622
	g_PrivateYachtDetails[16].ShoreSpawn[1].vCoords = <<1377.1730, -2710.0352, 2.1007>> 
	g_PrivateYachtDetails[16].ShoreSpawn[1].fHeading = 178.4367
	g_PrivateYachtDetails[16].ShoreSpawn[2].vCoords = <<1313.2308, -2800.3259, 0.0>> 
	g_PrivateYachtDetails[16].ShoreSpawn[2].fHeading = 250.5272

	g_PrivateYachtDetails[17].ShoreSpawn[0].vCoords = <<1288.7203, -3343.1267, 4.9016>> 
	g_PrivateYachtDetails[17].ShoreSpawn[0].fHeading = 83.7660
	g_PrivateYachtDetails[17].ShoreSpawn[1].vCoords = <<1291.0033, -3327.3503, 4.9049>> 
	g_PrivateYachtDetails[17].ShoreSpawn[1].fHeading = 271.1578
	g_PrivateYachtDetails[17].ShoreSpawn[2].vCoords = <<1301.8271, -3357.4990, 0.0>> 
	g_PrivateYachtDetails[17].ShoreSpawn[2].fHeading = 301.3926

	g_PrivateYachtDetails[18].ShoreSpawn[0].vCoords = <<2797.3657, -698.9788, 2.5743>> 
	g_PrivateYachtDetails[18].ShoreSpawn[0].fHeading = 119.8324
	g_PrivateYachtDetails[18].ShoreSpawn[1].vCoords = <<2684.6345, -863.2911, 24.9476>> 
	g_PrivateYachtDetails[18].ShoreSpawn[1].fHeading = 212.3963
	g_PrivateYachtDetails[18].ShoreSpawn[2].vCoords = <<2871.9854, -733.9852, 0.0>> 
	g_PrivateYachtDetails[18].ShoreSpawn[2].fHeading = 228.3137

	g_PrivateYachtDetails[19].ShoreSpawn[0].vCoords = <<2799.0713, -713.1099, 3.5076>> 
	g_PrivateYachtDetails[19].ShoreSpawn[0].fHeading = 92.0258
	g_PrivateYachtDetails[19].ShoreSpawn[1].vCoords = <<2784.0935, -730.8027, 5.7956>> 
	g_PrivateYachtDetails[19].ShoreSpawn[1].fHeading = 299.0930
	g_PrivateYachtDetails[19].ShoreSpawn[2].vCoords = <<2862.4736, -601.0669, 0.0>> 
	g_PrivateYachtDetails[19].ShoreSpawn[2].fHeading = 267.4701

	g_PrivateYachtDetails[20].ShoreSpawn[0].vCoords = <<2803.4150, -676.9994, 1.6518>> 
	g_PrivateYachtDetails[20].ShoreSpawn[0].fHeading = 78.5588
	g_PrivateYachtDetails[20].ShoreSpawn[1].vCoords = <<2696.7485, -828.5058, 27.1032>> 
	g_PrivateYachtDetails[20].ShoreSpawn[1].fHeading = 193.2687
	g_PrivateYachtDetails[20].ShoreSpawn[2].vCoords = <<2866.8372, -791.7337, 0.0>> 
	g_PrivateYachtDetails[20].ShoreSpawn[2].fHeading = 232.5099

	g_PrivateYachtDetails[21].ShoreSpawn[0].vCoords = <<2768.2124, 1263.6573, 2.8663>> 
	g_PrivateYachtDetails[21].ShoreSpawn[0].fHeading = 118.7555
	g_PrivateYachtDetails[21].ShoreSpawn[1].vCoords = <<2566.1748, 1751.5265, 23.7902>> 
	g_PrivateYachtDetails[21].ShoreSpawn[1].fHeading = 273.4148 
	g_PrivateYachtDetails[21].ShoreSpawn[2].vCoords = <<2820.8628, 1272.8854, 0.0>> 
	g_PrivateYachtDetails[21].ShoreSpawn[2].fHeading = 275.3719

	g_PrivateYachtDetails[22].ShoreSpawn[0].vCoords = <<2774.6990, 1207.8546, 0.7296>> 
	g_PrivateYachtDetails[22].ShoreSpawn[0].fHeading = 90.8381 
	g_PrivateYachtDetails[22].ShoreSpawn[1].vCoords = <<2567.0396, 931.3610, 82.2892>> 
	g_PrivateYachtDetails[22].ShoreSpawn[1].fHeading = 294.8305
	g_PrivateYachtDetails[22].ShoreSpawn[2].vCoords = <<2813.2529, 1202.9548, 0.0>> 
	g_PrivateYachtDetails[22].ShoreSpawn[2].fHeading = 268.3473

	g_PrivateYachtDetails[23].ShoreSpawn[0].vCoords = <<3866.2148, 4345.1816, 5.5636>> 
	g_PrivateYachtDetails[23].ShoreSpawn[0].fHeading = 24.0254
	g_PrivateYachtDetails[23].ShoreSpawn[1].vCoords = <<2700.4700, 2979.3467, 35.5843>> 
	g_PrivateYachtDetails[23].ShoreSpawn[1].fHeading = 211.7970
	g_PrivateYachtDetails[23].ShoreSpawn[2].vCoords = <<3892.0105, 4291.3687, 0.0>> 
	g_PrivateYachtDetails[23].ShoreSpawn[2].fHeading = 228.3766

	g_PrivateYachtDetails[24].ShoreSpawn[0].vCoords = <<3868.0266, 4327.9463, 5.1203>> 
	g_PrivateYachtDetails[24].ShoreSpawn[0].fHeading = 5.5954
	g_PrivateYachtDetails[24].ShoreSpawn[1].vCoords = <<3928.7659, 4393.2178, 15.5509>> 
	g_PrivateYachtDetails[24].ShoreSpawn[1].fHeading = 292.8381
	g_PrivateYachtDetails[24].ShoreSpawn[2].vCoords = <<3907.1599, 4311.7227, 0.0>> 
	g_PrivateYachtDetails[24].ShoreSpawn[2].fHeading = 256.1528

	g_PrivateYachtDetails[25].ShoreSpawn[0].vCoords = <<3809.3376, 4456.8545, 3.1726>> 
	g_PrivateYachtDetails[25].ShoreSpawn[0].fHeading = 74.3336
	g_PrivateYachtDetails[25].ShoreSpawn[1].vCoords = <<3788.6233, 4507.4893, 6.0836>> 
	g_PrivateYachtDetails[25].ShoreSpawn[1].fHeading = 328.0288
	g_PrivateYachtDetails[25].ShoreSpawn[2].vCoords = <<3854.0210, 4504.2681, 0.0>> 
	g_PrivateYachtDetails[25].ShoreSpawn[2].fHeading = 285.2981

	g_PrivateYachtDetails[26].ShoreSpawn[0].vCoords = <<3811.9473, 4469.6689, 2.9054>> 
	g_PrivateYachtDetails[26].ShoreSpawn[0].fHeading = 120.0346
	g_PrivateYachtDetails[26].ShoreSpawn[1].vCoords = <<3847.5308, 4397.8936, 3.1748>>  
	g_PrivateYachtDetails[26].ShoreSpawn[1].fHeading = 338.2054
	g_PrivateYachtDetails[26].ShoreSpawn[2].vCoords = <<3868.6265, 4442.4028, 0.0>>
	g_PrivateYachtDetails[26].ShoreSpawn[2].fHeading = 281.4086

	g_PrivateYachtDetails[27].ShoreSpawn[0].vCoords = <<3258.9163, 5217.6963, 18.9450>> 
	g_PrivateYachtDetails[27].ShoreSpawn[0].fHeading = 181.2822 
	g_PrivateYachtDetails[27].ShoreSpawn[1].vCoords = <<3275.7273, 5199.2183, 17.9096>> 
	g_PrivateYachtDetails[27].ShoreSpawn[1].fHeading = 328.5930
	g_PrivateYachtDetails[27].ShoreSpawn[2].vCoords = <<3248.6433, 5322.5044, 0.0>> 
	g_PrivateYachtDetails[27].ShoreSpawn[2].fHeading = 274.5202

	g_PrivateYachtDetails[28].ShoreSpawn[0].vCoords = <<3338.1846, 5573.4429, 20.3965>> 
	g_PrivateYachtDetails[28].ShoreSpawn[0].fHeading = 200.2075
	g_PrivateYachtDetails[28].ShoreSpawn[1].vCoords = <<3368.6536, 5570.1812, 13.6552>>
	g_PrivateYachtDetails[28].ShoreSpawn[1].fHeading = 359.9985
	g_PrivateYachtDetails[28].ShoreSpawn[2].vCoords = <<3386.8867, 5696.8638, 0.0>> 
	g_PrivateYachtDetails[28].ShoreSpawn[2].fHeading = 285.1223

	g_PrivateYachtDetails[29].ShoreSpawn[0].vCoords = <<3780.4744, 4477.6982, 5.2422>> 
	g_PrivateYachtDetails[29].ShoreSpawn[0].fHeading = 135.4026
	g_PrivateYachtDetails[29].ShoreSpawn[1].vCoords = <<3392.1343, 4993.0508, 30.5221>> 
	g_PrivateYachtDetails[29].ShoreSpawn[1].fHeading = 300.8156
	g_PrivateYachtDetails[29].ShoreSpawn[2].vCoords = <<3800.5732, 4576.0322, 0.0>> 
	g_PrivateYachtDetails[29].ShoreSpawn[2].fHeading = 328.0469

	g_PrivateYachtDetails[30].ShoreSpawn[0].vCoords = <<265.2008, 6992.3252, 2.9654>> 
	g_PrivateYachtDetails[30].ShoreSpawn[0].fHeading = 165.6404
	g_PrivateYachtDetails[30].ShoreSpawn[1].vCoords = <<228.2158, 6992.4810, 1.5349>> 
	g_PrivateYachtDetails[30].ShoreSpawn[1].fHeading = 338.5522
	g_PrivateYachtDetails[30].ShoreSpawn[2].vCoords = <<309.4163, 7025.8320, 0.0>> 
	g_PrivateYachtDetails[30].ShoreSpawn[2].fHeading = 298.1702

	g_PrivateYachtDetails[31].ShoreSpawn[0].vCoords = <<1459.0299, 6570.5986, 12.3527>> 
	g_PrivateYachtDetails[31].ShoreSpawn[0].fHeading = 133.2433
	g_PrivateYachtDetails[31].ShoreSpawn[1].vCoords = <<1618.7212, 6659.7656, 22.3415>> 
	g_PrivateYachtDetails[31].ShoreSpawn[1].fHeading = 342.8323
	g_PrivateYachtDetails[31].ShoreSpawn[2].vCoords = <<1563.6287, 6684.2720, 0.0>> 
	g_PrivateYachtDetails[31].ShoreSpawn[2].fHeading = 357.4255

	g_PrivateYachtDetails[32].ShoreSpawn[0].vCoords = <<1390.5017, 6592.2861, 11.5278>> 
	g_PrivateYachtDetails[32].ShoreSpawn[0].fHeading = 283.1772
	g_PrivateYachtDetails[32].ShoreSpawn[1].vCoords = <<1142.3761, 6535.1055, 17.5983>> 
	g_PrivateYachtDetails[32].ShoreSpawn[1].fHeading = 342.9795 
	g_PrivateYachtDetails[32].ShoreSpawn[2].vCoords = <<1280.1469, 6660.7207, 0.0>> 
	g_PrivateYachtDetails[32].ShoreSpawn[2].fHeading = 344.6634

	g_PrivateYachtDetails[33].ShoreSpawn[0].vCoords = <<-817.0295, 5881.5435, 6.2074>> 
	g_PrivateYachtDetails[33].ShoreSpawn[0].fHeading = 258.9158
	g_PrivateYachtDetails[33].ShoreSpawn[1].vCoords = <<-821.1178, 5935.9595, 19.9904>> 
	g_PrivateYachtDetails[33].ShoreSpawn[1].fHeading = 129.2778
	g_PrivateYachtDetails[33].ShoreSpawn[2].vCoords = <<-915.4951, 5851.0796, 0.0>> 
	g_PrivateYachtDetails[33].ShoreSpawn[2].fHeading = 79.7242

	g_PrivateYachtDetails[34].ShoreSpawn[0].vCoords = <<-635.6970, 6156.9326, 1.9706>> 
	g_PrivateYachtDetails[34].ShoreSpawn[0].fHeading = 272.4947
	g_PrivateYachtDetails[34].ShoreSpawn[1].vCoords = <<-560.5052, 6254.3252, 7.6306>> 
	g_PrivateYachtDetails[34].ShoreSpawn[1].fHeading = 42.5652
	g_PrivateYachtDetails[34].ShoreSpawn[2].vCoords = <<-685.4041, 6252.0361, 0.0>> 
	g_PrivateYachtDetails[34].ShoreSpawn[2].fHeading = 51.8815 

	g_PrivateYachtDetails[35].ShoreSpawn[0].vCoords = <<-99.1651, 6722.0078, 0.5012>> 
	g_PrivateYachtDetails[35].ShoreSpawn[0].fHeading = 268.1040
	g_PrivateYachtDetails[35].ShoreSpawn[1].vCoords = <<-142.6568, 6651.6846, 0.9997>> 
	g_PrivateYachtDetails[35].ShoreSpawn[1].fHeading = 32.9657
	g_PrivateYachtDetails[35].ShoreSpawn[2].vCoords = <<-185.4709, 6733.2729, 0.0>> 
	g_PrivateYachtDetails[35].ShoreSpawn[2].fHeading = 14.6319
	

	// spawn location offsets on yachts
	g_PrivateYachtSpawnLocationOffest[0].vPlayerLoc = <<5.8173, -48.2285, -5.5287>>
	g_PrivateYachtSpawnLocationOffest[0].fPlayerHeading = 30.0067

	g_PrivateYachtSpawnLocationOffest[1].vPlayerLoc = <<-5.0147, -46.5139, -5.5286>>
	g_PrivateYachtSpawnLocationOffest[1].fPlayerHeading = 198.2057
	
	g_PrivateYachtSpawnLocationOffest[2].vPlayerLoc = <<-0.3064, -40.6402, -2.4310>>
	g_PrivateYachtSpawnLocationOffest[2].fPlayerHeading = 190.9040
	
	g_PrivateYachtSpawnLocationOffest[3].vPlayerLoc = <<6.7080, -25.9916, 0.4969>>
	g_PrivateYachtSpawnLocationOffest[3].fPlayerHeading = 286.8118
	
	g_PrivateYachtSpawnLocationOffest[4].vPlayerLoc = <<-3.9829, -25.1820, 0.4970>>
	g_PrivateYachtSpawnLocationOffest[4].fPlayerHeading = 187.8127
	
	g_PrivateYachtSpawnLocationOffest[5].vPlayerLoc = <<1.1870, -14.8577, 0.4968>>
	g_PrivateYachtSpawnLocationOffest[5].fPlayerHeading = 223.2354	
	
	g_PrivateYachtSpawnLocationOffest[6].vPlayerLoc =  <<1.9789, -43.6601, -5.5216>>
	g_PrivateYachtSpawnLocationOffest[6].fPlayerHeading =  132.0817

	g_PrivateYachtSpawnLocationOffest[7].vPlayerLoc =  <<-2.2018, -43.2581, -5.5209>>
	g_PrivateYachtSpawnLocationOffest[7].fPlayerHeading =  210.6833

	g_PrivateYachtSpawnLocationOffest[8].vPlayerLoc =  <<-3.5979, -37.4337, -2.4323>>
	g_PrivateYachtSpawnLocationOffest[8].fPlayerHeading =  232.0388

	g_PrivateYachtSpawnLocationOffest[9].vPlayerLoc =  <<3.5456, -36.9838, -2.4323>>
	g_PrivateYachtSpawnLocationOffest[9].fPlayerHeading =  141.3021

	g_PrivateYachtSpawnLocationOffest[10].vPlayerLoc =  <<2.0707, -34.8316, -2.4323>>
	g_PrivateYachtSpawnLocationOffest[10].fPlayerHeading =  157.8206

	g_PrivateYachtSpawnLocationOffest[11].vPlayerLoc =  <<-1.0928, -33.9597, -2.4323>>
	g_PrivateYachtSpawnLocationOffest[11].fPlayerHeading =  175.8183

	g_PrivateYachtSpawnLocationOffest[12].vPlayerLoc =  <<-5.8420, -28.0408, 0.5039>>
	g_PrivateYachtSpawnLocationOffest[12].fPlayerHeading =  215.1446

	g_PrivateYachtSpawnLocationOffest[13].vPlayerLoc =  <<-1.4056, -15.1676, 0.5038>>
	g_PrivateYachtSpawnLocationOffest[13].fPlayerHeading =  161.5367

	g_PrivateYachtSpawnLocationOffest[14].vPlayerLoc =  <<-1.4056, -15.1676, 0.5038>>
	g_PrivateYachtSpawnLocationOffest[14].fPlayerHeading =  161.5367

	g_PrivateYachtSpawnLocationOffest[15].vPlayerLoc =  <<5.7803, -18.6142, 0.5038>>
	g_PrivateYachtSpawnLocationOffest[15].fPlayerHeading =  232.0092

	g_PrivateYachtSpawnLocationOffest[16].vPlayerLoc =  <<4.8851, -27.8689, 0.5040>>
	g_PrivateYachtSpawnLocationOffest[16].fPlayerHeading =  159.7201

	g_PrivateYachtSpawnLocationOffest[17].vPlayerLoc =  <<6.4316, -51.9249, -5.5218>>
	g_PrivateYachtSpawnLocationOffest[17].fPlayerHeading =  171.7628

	g_PrivateYachtSpawnLocationOffest[18].vPlayerLoc =  <<-6.4454, -51.5630, -5.5221>>
	g_PrivateYachtSpawnLocationOffest[18].fPlayerHeading =  170.0196

	g_PrivateYachtSpawnLocationOffest[19].vPlayerLoc =  <<-6.0149, -38.0291, -5.5207>>
	g_PrivateYachtSpawnLocationOffest[19].fPlayerHeading =  141.0158

	g_PrivateYachtSpawnLocationOffest[20].vPlayerLoc =  <<5.9235, -38.2919, -5.5218>>
	g_PrivateYachtSpawnLocationOffest[20].fPlayerHeading =  251.4813

	g_PrivateYachtSpawnLocationOffest[21].vPlayerLoc =  <<6.8878, -27.0348, -5.5218>>
	g_PrivateYachtSpawnLocationOffest[21].fPlayerHeading =  227.1692

	g_PrivateYachtSpawnLocationOffest[22].vPlayerLoc =  <<6.8878, -27.0348, -5.5218>>
	g_PrivateYachtSpawnLocationOffest[22].fPlayerHeading =  227.1692

	g_PrivateYachtSpawnLocationOffest[23].vPlayerLoc =  <<-6.4161, -33.4939, -5.5201>>
	g_PrivateYachtSpawnLocationOffest[23].fPlayerHeading =  129.7836

	g_PrivateYachtSpawnLocationOffest[24].vPlayerLoc =  <<-6.4161, -33.4939, -5.5201>>
	g_PrivateYachtSpawnLocationOffest[24].fPlayerHeading =  129.7836

	g_PrivateYachtSpawnLocationOffest[25].vPlayerLoc =  <<-6.8883, -28.2909, -5.5199>>
	g_PrivateYachtSpawnLocationOffest[25].fPlayerHeading =  176.8486

	g_PrivateYachtSpawnLocationOffest[26].vPlayerLoc =  <<-6.5118, -25.1397, -5.5203>>
	g_PrivateYachtSpawnLocationOffest[26].fPlayerHeading =  113.7529
		
	g_PrivateYachtSpawnLocationOffest[27].vPlayerLoc =  <<-6.2417, -33.0709, -0.6002>>
	g_PrivateYachtSpawnLocationOffest[27].fPlayerHeading = 188.1441

	g_PrivateYachtSpawnLocationOffest[28].vPlayerLoc =  <<6.1422, -32.3880, -0.2794>>
	g_PrivateYachtSpawnLocationOffest[28].fPlayerHeading = 165.4950

	g_PrivateYachtSpawnLocationOffest[29].vPlayerLoc =  <<-0.7784, -22.1027, 0.5038>>
	g_PrivateYachtSpawnLocationOffest[29].fPlayerHeading = 172.0094
		
	
	// spawn locations for inside yacht apartment
	g_PrivateYachtSpawnLocationApartmentOffest[0].vPlayerLoc = <<-0.8319, 12.3016, -5.5208>>
	g_PrivateYachtSpawnLocationApartmentOffest[0].fPlayerHeading = 270.5026
	
	g_PrivateYachtSpawnLocationApartmentOffest[1].vPlayerLoc = <<-4.6730, 24.2213, -5.5210>>
	g_PrivateYachtSpawnLocationApartmentOffest[1].fPlayerHeading = 164.5550
	
	g_PrivateYachtSpawnLocationApartmentOffest[2].vPlayerLoc = <<-2.4334, 5.4224, -2.4335>>
	g_PrivateYachtSpawnLocationApartmentOffest[2].fPlayerHeading = 270.4796
	
	
	
	// spawn vehicles on yachts
	SET_PRIVATE_YACHT_VEHICLES_INITIAL_VALUES_LEVEL_3()


	// zone coordsF
	g_PrivateYachtZoneCoords[0] = <<0.0, 0.0, 0.0>> 				
	g_PrivateYachtZoneCoords[1] = g_PrivateYachtDetails[3].vInteriorCoords + g_PrivateYachtDetails[4].vInteriorCoords + g_PrivateYachtDetails[5].vInteriorCoords
	g_PrivateYachtZoneCoords[2] = g_PrivateYachtDetails[0].vInteriorCoords + g_PrivateYachtDetails[1].vInteriorCoords + g_PrivateYachtDetails[2].vInteriorCoords 
	g_PrivateYachtZoneCoords[3] = g_PrivateYachtDetails[6].vInteriorCoords + g_PrivateYachtDetails[7].vInteriorCoords + g_PrivateYachtDetails[8].vInteriorCoords
	g_PrivateYachtZoneCoords[4] = g_PrivateYachtDetails[9].vInteriorCoords + g_PrivateYachtDetails[10].vInteriorCoords + g_PrivateYachtDetails[11].vInteriorCoords
	g_PrivateYachtZoneCoords[5] = g_PrivateYachtDetails[12].vInteriorCoords + g_PrivateYachtDetails[13].vInteriorCoords + g_PrivateYachtDetails[14].vInteriorCoords
	g_PrivateYachtZoneCoords[6] = g_PrivateYachtDetails[15].vInteriorCoords + g_PrivateYachtDetails[16].vInteriorCoords + g_PrivateYachtDetails[17].vInteriorCoords
	g_PrivateYachtZoneCoords[7] = g_PrivateYachtDetails[18].vInteriorCoords + g_PrivateYachtDetails[19].vInteriorCoords + g_PrivateYachtDetails[20].vInteriorCoords
	g_PrivateYachtZoneCoords[8] = g_PrivateYachtDetails[21].vInteriorCoords + g_PrivateYachtDetails[22].vInteriorCoords + g_PrivateYachtDetails[23].vInteriorCoords
	g_PrivateYachtZoneCoords[9] = g_PrivateYachtDetails[24].vInteriorCoords + g_PrivateYachtDetails[25].vInteriorCoords + g_PrivateYachtDetails[26].vInteriorCoords
	g_PrivateYachtZoneCoords[10] = g_PrivateYachtDetails[27].vInteriorCoords + g_PrivateYachtDetails[28].vInteriorCoords + g_PrivateYachtDetails[29].vInteriorCoords
	g_PrivateYachtZoneCoords[11] = g_PrivateYachtDetails[30].vInteriorCoords + g_PrivateYachtDetails[31].vInteriorCoords + g_PrivateYachtDetails[32].vInteriorCoords
	g_PrivateYachtZoneCoords[12] = g_PrivateYachtDetails[33].vInteriorCoords + g_PrivateYachtDetails[34].vInteriorCoords + g_PrivateYachtDetails[35].vInteriorCoords	
	REPEAT 13 i
		g_PrivateYachtZoneCoords[i] = g_PrivateYachtZoneCoords[i] * 0.333333
	ENDREPEAT
	
	// Buoys locations
	g_PrivateYachtBuoysCoords[0] = <<7.870, -54.898, -10.9966>>
	g_PrivateYachtBuoysCoords[1] = <<2.417, -60.360, -10.9442>>
	g_PrivateYachtBuoysCoords[2] = <<-2.4168, -60.360, -10.9442>>
	g_PrivateYachtBuoysCoords[3] = <<-7.870, -54.898, -10.9966>>
	
	
	//INIT_YACHT_COORDS()	
	
ENDPROC

FUNC BOOL IS_PRIVATE_YACHT_BUOY_FLAG_SET(INT iYachtID, INT Buoy, PRIVATE_YACHT_VEHICLE_FLAGS eFlag)
	INT iFlags = GET_BUOY_FLAG(iYachtID, Buoy)
	IF ((iFlags & ENUM_TO_INT(eFlag)) != 0)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(INT iVehicleID, PRIVATE_YACHT_VEHICLE_FLAGS eFlag)
	IF ((gPrivateYachtSpawnVehicleDetails[iVehicleID].iFlags & ENUM_TO_INT(eFlag)) != 0)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC SET_PRIVATE_YACHT_VEHICLE_FLAG(INT iVehicleID, PRIVATE_YACHT_VEHICLE_FLAGS eFlag, BOOL bSet)	
	CPRINTLN(DEBUG_YACHT, "SET_PRIVATE_YACHT_VEHICLE_FLAG - called with ", iVehicleID, ", ", ENUM_TO_INT(eFlag), ", ", bSet)
	DEBUG_PRINTCALLSTACK()	
	IF (bSet)
		IF NOT IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicleID, eFlag)
			gPrivateYachtSpawnVehicleDetails[iVehicleID].iFlags += ENUM_TO_INT(eFlag)	
		ELSE
			CPRINTLN(DEBUG_YACHT, "SET_PRIVATE_YACHT_VEHICLE_FLAG - flag is already set!")
		ENDIF
	ELSE
		IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicleID, eFlag)
			gPrivateYachtSpawnVehicleDetails[iVehicleID].iFlags -= ENUM_TO_INT(eFlag)	
		ELSE
			CPRINTLN(DEBUG_YACHT, "SET_PRIVATE_YACHT_VEHICLE_FLAG - flag is already unset!")
		ENDIF	
	ENDIF
	CPRINTLN(DEBUG_YACHT, "SET_PRIVATE_YACHT_VEHICLE_FLAG - new value for vehicle ", iVehicleID, ", ", gPrivateYachtSpawnVehicleDetails[iVehicleID].iFlags)
ENDPROC


//FUNC VECTOR GET_COORD_OFFSET_FOR_PRIVATE_YACHT(INT iYacht)
//	VECTOR vReturn = <<PRIVATE_YACHT_MAIN_COORDS_OFFSET_X, PRIVATE_YACHT_MAIN_COORDS_OFFSET_Y, PRIVATE_YACHT_MAIN_COORDS_OFFSET_Z>>
//	RotateVec(vReturn, <<0.0, 0.0, (g_PrivateYachtDetails[iYacht].fInteriorHeading + PRIVATE_YACHT_MAIN_HEADING_OFFSET) >>)
//	RETURN vReturn
//ENDFUNC

//FUNC VECTOR GET_COORDS_OF_PRIVATE_YACHT(INT iYachtID)
//	#IF IS_DEBUG_BUILD
//		IF (g_bUseDebugOffsetsForYacht)
//			g_PrivateYachtDetails[iYachtID].vCoords = g_PrivateYachtDetails[iYachtID].vInteriorCoords + GET_COORD_OFFSET_FOR_PRIVATE_YACHT(iYachtID)	
//		ENDIF
//	#ENDIF	
//	RETURN g_PrivateYachtDetails[iYachtID].vCoords
//ENDFUNC

FUNC VECTOR GET_MODEL_COORDS_OF_YACHT(INT iYachtID)
	RETURN g_PrivateYachtDetails[iYachtID].vModelCoords
ENDFUNC

FUNC FLOAT GET_MODEL_HEADING_OF_YACHT(INT iYachtID)
	RETURN g_PrivateYachtDetails[iYachtID].fModelHeading
ENDFUNC

FUNC VECTOR GET_PRIVATE_YACHT_BLIP_COORDS(INT iYachtID)
	RETURN g_PrivateYachtDetails[iYachtID].vBlipCoords	
ENDFUNC


FUNC BOOL IS_PRIVATE_YACHT_ACTIVE(INT iYachtID)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		RETURN MPGlobalsPrivateYacht.bPrivateYachtIsActive[iYachtID]
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PRIVATE_YACHT_FULLY_LOADED(INT iYachtID)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		RETURN MPGlobalsPrivateYacht.bPrivateYachtIsActive[iYachtID] AND MPGlobalsPrivateYacht.bPrivateYachtIsFullyLoaded[iYachtID]
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC INT GET_NEAREST_PRIVATE_YACHT_TO_POINT(VECTOR vPoint, BOOL bSearchOnlyActive)
	INT iNumberOfPrivateYachts = NUMBER_OF_PRIVATE_YACHTS - 6
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		iNumberOfPrivateYachts = NUMBER_OF_PRIVATE_YACHTS - 5
	ENDIF
	
	FLOAT fNearestDist = 999999.9
	INT iNearest = -1
	FLOAT fDist
	
	INT i
	REPEAT iNumberOfPrivateYachts i
		IF IS_PRIVATE_YACHT_ACTIVE(i)
		OR (bSearchOnlyActive = FALSE)
			fDist = VDIST(GET_COORDS_OF_PRIVATE_YACHT(i), vPoint)
			IF (fDist < fNearestDist)
				iNearest = i
				fNearestDist = fDist
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iNearest

ENDFUNC

FUNC INT GET_NEAREST_ACTIVE_PRIVATE_YACHT_TO_PLAYER(PLAYER_INDEX PlayerID)
	RETURN GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.iYachtIAmNearest
ENDFUNC


FUNC FLOAT GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(INT iYachtID, FLOAT fOffsetHeading)
	FLOAT fYachtHeading = GET_HEADING_OF_PRIVATE_YACHT(iYachtID)
	fOffsetHeading += fYachtHeading
	WHILE fOffsetHeading < 0.0
		fOffsetHeading += 360.0
	ENDWHILE
	WHILE fOffsetHeading >= 360.0
		fOffsetHeading -= 360.0
	ENDWHILE	
	RETURN fOffsetHeading
ENDFUNC

FUNC VECTOR GET_OFFSET_FROM_YACHT_MODEL_IN_WORLD_COORDS(INT iYachtID, VECTOR vOffset)
	RETURN GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS ( GET_MODEL_COORDS_OF_YACHT(iYachtID), GET_MODEL_HEADING_OF_YACHT(iYachtID), vOffset )
ENDFUNC

FUNC FLOAT GET_OFFSET_HEADING_FROM_YACHT_MODEL_AS_WORLD_HEADING(INT iYachtID, FLOAT fOffsetHeading)
	FLOAT fYachtHeading = GET_MODEL_HEADING_OF_YACHT(iYachtID)
	fOffsetHeading += fYachtHeading
	WHILE fOffsetHeading < 0.0
		fOffsetHeading += 360.0
	ENDWHILE
	WHILE fOffsetHeading >= 360.0
		fOffsetHeading -= 360.0
	ENDWHILE	
	RETURN fOffsetHeading
ENDFUNC

FUNC VECTOR GET_COORDS_AS_OFFSET_FROM_YACHT_GIVEN_WORLD_COORDS(INT iYachtID, VECTOR vCoords)
	
	VECTOR vVec = vCoords - GET_COORDS_OF_PRIVATE_YACHT(iYachtID)
	
	RotateVec(vVec, <<0.0, 0.0, GET_HEADING_OF_PRIVATE_YACHT(iYachtID) * -1.0 >>)
	
	RETURN vVec

ENDFUNC

FUNC FLOAT GET_HEADING_AS_OFFSET_FROM_YACHT_GIVEN_WORLD_HEADING(INT iYachtID, FLOAT fHeading)
	
	FLOAT fDiff = fHeading - GET_HEADING_OF_PRIVATE_YACHT(iYachtID) 
	
	WHILE fDiff < 0.0
		fDiff += 360.0
	ENDWHILE
	WHILE fDiff >= 360.0
		fDiff -= 360.0
	ENDWHILE
	
	RETURN fDiff
	
ENDFUNC

PROC GET_SPAWN_LOCATIONS_FOR_YACHT(INT iYachtID, YACHT_SPAWN_LOCATIONS &SpawnLocation[], BOOL bForApartment)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		INT i
		IF NOT (bForApartment)
			REPEAT NUMBER_OF_SPAWN_LOCATIONS_ON_YACHT_EXTERIOR i
				SpawnLocation[i].vPlayerLoc = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, g_PrivateYachtSpawnLocationOffest[i].vPlayerLoc)
				SpawnLocation[i].fPlayerHeading = GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(iYachtID, g_PrivateYachtSpawnLocationOffest[i].fPlayerHeading)	
			ENDREPEAT
		ELSE
			REPEAT NUMBER_OF_SPAWN_LOCATIONS_YACHT_APARTMENT i
				SpawnLocation[i].vPlayerLoc = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, g_PrivateYachtSpawnLocationApartmentOffest[i].vPlayerLoc)
				SpawnLocation[i].fPlayerHeading = GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(iYachtID, g_PrivateYachtSpawnLocationApartmentOffest[i].fPlayerHeading)	
			ENDREPEAT
		ENDIF
	ELSE
		CASSERTLN(DEBUG_YACHT, "GET_SPAWN_LOCATION_FOR_YACHT - invalid yacht id - ", iYachtID)
	ENDIF
ENDPROC

PROC GET_PRIVATE_YACHT_PERSONAL_VEHICLE_SPAWN_LOCATION(INT iYachtID, VECTOR &vCoords, FLOAT &fHeading)
	//INT iYachtID = LOCAL_PLAYER_PRIVATE_YACHT_ID()
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		vCoords =  g_PrivateYachtDetails[iYachtID].ShoreSpawn[0].vCoords
		fHeading =  g_PrivateYachtDetails[iYachtID].ShoreSpawn[0].fHeading
	ENDIF
ENDPROC

PROC GET_PRIVATE_YACHT_SPAWN_VEHICLE_LOCATION(INT iVehicle, INT iYachtID, VECTOR &vCoords, FLOAT &fHeading, BOOL bOnLand)
	
	IF NOT (bOnLand)

		vCoords = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, gPrivateYachtSpawnVehicleDetails[iVehicle].vOffsetCoords)
		fHeading = GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(iYachtID, gPrivateYachtSpawnVehicleDetails[iVehicle].fOffsetHeading)
		
	ELSE
		
		// is it a sea vehicle?
		IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SET_ANCHOR)
			vCoords =  g_PrivateYachtDetails[iYachtID].ShoreSpawn[2].vCoords
			fHeading =  g_PrivateYachtDetails[iYachtID].ShoreSpawn[2].fHeading
		ELSE
			vCoords =  g_PrivateYachtDetails[iYachtID].ShoreSpawn[1].vCoords
			fHeading =  g_PrivateYachtDetails[iYachtID].ShoreSpawn[1].fHeading
		ENDIF
	
	ENDIF
ENDPROC

PROC GET_PRIVATE_YACHT_DOCKING_VEHICLE_LOCATION(INT iDocking, INT iYachtID, VECTOR &vCoords, FLOAT &fHeading)

	vCoords = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, gPrivateYachtDockingOffsets[iDocking].vOffsetCoords)
	fHeading = GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(iYachtID, gPrivateYachtDockingOffsets[iDocking].fOffsetHeading)
		
ENDPROC

PROC SET_MY_PRIVATE_YACHT_VEHICLE_SPAWN_ON_LAND(INT iVehicle, BOOL bSet) // 0 = heli, 1 = dingy, 2 & 3 = jetskii
	SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_SPAWN_ON_LAND, bSet)
ENDPROC

FUNC BOOL IS_MY_PRIVATE_YACHT_VEHICLE_SET_TO_SPAWN_ON_LAND(INT iVehicle)
	RETURN IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SPAWN_ON_LAND)
ENDFUNC

FUNC BOOL HAS_MY_PRIVATE_YACHT_VEHCILE_BEEN_CREATED(INT iVehicle)
	IF FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
	AND IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_DONT_SPAWN)
		RETURN TRUE
	ENDIF
	
	RETURN IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_CREATED)
ENDFUNC

FUNC BOOL HAS_MY_PRIVATE_YACHT_VEHCILE_BEEN_CREATED_ON_LAND(INT iVehicle)
	RETURN IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_CREATED_ON_LAND)	
ENDFUNC

FUNC BOOL HAS_MY_PRIVATE_YACHT_VEHCILE_BEEN_CREATED_AT_YACHT(INT iVehicle)
	IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_CREATED)	
		IF NOT IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_CREATED_ON_LAND)	
			RETURN (TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC MODEL_NAMES GET_PRIVATE_YACHT_SPAWN_VEHICLE_MODEL(INT iVehicle)
	RETURN gPrivateYachtSpawnVehicleDetails[iVehicle].ModelName
ENDFUNC

ENUM PRIVATE_YACHT_SPAWN_ZONE_ENUM
	PYSZ_NULL
	,PYSZ_1						
	,PYSZ_2								
	,PYSZ_3		
	,PYSZ_4	
	,PYSZ_5	
	,PYSZ_6	
	,PYSZ_7		
	,PYSZ_8
	,PYSZ_9
	,PYSZ_10
	,PYSZ_11
	,PYSZ_12
	,PYSZ_TOTAL
ENDENUM


#IF IS_DEBUG_BUILD
	FUNC STRING GET_STRING_FOR_PRIVATE_YACHT_SPAWN_ZONE(PRIVATE_YACHT_SPAWN_ZONE_ENUM eZone)
		SWITCH eZone
			CASE PYSZ_NULL			RETURN "NULL" 			BREAK
			CASE PYSZ_1				RETURN "PYSZ_1" 		BREAK
			CASE PYSZ_2				RETURN "PYSZ_2" 		BREAK
			CASE PYSZ_3				RETURN "PYSZ_3" 		BREAK
			CASE PYSZ_4				RETURN "PYSZ_4" 		BREAK
			CASE PYSZ_5				RETURN "PYSZ_5" 		BREAK
			CASE PYSZ_6				RETURN "PYSZ_6" 		BREAK
			CASE PYSZ_7				RETURN "PYSZ_7" 		BREAK
			CASE PYSZ_8				RETURN "PYSZ_8" 		BREAK
			CASE PYSZ_9				RETURN "PYSZ_9" 		BREAK
			CASE PYSZ_10			RETURN "PYSZ_10" 		BREAK
			CASE PYSZ_11			RETURN "PYSZ_11" 		BREAK
			CASE PYSZ_12			RETURN "PYSZ_12" 		BREAK
		ENDSWITCH
		
		TEXT_LABEL str = "UNKNOWN "
		str += ENUM_TO_INT(eZone)
		RETURN GET_STRING_FROM_STRING(str, 0, GET_LENGTH_OF_LITERAL_STRING(str))
	ENDFUNC
#ENDIF

FUNC VECTOR GET_COORDS_FOR_YACHT_SPAWN_ZONE(PRIVATE_YACHT_SPAWN_ZONE_ENUM eZone)
	RETURN g_PrivateYachtZoneCoords[ENUM_TO_INT(eZone)]
ENDFUNC

/// PURPOSE: check if there is a free yacht available in ‘zone’
FUNC BOOL IS_FREE_YACHT_SLOT_NEAR_YACHT_SPAWN_ZONE(PRIVATE_YACHT_SPAWN_ZONE_ENUM eZone) //, FLOAT fMaxDist = 2000.0)

	SWITCH ENUM_TO_INT(eZone)
		CASE 0
		
		BREAK
		CASE 1 // yachts 3,4,5
			IF ((GlobalServerBD_BlockC.PYYachtDetails[3].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[3].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[4].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[4].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[5].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[5].iPlayerPrivateYachtiIsReservedFor = -1))
				RETURN(TRUE)
			ENDIF
		BREAK
		CASE 2 // yachts 0,1,2
			IF ((GlobalServerBD_BlockC.PYYachtDetails[0].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[0].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[1].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[1].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[2].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[2].iPlayerPrivateYachtiIsReservedFor = -1))
				RETURN(TRUE)
			ENDIF
		BREAK
		CASE 3 // yachts 6,7,8
			IF ((GlobalServerBD_BlockC.PYYachtDetails[6].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[6].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[7].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[7].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[8].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[8].iPlayerPrivateYachtiIsReservedFor = -1))
				RETURN(TRUE)
			ENDIF
		BREAK
		CASE 4 // yachts 9,10,11
			IF ((GlobalServerBD_BlockC.PYYachtDetails[9].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[9].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[10].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[10].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[11].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[11].iPlayerPrivateYachtiIsReservedFor = -1))
				RETURN(TRUE)
			ENDIF
		BREAK
		CASE 5 // yachts 12,13,14
			IF ((GlobalServerBD_BlockC.PYYachtDetails[12].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[12].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[13].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[13].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[14].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[14].iPlayerPrivateYachtiIsReservedFor = -1))
				RETURN(TRUE)
			ENDIF
		BREAK
		CASE 6 // yachts 15, 16, 17
			IF ((GlobalServerBD_BlockC.PYYachtDetails[15].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[15].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[16].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[16].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[17].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[17].iPlayerPrivateYachtiIsReservedFor = -1))
				RETURN(TRUE)
			ENDIF
		BREAK
		CASE 7 // yachts 18, 19, 20
			IF ((GlobalServerBD_BlockC.PYYachtDetails[18].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[18].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[19].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[19].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[20].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[20].iPlayerPrivateYachtiIsReservedFor = -1))
				RETURN(TRUE)
			ENDIF
		BREAK
		CASE 8 // yachts 21, 22, 23
			IF ((GlobalServerBD_BlockC.PYYachtDetails[21].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[21].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[22].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[22].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[23].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[23].iPlayerPrivateYachtiIsReservedFor = -1))
				RETURN(TRUE)
			ENDIF
		BREAK
		CASE 9 // yachts 24, 25, 26
			IF ((GlobalServerBD_BlockC.PYYachtDetails[24].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[24].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[25].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[25].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[26].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[26].iPlayerPrivateYachtiIsReservedFor = -1))
				RETURN(TRUE)
			ENDIF
		BREAK
		CASE 10 // yachts 27, 28, 29
			IF ((GlobalServerBD_BlockC.PYYachtDetails[27].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[27].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[28].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[28].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[29].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[29].iPlayerPrivateYachtiIsReservedFor = -1))
				RETURN(TRUE)
			ENDIF
		BREAK
		CASE 11 // yachts 30, 31, 32
			IF ((GlobalServerBD_BlockC.PYYachtDetails[30].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[30].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[31].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[31].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[32].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[32].iPlayerPrivateYachtiIsReservedFor = -1))
				RETURN(TRUE)
			ENDIF
		BREAK
		CASE 12 // yachts 33, 34, 35
			IF ((GlobalServerBD_BlockC.PYYachtDetails[33].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[33].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[34].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[34].iPlayerPrivateYachtiIsReservedFor = -1))
			OR ((GlobalServerBD_BlockC.PYYachtDetails[35].iPlayerAssignedToPrivateYacht = -1) AND (GlobalServerBD_BlockC.PYYachtDetails[35].iPlayerPrivateYachtiIsReservedFor = -1))
				RETURN(TRUE)
			ENDIF
		BREAK
	ENDSWITCH

	RETURN FALSE


//	INT i
//	FLOAT fDist
//	REPEAT NUMBER_OF_PRIVATE_YACHTS i
//		fDist = VDIST( GET_COORDS_OF_PRIVATE_YACHT(i) , g_PrivateYachtZoneCoords[ENUM_TO_INT(eZone)])
//		IF (fDist < fMaxDist)			
//			CPRINTLN(DEBUG_YACHT, "IS_FREE_YACHT_SLOT_NEAR_YACHT_SPAWN_ZONE - yacht ", i, ", dist = ", fDist)
//			IF (GlobalServerBD_BlockC.PYYachtDetails[i].iPlayerAssignedToPrivateYacht = -1)
//			AND (GlobalServerBD_BlockC.PYYachtDetails[i].iPlayerPrivateYachtiIsReservedFor = -1)
//				CPRINTLN(DEBUG_YACHT, "IS_FREE_YACHT_SLOT_NEAR_YACHT_SPAWN_ZONE - yacht ", i, ", is free!")
//				RETURN TRUE
//			ELSE
//				CPRINTLN(DEBUG_YACHT, "IS_FREE_YACHT_SLOT_NEAR_YACHT_SPAWN_ZONE - yacht ", i, ", assigned to player = ", GlobalServerBD_BlockC.PYYachtDetails[i].iPlayerAssignedToPrivateYacht, " reserved for player ", GlobalServerBD_BlockC.PYYachtDetails[i].iPlayerPrivateYachtiIsReservedFor)	
//			ENDIF
//		ENDIF	
//	ENDREPEAT
//	RETURN FALSE
ENDFUNC

FUNC PRIVATE_YACHT_SPAWN_ZONE_ENUM GET_CLOSEST_PRIVATE_YACHT_ZONE_FOR_COORDS(VECTOR vCoords)
	
	PRIVATE_YACHT_SPAWN_ZONE_ENUM eClosest = INT_TO_ENUM(PRIVATE_YACHT_SPAWN_ZONE_ENUM, PYSZ_TOTAL)
	FLOAT fClosest = 999999999.0
	FLOAT fThisVDist2
	INT i
	
	REPEAT INT_TO_ENUM(PRIVATE_YACHT_SPAWN_ZONE_ENUM, PYSZ_TOTAL) i
		fThisVDist2 = VDIST2(vCoords, GET_COORDS_FOR_YACHT_SPAWN_ZONE(INT_TO_ENUM(PRIVATE_YACHT_SPAWN_ZONE_ENUM, i)))
		IF (fThisVDist2 < fClosest)
			eClosest = INT_TO_ENUM(PRIVATE_YACHT_SPAWN_ZONE_ENUM, i)
			fClosest = fThisVDist2
		ENDIF
	ENDREPEAT
	
	RETURN eClosest
ENDFUNC

FUNC PRIVATE_YACHT_SPAWN_ZONE_ENUM GET_ZONE_YACHT_IS_IN(INT iYachtID)
	RETURN GET_CLOSEST_PRIVATE_YACHT_ZONE_FOR_COORDS(GET_COORDS_OF_PRIVATE_YACHT(iYachtID))
ENDFUNC

FUNC PRIVATE_YACHT_SPAWN_ZONE_ENUM GET_ZONE_OF_LOCAL_PLAYER_YACHT()
	RETURN GET_CLOSEST_PRIVATE_YACHT_ZONE_FOR_COORDS(GET_COORDS_OF_PRIVATE_YACHT(LOCAL_PLAYER_PRIVATE_YACHT_ID()))
ENDFUNC



PROC GET_YACHT_BOUNDING_BOX(INT iYachtID, VECTOR &vCoords1, VECTOR &vCoords2, FLOAT &fWidth, FLOAT fAdditionalDist=0.0)
	
	VECTOR vPos = GET_COORDS_OF_PRIVATE_YACHT(iYachtID)
	FLOAT fHeading = GET_HEADING_OF_PRIVATE_YACHT(iYachtID)

	VECTOR vForward = <<0.0, 1.0, 0.0>>
	RotateVec(vForward, <<0.0, 0.0, fHeading>> )
	vForward /= VMAG(vForward)
	
	vCoords1 = vPos + (vForward * (0.5 * (PRIVATE_YACHT_LENGTH + fAdditionalDist)) )
	vCoords1.z = (vCoords1.z - (0.5 * (PRIVATE_YACHT_HEIGHT + fAdditionalDist)) ) 
	
	vCoords2 = vPos - (vForward * (0.5 * (PRIVATE_YACHT_LENGTH + fAdditionalDist)))
	vCoords2.z = (vCoords2.z + (0.5 * (PRIVATE_YACHT_HEIGHT + fAdditionalDist)))  
		
	fWidth = PRIVATE_YACHT_WIDTH + fAdditionalDist
	
ENDPROC


FUNC BOOL IS_PLAYER_ON_ANY_YACHT(PLAYER_INDEX PlayerID, BOOL bIncludingVehicles=FALSE)
	IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
		IF NOT (bIncludingVehicles)
			IF NOT (GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.iYachtIAmOn = -1)
				RETURN(TRUE)
			ENDIF
		ELSE
			IF NOT (GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.iYachtIAmOnIncludingVehicles = -1)
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PLAYER_IN_ANY_YACHT_SPACE(PLAYER_INDEX PlayerID)
	IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
		IF NOT (GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.iYachtSpaceIAmIn = -1)
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ON_ANY_YACHT(BOOL bIncludingVehicles = FALSE)
	RETURN IS_PLAYER_ON_ANY_YACHT(PLAYER_ID(), bIncludingVehicles)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_ANY_YACHT_SPACE()
	RETURN IS_PLAYER_IN_ANY_YACHT_SPACE(PLAYER_ID())
ENDFUNC


FUNC BOOL IS_PLAYER_NEAR_ANY_YACHT(PLAYER_INDEX PlayerID)
	IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
		INT iArray
		REPEAT NUMBER_OF_PRIVATE_YACHT_BITSETS iArray
			IF NOT (GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.iBS_YachtsIAmNear[iArray] = 0)
				RETURN(TRUE)
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_NEAR_ANY_YACHT()
	RETURN IS_PLAYER_NEAR_ANY_YACHT(PLAYER_ID())
ENDFUNC

FUNC INT GET_YACHT_PLAYER_IS_NEAR(PLAYER_INDEX PlayerID)
	IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)	
		RETURN GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.iYachtIAmNearest
	ENDIF
	RETURN -1
ENDFUNC

FUNC INT GET_YACHT_SPACE_PLAYER_IS_IN(PLAYER_INDEX PlayerID)
	IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)	
		RETURN GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.iYachtSpaceIAmIn
	ENDIF
	RETURN -1
ENDFUNC


FUNC INT GET_YACHT_PLAYER_IS_ON(PLAYER_INDEX PlayerID, BOOL bIncludingVehicles = FALSE)
	IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
		IF NOT (bIncludingVehicles)
			RETURN GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.iYachtIAmOn
		ELSE
			RETURN GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.iYachtIAmOnIncludingVehicles
		ENDIF
	ENDIF
	RETURN -1
ENDFUNC


FUNC INT GET_YACHT_LOCAL_PLAYER_IS_ON(BOOL bIncludingVehicles = FALSE)
	RETURN GET_YACHT_PLAYER_IS_ON(PLAYER_ID(), bIncludingVehicles)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_RUNNING_YACHT_TAXI_HELI()
	IF NETWORK_IS_SCRIPT_ACTIVE(GET_MP_MISSION_NAME(eAM_HELI_TAXI), -1, TRUE)
		IF MPGlobalsAmbience.bIsYachtTaxi
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_POINT_IN_YACHT_BOUNDING_BOX(VECTOR vPoint, INT iYachtID, FLOAT fAdditionalDist=0.0)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		VECTOR vCoords1, vCoords2
		FLOAT fWidth
		GET_YACHT_BOUNDING_BOX(iYachtID, vCoords1, vCoords2, fWidth, fAdditionalDist)
		RETURN IS_POINT_IN_ANGLED_AREA(vPoint, vCoords1, vCoords2, fWidth)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_POINT_NEAR_YACHT(VECTOR vPoint, INT iYachtID, FLOAT fDist = PRIVATE_YACHT_NEAR_DISTANCE)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		IF (VDIST2(vPoint, GET_COORDS_OF_PRIVATE_YACHT(iYachtID)) < (fDist*fDist))
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PLAYER_ON_YACHT(PLAYER_INDEX PlayerID, INT iYachtID, FLOAT fAdditionalDist=0.0, BOOL bIncludingVehicles=FALSE)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		IF NOT (bIncludingVehicles)
			RETURN IS_POINT_IN_YACHT_BOUNDING_BOX(GET_PLAYER_COORDS(PlayerID), iYachtID, fAdditionalDist)
		ELSE
			IF IS_POINT_IN_YACHT_BOUNDING_BOX(GET_PLAYER_COORDS(PlayerID), iYachtID, fAdditionalDist)
				RETURN(TRUE)
			ELSE
				IF IS_POINT_IN_YACHT_BOUNDING_BOX(GET_PLAYER_COORDS(PlayerID), iYachtID, YACHT_INCLUDING_VEHICLES_DISTANCE)
					IF IS_NET_PLAYER_OK(PlayerID)
						PED_INDEX playerPed = GET_PLAYER_PED(PlayerID)				
						IF DOES_ENTITY_EXIST(playerPed)
						AND NOT IS_ENTITY_DEAD(playerPed)	
							IF IS_PED_IN_ANY_VEHICLE(playerPed)
								VEHICLE_INDEX viVehicle = GET_VEHICLE_PED_IS_USING(playerPed)					
								IF DOES_ENTITY_EXIST(viVehicle)
								AND NOT IS_ENTITY_DEAD(viVehicle)					
									IF DECOR_EXIST_ON(viVehicle, "PYV_Yacht")
										INT iVehYachtID = DECOR_GET_INT(viVehicle, "PYV_Yacht")
										IF (iVehYachtID = iYachtID)
											RETURN(TRUE)	
										ENDIF
									ENDIF					
								ENDIF					
							ENDIF
						ENDIF
					ENDIF			
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

// warning - this is expensive, should not be called every frame.
FUNC BOOL IS_PLAYER_ON_ANY_YACHT_EXPENSIVE(PLAYER_INDEX PlayerID, FLOAT fAdditionalDist=0.0, BOOL bIncludingVehicles=FALSE)
	INT iYachtID
	REPEAT NUMBER_OF_PRIVATE_YACHTS iYachtID
		IF IS_PLAYER_ON_YACHT(PlayerID, iYachtID, fAdditionalDist, bIncludingVehicles)
			RETURN(TRUE)
		ENDIF
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_COORD_ON_YACHT(VECTOR vPos, INT iYachtID, FLOAT fAdditionalDist=0.0, BOOL bIncludingVehicles=FALSE)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		IF NOT (bIncludingVehicles)
			RETURN IS_POINT_IN_YACHT_BOUNDING_BOX(vPos, iYachtID, fAdditionalDist)
		ELSE
			IF IS_POINT_IN_YACHT_BOUNDING_BOX(vPos, iYachtID, fAdditionalDist)
				RETURN(TRUE)
			ELSE
				IF IS_POINT_IN_YACHT_BOUNDING_BOX(vPos, iYachtID, YACHT_INCLUDING_VEHICLES_DISTANCE)
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						PED_INDEX playerPed = PLAYER_PED_ID()
						IF DOES_ENTITY_EXIST(playerPed)
						AND NOT IS_ENTITY_DEAD(playerPed)	
							IF IS_PED_IN_ANY_VEHICLE(playerPed)
								VEHICLE_INDEX viVehicle = GET_VEHICLE_PED_IS_USING(playerPed)					
								IF DOES_ENTITY_EXIST(viVehicle)
								AND NOT IS_ENTITY_DEAD(viVehicle)					
									IF DECOR_EXIST_ON(viVehicle, "PYV_Yacht")
										INT iVehYachtID = DECOR_GET_INT(viVehicle, "PYV_Yacht")
										IF (iVehYachtID = iYachtID)
											RETURN(TRUE)	
										ENDIF
									ENDIF					
								ENDIF					
							ENDIF
						ENDIF
					ENDIF			
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

// warning - this is expensive, should not be called every frame.
FUNC BOOL IS_COORD_ON_ANY_YACHT_EXPENSIVE(VECTOR vPos, FLOAT fAdditionalDist=0.0, BOOL bIncludingVehicles=FALSE)
	INT iYachtID
	REPEAT NUMBER_OF_PRIVATE_YACHTS iYachtID
		IF IS_COORD_ON_YACHT(vPos, iYachtID, fAdditionalDist, bIncludingVehicles)
			RETURN(TRUE)
		ENDIF
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC

// warning - this is expensive, should not be called every frame.
FUNC INT GET_YACHT_COORD_IS_ON_EXPENSIVE(VECTOR vPos, FLOAT fAdditionalDist=0.0, BOOL bIncludingVehicles=FALSE)
	INT iYachtID
	REPEAT NUMBER_OF_PRIVATE_YACHTS iYachtID
		IF IS_COORD_ON_YACHT(vPos, iYachtID, fAdditionalDist, bIncludingVehicles)
			RETURN iYachtID
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

FUNC BOOL IS_VEHICLE_ON_YACHT(VEHICLE_INDEX VehicleID, INT iYachtID, FLOAT fAdditionalDist=0.0)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		IF DOES_ENTITY_EXIST(VehicleID)
		AND NOT IS_ENTITY_DEAD(VehicleID)
			IF IS_POINT_IN_YACHT_BOUNDING_BOX(GET_ENTITY_COORDS(VehicleID), iYachtID, fAdditionalDist)
				RETURN(TRUE)	
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PLAYER_NEAR_YACHT(PLAYER_INDEX PlayerID, INT iYachtID, FLOAT fDist = PRIVATE_YACHT_NEAR_DISTANCE)

	IF FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
		RETURN TRUE
	ENDIF
	
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		IF DOES_ENTITY_EXIST(GET_LOCAL_DRONE_PROP_OBJ_INDEX())
			IF IS_POINT_NEAR_YACHT(GET_ENTITY_COORDS(GET_LOCAL_DRONE_PROP_OBJ_INDEX()), iYachtID, fDist) AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE
				RETURN TRUE
			ENDIF
		ENDIF
		RETURN IS_POINT_NEAR_YACHT(GET_PLAYER_COORDS(PlayerID), iYachtID, fDist)
	ENDIF
	RETURN(FALSE)
ENDFUNC

// expensive should not be called every frame
FUNC INT GET_PRIVATE_YACHT_POINT_IS_ON(VECTOR vPoint)
	INT iYacht
	REPEAT NUMBER_OF_PRIVATE_YACHTS iYacht
		IF IS_POINT_IN_YACHT_BOUNDING_BOX(vPoint, iYacht)
			RETURN iYacht
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

FUNC BOOL IS_PLAYER_ON_THEIR_YACHT(PLAYER_INDEX PlayerID, FLOAT fAdditionalDist=0.0)
	IF DOES_PLAYER_OWN_PRIVATE_YACHT(PlayerID)
		INT iYachtID = GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PlayerID)
		IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
			RETURN IS_PLAYER_ON_YACHT(PlayerID, iYachtID, fAdditionalDist)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ON_THEIR_YACHT(FLOAT fAdditionalDist=0.0)
	RETURN IS_PLAYER_ON_THEIR_YACHT(PLAYER_ID(), fAdditionalDist)
ENDFUNC

FUNC STRING TL2Str(STRING strInput)
    RETURN(strInput)
ENDFUNC

FUNC STRING GET_IPL_STRING_FOR_PRIVATE_YACHT(INT iYachtID, INT iComponent)
	RETURN TL2Str(g_PrivateYachtDetails[iYachtID].strIPL[iComponent])
ENDFUNC

ENUM HULL_PARTS
	HP_HULL,
//	HP_JACUZZI,
	HP_COL_A,
	HP_COL_B,
	HP_COL_C,
	HP_COL_D,
	HP_COL_E,
	HP_TOTAL_HULL_PARTS
ENDENUM

FUNC MODEL_NAMES MODEL_NAME_FOR_OPTION(INT iOption, HULL_PARTS ePart)
	SWITCH ePart
		CASE HP_HULL // hull
			SWITCH iOption
				CASE 0 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_Option1"))) BREAK
				CASE 1 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_Option2"))) BREAK
				CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_Option3"))) BREAK
			ENDSWITCH
		BREAK
//		CASE HP_JACUZZI // jacuzzi
//			RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_Jacuzzi_Ripple1"))) BREAK
//		BREAK
		CASE HP_COL_A
			SWITCH iOption
				CASE 0 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_Option1_ColA"))) BREAK
				CASE 1 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_Option2_ColA"))) BREAK
				CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_Option3_ColA"))) BREAK
			ENDSWITCH
		BREAK
		CASE HP_COL_B
			SWITCH iOption
				CASE 1 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_Option2_ColB"))) BREAK
				CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_Option3_ColB"))) BREAK
			ENDSWITCH
		BREAK
		CASE HP_COL_C
			SWITCH iOption
				CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_Option3_ColC"))) BREAK
			ENDSWITCH
		BREAK
		CASE HP_COL_D
			SWITCH iOption
				CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_Option3_ColD"))) BREAK
			ENDSWITCH
		BREAK
		CASE HP_COL_E
			SWITCH iOption
				CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_Option3_ColE"))) BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

//FUNC BOOL DOES_HULL_PART_USE_OFFSETS(HULL_PARTS ePart)
//	IF (ePart = HP_JACUZZI)
//		RETURN(TRUE)
//	ENDIF
//	RETURN(FALSE)
//ENDFUNC

//PROC GET_OFFSET_FOR_HULL_PART(HULL_PARTS ePart, VECTOR &vOffset, FLOAT &fHeading)
//	SWITCH ePart
//		CASE HP_JACUZZI
//			vOffset = <<-50.8065,-1.98013,0.136816>>
//			fHeading = 0.0
//		BREAK
//	ENDSWITCH
//ENDPROC


FUNC BOOL DOES_OPTION_HAVE_HULL_PART(INT iOption, HULL_PARTS ePart)
	SWITCH iOption
		CASE 0
			SWITCH ePart
				CASE HP_HULL 
				CASE HP_COL_A
					RETURN(TRUE)
				BREAK
			ENDSWITCH		
		BREAK
		CASE 1
			SWITCH ePart
				CASE HP_HULL 
				//CASE HP_JACUZZI
				CASE HP_COL_A
				CASE HP_COL_B
					RETURN(TRUE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH ePart
				CASE HP_HULL 
				//CASE HP_JACUZZI
				CASE HP_COL_A
				CASE HP_COL_B
				CASE HP_COL_C
				CASE HP_COL_D
				CASE HP_COL_E
					RETURN(TRUE)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
	RETURN FALSE	
ENDFUNC

PROC REQUEST_MODELS_FOR_OPTION(INT iOption)
	INT iPart
	REPEAT ENUM_TO_INT(HP_TOTAL_HULL_PARTS) iPart
		IF DOES_OPTION_HAVE_HULL_PART(iOption, INT_TO_ENUM(HULL_PARTS, iPart))
			MODEL_NAMES ModelName = MODEL_NAME_FOR_OPTION(iOption, INT_TO_ENUM(HULL_PARTS, iPart))
			IF NOT (ModelName = DUMMY_MODEL_FOR_SCRIPT)
				REQUEST_MODEL(ModelName)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC SET_MODELS_AS_NO_LONGER_NEEDED_FOR_OPTION(INT iOption)
	INT iPart
	REPEAT ENUM_TO_INT(HP_TOTAL_HULL_PARTS) iPart
		IF DOES_OPTION_HAVE_HULL_PART(iOption, INT_TO_ENUM(HULL_PARTS, iPart))
			MODEL_NAMES ModelName = MODEL_NAME_FOR_OPTION(iOption, INT_TO_ENUM(HULL_PARTS, iPart))
			IF NOT (ModelName = DUMMY_MODEL_FOR_SCRIPT)
				SET_MODEL_AS_NO_LONGER_NEEDED(ModelName)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL HAVE_MODELS_LOADED_FOR_OPTION(INT iOption)
	INT iPart
	REPEAT ENUM_TO_INT(HP_TOTAL_HULL_PARTS) iPart
		IF DOES_OPTION_HAVE_HULL_PART(iOption, INT_TO_ENUM(HULL_PARTS, iPart))
			MODEL_NAMES ModelName = MODEL_NAME_FOR_OPTION(iOption, INT_TO_ENUM(HULL_PARTS, iPart))
			IF NOT (ModelName = DUMMY_MODEL_FOR_SCRIPT)
				IF NOT HAS_MODEL_LOADED(ModelName)
					RETURN(FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

FUNC MODEL_NAMES MODEL_NAME_FOR_JACUZZI()
	RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_Jacuzzi_Ripple1")))
ENDFUNC

PROC REQUEST_ASSETS_FOR_JACUZZI(INT iOption)
	IF (iOption > 0)
		REQUEST_MODEL(MODEL_NAME_FOR_JACUZZI())
	ENDIF
ENDPROC

PROC REMOVE_ASSETS_FOR_JACUZZI(INT iOption)
	IF (iOption > 0)
		SET_MODEL_AS_NO_LONGER_NEEDED(MODEL_NAME_FOR_JACUZZI()) 
	ENDIF
ENDPROC

FUNC BOOL HAVE_ASSETS_LOADED_FOR_JACUZZI(INT iOption)
	IF (iOption > 0)
		RETURN HAS_MODEL_LOADED(MODEL_NAME_FOR_JACUZZI()) 
	ENDIF
	RETURN(TRUE)
ENDFUNC


PROC GET_OFFSET_FOR_JACUZZI(VECTOR &vOffset, FLOAT &fHeading)
	vOffset = <<-50.8065,-1.98013,0.136816>>
	fHeading = 0.0
ENDPROC


FUNC MODEL_NAMES MODEL_NAME_FOR_RADAR()
	RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_Radar_01a")))
ENDFUNC

PROC REQUEST_ASSETS_FOR_RADAR()
	REQUEST_MODEL(MODEL_NAME_FOR_RADAR())
ENDPROC

PROC REMOVE_ASSETS_FOR_RADAR()
	SET_MODEL_AS_NO_LONGER_NEEDED(MODEL_NAME_FOR_RADAR()) 
ENDPROC

FUNC BOOL HAVE_ASSETS_LOADED_FOR_RADAR()
	RETURN HAS_MODEL_LOADED(MODEL_NAME_FOR_RADAR()) 
ENDFUNC

PROC GET_OFFSET_FOR_RADAR(INT iOption, INT iRadar, VECTOR &vOffset, FLOAT &fHeading)
	SWITCH iOption
		CASE 0
			SWITCH iRadar
				CASE 0
					vOffset = <<0.953125,-2.17139,9.60403>>
					fHeading = 90.0
				BREAK
				CASE 1
					vOffset = <<1.27979,-1.99268,13.4305>>
					fHeading = -180.0
				BREAK
				CASE 2
					vOffset = <<5.48218,-1.98486,18.1568>>
					fHeading = -90.0
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iRadar
				CASE 0
					vOffset = <<-2.24878,-1.99268,17.32>>
					fHeading = -90.0 
				BREAK
				CASE 1
					vOffset = <<1.6189,-1.99268,14.0505>>
					fHeading = -180.0
				BREAK
				CASE 2
					vOffset = <<7.63501,-1.99268,10.3491>>
					fHeading = 90.0
				BREAK
			ENDSWITCH	
		BREAK
		CASE 2
			SWITCH iRadar
				CASE 0
					vOffset = <<10.8328,-1.99268,9.85299>>
					fHeading = 90.0
				BREAK
				CASE 1
					vOffset = <<-0.226318,-1.96289,12.8964>>
					fHeading = 180.0
				BREAK
				CASE 2
					vOffset = <<-15.052,-1.99463,9.06741>>
					fHeading = 90.0
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

FUNC MODEL_NAMES MODEL_NAME_FOR_LIGHTING(INT iOption, INT iLighting)
	SWITCH iLighting
		CASE 0
			SWITCH iOption
				CASE 0 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y1_L1A"))) BREAK
				CASE 1 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y2_L1A"))) BREAK
				CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y3_L1A"))) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iOption
				CASE 0 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y1_L1B"))) BREAK
				CASE 1 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y2_L1B"))) BREAK
				CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y3_L1B"))) BREAK
			ENDSWITCH
		BREAK	
		CASE 2
			SWITCH iOption
				CASE 0 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y1_L1C"))) BREAK
				CASE 1 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y2_L1C"))) BREAK
				CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y3_L1C"))) BREAK
			ENDSWITCH
		BREAK			
		CASE 3
			SWITCH iOption
				CASE 0 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y1_L1D"))) BREAK
				CASE 1 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y2_L1D"))) BREAK
				CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y3_L1D"))) BREAK
			ENDSWITCH
		BREAK	
		CASE 4
			SWITCH iOption
				CASE 0 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y1_L2A"))) BREAK
				CASE 1 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y2_L2A"))) BREAK
				CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y3_L2A"))) BREAK
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH iOption
				CASE 0 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y1_L2B"))) BREAK
				CASE 1 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y2_L2B"))) BREAK
				CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y3_L2B"))) BREAK
			ENDSWITCH
		BREAK	
		CASE 6
			SWITCH iOption
				CASE 0 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y1_L2C"))) BREAK
				CASE 1 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y2_L2C"))) BREAK
				CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y3_L2C"))) BREAK
			ENDSWITCH
		BREAK			
		CASE 7
			SWITCH iOption
				CASE 0 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y1_L2D"))) BREAK
				CASE 1 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y2_L2D"))) BREAK
				CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_apa_Y3_L2D"))) BREAK
			ENDSWITCH
		BREAK			
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

PROC REQUEST_MODEL_FOR_LIGHTING(INT iOption, INT iLighting)
	MODEL_NAMES ModelName = MODEL_NAME_FOR_LIGHTING(iOption, iLighting)
	IF NOT (ModelName = DUMMY_MODEL_FOR_SCRIPT)
		REQUEST_MODEL(ModelName)
	ENDIF
ENDPROC

PROC SET_MODEL_AS_NO_LONGER_NEEDED_FOR_LIGHTING(INT iOption, INT iLighting)
	MODEL_NAMES ModelName = MODEL_NAME_FOR_LIGHTING(iOption, iLighting)
	IF NOT (ModelName = DUMMY_MODEL_FOR_SCRIPT)
		SET_MODEL_AS_NO_LONGER_NEEDED(ModelName) 
	ENDIF
ENDPROC

FUNC BOOL HAS_MODEL_LOADED_FOR_LIGHTING(INT iOption, INT iLighting)
	MODEL_NAMES ModelName = MODEL_NAME_FOR_LIGHTING(iOption, iLighting)
	IF NOT (ModelName = DUMMY_MODEL_FOR_SCRIPT)
		RETURN HAS_MODEL_LOADED(ModelName) 
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC MODEL_NAMES MODEL_NAME_FOR_RAILING(INT iOption, INT iRailing)
	SWITCH iRailing
		CASE 0
			SWITCH iOption            
				CASE 0 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_O1_Rail_A"))) BREAK
				CASE 1 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_O2_Rail_A"))) BREAK
				CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_O3_Rail_A"))) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iOption
				CASE 0 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_O1_Rail_B"))) BREAK
				CASE 1 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_O2_Rail_B"))) BREAK
				CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, (HASH("apa_MP_Apa_Yacht_O3_Rail_B"))) BREAK
			ENDSWITCH
		BREAK			
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

PROC REQUEST_MODEL_FOR_RAILING(INT iOption, INT iRailing)
	MODEL_NAMES ModelName = MODEL_NAME_FOR_RAILING(iOption, iRailing)
	IF NOT (ModelName = DUMMY_MODEL_FOR_SCRIPT)
		REQUEST_MODEL(ModelName)
	ENDIF
ENDPROC

PROC SET_MODEL_AS_NO_LONGER_NEEDED_FOR_RAILING(INT iOption, INT iRailing)
	MODEL_NAMES ModelName = MODEL_NAME_FOR_RAILING(iOption, iRailing)
	IF NOT (ModelName = DUMMY_MODEL_FOR_SCRIPT)
		SET_MODEL_AS_NO_LONGER_NEEDED(ModelName)
	ENDIF
ENDPROC

FUNC BOOL HAS_MODEL_LOADED_FOR_RAILING(INT iOption, INT iRailing)
	MODEL_NAMES ModelName = MODEL_NAME_FOR_RAILING(iOption, iRailing)
	IF NOT (ModelName = DUMMY_MODEL_FOR_SCRIPT)
		RETURN HAS_MODEL_LOADED(ModelName) 
	ENDIF
	RETURN TRUE
ENDFUNC

PROC GET_OFFSET_FOR_FLAGPOLE(VECTOR &vOffset, VECTOR &vOffsetRot)
	vOffset = <<-0.1400, -58.1000, -3.3800>>
	vOffsetRot = <<49.6800, 0.0000, 0.0000>>
ENDPROC


FUNC MODEL_NAMES MODEL_NAME_FOR_YACHT_FLAGPOLE()
	RETURN PROP_GOLF_BALL
ENDFUNC

FUNC MODEL_NAMES MODEL_NAME_FOR_YACHT_FLAG(INT iFlag, BOOL bSupressAssert = FALSE)
	IF (iFlag = -1)
	OR (iFlag = 255)
		CPRINTLN(DEBUG_YACHT, "MODEL_NAME_FOR_YACHT_FLAG - invalid flag int ", iFlag)
		RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDIF
	
	SWITCH iFlag
		CASE 0 RETURN APA_Prop_Flag_SCOTLAND_YT BREAK
		CASE 1 RETURN APA_Prop_Flag_US_YT BREAK
		CASE 2 RETURN APA_Prop_Flag_FRANCE BREAK
		CASE 3 RETURN APA_Prop_Flag_ITALY BREAK
		CASE 4 RETURN APA_Prop_Flag_SWEDEN BREAK
		CASE 5 RETURN APA_Prop_Flag_ARGENTINA BREAK
		CASE 6 RETURN APA_Prop_Flag_EU_YT BREAK
		CASE 7 RETURN APA_Prop_Flag_FINLAND BREAK
		CASE 8 RETURN APA_Prop_Flag_NETHERLANDS BREAK
		CASE 9 RETURN APA_Prop_Flag_PORTUGAL BREAK
		
		CASE 10 RETURN APA_Prop_Flag_SOUTHKOREA BREAK
		CASE 11 RETURN APA_Prop_Flag_AUSTRALIA BREAK
		CASE 12 RETURN APA_Prop_Flag_German_YT BREAK
		CASE 13 RETURN APA_Prop_Flag_SWITZERLAND BREAK
		CASE 14 RETURN APA_Prop_Flag_BELGIUM BREAK
		CASE 15 RETURN APA_Prop_Flag_TURKEY BREAK
		CASE 16 RETURN APA_Prop_Flag_CHINA BREAK
		CASE 17 RETURN APA_Prop_Flag_HUNGARY BREAK
		CASE 18 RETURN APA_Prop_Flag_NEWZEALAND BREAK
		CASE 19 RETURN APA_Prop_Flag_PUERTORICO BREAK
		
		CASE 20 RETURN APA_Prop_Flag_BRAZIL BREAK
		CASE 21 RETURN APA_Prop_Flag_Japan_YT BREAK
		CASE 22 RETURN APA_Prop_Flag_JAMAICA BREAK
		CASE 23 RETURN APA_Prop_Flag_Mexico_YT BREAK
		CASE 24 RETURN APA_Prop_Flag_IRELAND BREAK
		CASE 25 RETURN APA_Prop_Flag_CROATIA BREAK
		CASE 26 RETURN APA_Prop_Flag_ISRAEL BREAK
		CASE 27 RETURN APA_Prop_Flag_NIGERIA BREAK
		CASE 28 RETURN APA_Prop_Flag_SLOVAKIA BREAK
		CASE 29 RETURN APA_Prop_Flag_SPAIN BREAK
		
		CASE 30 RETURN APA_Prop_Flag_COLOMBIA BREAK
		CASE 31 RETURN APA_Prop_Flag_AUSTRIA BREAK
		CASE 32 RETURN APA_Prop_Flag_WALES BREAK
		CASE 33 RETURN APA_Prop_Flag_CZECHREP BREAK
		CASE 34 RETURN APA_Prop_Flag_LSTEIN BREAK
		CASE 35 RETURN APA_Prop_Flag_PALESTINE BREAK
		CASE 36 RETURN APA_Prop_Flag_SOUTHAFRICA BREAK
		CASE 37 RETURN APA_Prop_Flag_Canada_YT BREAK
		CASE 38 RETURN APA_Prop_Flag_UK_YT BREAK
		CASE 39 RETURN APA_Prop_Flag_NORWAY BREAK
		
		CASE 40 RETURN APA_Prop_Flag_Russia_YT BREAK
		CASE 41 RETURN APA_Prop_Flag_ENGLAND BREAK
		CASE 42 RETURN APA_Prop_Flag_DENMARK BREAK
		CASE 43 RETURN APA_Prop_Flag_MALTA BREAK
		CASE 44 RETURN APA_Prop_Flag_POLAND BREAK
		CASE 45 RETURN APA_Prop_Flag_SLOVENIA BREAK
	ENDSWITCH
	
	IF NOT bSupressAssert
		CASSERTLN(DEBUG_YACHT, "MODEL_NAME_FOR_YACHT_FLAG - unknown flag int ", iFlag)
	ENDIF
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

PROC REQUEST_MODEL_FOR_FLAG(INT iFlag)
	MODEL_NAMES ModelName = MODEL_NAME_FOR_YACHT_FLAG(iFlag)
	IF NOT (ModelName = DUMMY_MODEL_FOR_SCRIPT)
		REQUEST_MODEL(ModelName)
	ENDIF
	REQUEST_MODEL(MODEL_NAME_FOR_YACHT_FLAGPOLE())
ENDPROC

PROC SET_MODEL_AS_NO_LONGER_NEEDED_FOR_FLAG(INT iFlag)
	MODEL_NAMES ModelName = MODEL_NAME_FOR_YACHT_FLAG(iFlag)
	IF NOT (ModelName = DUMMY_MODEL_FOR_SCRIPT)
		SET_MODEL_AS_NO_LONGER_NEEDED(ModelName)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(MODEL_NAME_FOR_YACHT_FLAGPOLE())
ENDPROC

FUNC BOOL HAS_MODEL_LOADED_FOR_FLAG(INT iFlag)
	BOOL bModelLoaded = TRUE

	MODEL_NAMES ModelName = MODEL_NAME_FOR_YACHT_FLAG(iFlag)
	IF NOT (ModelName = DUMMY_MODEL_FOR_SCRIPT)
		IF NOT HAS_MODEL_LOADED(ModelName)
			bModelLoaded = FALSE
		ENDIF
	ENDIF
	IF NOT HAS_MODEL_LOADED(MODEL_NAME_FOR_YACHT_FLAGPOLE())
		bModelLoaded = FALSE
	ENDIF
	RETURN bModelLoaded
ENDFUNC

PROC REQUEST_ASSETS_FOR_PRIVATE_YACHT(YACHT_APPEARANCE Appearance, BOOL bObjectsDisabled)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_YACHT, "REQUEST_ASSETS_FOR_PRIVATE_YACHT - called")
	PRINTLN_YACHT_APPEARANCE(Appearance)
	IF (bObjectsDisabled)
	CPRINTLN(DEBUG_YACHT, "REQUEST_ASSETS_FOR_PRIVATE_YACHT - DISABLED ")
	ENDIF
	#ENDIF
	REQUEST_MODELS_FOR_OPTION(Appearance.iOption)
	IF NOT (bObjectsDisabled)
		REQUEST_ASSETS_FOR_JACUZZI(Appearance.iOption)
		REQUEST_MODEL_FOR_LIGHTING(Appearance.iOption, Appearance.iLighting)
		REQUEST_MODEL_FOR_RAILING(Appearance.iOption, Appearance.iRailing)	
		REQUEST_MODEL_FOR_Flag(Appearance.iFlag)
		REQUEST_ASSETS_FOR_RADAR()
	ENDIF
ENDPROC



FUNC BOOL HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT(YACHT_APPEARANCE Appearance, BOOL bObjectsDisabled)
	REQUEST_ASSETS_FOR_PRIVATE_YACHT(Appearance, bObjectsDisabled)
	IF HAVE_MODELS_LOADED_FOR_OPTION(Appearance.iOption)
	AND ((bObjectsDisabled = FALSE) AND HAVE_ASSETS_LOADED_FOR_JACUZZI(Appearance.iOption))
	AND ((bObjectsDisabled = FALSE) AND HAS_MODEL_LOADED_FOR_LIGHTING(Appearance.iOption, Appearance.iLighting))
	AND ((bObjectsDisabled = FALSE) AND HAS_MODEL_LOADED_FOR_RAILING(Appearance.iOption, Appearance.iRailing))
	AND ((bObjectsDisabled = FALSE) AND HAS_MODEL_LOADED_FOR_FLAG(Appearance.iFlag))
	AND ((bObjectsDisabled = FALSE) AND HAVE_ASSETS_LOADED_FOR_RADAR())
		RETURN(TRUE)	
	ELSE
		#IF IS_DEBUG_BUILD
			IF NOT HAVE_MODELS_LOADED_FOR_OPTION(Appearance.iOption)
				CPRINTLN(DEBUG_YACHT, "HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT - waiting on OPTION models")
			ENDIF
			IF NOT (bObjectsDisabled)
				IF NOT HAVE_ASSETS_LOADED_FOR_JACUZZI(Appearance.iOption)
					CPRINTLN(DEBUG_YACHT, "HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT - waiting on JACUZZI models")
				ENDIF
				IF NOT HAS_MODEL_LOADED_FOR_LIGHTING(Appearance.iOption, Appearance.iLighting)
					CPRINTLN(DEBUG_YACHT, "HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT - waiting on LIGHTING models")
				ENDIF
				IF NOT HAS_MODEL_LOADED_FOR_RAILING(Appearance.iOption, Appearance.iRailing)
					CPRINTLN(DEBUG_YACHT, "HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT - waiting on RAILING models")
				ENDIF
				IF NOT HAS_MODEL_LOADED_FOR_FLAG(Appearance.iFlag)
					CPRINTLN(DEBUG_YACHT, "HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT - waiting on FLAG models")
				ENDIF
				IF NOT HAVE_ASSETS_LOADED_FOR_RADAR()
					CPRINTLN(DEBUG_YACHT, "HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT - waiting on RADAR models")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_YACHT, "HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT - bObjectsDisabled=TRUE")	
			ENDIF
		#ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC REMOVE_ASSETS_FOR_YACHT(YACHT_APPEARANCE Appearance)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_YACHT, "REMOVE_ASSETS_FOR_YACHT - called")
	PRINTLN_YACHT_APPEARANCE(Appearance)
	#ENDIF
	SET_MODELS_AS_NO_LONGER_NEEDED_FOR_OPTION(Appearance.iOption)
	REMOVE_ASSETS_FOR_JACUZZI(Appearance.iOption)
	SET_MODEL_AS_NO_LONGER_NEEDED_FOR_LIGHTING(Appearance.iOption, Appearance.iLighting)
	SET_MODEL_AS_NO_LONGER_NEEDED_FOR_RAILING(Appearance.iOption, Appearance.iRailing)
	SET_MODEL_AS_NO_LONGER_NEEDED_FOR_FLAG(Appearance.iFlag)
	REMOVE_ASSETS_FOR_RADAR()
ENDPROC


FUNC MODEL_NAMES GET_YACHT_COLLISION_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("APA_MP_Apa_Yacht_Win"))
ENDFUNC

FUNC MODEL_NAMES GET_YACHT_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("APA_MP_APA_YACHT"))
ENDFUNC

PROC REQUEST_IPLS_FOR_PRIVATE_YACHT(INT iYachtID, BOOL bIgnoreRequestCount=FALSE)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		INT iComponent
		REPEAT NUMBER_OF_PRIVATE_YACHT_IPLS iComponent
			#IF IS_DEBUG_BUILD	
				STRING str = GET_IPL_STRING_FOR_PRIVATE_YACHT(iYachtID, iComponent)
				CPRINTLN(DEBUG_YACHT, "REQUEST_IPLS_FOR_PRIVATE_YACHT - resquesting IPL: ", str, " for yacht ", iYachtID)
			#ENDIF
			REQUEST_IPL(GET_IPL_STRING_FOR_PRIVATE_YACHT(iYachtID, iComponent))
		ENDREPEAT
		
		// request the base model and collision
		//IF NOT MPGlobalsPrivateYacht.bUseYachtObjectsOptimisation
			REQUEST_MODEL(GET_YACHT_COLLISION_MODEL())
			REQUEST_MODEL(GET_YACHT_MODEL())
		//ENDIF
		
		// increment the request count
		IF NOT (bIgnoreRequestCount)
			IF NOT (MPGlobalsPrivateYacht.bPrivateYachtRequested[iYachtID])
				MPGlobalsPrivateYacht.bPrivateYachtRequested[iYachtID] = TRUE
				MPGlobalsPrivateYacht.iYachtRequests += 1	
				CPRINTLN(DEBUG_YACHT, "REQUEST_IPLS_FOR_PRIVATE_YACHT - MPGlobalsPrivateYacht.iYachtRequests incremented to ", MPGlobalsPrivateYacht.iYachtRequests, " yacht id ", iYachtID)
			ENDIF
		ENDIF
		
	ELSE
		CASSERTLN(DEBUG_YACHT, "REQUEST_IPLS_FOR_PRIVATE_YACHT - invalid yacht id - ", iYachtID)
	ENDIF
ENDPROC

PROC REMOVE_IPLS_FOR_PRIVATE_YACHT(INT iYachtID, BOOL bIgnoreRequestCount=FALSE)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		INT iComponent
		REPEAT NUMBER_OF_PRIVATE_YACHT_IPLS iComponent
			#IF IS_DEBUG_BUILD	
				STRING str = GET_IPL_STRING_FOR_PRIVATE_YACHT(iYachtID, iComponent)
				CPRINTLN(DEBUG_YACHT, "REMOVE_IPLS_FOR_PRIVATE_YACHT - removing IPL: ", str, " for yacht ", iYachtID)
			#ENDIF		
			REMOVE_IPL(GET_IPL_STRING_FOR_PRIVATE_YACHT(iYachtID, iComponent))
		ENDREPEAT
		
		// decrement the request count
		IF NOT (bIgnoreRequestCount)
		
			IF (MPGlobalsPrivateYacht.bPrivateYachtRequested[iYachtID])
				MPGlobalsPrivateYacht.bPrivateYachtRequested[iYachtID] = FALSE
				MPGlobalsPrivateYacht.iYachtRequests += -1	
				CPRINTLN(DEBUG_YACHT, "REMOVE_IPLS_FOR_PRIVATE_YACHT - MPGlobalsPrivateYacht.iYachtRequests decrement to ", MPGlobalsPrivateYacht.iYachtRequests, " yacht id ", iYachtID)
			ENDIF
			
			// only unload models if there are no requests
			IF (MPGlobalsPrivateYacht.iYachtRequests <= 0)
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_YACHT_COLLISION_MODEL())
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_YACHT_MODEL())					
				MPGlobalsPrivateYacht.iYachtRequests = 0 // just to make sure it stays at zero
				CPRINTLN(DEBUG_YACHT, "REMOVE_IPLS_FOR_PRIVATE_YACHT - unloaded models, iYachtRequests set to Zero")
			ENDIF			
			
		ENDIF
		
	ENDIF
ENDPROC

FUNC BOOL ARE_ANY_IPLS_ACTIVE_FOR_PRIVATE_YACHT(INT iYachtID)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		INT iComponent
		REPEAT NUMBER_OF_PRIVATE_YACHT_IPLS iComponent
			IF IS_IPL_ACTIVE(GET_IPL_STRING_FOR_PRIVATE_YACHT(iYachtID, iComponent))
				CPRINTLN(DEBUG_YACHT, "ARE_ANY_IPLS_ACTIVE_FOR_PRIVATE_YACHT - iYachtID ", iYachtID, ", iComponent ", iComponent)
				RETURN(TRUE)
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN(FALSE)
ENDFUNC


FUNC BOOL ARE_IPLS_ACTIVE_FOR_PRIVATE_YACHT(INT iYachtID)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		INT iComponent
		REPEAT NUMBER_OF_PRIVATE_YACHT_IPLS iComponent
			IF NOT IS_IPL_ACTIVE(GET_IPL_STRING_FOR_PRIVATE_YACHT(iYachtID, iComponent))
				CPRINTLN(DEBUG_YACHT, "ARE_IPLS_ACTIVE_FOR_PRIVATE_YACHT - iYachtID ", iYachtID, " Component ", iComponent, " not active.")
				RETURN(FALSE)
			ENDIF
		ENDREPEAT
		RETURN(TRUE)
	ELSE
		CASSERTLN(DEBUG_YACHT, "ARE_IPLS_ACTIVE_FOR_PRIVATE_YACHT - invalid yacht id - ", iYachtID)
	ENDIF	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(PLAYER_INDEX PlayerID)
	RETURN GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.bWarpActive	
ENDFUNC

FUNC INT SERVER_FIND_FREE_PRIVATE_YACHT_SLOT(INT iPlayer, VECTOR vDesiredCoords, FLOAT fNearPlayersDist)
	INT iNumberOfYachts = NUMBER_OF_PRIVATE_YACHTS - 6
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		iNumberOfYachts = NUMBER_OF_PRIVATE_YACHTS - 5
	ENDIF
	
	// first build a list of all free yachts
	INT iFreeYachts[NUMBER_OF_PRIVATE_YACHTS]
	INT iFreeYachtsNotReserved[NUMBER_OF_PRIVATE_YACHTS]
	INT iFreeYachtsNotNearPlayers[NUMBER_OF_PRIVATE_YACHTS]	
	INT iCount
	INT iCountNotReserved
	INT iCountNotNearPlayers	
	INT i
	INT iPlayers
	INT iNumPlayers
	INT j
	INT iLastYachtForPlayer = -1
	PLAYER_INDEX PlayerID
	BOOL bAPlayerIsNear
	
	#IF IS_DEBUG_BUILD
		IF (g_iPrioritiseThisYacht > -1)
			IF GlobalServerBD_BlockC.PYYachtDetails[g_iPrioritiseThisYacht].iPlayerAssignedToPrivateYacht = -1	
				CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - using g_iPrioritiseThisYacht ", g_iPrioritiseThisYacht)
				RETURN(g_iPrioritiseThisYacht)
			ENDIF
		ENDIF
	#ENDIF
	
	INT iHashOfThisPlayer = -1
	
	IF NETWORK_IS_PLAYER_CONNECTED(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))
		iHashOfThisPlayer = NETWORK_HASH_FROM_PLAYER_HANDLE(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))			
	ENDIF
	CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - iHashOfThisPlayer ", iHashOfThisPlayer)
	
	REPEAT iNumberOfYachts i
		IF GlobalServerBD_BlockC.PYYachtDetails[i].iPlayerAssignedToPrivateYacht = -1
			
			// store that this yacht is free.
			iFreeYachts[iCount] = i
			iCount++
			
			// is this yacht not reserved?
			IF (GlobalServerBD_BlockC.PYYachtDetails[i].iPlayerPrivateYachtiIsReservedFor = -1)
			OR (GlobalServerBD_BlockC.PYYachtDetails[i].iPlayerPrivateYachtiIsReservedFor = iPlayer)
			
				iFreeYachtsNotReserved[iCountNotReserved] = i
				iCountNotReserved++
							
				// was this the player's last yacht? - only if player is not warping their yacht. (fix for 2681452)
				IF NOT IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))
					IF (iLastYachtForPlayer = -1)
						IF NOT (iHashOfThisPlayer = -1)
							IF (GlobalServerBD_BlockC.PYYachtDetails[i].bYachtIsActive)
							AND (GlobalServerBD_BlockC.PYYachtDetails[i].iHashOfLastOwner = iHashOfThisPlayer)
								// found players last yacht in session
								iLastYachtForPlayer = i
								CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - found players last yacht in session, yacht ", i)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
								
				// are any players near?
				NETWORK_IS_ANY_PLAYER_NEAR(iPlayers, iNumPlayers, GET_COORDS_OF_PRIVATE_YACHT(i), fNearPlayersDist, FALSE)
				IF (iNumPlayers = 0)
					iFreeYachtsNotNearPlayers[iCountNotNearPlayers] = i			
					iCountNotNearPlayers++ 	
					CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - no players are near yacht ", i)
				ELSE
					// some players are near, check if any of them are important enough.	
					bAPlayerIsNear = FALSE
					REPEAT NUM_NETWORK_PLAYERS j
						IF IS_BIT_SET(iPlayers, j)
							
							PlayerID = INT_TO_NATIVE(PLAYER_INDEX, j)
							
							// ignore the player we are checking against.
							IF NOT (NATIVE_TO_INT(PlayerID) = iPlayer)
								
								// ignore if the player is in a corona
								IF (GlobalplayerBD_FM[NATIVE_TO_INT(PlayerID)].eCoronaStatus != CORONA_STATUS_IDLE)
									bAPlayerIsNear = TRUE	
									CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - player is near ", NATIVE_TO_INT(PlayerID))
								ELSE
									CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - ignoring player - this is player is in a corona ", NATIVE_TO_INT(PlayerID))
								ENDIF
							
							ELSE
								CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - ignoring player - this is player we are checking against ", NATIVE_TO_INT(PlayerID))	
							ENDIF
						
						ENDIF
					ENDREPEAT
					
					IF NOT (bAPlayerIsNear)
						iFreeYachtsNotNearPlayers[iCountNotNearPlayers] = i			
						iCountNotNearPlayers++ 							
						CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - no important players are near yacht ", i)
					ENDIF
					
				ENDIF	
				
			ELSE
			
				CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - yacht ", i, " reserved for player ", GlobalServerBD_BlockC.PYYachtDetails[i].iPlayerPrivateYachtiIsReservedFor)
				
			ENDIF
		

			
		ENDIF
	ENDREPEAT
	
	
	IF NOT (iLastYachtForPlayer = -1)
		CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - returning players last yacht in session, yacht ", iLastYachtForPlayer)
		BROADCAST_DELETE_YACHT_VEHICLES_NOW(iLastYachtForPlayer)
		RETURN iLastYachtForPlayer
	ENDIF
	
	// return the nearest free yacht to the desired coords
	IF VMAG(vDesiredCoords) > 0.0
		FLOAT fDist = 999999999.9
		FLOAT fThisDist
		INT iNearest = -1	
		CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - find near desired coords <<", vDesiredCoords.x, ", ", vDesiredCoords.y, ", ", vDesiredCoords.z, ">>")
		IF (iCountNotNearPlayers > 0)
			
			REPEAT iCountNotNearPlayers i
				fThisDist = VDIST2(vDesiredCoords, GET_COORDS_OF_PRIVATE_YACHT(iFreeYachtsNotNearPlayers[i]))
				IF (fThisDist < fDist)
					fDist = fThisDist
					iNearest = iFreeYachtsNotNearPlayers[i]
				ENDIF
			ENDREPEAT
			
			CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - using free yacht not near any player, yacht id ", iNearest, " iCountNotNearPlayers = ", iCountNotNearPlayers)
			RETURN iNearest
			
		ELIF (iCountNotReserved > 0)
		
			REPEAT iCountNotReserved i
				fThisDist = VDIST2(vDesiredCoords, GET_COORDS_OF_PRIVATE_YACHT(iFreeYachtsNotReserved[i]))
				IF (fThisDist < fDist)
					fDist = fThisDist
					iNearest = iFreeYachtsNotReserved[i]
				ENDIF
			ENDREPEAT	

			CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - using free yacht not reserved, yacht id ", iNearest, " iCountNotReserved = ", iCountNotReserved)
			RETURN iNearest
		
		ELIF (iCount > 0)
		
			REPEAT iCount i
				fThisDist = VDIST2(vDesiredCoords, GET_COORDS_OF_PRIVATE_YACHT(iFreeYachts[i]))
				IF (fThisDist < fDist)
					fDist = fThisDist
					iNearest = iFreeYachtsNotNearPlayers[i]
				ENDIF
			ENDREPEAT	

			CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - using free yacht, yacht id ", iNearest, " iCount = ", iCount)
			RETURN iNearest
		ENDIF
	ELSE
		// desired coords were not set, so just use random
		CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - desired coords not set, using random.")
		INT iRand
		IF (iCountNotNearPlayers > 0)
			iRand = GET_RANDOM_INT_IN_RANGE(0, iCountNotNearPlayers)
			CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - using free yacht not near any player, yacht id ", iFreeYachtsNotNearPlayers[iRand], " iCountNotNearPlayers = ", iCountNotNearPlayers)
			RETURN iFreeYachtsNotNearPlayers[iRand]
		ELIF (iCountNotReserved > 0)
			iRand = GET_RANDOM_INT_IN_RANGE(0, iCountNotReserved)
			CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - using free yacht not reserved, yacht id ", iFreeYachtsNotReserved[iRand], " iCountNotReserved = ", iCountNotReserved)
			RETURN iFreeYachtsNotReserved[iRand]	
		ELIF (iCount > 0)
			iRand = GET_RANDOM_INT_IN_RANGE(0, iCount)
			CPRINTLN(DEBUG_YACHT, "SERVER_FIND_FREE_PRIVATE_YACHT_SLOT - using free yacht, yacht id ", iFreeYachtsNotNearPlayers[iRand], " iCount = ", iCount)
			RETURN iFreeYachts[iRand]
		ENDIF		
	ENDIF
	
	RETURN -1
	
ENDFUNC

PROC SERVER_UNRESERVE_YACHT_FOR_PLAYER(PLAYER_INDEX PlayerID)
	INT iPlayer = NATIVE_TO_INT(PlayerID)
	INT iYachtID = GET_PRIVATE_YACHT_RESERVED_FOR_PLAYER(PlayerID)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		GlobalServerBD_BlockC.PYYachtDetails[iYachtID].iPlayerPrivateYachtiIsReservedFor = -1
		GlobalServerBD_BlockC.PYPlayerDetails[iPlayer].iPrivateYachtReservedForPlayer = -1
		CPRINTLN(DEBUG_YACHT, "SERVER_UNRESERVE_YACHT_FOR_PLAYER - unreserving yacht id ", iYachtID, " from player ", iPlayer)
	ELSE
		CPRINTLN(DEBUG_YACHT, "SERVER_UNRESERVE_YACHT_FOR_PLAYER - no yacht reserved for player ", iPlayer)
	ENDIF
ENDPROC

FUNC INT COUNT_NUMBER_OF_EXISTING_YACHTS()
	INT iYachtID
	INT iCount
	REPEAT NUMBER_OF_PRIVATE_YACHTS iYachtID
		IF (GlobalServerBD_BlockC.PYYachtDetails[iYachtID].bYachtIsActive)
		OR (GlobalServerBD_BlockC.PYYachtDetails[iYachtID].bYachtIsMarkedForCleanup)
			iCount++	
		ENDIF
	ENDREPEAT
	RETURN iCount
ENDFUNC

PROC SERVER_ASSIGN_PRIVATE_YACHT_TO_PLAYER(INT iYachtID, INT iPlayer, YACHT_APPEARANCE Appearance)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)				
		GlobalServerBD_BlockC.PYPlayerDetails[iPlayer].iPrivateYachtAssignedToPlayer = iYachtID	
		GlobalServerBD_BlockC.PYYachtDetails[iYachtID].iPlayerAssignedToPrivateYacht = iPlayer
		GlobalServerBD_BlockC.PYYachtDetails[iYachtID].iHashOfLastOwner = -1
		IF NETWORK_IS_PLAYER_CONNECTED(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))
			GlobalServerBD_BlockC.PYYachtDetails[iYachtID].iHashOfOwner = NETWORK_HASH_FROM_PLAYER_HANDLE(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))
		ENDIF
		GlobalServerBD_BlockC.PYYachtDetails[iYachtID].bYachtIsActive = TRUE
		GlobalServerBD_BlockC.PYYachtDetails[iYachtID].bYachtIsMarkedForCleanup = FALSE	
		GlobalServerBD_BlockC.PYYachtDetails[iYachtID].Appearance = Appearance
		GlobalServerBD_BlockC.PYYachtDetails[iYachtID].bForceCleanup = FALSE
		GlobalServerBD_BlockC.iTotalExistingPrivateYachts = COUNT_NUMBER_OF_EXISTING_YACHTS()
		
		BROADCAST_DELETE_ABANDONED_VEHICLES_ON_YACHT_NOW(iYachtID)
		
		SERVER_UNRESERVE_YACHT_FOR_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))		
		CPRINTLN(DEBUG_YACHT, "SERVER_ASSIGN_PRIVATE_YACHT_TO_PLAYER - assigning yacht id ", iYachtID, " to player ", iPlayer)
	ELSE
		CASSERTLN(DEBUG_YACHT, "SERVER_ASSIGN_PRIVATE_YACHT_TO_PLAYER - invalid yacht id ", iYachtID, " for player ", iPlayer)
	ENDIF
ENDPROC

PROC SERVER_RESERVE_PRIVATE_YACHT_FOR_PLAYER(INT iYachtID, INT iPlayer)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		GlobalServerBD_BlockC.PYPlayerDetails[iPlayer].iPrivateYachtReservedForPlayer = iYachtID
		GlobalServerBD_BlockC.PYYachtDetails[iYachtID].iPlayerPrivateYachtiIsReservedFor = iPlayer
		GlobalServerBD_BlockC.PYYachtDetails[iYachtID].ReservedAtTime = GET_NETWORK_TIME()
		CPRINTLN(DEBUG_YACHT, "SERVER_RESERVE_PRIVATE_YACHT_FOR_PLAYER - reserving yacht id ", iYachtID, " for player ", iPlayer)
	ELSE
		CASSERTLN(DEBUG_YACHT, "SERVER_RESERVE_PRIVATE_YACHT_FOR_PLAYER - invalid yacht id ", iYachtID, " for player ", iPlayer)
	ENDIF
ENDPROC

PROC SERVER_REMOVE_YACHT_FROM_PLAYER(PLAYER_INDEX PlayerID)
	INT iPlayer = NATIVE_TO_INT(PlayerID)
	INT iYachtID = GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PlayerID)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		GlobalServerBD_BlockC.PYPlayerDetails[iPlayer].iPrivateYachtAssignedToPlayer = -1
		GlobalServerBD_BlockC.PYYachtDetails[iYachtID].iPlayerAssignedToPrivateYacht = -1
		GlobalServerBD_BlockC.PYYachtDetails[iYachtID].iLastPlayerAssignedToPrivateYacht = iPlayer
		GlobalServerBD_BlockC.PYYachtDetails[iYachtID].iHashOfLastOwner = GlobalServerBD_BlockC.PYYachtDetails[iYachtID].iHashOfOwner
		GlobalServerBD_BlockC.PYYachtDetails[iYachtID].iHashOfOwner = -1
		GlobalServerBD_BlockC.PYYachtDetails[iYachtID].bYachtIsMarkedForCleanup = TRUE
		
		// dont clear the appearance here, do it when iLastPlayerAssignedToPrivateYacht gets cleared
		//YACHT_APPEARANCE EmptyAppearance
		//GlobalServerBD_BlockC.PYYachtDetails[iYachtID].Appearance = EmptyAppearance

		SERVER_UNRESERVE_YACHT_FOR_PLAYER(PlayerID)
		
		CPRINTLN(DEBUG_YACHT, "SERVER_REMOVE_YACHT_FROM_PLAYER - removing yacht id ", iYachtID, " from player ", iPlayer)
	ELSE
		CASSERTLN(DEBUG_YACHT, "SERVER_REMOVE_YACHT_FROM_PLAYER - invalid yacht id ", iYachtID, " for player ", iPlayer)
	ENDIF
ENDPROC

PROC INIT_YACHT_VEHICLE_FLAGS(INT iOption)
	CPRINTLN(DEBUG_YACHT, "INIT_YACHT_VEHICLE_FLAGS - iOption ", iOption)
	SWITCH iOption
		CASE 0
		DEFAULT
			SET_PRIVATE_YACHT_VEHICLES_INITIAL_VALUES_LEVEL_1()
		BREAK
		CASE 1
			SET_PRIVATE_YACHT_VEHICLES_INITIAL_VALUES_LEVEL_2()
		BREAK
		CASE 2
			SET_PRIVATE_YACHT_VEHICLES_INITIAL_VALUES_LEVEL_3()
		BREAK		
	ENDSWITCH
ENDPROC


PROC INIT_PRIVATE_YACHT_CLIENT()
	CPRINTLN(DEBUG_YACHT, "INIT_PRIVATE_YACHT_CLIENT - called")

	INIT_YACHT_COORDS()	

	IF DOES_LOCAL_PLAYER_STAT_OWN_PRIVATE_YACHT()

		INT iOption = GET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_MOD)
		INT iTint = GET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_COLOR)
		INT iLighting = GET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_LIGHTING)
		INT iRailing = GET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_FIXTURE)
		INT iFlag = GET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_FLAG)
		TEXT_LABEL_63 sName = GET_MP_LONG_STRING_CHARACTER_STAT(MP_STAT_YACHT_NAME, MP_STAT_YACHT_NAME2)
				
		SET_LOCAL_PLAYER_OWNS_PRIVATE_YACHT(TRUE, iOption, iTint, iLighting, iRailing, iFlag)
		
		SET_LOCAL_PLAYER_BD_YACHT_NAME(sName)
		
		INIT_YACHT_VEHICLE_FLAGS(iOption)
		
		
	ELSE
		SET_LOCAL_PLAYER_OWNS_PRIVATE_YACHT(FALSE)
	ENDIF
		

ENDPROC



FUNC BOOL IS_LOCAL_PLAYER_IN_A_BOAT()
	VEHICLE_INDEX VehicleID
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(VehicleID))
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_VEHICLE_A_YACHT_DOCKABLE_VEHICLE(MODEL_NAMES aModel)

	IF IS_THIS_MODEL_A_BOAT(aModel)
	OR aModel = DODO
	OR aModel = SUBMERSIBLE
	OR aModel = SUBMERSIBLE2
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_A_DOCKABLE_VEHICLE()
	VEHICLE_INDEX VehicleID
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_A_YACHT_DOCKABLE_VEHICLE(GET_ENTITY_MODEL(VehicleID))
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PLAYER_IN_VEHICLE_WITH_YACHT_OWNER(INT iYachtID)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			RETURN IS_PED_IN_VEHICLE(
				GET_PLAYER_PED(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)),
				GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_GROUND_COORD_ADJUSTED_FOR_MODEL_DIMENSIONS_DOCKING(MODEL_NAMES CarModel, VECTOR vInCoords)
	VECTOR vModelMin, vModelMax
	SAFE_GET_MODEL_DIMENSIONS(CarModel, vModelMin, vModelMax)
	vInCoords.z -= ((vModelMax.z - vModelMin.z)*0.5)
	
	RETURN(vInCoords)
ENDFUNC 

PROC GET_PRIVATE_YACHT_VEHICLE_CAR_COLOURS(MODEL_NAMES aModel, INT iTint, INT&Colour1,INT&Colour2,INT&Colour3,INT&Colour4,INT&Colour5,INT&Colour6 )
	INT iYachtColour = iTint
	//IF iYachtColour != GET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_COLOR)
		SWITCH aModel
			CASE SPEEDER2
				SWITCH iYachtColour
					CASE 0 	Colour1 = 77 	Colour2 = 11 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 78 	BREAK
					CASE 1 	Colour1 = 63 	Colour2 = 111 	Colour3 = 7 	Colour4 = 67 	Colour5 = 65 	Colour6 = 74 	BREAK
					CASE 2 	Colour1 = 73 	Colour2 = 111 	Colour3 = 7 	Colour4 = 73 	Colour5 = 111 	Colour6 = 73 	BREAK
					CASE 3 	Colour1 = 23 	Colour2 = 23 	Colour3 = 7 	Colour4 = 8 	Colour5 = 23 	Colour6 = 8 	BREAK
					CASE 4 	Colour1 = 9 	Colour2 = 9 	Colour3 = 7 	Colour4 = 8 	Colour5 = 9 	Colour6 = 6 	BREAK
					CASE 5 	Colour1 = 2 	Colour2 = 2 	Colour3 = 7 	Colour4 = 45 	Colour5 = 2 	Colour6 = 45	BREAK
					CASE 6 	Colour1 = 2		Colour2 = 111	Colour3 = 7		Colour4 = 2		Colour5 = 2		Colour6 = 2		BREAK
					CASE 7 	Colour1 = 111	Colour2 = 0		Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 0		BREAK
					CASE 8 	Colour1 = 111	Colour2 = 111	Colour3 = 7		Colour4 = 138	Colour5 = 111	Colour6 = 138	BREAK
					CASE 9 	Colour1 = 2		Colour2 = 18	Colour3 = 7		Colour4 = 2		Colour5 = 2		Colour6 = 2		BREAK
					CASE 10	Colour1 = 111	Colour2 = 111	Colour3 = 7		Colour4 = 59	Colour5 = 111	Colour6 = 59	BREAK
					CASE 11	Colour1 = 30	Colour2 = 111	Colour3 = 7		Colour4 = 111	Colour5 = 30	Colour6 = 111	BREAK
					CASE 12	Colour1 = 121	Colour2 = 121	Colour3 = 7		Colour4 = 77	Colour5 = 121	Colour6 = 77	BREAK
					CASE 13	Colour1 = 111	Colour2 = 111	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 14	Colour1 = 33	Colour2 = 111	Colour3 = 7		Colour4 = 33	Colour5 = 111	Colour6 = 33	BREAK
					CASE 15	Colour1 = 1		Colour2 = 1		Colour3 = 7		Colour4 = 99	Colour5 = 158	Colour6 = 1		BREAK
					DEFAULT	Colour1 = 0		Colour2 = 0		Colour3 = 0		Colour4 = 0		Colour5 = 0		Colour6 = 0		BREAK
				ENDSWITCH
				CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] GET_PRIVATE_YACHT_VEHICLE_CAR_COLOURS - SPEEDER2 iYachtColour ",iYachtColour," Colour1 = ",Colour1, " Colour2 = ",Colour2, " Colour3 = ", Colour3," Colour4 = ", Colour4," Colour5 = ", Colour5," Colour6 = ", Colour6, " " )
			BREAK
			CASE DINGHY4
				SWITCH iYachtColour
					CASE 0 	Colour1 = 111 	Colour2 = 77 	Colour3 = 8 	Colour4 = 77 	Colour5 = 77 	Colour6 = 111 	BREAK
					CASE 1 	Colour1 = 111 	Colour2 = 63 	Colour3 = 8 	Colour4 = 63 	Colour5 = 63 	Colour6 = 63 	BREAK
					CASE 2 	Colour1 = 111 	Colour2 = 111 	Colour3 = 8 	Colour4 = 73 	Colour5 = 73 	Colour6 = 111 	BREAK
					CASE 3 	Colour1 = 23 	Colour2 = 23 	Colour3 = 8 	Colour4 = 8 	Colour5 = 8 	Colour6 = 23 	BREAK
					CASE 4 	Colour1 = 9 	Colour2 = 9 	Colour3 = 8 	Colour4 = 9 	Colour5 = 8 	Colour6 = 6 	BREAK
					CASE 5 	Colour1 = 2 	Colour2 = 2 	Colour3 = 8 	Colour4 = 2 	Colour5 = 45 	Colour6 = 2		BREAK
					CASE 6 	Colour1 = 2		Colour2 = 111	Colour3 = 8		Colour4 = 2		Colour5 = 2		Colour6 = 111	BREAK
					CASE 7 	Colour1 = 111	Colour2 = 0		Colour3 = 8		Colour4 = 2		Colour5 = 2		Colour6 = 111	BREAK
					CASE 8 	Colour1 = 111	Colour2 = 111	Colour3 = 8		Colour4 = 138	Colour5 = 138	Colour6 = 111	BREAK
					CASE 9 	Colour1 = 2		Colour2 = 18	Colour3 = 8		Colour4 = 2		Colour5 = 2		Colour6 = 18	BREAK
					CASE 10	Colour1 = 111	Colour2 = 111	Colour3 = 8		Colour4 = 2		Colour5 = 59	Colour6 = 132	BREAK
					CASE 11	Colour1 = 31	Colour2 = 111	Colour3 = 8		Colour4 = 2		Colour5 = 111	Colour6 = 31	BREAK
					CASE 12	Colour1 = 121	Colour2 = 121	Colour3 = 8		Colour4 = 77	Colour5 = 77	Colour6 = 121	BREAK
					CASE 13	Colour1 = 111	Colour2 = 111	Colour3 = 8		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 14	Colour1 = 111	Colour2 = 111	Colour3 = 8		Colour4 = 33	Colour5 = 33	Colour6 = 111	BREAK
					CASE 15	Colour1 = 1		Colour2 = 1		Colour3 = 0		Colour4 = 1		Colour5 = 158	Colour6 = 1		BREAK
					DEFAULT	Colour1 = 0		Colour2 = 0		Colour3 = 0		Colour4 = 0		Colour5 = 0		Colour6 = 0		BREAK
				ENDSWITCH
				CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] GET_PRIVATE_YACHT_VEHICLE_CAR_COLOURS - DINGHY4 iYachtColour ",iYachtColour," Colour1 = ",Colour1, " Colour2 = ",Colour2, " Colour3 = ", Colour3," Colour4 = ", Colour4," Colour5 = ", Colour5," Colour6 = ", Colour6, " " )
			BREAK
			CASE SEASHARK3
				SWITCH iYachtColour
					CASE 0 	Colour1 = 111 	Colour2 = 111 	Colour3 = 7 	Colour4 = 111 	Colour5 = 77 	Colour6 = 77 	BREAK
					CASE 1 	Colour1 = 111 	Colour2 = 63 	Colour3 = 7 	Colour4 = 63 	Colour5 = 63 	Colour6 = 63 	BREAK
					CASE 2 	Colour1 = 111 	Colour2 = 73 	Colour3 = 7 	Colour4 = 111 	Colour5 = 73 	Colour6 = 73 	BREAK
					CASE 3 	Colour1 = 23 	Colour2 = 8 	Colour3 = 7 	Colour4 = 8 	Colour5 = 8 	Colour6 = 8	 	BREAK
					CASE 4 	Colour1 = 8 	Colour2 = 9 	Colour3 = 7 	Colour4 = 8 	Colour5 = 8 	Colour6 = 9 	BREAK
					CASE 5 	Colour1 = 45 	Colour2 = 2 	Colour3 = 7 	Colour4 = 45 	Colour5 = 2 	Colour6 = 2		BREAK
					CASE 6 	Colour1 = 2		Colour2 = 111	Colour3 = 7		Colour4 = 8		Colour5 = 2		Colour6 = 2		BREAK
					CASE 7 	Colour1 = 2		Colour2 = 111	Colour3 = 7		Colour4 = 2		Colour5 = 2		Colour6 = 111	BREAK
					CASE 8 	Colour1 = 111	Colour2 = 138	Colour3 = 7		Colour4 = 138	Colour5 = 111	Colour6 = 138	BREAK
					CASE 9 	Colour1 = 18	Colour2 = 2		Colour3 = 7		Colour4 = 2		Colour5 = 2		Colour6 = 2		BREAK
					CASE 10	Colour1 = 111	Colour2 = 8		Colour3 = 7		Colour4 = 8		Colour5 = 8		Colour6 = 111	BREAK
					CASE 11	Colour1 = 111	Colour2 = 31	Colour3 = 7		Colour4 = 31	Colour5 = 111	Colour6 = 31	BREAK
					CASE 12	Colour1 = 121	Colour2 = 77	Colour3 = 7		Colour4 = 77	Colour5 = 121	Colour6 = 77	BREAK
					CASE 13	Colour1 = 111	Colour2 = 18	Colour3 = 7		Colour4 = 18	Colour5 = 18	Colour6 = 111	BREAK
					CASE 14	Colour1 = 33	Colour2 = 111	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 33	BREAK
					CASE 15	Colour1 = 1		Colour2 = 158	Colour3 = 7		Colour4 = 1		Colour5 = 1		Colour6 = 1		BREAK
					DEFAULT	Colour1 = 0		Colour2 = 0		Colour3 = 0		Colour4 = 0		Colour5 = 0		Colour6 = 0		BREAK
				ENDSWITCH
				CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] GET_PRIVATE_YACHT_VEHICLE_CAR_COLOURS - SEASHARK3 iYachtColour ",iYachtColour," Colour1 = ",Colour1, " Colour2 = ",Colour2, " Colour3 = ", Colour3," Colour4 = ", Colour4," Colour5 = ", Colour5," Colour6 = ", Colour6, " " )
			BREAK
			CASE TORO2
				SWITCH iYachtColour
					CASE 0 	Colour1 = 111 	Colour2 = 104 	Colour3 = 7 	Colour4 = 77 	Colour5 = 0 	Colour6 = 0 	BREAK
					CASE 1 	Colour1 = 63 	Colour2 = 103 	Colour3 = 7 	Colour4 = 63 	Colour5 = 0 	Colour6 = 0 	BREAK
					CASE 2 	Colour1 = 73 	Colour2 = 103 	Colour3 = 7 	Colour4 = 111 	Colour5 = 0 	Colour6 = 0 	BREAK
					CASE 3 	Colour1 = 23 	Colour2 = 103 	Colour3 = 7 	Colour4 = 8 	Colour5 = 0 	Colour6 = 0	 	BREAK
					CASE 4 	Colour1 = 9 	Colour2 = 103 	Colour3 = 7 	Colour4 = 8 	Colour5 = 0 	Colour6 = 0 	BREAK
					CASE 5 	Colour1 = 2 	Colour2 = 11 	Colour3 = 7 	Colour4 = 45 	Colour5 = 0 	Colour6 = 0		BREAK
					CASE 6 	Colour1 = 2		Colour2 = 94	Colour3 = 7		Colour4 = 2		Colour5 = 0		Colour6 = 0		BREAK
					CASE 7 	Colour1 = 111	Colour2 = 11	Colour3 = 7		Colour4 = 2		Colour5 = 0		Colour6 = 0		BREAK
					CASE 8 	Colour1 = 138	Colour2 = 104	Colour3 = 7		Colour4 = 138	Colour5 = 0		Colour6 = 0		BREAK
					CASE 9 	Colour1 = 2		Colour2 = 101	Colour3 = 7		Colour4 = 18	Colour5 = 0		Colour6 = 0		BREAK
					CASE 10	Colour1 = 111	Colour2 = 97	Colour3 = 7		Colour4 = 2		Colour5 = 0		Colour6 = 0		BREAK
					CASE 11	Colour1 = 31	Colour2 = 104	Colour3 = 7		Colour4 = 111	Colour5 = 0		Colour6 = 0		BREAK
					CASE 12	Colour1 = 111	Colour2 = 103	Colour3 = 7		Colour4 = 2		Colour5 = 0		Colour6 = 0		BREAK
					CASE 13	Colour1 = 111	Colour2 = 104	Colour3 = 7		Colour4 = 2		Colour5 = 111	Colour6 = 111	BREAK
					CASE 14	Colour1 = 111	Colour2 = 104	Colour3 = 7		Colour4 = 33	Colour5 = 0		Colour6 = 0		BREAK
					CASE 15	Colour1 = 1		Colour2 = 2		Colour3 = 7		Colour4 = 158	Colour5 = 0		Colour6 = 0		BREAK
					DEFAULT	Colour1 = 0		Colour2 = 0		Colour3 = 0		Colour4 = 0		Colour5 = 0		Colour6 = 0		BREAK
				ENDSWITCH
				CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] GET_PRIVATE_YACHT_VEHICLE_CAR_COLOURS - TORO2 iYachtColour ",iYachtColour," Colour1 = ",Colour1, " Colour2 = ",Colour2, " Colour3 = ", Colour3," Colour4 = ", Colour4," Colour5 = ", Colour5," Colour6 = ", Colour6, " " )
			BREAK
			CASE TROPIC2
				SWITCH iYachtColour
					CASE 0 	Colour1 = 111 	Colour2 = 77 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111 	BREAK
					CASE 1 	Colour1 = 111 	Colour2 = 63 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111 	BREAK
					CASE 2 	Colour1 = 111 	Colour2 = 73 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111 	BREAK
					CASE 3 	Colour1 = 23 	Colour2 = 8 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111	BREAK
					CASE 4 	Colour1 = 9 	Colour2 = 8 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111 	BREAK
					CASE 5 	Colour1 = 2 	Colour2 = 45 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111	BREAK
					CASE 6 	Colour1 = 2		Colour2 = 111	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 7 	Colour1 = 111	Colour2 = 2		Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 8 	Colour1 = 111	Colour2 = 138	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 9 	Colour1 = 2		Colour2 = 18	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 10	Colour1 = 111	Colour2 = 8		Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 11	Colour1 = 111	Colour2 = 31	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 12	Colour1 = 121	Colour2 = 77	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 13	Colour1 = 111	Colour2 = 111	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 14	Colour1 = 111	Colour2 = 33	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 15	Colour1 = 1		Colour2 = 158	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					DEFAULT	Colour1 = 0		Colour2 = 0		Colour3 = 0		Colour4 = 0		Colour5 = 0		Colour6 = 0		BREAK
				ENDSWITCH
				CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] GET_PRIVATE_YACHT_VEHICLE_CAR_COLOURS - TROPIC2 iYachtColour ",iYachtColour," Colour1 = ",Colour1, " Colour2 = ",Colour2, " Colour3 = ", Colour3," Colour4 = ", Colour4," Colour5 = ", Colour5," Colour6 = ", Colour6, " " )
			BREAK
			CASE SWIFT2
				SWITCH iYachtColour
					CASE 0 	Colour1 = 111 	Colour2 = 77 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111 	BREAK
					CASE 1 	Colour1 = 111 	Colour2 = 63 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111 	BREAK
					CASE 2 	Colour1 = 111 	Colour2 = 73 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111 	BREAK
					CASE 3 	Colour1 = 23 	Colour2 = 8 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111	BREAK
					CASE 4 	Colour1 = 9 	Colour2 = 8 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111 	BREAK
					CASE 5 	Colour1 = 2 	Colour2 = 45 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111	BREAK
					CASE 6 	Colour1 = 2		Colour2 = 111	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 7 	Colour1 = 111	Colour2 = 2		Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 8 	Colour1 = 111	Colour2 = 138	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 9 	Colour1 = 2		Colour2 = 18	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 10	Colour1 = 111	Colour2 = 8		Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 11	Colour1 = 111	Colour2 = 31	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 12	Colour1 = 121	Colour2 = 77	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 13	Colour1 = 111	Colour2 = 111	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 14	Colour1 = 111	Colour2 = 33	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 15	Colour1 = 1		Colour2 = 158	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					DEFAULT	Colour1 = 0		Colour2 = 0		Colour3 = 0		Colour4 = 0		Colour5 = 0		Colour6 = 0		BREAK
				ENDSWITCH
				CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] GET_PRIVATE_YACHT_VEHICLE_CAR_COLOURS - SWIFT2 iYachtColour ",iYachtColour," Colour1 = ",Colour1, " Colour2 = ",Colour2, " Colour3 = ", Colour3," Colour4 = ", Colour4," Colour5 = ", Colour5," Colour6 = ", Colour6, " " )
			BREAK			
			CASE SUPERVOLITO2
				SWITCH iYachtColour
					CASE 0 	Colour1 = 111 	Colour2 = 77 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111 	BREAK
					CASE 1 	Colour1 = 111 	Colour2 = 63 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111 	BREAK
					CASE 2 	Colour1 = 111 	Colour2 = 73 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111 	BREAK
					CASE 3 	Colour1 = 23 	Colour2 = 8 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111	BREAK
					CASE 4 	Colour1 = 9 	Colour2 = 8 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111 	BREAK
					CASE 5 	Colour1 = 2 	Colour2 = 45 	Colour3 = 7 	Colour4 = 111 	Colour5 = 111 	Colour6 = 111	BREAK
					CASE 6 	Colour1 = 2		Colour2 = 111	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 7 	Colour1 = 111	Colour2 = 2		Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 8 	Colour1 = 111	Colour2 = 138	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 9 	Colour1 = 2		Colour2 = 18	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 10	Colour1 = 111	Colour2 = 8		Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 11	Colour1 = 111	Colour2 = 31	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 12	Colour1 = 121	Colour2 = 77	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 13	Colour1 = 111	Colour2 = 111	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 14	Colour1 = 111	Colour2 = 33	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					CASE 15	Colour1 = 1		Colour2 = 158	Colour3 = 7		Colour4 = 111	Colour5 = 111	Colour6 = 111	BREAK
					DEFAULT	Colour1 = 0		Colour2 = 0		Colour3 = 0		Colour4 = 0		Colour5 = 0		Colour6 = 0		BREAK
				ENDSWITCH
				CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] GET_PRIVATE_YACHT_VEHICLE_CAR_COLOURS - SUPERVOLITO2 iYachtColour ",iYachtColour," Colour1 = ",Colour1, " Colour2 = ",Colour2, " Colour3 = ", Colour3," Colour4 = ", Colour4," Colour5 = ", Colour5," Colour6 = ", Colour6, " " )
			BREAK
			
			
		ENDSWITCH
	//ENDIF
	
ENDPROC


PROC SERVER_RESERVE_FREE_YACHT_FOR_PLAYER_NEAR_COORDS(INT iPlayer, VECTOR vDesiredCoords)
	CPRINTLN(DEBUG_YACHT, "SERVER_RESERVE_FREE_YACHT_FOR_PLAYER_NEAR_COORDS - iPlayer ", iPlayer, ", vDesiredCoords = ", vDesiredCoords)
	INT iYachtID = SERVER_FIND_FREE_PRIVATE_YACHT_SLOT(iPlayer, vDesiredCoords, 0.0)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		SERVER_RESERVE_PRIVATE_YACHT_FOR_PLAYER(iYachtID, iPlayer) 	
	ELSE
		CPRINTLN(DEBUG_YACHT, "SERVER_RESERVE_FREE_YACHT_FOR_PLAYER_NEAR_COORDS - can't find free yacht for player ", iPlayer)				
	ENDIF
ENDPROC

PROC SERVER_ASSIGN_FREE_YACHT_TO_PLAYER(INT iPlayer, INT iDesiredYachtID, YACHT_APPEARANCE Appearance)
	CPRINTLN(DEBUG_YACHT, "SERVER_ASSIGN_FREE_YACHT_TO_PLAYER - iPlayer ", iPlayer, ", iDesiredYachtID = ", iDesiredYachtID)
	#IF IS_DEBUG_BUILD
	PRINTLN_YACHT_APPEARANCE(Appearance)
	#ENDIF
	IF IS_PRIVATE_YACHT_ID_VALID(iDesiredYachtID)
		SERVER_ASSIGN_PRIVATE_YACHT_TO_PLAYER(iDesiredYachtID, iPlayer, Appearance) 	
	ELSE
		CPRINTLN(DEBUG_YACHT, "SERVER_ASSIGN_FREE_YACHT_TO_PLAYER - can't find free yacht for player ", iPlayer)				
	ENDIF
ENDPROC

PROC SERVER_ASSIGN_FREE_YACHT_TO_PLAYER_NEAR_COORDS(INT iPlayer, VECTOR vDesiredCoords, YACHT_APPEARANCE Appearance)
	CPRINTLN(DEBUG_YACHT, "SERVER_ASSIGN_FREE_YACHT_TO_PLAYER_NEAR_COORDS - iPlayer ", iPlayer, ", vDesiredCoords = ", vDesiredCoords)
	#IF IS_DEBUG_BUILD
	PRINTLN_YACHT_APPEARANCE(Appearance)
	#ENDIF
	INT iYachtID = SERVER_FIND_FREE_PRIVATE_YACHT_SLOT(iPlayer, vDesiredCoords, 100.0)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		SERVER_ASSIGN_PRIVATE_YACHT_TO_PLAYER(iYachtID, iPlayer, Appearance) 	
	ELSE
		CPRINTLN(DEBUG_YACHT, "SERVER_ASSIGN_FREE_YACHT_TO_PLAYER_NEAR_COORDS - can't find free yacht for player ", iPlayer)				
	ENDIF
ENDPROC

PROC SERVER_REASSIGN_YACHT_TO_PLAYER_NEAR_COORDS(INT iPlayer, VECTOR vDesiredCoords, YACHT_APPEARANCE Appearance)
	CPRINTLN(DEBUG_YACHT, "SERVER_REASSIGN_YACHT_TO_PLAYER_NEAR_COORDS - iPlayer ", iPlayer, ", vDesiredCoords = ", vDesiredCoords)	
	#IF IS_DEBUG_BUILD
	PRINTLN_YACHT_APPEARANCE(Appearance)
	#ENDIF	
	IF HAS_PLAYER_BEEN_ASSIGNED_PRIVATE_YACHT(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))
	
		// send out event to delete old vehicles.
		BROADCAST_DELETE_YACHT_VEHICLES_NOW(GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, iPlayer)))
	
		CPRINTLN(DEBUG_YACHT, "SERVER_REASSIGN_YACHT_TO_PLAYER_NEAR_COORDS - removing currently assigned yacht")
		SERVER_REMOVE_YACHT_FROM_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))	
	ENDIF
	SERVER_ASSIGN_FREE_YACHT_TO_PLAYER_NEAR_COORDS(iPlayer, vDesiredCoords, Appearance)
ENDPROC

PROC SERVER_RESERVE_YACHT_FOR_PLAYER_NEAR_COORDS(INT iPlayer, VECTOR vDesiredCoords)
	CPRINTLN(DEBUG_YACHT, "SERVER_RESERVE_YACHT_FOR_PLAYER_NEAR_COORDS - iPlayer ", iPlayer, ", vDesiredCoords = ", vDesiredCoords)	
	IF HAS_PLAYER_RESERVED_PRIVATE_YACHT(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))
		CPRINTLN(DEBUG_YACHT, "SERVER_RESERVE_YACHT_FOR_PLAYER_NEAR_COORDS - removing currently reserved yacht")
		SERVER_UNRESERVE_YACHT_FOR_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))	
	ENDIF
	SERVER_RESERVE_FREE_YACHT_FOR_PLAYER_NEAR_COORDS(iPlayer, vDesiredCoords)
ENDPROC



/// PURPOSE: start the warp to the new zone, will fade out all players on the yacht and warp them to new position and then fade in (and restore control)
PROC START_WARP_PRIVATE_YACHT_TO_ZONE(PRIVATE_YACHT_SPAWN_ZONE_ENUM eZone)

	CPRINTLN(DEBUG_YACHT, "START_WARP_PRIVATE_YACHT_TO_ZONE - called with ", ENUM_TO_INT(eZone))

	g_SpawnData.iYachtToWarpFrom = LOCAL_PLAYER_PRIVATE_YACHT_ID()
	g_SpawnData.iYachtZoneToWarpTo = ENUM_TO_INT(eZone)

	VECTOR vZoneCoords = GET_COORDS_FOR_YACHT_SPAWN_ZONE(eZone)
	BROADCAST_RESERVE_PRIVATE_YACHT_NEAR_COORDS(vZoneCoords)
	
	INT iYachtID = GET_NEAREST_PRIVATE_YACHT_TO_POINT(vZoneCoords, FALSE)	
	CPRINTLN(DEBUG_YACHT, "START_WARP_PRIVATE_YACHT_TO_ZONE - provisionally storing yacht  ", iYachtID)
	
	IF iYachtID < 36
		SET_MP_INT_CHARACTER_STAT(MP_STAT_YACHTPREFERREDAREA, iYachtID+1)
	ENDIF
	
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.bWarpActive = TRUE
	
ENDPROC

//FUNC BOOL IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(PLAYER_INDEX PlayerID)
//	RETURN GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.bWarpActive	
//ENDFUNC

/// PURPOSE: is player involved in an active yacht warp.
FUNC BOOL IS_PLAYER_DOING_A_YACHT_WARP(PLAYER_INDEX PlayerID)
	IF (GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.iWarpState != 0 )
	OR (GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.bWarpActive)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC



PROC UPDATE_YACHTS_I_AM_ON_OR_NEAR(INT iYachtID)
	
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	FLOAT fDist

	// have i left the yacht i was on?
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOn != -1)
		IF IS_PRIVATE_YACHT_ACTIVE(GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOn)
			IF NOT IS_PLAYER_ON_YACHT(PLAYER_ID(), GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOn)
				CPRINTLN(DEBUG_YACHT, "UPDATE_YACHTS_I_AM_ON_OR_NEAR - i have left yacht ", GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOn)
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOn = -1				
			ENDIF
		ELSE
			CPRINTLN(DEBUG_YACHT, "UPDATE_YACHTS_I_AM_ON_OR_NEAR - yacht is no longer active ", GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOn)
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOn = -1
		ENDIF
	ENDIF

	// have i left the yacht i was on? (including vehicleS)
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOnIncludingVehicles != -1)
		IF IS_PRIVATE_YACHT_ACTIVE(GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOnIncludingVehicles)
			IF NOT IS_PLAYER_ON_YACHT(PLAYER_ID(), GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOnIncludingVehicles, DEFAULT, TRUE)
				CPRINTLN(DEBUG_YACHT, "UPDATE_YACHTS_I_AM_ON_OR_NEAR - i have left yacht (including vehicles) ", GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOnIncludingVehicles)
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOnIncludingVehicles = -1				
			ENDIF
		ELSE
			CPRINTLN(DEBUG_YACHT, "UPDATE_YACHTS_I_AM_ON_OR_NEAR - yacht is no longer active (including vehicles)", GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOnIncludingVehicles)
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOnIncludingVehicles = -1
		ENDIF
	ENDIF
	
	// have i left the yacht space i was in?
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtSpaceIAmIn != -1)
		IF NOT IS_PLAYER_ON_YACHT(PLAYER_ID(), GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtSpaceIAmIn)
			CPRINTLN(DEBUG_YACHT, "UPDATE_YACHTS_I_AM_ON_OR_NEAR - i have left yacht space ", GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtSpaceIAmIn)
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtSpaceIAmIn = -1				
		ENDIF
	ENDIF	
		
		
		
	IF IS_PRIVATE_YACHT_ACTIVE(iYachtID)
		IF IS_PLAYER_ON_YACHT(PLAYER_ID(), iYachtID)
			IF NOT (GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOn = iYachtID)
				CPRINTLN(DEBUG_YACHT, "UPDATE_YACHTS_I_AM_ON_OR_NEAR - i have just got on yacht ", iYachtID)
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOn = iYachtID
			ENDIF
		ENDIF
		IF IS_PLAYER_ON_YACHT(PLAYER_ID(), iYachtID, DEFAULT, TRUE)
			IF NOT (GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOnIncludingVehicles = iYachtID)
				CPRINTLN(DEBUG_YACHT, "UPDATE_YACHTS_I_AM_ON_OR_NEAR - i have just got on yacht (including vehicles) ", iYachtID)
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmOnIncludingVehicles = iYachtID
			ENDIF
		ENDIF
		
		
		
		
		INT iArray = FLOOR( TO_FLOAT(iYachtID) / 32.0)
		#IF IS_DEBUG_BUILD
			IF NOT (iArray < NUMBER_OF_PRIVATE_YACHT_BITSETS)
				SCRIPT_ASSERT("UPDATE_YACHTS_I_AM_ON_OR_NEAR - PrivateYachtDetails.iBS_YachtsIAmNear - need to increase NUMBER_OF_PRIVATE_YACHT_BITSETS! bug neil f") 
			ENDIF
		#ENDIF		
		IF IS_PLAYER_NEAR_YACHT(PLAYER_ID(), iYachtID)
			SET_BIT(GlobalplayerBD[iPlayer].PrivateYachtDetails.iBS_YachtsIAmNear[iArray], iYachtID - (iArray*32))
		ELSE
			CLEAR_BIT(GlobalplayerBD[iPlayer].PrivateYachtDetails.iBS_YachtsIAmNear[iArray], iYachtID - (iArray*32))
		ENDIF
		
		// is this my nearest yacht?
		fDist = VDIST(GET_COORDS_OF_PRIVATE_YACHT(iYachtID), GET_PLAYER_COORDS(PLAYER_ID()))
		IF (iYachtID = GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmNearest)
			// update the stored dist of currently nearest yacht.
			g_SpawnData.fNearestYachtDist = fDist
		ELSE
			IF (fDist < g_SpawnData.fNearestYachtDist)
			OR (g_SpawnData.fNearestYachtDist <= 0.0)
				g_SpawnData.fNearestYachtDist = fDist
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmNearest = iYachtID
				CPRINTLN(DEBUG_YACHT, "UPDATE_YACHTS_I_AM_ON_OR_NEAR - setting this yacht as my nearest ", iYachtID, " fDist = ", fDist)
			ENDIF
		ENDIF
		
	ELSE
	
		IF (iYachtID = GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmNearest)
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtIAmNearest = -1
			g_SpawnData.fNearestYachtDist = -1.0
			CPRINTLN(DEBUG_YACHT, "UPDATE_YACHTS_I_AM_ON_OR_NEAR - nearest yacht is no longer active ", iYachtID)
		ENDIF
	ENDIF
	
	// update the yacht space player is in
	IF IS_PLAYER_ON_YACHT(PLAYER_ID(), iYachtID)
		IF NOT (GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtSpaceIAmIn = iYachtID)
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iYachtSpaceIAmIn = iYachtID
			CPRINTLN(DEBUG_YACHT, "UPDATE_YACHTS_I_AM_ON_OR_NEAR - i have entered yacht space ", iYachtID)
		ENDIF
	ENDIF
	
ENDPROC

PROC UPDATE_ALL_YACHTS_I_AM_ON_OR_NEAR()
	CPRINTLN(DEBUG_YACHT, "UPDATE_ALL_YACHTS_I_AM_ON_OR_NEAR - called")
	
	// reset the 'nearest' to dist. so we get a fresh value.
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtIAmNearest = -1
	g_SpawnData.fNearestYachtDist = -1.0
	
	INT i
	REPEAT NUMBER_OF_PRIVATE_YACHTS i
		UPDATE_YACHTS_I_AM_ON_OR_NEAR(i)
	ENDREPEAT
	
	// if i am on a yacht refresh the shoreline sound. 2575897
	IF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
		MPGlobalsPrivateYacht.bRefreshShoreSound = TRUE
		MPGlobalsPrivateYacht.RefreshShoreSoundTime = GET_NETWORK_TIME()
		CPRINTLN(DEBUG_YACHT, "UPDATE_ALL_YACHTS_I_AM_ON_OR_NEAR - set bRefreshShoreSound = TRUE")
	ENDIF
	
ENDPROC

PROC HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(INT iYachtID)
	PLAYER_INDEX OwnerID = GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)
	IF (OwnerID = INVALID_PLAYER_INDEX())
		OwnerID = GET_LAST_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID) 
		CPRINTLN(DEBUG_YACHT, "HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY - iYachtID ", iYachtID, ", using last owner ", NATIVE_TO_INT(OwnerID))	
	ELSE
		CPRINTLN(DEBUG_YACHT, "HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY - iYachtID ", iYachtID, ", owner ", NATIVE_TO_INT(OwnerID))		
	ENDIF	
	INT iVehicle
	IF NOT (OwnerID = INVALID_PLAYER_INDEX())
		REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES iVehicle
			IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SET_ANCHOR)
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].PrivateYachtDetails.netVehicleID[iVehicle])	
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].PrivateYachtDetails.netVehicleID[iVehicle])
						SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].PrivateYachtDetails.netVehicleID[iVehicle]))
						CPRINTLN(DEBUG_YACHT, "HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY - iYachtID ", iYachtID, ", iVehicle ", iVehicle, " calling SET_ENTITY_LOCALLY_INVISIBLE")	
					ELSE
						CPRINTLN(DEBUG_YACHT, "HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY - iYachtID ", iYachtID, ", iVehicle ", iVehicle, " entity does not exist")	
					ENDIF
				ELSE
					CPRINTLN(DEBUG_YACHT, "HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY - iYachtID ", iYachtID, ", iVehicle ", iVehicle, " network id does not exist")	
				ENDIF
			ELSE
				CPRINTLN(DEBUG_YACHT, "HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY - iYachtID ", iYachtID, ", iVehicle ", iVehicle, " anchor not set")	
			ENDIF
		ENDREPEAT
	ELSE
		CPRINTLN(DEBUG_YACHT, "HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY - iYachtID ", iYachtID, ", Owner invalid")		
	ENDIF
ENDPROC

FUNC BOOL IS_BLIP_HIDDEN_FOR_PRIVATE_YACHT(INT iYachtID)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		IF IS_THREAD_ACTIVE(MPGlobalsPrivateYacht.BlipDisabledForYachtThreadID)	
			RETURN MPGlobalsPrivateYacht.bBlipDisabledForYacht[iYachtID]
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL ARE_ANY_BLIPS_HIDDEN_FOR_PRIVATE_YACHTS()
	INT iYachtID
	REPEAT NUMBER_OF_PRIVATE_YACHTS iYachtID
		IF IS_BLIP_HIDDEN_FOR_PRIVATE_YACHT(iYachtID)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC

PROC SET_PRIVATE_YACHT_BLIP_AS_HIDDEN(INT iYachtID, BOOL bHide)	
	CPRINTLN(DEBUG_YACHT, "SET_PRIVATE_YACHT_BLIP_AS_HIDDEN - yacht ", iYachtID, " bHide = ", bHide)
	DEBUG_PRINTCALLSTACK()
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
	
		#IF IS_DEBUG_BUILD
		IF IS_THREAD_ACTIVE(MPGlobalsPrivateYacht.BlipDisabledForYachtThreadID)								
			IF NOT (MPGlobalsPrivateYacht.BlipDisabledForYachtThreadID = GET_ID_OF_THIS_THREAD())	
				CASSERTLN(DEBUG_YACHT, "SET_PRIVATE_YACHT_BLIP_AS_HIDDEN - already in use by another thread.")			
			ENDIF
		ENDIF	
		#ENDIF
		
		
		IF (bHide)
			IF NOT (MPGlobalsPrivateYacht.bBlipDisabledForYacht[iYachtID])
				MPGlobalsPrivateYacht.bBlipDisabledForYacht[iYachtID] = TRUE
				IF NOT (MPGlobalsPrivateYacht.BlipDisabledForYachtThreadID = GET_ID_OF_THIS_THREAD())
					MPGlobalsPrivateYacht.BlipDisabledForYachtThreadID = GET_ID_OF_THIS_THREAD()
					CPRINTLN(DEBUG_YACHT, "SET_PRIVATE_YACHT_BLIP_AS_HIDDEN - storing threadid.")
				ENDIF
			ENDIF
		ELSE
			// are any other blips still hidden?
			IF (MPGlobalsPrivateYacht.bBlipDisabledForYacht[iYachtID])
				MPGlobalsPrivateYacht.bBlipDisabledForYacht[iYachtID] = FALSE
				IF NOT ARE_ANY_BLIPS_HIDDEN_FOR_PRIVATE_YACHTS()
					MPGlobalsPrivateYacht.BlipDisabledForYachtThreadID = INT_TO_NATIVE(THREADID, -1)
					CPRINTLN(DEBUG_YACHT, "SET_PRIVATE_YACHT_BLIP_AS_HIDDEN - clearing threadid.")
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

PROC REMOVE_ALL_PRIVATE_YACHTS_IPLS_NOW()
	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_YACHT, "REMOVE_ALL_PRIVATE_YACHTS_IPLS_NOW - called.")
	INT iYacht
	REPEAT NUMBER_OF_PRIVATE_YACHTS iYacht	
		REMOVE_IPLS_FOR_PRIVATE_YACHT(iYacht)
	ENDREPEAT
ENDPROC


FUNC BOOL HAS_YACHT_COLLISION_LOADED(BOOL bIgnoreRequestCount=FALSE)
	IF (MPGlobalsPrivateYacht.iYachtRequests > 0)
	OR (bIgnoreRequestCount)
		RETURN HAS_MODEL_LOADED(GET_YACHT_COLLISION_MODEL())	
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL HAS_YACHT_MODEL_LOADED(BOOL bIgnoreRequestCount=FALSE)
	IF (MPGlobalsPrivateYacht.iYachtRequests > 0)
	OR (bIgnoreRequestCount)
		RETURN HAS_MODEL_LOADED(GET_YACHT_MODEL())	
	ENDIF	
	RETURN FALSE
ENDFUNC



PROC UPDATE_LOCAL_PLAYER_BD_YACHT_AIR_DEFENCE_SETTING()
	IF NETWORK_IS_ACTIVITY_SESSION()
		IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtDefencePISetting != 0
			DEBUG_PRINTCALLSTACK()
			
			CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_LOCAL_PLAYER_BD_YACHT_AIR_DEFENCE_SETTING from ",GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtDefencePISetting,
			" to ", 0, " ")
			
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtDefencePISetting = 0
		ENDIF
	ELIF HAVE_STATS_LOADED(GET_ACTIVE_CHARACTER_SLOT())
		IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtDefencePISetting != GET_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_DEFENCE_SETTING)
			DEBUG_PRINTCALLSTACK()
			
			CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_LOCAL_PLAYER_BD_YACHT_AIR_DEFENCE_SETTING from ",GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtDefencePISetting,
			" to ", GET_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_DEFENCE_SETTING), " ")
			
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtDefencePISetting = GET_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_DEFENCE_SETTING)
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_PLAYER_BD_YACHT_DEFENCE_SETTING(PLAYER_INDEX PlayerID)
	RETURN GlobalplayerBD[NATIVE_TO_INT(PlayerID)].PrivateYachtDetails.iYachtDefencePISetting 
ENDFUNC	

FUNC BOOL SHOULD_PRIVATE_YACHTS_BE_ACTIVE()
	#IF IS_DEBUG_BUILD
	IF (g_bActivateAllPrivateYachts = TRUE)
		RETURN TRUE
	ENDIF
	#ENDIF
	
	#IF FEATURE_FREEMODE_ARCADE
	#IF FEATURE_COPS_N_CROOKS
	IF IS_FREEMODE_ARCADE()
	AND GET_ARCADE_MODE() = ARC_COPS_CROOKS
		RETURN FALSE
	ENDIF
	#ENDIF // FEATURE_COPS_N_CROOKS
	#ENDIF // FEATURE_FREEMODE_ARCADE
	
	IF FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
		PRINTLN("[MYACHT_Spam][HostOwnedYacht_Spam] SHOULD_PRIVATE_YACHTS_BE_ACTIVE - Returning TRUE due to FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
	AND NOT (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_PILOT_SCHOOL)
	AND NOT NETWORK_IS_ACTIVITY_SESSION()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_YACHT_VEHICLE(PLAYER_INDEX piPlayer, INT iVehicle)
	IF IS_NET_PLAYER_OK(piPlayer)
		PED_INDEX playerPed = GET_PLAYER_PED(piPlayer)
		
		IF DOES_ENTITY_EXIST(playerPed)
		AND NOT IS_ENTITY_DEAD(playerPed)
			
			IF IS_PED_IN_ANY_VEHICLE(playerPed)
				VEHICLE_INDEX viVehicle = GET_VEHICLE_PED_IS_USING(playerPed)
				
				IF DOES_ENTITY_EXIST(viVehicle)
				AND NOT IS_ENTITY_DEAD(viVehicle)
					
					IF DECOR_EXIST_ON(viVehicle, "PYV_Owner")
					AND DECOR_EXIST_ON(viVehicle, "PYV_Vehicle")
						INT iPlayerHash = DECOR_GET_INT(viVehicle, "PYV_Owner")
						INT iVehicleHash = DECOR_GET_INT(viVehicle, "PYV_Vehicle")
						
						//IF (iPlayerHash = GET_HASH_KEY(GET_PLAYER_NAME(piPlayer))
						IF iPlayerHash = NETWORK_HASH_FROM_PLAYER_HANDLE(piPlayer)
						AND iVehicle = iVehicleHash
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_YACHT_DEFENCE_DISABLED()
	#IF IS_DEBUG_BUILD
	IF g_sMPTunables.byacht_disable_defenses
		CPRINTLN(DEBUG_YACHT, "IS_YACHT_DEFENCE_DISABLED: byacht_disable_defenses = TRUE")
	ENDIF
	#ENDIF
	RETURN g_sMPTunables.byacht_disable_defenses
ENDFUNC

FUNC BOOL SHOULD_PLAYER_DISABLE_YACHT_DEFENCES(PLAYER_INDEX aPlayer)
	
	#IF IS_DEBUG_BUILD
		BOOL bPrint
		IF GET_FRAME_COUNT() % 2000 < NUM_NETWORK_PLAYERS
			bPRINT = TRUE
		ENDIF
	#ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		#IF IS_DEBUG_BUILD
		IF bPrint 
			PRINTLN("[AIRDEFDIS] SHOULD_PLAYER_DISABLE_YACHT_DEFENCES - Forcing TRUE for activity session ")
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF aPlayer != INVALID_PLAYER_INDEX()
	AND NATIVE_TO_INT(aPlayer) > -1
	
		IF IS_YACHT_DEFENCE_DISABLED()
			#IF IS_DEBUG_BUILD
			IF bPrint 
				PRINTLN("[AIRDEFDIS] SHOULD_PLAYER_DISABLE_YACHT_DEFENCES - IS_YACHT_DEFENCE_DISABLED = TRUE ")
			ENDIF
			#ENDIF
			RETURN TRUE
		ENDIF
			

		IF GB_IS_PLAYER_CRITICAL_BOSS_ON_JOB(aPlayer)
		AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(aPlayer) = FMMC_TYPE_GB_YACHT_ROBBERY
			#IF IS_DEBUG_BUILD
			IF bPrint 
				PRINTLN("[AIRDEFDIS] SHOULD_PLAYER_DISABLE_YACHT_DEFENCES - FMMC_TYPE_GB_YACHT_ROBBERY = TRUE 1 ")
			ENDIF
			#ENDIF
			RETURN TRUE
		ENDIF	
		 
		IF GlobalplayerBD_FM[NATIVE_TO_INT(aPlayer)].iCurrentMissionType = FMMC_TYPE_GB_BOSS_DEATHMATCH
		OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_SALVAGE
		OR GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		OR MPGlobalsAmbience.piBeastPlayer = PLAYER_ID()
			#IF IS_DEBUG_BUILD
			IF bPrint 
				PRINTLN("[AIRDEFDIS] SHOULD_PLAYER_DISABLE_YACHT_DEFENCES - FMMC_TYPE_GB_BOSS_DEATHMATCH OR Wanted level or beast ")
			ENDIF
			#ENDIF
			RETURN TRUE
		ENDIF
		
		IF DOES_PLAYER_OWN_PRIVATE_YACHT(aPlayer)
			INT iYachtID = GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(aPlayer)
			IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
				IF IS_PLAYER_NEAR_YACHT(aPlayer, iYachtID, 350)
				AND IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(aPlayer), TRUE)
					#IF IS_DEBUG_BUILD
					IF bPrint 
						PRINTLN("[AIRDEFDIS] SHOULD_PLAYER_DISABLE_YACHT_DEFENCES - Yacht owner in vehicle nearby ")
					ENDIF
					#ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE

ENDFUNC

FUNC BOOL DOES_PLAYER_OWN_YACHT_AND_DISABLE_YACHT_DEFENCES(PLAYER_INDEX aPlayer)
	
	IF aPlayer != INVALID_PLAYER_INDEX()
		IF DOES_PLAYER_OWN_PRIVATE_YACHT(aPlayer)
			IF SHOULD_PLAYER_DISABLE_YACHT_DEFENCES(aPlayer)
				RETURN TRUE
			ENDIF

		ENDIF
	ENDIF
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_NEAR_ANY_AIR_DEF()
	
	INT iYachtID = GET_NEAREST_ACTIVE_PRIVATE_YACHT_TO_PLAYER(PLAYER_ID())
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		IF IS_PLAYER_NEAR_YACHT(PLAYER_ID(), iYachtID, 350)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_YACHT_HAVE_JACUZZI(INT iYachtID)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		IF IS_PRIVATE_YACHT_ACTIVE(iYachtID)
			RETURN GET_OPTION_OF_PRIVATE_YACHT(iYachtID) != 0
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_LOCAL_PRIVATE_YACHT_USE_SWIMWEAR(BOOL bUseSwimwear)
	IF DOES_PLAYER_OWN_PRIVATE_YACHT(PLAYER_ID())
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_BLOCK_HOT_TUB_SWIMWEAR, NOT bUseSwimwear)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.bYachtJacuzziUseSwimwear = bUseSwimwear
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_YACHT, "SET_PRIVATE_YACHT_PLAYER_BD_USE_SWIMWEAR - setting to ", bUseSwimwear)
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Use swimwear setting for player is saved using SET_PACKED_STAT_BOOL
///    We need other players to know which setting it is so on yacht load we
///    put player's stat setting into his BD so other players can know if they have
///    to use swimwear or not
PROC SYNC_PLAYER_BD_WITH_YACHT_USE_SWIMWEAR_SETTING()
	IF DOES_PLAYER_OWN_PRIVATE_YACHT(PLAYER_ID())
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.bYachtJacuzziUseSwimwear = NOT GET_PACKED_STAT_BOOL(PACKED_MP_STAT_BLOCK_HOT_TUB_SWIMWEAR)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_YACHT, "SYNC_PLAYER_BD_WITH_YACHT_USE_SWIMWEAR_SETTING - synced! value: ", NOT GET_PACKED_STAT_BOOL(PACKED_MP_STAT_BLOCK_HOT_TUB_SWIMWEAR))
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL GET_PRIVATE_YACHT_USE_SWIMWEAR(INT iYachtID)
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		IF IS_PRIVATE_YACHT_ACTIVE(iYachtID)
			//INT iPlayerID = NATIVE_TO_INT(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID))
			//BOOL bUseSwimwear = GlobalplayerBD[iPlayerID].PrivateYachtDetails.bYachtJacuzziUseSwimwear
			BOOL bUseSwimwear = GlobalServerBD_BlockC.PYYachtDetails[iYachtID].bYachtJacuzziUseSwimwear
			
			//#IF IS_DEBUG_BUILD
			//	CPRINTLN(DEBUG_YACHT, "GET_PRIVATE_YACHT_USE_SWIMWEAR - yacht with ID ", iYachtID, " is using swimwear: ", bUseSwimwear)
			//#ENDIF
			
			RETURN bUseSwimwear
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_YACHT_HOT_TUB()
	RETURN g_BInYachtJacuzzi
ENDFUNC

/// PURPOSE:
///    Returns TRUE if player at this very moment should be wearing his swimwear due to being in
///    a yacht jacuzzi and being required to wear swimwear
/// RETURNS:
///    
FUNC BOOL SHOULD_PLAYER_BE_IN_JACUZZI_SWIMWEAR()
	INT iYachtID = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
	
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID) AND IS_PRIVATE_YACHT_ACTIVE(iYachtID)
		RETURN IS_LOCAL_PLAYER_IN_YACHT_HOT_TUB() AND GET_PRIVATE_YACHT_USE_SWIMWEAR(iYachtID)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Display Yacht model name, Yacht name and gamertag
/// when player gets colser to yacht
PROC DISPLAY_YACHT_INFO(INT iYachtID,DISPLAY_INFO_STRUCT& sDisplayInfoStruct)
		
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].eCoronaStatus != CORONA_STATUS_IDLE
		EXIT
	ENDIF
	
	IF g_bDisplayYachtInfo
		IF NOT HAS_NET_TIMER_STARTED(sDisplayInfoStruct.safeDisplayInfoTimer)
		AND NOT IS_PLAYER_SPECTATING(PLAYER_ID())
		AND NOT IS_PAUSE_MENU_ACTIVE()
		AND NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
			START_NET_TIMER(sDisplayInfoStruct.safeDisplayInfoTimer)
		ELSE
			IF HAS_NET_TIMER_STARTED(sDisplayInfoStruct.safeDisplayInfoTimer)
			//IF HAS_NET_TIMER_EXPIRED(safeDisplayInfoTimer,500)
			// we are currently displaying the title
			INT iTimeDisplayed
			INT iAlpha
			INT iTimeLeft
			PLAYER_INDEX yachtOwner = GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)
			TEXT_LABEL_31 txtYachtPersonalName
			STRING txtModelName = ""
			FLOAT fDisplayX, fDisplayY
			IF yachtOwner != INVALID_PLAYER_INDEX()
			AND NETWORK_IS_PLAYER_ACTIVE(yachtOwner)
				sDisplayInfoStruct.sSavedPlayerName = GET_PLAYER_NAME(yachtOwner)
				fDisplayY = sDisplayInfoStruct.fInfoDisplayPosY
			ELSE 
				sDisplayInfoStruct.sSavedPlayerName = ""
				fDisplayY = sDisplayInfoStruct.fInfoDisplayPosY + 0.02
			ENDIF
			
			// Dont show yacht name for restricted accounts
			IF GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID) != INVALID_PLAYER_INDEX()
				IF GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID) = PLAYER_ID()
					txtYachtPersonalName = GET_NAME_OF_PRIVATE_YACHT(iYachtID)
					
				ELIF (GB_IS_GLOBAL_CLIENT_BIT0_SET(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID), eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_ACCOUNT)
				OR GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_ACCOUNT)
				OR (DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID))) AND GB_IS_THIS_PLAYERS_GANG_NAME_BLOCKED(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)))
				OR sDisplayInfoStruct.bUserContentIsBlocked = TRUE)
				AND NOT IS_DURANGO_AND_CAN_VIEW_GAMER_USER_CONTENT(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID))
					IF NOT (sDisplayInfoStruct.bUserContentIsBlocked)
						sDisplayInfoStruct.bUserContentIsBlocked = TRUE
						CPRINTLN(DEBUG_YACHT, "setting sDisplayInfoStruct.bUserContentIsBlocked = TRUE")
					ENDIF
					txtYachtPersonalName = GET_FILENAME_FOR_AUDIO_CONVERSATION("YACHT_GSY")
				ELSE
					txtYachtPersonalName = GET_NAME_OF_PRIVATE_YACHT(iYachtID)
				ENDIF
			ENDIF
			
			//Block area name from displaying as this will clash.
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
			iTimeDisplayed = GET_GAME_TIMER() - g_iFlowTimeBeganDisplayMissionTitle	
				IF NOT IS_SCREEN_FADED_OUT()
					iAlpha = 255
					IF iTimeDisplayed < sDisplayInfoStruct.iInfoDisplayFadeTime
						//Fade text in over the first second.
						iAlpha = CEIL(TO_FLOAT(iTimeDisplayed)/1000.0*255.0)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sDisplayInfoStruct.safeDisplayInfoTimer,3000)
							iTimeLeft = sDisplayInfoStruct.iInfoDisplayTime - iTimeDisplayed
							IF iTimeLeft < sDisplayInfoStruct.iInfoDisplayFadeTime
								//Fade text out over the last second.
								iAlpha = CEIL(TO_FLOAT(iTimeLeft)/1000.0*255.0)
								IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_YACHT_INFO_DISPLAY)
									CPRINTLN(DEBUG_YACHT, "Yacht info display alpha:",iAlpha)
									SET_BIT (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_YACHT_INFO_DISPLAY)
									RESET_NET_TIMER(sDisplayInfoStruct.safeDisplayInfoTimer)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					fDisplayX= sDisplayInfoStruct.fInfoDisplayPosX
					

					SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_BOTTOM)
					SET_TEXT_FONT(FONT_CURSIVE)
					SET_TEXT_JUSTIFICATION(FONT_RIGHT)
		
					SET_TEXT_COLOUR(255	, 255, 255, iAlpha) // white text
					SET_TEXT_DROP_SHADOW()
					
					SWITCH  GET_OPTION_OF_PRIVATE_YACHT(iYachtID)
						CASE 0
							txtModelName = "YACHT_CLASSIC"
						BREAK
						CASE 1
							txtModelName = "YACHT_DELUXE"
						BREAK
						CASE 2
							txtModelName = "YACHT_SUPREME"
						BREAK	
					ENDSWITCH

					// in game version
					SET_SCRIPT_GFX_ALIGN_PARAMS(fDisplayX, fDisplayY, sDisplayInfoStruct.fInfoDisplaySizeX,sDisplayInfoStruct.fInfoDisplaySizeY - 0.008)
					SET_TEXT_SCALE(sDisplayInfoStruct.fInfoDisplayScaleX, sDisplayInfoStruct.fInfoDisplayScaleY)

					// if 4:3 use smaller value
					IF (NOT GET_IS_WIDESCREEN() AND NOT GET_IS_HIDEF())
						sDisplayInfoStruct.fMaxInfoDisplayWidth = sDisplayInfoStruct.fInfoDisplayMaxWidth_4_3
						//CPRINTLN(DEBUG_ACHIEVEMENT, "[4:3] MISSION TITLE:", txtModelName, " WIDTH:", GET_STRING_WIDTH(txtModelName), " MAX:", fMaxInfoDisplayWidth)
					ELSE
						sDisplayInfoStruct.fMaxInfoDisplayWidth = sDisplayInfoStruct.fInfoDisplayMaxWidth
						//CPRINTLN(DEBUG_ACHIEVEMENT, "MISSION TITLE:", txtModelName, " WIDTH:", GET_STRING_WIDTH(txtModelName), " MAX:", fMaxInfoDisplayWidth)
					ENDIF
					
					IF (fDisplayY = sDisplayInfoStruct.fInfoDisplayPosY) 
						IF (GET_STRING_WIDTH(txtModelName) > sDisplayInfoStruct.fMaxInfoDisplayWidth)
							IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_SUBTITLE_TEXT)
								//SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, vOriginalSubtitleTextPos.x, vOriginalSubtitleTextPos.y + fSubtitleTextPosOffsetY)	
								//g_bMissionTitleMovedSubtitles = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					BEGIN_TEXT_COMMAND_DISPLAY_TEXT(txtModelName)
					END_TEXT_COMMAND_DISPLAY_TEXT(fDisplayX, fDisplayY-0.08)
					CPRINTLN(DEBUG_YACHT, "Displaying yacht model name")

					SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_BOTTOM)
					SET_TEXT_FONT(FONT_CURSIVE)
					SET_TEXT_JUSTIFICATION(FONT_RIGHT)

					SET_TEXT_COLOUR(255	, 255, 255, iAlpha) // white text
					SET_TEXT_DROP_SHADOW()
					
					// in game version
					SET_SCRIPT_GFX_ALIGN_PARAMS(fDisplayX, fDisplayY, sDisplayInfoStruct.fInfoDisplaySizeX, sDisplayInfoStruct.fInfoDisplaySizeY)
					SET_TEXT_SCALE(sDisplayInfoStruct.fInfoDisplayScaleX, sDisplayInfoStruct.fInfoDisplayScaleY)

					// if 4:3 use smaller value
					IF (NOT GET_IS_WIDESCREEN() AND NOT GET_IS_HIDEF())
						sDisplayInfoStruct.fMaxInfoDisplayWidth = sDisplayInfoStruct.fInfoDisplayMaxWidth_4_3
						//CPRINTLN(DEBUG_ACHIEVEMENT, "[4:3] MISSION TITLE:", txtModelName, " WIDTH:", GET_STRING_WIDTH(txtModelName), " MAX:", fMaxInfoDisplayWidth)
					ELSE
						sDisplayInfoStruct.fMaxInfoDisplayWidth = sDisplayInfoStruct.fInfoDisplayMaxWidth
						//CPRINTLN(DEBUG_ACHIEVEMENT, "MISSION TITLE:", txtModelName, " WIDTH:", GET_STRING_WIDTH(txtModelName), " MAX:", fMaxInfoDisplayWidth)
					ENDIF
			
					BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sDisplayInfoStruct.sSavedPlayerName)
					END_TEXT_COMMAND_DISPLAY_TEXT(fDisplayX, fDisplayY)
					CPRINTLN(DEBUG_YACHT, "Displaying yacht owner name")
					
					SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_BOTTOM)
					SET_TEXT_FONT(FONT_CURSIVE)
					SET_TEXT_JUSTIFICATION(FONT_RIGHT)

					SET_TEXT_COLOUR(255	, 255, 255, iAlpha) // white text
					SET_TEXT_DROP_SHADOW()
					
					// in game version
					SET_SCRIPT_GFX_ALIGN_PARAMS(fDisplayX, fDisplayY, sDisplayInfoStruct.fInfoDisplaySizeX, sDisplayInfoStruct.fInfoDisplaySizeY - 0.004)
					SET_TEXT_SCALE(sDisplayInfoStruct.fInfoDisplayScaleX, sDisplayInfoStruct.fInfoDisplayScaleY)
					// if 4:3 use smaller value
					IF (NOT GET_IS_WIDESCREEN() AND NOT GET_IS_HIDEF())
						sDisplayInfoStruct.fMaxInfoDisplayWidth = sDisplayInfoStruct.fInfoDisplayMaxWidth_4_3
						//CPRINTLN(DEBUG_ACHIEVEMENT, "[4:3] MISSION TITLE:", txtModelName, " WIDTH:", GET_STRING_WIDTH(txtModelName), " MAX:", fMaxInfoDisplayWidth)
					ELSE
						sDisplayInfoStruct.fMaxInfoDisplayWidth = sDisplayInfoStruct.fInfoDisplayMaxWidth
						//CPRINTLN(DEBUG_ACHIEVEMENT, "MISSION TITLE:", txtModelName, " WIDTH:", GET_STRING_WIDTH(txtModelName), " MAX:", fMaxInfoDisplayWidth)
					ENDIF
					
					IF (fDisplayY = sDisplayInfoStruct.fInfoDisplayPosY) 
						IF (GET_STRING_WIDTH_PLAYER_NAME(txtYachtPersonalName) > sDisplayInfoStruct.fMaxInfoDisplayWidth)
							IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_SUBTITLE_TEXT)
								//SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, vOriginalSubtitleTextPos.x, vOriginalSubtitleTextPos.y + fSubtitleTextPosOffsetY)	
								//g_bMissionTitleMovedSubtitles = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(txtYachtPersonalName)
					END_TEXT_COMMAND_DISPLAY_TEXT(fDisplayX, fDisplayY-0.04)
					CPRINTLN(DEBUG_YACHT, "Displaying yacht model name")
					
					RESET_SCRIPT_GFX_ALIGN()
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	g_bDisplayYachtInfo = TRUE
ENDPROC

/// PURPOSE: Disable displaying yacht info in bottom right corner 
PROC DISABLE_YACHT_INFO_THIS_FRAME()
	g_bDisplayYachtInfo = FALSE
	
	#IF IS_DEBUG_BUILD
	IF GET_FRAME_COUNT() % 1000 = 0
		DEBUG_PRINTCALLSTACK()
		CPRINTLN(DEBUG_YACHT, "Disable displaying yacht info")
	ENDIF
	#ENDIF
ENDPROC 

/*
FUNC BOOL IS_ENTITY_TOUCHING_A_BOAT(ENTITY_INDEX entity)
	IF IS_ENTITY_TOUCHING_MODEL(entity, DINGHY)
	OR IS_ENTITY_TOUCHING_MODEL(entity, DINGHY2)
	OR IS_ENTITY_TOUCHING_MODEL(entity, DINGHY3)
	OR IS_ENTITY_TOUCHING_MODEL(entity, DINGHY4)
	OR IS_ENTITY_TOUCHING_MODEL(entity, JETMAX)
	OR IS_ENTITY_TOUCHING_MODEL(entity, MARQUIS)
	OR IS_ENTITY_TOUCHING_MODEL(entity, PREDATOR)
	OR IS_ENTITY_TOUCHING_MODEL(entity, SEASHARK)
	OR IS_ENTITY_TOUCHING_MODEL(entity, SEASHARK2)
	OR IS_ENTITY_TOUCHING_MODEL(entity, SEASHARK3)
	OR IS_ENTITY_TOUCHING_MODEL(entity, SQUALO)
	OR IS_ENTITY_TOUCHING_MODEL(entity, SPEEDER)
	OR IS_ENTITY_TOUCHING_MODEL(entity, SPEEDER2)
	OR IS_ENTITY_TOUCHING_MODEL(entity, SUNTRAP)
	OR IS_ENTITY_TOUCHING_MODEL(entity, TORO)
	OR IS_ENTITY_TOUCHING_MODEL(entity, TORO2)
	OR IS_ENTITY_TOUCHING_MODEL(entity, TROPIC)
	OR IS_ENTITY_TOUCHING_MODEL(entity, TROPIC2)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
*/

FUNC BOOL ARE_YACHT_BUOYS_DISABLED()
	#IF IS_DEBUG_BUILD
	IF g_sMPTunables.byacht_disable_buoys
		CPRINTLN(DEBUG_YACHT, "ARE_YACHT_BUOYS_DISABLED: byacht_disable_buoys = TRUE")
	ENDIF
	#ENDIF
	RETURN g_sMPTunables.byacht_disable_buoys
ENDFUNC

FUNC BOOL IS_YACHT_REAR_DOCKING_DISABLED()
	#IF IS_DEBUG_BUILD
	IF g_sMPTunables.byacht_disable_rear_docking
		CPRINTLN(DEBUG_YACHT, "IS_YACHT_REAR_DOCKING_DISABLED: byacht_disable_rear_docking = TRUE")
	ENDIF
	#ENDIF
	
	IF FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
	AND g_bMissionEnding
		RETURN FALSE
	ENDIF
	
	RETURN g_sMPTunables.byacht_disable_rear_docking
ENDFUNC

PROC DELETE_VEHICLE_NOW_FROM_YACHT(VEHICLE_INDEX VehicleID, INT iYachtID)
	CPRINTLN(DEBUG_YACHT, "DELETE_VEHICLE_NOW_FROM_YACHT - called, yacht ", iYachtID)
	IF DOES_ENTITY_EXIST(VehicleID)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)	
			IF IS_VEHICLE_ON_YACHT(VehicleID, iYachtID, YACHT_INCLUDING_VEHICLES_DISTANCE)
			
				IF DECOR_IS_REGISTERED_AS_TYPE("PYV_WarpFrom", DECOR_TYPE_INT)
				AND DECOR_EXIST_ON(VehicleID, "PYV_WarpFrom")
				AND (DECOR_GET_INT(VehicleID, "PYV_WarpFrom") = iYachtID)
					CPRINTLN(DEBUG_YACHT, "DELETE_VEHICLE_NOW_FROM_YACHT - this vehicle is set to warp, do not delete. ", iYachtID)
				ELIF DOES_THIS_VEHICLE_BELONG_TO_GANG_BOSS_MISSION(VehicleID)
					CPRINTLN(DEBUG_YACHT, "DELETE_VEHICLE_NOW_FROM_YACHT - not touching this veh, it belongs to GB mission. ", iYachtID)
				ELSE
				
					SET_ENTITY_AS_MISSION_ENTITY(VehicleID, FALSE, TRUE )
					IF IS_VEHICLE_EMPTY(VehicleID, TRUE, TRUE)
						DELETE_VEHICLE(VehicleID)
						CPRINTLN(DEBUG_YACHT, "DELETE_VEHICLE_NOW_FROM_YACHT - deleted vehicle. ", iYachtID)
					ELSE
						SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
						CPRINTLN(DEBUG_YACHT, "DELETE_VEHICLE_NOW_FROM_YACHT - set vehicle as no longer needed. ", iYachtID)
					ENDIF
					
					
				ENDIF
			ELSE
				CPRINTLN(DEBUG_YACHT, "DELETE_VEHICLE_NOW_FROM_YACHT - vehicle not on yacht. ", iYachtID)
			ENDIF
		ELSE
			CPRINTLN(DEBUG_YACHT, "DELETE_VEHICLE_NOW_FROM_YACHT - not in control. ", iYachtID)
		ENDIF	
	ELSE
		CPRINTLN(DEBUG_YACHT, "DELETE_VEHICLE_NOW_FROM_YACHT - not exist. ", iYachtID)
	ENDIF
ENDPROC

PROC DELETE_YACHT_VEHICLES_NOW_FOR_YACHT(INT iYachtID)

	INT iNumVehicles
	VEHICLE_INDEX VehicleID[32]
	iNumVehicles = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), VehicleID)
	INT i
	
	CPRINTLN(DEBUG_YACHT, "DELETE_YACHT_VEHICLES_NOW_FOR_YACHT - called, iYachtID ", iYachtID, " iNumVehicles = ", iNumVehicles)
	
	REPEAT iNumVehicles i
		DELETE_VEHICLE_NOW_FROM_YACHT(VehicleID[i], iYachtID)
	ENDREPEAT


	INT iVehicle
	VECTOR vCoords
	FLOAT fHeading
	VEHICLE_INDEX ThisVehicleID
	REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES iVehicle
		GET_PRIVATE_YACHT_SPAWN_VEHICLE_LOCATION(iVehicle, iYachtID, vCoords, fHeading, FALSE)
		CPRINTLN(DEBUG_YACHT, "DELETE_YACHT_VEHICLES_NOW_FOR_YACHT - checking iVehicle ", iVehicle, ", iYachtID ", iYachtID, ", vCoords ", vCoords, ", fHeading ", fHeading)	
		ThisVehicleID = GET_CLOSEST_VEHICLE(vCoords, 5.0, DUMMY_MODEL_FOR_SCRIPT, 12294)
		DELETE_VEHICLE_NOW_FROM_YACHT(ThisVehicleID, iYachtID)
	ENDREPEAT
		

ENDPROC

PROC DELETE_ABANDONED_VEHICLE_NOW_FROM_YACHT(VEHICLE_INDEX VehicleID, INT iYachtID)
	IF DOES_ENTITY_EXIST(VehicleID)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)	
			IF IS_VEHICLE_ON_YACHT(VehicleID, iYachtID, YACHT_INCLUDING_VEHICLES_DISTANCE)
			
				// is this vehicle abandoned? i.e the personal vehicle decorator has been set to -1
				IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
				AND DECOR_EXIST_ON(VehicleID, "Player_Vehicle")
				AND (DECOR_GET_INT(VehicleID, "Player_Vehicle") = -1)
		
					SET_ENTITY_AS_MISSION_ENTITY(VehicleID, FALSE, TRUE )	
					DELETE_VEHICLE(VehicleID)		
					
				ELSE
					CPRINTLN(DEBUG_YACHT, "DELETE_ABANDONED_VEHICLE_NOW_FROM_YACHT - not an abandoned pv. ", iYachtID)	
				ENDIF
			ELSE
				CPRINTLN(DEBUG_YACHT, "DELETE_ABANDONED_VEHICLE_NOW_FROM_YACHT - vehicle not on yacht. ", iYachtID)	
			ENDIF
		ELSE
			CPRINTLN(DEBUG_YACHT, "DELETE_ABANDONED_VEHICLE_NOW_FROM_YACHT - not in control. ", iYachtID)	
		ENDIF	
	ELSE
		CPRINTLN(DEBUG_YACHT, "DELETE_ABANDONED_VEHICLE_NOW_FROM_YACHT - not exist. ", iYachtID)
	ENDIF
ENDPROC

PROC DELETE_ABANDONED_VEHICLES_ON_YACHT_NOW(INT iYachtID)

	INT iNumVehicles
	VEHICLE_INDEX VehicleID[32]
	iNumVehicles = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), VehicleID)
	INT i
	
	CPRINTLN(DEBUG_YACHT, "DELETE_ABANDONED_VEHICLES_ON_YACHT_NOW - called, iYachtID ", iYachtID, " iNumVehicles = ", iNumVehicles)
	
	REPEAT iNumVehicles i
		DELETE_ABANDONED_VEHICLE_NOW_FROM_YACHT(VehicleID[i], iYachtID)
	ENDREPEAT


	INT iVehicle
	VECTOR vCoords
	FLOAT fHeading
	VEHICLE_INDEX ThisVehicleID
	REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES iVehicle
		GET_PRIVATE_YACHT_SPAWN_VEHICLE_LOCATION(iVehicle, iYachtID, vCoords, fHeading, FALSE)
		CPRINTLN(DEBUG_YACHT, "DELETE_ABANDONED_VEHICLES_ON_YACHT_NOW - checking iVehicle ", iVehicle, ", iYachtID ", iYachtID, ", vCoords ", vCoords, ", fHeading ", fHeading)	
		ThisVehicleID = GET_CLOSEST_VEHICLE(vCoords, 5.0, DUMMY_MODEL_FOR_SCRIPT, 12294)
		DELETE_ABANDONED_VEHICLE_NOW_FROM_YACHT(ThisVehicleID, iYachtID)
	ENDREPEAT

ENDPROC



// EOF

