USING "net_cinematic_cam_sequencer_typedefs.sch"
USING "cinematic_cam_behaviour.sch"
USING "rc_helper_functions.sch"
USING "rgeneral_include.sch"

CONST_INT CINEMATIC_CAM_SEQUENCE_DEBUG_FEATURE_XML_SAVING 1

#IF IS_DEBUG_BUILD
DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_LERP_BEHAVIOUR_WIDGET(CINEMATIC_CAM_BEHAVIOUR_DEBUG &sInst)
	START_WIDGET_GROUP("Position Lerp")
		ADD_WIDGET_INT_SLIDER("Duration", sInst.sCam.iFOVLerpDurationMS, 0, 20000, 100)
		ADD_WIDGET_INT_SLIDER("Hold Duration", sInst.sCam.iLerpDelay, -1, 20000, 100)
		START_WIDGET_GROUP("Start")
		ADD_WIDGET_VECTOR_SLIDER("Start Position", sInst.sCam.vStartPos, -100000, 100000, 0.1)
		ADD_WIDGET_VECTOR_SLIDER("Start Rotation", sInst.sCam.vStartRot, -100000, 100000, 0.1)
		ADD_WIDGET_BOOL("Copy Debug Cam Transform", sInst.bCopyStartTransform)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("End")
		ADD_WIDGET_VECTOR_SLIDER("End Position", sInst.sCam.vEndPos, -100000, 100000, 0.1)
		ADD_WIDGET_VECTOR_SLIDER("End Rotation", sInst.sCam.vEndRot, -100000, 100000, 0.1)
		ADD_WIDGET_BOOL("Copy Debug Cam Transform", sInst.bCopyEndTransform)	
		STOP_WIDGET_GROUP()
		ADD_WIDGET_BOOL("Remove", sInst.bRemoveBehaviour[CCB_LERP])
	STOP_WIDGET_GROUP()
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_FOV_LERP_BEHAVIOUR_WIDGET(CINEMATIC_CAM_BEHAVIOUR_DEBUG &sInst)
	START_WIDGET_GROUP("FOV Lerp")
		ADD_WIDGET_FLOAT_SLIDER("Start FOV", sInst.sCam.fStartFOV, 1, 100, 1)
		ADD_WIDGET_BOOL("Copy Debug Cam FOV to Start FOV", sInst.bCopyStartFov)
		ADD_WIDGET_FLOAT_SLIDER("End FOV",  sInst.sCam.fEndFOV, 1, 100, 1)
		ADD_WIDGET_BOOL("Copy Debug Cam FOV to End FOV", sInst.bCopyEndFov)
		ADD_WIDGET_INT_SLIDER("Duration MS", sInst.sCam.iFOVLerpDurationMS, 0, 60000, 100)		
		ADD_WIDGET_BOOL("Remove", sInst.bRemoveBehaviour[CCB_LERP_FOV])
	STOP_WIDGET_GROUP()
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_ORBIT_COORD_BEHAVIOUR_WIDGET(CINEMATIC_CAM_BEHAVIOUR_DEBUG &sInst)
	START_WIDGET_GROUP("Orbit Coord")
		ADD_WIDGET_INT_SLIDER("Duration MS", sInst.sCam.iCameraDuration, -1, 20000, 100)
		ADD_WIDGET_VECTOR_SLIDER("Origin", sInst.sCam.vOrbitCentre, -100000, 100000, 0.1)
		ADD_WIDGET_BOOL("Copy Debug Cam Pos to Orbit Origin", sInst.bCopyOrbitOrigin)
		ADD_WIDGET_FLOAT_SLIDER("Distance", sInst.sCam.fDistanceFromCentre, 0, 100, 0.1)
		ADD_WIDGET_BOOL("Copy Debug Cam Orbit", sInst.bCopyOrbitDistance)
		ADD_WIDGET_FLOAT_SLIDER("Speed (Degrees Per Second)", sInst.sCam.fOrbitSpeed, 0.1, 100, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Start Angle", sInst.sCam.fStartTheta, 0, 360, 1)
		ADD_WIDGET_INT_SLIDER("Direction", sInst.sCam.iOrbitDirection, -1, 1, 2)
		ADD_WIDGET_BOOL("Remove", sInst.bRemoveBehaviour[CCB_ORBIT_COORD])
	STOP_WIDGET_GROUP()
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_ORBIT_PLAYER_BEHAVIOUR_WIDGET(CINEMATIC_CAM_BEHAVIOUR_DEBUG &sInst)
	START_WIDGET_GROUP("Orbit Player")
		ADD_WIDGET_INT_SLIDER("Duration MS", sInst.sCam.iCameraDuration, 0, 20000, 100)
		ADD_WIDGET_VECTOR_SLIDER("Offset", sInst.sCam.vOrbitOffset, -100000, 100000, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Distance", sInst.sCam.fDistanceFromCentre, 0, 100, 0.1)
		ADD_WIDGET_BOOL("Copy Debug Cam distance to Player", sInst.bCopyOrbitDistance)
		ADD_WIDGET_FLOAT_SLIDER("Speed (Degrees Per Second)", sInst.sCam.fOrbitSpeed, 0.1, 100, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Start Angle", sInst.sCam.fStartTheta, 0, 360, 1)
		ADD_WIDGET_INT_SLIDER("Direction", sInst.sCam.iOrbitDirection, -1, 1, 2)
		ADD_WIDGET_BOOL("Remove", sInst.bRemoveBehaviour[CCB_ORBIT_ENTITY])
	STOP_WIDGET_GROUP()
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_LOOK_AT_COORD_BEHAVIOUR_WIDGET(CINEMATIC_CAM_BEHAVIOUR_DEBUG &sInst)
	START_WIDGET_GROUP("Look At Coord")
		ADD_WIDGET_VECTOR_SLIDER("Target", sInst.sCam.vLookAtCoord, -100000, 100000, 0.1)
		ADD_WIDGET_BOOL("Copy Debug Cam Coord to Look At Coord", sInst.bCopyLookatCoord)
		ADD_WIDGET_BOOL("Remove", sInst.bRemoveBehaviour[CCB_LOOK_AT_COORD])
	STOP_WIDGET_GROUP()
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_LOOK_AT_PLAYER_BEHAVIOUR_WIDGET(CINEMATIC_CAM_BEHAVIOUR_DEBUG &sInst)
	START_WIDGET_GROUP("Look At Player")
		ADD_WIDGET_VECTOR_SLIDER("Offset", sInst.sCam.vLookAtOffset, -100000, 100000, 0.1)
		ADD_WIDGET_BOOL("Remove", sInst.bRemoveBehaviour[CCB_LOOK_AT_ENTITY])
	STOP_WIDGET_GROUP()
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_SHAKE_BEHAVIOUR(CINEMATIC_CAM_BEHAVIOUR_DEBUG &sInst)
	START_WIDGET_GROUP("Shake")
		ADD_WIDGET_FLOAT_SLIDER("Strength", sInst.sCam.fCamShakeIntensity, 0, 100.0, 0.1)
		ADD_WIDGET_BOOL("Remove", sInst.bRemoveBehaviour[CCB_SHAKE])
	STOP_WIDGET_GROUP()
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_BASIC_SETTINGS(CINEMATIC_CAM_BEHAVIOUR_DEBUG &sInst)	
	ADD_WIDGET_BOOL("Preview", sInst.bPreview)
	START_WIDGET_GROUP("Basic Settings")
		ADD_WIDGET_FLOAT_SLIDER("Random Weighting", sInst.sCam.fWeighting, 0.0, 100.0, 1.0)
		ADD_WIDGET_INT_SLIDER("Duration MS", sInst.sCam.iCameraDuration, -1, 20000, 100)
		ADD_WIDGET_FLOAT_SLIDER("FOV", sInst.sCam.fStartFOV, 1, 100, 1)
		
		START_WIDGET_GROUP("Start Position")
			ADD_WIDGET_VECTOR_SLIDER("Position", sInst.sCam.vStartPos, -100000, 100000, 0.1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Start Rotation")
			ADD_WIDGET_VECTOR_SLIDER("Rotation", sInst.sCam.vStartRot, -100000, 100000, 0.1)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_BOOL("Copy Current Cam Settings", sInst.bCopyBasicSettings)		
	STOP_WIDGET_GROUP()
	ADD_WIDGET_BOOL("Remove Camera", sInst.bRemoveCam)
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_BEHAVIOUR_WIDGETS(CINEMATIC_CAM_BEHAVIOUR_DEBUG &sInst)
	START_NEW_WIDGET_COMBO()
		INT iBehaviour
		INT iMaxBehaviour = COUNT_OF(CINEMATIC_CAM_BEHAVIOUR_TYPE)
		STRING strBehaviourName
		
		REPEAT iMaxBehaviour iBehaviour
			strBehaviourName = DEBUG_GET_CINEMATIC_CAM_BEHAVIOUR_TYPE_AS_STRING(INT_TO_ENUM(CINEMATIC_CAM_BEHAVIOUR_TYPE, iBehaviour))
			ADD_TO_WIDGET_COMBO(strBehaviourName)
		ENDREPEAT
	STOP_WIDGET_COMBO("Behaviour To Add: ", sInst.iBehaviourType)
	ADD_WIDGET_BOOL("Add selected behaviour: ", sInst.bAddBehaviour)
	
	IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_LERP)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_LERP_BEHAVIOUR_WIDGET(sInst)
	ENDIF
	
	IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_LERP_FOV)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_FOV_LERP_BEHAVIOUR_WIDGET(sInst)
	ENDIF
	
	IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_ORBIT_COORD)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_ORBIT_COORD_BEHAVIOUR_WIDGET(sInst)
	ENDIF
	
	IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_ORBIT_ENTITY)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_ORBIT_PLAYER_BEHAVIOUR_WIDGET(sInst)
	ENDIF
	
	IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_LOOK_AT_COORD)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_LOOK_AT_COORD_BEHAVIOUR_WIDGET(sInst)
	ENDIF
	
	IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_LOOK_AT_ENTITY)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_LOOK_AT_PLAYER_BEHAVIOUR_WIDGET(sInst)
	ENDIF
	
	IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_SHAKE)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_SHAKE_BEHAVIOUR(sInst)
	ENDIF
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_CAMERA_WIDGET(CINEMATIC_CAM_BEHAVIOUR_DEBUG &sInst, INT iCamIndex)
	TEXT_LABEL_63 tlName = sInst.strName
	tlName = "CAM_"
	tlName += iCamIndex
	sInst.strName = tlName
	
	sInst.wgiRootID = START_WIDGET_GROUP(tlName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_BASIC_SETTINGS(sInst)
		
		sInst.wgiBehaviourGroupID = START_WIDGET_GROUP("Behaviours")
			_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_BEHAVIOUR_WIDGETS(sInst)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	
	sInst.bAdded = TRUE
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_CAMERAS_WIDGETS(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	INT iMaxCam = sInst.sDebug.iCamCount
	
	IF iMaxCam <=0
		EXIT
	ENDIF
	
	INT iCam
	sInst.sDebug.iNextID = 0
	
	REPEAT iMaxCam iCam
		
		_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_CAMERA_WIDGET(sInst.sDebug.sDebugCams[iCam], sInst.sDebug.iNextID)
		sInst.sDebug.iNextID++
	ENDREPEAT
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__CREATE_WIDGETS(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst, STRING strTitle = NULL)
	
	IF DOES_WIDGET_GROUP_EXIST(sInst.sDebug.wgiRootID)
		DELETE_WIDGET_GROUP(sInst.sDebug.wgiRootID)
		PRINTLN("[CINEMATIC_CAM_SEQUENCE][DEBUG] ADD_WIDGETS - Widget group exists, deleting")
	ENDIF
	
	TEXT_LABEL_63 tlTitle = "Camera Sequence"
	
	IF NOT IS_STRING_NULL_OR_EMPTY(strTitle)
		tlTitle += " - "
		tlTitle += strTitle
	ENDIF
	
	sInst.sDebug.wgiRootID = START_WIDGET_GROUP(tlTitle)
		ADD_WIDGET_BOOL("Edit Sequence", sInst.sDebug.bEdit)
		ADD_WIDGET_BOOL("Reset to Script Cams", sInst.sDebug.bResetToScriptCams)
		ADD_WIDGET_INT_READ_ONLY("Previewing Camera", sInst.iCurrentCamBehaviourIndex)
		ADD_WIDGET_BOOL("Random sequence", sInst.bRandomise)
		ADD_WIDGET_BOOL("Draw Debug Info", sInst.sDebug.bDrawDebug)
		
		#IF CINEMATIC_CAM_SEQUENCE_DEBUG_FEATURE_XML_SAVING
		sInst.sDebug.twiXMLLoadName = ADD_TEXT_WIDGET("Load Name")
		ADD_WIDGET_BOOL("Load XML", sInst.sDebug.bLoadXML)	
		sInst.sDebug.twiXMLSaveName = ADD_TEXT_WIDGET("Save Name")
		ADD_WIDGET_BOOL("Save XML", sInst.sDebug.bSaveXML)
		#ENDIF
		
		ADD_WIDGET_BOOL("Output Script Lookup to temp debug", sInst.sDebug.bOutputScript)
		
		// Inject instance specific widgets if provided
		IF sInst.sDebug.fpCustomWidget != NULL
			CALL sInst.sDebug.fpCustomWidget()
		ENDIF
		
		
		sInst.sDebug.wgiCamGroupID = START_WIDGET_GROUP("Cameras")
			_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_CAMERAS_WIDGETS(sInst)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_BOOL("Add Camera", sInst.sDebug.bAddCam)
	STOP_WIDGET_GROUP()
ENDPROC

PROC CINEMATIC_CAM_SEQUENCE_DEBUG__SET_CUSTOM_WIDGETS(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst, DANCE_CINEMATIC_CAM_ADD_WIDGET fpWidget)
	sInst.sDebug.fpCustomWidget = fpWidget
ENDPROC

PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__COPY_CAM_LOOKUP_TO_DEBUG_CAMS(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	INT iMaxCam = sInst.iMaxCam
	
	IF iMaxCam <= 0 
		EXIT
	ENDIF
	
	INT iCam
	
	CINEMATIC_CAM_BEHAVIOUR_DEBUG sEmpty
	
	REPEAT iMaxCam iCam
		IF iCam >= COUNT_OF(sInst.sDebug.sDebugCams)
			ASSERTLN("[CINEMATIC_CAM_SEQUENCE][DEBUG] COPY_CAM_LOOKUP_TO_DEBUG_CAMS - exceeded debug cam limit! some cameras will not be shown. Contact Tom.Turner@rockstarnorth.com to get the limit increased")
			BREAKLOOP
		ENDIF
	
		IF sInst.fpCamLookup != NULL
			sInst.sDebug.sDebugCams[iCam] = sEmpty
			sInst.sDebug.sDebugCams[iCam].sCam = CALL sInst.fpCamLookup(iCam)
		ENDIF
	ENDREPEAT
	
	sInst.sDebug.iNextID = 0
	sInst.sDebug.iCamCount = iMaxCam
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_CAMS_CURRENT_SELECTED_BEHAVIOUR(CINEMATIC_CAM_BEHAVIOUR_DEBUG &sInst)
	ENTITY_INDEX eiPlayer = PLAYER_PED_ID()
	VECTOR vPlayerCoord = GET_ENTITY_COORDS(eiPlayer)
		
	SWITCH INT_TO_ENUM(CINEMATIC_CAM_BEHAVIOUR_TYPE, sInst.iBehaviourType)
		CASE CCB_LERP
			IF NOT CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_LERP)
			AND NOT CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_ORBIT_COORD)
			AND NOT CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_ORBIT_ENTITY)
				CB_ADD_CAMERA_LERP(sInst.sCam, vPlayerCoord, <<0.0, 0.0, 0.0>>, vPlayerCoord + <<3.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 4000)
			ENDIF
		BREAK
		CASE CCB_SHAKE
			IF NOT CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_SHAKE)
				CB_ADD_CAMERA_SHAKE(sInst.sCam, 1.0, "HAND_SHAKE")
			ENDIF
		BREAK
		CASE CCB_ORBIT_COORD
			IF NOT CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_ORBIT_COORD)
			AND NOT CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_ORBIT_ENTITY)
			AND NOT CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_LERP)
				CB_ADD_ORBIT_COORD(sInst.sCam, vPlayerCoord, 5.0, 5.0, 5000, 1)
			ENDIF
		BREAK
		CASE CCB_ORBIT_ENTITY
			IF NOT CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_ORBIT_COORD)
			AND NOT CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_ORBIT_ENTITY)
			AND NOT CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_LERP)
				CB_ADD_ORBIT_ENTITY(sInst.sCam, eiPlayer, 5.0, 5.0, 5000, 1, 0.0, <<0.0, 0.0, 1.0>>)
			ENDIF
		BREAK
		CASE CCB_LOOK_AT_ENTITY
			IF NOT CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_LOOK_AT_ENTITY)
			AND NOT CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_LOOK_AT_COORD)
				CB_ADD_LOOK_AT_ENTITY(sInst.sCam, eiPlayer, <<0.0, 0.0, 0.0>>)
			ENDIF
		BREAK
		CASE CCB_LOOK_AT_COORD
			IF NOT CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_LOOK_AT_ENTITY)
			AND NOT CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_LOOK_AT_COORD)
				CB_ADD_LOOK_AT_COORD(sInst.sCam, vPlayerCoord)
			ENDIF
		BREAK
		CASE CCB_LERP_FOV
			IF NOT CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sInst.sCam, CCB_LERP_FOV)
				CB_ADD_FOV_LERP(sInst.sCam, 65, 30, 3000)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__REFRESH_CAMERA(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst, CINEMATIC_CAM_BEHAVIOUR_DEBUG &sDebugCam)
	IF DOES_WIDGET_GROUP_EXIST(sDebugCam.wgiRootID)
		DELETE_WIDGET_GROUP(sDebugCam.wgiRootID)
	ENDIF
	
	SET_CURRENT_WIDGET_GROUP(sInst.sDebug.wgiRootID)
	_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_CAMERA_WIDGET(sDebugCam, -1)
	CLEAR_CURRENT_WIDGET_GROUP(sInst.sDebug.wgiRootID)	
ENDPROC

DEBUGONLY PROC  _CINEMATIC_CAM_SEQUENCE_DEBUG__REFRESH_CAMERA_BEHAVIOUR_GROUP(CINEMATIC_CAM_BEHAVIOUR_DEBUG &sDebugCam)
	IF DOES_WIDGET_GROUP_EXIST(sDebugCam.wgiBehaviourGroupID)
		DELETE_WIDGET_GROUP(sDebugCam.wgiBehaviourGroupID)
	ENDIF
	
	SET_CURRENT_WIDGET_GROUP(sDebugCam.wgiRootID)	
	sDebugCam.wgiBehaviourGroupID = START_WIDGET_GROUP("Behaviours")
		_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_BEHAVIOUR_WIDGETS(sDebugCam)
	STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(sDebugCam.wgiRootID)
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__MAINTAIN_CAM_ACTIONS(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst, CINEMATIC_CAM_BEHAVIOUR_DEBUG &sDebugCam, INT iCamIndex)
	UNUSED_PARAMETER(sInst)
	
	IF sDebugCam.bPreview
		IF sInst.sDebug.iPreviewCam != iCamIndex
			IF sInst.sDebug.iPreviewCam != -1
				sInst.sDebug.sDebugCams[sInst.sDebug.iPreviewCam].bPreview = FALSE
			ENDIF
			
			sInst.sDebug.iPreviewCam = iCamIndex
		ENDIF
	ENDIF
	
	IF sDebugCam.bCopyStartTransform
		sDebugCam.bCopyStartTransform = FALSE
		sDebugCam.sCam.vStartPos = GET_FINAL_RENDERED_CAM_COORD()
		sDebugCam.sCam.vStartRot = GET_FINAL_RENDERED_CAM_ROT()
	ENDIF
	
	IF sDebugCam.bCopyEndTransform
		sDebugCam.bCopyEndTransform = FALSE
		sDebugCam.sCam.vEndPos = GET_FINAL_RENDERED_CAM_COORD()
		sDebugCam.sCam.vEndRot = GET_FINAL_RENDERED_CAM_ROT()
	ENDIF
	
	IF sDebugCam.bCopyBasicSettings
		sDebugCam.bCopyBasicSettings = FALSE
		sDebugCam.sCam.vStartPos = GET_FINAL_RENDERED_CAM_COORD()
		sDebugCam.sCam.vStartRot = GET_FINAL_RENDERED_CAM_ROT()
		sDebugCam.sCam.fStartFOV = GET_FINAL_RENDERED_CAM_FOV()
	ENDIF
	
	IF sDebugCam.bCopyStartFov
		sDebugCam.bCopyStartFov = FALSE
		sDebugCam.sCam.fStartFOV = GET_FINAL_RENDERED_CAM_FOV()
	ENDIF
	
	IF sDebugCam.bCopyEndFov
		sDebugCam.bCopyEndFov = FALSE
		sDebugCam.sCam.fEndFOV = GET_FINAL_RENDERED_CAM_FOV()
	ENDIF
	
	IF sDebugCam.bCopyLookatCoord
		sDebugCam.bCopyLookatCoord = FALSE
		sDebugCam.sCam.vLookAtCoord = GET_FINAL_RENDERED_CAM_COORD()
	ENDIF
	
	IF sDebugCam.bCopyOrbitOrigin
		sDebugCam.bCopyOrbitOrigin = FALSE
		sDebugCam.sCam.vOrbitCentre = GET_FINAL_RENDERED_CAM_COORD()
	ENDIF
	
	IF sDebugCam.bCopyOrbitDistance
		sDebugCam.bCopyOrbitDistance = FALSE
		BOOL bOrbitPlayer = CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sDebugCam.sCam, CCB_ORBIT_ENTITY)
		VECTOR vOrbitCoords = PICK_VECTOR(bOrbitPlayer, GET_ENTITY_COORDS(PLAYER_PED_ID()), sDebugCam.sCam.vOrbitCentre)
		VECTOR vDebugCamCoord = GET_FINAL_RENDERED_CAM_COORD()
		sDebugCam.sCam.vStartPos = vDebugCamCoord
		sDebugCam.sCam.vStartRot = GET_FINAL_RENDERED_CAM_ROT()
		sDebugCam.sCam.fDistanceFromCentre =  GET_DISTANCE_BETWEEN_COORDS(vOrbitCoords, vDebugCamCoord, FALSE)		
	ENDIF

	IF sDebugCam.bAddBehaviour
		sDebugCam.bAddBehaviour = FALSE
		_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_CAMS_CURRENT_SELECTED_BEHAVIOUR(sDebugCam)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__REFRESH_CAMERA_BEHAVIOUR_GROUP(sDebugCam)
		PRINTLN("[CINEMATIC_CAM_SEQUENCE][DEBUG] MAINTAIN_CAM_ACTIONS - Adding selected behaviour")
	ENDIF
		
	INT iBehaviour
	INT iMaxBehaviour = COUNT_OF(sDebugCam.bRemoveBehaviour)
	CINEMATIC_CAM_BEHAVIOUR_TYPE eBehaviour
	
	REPEAT iMaxBehaviour iBehaviour
		eBehaviour = INT_TO_ENUM(CINEMATIC_CAM_BEHAVIOUR_TYPE, iBehaviour)
		IF sDebugCam.bRemoveBehaviour[eBehaviour]
			CINEMATIC_CAM_BEHAVIOUR__DISABLE_BEHAVIOUR_TYPE(sDebugCam.sCam, eBehaviour)
			sDebugCam.bRemoveBehaviour[eBehaviour] = FALSE
			_CINEMATIC_CAM_SEQUENCE_DEBUG__REFRESH_CAMERA_BEHAVIOUR_GROUP(sDebugCam)
		ENDIF
	ENDREPEAT
	
ENDPROC

DEBUGONLY FUNC BOOL _CINEMATIC_CAM_SEQUENCE_DEBUG__CREATE_HELPER_MODEL(CINEMATIC_CAM_SEQUENCE_DEBUG &sDebug)
	
	IF DOES_ENTITY_EXIST(sDebug.objHelper)
		RETURN TRUE
	ENDIF
	
	MODEL_NAMES modelHelper = PROP_GOLF_BALL
	REQUEST_MODEL(modelHelper)
	
	IF NOT HAS_MODEL_LOADED(modelHelper)
		RETURN FALSE
	ENDIF

	sDebug.objHelper = CREATE_OBJECT_NO_OFFSET(modelHelper, GET_ENTITY_COORDS(PLAYER_PED_ID()))
	FREEZE_ENTITY_POSITION(sDebug.objHelper, TRUE)
	SET_ENTITY_COLLISION(sDebug.objHelper, FALSE)
	SET_ENTITY_VISIBLE(sDebug.objHelper, FALSE)
	RETURN TRUE
	
ENDFUNC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__DRAW_CAMERA(CINEMATIC_CAM_SEQUENCE_DEBUG &sDebug, VECTOR vPos, VECTOR vRot, INT r, INT g, INT b, INT a)
	CONST_FLOAT CAM_LENGTH	0.18
	CONST_FLOAT CAM_WIDTH	0.15
	CONST_FLOAT CAM_HEIGHT	0.15
	
	IF NOT _CINEMATIC_CAM_SEQUENCE_DEBUG__CREATE_HELPER_MODEL(sDebug)
		EXIT
	ENDIF
	
	SET_ENTITY_COORDS_NO_OFFSET(sDebug.objHelper, vPos)
	SET_ENTITY_ROTATION(sDebug.objHelper, vRot)
	
	VECTOR vFrontTopLeftCorner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sDebug.objHelper, <<-CAM_WIDTH, CAM_LENGTH, CAM_HEIGHT>>)
	VECTOR vFrontTopRightCorner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sDebug.objHelper, <<CAM_WIDTH, CAM_LENGTH, CAM_HEIGHT>>)
	VECTOR vFrontBottomLeftCorner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sDebug.objHelper, <<-CAM_WIDTH, CAM_LENGTH, -CAM_HEIGHT>>)
	VECTOR vFrontBottomRightCorner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sDebug.objHelper, <<CAM_WIDTH, CAM_LENGTH, -CAM_HEIGHT>>)
	VECTOR vBackTopLeftCorner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sDebug.objHelper, <<-CAM_WIDTH, -CAM_LENGTH, CAM_HEIGHT>>)
	VECTOR vBackTopRightCorner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sDebug.objHelper, <<CAM_WIDTH, -CAM_LENGTH, CAM_HEIGHT>>)
	VECTOR vBackBottomLeftCorner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sDebug.objHelper, <<-CAM_WIDTH, -CAM_LENGTH, -CAM_HEIGHT>>)
	VECTOR vBackBottomRightCorner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sDebug.objHelper, <<CAM_WIDTH, -CAM_LENGTH, -CAM_HEIGHT>>)
	VECTOR vConeCentre = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sDebug.objHelper, <<0.0, CAM_LENGTH, 0.0>>)
	VECTOR vConeTopLeft = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sDebug.objHelper, <<-CAM_WIDTH * 0.75, CAM_LENGTH * 2.0, CAM_HEIGHT * 0.75>>)
	VECTOR vConeTopRight = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sDebug.objHelper, <<CAM_WIDTH * 0.75, CAM_LENGTH * 2.0, CAM_HEIGHT * 0.75>>)
	VECTOR vConeBottomLeft = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sDebug.objHelper, <<-CAM_WIDTH * 0.75, CAM_LENGTH * 2.0, -CAM_HEIGHT * 0.75>>)
	VECTOR vConeBottomRight = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sDebug.objHelper, <<CAM_WIDTH * 0.75, CAM_LENGTH * 2.0, -CAM_HEIGHT * 0.75>>)
	
	DRAW_DEBUG_LINE(vFrontTopLeftCorner, vFrontTopRightCorner, r, g, b, a)
	DRAW_DEBUG_LINE(vFrontTopRightCorner, vFrontBottomRightCorner, r, g, b, a)
	DRAW_DEBUG_LINE(vFrontBottomRightCorner, vFrontBottomLeftCorner, r, g, b, a)
	DRAW_DEBUG_LINE(vFrontBottomLeftCorner, vFrontTopLeftCorner, r, g, b, a)
	
	DRAW_DEBUG_LINE(vBackTopLeftCorner, vBackTopRightCorner, r, g, b, a)
	DRAW_DEBUG_LINE(vBackTopRightCorner, vBackBottomRightCorner, r, g, b, a)
	DRAW_DEBUG_LINE(vBackBottomRightCorner, vBackBottomLeftCorner, r, g, b, a)
	DRAW_DEBUG_LINE(vBackBottomLeftCorner, vBackTopLeftCorner, r, g, b, a)
	
	DRAW_DEBUG_LINE(vFrontTopLeftCorner, vBackTopLeftCorner, r, g, b, a)
	DRAW_DEBUG_LINE(vFrontTopRightCorner, vBackTopRightCorner, r, g, b, a)
	DRAW_DEBUG_LINE(vFrontBottomLeftCorner, vBackBottomLeftCorner, r, g, b, a)
	DRAW_DEBUG_LINE(vFrontBottomRightCorner, vBackBottomRightCorner, r, g, b, a)
	
	DRAW_DEBUG_LINE(vConeCentre, vConeTopLeft, r, g, b, a)
	DRAW_DEBUG_LINE(vConeCentre, vConeTopRight, r, g, b, a)
	DRAW_DEBUG_LINE(vConeCentre, vConeBottomLeft, r, g, b, a)
	DRAW_DEBUG_LINE(vConeCentre, vConeBottomRight, r, g, b, a)
	
	DRAW_DEBUG_LINE(vConeTopLeft, vConeTopRight, r, g, b, a)
	DRAW_DEBUG_LINE(vConeTopRight, vConeBottomRight, r, g, b, a)
	DRAW_DEBUG_LINE(vConeBottomRight, vConeBottomLeft, r, g, b, a)

ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__DRAW_CAM(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst, CINEMATIC_CAM_BEHAVIOUR_DEBUG &sDebugCam)
	
	IF NOT IS_CAM_RENDERING(GET_DEBUG_CAM())
		EXIT
	ENDIF
	
	IF NOT sInst.sDebug.bEdit OR NOT sDebugCam.bPreview
		EXIT
	ENDIF
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	CONST_FLOAT TEXT_OFFSET 0.35

	// Always draw start position
	DRAW_DEBUG_TEXT_ABOVE_COORDS(sDebugCam.sCam.vStartPos, "Start", TEXT_OFFSET, 0, 255, 0, 255)
	_CINEMATIC_CAM_SEQUENCE_DEBUG__DRAW_CAMERA(sInst.sDebug, sDebugCam.sCam.vStartPos, sDebugCam.sCam.vStartRot, 0, 255, 0, 255)
	
	IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sDebugCam.sCam, CCB_LERP)
		DRAW_DEBUG_TEXT_ABOVE_COORDS(sDebugCam.sCam.vEndPos, "End", TEXT_OFFSET, 0, 0, 255, 255)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__DRAW_CAMERA(sInst.sDebug, sDebugCam.sCam.vEndPos, sDebugCam.sCam.vEndRot, 0, 0, 255, 255)
		DRAW_DEBUG_LINE_WITH_TWO_COLOURS(sDebugCam.sCam.vStartPos, sDebugCam.sCam.vEndPos, 0, 255, 0, 255, 0, 0, 255, 255)
	ENDIF
	
	IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sDebugCam.sCam, CCB_ORBIT_COORD)	
		DRAW_DEBUG_TEXT_ABOVE_COORDS(sDebugCam.sCam.vOrbitCentre, "Orbit Origin", TEXT_OFFSET, 255, 0, 255)
		DRAW_DEBUG_CROSS(sDebugCam.sCam.vOrbitCentre, 1.0, 0, 255, 0, 255)
		DRAW_DEBUG_CIRCLE(sDebugCam.sCam.vOrbitCentre, sDebugCam.sCam.fDistanceFromCentre, 0, 0, 255)
	ENDIF
	
	IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sDebugCam.sCam, CCB_ORBIT_ENTITY)	
		VECTOR vOrbitTargetCoords = GET_ENTITY_COORDS(sDebugCam.sCam.eiOrbitTarget)
		VECTOR vOffsetOrbit = vOrbitTargetCoords + sDEbugCam.sCam.vOrbitOffset
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(sDebugCam.sCam.eiOrbitTarget, "Orbit Target", TEXT_OFFSET, 255, 0, 255)
		DRAW_DEBUG_CROSS(vOrbitTargetCoords, 1.0, 0, 255, 0, 255)
		
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(sDebugCam.sCam.eiOrbitTarget, "Orbit Offset", TEXT_OFFSET, 255, 255, 0)
		DRAW_DEBUG_CROSS(vOffsetOrbit, 1.0, 255, 255, 0)
		DRAW_DEBUG_LINE(vOffsetOrbit, vOrbitTargetCoords, 255, 255)
		DRAW_DEBUG_CIRCLE(vOffsetOrbit, sDebugCam.sCam.fDistanceFromCentre, 0, 0, 255)
	ENDIF
	
	IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sDebugCam.sCam, CCB_LOOK_AT_COORD)
		DRAW_DEBUG_CROSS(sDebugCam.sCam.vLookAtCoord, 1.0, 0, 0, 255, 255)
		DRAW_DEBUG_LINE(GET_CAM_COORD(sInst.ciCamera), sDebugCam.sCam.vLookAtCoord, 0, 0, 255, 255)
		DRAW_DEBUG_TEXT_ABOVE_COORDS(sDebugCam.sCam.vLookAtCoord, "Look At Coord", TEXT_OFFSET, 0, 0, 255, 255)
	ENDIF
	
	IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sDebugCam.sCam, CCB_LOOK_AT_ENTITY)
		VECTOR vLookAtTargetCoords = GET_ENTITY_COORDS(sDEbugCAm.sCam.eiLookAtTarget)
		VECTOR vFinalLookTarget = vLookAtTargetCoords + sDebugCam.sCam.vLookAtOffset
		DRAW_DEBUG_CROSS(vFinalLookTarget, 1.0, 0, 0, 255, 255)
		DRAW_DEBUG_LINE(GET_CAM_COORD(sInst.ciCamera), vFinalLookTarget, 0, 0, 255, 255)
		DRAW_DEBUG_TEXT_ABOVE_COORDS(vFinalLookTarget, "Look At Target", TEXT_OFFSET, 0, 0, 255, 255)
	ENDIF	
	
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__DRAW_CURRENT_CAM(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	IF sInst.sDebug.bEdit AND IS_CAM_RENDERING(GET_DEBUG_CAM())
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__DRAW_CAMERA(sInst.sDebug, GET_CAM_COORD(sInst.ciCamera), GET_CAM_ROT(sInst.ciCamera), 255, 0, 0, 255)
	ENDIF
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__REMOVE_CAMERA(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst, INT iCamToRemove)
	sInst.sDebug.sDebugCams[iCamToRemove].bRemoveCam = FALSE
	
	IF DOES_WIDGET_GROUP_EXIST(sInst.sDebug.sDebugCams[iCamToRemove].wgiRootID)
		DELETE_WIDGET_GROUP(sInst.sDebug.sDebugCams[iCamToRemove].wgiRootID)
	ENDIF
	
	CINEMATIC_CAM_BEHAVIOUR_DEBUG sEmpty
	sInst.sDebug.sDebugCams[iCamToRemove] = sEmpty

	sInst.sDebug.iCamCount = IMAX(0, sInst.sDebug.iCamCount - 1)
	PRINTLN("[CINEMATIC_CAM_SEQUENCE][DEBUG] - REMOVE_CAMERA iCamCount: ", sInst.sDebug.iCamCount)
	
	// If we were previewing the removed cam stop preview
	IF sInst.sDebug.iPreviewCam = iCamToRemove
		sInst.sDebug.iPreviewCam = -1
	ENDIF
	
	CINEMATIC_CAM_SEQUENCE__REFRESH_CAM_WEIGHTINGS(sInst)
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__MAINTAIN_ALL_CAM_ACTIONS(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)	
	INT iMaxCam = COUNT_OF(sInst.sDebug.sDebugCams)
	
	IF iMaxCam <= 0
		EXIT
	ENDIF
	
	_CINEMATIC_CAM_SEQUENCE_DEBUG__DRAW_CURRENT_CAM(sInst)
	
	INT iCam
	BOOL bPreviewing
	INT iRemoveCam = -1
	
	REPEAT iMaxCam iCam
		IF NOT sInst.sDebug.sDebugCams[iCam].bAdded
			RELOOP
		ENDIF
	
		_CINEMATIC_CAM_SEQUENCE_DEBUG__MAINTAIN_CAM_ACTIONS(sInst, sInst.sDebug.sDebugCams[iCam], iCam)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__DRAW_CAM(sInst, sInst.sDebug.sDebugCams[iCam])
		
		IF sInst.sDebug.sDebugCams[iCam].bRemoveCam AND iRemoveCam = -1
			iRemoveCam = iCam
		ENDIF
		
		IF sInst.sDebug.sDebugCams[iCam].bPreview
			bPreviewing = TRUE
		ENDIF
	ENDREPEAT
	
	IF iRemoveCam != -1
		_CINEMATIC_CAM_SEQUENCE_DEBUG__REMOVE_CAMERA(sInst, iRemoveCam)
	ENDIF
	
	IF NOT bPreviewing
		sInst.sDebug.iPreviewCam = -1
	ENDIF
	
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_CAMERA_TO_CAMERA_GROUP(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst, INT iCamIndex, INT iCamID)
	SET_CURRENT_WIDGET_GROUP(sInst.sDebug.wgiCamGroupID)
	_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_CAMERA_WIDGET(sInst.sDebug.sDebugCams[iCamIndex], iCamID)
	CLEAR_CURRENT_WIDGET_GROUP(sInst.sDebug.wgiCamGroupID)
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_CAMERA(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	IF sInst.sDebug.iCamCount >= COUNT_OF(sInst.sDebug.sDebugCams)
		PRINTLN("[CINEMATIC_CAM_SEQUENCE][DEBUG] ADD_CAMERA - Exceeded camera count not adding")
		EXIT
	ENDIF
	
	VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	INT i
	INT iMaxCam = COUNT_OF(sInst.sDebug.sDebugCams)
	INT iNextFreeCamera
	
	REPEAT iMaxCam i
		IF NOT sInst.sDebug.sDebugCams[i].bAdded
			iNextFreeCamera = i
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	CINEMATIC_CAM_BEHAVIOUR__INIT(sInst.sDebug.sDebugCams[iNextFreeCamera].sCam, vPlayerCoord, <<0.0, 0.0, 0.0>>, 65, 5000)	
	_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_CAMERA_TO_CAMERA_GROUP(sInst, iNextFreeCamera, sInst.sDebug.iNextID)
	sInst.sDebug.iNextID++
	sInst.sDebug.iCamCount++
	CINEMATIC_CAM_SEQUENCE__REFRESH_CAM_WEIGHTINGS(sInst)
	PRINTLN("[CINEMATIC_CAM_SEQUENCE][DEBUG] ADD_CAMERA - Adding camera")
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__REFRESH_CAM_WIDGET_LIST(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst) 
	INT iMaxCam = sInst.sDebug.iCamCount
	INT iCam
	
	REPEAT iMaxCam iCam
		IF DOES_WIDGET_GROUP_EXIST(sInst.sDebug.sDebugCams[iCam].wgiRootID)
			DELETE_WIDGET_GROUP(sInst.sDebug.sDebugCams[iCam].wgiRootID)
		ENDIF
		
		_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_CAMERA_TO_CAMERA_GROUP(sInst, iCam, sInst.sDebug.iNextID)
		sInst.sDebug.iNextID++
	ENDREPEAT
	
ENDPROC

DEBUGONLY PROC CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_WIDGETS(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst, STRING strTitle = NULL)
	_CINEMATIC_CAM_SEQUENCE_DEBUG__COPY_CAM_LOOKUP_TO_DEBUG_CAMS(sInst)
	_CINEMATIC_CAM_SEQUENCE_DEBUG__CREATE_WIDGETS(sInst, strTitle)
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__RESET_TO_SCRIPT_CAMS(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	INT iMaxCam = sInst.iMaxCam
	
	IF iMaxCam <= 0
		EXIT
	ENDIF
	
	INT iCam
	sInst.sDebug.iNextID = 0
	sInst.sDebug.iCamCount = iMaxCam
	CINEMATIC_CAM_BEHAVIOUR_DEBUG sEmpty
	
	REPEAT iMaxCam iCam
		IF sInst.fpCamLookup != NULL
			IF DOES_WIDGET_GROUP_EXIST(sInst.sDebug.sDebugCams[iCam].wgiRootID)
				DELETE_WIDGET_GROUP(sInst.sDebug.sDebugCams[iCam].wgiRootID)
			ENDIF
			
			sInst.sDebug.sDebugCams[iCam] = sEmpty
			sInst.sDebug.sDebugCams[iCam].sCam = CALL sInst.fpCamLookup(iCam)
			_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_CAMERA_TO_CAMERA_GROUP(sInst, iCam, sInst.sDebug.iNextID)
			sInst.sDebug.iNextID++
		ENDIF
	ENDREPEAT
	
	CINEMATIC_CAM_SEQUENCE__REFRESH_CAM_WEIGHTINGS(sInst)
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(STRING strPath, STRING strFileName, TEXT_LABEL_63 tlIndent)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlIndent, strPath, strFileName)
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__SAVE_VECTOR_AS_ATTRIBUTES(STRING strVectorName, VECTOR vVector, STRING strPath, STRING strFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("<", strPath, strFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(strVectorName, strPath, strFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(" x = \"", strPath, strFileName)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(vVector.x, strPath, strFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("\" y = \"", strPath, strFileName)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(vVector.y, strPath, strFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("\" z = \"", strPath, strFileName)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(vVector.z, strPath, strFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__SAVE_CAM_TO_XML(CINEMATIC_CAM_BEHAVIOUR_DEBUG &sDebugCam, STRING strPath, STRING strFileName, STRING strIndent)
	TEXT_LABEL_63 tlIndent = strIndent
	TEXT_LABEL_63 tlStartIndent = tlIndent
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(strIndent, strPath, strFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("<cam name = \"", strPath, strFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(sDebugCam.strName, strPath, strFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("\">", strPath, strFileName)
		tlIndent += "	"	
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<iCamBehaviourBS value = \"", strPath, strFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(sDebugCam.sCam.iCamBehaviourBS, strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<fWeighting value = \"", strPath, strFileName)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(sDebugCam.sCam.fWeighting, strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		_CINEMATIC_CAM_SEQUENCE_DEBUG__SAVE_VECTOR_AS_ATTRIBUTES("vStartPos", sDebugCam.sCam.vStartPos, strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		_CINEMATIC_CAM_SEQUENCE_DEBUG__SAVE_VECTOR_AS_ATTRIBUTES("vStartRot", sDebugCam.sCam.vStartRot, strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<fStartFOV value = \"", strPath, strFileName)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(sDebugCam.sCam.fStartFOV, strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		_CINEMATIC_CAM_SEQUENCE_DEBUG__SAVE_VECTOR_AS_ATTRIBUTES("vEndPos", sDebugCam.sCam.vEndPos, strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		_CINEMATIC_CAM_SEQUENCE_DEBUG__SAVE_VECTOR_AS_ATTRIBUTES("vEndRot", sDebugCam.sCam.vEndRot, strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<iLerpDuration value = \"", strPath, strFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(sDebugCam.sCam.iLerpDuration, strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<iLerpDelay value = \"", strPath, strFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(sDebugCam.sCam.iLerpDelay, strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<eLerpGraph value = \"", strPath, strFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(ENUM_TO_INT(sDebugCam.sCam.eLerpGraph), strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<iFOVLerpDurationMS value = \"", strPath, strFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(sDebugCam.sCam.iFOVLerpDurationMS, strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<fEndFOV value = \"", strPath, strFileName)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(sDebugCam.sCam.fEndFOV, strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<iCameraDuration value = \"", strPath, strFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(sDebugCam.sCam.iCameraDuration, strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<eiLookAtTarget value = \"", strPath, strFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(BOOL_TO_INT(sDebugCam.sCam.eiLookAtTarget != NULL), strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		_CINEMATIC_CAM_SEQUENCE_DEBUG__SAVE_VECTOR_AS_ATTRIBUTES("vLookAtOffset", sDebugCam.sCam.vLookAtOffset, strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		_CINEMATIC_CAM_SEQUENCE_DEBUG__SAVE_VECTOR_AS_ATTRIBUTES("vLookAtCoord", sDebugCam.sCam.vLookAtCoord, strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<strCamShakeType value = \"", strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(sDebugCam.sCam.strCamShakeType, strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<fCamShakeIntensity value = \"", strPath, strFileName)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(sDebugCam.sCam.fCamShakeIntensity, strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		_CINEMATIC_CAM_SEQUENCE_DEBUG__SAVE_VECTOR_AS_ATTRIBUTES("vOrbitCentre", sDebugCam.sCam.vOrbitCentre, strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		_CINEMATIC_CAM_SEQUENCE_DEBUG__SAVE_VECTOR_AS_ATTRIBUTES("vOrbitOffsets", sDebugCam.sCam.vOrbitOffset, strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<eiOrbitTarget value = \"", strPath, strFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(BOOL_TO_INT(sDebugCam.sCam.eiOrbitTarget != NULL), strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<fOrbitSpeed value = \"", strPath, strFileName)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(sDebugCam.sCam.fOrbitSpeed, strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<fDistanceFromCentre value = \"", strPath, strFileName)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(sDebugCam.sCam.fDistanceFromCentre, strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<fStartTheta value = \"", strPath, strFileName)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(sDebugCam.sCam.fStartTheta, strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
		_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlIndent)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<iOrbitDirection value = \"", strPath, strFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(sDebugCam.sCam.iOrbitDirection, strPath, strFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", strPath, strFileName)
		
	_CINEMATIC_CAM_SEQUENCE_DEBUG__XML_NEXT_LINE(strPath, strFileName, tlStartIndent)		
	SAVE_STRING_TO_NAMED_DEBUG_FILE("</cam>", strPath, strFileName)	
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__SAVE_CAM_SEQUENCE_TO_XML(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)    
	//// FORMAT
	////<camera_sequence>
	////	<cameras>
	////		<cam = "CAM 0">
	////			<iCamBehaviourBS> 3 </iCamBehaviourBS> 
	///     		<fWeighting> 100.0 </fWeighting>
	////			<vStartPos x = "1549.0707" y = "250.0233" z = "-46.2809" />
	////			<vStartRot x = "-14.3047" y = "0.0000" z = "-92.4111" />
	////			<fStartFOV> 65.0000 </fStartFOV> 
	////			<vEndPos x = "1560.5397" y = "249.5688" z = "-46.3712" />
	////			<vEndRot x = "-28.1636" y = "0.0000" z = "-92.4111" />
	////			<iLerpDuration> 3800 </iLerpDuration> 
	////			<iLerpDelay> -1 </iLerpDelay> 
	////			<eLerpGraph> 0 </eLerpGraph> 
	////			<iFOVLerpDurationMS> 0 </iFOVLerpDurationMS> 
	////			<fEndFOV> 65.0000 </fEndFOV> 
	////			<iCameraDuration> -1 </iCameraDuration> 
	////			<lookAtPlayer> FALSE </lookAtPlayer> 
	////			<vLookAtOffset x = "0.0000" y = "0.0000" z = "0.0000" />
	////			<vLookAtCoord x = "0.0000" y = "0.0000" z = "0.0000" />
	////			<strCamShakeType> HAND_SHAKE </strCamShakeType> 
	////			<fCamShakeIntensity> 1.0000 </fCamShakeIntensity> 
	////			<vOrbitCentre x = "0.0000" y = "0.0000" z = "0.0000" />
	////			<vOrbitOffsets x = "0.0000" y = "0.0000" z = "0.0000" />
	////			<orbitPlayer> FALSE </orbitPlayer> 
	////			<fOrbitSpeed> 0.0000 </fOrbitSpeed> 
	////			<fDistanceFromCentre> 0.0000 </fDistanceFromCentre> 
	////			<fStartTheta> 0.0000 </fStartTheta> 
	////			<iOrbitDirection> 0 </iOrbitDirection> 
	////		</cam>
	////	</cameras>
	////</camera_sequence>
	
	STRING strPath =  "X:\\"
	TEXT_LABEL_63 strFile = GET_CONTENTS_OF_TEXT_WIDGET(sInst.sDebug.twiXMLSaveName)
	
	
	IF IS_STRING_NULL_OR_EMPTY(strPath)
		PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] SAVE_CAM_SEQUENCE_TO_XML - No save name set!")
		PRINT_DEBUG_TICKER("Cam Sequence XML Save FAILED! You must set a save name")
		EXIT
	ENDIF
	
	strFile += ".xml"
	
	STRING strIndent = ""
	CLEAR_NAMED_DEBUG_FILE(strPath, strFile)
	OPEN_NAMED_DEBUG_FILE(strPath, strFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<camera_sequence>", strPath, strFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
			strIndent = "	"
			SAVE_STRING_TO_NAMED_DEBUG_FILE(strIndent, strPath, strFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("<cameras>", strPath, strFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
				strIndent = "		"
				
				INT iMaxCam = COUNT_OF(sInst.sDebug.sDebugCams)
				INT iCam
				
				REPEAT iMaxCam iCam
					IF sInst.sDebug.sDebugCams[iCam].bAdded
						_CINEMATIC_CAM_SEQUENCE_DEBUG__SAVE_CAM_TO_XML(sInst.sDebug.sDebugCams[iCam], strPath, strFile, strIndent)
						SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
					ENDIF
				ENDREPEAT
			
			strIndent = "	"
			SAVE_STRING_TO_NAMED_DEBUG_FILE(strIndent, strPath, strFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("</cameras>", strPath, strFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("</camera_sequence>", strPath, strFile)
	CLOSE_DEBUG_FILE()

	PRINT_DEBUG_TICKER("Cam Sequence XML Saved to X:\\")
ENDPROC

DEBUGONLY FUNC VECTOR _CINEMATIC_CAM_SEQUENCE_DEBUG__LOAD_VECTOR_FROM_XML_ATTRIBUTES()
	INT iAttribute
	INT iAttributeNameHash
	VECTOR vResult
	
	REPEAT 3 iAttribute
		iAttributeNameHash = GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(iAttribute))
		SWITCH iAttributeNameHash
			CASE HASH("x") vResult.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
			CASE HASH("y") vResult.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
			CASE HASH("z") vResult.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		ENDSWITCH
	ENDREPEAT
	
	RETURN vResult
ENDFUNC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__CLEAR_DEBUG_CAMS(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	INT iCam
	INT iMaxCam = COUNT_OF(sInst.sDebug.sDebugCams)
	CINEMATIC_CAM_BEHAVIOUR_DEBUG sEmpty
	
	REPEAT iMaxCam iCam
		IF DOES_WIDGET_GROUP_EXIST(sInst.sDebug.sDebugCams[iCam].wgiRootID)
			DELETE_WIDGET_GROUP(sInst.sDebug.sDebugCams[iCam].wgiRootID)
		ENDIF
		
		sInst.sDebug.sDebugCams[iCam] = sEmpty
	ENDREPEAT
	
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__LOAD_CAM_SEQUENCE_FROM_XML(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)  
		
	TEXT_LABEL_63 tlFileName = GET_CONTENTS_OF_TEXT_WIDGET(sInst.sDebug.twiXMLLoadName)
	
	IF IS_STRING_NULL_OR_EMPTY(tlFileName)
		PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - No load path set!")
		PRINT_HELP_WITH_COLOURED_LITERAL_STRING("STRING", "Cam Sequence XML Load FAILED! You must set a load path", HUD_COLOUR_RED, DEFAULT_HELP_TEXT_TIME, TRUE)
		EXIT
	ENDIF
	
	TEXT_LABEL_63 strPath =  "X:\\"
	strPath = "X:\\"
	strPath += tlFileName
	strPath += ".xml"
	
	IF NOT LOAD_XML_FILE(strPath)
		PRINT_HELP_WITH_COLOURED_LITERAL_STRING("STRING", "Cam Sequence XML Load FAILED! File not found", HUD_COLOUR_RED, DEFAULT_HELP_TEXT_TIME, TRUE)
		EXIT
	ENDIF
	
	PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - File loaded")
	
	INT iNodeCount = GET_NUMBER_OF_XML_NODES()
	
	IF iNodeCount <= 0
		PRINT_HELP_WITH_COLOURED_LITERAL_STRING("STRING", "Cam Sequence XML Load FAILED! File corrupted", HUD_COLOUR_RED, DEFAULT_HELP_TEXT_TIME, TRUE)
		PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - No Nodes")
		EXIT
	ENDIF
	
	_CINEMATIC_CAM_SEQUENCE_DEBUG__CLEAR_DEBUG_CAMS(sInst)
	PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Cleared cameras")
	
	INT iNode
	INT iNodeNameHash
	INT iCamCount = -1
	STRING strNodeName
	REPEAT iNodeCount iNode
		strNodeName = GET_XML_NODE_NAME()
		iNodeNameHash = GET_HASH_KEY(strNodeName)
		PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Current Node: ", strNodeName)	
		
		SWITCH iNodeNameHash
			CASE HASH("cam") 
				iCamCount++
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Adding Cam: ", iCamCount, " ----------------------------------")
				sInst.sDebug.sDebugCams[iCamCount].strName = GET_STRING_FROM_XML_NODE_ATTRIBUTE(0)
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded strName: ", sInst.sDebug.sDebugCams[iCamCount].strName)
			BREAK
			CASE HASH("iCamBehaviourBS")
				sInst.sDebug.sDebugCams[iCamCount].sCam.iCamBehaviourBS = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded iCamBehaviourBS: ", sInst.sDebug.sDebugCams[iCamCount].sCam.iCamBehaviourBS)
			BREAK
			CASE HASH("fWeighting")
				sInst.sDebug.sDebugCams[iCamCount].sCam.fWeighting = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(0)
			BREAK
			CASE HASH("vStartPos")
				sInst.sDebug.sDebugCams[iCamCount].sCam.vStartPos = _CINEMATIC_CAM_SEQUENCE_DEBUG__LOAD_VECTOR_FROM_XML_ATTRIBUTES()
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded vStartPos: ", sInst.sDebug.sDebugCams[iCamCount].sCam.vStartPos)
			BREAK
			CASE HASH("vStartRot")
				sInst.sDebug.sDebugCams[iCamCount].sCam.vStartRot = _CINEMATIC_CAM_SEQUENCE_DEBUG__LOAD_VECTOR_FROM_XML_ATTRIBUTES()
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded vStartRot: ", sInst.sDebug.sDebugCams[iCamCount].sCam.vStartRot)
			BREAK
			CASE HASH("fStartFOV")
				sInst.sDebug.sDebugCams[iCamCount].sCam.fStartFOV = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(0)
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded fStartFOV: ", sInst.sDebug.sDebugCams[iCamCount].sCam.fStartFOV)
			BREAK
			CASE HASH("vEndPos")
				sInst.sDebug.sDebugCams[iCamCount].sCam.vEndPos = _CINEMATIC_CAM_SEQUENCE_DEBUG__LOAD_VECTOR_FROM_XML_ATTRIBUTES()
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded vEndPos: ", sInst.sDebug.sDebugCams[iCamCount].sCam.vEndPos)
			BREAK
			CASE HASH("vEndRot")
				sInst.sDebug.sDebugCams[iCamCount].sCam.vEndRot = _CINEMATIC_CAM_SEQUENCE_DEBUG__LOAD_VECTOR_FROM_XML_ATTRIBUTES()
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded vEndRot: ", sInst.sDebug.sDebugCams[iCamCount].sCam.vEndRot )
			BREAK
			CASE HASH("iLerpDuration")
				sInst.sDebug.sDebugCams[iCamCount].sCam.iLerpDuration = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded iLerpDuration: ", sInst.sDebug.sDebugCams[iCamCount].sCam.iLerpDuration)
			BREAK
			CASE HASH("iLerpDelay")
				sInst.sDebug.sDebugCams[iCamCount].sCam.iLerpDelay = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded iLerpDelay: ", sInst.sDebug.sDebugCams[iCamCount].sCam.iLerpDelay)
			BREAK
			CASE HASH("eLerpGraph")
				sInst.sDebug.sDebugCams[iCamCount].sCam.eLerpGraph = INT_TO_ENUM(CAMERA_GRAPH_TYPE, GET_INT_FROM_XML_NODE_ATTRIBUTE(0))
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded eLerpGraph: ", sInst.sDebug.sDebugCams[iCamCount].sCam.eLerpGraph)
			BREAK
			CASE HASH("iFOVLerpDurationMS")
				sInst.sDebug.sDebugCams[iCamCount].sCam.iFOVLerpDurationMS = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded iFOVLerpDurationMS: ", sInst.sDebug.sDebugCams[iCamCount].sCam.iFOVLerpDurationMS )
			BREAK
			CASE HASH("fEndFOV")
				sInst.sDebug.sDebugCams[iCamCount].sCam.fEndFOV = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(0)
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded fEndFOV: ", sInst.sDebug.sDebugCams[iCamCount].sCam.fEndFOV)
			BREAK
			CASE HASH("iCameraDuration")
				sInst.sDebug.sDebugCams[iCamCount].sCam.iCameraDuration = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded iCameraDuration: ", sInst.sDebug.sDebugCams[iCamCount].sCam.iCameraDuration)
			BREAK
			CASE HASH("eiLookAtTarget")
				IF INT_TO_BOOL(GET_INT_FROM_XML_NODE_ATTRIBUTE(0))
					sInst.sDebug.sDebugCams[iCamCount].sCam.eiLookAtTarget = PLAYER_PED_ID()
				ENDIF			
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded eiLookAtTarget: ", sInst.sDebug.sDebugCams[iCamCount].sCam.eiLookAtTarget != NULL)
			BREAK
			CASE HASH("vLookAtOffset")
				sInst.sDebug.sDebugCams[iCamCount].sCam.vLookAtOffset = _CINEMATIC_CAM_SEQUENCE_DEBUG__LOAD_VECTOR_FROM_XML_ATTRIBUTES()
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded vLookAtOffset: ", sInst.sDebug.sDebugCams[iCamCount].sCam.vLookAtOffset)
			BREAK
			CASE HASH("vLookAtCoord")
				sInst.sDebug.sDebugCams[iCamCount].sCam.vLookAtCoord = _CINEMATIC_CAM_SEQUENCE_DEBUG__LOAD_VECTOR_FROM_XML_ATTRIBUTES()
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded vLookAtCoord: ", sInst.sDebug.sDebugCams[iCamCount].sCam.vLookAtCoord)
			BREAK
			CASE HASH("strCamShakeType")
				sInst.sDebug.sDebugCams[iCamCount].sCam.strCamShakeType = GET_STRING_FROM_XML_NODE_ATTRIBUTE(0)
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded strCamShakeType: ", sInst.sDebug.sDebugCams[iCamCount].sCam.strCamShakeType)
			BREAK
			CASE HASH("fCamShakeIntensity")
				sInst.sDebug.sDebugCams[iCamCount].sCam.fCamShakeIntensity = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(0)
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded fCamShakeIntensity: ", sInst.sDebug.sDebugCams[iCamCount].sCam.fCamShakeIntensity)
			BREAK
			CASE HASH("vOrbitCentre")
				sInst.sDebug.sDebugCams[iCamCount].sCam.vOrbitCentre = _CINEMATIC_CAM_SEQUENCE_DEBUG__LOAD_VECTOR_FROM_XML_ATTRIBUTES()
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded vOrbitCentre: ", sInst.sDebug.sDebugCams[iCamCount].sCam.vOrbitCentre)
			BREAK
			CASE HASH("vOrbitOffset")
				sInst.sDebug.sDebugCams[iCamCount].sCam.vOrbitOffset = _CINEMATIC_CAM_SEQUENCE_DEBUG__LOAD_VECTOR_FROM_XML_ATTRIBUTES()
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded vOrbitOffset: ", sInst.sDebug.sDebugCams[iCamCount].sCam.vOrbitOffset)
			BREAK
			CASE HASH("eiOrbitTarget")
				IF INT_TO_BOOL(GET_INT_FROM_XML_NODE_ATTRIBUTE(0))
					sInst.sDebug.sDebugCams[iCamCount].sCam.eiOrbitTarget = PLAYER_PED_ID()
				ENDIF
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded eiOrbitTarget: ", sInst.sDebug.sDebugCams[iCamCount].sCam.eiOrbitTarget != NULL)
			BREAK
			CASE HASH("fOrbitSpeed")
				sInst.sDebug.sDebugCams[iCamCount].sCam.fOrbitSpeed = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(0)
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded fOrbitSpeed: ", sInst.sDebug.sDebugCams[iCamCount].sCam.fOrbitSpeed )
			BREAK
			CASE HASH("fDistanceFromCentre")
				sInst.sDebug.sDebugCams[iCamCount].sCam.fDistanceFromCentre = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(0)
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded fDistanceFromCentre: ", sInst.sDebug.sDebugCams[iCamCount].sCam.fDistanceFromCentre )
			BREAK
			CASE HASH("fStartTheta")
				sInst.sDebug.sDebugCams[iCamCount].sCam.fStartTheta = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(0)
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded fStartTheta: ", sInst.sDebug.sDebugCams[iCamCount].sCam.fStartTheta)
			BREAK
			CASE HASH("iOrbitDirection")
				sInst.sDebug.sDebugCams[iCamCount].sCam.iOrbitDirection = GET_INT_FROM_XML_NODE_ATTRIBUTE(0)
				PRINTLN("[CINEMATIC_CAM_SEQUENCE_DEBUG] LOAD_CAM_SEQUENCE_FROM_XML - Loaded iOrbitDirection: ", sInst.sDebug.sDebugCams[iCamCount].sCam.iOrbitDirection)
			BREAK
		ENDSWITCH
		
		GET_NEXT_XML_NODE()		
	ENDREPEAT	
	
	DELETE_XML_FILE()
	sInst.sDebug.iCamCount = iCamCount
	sInst.sDebug.iNextID = 0
	CINEMATIC_CAM_SEQUENCE__REFRESH_CAM_WEIGHTINGS(sInst)
	_CINEMATIC_CAM_SEQUENCE_DEBUG__REFRESH_CAM_WIDGET_LIST(sInst)
	
ENDPROC

DEBUGONLY FUNC TEXT_LABEL _CINEMATIC_CAM_SEQUENCE__GET_ENUM_FOR_DEBUG_CAM(CINEMATIC_CAM_BEHAVIOUR_DEBUG &sDebugCam)
	TEXT_LABEL tlEnum
	tlEnum += "[PREFIX]_"
	tlEnum += sDebugCam.strName
	return tlEnum
ENDFUNC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__OUTPUT_SCRIPT_LOOKUP(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	TEXT_LABEL tlIndent
	INT iMaxCams = sInst.sDebug.iCamCount
	
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("--------------------------- CAM SEQUENCE EDITOR OUTPUT -----------------------------")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	SAVE_STRING_TO_DEBUG_FILE("ENUM [PREFIX]_CAMERAS")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	tlIndent = "	"
	SAVE_STRING_TO_DEBUG_FILE(tlIndent)
	
	
	
	INT iEnum
	TEXT_LABEL_63 tlEnum
	CINEMATIC_CAM_BEHAVIOUR_DEBUG sDebugCam
	REPEAT iMaxCams iEnum
		// Make sure names are correct
		sInst.sDebug.sDebugCams[iEnum].strName = "CAM_"
		sInst.sDebug.sDebugCams[iEnum].strName += iEnum
	
		sDebugCam = sInst.sDebug.sDebugCams[iEnum]
		tlEnum = ""
		
		IF iEnum > 0
		tlEnum += ","
		ENDIF
		TEXT_LABEL_63 tlEnumName = _CINEMATIC_CAM_SEQUENCE__GET_ENUM_FOR_DEBUG_CAM(sDebugCam)
		tlEnum += tlEnumName
	
		SAVE_STRING_TO_DEBUG_FILE(tlEnum)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		tlIndent = "	"
		SAVE_STRING_TO_DEBUG_FILE(tlIndent)
	ENDREPEAT
	
	SAVE_STRING_TO_DEBUG_FILE("ENDENUM")
	
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("FUNC CINEMATIC_CAM_BEHAVIOUR_STRUCT [PREFIX]__GET_CAMERA(INT iCamIndex)")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	tlIndent = "	"
	SAVE_STRING_TO_DEBUG_FILE(tlIndent)
	SAVE_STRING_TO_DEBUG_FILE("CINEMATIC_CAM_BEHAVIOUR_STRUCT sCam")
	
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(tlIndent)
	SAVE_STRING_TO_DEBUG_FILE("SWITCH INT_TO_ENUM([PREFIX]__CAMERAS, iCamIndex)")
	
	INT iCase
	
	REPEAT iMaxCams iCase
		sDebugCam = sInst.sDebug.sDebugCams[iCase]
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		tlIndent = "		"
		SAVE_STRING_TO_DEBUG_FILE(tlIndent)
		SAVE_STRING_TO_DEBUG_FILE("CASE ")
		TEXT_LABEL tlCaseEnum = _CINEMATIC_CAM_SEQUENCE__GET_ENUM_FOR_DEBUG_CAM(sDebugCam)
		SAVE_STRING_TO_DEBUG_FILE(tlCaseEnum)
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		tlIndent = "			"
		SAVE_STRING_TO_DEBUG_FILE(tlIndent)
		
		/// INIT
		SAVE_STRING_TO_DEBUG_FILE("CINEMATIC_CAM_BEHAVIOUR__INIT(sCam, ")
		SAVE_VECTOR_TO_DEBUG_FILE(sDebugCam.sCam.vStartPos)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_VECTOR_TO_DEBUG_FILE(sDebugCam.sCam.vStartRot)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(sDebugCam.sCam.fStartFOV)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_INT_TO_DEBUG_FILE(sDebugCam.sCam.iCameraDuration)
		SAVE_STRING_TO_DEBUG_FILE(")")
		
		// Weighting
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE(tlIndent)
		SAVE_STRING_TO_DEBUG_FILE("CB_ADD_CAMERA_RANDOM_WEIGHTING(sCam, ")
		SAVE_FLOAT_TO_DEBUG_FILE(sDebugCam.sCam.fWeighting)
		SAVE_STRING_TO_DEBUG_FILE(")")
		
		IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sDebugCam.sCam, CCB_LERP)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE(tlIndent)
			SAVE_STRING_TO_DEBUG_FILE("CB_ADD_CAMERA_LERP(sCam, ")
			SAVE_VECTOR_TO_DEBUG_FILE(sDebugCam.sCam.vStartPos)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_VECTOR_TO_DEBUG_FILE(sDebugCam.sCam.vStartRot)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_VECTOR_TO_DEBUG_FILE(sDebugCam.sCam.vEndPos)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_VECTOR_TO_DEBUG_FILE(sDebugCam.sCam.vEndRot)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_INT_TO_DEBUG_FILE(sDebugCam.sCam.iLerpDuration)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_STRING_TO_DEBUG_FILE(GET_CAMERA_GRAPH_TYPE_NAME(sDebugCam.sCam.eLerpGraph))
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_INT_TO_DEBUG_FILE(sDebugCam.sCam.iLerpDelay)
			SAVE_STRING_TO_DEBUG_FILE(")")
		ENDIF
		
		IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sDebugCam.sCam, CCB_LERP_FOV)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE(tlIndent)
			SAVE_STRING_TO_DEBUG_FILE("CB_ADD_FOV_LERP(sCam, ")
			SAVE_FLOAT_TO_DEBUG_FILE(sDebugCam.sCam.fStartFOV)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_FLOAT_TO_DEBUG_FILE(sDebugCam.sCam.fEndFOV)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_INT_TO_DEBUG_FILE(sDebugCam.sCam.iFOVLerpDurationMS)
			SAVE_STRING_TO_DEBUG_FILE(")")
		ENDIF
		
		IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sDebugCam.sCam, CCB_LOOK_AT_COORD)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE(tlIndent)
			SAVE_STRING_TO_DEBUG_FILE("CB_ADD_LOOK_AT_COORD(sCam, ")
			SAVE_VECTOR_TO_DEBUG_FILE(sDebugCam.sCam.vLookAtCoord)
			SAVE_STRING_TO_DEBUG_FILE(")")
		ENDIF
		
		IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sDebugCam.sCam, CCB_LOOK_AT_ENTITY)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE(tlIndent)
			SAVE_STRING_TO_DEBUG_FILE("CB_ADD_LOOK_AT_ENTITY(sCam, ")
			SAVE_STRING_TO_DEBUG_FILE("PLAYER_PED_ID()")
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_VECTOR_TO_DEBUG_FILE(sDebugCam.sCam.vLookAtOffset)
			SAVE_STRING_TO_DEBUG_FILE(")")
		ENDIF
		
		IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sDebugCam.sCam, CCB_ORBIT_COORD)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE(tlIndent)
			SAVE_STRING_TO_DEBUG_FILE("CB_ADD_ORBIT_COORD(sCam, ")
			SAVE_VECTOR_TO_DEBUG_FILE(sDebugCam.sCam.vOrbitCentre)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_FLOAT_TO_DEBUG_FILE(sDebugCam.sCam.fDistanceFromCentre)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_FLOAT_TO_DEBUG_FILE(sDebugCam.sCam.fOrbitSpeed)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_INT_TO_DEBUG_FILE(sDebugCam.sCam.iCameraDuration)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_INT_TO_DEBUG_FILE(sDebugCam.sCam.iOrbitDirection)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_FLOAT_TO_DEBUG_FILE(sDebugCam.sCam.fStartTheta)
			SAVE_STRING_TO_DEBUG_FILE(")")
		ENDIF
		
		IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sDebugCam.sCam, CCB_ORBIT_ENTITY)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE(tlIndent)
			SAVE_STRING_TO_DEBUG_FILE("CB_ADD_ORBIT_ENTITY(sCam, ")
			SAVE_STRING_TO_DEBUG_FILE("PLAYER_PED_ID()")
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_FLOAT_TO_DEBUG_FILE(sDebugCam.sCam.fDistanceFromCentre)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_FLOAT_TO_DEBUG_FILE(sDebugCam.sCam.fOrbitSpeed)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_INT_TO_DEBUG_FILE(sDebugCam.sCam.iCameraDuration)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_INT_TO_DEBUG_FILE(sDebugCam.sCam.iOrbitDirection)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_FLOAT_TO_DEBUG_FILE(sDebugCam.sCam.fStartTheta)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_VECTOR_TO_DEBUG_FILE(sDebugCam.sCam.vOrbitOffset)
			SAVE_STRING_TO_DEBUG_FILE(")")
		ENDIF
		
		IF CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(sDebugCam.sCam, CCB_SHAKE)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE(tlIndent)
			SAVE_STRING_TO_DEBUG_FILE("CB_ADD_CAMERA_SHAKE(sCam, ")
			SAVE_FLOAT_TO_DEBUG_FILE(sDebugCam.sCam.fCamShakeIntensity)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_STRING_TO_DEBUG_FILE("\"HAND_SHAKE\"")
			SAVE_STRING_TO_DEBUG_FILE(")")
		ENDIF
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		tlIndent = "		"
		SAVE_STRING_TO_DEBUG_FILE(tlIndent)
		SAVE_STRING_TO_DEBUG_FILE("BREAK")
		
	ENDREPEAT
	
	SAVE_NEWLINE_TO_DEBUG_FILE()
	tlIndent = "	"
	SAVE_STRING_TO_DEBUG_FILE(tlIndent)
	SAVE_STRING_TO_DEBUG_FILE("ENDSWITCH")
	
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(tlIndent)
	SAVE_STRING_TO_DEBUG_FILE("RETURN sCam")
	
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("ENDFUNC")
	
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("--------------------------------------------------------------------------------")
	
	PRINT_DEBUG_TICKER("Camera Sequence lookup output to temp_debug")
ENDPROC

DEBUGONLY PROC _CINEMATIC_CAM_SEQUENCE_DEBUG__DRAW_DEBUG_INFO(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	SET_TEXT_SCALE(0.35, 0.35)
	VECTOR vOrigin = <<0.1, 0.1, 0.0>>
	VECTOR vOffset = <<0.0, GET_RENDERED_CHARACTER_HEIGHT(0.35), 0.0>>
	
	SET_TEXT_COLOUR(255, 255, 255, 255)
	TEXT_LABEL_63 tlText = "Current Cam: ("
	tlText += sInst.iCurrentCamBehaviourIndex
	tlText += ") "
	tlText += sInst.sDebug.sDebugCams[sInst.iCurrentCamBehaviourIndex].strName
	DISPLAY_TEXT_WITH_LITERAL_STRING(vOrigin.x, vOrigin.y, "STRING", GET_STRING_FROM_TL(tlText))
	
	SET_TEXT_SCALE(0.35, 0.35)
	SET_TEXT_COLOUR(0, 255, 0, 255)
	vOrigin += vOffset
	tlText = "Accumulated Weights: "
	tlText += GET_STRING_FROM_FLOAT(sInst.fTotalAccumulatedWeights)
	DISPLAY_TEXT_WITH_LITERAL_STRING(vOrigin.x, vOrigin.y, "STRING", GET_STRING_FROM_TL(tlText))
	
	SET_TEXT_SCALE(0.35, 0.35)
	SET_TEXT_COLOUR(0, 255, 0, 255)
	vOrigin += vOffset
	tlText = "Debug cam count: "
	tlText += GET_STRING_FROM_INT(sInst.sDebug.iCamCount)
	DISPLAY_TEXT_WITH_LITERAL_STRING(vOrigin.x, vOrigin.y, "STRING", GET_STRING_FROM_TL(tlText))
ENDPROC

DEBUGONLY PROC CINEMATIC_CAM_SEQUENCE_DEBUG__MAINTAIN(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)	
	
	IF sInst.sDebug.bDrawDebug
		_CINEMATIC_CAM_SEQUENCE_DEBUG__DRAW_DEBUG_INFO(sInst)
	ENDIF
	
	IF sInst.sDebug.bSaveXML		
		_CINEMATIC_CAM_SEQUENCE_DEBUG__SAVE_CAM_SEQUENCE_TO_XML(sInst)
		sInst.sDebug.bSaveXML = FALSE
	ENDIF
	
	IF sInst.sDebug.bLoadXML
		_CINEMATIC_CAM_SEQUENCE_DEBUG__LOAD_CAM_SEQUENCE_FROM_XML(sInst)
		sInst.sDebug.bLoadXML = FALSE
	ENDIF
	
	IF sInst.sDebug.bOutputScript		
		_CINEMATIC_CAM_SEQUENCE_DEBUG__OUTPUT_SCRIPT_LOOKUP(sInst)
		sInst.sDebug.bOutputScript = FALSE
	ENDIF
	
	_CINEMATIC_CAM_SEQUENCE_DEBUG__MAINTAIN_ALL_CAM_ACTIONS(sInst)
		
	IF sInst.sDebug.bAddCam		
		_CINEMATIC_CAM_SEQUENCE_DEBUG__ADD_CAMERA(sInst)
		sInst.sDebug.bAddCam = FALSE
	ENDIF	
	
	IF sInst.sDebug.bResetToScriptCams
		_CINEMATIC_CAM_SEQUENCE_DEBUG__RESET_TO_SCRIPT_CAMS(sInst)
		sInst.sDebug.bResetToScriptCams = FALSE
	ENDIF
ENDPROC

DEBUGONLY PROC CINEMATIC_CAM_SEQUENCE_DEBUG__CLEANUP(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	SAFE_DELETE_OBJECT(sInst.sDebug.objHelper)
ENDPROC
#ENDIF
