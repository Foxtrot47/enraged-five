//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_CLUB_LIGHTING.sch																					//
// Description: Header file containing functionality for club scripted lighting.										//
// Written by:  Online Technical Team: Scott Ranken & Alex Murphy														//
// Date:  		04/09/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "rc_helper_functions.sch"
USING "net_simple_interior.sch"

#IF FEATURE_CASINO_NIGHTCLUB

USING "net_club_lighting_vars.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ FUNCTIONS ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Gets the club scripted lamp model.
/// RETURNS: The club scripted lamp model enum.
FUNC MODEL_NAMES GET_CLUB_LAMP_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_battle_lights_fx_lamp"))
ENDFUNC

/// PURPOSE:
///    Gets the club scripted lamp frame model.
/// RETURNS: The club scripted lamp frame model enum.
FUNC MODEL_NAMES GET_CLUB_LAMP_FRAME_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_battle_lights_fx_rotator"))
ENDFUNC

/// PURPOSE:
///    Gets a club light coords.
/// PARAMS:
///    eLocation - Lighting location to query.
/// RETURNS: The coords of a scripted light.
FUNC VECTOR GET_CLUB_LIGHT_COORDS(CLUB_HANGING_LIGHT_LOCATION eLocation)
	VECTOR vLightCoords = <<0.0, 0.0, 0.0>>
	
	SWITCH eLocation
		CASE CLUB_HANGING_LIGHT_SOUTH_1		vLightCoords = <<1562.9879, 248.8343, -47.4049>>	BREAK
		CASE CLUB_HANGING_LIGHT_SOUTH_2		vLightCoords = <<1562.9879, 251.8804, -47.4049>>	BREAK
		CASE CLUB_HANGING_LIGHT_EAST_1		vLightCoords = <<1560.2854, 254.2484, -47.4049>>	BREAK
		CASE CLUB_HANGING_LIGHT_EAST_2		vLightCoords = <<1556.9329, 254.2484, -47.4049>>	BREAK
		CASE CLUB_HANGING_LIGHT_FRONT_DJ_1	vLightCoords = <<1552.4967, 251.6578, -46.9362>>	BREAK
		CASE CLUB_HANGING_LIGHT_FRONT_DJ_2	vLightCoords = <<1552.4967, 248.1372, -46.9367>>	BREAK
		CASE CLUB_HANGING_LIGHT_BEHIND_DJ_1	vLightCoords = <<1548.0348, 252.1521, -45.7357>>	BREAK
		CASE CLUB_HANGING_LIGHT_BEHIND_DJ_2	vLightCoords = <<1548.0232, 247.6361, -45.7357>>	BREAK
		CASE CLUB_HANGING_LIGHT_WEST_1		vLightCoords = <<1552.2959, 243.7544, -47.4049>>	BREAK
	ENDSWITCH
	
	RETURN vLightCoords
ENDFUNC

/// PURPOSE:
///    Gets a club light heading.
/// PARAMS:
///    eLocation - Lighting location to query.
/// RETURNS: The heading of a scripted light.
FUNC FLOAT GET_CLUB_LIGHT_HEADING(CLUB_HANGING_LIGHT_LOCATION eLocation)
	FLOAT fLightHeading = 0.0
	
	SWITCH eLocation
		CASE CLUB_HANGING_LIGHT_SOUTH_1		fLightHeading = 180.0	BREAK
		CASE CLUB_HANGING_LIGHT_SOUTH_2		fLightHeading = 180.0	BREAK
		CASE CLUB_HANGING_LIGHT_EAST_1		fLightHeading = -90.0	BREAK
		CASE CLUB_HANGING_LIGHT_EAST_2		fLightHeading = -90.0	BREAK
		CASE CLUB_HANGING_LIGHT_FRONT_DJ_1	fLightHeading = 0.0		BREAK
		CASE CLUB_HANGING_LIGHT_FRONT_DJ_2	fLightHeading = 0.0		BREAK
		CASE CLUB_HANGING_LIGHT_BEHIND_DJ_1	fLightHeading = -30.0	BREAK
		CASE CLUB_HANGING_LIGHT_BEHIND_DJ_2	fLightHeading = 30.0	BREAK
		CASE CLUB_HANGING_LIGHT_WEST_1		fLightHeading = 40.0	BREAK
	ENDSWITCH
	
	RETURN fLightHeading
ENDFUNC

/// PURPOSE:
///    Gets a club lamp frame coords.
///    Uses the light coords and minuses a Z axis offset.
/// PARAMS:
///    eLocation - Lighting location to query.
/// RETURNS: The coords of a scripted lamp frame.
FUNC VECTOR GET_CLUB_LAMP_FRAME_COORDS(CLUB_HANGING_LIGHT_LOCATION eLocation)
	VECTOR vLightCoords = GET_CLUB_LIGHT_COORDS(eLocation)
	
	vLightCoords.z -= LAMP_FRAME_Z_AXIS_OFFSET
	
	RETURN vLightCoords
ENDFUNC

/// PURPOSE:
///    Gets a club light type model.
/// PARAMS:
///    eLightType - Lighting type to query.
/// RETURNS: The model of a light type.
FUNC MODEL_NAMES GET_CLUB_HANGING_LIGHT_TYPE_MODEL(CLUB_HANGING_LIGHT_TYPE eLightType)
	MODEL_NAMES eLightModel = DUMMY_MODEL_FOR_SCRIPT
	
	SWITCH eLightType
		CASE CLUB_HANGING_LIGHT_TYPE_SOFT_SPOT_LIGHT			eLightModel = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_battle_lights_fx_rigA"))	BREAK
		CASE CLUB_HANGING_LIGHT_TYPE_THIN_STRONG_SPOT_LIGHT		eLightModel = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_battle_lights_fx_rigB"))	BREAK
		CASE CLUB_HANGING_LIGHT_TYPE_ABSTRACT_GOBO				eLightModel = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_battle_lights_fx_rigC"))	BREAK
		CASE CLUB_HANGING_LIGHT_TYPE_STRIPY_GOBO				eLightModel = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_battle_lights_fx_rigD"))	BREAK
		CASE CLUB_HANGING_LIGHT_TYPE_STROBE						eLightModel = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_battle_lights_fx_rigE"))	BREAK
		CASE CLUB_HANGING_LIGHT_TYPE_PATTERN					eLightModel = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_battle_lights_fx_rigF"))	BREAK
		CASE CLUB_HANGING_LIGHT_TYPE_DOTS						eLightModel = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_battle_lights_fx_rigG"))	BREAK
		CASE CLUB_HANGING_LIGHT_TYPE_TRIANGLE					eLightModel = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_battle_lights_fx_rigH"))	BREAK
	ENDSWITCH
	
	RETURN eLightModel
ENDFUNC

/// PURPOSE:
///    Reset the colour data to white.
/// PARAMS:
///    ColourData - Colour data to set.
PROC RESET_LIGHT_COLOUR_DATA(COLOR_STRUCT &ColourData)
	ColourData.r = 255
	ColourData.g = 255
	ColourData.b = 255
	ColourData.a = 255
ENDPROC

/// PURPOSE:
///    Gets the light colour variants for the light location.
/// PARAMS:
///    eLocation - Lighting location to query.
/// RETURNS: The lighting variant type as an INT.
FUNC INT GET_LIGHT_COLOUR_VARIANT_FOR_LIGHT_LOCATION(CLUB_HANGING_LIGHT_LOCATION eLocation)
	INT iVariant = ENUM_TO_INT(DJBCV__INVALID)
	
	SWITCH eLocation
		CASE CLUB_HANGING_LIGHT_SOUTH_1			iVariant = ENUM_TO_INT(DJBCV__PRIMARY)		BREAK
		CASE CLUB_HANGING_LIGHT_SOUTH_2			iVariant = ENUM_TO_INT(DJBCV__SECONDARY)	BREAK
		CASE CLUB_HANGING_LIGHT_EAST_1			iVariant = ENUM_TO_INT(DJBCV__TERTIARY)		BREAK
		CASE CLUB_HANGING_LIGHT_EAST_2			iVariant = ENUM_TO_INT(DJBCV__PRIMARY)		BREAK
		CASE CLUB_HANGING_LIGHT_FRONT_DJ_1		iVariant = ENUM_TO_INT(DJBCV__SECONDARY)	BREAK
		CASE CLUB_HANGING_LIGHT_FRONT_DJ_2		iVariant = ENUM_TO_INT(DJBCV__TERTIARY)		BREAK
		CASE CLUB_HANGING_LIGHT_BEHIND_DJ_1		iVariant = ENUM_TO_INT(DJBCV__PRIMARY)		BREAK
		CASE CLUB_HANGING_LIGHT_BEHIND_DJ_2		iVariant = ENUM_TO_INT(DJBCV__SECONDARY)	BREAK
		CASE CLUB_HANGING_LIGHT_WEST_1			iVariant = ENUM_TO_INT(DJBCV__TERTIARY)		BREAK
	ENDSWITCH
	
	RETURN iVariant
ENDFUNC

/// PURPOSE:
///    Sets the RGB light colour data based on the current DJ.
/// PARAMS:
///    ColourData - Light colour data to set.
///    eLocation - Lighting location to query.
///    eDJ - The current DJ to set colour data for.
PROC SET_LIGHT_COLOUR_DATA(COLOR_STRUCT &ColourData, CLUB_HANGING_LIGHT_LOCATION eLocation, CLUB_DJS eDJ, INT iVariant = -1)
	RESET_LIGHT_COLOUR_DATA(ColourData)
	
	IF iVariant = -1
		iVariant = GET_LIGHT_COLOUR_VARIANT_FOR_LIGHT_LOCATION(eLocation)
	ENDIF
	
	SWITCH eDJ
		CASE CLUB_DJ_KEINEMUSIK_NIGHTCLUB
			CASINO_NIGHTCLUB_BATTLE_SCREENS__GET_KEINEMUSIK_COLOUR_VARIANTS(iVariant, ColourData)
		BREAK
		
		CASE CLUB_DJ_PALMS_TRAX
			CASINO_NIGHTCLUB_BATTLE_SCREENS__GET_PALMS_TRAX_COLOUR_VARIANTS(iVariant, ColourData)
		BREAK
		
		CASE CLUB_DJ_MOODYMANN
			CASINO_NIGHTCLUB_BATTLE_SCREENS__GET_MOODYMANN_COLOUR_VARIANTS(iVariant, ColourData)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Sets the club lighting state.
/// PARAMS:
///    LightData - Lighting data to set.
///    eState - Lighting state to set.
PROC SET_CLUB_LIGHT_STATE(HANGING_LIGHT_DATA &LightData, CLUB_LIGHT_STATES eState)
	IF (LightData.eState != eState)
		LightData.eState = eState
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the club lighting sequence.
/// PARAMS:
///    LightData - Lighting data to set.
///    eSequence - Lighting sequence to set.
PROC SET_CLUB_HANGING_LIGHT_SEQUENCE(HANGING_LIGHT_DATA &LightData, CLUB_HANGING_LIGHT_SEQUENCE eSequence)
	IF (LightData.eSequence != eSequence)
		LightData.eSequence = eSequence
	ENDIF
ENDPROC

/// PURPOSE:
///    Iterates over all the lighting types of a lamp and sets the required types visible.
///    Visibility is achieved through changing the light objects alpha level.
/// PARAMS:
///    LightData - Lighting data to set.
///    eShowLightType - Lighting type to show.
PROC SHOW_LIGHT_SEQUENCE_LIGHT_TYPES(HANGING_LIGHT_DATA &LightData, CLUB_HANGING_LIGHT_TYPE eShowLightType)
	INT iLightType
	REPEAT MAX_CLUB_HANGING_LIGHT_TYPES iLightType
		IF (iLightType = ENUM_TO_INT(eShowLightType))
			SET_ENTITY_ALPHA(LightData.LightID[iLightType], 255, FALSE)
		ELSE
			SET_ENTITY_ALPHA(LightData.LightID[iLightType], 0, FALSE)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Sets the alpha of a light object.
/// PARAMS:
///    LightData - Lighting data to set.
///    eLightType - Lighting type to change alpha of.
///    iAlpha - Alpha value to set.
PROC SET_LIGHT_ALPHA(HANGING_LIGHT_DATA &LightData, CLUB_HANGING_LIGHT_TYPE eLightType, INT iAlpha)
	IF (iAlpha >= 0 AND iAlpha <= 255)
		SET_ENTITY_ALPHA(LightData.LightID[ENUM_TO_INT(eLightType)], iAlpha, FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Each light type for a lamp is created on initialise. This function shows the correct light
///    type for a sequence and hides the rest. Visibility is achieved through changing light alphas.
/// PARAMS:
///    LightData - Lighting data to set.
///    eSequence - Lighting sequence to set.
///    iSequenceID - Sequences can use multiple light types. This ID specifies which to use.
PROC ACTIVATE_SEQUENCE_LIGHT_TYPES(HANGING_LIGHT_DATA &LightData, CLUB_HANGING_LIGHT_SEQUENCE eSequence, INT iSequenceID = 0)
	SWITCH eSequence
		CASE CLUB_HANGING_LIGHT_SEQUENCE_MEXICAN_WAVE
			SWITCH iSequenceID
				CASE 0	SHOW_LIGHT_SEQUENCE_LIGHT_TYPES(LightData, CLUB_HANGING_LIGHT_TYPE_THIN_STRONG_SPOT_LIGHT)	BREAK
			ENDSWITCH
		BREAK
		
		CASE CLUB_HANGING_LIGHT_SEQUENCE_CIRCUMFERENCE_POINT_CIRCLE
			SWITCH iSequenceID
				CASE 0	SHOW_LIGHT_SEQUENCE_LIGHT_TYPES(LightData, CLUB_HANGING_LIGHT_TYPE_THIN_STRONG_SPOT_LIGHT)	BREAK
			ENDSWITCH
		BREAK
		
		CASE CLUB_HANGING_LIGHT_SEQUENCE_SWEEP_UP
		CASE CLUB_HANGING_LIGHT_SEQUENCE_RANDOM_FLASH
			SWITCH LightData.iTypeCounter
				CASE 0	SHOW_LIGHT_SEQUENCE_LIGHT_TYPES(LightData, CLUB_HANGING_LIGHT_TYPE_ABSTRACT_GOBO)	BREAK
				CASE 1	SHOW_LIGHT_SEQUENCE_LIGHT_TYPES(LightData, CLUB_HANGING_LIGHT_TYPE_PATTERN)			BREAK
				CASE 2	SHOW_LIGHT_SEQUENCE_LIGHT_TYPES(LightData, CLUB_HANGING_LIGHT_TYPE_DOTS)			BREAK
				CASE 3	SHOW_LIGHT_SEQUENCE_LIGHT_TYPES(LightData, CLUB_HANGING_LIGHT_TYPE_TRIANGLE)		BREAK
			ENDSWITCH
		BREAK
		
		CASE CLUB_HANGING_LIGHT_SEQUENCE_VERTICAL_STROBE
			SWITCH LightData.iTypeCounter
				CASE 0	SHOW_LIGHT_SEQUENCE_LIGHT_TYPES(LightData, CLUB_HANGING_LIGHT_TYPE_SOFT_SPOT_LIGHT)	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Sets a lights colour. The native SET_PROP_LIGHT_COLOR is very expensive and is
///    only called a maximum of three times per frame. Any more and framerate begins to drop.
/// PARAMS:
///    LightData - Lighting data to set.
///    eLocation - Lighting location to query.
///    eLightType - Lighting type to query.
/// RETURNS: TRUE when a lights colour has been set. FALSE otherwise.
FUNC BOOL SET_LIGHT_COLOUR(LOCAL_HANGING_LIGHT_DATA &LightData, CLUB_HANGING_LIGHT_LOCATION eLocation, CLUB_HANGING_LIGHT_TYPE eLightType, BOOL bSkipStagger = FALSE)
	IF NOT bSkipStagger
		IF (LightData.iColourLightCounter >= MAX_CLUB_LIGHTS_COLOURED_PER_FRAME)
			LightData.iColourLightCounter = 0
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF SET_PROP_LIGHT_COLOR(LightData.Data[ENUM_TO_INT(eLocation)].LightID[ENUM_TO_INT(eLightType)], TRUE, LightData.Data[ENUM_TO_INT(eLocation)].ColourData.r, LightData.Data[ENUM_TO_INT(eLocation)].ColourData.g, LightData.Data[ENUM_TO_INT(eLocation)].ColourData.b)
		SET_BIT(LightData.Data[ENUM_TO_INT(eLocation)].iBS, BS_CLUB_HANGING_LIGHTS_SET_COLOUR)
		
		IF NOT bSkipStagger
			LightData.iColourLightCounter++
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF NOT bSkipStagger
		LightData.iColourLightCounter++
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Updates a lights rotation for a sequenced effect.
///    Rotation is achieved through interpolation.
/// PARAMS:
///    LightData - Lighting data to set.
///    iRotationTimeMS - Rotation time in milliseconds.
///    vStartRotation - The starting rotation of the interp.
///    vEndRotation - The ending rotation of the interp.
///    bRotateLampFrame - Whether the lamp frame should rotate with the lamp.
/// RETURNS: TRUE when the light has finished rotating from vStartRotation to vEndRotation.
FUNC BOOL UPDATE_SEQUENCE_LIGHT_ROTATION(HANGING_LIGHT_DATA &LightData, INT iRotationTimeMS, VECTOR vStartRotation, VECTOR vEndRotation, BOOL bRotateLampFrame = FALSE)
	IF NOT IS_BIT_SET(LightData.iBS, BS_CLUB_HANGING_LIGHTS_SET_ROTATION_TIMER)
		SET_BIT(LightData.iBS, BS_CLUB_HANGING_LIGHTS_SET_ROTATION_TIMER)
		
		LightData.tdLightRotationTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), iRotationTimeMS)
	ENDIF
	
	INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), LightData.tdLightRotationTimer))
	
	FLOAT fAlpha = ABSF(1.0 - ((TO_FLOAT(iTimeDifference) / iRotationTimeMS) * 1.0))
	
	VECTOR vNewRotation = INTERP_ROTATION(vStartRotation, vEndRotation, fAlpha, INTERPTYPE_COSINE)
	
	SET_ENTITY_ROTATION(LightData.Lamp, vNewRotation)
	
	IF (bRotateLampFrame)
		SET_ENTITY_ROTATION(LightData.LampFrame, <<0.0, 0.0, vNewRotation.z>>)
	ENDIF
	
	IF (fAlpha >= MAX_LIGHT_ROTATION_ALPHA)
		SET_ENTITY_ROTATION(LightData.Lamp, vEndRotation)
		
		IF (bRotateLampFrame)
			SET_ENTITY_ROTATION(LightData.LampFrame, <<0.0, 0.0, vEndRotation.z>>)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Performs pulse effect on light. Based on beat every how many bars.
/// PARAMS:
///    LightData - Lighting data to query.
///    eLocation - Lighting location to query.
///    iBeat - Beat to query
///    iBar - Bar to query
/// RETURNS:
///    TRUE if music is on selected beat
FUNC BOOL IS_MUSIC_ON_THIS_BEAT(HANGING_LIGHT_DATA &LightData, INT iBeat, INT iBar)
	FLOAT fTimeS
	FLOAT fBPM
	
	INT iBeatNum
	INT iSelectedBeat
	
	IF GET_NEXT_AUDIBLE_BEAT(fTimeS, fBPM, iBeatNum)
		iSelectedBeat = (iBar * 4) + iBeat
		
		IF LightData.iBPM != iBeatNum
			LightData.iPulseCounter++
			
			IF iSelectedBeat <= 4
				IF LightData.iPulseCounter > (iBeat + 4)
					LightData.iPulseCounter = iBeatNum
				ENDIF
			ELSE
				IF LightData.iPulseCounter > iSelectedBeat
					LightData.iPulseCounter = iBeatNum
				ENDIF
			ENDIF
			
			LightData.iBPM = iBeatNum
		ENDIF
		
		IF LightData.iPulseCounter = iSelectedBeat
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_BEAT_AT_START_OF_BAR()
	FLOAT fTimeS, fBPM
	
	INT iBeatNum
	
	IF GET_NEXT_AUDIBLE_BEAT(fTimeS, fBPM, iBeatNum)
		RETURN (iBeatNum = 1)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Get the number of light rotations for the specified sequence
/// PARAMS:
///    eSequence - Lighting sequence to query.
/// RETURNS:
///    Total number of light rotations
FUNC INT GET_SEQUENCE_TOTAL(CLUB_HANGING_LIGHT_SEQUENCE eSequence, NIGHTCLUB_AUDIO_TAGS eIntensity = AUDIO_TAG_NULL)
	SWITCH eSequence
		CASE CLUB_HANGING_LIGHT_SEQUENCE_CIRCUMFERENCE_POINT_CIRCLE
			IF eIntensity = AUDIO_TAG_NULL
			OR eIntensity = AUDIO_TAG_LOW
			OR eIntensity = AUDIO_TAG_MEDIUM
				RETURN 8
			ELSE
				RETURN 12
			ENDIF
		BREAK
		
		CASE CLUB_HANGING_LIGHT_SEQUENCE_SWEEP_UP			RETURN 16
		CASE CLUB_HANGING_LIGHT_SEQUENCE_VERTICAL_STROBE	RETURN 17
		CASE CLUB_HANGING_LIGHT_SEQUENCE_RANDOM_FLASH		RETURN 32
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the current sweep light type
/// PARAMS:
///    LightData - Lighting data to query.
/// RETURNS:
///    CLUB_HANGING_LIGHT_TYPE - Current sweep light type
FUNC CLUB_HANGING_LIGHT_TYPE GET_VARIED_LIGHT_TYPE(HANGING_LIGHT_DATA &LightData)
	SWITCH LightData.iTypeCounter
		CASE 0	RETURN CLUB_HANGING_LIGHT_TYPE_ABSTRACT_GOBO
		CASE 1	RETURN CLUB_HANGING_LIGHT_TYPE_PATTERN
		CASE 2	RETURN CLUB_HANGING_LIGHT_TYPE_DOTS
		CASE 3	RETURN CLUB_HANGING_LIGHT_TYPE_TRIANGLE
	ENDSWITCH
	
	RETURN CLUB_HANGING_LIGHT_TYPE_INVALID
ENDFUNC

/// PURPOSE:
///    Get a random rotation for the specified light
/// PARAMS:
///    eLocation - Light location to query
///    iRandID - Random index
/// RETURNS:
///    VECTOR - Light rotation
FUNC VECTOR GET_RANDOM_LIGHT_ROTATION(CLUB_HANGING_LIGHT_LOCATION eLocation, INT iRandID)
	VECTOR vNewRot
	VECTOR vDefaultRot = <<0.0, 0.0, GET_CLUB_LIGHT_HEADING(eLocation)>>
	
	SWITCH ENUM_TO_INT(eLocation)
		CASE 0
			SWITCH iRandID
				CASE 0	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 30, vDefaultRot.z + 20>>	BREAK
				CASE 1	vNewRot = <<vDefaultRot.x, vDefaultRot.y - 10, vDefaultRot.z - 50>>	BREAK
				CASE 2	vNewRot = <<vDefaultRot.x, vDefaultRot.y - 40, vDefaultRot.z + 10>>	BREAK
				CASE 3	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 20, vDefaultRot.z + 40>>	BREAK
				CASE 4	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 60, vDefaultRot.z>>		BREAK
			ENDSWITCH
		BREAK
		
		CASE 1
			SWITCH iRandID
				CASE 0	vNewRot = <<vDefaultRot.x, vDefaultRot.y - 10, vDefaultRot.z - 60>>	BREAK
				CASE 1	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 40, vDefaultRot.z - 5>>	BREAK
				CASE 2	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 20, vDefaultRot.z + 30>>	BREAK
				CASE 3	vNewRot = <<vDefaultRot.x, vDefaultRot.y - 25, vDefaultRot.z + 10>>	BREAK
				CASE 4	vNewRot = <<vDefaultRot.x, vDefaultRot.y - 40, vDefaultRot.z + 60>>	BREAK
			ENDSWITCH
		BREAK
		
		CASE 2
			SWITCH iRandID
				CASE 0	vNewRot = <<vDefaultRot.x, vDefaultRot.y - 35, vDefaultRot.z + 10>>	BREAK
				CASE 1	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 35, vDefaultRot.z>>		BREAK
				CASE 2	vNewRot = <<vDefaultRot.x, vDefaultRot.y - 10, vDefaultRot.z + 70>>	BREAK
				CASE 3	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 60, vDefaultRot.z - 15>>	BREAK
				CASE 4	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 70, vDefaultRot.z - 55>>	BREAK
			ENDSWITCH
		BREAK
		
		CASE 3
			SWITCH iRandID
				CASE 0	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 60, vDefaultRot.z - 30>>	BREAK
				CASE 1	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 10, vDefaultRot.z + 15>>	BREAK
				CASE 2	vNewRot = <<vDefaultRot.x, vDefaultRot.y - 40, vDefaultRot.z>>		BREAK
				CASE 3	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 25, vDefaultRot.z + 50>>	BREAK
				CASE 4	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 60, vDefaultRot.z + 10>>	BREAK
			ENDSWITCH
		BREAK
		
		CASE 4
			SWITCH iRandID
				CASE 0	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 50, vDefaultRot.z>>		BREAK
				CASE 1	vNewRot = <<vDefaultRot.x, vDefaultRot.y - 20, vDefaultRot.z + 35>>	BREAK
				CASE 2	vNewRot = <<vDefaultRot.x, vDefaultRot.y, vDefaultRot.z + 50>>		BREAK
				CASE 3	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 10, vDefaultRot.z>>		BREAK
				CASE 4	vNewRot = <<vDefaultRot.x, vDefaultRot.y - 30, vDefaultRot.z - 20>>	BREAK
			ENDSWITCH
		BREAK
		
		CASE 5
			SWITCH iRandID
				CASE 0	vNewRot = <<vDefaultRot.x, vDefaultRot.y, vDefaultRot.z + 30>>		BREAK
				CASE 1	vNewRot = <<vDefaultRot.x, vDefaultRot.y - 15, vDefaultRot.z + 60>>	BREAK
				CASE 2	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 50, vDefaultRot.z - 10>>	BREAK
				CASE 3	vNewRot = <<vDefaultRot.x, vDefaultRot.y - 20, vDefaultRot.z + 20>>	BREAK
				CASE 4	vNewRot = <<vDefaultRot.x, vDefaultRot.y, vDefaultRot.z - 20>>		BREAK
			ENDSWITCH
		BREAK
		
		CASE 6
			SWITCH iRandID
				CASE 0	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 40, vDefaultRot.z + 60>>	BREAK
				CASE 1	vNewRot = <<vDefaultRot.x, vDefaultRot.y - 20, vDefaultRot.z - 30>>	BREAK
				CASE 2	vNewRot = <<vDefaultRot.x, vDefaultRot.y - 40, vDefaultRot.z + 10>>	BREAK
				CASE 3	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 30, vDefaultRot.z - 20>>	BREAK
				CASE 4	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 60, vDefaultRot.z + 20>>	BREAK
			ENDSWITCH
		BREAK
		
		CASE 7
			SWITCH iRandID
				CASE 0	vNewRot = <<vDefaultRot.x, vDefaultRot.y - 20, vDefaultRot.z - 60>>	BREAK
				CASE 1	vNewRot = <<vDefaultRot.x, vDefaultRot.y, vDefaultRot.z>>			BREAK
				CASE 2	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 30, vDefaultRot.z + 10>>	BREAK
				CASE 3	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 50, vDefaultRot.z - 10>>	BREAK
				CASE 4	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 10, vDefaultRot.z + 30>>	BREAK
			ENDSWITCH
		BREAK
		
		CASE 8
			SWITCH iRandID
				CASE 0	vNewRot = <<vDefaultRot.x, vDefaultRot.y - 10, vDefaultRot.z - 10>>	BREAK
				CASE 1	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 30, vDefaultRot.z - 35>>	BREAK
				CASE 2	vNewRot = <<vDefaultRot.x, vDefaultRot.y + 60, vDefaultRot.z + 20>>	BREAK
				CASE 3	vNewRot = <<vDefaultRot.x, vDefaultRot.y, vDefaultRot.z + 10>>		BREAK
				CASE 4	vNewRot = <<vDefaultRot.x, vDefaultRot.y - 20, vDefaultRot.z + 50>>	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN vNewRot
ENDFUNC

FUNC VECTOR GET_GRID_LIGHT_COORD(INT x, INT y, SIMPLE_INTERIOR_DETAILS &sSimpleInteriorDetails)
	SWITCH x
		CASE 0
			SWITCH y
				CASE 0	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<3.7219, 3.4921, 5.721>>, sSimpleInteriorDetails)
				CASE 1	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<3.7219, 2.1514, 5.4099>>, sSimpleInteriorDetails)
				CASE 2	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<3.7219, 0.7985, 5.721>>, sSimpleInteriorDetails)
				CASE 3	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<3.7219, -1.0028, 5.721>>, sSimpleInteriorDetails)
				CASE 4	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<3.7219, -2.3244, 5.4099>>, sSimpleInteriorDetails)
				CASE 5	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<3.7219, -3.7085, 5.721>>, sSimpleInteriorDetails)
			ENDSWITCH
		BREAK
		
		CASE 1
			SWITCH y
				CASE 0	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<5.8927, 3.4921, 5.721>>, sSimpleInteriorDetails)
				CASE 1	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<5.8927, 2.1514, 5.4099>>, sSimpleInteriorDetails)
				CASE 2	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<5.8927, 0.7985, 5.721>>, sSimpleInteriorDetails)
				CASE 3	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<5.8927, -1.0028, 5.721>>, sSimpleInteriorDetails)
				CASE 4	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<5.8927, -2.3244, 5.4099>>, sSimpleInteriorDetails)
				CASE 5	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<5.8927, -3.7085, 5.721>>, sSimpleInteriorDetails)
			ENDSWITCH
		BREAK
		
		CASE 2
			SWITCH y
				CASE 0	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<8.0635, 3.4921, 5.721>>, sSimpleInteriorDetails)
				CASE 1	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<8.0635, 2.1514, 5.4099>>, sSimpleInteriorDetails)
				CASE 2	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<8.0635, 0.7985, 5.721>>, sSimpleInteriorDetails)
				CASE 3	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<8.0635, -1.0028, 5.721>>, sSimpleInteriorDetails)
				CASE 4	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<8.0635, -2.3244, 5.4099>>, sSimpleInteriorDetails)
				CASE 5	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<8.0635, -3.7085, 5.721>>, sSimpleInteriorDetails)
			ENDSWITCH
		BREAK
		
		CASE 3
			SWITCH y
				CASE 0	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<10.2343, 3.4921, 5.721>>, sSimpleInteriorDetails)
				CASE 1	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<10.2343, 2.1514, 5.4099>>, sSimpleInteriorDetails)
				CASE 2	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<10.2343, 0.7985, 5.721>>, sSimpleInteriorDetails)
				CASE 3	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<10.2343, -1.0028, 5.721>>, sSimpleInteriorDetails)
				CASE 4	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<10.2343, -2.3244, 5.4099>>, sSimpleInteriorDetails)
				CASE 5	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<10.2343, -3.7085, 5.721>>, sSimpleInteriorDetails)
			ENDSWITCH
		BREAK
		
		CASE 4
			SWITCH y
				CASE 0	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<12.4051, 3.4921, 5.721>>, sSimpleInteriorDetails)
				CASE 1	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<12.4051, 2.1514, 5.4099>>, sSimpleInteriorDetails)
				CASE 2	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<12.4051, 0.7985, 5.721>>, sSimpleInteriorDetails)
				CASE 3	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<12.4051, -1.0028, 5.721>>, sSimpleInteriorDetails)
				CASE 4	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<12.4051, -2.3244, 5.4099>>, sSimpleInteriorDetails)
				CASE 5	RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(<<12.4051, -3.7085, 5.721>>, sSimpleInteriorDetails)
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BOOL HAVE_GRID_LIGHTS_BEEN_CREATED(LOCAL_GRID_LIGHT_DATA &LightData)
	RETURN IS_BIT_SET(LightData.iBS, BS_CLUB_GRID_LIGHTS_CREATED)
ENDFUNC

FUNC BOOL IS_GRID_LIGHT_BIT_SET_VALID(INT iBit)
	RETURN (iBit != -1)
ENDFUNC

FUNC INT GET_GRID_LIGHT_BIT_SET(INT x, INT y)
	SWITCH x
		CASE 0
			SWITCH y
				CASE 0 RETURN BS_GRID_LIGHT_SET_TINT_0
				CASE 1 RETURN BS_GRID_LIGHT_SET_TINT_1
				CASE 2 RETURN BS_GRID_LIGHT_SET_TINT_2
				CASE 3 RETURN BS_GRID_LIGHT_SET_TINT_3
				CASE 4 RETURN BS_GRID_LIGHT_SET_TINT_4
				CASE 5 RETURN BS_GRID_LIGHT_SET_TINT_5
			ENDSWITCH
		BREAK
		
		CASE 1
			SWITCH y
				CASE 0 RETURN BS_GRID_LIGHT_SET_TINT_6
				CASE 1 RETURN BS_GRID_LIGHT_SET_TINT_7
				CASE 2 RETURN BS_GRID_LIGHT_SET_TINT_8
				CASE 3 RETURN BS_GRID_LIGHT_SET_TINT_9
				CASE 4 RETURN BS_GRID_LIGHT_SET_TINT_10
				CASE 5 RETURN BS_GRID_LIGHT_SET_TINT_11
			ENDSWITCH
		BREAK
		
		CASE 2
			SWITCH y
				CASE 0 RETURN BS_GRID_LIGHT_SET_TINT_12
				CASE 1 RETURN BS_GRID_LIGHT_SET_TINT_13
				CASE 2 RETURN BS_GRID_LIGHT_SET_TINT_14
				CASE 3 RETURN BS_GRID_LIGHT_SET_TINT_15
				CASE 4 RETURN BS_GRID_LIGHT_SET_TINT_16
				CASE 5 RETURN BS_GRID_LIGHT_SET_TINT_17
			ENDSWITCH
		BREAK
		
		CASE 3
			SWITCH y
				CASE 0 RETURN BS_GRID_LIGHT_SET_TINT_18
				CASE 1 RETURN BS_GRID_LIGHT_SET_TINT_19
				CASE 2 RETURN BS_GRID_LIGHT_SET_TINT_20
				CASE 3 RETURN BS_GRID_LIGHT_SET_TINT_21
				CASE 4 RETURN BS_GRID_LIGHT_SET_TINT_22
				CASE 5 RETURN BS_GRID_LIGHT_SET_TINT_23
			ENDSWITCH
		BREAK
		
		CASE 4
			SWITCH y
				CASE 0 RETURN BS_GRID_LIGHT_SET_TINT_24
				CASE 1 RETURN BS_GRID_LIGHT_SET_TINT_25
				CASE 2 RETURN BS_GRID_LIGHT_SET_TINT_26
				CASE 3 RETURN BS_GRID_LIGHT_SET_TINT_27
				CASE 4 RETURN BS_GRID_LIGHT_SET_TINT_28
				CASE 5 RETURN BS_GRID_LIGHT_SET_TINT_29
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC BOOL HAVE_GRID_LIGHTS_BEEN_INIT_TINTED(LOCAL_GRID_LIGHT_DATA &LightData)
	IF NOT IS_BIT_SET(LightData.iBS, BS_CLUB_GRID_LIGHTS_INIT_TINT)
		INT iLightsTinted
		
		INT x
		REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_X x
			INT y
			REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_Y y
				IF IS_GRID_LIGHT_BIT_SET_VALID(GET_GRID_LIGHT_BIT_SET(x, y))
					IF IS_BIT_SET(LightData.iGridLightBS, GET_GRID_LIGHT_BIT_SET(x, y))
						iLightsTinted++
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
		
		IF iLightsTinted = (NIGHTCLUB_GRID_LIGHTS_ARRAY_X * NIGHTCLUB_GRID_LIGHTS_ARRAY_Y)
			SET_BIT(LightData.iBS, BS_CLUB_GRID_LIGHTS_INIT_TINT)
		ENDIF
	ENDIF
	
	RETURN IS_BIT_SET(LightData.iBS, BS_CLUB_GRID_LIGHTS_INIT_TINT)
ENDFUNC

PROC SET_GRID_LIGHT_TINT(LOCAL_GRID_LIGHT_DATA &LightData, INT x, INT y, INT iR, INT iG, INT iB)
	IF x = LightData.iTintX
	AND y = LightData.iTintY
		IF GET_FRAME_COUNT() % 2 = 0
			IF IS_GRID_LIGHT_BIT_SET_VALID(GET_GRID_LIGHT_BIT_SET(x, y))
				IF NOT IS_BIT_SET(LightData.iGridLightBS, GET_GRID_LIGHT_BIT_SET(x, y))
					IF NOT IS_BIT_SET(LightData.iBS, BS_CLUB_GRID_LIGHTS_APPLY_TINT)
						IF DOES_ENTITY_EXIST(LightData.objLight[x][y])
							IF SET_PROP_LIGHT_COLOR(LightData.objLight[x][y], TRUE, iR, iG, iB)
								SET_BIT(LightData.iGridLightBS, GET_GRID_LIGHT_BIT_SET(x, y))
								
								LightData.iTintY++
								
								IF LightData.iTintY > (NIGHTCLUB_GRID_LIGHTS_ARRAY_Y - 1)
									LightData.iTintY = 0
									
									LightData.iTintX++
									
									IF LightData.iTintX > (NIGHTCLUB_GRID_LIGHTS_ARRAY_X - 1)
										LightData.iTintX = 0
									ENDIF
								ENDIF
								
								SET_BIT(LightData.iBS, BS_CLUB_GRID_LIGHTS_APPLY_TINT)
								
								LightData.iLightsTinted++
								
								EXIT
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(LightData.iBS, BS_CLUB_GRID_LIGHTS_APPLY_TINT)
				CLEAR_BIT(LightData.iBS, BS_CLUB_GRID_LIGHTS_APPLY_TINT)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GRID_LIGHT_INIT_TINTING(LOCAL_GRID_LIGHT_DATA &LightData)
	IF HAVE_GRID_LIGHTS_BEEN_INIT_TINTED(LightData)
		EXIT
	ENDIF
	
	INT x
	REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_X x
		INT y
		REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_Y y
			SET_GRID_LIGHT_TINT(LightData, x, y, LightData.ColourData.r, LightData.ColourData.g, LightData.ColourData.b)
		ENDREPEAT
	ENDREPEAT
ENDPROC

FUNC INT GET_GRID_LIGHT_FADE_BS(INT y)
	SWITCH y
		CASE 0 RETURN BS_CLUB_GRID_LIGHTS_SET_FADE_TIMER_1
		CASE 1 RETURN BS_CLUB_GRID_LIGHTS_SET_FADE_TIMER_2
		CASE 2 RETURN BS_CLUB_GRID_LIGHTS_SET_FADE_TIMER_3
		CASE 3 RETURN BS_CLUB_GRID_LIGHTS_SET_FADE_TIMER_4
		CASE 4 RETURN BS_CLUB_GRID_LIGHTS_SET_FADE_TIMER_5
		CASE 5 RETURN BS_CLUB_GRID_LIGHTS_SET_FADE_TIMER_6
	ENDSWITCH
	
	RETURN -1
ENDFUNC

PROC CLAMP_GRID_LIGHT_ALPHAS(LOCAL_GRID_LIGHT_DATA &LightData)
	REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_Y LightData.iCounter
		IF LightData.fAlpha[LightData.iCounter] < 0
			LightData.fAlpha[LightData.iCounter] = 0
			
			IF IS_BIT_SET(LightData.iBS, GET_GRID_LIGHT_FADE_BS(LightData.iCounter))
				CLEAR_BIT(LightData.iBS, GET_GRID_LIGHT_FADE_BS(LightData.iCounter))
			ENDIF
		ELIF LightData.fAlpha[LightData.iCounter] > 255
			LightData.fAlpha[LightData.iCounter] = 255
			
			IF IS_BIT_SET(LightData.iBS, GET_GRID_LIGHT_FADE_BS(LightData.iCounter))
				CLEAR_BIT(LightData.iBS, GET_GRID_LIGHT_FADE_BS(LightData.iCounter))
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ CLEANUP ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Deletes the club scripted lighting objects.
/// PARAMS:
///    LightData - Lighting data to set.
PROC CLEANUP_CLUB_HANGING_LIGHTS(HANGING_LIGHT_DATA &LightData[])
	INT iLightID
	REPEAT MAX_CLUB_HANGING_LIGHT_OBJECTS iLightID
		SAFE_DELETE_OBJECT(LightData[iLightID].Lamp)
		SAFE_DELETE_OBJECT(LightData[iLightID].LampFrame)
		
		INT iLightType
		REPEAT MAX_CLUB_HANGING_LIGHT_TYPES iLightType
			SAFE_DELETE_OBJECT(LightData[iLightID].LightID[iLightType])
		ENDREPEAT
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Deletes the club scripted lighting objects.
/// PARAMS:
///    LightData - Lighting data to set.
PROC CLEANUP_CLUB_GRID_LIGHTS(LOCAL_GRID_LIGHT_DATA &LightData)
	INT x
	REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_X x
		INT y
		REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_Y y
			SAFE_DELETE_OBJECT(LightData.objLight[x][y])
		ENDREPEAT
	ENDREPEAT
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INITIALISE ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Hides the interior placement lighting so we can create scripted lighting in its place.
/// PARAMS:
///    eLocation - Lighting location to query.
PROC MODEL_HIDE_INTERIOR_LIGHTING(CLUB_HANGING_LIGHT_LOCATION eLocation)
	CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(GET_CLUB_LIGHT_COORDS(eLocation), MAX_MODEL_HIDE_LIGHT_RADIUS, GET_CLUB_LAMP_MODEL(), TRUE)
	CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(GET_CLUB_LIGHT_COORDS(eLocation), MAX_MODEL_HIDE_LIGHT_RADIUS, GET_CLUB_LAMP_FRAME_MODEL(), TRUE)
ENDPROC

/// PURPOSE:
///    Attaches a lamp frame to a lamp.
/// PARAMS:
///    LightData - Lighting data to set.
PROC ATTACH_LAMP_FRAME_TO_LAMP(HANGING_LIGHT_DATA &LightData)
	IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(LightData.LampFrame, LightData.Lamp)
		ATTACH_ENTITY_TO_ENTITY(LightData.LampFrame, LightData.Lamp, -1, <<0.0, 0.0, LIGHT_ATTACHMENT_Z_AXIS_OFFSET>>, <<0.0, 0.0, 0.0>>)
	ENDIF
ENDPROC

/// PURPOSE:
///    Detaches a lamp frame from a lamp.
/// PARAMS:
///    LightData - Lighting data to set.
PROC DETACH_LAMP_FRAME_FROM_LAMP(HANGING_LIGHT_DATA &LightData)
	IF IS_ENTITY_ATTACHED_TO_ENTITY(LightData.LampFrame, LightData.Lamp)
		DETACH_ENTITY(LightData.LampFrame)
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates the lamp, lamp frame and all light type objects used for the scripted lighting.
///    All lighting types are created here and model shown/hidden when needed.
/// PARAMS:
///    LightData - Lighting data to set.
///    eLocation - Lighting location to query.
///    interiorID - ID of the interior.
///    iRoomKey - Room key the lights are spawned in. Used to force light objects for room.
PROC CREATE_CLUB_SCRIPTED_LIGHTING(HANGING_LIGHT_DATA &LightData, CLUB_HANGING_LIGHT_LOCATION eLocation, INTERIOR_INSTANCE_INDEX interiorID, INT iRoomKey)
	LightData.Lamp = CREATE_OBJECT_NO_OFFSET(GET_CLUB_LAMP_MODEL(), GET_CLUB_LIGHT_COORDS(eLocation), FALSE, FALSE)
	LightData.LampFrame = CREATE_OBJECT_NO_OFFSET(GET_CLUB_LAMP_FRAME_MODEL(), GET_CLUB_LAMP_FRAME_COORDS(eLocation), FALSE, FALSE)
	
	SET_ENTITY_ROTATION(LightData.Lamp, <<0.0, 0.0, GET_CLUB_LIGHT_HEADING(eLocation)>>)
	SET_ENTITY_ROTATION(LightData.LampFrame, <<0.0, 0.0, GET_CLUB_LIGHT_HEADING(eLocation)>>)
	
	FORCE_ROOM_FOR_ENTITY(LightData.Lamp, interiorID, iRoomKey)
	FORCE_ROOM_FOR_ENTITY(LightData.LampFrame, interiorID, iRoomKey)
	
	ATTACH_LAMP_FRAME_TO_LAMP(LightData)
	
	INT iLightType
	REPEAT MAX_CLUB_HANGING_LIGHT_TYPES iLightType
		LightData.LightID[iLightType] = CREATE_OBJECT_NO_OFFSET(GET_CLUB_HANGING_LIGHT_TYPE_MODEL(INT_TO_ENUM(CLUB_HANGING_LIGHT_TYPE, iLightType)), GET_CLUB_LIGHT_COORDS(eLocation), FALSE, FALSE, TRUE)
		
		SET_ENTITY_ROTATION(LightData.LightID[iLightType], <<0.0, 0.0, GET_CLUB_LIGHT_HEADING(eLocation)>>)
		
		SET_ENTITY_ALPHA(LightData.LightID[iLightType], 0, FALSE)
		
		FORCE_ROOM_FOR_ENTITY(LightData.LightID[iLightType], interiorID, iRoomKey)
		
		ATTACH_ENTITY_TO_ENTITY(LightData.LightID[iLightType], LightData.Lamp, -1, <<0.0, 0.0, LIGHT_ATTACHMENT_Z_AXIS_OFFSET>>, <<0.0, 0.0, 0.0>>)
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Initialises the club scripted lighting.
///    Needs to be called in the child script idle state, after the interior is ready.
/// PARAMS:
///    LightData - Lighting data to set.
///    eLocation - Lighting location to query.
///    interiorID - ID of the interior.
///    iRoomKey - Room key the lights are spawned in. Used to force light objects for room.
/// RETURNS: TRUE when initialisation is complete.
FUNC BOOL INITIALISE_CLUB_HANGING_LIGHTS(HANGING_LIGHT_DATA &LightData, CLUB_HANGING_LIGHT_LOCATION eLocation, INTERIOR_INSTANCE_INDEX interiorID, INT iRoomKey)
	IF IS_INTERIOR_READY(interiorID)
		MODEL_HIDE_INTERIOR_LIGHTING(eLocation)
		
		CREATE_CLUB_SCRIPTED_LIGHTING(LightData, eLocation, interiorID, iRoomKey)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC INITIALISE_CLUB_GRID_LIGHTS(LOCAL_GRID_LIGHT_DATA &LightData, SIMPLE_INTERIOR_DETAILS &sSimpleInteriorDetails, INTERIOR_INSTANCE_INDEX interiorID, INT iRoomKey, CLUB_DJS eDJ)
	REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_club_emis_rig_10")))
	
	IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_club_emis_rig_10")))
		IF NOT IS_BIT_SET(LightData.iBS, BS_CLUB_GRID_LIGHTS_CREATED)
			INT x, y
			REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_X x
				REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_Y y
					LightData.objLight[x][y] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_club_emis_rig_10")), GET_GRID_LIGHT_COORD(x, y, sSimpleInteriorDetails), FALSE, FALSE)
					
					IF IS_INTERIOR_READY(interiorID)
						FORCE_ROOM_FOR_ENTITY(LightData.objLight[x][y], interiorID, iRoomKey)
					ENDIF
					
					SET_ENTITY_ALPHA(LightData.objLight[x][y], 0, FALSE)
				ENDREPEAT
			ENDREPEAT
			
			REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_Y y
				LightData.fAlpha[y] = 0
			ENDREPEAT
			
			SET_BIT(LightData.iBS, BS_CLUB_GRID_LIGHTS_CREATED)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_club_emis_rig_10")))
		ENDIF
		
		SET_LIGHT_COLOUR_DATA(LightData.ColourData, CLUB_HANGING_LIGHT_INVALID, eDJ, LightData.iTintID)
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ HANGING SEQUENCES ╞═══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Increments the current light ID. Used for light sequences.
/// PARAMS:
///    LightData - Lighting data to set.
PROC INCREMENT_CURRENT_LIGHT_ID(LOCAL_HANGING_LIGHT_DATA &LightData)
	LightData.iCurrentLightID++
	
	IF (LightData.iCurrentLightID > MAX_CLUB_HANGING_LIGHT_OBJECTS - 1)
		LightData.iCurrentLightID = 0
	ENDIF
ENDPROC

/// PURPOSE:
///    Used to query if a particular bit is set across all lights.
/// PARAMS:
///    LightData - Lighting data to set.
///    iBS - The bit to query.
/// RETURNS: TRUE if a bit is set across all lights. FALSE otherwise.
FUNC BOOL ARE_CLUB_LIGHTS_IN_SYNC(HANGING_LIGHT_DATA &LightData[], INT iBS)
	INT iLightReady
	
	INT iLightID
	REPEAT MAX_CLUB_HANGING_LIGHT_OBJECTS iLightID
		IF IS_BIT_SET(LightData[iLightID].iBS, iBS)
			iLightReady++
		ENDIF
	ENDREPEAT
	
	RETURN (iLightReady = MAX_CLUB_HANGING_LIGHT_OBJECTS)
ENDFUNC

/// PURPOSE:
///    Sets the light rotation timer.
///    Wrapper in a bit set check so can be spammed.
/// PARAMS:
///    LightData - Lighting data to set.
///    iTimeMS - The time to set in milliseconds.
PROC SET_LIGHT_ROTATION_TIMER(HANGING_LIGHT_DATA &LightData, INT iTimeMS)
	IF NOT IS_BIT_SET(LightData.iBS, BS_CLUB_HANGING_LIGHTS_SET_ROTATION_TIMER)
		SET_BIT(LightData.iBS, BS_CLUB_HANGING_LIGHTS_SET_ROTATION_TIMER)
		
		LightData.tdLightRotationTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), iTimeMS)
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the time difference from the light rotation timer.
/// PARAMS:
///    LightData - Lighting data to query.
/// RETURNS: The timer difference from the light rotation timer in milliseconds.
FUNC INT GET_TIME_DIFFERENCE_FROM_LIGHT_ROTATION_TIMER(HANGING_LIGHT_DATA &LightData)
	RETURN ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), LightData.tdLightRotationTimer))
ENDFUNC

/// PURPOSE:
///    Clears the light rotation timer bit.
/// PARAMS:
///    LightData - Lighting data to query.
PROC CLEAR_LIGHT_ROTATION_TIMER(HANGING_LIGHT_DATA &LightData)
	CLEAR_BIT(LightData.iBS, BS_CLUB_HANGING_LIGHTS_SET_ROTATION_TIMER)
ENDPROC

/// PURPOSE:
///    Resets lights for next sequence.
/// PARAMS:
///    LightData - Lighting data to query.
PROC RESET_LIGHT_DATA(HANGING_LIGHT_DATA &LightData)
	CLEAR_BIT(LightData.iBS, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
	CLEAR_BIT(LightData.iBS, BS_CLUB_HANGING_LIGHTS_SET_COLOUR)
	CLEAR_BIT(LightData.iBS, BS_CLUB_HANGING_LIGHTS_SET_COLOUR_DURING_SEQUENCE)
	CLEAR_BIT(LightData.iBS, BS_CLUB_HANGING_LIGHTS_PREP_SETTING_ALPHA)
	CLEAR_BIT(LightData.iBS, BS_CLUB_HANGING_LIGHTS_SET_ALPHA)
	CLEAR_BIT(LightData.iBS, BS_CLUB_HANGING_LIGHTS_READY_FOR_SWEEP)
	CLEAR_BIT(LightData.iBS, BS_CLUB_HANGING_LIGHTS_DONE_SWEEP)
	CLEAR_BIT(LightData.iBS, BS_CLUB_HANGING_LIGHTS_STROBE_STARTED)
	CLEAR_BIT(LightData.iBS, BS_CLUB_HANGING_LIGHTS_ROTATION_INIT)
	CLEAR_BIT(LightData.iBS, BS_CLUB_HANGING_LIGHTS_FLASHING)
	CLEAR_BIT(LightData.iBS, BS_CLUB_HANGING_LIGHTS_FLASHING_COMPLETE)
	
	LightData.iBPM = 1
	LightData.iTypeCounter = 0
	LightData.iPulseCounter = 1
	LightData.iSequenceFlow = 0
	LightData.iColourCounter = 0
	LightData.iSequenceCounter = 0
ENDPROC

/// PURPOSE:
///    Performs a Mexican Wave effect. The lights turn one by one and cycle around the club. Starts off
///    spinning fast and decreases speed over time. Stops when lights have been lowered to their default rotations.
/// PARAMS:
///    LightData - Lighting data to set.
///    eLocation - Lighting location to query.
///    eSequence - Lighting sequence to set.
///    eDJ - The current DJ playing. Used to populate the colour scheme.
/// RETURNS: TRUE when the sequence has finished. FALSE otherwise.
FUNC BOOL PERFORM_CLUB_HANGING_LIGHT_SEQUENCE_MEXICAN_WAVE(LOCAL_HANGING_LIGHT_DATA &LightData, CLUB_HANGING_LIGHT_LOCATION eLocation, CLUB_HANGING_LIGHT_SEQUENCE eSequence, CLUB_DJS eDJ, NIGHTCLUB_AUDIO_TAGS eIntensity)
	FLOAT fAlpha
	
	INT iTimeDifference
	INT iLightID = ENUM_TO_INT(eLocation)
	
	SWITCH LightData.Data[iLightID].iSequenceFlow
		CASE 0
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
				// Set bit to inform other lights we are in this sequence
				SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
				
				RESET_NET_TIMER(LightData.Data[iLightID].stLightTimer)
				
				// Start light counter over
				LightData.iCurrentLightID = 0
				LightData.iColourLightCounter = 0
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 1
			// Wait until all lights are in the same sequence before starting
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
				SWITCH eIntensity
					CASE AUDIO_TAG_NULL
					CASE AUDIO_TAG_LOW
						LightData.Data[iLightID].fInitRotationTiming = cfMEXICAN_WAVE_INIT_ROTATION_TIMING_LOW
						LightData.Data[iLightID].fRotationTiming = cfMEXICAN_WAVE_ROTATION_TIMING_LOW
						LightData.Data[iLightID].fWaveTimingMax = cfMEXICAN_WAVE_MAX_UPDATE_TIME_LOW
						LightData.Data[iLightID].fWaveTimingMin = cfMEXICAN_WAVE_MIN_UPDATE_TIME_LOW
						LightData.Data[iLightID].fWaveTimer = cfMEXICAN_WAVE_TIMER_LOW
					BREAK
					
					CASE AUDIO_TAG_MEDIUM
						LightData.Data[iLightID].fInitRotationTiming = cfMEXICAN_WAVE_INIT_ROTATION_TIMING_MEDIUM
						LightData.Data[iLightID].fRotationTiming = cfMEXICAN_WAVE_ROTATION_TIMING_MEDIUM
						LightData.Data[iLightID].fWaveTimingMax = cfMEXICAN_WAVE_MAX_UPDATE_TIME_MEDIUM
						LightData.Data[iLightID].fWaveTimingMin = cfMEXICAN_WAVE_MIN_UPDATE_TIME_MEDIUM
						LightData.Data[iLightID].fWaveTimer = cfMEXICAN_WAVE_TIMER_MEDIUM
					BREAK
					
					CASE AUDIO_TAG_HIGH
					CASE AUDIO_TAG_HIGH_HANDS
						LightData.Data[iLightID].fInitRotationTiming = cfMEXICAN_WAVE_INIT_ROTATION_TIMING_HIGH
						LightData.Data[iLightID].fRotationTiming = cfMEXICAN_WAVE_ROTATION_TIMING_HIGH
						LightData.Data[iLightID].fWaveTimingMax = cfMEXICAN_WAVE_MAX_UPDATE_TIME_HIGH
						LightData.Data[iLightID].fWaveTimingMin = cfMEXICAN_WAVE_MIN_UPDATE_TIME_HIGH
						LightData.Data[iLightID].fWaveTimer = cfMEXICAN_WAVE_TIMER_HIGH
					BREAK
				ENDSWITCH
				
				CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 2
			// Hide all unused light types for this sequence
			ACTIVATE_SEQUENCE_LIGHT_TYPES(LightData.Data[iLightID], eSequence)
			
			LightData.Data[iLightID].iSequenceFlow++
		BREAK
		
		CASE 3
			// Point the lights towards the ceiling.
			DETACH_LAMP_FRAME_FROM_LAMP(LightData.Data[iLightID])
			
			IF UPDATE_SEQUENCE_LIGHT_ROTATION(LightData.Data[iLightID], FLOOR(LightData.Data[iLightID].fInitRotationTiming), GET_ENTITY_ROTATION(LightData.Data[iLightID].Lamp), <<0.0, -80.0, GET_CLUB_LIGHT_HEADING(eLocation)>>, TRUE)
				CLEAR_LIGHT_ROTATION_TIMER(LightData.Data[iLightID])
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 4
			// Set the light colour data
			SET_LIGHT_COLOUR_DATA(LightData.Data[iLightID].ColourData, eLocation, eDJ)
			
			LightData.Data[iLightID].iSequenceFlow++
		BREAK
		
		CASE 5
			// Set the light colour
			IF SET_LIGHT_COLOUR(LightData, eLocation, CLUB_HANGING_LIGHT_TYPE_THIN_STRONG_SPOT_LIGHT)
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 6
			// Wait until every light has been coloured before continuing
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_COLOUR)
				CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 7
			// Perform the Mexican Wave effect
			IF (iLightID = LightData.iCurrentLightID)
				// Set rotation timer to decrease spin speed over time
				SET_LIGHT_ROTATION_TIMER(LightData.Data[iLightID], FLOOR(LightData.Data[iLightID].fWaveTimer))
				
				iTimeDifference = GET_TIME_DIFFERENCE_FROM_LIGHT_ROTATION_TIMER(LightData.Data[iLightID])
				
				fAlpha = (LightData.Data[iLightID].fWaveTimingMax - ((TO_FLOAT(iTimeDifference) / LightData.Data[iLightID].fWaveTimer) * LightData.Data[iLightID].fWaveTimingMax))
				
				IF fAlpha < LightData.Data[iLightID].fWaveTimingMin
					fAlpha = LightData.Data[iLightID].fWaveTimingMin
				ELIF fAlpha > LightData.Data[iLightID].fWaveTimingMax
					fAlpha = LightData.Data[iLightID].fWaveTimingMax
				ENDIF
				
				SET_LIGHT_ALPHA(LightData.Data[iLightID], CLUB_HANGING_LIGHT_TYPE_THIN_STRONG_SPOT_LIGHT, 255)
				
				IF HAS_NET_TIMER_EXPIRED(LightData.Data[iLightID].stLightTimer, ROUND(fAlpha))
					SET_LIGHT_ALPHA(LightData.Data[iLightID], CLUB_HANGING_LIGHT_TYPE_THIN_STRONG_SPOT_LIGHT, 0)
					
					RESET_NET_TIMER(LightData.Data[iLightID].stLightTimer)
					
					INCREMENT_CURRENT_LIGHT_ID(LightData)
				ENDIF
			ENDIF
			
			// Lower lights over time
			IF UPDATE_SEQUENCE_LIGHT_ROTATION(LightData.Data[iLightID], FLOOR(LightData.Data[iLightID].fRotationTiming), <<0.0, -80.0, GET_CLUB_LIGHT_HEADING(eLocation)>>, <<0.0, 0.0, GET_CLUB_LIGHT_HEADING(eLocation)>>)
				SET_LIGHT_ALPHA(LightData.Data[iLightID], CLUB_HANGING_LIGHT_TYPE_THIN_STRONG_SPOT_LIGHT, 0)
				
				RESET_NET_TIMER(LightData.Data[iLightID].stLightTimer)
				
				CLEAR_LIGHT_ROTATION_TIMER(LightData.Data[iLightID])
				
				// Set bit to inform other lights we are done this sequence
				SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 8
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
				RESET_LIGHT_DATA(LightData.Data[iLightID])
				
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Performs a Circumference Point Circle effect. All lights follow the circumference point of a circle.
///    Colours swap between primary and tertiary per every two bar.
/// PARAMS:
///    LightData - Lighting data to set.
///    eLocation - Lighting location to query.
///    eSequence - Lighting sequence to set.
///    eDJ - The current DJ playing. Used to populate the colour scheme.
/// RETURNS: TRUE when the sequence has finished. FALSE otherwise.
FUNC BOOL PERFORM_CLUB_HANGING_LIGHT_SEQUENCE_CIRCUMFERENCE_POINT_CIRCLE(LOCAL_HANGING_LIGHT_DATA &LightData, CLUB_HANGING_LIGHT_LOCATION eLocation, CLUB_HANGING_LIGHT_SEQUENCE eSequence, CLUB_DJS eDJ, NIGHTCLUB_AUDIO_TAGS eIntensity)
	FLOAT fDist
	FLOAT fAlpha
	FLOAT fHeight
	FLOAT fZHeading
	FLOAT fPitchToCoord
	
	INT iLightID = ENUM_TO_INT(eLocation)
	INT iTimeDifference
	
	VECTOR vLightRot
	VECTOR vLightCoord
	VECTOR vCircumference
	
	SWITCH LightData.Data[iLightID].iSequenceFlow
		CASE 0
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
				// Set bit to inform other lights we are in this sequence
				SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
				
				RESET_NET_TIMER(LightData.Data[iLightID].stLightTimer)
				
				// Start light counter over
				LightData.iCurrentLightID = 0
				LightData.iColourLightCounter = 0
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 1
			// Wait until all lights are in the same sequence before starting
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
				SWITCH eIntensity
					CASE AUDIO_TAG_NULL
					CASE AUDIO_TAG_LOW
						LightData.Data[iLightID].fInitRotationTiming = cfCIRCUMFERENCE_INIT_ROTATION_TIMING_LOW
						LightData.Data[iLightID].fRotationTiming = cfCIRCUMFERENCE_ROTATION_TIMING_LOW
					BREAK
					
					CASE AUDIO_TAG_MEDIUM
						LightData.Data[iLightID].fInitRotationTiming = cfCIRCUMFERENCE_INIT_ROTATION_TIMING_MEDIUM
						LightData.Data[iLightID].fRotationTiming = cfCIRCUMFERENCE_ROTATION_TIMING_MEDIUM
					BREAK
					
					CASE AUDIO_TAG_HIGH
					CASE AUDIO_TAG_HIGH_HANDS
						LightData.Data[iLightID].fInitRotationTiming = cfCIRCUMFERENCE_INIT_ROTATION_TIMING_HIGH
						LightData.Data[iLightID].fRotationTiming = cfCIRCUMFERENCE_ROTATION_TIMING_HIGH
					BREAK
				ENDSWITCH
				
				CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 2
			// Hide all unused light types for this sequence
			ACTIVATE_SEQUENCE_LIGHT_TYPES(LightData.Data[iLightID], eSequence)
			
			// Hide used lights at the start to mask jump in rotation
			SET_LIGHT_ALPHA(LightData.Data[iLightID], CLUB_HANGING_LIGHT_TYPE_THIN_STRONG_SPOT_LIGHT, 0)
			
			LightData.Data[iLightID].iSequenceFlow++
		BREAK
		
		CASE 3
			DETACH_LAMP_FRAME_FROM_LAMP(LightData.Data[iLightID])
			
			// Reset the light rotation to starting position
			IF UPDATE_SEQUENCE_LIGHT_ROTATION(LightData.Data[iLightID], FLOOR(LightData.Data[iLightID].fInitRotationTiming), GET_ENTITY_ROTATION(LightData.Data[iLightID].Lamp), <<0.0, 0.0, GET_CLUB_LIGHT_HEADING(eLocation)>>, TRUE)
				CLEAR_LIGHT_ROTATION_TIMER(LightData.Data[iLightID])
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 4
			// Set the light colour data
			SET_LIGHT_COLOUR_DATA(LightData.Data[iLightID].ColourData, eLocation, eDJ, LightData.Data[iLightID].iColourCounter)
			
			LightData.Data[iLightID].iSequenceFlow++
		BREAK
		
		CASE 5
			// Set the light colour
			IF SET_LIGHT_COLOUR(LightData, eLocation, CLUB_HANGING_LIGHT_TYPE_THIN_STRONG_SPOT_LIGHT)
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 6
			// Wait until every light has been coloured before continuing
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_COLOUR)
				CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 7
			// Perform the Circumference Point Circle effect
			
			// Set light tint colour on beat
			IF IS_MUSIC_ON_THIS_BEAT(LightData.Data[iLightID], 1, 2)
				IF NOT IS_BIT_SET(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COLOUR_DURING_SEQUENCE)
					LightData.Data[iLightID].iColourCounter += 2
					
					IF LightData.Data[iLightID].iColourCounter > 2
						LightData.Data[iLightID].iColourCounter = 0
					ENDIF
					
					SET_LIGHT_COLOUR_DATA(LightData.Data[iLightID].ColourData, eLocation, eDJ, LightData.Data[iLightID].iColourCounter)
					
					SET_LIGHT_COLOUR(LightData, eLocation, CLUB_HANGING_LIGHT_TYPE_THIN_STRONG_SPOT_LIGHT, TRUE)
					
					SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COLOUR_DURING_SEQUENCE)
				ENDIF
			ELSE
				IF IS_BIT_SET(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COLOUR_DURING_SEQUENCE)
					CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COLOUR_DURING_SEQUENCE)
				ENDIF
			ENDIF
			
			// Get time offset
			IF NOT IS_BIT_SET(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_ROTATION_TIMER)
				SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_ROTATION_TIMER)
				
				LightData.Data[iLightID].tdLightRotationTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), FLOOR(LightData.Data[iLightID].fRotationTiming))
				
				LightData.Data[iLightID].iSequenceCounter++
			ENDIF
			
			// Calculate alpha based on time
			iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), LightData.Data[iLightID].tdLightRotationTimer))
			
			fAlpha = (360.0 - ((TO_FLOAT(iTimeDifference) / LightData.Data[iLightID].fRotationTiming) * 360.0))
			
			// Get current circumference point
			vCircumference = GET_POINT_ON_CIRCUMFERENCE(<<1558.3, 249.9, -49.0>>, 3.0, fAlpha) // TODO: Abstract dance floor coords for future clubs
			
			// Clamp alpha between 180 & -180
			IF fAlpha > 180.0
				fAlpha -= 360.0
			ELIF fAlpha < -180.0
				fAlpha += 360.0
			ENDIF
			
			// Reset alpha as the time difference reaches 0. 
			// This will make the lights follow a continuous circle.
			IF (fAlpha < 0.0 AND fAlpha >= -5.0)
			OR iTimeDifference <= 20.0
				IF IS_BIT_SET(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_ROTATION_TIMER)
					CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_ROTATION_TIMER)
				ENDIF
			ENDIF
			
			vLightCoord = GET_CLUB_LIGHT_COORDS(eLocation)
			vLightRot = <<0.0, 0.0, GET_CLUB_LIGHT_HEADING(eLocation)>>
			
			fDist = GET_DISTANCE_BETWEEN_COORDS(<<vLightCoord.x, vLightCoord.y, -79.0>>, <<vCircumference.x, vCircumference.y, -79.0>>)
			fHeight = vCircumference.z - vLightCoord.z
			
			IF fDist > 0
				fPitchToCoord = ATAN(fHeight / fDist)
			ELSE
				fPitchToCoord = 0
			ENDIF
			
			fZHeading = ATAN2((vCircumference.y - vLightCoord.y), (vCircumference.x - vLightCoord.x))
			
			// Rotate to current circumference point
			SET_ENTITY_ROTATION(LightData.Data[iLightID].Lamp, <<vLightRot.x, (vLightRot.y - fPitchToCoord) - 18.0, fZHeading>>)
			SET_ENTITY_ROTATION(LightData.Data[iLightID].LampFrame, <<vLightRot.x, vLightRot.y, fZHeading>>)
			
			// Only show lights once they have started rotating
			IF NOT IS_BIT_SET(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_PREP_SETTING_ALPHA)
				SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_PREP_SETTING_ALPHA)
			ELIF NOT IS_BIT_SET(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_ALPHA)
				SET_LIGHT_ALPHA(LightData.Data[iLightID], CLUB_HANGING_LIGHT_TYPE_THIN_STRONG_SPOT_LIGHT, 255)
				
				SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_ALPHA)
			ENDIF
			
			// Sequence complete after specified number of rotations
			IF LightData.Data[iLightID].iSequenceCounter >= GET_SEQUENCE_TOTAL(eSequence, eIntensity)
				SET_LIGHT_ALPHA(LightData.Data[iLightID], CLUB_HANGING_LIGHT_TYPE_THIN_STRONG_SPOT_LIGHT, 0)
				
				RESET_NET_TIMER(LightData.Data[iLightID].stLightTimer)
				
				CLEAR_LIGHT_ROTATION_TIMER(LightData.Data[iLightID])
				
				// Set bit to inform other lights we are done this sequence
				SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 8
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
				RESET_LIGHT_DATA(LightData.Data[iLightID])
				
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Performs a Sweep Up effect. Turn each light on per beat pointed down low.
///    Sweep all lights up through crowd and turn lights off at top.
///    Start again at bottom and continue. Use secondary and tertiary colours.
///    Change lighting type each rotation.
/// PARAMS:
///    LightData - Lighting data to set.
///    eLocation - Lighting location to query.
///    eSequence - Lighting sequence to set.
///    eDJ - The current DJ playing. Used to populate the colour scheme.
/// RETURNS: TRUE when the sequence has finished. FALSE otherwise.
FUNC BOOL PERFORM_CLUB_HANGING_LIGHT_SEQUENCE_SWEEP_UP(LOCAL_HANGING_LIGHT_DATA &LightData, CLUB_HANGING_LIGHT_LOCATION eLocation, CLUB_HANGING_LIGHT_SEQUENCE eSequence, CLUB_DJS eDJ, NIGHTCLUB_AUDIO_TAGS eIntensity)
	FLOAT fYOffset
	
	INT iLightID = ENUM_TO_INT(eLocation)
	
	SWITCH LightData.Data[iLightID].iSequenceFlow
		CASE 0
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
				// Set bit to inform other lights we are in this sequence
				SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
				
				RESET_NET_TIMER(LightData.Data[iLightID].stLightTimer)
				
				// Start light counter over
				LightData.iCurrentLightID = 0
				LightData.iColourLightCounter = 0
				
				LightData.Data[iLightID].iColourCounter = 1
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 1
			// Wait until all lights are in the same sequence before starting
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
				SWITCH eIntensity
					CASE AUDIO_TAG_NULL
					CASE AUDIO_TAG_LOW
						LightData.Data[iLightID].fInitRotationTiming = cfSWEEP_UP_INIT_ROTATION_TIMING_LOW
						LightData.Data[iLightID].fRotationTiming = cfSWEEP_UP_ROTATION_TIMING_LOW
						LightData.Data[iLightID].fWaveTimingMax = cfSWEEP_UP_MEXICAN_WAVE_ON_TIMING_LOW
						LightData.Data[iLightID].fWaveTimingMin = cfSWEEP_UP_MEXICAN_WAVE_OFF_TIMING_LOW
					BREAK
					
					CASE AUDIO_TAG_MEDIUM
						LightData.Data[iLightID].fInitRotationTiming = cfSWEEP_UP_INIT_ROTATION_TIMING_MEDIUM
						LightData.Data[iLightID].fRotationTiming = cfSWEEP_UP_ROTATION_TIMING_MEDIUM
						LightData.Data[iLightID].fWaveTimingMax = cfSWEEP_UP_MEXICAN_WAVE_ON_TIMING_MEDIUM
						LightData.Data[iLightID].fWaveTimingMin = cfSWEEP_UP_MEXICAN_WAVE_OFF_TIMING_MEDIUM
					BREAK
					
					CASE AUDIO_TAG_HIGH
					CASE AUDIO_TAG_HIGH_HANDS
						LightData.Data[iLightID].fInitRotationTiming = cfSWEEP_UP_INIT_ROTATION_TIMING_HIGH
						LightData.Data[iLightID].fRotationTiming = cfSWEEP_UP_ROTATION_TIMING_HIGH
						LightData.Data[iLightID].fWaveTimingMax = cfSWEEP_UP_MEXICAN_WAVE_ON_TIMING_HIGH
						LightData.Data[iLightID].fWaveTimingMin = cfSWEEP_UP_MEXICAN_WAVE_OFF_TIMING_HIGH
					BREAK
				ENDSWITCH
				
				CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 2
			// Hide all unused light types for this sequence
			ACTIVATE_SEQUENCE_LIGHT_TYPES(LightData.Data[iLightID], eSequence)
			
			// Hide used lights at the start
			SET_LIGHT_ALPHA(LightData.Data[iLightID], GET_VARIED_LIGHT_TYPE(LightData.Data[iLightID]), 0)
			
			LightData.Data[iLightID].iSequenceFlow++
		BREAK
		
		CASE 3
			// Point the lights towards the floor.
			DETACH_LAMP_FRAME_FROM_LAMP(LightData.Data[iLightID])
			
			fYOffset = 40.0
			
			IF iLightID = 4
			OR iLightID = 5
				fYOffset = 20.0
			ENDIF
			
			IF UPDATE_SEQUENCE_LIGHT_ROTATION(LightData.Data[iLightID], FLOOR(LightData.Data[iLightID].fInitRotationTiming), GET_ENTITY_ROTATION(LightData.Data[iLightID].Lamp), <<0.0, fYOffset, GET_CLUB_LIGHT_HEADING(eLocation)>>, TRUE)
				CLEAR_LIGHT_ROTATION_TIMER(LightData.Data[iLightID])
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 4
			// Set the light colour data
			SET_LIGHT_COLOUR_DATA(LightData.Data[iLightID].ColourData, eLocation, eDJ, LightData.Data[iLightID].iColourCounter)
			
			LightData.Data[iLightID].iSequenceFlow++
		BREAK
		
		CASE 5
			// Set the light colour
			IF SET_LIGHT_COLOUR(LightData, eLocation, GET_VARIED_LIGHT_TYPE(LightData.Data[iLightID]))
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 6
			// Wait until every light has been coloured before continuing
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_COLOUR)
				CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 7
			// Perform the Mexican Wave effect to turn on
			IF (iLightID = LightData.iCurrentLightID)
				IF HAS_NET_TIMER_EXPIRED(LightData.Data[iLightID].stLightTimer, FLOOR(LightData.Data[iLightID].fWaveTimingMax))
					SET_LIGHT_ALPHA(LightData.Data[iLightID], GET_VARIED_LIGHT_TYPE(LightData.Data[iLightID]), 255)
					
					RESET_NET_TIMER(LightData.Data[iLightID].stLightTimer)
					
					INCREMENT_CURRENT_LIGHT_ID(LightData)
					
					SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_READY_FOR_SWEEP)
					
					LightData.Data[iLightID].iSequenceFlow++
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			// Wait until all lights are in the same sequence before starting
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_READY_FOR_SWEEP)
				CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_DONE_SWEEP)
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 9
			fYOffset = 40.0
			
			IF iLightID = 4
			OR iLightID = 5
				fYOffset = 20.0
			ENDIF
			
			// Sweep up
			IF UPDATE_SEQUENCE_LIGHT_ROTATION(LightData.Data[iLightID], FLOOR(LightData.Data[iLightID].fRotationTiming), <<0.0, fYOffset, GET_CLUB_LIGHT_HEADING(eLocation)>>, <<0.0, -100.0, GET_CLUB_LIGHT_HEADING(eLocation)>>)
				SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_DONE_SWEEP)
				
				LightData.Data[iLightID].iSequenceCounter++
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 10
			// Wait until all lights are in the same sequence before starting
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_DONE_SWEEP)
				CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_READY_FOR_SWEEP)
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 11
			// Perform the Mexican Wave effect to turn off
			IF (iLightID = LightData.iCurrentLightID)
				IF HAS_NET_TIMER_EXPIRED(LightData.Data[iLightID].stLightTimer, FLOOR(LightData.Data[iLightID].fWaveTimingMin))
					SET_LIGHT_ALPHA(LightData.Data[iLightID], GET_VARIED_LIGHT_TYPE(LightData.Data[iLightID]), 0)
					
					RESET_NET_TIMER(LightData.Data[iLightID].stLightTimer)
					
					CLEAR_LIGHT_ROTATION_TIMER(LightData.Data[iLightID])
					
					LightData.Data[iLightID].iTypeCounter++
					
					IF LightData.Data[iLightID].iTypeCounter >= 3
						LightData.Data[iLightID].iTypeCounter = 0
					ENDIF
					
					LightData.Data[iLightID].iColourCounter++
					
					IF LightData.Data[iLightID].iColourCounter > 2
						LightData.Data[iLightID].iColourCounter = 1
					ENDIF
					
					INCREMENT_CURRENT_LIGHT_ID(LightData)
					
					IF LightData.Data[iLightID].iSequenceCounter >= GET_SEQUENCE_TOTAL(eSequence)
						// Set bit to inform other lights we are done this sequence
						SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
						
						LightData.Data[iLightID].iSequenceFlow++
					ELSE
						SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
						
						LightData.Data[iLightID].iSequenceFlow = 1
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 12
			IF IS_BEAT_AT_START_OF_BAR()
			AND ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
				RESET_LIGHT_DATA(LightData.Data[iLightID])
				
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Performs a Vertical Strobe effect.
///    All lights move vertically up and down with each beat while strobing, use all colours.
/// PARAMS:
///    LightData - Lighting data to set.
///    eLocation - Lighting location to query.
///    eSequence - Lighting sequence to set.
///    eDJ - The current DJ playing. Used to populate the colour scheme.
/// RETURNS: TRUE when the sequence has finished. FALSE otherwise.
FUNC BOOL PERFORM_CLUB_HANGING_LIGHT_SEQUENCE_VERTICAL_STROBE(LOCAL_HANGING_LIGHT_DATA &LightData, CLUB_HANGING_LIGHT_LOCATION eLocation, CLUB_HANGING_LIGHT_SEQUENCE eSequence, CLUB_DJS eDJ, NIGHTCLUB_AUDIO_TAGS eIntensity)
	INT iLightID = ENUM_TO_INT(eLocation)
	
	SWITCH LightData.Data[iLightID].iSequenceFlow
		CASE 0
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
				// Set bit to inform other lights we are in this sequence
				SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
				
				RESET_NET_TIMER(LightData.Data[iLightID].stLightTimer)
				
				// Start light counter over
				LightData.iCurrentLightID = 0
				LightData.iColourLightCounter = 0
				
				LightData.Data[iLightID].iColourCounter = 0
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 1
			// Wait until all lights are in the same sequence before starting
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
				SWITCH eIntensity
					CASE AUDIO_TAG_NULL
					CASE AUDIO_TAG_LOW
						LightData.Data[iLightID].fInitRotationTiming = cfVERTICAL_STROBE_INIT_ROTATION_TIMING_LOW
						LightData.Data[iLightID].fRotationTiming = cfVERTICAL_STROBE_ROTATION_TIMING_LOW
					BREAK
					
					CASE AUDIO_TAG_MEDIUM
						LightData.Data[iLightID].fInitRotationTiming = cfVERTICAL_STROBE_INIT_ROTATION_TIMING_MEDIUM
						LightData.Data[iLightID].fRotationTiming = cfVERTICAL_STROBE_ROTATION_TIMING_MEDIUM
					BREAK
					
					CASE AUDIO_TAG_HIGH
					CASE AUDIO_TAG_HIGH_HANDS
						LightData.Data[iLightID].fInitRotationTiming = cfVERTICAL_STROBE_INIT_ROTATION_TIMING_HIGH
						LightData.Data[iLightID].fRotationTiming = cfVERTICAL_STROBE_ROTATION_TIMING_HIGH
					BREAK
				ENDSWITCH
				
				CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 2
			// Hide all unused light types for this sequence
			ACTIVATE_SEQUENCE_LIGHT_TYPES(LightData.Data[iLightID], eSequence)
			
			// Hide lights until colour is set
			IF NOT IS_BIT_SET(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_STROBE_STARTED)
				SET_LIGHT_ALPHA(LightData.Data[iLightID], CLUB_HANGING_LIGHT_TYPE_SOFT_SPOT_LIGHT, 0)
			ENDIF
			
			LightData.Data[iLightID].iSequenceFlow++
		BREAK
		
		CASE 3
			// Point the lights towards the floor.
			DETACH_LAMP_FRAME_FROM_LAMP(LightData.Data[iLightID])
			
			IF UPDATE_SEQUENCE_LIGHT_ROTATION(LightData.Data[iLightID], FLOOR(LightData.Data[iLightID].fInitRotationTiming), GET_ENTITY_ROTATION(LightData.Data[iLightID].Lamp), <<0.0, 40.0, GET_CLUB_LIGHT_HEADING(eLocation)>>, TRUE)
				CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COLOUR)
				
				CLEAR_LIGHT_ROTATION_TIMER(LightData.Data[iLightID])
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 4
			// Set the light colour data
			SET_LIGHT_COLOUR_DATA(LightData.Data[iLightID].ColourData, eLocation, eDJ, LightData.Data[iLightID].iColourCounter)
			
			LightData.Data[iLightID].iSequenceFlow++
		BREAK
		
		CASE 5
			// Set the light colour
			IF SET_LIGHT_COLOUR(LightData, eLocation, CLUB_HANGING_LIGHT_TYPE_SOFT_SPOT_LIGHT)
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 6
			// Wait until every light has been coloured before continuing
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_COLOUR)
				CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
				
				SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_STROBE_STARTED)
				
				SET_LIGHT_ALPHA(LightData.Data[iLightID], CLUB_HANGING_LIGHT_TYPE_SOFT_SPOT_LIGHT, 255)
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 7
			// Lower lights over time
			IF UPDATE_SEQUENCE_LIGHT_ROTATION(LightData.Data[iLightID], FLOOR(LightData.Data[iLightID].fRotationTiming), <<0.0, 40.0, GET_CLUB_LIGHT_HEADING(eLocation)>>, <<0.0, -100.0, GET_CLUB_LIGHT_HEADING(eLocation)>>, TRUE)
				LightData.Data[iLightID].iSequenceCounter++
				
				LightData.Data[iLightID].iColourCounter++
				
				IF LightData.Data[iLightID].iColourCounter > 2
					LightData.Data[iLightID].iColourCounter = 0
				ENDIF
				
				IF LightData.Data[iLightID].iSequenceCounter >= GET_SEQUENCE_TOTAL(eSequence)
					SET_LIGHT_ALPHA(LightData.Data[iLightID], CLUB_HANGING_LIGHT_TYPE_SOFT_SPOT_LIGHT, 0)
					
					RESET_NET_TIMER(LightData.Data[iLightID].stLightTimer)
					
					CLEAR_LIGHT_ROTATION_TIMER(LightData.Data[iLightID])
					
					// Set bit to inform other lights we are done this sequence
					SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
					
					LightData.Data[iLightID].iSequenceFlow++
				ELSE
					RESET_NET_TIMER(LightData.Data[iLightID].stLightTimer)
					
					CLEAR_LIGHT_ROTATION_TIMER(LightData.Data[iLightID])
					
					SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
					
					LightData.Data[iLightID].iSequenceFlow = 1
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			IF IS_BEAT_AT_START_OF_BAR()
			AND ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
				RESET_LIGHT_DATA(LightData.Data[iLightID])
				
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_LIGHT_SHINE_FOR_SPLIT_LIGHT_EFFECT(INT iLightID)
	SWITCH iLightID
		CASE 0
		CASE 2
		CASE 4
		CASE 7
		CASE 8
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Performs a Random Flash effect.
///    Half lights turn off per beat and moves to a new random rotation. Other half flash on.
/// PARAMS:
///    LightData - Lighting data to set.
///    eLocation - Lighting location to query.
///    eSequence - Lighting sequence to set.
///    eDJ - The current DJ playing. Used to populate the colour scheme.
/// RETURNS: TRUE when the sequence has finished. FALSE otherwise.
FUNC BOOL PERFORM_CLUB_HANGING_LIGHT_SEQUENCE_RANDOM_FLASH(LOCAL_HANGING_LIGHT_DATA &LightData, CLUB_HANGING_LIGHT_LOCATION eLocation, CLUB_HANGING_LIGHT_SEQUENCE eSequence, CLUB_DJS eDJ, NIGHTCLUB_AUDIO_TAGS eIntensity)
	INT iLightID = ENUM_TO_INT(eLocation)
	
	SWITCH LightData.Data[iLightID].iSequenceFlow
		CASE 0
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
				// Set bit to inform other lights we are in this sequence
				SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
				
				RESET_NET_TIMER(LightData.Data[iLightID].stLightTimer)
				
				// Start light counter over
				LightData.iCurrentLightID = 0
				LightData.iColourLightCounter = 0
				
				LightData.Data[iLightID].iColourCounter = 0
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 1
			// Wait until all lights are in the same sequence before starting
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
				SWITCH eIntensity
					CASE AUDIO_TAG_NULL
					CASE AUDIO_TAG_LOW
						LightData.Data[iLightID].fInitRotationTiming = cfRANDOM_FLASH_INIT_ROTATION_TIMING_LOW
						LightData.Data[iLightID].fRotationTiming = cfRANDOM_FLASH_ROTATION_TIMING_LOW
					BREAK
					
					CASE AUDIO_TAG_MEDIUM
						LightData.Data[iLightID].fInitRotationTiming = cfRANDOM_FLASH_INIT_ROTATION_TIMING_MEDIUM
						LightData.Data[iLightID].fRotationTiming = cfRANDOM_FLASH_ROTATION_TIMING_MEDIUM
					BREAK
					
					CASE AUDIO_TAG_HIGH
					CASE AUDIO_TAG_HIGH_HANDS
						LightData.Data[iLightID].fInitRotationTiming = cfRANDOM_FLASH_INIT_ROTATION_TIMING_HIGH
						LightData.Data[iLightID].fRotationTiming = cfRANDOM_FLASH_ROTATION_TIMING_HIGH
					BREAK
				ENDSWITCH
				
				CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COLOUR)
				CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 2
			// Hide all unused light types for this sequence
			ACTIVATE_SEQUENCE_LIGHT_TYPES(LightData.Data[iLightID], eSequence)
			
			// Hide used lights at the start
			SET_LIGHT_ALPHA(LightData.Data[iLightID], GET_VARIED_LIGHT_TYPE(LightData.Data[iLightID]), 0)
			
			LightData.Data[iLightID].iSequenceFlow++
		BREAK
		
		CASE 3
			DETACH_LAMP_FRAME_FROM_LAMP(LightData.Data[iLightID])
			
			IF NOT IS_BIT_SET(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_ROTATION_INIT)
				IF UPDATE_SEQUENCE_LIGHT_ROTATION(LightData.Data[iLightID], FLOOR(LightData.Data[iLightID].fInitRotationTiming), GET_ENTITY_ROTATION(LightData.Data[iLightID].Lamp), <<0.0, 0.0, GET_CLUB_LIGHT_HEADING(eLocation)>>, TRUE)
					CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COLOUR)
					
					CLEAR_LIGHT_ROTATION_TIMER(LightData.Data[iLightID])
					
					SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_ROTATION_INIT)
					
					LightData.Data[iLightID].iSequenceFlow++
				ENDIF
			ELSE
				CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COLOUR)
				
				CLEAR_LIGHT_ROTATION_TIMER(LightData.Data[iLightID])
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 4
			// Set the light colour data
			SET_LIGHT_COLOUR_DATA(LightData.Data[iLightID].ColourData, eLocation, eDJ, LightData.Data[iLightID].iColourCounter)
			
			CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_FLASHING_COMPLETE)
			
			LightData.Data[iLightID].iSequenceFlow++
		BREAK
		
		CASE 5
			// Set the light colour
			IF SET_LIGHT_COLOUR(LightData, eLocation, GET_VARIED_LIGHT_TYPE(LightData.Data[iLightID]))
				IF LightData.Data[iLightID].iSequenceCounter % 2 = 0
					IF SHOULD_LIGHT_SHINE_FOR_SPLIT_LIGHT_EFFECT(iLightID)
						SET_LIGHT_ALPHA(LightData.Data[iLightID], GET_VARIED_LIGHT_TYPE(LightData.Data[iLightID]), 255)
						
						SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_FLASHING)
					ELSE
						SET_LIGHT_ALPHA(LightData.Data[iLightID], GET_VARIED_LIGHT_TYPE(LightData.Data[iLightID]), 0)
						
						CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_FLASHING)
					ENDIF
				ELSE
					IF NOT SHOULD_LIGHT_SHINE_FOR_SPLIT_LIGHT_EFFECT(iLightID)
						SET_LIGHT_ALPHA(LightData.Data[iLightID], GET_VARIED_LIGHT_TYPE(LightData.Data[iLightID]), 255)
						
						SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_FLASHING)
					ELSE
						SET_LIGHT_ALPHA(LightData.Data[iLightID], GET_VARIED_LIGHT_TYPE(LightData.Data[iLightID]), 0)
						
						CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_FLASHING)
					ENDIF
				ENDIF
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 6
			// Wait until every light has been coloured before continuing
			IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_COLOUR)
				CLEAR_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
				
				LightData.Data[iLightID].iSequenceFlow++
			ENDIF
		BREAK
		
		CASE 7
			// Move lights to random rotation
			IF IS_BIT_SET(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_FLASHING)
			OR UPDATE_SEQUENCE_LIGHT_ROTATION(LightData.Data[iLightID], FLOOR(LightData.Data[iLightID].fRotationTiming), GET_ENTITY_ROTATION(LightData.Data[iLightID].Lamp), GET_RANDOM_LIGHT_ROTATION(eLocation, LightData.Data[iLightID].iRandomRotationCounter), TRUE)
				SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_FLASHING_COMPLETE)
				
				IF ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_FLASHING_COMPLETE)
					LightData.Data[iLightID].iSequenceCounter++
					LightData.Data[iLightID].iRandomRotationCounter++
					
					IF LightData.Data[iLightID].iRandomRotationCounter > 4
						LightData.Data[iLightID].iRandomRotationCounter = 0
					ENDIF
					
					IF LightData.Data[iLightID].iSequenceCounter = 8
					OR LightData.Data[iLightID].iSequenceCounter = 16
					OR LightData.Data[iLightID].iSequenceCounter = 24
						LightData.Data[iLightID].iColourCounter++
						
						IF LightData.Data[iLightID].iColourCounter > 2
							LightData.Data[iLightID].iColourCounter = 0
						ENDIF
						
						LightData.Data[iLightID].iTypeCounter++
						
						IF LightData.Data[iLightID].iTypeCounter >= 3
							LightData.Data[iLightID].iTypeCounter = 0
						ENDIF
					ENDIF
					
					IF LightData.Data[iLightID].iSequenceCounter >= GET_SEQUENCE_TOTAL(eSequence)
						SET_LIGHT_ALPHA(LightData.Data[iLightID], GET_VARIED_LIGHT_TYPE(LightData.Data[iLightID]), 0)
						
						RESET_NET_TIMER(LightData.Data[iLightID].stLightTimer)
						
						CLEAR_LIGHT_ROTATION_TIMER(LightData.Data[iLightID])
						
						// Set bit to inform other lights we are done this sequence
						SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
						
						LightData.Data[iLightID].iSequenceFlow++
					ELSE
						RESET_NET_TIMER(LightData.Data[iLightID].stLightTimer)
						
						CLEAR_LIGHT_ROTATION_TIMER(LightData.Data[iLightID])
						
						SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_INSIDE_SEQUENCE)
						
						LightData.Data[iLightID].iSequenceFlow = 1
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			IF IS_BEAT_AT_START_OF_BAR()
			AND ARE_CLUB_LIGHTS_IN_SYNC(LightData.Data, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
				RESET_LIGHT_DATA(LightData.Data[iLightID])
				
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ GRID SEQUENCES ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC RESET_GRID_LIGHT_RAIN_DATA(LOCAL_GRID_LIGHT_DATA &LightData, FLOAT fAlpha)
	INT x
	REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_X x
		INT y
		REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_Y y
			LightData.fRainAlpha[x][y] = fAlpha
			
			LightData.bRainFadeFlag[x][y] = TRUE
			
			LightData.iGridLightFadeOffset[x][y] = 0
		ENDREPEAT
	ENDREPEAT
	
	LightData.iGridLightRainBS = 0
	
	LightData.iRainCurrentGroup = 1
	
	RESET_NET_TIMER(LightData.stActiveTimer)
ENDPROC

PROC RESET_GRID_LIGHT_DATA(LOCAL_GRID_LIGHT_DATA &LightData, FLOAT fAlpha)
	INT y
	REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_Y y
		LightData.fAlpha[y] = fAlpha
		
		LightData.bFadeLight[y] = FALSE
		
		IF IS_BIT_SET(LightData.iBS, GET_GRID_LIGHT_FADE_BS(y))
			CLEAR_BIT(LightData.iBS, GET_GRID_LIGHT_FADE_BS(y))
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLEAR_GRID_LIGHT_TINT_BITSETS(LOCAL_GRID_LIGHT_DATA &LightData)
	INT x
	REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_X x
		INT y
		REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_Y y
			IF IS_GRID_LIGHT_BIT_SET_VALID(GET_GRID_LIGHT_BIT_SET(x, y))
				CLEAR_BIT(LightData.iGridLightBS, GET_GRID_LIGHT_BIT_SET(x, y))
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC

PROC RESET_GRID_LIGHT_BAR_COUNTER(LOCAL_GRID_LIGHT_DATA &LightData)
	LightData.iGridLightBarCounter = 0
ENDPROC

FUNC INT GET_GRID_LIGHT_BAR_COUNTER(LOCAL_GRID_LIGHT_DATA &LightData)
	RETURN LightData.iGridLightBarCounter
ENDFUNC

PROC MAINTAIN_GRID_LIGHT_BAR_COUNTER(LOCAL_GRID_LIGHT_DATA &LightData)
	FLOAT fTimeS, fBPM
	
	INT iBeatNum
	
	IF GET_NEXT_AUDIBLE_BEAT(fTimeS, fBPM, iBeatNum)
		IF iBeatNum = 1
			IF NOT IS_BIT_SET(LightData.iBS, BS_CLUB_GRID_LIGHTS_SET_BAR_COUNTER)
				LightData.iGridLightBarCounter++
				
				SET_BIT(LightData.iBS, BS_CLUB_GRID_LIGHTS_SET_BAR_COUNTER)
			ENDIF
		ELSE
			CLEAR_BIT(LightData.iBS, BS_CLUB_GRID_LIGHTS_SET_BAR_COUNTER)
		ENDIF
	ENDIF
ENDPROC

PROC TINT_GRID_LIGHTS_NEXT_COLOUR(LOCAL_GRID_LIGHT_DATA &LightData, INT iCurrentX, INT iCurrentY, INT iMaxX, INT iMaxY)
	IF iCurrentX > iMaxX
		IF LightData.iTintMaxRow = 1
			LightData.iTintMaxRow = 3
			LightData.iTintCurrentRow = 2
		ELIF LightData.iTintMaxRow = 3
			LightData.iTintMaxRow = 4
			LightData.iTintCurrentRow = 4
		ELIF LightData.iTintMaxRow = 4
			LightData.iTintMaxRow = 1
			LightData.iTintCurrentRow = 0
		ENDIF
		
		EXIT
	ENDIF
	
	SET_GRID_LIGHT_TINT(LightData, iCurrentX, iCurrentY, LightData.ColourData.r, LightData.ColourData.g, LightData.ColourData.b)
	
	iCurrentY += 1
	
	IF iCurrentY > (iMaxY - 1)
		iCurrentY = 0
		iCurrentX += 1
	ENDIF
	
	TINT_GRID_LIGHTS_NEXT_COLOUR(LightData, iCurrentX, iCurrentY, iMaxX, iMaxY)
ENDPROC

FUNC BOOL HAVE_GRID_LIGHTS_BEEN_TINTED(INT iLightsTinted)
	RETURN (iLightsTinted = (NIGHTCLUB_GRID_LIGHTS_ARRAY_X * NIGHTCLUB_GRID_LIGHTS_ARRAY_Y))
ENDFUNC

PROC INCREMENT_GRID_LIGHT_TINT_ID(LOCAL_GRID_LIGHT_DATA &LightData, CLUB_DJS eDJ)
	IF NOT IS_BIT_SET(LightData.iBS, BS_CLUB_GRID_LIGHTS_TINTED)
		LightData.iTintID++
		
		IF LightData.iTintID > 2
			LightData.iTintID = 0
		ENDIF
		
		SET_LIGHT_COLOUR_DATA(LightData.ColourData, CLUB_HANGING_LIGHT_INVALID, eDJ, LightData.iTintID)
		
		SET_BIT(LightData.iBS, BS_CLUB_GRID_LIGHTS_TINTED)
	ENDIF
ENDPROC

PROC MAINTAIN_GRID_LIGHT_TINTING(LOCAL_GRID_LIGHT_DATA &LightData, CLUB_DJS eDJ)
	IF (LightData.iMainFlow > 0)
	AND NOT IS_BIT_SET(LightData.iBS, BS_CLUB_GRID_LIGHTS_TINTED)
		TINT_GRID_LIGHTS_NEXT_COLOUR(LightData, LightData.iTintCurrentRow, 0, LightData.iTintMaxRow, NIGHTCLUB_GRID_LIGHTS_ARRAY_Y)
		
		IF HAVE_GRID_LIGHTS_BEEN_TINTED(LightData.iLightsTinted)
			INCREMENT_GRID_LIGHT_TINT_ID(LightData, eDJ)
		ENDIF
	ELSE
		CLEAR_BIT(LightData.iBS, BS_CLUB_GRID_LIGHTS_TINTED)
		
		CLEAR_GRID_LIGHT_TINT_BITSETS(LightData)
		
		LightData.iLightsTinted = 0
	ENDIF
ENDPROC

PROC PERFORM_GRID_LIGHTS_RANDOM_EFFECT(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	INT iRand
	INT iTimeDifference
	INT iTimeOffset
	
	FLOAT fAlpha
	
	SWITCH eIntensity
		CASE AUDIO_TAG_LOW			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_LOW_MS_RAND		BREAK
		CASE AUDIO_TAG_MEDIUM		iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_MEDIUM_MS_RAND	BREAK
		CASE AUDIO_TAG_HIGH			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_HIGH_MS_RAND		BREAK
		CASE AUDIO_TAG_HIGH_HANDS	iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_HIGH_MS_RAND		BREAK
	ENDSWITCH
	
	SWITCH LightData.iFlow
		CASE 0
			RESET_GRID_LIGHT_DATA(LightData, 0)
			
			RESET_GRID_LIGHT_RAIN_DATA(LightData, 0)
			
			LightData.iFlow++
		BREAK
		
		CASE 1
			IF NOT HAS_NET_TIMER_STARTED(LightData.stRandomTimer)
				START_NET_TIMER(LightData.stRandomTimer)
				
				LightData.tdRandTime = GET_TIME_OFFSET(GET_NETWORK_TIME(), iTimeOffset)
			ELIF HAS_NET_TIMER_EXPIRED(LightData.stRandomTimer, iTimeOffset)
				CLEAR_BIT(LightData.iBS, BS_CLUB_GRID_LIGHTS_SET_RANDOM_LIGHT)
				
				REPEAT NIGHTCLUB_GRID_LIGHTS_RANDOM_TOTAL iRand
					SET_ENTITY_ALPHA(LightData.objLight[LightData.iRandomX[iRand]][LightData.iRandomY[iRand]], 0, FALSE)
				ENDREPEAT
			ELSE
				iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), LightData.tdRandTime))
				
				fAlpha = ((TO_FLOAT(iTimeDifference) / iTimeOffset) * 255.0)
				
				REPEAT NIGHTCLUB_GRID_LIGHTS_RANDOM_TOTAL iRand
					IF ROUND(fAlpha) >= 0 AND ROUND(fAlpha) <= 255
						SET_ENTITY_ALPHA(LightData.objLight[LightData.iRandomX[iRand]][LightData.iRandomY[iRand]], ROUND(fAlpha), FALSE)
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF NOT IS_BIT_SET(LightData.iBS, BS_CLUB_GRID_LIGHTS_SET_RANDOM_LIGHT)
				INT i
				REPEAT NIGHTCLUB_GRID_LIGHTS_RANDOM_TOTAL i
					LightData.iRandomX[i] = GET_RANDOM_INT_IN_RANGE(0, NIGHTCLUB_GRID_LIGHTS_ARRAY_X)
					LightData.iRandomY[i] = GET_RANDOM_INT_IN_RANGE(0, NIGHTCLUB_GRID_LIGHTS_ARRAY_Y)
				ENDREPEAT
				
				SET_BIT(LightData.iBS, BS_CLUB_GRID_LIGHTS_SET_RANDOM_LIGHT)
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(LightData.stRandomTimer, iTimeOffset)
				RESET_NET_TIMER(LightData.stRandomTimer)
				
				REPEAT NIGHTCLUB_GRID_LIGHTS_RANDOM_TOTAL iRand
					SET_ENTITY_ALPHA(LightData.objLight[LightData.iRandomX[iRand]][LightData.iRandomY[iRand]], 255, FALSE)
				ENDREPEAT
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL HAVE_ALL_GRID_LIGHTS_FINISHED_FADING(LOCAL_GRID_LIGHT_DATA &LightData, BOOL bXAxis)
	INT iTotal = NIGHTCLUB_GRID_LIGHTS_ARRAY_Y
	INT iFadeFinished
	
	IF bXAxis
		iTotal = NIGHTCLUB_GRID_LIGHTS_ARRAY_X
	ENDIF
	
	INT iLight
	REPEAT iTotal iLight
		IF LightData.bFadeLight[iLight]
			iFadeFinished++
		ENDIF
	ENDREPEAT
	
	IF iFadeFinished = iTotal
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_GRID_LIGHT_FADE_TIMER(LOCAL_GRID_LIGHT_DATA &LightData, INT x, INT iTimeOffset, BOOL bFadeIn)
	IF NOT IS_BIT_SET(LightData.iBS, GET_GRID_LIGHT_FADE_BS(x))
		LightData.tdFadeTimer[x] = GET_TIME_OFFSET(GET_NETWORK_TIME(), iTimeOffset)
		
		SET_BIT(LightData.iBS, GET_GRID_LIGHT_FADE_BS(x))
	ENDIF
	
	LightData.iTimeDifference[x] = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), LightData.tdFadeTimer[x]))
	
	LightData.fAlpha[x] = ((TO_FLOAT(LightData.iTimeDifference[x]) / iTimeOffset) * 255.0)
	
	IF bFadeIn
		LightData.fAlpha[x] = (255.0 - ((TO_FLOAT(LightData.iTimeDifference[x]) / iTimeOffset) * 255.0))
	ENDIF
ENDPROC

FUNC INT GET_GRID_LIGHT_ALPHA(LOCAL_GRID_LIGHT_DATA &LightData, INT x)
	RETURN ROUND(LightData.fAlpha[x])
ENDFUNC

PROC MAINTAIN_GRID_LIGHT_ALPHA(LOCAL_GRID_LIGHT_DATA &LightData, BOOL bXAxis)
	INT x, y
	
	IF bXAxis
		REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_X x
			REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_Y y
				IF GET_GRID_LIGHT_ALPHA(LightData, x) >= 0 AND GET_GRID_LIGHT_ALPHA(LightData, x) <= 255
					SET_ENTITY_ALPHA(LightData.objLight[x][y], GET_GRID_LIGHT_ALPHA(LightData, x), FALSE)
				ENDIF
			ENDREPEAT
		ENDREPEAT
	ELSE
		REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_Y y
			REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_X x
				IF GET_GRID_LIGHT_ALPHA(LightData, y) >= 0 AND GET_GRID_LIGHT_ALPHA(LightData, y) <= 255
					SET_ENTITY_ALPHA(LightData.objLight[x][y], GET_GRID_LIGHT_ALPHA(LightData, y), FALSE)
				ENDIF
			ENDREPEAT
		ENDREPEAT
	ENDIF
ENDPROC

PROC PERFORM_GRID_LIGHTS_FADE_IN_NORTH_EFFECT(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	INT x
	INT iTimeOffset
	
	FLOAT fAlphaThreshold
	
	SWITCH eIntensity
		CASE AUDIO_TAG_LOW
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_LOW_MS
			
			fAlphaThreshold = 55.0
		BREAK
		
		CASE AUDIO_TAG_MEDIUM
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_MEDIUM_MS
			
			fAlphaThreshold = 55.0
		BREAK
		
		CASE AUDIO_TAG_HIGH
		CASE AUDIO_TAG_HIGH_HANDS
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_HIGH_MS
			
			fAlphaThreshold = 205.0
		BREAK
	ENDSWITCH
	
	SWITCH LightData.iFlow
		CASE 0
			RESET_GRID_LIGHT_DATA(LightData, 0)
			
			RESET_GRID_LIGHT_RAIN_DATA(LightData, 0)
			
			LightData.iFlow = 1
		BREAK
		
		CASE 1
			FOR x = (NIGHTCLUB_GRID_LIGHTS_ARRAY_X - 1) TO 0 STEP -1
				IF NOT LightData.bFadeLight[x]
					IF x = (NIGHTCLUB_GRID_LIGHTS_ARRAY_X - 1)
						SET_GRID_LIGHT_FADE_TIMER(LightData, x, iTimeOffset, TRUE)
					ELSE
						IF LightData.fAlpha[x + 1] > fAlphaThreshold
							SET_GRID_LIGHT_FADE_TIMER(LightData, x, iTimeOffset, TRUE)
						ENDIF
					ENDIF
					
					IF LightData.fAlpha[x] > 225.0
						LightData.fAlpha[x] = 255.0
						
						LightData.bFadeLight[x] = TRUE
					ENDIF
				ENDIF
			ENDFOR
		BREAK
	ENDSWITCH
	
	MAINTAIN_GRID_LIGHT_ALPHA(LightData, TRUE)
ENDPROC

PROC PERFORM_GRID_LIGHTS_FADE_OUT_NORTH_EFFECT(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	INT x
	INT iTimeOffset
	
	FLOAT fAlphaThreshold
	
	SWITCH eIntensity
		CASE AUDIO_TAG_LOW
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_LOW_MS
			
			fAlphaThreshold = 200.0
		BREAK
		
		CASE AUDIO_TAG_MEDIUM
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_MEDIUM_MS
			
			fAlphaThreshold = 200.0
		BREAK
		
		CASE AUDIO_TAG_HIGH
		CASE AUDIO_TAG_HIGH_HANDS
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_HIGH_MS
			
			fAlphaThreshold = 50.0
		BREAK
	ENDSWITCH
	
	SWITCH LightData.iFlow
		CASE 0
			RESET_GRID_LIGHT_DATA(LightData, 255)
			
			RESET_GRID_LIGHT_RAIN_DATA(LightData, 0)
			
			LightData.iFlow = 1
		BREAK
		
		CASE 1
			FOR x = (NIGHTCLUB_GRID_LIGHTS_ARRAY_X - 1) TO 0 STEP -1
				IF NOT LightData.bFadeLight[x]
					IF x = (NIGHTCLUB_GRID_LIGHTS_ARRAY_X - 1)
						SET_GRID_LIGHT_FADE_TIMER(LightData, x, iTimeOffset, FALSE)
					ELSE
						IF LightData.fAlpha[x + 1] < fAlphaThreshold
							SET_GRID_LIGHT_FADE_TIMER(LightData, x, iTimeOffset, FALSE)
						ENDIF
					ENDIF
					
					IF LightData.fAlpha[x] < 30.0
						LightData.fAlpha[x] = 0.0
						
						LightData.bFadeLight[x] = TRUE
					ENDIF
				ENDIF
			ENDFOR
		BREAK
	ENDSWITCH
	
	MAINTAIN_GRID_LIGHT_ALPHA(LightData, TRUE)
ENDPROC

PROC PERFORM_GRID_LIGHTS_FADE_IN_SOUTH_EFFECT(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	INT x
	INT iTimeOffset
	
	FLOAT fAlphaThreshold
	
	SWITCH eIntensity
		CASE AUDIO_TAG_LOW
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_LOW_MS
			
			fAlphaThreshold = 55.0
		BREAK
		
		CASE AUDIO_TAG_MEDIUM
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_MEDIUM_MS
			
			fAlphaThreshold = 55.0
		BREAK
		
		CASE AUDIO_TAG_HIGH
		CASE AUDIO_TAG_HIGH_HANDS
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_HIGH_MS
			
			fAlphaThreshold = 205.0
		BREAK
	ENDSWITCH
	
	SWITCH LightData.iFlow
		CASE 0
			RESET_GRID_LIGHT_DATA(LightData, 0)
			
			RESET_GRID_LIGHT_RAIN_DATA(LightData, 0)
			
			LightData.iFlow = 1
		BREAK
		
		CASE 1
			REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_X x
				IF NOT LightData.bFadeLight[x]
					IF x = 0
						SET_GRID_LIGHT_FADE_TIMER(LightData, x, iTimeOffset, TRUE)
					ELSE
						IF LightData.fAlpha[x - 1] > fAlphaThreshold
							SET_GRID_LIGHT_FADE_TIMER(LightData, x, iTimeOffset, TRUE)
						ENDIF
					ENDIF
					
					IF LightData.fAlpha[x] > 225.0
						LightData.fAlpha[x] = 255.0
						
						LightData.bFadeLight[x] = TRUE
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
	ENDSWITCH
	
	MAINTAIN_GRID_LIGHT_ALPHA(LightData, TRUE)
ENDPROC

PROC PERFORM_GRID_LIGHTS_FADE_OUT_SOUTH_EFFECT(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	INT x
	INT iTimeOffset
	
	FLOAT fAlphaThreshold
	
	SWITCH eIntensity
		CASE AUDIO_TAG_LOW
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_LOW_MS
			
			fAlphaThreshold = 200.0
		BREAK
		
		CASE AUDIO_TAG_MEDIUM
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_MEDIUM_MS
			
			fAlphaThreshold = 200.0
		BREAK
		
		CASE AUDIO_TAG_HIGH
		CASE AUDIO_TAG_HIGH_HANDS
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_HIGH_MS
			
			fAlphaThreshold = 50.0
		BREAK
	ENDSWITCH
	
	SWITCH LightData.iFlow
		CASE 0
			RESET_GRID_LIGHT_DATA(LightData, 255)
			
			RESET_GRID_LIGHT_RAIN_DATA(LightData, 0)
			
			LightData.iFlow = 1
		BREAK
		
		CASE 1
			REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_X x
				IF NOT LightData.bFadeLight[x]
					IF x = 0
						SET_GRID_LIGHT_FADE_TIMER(LightData, x, iTimeOffset, FALSE)
					ELSE
						IF LightData.fAlpha[x - 1] < fAlphaThreshold
							SET_GRID_LIGHT_FADE_TIMER(LightData, x, iTimeOffset, FALSE)
						ENDIF
					ENDIF
					
					IF LightData.fAlpha[x] < 30.0
						LightData.fAlpha[x] = 0.0
						
						LightData.bFadeLight[x] = TRUE
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
	ENDSWITCH
	
	MAINTAIN_GRID_LIGHT_ALPHA(LightData, TRUE)
ENDPROC

PROC PERFORM_GRID_LIGHTS_FADE_IN_EAST_EFFECT(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	INT y
	INT iTimeOffset
	
	FLOAT fAlphaThreshold
	
	SWITCH eIntensity
		CASE AUDIO_TAG_LOW
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_LOW_MS
			
			fAlphaThreshold = 55.0
		BREAK
		
		CASE AUDIO_TAG_MEDIUM
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_MEDIUM_MS
			
			fAlphaThreshold = 55.0
		BREAK
		
		CASE AUDIO_TAG_HIGH
		CASE AUDIO_TAG_HIGH_HANDS
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_HIGH_MS
			
			fAlphaThreshold = 205.0
		BREAK
	ENDSWITCH
	
	SWITCH LightData.iFlow
		CASE 0
			RESET_GRID_LIGHT_DATA(LightData, 0)
			
			RESET_GRID_LIGHT_RAIN_DATA(LightData, 0)
			
			LightData.iFlow = 1
		BREAK
		
		CASE 1
			REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_Y y
				IF NOT LightData.bFadeLight[y]
					IF y = 0
						SET_GRID_LIGHT_FADE_TIMER(LightData, y, iTimeOffset, TRUE)
					ELSE
						IF LightData.fAlpha[y - 1] > fAlphaThreshold
							SET_GRID_LIGHT_FADE_TIMER(LightData, y, iTimeOffset, TRUE)
						ENDIF
					ENDIF
					
					IF LightData.fAlpha[y] > 225.0
						LightData.fAlpha[y] = 255.0
						
						LightData.bFadeLight[y] = TRUE
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
	ENDSWITCH
	
	MAINTAIN_GRID_LIGHT_ALPHA(LightData, FALSE)
ENDPROC

PROC PERFORM_GRID_LIGHTS_FADE_OUT_EAST_EFFECT(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	INT y
	INT iTimeOffset
	
	FLOAT fAlphaThreshold
	
	SWITCH eIntensity
		CASE AUDIO_TAG_LOW
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_LOW_MS
			
			fAlphaThreshold = 200.0
		BREAK
		
		CASE AUDIO_TAG_MEDIUM
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_MEDIUM_MS
			
			fAlphaThreshold = 200.0
		BREAK
		
		CASE AUDIO_TAG_HIGH
		CASE AUDIO_TAG_HIGH_HANDS
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_HIGH_MS
			
			fAlphaThreshold = 50.0
		BREAK
	ENDSWITCH
	
	SWITCH LightData.iFlow
		CASE 0
			RESET_GRID_LIGHT_DATA(LightData, 255)
			
			RESET_GRID_LIGHT_RAIN_DATA(LightData, 0)
			
			LightData.iFlow = 1
		BREAK
		
		CASE 1
			REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_Y y
				IF NOT LightData.bFadeLight[y]
					IF y = 0
						SET_GRID_LIGHT_FADE_TIMER(LightData, y, iTimeOffset, FALSE)
					ELSE
						IF LightData.fAlpha[y - 1] < fAlphaThreshold
							SET_GRID_LIGHT_FADE_TIMER(LightData, y, iTimeOffset, FALSE)
						ENDIF
					ENDIF
					
					IF LightData.fAlpha[y] < 30.0
						LightData.fAlpha[y] = 0.0
						
						LightData.bFadeLight[y] = TRUE
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
	ENDSWITCH
	
	MAINTAIN_GRID_LIGHT_ALPHA(LightData, FALSE)
ENDPROC

PROC PERFORM_GRID_LIGHTS_FADE_IN_WEST_EFFECT(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	INT y
	INT iTimeOffset
	
	FLOAT fAlphaThreshold
	
	SWITCH eIntensity
		CASE AUDIO_TAG_LOW
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_LOW_MS
			
			fAlphaThreshold = 55.0
		BREAK
		
		CASE AUDIO_TAG_MEDIUM
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_MEDIUM_MS
			
			fAlphaThreshold = 55.0
		BREAK
		
		CASE AUDIO_TAG_HIGH
		CASE AUDIO_TAG_HIGH_HANDS
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_HIGH_MS
			
			fAlphaThreshold = 205.0
		BREAK
	ENDSWITCH
	
	SWITCH LightData.iFlow
		CASE 0
			RESET_GRID_LIGHT_DATA(LightData, 0)
			
			RESET_GRID_LIGHT_RAIN_DATA(LightData, 0)
			
			LightData.iFlow = 1
		BREAK
		
		CASE 1
			FOR y = (NIGHTCLUB_GRID_LIGHTS_ARRAY_Y - 1) TO 0 STEP -1
				IF NOT LightData.bFadeLight[y]
					IF y = (NIGHTCLUB_GRID_LIGHTS_ARRAY_Y - 1)
						SET_GRID_LIGHT_FADE_TIMER(LightData, y, iTimeOffset, TRUE)
					ELSE
						IF LightData.fAlpha[y + 1] > fAlphaThreshold
							SET_GRID_LIGHT_FADE_TIMER(LightData, y, iTimeOffset, TRUE)
						ENDIF
					ENDIF
					
					IF LightData.fAlpha[y] > 225.0
						LightData.fAlpha[y] = 255.0
						
						LightData.bFadeLight[y] = TRUE
					ENDIF
				ENDIF
			ENDFOR
		BREAK
	ENDSWITCH
	
	MAINTAIN_GRID_LIGHT_ALPHA(LightData, FALSE)
ENDPROC

PROC PERFORM_GRID_LIGHTS_FADE_OUT_WEST_EFFECT(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	INT y
	INT iTimeOffset
	
	FLOAT fAlphaThreshold
	
	SWITCH eIntensity
		CASE AUDIO_TAG_LOW
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_LOW_MS
			
			fAlphaThreshold = 200.0
		BREAK
		
		CASE AUDIO_TAG_MEDIUM
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_MEDIUM_MS
			
			fAlphaThreshold = 200.0
		BREAK
		
		CASE AUDIO_TAG_HIGH
		CASE AUDIO_TAG_HIGH_HANDS
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_HIGH_MS
			
			fAlphaThreshold = 50.0
		BREAK
	ENDSWITCH
	
	SWITCH LightData.iFlow
		CASE 0
			RESET_GRID_LIGHT_DATA(LightData, 255)
			
			RESET_GRID_LIGHT_RAIN_DATA(LightData, 0)
			
			LightData.iFlow = 1
		BREAK
		
		CASE 1
			FOR y = (NIGHTCLUB_GRID_LIGHTS_ARRAY_Y - 1) TO 0 STEP -1
				IF NOT LightData.bFadeLight[y]
					IF y = (NIGHTCLUB_GRID_LIGHTS_ARRAY_Y - 1)
						SET_GRID_LIGHT_FADE_TIMER(LightData, y, iTimeOffset, FALSE)
					ELSE
						IF LightData.fAlpha[y + 1] < fAlphaThreshold
							SET_GRID_LIGHT_FADE_TIMER(LightData, y, iTimeOffset, FALSE)
						ENDIF
					ENDIF
					
					IF LightData.fAlpha[y] < 30.0
						LightData.fAlpha[y] = 0.0
						
						LightData.bFadeLight[y] = TRUE
					ENDIF
				ENDIF
			ENDFOR
		BREAK
	ENDSWITCH
	
	MAINTAIN_GRID_LIGHT_ALPHA(LightData, FALSE)
ENDPROC

PROC PERFORM_GRID_LIGHTS_NORTH_FADING(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	SWITCH LightData.iSequenceFlow
		CASE 0
			PERFORM_GRID_LIGHTS_FADE_IN_NORTH_EFFECT(LightData, eIntensity)
			
			IF HAVE_ALL_GRID_LIGHTS_FINISHED_FADING(LightData, TRUE)
				LightData.iFlow = 0
				LightData.iSequenceFlow++
				LightData.iSequenceCounter++
			ENDIF
		BREAK
		
		CASE 1
			PERFORM_GRID_LIGHTS_FADE_OUT_NORTH_EFFECT(LightData, eIntensity)
			
			IF HAVE_ALL_GRID_LIGHTS_FINISHED_FADING(LightData, TRUE)
				LightData.iFlow = 0
				LightData.iSequenceFlow = 0
				LightData.iSequenceCounter++
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC PERFORM_GRID_LIGHTS_SOUTH_FADING(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	SWITCH LightData.iSequenceFlow
		CASE 0
			PERFORM_GRID_LIGHTS_FADE_IN_SOUTH_EFFECT(LightData, eIntensity)
			
			IF HAVE_ALL_GRID_LIGHTS_FINISHED_FADING(LightData, TRUE)
				LightData.iFlow = 0
				LightData.iSequenceFlow++
				LightData.iSequenceCounter++
			ENDIF
		BREAK
		
		CASE 1
			PERFORM_GRID_LIGHTS_FADE_OUT_SOUTH_EFFECT(LightData, eIntensity)
			
			IF HAVE_ALL_GRID_LIGHTS_FINISHED_FADING(LightData, TRUE)
				LightData.iFlow = 0
				LightData.iSequenceFlow = 0
				LightData.iSequenceCounter++
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC PERFORM_GRID_LIGHTS_EAST_FADING(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	SWITCH LightData.iSequenceFlow
		CASE 0
			PERFORM_GRID_LIGHTS_FADE_IN_EAST_EFFECT(LightData, eIntensity)
			
			IF HAVE_ALL_GRID_LIGHTS_FINISHED_FADING(LightData, FALSE)
				LightData.iFlow = 0
				LightData.iSequenceFlow++
				LightData.iSequenceCounter++
			ENDIF
		BREAK
		
		CASE 1
			PERFORM_GRID_LIGHTS_FADE_OUT_EAST_EFFECT(LightData, eIntensity)
			
			IF HAVE_ALL_GRID_LIGHTS_FINISHED_FADING(LightData, FALSE)
				LightData.iFlow = 0
				LightData.iSequenceFlow = 0
				LightData.iSequenceCounter++
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC PERFORM_GRID_LIGHTS_WEST_FADING(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	SWITCH LightData.iSequenceFlow
		CASE 0
			PERFORM_GRID_LIGHTS_FADE_IN_WEST_EFFECT(LightData, eIntensity)
			
			IF HAVE_ALL_GRID_LIGHTS_FINISHED_FADING(LightData, FALSE)
				LightData.iFlow = 0
				LightData.iSequenceFlow++
				LightData.iSequenceCounter++
			ENDIF
		BREAK
		
		CASE 1
			PERFORM_GRID_LIGHTS_FADE_OUT_WEST_EFFECT(LightData, eIntensity)
			
			IF HAVE_ALL_GRID_LIGHTS_FINISHED_FADING(LightData, FALSE)
				LightData.iFlow = 0
				LightData.iSequenceFlow = 0
				LightData.iSequenceCounter++
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC PERFORM_GRID_LIGHTS_HIGH_INTENSITY(LOCAL_GRID_LIGHT_DATA &LightData, CLUB_DJS eDJ)
	IF LightData.iMainFlow > 5
		LightData.iMainFlow = 0
	ENDIF
	
	SWITCH LightData.iMainFlow
		CASE 0
			PERFORM_GRID_LIGHTS_RANDOM_EFFECT(LightData, AUDIO_TAG_HIGH)
			
			MAINTAIN_GRID_LIGHT_BAR_COUNTER(LightData)
			
			IF GET_GRID_LIGHT_BAR_COUNTER(LightData) >= 4
				LightData.iMainFlow++
				
				RESET_GRID_LIGHT_BAR_COUNTER(LightData)
			ENDIF
		BREAK
		
		CASE 1
			PERFORM_GRID_LIGHTS_NORTH_FADING(LightData, AUDIO_TAG_HIGH)
			
			IF LightData.iSequenceCounter >= 2
				LightData.iMainFlow++
				LightData.iFlow = 0
				LightData.iSequenceCounter = 0
			ENDIF
		BREAK
		
		CASE 2
			PERFORM_GRID_LIGHTS_SOUTH_FADING(LightData, AUDIO_TAG_HIGH)
			
			IF LightData.iSequenceCounter >= 2
				LightData.iMainFlow++
				LightData.iFlow = 0
				LightData.iSequenceCounter = 0
			ENDIF
		BREAK
		
		CASE 3
			PERFORM_GRID_LIGHTS_EAST_FADING(LightData, AUDIO_TAG_HIGH)
			
			IF LightData.iSequenceCounter >= 2
				LightData.iMainFlow++
				LightData.iFlow = 0
				LightData.iSequenceCounter = 0
			ENDIF
		BREAK
		
		CASE 4
			PERFORM_GRID_LIGHTS_WEST_FADING(LightData, AUDIO_TAG_HIGH)
			
			IF LightData.iSequenceCounter >= 2
				LightData.iMainFlow++
				LightData.iFlow = 0
				LightData.iSequenceCounter = 0
			ENDIF
		BREAK
		
		CASE 5
			PERFORM_GRID_LIGHTS_RANDOM_EFFECT(LightData, AUDIO_TAG_HIGH)
			
			MAINTAIN_GRID_LIGHT_BAR_COUNTER(LightData)
			
			IF GET_GRID_LIGHT_BAR_COUNTER(LightData) >= 4
				LightData.iMainFlow = 0
				
				RESET_GRID_LIGHT_BAR_COUNTER(LightData)
			ENDIF
		BREAK
	ENDSWITCH
	
	MAINTAIN_GRID_LIGHT_TINTING(LightData, eDJ)
ENDPROC

FUNC INT GET_LIGHT_GRID_X_VALUE(INT iLight)
	RETURN ROUND_DOWN_TO_NEAREST((iLight / NIGHTCLUB_GRID_LIGHTS_ARRAY_X), 1) - 1
ENDFUNC

FUNC INT GET_LIGHT_GRID_Y_VALUE(INT iLight)
	RETURN ROUND_DOWN_TO_NEAREST((iLight / NIGHTCLUB_GRID_LIGHTS_ARRAY_Y), 1) + 1
ENDFUNC

PROC MAINTAIN_GRID_LIGHT_ALPHA_CENTRE_OUT(LOCAL_GRID_LIGHT_DATA &LightData)
	INT iCentreX = GET_LIGHT_GRID_X_VALUE((NIGHTCLUB_GRID_LIGHTS_ARRAY_X * NIGHTCLUB_GRID_LIGHTS_ARRAY_Y) / 2)
	INT iCentreY = GET_LIGHT_GRID_Y_VALUE((NIGHTCLUB_GRID_LIGHTS_ARRAY_X * NIGHTCLUB_GRID_LIGHTS_ARRAY_Y) / 2)
	
	INT x
	REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_X x
		INT y
		REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_Y y
			IF (x = iCentreX)
			AND (y = iCentreY OR y = (iCentreY - 1))
				IF GET_GRID_LIGHT_ALPHA(LightData, 0) >= 0 AND GET_GRID_LIGHT_ALPHA(LightData, 0) <= 255
					SET_ENTITY_ALPHA(LightData.objLight[x][y], GET_GRID_LIGHT_ALPHA(LightData, 0), FALSE)
				ENDIF
			ELIF (x = iCentreX OR x = iCentreX - 1 OR x = iCentreX + 1)
			AND (y = iCentreY OR y = iCentreY - 1 OR y = iCentreY - 2 OR y = iCentreY + 1)
				IF (x = iCentreX)
				AND (y = iCentreY OR y = (iCentreY - 1))
					// Do nothing
				ELSE
					IF GET_GRID_LIGHT_ALPHA(LightData, 1) >= 0 AND GET_GRID_LIGHT_ALPHA(LightData, 1) <= 255
						SET_ENTITY_ALPHA(LightData.objLight[x][y], GET_GRID_LIGHT_ALPHA(LightData, 1), FALSE)
					ENDIF
				ENDIF
			ELSE
				IF (x > 0 AND x < (NIGHTCLUB_GRID_LIGHTS_ARRAY_X - 1))
				AND (y > 0 AND y < (NIGHTCLUB_GRID_LIGHTS_ARRAY_Y - 1))
					// Do nothing
				ELSE
					IF GET_GRID_LIGHT_ALPHA(LightData, 2) >= 0 AND GET_GRID_LIGHT_ALPHA(LightData, 2) <= 255
						SET_ENTITY_ALPHA(LightData.objLight[x][y], GET_GRID_LIGHT_ALPHA(LightData, 2), FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC

FUNC BOOL HAVE_GRID_LIGHTS_CENTRE_OUT_FINISHED_FADING(LOCAL_GRID_LIGHT_DATA &LightData)
	INT iTotal = 3
	INT iFadeFinished
	
	INT iLight
	REPEAT iTotal iLight
		IF LightData.bFadeLight[iLight]
			iFadeFinished++
		ENDIF
	ENDREPEAT
	
	IF iFadeFinished = iTotal
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PERFORM_GRID_LIGHTS_FADE_IN_CENTRE_IN_EFFECT(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	INT i
	INT iAlphaRows = 3
	INT iTimeOffset
	
	FLOAT fAlphaThreshold
	
	SWITCH eIntensity
		CASE AUDIO_TAG_LOW
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_LOW_MS
			
			fAlphaThreshold = 55.0
		BREAK
		
		CASE AUDIO_TAG_MEDIUM
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_MEDIUM_MS
			
			fAlphaThreshold = 55.0
		BREAK
		
		CASE AUDIO_TAG_HIGH
		CASE AUDIO_TAG_HIGH_HANDS
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_HIGH_MS
			
			fAlphaThreshold = 205.0
		BREAK
	ENDSWITCH
	
	SWITCH LightData.iFlow
		CASE 0
			RESET_GRID_LIGHT_DATA(LightData, 0)
			
			RESET_GRID_LIGHT_RAIN_DATA(LightData, 0)
			
			LightData.iFlow = 1
		BREAK
		
		CASE 1
			IF NOT LightData.bFadeLight[2]
				SET_GRID_LIGHT_FADE_TIMER(LightData, 2, iTimeOffset, TRUE)
			ENDIF
			
			IF NOT LightData.bFadeLight[1]
				IF LightData.fAlpha[2] > fAlphaThreshold
					SET_GRID_LIGHT_FADE_TIMER(LightData, 1, iTimeOffset, TRUE)
				ENDIF
			ENDIF
			
			IF NOT LightData.bFadeLight[0]
				IF LightData.fAlpha[1] > fAlphaThreshold
					SET_GRID_LIGHT_FADE_TIMER(LightData, 0, iTimeOffset, TRUE)
				ENDIF
			ENDIF
			
			FOR i = (iAlphaRows - 1) TO 0 STEP -1
				IF LightData.fAlpha[i] > 225.0
					LightData.fAlpha[i] = 255.0
					
					LightData.bFadeLight[i] = TRUE
				ENDIF
			ENDFOR
			
			MAINTAIN_GRID_LIGHT_ALPHA_CENTRE_OUT(LightData)
		BREAK
	ENDSWITCH
ENDPROC

PROC PERFORM_GRID_LIGHTS_FADE_OUT_CENTRE_OUT_EFFECT(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	INT i
	INT iAlphaRows = 3
	INT iTimeOffset
	
	FLOAT fAlphaThreshold
	
	SWITCH eIntensity
		CASE AUDIO_TAG_LOW
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_LOW_MS
			
			fAlphaThreshold = 200.0
		BREAK
		
		CASE AUDIO_TAG_MEDIUM
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_MEDIUM_MS
			
			fAlphaThreshold = 200.0
		BREAK
		
		CASE AUDIO_TAG_HIGH
		CASE AUDIO_TAG_HIGH_HANDS
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_HIGH_MS
			
			fAlphaThreshold = 50.0
		BREAK
	ENDSWITCH
	
	SWITCH LightData.iFlow
		CASE 0
			RESET_GRID_LIGHT_DATA(LightData, 255)
			
			RESET_GRID_LIGHT_RAIN_DATA(LightData, 0)
			
			LightData.iFlow = 1
		BREAK
		
		CASE 1
			IF NOT LightData.bFadeLight[0]
				SET_GRID_LIGHT_FADE_TIMER(LightData, 0, iTimeOffset, FALSE)
			ENDIF
			
			IF NOT LightData.bFadeLight[1]
				IF LightData.fAlpha[0] < fAlphaThreshold
					SET_GRID_LIGHT_FADE_TIMER(LightData, 1, iTimeOffset, FALSE)
				ENDIF
			ENDIF
			
			IF NOT LightData.bFadeLight[2]
				IF LightData.fAlpha[1] < fAlphaThreshold
					SET_GRID_LIGHT_FADE_TIMER(LightData, 2, iTimeOffset, FALSE)
				ENDIF
			ENDIF
			
			REPEAT iAlphaRows i
				IF LightData.fAlpha[i] < 30.0
					LightData.fAlpha[i] = 0.0
					
					LightData.bFadeLight[i] = TRUE
				ENDIF
			ENDREPEAT
			
			MAINTAIN_GRID_LIGHT_ALPHA_CENTRE_OUT(LightData)
		BREAK
	ENDSWITCH
ENDPROC

PROC PERFORM_GRID_LIGHTS_CENTRE_FADE_OUT(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	SWITCH LightData.iSequenceFlow
		CASE 0
			PERFORM_GRID_LIGHTS_FADE_IN_CENTRE_IN_EFFECT(LightData, eIntensity)
			
			IF HAVE_GRID_LIGHTS_CENTRE_OUT_FINISHED_FADING(LightData)
				LightData.iFlow = 0
				LightData.iSequenceFlow = 1
				LightData.iSequenceCounter++
			ENDIF
		BREAK
		
		CASE 1
			PERFORM_GRID_LIGHTS_FADE_OUT_CENTRE_OUT_EFFECT(LightData, eIntensity)
			
			IF HAVE_GRID_LIGHTS_CENTRE_OUT_FINISHED_FADING(LightData)
				LightData.iFlow = 0
				LightData.iSequenceFlow = 0
				LightData.iSequenceCounter++
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC PERFORM_GRID_LIGHTS_FADE_IN_CENTRE_OUT_EFFECT(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	INT i
	INT iAlphaRows = 3
	INT iTimeOffset
	
	FLOAT fAlphaThreshold
	
	SWITCH eIntensity
		CASE AUDIO_TAG_LOW
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_LOW_MS
			
			fAlphaThreshold = 55.0
		BREAK
		
		CASE AUDIO_TAG_MEDIUM
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_MEDIUM_MS
			
			fAlphaThreshold = 55.0
		BREAK
		
		CASE AUDIO_TAG_HIGH
		CASE AUDIO_TAG_HIGH_HANDS
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_HIGH_MS
			
			fAlphaThreshold = 205.0
		BREAK
	ENDSWITCH
	
	SWITCH LightData.iFlow
		CASE 0
			RESET_GRID_LIGHT_DATA(LightData, 0)
			
			RESET_GRID_LIGHT_RAIN_DATA(LightData, 0)
			
			LightData.iFlow = 1
		BREAK
		
		CASE 1
			IF NOT LightData.bFadeLight[0]
				SET_GRID_LIGHT_FADE_TIMER(LightData, 0, iTimeOffset, TRUE)
			ENDIF
			
			IF NOT LightData.bFadeLight[1]
				IF LightData.fAlpha[0] > fAlphaThreshold
					SET_GRID_LIGHT_FADE_TIMER(LightData, 1, iTimeOffset, TRUE)
				ENDIF
			ENDIF
			
			IF NOT LightData.bFadeLight[2]
				IF LightData.fAlpha[1] > fAlphaThreshold
					SET_GRID_LIGHT_FADE_TIMER(LightData, 2, iTimeOffset, TRUE)
				ENDIF
			ENDIF
			
			REPEAT iAlphaRows i
				IF LightData.fAlpha[i] > 225.0
					LightData.fAlpha[i] = 255.0
					
					LightData.bFadeLight[i] = TRUE
				ENDIF
			ENDREPEAT
			
			MAINTAIN_GRID_LIGHT_ALPHA_CENTRE_OUT(LightData)
		BREAK
	ENDSWITCH
ENDPROC

PROC PERFORM_GRID_LIGHTS_FADE_OUT_CENTRE_IN_EFFECT(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	INT i
	INT iAlphaRows = 3
	INT iTimeOffset
	
	FLOAT fAlphaThreshold
	
	SWITCH eIntensity
		CASE AUDIO_TAG_LOW
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_LOW_MS
			
			fAlphaThreshold = 200.0
		BREAK
		
		CASE AUDIO_TAG_MEDIUM
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_MEDIUM_MS
			
			fAlphaThreshold = 200.0
		BREAK
		
		CASE AUDIO_TAG_HIGH
		CASE AUDIO_TAG_HIGH_HANDS
			iTimeOffset = NIGHTCLUB_GRID_LIGHTS_FADE_TIME_HIGH_MS
			
			fAlphaThreshold = 50.0
		BREAK
	ENDSWITCH
	
	SWITCH LightData.iFlow
		CASE 0
			RESET_GRID_LIGHT_DATA(LightData, 255)
			
			RESET_GRID_LIGHT_RAIN_DATA(LightData, 0)
			
			LightData.iFlow = 1
		BREAK
		
		CASE 1
			
			IF NOT LightData.bFadeLight[2]
				SET_GRID_LIGHT_FADE_TIMER(LightData, 2, iTimeOffset, FALSE)
			ENDIF
			
			IF NOT LightData.bFadeLight[1]
				IF LightData.fAlpha[2] < fAlphaThreshold
					SET_GRID_LIGHT_FADE_TIMER(LightData, 1, iTimeOffset, FALSE)
				ENDIF
			ENDIF
			
			IF NOT LightData.bFadeLight[0]
				IF LightData.fAlpha[1] < fAlphaThreshold
					SET_GRID_LIGHT_FADE_TIMER(LightData, 0, iTimeOffset, FALSE)
				ENDIF
			ENDIF
			
			FOR i = (iAlphaRows - 1) TO 0 STEP -1
				IF LightData.fAlpha[i] < 30.0
					LightData.fAlpha[i] = 0.0
					
					LightData.bFadeLight[i] = TRUE
				ENDIF
			ENDFOR
			
			MAINTAIN_GRID_LIGHT_ALPHA_CENTRE_OUT(LightData)
		BREAK
	ENDSWITCH
ENDPROC

PROC PERFORM_GRID_LIGHTS_CENTRE_FADE_IN(LOCAL_GRID_LIGHT_DATA &LightData, NIGHTCLUB_AUDIO_TAGS eIntensity)
	SWITCH LightData.iSequenceFlow
		CASE 0
			PERFORM_GRID_LIGHTS_FADE_IN_CENTRE_OUT_EFFECT(LightData, eIntensity)
			
			IF HAVE_GRID_LIGHTS_CENTRE_OUT_FINISHED_FADING(LightData)
				LightData.iFlow = 0
				LightData.iSequenceFlow = 1
				LightData.iSequenceCounter++
			ENDIF
		BREAK
		
		CASE 1
			PERFORM_GRID_LIGHTS_FADE_OUT_CENTRE_IN_EFFECT(LightData, eIntensity)
			
			IF HAVE_GRID_LIGHTS_CENTRE_OUT_FINISHED_FADING(LightData)
				LightData.iFlow = 0
				LightData.iSequenceFlow = 0
				LightData.iSequenceCounter++
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC PERFORM_GRID_LIGHTS_MEDIUM_INTENSITY(LOCAL_GRID_LIGHT_DATA &LightData, CLUB_DJS eDJ)
	IF LightData.iMainFlow > 6
		LightData.iMainFlow = 0
	ENDIF
	
	SWITCH LightData.iMainFlow
		CASE 0
			PERFORM_GRID_LIGHTS_RANDOM_EFFECT(LightData, AUDIO_TAG_MEDIUM)
			
			MAINTAIN_GRID_LIGHT_BAR_COUNTER(LightData)
			
			IF GET_GRID_LIGHT_BAR_COUNTER(LightData) >= 4
				LightData.iMainFlow = 1
				LightData.iFlow = 0
				
				RESET_GRID_LIGHT_BAR_COUNTER(LightData)
			ENDIF
		BREAK
		
		CASE 1
			PERFORM_GRID_LIGHTS_NORTH_FADING(LightData, AUDIO_TAG_MEDIUM)
			
			IF LightData.iSequenceCounter >= 2
				LightData.iMainFlow = 2
				LightData.iFlow = 0
				LightData.iSequenceCounter = 0
			ENDIF
		BREAK
		
		CASE 2
			PERFORM_GRID_LIGHTS_SOUTH_FADING(LightData, AUDIO_TAG_MEDIUM)
			
			IF LightData.iSequenceCounter >= 2
				LightData.iMainFlow = 3
				LightData.iFlow = 0
				LightData.iSequenceCounter = 0
			ENDIF
		BREAK
		
		CASE 3
			PERFORM_GRID_LIGHTS_RANDOM_EFFECT(LightData, AUDIO_TAG_MEDIUM)
			
			MAINTAIN_GRID_LIGHT_BAR_COUNTER(LightData)
			
			IF GET_GRID_LIGHT_BAR_COUNTER(LightData) >= 4
				LightData.iMainFlow = 4
				LightData.iFlow = 0
				
				RESET_GRID_LIGHT_BAR_COUNTER(LightData)
			ENDIF
		BREAK
		
		CASE 4
			PERFORM_GRID_LIGHTS_CENTRE_FADE_IN(LightData, AUDIO_TAG_MEDIUM)
			
			IF LightData.iSequenceCounter >= 2
				LightData.iMainFlow = 5
				LightData.iFlow = 0
				LightData.iSequenceCounter = 0
			ENDIF
		BREAK
		
		CASE 5
			PERFORM_GRID_LIGHTS_CENTRE_FADE_OUT(LightData, AUDIO_TAG_MEDIUM)
			
			IF LightData.iSequenceCounter >= 2
				LightData.iMainFlow = 6
				LightData.iFlow = 0
				LightData.iSequenceCounter = 0
			ENDIF
		BREAK
		
		CASE 6
			PERFORM_GRID_LIGHTS_RANDOM_EFFECT(LightData, AUDIO_TAG_MEDIUM)
			
			MAINTAIN_GRID_LIGHT_BAR_COUNTER(LightData)
			
			IF GET_GRID_LIGHT_BAR_COUNTER(LightData) >= 4
				LightData.iMainFlow = 0
				LightData.iFlow = 0
				
				RESET_GRID_LIGHT_BAR_COUNTER(LightData)
			ENDIF
		BREAK
	ENDSWITCH
	
	MAINTAIN_GRID_LIGHT_TINTING(LightData, eDJ)
ENDPROC

FUNC INT GET_GRID_LIGHT_RAIN_FADE_BS(INT x, INT y)
	SWITCH x
		CASE 0
			SWITCH y
				CASE 0 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_0
				CASE 1 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_1
				CASE 2 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_2
				CASE 3 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_3
				CASE 4 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_4
				CASE 5 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_5
			ENDSWITCH
		BREAK
		
		CASE 1
			SWITCH y
				CASE 0 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_6
				CASE 1 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_7
				CASE 2 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_8
				CASE 3 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_9
				CASE 4 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_10
				CASE 5 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_11
			ENDSWITCH
		BREAK
		
		CASE 2
			SWITCH y
				CASE 0 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_12
				CASE 1 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_13
				CASE 2 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_14
				CASE 3 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_15
				CASE 4 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_16
				CASE 5 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_17
			ENDSWITCH
		BREAK
		
		CASE 3
			SWITCH y
				CASE 0 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_18
				CASE 1 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_19
				CASE 2 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_20
				CASE 3 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_21
				CASE 4 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_22
				CASE 5 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_23
			ENDSWITCH
		BREAK
		
		CASE 4
			SWITCH y
				CASE 0 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_24
				CASE 1 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_25
				CASE 2 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_26
				CASE 3 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_27
				CASE 4 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_28
				CASE 5 RETURN g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_FADE_TIMER_29
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC INT GET_GRID_LIGHT_RAIN_GROUP(INT x, INT y)
	SWITCH x
		CASE 0
			SWITCH y
				CASE 0 RETURN 6
				CASE 1 RETURN 6
				CASE 2 RETURN 4
				CASE 3 RETURN 2
				CASE 4 RETURN 2
				CASE 5 RETURN 5
			ENDSWITCH
		BREAK
		
		CASE 1
			SWITCH y
				CASE 0 RETURN 6
				CASE 1 RETURN 4
				CASE 2 RETURN 4
				CASE 3 RETURN 5
				CASE 4 RETURN 5
				CASE 5 RETURN 2
			ENDSWITCH
		BREAK
		
		CASE 2
			SWITCH y
				CASE 0 RETURN 4
				CASE 1 RETURN 1
				CASE 2 RETURN 1
				CASE 3 RETURN 3
				CASE 4 RETURN 2
				CASE 5 RETURN 2
			ENDSWITCH
		BREAK
		
		CASE 3
			SWITCH y
				CASE 0 RETURN 6
				CASE 1 RETURN 4
				CASE 2 RETURN 1
				CASE 3 RETURN 3
				CASE 4 RETURN 3
				CASE 5 RETURN 5
			ENDSWITCH
		BREAK
		
		CASE 4
			SWITCH y
				CASE 0 RETURN 1
				CASE 1 RETURN 1
				CASE 2 RETURN 6
				CASE 3 RETURN 3
				CASE 4 RETURN 5
				CASE 5 RETURN 3
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_RAIN_GRID_LIGHT_CURRENTLY_FADING(LOCAL_GRID_LIGHT_DATA &LightData, INT x, INT y)
	IF LightData.fRainAlpha[x][y] > 0
	AND LightData.fRainAlpha[x][y] < 255
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_GRID_LIGHT_RAIN_FADE_TIMER(LOCAL_GRID_LIGHT_DATA &LightData, INT x, INT y, BOOL bFadeIn)
	IF NOT IS_BIT_SET(LightData.iGridLightRainBS, GET_GRID_LIGHT_RAIN_FADE_BS(x, y))
		LightData.iGridLightFadeOffset[x][y] = GET_RANDOM_INT_IN_RANGE(750, 2001)
		
		LightData.tdRainFadeTimer[x][y] = GET_TIME_OFFSET(GET_NETWORK_TIME(), LightData.iGridLightFadeOffset[x][y])
		
		SET_BIT(LightData.iGridLightRainBS, GET_GRID_LIGHT_RAIN_FADE_BS(x, y))
	ENDIF
	
	INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), LightData.tdRainFadeTimer[x][y]))
	
	LightData.fRainAlpha[x][y] = ((TO_FLOAT(iTimeDifference) / LightData.iGridLightFadeOffset[x][y]) * 255.0)
	
	IF bFadeIn
		LightData.fRainAlpha[x][y] = (255.0 - ((TO_FLOAT(iTimeDifference) / LightData.iGridLightFadeOffset[x][y]) * 255.0))
	ENDIF
ENDPROC

FUNC INT GET_GRID_LIGHT_RAIN_ALPHA(LOCAL_GRID_LIGHT_DATA &LightData, INT x, INT y)
	RETURN ROUND(LightData.fRainAlpha[x][y])
ENDFUNC

PROC SET_GRID_LIGHT_RAIN_ALPHA(LOCAL_GRID_LIGHT_DATA &LightData, INT x, INT y, INT iAlpha)
	LightData.fRainAlpha[x][y] = TO_FLOAT(iAlpha)
ENDPROC

PROC MAINTAIN_GRID_LIGHT_RAIN_ALPHA(LOCAL_GRID_LIGHT_DATA &LightData, INT x, INT y)
	IF GET_GRID_LIGHT_RAIN_ALPHA(LightData, x, y) >= 0 AND GET_GRID_LIGHT_RAIN_ALPHA(LightData, x, y) <= 255
		SET_ENTITY_ALPHA(LightData.objLight[x][y], GET_GRID_LIGHT_RAIN_ALPHA(LightData, x, y), FALSE)
	ENDIF
	
	IF LightData.fRainAlpha[x][y] < 0
		LightData.fRainAlpha[x][y] = 0
		
		IF NOT LightData.bRainFadeFlag[x][y]
			LightData.bRainFadeFlag[x][y] = TRUE
		ENDIF
		
		IF IS_BIT_SET(LightData.iGridLightRainBS, GET_GRID_LIGHT_RAIN_FADE_BS(x, y))
			CLEAR_BIT(LightData.iGridLightRainBS, GET_GRID_LIGHT_RAIN_FADE_BS(x, y))
		ENDIF
	ELIF LightData.fRainAlpha[x][y] > 255
		LightData.fRainAlpha[x][y] = 255
		
		IF LightData.bRainFadeFlag[x][y]
			LightData.bRainFadeFlag[x][y] = FALSE
		ENDIF
		
		IF IS_BIT_SET(LightData.iGridLightRainBS, GET_GRID_LIGHT_RAIN_FADE_BS(x, y))
			CLEAR_BIT(LightData.iGridLightRainBS, GET_GRID_LIGHT_RAIN_FADE_BS(x, y))
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_GRID_LIGHT_RAIN_GROUP_FADING_OUT(LOCAL_GRID_LIGHT_DATA &LightData, INT iGroup)
	INT x
	REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_X x
		INT y
		REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_Y y
			IF GET_GRID_LIGHT_RAIN_GROUP(x, y) = iGroup
				IF NOT LightData.bRainFadeFlag[x][y]
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SET_GRID_LIGHT_GROUP(LOCAL_GRID_LIGHT_DATA &LightData, INT iGroup)
	LightData.iGridLightCurrentGroup = iGroup
	
	CLEAR_BIT(LightData.iGridLightRainBS, g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_SET_RAND_TIME)
	
	REINIT_NET_TIMER(LightData.stActiveTimer)
ENDPROC

PROC CHECK_SURROUNDING_GRID_LIGHT_GROUPS(LOCAL_GRID_LIGHT_DATA &LightData, INT iRandGroup)
	iRandGroup++
	
	IF iRandGroup > 6
		iRandGroup = 1
	ENDIF
	
	IF NOT IS_GRID_LIGHT_RAIN_GROUP_FADING_OUT(LightData, iRandGroup)
		SET_GRID_LIGHT_GROUP(LightData, iRandGroup)
	ELSE
		iRandGroup -= 2
		
		IF iRandGroup = -1
			iRandGroup = 5
		ELIF iRandGroup = 0
			iRandGroup = 6
		ENDIF
		
		IF NOT IS_GRID_LIGHT_RAIN_GROUP_FADING_OUT(LightData, iRandGroup)
			SET_GRID_LIGHT_GROUP(LightData, iRandGroup)
		ELSE
			SET_GRID_LIGHT_GROUP(LightData, 0)
		ENDIF
	ENDIF
ENDPROC

PROC FIND_NEXT_SUITABLE_GRID_LIGHT_GROUP(LOCAL_GRID_LIGHT_DATA &LightData)
	INT iPrevGrop = LightData.iGridLightCurrentGroup
	INT iRandGroup = GET_RANDOM_INT_IN_RANGE(1, 7)
	
	IF iRandGroup != iPrevGrop
		IF NOT IS_GRID_LIGHT_RAIN_GROUP_FADING_OUT(LightData, iRandGroup)
			SET_GRID_LIGHT_GROUP(LightData, iRandGroup)
		ELSE
			CHECK_SURROUNDING_GRID_LIGHT_GROUPS(LightData, iRandGroup)
		ENDIF
	ELSE
		CHECK_SURROUNDING_GRID_LIGHT_GROUPS(LightData, iRandGroup)
	ENDIF
ENDPROC

PROC MAINTAIN_GRID_LIGHT_RAIN_CURRENT_GROUP(LOCAL_GRID_LIGHT_DATA &LightData)
	IF NOT IS_BIT_SET(LightData.iGridLightRainBS, g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_SET_RAND_TIME)
		LightData.iGridLightRandTime = GET_RANDOM_INT_IN_RANGE(2000, 4001)
		
		SET_BIT(LightData.iGridLightRainBS, g_ciBS_NIGHTCLUB_GRID_LIGHT_RAIN_SET_RAND_TIME)
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(LightData.stActiveTimer)
		START_NET_TIMER(LightData.stActiveTimer)
	ELIF HAS_NET_TIMER_EXPIRED(LightData.stActiveTimer, LightData.iGridLightRandTime)
		FIND_NEXT_SUITABLE_GRID_LIGHT_GROUP(LightData)
	ENDIF
ENDPROC

PROC PERFORM_GRID_LIGHTS_RAIN_EFFECT(LOCAL_GRID_LIGHT_DATA &LightData)
	SWITCH LightData.iFlow
		CASE 0
			RESET_GRID_LIGHT_DATA(LightData, 0)
			
			RESET_GRID_LIGHT_RAIN_DATA(LightData, 0)
			
			LightData.iFlow = 1
		BREAK
		
		CASE 1
			INT x
			REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_X x
				INT y
				REPEAT NIGHTCLUB_GRID_LIGHTS_ARRAY_Y y
					IF GET_GRID_LIGHT_RAIN_GROUP(x, y) = LightData.iRainCurrentGroup
					OR IS_RAIN_GRID_LIGHT_CURRENTLY_FADING(LightData, x, y)
					OR NOT LightData.bRainFadeFlag[x][y]
						SET_GRID_LIGHT_RAIN_FADE_TIMER(LightData, x, y, LightData.bRainFadeFlag[x][y])
					ENDIF
					
					IF LightData.bRainFadeFlag[x][y]
						IF GET_GRID_LIGHT_RAIN_ALPHA(LightData, x, y) > 235
							SET_GRID_LIGHT_RAIN_ALPHA(LightData, x, y, 255)
							
							LightData.bRainFadeFlag[x][y] = FALSE
							
							CLEAR_BIT(LightData.iGridLightRainBS, GET_GRID_LIGHT_RAIN_FADE_BS(x, y))
						ENDIF
					ELSE
						IF GET_GRID_LIGHT_RAIN_ALPHA(LightData,x, y) < 20
							SET_GRID_LIGHT_RAIN_ALPHA(LightData,x, y, 0)
							
							LightData.bRainFadeFlag[x][y] = TRUE
							
							CLEAR_BIT(LightData.iGridLightRainBS, GET_GRID_LIGHT_RAIN_FADE_BS(x, y))
						ENDIF
					ENDIF
					
					MAINTAIN_GRID_LIGHT_RAIN_ALPHA(LightData, x, y)
				ENDREPEAT
			ENDREPEAT
		BREAK
	ENDSWITCH
	
	MAINTAIN_GRID_LIGHT_RAIN_CURRENT_GROUP(LightData)
ENDPROC

PROC PERFORM_GRID_LIGHTS_LOW_INTENSITY(LOCAL_GRID_LIGHT_DATA &LightData, CLUB_DJS eDJ)
	IF LightData.iMainFlow > 3
		LightData.iMainFlow = 0
	ENDIF
	
	SWITCH LightData.iMainFlow
		CASE 0
			PERFORM_GRID_LIGHTS_RAIN_EFFECT(LightData)
			
			MAINTAIN_GRID_LIGHT_BAR_COUNTER(LightData)
			
			IF GET_GRID_LIGHT_BAR_COUNTER(LightData) >= 8
				LightData.iMainFlow = 1
				LightData.iFlow = 0
				
				CLEAR_GRID_LIGHT_TINT_BITSETS(LightData)
				
				RESET_GRID_LIGHT_BAR_COUNTER(LightData)
			ENDIF
		BREAK
		
		CASE 1
			PERFORM_GRID_LIGHTS_NORTH_FADING(LightData, AUDIO_TAG_LOW)
			
			IF LightData.iSequenceCounter >= 2
				LightData.iMainFlow = 2
				LightData.iFlow = 0
				LightData.iSequenceCounter = 0
			ENDIF
		BREAK
		
		CASE 2
			PERFORM_GRID_LIGHTS_RAIN_EFFECT(LightData)
			
			MAINTAIN_GRID_LIGHT_BAR_COUNTER(LightData)
			
			IF GET_GRID_LIGHT_BAR_COUNTER(LightData) >= 8
				LightData.iMainFlow = 3
				LightData.iFlow = 0
				
				CLEAR_GRID_LIGHT_TINT_BITSETS(LightData)
				
				RESET_GRID_LIGHT_BAR_COUNTER(LightData)
			ENDIF
		BREAK
		
		CASE 3
			PERFORM_GRID_LIGHTS_CENTRE_FADE_IN(LightData, AUDIO_TAG_LOW)
			
			IF LightData.iSequenceCounter >= 2
				LightData.iMainFlow = 0
				LightData.iFlow = 0
				LightData.iSequenceCounter = 0
			ENDIF
		BREAK
	ENDSWITCH
	
	MAINTAIN_GRID_LIGHT_TINTING(LightData, eDJ)
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ MAINTAIN ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Maintains the club hanging lights
/// PARAMS:
///    LightData - Lighting data to set.
///    interiorID - ID of the interior.
///    iRoomKey - Room key the lights are spawned in. Used to force light objects for room.
///    eDJ - The current DJ playing. Used to populate the colour scheme.
PROC MAINTAIN_CLUB_HANGING_LIGHTS(LOCAL_HANGING_LIGHT_DATA &LightData, CLUB_HANGING_LIGHT_SEQUENCE &eServerSequence, INTERIOR_INSTANCE_INDEX interiorID, INT iRoomKey, CLUB_DJS eDJ, NIGHTCLUB_AUDIO_TAGS eIntensity)
	INT iLightID
	REPEAT MAX_CLUB_HANGING_LIGHT_OBJECTS iLightID
		SWITCH LightData.Data[iLightID].eState
			CASE CLUB_LIGHT_STATE_INIT
				IF INITIALISE_CLUB_HANGING_LIGHTS(LightData.Data[iLightID], INT_TO_ENUM(CLUB_HANGING_LIGHT_LOCATION, iLightID), interiorID, iRoomKey)
					SET_BIT(LightData.Data[iLightID].iBS, BS_CLUB_HANGING_LIGHTS_SET_COMPLETED_SEQUENCE)
					
					SET_CLUB_HANGING_LIGHT_SEQUENCE(LightData.Data[iLightID], eServerSequence)
					
					SET_CLUB_LIGHT_STATE(LightData.Data[iLightID], CLUB_LIGHT_STATE_UPDATE)
				ENDIF
			BREAK
			
			CASE CLUB_LIGHT_STATE_UPDATE
				SWITCH LightData.Data[iLightID].eSequence
					CASE CLUB_HANGING_LIGHT_SEQUENCE_MEXICAN_WAVE
						IF PERFORM_CLUB_HANGING_LIGHT_SEQUENCE_MEXICAN_WAVE(LightData, INT_TO_ENUM(CLUB_HANGING_LIGHT_LOCATION, iLightID), LightData.Data[iLightID].eSequence, eDJ, eIntensity)
							SET_CLUB_HANGING_LIGHT_SEQUENCE(LightData.Data[iLightID], CLUB_HANGING_LIGHT_SEQUENCE_CIRCUMFERENCE_POINT_CIRCLE)
						ENDIF
					BREAK
					
					CASE CLUB_HANGING_LIGHT_SEQUENCE_CIRCUMFERENCE_POINT_CIRCLE
						IF PERFORM_CLUB_HANGING_LIGHT_SEQUENCE_CIRCUMFERENCE_POINT_CIRCLE(LightData, INT_TO_ENUM(CLUB_HANGING_LIGHT_LOCATION, iLightID), LightData.Data[iLightID].eSequence, eDJ, eIntensity)
							SET_CLUB_HANGING_LIGHT_SEQUENCE(LightData.Data[iLightID], CLUB_HANGING_LIGHT_SEQUENCE_SWEEP_UP)
						ENDIF
					BREAK
					
					CASE CLUB_HANGING_LIGHT_SEQUENCE_SWEEP_UP
						IF PERFORM_CLUB_HANGING_LIGHT_SEQUENCE_SWEEP_UP(LightData, INT_TO_ENUM(CLUB_HANGING_LIGHT_LOCATION, iLightID), LightData.Data[iLightID].eSequence, eDJ, eIntensity)
							IF eIntensity = AUDIO_TAG_NULL
							OR eIntensity = AUDIO_TAG_LOW
							OR eIntensity = AUDIO_TAG_MEDIUM
								SET_CLUB_HANGING_LIGHT_SEQUENCE(LightData.Data[iLightID], CLUB_HANGING_LIGHT_SEQUENCE_CIRCUMFERENCE_POINT_CIRCLE)
							ELSE
								SET_CLUB_HANGING_LIGHT_SEQUENCE(LightData.Data[iLightID], CLUB_HANGING_LIGHT_SEQUENCE_VERTICAL_STROBE)
							ENDIF
						ENDIF
					BREAK
					
					CASE CLUB_HANGING_LIGHT_SEQUENCE_VERTICAL_STROBE
						IF PERFORM_CLUB_HANGING_LIGHT_SEQUENCE_VERTICAL_STROBE(LightData, INT_TO_ENUM(CLUB_HANGING_LIGHT_LOCATION, iLightID), LightData.Data[iLightID].eSequence, eDJ, eIntensity)
							SET_CLUB_HANGING_LIGHT_SEQUENCE(LightData.Data[iLightID], CLUB_HANGING_LIGHT_SEQUENCE_RANDOM_FLASH)
						ENDIF
					BREAK
					
					CASE CLUB_HANGING_LIGHT_SEQUENCE_RANDOM_FLASH
						IF PERFORM_CLUB_HANGING_LIGHT_SEQUENCE_RANDOM_FLASH(LightData, INT_TO_ENUM(CLUB_HANGING_LIGHT_LOCATION, iLightID), LightData.Data[iLightID].eSequence, eDJ, eIntensity)
							SET_CLUB_HANGING_LIGHT_SEQUENCE(LightData.Data[iLightID], CLUB_HANGING_LIGHT_SEQUENCE_CIRCUMFERENCE_POINT_CIRCLE)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDREPEAT
	
	// Try to keep lights in sync across players
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		eServerSequence = LightData.Data[0].eSequence
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the club hanging lights
/// PARAMS:
///    LightData - Lighting data to set.
///    interiorID - ID of the interior.
///    iRoomKey - Room key the lights are spawned in. Used to force light objects for room.
///    eDJ - The current DJ playing. Used to populate the colour scheme.
PROC MAINTAIN_CLUB_GRID_LIGHTS(LOCAL_GRID_LIGHT_DATA &LightData, SIMPLE_INTERIOR_DETAILS &sSimpleInteriorDetails, INTERIOR_INSTANCE_INDEX interiorID, INT iRoomKey, CLUB_DJS eDJ, NIGHTCLUB_AUDIO_TAGS eIntensity)
	INITIALISE_CLUB_GRID_LIGHTS(LightData, sSimpleInteriorDetails, interiorID, iRoomKey, eDJ)
	
	MAINTAIN_GRID_LIGHT_INIT_TINTING(LightData)
	
	IF HAVE_GRID_LIGHTS_BEEN_CREATED(LightData)
	AND HAVE_GRID_LIGHTS_BEEN_INIT_TINTED(LightData)
		CLAMP_GRID_LIGHT_ALPHAS(LightData)
		
		SWITCH eIntensity
			CASE AUDIO_TAG_NULL
			CASE AUDIO_TAG_LOW
				PERFORM_GRID_LIGHTS_LOW_INTENSITY(LightData, eDJ)
			BREAK
			
			CASE AUDIO_TAG_MEDIUM
				PERFORM_GRID_LIGHTS_MEDIUM_INTENSITY(LightData, eDJ)
			BREAK
			
			CASE AUDIO_TAG_HIGH
			CASE AUDIO_TAG_HIGH_HANDS
				PERFORM_GRID_LIGHTS_HIGH_INTENSITY(LightData, eDJ)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

#ENDIF // FEATURE_CASINO_NIGHTCLUB
