// Name:        NET_CASINO_PEDS_COMPONENT_DATA.sch
// Description: Lookup for Casino peds component data.

USING "net_include.sch"

#IF FEATURE_CASINO
USING "net_casino_peds_vars.sch"

FUNC BOOL IS_CASINO_PED_COMPONENT_DATA_VALID(INT iDrawable1, INT iDrawable2, INT iTexture1, INT iTexture2)
	RETURN (iDrawable1 > 0 OR iDrawable2 > 0 OR iTexture1 > 0 OR iTexture2 > 0)
ENDFUNC

PROC GET_CASINO_PED_COMPONENT_DATA_1(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287748
		iPackedDrawable2 = 0
		iPackedTexture1 = 135424
		iPackedTexture2 = 0
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8961
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70144
		iPackedDrawable2 = 4096
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73986
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66817
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 74240
		iPackedTexture2 = 48
	BREAK 
	CASE 9
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4098
		iPackedDrawable2 = 0
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70147
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707083
		iPackedDrawable2 = 0
		iPackedTexture1 = 147968
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 196865
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221959
		iPackedDrawable2 = 96
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4353
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360712
		iPackedDrawable2 = 48
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 16
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 73730
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 4608
		iPackedTexture2 = 16
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152835
		iPackedDrawable2 = 16
		iPackedTexture1 = 131073
		iPackedTexture2 = 32
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 70144
		iPackedTexture2 = 16
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 70145
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728330
		iPackedDrawable2 = 0
		iPackedTexture1 = 66050
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222980
		iPackedDrawable2 = 32
		iPackedTexture1 = 8704
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 344320
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 66050
		iPackedTexture2 = 16
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 144389
		iPackedDrawable2 = 80
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 86016
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 8192
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_CAROL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 580875
		iPackedDrawable2 = 96
		iPackedTexture1 = 512
		iPackedTexture2 = 0
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3072
		iPackedDrawable2 = 0
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 581131
		iPackedDrawable2 = 96
		iPackedTexture1 = 73986
		iPackedTexture2 = 0
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694795
		iPackedDrawable2 = 0
		iPackedTexture1 = 274433
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 144389
		iPackedDrawable2 = 80
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221959
		iPackedDrawable2 = 96
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4096
		iPackedDrawable2 = 0
		iPackedTexture1 = 66049
		iPackedTexture2 = 16
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 74240
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 16384
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 8449
		iPackedTexture2 = 16
	BREAK 
	CASE 52
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646145
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 516
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_FRONT_DESK_GUARD
		iPackedDrawable1 = 66564
		iPackedDrawable2 = 8448
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 131329
		iPackedTexture2 = 16
	BREAK 
	CASE 61
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 695054
		iPackedDrawable2 = 0
		iPackedTexture1 = 20737
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 135682
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 4353
		iPackedTexture2 = 32
	BREAK 
	CASE 67
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66818
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74244
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 371208
		iPackedDrawable2 = 64
		iPackedTexture1 = 73984
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 77825
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 131586
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 514
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 585227
		iPackedDrawable2 = 96
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 288004
		iPackedDrawable2 = 0
		iPackedTexture1 = 196609
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70146
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1024
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360456
		iPackedDrawable2 = 48
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 131330
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 440840
		iPackedDrawable2 = 64
		iPackedTexture1 = 73729
		iPackedTexture2 = 16
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 227077
		iPackedDrawable2 = 32
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297222
		iPackedDrawable2 = 48
		iPackedTexture1 = 196609
		iPackedTexture2 = 0
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66051
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69889
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8961
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707339
		iPackedDrawable2 = 0
		iPackedTexture1 = 73730
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8450
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 55309
		iPackedDrawable2 = 0
		iPackedTexture1 = 78080
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 135680
		iPackedTexture2 = 16
	BREAK 
	CASE 100
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 410633
		iPackedDrawable2 = 48
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74243
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69634
		iPackedDrawable2 = 0
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69632
		iPackedDrawable2 = 12288
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 2
		iPackedTexture2 = 16
	BREAK 
	CASE 106
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56320
		iPackedDrawable2 = 0
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 645132
		iPackedDrawable2 = 0
		iPackedTexture1 = 20482
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 8449
		iPackedTexture2 = 16
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4612
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73985
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74240
		iPackedDrawable2 = 16384
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148482
		iPackedDrawable2 = 16
		iPackedTexture1 = 131584
		iPackedTexture2 = 48
	BREAK 
	CASE 115
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 514
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 217859
		iPackedDrawable2 = 0
		iPackedTexture1 = 70145
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1282
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 16384
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 119
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SECURITY_GUARD_2
		iPackedDrawable1 = 66308
		iPackedDrawable2 = 8448
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC


PROC GET_CASINO_PED_COMPONENT_DATA_2(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69890
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 513
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 74240
		iPackedTexture2 = 48
	BREAK 
	CASE 9
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70147
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707342
		iPackedDrawable2 = 0
		iPackedTexture1 = 262401
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 131329
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221959
		iPackedDrawable2 = 96
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 139776
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3072
		iPackedDrawable2 = 0
		iPackedTexture1 = 73730
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 8704
		iPackedTexture2 = 16
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69632
		iPackedTexture2 = 16
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222724
		iPackedDrawable2 = 32
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 209153
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 70145
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70656
		iPackedDrawable2 = 16384
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73986
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 131329
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360712
		iPackedDrawable2 = 48
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66050
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1282
		iPackedDrawable2 = 0
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_CAROL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 0
		iPackedTexture1 = 139265
		iPackedTexture2 = 16
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 74242
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 70146
		iPackedTexture2 = 16
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646145
		iPackedDrawable2 = 0
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 516
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152835
		iPackedDrawable2 = 16
		iPackedTexture1 = 131329
		iPackedTexture2 = 32
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 12289
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 645388
		iPackedDrawable2 = 0
		iPackedTexture1 = 20994
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301063
		iPackedDrawable2 = 48
		iPackedTexture1 = 8449
		iPackedTexture2 = 16
	BREAK 
	CASE 52
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707339
		iPackedDrawable2 = 0
		iPackedTexture1 = 86018
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646144
		iPackedDrawable2 = 0
		iPackedTexture1 = 147457
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 151553
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 585227
		iPackedDrawable2 = 96
		iPackedTexture1 = 65794
		iPackedTexture2 = 0
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_FRONT_DESK_GUARD
		iPackedDrawable1 = 66564
		iPackedDrawable2 = 8448
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 581131
		iPackedDrawable2 = 96
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504073
		iPackedDrawable2 = 0
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728586
		iPackedDrawable2 = 0
		iPackedTexture1 = 8448
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 4354
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 16
		iPackedTexture1 = 8449
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296966
		iPackedDrawable2 = 48
		iPackedTexture1 = 197121
		iPackedTexture2 = 16
	BREAK 
	CASE 67
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135424
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70913
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4610
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 514
		iPackedTexture2 = 16
	BREAK 
	CASE 71
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 12288
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 0
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8194
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74244
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 436744
		iPackedDrawable2 = 64
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 292100
		iPackedDrawable2 = 0
		iPackedTexture1 = 208897
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 513
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 736778
		iPackedDrawable2 = 0
		iPackedTexture1 = 131585
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 12288
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4611
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74240
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 418569
		iPackedDrawable2 = 48
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5120
		iPackedDrawable2 = 8192
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650507
		iPackedDrawable2 = 96
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 131072
		iPackedTexture2 = 16
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 131584
		iPackedTexture2 = 0
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 65538
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 513
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69890
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 148227
		iPackedDrawable2 = 0
		iPackedTexture1 = 4352
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135681
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 143362
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 8704
		iPackedTexture2 = 16
	BREAK 
	CASE 100
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360456
		iPackedDrawable2 = 48
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70147
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 8448
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 257
		iPackedDrawable2 = 0
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 227077
		iPackedDrawable2 = 32
		iPackedTexture1 = 135424
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 70146
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 66050
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 4097
		iPackedTexture2 = 48
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74240
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1282
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4612
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 410633
		iPackedDrawable2 = 80
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148482
		iPackedDrawable2 = 16
		iPackedTexture1 = 131584
		iPackedTexture2 = 32
	BREAK 
	CASE 115
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 152065
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 16640
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 512
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 8192
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 119
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360456
		iPackedDrawable2 = 48
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SECURITY_GUARD_2
		iPackedDrawable1 = 66308
		iPackedDrawable2 = 8448
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_3(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 147457
		iPackedTexture2 = 0
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70144
		iPackedDrawable2 = 8192
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1025
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 74240
		iPackedTexture2 = 48
	BREAK 
	CASE 9
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70913
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74243
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 143617
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 217344
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 70144
		iPackedTexture2 = 0
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 131328
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 65538
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 143617
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 4608
		iPackedTexture2 = 16
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 584971
		iPackedDrawable2 = 96
		iPackedTexture1 = 4354
		iPackedTexture2 = 0
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 78336
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 292101
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301063
		iPackedDrawable2 = 48
		iPackedTexture1 = 513
		iPackedTexture2 = 16
	BREAK 
	CASE 30
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135682
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 77825
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 131329
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287494
		iPackedDrawable2 = 96
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728330
		iPackedDrawable2 = 0
		iPackedTexture1 = 513
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66818
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 65538
		iPackedTexture2 = 0
	BREAK 
	CASE 41
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 73730
		iPackedTexture2 = 16
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 82434
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152835
		iPackedDrawable2 = 16
		iPackedTexture1 = 131073
		iPackedTexture2 = 32
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226055
		iPackedDrawable2 = 96
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 410633
		iPackedDrawable2 = 80
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 70144
		iPackedTexture2 = 16
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707339
		iPackedDrawable2 = 0
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 204801
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4096
		iPackedDrawable2 = 0
		iPackedTexture1 = 73985
		iPackedTexture2 = 16
	BREAK 
	CASE 52
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 151553
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 539914
		iPackedDrawable2 = 0
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56320
		iPackedDrawable2 = 0
		iPackedTexture1 = 77825
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 646411
		iPackedDrawable2 = 96
		iPackedTexture1 = 512
		iPackedTexture2 = 0
	BREAK 
	CASE 56
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135682
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 0
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_FRONT_DESK_GUARD
		iPackedDrawable1 = 66564
		iPackedDrawable2 = 8448
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 226820
		iPackedDrawable2 = 32
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 70146
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504073
		iPackedDrawable2 = 0
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 4352
		iPackedTexture2 = 16
	BREAK 
	CASE 65
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8450
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69890
		iPackedDrawable2 = 0
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152578
		iPackedDrawable2 = 16
		iPackedTexture1 = 4609
		iPackedTexture2 = 48
	BREAK 
	CASE 71
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 736778
		iPackedDrawable2 = 0
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8961
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70146
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297223
		iPackedDrawable2 = 48
		iPackedTexture1 = 143616
		iPackedTexture2 = 16
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 147456
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 645133
		iPackedDrawable2 = 0
		iPackedTexture1 = 512
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 8192
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 341001
		iPackedDrawable2 = 48
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360456
		iPackedDrawable2 = 48
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 227077
		iPackedDrawable2 = 32
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301318
		iPackedDrawable2 = 48
		iPackedTexture1 = 209409
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 513
		iPackedTexture2 = 0
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70148
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 65792
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 16384
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 16386
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66561
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 695051
		iPackedDrawable2 = 0
		iPackedTexture1 = 331778
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 16
		iPackedTexture1 = 8449
		iPackedTexture2 = 0
	BREAK 
	CASE 100
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74242
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4611
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 516
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5377
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 646411
		iPackedDrawable2 = 96
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 440840
		iPackedDrawable2 = 64
		iPackedTexture1 = 4096
		iPackedTexture2 = 16
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74243
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 12288
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 2
		iPackedDrawable2 = 0
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8708
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148482
		iPackedDrawable2 = 16
		iPackedTexture1 = 65792
		iPackedTexture2 = 32
	BREAK 
	CASE 115
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 8705
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9474
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 119
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 425992
		iPackedDrawable2 = 48
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SECURITY_GUARD_2
		iPackedDrawable1 = 66308
		iPackedDrawable2 = 8448
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_4(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 74240
		iPackedTexture2 = 48
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9217
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 9
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 540170
		iPackedDrawable2 = 0
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 131584
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 151554
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 205314
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 8448
		iPackedTexture2 = 16
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 144389
		iPackedDrawable2 = 80
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70400
		iPackedDrawable2 = 8192
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 65793
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70146
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 73985
		iPackedTexture2 = 16
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56320
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 226820
		iPackedDrawable2 = 32
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 513
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8707
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 135426
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707339
		iPackedDrawable2 = 0
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 4610
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70913
		iPackedDrawable2 = 0
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4354
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221959
		iPackedDrawable2 = 96
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226054
		iPackedDrawable2 = 96
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_CAROL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 81922
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73985
		iPackedTexture2 = 0
	BREAK 
	CASE 45
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221959
		iPackedDrawable2 = 96
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 425992
		iPackedDrawable2 = 48
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 147971
		iPackedDrawable2 = 0
		iPackedTexture1 = 4352
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1282
		iPackedDrawable2 = 0
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646145
		iPackedDrawable2 = 0
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 646667
		iPackedDrawable2 = 96
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152579
		iPackedDrawable2 = 16
		iPackedTexture1 = 513
		iPackedTexture2 = 32
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8448
		iPackedDrawable2 = 8192
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_FRONT_DESK_GUARD
		iPackedDrawable1 = 66564
		iPackedDrawable2 = 8448
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66048
		iPackedDrawable2 = 12288
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360712
		iPackedDrawable2 = 48
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 0
		iPackedTexture1 = 65793
		iPackedTexture2 = 16
	BREAK 
	CASE 63
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3072
		iPackedDrawable2 = 0
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8192
		iPackedDrawable2 = 4096
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69890
		iPackedDrawable2 = 0
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 16385
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5377
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3072
		iPackedDrawable2 = 0
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 345097
		iPackedDrawable2 = 80
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287497
		iPackedDrawable2 = 96
		iPackedTexture1 = 131073
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 8450
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 70144
		iPackedTexture2 = 16
	BREAK 
	CASE 73
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66052
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4610
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 139778
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 292100
		iPackedDrawable2 = 0
		iPackedTexture1 = 208897
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4864
		iPackedDrawable2 = 8192
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70145
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 131586
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296967
		iPackedDrawable2 = 48
		iPackedTexture1 = 12545
		iPackedTexture2 = 16
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650763
		iPackedDrawable2 = 96
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 131585
		iPackedTexture2 = 16
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 514
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 73986
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504073
		iPackedDrawable2 = 0
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 4352
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9217
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 100
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70148
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73985
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66051
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 513
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 217859
		iPackedDrawable2 = 0
		iPackedTexture1 = 513
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 515
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 135425
		iPackedTexture2 = 48
	BREAK 
	CASE 112
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70148
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1282
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 115
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226054
		iPackedDrawable2 = 96
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65792
		iPackedDrawable2 = 8192
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 12288
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 119
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SECURITY_GUARD_2
		iPackedDrawable1 = 66308
		iPackedDrawable2 = 8448
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_5(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 74240
		iPackedTexture2 = 48
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1025
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70913
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 9
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3072
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 135424
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 135425
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 74240
		iPackedTexture2 = 16
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221959
		iPackedDrawable2 = 96
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69634
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 66050
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70145
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297223
		iPackedDrawable2 = 48
		iPackedTexture1 = 135425
		iPackedTexture2 = 16
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 147458
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728330
		iPackedDrawable2 = 0
		iPackedTexture1 = 65794
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8962
		iPackedDrawable2 = 0
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 213505
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8707
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66050
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 288004
		iPackedDrawable2 = 0
		iPackedTexture1 = 196610
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226055
		iPackedDrawable2 = 96
		iPackedTexture1 = 65538
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_CAROL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 151552
		iPackedTexture2 = 0
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 45
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287495
		iPackedDrawable2 = 96
		iPackedTexture1 = 131073
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 258
		iPackedDrawable2 = 0
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 139521
		iPackedTexture2 = 16
	BREAK 
	CASE 50
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69888
		iPackedDrawable2 = 16384
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707083
		iPackedDrawable2 = 0
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694795
		iPackedDrawable2 = 0
		iPackedTexture1 = 344576
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 257
		iPackedTexture2 = 16
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 540426
		iPackedDrawable2 = 0
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 74242
		iPackedTexture2 = 16
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 12288
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_FRONT_DESK_GUARD
		iPackedDrawable1 = 66564
		iPackedDrawable2 = 8448
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 4096
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 513
		iPackedTexture2 = 16
	BREAK 
	CASE 63
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 77825
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 8192
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 4354
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 131586
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707342
		iPackedDrawable2 = 0
		iPackedTexture1 = 86272
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70148
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8708
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 75010
		iPackedDrawable2 = 0
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222980
		iPackedDrawable2 = 32
		iPackedTexture1 = 8448
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4866
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65794
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226054
		iPackedDrawable2 = 96
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66051
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 143872
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 139777
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 436744
		iPackedDrawable2 = 64
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 70144
		iPackedTexture2 = 16
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301318
		iPackedDrawable2 = 48
		iPackedTexture1 = 213505
		iPackedTexture2 = 16
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8708
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3072
		iPackedDrawable2 = 0
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 147457
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 292100
		iPackedDrawable2 = 0
		iPackedTexture1 = 200704
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 144389
		iPackedDrawable2 = 80
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1025
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646145
		iPackedDrawable2 = 0
		iPackedTexture1 = 208896
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650763
		iPackedDrawable2 = 96
		iPackedTexture1 = 70145
		iPackedTexture2 = 0
	BREAK 
	CASE 100
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 406537
		iPackedDrawable2 = 48
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70146
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 345097
		iPackedDrawable2 = 48
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70913
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66051
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 584971
		iPackedDrawable2 = 96
		iPackedTexture1 = 131586
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 425992
		iPackedDrawable2 = 48
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 131072
		iPackedTexture2 = 32
	BREAK 
	CASE 112
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360712
		iPackedDrawable2 = 48
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 12288
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 115
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66050
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 436744
		iPackedDrawable2 = 64
		iPackedTexture1 = 139776
		iPackedTexture2 = 16
	BREAK 
	CASE 119
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SECURITY_GUARD_2
		iPackedDrawable1 = 66308
		iPackedDrawable2 = 8448
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_6(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 74240
		iPackedTexture2 = 48
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1281
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5121
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 9
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728586
		iPackedDrawable2 = 0
		iPackedTexture1 = 8449
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 695051
		iPackedDrawable2 = 0
		iPackedTexture1 = 143360
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 196865
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301063
		iPackedDrawable2 = 48
		iPackedTexture1 = 512
		iPackedTexture2 = 16
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73984
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 55308
		iPackedDrawable2 = 0
		iPackedTexture1 = 86530
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287495
		iPackedDrawable2 = 96
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 580875
		iPackedDrawable2 = 96
		iPackedTexture1 = 65792
		iPackedTexture2 = 0
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 131073
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 73728
		iPackedTexture2 = 16
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287748
		iPackedDrawable2 = 0
		iPackedTexture1 = 135426
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70147
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69634
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 86272
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 227077
		iPackedDrawable2 = 32
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9474
		iPackedDrawable2 = 0
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360712
		iPackedDrawable2 = 48
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221959
		iPackedDrawable2 = 96
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 152323
		iPackedDrawable2 = 0
		iPackedTexture1 = 4353
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 515
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_CAROL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 513
		iPackedTexture2 = 0
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 143362
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70400
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 45
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74242
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8448
		iPackedDrawable2 = 16384
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 213507
		iPackedDrawable2 = 0
		iPackedTexture1 = 65792
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152835
		iPackedDrawable2 = 16
		iPackedTexture1 = 4097
		iPackedTexture2 = 32
	BREAK 
	CASE 50
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 70145
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707083
		iPackedDrawable2 = 0
		iPackedTexture1 = 4354
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 135424
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 278528
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 70145
		iPackedTexture2 = 16
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65794
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_FRONT_DESK_GUARD
		iPackedDrawable1 = 66564
		iPackedDrawable2 = 8448
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 12288
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4612
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 585227
		iPackedDrawable2 = 96
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 16385
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 258
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 8449
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707083
		iPackedDrawable2 = 0
		iPackedTexture1 = 344577
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9474
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 425992
		iPackedDrawable2 = 48
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69634
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69888
		iPackedTexture2 = 16
	BREAK 
	CASE 73
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66050
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73728
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 345097
		iPackedDrawable2 = 80
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74244
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728586
		iPackedDrawable2 = 0
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 81920
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4610
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 16
		iPackedTexture1 = 73985
		iPackedTexture2 = 16
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 73984
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222980
		iPackedDrawable2 = 32
		iPackedTexture1 = 135424
		iPackedTexture2 = 0
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504073
		iPackedDrawable2 = 0
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 143361
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 147968
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226054
		iPackedDrawable2 = 96
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 12800
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 135169
		iPackedTexture2 = 16
	BREAK 
	CASE 100
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70144
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 515
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 406537
		iPackedDrawable2 = 48
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5121
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66817
		iPackedDrawable2 = 0
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69632
		iPackedDrawable2 = 16384
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 4354
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 736522
		iPackedDrawable2 = 0
		iPackedTexture1 = 65794
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66052
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152578
		iPackedDrawable2 = 16
		iPackedTexture1 = 131328
		iPackedTexture2 = 48
	BREAK 
	CASE 112
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 131585
		iPackedTexture2 = 0
	BREAK 
	CASE 115
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 4096
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73986
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 585227
		iPackedDrawable2 = 96
		iPackedTexture1 = 4610
		iPackedTexture2 = 0
	BREAK 
	CASE 119
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 77825
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SECURITY_GUARD_2
		iPackedDrawable1 = 66308
		iPackedDrawable2 = 8448
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_7(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728842
		iPackedDrawable2 = 0
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69634
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73985
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 131330
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 0
		iPackedTexture1 = 512
		iPackedTexture2 = 0
	BREAK 
	CASE 9
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1281
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66048
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 292101
		iPackedDrawable2 = 0
		iPackedTexture1 = 143361
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 73985
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4612
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70400
		iPackedDrawable2 = 16384
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221959
		iPackedDrawable2 = 96
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152579
		iPackedDrawable2 = 16
		iPackedTexture1 = 73984
		iPackedTexture2 = 48
	BREAK 
	CASE 19
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139777
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 645388
		iPackedDrawable2 = 0
		iPackedTexture1 = 217600
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 139776
		iPackedTexture2 = 16
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 581131
		iPackedDrawable2 = 96
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 256
		iPackedTexture2 = 16
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 81921
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 695051
		iPackedDrawable2 = 0
		iPackedTexture1 = 151810
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 440840
		iPackedDrawable2 = 64
		iPackedTexture1 = 135424
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1282
		iPackedDrawable2 = 0
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 77824
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 16
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74244
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 513
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4610
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 55309
		iPackedDrawable2 = 0
		iPackedTexture1 = 131584
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 736522
		iPackedDrawable2 = 0
		iPackedTexture1 = 135682
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 131072
		iPackedTexture2 = 16
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 8193
		iPackedTexture2 = 16
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650507
		iPackedDrawable2 = 96
		iPackedTexture1 = 4609
		iPackedTexture2 = 0
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646145
		iPackedDrawable2 = 0
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287748
		iPackedDrawable2 = 0
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_BLANE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 514
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74244
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226054
		iPackedDrawable2 = 96
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74242
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 4608
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74240
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 425992
		iPackedDrawable2 = 48
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_DEALER_FEMALE_4
		iPackedDrawable1 = 78338
		iPackedDrawable2 = 12816
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_DEALER_FEMALE_5
		iPackedDrawable1 = 66307
		iPackedDrawable2 = 272
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_DEALER_FEMALE_2
		iPackedDrawable1 = 4353
		iPackedDrawable2 = 4608
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73986
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_CURTIS
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4611
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 131586
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 12546
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 139778
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_TAYLOR
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_LAUREN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 152323
		iPackedDrawable2 = 0
		iPackedTexture1 = 66049
		iPackedTexture2 = 0
	BREAK 
	CASE 81
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 147458
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 646667
		iPackedDrawable2 = 96
		iPackedTexture1 = 65794
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 131330
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_USHI
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 8192
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 131329
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 90
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301063
		iPackedDrawable2 = 48
		iPackedTexture1 = 4609
		iPackedTexture2 = 16
	BREAK 
	CASE 91
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 12288
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222981
		iPackedDrawable2 = 32
		iPackedTexture1 = 8448
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5121
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148738
		iPackedDrawable2 = 16
		iPackedTexture1 = 135424
		iPackedTexture2 = 32
	BREAK 
	CASE 95
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3072
		iPackedDrawable2 = 0
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301062
		iPackedDrawable2 = 48
		iPackedTexture1 = 86016
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 292101
		iPackedDrawable2 = 0
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 0
		iPackedTexture1 = 139265
		iPackedTexture2 = 16
	BREAK 
	CASE 100
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 8705
		iPackedTexture2 = 16
	BREAK 
	CASE 102
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296967
		iPackedDrawable2 = 48
		iPackedTexture1 = 143616
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66048
		iPackedDrawable2 = 16384
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 516
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 341001
		iPackedDrawable2 = 80
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74243
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4098
		iPackedDrawable2 = 0
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70145
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 78081
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 226820
		iPackedDrawable2 = 32
		iPackedTexture1 = 139776
		iPackedTexture2 = 0
	BREAK 
	CASE 115
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66817
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8449
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 124
	//Ped type: CASINO_SECURITY_GUARD_6
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20480
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_8(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73984
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74240
		iPackedDrawable2 = 12288
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152579
		iPackedDrawable2 = 16
		iPackedTexture1 = 73984
		iPackedTexture2 = 48
	BREAK 
	CASE 9
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70401
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70144
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 143361
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56320
		iPackedDrawable2 = 0
		iPackedTexture1 = 77825
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301062
		iPackedDrawable2 = 48
		iPackedTexture1 = 86016
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 65792
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 70145
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694795
		iPackedDrawable2 = 0
		iPackedTexture1 = 348673
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139777
		iPackedTexture2 = 0
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 135426
		iPackedTexture2 = 16
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152835
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 32
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 86018
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 8193
		iPackedTexture2 = 16
	BREAK 
	CASE 30
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66051
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8449
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 531978
		iPackedDrawable2 = 0
		iPackedTexture1 = 8704
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 581131
		iPackedDrawable2 = 96
		iPackedTexture1 = 139776
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4610
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 131073
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 139777
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 143360
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 258
		iPackedTexture2 = 16
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 256
		iPackedTexture2 = 16
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694795
		iPackedDrawable2 = 0
		iPackedTexture1 = 278786
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 77824
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 81921
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_BLANE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 425992
		iPackedDrawable2 = 48
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8961
		iPackedDrawable2 = 0
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 513
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221959
		iPackedDrawable2 = 96
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221959
		iPackedDrawable2 = 96
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1282
		iPackedDrawable2 = 0
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66561
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 349193
		iPackedDrawable2 = 48
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 143361
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_DEALER_FEMALE_4
		iPackedDrawable1 = 78338
		iPackedDrawable2 = 12816
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_DEALER_FEMALE_5
		iPackedDrawable1 = 66307
		iPackedDrawable2 = 272
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_DEALER_FEMALE_2
		iPackedDrawable1 = 4353
		iPackedDrawable2 = 4608
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69890
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_CURTIS
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 131330
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646145
		iPackedDrawable2 = 0
		iPackedTexture1 = 16385
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707339
		iPackedDrawable2 = 0
		iPackedTexture1 = 86274
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_TAYLOR
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_LAUREN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 131328
		iPackedTexture2 = 0
	BREAK 
	CASE 81
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707086
		iPackedDrawable2 = 0
		iPackedTexture1 = 135425
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 16
		iPackedTexture1 = 8705
		iPackedTexture2 = 16
	BREAK 
	CASE 83
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8707
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226055
		iPackedDrawable2 = 96
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_USHI
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 4096
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 540426
		iPackedDrawable2 = 0
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 151552
		iPackedTexture2 = 0
	BREAK 
	CASE 90
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 371208
		iPackedDrawable2 = 64
		iPackedTexture1 = 70144
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 16384
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 8192
		iPackedTexture2 = 16
	BREAK 
	CASE 93
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 131328
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222725
		iPackedDrawable2 = 32
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728330
		iPackedDrawable2 = 0
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297222
		iPackedDrawable2 = 48
		iPackedTexture1 = 196865
		iPackedTexture2 = 16
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 213507
		iPackedDrawable2 = 0
		iPackedTexture1 = 135425
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 100
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 440840
		iPackedDrawable2 = 64
		iPackedTexture1 = 65793
		iPackedTexture2 = 16
	BREAK 
	CASE 101
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 584971
		iPackedDrawable2 = 96
		iPackedTexture1 = 8450
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 135426
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 226820
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66816
		iPackedDrawable2 = 0
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360456
		iPackedDrawable2 = 48
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70148
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 144389
		iPackedDrawable2 = 80
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8192
		iPackedDrawable2 = 16384
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 4608
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 209408
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 55308
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 256
		iPackedDrawable2 = 0
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 115
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8962
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 124
	//Ped type: CASINO_SECURITY_GUARD_6
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20480
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_9(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_2
		iPackedDrawable1 = 66308
		iPackedDrawable2 = 8448
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 4
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301062
		iPackedDrawable2 = 48
		iPackedTexture1 = 86528
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152835
		iPackedDrawable2 = 16
		iPackedTexture1 = 139776
		iPackedTexture2 = 48
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 4608
		iPackedTexture2 = 16
	BREAK 
	CASE 9
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70400
		iPackedDrawable2 = 16384
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70147
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_CAROL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 78081
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226054
		iPackedDrawable2 = 96
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 513
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 16
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650763
		iPackedDrawable2 = 96
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 645389
		iPackedDrawable2 = 0
		iPackedTexture1 = 205057
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707086
		iPackedDrawable2 = 0
		iPackedTexture1 = 344320
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 66048
		iPackedTexture2 = 16
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 584971
		iPackedDrawable2 = 96
		iPackedTexture1 = 131329
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 70144
		iPackedTexture2 = 16
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 152323
		iPackedDrawable2 = 0
		iPackedTexture1 = 4608
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 77826
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 436744
		iPackedDrawable2 = 64
		iPackedTexture1 = 135424
		iPackedTexture2 = 16
	BREAK 
	CASE 30
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66048
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70657
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 540170
		iPackedDrawable2 = 0
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297223
		iPackedDrawable2 = 48
		iPackedTexture1 = 135169
		iPackedTexture2 = 16
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 213507
		iPackedDrawable2 = 0
		iPackedTexture1 = 131585
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 135424
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139776
		iPackedTexture2 = 16
	BREAK 
	CASE 40
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 371208
		iPackedDrawable2 = 64
		iPackedTexture1 = 66048
		iPackedTexture2 = 16
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222725
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 131072
		iPackedTexture2 = 32
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 292100
		iPackedDrawable2 = 0
		iPackedTexture1 = 208897
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 539914
		iPackedDrawable2 = 0
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_BLANE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 513
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70144
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226054
		iPackedDrawable2 = 96
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 16
		iPackedTexture1 = 139777
		iPackedTexture2 = 16
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 425992
		iPackedDrawable2 = 48
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66050
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 143362
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8708
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222725
		iPackedDrawable2 = 32
		iPackedTexture1 = 65792
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 410633
		iPackedDrawable2 = 80
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_DEALER_FEMALE_3
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 8192
		iPackedTexture1 = 12288
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_DEALER_MALE_2
		iPackedDrawable1 = 1042
		iPackedDrawable2 = 4624
		iPackedTexture1 = 16386
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_DEALER_FEMALE_2
		iPackedDrawable1 = 4353
		iPackedDrawable2 = 4608
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 131073
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4096
		iPackedDrawable2 = 0
		iPackedTexture1 = 135681
		iPackedTexture2 = 16
	BREAK 
	CASE 73
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 74242
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 131073
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_USHI
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 81
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 4352
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 16385
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_CURTIS
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360712
		iPackedDrawable2 = 48
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70146
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9472
		iPackedDrawable2 = 0
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 736522
		iPackedDrawable2 = 0
		iPackedTexture1 = 135425
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 90
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301062
		iPackedDrawable2 = 48
		iPackedTexture1 = 212993
		iPackedTexture2 = 16
	BREAK 
	CASE 91
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707342
		iPackedDrawable2 = 0
		iPackedTexture1 = 209153
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 256
		iPackedDrawable2 = 0
		iPackedTexture1 = 4608
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 258
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 695051
		iPackedDrawable2 = 0
		iPackedTexture1 = 270338
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 8706
		iPackedTexture2 = 16
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 86530
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 288004
		iPackedDrawable2 = 0
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 100
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222981
		iPackedDrawable2 = 32
		iPackedTexture1 = 135424
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650763
		iPackedDrawable2 = 96
		iPackedTexture1 = 74242
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 20480
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 2
		iPackedTexture2 = 16
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 74242
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 345097
		iPackedDrawable2 = 80
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 8705
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5121
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728842
		iPackedDrawable2 = 0
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 65537
		iPackedTexture2 = 16
	BREAK 
	CASE 115
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73985
		iPackedDrawable2 = 0
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 74241
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 4096
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5378
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 119
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 516
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 124
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_10(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152835
		iPackedDrawable2 = 16
		iPackedTexture1 = 73728
		iPackedTexture2 = 48
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 512
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301062
		iPackedDrawable2 = 48
		iPackedTexture1 = 82176
		iPackedTexture2 = 0
	BREAK 
	CASE 9
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 16
		iPackedTexture1 = 70146
		iPackedTexture2 = 16
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 213507
		iPackedDrawable2 = 0
		iPackedTexture1 = 70144
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 695051
		iPackedDrawable2 = 0
		iPackedTexture1 = 81922
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74754
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296967
		iPackedDrawable2 = 48
		iPackedTexture1 = 143360
		iPackedTexture2 = 16
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70146
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 75008
		iPackedDrawable2 = 8192
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70401
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 513
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139776
		iPackedTexture2 = 0
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 205058
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 8449
		iPackedTexture2 = 0
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 12290
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221959
		iPackedDrawable2 = 96
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 152067
		iPackedDrawable2 = 0
		iPackedTexture1 = 73985
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 513
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 516
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 539914
		iPackedDrawable2 = 0
		iPackedTexture1 = 4353
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 131584
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 65538
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728330
		iPackedDrawable2 = 0
		iPackedTexture1 = 73730
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152579
		iPackedDrawable2 = 16
		iPackedTexture1 = 4609
		iPackedTexture2 = 32
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148738
		iPackedDrawable2 = 16
		iPackedTexture1 = 131328
		iPackedTexture2 = 48
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301062
		iPackedDrawable2 = 48
		iPackedTexture1 = 217601
		iPackedTexture2 = 16
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 288005
		iPackedDrawable2 = 0
		iPackedTexture1 = 204800
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69890
		iPackedTexture2 = 16
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707342
		iPackedDrawable2 = 0
		iPackedTexture1 = 16640
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287495
		iPackedDrawable2 = 96
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650763
		iPackedDrawable2 = 96
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74243
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1282
		iPackedDrawable2 = 0
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360456
		iPackedDrawable2 = 48
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 81921
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226054
		iPackedDrawable2 = 96
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 0
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4354
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 349193
		iPackedDrawable2 = 80
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 514
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707086
		iPackedDrawable2 = 0
		iPackedTexture1 = 270849
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_DEALER_FEMALE_4
		iPackedDrawable1 = 78338
		iPackedDrawable2 = 12816
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_DEALER_FEMALE_5
		iPackedDrawable1 = 66307
		iPackedDrawable2 = 272
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_DEALER_FEMALE_2
		iPackedDrawable1 = 4353
		iPackedDrawable2 = 4608
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 4096
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_CURTIS
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70147
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 12288
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 135425
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_TAYLOR
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_LAUREN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707083
		iPackedDrawable2 = 0
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 81
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 77826
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74244
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_USHI
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 1
		iPackedTexture2 = 16
	BREAK 
	CASE 93
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 74240
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65538
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4611
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 227077
		iPackedDrawable2 = 32
		iPackedTexture1 = 74240
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56320
		iPackedDrawable2 = 0
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 4609
		iPackedTexture2 = 16
	BREAK 
	CASE 100
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 440840
		iPackedDrawable2 = 64
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3072
		iPackedDrawable2 = 0
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504073
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66052
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70401
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 425992
		iPackedDrawable2 = 48
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9472
		iPackedDrawable2 = 0
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646144
		iPackedDrawable2 = 0
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 135681
		iPackedTexture2 = 16
	BREAK 
	CASE 114
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 115
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 8449
		iPackedTexture2 = 16
	BREAK 
	CASE 123
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9474
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 124
	//Ped type: CASINO_SECURITY_GUARD_6
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20480
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_11(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152579
		iPackedDrawable2 = 16
		iPackedTexture1 = 65792
		iPackedTexture2 = 48
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66561
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 135426
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301318
		iPackedDrawable2 = 48
		iPackedTexture1 = 77824
		iPackedTexture2 = 0
	BREAK 
	CASE 9
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3072
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 139521
		iPackedTexture2 = 16
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728586
		iPackedDrawable2 = 0
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694795
		iPackedDrawable2 = 0
		iPackedTexture1 = 327681
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69888
		iPackedDrawable2 = 8192
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222724
		iPackedDrawable2 = 32
		iPackedTexture1 = 65792
		iPackedTexture2 = 0
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69890
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 8704
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 349193
		iPackedDrawable2 = 48
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 256
		iPackedTexture2 = 16
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 70144
		iPackedTexture2 = 0
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226054
		iPackedDrawable2 = 96
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 0
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 217088
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296967
		iPackedDrawable2 = 48
		iPackedTexture1 = 135681
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8194
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66051
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226054
		iPackedDrawable2 = 96
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8708
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 16386
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 16
	BREAK 
	CASE 40
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 131329
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139776
		iPackedTexture2 = 0
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650507
		iPackedDrawable2 = 96
		iPackedTexture1 = 131329
		iPackedTexture2 = 0
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 86016
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222725
		iPackedDrawable2 = 32
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 77824
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 440840
		iPackedDrawable2 = 64
		iPackedTexture1 = 139521
		iPackedTexture2 = 16
	BREAK 
	CASE 50
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70146
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 78082
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 513
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8960
		iPackedDrawable2 = 4096
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66818
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 341001
		iPackedDrawable2 = 80
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 147969
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_DEALER_FEMALE_4
		iPackedDrawable1 = 78338
		iPackedDrawable2 = 12816
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_DEALER_FEMALE_5
		iPackedDrawable1 = 66307
		iPackedDrawable2 = 272
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_DEALER_FEMALE_2
		iPackedDrawable1 = 4353
		iPackedDrawable2 = 4608
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_CURTIS
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 144389
		iPackedDrawable2 = 80
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 8192
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 86017
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_TAYLOR
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_LAUREN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 70146
		iPackedTexture2 = 0
	BREAK 
	CASE 81
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 147456
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 147457
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 425992
		iPackedDrawable2 = 48
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70144
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_USHI
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 0
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 736522
		iPackedDrawable2 = 0
		iPackedTexture1 = 131585
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1025
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74243
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 66048
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221959
		iPackedDrawable2 = 96
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 0
		iPackedTexture1 = 70145
		iPackedTexture2 = 16
	BREAK 
	CASE 97
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287494
		iPackedDrawable2 = 96
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 135680
		iPackedTexture2 = 32
	BREAK 
	CASE 100
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301062
		iPackedDrawable2 = 48
		iPackedTexture1 = 212993
		iPackedTexture2 = 16
	BREAK 
	CASE 101
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 695051
		iPackedDrawable2 = 0
		iPackedTexture1 = 282624
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360712
		iPackedDrawable2 = 48
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 213763
		iPackedDrawable2 = 0
		iPackedTexture1 = 512
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504073
		iPackedDrawable2 = 0
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4611
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 8448
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 144389
		iPackedDrawable2 = 80
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 514
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74244
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5378
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 55564
		iPackedDrawable2 = 0
		iPackedTexture1 = 513
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 8704
		iPackedTexture2 = 16
	BREAK 
	CASE 114
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 514
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 115
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 371208
		iPackedDrawable2 = 64
		iPackedTexture1 = 512
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 12288
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 124
	//Ped type: CASINO_SECURITY_GUARD_6
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20480
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_12(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 65792
		iPackedTexture2 = 48
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65538
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9217
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296966
		iPackedDrawable2 = 48
		iPackedTexture1 = 65792
		iPackedTexture2 = 0
	BREAK 
	CASE 9
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152835
		iPackedDrawable2 = 16
		iPackedTexture1 = 8193
		iPackedTexture2 = 32
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56320
		iPackedDrawable2 = 0
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 4610
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 75008
		iPackedDrawable2 = 16384
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297223
		iPackedDrawable2 = 48
		iPackedTexture1 = 143873
		iPackedTexture2 = 16
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221959
		iPackedDrawable2 = 96
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8450
		iPackedDrawable2 = 0
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 65538
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 12289
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 349193
		iPackedDrawable2 = 48
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222980
		iPackedDrawable2 = 32
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 151554
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 135168
		iPackedTexture2 = 16
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646145
		iPackedDrawable2 = 0
		iPackedTexture1 = 208897
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74244
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 135425
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 288004
		iPackedDrawable2 = 0
		iPackedTexture1 = 143360
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 74241
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4864
		iPackedDrawable2 = 12288
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66050
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 645133
		iPackedDrawable2 = 0
		iPackedTexture1 = 74241
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 65794
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 152067
		iPackedDrawable2 = 0
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 147457
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139776
		iPackedTexture2 = 0
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 8705
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148482
		iPackedDrawable2 = 16
		iPackedTexture1 = 135681
		iPackedTexture2 = 32
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 645132
		iPackedDrawable2 = 0
		iPackedTexture1 = 151810
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 585227
		iPackedDrawable2 = 96
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 8450
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226055
		iPackedDrawable2 = 96
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 227077
		iPackedDrawable2 = 32
		iPackedTexture1 = 135424
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 73730
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 0
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 736778
		iPackedDrawable2 = 0
		iPackedTexture1 = 131330
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66051
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70914
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 144389
		iPackedDrawable2 = 80
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 77824
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_DEALER_FEMALE_4
		iPackedDrawable1 = 78338
		iPackedDrawable2 = 12816
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_DEALER_FEMALE_5
		iPackedDrawable1 = 66307
		iPackedDrawable2 = 272
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_DEALER_FEMALE_2
		iPackedDrawable1 = 4353
		iPackedDrawable2 = 4608
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135682
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_CURTIS
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66051
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65792
		iPackedDrawable2 = 4096
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 695051
		iPackedDrawable2 = 0
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 73984
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_TAYLOR
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_LAUREN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 131585
		iPackedTexture2 = 0
	BREAK 
	CASE 81
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287748
		iPackedDrawable2 = 0
		iPackedTexture1 = 197121
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 65794
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 516
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_USHI
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694795
		iPackedDrawable2 = 0
		iPackedTexture1 = 78337
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 65538
		iPackedTexture2 = 0
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 2
		iPackedDrawable2 = 0
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287495
		iPackedDrawable2 = 96
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69633
		iPackedTexture2 = 16
	BREAK 
	CASE 93
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4612
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152578
		iPackedDrawable2 = 16
		iPackedTexture1 = 131072
		iPackedTexture2 = 48
	BREAK 
	CASE 97
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 425992
		iPackedDrawable2 = 48
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 20482
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 440840
		iPackedDrawable2 = 64
		iPackedTexture1 = 65792
		iPackedTexture2 = 0
	BREAK 
	CASE 100
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 257
		iPackedTexture2 = 16
	BREAK 
	CASE 101
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728330
		iPackedDrawable2 = 0
		iPackedTexture1 = 513
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 414729
		iPackedDrawable2 = 80
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74242
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74753
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287494
		iPackedDrawable2 = 96
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4610
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360712
		iPackedDrawable2 = 48
		iPackedTexture1 = 73730
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73986
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 292101
		iPackedDrawable2 = 0
		iPackedTexture1 = 143360
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301318
		iPackedDrawable2 = 48
		iPackedTexture1 = 209153
		iPackedTexture2 = 16
	BREAK 
	CASE 114
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 144389
		iPackedDrawable2 = 80
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 115
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8192
		iPackedDrawable2 = 16384
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 124
	//Ped type: CASINO_SECURITY_GUARD_6
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20480
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_13(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 81922
		iPackedTexture2 = 0
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70402
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8194
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 139776
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152835
		iPackedDrawable2 = 16
		iPackedTexture1 = 74240
		iPackedTexture2 = 48
	BREAK 
	CASE 9
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70145
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 151552
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4611
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301062
		iPackedDrawable2 = 48
		iPackedTexture1 = 86528
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301063
		iPackedDrawable2 = 48
		iPackedTexture1 = 4352
		iPackedTexture2 = 16
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 8704
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287748
		iPackedDrawable2 = 0
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152835
		iPackedDrawable2 = 16
		iPackedTexture1 = 4353
		iPackedTexture2 = 32
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 4353
		iPackedTexture2 = 16
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 131073
		iPackedTexture2 = 16
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 16385
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 531722
		iPackedDrawable2 = 0
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148482
		iPackedDrawable2 = 16
		iPackedTexture1 = 139777
		iPackedTexture2 = 32
	BREAK 
	CASE 30
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 414729
		iPackedDrawable2 = 80
		iPackedTexture1 = 131073
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70912
		iPackedDrawable2 = 4096
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707342
		iPackedDrawable2 = 0
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 512
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 353033
		iPackedDrawable2 = 96
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66048
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139522
		iPackedTexture2 = 16
	BREAK 
	CASE 40
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 65538
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222724
		iPackedDrawable2 = 32
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 436744
		iPackedDrawable2 = 64
		iPackedTexture1 = 135681
		iPackedTexture2 = 16
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 8449
		iPackedTexture2 = 0
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287748
		iPackedDrawable2 = 0
		iPackedTexture1 = 139778
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 196608
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728330
		iPackedDrawable2 = 0
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_BLANE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 514
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74244
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226054
		iPackedDrawable2 = 96
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646145
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 516
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_FRONT_DESK_GUARD
		iPackedDrawable1 = 66564
		iPackedDrawable2 = 8448
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 131329
		iPackedTexture2 = 16
	BREAK 
	CASE 61
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 695054
		iPackedDrawable2 = 0
		iPackedTexture1 = 20737
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 135682
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 4353
		iPackedTexture2 = 32
	BREAK 
	CASE 67
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66818
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74244
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 371208
		iPackedDrawable2 = 64
		iPackedTexture1 = 73984
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 77825
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 131586
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 514
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 585227
		iPackedDrawable2 = 96
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 288004
		iPackedDrawable2 = 0
		iPackedTexture1 = 196609
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70146
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1024
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360456
		iPackedDrawable2 = 48
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 131330
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 440840
		iPackedDrawable2 = 64
		iPackedTexture1 = 73729
		iPackedTexture2 = 16
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 227077
		iPackedDrawable2 = 32
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297222
		iPackedDrawable2 = 48
		iPackedTexture1 = 196609
		iPackedTexture2 = 0
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66051
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69889
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8961
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707339
		iPackedDrawable2 = 0
		iPackedTexture1 = 73730
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8450
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 55309
		iPackedDrawable2 = 0
		iPackedTexture1 = 78080
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 135680
		iPackedTexture2 = 16
	BREAK 
	CASE 100
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 410633
		iPackedDrawable2 = 48
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74243
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69634
		iPackedDrawable2 = 0
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69632
		iPackedDrawable2 = 12288
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 2
		iPackedTexture2 = 16
	BREAK 
	CASE 106
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56320
		iPackedDrawable2 = 0
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 645132
		iPackedDrawable2 = 0
		iPackedTexture1 = 20482
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 8449
		iPackedTexture2 = 16
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4612
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73985
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74240
		iPackedDrawable2 = 16384
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148482
		iPackedDrawable2 = 16
		iPackedTexture1 = 131584
		iPackedTexture2 = 48
	BREAK 
	CASE 115
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 514
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 217859
		iPackedDrawable2 = 0
		iPackedTexture1 = 70145
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1282
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 16384
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 119
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SECURITY_GUARD_2
		iPackedDrawable1 = 66308
		iPackedDrawable2 = 8448
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_14(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707083
		iPackedDrawable2 = 0
		iPackedTexture1 = 131584
		iPackedTexture2 = 0
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70914
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 74242
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73985
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70656
		iPackedDrawable2 = 12288
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152579
		iPackedDrawable2 = 16
		iPackedTexture1 = 73984
		iPackedTexture2 = 48
	BREAK 
	CASE 9
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70145
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504073
		iPackedDrawable2 = 0
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 341001
		iPackedDrawable2 = 80
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9472
		iPackedDrawable2 = 16384
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74243
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301062
		iPackedDrawable2 = 48
		iPackedTexture1 = 77824
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148738
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 32
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 16
		iPackedTexture1 = 4353
		iPackedTexture2 = 16
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139776
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 256
		iPackedDrawable2 = 0
		iPackedTexture1 = 139776
		iPackedTexture2 = 0
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 540170
		iPackedDrawable2 = 0
		iPackedTexture1 = 514
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 131586
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296967
		iPackedDrawable2 = 48
		iPackedTexture1 = 4608
		iPackedTexture2 = 16
	BREAK 
	CASE 30
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 418569
		iPackedDrawable2 = 96
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 139777
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222980
		iPackedDrawable2 = 32
		iPackedTexture1 = 70144
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 8449
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 139777
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 143360
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 258
		iPackedTexture2 = 16
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 256
		iPackedTexture2 = 16
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694795
		iPackedDrawable2 = 0
		iPackedTexture1 = 278786
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 77824
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 81921
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_BLANE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 425992
		iPackedDrawable2 = 48
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8961
		iPackedDrawable2 = 0
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 513
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 151553
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 585227
		iPackedDrawable2 = 96
		iPackedTexture1 = 65794
		iPackedTexture2 = 0
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_FRONT_DESK_GUARD
		iPackedDrawable1 = 66564
		iPackedDrawable2 = 8448
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 581131
		iPackedDrawable2 = 96
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504073
		iPackedDrawable2 = 0
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728586
		iPackedDrawable2 = 0
		iPackedTexture1 = 8448
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 4354
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 16
		iPackedTexture1 = 8449
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296966
		iPackedDrawable2 = 48
		iPackedTexture1 = 197121
		iPackedTexture2 = 16
	BREAK 
	CASE 67
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135424
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70913
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4610
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 514
		iPackedTexture2 = 16
	BREAK 
	CASE 71
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 12288
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 0
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8194
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74244
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 436744
		iPackedDrawable2 = 64
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 292100
		iPackedDrawable2 = 0
		iPackedTexture1 = 208897
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 513
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 736778
		iPackedDrawable2 = 0
		iPackedTexture1 = 131585
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 12288
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4611
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74240
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 418569
		iPackedDrawable2 = 48
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5120
		iPackedDrawable2 = 8192
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650507
		iPackedDrawable2 = 96
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 131072
		iPackedTexture2 = 16
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 131584
		iPackedTexture2 = 0
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 65538
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 513
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69890
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 148227
		iPackedDrawable2 = 0
		iPackedTexture1 = 4352
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135681
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 143362
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 8704
		iPackedTexture2 = 16
	BREAK 
	CASE 100
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360456
		iPackedDrawable2 = 48
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70147
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 8448
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 257
		iPackedDrawable2 = 0
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 227077
		iPackedDrawable2 = 32
		iPackedTexture1 = 135424
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 70146
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 66050
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 4097
		iPackedTexture2 = 48
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74240
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1282
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4612
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 410633
		iPackedDrawable2 = 80
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148482
		iPackedDrawable2 = 16
		iPackedTexture1 = 131584
		iPackedTexture2 = 32
	BREAK 
	CASE 115
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 152065
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 16640
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 512
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 8192
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 119
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360456
		iPackedDrawable2 = 48
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SECURITY_GUARD_2
		iPackedDrawable1 = 66308
		iPackedDrawable2 = 8448
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_15(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 143362
		iPackedTexture2 = 0
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5378
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73984
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65794
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4096
		iPackedDrawable2 = 0
		iPackedTexture1 = 256
		iPackedTexture2 = 16
	BREAK 
	CASE 9
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70144
		iPackedDrawable2 = 16384
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 4352
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694795
		iPackedDrawable2 = 0
		iPackedTexture1 = 279041
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287495
		iPackedDrawable2 = 96
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5121
		iPackedDrawable2 = 0
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360712
		iPackedDrawable2 = 48
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 66048
		iPackedTexture2 = 32
	BREAK 
	CASE 19
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297222
		iPackedDrawable2 = 48
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 20480
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 147970
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301063
		iPackedDrawable2 = 48
		iPackedTexture1 = 4609
		iPackedTexture2 = 16
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148482
		iPackedDrawable2 = 16
		iPackedTexture1 = 8449
		iPackedTexture2 = 48
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504073
		iPackedDrawable2 = 0
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 226820
		iPackedDrawable2 = 32
		iPackedTexture1 = 74240
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4611
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 70145
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9472
		iPackedDrawable2 = 8192
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 131586
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 8449
		iPackedTexture2 = 16
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 357129
		iPackedDrawable2 = 48
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 414729
		iPackedDrawable2 = 80
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139776
		iPackedTexture2 = 16
	BREAK 
	CASE 40
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 371208
		iPackedDrawable2 = 64
		iPackedTexture1 = 66048
		iPackedTexture2 = 16
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222725
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 131072
		iPackedTexture2 = 32
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 292100
		iPackedDrawable2 = 0
		iPackedTexture1 = 208897
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 539914
		iPackedDrawable2 = 0
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_BLANE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 513
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70144
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226054
		iPackedDrawable2 = 96
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56320
		iPackedDrawable2 = 0
		iPackedTexture1 = 77825
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 646411
		iPackedDrawable2 = 96
		iPackedTexture1 = 512
		iPackedTexture2 = 0
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 0
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_FRONT_DESK_GUARD
		iPackedDrawable1 = 66564
		iPackedDrawable2 = 8448
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 226820
		iPackedDrawable2 = 32
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 70146
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504073
		iPackedDrawable2 = 0
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 4352
		iPackedTexture2 = 16
	BREAK 
	CASE 65
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8450
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69890
		iPackedDrawable2 = 0
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152578
		iPackedDrawable2 = 16
		iPackedTexture1 = 4609
		iPackedTexture2 = 48
	BREAK 
	CASE 71
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 736778
		iPackedDrawable2 = 0
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8961
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70146
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297223
		iPackedDrawable2 = 48
		iPackedTexture1 = 143616
		iPackedTexture2 = 16
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 147456
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 645133
		iPackedDrawable2 = 0
		iPackedTexture1 = 512
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 8192
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 341001
		iPackedDrawable2 = 48
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360456
		iPackedDrawable2 = 48
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 227077
		iPackedDrawable2 = 32
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301318
		iPackedDrawable2 = 48
		iPackedTexture1 = 209409
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 513
		iPackedTexture2 = 0
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70148
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 65792
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 16384
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 16386
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66561
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 695051
		iPackedDrawable2 = 0
		iPackedTexture1 = 331778
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 16
		iPackedTexture1 = 8449
		iPackedTexture2 = 0
	BREAK 
	CASE 100
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74242
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4611
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 516
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5377
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 646411
		iPackedDrawable2 = 96
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 440840
		iPackedDrawable2 = 64
		iPackedTexture1 = 4096
		iPackedTexture2 = 16
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74243
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 12288
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 2
		iPackedDrawable2 = 0
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8708
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148482
		iPackedDrawable2 = 16
		iPackedTexture1 = 65792
		iPackedTexture2 = 32
	BREAK 
	CASE 115
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 8705
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9474
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 119
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 425992
		iPackedDrawable2 = 48
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SECURITY_GUARD_2
		iPackedDrawable1 = 66308
		iPackedDrawable2 = 8448
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_16(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152579
		iPackedDrawable2 = 16
		iPackedTexture1 = 74240
		iPackedTexture2 = 48
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5378
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 75008
		iPackedDrawable2 = 8192
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 8450
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301062
		iPackedDrawable2 = 48
		iPackedTexture1 = 78080
		iPackedTexture2 = 16
	BREAK 
	CASE 9
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3072
		iPackedDrawable2 = 0
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 8193
		iPackedTexture2 = 16
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694795
		iPackedDrawable2 = 0
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9217
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 66048
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707083
		iPackedDrawable2 = 0
		iPackedTexture1 = 131584
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 345097
		iPackedDrawable2 = 48
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148738
		iPackedDrawable2 = 16
		iPackedTexture1 = 8193
		iPackedTexture2 = 32
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 227076
		iPackedDrawable2 = 32
		iPackedTexture1 = 131584
		iPackedTexture2 = 0
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 695051
		iPackedDrawable2 = 0
		iPackedTexture1 = 278786
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70145
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4864
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 531978
		iPackedDrawable2 = 0
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 512
		iPackedTexture2 = 16
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70145
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426761
		iPackedDrawable2 = 96
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 516
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 144389
		iPackedDrawable2 = 80
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 152067
		iPackedDrawable2 = 0
		iPackedTexture1 = 65793
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 436744
		iPackedDrawable2 = 64
		iPackedTexture1 = 8704
		iPackedTexture2 = 16
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 139776
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 139520
		iPackedTexture2 = 16
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 440840
		iPackedDrawable2 = 64
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 4352
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 515
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_BLANE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360456
		iPackedDrawable2 = 48
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 144389
		iPackedDrawable2 = 80
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 81921
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152579
		iPackedDrawable2 = 16
		iPackedTexture1 = 513
		iPackedTexture2 = 32
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8448
		iPackedDrawable2 = 8192
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_FRONT_DESK_GUARD
		iPackedDrawable1 = 66564
		iPackedDrawable2 = 8448
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66048
		iPackedDrawable2 = 12288
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360712
		iPackedDrawable2 = 48
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 0
		iPackedTexture1 = 65793
		iPackedTexture2 = 16
	BREAK 
	CASE 63
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3072
		iPackedDrawable2 = 0
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8192
		iPackedDrawable2 = 4096
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69890
		iPackedDrawable2 = 0
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 16385
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5377
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3072
		iPackedDrawable2 = 0
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 345097
		iPackedDrawable2 = 80
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287497
		iPackedDrawable2 = 96
		iPackedTexture1 = 131073
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 8450
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 70144
		iPackedTexture2 = 16
	BREAK 
	CASE 73
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66052
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4610
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 139778
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 292100
		iPackedDrawable2 = 0
		iPackedTexture1 = 208897
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4864
		iPackedDrawable2 = 8192
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70145
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 131586
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296967
		iPackedDrawable2 = 48
		iPackedTexture1 = 12545
		iPackedTexture2 = 16
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650763
		iPackedDrawable2 = 96
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 131585
		iPackedTexture2 = 16
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 514
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 73986
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504073
		iPackedDrawable2 = 0
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 4352
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9217
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 100
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70148
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73985
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66051
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 513
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 217859
		iPackedDrawable2 = 0
		iPackedTexture1 = 513
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 515
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 135425
		iPackedTexture2 = 48
	BREAK 
	CASE 112
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70148
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1282
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 115
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226054
		iPackedDrawable2 = 96
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65792
		iPackedDrawable2 = 8192
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 12288
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 119
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SECURITY_GUARD_2
		iPackedDrawable1 = 66308
		iPackedDrawable2 = 8448
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_17(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148483
		iPackedDrawable2 = 16
		iPackedTexture1 = 73728
		iPackedTexture2 = 48
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5378
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73986
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8961
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296966
		iPackedDrawable2 = 48
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 9
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 288004
		iPackedDrawable2 = 0
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 226820
		iPackedDrawable2 = 32
		iPackedTexture1 = 131584
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707342
		iPackedDrawable2 = 0
		iPackedTexture1 = 151809
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 736778
		iPackedDrawable2 = 0
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74752
		iPackedDrawable2 = 16384
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 4097
		iPackedTexture2 = 16
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74240
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 73986
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 513
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148738
		iPackedDrawable2 = 16
		iPackedTexture1 = 4097
		iPackedTexture2 = 32
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 12546
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 512
		iPackedTexture2 = 0
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4612
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 131585
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139522
		iPackedTexture2 = 16
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 513
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 65538
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 341001
		iPackedDrawable2 = 80
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 414729
		iPackedDrawable2 = 80
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728330
		iPackedDrawable2 = 0
		iPackedTexture1 = 65793
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 131328
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301063
		iPackedDrawable2 = 48
		iPackedTexture1 = 139520
		iPackedTexture2 = 16
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69633
		iPackedTexture2 = 16
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4096
		iPackedDrawable2 = 0
		iPackedTexture1 = 65793
		iPackedTexture2 = 16
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 139778
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 371208
		iPackedDrawable2 = 64
		iPackedTexture1 = 69888
		iPackedTexture2 = 16
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 65793
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_BLANE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360456
		iPackedDrawable2 = 48
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70913
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8708
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 78082
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 540426
		iPackedDrawable2 = 0
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 74242
		iPackedTexture2 = 16
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 12288
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_FRONT_DESK_GUARD
		iPackedDrawable1 = 66564
		iPackedDrawable2 = 8448
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 4096
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 513
		iPackedTexture2 = 16
	BREAK 
	CASE 63
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 77825
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 8192
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 4354
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 131586
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707342
		iPackedDrawable2 = 0
		iPackedTexture1 = 86272
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70148
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8708
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 75010
		iPackedDrawable2 = 0
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222980
		iPackedDrawable2 = 32
		iPackedTexture1 = 8448
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4866
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65794
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226054
		iPackedDrawable2 = 96
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66051
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 143872
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 139777
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 436744
		iPackedDrawable2 = 64
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 70144
		iPackedTexture2 = 16
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301318
		iPackedDrawable2 = 48
		iPackedTexture1 = 213505
		iPackedTexture2 = 16
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8708
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3072
		iPackedDrawable2 = 0
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 147457
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 292100
		iPackedDrawable2 = 0
		iPackedTexture1 = 200704
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 144389
		iPackedDrawable2 = 80
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1025
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646145
		iPackedDrawable2 = 0
		iPackedTexture1 = 208896
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650763
		iPackedDrawable2 = 96
		iPackedTexture1 = 70145
		iPackedTexture2 = 0
	BREAK 
	CASE 100
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 406537
		iPackedDrawable2 = 48
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70146
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 345097
		iPackedDrawable2 = 48
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70913
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66051
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 584971
		iPackedDrawable2 = 96
		iPackedTexture1 = 131586
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 425992
		iPackedDrawable2 = 48
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 131072
		iPackedTexture2 = 32
	BREAK 
	CASE 112
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360712
		iPackedDrawable2 = 48
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 12288
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 115
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66050
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 436744
		iPackedDrawable2 = 64
		iPackedTexture1 = 139776
		iPackedTexture2 = 16
	BREAK 
	CASE 119
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SECURITY_GUARD_2
		iPackedDrawable1 = 66308
		iPackedDrawable2 = 8448
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_18(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 70144
		iPackedTexture2 = 48
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 75009
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74240
		iPackedDrawable2 = 12288
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5120
		iPackedDrawable2 = 4096
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301318
		iPackedDrawable2 = 48
		iPackedTexture1 = 86272
		iPackedTexture2 = 0
	BREAK 
	CASE 9
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 16896
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301063
		iPackedDrawable2 = 48
		iPackedTexture1 = 513
		iPackedTexture2 = 16
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3072
		iPackedDrawable2 = 0
		iPackedTexture1 = 73730
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 288004
		iPackedDrawable2 = 0
		iPackedTexture1 = 200704
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 257
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 512
		iPackedTexture2 = 0
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135682
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70401
		iPackedDrawable2 = 0
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 78081
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 4097
		iPackedTexture2 = 32
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 81920
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222724
		iPackedDrawable2 = 32
		iPackedTexture1 = 4352
		iPackedTexture2 = 0
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 55564
		iPackedDrawable2 = 0
		iPackedTexture1 = 135425
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287495
		iPackedDrawable2 = 96
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5378
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139522
		iPackedTexture2 = 16
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73985
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 341001
		iPackedDrawable2 = 80
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66051
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3072
		iPackedDrawable2 = 0
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 65794
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 152067
		iPackedDrawable2 = 0
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 147457
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139776
		iPackedTexture2 = 0
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 8705
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148482
		iPackedDrawable2 = 16
		iPackedTexture1 = 135681
		iPackedTexture2 = 32
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 645132
		iPackedDrawable2 = 0
		iPackedTexture1 = 151810
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 585227
		iPackedDrawable2 = 96
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 8450
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226055
		iPackedDrawable2 = 96
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 227077
		iPackedDrawable2 = 32
		iPackedTexture1 = 135424
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 73730
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 0
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 736778
		iPackedDrawable2 = 0
		iPackedTexture1 = 131330
		iPackedTexture2 = 0
	BREAK
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 278528
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 70145
		iPackedTexture2 = 16
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65794
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_FRONT_DESK_GUARD
		iPackedDrawable1 = 66564
		iPackedDrawable2 = 8448
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 12288
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4612
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 585227
		iPackedDrawable2 = 96
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 16385
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 258
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 8449
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707083
		iPackedDrawable2 = 0
		iPackedTexture1 = 344577
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9474
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 425992
		iPackedDrawable2 = 48
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69634
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69888
		iPackedTexture2 = 16
	BREAK 
	CASE 73
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66050
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73728
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 345097
		iPackedDrawable2 = 80
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74244
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728586
		iPackedDrawable2 = 0
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 81920
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4610
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 16
		iPackedTexture1 = 73985
		iPackedTexture2 = 16
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 73984
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222980
		iPackedDrawable2 = 32
		iPackedTexture1 = 135424
		iPackedTexture2 = 0
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504073
		iPackedDrawable2 = 0
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 143361
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 147968
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226054
		iPackedDrawable2 = 96
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 12800
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 135169
		iPackedTexture2 = 16
	BREAK 
	CASE 100
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70144
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 515
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 406537
		iPackedDrawable2 = 48
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5121
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66817
		iPackedDrawable2 = 0
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69632
		iPackedDrawable2 = 16384
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 4354
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 736522
		iPackedDrawable2 = 0
		iPackedTexture1 = 65794
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66052
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152578
		iPackedDrawable2 = 16
		iPackedTexture1 = 131328
		iPackedTexture2 = 48
	BREAK 
	CASE 112
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 131585
		iPackedTexture2 = 0
	BREAK 
	CASE 115
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 4096
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73986
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 585227
		iPackedDrawable2 = 96
		iPackedTexture1 = 4610
		iPackedTexture2 = 0
	BREAK 
	CASE 119
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 77825
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SECURITY_GUARD_2
		iPackedDrawable1 = 66308
		iPackedDrawable2 = 8448
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_19(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301063
		iPackedDrawable2 = 48
		iPackedTexture1 = 1
		iPackedTexture2 = 16
	BREAK 
	CASE 40
	//Ped type: CASINO_CAROL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 584971
		iPackedDrawable2 = 96
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 43
	//Ped type: CASINO_MANAGER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 695051
		iPackedDrawable2 = 0
		iPackedTexture1 = 143362
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 66049
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56320
		iPackedDrawable2 = 0
		iPackedTexture1 = 81922
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287748
		iPackedDrawable2 = 0
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 531722
		iPackedDrawable2 = 0
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707083
		iPackedDrawable2 = 0
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 645132
		iPackedDrawable2 = 0
		iPackedTexture1 = 135426
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 0
		iPackedTexture2 = 16
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 515
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360712
		iPackedDrawable2 = 48
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74244
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135681
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 16385
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148489
		iPackedDrawable2 = 80
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 226821
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56320
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_DEALER_FEMALE_4
		iPackedDrawable1 = 78338
		iPackedDrawable2 = 12816
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_DEALER_FEMALE_5
		iPackedDrawable1 = 66307
		iPackedDrawable2 = 272
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_DEALER_FEMALE_2
		iPackedDrawable1 = 4353
		iPackedDrawable2 = 4608
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650507
		iPackedDrawable2 = 96
		iPackedTexture1 = 131585
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 292101
		iPackedDrawable2 = 0
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9474
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_CURTIS
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 12288
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_TAYLOR
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646145
		iPackedDrawable2 = 0
		iPackedTexture1 = 143361
		iPackedTexture2 = 0
	BREAK 
	CASE 81
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 4353
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 78336
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 414729
		iPackedDrawable2 = 48
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74243
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_USHI
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4866
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152579
		iPackedDrawable2 = 16
		iPackedTexture1 = 70144
		iPackedTexture2 = 48
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148738
		iPackedDrawable2 = 16
		iPackedTexture1 = 4609
		iPackedTexture2 = 32
	BREAK 
	CASE 90
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297222
		iPackedDrawable2 = 48
		iPackedTexture1 = 205312
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65794
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 0
		iPackedTexture1 = 70145
		iPackedTexture2 = 16
	BREAK 
	CASE 95
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 131585
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152578
		iPackedDrawable2 = 16
		iPackedTexture1 = 139520
		iPackedTexture2 = 32
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 20994
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148483
		iPackedDrawable2 = 16
		iPackedTexture1 = 65537
		iPackedTexture2 = 48
	BREAK 
	CASE 100
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 70145
		iPackedTexture2 = 16
	BREAK 
	CASE 101
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 135424
		iPackedTexture2 = 16
	BREAK 
	CASE 102
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 540426
		iPackedDrawable2 = 0
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 131584
		iPackedTexture2 = 16
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226055
		iPackedDrawable2 = 96
		iPackedTexture1 = 65538
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4610
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287494
		iPackedDrawable2 = 96
		iPackedTexture1 = 73730
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 516
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73985
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1025
		iPackedDrawable2 = 0
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 736778
		iPackedDrawable2 = 0
		iPackedTexture1 = 65793
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287748
		iPackedDrawable2 = 0
		iPackedTexture1 = 200962
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 371208
		iPackedDrawable2 = 64
		iPackedTexture1 = 256
		iPackedTexture2 = 16
	BREAK 
	CASE 115
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 4354
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 213507
		iPackedDrawable2 = 0
		iPackedTexture1 = 4608
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 75008
		iPackedDrawable2 = 0
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 119
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70147
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 121
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 226820
		iPackedDrawable2 = 32
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 124
	//Ped type: CASINO_SECURITY_GUARD_6
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20480
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_20(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3072
		iPackedDrawable2 = 0
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65794
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 65793
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5378
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 9
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4353
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221959
		iPackedDrawable2 = 96
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 0
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70145
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 73984
		iPackedTexture2 = 48
	BREAK 
	CASE 19
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301062
		iPackedDrawable2 = 48
		iPackedTexture1 = 86528
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694795
		iPackedDrawable2 = 0
		iPackedTexture1 = 143872
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 737034
		iPackedDrawable2 = 0
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 139777
		iPackedTexture2 = 16
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 4609
		iPackedTexture2 = 32
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 695051
		iPackedDrawable2 = 0
		iPackedTexture1 = 82177
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 81921
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152578
		iPackedDrawable2 = 16
		iPackedTexture1 = 139776
		iPackedTexture2 = 48
	BREAK 
	CASE 30
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 204802
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 581131
		iPackedDrawable2 = 96
		iPackedTexture1 = 65794
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 144389
		iPackedDrawable2 = 80
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66048
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 66048
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_CAROL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 0
		iPackedTexture2 = 16
	BREAK 
	CASE 43
	//Ped type: CASINO_MANAGER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 131585
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 86018
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 4096
		iPackedTexture2 = 16
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 143361
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 585227
		iPackedDrawable2 = 96
		iPackedTexture1 = 139776
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707086
		iPackedDrawable2 = 0
		iPackedTexture1 = 335872
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707086
		iPackedDrawable2 = 0
		iPackedTexture1 = 266497
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 292101
		iPackedDrawable2 = 0
		iPackedTexture1 = 200704
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 65792
		iPackedTexture2 = 16
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 515
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70145
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8707
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 8448
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70656
		iPackedDrawable2 = 4096
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646145
		iPackedDrawable2 = 0
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 514
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222724
		iPackedDrawable2 = 32
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 144389
		iPackedDrawable2 = 80
		iPackedTexture1 = 131073
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_DEALER_FEMALE_4
		iPackedDrawable1 = 78338
		iPackedDrawable2 = 12816
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_DEALER_FEMALE_5
		iPackedDrawable1 = 66307
		iPackedDrawable2 = 272
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_DEALER_FEMALE_2
		iPackedDrawable1 = 4353
		iPackedDrawable2 = 4608
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 513
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_CURTIS
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70144
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 514
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 55308
		iPackedDrawable2 = 0
		iPackedTexture1 = 8705
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_TAYLOR
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 65794
		iPackedTexture2 = 0
	BREAK 
	CASE 81
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287748
		iPackedDrawable2 = 0
		iPackedTexture1 = 4608
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152835
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 32
	BREAK 
	CASE 83
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 213507
		iPackedDrawable2 = 0
		iPackedTexture1 = 66048
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_USHI
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8960
		iPackedDrawable2 = 16384
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 371208
		iPackedDrawable2 = 64
		iPackedTexture1 = 135425
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297222
		iPackedDrawable2 = 48
		iPackedTexture1 = 204801
		iPackedTexture2 = 16
	BREAK 
	CASE 90
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301063
		iPackedDrawable2 = 48
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707339
		iPackedDrawable2 = 0
		iPackedTexture1 = 16386
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222981
		iPackedDrawable2 = 32
		iPackedTexture1 = 512
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 70145
		iPackedTexture2 = 16
	BREAK 
	CASE 95
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646144
		iPackedDrawable2 = 0
		iPackedTexture1 = 65538
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 646667
		iPackedDrawable2 = 96
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 147969
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 4609
		iPackedTexture2 = 0
	BREAK 
	CASE 100
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 65537
		iPackedTexture2 = 16
	BREAK 
	CASE 101
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296967
		iPackedDrawable2 = 48
		iPackedTexture1 = 135425
		iPackedTexture2 = 16
	BREAK 
	CASE 102
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 288004
		iPackedDrawable2 = 0
		iPackedTexture1 = 204802
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 73984
		iPackedTexture2 = 16
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 12288
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152585
		iPackedDrawable2 = 48
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70148
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 0
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1282
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 147971
		iPackedDrawable2 = 0
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 74240
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 16
		iPackedTexture1 = 4609
		iPackedTexture2 = 0
	BREAK 
	CASE 115
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69889
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8960
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 12288
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 119
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 121
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297222
		iPackedDrawable2 = 48
		iPackedTexture1 = 196865
		iPackedTexture2 = 16
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70914
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 124
	//Ped type: CASINO_SECURITY_GUARD_6
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20480
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_21(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 55564
		iPackedDrawable2 = 0
		iPackedTexture1 = 143618
		iPackedTexture2 = 0
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152579
		iPackedDrawable2 = 16
		iPackedTexture1 = 65536
		iPackedTexture2 = 48
	BREAK 
	CASE 9
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8961
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70144
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 539914
		iPackedDrawable2 = 0
		iPackedTexture1 = 74240
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 131329
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8708
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74240
		iPackedDrawable2 = 8192
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360712
		iPackedDrawable2 = 48
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148738
		iPackedDrawable2 = 16
		iPackedTexture1 = 8705
		iPackedTexture2 = 32
	BREAK 
	CASE 19
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301062
		iPackedDrawable2 = 48
		iPackedTexture1 = 78336
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 12288
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 135424
		iPackedTexture2 = 32
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 0
		iPackedTexture1 = 73729
		iPackedTexture2 = 16
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 8704
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 227076
		iPackedDrawable2 = 32
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66818
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70657
		iPackedDrawable2 = 0
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 539914
		iPackedDrawable2 = 0
		iPackedTexture1 = 131330
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 440840
		iPackedDrawable2 = 64
		iPackedTexture1 = 139776
		iPackedTexture2 = 16
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66051
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221959
		iPackedDrawable2 = 96
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4610
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646144
		iPackedDrawable2 = 0
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 646411
		iPackedDrawable2 = 96
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_CAROL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 66048
		iPackedTexture2 = 0
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 135170
		iPackedTexture2 = 16
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694795
		iPackedDrawable2 = 0
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707083
		iPackedDrawable2 = 0
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152579
		iPackedDrawable2 = 16
		iPackedTexture1 = 8449
		iPackedTexture2 = 48
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 147458
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 16386
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 4608
		iPackedTexture2 = 16
	BREAK 
	CASE 52
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 143360
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728586
		iPackedDrawable2 = 0
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 513
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74243
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 4096
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 208897
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 0
		iPackedTexture2 = 16
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66050
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 288004
		iPackedDrawable2 = 0
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_DEALER_FEMALE_4
		iPackedDrawable1 = 78338
		iPackedDrawable2 = 12816
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_DEALER_FEMALE_5
		iPackedDrawable1 = 66307
		iPackedDrawable2 = 272
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_DEALER_FEMALE_2
		iPackedDrawable1 = 4353
		iPackedDrawable2 = 4608
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 584971
		iPackedDrawable2 = 96
		iPackedTexture1 = 73986
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 197122
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4354
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_CURTIS
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 70144
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 151553
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_TAYLOR
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_LAUREN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 201218
		iPackedTexture2 = 0
	BREAK 
	CASE 81
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222725
		iPackedDrawable2 = 32
		iPackedTexture1 = 8448
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646144
		iPackedDrawable2 = 0
		iPackedTexture1 = 73730
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70148
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_USHI
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9474
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301062
		iPackedDrawable2 = 48
		iPackedTexture1 = 217089
		iPackedTexture2 = 16
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 90
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297223
		iPackedDrawable2 = 48
		iPackedTexture1 = 143361
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 148227
		iPackedDrawable2 = 0
		iPackedTexture1 = 8704
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 70145
		iPackedTexture2 = 16
	BREAK 
	CASE 93
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 131329
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650763
		iPackedDrawable2 = 96
		iPackedTexture1 = 135425
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707083
		iPackedDrawable2 = 0
		iPackedTexture1 = 78082
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 86017
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 513
		iPackedTexture2 = 16
	BREAK 
	CASE 100
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 66049
		iPackedTexture2 = 16
	BREAK 
	CASE 101
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 131584
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 371208
		iPackedDrawable2 = 64
		iPackedTexture1 = 135425
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 425992
		iPackedDrawable2 = 48
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226055
		iPackedDrawable2 = 96
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 513
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707342
		iPackedDrawable2 = 0
		iPackedTexture1 = 262400
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 585227
		iPackedDrawable2 = 96
		iPackedTexture1 = 131586
		iPackedTexture2 = 0
	BREAK 
	CASE 115
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 20480
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 119
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360456
		iPackedDrawable2 = 48
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 121
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 73984
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 124
	//Ped type: CASINO_SECURITY_GUARD_6
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20480
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_22(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152835
		iPackedDrawable2 = 16
		iPackedTexture1 = 73728
		iPackedTexture2 = 48
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74242
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 5121
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 139778
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 584971
		iPackedDrawable2 = 96
		iPackedTexture1 = 8448
		iPackedTexture2 = 0
	BREAK 
	CASE 9
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3072
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650763
		iPackedDrawable2 = 96
		iPackedTexture1 = 66050
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287748
		iPackedDrawable2 = 0
		iPackedTexture1 = 135425
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707083
		iPackedDrawable2 = 0
		iPackedTexture1 = 147714
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66817
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 0
		iPackedTexture1 = 73985
		iPackedTexture2 = 16
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360712
		iPackedDrawable2 = 48
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4354
		iPackedDrawable2 = 0
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 212994
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70144
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 257
		iPackedTexture2 = 32
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 152323
		iPackedDrawable2 = 0
		iPackedTexture1 = 131328
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301063
		iPackedDrawable2 = 48
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 81922
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8708
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 135682
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287495
		iPackedDrawable2 = 96
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 73985
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66048
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 217603
		iPackedDrawable2 = 0
		iPackedTexture1 = 135681
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 4353
		iPackedTexture2 = 16
	BREAK 
	CASE 40
	//Ped type: CASINO_CAROL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 70145
		iPackedTexture2 = 0
	BREAK 
	CASE 43
	//Ped type: CASINO_MANAGER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 227077
		iPackedDrawable2 = 32
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 200706
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56320
		iPackedDrawable2 = 0
		iPackedTexture1 = 86018
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 440840
		iPackedDrawable2 = 64
		iPackedTexture1 = 8705
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 531978
		iPackedDrawable2 = 0
		iPackedTexture1 = 8706
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 147457
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 16
		iPackedTexture1 = 66048
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 55308
		iPackedDrawable2 = 0
		iPackedTexture1 = 131328
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139778
		iPackedTexture2 = 16
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 515
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 406537
		iPackedDrawable2 = 80
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 8192
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70144
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301318
		iPackedDrawable2 = 48
		iPackedTexture1 = 82433
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70147
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65538
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 131584
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 516
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 414729
		iPackedDrawable2 = 48
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 74241
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296966
		iPackedDrawable2 = 48
		iPackedTexture1 = 197120
		iPackedTexture2 = 16
	BREAK 
	CASE 73
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4354
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 540170
		iPackedDrawable2 = 0
		iPackedTexture1 = 65792
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646144
		iPackedDrawable2 = 0
		iPackedTexture1 = 77825
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360456
		iPackedDrawable2 = 48
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70145
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1281
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139521
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 227076
		iPackedDrawable2 = 32
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 436744
		iPackedDrawable2 = 64
		iPackedTexture1 = 73728
		iPackedTexture2 = 16
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74242
		iPackedDrawable2 = 0
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 147458
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728330
		iPackedDrawable2 = 0
		iPackedTexture1 = 135425
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 201217
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70657
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 204800
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 580875
		iPackedDrawable2 = 96
		iPackedTexture1 = 74241
		iPackedTexture2 = 0
	BREAK 
	CASE 100
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 515
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226054
		iPackedDrawable2 = 96
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70146
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135426
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8961
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70144
		iPackedDrawable2 = 4096
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152578
		iPackedDrawable2 = 16
		iPackedTexture1 = 135168
		iPackedTexture2 = 32
	BREAK 
	CASE 108
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 66050
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 516
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 0
		iPackedTexture2 = 16
	BREAK 
	CASE 112
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70145
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73984
		iPackedDrawable2 = 16384
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 115
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74242
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69632
		iPackedTexture2 = 16
	BREAK 
	CASE 119
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 288004
		iPackedDrawable2 = 0
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 121
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 513
		iPackedTexture2 = 48
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SECURITY_GUARD_2
		iPackedDrawable1 = 66308
		iPackedDrawable2 = 8448
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_23(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152579
		iPackedDrawable2 = 16
		iPackedTexture1 = 66048
		iPackedTexture2 = 48
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 74240
		iPackedDrawable2 = 4096
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4864
		iPackedDrawable2 = 8192
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1282
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73984
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148482
		iPackedDrawable2 = 16
		iPackedTexture1 = 4352
		iPackedTexture2 = 32
	BREAK 
	CASE 9
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 213763
		iPackedDrawable2 = 0
		iPackedTexture1 = 4353
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4096
		iPackedDrawable2 = 0
		iPackedTexture1 = 135681
		iPackedTexture2 = 16
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 135425
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 209154
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 8705
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 139521
		iPackedTexture2 = 32
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70147
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70657
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 65538
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360712
		iPackedDrawable2 = 48
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 226820
		iPackedDrawable2 = 32
		iPackedTexture1 = 131584
		iPackedTexture2 = 0
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694795
		iPackedDrawable2 = 0
		iPackedTexture1 = 20736
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 436744
		iPackedDrawable2 = 64
		iPackedTexture1 = 65792
		iPackedTexture2 = 0
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 143360
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226054
		iPackedDrawable2 = 96
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73986
		iPackedDrawable2 = 0
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 581131
		iPackedDrawable2 = 96
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 73986
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 513
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66052
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 425992
		iPackedDrawable2 = 48
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4612
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 131329
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 646411
		iPackedDrawable2 = 96
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_CAROL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 43
	//Ped type: CASINO_MANAGER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297223
		iPackedDrawable2 = 48
		iPackedTexture1 = 12801
		iPackedTexture2 = 0
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 74241
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707339
		iPackedDrawable2 = 0
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 695054
		iPackedDrawable2 = 0
		iPackedTexture1 = 266496
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296966
		iPackedDrawable2 = 48
		iPackedTexture1 = 65537
		iPackedTexture2 = 16
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 8449
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 143361
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504073
		iPackedDrawable2 = 0
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 86018
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 70144
		iPackedTexture2 = 16
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 4098
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 515
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74244
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8449
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4098
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296966
		iPackedDrawable2 = 48
		iPackedTexture1 = 205056
		iPackedTexture2 = 16
	BREAK 
	CASE 63
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148489
		iPackedDrawable2 = 80
		iPackedTexture1 = 131073
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707083
		iPackedDrawable2 = 0
		iPackedTexture1 = 340482
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221959
		iPackedDrawable2 = 96
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 131074
		iPackedTexture2 = 16
	BREAK 
	CASE 73
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74243
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 75010
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 341001
		iPackedDrawable2 = 80
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 152067
		iPackedDrawable2 = 0
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4354
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287495
		iPackedDrawable2 = 96
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8449
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650763
		iPackedDrawable2 = 96
		iPackedTexture1 = 4608
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 69633
		iPackedTexture2 = 16
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70657
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70145
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 65538
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 81920
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 539914
		iPackedDrawable2 = 0
		iPackedTexture1 = 70146
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 66049
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 728842
		iPackedDrawable2 = 0
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 100
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65538
		iPackedDrawable2 = 16384
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226055
		iPackedDrawable2 = 96
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 139265
		iPackedTexture2 = 48
	BREAK 
	CASE 108
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 81921
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66048
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 4352
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70146
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 115
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66048
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 371208
		iPackedDrawable2 = 64
		iPackedTexture1 = 8193
		iPackedTexture2 = 16
	BREAK 
	CASE 119
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 151554
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 121
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222981
		iPackedDrawable2 = 32
		iPackedTexture1 = 70144
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SECURITY_GUARD_2
		iPackedDrawable1 = 66308
		iPackedDrawable2 = 8448
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_24(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SECURITY_GUARD_5
		iPackedDrawable1 = 66305
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148483
		iPackedDrawable2 = 16
		iPackedTexture1 = 65792
		iPackedTexture2 = 48
	BREAK 
	CASE 4
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 73984
		iPackedDrawable2 = 16384
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 5
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 12288
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 8450
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 769
		iPackedDrawable2 = 8192
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 9
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 540170
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 581131
		iPackedDrawable2 = 96
		iPackedTexture1 = 73984
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56320
		iPackedDrawable2 = 0
		iPackedTexture1 = 143361
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65538
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 8706
		iPackedTexture2 = 16
	BREAK 
	CASE 16
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70148
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 75009
		iPackedDrawable2 = 0
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 147971
		iPackedDrawable2 = 0
		iPackedTexture1 = 65536
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8705
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152578
		iPackedDrawable2 = 16
		iPackedTexture1 = 513
		iPackedTexture2 = 32
	BREAK 
	CASE 25
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 4353
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 513
		iPackedTexture2 = 16
	BREAK 
	CASE 27
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 512
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 516
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 66049
		iPackedTexture2 = 0
	BREAK 
	CASE 32
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8450
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 4609
		iPackedTexture2 = 0
	BREAK 
	CASE 35
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 144389
		iPackedDrawable2 = 80
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 36
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 514
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 37
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70146
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 38
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 148227
		iPackedDrawable2 = 0
		iPackedTexture1 = 8705
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297223
		iPackedDrawable2 = 48
		iPackedTexture1 = 135424
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_CAROL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 226820
		iPackedDrawable2 = 32
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 4096
		iPackedTexture2 = 16
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646144
		iPackedDrawable2 = 0
		iPackedTexture1 = 20482
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 200960
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 139777
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707339
		iPackedDrawable2 = 0
		iPackedTexture1 = 143361
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 16
		iPackedTexture1 = 73985
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 135425
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646145
		iPackedDrawable2 = 0
		iPackedTexture1 = 73730
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707339
		iPackedDrawable2 = 0
		iPackedTexture1 = 262402
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 69889
		iPackedTexture2 = 16
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 426248
		iPackedDrawable2 = 48
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 8194
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70147
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 144899
		iPackedDrawable2 = 20480
		iPackedTexture1 = 131586
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301063
		iPackedDrawable2 = 48
		iPackedTexture1 = 8705
		iPackedTexture2 = 16
	BREAK 
	CASE 63
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8708
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4610
		iPackedDrawable2 = 0
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 66
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694795
		iPackedDrawable2 = 0
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_DEALER_FEMALE_4
		iPackedDrawable1 = 78338
		iPackedDrawable2 = 12816
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_DEALER_FEMALE_5
		iPackedDrawable1 = 66307
		iPackedDrawable2 = 272
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_DEALER_FEMALE_2
		iPackedDrawable1 = 4353
		iPackedDrawable2 = 4608
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214787
		iPackedDrawable2 = 20480
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 4608
		iPackedDrawable2 = 16384
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_CURTIS
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226055
		iPackedDrawable2 = 96
		iPackedTexture1 = 131073
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 131073
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 86018
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_TAYLOR
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_LAUREN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649484
		iPackedDrawable2 = 0
		iPackedTexture1 = 143873
		iPackedTexture2 = 0
	BREAK 
	CASE 81
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 213506
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 16384
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70147
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 73984
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 349193
		iPackedDrawable2 = 80
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 65794
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152578
		iPackedDrawable2 = 16
		iPackedTexture1 = 139264
		iPackedTexture2 = 32
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 436744
		iPackedDrawable2 = 64
		iPackedTexture1 = 135424
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650507
		iPackedDrawable2 = 96
		iPackedTexture1 = 65793
		iPackedTexture2 = 0
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 9473
		iPackedDrawable2 = 0
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 20480
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287748
		iPackedDrawable2 = 0
		iPackedTexture1 = 66048
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 514
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 258
		iPackedDrawable2 = 12288
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 55565
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 8705
		iPackedTexture2 = 48
	BREAK 
	CASE 100
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291590
		iPackedDrawable2 = 96
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8448
		iPackedDrawable2 = 4096
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 106
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221961
		iPackedDrawable2 = 48
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301062
		iPackedDrawable2 = 48
		iPackedTexture1 = 217088
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504073
		iPackedDrawable2 = 0
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4611
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287494
		iPackedDrawable2 = 96
		iPackedTexture1 = 65538
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139266
		iPackedTexture2 = 16
	BREAK 
	CASE 112
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221959
		iPackedDrawable2 = 96
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360456
		iPackedDrawable2 = 48
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 69634
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 115
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 512
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65536
		iPackedDrawable2 = 16384
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70657
		iPackedDrawable2 = 4096
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301318
		iPackedDrawable2 = 48
		iPackedTexture1 = 77825
		iPackedTexture2 = 16
	BREAK 
	CASE 119
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 204802
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 121
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 70145
		iPackedTexture2 = 16
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SECURITY_GUARD_2
		iPackedDrawable1 = 66308
		iPackedDrawable2 = 8448
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_MISSION_1(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 86018
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 73728
		iPackedTexture2 = 48
	BREAK 
	CASE 4
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 131328
		iPackedTexture2 = 16
	BREAK 
	CASE 5
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 147456
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 66049
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 226820
		iPackedDrawable2 = 32
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 8705
		iPackedTexture2 = 32
	BREAK 
	CASE 9
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 580875
		iPackedDrawable2 = 96
		iPackedTexture1 = 66050
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 531978
		iPackedDrawable2 = 0
		iPackedTexture1 = 65793
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 513
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646144
		iPackedDrawable2 = 0
		iPackedTexture1 = 196609
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 143361
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139264
		iPackedTexture2 = 16
	BREAK 
	CASE 16
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 20482
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650507
		iPackedDrawable2 = 96
		iPackedTexture1 = 131329
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 70144
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 581131
		iPackedDrawable2 = 96
		iPackedTexture1 = 8704
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 147971
		iPackedDrawable2 = 0
		iPackedTexture1 = 70144
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 152323
		iPackedDrawable2 = 0
		iPackedTexture1 = 131073
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 645389
		iPackedDrawable2 = 0
		iPackedTexture1 = 16898
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148482
		iPackedDrawable2 = 16
		iPackedTexture1 = 135168
		iPackedTexture2 = 48
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 8705
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152579
		iPackedDrawable2 = 16
		iPackedTexture1 = 65793
		iPackedTexture2 = 32
	BREAK 
	CASE 45
	//Ped type: CASINO_CURTIS
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 55564
		iPackedDrawable2 = 0
		iPackedTexture1 = 143361
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 292101
		iPackedDrawable2 = 0
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297222
		iPackedDrawable2 = 48
		iPackedTexture1 = 197121
		iPackedTexture2 = 16
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 86016
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 135426
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707339
		iPackedDrawable2 = 0
		iPackedTexture1 = 327682
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 4610
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 8448
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222725
		iPackedDrawable2 = 32
		iPackedTexture1 = 65792
		iPackedTexture2 = 0
	BREAK 
	CASE 56
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69890
		iPackedTexture2 = 16
	BREAK 
	CASE 57
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 371208
		iPackedDrawable2 = 64
		iPackedTexture1 = 135425
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707339
		iPackedDrawable2 = 0
		iPackedTexture1 = 147713
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646144
		iPackedDrawable2 = 0
		iPackedTexture1 = 151554
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 135425
		iPackedTexture2 = 16
	BREAK 
	CASE 61
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 131586
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 73985
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301063
		iPackedDrawable2 = 48
		iPackedTexture1 = 131585
		iPackedTexture2 = 16
	BREAK 
	CASE 65
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 69633
		iPackedTexture2 = 16
	BREAK 
	CASE 67
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301062
		iPackedDrawable2 = 48
		iPackedTexture1 = 78080
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 131328
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 436744
		iPackedDrawable2 = 64
		iPackedTexture1 = 8704
		iPackedTexture2 = 16
	BREAK 
	CASE 71
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 205056
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 0
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 139266
		iPackedTexture2 = 16
	BREAK 
	CASE 74
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 12289
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 584971
		iPackedDrawable2 = 96
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 55309
		iPackedDrawable2 = 0
		iPackedTexture1 = 8449
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 736522
		iPackedDrawable2 = 0
		iPackedTexture1 = 139778
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 81
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70147
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 646667
		iPackedDrawable2 = 96
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 78338
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1025
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 73986
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 436744
		iPackedDrawable2 = 64
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148483
		iPackedDrawable2 = 16
		iPackedTexture1 = 131585
		iPackedTexture2 = 32
	BREAK 
	CASE 90
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 0
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 361225
		iPackedDrawable2 = 48
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707342
		iPackedDrawable2 = 0
		iPackedTexture1 = 266753
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 440840
		iPackedDrawable2 = 64
		iPackedTexture1 = 135424
		iPackedTexture2 = 16
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 8193
		iPackedTexture2 = 16
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_MISSION_2(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 86016
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296966
		iPackedDrawable2 = 48
		iPackedTexture1 = 204800
		iPackedTexture2 = 0
	BREAK 
	CASE 4
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139264
		iPackedTexture2 = 16
	BREAK 
	CASE 5
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 646411
		iPackedDrawable2 = 96
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 74497
		iPackedDrawable2 = 16
		iPackedTexture1 = 74242
		iPackedTexture2 = 16
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 139521
		iPackedTexture2 = 16
	BREAK 
	CASE 9
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 0
		iPackedTexture1 = 131585
		iPackedTexture2 = 16
	BREAK 
	CASE 11
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 152067
		iPackedDrawable2 = 0
		iPackedTexture1 = 4352
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 375304
		iPackedDrawable2 = 64
		iPackedTexture1 = 135680
		iPackedTexture2 = 16
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 16
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707342
		iPackedDrawable2 = 0
		iPackedTexture1 = 336129
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 135680
		iPackedTexture2 = 16
	BREAK 
	CASE 18
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 531978
		iPackedDrawable2 = 0
		iPackedTexture1 = 4609
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148482
		iPackedDrawable2 = 16
		iPackedTexture1 = 73985
		iPackedTexture2 = 48
	BREAK 
	CASE 20
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 81920
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 65794
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_DEALER_MALE_1
		iPackedDrawable1 = 4883
		iPackedDrawable2 = 12816
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_DEALER_FEMALE_1
		iPackedDrawable1 = 257
		iPackedDrawable2 = 0
		iPackedTexture1 = 12288
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_DEALER_MALE_2
		iPackedDrawable1 = 1042
		iPackedDrawable2 = 4624
		iPackedTexture1 = 16386
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 410633
		iPackedDrawable2 = 48
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301062
		iPackedDrawable2 = 48
		iPackedTexture1 = 82433
		iPackedTexture2 = 16
	BREAK 
	CASE 44
	//Ped type: CASINO_DEALER_FEMALE_2
		iPackedDrawable1 = 4353
		iPackedDrawable2 = 4608
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 45
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70147
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287494
		iPackedDrawable2 = 96
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 513
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 226820
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 217088
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 70144
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_DEALER_MALE_3
		iPackedDrawable1 = 530
		iPackedDrawable2 = 4624
		iPackedTexture1 = 12289
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 4354
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646144
		iPackedDrawable2 = 0
		iPackedTexture1 = 8193
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 584971
		iPackedDrawable2 = 96
		iPackedTexture1 = 73730
		iPackedTexture2 = 0
	BREAK 
	CASE 56
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301063
		iPackedDrawable2 = 48
		iPackedTexture1 = 131328
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_DEALER_MALE_4
		iPackedDrawable1 = 4882
		iPackedDrawable2 = 12816
		iPackedTexture1 = 12288
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_DEALER_MALE_5
		iPackedDrawable1 = 788
		iPackedDrawable2 = 4624
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_DEALER_FEMALE_3
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 8192
		iPackedTexture1 = 12288
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 4609
		iPackedTexture2 = 32
	BREAK 
	CASE 61
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_DEALER_FEMALE_4
		iPackedDrawable1 = 78338
		iPackedDrawable2 = 12816
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 580875
		iPackedDrawable2 = 96
		iPackedTexture1 = 4609
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 209410
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 213507
		iPackedDrawable2 = 0
		iPackedTexture1 = 74241
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 70145
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 77825
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66049
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 645132
		iPackedDrawable2 = 0
		iPackedTexture1 = 151809
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 69888
		iPackedTexture2 = 16
	BREAK 
	CASE 75
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8707
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649485
		iPackedDrawable2 = 0
		iPackedTexture1 = 208898
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152835
		iPackedDrawable2 = 16
		iPackedTexture1 = 139264
		iPackedTexture2 = 48
	BREAK 
	CASE 79
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504073
		iPackedDrawable2 = 0
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222981
		iPackedDrawable2 = 32
		iPackedTexture1 = 131328
		iPackedTexture2 = 0
	BREAK 
	CASE 81
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 695051
		iPackedDrawable2 = 0
		iPackedTexture1 = 262658
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 8193
		iPackedTexture2 = 16
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66048
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287748
		iPackedDrawable2 = 0
		iPackedTexture1 = 197122
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 539914
		iPackedDrawable2 = 0
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 69888
		iPackedTexture2 = 32
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 4609
		iPackedTexture2 = 0
	BREAK 
	CASE 90
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707339
		iPackedDrawable2 = 0
		iPackedTexture1 = 81921
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 436744
		iPackedDrawable2 = 64
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 288005
		iPackedDrawable2 = 0
		iPackedTexture1 = 143360
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69890
		iPackedTexture2 = 16
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4096
		iPackedDrawable2 = 0
		iPackedTexture1 = 73984
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_DEALER_FEMALE_5
		iPackedDrawable1 = 66307
		iPackedDrawable2 = 272
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_DEALER_MALE_6
		iPackedDrawable1 = 20
		iPackedDrawable2 = 4624
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 97
	//Ped type: CASINO_DEALER_FEMALE_6
		iPackedDrawable1 = 70403
		iPackedDrawable2 = 4624
		iPackedTexture1 = 4353
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEALER_MALE_7
		iPackedDrawable1 = 5140
		iPackedDrawable2 = 12816
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 99
	//Ped type: CASINO_DEALER_FEMALE_7
		iPackedDrawable1 = 74756
		iPackedDrawable2 = 8464
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_MISSION_3(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 86018
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 73728
		iPackedTexture2 = 48
	BREAK 
	CASE 4
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 131328
		iPackedTexture2 = 16
	BREAK 
	CASE 5
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 147456
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 66049
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 226820
		iPackedDrawable2 = 32
		iPackedTexture1 = 131072
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 8705
		iPackedTexture2 = 32
	BREAK 
	CASE 9
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 580875
		iPackedDrawable2 = 96
		iPackedTexture1 = 66050
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 531978
		iPackedDrawable2 = 0
		iPackedTexture1 = 65793
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 513
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646144
		iPackedDrawable2 = 0
		iPackedTexture1 = 196609
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 143361
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139264
		iPackedTexture2 = 16
	BREAK 
	CASE 16
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427783
		iPackedDrawable2 = 0
		iPackedTexture1 = 20482
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 650507
		iPackedDrawable2 = 96
		iPackedTexture1 = 131329
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 70144
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 581131
		iPackedDrawable2 = 96
		iPackedTexture1 = 8704
		iPackedTexture2 = 0
	BREAK 
	CASE 20
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 147971
		iPackedDrawable2 = 0
		iPackedTexture1 = 70144
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 152323
		iPackedDrawable2 = 0
		iPackedTexture1 = 131073
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 645389
		iPackedDrawable2 = 0
		iPackedTexture1 = 16898
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148482
		iPackedDrawable2 = 16
		iPackedTexture1 = 135168
		iPackedTexture2 = 48
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 8705
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152579
		iPackedDrawable2 = 16
		iPackedTexture1 = 65793
		iPackedTexture2 = 32
	BREAK 
	CASE 45
	//Ped type: CASINO_CURTIS
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 55564
		iPackedDrawable2 = 0
		iPackedTexture1 = 143361
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 292101
		iPackedDrawable2 = 0
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297222
		iPackedDrawable2 = 48
		iPackedTexture1 = 197121
		iPackedTexture2 = 16
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649229
		iPackedDrawable2 = 0
		iPackedTexture1 = 86016
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 135170
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 135426
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707339
		iPackedDrawable2 = 0
		iPackedTexture1 = 327682
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 4610
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301319
		iPackedDrawable2 = 48
		iPackedTexture1 = 8448
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222725
		iPackedDrawable2 = 32
		iPackedTexture1 = 65792
		iPackedTexture2 = 0
	BREAK 
	CASE 56
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69890
		iPackedTexture2 = 16
	BREAK 
	CASE 57
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 371208
		iPackedDrawable2 = 64
		iPackedTexture1 = 135425
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707339
		iPackedDrawable2 = 0
		iPackedTexture1 = 147713
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646144
		iPackedDrawable2 = 0
		iPackedTexture1 = 151554
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 135425
		iPackedTexture2 = 16
	BREAK 
	CASE 61
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 131586
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 56321
		iPackedDrawable2 = 0
		iPackedTexture1 = 69634
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 73985
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301063
		iPackedDrawable2 = 48
		iPackedTexture1 = 131585
		iPackedTexture2 = 16
	BREAK 
	CASE 65
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 69633
		iPackedTexture2 = 16
	BREAK 
	CASE 67
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301062
		iPackedDrawable2 = 48
		iPackedTexture1 = 78080
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 131328
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 504072
		iPackedDrawable2 = 0
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 436744
		iPackedDrawable2 = 64
		iPackedTexture1 = 8704
		iPackedTexture2 = 16
	BREAK 
	CASE 71
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 205056
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 0
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 73
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 139266
		iPackedTexture2 = 16
	BREAK 
	CASE 74
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 12289
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 584971
		iPackedDrawable2 = 96
		iPackedTexture1 = 135168
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 55309
		iPackedDrawable2 = 0
		iPackedTexture1 = 8449
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 736522
		iPackedDrawable2 = 0
		iPackedTexture1 = 139778
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 152581
		iPackedDrawable2 = 80
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 81
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70147
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 646667
		iPackedDrawable2 = 96
		iPackedTexture1 = 69889
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 78338
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 1025
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 361990
		iPackedDrawable2 = 0
		iPackedTexture1 = 73986
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 88
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 436744
		iPackedDrawable2 = 64
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 89
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148483
		iPackedDrawable2 = 16
		iPackedTexture1 = 131585
		iPackedTexture2 = 32
	BREAK 
	CASE 90
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4352
		iPackedDrawable2 = 0
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 361225
		iPackedDrawable2 = 48
		iPackedTexture1 = 135169
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707342
		iPackedDrawable2 = 0
		iPackedTexture1 = 266753
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 440840
		iPackedDrawable2 = 64
		iPackedTexture1 = 135424
		iPackedTexture2 = 16
	BREAK 
	CASE 94
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510986
		iPackedDrawable2 = 80
		iPackedTexture1 = 8193
		iPackedTexture2 = 16
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_CUTSCENE_1(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8194
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_DEALER_FEMALE_5
		iPackedDrawable1 = 66307
		iPackedDrawable2 = 272
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 4
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 73728
		iPackedTexture2 = 48
	BREAK 
	CASE 5
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 66048
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 70145
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646144
		iPackedDrawable2 = 0
		iPackedTexture1 = 16385
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_DEALER_FEMALE_3
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 8192
		iPackedTexture1 = 12288
		iPackedTexture2 = 0
	BREAK 
	CASE 9
	//Ped type: CASINO_DEALER_MALE_2
		iPackedDrawable1 = 1042
		iPackedDrawable2 = 4624
		iPackedTexture1 = 16386
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 55308
		iPackedDrawable2 = 0
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 226820
		iPackedDrawable2 = 32
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 531978
		iPackedDrawable2 = 0
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 131328
		iPackedTexture2 = 16
	BREAK 
	CASE 16
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 131586
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 8705
		iPackedTexture2 = 32
	BREAK 
	CASE 20
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139777
		iPackedTexture2 = 16
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 580875
		iPackedDrawable2 = 96
		iPackedTexture1 = 66050
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74244
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 25
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4610
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 27
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 65792
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 78080
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148482
		iPackedDrawable2 = 16
		iPackedTexture1 = 135424
		iPackedTexture2 = 48
	BREAK 
	CASE 32
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_DEALER_MALE_4
		iPackedDrawable1 = 4882
		iPackedDrawable2 = 12816
		iPackedTexture1 = 12288
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_DEALER_FEMALE_1
		iPackedDrawable1 = 257
		iPackedDrawable2 = 0
		iPackedTexture1 = 12288
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70657
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_CAROL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301318
		iPackedDrawable2 = 48
		iPackedTexture1 = 213505
		iPackedTexture2 = 16
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707342
		iPackedDrawable2 = 0
		iPackedTexture1 = 274946
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 74241
		iPackedTexture2 = 0
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 213248
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66050
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4096
		iPackedDrawable2 = 0
		iPackedTexture1 = 131329
		iPackedTexture2 = 16
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 16386
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296967
		iPackedDrawable2 = 48
		iPackedTexture1 = 4609
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 288004
		iPackedDrawable2 = 0
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 645389
		iPackedDrawable2 = 0
		iPackedTexture1 = 152066
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4612
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8449
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_DEALER_FEMALE_6
		iPackedDrawable1 = 70403
		iPackedDrawable2 = 4624
		iPackedTexture1 = 4353
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222725
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 77825
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 200961
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 531722
		iPackedDrawable2 = 0
		iPackedTexture1 = 73985
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 584971
		iPackedDrawable2 = 96
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296967
		iPackedDrawable2 = 48
		iPackedTexture1 = 143616
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_DEALER_MALE_6
		iPackedDrawable1 = 20
		iPackedDrawable2 = 4624
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65794
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287494
		iPackedDrawable2 = 96
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 646667
		iPackedDrawable2 = 96
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646145
		iPackedDrawable2 = 0
		iPackedTexture1 = 208898
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152579
		iPackedDrawable2 = 16
		iPackedTexture1 = 65537
		iPackedTexture2 = 32
	BREAK 
	CASE 73
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 4609
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 152067
		iPackedDrawable2 = 0
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 371208
		iPackedDrawable2 = 64
		iPackedTexture1 = 135681
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 147971
		iPackedDrawable2 = 0
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66048
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 12289
		iPackedTexture2 = 0
	BREAK 
	CASE 81
	//Ped type: CASINO_DEALER_FEMALE_2
		iPackedDrawable1 = 4353
		iPackedDrawable2 = 4608
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8707
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360712
		iPackedDrawable2 = 48
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70146
		iPackedDrawable2 = 0
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297222
		iPackedDrawable2 = 48
		iPackedTexture1 = 73728
		iPackedTexture2 = 16
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70400
		iPackedDrawable2 = 16384
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66817
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 8192
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70145
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 86018
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 100
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 514
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360456
		iPackedDrawable2 = 48
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226055
		iPackedDrawable2 = 96
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 4096
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69890
		iPackedTexture2 = 16
	BREAK 
	CASE 106
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707083
		iPackedDrawable2 = 0
		iPackedTexture1 = 331778
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 139776
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 139778
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4611
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66051
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 4096
		iPackedTexture2 = 16
	BREAK 
	CASE 115
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694795
		iPackedDrawable2 = 0
		iPackedTexture1 = 135681
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646144
		iPackedDrawable2 = 0
		iPackedTexture1 = 73730
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8449
		iPackedDrawable2 = 0
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 119
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA_CUTSCENE_2(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2)
SWITCH iPedID
	CASE 2
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8194
		iPackedDrawable2 = 4096
		iPackedTexture1 = 69890
		iPackedTexture2 = 0
	BREAK 
	CASE 3
	//Ped type: CASINO_DEALER_FEMALE_5
		iPackedDrawable1 = 66307
		iPackedDrawable2 = 272
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 4
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148739
		iPackedDrawable2 = 16
		iPackedTexture1 = 73728
		iPackedTexture2 = 48
	BREAK 
	CASE 5
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 66048
		iPackedTexture2 = 0
	BREAK 
	CASE 6
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78593
		iPackedDrawable2 = 16
		iPackedTexture1 = 70145
		iPackedTexture2 = 0
	BREAK 
	CASE 7
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646144
		iPackedDrawable2 = 0
		iPackedTexture1 = 16385
		iPackedTexture2 = 0
	BREAK 
	CASE 8
	//Ped type: CASINO_DEALER_FEMALE_3
		iPackedDrawable1 = 8706
		iPackedDrawable2 = 8192
		iPackedTexture1 = 12288
		iPackedTexture2 = 0
	BREAK 
	CASE 9
	//Ped type: CASINO_DEALER_MALE_2
		iPackedDrawable1 = 1042
		iPackedDrawable2 = 4624
		iPackedTexture1 = 16386
		iPackedTexture2 = 0
	BREAK 
	CASE 11
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 139264
		iPackedTexture2 = 0
	BREAK 
	CASE 12
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 55308
		iPackedDrawable2 = 0
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 13
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 226820
		iPackedDrawable2 = 32
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 14
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 531978
		iPackedDrawable2 = 0
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 15
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 131328
		iPackedTexture2 = 16
	BREAK 
	CASE 16
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 17
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 73730
		iPackedDrawable2 = 0
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 18
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 287749
		iPackedDrawable2 = 0
		iPackedTexture1 = 131586
		iPackedTexture2 = 0
	BREAK 
	CASE 19
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152834
		iPackedDrawable2 = 16
		iPackedTexture1 = 8705
		iPackedTexture2 = 32
	BREAK 
	CASE 20
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 139777
		iPackedTexture2 = 16
	BREAK 
	CASE 21
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 580875
		iPackedDrawable2 = 96
		iPackedTexture1 = 66050
		iPackedTexture2 = 0
	BREAK 
	CASE 22
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 23
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74244
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 24
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 221958
		iPackedDrawable2 = 96
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 25
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 26
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4610
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 27
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 214531
		iPackedDrawable2 = 20480
		iPackedTexture1 = 65537
		iPackedTexture2 = 0
	BREAK 
	CASE 28
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354564
		iPackedDrawable2 = 24576
		iPackedTexture1 = 65792
		iPackedTexture2 = 0
	BREAK 
	CASE 29
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 649228
		iPackedDrawable2 = 0
		iPackedTexture1 = 78080
		iPackedTexture2 = 0
	BREAK 
	CASE 30
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 3073
		iPackedDrawable2 = 0
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 31
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 148482
		iPackedDrawable2 = 16
		iPackedTexture1 = 135424
		iPackedTexture2 = 48
	BREAK 
	CASE 32
	//Ped type: CASINO_SECURITY_GUARD_1
		iPackedDrawable1 = 65537
		iPackedDrawable2 = 8448
		iPackedTexture1 = 8192
		iPackedTexture2 = 0
	BREAK 
	CASE 33
	//Ped type: CASINO_DEALER_MALE_4
		iPackedDrawable1 = 4882
		iPackedDrawable2 = 12816
		iPackedTexture1 = 12288
		iPackedTexture2 = 0
	BREAK 
	CASE 34
	//Ped type: CASINO_DEALER_FEMALE_1
		iPackedDrawable1 = 257
		iPackedDrawable2 = 0
		iPackedTexture1 = 12288
		iPackedTexture2 = 0
	BREAK 
	CASE 39
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70657
		iPackedDrawable2 = 16384
		iPackedTexture1 = 139266
		iPackedTexture2 = 0
	BREAK 
	CASE 40
	//Ped type: CASINO_CAROL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 41
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 427526
		iPackedDrawable2 = 0
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 42
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 301318
		iPackedDrawable2 = 48
		iPackedTexture1 = 213505
		iPackedTexture2 = 16
	BREAK 
	CASE 43
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707342
		iPackedDrawable2 = 0
		iPackedTexture1 = 274946
		iPackedTexture2 = 0
	BREAK 
	CASE 44
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510730
		iPackedDrawable2 = 80
		iPackedTexture1 = 74241
		iPackedTexture2 = 0
	BREAK 
	CASE 45
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694798
		iPackedDrawable2 = 0
		iPackedTexture1 = 213248
		iPackedTexture2 = 0
	BREAK 
	CASE 46
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66050
		iPackedDrawable2 = 0
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 47
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 32
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 48
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 4096
		iPackedDrawable2 = 0
		iPackedTexture1 = 131329
		iPackedTexture2 = 16
	BREAK 
	CASE 49
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 16386
		iPackedTexture2 = 0
	BREAK 
	CASE 50
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499976
		iPackedDrawable2 = 0
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 51
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296967
		iPackedDrawable2 = 48
		iPackedTexture1 = 4609
		iPackedTexture2 = 0
	BREAK 
	CASE 52
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 288004
		iPackedDrawable2 = 0
		iPackedTexture1 = 73728
		iPackedTexture2 = 0
	BREAK 
	CASE 53
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 645389
		iPackedDrawable2 = 0
		iPackedTexture1 = 152066
		iPackedTexture2 = 0
	BREAK 
	CASE 54
	//Ped type: CASINO_BETH
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 55
	//Ped type: CASINO_VINCE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 56
	//Ped type: CASINO_GABRIEL
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 57
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4612
		iPackedDrawable2 = 16
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 58
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8449
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 59
	//Ped type: CASINO_DEALER_FEMALE_6
		iPackedDrawable1 = 70403
		iPackedDrawable2 = 4624
		iPackedTexture1 = 4353
		iPackedTexture2 = 0
	BREAK 
	CASE 60
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 222725
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 61
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 499977
		iPackedDrawable2 = 0
		iPackedTexture1 = 77825
		iPackedTexture2 = 0
	BREAK 
	CASE 62
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291844
		iPackedDrawable2 = 0
		iPackedTexture1 = 200961
		iPackedTexture2 = 0
	BREAK 
	CASE 63
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 531722
		iPackedDrawable2 = 0
		iPackedTexture1 = 73985
		iPackedTexture2 = 0
	BREAK 
	CASE 64
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 584971
		iPackedDrawable2 = 96
		iPackedTexture1 = 256
		iPackedTexture2 = 0
	BREAK 
	CASE 65
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 296967
		iPackedDrawable2 = 48
		iPackedTexture1 = 143616
		iPackedTexture2 = 0
	BREAK 
	CASE 67
	//Ped type: CASINO_DEALER_MALE_6
		iPackedDrawable1 = 20
		iPackedDrawable2 = 4624
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 68
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 65794
		iPackedDrawable2 = 8192
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 69
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 287494
		iPackedDrawable2 = 96
		iPackedTexture1 = 4097
		iPackedTexture2 = 0
	BREAK 
	CASE 70
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 646667
		iPackedDrawable2 = 96
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 71
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646145
		iPackedDrawable2 = 0
		iPackedTexture1 = 208898
		iPackedTexture2 = 0
	BREAK 
	CASE 72
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 152579
		iPackedDrawable2 = 16
		iPackedTexture1 = 65537
		iPackedTexture2 = 32
	BREAK 
	CASE 73
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510985
		iPackedDrawable2 = 80
		iPackedTexture1 = 4609
		iPackedTexture2 = 0
	BREAK 
	CASE 74
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 152067
		iPackedDrawable2 = 0
		iPackedTexture1 = 135680
		iPackedTexture2 = 0
	BREAK 
	CASE 75
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 371208
		iPackedDrawable2 = 64
		iPackedTexture1 = 135681
		iPackedTexture2 = 0
	BREAK 
	CASE 76
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 147971
		iPackedDrawable2 = 0
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 77
	//Ped type: CASINO_CALEB
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 78
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66048
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 79
	//Ped type: CASINO_EILEEN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 80
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 12289
		iPackedTexture2 = 0
	BREAK 
	CASE 81
	//Ped type: CASINO_DEALER_FEMALE_2
		iPackedDrawable1 = 4353
		iPackedDrawable2 = 4608
		iPackedTexture1 = 12545
		iPackedTexture2 = 0
	BREAK 
	CASE 82
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284676
		iPackedDrawable2 = 24576
		iPackedTexture1 = 257
		iPackedTexture2 = 0
	BREAK 
	CASE 83
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 8707
		iPackedDrawable2 = 32
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 84
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360712
		iPackedDrawable2 = 48
		iPackedTexture1 = 69632
		iPackedTexture2 = 0
	BREAK 
	CASE 85
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 291591
		iPackedDrawable2 = 96
		iPackedTexture1 = 73729
		iPackedTexture2 = 0
	BREAK 
	CASE 86
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70146
		iPackedDrawable2 = 0
		iPackedTexture1 = 69633
		iPackedTexture2 = 0
	BREAK 
	CASE 87
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 297222
		iPackedDrawable2 = 48
		iPackedTexture1 = 73728
		iPackedTexture2 = 16
	BREAK 
	CASE 90
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 70400
		iPackedDrawable2 = 16384
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 91
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 148485
		iPackedDrawable2 = 80
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 92
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 66817
		iPackedDrawable2 = 12288
		iPackedTexture1 = 139520
		iPackedTexture2 = 0
	BREAK 
	CASE 93
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8704
		iPackedDrawable2 = 8192
		iPackedTexture1 = 258
		iPackedTexture2 = 0
	BREAK 
	CASE 94
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 70145
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 95
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 362247
		iPackedDrawable2 = 0
		iPackedTexture1 = 86018
		iPackedTexture2 = 0
	BREAK 
	CASE 96
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 69888
		iPackedTexture2 = 0
	BREAK 
	CASE 98
	//Ped type: CASINO_DEAN
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 100
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 514
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 101
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 360456
		iPackedDrawable2 = 48
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 102
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 226055
		iPackedDrawable2 = 96
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 103
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 0
		iPackedDrawable2 = 4096
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 104
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 354308
		iPackedDrawable2 = 24576
		iPackedTexture1 = 4096
		iPackedTexture2 = 0
	BREAK 
	CASE 105
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 720652
		iPackedDrawable2 = 112
		iPackedTexture1 = 69890
		iPackedTexture2 = 16
	BREAK 
	CASE 106
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 707083
		iPackedDrawable2 = 0
		iPackedTexture1 = 331778
		iPackedTexture2 = 0
	BREAK 
	CASE 107
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 291845
		iPackedDrawable2 = 0
		iPackedTexture1 = 139776
		iPackedTexture2 = 0
	BREAK 
	CASE 108
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 78337
		iPackedDrawable2 = 16
		iPackedTexture1 = 131074
		iPackedTexture2 = 0
	BREAK 
	CASE 109
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 74241
		iPackedDrawable2 = 32
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 110
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 111
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 145155
		iPackedDrawable2 = 20480
		iPackedTexture1 = 139778
		iPackedTexture2 = 0
	BREAK 
	CASE 112
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4611
		iPackedDrawable2 = 16
		iPackedTexture1 = 2
		iPackedTexture2 = 0
	BREAK 
	CASE 113
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 66051
		iPackedDrawable2 = 0
		iPackedTexture1 = 1
		iPackedTexture2 = 0
	BREAK 
	CASE 114
	//Ped type: CASINO_GENERIC_FEMALE
		iPackedDrawable1 = 510729
		iPackedDrawable2 = 80
		iPackedTexture1 = 4096
		iPackedTexture2 = 16
	BREAK 
	CASE 115
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 694795
		iPackedDrawable2 = 0
		iPackedTexture1 = 135681
		iPackedTexture2 = 0
	BREAK 
	CASE 116
	//Ped type: CASINO_GENERIC_MALE
		iPackedDrawable1 = 646144
		iPackedDrawable2 = 0
		iPackedTexture1 = 73730
		iPackedTexture2 = 0
	BREAK 
	CASE 117
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 8449
		iPackedDrawable2 = 0
		iPackedTexture1 = 139265
		iPackedTexture2 = 0
	BREAK 
	CASE 118
	//Ped type: CASINO_SMART_FEMALE
		iPackedDrawable1 = 284932
		iPackedDrawable2 = 24576
		iPackedTexture1 = 139522
		iPackedTexture2 = 0
	BREAK 
	CASE 119
	//Ped type: CASINO_SMART_MALE
		iPackedDrawable1 = 4609
		iPackedDrawable2 = 16
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 120
	//Ped type: CASINO_CASHIER
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 122
	//Ped type: CASINO_MAITRED
		iPackedDrawable1 = 0
		iPackedDrawable2 = 0
		iPackedTexture1 = 0
		iPackedTexture2 = 0
	BREAK 
	CASE 123
	//Ped type: CASINO_SECURITY_GUARD_3
		iPackedDrawable1 = 66309
		iPackedDrawable2 = 8448
		iPackedTexture1 = 20481
		iPackedTexture2 = 0
	BREAK 
ENDSWITCH
ENDPROC

PROC GET_CASINO_PED_COMPONENT_DATA(INT iPedID, INT &iPackedDrawable1, INT &iPackedDrawable2, INT &iPackedTexture1, INT &iPackedTexture2, INT iLayout, CASINO_AREA eCasinoArea, INT iVariation)
	
	SWITCH eCasinoArea
		CASE CASINO_AREA_PERMANENT
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
					CASE 2
					//Ped type: CASINO_SECURITY_GUARD_5
						iPackedDrawable1 = 66305
						iPackedDrawable2 = 8448
						iPackedTexture1 = 20481
						iPackedTexture2 = 0
					BREAK 
					CASE 3
					//Ped type: CASINO_GENERIC_MALE
						iPackedDrawable1 = 287748
						iPackedDrawable2 = 0
						iPackedTexture1 = 135424
						iPackedTexture2 = 0
					BREAK 
					CASE 4
					//Ped type: CASINO_SMART_FEMALE
						iPackedDrawable1 = 8961
						iPackedDrawable2 = 12288
						iPackedTexture1 = 139265
						iPackedTexture2 = 0
					BREAK 
					CASE 5
					//Ped type: CASINO_SMART_FEMALE
						iPackedDrawable1 = 70144
						iPackedDrawable2 = 4096
						iPackedTexture1 = 2
						iPackedTexture2 = 0
					BREAK 
					CASE 6
					//Ped type: CASINO_SMART_FEMALE
						iPackedDrawable1 = 73986
						iPackedDrawable2 = 8192
						iPackedTexture1 = 69889
						iPackedTexture2 = 0
					BREAK 
					CASE 7
					//Ped type: CASINO_SMART_FEMALE
						iPackedDrawable1 = 66817
						iPackedDrawable2 = 16384
						iPackedTexture1 = 139522
						iPackedTexture2 = 0
					BREAK 
					CASE 8
					//Ped type: CASINO_GENERIC_FEMALE
						iPackedDrawable1 = 148739
						iPackedDrawable2 = 16
						iPackedTexture1 = 74240
						iPackedTexture2 = 48
					BREAK 
					CASE 9
					//Ped type: CASINO_SMART_FEMALE
						iPackedDrawable1 = 4098
						iPackedDrawable2 = 0
						iPackedTexture1 = 69888
						iPackedTexture2 = 0
					BREAK 
					CASE 11
					//Ped type: CASINO_SMART_MALE
						iPackedDrawable1 = 70147
						iPackedDrawable2 = 32
						iPackedTexture1 = 2
						iPackedTexture2 = 0
					BREAK 
					CASE 12
					//Ped type: CASINO_GENERIC_MALE
						iPackedDrawable1 = 707083
						iPackedDrawable2 = 0
						iPackedTexture1 = 147968
						iPackedTexture2 = 0
					BREAK 
					CASE 13
					//Ped type: CASINO_GENERIC_MALE
						iPackedDrawable1 = 649485
						iPackedDrawable2 = 0
						iPackedTexture1 = 196865
						iPackedTexture2 = 0
					BREAK 
					CASE 14
					//Ped type: CASINO_SMART_MALE
						iPackedDrawable1 = 221959
						iPackedDrawable2 = 96
						iPackedTexture1 = 2
						iPackedTexture2 = 0
					BREAK 
					CASE 15
					//Ped type: CASINO_SMART_FEMALE
						iPackedDrawable1 = 4353
						iPackedDrawable2 = 4096
						iPackedTexture1 = 139264
						iPackedTexture2 = 0
					BREAK 
					CASE 16
					//Ped type: CASINO_SMART_MALE
						iPackedDrawable1 = 360712
						iPackedDrawable2 = 48
						iPackedTexture1 = 69632
						iPackedTexture2 = 0
					BREAK 
					CASE 17
					//Ped type: CASINO_SECURITY_GUARD_1
						iPackedDrawable1 = 65537
						iPackedDrawable2 = 8448
						iPackedTexture1 = 8192
						iPackedTexture2 = 0
					BREAK 
					CASE 18
					//Ped type: CASINO_GENERIC_FEMALE
						iPackedDrawable1 = 74497
						iPackedDrawable2 = 16
						iPackedTexture1 = 131072
						iPackedTexture2 = 0
					BREAK 
					CASE 19
					//Ped type: CASINO_GENERIC_FEMALE
						iPackedDrawable1 = 720652
						iPackedDrawable2 = 112
						iPackedTexture1 = 139521
						iPackedTexture2 = 0
					BREAK 
					CASE 20
					//Ped type: CASINO_SECURITY_GUARD_3
						iPackedDrawable1 = 66309
						iPackedDrawable2 = 8448
						iPackedTexture1 = 20481
						iPackedTexture2 = 0
					BREAK 
					CASE 21
					//Ped type: CASINO_GENERIC_MALE
						iPackedDrawable1 = 504072
						iPackedDrawable2 = 0
						iPackedTexture1 = 139264
						iPackedTexture2 = 0
					BREAK 
					CASE 22
					//Ped type: CASINO_GENERIC_MALE
						iPackedDrawable1 = 56321
						iPackedDrawable2 = 0
						iPackedTexture1 = 73730
						iPackedTexture2 = 0
					BREAK 
					CASE 23
					//Ped type: CASINO_GENERIC_MALE
						iPackedDrawable1 = 649485
						iPackedDrawable2 = 0
						iPackedTexture1 = 196865
						iPackedTexture2 = 0
					BREAK 
					CASE 24
					//Ped type: CASINO_GENERIC_FEMALE
						iPackedDrawable1 = 510986
						iPackedDrawable2 = 80
						iPackedTexture1 = 4608
						iPackedTexture2 = 16
					BREAK 
					CASE 25
					//Ped type: CASINO_GENERIC_FEMALE
						iPackedDrawable1 = 152835
						iPackedDrawable2 = 16
						iPackedTexture1 = 131073
						iPackedTexture2 = 32
					BREAK 
					CASE 26
					//Ped type: CASINO_GENERIC_FEMALE
						iPackedDrawable1 = 720652
						iPackedDrawable2 = 112
						iPackedTexture1 = 70144
						iPackedTexture2 = 16
					BREAK 
					CASE 27
					//Ped type: CASINO_GENERIC_MALE
						iPackedDrawable1 = 291845
						iPackedDrawable2 = 0
						iPackedTexture1 = 70145
						iPackedTexture2 = 0
					BREAK 
					CASE 28
					//Ped type: CASINO_GENERIC_MALE
						iPackedDrawable1 = 728330
						iPackedDrawable2 = 0
						iPackedTexture1 = 66050
						iPackedTexture2 = 0
					BREAK 
					CASE 29
					//Ped type: CASINO_GENERIC_FEMALE
						iPackedDrawable1 = 222980
						iPackedDrawable2 = 32
						iPackedTexture1 = 8704
						iPackedTexture2 = 0
					BREAK 
					CASE 30
					//Ped type: CASINO_SMART_MALE
						iPackedDrawable1 = 291590
						iPackedDrawable2 = 96
						iPackedTexture1 = 135170
						iPackedTexture2 = 0
					BREAK 
					CASE 31
					//Ped type: CASINO_SMART_FEMALE
						iPackedDrawable1 = 354308
						iPackedDrawable2 = 24576
						iPackedTexture1 = 69634
						iPackedTexture2 = 0
					BREAK 
					CASE 32
					//Ped type: CASINO_SMART_FEMALE
						iPackedDrawable1 = 284932
						iPackedDrawable2 = 24576
						iPackedTexture1 = 0
						iPackedTexture2 = 0
					BREAK 
					CASE 33
					//Ped type: CASINO_GENERIC_MALE
						iPackedDrawable1 = 694798
						iPackedDrawable2 = 0
						iPackedTexture1 = 344320
						iPackedTexture2 = 0
					BREAK 
					CASE 34
					//Ped type: CASINO_GENERIC_FEMALE
						iPackedDrawable1 = 78337
						iPackedDrawable2 = 16
						iPackedTexture1 = 66050
						iPackedTexture2 = 16
					BREAK 
					CASE 35
					//Ped type: CASINO_SMART_MALE
						iPackedDrawable1 = 8706
						iPackedDrawable2 = 16
						iPackedTexture1 = 2
						iPackedTexture2 = 0
					BREAK 
					CASE 36
					//Ped type: CASINO_SMART_MALE
						iPackedDrawable1 = 221958
						iPackedDrawable2 = 96
						iPackedTexture1 = 139265
						iPackedTexture2 = 0
					BREAK 
					CASE 37
					//Ped type: CASINO_SMART_MALE
						iPackedDrawable1 = 144389
						iPackedDrawable2 = 80
						iPackedTexture1 = 73728
						iPackedTexture2 = 0
					BREAK 
					CASE 38
					//Ped type: CASINO_GENERIC_MALE
						iPackedDrawable1 = 499977
						iPackedDrawable2 = 0
						iPackedTexture1 = 86016
						iPackedTexture2 = 0
					BREAK 
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						CASE 2
						//Ped type: CASINO_SECURITY_GUARD_5
							iPackedDrawable1 = 66305
							iPackedDrawable2 = 8448
							iPackedTexture1 = 20481
							iPackedTexture2 = 0
						BREAK 
						CASE 3
						//Ped type: CASINO_GENERIC_FEMALE
							iPackedDrawable1 = 148739
							iPackedDrawable2 = 16
							iPackedTexture1 = 74240
							iPackedTexture2 = 48
						BREAK 
						CASE 4
						//Ped type: CASINO_SMART_FEMALE
							iPackedDrawable1 = 66049
							iPackedDrawable2 = 12288
							iPackedTexture1 = 139265
							iPackedTexture2 = 0
						BREAK 
						CASE 5
						//Ped type: CASINO_SMART_FEMALE
							iPackedDrawable1 = 354308
							iPackedDrawable2 = 24576
							iPackedTexture1 = 65536
							iPackedTexture2 = 0
						BREAK 
						CASE 6
						//Ped type: CASINO_SMART_FEMALE
							iPackedDrawable1 = 9217
							iPackedDrawable2 = 4096
							iPackedTexture1 = 139266
							iPackedTexture2 = 0
						BREAK 
						CASE 7
						//Ped type: CASINO_SMART_FEMALE
							iPackedDrawable1 = 73730
							iPackedDrawable2 = 16384
							iPackedTexture1 = 69889
							iPackedTexture2 = 0
						BREAK 
						CASE 8
						//Ped type: CASINO_GENERIC_FEMALE
							iPackedDrawable1 = 720652
							iPackedDrawable2 = 112
							iPackedTexture1 = 139521
							iPackedTexture2 = 0
						BREAK 
						CASE 9
						//Ped type: CASINO_GENERIC_MALE
							iPackedDrawable1 = 540170
							iPackedDrawable2 = 0
							iPackedTexture1 = 135680
							iPackedTexture2 = 0
						BREAK 
						CASE 11
						//Ped type: CASINO_GENERIC_FEMALE
							iPackedDrawable1 = 74241
							iPackedDrawable2 = 16
							iPackedTexture1 = 131584
							iPackedTexture2 = 0
						BREAK 
						CASE 12
						//Ped type: CASINO_GENERIC_MALE
							iPackedDrawable1 = 499977
							iPackedDrawable2 = 0
							iPackedTexture1 = 151554
							iPackedTexture2 = 0
						BREAK 
						CASE 13
						//Ped type: CASINO_GENERIC_MALE
							iPackedDrawable1 = 649485
							iPackedDrawable2 = 0
							iPackedTexture1 = 205314
							iPackedTexture2 = 0
						BREAK 
						CASE 14
						//Ped type: CASINO_SMART_FEMALE
							iPackedDrawable1 = 144899
							iPackedDrawable2 = 20480
							iPackedTexture1 = 4098
							iPackedTexture2 = 0
						BREAK 
						CASE 15
						//Ped type: CASINO_GENERIC_FEMALE
							iPackedDrawable1 = 510986
							iPackedDrawable2 = 80
							iPackedTexture1 = 8448
							iPackedTexture2 = 16
						BREAK 
						CASE 16
						//Ped type: CASINO_SMART_MALE
							iPackedDrawable1 = 144389
							iPackedDrawable2 = 80
							iPackedTexture1 = 69633
							iPackedTexture2 = 0
						BREAK 
						CASE 17
						//Ped type: CASINO_SECURITY_GUARD_1
							iPackedDrawable1 = 65537
							iPackedDrawable2 = 8448
							iPackedTexture1 = 8192
							iPackedTexture2 = 0
						BREAK 
						CASE 18
						//Ped type: CASINO_SMART_FEMALE
							iPackedDrawable1 = 214787
							iPackedDrawable2 = 20480
							iPackedTexture1 = 0
							iPackedTexture2 = 0
						BREAK 
						CASE 19
						//Ped type: CASINO_SMART_FEMALE
							iPackedDrawable1 = 70400
							iPackedDrawable2 = 8192
							iPackedTexture1 = 2
							iPackedTexture2 = 0
						BREAK 
						CASE 20
						//Ped type: CASINO_SECURITY_GUARD_3
							iPackedDrawable1 = 66309
							iPackedDrawable2 = 8448
							iPackedTexture1 = 20481
							iPackedTexture2 = 0
						BREAK 
						CASE 21
						//Ped type: CASINO_GENERIC_MALE
							iPackedDrawable1 = 73730
							iPackedDrawable2 = 0
							iPackedTexture1 = 65793
							iPackedTexture2 = 0
						BREAK 
						CASE 22
						//Ped type: CASINO_SMART_MALE
							iPackedDrawable1 = 70146
							iPackedDrawable2 = 32
							iPackedTexture1 = 2
							iPackedTexture2 = 0
						BREAK 
						CASE 23
						//Ped type: CASINO_GENERIC_MALE
							iPackedDrawable1 = 540170
							iPackedDrawable2 = 0
							iPackedTexture1 = 135680
							iPackedTexture2 = 0
						BREAK 
						CASE 24
						//Ped type: CASINO_GENERIC_FEMALE
							iPackedDrawable1 = 78593
							iPackedDrawable2 = 16
							iPackedTexture1 = 73985
							iPackedTexture2 = 16
						BREAK 
						CASE 25
						//Ped type: CASINO_GENERIC_MALE
							iPackedDrawable1 = 56320
							iPackedDrawable2 = 0
							iPackedTexture1 = 2
							iPackedTexture2 = 0
						BREAK 
						CASE 26
						//Ped type: CASINO_GENERIC_FEMALE
							iPackedDrawable1 = 226820
							iPackedDrawable2 = 32
							iPackedTexture1 = 139264
							iPackedTexture2 = 0
						BREAK 
						CASE 27
						//Ped type: CASINO_GENERIC_MALE
							iPackedDrawable1 = 427526
							iPackedDrawable2 = 0
							iPackedTexture1 = 513
							iPackedTexture2 = 0
						BREAK 
						CASE 28
						//Ped type: CASINO_SMART_MALE
							iPackedDrawable1 = 8707
							iPackedDrawable2 = 16
							iPackedTexture1 = 2
							iPackedTexture2 = 0
						BREAK 
						CASE 29
						//Ped type: CASINO_SMART_FEMALE
							iPackedDrawable1 = 354308
							iPackedDrawable2 = 24576
							iPackedTexture1 = 135426
							iPackedTexture2 = 0
						BREAK 
						CASE 30
						//Ped type: CASINO_GENERIC_MALE
							iPackedDrawable1 = 707339
							iPackedDrawable2 = 0
							iPackedTexture1 = 139265
							iPackedTexture2 = 0
						BREAK 
						CASE 31
						//Ped type: CASINO_GENERIC_FEMALE
							iPackedDrawable1 = 78593
							iPackedDrawable2 = 16
							iPackedTexture1 = 4610
							iPackedTexture2 = 0
						BREAK 
						CASE 32
						//Ped type: CASINO_SMART_FEMALE
							iPackedDrawable1 = 70913
							iPackedDrawable2 = 0
							iPackedTexture1 = 139520
							iPackedTexture2 = 0
						BREAK 
						CASE 33
						//Ped type: CASINO_SMART_MALE
							iPackedDrawable1 = 66049
							iPackedDrawable2 = 0
							iPackedTexture1 = 1
							iPackedTexture2 = 0
						BREAK 
						CASE 34
						//Ped type: CASINO_SMART_FEMALE
							iPackedDrawable1 = 4354
							iPackedDrawable2 = 16384
							iPackedTexture1 = 69888
							iPackedTexture2 = 0
						BREAK 
						CASE 35
						//Ped type: CASINO_SMART_MALE
							iPackedDrawable1 = 221959
							iPackedDrawable2 = 96
							iPackedTexture1 = 139266
							iPackedTexture2 = 0
						BREAK 
						CASE 36
						//Ped type: CASINO_SMART_MALE
							iPackedDrawable1 = 152581
							iPackedDrawable2 = 80
							iPackedTexture1 = 139264
							iPackedTexture2 = 0
						BREAK 
						CASE 37
						//Ped type: CASINO_SMART_MALE
							iPackedDrawable1 = 226054
							iPackedDrawable2 = 96
							iPackedTexture1 = 4098
							iPackedTexture2 = 0
						BREAK 
						CASE 38
						//Ped type: CASINO_GENERIC_MALE
							iPackedDrawable1 = 291844
							iPackedDrawable2 = 0
							iPackedTexture1 = 139520
							iPackedTexture2 = 0
						BREAK 
					ENDSWITCH
				BREAK
				CASE 3
					GET_CASINO_PED_COMPONENT_DATA_MISSION_3(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
				BREAK				
				CASE 4
					GET_CASINO_PED_COMPONENT_DATA_CUTSCENE_2(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AREA_MAIN_FLOOR
		CASE CASINO_AREA_MANAGERS_OFFICE
			SWITCH iLayout
				CASE 1
					SWITCH iVariation
						CASE 0
							GET_CASINO_PED_COMPONENT_DATA_1(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
						CASE 1
							GET_CASINO_PED_COMPONENT_DATA_2(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
						CASE 2
							GET_CASINO_PED_COMPONENT_DATA_3(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iVariation
						CASE 0
							GET_CASINO_PED_COMPONENT_DATA_4(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
						CASE 1
							GET_CASINO_PED_COMPONENT_DATA_5(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
						CASE 2
							GET_CASINO_PED_COMPONENT_DATA_6(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
					ENDSWITCH
				BREAK
				CASE 3
					GET_CASINO_PED_COMPONENT_DATA_MISSION_1(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
				BREAK
				CASE 4
					GET_CASINO_PED_COMPONENT_DATA_CUTSCENE_1(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AREA_TABLE_GAMES
			SWITCH iLayout
				CASE 1
					SWITCH iVariation
						CASE 0
							GET_CASINO_PED_COMPONENT_DATA_7(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
						CASE 1
							GET_CASINO_PED_COMPONENT_DATA_8(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
						CASE 2
							GET_CASINO_PED_COMPONENT_DATA_9(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iVariation
						CASE 0
							GET_CASINO_PED_COMPONENT_DATA_10(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
						CASE 1
							GET_CASINO_PED_COMPONENT_DATA_11(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
						CASE 2
							GET_CASINO_PED_COMPONENT_DATA_12(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
					ENDSWITCH
				BREAK
				CASE 3
					GET_CASINO_PED_COMPONENT_DATA_MISSION_2(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
				BREAK
				CASE 4
					PRINTLN("Wrong combination area layout, what's going on? Layout: ", iLayout, " Area: ", CASINO_AREA_TABLE_GAMES)
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AREA_SPORTS_BETTING
			SWITCH iLayout
				CASE 1				
					SWITCH iVariation
						CASE 0
							GET_CASINO_PED_COMPONENT_DATA_13(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
						CASE 1
							GET_CASINO_PED_COMPONENT_DATA_14(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
						CASE 2
							GET_CASINO_PED_COMPONENT_DATA_15(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iVariation
						CASE 0
							GET_CASINO_PED_COMPONENT_DATA_16(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
						CASE 1
							GET_CASINO_PED_COMPONENT_DATA_17(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
						CASE 2
							GET_CASINO_PED_COMPONENT_DATA_18(iPedID, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
						BREAK
					ENDSWITCH
				BREAK
				CASE 3
				CASE 4
					PRINTLN("Wrong combination area layout, what's going on? Layout: ", iLayout, " Area: ", CASINO_AREA_SPORTS_BETTING)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
		
	#IF IS_DEBUG_BUILD	
	STRING sArea
	
	SWITCH eCasinoArea
		CASE CASINO_AREA_MAIN_FLOOR			sArea = "CASINO_AREA_MAIN_FLOOR" BREAK
		CASE CASINO_AREA_PERMANENT			sArea = "CASINO_AREA_PERMANENT" BREAK
		CASE CASINO_AREA_TABLE_GAMES		sArea = "CASINO_AREA_TABLE_GAMES" BREAK
		CASE CASINO_AREA_SPORTS_BETTING		sArea = "CASINO_AREA_SPORTS_BETTING" BREAK
		CASE CASINO_AREA_MANAGERS_OFFICE	sArea = "CASINO_AREA_MANAGERS_OFFICE" BREAK
	ENDSWITCH
	
	PRINTLN("Getting random ped variation. iVariation ", iVariation, " iPedID ", iPedID, " iPackedDrawable1 ", iPackedDrawable1, " iPackedDrawable2 ", iPackedDrawable2, " iPackedTexture1 ", iPackedTexture1, " iPackedTexture2 ", iPackedTexture2, " iLayout ", iLayout, " Area ", sArea)
	#ENDIF
	
ENDPROC
#ENDIF // FEATURE_CASINO
