USING "commands_misc.sch"
USING "commands_debug.sch"
USING "commands_script.sch"
USING "net_prints.sch"
#IF IS_DEBUG_BUILD
USING "shared_debug.sch"
#ENDIF

#IF IS_DEBUG_BUILD
INT iWaitZeroFrameCount = -1
INT iMaxInstructionsCalled
BOOL bTrackCommandsExecuted = GET_COMMANDLINE_PARAM_EXISTS("sc_TrackCommandsExecuted")
#ENDIF

// One wait to rule them all. This can be the ONLY wait from here on in.....
PROC MP_LOOP_WAIT_ZERO()
	#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() <> iWaitZeroFrameCount)
		AND (iWaitZeroFrameCount > -1)
			PRINTLN("MP_LOOP_WAIT_ZERO - frame count mismatch, there must be a rogue WAIT(0) in script ", GET_THIS_SCRIPT_NAME(), " iWaitZeroFrameCount: ", iWaitZeroFrameCount, " current Frame Count: ", GET_FRAME_COUNT())
			SCRIPT_ASSERT("iWaitZeroFrameCount at end of frame does not match, must be a rogue WAIT(0)")		
		ENDIF
	#ENDIF
	#IF IS_DEBUG_BUILD
	IF bTrackCommandsExecuted
		UPDATE_HIGHEST_NUMBER_OF_COMMANDS_EXECUTED_THIS_FRAME(iMaxInstructionsCalled)
	ENDIF	
	#ENDIF	
	WAIT(0) 
	#IF IS_DEBUG_BUILD
		iWaitZeroFrameCount = GET_FRAME_COUNT()
	#ENDIF
ENDPROC


