USING "script_maths.sch"
USING "script_conversion.sch"

STRUCT VECTOR_2D
	FLOAT x
	FLOAT y
ENDSTRUCT

FUNC VECTOR_2D ROTATE_VECTOR_2D_AROUND_VECTOR_2D(VECTOR_2D vRotateAround, VECTOR_2D vToRotate, FLOAT fAngle)
	VECTOR_2D vReturn
	FLOAT fSin = SIN(fAngle)
	FLOAT fCos = COS(fAngle)
	
	vReturn.x = vToRotate.x - vRotateAround.x
	vReturn.y = vToRotate.y - vRotateAround.y
	
	FLOAT fNewX = vReturn.x * fCos - vReturn.y * fSin
	FLOAT fNewY = vReturn.x * fSin + vReturn.y * fCos
	
	vReturn.x = fNewX + vRotateAround.x
	vReturn.y = fNewY + vRotateAround.y
	RETURN vReturn
ENDFUNC

FUNC VECTOR_2D ROTATE_VECTOR_2D_AROUND_VECTOR_2D_PRECOMPUTE(VECTOR_2D vRotateAround, VECTOR_2D vToRotate, FLOAT fSin, FLOAT fCos)
	VECTOR_2D vReturn
	
	vReturn.x = vToRotate.x - vRotateAround.x
	vReturn.y = vToRotate.y - vRotateAround.y
	
	FLOAT fNewX = vReturn.x * fCos - vReturn.y * fSin
	FLOAT fNewY = vReturn.x * fSin + vReturn.y * fCos
	
	vReturn.x = fNewX + vRotateAround.x
	vReturn.y = fNewY + vRotateAround.y
	RETURN vReturn
ENDFUNC

FUNC FLOAT DOT_PRODUCT_VECTOR_2D(VECTOR_2D v, VECTOR_2D w)
    RETURN(v.x * w.x + v.y * w.y)
ENDFUNC

FUNC VECTOR_2D GET_LEFT_PERPENDICULAR_VECTOR_2D(VECTOR_2D v)
	VECTOR_2D vToReturn
	vToReturn.x = v.y
	vToReturn.y = -v.x
	RETURN vToReturn
ENDFUNC

FUNC VECTOR_2D GET_RIGHT_PERPENDICULAR_VECTOR_2D(VECTOR_2D v)
	VECTOR_2D vToReturn
	vToReturn.x = -v.y
	vToReturn.y = v.x
	RETURN vToReturn
ENDFUNC

FUNC VECTOR_2D SUBTRACT_VECTOR_2D(VECTOR_2D v0, VECTOR_2D v1)
	VECTOR_2D vToReturn
	vToReturn.x = v0.x - v1.x
	vToReturn.y = v0.y - v1.y
	RETURN vToReturn
ENDFUNC

FUNC VECTOR_2D ADD_VECTOR_2D(VECTOR_2D v0, VECTOR_2D v1)
	VECTOR_2D vToReturn
	vToReturn.x = v0.x + v1.x
	vToReturn.y = v0.y + v1.y
	RETURN vToReturn
ENDFUNC

FUNC VECTOR_2D MULTIPLY_VECTOR_2D(VECTOR_2D v, FLOAT fScaler)
	VECTOR_2D vToReturn
	vToReturn.x = v.x * fScaler
	vToReturn.y = v.y * fScaler
	RETURN vToReturn
ENDFUNC

FUNC VECTOR_2D DIVIDE_VECTOR_2D(VECTOR_2D v, FLOAT fScaler)
	VECTOR_2D vToReturn
	vToReturn.x = v.x / fScaler
	vToReturn.y = v.y / fScaler
	RETURN vToReturn
ENDFUNC

FUNC FLOAT VECTOR_2D_MAG(VECTOR_2D v)
	RETURN SQRT(POW(v.x, 2) + POW(v.y, 2))
ENDFUNC

FUNC VECTOR_2D NORMALISE_VECTOR_2D(VECTOR_2D v)
	VECTOR_2D vToReturn
	FLOAT fMag = VECTOR_2D_MAG(v)
	IF fMag != 0.0
		FLOAT fInvMag = 1.0 / fMag
		vToReturn = MULTIPLY_VECTOR_2D(v, fInvMag)
	ELSE
		//Trying to divide by 0...
		vToReturn.x = 0
		vToReturn.y = 0
	ENDIF
	RETURN vToReturn
ENDFUNC

FUNC BOOL IS_VECTOR_2D_ZERO(VECTOR_2D v)
	RETURN v.x = 0.0 AND v.y = 0.0
ENDFUNC

FUNC VECTOR_2D INIT_VECTOR_2D(FLOAT x, FLOAT y)
	VECTOR_2D v
	v.x = x
	v.y = y
	RETURN v
ENDFUNC

FUNC FLOAT VECTOR_2D_DIST2(VECTOR_2D v0, VECTOR_2D v1)
	RETURN POW(v1.x - v0.x, 2.0) + POW(v1.y - v0.y, 2.0)
ENDFUNC

FUNC FLOAT VECTOR_2D_DIST(VECTOR_2D v0, VECTOR_2D v1)
	RETURN SQRT(POW(v1.x - v0.x, 2.0) + POW(v1.y - v0.y, 2.0))
ENDFUNC

FUNC VECTOR_2D LERP_VECTOR_2D(VECTOR_2D v1, VECTOR_2D v2, FLOAT fAlpha)
	RETURN ADD_VECTOR_2D(MULTIPLY_VECTOR_2D(v1, (1.0 - fAlpha)), MULTIPLY_VECTOR_2D(v2, fAlpha))
ENDFUNC

FUNC VECTOR_2D INTERP_VECTOR_2D(VECTOR_2D v1, VECTOR_2D v2, FLOAT fAlpha, INTERPOLATION_TYPE eInterpType = INTERPTYPE_LINEAR)
	VECTOR_2D vResult
	vResult.x = INTERP_FLOAT(v1.x, v2.x, fAlpha, eInterpType)
	vResult.y = INTERP_FLOAT(v1.y, v2.y, fAlpha, eInterpType)
	RETURN vResult
ENDFUNC

/// PURPOSE:
///    Converts a vector_2d to a string. Helpful when printing debug values.
/// PARAMS:
///    vValue - Vector_2d value to convert
///    iPrecision - floating point precision (number of decimal places required)
///    eFormat - formatting for the result (AS SEEN IN SCRIPT = <<0,0,0>>, PER COMPONENET = X-0.0 Y-0.0 Z-0.0)
/// RETURNS:
///    Returns the string format of the vector passed in.
FUNC STRING VECTOR_2D_TO_STRING(VECTOR_2D vValue, INT iPrecision = DEFAULT_FLOAT_TO_STRING_PRECISION, FORMAT_VECTOR_TO_STRING eFormat = FORMATVECTORTOSTRING_AS_SEEN_IN_SCRIPT)

	TEXT_LABEL_63 tlResult
	
	TEXT_LABEL_63 tlX, tlY
	tlX = FLOAT_TO_STRING(vValue.x, iPrecision)
	tlY = FLOAT_TO_STRING(vValue.y, iPrecision)
	
	SWITCH eFormat
		CASE FORMATVECTORTOSTRING_AS_SEEN_IN_SCRIPT
			tlResult	+= "<< "
			tlResult	+= tlX
			tlResult	+= ", "
			tlResult	+= tlY
			tlResult	+= " >>"
		BREAK
		CASE FORMATVECTORTOSTRING_PER_COMPONENT
			tlResult	+= "X-"
			tlResult	+= tlX
			tlResult	+= " Y-"
			tlResult	+= tlY
		BREAK
	ENDSWITCH
	
	RETURN TEXT_LABEL_TO_STRING( tlResult )

ENDFUNC


#IF IS_DEBUG_BUILD
/// PURPOSE:    
///    
/// PARAMS:     
///    model_name - 
/// AUTHOR:     alwyn.roberts@rockstarnorth.com
/// COMMENTS:   
PROC ADD_WIDGET_VECTOR_2D_SLIDER(STRING name, VECTOR_2D &pos_to_change, FLOAT min, FLOAT max, FLOAT slider_step)
	TEXT_LABEL_63 slider_name = name
	slider_name += " x"
	IF (slider_step != 0.0)
		ADD_WIDGET_FLOAT_SLIDER(slider_name, pos_to_change.x, min, max, slider_step)
	ELSE
		ADD_WIDGET_FLOAT_READ_ONLY(slider_name, pos_to_change.x)
	ENDIF
	slider_name = name
	slider_name += " y"
	IF (slider_step != 0.0)
		ADD_WIDGET_FLOAT_SLIDER(slider_name, pos_to_change.y, min, max, slider_step)
	ELSE
		ADD_WIDGET_FLOAT_READ_ONLY(slider_name, pos_to_change.y)
	ENDIF
ENDPROC

/// PURPOSE:    
///    
/// PARAMS:     
///    model_name - 
/// AUTHOR:    neil f
/// COMMENTS:   
PROC ADD_WIDGET_VECTOR_2D_READ_ONLY(STRING name, VECTOR_2D &pos_to_change)
	ADD_WIDGET_VECTOR_2D_SLIDER(name, pos_to_change, 0.0,0.0, 0.0)
ENDPROC

/// PURPOSE:    saves the vector to the given file at the given path.
/// PARAMS:     
///    vVector_to_save - 
///    FilePath - 
///    FileName - 
/// AUTHOR:     alwyn.roberts@rockstarnorth.com
/// COMMENTS:   
PROC SAVE_VECTOR_2D_TO_NAMED_DEBUG_FILE(VECTOR_2D vVector_to_save, string FilePath, string FileName)
    SAVE_STRING_TO_NAMED_DEBUG_FILE("(", FilePath, FileName)
    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(vVector_to_save.x, FilePath, FileName)
    SAVE_STRING_TO_NAMED_DEBUG_FILE(", ", FilePath, FileName)
    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(vVector_to_save.y, FilePath, FileName)
    SAVE_STRING_TO_NAMED_DEBUG_FILE(")", FilePath, FileName)
ENDPROC

/// PURPOSE:    saves the vector to temp_debug.txt
/// PARAMS:     
///    vVector_to_save - 
/// AUTHOR:     alwyn.roberts@rockstarnorth.com
/// COMMENTS:   
PROC SAVE_VECTOR_2D_TO_DEBUG_FILE(VECTOR_2D vVector_to_save)
    SAVE_STRING_TO_DEBUG_FILE("(")
    SAVE_FLOAT_TO_DEBUG_FILE(vVector_to_save.x)
    SAVE_STRING_TO_DEBUG_FILE(", ")
    SAVE_FLOAT_TO_DEBUG_FILE(vVector_to_save.y)
    SAVE_STRING_TO_DEBUG_FILE(")")
ENDPROC
#ENDIF

