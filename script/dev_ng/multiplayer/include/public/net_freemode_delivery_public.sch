//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name: net_freemode_delivery_public.sch																	//
// Description: System for managing deliveries of various items on freemode missions.						//
//				This is for public functions called outside of the system but providing main system logic.	//
// Written by:  Tymon																						//
// Date:  		18/01/2017																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "commands_debug.sch"
USING "net_freemode_delivery_common.sch"
USING "net_freemode_delivery_private.sch"
USING "net_realty_nightclub.sch"

#IF IS_DEBUG_BUILD
PROC FREEMODE_DELIVERY_CREATE_DEBUG_WIDGETS(FREEMODE_DELIVERY_FREEMODE_DATA &data)
	INT i, j, k
	TEXT_LABEL_63 txtLabel, txtPlayerName
	
	START_WIDGET_GROUP("Freemode Delivery")
		
		ADD_WIDGET_BOOL("Display current server state", data.debug.bShowServerState)
		ADD_WIDGET_BOOL("Display enhanced Fm delivery system display", data.debug.bShowFMDeliverySystem)
		
		START_WIDGET_GROUP("Test Dropoff Reservation")
			
			ADD_WIDGET_INT_SLIDER("Player to reserve for: ", data.debug.iPlayerToReserveFor, 0, 31, 1)
		
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("Bunker - Setup/Buy Mission")
				ADD_TO_WIDGET_COMBO("[GR] Sell - Hill Climb")
				ADD_TO_WIDGET_COMBO("[GR] Sell - Rough Terrain")
				ADD_TO_WIDGET_COMBO("[GR] Sell - Phantom")
				ADD_TO_WIDGET_COMBO("[GR] Sell - Ambushed")
				ADD_TO_WIDGET_COMBO("[GR] Sell - Follow the Leader")
				ADD_TO_WIDGET_COMBO("[GR] Sell - Move Weapons")
				
				ADD_TO_WIDGET_COMBO("[SM] Sell - Heavy Lifting")
				ADD_TO_WIDGET_COMBO("[SM] Sell - Contested")
				ADD_TO_WIDGET_COMBO("[SM] Sell - Agile Delivery")
				ADD_TO_WIDGET_COMBO("[SM] Sell - Precission Delivery")
				ADD_TO_WIDGET_COMBO("[SM] Sell - Flying Fortress")
				ADD_TO_WIDGET_COMBO("[SM] Sell - Fly Low")
				ADD_TO_WIDGET_COMBO("[SM] Sell - Air Delivery")
				ADD_TO_WIDGET_COMBO("[SM] Sell - Air Police")
	
			STOP_WIDGET_COMBO("Mission variation", data.debug.iReserveForMission)
			
			ADD_WIDGET_BOOL("Reserve Bunker dropoff for player", data.debug.bTestReserveBunkerDropOff)
			ADD_WIDGET_BOOL("Reserve mission dropoff for player", data.debug.bTestReserveSellMissionDropOff)
			
			ADD_WIDGET_BOOL("Clear all active dropoffs", data.debug.bClearAllReservedDropoffs)
			ADD_WIDGET_BOOL("Clear reserved dropoffs for selected player", data.debug.bClearPlayersReservedDropoffs)
			
			ADD_WIDGET_BOOL("Show Reserved dropoff for slot: ", data.debug.bShowReservedDropoffOverlay)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Select Freemode Dropoffs")
			
			ADD_WIDGET_STRING("Select dropofs for each mission variation.")
			ADD_WIDGET_STRING("When the mission starts the selected dropoffs will be reserved.")
			ADD_WIDGET_STRING("The number reserved will still adhere to the mission rules based on product total.")
			
			START_WIDGET_GROUP("Smuggler - Heavy Lifting")
			
				START_NEW_WIDGET_COMBO()
					REPEAT 20 i
						txtLabel = "Heavy Lifting: "
						
						IF i >= 10
							txtLabel += "Zancudo "
							j = ((i+1) - 10)
						ELSE
							txtLabel += "LSIA "
							j = (i+1)
						ENDIF
						
						txtLabel += "- Dropoff "
						txtLabel += j
						ADD_TO_WIDGET_COMBO(txtLabel)
					ENDREPEAT
				STOP_WIDGET_COMBO("Heavy Lifting dropoffs", data.debug.iDropoffSlectDebugCombo[8])
				
				ADD_WIDGET_BOOL("Confirm Heavy Lifting dropoffs and launch sell mission.", data.debug.bAssignDebugDropoff[6])
			STOP_WIDGET_GROUP()
						
			START_WIDGET_GROUP("Smuggler - Contested")
			
				START_NEW_WIDGET_COMBO()
					REPEAT 30 i
						txtLabel = "Contested"
						
						j = (i+1)
						
						txtLabel += "- Dropoff "
						txtLabel += j
						ADD_TO_WIDGET_COMBO(txtLabel)
					ENDREPEAT
				STOP_WIDGET_COMBO("Contested dropoffs", data.debug.iDropoffSlectDebugCombo[9])
				
				ADD_WIDGET_BOOL("Confirm Contested dropoffs and launch sell mission.", data.debug.bAssignDebugDropoff[7])
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Smuggler - Agile Delivery")
			
				START_NEW_WIDGET_COMBO()
					REPEAT 60 i
						txtLabel = "Agile Delivery"
						
						j = (i+1)
						
						txtLabel += "- Dropoff "
						txtLabel += j
						ADD_TO_WIDGET_COMBO(txtLabel)
					ENDREPEAT
				STOP_WIDGET_COMBO("Agile Delivery dropoffs", data.debug.iDropoffSlectDebugCombo[10])
				
				ADD_WIDGET_BOOL("Confirm Agile Delivery dropoffs and launch sell mission.", data.debug.bAssignDebugDropoff[8])
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Smuggler - Precision Delivery")
			
				START_NEW_WIDGET_COMBO()
					REPEAT 30 i
						txtLabel = "Precision Deliv: "
						
						IF i >= 15
							txtLabel += "Zancudo "
							j = ((i+1) - 15)
						ELSE
							txtLabel += "LSIA "
							j = (i+1)
						ENDIF
						
						txtLabel += "- Drop "
						txtLabel += j
						ADD_TO_WIDGET_COMBO(txtLabel)
					ENDREPEAT
				STOP_WIDGET_COMBO("Precision Delivery dropoffs", data.debug.iDropoffSlectDebugCombo[11])
				
				ADD_WIDGET_BOOL("Confirm Precision Delivery dropoffs and launch sell mission.", data.debug.bAssignDebugDropoff[9])
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Smuggler - Flying Fortress")
				START_NEW_WIDGET_COMBO()
					REPEAT 10 i
						txtLabel = "Flying Fortress: "
						
						IF i >= 5
							txtLabel += "Zancudo "
							j = ((i+1) - 5)
						ELSE
							txtLabel += "LSIA "
							j = (i+1)
						ENDIF
						
						txtLabel += "- Route "
						txtLabel += j
						
						ADD_TO_WIDGET_COMBO(txtLabel)
					ENDREPEAT
				STOP_WIDGET_COMBO("Flying Fortress dropoff routes", data.debug.iDropoffSlectDebugCombo[12])
				
				ADD_WIDGET_BOOL("Confirm Flying Fortress route and launch sell mission.", data.debug.bAssignDebugDropoff[10])
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Smuggler - Fly Low")
			
				START_NEW_WIDGET_COMBO()
					REPEAT 33 i
						txtLabel = "Fly Low"
						
						j = (i+1)
						
						txtLabel += "- Dropoff "
						txtLabel += j
						ADD_TO_WIDGET_COMBO(txtLabel)
					ENDREPEAT
				STOP_WIDGET_COMBO("Fly Low dropoffs", data.debug.iDropoffSlectDebugCombo[13])
				
				ADD_WIDGET_BOOL("Confirm Fly Low dropoffs and launch sell mission.", data.debug.bAssignDebugDropoff[11])
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Smuggler - Air Delivery")
			
				START_NEW_WIDGET_COMBO()
					REPEAT 30 i
						txtLabel = "Air Delivery"
						
						j = (i+1)
						
						txtLabel += "- Dropoff "
						txtLabel += j
						ADD_TO_WIDGET_COMBO(txtLabel)
					ENDREPEAT
				STOP_WIDGET_COMBO("Air Delivery dropoffs", data.debug.iDropoffSlectDebugCombo[14])
				
				ADD_WIDGET_BOOL("Confirm Air Delivery dropoffs and launch sell mission.", data.debug.bAssignDebugDropoff[12])
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Smuggler - Air Police")
			
				START_NEW_WIDGET_COMBO()
					REPEAT 30 i
						txtLabel = "Air Police"
						
						j = (i+1)
						
						txtLabel += "- Dropoff "
						txtLabel += j
						ADD_TO_WIDGET_COMBO(txtLabel)
					ENDREPEAT
				STOP_WIDGET_COMBO("Air Police dropoffs", data.debug.iDropoffSlectDebugCombo[15])
				
				ADD_WIDGET_BOOL("Confirm Air Police dropoffs and launch sell mission.", data.debug.bAssignDebugDropoff[13])
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Smuggler - Under the Radar")
			
				START_NEW_WIDGET_COMBO()
					REPEAT 30 i
						txtLabel = "Under Radar: "
						
						IF i >= 15
							txtLabel += "Zancudo "
							j = ((i+1) - 15)
						ELSE
							txtLabel += "LSIA "
							j = (i+1)
						ENDIF
						
						txtLabel += "- Dropoff "
						txtLabel += j
						ADD_TO_WIDGET_COMBO(txtLabel)
					ENDREPEAT
				STOP_WIDGET_COMBO("Under the Radar dropoffs", data.debug.iDropoffSlectDebugCombo[16])
				
				ADD_WIDGET_BOOL("Confirm Under the Radar dropoffs and launch sell mission.", data.debug.bAssignDebugDropoff[14])
			STOP_WIDGET_GROUP()
						
			START_WIDGET_GROUP("Gunrunning - Move Weapons")
				ADD_WIDGET_STRING("Select 3 !Different! dropoffs for move weapons")
				REPEAT 3 k
					START_NEW_WIDGET_COMBO()
						REPEAT 60 i
							txtLabel = "Move Weapons - "
							
							IF i >= 30
								txtLabel += "City "
								j = ((i+1) - 30)
							ELSE
								txtLabel += "Country "
								j = (i+1)
							ENDIF
							
							txtLabel += "- Dropoff "
							txtLabel += j
							ADD_TO_WIDGET_COMBO(txtLabel)
						ENDREPEAT
					STOP_WIDGET_COMBO("Move Weapons dropoffs", data.debug.iDropoffSlectDebugCombo[k])
				ENDREPEAT
				
				ADD_WIDGET_BOOL("Confirm Move Weapons dropoffs and launch sell mission.", data.debug.bAssignDebugDropoff[0])
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Gunrunning - Follow The Leader")
				START_NEW_WIDGET_COMBO()
					REPEAT 20 i
						txtLabel = "Follow Leader "
						
						IF i >= 10
							txtLabel += "City "
							j = ((i+1) - 10)
						ELSE
							txtLabel += "Country "
							j = (i+1)
						ENDIF
						
						txtLabel += "- Dropoff "
						txtLabel += j
						
						ADD_TO_WIDGET_COMBO(txtLabel)
					ENDREPEAT
				STOP_WIDGET_COMBO("Follow The Leader dropoffs", data.debug.iDropoffSlectDebugCombo[3])
				
				ADD_WIDGET_BOOL("Does this group use extra props? ", data.debug.bFTLRouteHasProps)
				ADD_WIDGET_BOOL("Confirm Follow the leader dropoff and launch sell mission.", data.debug.bAssignDebugDropoff[1])
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Gunrunning - Ambushed")
				
				START_NEW_WIDGET_COMBO()
					REPEAT 13 i
						txtLabel = "Ambushed "
						
						IF i >= 6
							txtLabel += "Country "
							j = ((i+1) - 6)
						ELSE
							txtLabel += "City "
							j = (i+1)
						ENDIF
						
						txtLabel += "- Group "
						txtLabel += j
						
						ADD_TO_WIDGET_COMBO(txtLabel)
					ENDREPEAT
				STOP_WIDGET_COMBO("Ambushed dropoff groups", data.debug.iDropoffSlectDebugCombo[4])				
				
				ADD_WIDGET_BOOL("Confirm Ambushed Route and launch sell mission.", data.debug.bAssignDebugDropoff[2])
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Gunrunning - Hill Climb")
				START_NEW_WIDGET_COMBO()
					REPEAT 20 i
						txtLabel = "Hill Climb "
						
						IF i >= 10
							txtLabel += "Country "
							j = ((i+1) - 10)
						ELSE
							txtLabel += "City "
							j = (i+1)
						ENDIF
						
						txtLabel += "- Route "
						txtLabel += j
						
						ADD_TO_WIDGET_COMBO(txtLabel)
					ENDREPEAT
				STOP_WIDGET_COMBO("Hill Climb dropoff routes", data.debug.iDropoffSlectDebugCombo[5])
				
				ADD_WIDGET_BOOL("Confirm Hill Climb route and launch sell mission.", data.debug.bAssignDebugDropoff[3])
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Gunrunning - Rough Terrain")
				
				START_NEW_WIDGET_COMBO()
					REPEAT 20 i
						txtLabel = "Rough Terrain "
						
						IF i >= 10
							txtLabel += "Country "
							j = ((i+1) - 10)
						ELSE
							txtLabel += "City "
							j = (i+1)
						ENDIF
						
						txtLabel += "- Route "
						txtLabel += j
						
						ADD_TO_WIDGET_COMBO(txtLabel)
					ENDREPEAT
				STOP_WIDGET_COMBO("Rough Terrain dropoff groups", data.debug.iDropoffSlectDebugCombo[6])
				
				ADD_WIDGET_BOOL("Confirm Rough Terrain dropoff group and launch sell mission.", data.debug.bAssignDebugDropoff[4])
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Gunrunning - Phantom")
				
				START_NEW_WIDGET_COMBO()
					REPEAT 20 i
						txtLabel = "Phantom - "
						
						IF i >= 10
							txtLabel += "City "
							j = ((i+1) - 10)
						ELSE
							txtLabel += "Country "
							j = (i+1)
						ENDIF
						
						txtLabel += "- Dropoff "
						txtLabel += j
						ADD_TO_WIDGET_COMBO(txtLabel)
					ENDREPEAT
				STOP_WIDGET_COMBO("Phantom dropoffs", data.debug.iDropoffSlectDebugCombo[7])
				
				ADD_WIDGET_BOOL("Confirm Phantom dropoff and launch sell mission.", data.debug.bAssignDebugDropoff[5])
			STOP_WIDGET_GROUP()
			
			
			ADD_WIDGET_BOOL("Clear all debug dropoff selections", data.debug.bClearDBGSelDropoffs)
			j = 0
		STOP_WIDGET_GROUP()
	
		ADD_WIDGET_BOOL("Print server state", data.debug.bPrintServerState)
		ADD_WIDGET_BOOL("All dropoffs accept everything", g_FreemodeDeliveryData.d_bAllDropoffsAcceptEverything)
		ADD_WIDGET_BOOL("Show debug overlay", g_FreemodeDeliveryData.d_bShowDebugOverlay)
		ADD_WIDGET_BOOL("Get pos relative to focus vehicle", data.debug.bGetRelativePosToFocusEntity)
		
		START_WIDGET_GROUP("Create a deliverable")
			ADD_WIDGET_BOOL("Create deliverables", data.debug.bCreateDeliverableID)
			REPEAT COUNT_OF(data.debug.iCreateDeliverableIDIndices) i
				data.debug.iCreateDeliverableIDIndices[i] = -1
				
				txtLabel = "Deliverable "
				txtLabel += i
				txtLabel += " ID"
				ADD_WIDGET_INT_SLIDER(txtLabel, data.debug.iCreateDeliverableIDIndices[i], -1, 8, 1)
				
				txtLabel = "Deliverable "
				txtLabel += i
				txtLabel += " Player ID"
				
				START_NEW_WIDGET_COMBO()
				REPEAT NUM_NETWORK_REAL_PLAYERS() j
					IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(j), FALSE)
						ADD_TO_WIDGET_COMBO(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(j)))
					ELSE
						txtPlayerName = "Player "
						txtPlayerName += j
						ADD_TO_WIDGET_COMBO(txtPlayerName)
					ENDIF
				ENDREPEAT
				STOP_WIDGET_COMBO(txtLabel, data.debug.iCreateDeliverableIDPlayerIDs[i])
				
				txtLabel = "Deliverable "
				txtLabel += i
				txtLabel += " Type"
				
				START_NEW_WIDGET_COMBO()
				REPEAT COUNT_OF(FREEMODE_DELIVERABLE_TYPE) j
					ADD_TO_WIDGET_COMBO(FREEMODE_DELIVERY_GET_DELIVERABLE_TYPE_DEBUG_NAME(INT_TO_ENUM(FREEMODE_DELIVERABLE_TYPE, j)))
				ENDREPEAT
				STOP_WIDGET_COMBO(txtLabel, data.debug.iCreateDeliverableType[i])
				
				txtLabel = "Deliverable "
				txtLabel += i
				txtLabel += " Quantity"
				data.debug.iCreateDeliverableQty[i] = 1
				ADD_WIDGET_INT_SLIDER(txtLabel, data.debug.iCreateDeliverableQty[i], 1, 5, 1)
				
			ENDREPEAT
			ADD_WIDGET_BOOL("Cleanup deliverables", data.debug.bCleanupDeliverables)
			ADD_WIDGET_BOOL("Set player in possession", data.debug.bSetPlayerInPossessionOfDeliverable)
			ADD_WIDGET_INT_SLIDER("Deliverable to set in possession", data.debug.iDeliverableToSetInPossession, 0, 3, 1)
			ADD_WIDGET_BOOL("Clear player possession", data.debug.bClearPlayerPossession)
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("Dropoffs")
			/*
			START_NEW_WIDGET_COMBO()
			REPEAT FREEMODE_DELIVERY_DROPOFF_COUNT i
				ADD_TO_WIDGET_COMBO(FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, i)))
			ENDREPEAT
			STOP_WIDGET_COMBO("Selected dropoff", data.debug.iCurrentSelectedDropoff)
			*/
			ADD_WIDGET_INT_SLIDER("Selected dropoff", data.debug.iCurrentSelectedDropoff, 1, ENUM_TO_INT(FREEMODE_DELIVERY_DROPOFF_COUNT), 1)
			data.debug.widgetDropoffName = ADD_TEXT_WIDGET("Name")
			
			ADD_WIDGET_BOOL("Teleport", data.debug.bDoTeleport)
			
			START_NEW_WIDGET_COMBO()
			REPEAT NUM_NETWORK_REAL_PLAYERS() j
				txtPlayerName = "Player "
				txtPlayerName += j
				ADD_TO_WIDGET_COMBO(txtPlayerName)
			ENDREPEAT
			STOP_WIDGET_COMBO("Set active for player", data.debug.iDropoffActiveForPlayer)
			
			ADD_WIDGET_BOOL("Toggle active", data.debug.bToggleActive)
			ADD_WIDGET_BOOL("Request active", data.debug.bRequestActivate)
			ADD_WIDGET_BOOL("Request deactivate all", data.debug.bRequestDeactivate)
			ADD_WIDGET_INT_READ_ONLY("Active", data.debug.iIsActive)
			ADD_WIDGET_BOOL("Toggle disabled", data.debug.bToggleDisabled)
			ADD_WIDGET_INT_READ_ONLY("Disabled", data.debug.iIsDisabled)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	
	data.debug.bClearDBGSelDropoffs = TRUE
ENDPROC

PROC _FREEMODE_DELIVERY_DRAW_DEBUG_OVERLAY()
	IF FREEMODE_DELIVERY_IS_PLAYER_IN_POSSESSION_OF_ANY_DELIVERABLE(PLAYER_ID())
		FREEMODE_DELIVERABLE_ID deliverableID = FREEMODE_DELIVERY_GET_DELIVERABLE_PLAYER_POSSESSES(PLAYER_ID())
		
		TEXT_LABEL_63 txt = "DLVRB("
		
		IF FREEMODE_DELIVERY_IS_DELIVERABLE_VALID(deliverableID)
			txt += "V"
		ELSE
			txt += "INV"
		ENDIF
		
		IF FREEMODE_DELIVERY_DOES_DELIVERABLE_ID_EXIST_ON_SERVER(deliverableID)
			txt += ", SRV): "
		ELSE
			txt += ", ---): "
		ENDIF
		
		txt += "P" txt += NATIVE_TO_INT(deliverableID.pOwner)
		txt += ", " txt += deliverableID.iIndex
		txt += ", TYP" txt += ENUM_TO_INT(FREEMODE_DELIVERY_GET_DELIVERABLE_TYPE(deliverableID))
		txt += ", STA" txt += ENUM_TO_INT(FREEMODE_DELIVERY_GET_DELIVERABLE_STATUS(deliverableID))
		
		txt += " - DROPOFF: "
		txt += ENUM_TO_INT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.eCurrentActiveDropoff)
		
		IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.eCurrentActiveDropoff != FREEMODE_DELIVERY_DROPOFF_INVALID
			IF FREEMODE_DELIVERY_IS_DROPOFF_DISABLED(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.eCurrentActiveDropoff)
				txt += " (d)"
			ENDIF
		ENDIF
		
		SET_TEXT_SCALE(0.0000, 0.25)
		SET_TEXT_COLOUR(28, 219, 203, 192)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.005, 0.005, "STRING", txt)
	ENDIF
ENDPROC

PROC DEBUG_SHOW_SERVER_DROPOFF_STATE()
	
	FLOAT fRowSpacing = 0.025
	FLOAT fYPos
	TEXT_LABEL_31 txtPlayerName
	TEXT_LABEL_31 txtPlayerID
	TEXT_LABEL_31 txtStatus
	TEXT_LABEL_31 txtID
	TEXT_LABEL_31 txtType
	TEXT_LABEL_31 txtQty
	
	//Background rectangle
	DRAW_RECT(0.14, 0.13 , 0.23, 0.2, 0, 0, 0, 200)
	
	//Title
    SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_COLOUR(0, 255, 0, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.0268, 0.0295, "STRING", "Server Deliverables state")
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275, 0.068, "STRING", "ID")
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.04, 0.068, "STRING", "Player")
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.15, 0.068, "STRING", "Deliverable Details")
	
	//Business data
	INT i
		
	REPEAT 5 i
				
		fYPos = ((i + 1) * fRowSpacing) + 0.068
		
		//IF GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].deliverableID.iIndex != -1
			txtPlayerID = ""
			txtPlayerID += NATIVE_TO_INT(GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].deliverableID.pOwner)
			
			IF GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].deliverableID.pOwner != INVALID_PLAYER_INDEX()
			AND IS_NET_PLAYER_OK(GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].deliverableID.pOwner, FALSE, FALSE)
				txtPlayerName = GET_PLAYER_NAME(GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].deliverableID.pOwner)
			ELSE
				txtPlayerName = "INVALID"
			ENDIF
			
			IF GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].eStatus = DELIVERABLE_STATUS_INITIAL
				txtStatus = "Initial"
			ELIF GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].eStatus = DELIVERABLE_STATUS_DELIVERED
				txtStatus = "Delivered"
			ELIF GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].eStatus = DELIVERABLE_STATUS_IN_PROGRESS
				txtStatus = "In progress"
			ELIF GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].eStatus = DELIVERABLE_STATUS_INVALID
				txtStatus = "Invalid"
			ENDIF
			
			txtType = FREEMODE_DELIVERY_GET_DELIVERABLE_TYPE_DEBUG_NAME(GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].eType)
			
			txtQty = ""
			txtQty += GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].iQuantity
			
			txtID = GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].deliverableID.iIndex
			
			txtID 			= _GET_PADDED_STRING(txtID, 5)
			txtPlayerID 	= _GET_PADDED_STRING(txtPlayerID, 9)
			txtPlayerName 	= _GET_PADDED_STRING(txtPlayerName, 31)
			txtStatus 		= _GET_PADDED_STRING(txtStatus, 11)
			txtType 		= _GET_PADDED_STRING(txtType, 11)
			txtQty 			= _GET_PADDED_STRING(txtQty, 3)
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275, fYPos, "STRING", txtID)
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.04, fYPos, "STRING", txtPlayerID)
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, fYPos, "STRING", txtPlayerName)
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.15, fYPos, "STRING", txtStatus)
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.2, fYPos, "STRING", txtType)
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.24, fYPos, "STRING", txtQty)
			
		//ENDIF	
		
	ENDREPEAT	
ENDPROC

PROC DEBUG_INIT_DROPOFF_REQUEST_DATA(FREEMODE_DELIVERY_DROPOFFS &eSelectedDropoffs[3], FREEMODE_DELIVERY_ROUTES eSelectedRoute)
	UNUSED_PARAMETER(eSelectedRoute)
	eSelectedRoute = FREEMODE_DELIVERY_ROUTE_NO_ROUTE
	eSelectedDropoffs[0] = FREEMODE_DELIVERY_DROPOFF_INVALID
	eSelectedDropoffs[1] = FREEMODE_DELIVERY_DROPOFF_INVALID
	eSelectedDropoffs[2] = FREEMODE_DELIVERY_DROPOFF_INVALID
ENDPROC

FUNC INT FIND_DROPOFF_SLOT_FOR_LOCAL_PLAYER()
	INT i
	
	REPEAT MAX_NUM_GANGS i
		IF GlobalServerBD_BlockB.freemodeDelivery.sDropoffStates[i].pOwner = PLAYER_ID()
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

ENUM BG_TEXT_COLOUR
	BGT_RED,
	BGT_GREEN,
	BGT_LIGHT_BLUE,
	BGT_BLUE,
	BGT_PURPLE,
	BGT_WHITE
ENDENUM

PROC GET_COLOUR_VALUES(BG_TEXT_COLOUR colour, INT &r, INT &g, INT &b)
	SWITCH colour
		CASE BGT_RED 		r = 255 g = 0 	b = 0 	BREAK
		CASE BGT_GREEN 		r = 0 	g = 255 b = 0 	BREAK
		CASE BGT_BLUE 		r = 0 	g = 0 	b = 255 BREAK
		CASE BGT_WHITE 		r = 255 g = 255 b = 255 BREAK
		CASE BGT_PURPLE		r = 204 g = 102 b = 255 BREAK
		CASE BGT_LIGHT_BLUE	r = 102 g = 204 b = 255 BREAK
	ENDSWITCH
ENDPROC

PROC DISPLAY_TEXT_WITH_LITERAL_STRING_CUSTOM(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pLiteralString, BG_TEXT_COLOUR colour, FLOAT fSize)
	
	INT r, g, b
	GET_COLOUR_VALUES(colour, r, g, b)
	
	SET_TEXT_SCALE(fSize, fSize)
	SET_TEXT_COLOUR(r, g, b, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(DisplayAtX,DisplayAtY, "STRING", pLiteralString)
ENDPROC

FUNC INT GET_NUM_DROPOFFS_ACTIVE_FOR_LOCAL_PLAYER(INT iSlot)
	INT iCount = 0
	
	IF g_FreemodeDeliveryData.sLocalDropoff.eDropoff != FREEMODE_DELIVERY_DROPOFF_INVALID
		iCount++
	ENDIF
	
	IF iSlot < 0
	OR iSlot >= MAX_NUM_GANGS
		RETURN iCount
	ENDIF
	
	INT i
	
	REPEAT FREEMODE_DELIVERY_MAX_ACTIVE_DROPOFFS i
		IF GlobalServerBD_BlockB.freemodeDelivery.sDropoffStates[iSlot].eActiveDropoffs[i] != FREEMODE_DELIVERY_DROPOFF_INVALID
			iCount ++
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC

PROC DISPLAY_DROPOFF(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FM_DELIVERY_MISSION_ID sMissionID, FLOAT x, FLOAT y, INT iSlot, BG_TEXT_COLOUR eColour)
	//Print the dropoff ID
	TEXT_LABEL_31 tlDropoff = iSlot
	
	IF iSlot = -1
		tlDropoff = "LD: "
	ELSE
		tlDropoff += ". "
		IF iSlot = 1
			tlDropoff += " "
		ENDIF
	ENDIF
	
	tlDropoff += ENUM_TO_INT(eDropoffID)
	
	//Add in the mission ID
	tlDropoff += "  Mi:  "
	tlDropoff += sMissionID.iScriptHash
	tlDropoff += " : "
	tlDropoff += sMissionID.iScriptInstance
	
	DISPLAY_TEXT_WITH_LITERAL_STRING_CUSTOM(x, y, tlDropoff, eColour, 0.3)
ENDPROC

PROC SHOW_RESERVED_DROPOFF_DATA()
	
	BG_TEXT_COLOUR eColour
	INT iSlot 		= FIND_DROPOFF_SLOT_FOR_LOCAL_PLAYER()
	INT iRowCount	= (GET_NUM_DROPOFFS_ACTIVE_FOR_LOCAL_PLAYER(iSlot) + 1)
	
	//X	
	FLOAT fBGRectx = 0.1
	
	IF GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_FIRST
		fBGRectx = 0.35
	ENDIF
	
	CONST_FLOAT fBGWidth 0.15
	
	FLOAT fLeftMostX = (fBGRectx - (fBGWidth/2.0))
	
	//Y
	FLOAT fTopMostY	= 0.025
	
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		fTopMostY = 0.175
	ELIF GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_FIRST
		fTopMostY = 0.1
	ENDIF
	
	CONST_FLOAT fRowSpacing 0.0195
	
	FLOAT fBGHeight 	= (fRowSpacing * iRowCount) + fRowSpacing
	FLOAT fBGRecty 		= fTopMostY + (fBGHeight/2)
	FLOAT fRowOneY 		= (fTopMostY - 0.001)	
	FLOAT fRowTwoY 		= (fRowOneY + fRowSpacing)
	
	IF iSlot < 0
	OR iSlot > MAX_NUM_GANGS
		IF g_FreemodeDeliveryData.sLocalDropoff.eDropoff != FREEMODE_DELIVERY_DROPOFF_INVALID
			//Background rectangle
			DRAW_RECT(fBGRectx, fBGRecty , fBGWidth, fBGHeight, 0, 0, 0, 200)
			
			IF GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.eCurrentActiveDropoff = g_FreemodeDeliveryData.sLocalDropoff.eDropoff
				eColour = BGT_PURPLE
			ENDIF
			
			DISPLAY_TEXT_WITH_LITERAL_STRING_CUSTOM(fLeftMostX, fRowOneY, "Local dropoff", BGT_GREEN, 0.3)
			DISPLAY_DROPOFF(g_FreemodeDeliveryData.sLocalDropoff.eDropoff, g_FreemodeDeliveryData.sLocalDropoff.sMissionID,
							fLeftMostX, fRowTwoY, -1, eColour)
		ELSE
			//Background rectangle
			DRAW_RECT(fBGRectx, fBGRecty , fBGWidth, fBGHeight, 0, 0, 0, 200)
			
			DISPLAY_TEXT_WITH_LITERAL_STRING_CUSTOM(fLeftMostX, fRowOneY, "No active dropoffs", BGT_GREEN, 0.3)
		ENDIF
		
		EXIT
	ENDIF
	
	FLOAT fYPos
	TEXT_LABEL_31 tlSlot = "Slot: "
	tlSlot += iSlot
	
	//Background rectangle
	DRAW_RECT(fBGRectx, fBGRecty, fBGWidth, fBGHeight, 0, 0, 0, 200)
	
	//Title
	DISPLAY_TEXT_WITH_LITERAL_STRING_CUSTOM(fLeftMostX, fRowOneY, "Dropoffs", BGT_GREEN, 0.35)	
	//Slot #
	DISPLAY_TEXT_WITH_LITERAL_STRING_CUSTOM(fLeftMostX, fRowTwoY, tlSlot, BGT_WHITE, 0.3)
	
	//Dropoff data
	INT i, iCount
	
	IF g_FreemodeDeliveryData.sLocalDropoff.eDropoff != FREEMODE_DELIVERY_DROPOFF_INVALID
		fYPos = ((iCount + 1) * fRowSpacing) + fRowTwoY
		iCount ++
		
		IF GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.eCurrentActiveDropoff = g_FreemodeDeliveryData.sLocalDropoff.eDropoff
			eColour = BGT_PURPLE
		ENDIF
		
		DISPLAY_DROPOFF(g_FreemodeDeliveryData.sLocalDropoff.eDropoff, 
						g_FreemodeDeliveryData.sLocalDropoff.sMissionID,
						fLeftMostX, fYPos, -1, eColour)
	ENDIF
		
	REPEAT FREEMODE_DELIVERY_MAX_ACTIVE_DROPOFFS i
		IF GlobalServerBD_BlockB.freemodeDelivery.sDropoffStates[iSlot].eActiveDropoffs[i] != FREEMODE_DELIVERY_DROPOFF_INVALID
			
			fYPos = ((iCount + 1) * fRowSpacing) + fRowTwoY
			iCount ++
			
			IF GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.eCurrentActiveDropoff = GlobalServerBD_BlockB.freemodeDelivery.sDropoffStates[iSlot].eActiveDropoffs[i]
				eColour = BGT_PURPLE
			ELSE
				eColour = BGT_WHITE
			ENDIF
			
			DISPLAY_DROPOFF(GlobalServerBD_BlockB.freemodeDelivery.sDropoffStates[iSlot].eActiveDropoffs[i], 
							GlobalServerBD_BlockB.freemodeDelivery.sDropoffStates[iSlot].sMissionID[i],
							fLeftMostX, fYPos, i, eColour)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_NUMBER_OF_DELIVERABLES_TO_DISPLAY()
	INT i, iCount
		
	REPEAT FREEMODE_DELIVERY_GLOBAL_SERVER_BD_DELIVERABLE_POOL_SIZE i
		IF FREEMODE_DELIVERY_IS_DELIVERABLE_VALID(GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].deliverableID)
			iCount ++
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC

FUNC BOOL FREEMODE_DELIVERY_IS_PLAYER_HOLDING_THIS_DELIVERABLE(PLAYER_INDEX playerID, FREEMODE_DELIVERABLE_ID eDeiliverable)
	INT i
	
	IF playerID = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
			
	REPEAT FREEMODE_DELIVERY_MAX_HELD_DELIVERABLES i
		//Is it valid
		IF FREEMODE_DELIVERY_IS_DELIVERABLE_VALID(GlobalplayerBD_FM_3[NATIVE_TO_INT(playerID)].freemodeDelivery.currentDeliverable[i])
		AND FREEMODE_DELIVERY_ARE_DELIVERABLES_EQUAL(GlobalplayerBD_FM_3[NATIVE_TO_INT(playerID)].freemodeDelivery.currentDeliverable[i], eDeiliverable)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SHOW_DELIVERABLE_STATE()
	
	FLOAT fYPos
	INT iNumRows 			= GET_NUMBER_OF_DELIVERABLES_TO_DISPLAY()
	BOOL bNoDeliverables 	= FALSE
	
	IF iNumRows = 0
		iNumRows = 1
		bNoDeliverables = TRUE
	ENDIF
	
	//X	
	FLOAT fBGRectx = 0.85
	
	IF IS_PHONE_ONSCREEN()
	AND iNumRows > 25
		fBGRectx = 0.72
	ENDIF
	
	CONST_FLOAT fBGWidth 0.225
	
	FLOAT fLeftMostX 	= (fBGRectx - (fBGWidth/2.0))
	FLOAT fColOneX 		= (fLeftMostX + 0.0007)
	
	//Y
	CONST_FLOAT fRowSpacing 0.0195
	CONST_FLOAT fTopMostY	0.025
	
	FLOAT fBGHeight 	= (fRowSpacing * iNumRows) + fRowSpacing
	FLOAT fBGRecty 		= fTopMostY + (fBGHeight/2)
	FLOAT fRowOneY 		= (fTopMostY - 0.001)	
	FLOAT fRowTwoY 		= (fRowOneY + fRowSpacing)
	
	//Background rectangle
	DRAW_RECT(fBGRectx, fBGRecty, fBGWidth, fBGHeight, 0, 0, 0, 200)
	
	//Title
	DISPLAY_TEXT_WITH_LITERAL_STRING_CUSTOM(fLeftMostX, fRowOneY, "Server Deliverables state", BGT_GREEN, 0.35)
	
	IF bNoDeliverables
		DISPLAY_TEXT_WITH_LITERAL_STRING_CUSTOM(fColOneX, fRowTwoY, "No active deliverables", BGT_WHITE, 0.3)
	ELSE
		//Deliverable data
		INT i, iCount
		TEXT_LABEL_63 txtDeliverable
			
		REPEAT FREEMODE_DELIVERY_GLOBAL_SERVER_BD_DELIVERABLE_POOL_SIZE i
			IF FREEMODE_DELIVERY_IS_DELIVERABLE_VALID(GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].deliverableID)
				fYPos = (iCount * fRowSpacing) + fRowTwoY
				
				BG_TEXT_COLOUR eColour
				
				//Index:
				txtDeliverable = i
				
				//Deliverable ID
				txtDeliverable += "  ID: "		
				txtDeliverable += GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].deliverableID.iIndex			
				
				//Owner:
				txtDeliverable += "  Plyr: "
				txtDeliverable += NATIVE_TO_INT(GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].deliverableID.pOwner)
				
				//Status
				txtDeliverable += "  State: "
				IF GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].eStatus = DELIVERABLE_STATUS_INITIAL
					txtDeliverable += "I"
				ELIF GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].eStatus = DELIVERABLE_STATUS_DELIVERED
					txtDeliverable += "D"
				ELIF GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].eStatus = DELIVERABLE_STATUS_IN_PROGRESS
					txtDeliverable += "P"
				ELIF GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].eStatus = DELIVERABLE_STATUS_INVALID
					txtDeliverable += "V"
				ENDIF
				
				//Type:
				txtDeliverable += "  Typ: "
				txtDeliverable += ENUM_TO_INT(GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].eType)
				
				//Quantity:
				txtDeliverable += "  Q: "
				txtDeliverable += GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].iQuantity
				
				eColour = BGT_WHITE
				
				//Holding?
				IF FREEMODE_DELIVERY_IS_PLAYER_HOLDING_THIS_DELIVERABLE(PLAYER_ID(), GlobalServerBD_BlockB.freemodeDelivery.deliverablePool[i].deliverableID)
					eColour = BGT_PURPLE
				ENDIF
				
				DISPLAY_TEXT_WITH_LITERAL_STRING_CUSTOM(fColOneX, fYPos, txtDeliverable, eColour, 0.3)
				
				iCount ++
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC STORE_FM_DELIVERY_EVENT(FREEMODE_DELIVERY_FREEMODE_DEBUG_DATA &debugData, FM_DELIVERY_EVENT_HISTORY &sData)	
	debugData.fmDeliveryHistory[debugData.iCurrentEvent] = sData	
	debugData.iCurrentEvent = (debugData.iCurrentEvent + 1) % MAX_FM_DELIVERY_EVENT_STORE
ENDPROC

FUNC INT GET_NUM_FM_DELIVERY_EVENTS_TO_DISPLAY(FREEMODE_DELIVERY_FREEMODE_DEBUG_DATA &debugData)
	INT i, iCount
	
	REPEAT MAX_FM_DELIVERY_EVENT_STORE i
		IF debugData.fmDeliveryHistory[i].iSender != -1
			iCount ++
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC

PROC MAINTAIN_FM_EVENT_LIST_TO(FREEMODE_DELIVERY_FREEMODE_DEBUG_DATA &debugData)
	INT i
	
	REPEAT MAX_FM_DELIVERY_EVENT_STORE i
		IF debugData.fmDeliveryHistory[i].iSender != -1
			IF (GET_FRAME_COUNT() - debugData.fmDeliveryHistory[i].iFC) > 1000
				debugData.fmDeliveryHistory[i].iFC = 0
				debugData.fmDeliveryHistory[i].iSender = -1
				debugData.fmDeliveryHistory[i].sEventName = ""
				debugData.fmDeliveryHistory[i].tlEventPrint = ""
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC DEBUG_DISPLAY_PROCESS_REQUEST_DELIVERABLE_ID(FREEMODE_DELIVERY_FREEMODE_DEBUG_DATA &debugData, INT iEvent)
	
	FM_DELIVERY_EVENT_HISTORY sData					
	SCRIPT_EVENT_DATA_FREEMODE_DELIVERY_REQUEST_DELIVERABLE_ID Event
	
	sData.sEventName = "Request deliverable"
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEvent, Event, SIZE_OF(Event))
		
		sData.iFC 			= GET_FRAME_COUNT()
		sData.iSender 		= NATIVE_TO_INT(Event.Details.FromPlayerIndex)
		
		sData.tlEventPrint = "ID: "
		sData.tlEventPrint += Event.deliverableID.iIndex
		sData.tlEventPrint += " : "
		sData.tlEventPrint += NATIVE_TO_INT(Event.deliverableID.pOwner)
		
		sData.tlEventPrint += " Tp: "
		sData.tlEventPrint += ENUM_TO_INT(Event.eType)
		
		sData.tlEventPrint += " DO: "
		sData.tlEventPrint += ENUM_TO_INT(Event.eDropoff)
		
		sData.tlEventPrint += " Q: "
		sData.tlEventPrint += Event.iQuantity
		
		sData.tlEventPrint += " M: "
		sData.tlEventPrint += Event.sMissionID.iScriptHash
		sData.tlEventPrint += " : "
		sData.tlEventPrint += Event.sMissionID.iScriptInstance
	ELSE
		sData.iSender = -2
		sData.tlEventPrint = "Failed to get data"
	ENDIF
	
	STORE_FM_DELIVERY_EVENT(debugData, sData)
ENDPROC

PROC DEBUG_DISPLAY_PROCESS_CLEANUP_DELIVERABLE_ID(FREEMODE_DELIVERY_FREEMODE_DEBUG_DATA &debugData, INT iEvent)
	
	FM_DELIVERY_EVENT_HISTORY sData					
	SCRIPT_EVENT_DATA_FREEMODE_DELIVERY_CLEANUP_DELIVERABLE_ID Event
	
	sData.sEventName = "Cleanup deliverable"
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEvent, Event, SIZE_OF(Event))
		sData.iFC 			= GET_FRAME_COUNT()
		sData.iSender 		= NATIVE_TO_INT(Event.Details.FromPlayerIndex)
		
		sData.tlEventPrint = "Deliverable: "
		sData.tlEventPrint += Event.deliverableID.iIndex
		sData.tlEventPrint += " : "
		sData.tlEventPrint += NATIVE_TO_INT(Event.deliverableID.pOwner)
	ELSE
		sData.iSender = -2
		sData.tlEventPrint = "Failed to get data"
	ENDIF
	
	STORE_FM_DELIVERY_EVENT(debugData, sData)
ENDPROC

PROC DEBUG_DISPLAY_PROCESS_REQUEST_DROPOFF_ACTIVATION(FREEMODE_DELIVERY_FREEMODE_DEBUG_DATA &debugData, INT iEvent)
	
	FM_DELIVERY_EVENT_HISTORY sData					
	SCRIPT_EVENT_DATA_FREEMODE_DELIVERY_REQUEST_DROPOFF_ACTIVATION Event
	
	sData.sEventName = "Dropoff activation "
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEvent, Event, SIZE_OF(Event))
		sData.iFC 			= GET_FRAME_COUNT()
		sData.iSender 		= NATIVE_TO_INT(Event.Details.FromPlayerIndex)
		
		sData.tlEventPrint = "Dropoff: "
		sData.tlEventPrint += ENUM_TO_INT(Event.eDropoffID)
		
		sData.tlEventPrint += " MIS: "
		sData.tlEventPrint += Event.sMissionID.iScriptHash
		sData.tlEventPrint += " : "
		sData.tlEventPrint += Event.sMissionID.iScriptInstance
	ELSE
		sData.iSender = -2
		sData.tlEventPrint = "Failed to get data"
	ENDIF
	
	STORE_FM_DELIVERY_EVENT(debugData, sData)
ENDPROC

PROC DEBUG_DISPLAY_PROCESS_REQUEST_DROPOFF_MISSION_ID_ASSIGNMENT(FREEMODE_DELIVERY_FREEMODE_DEBUG_DATA &debugData, INT iEvent)
	
	CONST_INT LIST_LIMIT 5
	FM_DELIVERY_EVENT_HISTORY sData					
	SCRIPT_EVENT_DATA_FREEMODE_DELIVERY_REQUEST_DROPOFF_MISSION_ID_ASSIGNMENT Event
	
	sData.sEventName = "Mission ID assign  "
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEvent, Event, SIZE_OF(Event))
		sData.iFC 			= GET_FRAME_COUNT()
		sData.iSender 		= NATIVE_TO_INT(Event.Details.FromPlayerIndex)
		
		sData.tlEventPrint = "Owner: "
		sData.tlEventPrint += NATIVE_TO_INT(Event.pOwner)
		
		sData.tlEventPrint += " MIS: "
		sData.tlEventPrint += Event.sMissionID.iScriptHash
		sData.tlEventPrint += " : "
		sData.tlEventPrint += Event.sMissionID.iScriptInstance
		
		sData.tlEventPrint += " DRPS: "
		sData.tlEventPrint += Event.iDropoffCount
		
		sData.tlEventPrint += " List: "
		
		INT i
		
		REPEAT LIST_LIMIT i
			IF i = (Event.iDropoffCount - 1)
				BREAKLOOP
			ENDIF
			
			sData.tlEventPrint += ENUM_TO_INT(Event.eDropoffs[i])
			
			IF i != (LIST_LIMIT - 1)
				sData.tlEventPrint += ", "
			ENDIF
		ENDREPEAT
	ELSE
		sData.iSender = -2
		sData.tlEventPrint = "Failed to get data"
	ENDIF
	
	STORE_FM_DELIVERY_EVENT(debugData, sData)
ENDPROC

PROC DEBUG_DISPLAY_PROCESS_FM_DELIVERABLE_DELIVERY(FREEMODE_DELIVERY_FREEMODE_DEBUG_DATA &debugData, INT iEvent)
	FM_DELIVERY_EVENT_HISTORY sData					
	SCRIPT_EVENT_DATA_FM_DELIVERABLE_DELIVERY Event
	
	sData.sEventName = "Dlvrble Delivery   "
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEvent, Event, SIZE_OF(Event))
		sData.iFC 			= GET_FRAME_COUNT()
		sData.iSender 		= NATIVE_TO_INT(Event.Details.FromPlayerIndex)
		
		sData.tlEventPrint = "Deliverable: "
		sData.tlEventPrint += Event.eDeliverable.iIndex
		sData.tlEventPrint += " : "
		sData.tlEventPrint += NATIVE_TO_INT(Event.eDeliverable.pOwner)
		
		sData.tlEventPrint = " TY: "
		sData.tlEventPrint += ENUM_TO_INT(Event.eType)
		
		sData.tlEventPrint += " DO: "
		sData.tlEventPrint += ENUM_TO_INT(Event.eDropoffDeliverdTo)
		
		sData.tlEventPrint += " Q: "
		sData.tlEventPrint += Event.iQuantityRemaining
				
		sData.tlEventPrint += " S: "
		sData.tlEventPrint += BOOL_TO_INT(Event.bStolen)
		
		sData.tlEventPrint += " R: "
		sData.tlEventPrint += Event.iRivalFMMCType
		sData.tlEventPrint += " : "
		sData.tlEventPrint += Event.iRivalVariation
		
	ELSE
		sData.iSender = -2
		sData.tlEventPrint = "Failed to get data"
	ENDIF
	
	STORE_FM_DELIVERY_EVENT(debugData, sData)
ENDPROC

PROC DEBUG_DISPLAY_PROCESS_FM_MULTIPLE_DELIVERABLE_DELIVERY(FREEMODE_DELIVERY_FREEMODE_DEBUG_DATA &debugData, INT iEvent)	
	FM_DELIVERY_EVENT_HISTORY sData					
	SCRIPT_EVENT_DATA_FM_MUTLTIPLE_DELIVERABLE_DELIVERY Event
	
	sData.sEventName = "Mlt Dlvrble Delvry "
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEvent, Event, SIZE_OF(Event))
		sData.iFC 			= GET_FRAME_COUNT()
		sData.iSender 		= NATIVE_TO_INT(Event.Details.FromPlayerIndex)
		
		sData.tlEventPrint = "Dlvrabl: "
		sData.tlEventPrint += Event.eDeliverable[0].iIndex
		sData.tlEventPrint += " : "
		sData.tlEventPrint += NATIVE_TO_INT(Event.eDeliverable[0].pOwner)
		
		sData.tlEventPrint += " TY: "
		sData.tlEventPrint += ENUM_TO_INT(Event.eType[0])
		
		sData.tlEventPrint += " Q: "
		sData.tlEventPrint += Event.iQuantityRemaining[0]
	ELSE
		sData.iSender = -2
		sData.tlEventPrint = "Failed to get data"
	ENDIF
	
	STORE_FM_DELIVERY_EVENT(debugData, sData)
ENDPROC

/// PURPOSE:
///    Function for use with a freemode delivery debug display
///    Runs a network event loop, but we are leaving this here because it is debug only and only runs when a rag widget is ticked
///    widget: data.debug.bShowFMDeliverySystem
/// PARAMS:
///    debugData - data struct
PROC TRACK_RECEIVIED_FM_DELIVERY_EVENTS(FREEMODE_DELIVERY_FREEMODE_DEBUG_DATA &debugData)
	INT iCount
	EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		
		IF ThisScriptEvent = EVENT_NETWORK_SCRIPT_EVENT
	
			GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
						
			SWITCH Details.Type
				CASE SCRIPT_EVENT_FREEMODE_DELIVERY_REQUEST_DELIVERABLE_ID
					DEBUG_DISPLAY_PROCESS_REQUEST_DELIVERABLE_ID(debugData, iCount)
				BREAK
				CASE SCRIPT_EVENT_FREEMODE_DELIVERY_CLEANUP_DELIVERABLE_ID
					DEBUG_DISPLAY_PROCESS_CLEANUP_DELIVERABLE_ID(debugData, iCount)
				BREAK
				CASE SCRIPT_EVENT_FREEMODE_DELIVERY_REQUEST_DROPOFF_ACTIVATION
					 DEBUG_DISPLAY_PROCESS_REQUEST_DROPOFF_ACTIVATION(debugData, iCount)
				BREAK
				CASE SCRIPT_EVENT_FREEMODE_DELIVERY_REQUEST_DROPOFF_MISSION_ID_ASSIGNMENT
					DEBUG_DISPLAY_PROCESS_REQUEST_DROPOFF_MISSION_ID_ASSIGNMENT(debugData, iCount)
				BREAK
				CASE SCRIPT_EVENT_FM_DELIVERABLE_DELIVERY
					DEBUG_DISPLAY_PROCESS_FM_DELIVERABLE_DELIVERY(debugData, iCount)
				BREAK				
				CASE SCRIPT_EVENT_FM_MULTIPLE_DELIVERABLE_DELIVERY
					DEBUG_DISPLAY_PROCESS_FM_MULTIPLE_DELIVERABLE_DELIVERY(debugData, iCount)
				BREAK
			ENDSWITCH
		ENDIF
	ENDREPEAT
ENDPROC

ENUM MISSION_SCRIPTS
	BUSINESS_BATTLES,
	GB_GUNRUNNING,
	business_battles_sell,
	gb_casino,
	gb_casino_heist,
	gb_gangops,
	gb_gunrunning_defend,
	gb_smuggler,
	fm_content_drug_vehicle,
	fm_content_business_battles,
	fm_content_movie_props,
	fm_content_island_heist,
	fm_content_golden_gun,
	fm_content_island_dj,
	MISSION_SCRIPTS_COUNT
ENDENUM

FUNC INT GET_SCRIPT_HASH(MISSION_SCRIPTS eScript)
	SWITCH eScript
		CASE BUSINESS_BATTLES				RETURN HASH("BUSINESS_BATTLES")				BREAK
		CASE GB_GUNRUNNING					RETURN HASH("GB_GUNRUNNING")				BREAK
		CASE business_battles_sell			RETURN HASH("business_battles_sell")		BREAK
		CASE gb_casino						RETURN HASH("gb_casino")					BREAK
		CASE gb_casino_heist				RETURN HASH("gb_casino_heist")				BREAK
		CASE gb_gangops						RETURN HASH("gb_gangops")					BREAK
		CASE gb_gunrunning_defend			RETURN HASH("gb_gunrunning_defend")			BREAK
		CASE gb_smuggler					RETURN HASH("gb_smuggler")					BREAK
		CASE fm_content_drug_vehicle		RETURN HASH("fm_content_drug_vehicle")		BREAK
		CASE fm_content_business_battles	RETURN HASH("fm_content_business_battles")	BREAK
		CASE fm_content_movie_props			RETURN HASH("fm_content_movie_props")		BREAK
		CASE fm_content_island_heist		RETURN HASH("fm_content_island_heist")		BREAK
		CASE fm_content_golden_gun			RETURN HASH("fm_content_golden_gun")		BREAK
		CASE fm_content_island_dj			RETURN HASH("fm_content_island_dj")			BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

DEBUGONLY FUNC STRING GET_MISSION_TEXT_CODE(MISSION_SCRIPTS eScript)
	SWITCH eScript
		CASE BUSINESS_BATTLES				RETURN "BB"		BREAK
		CASE GB_GUNRUNNING					RETURN "GR"		BREAK
		CASE business_battles_sell			RETURN "BBS"	BREAK
		CASE gb_casino						RETURN "CAS"	BREAK
		CASE gb_casino_heist				RETURN "CH"		BREAK
		CASE gb_gangops						RETURN "GO"		BREAK
		CASE gb_gunrunning_defend			RETURN "GRD"	BREAK
		CASE gb_smuggler					RETURN "SM"		BREAK
		CASE fm_content_drug_vehicle		RETURN "DV"		BREAK
		CASE fm_content_business_battles	RETURN "FMBB"	BREAK
		CASE fm_content_movie_props			RETURN "MP"		BREAK
		CASE fm_content_island_heist		RETURN "IH"		BREAK
		CASE fm_content_golden_gun			RETURN "GG"		BREAK
		CASE fm_content_island_dj			RETURN "DJ"		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC INT GET_ACTIVE_PLAYER_COUNT(BOOL bRunningMainScriptCase)
	INT i, iCount
	
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, i), FALSE, bRunningMainScriptCase)
			iCount ++
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC

PROC DISPLAY_RUNNING_MISSION_SCRIPTS(FLOAT x, FLOAT y, FLOAT width, FLOAT height, FLOAT fColOneX, FLOAT fRowOneY)
	INT i
	BOOL bAddedMission
	TEXT_LABEL_15 tlPlayerCount = "Ply: "
	TEXT_LABEL_15 tlFrameCount = "FC: "
	TEXT_LABEL_31 tlMissions = "Missions: "
	TEXT_LABEL_15 tlPMServerState = "PM srv state: "
	
	FLOAT fColTwoX = (fColOneX + 0.1)
	FLOAT fColThreeX = (fColTwoX + 0.05)
	FLOAT fColFourX = (fColThreeX + 0.15)
	
	tlFrameCount += GET_FRAME_COUNT()
	tlPlayerCount += GET_ACTIVE_PLAYER_COUNT(TRUE)
	tlPlayerCount += "/"
	tlPlayerCount += GET_ACTIVE_PLAYER_COUNT(FALSE)
	tlPMServerState += GlobalServerBD_FM_events.sPatrick_serverVars.iServerGameState
	
	REPEAT MISSION_SCRIPTS_COUNT i
		MISSION_SCRIPTS eMission = INT_TO_ENUM(MISSION_SCRIPTS, i)
		INT iMissionHash = GET_SCRIPT_HASH(eMission)
		
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(iMissionHash) > 0
			IF bAddedMission
				tlMissions += ", "
			ENDIF
			
			tlMissions += GET_MISSION_TEXT_CODE(eMission)
			bAddedMission = TRUE
		ENDIF
	ENDREPEAT
	
	IF GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].patrickplayerBD.eClientStage != eMS_PATRICK_INIT
		IF bAddedMission
			tlMissions += ", PM"
		ELSE
			tlMissions += "PM"
		ENDIF
		
		bAddedMission = TRUE
	ENDIF
	
	IF NOT bAddedMission
		tlMissions = "No active missions"
	ENDIF
	
	//BG
	DRAW_RECT(x, y, width, height, 0, 0, 0, 200)
	DISPLAY_TEXT_WITH_LITERAL_STRING_CUSTOM(fColOneX, fRowOneY, tlFrameCount, BGT_WHITE, 0.3)
	DISPLAY_TEXT_WITH_LITERAL_STRING_CUSTOM(fColTwoX, fRowOneY, tlPlayerCount, BGT_WHITE, 0.3)
	DISPLAY_TEXT_WITH_LITERAL_STRING_CUSTOM(fColThreeX, fRowOneY, tlMissions, BGT_WHITE, 0.3)
	DISPLAY_TEXT_WITH_LITERAL_STRING_CUSTOM(fColFourX, fRowOneY, tlPMServerState, BGT_WHITE, 0.3)
ENDPROC

PROC DISPLAY_FM_DELIVERY_EVENTS(FREEMODE_DELIVERY_FREEMODE_DATA &data)
	
	MAINTAIN_FM_EVENT_LIST_TO(data.debug)
	///    Function for use with a freemode delivery debug display
	///    Runs a network event loop, but we are leaving this here because it is debug only and only runs when a rag widget is ticked
	///    data.debug.bShowFMDeliverySystem
	TRACK_RECEIVIED_FM_DELIVERY_EVENTS(data.debug)
	
	INT iEvents = GET_NUM_FM_DELIVERY_EVENTS_TO_DISPLAY(data.debug)
	
	//X	
	CONST_FLOAT fBGRectx 0.5
	CONST_FLOAT fBGWidth 0.5
	
	FLOAT fLeftMostX 	= (fBGRectx - (fBGWidth/2.0))
	FLOAT fColOneX 		= (fLeftMostX + 0.0007)
	FLOAT fColTwoX 		= (fColOneX + 0.1)
	FLOAT fColThreeX 	= (fColTwoX + 0.1)
	
	//Y
	CONST_FLOAT fRowSpacing 0.0195
	
	FLOAT fTopMostY		= 0.875 - (iEvents * fRowSpacing)	
	FLOAT fBGHeight 	= (fRowSpacing * iEvents) + fRowSpacing
	FLOAT fBGRecty 		= fTopMostY + (fBGHeight/2)
	FLOAT fRowOneY 		= (fTopMostY - 0.005)	
	FLOAT fRowTwoY 		= (fRowOneY + fRowSpacing)
	
	//BG
	DRAW_RECT(fBGRectx, fBGRecty, fBGWidth, fBGHeight, 0, 0, 0, 200)
	
	//Title
	DISPLAY_TEXT_WITH_LITERAL_STRING_CUSTOM(fLeftMostX, fRowOneY, "Events received", BGT_GREEN, 0.35)
	
	INT i, iCount
	
	REPEAT MAX_FM_DELIVERY_EVENT_STORE i
		
		INT index = (data.debug.iCurrentEvent - i)
		
		IF index < 0 
			index = (MAX_FM_DELIVERY_EVENT_STORE - (index * -1)) - 1
		ENDIF
			
		IF data.debug.fmDeliveryHistory[index].iSender != -1
			FLOAT fYPos = (iCount * fRowSpacing) + fRowTwoY
			TEXT_LABEL_31 tlBasicDetails = "Sender: "
			tlBasicDetails += data.debug.fmDeliveryHistory[index].iSender
			tlBasicDetails += "  FC: "
			tlBasicDetails += data.debug.fmDeliveryHistory[index].iFC
			
			DISPLAY_TEXT_WITH_LITERAL_STRING_CUSTOM(fColOneX, fYPos, data.debug.fmDeliveryHistory[index].sEventName, BGT_WHITE, 0.3)
			DISPLAY_TEXT_WITH_LITERAL_STRING_CUSTOM(fColTwoX, fYPos, tlBasicDetails, BGT_WHITE, 0.3)
			DISPLAY_TEXT_WITH_LITERAL_STRING_CUSTOM(fColThreeX, fYPos, data.debug.fmDeliveryHistory[index].tlEventPrint, BGT_WHITE, 0.3)
			
			iCount ++ 
		ENDIF
	ENDREPEAT
	
	//As the above display moves with incoming events we need to base the misions display on this
	CONST_FLOAT MISSIONS_DISPLAY_OFFSET 0.0225
	
	fRowOneY -= MISSIONS_DISPLAY_OFFSET
	fBGRecty = fRowOneY + 0.015
	
	DISPLAY_RUNNING_MISSION_SCRIPTS(fBGRectx, fBGRecty, fBGWidth, fRowSpacing, fColOneX, fRowOneY)
ENDPROC

PROC FREEMODE_DELIVERY_MAINTAIN_DEBUG_WIDGETS(FREEMODE_DELIVERY_FREEMODE_DATA &data)
	
	FM_DELIVERY_MISSION_ID sDebugMissionID = _GET_DEBUG_MISSION_ID()
	
	IF g_FreemodeDeliveryData.d_bShowDebugOverlay
		_FREEMODE_DELIVERY_DRAW_DEBUG_OVERLAY()
	ENDIF
	
	IF g_FreemodeDeliveryData.iDbgShowSelectDropoffFailOnScreen > 0
		TEXT_LABEL_63 tlError = "Unknown error launching the selected mission"
		SWITCH g_FreemodeDeliveryData.iDbgShowSelectDropoffFailOnScreen			
			CASE FREEMODE_DELIVERY_DEBUG_HELP_NOT_BOSS 			tlError = "You Need to be a boss to launch that." 							BREAK
			CASE FREEMODE_DELIVERY_DEBUG_HELP_ON_MISSION 		tlError = "You can't do that on a mission." 								BREAK
			CASE FREEMODE_DELIVERY_DEBUG_HELP_NO_BUNKER			tlError = "You need a bunker." 												BREAK
			CASE FREEMODE_DELIVERY_DEBUG_HELP_SERVER_SAYS_NO 	tlError = "Host was unable to process request try again in a few seconds" 	BREAK
			CASE FREEMODE_DELIVERY_DEBUG_HELP_NO_HANGAR			tlError = "You need a hangar." 												BREAK
		ENDSWITCH
		
		SET_TEXT_SCALE(0.6, 0.6)
		SET_TEXT_COLOUR(255, 0, 0, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.1, "STRING", tlError)
		
		IF NOT HAS_NET_TIMER_STARTED(data.debug.dbgSTLaunchFailHelpTimer)
			START_NET_TIMER(data.debug.dbgSTLaunchFailHelpTimer)
		ELIF HAS_NET_TIMER_EXPIRED(data.debug.dbgSTLaunchFailHelpTimer, 4000)
			RESET_NET_TIMER(data.debug.dbgSTLaunchFailHelpTimer)
			g_FreemodeDeliveryData.iDbgShowSelectDropoffFailOnScreen = 0
		ENDIF
	ENDIF
	
	IF data.debug.bGetRelativePosToFocusEntity
		ENTITY_INDEX focusEntity = GET_FOCUS_ENTITY_INDEX()
		IF DOES_ENTITY_EXIST(focusEntity)
		AND NOT IS_ENTITY_DEAD(focusEntity)
			VECTOR vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(focusEntity, GET_FINAL_RENDERED_CAM_COORD())
			VECTOR vCamRot = GET_FINAL_RENDERED_CAM_ROT()
			FLOAT fRelativeHeading = vCamRot.Z - GET_ENTITY_HEADING(focusEntity)
			IF fRelativeHeading < 0.0
				fRelativeHeading += 360.0
			ENDIF
			OPEN_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("Focus entity: ") SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(focusEntity))) SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("Relative pos to entity: ") SAVE_VECTOR_TO_DEBUG_FILE(vOffset) SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("Relative heading to entity: ") SAVE_FLOAT_TO_DEBUG_FILE(fRelativeHeading) SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			CLOSE_DEBUG_FILE()
		ENDIF
		data.debug.bGetRelativePosToFocusEntity = FALSE
	ENDIF
	
	FREEMODE_DELIVERY_DROPOFFS eCurrentlySelectedDropoffID = INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, data.debug.iCurrentSelectedDropoff)
	IF data.debug.iCurrentSelectedDropoff != data.debug.iPreviousSelectedDropoff
		IF DOES_TEXT_WIDGET_EXIST(data.debug.widgetDropoffName)
			SET_CONTENTS_OF_TEXT_WIDGET(data.debug.widgetDropoffName, FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(eCurrentlySelectedDropoffID))
		ENDIF
		data.debug.iPreviousSelectedDropoff = data.debug.iCurrentSelectedDropoff
	ENDIF
	
	IF data.debug.bDoTeleport
		IF data.debug.eDropoffToTeleport = FREEMODE_DELIVERY_DROPOFF_INVALID
			data.debug.eDropoffToTeleport = eCurrentlySelectedDropoffID
		ENDIF
		data.debug.bDoTeleport = FALSE
	ENDIF

	IF data.debug.bToggleActive
		PLAYER_INDEX pActiveForPlayer = INT_TO_PLAYERINDEX(data.debug.iDropoffActiveForPlayer)
		IF NOT FREEMODE_DELIVERY_IS_DROPOFF_ACTIVE(eCurrentlySelectedDropoffID)
			FREEMODE_DELIVERY_SET_DROPOFF_ACTIVE_ON_SERVER_FOR_PLAYER(eCurrentlySelectedDropoffID, pActiveForPlayer, sDebugMissionID, TRUE)
		ELSE
			FREEMODE_DELIVERY_SET_DROPOFF_ACTIVE_ON_SERVER_FOR_PLAYER(eCurrentlySelectedDropoffID, pActiveForPlayer, sDebugMissionID, FALSE)
		ENDIF
		
		data.debug.bToggleActive = FALSE
	ENDIF
	
	IF data.debug.bToggleDisabled
		IF NOT FREEMODE_DELIVERY_IS_DROPOFF_DISABLED(eCurrentlySelectedDropoffID)
			FREEMODE_DELIVERY_SET_DROPOFF_DISABLED(eCurrentlySelectedDropoffID, TRUE)
		ELSE
			FREEMODE_DELIVERY_SET_DROPOFF_DISABLED(eCurrentlySelectedDropoffID, FALSE)
		ENDIF
		data.debug.bToggleDisabled = FALSE
	ENDIF
		
	IF FREEMODE_DELIVERY_IS_DROPOFF_ACTIVE(eCurrentlySelectedDropoffID)
		data.debug.iIsActive = 1
	ELSE
		data.debug.iIsActive = 0
	ENDIF
	
	IF FREEMODE_DELIVERY_IS_DROPOFF_DISABLED(eCurrentlySelectedDropoffID)
		data.debug.iIsDisabled = 1
	ELSE
		data.debug.iIsDisabled = 0
	ENDIF
	
	IF data.debug.eDropoffToTeleport != FREEMODE_DELIVERY_DROPOFF_INVALID
		IF IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			IF NET_WARP_TO_COORD(g_FreemodeDeliveryData.dropoffs[ENUM_TO_INT(data.debug.eDropoffToTeleport)].vOnFootCorona, 0.0)
				DO_SCREEN_FADE_IN(500)
				data.debug.eDropoffToTeleport = FREEMODE_DELIVERY_DROPOFF_INVALID
			ENDIF
		ELSE
			DO_SCREEN_FADE_OUT(500)
		ENDIF
	ENDIF
	
	BOOL bAllDone
	INT i
	
	IF data.debug.bCreateDeliverableID
		bAllDone = TRUE
		REPEAT COUNT_OF(data.debug.iCreateDeliverableIDIndices) i
			IF data.debug.iCreateDeliverableIDIndices[i] > -1
				IF NOT FREEMODE_DELIVERY_CREATE_DELIVERABLE(
					data.debug.createdDeliverableID[i], 
					INT_TO_PLAYERINDEX(data.debug.iCreateDeliverableIDPlayerIDs[i]), 
					data.debug.iCreateDeliverableIDIndices[i], 
					INT_TO_ENUM(FREEMODE_DELIVERABLE_TYPE,
					data.debug.iCreateDeliverableType[i]),
					sDebugMissionID,
					data.debug.iCreateDeliverableQty[i]
				)
					bAllDone = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF bAllDone
			data.debug.bCreateDeliverableID = FALSE
		ENDIF
	ENDIF
	
	IF data.debug.bCleanupDeliverables
		bAllDone = TRUE
		REPEAT COUNT_OF(data.debug.createdDeliverableID) i
			IF FREEMODE_DELIVERY_IS_DELIVERABLE_VALID(data.debug.createdDeliverableID[i])
				IF NOT FREEMODE_DELIVERY_CLEANUP_DELIVERABLE(data.debug.createdDeliverableID[i])
					bAllDone = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF bAllDone
			data.debug.bCleanupDeliverables = FALSE
		ENDIF
	ENDIF
	
	IF data.debug.bSetPlayerInPossessionOfDeliverable
		IF FREEMODE_DELIVERY_IS_DELIVERABLE_VALID(data.debug.createdDeliverableID[data.debug.iDeliverableToSetInPossession])
			FREEMODE_DELIVERY_SET_LOCAL_PLAYER_IN_POSSESSION_OF_DELIVERABLE(data.debug.createdDeliverableID[data.debug.iDeliverableToSetInPossession])
		ENDIF
		data.debug.bSetPlayerInPossessionOfDeliverable = FALSE
	ENDIF
	
	IF data.debug.bClearPlayerPossession
		FREEMODE_DELIVERY_CLEAR_LOCAL_PLAYER_DELIVERABLE_POSSESSION()
		data.debug.bClearPlayerPossession = FALSE
	ENDIF
	
	IF data.debug.bPrintServerState
		FREEMODE_DELIVERY_DEBUG_PRINT_SERVER_STATE()
		data.debug.bPrintServerState = FALSE
	ENDIF
	
	IF data.debug.bShowReservedDropoffOverlay
		SHOW_RESERVED_DROPOFF_DATA()
	ENDIF
	
	IF data.debug.bShowServerState
		DEBUG_SHOW_SERVER_DROPOFF_STATE()
	ENDIF
	
	IF data.debug.bShowFMDeliverySystem
		SHOW_RESERVED_DROPOFF_DATA()
		SHOW_DELIVERABLE_STATE()
		DISPLAY_FM_DELIVERY_EVENTS(data)
	ENDIF
	
	//Move Weapons
	IF data.debug.bAssignDebugDropoff[0]
		INT iMWLoop
		
		IF data.debug.iDropoffSlectDebugCombo[0] = data.debug.iDropoffSlectDebugCombo[1]
		OR data.debug.iDropoffSlectDebugCombo[2] = data.debug.iDropoffSlectDebugCombo[1]
		OR data.debug.iDropoffSlectDebugCombo[2] = data.debug.iDropoffSlectDebugCombo[0]
			ASSERTLN("FREEMODE_DELIVERY_MAINTAIN_DEBUG_WIDGETS - Selected move weapons dropoffs invalid! Select 3 different dropoffs")
			data.debug.bAssignDebugDropoff[0] = FALSE
			EXIT
		ENDIF
		
		FREEMODE_DELIVERY_DROPOFFS eSelectedDropoffs[3]
		FREEMODE_DELIVERY_ROUTES eRoute
		DEBUG_INIT_DROPOFF_REQUEST_DATA(eSelectedDropoffs, eRoute)
		
		REPEAT 3 iMWLoop
			INT iDropoff = ENUM_TO_INT(GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_1)
			iDropoff += data.debug.iDropoffSlectDebugCombo[iMWLoop]
			
			eSelectedDropoffs[iMWLoop] = INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, iDropoff)
			
			PRINTLN("[FREEMODE_DELIVERY][FM_DO_RES] Debug selected dropoff: ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(eSelectedDropoffs[iMWLoop]))
		ENDREPEAT
		
		DEBUG_BROADCAST_REQUEST_MISSION_LAUNCH_WITH_DROPOFF(eSelectedDropoffs,eRoute)
		
		data.debug.bAssignDebugDropoff[0] = FALSE
	ENDIF
	
	//Follow the Leader
	IF data.debug.bAssignDebugDropoff[1]
		INT iDropoff = ENUM_TO_INT(GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_1)
		iDropoff += data.debug.iDropoffSlectDebugCombo[3]
		
		FREEMODE_DELIVERY_DROPOFFS eSelectedDropoffs[3]
		FREEMODE_DELIVERY_ROUTES eRoute
		DEBUG_INIT_DROPOFF_REQUEST_DATA(eSelectedDropoffs, eRoute)
		
		eSelectedDropoffs[0] = INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, iDropoff)
		
		DEBUG_BROADCAST_REQUEST_MISSION_LAUNCH_WITH_DROPOFF(eSelectedDropoffs,eRoute)
		
		PRINTLN("[FREEMODE_DELIVERY][FM_DO_RES] Debug selected dropoff: ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(g_FreemodeDeliveryData.eDbgSelectedDropoff[0]))
		data.debug.bAssignDebugDropoff[1] = FALSE
	ENDIF
	
	IF (GET_FRAME_COUNT() % 10) = 0
		data.debug.bFTLRouteHasProps = (GUNRUN_GET_DROPOFF_COUNT_OF_PROPS_REQUIRED((GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_1 + ( INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, data.debug.iDropoffSlectDebugCombo[3])))) > 0)
	ENDIF
	
	//Ambushed
	IF data.debug.bAssignDebugDropoff[2]
		INT iRoute = ENUM_TO_INT(FREEMODE_DELIVERY_ROUTE_AMBUSHED_CITY_1)
		iRoute += data.debug.iDropoffSlectDebugCombo[4]
				
		FREEMODE_DELIVERY_ROUTES eRoute		
		FREEMODE_DELIVERY_DROPOFFS eSelectedDropoffs[3]
		DEBUG_INIT_DROPOFF_REQUEST_DATA(eSelectedDropoffs, eRoute)
		
		eRoute = INT_TO_ENUM(FREEMODE_DELIVERY_ROUTES, iRoute)
		
		DEBUG_BROADCAST_REQUEST_MISSION_LAUNCH_WITH_DROPOFF(eSelectedDropoffs,eRoute)
		
		PRINTLN("[FREEMODE_DELIVERY][FM_DO_RES] Debug selected dropoff route: ", FREEMODE_DELIVERY_GET_ROUTE_DEBUG_NAME(eRoute))
		data.debug.bAssignDebugDropoff[2] = FALSE
	ENDIF
	
	//Hill Climb
	IF data.debug.bAssignDebugDropoff[3]
		INT iRoute = ENUM_TO_INT(FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_CITY_1)
		iRoute += data.debug.iDropoffSlectDebugCombo[5]		
		
		FREEMODE_DELIVERY_ROUTES eRoute		
		FREEMODE_DELIVERY_DROPOFFS eSelectedDropoffs[3]
		DEBUG_INIT_DROPOFF_REQUEST_DATA(eSelectedDropoffs, eRoute)
		
		eRoute = INT_TO_ENUM(FREEMODE_DELIVERY_ROUTES, iRoute)
		
		DEBUG_BROADCAST_REQUEST_MISSION_LAUNCH_WITH_DROPOFF(eSelectedDropoffs,eRoute)
		
		PRINTLN("[FREEMODE_DELIVERY][FM_DO_RES] Debug selected dropoff route: ", FREEMODE_DELIVERY_GET_ROUTE_DEBUG_NAME(eRoute))
		data.debug.bAssignDebugDropoff[3] = FALSE
	ENDIF
	
	//Rough Terrain
	IF data.debug.bAssignDebugDropoff[4]
		INT iRoute = ENUM_TO_INT(FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_CITY_1)
		iRoute += data.debug.iDropoffSlectDebugCombo[6]
		
		FREEMODE_DELIVERY_ROUTES eRoute		
		FREEMODE_DELIVERY_DROPOFFS eSelectedDropoffs[3]
		DEBUG_INIT_DROPOFF_REQUEST_DATA(eSelectedDropoffs, eRoute)
		
		eRoute = INT_TO_ENUM(FREEMODE_DELIVERY_ROUTES, iRoute)
		
		DEBUG_BROADCAST_REQUEST_MISSION_LAUNCH_WITH_DROPOFF(eSelectedDropoffs,eRoute)
		
		PRINTLN("[FREEMODE_DELIVERY][FM_DO_RES] Debug selected dropoff route: ", FREEMODE_DELIVERY_GET_ROUTE_DEBUG_NAME(eRoute))
		data.debug.bAssignDebugDropoff[4] = FALSE
	ENDIF
	
	//Phantom
	IF data.debug.bAssignDebugDropoff[5]
		INT iDropoff = ENUM_TO_INT(GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_1)
		iDropoff += data.debug.iDropoffSlectDebugCombo[7]
		
		FREEMODE_DELIVERY_DROPOFFS eSelectedDropoffs[3]
		FREEMODE_DELIVERY_ROUTES eRoute
		DEBUG_INIT_DROPOFF_REQUEST_DATA(eSelectedDropoffs, eRoute)
		
		eSelectedDropoffs[0] = INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, iDropoff)
		
		DEBUG_BROADCAST_REQUEST_MISSION_LAUNCH_WITH_DROPOFF(eSelectedDropoffs,eRoute)
		
		PRINTLN("[FREEMODE_DELIVERY][FM_DO_RES] Debug selected dropoff: ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(g_FreemodeDeliveryData.eDbgSelectedDropoff[0]))
		data.debug.bAssignDebugDropoff[5] = FALSE
	ENDIF
	
	/////////////////////////////
	///     Smuggler dropoffs
	
	//Heavy Lifting
	IF data.debug.bAssignDebugDropoff[6]
		INT iDropoff = ENUM_TO_INT(SMUGGLER_DROPOFF_HEAVY_LIFTING_1)
		iDropoff += data.debug.iDropoffSlectDebugCombo[8]
		
		FREEMODE_DELIVERY_DROPOFFS eSelectedDropoffs[3]
		FREEMODE_DELIVERY_ROUTES eRoute = FREEMODE_DELIVERY_ROUTE_NO_ROUTE
		DEBUG_INIT_DROPOFF_REQUEST_DATA(eSelectedDropoffs, eRoute)
		
		eSelectedDropoffs[0] = INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, iDropoff)
		
		DEBUG_BROADCAST_REQUEST_MISSION_LAUNCH_WITH_DROPOFF(eSelectedDropoffs,eRoute)
		
		PRINTLN("[FREEMODE_DELIVERY][FM_DO_RES] Debug selected dropoff: ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(g_FreemodeDeliveryData.eDbgSelectedDropoff[0]))
		data.debug.bAssignDebugDropoff[6] = FALSE
	ENDIF
	
	//Contested
	IF data.debug.bAssignDebugDropoff[7]
		INT iDropoff = ENUM_TO_INT(SMUGGLER_DROPOFF_CONTESTED_1)
		iDropoff += data.debug.iDropoffSlectDebugCombo[9]
		
		FREEMODE_DELIVERY_DROPOFFS eSelectedDropoffs[3]
		FREEMODE_DELIVERY_ROUTES eRoute = FREEMODE_DELIVERY_ROUTE_NO_ROUTE
		DEBUG_INIT_DROPOFF_REQUEST_DATA(eSelectedDropoffs, eRoute)
		
		eSelectedDropoffs[0] = INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, iDropoff)
		
		DEBUG_BROADCAST_REQUEST_MISSION_LAUNCH_WITH_DROPOFF(eSelectedDropoffs,eRoute)
		
		PRINTLN("[FREEMODE_DELIVERY][FM_DO_RES] Debug selected dropoff: ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(g_FreemodeDeliveryData.eDbgSelectedDropoff[0]))
		data.debug.bAssignDebugDropoff[7] = FALSE
	ENDIF
	
	//Agile Delivery
	IF data.debug.bAssignDebugDropoff[8]
		INT iDropoff = ENUM_TO_INT(SMUGGLER_DROPOFF_AGILE_DELIVERY_1)
		iDropoff += data.debug.iDropoffSlectDebugCombo[10]
		
		FREEMODE_DELIVERY_DROPOFFS eSelectedDropoffs[3]
		FREEMODE_DELIVERY_ROUTES eRoute = FREEMODE_DELIVERY_ROUTE_NO_ROUTE
		DEBUG_INIT_DROPOFF_REQUEST_DATA(eSelectedDropoffs, eRoute)
		
		eSelectedDropoffs[0] = INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, iDropoff)
		
		DEBUG_BROADCAST_REQUEST_MISSION_LAUNCH_WITH_DROPOFF(eSelectedDropoffs,eRoute)
		
		PRINTLN("[FREEMODE_DELIVERY][FM_DO_RES] Debug selected dropoff: ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(g_FreemodeDeliveryData.eDbgSelectedDropoff[0]))
		data.debug.bAssignDebugDropoff[8] = FALSE
	ENDIF
	
	//Precision Delivery
	IF data.debug.bAssignDebugDropoff[9]
		INT iDropoff = ENUM_TO_INT(SMUGGLER_DROPOFF_PRECISION_DELIVERY_1)
		iDropoff += data.debug.iDropoffSlectDebugCombo[11]
		
		FREEMODE_DELIVERY_DROPOFFS eSelectedDropoffs[3]
		FREEMODE_DELIVERY_ROUTES eRoute = FREEMODE_DELIVERY_ROUTE_NO_ROUTE
		DEBUG_INIT_DROPOFF_REQUEST_DATA(eSelectedDropoffs, eRoute)
		
		eSelectedDropoffs[0] = INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, iDropoff)
		
		DEBUG_BROADCAST_REQUEST_MISSION_LAUNCH_WITH_DROPOFF(eSelectedDropoffs,eRoute)
		
		PRINTLN("[FREEMODE_DELIVERY][FM_DO_RES] Debug selected dropoff: ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(g_FreemodeDeliveryData.eDbgSelectedDropoff[0]))
		data.debug.bAssignDebugDropoff[9] = FALSE
	ENDIF
	
	//Flying Fortress
	IF data.debug.bAssignDebugDropoff[10]
		INT iRoute = ENUM_TO_INT(FREEMODE_DELIVERY_FLYING_FORTRESS_1)
		iRoute += data.debug.iDropoffSlectDebugCombo[12]		
		
		FREEMODE_DELIVERY_ROUTES eRoute		
		FREEMODE_DELIVERY_DROPOFFS eSelectedDropoffs[3]
		DEBUG_INIT_DROPOFF_REQUEST_DATA(eSelectedDropoffs, eRoute)
		
		eSelectedDropoffs[0] = FREEMODE_DELIVERY_DROPOFF_INVALID
		eRoute = INT_TO_ENUM(FREEMODE_DELIVERY_ROUTES, iRoute)
		
		DEBUG_BROADCAST_REQUEST_MISSION_LAUNCH_WITH_DROPOFF(eSelectedDropoffs,eRoute)
		
		PRINTLN("[FREEMODE_DELIVERY][FM_DO_RES] Debug selected dropoff route: ", FREEMODE_DELIVERY_GET_ROUTE_DEBUG_NAME(eRoute))
		data.debug.bAssignDebugDropoff[10] = FALSE
	ENDIF
	
	//Fly Low
	IF data.debug.bAssignDebugDropoff[11]
		INT iDropoff = ENUM_TO_INT(SMUGGLER_DROPOFF_FLY_LOW_1)
		iDropoff += data.debug.iDropoffSlectDebugCombo[13]
		
		FREEMODE_DELIVERY_DROPOFFS eSelectedDropoffs[3]
		FREEMODE_DELIVERY_ROUTES eRoute = FREEMODE_DELIVERY_ROUTE_NO_ROUTE
		DEBUG_INIT_DROPOFF_REQUEST_DATA(eSelectedDropoffs, eRoute)
		
		eSelectedDropoffs[0] = INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, iDropoff)
		
		DEBUG_BROADCAST_REQUEST_MISSION_LAUNCH_WITH_DROPOFF(eSelectedDropoffs,eRoute)
		
		PRINTLN("[FREEMODE_DELIVERY][FM_DO_RES] Debug selected dropoff: ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(g_FreemodeDeliveryData.eDbgSelectedDropoff[0]))
		data.debug.bAssignDebugDropoff[11] = FALSE
	ENDIF	
	
	//Air Delivery
	IF data.debug.bAssignDebugDropoff[12]
		INT iDropoff = ENUM_TO_INT(SMUGGLER_DROPOFF_AIR_DELIVERY_1)
		iDropoff += data.debug.iDropoffSlectDebugCombo[14]
		
		FREEMODE_DELIVERY_DROPOFFS eSelectedDropoffs[3]
		FREEMODE_DELIVERY_ROUTES eRoute = FREEMODE_DELIVERY_ROUTE_NO_ROUTE
		DEBUG_INIT_DROPOFF_REQUEST_DATA(eSelectedDropoffs, eRoute)
		
		eSelectedDropoffs[0] = INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, iDropoff)
		
		DEBUG_BROADCAST_REQUEST_MISSION_LAUNCH_WITH_DROPOFF(eSelectedDropoffs,eRoute)
		
		PRINTLN("[FREEMODE_DELIVERY][FM_DO_RES] Debug selected dropoff: ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(g_FreemodeDeliveryData.eDbgSelectedDropoff[0]))
		data.debug.bAssignDebugDropoff[12] = FALSE
	ENDIF
	
	//Air Police
	IF data.debug.bAssignDebugDropoff[13]
		INT iDropoff = ENUM_TO_INT(SMUGGLER_DROPOFF_AIR_POLICE_1)
		iDropoff += data.debug.iDropoffSlectDebugCombo[15]
		
		FREEMODE_DELIVERY_DROPOFFS eSelectedDropoffs[3]
		FREEMODE_DELIVERY_ROUTES eRoute = FREEMODE_DELIVERY_ROUTE_NO_ROUTE
		DEBUG_INIT_DROPOFF_REQUEST_DATA(eSelectedDropoffs, eRoute)
		
		eSelectedDropoffs[0] = INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, iDropoff)
		
		DEBUG_BROADCAST_REQUEST_MISSION_LAUNCH_WITH_DROPOFF(eSelectedDropoffs,eRoute)
		
		PRINTLN("[FREEMODE_DELIVERY][FM_DO_RES] Debug selected dropoff: ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(g_FreemodeDeliveryData.eDbgSelectedDropoff[0]))
		data.debug.bAssignDebugDropoff[13] = FALSE
	ENDIF
	
	
	//Under the Radar
	IF data.debug.bAssignDebugDropoff[14]
		INT iDropoff = ENUM_TO_INT(SMUGGLER_DROPOFF_UNDER_RADAR_1)
		iDropoff += data.debug.iDropoffSlectDebugCombo[16]
		
		FREEMODE_DELIVERY_DROPOFFS eSelectedDropoffs[3]
		FREEMODE_DELIVERY_ROUTES eRoute = FREEMODE_DELIVERY_ROUTE_NO_ROUTE
		DEBUG_INIT_DROPOFF_REQUEST_DATA(eSelectedDropoffs, eRoute)
		
		eSelectedDropoffs[0] = INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, iDropoff)
		
		DEBUG_BROADCAST_REQUEST_MISSION_LAUNCH_WITH_DROPOFF(eSelectedDropoffs,eRoute)
		
		PRINTLN("[FREEMODE_DELIVERY][FM_DO_RES] Debug selected dropoff: ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(g_FreemodeDeliveryData.eDbgSelectedDropoff[0]))
		data.debug.bAssignDebugDropoff[14] = FALSE
	ENDIF
	
	IF data.debug.bClearDBGSelDropoffs
		g_FreemodeDeliveryData.eDbgSelectedDropoff[0]	= FREEMODE_DELIVERY_DROPOFF_INVALID
		g_FreemodeDeliveryData.eDbgSelectedDropoff[1]	= FREEMODE_DELIVERY_DROPOFF_INVALID
		g_FreemodeDeliveryData.eDbgSelectedDropoff[2]	= FREEMODE_DELIVERY_DROPOFF_INVALID
		g_FreemodeDeliveryData.eDbgSelectedRoute		= FREEMODE_DELIVERY_ROUTE_NO_ROUTE
		data.debug.bClearDBGSelDropoffs					= FALSE
	ENDIF
	
	FREEMODE_DELIVERY_ACTIVE_DROPOFF_PROPERTIES sData
	PLAYER_INDEX piPlayer = INT_TO_PLAYERINDEX(data.debug.iPlayerToReserveFor)
	
	IF NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT()) != PLAYER_ID()
	OR NOT IS_NET_PLAYER_OK(piPlayer, FALSE, TRUE)
		EXIT
	ENDIF
	
	IF data.debug.bTestReserveBunkerDropOff
		GET_DROP_OFF_FOR_GUNRUN_VARIATION(piPlayer, GET_OWNED_BUNKER(piPlayer), GRV_ALTRUISTS, sData, FREEMODE_DELIVERY_LOCATION_CITY)
		RESERVE_DROPOFFS_ON_SERVER(sData.eDropoffList, piPlayer, TRUE)
		data.debug.bTestReserveBunkerDropOff = FALSE
	ENDIF
	
	IF data.debug.bTestReserveSellMissionDropOff
		
		GUNRUN_VARIATION eVariation = GRV_INVALID
		SMUGGLER_VARIATION eSmugVariation = SMV_INVALID
		
		IF data.debug.iReserveForMission = 0
			eVariation = GRV_ALTRUISTS
		ELIF data.debug.iReserveForMission = 1
			eVariation = GRV_SELL_HILL_CLIMB
		ELIF data.debug.iReserveForMission = 2
			eVariation = GRV_SELL_ROUGH_TERRAIN
		ELIF data.debug.iReserveForMission = 3
			eVariation = GRV_SELL_PHANTOM
		ELIF data.debug.iReserveForMission = 4
			eVariation = GRV_SELL_AMBUSHED
		ELIF data.debug.iReserveForMission = 5
			eVariation = GRV_SELL_FOLLOW_THE_LEADER
		ELIF data.debug.iReserveForMission = 6
			eVariation = GRV_SELL_MOVE_WEAPONS
			
		ELIF data.debug.iReserveForMission = 7
			eSmugVariation = SMV_SELL_HEAVY_LIFTING
		ELIF data.debug.iReserveForMission = 8
			eSmugVariation = SMV_SELL_CONTESTED
		ELIF data.debug.iReserveForMission = 9
			eSmugVariation = SMV_SELL_AGILE_DELIVERY
		ELIF data.debug.iReserveForMission = 10
			eSmugVariation = SMV_SELL_PRECISION_DELIVERY
		ELIF data.debug.iReserveForMission = 11
			eSmugVariation = SMV_SELL_FLYING_FORTRESS
		ELIF data.debug.iReserveForMission = 12
			eSmugVariation = SMV_SELL_FLY_LOW
		ELIF data.debug.iReserveForMission = 13
			eSmugVariation = SMV_SELL_AGILE_DELIVERY
		ELIF data.debug.iReserveForMission = 14
			eSmugVariation = SMV_SELL_AIR_POLICE
		ENDIF
		
		PRINTLN("[FM_DO_RES] FREEMODE_DELIVERY_MAINTAIN_DEBUG_WIDGETS Attempting to reserve dropoffs for player: ", GET_PLAYER_NAME(piPlayer), " with variation: ", data.debug.iReserveForMission)
		IF eVariation != GRV_INVALID
			GET_DROP_OFF_FOR_GUNRUN_VARIATION(piPlayer, GET_OWNED_BUNKER(piPlayer), eVariation, sData, FREEMODE_DELIVERY_LOCATION_COUNTRYSIDE)
		ELIF eSmugVariation != SMV_INVALID
			GET_DROP_OFF_FOR_SMUGGLER_VARIATION(piPlayer, GET_LOCAL_PLAYERS_OWNED_HANGAR(), eSmugVariation, sData)
		ENDIF
		
		RESERVE_DROPOFFS_ON_SERVER(sData.eDropoffList, piPlayer, TRUE)
		data.debug.bTestReserveSellMissionDropOff = FALSE
	ENDIF
	
	IF data.debug.bClearAllReservedDropoffs
		PRINTLN("FREEMODE_DELIVERY_MAINTAIN_DEBUG_WIDGETS Attempting to clear all dropoffs")
		DEBUG_CLEAR_ALL_RESERVED_DROPOFFS()
		data.debug.bClearAllReservedDropoffs = FALSE
	ENDIF
	
	IF data.debug.bClearPlayersReservedDropoffs
		PRINTLN("FREEMODE_DELIVERY_MAINTAIN_DEBUG_WIDGETS Attempting to clear dropoffs for player: ", GET_PLAYER_NAME(piPlayer), " with variation: ", data.debug.iReserveForMission)
		CLEAR_SERVER_RESERVED_DROPOFFS_FOR_PLAYER(piPlayer)
		data.debug.bClearPlayersReservedDropoffs = FALSE
	ENDIF
	
	IF data.debug.bRequestActivate
		FREEMODE_DELIVERY_SET_DROPOFF_ACTIVE(eCurrentlySelectedDropoffID, sDebugMissionID)
		data.debug.bRequestActivate = FALSE
	ENDIF
	
	IF data.debug.bRequestDeactivate
		FREEMODE_DELIVERY_DEACTIVATE_ALL_DROPOFFS()
		data.debug.bRequestDeactivate = FALSE
	ENDIF
ENDPROC
#ENDIF

/// PURPOSE:
///    Called at start of freemode.
/// PARAMS:
///    data - 
PROC FREEMODE_DELIVERY_INITIALISE(FREEMODE_DELIVERY_FREEMODE_DATA &data)
	UNUSED_PARAMETER(data)
	
	IF NETWORK_IS_SESSION_ACTIVE()
	AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
		_FREEMODE_DELIVERY_INITIALISE_GLOBAL_SERVER_DATA()
	ENDIF
	
	IF NOT IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.iBS, BS_FREEMODE_DELIVERY_GLOBAL_PLAYER_BD_INITIALISED)
		INT i
		REPEAT FREEMODE_DELIVERY_MAX_ACTIVE_DROPOFFS i
			g_FreemodeDeliveryData.eDisabledDropoffs[i] = FREEMODE_DELIVERY_DROPOFF_INVALID
		ENDREPEAT
		
		_FREEMODE_DELIVERY_INIT_DELAYED_CLEANUP_TIMER_DATA()
		PRINTLN("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_INITIALISE - Initialised global player BD.")
	ENDIF
ENDPROC

/// PURPOSE:
///    Called every frame in freemode.
/// PARAMS:
///    data - 
PROC FREEMODE_DELIVERY_MAINTAIN(FREEMODE_DELIVERY_FREEMODE_DATA &data)
	
	IF MPGlobals.sFreemodeCache.bNetworkIsActivitySession
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(data.iBS, BS_FREEMODE_DELIVERY_FREEMODE_DATA_DATA_CACHED)
		_FREEMODE_DELIVERY_INITIALISE_DROPOFF_DATA()
		SET_BIT(data.iBS, BS_FREEMODE_DELIVERY_FREEMODE_DATA_DATA_CACHED)
		PRINTLN("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_INITIALISE - Dropoff data cached.")
	ENDIF
	
	_FREEMODE_DELIVERY_MAINTAIN_ACTIVE_DROPOFF(data)
	_FREEMODE_DELIVERY_MAINTAIN_DELIVERABLE_CLEANUP()
	_FREEMODE_DELIVERY_MAINTAIN_DELAYED_DELIVERY_TIMER()	
	_FREEMODE_DELIVERY_MAINTAIN_DELAYED_CLEANUP_TIMER()	
	FREEMODE_DELIVERY_MAINTAIN_BLOCKING_ZONES(data)
	
	#IF IS_DEBUG_BUILD
	FREEMODE_DELIVERY_MAINTAIN_DEBUG_WIDGETS(data)
	#ENDIF
	
	IF NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT()) = PLAYER_ID()
		_FREEMODE_DELIVERY_MAINTAIN_SERVER(data)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.iBS, BS_FREEMODE_DELIVERY_GLOBAL_PLAYER_BD_HAS_DELIVERABLE)
			IF (GET_FRAME_COUNT() % 120) = 0
			OR NOT IS_BIT_SET(g_FreemodeDeliveryData.iBS, BS_FREEMODE_DELIVERY_GLOBAL_DATA_HAD_DELIVERABLE_LAST_FRAME)
				IF NOT IS_BIT_SET(g_FreemodeDeliveryData.iBS, BS_FREEMODE_DELIVERY_GLOBAL_DATA_HAD_DELIVERABLE_LAST_FRAME)
					PRINTLN("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_MAINTAIN - Local player has a deliverable on them - first frame.")
				ELSE
					PRINTLN("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_MAINTAIN - Local player has a deliverable on them.")
				ENDIF
				
				INT i
				
				REPEAT FREEMODE_DELIVERY_MAX_HELD_DELIVERABLES i					
					FREEMODE_DELIVERY_DEBUG_PRINT_DELIVERABLE_ID(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.currentDeliverable[i], "FREEMODE_DELIVERY_MAINTAIN")
				ENDREPEAT
			ENDIF
		ENDIF
		
		IF g_MPF9ScreenIsRendering
			IF NOT IS_BIT_SET(data.debug.iBS, BS_FREEMODE_DELIVERY_FREEMODE_DEBUG_DATA_F9_WAS_ON)
				PRINTLN("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_MAINTAIN - F9 is on, printing server state.")
				FREEMODE_DELIVERY_DEBUG_PRINT_SERVER_STATE()
				SET_BIT(data.debug.iBS, BS_FREEMODE_DELIVERY_FREEMODE_DEBUG_DATA_F9_WAS_ON)
			ENDIF
		ELSE
			IF IS_BIT_SET(data.debug.iBS, BS_FREEMODE_DELIVERY_FREEMODE_DEBUG_DATA_F9_WAS_ON)
				CLEAR_BIT(data.debug.iBS, BS_FREEMODE_DELIVERY_FREEMODE_DEBUG_DATA_F9_WAS_ON)
			ENDIF
		ENDIF
	#ENDIF
	
	IF IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.iBS, BS_FREEMODE_DELIVERY_GLOBAL_PLAYER_BD_HAS_DELIVERABLE)
		IF NOT IS_BIT_SET(g_FreemodeDeliveryData.iBS, BS_FREEMODE_DELIVERY_GLOBAL_DATA_HAD_DELIVERABLE_LAST_FRAME)
			SET_BIT(g_FreemodeDeliveryData.iBS, BS_FREEMODE_DELIVERY_GLOBAL_DATA_HAD_DELIVERABLE_LAST_FRAME)
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FreemodeDeliveryData.iBS, BS_FREEMODE_DELIVERY_GLOBAL_DATA_HAD_DELIVERABLE_LAST_FRAME)
			CLEAR_BIT(g_FreemodeDeliveryData.iBS, BS_FREEMODE_DELIVERY_GLOBAL_DATA_HAD_DELIVERABLE_LAST_FRAME)
		ENDIF
	ENDIF
	
	data.eCurrentStaggerDropoff = INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, (ENUM_TO_INT(data.eCurrentStaggerDropoff) + 1) % ENUM_TO_INT(FREEMODE_DELIVERY_DROPOFF_COUNT))
	data.iPlayerStagger = (data.iPlayerStagger + 1) % NUM_NETWORK_PLAYERS
ENDPROC

FUNC FREEMODE_DELIVERY_DROPOFF_PROPS _FREEMODE_DELIVERY_GET_DROPOFF_EXTRA_DATA(FREEMODE_DELIVERY_DROPOFFS eDropoffID)
	FREEMODE_DELIVERY_DROPOFF_PROPS sData
	
	IF eDropoffID != FREEMODE_DELIVERY_DROPOFF_INVALID
		SWITCH _FREEMODE_DELIVERY_GET_DROPOFF_CATEGORY(eDropoffID)
			CASE FREEMODE_DELIVERY_DROPOFF_CATEGORY_GUNRUNNING
				sData = GUNRUN_GET_DROPOFF_EXTRA_DATA(eDropoffID)
			BREAK
			CASE FREEMODE_DELIVERY_DROPOFF_CATEGORY_SMUGGLER
			CASE FREEMODE_DELIVERY_DROPOFF_CATEGORY_FM_GANGOPS
				PRINTLN("[FREEMODE_DELIVERY] _FREEMODE_DELIVERY_GET_DROPOFF_EXTRA_DATA - No extra data to retrieve.")
			BREAK
			DEFAULT
				ASSERTLN("_FREEMODE_DELIVERY_GET_DROPOFF_EXTRA_DATA - No category assigned to dropoff.")
				PRINTLN("[FREEMODE_DELIVERY][GUNRUNNING] _FREEMODE_DELIVERY_GET_DROPOFF_EXTRA_DATA - No category assigned to dropoff.")
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN sData
ENDFUNC

PROC FREEMODE_DELIVERY_REQUEST_DELIVERABLE_CLEANUP(FREEMODE_DELIVERABLE_ID eDeliverable)
	INT i
	
	REPEAT FREEMODE_DELIVERY_MAX_DELIVERABLES_PER_PLAYER i
		IF g_FreemodeDeliveryData.eDeliverablesToCleanup[i].iIndex = -1
			g_FreemodeDeliveryData.eDeliverablesToCleanup[i] = eDeliverable
			PRINTLN("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_REQUEST_DELIVERABLE_CLEANUP - added deliverable with ID ", eDeliverable.iIndex, " to be cleaned up")
			#IF USE_FINAL_PRINTS PRINTLN_FINAL("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_REQUEST_DELIVERABLE_CLEANUP - added deliverable with ID ", eDeliverable.iIndex, " to be cleaned up")#ENDIF
			SET_BIT(g_FreemodeDeliveryData.iBS, BS_FREEMODE_DELIVERY_GLOBAL_DATA_DELIVERABLE_CLEANUP_ACTIVE)			
			EXIT
		ENDIF
	ENDREPEAT
	
	IF NOT IS_BIT_SET(g_FreemodeDeliveryData.iBS, BS_FREEMODE_DELIVERY_GLOBAL_DATA_DELIVERABLE_CLEANUP_ACTIVE)
		SET_BIT(g_FreemodeDeliveryData.iBS, BS_FREEMODE_DELIVERY_GLOBAL_DATA_DELIVERABLE_CLEANUP_ACTIVE)
	ENDIF
	
	ASSERTLN("FREEMODE_DELIVERY_REQUEST_DELIVERABLE_CLEANUP - No spaces left. array size needs to be increased")
	PRINTLN("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_REQUEST_DELIVERABLE_CLEANUP - No spaces left. array size needs to be increased")
ENDPROC

FUNC eSMUGGLED_GOODS_TYPE GET_SMUGGLED_GOODS_TYPE_FROM_DELIVERABLE_TYPE(FREEMODE_DELIVERABLE_TYPE eType)
	SWITCH eType
		CASE SMUGGLER_DELIVERABLE_ANIMAL_MATERIALS			RETURN eSGT_ANIMAL_MATERIALS
		CASE SMUGGLER_DELIVERABLE_ART_AND_ANTIQUES			RETURN eSGT_ART_AND_ANTIQUES
		CASE SMUGGLER_DELIVERABLE_CHEMICALS					RETURN eSGT_CHEMICALS
		CASE SMUGGLER_DELIVERABLE_COUNTERFEIT_GOODS			RETURN eSGT_COUNTERFEIT_GOODS
		CASE SMUGGLER_DELIVERABLE_JEWELRY_AND_GEMSTONES		RETURN eSGT_JEWELRY_AND_GEMSTONES
		CASE SMUGGLER_DELIVERABLE_MEDICAL_SUPPLIES			RETURN eSGT_MEDICAL_SUPPLIES
		CASE SMUGGLER_DELIVERABLE_NARCOTICS					RETURN eSGT_NARCOTICS
		CASE SMUGGLER_DELIVERABLE_TOBACCO_AND_ALCOHOL		RETURN eSGT_TOBACCO_AND_ALCOHOL
	ENDSWITCH
	
	RETURN eSGT_INVALID
ENDFUNC

FUNC FREEMODE_DELIVERABLE_TYPE GET_FREEMODE_DELIVERABLE_TYPE_FROM_SMUGGLED_GOODS(eSMUGGLED_GOODS_TYPE eType)
	SWITCH eType
		CASE eSGT_ANIMAL_MATERIALS 		RETURN SMUGGLER_DELIVERABLE_ANIMAL_MATERIALS
		CASE eSGT_ART_AND_ANTIQUES	 	RETURN SMUGGLER_DELIVERABLE_ART_AND_ANTIQUES
		CASE eSGT_CHEMICALS 			RETURN SMUGGLER_DELIVERABLE_CHEMICALS
		CASE eSGT_COUNTERFEIT_GOODS 	RETURN SMUGGLER_DELIVERABLE_COUNTERFEIT_GOODS
		CASE eSGT_JEWELRY_AND_GEMSTONES RETURN SMUGGLER_DELIVERABLE_JEWELRY_AND_GEMSTONES
		CASE eSGT_MEDICAL_SUPPLIES 		RETURN SMUGGLER_DELIVERABLE_MEDICAL_SUPPLIES
		CASE eSGT_NARCOTICS 			RETURN SMUGGLER_DELIVERABLE_NARCOTICS
		CASE eSGT_TOBACCO_AND_ALCOHOL 	RETURN SMUGGLER_DELIVERABLE_TOBACCO_AND_ALCOHOL
	ENDSWITCH
	
	RETURN FREEMODE_DELIVERABLE_TYPE_NO_TYPE
ENDFUNC

/// PURPOSE:
///    Uses the mission location to randomly select one of the furthest 3 dropoff locations to be used by all player on the business battle event
FUNC FREEMODE_DELIVERY_DROPOFFS FMBB_GET_RANDOM_SECONDARY_DROPOFF_FOR_BATTLE(VECTOR vMissionLocation)
	
	CONST_INT NUM_RANDOM_DROPOFFS 3
	
	INT i, j
	//Keep track of the three furthest dropoffs to pick one at random
	FLOAT fDropoffDist[NUM_RANDOM_DROPOFFS]
	FREEMODE_DELIVERY_DROPOFFS eSelectedDropoff[NUM_RANDOM_DROPOFFS]
	
	FOR i =  ENUM_TO_INT(BB_DROPOFF_SEC_1) TO ENUM_TO_INT(BB_DROPOFF_SEC_10)
		VECTOR vdropPos 								= FMBB_GET_IN_VEHICLE_DROPOFF_LOCATION(INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, i))		
		FLOAT fCheckDropoffDistance 					= VDIST2(vdropPos, vMissionLocation)
		FREEMODE_DELIVERY_DROPOFFS eDistCheckDropoff	= INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, i)
		
		REPEAT NUM_RANDOM_DROPOFFS j
			IF fCheckDropoffDistance > fDropoffDist[j]
				FLOAT fSwapDropoff	 					= fDropoffDist[j]
				FREEMODE_DELIVERY_DROPOFFS eSwapDropoff = eSelectedDropoff[j]
				
				fDropoffDist[j]		= fCheckDropoffDistance
				eSelectedDropoff[j] = eDistCheckDropoff
				
				fCheckDropoffDistance 	= fSwapDropoff
				eDistCheckDropoff 		= eSwapDropoff
			ENDIF
		ENDREPEAT
		
		fCheckDropoffDistance = 0.0
	ENDFOR
	
	i = GET_RANDOM_INT_IN_RANGE(0, NUM_RANDOM_DROPOFFS)
	
	//url:bugstar:4604016 - Business Battles - Can we weight this secondary drop-off in Paleto Bay so its rarely ever selected?
	IF eSelectedDropoff[i] = BB_DROPOFF_SEC_8
		PRINTLN("[FREEMODE_DELIVERY] FMBB_GET_RANDOM_SECONDARY_DROPOFF_FOR_BATTLE initial selection = BB_DROPOFF_SEC_8. Giving 10% chance to keep this dropoff")
		IF GET_RANDOM_INT_IN_RANGE(0, 100) <= 90
			IF (i + 1) = NUM_RANDOM_DROPOFFS
				i = 0
			ELSE
				i++
			ENDIF
		ENDIF
	ENDIF
	//url:bugstar:4604016 - Business Battles - Can we weight this secondary drop-off in Paleto Bay so its rarely ever selected?
	
	PRINTLN("[FREEMODE_DELIVERY] FMBB_GET_RANDOM_SECONDARY_DROPOFF_FOR_BATTLE - dropoff set to ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(eSelectedDropoff[i]), " index = ", i, " mission location is ", vMissionLocation)
	RETURN eSelectedDropoff[i]
ENDFUNC

/// PURPOSE:
///    Uses the mission location to randomly select one of the furthest 3 dropoff locations to be used by all player on the business battle event
FUNC FREEMODE_DELIVERY_DROPOFFS FREEMODE_DELIVERY_GET_CLOSEST_DROPOFF_IN_RANGE(FREEMODE_DELIVERY_DROPOFFS eRangeStart, FREEMODE_DELIVERY_DROPOFFS eRangeEnd)
	#IF IS_DEBUG_BUILD
		IF eRangeStart > eRangeEnd
			ASSERTLN("FREEMODE_DELIVERY_GET_CLOSEST_DROPOFF_IN_RANGE - Range end is greater than start")
			RETURN FREEMODE_DELIVERY_DROPOFF_INVALID
		ENDIF
		
		IF (ENUM_TO_INT(eRangeEnd) - ENUM_TO_INT(eRangeStart)) > 10
			ASSERTLN("FREEMODE_DELIVERY_GET_CLOSEST_DROPOFF_IN_RANGE - Range is too large")
			RETURN FREEMODE_DELIVERY_DROPOFF_INVALID
		ENDIF
	#ENDIF
	
	INT i
	//Keep track of the three furthest dropoffs to pick one at random
	FLOAT fSelectedDropoffDist = 9999999.99
	VECTOR vPlayerPos
	FREEMODE_DELIVERY_DROPOFFS eSelectedDropoff
	
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		PRINTLN("FREEMODE_DELIVERY_GET_CLOSEST_DROPOFF_IN_RANGE - Local player is not alive")
		RETURN FREEMODE_DELIVERY_DROPOFF_INVALID
	ELSE
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		PRINTLN("FREEMODE_DELIVERY_GET_CLOSEST_DROPOFF_IN_RANGE - Checking from position: ", vPlayerPos)
	ENDIF
	
	FOR i =  ENUM_TO_INT(eRangeStart) TO ENUM_TO_INT(eRangeEnd)
		VECTOR vdropPos = FMBB_GET_IN_VEHICLE_DROPOFF_LOCATION(INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, i))
		FLOAT fDist		= VDIST2(vdropPos, vPlayerPos)
		
		IF fDist < fSelectedDropoffDist
			eSelectedDropoff 		= INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, i)
			fSelectedDropoffDist	= fDist
			PRINTLN("FREEMODE_DELIVERY_GET_CLOSEST_DROPOFF_IN_RANGE - new closest dropoff: ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(eSelectedDropoff), " Distance = ", fDist)
		ENDIF
		
	ENDFOR
	
	RETURN eSelectedDropoff
ENDFUNC

/// PURPOSE:
///    Returns a dropoff for the local player to deliver to and activates it if for the local player if requested
/// PARAMS:
///    deliverableID 	- The pre selected secondary drop location. This must be selected by the mission using this function
///    eType			- The type of deliverable being used in this mission
///    bActivateDropoff - Does the local player need activate the selected dropoff
FUNC FREEMODE_DELIVERY_DROPOFFS FREEMODE_DELIVERY_GET_UNRESERVED_DROPOFF_FOR_FMBB(FREEMODE_DELIVERY_DROPOFFS eSelectedSecondaryLocation, FREEMODE_DELIVERABLE_TYPE eType, FM_DELIVERY_MISSION_ID sMissionID, BOOL bActivateDropoff = TRUE  #IF IS_DEBUG_BUILD , BOOL bPrintInfo = FALSE #ENDIF)
	
	IF g_FreemodeDeliveryData.sLocalDropoff.eDropoff != FREEMODE_DELIVERY_DROPOFF_INVALID
		IF bActivateDropoff
		#IF IS_DEBUG_BUILD 
		OR bPrintInfo 
		#ENDIF
			#IF IS_DEBUG_BUILD
				IF (GET_FRAME_COUNT() % 120) = 0
					PRINTLN("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_ACTIVATE_UNRESERVED_DROPOFF_FOR_FMBB - %120 - dropoff already set to ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(g_FreemodeDeliveryData.sLocalDropoff.eDropoff), ". EXIT")
				ENDIF
			#ENDIF
			RETURN g_FreemodeDeliveryData.sLocalDropoff.eDropoff
		ENDIF
	ENDIF
	
	PLAYER_INDEX playerOwner = PLAYER_ID()
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		playerOwner = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	ENDIF
	
	IF playerOwner = INVALID_PLAYER_INDEX()
		PRINTLN("FREEMODE_DELIVERY_GET_UNRESERVED_DROPOFF_FOR_FMBB - Unable to determine a valid owner")
		SCRIPT_ASSERT("FREEMODE_DELIVERY_GET_UNRESERVED_DROPOFF_FOR_FMBB - Unable to determine a valid owner")
		RETURN FREEMODE_DELIVERY_DROPOFF_INVALID
	ENDIF
	
	NIGHTCLUB_ID 		nClubID					= GET_PLAYERS_OWNED_NIGHTCLUB(playerOwner)
	SIMPLE_INTERIORS 	eNightClubInteriorID 	= SIMPLE_INTERIOR_INVALID
	
	IF IS_NIGHTCLUB_ID_VALID(nClubID)
		eNightClubInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_NIGHTCLUB_ID(nClubID)
	ENDIF
	
	SWITCH eType
		CASE BBT_DELIVERABLE_BIKER_PRODUCT
		CASE BBT_DELIVERABLE_BUNKER_PRODUCT
		CASE BBT_DELIVERABLE_HANGAR_PRODUCT
		CASE BBT_DELIVERABLE_CEO_PRODUCT
			
			BOOL bBusinessAcceptsDelivery
			
			bBusinessAcceptsDelivery = TRUE
			
			IF NOT HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(playerOwner, MBV_COLLECT_STAFF)
			OR NOT HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(playerOwner, MBV_COLLECT_EQUIPMENT)
			OR NOT HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(playerOwner, MBV_COLLECT_DJ_CRASH)
				#IF IS_DEBUG_BUILD
					IF bPrintInfo
					AND (GET_FRAME_COUNT() % 30) = 0
						PRINTLN("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_ACTIVATE_UNRESERVED_DROPOFF_FOR_FMBB - HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION MBV_COLLECT_STAFF = ", HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(playerOwner, MBV_COLLECT_STAFF))
						PRINTLN("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_ACTIVATE_UNRESERVED_DROPOFF_FOR_FMBB - HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION MBV_COLLECT_EQUIPMENT = ", HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(playerOwner, MBV_COLLECT_EQUIPMENT))
						PRINTLN("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_ACTIVATE_UNRESERVED_DROPOFF_FOR_FMBB - HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION MBV_COLLECT_DJ_CRASH = ", HAS_PLAYER_COMPLETED_NIGHTCLUB_SETUP_MISSION(playerOwner, MBV_COLLECT_DJ_CRASH))
					ENDIF
				#ENDIF
				bBusinessAcceptsDelivery = FALSE
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF bPrintInfo
				AND (GET_FRAME_COUNT() % 30) = 0
					PRINTLN("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_ACTIVATE_UNRESERVED_DROPOFF_FOR_FMBB - bBusinessAcceptsDelivery = FALSE")
				ENDIF
			#ENDIF
						
			IF IS_NIGHTCLUB_ID_VALID(nClubID)
			AND bBusinessAcceptsDelivery
				IF bActivateDropoff
					FREEMODE_DELIVERY_SET_LOCALLY_RESERVED_DROPOFF(GET_FMBB_DROPOFF_SIMPLE_INTERIOR(eNightClubInteriorID), sMissionID)
					PRINTLN("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_ACTIVATE_UNRESERVED_DROPOFF_FOR_FMBB - dropoff set to ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(GET_FMBB_DROPOFF_SIMPLE_INTERIOR(eNightClubInteriorID)), " bBusinessAcceptsDelivery? ", bBusinessAcceptsDelivery, " player owner = ", GET_PLAYER_NAME(playerOwner))
					RETURN g_FreemodeDeliveryData.sLocalDropoff.eDropoff
				ENDIF
				
				#IF IS_DEBUG_BUILD
					IF bPrintInfo
					AND (GET_FRAME_COUNT() % 30) = 0
						PRINTLN("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_ACTIVATE_UNRESERVED_DROPOFF_FOR_FMBB - dropoff returned = ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(GET_FMBB_DROPOFF_SIMPLE_INTERIOR(eNightClubInteriorID)), " bBusinessAcceptsDelivery? ", bBusinessAcceptsDelivery, " player owner = ", GET_PLAYER_NAME(playerOwner))
					ENDIF
				#ENDIF
				
				RETURN GET_FMBB_DROPOFF_SIMPLE_INTERIOR(eNightClubInteriorID)
			ELSE
				IF bActivateDropoff
					PRINTLN("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_ACTIVATE_UNRESERVED_DROPOFF_FOR_FMBB - dropoff set to ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(eSelectedSecondaryLocation), "bBusinessAcceptsDelivery? ", bBusinessAcceptsDelivery, " player owner = ", GET_PLAYER_NAME(playerOwner))
					FREEMODE_DELIVERY_SET_LOCALLY_RESERVED_DROPOFF(eSelectedSecondaryLocation, sMissionID)
				ENDIF
					
				#IF IS_DEBUG_BUILD
					IF bPrintInfo
					AND (GET_FRAME_COUNT() % 30) = 0
						PRINTLN("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_ACTIVATE_UNRESERVED_DROPOFF_FOR_FMBB - dropoff set to ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(eSelectedSecondaryLocation), "bBusinessAcceptsDelivery? ", bBusinessAcceptsDelivery, " player owner = ", GET_PLAYER_NAME(playerOwner))
					ENDIF
				#ENDIF
				
				RETURN eSelectedSecondaryLocation
			ENDIF			
			
		BREAK
		CASE AW_DELIVERABLE_EVENT_ITEM
			IF IS_NIGHTCLUB_ID_VALID(nClubID)
				IF bActivateDropoff
					FREEMODE_DELIVERY_SET_LOCALLY_RESERVED_DROPOFF(GET_FMBB_DROPOFF_SIMPLE_INTERIOR(eNightClubInteriorID), sMissionID)
					PRINTLN("[FREEMODE_DELIVERY][EVENT_ITEM] FREEMODE_DELIVERY_ACTIVATE_UNRESERVED_DROPOFF_FOR_FMBB - dropoff set to ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(GET_FMBB_DROPOFF_SIMPLE_INTERIOR(eNightClubInteriorID)), " bBusinessAcceptsDelivery? ", bBusinessAcceptsDelivery, " player owner = ", GET_PLAYER_NAME(playerOwner))
					RETURN g_FreemodeDeliveryData.sLocalDropoff.eDropoff
				ENDIF
				
				#IF IS_DEBUG_BUILD
					IF bPrintInfo
					AND (GET_FRAME_COUNT() % 30) = 0
						PRINTLN("[FREEMODE_DELIVERY][EVENT_ITEM] FREEMODE_DELIVERY_ACTIVATE_UNRESERVED_DROPOFF_FOR_FMBB - dropoff returned = ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(GET_FMBB_DROPOFF_SIMPLE_INTERIOR(eNightClubInteriorID)), " bBusinessAcceptsDelivery? ", bBusinessAcceptsDelivery, " player owner = ", GET_PLAYER_NAME(playerOwner))
					ENDIF
				#ENDIF
				
				RETURN GET_FMBB_DROPOFF_SIMPLE_INTERIOR(eNightClubInteriorID)
			ELSE
				IF bActivateDropoff
					PRINTLN("[FREEMODE_DELIVERY][EVENT_ITEM] FREEMODE_DELIVERY_ACTIVATE_UNRESERVED_DROPOFF_FOR_FMBB - dropoff set to ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(eSelectedSecondaryLocation), "bBusinessAcceptsDelivery? ", bBusinessAcceptsDelivery, " player owner = ", GET_PLAYER_NAME(playerOwner))
					FREEMODE_DELIVERY_SET_LOCALLY_RESERVED_DROPOFF(eSelectedSecondaryLocation, sMissionID)
				ENDIF
					
				#IF IS_DEBUG_BUILD
					IF bPrintInfo
					AND (GET_FRAME_COUNT() % 30) = 0
						PRINTLN("[FREEMODE_DELIVERY][EVENT_ITEM] FREEMODE_DELIVERY_ACTIVATE_UNRESERVED_DROPOFF_FOR_FMBB - dropoff set to ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(eSelectedSecondaryLocation), "bBusinessAcceptsDelivery? ", bBusinessAcceptsDelivery, " player owner = ", GET_PLAYER_NAME(playerOwner))
					ENDIF
				#ENDIF
				
				RETURN eSelectedSecondaryLocation
			ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		IF bPrintInfo
		AND (GET_FRAME_COUNT() % 30) = 0
			PRINTLN("[FREEMODE_DELIVERY] FREEMODE_DELIVERY_ACTIVATE_UNRESERVED_DROPOFF_FOR_FMBB - END dropoff", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(g_FreemodeDeliveryData.sLocalDropoff.eDropoff), "eType = ", eType, " player owner = ", GET_PLAYER_NAME(playerOwner))
		ENDIF
	#ENDIF
	
	RETURN g_FreemodeDeliveryData.sLocalDropoff.eDropoff
ENDFUNC

PROC _FREEMODE_DELIVERY_UPDATE_ACTIVE_BUSINESS_BATTLES_DROPOFF(FREEMODE_DELIVERY_DROPOFFS eSelectedSecondaryLocation, FM_DELIVERY_MISSION_ID sMissionID)
	
	IF NOT FREEMODE_DELIVERY_IS_MISSION_ID_VALID(sMissionID)
		SCRIPT_ASSERT("_FREEMODE_DELIVERY_UPDATE_ACTIVE_BUSINESS_BATTLES_DROPOFF - Invalid mission ID")
		EXIT
	ENDIF
	
	IF (GET_FRAME_COUNT() % 20) = 0
	AND g_FreemodeDeliveryData.sLocalDropoff.eDropoff != FREEMODE_DELIVERY_DROPOFF_INVALID
		
		FREEMODE_DELIVERABLE_TYPE eDeliverableType 	= FREEMODE_DELIVERY_GET_DELIVERABLE_TYPE(FREEMODE_DELIVERY_GET_DELIVERABLE_PLAYER_POSSESSES(PLAYER_ID()))
		FREEMODE_DELIVERY_DROPOFFS eDropoff 		= FREEMODE_DELIVERY_GET_UNRESERVED_DROPOFF_FOR_FMBB(eSelectedSecondaryLocation, eDeliverableType, sMissionID, FALSE)
		
		IF eDropoff != g_FreemodeDeliveryData.sLocalDropoff.eDropoff
			PRINTLN("[FREEMODE_DELIVERY] _FREEMODE_DELIVERY_UPDATE_ACTIVE_BUSINESS_BATTLES_DROPOFF - updating to ", eDropoff, " from ", g_FreemodeDeliveryData.sLocalDropoff.eDropoff)
			#IF USE_FINAL_PRINTS PRINTLN_FINAL("[FREEMODE_DELIVERY] _FREEMODE_DELIVERY_UPDATE_ACTIVE_BUSINESS_BATTLES_DROPOFF - updating to ", eDropoff, " from ", g_FreemodeDeliveryData.sLocalDropoff.eDropoff)#ENDIF
			FREEMODE_DELIVERY_SET_LOCALLY_RESERVED_DROPOFF(eDropoff, sMissionID)
		ENDIF
		
	ENDIF
ENDPROC
