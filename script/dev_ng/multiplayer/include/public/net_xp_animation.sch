// Name: net_xp_animation.sch
// Author: James Adwick
// Date: 14/05/2012
//
// Header to handle the positioning and animation of XP for in-game entities

USING "globals.sch"
USING "commands_network.sch"
USING "hud_drawing.sch"
//USING "uiutil.sch"

// ------------------------ Utility Functions -------------------------

FUNC ENTITY_INDEX CONVERT_TO_ENTITY(ENTITY_INDEX EntityID)
	RETURN EntityID
ENDFUNC

/// PURPOSE:
/// 	Calculates the offset in the z-dim based on the entity model
///    	This is the starting position for the xp animation.
FUNC VECTOR CALC_Z_OFFSET(ENTITY_INDEX xpEntity, BOOL bAbove = TRUE)
	
	VECTOR vXPOrigin
	VECTOR CamRot

	IF IS_GAMEPLAY_CAM_RENDERING()
		CamRot = GET_GAMEPLAY_CAM_ROT()
	ENDIF
	
	// added by Neil F. for 1974053
	IF (xpEntity = CONVERT_TO_ENTITY(PLAYER_PED_ID()))
	AND (GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON)
		vXPOrigin = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(xpEntity, <<0.0, FIRST_PERSON_PICKUP_OFFSET_Y, FIRST_PERSON_PICKUP_OFFSET_z>>)
	ELSE
		vXPOrigin = GET_ENTITY_COORDS(xpEntity, FALSE)
	ENDIF
	
	FLOAT fHeading = 0.0
	
	IF NOT IS_ENTITY_DEAD(xpEntity)
		fHeading = GET_ENTITY_HEADING(xpEntity)
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
			fHeading = CamRot.z
		ENDIF
	ENDIF
	
	VECTOR vMin, vMax
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(xpEntity), vMin, vMax)
	
	FLOAT zOffsetLocal
	IF bAbove
		zOffsetLocal = (vMax.z + 0.18)
	ELSE
		zOffsetLocal = (vMin.z + 0.18)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bXpAnimAboveHead
		
			// Tweak starting position of text if aiming
			IF IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
				zOffsetLocal = vMax.z - 0.1
			ENDIF
		ENDIF
	#ENDIF

	vXPOrigin = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vXPOrigin, fHeading, <<0,0,zOffsetLocal>>)
	
	RETURN vXPOrigin
ENDFUNC

/// PURPOSE:
///    Calculates the scale of the text based on the distance from game camera.
FUNC FLOAT CALC_XP_SCALE(VECTOR vEntityPos, FLOAT& fScaleX, FLOAT& fScaleY)
	
	FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(GET_GAMEPLAY_CAM_COORD(), vEntityPos)
	
	
	// Limit to maximum size when near to camera
	IF fDist < XP_ANIM_MIN_DIST
		fScaleX = XP_ANIM_MAX_SCALEX
		fScaleY = XP_ANIM_MAX_SCALEY
		
		RETURN 0.0
	ENDIF
	
	// Limit to minimum size when too far away 
	// (means we will always see it - could look wierd over very small ped, although less often then close up)
	IF fDist > XP_ANIM_MAX_DIST
		fScaleX = XP_ANIM_MIN_SCALEX
		fScaleY = XP_ANIM_MIN_SCALEY
		
		RETURN 1.0
	ENDIF
	
	// Calculate scale between values
	FLOAT fScale = 1 - ((fDist - XP_ANIM_MIN_DIST) / (XP_ANIM_MAX_DIST - XP_ANIM_MIN_DIST))
	FLOAT fTempScaleX = (fScale * (XP_ANIM_MAX_SCALEX - XP_ANIM_MIN_SCALEX))
	FLOAT fTempScaleY = (fScale * (XP_ANIM_MAX_SCALEY - XP_ANIM_MIN_SCALEY))

	fScaleX = fTempScaleX + XP_ANIM_MIN_SCALEX
	fScaleY = fTempScaleY + XP_ANIM_MIN_SCALEY
	
	RETURN fScale
ENDFUNC

FUNC INT GET_XP_ANIM_MULTIPLIER()
	INT iMult = MPGlobals.g_iXPAnimMultiplier
	MPGlobals.g_iXPAnimMultiplier = 1
	
	RETURN iMult	
ENDFUNC

FUNC BOOL GET_ENTITY_TRACK_BOOL()
	IF MPGlobals.g_bXPTrackEntity
		MPGlobals.g_bXPTrackEntity = FALSE
		RETURN TRUE
	ELSE
		MPGlobals.g_bXPTrackEntity = FALSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC STORE_XP_ANIMATION(VECTOR vAnimLoc, INT iXPValue, HUD_ANIM_TYPE animType = HUD_ANIM_XP, STRING stXPDesc = NULL, INT iDisplayDurationMS = 0)
	
	DEBUG_PRINTCALLSTACK()
	
	IF iXPValue != 0		// 1035715 - we now lose XP for suicide
		INT i
		INT index = -1
		REPEAT NUM_XP_ANIMS i

			IF MPGlobals.XpAnims[i].animPhase = PHASE_NULL
			OR MPGlobals.XpAnims[i].animPhase = PHASE_COMPLETE
				index = i
				i = NUM_XP_ANIMS	// Exit loop
			ENDIF

		ENDREPEAT

		// If space for xp animation - add to array
		IF index != -1
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("[JA@XP] STORE_XP_ANIMATION - called... XP value = ") NET_PRINT_INT(iXPValue) NET_NL()
				NET_PRINT("[JA@XP] Location of entity: ") NET_PRINT_VECTOR(vAnimLoc) NET_NL()
			#ENDIF
			
			MPGlobals.XpAnims[index].vEntityOrigin = vAnimLoc
			MPGlobals.XpAnims[index].animPhase = PHASE_READY
			MPGlobals.XpAnims[index].fFloatScale = CALC_XP_SCALE(MPGlobals.XpAnims[index].vEntityOrigin, fXPScaleX, fXPScaleY)
			MPGlobals.XpAnims[index].iPhaseStartTime = GET_NETWORK_TIME()
			MPGlobals.XpAnims[index].iXPValue = iXPValue
			MPGlobals.XpAnims[index].animType = animType
			MPGlobals.XpAnims[index].iPhaseMultiplier = GET_XP_ANIM_MULTIPLIER()
			MPGlobals.XpAnims[index].bTrackEntity = GET_ENTITY_TRACK_BOOL()
			MPGlobals.XpAnims[index].tlMainText = stXPDesc														 // Added for 955624
			MPGlobals.XpAnims[index].iDisplayDuration = GET_TIME_OFFSET(GET_NETWORK_TIME(), iDisplayDurationMS)  // Added for 2879937
			
		#IF IS_DEBUG_BUILD
		ELSE
			NET_PRINT("[JA@XP] Hit maximum amount of XP at one time (25)") NET_NL()
		#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC STORE_XP_ANIMATION_WITH_NAME(VECTOR vAnimLoc, INT iXPValue, TEXT_LABEL_63 tlName)

	INT i
	INT index = -1
	REPEAT NUM_XP_ANIMS i
	
		IF MPGlobals.XpAnims[i].animPhase = PHASE_NULL
		OR MPGlobals.XpAnims[i].animPhase = PHASE_COMPLETE
			index = i
			i = NUM_XP_ANIMS	// Exit loop
		ENDIF
	
	ENDREPEAT
	
	// If space for xp animation - add to array
	IF index != -1
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("[JA@XP] STORE_XP_ANIMATION_WITH_NAME - called... XP value = ") NET_PRINT_INT(iXPValue) NET_NL()
			NET_PRINT("[JA@XP] Location of entity: ") NET_PRINT_VECTOR(vAnimLoc) NET_NL()
		#ENDIF
		
		MPGlobals.XpAnims[index].vEntityOrigin = vAnimLoc
		MPGlobals.XpAnims[index].animPhase = PHASE_READY
		MPGlobals.XpAnims[index].fFloatScale = CALC_XP_SCALE(MPGlobals.XpAnims[index].vEntityOrigin, fXPScaleX, fXPScaleY)
		MPGlobals.XpAnims[index].iPhaseStartTime = GET_NETWORK_TIME()
		MPGlobals.XpAnims[index].iXPValue = iXPValue
		MPGlobals.XpAnims[index].tlTextToDisplay = tlName
		MPGlobals.XpAnims[index].animType = HUD_ANIM_XP_WITH_NAME
		MPGlobals.XpAnims[index].iPhaseMultiplier = GET_XP_ANIM_MULTIPLIER()
		MPGlobals.XpAnims[index].bTrackEntity = GET_ENTITY_TRACK_BOOL()
	
	#IF IS_DEBUG_BUILD
	ELSE
		NET_PRINT("[JA@XP] Hit maximum amount of XP at one time (25)") NET_NL()
	#ENDIF
		
	ENDIF

ENDPROC

PROC STORE_ANIMATION_WITH_STRING(VECTOR vAnimLoc, TEXT_LABEL_63 tlName, BOOL bLiteralString = TRUE)

	INT i
	INT index = -1
	REPEAT NUM_XP_ANIMS i
	
		IF MPGlobals.XpAnims[i].animPhase = PHASE_NULL
		OR MPGlobals.XpAnims[i].animPhase = PHASE_COMPLETE
			index = i
			i = NUM_XP_ANIMS	// Exit loop
		ENDIF
	
	ENDREPEAT
	
	// If space for xp animation - add to array
	IF index != -1
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("[JA@XP] STORE_ANIMATION_WITH_STRING - called... ") NET_NL()
			NET_PRINT("[JA@XP] Location of entity: ") NET_PRINT_VECTOR(vAnimLoc) NET_NL()
		#ENDIF
		
		MPGlobals.XpAnims[index].vEntityOrigin = vAnimLoc
		MPGlobals.XpAnims[index].animPhase = PHASE_READY
		MPGlobals.XpAnims[index].fFloatScale = CALC_XP_SCALE(MPGlobals.XpAnims[index].vEntityOrigin, fXPScaleX, fXPScaleY)
		MPGlobals.XpAnims[index].iPhaseStartTime = GET_NETWORK_TIME()
		MPGlobals.XpAnims[index].tlTextToDisplay = tlName
		IF bLiteralString
			MPGlobals.XpAnims[index].animType = HUD_ANIM_WITH_NAME
		ELSE
			MPGlobals.XpAnims[index].animType = HUD_ANIM_WITH_LABEL
		ENDIF
		MPGlobals.XpAnims[index].iPhaseMultiplier = GET_XP_ANIM_MULTIPLIER()
		MPGlobals.XpAnims[index].bTrackEntity = GET_ENTITY_TRACK_BOOL()
		
	#IF IS_DEBUG_BUILD
	ELSE
		NET_PRINT("[JA@XP] Hit maximum amount of XP at one time (25)") NET_NL()
	#ENDIF
	ENDIF

ENDPROC


// ------------------------- Public Functions -----------------------------

ENUM XP_ANIM_MULTIPLIER
	XP_DOUBLE_MULT = 2,
	XP_TRIPLE_MULT,
	XP_QUAD_MULT
ENDENUM

/// PURPOSE: Set a multiplier for the phase time of the xp animations. (exists for the next animation only)
PROC SET_XP_ANIMATION_MULTIPLIER(XP_ANIM_MULTIPLIER multiplier)
	
	NET_PRINT("[JA@XP] SET_XP_ANIMATION_MULTIPLIER: ") NET_PRINT_INT(ENUM_TO_INT(multiplier)) NET_NL()

	MPGlobals.g_iXPAnimMultiplier = ENUM_TO_INT(multiplier)
ENDPROC

/// PURPOSE: Set the bool so the xp animation will track the entity passed
PROC SET_XP_ANIM_TO_TRACK_PLAYER(BOOL bTrack)
	MPGlobals.g_bXPTrackEntity = bTrack
ENDPROC

/// PURPOSE:
///    Called during the awarding of XP. Draw xp above in-game entity
PROC SET_XP_REWARD_FOR_ENTITY(ENTITY_INDEX xpEntity, INT iXPValue, STRING stXPDesc = NULL, INT iDisplayDurationMS = 0) //, BOOL bIsDead = FALSE)

	// Default mode (above entity)
	VECTOR vEntityPos = CALC_Z_OFFSET(xpEntity)
	
	// Override if we are in above head mode
	#IF IS_DEBUG_BUILD
		IF bXpAnimAboveHead
			vEntityPos = CALC_Z_OFFSET(PLAYER_PED_ID())
			
			IF bXpAnimXYZ
				bXpAnimXYZ = FALSE
			ENDIF
		ENDIF
		
		IF bXpAnimXYZ
			vEntityPos = vXpAnimPt
						
			IF bXpAnimAboveHead
				bXpAnimAboveHead = FALSE
			ENDIF			
		ENDIF
	#ENDIF
	
	// If we are the entity then allow it to track us
	IF xpEntity = GET_ENTITY_FROM_PED_OR_VEHICLE(PLAYER_PED_ID())
		SET_XP_ANIM_TO_TRACK_PLAYER(TRUE)
	ENDIF
	
	STORE_XP_ANIMATION(vEntityPos, iXPValue, HUD_ANIM_XP, stXPDesc, iDisplayDurationMS)
	
ENDPROC

#IF USE_TU_CHANGES

PROC SET_CASH_REWARD_FOR_ENTITY(ENTITY_INDEX cashEntity, INT iCashValue)
	//VECTOR vEntityPos = GET_ENTITY_COORDS(cashEntity, FALSE)
	VECTOR vEntityPos = CALC_Z_OFFSET(PLAYER_PED_ID())
	//vEntityPos.z -= 0.65
	
	// If we are the entity then allow it to track us
	IF cashEntity = GET_ENTITY_FROM_PED_OR_VEHICLE(PLAYER_PED_ID())
		SET_XP_ANIM_TO_TRACK_PLAYER(TRUE)
	ENDIF
	
	STORE_XP_ANIMATION(vEntityPos, iCashValue, HUD_ANIM_CASH_PLAYER)
ENDPROC

PROC SET_CASH_REWARD_FOR_LOCATION(VECTOR cashLoc, INT iCashValue)	
	STORE_XP_ANIMATION(cashLoc, iCashValue, HUD_ANIM_CASH)
ENDPROC

PROC SET_PLAYER_DEAD_ANIMATION(ENTITY_INDEX playerEntity)
			
	// Default mode (above entity)
	VECTOR vEntityPos = CALC_Z_OFFSET(playerEntity)
	
	IF IS_ENTITY_A_PED(playerEntity)
		IF IS_PED_A_PLAYER(GET_PED_INDEX_FROM_ENTITY_INDEX(playerEntity))
			PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_INDEX_FROM_ENTITY_INDEX(playerEntity))
			TEXT_LABEL_63 tlName = GET_PLAYER_NAME(playerID)
			
//			#IF IS_DEBUG_BUILD
//				NET_PRINT("[JA@XP] SET_XP_REWARD_FOR_ENTITY - called and displaying players name too") NET_NL()
//			#ENDIF
			
			STORE_ANIMATION_WITH_STRING(vEntityPos, tlName)
			EXIT										// <----- EXIT
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Called during the awarding of Ammo. Draw Ammo at ground level by player
PROC SET_AMMO_REWARD_FOR_ENTITY(ENTITY_INDEX ammoEntity, INT iAmmoValue)

	IF iAmmoValue > 0
		// Default mode (above entity)
		//VECTOR vEntityPos = GET_ENTITY_COORDS(ammoEntity, FALSE) //CALC_Z_OFFSET(PLAYER_PED_ID(), FALSE)
		IF IS_VECTOR_ZERO(GET_ENTITY_COORDS(ammoEntity, FALSE))
		
		ENDIF
		
		SET_XP_ANIM_TO_TRACK_PLAYER(TRUE)
		
		VECTOR vEntityPos = CALC_Z_OFFSET(PLAYER_PED_ID())

		//vEntityPos.z -= 0.65
		
		INT i
		INT index = -1
		REPEAT NUM_XP_ANIMS i
		
			IF MPGlobals.XpAnims[i].animPhase = PHASE_NULL
			OR MPGlobals.XpAnims[i].animPhase = PHASE_COMPLETE
				index = i
				i = NUM_XP_ANIMS	// Exit loop
			ENDIF
		
		ENDREPEAT
		
		// If space for xp animation - add to array
		IF index != -1
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("[JA@XP] SET_AMMO_REWARD_FOR_ENTITY - called... AMMO value = ") NET_PRINT_INT(iAmmoValue) NET_NL()
			#ENDIF

			MPGlobals.XpAnims[index].vEntityOrigin = vEntityPos
			MPGlobals.XpAnims[index].animPhase = PHASE_READY
			MPGlobals.XpAnims[index].fFloatScale = CALC_XP_SCALE(MPGlobals.XpAnims[index].vEntityOrigin, fXPScaleX, fXPScaleY)
			MPGlobals.XpAnims[index].iPhaseStartTime = GET_NETWORK_TIME()
			MPGlobals.XpAnims[index].iXPValue = iAmmoValue
			MPGlobals.XpAnims[index].animType = HUD_ANIM_AMMO
			MPGlobals.XpAnims[index].iPhaseMultiplier = GET_XP_ANIM_MULTIPLIER()
			MPGlobals.XpAnims[index].bTrackEntity = GET_ENTITY_TRACK_BOOL()
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("[JA@XP] GET_ENTITY_TRACK_BOOL()")NET_PRINT_BOOL(MPGlobals.XpAnims[index].bTrackEntity)
			#ENDIF
				g_bRewardedAmmo = TRUE
			REQUEST_STREAMED_TEXTURE_DICT("MPHud")
		#IF IS_DEBUG_BUILD
		ELSE
			NET_PRINT("[JA@XP] Hit maximum amount of XP at one time (25)") NET_NL()
		#ENDIF
		ENDIF
	ENDIF
ENDPROC

ENUM eMP_SCRIPT_PICKUP_TYPE
	MP_PICKUP_SNACK_1,
	MP_PICKUP_SNACK_2,
	MP_PICKUP_SNACK_3,
	MP_PICKUP_DRINK_1,
	MP_PICKUP_DRINK_2,
	#IF FEATURE_DLC_1_2022
	MP_PICKUP_DRINK_4,
	#ENDIF
	MP_PICKUP_SMOKES,
	MP_PICKUP_PARA_CHECKPOINT
ENDENUM

FUNC BOOL GET_PICKUP_ANIM_TEXT_LABEL(eMP_SCRIPT_PICKUP_TYPE ePickup, TEXT_LABEL_31& tlPickup)

	SWITCH ePickup
		CASE MP_PICKUP_SNACK_1
			tlPickup = "PU_REWARD_S1"
			RETURN TRUE
			
		CASE MP_PICKUP_SNACK_2
			tlPickup = "PU_REWARD_S2"
			RETURN TRUE
			
		CASE MP_PICKUP_SNACK_3
			tlPickup = "PU_REWARD_S3"
			RETURN TRUE
			
		CASE MP_PICKUP_DRINK_1
			tlPickup = "PU_REWARD_D1"
			RETURN TRUE
			
		CASE MP_PICKUP_DRINK_2
			tlPickup = "PU_REWARD_D2"
			RETURN TRUE
			
		CASE MP_PICKUP_SMOKES
			tlPickup = "PU_REWARD_11"
			RETURN TRUE
			
		CASE MP_PICKUP_PARA_CHECKPOINT
			tlPickup = "PU_REWARD_PC"
			RETURN TRUE
		#IF FEATURE_DLC_1_2022
		CASE MP_PICKUP_DRINK_4
			tlPickup = "PU_REWARD_D3"
			RETURN TRUE
		#ENDIF
			
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


PROC SET_PICKUP_ANIM_FOR_ENTITY( eMP_SCRIPT_PICKUP_TYPE ePickup, FLOAT iPickupValue = -1.0, HUD_ANIM_TYPE animType = HUD_ANIM_PICKUP)	// Could add an additional param (entity_index) if we want to animate from specific entity
	TEXT_LABEL_31 tlPickup

	IF iPickupValue > 0
		IF GET_PICKUP_ANIM_TEXT_LABEL(ePickup, tlPickup)
		
			//IF IS_SAFE_TO_ANIMATE(ePickup)
		
				// Default mode (above entity)
				VECTOR vEntityPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE) //CALC_Z_OFFSET(PLAYER_PED_ID(), FALSE)
				//vEntityPos.z -= 0.65
				
//				IF xpEntity = GET_ENTITY_FROM_PED_OR_VEHICLE(PLAYER_PED_ID()) 
               		SET_XP_ANIM_TO_TRACK_PLAYER(TRUE) 
					PRINTLN("SET_PICKUP_ANIM_FOR_ENTITY - SET_XP_ANIM_TO_TRACK_PLAYER(TRUE) ")
//       		ENDIF

				
				INT i
				INT index = -1
				REPEAT NUM_XP_ANIMS i
				
					IF MPGlobals.XpAnims[i].animPhase = PHASE_NULL
					OR MPGlobals.XpAnims[i].animPhase = PHASE_COMPLETE
						index = i
						i = NUM_XP_ANIMS	// Exit loop
					ENDIF
				
				ENDREPEAT
				
				// If space for xp animation - add to array
				IF index != -1
					
	//				#IF IS_DEBUG_BUILD
	//					NET_PRINT("[JA@XP] SET_PICKUP_ANIM_FOR_ENTITY - called...") NET_NL()
	//				#ENDIF
					
					MPGlobals.XpAnims[index].vEntityOrigin = vEntityPos
					MPGlobals.XpAnims[index].animPhase = PHASE_READY
					MPGlobals.XpAnims[index].fFloatScale = CALC_XP_SCALE(MPGlobals.XpAnims[index].vEntityOrigin, fXPScaleX, fXPScaleY)
					MPGlobals.XpAnims[index].fFloatToShow = iPickupValue
					MPGlobals.XpAnims[index].iPhaseStartTime = GET_NETWORK_TIME()
					MPGlobals.XpAnims[index].animType = animType
					MPGlobals.XpAnims[index].tlTextToDisplay = tlPickup
					MPGlobals.XpAnims[index].iPhaseMultiplier = GET_XP_ANIM_MULTIPLIER()
					MPGlobals.XpAnims[index].bTrackEntity = GET_ENTITY_TRACK_BOOL()
					//MPGlobals.g_iLastAnimTag = ENUM_TO_INT(ePickup)
					
				#IF IS_DEBUG_BUILD
				ELSE
					NET_PRINT("[JA@XP] Hit maximum amount of XP at one time (25)") NET_NL()
				#ENDIF
				ENDIF
			//ENDIF
		ENDIF
	ENDIF
ENDPROC

CONST_FLOAT fAMMO_SPRITE_SIZES_Width	0.0072	//0.018
CONST_FLOAT fAMMO_SPRITE_SIZES_Height	0.0450	//0.030

PROC GET_AMMO_SPRITE_SIZES(FLOAT &fWidth, FLOAT &fHeight, FLOAT fMultiplier, TEXT_STYLE textStyle)
	#IF IS_DEBUG_BUILD
	IF debugXPAnim.bUsedWidgetValues
		fMultiplier = debugXPAnim.fIconSizeMultiplier
	ENDIF
	#ENDIF
	
	fWidth = fAMMO_SPRITE_SIZES_Width*fMultiplier*(textStyle.XScale/fXPBaseScaleX) // Fixed width and height from trial and error
	fHeight = fAMMO_SPRITE_SIZES_Height*fMultiplier*(textStyle.YScale/fXPBaseScaleY)
	#IF IS_DEBUG_BUILD
		IF debugXPAnim.bDisplayDebug
			PRINTLN("GET_AMMO_SPRITE_SIZES: Icon X scale = ",textStyle.XScale/fXPBaseScaleX)
			PRINTLN("GET_AMMO_SPRITE_SIZES: Icon Y Scale = ",textStyle.YScale/fXPBaseScaleY)
		ENDIF
	#ENDIF
	
	FLOAT fAspectRatio
	INT iResX, iResY
	GET_SCREEN_RESOLUTION(iResX, iResY)
	fAspectRatio = TO_FLOAT(iResX) / TO_FLOAT(iResY)
	
	fWidth = fWidth * 16.0 / 9.0
	fHeight = fHeight / fAspectRatio
	
//	IF animPhase = PHASE_ANIMATE_140
//	OR animPhase = PHASE_READY
//		fWidth *= 1.4
//		fHeight *= 1.4
//	ENDIF
ENDPROC

/// PURPOSE: Check if a player is currently hacking.
/// 	
FUNC BOOL IS_PLAYER_HACKING()

	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
		#IF IS_DEBUG_BUILD
			PRINTLN("[AW_NET_XP] player is hacking")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// Perform checks required to pass to show XP animation
FUNC BOOL IS_SAFE_TO_SHOW_XP_ANIM()

//	957432
//	IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE)
//		RETURN FALSE
//	ENDIF
	
	IF IS_CINEMATIC_CAM_RENDERING()
	AND NOT IS_BONNET_CINEMATIC_CAM_RENDERING()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_HACKING()
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
		RETURN FALSE
	ENDIF
	
	IF g_b_OnLeaderboard
		RETURN FALSE
	ENDIF
	
	IF g_TransitionData.SwoopStage != SKYSWOOP_NONE
		RETURN FALSE
	ENDIF 

	IF NETWORK_IS_IN_MP_CUTSCENE()
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
/// 	
FUNC BOOL IS_PLAYER_RIDING_ROLLERCOASTER()

	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bUsingRollerCoaster)
		#IF IS_DEBUG_BUILD
			PRINTLN("[AW_NET_XP] player is using the rollercoaster")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC



CONST_FLOAT fRENDER_XP_Cash_Text_Width_100	1.55	//-0.55
CONST_FLOAT fRENDER_XP_Cash_Text_Width_10	2.05	//-0.55
CONST_FLOAT fRENDER_XP_Cash_Text_Width_1	3.55	//-0.55
CONST_FLOAT fRENDER_XP_Cash_Text_Width_2_to_9	2.9	//-0.55

CONST_FLOAT fRENDER_XP_Ammo_Text_Width_100	1.55	//-0.55
CONST_FLOAT fRENDER_XP_Ammo_Text_Width_10	2.05	//-0.55
CONST_FLOAT fRENDER_XP_Ammo_Text_Width_1	3.55	//-0.55
CONST_FLOAT fRENDER_XP_Ammo_Text_Width_2_to_9	2.9	//-0.55


/// PURPOSE:
///     Renders XP text above entity based on placement and style passed.
FUNC BOOL RENDER_XP(INT index, VECTOR vRenderOrigin, TEXT_PLACEMENT textPlacement, TEXT_STYLE textStyle, BOOL bPerformDelay = FALSE)
	
	// If we are in cinematic mode don't render text
	IF NOT IS_SAFE_TO_SHOW_XP_ANIM()
		RETURN FALSE
	ENDIF
	
	FLOAT fXPos
	FLOAT fYPos
	
	IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
		IF MPGlobals.XpAnims[index].bDoFirstPersonXP = FALSE
			MPGlobals.XpAnims[index].bDoFirstPersonXP = TRUE
			PRINTLN("[JA@XP] RENDER_XP] - Emulating First Person XP ")
		ENDIF
	ELSE
		IF MPGlobals.XpAnims[index].bDoFirstPersonXP = TRUE
			MPGlobals.XpAnims[index].bDoFirstPersonXP = FALSE
			PRINTLN("[JA@XP] RENDER_XP] - Not Emulating First Person XP ")
		ENDIF
	ENDIF
	
	IF MPGlobals.XpAnims[index].bDoFirstPersonXP = FALSE
		IF NOT MPGlobals.XpAnims[index].bDrawIn2DSpace
			IF GET_SCREEN_COORD_FROM_WORLD_COORD(vRenderOrigin, fXPos, fYpos)	// Check the co-ords are on screen
				MPGlobals.XpAnims[index].fLastKnownX = fXPos
				MPGlobals.XpAnims[index].fLastKnownY = fYpos
				
				// Keep the previous position
				IF MPGlobals.XpAnims[index].fLastKnownX < 0.07
					MPGlobals.XpAnims[index].fLastKnownX = 0.07
				ELIF MPGlobals.XpAnims[index].fLastKnownX > 0.93
					MPGlobals.XpAnims[index].fLastKnownX = 0.93
				ENDIF
				
				IF MPGlobals.XpAnims[index].fLastKnownY < 0.07
					MPGlobals.XpAnims[index].fLastKnownY = 0.07
				ELIF MPGlobals.XpAnims[index].fLastKnownY > 0.93
					MPGlobals.XpAnims[index].fLastKnownY = 0.93
				ENDIF

	//			MPGlobals.XpAnims[index].bDrawIn2DSpace = TRUE
				
				PRINTLN("[JA@XP] RENDER_XP - On Screen. Screen Coords: (", fXPos, ", ", fYpos, ")")
			ELSE
				PRINTLN("[JA@XP] RENDER_XP - Off Screen. Screen Coords: (", fXPos, ", ", fYpos, ")")
				IF (MPGlobals.XpAnims[index].fLastKnownX = 0 AND MPGlobals.XpAnims[index].fLastKnownY = 0)
				
					VECTOR vPlayerPos = GET_PLAYER_COORDS(PLAYER_ID())
					IF GET_SCREEN_COORD_FROM_WORLD_COORD(vPlayerPos, fXPos, fYpos)
					
						MPGlobals.XpAnims[index].fLastKnownX = fXPos
						MPGlobals.XpAnims[index].fLastKnownY = fYpos
						
						PRINTLN("[JA@XP] RENDER_XP - Off Screen. Last coords are zero. Initialise to player on screen coords (", MPGlobals.XpAnims[index].fLastKnownX, ", ", MPGlobals.XpAnims[index].fLastKnownY, ")")
					ELSE
						MPGlobals.XpAnims[index].fLastKnownX = 0.5
						MPGlobals.XpAnims[index].fLastKnownY = 0.8
						
						PRINTLN("[JA@XP] RENDER_XP - Off Screen. Last coords are zero. Player off-screen. Pick centre of screen (", MPGlobals.XpAnims[index].fLastKnownX, ", ", MPGlobals.XpAnims[index].fLastKnownY, ")")
					ENDIF
				ENDIF
			
				PRINTLN("[JA@XP] RENDER_XP - Off Screen. Last known coords: (", MPGlobals.XpAnims[index].fLastKnownX, ", ", MPGlobals.XpAnims[index].fLastKnownY, ")")
				
				fXPos = MPGlobals.XpAnims[index].fLastKnownX
				fYPos = MPGlobals.XpAnims[index].fLastKnownY
				
				MPGlobals.XpAnims[index].bDrawIn2DSpace = TRUE
				
				IF MPGlobals.XpAnims[index].animPhase < PHASE_FADE_OUT
					MPGlobals.XpAnims[index].animPhase = PHASE_FADE_OUT
				ENDIF
			ENDIF
		ENDIF
	ELSE
		MPGlobals.XpAnims[index].fLastKnownX = 0.5
		MPGlobals.XpAnims[index].fLastKnownY = 0.8
		fXPos = MPGlobals.XpAnims[index].fLastKnownX
		fYPos = MPGlobals.XpAnims[index].fLastKnownY
		MPGlobals.XpAnims[index].bDrawIn2DSpace = TRUE
		
		IF MPGlobals.XpAnims[index].animPhase < PHASE_FADE_OUT AND NOT bPerformDelay
			PRINTLN("[JA@XP] RENDER_XP] - MPGlobals.XpAnims[index].animPhase < PHASE_FADE_OUT")
			MPGlobals.XpAnims[index].animPhase = PHASE_FADE_OUT
		ENDIF
		
		PRINTLN("[JA@XP] RENDER_XP] - Doing FPS XP STUFF ")
	ENDIF
	
	// If we are not drawing on the screen, set origin to entity
	IF NOT MPGlobals.XpAnims[index].bDrawIn2DSpace
		SET_DRAW_ORIGIN(vRenderOrigin)
	ELSE
		IF MPGlobals.XpAnims[index].bDoFirstPersonXP = TRUE
			SET_DRAW_ORIGIN(vRenderOrigin)
		ENDIF
		textPlacement.x = MPGlobals.XpAnims[index].fLastKnownX
		textPlacement.y = MPGlobals.XpAnims[index].fLastKnownY
	ENDIF
	FLOAT ammoW
	FLOAT ammoH
	FLOAT fLocX 
	FLOAT fLocY
	eTextJustification textJustify
	FLOAT fStringW,fStringH
	SWITCH MPGlobals.XpAnims[index].animType
		CASE HUD_ANIM_XP 
		CASE HUD_ANIM_CASH_PLAYER
		CASE HUD_ANIM_CASH
		CASE HUD_ANIM_AMMO
		CASE HUD_ANIM_WITH_LABEL	//"XP_HEADSHOT"
		
			REQUEST_STREAMED_TEXTURE_DICT("MPHud")
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPHud")
				SET_TEXT_STYLE(textStyle)
				IF NOT IS_STRING_NULL_OR_EMPTY(MPGlobals.XpAnims[index].tlMainText)
					BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(MPGlobals.XpAnims[index].tlMainText)
						ADD_TEXT_COMPONENT_INTEGER(MPGlobals.XpAnims[index].iXPValue)	
					fStringW = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
				ELSE
					BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("XP_REWARD")
						ADD_TEXT_COMPONENT_INTEGER(MPGlobals.XpAnims[index].iXPValue)	
					fStringW = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
				ENDIF
				
				fStringH = GET_RENDERED_CHARACTER_HEIGHT(textStyle.YScale,textStyle.aFont)
				
				GET_AMMO_SPRITE_SIZES(ammoW, ammoH,0.750,textStyle)
				fLocX = 0
				fLocY = 0
//				ammoW = ammoW*(fScaleX/fXPBaseScaleX)
//				ammoH = ammoH*(fScaleY/fXPBaseScaleY)
				#IF IS_DEBUG_BUILD
				//IF debugXPAnim.bUsedWidgetValues
				debugXPAnim.fXIconLocRO = fLocX
				debugXPAnim.fYIconLocRO = fLocY
				debugXPAnim.fIconHeightRO =	ammoH
				debugXPAnim.fIconWidthRO = ammoW
				//ENDIF
				IF debugXPAnim.bDisplayDebug
					PRINTLN("RENDER_XP: string size: width: ", fStringW, " height: ",fStringH)
					PRINTLN("RENDER_XP: Drawing sprite at X: ",fLocX, " Y:", fLocY, " W:", ammoW, " H:", ammoH)
					PRINTLN("RENDER_XP: Actual X: ",fXPos + fLocX, " Y:", fYPos + fLocY)
				ENDIF
				#ENDIF
				
				IF MPGlobals.XpAnims[index].animType = HUD_ANIM_XP 
					IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING
					//OR IS_PLAYER_RIDING_ROLLERCOASTER()
					
					ELSE
						DRAW_SPRITE("MPHud", "mp_anim_rp", fLocX, fLocY, ammoW * 0.66, ammoH * 0.66, 0, 255, 255, 255, textStyle.a)
					ENDIF
				ELIF MPGlobals.XpAnims[index].animType = HUD_ANIM_CASH_PLAYER 
				OR MPGlobals.XpAnims[index].animType = HUD_ANIM_CASH
					IF MPGlobals.XpAnims[index].bDoFirstPersonXP = TRUE
						PRINTLN("[JA@XP] RENDER_XP] bDoFirstPersonXP for MONEY ")
						DRAW_SPRITE("MPHud", "mp_anim_cash", 0.5, 0.8, ammoW * 0.66, ammoH * 0.66, 0, 255, 255, 255, textStyle.a)
					ELSE
						DRAW_SPRITE("MPHud", "mp_anim_cash", fLocX, fLocY, ammoW * 0.66, ammoH * 0.66, 0, 255, 255, 255, textStyle.a)
					ENDIF
				ELIF MPGlobals.XpAnims[index].animType = HUD_ANIM_AMMO
					IF MPGlobals.XpAnims[index].bDoFirstPersonXP = TRUE
						PRINTLN("[JA@XP] RENDER_XP] bDoFirstPersonXP for AMMO ")
						DRAW_SPRITE("MPHud", "mp_anim_ammo", 0.5, 0.8, ammoW * 0.66, ammoH * 0.66, 0, 255, 255, 255, textStyle.a)
					ELSE
						DRAW_SPRITE("MPHud", "mp_anim_ammo", fLocX, fLocY, ammoW * 0.66, ammoH * 0.66, 0, 255, 255, 255, textStyle.a)
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF debugXPAnim.bUsedWidgetValues
					textPlacement.x = debugXPAnim.fTextIconOffsetMultiplier*ammoW + debugXPAnim.fTextWidthOffsetMultiplier*fStringW + debugXPAnim.fTextIconEmptySpaceOffsetMultiplier*ammoW
					textPlacement.y = debugXPAnim.fTextHeightOffsetMultiplier*fStringH + debugXPAnim.fTextIconHeightOffsetMultiplier*ammoH
					textJustify = INT_TO_ENUM(eTextJustification,debugXPAnim.iJustification)
				ELSE
				#ENDIF
				
				IF MPGlobals.XpAnims[index].animType = HUD_ANIM_CASH_PLAYER 
				OR MPGlobals.XpAnims[index].animType = HUD_ANIM_CASH
					PRINTLN("RENDER_XP: sMPGlobals.XpAnims[index].animType = HUD_ANIM_CASH_PLAYER")
					PRINTLN("RENDER_XP: sMPGlobals.XpAnims[index].animType = HUD_ANIM_CASH")
					FLOAT fRENDER_XP_Cash_Text_Width
					fRENDER_XP_Cash_Text_Width = fRENDER_XP_Cash_Text_Width_100
					IF MPGlobals.XpAnims[index].iXPValue < 10
						IF MPGlobals.XpAnims[index].iXPValue = 1
							fRENDER_XP_Cash_Text_Width = fRENDER_XP_Cash_Text_Width_1
						ELSE
							fRENDER_XP_Cash_Text_Width = fRENDER_XP_Cash_Text_Width_2_to_9
						ENDIF
					ELIF MPGlobals.XpAnims[index].iXPValue < 100
						fRENDER_XP_Cash_Text_Width = fRENDER_XP_Cash_Text_Width_10
					ELSE
						fRENDER_XP_Cash_Text_Width = fRENDER_XP_Cash_Text_Width_100
					ENDIF
					
					IF MPGlobals.XpAnims[index].bDoFirstPersonXP = FALSE
						textPlacement.x = -0.5*ammoW + (fRENDER_XP_Cash_Text_Width*fStringW)	
						textPlacement.y = 0.02*fStringH + (-0.5*ammoH)
					ENDIF
				ELSE
					IF MPGlobals.XpAnims[index].animType = HUD_ANIM_AMMO
						PRINTLN("RENDER_XP: sMPGlobals.XpAnims[index].animType = HUD_ANIM_AMMO")
						FLOAT fRENDER_XP_Ammo_Text_Width
						fRENDER_XP_Ammo_Text_Width = fRENDER_XP_Ammo_Text_Width_100
						IF MPGlobals.XpAnims[index].iXPValue < 10
							IF MPGlobals.XpAnims[index].iXPValue = 1
								fRENDER_XP_Ammo_Text_Width = fRENDER_XP_Ammo_Text_Width_1
							ELSE
								fRENDER_XP_Ammo_Text_Width = fRENDER_XP_Ammo_Text_Width_2_to_9
							ENDIF
						ELIF MPGlobals.XpAnims[index].iXPValue < 100
							fRENDER_XP_Ammo_Text_Width = fRENDER_XP_Ammo_Text_Width_10
						ELSE
							fRENDER_XP_Ammo_Text_Width = fRENDER_XP_Ammo_Text_Width_100
						ENDIF
						
						IF MPGlobals.XpAnims[index].bDoFirstPersonXP = FALSE
							textPlacement.x = -0.5*ammoW + (fRENDER_XP_Ammo_Text_Width*fStringW)	
							textPlacement.y = 0.02*fStringH + (-0.5*ammoH)
						ENDIF
					ELSE
						IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING
						OR IS_PLAYER_RIDING_ROLLERCOASTER()
							textPlacement.x = -0.5*ammoW + (-0.45*fStringW)	
							textPlacement.y = 0.02*fStringH + (-0.5*ammoH)
						ELSE
							textPlacement.x = -0.5*ammoW + (-0.45*fStringW)	
							textPlacement.y = 0.02*fStringH + (-0.5*ammoH)
						ENDIF
					ENDIF
				ENDIF

				textJustify = FONT_CENTRE
				
				#IF IS_DEBUG_BUILD
				ENDIF
				#ENDIF
				
				#IF IS_DEBUG_BUILD
				debugXPAnim.fXTextLocRO = textPlacement.x
				debugXPAnim.fYTextLocRO = textPlacement.y
				debugXPAnim.fTextHeightRO = textStyle.YScale
				debugXPAnim.fTextWidthRO = textStyle.XScale
				IF debugXPAnim.bDisplayDebug	
					PRINTLN("RENDER_XP: textStyle.WrapEndX: ",textStyle.WrapEndX)
					PRINTLN("RENDER_XP: Drawing text at X: ",textPlacement.x, " Y:", textPlacement.y, " W:", textStyle.XScale, " H:", textStyle.YScale," Justification:",textJustify)
					PRINTLN("RENDER_XP: Actual X: ",fXPos + textPlacement.x, " Y:", fYPos + textPlacement.y)
				ENDIF
				#ENDIF
				IF NOT IS_STRING_NULL_OR_EMPTY(MPGlobals.XpAnims[index].tlMainText)
					DRAW_TEXT_WITH_NUMBER(textPlacement, textStyle, MPGlobals.XpAnims[index].tlMainText, MPGlobals.XpAnims[index].iXPValue, textJustify)
				ELSE
					IF MPGlobals.XpAnims[index].animType = HUD_ANIM_XP 
						DRAW_TEXT_WITH_NUMBER(textPlacement, textStyle, "XP_REWARD", MPGlobals.XpAnims[index].iXPValue, textJustify)
					ELIF MPGlobals.XpAnims[index].animType = HUD_ANIM_CASH_PLAYER 
					OR MPGlobals.XpAnims[index].animType = HUD_ANIM_CASH
						DRAW_TEXT_WITH_NUMBER(textPlacement, textStyle, "CASH_REWARD", MPGlobals.XpAnims[index].iXPValue, textJustify)
					ELIF MPGlobals.XpAnims[index].animType = HUD_ANIM_AMMO
						DRAW_TEXT_WITH_NUMBER(textPlacement, textStyle, "AMMO_REWARD", MPGlobals.XpAnims[index].iXPValue, textJustify)
					ENDIF
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
			
		BREAK
//		CASE HUD_ANIM_AMMO
//		
//			REQUEST_STREAMED_TEXTURE_DICT("MPHud")
//			IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPHud")
//				GET_AMMO_SPRITE_SIZES( ammoW, ammoH,1.0,textStyle)
//				DRAW_SPRITE("MPHud", "Ammo_Pickup", -ammoW, 0.66*ammoH, ammoW, ammoH, 0, 255, 255, 255, textStyle.a)
//			ELSE
//				RETURN FALSE
//			ENDIF
//			
//			textPlacement.x = 0.006
//			
//			DRAW_TEXT_WITH_NUMBER(textPlacement, textStyle, "AMMO_REWARD", MPGlobals.XpAnims[index].iXPValue, FONT_CENTRE)
//		BREAK
		CASE HUD_ANIM_PICKUP
			IF MPGlobals.XpAnims[index].fFloatToShow > 0
				DRAW_TEXT_WITH_FLOAT(textPlacement, textStyle, MPGlobals.XpAnims[index].tlTextToDisplay, MPGlobals.XpAnims[index].fFloatToShow, 2, FONT_CENTRE)
			ELSE
				DRAW_TEXT_WITH_ALIGNMENT(textPlacement, textStyle, MPGlobals.XpAnims[index].tlTextToDisplay, TRUE, FALSE)
			ENDIF
		BREAK
		CASE HUD_ANIM_PICKUP_INT
			IF MPGlobals.XpAnims[index].fFloatToShow > 0
				DRAW_TEXT_WITH_NUMBER(textPlacement, textStyle, MPGlobals.XpAnims[index].tlTextToDisplay, ROUND(MPGlobals.XpAnims[index].fFloatToShow), FONT_CENTRE)
			ELSE
				DRAW_TEXT_WITH_ALIGNMENT(textPlacement, textStyle, MPGlobals.XpAnims[index].tlTextToDisplay, TRUE, FALSE)
			ENDIF
		BREAK
		//CASE HUD_ANIM_CASH
		//CASE HUD_ANIM_CASH_PLAYER
		//	DRAW_TEXT_WITH_NUMBER(textPlacement, textStyle, "CASH_REWARD", MPGlobals.XpAnims[index].iXPValue, FONT_CENTRE)
		//BREAK
		CASE HUD_ANIM_XP_WITH_NAME
			IF IS_SAFE_TO_DRAW_ON_SCREEN()
				SET_TEXT_STYLE(textStyle)	
				SET_TEXT_JUSTIFICATION(FONT_CENTRE)
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT("XP_NAME_REWARD")
					ADD_TEXT_COMPONENT_INTEGER(MPGlobals.XpAnims[index].iXPValue)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(MPGlobals.XpAnims[index].tlTextToDisplay)
				END_TEXT_COMMAND_DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(textPlacement.x), ADJUST_SCREEN_Y_POSITION(textPlacement.y))
			ENDIF
		BREAK
		CASE HUD_ANIM_WITH_NAME
			IF IS_SAFE_TO_DRAW_ON_SCREEN()
				SET_TEXT_STYLE(textStyle)	
				SET_TEXT_JUSTIFICATION(FONT_CENTRE)
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT("XP_NAME")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(MPGlobals.XpAnims[index].tlTextToDisplay)
				END_TEXT_COMMAND_DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(textPlacement.x), ADJUST_SCREEN_Y_POSITION(textPlacement.y))
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Process enlarged rendering of XP anim (140%) 
FUNC BOOL PROCESS_XP_ANIMATION_140(INT index)
	BOOL bPerformDelay = FALSE
	TEXT_PLACEMENT txtPlacement
	TEXT_STYLE txtStyle
	txtStyle.aFont = FONT_CONDENSED_NOT_GAMERNAME
	txtStyle.XScale = fXPScaleX 
	txtStyle.YScale = fXPScaleY
	txtStyle.r = 255
	txtStyle.g = 255
	txtStyle.b = 255
	txtStyle.a = 255
	txtStyle.drop = DROPSTYLE_ALL
	txtStyle.WrapEndX = 0.872
	
	#IF IS_DEBUG_BUILD
		IF bXpAnimAboveHead
			MPGlobals.XpAnims[index].vEntityOrigin = CALC_Z_OFFSET(PLAYER_PED_ID())
		ENDIF
	#ENDIF
	
	IF MPGlobals.XpAnims[index].animType = HUD_ANIM_PICKUP
	OR MPGlobals.XpAnims[index].animType = HUD_ANIM_AMMO
//	OR MPGlobals.XpAnims[index].animType = HUD_ANIM_CASH_PLAYER
	OR MPGlobals.XpAnims[index].bTrackEntity
		MPGlobals.XpAnims[index].vEntityOrigin = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE) //CALC_Z_OFFSET(PLAYER_PED_ID(), FALSE)
		//MPGlobals.XpAnims[index].vEntityOrigin.z -= 0.65
	ENDIF
	
	IF MPGlobals.XpAnims[index].animType = HUD_ANIM_XP
	AND GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
		MPGlobals.XpAnims[index].vEntityOrigin = CALC_Z_OFFSET(PLAYER_PED_ID())
		bPerformDelay = TRUE
	ENDIF
	
	// Calculate the scale
//	CALC_XP_SCALE(MPGlobals.XpAnims[index].vEntityOrigin, fXPScaleX, fXPScaleY)
//	txtStyle.XScale = fXPScaleX
//	txtStyle.YScale = fXPScaleY
	
	// Make 140%
	txtStyle.XScale *= 1.4
	txtStyle.YScale *= 1.4
	
	RETURN RENDER_XP(index, MPGlobals.XpAnims[index].vEntityOrigin, txtPlacement, txtStyle, bPerformDelay)
ENDFUNC

/// PURPOSE:
///    Process standard rendering of XP anim (100%) 
FUNC BOOL PROCESS_XP_ANIMATION_100(INT index)
	TEXT_PLACEMENT txtPlacement
	TEXT_STYLE txtStyle
	txtStyle.aFont = FONT_CONDENSED_NOT_GAMERNAME 
	txtStyle.XScale = fXPScaleX 
	txtStyle.YScale = fXPScaleY
	txtStyle.r = 255
	txtStyle.g = 255
	txtStyle.b = 255
	txtStyle.a = 255
	txtStyle.drop = DROPSTYLE_ALL
	txtStyle.WrapEndX = 0.872
	
	#IF IS_DEBUG_BUILD
		IF bXpAnimAboveHead
			MPGlobals.XpAnims[index].vEntityOrigin = CALC_Z_OFFSET(PLAYER_PED_ID())
		ENDIF
	#ENDIF
	
	IF MPGlobals.XpAnims[index].animType = HUD_ANIM_PICKUP
	OR MPGlobals.XpAnims[index].animType = HUD_ANIM_AMMO
//	OR MPGlobals.XpAnims[index].animType = HUD_ANIM_CASH_PLAYER
	OR MPGlobals.XpAnims[index].bTrackEntity
		MPGlobals.XpAnims[index].vEntityOrigin = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE) //CALC_Z_OFFSET(PLAYER_PED_ID(), FALSE)
		//MPGlobals.XpAnims[index].vEntityOrigin.z -= 0.65
	ENDIF
	
	// Calculate the scale
//	CALC_XP_SCALE(MPGlobals.XpAnims[index].vEntityOrigin, fXPScaleX, fXPScaleY)
//	txtStyle.XScale = fXPScaleX
//	txtStyle.YScale = fXPScaleY
	
	RETURN RENDER_XP(index, MPGlobals.XpAnims[index].vEntityOrigin, txtPlacement, txtStyle)
ENDFUNC



FLOAT fEasing = 0.001
CONST_FLOAT fEasingTweak 0.0001
CONST_FLOAT fShowerXPZTweak -0.5
CONST_FLOAT fParaChuteTweak -2.0
CONST_FLOAT fEasingLimit 1.0

CONST_FLOAT fRollercoasterTweak -1.3

SCRIPT_TIMER holdAlphaTimer
/// PURPOSE:
///    Process standard rendering of XP anim (100%) 
FUNC BOOL PROCESS_XP_ANIMATION_100_MOVE_UP_AND_FADE_OUT(INT index)
	TEXT_PLACEMENT txtPlacement
	TEXT_STYLE txtStyle
	txtStyle.aFont = FONT_CONDENSED_NOT_GAMERNAME
	txtStyle.XScale = fXPScaleX * 1.4
	txtStyle.YScale = fXPScaleY * 1.4
	txtStyle.r = 255
	txtStyle.g = 255
	txtStyle.b = 255
	txtStyle.a = 255
	txtStyle.drop = DROPSTYLE_ALL
	txtStyle.WrapEndX = 0.872
	
	PRINTLN("[AW_NET_XP] I'M IN HERE - 1")
	
	#IF IS_DEBUG_BUILD
		IF bXpAnimAboveHead
			MPGlobals.XpAnims[index].vEntityOrigin = CALC_Z_OFFSET(PLAYER_PED_ID())
		ENDIF
	#ENDIF
	
	IF MPGlobals.XpAnims[index].animType = HUD_ANIM_PICKUP
	OR MPGlobals.XpAnims[index].animType = HUD_ANIM_AMMO
	OR MPGlobals.XpAnims[index].animType = HUD_ANIM_CASH_PLAYER
	OR MPGlobals.XpAnims[index].bTrackEntity
		//MPGlobals.XpAnims[index].vEntityOrigin = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE) //CALC_Z_OFFSET(PLAYER_PED_ID(), FALSE)
		//MPGlobals.XpAnims[index].vEntityOrigin.z -= 0.65
	ENDIF
	
	IF MPGlobals.XpAnims[index].bTrackEntity
		IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING
			MPGlobals.XpAnims[index].vEntityOrigin = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE) //CALC_Z_OFFSET(PLAYER_PED_ID(), FALSE)
			MPGlobals.XpAnims[index].vEntityOrigin.z -= fParaChuteTweak
		ENDIF
	ENDIF
	
	IF MPGlobals.XpAnims[index].bTrackEntity
		PRINTLN("[AW_NET_XP] I'M IN HERE - 2")
		IF IS_PLAYER_RIDING_ROLLERCOASTER()
			//MPGlobals.XpAnims[index].bTrackEntity = TRUE
			PRINTLN("[AW_NET_XP] PLAYER USING ROLLERCOASTER - Z TWEAK")
			MPGlobals.XpAnims[index].vEntityOrigin = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE) //CALC_Z_OFFSET(PLAYER_PED_ID(), FALSE)
			MPGlobals.XpAnims[index].vEntityOrigin.z -= fRollercoasterTweak
		ELSE
			PRINTLN("[AW_NET_XP] I'M IN HERE - 4")
		ENDIF
	ELSE
		PRINTLN("[AW_NET_XP] I'M IN HERE - 3")
	ENDIF

	IF iPersonalAptActivity = ci_APT_ACT_SHOWER
		PRINTLN("NET_XP_ANIMATION - iPersonalAptActivity = ci_APT_ACT_SHOWER")
		MPGlobals.XpAnims[index].vEntityOrigin = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE) //CALC_Z_OFFSET(PLAYER_PED_ID(), FALSE)
		MPGlobals.XpAnims[index].vEntityOrigin.z -= fShowerXPZTweak
	ENDIF
	
	IF MPGlobals.XpAnims[index].animType = HUD_ANIM_AMMO
		PRINTLN("NET_XP_ANIMATION - MPGlobals.XpAnims[index].animType = HUD_ANIM_AMMO")
		MPGlobals.XpAnims[index].vEntityOrigin = CALC_Z_OFFSET(PLAYER_PED_ID()) //CALC_Z_OFFSET(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING
		PRINTLN("NET_XP_ANIMATION - iPersonalAptActivity = ci_APT_ACT_PARACHUTING")
//      SET_XP_ANIM_TO_TRACK_PLAYER(TRUE)
//		MPGlobals.XpAnims[index].vEntityOrigin = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE) //CALC_Z_OFFSET(PLAYER_PED_ID(), FALSE)
//		MPGlobals.XpAnims[index].vEntityOrigin.z -= fParaChuteTweak
	ENDIF
	
	IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
		MPGlobals.XpAnims[index].vEntityOrigin = CALC_Z_OFFSET(PLAYER_PED_ID())
		PRINTLN("NET_XP_ANIMATION - FIRST PERSON MODE - MPGlobals.XpAnims[index].vEntityOrigin: ", MPGlobals.XpAnims[index].vEntityOrigin)
	ENDIF
	
	// Calculate the scale
//	FLOAT fScale = CALC_XP_SCALE(MPGlobals.XpAnims[index].vEntityOrigin, fXPScaleX, fXPScaleY)
//	txtStyle.XScale = fXPScaleX
//	txtStyle.YScale = fXPScaleY
	
	FLOAT fMoveUpDist = MPGlobals.XpAnims[index].fFloatScale * (XP_MOVE_UP_DIST_MAX - XP_MOVE_UP_DIST_MIN)
	IF fMoveUpDist > XP_MOVE_UP_DIST_MIN
	ELSE	
		fMoveUpDist = XP_MOVE_UP_DIST_MIN
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bXpAnimAboveHead
			IF IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
				fMoveUpDist = 0.4
			ENDIF
		ENDIF
	#ENDIF
	
	INT iDeltaT = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.XpAnims[index].iPhaseStartTime))
	
	// Update Z position
	FLOAT fSpeed = fMoveUpDist / XP_PHASE_FADE_DIST * MPGlobals.XpAnims[index].fEasing * (XP_MOVE_UP_DIST_MAX - XP_MOVE_UP_DIST_MIN) //new
	
	
	IF fEasing < fEasingLimit
		PRINTLN("NET_XP_ANIMATION - fEasing: ")PRINTLN(MPGlobals.XpAnims[index].fEasing)
		MPGlobals.XpAnims[index].fEasing += fEasingTweak/10 + MPGlobals.XpAnims[index].fEasing
	ENDIF
	
	VECTOR vOrigin = MPGlobals.XpAnims[index].vEntityOrigin + <<0,0,(fSpeed * iDeltaT)>>

	
	PRINTLN("NET_XP_ANIMATION - fSpeed:")PRINTLN(fSpeed)PRINTNL()
	PRINTLN("NET_XP_ANIMATION - vOrigin:")PRINTLN(vOrigin)PRINTNL()
	PRINTLN("NET_XP_ANIMATION - fMoveUpDist: ")PRINTLN(MPGlobals.XpAnims[index].fEasing)PRINTNL()
	
//	// Update Z position
//	FLOAT fSpeed = fMoveUpDist / XP_PHASE_FADE_DIST		//XP_PHASE_100_MOVE_UP_AND_FADE_OUT_TIME
//	VECTOR vOrigin = MPGlobals.XpAnims[index].vEntityOrigin + <<0,0,(fSpeed * iDeltaT)>>
//	
	IF MPGlobals.XpAnims[index].bDrawIn2DSpace
		MPGlobals.XpAnims[index].fLastKnownY -= XP_MOVE_UP_DIST_2D
	ENDIF
	
	FLOAT fAlphaMult
	
	IF HAS_NET_TIMER_STARTED(holdAlphaTimer)
		IF HAS_NET_TIMER_EXPIRED(holdAlphaTimer, XP_PHASE_100_MOVE_UP_HOLD_AT_100_ALPHA)
			// Update alpha value
			FLOAT fAlpha = (TO_FLOAT(iDeltaT) / XP_PHASE_100_MOVE_UP_AND_FADE_OUT_TIME)	//XP_PHASE_FADE_TIME
			fAlphaMult = (fAlpha-0.5) * 2
			IF (fAlphaMult < 0)
				fAlphaMult = 0
			ENDIF
			IF (fAlphaMult <= 0)
				RESET_NET_TIMER(holdAlphaTimer) 
			ENDIF
		ENDIF
	ELSE
		START_NET_TIMER(holdAlphaTimer,TRUE)
	ENDIF
	
	txtStyle.a = ROUND(255 * (1 - fAlphaMult))
	IF (txtStyle.a < 0)
		txtStyle.a = 0
	ENDIF
	
	RETURN RENDER_XP(index, vOrigin, txtPlacement, txtStyle)
ENDFUNC


/// PURPOSE:
///    Process rendering of XP upwards with a fade 
FUNC BOOL PROCESS_XP_MOVE_UP_AND_FADE_OUT(INT index)
	TEXT_PLACEMENT txtPlacement
	TEXT_STYLE txtStyle
	txtStyle.aFont = FONT_CONDENSED_NOT_GAMERNAME
	txtStyle.XScale = fXPScaleX 
	txtStyle.YScale = fXPScaleY
	txtStyle.r = 255
	txtStyle.g = 255
	txtStyle.b = 255
	txtStyle.a = 255
	txtStyle.drop = DROPSTYLE_ALL
	txtStyle.WrapEndX = 0.872
	
	#IF IS_DEBUG_BUILD
		IF bXpAnimAboveHead
			MPGlobals.XpAnims[index].vEntityOrigin = CALC_Z_OFFSET(PLAYER_PED_ID())
		ENDIF
	#ENDIF
	
	IF MPGlobals.XpAnims[index].animType = HUD_ANIM_PICKUP
	OR MPGlobals.XpAnims[index].animType = HUD_ANIM_AMMO
//	OR MPGlobals.XpAnims[index].animType = HUD_ANIM_CASH_PLAYER
	OR MPGlobals.XpAnims[index].bTrackEntity
		MPGlobals.XpAnims[index].vEntityOrigin = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE) //CALC_Z_OFFSET(PLAYER_PED_ID(), FALSE)
		//MPGlobals.XpAnims[index].vEntityOrigin.z -= 0.65
		fEasing = 0
	ENDIF
	
	// Calculate the scale
//	FLOAT fScale = CALC_XP_SCALE(MPGlobals.XpAnims[index].vEntityOrigin, fXPScaleX, fXPScaleY)
//	txtStyle.XScale = fXPScaleX
//	txtStyle.YScale = fXPScaleY
	
	FLOAT fMoveUpDist = MPGlobals.XpAnims[index].fFloatScale * (XP_MOVE_UP_DIST_MAX - XP_MOVE_UP_DIST_MIN)
	fMoveUpDist += XP_MOVE_UP_DIST_MIN
	
	#IF IS_DEBUG_BUILD
		IF bXpAnimAboveHead
			IF IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
				fMoveUpDist = 0.4
			ENDIF
		ENDIF
	#ENDIF
	
	INT iDeltaT = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.XpAnims[index].iPhaseStartTime))
	
	// Update Z position
	FLOAT fSpeed = fMoveUpDist / XP_PHASE_FADE_DIST		//XP_PHASE_100_MOVE_UP_AND_FADE_OUT_TIME
	VECTOR vOrigin = MPGlobals.XpAnims[index].vEntityOrigin + <<0,0,(fSpeed * iDeltaT)>>
	
	PRINTLN("NET_XP_ANIMATION - vOrigin:")PRINTLN(vOrigin)PRINTNL()
	
	IF MPGlobals.XpAnims[index].bDrawIn2DSpace
		MPGlobals.XpAnims[index].fLastKnownY -= XP_MOVE_UP_DIST_2D
	ENDIF
	
	// Update alpha value
	txtStyle.a = ROUND(255 * (1 - (TO_FLOAT(iDeltaT) / XP_PHASE_FADE_TIME)))
	IF txtStyle.a < 0
		txtStyle.a = 0
	ENDIF
	
	RETURN RENDER_XP(index, vOrigin, txtPlacement, txtStyle)
ENDFUNC

/// PURPOSE:
///    Returns phase times based on TODO 499868
FUNC INT GET_PHASE_TIME(XP_ANIM_PHASE animPhase, INT iMultiplier)

	SWITCH animPhase
	
		CASE PHASE_ANIMATE_140	RETURN (XP_PHASE_140_TIME * iMultiplier)
		CASE PHASE_ANIMATE_100	RETURN (XP_PHASE_100_TIME * iMultiplier)
		CASE PHASE_FADE_OUT		RETURN XP_PHASE_FADE_TIME
		
		CASE PHASE_ANIMATE_100_MOVE_UP_AND_FADE_OUT	RETURN XP_PHASE_100_MOVE_UP_AND_FADE_OUT_TIME
	
	ENDSWITCH

//	#IF IS_DEBUG_BUILD
//		NET_PRINT("[JA@XP] GET_PHASE_TIME - called on animation phase that is not timed (e.g. COMPLETE / NULL)") NET_NL()
//	#ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Check current phase and time that has passed. Moves to next phase if time is up. 
PROC CHECK_PHASE_TIME(INT index)
	#IF IS_DEBUG_BUILD
	IF debugXPAnim.bDisplayDebug
		PRINTLN("NET_XP_ANIMATION - CHECK_PHASE_TIME MPGlobals.XpAnims[",index,"].iPhaseStartTime = ",GET_TIME_AS_STRING(MPGlobals.XpAnims[index].iPhaseStartTime))
		PRINTLN("NET_XP_ANIMATION - CHECK_PHASE_TIME GET_PHASE_TIME(MPGlobals.XpAnims[",index,"].animPhase, MPGlobals.XpAnims[",index,"].iPhaseMultiplier)) = ",GET_PHASE_TIME(MPGlobals.XpAnims[index].animPhase, MPGlobals.XpAnims[index].iPhaseMultiplier))
		PRINTLN("NET_XP_ANIMATION - CHECK_PHASE_TIME GET_TIME_OFFSET = ",GET_TIME_AS_STRING(GET_TIME_OFFSET(MPGlobals.XpAnims[index].iPhaseStartTime, GET_PHASE_TIME(MPGlobals.XpAnims[index].animPhase, MPGlobals.XpAnims[index].iPhaseMultiplier))))
		PRINTLN("NET_XP_ANIMATION - CHECK_PHASE_TIME GET_NETWORK_TIME() = ",GET_TIME_AS_STRING(GET_NETWORK_TIME()))
	ENDIF
	#ENDIF
	IF IS_TIME_LESS_THAN(GET_TIME_OFFSET(MPGlobals.XpAnims[index].iPhaseStartTime, GET_PHASE_TIME(MPGlobals.XpAnims[index].animPhase, MPGlobals.XpAnims[index].iPhaseMultiplier)), GET_NETWORK_TIME())
		INT iPhaseEnum = ENUM_TO_INT(MPGlobals.XpAnims[index].animPhase)
		iPhaseEnum++
		
		MPGlobals.XpAnims[index].animPhase = INT_TO_ENUM(XP_ANIM_PHASE, iPhaseEnum)
		MPGlobals.XpAnims[index].iPhaseStartTime = GET_NETWORK_TIME()		
	ENDIF
	
	// If we fade out then clean up all animations as start afresh when respawn
	IF IS_SCREEN_FADED_OUT()
		MPGlobals.XpAnims[index].animPhase = PHASE_COMPLETE
	ENDIF
ENDPROC


// --------------------- MAIN SYSTEM LOOP --------------------

PROC CLEANUP_XP_ANIM(INT i)
	XP_ANIM emptyXPAnim
	MPGlobals.XpAnims[i] = emptyXPAnim
ENDPROC

PROC CLEANUP_ALL_XP_ANIM()
	
	INT i
	REPEAT NUM_XP_ANIMS i
		CLEANUP_XP_ANIM(i)
	ENDREPEAT

ENDPROC



/// PURPOSE:
/// 	Called every frame to maintain active XP anims.
PROC MAINTAIN_XP_ANIMS()

	INT i
	
	BOOL bAnyXPAnimsActive = FALSE
	
	
	
	// Loop over all anims, switching based on that state
	REPEAT NUM_XP_ANIMS i
		#IF IS_DEBUG_BUILD
		IF debugXPAnim.bDisplayDebug	
			PRINTLN("MAINTAIN_XP_ANIMS: MPGlobals.XpAnims[",i,"].animPhase: ",MPGlobals.XpAnims[i].animPhase)
		ENDIF
		#ENDIF
		SWITCH MPGlobals.XpAnims[i].animPhase
			
			// Draw the XP text above the entity
			CASE PHASE_READY
			
				IF NOT MPGlobals.g_bXPAnimationActive
					
					PRINTLN("TRY TO DISPLAY ANOTHER ANIMATION")
				
					IF IS_SAFE_TO_SHOW_XP_ANIM()
						
					//	//use 100 not 140 here
					//	PROCESS_XP_ANIMATION_140(i)
					//	MPGlobals.XpAnims[i].iPhaseStartTime = GET_NETWORK_TIME()
					//	MPGlobals.XpAnims[i].animPhase = PHASE_ANIMATE_140
						IF NOT MPGlobals.XpAnims[i].bEasingSet
							MPGlobals.XpAnims[i].fEasing = fEasing
							MPGlobals.XpAnims[i].bEasingSet = TRUE
						ENDIF
						
						IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING
							PRINTLN("NET_XP_ANIMATION - MAINTAIN_XP_ANIMS = PARACHUTING - SET_XP_ANIM_TO_TRACK_PLAYER")
							SET_XP_ANIM_TO_TRACK_PLAYER(TRUE)
						ENDIF
						
						IF IS_PLAYER_RIDING_ROLLERCOASTER()
							PRINTLN("NET_XP_ANIMATION - MAINTAIN_XP_ANIMS = ROLLERCOASTER - SET_XP_ANIM_TO_TRACK_PLAYER")
							//SET_XP_ANIM_TO_TRACK_PLAYER(TRUE)
						ENDIF
						
						#IF IS_DEBUG_BUILD
						IF debugXPAnim.bDisplayDebug
							PRINTLN("NET_XP_ANIMATION - MAINTAIN_XP_ANIMS GET_NETWORK_TIME() = ",GET_TIME_AS_STRING(GET_NETWORK_TIME()))
							PRINTLN("NET_XP_ANIMATION - MAINTAIN_XP_ANIMS MPGlobals.XpAnims[",i,"].iDisplayDuration) = ",GET_TIME_AS_STRING(MPGlobals.XpAnims[i].iDisplayDuration))
						ENDIF
						#ENDIF
						// Delay
						IF NOT IS_TIME_LESS_THAN(GET_NETWORK_TIME(), MPGlobals.XpAnims[i].iDisplayDuration)
						
							PROCESS_XP_ANIMATION_100_MOVE_UP_AND_FADE_OUT(i)
							MPGlobals.XpAnims[i].iPhaseStartTime = GET_NETWORK_TIME()
							MPGlobals.XpAnims[i].animPhase = PHASE_ANIMATE_100_MOVE_UP_AND_FADE_OUT
							#IF IS_DEBUG_BUILD
							IF debugXPAnim.bDisplayDebug
								PRINTLN("NET_XP_ANIMATION - MAINTAIN_XP_ANIMS MPGlobals.XpAnims[",i,"].animPhase = PHASE_ANIMATE_100_MOVE_UP_AND_FADE_OUT")
							ENDIF
							#ENDIF
							// We currently have an animation being displayed (all others should be suppressed)
							MPGlobals.g_bXPAnimationActive = TRUE
							bAnyXPAnimsActive = TRUE
						
							CHECK_PHASE_TIME(i)
						ELSE
							MPGlobals.XpAnims[i].animPhase = PHASE_ANIMATE_140_STILL
							#IF IS_DEBUG_BUILD
							IF debugXPAnim.bDisplayDebug
								PRINTLN("NET_XP_ANIMATION - MAINTAIN_XP_ANIMS MPGlobals.XpAnims[",i,"].animPhase = PHASE_ANIMATE_140_STILL")
							ENDIF
							#ENDIF
						ENDIF
					ELSE
						MPGlobals.XpAnims[i].animPhase = PHASE_COMPLETE
					ENDIF
				ENDIF
			BREAK
			
			CASE PHASE_ANIMATE_140_STILL
			
				IF IS_SAFE_TO_SHOW_XP_ANIM()
					bAnyXPAnimsActive = TRUE
					
					PROCESS_XP_ANIMATION_140(i)
					
					IF NOT IS_TIME_LESS_THAN(GET_NETWORK_TIME(), MPGlobals.XpAnims[i].iDisplayDuration)
						MPGlobals.XpAnims[i].animPhase = PHASE_READY
					ENDIF
				
					#IF IS_DEBUG_BUILD
					IF debugXPAnim.bDisplayDebug
						PRINTLN("NET_XP_ANIMATION - MAINTAIN_XP_ANIMS PHASE_ANIMATE_140_STILL GET_NETWORK_TIME() = ",GET_TIME_AS_STRING(GET_NETWORK_TIME()))
						PRINTLN("NET_XP_ANIMATION - MAINTAIN_XP_ANIMS PHASE_ANIMATE_140_STILL MPGlobals.XpAnims[",i,"].iDisplayDuration) = ",GET_TIME_AS_STRING(MPGlobals.XpAnims[i].iDisplayDuration))
					ENDIF
					#ENDIF
				ELSE
					MPGlobals.XpAnims[i].animPhase = PHASE_COMPLETE
				ENDIF
			BREAK
			
			CASE PHASE_ANIMATE_140
			
				IF IS_SAFE_TO_SHOW_XP_ANIM()
					bAnyXPAnimsActive = TRUE
				
					PROCESS_XP_ANIMATION_140(i)
					CHECK_PHASE_TIME(i)
				ELSE
					MPGlobals.XpAnims[i].animPhase = PHASE_COMPLETE
				ENDIF
			BREAK
			
			CASE PHASE_ANIMATE_100

				IF IS_SAFE_TO_SHOW_XP_ANIM()
					bAnyXPAnimsActive = TRUE
			
					PROCESS_XP_ANIMATION_100(i)
					CHECK_PHASE_TIME(i)
				ELSE
					MPGlobals.XpAnims[i].animPhase = PHASE_COMPLETE
				ENDIF
			BREAK
			
			CASE PHASE_ANIMATE_100_MOVE_UP_AND_FADE_OUT

				IF IS_SAFE_TO_SHOW_XP_ANIM()
					bAnyXPAnimsActive = TRUE
					
					IF MPGlobals.XpAnims[i].animType = HUD_ANIM_WITH_LABEL	//"XP_HEADSHOT"
					OR MPGlobals.XpAnims[i].animType = HUD_ANIM_WITH_NAME
						
						PRINTLN("NET_XP_ANIMATION: HEADSHOT")
					
						//INT iDeltaT
						//FLOAT fAlpha
						
						//iDeltaT = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.XpAnims[i].iPhaseStartTime))
						//fAlpha = (TO_FLOAT(iDeltaT) / XP_PHASE_100_MOVE_UP_AND_FADE_OUT_TIME)
						
						
						
//						IF MPGlobals.XpAnims[i].animType = HUD_ANIM_WITH_LABEL
//							SAVE_STRING_TO_DEBUG_FILE("HUD_ANIM_WITH_LABEL - ")
//						
//							SAVE_STRING_TO_DEBUG_FILE("\"")
//							SAVE_STRING_TO_DEBUG_FILE(MPGlobals.XpAnims[i].tlMainText)
//							SAVE_STRING_TO_DEBUG_FILE("\": ")
//							
//						ELIF MPGlobals.XpAnims[i].animType = HUD_ANIM_WITH_NAME
//							SAVE_STRING_TO_DEBUG_FILE("HUD_ANIM_WITH_NAME - ")
//						
//							SAVE_STRING_TO_DEBUG_FILE("\"")
//							SAVE_STRING_TO_DEBUG_FILE(MPGlobals.XpAnims[i].tlTextToDisplay)
//							SAVE_STRING_TO_DEBUG_FILE("\": ")
//							
//						ELSE
//							SAVE_STRING_TO_DEBUG_FILE("HUD_ANIM_WITH_OTHER - ")
//						ENDIF
//						
//						SAVE_STRING_TO_DEBUG_FILE(", iDeltaT: ")
//						SAVE_INT_TO_DEBUG_FILE(iDeltaT)
//						SAVE_STRING_TO_DEBUG_FILE(", fAlpha: ")
//						SAVE_FLOAT_TO_DEBUG_FILE(fAlpha)
//						SAVE_NEWLINE_TO_DEBUG_FILE()
						
//						IF fAlpha > 0.33
//							IF MPGlobals.XpAnims[i].animType = HUD_ANIM_WITH_LABEL
//							AND ARE_STRINGS_EQUAL("XP_HEADSHOT", MPGlobals.XpAnims[i].tlMainText)
//								bAnyXPAnimsActive = FALSE
//							ENDIF
//							
//							IF MPGlobals.XpAnims[i].animType = HUD_ANIM_WITH_NAME
//							AND ARE_STRINGS_EQUAL("HEADSHOT", MPGlobals.XpAnims[i].tlTextToDisplay)
//								bAnyXPAnimsActive = FALSE
//							ENDIF
//						ENDIF
						
					ENDIF
					
					
					PROCESS_XP_ANIMATION_100_MOVE_UP_AND_FADE_OUT(i)
					CHECK_PHASE_TIME(i)
				ELSE
					MPGlobals.XpAnims[i].animPhase = PHASE_COMPLETE
				ENDIF
			BREAK
		
			CASE PHASE_FADE_OUT
			
				IF IS_SAFE_TO_SHOW_XP_ANIM()
					PROCESS_XP_MOVE_UP_AND_FADE_OUT(i)
					CHECK_PHASE_TIME(i)
				ELSE
					MPGlobals.XpAnims[i].animPhase = PHASE_COMPLETE
				ENDIF
			BREAK
		
			// Cleanup xp animation
			CASE PHASE_COMPLETE
				//REQUEST_STREAMED_TEXTURE_DICT("MPHud")
				//IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPHud")
					SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPHud")
				//ENDIF
				MPGlobals.XpAnims[i].bEasingSet = FALSE
			
				CLEANUP_XP_ANIM(i)
			BREAK
		
		ENDSWITCH
	
	ENDREPEAT
	
	IF bAnyXPAnimsActive != MPGlobals.g_bXPAnimationActive
		MPGlobals.g_bXPAnimationActive = bAnyXPAnimsActive
	ENDIF
	
	// Reset draw origin for any future rendering
	CLEAR_DRAW_ORIGIN()
ENDPROC

// ---------------- DEBUG WIDGETS ----------------
#IF IS_DEBUG_BUILD
	PROC CREATE_XP_WIDGETS()
		
		START_WIDGET_GROUP("XP Animation")
			
			ADD_WIDGET_BOOL("Disable Xp animation", bDisableXPAnimation)
			ADD_WIDGET_INT_SLIDER("Delay Time(MS)", iDisplayXPTime, -20000, 20000, 1)
			
			ADD_WIDGET_STRING("Edit the XP animation mode (if unticked default is running - above entities):")
			ADD_WIDGET_BOOL("Mode: Above Player's Head", bXpAnimAboveHead)
			ADD_WIDGET_BOOL("Mode: XYZ", bXpAnimXYZ)
			ADD_WIDGET_FLOAT_SLIDER("Mode XYZ - x-coord:", vXpAnimPt.x, -5000, 5000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Mode XYZ - y-coord:", vXpAnimPt.y, -5000, 5000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Mode XYZ - z-coord:", vXpAnimPt.z, -500, 5000, 0.1)
			
			ADD_WIDGET_STRING("Phase Variables")
			ADD_WIDGET_INT_SLIDER("Phase 140 Time", XP_PHASE_140_TIME, 0, 1000, 1)
			ADD_WIDGET_INT_SLIDER("Phase 100 Time", XP_PHASE_100_TIME, 0, 1000, 1)
			ADD_WIDGET_INT_SLIDER("Phase Move up and Fade Time", XP_PHASE_FADE_TIME, 0, 3000, 1)
			ADD_WIDGET_INT_SLIDER("Phase 100, move up and Fade Time", XP_PHASE_100_MOVE_UP_AND_FADE_OUT_TIME, 0, 3000, 1)
			ADD_WIDGET_FLOAT_SLIDER("Max move up distance", XP_MOVE_UP_DIST_MAX, 0, 20.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Min move up distance", XP_MOVE_UP_DIST_MIN, 0, 20.0, 0.001)
			
			ADD_WIDGET_STRING("Change ranges for scaling text")
			ADD_WIDGET_FLOAT_SLIDER("XP_MOVE_UP_DIST_2D", XP_MOVE_UP_DIST_2D, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("XP_ANIM_MIN_SCALEX", XP_ANIM_MIN_SCALEX, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("XP_ANIM_MAX_SCALEX", XP_ANIM_MAX_SCALEX, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("XP_ANIM_MIN_SCALEY", XP_ANIM_MIN_SCALEY, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("XP_ANIM_MAX_SCALEY", XP_ANIM_MAX_SCALEY, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("XP_ANIM_MIN_DIST", XP_ANIM_MIN_DIST, 0.0, 20.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("XP_ANIM_MAX_DIST", XP_ANIM_MAX_DIST, 0.0, 60.0, 0.1)
			ADD_WIDGET_INT_SLIDER("XP_PHASE_FADE_DIST", XP_PHASE_FADE_DIST, 0, 3000, 1)
			ADD_WIDGET_FLOAT_SLIDER("FIRST_PERSON_PICKUP_OFFSET_Y", FIRST_PERSON_PICKUP_OFFSET_Y, -10.0, 10.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("FIRST_PERSON_PICKUP_OFFSET_z", FIRST_PERSON_PICKUP_OFFSET_z, -10.0, 10.0, 0.001)
			
			ADD_WIDGET_STRING("READ ONLY - scales used")
			ADD_WIDGET_FLOAT_SLIDER("Text Scale X", fXPScaleX, 0.0, 10.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Text Scale Y", fXPScaleY, 0.0, 10.0, 0.001)
			//ADD_WIDGET_FLOAT_SLIDER("fAMMO_SPRITE_SIZES_Width", fAMMO_SPRITE_SIZES_Width, 0.0, 10.0, 0.0001)
			//ADD_WIDGET_FLOAT_SLIDER("fAMMO_SPRITE_SIZES_Height", fAMMO_SPRITE_SIZES_Height, 0.0, 10.0, 0.0001)
			
			ADD_WIDGET_STRING("fRENDER_XP_Cash_Text_Width")
			//ADD_WIDGET_FLOAT_SLIDER("Cash_Text_Width_1", fRENDER_XP_Cash_Text_Width_1, -10.0, 10.0, 0.0001)
			//ADD_WIDGET_FLOAT_SLIDER("Cash_Text_Width_2_to_9", fRENDER_XP_Cash_Text_Width_2_to_9, -10.0, 10.0, 0.0001)
			//ADD_WIDGET_FLOAT_SLIDER("Cash_Text_Width_10", fRENDER_XP_Cash_Text_Width_10, -10.0, 10.0, 0.0001)
			//ADD_WIDGET_FLOAT_SLIDER("Cash_Text_Width_100", fRENDER_XP_Cash_Text_Width_100, -10.0, 10.0, 0.0001)
			
			ADD_WIDGET_STRING("fRENDER_XP_Ammo_Text_Width")
			//ADD_WIDGET_FLOAT_SLIDER("Ammo_Width_1", fRENDER_XP_Ammo_Text_Width_1, -10.0, 10.0, 0.0001)
			//ADD_WIDGET_FLOAT_SLIDER("Ammo_Text_Width_2_to_9", fRENDER_XP_Ammo_Text_Width_2_to_9, -10.0, 10.0, 0.0001)
			//ADD_WIDGET_FLOAT_SLIDER("Ammo_Width_10", fRENDER_XP_Ammo_Text_Width_10, -10.0, 10.0, 0.0001)
			//ADD_WIDGET_FLOAT_SLIDER("Ammo_Width_100", fRENDER_XP_Ammo_Text_Width_100, -10.0, 10.0, 0.0001)
			
			ADD_WIDGET_STRING("Test XP reward")
			ADD_WIDGET_BOOL("Test at coords",debugXPAnim.bTriggerAnim)
			ADD_WIDGET_BOOL("Grab player coords",debugXPAnim.bGrabPlayerCoords )
			ADD_WIDGET_BOOL("Used widget values",debugXPAnim.bUsedWidgetValues )
			ADD_WIDGET_BOOL("Debug output",debugXPAnim.bDisplayDebug)
			ADD_WIDGET_VECTOR_SLIDER("Vector",debugXPAnim.vTestLoc,-5000,5000,0.1)
			ADD_WIDGET_INT_SLIDER("XP value",debugXPAnim.iValue,-999999,999999,5)
			
			debugXPAnim.tTestName = ADD_TEXT_WIDGET("tTestName")
			SET_CONTENTS_OF_TEXT_WIDGET(debugXPAnim.tTestName, "")
			
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("HUD_ANIM_XP")
				ADD_TO_WIDGET_COMBO("HUD_ANIM_AMMO")
				ADD_TO_WIDGET_COMBO("HUD_ANIM_PICKUP")
				ADD_TO_WIDGET_COMBO("HUD_ANIM_PICKUP_INT")
				ADD_TO_WIDGET_COMBO("HUD_ANIM_XP_WITH_NAME")
				ADD_TO_WIDGET_COMBO("HUD_ANIM_WITH_NAME")
				ADD_TO_WIDGET_COMBO("HUD_ANIM_CASH")
				ADD_TO_WIDGET_COMBO("HUD_ANIM_CASH_PLAYER")
				ADD_TO_WIDGET_COMBO("HUD_ANIM_WITH_LABEL")
			STOP_WIDGET_COMBO("XP type", debugXPAnim.iAnimType)
			
			ADD_WIDGET_FLOAT_READ_ONLY("X Text position",debugXPAnim.fXTextLocRO)
			ADD_WIDGET_FLOAT_READ_ONLY("Y Text position",debugXPAnim.fYTextLocRO)
			ADD_WIDGET_FLOAT_READ_ONLY("Text height",debugXPAnim.fTextHeightRO)
			ADD_WIDGET_FLOAT_READ_ONLY("Text width",debugXPAnim.fTextWidthRO)

			ADD_WIDGET_FLOAT_READ_ONLY("X Icon position",debugXPAnim.fXIconLocRO)
			ADD_WIDGET_FLOAT_READ_ONLY("X Icon position",debugXPAnim.fYIconLocRO)
			ADD_WIDGET_FLOAT_READ_ONLY("Icon Height",debugXPAnim.fIconHeightRO)
			ADD_WIDGET_FLOAT_READ_ONLY("Icon Width",debugXPAnim.fIconWidthRO)
			
			ADD_WIDGET_FLOAT_SLIDER("Icon size multiplier",debugXPAnim.fIconSizeMultiplier,-10, 10, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Text offset multiplier",debugXPAnim.fTextWidthOffsetMultiplier,-5, 5, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Text icon offset multiplier",debugXPAnim.fTextIconOffsetMultiplier,-5, 5, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Text icon empty space multiplier",debugXPAnim.fTextIconEmptySpaceOffsetMultiplier,-5, 5, 0.001)
			
			ADD_WIDGET_FLOAT_SLIDER("Text Height offset multiplier",debugXPAnim.fTextHeightOffsetMultiplier,-5, 5, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Text Icon height offset multiplier",debugXPAnim.fTextIconHeightOffsetMultiplier,-5, 5, 0.001)
			//ADD_WIDGET_FLOAT_SLIDER("Text Icon height offset",debugXPAnim.fTextIconTextOffset,-5, 5, 0.001)
			
			ADD_WIDGET_FLOAT_SLIDER("Text X end Wrap offset", debugXPAnim.WrapEndX,0.0,1.0,0.001)
			
			ADD_WIDGET_INT_SLIDER("Justification",debugXPAnim.iJustification, 0,4,1)
			
			ADD_WIDGET_FLOAT_SLIDER("fEasing", fEasing,0.0,10.0,0.001)
			
			//ADD_WIDGET_FLOAT_SLIDER("fRollercoasterTweak", fRollercoasterTweak,-10,10.0,0.001)
			
			//ADD_WIDGET_FLOAT_SLIDER("fEasingTweak", fEasingTweak,0.0,10.0,0.001)
			//ADD_WIDGET_FLOAT_SLIDER("fEasingLimit", fEasingLimit,-10,100.0,0.001)
			
			//ADD_WIDGET_FLOAT_SLIDER("fShowerXPZTweak", fShowerXPZTweak,-10,10.0,0.001)
			//ADD_WIDGET_FLOAT_SLIDER("fParaChuteTweak", fParaChuteTweak,-10,10.0,0.001)

//			ADD_WIDGET_FLOAT_SLIDER("Ammo W", fAmmoW, 0, 1.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("Ammo H", fAmmoH, 0, 1.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("fAmmoTextX", fAmmoTextX, -1.0, 1.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("fAmmoSpriteX", fAmmoSpriteX, -1.0, 1.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("fAmmoSpriteY", fAmmoSpriteY, -1.0, 1.0, 0.001)
		STOP_WIDGET_GROUP()
		debugXPAnim.iValue = 100
		debugXPAnim.fIconSizeMultiplier = 1.1
		
		debugXPAnim.fTextWidthOffsetMultiplier = -0.5
		debugXPAnim.fTextIconOffsetMultiplier = -0.5
		debugXPAnim.fTextIconEmptySpaceOffsetMultiplier = 0.002
		
		debugXPAnim.fTextHeightOffsetMultiplier = -0.5
		debugXPAnim.fTextIconHeightOffsetMultiplier = -0.5
		
		debugXPAnim.WrapEndX = 0.0
		
	ENDPROC
	
	PROC TRIGGER_ANIMATED_XP_TEST()
		//VECTOR vAnimLoc
		IF debugXPAnim.bTriggerAnim
			
			IF ARE_VECTORS_EQUAL(debugXPAnim.vTestLoc, <<0,0,0>>)
				debugXPAnim.vTestLoc = GET_PLAYER_COORDS(PLAYER_ID())
			ENDIF
			
			STRING stXPDesc = GET_CONTENTS_OF_TEXT_WIDGET(debugXPAnim.tTestName)
			
			STORE_XP_ANIMATION(debugXPAnim.vTestLoc,debugXPAnim.iValue,INT_TO_ENUM(HUD_ANIM_TYPE, debugXPAnim.iAnimType), stXPDesc, iDisplayXPTime)
			PRINTLN("TRIGGER_ANIMATED_XP_TEST: triggered ", debugXPAnim.iAnimType, " at ",debugXPAnim.vTestLoc, " \"", stXPDesc, "\"", "with delay time(MS): ", iDisplayXPTime)
			debugXPAnim.bTriggerAnim = FALSE
		ENDIF
		IF debugXPAnim.bGrabPlayerCoords
			debugXPAnim.vTestLoc = GET_PLAYER_COORDS(PLAYER_ID())
			debugXPAnim.bGrabPlayerCoords = FALSE
		ENDIF
	ENDPROC
#ENDIF


#ENDIF	//	USE_TU_CHANGES


