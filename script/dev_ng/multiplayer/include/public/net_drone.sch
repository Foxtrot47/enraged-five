//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_drone.sch																							//
// Description: This is data and function helpers needed for drone script												//
//																														//
// Written by:  Ata																										//
// Date:  		22/08/2017																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


USING "globals.sch"
USING "MP_globals_FM.sch"
USING "net_arcade_cabinet_common.sch"

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    CONSTS    ╞════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

// Shape Test Stages
CONST_INT DRONE_SHAPETEST_INIT								0
CONST_INT DRONE_SHAPETEST_PROCESS							1

// Area Check Stages
CONST_INT DRONE_CAM_AREA_CHECK_INIT							0
CONST_INT DRONE_CAM_AREA_CHECK_NOT_HIT_ANYTHING				1
CONST_INT DRONE_CAM_AREA_CHECK_HIT_SOMETHING				2

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡    ENUMS    ╞════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

ENUM DRONE_STATE
	DRONE_STATE_IDLE =	0,
	DRONE_STATE_INIT,
	DRONE_STATE_START_ANIMATION,
	#IF FEATURE_HEIST_ISLAND
	DRONE_STATE_LAUNCH_VFX,
	#ENDIF
	DRONE_STATE_FLYING,	
	DRONE_STATE_FUZZ_CAM,
	DRONE_STATE_REMOTE_DRONE,
	DRONE_STATE_CLEANUP
ENDENUM

ENUM DRONE_SNAPMATIC_STATE
	DRONE_SNAPMATIC_STATE_IDLE =	0,
	DRONE_SNAPMATIC_STATE_INIT,
	DRONE_SNAPMATIC_STATE_ACTIVE,
	DRONE_SNAPMATIC_STATE_CLEANUP
ENDENUM

ENUM DRONE_ANIMATION_NAME
	DRONE_ANIM_ENTER,
	DRONE_ANIM_BASE,
	DRONE_ANIM_USE_01,
	DRONE_ANIM_USE_02,
	DRONE_ANIM_USE_03,
	DRONE_ANIM_FAIL,
	DRONE_ANIM_EXIT
ENDENUM

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    STRUCT    ╞════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

STRUCT DRONE_CONTROLS_STRUCT
	INT iStickPosition[4]
	INT iLocalBS, iLocalBSTwo, iDroneRemoteScriptBS, iDroneRemoteScriptStartedWithoutDronePropBS
	INT idroneHoverShapeTest, iDroneHoverHitSomthing, idroneCollisionShapeTest, iDroneCollisionHitSomthing, idronePedDetectShapeTest, iDronePedDetecHitSomthing
	INT idroneSpawnShapeTest, iDroneSpawnHitSomthing
	INT InteriorNameHash
	INT iStunRechargerFill, iDetonateFill, iBoostFill, iDetonatePausedTime, iDetonateBarLimit, iDetonatePausedFill, iTranqRechargerFill
	INT iCamZoomLevel
	INT iDroneStatEndReason, iDroneTotalTases, iDroneShockEvent, iDroneTotalTranq
	INT iSoundID = -1
	INT iFlightLoopSoundID = -1
	INT iStunSoundID = -1
	INT iDetonateSoundID = -1
	INT iStaticSoundID = -1
	INT iFireShock = -1
	INT iDestroyedSound = -1
	INT iAlarmLoopSound = -1
	INT iHudDisconnectSound = -1
	INT iCamZoomSound = -1
	INT iStartUpSound = -1
	INT iHudLoopSoundID = -1
	INT iBoostLoopSoundID = -1
	INT iBoostRechargerSoundID = -1
	INT iScannerLoop = -1
	INT iPlayerDetectionStagger = -1
	INT iRemoteFlightLoopSoundID[NUM_NETWORK_PLAYERS]
	INT iDroneSyncScene
	INT iDroneAnimStage = 0
	INT iFireTranq = -1
	#IF FEATURE_HEIST_ISLAND
	INT iSubmarineHatch[NUM_NETWORK_PLAYERS]
	INT iMissileLaunchSound = -1
	FLOAT fSubmarineMissileHeading
	INT iSubmarineVFXBS
	#ENDIF
	
	FLOAT vAnimHeading
	FLOAT fDroneForwardSpeed, fDistanceBetDroneAndControl, fHeightDistanceBetDroneGround		
	FLOAT fHoverHeight = 1.5
	FLOAT fInteriorHeading, fDroneHeading
	FLOAT fBaseCamFov, fCamFOVTarget, fMaxZoom, fCamFOVCurrent
	FLOAT fRotationalSpeedForSound
	FLOAT fInterpSpeed
	FLOAT fControlShakeMultiplier = 1.0
	
	VECTOR vDroneHoverHitPos, vDroneTriggerCoord, vDroneStartCoord, vTargetCoord, vDroneInitialStartCoord
	VECTOR vInteriorPos , vDroneCoords
	VECTOR vAnimCoords
	
	BOOL bDroneEMPCoolDown
	
	STRING sSoundSet
	STRING sAnimDicName
	
	NETWORK_INDEX cameraProp
	NETWORK_INDEX phoneObject
	PED_INDEX fakePed
	OBJECT_INDEX targetObject
	VEHICLE_INDEX fakeVeh
	PLAYER_INDEX pDroneOwner
	
	SHAPETEST_INDEX droneHoverShapeTest, droneCollisionShapeTest, dronePedDetectShapeTest, droneSpawnShapeTest
	
	CAMERA_INDEX droneCamera
	
	SCALEFORM_INDEX droneStunSF
	
	BLIP_INDEX droneBlip
	BLIP_INDEX playerBlip
	
	TIME_DATATYPE droneUsageStat
	
	PTFX_ID missilePFX[NUM_NETWORK_PLAYERS]
	
	#IF FEATURE_HEIST_ISLAND
	PTFX_ID missileLaunchPFX[NUM_NETWORK_PLAYERS]
	#ENDIF
	
	MODEL_NAMES dronePropModel
	
	DRONE_STATE eDroneState
	DRONE_SNAPMATIC_STATE eDroneSnapmaticState
	DRONE_ANIMATION_NAME	eCurrentAnimation

	SHAPETEST_STATUS pedDetectTestResult
	
	SCRIPT_TIMER sFuzzCycleTimer, sCollisionTimer, sStunGunTimer, sBoostTimer, iBoostChargerTimer, sMissionDronKillTimer, sTranqGunTimer
	SCRIPT_TIMER sRelshipGroupTimer, sDroneBroadcastTimer, sDroneBroadcastWaitTimer, sShockEventTimer, sDroneSnapMaticStartTimer
	SCRIPT_TIMER sMissileDroneCollision, sCameraGridFailtimer, sDetonateDrainingTimer, sSelectorTimer, sPCZoomDelay, sParticipantTimer
	#IF FEATURE_HEIST_ISLAND
	SCRIPT_TIMER vMissileLaunchVFXTimer[NUM_NETWORK_PLAYERS]
	#ENDIF
	SCRIPT_TIMER sAirSpaceTimer
	SCRIPT_CONTROL_HOLD_TIMER sExplosion
ENDSTRUCT

STRUCT DRONE_REMOTE_BLIP_STRUCT
	BLIP_INDEX droneBlip[NUM_NETWORK_PLAYERS]
	INT iRemoteDroneBlipStagger
ENDSTRUCT

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡    FUNC / PROC    ╞═══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL SHOULD_KILL_DRONE_SCRIPT()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_SET_KILL_SCRIPT)
ENDFUNC

/// PURPOSE:
///    Kills drone script without fuzz time cycle
/// PARAMS:
///    bKill - true to kill
PROC SET_KILL_DRONE_SCRIPT(BOOL bKill)
	IF bKill
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("AM_MP_DRONE")) > 0 
			IF !SHOULD_KILL_DRONE_SCRIPT()
				SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_SET_KILL_SCRIPT)
				DEBUG_PRINTCALLSTACK()
				PRINTLN("[AM_MP_DRONE] - SET_KILL_DRONE_SCRIPT - TRUE")
			ENDIF
		ELSE
			PRINTLN("[AM_MP_DRONE] - SET_KILL_DRONE_SCRIPT - UNABLE TO SET TO TRUE GET_NUMBER_OF_THREADS_RUNNING: ", GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("AM_MP_DRONE")))
		ENDIF
	ELSE
		IF SHOULD_KILL_DRONE_SCRIPT()
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_SET_KILL_SCRIPT)
			PRINTLN("[AM_MP_DRONE] - SET_KILL_DRONE_SCRIPT - FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_IGNORE_DISTANCE_BETWEEN_PLAYER_AND_DRONE_CHECK()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_PLAYER_AND_DRONE_DISTANCE_CHECK)
ENDFUNC

PROC SET_IGNORE_DISTANCE_BETWEEN_PLAYER_AND_DRONE_CHECK(BOOL bIgnore)
	IF bIgnore
		IF !SHOULD_KILL_DRONE_SCRIPT()
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_PLAYER_AND_DRONE_DISTANCE_CHECK)
			PRINTLN("[AM_MP_DRONE] - SET_IGNORE_DISTANCE_BETWEEN_PLAYER_AND_DRONE_CHECK - TRUE")
		ENDIF
	ELSE
		IF SHOULD_KILL_DRONE_SCRIPT()
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_PLAYER_AND_DRONE_DISTANCE_CHECK)
			PRINTLN("[AM_MP_DRONE] - SET_IGNORE_DISTANCE_BETWEEN_PLAYER_AND_DRONE_CHECK - FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_IGNORE_TARGET_DAMAGE()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_IGNORE_TARGET_DAMAGE)
ENDFUNC

FUNC BOOL SHOULD_ENABLE_DRONE_INSTANT_COLLISION()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_ENABLE_INSTANT_COLLISION)
ENDFUNC

PROC ENABLE_DRONE_INSTANT_COLLISION(BOOL bEnable)
	IF bEnable
		IF !SHOULD_ENABLE_DRONE_INSTANT_COLLISION()
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_ENABLE_INSTANT_COLLISION)
			PRINTLN("[AM_MP_DRONE] - ENABLE_DRONE_INSTANT_COLLISION - TRUE")
		ENDIF
	ELSE
		IF SHOULD_ENABLE_DRONE_INSTANT_COLLISION()
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_ENABLE_INSTANT_COLLISION)
			PRINTLN("[AM_MP_DRONE] - ENABLE_DRONE_INSTANT_COLLISION - FALSE")
		ENDIF
	ENDIF
ENDPROC

PROC SET_IGNORE_DRONE_TARGET_DAMAGE(BOOL bIgnore)
	IF bIgnore
		IF !SHOULD_IGNORE_TARGET_DAMAGE()
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_IGNORE_TARGET_DAMAGE)
			PRINTLN("[AM_MP_DRONE] - SET_IGNORE_DRONE_TARGET_DAMAGE - TRUE")
		ENDIF
	ELSE
		IF SHOULD_IGNORE_TARGET_DAMAGE()
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_IGNORE_TARGET_DAMAGE)
			PRINTLN("[AM_MP_DRONE] - SET_IGNORE_DRONE_TARGET_DAMAGE - FALSE")
		ENDIF
	ENDIF
ENDPROC

PROC SET_DRONE_DISTANCE_LIMIT(FLOAT fLimit)
	IF g_sDroneGlobals.fDroneDistanceCheck != fLimit
		g_sDroneGlobals.fDroneDistanceCheck = fLimit
		PRINTLN("[AM_MP_DRONE] - SET_DRONE_DISTANCE_LIMIT: ", fLimit)
	ENDIF	
ENDPROC

FUNC BOOL IS_ALL_DRONE_UI_HIDDEN()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_HIDE_ALL_UI)
ENDFUNC

PROC HIDE_ALL_DRONE_UI()
	IF !IS_ALL_DRONE_UI_HIDDEN()
		SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_HIDE_ALL_UI)
		PRINTLN("[AM_MP_DRONE] - HIDE_ALL_DRONE_UI")
	ENDIF
ENDPROC

PROC SHOW_ALL_DRONE_UI()
	IF IS_ALL_DRONE_UI_HIDDEN()
		CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_HIDE_ALL_UI)
		PRINTLN("[AM_MP_DRONE] - SHOW_ALL_DRONE_UI")
	ENDIF
ENDPROC

FUNC BOOL HAS_DRONE_TARGET_HIT()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_PLAYER_HIT_TARGET)
ENDFUNC

PROC SET_DRONE_TARGET_HIT(BOOL bHit)
	IF bHit
		IF !HAS_DRONE_TARGET_HIT()
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_PLAYER_HIT_TARGET)
			PRINTLN("[AM_MP_DRONE] - SET_DRONE_TARGET_HIT - TRUE")
		ENDIF
	ELSE
		IF HAS_DRONE_TARGET_HIT()
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_PLAYER_HIT_TARGET)
			PRINTLN("[AM_MP_DRONE] - SET_DRONE_TARGET_HIT - FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_DRONE_FORCED_EXTERNAL_CLEAN_UP()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_EXTERNAL_FORCE_CLEANUP)
ENDFUNC

PROC SET_DRONE_FORCE_EXTERNAL_CLEANING_UP(BOOL bForceCleanup)
	IF bForceCleanup
		IF !IS_DRONE_FORCED_EXTERNAL_CLEAN_UP()
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_EXTERNAL_FORCE_CLEANUP)
			PRINTLN("[AM_MP_DRONE] - SET_DRONE_FORCE_EXTERNAL_CLEANING_UP - TRUE")
		ENDIF
	ELSE
		IF IS_DRONE_FORCED_EXTERNAL_CLEAN_UP()
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_EXTERNAL_FORCE_CLEANUP)
			PRINTLN("[AM_MP_DRONE] - SET_DRONE_FORCE_EXTERNAL_CLEANING_UP - FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_DRONE_HOVER_MODEL_DISABELED()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_DISABLE_HOVER_MODE)
ENDFUNC

FUNC BOOL SHOULD_START_DRONE_WITHOUT_SEAT()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_START_DRONE_WITHOUT_SEAT)
ENDFUNC 

PROC START_DRONE_WITHOUT_SEAT(BOOL bStart)
	IF bStart
		IF !SHOULD_START_DRONE_WITHOUT_SEAT()
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_START_DRONE_WITHOUT_SEAT)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_WITHOUT_SEAT - TRUE")
		ENDIF
	ELSE
		IF SHOULD_START_DRONE_WITHOUT_SEAT()
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_START_DRONE_WITHOUT_SEAT)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_WITHOUT_SEAT - FALSE")
		ENDIF
	ENDIF
ENDPROC

PROC DISABLE_DRONE_HOVER_MODE(BOOL bDisable)
	IF bDisable
		IF !IS_DRONE_HOVER_MODEL_DISABELED()
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_DISABLE_HOVER_MODE)
			PRINTLN("[AM_MP_DRONE] - DISABLE_DRONE_HOVER_MODE - TRUE")
		ENDIF
	ELSE
		IF IS_DRONE_HOVER_MODEL_DISABELED()
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_DISABLE_HOVER_MODE)
			PRINTLN("[AM_MP_DRONE] - DISABLE_DRONE_HOVER_MODE - FALSE")
		ENDIF
	ENDIF
ENDPROC

PROC SET_KEEP_INTERIOR_RUNNING_FOR_DRONE(BOOL bRunning)
	IF bRunning
		IF !IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_KEEP_INTERIOR_RUNNING)
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_KEEP_INTERIOR_RUNNING)
			PRINTLN("[AM_MP_DRONE] - SET_KEEP_INTERIOR_RUNNING_FOR_DRONE - TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_KEEP_INTERIOR_RUNNING)
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_KEEP_INTERIOR_RUNNING)
			PRINTLN("[AM_MP_DRONE] - SET_KEEP_INTERIOR_RUNNING_FOR_DRONE - FALSE")
		ENDIF
	ENDIF
ENDPROC

PROC SET_PLAYER_INITIALISING_DRONE(BOOL bUsing)
	IF bUsing
		IF !IS_PLAYER_INITIALISING_DRONE()
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_INITIALISING_DRONE)
			PRINTLN("[AM_MP_DRONE] - SET_PLAYER_INITIALISING_DRONE - TRUE")
		ENDIF
	ELSE
		IF IS_PLAYER_INITIALISING_DRONE()
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_INITIALISING_DRONE)
			PRINTLN("[AM_MP_DRONE] - SET_PLAYER_INITIALISING_DRONE - FALSE")
		ENDIF
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_USING_DRONE(BOOL bUsing)
	IF bUsing
		IF !IS_LOCAL_PLAYER_USING_DRONE()
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_PLAYER_USING_DRONE)
			PRINTLN("[AM_MP_DRONE] - SET_LOCAL_PLAYER_USING_DRONE - TRUE")
		ENDIF
	ELSE
		IF IS_LOCAL_PLAYER_USING_DRONE()
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_PLAYER_USING_DRONE)
			PRINTLN("[AM_MP_DRONE] - SET_LOCAL_PLAYER_USING_DRONE - FALSE")
		ENDIF
		SET_PLAYER_INITIALISING_DRONE(FALSE)
	ENDIF
ENDPROC

FUNC BOOL IS_DRONE_WEAPONS_DISABLED()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_DISABLE_WEAPONS)
ENDFUNC

PROC DISABLE_DRONE_WEAPONS(BOOL bDisable)
	IF bDisable
		IF !IS_DRONE_WEAPONS_DISABLED()
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_DISABLE_WEAPONS)
			PRINTLN("[AM_MP_DRONE] - DISABLE_DRONE_WEAPONS - TRUE")
		ENDIF
	ELSE
		IF IS_DRONE_WEAPONS_DISABLED()
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_DISABLE_WEAPONS)
			PRINTLN("[AM_MP_DRONE] - DISABLE_DRONE_WEAPONS - FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_RETURN_DRONE_TO_PLAYER()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_RETURN_DONE_TO_PLAYER)
ENDFUNC

FUNC BOOL SHOULD_RETURN_DRONE_TO_PLAYER_AND_KILL_SCRIPT()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_RETURN_DONE_TO_PLAYER_KILL_SCRIPT)
ENDFUNC

/// PURPOSE:
///    overrides drone default cool down timer
/// PARAMS:
///    iTime - time in mil sceonds 
///    set back to -1 when you are done
PROC OVERRIDE_DRONE_COOL_DOWN_TIMER(INT iTime)
	IF g_sDroneGlobals.iDroneCoolDownTimer != iTime
	 	g_sDroneGlobals.iDroneCoolDownTimer = iTime
		PRINTLN("[AM_MP_DRONE] - OVERRIDE_DRONE_COOL_DOWN_TIMER - iTime: ", iTime)
	ENDIF
ENDPROC

/// PURPOSE:
///   
/// PARAMS:
///    fValue - value between 0 and 100 to cleanup this override set this to -1
PROC OVERRIDE_DRONE_MINIMAP_ZOOM(FLOAT fValue)
	IF g_sDroneGlobals.fOverrideDroneMapZoom != fValue 
		g_sDroneGlobals.fOverrideDroneMapZoom = fValue
		PRINTLN("[AM_MP_DRONE] - OVERRIDE_DRONE_MINIMAP_ZOOM - g_sDroneGlobals.fOverrideDroneMapZoom: ", g_sDroneGlobals.fOverrideDroneMapZoom)
	ENDIF
ENDPROC	

FUNC BOOL SHOULD_SHOW_DRONE_TICKER()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_SHOW_DRONE_TICKER)
ENDFUNC
 
PROC SHOW_DRONE_TICKER(BOOL bShow)
	IF bShow
		IF !SHOULD_SHOW_DRONE_TICKER()
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_SHOW_DRONE_TICKER)
			PRINTLN("[AM_MP_DRONE] - SHOW_DRONE_TICKER - TRUE")
		ENDIF
	ELSE
		IF SHOULD_SHOW_DRONE_TICKER()
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_SHOW_DRONE_TICKER)
			PRINTLN("[AM_MP_DRONE] - SHOW_DRONE_TICKER - FALSE")
		ENDIF
	ENDIF
ENDPROC
 
PROC SET_DRONE_CLEANING_UP(BOOL bCleaning)
	IF bCleaning
		IF !IS_DRONE_CLEANING_UP()
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_CLEANING_UP)
			PRINTLN("[AM_MP_DRONE] - SET_DRONE_CLEANING_UP - TRUE")
		ENDIF
	ELSE
		IF IS_DRONE_CLEANING_UP()
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_CLEANING_UP)
			PRINTLN("[AM_MP_DRONE] - SET_DRONE_CLEANING_UP - FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_DRONE_RADAR_ACTIVE()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_ACTIVATE_DRONE_RADAR)
ENDFUNC

PROC ACTIVATE_DRONE_RADAR(BOOL bActivate)
	IF bActivate
		IF !IS_DRONE_RADAR_ACTIVE()
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_ACTIVATE_DRONE_RADAR)
			PRINTLN("[AM_MP_DRONE] - ACTIVATE_DRONE_RADAR - TRUE")
		ENDIF
	ELSE
		IF IS_DRONE_RADAR_ACTIVE()
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_ACTIVATE_DRONE_RADAR)
			PRINTLN("[AM_MP_DRONE] - ACTIVATE_DRONE_RADAR - FALSE")
		ENDIF
	ENDIF
ENDPROC

PROC SET_DRONE_TARGET(VECTOR vTargetCoord, FLOAT fHeading) 
	IF !ARE_VECTORS_EQUAL(g_sDroneGlobals.vDroneTargetCoord, vTargetCoord)
		g_sDroneGlobals.vDroneTargetCoord 	= vTargetCoord
		g_sDroneGlobals.fDroneTargetHeading = fHeading
		PRINTLN("[AM_MP_DRONE] - SET_DRONE_TARGET - ", g_sDroneGlobals.vDroneTargetCoord, " heading: ", fHeading)
	ENDIF	
ENDPROC	

PROC SET_DRONE_COORD_AND_ROTATION(VECTOR vCoord, VECTOR vRotation) 
	IF !ARE_VECTORS_EQUAL(g_sDroneGlobals.vDroneCoord, vCoord)
		g_sDroneGlobals.vDroneCoord 	= vCoord
		
		// Prop orientation is not matching RAG orientation 
		vRotation += <<180, -180, 0.0>>		
		g_sDroneGlobals.fDroneRotation	= vRotation
		
		PRINTLN("[AM_MP_DRONE] - SET_DRONE_COORD_AND_ROTATION - vCoord: ", vCoord, " heading: ", vRotation)
	ENDIF	
ENDPROC 

FUNC VECTOR GET_DRONE_OVERRIDE_COORDS()
	RETURN g_sDroneGlobals.vDroneCoord
ENDFUNC

FUNC VECTOR GET_DRONE_OVERRIDE_ROTATION()
	RETURN g_sDroneGlobals.fDroneRotation
ENDFUNC

FUNC VECTOR GET_DRONE_TARGET_POSITION(DRONE_CONTROLS_STRUCT &sDroneControlsStruct)
	IF NOT IS_VECTOR_ZERO(g_sDroneGlobals.vDroneTargetCoord)
		RETURN g_sDroneGlobals.vDroneTargetCoord
	ELSE
		RETURN sDroneControlsStruct.vTargetCoord
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC FLOAT GET_DRONE_TARGET_HEADING()
	IF g_sDroneGlobals.fDroneTargetHeading != 0.0
		RETURN g_sDroneGlobals.fDroneTargetHeading
	ENDIF
	
	RETURN 0.0
ENDFUNC

PROC SET_DRONE_TARGET_MODEL(MODEL_NAMES vTargetModel) 
	IF vTargetModel != DUMMY_MODEL_FOR_SCRIPT
		IF g_sDroneGlobals.DroneTargetModel != vTargetModel
			g_sDroneGlobals.DroneTargetModel = vTargetModel
			PRINTLN("[AM_MP_DRONE] - SET_DRONE_TARGET - ", GET_MODEL_NAME_FOR_DEBUG(vTargetModel))
		ENDIF	
	ELSE
		PRINTLN("[AM_MP_DRONE] - SET_DRONE_TARGET - passing DUMMY_MODEL_FOR_SCRIPT")
	ENDIF	
ENDPROC	

FUNC MODEL_NAMES GET_DRONE_TARGET_MODEL(BOOL bDamagedModel = FALSE)
	IF NOT bDamagedModel
		IF g_sDroneGlobals.DroneTargetModel != DUMMY_MODEL_FOR_SCRIPT
			RETURN g_sDroneGlobals.DroneTargetModel
		ELSE
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_Battle_SecPanel"))
		ENDIF
	ELSE
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_Battle_SecPanel_Dam"))
	ENDIF
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

PROC RESET_DRONE_TARGET()
	SET_DRONE_TARGET(<<0,0,0>>, 0.0)
	SET_DRONE_TARGET_MODEL(DUMMY_MODEL_FOR_SCRIPT)
	SET_DRONE_TARGET_HIT(FALSE)
ENDPROC

FUNC PED_INDEX GET_DRONE_FAKE_PED_INDEX()
	RETURN g_sDroneGlobals.DroneFakePed
ENDFUNC 

FUNC BOOL IS_PLAYER_TRYING_TO_SEAT_IN_DRONE_STATION()
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "enter")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "enter_left")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "exit")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "exit_left")
	OR IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CAN_TURRET_PED_LAUNCH_TURRET_HUD: returning false, player is playing enter anim")
		RETURN TRUE
	ENDIF
		
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "base")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "computer_enter")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "computer_exit")
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CAN_TURRET_PED_LAUNCH_TURRET_HUD: returning false, player is not in the seat")
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

#IF FEATURE_HEIST_ISLAND
FUNC BOOL IS_PLAYER_TRYING_TO_SEAT_IN_SUBMARINE_MISSILE_STATION()
	STRING sAnimDic = "anim@scripted@submarine@ig28_submarine_turret_control@male@"
	IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
		sAnimDic = "anim@scripted@submarine@ig28_submarine_turret_control@heeled@"
	ENDIF
	
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "enter")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "enter_left")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "exit")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "exit_left")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDic, "enter_left")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDic, "enter")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDic, "exit")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDic, "exit_left")
	OR IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CAN_TURRET_PED_LAUNCH_TURRET_HUD: returning false, player is playing enter anim")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL CAN_TURRET_PED_LAUNCH_TURRET_HUD()
	IF g_iShouldLaunchTruckTurret != -1
	OR g_iInteriorTurretSeat != -1
	#IF FEATURE_HEIST_ISLAND
	OR g_iShouldLaunchSubmarineSeat != -1
	#ENDIF
		STRING sAnimDic = "anim@scripted@submarine@ig28_submarine_turret_control@male@"
		IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
			sAnimDic = "anim@scripted@submarine@ig28_submarine_turret_control@heeled@"
		ENDIF
	
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "enter")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "enter_left")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "exit")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "exit_left")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDic, "enter_left")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDic, "enter")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDic, "exit")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDic, "exit_left")
		OR IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CAN_TURRET_PED_LAUNCH_TURRET_HUD: returning false, player is playing enter anim")
			RETURN FALSE
		ENDIF
		
		IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "base")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "computer_enter")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "computer_exit")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "COMPUTER_idle")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "COMPUTER_idle_control")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "COMPUTER_enter_control")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDic, "base")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDic, "idle_a")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDic, "idle_b")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDic, "idle_c")

			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CAN_TURRET_PED_LAUNCH_TURRET_HUD: returning false, player is not in the seat")
			RETURN FALSE
		ENDIF
		
		IF SHOULD_BAIL_FROM_TURRET()
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CAN_TURRET_PED_LAUNCH_TURRET_HUD: returning false, SHOULD_BAIL_FROM_TURRET ")
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_DRONE_SEAT_IN_HACKER_TRUCK()
	IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
	AND g_iShouldLaunchTruckTurret = 1
	AND CAN_TURRET_PED_LAUNCH_TURRET_HUD()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_SEAT_LOCATE()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1421.593140,-3011.169922,-80.249939>>, <<-1423.142090,-3012.295410,-77.749939>>, 2.0)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1419.463013,-3009.144775,-80.499939>>, <<-1420.684326,-3010.616943,-77.999939>>, 2.0)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_START_DRONE_FROM_PLAYER_PHONE()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_START_FROM_PLAYER_PHONE)
ENDFUNC

PROC SET_START_DRONE_FROM_PLAYER_PHONE(BOOL bActivate)
	IF bActivate
		IF !SHOULD_START_DRONE_FROM_PLAYER_PHONE()
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_START_FROM_PLAYER_PHONE)
			PRINTLN("[AM_MP_DRONE] - SET_START_DRONE_FROM_PLAYER_PHONE - TRUE")
		ENDIF
	ELSE
		IF SHOULD_START_DRONE_FROM_PLAYER_PHONE()
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_START_FROM_PLAYER_PHONE)
			PRINTLN("[AM_MP_DRONE] - SET_START_DRONE_FROM_PLAYER_PHONE - FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_DRONE_TYPE_NAME(DRONE_TYPE eDroneType)
 	SWITCH eDroneType 
		CASE DRONE_TYPE_DEBUG 					RETURN "DRONE_TYPE_DEBUG"
		CASE DRONE_TYPE_HACKER_TRUCK			RETURN "DRONE_TYPE_HACKER_TRUCK"
		CASE DRONE_TYPE_ARENA_WARS_ONE			RETURN "DRONE_TYPE_ARENA_WARS_ONE"				
		CASE DRONE_TYPE_ARENA_WARS_TWO			RETURN "DRONE_TYPE_ARENA_WARS_TWO"
		CASE DRONE_TYPE_ARENA_WARS_MISSILES		RETURN "DRONE_TYPE_ARENA_WARS_MISSILES"
 		CASE DRONE_TYPE_HEIST_FREEMODE			RETURN "DRONE_TYPE_HEIST_FREEMODE"
		CASE DRONE_TYPE_CASINO_HEIST_MISSION	RETURN "DRONE_TYPE_CASINO_HEIST_MISSION"
		#IF FEATURE_COPS_N_CROOKS
		CASE DRONE_TYPE_ARCADE_COP				RETURN "DRONE_TYPE_ARCADE_COP"				
		#ENDIF
	ENDSWITCH
	RETURN "[AM_MP_DRONE] INVALID drone type!"
ENDFUNC

/// PURPOSE:
///    Start drone script - call this until returns true
/// PARAMS:
///    vTriggerCoord - coord which player starts drone from if pass <<0,0,0>> drone will start immediately after START_DRONE is true without help text and player input 
///    vDroneStartCoord - coord which drone should start flying from if pass <<0,0,0>> it will start from player coord
///    vTargetCoord - coord which player should hit for mission if player hit this coord with stun gun drone will cleanup and HAS_DRONE_TARGET_HIT will set to true
///    DRONE_TYPE - DRONE_TYPE_HACKER_TRUCK, DRONE_TYPE_ARENA_WARS_ONE, DRONE_TYPE_ARENA_WARS_TWO, DRONE_TYPE_ARENA_WARS_MISSILES, DRONE_TYPE_HEIST_FREEMODE, DRONE_TYPE_ARCADE_COP.
///
SCRIPT_TIMER sDroneScriptTimer
THREADID DroneThread
FUNC BOOL START_DRONE(DRONE_TYPE eType, VECTOR vTriggerCoord, VECTOR vDroneStartCoord, VECTOR vTargetCoord)
	
	IF SHOULD_KILL_DRONE_SCRIPT()
		IF NOT HAS_NET_TIMER_STARTED(sDroneScriptTimer)
			START_NET_TIMER(sDroneScriptTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sDroneScriptTimer, 8000)
				RESET_NET_TIMER(sDroneScriptTimer)
				SET_KILL_DRONE_SCRIPT(FALSE)
			ENDIF	
		ENDIF
	ENDIF
	
	IF NOT IS_THREAD_ACTIVE(DroneThread)
	AND NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("AM_MP_DRONE"))
		REQUEST_SCRIPT("AM_MP_DRONE")
	ENDIF

	IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("AM_MP_DRONE"))
	AND NOT IS_THREAD_ACTIVE(DroneThread)
	AND !SHOULD_KILL_DRONE_SCRIPT()
		DRONE_LAUNCHER_STRUCT sDroneLauncherData
		sDroneLauncherData.iInstanceId 		= 0
		sDroneLauncherData.vTriggerCoord 	= vTriggerCoord
		sDroneLauncherData.vDroneStartCoord = vDroneStartCoord
		sDroneLauncherData.vTargetCoord 	= vTargetCoord
		sDroneLauncherData.iType			= ENUM_TO_INT(eType)
		
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_DRONE")) < 1
			IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_MP_DRONE", sDroneLauncherData.iInstanceId, TRUE)
				DroneThread = START_NEW_SCRIPT_WITH_NAME_HASH_AND_ARGS(HASH("AM_MP_DRONE"), sDroneLauncherData, SIZE_OF(sDroneLauncherData), DEFAULT_STACK_SIZE)
				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("AM_MP_DRONE"))
				RESET_NET_TIMER(sDroneScriptTimer)
				#IF IS_DEBUG_BUILD
				PRINTLN("[AM_MP_DRONE] start drone script type: ", GET_DRONE_TYPE_NAME(eType))
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				PRINTLN("[AM_MP_DRONE] launching script")
				#ENDIF
				RETURN TRUE					
			ENDIF
		
		ENDIF
		
	ELIF IS_THREAD_ACTIVE(DroneThread)
	AND !SHOULD_KILL_DRONE_SCRIPT()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC REL_GROUP_HASH GET_DRONE_RELATIONSHIP_GROUP()
	IF g_sDroneGlobals.iDroneRelationShipGroup != -1
	AND g_sDroneGlobals.iDroneRelationShipGroup != 0
		RETURN INT_TO_ENUM(REL_GROUP_HASH, g_sDroneGlobals.iDroneRelationShipGroup)
	ENDIF
	RETURN rgFM_HateEveryOne
ENDFUNC 

PROC SET_DRONE_RELATIONSHIP_GROUP(REL_GROUP_HASH relGroup)
	IF INT_TO_ENUM(REL_GROUP_HASH, g_sDroneGlobals.iDroneRelationShipGroup) != relGroup
		g_sDroneGlobals.iDroneRelationShipGroup =  ENUM_TO_INT(relGroup)
		PRINTLN("[AM_MP_DRONE] - SET_DRONE_RELATIONSHIP_GROUP: ", GET_RELATIONSHIP_NAME_FOR_DEBUG(relGroup))
	ENDIF	
ENDPROC

FUNC FLOAT GET_INVERSE_HEADING(FLOAT fHeading)
	IF fHeading > 180 
		RETURN (fHeading - 180)
	ELSE
		RETURN (fHeading + 180)
	ENDIF
ENDFUNC

PROC CLEANUP_DRONE_REMOTE_BLIP(DRONE_REMOTE_BLIP_STRUCT &sDroneRemoteBlip)
	IF DOES_BLIP_EXIST(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger])
		REMOVE_BLIP(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger])
	ENDIF
ENDPROC

FUNC FLOAT GET_DRONE_DISTANCE_LIMIT()
	IF g_sDroneGlobals.fDroneDistanceCheck = 0
		RETURN g_sMPTunables.fBB_TERRORBYTE_DRONE_DISTANCE_LIMIT
	ELSE
		RETURN g_sDroneGlobals.fDroneDistanceCheck
	ENDIF
ENDFUNC 

FUNC BOOL SHOULD_DRAW_DRONE_BLIP()
	IF PLAYER_ID() = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		IF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.eArcadeCabLocatePlayerIn = ARCADE_CABINET_CH_DRONE_MINI_G
			RETURN TRUE
		ENDIF	
	ELSE
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DRAW_REMOTE_PLAYER_DRONE_BLIP(PLAYER_INDEX pPlayerToCheck)
	IF pPlayerToCheck = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF GlobalplayerBD_FM_4[NATIVE_TO_INT(pPlayerToCheck)].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE
		RETURN FALSE
	ENDIF	
	#ENDIF	
		
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		IF ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(PLAYER_ID(), pPlayerToCheck, TRUE)
			IF globalPlayerBD[NATIVE_TO_INT(pPlayerToCheck)].sArcadeManagerGlobalPlayerBD.eArcadeCabLocatePlayerIn = ARCADE_CABINET_CH_DRONE_MINI_G
				RETURN TRUE
			ENDIF	
		ENDIF
	ELSE
		IF GB_ARE_PLAYERS_IN_SAME_GANG(pPlayerToCheck, PLAYER_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_DRONE_BLIP_COLOUR(PLAYER_INDEX pPlayerToCheck)
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		ARCADE_CAB_MANAGER_SLOT iDisplaySlot = INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(pPlayerToCheck))
		SWITCH iDisplaySlot
			CASE ACM_SLOT_38
				RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_ORANGE)
			BREAK	
			CASE ACM_SLOT_39
				RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_BLUE)
			BREAK
			CASE ACM_SLOT_40
				RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_YELLOW)
			BREAK
			CASE ACM_SLOT_41
				RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_RED)
			BREAK
		ENDSWITCH
	ELSE
		RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
	ENDIF
	
	RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_WHITE)
ENDFUNC

PROC MAINTAIN_DRONE_REMOTE_BLIPS(DRONE_REMOTE_BLIP_STRUCT &sDroneRemoteBlip)
	
	IF sDroneRemoteBlip.iRemoteDroneBlipStagger < 0 
	OR sDroneRemoteBlip.iRemoteDroneBlipStagger >=  NUM_NETWORK_PLAYERS
		sDroneRemoteBlip.iRemoteDroneBlipStagger  = 0
	ENDIF	
	
	IF SHOULD_DRAW_DRONE_BLIP()
		PLAYER_INDEX pPlayerToCheck = INT_TO_PLAYERINDEX(sDroneRemoteBlip.iRemoteDroneBlipStagger)
		IF NETWORK_IS_PLAYER_ACTIVE(pPlayerToCheck)
		AND PLAYER_ID() != pPlayerToCheck
			IF SHOULD_DRAW_REMOTE_PLAYER_DRONE_BLIP(pPlayerToCheck)
				IF IS_PLAYER_USING_DRONE(pPlayerToCheck)
					VECTOR vdroneCoord = GET_PLAYER_DRONE_COORDS(pPlayerToCheck)
					FLOAT fHeading = GET_INVERSE_HEADING(GET_PLAYER_DRONE_HEADING(pPlayerToCheck))
					IF DOES_ENTITY_EXIST(GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].objID_remoteDrone[NATIVE_TO_INT(pPlayerToCheck)])
						IF NOT DOES_BLIP_EXIST(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger])
							sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger] = CREATE_BLIP_FOR_ENTITY(GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].objID_remoteDrone[NATIVE_TO_INT(pPlayerToCheck)])
							SET_BLIP_SPRITE(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger] , RADAR_TRACE_BAT_DRONE)
							SET_BLIP_ROTATION(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], ROUND(fHeading))
							SET_BLIP_SCALE(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], 0.8)
							SET_BLIP_PRIORITY(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], BLIPPRIORITY_HIGHEST)
							SET_BLIP_COLOUR(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], GET_DRONE_BLIP_COLOUR(pPlayerToCheck))
							PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_REMOTE_BLIPS - created blip on entity true")
						ELSE
							SET_BLIP_ROTATION(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], ROUND(fHeading))
							SET_BLIP_SCALE(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], 0.8)
							SET_BLIP_PRIORITY(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], BLIPPRIORITY_HIGHEST)
							SET_BLIP_COLOUR(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], GET_DRONE_BLIP_COLOUR(pPlayerToCheck))
						ENDIF
					ELIF !IS_VECTOR_ZERO(vdroneCoord)
					AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
						IF NOT DOES_BLIP_EXIST(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger])
							sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger] = CREATE_BLIP_FOR_COORD(vdroneCoord)
							SET_BLIP_SPRITE(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger] , RADAR_TRACE_BAT_DRONE)
							SET_BLIP_ROTATION(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], ROUND(fHeading))
							SET_BLIP_COORDS(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], vdroneCoord)
							SET_BLIP_SCALE(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], 0.8)
							SET_BLIP_PRIORITY(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], BLIPPRIORITY_HIGHEST)
							SET_BLIP_COLOUR(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], GET_DRONE_BLIP_COLOUR(pPlayerToCheck))
							PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_REMOTE_BLIPS - created blip coords true")
						ELSE
							SET_BLIP_ROTATION(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], ROUND(fHeading))
							SET_BLIP_COORDS(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], vdroneCoord)
							SET_BLIP_SCALE(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], 0.8)
							SET_BLIP_PRIORITY(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], BLIPPRIORITY_HIGHEST)
							SET_BLIP_COLOUR(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger], GET_DRONE_BLIP_COLOUR(pPlayerToCheck))
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger])
							CLEANUP_DRONE_REMOTE_BLIP(sDroneRemoteBlip)
							PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_REMOTE_BLIPS - drone is not live: ", sDroneRemoteBlip.iRemoteDroneBlipStagger)
						ENDIF	
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger])
						CLEANUP_DRONE_REMOTE_BLIP(sDroneRemoteBlip)
						PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_REMOTE_BLIPS - player is not using drone: ", sDroneRemoteBlip.iRemoteDroneBlipStagger)
					ENDIF	
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger])
					CLEANUP_DRONE_REMOTE_BLIP(sDroneRemoteBlip)
					PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_REMOTE_BLIPS - SHOULD_DRAW_REMOTE_PLAYER_DRONE_BLIP ")
				ENDIF	
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger])
				CLEANUP_DRONE_REMOTE_BLIP(sDroneRemoteBlip)
				PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_REMOTE_BLIPS - player not active: ", sDroneRemoteBlip.iRemoteDroneBlipStagger)
			ENDIF	
		ENDIF	
	ELSE
		IF DOES_BLIP_EXIST(sDroneRemoteBlip.droneBlip[sDroneRemoteBlip.iRemoteDroneBlipStagger])
			CLEANUP_DRONE_REMOTE_BLIP(sDroneRemoteBlip)
			PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_REMOTE_BLIPS - local player is not in gang", sDroneRemoteBlip.iRemoteDroneBlipStagger)
		ENDIF	
	ENDIF
	
	sDroneRemoteBlip.iRemoteDroneBlipStagger++
ENDPROC

FUNC BOOL IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARENA_WARS_TYPE_TWO)
ENDFUNC 

FUNC BOOL IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARENA_WARS_TYPE_ONE)
ENDFUNC 

FUNC BOOL IS_DRONE_STARTED_FROM_TRUCK()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_START_DRONE_FROM_TRUCK)
ENDFUNC 

FUNC BOOL IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARENA_WARS_TYPE_MISSILE)
ENDFUNC 

#IF FEATURE_HEIST_ISLAND
FUNC BOOL IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_SUBMARINE_TYPE_MISSILE)
ENDFUNC 
#ENDIF

FUNC BOOL IS_REMOTE_DRONE_STARTED()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_REMOTE)
ENDFUNC

FUNC BOOL IS_DRONE_USED_AS_GUIDED_MISSILE()
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
		RETURN TRUE
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

#IF FEATURE_COPS_N_CROOKS
FUNC BOOL IS_DRONE_STARTED_FROM_ARCADE_COP()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARCADE_COP)
ENDFUNC

PROC START_DRONE_FROM_ARCADE_COP(BOOL bStart)
	IF bStart
		IF NOT IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARCADE_COP)
			g_sDroneGlobals.iDroneTypeBS = 0
			SET_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARCADE_COP)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_ARCADE_COP TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARCADE_COP)
			CLEAR_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARCADE_COP)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_ARCADE_COP FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_COPS_CROOK_DRONE_TYPE_COP()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneCopsCrookType, DRONE_TYPE_BS_DRONE_START_ARCADE_COP)
ENDFUNC

/// PURPOSE:
///    Set flag to allow drone controller to start the drone from PI menu
PROC SET_COPS_CROOK_DRONE_TYPE_COP(BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(g_sDroneGlobals.iDroneCopsCrookType, DRONE_TYPE_BS_DRONE_START_ARCADE_COP)
			g_sDroneGlobals.iDroneCopsCrookType = 0
			SET_BIT(g_sDroneGlobals.iDroneCopsCrookType, DRONE_TYPE_BS_DRONE_START_ARCADE_COP)
			PRINTLN("[AM_MP_DRONE] - SET_COPS_CROOK_DRONE_TYPE_COP TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_sDroneGlobals.iDroneCopsCrookType, DRONE_TYPE_BS_DRONE_START_ARCADE_COP)
			CLEAR_BIT(g_sDroneGlobals.iDroneCopsCrookType, DRONE_TYPE_BS_DRONE_START_ARCADE_COP)
			PRINTLN("[AM_MP_DRONE] - SET_COPS_CROOK_DRONE_TYPE_COP FALSE")
		ENDIF
	ENDIF
ENDPROC

#ENDIF

FUNC BOOL SHOULD_ENABLE_ARCADE_LOGIC_WHILE_DRONE_ACTIVE()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_ENABLE_ARCADE_LOGIC_WHILE_DRONE_ACTIVE)
ENDFUNC

PROC ENABLE_ARCADE_LOGIC_WHILE_DRONE_ACTIVE(BOOL bEnable)
	IF bEnable
		IF !SHOULD_ENABLE_ARCADE_LOGIC_WHILE_DRONE_ACTIVE()
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_ENABLE_ARCADE_LOGIC_WHILE_DRONE_ACTIVE)
			PRINTLN("[AM_MP_DRONE] - ENABLE_ARCADE_LOGIC_WHILE_DRONE_ACTIVE - TRUE")
		ENDIF
	ELSE
		IF SHOULD_ENABLE_ARCADE_LOGIC_WHILE_DRONE_ACTIVE()
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_ENABLE_ARCADE_LOGIC_WHILE_DRONE_ACTIVE)
			PRINTLN("[AM_MP_DRONE] - ENABLE_ARCADE_LOGIC_WHILE_DRONE_ACTIVE - FALSE")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Flag when shape test detects object 
FUNC BOOL IS_DRONE_AREA_SAFE_TO_SPAWN()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_SAFE_DRONE_AREA)
ENDFUNC

PROC SET_DRONE_AREA_SAFE_TO_SPAWN(BOOL bActivate)
	IF bActivate
		IF !IS_DRONE_AREA_SAFE_TO_SPAWN()
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_SAFE_DRONE_AREA)
			PRINTLN("[AM_MP_DRONE] - SET_DRONE_AREA_SAFE_TO_SPAWN - TRUE")
		ENDIF
	ELSE
		IF IS_DRONE_AREA_SAFE_TO_SPAWN()
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_SAFE_DRONE_AREA)
			PRINTLN("[AM_MP_DRONE] - SET_DRONE_AREA_SAFE_TO_SPAWN - FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_HEIST_FREEMODE)
ENDFUNC

PROC START_DRONE_FROM_HEIST_FREEMODE(BOOL bStart)
	IF bStart
		IF NOT IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_HEIST_FREEMODE)
			g_sDroneGlobals.iDroneTypeBS = 0
			SET_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_HEIST_FREEMODE)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_HEIST_FREEMODE TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_HEIST_FREEMODE)
			CLEAR_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_HEIST_FREEMODE)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_HEIST_FREEMODE FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_DRONE_STARTED_FROM_CASINO_HEIST_MISSON()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_CASINO_HEIST_MISSION)
ENDFUNC

PROC START_DRONE_FROM_CASINO_HEIST_MISSON(BOOL bStart)
	IF bStart
		IF NOT IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_CASINO_HEIST_MISSION)
			g_sDroneGlobals.iDroneTypeBS = 0
			SET_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_CASINO_HEIST_MISSION)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_CASINO_HEIST_MISSON TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_CASINO_HEIST_MISSION)
			CLEAR_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_CASINO_HEIST_MISSION)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_CASINO_HEIST_MISSON FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_LOCAL_DRONE_IN_COOL_DOWN()
	#IF IS_DEBUG_BUIlD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_NoDroneCoolDown")
		IF GET_FRAME_COUNT() % 10000 = 0
			PRINTLN("[AM_MP_DRONE] - IS_LOCAL_DRONE_IN_COOL_DOWN sc_NoDroneCoolDown is true")
		ENDIF	
		RETURN FALSE
	ENDIF	
	#ENDIF

	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iHackerTruckModBS, PROPERTY_BROADCAST_HACKER_TRUCK_DRONE_IN_COOL_DOWN)
	ENDIF	
	RETURN FALSE
ENDFUNC 

FUNC INT GET_DRONE_TRANQUILIZER_AMMO()
	RETURN g_sDroneGlobals.iDroneTranQuilizerAmmo
ENDFUNC

PROC SET_DRONE_TRANQUILIZER_AMMO(INT iAmmo)
	IF g_sDroneGlobals.iDroneTranQuilizerAmmo != iAmmo
		g_sDroneGlobals.iDroneTranQuilizerAmmo = iAmmo
		PRINTLN("[AM_MP_DRONE] - SET_DRONE_TRANQUILIZER_AMMO iAmmo: ", iAmmo)
	ENDIF
ENDPROC

PROC START_DRONE_FROM_TRUCK(BOOL bStart)
	IF bStart
		IF NOT IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_START_DRONE_FROM_TRUCK)
			g_sDroneGlobals.iDroneTypeBS = 0
			SET_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_START_DRONE_FROM_TRUCK)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_TRUCK TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_START_DRONE_FROM_TRUCK)
			CLEAR_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_START_DRONE_FROM_TRUCK)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_TRUCK FALSE")
		ENDIF
	ENDIF
ENDPROC

PROC START_DRONE_FROM_ARENA_WARS_ONE(BOOL bStart)
	IF bStart
		IF NOT IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARENA_WARS_TYPE_ONE)
			g_sDroneGlobals.iDroneTypeBS = 0
			SET_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARENA_WARS_TYPE_ONE)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_ARENA_WARS_ONE TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARENA_WARS_TYPE_ONE)
			CLEAR_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARENA_WARS_TYPE_ONE)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_ARENA_WARS_ONE FALSE")
		ENDIF
	ENDIF
ENDPROC

PROC START_DRONE_FROM_ARENA_WARS_TWO(BOOL bStart)
	IF bStart
		IF NOT IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARENA_WARS_TYPE_TWO)
			g_sDroneGlobals.iDroneTypeBS = 0
			SET_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARENA_WARS_TYPE_TWO)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_ARENA_WARS_TWO TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARENA_WARS_TYPE_TWO)
			CLEAR_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARENA_WARS_TYPE_TWO)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_ARENA_WARS_TWO FALSE")
		ENDIF
	ENDIF
ENDPROC

PROC START_DRONE_FROM_ARENA_WARS_MISSILE(BOOL bStart)
	IF bStart
		IF NOT IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARENA_WARS_TYPE_MISSILE)
			g_sDroneGlobals.iDroneTypeBS = 0
			SET_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARENA_WARS_TYPE_MISSILE)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_ARENA_WARS_MISSILE TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARENA_WARS_TYPE_MISSILE)
			CLEAR_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_ARENA_WARS_TYPE_MISSILE)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_ARENA_WARS_MISSILE FALSE")
		ENDIF
	ENDIF
ENDPROC

PROC START_REMOTE_DRONE(BOOL bStart)
	IF bStart
		IF NOT IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_REMOTE)
			g_sDroneGlobals.iDroneTypeBS = 0
			SET_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_REMOTE)
			PRINTLN("[AM_MP_DRONE] - START_REMOTE_DRONE TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_REMOTE)
			CLEAR_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_REMOTE)
			PRINTLN("[AM_MP_DRONE] - START_REMOTE_DRONE FALSE")
		ENDIF
	ENDIF
ENDPROC

#IF FEATURE_HEIST_ISLAND
PROC START_DRONE_FROM_SUBMARINE_MISSILE(BOOL bStart)
	IF bStart
		IF NOT IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_SUBMARINE_TYPE_MISSILE)
			g_sDroneGlobals.iDroneTypeBS = 0
			SET_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_SUBMARINE_TYPE_MISSILE)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_SUBMARINE_MISSILE TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_SUBMARINE_TYPE_MISSILE)
			CLEAR_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_SUBMARINE_TYPE_MISSILE)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_SUBMARINE_MISSILE FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_MISSILE_SEAT_IN_SUBMARINE()
	IF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
	AND CAN_TURRET_PED_LAUNCH_TURRET_HUD()
		IF g_iShouldLaunchSubmarineSeat = 2
		OR g_iShouldLaunchSubmarineSeat = 1
			RETURN TRUE
		ENDIF	
	ENDIF
	RETURN FALSE
ENDFUNC
#ENDIF

PROC START_DRONE_FROM_DEBUG(BOOL bStart)
	IF bStart
		IF NOT IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_TYPE_DEBUG)
			g_sDroneGlobals.iDroneTypeBS = 0
			SET_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_TYPE_DEBUG)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_DEBUG TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_TYPE_DEBUG)
			CLEAR_BIT(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_TYPE_DEBUG)
			PRINTLN("[AM_MP_DRONE] - START_DRONE_FROM_DEBUG FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_DRONE_STARTED_FROM_DEBUG()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneTypeBS, DRONE_TYPE_BS_DRONE_START_TYPE_DEBUG)
ENDFUNC 

FUNC BOOL SHOULD_START_REMOTE_DRONE_SCRIPT()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_START_REMOTE_DRONE_SCRIPT)
ENDFUNC 

FUNC BOOL SHOULD_TERMINATE_REMOTE_DRONE_SCRIPT()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_TERMINATE_REMOTE_DRONE_SCRIPT)
ENDFUNC 

PROC SET_START_REMOTE_DRONE_SCRIPT(BOOL bStart)
	IF bStart 
		IF NOT IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_START_REMOTE_DRONE_SCRIPT)
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_START_REMOTE_DRONE_SCRIPT)
			PRINTLN("[AM_MP_DRONE] - SET_START_REMOTE_DRONE_SCRIPT TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_START_REMOTE_DRONE_SCRIPT)
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_START_REMOTE_DRONE_SCRIPT)
			PRINTLN("[AM_MP_DRONE] - SET_START_REMOTE_DRONE_SCRIPT FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_DRONE_USE_FROM_PI_MENU_ENABLED_IN_MISSION()
	RETURN IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_ENABLE_INTERACTION_MENU_DEPLOY_IN_MISSIONS)
ENDFUNC 

PROC SET_ENABLE_DRONE_USE_FROM_PI_MENU_IN_MISSION(BOOL bEnable)
	IF bEnable 
		IF NOT IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_ENABLE_INTERACTION_MENU_DEPLOY_IN_MISSIONS)
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_ENABLE_INTERACTION_MENU_DEPLOY_IN_MISSIONS)
			PRINTLN("[AM_MP_DRONE] - SET_ENABLE_DRONE_USE_FROM_PI_MENU_IN_MISSION TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_ENABLE_INTERACTION_MENU_DEPLOY_IN_MISSIONS)
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_ENABLE_INTERACTION_MENU_DEPLOY_IN_MISSIONS)
			PRINTLN("[AM_MP_DRONE] - SET_ENABLE_DRONE_USE_FROM_PI_MENU_IN_MISSION FALSE")
		ENDIF
	ENDIF
ENDPROC


PROC SET_TERMINATE_REMOTE_DRONE_SCRIPT(BOOL bTerminate)
	IF bTerminate 
		IF NOT IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_TERMINATE_REMOTE_DRONE_SCRIPT)
			SET_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_TERMINATE_REMOTE_DRONE_SCRIPT)
			PRINTLN("[AM_MP_DRONE] - SET_TERMINATE_REMOTE_DRONE_SCRIPT TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_TERMINATE_REMOTE_DRONE_SCRIPT)
			CLEAR_BIT(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_TERMINATE_REMOTE_DRONE_SCRIPT)
			PRINTLN("[AM_MP_DRONE] - SET_TERMINATE_REMOTE_DRONE_SCRIPT FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_STARTED_REMOTE_DRONE_SCRIPT(PLAYER_INDEX playerToCheck)
	IF playerToCheck != INT_TO_NATIVE(PLAYER_INDEX, -1)
		RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iArmourAircraftModBS, PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_STARTED_REMOTE_DRONE_SCRIPT)
	ENDIF	
	RETURN FALSE
ENDFUNC 

PROC SET_PLAYER_STARTED_REMOTE_DRONE_SCRIPT(BOOL bStart)
	IF bStart
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iArmourAircraftModBS, PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_STARTED_REMOTE_DRONE_SCRIPT)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iArmourAircraftModBS, PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_STARTED_REMOTE_DRONE_SCRIPT)
			PRINTLN("SET_PLAYER_STARTED_REMOTE_DRONE_SCRIPT - TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iArmourAircraftModBS, PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_STARTED_REMOTE_DRONE_SCRIPT)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iArmourAircraftModBS, PROPERTY_BROADCAST_ARMOUR_AIRCRAFT_STARTED_REMOTE_DRONE_SCRIPT)
			PRINTLN("SET_PLAYER_STARTED_REMOTE_DRONE_SCRIPT - FALSE")
		ENDIF
	ENDIF
ENDPROC
