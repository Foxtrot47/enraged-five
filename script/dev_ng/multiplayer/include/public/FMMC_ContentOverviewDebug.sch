#IF IS_DEBUG_BUILD
USING "mp_globals_fm.sch"
USING "fmmc_vars.sch"
USING "FMMC_OnscreenRunTimeErrors.sch"
USING "FMMC_ContentOverviewDebug_Utility.sch"

USING "building_control_private.sch"

CONTENT_OVERVIEW_DEBUG_STRUCT	sContentOverviewDebug

TYPEDEF PROC SPECIFIC_CONTENT_OVERVIEW_DEBUG_WINDOW_CALLS()
TYPEDEF PROC SPECIFIC_CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_CALLS()

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Mission State Debug Window -----------------------------------------------------
// ##### Description: Debug functions for the mission state debug window ------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

FUNC INT GET_SAFE_DEBUG_WINDOW_CURRENT_PAGE(CONTENT_OVERVIEW_DEBUG_WINDOW eDebugWindowPrevious)	
	IF eDebugWindowPrevious >= CONTENT_OVERVIEW_DEBUG_WINDOW_NONE
	OR eDebugWindowPrevious < CONTENT_OVERVIEW_DEBUG_WINDOW_MAX
		RETURN sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(eDebugWindowPrevious)]
	ENDIF	
	RETURN 0
ENDFUNC
PROC PASS_OVER_PREVIOUS_DEBUG_WINDOW_INFORMATION(INT iScriptIndex, CONTENT_OVERVIEW_DEBUG_WINDOW eDebugWindow, CONTENT_OVERVIEW_DEBUG_WINDOW eDebugWindowPrevious)
	IF iScriptIndex >= ciCONTENT_OVERVIEW_DEBUG_WINDOW_SCRIPT__MAX
	OR iScriptIndex < 0
		EXIT
	ENDIF
	g_iDebugWindowScriptHashToApplyTo[iScriptIndex] = GET_HASH_OF_THIS_SCRIPT_NAME()
	g_eDebugWindow_Saved[iScriptIndex] = eDebugWindow
	g_eDebugWindowPrevious_Saved[iScriptIndex] = eDebugWindowPrevious
	g_iDebugWindowPage_Saved[iScriptIndex] = GET_SAFE_DEBUG_WINDOW_CURRENT_PAGE(eDebugWindowPrevious)
ENDPROC
PROC SET_DEFAULT_DEBUG_WINDOW_FOR_SCRIPT(CONTENT_OVERVIEW_DEBUG_WINDOW eDebugWindow)
	sContentOverviewDebug.eDebugWindowDefault = eDebugWindow
ENDPROC

PROC PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_ACTIVATION()
		
	IF NOT HAS_NET_TIMER_STARTED(sContentOverviewDebug.tdToggleEntitiesDueToTooMuchTextDraws)
		START_NET_TIMER(sContentOverviewDebug.tdToggleEntitiesDueToTooMuchTextDraws)
	ENDIF
	
	START_NET_TIMER(sContentOverviewDebug.tdWindowActivationDelay)	
	IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sContentOverviewDebug.tdWindowActivationDelay, 3000)
	
		// Reapply the Page and Tab we were on now that we've relaunched the script.
		INT i = 0
		FOR i = 0 TO ciCONTENT_OVERVIEW_DEBUG_WINDOW_SCRIPT__MAX-1
			IF g_iDebugWindowScriptHashToApplyTo[i] = GET_HASH_OF_THIS_SCRIPT_NAME()
				g_iDebugWindowScriptHashToApplyTo[i] = 0						
				
				IF g_eDebugWindow_Saved[i] = CONTENT_OVERVIEW_DEBUG_WINDOW_NONE
					sContentOverviewDebug.eDebugWindowPrevious = g_eDebugWindowPrevious_Saved[i]				
					IF sContentOverviewDebug.eDebugWindowPrevious >= CONTENT_OVERVIEW_DEBUG_WINDOW_NONE
					OR sContentOverviewDebug.eDebugWindowPrevious < CONTENT_OVERVIEW_DEBUG_WINDOW_MAX		
						sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindowPrevious)] = g_iDebugWindowPage_Saved[i]
					ENDIF
				ELSE
					sContentOverviewDebug.eDebugWindow = g_eDebugWindow_Saved[i]
					sContentOverviewDebug.eDebugWindowPrevious = g_eDebugWindow_Saved[i]
					IF sContentOverviewDebug.eDebugWindow >= CONTENT_OVERVIEW_DEBUG_WINDOW_NONE
					OR sContentOverviewDebug.eDebugWindow < CONTENT_OVERVIEW_DEBUG_WINDOW_MAX		
						sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = g_iDebugWindowPage_Saved[i]
					ENDIF
				ENDIF
				
				g_eDebugWindow_Saved[i] = CONTENT_OVERVIEW_DEBUG_WINDOW_NONE
				g_eDebugWindowPrevious_Saved[i] = CONTENT_OVERVIEW_DEBUG_WINDOW_NONE
			ENDIF
		ENDFOR
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_6, KEYBOARD_MODIFIER_NONE, "EntityState")
	OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_MULTIPLY)
		IF sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_NONE			
			IF sContentOverviewDebug.eDebugWindowPrevious != CONTENT_OVERVIEW_DEBUG_WINDOW_NONE
				sContentOverviewDebug.eDebugWindow = sContentOverviewDebug.eDebugWindowPrevious
			ELSE	
				sContentOverviewDebug.eDebugWindow = sContentOverviewDebug.eDebugWindowDefault				
			ENDIF			
		ELSE
			IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_ClearMode)
				SET_BIT(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_ClearMode)
			ELSE
				CLEAR_BIT(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_ClearMode)
				sContentOverviewDebug.eDebugWindowPrevious = sContentOverviewDebug.eDebugWindow
				sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_NONE
			ENDIF
		ENDIF
	ENDIF
	
	IF sContentOverviewDebug.eDebugWindow != CONTENT_OVERVIEW_DEBUG_WINDOW_NONE
		BOOL bIncremented
		INT iTemp = ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_RIGHT, KEYBOARD_MODIFIER_NONE, "EntityState+")			
			WHILE NOT IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[iTemp / 32], iTemp % 32) OR NOT bIncremented
				bIncremented = TRUE
				iTemp--
				IF iTemp <= ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_NONE)
					iTemp = ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_MAX)-1					
				ENDIF
			ENDWHILE
		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_LEFT, KEYBOARD_MODIFIER_NONE, "EntityState-")			
			WHILE NOT IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[iTemp / 32], iTemp % 32) OR NOT bIncremented
				bIncremented = TRUE
				iTemp++
				IF iTemp >= ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_MAX)
					iTemp = ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_NONE)+1					
				ENDIF
			ENDWHILE
		ENDIF
		sContentOverviewDebug.eDebugWindow = INT_TO_ENUM(CONTENT_OVERVIEW_DEBUG_WINDOW, iTemp)
		
		IF sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] > 1
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_UP, KEYBOARD_MODIFIER_NONE, "EntityStatePage+")			
				sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]--
				IF sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] < 0
					sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]-1
				ENDIF
			ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_DOWN, KEYBOARD_MODIFIER_NONE, "EntityStatePage-")
				sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
				IF sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] >= sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
					sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC GET_MISSION_ENTITY_WINDOW_COLOURS_FOR_CREATION_TYPE(CONTENT_OVERVIEW_DEBUG_WINDOW eDebugWindow, INT &iRedReturn, INT &iGreenReturn, INT &iBlueReturn, INT &iAlphaReturn)
	SWITCH eDebugWindow
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_NONE
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_MISSION_OVERVIEW
			iRedReturn		= 255
			iGreenReturn	= 255
			iBlueReturn		= 255
			iAlphaReturn	= 100
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_RUNTIME_ERRORS
			iRedReturn		= 255
			iGreenReturn	= 255
			iBlueReturn		= 255
			iAlphaReturn	= 100
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
			iRedReturn		= 255
			iGreenReturn	= 255
			iBlueReturn		= 255
			iAlphaReturn	= 100
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
			iRedReturn		= 255
			iGreenReturn	= 255
			iBlueReturn		= 255
			iAlphaReturn	= 100
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS
			iRedReturn		= 255
			iGreenReturn	= 255
			iBlueReturn		= 255
			iAlphaReturn	= 100
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE
			iRedReturn		= 255
			iGreenReturn	= 255
			iBlueReturn		= 255
			iAlphaReturn	= 100
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP
			iRedReturn		= 255
			iGreenReturn	= 255
			iBlueReturn		= 255
			iAlphaReturn	= 100
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP
			iRedReturn		= 255
			iGreenReturn	= 255
			iBlueReturn		= 255
			iAlphaReturn	= 100
		BREAK		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES
			iRedReturn		= 255
			iGreenReturn	= 255
			iBlueReturn		= 255
			iAlphaReturn	= 100
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS
			iRedReturn		= 255
			iGreenReturn	= 255
			iBlueReturn		= 255
			iAlphaReturn	= 100
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
			iRedReturn		= 255
			iGreenReturn	= 255
			iBlueReturn		= 255
			iAlphaReturn	= 100
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS
			iRedReturn		= 255
			iGreenReturn	= 255
			iBlueReturn		= 255
			iAlphaReturn	= 100
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_WARP_PORTALS
			iRedReturn		= 255
			iGreenReturn	= 255
			iBlueReturn		= 255
			iAlphaReturn	= 100
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_GANG_CHASE
			iRedReturn		= 255
			iGreenReturn	= 255
			iBlueReturn		= 255
			iAlphaReturn	= 100
		BREAK
		DEFAULT
			iRedReturn		= 255
			iGreenReturn	= 255
			iBlueReturn		= 255
			iAlphaReturn	= 100
		BREAK
	ENDSWITCH	
ENDPROC

PROC GET_MISSION_ENTITY_OVERVIEW_WINDOW_ACCENT_COLOURS_FOR_CREATION_TYPE(CONTENT_OVERVIEW_DEBUG_WINDOW eDebugWindow, INT &iRedReturn, INT &iGreenReturn, INT &iBlueReturn, INT &iAlphaReturn)
	SWITCH eDebugWindow
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_NONE
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_MISSION_OVERVIEW
			iRedReturn		= 255
			iGreenReturn	= 255
			iBlueReturn		= 255
			iAlphaReturn	= 180
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
			iRedReturn		= 225
			iGreenReturn	= 100
			iBlueReturn		= 100
			iAlphaReturn	= 180
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_RUNTIME_ERRORS
			iRedReturn		= 225
			iGreenReturn	= 50
			iBlueReturn		= 50
			iAlphaReturn	= 180
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
			iRedReturn		= 100
			iGreenReturn	= 100
			iBlueReturn		= 225
			iAlphaReturn	= 180
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS
			iRedReturn		= 100
			iGreenReturn	= 225
			iBlueReturn		= 100
			iAlphaReturn	= 180
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE
			iRedReturn		= 15
			iGreenReturn	= 100
			iBlueReturn		= 15
			iAlphaReturn	= 180
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP
			iRedReturn		= 180
			iGreenReturn	= 140
			iBlueReturn		= 225
			iAlphaReturn	= 180
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP
			iRedReturn		= 190
			iGreenReturn	= 150
			iBlueReturn		= 240
			iAlphaReturn	= 180
		BREAK		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES
			iRedReturn		= 215
			iGreenReturn	= 150
			iBlueReturn		= 70
			iAlphaReturn	= 180
		BREAK	
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS
			iRedReturn		= 180
			iGreenReturn	= 170
			iBlueReturn		= 25
			iAlphaReturn	= 180
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
			iRedReturn		= 45
			iGreenReturn	= 45
			iBlueReturn		= 215
			iAlphaReturn	= 180
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS
			iRedReturn		= 145
			iGreenReturn	= 145
			iBlueReturn		= 215
			iAlphaReturn	= 180
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_WARP_PORTALS
			iRedReturn		= 110
			iGreenReturn	= 110
			iBlueReturn		= 230
			iAlphaReturn	= 180
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_GANG_CHASE
			iRedReturn		= 110
			iGreenReturn	= 110
			iBlueReturn		= 230
			iAlphaReturn	= 180
		BREAK
		DEFAULT 
			iRedReturn		= 255
			iGreenReturn	= 255
			iBlueReturn		= 255
			iAlphaReturn	= 180
		BREAK
	ENDSWITCH	
ENDPROC

// Only necessary for some entity types with excessive item counts like props (200).
FUNC INT GET_NUMBER_OF_ITEM_PAGES(CONTENT_OVERVIEW_DEBUG_WINDOW eDebugWindowIn)
	SWITCH eDebugWindowIn
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP
			RETURN 1
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
			RETURN 1
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC INT GET_NUMBER_OF_COLUMNS_FOR_ITEMS(CONTENT_OVERVIEW_DEBUG_WINDOW eDebugWindowIn, INT iPageModifier = -1)
	SWITCH eDebugWindowIn
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_MISSION_OVERVIEW
			IF iPageModifier = 1
			OR iPageModifier = 2
				RETURN 4
			ENDIF
			RETURN 3
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_RUNTIME_ERRORS
			RETURN 3
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
			RETURN 4
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
			RETURN 4
		BREAK		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS
			RETURN 4
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE
			RETURN 4
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP
			RETURN 4
		BREAK		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP
			RETURN 4
		BREAK			
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES
			RETURN 4
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS
			RETURN 4
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
			RETURN 3
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS
			RETURN 4
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_WARP_PORTALS
			RETURN 4
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_GANG_CHASE
			RETURN 4
		BREAK
	ENDSWITCH
	
	RETURN 4
ENDFUNC

FUNC INT GET_NUMBER_ITEMS_FOR_COLUMNS(CONTENT_OVERVIEW_DEBUG_WINDOW eDebugWindowIn)
	SWITCH eDebugWindowIn
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_MISSION_OVERVIEW
			IF sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_MISSION_OVERVIEW)] = 5
				RETURN 21
			ELSE
				RETURN 20
			ENDIF
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_RUNTIME_ERRORS
			RETURN 12
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
			RETURN (FMMC_MAX_PEDS/GET_NUMBER_OF_COLUMNS_FOR_ITEMS(eDebugWindowIn))
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
			RETURN (FMMC_MAX_VEHICLES/GET_NUMBER_OF_COLUMNS_FOR_ITEMS(eDebugWindowIn))
		BREAK		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS
			RETURN (FMMC_MAX_NUM_OBJECTS/GET_NUMBER_OF_COLUMNS_FOR_ITEMS(eDebugWindowIn))
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE
			RETURN (FMMC_MAX_NUM_INTERACTABLES/GET_NUMBER_OF_COLUMNS_FOR_ITEMS(eDebugWindowIn))
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP
			RETURN (FMMC_MAX_NUM_DYNOPROPS/GET_NUMBER_OF_COLUMNS_FOR_ITEMS(eDebugWindowIn))
		BREAK		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP
			RETURN 40
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES
			RETURN (FMMC_MAX_NUM_ZONES/GET_NUMBER_OF_COLUMNS_FOR_ITEMS(eDebugWindowIn))
		BREAK	
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS
			RETURN (FMMC_MAX_GO_TO_LOCATIONS/GET_NUMBER_OF_COLUMNS_FOR_ITEMS(eDebugWindowIn))
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
			RETURN 20
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS
			RETURN 20
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_WARP_PORTALS
			RETURN (FMMC_MAX_WARP_PORTALS/GET_NUMBER_OF_COLUMNS_FOR_ITEMS(eDebugWindowIn))
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_GANG_CHASE
			RETURN 10
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DM_MODIFIERS
			RETURN 14
		BREAK
	ENDSWITCH
	
	RETURN (FMMC_MAX_PEDS/GET_NUMBER_OF_COLUMNS_FOR_ITEMS(eDebugWindowIn))
ENDFUNC

FUNC FLOAT GET_CONTENT_OVERVIEW_DEBUG_WINDOW_Y_ITEM_SPACING_SIZE(CONTENT_OVERVIEW_DEBUG_WINDOW eDebugWindowIn)
	SWITCH eDebugWindowIn
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_MISSION_OVERVIEW
			RETURN 0.025
		BREAK		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
			RETURN 0.025
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
			RETURN 0.025
		BREAK		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS
			RETURN 0.025
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE
			RETURN 0.025
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP
			RETURN 0.025
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP
			RETURN 0.0125
		BREAK		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES
			RETURN 0.025
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS
			RETURN 0.025
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
			RETURN 0.025
		BREAK		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS
			RETURN 0.025
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_WARP_PORTALS
			RETURN 0.025
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_RUNTIME_ERRORS
			RETURN 0.025
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_CUSTOM_DEBUG
			RETURN 0.025
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_GANG_CHASE
			RETURN 0.025
		BREAK
	ENDSWITCH
	
	RETURN 0.025
ENDFUNC

PROC GET_CONTENT_OVERVIEW_DEBUG_WINDOW_X_AND_Y(FLOAT &fReturnX, FLOAT &fReturnY)
	
	fReturnX = 0.045
	fReturnY = 0.05
		
	PROCESS_DEBUG_MOVER_FOR_TYPE(sContentOverviewDebug.sDebugMoversStruct, ciDEBUG_ON_SCREEN_MOVERS_ENTITY_STATE, fReturnX, fReturnY, 0.05, 0.03, FALSE, TRUE, FALSE)
	
	fReturnY += 0.05
	
ENDPROC

PROC GET_CONTENT_OVERVIEW_DEBUG_WINDOW_SIZEX_AND_SIZEY(FLOAT &fReturnX, FLOAT &fReturnY, CONTENT_OVERVIEW_DEBUG_WINDOW eDebugWindowIn)
	fReturnX = 0.9
	fReturnY = GET_CONTENT_OVERVIEW_DEBUG_WINDOW_Y_ITEM_SPACING_SIZE(eDebugWindowIn) * (1+GET_NUMBER_ITEMS_FOR_COLUMNS(eDebugWindowIn)) + 0.015
	
	/*FIXHERE IF bShowProfiler
		fX = 0.7
	ENDIF*/
ENDPROC

PROC GET_CONTENT_OVERVIEW_DEBUG_WINDOW_SIZEX_AND_SIZEY_TWO(FLOAT &fReturnX, FLOAT &fReturnY)	
	fReturnX = 0.9
	fReturnY = 0.125
	
	/*FIXHERE IF bShowProfiler
		fX = 0.7
	ENDIF*/
ENDPROC

PROC GET_CONTENT_OVERVIEW_DEBUG_WINDOW_STATUS_RECT_SIZE_X_AND_Y(FLOAT &fReturnX, FLOAT &fReturnY)
	fReturnX = 0.0055
	fReturnY = 0.009
ENDPROC

FUNC BOOL CAN_ACTIVATE_STATE_DEBUG_WINDOW_RECT_CLICK_EVENT()
		
	IF sContentOverviewDebug.eEntityStateClickEvent != CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_NONE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_HOVERING_OVER_STATE_DEBUG_WINDOW_RECT_ALREADY_GOT_SELECTION()
		
	IF sContentOverviewDebug.iEntityIndexSelected != -1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_HOVERING_OVER_DEBUG_WINDOW_RECT(FLOAT fPosX, FLOAT fPosY, FLOAT fSizeX, FLOAT fSizeY, FLOAT &fXReturn, FLOAT &fYReturn)
	
	FLOAT fMouseX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
	FLOAT fMouseY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
	
	FLOAT fRectX_Max = fPosX + fSizeX/2
	FLOAT fRectY_Max = fPosY + fSizeY/2
	FLOAT fRectX_Min = fPosX - fSizeX/2
	FLOAT fRectY_Min = fPosY - fSizeY/2
	
	IF fMouseX >= fRectX_Min AND fMouseX <= fRectX_Max
	AND fMouseY >= fRectY_Min AND fMouseY <= fRectY_Max
		fXReturn = fMouseX
		fYReturn = fMouseY
		RETURN TRUE	
	ENDIF
	
	RETURN FALSE
ENDFUNC

// Wraps the rectangle around an entity row.
PROC SCALE_DOWN_HOVERING_OVER_STATE_DEBUG_WINDOW_ENTITY_REGION(FLOAT &fEntityButtonX, FLOAT &fEntityButtonY, FLOAT &fEntityButtonSizeX, FLOAT &fEntityButtonSizeY, FLOAT &fEntityNamePosX, FLOAT &fEntityNamePosY, FLOAT &fEntityListColumnSpacingBase)
	
	fEntityButtonX = fEntityNamePosX+(fEntityListColumnSpacingBase/2)
	fEntityButtonY = fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText
	fEntityButtonSizeX = fEntityListColumnSpacingBase*0.95
	fEntityButtonSizeY = sContentOverviewDebug.fYSpacing
	
ENDPROC

// Wraps the rectangle as a smaller button in the corner of the entity region.
PROC SCALE_DOWN_HOVERING_OVER_STATE_DEBUG_WINDOW_ENTITY_BUTTON_REGION(INT iButtonPlace, FLOAT &fPosX, FLOAT &fPosY, FLOAT &fSizeX, FLOAT &fSizeY, FLOAT &fEntityListColumnSpacingBase)
			
	IF iButtonPlace >= ciCONTENT_OVERVIEW_DEBUG_BUTTON_NARROW_INCREMENTING_LEFT_TO_RIGHT
	AND iButtonPlace <= ciCONTENT_OVERVIEW_DEBUG_BUTTON_NARROW_INCREMENTING_LEFT_TO_RIGHT_END
		fSizeX *= 0.07
		fSizeY *= 0.7
		fPosX += -(fEntityListColumnSpacingBase/2)
		fPosX += 0.0075
		fPosX += (fSizeX*1.3)*(iButtonPlace-ciCONTENT_OVERVIEW_DEBUG_BUTTON_NARROW_INCREMENTING_LEFT_TO_RIGHT)
		EXIT
	ENDIF
	
	SWITCH iButtonPlace
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_LEFT
			fPosX += fSizeX*0.375
			fPosY -= fSizeY*0.2
			fSizeX *= 0.06
			fSizeY *= 0.4
		BREAK
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_RIGHT
			fPosX += fSizeX*0.45
			fPosY -= fSizeY*0.2
			fSizeX *= 0.06
			fSizeY *= 0.4
		BREAK
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_BOTTOM_LEFT
			fPosX += fSizeX*0.375
			fPosY += fSizeY*0.2
			fSizeX *= 0.06
			fSizeY *= 0.4
		BREAK
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_BOTTOM_RIGHT
			fPosX += fSizeX*0.45
			fPosY += fSizeY*0.2
			fSizeX *= 0.06
			fSizeY *= 0.4
		BREAK
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_CENTER_LARGE
			//fPosX += fSizeX*0.45
			//fPosY += fSizeY*0.2
			fSizeX *= 2.0
			fSizeY *= 0.8
		BREAK
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_CENTER_LARGE_WIDE
			//fPosX += fSizeX*0.45
			//fPosY += fSizeY*0.2
			fSizeX *= 4.0
			fSizeY *= 0.8
		BREAK
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_CENTER_NARROW
			fSizeX *= 0.7
			fSizeY *= 0.8
		BREAK
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_PREREQ_COLUMN_1_OF_10_NARROW
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_PREREQ_COLUMN_2_OF_10_NARROW
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_PREREQ_COLUMN_3_OF_10_NARROW		
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_PREREQ_COLUMN_4_OF_10_NARROW
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_PREREQ_COLUMN_5_OF_10_NARROW
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_PREREQ_COLUMN_6_OF_10_NARROW
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_PREREQ_COLUMN_7_OF_10_NARROW
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_PREREQ_COLUMN_8_OF_10_NARROW
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_PREREQ_COLUMN_9_OF_10_NARROW
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_PREREQ_COLUMN_10_OF_10_NARROW			
			fSizeX *= 0.07
			fSizeY *= 0.7
			fPosX += -(fEntityListColumnSpacingBase/2)
			fPosX += 0.0075			
			fPosX += (fSizeX*1.8)*(iButtonPlace-ciCONTENT_OVERVIEW_DEBUG_BUTTON_PREREQ_COLUMN_1_OF_10_NARROW)
		BREAK
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_1
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_2
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_3
		CASE ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_4
			fPosX += (fSizeX*0.315 + (0.015*(iButtonPlace-ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_1)))
			//fPosY -= fSizeY*0.2
			fSizeX *= 0.065
			fSizeY *= 0.6
		BREAK	
		DEFAULT 
			fPosX += fSizeX*0.45
			fPosY += fSizeY*0.2
			fSizeX *= 0.06
			fSizeY *= 0.4
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL IS_DEBUG_MOUSE_CLICK_JUST_PRESSED(MOUSE_BUTTON eMB)
	
	IF HAS_NET_TIMER_STARTED(sContentOverviewDebug.tdMouseClickTimer)
	AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(sContentOverviewDebug.tdMouseClickTimer, 150)
		RETURN FALSE
	ENDIF
	
	// TODO - This is unreliable for some reason. Sometimes not detecting the press, including IS_MOUSE_BUTTON_PRESSED. Investigate a better way. Note: INPUT_CURSOR_ACCEPT does not work while in Debug Keyboard.
	IF IS_MOUSE_BUTTON_JUST_PRESSED(eMB)	
	//OR IS_MOUSE_BUTTON_JUST_RELEASED(eMB)
	//OR IS_MOUSE_BUTTON_PRESSED(eMB)
		REINIT_NET_TIMER(sContentOverviewDebug.tdMouseClickTimer)
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

PROC PROCESS_STATE_DEBUG_WINDOW_CLICK_EVENT(CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT eClick)
		
	IF NOT CAN_ACTIVATE_STATE_DEBUG_WINDOW_RECT_CLICK_EVENT()
		EXIT
	ENDIF
	
	SWITCH eClick 
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_CHANGE_TAB
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_PLAY_OR_TRIGGER
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_RESET
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_HIDE_LEGEND
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_DRAW_MARKER_DEBUG
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_CACHE_PLAYER_POS_INDEX
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_PLAYER_CACHE_INDEX
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_PLAYER_CACHE_INDEX
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_PLAYER_INDEX
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_PLAYER_INDEX
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_DELETE_OR_REMOVE
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_SELECT_TEAM
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_SELECT_PAGE
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_INCREMENT_PAGE
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_DECREMENT_PAGE
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_ENABLE_ALL_WINDOW_DEBUG
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_MUTE_DIALOGUE_HANDLER
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_MUTE_MUSIC
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_PRE_REQ
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_SPAWN_GROUP
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_SPAWN_GROUP_SUBGROUP
			IF NOT IS_DEBUG_MOUSE_CLICK_JUST_PRESSED(MB_LEFT_BTN)
				EXIT
			ENDIF		
		BREAK
	ENDSWITCH
	
	sContentOverviewDebug.eEntityStateClickEvent = eClick
	
ENDPROC

FUNC BOOL DOES_CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_REQUIRE_INDEX()
	
	IF sContentOverviewDebug.eEntityStateClickEvent = CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_HIDE_LEGEND
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC STRING GET_NAME_OF_ENTITY_CLICK_EVENT_REGION(CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT eClick)
	SWITCH eClick 		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_PLAYER_CACHE_INDEX
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_PLAYER_INDEX
			RETURN "Warp Player"
		BREAK		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_PLAYER_CACHE_INDEX
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_PLAYER_INDEX
			RETURN "Warp Camera"
		BREAK		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_PLAY_OR_TRIGGER
			RETURN "Play"
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_RESET
			RETURN "Reset"
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_HIDE_LEGEND
			RETURN "Toggle Legend"
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_DRAW_MARKER_DEBUG		
			RETURN "Draw Debug"
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_CACHE_PLAYER_POS_INDEX
			RETURN "Cache Position"
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_DELETE_OR_REMOVE
			RETURN "Delete/Remove"
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_SELECT_TEAM
			RETURN "Select Team"
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_SELECT_PAGE
			RETURN "Select Page"
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_INCREMENT_PAGE
			RETURN "Next Page"
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_DECREMENT_PAGE
			RETURN "Prev Page"
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_ENABLE_ALL_WINDOW_DEBUG
			RETURN "Enable Debug"
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_MUTE_DIALOGUE_HANDLER
			RETURN "Toggle Mute Dialogue"
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_MUTE_MUSIC
			RETURN "Toggle Mute Music"
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_PRE_REQ
			RETURN "Toggle Pre-req"
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_SPAWN_GROUP		
			RETURN "Toggle Spawn Group"
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_SPAWN_GROUP_SUBGROUP
			RETURN "Toggle Sub Spawn Group"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC DRAW_CONTENT_OVERVIEW_DEBUG_BUTTON_TEXT(FLOAT fPosX, FLOAT fPosY, FLOAT fTextSize, STRING sText)
	IF fTextSize = 0.0
	OR IS_STRING_NULL_OR_EMPTY(sText)
		EXIT
	ENDIF
	SET_TEXT_SCALE(fTextSize, fTextSize)
	SET_TEXT_COLOUR(255, 255, 255, 255)	
	SET_TEXT_DROPSHADOW(2, 0, 0, 0, 175)
	SET_TEXT_JUSTIFICATION(FONT_CENTRE)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fPosX, fPosY, "STRING", sText)
ENDPROC

PROC DRAW_STATE_DEBUG_WINDOW_CLICK_EVENT_REGION_NOT_HOVERED(FLOAT fPosX, FLOAT fPosY, FLOAT fSizeX, FLOAT fSizeY, MENU_ICON_TYPE MenuIcon, BOOL bisTurnedOnParam)
	IF bisTurnedOnParam
		IF MenuIcon = MENU_ICON_BOX_EMPTY
			DRAW_RECT(fPosX, fPosY, fSizeX/2, fSizeY/2, 255, 255, 255, 100)
		ELIF MenuIcon = MENU_ICON_DUMMY
			DRAW_RECT(fPosX, fPosY, fSizeX, fSizeY, 100, 255, 100, 50)	
		ENDIF
	ENDIF	
	IF MenuIcon != MENU_ICON_DUMMY
		DRAW_SPRITE(GET_MENU_ICON_TXD(MenuIcon), GET_MENU_ICON_TEXTURE(MenuIcon, FALSE), fPosX, fPosY, fSizeX*2, fSizeY*2, 0.0, 255, 255, 255, 120)
	ELSE
		DRAW_RECT(fPosX, fPosY, fSizeX, fSizeY, 255, 255, 255, 20)
	ENDIF	
ENDPROC

PROC DRAW_STATE_DEBUG_WINDOW_CLICK_EVENT_REGION_HOVERED(FLOAT fPosX, FLOAT fPosY, FLOAT fSizeX, FLOAT fSizeY, MENU_ICON_TYPE MenuIcon, BOOL bisTurnedOnParam)
	IF bisTurnedOnParam
		IF MenuIcon = MENU_ICON_BOX_EMPTY
			DRAW_RECT(fPosX, fPosY, fSizeX/2, fSizeY/2, 100, 255, 100, 100)
		ELIF MenuIcon = MENU_ICON_DUMMY
			DRAW_RECT(fPosX, fPosY, fSizeX, fSizeY, 100, 255, 100, 50)
		ENDIF
	ELSE
		IF MenuIcon = MENU_ICON_BOX_EMPTY
			DRAW_RECT(fPosX, fPosY, fSizeX/2, fSizeY/2, 100, 255, 100, 20)
		ENDIF
	ENDIF	
	IF MenuIcon != MENU_ICON_DUMMY
		DRAW_SPRITE(GET_MENU_ICON_TXD(MenuIcon), GET_MENU_ICON_TEXTURE(MenuIcon, FALSE), fPosX, fPosY, fSizeX*2, fSizeY*2, 0.0, 100, 255, 100, 120)
	ELSE
		DRAW_RECT(fPosX, fPosY, fSizeX, fSizeY, 255, 255, 255, 50)
	ENDIF
ENDPROC

FUNC BOOL IS_STATE_DEBUG_WINDOW_CLICK_EVENT_REGION_HOVERED(INT iEntityIndex, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT eClickEvent, CONTENT_OVERVIEW_DEBUG_WINDOW eDebugWindow, FLOAT fPosX, FLOAT fPosY, FLOAT fSizeX, FLOAT fSizeY, BOOL bDrawWhenNotHovering, STRING sButtonText, FLOAT fTextSize = 0.0, MENU_ICON_TYPE MenuIcon = MENU_ICON_DUMMY, BOOL bisTurnedOnParam = FALSE)
	
	IF HAS_HOVERING_OVER_STATE_DEBUG_WINDOW_RECT_ALREADY_GOT_SELECTION()		
		IF bDrawWhenNotHovering
			DRAW_STATE_DEBUG_WINDOW_CLICK_EVENT_REGION_NOT_HOVERED(fPosX, fPosY, fSizeX, fSizeY, MenuIcon, bisTurnedOnParam)
			DRAW_CONTENT_OVERVIEW_DEBUG_BUTTON_TEXT(fPosX, fPosY-(fSizeY*0.5), fTextSize, sButtonText)
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	FLOAT fMouseX, fMouseY
	
	IF NOT IS_HOVERING_OVER_DEBUG_WINDOW_RECT(fPosX, fPosY, fSizeX, fSizeY, fMouseX, fMouseY)
		IF bDrawWhenNotHovering	
			DRAW_STATE_DEBUG_WINDOW_CLICK_EVENT_REGION_NOT_HOVERED(fPosX, fPosY, fSizeX, fSizeY, MenuIcon, bisTurnedOnParam)
			DRAW_CONTENT_OVERVIEW_DEBUG_BUTTON_TEXT(fPosX, fPosY-(fSizeY*0.5), fTextSize, sButtonText)		
		ENDIF
		RETURN FALSE
	ENDIF
	
	// Hovered
	DRAW_STATE_DEBUG_WINDOW_CLICK_EVENT_REGION_HOVERED(fPosX, fPosY, fSizeX, fSizeY, MenuIcon, bisTurnedOnParam)
	DRAW_CONTENT_OVERVIEW_DEBUG_BUTTON_TEXT(fPosX, fPosY-(fSizeY*0.5), fTextSize, sButtonText)
	
	// Mouse Cache
	sContentOverviewDebug.sEntityStateClickName = GET_NAME_OF_ENTITY_CLICK_EVENT_REGION(eClickEvent)
	sContentOverviewDebug.fEntityStateMouseX = fMouseX
	sContentOverviewDebug.fEntityStateMouseY = fMouseY
	sContentOverviewDebug.iEntityIndexSelected = iEntityIndex
	sContentOverviewDebug.eDebugWindowTypeSelected = eDebugWindow
			
	RETURN TRUE
	
ENDFUNC

PROC CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(INT iButtonPlace, INT iEntityIndex, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT eClickEvent, FLOAT fEntityNamePosX, FLOAT fEntityNamePosY, FLOAT fEntityListColumnSpacingBase, BOOL bDrawWhenNotHovering, STRING sButtonText, FLOAT fTextSize = 0.0, MENU_ICON_TYPE MenuIcon = MENU_ICON_BOX_EMPTY, BOOL bisTurnedOnParam = FALSE)
	FLOAT fEntityButtonX = 0
	FLOAT fEntityButtonY = 0
	FLOAT fEntityButtonSizeX = 0
	FLOAT fEntityButtonSizeY = 0
	SCALE_DOWN_HOVERING_OVER_STATE_DEBUG_WINDOW_ENTITY_REGION(fEntityButtonX, fEntityButtonY, fEntityButtonSizeX, fEntityButtonSizeY, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase)
	SCALE_DOWN_HOVERING_OVER_STATE_DEBUG_WINDOW_ENTITY_BUTTON_REGION(iButtonPlace, fEntityButtonX, fEntityButtonY, fEntityButtonSizeX, fEntityButtonSizeY, fEntityListColumnSpacingBase)		
	IF IS_STATE_DEBUG_WINDOW_CLICK_EVENT_REGION_HOVERED(iEntityIndex, eClickEvent, sContentOverviewDebug.eDebugWindow, fEntityButtonX, fEntityButtonY, fEntityButtonSizeX, fEntityButtonSizeY, bDrawWhenNotHovering, sButtonText, fTextSize, MenuIcon, bisTurnedOnParam)
		PROCESS_STATE_DEBUG_WINDOW_CLICK_EVENT(eClickEvent)
	ENDIF
ENDPROC

// Mission State Debug Helper Functions END #############


// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Entity State Windows				-----------------------------------------------
// ##### Description: Information related to Peds		-----------------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC REMOVE_UNDERSCORES_AND_REPLACE_WITH_PERIOD(TEXT_LABEL_63 &tl63)
	INT iStringLength = GET_LENGTH_OF_LITERAL_STRING(tl63)
	TEXT_LABEL_63 tl63Temp = ""
	STRING sChar
	INT iChar = 0
	FOR iChar = 0 TO iStringLength-1
		sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl63, iChar, iChar+1)
		
		IF ARE_STRINGS_EQUAL(sChar, "_") 
			sChar = "."
			tl63Temp += sChar
			RELOOP
		ENDIF
		
		tl63Temp += sChar
	ENDFOR		
	tl63 = tl63Temp
ENDPROC

PROC DRAW_ENTITY_DEBUG_WINDOW_TEXT(TEXT_LABEL_63 tl63_EntityName, FLOAT fEntityNamePosX, FLOAT fEntityNamePosY, FLOAT fBaseTextSizeIn, BOOL bForceSmallText = FALSE, BOOL bKeepUnderscores = FALSE, BOOL bMonospace = FALSE)
	
	IF NOT bKeepUnderscores
		REMOVE_UNDERSCORES_AND_REPLACE_WITH_PERIOD(tl63_EntityName)
	ENDIF
	
	IF GET_LENGTH_OF_LITERAL_STRING(tl63_EntityName) > 46
	OR bForceSmallText
		SET_TEXT_SCALE(fBaseTextSizeIn+0.02, fBaseTextSizeIn-0.02)
	ELSE
		SET_TEXT_SCALE(fBaseTextSizeIn+0.02, fBaseTextSizeIn+0.02)
	ENDIF
	SET_TEXT_COLOUR(255, 255, 255, 255)	
	SET_TEXT_DROPSHADOW(2, 0, 0, 0, 175)
	
	IF bMonospace
		SET_TEXT_FONT(FONT_STYLE_FIXED_WIDTH_NUMBERS)
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(tl63_EntityName)
		DISPLAY_TEXT_WITH_LITERAL_STRING(fEntityNamePosX, fEntityNamePosY, "STRING", tl63_EntityName)
	ENDIF
	
	IF bMonospace
		SET_TEXT_FONT(FONT_STANDARD)
	ENDIF
	
ENDPROC

PROC DRAW_LEGEND_DEBUG_WINDOW_TEXT(STRING sEntityName, FLOAT fBaseTextSizeIn, BOOL bForceSmallText, INT iIncrementSpacingX = 1, INT iIncrementSpacingY = 0, FLOAT fExtraX = 0.0, BOOL bKeepUnderscores = FALSE)
	
	TEXT_LABEL_63 tl63 = sEntityName
	
	DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63, fExtraX+sContentOverviewDebug.vLegendTextDrawPos.x+((sContentOverviewDebug.fRectSizeX2/4)*sContentOverviewDebug.iLegendPrintX), sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY), fBaseTextSizeIn, bForceSmallText, bKeepUnderscores)
	sContentOverviewDebug.iLegendPrintX += iIncrementSpacingX
	sContentOverviewDebug.iLegendPrintY += iIncrementSpacingY
	
ENDPROC

PROC DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW eDebugWindow, FLOAT fPosX, FLOAT fPosY, FLOAT fTabSizeX, FLOAT fTabSizeY, FLOAT fBaseTextSizeIn, STRING tl63, BOOL bSelected, INT &iTabX, INt &iTabY, FLOAT fXSpacing)
	TEXT_LABEL_63 tl63_Tab	
	SET_TEXT_SCALE(fBaseTextSizeIn, fBaseTextSizeIn)
	SET_TEXT_COLOUR(255, 255, 255, 255)	
	SET_TEXT_DROPSHADOW(2, 0, 0, 0, 175)
	SET_TEXT_JUSTIFICATION(FONT_CENTRE)
	IF bSelected
		tl63_Tab = "~Bold~"
		tl63_Tab += tl63		
		DRAW_RECT(fPosX - PICK_FLOAT(iTabY = 1, fXSpacing/2, 0.0), fPosY, fTabSizeX, fTabSizeY, sContentOverviewDebug.iRedType, sContentOverviewDebug.iGreenType, sContentOverviewDebug.iBlueType, 125)
		DRAW_SPRITE("CommonMenu", "Gradient_Bgd", fPosX - PICK_FLOAT(iTabY = 1, fXSpacing/2, 0.0), fPosY, fTabSizeX, fTabSizeY, 0.0, sContentOverviewDebug.iRedType, sContentOverviewDebug.iGreenType, sContentOverviewDebug.iBlueType, sContentOverviewDebug.iAlphaType)		
	ELSE
		tl63_Tab = tl63
		DRAW_SPRITE("CommonMenu", "Gradient_Bgd", fPosX - PICK_FLOAT(iTabY = 1, fXSpacing/2, 0.0), fPosY, fTabSizeX, fTabSizeY, 0.0, 200, 0, 0, 100)
	ENDIF
	
	IF eDebugWindow != CONTENT_OVERVIEW_DEBUG_WINDOW_NONE
		IF IS_STATE_DEBUG_WINDOW_CLICK_EVENT_REGION_HOVERED(0, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_CHANGE_TAB, eDebugWindow, fPosX - PICK_FLOAT(iTabY = 1, fXSpacing/2, 0.0), fPosY, fTabSizeX, fTabSizeY, TRUE, tl63_Tab, fBaseTextSizeIn)
			PROCESS_STATE_DEBUG_WINDOW_CLICK_EVENT(CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_CHANGE_TAB)
		ENDIF
	ELSE 
		DISPLAY_TEXT_WITH_LITERAL_STRING(fPosX - PICK_FLOAT(iTabY = 1, fXSpacing/2, 0.0), fPosY-(fTabSizeY*0.5), "STRING", tl63_Tab)	
	ENDIF
		
	iTabX++
	IF iTabX >= 9
		iTabX = 0
		iTabY++
	ENDIF	
	
ENDPROC

/* Keeping this here in case I decide to use / revisit. It made a bar/rect across a row but didn't really help readability.
		// Draw alternating bar effect
		DRAW_CONTENT_OVERVIEW_ALTERNATING_ROW_RECT_MAIN(sContentOverviewDebug.fRectPosX, fEntityNamePosY, sContentOverviewDebug.fRectSizeX, i, GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))	
PROC DRAW_CONTENT_OVERVIEW_ALTERNATING_ROW_RECT_MAIN(FLOAT fPosX, FLOAT fPosY, FLOAT fWidth, INT i, INT iNumberOfItemsPerColumn)			
	IF IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_ClearMode)
		EXIT
	ENDIF
	IF (i / iNumberOfItemsPerColumn) > 0
		EXIT
	ENDIF
	FLOAT fHeight = GET_RENDERED_CHARACTER_HEIGHT(sContentOverviewDebug.fBaseTextSize, FONT_STANDARD) // 0.0025 // GET_RENDERED_CHARACTER_HEIGHT(fBaseTextSizeIn+0.02, FONT_STANDARD)
	
	IF sContentOverviewDebug.iRedType = 255
	AND sContentOverviewDebug.iGreenType = 255
	AND sContentOverviewDebug.iBlueType = 255
		DRAW_RECT(fPosX, fPosY+sContentOverviewDebug.fBringRectInLineWithText, fWidth, fHeight, sContentOverviewDebug.iRedType, sContentOverviewDebug.iGreenType, sContentOverviewDebug.iBlueType, 18)
	ELSE
		DRAW_RECT(fPosX, fPosY+sContentOverviewDebug.fBringRectInLineWithText, fWidth, fHeight, sContentOverviewDebug.iRedType, sContentOverviewDebug.iGreenType, sContentOverviewDebug.iBlueType, 30)
	ENDIF
ENDPROC
*/
FUNC STRING GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(CONTENT_OVERVIEW_DEBUG_WINDOW eDebugWindow, BOOL bShort)
	
	IF bShort
		SWITCH eDebugWindow
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_MISSION_OVERVIEW
				RETURN "Content"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
				RETURN "Peds"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
				RETURN "Vehicles"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS
				RETURN "Objects"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE
				RETURN "Interacts"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP
				RETURN "DynoProps"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP
				RETURN "Props"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES
				RETURN "Zones"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS
				RETURN "Locations"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
				RETURN "D.Triggers"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS
				RETURN "T.Spawn"
			BREAK	
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_WARP_PORTALS
				RETURN "W.Portals"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_GANG_CHASE
				RETURN "G.Chase"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_RUNTIME_ERRORS
				RETURN "Run Errors"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_CUSTOM_DEBUG
				RETURN "Shared Dbg"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DM_MODIFIERS
				RETURN "DM Mods"
			BREAK
		ENDSWITCH		
	ELSE
		SWITCH eDebugWindow
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_MISSION_OVERVIEW
				RETURN "Content"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
				RETURN "Peds"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
				RETURN "Vehicles"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS
				RETURN "Objects"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE
				RETURN "Interactables"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP
				RETURN "Dyno-Props"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP
				RETURN "Props"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES
				RETURN "Zones"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS
				RETURN "Locations"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
				RETURN "Dialogue Triggers"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS
				RETURN "Team Spawn Points"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_WARP_PORTALS
				RETURN "Warp Portals"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_GANG_CHASE
				RETURN "Gang Chase"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_RUNTIME_ERRORS
				RETURN "Run Time Errors"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_CUSTOM_DEBUG
				RETURN "Shared Debug"
			BREAK
			CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DM_MODIFIERS
				RETURN "Deathmatch Modifiers"
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN "INVALID"
ENDFUNC

PROC PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_TABS()
		
	FLOAT fTabSizeX = 0.05
	FLOAT fTabSizeY = 0.024
	FLOAT fSpacingX = (fTabSizeX*1.05)
	FLOAT fExtraSpacingY = fTabSizeY + 0.0025
	INT iTabX, iTabY, iCustomTabX, iCustomTabY
	
	FLOAT fTabTextSize = sContentOverviewDebug.fBaseTextSize + 0.05
	FLOAT fNewRectPosTabsX = sContentOverviewDebug.fRectPosTabsX - (fTabSizeX*0.5)
	FLOAT fNewRectPosTabsY = sContentOverviewDebug.fRectPosTabsY - (fTabSizeY*0.75)
	
	IF IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_MISSION_OVERVIEW)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_MISSION_OVERVIEW) % 32)
		DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW_MISSION_OVERVIEW, fNewRectPosTabsX-(iTabX*fSpacingX), fNewRectPosTabsY-(iTabY*fExtraSpacingY), fTabSizeX, fTabSizeY, fTabTextSize, GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(CONTENT_OVERVIEW_DEBUG_WINDOW_MISSION_OVERVIEW, TRUE), sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_MISSION_OVERVIEW, iTabX, iTabY, fSpacingX)
	ENDIF
	
	IF IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS) % 32)
		DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS, fNewRectPosTabsX-(iTabX*fSpacingX), fNewRectPosTabsY-(iTabY*fExtraSpacingY), fTabSizeX, fTabSizeY, fTabTextSize, GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS, TRUE), sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS, iTabX, iTabY, fSpacingX)		
	ENDIF
	
	IF IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES) % 32)
		DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES, fNewRectPosTabsX-(iTabX*fSpacingX), fNewRectPosTabsY-(iTabY*fExtraSpacingY), fTabSizeX, fTabSizeY, fTabTextSize, GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES, TRUE), sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES, iTabX, iTabY, fSpacingX)		
	ENDIF
	
	IF IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS) % 32)
		DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS, fNewRectPosTabsX-(iTabX*fSpacingX), fNewRectPosTabsY-(iTabY*fExtraSpacingY), fTabSizeX, fTabSizeY, fTabTextSize, GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS, TRUE), sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS, iTabX, iTabY, fSpacingX)		
	ENDIF
	
	IF IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE) % 32)
		DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE, fNewRectPosTabsX-(iTabX*fSpacingX), fNewRectPosTabsY-(iTabY*fExtraSpacingY), fTabSizeX, fTabSizeY, fTabTextSize, GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE, TRUE), sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE, iTabX, iTabY, fSpacingX)
	ENDIF		
	
	IF IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP) % 32)
		DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP, fNewRectPosTabsX-(iTabX*fSpacingX), fNewRectPosTabsY-(iTabY*fExtraSpacingY), fTabSizeX, fTabSizeY, fTabTextSize, GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP, TRUE), sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP, iTabX, iTabY, fSpacingX)		
	ENDIF
	
	IF IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_PROP)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_PROP) % 32)
		DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW_PROP, fNewRectPosTabsX-(iTabX*fSpacingX), fNewRectPosTabsY-(iTabY*fExtraSpacingY), fTabSizeX, fTabSizeY, fTabTextSize, GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(CONTENT_OVERVIEW_DEBUG_WINDOW_PROP, TRUE), sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_PROP, iTabX, iTabY, fSpacingX)	
	ENDIF
				
	IF IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES) % 32)
		DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES, fNewRectPosTabsX-(iTabX*fSpacingX), fNewRectPosTabsY-(iTabY*fExtraSpacingY), fTabSizeX, fTabSizeY, fTabTextSize, GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES, TRUE), sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES, iTabX, iTabY, fSpacingX)
	ENDIF
	
	IF IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_DM_MODIFIERS)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_DM_MODIFIERS) % 32)
		DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW_DM_MODIFIERS, fNewRectPosTabsX-(iTabX*fSpacingX), fNewRectPosTabsY-(iTabY*fExtraSpacingY), fTabSizeX, fTabSizeY, fTabTextSize, GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(CONTENT_OVERVIEW_DEBUG_WINDOW_DM_MODIFIERS, TRUE), sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_DM_MODIFIERS, iTabX, iTabY, fSpacingX)
	ENDIF
	
	IF IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS) % 32)
		DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS, fNewRectPosTabsX-(iTabX*fSpacingX), fNewRectPosTabsY-(iTabY*fExtraSpacingY), fTabSizeX, fTabSizeY, fTabTextSize, GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS, TRUE), sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS, iTabX, iTabY, fSpacingX)	
	ENDIF
	
	IF IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER) % 32)
		DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER, fNewRectPosTabsX-(iTabX*fSpacingX), fNewRectPosTabsY-(iTabY*fExtraSpacingY), fTabSizeX, fTabSizeY, fTabTextSize, GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER, TRUE), sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER, iTabX, iTabY, fSpacingX)
	ENDIF
		
	IF IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS) % 32)
		DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS, fNewRectPosTabsX-(iTabX*fSpacingX), fNewRectPosTabsY-(iTabY*fExtraSpacingY), fTabSizeX, fTabSizeY, fTabTextSize, GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS, TRUE), sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS, iTabX, iTabY, fSpacingX)
	ENDIF
	
	IF IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_WARP_PORTALS)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_WARP_PORTALS) % 32)
		DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW_WARP_PORTALS, fNewRectPosTabsX-(iTabX*fSpacingX), fNewRectPosTabsY-(iTabY*fExtraSpacingY), fTabSizeX, fTabSizeY, fTabTextSize, GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(CONTENT_OVERVIEW_DEBUG_WINDOW_WARP_PORTALS, TRUE), sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_WARP_PORTALS, iTabX, iTabY, fSpacingX)
	ENDIF	
		
	IF IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_GANG_CHASE)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_GANG_CHASE) % 32)
		DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW_GANG_CHASE, fNewRectPosTabsX-(iTabX*fSpacingX), fNewRectPosTabsY-(iTabY*fExtraSpacingY), fTabSizeX, fTabSizeY, fTabTextSize, GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(CONTENT_OVERVIEW_DEBUG_WINDOW_GANG_CHASE, TRUE), sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_GANG_CHASE, iTabX, iTabY, fSpacingX)
	ENDIF	
	
	IF IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_RUNTIME_ERRORS)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_RUNTIME_ERRORS) % 32)
		DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_RUNTIME_ERRORS, fNewRectPosTabsX-(iTabX*fSpacingX), fNewRectPosTabsY-(iTabY*fExtraSpacingY), fTabSizeX, fTabSizeY, fTabTextSize, GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_RUNTIME_ERRORS, TRUE), sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_RUNTIME_ERRORS, iTabX, iTabY, fSpacingX)
	ENDIF
	
	IF IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_CUSTOM_DEBUG)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_CUSTOM_DEBUG) % 32)
		DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_CUSTOM_DEBUG, fNewRectPosTabsX-(iTabX*fSpacingX), fNewRectPosTabsY-(iTabY*fExtraSpacingY), fTabSizeX, fTabSizeY, fTabTextSize, GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_CUSTOM_DEBUG, TRUE), sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_CUSTOM_DEBUG, iTabX, iTabY, fSpacingX)
	ENDIF
	
	// Title
	TEXT_LABEL_63 tl63Title
	tl63Title = "~bold~ "
	tl63Title += GET_CONTENT_OVERVIEW_NAME_FOR_DEBUG_WINDOW(sContentOverviewDebug.eDebugWindow, FALSE)
	tl63Title += " Overview"
	iCustomTabX = 11
	iCustomTabY = 0
	DRAW_STATE_DEBUG_WINDOW_TAB(CONTENT_OVERVIEW_DEBUG_WINDOW_NONE, fNewRectPosTabsX-(iCustomTabX*fSpacingX), fNewRectPosTabsY-(iCustomTabY*fExtraSpacingY), fTabSizeX*4, fTabSizeY, fTabTextSize*1.15, tl63Title, TRUE, iCustomTabX, iCustomTabY, 0.0)
	
ENDPROC

PROC PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_MAIN()	
	
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_ClearMode)
		// Main Window				
		DRAW_RECT(sContentOverviewDebug.fRectPosX, sContentOverviewDebug.fRectPosY-(sContentOverviewDebug.fRectSizeY/2)-0.005, sContentOverviewDebug.fRectSizeX+0.005, 0.0025, sContentOverviewDebug.iRedType, sContentOverviewDebug.iGreenType, sContentOverviewDebug.iBlueType, 120)
		DRAW_RECT(sContentOverviewDebug.fRectPosX, sContentOverviewDebug.fRectPosY+(sContentOverviewDebug.fRectSizeY/2)+0.005, sContentOverviewDebug.fRectSizeX+0.005, 0.0025, sContentOverviewDebug.iRedType, sContentOverviewDebug.iGreenType, sContentOverviewDebug.iBlueType, 120)
		
		DRAW_RECT(sContentOverviewDebug.fRectPosX-(sContentOverviewDebug.fRectSizeX/2)-0.0025, sContentOverviewDebug.fRectPosY, 0.0015, sContentOverviewDebug.fRectSizeY+0.01, sContentOverviewDebug.iRedType, sContentOverviewDebug.iGreenType, sContentOverviewDebug.iBlueType, 120)
		DRAW_RECT(sContentOverviewDebug.fRectPosX+(sContentOverviewDebug.fRectSizeX/2)+0.0025, sContentOverviewDebug.fRectPosY, 0.0015, sContentOverviewDebug.fRectSizeY+0.01, sContentOverviewDebug.iRedType, sContentOverviewDebug.iGreenType, sContentOverviewDebug.iBlueType, 120)
		
		DRAW_SPRITE("CommonMenu", "Gradient_Bgd", sContentOverviewDebug.fRectPosX, sContentOverviewDebug.fRectPosY, sContentOverviewDebug.fRectSizeX+0.005, sContentOverviewDebug.fRectSizeY+0.01, 0.0, 0, 0, 0, 180)
	ENDIF
		
ENDPROC

PROC PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_LEGEND(CONTENT_OVERVIEW_DEBUG_WINDOW EntityStateType = CONTENT_OVERVIEW_DEBUG_WINDOW_NONE)
		
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_ClearMode)
	AND NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
		// Second Window Draw
		DRAW_RECT(sContentOverviewDebug.fRectPosX2, sContentOverviewDebug.fRectPosY2-(sContentOverviewDebug.fRectSizeY2/2)-0.005, sContentOverviewDebug.fRectSizeX2+0.005, 0.0025, sContentOverviewDebug.iRedType, sContentOverviewDebug.iGreenType, sContentOverviewDebug.iBlueType, 120)
		DRAW_RECT(sContentOverviewDebug.fRectPosX2, sContentOverviewDebug.fRectPosY2+(sContentOverviewDebug.fRectSizeY2/2)+0.005, sContentOverviewDebug.fRectSizeX2+0.005, 0.0025, sContentOverviewDebug.iRedType, sContentOverviewDebug.iGreenType, sContentOverviewDebug.iBlueType, 120)
		
		DRAW_RECT(sContentOverviewDebug.fRectPosX2-(sContentOverviewDebug.fRectSizeX2/2)-0.0025, sContentOverviewDebug.fRectPosY2, 0.0015, sContentOverviewDebug.fRectSizeY2+0.01, sContentOverviewDebug.iRedType, sContentOverviewDebug.iGreenType, sContentOverviewDebug.iBlueType, 120)
		DRAW_RECT(sContentOverviewDebug.fRectPosX2+(sContentOverviewDebug.fRectSizeX2/2)+0.0025, sContentOverviewDebug.fRectPosY2, 0.0015, sContentOverviewDebug.fRectSizeY2+0.01, sContentOverviewDebug.iRedType, sContentOverviewDebug.iGreenType, sContentOverviewDebug.iBlueType, 120)
		
		DRAW_SPRITE("CommonMenu", "Gradient_Bgd", sContentOverviewDebug.fRectPosX2, sContentOverviewDebug.fRectPosY2, sContentOverviewDebug.fRectSizeX2+0.005, sContentOverviewDebug.fRectSizeY2+0.01, 180.0, 0, 0, 0, 140)
	ENDIF
	
	FLOAT fButtonX, fButtonY, fButtonSizeX, fButtonSizeY
	fButtonX = sContentOverviewDebug.fRectPosX2+(sContentOverviewDebug.fRectSizeX2*0.485)
	fButtonY = sContentOverviewDebug.fRectPosY2-(sContentOverviewDebug.fRectSizeY2*0.45)
	fButtonSizeX = sContentOverviewDebug.fRectSizeX2*0.02
	fButtonSizeY = sContentOverviewDebug.fRectSizeY2*0.15
	
	// Hide Legend Button
	IF IS_STATE_DEBUG_WINDOW_CLICK_EVENT_REGION_HOVERED(-1, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_HIDE_LEGEND, EntityStateType, fButtonX, fButtonY, fButtonSizeX, fButtonSizeY, TRUE, "", DEFAULT, MENU_ICON_BIN_LOCK, IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend) OR IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_ClearMode))
		PROCESS_STATE_DEBUG_WINDOW_CLICK_EVENT(CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_HIDE_LEGEND)	
	ENDIF	
	
ENDPROC

PROC PROCESS_DRAW_MOUSE_CLICK_EVENT_NAME()

	// Mouse
	IF NOT IS_STRING_NULL_OR_EMPTY(sContentOverviewDebug.sEntityStateClickName)					
		SET_TEXT_SCALE(0.45, 0.45)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_DROPSHADOW(3, 0, 0, 0, 255)
		SET_TEXT_EDGE(3, 0, 0, 0, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(sContentOverviewDebug.fEntityStateMouseX+0.015, sContentOverviewDebug.fEntityStateMouseY-0.01, "STRING", sContentOverviewDebug.sEntityStateClickName)		
	ENDIF
	
	sContentOverviewDebug.sEntityStateClickName = ""
ENDPROC

FUNC BOOL SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(INT &iCurrentLength, INT iRow)
	
	IF iCurrentLength = iRow
		iCurrentLength++
		RETURN TRUE
	ENDIF
	
	iCurrentLength++
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_THIS_CURRENT_TAB_SHOW_TEAM_INDEX_SELECTOR_TABS(CONTENT_OVERVIEW_DEBUG_WINDOW eType)
	
	SWITCH eType
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_THIS_CURRENT_TAB_SHOW_ENABLE_DEBUG_BUTTON(CONTENT_OVERVIEW_DEBUG_WINDOW eType)
	
	SWITCH eType
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CURRENT_TAB_TEAM_INDEX_MAX(CONTENT_OVERVIEW_DEBUG_WINDOW eType)
	
	SWITCH eType
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS
			RETURN FMMC_MAX_TEAMS
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC BOOL SHOULD_THIS_CURRENT_TAB_SHOW_MUTE_DIALOGUE_HANDLER_DEBUG_BUTTON(CONTENT_OVERVIEW_DEBUG_WINDOW eType)
	
	SWITCH eType
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_CUSTOM_DEBUG
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_THIS_CURRENT_TAB_SHOW_MUTE_MUSIC_DEBUG_BUTTON(CONTENT_OVERVIEW_DEBUG_WINDOW eType)
	
	SWITCH eType
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_CUSTOM_DEBUG
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_CONTENT_OVERVIEW_DEBUG_SHARED_BUTTONS()
	
	TEXT_LABEL_23 tl23_Button	
	FLOAT fButtonSpacingX
	FLOAT fExtraSpacingY = 0.02
	
	FLOAT fButtonTextSize = sContentOverviewDebug.fBaseTextSize + 0.025
	
	FLOAT fNewRectPosButtonsX 
	FLOAT fNewRectPosButtonsY 
	INT iButtonX
	
	IF SHOULD_THIS_CURRENT_TAB_SHOW_ENABLE_DEBUG_BUTTON(sContentOverviewDebug.eDebugWindow)
		fButtonSpacingX = 0.075
		fNewRectPosButtonsX = sContentOverviewDebug.fRectPosX - (sContentOverviewDebug.fRectSizeX*0.5) + 0.01			// Make this better: url:bugstar:7660898
		fNewRectPosButtonsY = sContentOverviewDebug.fRectPosY - (sContentOverviewDebug.fRectSizeY*0.5) + 0.005			// Make this better: url:bugstar:7660898
		tl23_Button = "Debug"
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_CENTER_LARGE, 0, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_ENABLE_ALL_WINDOW_DEBUG, fNewRectPosButtonsX+(fButtonSpacingX*iButtonX)+(fButtonSpacingX*0.5), fNewRectPosButtonsY, fExtraSpacingY, TRUE, tl23_Button, fButtonTextSize, MENU_ICON_DUMMY)
		iButtonX++
	ENDIF
			
	IF SHOULD_THIS_CURRENT_TAB_SHOW_MUTE_DIALOGUE_HANDLER_DEBUG_BUTTON(sContentOverviewDebug.eDebugWindow)
		fButtonSpacingX = 0.1
		fNewRectPosButtonsX = sContentOverviewDebug.fRectPosX - (sContentOverviewDebug.fRectSizeX*0.5) + 0.01			// Make this better: url:bugstar:7660898
		fNewRectPosButtonsY = sContentOverviewDebug.fRectPosY - (sContentOverviewDebug.fRectSizeY*0.5) + 0.005			// Make this better: url:bugstar:7660898
		IF g_bEnableConversationMute
			tl23_Button = "Unmute Dialogue"
		ELSE
			tl23_Button = "Mute Dialogue"
		ENDIF
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_CENTER_LARGE_WIDE, 0, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_MUTE_DIALOGUE_HANDLER, fNewRectPosButtonsX+(fButtonSpacingX*iButtonX)+(fButtonSpacingX*0.5), fNewRectPosButtonsY, fExtraSpacingY, TRUE, tl23_Button, fButtonTextSize, MENU_ICON_DUMMY, g_bEnableConversationMute)
		iButtonX++
	ENDIF
			
	IF SHOULD_THIS_CURRENT_TAB_SHOW_MUTE_MUSIC_DEBUG_BUTTON(sContentOverviewDebug.eDebugWindow)
		fButtonSpacingX = 0.1
		fNewRectPosButtonsX = sContentOverviewDebug.fRectPosX - (sContentOverviewDebug.fRectSizeX*0.5) + 0.01			// Make this better: url:bugstar:7660898
		fNewRectPosButtonsY = sContentOverviewDebug.fRectPosY - (sContentOverviewDebug.fRectSizeY*0.5) + 0.005			// Make this better: url:bugstar:7660898
		tl23_Button = "Toggle Music"
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_CENTER_LARGE_WIDE, 0, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_MUTE_MUSIC, fNewRectPosButtonsX+(fButtonSpacingX*iButtonX)+(fButtonSpacingX*0.5), fNewRectPosButtonsY, fExtraSpacingY, TRUE, tl23_Button, fButtonTextSize, MENU_ICON_DUMMY, g_bEnableMusicMute)
		iButtonX++
	ENDIF
	
	IF SHOULD_THIS_CURRENT_TAB_SHOW_TEAM_INDEX_SELECTOR_TABS(sContentOverviewDebug.eDebugWindow)						
		fButtonSpacingX = 0.075
		fNewRectPosButtonsX = sContentOverviewDebug.fRectPosX - (sContentOverviewDebug.fRectSizeX*0.5) + 0.01			// Make this better: url:bugstar:7660898
		fNewRectPosButtonsY = sContentOverviewDebug.fRectPosY - (sContentOverviewDebug.fRectSizeY*0.5) + 0.005			// Make this better: url:bugstar:7660898
		INT iTeam
		FOR iTeam = 0 TO GET_CURRENT_TAB_TEAM_INDEX_MAX(sContentOverviewDebug.eDebugWindow)-1
			tl23_Button = "Team "
			tl23_Button += iTeam
			CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_CENTER_LARGE, iTeam, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_SELECT_TEAM, fNewRectPosButtonsX+(fButtonSpacingX*iButtonX)+(fButtonSpacingX*0.5), fNewRectPosButtonsY, fExtraSpacingY, TRUE, tl23_Button, fButtonTextSize, MENU_ICON_DUMMY, sContentOverviewDebug.iTeamIndexSelected = iTeam)
			iButtonX++
		ENDFOR
	ENDIF
		
	IF sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] > 1
		
		iButtonX = 0
		fButtonSpacingX = 0.0275
		
		fButtonTextSize = sContentOverviewDebug.fBaseTextSize	
		fNewRectPosButtonsX = sContentOverviewDebug.fRectPosX + (sContentOverviewDebug.fRectSizeX*0.4) + 0.01			// Make this better: url:bugstar:7660898
		fNewRectPosButtonsY = sContentOverviewDebug.fRectPosY - (sContentOverviewDebug.fRectSizeY*0.5) + 0.005			// Make this better: url:bugstar:7660898
		
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_CENTER_NARROW, 0, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_DECREMENT_PAGE, fNewRectPosButtonsX+(fButtonSpacingX*iButtonX), fNewRectPosButtonsY, fExtraSpacingY, TRUE, "", fButtonTextSize, MENU_ICON_ARROW_L)
		iButtonX++
		
		tl23_Button = "Page: "
		tl23_Button += sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]+1
		tl23_Button += " / "
		tl23_Button += sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]	
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_CENTER_LARGE, 0, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_SELECT_PAGE, fNewRectPosButtonsX+(fButtonSpacingX*iButtonX), fNewRectPosButtonsY, fExtraSpacingY, TRUE, tl23_Button, fButtonTextSize, MENU_ICON_DUMMY)		
		iButtonX++
		
		fButtonSpacingX = 0.0275
		
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_CENTER_NARROW, 0, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_INCREMENT_PAGE, fNewRectPosButtonsX+(fButtonSpacingX*iButtonX), fNewRectPosButtonsY, fExtraSpacingY, TRUE, "", fButtonTextSize, MENU_ICON_ARROW_R)
		iButtonX++
	ENDIF
		
ENDPROC

FUNC BOOL SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
	
	REQUEST_STREAMED_TEXTURE_DICT("CommonMenu")	
		
	IF NOT IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)/32], ENUM_TO_INT(sContentOverviewDebug.eDebugWindow) % 32)
		SET_BIT(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)/32], ENUM_TO_INT(sContentOverviewDebug.eDebugWindow) % 32)
		RETURN FALSE
	ENDIF
	
	sContentOverviewDebug.fYSpacing = GET_CONTENT_OVERVIEW_DEBUG_WINDOW_Y_ITEM_SPACING_SIZE(sContentOverviewDebug.eDebugWindow)
	sContentOverviewDebug.fBaseTextSize = 0.225
	
	GET_CONTENT_OVERVIEW_DEBUG_WINDOW_X_AND_Y(sContentOverviewDebug.fMainX, sContentOverviewDebug.fMainY)
	GET_CONTENT_OVERVIEW_DEBUG_WINDOW_SIZEX_AND_SIZEY(sContentOverviewDebug.fRectSizeX, sContentOverviewDebug.fRectSizeY, sContentOverviewDebug.eDebugWindow)
	GET_CONTENT_OVERVIEW_DEBUG_WINDOW_SIZEX_AND_SIZEY_TWO(sContentOverviewDebug.fRectSizeX2, sContentOverviewDebug.fRectSizeY2)
		
	// First Window
	sContentOverviewDebug.fRectPosX = sContentOverviewDebug.fMainX + (sContentOverviewDebug.fRectSizeX/2)
	sContentOverviewDebug.fRectPosY = sContentOverviewDebug.fMainY + (sContentOverviewDebug.fRectSizeY/2)
	sContentOverviewDebug.fTextPosX = sContentOverviewDebug.fMainX + 0.005
	sContentOverviewDebug.fTextPosY = sContentOverviewDebug.fMainY + 0.01

	// Second Window
	sContentOverviewDebug.fRectPosX2 = sContentOverviewDebug.fMainX + (sContentOverviewDebug.fRectSizeX2/2)
	sContentOverviewDebug.fRectPosY2 = sContentOverviewDebug.fMainY + sContentOverviewDebug.fRectSizeY + (sContentOverviewDebug.fRectSizeY2/2) + 0.02
	sContentOverviewDebug.fTextPosX2 = sContentOverviewDebug.fMainX + 0.005
	sContentOverviewDebug.fTextPosY2 = sContentOverviewDebug.fRectPosY2 - (sContentOverviewDebug.fRectSizeY2/2)

	// Draw Tabs
	sContentOverviewDebug.fRectPosTabsX = sContentOverviewDebug.fMainX + sContentOverviewDebug.fRectSizeX
	sContentOverviewDebug.fRectPosTabsY = sContentOverviewDebug.fMainY

	// Other
	sContentOverviewDebug.fDrawSpacingY = 0.015
	
	GET_CONTENT_OVERVIEW_DEBUG_WINDOW_STATUS_RECT_SIZE_X_AND_Y(sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY)	
	sContentOverviewDebug.fBringRectInLineWithText = (sContentOverviewDebug.fStatusRectY*1.2)
	
	GET_MISSION_ENTITY_OVERVIEW_WINDOW_ACCENT_COLOURS_FOR_CREATION_TYPE(sContentOverviewDebug.eDebugWindow, sContentOverviewDebug.iRedType, sContentOverviewDebug.iGreenType, sContentOverviewDebug.iBlueType, sContentOverviewDebug.iAlphaType)
	
	// Draw Window Tabs (first so it appears behind)
	PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_TABS()
	sContentOverviewDebug.iAlphaType = ROUND(TO_FLOAT(sContentOverviewDebug.iAlphaType) * 0.4)	
	
	// Main Window Backgrounds
	PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_MAIN()
		
	// Top Extra Info Draw ---------------
	sContentOverviewDebug.vLegendTextDrawPos.x = sContentOverviewDebug.fTextPosX2
	sContentOverviewDebug.vLegendTextDrawPos.y = sContentOverviewDebug.fTextPosY2	
	
	sContentOverviewDebug.iLegendPrintX = 0
	sContentOverviewDebug.iLegendPrintY = 0	
		
	PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_LEGEND(sContentOverviewDebug.eDebugWindow)
	
	PROCESS_CONTENT_OVERVIEW_DEBUG_SHARED_BUTTONS()
	
	RETURN TRUE
	
ENDFUNC

PROC CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()

	sContentOverviewDebug.eEntityStateClickEvent = CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_NONE

ENDPROC

FUNC BOOL SHOULD_CALL_CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT()	
	
	IF sContentOverviewDebug.eDebugWindowTypeSelected = CONTENT_OVERVIEW_DEBUG_WINDOW_NONE
		CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		RETURN FALSE
	ENDIF
	
	IF sContentOverviewDebug.iEntityIndexSelected = -1
	AND DOES_CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_REQUIRE_INDEX()
		CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_CAMERA()
	IF DOES_CAM_EXIST(sContentOverviewDebug.ciEntityStateCamera)			
		SET_TEXT_SCALE(0.35, 0.35)
		SET_TEXT_COLOUR(255, 100, 100, 255)
		SET_TEXT_DROPSHADOW(3, 0, 0, 0, 255)
		SET_TEXT_EDGE(3, 0, 0, 0, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.35, 0.85, "STRING", "Use Movement or Accept to Reset Camera")	
		
		IF GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X) != 127
		OR GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y) != 127
		OR GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X) != 127
		OR GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y) != 127
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)					
			RENDER_SCRIPT_CAMS(FALSE, FALSE, 3000, DEFAULT, TRUE)
			DESTROY_CAM(sContentOverviewDebug.ciEntityStateCamera)
			CLEAR_FOCUS()
		ENDIF		
	ENDIF
ENDPROC

FUNC BOOL IS_ANY_CONTENT_OVERVIEW_DEBUG_WINDOW_TAG_BITSET_SET()
	INT i = 0
	FOR i = 0 TO ciMAX_TAB_TYPES-1
		IF sContentOverviewDebug.iDebugWindowTabBitset[i] != 0
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

// DRAW MENUS START #################	
PROC PROCESS_SHARED_RUNTIME_ERRORS_STATE_DEBUG_WINDOW()

	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
		
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 1
	
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)			
		tl63_Name = "Notifications, Warnings and Critical Errors are stored here."
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
		
		tl63_Name = GET_MISSION_STATE_RUNTIME_ERROR_TEXT_FOR_TYPE(ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION)	
		tl63_Name += "Information or notifications."
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
		
		tl63_Name = GET_MISSION_STATE_RUNTIME_ERROR_TEXT_FOR_TYPE(ENTITY_RUNTIME_ERROR_TYPE_WARNING)	
		tl63_Name += "Investigate - potentially ignorable."
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
			
		tl63_Name = GET_MISSION_STATE_RUNTIME_ERROR_TEXT_FOR_TYPE(ENTITY_RUNTIME_ERROR_TYPE_CRITICAL)	
		tl63_Name += "Investigate and Fix."
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
	ENDIF
	
	CLEANUP_RUNTIME_ERROR_ALERT_ON_SCREEN()
	
	// Main Draw Loop -------------------------
	INT i = 0
	FOR i = 0 TO ci_MAX_NUMBER_OF_RUNTIME_ERRORS-1 // iEntityRunTimeErrorCounter-1 // GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow)
		
		FMMC_CLEAR_LONG_BIT(iEntityRunTimeErrorShouldDisplayBSLong, i)
		
		sContentOverviewDebug.fBaseTextSize = 0.26
		tl63_Name = ""
		
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))		
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		tl63_Name = tlEntityRuntimeErrorText[i]
		
		IF i % 3 = 0
		OR i = 0
			IF NOT IS_STRING_NULL_OR_EMPTY(tl63_Name)
				CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_2, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_DELETE_OR_REMOVE, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
				DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX/2, sContentOverviewDebug.fStatusRectY/2, 255, 50, 50, 175)
			ENDIF
		ENDIF
		
		IF i >= 2
		AND (i-2) % 3 = 0
			IF NOT IS_STRING_NULL_OR_EMPTY(tlEntityRuntimeErrorText[i-2])
				DRAW_RECT(fEntityNamePosX+(fEntityListColumnSpacingBase/2), fEntityNamePosY+(sContentOverviewDebug.fBringRectInLineWithText*2), fEntityListColumnSpacingBase*0.95, sContentOverviewDebug.fStatusRectY*0.5, 255, 255, 255, 75)
			ENDIF
		ENDIF
				
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_Name, fEntityNamePosX, fEntityNamePosY, fTextSize, TRUE)	
	ENDFOR
ENDPROC

PROC DRAW_LOCAL_PLAYER_INTERIOR_INFO_DEBUG_STATE_LEGEND(PED_INDEX PlayerPed)	
	TEXT_LABEL_63 tl63_Name
	INTERIOR_INSTANCE_INDEX LocalPlayerCurrentInterior = GET_INTERIOR_FROM_ENTITY(PlayerPed)
	VECTOR vLocalPlayerCurrentInteriorPos
	INT iLocalPlayerCurrentRoomKey
	INT iLocalPlayerCurrentInteriorHash
	STRING sLocalPlayerInteriorNameDbg	
	IF IS_VALID_INTERIOR(LocalPlayerCurrentInterior)
		IF NOT IS_PED_INJURED(PlayerPed)
			INT iTemp = iLocalPlayerCurrentRoomKey
			GET_INTERIOR_LOCATION_AND_NAMEHASH(LocalPlayerCurrentInterior, vLocalPlayerCurrentInteriorPos, iLocalPlayerCurrentInteriorHash)
			iLocalPlayerCurrentRoomKey = GET_ROOM_KEY_FROM_ENTITY(PlayerPed)
			
			// Grabbing the interior art name for debug.
			IF iLocalPlayerCurrentRoomKey != iTemp
				INTERIOR_DATA_STRUCT structInteriorData
				INT i = 0
				FOR i = 0 TO ENUM_TO_INT(INTERIOR_MAX_NUM)-1
					INTERIOR_NAME_ENUM eInterior = INT_TO_ENUM(INTERIOR_NAME_ENUM, i)
					structInteriorData = GET_INTERIOR_DATA(eInterior)
					IF ARE_VECTORS_ALMOST_EQUAL(structInteriorData.vPos, vLocalPlayerCurrentInteriorPos)
						sLocalPlayerInteriorNameDbg = structInteriorData.sDebugName
						BREAKLOOP
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
	tl63_Name = "~bold~~g~Inside Interior: "
	IF IS_VALID_INTERIOR(LocalPlayerCurrentInterior)
		tl63_Name += "Yes"
	ELSE
		tl63_Name += "No"
	ENDIF
	DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
	
	tl63_Name = "~bold~~g~Interior Hash: "
	tl63_Name += iLocalPlayerCurrentInteriorHash
	DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
	
	tl63_Name = "~bold~~g~Interior Room Key: "
	tl63_Name += iLocalPlayerCurrentRoomKey
	DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
	
	tl63_Name = "~bold~~g~Interior Name: "
	tl63_Name += sLocalPlayerInteriorNameDbg
	DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1, DEFAULT, TRUE)
ENDPROC

PROC PROCESS_SHARED_CUSTOM_DEBUG_STATE_DEBUG_WINDOW()

	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 1

	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)			
		tl63_Name = "~bold~~g~Local Player Index:   "
		tl63_Name += NATIVE_TO_INT(PLAYER_ID())
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "~bold~~g~Local Player Name:   "
		tl63_Name += GET_PLAYER_NAME(PLAYER_ID())
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		DRAW_LOCAL_PLAYER_INTERIOR_INFO_DEBUG_STATE_LEGEND(PLAYER_PED_ID())	
	ENDIF
	
	// Main Draw Loop -------------------------			
	INT i = 0
	FOR i = 0 TO GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow, sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)])

		sContentOverviewDebug.fBaseTextSize = 0.225		
		tl63_Name = ""
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize*1.1
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow, sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
	
		INT iCurrentRow
		iCurrentRow = 0
		
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0
							
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "~bold~Player List"
				ENDIF
				VECTOR vTemp				
				INT iPlayer
				PED_INDEX piPed
				PLAYER_INDEX piPlayer
				FOR iPlayer = 0 TO NUM_NETWORK_PLAYERS-1
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						piPlayer = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
						IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
							piPed = GET_PLAYER_PED(piPlayer)
							IF DOES_ENTITY_EXIST(piPed)
								vTemp = GET_ENTITY_COORDS(piPed, FALSE)
								
								tl63_Name = GET_PLAYER_NAME(piPlayer)
								tl63_Name += " <<"
								tl63_Name += ROUND(vTemp.x)
								tl63_Name += ", "
								tl63_Name += ROUND(vTemp.y)
								tl63_Name += ", "
								tl63_Name += ROUND(vTemp.z)
								tl63_Name += ">>"													
								
								CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_1, iPlayer, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_PLAYER_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
								CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_2, iPlayer, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_PLAYER_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")		
							ENDIF
						ELSE
							tl63_Name = "-- Player "
							tl63_Name += iPlayer
							tl63_Name += " Inactive --"
						ENDIF
					ENDIF
				ENDFOR
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap.
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "~bold~Cached Warp Positions"
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Pos: <<"
					tl63_Name += ROUND(vContentOverviewSharedWarpCache[0].x)
					tl63_Name += ", "
					tl63_Name += ROUND(vContentOverviewSharedWarpCache[0].y)
					tl63_Name += ", "
					tl63_Name += ROUND(vContentOverviewSharedWarpCache[0].z)
					tl63_Name += ">>"
					tl63_Name += " Head: "
					tl63_Name += ROUND(fContentOverviewSharedWarpCache[0])	
					CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_1, 0, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_PLAYER_CACHE_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
					CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_2, 0, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_PLAYER_CACHE_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")		
					CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_3, 0, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_CACHE_PLAYER_POS_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Pos: <<"
					tl63_Name += ROUND(vContentOverviewSharedWarpCache[1].x)
					tl63_Name += ", "
					tl63_Name += ROUND(vContentOverviewSharedWarpCache[1].y)
					tl63_Name += ", "
					tl63_Name += ROUND(vContentOverviewSharedWarpCache[1].z)
					tl63_Name += ">>"
					tl63_Name += " Head: "
					tl63_Name += ROUND(fContentOverviewSharedWarpCache[1])
					CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_1, 1, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_PLAYER_CACHE_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
					CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_2, 1, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_PLAYER_CACHE_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")		
					CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_FROM_LEFT_3, 1, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_CACHE_PLAYER_POS_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Add new here
				ENDIF
			BREAK
		ENDSWITCH
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tl63_Name)
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX/2, sContentOverviewDebug.fStatusRectY/2, 255, 255, 255, 175)				
		ENDIF
		
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_Name, fEntityNamePosX, fEntityNamePosY, fTextSize)
	ENDFOR
ENDPROC

PROC PROCESS_SHARED_CONTENT_OVERVIEW_DEBUG_WINDOW()	
	 
	// Shared for all scripts using this system.
		
	SWITCH sContentOverviewDebug.eDebugWindow
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_RUNTIME_ERRORS
			PROCESS_SHARED_RUNTIME_ERRORS_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_CUSTOM_DEBUG
			PROCESS_SHARED_CUSTOM_DEBUG_STATE_DEBUG_WINDOW()
		BREAK
	ENDSWITCH
		
ENDPROC
// DRAW MENUS END #################

PROC PROCESS_SHARED_CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT()
			
	SWITCH sContentOverviewDebug.eEntityStateClickEvent
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_CHANGE_TAB
			sContentOverviewDebug.eDebugWindow = sContentOverviewDebug.eDebugWindowTypeSelected			
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_PLAYER_CACHE_INDEX
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_PLAYER_CACHE_INDEX
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_PLAYER_INDEX
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_PLAYER_INDEX
		
			FLOAT fHead
			VECTOR vWarp
			
			SWITCH sContentOverviewDebug.eEntityStateClickEvent
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_PLAYER_CACHE_INDEX
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_PLAYER_CACHE_INDEX
					vWarp = vContentOverviewSharedWarpCache[sContentOverviewDebug.iEntityIndexSelected]
					fHead = fContentOverviewSharedWarpCache[sContentOverviewDebug.iEntityIndexSelected]
				BREAK
						
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_PLAYER_INDEX
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_PLAYER_INDEX
					PED_INDEX piPed
					PLAYER_INDEX piPlayer
					
					piPlayer = INT_TO_NATIVE(PLAYER_INDEX, sContentOverviewDebug.iEntityIndexSelected)
					IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
						piPed = GET_PLAYER_PED(piPlayer)
						
						IF DOES_ENTITY_EXIST(piPed)
							vWarp = GET_ENTITY_COORDS(piPed, FALSE)
							fHead = GET_ENTITY_HEADING(piPed)
						ENDIF
					ENDIF
					
				BREAK
			ENDSWITCH
			
			IF IS_VECTOR_ZERO(vWarp)
				CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
				EXIT
			ENDIF
			
			IF sContentOverviewDebug.eEntityStateClickEvent = CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_PLAYER_CACHE_INDEX	
			OR sContentOverviewDebug.eEntityStateClickEvent = CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_PLAYER_INDEX
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					SET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), vWarp, FALSE, TRUE)
					SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), fHead)
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarp, FALSE, TRUE)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), fHead)
				ENDIF
				
			ELIF sContentOverviewDebug.eEntityStateClickEvent = CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_PLAYER_CACHE_INDEX				
			OR sContentOverviewDebug.eEntityStateClickEvent = CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_PLAYER_INDEX
			
				IF DOES_CAM_EXIST(sContentOverviewDebug.ciEntityStateCamera)
					DESTROY_CAM(sContentOverviewDebug.ciEntityStateCamera)
				ENDIF
			
				VECTOR vCamPos, vCamPosOffset
				INT iMinOffset, iMaxOffset
				INT iRand
				iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
				
				IF sContentOverviewDebug.eDebugWindowTypeSelected	!= CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
					iMinOffset = 2
					iMaxOffset = 5
				ELSE
					iMinOffset = 5
					iMaxOffset = 8
				ENDIF
				
				SWITCH iRand
					CASE 0
						vCamPosOffset = <<-TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), -TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(1, 3))>>
					BREAK
					CASE 1
						vCamPosOffset = <<TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(1, 3))>>
					BREAK
					CASE 2
						vCamPosOffset = <<-TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(1, 3))>>
					BREAK
					CASE 3
						vCamPosOffset = <<TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), -TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(1, 3))>>
					BREAK
				ENDSWITCH
				
				vCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarp, fHead, vCamPosOffset)
					
				sContentOverviewDebug.ciEntityStateCamera = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPos, <<0.0, 0.0, 0.0>>, 70.0)				
				
				SET_CAM_ACTIVE(sContentOverviewDebug.ciEntityStateCamera, TRUE)
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE, 1500, DEFAULT, TRUE)
				
				SET_CAM_FOV(sContentOverviewDebug.ciEntityStateCamera, 70.0)
							
				POINT_CAM_AT_COORD(sContentOverviewDebug.ciEntityStateCamera, vWarp)
			ENDIF
			
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_CACHE_PLAYER_POS_INDEX
			vContentOverviewSharedWarpCache[sContentOverviewDebug.iEntityIndexSelected] = GET_ENTITY_COORDS(PLAYER_PED_ID())
			fContentOverviewSharedWarpCache[sContentOverviewDebug.iEntityIndexSelected] = GET_ENTITY_HEADING(PLAYER_PED_ID())
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_DELETE_OR_REMOVE
			SWITCH sContentOverviewDebug.eDebugWindowTypeSelected			
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_SHARED_RUNTIME_ERRORS
					INT i
					FOR i = 0 TO ci_MAX_NUMBER_OF_MISSION_ALERT_LINES-1
						tlEntityRuntimeErrorText[sContentOverviewDebug.iEntityIndexSelected+i] = ""
						FMMC_CLEAR_LONG_BIT(iEntityRunTimeErrorDisplayedBSLong, sContentOverviewDebug.iEntityIndexSelected+i)
						FMMC_CLEAR_LONG_BIT(iEntityRunTimeErrorShouldDisplayBSLong, sContentOverviewDebug.iEntityIndexSelected+i)
					ENDFOR					
				BREAK
			ENDSWITCH
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_HIDE_LEGEND			
			IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
				SET_BIT(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
			ELSE
				CLEAR_BIT(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
			ENDIF
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_SELECT_TEAM
			sContentOverviewDebug.iTeamIndexSelected = sContentOverviewDebug.iEntityIndexSelected
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_SELECT_PAGE
			sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = sContentOverviewDebug.iEntityIndexSelected
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_INCREMENT_PAGE
			sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
			IF sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] >= sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
				sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
			ENDIF			
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_DECREMENT_PAGE
			sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]--
			IF sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] < 0
				sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]-1
			ENDIF
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_MUTE_DIALOGUE_HANDLER
			IF g_bEnableConversationMute
				g_bEnableConversationMute = FALSE
			ELIF NOT g_bEnableConversationMute
				g_bEnableConversationMute = TRUE
				KILL_FACE_TO_FACE_CONVERSATION()
			ENDIF
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_TOGGLE_MUTE_MUSIC
			g_bEnableMusicMute = TRUE
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC INITIALIZE_CONTENT_OVERVIEW_DEBUG_WINDOW_MAIN_FOR_SPECIFIC_CONTENT(SPECIFIC_CONTENT_OVERVIEW_DEBUG_WINDOW_CALLS SpecificContentOverviewDebugWindowCall)

	IF IS_ANY_CONTENT_OVERVIEW_DEBUG_WINDOW_TAG_BITSET_SET()
	OR IS_BIT_SET(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_EMPTY)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_EMPTY) % 32)
		EXIT
	ENDIF
	
	INT i = 0
	INT iMaxWindow = ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_MAX)
	FOR i = 0 TO iMaxWindow-1
		sContentOverviewDebug.eDebugWindow = INT_TO_ENUM(CONTENT_OVERVIEW_DEBUG_WINDOW, i)
		CALL SpecificContentOverviewDebugWindowCall()
		PROCESS_SHARED_CONTENT_OVERVIEW_DEBUG_WINDOW()
	ENDFOR
		
	IF NOT IS_ANY_CONTENT_OVERVIEW_DEBUG_WINDOW_TAG_BITSET_SET()
		SET_BIT(sContentOverviewDebug.iDebugWindowTabBitset[ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_EMPTY)/32], ENUM_TO_INT(CONTENT_OVERVIEW_DEBUG_WINDOW_EMPTY) % 32)
	ENDIF
	
	sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_NONE
	
ENDPROC
  	
PROC PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW(SPECIFIC_CONTENT_OVERVIEW_DEBUG_WINDOW_CALLS SpecificContentOverviewDebugWindowCall, SPECIFIC_CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_CALLS SpecificContentOverviewDebugWindowClickEventCalls)
	
	IF sContentOverviewDebug.eEntityStateClickEvent = CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_NONE
		sContentOverviewDebug.iEntityIndexSelected = -1
		sContentOverviewDebug.eDebugWindowTypeSelected = CONTENT_OVERVIEW_DEBUG_WINDOW_NONE
	ENDIF	
	
	PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_ACTIVATION()
	
	INITIALIZE_CONTENT_OVERVIEW_DEBUG_WINDOW_MAIN_FOR_SPECIFIC_CONTENT(SpecificContentOverviewDebugWindowCall)
	
	// Calls the Specific debug related to the Script using this header.
	CALL SpecificContentOverviewDebugWindowCall()
	// -- 
	
	// Calls the Shared debug which all scripts have access to.
	PROCESS_SHARED_CONTENT_OVERVIEW_DEBUG_WINDOW()
	// -- 
	
	PROCESS_DRAW_MOUSE_CLICK_EVENT_NAME()	
		
	IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sContentOverviewDebug.tdToggleEntitiesDueToTooMuchTextDraws, 5000)
		REINIT_NET_TIMER(sContentOverviewDebug.tdToggleEntitiesDueToTooMuchTextDraws)
		sContentOverviewDebug.iToggleEntitiesDueToTooMuchTextDraws++
		IF sContentOverviewDebug.iToggleEntitiesDueToTooMuchTextDraws > GET_NUMBER_OF_ITEM_PAGES(sContentOverviewDebug.eDebugWindow)
			sContentOverviewDebug.iToggleEntitiesDueToTooMuchTextDraws = 0
		ENDIF
	ENDIF
	
	PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_CAMERA()
	IF SHOULD_CALL_CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT()
		// Calls the Specific debug related to the Script using this header.
		CALL SpecificContentOverviewDebugWindowClickEventCalls()
		// Calls the Shared debug which all scripts have access to.
		PROCESS_SHARED_CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT()
	ENDIF
ENDPROC

#ENDIF

