/* ------------------------------------------------------------------
* Name: net_betting_client.sch
* Author: James Adwick
* Date: 17/08/2012
* Purpose: Client processing for betting throughout MP.
* This header file contains commands for clients to request to take 
* part in betting and place their bets.
* Also display betting info on screen when button pressed
* ------------------------------------------------------------------*/

USING "net_betting_drawing.sch"
USING "net_scoring_common.sch"
USING "net_leaderboards.sch"
USING "commands_money.sch"
USING "net_big_message.sch"


#IF IS_DEBUG_BUILD
USING "net_betting_debug.sch"
#ENDIF

PROC CLEAR_BETTING_STRUCT()
    STRUCT_MP_BETTING emptyBettingStruct
	
	// Copy these across as never really need to reset
	emptyBettingStruct.stUserName = MPGlobals.g_MPBettingData.stUserName
	emptyBettingStruct.stFileName = MPGlobals.g_MPBettingData.stFileName
	
    MPGlobals.g_MPBettingData = emptyBettingStruct
ENDPROC

PROC CLEAR_CLIENT_BETTING_STRUCT()
            // Clear player BD
    IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
		CLIENT_BETTING_DATA emptyBettingData
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData = emptyBettingData
    ENDIF
ENDPROC

// Clear out our betting struct
PROC CLEAR_PLAYER_LOCAL_BETTING_DATA()

	#IF IS_DEBUG_BUILD
	      PRINTLN("[JA@BETTING] CLEAR_PLAYER_LOCAL_BETTING_DATA - clearing my betting details (MPGlobals & GlobaplayerBD & Menu)")NET_NL()
	#ENDIF

	CLEAR_BETTING_STRUCT()
	CLEAR_CLIENT_BETTING_STRUCT()
ENDPROC

PROC REFRESH_PLAYER_JOB_STATS_FOR_CORONA(INT iType = -1)
	
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iStat1 = 0
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iStat2 = 0
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iStat3 = 0

	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())

		SWITCH iType
		
			CASE FMMC_TYPE_MG_DARTS
				
				PRINTLN("[JA@BETTING] REFRESH_PLAYER_JOB_STATS_FOR_CORONA - DARTS type")
			
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iWins = GET_MP_INT_PLAYER_STAT(MPPLY_DARTS_TOTAL_WINS)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iLosses = (GET_MP_INT_PLAYER_STAT(MPPLY_DARTS_TOTAL_MATCHES) - GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iWins)
			BREAK
			
			CASE FMMC_TYPE_MG_ARM_WRESTLING
			
				PRINTLN("[JA@BETTING] REFRESH_PLAYER_JOB_STATS_FOR_CORONA - ARM WRESTLING type")
			
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iWins = GET_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_WINS)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iLosses = (GET_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_MATCH) - GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iWins)
			BREAK
			
			CASE FMMC_TYPE_MG_TENNIS
			
				PRINTLN("[JA@BETTING] REFRESH_PLAYER_JOB_STATS_FOR_CORONA - TENNIS type")
			
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iWins = GET_MP_INT_PLAYER_STAT(MPPLY_TENNIS_MATCHES_WON)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iLosses = GET_MP_INT_PLAYER_STAT(MPPLY_TENNIS_MATCHES_LOST)
			BREAK
			
			CASE FMMC_TYPE_RACE
			
				PRINTLN("[JA@BETTING] REFRESH_PLAYER_JOB_STATS_FOR_CORONA - RACE type")
			
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iWins = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_RACES_WON)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iLosses = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_RACES_LOST)
			BREAK
			
			CASE FMMC_TYPE_BASE_JUMP
				
				PRINTLN("[JA@BETTING] REFRESH_PLAYER_JOB_STATS_FOR_CORONA - BASEJUMP type")
			
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iWins = GET_MP_INT_PLAYER_STAT(MPPLY_BJ_WINS)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iLosses = GET_MP_INT_PLAYER_STAT(MPPLY_BJ_LOST)
			BREAK
			
			CASE FMMC_TYPE_MG_GOLF
			
				PRINTLN("[JA@BETTING] REFRESH_PLAYER_JOB_STATS_FOR_CORONA - GOLF type")
			
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iWins = GET_MP_INT_PLAYER_STAT(MPPLY_GOLF_WINS)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iLosses = GET_MP_INT_PLAYER_STAT(MPPLY_GOLF_LOSSES)
			BREAK
			
			CASE FMMC_TYPE_MG_SHOOTING_RANGE
							
				PRINTLN("[JA@BETTING] REFRESH_PLAYER_JOB_STATS_FOR_CORONA - SHOOTING RANGE type")
			
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iWins = GET_MP_INT_PLAYER_STAT(MPPLY_SHOOTINGRANGE_WINS)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iLosses = GET_MP_INT_PLAYER_STAT(MPPLY_SHOOTINGRANGE_LOSSES)
			BREAK	
				
			CASE FMMC_TYPE_RACE_TO_POINT
							
				PRINTLN("[JA@BETTING] REFRESH_PLAYER_JOB_STATS_FOR_CORONA - RACE TO POINT type")
			
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iWins = GET_MP_INT_PLAYER_STAT(MPPLY_RACE_2_POINT_WINS)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iLosses = GET_MP_INT_PLAYER_STAT(MPPLY_RACE_2_POINT_LOST)
			BREAK
			
			CASE FMMC_TYPE_DEATHMATCH
				IF g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_TYPE] = FMMC_DEATHMATCH_TEAM
				
					PRINTLN("[JA@BETTING] REFRESH_PLAYER_JOB_STATS_FOR_CORONA - TEAM DEATHMATCH type")
				
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iWins = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TDEATHMATCH_WON)
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iLosses = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TDEATHMATCH_LOST)			
				ELSE
					PRINTLN("[JA@BETTING] REFRESH_PLAYER_JOB_STATS_FOR_CORONA - DEATHMATCH type")
				
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iWins = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_DEATHMATCH_WON)
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iLosses = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_DEATHMATCH_LOST)
				ENDIF
				
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iStat1 = GET_MP_INT_PLAYER_STAT(MPPLY_DM_TOTAL_KILLS)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iStat2 = GET_MP_INT_PLAYER_STAT(MPPLY_DM_TOTAL_DEATHS)
			BREAK
			
			DEFAULT
			
				PRINTLN("[JA@BETTING] REFRESH_PLAYER_JOB_STATS_FOR_CORONA - DEFAULT type")
			
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iWins = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_DEATHMATCH_WON)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iLosses = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_DEATHMATCH_LOST)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iStat1 = GET_MP_INT_PLAYER_STAT(MPPLY_DM_TOTAL_KILLS)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iStat2 = GET_MP_INT_PLAYER_STAT(MPPLY_DM_TOTAL_DEATHS)
			BREAK
		
		ENDSWITCH
	ENDIF
	
	PRINTLN("[JA@BETTING] GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iWins = ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iWins)
	PRINTLN("[JA@BETTING] GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iLosses = ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.playerBettingStats.iLosses)
	
ENDPROC

PROC UPDATE_PLAYER_BETTING_STATS(INT iType = -1)	
	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
	
		INT iActivityCost = 0
	
		IF DO_I_NEED_TO_PAY_TO_PLAY_THIS_FM_ACTIVITY(g_FMMC_STRUCT.iMissionType, g_FMMC_STRUCT.iMissionSubType)
			iActivityCost = GET_FM_ACTIVITY_COST(g_FMMC_STRUCT.iMissionType, g_FMMC_STRUCT.iMissionSubType, TRUE)
		ENDIF
		
		//The SCRIPT_MAX_INT32 overflow of GET_LOCAL_PLAYER_VC_AMOUNT doesn't need fixing here (see the comment in GET_LOCAL_PLAYER_VC_AMOUNT).
		MPGlobals.g_MPBettingData.iClientAvailableCash = GET_LOCAL_PLAYER_VC_AMOUNT(FALSE) - iActivityCost
	
		PRINTLN("[JA@BETTING] UPDATE_PLAYER_BETTING_STATS - Calculated cash available: ", MPGlobals.g_MPBettingData.iClientAvailableCash)
	
		IF iType = -1
			BETTING_ID bettingID = GET_MY_BETTING_ID()
			iType = bettingID.iMissionType
		ENDIF
	
		REFRESH_PLAYER_JOB_STATS_FOR_CORONA(iType)
	ENDIF
ENDPROC

// This probably should be the sum of kills, assists and killstreaks divided by number of DM they have played (average score) + # DM they have won?

FUNC INT GET_PLAYER_STAT_SUM(PLAYER_INDEX playerID)
	INT iStat = 0
	
	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)

		INT iWins = GlobalplayerBD[NATIVE_TO_INT(playerID)].clientBetData.playerBettingStats.iWins
		INT iTotal = (GlobalplayerBD[NATIVE_TO_INT(playerID)].clientBetData.playerBettingStats.iWins + GlobalplayerBD[NATIVE_TO_INT(playerID)].clientBetData.playerBettingStats.iLosses)

		PRINTLN("iWins: ", iWins, " iTotal: ", iTotal)

		IF iTotal = 0
			iStat = 0
		ELSE
			iStat = ROUND( (TO_FLOAT(iWins) / TO_FLOAT(iTotal)) * 100.0 )		// Getting win % (can use other stats if we want to)
			
			PRINTLN("[JA@BETTING] GET_PLAYER_STAT_SUM - stat value for player ", GET_PLAYER_NAME(playerID), " = ", iStat)
		ENDIF
		
		IF iStat < 0
			iStat = 0
		ENDIF
	ENDIF

	RETURN iStat
ENDFUNC

// --------------------- CLIENT EVENTS -----------------------

/// PURPOSE: Event for clients to notify server they are in a valid area / activity for betting
PROC BROADCAST_PLAYER_AVAILABLE_FOR_BETTING(BETTING_ID bettingID, BOOL bTeamBetting = FALSE, STRING stUserName = NULL, STRING stFilename = NULL)
	
	#IF IS_DEBUG_BUILD
		IF NOT IS_BETTING_ENABLED()
			EXIT
		ENDIF
	#ENDIF
		
	// We reset as we are entering a new betting Job
	g_iProcessBetTelemetry = FALSE
	
	EVENT_STRUCT_PLAYER_AVAILABLE_FOR_BETTING Event
	Event.Details.Type 					= SCRIPT_EVENT_PLAYER_AVAILABLE_FOR_BETTING
	Event.Details.FromPlayerIndex 		= PLAYER_ID()
	Event.bettingID 					= bettingID
	Event.bTeamBetting	 				= bTeamBetting
	Event.bTutorial						= IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()		// Are we in the tutorial corona
	Event.iTeam							= GET_MY_TEAM_IN_CORONA()
	
	INT iSendTo = ALL_PLAYERS()
	IF NOT (iSendTo = 0)		
		// Reset anything we were previously involved in
		CLEAR_PLAYER_LOCAL_BETTING_DATA()
		
		// Update our stats and cash for all to see (these may need to be broadcast too)
		UPDATE_PLAYER_BETTING_STATS(bettingID.iMissionType)
		
		// Save out our betting ID so we can handle host migration
		MPGlobals.g_SyncBettingID = bettingID
		
		MPGlobals.g_MPBettingData.stUserName = stUserName
		MPGlobals.g_MPBettingData.stFilename = stFilename

		Event.iStatValue = GET_PLAYER_STAT_SUM(PLAYER_ID())
		
		// We have no previous knowledge about player so randomly place them this time round
		IF Event.iStatValue <= 0
			Event.iStatValue = GET_RANDOM_INT_IN_RANGE(5, 100)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		
			PRINTLN("[JA@BETTING] BROADCAST_AVAILABLE_FOR_BETTING called...")
			PRINTLN("		From script: ", GET_THIS_SCRIPT_NAME())
			PRINTSTRING("		BROADCAST_AVAILABLE_FOR_BETTING iBettingID: ") PRINT_BETTING_ID(bettingID) PRINTNL()
			PRINTLN("		BROADCAST_AVAILABLE_FOR_BETTING iStatVal: ", Event.iStatValue)
			PRINTLN("		BROADCAST_AVAILABLE_FOR_BETTING bTeamBetting: ", PICK_STRING(Event.bTeamBetting, "TRUE", "FALSE"))
			PRINTLN("		BROADCAST_AVAILABLE_FOR_BETTING bTutorial: ", PICK_STRING(Event.bTutorial, "TRUE", "FALSE"))
			PRINTLN("		BROADCAST_AVAILABLE_FOR_BETTING iTeam: ", Event.iTeam)
		#ENDIF
				
		// Set ourselves ready to bet, if anyone else is				
		SET_CLIENT_BETTING_STATE(MP_BETTING_READY)
		
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
	ELSE
		PRINTLN("BROADCAST_AVAILABLE_FOR_BETTING - playerflags = 0 so not broadcasting")
	ENDIF	
ENDPROC

/// PURPOSE: Event for client to notify server they have left a betting area (clear them from betting pool)
PROC BROADCAST_PLAYER_LEFT_BETTING()

	#IF IS_DEBUG_BUILD
		IF NOT IS_BETTING_ENABLED()
			EXIT
		ENDIF
	#ENDIF

	BETTING_ID emptyBettingID
	IF NOT ARE_BETTING_IDS_EQUAL(GET_MY_BETTING_ID(), emptyBettingID)
		EVENT_STRUCT_PLAYER_LEFT_BETTING Event
		Event.Details.Type = SCRIPT_EVENT_PLAYER_LEFT_BETTING
		Event.Details.FromPlayerIndex = PLAYER_ID()

		INT iSendTo = ALL_PLAYERS()
		IF NOT (iSendTo = 0)
		
			PRINTLN("[JA@BETTING] BROADCAST_PLAYER_LEFT_BETTING called...")
			
			// Reset our state so we skip processing every frame (Do we need to clean up everything else?)
			CLEAR_PLAYER_LOCAL_BETTING_DATA()
			SET_CLIENT_BETTING_STATE(MP_BETTING_NULL)
		
			SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
		ELSE
			PRINTLN("BROADCAST_PLAYER_LEFT_BETTING - playerflags = 0 so not broadcasting")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Send event to server to lock down voting so odds / list etc will not change
PROC NOTIFY_SERVER_OF_BETTING_LOCKED(BETTING_ID bettingID)
		
	EVENT_STRUCT_BETTING_POOL_LOCKED Event
	Event.Details.Type = SCRIPT_EVENT_BETTING_POOL_LOCKED
	Event.Details.FromPlayerIndex = PLAYER_ID()
	Event.bettingID = bettingID

	INT iSendTo = ALL_PLAYERS()
	IF NOT (iSendTo = 0)
	
		PRINTLN("[JA@BETTING] NOTIFY_SERVER_OF_BETTING_LOCKED called...")
	
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
	ELSE
		PRINTLN("NOTIFY_SERVER_OF_BETTING_LOCKED - playerflags = 0 so not broadcasting")
	ENDIF
ENDPROC

/// PURPOSE: Called when a mission is launched on the client. Client will see if they have placed bet and take money
PROC BROADCAST_BETTING_MISSION_STARTED()
	
	#IF IS_DEBUG_BUILD
		
		IF g_bDebugDisableTimerForBetting
			g_bDebugDisableTimerForBetting = FALSE
		ENDIF
	
		IF NOT IS_BETTING_ENABLED()
			EXIT
		ENDIF
	#ENDIF
	
	IF IS_BETTING_AVAILABLE_TO_PLAYER(PLAYER_ID(), TRUE)
		NOTIFY_SERVER_OF_BETTING_LOCKED(GET_MY_BETTING_ID())
		
		IF GET_CLIENT_BETTING_STATE() != MP_BETTING_LOCK_DOWN
			SET_CLIENT_BETTING_STATE(MP_BETTING_WAIT_FOR_ODDS)
		ELSE
			PRINTLN("[JA@BETTING] BROADCAST_BETTING_MISSION_STARTED - already in lock down so don't need to wait for final odds")
		ENDIF
	ELSE
		// Reset our state so we skip processing every frame (Do we need to clean up everything else?)
		BROADCAST_PLAYER_LEFT_BETTING()
	ENDIF
ENDPROC

/// PURPOSE: Called when a mission finished on the client. Client will see if they have bet on the winning player
FUNC INT BROADCAST_BETTING_MISSION_FINISHED_TIED()
	INT iScriptTransactionIndex
	// Always reset the players winnings
	INT iTotalEarnings = 0
	g_iMyBetWinnings = 0

	#IF IS_DEBUG_BUILD
		IF NOT IS_BETTING_ENABLED()
			RETURN iTotalEarnings
		ENDIF
	#ENDIF

	IF IS_BETTING_AVAILABLE_TO_PLAYER(PLAYER_ID())
	
		PRINTLN("[JA@BETTING] BROADCAST_BETTING_MISSION_FINISHED_TIED - return player's stake")
		
		IF MPGlobals.g_MPBettingData.iYourTotalBet > 0
			// Return the player stakes from the betting since the result was a tie
			IF USE_SERVER_TRANSACTIONS()
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_BETTING, MPGlobals.g_MPBettingData.iYourTotalBet, iScriptTransactionIndex, DEFAULT, DEFAULT)
				g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl23Challenge = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl31CurrentMissionFileName
			ELSE
				NETWORK_EARN_FROM_BETTING(MPGlobals.g_MPBettingData.iYourTotalBet, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl31CurrentMissionFileName)
				GIVE_LOCAL_PLAYER_CASH(MPGlobals.g_MPBettingData.iYourTotalBet)
			ENDIF
		ENDIF
	
		g_iMyBetWinnings = 0	// Only return stakes so no cash difference
		
		BROADCAST_PLAYER_LEFT_BETTING()
	ELSE
		PRINTLN("[JA@BETTING] BROADCAST_BETTING_MISSION_FINISHED_TIED - BETTING NOT AVAILABLE TO PLAYER")
	ENDIF
	
	PRINTLN("[JA@BETTING] BROADCAST_BETTING_MISSION_FINISHED - overall wins/losses: ", g_iMyBetWinnings)
		
	RETURN g_iMyBetWinnings
ENDFUNC

PROC WRITE_TO_BETTING_SC_LB(INT iScore,INT iBet, INT iPayout, INT iNumBets)
 	//*SCORE_COLUMN( AGG_SUM ) : COLUMN_ID_LB_SCORE - LBCOLUMNTYPE_INT64
   // BOOKIE_INCOME( AGG_SUM ) : COLUMN_ID_LB_BOOKIE_INCOME - LBCOLUMNTYPE_INT64
    //BOOKIE_PAYOUT( AGG_SUM ) : COLUMN_ID_LB_BOOKIE_PAYOUT - LBCOLUMNTYPE_INT64
   // NUMBER_BETS( AGG_SUM ) : COLUMN_ID_LB_NUMBER_BETS - LBCOLUMNTYPE_INT64
	// LEADERBOARD_FREEMODE_GLOBAL_BETS
	//LEADERBOARD_FREEMODE_MISSION_BETS
	
	// Don't write if spectating
	IF IS_PLAYER_SCTV(PLAYER_ID())
	
		PRINTLN("WRITE_TO_BETTING_SC_LBbypassed because IS_PLAYER_SCTV")
		
		EXIT
	ENDIF
	
	PRINTLN("WRITE_TO_BETTING_SC_LB ") 
	TEXT_LABEL_23 sIdentifier[2]
	TEXT_LABEL_31 categoryName[2]
	BOOL bGameTypeAdded
	INT iNumCategories
	IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_GLOBAL_BETS,sIdentifier,categoryName,0)
		
					
		// Write score
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,iScore,0)

		// Write to total value of bets placed
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BOOKIE_INCOME,iBet,0)
				
		// Write to total value of payout
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BOOKIE_PAYOUT,iPayout,0)
					
		// Write to total num of bets placed		
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUMBER_BETS,iNumBets,0)
				
		PRINTLN(" -<>- WRITE_TO_BETTING_SC_LB:(GLOBAL) Score: ", iScore)
		PRINTLN(" -<>- WRITE_TO_BETTING_SC_LB:(GLOBAL) Value of bets: ", iBet)
		PRINTLN(" -<>- WRITE_TO_BETTING_SC_LB:(GLOBAL) Payout of bets: ", iPayout)
		PRINTLN(" -<>- WRITE_TO_BETTING_SC_LB:(GLOBAL) Num best placed: ", iNumBets)
		PRINTLN(" -<>- WRITE_TO_BETTING_SC_LB:(GLOBAL) stUserName: ", MPGlobals.g_MPBettingData.stUserName)
		PRINTLN(" -<>- WRITE_TO_BETTING_SC_LB:(GLOBAL) stFileName: ", MPGlobals.g_MPBettingData.stFilename)
		
		
		categoryName[0] = "Mission"
		BETTING_ID bettingID = GET_MY_BETTING_ID()
		BOOL bWriteToMissionBoard = FALSE
		SWITCH bettingID.iMissionType
			CASE FMMC_TYPE_MG_ARM_WRESTLING
				sIdentifier[0] = "FREEMODE$ARMW"
				bWriteToMissionBoard = TRUE
			BREAK
			
			CASE FMMC_TYPE_MG_DARTS
				sIdentifier[0] = "FREEMODE$DARTS"
				bWriteToMissionBoard = TRUE
			BREAK
			
			CASE FMMC_TYPE_MG_GOLF
				sIdentifier[0] = "FREEMODE$GOLF"
				bWriteToMissionBoard = TRUE
			BREAK
			
			CASE FMMC_TYPE_MG_TENNIS
				sIdentifier[0] = "FREEMODE$TENNIS"
				bWriteToMissionBoard = TRUE
			BREAK
			
			CASE FMMC_TYPE_MG_RANGE_COVERED
			CASE FMMC_TYPE_MG_RANGE_GRID
			CASE FMMC_TYPE_MG_SHOOTING_RANGE
			CASE FMMC_TYPE_MG_RANGE_RANDOM
				sIdentifier[0] = "FREEMODE$SRANGE"
				bWriteToMissionBoard = TRUE
			BREAK
			
			DEFAULT
				IF NOT ARE_STRINGS_EQUAL(MPGlobals.g_MPBettingData.stFilename,"")
				AND NOT ARE_STRINGS_EQUAL(MPGlobals.g_MPBettingData.stUserName,"")
					sIdentifier[0] = MPGlobals.g_MPBettingData.stFilename
					bWriteToMissionBoard = TRUE
				ENDIF
			BREAK
		ENDSWITCH

		categoryName[1] = "GameType"
		SWITCH bettingID.iMissionType
			CASE FMMC_TYPE_MISSION
				SWITCH bettingID.iMissionVar
					CASE ciMISSION_SCORING_TYPE_TIME
						sIdentifier[1] = "MISSIONTIME"
					BREAK
					CASE ciMISSION_SCORING_TYPE_ALL_VS_1
					CASE ciMISSION_SCORING_TYPE_FIRST_FINISH
					CASE ciMISSION_SCORING_TYPE_HIGHEST
					CASE ciMISSION_SCORING_TYPE_SHOW_SCORING
						sIdentifier[1] = "MISSIONSCORE"
					BREAK
					DEFAULT 
						sIdentifier[1] = "MISSION"
					BREAK
				ENDSWITCH
				bGameTypeAdded = TRUE
			BREAK
			CASE FMMC_TYPE_DEATHMATCH
				SWITCH INT_TO_ENUM(LBD_SUB_MODE ,bettingID.iMissionVar)
					CASE SUB_DM_FFA
						sIdentifier[1] = "DMFFA"
					BREAK
					CASE SUB_DM_TEAM
						sIdentifier[1] = "DMTEAM"
					BREAK
					CASE SUB_DM_VEH
						sIdentifier[1] = "DMVEH"
					BREAK
					
					DEFAULT 
						sIdentifier[1] = "DM"
					BREAK
				ENDSWITCH
				bGameTypeAdded = TRUE
			BREAK
			CASE FMMC_TYPE_RACE
				SWITCH bettingID.iMissionVar
					CASE ciRACE_SUB_TYPE_STANDARD
						sIdentifier[1] = "RACENORM"
					BREAK
					CASE ciRACE_SUB_TYPE_GTA
						sIdentifier[1] = "GTARACE"
					BREAK
					CASE ciRACE_SUB_TYPE_RALLY
						sIdentifier[1] = "RALLY"
					BREAK
					DEFAULT 
						sIdentifier[1] = "RACE"
					BREAK
				ENDSWITCH
				bGameTypeAdded = TRUE
			BREAK
			CASE FMMC_TYPE_SURVIVAL
				sIdentifier[1] = "SURVIVAL"
				bGameTypeAdded = TRUE
			BREAK
			CASE FMMC_TYPE_IMPROMPTU_DM
				sIdentifier[1] = "IMPDM"
				bGameTypeAdded = TRUE
			BREAK
//			CASE FMMC_TYPE_TRIATHLON
//				sIdentifier[1] = "TRIATHLON"
//				bGameTypeAdded = TRUE
//			BREAK
			CASE FMMC_TYPE_BASE_JUMP
				sIdentifier[1] = "BASEJ"
				bGameTypeAdded = TRUE
			BREAK
			CASE FMMC_TYPE_RACE_TO_POINT
				sIdentifier[1] = "R2P"
				bGameTypeAdded = TRUE
			BREAK
			CASE FMMC_TYPE_PLAYLIST
				sIdentifier[1] = "PLAYLIST"
				bGameTypeAdded = TRUE
			BREAK
		ENDSWITCH

		IF bWriteToMissionBoard
			IF bGameTypeAdded
				iNumCategories = 2
			ELSE
				iNumCategories = 1
			ENDIF
			IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_MISSION_BETS,sIdentifier,categoryName,iNumCategories)
				
				// Write score
				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,iScore,0)

				// Write to total value of bets placed
				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,iBet,0)
						
				// Write to total value of payout
				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,iPayout,0)
							
				// Write to total num of bets placed		
				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,iNumBets,0)
				
				PRINTLN(" -<>- WRITE_TO_BETTING_SC_LB:(MISSION) Score: ", iScore)
				PRINTLN(" -<>- WRITE_TO_BETTING_SC_LB:(MISSION) Value of bets: ", iBet)
				PRINTLN(" -<>- WRITE_TO_BETTING_SC_LB:(MISSION) Payout of bets: ", iPayout)
				PRINTLN(" -<>- WRITE_TO_BETTING_SC_LB:(MISSION) Num best placed: ", iNumBets)
			ENDIF
		ENDIF
	ENDIF
	//mpGlobals.g_bFlushLBWrites = TRUE
ENDPROC 

PROC CHECK_AND_GIVE_INT_BETTING_AWARD(MP_INT_AWARD anaward, int iteam)

	    IF NOT IS_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(anAward) 
	        IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), GET_AWARD_INTCHAR_LEVEL_NUMBER(anAward, AWARDPOSITIONS_BRONZE, iTeam)) //10 Seconds
	            SET_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(anAward, TRUE, iTeam)
	            
	        ENDIF
	    ELSE
	        IF NOT IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), GET_AWARD_INTCHAR_LEVEL_NUMBER(anAward, AWARDPOSITIONS_BRONZE, iTeam)) //10 Seconds
	            SET_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(anAward, FALSE, iTeam)
	        ENDIF
	    ENDIF
	    
	    IF NOT IS_MP_AWARD_SILVER_INTCHAR_UNLOCKED(anAward) 
	        IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), GET_AWARD_INTCHAR_LEVEL_NUMBER(anAward, AWARDPOSITIONS_SILVER, iTeam)) //10 Minutes
	            SET_MP_AWARD_SILVER_INTCHAR_UNLOCKED(anAward, TRUE, iTeam)
	        ENDIF
	    ELSE
	        IF NOT IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), GET_AWARD_INTCHAR_LEVEL_NUMBER(anAward, AWARDPOSITIONS_SILVER, iTeam)) //10 Minutes
	            SET_MP_AWARD_SILVER_INTCHAR_UNLOCKED(anAward, FALSE, iTeam)
	        ENDIF
	    ENDIF
	    
	    IF NOT IS_MP_AWARD_GOLD_INTCHAR_UNLOCKED(anAward) 
	        IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), GET_AWARD_INTCHAR_LEVEL_NUMBER(anAward, AWARDPOSITIONS_GOLD, iTeam)) //30 Minutes
	            SET_MP_AWARD_GOLD_INTCHAR_UNLOCKED(anAward, TRUE, iTeam)
	        ENDIF
	    ELSE
	        IF NOT IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), GET_AWARD_INTCHAR_LEVEL_NUMBER(anAward, AWARDPOSITIONS_GOLD, iTeam)) //30 Minutes
	            SET_MP_AWARD_GOLD_INTCHAR_UNLOCKED(anAward, FALSE, iTeam)
	        ENDIF
	    ENDIF
	    
	    IF NOT IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(anAward) 
	        IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), GET_AWARD_INTCHAR_LEVEL_NUMBER(anAward, AWARDPOSITIONS_PLATINUM, iTeam)) //1 Hour
	            SET_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(anAward, TRUE, iTeam)
	        ENDIF
	    ELSE
	        IF NOT IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), GET_AWARD_INTCHAR_LEVEL_NUMBER(anAward, AWARDPOSITIONS_PLATINUM, iTeam)) //1 Hour
	            SET_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(anAward, FALSE, iTeam)
	        ENDIF
	    ENDIF
	
ENDPROC

FUNC INT PROCESS_BETTING_WINNINGS_FOR_INDEX(INT iBettingIndex)
	INT iScriptTransactionIndex
	INT iTotalEarnings = 0
	IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[iBettingIndex].iWinnings > 0
		// We have placed a bet on the winning player					
		PRINTLN("[JA@BETTING] PROCESS_BETTING_WINNINGS_FOR_INDEX - Player WON this bet with winnings of: ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[iBettingIndex].iWinnings)
		
		INT iProfit = (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[iBettingIndex].iWinnings - GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[iBettingIndex].iPlacedBet)
		
		// Right the profit for this bet into award
		PRINTLN("[JA@BETTING] PROCESS_BETTING_WINNINGS_FOR_INDEX - Incrementing stat FMBBETWIN: ", iProfit)
		INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FMBBETWIN, iProfit)
		CHECK_AND_GIVE_INT_BETTING_AWARD(MP_AWARD_FMBBETWIN, TEAM_FREEMODE)
		
		//INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_GAINED_BETTING, GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[iWinnerBettingSlot].iWinnings)
		SET_PACKED_STAT_BOOL(PACKED_MP_GAINED_FROM_BETTING, TRUE) 

		
		// CONOR WRITE TO WINNINGS BOARD
		WRITE_TO_BETTING_SC_LB(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[iBettingIndex].iWinnings,
								0,GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[iBettingIndex].iWinnings,0)
								

		iTotalEarnings = GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[iBettingIndex].iWinnings //- MPGlobals.g_MPBettingData.iYourTotalBet)				
		g_iMyWinningBet = GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[iBettingIndex].iPlacedBet
		PRINTLN("[JA@BETTING] PROCESS_BETTING_WINNINGS_FOR_INDEX - The winning bet: ", g_iMyWinningBet)
		PRINTLN("[JA@BETTING] PROCESS_BETTING_WINNINGS_FOR_INDEX - Calling NETWORK_EARN_FROM_BETTING / REQUEST_SYSTEM_ACTIVITY_TYPE_MADE_MONEY_BETTING with: ", iTotalEarnings, " and ContentID: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl31CurrentMissionFileName)
		
		IF USE_SERVER_TRANSACTIONS()
			TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_BETTING, iTotalEarnings, iScriptTransactionIndex, DEFAULT, DEFAULT)
			g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl23Challenge = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl31CurrentMissionFileName
		ELSE
			NETWORK_EARN_FROM_BETTING(iTotalEarnings, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl31CurrentMissionFileName)
			GIVE_LOCAL_PLAYER_CASH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[iBettingIndex].iWinnings)
		ENDIF
	ELSE
		PRINTLN("[JA@BETTING] PROCESS_BETTING_WINNINGS_FOR_INDEX - Player didn't win any bets")
	ENDIF

	RETURN iTotalEarnings
ENDFUNC


// Function to handle the tutorial ped winning the race
FUNC INT BROADCAST_BETTING_TUTORIAL_PED_FINISHED()
	INT iTotalEarnings = 0
	g_iMyBetWinnings = 0
	g_iMyWinningBet = 0

	#IF IS_DEBUG_BUILD
		IF NOT IS_BETTING_ENABLED()
			RETURN iTotalEarnings
		ENDIF
	#ENDIF

	IF IS_BETTING_AVAILABLE_TO_PLAYER(PLAYER_ID())
	
		PRINTLN("[JA@BETTING] BROADCAST_BETTING_TUTORIAL_PED_FINISHED - winner: LAMAR")
	
		IF HAS_PLAYER_PLACED_BETS()
			IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
				iTotalEarnings = PROCESS_BETTING_WINNINGS_FOR_INDEX(BETTING_TUTORIAL_PED_INDEX)
				
				PRINTLN("[JA@BETTING] BROADCAST_BETTING_TUTORIAL_PED_FINISHED - total earnings = $", iTotalEarnings)
			ENDIF
		ELSE
			PRINTLN("[JA@BETTING] BROADCAST_BETTING_TUTORIAL_PED_FINISHED - did not have any bets placed")
		ENDIF
				
		g_iMyBetWinnings = (iTotalEarnings - MPGlobals.g_MPBettingData.iYourTotalBet)
		
		BROADCAST_PLAYER_LEFT_BETTING()
	ELSE
		PRINTLN("[JA@BETTING] BROADCAST_BETTING_TUTORIAL_PED_FINISHED - BETTING NOT AVAILABLE TO PLAYER")
	ENDIF
	
	PRINTLN("[JA@BETTING] BROADCAST_BETTING_TUTORIAL_PED_FINISHED - overall wins/losses: ", g_iMyBetWinnings)	
	
	RETURN g_iMyBetWinnings
ENDFUNC

/// PURPOSE: Called when a mission finished on the client. Client will see if they have bet on the winning player
FUNC INT BROADCAST_BETTING_MISSION_FINISHED(PLAYER_INDEX playerID)

	INT iTotalEarnings = 0
	g_iMyBetWinnings = 0
	g_iMyWinningBet = 0

	#IF IS_DEBUG_BUILD
		IF NOT IS_BETTING_ENABLED()
			RETURN iTotalEarnings
		ENDIF
	#ENDIF

	IF IS_BETTING_AVAILABLE_TO_PLAYER(PLAYER_ID())
	
		PRINTLN("[JA@BETTING] BROADCAST_BETTING_MISSION_FINISHED - winner: ", GET_PLAYER_NAME(playerID), " his betting Index is: ", GlobalServerBD_Betting.playerBettingData[NATIVE_TO_INT(playerID)].iTeam, " T(", GET_CLOUD_TIME_AS_INT(), ")")
	
		IF HAS_PLAYER_PLACED_BETS()
			IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
						
				INT iWinnerBettingSlot = GlobalServerBD_Betting.playerBettingData[NATIVE_TO_INT(playerID)].iTeam
			
				iTotalEarnings = PROCESS_BETTING_WINNINGS_FOR_INDEX(iWinnerBettingSlot)
				
				PRINTLN("[JA@BETTING] BROADCAST_BETTING_MISSION_FINISHED - total earnings = $", iTotalEarnings)
			ENDIF
		ELSE
			PRINTLN("[JA@BETTING] BROADCAST_BETTING_MISSION_FINISHED - did not have any bets placed")
		ENDIF
				
		g_iMyBetWinnings = (iTotalEarnings - MPGlobals.g_MPBettingData.iYourTotalBet)
		
		// Update the new system activities
		PRINTLN("[JA@BETTING] BROADCAST_BETTING_MISSION_FINISHED - REQUEST_SYSTEM_ACTIVITY_TYPE_MADE_MONEY_BETTING - ", g_iMyBetWinnings)
		IF g_iMyBetWinnings != 0
			REQUEST_SYSTEM_ACTIVITY_TYPE_MADE_MONEY_BETTING(g_iMyBetWinnings)
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[JA@BETTING] BROADCAST_BETTING_MISSION_FINISHED - g_iMyBetWinnings = 0 so NOT calling REQUEST_SYSTEM_ACTIVITY_TYPE_MADE_MONEY_BETTING ")
		#ENDIF
		ENDIF
		
		BROADCAST_PLAYER_LEFT_BETTING()
	ELSE
		PRINTLN("[JA@BETTING] BROADCAST_BETTING_MISSION_FINISHED - BETTING NOT AVAILABLE TO PLAYER")
	ENDIF
	
	PRINTLN("[JA@BETTING] BROADCAST_BETTING_MISSION_FINISHED - overall wins/losses: ", g_iMyBetWinnings)
	
	RETURN g_iMyBetWinnings
ENDFUNC

/// PURPOSE: This will broadcast the players delta cash bet on the betting index so the server can handle it
PROC BROADCAST_NEW_CASH_BET_ON_BETTING_INDEX(INT iBettingIndex)

	INT iCashDelta = MPGlobals.g_MPBettingData.iBetOnCoronaPlayer - GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[iBettingIndex].iPlacedBet
		
	EVENT_STRUCT_BETTING_VIRTUAL_CASH_BET Event
	Event.Details.Type = SCRIPT_EVENT_VIRTUAL_CASH_BET
	Event.Details.FromPlayerIndex = PLAYER_ID()
	Event.iBettingIndex = iBettingIndex
	Event.iCashDelta = iCashDelta
	
	INT iSendTo = ALL_PLAYERS()
	IF NOT (iSendTo = 0)
	
		PRINTLN("[JA@BETTING] BROADCAST_NEW_CASH_BET_ON_BETTING_INDEX called...(", GET_CLOUD_TIME_AS_INT(), ")")
		PRINTLN("[JA@BETTING] 		- iBettingIndex = ", iBettingIndex)
		PRINTLN("[JA@BETTING] 		- iCashDelta = ", iCashDelta)
		PRINTLN("[JA@BETTING] ********************************************************")
		
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
		
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("BROADCAST_NEW_CASH_BET_ON_BETTING_INDEX - playerflags = 0 so not broadcasting")
	#ENDIF
		
	ENDIF

ENDPROC

/// PURPOSE: Handle the rejection from the server
PROC PROCESS_BETTING_VIRTUAL_CASH_REJECTED(INT iEventID)
	
	EVENT_STRUCT_BETTING_VIRTUAL_CASH_REJECTED Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
	
		PRINTLN("[JA@BETTING] PROCESS_BETTING_VIRTUAL_CASH_REJECTED - rejected bet")
		PRINTLN("[JA@BETTING] 					- iBettingIndex = ", Event.iBettingIndex)
		PRINTLN("[JA@BETTING] 					- iCashDelta = ", Event.iCashDelta)
	
		INT iCurrentBet = GET_LOCAL_CASH_BET_ON_PLAYER(Event.iBettingIndex)		
		iCurrentBet -= Event.iCashDelta
		
		PRINTLN("[JA@BETTING] 					- Player broadcast current bet = ", iCurrentBet)
	
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[Event.iBettingIndex].iPlacedBet = iCurrentBet
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.iBetSyncID = GET_UNIQUE_BETTING_SYNC_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.iBetSyncID)
		
		MPGlobals.g_bBettingDetailsUpdated = TRUE
		MPGlobals.g_MPBettingRejected.bBettingRejected = TRUE
		MPGlobals.g_MPBettingRejected.iBettingIndex = Event.iBettingIndex
	ELSE
		SCRIPT_ASSERT("PROCESS_BETTING_VIRTUAL_CASH_REJECTED - could not retrieve data")
	ENDIF
	
ENDPROC


/// PURPOSE: Process a bet on the selected player in corona
PROC UPDATE_CURRENT_BET_ON_PLAYER(INT iBettingIndex)
	
	PRINTLN("[JA@BETTING] UPDATE_CURRENT_BET_ON_PLAYER - called...")
	PRINTLN("				Bet already on betting index ", iBettingIndex, " : ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[iBettingIndex].iPlacedBet)
	PRINTLN("				Bet being placed: ", MPGlobals.g_MPBettingData.iBetOnCoronaPlayer)
	
	BROADCAST_NEW_CASH_BET_ON_BETTING_INDEX(iBettingIndex)
		
	// Set the amount we are betting on this slot
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[iBettingIndex].iPlacedBet = MPGlobals.g_MPBettingData.iBetOnCoronaPlayer
		
	MPGlobals.g_MPBettingData.bClientHasBet = TRUE
	
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.iBetSyncID = GET_UNIQUE_BETTING_SYNC_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.iBetSyncID)
ENDPROC

/// PURPOSE: Detects if client betting list is out of sync with server (TRUE if updated)
FUNC BOOL SYNC_CLIENT_BETTING_LIST()
	
	INT iMyGBDSlot = GET_CORONA_GBD_SLOT()
	
	IF MPGlobals.g_MPBettingData.iClientSyncID != GET_PLAYER_BETTING_SYNC_ID(iMyGBDSlot)
		MPGlobals.g_MPBettingData.iClientSyncID = GET_PLAYER_BETTING_SYNC_ID(iMyGBDSlot)
		
		PRINTLN("[JA@BETTING] SYNC_CLIENT_BETTING_LIST - IDs out of sync. Update to ID: ", MPGlobals.g_MPBettingData.iClientSyncID)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC 

PROC CALCULATE_MY_WINNINGS()
	
	IF IS_BIT_SET(GlobalServerBD_Betting.playerBettingData[NATIVE_TO_INT(PLAYER_ID())].iBSPlayerState, BS_BETTING_POOL_VOID)
		// If we had placed bets then refund the cash
		PRINTLN("[JA@BETTING] Beting pool is void, don't calculate winnings, refunding: ", MPGlobals.g_MPBettingData.iYourTotalBet)	
		EXIT
	ENDIF

	IF HAS_PLAYER_PLACED_BETS()

		INT i
		INT iOddNum
		INT iOddDenom
		INT iBetOnPlayer
		INT iTotalBet
		INT iBetsPlaced
		REPEAT NUM_NETWORK_PLAYERS i
			IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
				GET_PLAYER_BETTING_ODDS(i, iOddNum, iOddDenom)
				
				iBetOnPlayer = GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[i].iPlacedBet
				
				IF iOddDenom != 0
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[i].iWinnings = 
									ROUND((iBetOnPlayer * (TO_FLOAT(iOddNum) / TO_FLOAT(iOddDenom))) + iBetOnPlayer)
				ENDIF
				
				IF iBetOnPlayer > 0
					iBetsPlaced++
					
					// If we are processing bets on ourselves?
					IF i = GlobalServerBD_Betting.playerBettingData[NATIVE_TO_INT(PLAYER_ID())].iTeam
					
						// Check if there is a gift active
						IF MPGlobals.g_MPBettingData.iBetGiftAmount > 0
							
							// Reduce our 'bet' by the gift
							IF iBetOnPlayer > MPGlobals.g_MPBettingData.iBetGiftAmount
								iBetOnPlayer -= MPGlobals.g_MPBettingData.iBetGiftAmount
							ELSE
								iBetOnPlayer = 0
							ENDIF
							
							PRINTLN("[JA@BETTING] CALCULATE_MY_WINNINGS - Adjust bet on ME for R* gift: $", MPGlobals.g_MPBettingData.iBetGiftAmount, ". Cash Bet Now: $", iBetOnPlayer)
						ENDIF
					ENDIF
					
					
					iTotalBet += iBetOnPlayer
					
					PRINTLN("[JA@BETTING] CALCULATE_MY_WINNINGS - Bet on betting index: ", i, ". Cash Bet: $", iBetOnPlayer)
					PRINTLN("[JA@BETTING] CALCULATE_MY_WINNINGS - Final odds for betting index: ", iOddNum, "/", iOddDenom)
					PRINTLN("[JA@BETTING] CALCULATE_MY_WINNINGS - Potential Winnings: ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[i].iWinnings)
					
				ENDIF
			ENDIF
		ENDREPEAT
		
		PRINTLN("[JA@BETTING] CALCULATE_MY_WINNINGS - I have placed bets. Total Cash: $", iTotalBet, " g_tl31uniqueMatchId = ", g_tl31uniqueMatchId)

		MPGlobals.g_MPBettingData.iYourTotalBet = iTotalBet							// Amount of cash player will lose
		
		IF NETWORK_CAN_BET(iTotalBet)
		AND NETWORK_CAN_SPEND_MONEY(iTotalBet, FALSE, FALSE, FALSE)
		
			IF iTotalBet > 0
				INT iScriptTransactionIndex
				IF USE_SERVER_TRANSACTIONS()
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_BETTING, iTotalBet, iScriptTransactionIndex, DEFAULT, DEFAULT)
					g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl23Challenge = g_tl31uniqueMatchId
				ELSE
					NETWORK_SPENT_BETTING(iTotalBet, 0, g_tl31uniqueMatchId)						// Update cash based on our total bets
					GIVE_LOCAL_PLAYER_CASH(-1 * iTotalBet)										// Deduct money from player
				ENDIF
				WRITE_TO_BETTING_SC_LB((-1*iTotalBet),iTotalBet,0,iBetsPlaced)				// Write to global leaderboards
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_BETAMOUNT, iTotalBet)			// Update player stat
			ENDIF
		ELSE
			IF GET_BETTING_GIFT_VALUE() <= 0
				PRINTLN("[JA@BETTING] CALCULATE_MY_WINNINGS - Player can not bet: $", iTotalBet, ". LEAVE BETTING, CAN_SPEND_MONEY = ", NETWORK_CAN_SPEND_MONEY(iTotalBet, FALSE, FALSE, FALSE))
				//SCRIPT_ASSERT("BETTING: Player can not afford the total bets > cash_in_wallet. Leaving betting")
				BROADCAST_PLAYER_LEFT_BETTING()
			ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[JA@BETTING] CALCULATE_MY_WINNINGS - I have NOT placed bets")
	#ENDIF
	ENDIF
ENDPROC

PROC UPDATE_BETTING_TELEMETRY()
	
	IF MPGlobals.g_MPBettingData.iYourTotalBet > 0
		PRINTLN("[JA@BETTING] Calling UPDATE_BETTING_TELEMETRY with Total Bet: ", MPGlobals.g_MPBettingData.iYourTotalBet, " and Mission Type: ", g_FMMC_STRUCT.iMissionType," and Rake: ", (g_sMPTunables.fRake*0.01))
	
		PLAYSTATS_ROS_BET(MPGlobals.g_MPBettingData.iYourTotalBet, g_FMMC_STRUCT.iMissionType, NATIVE_TO_INT(PLAYER_ID()), (g_sMPTunables.fRake*0.01))	
	ENDIF
ENDPROC

/// PURPOSE: Check to see if server has updated our betting ID
PROC PROCESS_WAIT_FOR_BETTING_SERVER_UPDATE()
	
	// Check to see if the server has us betting
	IF ARE_BETTING_IDS_EQUAL(GET_MY_BETTING_ID(), MPGlobals.g_SyncBettingID)
		
		//...yes, exit. Nothing to update
		EXIT
	ENDIF
	
	// Check if we have requested over 5s ago
	IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.g_BettingSyncTimer)) > 5000
	
		PRINTLN("[JA@BETTING] PROCESS_WAIT_FOR_BETTING_SERVER_UPDATE - betting ID not synced with server. Try again.")
	
		//...yes, broadcast request again and reset timer
		BROADCAST_PLAYER_AVAILABLE_FOR_BETTING(MPGlobals.g_SyncBettingID)
	ENDIF
ENDPROC

FUNC BOOL IS_ALTERNATE_BETTING_PLACE_BETS_BUTTON_PRESSED(INT iBettingIndex)

	// If we are not selecting a valid betting index, return
	IF iBettingIndex < 0
		RETURN FALSE
	ENDIF

	// If our current bet is the amount already placed, return
	IF MPGlobals.g_MPBettingData.iBetOnCoronaPlayer = GET_LOCAL_CASH_BET_ON_PLAYER(iBettingIndex)
		RETURN FALSE
	ENDIF

	// Allow the player to place a bet on this betting index
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)	// Square to place bets
		
		PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
	
		//UPDATE_CURRENT_BET_ON_PLAYER()
	
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

// -------------------- MAIN SYSTEM LOOP ---------------------

/// PURPOSE: Main client loop for current state.
PROC MAINTAIN_CLIENT_BETTING()

	#IF IS_DEBUG_BUILD
		IF NOT IS_BETTING_ENABLED()
			EXIT
		ENDIF
	#ENDIF
	
	// Monitor our global switch for betting
	IF GET_CLIENT_BETTING_STATE() > MP_BETTING_NULL
		IF g_sMPTunables.bDisableJobBetting
			PRINTLN("[JA@BETTING] MAINTAIN_CLIENT_BETTING - g_sMPTunables.bDisableJobBetting = TRUE. Leave betting")
			BROADCAST_PLAYER_LEFT_BETTING()
		ENDIF
	ENDIF
	
	
	SWITCH GET_CLIENT_BETTING_STATE()

		CASE MP_BETTING_NULL
			// Player is not in betting mode (not broadcast event)
		BREAK

		// Player has notified server, wait for other players to be available
		CASE MP_BETTING_READY
		
			PROCESS_WAIT_FOR_BETTING_SERVER_UPDATE()		
		
			IF IS_BETTING_AVAILABLE_TO_PLAYER(PLAYER_ID())
				UPDATE_PLAYER_BETTING_STATS()
				//UPDATE_BETTING_PLAYERS_LIST(TRUE)
				SET_CLIENT_BETTING_STATE(MP_BETTING_AVAILABLE)
			ENDIF
		BREAK
		
		// Enough players for betting to take place - listen for menu being requested
		CASE MP_BETTING_AVAILABLE
			IF NOT IS_BETTING_AVAILABLE_TO_PLAYER(PLAYER_ID())
				PRINTLN("[JA@BETTING] MAINTAIN_CLIENT_BETTING - MP_BETTING_AVAILABLE betting no longer available.")
				CLEAR_PLAYER_LOCAL_BETTING_DATA()
				SET_CLIENT_BETTING_STATE(MP_BETTING_READY)
			ENDIF			
		BREAK
				
		// Listen for updates to list / bets and wait for betting to end
		CASE MP_BETTING_PLACING_BETS		
		
			IF SYNC_CLIENT_BETTING_LIST()			
				//UPDATE_BETTING_PLAYERS_LIST()
								
				// NEW: Update flag so corona picks up there has been an update
				MPGlobals.g_bBettingDetailsUpdated = TRUE
			ENDIF
			
			IF NOT IS_BETTING_AVAILABLE_TO_PLAYER(PLAYER_ID())
				PRINTLN("[JA@BETTING] MAINTAIN_CLIENT_BETTING - MP_BETTING_PLACING_BETS betting no longer available.")
				
				CLEAR_PLAYER_LOCAL_BETTING_DATA()
				SET_CLIENT_BETTING_STATE(MP_BETTING_READY)
			ENDIF
		
		BREAK
		
		CASE MP_BETTING_WAIT_FOR_ODDS
			IF ARE_FINAL_ODDS_CALCULATED(NATIVE_TO_INT(PLAYER_ID()))		// Once server has calculated odds
			AND g_iProcessBetTelemetry										// And the Job / Activity has started
				
				//...calculate winnings
				CALCULATE_MY_WINNINGS()
				
				IF NOT IS_BIT_SET(GlobalServerBD_Betting.playerBettingData[NATIVE_TO_INT(PLAYER_ID())].iBSPlayerState, BS_BETTING_POOL_VOID)
					//...update our telemetry if valid
					UPDATE_BETTING_TELEMETRY()
				ENDIF
			
				g_iProcessBetTelemetry = FALSE
			
				SET_CLIENT_BETTING_STATE(MP_BETTING_LOCK_DOWN)
			ENDIF
		BREAK
		
		CASE MP_BETTING_LOCK_DOWN
			
			// If the betting pool is void - leave the betting / display message
			IF IS_BIT_SET(GlobalServerBD_Betting.playerBettingData[NATIVE_TO_INT(PLAYER_ID())].iBSPlayerState, BS_BETTING_POOL_VOID)
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_MIDSIZED, "BM_BET_CANC", "BM_BET_CANCS", HUD_COLOUR_RED)
				BROADCAST_PLAYER_LEFT_BETTING()
				
				PRINTLN("[JA@BETTING] MAINTAIN_CLIENT_BETTING - MP_BETTING_LOCK_DOWN betting pool is void. Clean us up from here, broadcasting if we haven't already been removed from pool")
				
				// We need to get out of this state:
				CLEAR_PLAYER_LOCAL_BETTING_DATA()
				SET_CLIENT_BETTING_STATE(MP_BETTING_NULL)
			ENDIF				
			
			// If the transaction failed - leave the betting / display message
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.iBSBettingState, BETTING_CLIENT_TRANS_FAILED)
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_MIDSIZED, "BM_BET_CANC", "BM_BET_CANCS", HUD_COLOUR_RED)
				BROADCAST_PLAYER_LEFT_BETTING()
				
				PRINTLN("[JA@BETTING] MAINTAIN_CLIENT_BETTING - MP_BETTING_LOCK_DOWN betting spend transaction failed. Clean us up from here, broadcasting if we haven't already been removed from pool")
				
				// We need to get out of this state:
				CLEAR_PLAYER_LOCAL_BETTING_DATA()
				SET_CLIENT_BETTING_STATE(MP_BETTING_NULL)
			ENDIF
		
		BREAK
		
	ENDSWITCH
ENDPROC

// -------------------- DEBUG FUNCTIONS ----------------------

#IF IS_DEBUG_BUILD

FUNC BOOL IS_BETTING_DEBUG_CONTROL_ON()
	RETURN g_bDebugControlBetting
ENDFUNC

/// PURPOSE: Create widgets used to debug betting
///    g_bDebugSendAvailForBetting: Client will send event to be available for betting
///    g_bDebugSendLeaveBetting: Client will send event to leave betting (not on mission or crashed)
///    g_iDebugBettingUniqueID: Set to specify unique ID for betting
PROC CREATE_BETTING_WIDGETS(WIDGET_GROUP_ID wgID)
	INT i

	SET_CURRENT_WIDGET_GROUP(wgID)
		START_WIDGET_GROUP("Betting")
		
			START_WIDGET_GROUP("Global Control of Betting")
				ADD_WIDGET_BOOL("g_bDebugAllowTwoPlayerBetting", g_bDebugAllowTwoPlayerBetting)
				ADD_WIDGET_BOOL("g_bDebugBettingAllowUnlimitedBets", g_bDebugBettingAllowUnlimitedBets)
				ADD_WIDGET_STRING("Turn betting on and off (DO NOT CHANGE value of Betting Enabled!!)")
				ADD_WIDGET_BOOL("TOGGLE BETTING (default off)", g_bDebugEnableBetting)
				ADD_WIDGET_BOOL("Betting Enabled (READY ONLY)", GlobalServerBD.g_bDebugBettingEnabled)
			STOP_WIDGET_GROUP()
		
			START_WIDGET_GROUP("Client Widgets")
			
				ADD_WIDGET_BOOL("Display debug version of menu", g_bDebugBettingMenu)
			
				ADD_WIDGET_STRING("Use to simulate joining / leaving betting")
				
				ADD_WIDGET_BOOL("Take Debug Control of betting (outside corona)", g_bDebugControlBetting)
				ADD_WIDGET_BOOL("Disable Corona Timer (client)", g_bDebugDisableTimerForBetting)
				ADD_WIDGET_BOOL("I am available for betting", g_bDebugSendAvailForBetting)
				ADD_WIDGET_BOOL("I have left betting", g_bDebugSendLeaveBetting)
				ADD_WIDGET_BOOL("Mission has begun", g_bDebugSendMissionLaunched)
				ADD_WIDGET_BOOL("Mission has finished", g_bDebugSendMissionFinished)
				ADD_WIDGET_INT_SLIDER("Betting ID:", g_iDebugBettingUniqueID, 0, NUM_NETWORK_PLAYERS, 1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Client Data")
				START_WIDGET_GROUP("Client State Details")
					ADD_WIDGET_STRING("0 - MP_BETTING_NULL")
					ADD_WIDGET_STRING("1 - MP_BETTING_READY")
					ADD_WIDGET_STRING("2 - MP_BETTING_AVAILABLE")
					ADD_WIDGET_STRING("3 - MP_BETTING_PLACING_BETS")
					ADD_WIDGET_STRING("4 - MP_BETTING_WAIT_FOR_ODDS")
					ADD_WIDGET_STRING("5 - MP_BETTING_LOCK_DOWN")
				STOP_WIDGET_GROUP()
				
				ADD_WIDGET_INT_READ_ONLY("Client Betting State:", MPGlobals.g_MPBettingData.clientBettingState)
				ADD_WIDGET_INT_READ_ONLY("Betting Sync ID:", MPGlobals.g_MPBettingData.iClientSyncID)
				ADD_WIDGET_INT_READ_ONLY("Cash Pot:", MPGlobals.g_MPBettingData.iClientPotTotal)
				ADD_WIDGET_BOOL("Has player bet:", MPGlobals.g_MPBettingData.bClientHasBet)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Server Broadcast Data")
				TEXT_LABEL_63 tlBetting
			
				START_WIDGET_GROUP("Player Betting Data")
					REPEAT NUM_NETWORK_PLAYERS i
						tlBetting = "GlobalServerBD_Betting.playerBettingData["
						tlBetting +=  i
						tlBetting += "]:"
						START_WIDGET_GROUP(tlBetting)
	//						ADD_WIDGET_BOOL("Betting Active", GlobalServerBD_Betting.playerBettingData[i].bBettingActive)
	//						ADD_WIDGET_BOOL("Seen player has bet", GlobalServerBD_Betting.playerBettingData[i].bPlayerHasBet)
							
							ADD_WIDGET_STRING("0 - POOL_IS_ACTIVE, 1 - PLAYER_HAS_BET, 2 - POOL_LOCKED")
							ADD_WIDGET_INT_READ_ONLY("Player state Bitset:", GlobalServerBD_Betting.playerBettingData[i].iBSPlayerState)
							ADD_WIDGET_INT_READ_ONLY("Player stat value:", GlobalServerBD_Betting.playerBettingData[i].iStatSum)
							ADD_WIDGET_INT_READ_ONLY("Player team:", GlobalServerBD_Betting.playerBettingData[i].iTeam)
							ADD_WIDGET_INT_READ_ONLY("Betting Sync ID:", GlobalServerBD_Betting.playerBettingData[i].iSyncDataID)
							ADD_WIDGET_INT_READ_ONLY("Players in Pool: ", GlobalServerBD_Betting.playerBettingData[i].iNumberPlayersInPool)
							ADD_WIDGET_INT_READ_ONLY("Betting ID: iMissionID:", GlobalServerBD_Betting.playerBettingData[i].bettingID.iMissionID)
							ADD_WIDGET_INT_READ_ONLY("Betting ID: iMissionVar:", GlobalServerBD_Betting.playerBettingData[i].bettingID.iMissionVar)
							ADD_WIDGET_INT_READ_ONLY("Betting ID: iMissionType:", GlobalServerBD_Betting.playerBettingData[i].bettingID.iMissionType)
						STOP_WIDGET_GROUP()
					ENDREPEAT
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Active Betting Data")
					
					REPEAT NUM_NETWORK_PLAYERS i
						tlBetting = "GlobalServerBD_Betting.activeBettingData["
						tlBetting +=  i
						tlBetting += "]:"
						START_WIDGET_GROUP(tlBetting)
							ADD_WIDGET_INT_READ_ONLY("iActiveMembers: ", GlobalServerBD_Betting.activeBettingData[i].iActiveMembers)
							ADD_WIDGET_BOOL("Team Betting? ", GlobalServerBD_Betting.activeBettingData[i].bTeamBetting)
							ADD_WIDGET_INT_READ_ONLY("Total cash bet on betting slot:", GlobalServerBD_Betting.activeBettingData[i].iCashBetOnPlayer)
							ADD_WIDGET_INT_READ_ONLY("Odd (Numerator):", GlobalServerBD_Betting.activeBettingData[i].iOddNumerator)
							ADD_WIDGET_INT_READ_ONLY("Odd (Denominator):", GlobalServerBD_Betting.activeBettingData[i].iOddDenominator)
							ADD_WIDGET_INT_READ_ONLY("Stat for betting slot: ", GlobalServerBD_Betting.activeBettingData[i].iStatSum)
							
						STOP_WIDGET_GROUP()
					ENDREPEAT
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
					
//			INT j
//			START_WIDGET_GROUP("Player Broadcast Data")
//				TEXT_LABEL_63 tlBettingPl
//				TEXT_LABEL_63 tlBettingPl2
//				REPEAT NUM_NETWORK_PLAYERS i
//					tlBettingPl = "GlobalPlayerBD["
//					tlBettingPl +=  i
//					tlBettingPl += "].clientBetData.bIHaveBet:"
//					START_WIDGET_GROUP(tlBettingPl)
//						START_WIDGET_GROUP("Bets on player")
//							REPEAT NUM_NETWORK_PLAYERS j
//								tlBettingPl2 = "Bets on player "
//								tlBettingPl2 += j
//								
//								ADD_WIDGET_INT_READ_ONLY(tlBettingPl2, GlobalplayerBD[i].clientBetData.betsOnPlayers[j].iPlacedBet)
//								
//								tlBettingPl2 = "Winnings on player "
//								tlBettingPl2 += j
//								ADD_WIDGET_INT_READ_ONLY(tlBettingPl2, GlobalplayerBD[i].clientBetData.betsOnPlayers[j].iWinnings)
//							ENDREPEAT
//						STOP_WIDGET_GROUP()
//						
//					STOP_WIDGET_GROUP()
//				ENDREPEAT
//			STOP_WIDGET_GROUP()
			
//			START_WIDGET_GROUP("Debug Screen Positioning")
//				ADD_WIDGET_FLOAT_SLIDER("X pos:", g_BettingDebugBGTopX, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Y pos:", g_BettingDebugBGTopY, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Width:", g_BettingDebugBGW, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Height:", g_BettingDebugBGH, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Title X:", g_BettingDebugTitleX, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Title Y:", g_BettingDebugTitleY, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Name X:", g_BettingDebugNameX, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Name Y:", g_BettingDebugNameY, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Betting ID X:", g_BettingDebugIDX, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Player Stats X:", g_BettingDebugStatX, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Player ML X:", g_BettingDebugMLX, 0, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Player Odds X:", g_BettingDebugOddX, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Cash Bet X:", g_BettingDebugCashBetOnX, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Profit:", g_BettingDebugProfitLossX, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Row Start Y:", g_BettingDebugRowStartY, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Row Start Y Space:", g_BettingDebugRowSpaceY, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Betting pool Y:", g_BettingDebugPoolY, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Betting pool Columns Y:", g_BettingDebugPoolColY, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Betting pool Players X:", g_BettingDebugPlayersX, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Betting pool cash total X:", g_BettingDebugTotalCashX, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("Betting pool startY:", g_BettingDebugRowPoolStartY, 0, 1.0, 0.001)
//			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(wgID)

ENDPROC

PROC RESET_BETTING_DEBUG_GLOBALS()
	g_bDebugSendAvailForBetting = FALSE
	g_bDebugSendLeaveBetting = FALSE
	//g_iDebugBettingUniqueID = 0
ENDPROC

PROC BROADCAST_TOGGLE_BETTING()
	EVENT_STRUCT_DEBUG_TOGGLE_BETTING Event
	Event.Details.Type = SCRIPT_EVENT_TOGGLE_BETTING
	Event.Details.FromPlayerIndex = PLAYER_ID()

	INT iSendTo = ALL_PLAYERS()
	IF NOT (iSendTo = 0)
	
		PRINTLN("[JA@BETTING] BROADCAST_TOGGLE_BETTING called...")
	
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
	ELSE
		PRINTLN("BROADCAST_TOGGLE_BETTING - playerflags = 0 so not broadcasting")
	ENDIF
ENDPROC

PROC MAINTAIN_BETTING_WIDGETS()

	IF g_bDebugSendAvailForBetting
		PRINTLN("[JA@BETTING] MAINTAIN_BETTING_WIDGETS - debug send request to join betting")
		
		BETTING_ID newBettingID
		newBettingID.iMissionID = g_iDebugBettingUniqueID
		newBettingID.iMissionVar = g_iDebugBettingUniqueID
		newBettingID.iMissionType = g_iDebugBettingUniqueID
		
		BROADCAST_PLAYER_AVAILABLE_FOR_BETTING(newBettingID)
		
		RESET_BETTING_DEBUG_GLOBALS()
	ENDIF

	IF g_bDebugSendLeaveBetting
		PRINTLN("[JA@BETTING] MAINTAIN_BETTING_WIDGETS - debug send request to leave betting")
		
		BROADCAST_PLAYER_LEFT_BETTING()
		
		RESET_BETTING_DEBUG_GLOBALS()
	ENDIF
	
	IF g_bDebugSendMissionLaunched
		PRINTLN("[JA@BETTING] MAINTAIN_BETTING_WIDGETS - debug send mission has started")
		g_bDebugSendMissionLaunched = FALSE
		
		BROADCAST_BETTING_MISSION_STARTED()
	ENDIF
	
	IF g_bDebugSendMissionFinished
		PRINTLN("[JA@BETTING] MAINTAIN_BETTING_WIDGETS - debug send mission finished")
		g_bDebugSendMissionFinished = FALSE
		
		BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
	ENDIF
	
	IF g_bDebugEnableBetting
		
		g_bDebugEnableBetting = FALSE
		
		BROADCAST_TOGGLE_BETTING()
	ENDIF
		
	MAINTAIN_BETTING_DEBUG_SCREEN()	
	
ENDPROC

#ENDIF

// ----------------- FREEMODE SPECIFIC FUNCTIONS -----------------

FUNC BOOL IS_BETTING_ALLOWED_FOR_TYPE(INT iMissionType)
	IF iMissionType = FMMC_TYPE_RACE
	OR iMissionType = FMMC_TYPE_BASE_JUMP
	OR iMissionType = FMMC_TYPE_MG_DARTS
	OR iMissionType = FMMC_TYPE_MG_TENNIS
	OR iMissionType = FMMC_TYPE_MG_GOLF
	OR iMissionType = FMMC_TYPE_MG_SHOOTING_RANGE
	OR iMissionType = FMMC_TYPE_RACE_TO_POINT
	OR iMissionType	= FMMC_TYPE_MISSION // Only LTS and CTF missions allow betting
		RETURN TRUE
	ENDIF
	
	IF iMissionType = FMMC_TYPE_DEATHMATCH
		IF NOT IS_THIS_A_KING_OF_THE_HILL_CORONA(FMMC_TYPE_DEATHMATCH, g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_TYPE])
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Freemode specific function to make players available
PROC SET_PLAYER_AS_AVAILABLE_FOR_FM_BETTING(INT iMissionToStart, INT iMissionVariation, INT iMissionType, BOOL bTeamBetting = FALSE, BOOL bMissionRunning = FALSE, STRING stUserName = NULL, STRING stFileName = NULL)
	#IF IS_DEBUG_BUILD
	IF NOT IS_BETTING_DEBUG_CONTROL_ON()
	#ENDIF							

	IF bMissionRunning
		PRINTLN("[JA@BETTING] SET_PLAYER_AS_AVAILABLE_FOR_FM_BETTING - mission is running so don't broadcast availablility")
		EXIT
	ENDIF
	
	// Do not allow us to join betting if the tuneable is enabled
	IF g_sMPTunables.bDisableJobBetting
		PRINTLN("[JA@BETTING] SET_PLAYER_AS_AVAILABLE_FOR_FM_BETTING - g_sMPTunables.bDisableJobBetting = TRUE")
		EXIT
	ENDIF

	IF IS_BETTING_ALLOWED_FOR_TYPE(iMissionType)
	
		BETTING_ID bettingID
		bettingID.iMissionID = iMissionToStart
		bettingID.iMissionVar = iMissionVariation
		bettingID.iMissionType = iMissionType
		
		PRINTLN("[JA@BETTING] SET_PLAYER_AS_AVAILABLE_FOR_FM_BETTING - Mission Type: ", iMissionType, " and Mission Var: ", iMissionVariation)
			
		BROADCAST_PLAYER_AVAILABLE_FOR_BETTING(bettingID, bTeamBetting, stUserName, stFileName)
	ELSE
		PRINTLN("[JA@BETTING] SET_PLAYER_AS_AVAILABLE_FOR_FM_BETTING - mission type is not a valid type for betting. Not initialising betting")
	ENDIF
		
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
ENDPROC

/// PURPOSE: Freemode specific function to mark mission as started
PROC SET_FM_MISSION_AS_STARTED_FOR_BETTING()
	#IF IS_DEBUG_BUILD
	IF NOT IS_BETTING_DEBUG_CONTROL_ON()
	#ENDIF
		
	BROADCAST_BETTING_MISSION_STARTED()
		
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
ENDPROC

