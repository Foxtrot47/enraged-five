// ====================================================================================
// ====================================================================================
//
// Name:        net_mission_orbital_cannon.sch
// Description: Header for Orbital Cannon functionality for missions
// Written By:  Alex Murphy
//
// ====================================================================================
// ====================================================================================

USING "net_orbital_cannon.sch"

STRUCT MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT
	BOOL bFired = FALSE
	BOOL bFiring = FALSE
	BOOL bActivated = FALSE
	BOOL bAudioBankLoaded = FALSE
	
	CAMERA_INDEX camOrbitalCannon
	
	INT iBitset = 0
	INT iSoundID = -1
	INT iZoomLevel = 0
	INT iControlBS = 0
	INT iScaleformBS = 0
	INT iSwitchBitset = 0
	INT iCamSwitchTimer = 0
	INT iSoundPanSoundID = -1
	INT iChargingSoundID = -1
	INT iBackgroundSoundID = -1
	INT iSoundZoomInSoundID = -1
	INT iSoundZoomOutSoundID = -1
	INT iMissionOrbitalCannonID = 0
	INT iOrbitalCannonContextIntention = NEW_CONTEXT_INTENTION
	
	ORBITAL_CANNON_CLIENT_STAGE eStage = ORBITAL_CANNON_CLIENT_STAGE_INIT
	
	ORBITAL_CANNON_SWITCH_STATE eSwitchState = ORBITAL_CANNON_SWITCH_STATE_USING_CANNON
	
	SCALEFORM_INDEX sfButton
	SCALEFORM_INDEX sfOrbitalCannon
	
	SCALEFORM_INSTRUCTIONAL_BUTTONS scaleformInstructionalButtons
	
	SCRIPT_TIMER sFiringTimer
	SCRIPT_TIMER sSwitchDelay
	SCRIPT_TIMER sInitialDelay
	SCRIPT_TIMER sCooldownTimer
	SCRIPT_TIMER sAudioBankTimer
	
	VECTOR vLoadSceneRot
	VECTOR vLoadSceneLocation
ENDSTRUCT

PROC SET_MISSION_ORBITAL_CANNON_SWITCH_STATE(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE eState)
	PRINTLN("[AM_MP_ORBITAL_CANNON] SET_MISSION_ORBITAL_CANNON_SWITCH_STATE - ", ENUM_TO_INT(eState))
	
	OrbitalCannonLocal.eSwitchState = eState
ENDPROC

FUNC ORBITAL_CANNON_SWITCH_STATE GET_MISSION_ORBITAL_CANNON_SWITCH_STATE(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	RETURN OrbitalCannonLocal.eSwitchState
ENDFUNC

PROC SET_MISSION_ORBITAL_CANNON_STAGE(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, ORBITAL_CANNON_CLIENT_STAGE eStage)
	PRINTLN("[AM_MP_ORBITAL_CANNON] SET_MISSION_ORBITAL_CANNON_STAGE - ", ENUM_TO_INT(eStage))
	
	OrbitalCannonLocal.eStage = eStage
ENDPROC

PROC START_MISSION_ORBITAL_CANNON_SOUND(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iSoundID = -1
		OrbitalCannonLocal.iSoundID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(OrbitalCannonLocal.iSoundID, "cannon_charge_fire_loop", "dlc_xm_orbital_cannon_sounds", TRUE)
	ENDIF
ENDPROC

PROC STOP_MISSION_ORBITAL_CANNON_SOUND(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iSoundID != -1
		STOP_SOUND(OrbitalCannonLocal.iSoundID)
		RELEASE_SOUND_ID(OrbitalCannonLocal.iSoundID)
		OrbitalCannonLocal.iSoundID = -1
	ENDIF
ENDPROC

PROC START_MISSION_ORBITAL_CANNON_BACKGROUND_SOUND(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iBackgroundSoundID = -1
		OrbitalCannonLocal.iBackgroundSoundID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(OrbitalCannonLocal.iBackgroundSoundID, "background_loop", "dlc_xm_orbital_cannon_sounds", TRUE)
	ENDIF
ENDPROC

PROC STOP_MISSION_ORBITAL_CANNON_BACKGROUND_SOUND(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iBackgroundSoundID != -1
		STOP_SOUND(OrbitalCannonLocal.iBackgroundSoundID)
		RELEASE_SOUND_ID(OrbitalCannonLocal.iBackgroundSoundID)
		OrbitalCannonLocal.iBackgroundSoundID = -1
	ENDIF
ENDPROC

PROC START_MISSION_ORBITAL_CANNON_CHARGING_SOUND(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iChargingSoundID = -1
		OrbitalCannonLocal.iChargingSoundID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(OrbitalCannonLocal.iChargingSoundID, "cannon_activating_loop", "dlc_xm_orbital_cannon_sounds", TRUE)
	ENDIF
ENDPROC

PROC STOP_MISSION_ORBITAL_CANNON_CHARGING_SOUND(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iChargingSoundID != -1
		STOP_SOUND(OrbitalCannonLocal.iChargingSoundID)
		RELEASE_SOUND_ID(OrbitalCannonLocal.iChargingSoundID)
		OrbitalCannonLocal.iChargingSoundID = -1
	ENDIF
ENDPROC

PROC START_MISSION_ORBITAL_CANNON_PAN_SOUND(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iSoundPanSoundID = -1
		OrbitalCannonLocal.iSoundPanSoundID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(OrbitalCannonLocal.iSoundPanSoundID, "pan_loop", "dlc_xm_orbital_cannon_sounds", TRUE)
	ENDIF
ENDPROC

PROC STOP_MISSION_ORBITAL_CANNON_PAN_SOUND(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iSoundPanSoundID != -1
		STOP_SOUND(OrbitalCannonLocal.iSoundPanSoundID)
		RELEASE_SOUND_ID(OrbitalCannonLocal.iSoundPanSoundID)
		OrbitalCannonLocal.iSoundPanSoundID = -1
	ENDIF
ENDPROC

PROC START_MISSION_ORBITAL_CANNON_ZOOM_IN_SOUND(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iSoundZoomInSoundID = -1
		OrbitalCannonLocal.iSoundZoomInSoundID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(OrbitalCannonLocal.iSoundZoomInSoundID, "zoom_out_loop", "dlc_xm_orbital_cannon_sounds", TRUE)
	ENDIF
ENDPROC

PROC STOP_MISSION_ORBITAL_CANNON_ZOOM_IN_SOUND(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iSoundZoomInSoundID != -1
		STOP_SOUND(OrbitalCannonLocal.iSoundZoomInSoundID)
		RELEASE_SOUND_ID(OrbitalCannonLocal.iSoundZoomInSoundID)
		OrbitalCannonLocal.iSoundZoomInSoundID = -1
	ENDIF
ENDPROC

PROC START_MISSION_ORBITAL_CANNON_ZOOM_OUT_SOUND(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iSoundZoomOutSoundID = -1
		OrbitalCannonLocal.iSoundZoomOutSoundID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(OrbitalCannonLocal.iSoundZoomOutSoundID, "zoom_out_loop", "dlc_xm_orbital_cannon_sounds", TRUE)
	ENDIF
ENDPROC

PROC STOP_MISSION_ORBITAL_CANNON_ZOOM_OUT_SOUND(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iSoundZoomOutSoundID != -1
		STOP_SOUND(OrbitalCannonLocal.iSoundZoomOutSoundID)
		RELEASE_SOUND_ID(OrbitalCannonLocal.iSoundZoomOutSoundID)
		OrbitalCannonLocal.iSoundZoomOutSoundID = -1
	ENDIF
ENDPROC


PROC TIDYUP_MISSION_ORBITAL_CANNON(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, BOOL bReturnControl)
	PRINTLN("[AM_MP_ORBITAL_CANNON] TIDYUP_MISSION_ORBITAL_CANNON")
	
	CLEAR_BIT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CANNON)
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("helicopterhud")
	
	STOP_MISSION_ORBITAL_CANNON_SOUND(OrbitalCannonLocal)
	STOP_MISSION_ORBITAL_CANNON_BACKGROUND_SOUND(OrbitalCannonLocal)
	STOP_MISSION_ORBITAL_CANNON_CHARGING_SOUND(OrbitalCannonLocal)
	STOP_MISSION_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
	STOP_MISSION_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
	STOP_MISSION_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
	
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_ION_CANNON")
	
	STOP_AUDIO_SCENE("dlc_xm_orbital_cannon_camera_active_scene")
	
	RESET_NET_TIMER(OrbitalCannonLocal.sAudioBankTimer)
	
	OrbitalCannonLocal.bActivated = FALSE
	OrbitalCannonLocal.bAudioBankLoaded = FALSE
	
	IF ANIMPOSTFX_IS_RUNNING("MP_OrbitalCannon")
		ANIMPOSTFX_STOP("MP_OrbitalCannon")
	ENDIF
	
	OrbitalCannonLocal.iScaleformBS = 0
	
	IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FOCUS_SET)
		CLEAR_FOCUS()
		NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
		
		CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FOCUS_SET)
	ENDIF
	
	CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_WARNING)
	CLEAR_BIT(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_GO_TO_TASK)
	CLEAR_BIT(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_ANIM_RUNNING)
	
	RESET_NET_TIMER(OrbitalCannonLocal.sSwitchDelay)
	
	ENABLE_WEAPON_SWAP()
	Enable_MP_Comms()
	
	IF IS_BIT_SET(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_SHADOWS)
		CASCADE_SHADOWS_SET_AIRCRAFT_MODE(FALSE)
	ENDIF
	
	IF IS_BIT_SET(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_WEATHER)
		CLEAR_OVERRIDE_WEATHER()
	ENDIF
	
	DISPLAY_CASH(TRUE)
	
	IF DOES_CAM_EXIST(OrbitalCannonLocal.camOrbitalCannon)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDIF
	
	IF DOES_CAM_EXIST(OrbitalCannonLocal.camOrbitalCannon)
		DESTROY_CAM(OrbitalCannonLocal.camOrbitalCannon)
	ENDIF
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(OrbitalCannonLocal.sfOrbitalCannon)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, FALSE)
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
	OR IS_SCREEN_FADING_OUT()
		PRINTLN("[AM_MP_ORBITAL_CANNON] TIDYUP_MISSION_ORBITAL_CANNON - FADING SCREEN IN")
		DO_SCREEN_FADE_IN(1000)
	ENDIF
	
	CLEAR_FOCUS()
	
	g_iOrbitalCannonObjectInUse = -1
	
	IF bReturnControl
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND IS_BIT_SET(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_CONTROLS)
			PRINTLN("[AM_MP_ORBITAL_CANNON] TIDYUP_MISSION_ORBITAL_CANNON - Returning player control")
			
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, NSPC_PREVENT_VISIBILITY_CHANGES)
		ENDIF
	ENDIF
	
	OrbitalCannonLocal.iControlBS = 0
ENDPROC

PROC CLEANUP_MISSION_ORBITAL_CANNON(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, BOOl bReturnControl)
	PRINTLN("[AM_MP_ORBITAL_CANNON] CLEANUP_MISSION_ORBITAL_CANNON")
	
	TIDYUP_MISSION_ORBITAL_CANNON(OrbitalCannonLocal, bReturnControl)
	
	IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_SUICIDE)
		PRINTLN("[AM_MP_ORBITAL_CANNON] CLEANUP_MISSION_ORBITAL_CANNON - Enabling kill yourself option")
		
		ENABLE_KILL_YOURSELF_OPTION()
		
		CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_SUICIDE)
	ENDIF
	
	IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_INTERACTION)
		PRINTLN("[AM_MP_ORBITAL_CANNON] CLEANUP_MISSION_ORBITAL_CANNON - Enabling interaction menu")
		
		ENABLE_INTERACTION_MENU()
		
		CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_INTERACTION)
	ENDIF
	
	IF OrbitalCannonLocal.iOrbitalCannonContextIntention != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(OrbitalCannonLocal.iOrbitalCannonContextIntention)
	ENDIF
	
	SET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_USING_CANNON)
	
	OrbitalCannonLocal.iBitset = 0
	OrbitalCannonLocal.iSwitchBitset = 0
	
	SET_MISSION_ORBITAL_CANNON_STAGE(OrbitalCannonLocal, ORBITAL_CANNON_CLIENT_STAGE_INIT)
ENDPROC

FUNC BOOL CLIENT_CLEANUP_MISSION_ORBITAL_CANNON_CAMERA(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	PRINTLN("[ORBITAL_CANNON] CLIENT_CLEANUP_ORBITAL_CANNON_CAMERA - Cleaning up Orbital Cannon camera")
	
	IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FOCUS_SET)
		CLEAR_FOCUS()
		NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
		
		CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FOCUS_SET)
	ENDIF
	
	SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
			
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	RETURN TRUE
ENDFUNC

FUNC ORBITAL_CANNON_CAMERA_DETAILS GET_MISSION_ORBITAL_CANNON_CAMERA_DETAILS()
	ORBITAL_CANNON_CAMERA_DETAILS tempCamDetails
	
	tempCamDetails.vPos = <<-8.8511, 6835.0034, 400.0>>
	tempCamDetails.vRot = <<-90.0, 0.0, 0.0>>
	tempCamDetails.fFOV = 100.0
	
	IF g_iOrbitalCannonObjectInUse > -1
		tempCamDetails.vPos.x = g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_iOrbitalCannonObjectInUse].vPos.x
		tempCamDetails.vPos.y =	g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_iOrbitalCannonObjectInUse].vPos.y
	ENDIF
	
	RETURN tempCamDetails
ENDFUNC

PROC CLEANUP_FIRING_MISSION_ORBITAL_CANNON(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	OrbitalCannonLocal.bFiring = FALSE
	
	IF IS_CAM_SHAKING(OrbitalCannonLocal.camOrbitalCannon)
		STOP_CAM_SHAKING(OrbitalCannonLocal.camOrbitalCannon)
	ENDIF
	
	STOP_CONTROL_SHAKE(PLAYER_CONTROL)
	
	CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS)
	CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_2)
	CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_1)
	
	RESET_NET_TIMER(OrbitalCannonLocal.sFiringTimer)
ENDPROC

FUNC FLOAT GET_MISSION_ZOOM_LEVEL_FLOAT(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, BOOL bDecrease = FALSE)
	IF NOT bDecrease
		SWITCH OrbitalCannonLocal.iZoomLevel
			CASE 0	RETURN 0.25	BREAK
			CASE 1	RETURN 0.5	BREAK
			CASE 2	RETURN 0.75	BREAK
			CASE 3	RETURN 1.0	BREAK
		ENDSWITCH
	ELSE
		SWITCH OrbitalCannonLocal.iZoomLevel
			CASE 1	RETURN 0.0	BREAK
			CASE 2	RETURN 0.25	BREAK
			CASE 3	RETURN 0.5	BREAK
			CASE 4	RETURN 0.75	BREAK
		ENDSWITCH
	ENDIF
	
	RETURN 0.0
ENDFUNC

PROC MAINTAIN_MISSION_CAMERA_ZOOM(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, FLOAT &fNewFOV)
	FLOAT fStep = 125.0
	
	IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
		fNewFOV = fNewFOV +@ fStep
		
		SWITCH OrbitalCannonLocal.iZoomLevel
			CASE 1
				IF fNewFOV > 100.0
					fNewFOV = 100.0
					OrbitalCannonLocal.iZoomLevel = 0
					
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
				ENDIF
			BREAK
			
			CASE 2
				IF fNewFOV > 80.0
					fNewFOV = 80.0
					OrbitalCannonLocal.iZoomLevel = 1
					
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
				ENDIF
			BREAK
			
			CASE 3
				IF fNewFOV > 60.0
					fNewFOV = 60.0
					OrbitalCannonLocal.iZoomLevel = 2
					
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
				ENDIF
			BREAK
			
			CASE 4
				IF fNewFOV > 40.0
					fNewFOV = 40.0
					OrbitalCannonLocal.iZoomLevel = 3
					
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
				ENDIF
			BREAK
		ENDSWITCH
	ELIF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
		fNewFOV = fNewFOV -@ fStep
		
		SWITCH OrbitalCannonLocal.iZoomLevel
			CASE 0
				IF fNewFOV < 80.0
					fNewFOV = 80.0
					OrbitalCannonLocal.iZoomLevel = 1
					
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
				ENDIF
			BREAK
			
			CASE 1
				IF fNewFOV < 60.0
					fNewFOV = 60.0
					OrbitalCannonLocal.iZoomLevel = 2
					
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
				ENDIF
			BREAK
			
			CASE 2
				IF fNewFOV < 40.0
					fNewFOV = 40.0
					OrbitalCannonLocal.iZoomLevel = 3
					
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
				ENDIF
			BREAK
			
			CASE 3
				IF fNewFOV < 20.0
					fNewFOV = 20.0
					OrbitalCannonLocal.iZoomLevel = 4
					
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC MAINTAIN_MISSION_CAMERA_MOVEMENT(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	FLOAT fRightY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
	
	IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
		IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			IF fRightY = 0.0
				CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
			ENDIF
		ELSE
			IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
			AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
				CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_CAM_EXIST(OrbitalCannonLocal.camOrbitalCannon)
		BOOL bUpdate = FALSE
		BOOL bUpdateZoom = FALSE
		
		FLOAT fNewFOV = GET_CAM_FOV(OrbitalCannonLocal.camOrbitalCannon)
		FLOAT fStep = 35.0 + GET_MOVEMENT_SPEED(OrbitalCannonLocal.iZoomLevel)
		FLOAT fLeftX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
		FLOAT fLeftY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
		
		VECTOR vNewCamCoords = GET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon)
		
		IF OrbitalCannonLocal.bFiring
			fStep = 50.0
		ENDIF
		
		IF fLeftX > 0.1
			IF (vNewCamCoords.x +@ ABSF(fStep * fLeftX)) <= 4000.0
				bUpdate = TRUE
				
				vNewCamCoords.x = vNewCamCoords.x +@ ABSF(fStep * fLeftX)
			ENDIF
		ELIF fLeftX < -0.1
			IF (vNewCamCoords.x -@ ABSF(fStep * fLeftX)) >= -4000.0
				bUpdate = TRUE
				
				vNewCamCoords.x = vNewCamCoords.x -@ ABSF(fStep * fLeftX)
			ENDIF
		ENDIF
		
		IF fLeftY > 0.1
			IF (vNewCamCoords.y -@ ABSF(fStep * fLeftY)) >= -4000.0
				bUpdate = TRUE
				
				vNewCamCoords.y = vNewCamCoords.y -@ ABSF(fStep * fLeftY)
			ENDIF
		ELIF fLeftY < -0.1
			IF (vNewCamCoords.y +@ ABSF(fStep * fLeftY)) <= 8000.0
				bUpdate = TRUE
				
				vNewCamCoords.y = vNewCamCoords.y +@ ABSF(fStep * fLeftY)
			ENDIF
		ENDIF
		
		FLOAT fZPos = GET_Z_POS(vNewCamCoords)
		
		IF vNewCamCoords.z != fZPos
			IF vNewCamCoords.z < fZPos
				vNewCamCoords.z = vNewCamCoords.z +@ ABSF(fStep)
				
				IF vNewCamCoords.z > fZPos
					vNewCamCoords.z = fZPos
				ENDIF
			ELIF vNewCamCoords.z > fZPos
				vNewCamCoords.z = vNewCamCoords.z -@ ABSF(fStep)
				
				IF vNewCamCoords.z < fZPos
					vNewCamCoords.z = fZPos
				ENDIF
			ENDIF
			
			bUpdate = TRUE
		ENDIF
		
		IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
			IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				IF fRightY > 0.3
					IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
					AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
					AND OrbitalCannonLocal.iZoomLevel > 0
						BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_ZOOM_LEVEL")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_MISSION_ZOOM_LEVEL_FLOAT(OrbitalCannonLocal, TRUE))
						END_SCALEFORM_MOVIE_METHOD()
						
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
					ENDIF
				ELIF fRightY < -0.3
					IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
					AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
					AND OrbitalCannonLocal.iZoomLevel < 4
						BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_ZOOM_LEVEL")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_MISSION_ZOOM_LEVEL_FLOAT(OrbitalCannonLocal, FALSE))
						END_SCALEFORM_MOVIE_METHOD()
						
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
					ENDIF
				ENDIF
			ELSE
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
					IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
					AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
					AND OrbitalCannonLocal.iZoomLevel > 0
						BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_ZOOM_LEVEL")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_MISSION_ZOOM_LEVEL_FLOAT(OrbitalCannonLocal, TRUE))
						END_SCALEFORM_MOVIE_METHOD()
						
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
					ENDIF
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
					IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
					AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
					AND OrbitalCannonLocal.iZoomLevel < 4
						BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_ZOOM_LEVEL")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_MISSION_ZOOM_LEVEL_FLOAT(OrbitalCannonLocal, FALSE))
						END_SCALEFORM_MOVIE_METHOD()
						
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
		OR IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
			bUpdateZoom = TRUE
		ENDIF
		
		FLOAT fXIncrease = 0.0
		FLOAT fYIncrease = 0.0
		
		VECTOR vTemp = GET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon)
		
		IF vTemp.x < vNewCamCoords.x
			fXIncrease = 50.0
		ELIF vTemp.x > vNewCamCoords.x
			fXIncrease = -50.0
		ENDIF
		
		IF vTemp.y < vNewCamCoords.y
			fYIncrease = 50.0
		ELIF vTemp.y > vNewCamCoords.y
			fYIncrease = -50.0
		ENDIF
		
		IF bUpdate
		OR bUpdateZoom
			IF GET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal) = ORBITAL_CANNON_SWITCH_STATE_USING_CANNON
				IF bUpdate
					START_MISSION_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
				ELSE
					STOP_MISSION_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
					STOP_MISSION_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
					STOP_MISSION_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
				ENDIF
				
				IF bUpdateZoom
					IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
						START_MISSION_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
					ELIF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
						START_MISSION_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
					ENDIF
				ELSE
					STOP_MISSION_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
					STOP_MISSION_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
				ENDIF
			ENDIF
			
			MAINTAIN_MISSION_CAMERA_ZOOM(OrbitalCannonLocal, fNewFOV)
			
			SET_CAM_FOV(OrbitalCannonLocal.camOrbitalCannon, fNewFOV)
			
			SET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon, vNewCamCoords)
		ELSE
			STOP_MISSION_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
			STOP_MISSION_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
			STOP_MISSION_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
		ENDIF
		
		FLOAT fTempZPos
		GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(vNewCamCoords, fTempZPos, TRUE)
		
		SET_FOCUS_POS_AND_VEL(<<vNewCamCoords.x + fXIncrease, vNewCamCoords.y + fYIncrease, fTempZPos + 50.0>>, <<-90.0, 0.0, 0.0>>)
	ENDIF
ENDPROC

FUNC BOOL CLIENT_MAINTAIN_MISSION_ORBITAL_CANNON_INPUT(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sCooldownTimer)
	AND HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sCooldownTimer, 5000)
		CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS)
		CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_2)
		CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_1)
		
		RESET_NET_TIMER(OrbitalCannonLocal.sFiringTimer)
		RESET_NET_TIMER(OrbitalCannonLocal.sCooldownTimer)
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sInitialDelay)
		IF HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sInitialDelay, 3000)
			STOP_MISSION_ORBITAL_CANNON_CHARGING_SOUND(OrbitalCannonLocal)
			
			PLAY_SOUND_FRONTEND(-1, "cannon_active", "dlc_xm_orbital_cannon_sounds", TRUE)
			
			RESET_NET_TIMER(OrbitalCannonLocal.sInitialDelay)
			
			SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
		ENDIF
	ENDIF
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_PHONE_ONSCREEN()
	AND NOT IS_BROWSER_OPEN()
	AND NOT IS_TRANSITION_ACTIVE()
	AND GET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal) = ORBITAL_CANNON_SWITCH_STATE_USING_CANNON
		ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_ATTACK)
		AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS)
		AND NOT HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sInitialDelay)
		AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_WARNING)
		AND NOT IS_WARNING_MESSAGE_ACTIVE()
		AND DOES_CAM_EXIST(OrbitalCannonLocal.camOrbitalCannon)
			OrbitalCannonLocal.bFiring = TRUE
			
			IF NOT HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sFiringTimer)
				START_NET_TIMER(OrbitalCannonLocal.sFiringTimer)
				
				START_MISSION_ORBITAL_CANNON_SOUND(OrbitalCannonLocal)
				
				SET_CONTROL_SHAKE(PLAYER_CONTROL, 1000, 50)
				
				IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SOUND_VARIABLE)
					SET_VARIABLE_ON_SOUND(OrbitalCannonLocal.iSoundID, "Firing", 0)
					
					SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SOUND_VARIABLE)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sFiringTimer, 3000)
					IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS)
						VECTOR vCamPos = GET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon)
						
						PRINTLN("[AM_MP_ORBITAL_CANNON] CLIENT_MAINTAIN_MISSION_ORBITAL_CANNON_INPUT - Firing Orbital Cannon")
						
						SET_VARIABLE_ON_SOUND(OrbitalCannonLocal.iSoundID, "Firing", 1)
						
						FIRE_ORBITAL_CANNON(vCamPos)
						
						SET_CONTROL_SHAKE(PLAYER_CONTROL, 500, 256)
						
						SHAKE_CAM(OrbitalCannonLocal.camOrbitalCannon, "GAMEPLAY_EXPLOSION_SHAKE", 1.5)
						
						START_NET_TIMER(OrbitalCannonLocal.sCooldownTimer)
						
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS)
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
						
						OrbitalCannonLocal.bFired = TRUE
					ENDIF
				ELIF HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sFiringTimer, 2000)
					IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_2)
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_2)
						
						SET_CONTROL_SHAKE(PLAYER_CONTROL, 1000, 50)
					ENDIF
				ELIF HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sFiringTimer, 1000)
					IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_1)
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_1)
						
						SET_CONTROL_SHAKE(PLAYER_CONTROL, 1000, 50)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			BOOL bSkip = FALSE
			
			IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_ATTACK)
			AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_WARNING)
			AND NOT IS_WARNING_MESSAGE_ACTIVE()
				PLAY_SOUND_FRONTEND(-1, "inactive_fire_fail",  "dlc_xm_orbital_cannon_sounds")
			ENDIF
			
			OrbitalCannonLocal.bFiring = FALSE
			
			STOP_MISSION_ORBITAL_CANNON_SOUND(OrbitalCannonLocal)
			
			IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SOUND_VARIABLE)
				CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SOUND_VARIABLE)
			ENDIF
			
			IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS)
				CLEANUP_FIRING_MISSION_ORBITAL_CANNON(OrbitalCannonLocal)
			ENDIF
			
			IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_WARNING)
			AND NOT bSkip
				STOP_MISSION_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
				STOP_MISSION_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
				STOP_MISSION_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
				
				CLEANUP_FIRING_MISSION_ORBITAL_CANNON(OrbitalCannonLocal)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				PRINTLN("[JR]-Exiting Orbital Cannon, clearing bits")
				SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMissionModeBit, ciMISSION_USING_ORBITAL_CANNON)
				RETURN TRUE
			ENDIF
			
			IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
				CLEANUP_FIRING_MISSION_ORBITAL_CANNON(OrbitalCannonLocal)
				
				OrbitalCannonLocal.iCamSwitchTimer = 0
				
				SET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_FADE_OUT)
				
				CLEAR_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_ACTIVATING)
				CLEAR_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
				
				SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
				
				SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_ACTIVATING)
				
				SET_MISSION_ORBITAL_CANNON_STAGE(OrbitalCannonLocal, ORBITAL_CANNON_CLIENT_STAGE_ACTIVATED)
			ENDIF
		ENDIF
		
		IF NOT IS_CAM_SHAKING(OrbitalCannonLocal.camOrbitalCannon)
			MAINTAIN_MISSION_CAMERA_MOVEMENT(OrbitalCannonLocal)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CLIENT_MAINTAIN_ACTIVATED_MISSION_ORBITAL_CANNON(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	BOOL bFinished = FALSE
	BOOL bReturnTrue = FALSE
	
	CONTROL_ACTION eZoomInControl = INPUT_SCRIPT_RIGHT_AXIS_Y
	CONTROL_ACTION eZoomOutControl = INPUT_SCRIPT_RIGHT_AXIS_Y
	
	ORBITAL_CANNON_CAMERA_DETAILS thisCamera
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		eZoomInControl = INPUT_CURSOR_SCROLL_UP
		eZoomOutControl = INPUT_CURSOR_SCROLL_DOWN
	ENDIF
	
	SWITCH GET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal)
		CASE ORBITAL_CANNON_SWITCH_STATE_FADE_OUT
			IF NOT IS_SCREEN_FADED_OUT()
				IF NOT IS_SCREEN_FADING_OUT()
					IF IS_BIT_SET(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_ACTIVATING)
						DO_SCREEN_FADE_OUT(500)
					ELSE
						DO_SCREEN_FADE_OUT(0)
					ENDIF
				ENDIF
			ELSE
				OrbitalCannonLocal.iCamSwitchTimer = GET_GAME_TIMER()
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
				
				SET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_WAIT_FOR_FADE_OUT)
			ENDIF
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_WAIT_FOR_FADE_OUT
			IF IS_SCREEN_FADED_OUT()
				IF IS_BIT_SET(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FORCE_SCALEFORM_DRAW)
				ENDIF
				
				STOP_MISSION_ORBITAL_CANNON_BACKGROUND_SOUND(OrbitalCannonLocal)
				
				SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SWITCHING_CAMERA)
				
				ENABLE_NIGHTVISION(VISUALAID_OFF)
				
				SET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_CLEANUP_CURRENT)
			ENDIF
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_CLEANUP_CURRENT
			IF NOT IS_BIT_SET(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
				ANIMPOSTFX_PLAY("MP_OrbitalCannon", 0, TRUE)
				
				OrbitalCannonLocal.iZoomLevel = 0
				
				CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
				CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
				CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
				
				CLEAR_HELP()
				SET_OVERRIDE_WEATHER("Clear")
				CLEAR_TIMECYCLE_MODIFIER()
				CASCADE_SHADOWS_SET_AIRCRAFT_MODE(TRUE)
				
				SET_BIT(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_WEATHER)
				SET_BIT(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_SHADOWS)
				
				START_MISSION_ORBITAL_CANNON_CHARGING_SOUND(OrbitalCannonLocal)
				
				START_NET_TIMER(OrbitalCannonLocal.sInitialDelay)
			ELSE
				ANIMPOSTFX_STOP("MP_OrbitalCannon")
				
				IF IS_BIT_SET(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_SHADOWS)
					CASCADE_SHADOWS_SET_AIRCRAFT_MODE(FALSE)
					CLEAR_BIT(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_SHADOWS)
				ENDIF
				
				CLEAR_HELP()
				
				IF IS_BIT_SET(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_WEATHER)
					CLEAR_OVERRIDE_WEATHER()
					CLEAR_BIT(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_WEATHER)
				ENDIF
			ENDIF
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			IF DOES_CAM_EXIST(OrbitalCannonLocal.camOrbitalCannon)
				DESTROY_CAM(OrbitalCannonLocal.camOrbitalCannon)
			ENDIF
			
			IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FOCUS_SET)
				NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
				
				CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FOCUS_SET)
			ENDIF
			
			SET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_SETUP_DESIRED)
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_SETUP_DESIRED
			IF IS_BIT_SET(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
				SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_REQUIRED)
				
				CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FORCE_SCALEFORM_DRAW)
				
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(OrbitalCannonLocal.sfOrbitalCannon)
				
				OrbitalCannonLocal.vLoadSceneLocation = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				OrbitalCannonLocal.vLoadSceneRot = GET_GAMEPLAY_CAM_ROT()
				bFinished = TRUE
			ELSE
				IF NOT OrbitalCannonLocal.bActivated
					START_AUDIO_SCENE("dlc_xm_orbital_cannon_camera_active_scene")
				ENDIF
				
				OrbitalCannonLocal.bActivated = TRUE
				OrbitalCannonLocal.sfOrbitalCannon = REQUEST_SCALEFORM_MOVIE("ORBITAL_CANNON_CAM")
				
				IF HAS_SCALEFORM_MOVIE_LOADED(OrbitalCannonLocal.sfOrbitalCannon)
					thisCamera = GET_MISSION_ORBITAL_CANNON_CAMERA_DETAILS()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_ZOOM_LEVEL")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0)
					END_SCALEFORM_MOVIE_METHOD()
					
					IF thisCamera.vPos.x <> 0.0
					AND thisCamera.vPos.y <> 0.0
					AND thisCamera.vPos.z <> 0.0
					AND thisCamera.fFOV <> 0.0
						IF DOES_CAM_EXIST(OrbitalCannonLocal.camOrbitalCannon)
							DESTROY_CAM(OrbitalCannonLocal.camOrbitalCannon)
						ENDIF
						
						SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_REQUIRED)
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FORCE_SCALEFORM_DRAW)
						
						OrbitalCannonLocal.camOrbitalCannon = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
						
						OrbitalCannonLocal.vLoadSceneLocation = thisCamera.vPos
						
						SET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon, thisCamera.vPos)
						SET_CAM_ROT(OrbitalCannonLocal.camOrbitalCannon, thisCamera.vRot)
						SET_CAM_FOV(OrbitalCannonLocal.camOrbitalCannon, thisCamera.fFOV)
						SET_CAM_ACTIVE(OrbitalCannonLocal.camOrbitalCannon, TRUE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
						bFinished = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF bFinished
				SET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_WAIT_FOR_FILTER_TIME)
			ENDIF
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_WAIT_FOR_FILTER_TIME
			IF GET_GAME_TIMER() - OrbitalCannonLocal.iCamSwitchTimer > SPEC_HUD_FILTER_CHANGE_TIME
				SET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_SETUP_LOAD_SCENE)
			ENDIF
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_SETUP_LOAD_SCENE
			IF IS_BIT_SET(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_RUNNING)
				NEW_LOAD_SCENE_STOP()
			ENDIF
			
			IF OrbitalCannonLocal.vLoadSceneRot.x = 0.0
			AND OrbitalCannonLocal.vLoadSceneRot.y = 0.0
			AND OrbitalCannonLocal.vLoadSceneRot.z = 0.0
				NEW_LOAD_SCENE_START_SPHERE(OrbitalCannonLocal.vLoadSceneLocation, 300.0)
			ELSE
				NEW_LOAD_SCENE_START(OrbitalCannonLocal.vLoadSceneLocation, OrbitalCannonLocal.vLoadSceneRot, 300)
			ENDIF
			
			SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_RUNNING)
			
			SET_FOCUS_POS_AND_VEL(OrbitalCannonLocal.vLoadSceneLocation, <<-90.0, 0.0, 0.0>>)
			
			NETWORK_SET_IN_FREE_CAM_MODE(TRUE)
			
			SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FOCUS_SET)
			
			OrbitalCannonLocal.iCamSwitchTimer = GET_GAME_TIMER()
			
			SET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_WAIT_FOR_LOAD_SCENE)
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_WAIT_FOR_LOAD_SCENE
			IF NOT HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sSwitchDelay)
				IF IS_NEW_LOAD_SCENE_LOADED()
				OR GET_GAME_TIMER() - OrbitalCannonLocal.iCamSwitchTimer > 10000
					IF IS_BIT_SET(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_RUNNING)
						NEW_LOAD_SCENE_STOP()
						CLEAR_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_RUNNING)
					ENDIF
					
					START_NET_TIMER(OrbitalCannonLocal.sSwitchDelay)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sSwitchDelay, 2000)
					RESET_NET_TIMER(OrbitalCannonLocal.sSwitchDelay)
					
					SET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_CLEANUP_LOAD_SCENE)
				ENDIF
			ENDIF
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_CLEANUP_LOAD_SCENE
			CLEAR_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_REQUIRED)
			CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SWITCHING_CAMERA)
			
			OrbitalCannonLocal.vLoadSceneLocation = <<0.0, 0.0, 0.0>>
			OrbitalCannonLocal.vLoadSceneRot = <<0.0, 0.0, 0.0>>
			
			RESET_ADAPTATION()
			
			IF NOT IS_BIT_SET(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
				START_MISSION_ORBITAL_CANNON_BACKGROUND_SOUND(OrbitalCannonLocal)
			ENDIF
			
			SET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_FADE_IN)
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_FADE_IN
			IF g_iSimpleInteriorState = SIMPLE_INT_STATE_IDLE
			OR g_bMissionPlacedOrbitalCannon
				IF NOT IS_SCREEN_FADED_IN()
					IF NOT IS_SCREEN_FADING_IN()
						TOGGLE_RENDERPHASES(TRUE)
						
						DO_SCREEN_FADE_IN(500)
					ENDIF
				ELSE
					SET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_CLEANUP_PROCESS)
				ENDIF
			ENDIF
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_CLEANUP_PROCESS
			CLEAR_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_REQUIRED)
			
			OrbitalCannonLocal.vLoadSceneLocation = <<0.0, 0.0, 0.0>>
			OrbitalCannonLocal.vLoadSceneRot = <<0.0, 0.0, 0.0>>
			
			SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
			
			SET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_USING_CANNON)
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_USING_CANNON
			IF NOT IS_BIT_SET(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
				FORCE_ALLOW_TIME_BASED_FADING_THIS_FRAME()
			ENDIF
						
			bReturnTrue = TRUE
		BREAK
	ENDSWITCH
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
		IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FORCE_SCALEFORM_DRAW)
		AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_WARNING)
			IF HAS_SCALEFORM_MOVIE_LOADED(OrbitalCannonLocal.sfOrbitalCannon)
				IF NOT IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_STATE_SET)
					BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_STATE")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
					END_SCALEFORM_MOVIE_METHOD()
					
					SET_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_STATE_SET)
				ENDIF
				
				IF HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sFiringTimer)
					IF NOT HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sFiringTimer, 1000)
						IF NOT IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_3)
							BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_COUNTDOWN")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
							END_SCALEFORM_MOVIE_METHOD()
							
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_0)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_1)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_2)
							SET_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_3)
						ENDIF
					ELIF NOT HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sFiringTimer, 2000)
						IF NOT IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_2)
							BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_COUNTDOWN")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
							END_SCALEFORM_MOVIE_METHOD()
							
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_0)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_1)
							SET_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_2)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_3)
						ENDIF
					ELIF NOT HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sFiringTimer, 3000)
						IF NOT IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_1)
							BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_COUNTDOWN")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
							END_SCALEFORM_MOVIE_METHOD()
							
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_0)
							SET_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_1)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_2)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_3)
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_0)
							BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_COUNTDOWN")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
							END_SCALEFORM_MOVIE_METHOD()
							
							SET_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_0)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_1)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_2)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_3)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_0)
						BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_COUNTDOWN")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						END_SCALEFORM_MOVIE_METHOD()
						
						SET_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_0)
						CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_1)
						CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_2)
						CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_3)
					ENDIF
				ENDIF
				
				IF HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sInitialDelay)
					BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_CHARGING_LEVEL")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(OrbitalCannonLocal.sInitialDelay) / 3000.0)
					END_SCALEFORM_MOVIE_METHOD()
					
					IF IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_CHARGING_FULL)
						CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_CHARGING_FULL)
					ENDIF
					
					IF IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_INACTIVE)
						CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_INACTIVE)
					ENDIF
				ELSE
					IF GET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal) = ORBITAL_CANNON_SWITCH_STATE_USING_CANNON
						IF NOT IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_CHARGING_FULL)
							BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_CHARGING_LEVEL")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1.0)
							END_SCALEFORM_MOVIE_METHOD()
							
							SET_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_CHARGING_FULL)
							
							IF IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_INACTIVE)
								CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_INACTIVE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD_PRIORITY_LOW)
				DRAW_SCALEFORM_MOVIE_FULLSCREEN(OrbitalCannonLocal.sfOrbitalCannon, 255, 255, 255, 0, 1)
				RESET_SCRIPT_GFX_ALIGN()
				
				IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
				OR HAVE_CONTROLS_CHANGED(PLAYER_CONTROL)
					REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(OrbitalCannonLocal.scaleformInstructionalButtons)
					
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_FRONTEND_CANCEL), "HUD_INPUT3", OrbitalCannonLocal.scaleformInstructionalButtons)
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_FRONTEND_X), "ORB_CAN_RE", OrbitalCannonLocal.scaleformInstructionalButtons)
						
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, eZoomOutControl), "ORB_CAN_ZOOMO", OrbitalCannonLocal.scaleformInstructionalButtons)
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, eZoomInControl), "ORB_CAN_ZOOMI", OrbitalCannonLocal.scaleformInstructionalButtons)
					ELSE
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, eZoomInControl), "ORB_CAN_ZOOM", OrbitalCannonLocal.scaleformInstructionalButtons)
					ENDIF
						
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_ATTACK), "ORB_CAN_FIRE", OrbitalCannonLocal.scaleformInstructionalButtons)
					
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
				ENDIF
				
				SPRITE_PLACEMENT thisSpritePlacement = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
				SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(OrbitalCannonLocal.scaleformInstructionalButtons, 1.0)
				RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(OrbitalCannonLocal.sfButton, thisSpritePlacement, OrbitalCannonLocal.scaleformInstructionalButtons, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(OrbitalCannonLocal.scaleformInstructionalButtons))
				SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bReturnTrue
ENDFUNC

FUNC BOOL CLIENT_MAINTAIN_USING_MISSION_ORBITAL_CANNON(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	
	DISABLE_DPADDOWN_THIS_FRAME()
	DISPLAY_AMMO_THIS_FRAME(FALSE)
	HUD_FORCE_WEAPON_WHEEL(FALSE)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	DISABLE_SELECTOR_THIS_FRAME()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_USE_CANNON_CLEANUP)
		IF CLIENT_MAINTAIN_ACTIVATED_MISSION_ORBITAL_CANNON(OrbitalCannonLocal)
			IF CLIENT_MAINTAIN_MISSION_ORBITAL_CANNON_INPUT(OrbitalCannonLocal)
				PRINTLN("[AM_MP_ORBITAL_CANNON] CLIENT_MAINTAIN_USING_MISSION_ORBITAL_CANNON - Closing the Orbital Cannon")
				
				IF IS_BIT_SET(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_WEATHER)
					CLEAR_OVERRIDE_WEATHER()
					CLEAR_BIT(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_WEATHER)
				ENDIF
				
				OrbitalCannonLocal.iCamSwitchTimer = 0
				
				SET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_FADE_OUT)
				
				CLEAR_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_ACTIVATING)
				CLEAR_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
				
				SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
				
				SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
				
				SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_USE_CANNON_CLEANUP)
			ENDIF
		ENDIF
	ELSE
		IF CLIENT_MAINTAIN_ACTIVATED_MISSION_ORBITAL_CANNON(OrbitalCannonLocal)
			CLIENT_CLEANUP_MISSION_ORBITAL_CANNON_CAMERA(OrbitalCannonLocal)
			
			CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_USE_CANNON_CLEANUP)
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_LOCATION_OF_ORBITAL_CANNON_TERMINAL(INT &iIndex)
	
	
	IF g_iOrbitalCannonObjectInUse = -1
		INT i
		REPEAT FMMC_MAX_NUM_OBJECTS i
			IF IS_BIT_SET(g_iMissionObjectHasSpawnedBitset, i)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iOrbitalCannonIndex > -1
				AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vPos, (<<1.5, 1.5, 1.5>>), FALSE, TRUE)
					iIndex = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iOrbitalCannonIndex
					g_iOrbitalCannonObjectInUse = i
					PRINTLN("[AM_MP_ORBITAL_CANNON][GET_LOCATION_OF_ORBITAL_CANNON_TERMINAL] (1) - iIndex: ", iIndex, " g_iOrbitalCannonObjectInUse: ", g_iOrbitalCannonObjectInUse, "vPos.x: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_iOrbitalCannonObjectInUse].vPos.x, " vPos.y: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_iOrbitalCannonObjectInUse].vPos.y, " vPos.z: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_iOrbitalCannonObjectInUse].vPos.z)
					RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vPos
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		IF IS_BIT_SET(g_iMissionObjectHasSpawnedBitset, g_iOrbitalCannonObjectInUse)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_iOrbitalCannonObjectInUse].iOrbitalCannonIndex > -1
			AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_iOrbitalCannonObjectInUse].vPos, (<<1.5, 1.5, 1.5>>), FALSE, TRUE)
				iIndex = g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_iOrbitalCannonObjectInUse].iOrbitalCannonIndex
				PRINTLN("[AM_MP_ORBITAL_CANNON][GET_LOCATION_OF_ORBITAL_CANNON_TERMINAL] (2) - iIndex: ", iIndex, " g_iOrbitalCannonObjectInUse: ", g_iOrbitalCannonObjectInUse, "vPos.x: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_iOrbitalCannonObjectInUse].vPos.x, " vPos.y: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_iOrbitalCannonObjectInUse].vPos.y, " vPos.z: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_iOrbitalCannonObjectInUse].vPos.z)
				RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_iOrbitalCannonObjectInUse].vPos
			ENDIF
		ENDIF	
	ENDIF
	
	g_iOrbitalCannonObjectInUse = -1
	iIndex = -1
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_MISSION_ORBITAL_CANNON_AREA(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	
	INT iMissionCannonIndex
	IF NOT IS_VECTOR_ZERO(GET_LOCATION_OF_ORBITAL_CANNON_TERMINAL(iMissionCannonIndex))
		IF iMissionCannonIndex > -1
			OrbitalCannonLocal.iMissionOrbitalCannonID = iMissionCannonIndex		
			PRINTLN("[AM_MP_ORBITAL_CANNON][IS_LOCAL_PLAYER_IN_MISSION_ORBITAL_CANNON_AREA] Returning true")
			RETURN TRUE
		ENDIF
	ENDIF
	
//	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<252.032074,6126.419434,-160.370575>>, <<253.117538,6126.419434,-158.369034>>, 1.5)
//		OrbitalCannonLocal.iMissionOrbitalCannonID = 0
//		
//		RETURN TRUE
//	ENDIF
//	
//	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<254.357483,6126.419434,-160.389389>>, <<255.523453,6126.419434,-158.387299>>, 1.5)
//		OrbitalCannonLocal.iMissionOrbitalCannonID = 1
//		
//		RETURN TRUE
//	ENDIF
//	
//	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<260.657471,6126.419434,-160.373611>>, <<262.075043,6126.419434,-158.382721>>, 1.5)
//		OrbitalCannonLocal.iMissionOrbitalCannonID = 2
//		
//		RETURN TRUE
//	ENDIF
//	
//	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<263.207642,6126.419434,-160.398376>>, <<264.539764,6126.419434,-158.372314>>, 1.5)
//		OrbitalCannonLocal.iMissionOrbitalCannonID = 3
//		
//		RETURN TRUE
//	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_FACING_MISSION_ORBITAL_CANNON(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	FLOAT fHeading = 0.0
	
	SWITCH OrbitalCannonLocal.iMissionOrbitalCannonID
		CASE 0	fHeading = 0.0	BREAK
		CASE 1	fHeading = 0.0	BREAK
		CASE 2	fHeading = 0.0	BREAK
		CASE 3	fHeading = 0.0	BREAK
	ENDSWITCH
		
	INT iMissionCannonIndex
	IF NOT IS_VECTOR_ZERO(GET_LOCATION_OF_ORBITAL_CANNON_TERMINAL(iMissionCannonIndex))
		IF IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(), GET_LOCATION_OF_ORBITAL_CANNON_TERMINAL(iMissionCannonIndex), 45.0)
			IF iMissionCannonIndex > -1
				RETURN TRUE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(PLAYER_PED_ID()), fHeading, 45.0)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ALONE_IN_MISSION_ORBITAL_CANNON_AREA(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	FLOAT fAngledAreaWidth = 0.0
	
	VECTOR vAngledArea1 = <<0.0, 0.0, 0.0>>
	VECTOR vAngledArea2 = <<0.0, 0.0, 0.0>>
	
	SWITCH OrbitalCannonLocal.iMissionOrbitalCannonID
		CASE 0
			fAngledAreaWidth = 1.5
			
			vAngledArea1 = <<252.032074,6126.419434,-160.370575>>
			vAngledArea2 = <<253.117538,6126.419434,-158.369034>>
		BREAK
		
		CASE 1
			fAngledAreaWidth = 1.5
			
			vAngledArea1 = <<254.357483,6126.419434,-160.389389>>
			vAngledArea2 = <<255.523453,6126.419434,-158.387299>>
		BREAK
		
		CASE 2
			fAngledAreaWidth = 1.5
			
			vAngledArea1 = <<260.657471,6126.419434,-160.373611>>
			vAngledArea2 = <<262.075043,6126.419434,-158.382721>>
		BREAK
		
		CASE 3
			fAngledAreaWidth = 1.5
			
			vAngledArea1 = <<263.207642,6126.419434,-160.398376>>
			vAngledArea2 = <<264.539764,6126.419434,-158.372314>>
		BREAK
	ENDSWITCH	
	
	INT iMissionCannonIndex
	VECTOR vPosPlayer
	vPosPlayer = GET_LOCATION_OF_ORBITAL_CANNON_TERMINAL(iMissionCannonIndex)
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		PLAYER_INDEX pPlayer
		
		pPlayer = INT_TO_PLAYERINDEX(i)
		
		IF pPlayer != INVALID_PLAYER_INDEX()
		AND pPlayer != PLAYER_ID()
		AND IS_NET_PLAYER_OK(pPlayer)
			IF IS_VECTOR_ZERO(vPosPlayer)
				IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(pPlayer), vAngledArea1, vAngledArea2, fAngledAreaWidth)
					PRINTLN("[AM_MP_ORBITAL_CANNON][IS_LOCAL_PLAYER_ALONE_IN_MISSION_ORBITAL_CANNON_AREA] (1) Returning FALSE")
					RETURN FALSE
				ENDIF
			ELSE
				IF IS_ENTITY_AT_COORD(GET_PLAYER_PED(pPlayer), vPosPlayer, <<1.0, 1.0, 1.0>>, FALSE, TRUE)
					PRINTLN("[AM_MP_ORBITAL_CANNON][IS_LOCAL_PLAYER_ALONE_IN_MISSION_ORBITAL_CANNON_AREA] (2) Returning FALSE")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
		
	RETURN TRUE
ENDFUNC

FUNC BOOL CLIENT_MAINTAIN_ACTIVATING_MISSION_ORBITAL_CANNON(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	BOOL bReadyForContext = FALSE
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
	AND NOT IS_BIT_SET(MPGlobals.KillYourselfData.iBitSet, biKY_AnimStarted)
	AND NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)
	AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
	AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
	AND NOT IS_PAUSE_MENU_ACTIVE_EX()
		IF IS_LOCAL_PLAYER_IN_MISSION_ORBITAL_CANNON_AREA(OrbitalCannonLocal)
		AND IS_LOCAL_PLAYER_FACING_MISSION_ORBITAL_CANNON(OrbitalCannonLocal)
		AND IS_LOCAL_PLAYER_ALONE_IN_MISSION_ORBITAL_CANNON_AREA(OrbitalCannonLocal)
			bReadyForContext = TRUE
		ENDIF
	ENDIF
	
	IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		RELEASE_CONTEXT_INTENTION(OrbitalCannonLocal.iOrbitalCannonContextIntention)
		OrbitalCannonLocal.iOrbitalCannonContextIntention = NEW_CONTEXT_INTENTION
	ENDIF
	
	IF bReadyForContext
	AND NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
		IF OrbitalCannonLocal.iOrbitalCannonContextIntention = NEW_CONTEXT_INTENTION
			REGISTER_CONTEXT_INTENTION(OrbitalCannonLocal.iOrbitalCannonContextIntention, CP_HIGH_PRIORITY, "ORB_CAN_START")
		ELSE
			IF HAS_CONTEXT_BUTTON_TRIGGERED(OrbitalCannonLocal.iOrbitalCannonContextIntention)
				PRINTLN("[AM_MP_ORBITAL_CANNON] CLIENT_MAINTAIN_ACTIVATING_MISSION_ORBITAL_CANNON - Triggering Orbital Cannon activation")
				
				CLEAR_ALL_BIG_MESSAGES()
				CLEANUP_BIG_MESSAGE()
				
				RELEASE_CONTEXT_INTENTION(OrbitalCannonLocal.iOrbitalCannonContextIntention)
				
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMissionModeBit, ciMISSION_USING_ORBITAL_CANNON)
					PRINTLN("[JR]-Setting orbital cannon use bit")
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMissionModeBit, ciMISSION_USING_ORBITAL_CANNON)
				ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RELEASE_CONTEXT_INTENTION(OrbitalCannonLocal.iOrbitalCannonContextIntention)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_MISSION_ORBITAL_CANNON(MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	SWITCH OrbitalCannonLocal.eStage
		CASE ORBITAL_CANNON_CLIENT_STAGE_INIT
			PRINTLN("[AM_MP_ORBITAL_CANNON] MAINTAIN_MISSION_ORBITAL_CANNON - Initialising Orbital Cannon")
			
			SET_MISSION_ORBITAL_CANNON_STAGE(OrbitalCannonLocal, ORBITAL_CANNON_CLIENT_STAGE_WALKING)
		BREAK
		
		CASE ORBITAL_CANNON_CLIENT_STAGE_WALKING
			IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_SUICIDE)
				PRINTLN("[AM_MP_ORBITAL_CANNON] MAINTAIN_MISSION_ORBITAL_CANNON - Enabling kill yourself option")
				
				ENABLE_KILL_YOURSELF_OPTION()
				
				CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_SUICIDE)
			ENDIF
			
			IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_INTERACTION)
				PRINTLN("[AM_MP_ORBITAL_CANNON] MAINTAIN_MISSION_ORBITAL_CANNON - Enabling interaction menu")
				
				ENABLE_INTERACTION_MENU()
				
				CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_INTERACTION)
			ENDIF
			
			IF CLIENT_MAINTAIN_ACTIVATING_MISSION_ORBITAL_CANNON(OrbitalCannonLocal)
				PRINTLN("[AM_MP_ORBITAL_CANNON] MAINTAIN_MISSION_ORBITAL_CANNON - Player is activating the Orbital Cannon")
				
				DISPLAY_CASH(FALSE)
				
				PRINTLN("[ORBITAL_CANNON] CLIENT_SETUP_SWITCH")
				
				OrbitalCannonLocal.iCamSwitchTimer = 0
				
				SET_MISSION_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_FADE_OUT)
				
				CLEAR_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_ACTIVATING)
				CLEAR_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
				
				SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
				
				SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_ACTIVATING)
				
				REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
				
				SET_MISSION_ORBITAL_CANNON_STAGE(OrbitalCannonLocal, ORBITAL_CANNON_CLIENT_STAGE_ACTIVATED)
				
				SET_BIT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CANNON)
			ENDIF
		BREAK
		
		CASE ORBITAL_CANNON_CLIENT_STAGE_ACTIVATED
			SET_IDLE_KICK_DISABLED_THIS_FRAME()
			
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				PRINTLN("[AM_MP_ORBITAL_CANNON] MAINTAIN_MISSION_ORBITAL_CANNON - Disabling player control")
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_PREVENT_VISIBILITY_CHANGES)
				
				SET_BIT(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_CONTROLS)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, TRUE)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				ENDIF
			ENDIF
			
			IF NOT IS_KILL_YOURSELF_OPTION_DISABLED()
				PRINTLN("[AM_MP_ORBITAL_CANNON] MAINTAIN_MISSION_ORBITAL_CANNON - Disabling kill yourself option")
				
				DISABLE_KILL_YOURSELF_OPTION()
				
				SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_SUICIDE)
			ENDIF
			
			IF NOT IS_INTERACTION_MENU_DISABLED()
				PRINTLN("[AM_MP_ORBITAL_CANNON] MAINTAIN_MISSION_ORBITAL_CANNON - Disabling interaction menu")
				
				DISABLE_INTERACTION_MENU()
				
				SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_INTERACTION)
			ENDIF
			
			SUPPRESS_HD_MAP_STREAMING_THIS_FRAME()
			OVERRIDE_LODSCALE_THIS_FRAME(1.0)
			USE_SCRIPT_CAM_FOR_AMBIENT_POPULATION_ORIGIN_THIS_FRAME(TRUE, TRUE)
			SET_SCRIPTED_CONVERSION_COORD_THIS_FRAME(GET_FINAL_RENDERED_CAM_COORD())
			
			IF CLEAN_UP_ON_CALL_WITH_OUT_WAIT()
				IF CLIENT_MAINTAIN_USING_MISSION_ORBITAL_CANNON(OrbitalCannonLocal)
					PRINTLN("[AM_MP_ORBITAL_CANNON] MAINTAIN_MISSION_ORBITAL_CANNON - Player chose to exit the Orbital Cannon")
					
					TIDYUP_MISSION_ORBITAL_CANNON(OrbitalCannonLocal, TRUE)
					
					SET_MISSION_ORBITAL_CANNON_STAGE(OrbitalCannonLocal, ORBITAL_CANNON_CLIENT_STAGE_WALKING)
				ENDIF
			ENDIF
			
			SET_CLEAR_ON_CALL_HUD_THIS_FRAME(TRUE)
		BREAK
	ENDSWITCH
	
	IF OrbitalCannonLocal.eStage = ORBITAL_CANNON_CLIENT_STAGE_ACTIVATED
		SUPPRESS_HD_MAP_STREAMING_THIS_FRAME()
		OVERRIDE_LODSCALE_THIS_FRAME(1.0)
		USE_SCRIPT_CAM_FOR_AMBIENT_POPULATION_ORIGIN_THIS_FRAME(TRUE, TRUE)
		SET_SCRIPTED_CONVERSION_COORD_THIS_FRAME(GET_FINAL_RENDERED_CAM_COORD())
		
		IF NOT OrbitalCannonLocal.bAudioBankLoaded
			IF NOT HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sAudioBankTimer, 5000)
				IF REQUEST_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_ION_CANNON")
					OrbitalCannonLocal.bAudioBankLoaded = TRUE
				ELSE
					IF NOT HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sAudioBankTimer)
						START_NET_TIMER(OrbitalCannonLocal.sAudioBankTimer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
		
		SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
	ENDIF
ENDPROC
