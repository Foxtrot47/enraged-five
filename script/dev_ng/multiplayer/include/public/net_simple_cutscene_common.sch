//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_simple_cutscene.sch																		//
// Description: Simple cutscene system to go with simple interior (common functions).						//
// Written by:  Tymon																						//
// Date:  		29/08/2016																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
USING "globals.sch"
USING "net_include.sch"

FUNC BOOL IS_PLAYER_IN_SIMPLE_CUTSCENE(PLAYER_INDEX playerID)
	IF playerID != INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(playerID)
		RETURN IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(playerID)].simpleCutscene.iBS, BS_SIMPLE_CUTSCENE_GLOBAL_PLAYER_BD_PLAYING)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_SIMPLE_CUTSCENE_HASH_PLAYER_IS_IN(PLAYER_INDEX playerID)
	IF IS_PLAYER_IN_SIMPLE_CUTSCENE(playerID)
		RETURN GlobalplayerBD_FM_3[NATIVE_TO_INT(playerID)].simpleCutscene.iCutsceneHash
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC BOOL IS_PLAYER_IN_THIS_SIMPLE_CUTSCENE(PLAYER_INDEX playerID, STRING strName)
	IF IS_PLAYER_IN_SIMPLE_CUTSCENE(playerID)
		RETURN GlobalplayerBD_FM_3[NATIVE_TO_INT(playerID)].simpleCutscene.iCutsceneHash = GET_HASH_KEY(strName)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_THIS_SIMPLE_CUTSCENE_BY_HASH_KEY(PLAYER_INDEX playerID, INT iHashKey)
	IF IS_PLAYER_IN_SIMPLE_CUTSCENE(playerID)
		RETURN GlobalplayerBD_FM_3[NATIVE_TO_INT(playerID)].simpleCutscene.iCutsceneHash = iHashKey
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SIMPLE_CUTSCENE_IS_SCENE_RUNNING(SIMPLE_CUTSCENE &cutscene, INT iSceneID)
	RETURN cutscene.iCurrentScene = iSceneID AND IS_BIT_SET(cutscene.iBSScenesInit, iSceneID)
ENDFUNC

FUNC BOOL SIMPLE_CUTSCENE_LIGHT_IS_SCENE_RUNNING(SIMPLE_CUTSCENE_LIGHT &cutscene, INT iSceneID)
	RETURN cutscene.iCurrentScene = iSceneID AND IS_BIT_SET(cutscene.iBSScenesInit, iSceneID)
ENDFUNC

FUNC INT SIMPLE_CUTSCENE_GET_CURRENT_SCENE_REMAINING_TIME(SIMPLE_CUTSCENE &cutscene)
	RETURN (cutscene.sScenes[cutscene.iCurrentScene].iDuration - cutscene.iCurrentSceneElapsedTime)
ENDFUNC
