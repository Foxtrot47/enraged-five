//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   net_plate_dups.sch                                       	//
//      AUTHOR          :   Neil Ferguson                                               //
//      DESCRIPTION     :   Commands to catch vehicle dupes based on licence plates		//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////


USING "rage_builtins.sch" 
USING "globals.sch"
USING "net_prints.sch"
USING "net_stat_system.sch"
USING "commands_debug.sch"
USING "script_misc.sch"
USING "vehicle_public.sch"
USING "social_public.sch"
USING "shop_public.sch"
USING "net_mod_value.sch"
USING "net_plate_dupes_private.sch"


#IF IS_DEBUG_BUILD
	
PROC CREATE_PLATE_DUPE_WIDGET()
	
	START_WIDGET_GROUP("Dupe Detector")
		TEXT_LABEL_63 str
		INT i
		
		START_WIDGET_GROUP("stat values")
		
			ADD_WIDGET_BOOL("Save Edited Values", g_bSavePlateDupeStatLists )
		
			REPEAT TOTAL_STORED_STATS_PLATES_PERSONALISED i
				str = "g_iPersonalisedCarPlateStats["
				str += i
				str += "]"
				ADD_WIDGET_INT_SLIDER(str, g_iPersonalisedCarPlateStats[i], LOWEST_INT, HIGHEST_INT, 1)
			ENDREPEAT
			
			REPEAT TOTAL_STORED_STATS_PLATES_SOLD i
				str = "g_iSoldCarPlateStats["
				str += i
				str += "]"
				ADD_WIDGET_INT_SLIDER(str, g_iSoldCarPlateStats[i], LOWEST_INT, HIGHEST_INT, 1)
			ENDREPEAT
			
			REPEAT TOTAL_STORED_STATS_KNOWN_DUPES i
				str = "g_iKnownDupePlateStats["
				str += i
				str += "]"
				ADD_WIDGET_INT_SLIDER(str, g_iKnownDupePlateStats[i], LOWEST_INT, HIGHEST_INT, 1)
				
				str = "g_iKnownDupeSoldStats["
				str += i
				str += "]"
				ADD_WIDGET_INT_SLIDER(str, g_iKnownDupeSoldStats[i], LOWEST_INT, HIGHEST_INT, 1)
			ENDREPEAT
			
			ADD_WIDGET_INT_SLIDER("g_iStoredIFruitPlate", g_iStoredIFruitPlate, LOWEST_INT, HIGHEST_INT, 1)
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("global values")
		
			REPEAT TOTAL_STORED_GLOBAL_PLATES_PERSONALISED i
				str = "g_iPersonalisedCarPlateGlobals["
				str += i
				str += "]"
				ADD_WIDGET_INT_SLIDER(str, g_iPersonalisedCarPlateGlobals[i], LOWEST_INT, HIGHEST_INT, 1)
			ENDREPEAT
			
			REPEAT TOTAL_STORED_GLOBAL_PLATES_SOLD i
				str = "g_iSoldCarPlateGlobals["
				str += i
				str += "]"
				ADD_WIDGET_INT_SLIDER(str, g_iSoldCarPlateGlobals[i], LOWEST_INT, HIGHEST_INT, 1)
			ENDREPEAT
			
			REPEAT TOTAL_STORED_GLOBAL_PLATES_KNOWN_DUPES i
				str = "g_iKnownDupePlateGlobals["
				str += i
				str += "]"
				ADD_WIDGET_INT_SLIDER(str, g_iKnownDupePlateGlobals[i], LOWEST_INT, HIGHEST_INT, 1)
				
				str = "g_iKnownDupeSoldGlobals["
				str += i
				str += "]"
				ADD_WIDGET_INT_SLIDER(str, g_iKnownDupeSoldGlobals[i], LOWEST_INT, HIGHEST_INT, 1)
			ENDREPEAT
			
			REPEAT NUMBER_OF_VEHICLES_IN_METRIC_SENT i
				str = "g_iVehicleIDMetricSent["
				str += i
				str += "]"
				ADD_WIDGET_INT_SLIDER(str, g_iVehicleIDMetricSent[i], LOWEST_INT, HIGHEST_INT, 1)	
			ENDREPEAT
			
			REPEAT NUMBER_OF_VEHICLES_IN_METRIC_SENT i
				str = "g_iLocationMetricSent["
				str += i
				str += "]"
				ADD_WIDGET_INT_SLIDER(str, g_iLocationMetricSent[i], LOWEST_INT, HIGHEST_INT, 1)	
			ENDREPEAT
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Tunables")
		
			START_WIDGET_GROUP("Functions")
				ADD_WIDGET_BOOL("bEnabled_DUPE_DETECTOR", g_sMPTunables.bEnabled_DUPE_DETECTOR)
				ADD_WIDGET_BOOL("bEnabled_IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER",  g_sMPTunables.bEnabled_IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER )
				ADD_WIDGET_BOOL("bEnabled_IS_PLATE_DUPE_OF_CAR_IN_GARAGE",  g_sMPTunables.bEnabled_IS_PLATE_DUPE_OF_CAR_IN_GARAGE)
				ADD_WIDGET_BOOL("bEnabled_ADD_PLATE_TO_RECENTLY_USED_PERSONALISED_PLATE_LIST",  g_sMPTunables.bEnabled_ADD_PLATE_TO_RECENTLY_USED_PERSONALISED_PLATE_LIST)
				ADD_WIDGET_BOOL("bEnabled_HAS_PLATE_BEEN_RECENTLY_USED_IN_PERSONALISED_PLATE_OPTION",  g_sMPTunables.bEnabled_HAS_PLATE_BEEN_RECENTLY_USED_IN_PERSONALISED_PLATE_OPTION)
				ADD_WIDGET_BOOL("bEnabled_ADD_PLATE_TO_RECENTLY_SOLD_PLATE_LIST",  g_sMPTunables.bEnabled_ADD_PLATE_TO_RECENTLY_SOLD_PLATE_LIST)
				ADD_WIDGET_BOOL("bEnabled_HAS_PLATE_BEEN_RECENTLY_SOLD",  g_sMPTunables.bEnabled_HAS_PLATE_BEEN_RECENTLY_SOLD)
				ADD_WIDGET_BOOL("bEnabled_DOES_VEHICLE_HAVE_REQUIRED_DECORATORS",  g_sMPTunables.bEnabled_DOES_VEHICLE_HAVE_REQUIRED_DECORATORS)
				ADD_WIDGET_BOOL("bEnabled_ADD_PLATE_TO_KNOWN_DUPE_PLATE_LIST",  g_sMPTunables.bEnabled_ADD_PLATE_TO_KNOWN_DUPE_PLATE_LIST)
				ADD_WIDGET_BOOL("bEnabled_IS_PLATE_A_KNOWN_DUPE",  g_sMPTunables.bEnabled_IS_PLATE_A_KNOWN_DUPE)
				ADD_WIDGET_BOOL("bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE",  g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE)
				ADD_WIDGET_BOOL("bEnabled_IS_VEHICLE_A_GARAGE_DUPE",  g_sMPTunables.bEnabled_IS_VEHICLE_A_GARAGE_DUPE)
				ADD_WIDGET_BOOL("bEnabled_LOCK_ANY_AMBIENT_DUPES_OF_THIS_VEHICLE",  g_sMPTunables.bEnabled_LOCK_ANY_AMBIENT_DUPES_OF_THIS_VEHICLE)
				ADD_WIDGET_BOOL("bEnabled_ENSURE_PV_PLATE_MATCHES_SAVED_DATA",  g_sMPTunables.bEnabled_ENSURE_PV_PLATE_MATCHES_SAVED_DATA)
				ADD_WIDGET_BOOL("bEnabled_IS_VEHICLE_A_SUSPECTED_DUPE",  g_sMPTunables.bEnabled_IS_VEHICLE_A_SUSPECTED_DUPE)
			STOP_WIDGET_GROUP()
					
			
			START_WIDGET_GROUP("Checks")
			
				ADD_WIDGET_BOOL("bEnabled_CAN_WE_USE_THIS_VEHICLE_check_1",  g_sMPTunables.bEnabled_CAN_WE_USE_THIS_VEHICLE_check_1)
				ADD_WIDGET_BOOL("bEnabled_CAN_WE_USE_THIS_VEHICLE_check_2",  g_sMPTunables.bEnabled_CAN_WE_USE_THIS_VEHICLE_check_2)
				ADD_WIDGET_BOOL("bEnabled_CAN_WE_USE_THIS_VEHICLE_check_3",  g_sMPTunables.bEnabled_CAN_WE_USE_THIS_VEHICLE_check_3)
				ADD_WIDGET_BOOL("bEnabled_CAN_WE_USE_THIS_VEHICLE_check_4",  g_sMPTunables.bEnabled_CAN_WE_USE_THIS_VEHICLE_check_4)
				
				ADD_WIDGET_BOOL("bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_1",  g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_1)
				ADD_WIDGET_BOOL("bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_2",  g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_2)
				ADD_WIDGET_BOOL("bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_3",  g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_3)
				ADD_WIDGET_BOOL("bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_4",  g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_4)
				ADD_WIDGET_BOOL("bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_5",  g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_5)
			
				ADD_WIDGET_BOOL("bEnabled_DO_STAGE_PLATES_check_1",  g_sMPTunables.bEnabled_DO_STAGE_PLATES_check_1)
			
				ADD_WIDGET_BOOL("bEnabled_GET_CARMOD_SELL_VALUE_apply_dupe_multiplier",  g_sMPTunables.bEnabled_GET_CARMOD_SELL_VALUE_apply_dupe_multiplier)
			
			STOP_WIDGET_GROUP()	
			
			START_WIDGET_GROUP("Limits")
			
				ADD_WIDGET_INT_SLIDER("iTotalModValueThresholdLow", g_sMPTunables.iTotalModValueThresholdLow, 0, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("iTotalModValueThresholdHigh", g_sMPTunables.iTotalModValueThresholdHigh, 0, HIGHEST_INT, 1)
				ADD_WIDGET_FLOAT_SLIDER("fKnownDupeMultiplierLevel0",  g_sMPTunables.fKnownDupeMultiplierLevel0, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fKnownDupeMultiplierLevel1",  g_sMPTunables.fKnownDupeMultiplierLevel1, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fKnownDupeMultiplierLevel2",  g_sMPTunables.fKnownDupeMultiplierLevel2, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fKnownDupeMultiplierLevel3",  g_sMPTunables.fKnownDupeMultiplierLevel3, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fKnownDupeMultiplierLevel4",  g_sMPTunables.fKnownDupeMultiplierLevel4, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fKnownDupeMultiplierLevel5",  g_sMPTunables.fKnownDupeMultiplierLevel5, 0.0, 1.0, 0.01)
				ADD_WIDGET_INT_SLIDER("iNumberOfGarageDupesAllowedToBeSold", g_sMPTunables.iNumberOfGarageDupesAllowedToBeSold, 0, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("iNumberOfGarageDupesSoldToTriggerLimiterChanges", g_sMPTunables.iNumberOfGarageDupesSoldToTriggerLimiterChanges, 0, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("iForcedExploitLevelIncrease", g_sMPTunables.iForcedExploitLevelIncrease, 0, HIGHEST_INT, 1)
				ADD_WIDGET_BOOL("bForceMaxCapHitToday", g_sMPTunables.bForceMaxCapHitToday)

			STOP_WIDGET_GROUP()
			

		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Dupe Creator")
		
			g_AmbientDupePlateText = ADD_TEXT_WIDGET("Plate Text")
			SET_CONTENTS_OF_TEXT_WIDGET(g_AmbientDupePlateText, "Dupe 1")
			//ADD_WIDGET_BOOL("Get hash of plate", g_bGetHashOfPlate)
			//ADD_WIDGET_INT_READ_ONLY("Plate as hash", g_iHashOfPlate)
			
			ADD_WIDGET_INT_SLIDER("ModelHash", g_iAmbientDupeModelHash, LOWEST_INT, HIGHEST_INT, 1)
			ADD_WIDGET_BOOL("Create ambient dupe", g_bCreateAmbientDupe)	
			ADD_WIDGET_STRING("Must be in vehicle to apply")
			ADD_WIDGET_BOOL("Apply Plate", g_bApplyPlateToCurrentVehicle)
			ADD_WIDGET_BOOL("Apply Random Mods", g_bGiveVehicleRandomMods)
			ADD_WIDGET_BOOL("Apply Random Colour", g_bGiveVehicleRandomColour)
		
			ADD_WIDGET_BOOL("Dupe Current Vehicle", g_bDupeCurrentVehicle)
			ADD_WIDGET_BOOL("Dupe Current Vehicle Using Above Plate", g_bDupeCurrentVehicleAndPlate)
			ADD_WIDGET_BOOL("Dupe Current Garage Vehicle", g_bDupeCurrentGarageVehicle)
			ADD_WIDGET_BOOL("Unbrick car player is using", g_bUnbrickCarPlayerIsUsing)
			
			ADD_WIDGET_BOOL("Spawn current vehicle dupe in avenger", g_bOverideDupeCoord)
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Debug")
			ADD_WIDGET_BOOL("g_bClearPlateDupeStatLists", g_bClearPlateDupeStatLists )
			ADD_WIDGET_BOOL("g_bOutputVehicleTimestamps", g_bOutputVehicleTimestamps)
			ADD_WIDGET_INT_SLIDER("g_iCurrentPVSlotInProperty", g_iCurrentPVSlotInProperty, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("g_iCurrentPVSlotInOfficeGarage", g_iCurrentPVSlotInOfficeGarage, -1, HIGHEST_INT, 1)	
			
	
			ADD_WIDGET_BOOL("Clear Personalised Plates Lists", g_bClearRecentlyChangedPlatesLists )
			ADD_WIDGET_BOOL("Clear Recently Sold Lists", g_bClearRecentlySoldLists )
			ADD_WIDGET_BOOL("Clear Known Dupes Lists", g_bClearKnownDupesLists )
			
			ADD_WIDGET_FLOAT_SLIDER("g_fDD_Scale", g_fDD_Scale, 0.0, 1.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("g_fDD_LineY", g_fDD_LineY, 0.0, 1.0, 0.01)
			ADD_WIDGET_BOOL("g_bDD_ShowDebugText", g_bDD_ShowDebugText )
			ADD_WIDGET_BOOL("g_bDD_OutputAllValues", g_bDD_OutputAllValues)
			ADD_WIDGET_BOOL("g_bHasProcessedIFruitOrder", g_bHasProcessedIFruitOrder)
			
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	
ENDPROC

#ENDIF




ENUM DD_LOCATION
	DD_LOC_NULL,
	DD_LOC_VEHICLE_ENTRY,
	DD_LOC_MOD_SHOP_ENTRY,
	DD_LOC_MOD_SHOP_SELL
ENDENUM

ENUM DD_REASON
	
	DD_REASON_NULL,
	DD_REASON_PERSONALISED_PLATE_BUT_NO_DECORATORS,
	DD_REASON_PLATE_MATCHES_CAR_IN_GARAGE,
	DD_REASON_VALUE_OF_MODS_IS_TOO_HIGH,
	DD_REASON_MATCHES_PLATE_OF_NEARBY_AMBIENT_VEHICLE,
	DD_REASON_IS_A_KNOWN_DUPE,
	DD_REASON_PLATE_HAS_BEEN_RECENTLY_PERSONALISED,
	DD_REASON_HAS_BEEN_RECENTLY_SOLD
ENDENUM




// ============== sting functions for number plate -========================

FUNC BOOL IS_CHAR_A_SINGLE_DIGIT_POSITIVE_NUMBER(STRING sChar)
	INT iNumber
	IF (STRING_TO_INT(sChar, iNumber))
	AND (iNumber >= 0)
	AND (iNumber <= 9)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_AMBIENT_PLATE_FORMAT(TEXT_LABEL_15 tlPlate)
	
	STRING sChar
	
	Pad_Plate_To_8_Characters(tlPlate)
	
	// 1st char - NUMBER
	sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlPlate, 0, 1)
	IF NOT IS_CHAR_A_SINGLE_DIGIT_POSITIVE_NUMBER(sChar)
		//PRINTLN("IS_AMBIENT_PLATE_FORMAT - false char 1 not a number '", tlPlate, "'")
		RETURN FALSE
	ENDIF
	
	// 2nd char - NUMBER
	sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlPlate, 1, 2)
	IF NOT IS_CHAR_A_SINGLE_DIGIT_POSITIVE_NUMBER(sChar)
		//PRINTLN("IS_AMBIENT_PLATE_FORMAT - false char 2 not a number '", tlPlate, "'")
		RETURN FALSE
	ENDIF
	
	// 3rd char - LETTER
	sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlPlate, 2, 3)
	IF IS_CHAR_A_SINGLE_DIGIT_POSITIVE_NUMBER(sChar)
		//PRINTLN("IS_AMBIENT_PLATE_FORMAT - false char 3 is a number '", tlPlate, "'")
		RETURN FALSE
	ENDIF
	
	// 4th char - LETTER
	sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlPlate, 3, 4)
	IF IS_CHAR_A_SINGLE_DIGIT_POSITIVE_NUMBER(sChar)
		//PRINTLN("IS_AMBIENT_PLATE_FORMAT - false char 4 is a number '", tlPlate, "'")
		RETURN FALSE
	ENDIF
	
	// 5th char - LETTER
	sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlPlate, 4, 5)
	IF IS_CHAR_A_SINGLE_DIGIT_POSITIVE_NUMBER(sChar)
		//PRINTLN("IS_AMBIENT_PLATE_FORMAT - false char 5 is a number '", tlPlate, "'")
		RETURN FALSE
	ENDIF	
	
	// 6th char - NUMBER
	sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlPlate, 5, 6)
	IF NOT IS_CHAR_A_SINGLE_DIGIT_POSITIVE_NUMBER(sChar)
		//PRINTLN("IS_AMBIENT_PLATE_FORMAT - false char 6 not a number '", tlPlate, "'")
		RETURN FALSE
	ENDIF		
	
	// 6th char - NUMBER
	sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlPlate, 6, 7)
	IF NOT IS_CHAR_A_SINGLE_DIGIT_POSITIVE_NUMBER(sChar)
		//PRINTLN("IS_AMBIENT_PLATE_FORMAT - false char 7 not a number '", tlPlate, "'")
		RETURN FALSE
	ENDIF
	
	// 6th char - NUMBER
	sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlPlate, 7, 8)
	IF NOT IS_CHAR_A_SINGLE_DIGIT_POSITIVE_NUMBER(sChar)
		//PRINTLN("IS_AMBIENT_PLATE_FORMAT - false char 8 not a number '", tlPlate, "'")
		RETURN FALSE
	ENDIF

	//PRINTLN("IS_AMBIENT_PLATE_FORMAT - true for '", tlPlate, "'")
	RETURN TRUE

ENDFUNC

FUNC BOOL IS_FREE_PLATE_FORMAT(TEXT_LABEL_15 tlPlate)
	
	STRING sChar
	
	Pad_Plate_To_8_Characters(tlPlate)
	
	// 1st char - NUMBER
	sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlPlate, 0, 1)
	IF NOT IS_CHAR_A_SINGLE_DIGIT_POSITIVE_NUMBER(sChar)
		//PRINTLN("IS_FREE_PLATE_FORMAT - false char 1 not a number '", tlPlate, "'")
		RETURN FALSE
	ENDIF
	
	// 2nd char - LETTER
	sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlPlate, 1, 2)
	IF IS_CHAR_A_SINGLE_DIGIT_POSITIVE_NUMBER(sChar)
		//PRINTLN("IS_FREE_PLATE_FORMAT - false char 2 is a number '", tlPlate, "'")
		RETURN FALSE
	ENDIF
	
	// 3rd char - LETTER
	sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlPlate, 2, 3)
	IF IS_CHAR_A_SINGLE_DIGIT_POSITIVE_NUMBER(sChar)
		//PRINTLN("IS_FREE_PLATE_FORMAT - false char 3 is a number '", tlPlate, "'")
		RETURN FALSE
	ENDIF
	
	// 4th char - LETTER
	sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlPlate, 3, 4)
	IF IS_CHAR_A_SINGLE_DIGIT_POSITIVE_NUMBER(sChar)
		//PRINTLN("IS_FREE_PLATE_FORMAT - false char 4 is a number '", tlPlate, "'")
		RETURN FALSE
	ENDIF
	
	// 5th char - LETTER
	sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlPlate, 4, 5)
	IF IS_CHAR_A_SINGLE_DIGIT_POSITIVE_NUMBER(sChar)
		//PRINTLN("IS_FREE_PLATE_FORMAT - false char 5 is a number '", tlPlate, "'")
		RETURN FALSE
	ENDIF	
	
	// 6th char - NUMBER
	sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlPlate, 5, 6)
	IF NOT IS_CHAR_A_SINGLE_DIGIT_POSITIVE_NUMBER(sChar)
		//PRINTLN("IS_FREE_PLATE_FORMAT - false char 6 not a number '", tlPlate, "'")
		RETURN FALSE
	ENDIF		
	
	// 7th char - NUMBER
	sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlPlate, 6, 7)
	IF NOT IS_CHAR_A_SINGLE_DIGIT_POSITIVE_NUMBER(sChar)
		//PRINTLN("IS_FREE_PLATE_FORMAT - false char 7 not a number '", tlPlate, "'")
		RETURN FALSE
	ENDIF
	
	// 8th char - NUMBER
	sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlPlate, 7, 8)
	IF NOT IS_CHAR_A_SINGLE_DIGIT_POSITIVE_NUMBER(sChar)
		//PRINTLN("IS_FREE_PLATE_FORMAT - false char 8 not a number '", tlPlate, "'")
		RETURN FALSE
	ENDIF

	//PRINTLN("IS_FREE_PLATE_FORMAT - true for '", tlPlate, "'")
	RETURN TRUE

ENDFUNC




FUNC BOOL IS_PLATE_A_KNOWN_DUPE(TEXT_LABEL_15 tlPlate, INT iNumberAllowedToBeSold, BOOL bCheckGlobals=TRUE)

	IF NOT (g_sMPTunables.bEnabled_DUPE_DETECTOR)
		PRINTLN("[platedupes] IS_PLATE_A_KNOWN_DUPE - dupe detector disabled")
		RETURN FALSE
	ENDIF	

	IF ( g_sMPTunables.bEnabled_IS_PLATE_A_KNOWN_DUPE)		
		
		Pad_Plate_To_8_Characters(tlPlate)
		
		INT i
		INT iHashPlate = GET_HASH_KEY(tlPlate)
		INT iSold
		REPEAT TOTAL_STORED_STATS_KNOWN_DUPES i
			IF (g_iKnownDupePlateStats[i] = iHashPlate)
				iSold = GetStoredKnownDupeSoldForPlate(tlPlate)
				IF iSold >= iNumberAllowedToBeSold
			
					PRINTLN("[platedupes] IsKnownDupeStat  true for '", tlPlate, "', stat i = ", i, " sold ", iSold, " hash ", iHashPlate)				
					RETURN TRUE
					
				ENDIF
			ENDIF
		ENDREPEAT	
		IF (bCheckGlobals)
			IF IsInValueInGlobalArray(g_iKnownDupePlateGlobals, iHashPlate)
				iSold = GetStoredKnownDupeSoldForPlate(tlPlate)
				IF iSold >= iNumberAllowedToBeSold
					PRINTLN("[platedupes] IS_PLATE_A_KNOWN_DUPE  true for '", tlPlate, "', global sold ", iSold, " hash ", iHashPlate)				
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[platedupes] IS_PLATE_A_KNOWN_DUPE  disabled")	
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_PLATE_STORED_IFRUIT_PLATE(TEXT_LABEL_15 tlPlate)
	// check the free ifruit plate
	IF NOT (g_iStoredIFruitPlate = 0)
	AND NOT (g_iStoredIFruitPlate = -1)
		IF (GET_HASH_KEY(tlPlate) = g_iStoredIFruitPlate)
			PRINTLN("[platedupes] IS_PLATE_STORED_IFRUIT_PLATE - true for free ifruit plate '", tlPlate, "'")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER(TEXT_LABEL_15 tlPlate)

	IF NOT (g_sMPTunables.bEnabled_DUPE_DETECTOR)
		PRINTLN("[platedupes] IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER - dupe detector disabled")
		RETURN FALSE
	ENDIF
	
	IF ( g_sMPTunables.bEnabled_IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER)
	
		IF NOT IS_PLATE_A_KNOWN_DUPE(tlPlate, 0)
	
			IF IS_PLAYERS_SC_NUMBER_PLATES_READY()
			
				INT i
				REPEAT MAX_NUMBER_OF_SC_LICENSE_PLATES i
					IF g_bPlateTextFromSCAccountInMPList[i]
						IF Are_Plates_Equal(tlPlate, g_tlPlateTextFromSCAccount[i])
							PRINTLN("[platedupes] IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER - true for '", tlPlate, "'")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDREPEAT
			ELSE
				PRINTLN("[platedupes] IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER  IS_PLAYERS_SC_NUMBER_PLATES_READY = FALSE")
			ENDIF
			
			// check the app plate
			IF Are_Plates_Equal(tlPlate, g_savedGlobals.sSocialData.tlCarAppPlateText)
				PRINTLN("[platedupes] IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER - true for app plate '", tlPlate, "'")
				RETURN TRUE
			ENDIF
			
			// check the free ifruit plate
			IF IS_PLATE_STORED_IFRUIT_PLATE(tlPlate)
				PRINTLN("[platedupes] IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER - true for free ifruit plate '", tlPlate, "'")
				RETURN TRUE
			ENDIF
			
		ELSE
			PRINTLN("[platedupes] IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER - is a known dupe '", tlPlate, "'")	
		ENDIF
		
	ELSE
		PRINTLN("[platedupes] IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER  disabled")
	ENDIF

	RETURN FALSE

ENDFUNC

#IF IS_DEBUG_BUILD

PROC DEBUG_DUPE_TEXT(STRING sMessage, FLOAT fScale, FLOAT PosX, FLOAT PosY)
	IF (g_bDD_ShowDebugText)
		SET_TEXT_SCALE(0.0000, fScale)
		SET_TEXT_COLOUR(255,0,0, 255)
		DISPLAY_TEXT_WITH_PLAYER_NAME(PosX, PosY, "STRING", sMessage, HUD_COLOUR_RED)
	ENDIF
ENDPROC

PROC DISPLAY_DUPE_MESSAGE_ON_VEHICLE(VEHICLE_INDEX VehicleID, STRING sMessage)
	IF (g_bDD_ShowDebugText)
		IF DOES_ENTITY_EXIST(VehicleID)
		AND NOT IS_ENTITY_DEAD(VehicleID)
			VECTOR vPos = GET_ENTITY_COORDS(VehicleID)
			vPos.z += 0.5
			FLOAT fPosX, fPosY
			GET_HUD_SCREEN_POSITION_FROM_WORLD_POSITION(vPos, fPosX, fPosY)
			DEBUG_DUPE_TEXT("DUPE DETECTOR", g_fDD_Scale, fPosX, fPosY-g_fDD_LineY)
			DEBUG_DUPE_TEXT(sMessage, g_fDD_Scale, fPosX, fPosY)
		ENDIF
	ENDIF
ENDPROC

#ENDIF

FUNC BOOL IS_VEHICLE_A_LOCKED_PV(VEHICLE_INDEX VehicleID)
	IF DECOR_EXIST_ON(VehicleID,"Player_Vehicle")	
	AND (DECOR_GET_INT(VehicleID, "Player_Vehicle") = -1)
		RETURN TRUE
	ENDIF
	IF DECOR_EXIST_ON(VehicleID,"Player_Truck")	
	AND (DECOR_GET_INT(VehicleID, "Player_Truck") = -1)
		RETURN TRUE
	ENDIF
	IF DECOR_EXIST_ON(VehicleID,"Player_Avenger")	
	AND (DECOR_GET_INT(VehicleID, "Player_Avenger") = -1)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_VEHICLE_HAVE_REQUIRED_DECORATORS(VEHICLE_INDEX VehicleID)
	
	IF NOT (g_sMPTunables.bEnabled_DUPE_DETECTOR)
		PRINTLN("[platedupes] DOES_VEHICLE_HAVE_REQUIRED_DECORATORS - dupe detector disabled")
		RETURN TRUE
	ENDIF	
	
	IF ( g_sMPTunables.bEnabled_DOES_VEHICLE_HAVE_REQUIRED_DECORATORS)

		IF DOES_ENTITY_EXIST(VehicleID)
		AND NOT IS_ENTITY_DEAD(VehicleID)
				
			IF IS_ENTITY_A_MISSION_ENTITY(VehicleID) // only process non mission entity vehicles
				PRINTLN("[platedupes] DOES_VEHICLE_HAVE_REQUIRED_DECORATORS - is a mission vehicle")
				RETURN TRUE
			ENDIF		
		
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_A_GARAGE)
		
				BOOL bGotValidDecorator = FALSE
				
				// pv decorator.
				IF DECOR_EXIST_ON(VehicleID,"Player_Vehicle")	
				AND (DECOR_GET_INT(VehicleID, "Player_Vehicle") = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
					bGotValidDecorator = TRUE	
				ENDIF
				
				// modded by decorator.
				IF DECOR_EXIST_ON(VehicleID, "Veh_Modded_By_Player")
				AND (DECOR_GET_INT(VehicleID, "Veh_Modded_By_Player") = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
					bGotValidDecorator = TRUE		
				ENDIF	
				
				// pv truck
				IF DECOR_EXIST_ON(VehicleID,"Player_Truck")	
				AND (DECOR_GET_INT(VehicleID, "Player_Truck") = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
					bGotValidDecorator = TRUE	
				ENDIF
				
				IF DECOR_EXIST_ON(VehicleID,"Player_Avenger")	
				AND (DECOR_GET_INT(VehicleID, "Player_Avenger") = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
					bGotValidDecorator = TRUE	
				ENDIF
				
				IF NOT (bGotValidDecorator)
					PRINTLN("[platedupes] DOES_VEHICLE_HAVE_REQUIRED_DECORATORS - failed.")
					RETURN FALSE
				ENDIF	
				
			ELSE
				PRINTLN("[platedupes] DOES_VEHICLE_HAVE_REQUIRED_DECORATORS  - in garage.")
			ENDIF
		
		ENDIF
		
	ELSE
		PRINTLN("[platedupes]  g_sMPTunables.bEnabled_DOES_VEHICLE_HAVE_REQUIRED_DECORATORS  disabled")
	ENDIF
	
	RETURN TRUE

ENDFUNC



FUNC INT GET_MOD_VALUE_LIMIT_FOR_VEHICLE(VEHICLE_INDEX VehicleID)
	
	#IF IS_DEBUG_BUILD
	IF (g_bAllowAllPremiumVehiclesToBeModified)  // for kenneths command line stuff
		RETURN 999999999 
	ENDIF
	#ENDIF	
	
	MODEL_NAMES ModelName = GET_ENTITY_MODEL(VehicleID)
	
	// if its a premium vehicle return the high
	IF IS_MP_PREMIUM_VEHICLE(ModelName)
		RETURN g_sMPTunables.iTotalModValueThresholdHigh
	ENDIF
	
	// is in high list?
	INT i
	REPEAT NUMBER_OF_HIGH_MOD_VALUE_MODELS i
		IF (g_HighModValueModels[i] = ModelName)
			RETURN g_sMPTunables.iTotalModValueThresholdHigh
		ENDIF
	ENDREPEAT
	
	// else return low
	RETURN g_sMPTunables.iTotalModValueThresholdLow
	
ENDFUNC


FUNC BOOL DOES_VEHICLE_HAVE_PERSONALISED_PLATE_OF_LOCAL_PLAYER(VEHICLE_INDEX VehicleID)
	TEXT_LABEL_15 tlPlate = GET_VEHICLE_NUMBER_PLATE_TEXT(VehicleID) 
	RETURN IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER(tlPlate)
ENDFUNC


FUNC BOOL IS_EARLIEST_TIMESTAMP_VEHICLE_IN_GARAGE_WITH_PLATE(TEXT_LABEL_15 tlPlate, INT iSlot)
	
	// time of this vehicle
	INT iTimeOfThisVehicle = g_MpSavedVehicles[iSlot].iObtainedTime 
	
	// find earliest time in garage with this slot
	INT iEarliestTimeInGarage = HIGHEST_INT
	INT i
	REPEAT MAX_MP_SAVED_VEHICLES i
		IF (i != iSlot)
			IF (g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT)
				IF ArePlateHashesEqual(tlPlate, g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.tlPlateText)
					IF (g_MpSavedVehicles[i].iObtainedTime < iEarliestTimeInGarage)
						iEarliestTimeInGarage = g_MpSavedVehicles[i].iObtainedTime
						PRINTLN("[platedupes] IS_EARLIEST_TIMESTAMP_VEHICLE_IN_GARAGE_WITH_PLATE  ealier time in garage for plate ", tlPlate, " slot ", i, ", timestamp = ", iEarliestTimeInGarage)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT	
	
	PRINTLN("[platedupes] IS_EARLIEST_TIMESTAMP_VEHICLE_IN_GARAGE_WITH_PLATE  iTimeOfThisVehicle ", iTimeOfThisVehicle, " iEarliestTimeInGarage ", iEarliestTimeInGarage)
	
	IF (iTimeOfThisVehicle > iEarliestTimeInGarage)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC INT GET_NUMBER_OF_CARS_IN_GARAGE_WITH_PLATE(TEXT_LABEL_15 tlPlate)
	
	IF NOT (g_sMPTunables.bEnabled_DUPE_DETECTOR)
		PRINTLN("[platedupes] GET_NUMBER_OF_CARS_IN_GARAGE_WITH_PLATE - dupe detector disabled")
		RETURN 0
	ENDIF
	
	INT iCount
	INT i
	iCount = 0
	REPEAT MAX_MP_SAVED_VEHICLES i
		IF (g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT)
			IF ArePlateHashesEqual(tlPlate, g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.tlPlateText)
				iCount++
			ENDIF		
		ENDIF
	ENDREPEAT	
	
	PRINTLN("[platedupes] GET_NUMBER_OF_CARS_IN_GARAGE_WITH_PLATE - there are ", iCount, " vehicles in garage with plate '", tlPlate, "'")
	
	RETURN iCount
	
ENDFUNC	

FUNC INT GET_TOTAL_NUMBER_OF_CARS_OWNED_BY_PLAYER()

	IF NOT (g_sMPTunables.bEnabled_DUPE_DETECTOR)
		PRINTLN("[platedupes] GET_TOTAL_NUMBER_OF_CARS_OWNED_BY_PLAYER - dupe detector disabled")
		RETURN 0
	ENDIF
	
	INT iCount
	INT i
	iCount = 0
	REPEAT MAX_MP_SAVED_VEHICLES i
		IF (g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT)
		AND NOT IS_THIS_MODEL_A_SPECIAL_VEHICLE(g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel)
		AND NOT IS_THIS_MODEL_ALLOWED_IN_HANGAR(g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel)
			iCount++
		ENDIF
	ENDREPEAT	
	
	PRINTLN("[platedupes] GET_TOTAL_NUMBER_OF_CARS_OWNED_BY_PLAYER - player owns ", iCount, " vehicles.")
	
	RETURN iCount
ENDFUNC
 
FUNC BOOL IS_PLATE_DUPE_OF_CAR_IN_GARAGE(TEXT_LABEL_15 tlPlate, MODEL_NAMES ModelName, INT iSlot=-1)

	IF NOT (g_sMPTunables.bEnabled_DUPE_DETECTOR)
		PRINTLN("[platedupes] IS_PLATE_DUPE_OF_CAR_IN_GARAGE - dupe detector disabled")
		RETURN FALSE
	ENDIF
	
	Pad_Plate_To_8_Characters(tlPlate)

	IF ( g_sMPTunables.bEnabled_IS_PLATE_DUPE_OF_CAR_IN_GARAGE)
	
		IF IS_AMBIENT_PLATE_FORMAT(tlPlate)
			INT i
			REPEAT MAX_MP_SAVED_VEHICLES i
				IF (i != iSlot)
					IF (g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT)
						IF (g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel = ModelName)
							IF ArePlateHashesEqual(tlPlate, g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.tlPlateText)
								
								#IF IS_DEBUG_BUILD
									INT iHashPlate = GET_HASH_KEY(tlPlate) 
									PRINTLN("[platedupes] IS_PLATE_DUPE_OF_CAR_IN_GARAGE - true for ", tlPlate, " saved slot ", i, ", iSlot = ", iSlot, " hash ", iHashPlate)
								#ENDIF
													
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ELSE
			PRINTLN("[platedupes] IS_PLATE_DUPE_OF_CAR_IN_GARAGE - is not ambient plate format.")
		ENDIF
	ELSE
		PRINTLN("[platedupes] IS_PLATE_DUPE_OF_CAR_IN_GARAGE  disabled")
	ENDIF
	
	RETURN FALSE
	
ENDFUNC





FUNC BOOL HAS_PLATE_BEEN_RECENTLY_USED_IN_PERSONALISED_PLATE_OPTION(TEXT_LABEL_15 tlPlate, BOOL bCheckGlobals=TRUE)

	IF NOT (g_sMPTunables.bEnabled_DUPE_DETECTOR)
		PRINTLN("[platedupes] HAS_PLATE_BEEN_RECENTLY_USED_IN_PERSONALISED_PLATE_OPTION - dupe detector disabled")
		RETURN FALSE
	ENDIF

	IF ( g_sMPTunables.bEnabled_HAS_PLATE_BEEN_RECENTLY_USED_IN_PERSONALISED_PLATE_OPTION)
		INT i
		INT iHashPlate = GET_HASH_KEY(tlPlate)
		REPEAT TOTAL_STORED_STATS_PLATES_PERSONALISED i
			IF (g_iPersonalisedCarPlateStats[i] = iHashPlate)
				PRINTLN("[platedupes] HAS_PLATE_BEEN_RECENTLY_USED_IN_PERSONALISED_PLATE_OPTION  true for '", tlPlate, "', stat i = ", i, " hash ", iHashPlate)				
				RETURN TRUE
			ENDIF
		ENDREPEAT
		IF (bCheckGlobals)
			IF IsInValueInGlobalArray(g_iPersonalisedCarPlateGlobals, iHashPlate)
				PRINTLN("[platedupes] HAS_PLATE_BEEN_RECENTLY_USED_IN_PERSONALISED_PLATE_OPTION  true for '", tlPlate, "', global, hash ", iHashPlate)				
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[platedupes] HAS_PLATE_BEEN_RECENTLY_USED_IN_PERSONALISED_PLATE_OPTION  disabled")		
	ENDIF
	RETURN FALSE
ENDFUNC

PROC ADD_PLATE_TO_RECENTLY_USED_PERSONALISED_PLATE_LIST(TEXT_LABEL_15 tlPlate)
	
	IF NOT (g_sMPTunables.bEnabled_DUPE_DETECTOR)
		PRINTLN("[platedupes] ADD_PLATE_TO_RECENTLY_USED_PERSONALISED_PLATE_LIST - dupe detector disabled")
		EXIT
	ENDIF	
	
	IF IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER(tlPlate)
		PRINTLN("[platedupes] ADD_PLATE_TO_RECENTLY_USED_PERSONALISED_PLATE_LIST - this is a personalised plate, not adding to list")
		EXIT
	ENDIF
	
	IF ( g_sMPTunables.bEnabled_ADD_PLATE_TO_RECENTLY_USED_PERSONALISED_PLATE_LIST)

		INT iHashPlate = GET_HASH_KEY(tlPlate)
		
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[platedupes] ADD_PLATE_TO_RECENTLY_USED_PERSONALISED_PLATE_LIST  called with '", tlPlate, "', hash = ", iHashPlate)	
		
		// stats
		IF NOT HAS_PLATE_BEEN_RECENTLY_USED_IN_PERSONALISED_PLATE_OPTION(tlPlate, FALSE)

			// stats
			ShiftRecentlyUsedPersonalisedPlateStatsListUp()
			SetRecentlyUsedPersonalisedPlateStatValue(TOTAL_STORED_STATS_PLATES_PERSONALISED-1, iHashPlate) // newest added at bottom.
		
		ENDIF

		// globals
		AddValueToGlobalArray(g_iPersonalisedCarPlateGlobals, iHashPlate)
		
		// print
		#IF IS_DEBUG_BUILD
			INT i
			REPEAT TOTAL_STORED_STATS_PLATES_PERSONALISED i
				PRINTLN("[platedupes] ADD_PLATE_TO_RECENTLY_USED_PERSONALISED_PLATE_LIST  g_iPersonalisedCarPlateStats[", i, "] = ", g_iPersonalisedCarPlateStats[i])		
			ENDREPEAT
			REPEAT TOTAL_STORED_GLOBAL_PLATES_PERSONALISED i
				PRINTLN("[platedupes] ADD_PLATE_TO_RECENTLY_USED_PERSONALISED_PLATE_LIST  g_iPersonalisedCarPlateGlobals[", i, "] = ", g_iPersonalisedCarPlateGlobals[i])		
			ENDREPEAT
		#ENDIF

	ELSE
		PRINTLN("[platedupes] ADD_PLATE_TO_RECENTLY_USED_PERSONALISED_PLATE_LIST  disabled")		
	ENDIF
	
ENDPROC



FUNC BOOL HAS_PLATE_BEEN_RECENTLY_SOLD(TEXT_LABEL_15 tlPlate, BOOL bCheckGlobals=TRUE)

	IF NOT (g_sMPTunables.bEnabled_DUPE_DETECTOR)
		PRINTLN("[platedupes] HAS_PLATE_BEEN_RECENTLY_SOLD - dupe detector disabled")
		RETURN FALSE
	ENDIF	

	IF ( g_sMPTunables.bEnabled_HAS_PLATE_BEEN_RECENTLY_SOLD)
		INT i
		INT iHashPlate = GET_HASH_KEY(tlPlate)
		REPEAT TOTAL_STORED_STATS_PLATES_SOLD i
			IF (g_iSoldCarPlateStats[i] = iHashPlate)
				PRINTLN("[platedupes] HAS_PLATE_BEEN_RECENTLY_SOLD  true for '", tlPlate, "', stat i = ", i, " hash ", iHashPlate)				
				RETURN TRUE
			ENDIF
		ENDREPEAT
		IF (bCheckGlobals)
			IF IsInValueInGlobalArray(g_iSoldCarPlateGlobals, iHashPlate)
				PRINTLN("[platedupes] HAS_PLATE_BEEN_RECENTLY_SOLD  true for '", tlPlate, "', global, hash ", iHashPlate)				
				RETURN TRUE
			ENDIF	
		ENDIF
	ELSE
		PRINTLN("[platedupes] HAS_PLATE_BEEN_RECENTLY_SOLD  disabled")	
	ENDIF
	RETURN FALSE
ENDFUNC

PROC ADD_PLATE_TO_RECENTLY_SOLD_PLATE_LIST(TEXT_LABEL_15 tlPlate)
	
	IF NOT (g_sMPTunables.bEnabled_DUPE_DETECTOR)
		PRINTLN("[platedupes] ADD_PLATE_TO_RECENTLY_SOLD_PLATE_LIST - dupe detector disabled")
		EXIT
	ENDIF	
	
	IF IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER(tlPlate)
		PRINTLN("[platedupes] ADD_PLATE_TO_RECENTLY_SOLD_PLATE_LIST - this is a personalised plate, not adding to list")
		EXIT
	ENDIF	
	
	IF ( g_sMPTunables.bEnabled_ADD_PLATE_TO_RECENTLY_SOLD_PLATE_LIST)
					
		INT iHashPlate = GET_HASH_KEY(tlPlate)
		
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[platedupes] ADD_PLATE_TO_RECENTLY_SOLD_PLATE_LIST  called with '", tlPlate, "', hash = ", iHashPlate)
			
		IF NOT HAS_PLATE_BEEN_RECENTLY_SOLD(tlPlate, FALSE)		
			// stats
			ShiftRecentlySoldPlateListUp()
			SetRecentlySoldPlateStatValue(TOTAL_STORED_STATS_PLATES_SOLD-1, iHashPlate)					
		ENDIF
			
		// globals
		AddValueToGlobalArray(g_iSoldCarPlateGlobals, iHashPlate)
			
		// print
		#IF IS_DEBUG_BUILD
			INT i
			REPEAT TOTAL_STORED_STATS_PLATES_SOLD i
				PRINTLN("[platedupes] ADD_PLATE_TO_RECENTLY_SOLD_PLATE_LIST  g_iSoldCarPlateStats[", i, "] = ", g_iSoldCarPlateStats[i])		
			ENDREPEAT
			REPEAT TOTAL_STORED_GLOBAL_PLATES_SOLD i
				PRINTLN("[platedupes] ADD_PLATE_TO_RECENTLY_SOLD_PLATE_LIST  g_iSoldCarPlateGlobals[", i, "] = ", g_iSoldCarPlateGlobals[i])		
			ENDREPEAT
		#ENDIF
			
	ELSE
		PRINTLN("[platedupes] ADD_PLATE_TO_RECENTLY_SOLD_PLATE_LIST  disabled")	
	ENDIF
		
ENDPROC



FUNC FLOAT GET_KNOWN_DUPE_PRICE_MULTIPLIER(INT iNumberSold)
	SWITCH iNumberSold
		CASE 0 RETURN  g_sMPTunables.fKnownDupeMultiplierLevel0 
		CASE 1 RETURN  g_sMPTunables.fKnownDupeMultiplierLevel1
		CASE 2 RETURN  g_sMPTunables.fKnownDupeMultiplierLevel2
		CASE 3 RETURN  g_sMPTunables.fKnownDupeMultiplierLevel3
		CASE 4 RETURN  g_sMPTunables.fKnownDupeMultiplierLevel4
		CASE 5 RETURN  g_sMPTunables.fKnownDupeMultiplierLevel5
	ENDSWITCH
	RETURN  g_sMPTunables.fKnownDupeMultiplierLevel5
ENDFUNC

FUNC INT GET_NUMBER_OF_KNOWN_DUPES_SOLD_FOR_VEHICLE(VEHICLE_INDEX VehicleID)
	TEXT_LABEL_15 tlPlate = GET_VEHICLE_NUMBER_PLATE_TEXT(VehicleID)
	RETURN GetStoredKnownDupeSoldForPlate(tlPlate)
ENDFUNC

FUNC BOOL IS_VEHICLE_A_KNOWN_DUPE(VEHICLE_INDEX VehicleID, INT iNumberAllowedToBeSold, BOOL bCheckGlobals=TRUE)
	TEXT_LABEL_15 tlPlate = GET_VEHICLE_NUMBER_PLATE_TEXT(VehicleID)
	RETURN IS_PLATE_A_KNOWN_DUPE(tlPlate, iNumberAllowedToBeSold, bCheckGlobals)
ENDFUNC

PROC IncrementKnownDupeSoldForPlate(TEXT_LABEL_15 tlPlate)
	INT i
	INT iHashPlate = GET_HASH_KEY(tlPlate)
	REPEAT TOTAL_STORED_STATS_KNOWN_DUPES i
		IF (g_iKnownDupePlateStats[i] = iHashPlate)			
			INT iSold = GetKnownDupeSoldStatValue(i)		
			SetKnownDupeSoldStatValue(i, iSold+1)			
		ENDIF
	ENDREPEAT	
ENDPROC


PROC ADD_PLATE_TO_KNOWN_DUPE_PLATE_LIST(TEXT_LABEL_15 tlPlate, INT iSold=1)
	
	IF NOT (g_sMPTunables.bEnabled_DUPE_DETECTOR)
		PRINTLN("[platedupes] ADD_PLATE_TO_KNOWN_DUPE_PLATE_LIST - dupe detector disabled")
		EXIT
	ENDIF	
	
	IF IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER(tlPlate)
		PRINTLN("[platedupes] ADD_PLATE_TO_KNOWN_DUPE_PLATE_LIST - this is a personalised plate, not adding to list")
		EXIT
	ENDIF	
	
	IF ( g_sMPTunables.bEnabled_ADD_PLATE_TO_KNOWN_DUPE_PLATE_LIST)

		INT iHashPlate = GET_HASH_KEY(tlPlate)
		
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[platedupes] ADD_PLATE_TO_KNOWN_DUPE_PLATE_LIST  called with '", tlPlate, "', hash = ", iHashPlate)
				
		IF NOT IS_PLATE_A_KNOWN_DUPE(tlPlate, 0)
			PRINTLN("[platedupes] ADD_PLATE_TO_KNOWN_DUPE_PLATE_LIST  not on list, so adding, iSold = ", iSold)
			// stats
			ShiftKnownDupePlateListUp()
			ShiftKnownDupeSoldListUp()
			SetKnownDupePlateStatValue(TOTAL_STORED_STATS_KNOWN_DUPES-1, iHashPlate)
			SetKnownDupeSoldStatValue(TOTAL_STORED_STATS_KNOWN_DUPES-1, iSold)			
		ELSE
			// increment
			PRINTLN("[platedupes] ADD_PLATE_TO_KNOWN_DUPE_PLATE_LIST  already on list, so incrementing.")
			IncrementKnownDupeSoldForPlate(tlPlate)
		ENDIF
			
		// globals
		AddValueToGlobalArray(g_iKnownDupePlateGlobals, iHashPlate)
		
		// update sold value in global array
		INT i
		REPEAT COUNT_OF(g_iKnownDupePlateGlobals) i
			IF (g_iKnownDupePlateGlobals[i] = iHashPlate)
				g_iKnownDupeSoldGlobals[i] = GetStoredKnownDupeSoldForPlate(tlPlate)
			ENDIF
		ENDREPEAT
		
			
		// print
		#IF IS_DEBUG_BUILD
			REPEAT TOTAL_STORED_STATS_KNOWN_DUPES i
				PRINTLN("[platedupes] ADD_PLATE_TO_KNOWN_DUPE_PLATE_LIST  g_iKnownDupePlateStats[", i, "] = ", g_iKnownDupePlateStats[i], ", g_iKnownDupeSoldStats[", i, "] = ", g_iKnownDupeSoldStats[i])		
			ENDREPEAT
			REPEAT TOTAL_STORED_GLOBAL_PLATES_KNOWN_DUPES i
				PRINTLN("[platedupes] ADD_PLATE_TO_KNOWN_DUPE_PLATE_LIST  g_iKnownDupePlateGlobals[", i, "] = ", g_iKnownDupePlateGlobals[i], ", g_iKnownDupeSoldGlobals[", i, "] = ", g_iKnownDupeSoldGlobals[i])			
			ENDREPEAT
		#ENDIF
		
	ELSE
		PRINTLN("[platedupes] ADD_PLATE_TO_KNOWN_DUPE_PLATE_LIST  disabled")	
	ENDIF
		
ENDPROC

PROC ADD_VEHICLE_TO_KNOWN_DUPE_PLATE_LIST(VEHICLE_INDEX VehicleID)
	TEXT_LABEL_15 tlPlate = GET_VEHICLE_NUMBER_PLATE_TEXT(VehicleID) 
	ADD_PLATE_TO_KNOWN_DUPE_PLATE_LIST(tlPlate)
ENDPROC


FUNC BOOL IS_VEHICLE_A_GARAGE_DUPE(VEHICLE_INDEX VehicleID, BOOL bAllowOriginal, INT iPVSlotOverride=-1)
	
	IF NOT (g_sMPTunables.bEnabled_DUPE_DETECTOR)
		PRINTLN("[platedupes] IS_VEHICLE_A_GARAGE_DUPE - dupe detector disabled")
		RETURN FALSE
	ENDIF		
	
	IF ( g_sMPTunables.bEnabled_IS_VEHICLE_A_GARAGE_DUPE)
	
		INT iPVSlot
		TEXT_LABEL_15 tlPlate = GET_VEHICLE_NUMBER_PLATE_TEXT(VehicleID) 
		MODEL_NAMES ModelName = GET_ENTITY_MODEL(VehicleID)
		
		#IF IS_DEBUG_BUILD
			IF (iPVSlotOverride > -1)
				PRINTLN("[platedupes] IS_VEHICLE_A_GARAGE_DUPE iPVSlotOverride = ", iPVSlotOverride)	
			ENDIF
		#ENDIF
		
		iPVSlot = iPVSlotOverride
		IF (VehicleID = PERSONAL_VEHICLE_ID())
			iPVSlot = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed	
		ENDIF

		IF (iPVSlot > -1)

			IF NOT (bAllowOriginal)
				IF IS_PLATE_DUPE_OF_CAR_IN_GARAGE(tlPlate, ModelName, iPVSlot)
					PRINTLN("[platedupes] IS_VEHICLE_A_GARAGE_DUPE returning TRUE 1.")
					RETURN TRUE
				ENDIF
			ELSE
				IF IS_PLATE_DUPE_OF_CAR_IN_GARAGE(tlPlate, ModelName, iPVSlot)
//					IF (GetStoredKnownDupeSoldForPlate(tlPlate) >= iNumberAllowedToBeSold)
//						PRINTLN("[platedupes] IS_VEHICLE_A_GARAGE_DUPE returning TRUE - already sold limit of know dupes. limit is ", iNumberAllowedToBeSold)
//					ENDIF
//				
					IF NOT IS_EARLIEST_TIMESTAMP_VEHICLE_IN_GARAGE_WITH_PLATE(tlPlate, iPVSlot)	
						PRINTLN("[platedupes] IS_VEHICLE_A_GARAGE_DUPE returning TRUE - this is not the original dupe.")
						RETURN TRUE
					ENDIF	
				ENDIF
			ENDIF		
			
		ELSE
			IF IS_PLATE_DUPE_OF_CAR_IN_GARAGE(tlPlate, ModelName)
				PRINTLN("[platedupes] IS_VEHICLE_A_GARAGE_DUPE returning TRUE 2. Not a pv, but PV exists in garage with this plate. '", tlPlate, "'")
				RETURN TRUE
			ENDIF
		ENDIF	

		
	ELSE
		PRINTLN("[platedupes] IS_VEHICLE_A_GARAGE_DUPE not enabled ")
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC INT COUNT_FREE_KNOWN_DUPE_SLOTS()
	INT i
	INT iCount
	REPEAT TOTAL_STORED_STATS_KNOWN_DUPES i
		IF g_iKnownDupePlateStats[i] = 0
			iCount++	
		ENDIF
	ENDREPEAT
	PRINTLN("[platedupes] COUNT_FREE_KNOWN_DUPE_SLOTS returning ", iCount)
	RETURN iCount
ENDFUNC

FUNC BOOL IS_KNOWN_DUPE_LIST_EMPTY()
	IF COUNT_FREE_KNOWN_DUPE_SLOTS() = TOTAL_STORED_STATS_KNOWN_DUPES
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL HAS_PLAYER_PROCESSED_AN_IFRUIT_ORDER()
	IF (g_savedMPGlobalsNew.g_savedMPGlobals[0].MpSavedCarApp.iOrderCount > 0)
	#IF IS_DEBUG_BUILD
	OR (g_bHasProcessedIFruitOrder)
	#ENDIF
	
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC STORE_FREE_IFRUIT_PLATE()

	g_iStoredIFruitPlate = GET_MP_INT_PLAYER_STAT(MPPLY_IFPUMA)
	PRINTLN("[platedupes] STORE_FREE_IFRUIT_PLATE - setting from stat, g_iStoredIFruitPlate = ", g_iStoredIFruitPlate)

	IF (g_iStoredIFruitPlate = 0)
		IF IS_KNOWN_DUPE_LIST_EMPTY()
		
			INT i
			INT iHashPlate
			
			// check existing plates in gararge and remove those with 'free' plate format from the known dupe list
			REPEAT MAX_MP_SAVED_VEHICLES i
				IF (g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT)
					IF IS_FREE_PLATE_FORMAT(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.tlPlateText)						
						iHashPlate = GET_HASH_KEY(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.tlPlateText)
						g_iStoredIFruitPlate = iHashPlate						
						PRINTLN("[platedupes] STORE_FREE_IFRUIT_PLATE found free plate '", g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.tlPlateText, "' hash ", iHashPlate)
						SET_MP_INT_PLAYER_STAT(MPPLY_IFPUMA, g_iStoredIFruitPlate)
						EXIT
					ENDIF
				ENDIF
			ENDREPEAT
			
			// if no orders have been processed byg there is a plate in the sc list and it's free format then add
			IF NOT HAS_PLAYER_PROCESSED_AN_IFRUIT_ORDER()
				REPEAT MAX_NUMBER_OF_SC_LICENSE_PLATES i
					IF IS_FREE_PLATE_FORMAT(g_tlPlateTextFromSCAccount[i])
						iHashPlate = GET_HASH_KEY(g_tlPlateTextFromSCAccount[i])
						g_iStoredIFruitPlate = iHashPlate	
						PRINTLN("[platedupes] STORE_FREE_IFRUIT_PLATE found free plate from g_tlPlateTextFromSCAccount[", i, "] '", g_tlPlateTextFromSCAccount[i], "' hash ", iHashPlate)
						SET_MP_INT_PLAYER_STAT(MPPLY_IFPUMA, g_iStoredIFruitPlate)
						EXIT
					ENDIF
				ENDREPEAT
			ENDIF
				
			g_iStoredIFruitPlate = -1	
			PRINTLN("[platedupes] STORE_FREE_IFRUIT_PLATE - no free plates in garage or g_tlPlateTextFromSCAccount ")
			
		ELSE
			g_iStoredIFruitPlate = -1	
			PRINTLN("[platedupes] STORE_FREE_IFRUIT_PLATE - known dupe list not empty. ")
		ENDIF
		
		SET_MP_INT_PLAYER_STAT(MPPLY_IFPUMA, g_iStoredIFruitPlate)
		
	ELSE
		PRINTLN("[platedupes] STORE_FREE_IFRUIT_PLATE - g_iStoredIFruitPlate = ", g_iStoredIFruitPlate)
	ENDIF
ENDPROC

PROC CLEAN_DUPE_LISTS()
	
	INT i
	INT iHashPlate
	TEXT_LABEL_15 tlPlate
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[platedupes] --- CLEAN_DUPE_LISTS ---- START ----")
	OUTPUT_ALL_DUPE_DETECT_VALUES()	
	#ENDIF
	
	// check current personal plates, if any are not ambient then make sure they are not on the known dupe list
	IF IS_PLAYERS_SC_NUMBER_PLATES_READY()
	
		REPEAT MAX_NUMBER_OF_SC_LICENSE_PLATES i
			IF g_bPlateTextFromSCAccountInMPList[i]
				IF NOT IS_AMBIENT_PLATE_FORMAT(g_tlPlateTextFromSCAccount[i])
					iHashPlate = GET_HASH_KEY(g_tlPlateTextFromSCAccount[i]) 			
					PRINTLN("[platedupes] CLEAN_DUPE_LISTS - personal plate '", g_tlPlateTextFromSCAccount[i], "' hash ", iHashPlate)
					RemoveHashPlateFromKnownDupeList(iHashPlate)
					RemoveHashPlateFromRecentlySoldList(iHashPlate)
					RemoveHashPlateFromRecentlyUsedPersonalisedList(iHashPlate)
				ENDIF
			ENDIF
		ENDREPEAT
	
	ELSE
		PRINTLN("[platedupes] CLEAN_DUPE_LISTS - IS_PLAYERS_SC_NUMBER_PLATES_READY = FALSE")
	ENDIF
	
	// check car app plate
	tlPlate = g_savedGlobals.sSocialData.tlCarAppPlateText
	Pad_Plate_To_8_Characters(tlPlate)
	IF NOT IS_AMBIENT_PLATE_FORMAT(tlPlate)
		iHashPlate = GET_HASH_KEY(tlPlate) 			
		PRINTLN("[platedupes] CLEAN_DUPE_LISTS - app plate '", tlPlate, "' hash ", iHashPlate)
		RemoveHashPlateFromKnownDupeList(iHashPlate)
		RemoveHashPlateFromRecentlySoldList(iHashPlate)
		RemoveHashPlateFromRecentlyUsedPersonalisedList(iHashPlate)
	ENDIF
	
	
	// check existing plates in gararge and remove those with 'free' plate format from the known dupe list
	REPEAT MAX_MP_SAVED_VEHICLES i
		IF (g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT)
			IF IS_FREE_PLATE_FORMAT(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.tlPlateText)
				iHashPlate = GET_HASH_KEY(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.tlPlateText) 	
				PRINTLN("[platedupes] CLEAN_DUPE_LISTS - garage plate '", g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.tlPlateText, "' hash ", iHashPlate)
				RemoveHashPlateFromKnownDupeList(iHashPlate)
				RemoveHashPlateFromRecentlySoldList(iHashPlate)
				RemoveHashPlateFromRecentlyUsedPersonalisedList(iHashPlate)
			ENDIF
		ENDIF
	ENDREPEAT
	
	// check the free ifruit plate
	IF NOT (g_iStoredIFruitPlate = 0)
	AND NOT (g_iStoredIFruitPlate = -1)
		PRINTLN("[platedupes] CLEAN_DUPE_LISTS - free ifruit plate hash ", g_iStoredIFruitPlate)
		RemoveHashPlateFromKnownDupeList(g_iStoredIFruitPlate)
		RemoveHashPlateFromRecentlySoldList(g_iStoredIFruitPlate)
		RemoveHashPlateFromRecentlyUsedPersonalisedList(g_iStoredIFruitPlate)
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[platedupes] --- CLEAN_DUPE_LISTS ---- END ----")
	OUTPUT_ALL_DUPE_DETECT_VALUES()	
	#ENDIF
		
	
	// check duplicate entries in the lists
	RemoveDuplicateHashPlateEntriesFromKnownLists()
	

ENDPROC


PROC GET_GARAGE_KNOWN_DUPES()

	// go through each plate in garage
	IF IS_PLAYERS_SC_NUMBER_PLATES_READY()
		INT i
		REPEAT MAX_MP_SAVED_VEHICLES i
			IF (g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT)
				
				PRINTLN("[platedupes] GET_GARAGE_KNOWN_DUPES - checking slot ", i, ", plate ", g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.tlPlateText)
				IF NOT IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.tlPlateText)
					IF NOT IS_PLATE_A_KNOWN_DUPE(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.tlPlateText, 0, FALSE)	
						IF IS_PLATE_DUPE_OF_CAR_IN_GARAGE(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.tlPlateText, g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel, i)									
							PRINTLN("[platedupes] GET_GARAGE_KNOWN_DUPES - found non-known dupe - ", g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.tlPlateText)									
							IF COUNT_FREE_KNOWN_DUPE_SLOTS() >= g_iRequiredSpacesInKnownDupeListForGarageScan
								ADD_PLATE_TO_KNOWN_DUPE_PLATE_LIST(g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.tlPlateText, 0)	
								PRINTLN("[platedupes] GET_GARAGE_KNOWN_DUPES - added non-known dupe - ", g_MpSavedVehicles[i].VehicleSetupMP.VehicleSetup.tlPlateText)
							ELSE										
								PRINTLN("[platedupes] GET_GARAGE_KNOWN_DUPES - too many dupes already on list. ")											
							ENDIF
						ELSE
							PRINTLN("[platedupes] GET_GARAGE_KNOWN_DUPES - not matching any other plates in garage")	
						ENDIF
					ELSE
						PRINTLN("[platedupes] GET_GARAGE_KNOWN_DUPES - is a known dupe already")	
					ENDIF
				ELSE
					PRINTLN("[platedupes] GET_GARAGE_KNOWN_DUPES - is a personal plate")		
				ENDIF
			ENDIF
		ENDREPEAT		
	ELSE
		PRINTLN("[platedupes] GET_GARAGE_KNOWN_DUPES - waiting for IS_PLAYERS_SC_NUMBER_PLATES_READY")				
	ENDIF
	#IF IS_DEBUG_BUILD
	PRINTLN("[platedupes] --- GET_GARAGE_KNOWN_DUPES ---- END ----")
	OUTPUT_ALL_DUPE_DETECT_VALUES()	
	#ENDIF


ENDPROC

PROC INIT_PLATE_DUPE_DATA()

	INT i
	REPEAT NUMBER_OF_HIGH_MOD_VALUE_MODELS i
		g_HighModValueModels[i] = DUMMY_MODEL_FOR_SCRIPT
	ENDREPEAT

	g_HighModValueModels[0] = DUBSTA2
	g_HighModValueModels[1] = RATLOADER
	g_HighModValueModels[2] = DOMINATOR
	g_HighModValueModels[3] = SANDKING
	g_HighModValueModels[4] = SENTINEL
	g_HighModValueModels[5] = SABREGT
	g_HighModValueModels[6] = RUINER
	g_HighModValueModels[7] = GAUNTLET

	// store initial stat values into globals
	REPEAT TOTAL_STORED_STATS_PLATES_PERSONALISED i
		g_iPersonalisedCarPlateStats[i] = GetRecentlyUsedPersonalisedPlateStatValue(i)
	ENDREPEAT
	REPEAT TOTAL_STORED_STATS_PLATES_SOLD i
		g_iSoldCarPlateStats[i] = GetRecentlySoldPlateStatValue(i)
	ENDREPEAT 
	REPEAT TOTAL_STORED_STATS_KNOWN_DUPES i 
		g_iKnownDupePlateStats[i] = GetKnownDupePlateStatValue(i)
		g_iKnownDupeSoldStats[i] = GetKnownDupeSoldStatValue(i)
	ENDREPEAT
	REPEAT NUMBER_OF_VEHICLES_IN_METRIC_SENT i
		g_iVehicleIDMetricSent[i] = -1
	ENDREPEAT
	
	g_bOptOutPlayerFromDupeDetector = GET_MP_BOOL_PLAYER_STAT(MPPLY_DISABLEDOZERDETECT)
	#IF IS_DEBUG_BUILD
	IF (g_bOptOutPlayerFromDupeDetector) 
		PRINTLN("[platedupes] INIT_PLATE_DUPE_DATA  g_bOptOutPlayerFromDupeDetector = TRUE")	
	ENDIF
	#ENDIF
	

	#IF IS_DEBUG_BUILD	
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_disableDupeDetector")
			g_sMPTunables.bEnabled_DUPE_DETECTOR = FALSE
			PRINTLN("[platedupes] INIT_PLATE_DUPE_DATA  disabling dupe detector due to command line.")
		ENDIF
	#ENDIF




	PRINTLN("[platedupes] INIT_PLATE_DUPE_DATA  done")
	
	

ENDPROC


FUNC BOOL IS_VEHICLE_A_SUSPECTED_DUPE(VEHICLE_INDEX VehicleID, INT iPVSlotOverride, DD_REASON &eReason)

	IF NOT (g_sMPTunables.bEnabled_DUPE_DETECTOR)
		PRINTLN("[platedupes] IS_VEHICLE_A_SUSPECTED_DUPE - dupe detector disabled")
		RETURN FALSE
	ENDIF	

	IF ( g_sMPTunables.bEnabled_IS_VEHICLE_A_SUSPECTED_DUPE)
		IF DOES_ENTITY_EXIST(VehicleID)
		AND NOT IS_ENTITY_DEAD(VehicleID)
		
			TEXT_LABEL_15 tlPlate = GET_VEHICLE_NUMBER_PLATE_TEXT(VehicleID) 

			IF NOT IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER(tlPlate)
						
				IF IS_VEHICLE_A_GARAGE_DUPE(VehicleID, FALSE, iPVSlotOverride)
					PRINTLN("[platedupes] IS_VEHICLE_A_SUSPECTED_DUPE - garage dupe")
					eReason = DD_REASON_PLATE_MATCHES_CAR_IN_GARAGE
					RETURN TRUE
				ENDIF
				
				IF HAS_PLATE_BEEN_RECENTLY_USED_IN_PERSONALISED_PLATE_OPTION(tlPlate)
					PRINTLN("[platedupes] IS_VEHICLE_A_SUSPECTED_DUPE - plate has been recently personalised")
					eReason = DD_REASON_PLATE_HAS_BEEN_RECENTLY_PERSONALISED
					RETURN TRUE
				ENDIF
				
				IF HAS_PLATE_BEEN_RECENTLY_SOLD(tlPlate)
					PRINTLN("[platedupes] IS_VEHICLE_A_SUSPECTED_DUPE - plate has been recently sold")
					eReason = DD_REASON_HAS_BEEN_RECENTLY_SOLD
					RETURN TRUE
				ENDIF
				
				IF IS_PLATE_A_KNOWN_DUPE(tlPlate, 0)					
					PRINTLN("[platedupes] IS_VEHICLE_A_SUSPECTED_DUPE - plate is a known dupe")
					eReason = DD_REASON_IS_A_KNOWN_DUPE
					RETURN TRUE					
				ENDIF
			
			ENDIF
		
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ENSURE_PV_PLATE_MATCHES_SAVED_DATA(VEHICLE_INDEX VehicleID)
	
	IF NOT (g_sMPTunables.bEnabled_DUPE_DETECTOR)
		PRINTLN("[platedupes] ENSURE_PV_PLATE_MATCHES_SAVED_DATA - dupe detector disabled")
		EXIT
	ENDIF	
	
	IF ( g_sMPTunables.bEnabled_ENSURE_PV_PLATE_MATCHES_SAVED_DATA)
		IF (VehicleID = PERSONAL_VEHICLE_ID())
			
			TEXT_LABEL_15 tlPlate = GET_VEHICLE_NUMBER_PLATE_TEXT(VehicleID) 
			INT iPVSlot = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed
			
			IF iPVSlot != -1
				IF NOT ArePlateHashesEqual(tlPlate, g_MpSavedVehicles[iPVSlot].VehicleSetupMP.VehicleSetup.tlPlateText)
					PRINTLN("[platedupes] ENSURE_PV_PLATE_MATCHES_SAVED_DATA - plate not matching save data, changing from ", tlPlate, " to ", g_MpSavedVehicles[iPVSlot].VehicleSetupMP.VehicleSetup.tlPlateText)	
					BOOL bMissionEnt
					IF CAN_EDIT_THIS_ENTITY(VehicleID, bMissionEnt)		
						SET_VEHICLE_NUMBER_PLATE_TEXT(VehicleID, g_MpSavedVehicles[iPVSlot].VehicleSetupMP.VehicleSetup.tlPlateText)
						g_VehicleIDPlateDupeCheck = INT_TO_NATIVE(VEHICLE_INDEX, -1)
					ELSE
						PRINTLN("[platedupes] ENSURE_PV_PLATE_MATCHES_SAVED_DATA - cant edit")	
					ENDIF
					IF (bMissionEnt)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
					ENDIF									
				ENDIF
			ELSE	
				//SCRIPT_ASSERT("ENSURE_PV_PLATE_MATCHES_SAVED_DATA - iPVSlot = -1 !!!")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


FUNC BOOL ShouldIgnorePlateDupeCheck()
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)	
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP_OF_TYPE(SHOP_TYPE_PERSONAL_CARMOD)
	OR NETWORK_IS_ACTIVITY_SESSION()
	OR (g_bOverride_ShouldIgnorePlateDupeCheck)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_NUMBER_OF_GARAGE_SPACES()
	INT iCount
	INT i
	iCount = 0
	REPEAT MAX_OWNED_PROPERTIES i
	      IF IS_PROPERTY_SLOT_NO_PER_VEHS_PROPERTY(i)
	            // nothing
	      ELIF GET_OWNED_PROPERTY(i) >= COUNT_OF(mpProperties)
	            // nothing
	      ELIF GET_OWNED_PROPERTY(i) > 0
	      AND GET_OWNED_PROPERTY(i) <= MAX_MP_PROPERTIES
	            iCount += mpProperties[GET_OWNED_PROPERTY(i)].iGarageSize
	      ENDIF
	ENDREPEAT
	RETURN iCount
ENDFUNC

// ====== list handling functions ======

FUNC BOOL DECORATORS_SHOW_VEHICLE_BELONGS_TO_ANOTHER_PLAYER(VEHICLE_INDEX VehicleID)
	INT iValue
	IF DOES_ENTITY_EXIST(VehicleID)
	AND NOT IS_ENTITY_DEAD(VehicleID)
	
		IF DECOR_EXIST_ON(VehicleID,"Player_Vehicle")		
			iValue = DECOR_GET_INT(VehicleID, "Player_Vehicle")
			IF (iValue != -1)
			AND (iValue != NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
				PRINTLN("[platedupes] DECORATORS_SHOW_VEHICLE_BELONGS_TO_ANOTHER_PLAYER - pv belongs to player hash ", iValue)				
				RETURN TRUE	
			ENDIF
		ENDIF
		
		IF DECOR_EXIST_ON(VehicleID,"Veh_Modded_By_Player")		
			iValue = DECOR_GET_INT(VehicleID, "Veh_Modded_By_Player")
			IF (iValue != -1)
			AND (iValue != NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
				PRINTLN("[platedupes] DECORATORS_SHOW_VEHICLE_BELONGS_TO_ANOTHER_PLAYER - modded by player hash ", iValue)	
				RETURN TRUE	
			ENDIF
		ENDIF		
		
		IF DECOR_EXIST_ON(VehicleID,"Player_Truck")		
			iValue = DECOR_GET_INT(VehicleID, "Player_Truck")
			IF (iValue != -1)
			AND (iValue != NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
				PRINTLN("[platedupes] DECORATORS_SHOW_VEHICLE_BELONGS_TO_ANOTHER_PLAYER - pv truck belongs to player hash ", iValue)				
				RETURN TRUE	
			ENDIF
		ENDIF
		
		IF DECOR_EXIST_ON(VehicleID,"Player_Avenger")		
			iValue = DECOR_GET_INT(VehicleID, "Player_Avenger")
			IF (iValue != -1)
			AND (iValue != NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
				PRINTLN("[platedupes] DECORATORS_SHOW_VEHICLE_BELONGS_TO_ANOTHER_PLAYER - pv avenger belongs to player hash ", iValue)				
				RETURN TRUE	
			ENDIF
		ENDIF	
		
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DECORATORS_SHOW_VEHICLE_NON_SAVEABLE(VEHICLE_INDEX VehicleID)
	IF DOES_ENTITY_EXIST(VehicleID)
	AND NOT IS_ENTITY_DEAD(VehicleID)
		IF DECOR_EXIST_ON(VehicleID,"Not_Allow_As_Saved_Veh")					
			RETURN TRUE	
		ENDIF	
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLATE_A_SUSPECTED_DUPE(TEXT_LABEL_15 tlPlate, MODEL_NAMES ModelName, INT iPVSlot)
	
	IF NOT (g_sMPTunables.bEnabled_DUPE_DETECTOR)
		PRINTLN("[platedupes] IS_PLATE_A_SUSPECTED_DUPE - dupe detector disabled")
		RETURN FALSE
	ENDIF	
	
	Pad_Plate_To_8_Characters(tlPlate)

	IF NOT IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER(tlPlate)
				
		IF IS_PLATE_DUPE_OF_CAR_IN_GARAGE(tlPlate, ModelName, iPVSlot)
			PRINTLN("[platedupes] IS_PLATE_A_SUSPECTED_DUPE returning TRUE 1. '", tlPlate, "'")
			RETURN TRUE
		ENDIF

		IF HAS_PLATE_BEEN_RECENTLY_USED_IN_PERSONALISED_PLATE_OPTION(tlPlate)
			PRINTLN("[platedupes] IS_PLATE_A_SUSPECTED_DUPE - plate has been recently personalised '", tlPlate, "'")
			RETURN TRUE
		ENDIF
		
		IF HAS_PLATE_BEEN_RECENTLY_SOLD(tlPlate)
			PRINTLN("[platedupes] IS_PLATE_A_SUSPECTED_DUPE - plate has been recently sold '", tlPlate, "'")
			RETURN TRUE
		ENDIF
		
		IF IS_PLATE_A_KNOWN_DUPE(tlPlate, 0)					
			PRINTLN("[platedupes] IS_PLATE_A_SUSPECTED_DUPE - plate is a known dupe '", tlPlate, "'")
			RETURN TRUE					
		ENDIF
		
	ELSE
		PRINTLN("[platedupes] IS_PLATE_A_SUSPECTED_DUPE - legitimate personal plate '", tlPlate, "'")	
	
	ENDIF

	PRINTLN("[platedupes] IS_PLATE_A_SUSPECTED_DUPE - FALSE  '", tlPlate, "'")
	RETURN FALSE
ENDFUNC


PROC METRIC_DUPE_DETECTION(DD_LOCATION eLocation, DD_REASON eReason, VEHICLE_INDEX VehicleID, INT iExploitLevelIncrease=0)

	IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_A_GARAGE) // dont send data when in car in garage

		IF NOT HasVehicleHadMetricSent(VehicleID, ENUM_TO_INT(eLocation))
			
			DEBUG_PRINTCALLSTACK()

			TEXT_LABEL_15 tlPlate = GET_VEHICLE_NUMBER_PLATE_TEXT(VehicleID) 

			STRUCT_DUPE_DETECTION Data

			Data.m_locationBlocked = ENUM_TO_INT(eLocation)
			Data.m_reasonBlocked = ENUM_TO_INT(eReason)
			Data.m_vehicleModel = ENUM_TO_INT(GET_ENTITY_MODEL(VehicleID))	
			Data.m_dupesSold = GET_NUMBER_OF_KNOWN_DUPES_SOLD_FOR_VEHICLE(VehicleID)
			Data.m_blockedVehicles = GET_NUMBER_OF_CARS_IN_GARAGE_WITH_PLATE(tlPlate)
			Data.m_ownedVehicles = GET_TOTAL_NUMBER_OF_CARS_OWNED_BY_PLAYER()
			Data.m_garageSpace = GET_NUMBER_OF_GARAGE_SPACES()
			Data.m_exploitLevel = GET_MP_INT_PLAYER_STAT(MPPLY_CHAR_EXPLOIT_LEVEL)
			Data.m_levelIncrease = iExploitLevelIncrease
			Data.m_iFruit = GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_MOD_CAR_USING_APP) 
			Data.m_spareSlot1 = 0
			Data.m_spareSlot2 = 0
			
			PRINTLN("METRIC_DUPE_DETECTION - Data: ")
			PRINTLN("m_locationBlocked = ", Data.m_locationBlocked)
			PRINTLN("m_reasonBlocked = ", Data.m_reasonBlocked)
			PRINTLN("m_vehicleModel = ", Data.m_vehicleModel)
			PRINTLN("m_dupesSold = ", Data.m_dupesSold)
			PRINTLN("m_blockedVehicles = ", Data.m_blockedVehicles)
			PRINTLN("m_ownedVehicles = ", Data.m_ownedVehicles)
			PRINTLN("m_garageSpace = ", Data.m_garageSpace)
			PRINTLN("m_exploitLevel = ", Data.m_exploitLevel)
			PRINTLN("m_levelIncrease = ", Data.m_levelIncrease)
			PRINTLN("m_iFruit = ", Data.m_iFruit)
			PRINTLN("m_spareSlot1 = ", Data.m_spareSlot1)
			PRINTLN("m_spareSlot2 = ", Data.m_spareSlot2)
			

			PLAYSTATS_DUPE_DETECTED(Data)
			
			AddVehicleToMetricSentList(VehicleID, ENUM_TO_INT(eLocation))
				
		ELSE
			PRINTLN("METRIC_DUPE_DETECTION - data already sent for this vehicle.")	
		ENDIF
	
	ELSE
		PRINTLN("METRIC_DUPE_DETECTION - in garage.")
	ENDIF
	
	
ENDPROC


PROC LOCK_DUPED_VEHICLE(VEHICLE_INDEX VehicleID, DD_REASON eReason, BOOL bTunableEnabled, BOOL bNonSaveableOnly)

	IF NOT (g_sMPTunables.bEnabled_DUPE_DETECTOR)
		PRINTLN("[platedupes] LOCK_DUPED_VEHICLE - dupe detector disabled")
		EXIT
	ENDIF
	
	IF (g_bOptOutPlayerFromDupeDetector)
		PRINTLN("[platedupes] LOCK_DUPED_VEHICLE - opt out.")
		EXIT
	ENDIF

	BOOL bMissionEnt
	IF CAN_EDIT_THIS_ENTITY(VehicleID, bMissionEnt)		
		
		IF (bTunableEnabled)
			IF (bNonSaveableOnly)
				PRINTLN("[platedupes] LOCK_DUPED_VEHICLE - making non saveable")		
				DECOR_SET_INT(VehicleID, "Not_Allow_As_Saved_Veh", 1)
			ELSE
				PRINTLN("[platedupes] LOCK_DUPED_VEHICLE - locking vehicle")		
				DECOR_SET_INT(VehicleID, "Player_Vehicle", -1)
				SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(VehicleID, TRUE)
				LOCK_DOORS_WHEN_NO_LONGER_NEEDED(VehicleID)
				FREEZE_ENTITY_POSITION(VehicleID, FALSE)
			ENDIF
			
			#IF IS_DEBUG_BUILD
			g_LockedDupe = VehicleID
			g_LockedDupeReason = ENUM_TO_INT(eReason)
			#ENDIF
		ENDIF
			
		METRIC_DUPE_DETECTION(DD_LOC_VEHICLE_ENTRY, eReason, VehicleID)
	
	ELSE
		PRINTLN("[platedupes] LOCK_DUPED_VEHICLE - cant edit vehicle")
	ENDIF
	IF (bMissionEnt)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
	ENDIF
ENDPROC


FUNC BOOL DoVehiclesHaveSameModelAndColour(VEHICLE_INDEX VehicleID1, VEHICLE_INDEX VehicleID2)
	IF (GET_ENTITY_MODEL(VehicleID1) = GET_ENTITY_MODEL(VehicleID2))
		INT iR1, iG1, iB1
		INT iR2, iG2, iB2
		GET_VEHICLE_COLOR(VehicleID1, iR1, iG1, iB1) 
		GET_VEHICLE_COLOR(VehicleID2, iR2, iG2, iB2) 		
		IF (iR1 = iR2)
		AND (iG1 = iG2)
		AND (iB1 = iB2)		
			RETURN TRUE		
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CHECK_FOR_VEHICLE_MODEL(VEHICLE_INDEX pedVeh)
	IF DOES_ENTITY_EXIST(pedVeh)
	AND NOT IS_ENTITY_DEAD(pedVeh)
		SWITCH GET_ENTITY_MODEL(pedVeh)
			CASE TERBYTE
			CASE AVENGER
			CASE HAULER2
			CASE PHANTOM3
				RETURN FALSE
			BREAK
		ENDSWITCH
	ENDIF
	RETURN TRUE
ENDFUNC 


BOOL bMyPVDupeDetected
BOOL bRemoteDupeDetected
BOOL bSomeoneElsePV
PROC CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE()
	
	IF g_bDontCheckForArmoryPropertyDupe 
	OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		EXIT
	ENDIF	
	
	VEHICLE_INDEX nearbyVehs[2]
	IF IS_SCREEN_FADED_IN()
	AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		IF (IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID()) AND PLAYER_ID() = g_OwnerOfArmoryAircraftPropertyIAmIn)
		OR (IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID()) AND PLAYER_ID() = g_ownerOfArmoryTruckPropertyIAmIn)
		OR (IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID()) AND PLAYER_ID() = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
			GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs)
			IF DOES_ENTITY_EXIST(nearbyVehs[0])
			AND IS_ENTITY_A_VEHICLE(nearbyVehs[0])
			AND DOES_ENTITY_EXIST(nearbyVehs[1])
			AND IS_ENTITY_A_VEHICLE(nearbyVehs[1])
				IF SHOULD_CHECK_FOR_VEHICLE_MODEL(nearbyVehs[0])
				AND SHOULD_CHECK_FOR_VEHICLE_MODEL(nearbyVehs[1])
					IF nearbyVehs[0] != nearbyVehs[1]
						IF (GET_OWNER_OF_PERSONAL_VEHICLE(nearbyVehs[0]) != GET_OWNER_OF_PERSONAL_VEHICLE(nearbyVehs[1]))
							IF NOT bMyPVDupeDetected 
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
								PRINTLN("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE some one else's vehicle detected in my property")
								bRemoteDupeDetected = TRUE
							ENDIF	
						ENDIF
						IF (!IS_STRING_NULL_OR_EMPTY(GET_VEHICLE_NUMBER_PLATE_TEXT(nearbyVehs[0])) AND !IS_STRING_NULL_OR_EMPTY(GET_VEHICLE_NUMBER_PLATE_TEXT(nearbyVehs[1])) AND ARE_STRINGS_EQUAL(GET_VEHICLE_NUMBER_PLATE_TEXT(nearbyVehs[0]),GET_VEHICLE_NUMBER_PLATE_TEXT(nearbyVehs[1])))
						OR (GET_OWNER_OF_PERSONAL_VEHICLE(nearbyVehs[0]) = GET_OWNER_OF_PERSONAL_VEHICLE(nearbyVehs[1]))
							IF NOT bRemoteDupeDetected
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
								PRINTLN("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE vehicles detected")
								bMyPVDupeDetected = TRUE
							ENDIF	
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
			
			IF DOES_ENTITY_EXIST(nearbyVehs[0])
			AND IS_ENTITY_A_VEHICLE(nearbyVehs[0])
				IF SHOULD_CHECK_FOR_VEHICLE_MODEL(nearbyVehs[0])
					
					IF GET_OWNER_OF_PERSONAL_VEHICLE(nearbyVehs[0]) != PLAYER_ID()
						IF NOT bMyPVDupeDetected 
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
							PRINTLN("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE some one else's vehicle detected in my property bSomeoneElsePV true")
							bSomeoneElsePV = TRUE
						ENDIF	
					ENDIF
					
				ENDIF
			
			ENDIF
			
			IF bSomeoneElsePV
			AND NOT bRemoteDupeDetected
			AND NOT bMyPVDupeDetected
				IF DOES_ENTITY_EXIST(nearbyVehs[0])
				AND IS_ENTITY_A_VEHICLE(nearbyVehs[0])
					IF GET_OWNER_OF_PERSONAL_VEHICLE(nearbyVehs[0]) != PLAYER_ID()
						IF NETWORK_HAS_CONTROL_OF_ENTITY(nearbyVehs[0])
							INT iInstance
							IF NOT NETWORK_IS_ENTITY_CONCEALED(nearbyVehs[0])
								IF NOT IS_ENTITY_DEAD(nearbyVehs[0])
								AND !IS_STRING_NULL_OR_EMPTY(GET_ENTITY_SCRIPT(nearbyVehs[0],iInstance))
								AND ARE_STRINGS_EQUAL(GET_ENTITY_SCRIPT(nearbyVehs[0],iInstance),FREEMODE_SCRIPT())
									PRINTLN("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE about to delete bSomeoneElsePV : ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(nearbyVehs[0])))
									SET_ENTITY_AS_MISSION_ENTITY(nearbyVehs[0], FALSE, TRUE)
									DELETE_VEHICLE(nearbyVehs[0])
									#IF IS_DEBUG_BUILD
									SCRIPT_ASSERT("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE bSomeoneElsePV DELETING VEHICLE DUP")
									#ENDIF
									bSomeoneElsePV = FALSE
									PRINTLN("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE bSomeoneElsePV deleting unknown PV")
								ENDIF	
							ENDIF	
						ELSE
							NETWORK_REQUEST_CONTROL_OF_ENTITY(nearbyVehs[0])
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
			
			IF bRemoteDupeDetected
			AND NOT bMyPVDupeDetected
				IF DOES_ENTITY_EXIST(nearbyVehs[0])
				AND IS_ENTITY_A_VEHICLE(nearbyVehs[0])
					IF GET_OWNER_OF_PERSONAL_VEHICLE(nearbyVehs[0]) != PLAYER_ID()
						IF NETWORK_HAS_CONTROL_OF_ENTITY(nearbyVehs[0])
							INT iInstance
							IF NOT NETWORK_IS_ENTITY_CONCEALED(nearbyVehs[0])
								IF NOT IS_ENTITY_DEAD(nearbyVehs[0])
								AND !IS_STRING_NULL_OR_EMPTY(GET_ENTITY_SCRIPT(nearbyVehs[0],iInstance))
								AND ARE_STRINGS_EQUAL(GET_ENTITY_SCRIPT(nearbyVehs[0],iInstance),FREEMODE_SCRIPT())
									PRINTLN("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE about to delete: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(nearbyVehs[0])))
									SET_ENTITY_AS_MISSION_ENTITY(nearbyVehs[0], FALSE, TRUE)
									DELETE_VEHICLE(nearbyVehs[0])
									SCRIPT_ASSERT("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE DELETING VEHICLE DUP")
									bRemoteDupeDetected = FALSE
									PRINTLN("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE deleting unknown PV")
								ENDIF	
							ENDIF	
						ELSE
							NETWORK_REQUEST_CONTROL_OF_ENTITY(nearbyVehs[0])
						ENDIF
					ENDIF	
				ENDIF
				
				IF DOES_ENTITY_EXIST(nearbyVehs[1])
				AND IS_ENTITY_A_VEHICLE(nearbyVehs[1])
					IF GET_OWNER_OF_PERSONAL_VEHICLE(nearbyVehs[1]) != PLAYER_ID()
						IF NETWORK_HAS_CONTROL_OF_ENTITY(nearbyVehs[1])
							INT iInstance
							IF NOT NETWORK_IS_ENTITY_CONCEALED(nearbyVehs[1])
								IF NOT IS_ENTITY_DEAD(nearbyVehs[1])
								AND !IS_STRING_NULL_OR_EMPTY(GET_ENTITY_SCRIPT(nearbyVehs[1],iInstance))
								AND ARE_STRINGS_EQUAL(GET_ENTITY_SCRIPT(nearbyVehs[1],iInstance),FREEMODE_SCRIPT())
									PRINTLN("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE about to delete: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(nearbyVehs[1])))
									SET_ENTITY_AS_MISSION_ENTITY(nearbyVehs[1], FALSE, TRUE)
									DELETE_VEHICLE(nearbyVehs[1])
									SCRIPT_ASSERT("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE DELETING VEHICLE DUP")
									bRemoteDupeDetected = FALSE
									PRINTLN("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE deleting second unknown PV")
								ENDIF	
							ENDIF	
						ELSE
							NETWORK_REQUEST_CONTROL_OF_ENTITY(nearbyVehs[1])
						ENDIF
					ENDIF	
				ENDIF	
			ENDIF
			
			IF bMyPVDupeDetected
			AND NOT bRemoteDupeDetected
				IF DOES_ENTITY_EXIST(nearbyVehs[0])
				AND IS_ENTITY_A_VEHICLE(nearbyVehs[0])
					IF NETWORK_HAS_CONTROL_OF_ENTITY(nearbyVehs[0])
						INT iInstance
						IF NOT NETWORK_IS_ENTITY_CONCEALED(nearbyVehs[0])
							IF NOT IS_ENTITY_DEAD(nearbyVehs[0])
							AND !IS_STRING_NULL_OR_EMPTY(GET_ENTITY_SCRIPT(nearbyVehs[0],iInstance))
							AND ARE_STRINGS_EQUAL(GET_ENTITY_SCRIPT(nearbyVehs[0],iInstance),FREEMODE_SCRIPT())
								PRINTLN("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE about to delete: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(nearbyVehs[0])))
								SET_ENTITY_AS_MISSION_ENTITY(nearbyVehs[0], FALSE, TRUE)
								DELETE_VEHICLE(nearbyVehs[0])
								SCRIPT_ASSERT("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE DELETING VEHICLE DUP")
								PRINTLN("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE first vehicle deleted")
							ENDIF	
						ENDIF	
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(nearbyVehs[0])
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(nearbyVehs[1])
				AND IS_ENTITY_A_VEHICLE(nearbyVehs[1])
					IF NETWORK_HAS_CONTROL_OF_ENTITY(nearbyVehs[1])
						INT iInstance
						IF NOT NETWORK_IS_ENTITY_CONCEALED(nearbyVehs[1])
							IF NOT IS_ENTITY_DEAD(nearbyVehs[1])
							AND !IS_STRING_NULL_OR_EMPTY(GET_ENTITY_SCRIPT(nearbyVehs[1],iInstance))
							AND ARE_STRINGS_EQUAL(GET_ENTITY_SCRIPT(nearbyVehs[1],iInstance),FREEMODE_SCRIPT())
								PRINTLN("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE about to delete: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(nearbyVehs[1])))
								SET_ENTITY_AS_MISSION_ENTITY(nearbyVehs[1], FALSE, TRUE)
								DELETE_VEHICLE(nearbyVehs[1])
								SCRIPT_ASSERT("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE DELETING VEHICLE DUP")
								PRINTLN("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE second vehicle deleted")
							ENDIF	
						ENDIF	
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(nearbyVehs[1])
					ENDIF
				ENDIF
				IF !DOES_ENTITY_EXIST(nearbyVehs[0])
				AND !DOES_ENTITY_EXIST(nearbyVehs[1])
					IF bMyPVDupeDetected
						bMyPVDupeDetected = FALSE
						PRINTLN("CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE bMyPVDupeDetected = FALSE")
					ENDIF
				ENDIF
				
			ENDIF	
		ENDIF	
	ENDIF	
	
ENDPROC 


PROC RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE()
	
	IF NOT (g_sMPTunables.bEnabled_DUPE_DETECTOR)
		PRINTLN("[platedupes] RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE - dupe detector disabled")
		EXIT
	ENDIF		
	
	CHECK_ARMORY_VEHICLE_PROPERTY_VEHICLE_DUPE()
	//PRINTLN("[platedupes] RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE - g_SpawnedInVehicle = ", NATIVE_TO_INT(g_SpawnedInVehicle))
	
	IF NOT (g_SpawnedInVehicle = INT_TO_NATIVE(VEHICLE_INDEX, -1))	
		IF NOT DOES_ENTITY_EXIST(g_SpawnedInVehicle)
			g_SpawnedInVehicle = INT_TO_NATIVE(VEHICLE_INDEX, -1)
			PRINTLN("[platedupes] RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE - g_SpawnedInVehicle no longer exists.")
		ENDIF
	ENDIF
	
	IF ( g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE)
	
		VEHICLE_INDEX VehicleID
				
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT ShouldIgnorePlateDupeCheck()
		
			VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF DOES_ENTITY_EXIST(VehicleID)
			AND NOT IS_ENTITY_DEAD(VehicleID)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID) // no point if we dont have control of vehicle													
			
				// is this my pv?
				ENSURE_PV_PLATE_MATCHES_SAVED_DATA(VehicleID)
					
				IF NOT (VehicleID = g_VehicleIDPlateDupeCheck)	
				
					// this isn't the spawned vehicle. 
					IF NOT (g_SpawnedInVehicle = VehicleID)
					
						IF NOT DECORATORS_SHOW_VEHICLE_BELONGS_TO_ANOTHER_PLAYER(VehicleID)

							TEXT_LABEL_15 tlPlate = GET_VEHICLE_NUMBER_PLATE_TEXT(VehicleID) 
							INT iPlateHash = GET_HASH_KEY(tlPlate)
											
							// only process car player is in if it has no decorators.
							IF NOT DOES_VEHICLE_HAVE_REQUIRED_DECORATORS(VehicleID)
							AND NOT DECORATORS_SHOW_VEHICLE_NON_SAVEABLE(VehicleID)
						
								// does this vehicle have a personalised plate of mine?
								IF IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER(tlPlate)	
									
									#IF IS_DEBUG_BUILD
										IF (g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_1)
											PRINTLN("[platedupes] RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE - in vehicle with personalised plates but not decorators.")
										ENDIF
									#ENDIF
									LOCK_DUPED_VEHICLE(VehicleID, DD_REASON_PERSONALISED_PLATE_BUT_NO_DECORATORS, g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_1, g_LockDupeVehicle_NonSaveOnly_1)	

								ELSE
								
									// does this plate match any vehicles in my garage?
									IF IS_PLATE_DUPE_OF_CAR_IN_GARAGE(tlPlate, GET_ENTITY_MODEL(VehicleID))
										#IF IS_DEBUG_BUILD
											IF (g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_2)
												PRINTLN("[platedupes] RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE - in vehicle matching one in garage.")
											ENDIF
										#ENDIF
										LOCK_DUPED_VEHICLE(VehicleID, DD_REASON_PLATE_MATCHES_CAR_IN_GARAGE, g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_2, g_LockDupeVehicle_NonSaveOnly_2)									
									ENDIF
									
									// if the car has mods over a certain value then check its decorators.
									IF (GET_CARMOD_MOD_SELL_VALUE(VehicleID) > GET_MOD_VALUE_LIMIT_FOR_VEHICLE(VehicleID)) // need to tunablise
										#IF IS_DEBUG_BUILD
											IF (g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_3)
												PRINTLN("[platedupes] RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE - too many mods on this.")
											ENDIF
										#ENDIF
										LOCK_DUPED_VEHICLE(VehicleID, DD_REASON_VALUE_OF_MODS_IS_TOO_HIGH, g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_3, g_LockDupeVehicle_NonSaveOnly_3)						
									ENDIF
									
									// compare to any nearby ambient vehicles
									VEHICLE_INDEX NearbyVehicleID[64]
									INT iNearbyVehicles = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), NearbyVehicleID)
									INT i
									TEXT_LABEL_15 tlThisPlate
									INT iThisPlateHash
									REPEAT iNearbyVehicles i
										IF NOT (NearbyVehicleID[i] = VehicleID)
										AND DOES_ENTITY_EXIST(NearbyVehicleID[i])
										AND NOT IS_ENTITY_DEAD(NearbyVehicleID[i])
										AND NOT IS_VEHICLE_A_LOCKED_PV(NearbyVehicleID[i])
											tlThisPlate = GET_VEHICLE_NUMBER_PLATE_TEXT(NearbyVehicleID[i]) 
											iThisPlateHash = GET_HASH_KEY(tlThisPlate)
											IF (iThisPlateHash = iPlateHash)
												IF (NATIVE_TO_INT(VehicleID) > NATIVE_TO_INT(NearbyVehicleID[i])) // only lock the later created vehicle.
												OR (NearbyVehicleID[i] = g_SpawnedInVehicle) // or the nearby vehicle is a spawned vehicle.													
													
													#IF IS_DEBUG_BUILD
														
														PRINTLN("[platedupes] RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE - VehicleID = ", NATIVE_TO_INT(VehicleID) , " NearbyVehicleID[i] = ", NATIVE_TO_INT(NearbyVehicleID[i]), " g_SpawnedInVehicle = ", NATIVE_TO_INT(g_SpawnedInVehicle))
														
														VECTOR vCoords
														MODEL_NAMES ModelName
														ModelName = GET_ENTITY_MODEL(VehicleID)
														vCoords = GET_ENTITY_COORDS(VehicleID)
														PRINTLN("[platedupes] RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE - matching nearby dupe. vehicle model ", ModelName, ", at ", vCoords, " plate ", tlPlate, ", plate hash ", iPlateHash)
														
														ModelName = GET_ENTITY_MODEL(NearbyVehicleID[i])
														vCoords = GET_ENTITY_COORDS(NearbyVehicleID[i])
														PRINTLN("[platedupes] RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE - iNearbyVehicles = ", iNearbyVehicles, ", NearbyVehicleID[", i, "] model ", ModelName, ", at ", vCoords, " plate ", tlThisPlate, ", plate hash ", iThisPlateHash)															
														
														
													#ENDIF																											
													
													IF DoVehiclesHaveSameModelAndColour(VehicleID, NearbyVehicleID[i])
													
														// found a dup
														#IF IS_DEBUG_BUILD
															IF (g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_4)
																PRINTLN("[platedupes] RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE - found ambient dupe.")
															ENDIF
														#ENDIF
																																																																		
														LOCK_DUPED_VEHICLE(VehicleID, DD_REASON_MATCHES_PLATE_OF_NEARBY_AMBIENT_VEHICLE, g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_4, g_LockDupeVehicle_NonSaveOnly_4)										
													ELSE
														PRINTLN("[platedupes] RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE - models or colours mismatch.")													
													ENDIF
														
												ENDIF								
											ENDIF
										ENDIF
									ENDREPEAT
											
									// is this a suspected dupe?
									DD_REASON eReason
									IF IS_VEHICLE_A_SUSPECTED_DUPE(VehicleID, -1, eReason)
										#IF IS_DEBUG_BUILD
											IF (g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_5)
												PRINTLN("[platedupes] RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE - suspected dupe.")
											ENDIF
										#ENDIF
										LOCK_DUPED_VEHICLE(VehicleID , eReason, g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_5, g_LockDupeVehicle_NonSaveOnly_5)
									ENDIF
																
																
								ENDIF
							
							ENDIF
							
						ENDIF
					
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("[platedupes] RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE - this is spawn vehicle.")
						#ENDIF
					ENDIF							
							
					// store so we only run check once on each vehicle we get in.
					g_VehicleIDPlateDupeCheck = VehicleID
											
				ENDIF
								
				
			ENDIF
			

		ELSE
			IF NOT (g_VehicleIDPlateDupeCheck = INT_TO_NATIVE(VEHICLE_INDEX, -1))
				g_VehicleIDPlateDupeCheck = INT_TO_NATIVE(VEHICLE_INDEX, -1)
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD

PROC GiveVehicleRandomColour(VEHICLE_INDEX VehicleID)

	// give random colour
	INT iRandR = GET_RANDOM_INT_IN_RANGE(0, 255)
	INT iRandG = GET_RANDOM_INT_IN_RANGE(0, 255)	
	INT iRandB = GET_RANDOM_INT_IN_RANGE(0, 255)	
	SET_VEHICLE_CUSTOM_PRIMARY_COLOUR(VehicleID, iRandR, iRandG, iRandB)
	SET_VEHICLE_CUSTOM_SECONDARY_COLOUR(VehicleID, iRandR, iRandG, iRandB)

ENDPROC

PROC GiveVehicleRandomMods(VEHICLE_INDEX VehicleID)
	
	IF GET_NUM_MOD_KITS(VehicleID) > 0

		// give mod kit
		SET_VEHICLE_MOD_KIT(VehicleID, 0)

		MOD_TYPE eModType
		INT iModType
		INT iNumMods
		INT iRandomMod
		
		REPEAT COUNT_OF(MOD_TYPE) iModType
			eModType = INT_TO_ENUM(MOD_TYPE, iModType)
			iNumMods = GET_NUM_VEHICLE_MODS(VehicleID, eModType)
			IF (iNumMods > 0)
				iRandomMod = GET_RANDOM_INT_IN_RANGE(0, iNumMods)
				SET_VEHICLE_MOD(VehicleID, eModType, iRandomMod)
			ENDIF
		ENDREPEAT
		
		//GiveVehicleRandomColour(VehicleID)
		
	ENDIF

ENDPROC

PROC UPDATE_PLATE_DUPE_WIDGET()

	VEHICLE_INDEX VehicleID
	STRING PlateText
	INT i
	BOOL bMissionEnt
	VECTOR vPos
	FLOAT fHeading 
	INT ilanes
	INT iPlateHash
	INT iMPBitset
	INT iPlayerVehicle
	
	// create an ambient dupe in the world near the player
	IF (g_bCreateAmbientDupe)
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, g_iAmbientDupeModelHash))
		IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, g_iAmbientDupeModelHash))
		
			vPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)

			GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vPos, 0, vPos, fHeading, ilanes)

			VehicleID = CREATE_VEHICLE(INT_TO_ENUM(MODEL_NAMES, g_iAmbientDupeModelHash), vPos, fHeading, TRUE, FALSE)			
			
			GiveVehicleRandomColour(VehicleID)
			
			PlateText = GET_CONTENTS_OF_TEXT_WIDGET(g_AmbientDupePlateText)
			
			IF DOES_ENTITY_EXIST(VehicleID)
			AND NOT IS_ENTITY_DEAD(VehicleID)
				SET_VEHICLE_NUMBER_PLATE_TEXT(VehicleID, PlateText)									
				PlateText = GET_VEHICLE_NUMBER_PLATE_TEXT(VehicleID)
				iPlateHash = GET_HASH_KEY(PlateText)
			ENDIF
			PRINTLN("[platedupes] UPDATE_PLATE_DUPE_WIDGET  created ambient dupe vehicle with plate '", PlateText, "' hash ", iPlateHash)

			SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
			SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, g_iAmbientDupeModelHash))
			g_bCreateAmbientDupe = FALSE
		ENDIF
	ENDIF
	
	IF (g_bDupeCurrentGarageVehicle)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())	
			
			IF DOES_ENTITY_EXIST(VehicleID)
			AND NOT IS_ENTITY_DEAD(VehicleID)
			
				VEHICLE_SETUP_STRUCT_MP VehicleSetupMP
				GET_VEHICLE_SETUP_MP(VehicleID, VehicleSetupMP)
				
				vPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleID, <<2.5, 0.0, 0.0>>)
				fHeading = GET_ENTITY_HEADING(VehicleID)
	
				IF DECOR_EXIST_ON(VehicleID, "MPBitset")
					iMPBitset = DECOR_GET_INT(VehicleID, "MPBitset")
				ENDIF
				IF DECOR_EXIST_ON(VehicleID, "Player_Vehicle")
					iPlayerVehicle = DECOR_GET_INT(VehicleID, "Player_Vehicle")
				ENDIF
				
				VehicleID = CREATE_VEHICLE(VehicleSetupMP.VehicleSetup.eModel, vPos, fHeading, TRUE, FALSE)	
				SET_VEHICLE_SETUP_MP(VehicleID, VehicleSetupMP, DEFAULT, DEFAULT, TRUE)
				
				DECOR_SET_BOOL(VehicleID,"IgnoredByQuickSave",FALSE)
				DECOR_SET_INT(VehicleID, "MPBitset", iMPBitset)
				DECOR_SET_INT(VehicleID, "Player_Vehicle", iPlayerVehicle)
				
				SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
				SET_MODEL_AS_NO_LONGER_NEEDED(VehicleSetupMP.VehicleSetup.eModel)
				
				PRINTLN("[platedupes] UPDATE_PLATE_DUPE_WIDGET created garage dupe")
	
			ENDIF
			
		ENDIF
		g_bDupeCurrentGarageVehicle = FALSE
	ENDIF
	
	IF (g_bDupeCurrentVehicle)
	OR (g_bDupeCurrentVehicleAndPlate)
	
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())		
	
			VEHICLE_SETUP_STRUCT_MP VehicleSetupMP
		
			GET_VEHICLE_SETUP_MP(VehicleID, VehicleSetupMP)
			
			REQUEST_MODEL(VehicleSetupMP.VehicleSetup.eModel)
			IF HAS_MODEL_LOADED(VehicleSetupMP.VehicleSetup.eModel)
			
				vPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				
				IF g_bOverideDupeCoord 
					vPos = <<519.8810, 4750.6089, -69.9960>>
				ELSE
					GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vPos, 0, vPos, fHeading, ilanes)
				ENDIF
				
				VehicleID = CREATE_VEHICLE(VehicleSetupMP.VehicleSetup.eModel, vPos, 90, TRUE, FALSE)	
				SET_VEHICLE_SETUP_MP(VehicleID, VehicleSetupMP, DEFAULT, DEFAULT, TRUE)
				
				IF (g_bDupeCurrentVehicleAndPlate)
					PlateText = GET_CONTENTS_OF_TEXT_WIDGET(g_AmbientDupePlateText)					
					IF DOES_ENTITY_EXIST(VehicleID)
					AND NOT IS_ENTITY_DEAD(VehicleID)
						SET_VEHICLE_NUMBER_PLATE_TEXT(VehicleID, PlateText)											
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(VehicleID)
				AND NOT IS_ENTITY_DEAD(VehicleID)
					PlateText = GET_VEHICLE_NUMBER_PLATE_TEXT(VehicleID)
					iPlateHash = GET_HASH_KEY(PlateText)	
				ENDIF	
				PRINTLN("[platedupes] UPDATE_PLATE_DUPE_WIDGET duped current vehicle with plate '", PlateText, "' hash ", iPlateHash)

					
				SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
				SET_MODEL_AS_NO_LONGER_NEEDED(VehicleSetupMP.VehicleSetup.eModel)
			ENDIF
		ENDIF
	
		g_bDupeCurrentVehicleAndPlate = FALSE
		g_bDupeCurrentVehicle = FALSE
	ENDIF
	
	IF (g_bUnbrickCarPlayerIsUsing)
		
		VehicleID = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(VehicleID)
			IF CAN_EDIT_THIS_ENTITY(VehicleID, bMissionEnt)	
						
				VEHICLE_SETUP_STRUCT_MP VehicleSetupMP		
				GET_VEHICLE_SETUP_MP(VehicleID, VehicleSetupMP)			
			
				vPos = GET_ENTITY_COORDS(VehicleID)
				fHeading = GET_ENTITY_HEADING(VehicleID)
				
				DELETE_VEHICLE(VehicleID)
			
				REQUEST_MODEL(VehicleSetupMP.VehicleSetup.eModel)
				IF HAS_MODEL_LOADED(VehicleSetupMP.VehicleSetup.eModel)
			
					VehicleID = CREATE_VEHICLE(VehicleSetupMP.VehicleSetup.eModel, vPos, fHeading, TRUE, FALSE)	
					
					VehicleSetupMP.iOwnerStatus = OWNER_STATUS_NULL
					
					SET_VEHICLE_SETUP_MP(VehicleID, VehicleSetupMP, FALSE, DEFAULT, TRUE)					
					
					SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
					SET_MODEL_AS_NO_LONGER_NEEDED(VehicleSetupMP.VehicleSetup.eModel)					
			
				ENDIF
									
				PRINTLN("[platedupes] UPDATE_PLATE_DUPE_WIDGET unbricked car ")
			
				g_bUnbrickCarPlayerIsUsing = FALSE
			
			ENDIF
			IF (bMissionEnt)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
			ENDIF	
			
			
		ENDIF
		
	ENDIF
	
	IF (g_bClearPlateDupeStatLists)
		
		REPEAT TOTAL_STORED_STATS_PLATES_PERSONALISED i 
			SetRecentlyUsedPersonalisedPlateStatValue(i, 0)
		ENDREPEAT
		
		REPEAT TOTAL_STORED_GLOBAL_PLATES_PERSONALISED i
			g_iPersonalisedCarPlateGlobals[i] = 0
		ENDREPEAT
		
		REPEAT TOTAL_STORED_STATS_PLATES_SOLD i
			SetRecentlySoldPlateStatValue(i, 0)
		ENDREPEAt
		
		REPEAT TOTAL_STORED_GLOBAL_PLATES_PERSONALISED i
			g_iSoldCarPlateGlobals[i] = 0
		ENDREPEAT
		
		REPEAT TOTAL_STORED_STATS_KNOWN_DUPES i 
			SetKnownDupePlateStatValue(i, 0)
			SetKnownDupeSoldStatValue(i, 0)
		ENDREPEAT
		
		REPEAT TOTAL_STORED_GLOBAL_PLATES_KNOWN_DUPES i
			g_iKnownDupePlateGlobals[i] = 0
			g_iKnownDupeSoldGlobals[i] = 0
		ENDREPEAT
		
		g_bClearPlateDupeStatLists = FALSE
	ENDIF

	// save stats
	IF (g_bSavePlateDupeStatLists)
		
		REPEAT TOTAL_STORED_STATS_PLATES_PERSONALISED i 
			SetRecentlyUsedPersonalisedPlateStatValue(i, g_iPersonalisedCarPlateStats[i])	
		ENDREPEAT
		REPEAT TOTAL_STORED_STATS_PLATES_SOLD i
			SetRecentlySoldPlateStatValue(i, g_iSoldCarPlateStats[i])
		ENDREPEAt
		REPEAT TOTAL_STORED_STATS_KNOWN_DUPES i 
			SetKnownDupePlateStatValue(i, g_iKnownDupePlateStats[i])
			SetKnownDupeSoldStatValue(i, g_iKnownDupeSoldStats[i])
		ENDREPEAT
		
		g_bSavePlateDupeStatLists = FALSE
	ENDIF
	
	IF (g_bClearRecentlyChangedPlatesLists)
		REPEAT TOTAL_STORED_STATS_PLATES_PERSONALISED i 
			SetRecentlyUsedPersonalisedPlateStatValue(i, 0)
		ENDREPEAT
		REPEAT TOTAL_STORED_GLOBAL_PLATES_PERSONALISED i
			g_iPersonalisedCarPlateGlobals[i] = 0
		ENDREPEAT	
		g_bClearRecentlyChangedPlatesLists = FALSE
	ENDIF
	
	IF (g_bClearRecentlySoldLists)
		
		REPEAT TOTAL_STORED_STATS_PLATES_SOLD i
			SetRecentlySoldPlateStatValue(i, 0)
		ENDREPEAt
		
		REPEAT TOTAL_STORED_GLOBAL_PLATES_PERSONALISED i
			g_iSoldCarPlateGlobals[i] = 0
		ENDREPEAT	
		g_bClearRecentlySoldLists = FALSE
	ENDIF
	
	IF (g_bClearKnownDupesLists)
		REPEAT TOTAL_STORED_STATS_KNOWN_DUPES i 
			SetKnownDupePlateStatValue(i, 0)
			SetKnownDupeSoldStatValue(i, 0)
		ENDREPEAT
		
		REPEAT TOTAL_STORED_GLOBAL_PLATES_KNOWN_DUPES i
			g_iKnownDupePlateGlobals[i] = 0
			g_iKnownDupeSoldGlobals[i] = 0
		ENDREPEAT
			
		g_bClearKnownDupesLists = FALSE
	ENDIF
	
	IF (g_bGetHashOfPlate)
		PlateText = GET_CONTENTS_OF_TEXT_WIDGET(g_AmbientDupePlateText)
		g_iHashOfPlate = GET_HASH_KEY(PlateText)
		g_bGetHashOfPlate = FALSE
	ENDIF

	IF (g_bApplyPlateToCurrentVehicle)
	
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			PlateText = GET_CONTENTS_OF_TEXT_WIDGET(g_AmbientDupePlateText)
			
			
			IF CAN_EDIT_THIS_ENTITY(VehicleID, bMissionEnt)	
				SET_VEHICLE_NUMBER_PLATE_TEXT(VehicleID, PlateText)
				g_VehicleIDPlateDupeCheck = INT_TO_NATIVE(VEHICLE_INDEX, -1)
			ENDIF
			IF (bMissionEnt)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
			ENDIF			
		ENDIF
		
	
		g_bApplyPlateToCurrentVehicle = FALSE
	ENDIF
	
	IF (g_bGiveVehicleRandomMods)	
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())			
			VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())			
			IF CAN_EDIT_THIS_ENTITY(VehicleID, bMissionEnt)
				GiveVehicleRandomMods(VehicleID)
				g_VehicleIDPlateDupeCheck = INT_TO_NATIVE(VEHICLE_INDEX, -1)
			ENDIF		
			IF (bMissionEnt)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
			ENDIF									
		ENDIF
		
		g_bGiveVehicleRandomMods = FALSE
	ENDIF
	
	IF (g_bGiveVehicleRandomColour)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())			
			VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())			
			IF CAN_EDIT_THIS_ENTITY(VehicleID, bMissionEnt)
				GiveVehicleRandomColour(VehicleID)
			ENDIF		
			IF (bMissionEnt)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
			ENDIF
			g_VehicleIDPlateDupeCheck = INT_TO_NATIVE(VEHICLE_INDEX, -1)	
		ENDIF
		g_bGiveVehicleRandomColour = FALSE
	ENDIF
	
	IF (g_bOutputVehicleTimestamps)
		REPEAT MAX_MP_SAVED_VEHICLES i
			PRINTLN("[platedupes] g_MpSavedVehicles[", i, "].iObtainedTime  ", g_MpSavedVehicles[i].iObtainedTime )		
		ENDREPEAT
		g_bOutputVehicleTimestamps = FALSE
	ENDIF
	
	IF (g_bDD_OutputAllValues)
		
		OUTPUT_ALL_DUPE_DETECT_VALUES()
		
		g_bDD_OutputAllValues = FALSE
	ENDIF

ENDPROC
#ENDIF

#IF IS_DEBUG_BUILD
PROC RENDER_DUPE_DETECTOR_DEBUG_PRINTS()
	IF DOES_ENTITY_EXIST(g_LockedDupe)
	AND NOT IS_ENTITY_DEAD(g_LockedDupe)
		VECTOR vCam = GET_FINAL_RENDERED_CAM_COORD()	
		VECTOR vPos = GET_ENTITY_COORDS(g_LockedDupe)
		IF VDIST2(vCam, vPos) < 2000
		AND IS_ENTITY_ON_SCREEN(g_LockedDupe)
			SWITCH INT_TO_ENUM(DD_REASON, g_LockedDupeReason)
				CASE DD_REASON_PERSONALISED_PLATE_BUT_NO_DECORATORS DISPLAY_DUPE_MESSAGE_ON_VEHICLE(g_LockedDupe, "personalised plates but no decorators") BREAK
				CASE DD_REASON_PLATE_MATCHES_CAR_IN_GARAGE DISPLAY_DUPE_MESSAGE_ON_VEHICLE(g_LockedDupe, "matching plate in players garage") BREAK
				CASE DD_REASON_VALUE_OF_MODS_IS_TOO_HIGH DISPLAY_DUPE_MESSAGE_ON_VEHICLE(g_LockedDupe, "mod value too great") BREAK
				CASE DD_REASON_MATCHES_PLATE_OF_NEARBY_AMBIENT_VEHICLE DISPLAY_DUPE_MESSAGE_ON_VEHICLE(g_LockedDupe, "nearby ambient plate match") BREAK				
				CASE DD_REASON_IS_A_KNOWN_DUPE DISPLAY_DUPE_MESSAGE_ON_VEHICLE(g_LockedDupe, "matches a known dupe plate") BREAK
				CASE DD_REASON_PLATE_HAS_BEEN_RECENTLY_PERSONALISED DISPLAY_DUPE_MESSAGE_ON_VEHICLE(g_LockedDupe, "matches recently personalised plate") BREAK
				CASE DD_REASON_HAS_BEEN_RECENTLY_SOLD DISPLAY_DUPE_MESSAGE_ON_VEHICLE(g_LockedDupe, "matches recently sold plate") BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL DOES_PLAYER_OWN_KNOWN_DUPES()
	INT i
	REPEAT TOTAL_STORED_STATS_KNOWN_DUPES i
		IF (g_iKnownDupePlateStats[i] != 0)
		AND (g_iKnownDupePlateStats[i] != -1)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC SET_KNOWN_DUPER_STATUS()
	IF (NATIVE_TO_INT(PLAYER_ID()) > -1)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bIsKnownDuper = DOES_PLAYER_OWN_KNOWN_DUPES()
		PRINTLN("[platedupes] SET_KNOWN_DUPER - ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bIsKnownDuper)
	ENDIF	
ENDPROC

FUNC BOOL IS_KNOWN_DUPER(PLAYER_INDEX PlayerID)
	RETURN GlobalplayerBD[NATIVE_TO_INT(PlayerID)].bIsKnownDuper
ENDFUNC

PROC UPDATE_PLATE_DUPES()


	IF NOT (MPGlobalsAmbience.bPlateDupeDataInitialied)
		INIT_PLATE_DUPE_DATA()		
		CLEAN_DUPE_LISTS()
		GET_GARAGE_KNOWN_DUPES()			
		STORE_FREE_IFRUIT_PLATE()
		SET_KNOWN_DUPER_STATUS()
		MPGlobalsAmbience.bPlateDupeDataInitialied = TRUE
	ENDIF
	
	RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE()
	
	#IF IS_DEBUG_BUILD
		UPDATE_PLATE_DUPE_WIDGET()
	#ENDIF
	

ENDPROC







