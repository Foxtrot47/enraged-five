//////////////////////////////////////////////////////////////
// NAME:        net_friend_request.sch						//
// DESCRIPTION: Creates and controls relationship groups	//
// WRITTEN BY:  Ryan Baker									//
// DATE:  		18/07/12									//
//////////////////////////////////////////////////////////////
    
USING "globals.sch"

USING "net_entity_icons.sch"
USING "net_race_to_point.sch"
USING "menu_public.sch"
USING "Net_realty_new.sch"

#IF IS_DEBUG_BUILD
USING "net_debug.sch"
#ENDIF

CONST_INT PIM_OPEN_DELAY	350




///***----- MENU VISUAL HEADER -----***///
CONST_FLOAT fAlignX	0.0//-0.05	//-0.0755
CONST_FLOAT fAlignY	-0.0755

STRUCT PIM_VHEADER_DATA
	TEXT_STYLE PlayerNameStyle
	TEXT_PLACEMENT PlayerNamePlace
	
	//RECT Background
	FLOAT fOffsetY = -0.014
	//TEXT_STYLE CrewNameStyle
	//TEXT_PLACEMENT CrewNamePlace
	//SPRITE_PLACEMENT Headshot
	SPRITE_PLACEMENT SpriteBackground

	//FLOAT fTagX = 0.130
	//FLOAT fTagY = (0.120 + CUSTOM_MENU_ITEM_BAR_H)
	//FLOAT fTagScale = 0.100
					//Double	//Triple	//Quad		//Single
	//FLOAT fRankX = 0.2747		//0.2747	//			//
	//FLOAT fRankY = 0.044		//0.048		//			//
	//FLOAT fRankScale = 0.770	//0.600		//			//

	//FLOAT fAlignMaxX 	= 0.0
	//FLOAT fAlignMaxY 	= 0.0

	//STRING strHeadshotTxd
	//PEDHEADSHOT_ID pedHeadshot
		
	//SPRITE_PLACEMENT TempTest
ENDSTRUCT

//WIDGET_GROUP_ID widgetGroupPIM
//PURPOSE: Initialises the Menu Header - Shows Player Headshot, Name + CrewTag, Crew Name
PROC INIT_PIM_MENU_HEADER(PIM_VHEADER_DATA &VHeaderData)
	//Move Main Menu Down
	CUSTOM_MENU_Y = (((CUSTOM_MENU_ITEM_BAR_H*3)+0.05) + VHeaderData.fOffsetY)
	
	//Temp Test
	/*TempTest.x = 0.500
	TempTest.y = 0.514
	TempTest.w = 1.0
	TempTest.h = 1.0
	TempTest.r = 255
	TempTest.g = 255
	TempTest.b = 255
	TempTest.a = 0*/
	
	//Sprite Background
	VHeaderData.SpriteBackground.x = (0.0+(CUSTOM_MENU_W*0.5))	//0//(CUSTOM_MENU_X-0.05)	//(0.0+(CUSTOM_MENU_W*0.5)) //0.188
	VHeaderData.SpriteBackground.y = (0.100 + CUSTOM_MENU_ITEM_BAR_H + VHeaderData.fOffsetY)
	VHeaderData.SpriteBackground.w = (CUSTOM_MENU_W + 0.0005)
	VHeaderData.SpriteBackground.h = 0.090
	VHeaderData.SpriteBackground.r = 255
	VHeaderData.SpriteBackground.g = 255
	VHeaderData.SpriteBackground.b = 255
	VHeaderData.SpriteBackground.a = 255
	
	
	//Player Headshot
	/*Headshot.x = 0.100
	Headshot.y = 0.137 //+ CUSTOM_MENU_ITEM_BAR_H
	Headshot.w = 0.047	//0.050		//(CUSTOM_MENU_PIXEL_WIDTH*50)	//0.050	//
	Headshot.h = 0.084	//0.090		//(CUSTOM_MENU_PIXEL_HEIGHT*64)	//0.090	//
	Headshot.r = 255
	Headshot.g = 255
	Headshot.b = 255
	Headshot.a = 5*/
	
	
	//Background
	/*VHeaderData.Background.x = 0.188
	VHeaderData.Background.y = (0.100 + CUSTOM_MENU_ITEM_BAR_H + VHeaderData.fOffsetY)	//0.111
	VHeaderData.Background.w = CUSTOM_MENU_W
	VHeaderData.Background.h = 0.090		//(CUSTOM_MENU_ITEM_BAR_H*2)
	INT iHudR, iHudG, iHudB, iHudA
	GET_HUD_COLOUR(HUD_COLOUR_FREEMODE, iHudR, iHudG, iHudB, iHudA)
	VHeaderData.Background.r = iHudR	//68	//200
	VHeaderData.Background.g = iHudG	//158	//66
	VHeaderData.Background.b = iHudB	//208	//66
	VHeaderData.Background.a = iHudA	//255*/
	
	
	//Player Name
	VHeaderData.PlayerNameStyle.aFont = FONT_CONDENSED
	VHeaderData.PlayerNameStyle.XScale = 0.500
	VHeaderData.PlayerNameStyle.YScale = 0.750
	VHeaderData.PlayerNameStyle.r = 0
	VHeaderData.PlayerNameStyle.g = 0
	VHeaderData.PlayerNameStyle.b = 0
	VHeaderData.PlayerNameStyle.a = 255
	VHeaderData.PlayerNameStyle.drop = DROPSTYLE_NONE
	VHeaderData.PlayerNameStyle.wrapEndX = 0
	VHeaderData.PlayerNameStyle.wrapStartX = 0
	
	VHeaderData.PlayerNamePlace.x = CUSTOM_MENU_TITLE_TEXT_INDENT_X
	
	
	VHeaderData.PlayerNamePlace.y = (0.110 + VHeaderData.fOffsetY)	//+ CUSTOM_MENU_ITEM_BAR_H
	
	//Player Rank
	/*INT iPlayerRank = GET_PLAYER_RANK(PLAYER_ID())
	IF iPlayerRank < 10
		fRankX 		= 0.2775	//Scale = 0.500
		fRankY 		= 0.087 	
	ELIF iPlayerRank < 100
		fRankX 		= 0.2780	//Scale = 0.500
		fRankY 		= 0.087 	
	ELIF iPlayerRank < 1000
		fRankX 		= 0.278		//Scale = 0.400
		fRankY 		= 0.090 	
	ELSE
		fRankX 		= 0.2785	//Scale = 0.300
		fRankY 		= 0.093 	
	ENDIF*/
	
	//Player Crew Tag
	//SET_TEXT_STYLE(PlayerNameStyle)	
	//fTagX 		= 0.2	//(PlayerNamePlace.x + GET_STRING_WIDTH_PLAYER_NAME(GET_PLAYER_NAME(PLAYER_ID())))
	//fTagY 		= 0.102
	//fTagScale 	= 0.400
	
	
	//Player Crew Name
	/*CrewNameStyle.aFont = FONT_STANDARD
	CrewNameStyle.XScale = 0.332
	CrewNameStyle.YScale = 0.342
	CrewNameStyle.r = 0
	CrewNameStyle.g = 0
	CrewNameStyle.b = 0
	CrewNameStyle.a = 255
	CrewNameStyle.drop = DROPSTYLE_NONE
	CrewNameStyle.wrapEndX = 0
	CrewNameStyle.wrapStartX = 0
	
	CrewNamePlace.x = 0.124	//(Headshot.x +(CUSTOM_MENU_PIXEL_WIDTH*58))
	CrewNamePlace.y = 0.136 //+ CUSTOM_MENU_ITEM_BAR_H*/
	
	
	//Widgets
//	#IF IS_DEBUG_BUILD
//		IF DOES_WIDGET_GROUP_EXIST(widgetGroupPIM) 
//			//**TWH - CMcM - #1416616 - Wrapped header widget group.
//			SET_CURRENT_WIDGET_GROUP(widgetGroupPIM)
//			START_WIDGET_GROUP("PIM HEADER")
//				//CREATE_A_SPRITE_PLACEMENT_WIDGET(Headshot, "Headshot")
//				
//				//CREATE_A_RECT_PLACEMENT_WIDGET(Background, "Background")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(VHeaderData.SpriteBackground, "SpriteBackground")
//				
//				//CREATE_A_TEXT_STYLE_WIGET(PlayerNameStyle, "PlayerNameStyle")
//				//CREATE_A_TEXT_PLACEMENT_WIDGET(PlayerNamePlace, "PlayerNamePlace")
//				
//				//CREATE_A_TEXT_STYLE_WIGET(CrewNameStyle, "CrewNameStyle")
//				//CREATE_A_TEXT_PLACEMENT_WIDGET(CrewNamePlace, "CrewNamePlace")
//				
//				//CREATE_A_SPRITE_PLACEMENT_WIDGET(TempTest, "TempTest")
//				
//				//ADD_WIDGET_FLOAT_SLIDER("fRankX", fRankX, -1.0, 1.0, 0.0001)
//				//ADD_WIDGET_FLOAT_SLIDER("fRankY", fRankY, -1.0, 1.0, 0.0001)
//				//ADD_WIDGET_FLOAT_SLIDER("fRankScale", fRankScale, -1.0, 1.0, 0.0001)
//				//ADD_WIDGET_INT_SLIDER("iRank", iRank, 0, 9999, 1)
//				
//				//ADD_WIDGET_FLOAT_SLIDER("fTagX", fTagX, -1.0, 1.0, 0.0001)
//				//ADD_WIDGET_FLOAT_SLIDER("fTagY", fTagY, -1.0, 1.0, 0.0001)
//				//ADD_WIDGET_FLOAT_SLIDER("fTagScale", fTagScale, -1.0, 1.0, 0.1)
//				
//				//ADD_WIDGET_FLOAT_SLIDER("fAlignX", fAlignX, -1.0, 1.0, 0.0001)
//				//ADD_WIDGET_FLOAT_SLIDER("fAlignY", fAlignY, -1.0, 1.0, 0.0001)
//				//ADD_WIDGET_FLOAT_SLIDER("fAlignMaxX", fAlignMaxX, -1.0, 1.0, 0.0001)
//				//ADD_WIDGET_FLOAT_SLIDER("fAlignMaxY", fAlignMaxY, -1.0, 1.0, 0.0001)
//			STOP_WIDGET_GROUP()
//			CLEAR_CURRENT_WIDGET_GROUP(widgetGroupPIM)
//		ENDIF
//	#ENDIF

	CDEBUG3LN(DEBUG_AMBIENT, "INIT_PIM_MENU_HEADER - DONE")
ENDPROC

//PURPOSE: Draws the Menu Header when playing a Fixer Short Trip (as Lamar or Franklin)
PROC DRAW_PIM_MENU_HEADER_SHORT_TRIPS(PIM_VHEADER_DATA &VHeaderData)
	//Move Main Menu Down
	CUSTOM_MENU_Y = (((CUSTOM_MENU_ITEM_BAR_H*3)+0.05) + VHeaderData.fOffsetY)
	
	STRING sTextureDictName = "FIXER_MENU_BANNERS"
	STRING sTextureName = "SHOP_BANNER_SHORT_TRIPS_FRANKLIN"
	
	IF GET_PLAYERS_PED_MODEL(PLAYER_ID()) = INT_TO_ENUM(MODEL_NAMES, HASH("IG_LamarDavis_02")) // INT_TO_ENUM(MODEL_NAMES, HASH("P_Franklin_02"))
		sTextureName = "SHOP_BANNER_SHORT_TRIPS_LAMAR"
	ENDIF
	
	REQUEST_STREAMED_TEXTURE_DICT(sTextureDictName)
	IF HAS_STREAMED_TEXTURE_DICT_LOADED(sTextureDictName)
		SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
		SET_SCRIPT_GFX_ALIGN_PARAMS(fAlignX, fAlignY, 0.0, 0.0)
		DRAW_2D_SPRITE(sTextureDictName, sTextureName, VHeaderData.SpriteBackground)
		RESET_SCRIPT_GFX_ALIGN()
	ENDIF
	RESET_SCRIPT_GFX_ALIGN()	
ENDPROC

//PURPOSE: Draws the Menu Header - Shows Player Headshot, Name + CrewTag, Crew Name
PROC DRAW_PIM_MENU_HEADER_DEFAULT(PIM_VHEADER_DATA &VHeaderData)
	//SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
	//SET_SCRIPT_GFX_ALIGN_PARAMS(-0.05, -0.05, 0.0, 0.0)
	//SET_SCRIPT_GFX_ALIGN_PARAMS(fAlignX, fAlignY, 0.0, 0.0)
	
	
	//Move Main Menu Down
	CUSTOM_MENU_Y = (((CUSTOM_MENU_ITEM_BAR_H*3)+0.05) + VHeaderData.fOffsetY)
	
	
	//Background
	//DRAW_RECTANGLE(VHeaderData.Background)
	
	REQUEST_STREAMED_TEXTURE_DICT("CommonMenu")
	IF HAS_STREAMED_TEXTURE_DICT_LOADED("CommonMenu")
		SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
		SET_SCRIPT_GFX_ALIGN_PARAMS(fAlignX, fAlignY, 0.0, 0.0)
		DRAW_2D_SPRITE("CommonMenu", "Interaction_bgd", VHeaderData.SpriteBackground)
		RESET_SCRIPT_GFX_ALIGN()
	ENDIF
	
	/*//Player Headshot
	//IF NOT IS_BIT_SET(iBoolsBitSet, biHeadshotDone)
		pedHeadshot = Get_Transparent_HeadshotID_For_Player(PLAYER_ID())	//Get_HeadshotID_For_Player(PLAYER_ID())
		
		IF pedHeadshot != NULL
			strHeadshotTxd = GET_PEDHEADSHOT_TXD_STRING(pedHeadshot)
			SET_BIT(iBoolsBitSet, biHeadshotDone)
			//PRINTLN("[Script_PIM] DRAW_PIM_MENU_HEADER - biHeadshotDone SET     <----------     ")
		//ELSE
		//	strHeadshotTxd = "CHAR_DEFAULT"
		ENDIF
	//ENDIF*/
	
	/*IF IS_BIT_SET(iBoolsBitSet, biHeadshotDone)
		REQUEST_STREAMED_TEXTURE_DICT(strHeadshotTxd)
		IF HAS_STREAMED_TEXTURE_DICT_LOADED(strHeadshotTxd)
			DRAW_2D_SPRITE(strHeadshotTxd, strHeadshotTxd, Headshot)
			IF Headshot.a < 255
				Headshot.a += 10
				IF Headshot.a > 255
					Headshot.a = 255
				ENDIF
			ENDIF
		ENDIF
	ENDIF*/
	
	//Player Name
	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
	SET_SCRIPT_GFX_ALIGN_PARAMS(fAlignX, fAlignY, 0.0, 0.0)
	
	DRAW_TEXT_WITH_PLAYER_NAME(VHeaderData.PlayerNamePlace, VHeaderData.PlayerNameStyle, GET_PLAYER_NAME(PLAYER_ID()), DEFAULT, DEFAULT, FONT_LEFT)//GET_PLAYER_NAME(PLAYER_ID()))	//"18 point Chalet Comprine font"		"AdamRSN")//	WgWWWWWWWWWWWWWW
	
	
	RESET_SCRIPT_GFX_ALIGN()
	
	//Player Rank			//iRank		
	//DRAW_RANK_BADGE_FONT_LEADERBOARD(GET_PLAYER_RANK(PLAYER_ID()), fRankX, fRankY, RANKDISPLAYTYPE_JUST_NUMBER, HUD_COLOUR_PURE_WHITE, HUD_COLOUR_PURE_WHITE)	//, fRankScale)
	
	//Player Crew Tag
	//DRAW_CREW_TAG_PLAYER(PLAYER_ID(), fTagX, fTagY, fTagScale)
	
	
	//Player Crew Name
	//GAMER_HANDLE thisHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	//TEXT_LABEL_63 tlCrewName = GET_PLAYER_CLAN_NAME(thisHandle)
	//DRAW_TEXT_WITH_PLAYER_NAME(CrewNamePlace, CrewNameStyle, tlCrewName)	//"14 point Chalet font")	//"Monkey Bar Crew") //
	
	
	//TEST TO MAKE MATCH
	/*REQUEST_STREAMED_TEXTURE_DICT("MPInteraction")
	IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPInteraction")
		DRAW_2D_SPRITE("MPInteraction", "LayoutGuide", TempTest)
	ENDIF*/
	
	//RESET_SCRIPT_GFX_ALIGN()
ENDPROC

PROC DRAW_PIM_MENU_HEADER(PIM_VHEADER_DATA &VHeaderData)
	#IF FEATURE_FIXER
	IF IS_THIS_A_FIXER_SHORT_TRIP()
	AND NETWORK_IS_ACTIVITY_SESSION()
		DRAW_PIM_MENU_HEADER_SHORT_TRIPS(VHeaderData)
	ELSE
	#ENDIF
		DRAW_PIM_MENU_HEADER_DEFAULT(VHeaderData)
	#IF FEATURE_FIXER
	ENDIF
	#ENDIF
ENDPROC

///***----- GLARE ON MENU HEADER -----***///
STRUCT PIM_GLARE_DATA
	SCALEFORM_INDEX sfMenuGlare
	FLOAT			fSavedGlareDirection
ENDSTRUCT

PROC DRAW_PIM_GLARE(PIM_GLARE_DATA &GlareData)
	//add rotation
	FLOAT fDirection = 0
	FLOAT fRotationTolerance = 0.5
	VECTOR vDirection = <<0, 0, 0>>
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(GlareData.sfMenuGlare)
		GlareData.sfMenuGlare = REQUEST_SCALEFORM_MOVIE("MP_MENU_GLARE")
	ELSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			vDirection = GET_FINAL_RENDERED_CAM_ROT()
			fDirection = WRAP(vDirection.z, 0, 360)
			IF GlareData.fSavedGlareDirection = 0 OR (GlareData.fSavedGlareDirection - fDirection) > fRotationTolerance OR (GlareData.fSavedGlareDirection - fDirection) < -fRotationTolerance
				GlareData.fSavedGlareDirection = fDirection
				BEGIN_SCALEFORM_MOVIE_METHOD(GlareData.sfMenuGlare, "SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GlareData.fSavedGlareDirection)
		//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE) //show glint
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
		ENDIF
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(GlareData.sfMenuGlare, 255, 255, 255, 255)
	ENDIF
ENDPROC

PROC CLEANUP_PIM_GLARE(PIM_GLARE_DATA &GlareData)
	IF GlareData.sfMenuGlare != NULL
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(GlareData.sfMenuGlare)
		GlareData.sfMenuGlare = NULL
	ENDIF
ENDPROC

PROC DO_PIM_GLARE(PIM_GLARE_DATA &GlareData)
	#IF FEATURE_FIXER
	IF NOT (IS_THIS_A_FIXER_SHORT_TRIP()
	AND NETWORK_IS_ACTIVITY_SESSION())
	#ENDIF
		IF IS_SCREEN_FADED_IN()
			DRAW_PIM_GLARE(GlareData)
		ELSE
			CLEANUP_PIM_GLARE(GlareData)
		ENDIF
	#IF FEATURE_FIXER
	ENDIF
	#ENDIF
ENDPROC

//PURPOSE: Reset the Send Friend Request Data
PROC RESET_SEND_FRIEND_REQUEST_DATA(INT iParticipant)
	IF IS_BIT_SET(MPGlobals.PlayerInteractionData.BitSet, iParticipant)
		CLEAR_BIT(MPGlobals.PlayerInteractionData.BitSet, iParticipant)
		NET_PRINT_TIME()NET_PRINT("     ---------->     FRIEND REQUEST - RESET_SEND_FRIEND_REQUEST_DATA     <----------     ")NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Checks if it is okay to do the friend request display
FUNC BOOL SAFE_TO_DISPLAY_FRIEND_REQUEST(PLAYER_INDEX PlayerId)
	
	IF IS_INTERACTION_MENU_DISABLED()
		PRINTLN("[Script_PIM] CAN'T OPEN - IS_INTERACTION_MENU_DISABLED")
		RETURN FALSE
	ENDIF
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_RAGDOLL(PLAYER_PED_ID())
			#IF IS_DEBUG_BUILD
			IF g_bLaunchPIMDebug
				PRINTLN("[Script_PIM] CAN'T OPEN - RAGDOLL")
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - NOT OKAY")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - IS_PLAYER_CONTROL_ON")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	//Target Player Checks
	IF PlayerId != PLAYER_ID()
		IF NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId)
			#IF IS_DEBUG_BUILD
			IF g_bLaunchPIMDebug
				PRINTLN("[Script_PIM] CAN'T OPEN - OVERHEAD LOGIC NOT ACTIVE")
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
		
		IF GET_PLAYER_TEAM(PLAYER_ID()) != GET_PLAYER_TEAM(PlayerId)
			#IF IS_DEBUG_BUILD
			IF g_bLaunchPIMDebug
				PRINTLN("[Script_PIM] CAN'T OPEN - NOT ON SAME TEAM")
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
		
		IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), GET_PLAYER_PED(PlayerId), <<30, 30, 30>>)
			#IF IS_DEBUG_BUILD
			IF g_bLaunchPIMDebug
				PRINTLN("[Script_PIM] CAN'T OPEN - NOT IN RANGE OF PLAYER")
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
		
		IF NOT NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerId)
			#IF IS_DEBUG_BUILD
			IF g_bLaunchPIMDebug
				PRINTLN("[Script_PIM] CAN'T OPEN - NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION")
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
			
	IF NOT HAS_FM_RACE_TUT_BEEN_DONE()
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - HAS_FM_RACE_TUT_BEEN_DONE")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	/*IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) != WEAPONTYPE_UNARMED
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF*/
	
	//IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY) 
  	//OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_ANOTHER_PLAYERS_PROPERTY)
	/*IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
		RETURN FALSE
	ENDIF*/
		
	/*IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF*/
		
	IF IS_PHONE_ONSCREEN()
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - IS_PHONE_ONSCREEN")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
		
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()	//IS_PLAYER_IN_ANY_SHOP() //
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - IS_CUSTOM_MENU_ON_SCREEN")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_DARTS
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_ARM_WRESTLING
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_TENNIS
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_GOLF
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_SHOOTING_RANGE
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - IN MINIGAME")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - MPAM_TYPE_CINEMA")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bUsingFerrisWheel)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bUsingRollerCoaster)
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - PLAYER ON FAIRGROUND RIDE")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_CELLPHONE_CAMERA_IN_USE()
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - IS_CELLPHONE_CAMERA_IN_USE")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	//IF g_b_On_Deathmatch
	IF g_b_On_Race
	AND NOT IS_PLAYER_USING_ACTIVE_ROAMING_SPECTATOR_MODE(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - g_b_On_Race")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE()
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_b_OnLeaderboard
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - g_b_OnLeaderboard")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_bMissionEnding
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - g_bMissionEnding")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_TRANSITION_ACTIVE()
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - IS_TRANSITION_ACTIVE")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_bInAtm
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - g_bInAtm")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - NETWORK_IS_IN_MP_CUTSCENE")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_bBrowserVisible
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - g_bBrowserVisible")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_A_SPECTATOR_CAM_ACTIVE()
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - IS_A_SPECTATOR_CAM_ACTIVE")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam)
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - bInHeliGunCam")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_WARNING_MESSAGE_ACTIVE()
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - IS_WARNING_MESSAGE_ACTIVE")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
	AND (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT) = CAM_VIEW_MODE_FIRST_PERSON)
		#IF IS_DEBUG_BUILD
		IF g_bLaunchPIMDebug
			PRINTLN("[Script_PIM] CAN'T OPEN - IN HELI USING CAM")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Checks if it is okay to do the Race to Point option
FUNC BOOL SAFE_TO_DISPLAY_R2P_OPTION()
	RETURN CAN_DO_RACE_TO_POINT()
ENDFUNC


//PURPOSE: Does Join Crew checks for Player Interaction Menu
FUNC INT CAN_JOIN_CREW(PLAYER_INDEX thisPlayer, GAMER_HANDLE thisGamer)
	IF NOT NETWORK_HAS_SOCIAL_CLUB_ACCOUNT()
		RETURN 4
	ELIF NOT IS_PLAYER_IN_ACTIVE_CLAN(thisGamer)
		RETURN 1
	ELIF IS_PLAYER_IN_PLAYERS_ACTIVE_CLAN(PLAYER_ID(), thisPlayer)
		RETURN 2
	ELIF IS_PLAYER_CLAN_PRIVATE(thisGamer)
		RETURN 3
	ENDIF
	
	RETURN 0
ENDFUNC


//PURPOSE: Controls the player sending a friend request to a targeted buddy
PROC PROCESS_SEND_FRIEND_REQUEST(PLAYER_INDEX PlayerId)
	
	PARTICIPANT_INDEX ParticipantID = NETWORK_GET_PARTICIPANT_INDEX(PlayerId)
	INT iParticipant = NATIVE_TO_INT(ParticipantID)
	
	IF MPGlobals.PlayerInteractionData.iTargetPlayerInt = -1
	AND MPGlobals.PlayerInteractionData.iLaunchStage = -1
		IF IS_NET_PLAYER_OK(PlayerId) 
		AND IS_NET_PLAYER_OK(PLAYER_ID())
			IF PLAYER_ID() <> PlayerId 
									
				IF SAFE_TO_DISPLAY_FRIEND_REQUEST(PlayerId)
										
					IF NOT IS_BIT_SET(MPGlobals.PlayerInteractionData.BitSet, iParticipant)
						MPGlobals.PlayerInteractionData.BitSet = 0
						SET_BIT(MPGlobals.PlayerInteractionData.BitSet, iParticipant)
						NET_PRINT_TIME()NET_PRINT("     ---------->     FRIEND REQUEST - STARTED AIMING AT FRIEND     <----------     ")NET_NL()
					ENDIF

					IF GET_PLAYER_TEAM(PLAYER_ID()) = GET_PLAYER_TEAM(PlayerId)
						IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_INTERACTION_MENU)	//IS_DISABLED_CONTROL_JUST_PRESSED INPUT_FRONTEND_PAUSE)
							IF MPGlobals.PlayerInteractionData.bCloseReleasedCheck = FALSE
								IF HAS_NET_TIMER_EXPIRED(MPGlobals.PlayerInteractionData.OpenTimer, PIM_OPEN_DELAY, TRUE)
								OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
									
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
									RESET_NET_TIMER(MPGlobals.PlayerInteractionData.OpenTimer)
									NET_PRINT_TIME()NET_PRINT("     ---------->     FRIEND REQUEST - RESET OpenTimer - B")NET_NL()
				
									MPGlobals.PlayerInteractionData.iTargetPlayerInt = NATIVE_TO_INT(PlayerId)
									MPGlobals.PlayerInteractionData.iLaunchStage = 0
									NET_PRINT_TIME()NET_PRINT("     ---------->     FRIEND REQUEST - REMOTE - Target Player = ")NET_PRINT(GET_PLAYER_NAME(PlayerId))NET_NL()
									
								ENDIF
							ENDIF
						//HELP
						ELIF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							INT iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET)
							IF NOT IS_BIT_SET(iStat, TUTBS_PIM_HELP_1)
							AND NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
								//PRINT_HELP("PIM_HELP1")	//~s~When aiming at another player press ~INPUT_FRONTEND_SELECT~ to open the Player Interaction Menu.
								SET_BIT(iStat, TUTBS_PIM_HELP_1)
								SET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET, iStat)
								NET_PRINT_TIME()NET_PRINT("     ---------->     FRIEND REQUEST - TUTBS_PIM_HELP_1 SET ")NET_NL()
							ENDIF
						ENDIF
					ENDIF
						
				ELSE
					RESET_SEND_FRIEND_REQUEST_DATA(iParticipant)
				ENDIF
			ENDIF
		ELSE
			RESET_SEND_FRIEND_REQUEST_DATA(iParticipant)
		ENDIF
	ENDIF
	
	//CHECK FOR MY OWN MENU
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_INTERACTION_MENU)	//IS_CONTROL_PRESSED(IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM) OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM))
		NET_PRINT_TIME()NET_PRINT("     ---------->     FRIEND REQUEST - INPUT_INTERACTION_MENU - PRESSED - STEP 1")NET_NL()
		IF MPGlobals.PlayerInteractionData.BitSet = 0
		AND MPGlobals.PlayerInteractionData.iTargetPlayerInt = -1
		AND MPGlobals.PlayerInteractionData.iLaunchStage = -1
		AND MPGlobals.PlayerInteractionData.bCloseReleasedCheck = FALSE
			NET_PRINT_TIME()NET_PRINT("     ---------->     FRIEND REQUEST - INPUT_INTERACTION_MENU - STEP 2 ")NET_NL()
			IF HAS_NET_TIMER_EXPIRED(MPGlobals.PlayerInteractionData.OpenTimer, PIM_OPEN_DELAY, TRUE)
			OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				IF SAFE_TO_DISPLAY_FRIEND_REQUEST(PLAYER_ID())
				//AND (GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_UNARMED OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
						
					NET_PRINT_TIME()NET_PRINT("     ---------->     FRIEND REQUEST - INPUT_INTERACTION_MENU - STEP 3 ")NET_NL()
				
					//Check if player is shooting
					/*IF IS_PED_SHOOTING(PLAYER_PED_ID())
						RESET_SEND_FRIEND_REQUEST_DATA(PARTICIPANT_ID_TO_INT())
						NET_PRINT_TIME()NET_PRINT("     ---------->     FRIEND REQUEST - PLAYER IS SHOOTING - RESET     <----------     ")NET_NL()
						EXIT
					ENDIF*/
										
					//Check if player is sprinting
					/*IF IS_PED_SPRINTING(PLAYER_PED_ID())
						RESET_SEND_FRIEND_REQUEST_DATA(PARTICIPANT_ID_TO_INT())
						NET_PRINT_TIME()NET_PRINT("     ---------->     FRIEND REQUEST - PLAYER IS SPRINTING - RESET     <----------     ")NET_NL()
						EXIT
					ENDIF*/
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
					RESET_NET_TIMER(MPGlobals.PlayerInteractionData.OpenTimer)
					NET_PRINT_TIME()NET_PRINT("     ---------->     FRIEND REQUEST - RESET OpenTimer - C")NET_NL()
					
					SET_CURSOR_POSITION_FOR_MENU()
					
					MPGlobals.PlayerInteractionData.iTargetPlayerInt = NATIVE_TO_INT(PLAYER_ID())
					MPGlobals.PlayerInteractionData.iLaunchStage = 0
					NET_PRINT_TIME()NET_PRINT("     ---------->     FRIEND REQUEST - LOCAL - Target Player = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
				
				ELSE
					NET_PRINT_TIME()NET_PRINT("     ---------->     FRIEND REQUEST - INPUT_INTERACTION_MENU - SAFE_TO_DISPLAY_FRIEND_REQUEST = FALSE  ")NET_NL()
				ENDIF
			ELSE
				PRINTLN("     ---------->     FRIEND REQUEST - INPUT_INTERACTION_MENU - WAITING ON TIMER - PIM_OPEN_DELAY - CURRENT TIME = ", GET_TIME_AS_STRING(GET_NETWORK_TIME()))
				PRINTLN("     ---------->     FRIEND REQUEST - TIME DIFFERENCE = ", GET_TIME_AS_STRING(GET_NETWORK_TIME()))GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MPGlobals.PlayerInteractionData.OpenTimer, TRUE)
			ENDIF
		ELSE
			MPGlobals.PlayerInteractionData.BitSet = 0
		ENDIF
		
		//Block Select Cam Button
		IF HAS_NET_TIMER_STARTED(MPGlobals.PlayerInteractionData.OpenTimer)
			IF HAS_NET_TIMER_EXPIRED(MPGlobals.PlayerInteractionData.OpenTimer, PIM_OPEN_DELAY, TRUE)
			AND NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			ENDIF
		ENDIF
		
	//Reset Open Timer
	ELSE
		IF HAS_NET_TIMER_STARTED(MPGlobals.PlayerInteractionData.OpenTimer)
			RESET_NET_TIMER(MPGlobals.PlayerInteractionData.OpenTimer)
			NET_PRINT_TIME()NET_PRINT("     ---------->     FRIEND REQUEST - INPUT_INTERACTION_MENU - NOT PRESSED - RESET OpenTimer - C")NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Launches the Player Interaction Menu script
FUNC BOOL PROCESS_LAUNCH_PI_MENU()	
		
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_PI_MENU")) = 0)
		REQUEST_SCRIPT("AM_PI_MENU")
    	IF (HAS_SCRIPT_LOADED("AM_PI_MENU"))
		    START_NEW_SCRIPT("AM_PI_MENU", INTERACTION_MENU_STACK_SIZE)	//MULTIPLAYER_MISSION_STACK_SIZE)
		    SET_SCRIPT_AS_NO_LONGER_NEEDED("AM_PI_MENU")
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_killPiMenuForPreview")
				RETURN FALSE
			ENDIF
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

