USING "rage_builtins.sch"
USING "globals.sch"

USING "net_prints.sch"
USING "net_activity_selector_groups_common.sch"
USING "context_control_public.sch"

USING "net_heists_public.sch"
USING "net_heists_cutscenes.sch"
USING "Transition_Common.sch"
USING "net_mission_joblist.sch"

// KGM 6/4/14: Include the Debug functionality which contains some TEMP Heist functionality which will get moved to more permanent locations
#IF IS_DEBUG_BUILD
USING "net_activity_selector_debug.sch"
#ENDIF


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   net_activity_selector_groups_heists.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Group-Specific functionality used by Heists.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************

	
// NOTE: This is larger than the Heist Corona size (1.4m), so need to ensure the player always gets into the corona when it appears.
CONST_FLOAT	HEIST_CUTSCENE_TRIGGER_m			3.5
CONST_FLOAT	HEIST_CUTSCENE_TRIGGER_LEEWAY_m		0.5



// ===========================================================================================================
//      Contact Mission Widget Functions
// ===========================================================================================================

// PURPOSE: Setup Contact Mission widgets
#IF IS_DEBUG_BUILD
PROC Create_Heist_Widgets()

	INT tempLoop = 0
	REPEAT MAX_HEIST_PLANNING_MISSIONS tempLoop
		g_DEBUG_WidgetRandomPrepToPlay[tempLoop] = 0
	ENDREPEAT
	g_DEBUG_widgetUseSpecifiedOrder = FALSE
	g_DEBUG_widgetClearAllReplayTimers = FALSE
	g_DEBUG_widgetEachFrameOutputInvitorGH = FALSE

	START_WIDGET_GROUP("Heists")
		START_WIDGET_GROUP("Prep Mission Variation Picker (in Planning Board Order)")
			ADD_WIDGET_BOOL("Use Variation Specified By Widgets?", g_DEBUG_widgetUseSpecifiedOrder)
			REPEAT MAX_HEIST_PLANNING_MISSIONS tempLoop
				ADD_WIDGET_INT_SLIDER("Choose Prep Variation", g_DEBUG_WidgetRandomPrepToPlay[tempLoop], 0, FMMC_MAX_PLANNING_MISSIONS, 1)
			ENDREPEAT
		STOP_WIDGET_GROUP()
		ADD_WIDGET_BOOL("Clear All Heist Replay Delay Timers?", g_DEBUG_widgetClearAllReplayTimers)
		ADD_WIDGET_BOOL("Output SameSession InvitorGH Each Frame?", g_DEBUG_widgetEachFrameOutputInvitorGH)
	STOP_WIDGET_GROUP()
		
ENDPROC
#ENDIF	// IS_DEBUG_BUILD
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      Functions setup by Alastair and moved here to check if the game is having problems saving
// ===========================================================================================================

/// PURPOSE:
///    Search each heist prep mission bit, and if it is set, save the corresponding rContID into the
///    passed in array.
/// PARAMS:
///    iSourceArray - ByREF - Array passed in for filling.
PROC GET_HEIST_COMPLETED_PREP_MISSIONS(INT& iSourceArray[])
	
	INT iArrayPointer
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_SERIESA_STEAL_METH_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Series_A_Steal_Meth 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_SERIESA_COKE_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Series_A_Coke 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_SERIESA_WEED_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Series_A_Weed 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_SERIESA_TRASH_TRUCK_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Series_A_Trash_Truck
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
		
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Series_A_Trash_TruckV2
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
		
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Series_A_Trash_TruckV3
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_SERIESA_BIKERS_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Series_A_Bikers 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
		
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Series_A_Bikers_2 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
		
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Series_A_Bikers_3 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_PACIFIC_STANDARD_VANS_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Pacific_Standard_Vans 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_PACIFIC_STANDARD_CONVOY_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Pacific_Standard_Convoy 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_PACIFIC_STANDARD_BIKES_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Pacific_Standard_Bike 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_PACIFIC_STANDARD_HACK_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Pacific_Standard_Hack 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_PACIFIC_STANDARD_SIGNAL_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Pacific_Standard_Witsec 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_FLEECA_KARUMA_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Tutorial_Car 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_FLEECA_SCOPE_OUT_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Tutorial_Scope_Out 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_PRISON_PLANE_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Prison_Break_Plane 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_PRISON_BUS_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Prison_Break_Bus
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_PRISON_STATION_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Prison_Break_Station 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_PRISON_WET_WORK_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Prison_Break_Unfshd_Biz 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
		
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Prison_Break_Unfshd_ALT 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_HUMANE_VALKYRIE_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Humane_Labs_Valkyrie 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_HUMANE_KEY_CODES_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Humane_Labs_Key_Codes 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_HUMANE_INSURGENTS_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Humane_Labs_Insurgents 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_HUMANE_EMP_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Humane_Labs_EMP 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
		
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Humane_Labs_EMP_v2 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_HUMANE_DELIVER_EMP_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Humane_Labs_Deliver_EMP 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_PREP_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF

ENDPROC


/// PURPOSE:
///    Search each heist finale mission bit, and if it is set, save the corresponding rContID into the
///    passed in array.
/// PARAMS:
///    iSourceArray - ByREF - Array passed in for filling.
PROC GET_HEIST_COMPLETED_FINALE_MISSIONS(INT& iSourceArray[])
	
	INT iArrayPointer
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_SERIESA_FINALE_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_Series_A_Funding 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_FINALE_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_PACIFIC_STANDARD_FINALE_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_The_Pacific_Standard_Job 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_FINALE_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
		
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_The_Pacific_Standard_Job2a 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_FINALE_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_FLEECA_FINALE_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_The_Flecca_Job 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_FINALE_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_PRISON_FINALE_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_The_Prison_Break 
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_FINALE_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF
	
	IF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_HUMANE_FINALE_DONE)
		iSourceArray[iArrayPointer] = g_sMPTunables.iroot_id_HASH_The_Humane_Labs_Raid
		PRINTLN("[AMEC][BCSYNC] - GET_HEIST_COMPLETED_FINALE_MISSIONS - Saving rCondID: ",iSourceArray[iArrayPointer],", in array slot: ", iArrayPointer)
		iArrayPointer++
	ENDIF

ENDPROC


/// PURPOSE:
///    Search the heist cheater bits to calculate the total number of used chances. 
FUNC INT GET_USED_HEIST_ANTICHEAT_CHANCES()
	
	INT iTotalUsedChances
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_FINALE_CHANCE_1)
		iTotalUsedChances++
		PRINTLN("[AMEC][BCSYNC] - GET_USED_HEIST_ANTICHEAT_CHANCES - PSFP_HEIST_FINALE_CHANCE_1 has been USED for a total of: ", iTotalUsedChances)
	ENDIF
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_FINALE_CHANCE_2)
		iTotalUsedChances++
		PRINTLN("[AMEC][BCSYNC] - GET_USED_HEIST_ANTICHEAT_CHANCES - PSFP_HEIST_FINALE_CHANCE_2 has been USED for a total of: ", iTotalUsedChances)
	ENDIF
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_FINALE_CHANCE_3)
		iTotalUsedChances++
		PRINTLN("[AMEC][BCSYNC] - GET_USED_HEIST_ANTICHEAT_CHANCES - PSFP_HEIST_FINALE_CHANCE_3 has been USED for a total of: ", iTotalUsedChances)
	ENDIF
	
	RETURN iTotalUsedChances

ENDFUNC

/// PURPOSE:
///    Search the gangops cheater bits to calculate the total number of used chances. 
FUNC INT GET_USED_GANGOPS_ANTICHEAT_CHANCES()
	
	INT iTotalUsedChances
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_GANGOPS_FINALE_CHANCE_1)
		iTotalUsedChances++
		PRINTLN("[AMEC][BCSYNC] - GET_USED_GANGOPS_ANTICHEAT_CHANCES - PSFP_GANGOPS_FINALE_CHANCE_1 has been USED for a total of: ", iTotalUsedChances)
	ENDIF
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_GANGOPS_FINALE_CHANCE_2)
		iTotalUsedChances++
		PRINTLN("[AMEC][BCSYNC] - GET_USED_GANGOPS_ANTICHEAT_CHANCES - PSFP_GANGOPS_FINALE_CHANCE_2 has been USED for a total of: ", iTotalUsedChances)
	ENDIF
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_GANGOPS_FINALE_CHANCE_3)
		iTotalUsedChances++
		PRINTLN("[AMEC][BCSYNC] - GET_USED_GANGOPS_ANTICHEAT_CHANCES - PSFP_GANGOPS_FINALE_CHANCE_3 has been USED for a total of: ", iTotalUsedChances)
	ENDIF
	
	RETURN iTotalUsedChances

ENDFUNC

/// PURPOSE:
///    Search the casino heist cheater bits to calculate the total number of used chances. 
FUNC INT GET_USED_CASINO_HEIST_ANTICHEAT_CHANCES()
	
	INT iTotalUsedChances
	
	IF IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_CASINO_HEIST_FINALE_CHANCE_1)
		iTotalUsedChances++
		PRINTLN("[AMEC][CASINO_HEIST_ANTICHEAT] - GET_USED_CASINO_HEIST_ANTICHEAT_CHANCES - PSSPS_CASINO_HEIST_FINALE_CHANCE_1 has been USED for a total of: ", iTotalUsedChances)
	ENDIF
	
	IF IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_CASINO_HEIST_FINALE_CHANCE_2)
		iTotalUsedChances++
		PRINTLN("[AMEC][CASINO_HEIST_ANTICHEAT] - GET_USED_CASINO_HEIST_ANTICHEAT_CHANCES - PSSPS_CASINO_HEIST_FINALE_CHANCE_2 has been USED for a total of: ", iTotalUsedChances)
	ENDIF
	
	IF IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_CASINO_HEIST_FINALE_CHANCE_3)
		iTotalUsedChances++
		PRINTLN("[AMEC][CASINO_HEIST_ANTICHEAT] - GET_USED_CASINO_HEIST_ANTICHEAT_CHANCES - PSSPS_CASINO_HEIST_FINALE_CHANCE_3 has been USED for a total of: ", iTotalUsedChances)
	ENDIF
	
	RETURN iTotalUsedChances

ENDFUNC

/// PURPOSE:
///    Search the island heist cheater bits to calculate the total number of used chances. 
FUNC INT GET_USED_ISLAND_HEIST_ANTICHEAT_CHANCES()
	
	INT iTotalUsedChances
	
	IF IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_ISLAND_HEIST_FINALE_CHANCE_1)
		iTotalUsedChances++
		PRINTLN("[AMEC][ISLAND_HEIST_ANTICHEAT] - GET_USED_ISLAND_HEIST_ANTICHEAT_CHANCES - PSSPS_ISLAND_HEIST_FINALE_CHANCE_1 has been USED for a total of: ", iTotalUsedChances)
	ENDIF
	
	IF IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_ISLAND_HEIST_FINALE_CHANCE_2)
		iTotalUsedChances++
		PRINTLN("[AMEC][ISLAND_HEIST_ANTICHEAT] - GET_USED_ISLAND_HEIST_ANTICHEAT_CHANCES - PSSPS_ISLAND_HEIST_FINALE_CHANCE_2 has been USED for a total of: ", iTotalUsedChances)
	ENDIF
	
	IF IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_ISLAND_HEIST_FINALE_CHANCE_3)
		iTotalUsedChances++
		PRINTLN("[AMEC][ISLAND_HEIST_ANTICHEAT] - GET_USED_ISLAND_HEIST_ANTICHEAT_CHANCES - PSSPS_ISLAND_HEIST_FINALE_CHANCE_3 has been USED for a total of: ", iTotalUsedChances)
	ENDIF
	
	RETURN iTotalUsedChances

ENDFUNC


/// PURPOSE:
///    Look in the Prologue Settings bitset for any heist missions that have been started and not finished properly.
/// PARAMS:
///    bFinale - TRUE if the marked mission is a finale.
/// RETURNS:
///    INT RootContentID hash - First one found.
FUNC INT GET_HEIST_FREEMODE_PROLOGUE_SETTING_HASH(BOOL& bFinale)
	
	INT iReturnValue = 0
	
	IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_SERIESA_STEAL_METH_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Series_A_Steal_Meth 
	ELIF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_SERIESA_COKE_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Series_A_Coke 
	ELIF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_SERIESA_WEED_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Series_A_Weed 
	ELIF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_SERIESA_FINALE_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Series_A_Funding 
		bFinale = TRUE
	ELIF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_SERIESA_TRASH_TRUCK_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Series_A_Trash_Truck
	ELIF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_SERIESA_BIKERS_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Series_A_Bikers 
	ELIF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_PACIFIC_STANDARD_VANS_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Pacific_Standard_Vans 
	ELIF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_PACIFIC_STANDARD_CONVOY_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Pacific_Standard_Convoy 
	ELIF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_PACIFIC_STANDARD_BIKES_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Pacific_Standard_Bike 
	ELIF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_PACIFIC_STANDARD_HACK_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Pacific_Standard_Hack 
	ELIF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_PACIFIC_STANDARD_SIGNAL_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Pacific_Standard_Witsec 
	ELIF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_PACIFIC_STANDARD_FINALE_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_The_Pacific_Standard_Job 
		bFinale = TRUE
	ENDIF
	
	PRINTLN("[AMEC][BCSYNC] - GET_HEIST_FREEMODE_PROLOGUE_SETTING_HASH - Returning rootContentID Hash: ", iReturnValue)
	
	RETURN iReturnValue

ENDFUNC


/// PURPOSE:
///    Look in the Job Activity bitset for any heist missions that have been started and not finished properly.
/// PARAMS:
///    bFinale - TRUE if the marked mission is a finale.
/// RETURNS:
///    INT RootContentID hash - First one found.
FUNC INT GET_HEIST_JOB_ACTIVITY_SETTING_HASH(BOOL& bFinale)

	INT iReturnValue = 0
	
	IF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_FLEECA_KARUMA_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Tutorial_Car 
	ELIF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_FLEECA_SCOPE_OUT_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Tutorial_Scope_Out 
	ELIF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_FLEECA_FINALE_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_The_Flecca_Job 
		bFinale = TRUE
	ELIF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_PRISON_PLANE_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Prison_Break_Plane 
	ELIF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_PRISON_BUS_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Prison_Break_Bus
	ELIF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_PRISON_STATION_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Prison_Break_Station 
	ELIF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_PRISON_WET_WORK_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Prison_Break_Unfshd_Biz 
	ELIF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_PRISON_FINALE_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_The_Prison_Break 
		bFinale = TRUE
	ELIF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_HUMANE_VALKYRIE_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Humane_Labs_Valkyrie 
	ELIF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_HUMANE_KEY_CODES_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Humane_Labs_Key_Codes 
	ELIF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_HUMANE_INSURGENTS_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Humane_Labs_Insurgents 
	ELIF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_HUMANE_EMP_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Humane_Labs_EMP 
	ELIF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_HUMANE_DELIVER_EMP_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_Humane_Labs_Deliver_EMP 
	ELIF IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_HUMANE_FINALE_DONE)
		iReturnValue = g_sMPTunables.iroot_id_HASH_The_Humane_Labs_Raid
		bFinale = TRUE
	ENDIF
	
	PRINTLN("[AMEC][BCSYNC] - GET_HEIST_JOB_ACTIVITY_SETTING_HASH - Returning rootContentID Hash: ", iReturnValue)
	
	RETURN iReturnValue
	
ENDFUNC




FUNC BOOL ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE()

	IF g_sMPTunables.bturn_off_save_failing_heist_correction
		PRINTLN("[AMEC][BCSYNC] - ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE = FALSE, TURNED OFF BY THE TUNABLE ")
	
		RETURN FALSE
	ENDIF

	#IF IS_DEBUG_BUILD
	IF g_b_SaveHeistDebug
		PRINTLN("[AMEC][BCSYNC] - ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE = TRUE, g_b_SaveHeistDebug = TRUE ")
		RETURN TRUE
	ENDIF
	#ENDIF

	IF IS_SAVING_HAVING_TROUBLE()
		PRINTLN("[AMEC][BCSYNC] - ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE = TRUE, IS_SAVING_HAVING_TROUBLE: BLOCK! ")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC




// ===========================================================================================================
//      Functions setup by Alastair and moved here to deal with DPADLEFT going OnCall
// ===========================================================================================================

FUNC BOOL REGISTER_FOR_HEIST_QUICK_MATCH_FROM_BOARD()
	IF NOT AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
		SET_TRANSITION_SESSIONS_STARTING_HEIST_QUICK_MATCH()
		SET_TRANSITION_SESSIONS_STARTING_ON_CALL()
		SET_ON_CALL_QUICKMATCH_ATTEMPTS(0)
		SET_TRANSITION_SESSION_HEIST_QUICK_MATCH_TYPE(GET_RANDOM_HEIST_QUICK_MATCH_TYPE())
		SET_TRANSITION_SESSIONS_STARTING_HEIST_QUICK_MATCH_DIRECT()
		PRINTLN(".KGM [Heist][AMEC][HEIST_COMMON] - REGISTER_FOR_HEIST_QUICK_MATCH_FROM_BOARD - Registering for quick match.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC





// ===========================================================================================================
//      General Heist Functions
// ===========================================================================================================

// PURPOSE:	Check if the passed in group array position contains the Heist group
//
// INPUT PARAMS:		paramGroupArrayPos		The position on the Group Array being checked
// RETURN VALUE:		BOOL					TRUE if the passed-in coords represent the Heist Group, FALSE if not
FUNC BOOL Is_This_The_Heist_Group(INT paramGroupArrayPos)
	RETURN (ARE_VECTORS_ALMOST_EQUAL(Get_Group_Coords_For_Group_Slot(paramGroupArrayPos), MP_HEIST_GENERIC_SAFEHOUSE_COORDS))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get a player's safehouseID (from playerBD)
//
// INPUT PARAMS:		paramPlayerID			The playerID
// RETURN VALUE:		INT						Player's SafehouseID
FUNC INT Get_Player_SafehouseID(PLAYER_INDEX paramPlayerID)
	RETURN (GlobalplayerBD[NATIVE_TO_INT(paramPlayerID)].playerActivitySelector.aspbHeistGeneral.hgcSafehouseID)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player has a safehouse
//
// INPUT PARAMS:		paramPlayerID			The playerID
// RETURN VALUE:		BOOL					TRUE if player owns a savehouse, otherwise FALSE
FUNC BOOL Does_Player_Own_Safehouse(PLAYER_INDEX paramPlayerID)

	INT safehouseID = Get_Player_SafehouseID(paramPlayerID)
	
	IF (safehouseID = NO_OWNED_SAFEHOUSE)
		RETURN FALSE
	ENDIF
	
	IF (IS_PROPERTY_ONLY_A_GARAGE(safehouseID))
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve a mission contact point location from the player's safehouse
//
// RETURN VALUE:		VECTOR					The contact point coords for the safehouse owned by the player
FUNC VECTOR Get_Contact_Point_Coords_In_This_Players_Safehouse()

	// OLD WAY - NOT NEEDED FOR NEW HEIST ROUTINES

	IF NOT (Does_Player_Own_Safehouse(PLAYER_ID()))
		RETURN (<< 0.0, 0.0, 0.0 >>)
	ENDIF
	
	INT mySafehouseID = Get_Player_SafehouseID(PLAYER_ID())
	
	VECTOR heistPosition = mpProperties[mySafehouseID].house.vHeistPlanningLocate
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][Heist]: Heist Planning Coords: ")
		NET_PRINT_VECTOR(heistPosition)
		NET_PRINT("  [Safehouse ID: ")
		NET_PRINT_INT(mySafehouseID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
	RETURN (heistPosition)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Heist cross-session Data needs to be braodcast to joining players in other sessions?
//
// REFERENCE PARAMS:		refHeistDataCS		An INT representing this player's propertyID and if corona is for cutscene (used to let cross-session joining players know)
// RETURN VALUE:			BOOL				TRUE if the data needs transmitted, otherwise FALSE
//
// NOTES:	The data should only be transmitted once when the player walks into their own heist mission corona, this function will then clear the data to prevent it being sent again
FUNC BOOL Get_Heist_CS_Data_Cross_Session_Transmission(INT &refHeistDataCS)

	IF (g_sLocalMPHeistCSData.lhcsDataToBeTransmittedToOthers = EMPTY_HEIST_CROSS_SESSION_DATA)
		// Don't Tranmit
		RETURN FALSE
	ENDIF
	
	// The person that transmits the data doesn't receive the event themselves, so storing the data here so that it is available after transition
	PRINTLN(".KGM [ActSelect][Heist][HCorona]: About to send Heist Cross-session transmission data. Storing locally for the Leader first.")
	Store_Heist_Data_Received_By_Cross_Session_Transmission(g_sLocalMPHeistCSData.lhcsDataToBeTransmittedToOthers)
		// FEATURE_HEIST_PLANNING
	
	// Pass back for transmission then clear
	refHeistDataCS = g_sLocalMPHeistCSData.lhcsDataToBeTransmittedToOthers
	
	Clear_Heist_Data_Stored_For_Cross_Session_Transmission()
		// FEATURE_HEIST_PLANNING
	
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the ApartmentID in which to place a Heist corona for cross-session joining
//
// RETURN VALUE:		VECTOR						The Corona Coordinates for the Heist triggerer's apartmentID
FUNC VECTOR Get_Heist_Coordinates_From_Cross_Session_ApartmentID()

	INT		theApartmentID	= g_sLocalMPHeistCSData.lhcsHeistApartmentReceivedCS
	VECTOR	heistPosition	= mpProperties[theApartmentID].house.vHeistPlanningLocate

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][Heist]: Heist Triggerer's Apartment Corona Coords: ")
		NET_PRINT_VECTOR(heistPosition)
		NET_PRINT("  [ApartmentID: ")
		NET_PRINT_INT(theApartmentID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
	RETURN (heistPosition)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the state of the new g_sPlayerAcceptedCrossSessionInviteToHeist flag
//
// INPUT PARAMS:		paramCSInviteIntoProperty		TRUE if this was a cross-session invite into a property, otherwise FALSE
//
// NOTES:	This gets set if the player has just accepted a Cross-Session Invite into a Heist in a property. It gets read by Conor allowing
//				him to create a delay within the apartment while he waits for more data to be broadcast to this player.
PROC Set_Player_Accepted_CSInvite_Into_Heist_Corona(BOOL paramCSInviteIntoProperty)

	g_sPlayerAcceptedCrossSessionInviteToHeist = paramCSInviteIntoProperty
	
	IF (g_sPlayerAcceptedCrossSessionInviteToHeist)
		PRINTLN(".KGM [ActSelect][Heist]: g_sPlayerAcceptedCrossSessionInviteToHeist set to TRUE")
		EXIT
	ENDIF
	
	PRINTLN(".KGM [ActSelect][Heist]: g_sPlayerAcceptedCrossSessionInviteToHeist set to FALSE - this may be just prior to it getting set to TRUE")
	
ENDPROC





// ===========================================================================================================
//      Heist Apartment Access Functions
// ===========================================================================================================

// PURPOSE: Check if a PropertyID is a valid property for a Heist
//
// INPUT PARAMS:		paramPropertyID			The PropertyID
// RETURN VALUE:		BOOL					TRUE if this propertyID is suitable for Heists, otherwise FALSE
FUNC BOOL Is_This_PropertyID_Suitable_For_Heists(INT paramPropertyID)
	RETURN (GET_PROPERTY_SIZE_TYPE(paramPropertyID) = PROP_SIZE_TYPE_LARGE_APT)
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Gets the players current apartment
//
// RETURN VALUE:		INT					The apartment ID
FUNC INT Get_Players_Current_Apartment()
	RETURN (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the apartment the local player is in is owned by the specified player
//
// INPUT PARAM:		PLAYER_INDEX			The player whose corona I'm trying to join
// RETURN VALUE:	BOOL					TRUE if the apartment the local player is in belongs to the other player
//
// NOTES:	Assumes pre-checks have been done to ensure both players are in the same apartment instance and that the playerID passed as parameter is valid
FUNC BOOL Does_This_Apartment_Belong_To_Other_Player(PLAYER_INDEX paramPlayer)
	
	GAMER_HANDLE otherGH = GET_GAMER_HANDLE_PLAYER(paramPlayer)
	IF NOT (IS_GAMER_HANDLE_VALID(otherGH))
		RETURN FALSE
	ENDIF
	
	GAMER_HANDLE ownerGH
	GET_GAMER_HANDLE_OF_PROPERTY_OWNER(PLAYER_ID(), ownerGH)
	IF NOT (IS_GAMER_HANDLE_VALID(ownerGH))
		RETURN FALSE
	ENDIF
	
	RETURN (NETWORK_ARE_HANDLES_THE_SAME(otherGH, ownerGH))
	
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the player's currently occupied own apartment is suitable for Heists
//
// RETURN VALUE:		BOOL				TRUE if the apartment is suitable for Heists, otherwise FALSE
FUNC BOOL Is_Players_Currently_Occupied_Own_Apartment_Suitable_For_Heists()

	// In own apartment?
	IF NOT (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE))
		RETURN FALSE
	ENDIF
	
	RETURN (Is_This_PropertyID_Suitable_For_Heists(Get_Players_Current_Apartment()))
	
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player owns a property suitable for heists
//
// RETURN VALUE:		BOOL					TRUE if the player owns a suitable property for Heists, FALSE if not
FUNC BOOL Does_Player_Own_An_Apartment_Suitable_For_Heists()

	INT tempLoop		= 0
	INT thisApartment	= 0
	
	REPEAT MAX_OWNED_PROPERTIES tempLoop
		thisApartment = GET_OWNED_PROPERTY(tempLoop)
		IF (thisApartment != NO_OWNED_SAFEHOUSE)
			IF (Is_This_PropertyID_Suitable_For_Heists(thisApartment))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Player doesn't own an apartment suitable for Heists
	RETURN FALSE
	
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the heist corona location within the player's owned apartment that they are currently in
//
// RETURN VALUE:		VECTOR					The contact point coords for the safehouse owned by the player
FUNC VECTOR Get_Heist_Corona_Coords_For_Players_Currently_Occupied_Apartment()

	// Safety Check - this shouldn't get called if the player is not in own apartment
	IF NOT (Is_Players_Currently_Occupied_Own_Apartment_Suitable_For_Heists())
	AND NOT SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [ActSelect][Heist]: Get_Heist_Corona_Coords_For_Players_Currently_Occupied_Apartment() has returned FALSE")
			
			IF NOT (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE))
				PRINTLN(".KGM [ActSelect][Heist]: ...because player is not in own apartment")
			ELSE
				PRINTLN(".KGM [ActSelect][Heist]: ...because Player's current apartment: ", Get_Players_Current_Apartment(), " can't be suitable for Heists")
			ENDIF
		#ENDIF
		
		RETURN (<< 0.0, 0.0, 0.0 >>)
	ENDIF

	// Retrueve the planning location within the player's current apartment
	INT playerSafehouseID = Get_Players_Current_Apartment()
	
	VECTOR heistPosition = mpProperties[playerSafehouseID].house.vHeistPlanningLocate
	RETURN (heistPosition)
	
ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if there is a general Heist delay
//
// RETURN VALUE:		BOOL			TRUE if there is a general heist delay, FALSE if not
FUNC BOOL Is_There_A_General_Reason_To_Delay_Heist_Related_Activities()

	IF NOT (Does_Player_Own_An_Apartment_Suitable_For_Heists())
		PRINTLN(".KGM [Heist][Delay]: Player no longer has an apartment suitable for Heists")
		RETURN TRUE
	ENDIF
	
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		PRINTLN(".KGM [Heist][Delay]: Player has a Focus Mission")
		RETURN TRUE
	ENDIF
	
	IF (IS_PLAYER_IN_CORONA())
		PRINTLN(".KGM [Heist][Delay]: Player is in a corona")
		RETURN TRUE
	ENDIF
	
	IF (IS_TRANSITION_SESSION_RESTARTING())
		PRINTLN(".KGM [Heist][Delay]: Transition Session is Restarting")
		RETURN TRUE
	ENDIF
	
	IF (IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED())
		PRINTLN(".KGM [Heist][Delay]: Transition Session Invite is being actioned (ie: JIP)")
		RETURN TRUE
	ENDIF
	
	IF (NETWORK_IS_ACTIVITY_SESSION())
		PRINTLN(".KGM [Heist][Delay]: Player is in an Activity Session")
		RETURN TRUE
	ENDIF
	
	// KGM 3/8/15 [BUG 2454974]: Block if the player is critical on an FM Event
	IF (IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID()))
		PRINTLN(".KGM [Heist][Delay]: Player is Critical to an FM Event")
		RETURN TRUE
	ENDIF
	
	// KGM 3/8/15 [BUG 2455836]: Block if the player is on a Time Trial
	IF (IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL))
		PRINTLN(".KGM [Heist][Delay]: Player is on a Time Trial")
		RETURN TRUE
	ENDIF
	
	IF (IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RC_TIME_TRIAL))
		PRINTLN(".KGM [Heist][Delay]: Player is on an RC Time Trial")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_ANY_PART_OF_CASINO(PLAYER_ID())
		PRINTLN(".KGM [Heist][Delay]: Player is inside part of the casino")
		RETURN TRUE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
	AND NOT SHOULD_HIDE_AMBIENT_EVENT(GET_iPI_TYPE_M3_CONST_FROM_FM_EVENT_TYPE(FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID())))
		PRINTLN(".KGM [Heist][Delay]: FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT AND NOT SHOULD_HIDE_AMBIENT_EVENT")
		RETURN TRUE
	ENDIF
	
	IF NOT (IS_SKYSWOOP_AT_GROUND())
		PRINTLN(".KGM [Heist][Delay]: Skyswoop is not At Ground")
		RETURN TRUE
	ENDIF
	
	// KGM 23/10/14 [BUG 2091246]: Ensure the phonecall doesn't occur during the 'intro to apartment' Cutscene
	IF (NETWORK_IS_IN_MP_CUTSCENE())
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [Heist][Delay]: Player is in an MP Cutscene (potentially the 'intro to apartment' cutscene)")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF NOT HAS_LESTER_INTRO_TO_HEISTS_CUTSCENE_BEEN_DONE()
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [Heist][Delay]: Not seen the Lester cutscene at warehouse")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	// No general delay
	RETURN FALSE
	
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if there is a reason to delay Heist Activation
//
// RETURN VALUE:		BOOL			TRUE if there is a reason, FALSE if not
FUNC BOOL Is_There_A_Reason_To_Delay_New_Heist_Strand()

	IF (Is_There_A_General_Reason_To_Delay_Heist_Related_Activities())
		RETURN TRUE
	ENDIF
	
	// Ignore if the player has already downloaded an 'on call' mission
	IF (AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL())
		PRINTLN(".KGM [Heist][Delay]: Player is transitioning into an On Call mission")
		RETURN TRUE
	ENDIF
	
	// Ignore the Cooldown Period (a 10-minute period that affects all players that have just completed a Heist Finale) for replays
	IF NOT (Has_New_Heist_Strand_Cooldown_Period_Elapsed())
		IF (g_replayHeistFlowOrder = UNKNOWN_HEIST_FLOW_ORDER)
			// ...not a replay request, so obey cooldown delay
			#IF IS_DEBUG_BUILD
				PRINTLN(".KGM [Heist][Delay]: New Heist Strand CoolDown Period has not elapsed. Delay Remaining: ", (g_heistCooldownTimeout - GET_GAME_TIMER()), "msec")
			#ENDIF
			RETURN TRUE
		ELSE
			PRINTLN(".KGM [Heist][Delay]: New Heist Strand CoolDown Period has not elapsed. BUT ALLOW ANYWAY: This is a Heist Replay Request, so no cooldown delay.")
		ENDIF
	ENDIF
	
	// No reason to delay the new heist strand
	RETURN FALSE
	
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Returns whether or not the new Heist Replay Board has been unlocked
//
// RETURN VALUE:			BOOL		TRUE if the replay board has been unlocked, otherwise FALSE
//
// NOTES:	MJM 12/2/15: Performs the various checks to see if the replay board is unlocked
//			KGM 15/2/15 [BUG 2235286]: Modified to take Lester's Intro Cutscene into account - player needs to have seen it (also solo sessions)
FUNC BOOL Has_Heist_Replay_Board_Been_Unlocked()

	// Clear the optimisation variable
	// Ignore if Solo Session
    IF (g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY = FM_SESSION_MENU_CHOICE_JOIN_CLOSED_SOLO_PLAYABLE_SESSION)
		PRINTLN(".KGM [Heist]: Has_Heist_Replay_Board_Been_Unlocked() - RETURN FALSE (solo session)")
		
		IF (g_hasReplayBoardBeenUnlocked)
			PRINTLN(".KGM [Heist]: Has_Heist_Replay_Board_Been_Unlocked() - Clearing optimisation global: g_hasReplayBoardBeenUnlocked = FALSE (solo session)")
			g_hasReplayBoardBeenUnlocked = FALSE
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Ensure player has completed Lester's 'intro to heists' cutscene
	IF NOT (Has_Intro_To_Heists_Been_Done())
		PRINTLN(".KGM [Heist]: Has_Heist_Replay_Board_Been_Unlocked() - RETURN FALSE (Lester's 'intro to heists' cutscene hasn't been done)")
		
		IF (g_hasReplayBoardBeenUnlocked)
			PRINTLN(".KGM [Heist]: Has_Heist_Replay_Board_Been_Unlocked() - Clearing optimisation global: g_hasReplayBoardBeenUnlocked = FALSE (Lester's 'intro to heists' cutscene hasn't been done)")
			g_hasReplayBoardBeenUnlocked = FALSE
		ENDIF
		
		RETURN FALSE
	ENDIF

	// Have all Heists been completed as Leader?
	INT flowOrderLoop = 0
	INT heistRCID = 0
	
	REPEAT MAX_HEIST_FLOW_ORDER flowOrderLoop
		heistRCID = Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(flowOrderLoop)
		IF (GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(heistRCID, TRUE) <= 0)
			PRINTLN(".KGM [Heist]: Has_Heist_Replay_Board_Been_Unlocked() - RETURN FALSE (Heist not complete as Leader). RCID: ", heistRCID)
		
			IF (g_hasReplayBoardBeenUnlocked)
				PRINTLN(".KGM [Heist]: Has_Heist_Replay_Board_Been_Unlocked() - Clearing optimisation global: g_hasReplayBoardBeenUnlocked = FALSE (Heist not complete as Leader)")
				g_hasReplayBoardBeenUnlocked = FALSE
			ENDIF
		
			RETURN FALSE
		ENDIF
	ENDREPEAT

	// Set the optimisation variable
	IF NOT (g_hasReplayBoardBeenUnlocked)
		PRINTLN(".KGM [Heist]: Has_Heist_Replay_Board_Been_Unlocked() - Setting optimisation global: g_hasReplayBoardBeenUnlocked = TRUE")
		g_hasReplayBoardBeenUnlocked = TRUE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Update the optimisation global for the Heist Replay Board being unlocked
// NOTE:	The return value here is not strictly required since the chekcing function also updates the global each time the checking function is called directly.
//				This function is just for ease so that we can ignore the other function's return value.
PROC Update_Optimisation_Global_Heist_Replay_Board_Unlocked()

	PRINTLN(".KGM [Heist]: Update_Optimisation_Global_Heist_Replay_Board_Unlocked() - OPTIMISATION CHECK: Storing result in g_hasReplayBoardBeenUnlocked.")
	
	g_hasReplayBoardBeenUnlocked = Has_Heist_Replay_Board_Been_Unlocked()
	
	#IF IS_DEBUG_BUILD
		IF (g_hasReplayBoardBeenUnlocked)
			PRINTLN(".KGM [Heist]: Update_Optimisation_Global_Heist_Replay_Board_Unlocked(): ...g_hasReplayBoardBeenUnlocked = TRUE")
		ELSE
			PRINTLN(".KGM [Heist]: Update_Optimisation_Global_Heist_Replay_Board_Unlocked(): ...g_hasReplayBoardBeenUnlocked = FALSE")
		ENDIF
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Returns whether or not the new Heist Replay Board has been unlocked using the optimisation global
//
// RETURN VALUE:			BOOL		TRUE if the replay board has been unlocked, otherwise FALSE
//
// NOTES:	KGM 18/2/15 [BUG 2239042]: Returns the result of the optimisation variable - used by Conor to prevent calling the above function every frame
FUNC BOOL Optimised_Has_Heist_Replay_Board_Been_Unlocked()
	RETURN (g_hasReplayBoardBeenUnlocked)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Returns whether or not the new Heist Replay Board should be displayed in teh player's apartment
//
// RETURN VALUE:			BOOL		TRUE if the replay board should be displayed, otherwise FALSE
//
// NOTES:	KGM 9/2/15 [BUG 2226481]: Performs the various checks to see if the replay board should be available
FUNC BOOL Should_Heist_Replay_Board_Be_Displayed_In_Apartment()

	#IF IS_DEBUG_BUILD
	// Allow players to disable / enable board through CTRL + NUM9
	IF g_HeistStrandClient.bDebugDisableBoard
		RETURN FALSE
	ENDIF
	#ENDIF

	// Ensure the player is in their own apartment
	IF NOT (Is_Players_Currently_Occupied_Own_Apartment_Suitable_For_Heists())
		PRINTLN(".KGM [Heist]: Should_Heist_Replay_Board_Be_Displayed_In_Apartment() - RETURN FALSE (player not in own apartment suitable for Heists)")
		RETURN FALSE
	ENDIF

	// Ensure there isn't already a post-phonecall heist
	IF (Is_There_A_Heist_Available_In_Apartment())
		PRINTLN(".KGM [Heist]: Should_Heist_Replay_Board_Be_Displayed_In_Apartment() - RETURN FALSE (there is already a post-phonecall Heist)")
		RETURN FALSE
	ENDIF
	
	// Ensure all Heists are complete as Leader
	IF NOT (Has_Heist_Replay_Board_Been_Unlocked())
		PRINTLN(".KGM [Heist]: Should_Heist_Replay_Board_Be_Displayed_In_Apartment() - RETURN FALSE (Heist replay board isn't unlocked)")
		RETURN FALSE
	ENDIF
	
	// Allow the replay board
	PRINTLN(".KGM [Heist]: Should_Heist_Replay_Board_Be_Displayed_In_Apartment() - RETURN TRUE (Replay Board allowed)")
	RETURN TRUE

ENDFUNC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      'Another Heist Soon' Help Text Access Functions
// ===========================================================================================================

// PURPOSE:	Activate 'another heist soon' help text timeout if the help text is needed shortly after Heist completion
PROC Activate_Another_Heist_Soon_Help_Text()

	PRINTLN(".KGM [ActSelect][Heist]: Checking if 'Another Heist Soon' help text is required.")
	
	IF NOT (Has_Intro_To_Heists_Been_Done())
		PRINTLN(".KGM [ActSelect][Heist]: ...NO - Intro To Heists hasn't been done")
		EXIT
	ENDIF

	// Find out what the next Heist would be
	INT nextHeistFlowValue	= GET_MP_INT_CHARACTER_STAT(MP_STAT_CURRENT_HEIST_STRAND)
	INT nextHeistHashRCID	= Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(nextHeistFlowValue)
	
	IF (GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(nextHeistHashRCID, TRUE) > 0)
		PRINTLN(".KGM [ActSelect][Heist]: ...NO - Next Heist (from STAT) has already been completed as Leader")
		EXIT
	ENDIF
	
	// Activate the 'another heist soon' help text
	PRINTLN(".KGM [ActSelect][Heist]: ...YES - Next Heist (from STAT) hasn't been completed as Leader. Display in ", ANOTHER_HEIST_SOON_INITIAL_DELAY_msec, "msec")
	
	g_sHeistHelpText.hhtAnotherHeistSoonRequired	= TRUE
	g_sHeistHelpText.hhtAnotherHeistSoonTimeout		= GET_GAME_TIMER() + ANOTHER_HEIST_SOON_INITIAL_DELAY_msec

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the 'another heist soon' timer
PROC Clear_Another_Heist_Soon_Help_Text_Controls()

	PRINTLN(".KGM [ActSelect][Heist]: Clear_Another_Heist_Soon_Help_Text_Controls()")
	
	g_sHeistHelpText.hhtAnotherHeistSoonRequired	= FALSE
	g_sHeistHelpText.hhtAnotherHeistSoonTimeout		= GET_GAME_TIMER()
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the 'Another Heist Soon' help text
PROC Maintain_Another_Heist_Soon_Help_Text()

	IF NOT (g_sHeistHelpText.hhtAnotherHeistSoonRequired)
		EXIT
	ENDIF
	
	IF (GET_GAME_TIMER() < g_sHeistHelpText.hhtAnotherHeistSoonTimeout)
		EXIT
	ENDIF
	
	Clear_Another_Heist_Soon_Help_Text_Controls()
	
	// Timeout has occurred so display the help text if possible
	PRINTLN(".KGM [ActSelect][Heist]: 'Another Heist Soon' help text timer has elapsed. Display?")
	
	// Is the stage suitable for displaying the help message?
	BOOL allowDisplayHelp = FALSE
	SWITCH (g_sLocalMPHeistControl.lhcStage)
		CASE HEIST_CLIENT_STAGE_NO_HEIST
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY
			allowDisplayHelp = TRUE
			BREAK
	ENDSWITCH
	
	IF NOT (allowDisplayHelp)
	OR (IS_HELP_MESSAGE_BEING_DISPLAYED())
	OR (Is_There_A_General_Reason_To_Delay_Heist_Related_Activities())
		// ...setup to try again soon
		IF NOT (allowDisplayHelp)
			PRINTLN(".KGM [ActSelect][Heist]: ...'Another Heist Soon' Help Text is not allowed due to help text specific delay.")
		ENDIF
		
		IF (IS_HELP_MESSAGE_BEING_DISPLAYED())
			PRINTLN(".KGM [ActSelect][Heist]: ...'Another Heist Soon' Help Text is already on display.")
		ENDIF
		
		PRINTLN(".KGM [ActSelect][Heist]: A display delay is required. Try again in ", ANOTHER_HEIST_SOON_QUICK_DELAY_msec, "msec")
		
		g_sHeistHelpText.hhtAnotherHeistSoonRequired	= TRUE
		g_sHeistHelpText.hhtAnotherHeistSoonTimeout		= GET_GAME_TIMER() + ANOTHER_HEIST_SOON_QUICK_DELAY_msec
	
		#IF IS_DEBUG_BUILD
			IF (GET_COMMANDLINE_PARAM_EXISTS("sc_debugQuickHeists"))
				INT i = ANOTHER_HEIST_SOON_QUICK_DELAY_msec
				IF (i > 30000)
					g_sHeistHelpText.hhtAnotherHeistSoonTimeout = GET_GAME_TIMER() + 30000
					PRINTLN(".KGM [ActSelect][Heist]: ...-sc_debugQuickHeists is active. Cut 'Another Heist Soon' Help Text retry delay to 30000msec")
				ENDIF
			ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	// "Lester will be in touch soon with details of another Heist."
	PRINTLN(".KGM [ActSelect][Heist]: ...YES")
	PRINT_HELP("HEIST_NEXT_SOON")
	
ENDPROC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      Heist Replay Reminder Help Text Access Functions
// ===========================================================================================================

// PURPOSE:	Clear out the heist 'replay reminder' help text control variables
PROC Clear_Heist_Replay_Reminder_Help_Text_Controls()

	g_sHeistHelpText.hhtHeistReplayReminderRequired		= FALSE
	g_sHeistHelpText.hhtHeistReplayReminderTimeout		= 0

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store Replay Reminder help text if required
//
// INPUTS:			paramHashRCID		The Heist Finale Hash rootContentID
PROC Store_Heist_Replay_Reminder_Help(INT paramHashRCID)

	CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_REPLAY_HEIST_HELP_TEXT_REQUIRED)
	CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_LEADER_REPLAY_HELP_BOARD_AVAILABLE)
	CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_REPLAY_HELP_ALL_FINALES_COMPLETE)

	// Display it the first time the Prison Heist is completed (don't set any supporting bits - just use original help text)
	IF (Is_This_Prison_Heist_RCID_Hash(paramHashRCID))
		IF (GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(paramHashRCID, TRUE) = 0)
			SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_REPLAY_HEIST_HELP_TEXT_REQUIRED)
			PRINTLN(".KGM [ActSelect][Heist]: Store_Heist_Replay_Reminder_Help(): Prison Heist completed for first time. Request Heist Replay Reminder")
		ELSE
			PRINTLN(".KGM [ActSelect][Heist]: Store_Heist_Replay_Reminder_Help(): Prison Heist previously completed. Replay Reminder help text is not required. Completed: ", GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(paramHashRCID, TRUE))
		ENDIF
		
		EXIT
	ENDIF

	// Display it the first time the last Heist is completed as Leader (use different help text, so set a supporting bit)
	IF (Is_This_Ornate_Heist_RCID_Hash(paramHashRCID))
		IF (GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(paramHashRCID, TRUE) = 0)
			SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_REPLAY_HEIST_HELP_TEXT_REQUIRED)
			SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_LEADER_REPLAY_HELP_BOARD_AVAILABLE)
			PRINTLN(".KGM [ActSelect][Heist]: Store_Heist_Replay_Reminder_Help(): Pacific Standard Heist completed for first time. Request Heist Replay Reminder")
		ELSE
			PRINTLN(".KGM [ActSelect][Heist]: Store_Heist_Replay_Reminder_Help(): Pacific Standard Heist previously completed. Replay Reminder help text is not required. Completed: ", GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(paramHashRCID, TRUE))
		ENDIF
		
		EXIT
	ENDIF
	
	PRINTLN(".KGM [ActSelect][Heist]: Store_Heist_Replay_Reminder_Help(): Replay Reminder help text is not required")

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a Replay Reminder help text is required and activate it
PROC Activate_Heist_Replay_Reminder_Help()
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_REPLAY_HEIST_HELP_TEXT_REQUIRED))
		g_sHeistHelpText.hhtHeistReplayReminderRequired		= TRUE
		
		// KGM 20/2/15 [BUG 2244429]: The 'all finales complete' version of the help text should be displayed sooner
		IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_REPLAY_HELP_ALL_FINALES_COMPLETE))
			g_sHeistHelpText.hhtHeistReplayReminderTimeout		= GET_GAME_TIMER() + REPLAY_REMINDER_ALL_FINALES_DELAY_msec
			PRINTLN(".KGM [Heist]: Activate_Heist_Replay_Reminder_Help(): 'All Finales Complete' version delay: ", REPLAY_REMINDER_ALL_FINALES_DELAY_msec, " msec")
		ELSE
			g_sHeistHelpText.hhtHeistReplayReminderTimeout		= GET_GAME_TIMER() + REPLAY_REMINDER_INITIAL_DELAY_msec
			PRINTLN(".KGM [Heist]: Activate_Heist_Replay_Reminder_Help(): Delay: ", REPLAY_REMINDER_INITIAL_DELAY_msec, " msec")
		ENDIF
		
		EXIT
	ENDIF
	
	PRINTLN(".KGM [Heist]: Activate_Heist_Replay_Reminder_Help(): ...not required - clearing control variables")
	Clear_Heist_Replay_Reminder_Help_Text_Controls()

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	
PROC Maintain_Heist_Replay_Reminder_Help_Text()

	IF NOT (g_sHeistHelpText.hhtHeistReplayReminderRequired)
		EXIT
	ENDIF
	
	IF (GET_GAME_TIMER() < g_sHeistHelpText.hhtHeistReplayReminderTimeout)
		EXIT
	ENDIF
	
	Clear_Heist_Replay_Reminder_Help_Text_Controls()
	
	// Timeout has occurred so display the help text if possible
	PRINTLN(".KGM [Heist]: 'Replay Reminder' help text timer has elapsed. Display?")
	
	// Is the stage suitable for displaying the help message?
	BOOL allowDisplayHelp = FALSE
	SWITCH (g_sLocalMPHeistControl.lhcStage)
		CASE HEIST_CLIENT_STAGE_NO_HEIST
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY
			allowDisplayHelp = TRUE
			BREAK
	ENDSWITCH
	
	// KGM 20/2/15 [BUG 2244429]: Don't delay the 'Leader or Crew' reminder even if the player has an active Heist
	IF NOT (allowDisplayHelp)
		// ...this is the 'leader or crew' help - used when all Finale's are complete even if not all completed as Leader
		IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_REPLAY_HELP_ALL_FINALES_COMPLETE))
			PRINTLN(".KGM [Heist]: ...(NOTE: Player must have an active heist, but this is the 'all finales completed' version of the help and shouldn't be delayed by that.")
			allowDisplayHelp = TRUE
		ENDIF
	ENDIF
	
	IF NOT (allowDisplayHelp)
	OR (IS_HELP_MESSAGE_BEING_DISPLAYED())
	OR (Is_There_A_General_Reason_To_Delay_Heist_Related_Activities())
		// ...setup to try again soon
		IF NOT (allowDisplayHelp)
			PRINTLN(".KGM [Heist]: ...'Replay Reminder' Help Text is not allowed due to an already active Heist Strand.")
		ENDIF
		
		IF (IS_HELP_MESSAGE_BEING_DISPLAYED())
			PRINTLN(".KGM [Heist]: ...'Replay Reminder' Help Text is already on display.")
		ENDIF
		
		g_sHeistHelpText.hhtHeistReplayReminderRequired		= TRUE
		
		IF NOT (allowDisplayHelp)
			// ...player is on a Heist Strand, so delay the help text reminder using the initial delay
			PRINTLN(".KGM [Heist]: A display delay is required because of an active Heist Strand. Try again in ", REPLAY_REMINDER_INITIAL_DELAY_msec, "msec")
			g_sHeistHelpText.hhtHeistReplayReminderTimeout		= GET_GAME_TIMER() + REPLAY_REMINDER_INITIAL_DELAY_msec
		ELSE
			// ...delay the help text reminder using the quick delay
			PRINTLN(".KGM [Heist]: A display delay is required. Try again in ", REPLAY_REMINDER_QUICK_DELAY_msec, "msec")
			g_sHeistHelpText.hhtHeistReplayReminderTimeout		= GET_GAME_TIMER() + REPLAY_REMINDER_QUICK_DELAY_msec
		ENDIF
	
		#IF IS_DEBUG_BUILD
			IF (GET_COMMANDLINE_PARAM_EXISTS("sc_debugQuickHeists"))
				INT iMsec = REPLAY_REMINDER_QUICK_DELAY_msec
				IF (iMsec > 40000)
					g_sHeistHelpText.hhtHeistReplayReminderTimeout = GET_GAME_TIMER() + 40000
					PRINTLN(".KGM [Heist]: ...-sc_debugQuickHeists is active. Cut 'Replay Reminder' help text retry delay to 40000msec")
				ENDIF
			ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	// Choose which replay reminder help text to display
	// ...leader has just completed all Strands?
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_LEADER_REPLAY_HELP_BOARD_AVAILABLE))
		// "You have completed all Heists. In addition to calling Lester, the planning board in your high-end Apartment can now be used to replay a Heist of your choice."
		PRINTLN(".KGM [Heist]: ...YES (Leader has unlocked Heist Replays on the Planning Board)")
		PRINT_HELP("HEIST_REPLAY_L")
		EXIT
	ENDIF

	// ...leader or Crew is unlocking replay board because they have all Finale awards?
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_REPLAY_HELP_ALL_FINALES_COMPLETE))
		// "You have completed all Heists as a Leader or a Crew Member. You are now able to call Lester to replay a Heist or launch them from the Replay Planning Board in a high-end Apartment."
		PRINTLN(".KGM [Heist]: ...YES (Leader or Crew Member has unlocked Heist Replays on the Planning Board by having all Finale awards)")
		PRINT_HELP("HEIST_REPLAY_FIN")
		EXIT
	ENDIF

	// ...Leader has completed Prison for the first time
	// "Call Lester to request a Heist replay. Each Heist is available for replay on the phone after completing them as Leader. There is a delay before you can replay the same Heist again."
	PRINTLN(".KGM [Heist]: ...YES (Leader has unlocked Heist Replays on phone)")
	PRINT_HELP("HEIST_REPLAY")
	
ENDPROC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      Heist Versus Mission Triggering from Pause Menu Help Text Access Functions
// ===========================================================================================================

// PURPOSE: Allow the Versus Pause Menu help text to be displayed if the other display conditions are matched
//
// INPUTS:			paramHashRCID		The Heist Finale Hash rootContentID
PROC Store_Heist_Allow_Versus_Pause_Menu_Triggering_Help(INT paramHashRCID)

	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_ALLOW_DISPLAY_VERSUS_PAUSE_HELP))
		PRINTLN(".KGM [Heist]: Store_Heist_Allow_Versus_Pause_Menu_Help(): Versus Triggering from Pause Menu help text is already allowed.")
		EXIT
	ENDIF

	// Allow it after the first time the Tutorial Heist is completed 
	IF (Is_This_Tutorial_Heist_RCID_Hash(paramHashRCID))
		IF (GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(paramHashRCID, TRUE) = 0)
			SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_ALLOW_DISPLAY_VERSUS_PAUSE_HELP)
			
			// May as well clear the control flags too - although they should already be cleared
			g_sHeistHelpText.hhtHeistVersusPauseHelpOnVersus	= FALSE
			g_sHeistHelpText.hhtHeistVersusPauseHelpRequired	= FALSE
			g_sHeistHelpText.hhtHeistVersusPauseHelpTimeout		= 0
			
			PRINTLN(".KGM [Heist]: Store_Heist_Allow_Versus_Pause_Menu_Help(): Fleeca Tutorial completed for first time. Allowing Versus Triggering from Pause Menu help.")
		ENDIF
		
		EXIT
	ENDIF
	
	PRINTLN(".KGM [Heist]: Store_Heist_Allow_Versus_Pause_Menu_Help(): Versus Triggering from Pause Menu help text is not required")

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check for the conditions that would allow help text to explain Versus Mission triggering from the Pause Menu
PROC Maintain_Versus_Pause_Menu_Triggering_Help_Text()

	// Is the Versus Pause Menu Triggering help text allowed?
	IF NOT (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_ALLOW_DISPLAY_VERSUS_PAUSE_HELP))
		EXIT
	ENDIF
	
	// Is the Versus Pause Menu Triggering help text currently required (this will be true after any Versus mission has been played if the help text was allowed at the time)
	IF NOT (g_sHeistHelpText.hhtHeistVersusPauseHelpRequired)
		// ...not currently required, so perform the 'on versus' mission checking instead
		
		// Don't do this check on this frame if the initial cloud load hasn't completed
		IF NOT (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
			EXIT
		ENDIF

		BOOL	wasPlayingVersus	= g_sHeistHelpText.hhtHeistVersusPauseHelpOnVersus
		BOOL	isPlayingVersus		= Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())
		
		// Is there a change in state?
		IF (wasPlayingVersus = isPlayingVersus)
			EXIT
		ENDIF
		
		// There is a change of state
		IF (isPlayingVersus)
			// ...now 'playing' a versus mission
			#IF IS_DEBUG_BUILD
				NET_PRINT(".KGM [Heist]:")
				NET_PRINT_TIME()
				NET_PRINT("Maintain_Versus_Pause_Menu_Triggering_Help_Text() - Player has started playing a Versus Mission")
				NET_NL()
			#ENDIF
			
			g_sHeistHelpText.hhtHeistVersusPauseHelpOnVersus = TRUE
			
			EXIT
		ENDIF
		
		// No longer playing a Versus Mission, so setup the help text requirements
		#IF IS_DEBUG_BUILD
			NET_PRINT(".KGM [Heist]:")
			NET_PRINT_TIME()
			NET_PRINT("Maintain_Versus_Pause_Menu_Triggering_Help_Text() - Player has finished playing a Versus Mission")
			NET_NL()
		#ENDIF
		
		g_sHeistHelpText.hhtHeistVersusPauseHelpOnVersus	= FALSE
		g_sHeistHelpText.hhtHeistVersusPauseHelpRequired	= TRUE
		g_sHeistHelpText.hhtHeistVersusPauseHelpTimeout		= GET_GAME_TIMER() + VERSUS_PAUSE_MENU_DELAY_msec
		
		#IF IS_DEBUG_BUILD
			NET_PRINT(".KGM [Heist]:")
			NET_PRINT_TIME()
			NET_PRINT("Maintain_Versus_Pause_Menu_Triggering_Help_Text() - Set up Help text timeout: ")
			NET_PRINT_INT(VERSUS_PAUSE_MENU_DELAY_msec)
			NET_PRINT(" msec")
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// The Versus Triggering from Pause Menu help text is allowed and required, so check for timeout
	IF (GET_GAME_TIMER() < g_sHeistHelpText.hhtHeistVersusPauseHelpTimeout)
		EXIT
	ENDIF
	
	// Timeout has occurred so display the help text if possible
	PRINTLN(".KGM [Heist]: Maintain_Versus_Pause_Menu_Triggering_Help_Text() - timer has elapsed. Display?")
	
	IF (IS_HELP_MESSAGE_BEING_DISPLAYED())
	OR (Is_There_A_General_Reason_To_Delay_Heist_Related_Activities())
		// ...setup to try again soon
		IF (IS_HELP_MESSAGE_BEING_DISPLAYED())
			PRINTLN(".KGM [Heist]: Maintain_Versus_Pause_Menu_Triggering_Help_Text(): Other help text is on display.")
		ENDIF
		
		g_sHeistHelpText.hhtHeistVersusPauseHelpTimeout = GET_GAME_TIMER() + VERSUS_PAUSE_MENU_DELAY_msec
		
		PRINTLN(".KGM [Heist]: Maintain_Versus_Pause_Menu_Triggering_Help_Text(). Delay Help Text. Try again in ", VERSUS_PAUSE_MENU_DELAY_msec, "msec")
		
		EXIT
	ENDIF
	
	// "The new Versus modes are also available in the Online section of the Pause Menu. Go to the Versus category of the Jobs menu to play these new modes."
	PRINTLN(".KGM [Heist]: Maintain_Versus_Pause_Menu_Triggering_Help_Text(): YES - clear all control variables")
	//PRINT_HELP("VS_PAUSE_MENU")
	
	// Clear all control variables (NOT forcing a STAT save, so there is a chance this help will get displayed again. Not a big deal).
	CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_ALLOW_DISPLAY_VERSUS_PAUSE_HELP)
	
	g_sHeistHelpText.hhtHeistVersusPauseHelpOnVersus	= FALSE
	g_sHeistHelpText.hhtHeistVersusPauseHelpRequired	= FALSE
	g_sHeistHelpText.hhtHeistVersusPauseHelpTimeout		= 0

ENDPROC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      Heist Completion Phonecall Access Functions
// ===========================================================================================================

// PURPOSE:	Clear the bits associated with the Heist Completion Phonecall in the Reward STAT
PROC Clear_All_Heist_Completion_Phonecall_Bits()

	CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_FLEECA_TUTORIAL)
	CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_PRISON_BREAK)
	CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_HUMANE_LABS)
	CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_NARCOTICS)
	CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_PACIFIC_STANDARD)

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Heist Completion Phonecall control variables
PROC Clear_Heist_Completion_Phonecall_Controls()

	PRINTLN(".KGM [ActSelect][Heist]: Clear_Heist_Completion_Phonecall_Controls()")
	
	g_sPostHeistPhonecall.hcpRequired		= FALSE
	g_sPostHeistPhonecall.hcpStage			= POST_HEIST_PHONECALL_STAGE_NONE
	g_sPostHeistPhonecall.hcpHashRCID		= 0
	g_sPostHeistPhonecall.hcpTimeout		= GET_GAME_TIMER()
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Activate Heist Completion Phonecall Control Variables if a phonecall is required
PROC Activate_Heist_Completion_Phonecall()

	PRINTLN(".KGM [ActSelect][Heist]: Activate_Heist_Completion_Phonecall(): Checking if a phonecall is required")
	
	// Tutorial
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_FLEECA_TUTORIAL))
		g_sPostHeistPhonecall.hcpRequired	= TRUE
		g_sPostHeistPhonecall.hcpStage		= POST_HEIST_PHONECALL_STAGE_DELAY
		g_sPostHeistPhonecall.hcpHashRCID	= Get_Tutorial_Heist_RCID_Hash()
		g_sPostHeistPhonecall.hcpTimeout	= GET_GAME_TIMER() + POST_HEIST_PHONECALL_INITIAL_DELAY_msec
		PRINTLN(".KGM [ActSelect][Heist]: Activate_Heist_Completion_Phonecall(): Fleeca Tutorial. Delay: ", POST_HEIST_PHONECALL_INITIAL_DELAY_msec, " msec")
		EXIT
	ENDIF
	
	// Prison Break
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_PRISON_BREAK))
		g_sPostHeistPhonecall.hcpRequired	= TRUE
		g_sPostHeistPhonecall.hcpStage		= POST_HEIST_PHONECALL_STAGE_DELAY
		g_sPostHeistPhonecall.hcpHashRCID	= Get_Prison_Heist_RCID_Hash()
		g_sPostHeistPhonecall.hcpTimeout	= GET_GAME_TIMER() + POST_HEIST_PHONECALL_INITIAL_DELAY_msec
		PRINTLN(".KGM [ActSelect][Heist]: Activate_Heist_Completion_Phonecall(): Prison Break. Delay: ", POST_HEIST_PHONECALL_INITIAL_DELAY_msec, " msec")
		EXIT
	ENDIF
	
	// Humane Labs
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_HUMANE_LABS))
		g_sPostHeistPhonecall.hcpRequired	= TRUE
		g_sPostHeistPhonecall.hcpStage		= POST_HEIST_PHONECALL_STAGE_DELAY
		g_sPostHeistPhonecall.hcpHashRCID	= Get_Biolab_Heist_RCID_Hash()
		g_sPostHeistPhonecall.hcpTimeout	= GET_GAME_TIMER() + POST_HEIST_PHONECALL_INITIAL_DELAY_msec
		PRINTLN(".KGM [ActSelect][Heist]: Activate_Heist_Completion_Phonecall(): Humane Labs. Delay: ", POST_HEIST_PHONECALL_INITIAL_DELAY_msec, " msec")
		EXIT
	ENDIF
	
	// Narcotics
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_NARCOTICS))
		g_sPostHeistPhonecall.hcpRequired	= TRUE
		g_sPostHeistPhonecall.hcpStage		= POST_HEIST_PHONECALL_STAGE_DELAY
		g_sPostHeistPhonecall.hcpHashRCID	= Get_Chicken_Heist_RCID_Hash()
		g_sPostHeistPhonecall.hcpTimeout	= GET_GAME_TIMER() + POST_HEIST_PHONECALL_INITIAL_DELAY_msec
		PRINTLN(".KGM [ActSelect][Heist]: Activate_Heist_Completion_Phonecall(): Narcotics. Delay: ", POST_HEIST_PHONECALL_INITIAL_DELAY_msec, " msec")
		EXIT
	ENDIF
	
	// Pacific Standard
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_PACIFIC_STANDARD))
		g_sPostHeistPhonecall.hcpRequired	= TRUE
		g_sPostHeistPhonecall.hcpStage		= POST_HEIST_PHONECALL_STAGE_DELAY
		g_sPostHeistPhonecall.hcpHashRCID	= Get_Ornate_Heist_RCID_Hash()
		g_sPostHeistPhonecall.hcpTimeout	= GET_GAME_TIMER() + POST_HEIST_PHONECALL_INITIAL_DELAY_msec
		PRINTLN(".KGM [ActSelect][Heist]: Activate_Heist_Completion_Phonecall(): Pacific Standard. Delay: ", POST_HEIST_PHONECALL_INITIAL_DELAY_msec, " msec")
		EXIT
	ENDIF
	
	PRINTLN(".KGM [ActSelect][Heist]: Activate_Heist_Completion_Phonecall(): ...not required - clearing control variables")
	Clear_Heist_Completion_Phonecall_Controls()

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the Heist Completion Phonecall requirements on Strand Completion
//
// INPUTS:			paramHashRCID		The Heist Finale Hash rootContentID
PROC Store_Heist_Completion_Phonecall(INT paramHashRCID)

	Clear_All_Heist_Completion_Phonecall_Bits()
	
	// Only play the first time
	IF (GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(paramHashRCID, TRUE) > 1)
		PRINTLN(".KGM [ActSelect][Heist]: Store_Heist_Completion_Phonecall(): NOT REQUIRED - previously completed as Leader")
		EXIT
	ENDIF

	IF (Is_This_Tutorial_Heist_RCID_Hash(paramHashRCID))
		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_FLEECA_TUTORIAL)
		PRINTLN(".KGM [ActSelect][Heist]: Store_Heist_Completion_Phonecall(): Fleeca Tutorial")
		EXIT
	ENDIF

	IF (Is_This_Prison_Heist_RCID_Hash(paramHashRCID))
		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_PRISON_BREAK)
		PRINTLN(".KGM [ActSelect][Heist]: Store_Heist_Completion_Phonecall(): Prison Break")
		EXIT
	ENDIF

	IF (Is_This_Biolab_Heist_RCID_Hash(paramHashRCID))
		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_HUMANE_LABS)
		PRINTLN(".KGM [ActSelect][Heist]: Store_Heist_Completion_Phonecall(): Humane Labs")
		EXIT
	ENDIF

	IF (Is_This_Chicken_Heist_RCID_Hash(paramHashRCID))
		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_NARCOTICS)
		PRINTLN(".KGM [ActSelect][Heist]: Store_Heist_Completion_Phonecall(): Narcotics")
		EXIT
	ENDIF

	IF (Is_This_Ornate_Heist_RCID_Hash(paramHashRCID))
		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_PACIFIC_STANDARD)
		PRINTLN(".KGM [ActSelect][Heist]: Store_Heist_Completion_Phonecall(): Pacific Standard")
		EXIT
	ENDIF
	
	PRINTLN(".KGM [ActSelect][Heist]: Store_Heist_Completion_Phonecall() - ERROR: Unknown Finale Hash RCID: ", paramHashRCID)
	SCRIPT_ASSERT("Store_Heist_Completion_Phonecall() - ERROR: Unknown Finale Hash RCID. Tell Keith.")

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Heist Completion Phonecall Delay
PROC Maintain_Heist_Completion_Phonecall_Delay()

	// Delay expired?
	IF (GET_GAME_TIMER() < g_sPostHeistPhonecall.hcpTimeout)
		EXIT
	ENDIF
	
	PRINTLN(".KGM [ActSelect][Heist]: Maintain_Heist_Completion_Phonecall_Delay(): Expired. Allow Post Heist Phonecall.")
	
	// Reasons to delay the Heist Strand
	IF (Is_There_A_General_Reason_To_Delay_Heist_Related_Activities())
		PRINTLN(".KGM [ActSelect][Heist]: Maintain_Heist_Completion_Phonecall_Delay(): Phonecall is being delayed: ", POST_HEIST_PHONECALL_QUICK_DELAY_msec, " msec")
		g_sPostHeistPhonecall.hcpTimeout = GET_GAME_TIMER() + POST_HEIST_PHONECALL_QUICK_DELAY_msec
		EXIT
	ENDIF
	
	// Move on
	PRINTLN(".KGM [ActSelect][Heist]: Maintain_Heist_Completion_Phonecall_Delay(): Activating Wait for Comms to be allowed")
	g_sPostHeistPhonecall.hcpStage = POST_HEIST_PHONECALL_STAGE_WAIT_FOR_COMMS
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Heist Completion Phonecall Comms Request
PROC Maintain_Heist_Completion_Phonecall_Request_Comms()
	
	PRINTLN(".KGM [ActSelect][Heist]: Maintain_Heist_Completion_Phonecall_Request_Comms(): Requesting Comms.")
	
	// Reasons to delay the Heist Strand
	IF (Is_There_A_General_Reason_To_Delay_Heist_Related_Activities())
		PRINTLN(".KGM [ActSelect][Heist]: Maintain_Heist_Completion_Phonecall_Request_Comms(): Phonecall is being delayed: ", POST_HEIST_PHONECALL_QUICK_DELAY_msec, " msec")
		g_sPostHeistPhonecall.hcpTimeout	= GET_GAME_TIMER() + POST_HEIST_PHONECALL_QUICK_DELAY_msec
		g_sPostHeistPhonecall.hcpStage		= POST_HEIST_PHONECALL_STAGE_DELAY
		EXIT
	ENDIF

	// Request comms to tell the player about the heist
	INT phonecallModifiers = ALL_MP_COMMS_MODIFIERS_CLEAR
	SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_PRIORITY_PHONECALL)
	
	structPedsForConversation pedForConversation
	INT Lester_SpeakerID = 3
	ADD_PED_FOR_DIALOGUE(pedForConversation, Lester_SpeakerID, NULL, "Lester")
	TEXT_LABEL_15 phonecallRootID = Get_Phonecall_RootID_For_Heist_Completion_Phonecall(g_sPostHeistPhonecall.hcpHashRCID)
	
	IF (IS_STRING_NULL_OR_EMPTY(phonecallRootID))
		PRINTLN("...KGM MP [ActSelect][Heist]: Maintain_Heist_Completion_Phonecall_Request_Comms(): Failed to find PhonecallRootID for this Finale RootContentID Hash: ", g_sPostHeistPhonecall.hcpHashRCID)
		SCRIPT_ASSERT("Maintain_Heist_Completion_Phonecall_Request_Comms() - ERROR: Phonecall rootContentID was NULL. Tell Keith")
		// Move on
		PRINTLN(".KGM [ActSelect][Heist]: Maintain_Heist_Completion_Phonecall_Request_Comms(): Move on to Cleanup")
		g_sPostHeistPhonecall.hcpStage = POST_HEIST_PHONECALL_STAGE_CLEANUP
		EXIT
	ENDIF
	
	SET_USE_DLC_DIALOGUE(TRUE)
	IF (Request_MP_Comms_Message(pedForConversation, CHAR_LESTER, "LCAU", phonecallRootID, phonecallModifiers))
		PRINTLN(".KGM [ActSelect][Heist]: Maintain_Heist_Completion_Phonecall_Request_Comms(): Heist Completion Phonecall in progress")
		PRINTLN(".KGM [ActSelect][Heist]: Maintain_Heist_Completion_Phonecall_Request_Comms(): Move on to Wait for Comms End")
		g_sPostHeistPhonecall.hcpStage = POST_HEIST_PHONECALL_STAGE_WAIT_FOR_COMMS_END
	ENDIF
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain Waiting for the Heist Completion Phonecall to end
PROC Maintain_Heist_Completion_Phonecall_Wait_For_Comms_End()

	IF (Is_MP_Comms_Still_Playing())
		EXIT
	ENDIF
	
	SET_USE_DLC_DIALOGUE(FALSE)
	
	IF NOT (Did_MP_Comms_Complete_Successfully())
		PRINTLN(".KGM [ActSelect][Heist]: Maintain_Heist_Completion_Phonecall_Wait_For_Comms_End() - Call did not end successfully. Try again. Delay: ", POST_HEIST_PHONECALL_QUICK_DELAY_msec, " msec")
		g_sPostHeistPhonecall.hcpTimeout	= GET_GAME_TIMER() + POST_HEIST_PHONECALL_QUICK_DELAY_msec
		g_sPostHeistPhonecall.hcpStage		= POST_HEIST_PHONECALL_STAGE_DELAY
		EXIT
	ENDIF
	
	PRINTLN(".KGM [ActSelect][Heist]: Maintain_Heist_Completion_Phonecall_Wait_For_Comms_End() - Call Ended Successfully. Moving to Cleanup.")
	g_sPostHeistPhonecall.hcpStage = POST_HEIST_PHONECALL_STAGE_CLEANUP
	
ENDPROC
	// FEATURE_HEIST_PLANNING	


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain Heist Completion Phonecall Cleanup and activate 'another heist soon' help text
PROC Maintain_Heist_Completion_Phonecall_Cleanup()

	// Activate the 'another heist soon' help
	Activate_Another_Heist_Soon_Help_Text()
	
	PRINTLN("...KGM MP [ActSelect][Heist]: Maintain_Heist_Completion_Phonecall_Cleanup() - Clearing all Heist Completion Phonecall variables.")
	Clear_Heist_Completion_Phonecall_Controls()
	Clear_All_Heist_Completion_Phonecall_Bits()
	
ENDPROC
	// FEATURE_HEIST_PLANNING	


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Heist Completion Phonecall sequence
PROC Maintain_Heist_Completion_Phonecall()

	SWITCH (g_sPostHeistPhonecall.hcpStage)
		CASE POST_HEIST_PHONECALL_STAGE_NONE
			// Nothing to do
			EXIT
			
		CASE POST_HEIST_PHONECALL_STAGE_DELAY
			Maintain_Heist_Completion_Phonecall_Delay()
			EXIT
			
		CASE POST_HEIST_PHONECALL_STAGE_WAIT_FOR_COMMS
			// Sit here and wait for Dave's stuff to allow the phonecall
			PRINTLN(".KGM [ActSelect][Heist]: Maintain_Heist_Completion_Phonecall(): Waiting for Dave's stuff to allow comms")
			EXIT
			
		CASE POST_HEIST_PHONECALL_STAGE_REQUEST_COMMS
			Maintain_Heist_Completion_Phonecall_Request_Comms()
			EXIT
			
		CASE POST_HEIST_PHONECALL_STAGE_WAIT_FOR_COMMS_END
			Maintain_Heist_Completion_Phonecall_Wait_For_Comms_End()
			EXIT
			
		CASE POST_HEIST_PHONECALL_STAGE_CLEANUP
			Maintain_Heist_Completion_Phonecall_Cleanup()
			EXIT
			
		CASE POST_HEIST_PHONECALL_STAGE_ERROR
			EXIT
	ENDSWITCH
	
	PRINTLN("...KGM MP [ActSelect][Heist]: Maintain_Heist_Completion_Phonecall() - ERROR: Unknown Heist Completion Phonecall Stage. Add to SWITCH.")
	SCRIPT_ASSERT("Maintain_Heist_Completion_Phonecall() - ERROR: Unknown Heist Completion Phonecall Stage. Tell Keith")

ENDPROC
	// FEATURE_HEIST_PLANNING





// ===========================================================================================================
//      General Heist Control Access Functions
// ===========================================================================================================

// PURPOSE:	Get the Current Heist Control Stage
//
// RETURN VALUE:		g_eHeistStagesClient		The current Heist Stage
FUNC g_eHeistStagesClient Get_Current_Heist_Control_Stage()
	RETURN (g_sLocalMPHeistControl.lhcStage)
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Current Heist Control Stage
//
// INPUT PARAMS:		paramHeistClientStage		The new Current Heist Stage
//						paramInitialDelay			[DEFAULT = 0]: When setting to HEIST_CLIENT_STAGE_NO_HEIST, also pass the required initial delay
PROC Set_Current_Heist_Control_Stage(g_eHeistStagesClient paramHeistClientStage, INT paramInitialDelay = 0)

	g_sLocalMPHeistControl.lhcStage	= paramHeistClientStage

	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Heist]: Change Current Heist Stage to: ", Convert_Client_Heist_Stage_To_String(paramHeistClientStage))
	#ENDIF
	
	// Check if the Initial Delay is required
	IF (paramHeistClientStage != HEIST_CLIENT_STAGE_NO_HEIST)
		// ...an initial delay is not required for any stage other then the 'no heist' stage
		IF (paramInitialDelay != 0)
			#IF IS_DEBUG_BUILD
				PRINTLN(".KGM [Heist]: An initial delay is not required for stage: ", Convert_Client_Heist_Stage_To_String(paramHeistClientStage), " - IGNORING.")
			#ENDIF
		ENDIF
		
		EXIT
	ENDIF
		
	// Ensure the 'no heist' stage gets an initial delay setup
	IF (paramInitialDelay = 0)
		// Use a default Initial Delay
		paramInitialDelay = HEIST_CONTROL_INTRO_TO_TUTORIAL_DELAY_msec
		
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [Heist]: An initial delay is REQUIRED for stage: ", Convert_Client_Heist_Stage_To_String(paramHeistClientStage), ". Auto-setting to: ", paramInitialDelay)
		#ENDIF
	ENDIF
	
	g_sLocalMPHeistControl.lhcRequiredInitialDelay	= paramInitialDelay
	
	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Heist]: Change Required Initial Delay: ", g_sLocalMPHeistControl.lhcRequiredInitialDelay)
	#ENDIF

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Current Heist Control Finale Heist RCID Hash
//
// RETURN VALUE:		INT						The current Heist Hash RCID
FUNC INT Get_Current_Heist_Control_Hash_HeistRCID()
	RETURN (g_sLocalMPHeistControl.lhcHashHeistRCID)
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Current Heist Control Finale Heist RCID Hash
//
// INPUT PARAMS:		paramHashHeistRCID		The new Heist Hash RCID
PROC Set_Current_Heist_Control_Hash_HeistRCID(INT paramHashHeistRCID)
	g_sLocalMPHeistControl.lhcHashHeistRCID = paramHashHeistRCID
	PRINTLN("...KGM MP [ActSelect][Heist]: Set Hash Heist RCID to: ", paramHashHeistRCID)
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Setup the Heist Control Initial Delay timeout
PROC Setup_MP_Heist_Control_Initial_Delay_Timeout()

	INT theDelay = g_sLocalMPHeistControl.lhcRequiredInitialDelay
	
	#IF IS_DEBUG_BUILD
		IF (GET_COMMANDLINE_PARAM_EXISTS("sc_debugQuickHeists"))
			theDelay = 60000
		ENDIF
	#ENDIF

	g_sLocalMPHeistControl.lhcTimeout = GET_GAME_TIMER() + theDelay
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][Heist]:")
		NET_PRINT_TIME()
		NET_PRINT(" Setting Heist Control Initial Delay Time to: ")
		NET_PRINT_INT(g_sLocalMPHeistControl.lhcTimeout)
		NET_PRINT(" (GET_GAME_TIMER() + ")
		NET_PRINT_INT(theDelay)
		NET_PRINT("msec)")
		IF (GET_COMMANDLINE_PARAM_EXISTS("sc_debugQuickHeists"))
			NET_PRINT(" - DEBUG QUICK DELAY (-sc_debugQuickHeists)")
		ENDIF
		NET_NL()
	#ENDIF
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Setup the Heist Control 'Repeat Phonecall' Delay timeout
PROC Setup_MP_Heist_Control_Repeat_Phonecall_Delay_Timeout()

	INT theDelay = HEIST_CONTROL_REPEAT_PHONECALL_DELAY_msec
	
	#IF IS_DEBUG_BUILD
		IF (GET_COMMANDLINE_PARAM_EXISTS("sc_debugQuickHeists"))
			IF (theDelay > 60000)
				theDelay = 60000
			ENDIF
		ENDIF
	#ENDIF

	g_sLocalMPHeistControl.lhcTimeout = GET_GAME_TIMER() + theDelay
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][Heist]:")
		NET_PRINT_TIME()
		NET_PRINT(" Setting Heist Control Repeat Phonecall Delay Time to: ")
		NET_PRINT_INT(g_sLocalMPHeistControl.lhcTimeout)
		NET_PRINT(" (GET_GAME_TIMER() + ")
		NET_PRINT_INT(theDelay)
		NET_PRINT("msec)")
		NET_NL()
	#ENDIF
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Setup the Heist Control 'Repeat Phonecall' Longer Delay timeout
PROC Setup_MP_Heist_Control_Repeat_Phonecall_Longer_Delay_Timeout()

	INT theDelay = HEIST_CONTROL_REPEAT_PHONECALL_LONGER_DELAY_msec

	g_sLocalMPHeistControl.lhcTimeout = GET_GAME_TIMER() + theDelay
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][Heist]:")
		NET_PRINT_TIME()
		NET_PRINT(" Setting Heist Control Repeat Phonecall Longer Delay Time to: ")
		NET_PRINT_INT(g_sLocalMPHeistControl.lhcTimeout)
		NET_PRINT(" (GET_GAME_TIMER() + ")
		NET_PRINT_INT(theDelay)
		NET_PRINT("msec)")
		NET_NL()
	#ENDIF
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Heist Control Initial Delay timeout has expired
//
// RETURN VALUE:		BOOL			TRUE if timeout has expired, otherwise FALSE
FUNC BOOL Has_MP_Heist_Control_Initial_Delay_Timeout_Expired()
	RETURN (GET_GAME_TIMER() > g_sLocalMPHeistControl.lhcTimeout)
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return which Heist Delay should be used when a Quick Retry is required
//
// RETURN VALUE:		INT			The msec delay to use for a Quick Retry
FUNC INT Get_Heist_Quick_Retry_Initial_Delay()
	INT retryDelayMsec = HEIST_CONTROL_QUICK_RETRY_DELAY_msec
	PRINTLN("...KGM MP [ActSelect][Heist]: Retrieving value for Heist Quick Retry Initial Delay: ", retryDelayMsec)
	RETURN (retryDelayMsec)
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Calculate which Heist Delay should be used between Heists
//
// INPUT PARAMS:		paramLogging		[DEFAULT = TRUE] TRUE to output to console log, FALSE to ignore logging (to avoid spam during debug safety checking)
// RETURN VALUE:		INT					The msec delay to use between Heists
FUNC INT Get_Required_Initial_Delay_Between_Heists(BOOL paramLogging = TRUE)
	
	// Use the current Heist value to determine the delay to use
	INT nextHeistFlowValueFromStat	= GET_MP_INT_CHARACTER_STAT(MP_STAT_CURRENT_HEIST_STRAND)
	INT nextHeistHashFinaleRCID		= Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(nextHeistFlowValueFromStat)
	INT delayToUse					= 0
	
	#IF IS_DEBUG_BUILD
		IF (paramLogging)
			INT numCompletedAsLeader = GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(nextHeistHashFinaleRCID, TRUE)
			
			IF (Is_This_Tutorial_Heist_RCID_Hash(nextHeistHashFinaleRCID))
				PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay: Tutorial Heist - numCompletedAsLeader: ", numCompletedAsLeader)
			ELIF (Is_This_Prison_Heist_RCID_Hash(nextHeistHashFinaleRCID))
				PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay: Prison Heist - numCompletedAsLeader: ", numCompletedAsLeader)
			ELIF (Is_This_Biolab_Heist_RCID_Hash(nextHeistHashFinaleRCID))
				PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay: Humane Heist - numCompletedAsLeader: ", numCompletedAsLeader)
			ELIF (Is_This_Chicken_Heist_RCID_Hash(nextHeistHashFinaleRCID))
				PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay: Narcotics Heist - numCompletedAsLeader: ", numCompletedAsLeader)
			ELIF (Is_This_Ornate_Heist_RCID_Hash(nextHeistHashFinaleRCID))
				PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay: Ornate Heist - numCompletedAsLeader: ", numCompletedAsLeader)
			ELSE
				PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay: Unknown Heist. RCID: ", nextHeistHashFinaleRCID)
			ENDIF
		ENDIF
	#ENDIF
	
	// Next Heist is Tutorial? (NOTE: This should only be true if no heist has been completed or if all heists have been completed)
	IF (Is_This_Tutorial_Heist_RCID_Hash(nextHeistHashFinaleRCID))
		// If the Tutorial Heist hasn't been completed as Leader then the next delay to use will be the delay between the intro cutscene and the Tutorial
		IF (GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(nextHeistHashFinaleRCID, TRUE) <= 0)
			delayToUse = HEIST_CONTROL_INTRO_TO_TUTORIAL_DELAY_msec
			IF (paramLogging)
				PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay: Tutorial not completed as Leader. Using delay: ", delayToUse)
			ENDIF
			
			RETURN (delayToUse)
		ENDIF
	ENDIF
	
	// Next Heist is Prison? (NOTE: Uses a shorter delay since this follows the Tutorial Heist)
	IF (Is_This_Prison_Heist_RCID_Hash(nextHeistHashFinaleRCID))
		// If the Prison Heist hasn't been completed as Leader then use the 'Tutorial Heist to Next Heist' shorter delay
		IF (GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(nextHeistHashFinaleRCID, TRUE) <= 0)
			// ...use Tutorial to Next Heist delay
			delayToUse = g_sMPTunables.iHestTutorialNextHeitDelay
			IF (paramLogging)
				PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay: Prison not completed as Leader. Using delay: ", delayToUse)
			ENDIF
			
			// KGM 13/11/14 [BUG 2132918]: Use half delay on a console reboot
			// KGM 2/2/15 [BUG 2220501]: Use a very short timeout if the 'next heist phonecall soon' stat is set
			IF (g_resetHeistsOnJoiningMP)
				IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_NEXT_HEIST_QUICK_PHONECALL))
					delayToUse = HEIST_NEXT_HEIST_QUICK_PHONECALL_TIMEOUT_msec
					IF (paramLogging)
						PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay (Prison): ...console reboot and 'quick phonecall' set. Using short delay: ", HEIST_NEXT_HEIST_QUICK_PHONECALL_TIMEOUT_msec)
					ENDIF
				ELSE
					delayToUse = (g_sMPTunables.iHestTutorialNextHeitDelay / 2)
					IF (paramLogging)
						PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay (Prison): ...console reboot. Using half delay: ", delayToUse)
					ENDIF
				ENDIF
				
				// Display 'another heist soon' help text (only if the heist completion call has been completed)
				IF (g_sPostHeistPhonecall.hcpStage = POST_HEIST_PHONECALL_STAGE_NONE)
					IF (paramLogging)
						PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay (Prison): ...console reboot. Attempt to display 'another heist soon' help message")
					ENDIF
					Activate_Another_Heist_Soon_Help_Text()
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_NextHeistDelayTutorial")			
					delayToUse = (60000 * GET_COMMANDLINE_PARAM_INT("sc_NextHeistDelayTutorial"))
					IF delayToUse = 0 
						delayToUse = HEIST_CONTROL_TUTORIAL_TO_NEXT_HEIST_DELAY_msec
					ENDIF
					IF (paramLogging)
						PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay: ...Override because -sc_NextHeistDelayTutorial active. Using delay: ", delayToUse)
					ENDIF
				ENDIF
			#ENDIF
			
			RETURN (delayToUse)
		ENDIF
	ENDIF
	
	// Next Heist should just use standard delay? (NOTE: This may include Tutorial or Prison Heists if they have already been played)
	IF (Is_This_Tutorial_Heist_RCID_Hash(nextHeistHashFinaleRCID))
	OR (Is_This_Prison_Heist_RCID_Hash(nextHeistHashFinaleRCID))
	OR (Is_This_Biolab_Heist_RCID_Hash(nextHeistHashFinaleRCID))					
	OR (Is_This_Chicken_Heist_RCID_Hash(nextHeistHashFinaleRCID))
	OR (Is_This_Ornate_Heist_RCID_Hash(nextHeistHashFinaleRCID))
		// If the Heist has already been played then use a really long delay
		IF (GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(nextHeistHashFinaleRCID, TRUE) > 0)
			delayToUse = g_sMPTunables.iHeistAlreadyPlayedDelay
			IF (paramLogging)
				PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay: Already Played. Use Long Heist Delay: ", delayToUse)
			ENDIF
				
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_NextHeistDelay")			
					delayToUse = (60000 * GET_COMMANDLINE_PARAM_INT("sc_NextHeistDelay"))
					IF delayToUse = 0 
						delayToUse = HEIST_CONTROL_BETWEEN_HEISTS_DELAY_msec
					ENDIF
					IF (paramLogging)
						PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay: ...Override because -sc_NextHeistDelay active. Using delay: ", delayToUse)
					ENDIF
				ENDIF
			#ENDIF
			
			RETURN (delayToUse)
		ENDIF

		// The heist hasn't been played, so use the standard delay
		delayToUse = g_sMPTunables.iHestNextHeitDelay
		IF (paramLogging)
			PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay: Using Standard Heist Delay: ", delayToUse)
		ENDIF
			
		// KGM 13/11/14 [BUG 2132918]: Use half delay on a console reboot
		IF (g_resetHeistsOnJoiningMP)
			IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_NEXT_HEIST_QUICK_PHONECALL))
				delayToUse = HEIST_NEXT_HEIST_QUICK_PHONECALL_TIMEOUT_msec
				IF (paramLogging)
					PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay: ...console reboot and 'quick phonecall' set. Using short delay: ", HEIST_NEXT_HEIST_QUICK_PHONECALL_TIMEOUT_msec)
				ENDIF
			ELSE
				delayToUse = (g_sMPTunables.iHestNextHeitDelay / 2)
				IF (paramLogging)
					PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay: ...console reboot. Using half delay: ", delayToUse)
				ENDIF
			ENDIF
				
			// Display 'another heist soon' help text (only if the heist completion call has been completed)
			IF (g_sPostHeistPhonecall.hcpStage = POST_HEIST_PHONECALL_STAGE_NONE)
				IF (paramLogging)
					PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay (Prison): ...console reboot. Attempt to display 'another heist soon' help message")
				ENDIF
				Activate_Another_Heist_Soon_Help_Text()
			ENDIF
		ENDIF
			
		#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_NextHeistDelay")			
				delayToUse = (60000 * GET_COMMANDLINE_PARAM_INT("sc_NextHeistDelay"))
				IF delayToUse = 0 
					delayToUse = HEIST_CONTROL_BETWEEN_HEISTS_DELAY_msec
				ENDIF
				IF (paramLogging)
					PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay: ...Override because -sc_NextHeistDelay active. Using delay: ", delayToUse)
				ENDIF
			ENDIF
		#ENDIF
		
		RETURN (delayToUse)
	ENDIF
	
	// Unknown Heist RCID, so use the normal heist delay as a default
	delayToUse = g_sMPTunables.iHestNextHeitDelay
	IF (paramLogging)
		PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay: ERROR - UNKNOWN HEIST RCID: ", nextHeistHashFinaleRCID, ". Using Standard Heist Delay: ", delayToUse)
	ENDIF
		
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_NextHeistDelay")			
			delayToUse = (60000 * GET_COMMANDLINE_PARAM_INT("sc_NextHeistDelay"))
			IF delayToUse = 0 
				delayToUse = HEIST_CONTROL_BETWEEN_HEISTS_DELAY_msec
			ENDIF
			IF (paramLogging)
				PRINTLN(".KGM [Heist]: Calculate Heist Initial Delay: ...Override because -sc_NextHeistDelay active. Using delay: ", delayToUse)
			ENDIF
		ENDIF
	#ENDIF
	
	SCRIPT_ASSERT("Get_Required_Initial_Delay_Between_Heists(): ERROR - UNKNOWN HEIST RCID. Tell Keith.")
	
	RETURN (delayToUse)
	
ENDFUNC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      Heist Unlocks (Rewards for Completing Heist Strands) Routines
// ===========================================================================================================

// PURPOSE:	Clear and Initialise any variables connected with Heist Unlocks
PROC Initialise_Local_Heist_Unlocks()

	#IF IS_DEBUG_BUILD
		NET_PRINT(".KGM [ActSelect][Heist]: Initialise Local Heist Unlocks") NET_NL()
	#ENDIF
	
	g_structLocalHeistUnlockables emptyUnlockablesStruct
	g_sLocalMPHeistUnlocks = emptyUnlockablesStruct
	
	// Gather the Copy STAT Initial value from the STAT, and update the 'known' value to match
	g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks = GET_MP_INT_CHARACTER_STAT(MP_STAT_BITSET_HEIST_VS_MISSIONS)
	g_sLocalMPHeistUnlocks.lhuKnownHeistUnlocksBitset = g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks
	
	#IF IS_DEBUG_BUILD
		Debug_Output_Heist_Unlocks_Control_Bitset()
	#ENDIF
	
	// Check if a Heist Completion Phonecall needs setup
	Activate_Heist_Completion_Phonecall()
	
	// Check if a Heist Replay Reminder Help Text needs setup
	Activate_Heist_Replay_Reminder_Help()

ENDPROC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      General Heist Strand Array Access Functions
// ===========================================================================================================

// PURPOSE:	Clear the local Heist Strand struct ready for a refresh of the data
PROC Clear_Local_Heist_Strands()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][Heist]: Clear Local Heist Strand Data") NET_NL()
	#ENDIF
	
	g_structLocalHeistStrands		emptyLocalHeistStrands
	INT								tempLoop		= 0
	
	REPEAT MAX_HEIST_STRANDS tempLoop
		g_sLocalMPHeists[tempLoop] = emptyLocalHeistStrands
		Clear_MP_MISSION_ID_DATA_Struct(g_sLocalMPHeists[tempLoop].lhsMissionIdData)
	ENDREPEAT
	
	g_numLocalMPHeists = 0

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Heist Strand's MissionID Data
//
// INPUT PARAMS:		paramArrayPos				The Heist Strand Array Position
// RETURN VALUE:		MP_MISSION_ID_DATA			The MissionID Data
FUNC MP_MISSION_ID_DATA Get_MissionID_Data_For_Heist_Strand(INT paramArrayPos)
	RETURN (g_sLocalMPHeists[paramArrayPos].lhsMissionIdData)
ENDFUNC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      Other Players Active Heist Coronas Routines
//		(When another player enters their corona it gets registered locally so the local player can
//			display it when in the other players apartment)
// ===========================================================================================================

// PURPOSE:	Clear the data that maintains the active Heist coronas for other players in the session
PROC Clear_One_Other_Active_Heist_Corona(INT paramArrayPos)

	g_structOtherHeistCoronas	emptyOtherHeistCoronas
	
	g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos] = emptyOtherHeistCoronas
	
	Clear_MP_MISSION_ID_DATA_Struct(g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcMissionIdData)
	g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcCoords		= << 0.0, 0.0, 0.0 >>
	g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcPlayerID	= INVALID_PLAYER_INDEX()

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the data that maintains the active Heist coronas for other players in the session
PROC Clear_Other_Active_Heist_Coronas()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][Heist]: Clear Other Players Active Heist Coronas Data") NET_NL()
	#ENDIF
	
	g_structOtherHeistCoronasControl	emptyOtherHeistCoronasControl
	g_sOtherMPHeistCoronas = emptyOtherHeistCoronasControl
	
	INT tempLoop = 0
	
	REPEAT NUM_OTHER_HEIST_CORONAS tempLoop
		Clear_One_Other_Active_Heist_Corona(tempLoop)
	ENDREPEAT

ENDPROC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      Local Heist Strand Selection and Replay Routines
// ===========================================================================================================

// PURPOSE:	Allow a specific Heist Strand to be setup using the local array position
//
// INPUT PARAMS:			paramArrayPos			Array Position on the local Heist Strand
//
// NOTES:	Setup for Dave's Replay routines
PROC Setup_Specific_Heist_Strand(INT paramArrayPos)

	// Ensure there are Heists to choose from
	IF (g_numLocalMPHeists <= paramArrayPos)
		PRINTLN("...KGM MP [ActSelect][Heist]: Setup_Specific_Heist_Strand() - ERROR: The passed in array position (", paramArrayPos, ") is >= max Heists Strands setup (", g_numLocalMPHeists, ")")
		EXIT
	ENDIF

	// Only allowed if a heist is not being performed
	IF (Get_Current_Heist_Control_Stage() != HEIST_CLIENT_STAGE_NO_HEIST)
	AND (Get_Current_Heist_Control_Stage() != HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY)
		PRINTLN("...KGM MP [ActSelect][Heist]: Setup_Specific_Heist_Strand() - IGNORING: Another Heist Strand is already in progress")
		EXIT
	ENDIF
	
	// Set the current heist to the rootContentID in the array position passed as parameter
	g_replayHeistFlowOrder = Get_Heist_Strand_Flow_Value_From_Finale_Hash_RCID(g_sLocalMPHeists[paramArrayPos].lhsHashFinaleRCID)
	g_replayHeistIsFromBoard = FALSE
	PRINTLN("...KGM MP [ActSelect][Heist]: Setup_Specific_Heist_Strand() - Setting Replay Flow Order (selected from Phone): ", g_replayHeistFlowOrder)
	
	PRINTLN("...KGM MP [ActSelect][Heist]: Setup_Specific_Heist_Strand() - Setting Current Heist RCID to: ", g_sLocalMPHeists[paramArrayPos].lhsHashFinaleRCID)
	Set_Current_Heist_Control_Hash_HeistRCID(g_sLocalMPHeists[paramArrayPos].lhsHashFinaleRCID)
	
	PRINTLN("...KGM MP [ActSelect][Heist]: Setup_Specific_Heist_Strand() - Advance Stage to download mission")
	Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_DOWNLOAD_DETAILS_FROM_FINALE)
	
	// Clear the replay reminder help text since the player has just chosen a replay
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_REPLAY_HEIST_HELP_TEXT_REQUIRED))
		PRINTLN(".KGM [ActSelect][Heist]: Setup_Specific_Heist_Strand(): Clearing Replay Reminder - a replay has just been given")
		CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_REPLAY_HEIST_HELP_TEXT_REQUIRED)
		Clear_Heist_Replay_Reminder_Help_Text_Controls()
	ENDIF
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Allow a specific Heist Strand to be setup using the Finale RCID
//
// INPUT PARAMS:			paramFinaleRCID			FinaleRCID for the replay heist
//
// NOTES:	KGM 9/2/15 [BUG 2226481]: Setup for triggering a replay from the planning board.
PROC Setup_Specific_Heist_Strand_Using_FinaleRCID(INT paramFinaleRCID)

	// Only allowed if a heist is not being performed
	IF (Get_Current_Heist_Control_Stage() != HEIST_CLIENT_STAGE_NO_HEIST)
	AND (Get_Current_Heist_Control_Stage() != HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY)
		PRINTLN(".KGM [Heist]: Setup_Specific_Heist_Strand_Using_FinaleRCID() - IGNORING: Another Heist Strand is already in progress")
		EXIT
	ENDIF
	
	// Set the current heist to the rootContentID in the array position passed as parameter
	g_replayHeistFlowOrder = Get_Heist_Strand_Flow_Value_From_Finale_Hash_RCID(paramFinaleRCID)
	g_replayHeistIsFromBoard = TRUE
	PRINTLN("...KGM MP [ActSelect][Heist]: Setup_Specific_Heist_Strand_Using_FinaleRCID() - Setting Replay Flow Order (selected from Board): ", g_replayHeistFlowOrder)
	
	PRINTLN("...KGM MP [ActSelect][Heist]: Setup_Specific_Heist_Strand_Using_FinaleRCID() - Setting Current Heist RCID to: ", paramFinaleRCID)
	Set_Current_Heist_Control_Hash_HeistRCID(paramFinaleRCID)
	
	PRINTLN("...KGM MP [ActSelect][Heist]: Setup_Specific_Heist_Strand_Using_FinaleRCID() - Advance Stage to download mission")
	Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_DOWNLOAD_DETAILS_FROM_FINALE)
	
	// Clear the replay reminder help text since the player has just chosen a replay
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_REPLAY_HEIST_HELP_TEXT_REQUIRED))
		PRINTLN(".KGM [ActSelect][Heist]: Setup_Specific_Heist_Strand_Using_FinaleRCID(): Clearing Replay Reminder - a replay has just been given")
		CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_REPLAY_HEIST_HELP_TEXT_REQUIRED)
		Clear_Heist_Replay_Reminder_Help_Text_Controls()
	ENDIF
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Setup a Heist Finale or Heist Planning corona based on the passed in contentID
//
// INPUT PARAMS:			paramHeistContentID			A contentID for the required Finale or Planning mission
//							paramFinaleHeist			[DEFAULT = FALSE] TRUE for a Finale mission, FALSE for a Planning mission
//							paramIntroCutscene			[DEFAULT = FALSE] TRUE if this should trigger the pre-planning cutscene only, FALSE to trigger either the Finale or a mid-strand cutscene
//							paramMidStrandCutscene		[DEFAULT = FALSE] TRUE if this should trigger the mid-strand cutscene only, FALSE to trigger either the Finale or an intro cutscene
//							paramTutorialHeist			[DEFAULT = FALSE] TRUE if this is for the Tutorial Cutscene which immediately launches the first Prep Mission, otherwise FALSE
// RETURN VALUE:			BOOL						TRUE if the corona was setup, FALSE if not
//
// NOTE:	This was originally setup as a temp function for Alastair, but it's probably the real way to trigger this now.
FUNC BOOL Setup_My_Heist_Corona_From_ContentID(	TEXT_LABEL_23 paramHeistContentID, 
												BOOL paramFinaleHeist = FALSE, 
												BOOL paramIntroCutscene = FALSE, 
												BOOL paramMidStrandCutscene = FALSE, 
												BOOL paramTutorialHeist = FALSE)

	#IF IS_DEBUG_BUILD
		PRINTLN("...KGM MP [ActSelect][Heist]: Setup_My_Heist_Corona_From_ContentID(): ", paramHeistContentID)
		IF (paramTutorialHeist)
			PRINTLN("...KGM MP [ActSelect][Heist]: ...Tutorial Heist so ContentID is a Prep Mission RootContentID - updated to latest contentID later")
		ENDIF
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramHeistContentID))
		PRINTLN("...KGM MP [ActSelect][Heist]: ContentID is NULL or EMPTY when Setting Up Heist Corona")
		SCRIPT_ASSERT("Setup_My_Heist_Corona_From_ContentID() - ERROR: ContentID is NULL or EMPTY. Tell Keith.")
		
		RETURN FALSE
	ENDIF
	
	// This should only get called by the Heist Leader
	IF NOT (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE))
		PRINTLN("...KGM MP [ActSelect][Heist]: Trying to setup My Heist Corona when not in my own apartment. Only the Leader should be setting up a Heist corona.")
		SCRIPT_ASSERT("Setup_My_Heist_Corona_From_ContentID() - ERROR: Setting up Heist Corona when not in my own apartment. Tell Keith.")
		
		RETURN FALSE
	ENDIF
	
	// Do various consistency checks based on whether the Tutorial flag is set or not
	IF (paramTutorialHeist)
		// This is a tutorial heist
		// ...the tutorial heist cutscene should be triggered by a prep mission corona
		IF (paramFinaleHeist)
			PRINTLN("...KGM MP [ActSelect][Heist]: ERROR: Finale Heist = TRUE - Tutorial Cutscene runs on a Prep Mission Corona")
			SCRIPT_ASSERT("Setup_My_Heist_Corona_From_ContentID() - ERROR: Finale Heist = TRUE - Tutorial Cutscene runs on a Prep Mission Corona. Tell Keith.")
			
			RETURN FALSE
		ENDIF
		
		IF (paramMidStrandCutscene)
			PRINTLN("...KGM MP [ActSelect][Heist]: ERROR: Mid-Strand Cutscene = TRUE - Tutorial Cutscene shouldn't be a mid-strand cutscene")
			SCRIPT_ASSERT("Setup_My_Heist_Corona_From_ContentID() - ERROR: Mid-Strand Cutscene = TRUE - Tutorial Cutscene shouldn't be a mid-strand cutscene. Tell Keith.")
			
			RETURN FALSE
		ENDIF
		
		IF NOT (paramIntroCutscene)
			PRINTLN("...KGM MP [ActSelect][Heist]: ERROR: Intro Cutscene = FALSE - Tutorial Cutscene needs to be an Intro cutscene")
			SCRIPT_ASSERT("Setup_My_Heist_Corona_From_ContentID() - ERROR: Intro Cutscene = FALSE - Tutorial Cutscene needs to be an Intro cutscene. Tell Keith.")
			
			RETURN FALSE
		ENDIF
	ELSE
		// This isn't a tutorial Heist
		// ...the intro cutscene should only be triggered through the Finale corona
		IF (paramIntroCutscene)
		AND NOT (paramFinaleHeist)
			PRINTLN("...KGM MP [ActSelect][Heist]: ERROR: Intro Cutscene = TRUE but Finale Heist = FALSE - Intro Cutscene runs on the Finale Corona")
			SCRIPT_ASSERT("Setup_My_Heist_Corona_From_ContentID() - ERROR: Intro Cutscene = TRUE but Finale Heist = FALSE - Intro Cutscene runs on the Finale Corona. Tell Keith.")
			
			RETURN FALSE
		ENDIF
		
		// ...the mid-strand cutscene should only be triggered on the Fianle corona
		IF (paramMidStrandCutscene)
		AND NOT (paramFinaleHeist)
			PRINTLN("...KGM MP [ActSelect][Heist]: ERROR: Mid-strand Cutscene = TRUE but Finale Heist = FALSE - Mid-strand Cutscene runs on the Finale Corona")
			SCRIPT_ASSERT("Setup_My_Heist_Corona_From_ContentID() - ERROR: Mid-strand Cutscene = TRUE but Finale Heist = FALSE - Mid-strand Cutscene runs on the Finale Corona. Tell Keith.")
			
			RETURN FALSE
		ENDIF
		
		// ...both the intro cutscene and the mid-strand cutscene flags should not be set at the same time
		IF (paramIntroCutscene)
		AND (paramMidStrandCutscene)
			PRINTLN("...KGM MP [ActSelect][Heist]: ERROR: Intro Cutscene = TRUE and Mid-strand Cutscene = TRUE - Both flags can't be TRUE")
			SCRIPT_ASSERT("Setup_My_Heist_Corona_From_ContentID() - ERROR: Intro Cutscene = TRUE and Mid-strand Cutscene = TRUE - Both flags can't be TRUE. Tell Keith.")
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Does the player own an apartment that allows Heist coronas to be displayed?
	IF NOT (Does_Player_Own_An_Apartment_Suitable_For_Heists())
		PRINTLN("...KGM MP [ActSelect][Heist]: ERROR: Player doesn't own an apartment suitable for setting up a Heist corona")
		SCRIPT_ASSERT("Setup_My_Heist_Corona_From_ContentID() - ERROR: Player doesn't own a Heist apartment. Tell Keith.")
		
		RETURN FALSE
	ENDIF
	
	// Store the details so that a maintenance function can setup the corona in the appropriate apartment at the appropriate time
	IF (g_sLocalMPHeistControl.lhcMyCorona.mhcAvailable)
		PRINTLN("...KGM MP [ActSelect][Heist]: ERROR: Player already has a Heist corona available for contentID: ", g_sLocalMPHeistControl.lhcMyCorona.mhcContentID)
		SCRIPT_ASSERT("Setup_My_Heist_Corona_From_ContentID() - ERROR: Player already has a heist corona available. Tell Keith.")
		
		RETURN FALSE
	ENDIF
	
	// Store the details so the maintenance function can control the display of these coronas
	g_sLocalMPHeistControl.lhcMyCorona.mhcAvailable				= TRUE
	g_sLocalMPHeistControl.lhcMyCorona.mhcContentID				= paramHeistContentID
	g_sLocalMPHeistControl.lhcMyCorona.mhcIsFinale				= paramFinaleHeist
	g_sLocalMPHeistControl.lhcMyCorona.mhcIsIntroCutscene		= paramIntroCutscene
	g_sLocalMPHeistControl.lhcMyCorona.mhcIsMidStrandCutscene	= paramMidStrandCutscene
	g_sLocalMPHeistControl.lhcMyCorona.mhcMatcID				= ILLEGAL_AT_COORDS_ID
	g_sLocalMPHeistControl.lhcMyCorona.mhcInCorona				= FALSE
	g_sLocalMPHeistControl.lhcMyCorona.mhcAlreadyTransitioned	= FALSE
	g_sLocalMPHeistControl.lhcMyCorona.mhcIsTutorialHeist		= paramTutorialHeist
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM MP [ActSelect][Heist]: Storing Available Heist Corona details. ContentID: ")
		PRINTSTRING(paramHeistContentID)
		IF (paramFinaleHeist)
			PRINTSTRING(" [FINALE]")
		ELSE
			PRINTSTRING(" [PLANNING]")
		ENDIF
		IF (paramIntroCutscene)
			PRINTSTRING(" [INTRO CUTSCENE]")
		ENDIF
		IF (paramMidStrandCutscene)
			PRINTSTRING(" [MID-STRAND CUTSCENE]")
		ENDIF
		IF (paramTutorialHeist)
			PRINTSTRING(" [TUTORIAL HEIST]")
		ENDIF
		PRINTNL()
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      Local Heist Strand Data Storage Routines
// ===========================================================================================================

// PURPOSE:	Add Details of a Heist Strand (using Heist Finale details) to the local Heist Strand array
//
// INPUT PARAMS:			paramContact			The activity's Contact (or NO_CHARACTER)
//							paramRank				The activity's rank
// REFERENCE PARAMS:		refMissionIdData		Mission Identification Data
//
// NOTES:	Assumes the array was cleared prior to registration and that there are no duplicates, so tag this entry on at the end
PROC Add_Heist_Strand_To_Local_Activity_Selector(MP_MISSION_ID_DATA &refMissionIdData, enumCharacterList paramContact, INT paramRank)

	// If there are no contact mission details stored yet then clear out the number of contact missions played per contact struct
	#IF IS_DEBUG_BUILD
		IF (g_numLocalMPHeists = 0)
			NET_PRINT("...KGM MP [ActSelect][Heist]:")
			NET_PRINT_TIME()
			NET_PRINT("Initialising Heist Strands")
			NET_NL()
		ENDIF
	#ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(refMissionIdData.idCloudFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][Heist]:")
			NET_PRINT_TIME()
			NET_PRINT("Request to store Heist Strand with NULL or EMPTY Heist Finale contentID. IGNORING.")
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Check Heist Data consistency
	#IF IS_DEBUG_BUILD
		IF NOT (Debug_Check_Heist_Mission_Data(refMissionIdData, paramContact, paramRank))
			NET_PRINT("...KGM MP [ActSelect][Heist]:")
			NET_PRINT_TIME()
			NET_PRINT("Request to store Unrecognised Heist Strand. IGNORING. (ContentID: ")
			NET_PRINT(refMissionIdData.idCloudFilename)
			NET_PRINT(")")
			NET_NL()
			
			EXIT
		ENDIF
	#ENDIF
	
	g_eFMHeistContactIDs	heistContactID		= Convert_CharacterListID_To_FM_Heist_ContactID(paramContact)
	INT						hashFinaleRCID		= Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(refMissionIdData)

	g_sLocalMPHeists[g_numLocalMPHeists].lhsContact				= heistContactID
	g_sLocalMPHeists[g_numLocalMPHeists].lhsRank				= paramRank
	g_sLocalMPHeists[g_numLocalMPHeists].lhsMissionIdData		= refMissionIdData
	g_sLocalMPHeists[g_numLocalMPHeists].lhsHashFinaleRCID		= hashFinaleRCID
	g_sLocalMPHeists[g_numLocalMPHeists].lhsCompletedAsLeader	= FALSE
	
	IF (GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(hashFinaleRCID, TRUE) > 0)
		g_sLocalMPHeists[g_numLocalMPHeists].lhsCompletedAsLeader = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		// I can utilise the Pending Activities debug function for output since the same details are being output
		NET_PRINT("...KGM MP [ActSelect][Heist]: Adding: ")
		Debug_Output_Pending_Activities_Details_Passed_As_Parameters_On_One_Line(refMissionIdData, MP_HEIST_GENERIC_SAFEHOUSE_COORDS, paramContact, paramRank)
		NET_PRINT(" [RCID: ")
		NET_PRINT_INT(hashFinaleRCID)
		NET_PRINT("]")
		IF (g_sLocalMPHeists[g_numLocalMPHeists].lhsCompletedAsLeader)
			NET_PRINT(" - COMPLETED AS LEADER")
		ELSE
			NET_PRINT(" - not completed as leader")
		ENDIF
		NET_NL()
	#ENDIF
	
	g_numLocalMPHeists++

ENDPROC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      External Heist Control Routines
// ===========================================================================================================

// PURPOSE:	If the player is waiting to be allowed to make a Heist call, do it now
// NOTES:	This allows the function called by Dave (Allow_FM_Heist_Triggering_Communication()) to indicate when it is safe to make the call
PROC Allow_MP_Heist_Comms_If_Available()

	IF (Get_Current_Heist_Control_Stage() != HEIST_CLIENT_STAGE_WAIT_UNTIL_COMMS_ALLOWED)
		EXIT
	ENDIF
	
	IF NOT (Does_Player_Own_An_Apartment_Suitable_For_Heists())
		PRINTLN("...KGM MP [ActSelect][Heist]: Player no longer owns a suitable Heist apartment when Heist comms are being allowed.")
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Heist_Quick_Retry_Initial_Delay())
		EXIT
	ENDIF
	
	PRINTLN("...KGM MP [ActSelect][Heist]: MP Comms is available, so now allowing it to be played. See callstack for calling function.")
	DEBUG_PRINTCALLSTACK()
	
	// NOTE: Move on to the 'Request Comms' stage
	Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_REQUEST_COMMS)
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a heist completion phonecall is allowed to be made
PROC Allow_MP_Heist_Completion_Phonecall_If_Available()

	// Ignore if there isn't one trying to play
	IF NOT (g_sPostHeistPhonecall.hcpRequired)
		EXIT
	ENDIF
	
	IF (g_sPostHeistPhonecall.hcpStage != POST_HEIST_PHONECALL_STAGE_WAIT_FOR_COMMS)
		EXIT
	ENDIF

	// Ignore if there is an active heist strand
	SWITCH (Get_Current_Heist_Control_Stage())
		CASE HEIST_CLIENT_STAGE_NO_HEIST
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY
			BREAK
			
		DEFAULT
			// Can't make the phonecall just now, so reset the timer
			g_sPostHeistPhonecall.hcpTimeout = GET_GAME_TIMER() + POST_HEIST_PHONECALL_QUICK_DELAY_msec
			PRINTLN(".KGM [ActSelect][Heist]: Allow_MP_Heist_Completion_Phonecall_If_Available(): There is an active Heist. Delay phonecall: ", POST_HEIST_PHONECALL_QUICK_DELAY_msec, " msec")
			EXIT
	ENDSWITCH
	
	// Make the phonecall
	PRINTLN(".KGM [ActSelect][Heist]: Allow_MP_Heist_Completion_Phonecall_If_Available(): Moving Phonecall Stage to Request Comms")
	g_sPostHeistPhonecall.hcpStage = POST_HEIST_PHONECALL_STAGE_REQUEST_COMMS

ENDPROC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      Heist Strand Completion Reward Routines
// ===========================================================================================================

// PURPOSE:	Check if there is already a Heist Reward active invite group
//
// RETURN VALUE:			BOOL		TRUE if there is an active invite group, FALSE if not
FUNC BOOL Is_There_An_Active_Heist_Reward_Invite_Group()
	RETURN (g_sActiveMPHeistReward.ahrInviteGroupID != NO_HEIST_REWARD_GROUP)
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Rockstar Created array position containing the 1st Heist Reward Active Invite mission
//
// RETURN VALUE:			INT			The rockstar created array position, or ILLEGAL_ARRAY_POSITION if not found
FUNC INT Get_1st_Active_Invite_Rockstar_Created_ArrayPos()

	IF (g_sActiveMPHeistReward.ahrInviteMissionRCID = 0)
		RETURN ILLEGAL_ARRAY_POSITION
	ENDIF

	INT tempLoop = GET_CONTENT_ID_INDEX(g_sActiveMPHeistReward.ahrInviteMissionRCID )
	// Is this the mission?
	IF tempLoop != -1
	AND IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE)
		RETURN tempLoop
	ENDIF
	
	g_sActiveMPHeistReward.ahrInviteMissionRCID = 0
	RETURN ILLEGAL_ARRAY_POSITION
	
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Rockstar Created array position containing the 2nd Heist Reward Active Invite mission
//
// RETURN VALUE:			INT			The rockstar created array position, or ILLEGAL_ARRAY_POSITION if not found
FUNC INT Get_2nd_Active_Invite_Rockstar_Created_ArrayPos()

	IF (g_sActiveMPHeistReward.ahrInviteMissionRCID_2nd = 0)
		RETURN ILLEGAL_ARRAY_POSITION
	ENDIF

	INT tempLoop = GET_CONTENT_ID_INDEX(g_sActiveMPHeistReward.ahrInviteMissionRCID_2nd)
	
	// Is this the mission?
	IF tempLoop != -1
	AND IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE)
		RETURN (tempLoop)
	ENDIF
	
	g_sActiveMPHeistReward.ahrInviteMissionRCID_2nd = 0
	RETURN ILLEGAL_ARRAY_POSITION
	
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Current Heist Reward Stage
//
// RETURN VALUE:		g_eHeistRewardStage			The current Heist Reward Stage
FUNC g_eHeistRewardStage Get_Current_Heist_Reward_Stage()
	RETURN (g_sLocalMPHeistUnlocks.lhuStage)
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Current Heist Reward Stage
//
// INPUT PARAMS:		paramHeistRewardStage		The new Current Heist Reward Stage
//						paramInitialDelay			[DEFAULT = 0]: When setting to HEIST_REWARD_STAGE_NO_REWARD, also pass the required initial delay
PROC Set_Current_Heist_Reward_Stage(g_eHeistRewardStage paramHeistRewardStage, INT paramInitialDelay = 0)

	g_sLocalMPHeistUnlocks.lhuStage	= paramHeistRewardStage

	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Heist]: Change Current Heist Reward Stage to: ", Convert_Client_Heist_Reward_Stage_To_String(paramHeistRewardStage))
	#ENDIF
	
	// Check if the Initial Delay is required
	IF (paramHeistRewardStage != HEIST_REWARD_STAGE_NO_REWARD)
		// ...an initial delay is not required for any stage other then the 'no reward' stage
		IF (paramInitialDelay != 0)
			#IF IS_DEBUG_BUILD
				NET_PRINT(".KGM [Heist]: An initial delay is not required for stage: ")
				NET_PRINT(Convert_Client_Heist_Reward_Stage_To_String(paramHeistRewardStage))
				NET_PRINT(" - IGNORING.")
				NET_NL()
			#ENDIF
		ENDIF
		
		EXIT
	ENDIF
		
	// Ensure the 'no reward' stage gets an initial delay setup
	IF (paramHeistRewardStage = HEIST_REWARD_STAGE_NO_REWARD)
		IF (paramInitialDelay = 0)
			// Use a default Initial Delay
			paramInitialDelay = HEIST_REWARD_NEW_UNLOCK_DELAY_msec
			
			#IF IS_DEBUG_BUILD
				NET_PRINT(".KGM [Heist]: An initial delay is REQUIRED for reward stage: ")
				NET_PRINT(Convert_Client_Heist_Reward_Stage_To_String(paramHeistRewardStage))
				NET_PRINT(". Auto-setting to: ")
				NET_PRINT_INT(paramInitialDelay)
				NET_NL()
			#ENDIF
		ENDIF
		
		// Ensure the timeout control variable is reset
		g_sLocalMPHeistUnlocks.lhuDelayTimeout = 0
		
		// Also, since we're 'forcing' the stage to be the 'no reward' stage then clear the active invite details
		Clear_Heist_Reward_Active_Invite()
	ENDIF
	
	g_sLocalMPHeistUnlocks.lhuInitialDelayToUse	= paramInitialDelay
	
	#IF IS_DEBUG_BUILD
		NET_PRINT(".KGM [ActSelect][Heist]: Change Required Initial Delay: ")
		NET_PRINT_INT(g_sLocalMPHeistUnlocks.lhuInitialDelayToUse)
		NET_NL()
	#ENDIF

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the GroupID of a recently unlocked but unplayed reward groupID
//
// RETURN VALUE:			g_eHeistRewardGroupID		The reward groupID, or NO_HEIST_REWARD_GROUP if none
FUNC g_eHeistRewardGroupID Get_Earliest_Unlocked_But_Unplayed_Reward_GroupID()

	// Manually check the bits for each of the reward types
	IF (g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks = HEIST_UNLOCKS_ALL_CLEAR)
		// ...nothing unlocked
		RETURN NO_HEIST_REWARD_GROUP
	ENDIF
	
	// The first reward group
	g_eHeistRewardGroupID checkRewardGroupID = HEIST_REWARD_VERSUS_AFTER_TUTORIAL
	IF (Is_This_Heist_Reward_Group_Unlocked(checkRewardGroupID))
	AND NOT (Is_This_Heist_Reward_Group_Played(checkRewardGroupID))
		RETURN (checkRewardGroupID)
	ENDIF
	
	// The second reward group
	checkRewardGroupID = HEIST_REWARD_VERSUS_AFTER_PRISON
	IF (Is_This_Heist_Reward_Group_Unlocked(checkRewardGroupID))
	AND NOT (Is_This_Heist_Reward_Group_Played(checkRewardGroupID))
		RETURN (checkRewardGroupID)
	ENDIF
	
	// The third reward group
	checkRewardGroupID = HEIST_REWARD_VERSUS_AFTER_HUMANE_LABS
	IF (Is_This_Heist_Reward_Group_Unlocked(checkRewardGroupID))
	AND NOT (Is_This_Heist_Reward_Group_Played(checkRewardGroupID))
		RETURN (checkRewardGroupID)
	ENDIF
	
	// The fourth reward group
	checkRewardGroupID = HEIST_REWARD_VERSUS_AFTER_NARCOTICS
	IF (Is_This_Heist_Reward_Group_Unlocked(checkRewardGroupID))
	AND NOT (Is_This_Heist_Reward_Group_Played(checkRewardGroupID))
		RETURN (checkRewardGroupID)
	ENDIF
	
	// The fifth reward group
	checkRewardGroupID = HEIST_REWARD_VERSUS_AFTER_ORNATE_BANK
	IF (Is_This_Heist_Reward_Group_Unlocked(checkRewardGroupID))
	AND NOT (Is_This_Heist_Reward_Group_Played(checkRewardGroupID))
		RETURN (checkRewardGroupID)
	ENDIF
	
	// Nothing currently unlocked but not played
	RETURN NO_HEIST_REWARD_GROUP

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return a Rockstar Created array position containing a random one of the missoins unlocked in a specific reward Batch
//
// INPUT PARAMS:			paramRewardGroupID		The Reward GroupID to search
//							paramSpecificHashRCID	[DEFAULT = 0] Non-0 means a specific mission is being selected (this was added so that Hasta La Vista I could be specified)
//							paramIgnoreHashRCID		[DEFAULT = 0] Non-0 means discount this RCID from the search (this is to ensure a second invite isn't a duplicate of a first invite)
// RETURN VALUE:			INT						The Rockstar Created array position containing the mission, or ILLEGAL_ARRAY_POSITION
FUNC INT Get_Rockstar_Created_ArrayPos_For_Random_Mission_In_Reward_Group(g_eHeistRewardGroupID paramRewardGroupID, INT paramSpecificHashRCID = 0, INT paramIgnoreHashRCID = 0)

	IF (paramRewardGroupID = NO_HEIST_REWARD_GROUP)
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [Heist]: Get_Rockstar_Created_ArrayPos_For_Random_Mission_In_Reward_Group(): ERROR - ILLEGAL Reward Batch: ", Convert_Reward_GroupID_To_String(paramRewardGroupID))
			SCRIPT_ASSERT("Get_Rockstar_Created_ArrayPos_For_Random_Mission_In_Reward_Group(): ERROR - ILLEGAL Reward Batch. Tell Keith.")
		#ENDIF
		
		RETURN (ILLEGAL_ARRAY_POSITION)
	ENDIF

	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Heist]: Get_Rockstar_Created_ArrayPos_For_Random_Mission_In_Reward_Group(): ", Convert_Reward_GroupID_To_String(paramRewardGroupID))
		IF (paramSpecificHashRCID != 0)
			PRINTLN(".KGM [Heist]: Get_Rockstar_Created_ArrayPos_For_Random_Mission_In_Reward_Group() - Choosing specific Mission. Hash RCID: ", paramSpecificHashRCID)
		ENDIF
		IF (paramIgnoreHashRCID != 0)
			PRINTLN(".KGM [Heist]: Get_Rockstar_Created_ArrayPos_For_Random_Mission_In_Reward_Group() - Ignore specific Mission. Hash RCID: ", paramIgnoreHashRCID)
		ENDIF
	#ENDIF
	
	// Safety check, ensure the Specific and Ignore RCIDs are different
	IF (paramSpecificHashRCID != 0)
	AND (paramIgnoreHashRCID != 0)
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [Heist]: Get_Rockstar_Created_ArrayPos_For_Random_Mission_In_Reward_Group(): ERROR - Specific RCID and Ignore RCID are the same. Ignoring Both by setting to 0")
			SCRIPT_ASSERT("Get_Rockstar_Created_ArrayPos_For_Random_Mission_In_Reward_Group() - ERROR: Specific RCID and Ignore RCID are the same. Ignoring Both by setting to 0. Tell Keith.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
		
		paramSpecificHashRCID = 0
		paramIgnoreHashRCID = 0
	ENDIF

	// Delay if the initial cloud download hasn't been performed
	IF NOT (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
		PRINTLN(".KGM [Heist]: ...DELAY: Initial Cloud Load hasn't completed. Allow Retry later.")
		RETURN (ILLEGAL_ARRAY_POSITION)
	ENDIF
	
	// Prepare an array of ArrayPositions holding the missions in the batch
	// ...need an array maximum as a boundary - this is randomly chosen based on current info so will need a boundary check in case it needs increased
	CONST_INT	MAX_MISSIONS_IN_REWARD_GROUP	10
	
	INT potentialMissionArrayPos[MAX_MISSIONS_IN_REWARD_GROUP]
	INT numPotentialMissionsFound = 0
	
	INT thisRCID = 0
	INT tempLoop = 0
	
	REPEAT FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD tempLoop
		IF (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
			// Ensure this is a Versus mission
			IF (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iType = FMMC_TYPE_MISSION)
				IF (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iSubType = FMMC_MISSION_TYPE_VERSUS)
					// ...found a Versus mission, so check which reward group it is in (if any)
					thisRCID = g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[tempLoop].iRootContentIdHash
					
					// ...store these details if this mission is for the correct reward groupID
					IF (Get_Heist_Reward_Versus_Mission_GroupID(thisRCID) = paramRewardGroupID)
						// ...found a potential mission, so store it on the potential missions array
						PRINTLN(".KGM [Heist]: ...POTENTIAL MISSION. ", numPotentialMissionsFound, ". ArrayPos: ", tempLoop, " - ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].tlMissionName)
						
						// Is this the RCID specifically requested?
						IF (paramSpecificHashRCID = 0)
						OR ((paramSpecificHashRCID != 0) AND (thisRCID = paramSpecificHashRCID))
							IF (numPotentialMissionsFound < MAX_MISSIONS_IN_REWARD_GROUP)
								// Check if this is the RCID to ignore
								IF (paramIgnoreHashRCID != 0)
								AND (paramIgnoreHashRCID = thisRCID)
									PRINTLN(".KGM [Heist]: ......IGNORE ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].tlMissionName,": This RCID Hash has to be ignored: ", paramIgnoreHashRCID, ".  Found: ", thisRCID)
								ELSE
									potentialMissionArrayPos[numPotentialMissionsFound] = tempLoop
									numPotentialMissionsFound++
								ENDIF
							ELSE
								PRINTLN(".KGM [Heist]: ......RAN OUT OF ROOM ON POTENTIAL MISSIONS ARRAY.")
								SCRIPT_ASSERT("Get_Rockstar_Created_ArrayPos_For_Random_Mission_In_Reward_Group(). Out of room on potential missions array. Increase MAX_MISSIONS_IN_REWARD_GROUP. Tell Keith.")
							ENDIF
						ELSE
							PRINTLN(".KGM [Heist]: ......IGNORE: This isn't the specifically requested RCID Hash. Required: ", paramSpecificHashRCID, ".  Found: ", thisRCID)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Are there any potential missions?
	IF (numPotentialMissionsFound = 0)
		PRINTLN(".KGM [Heist]: ...DELAY: There are no potential missions found. Allow Retry later.")
		RETURN (ILLEGAL_ARRAY_POSITION)
	ENDIF
	
	// Randomly choose one
	INT chosenMission = GET_RANDOM_INT_IN_RANGE(0, numPotentialMissionsFound)
	
	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Heist]: Choosing mission ", chosenMission, ".  ArrayPos: ", potentialMissionArrayPos[chosenMission], " - ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[potentialMissionArrayPos[chosenMission]].tlMissionName)
	#ENDIF
	
	RETURN (potentialMissionArrayPos[chosenMission])

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Send the Heist Reward Invite based on the array position within the Rockstar Created array
//
// INPUT PARAMS:			paramArrayPos			Array Position of mission within Rockster Created array
//							paramSpecificDescTL		String containing the specific description to use in the invite - if required
//							paramReAdding			[DEFAULT = FALSE] TRUE if this mission is being re-added after a session change, FALSE if not
PROC Send_Heist_Reward_Invite(INT paramArrayPos, TEXT_LABEL_63 paramSpecificDescTL, BOOL paramReAdding = FALSE)

	#IF IS_DEBUG_BUILD
		IF (paramReAdding)
			PRINTLN(".KGM [ActSelect][Heist]: Re-Adding Active Invite to Heist Reward Mission")
		ELSE
			PRINTLN(".KGM [ActSelect][Heist]: Send Invite to Heist Reward Mission")
		ENDIF
	#ENDIF

	INT					theType			= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramArrayPos].iType
	INT					theSubtype		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramArrayPos].iSubType
	VECTOR				theCoords		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramArrayPos].vStartPos
	INT					theCreatorID	= FMMC_ROCKSTAR_CREATOR_ID
	enumCharacterList	theNPC			= CHAR_MARTIN
	TEXT_LABEL_63		theMissionName	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramArrayPos].tlMissionName
	TEXT_LABEL_23		theContentID	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[paramArrayPos].tlName
	
	TEXT_LABEL_63		theDescription	= ""
	IF NOT (IS_STRING_NULL_OR_EMPTY(paramSpecificDescTL))
		theDescription = paramSpecificDescTL
		PRINTLN(".KGM [ActSelect][Heist]: Using Specific Description TextLabel: ", paramSpecificDescTL)
	ENDIF
	
	Store_Invite_Details_For_Joblist_From_NPC(theType, theSubtype, theCoords, theCreatorID, theNPC, theMissionName, theDescription, theContentID, paramReAdding)
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if there is an external reason to prevent the invite being sent
//
// RETURN VALUE:			BOOL		TRUE if there is a restriction, FALSE if not
FUNC BOOL Is_There_An_External_Restriction_To_Prevent_Sending_Heist_Reward_Invite()

	IF (NETWORK_IS_ACTIVITY_SESSION())
	OR (Is_There_A_MissionsAtCoords_Focus_Mission())
	OR (IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID()))
	OR (IS_TRANSITION_SESSION_RESTARTING())
	OR (HAS_TRANSITION_SESSION_SELECTED_RESTART_RANDOM())
	OR (IS_PLAYER_SCTV(PLAYER_ID()))
	OR (IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL())
	OR (IS_A_SPECTATOR_CAM_ACTIVE())
	OR (IS_CUTSCENE_PLAYING())
	OR NOT (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
		// Restriction
		RETURN TRUE
	ENDIF
	
	// No restriction
	RETURN FALSE
	
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the Delay before the second invite gets sent
PROC Generate_Heist_Reward_2nd_Invite_Delay()

	// If possible, we want the 2nd invite delay to be halfway through the delay between the just passed heist and the next heist.
	// If this isn't possible then use a default delay
	
	// Can we try to use the 'between heists' timings when the Heist COntroller is waiting for the initial delay to expire?
	SWITCH (Get_Current_Heist_Control_Stage())
		CASE HEIST_CLIENT_STAGE_NO_HEIST
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY
			BREAK
			
		// Not waiting for initial delay to expire, so use default delay
		DEFAULT
			PRINTLN(".KGM [Heist]: Generate_Heist_Reward_2nd_Invite_Delay() - Not 'Between Heists' so using Default Delay: ", HEIST_REWARD_2nd_INVITE_DEFAULT_DELAY_msec)
			g_sLocalMPHeistUnlocks.lhuDelayTimeout = GET_GAME_TIMER() + HEIST_REWARD_2nd_INVITE_DEFAULT_DELAY_msec
			EXIT
	ENDSWITCH
	
	// Still between Heists
	// Calculate how much of a delay remains
	INT betweenHeistsDelayRemaining = g_sLocalMPHeistControl.lhcTimeout - GET_GAME_TIMER()

	PRINTLN(".KGM [Heist]: Generate_Heist_Reward_2nd_Invite_Delay() - 'Between Heists' Delay Remaining: ", betweenHeistsDelayRemaining, " msec")
	
	// Has it expired?
	INT halfDelay = 0
	INT useTime = 0
	IF (betweenHeistsDelayRemaining > 0)
		// ...no, so check if the delay is inside the 'after tutorial heist' delay time
		IF (betweenHeistsDelayRemaining < HEIST_CONTROL_TUTORIAL_TO_NEXT_HEIST_DELAY_msec)
			// ...yes, so if there is more than half this delay remaining, then send the second invite at half the delay time, otherwise use default delay
			halfDelay = HEIST_CONTROL_TUTORIAL_TO_NEXT_HEIST_DELAY_msec / 2
			
			IF (betweenHeistsDelayRemaining > halfDelay)
				useTime = betweenHeistsDelayRemaining - halfDelay
				PRINTLN(".KGM [Heist]: Generate_Heist_Reward_2nd_Invite_Delay() - greater than half 'after Tutorial heist' delay remains, so using 'half delay': ", useTime)
				g_sLocalMPHeistUnlocks.lhuDelayTimeout = GET_GAME_TIMER() + useTime
			ELSE
				PRINTLN(".KGM [Heist]: Generate_Heist_Reward_2nd_Invite_Delay() - less than half 'after Tutorial heist' delay remains, so using Default Delay: ", HEIST_REWARD_2nd_INVITE_DEFAULT_DELAY_msec)
				g_sLocalMPHeistUnlocks.lhuDelayTimeout = GET_GAME_TIMER() + HEIST_REWARD_2nd_INVITE_DEFAULT_DELAY_msec
			ENDIF
			
			EXIT
		ENDIF
		
		// Not inside the 'after Tutorial Heist' delay time, so check if using the 'after normal heist' delay time
		IF (betweenHeistsDelayRemaining < HEIST_CONTROL_BETWEEN_HEISTS_DELAY_msec)
			// ...yes, so if there is more than half this delay remaining, then send the second invite at half the delay time, otherwise use default delay
			halfDelay = HEIST_CONTROL_BETWEEN_HEISTS_DELAY_msec / 2
			
			IF (betweenHeistsDelayRemaining > halfDelay)
				useTime = betweenHeistsDelayRemaining - halfDelay
				PRINTLN(".KGM [Heist]: Generate_Heist_Reward_2nd_Invite_Delay() - greater than half 'between heists' delay remains, so using 'half delay': ", useTime)
				g_sLocalMPHeistUnlocks.lhuDelayTimeout = GET_GAME_TIMER() + useTime
			ELSE
				PRINTLN(".KGM [Heist]: Generate_Heist_Reward_2nd_Invite_Delay() - less than half 'between heists' delay remains, so using Default Delay: ", HEIST_REWARD_2nd_INVITE_DEFAULT_DELAY_msec)
				g_sLocalMPHeistUnlocks.lhuDelayTimeout = GET_GAME_TIMER() + HEIST_REWARD_2nd_INVITE_DEFAULT_DELAY_msec
			ENDIF
			
			EXIT
		ENDIF
	ENDIF
	
	PRINTLN(".KGM [Heist]: Generate_Heist_Reward_2nd_Invite_Delay() - using Default Delay: ", HEIST_REWARD_2nd_INVITE_DEFAULT_DELAY_msec)
	g_sLocalMPHeistUnlocks.lhuDelayTimeout = GET_GAME_TIMER() + HEIST_REWARD_2nd_INVITE_DEFAULT_DELAY_msec
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the 'No Reward' Heist Reward Stage
PROC Maintain_Heist_Reward_Stage_No_Reward()

	// Wait for timeout to cut down on processing
	IF (g_sLocalMPHeistUnlocks.lhuDelayTimeout > 0)
		IF (GET_GAME_TIMER() < g_sLocalMPHeistUnlocks.lhuDelayTimeout)
			// ...still in sleep mode
			EXIT
		ENDIF
	ENDIF
	
	// Timeout. Check if new Reward Processing is required this frame
	PRINTLN(".KGM [Heist]: Check for Heist Reward processing.")
	g_sLocalMPHeistUnlocks.lhuDelayTimeout = 0

	// Is there any reason to prevent processing?
	IF (Is_There_An_External_Restriction_To_Prevent_Sending_Heist_Reward_Invite())
		// ...yes, so delay processing
		IF (Is_There_An_Active_Heist_Reward_Invite_Group())
			// ...there is an active heist reward invite group, so use a short delay to allow it to get re-added quickly, UNLESS there is a Focus Mission
			IF (Is_There_A_MissionsAtCoords_Focus_Mission())
				g_sLocalMPHeistUnlocks.lhuDelayTimeout = GET_GAME_TIMER() + HEIST_REWARD_SNOOZE_DELAY_msec
				PRINTLN(".KGM [Heist]: ...delayed. Active Invite, but Player has a Focus Mission. Snooze: ", HEIST_REWARD_SNOOZE_DELAY_msec)
			ELSE
				g_sLocalMPHeistUnlocks.lhuDelayTimeout = GET_GAME_TIMER() + HEIST_REWARD_ACTIVE_INVITE_SNOOZE_DELAY_msec
				PRINTLN(".KGM [Heist]: ...delayed. Use Active Invite Snooze: ", HEIST_REWARD_ACTIVE_INVITE_SNOOZE_DELAY_msec)
			ENDIF
			
			EXIT
		ENDIF
		
		// ...there is no active Heist Reward Invite, so delay processing for a while
		g_sLocalMPHeistUnlocks.lhuDelayTimeout = GET_GAME_TIMER() + HEIST_REWARD_SNOOZE_DELAY_msec
		PRINTLN(".KGM [Heist]: ...delayed. Snooze: ", HEIST_REWARD_SNOOZE_DELAY_msec)
		EXIT
	ENDIF
	
	// Check for an existing Heist Reward Invite that needs re-added
	IF (Is_There_An_Active_Heist_Reward_Invite_Group())
		PRINTLN(".KGM [Heist]: Found an Active Heist Versus Invite Group - checking if Invites need setup again.")
		INT activeInviteArrayPos = Get_1st_Active_Invite_Rockstar_Created_ArrayPos()
		INT activeInviteArrayPos_2nd = Get_2nd_Active_Invite_Rockstar_Created_ArrayPos()
		IF (activeInviteArrayPos = ILLEGAL_ARRAY_POSITION)
		AND (activeInviteArrayPos_2nd = ILLEGAL_ARRAY_POSITION)
			// ...no mission found, if both have been played then clear the active invite details and continue checking for a new reward to process
			IF (Is_This_Heist_Reward_Group_1st_Mission_Played(g_sActiveMPHeistReward.ahrInviteGroupID))
			AND (Is_This_Heist_Reward_Group_2nd_Mission_Played(g_sActiveMPHeistReward.ahrInviteGroupID))
				// Both missions in the group have been played
				PRINTLN(".KGM [Heist]: ...BUT failed to find any Active Invite missions on the Rockstar Created array and both missions played - clearing the details")
				Mark_Heist_Reward_Group_As_Not_Available(g_sActiveMPHeistReward.ahrInviteGroupID)
				Clear_Heist_Reward_Active_Invite()
			ENDIF
		ELSE
			// ...found at least one mission, so re-add them and move to 'Invite Sent' stage
			TEXT_LABEL_63 specificDescTL = ""
			IF (activeInviteArrayPos != ILLEGAL_ARRAY_POSITION)
				PRINTLN(".KGM [Heist]: ...1st mission found on the Rockstar Created array position: ", activeInviteArrayPos)

				// The 'tutorial' reward should use a specific description for the invite instead of the Versus Mission description
				IF (g_sActiveMPHeistReward.ahrInviteGroupID = HEIST_REWARD_VERSUS_AFTER_TUTORIAL)
					specificDescTL = "HEIST_TUTSMS"
				ENDIF
				Send_Heist_Reward_Invite(activeInviteArrayPos, specificDescTL, TRUE)
			
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [Heist]: Details of Heist Reward Invite Sent. GroupID: ", Convert_Reward_GroupID_To_String(g_sActiveMPHeistReward.ahrInviteGroupID), ". RCID: ", g_sActiveMPHeistReward.ahrInviteMissionRCID)
				#ENDIF
			ENDIF
			
			IF (activeInviteArrayPos_2nd != ILLEGAL_ARRAY_POSITION)
				PRINTLN(".KGM [Heist]: ...2nd mission found on the Rockstar Created array position: ", activeInviteArrayPos_2nd)

				specificDescTL = ""
				Send_Heist_Reward_Invite(activeInviteArrayPos_2nd, specificDescTL, TRUE)
			
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [Heist]: Details of Heist Reward Invite Sent. GroupID: ", Convert_Reward_GroupID_To_String(g_sActiveMPHeistReward.ahrInviteGroupID), ". RCID: ", g_sActiveMPHeistReward.ahrInviteMissionRCID_2nd)
				#ENDIF
			ENDIF
			
			Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_INVITE_SENT)
			
			EXIT
		ENDIF
	ENDIF
	
	// ...is there an unlocked but unplayed reward group to deal with?
	g_eHeistRewardGroupID unplayedGroupID = NO_HEIST_REWARD_GROUP
	IF (Is_There_An_Active_Heist_Reward_Invite_Group())
		unplayedGroupID = g_sActiveMPHeistReward.ahrInviteGroupID
	ELSE
		unplayedGroupID = Get_Earliest_Unlocked_But_Unplayed_Reward_GroupID()
	ENDIF

	IF (unplayedGroupID = NO_HEIST_REWARD_GROUP)
		// ...no, so snooze for a while
		g_sLocalMPHeistUnlocks.lhuDelayTimeout = GET_GAME_TIMER() + HEIST_REWARD_SNOOZE_DELAY_msec
		PRINTLN(".KGM [Heist]: ...nothing to process. Snooze.")
		EXIT
	ENDIF
	
	// Good to go
	g_sLocalMPHeistUnlocks.lhuDelayTimeout = GET_GAME_TIMER() + g_sLocalMPHeistUnlocks.lhuInitialDelayToUse
	
	// If the first mission has been played then jump to the 2nd Stage initial delay, setting the current reward group being dealt with
	IF (Is_This_Heist_Reward_Group_1st_Mission_Played(unplayedGroupID))
		PRINTLN(".KGM [Heist]: There is an unlocked Heist Reward group with first mission played. Activate processing. Setting 2nd Invite Delay before Invite: ", g_sLocalMPHeistUnlocks.lhuInitialDelayToUse, " msec")
		g_sActiveMPHeistReward.ahrInviteGroupID	= unplayedGroupID
		Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_2nd_INVITE_DELAY)
		EXIT
	ENDIF

	PRINTLN(".KGM [Heist]: There is an unlocked Heist Reward group with unplayed invites. Activate processing. Setting Initial Delay before Invite: ", g_sLocalMPHeistUnlocks.lhuInitialDelayToUse, " msec")
	Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_INITIAL_DELAY)

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the 'Initial Delay' Heist Reward Stage
PROC Maintain_Heist_Reward_Stage_Initial_Delay()

	// Wait for the initial delay to expire
	IF (GET_GAME_TIMER() < g_sLocalMPHeistUnlocks.lhuDelayTimeout)
		// ...delay still active
		EXIT
	ENDIF

	// Good to go
	g_sLocalMPHeistUnlocks.lhuDelayTimeout = 0
	
	PRINTLN(".KGM [ActSelect][Heist]: Initial Heist Reward Group Processing delay has expired.")
	Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_SEND_SMS)
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the 'Send SMS' Heist Reward Stage
PROC Maintain_Heist_Reward_Stage_Send_SMS()

	// KGM 5/11/14 [BUG 2111097]: We are no longer sending a separate SMS before the invite, so moving on.
	PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_Send_SMS() - Now Ignoring Send SMS Heist Reward Stage")
	Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_SEND_INVITE)
	
	EXIT

//	// Find the first group?
//	// NOTE: This isn't guaranteed to find an unplayed group since anything may have changed during the initial delay, so no error if an unplayed group isn't found
//	g_eHeistRewardGroupID rewardGroupIdForInvite = Get_Earliest_Unlocked_But_Unplayed_Reward_GroupID()
//	IF (rewardGroupIdForInvite = NO_HEIST_REWARD_GROUP)
//		PRINTLN(".KGM [ActSelect][Heist]: Preparing to send Heist Reward SMS, but there is no Unlocked but Unplayed Rewards. Quit Reward processing.")
//		Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_NO_REWARD, HEIST_REWARD_RETRY_DELAY_msec)
//		
//		EXIT
//	ENDIF
//	
//	// NOTE: A bit lazy, but currently only the TUTORIAL Heist uses a pre-Invite SMS, so I'm just going to hardcode this to the Tutorial Heist.
//	//			We can extract the functionality into functions if another pre-Invite SMS is required
//	
//	// Is an SMS required for this group
//	// FOR NOW: Hardcoded to Tutorial reward group
//	IF (rewardGroupIdForInvite != HEIST_REWARD_VERSUS_AFTER_TUTORIAL)
//		// ...SMS not required
//		#IF IS_DEBUG_BUILD
//			PRINTLN(".KGM [ActSelect][Heist]: A pre-Invite SMS is not required for this Heist Reward GroupID: ", Convert_Reward_GroupID_To_String(rewardGroupIdForInvite))
//		#ENDIF
//		
//		// ...move on to sending the invite
//		Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_SEND_INVITE)
//		EXIT
//	ENDIF
//	
//	// Has the SMS already been sent?
//	// FOR NOW: Hardcoded to Tutorial reward group
//	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_SMS_SENT))
//		// ...SMS for group already sent
//		#IF IS_DEBUG_BUILD
//			PRINTLN(".KGM [ActSelect][Heist]: A pre-Invite SMS has already been sent for this Heist Reward GroupID: ", Convert_Reward_GroupID_To_String(rewardGroupIdForInvite))
//		#ENDIF
//		
//		// ...move on to sending the invite
//		Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_SEND_INVITE)
//		EXIT
//	ENDIF
//	
//	// Check if there is now an external reason to delay processing
//	IF (Is_There_An_External_Restriction_To_Prevent_Sending_Heist_Reward_Invite())
//		PRINTLN(".KGM [ActSelect][Heist]: Preparing to send Heist Reward pre-Invite SMS, but there is an external restriction. Quit Reward processing.")
//		Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_NO_REWARD, HEIST_REWARD_RETRY_DELAY_msec)
//		
//		EXIT
//	ENDIF
//	
//	// Try to send the SMS
//	#IF IS_DEBUG_BUILD
//		PRINTLN(".KGM [ActSelect][Heist]: Attempting to send Heist pre-Invite SMS for Heist Reward GroupID: ", Convert_Reward_GroupID_To_String(rewardGroupIdForInvite))
//	#ENDIF
//		
//	// FOR NOW: Hardcoded to Tutorial reward group
//	IF (Request_MP_Comms_Txtmsg(CHAR_MARTIN, "HEIST_TUTSMS"))
//		PRINTLN(".KGM [ActSelect][Heist]: ...SUCCESS - wait for SMS to complete")
//		Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_ACTIVE_SMS)
//		
//		EXIT
//	ENDIF
//	
//	PRINTLN(".KGM [ActSelect][Heist]: ...still waiting for SMS to send")

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the 'Active SMS' Heist Reward Stage
PROC Maintain_Heist_Reward_Stage_Active_SMS()

	// KGM 5/11/14 [BUG 2111097]: We are no longer sending a separate SMS before the invite, so moving on.
	PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_Active_SMS() - Now Ignoring Send SMS Heist Reward Stage")
	Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_SEND_INVITE)
	
	EXIT

//	IF NOT (Is_MP_Comms_Still_Playing())
//		PRINTLN(".KGM [ActSelect][Heist]: Heist Reward pre-Invite SMS has completed. Introduce pre-Invite delay: ", HEIST_REWARD_POST_SMS_PRE_INVITE_DELAY_msec, " msec")
//		g_sLocalMPHeistUnlocks.lhuDelayTimeout = GET_GAME_TIMER() + HEIST_REWARD_POST_SMS_PRE_INVITE_DELAY_msec
//		
//		// Mark the SMS as having been sent to prevent re-sending if the Invite get delayed
//		// Hardcoded to Tutorial Heist
//		PRINTLN(".KGM [ActSelect][Heist]: Setting Heist Reward pre-Invite SMS as SENT.")
//		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_SMS_SENT)
//		
//		Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_DELAY_AFTER_SMS)
//		
//		EXIT
//	ENDIF
//	
//	PRINTLN(".KGM [ActSelect][Heist]: Waiting for Heist Reward pre-Invite SMS to complete")

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the 'Delay After SMS' Heist Reward Stage
PROC Maintain_Heist_Reward_Stage_Delay_After_SMS()

	// KGM 5/11/14 [BUG 2111097]: We are no longer sending a separate SMS before the invite, so moving on.
	PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_Delay_After_SMS() - Now Ignoring Send SMS Heist Reward Stage")
	Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_SEND_INVITE)
	
	EXIT

//	// Wait for the delay after sending the SMS to expire
//	IF (GET_GAME_TIMER() < g_sLocalMPHeistUnlocks.lhuDelayTimeout)
//		// ...delay still active
//		EXIT
//	ENDIF
//
//	// Good to go
//	g_sLocalMPHeistUnlocks.lhuDelayTimeout = 0
//	
//	PRINTLN(".KGM [ActSelect][Heist]: Initial Heist Reward Group Post-SMS delay has expired. Allow sending the Invite.")
//	Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_SEND_INVITE)
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the 'Send Invite' Heist Reward Stage
PROC Maintain_Heist_Reward_Stage_Send_Invite()

	PRINTLN(".KGM [Heist]: Attempting to send Heist Reward Group invite.")
	
	// Check if there is now an external reason to delay processing
	IF (Is_There_An_External_Restriction_To_Prevent_Sending_Heist_Reward_Invite())
		PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_Send_Invite(), there is an external restriction. Quit Reward processing.")
		Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_NO_REWARD, HEIST_REWARD_RETRY_DELAY_msec)
		
		EXIT
	ENDIF

	// At this stage there should be a newly unlocked group of missions that still require an Invite.
	// NOTE: This isn't guaranteed to find an unplayed group since anything may have changed during the initial delay, so no error if an unplayed group isn't found
	g_eHeistRewardGroupID rewardGroupIdForInvite = Get_Earliest_Unlocked_But_Unplayed_Reward_GroupID()
	IF (rewardGroupIdForInvite = NO_HEIST_REWARD_GROUP)
		PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_Send_Invite(), there is no Unlocked but Unplayed Rewards. Quit Reward processing.")
		Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_NO_REWARD, HEIST_REWARD_RETRY_DELAY_msec)
		
		EXIT
	ENDIF
	
	// Randomly choose a Rockstar Created array position containing a mission in the current reward groupID
	INT arrayPos = ILLEGAL_ARRAY_POSITION
	
	// KGM 16/12/14 [BUG 2168654]: The Hasta La Vista reward group should always choose the Storm Drain mission
	INT specificMissionHashRCID = 0
	IF (rewardGroupIdForInvite = HEIST_REWARD_VERSUS_AFTER_PRISON)
		specificMissionHashRCID = g_sMPTunables.iHastaLaVista0	// Hasta La Vista I (Storm Drain)
		PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_Send_Invite(), specifically choosing Hasta La Vista I after Prison Break. HashRCID: ", specificMissionHashRCID)
	ENDIF
	arrayPos = Get_Rockstar_Created_ArrayPos_For_Random_Mission_In_Reward_Group(rewardGroupIdForInvite, specificMissionHashRCID)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		// Failed to find the array position for a Group mission, so quit processing
		PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_Send_Invite(), failed to find a mission in the Group. Quit Reward processing.")
		Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_NO_REWARD, HEIST_REWARD_RETRY_DELAY_msec)
		
		EXIT
	ENDIF
	
	// Send a Heist Reward Invite
	// Use a specific description for the invite?
	// KGM 4/11/14 [BUG 2111097]: If this is for the Tutorial Reward Group then use what used to be the SMS text as the description for the invite
	TEXT_LABEL_63 specificDescTL = ""
	IF (Get_Earliest_Unlocked_But_Unplayed_Reward_GroupID() = HEIST_REWARD_VERSUS_AFTER_TUTORIAL)
		specificDescTL = "HEIST_TUTSMS"
	ENDIF
	Send_Heist_Reward_Invite(arrayPos, specificDescTL)
	
	// Store the details on the 'active invite' array
	g_sActiveMPHeistReward.ahrInviteGroupID			= rewardGroupIdForInvite
	g_sActiveMPHeistReward.ahrInviteMissionRCID		= g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[arrayPos].iRootContentIdHash
	
	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_Send_Invite(), Storing Details of Heist Reward Invite Sent. GroupID: ", Convert_Reward_GroupID_To_String(g_sActiveMPHeistReward.ahrInviteGroupID), ". RCID: ", g_sActiveMPHeistReward.ahrInviteMissionRCID)
	#ENDIF
	
	Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_INVITE_SENT)
		
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the 'Invite Sent' Heist Reward Stage
// NOTES:	This is a distribution stage that will move on based on whether or not there is a 2nd Invite sent or to be sent, etc.
PROC Maintain_Heist_Reward_Stage_Invite_Sent()

	// If there is already a 2nd Invite sent (ie: re-added after a session change) then move on
	IF (g_sActiveMPHeistReward.ahrInviteMissionRCID_2nd != 0)
		PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_Invite_Sent() - A second Invite has already been sent, so move on")
		Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_ALL_INVITES_SENT)
		EXIT
	ENDIF
	
	// Has the reward group's 2nd mission been played (this will also be TRUE if a second invite is not required)?
	IF (Is_This_Heist_Reward_Group_2nd_Mission_Played(g_sActiveMPHeistReward.ahrInviteGroupID))
		PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_Invite_Sent() - 2nd Mission already played (or no invite required), so moving on")
		Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_ALL_INVITES_SENT)
		EXIT
	ENDIF
	
	// 2nd Invite is required and not played, so calculate the second invite delay
	PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_Invite_Sent() - Allow 2nd Invite")
	Generate_Heist_Reward_2nd_Invite_Delay()
	
	Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_2nd_INVITE_DELAY)
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the '2nd Invite Delay' Heist Reward Stage
PROC Maintain_Heist_Reward_Stage_2nd_Invite_Delay()

	// Wait for the 2nd Invite delay to expire
	IF (GET_GAME_TIMER() < g_sLocalMPHeistUnlocks.lhuDelayTimeout)
		// ...delay still active
		EXIT
	ENDIF

	// Good to go
	g_sLocalMPHeistUnlocks.lhuDelayTimeout = 0

	PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_2nd_Invite_Delay() - Delay Expired")
	Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_SEND_2nd_INVITE)
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the 'Send 2nd Invite' Heist Reward Stage
PROC Maintain_Heist_Reward_Stage_Send_2nd_Invite()

	PRINTLN(".KGM [Heist]: Attempting to send Heist Reward Group 2nd invite.")
	
	// Check if there is now an external reason to delay processing
	IF (Is_There_An_External_Restriction_To_Prevent_Sending_Heist_Reward_Invite())
		PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_Send_2nd_Invite(), there is an external restriction. Delay Send Attempt.")
		Generate_Heist_Reward_2nd_Invite_Delay()
		Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_2nd_INVITE_DELAY)
		
		EXIT
	ENDIF

	// At this stage there should be an active reward group.
	g_eHeistRewardGroupID rewardGroupIdForInvite = g_sActiveMPHeistReward.ahrInviteGroupID
	IF (rewardGroupIdForInvite = NO_HEIST_REWARD_GROUP)
		PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_Send_2nd_Invite(), BUT: there was no active reward group. Move on.")
		Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_ALL_INVITES_SENT)
		
		EXIT
	ENDIF
	
	// Randomly choose a Rockstar Created array position containing a mission in the current reward groupID
	// NOTE: Ignore the first invite's RCID
	INT ignoreFirstInviteRCID = 0
	IF (g_sActiveMPHeistReward.ahrInviteMissionRCID != 0)
		ignoreFirstInviteRCID = g_sActiveMPHeistReward.ahrInviteMissionRCID
		PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_Send_2nd_Invite(): Ignore RCID of first Invite Mission: ", ignoreFirstInviteRCID)
	ELSE
		IF (g_sActiveMPHeistReward.ahrInviteMissionRCID_played != 0)
			ignoreFirstInviteRCID = g_sActiveMPHeistReward.ahrInviteMissionRCID_played
			PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_Send_2nd_Invite(): Ignore RCID of already Played Invite Mission: ", ignoreFirstInviteRCID)
		ENDIF
	ENDIF
	
	INT specificMissionHashRCID = 0
	INT arrayPos = Get_Rockstar_Created_ArrayPos_For_Random_Mission_In_Reward_Group(rewardGroupIdForInvite, specificMissionHashRCID, ignoreFirstInviteRCID)
	
	IF (arrayPos = ILLEGAL_ARRAY_POSITION)
		// Failed to find the array position for a Group mission, so quit processing
		PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_Send_2nd_Invite(), failed to find a 2nd mission in the Group. Move On.")
		Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_ALL_INVITES_SENT)
		
		EXIT
	ENDIF
	
	// Send a Heist Reward Invite
	TEXT_LABEL_63 specificDescTL = ""
	Send_Heist_Reward_Invite(arrayPos, specificDescTL)
	
	// Store the details on the 'active invite' array
	g_sActiveMPHeistReward.ahrInviteGroupID				= rewardGroupIdForInvite
	g_sActiveMPHeistReward.ahrInviteMissionRCID_2nd		= g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[arrayPos].iRootContentIdHash
	
	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_Send_2nd_Invite(), Storing Details of Heist Reward Invite Sent. GroupID: ", Convert_Reward_GroupID_To_String(g_sActiveMPHeistReward.ahrInviteGroupID), ". 2nd RCID: ", g_sActiveMPHeistReward.ahrInviteMissionRCID_2nd)
	#ENDIF
	
	Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_ALL_INVITES_SENT)
		
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the 'All Invites Sent' Heist Reward Stage
PROC Maintain_Heist_Reward_Stage_All_Invites_Sent()
	
	// Stay in here until either the active invite group details get cleared somewhere else or somewhere else updates the stage
	IF (Is_There_An_Active_Heist_Reward_Invite_Group())
		// End if all invite missions for this reward group have been played
		IF NOT (Is_This_Heist_Reward_Group_1st_Mission_Played(g_sActiveMPHeistReward.ahrInviteGroupID))
		OR NOT (Is_This_Heist_Reward_Group_2nd_Mission_Played(g_sActiveMPHeistReward.ahrInviteGroupID))
			EXIT
		ENDIF
		
		PRINTLN(".KGM [Heist]: Maintain_Heist_Reward_Stage_All_Invites_Sent(): All missions in reward group have been played. Ending the Group: ", Convert_Reward_GroupID_To_String(g_sActiveMPHeistReward.ahrInviteGroupID))
		Mark_Heist_Reward_Group_As_Not_Available(g_sActiveMPHeistReward.ahrInviteGroupID)
		Clear_Heist_Reward_Active_Invite()
	ENDIF
	
	PRINTLN(".KGM [Heist]: There is no longer any Active Invite details stored. Resetting Heist Reward Processing.")
	
	Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_NO_REWARD, HEIST_REWARD_RETRY_DELAY_msec)
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player is currently on a Heist Reward Versus Mission from the current Unplayed group
PROC Maintain_Checking_For_Heist_Reward_Being_Played()

	// Don't do this on this frame if the initial cloud load hasn't completed
	IF NOT (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
		EXIT
	ENDIF

	BOOL	wasPlayingVersus	= g_sLocalMPHeistUnlocks.lhuOnVersusMission
	BOOL	isPlayingVersus		= Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())
	
	// Is there a change in state?
	IF (wasPlayingVersus = isPlayingVersus)
		EXIT
	ENDIF
	
	// There is a change of state so if no longer on a Versus Mission then reset the flag
	IF NOT (isPlayingVersus)
		// Reset the 'on Versus' flag
		#IF IS_DEBUG_BUILD
			NET_PRINT(".KGM [Heist]:")
			NET_PRINT_TIME()
			NET_PRINT("Player is no longer playing a Versus Mission")
			NET_NL()
		#ENDIF
		
		g_sLocalMPHeistUnlocks.lhuOnVersusMission = FALSE
		
		EXIT
	ENDIF

	// Ensure the Versus Mission details are available - use the mission triggerer data since it sets the 'currently on CM' function's data
	TEXT_LABEL_31 theContentID = g_sTriggerMP.mtMissionData.mdID.idCloudFilename
	IF (IS_STRING_NULL_OR_EMPTY(theContentID))
		EXIT
	ENDIF
	
	INT theRCID = Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(g_sTriggerMP.mtMissionData.mdID)
	
	// Store the change of state
	g_sLocalMPHeistUnlocks.lhuOnVersusMission = TRUE
	
	// Player is now on a Versus mission
	#IF IS_DEBUG_BUILD
		NET_PRINT(".KGM [Heist]:")
		NET_PRINT_TIME()
		NET_NL()
		PRINTLN(".KGM [Heist]: Player is now playing a Versus Mission. RootContentID Hash: ", theRCID)
	#ENDIF
	
	// Check if this is a Reward Versus mission
	g_eHeistRewardGroupID theRewardGroupID = Get_Heist_Reward_Versus_Mission_GroupID(theRCID)
	
	IF (theRewardGroupID = NO_HEIST_REWARD_GROUP)
		PRINTLN(".KGM [Heist]: This Versus Mission is NOT a Heist Reward Versus Mission.")
		EXIT
	ENDIF
	
	// This is a reward Versus mission, so check if it is in the currently active invite group
	IF NOT (Is_There_An_Active_Heist_Reward_Invite_Group())
		PRINTLN(".KGM [Heist]: This Versus Mission is a Heist Reward Versus Mission, but there is no active Heist Reward Invite. Ignoring.")
		EXIT
	ENDIF
	
	// There is an active Heist Reward Invite, so check if this is the correct group
	IF (theRewardGroupID != g_sActiveMPHeistReward.ahrInviteGroupID)
		PRINTLN(".KGM [Heist]: This Versus Mission is a Heist Reward Versus Mission, but it is not in the active Heist Reward Invite Group. Ignoring.")
		EXIT
	ENDIF
	
	// Need to clear the Heist Reward Invite details and mark this Reward Group as Played - the player is playing a mission in the Active Invite group
	// Check for First or Second Invite
	IF (theRCID = g_sActiveMPHeistReward.ahrInviteMissionRCID)
		// First Invite
		PRINTLN(".KGM [Heist]: This Versus Mission is the 1st Heist Reward Versus Mission in the active Heist Reward Invite Group. Marking 1st Mission as 'Played'.")
		Mark_Heist_Reward_Group_As_Played(theRewardGroupID)
		
		// If we haven't stored a 'played' RCID, store it now
		IF (g_sActiveMPHeistReward.ahrInviteMissionRCID_played = 0)
			g_sActiveMPHeistReward.ahrInviteMissionRCID_played = g_sActiveMPHeistReward.ahrInviteMissionRCID
			g_sActiveMPHeistReward.ahrInviteMissionRCID = 0
		ENDIF
	ELIF (theRCID = g_sActiveMPHeistReward.ahrInviteMissionRCID_2nd)
		// Second Invite
		PRINTLN(".KGM [Heist]: This Versus Mission is the 2nd Heist Reward Versus Mission in the active Heist Reward Invite Group. Marking 2nd Mission as 'Played'.")
		Mark_Heist_Reward_Group_As_Played(theRewardGroupID, TRUE)
		
		// If we haven't stored a 'played' RCID, store it now
		IF (g_sActiveMPHeistReward.ahrInviteMissionRCID_played = 0)
			g_sActiveMPHeistReward.ahrInviteMissionRCID_played = g_sActiveMPHeistReward.ahrInviteMissionRCID_2nd
			g_sActiveMPHeistReward.ahrInviteMissionRCID_2nd = 0
		ENDIF
	ELSE
		PRINTLN(".KGM [Heist]: This Versus Mission is in the Heist Reward Group but with no Invite. Nothing more needs done.")
	ENDIF
	
	// Check if the whole group has been played
	IF (Is_This_Heist_Reward_Group_Played(theRewardGroupID))
		PRINTLN(".KGM [Heist]: All Invited Versus missions in the reward group have been played.")
		Mark_Heist_Reward_Group_As_Not_Available(theRewardGroupID)
		Clear_Heist_Reward_Active_Invite()
	ENDIF
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain checking to see if the Heist Unlocks Stat Copy has changed so that the STAT can be updated
PROC Maintain_Heist_Unlocks_Stat_Based_On_Copy()

	IF (g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks = g_sLocalMPHeistUnlocks.lhuKnownHeistUnlocksBitset)
		// ...nothing has changed
		EXIT
	ENDIF
	
	// The copy stat has changed, so update the STAT and update the known value of he copy stat
	SET_MP_INT_CHARACTER_STAT(MP_STAT_BITSET_HEIST_VS_MISSIONS, g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks)
	g_sLocalMPHeistUnlocks.lhuKnownHeistUnlocksBitset = g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks
	
	PRINTLN(".KGM [ActSelect][Heist]: The STAT Copy variable has changed. Updating the Reward STAT.")

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Update the Heist Flow Progression Stat
PROC Update_Heist_Flow_Progression_Stat()
	
	PRINTLN(".KGM [ActSelect][Heist]: Updating the Heist Flow Progression STAT on Heist Finale Completion for Leader.")
	
	// Ignore if the player hasn't completed the Lester cutscene
	IF NOT (Has_Intro_To_Heists_Been_Done())
		PRINTLN(".KGM [ActSelect][Heist]: ...IGNORED: Intro To Heists hasn't been completed. This appears to be a debug launch.")
		EXIT
	ENDIF
	
	// Ensure a Heist Strand is in progress (just in case this was a debug launch of the Finale stand-alone)
	IF (Get_Current_Heist_Control_Stage() != HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_COMPLETE)
		PRINTLN(".KGM [ActSelect][Heist]: ...IGNORED: Heist Stage isn't WAIT_FOR_HEIST_STRAND_COMPLETE. This appears to be a stand-alond Finale launch.")
		EXIT
	ENDIF
	
	// Ensure the STAT agrees with the Finale just completed (this prevents the STAT increasing due to a debug launch unless it is the same Heist Strand)
	INT flowProgressionStat		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CURRENT_HEIST_STRAND)
	INT flowProgressionHashRCID	= Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(flowProgressionStat)
	
	IF (flowProgressionHashRCID != Get_Current_Heist_Control_Hash_HeistRCID())
		PRINTLN(".KGM [ActSelect][Heist]: ...IGNORED: Heist Progression Stat doesn't match Current Active Heist Strand. This Strand may have been debug launched or a Replay.")
		EXIT
	ENDIF
	
	// Update the STAT
	flowProgressionStat = Get_Next_Heist_Flow_Progression_Value(flowProgressionStat)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CURRENT_HEIST_STRAND, flowProgressionStat)
	
	PRINTLN(".KGM [ActSelect][Heist]: ...Flow Progression Stat Updated to: ", flowProgressionStat)

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Update any rewards with an immediate STAT update check
// NOTES:	KGM 24/10/14 [BUG 2089694]: Called from FINISH_CURRENT_HEIST_STRAND()
//			KGM 15/11/14 [BUG 2121564]: Also now stores the heist completion phonecall details and activates the timer
PROC Immediate_Heist_Reward_Unlock_And_Stat_Update_On_Finale_Completion()

	PRINTLN(".KGM [Heist]: Immediate_Heist_Reward_Unlock_And_Stat_Update_On_Finale_Completion() - called with RCID: ", Get_Current_Heist_Control_Hash_HeistRCID())
	DEBUG_PRINTCALLSTACK()

	// To ensure this gets saved, the STAT update needs to take place immediately after the Stand Completion check on Finale end
	Unlock_Heist_Reward_On_Heist_Strand_Completion(Get_Current_Heist_Control_Hash_HeistRCID())
	Store_Heist_Completion_Phonecall(Get_Current_Heist_Control_Hash_HeistRCID())
	Activate_Heist_Completion_Phonecall()
	Store_Heist_Replay_Reminder_Help(Get_Current_Heist_Control_Hash_HeistRCID())
	Activate_Heist_Replay_Reminder_Help()
	Store_Heist_Allow_Versus_Pause_Menu_Triggering_Help(Get_Current_Heist_Control_Hash_HeistRCID())
	Maintain_Heist_Unlocks_Stat_Based_On_Copy()
	
	// KGM 2/2/15 [BUG 2220501]: Clear the 'next heist phonecall soon' STAT
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_NEXT_HEIST_QUICK_PHONECALL))
		CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_NEXT_HEIST_QUICK_PHONECALL)
		PRINTLN(".KGM [Heist]: Heist Strand Completion: Clearing the 'next heist phonecall soon' flag.")
	ENDIF

ENDPROC

/// PURPOSE:
///    Query if we should process heist rewards (ie. Adversary mode invites)
///    JA: 24/09/15 [BUG 2532810]: Called from Maintain_MP_Selector_Group_Heist_Rewards()
FUNC BOOL Should_Heist_Rewards_Be_Processed()
	
	IF g_sMPTunables.bEnable_Heist_Reward_Invite_System
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the invite and invite acceptance to Versus Missions unlocked after completion of a Heist
PROC Maintain_MP_Selector_Group_Heist_Rewards()
	
	// Maintain Checking for the mission being Played
	Maintain_Checking_For_Heist_Reward_Being_Played()
	
	// Maintain Check to see if the STAT needs updated based on a change of the global copyStat variable
	Maintain_Heist_Unlocks_Stat_Based_On_Copy()
	
	// We should exit if these invites are disabled
	IF NOT Should_Heist_Rewards_Be_Processed()
		EXIT
	ENDIF
	
	// Maintain the Heist Reward Stages
	SWITCH (Get_Current_Heist_Reward_Stage())
		CASE HEIST_REWARD_STAGE_NO_REWARD
			Maintain_Heist_Reward_Stage_No_Reward()
			BREAK
			
		CASE HEIST_REWARD_STAGE_INITIAL_DELAY
			Maintain_Heist_Reward_Stage_Initial_Delay()
			BREAK
			
		CASE HEIST_REWARD_STAGE_SEND_SMS
			Maintain_Heist_Reward_Stage_Send_SMS()
			BREAK
			
		CASE HEIST_REWARD_STAGE_ACTIVE_SMS
			Maintain_Heist_Reward_Stage_Active_SMS()
			BREAK
			
		CASE HEIST_REWARD_STAGE_DELAY_AFTER_SMS
			Maintain_Heist_Reward_Stage_Delay_After_SMS()
			BREAK
			
		CASE HEIST_REWARD_STAGE_SEND_INVITE
			Maintain_Heist_Reward_Stage_Send_Invite()
			BREAK
			
		CASE HEIST_REWARD_STAGE_INVITE_SENT
			Maintain_Heist_Reward_Stage_Invite_Sent()
			BREAK
			
		CASE HEIST_REWARD_STAGE_2nd_INVITE_DELAY
			Maintain_Heist_Reward_Stage_2nd_Invite_Delay()
			BREAK
			
		CASE HEIST_REWARD_STAGE_SEND_2nd_INVITE
			Maintain_Heist_Reward_Stage_Send_2nd_Invite()
			BREAK
			
		CASE HEIST_REWARD_STAGE_ALL_INVITES_SENT
			Maintain_Heist_Reward_Stage_All_Invites_Sent()
			BREAK
			
		DEFAULT
			#IF IS_DEBUG_BUILD
				PRINTLN("...KGM MP [ActSelect][Heist]: UNKNOWN HEIST REWARD STAGE.")
				SCRIPT_ASSERT("Maintain_MP_Selector_Group_Heist_Rewards() - ERROR: Trying to process Unknown Heist Reward Stage. Add to Switch statement. Tell Keith")
			#ENDIF
			
			BREAK
	ENDSWITCH

ENDPROC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      Local Heist Strand 'Not Unlocked' help text maintenance
// ===========================================================================================================

// PURPOSE:	Display Help Text if the heists aren't unlocked
PROC Maintain_MP_Selector_Group_Heists_Not_Unlocked()

	// Don't display new help if a help message is on display, but check if the 'still locked' help needs cleared
	IF (IS_HELP_MESSAGE_BEING_DISPLAYED())
		IF (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HLOCKED"))
		OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HLOCK_LESTER"))
		OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HLOCK_WAITCALL"))
			// Clear these messages if the player should no longer view them
			// ...not in own apartment?
			IF NOT (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE))
				PRINTLN("...KGM MP [ActSelect][Heist][LockHelp]: Player is not in own apartment: Clearing 'heists still locked' help text.")
				CLEAR_HELP()
				
				EXIT
			ENDIF
			
			// ...got a focus mission?
			IF (Is_There_A_MissionsAtCoords_Focus_Mission())
				PRINTLN("...KGM MP [ActSelect][Heist][LockHelp]: Player has a focus mission: Clearing 'heists still locked' help text.")
				CLEAR_HELP()
				
				EXIT
			ENDIF
			
			// ...heist stage is beyond initial delay
			IF NOT (Get_Current_Heist_Control_Stage() = HEIST_CLIENT_STAGE_NO_HEIST)
			AND NOT (Get_Current_Heist_Control_Stage() = HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY)
				PRINTLN("...KGM MP [ActSelect][Heist][LockHelp]: Player is launching a heist: Clearing 'heists still locked' help text.")
				CLEAR_HELP()
				
				EXIT
			ENDIF
			
			IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), Get_Heist_Corona_Coords_For_Players_Currently_Occupied_Apartment()) > HEIST_CUTSCENE_TRIGGER_m)
				PRINTLN("...KGM MP [ActSelect][Heist][LockHelp]: Player is not in range of Heist Corona location: Clearing 'heists still locked' help text.")
				CLEAR_HELP()
				
				EXIT
			ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	// Help message isn't being displayed. Is a 'still locked' message needed?
	// ...has the player visitied Lester for the 'Intro to Heists' cutscene?
	IF (HAS_LESTER_INTRO_TO_HEISTS_CUTSCENE_BEEN_DONE())
		EXIT
	ENDIF
	
	// Player hasn't yet visited Lester, so Heists aren't yet fully unlocked
	BOOL displayHelpSeeLester = TRUE
	
	// Are Heists unlocked (ie: the player has reached the appropriate rank)?
	IF NOT (IS_FM_TYPE_UNLOCKED(FMMC_TYPE_HEIST_INTRO))
		// ...no, so use the 'still locked' help text
		displayHelpSeeLester = FALSE
	ENDIF
	
	// Is a Help Message required?
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		EXIT
	ENDIF
	
	IF NOT (Get_Current_Heist_Control_Stage() = HEIST_CLIENT_STAGE_NO_HEIST)
	AND NOT (Get_Current_Heist_Control_Stage() = HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY)
		EXIT
	ENDIF
	
	IF NOT (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE))
		EXIT
	ENDIF
	
	IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), Get_Heist_Corona_Coords_For_Players_Currently_Occupied_Apartment()) > (HEIST_CUTSCENE_TRIGGER_m - HEIST_CUTSCENE_TRIGGER_LEEWAY_m))
		EXIT
	ENDIF
	
	// Heist Help message is required
	// Display 'see Lester'?
	IF (displayHelpSeeLester)
		PRINTLN("...KGM MP [ActSelect][Heist][LockHelp]: Player in range of Planning Board: Display Help - go visit Lester to unlock heists.")
		PRINT_HELP("HLOCK_LESTER", HEIST_SEMI_PERMANENT_HELP_TIME_msec)
		
		EXIT
	ENDIF
	
	// KGM 13/3/15 [BUG 2271528] If the player is already at Rank 12, then display a 'wait for Lester phonecall' to cover the small time gap until Lester calls - use a short display timeout
	IF (GET_PLAYER_RANK(PLAYER_ID()) = 12)
		PRINTLN("...KGM MP [ActSelect][Heist][LockHelp]: Player in range of Planning Board: Display Help - wait for Lester call.")
		PRINT_HELP("HLOCK_WAITCALL", 30000)
		
		EXIT
	ENDIF
	
	// Display 'Unlocked at Rank 12'
	PRINTLN("...KGM MP [ActSelect][Heist][LockHelp]: Player in range of Planning Board: Display Help - heists not unlocked.")
	PRINT_HELP("HLOCKED", HEIST_SEMI_PERMANENT_HELP_TIME_msec)

ENDPROC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      Local Heist Strand Maintenance Routines
// ===========================================================================================================

// PURPOSE:	Clear 'on display' variables for Other Player's Corona
PROC Clear_Other_On_Display_Heist_Coronas_Variables()

	PRINTLN("...KGM MP [ActSelect][Heist][HCorona]: Clear On Display variables for the current 'on display' corona")
	
	// Clear the on display variables
	g_sOtherMPHeistCoronas.ohccOnDisplayMatcRegID	= ILLEGAL_AT_COORDS_ID
	g_sOtherMPHeistCoronas.ohccOnDisplayHeistRegID	= ILLEGAL_HEIST_CORONA_REG_ID
				
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Remove Other Player's Corona from Matc
PROC Remove_Other_On_Display_Heist_Coronas_From_Matc()

	PRINTSTRING("...KGM MP [ActSelect][Heist][HCorona]: Remove Heist Corona on display with this MatcRegID: ") PRINTINT(g_sOtherMPHeistCoronas.ohccOnDisplayMatcRegID) PRINTNL()
	
	Mark_MP_Mission_At_Coords_For_Delete(g_sOtherMPHeistCoronas.ohccOnDisplayMatcRegID)
	
	// Clear the on display variables
	Clear_Other_On_Display_Heist_Coronas_Variables()
				
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain other players active Heist coronas in the session 
PROC Remove_Any_Other_Heist_Coronas_Marked_For_Delete()

	// Remove any that are marked for delete
	INT theFrom = 0
	INT theTo = 0
	INT lastEntry = g_sOtherMPHeistCoronas.ohccNumOtherCoronas
	BOOL moveThisEntry = FALSE

	#IF IS_DEBUG_BUILD
	BOOL entryDeleted = FALSE
	#ENDIF
	
	// Keep the array contiguous by shuffling all undeleted entries up the array to cover deleted entries, but the deleted entries may need to do work first
	WHILE (theFrom < lastEntry)
		moveThisEntry = TRUE
		
		IF (g_sOtherMPHeistCoronas.ohccOtherCoronas[theFrom].ohcMarkedForDelete)
			// Found an entry marked for delete.
			#IF IS_DEBUG_BUILD
				PRINTSTRING("...KGM MP [ActSelect][Heist][HCorona]: Other Heist Corona Marked For Delete. Cleaning Up. Details:") PRINTNL()
				Debug_Output_One_Other_Players_Active_Heist_Corona(theFrom)
			#ENDIF
			
			// Delete it on MatC if on display
			// KGM 18/8/14 (BUG 1908052) Otherwise become unreserved for the mission if the player had already accepted an invite to it but it is being deleted before getting setup
			IF (g_sOtherMPHeistCoronas.ohccOnDisplayHeistRegID = g_sOtherMPHeistCoronas.ohccOtherCoronas[theFrom].ohcRegID)
				// Corona already setup on MissionsAtCoords
				Remove_Other_On_Display_Heist_Coronas_From_Matc()
			ELSE
				// If the player is reserved for the mission on the Mission Controller due to accepting an invite but the corona hasn't been setup yet, cancel the reservation
				IF (g_sOtherMPHeistCoronas.ohccOtherCoronas[theFrom].ohcUniqueIDFromInvite != NO_UNIQUE_ID)
					#IF IS_DEBUG_BUILD
						PRINTLN("...KGM MP [ActSelect][Heist][HCorona]: Cancel Reservation due to Invite for this UniqueID: ", g_sOtherMPHeistCoronas.ohccOtherCoronas[theFrom].ohcUniqueIDFromInvite)
					#ENDIF
	
					Broadcast_Cancel_Mission_Reservation_By_Player(g_sOtherMPHeistCoronas.ohccOtherCoronas[theFrom].ohcUniqueIDFromInvite)
					g_sOtherMPHeistCoronas.ohccOtherCoronas[theFrom].ohcUniqueIDFromInvite = NO_UNIQUE_ID
				ENDIF
			ENDIF
			
			// Clear these variables
			Clear_One_Other_Active_Heist_Corona(theFrom)
			
			// There is one less entry in the array since this has been deleted, so don't move this entry
			moveThisEntry = FALSE
			
			#IF IS_DEBUG_BUILD
				entryDeleted = TRUE
			#ENDIF
		ENDIF
		
		// Shuffle the entries
		IF (moveThisEntry)
			IF (theFrom != theTo)
				g_sOtherMPHeistCoronas.ohccOtherCoronas[theTo] = g_sOtherMPHeistCoronas.ohccOtherCoronas[theFrom]
			ENDIF
			
			theTo++
		ENDIF
		
		theFrom++
	ENDWHILE
	
	// Set the new last entry
	g_sOtherMPHeistCoronas.ohccNumOtherCoronas = theTo
	
	// Clear out the excess entries in the array
	WHILE (theTo < lastEntry)
		Clear_One_Other_Active_Heist_Corona(TheTo)
		theTo++
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
		IF (entryDeleted)
			PRINTSTRING("...KGM MP [ActSelect][Heist][HCorona]: Current Array of Other Players Coronas Details After At Least One Corona Details deleted:") PRINTNL()
			Debug_Output_All_Other_Player_Active_Heist_Coronas()
		ENDIF
	#ENDIF

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Register another player's Heist Corona with Matc to display it
//
// INPUT PARAMS:			paramArrayPos			Array Position on the Other Player's Heist Coronas array
// RETURN VALUE:			BOOL					TRUE if the corona registered with Matc, otherwise FALSE
FUNC BOOL Display_Other_Players_Heist_Corona(INT paramArrayPos)

	VECTOR				otherCoronaLocation	= g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcCoords
	MP_MISSION_ID_DATA	thisMissionIdData	= g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcMissionIdData

	IF (ARE_VECTORS_ALMOST_EQUAL(otherCoronaLocation, << 0.0, 0.0, 0.0 >>))
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [ActSelect][Heist][HCorona]: Other Player's corona location is near the origin. Not setting up Heist corona. Marking for delete.")
			Debug_Output_One_Other_Players_Active_Heist_Corona(paramArrayPos)
			SCRIPT_ASSERT("Display_Other_Players_Heist_Corona() - ERROR: Other Player's Corona location retrieved is near the origin. Tell Keith.")
		#ENDIF
		
		Delete_Other_Player_Active_Heist_Corona(g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcRegID)
		RETURN FALSE
	ENDIF
	
	// KGM 25/9/14 [BUG 2051128]: Don't setup another corona in an activity session if there is already one setup at the same location.
	//		NOTE: It will have been setup on initialisation rather than by these routines - the player may be lagging behind on the cutscene.
	IF (NETWORK_IS_ACTIVITY_SESSION())
		IF (g_heistMocap.stage != HMS_STAGE_INACTIVE)
			PRINTLN(".KGM [ActSelect][Heist][HCorona]: Activity Session Active and Player is on a Heist Cutscene. Delaying setup.")
			RETURN FALSE
		ENDIF
		
		IF (Expensive_Is_Vector_In_Any_Corona_Triggering_Range(otherCoronaLocation))
			PRINTLN(".KGM [ActSelect][Heist][HCorona]: Activity Session Active and there is already a corona at this heist location. Delaying setup.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Setup the corona
	g_eMPMissionSource		theSource			= MP_MISSION_SOURCE_MISSION_FLOW_BLIP
	VECTOR					theCoords			= otherCoronaLocation
	g_structMatCBlipMP		theBlipDetails		= Gather_Blip_Details_For_Shared_Mission(thisMissionIdData)
	g_structMatCCoronaMP	theCorona			= Gather_Corona_Details_For_Shared_Mission(thisMissionIdData)
	g_structMatCOptionsMP	theOptions			= Gather_Optional_Flags_For_Shared_Mission(thisMissionIdData)
	INT						atCoordsID			= ILLEGAL_AT_COORDS_ID

	theBlipDetails.theBlipSprite 				= RADAR_TRACE_JEWELRY_HEIST
	theBlipDetails.theBlipName					= "FMMC_GRP_HS"
	theBlipDetails.theBlipPriority				= BLIPPRIORITY_HIGH
	
	// If an Intro Cutscene corona
	IF (g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcForIntroCutscene)
		theOptions.matcoForCutsceneOnly		= TRUE
	ENDIF
	
	// If a Mid-Strand Cutscene corona
	IF (g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcForMidStrandCutscene)
		theOptions.matcoForCutsceneOnly		= TRUE
		theOptions.matcoHeistMidStrandCut	= TRUE
	ENDIF
	
	// If a Mid-Strand Cutscene corona
	IF (g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcForTutorialCutscene)
		theOptions.matcoHeistTutorialCuts	= TRUE
	ENDIF
	
	// KGM 18/6/14: Other Players should not register the Leader's Heist-related mission as Shared Data. If they do then they get added to the Shared Mission's Player Bits
	//				meaning future players will reject the heist because the shared data will pass multiple player bits as set, so impossible to know which Player is Leader.
	// KGM 30/6/14: I'm going to allow other players to share the data again becasue if they don't then the shared data doesn't get copied leading to problems with 'Play Again'.
	//				Instead, I'm going to have to record the player index of the first player to register the mission in order to get the 'Leader'
	//theOptions.matcoShareCloudData = FALSE
	
	// KGM 30/6/14: This data is passed to here from the Shared Mission routines as an intermediate step to Matc, so treat this as a Shared Mission
	theOptions.matcoSharedMission = TRUE
	
	// KGM 6/8/14: Allow players to be sucked into Leader's Heist if they are in the planning room. Also ensure Crew Heist coronas get deleted after focus and are invisible.
	theCorona.triggerRadius = 4.5
	theOptions.matcoDeleteAfterFocus = TRUE
	theOptions.matcoDisplayNoCorona = TRUE

	atCoordsID = Register_MP_Mission_At_Coords(theSource, theCoords, theBlipDetails, theCorona, theOptions, thisMissionIdData)

	IF (atCoordsID = ILLEGAL_AT_COORDS_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT(".KGM [ActSelect][Heist][HCorona]: ") NET_PRINT_TIME() NET_PRINT(" Display_Other_Players_Heist_Corona(): FAILED TO REGISTER WITH MISSIONS AT COORDS") NET_NL()
		#ENDIF
		
		// NOT GOING TO MARK IT FOR DELETE, BUT I MAY NEED TO
		RETURN FALSE
	ENDIF
	
	// KGM 26/9/14 [BUG 2038714]: JIP from the planning board (must be by using the Xbox Blade) into another heist corona caused this players apartment details to be sent as transition data.
	//				I'll fix this by clearing the variable here that us used to decide if apartment details should be transition broadcast or not.
	PRINTLN(".KGM [ActSelect][Heist][HCorona]: Setup another player's Heist corona. Clear out the apartment transition data that gets set when in own corona.")
	Clear_Heist_Data_Stored_For_Cross_Session_Transmission()
	
	// KGM 1/9/14: Block the corona immediately if an estalishing shot is already a requirement, otherwise it'll get blocked in the Joblist when the establishing shot becomes a requirement
	IF NOT HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
	AND GET_HEIST_PROPERTY_CAM_STAGE() != HEIST_PROPERTY_HOLD_STAGE
		IF (IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED())
			PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Display_Other_Players_Heist_Corona - SAME SESSION BUT THROUGH JIP - Not blocking remote heist corona init.")
		ELSE
			// If an establishing shot is required then the corona needs to be blocked immediately
			IF (g_sJoblistWarpMP.jlwDoEstablishingShot)
				PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Display_Other_Players_Heist_Corona - SAME SESSION - Immediate Heist Corona Block required to allow establishing shot")
				Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_By_Filename(thisMissionIdData.idCloudFilename, FALSE, TRUE)
				g_HeistSharedClient.tlHeistContentID = thisMissionIdData.idCloudFilename
			    // Let MissionsAtCoords know that the mission is being triggered by Invite
				Set_MissionsAtCoords_Mission_Has_Had_Invite_Accepted_By_Filename(thisMissionIdData.idCloudFilename)
			ELSE
				PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Display_Other_Players_Heist_Corona - SAME SESSION - Establishing Shot not immediately required. Not blocking corona")
			ENDIF
		ENDIF
	ELSE
		PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Display_Other_Players_Heist_Corona - X SESSION or TRANSITION - Not blocking remote heist corona init.")
	ENDIF
	
	// Store the corona RegID
	g_sOtherMPHeistCoronas.ohccOnDisplayHeistRegID	= g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcRegID
	g_sOtherMPHeistCoronas.ohccOnDisplayMatcRegID	= atCoordsID

	#IF IS_DEBUG_BUILD
		NET_PRINT(".KGM [ActSelect][Heist][HCorona]: ")
		NET_PRINT_TIME()
		NET_PRINT(" Display_Other_Players_Heist_Corona(): Successfully registered with Missions At Coords RegID: ")
		NET_PRINT_INT(atCoordsID)
		NET_NL()
		Debug_Output_One_Other_Players_Active_Heist_Corona(paramArrayPos)
	#ENDIF
	
	// Ensure the Heist is unlocked whatever the player's rank
	Temporarily_Unlock_MissionsAtCoords_Mission_By_Filename(thisMissionIdData.idCloudFilename, TRUE)
	
	// KGM 25/7/14: Already in an Activity Session, so this must be the crew member joining the first Prep Mission while still in the cutscene Activity Session.
	//				Need to Update the Loaded Transitions Session FileName from Cutscene missionID to Prep MissionID (or Prep mission will fail to become Focus Mission)
	IF (NETWORK_IS_ACTIVITY_SESSION())
		PRINTLN(".KGM [ActSelect][Heist][HCorona]: Already in Activity Session. This must be the first Prep Mission after viewing an Intro or Mid-Strand Cutscene.")
		PRINTLN(".KGM [ActSelect][Heist][HCorona]: ...Update the Loaded Transition Session Filename to ", thisMissionIdData.idCloudFilename)
		SET_MY_TRANSITION_SESSION_CONTENT_ID(thisMissionIdData.idCloudFilename)
	ENDIF
	
//	IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), theCoords) < 5.0)
//		VECTOR coronaLocation = theCoords
//		coronaLocation.z += 0.7
//		GET_GROUND_Z_FOR_3D_COORD(coronaLocation, coronaLocation.z)
//		SET_ENTITY_COORDS(PLAYER_PED_ID(), coronaLocation)
//		PRINTLN(".KGM [ActSelect][Heist][HCorona]: Moving Player to centre of apartment owner's corona: ", coronaLocation)
//	ENDIF

	// The other player's Heist corona has been registered, so the UniqueID for Invite Acceptance can be cleared
	IF (g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcUniqueIDFromInvite != NO_UNIQUE_ID)
		g_sOtherMPHeistCoronas.ohccOtherCoronas[paramArrayPos].ohcUniqueIDFromInvite = NO_UNIQUE_ID
		
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [ActSelect][Heist][HCorona]: After Matc Registration, Clear UniqueID stored by Accepting an Invite into the Other Player's Heist. Updated array:")
			Debug_Output_All_Other_Player_Active_Heist_Coronas()
		#ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	There is another player's corona on display, so check if it should remain on display
PROC Maintain_Other_Heist_Corona_On_Display()
	
	// Is player still in a player's property?
	IF NOT (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE))
		#IF IS_DEBUG_BUILD
			NET_PRINT(".KGM [ActSelect][Heist][HCorona]: ")
			NET_PRINT_TIME()
			NET_PRINT(" Maintain_Other_Heist_Corona_On_Display(): CLEAR OTHER CORONA: No longer in another player's apartment")
			NET_NL()
		#ENDIF
		
		Remove_Other_On_Display_Heist_Coronas_From_Matc()
		EXIT
	ENDIF
	
	// Player is still in another player's apartment, so check if it is the player's apartment containing the current on-display Corona - if not, clean up
	INT tempLoop = 0
	INT myApartment = Get_Players_Current_Apartment()
	INT otherApartment = NO_OWNED_SAFEHOUSE
	PLAYER_INDEX otherPlayerID = INVALID_PLAYER_INDEX()
	
	REPEAT g_sOtherMPHeistCoronas.ohccNumOtherCoronas tempLoop
		// Is this the corona on display?
		IF (g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcRegID = g_sOtherMPHeistCoronas.ohccOnDisplayHeistRegID)
			otherPlayerID = g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcPlayerID
			
			IF NOT (IS_NET_PLAYER_OK(otherPlayerID, FALSE))
				#IF IS_DEBUG_BUILD
					NET_PRINT(".KGM [ActSelect][Heist][HCorona]: ")
					NET_PRINT_TIME()
					NET_PRINT(" Maintain_Other_Heist_Corona_On_Display(): CLEAR OTHER CORONA: Other Player is not OK")
					NET_NL()
				#ENDIF
		
				Remove_Other_On_Display_Heist_Coronas_From_Matc()
				EXIT
			ENDIF
			
			otherApartment = GlobalplayerBD_FM[NATIVE_TO_INT(otherPlayerID)].propertyDetails.iCurrentlyInsideProperty
			IF (otherApartment = NO_OWNED_SAFEHOUSE)
			OR (otherApartment < 0)
				#IF IS_DEBUG_BUILD
					NET_PRINT(".KGM [ActSelect][Heist][HCorona]: ")
					NET_PRINT_TIME()
					NET_PRINT(" Maintain_Other_Heist_Corona_On_Display(): CLEAR OTHER CORONA: Other Player is not in an apartment")
					NET_NL()
				#ENDIF
		
				Remove_Other_On_Display_Heist_Coronas_From_Matc()
				EXIT
			ENDIF
			
			IF (myApartment != otherApartment)
				#IF IS_DEBUG_BUILD
					NET_PRINT(".KGM [ActSelect][Heist][HCorona]: ")
					NET_PRINT_TIME()
					NET_PRINT(" Maintain_Other_Heist_Corona_On_Display(): CLEAR OTHER CORONA: I am not in the Other Player's apartment")
					NET_NL()
				#ENDIF
		
				Remove_Other_On_Display_Heist_Coronas_From_Matc()
				EXIT
			ENDIF
				
			// Players are in the same apartment, so check if they are in the same instance
			IF NOT (ARE_PLAYERS_IN_SAME_PROPERTY(PLAYER_ID(), otherPlayerID, TRUE))
				#IF IS_DEBUG_BUILD
					NET_PRINT(".KGM [ActSelect][Heist][HCorona]: ")
					NET_PRINT_TIME()
					NET_PRINT(" Maintain_Other_Heist_Corona_On_Display(): CLEAR OTHER CORONA: I am not in the same instance as the Other Player's apartment")
					NET_NL()
				#ENDIF
		
				Remove_Other_On_Display_Heist_Coronas_From_Matc()
				EXIT
			ENDIF
			
			// Finally, if the player has accepted an invite into this other player's corona, ensure the corona still exists. If not, clean up the variables so it can be re-added.
			IF (g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcUniqueIDFromInvite != NO_UNIQUE_ID)
				// ...an invite has been accepted into this corona so check if it still exists
				IF NOT (Check_If_MissionsAtCoords_Mission_With_This_RegID_Still_Exists(g_sOtherMPHeistCoronas.ohccOnDisplayMatcRegID))
					// ...corona is no longer registerd with MissionsAtCoords so player may have previously walked out of it, clean up the variables to allow it to get setup again
					#IF IS_DEBUG_BUILD
						PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player has received an invite to an 'on display' Other Player corona that no longer exists")
						PRINTLN(".KGM [ActSelect][Heist][HCorona]: ...clear variables to allow corona to get setup again if required. Matc REGID: ", g_sOtherMPHeistCoronas.ohccOnDisplayMatcRegID)
					#ENDIF
		
					Clear_Other_On_Display_Heist_Coronas_Variables()
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the display statuses of Other Players' Heist Coronas
PROC Maintain_Display_Status_Of_Other_Heist_Coronas()

	// KGM 1/9/14: I think the corona should only now get unblocked in the Joblist under control of the establishing shot
//	IF (GET_HEIST_CORONA_BLOCKED_STATE() = HEIST_INTRO_STATE_BLOCKED)
//		PRINTLN("[AMEC][HEIST_ANIMS] - Maintain_Display_Status_Of_Other_Heist_Coronas - HEIST_INTRO_STATE_BLOCKED, maintain intro anim.")
//	ELIF (GET_HEIST_CORONA_BLOCKED_STATE() = HEIST_INTRO_STATE_UNBLOCKED)
//		PRINTLN("[AMEC][HEIST_ANIMS] - Maintain_Display_Status_Of_Other_Heist_Coronas - HEIST_INTRO_STATE_UNBLOCKED, restore corona using contentID: ", g_HeistSharedClient.tlHeistContentID)
//		Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_By_Filename(g_HeistSharedClient.tlHeistContentID, TRUE)
//		SET_HEIST_CORONA_BLOCKED_STATE(HEIST_INTRO_STATE_NONE)
//		g_HeistSharedClient.tlHeistContentID = ""
//	ENDIF 

	// KGM 23/1/15 [BUG 2162925]: If not on display, cleanup Other Coronas if the player is not OK
	INT tempLoop = 0
	PLAYER_INDEX otherPlayerID = INVALID_PLAYER_INDEX()
	
	REPEAT g_sOtherMPHeistCoronas.ohccNumOtherCoronas tempLoop
		otherPlayerID = g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcPlayerID
		IF NOT (IS_NET_PLAYER_OK(otherPlayerID, FALSE))
			IF (g_sOtherMPHeistCoronas.ohccOnDisplayHeistRegID != g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcRegID)
				// Other player is no longer OK and the other's player's corona isn't the one on display, so clean it up
				PRINTLN("...KGM MP [Heist][HCorona]: Player is not OK so mark Other Corona for delete. Heist RegID:", g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcRegID)
				Delete_Other_Player_Active_Heist_Corona(g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcRegID)
			ENDIF
		ENDIF
	ENDREPEAT

	// Is there another player's Heist Corona already on display?
	IF (g_sOtherMPHeistCoronas.ohccOnDisplayHeistRegID != ILLEGAL_HEIST_CORONA_REG_ID)
		// ...yes, so maintain it
		Maintain_Other_Heist_Corona_On_Display()
		
		EXIT
	ENDIF
	
	// Another player's Heist corona is not on display, so check if one should be displayed
	// Are there any stored details of other coronas?
	IF (g_sOtherMPHeistCoronas.ohccNumOtherCoronas = 0)
		EXIT
	ENDIF
	
	// Is my own heist corona on display?
	IF (g_sLocalMPHeistControl.lhcMyCorona.mhcMatcID != ILLEGAL_AT_COORDS_ID)
		EXIT
	ENDIF
	
	// Is this player in a player's property?
	IF NOT (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE))
		EXIT
	ENDIF
	
	// Is it my own property?
	IF (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE))
		EXIT
	ENDIF
	
	// Player is in another player's property and there are active Heist coronas in the session, so need to check if the player is in a property with an active heist corona
	INT myApartment = Get_Players_Current_Apartment()
	INT otherApartment = NO_OWNED_SAFEHOUSE
	
	tempLoop = 0
	otherPlayerID = INVALID_PLAYER_INDEX()
	
	REPEAT g_sOtherMPHeistCoronas.ohccNumOtherCoronas tempLoop
		otherPlayerID = g_sOtherMPHeistCoronas.ohccOtherCoronas[tempLoop].ohcPlayerID
		IF (IS_NET_PLAYER_OK(otherPlayerID, FALSE))
			// KGM 4/9/14: Partial fix for BUG 2018528 to ensure the 'Other' player isn't the local player. Ideally this would never happen.
			IF NOT (otherPlayerID = PLAYER_ID())
				otherApartment = GlobalplayerBD_FM[NATIVE_TO_INT(otherPlayerID)].propertyDetails.iCurrentlyInsideProperty
				IF (otherApartment != NO_OWNED_SAFEHOUSE)
				AND (otherApartment > 0)
					IF (myApartment = otherApartment)
						// Players are in the same apartment, so check if they are in the same instance
						IF (ARE_PLAYERS_IN_SAME_PROPERTY(PLAYER_ID(), otherPlayerID, TRUE))
							// KGM 22/6/15 [BUG 2286454]: Final Check: Ensure the aparment belongs to the other player
							IF (Does_This_Apartment_Belong_To_Other_Player(otherPlayerID))
								IF (Display_Other_Players_Heist_Corona(tempLoop))
									EXIT
								ENDIF
							ELSE
								NET_PRINT(".KGM [Heist][HCorona]: Don't setup Heist corona. Apartment does not belong to the player whose corona I'm trying to join.")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain other players active Heist coronas in the session 
PROC Maintain_MP_Selector_Group_Other_Heist_Coronas()

	Remove_Any_Other_Heist_Coronas_Marked_For_Delete()
	Maintain_Display_Status_Of_Other_Heist_Coronas()
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Setup a Heist Corona from stored details
FUNC BOOL Setup_My_Heist_Corona_From_Stored_Details()

	INT						slotLoop			= 0
	BOOL					foundDetails		= FALSE
	MP_MISSION_ID_DATA		theMissionIdData
	VECTOR					heistCoronaLocation	= Get_Heist_Corona_Coords_For_Players_Currently_Occupied_Apartment()
	
	IF (ARE_VECTORS_ALMOST_EQUAL(heistCoronaLocation, << 0.0, 0.0, 0.0 >>))
		PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player's corona location is near the origin. Not setting up Heist corona.")
		SCRIPT_ASSERT("Setup_My_Heist_Corona_From_Stored_Details() - ERROR: Corona location retrieved is near the origin. Tell Keith.")
			
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		Debug_Output_Heist_Corona_Details()
	#ENDIF
	
	// For Heist Finale missions, grab the details from the Local Heist data
	IF (g_sLocalMPHeistControl.lhcMyCorona.mhcIsFinale)
		PRINTLN(".KGM [ActSelect][Heist][HCorona]: ...searching for Heist Finale ContentID on Local Heist Strand array: ", g_sLocalMPHeistControl.lhcMyCorona.mhcContentID)
		
		foundDetails = FALSE
		
		REPEAT g_numLocalMPHeists slotLoop
			IF NOT (foundDetails)
				IF (ARE_STRINGS_EQUAL(g_sLocalMPHeistControl.lhcMyCorona.mhcContentID, g_sLocalMPHeists[slotLoop].lhsMissionIdData.idCloudFilename))
					theMissionIdData = g_sLocalMPHeists[slotLoop].lhsMissionIdData
					foundDetails = TRUE
					
					PRINTLN(".KGM [ActSelect][Heist][HCorona]: ......FOUND Heist Finale ContentID: ", g_sLocalMPHeistControl.lhcMyCorona.mhcContentID, " at heist Strand array location: ", slotLoop)
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF NOT (foundDetails)
			PRINTLN(".KGM [ActSelect][Heist][HCorona]: Failed to find Heist Finale ContentID in Heist Strands array: ", g_sLocalMPHeistControl.lhcMyCorona.mhcContentID)
			SCRIPT_ASSERT("Setup_My_Heist_Corona_From_Stored_Details() - ERROR: Failed to find ContentID for Heist Finale. Tell Keith.")
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	// For Heist Planning missions, grab the details from the Rockstar Created array
	IF NOT (g_sLocalMPHeistControl.lhcMyCorona.mhcIsFinale)
		//KGM 10/8/14: Search in range 500 - 518 - these Planning Header details should have been downloaded
		INT	thisArrayPos = FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD
		INT endArrayPos	 = FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED
		INT hashContentID = GET_HASH_KEY(g_sLocalMPHeistControl.lhcMyCorona.mhcContentID)
		
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [ActSelect][Heist][HCorona]: Current Contents of Rockstar Created array locations ", thisArrayPos, " - ", (endArrayPos-1))
			Debug_Output_Rockstar_Created_500_PLUS()
			PRINTLN(".KGM [ActSelect][Heist][HCorona]: ...searching for Heist Planning ContentID (", g_sLocalMPHeistControl.lhcMyCorona.mhcContentID, " - ", hashContentID, ") in range ", thisArrayPos, " - ", endArrayPos)
		#ENDIF
		
		foundDetails = FALSE
		
		BOOL isTutorialHeistCutscene = g_sLocalMPHeistControl.lhcMyCorona.mhcIsTutorialHeist
		
		WHILE ((thisArrayPos < endArrayPos) AND NOT (foundDetails))
			IF (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
				IF (isTutorialHeistCutscene)
					// Tutorial Cutscene is setup by passing in the rootContentID for the Prep Mission - this should get updated below to the latest contentID
					IF (hashContentID = g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[thisArrayPos].iRootContentIdHash)
						foundDetails = TRUE
					ENDIF
				ELSE
					// Other Prep Missions are triggered using the contentID for the Prep Mission
					IF (hashContentID = g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[thisArrayPos].iHash)
						foundDetails = TRUE
					ENDIF
				ENDIF
				
				IF (foundDetails)
					// Ensure this is a Heist Planning mission
					IF NOT (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iType = FMMC_TYPE_MISSION)
					OR NOT (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iSubType = FMMC_MISSION_TYPE_PLANNING)
						#IF IS_DEBUG_BUILD
							IF (isTutorialHeistCutscene)
								PRINTLN(".KGM [ActSelect][Heist][HCorona]: Found Heist Planning RootContentID: ", g_sLocalMPHeistControl.lhcMyCorona.mhcContentID, "BUT Type or Subtype is wrong. Type: ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iType, " Subtype: ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iSubType)
							ELSE
								PRINTLN(".KGM [ActSelect][Heist][HCorona]: Found Heist Planning ContentID: ", g_sLocalMPHeistControl.lhcMyCorona.mhcContentID, "BUT Type or Subtype is wrong. Type: ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iType, " Subtype: ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iSubType)
							ENDIF
							SCRIPT_ASSERT("Setup_My_Heist_Corona_From_Stored_Details() - ERROR: Cloud Data Heist Planning Type or Subtype is wrong. Tell Keith.")
						#ENDIF
						
						RETURN FALSE
					ENDIF
					
					Clear_MP_MISSION_ID_DATA_Struct(theMissionIdData)
					theMissionIdData.idMission			= Convert_FM_Mission_Type_To_MissionID(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iType)
					theMissionIdData.idVariation		= thisArrayPos
					theMissionIdData.idCreator			= FMMC_ROCKSTAR_CREATOR_ID
					theMissionIdData.idCloudFilename	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].tlName
					
					// If this is the Tutorial Heist Cutscene, then the local contentID Hash is actually a rootContentIDHash, so update it to be the contentID Hash now that the mission is found
					IF (isTutorialHeistCutscene)
						PRINTLN(".KGM [ActSelect][Heist][HCorona]: Updating local ContentID for Tutorial Cutscene. From RCID [", g_sLocalMPHeistControl.lhcMyCorona.mhcContentID, "] to ContentID: ", theMissionIdData.idCloudFilename)
						g_sLocalMPHeistControl.lhcMyCorona.mhcContentID = theMissionIdData.idCloudFilename
					ENDIF
					
					foundDetails = TRUE
					
					PRINTLN(".KGM [ActSelect][Heist][HCorona]: ......FOUND Heist Planning ContentID: ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].tlName, " at R* Created array position: ", thisArrayPos)
				ENDIF
			ENDIF
				
			thisArrayPos++
		ENDWHILE
		
//// KGM 10/8/14: TEMP FALLBACK - SEARCH FOR THE MISSIONS IN THE 0-500 RANGE UNTIL ALL PREP MISSIONS ARE PROPERLY TAGGED AS 'PLANNING'
//// TO BE REMOVED
//		IF NOT (foundDetails)
//			PRINTLN(".KGM [ActSelect][Heist][HCorona]: Failed to find Heist Planning ContentID in Rockstar Created array locations 500-518: ", g_sLocalMPHeistControl.lhcMyCorona.mhcContentID)
//			PRINTLN(".KGM [ActSelect][Heist][HCorona]: TEMPORARY FALLBACK - TRY LOCATIONS 0-500")
//	
//			thisArrayPos = 0
//			endArrayPos	 = FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD
//			
//			PRINTLN(".KGM [ActSelect][Heist][HCorona]: ...searching for Heist Planning ContentID (", g_sLocalMPHeistControl.lhcMyCorona.mhcContentID, " - ", hashContentID, ") in range ", thisArrayPos, " - ", endArrayPos)
//			
//			foundDetails = FALSE
//			
//			WHILE ((thisArrayPos < endArrayPos) AND NOT (foundDetails))
//				IF (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
//					IF (hashContentID = g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[thisArrayPos].iHash)
//						// Ensure this is a Heist Planning mission
//						IF NOT (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iType = FMMC_TYPE_MISSION)
//						OR NOT (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iSubType = FMMC_MISSION_TYPE_PLANNING)
//							PRINTLN(".KGM [ActSelect][Heist][HCorona]: Found Heist Planning ContentID in range 0-500: ", g_sLocalMPHeistControl.lhcMyCorona.mhcContentID, " BUT Type or Subtype is wrong. Type: ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iType, " Subtype: ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iSubType)
//							SCRIPT_ASSERT("Setup_My_Heist_Corona_From_Stored_Details() - ERROR: Cloud Data Heist Planning Type or Subtype is wrong. Tell Keith.")
//							
//							RETURN FALSE
//						ENDIF
//						
//						Clear_MP_MISSION_ID_DATA_Struct(theMissionIdData)
//						theMissionIdData.idMission			= Convert_FM_Mission_Type_To_MissionID(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iType)
//						theMissionIdData.idVariation		= thisArrayPos
//						theMissionIdData.idCreator			= FMMC_ROCKSTAR_CREATOR_ID
//						theMissionIdData.idCloudFilename	= g_sLocalMPHeistControl.lhcMyCorona.mhcContentID
//						
//						foundDetails = TRUE
//						
//						PRINTLN(".KGM [ActSelect][Heist][HCorona]: ......FOUND Heist Planning ContentID: ", g_sLocalMPHeistControl.lhcMyCorona.mhcContentID, " at R* Created array TEMP FALLBACK position: ", thisArrayPos)
//					ENDIF
//				ENDIF
//					
//				thisArrayPos++
//			ENDWHILE
//		ENDIF
//// END: TO BE REMOVED
//// KGM 10/8/14: END: TEMP FALLBACK - SEARCH FOR THE MISSIONS IN THE 0-500 RANGE UNTIL ALL PREP MISSIONS ARE PROPERLY TAGGED AS 'PLANNING'
		
		IF NOT (foundDetails)
			PRINTLN(".KGM [ActSelect][Heist][HCorona]: Failed to find Heist Planning ContentID in Rockstar Created array locatiosn 500-518: ", g_sLocalMPHeistControl.lhcMyCorona.mhcContentID)
			SCRIPT_ASSERT("Setup_My_Heist_Corona_From_Stored_Details() - ERROR: Failed to find ContentID for Heist Planning. Tell Keith.")
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Setup the corona
	g_eMPMissionSource		theSource			= MP_MISSION_SOURCE_MISSION_FLOW_BLIP
	VECTOR					theCoords			= heistCoronaLocation
	g_structMatCBlipMP		theBlipDetails		= Gather_Blip_Details_For_Shared_Mission(theMissionIdData)
	g_structMatCCoronaMP	theCorona			= Gather_Corona_Details_For_Shared_Mission(theMissionIdData)
	g_structMatCOptionsMP	theOptions			= Gather_Optional_Flags_For_Shared_Mission(theMissionIdData)
	INT						atCoordsID			= ILLEGAL_AT_COORDS_ID

	theBlipDetails.theBlipSprite 				= RADAR_TRACE_JEWELRY_HEIST
	theBlipDetails.theBlipName					= "FMMC_GRP_HS"
	theBlipDetails.theBlipPriority				= BLIPPRIORITY_HIGH
	
	// Hide every corona for all heists. AMEC.
	theOptions.matcoDisplayNoCorona     = TRUE
    theOptions.matcoDeleteAfterFocus    = TRUE
	  
    // If an Intro Cutscene corona
    IF (g_sLocalMPHeistControl.lhcMyCorona.mhcIsIntroCutscene)
        theOptions.matcoForCutsceneOnly     = TRUE
    ENDIF
      
    // If a Mid-Strand Cutscene corona
    IF (g_sLocalMPHeistControl.lhcMyCorona.mhcIsMidStrandCutscene)
        theOptions.matcoForCutsceneOnly     = TRUE
		theOptions.matcoHeistMidStrandCut	= TRUE
    ENDIF
    
	// As well as being an Intro Cutscene, it may also be a cutscene for the Tutorial Heist
	IF (g_sLocalMPHeistControl.lhcMyCorona.mhcIsTutorialHeist)
		theOptions.matcoHeistTutorialCuts   = TRUE
	ENDIF
	
	// BUG 1934522: The Leader should always host their own instance of the mission
	theOptions.matcoHostOwnInstance		= TRUE

	atCoordsID = Register_MP_Mission_At_Coords(theSource, theCoords, theBlipDetails, theCorona, theOptions, theMissionIdData)

	IF (atCoordsID = ILLEGAL_AT_COORDS_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT(".KGM [ActSelect][Heist][HCorona]: ") NET_PRINT_TIME() NET_PRINT(" Setup_My_Heist_Corona_From_ContentID(): FAILED TO REGISTER WITH MISSIONS AT COORDS") NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Store the corona RegID
	g_sLocalMPHeistControl.lhcMyCorona.mhcMatcID				= atCoordsID
	g_sLocalMPHeistControl.lhcMyCorona.mhcInCorona				= FALSE
	g_sLocalMPHeistControl.lhcMyCorona.mhcAlreadyTransitioned	= FALSE

	#IF IS_DEBUG_BUILD
		NET_PRINT(".KGM [ActSelect][Heist][HCorona]: ")
		NET_PRINT_TIME()
		NET_PRINT(" Setup_My_Heist_Corona_From_Stored_Details(): Successfully registered with Missions At Coords RegID: ")
		NET_PRINT_INT(atCoordsID)
		NET_NL()
	#ENDIF
	
	// Ensure the Heist is unlocked whatever the player's rank
	Temporarily_Unlock_MissionsAtCoords_Mission_By_Filename(theMissionIdData.idCloudFilename, TRUE)
	
	// KGM 18/1/15 [BUG 2201572]: Make the mission Invite Accepted - his is to avoid a block due to the player somehow becoming Wanted)
	Set_MissionsAtCoords_Mission_Has_Had_Invite_Accepted_By_Filename(theMissionIdData.idCloudFilename)
	
	// KGM 29/7/15 [BUG 2448306]: Being on a challenge shouldn't block Heist Launching
	Unblock_One_MissionsAtCoords_Mission(theMissionIdData.idCloudFilename)
	
	// KGM 25/7/14: Already in an Activity Session, so this must be the player choosing the first Prep Mission while still in the cutscene Activity Session.
	//				Need to Update the Loaded Transitions Session FileName from Cutscene missionID to Prep MissionID (or Prep mission will fail to become Focus Mission)
	IF (NETWORK_IS_ACTIVITY_SESSION())
		PRINTLN(".KGM [ActSelect][Heist][HCorona]: Already in Activity Session. This must be the first Prep Mission after viewing Intro Cutscene. Update the Loaded Transition Session Filename to ", g_sLocalMPHeistControl.lhcMyCorona.mhcContentID)
		SET_MY_TRANSITION_SESSION_CONTENT_ID(g_sLocalMPHeistControl.lhcMyCorona.mhcContentID)
	ENDIF
	
	RETURN TRUE

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Remove my stored corona if on display
// NOTES:	This WON'T remove the local corona details
PROC Remove_My_Stored_Corona_If_On_Display()

	IF (g_sLocalMPHeistControl.lhcMyCorona.mhcMatcID = ILLEGAL_AT_COORDS_ID)
		EXIT
	ENDIF
	
	// Clean it up
	#IF IS_DEBUG_BUILD
		NET_PRINT(".KGM [ActSelect][Heist][HCorona]: ")
		NET_PRINT_TIME()
		NET_PRINT(" Deleting Corona on Missions At Coords with this RegID: ")
		NET_PRINT_INT(g_sLocalMPHeistControl.lhcMyCorona.mhcMatcID)
		NET_NL()
	#ENDIF
	
	Mark_MP_Mission_At_Coords_For_Delete(g_sLocalMPHeistControl.lhcMyCorona.mhcMatcID)
	
	g_sLocalMPHeistControl.lhcMyCorona.mhcMatcID				= ILLEGAL_AT_COORDS_ID
	g_sLocalMPHeistControl.lhcMyCorona.mhcInCorona				= FALSE
	g_sLocalMPHeistControl.lhcMyCorona.mhcAlreadyTransitioned	= FALSE
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Remove my stored corona if on display and cleanup the local variables
// NOTES:	This WILL remove the local corona details
PROC Delete_My_Stored_Corona_If_Available()

	IF NOT (g_sLocalMPHeistControl.lhcMyCorona.mhcAvailable)
		EXIT
	ENDIF

	PRINTLN(".KGM [ActSelect][Heist][HCorona]: Delete my stored corona if on display and clear the details")

	Remove_My_Stored_Corona_If_On_Display()
	Clear_My_Stored_Corona_Details()
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Based on an extract from SETUP_CORONA_CAMERA()
PROC Position_Heist_Camera_On_Starting_Strand()
		
	VECTOR vHeistVec
	VECTOR vCameraPos
	VECTOR vCameraRot
	
	// Heist Camera Location
	GET_PLAYER_PROPERTY_HEIST_LOCATION(vHeistVec, MP_PROP_ELEMENT_HEIST_CAM_POS_OVERVIEW, Get_Players_Current_Apartment())
	
	IF (IS_VECTOR_ZERO(vHeistVec))
		PRINTLN(".KGM [ActSelect][Heist][HCorona]: Heist Apartment Camera Location was the origin. Quitting Camera Positioning.")
		EXIT
	ENDIF
	
	vCameraPos = vHeistVec
	
	// Heist Camera Rotation
	GET_PLAYER_PROPERTY_HEIST_ROTATION(vHeistVec, MP_PROP_ELEMENT_HEIST_CAM_POS_OVERVIEW, Get_Players_Current_Apartment())
	
	vCameraRot = vHeistVec
	 
	PRINTLN(".KGM [ActSelect][Heist][HCorona]: Set camera position to heist board: ", vCameraPos)
	PRINTLN(".KGM [ActSelect][Heist][HCorona]: Set camera rotation to heist board: ", vCameraRot)

	// Use the Transition session cam, so it gets auto-cleared up when the corona takes over
	IF NOT DOES_CAM_EXIST(g_sTransitionSessionData.ciCam)
		g_sTransitionSessionData.ciCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
	ENDIF
		
	//Set the camera active
	SET_CAM_ACTIVE(g_sTransitionSessionData.ciCam, TRUE)
	SET_CAM_COORD(g_sTransitionSessionData.ciCam, vCameraPos)
	SET_CAM_ROT(g_sTransitionSessionData.ciCam,  vCameraRot)
	SET_CAM_FOV(g_sTransitionSessionData.ciCam, 40.0)
	
	RENDER_SCRIPT_CAMS(TRUE, FALSE)

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain Heist Coronas
PROC Maintain_MP_Selector_Group_My_Heist_Corona()

	// If there isn't a corona available, then nothing to do
	IF NOT (g_sLocalMPHeistControl.lhcMyCorona.mhcAvailable)
		EXIT
	ENDIF
	
	// Is the player in one of their own apartments?
	BOOL playerInOwnPropertySuitableForHeists	= Is_Players_Currently_Occupied_Own_Apartment_Suitable_For_Heists()
	BOOL playerHasFocusMissionOrIsInCorona		= FALSE
	BOOL playerIsTransitionSessionRestarting	= IS_TRANSITION_SESSION_RESTARTING()
	
	IF Is_There_A_MissionsAtCoords_Focus_Mission()
	OR IS_PLAYER_IN_CORONA()
		playerHasFocusMissionOrIsInCorona = TRUE
	ENDIF
	
	// Deal with the player being in their own property
	IF (playerInOwnPropertySuitableForHeists)
		// Setup the corona if not already setup and if another player's corona isn't setup
		IF (g_sLocalMPHeistControl.lhcMyCorona.mhcMatcID = ILLEGAL_AT_COORDS_ID)
			IF (g_sOtherMPHeistCoronas.ohccOnDisplayHeistRegID = ILLEGAL_HEIST_CORONA_REG_ID)
				IF NOT (Is_Cloud_Loaded_Mission_Data_Being_Refreshed())
					IF NOT (Setup_My_Heist_Corona_From_Stored_Details())
						PRINTLN(".KGM [ActSelect][Heist][HCorona]: Failed to setup Corona, so killing my corona details")
						Clear_My_Stored_Corona_Details()
						SET_HEIST_SUPPRESS_HUD_FOR_PLANNING(FALSE)
						SET_HEIST_CORONA_FROM_PLANNING_BOARD(FALSE)
						CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimFirst)
						CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimSecond)
						CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimThird, DEFAULT, DEFAULT, TRUE)
						STOP_ALL_CORONA_FX_IN()
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					ENDIF
				ELSE
					PRINTLN(".KGM [ActSelect][Heist][HCorona]: Delay setting up my corona. Cloud Refresh is in progress.")
				ENDIF
			ELSE
				PRINTLN(".KGM [ActSelect][Heist][HCorona]: Delay setting up my corona. Another player's corona is setup.")
			ENDIF
			
			EXIT
		ENDIF
		
		// Corona is setup, so check if the player has walked into it
		IF (playerHasFocusMissionOrIsInCorona)
			// ...player has a focus mission or is in a corona, so check if player was already known to be in the heist corona
			IF NOT (g_sLocalMPHeistControl.lhcMyCorona.mhcInCorona)
				// ...player wasn't known to be in heist corona, so check if the player is in heist now
				IF (Is_This_Missions_At_Coords_RegID_The_Focus_Mission(g_sLocalMPHeistControl.lhcMyCorona.mhcMatcID))
					// ...player is now in the heist corona
					PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player is now in their own Heist Corona")
					g_sLocalMPHeistControl.lhcMyCorona.mhcInCorona				= TRUE
					g_sLocalMPHeistControl.lhcMyCorona.mhcAlreadyTransitioned	= FALSE
					
					// KGM 24/7/14: The player can already be in an Activity Session at this stage if they've just chosen a Heist Prep mission from the board just after seeing an Intro Cutscene.
					//				This is an optimisation where the player no longer transitions back to Freemode after the cutscene, but immediately chooses a Prep Mission.
					IF (NETWORK_IS_ACTIVITY_SESSION())
						g_sLocalMPHeistControl.lhcMyCorona.mhcAlreadyTransitioned	= TRUE
						PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player is already in an Activity Session when setting up a new Heist Corona - setting 'Already Transitioned' flag.")
					ENDIF
					
					// ...store this info for cross-session transmission to other players
					Store_Heist_Data_Ready_For_Cross_Session_Transmission(Get_Players_Current_Apartment(), g_sLocalMPHeistControl.lhcMyCorona.mhcIsIntroCutscene, g_sLocalMPHeistControl.lhcMyCorona.mhcIsMidStrandCutscene, g_sLocalMPHeistControl.lhcMyCorona.mhcIsTutorialHeist)
				ENDIF
			ELSE
				// ...player was known to be in heist corona - if the player is now on an activity session with a focus mission but not transitioning and not in a corona, then we're done with this corona
				IF (NETWORK_IS_ACTIVITY_SESSION())
				AND NOT (IS_TRANSITION_SESSION_RESTARTING())
				AND (Is_There_A_MissionsAtCoords_Focus_Mission())
					IF NOT (IS_PLAYER_IN_CORONA())
						// KGM 24/7/14: If the player had already transitioned then this will be the first Prep Mission chosen after viewing an Intro Cutscene - so need to wait for the player to get picked up by the corona
						IF (g_sLocalMPHeistControl.lhcMyCorona.mhcAlreadyTransitioned)
							// ...this corona was setup while already in an Activity Session (so must be the first Prep Mission after viewing the Intro Cutscene), so wait a while longer
							PRINTLN(".KGM [ActSelect][Heist][HCorona]: KEEP WAITING - Player is in an activity session but hasn't been in a corona yet - this must be the first Prep Mission after viewing an Intro Cutscene.")
							EXIT
						ELSE
							// ...player is in an Activity Session but not in a Corona and hadn't already transitioned - so assume the player has now gone on mission
							PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player was in their own heist corona, and is not in a corona when in the activity session - assume now on mission and delete the corona.")
							Delete_My_Stored_Corona_If_Available()
							
							EXIT
						ENDIF
					ELSE
						// Player is in the corona, so clear the 'already transitioned' flag
						#IF IS_DEBUG_BUILD
							IF (g_sLocalMPHeistControl.lhcMyCorona.mhcAlreadyTransitioned)
								PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player is in the new Prep Mission Corona after having exitied the Intro Cutscene Corona - clearing 'Already Transitioned' flag.")
							ENDIF
						#ENDIF
						
						g_sLocalMPHeistControl.lhcMyCorona.mhcAlreadyTransitioned = FALSE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			// ...player no longer has a focus mission or is in corona, so check if this means the player is no longer in their heist corona
			IF (g_sLocalMPHeistControl.lhcMyCorona.mhcInCorona)
				// ...if the transition session is restarting then leave things as they are for now
				IF NOT (playerIsTransitionSessionRestarting)
					// ...player was known to be in their heist corona but is no longer in a corona and not transitioning, therefore no longer in heist corona
					PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player has left their own Heist Corona")
					g_sLocalMPHeistControl.lhcMyCorona.mhcInCorona = FALSE
					
					// [BUG 1907156] Clear the details stored for Cross-Session transmittion to other Players. If the player is being walked out of the corona the details would remain setup.
					Clear_Heist_Data_Stored_For_Cross_Session_Transmission()
					
					// ...if this is a Planning Corona then the corona should get deleted - they can relaunch from the board
					IF NOT (g_sLocalMPHeistControl.lhcMyCorona.mhcIsIntroCutscene)
					AND NOT (g_sLocalMPHeistControl.lhcMyCorona.mhcIsMidStrandCutscene)
					AND NOT (g_sLocalMPHeistControl.lhcMyCorona.mhcIsFinale)
						PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player has left their own Heist Planning Corona, so delete the corona. It can be re-added from the board if relevant.")
						Delete_My_Stored_Corona_If_Available()
						
						EXIT
					ENDIF
					
					// ...if this is the Intro Cutscene Corona then the corona should get deleted and the Heist Stage should go back to the Context Intention control
					// KGM 6/10/14 [BUG 2068302]: This used to check for 'Intro Cut' AND 'Finale' - but these conditiosn aren't true now for the Tutorial Intro Cut, so deleted the 'AND Finale' check
					IF (g_sLocalMPHeistControl.lhcMyCorona.mhcIsIntroCutscene)
						PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player has left their own Heist Cutscene Corona, so delete the corona.")
						Delete_My_Stored_Corona_If_Available()
						
						IF NOT (NETWORK_IS_ACTIVITY_SESSION())
							PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player is not in an activity session so must have quit the corona before the transition. Move Heist Stage back to Wait for Player Start Input.")
							Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT)
						ELSE
							PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player is in an activity session when the cutscene corona was quit, so the player must be about to choose a Prep Mission. Leave the Heist Stage untouched.")
						ENDIF
						
						EXIT
					ENDIF
					
					// ...if this is the mid-strand cutscene corna then the corona should get deleted - they can relaunch from the board
					IF (g_sLocalMPHeistControl.lhcMyCorona.mhcIsMidStrandCutscene)
					AND (g_sLocalMPHeistControl.lhcMyCorona.mhcIsFinale)
						PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player has left their own Mid-Strand Cutscene Corona, so delete the corona. It can be re-added from the board if relevant.")
						Delete_My_Stored_Corona_If_Available()
						
						EXIT
					ENDIF
					
					// ...if this is the Finale corona and the player has quit from the planning board then the Matc corona may have been deleted while the Heist strand thinks it is still active
					//		this can also get hit if the player walks out of the corona before transition, but the corona doesn't get cleaned up - best fix, just clean it up anyway and add it again
					IF (g_sLocalMPHeistControl.lhcMyCorona.mhcIsFinale)
					AND NOT (g_sLocalMPHeistControl.lhcMyCorona.mhcIsIntroCutscene)
						PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player has left their own Heist corona and Matc 'may' have deleted it - so remove it to allow it to get added again.")
						Delete_My_Stored_Corona_If_Available()
						
						EXIT
					ENDIF
				ELSE
					// ...player was known to be in their heist corona and is no longer in a corona but IS transitioning, so leave things as they are until the transition completes
					PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player has left their own Heist Corona. IGNORED FOR NOW: Player is transitioning.")
				ENDIF
			ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	// Player is no longer in their own property, so check if they were in the heist corona
	IF (g_sLocalMPHeistControl.lhcMyCorona.mhcInCorona)
		// If the player is still classed as in the heist corona then do nothing for now
		IF (playerHasFocusMissionOrIsInCorona)
		OR (playerIsTransitionSessionRestarting)
			// ...still in heist corona or is transitioning, so do nothing for now
			IF (playerIsTransitionSessionRestarting)
				PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player has left their own apartment but is transitioning - do nothing for now, player may be placed back in apartment")
			ELSE
				// ...player isn't transitioning so do nothing if the player is still in the corona, otherwise cleanup the corona
				IF (Is_There_A_MissionsAtCoords_Focus_Mission())
				AND NOT (IS_PLAYER_IN_CORONA())
					PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player has left their own apartment, was in their own heist corona, has a focus mission, but not in a corona - assume now on mission and delete the corona.")
					Delete_My_Stored_Corona_If_Available()
					
					EXIT
				ELSE
					PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player has left their own apartment but is still classed as in their own heist corona - do nothing for now, player may be placed back in apartment")
				ENDIF
			ENDIF
			
			EXIT
		ENDIF
		
		// Player is no longer classed as being in the heist corona
		// ...intro cutscene corona - the player was in the corona when they left the apartment so the transition to the cutscene has played. Delete the corona. We're done with it.
		IF (g_sLocalMPHeistControl.lhcMyCorona.mhcIsIntroCutscene)
			PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player was in the Intro Cutscene corona but must have transitioned to the mission, delete the corona.")
			Delete_My_Stored_Corona_If_Available()
			
			EXIT
		ENDIF
		
		// ...mid-strand cutscene corona - the player was in the corona when they left the apartment so the transition to the cutscene has played. Delete the corona. We're done with it.
		IF (g_sLocalMPHeistControl.lhcMyCorona.mhcIsMidStrandCutscene)
			PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player was in the Mid-Strand Cutscene corona but must have transitioned to the mission, delete the corona.")
			Delete_My_Stored_Corona_If_Available()
			
			EXIT
		ENDIF
		
		EXIT
	ENDIF
	
	// If the player is not in the apartment and the corona is not on display, then nothing more to do
	IF (g_sLocalMPHeistControl.lhcMyCorona.mhcMatcID = ILLEGAL_AT_COORDS_ID)
		EXIT
	ENDIF
	
	// ...only cutscene coronas and finale coronas should reach here, I think.
	IF NOT (g_sLocalMPHeistControl.lhcMyCorona.mhcIsIntroCutscene)
	AND NOT (g_sLocalMPHeistControl.lhcMyCorona.mhcIsMidStrandCutscene)
	AND NOT (g_sLocalMPHeistControl.lhcMyCorona.mhcIsFinale)
		// Output a message - I don't think Prep coronas should reach here
		PRINTLN(".KGM [ActSelect][Heist][HCorona]: POTENTIAL ERROR: Heist Planning corona but Player not in apartment and not in corona - I don't think the code should reach here.")
	ENDIF
	
	PRINTLN(".KGM [ActSelect][Heist][HCorona]: Player left their own apartment, is not classed as being in their own heist corona, and the corona was on display - remove Heist corona")
	Remove_My_Stored_Corona_If_On_Display()

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Need to store this player's active Heist stats as player broadcast data
PROC Store_Current_Heist_Planning_Data_For_Broadcast()
	
	TEXT_LABEL_23 tlHeistFinaleRootContID = GET_HEIST_FINALE_STAT_DATA()
		
	IF ARE_STRINGS_EQUAL(tlHeistFinaleRootContID, ".")
	OR ARE_STRINGS_EQUAL(tlHeistFinaleRootContID, "")
		PRINTLN(".KGM [Heist]: Store_Current_Heist_Planning_Data_For_Broadcast() - IGNORE: There is no Finale Data stored.")
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Heist]: EXISTING DATA BEFORE Copying the Planning Missions RCIDs and Board Levels from STATS into Player Broadcast Data")
		PRINTLN(".KGM [HEIST]: EXISTING FINALE RCID IN STATS: ", tlHeistFinaleRootContID)
		Debug_Output_Heist_Planning_Broadcast_Details()
	#ENDIF

	// This is reading from STATs, so I'm just going to use a long-winded manual method of populating the array
	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[0].tl23MissionRootContID	= GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_1)
	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[0].iBoardLevel			= GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_MISSION_DEPTH_LV_1)
	
	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[1].tl23MissionRootContID	= GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_2)
	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[1].iBoardLevel			= GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_MISSION_DEPTH_LV_2)
	
	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[2].tl23MissionRootContID	= GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_3)
	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[2].iBoardLevel			= GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_MISSION_DEPTH_LV_3)
	
	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[3].tl23MissionRootContID	= GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_4)
	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[3].iBoardLevel			= GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_MISSION_DEPTH_LV_4)
	
	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[4].tl23MissionRootContID	= GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_5)
	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[4].iBoardLevel			= GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_MISSION_DEPTH_LV_5)
	
	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[5].tl23MissionRootContID	= GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_6)
	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[5].iBoardLevel			= GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_MISSION_DEPTH_LV_6)
	
	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[6].tl23MissionRootContID	= GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_7)
	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[6].iBoardLevel			= GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_MISSION_DEPTH_LV_7)
	
	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Heist]: AFTER Copying the Planning Missions RCIDs and Board Levels from STATS into Player Broadcast Data:")
		Debug_Output_Heist_Planning_Broadcast_Details()
	#ENDIF

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear this player's active Heist data in player broadcast data
PROC Clear_Current_Heist_Planning_Data_For_Broadcast()

	INT tempLoop = 0
	
	REPEAT MAX_HEIST_PLANNING_ROWS tempLoop
		GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[tempLoop].tl23MissionRootContID	= ""
		GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[tempLoop].iBoardLevel				= -1
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		PRINTLN("...KGM MP [ActSelect][Heist]: Clearing the Planning Missions RCIDs and Board Levels in Player Broadcast Data:")
		Debug_Output_Heist_Planning_Broadcast_Details()
	#ENDIF

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the PlayerBD containing the Heist control details are empty
//
// RETURN VALUE:			BOOL		TRUE if playerBD is empty, FALSE if full
FUNC BOOL Is_Heist_Planning_Data_For_Broadcast_Empty()

	// I'll assume that if the first RCID entry in player broadcast is NULL or empty then the data needs refilled
	TEXT_LABEL_23 testRCID = GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[0].tl23MissionRootContID
	
	IF (IS_STRING_NULL_OR_EMPTY(testRCID))
	OR (ARE_STRINGS_EQUAL(testRCID, "."))
		// ...empty
		RETURN TRUE
	ENDIF
	
	// Filled
	RETURN FALSE

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	The Heist control variables don't reset when jumping around within MP, but the player broadcast variables do - check if the broadcast variables need setup again
// NOTES:	This should only get called when the strand is active
PROC Refill_Current_Heist_Planning_Data_For_Broadcast_If_Required()

	IF (Is_Heist_Planning_Data_For_Broadcast_Empty())
		PRINTLN("...KGM MP [ActSelect][Heist]: The first entry in player broadcast is Empty. Refill Player Broadcast from Stats")
		Store_Current_Heist_Planning_Data_For_Broadcast()
		
		EXIT
	ENDIF

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Find the number of variations for the next Prep Mission
//
// INPUT PARAMS:			paramFirstArrayPos		The array Position within the downloaded data that contains the first Variation for the next Prep Mission
//							paramNumDownloaded		The total number of Prep Missions downloaded
FUNC INT Get_Next_Prep_Mission_Num_Variations(INT paramFirstArrayPos, INT paramNumDownloaded)

	// Safety Check
	IF (paramFirstArrayPos >= paramNumDownloaded)
		PRINTLN(".KGM [ActSelect][Heist]: Get_Next_Prep_Mission_Num_Variations() - ERROR: Outside Array Boundries. First: ", paramFirstArrayPos, ".  Total: ", paramNumDownloaded)
		SCRIPT_ASSERT("Get_Next_Prep_Mission_Num_Variations() - ERROR: Outside Array Boundries. Tell Keith.")
		RETURN paramFirstArrayPos
	ENDIF

	INT thisGroupID	= g_FMMC_STRUCT.sPlanningMissions[paramFirstArrayPos].iVar
	
	// -1 is a default for data that hasn't been setup, so assume one variation for this Prep Mission
	IF (thisGroupID = -1)
		RETURN 1
	ENDIF
	
	INT thisBoardLevel	= g_FMMC_STRUCT.sPlanningMissions[paramFirstArrayPos].iLevel
	INT tempLoop		= paramFirstArrayPos
	INT	numInGroup		= 0
	
	BOOL keepChecking = TRUE
	
	WHILE (keepChecking)
		IF (g_FMMC_STRUCT.sPlanningMissions[tempLoop].iVar = thisGroupID)
			// Found a Prep Mission in the same group
			// ...safety check - ensure the same board level
			IF (g_FMMC_STRUCT.sPlanningMissions[tempLoop].iLevel != thisBoardLevel)
				PRINTLN(".KGM [ActSelect][Heist]: Get_Next_Prep_Mission_Num_Variations() - WARNING: Board Levels differ between variations of the same Prep Mission.")
				SCRIPT_ASSERT("Get_Next_Prep_Mission_Num_Variations() - ERROR: Board Levels differ between variations of the same Prep Mission. IGNORING. Using the earliest Board Level. Tell Keith.")
			ENDIF
			
			numInGroup++
			tempLoop++
		ELSE
			// Found a Prep Mission in a different Group
			keepChecking = FALSE
		ENDIF
		
		// Check for End Search conditions
		IF (keepChecking)
			IF (tempLoop >= paramNumDownloaded)
				keepChecking = FALSE
			ENDIF
		ENDIF
	ENDWHILE
	
	RETURN (numInGroup)

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Choose the Prep Mission Variations to play from the full mission data downloaded
// NOTES:	A value of -1 means no variation. All missions with the same value are variations of the same mission and one of those needs randomly selected.
//			ASSUMES: The data is all in the correct order, so all missions in the same group are listed together
PROC Choose_Prep_Mission_Variations_To_Play_From_Downloaded_Data()

	#IF IS_DEBUG_BUILD
		INT tempLoop = 0
		// Loop through and output the downloaded missions to randomly choose a subset from
		PRINTLN(".KGM [ActSelect][Heist]: ...All Downloaded Planning Details:")
		REPEAT FMMC_MAX_PLANNING_MISSIONS tempLoop
			PRINTLN(".KGM [ActSelect][Heist]: ......", tempLoop, ") [DOWNLOADED DATA] Board Level: ", g_FMMC_STRUCT.sPlanningMissions[tempLoop].iLevel, "    GroupID: ", g_FMMC_STRUCT.sPlanningMissions[tempLoop].iVar, "    Prep RCID: ", g_FMMC_STRUCT.sPlanningMissions[tempLoop].tlID)
		ENDREPEAT
	#ENDIF
	
	// Analyse the downloaded data to choose the mission variations to play
	// ...first step, work out how many missions have been downloaded based on the rootCOntentIDs - this simplifies the next step
	INT numMissionsDownloaded = 0
	BOOL keepChecking = TRUE
	WHILE (keepChecking)
		IF (IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sPlanningMissions[numMissionsDownloaded].tlID))
			keepChecking = FALSE
		ELSE
			numMissionsDownloaded++
		ENDIF
		
		IF (numMissionsDownloaded = FMMC_MAX_PLANNING_MISSIONS)
			keepChecking = FALSE
		ENDIF
	ENDWHILE
	
	PRINTLN(".KGM [ActSelect][Heist]: ......Num missions downloaded: ", numMissionsDownloaded, " (Based on first empty rootContentID)")
	
	IF (numMissionsDownloaded = 0)
		PRINTLN(".KGM [ActSelect][Heist]: Choose_Prep_Mission_Variations_To_Play_From_Downloaded_Data() - ERROR: Failed to find any downloaded missions.")
		SCRIPT_ASSERT("Choose_Prep_Mission_Variations_To_Play_From_Downloaded_Data() - ERROR: Based on rootContentIDs, there are no missions associated with this Finale. Tell Keith.")
		EXIT
	ENDIF
	
	// ...next step, loop through all the available missions and choose a random one from each grouping
	INT 	arrayPosThisGroupStart		= 0
	INT 	arrayPosThisGroupEndPlusOne	= 0
	INT		numInThisGroup				= 0
	INT		thisGroupChosenOption		= -1
	INT 	nextStoreArrayPos			= 0
	
	keepChecking = TRUE
	
	WHILE (keepChecking)
		// Get the number of choices availabe for next Prep Mission Variation
		numInThisGroup = Get_Next_Prep_Mission_Num_Variations(arrayPosThisGroupStart, numMissionsDownloaded)
		
		// There should always be options
		IF (numInThisGroup = 0)
			PRINTLN(".KGM [ActSelect][Heist]: Choose_Prep_Mission_Variations_To_Play_From_Downloaded_Data() - ERROR: No options for current Prep Mission Group.")
			SCRIPT_ASSERT("Choose_Prep_Mission_Variations_To_Play_From_Downloaded_Data() - ERROR: No options for current Prep Mission Group. Tell Keith.")
			EXIT
		ENDIF
		
		// Update the Last Option (NOTE: This will be array position + 1)
		arrayPosThisGroupEndPlusOne = arrayPosThisGroupStart + numInThisGroup
		
		// There are options
		IF (numInThisGroup = 1)
			// ...only one option?
			thisGroupChosenOption = arrayPosThisGroupStart
			
			#IF IS_DEBUG_BUILD
				// Also update the widget if necessary since there is only one option
				IF (g_DEBUG_widgetUseSpecifiedOrder)
					g_DEBUG_WidgetRandomPrepToPlay[nextStoreArrayPos] = 0
				ENDIF
			#ENDIF
		ELSE
			// ...multiple options?
			thisGroupChosenOption = GET_RANDOM_INT_IN_RANGE(arrayPosThisGroupStart, arrayPosThisGroupEndPlusOne)
			
			#IF IS_DEBUG_BUILD
				// Has the variation been specified by widget?
				IF (g_DEBUG_widgetUseSpecifiedOrder)
					IF (g_DEBUG_WidgetRandomPrepToPlay[nextStoreArrayPos] > numInThisGroup)
						g_DEBUG_WidgetRandomPrepToPlay[nextStoreArrayPos] = 0
					ENDIF
					
					thisGroupChosenOption = arrayPosThisGroupStart + g_DEBUG_WidgetRandomPrepToPlay[nextStoreArrayPos]
				ENDIF
			#ENDIF
		ENDIF
		
		// Debug Output the options
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [ActSelect][Heist]: ...Options For Next Prep Mission:")
			tempLoop = arrayPosThisGroupStart
			WHILE (tempLoop < arrayPosThisGroupEndPlusOne)
				PRINTLN(".KGM [ActSelect][Heist]: ......", tempLoop, ") [GROUP OPTION] Board Level: ", g_FMMC_STRUCT.sPlanningMissions[tempLoop].iLevel, "    GroupID: ", g_FMMC_STRUCT.sPlanningMissions[tempLoop].iVar, "    Prep RCID: ", g_FMMC_STRUCT.sPlanningMissions[tempLoop].tlID)
				tempLoop++
			ENDWHILE
			PRINTLN(".KGM [ActSelect][Heist]: ......NUM OPTIONS FOR GROUP: ", numInThisGroup, ". CHOSEN OPTION: ", thisGroupChosenOption)
			
			IF (g_DEBUG_widgetUseSpecifiedOrder)
				PRINTLN(".KGM [ActSelect][Heist]: ......DEBUG WIDGET: This option was chosen based on the debug widget value for this group. Variation: ", g_DEBUG_WidgetRandomPrepToPlay[nextStoreArrayPos])
			ENDIF
		#ENDIF
		
		// Store the Chosen Option
		g_sPreHeistDownloadData.phddPrepMissions[nextStoreArrayPos].rootContentID	= g_FMMC_STRUCT.sPlanningMissions[thisGroupChosenOption].tlID
		g_sPreHeistDownloadData.phddPrepMissions[nextStoreArrayPos].boardLevel		= g_FMMC_STRUCT.sPlanningMissions[thisGroupChosenOption].iLevel
		
		nextStoreArrayPos++
		
		// Check for End Loop Conditions
		// ...if we've run out of space on the storage array
		IF (nextStoreArrayPos >= MAX_HEIST_PLANNING_MISSIONS)
			PRINTLN(".KGM [ActSelect][Heist]: ...Ending Checks For Prep Mission Variations - Storage Array Full")
			keepChecking = FALSE
		ENDIF
		
		// ...if we've looped through all the downloaded missions
		IF (keepChecking)
			IF (arrayPosThisGroupEndPlusOne >= numMissionsDownloaded)
				PRINTLN(".KGM [ActSelect][Heist]: ...Ending Checks For Prep Mission Variations - All Downloaded Missions Checked")
				keepChecking = FALSE
			ENDIF
		ENDIF
		
		// ...should be an unnecessary array boundary check
		IF (keepChecking)
			IF (arrayPosThisGroupEndPlusOne >= FMMC_MAX_PLANNING_MISSIONS)
				PRINTLN(".KGM [ActSelect][Heist]: ...Ending Checks For Prep Mission Variations - ERROR: About to loop out of the downloaded array boundary")
				keepChecking = FALSE
			ENDIF
		ENDIF
		
		// Update the loop variables ready for the next loop
		IF (keepChecking)
			arrayPosThisGroupStart = arrayPosThisGroupEndPlusOne
		ENDIF
	ENDWHILE
					
	// Loop through and output the chosen missions
	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [ActSelect][Heist]: ...Stored Planning Details:")
		REPEAT MAX_HEIST_PLANNING_MISSIONS tempLoop
			PRINTLN(".KGM [ActSelect][Heist]: ......", tempLoop, ") [CHOSEN DATA] Board Level: ", g_sPreHeistDownloadData.phddPrepMissions[tempLoop].boardLevel, "  Prep RCID: ", g_sPreHeistDownloadData.phddPrepMissions[tempLoop].rootContentID)
		ENDREPEAT
	#ENDIF

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Heist Stage: No Heist
PROC Maintain_MP_Heist_Stage_No_Heist()

	// Choose the next Heist to give to the player
	
	// NOTE: This function can get called too early for the HAS_PLAYER_COMPLETED_INITIAL_APARTMENT_HEIST_PHONECALL() to be correctly filled out, so go straight to the source STAT
	BOOL lesterIntroToHeistsCompleted = Has_Intro_To_Heists_Been_Done()
	
	// KGM 30/10/14: Generate the initial value for the NUMPAD-7 debug Heist flow variable
	IF (g_resetHeistsOnJoiningMP)
		PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_No_Heist(): Reset Heists On Joining MP - doing some first time setup.")
		
		// On a fresh boot the numpad7 flag needs setup
		#IF IS_DEBUG_BUILD
			// Is Heist Strand already active?
			IF (IS_MP_HEIST_STRAND_ACTIVE())
				// ...Heist Strand is active, so set the Numpad7 variable to the active strand value
				TEXT_LABEL_23	activeFinaleRootContID	= GET_HEIST_FINALE_STAT_DATA()
				TEXT_LABEL_63	activeHeistDescription	= TEMP_Debug_Get_Heist_Description_From_FinaleRCID(activeFinaleRootContID)
				INT				activeFinaleHash		= GET_HASH_KEY(activeFinaleRootContID)
				
				g_DEBUG_Numpad7HeistFlowOrder = Get_Heist_Strand_Flow_Value_From_Finale_Hash_RCID(activeFinaleHash)
				
				PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_No_Heist(): Reset Heists On Joining MP - Heist Strand active.")
				PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_No_Heist(): ...Set Numpad7 variable to the active strand. Debug Flow Order: ", g_DEBUG_Numpad7HeistFlowOrder, ". Heist: ", activeHeistDescription)
			ELSE
				// Heist Strand is not active
				IF (lesterIntroToHeistsCompleted)
					// The 'intro to Heists' has been completed so set the Numpad7 variable to the STAT value
					g_DEBUG_Numpad7HeistFlowOrder = GET_MP_INT_CHARACTER_STAT(MP_STAT_CURRENT_HEIST_STRAND)
					
					PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_No_Heist(): Reset Heists On Joining MP - Heist Strand not active and Lester 'Intro To Heists' Cutscene done.")
					PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_No_Heist(): ...Set Numpad7 variable to be the STAT value. Debug Flow Order: ", g_DEBUG_Numpad7HeistFlowOrder)
				ELSE
					// The 'intro to Heists' is not complete, so set the Numpaad7 variable to the Tutorial Heist
					g_DEBUG_Numpad7HeistFlowOrder = HEIST_FLOW_ORDER_FLEECA_TUTORIAL
					
					PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_No_Heist(): Reset Heists On Joining MP - Heist Strand not active and Lester 'Intro To Heists' Cutscene not done.")
					PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_No_Heist(): ...Set Numpad7 variable to be the Fleeca Tutorial. Debug Flow Order: ", g_DEBUG_Numpad7HeistFlowOrder)
				ENDIF
			ENDIF
		#ENDIF
		
		// Calculate the initial delay to be used once the conditions for allowing a Heist to play get met
		g_sLocalMPHeistControl.lhcRequiredInitialDelay = Get_Required_Initial_Delay_Between_Heists()
		PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_No_Heist(): Reset Heists On Joining MP - Storing Initial Delay between Heists if needed: ", g_sLocalMPHeistControl.lhcRequiredInitialDelay, " msec")
		
		// Clear the 'reset heists' flag
		g_resetHeistsOnJoiningMP = FALSE
		PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_No_Heist(): Reset Heists On Joining MP - Clearing 'reset heists' flag")
	ENDIF

	// Allow debug launching of a Heist Strand (this will be set by numpad7 routine)
	#IF IS_DEBUG_BUILD
		IF (Check_For_Heist_Debug_Launch())
		AND NOT (Is_There_A_MissionsAtCoords_Focus_Mission())
			IF NOT (Does_Player_Own_An_Apartment_Suitable_For_Heists())
				PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_No_Heist(): Debug Heist launch requested. IGNORED - Player doesn't own an apartment suitable for Heists")
				g_DEBUG_UseNumpad7Heist = FALSE
				EXIT
			ENDIF
				
			IF (IS_CORONA_DOWNLOADING_FROM_UGC_SERVER())
				PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_No_Heist(): Debug Heist launch requested. IGNORED - IS_CORONA_DOWNLOADING_FROM_UGC_SERVER() is TRUE")
				g_DEBUG_UseNumpad7Heist = FALSE
				EXIT
			ENDIF

			IF (ARE_WE_CURRENTLY_GETTING_CONTENT_BY_ID())
				PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_No_Heist(): Debug Heist launch requested. IGNORED - ARE_WE_CURRENTLY_GETTING_CONTENT_BY_ID() is TRUE")
				g_DEBUG_UseNumpad7Heist = FALSE
				EXIT
			ENDIF

			// KGM 11/2/15 [BUG 2232717]: Also delay in own apartment if replay board is available
			PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_No_Heist(): DEBUG LAUNCH ATTEMPT: Checking if Replay Board is on show in apartment.")
			IF (Should_Heist_Replay_Board_Be_Displayed_In_Apartment())
				PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_No_Heist(): - DEBUG LAUNCH ATTEMPT: FAILED - Replay Board on show.")
				g_DEBUG_UseNumpad7Heist = FALSE
				EXIT
			ENDIF

			// Download the Finale data
			INT debugFinaleHashRCID = Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(g_DEBUG_Numpad7HeistFlowOrder)
			Set_Current_Heist_Control_Hash_HeistRCID(debugFinaleHashRCID)
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_DOWNLOAD_DETAILS_FROM_FINALE)
			
			EXIT
		ENDIF
	#ENDIF
	
	// Don't go beyond here if the player doesn't own an apartment suitable for Heists
	IF NOT (Does_Player_Own_An_Apartment_Suitable_For_Heists())
		EXIT
	ENDIF
	
	// Don't go beyond here if the header data isn't downloaded
	IF NOT g_sMPTunables.bHeistGET_FM_UGC_HAS_FINISHED
		IF NOT (GET_FM_UGC_HAS_FINISHED())
			PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_No_Heist(): GET_FM_UGC_HAS_FINISHED = FALSE")
			EXIT
		ENDIF
	ENDIF
	
	// If the player already has Heist Strand details setup, then a Heist must be active, so move to the appropriate stage
	IF (IS_MP_HEIST_STRAND_ACTIVE())
		IF (g_cleanupHeistStrandStats_AlastairStuff)
			PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_No_Heist(): Heist Strand already active BUT g_cleanupHeistStrandStats_AlastairStuff is TRUE - WAIT UNTIL FALSE")
			EXIT
		ENDIF
		
		TEXT_LABEL_23 storedFinaleRootContID = GET_HEIST_FINALE_STAT_DATA()
		#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 storedHeistDescription = TEMP_Debug_Get_Heist_Description_From_FinaleRCID(storedFinaleRootContID)
			
			// Stage is 'No Heist' but there is a Heist Strand Active - this isn't necessarily a bug
			//     perhaps in this situation we should move this on to the 'waiting for completion' stage
			PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_No_Heist(): Heist is active in stats: ", storedHeistDescription, " - advancing to 'Heist Active' stage")
		#ENDIF

		Store_Current_Heist_Planning_Data_For_Broadcast()
		
		Set_Current_Heist_Control_Hash_HeistRCID(GET_HASH_KEY(storedFinaleRootContID))
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_COMPLETE)
		
		EXIT
	ENDIF
	
	// Only move beyond here if the initial Heist phonecall has been done (ie: the 'intro to heist' cutscene at Lester's)
	IF NOT (lesterIntroToHeistsCompleted)
		EXIT
	ENDIF
	
	// Calculate the initial delay and get it setup - this will be used if the strands are activated
	Setup_MP_Heist_Control_Initial_Delay_Timeout()
	Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY)

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Flash the Heist Apartment Blips
PROC Flash_Heist_Apartment_Blips()

	CONST_INT	APARTMENT_BLIP_FLASH_TIMEOUT_msec	10000

	INT theApartmentEntrance = 0
	INT buildingID = 0
	BLIP_INDEX thisBlipID = NULL
	INT tempLoop = 0

	REPEAT MAX_OWNED_PROPERTIES tempLoop
		INT thisProperty = GET_OWNED_PROPERTY(tempLoop)
		IF (thisProperty != NO_OWNED_SAFEHOUSE)
			IF (Is_This_PropertyID_Suitable_For_Heists(thisProperty))
				buildingID = mpProperties[thisProperty].iBuildingID
				thisBlipID = mpPropMaintain.PropertyBlips[buildingID][theApartmentEntrance]
				IF (DOES_BLIP_EXIST(thisBlipID))
					SET_BLIP_FLASHES(thisBlipID, TRUE)
					SET_BLIP_FLASH_TIMER(thisBlipID, APARTMENT_BLIP_FLASH_TIMEOUT_msec)
					IF GET_BLIP_COLOUR(thisBlipID) != BLIP_COLOUR_GREEN
						SET_BLIP_COLOUR(thisBlipID ,BLIP_COLOUR_GREEN)
					ENDIF
					PRINTLN(".KGM [ActSelect][Heist]: Flash Blip for player's owned apartment: ", tempLoop)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display 'Go To Apartment' help text after phonecall
PROC Display_Help_Text_After_Heist_Phonecall()

	TEXT_LABEL_23 postCallHeistHelpText = Get_Heist_Help_Text_After_Call(Get_Current_Heist_Control_Hash_HeistRCID())
	
	//	------	Fix for bug: 2057835 Rowan J - check if to ensure player is not in planning room before help text is displayed.
	IF (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE))
	
		INT iRoomKey = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
	
		IF iRoomKey != ci_HEIST_PLANNING_BUS_ROOM_KEY 
		AND iRoomKey != ci_HEIST_PLANNING_ROOM_KEY
		AND iRoomKey != ci_HEIST_PLANNING_STILT_A_ROOM_KEY
		AND iRoomKey != ci_HEIST_PLANNING_CUST_0_ROOM_KEY
		AND iRoomKey != ci_HEIST_PLANNING_CUST_1_ROOM_KEY
		AND iRoomKey != ci_HEIST_PLANNING_CUST_2_ROOM_KEY
		AND iRoomKey != ci_HEIST_PLANNING_CUST_3_ROOM_KEY
		AND iRoomKey != ci_HEIST_PLANNING_CUST_4_ROOM_KEY
		AND iRoomKey != ci_HEIST_PLANNING_CUST_5_ROOM_KEY
		AND iRoomKey != ci_HEIST_PLANNING_CUST_6_ROOM_KEY
		AND iRoomKey != ci_HEIST_PLANNING_CUST_7_ROOM_KEY
			PRINT_HELP(postCallHeistHelpText)
		ENDIF
	ELSE
		PRINT_HELP(postCallHeistHelpText)
	ENDIF
	//	------	
	
	// If this is the help text for the Tutorial Heist then need to record the number of times it has been sent - this help will get repeated
	IF (Is_This_Tutorial_Heist_RCID_Hash(Get_Current_Heist_Control_Hash_HeistRCID()))
		Control_Tutorial_Heist_Help_Text_Seen()
	ENDIF
	
	Flash_Heist_Apartment_Blips()

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the 'Go To Apartment' Tutorial Heist help text needs displayed again
PROC Maintain_Tutorial_Heist_Help_Text_Repeat_Display()

	// Ignore if there is no timeout
	IF (g_sLocalMPHeistControl.lhcTutorialHelpTimeout = 0)
		EXIT
	ENDIF

	// Ignore if the current heist is not the Tutorial Heist (also, force timeout if not Tutorial - probably something debug has left it running)
	IF NOT (Is_This_Tutorial_Heist_RCID_Hash(Get_Current_Heist_Control_Hash_HeistRCID()))
		g_sLocalMPHeistControl.lhcTutorialHelpTimeout = 0
		
		EXIT
	ENDIF
	
	// Ignore if the timeout hasn't occurred yet
	IF (GET_GAME_TIMER() < g_sLocalMPHeistControl.lhcTutorialHelpTimeout)
		EXIT
	ENDIF

	// Check if there is a reason to delay re-displaying the help text
	IF (Is_There_A_Reason_To_Delay_New_Heist_Strand())
		// Delay the re-display
		PRINTLN("...KGM MP [ActSelect][Heist][Delay]: Tutorial Heist Help Text Re-display timer has elapsed. Can't display. Additional Short Delay: ", HEIST_HELP_TEXT_QUICK_DELAY_msec, " msec")
		g_sLocalMPHeistControl.lhcTutorialHelpTimeout = GET_GAME_TIMER() + HEIST_HELP_TEXT_QUICK_DELAY_msec
		
		EXIT
	ENDIF

	// Delay if already in their own apartment
	IF (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE))
		// Delay the re-display
		PRINTLN("...KGM MP [ActSelect][Heist][Delay]: Tutorial Heist Help Text Re-display timer has elapsed. Player in own property. Additional Short Delay: ", HEIST_HELP_TEXT_QUICK_DELAY_msec, " msec")
		g_sLocalMPHeistControl.lhcTutorialHelpTimeout = GET_GAME_TIMER() + HEIST_HELP_TEXT_QUICK_DELAY_msec
		
		EXIT
	ENDIF
	
	// Display the text again
	PRINTLN("...KGM MP [ActSelect][Heist][Delay]: Tutorial Heist Help Text Re-display timer has elapsed. Re-display Help Text.")
	Display_Help_Text_After_Heist_Phonecall()

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Heist Stage: Wait For Initial Delay Expiry
PROC Maintain_MP_Heist_Stage_Wait_For_Initial_Delay_Expiry()

	// Allow debug launching of a Heist Strand (this will be set by numpad7 routine)
	#IF IS_DEBUG_BUILD
		IF (Check_For_Heist_Debug_Launch())
		AND NOT (Is_There_A_MissionsAtCoords_Focus_Mission())
			IF (IS_CORONA_DOWNLOADING_FROM_UGC_SERVER())
				PRINTLN("...KGM MP [Heist]: Maintain_MP_Heist_Stage_Wait_For_Initial_Delay_Expiry() - Debug Heist launch requested. IGNORED - IS_CORONA_DOWNLOADING_FROM_UGC_SERVER() is TRUE")
				g_DEBUG_UseNumpad7Heist = FALSE
				EXIT
			ENDIF

			IF (ARE_WE_CURRENTLY_GETTING_CONTENT_BY_ID())
				PRINTLN("...KGM MP [Heist]: Maintain_MP_Heist_Stage_Wait_For_Initial_Delay_Expiry() - Debug Heist launch requested. IGNORED - ARE_WE_CURRENTLY_GETTING_CONTENT_BY_ID() is TRUE")
				g_DEBUG_UseNumpad7Heist = FALSE
				EXIT
			ENDIF

			// KGM 11/2/15 [BUG 2232717]: Also delay in own apartment if replay board is available
			PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Wait_For_Initial_Delay_Expiry() - DEBUG LAUNCH ATTEMPT: Checking if Replay Board is on show in apartment.")
			IF (Should_Heist_Replay_Board_Be_Displayed_In_Apartment())
				PRINTLN(".KGM [Heist][Delay]: Maintain_MP_Heist_Stage_Wait_For_Initial_Delay_Expiry() - DEBUG LAUNCH ATTEMPT: FAILED - Replay Board on show.")
				g_DEBUG_UseNumpad7Heist = FALSE
				EXIT
			ENDIF

			// Download the Finale data
			INT debugFinaleHashRCID = Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(g_DEBUG_Numpad7HeistFlowOrder)
			Set_Current_Heist_Control_Hash_HeistRCID(debugFinaleHashRCID)
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_DOWNLOAD_DETAILS_FROM_FINALE)
			
			EXIT
		ENDIF
	#ENDIF
	
	// Allow debug cancelling of a Heist Strand (this will be set by Numpad8 routine)
	#IF IS_DEBUG_BUILD
		IF (Check_For_Heist_Debug_Cancel())
			PRINTLN(".KGM [Heist]: Heist Strand debug cancelled during HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY")
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Required_Initial_Delay_Between_Heists())
			EXIT
		ENDIF
	#ENDIF

	IF NOT (Has_MP_Heist_Control_Initial_Delay_Timeout_Expired())
		// KGM 14/11/14 [BUG 2131861]: Sanity check to ensure the delay never goes above the expected value
		#IF IS_DEBUG_BUILD
			IF ((g_sLocalMPHeistControl.lhcTimeout - GET_GAME_TIMER()) > Get_Required_Initial_Delay_Between_Heists(FALSE))
				PRINTLN(".KGM [Heist]: The Heist Initial Delay expiry time is greater than the expected maximum value. Timeout - NOW: ", (g_sLocalMPHeistControl.lhcTimeout - GET_GAME_TIMER()), ". Expected Delay: ", Get_Required_Initial_Delay_Between_Heists())
				PRINTLN(".KGM [Heist]: ...setting to max value")
				SCRIPT_ASSERT("Maintain_MP_Heist_Stage_Wait_For_Initial_Delay_Expiry(): The initial delay is greater than the expected max delay. Setting to Max Value. Tell Keith.")
				
				g_sLocalMPHeistControl.lhcTimeout = GET_GAME_TIMER() + Get_Required_Initial_Delay_Between_Heists()
			ENDIF
		#ENDIF
		
		// KGM 28/1/15 [BUG 2211902]: If the player is doing a JIP, keep the time remaining above a certain threshold value to prevent timeout - this is to cover a small gap where
		//		the JIP mission gets flattened by an unfortunately timed Heist Strand timer expiring before a new condition kicks in to prevent it
		IF (IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED())
			IF ((g_sLocalMPHeistControl.lhcTimeout - GET_GAME_TIMER()) < HEIST_JIP_MINIMUM_TIME_REMAINING_THRESHOLD_msec)
				// ...timeout time is too short, so give the timeout time a boost
				g_sLocalMPHeistControl.lhcTimeout = GET_GAME_TIMER() + HEIST_JIP_MINIMUM_TIME_REMAINING_THRESHOLD_msec + HEIST_JIP_TIMEOUT_BOOST_msec
				PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Wait_For_Initial_Delay_Expiry(). Player Performing JIP. Prevent Timeout from dropping below minimum threshold. New Timeout: ", g_sLocalMPHeistControl.lhcTimeout)
			ENDIF
		ENDIF
		
		// KGM 2/2/15 [BUG 2220501]: If the timer has only a short time to go until timeout then set a bitflag STAT to indicate that a reboot should make the phonecall soon after
		//				the reboot. This is a compromise - I don't want to save out all the STATS just to save this bitflag so the hope is that the player do something that performs
		//				a STAT save before trying to trigger the heist. NOTE: This stuff is only needed if the player reboots just before or just after receiving the heist phonecall
		//				because, if the heist strand hasn't been started, a reboot would set the timer to half the normal delay time (currently 30mins for most heists).
		IF NOT (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_NEXT_HEIST_QUICK_PHONECALL))
			// Don't set the bit for DEBUG or REPLAY
			BOOL allowBitSet = TRUE
			#IF IS_DEBUG_BUILD
				IF (g_DEBUG_UseNumpad7Heist)
					allowBitSet = FALSE
				ENDIF
			#ENDIF
			IF (g_replayHeistFlowOrder != UNKNOWN_HEIST_FLOW_ORDER)
				allowBitSet = FALSE
			ENDIF
			IF (allowBitSet)
				IF ((g_sLocalMPHeistControl.lhcTimeout - GET_GAME_TIMER()) < HEIST_SET_QUICK_PHONECALL_STAT_THRESHOLD_msec)
					// ...timeout time is below the 'set quick phonecall stat' threshold, so set the STAT
					SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_NEXT_HEIST_QUICK_PHONECALL)
					PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Wait_For_Initial_Delay_Expiry(). Setting 'do quick phonecall' STAT (relies on the game at some point performing a STAT SAVE")
				ENDIF
			ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	PRINTLN(".KGM [Heist]: Heist Control Initial Delay has ended, allow the comms to give a new Heist if available")
	
	// Reasons to delay the Heist Strand
	IF (Is_There_A_Reason_To_Delay_New_Heist_Strand())
		PRINTLN(".KGM [Heist][Delay]: Maintain_MP_Heist_Stage_Wait_For_Initial_Delay_Expiry() - New Heist Strand is being delayed.")
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Heist_Quick_Retry_Initial_Delay())
		EXIT
	ENDIF
	
	// Also delay if a mission download is already in progress
	IF (IS_CORONA_DOWNLOADING_FROM_UGC_SERVER())
	OR (ARE_WE_CURRENTLY_GETTING_CONTENT_BY_ID())
		IF (IS_CORONA_DOWNLOADING_FROM_UGC_SERVER())
			PRINTLN(".KGM [Heist][Delay]: Maintain_MP_Heist_Stage_Wait_For_Initial_Delay_Expiry() - New Heist Strand is being delayed - IS_CORONA_DOWNLOADING_FROM_UGC_SERVER()")
		ENDIF
		
		IF (ARE_WE_CURRENTLY_GETTING_CONTENT_BY_ID())
			PRINTLN(".KGM [Heist][Delay]: Maintain_MP_Heist_Stage_Wait_For_Initial_Delay_Expiry() - New Heist Strand is being delayed - ARE_WE_CURRENTLY_GETTING_CONTENT_BY_ID()")
		ENDIF
		
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Heist_Quick_Retry_Initial_Delay())
		
		EXIT
	ENDIF
	
	// KGM 21/1/15 [BUG 2193380]: Also delay if player is on pause menu - just in case the player is about to launch a mission - this download could clash with that
	IF (IS_PAUSE_MENU_ACTIVE_EX())
		PRINTLN(".KGM [Heist][Delay]: Maintain_MP_Heist_Stage_Wait_For_Initial_Delay_Expiry() - New Heist Strand is being delayed - Player viewing Pause Menu")
		
		// Using the 'failed to make phonecall' shorter delay for this
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, HEIST_CONTROL_REPEAT_PHONECALL_DELAY_msec)
		
		EXIT
	ENDIF

	// KGM 11/2/15 [BUG 2232717]: Also delay in own apartment if replay board is available
	PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Wait_For_Initial_Delay_Expiry() - Checking if Replay Board is on show in apartment.")
	IF (Should_Heist_Replay_Board_Be_Displayed_In_Apartment())
		PRINTLN(".KGM [Heist][Delay]: Maintain_MP_Heist_Stage_Wait_For_Initial_Delay_Expiry() - New Heist Strand is being delayed - Replay Board on show.")
		
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Heist_Quick_Retry_Initial_Delay())
		
		EXIT
	ENDIF

	// KGM 23/2/15 [BUG 2247731]: Also delay if the game is having trouble saving heist data
	IF (ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE())
		PRINTLN(".KGM [Heist][Delay]: Maintain_MP_Heist_Stage_Wait_For_Initial_Delay_Expiry() - New Heist Strand is being delayed - Game is having trouble saving heist progress.")
		
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Heist_Quick_Retry_Initial_Delay())
		
		EXIT
	ENDIF
	
	// Need to set the CurrentHeistControl value here based on the type of launch (normal, replay, debug) so that it is all setup for the download phase
	BOOL chosenHeist = FALSE
	
	#IF IS_DEBUG_BUILD
		IF (g_DEBUG_UseNumpad7Heist)
			PRINTLN(".KGM [Heist]: ...g_DEBUG_UseNumpad7Heist = TRUE. Setting up Heist Download based on Numpad7 value. Debug Flow Order: ", g_DEBUG_Numpad7HeistFlowOrder)
			INT debugFinaleHashRCID = Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(g_DEBUG_Numpad7HeistFlowOrder)
			Set_Current_Heist_Control_Hash_HeistRCID(debugFinaleHashRCID)
			chosenHeist = TRUE
		ENDIF
	#ENDIF
	
	// If a debug Heist hasn't been chosen, check for a replay
	IF NOT (chosenHeist)
		IF (g_replayHeistFlowOrder != UNKNOWN_HEIST_FLOW_ORDER)
			PRINTLN(".KGM [Heist]: ...player chose to replay a Heist. Setting up Heist Download based on replay value. Replay Flow Order: ", g_replayHeistFlowOrder)
			INT replayFinaleHashRCID = Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(g_replayHeistFlowOrder)
			Set_Current_Heist_Control_Hash_HeistRCID(replayFinaleHashRCID)
			chosenHeist = TRUE
		ENDIF
	ENDIF
	
	// If a Heist hasn't been chosen, then use the STAT if the intro to Heists has been done, or delay
	IF NOT (chosenHeist)
		IF NOT (Has_Intro_To_Heists_Been_Done())
			#IF IS_DEBUG_BUILD
				PRINTLN(".KGM [Heist]: Heist not chosen and Intro To Heists not done during HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY. IGNORING Setup.")
				SCRIPT_ASSERT("Maintain_MP_Heist_Stage_Wait_For_Initial_Delay_Expiry(): ERROR - Intro To Heists not done but no Heist Chosen. Tell Keith.")
			#ENDIF
			
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Required_Initial_Delay_Between_Heists())
			
			EXIT
		ENDIF
		
		// Set up the Heist using the STAT
		INT heistFlowStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_CURRENT_HEIST_STRAND)
		
		PRINTLN(".KGM [Heist]: ...Setting up Heist Download based on STAT value. Flow Order: ", heistFlowStat)
		
		INT finaleHashRCID = Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(heistFlowStat)
		Set_Current_Heist_Control_Hash_HeistRCID(finaleHashRCID)
	ENDIF
	
	Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_DOWNLOAD_DETAILS_FROM_FINALE)

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Heist Stage: Wait Until Comms Allowed
// NOTES:	This is here for debug checks - this stage will get moved on by another function called by Dave when the phonecall is allowed
PROC Maintain_MP_Heist_Stage_Download_Details_From_Finale()

	// Clear out the Pre-Download data
	IF NOT (g_sPreHeistDownloadData.phddFinaleHashRCID = 0)
		// ...clear the struct that holds the download data
		PRINTLN(".KGM [Heist]: Clear the PreHeist Download data struct.")
		
		g_sPreHeistDownloadData.phddFinaleHashRCID		= 0
		g_sPreHeistDownloadData.phddReward				= 0
		g_sPreHeistDownloadData.phddUpfrontLeaderCost	= 0
		g_sPreHeistDownloadData.phddIsTutorial			= FALSE
		g_sPreHeistDownloadData.phddSessionIdMacAddr	= 0
		g_sPreHeistDownloadData.phddSessionIdPosixTime	= 0
		
		INT clearLoop = 0
		REPEAT MAX_HEIST_PLANNING_MISSIONS clearLoop
			g_sPreHeistDownloadData.phddPrepMissions[clearLoop].rootContentID	= "."
			g_sPreHeistDownloadData.phddPrepMissions[clearLoop].boardLevel		= -1
		ENDREPEAT
	ENDIF
	
	// Safety Check - relaunch the loader script if it isn't launched (this should never be the case)
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("net_cloud_mission_loader")) = 0)
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [Heist]: net_cloud_mission_loader.sc is being relaunched.")
		#ENDIF
		
		REQUEST_SCRIPT("net_cloud_mission_loader")
    	IF (HAS_SCRIPT_LOADED("net_cloud_mission_loader"))
		    START_NEW_SCRIPT("net_cloud_mission_loader", FRIEND_STACK_SIZE)
		    SET_SCRIPT_AS_NO_LONGER_NEEDED("net_cloud_mission_loader")
		ELSE
			PRINTLN(".KGM [Heist]: ...still waiting for net_cloud_mission_loader.sc to relaunch before downloading Heist Details from Finale.")
		ENDIF
		
		EXIT
	ENDIF
	
	// It looks like the cloud loaded mission data function will return TRUE if the data is already downloaded, so just going to request it everytime
	// 	(The Clear Previous Data flag enforces a fresh download, we'll need this here to ensure the mission data definitely gets downloaded)
	TEXT_LABEL_31		ignoreOfflineContentID	= ""
	BOOL				useOfflineContentID		= FALSE
	BOOL				clearPrevUGCData		= TRUE
	MP_MISSION_ID_DATA	theMissionIdData
	
	Clear_MP_MISSION_ID_DATA_Struct(theMissionIdData)
	
	BOOL	foundDetails	= FALSE
	INT		slotLoop		= 0
		
	REPEAT g_numLocalMPHeists slotLoop
		IF NOT (foundDetails)
			IF (Get_Current_Heist_Control_Hash_HeistRCID() = g_sLocalMPHeists[slotLoop].lhsHashFinaleRCID)
				theMissionIdData = g_sLocalMPHeists[slotLoop].lhsMissionIdData
				foundDetails = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT (foundDetails)
		PRINTLN(".KGM [Heist]: Heist Mission Details Download: Failed to find the RCID Hash stored on the array: ", Get_Current_Heist_Control_Hash_HeistRCID(), ". Restarting delay")
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Heist_Quick_Retry_Initial_Delay())
		
		EXIT
	ENDIF
	
	IF (Load_Cloud_Loaded_Mission_Data(theMissionIdData, ignoreOfflineContentID, useOfflineContentID, clearPrevUGCData))
		// ...it has now downloaded, so return FALSE (it the data is no longer waiting to be downloaded)
		PRINTLN(".KGM [Heist]: The Cloud-Loaded Mission Data load has ended.")
		
		IF (Did_Cloud_Loaded_Mission_Data_Load_Successfully())
// KGM 11/2/15: Commented this out for the Sam build. It's an additional safety check that is probably ok, but untested.
//			// KGM 11/2/15 [BUG 2232717]: Ignore this download data if in own apartment with replay board available and this isn't a replay
//			IF NOT (g_replayHeistIsFromBoard)
//				PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Download_Details_From_Finale() - Checking if Replay Board is on show in apartment.")
//				IF (Should_Heist_Replay_Board_Be_Displayed_In_Apartment())
//					PRINTLN(".KGM [Heist][Delay]: Maintain_MP_Heist_Stage_Download_Details_From_Finale() - Ignore downloaded data - Replay Board on show.")
//					
//					// Clear out a bunch of download data after we've stored the data we need to ensure the next download attempt does perform an actual download (to fix 1965286)
//					INT myPlayerGBD = NATIVE_TO_INT(PLAYER_ID())
//					CLEAR_BIT(GlobalplayerBD_FM[myPlayerGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded)
//					CLEAR_BIT(GlobalplayerBD_FM[myPlayerGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataCleared)
//					CLEAR_BIT(GlobalplayerBD_FM[myPlayerGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_ReadyToVote)
//					CLEAR_TRANSITION_SESSION_MISSION_LOADED_BEFORE_LAUNCH()
//					CLEAR_TRANSITION_SESSIONS_MISSION_HAS_LOADED()
//					#IF IS_DEBUG_BUILD
//						PRINTLN(".KGM [Heist]: Clear mission download data after storage to allow a fresh download for next mission data download request")
//					#ENDIF
//					
//					Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Heist_Quick_Retry_Initial_Delay())
//					
//					EXIT
//				ENDIF
//			ENDIF
	
			// Does this Finale have Planning data stored?
			BOOL retryDownload = FALSE
			IF (IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sPlanningMissions[0].tlID))
				// ...there doesn't appear to be any Planning mission details stored in this mission
				PRINTLN(".KGM [Heist]: ...Cloud-Loaded Mission Data download: SUCCESS. Downloaded this mission: ", g_FMMC_STRUCT.tl63MissionName)
				PRINTLN(".KGM [Heist]: ......but there doesn't appear to be any Planning data stored in this mission, so moving on.")
				
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [Heist]: ...Planning Details (appear to be unfilled):")
					
					REPEAT FMMC_MAX_PLANNING_MISSIONS slotLoop
						PRINTLN(".KGM [Heist]: ......", slotLoop, ") Board Level: ", g_FMMC_STRUCT.sPlanningMissions[slotLoop].iLevel, "  Prep RCID: ", g_FMMC_STRUCT.sPlanningMissions[slotLoop].tlID)
					ENDREPEAT
					
					PRINTLN(".KGM [Heist]: ...Clear the download details and retry")
				#ENDIF
				
				SCRIPT_ASSERT("Maintain_MP_Heist_Stage_Download_Details_From_Finale: Failed to find Planning Mission details stored in Finale Mission. Tell Keith.")
				
				retryDownload = TRUE
			ELSE
				// Store the data
				g_sPreHeistDownloadData.phddFinaleHashRCID		= Get_Current_Heist_Control_Hash_HeistRCID()
				g_sPreHeistDownloadData.phddReward				= g_FMMC_STRUCT.iCashReward
				g_sPreHeistDownloadData.phddUpfrontLeaderCost	= CALCULATE_HEIST_STRAND_ENTRY_FEE(g_sPreHeistDownloadData.phddReward, g_sPreHeistDownloadData.phddFinaleHashRCID)
				
				// Get the 'is tutorial' flag from the cloud download.
				g_sPreHeistDownloadData.phddIsTutorial = FALSE
				IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciHEIST_TUTORIAL_STRAND))
					g_sPreHeistDownloadData.phddIsTutorial = TRUE
				ENDIF
				
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [Heist]: ...Cloud-Loaded Mission Data download  : SUCCESS.")
					PRINTLN(".KGM [Heist]: ...Downloaded this mission             : ", g_FMMC_STRUCT.tl63MissionName, " [RCID Hash: ", g_sPreHeistDownloadData.phddFinaleHashRCID, "]")
					PRINTLN(".KGM [Heist]: ...Cash Reward Band on Heist Completion: ", g_sPreHeistDownloadData.phddReward)
					PRINTLN(".KGM [Heist]: ...Leader Upfront Heist Cost           : $", g_sPreHeistDownloadData.phddUpfrontLeaderCost)
					PRINTLN(".KGM [Heist]: ...Tutorial Heist?                     : ", PICK_STRING(g_sPreHeistDownloadData.phddIsTutorial, "TRUE", "FALSE"))
				#ENDIF
				
				// TUTORIAL FLAG COMMANDLINES TO SWITCH ON AND OFF
				#IF IS_DEBUG_BUILD
					IF (g_sPreHeistDownloadData.phddIsTutorial)
						// Ensure only the tutorial Heist has the flag set
						IF (g_sPreHeistDownloadData.phddFinaleHashRCID != Get_Tutorial_Heist_RCID_Hash())
							g_sPreHeistDownloadData.phddIsTutorial = FALSE
							PRINTLN(".KGM [Heist]: TUTORIAL HEIST FLAG is set for a Heist that isn't the Tutorial")
							SCRIPT_ASSERT("Maintain_MP_Heist_Stage_Download_Details_From_Finale(): ERROR - TUTORIAL HEIST FLAG is set for a Heist that isn't the Tutorial. Clearing Flag. Tell Keith.")
						ENDIF
						
						// Heists are on, so check if the flag should be debug switched off?
						IF (GET_COMMANDLINE_PARAM_EXISTS("sc_debugClearTutorialHeist"))
							g_sPreHeistDownloadData.phddIsTutorial = FALSE
							PRINTLN(".KGM [Heist]: Clearing TUTORIAL HEIST FLAG because of this commandline: sc_debugClearTutorialHeist")
						ENDIF
					ENDIF
				#ENDIF

				// KGM 27/9/14: Some prep missions have multiple variations, so choose the variations to play
				Choose_Prep_Mission_Variations_To_Play_From_Downloaded_Data()
				
				// To ensure the data is available for the planning board for the Intro Cutscene, immediately store the downloaded missions as Player Broadcast data
				Clear_Current_Heist_Planning_Data_For_Broadcast()
				
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [Heist]: Immediately store the newly downloaded data as Player Broadcast data")
				#ENDIF
				
				REPEAT MAX_HEIST_PLANNING_MISSIONS slotLoop
					GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[slotLoop].tl23MissionRootContID	= g_sPreHeistDownloadData.phddPrepMissions[slotLoop].rootContentID
					GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[slotLoop].iBoardLevel				= g_sPreHeistDownloadData.phddPrepMissions[slotLoop].boardLevel
				ENDREPEAT
				
				#IF IS_DEBUG_BUILD
					Debug_Output_Heist_Planning_Broadcast_Details()
				#ENDIF
			ENDIF
			
			// Store the MacAddress and the PosixTime as a way to uniquely identify this specific launch of a Heist Strand - used for telemetry
			IF NOT (retryDownload)
				PRINTLN(".KGM [Heist]: Heist Session ID Generation: Get the MacAddress and PosixTime to uniquely identify this play of a Heist Strand")
				
				IF NOT (PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(g_sPreHeistDownloadData.phddSessionIdMacAddr, g_sPreHeistDownloadData.phddSessionIdPosixTime))
					PRINTLN(".KGM [Heist]: Heist Session ID Generation: ...FAILED. PLAYSTATS_CREATE_MATCH_HISTORY_ID_2() returned FALSE.")
				ELSE
					PRINTLN(".KGM [Heist]: Heist Session ID Generation: ...SUCCESS. MacAddr: ", g_sPreHeistDownloadData.phddSessionIdMacAddr, ". PosixTime: ", g_sPreHeistDownloadData.phddSessionIdPosixTime)
				ENDIF
			ENDIF
			
			// Clear out a bunch of download data after we've stored the data we need to ensure the next download attempt does perform an actual download (to fix 1965286)
			INT myPlayerGBD = NATIVE_TO_INT(PLAYER_ID())
			CLEAR_BIT(GlobalplayerBD_FM[myPlayerGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded)
			CLEAR_BIT(GlobalplayerBD_FM[myPlayerGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataCleared)
			CLEAR_BIT(GlobalplayerBD_FM[myPlayerGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_ReadyToVote)
			CLEAR_TRANSITION_SESSION_MISSION_LOADED_BEFORE_LAUNCH()
			CLEAR_TRANSITION_SESSIONS_MISSION_HAS_LOADED()
			#IF IS_DEBUG_BUILD
				PRINTLN(".KGM [Heist]: Clear mission download data after storage to allow a fresh download for next mission data download request")
			#ENDIF
			
			// Retry download?
			IF (retryDownload)
				PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Download_Details_From_Finale(): Retry Heist Finale Mission Download")
				Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Heist_Quick_Retry_Initial_Delay())
				EXIT
			ENDIF
			
			// If I'm in my own apartment then force a prep mission download
			IF (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE))
				PRINTLN(".KGM [Heist]: Clearing lhcPrepsDownloadedIn500 to FALSE. Mission Finale pre-heist download complete while in own apartment.")
				g_sLocalMPHeistControl.lhcPrepsDownloadedIn500	= FALSE
				g_sLocalMPHeistControl.lhcDownloadAttempts		= 0
			ELSE
				PRINTLN(".KGM [Heist]: Leaving lhcPrepsDownloadedIn500 flag untouched. Mission Finale pre-heist download complete while NOT in own apartment.")
			ENDIF
			
			IF NOT g_bIsHeistStageCommsActive
				g_bIsHeistStageCommsActive	=	TRUE
			ENDIF
			
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_UNTIL_COMMS_ALLOWED)
			EXIT
		ENDIF
		
		// Failed to download
		PRINTLN(".KGM [Heist]: ...Cloud-Loaded Mission Data download: FAILED. Restarting Delay.")
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Heist_Quick_Retry_Initial_Delay())
		
		EXIT
	ELSE
		// KGM 23/4/15 [BUG 2291852]: Still downloading, so cancel the Next Heist Strand download if the player accepts a cross-session invite
		IF (Has_Cross_Session_Invite_Been_Recently_Accepted())
			PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Download_Details_From_Finale() - Player accepted cross-session invite during Next Heist Download. Cancel download.")
			Cancel_Ongoing_Cloud_Loaded_Mission_Data_Download()
			
		// KGM 4/8/15 [BUG 2456548]: Cancel Heist Finale download if the player goes On Call
		ELIF (AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL())
			PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Download_Details_From_Finale() - Player is starting On Call. Cancel download.")
			Cancel_Ongoing_Cloud_Loaded_Mission_Data_Download()
			
		ENDIF
	ENDIF
	
	// Still waiting for it to download
	PRINTLN(".KGM [Heist]: Still waiting for the Mission Details Download to complete.")

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Heist Stage: Wait Until Comms Allowed
// NOTES:	This is here for debug checks - this stage will get moved on by another function called by Dave when the phonecall is allowed
PROC Maintain_MP_Heist_Stage_Wait_Until_Comms_Allowed()

	// KGM 9/2/15 [BUG 2226481]: If the player has chosen a replay from the board, move on
	IF (g_replayHeistIsFromBoard)
		PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Wait_Until_Comms_Allowed() - Replay picked from Planning Board. Move on.")
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_REQUEST_COMMS)
		EXIT
	ENDIF
	
	IF NOT g_bIsHeistStageCommsActive
		g_bIsHeistStageCommsActive	=	TRUE
	ENDIF
	
	// Allow debug cancelling of a Heist Strand (this will be set by Numpad8 routine)
	#IF IS_DEBUG_BUILD
		IF (Check_For_Heist_Debug_Cancel())
			PRINTLN("...KGM MP [ActSelect][Heist]: Heist Strand debug cancelled during HEIST_CLIENT_STAGE_WAIT_UNTIL_COMMS_ALLOWED")
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Required_Initial_Delay_Between_Heists())
			EXIT
		ENDIF
	#ENDIF

	// If 'quick Heists' command line is active, don't wait for Dave's say-so
	#IF IS_DEBUG_BUILD
		IF (GET_COMMANDLINE_PARAM_EXISTS("sc_debugQuickHeists"))
			PRINTLN("...KGM MP [ActSelect][Heist]: sc_debugQuickHeists commandline param exists, so moving immediately to requesting Heist Comms")
			Allow_MP_Heist_Comms_If_Available()
		ENDIF
	#ENDIF

ENDPROC
	// FEATURE_HEIST_PLANNING

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Heist Stage: Request Comms
PROC Maintain_MP_Heist_Stage_Request_Comms()

	// KGM 9/2/15 [BUG 2226481]: If the player has chosen a replay from the board, move on
	IF (g_replayHeistIsFromBoard)
		PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Request_Comms() - Replay picked from Planning Board. Move on.")
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_FOR_COMMS_TO_END)
		EXIT
	ENDIF
	
	IF NOT g_bIsHeistStageCommsActive
		g_bIsHeistStageCommsActive	=	TRUE
	ENDIF
	
	// Allow debug cancelling of a Heist Strand (this will be set by Numpad8 routine)
	#IF IS_DEBUG_BUILD
		IF (Check_For_Heist_Debug_Cancel())
			PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Request_Comms() - Heist Strand debug cancelled")
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Required_Initial_Delay_Between_Heists())
			EXIT
		ENDIF
	#ENDIF
	
	// Reasons to delay the New Heist Strand phonecall
	IF (Is_There_A_Reason_To_Delay_New_Heist_Strand())
		PRINTLN(".KGM [Heist][Delay]: Maintain_MP_Heist_Stage_Request_Comms() - Phonecall to activate New Heist Strand is being delayed.")
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Heist_Quick_Retry_Initial_Delay())
		EXIT
	ENDIF
	
	// KGM 10/2/15 [BUG 2231832]: Prevent phonecall in apartment if replay board is available
	PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Request_Comms() - Checking if Replay Board is on show in apartment.")
	IF (Should_Heist_Replay_Board_Be_Displayed_In_Apartment())
		PRINTLN(".KGM [Heist][Delay]: Maintain_MP_Heist_Stage_Request_Comms() - Replay Board on show - Delay Phonecall.")
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Heist_Quick_Retry_Initial_Delay())
		EXIT
	ENDIF

	// KGM 23/2/15 [BUG 2247731]: Also delay if the game is having trouble saving heist data
	IF (ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE())
		PRINTLN(".KGM [Heist][Delay]: Maintain_MP_Heist_Stage_Request_Comms() - Game is having trouble saving heist progress - Delay Phonecall.")
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Heist_Quick_Retry_Initial_Delay())
		EXIT
	ENDIF

	// Request comms to tell the player about the heist
	INT phonecallModifiers = ALL_MP_COMMS_MODIFIERS_CLEAR
	SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_PRIORITY_PHONECALL)
	SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_FORCE_PLAYER_TO_ANSWER)
	
	structPedsForConversation pedForConversation
	INT Lester_SpeakerID = 3
	ADD_PED_FOR_DIALOGUE(pedForConversation, Lester_SpeakerID, NULL, "Lester")
	TEXT_LABEL_15 phonecallRootID = Get_Phonecall_RootID_For_Triggering_Heist(Get_Current_Heist_Control_Hash_HeistRCID())
	
	IF (IS_STRING_NULL_OR_EMPTY(phonecallRootID))
		PRINTLN(".KGM ][Heist]: Failed to find PhonecallRootID for this Finale RootContentID Hash: ", Get_Current_Heist_Control_Hash_HeistRCID())
		SCRIPT_ASSERT("Maintain_MP_Heist_Stage_Request_Comms() - ERROR: Phonecall rootContentID was NULL. Tell Keith")
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_SETUP_HEIST_CUTSCENE_CORONA)
	ENDIF
	
	SET_USE_DLC_DIALOGUE(TRUE)
	IF (Request_MP_Comms_Message(pedForConversation, CHAR_LESTER, "LCAU", phonecallRootID, phonecallModifiers))
		PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Request_Comms() - Phonecall for Heist is in progress")
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_FOR_COMMS_TO_END)
	ENDIF

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Heist Stage: Wait For Comms To End
PROC Maintain_MP_Heist_Stage_Wait_For_Comms_To_End()

	// KGM 9/2/15 [BUG 2226481]: If the player has chosen a replay from the board, move on
	IF (g_replayHeistIsFromBoard)
		PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Wait_For_Comms_To_End() - Replay picked from Planning Board. Move on.")
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT)
		EXIT
	ENDIF
		
	// Allow debug cancelling of a Heist Strand (this will be set by Numpad8 routine)
	#IF IS_DEBUG_BUILD
		IF (Check_For_Heist_Debug_Cancel())
			PRINTLN(".KGM [Heist]: Heist Strand debug cancelled during HEIST_CLIENT_STAGE_WAIT_FOR_COMMS_TO_END - Killing Comms if necessary")
			
			IF (Is_MP_Comms_Still_Playing())
				Kill_Any_MP_Comms()
			ENDIF
			
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Required_Initial_Delay_Between_Heists())
			EXIT
		ENDIF
	#ENDIF

	IF (Is_MP_Comms_Still_Playing())
		IF NOT g_bIsHeistStageCommsActive
			g_bIsHeistStageCommsActive	=	TRUE
		ENDIF
		EXIT
	ENDIF
	
	SET_USE_DLC_DIALOGUE(FALSE)
	
	IF NOT (Did_MP_Comms_Complete_Successfully())
		PRINTLN(".KGM [Heist]: Phonecall for Heist did not complete successfully")
		
		// If this phonecall is for a Heist Strand that has alrady been completed as Leader then don't spam the phonecall
		IF (g_replayHeistFlowOrder = UNKNOWN_HEIST_FLOW_ORDER)
		AND (GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(Get_Current_Heist_Control_Hash_HeistRCID(), TRUE) > 0)
			// ...heist already completed as Leader, using a longer phonecall delay
			PRINTLN(".KGM [Heist]: ...Heist previously completed as Leader, so use a longer 'try again' delay for phonecall")
			Setup_MP_Heist_Control_Repeat_Phonecall_Longer_Delay_Timeout()
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY)
			EXIT
		ENDIF
		
		Setup_MP_Heist_Control_Repeat_Phonecall_Delay_Timeout()
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY)
		EXIT
	ENDIF
	
	PRINTLN(".KGM [Heist]: Phonecall for Heist has ended successfully")
	
	//-- Dave W, want to trigger some reminder help text if the player doesn't go to their apartment.
	SET_LOCAL_PLAYER_SHOULD_CHECK_FOR_HEIST_REMINDER(TRUE, TRUE)
	
	#IF IS_DEBUG_BUILD
	IF g_DEBUG_quickLaunchPlanning
		PRINTLN(".KGM [Heist]: g_DEBUG_quickLaunchPlanning = TRUE, skip help-text.")
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT)
		EXIT
	ENDIF
	#ENDIF
	
	Display_Help_Text_After_Heist_Phonecall()
	
	// Clear any previous Heist Completion Phonecall
	PRINTLN(".KGM [Heist]: Clear any Heist Completion Phonecall request.")
	Clear_Heist_Completion_Phonecall_Controls()
	Clear_All_Heist_Completion_Phonecall_Bits()
		
	// KGM 2/2/15 [BUG 2220501]: Set a STAT once the phonecall is made so that a reboot makes the phonecall occur again soon after (NOTE: relies on the game performing a STAT SAVE).
	IF NOT (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_NEXT_HEIST_QUICK_PHONECALL))
		// ...the phonecall has been successfully completed, so set the STAT
		// Don't set the STAT for DEBUG or REPLAY
		BOOL allowBitSet = TRUE
		#IF IS_DEBUG_BUILD
			IF (g_DEBUG_UseNumpad7Heist)
				allowBitSet = FALSE
			ENDIF
		#ENDIF
		IF (g_replayHeistFlowOrder != UNKNOWN_HEIST_FLOW_ORDER)
			allowBitSet = FALSE
		ENDIF
		IF (allowBitSet)
			SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_NEXT_HEIST_QUICK_PHONECALL)
			PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Wait_For_Comms_To_End(). Setting 'do quick phonecall' STAT (relies on the game at some point performing a STAT SAVE")
		ENDIF
	ENDIF
	
	// Ensure the 'start cutscene' context button is clear
	g_sLocalMPHeistControl.lhcContextCutscene = NEW_CONTEXT_INTENTION
	Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT)

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Some intro anim maintenance - not entirely sure what it's doing, but it looks like it's setting up the 'walk to setee' cutscene
// NOTES:	KGM 11/10/14: Extracted out to its own function because it'll need maintained from the new 'heist cost alert' stage too
PROC Maintain_Heist_Intro_Anim_Housekeeping()

	// KGM 10/2/15 [BUG 2226481]: Don't perform these animation updates if a Replay from the Board has been selected
	IF (g_replayHeistIsFromBoard)
		EXIT
	ENDIF

	// Is player in own property?
	IF NOT (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE))
		EXIT
	ENDIF
	
	// Player is in own property
	IF g_HeistSharedClient.sHeistAnimFirst.bXThreadCleanup
		PRINTLN("[AMEC][HEIST_ANIMS] - MAINTAIN_MP_HEIST_STAGE_WAIT_FOR_PLAYER_START_INPUT - sHeistAnimFirst has a lingering bXThreadCleanup flag. Clean all old data before pre-load.")
		MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimFirst)
	ENDIF
	
	IF g_HeistSharedClient.sHeistAnimSecond.bXThreadCleanup
		PRINTLN("[AMEC][HEIST_ANIMS] - MAINTAIN_MP_HEIST_STAGE_WAIT_FOR_PLAYER_START_INPUT - sHeistAnimSecond has a lingering bXThreadCleanup flag. Clean all old data before pre-load.")
		MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond)
	ENDIF
	
	IF g_HeistSharedClient.sHeistAnimThird.bXThreadCleanup
		PRINTLN("[AMEC][HEIST_ANIMS] - MAINTAIN_MP_HEIST_STAGE_WAIT_FOR_PLAYER_START_INPUT - sHeistAnimThird has a lingering bXThreadCleanup flag. Clean all old data before pre-load.")
		MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimThird)
	ENDIF

	// KGM 19/11/14 [BUG 2138575]: Keep maintaining the anims while the alert screen is on display (don't check for being out of the planning room)
	IF IS_PLAYER_IN_A_HEIST_PLANNING_ROOM(PLAYER_ID())
	OR (Get_Current_Heist_Control_Stage() = HEIST_CLIENT_STAGE_DISPLAY_HEIST_COST)
	
		#IF IS_DEBUG_BUILD
			IF (Get_Current_Heist_Control_Stage() = HEIST_CLIENT_STAGE_DISPLAY_HEIST_COST)
				PRINTLN("[AMEC][HEIST_ANIMS] - MAINTAIN_MP_HEIST_STAGE_WAIT_FOR_PLAYER_START_INPUT - INFO: Player on 'display heist cost' alert screen, pre-loading anim assets.")
			ELSE
				PRINTLN("[AMEC][HEIST_ANIMS] - MAINTAIN_MP_HEIST_STAGE_WAIT_FOR_PLAYER_START_INPUT - INFO: Player ped in room, pre-loading anim assets.")
			ENDIF
		#ENDIF
	
		g_enumActiveHeistAnim eHeistAnim
	
		// Build a new module controller if one doesn;t already exist.
		IF NOT IS_ANIM_CONTROLLER_ALREADY_ACTIVE(g_HeistSharedClient.sHeistAnimFirst)
			CONSTRUCT_NEW_HEIST_ANIM_CONTROLLER(g_HeistSharedClient.sHeistAnimFirst, 0, HEIST_SELECTED_ANIM_PHONE)
		ENDIF
		
		IF NOT IS_ANIM_CONTROLLER_ALREADY_ACTIVE(g_HeistSharedClient.sHeistAnimSecond)
			eHeistAnim 	= GET_HEIST_INTO_ANIM_PRE(	Get_Current_Heist_Control_Hash_HeistRCID(), 
													(NOT IS_PLAYER_PED_FEMALE(INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex))),
													TRUE,
													FALSE,
													FALSE)		
			CONSTRUCT_NEW_HEIST_ANIM_CONTROLLER(g_HeistSharedClient.sHeistAnimSecond, 1, eHeistAnim)
		ENDIF
		
		IF NOT IS_ANIM_CONTROLLER_ALREADY_ACTIVE(g_HeistSharedClient.sHeistAnimThird)
			eHeistAnim	= GET_HEIST_INTO_ANIM_DURING(Get_Current_Heist_Control_Hash_HeistRCID(), 
													(NOT IS_PLAYER_PED_FEMALE(INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex))),
													TRUE,
													FALSE,
													FALSE)	
			CONSTRUCT_NEW_HEIST_ANIM_CONTROLLER(g_HeistSharedClient.sHeistAnimThird, 2, eHeistAnim)
		ENDIF


		IF NOT ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimFirst)
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimFirst, TRUE)
		ENDIF
		
		IF NOT ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimSecond)
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond, TRUE)
		ENDIF
		
		IF NOT ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimThird)
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimThird, TRUE)
		ENDIF
		
		EXIT
	ENDIF
		
	// Player isn't in either of the specified rooms
	IF ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimFirst)
		CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimFirst)
	ENDIF
	
	IF ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimSecond)
		CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimSecond)
	ENDIF
	
	IF ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimThird)
		CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimThird)
	ENDIF
		
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// Clear the Context Button
PROC Clear_Player_Start_Input_Context_Button()
			
	// Cleanup
	IF (IS_CONTEXT_INTENTION_HELP_DISPLAYING(g_sLocalMPHeistControl.lhcContextCutscene))
		HIDE_ALL_CONTEXT_HELP_NEXT_UPDATE()
	ENDIF
	
	RELEASE_CONTEXT_INTENTION(g_sLocalMPHeistControl.lhcContextCutscene)
	
	IF (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_CONTEXT"))
	OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_CONTEXT_LR"))
		CLEAR_HELP()
	ENDIF
			
	g_sLocalMPHeistControl.lhcContextCutscene = NEW_CONTEXT_INTENTION
			
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// Clear the Context Button or any warnings on screen
PROC Clear_Player_Start_Input_Context_Button_Or_Warnings()
			
	// Cleanup Context
	Clear_Player_Start_Input_Context_Button()
	
	// Clean up the 'not enough cash' or 'wanted level' help message
	IF (IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("HCUT_NOCASH", g_sPreHeistDownloadData.phddUpfrontLeaderCost))
	OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_WANTED"))
		CLEAR_HELP()
	ENDIF
			
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

PROC Maintain_MP_Heist_Stage_Wait_For_Player_Start_Input()

	// Clear the 'Clear Tasks Behind Alert' screen - doesn't matter that this is every frame while player input is available
	g_sLocalMPHeistControl.lhcClearTasksOnAlert = FALSE

	// KGM 9/2/15 [BUG 2226481]: If the player has chosen a replay from the board, move on
	IF (g_replayHeistIsFromBoard)
		// KGM 26/2/15 [BUG 2250537]: If there is a STAT save problem then drop into the standard routines to handle it
		IF (ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE())
			PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Wait_For_Player_Start_Input() - Replay picked from Planning Board BUT game is having trouble saving heist progress. Clear replay boar flag to handle this.")
			g_replayHeistIsFromBoard = FALSE
		ELSE
			PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Wait_For_Player_Start_Input() - Replay picked from Planning Board. Move on.")
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_DISPLAY_HEIST_COST)
			EXIT
		ENDIF
	ENDIF
			
	// Allow debug cancelling of a Heist Strand (this will be set by Numpad8 routine)
	#IF IS_DEBUG_BUILD
		IF (Check_For_Heist_Debug_Cancel())
			PRINTLN(".KGM [Heist]: Heist Strand debug cancelled during HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT, cleanup context and help text if active")
			
			// Cleanup Context
			Clear_Player_Start_Input_Context_Button_Or_Warnings()
			
			// Change stage
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Required_Initial_Delay_Between_Heists())
			
			EXIT
		ENDIF
	#ENDIF
	
	//-- Dave W, don't setup context if cutscene is still playing
	IF IS_LOCAL_PLAYER_RUNNING_INTRO_TO_PLANNING_BOARD_CUTSCENE()
		PRINTLN(".KGM [Heist]: Delaying Player Input: Player is still running Intro To Planning Board Cutscene.")
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_DEBUG_quickLaunchPlanning
		PRINTLN(".KGM [Heist]: g_DEBUG_quickLaunchPlanning = TRUE, skip corona.")
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_ACTIVE)
		EXIT
	ENDIF
	#ENDIF
	
	// KGM 11/10/14: Moved the anim functionality out to its own function so it can be called from the Heist Cost stage too
	Maintain_Heist_Intro_Anim_Housekeeping()

	// Is the Context Intention setup?
	IF (g_sLocalMPHeistControl.lhcContextCutscene = NEW_CONTEXT_INTENTION)
		// The context isn't setup
		
		// Check if the Tutorial Heist 'Go To Apartment' Help Text needs re-displayed
		Maintain_Tutorial_Heist_Help_Text_Repeat_Display()
		
		// Is the player within range in their own apartment?
		IF NOT (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE))
			// Clear the 'not enough cash' or 'wanted level' message if on display
			IF (IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("HCUT_NOCASH", g_sPreHeistDownloadData.phddUpfrontLeaderCost))
			OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_WANTED"))
			OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD"))
				PRINTLN(".KGM [Heist]: Clearing 'not enough cash' or 'wanted level' or 'saving issues' help text: Player has left the apartment.")
				CLEAR_HELP()
			ENDIF
			
			EXIT
		ENDIF
		
		// Check if the PlayerBD is empty
		IF (Is_Heist_Planning_Data_For_Broadcast_Empty())
			IF (IS_STRING_NULL_OR_EMPTY(g_sPreHeistDownloadData.phddPrepMissions[0].rootContentID))
			OR (ARE_STRINGS_EQUAL(g_sPreHeistDownloadData.phddPrepMissions[0].rootContentID, "."))
				// phone again
				PRINTLN(".KGM [Heist]: PlayerBD is empty, the Stats aren't setup at this stage, and the preDownload data has gone. Need to download the mission details again.")
			
				// Clean up the 'not enough cash' or 'wanted level' help message
				IF (IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("HCUT_NOCASH", g_sPreHeistDownloadData.phddUpfrontLeaderCost))
				OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_WANTED"))
				OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD"))
					PRINTLN(".KGM [Heist]: Clearing 'not enough cash' or 'wanted level' or 'saving issues' help text: Repeating the mission download.")
					CLEAR_HELP()
				ENDIF
				
				Setup_MP_Heist_Control_Repeat_Phonecall_Delay_Timeout()
				Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY)
				EXIT
			ELSE
				PRINTLN(".KGM [Heist]: Refill the Player Broadcast data from the PreDownload data")
				
				INT slotLoop = 0
				REPEAT MAX_HEIST_PLANNING_MISSIONS slotLoop
					GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[slotLoop].tl23MissionRootContID	= g_sPreHeistDownloadData.phddPrepMissions[slotLoop].rootContentID
					GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[slotLoop].iBoardLevel				= g_sPreHeistDownloadData.phddPrepMissions[slotLoop].boardLevel
				ENDREPEAT
				
				#IF IS_DEBUG_BUILD
					Debug_Output_Heist_Planning_Broadcast_Details()
				#ENDIF
			ENDIF
		ENDIF
		
		IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), Get_Heist_Corona_Coords_For_Players_Currently_Occupied_Apartment()) > (HEIST_CUTSCENE_TRIGGER_m - HEIST_CUTSCENE_TRIGGER_LEEWAY_m))
			// Clear the 'not enough cash' or 'wanted level' message if on display
			IF (IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("HCUT_NOCASH", g_sPreHeistDownloadData.phddUpfrontLeaderCost))
			OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_WANTED"))
			OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD"))
				PRINTLN(".KGM [Heist]: Clearing 'not enough cash' or 'wanted level' or 'saving issues' help text: Player is outside the trigger area.")
				CLEAR_HELP()
			ENDIF

			EXIT
		ENDIF

		// KGM 23/2/15 [BUG 2244469]: Check if a delay is needed for a one-off piece of special help text if the replay board has just become unlocked while there is an active heist
		IF (g_hasReplayBoardBeenUnlocked)
		AND NOT (IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_HEIST_RPLY_BOARD))
			PRINTLN(".KGM [Heist]: Delaying Player Start Heist Input: Replay Board Unlocked and one-off help text still needs to be displsyed.")
			EXIT
		ENDIF
		
		// Don't setup if there is a mission
		IF (Is_There_A_MissionsAtCoords_Focus_Mission())
			PRINTLN(".KGM [Heist]: Delaying Player Start Heist Input: Player has a Focus Mission.")
			
			// Clear the 'not enough cash' or 'wanted level' message if on display
			IF (IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("HCUT_NOCASH", g_sPreHeistDownloadData.phddUpfrontLeaderCost))
			OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_WANTED"))
			OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD"))
				PRINTLN(".KGM [Heist]: Clearing 'not enough cash' or 'wanted level' or 'saving issues' help text: Player has a focus mission.")
				CLEAR_HELP()
			ENDIF

			EXIT
		ENDIF
		
		// Don't setup if the player has activated the Pause Menu
		IF (IS_PAUSE_MENU_ACTIVE_EX())
			PRINTLN(".KGM [Heist]: Delaying Player Start Heist Input: The Pause Menu is now active.")
			
			// Clear 'not enough cash' or 'wanted level' messages if on display
			IF (IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("HCUT_NOCASH", g_sPreHeistDownloadData.phddUpfrontLeaderCost))
			OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_WANTED"))
			OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD"))
				PRINTLN(".KGM [Heist]: Clearing 'not enough cash' or 'wanted level' or 'saving issues' help text: The Pause Menu is now active.")
				CLEAR_HELP()
			ENDIF

			EXIT
		ENDIF
		
		// Don't setup if the player has accepted a cross-session invite
		IF (Has_Cross_Session_Invite_Been_Recently_Accepted())
			PRINTLN(".KGM [Heist]: Delaying Player Start Heist Input: Player has accepted a cross-session invite.")
			
			// Clear 'not enough cash' or 'wanted level' messages if on display
			IF (IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("HCUT_NOCASH", g_sPreHeistDownloadData.phddUpfrontLeaderCost))
			OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_WANTED"))
			OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD"))
				PRINTLN(".KGM [Heist]: Clearing 'not enough cash' or 'wanted level' or 'saving issues' help text: Player has accepted a cross-session invite.")
				CLEAR_HELP()
			ENDIF

			EXIT
		ENDIF
		
		// KGM 18/3/15 [BUG 2282423]: Don't setup if there is an ambient tutorial active
		IF (IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL())
			PRINTLN(".KGM [Heist]: Delaying Player Start Heist Input: Player is doing an Ambient Tutorial.")
			
			// Clear 'not enough cash' or 'wanted level' messages if on display
			IF (IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("HCUT_NOCASH", g_sPreHeistDownloadData.phddUpfrontLeaderCost))
			OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_WANTED"))
			OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD"))
				PRINTLN(".KGM [Heist]: Clearing 'not enough cash' or 'wanted level' or 'saving issues' help text: Player is doing an Ambient Tutorial.")
				CLEAR_HELP()
			ENDIF

			EXIT
		ENDIF
		
		// KGM 19/3/15 [BUG 2284341]: Don't allow interaction with the board if the player is in a network session - the player has probably been picked up for on call
		IF NOT (g_sMPTUNABLES.bDisable_Heist_On_Call_From_Planning_Board)
			IF (NETWORK_IS_IN_TRANSITION())
				PRINTLN(".KGM [Heist]: Delaying Player Start Heist Input: NETWORK_IS_IN_TRANSITION() - probably joining on call.")
				
				// Clear 'not enough cash' or 'wanted level' messages if on display
				IF (IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("HCUT_NOCASH", g_sPreHeistDownloadData.phddUpfrontLeaderCost))
				OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_WANTED"))
				OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD"))
					PRINTLN(".KGM [Heist]: Clearing 'not enough cash' or 'wanted level' or 'saving issues' help text: Player is doing an Ambient Tutorial.")
					CLEAR_HELP()
				ENDIF

				EXIT
			ENDIF
		ENDIF
	
		// KGM 23/2/15 [BUG 2247731]: Don't setup the DPADRIGHT context if the game is having trouble saving
		IF (ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE())
			// Clear the 'not enough cash' or 'wanted level' message if on display
			IF (IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("HCUT_NOCASH", g_sPreHeistDownloadData.phddUpfrontLeaderCost))
			OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_WANTED"))
				PRINTLN(".KGM [Heist]: Game is having trouble saving heist progress - the 'not enough cash' or 'wanted level' help text is on display and will get overwritten.")
			ENDIF
			
			IF NOT (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD"))
				PRINTLN(".KGM [Heist]: Game is having trouble saving heist progress - printing Help Message [FM_COR_HCLOUD]")
				PRINT_HELP("FM_COR_HCLOUD", HEIST_SEMI_PERMANENT_HELP_TIME_msec)
			ENDIF
			
			EXIT
		ENDIF
		
		// KGM 23/2/15 [BUG 2247731]: If we reach here, clear the help message
		IF (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD"))
			PRINTLN(".KGM [Heist]: Game is NO LONGER having trouble saving heist progress - clearing Help Message [FM_COR_HCLOUD]")
			CLEAR_HELP()
		ENDIF
		
		// Don't setup Context if player has a Wanted Level
		IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
			// ...wanted level, so display a help message instead
			IF NOT (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_WANTED"))
				PRINTLN(".KGM [Heist]: Player has a Wanted Level. (Re)displaying 'wanted level' help.")
				PRINT_HELP("HCUT_WANTED", HEIST_SEMI_PERMANENT_HELP_TIME_msec)
			ENDIF
			
			EXIT
		ELSE
			IF (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_WANTED"))
				PRINTLN(".KGM [Heist]: Player doesn't have a wanted level. Clearing 'wanted level' help.")
				CLEAR_HELP()
			ENDIF
		ENDIF		
		
		// Don't setup Context if not enough cash
		// ...to prevent checking cash all the time, just quit if the help message is already on display
		IF (IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("HCUT_NOCASH", g_sPreHeistDownloadData.phddUpfrontLeaderCost))
			EXIT
		ENDIF
		
		// ...check the cash (from Bank, then Wallet) 
		IF NOT (NETWORK_CAN_SPEND_MONEY(g_sPreHeistDownloadData.phddUpfrontLeaderCost, FALSE, TRUE, FALSE))
			// ...not enough cash, so display a help message instead
			PRINTLN(".KGM [Heist]: Player doesn't have enough cash. Requires $", g_sPreHeistDownloadData.phddUpfrontLeaderCost, ". Got (Bank+Wallet) $", (NETWORK_GET_VC_BALANCE()), ". (Re)displaying 'not enough cash' help.")
			PRINT_HELP_WITH_NUMBER("HCUT_NOCASH", g_sPreHeistDownloadData.phddUpfrontLeaderCost, HEIST_SEMI_PERMANENT_HELP_TIME_msec)
			
			EXIT
		ENDIF
		
		// KGM 13/2/15 [BUG 2224475]: The tutorial needs the first prep mission, so don't setup the context until the first prep mission has downloaded
		IF (Is_This_Tutorial_Heist_RCID_Hash(Get_Current_Heist_Control_Hash_HeistRCID()))
			// Tutorial Heist, so check if the 'extra' mission data slot has data
			IF NOT (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
				// ...Rockstar Created array extra data slot doesn't have data, so delay the context
				PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Wait_For_Player_Start_Input(): DELAY SETTING UP CONTEXT - Tutorial Heist Strand still waiting for Preps Download to complete.")
				EXIT
			ENDIF
		ENDIF
		
		// Player can afford Heist
		PRINTLN(".KGM [Heist]: Player within range and can afford Heist. Requires $", g_sPreHeistDownloadData.phddUpfrontLeaderCost, ". Got (Bank+Wallet) $", NETWORK_GET_VC_BALANCE(), ".")
		PRINTLN(".KGM [Heist]: Allow setup of Context Intention for triggering Heist Strand.")
		#IF IS_DEBUG_BUILD
			Debug_Output_Heist_Corona_Details()
		#ENDIF

//		// Clear any existing help message
//		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//		AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_CONTEXT")
//			CLEAR_HELP()
//		ENDIF
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_LEAVEH")
			CLEAR_HELP()
		ENDIF
		
		//	Context intention won't display until help messages are killed.
		
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_STR_BG3")
			g_HeistSharedClient.bSuppressPlanningBoardContextIntention = TRUE
		ELSE
			g_HeistSharedClient.bSuppressPlanningBoardContextIntention = FALSE
		ENDIF
		
		// Register the Heist button Context Intention
		IF NOT g_HeistSharedClient.bSuppressPlanningBoardContextIntention
			// KGM 18/3/15 [BUG 2283171]: Also allow DPADLEFT to be displayed along with DPADRIGHT if the player isn't already 'on call'
			// KGM 20/3/15 [BUG 2285702]: Don't allow DPADLEFT if the ped is drunk or stoned
			IF (AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL() AND NOT SHOULD_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW())
			OR (g_sMPTUNABLES.bDisable_Heist_On_Call_From_Planning_Board)
			OR (Is_Ped_Drunk(PLAYER_PED_ID()))
				PRINTLN(".KGM [Heist]: Context Intention is for DPADRIGHT only.")
				REGISTER_CONTEXT_INTENTION(g_sLocalMPHeistControl.lhcContextCutscene, CP_MAXIMUM_PRIORITY, "HCUT_CONTEXT")
			ELSE
				PRINTLN(".KGM [Heist]: Context Intention is for DPADRIGHT and for DPADLEFT (option to go on call instead).")
				REGISTER_CONTEXT_INTENTION(g_sLocalMPHeistControl.lhcContextCutscene, CP_MAXIMUM_PRIORITY, "HCUT_CONTEXT_LR")
			ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	// Context Intention is setup
	// Is the Context Help on display?
	IF NOT (IS_CONTEXT_INTENTION_HELP_DISPLAYING(g_sLocalMPHeistControl.lhcContextCutscene))
		PRINTLN(".KGM [Heist]: Heist Context Help Text not on display. Waiting...")
		EXIT
	ENDIF
	
	// Context Help is on display.
	BOOL keepTheContextSetup = TRUE
	
	// Is the player still in their own apartment?
	IF NOT (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE))
		PRINTLN(".KGM [Heist]: Player is no longer in their apartment. Clean up Start Heist context.")
		keepTheContextSetup = FALSE
	ENDIF
	
	// Is the player still within Context activation range in their own apartment?
	IF (keepTheContextSetup)
		IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), Get_Heist_Corona_Coords_For_Players_Currently_Occupied_Apartment()) > HEIST_CUTSCENE_TRIGGER_m)
			PRINTLN(".KGM [Heist]: Player is no longer within Heist Cutscene Context activation range. Clean up Start Heist context.")
			keepTheContextSetup = FALSE
		ENDIF
	ENDIF
	
	// Does the player have a Focus Mission?
	IF (keepTheContextSetup)
		IF (Is_There_A_MissionsAtCoords_Focus_Mission())
			PRINTLN(".KGM [Heist]: Player now has a Focus Mission. Clean up Start Heist context.")
			keepTheContextSetup = FALSE
		ENDIF
	ENDIF
	
	// Has the Pause Menu been activated?
	IF (keepTheContextSetup)
		IF (IS_PAUSE_MENU_ACTIVE_EX())
			PRINTLN(".KGM [Heist]: Pause Menu is now active. Clean up Start Heist context.")
			keepTheContextSetup = FALSE
		ENDIF
	ENDIF
	
	// Has the player accepted a cross-session invite?
	IF (keepTheContextSetup)
		IF (Has_Cross_Session_Invite_Been_Recently_Accepted())
			PRINTLN(".KGM [Heist]: Player has accepted a Cross-Session Invite. Clean up Start Heist context.")
			keepTheContextSetup = FALSE
		ENDIF
	ENDIF
	
	// KGM 18/3/15 [BUG 2282423]: Does the player have an ambient tutorial active?
	IF (keepTheContextSetup)
		IF (IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL())
			PRINTLN(".KGM [Heist]: Player has an ambient tutorial active. Clean up Start Heist context.")
			keepTheContextSetup = FALSE
		ENDIF
	ENDIF
	
	// KGM 19/3/15 [BUG 2284341]: Don't allow interaction with the board if the player is in a network session - the player has probably been picked up for on call
	IF (keepTheContextSetup)
		IF NOT (g_sMPTUNABLES.bDisable_Heist_On_Call_From_Planning_Board)
			IF (NETWORK_IS_IN_TRANSITION())
				PRINTLN(".KGM [Heist]: NETWORK_IS_IN_TRANSITION() - Player may be joining on call. Clean up Start Heist context.")
				keepTheContextSetup = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// Cleanup if the Cash drops below startup charge
	IF (keepTheContextSetup)
		IF NOT (NETWORK_CAN_SPEND_MONEY(g_sPreHeistDownloadData.phddUpfrontLeaderCost, FALSE, TRUE, FALSE))
			PRINTLN(".KGM [Heist]: Player doesn't have enough cash. Requires $", g_sPreHeistDownloadData.phddUpfrontLeaderCost, ". Got (Bank+Wallet) $", (NETWORK_GET_VC_BALANCE()), ". Clean up Start Heist context.")
			keepTheContextSetup = FALSE
		ENDIF
	ENDIF
	
	// Cleanup if the player gets a wanted level
	IF (keepTheContextSetup)
		IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
			PRINTLN(".KGM [Heist]: Player has a Wanted Level. Clean up Start Heist context.")
			keepTheContextSetup = FALSE
		ENDIF
	ENDIF
	
	// KGM 23/2/15 [BUG 2247731]: If the game starts having trouble saving
	IF (keepTheContextSetup)
		IF (ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE())
			PRINTLN(".KGM [Heist]: Game is having trouble saving heist progress. Clean up Start Heist context.")
			keepTheContextSetup = FALSE
		ENDIF
	ENDIF
	
	// KGM 18/3/15 [BUG 2283171]: If the player is not on call and the player pressed DPADLEFT then cleanup the context and setup on call
	// KGM 20/3/15 [BUG 2285702]: Don't allow DPADLEFT if the ped is drunk or stoned
	IF (keepTheContextSetup)
		IF NOT (g_sMPTUNABLES.bDisable_Heist_On_Call_From_Planning_Board)
			IF NOT (AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL())
			AND NOT (Is_Ped_Drunk(PLAYER_PED_ID()))
				// DPADLEFT is available
				IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT_SECONDARY))
					// Player pressed DPADLEFT
					PRINTLN(".KGM [Heist]: Player chose DPADLEFT to setup OnCall to a Heist. Also Clean up Start Heist context to allow it to be redisplayed without DPADLEFT.")
					IF (REGISTER_FOR_HEIST_QUICK_MATCH_FROM_BOARD())
						keepTheContextSetup = FALSE
					ELSE
						PRINTLN(".KGM [Heist]: ...IGNORE - REGISTER_FOR_HEIST_QUICK_MATCH_FROM_BOARD() returned FALSE. Keep the context setup.")
					ENDIF
				ELSE
					// KGM 30/4/15 [BUG 2288292]: Player didn't press DPADLEFT but conditions are good for DPADLEFT text to be displayed.
					//		If it's still only the DPADRIGHT message on display, clear the context so it can be re-displayed with DPADLEFT.
					IF (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_CONTEXT"))
						PRINTLN(".KGM [Heist]: DPADLEFT is available but not displayed by help message. Clear context so it can be re-displayed.")
						keepTheContextSetup = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Check if the content has triggered if the context should remain
	IF (keepTheContextSetup)
		// The context should remain setup.
		// Has the Context Button just been triggered?
		IF (HAS_CONTEXT_BUTTON_TRIGGERED(g_sLocalMPHeistControl.lhcContextCutscene, TRUE))
			// ...context button has been pressed, so clear up and take the player into the corona
			PRINTLN(".KGM [Heist]: Heist Context button has been pressed. Cleaning up context and activating corona.")
			
			IF (IS_CONTEXT_INTENTION_HELP_DISPLAYING(g_sLocalMPHeistControl.lhcContextCutscene))
				HIDE_ALL_CONTEXT_HELP_NEXT_UPDATE()
			ENDIF
			
			FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE(TRUE)
			
			RELEASE_CONTEXT_INTENTION(g_sLocalMPHeistControl.lhcContextCutscene)
			
			SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()
			
			IF (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_CONTEXT"))
			OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HCUT_CONTEXT_LR"))
				CLEAR_HELP()
			ENDIF
			
			// Remove player control immediately, but don't clear tasks until the alert screen is on show
			// KGM 24/10/14 [BUG 2090533]: Avoid a pop by hiding the 'clear tasks' behind the heist cost alert screen
			PRINTLN(".KGM [Heist]: Request player's tasks to be cleared when 'heist cost' alert screen is on display.")
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS | NSPC_FREEZE_POSITION)
			g_sLocalMPHeistControl.lhcClearTasksOnAlert = TRUE
			
			SET_HEIST_CORONA_FROM_PLANNING_BOARD(TRUE)
			
			// Move on to the next stage
			g_sLocalMPHeistControl.lhcContextCutscene = NEW_CONTEXT_INTENTION
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_DISPLAY_HEIST_COST)
		ENDIF
		
		EXIT
	ENDIF
	
	// The Context needs to be cleaned up
	#IF IS_DEBUG_BUILD
		Debug_Output_Heist_Corona_Details()
	#ENDIF
	
	Clear_Player_Start_Input_Context_Button()
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Heist Stage: Display Heist Cost
PROC Maintain_MP_Heist_Stage_Display_Heist_Cost()
			
	// Ignore debug cancelling of a Heist Strand during this stage (this will be set by Numpad8 routine)

	// Maintain the Heist Intro Anim - it looks like this is still needed while this screen is being processed
	// KGM 10/2/15 [BUG 2226481]: Don't perform these animation updates if a Replay from the Board has been selected
	IF NOT (g_replayHeistIsFromBoard)
		Maintain_Heist_Intro_Anim_Housekeeping()
	ENDIF
	
	// Ignore the alert screen if the cost is $0
	// KGM 9/2/15 [BUG 2226481]: Also ignore i fthe replay was chosen from the Planning Board
	IF (g_sPreHeistDownloadData.phddUpfrontLeaderCost > 0)
	AND NOT (g_replayHeistIsFromBoard)
		// Should the player 'clear tasks'?
		IF (g_sLocalMPHeistControl.lhcClearTasksOnAlert)
			IF (IS_WARNING_MESSAGE_ACTIVE())
				// Clear the player's tasks
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS | NSPC_FREEZE_POSITION)
				PRINTLN(".KGM [Heist]: Clearing Player's Tasks. 'heist cost' alert screen is on display.")
				g_sLocalMPHeistControl.lhcClearTasksOnAlert = FALSE
			ENDIF
		ENDIF
	
		// Maintain the Heist Cost alert screen
		SET_WARNING_MESSAGE_WITH_HEADER("HCOST_TITLE", "HCOST_BODY", FE_WARNING_OKCANCEL, "HEIST_WARN_2", TRUE, g_sPreHeistDownloadData.phddUpfrontLeaderCost, Get_Heist_Hardcoded_Title_TextLabel(g_sPreHeistDownloadData.phddFinaleHashRCID))
		
		// Did player reject the charge?
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			// ...yes, rejected
			PRINTLN(".KGM [Heist]: Player rejected the Heist Start Cost.")
			
			// Give player back control
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			PRINTLN(".KGM [Heist]: Clearing the 'Clear Tasks' flag. The alert screen was rejected.")
			g_sLocalMPHeistControl.lhcClearTasksOnAlert = FALSE
			
			// Move back to Player Input stage
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT)
			
			EXIT		
		ENDIF
		
		// Did player accept the charge?
		IF NOT (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
			// ...don't know yet, keep waiting
			EXIT
		ENDIF
		
		// Player Accepted Charge, so move on to the next stage - most of this was copied from the 'contenxt button pressed' section in 'Player Input' stage
		PRINTLN(".KGM [Heist]: Player accepted the Heist Start Cost.")
	ELSE
		// Don't display the alert cost screen
		// KGM 9/2/15 [BUG 2226481]: If the player has chosen a replay from the board, move on
		IF (g_replayHeistIsFromBoard)
			PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Display_Heist_Cost() - Ignore Heist Cost screen and move on. Replay picked from Planning Board.")
		ELSE
			PRINTLN(".KGM [Heist]: Ignore Heist Cost screen and move on. There is no charge for this Heist: $", g_sPreHeistDownloadData.phddUpfrontLeaderCost)
		ENDIF
	ENDIF

	// KGM 26/2/15 [BUG 2250537]: If there is a STAT save problem then drop into the standard routines to handle it
	IF (ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE())
		PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Display_Heist_Cost() - Game is having trouble saving heist progress. Go back to 'player input' stage to handle this.")
		
		// Give player back control
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		PRINTLN(".KGM [Heist]: Clearing the 'Clear Tasks' flag. Game is having trouble saving heist progress.")
		g_sLocalMPHeistControl.lhcClearTasksOnAlert = FALSE
		
		// Move back to Player Input stage
		Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT)
		
		EXIT		
	ENDIF
	
	// If we reach here and the player still needs to Clear Tasks, then do it now
	IF (g_sLocalMPHeistControl.lhcClearTasksOnAlert)
		// Clear the player's tasks
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS)	
		PRINTLN(".KGM [Heist]: Clearing Player's Tasks. Moving on from 'heist cost' alert screen.")
		g_sLocalMPHeistControl.lhcClearTasksOnAlert = FALSE
	ENDIF
		
	// BUG 1868761 - the 'message' radius is larger than the corona radius, so warping the player into the centre as a quick fix.
	//				 May need to change this - the warp will be spotted by other players in the leader's apartment.
	VECTOR coronaLocation = Get_Heist_Corona_Coords_For_Players_Currently_Occupied_Apartment()
	coronaLocation.z += 0.7
	GET_GROUND_Z_FOR_3D_COORD(coronaLocation, coronaLocation.z)
	SET_ENTITY_COORDS(PLAYER_PED_ID(), coronaLocation)
		
	// KGM 10/2/15 [BUG 2226481]: Don't perform these animation updates if a Replay from the Board has been selected
	IF NOT (g_replayHeistIsFromBoard)
		MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimFirst, DEFAULT, DEFAULT, TRUE, DEFAULT, TRUE)
		MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond, TRUE, DEFAULT, DEFAULT, DEFAULT, TRUE)
	ENDIF
	
	// Move on to the next stage
	Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_SETUP_HEIST_CUTSCENE_CORONA)

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Heist Stage: Setup Heist Cutscene Corona
PROC Maintain_MP_Heist_Stage_Setup_Heist_Cutscene_Corona()
	
	// Make sure the player doesn't get in the way of the camera.
	SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	// KGM 9/2/15 [BUG 2226481]: If the player has chosen a replay from the board, then wait for the Prep Missions to download if the Tutorial Heist was chosen
	//		NOTE: Because a replay from the board skips stages, the Preps Download is still in progress at this stage so the R* Created array locations 500+ won't have the data
	IF (g_replayHeistIsFromBoard)
		IF NOT (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
			PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Setup_Heist_Cutscene_Corona() - Heist Replay from Board waiting for Prep Missions download to prevent download clash. Still waiting.")
			EXIT
		ENDIF
	ENDIF
		
	// Allow debug cancelling of a Heist Strand (this will be set by Numpad8 routine)
	#IF IS_DEBUG_BUILD
		IF (Check_For_Heist_Debug_Cancel())
			PRINTLN("...KGM MP [ActSelect][Heist]: Heist Strand debug cancelled during HEIST_CLIENT_STAGE_SETUP_HEIST_CUTSCENE_CORONA")
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Required_Initial_Delay_Between_Heists())
			EXIT
		ENDIF
	#ENDIF
	
	IF NOT GET_HEIST_CORONA_ACTIVE()
		INT tempLoop = 0
		REPEAT g_numLocalMPHeists tempLoop
			IF (g_sLocalMPHeists[tempLoop].lhsHashFinaleRCID = Get_Current_Heist_Control_Hash_HeistRCID())
				// Found the Heist Finale, so setup the Cutscene corona
				BOOL isCoronaSetup = FALSE
				
				// The tutorial should use the first Prep mission as the cutscene corona so that the mission launches immediately after the cutscene
				// For the other heists, the Finale corona should be used because the mission won't launch - instead the player will be placed on the planning board after the cutscene
				IF (g_sPreHeistDownloadData.phddIsTutorial)
					// The Tutorial Heist should still set the 'Intro' cutscene flag to TRUE and the 'Tutorial' flag to TRUE, but set the 'Finale' flag to FALSE
					PRINTLN("...KGM MP [ActSelect][Heist]: Tutorial. Using first Prep Mission for Cutscene Corona: ", g_sPreHeistDownloadData.phddPrepMissions[0].rootContentID)
					isCoronaSetup = Setup_My_Heist_Corona_From_ContentID(g_sPreHeistDownloadData.phddPrepMissions[0].rootContentID, FALSE, TRUE, FALSE, TRUE)
				ELSE
					// A standard Heist intro cutscene should set the 'Intro' cutscene flag to TRUE and the 'Finale' flag to TRUE - it will use the 'finale' corona as a trigger
					PRINTLN("...KGM MP [ActSelect][Heist]: Found Heist Finale ContentID to be used for Cutscene Corona: ", g_sLocalMPHeists[tempLoop].lhsMissionIdData.idCloudFilename)
					isCoronaSetup = Setup_My_Heist_Corona_From_ContentID(g_sLocalMPHeists[tempLoop].lhsMissionIdData.idCloudFilename, TRUE, TRUE)
				ENDIF
				
				IF (isCoronaSetup)
					PRINTLN("[AMEC][HEIST_ANIMS] - Maintain_MP_Heist_Stage_Setup_Heist_Cutscene_Corona - Heist corona details successfully stored.")
					SET_HEIST_CORONA_ACTIVE(TRUE)
					g_HeistPrePlanningClient.bHeistInitStrandCorona = TRUE
				ELSE
					// Failed to setup the corona, so for now, moving back to the 'no heist' state
					PRINTLN("...KGM MP [ActSelect][Heist]: Failed to setup Heist Cutscene Corona")
					SCRIPT_ASSERT("Maintain_MP_Heist_Stage_Setup_Heist_Cutscene_Corona(): ERROR - Failed to setup Heist Cutscene corona")
					Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Heist_Quick_Retry_Initial_Delay())
					SET_HEIST_SUPPRESS_HUD_FOR_PLANNING(FALSE)
					SET_HEIST_CORONA_FROM_PLANNING_BOARD(FALSE)
					CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimFirst)
					CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimSecond)
					CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimThird, DEFAULT, DEFAULT, TRUE)
					STOP_ALL_CORONA_FX_IN()
					
					PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Setup_Heist_Cutscene_Corona() - isCoronaSetup = FALSE, clearing replay flag: g_replayHeistIsFromBoard")
					g_replayHeistIsFromBoard = FALSE
					EXIT
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF

	// KGM 10/2/15 [BUG 22226481]: If this was a Replay from the Board, then ignore all the animation stuff below then clear the 'replay from board' flag once corona setup
	IF (g_replayHeistIsFromBoard)
		IF (GET_HEIST_CORONA_ACTIVE())
			PRINTLN("[AMEC][HEIST_ANIMS] - Maintain_MP_Heist_Stage_Setup_Heist_Cutscene_Corona - bHeistCoronaActive = TRUE, move to wait for active strand (Replay from Board).")
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_ACTIVE)
			SET_HEIST_CORONA_ACTIVE(FALSE)
		ENDIF
		
		EXIT
	ENDIF
	
	IF NOT HAS_HEIST_STRAND_ANIM_FINISHED(g_HeistSharedClient.sHeistAnimFirst)
	
		SUPPRESS_FRONTEND_RENDERING_THIS_FRAME()
	
		MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimFirst, DEFAULT, DEFAULT, TRUE, DEFAULT, TRUE)
		
		IF g_HeistSharedClient.sHeistAnimFirst.bAtLastFrame
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond, FALSE, TRUE, DEFAULT, DEFAULT, TRUE)
		ELSE
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond, TRUE, TRUE, DEFAULT, DEFAULT, TRUE)
		ENDIF
		
		MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimThird, TRUE, DEFAULT, DEFAULT, DEFAULT, TRUE)
		
		EXIT
		
	ELSE
	
		IF NOT g_HeistSharedClient.sHeistAnimFirst.bBasicCleanupFinished
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimFirst, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
		ENDIF

		MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond, FALSE, TRUE, DEFAULT, DEFAULT, TRUE)
		
		IF NOT g_HeistSharedClient.sHeistAnimSecond.bAtLastFrame
			SUPPRESS_FRONTEND_RENDERING_THIS_FRAME()
		ENDIF
		
		IF HAS_HEIST_STRAND_ANIM_FINISHED(g_HeistSharedClient.sHeistAnimSecond)
		OR g_HeistSharedClient.sHeistAnimSecond.bAtLastFrame
		OR NETWORK_IS_ACTIVITY_SESSION()
			
			IF NETWORK_IS_ACTIVITY_SESSION()
				PRINTLN("[AMEC][HEIST_ANIMS] - Maintain_MP_Heist_Stage_Setup_Heist_Cutscene_Corona - ERROR: NETWORK_IS_ACTIVITY_SESSION = TRUE! dropping to wait for strand active.")
			ELSE
				PRINTLN("[AMEC][HEIST_ANIMS] - Maintain_MP_Heist_Stage_Setup_Heist_Cutscene_Corona - Finished intro anims, moving to idle.")
			ENDIF
			
			IF ANIMPOSTFX_IS_RUNNING("MenuMGHeistIntro")
				PRINTLN("[AMEC][HEIST_ANIMS] - Maintain_MP_Heist_Stage_Setup_Heist_Cutscene_Corona - Stopping heist PostFX: MenuMGHeistIntro.")
				ANIMPOSTFX_STOP("MenuMGHeistIntro")
			ENDIF
			
			IF NOT ANIMPOSTFX_IS_RUNNING("MenuMGHeistIn")
				PRINTLN("[AMEC][HEIST_ANIMS] - Maintain_MP_Heist_Stage_Setup_Heist_Cutscene_Corona - Triggering heist PostFX: MenuMGHeistIn.")
				ANIMPOSTFX_PLAY("MenuMGHeistIn", 0, TRUE)
				TOGGLE_RENDERPHASES(FALSE, TRUE)
			ENDIF

			IF GET_HEIST_CORONA_ACTIVE()
				PRINTLN("[AMEC][HEIST_ANIMS] - Maintain_MP_Heist_Stage_Setup_Heist_Cutscene_Corona - bHeistCoronaActive = TRUE, move to wait for active strand.")
				Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_ACTIVE)
				SET_HEIST_CORONA_ACTIVE(FALSE)

				CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimFirst, FALSE, FALSE) // Don't remove assets, we need to maintain them for the background animation.
				CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimSecond, TRUE, FALSE)		//	Changed cam cleanup to true for bug fix on: 2138598 RowanJ - Ensures 2nd cam cleaned on corona active.
				MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimThird, FALSE, FALSE, DEFAULT, TRUE, TRUE)
	
			ELSE
				PRINTLN("[AMEC][HEIST_ANIMS] - Maintain_MP_Heist_Stage_Setup_Heist_Cutscene_Corona - bHeistCoronaActive = FALSE but heist intro anim has finished, wait in idle.")
				IF ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimThird)
					MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimThird, FALSE, FALSE, DEFAULT, TRUE, TRUE)
				ENDIF
				
			ENDIF
			
		ENDIF

	ENDIF
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Heist Stage: Wait For Heist Strand Active
//
// INPUT PARAMS:			paramHeistFinaleRCID		Heist Finale RootContentID
PROC Maintain_MP_Heist_Stage_Wait_For_Heist_Strand_Active()
	
	#IF IS_DEBUG_BUILD
	IF NOT g_DEBUG_quickLaunchPlanning
	#ENDIF
	
//	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	IF ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimFirst)
		CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimFirst, FALSE)
		MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimFirst, FALSE, FALSE, FALSE, FALSE, TRUE)
	ENDIF
	
	IF ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimSecond)
		CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimSecond, FALSE)
		MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond, FALSE, FALSE, FALSE, FALSE, TRUE)
	ENDIF
	
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE)
		IF ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimThird)
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimThird, FALSE, FALSE, DEFAULT, TRUE, TRUE)
		ENDIF
	ELSE
		IF ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimThird)
			PRINTLN("[AMEC][HEIST_ANIMS] - Maintain_MP_Heist_Stage_Wait_For_Heist_Strand_Active - IS_PLAYER_IN_PROPERTY = FALSE, clean up idle anim.")
			CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimThird)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	
	ELSE
	
		IF NOT g_HeistPrePlanningClient.DEBUG_bQuickPlanning
			g_HeistPrePlanningClient.DEBUG_bQuickPlanning = TRUE
			g_HeistPrePlanningClient.DEBUG_tlHeistFinaleRootContID = Get_Finale_RootContentID_From_Hash(Get_Current_Heist_Control_Hash_HeistRCID())
			PRINTLN(".KGM [Heist]: g_DEBUG_quickLaunchPlanning: Saving finale rootContentID for trigger, value: ", g_HeistPrePlanningClient.DEBUG_tlHeistFinaleRootContID)
		ENDIF
	
	ENDIF
	
	#ENDIF
	
	// Allow debug cancelling of a Heist Strand (this will be set by Numpad8 routine)
	#IF IS_DEBUG_BUILD
		IF (Check_For_Heist_Debug_Cancel())
			// If the Heist Strand has activated, then move on to variables reset
			IF (IS_MP_HEIST_STRAND_ACTIVE())
				PRINTLN(".KGM [Heist]: Heist Strand debug cancelled during HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_ACTIVE - Strand had just activated")
				Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_HEIST_VARIABLES_RESET)
				EXIT
			ENDIF
			
			PRINTLN(".KGM [Heist]: Heist Strand debug cancelled during HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_ACTIVE - cutscene corona should get deleted if active")
			
			// Strand still hasn't activated, so delete the cutscene corona if active
			Delete_My_Stored_Corona_If_Available()
			
			// Just in case the strand isn't marked as active when the STATs first get stored, clear teh boradcast copy of the STATS
			Clear_Current_Heist_Planning_Data_For_Broadcast()
			
			// NOTE: Numpad8 is now non-functioning if the player is in a corona or on a mission so it should be safe to delete the corona if it exists
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Required_Initial_Delay_Between_Heists())
			EXIT
		ENDIF
	#ENDIF

	IF NOT (IS_MP_HEIST_STRAND_ACTIVE())
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_23 storedFinaleRootContID = GET_HEIST_FINALE_STAT_DATA()
	
		TEXT_LABEL_63 storedHeistDescription = TEMP_Debug_Get_Heist_Description_From_FinaleRCID(storedFinaleRootContID)
		TEXT_LABEL_23 finaleRCID = Get_Finale_RootContentID_From_Hash(Get_Current_Heist_Control_Hash_HeistRCID())
		TEXT_LABEL_63 expectedHeistDescription = TEMP_Debug_Get_Heist_Description_From_FinaleRCID(finaleRCID)
		
		IF (ARE_STRINGS_EQUAL(finaleRCID, storedFinaleRootContID))
			// The Correct Heist Strand is now active
			PRINTLN(".KGM [Heist]: HEIST STRAND ACTIVE: Finale RootContentID is now setup in the Heist Stats: ", finaleRCID, "[", expectedHeistDescription, "]")
		ELSE
			// A different Heist is setup, so something must have gone wrong somewhere - but run with it
			PRINTLN(".KGM [Heist]: Trying to setup this Heist Strand: ", expectedHeistDescription, ", but instead this Heist strand is setup: ", storedHeistDescription)
			SCRIPT_ASSERT("Maintain_MP_Heist_Stage_Wait_For_Heist_Strand_Active() - The wrong Heist Strand is setup. Tell Keith.")
		ENDIF
		
		#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Heist]: HEIST STRAND ACTIVE: Resetting debug variable: g_DEBUG_quickLaunchPlanning.")
		g_DEBUG_quickLaunchPlanning = FALSE
		#ENDIF
		
	#ENDIF
	
	// If a Heist Strand is active then the Tutorial Help text won't be needed again
	BOOL markAsAllSeen = TRUE
	Control_Tutorial_Heist_Help_Text_Seen(markAsAllSeen)
	
	// If the 'next heist soon' help text hasn't been displayed then clear it out - no longer needed
	Clear_Another_Heist_Soon_Help_Text_Controls()
	
	// KGM 2/2/15 [BUG 2220501]: Clear the 'next heist phonecall soon' STAT
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_NEXT_HEIST_QUICK_PHONECALL))
		CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_NEXT_HEIST_QUICK_PHONECALL)
		PRINTLN(".KGM [Heist]: HEIST STRAND ACTIVE: Clearing the 'next heist phonecall soon' flag.")
	ENDIF
	
	Store_Current_Heist_Planning_Data_For_Broadcast()
	Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_COMPLETE)

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Heist Stage: Wait For Heist Strand Completion
PROC Maintain_MP_Heist_Stage_Wait_For_Heist_Strand_Complete()
	
	// Ignore debug cancelling of a Heist Strand (as set by Numpad8 routine) if strand is active - this will be automatically cleaned up when the stats get cleaned up
	#IF IS_DEBUG_BUILD
		IF (Check_For_Heist_Debug_Cancel())
			PRINTLN("...KGM MP [ActSelect][Heist]: IGNORE: Heist Strand debug cancelled during HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_COMPLETE - should get auto-cleaned up when Heists Stats are cleaned up")
			EXIT
		ENDIF
	#ENDIF

	// Only class as complete if both the Stats are cleaned up and the mission has finished.
	IF (IS_MP_HEIST_STRAND_ACTIVE())
	OR (Is_There_A_MissionsAtCoords_Focus_Mission())
		// KGM 2/8/14: Check if the Player Broadcast data needs refilled - this may be needed after a transition
		Refill_Current_Heist_Planning_Data_For_Broadcast_If_Required()
		EXIT
	ENDIF
	
	PRINTLN("...KGM MP [ActSelect][Heist]: HEIST STRAND NO LONGER ACTIVE: Allowing the Heist Control variables to be cleaned up")
	
	Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_HEIST_VARIABLES_RESET)

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	General Heist Strand Reset routines - used by both completion and when cancelled by Lester phonecall
PROC Heist_Strand_General_Reset()
	
	// Delete any heist corona - this may be needed if the Heist Strand has been abandoned
	Delete_My_Stored_Corona_If_Available()
	
	// Clear the player broadcast data that allowed other players to get a copy of this player's Heist Planning missions
	Clear_Current_Heist_Planning_Data_For_Broadcast()
	
	// Force the Rockstar Created array location 500+ holding variable to be reset to enforce a Planning Missions header data reload for the next strand
    // KGM NOTE: Mainly required for debug launching the same Heist strand again to ensure the descriptions get reloaded
    g_sLocalMPHeistControl.lhcPrepsDownloadedIn500	= FALSE
	g_sLocalMPHeistControl.lhcDownloadAttempts		= 0
    PRINTLN(".KGM MP [ActSelect][Heist][HCorona]: Force resetting g_sLocalMPHeistControl.lhcPrepsDownloadedIn500 to FALSE when strand complete")
	
	// Clear the 'replay flow order' variable
	g_replayHeistFlowOrder = UNKNOWN_HEIST_FLOW_ORDER
	g_replayHeistIsFromBoard = FALSE
	
	// Clear the 'numpad7 launch' flag
	#IF IS_DEBUG_BUILD
		g_DEBUG_UseNumpad7Heist = FALSE
	#ENDIF
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Heist Stage: Variables Reset
PROC Maintain_MP_Heist_Stage_Variables_Reset()
	
	// Ignore debug cancelling of a Heist Strand (as set by Numpad8 routine)
	#IF IS_DEBUG_BUILD
		IF (Check_For_Heist_Debug_Cancel())
			PRINTLN("...KGM MP [ActSelect][Heist]: IGNORE: Heist Strand debug cancelled during HEIST_CLIENT_STAGE_HEIST_VARIABLES_RESET - already cleaning up")
			EXIT
		ENDIF
	#ENDIF
	
	PRINTLN(".KGM MP [ActSelect][Heist][HCorona]: HEIST_CLIENT_STAGE_HEIST_VARIABLES_RESET - so delete any active Heist corona")
	
	// The general reset routine used on both Completion and Abandoned through Lester
	Heist_Strand_General_Reset()
	
// KGM 24/10/12 [BUG 2089694]: Moved this to FINISH_CURRENT_HEIST_STRAND() to ensure the updated STATs get saved out in case of a crash during Finale end cutscene
//	// Unlock any post-heist rewards
//	Unlock_Heist_Reward_On_Heist_Strand_Completion(Get_Current_Heist_Control_Hash_HeistRCID())

// KGM 30/10/14: FINISH_CURRENT_HEIST_STRAND() will also update the Heist Flow Progression STAT
	
	// Activate a replay delay for this Heist, to prevent calling Lester to replay this one for a while
	Set_Replay_Delay_For_Completed_Heist_Strand(Get_Current_Heist_Control_Hash_HeistRCID())
	
// KGM 15/11/14: This is now done after heist completion phonecall has ended
//	// Activate any 'another heist will be given soon' help text that needs displayed
//	Activate_Another_Heist_Soon_Help_Text()
	
	// Remove any cooldown period that was set when the player completed a heist-related mission - we don't want to delay the next strand after a proper strand completion
	Clear_New_Heist_Strand_Cooldown_Period()
	
	// KGM 10/2/15 [BUG 2231896]: Force a quick 'replayable heists' update
	g_sReplayableHeists.rhTimeout = GET_GAME_TIMER() + REPLAYABLE_HEIST_QUICK_DATA_GATHERING_TIMEOUT_msec
	PRINTLN(".KGM [Heist]: Maintain_MP_Heist_Stage_Variables_Reset() - REPLAYS CHECK - HEIST FINISHED - SETTING QUICK UPDATE")

	Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Required_Initial_Delay_Between_Heists())

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check for the Heist Strand being cancelled by a phonecall to Lester, and clean up the strand
PROC Check_For_Heist_Strand_Cancelled_Through_Lester()

	IF NOT (Has_Current_Heist_Strand_Been_Cancelled_By_Player())
		EXIT
	ENDIF
	
	g_eHeistStagesClient currentStage = Get_Current_Heist_Control_Stage()

	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Heist][Cancel]: Check_For_Heist_Strand_Cancelled_Through_Lester() - Current Stage: ", Convert_Client_Heist_Stage_To_String(currentStage))
	#ENDIF

	// May need to do different things depending on the current Heist Stage
	SWITCH (currentStage)
		// Heist isn't setup, so just change the stage (shouldn't be possible to cancel through Lester at these stages anyway)
		CASE HEIST_CLIENT_STAGE_NO_HEIST
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY
		CASE HEIST_CLIENT_STAGE_WAIT_UNTIL_COMMS_ALLOWED
		CASE HEIST_CLIENT_STAGE_REQUEST_COMMS
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Required_Initial_Delay_Between_Heists())
			BREAK
			
		// Heist mission download is in progress so delay until finished (shouldn't be possible to cancel through Lester at this stage anyway)
		CASE HEIST_CLIENT_STAGE_DOWNLOAD_DETAILS_FROM_FINALE
			PRINTLN(".KGM [Heist][Cancel]:    - Delaying Cancel until Download Complete.")
			EXIT
		
		// Heist Comms is in progress so delay until finished (shouldn't be possible to cancel through Lester at this stage anyway)
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_COMMS_TO_END
			IF (Is_MP_Comms_Still_Playing())
				PRINTLN(".KGM [Heist][Cancel]:    - Delaying Cancel until MP Comms Complete.")
				EXIT
			ENDIF
			
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Required_Initial_Delay_Between_Heists())
			BREAK

		// Heist Comms waiting for player input, so clean up any context button message or warning messages
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT
			PRINTLN(".KGM [Heist][Cancel]: Check_For_Heist_Strand_Cancelled_Through_Lester() - Clearing Player Input Button Context or related On-Screen warnings")
			Clear_Player_Start_Input_Context_Button_Or_Warnings()
			Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Required_Initial_Delay_Between_Heists())
			BREAK
			
		// Heist Cost message on display, so ignore request (shouldn't be possible to cancel through Lester at this stage anyway)
		CASE HEIST_CLIENT_STAGE_DISPLAY_HEIST_COST
			PRINTLN(".KGM [Heist][Cancel]:    - Ignoring Cancel. Heist Cost screen is on display.")
			BREAK
		
		// Setting up Heist Cutscene Corona, so ignore request (shouldn't be possible to cancel through Lester at this stage anyway)
		CASE HEIST_CLIENT_STAGE_SETUP_HEIST_CUTSCENE_CORONA
			PRINTLN(".KGM [Heist][Cancel]:    - Ignoring Cancel. Setting Up Heist Cutscene Corona.")
			BREAK
		
		// Waiting for Heist Strand Active, so ignore request (shouldn't be possible to cancel through Lester at this stage anyway)
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_ACTIVE
			PRINTLN(".KGM [Heist][Cancel]:    - Ignoring Cancel. Waiting for Heist Strand Active.")
			BREAK
		
		// Do a complete cancellation of the active Heist Strand
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_COMPLETE
			IF (IS_MP_HEIST_STRAND_ACTIVE())
				// Request that Alastair's routines abandon the current Heist Strand (I can't call ABANDON_CURRENT_HEIST_STRAND() directly)
				PRINTLN(".KGM [Heist][Cancel]:    - Need Alastair's stuff to cleanup. Setting g_cleanupHeistStrandStats_AlastairStuff = TRUE.")
				g_cleanupHeistStrandStats_AlastairStuff = TRUE
				
				PRINTLN(".KGM [Heist][Cancel]:    - Call the general reset when cancelling up a Heist Strand")
				Heist_Strand_General_Reset()
				Set_Current_Heist_Control_Stage(HEIST_CLIENT_STAGE_NO_HEIST, Get_Required_Initial_Delay_Between_Heists())
			ELSE
				PRINTLN(".KGM [Heist][Cancel]:    - Ignoring Cancel. Heist Strand isn't active - probably edge case for timing.")
			ENDIF
						
			BREAK
			
		// Already resetting the Heist variables, so ignore request - the strand is finished anyway
		CASE HEIST_CLIENT_STAGE_HEIST_VARIABLES_RESET
			PRINTLN(".KGM [Heist][Cancel]:    - Ignoring Cancel. The strand is already finished and resetting.")
			BREAK

		DEFAULT
			#IF IS_DEBUG_BUILD
				PRINTLN("...KGM [Heist][Cancel]: Check_For_Heist_Strand_Cancelled_Through_Lester() - UNKNOWN HEIST CLIENT STAGE.")
				SCRIPT_ASSERT("Check_For_Heist_Strand_Cancelled_Through_Lester() - ERROR: Trying to process Unknown Heist Client Stage. Add to Switch statement. Tell Keith")
			#ENDIF
			
			BREAK
	ENDSWITCH
	
	// Cancel any Quick Heist invite
	PRINTLN(".KGM [Heist][Cancel]: Check_For_Heist_Strand_Cancelled_Through_Lester() - Cancelling any Heist Quick Invite (FMMC_TYPE_HEIST_PHONE_INVITE)")
	Cancel_Any_Basic_Invite_Of_Type(FMMC_TYPE_HEIST_PHONE_INVITE)
	
	// KGM 3/2/15 [BUG 2220501]: If the Heist is cancelled through Lester, ensure the 'next heist phonecall soon' STAT gets cleared too
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_NEXT_HEIST_QUICK_PHONECALL))
		CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_NEXT_HEIST_QUICK_PHONECALL)
		PRINTLN(".KGM [Heist]: Check_For_Heist_Strand_Cancelled_Through_Lester(): Clearing the 'next heist phonecall soon' flag.")
	ENDIF
	
	// Clear out the 'replay' flow order value
	g_replayHeistFlowOrder = UNKNOWN_HEIST_FLOW_ORDER
	g_replayHeistIsFromBoard = FALSE
	
	// KGM 10/2/15 [BUG 2231896]: Force a quick 'replayable heists' update
	g_sReplayableHeists.rhTimeout = GET_GAME_TIMER() + REPLAYABLE_HEIST_QUICK_DATA_GATHERING_TIMEOUT_msec
	PRINTLN(".KGM [Heist]: Check_For_Heist_Strand_Cancelled_Through_Lester() - REPLAYS CHECK - HEIST CANCELLED THROUGH LESTER - SETTING QUICK UPDATE")
	
	// Clear the 'cancel' flag
	g_cancelHeistStrandByLester_KeithStuff = FALSE
	PRINTLN(".KGM [Heist][Cancel]: Check_For_Heist_Strand_Cancelled_Through_Lester() - g_cancelHeistStrandByLester_KeithStuff = FALSE")

	// JA (10/2/15): Force an update to our heist planning boards due to a change in state, this is essentially the same as AlastairStuff flag but for all cancels
	SET_HEIST_UPDATE_AVAILABLE(TRUE)

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Heist Stages
PROC Maintain_MP_Selector_Group_Current_Heist_Stage()

	//	Reset this bool to false to ensure it's reset- checked each frame each frame.
	IF g_bIsHeistStageCommsActive
		g_bIsHeistStageCommsActive = FALSE
	ENDIF
	
	// KGM 15/12/14 [BUG 2135341]: Allow an active Heist Strand to be cancelled.
	// NOTE: 2 options, handle it all within a function, or handle it individually in the functions below - initially decided to handle it all within one function.
	Check_For_Heist_Strand_Cancelled_Through_Lester()

	SWITCH (Get_Current_Heist_Control_Stage())
		CASE HEIST_CLIENT_STAGE_NO_HEIST
			Maintain_MP_Heist_Stage_No_Heist()
			BREAK
			
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY
			Maintain_MP_Heist_Stage_Wait_For_Initial_Delay_Expiry()
			BREAK
			
		CASE HEIST_CLIENT_STAGE_DOWNLOAD_DETAILS_FROM_FINALE
			Maintain_MP_Heist_Stage_Download_Details_From_Finale()
			BREAK
			
		CASE HEIST_CLIENT_STAGE_WAIT_UNTIL_COMMS_ALLOWED
			// Stay in this stage until Dave's routines allow the Comms to trigger
			Maintain_MP_Heist_Stage_Wait_Until_Comms_Allowed()
			BREAK
			
		CASE HEIST_CLIENT_STAGE_REQUEST_COMMS
			PRINTLN(".KGM MP [ActSelect][Heist]: HEIST_CLIENT_STAGE_REQUEST_COMMS")
			Maintain_MP_Heist_Stage_Request_Comms()
			BREAK
			
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_COMMS_TO_END
			Maintain_MP_Heist_Stage_Wait_For_Comms_To_End()
			BREAK
			
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT
			Maintain_MP_Heist_Stage_Wait_For_Player_Start_Input()
			BREAK
			
		CASE HEIST_CLIENT_STAGE_DISPLAY_HEIST_COST
			Maintain_MP_Heist_Stage_Display_Heist_Cost()
			BREAK
			
		CASE HEIST_CLIENT_STAGE_SETUP_HEIST_CUTSCENE_CORONA
			Maintain_MP_Heist_Stage_Setup_Heist_Cutscene_Corona()
			BREAK
			
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_ACTIVE
			Maintain_MP_Heist_Stage_Wait_For_Heist_Strand_Active()
			BREAK
			
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_COMPLETE
			Maintain_MP_Heist_Stage_Wait_For_Heist_Strand_Complete()
			BREAK
			
		CASE HEIST_CLIENT_STAGE_HEIST_VARIABLES_RESET
			Maintain_MP_Heist_Stage_Variables_Reset()
			BREAK
			
		DEFAULT
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [ActSelect][Heist]: UNKNOWN HEIST CLIENT STAGE.") NET_NL()
				SCRIPT_ASSERT("Maintain_MP_Selector_Group_Client_Heist_Stage() - ERROR: Trying to process Unknown Heist Client Stage. Add to Switch statement. Tell Keith")
			#ENDIF
			
			BREAK
	ENDSWITCH

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	When a player is in their own apartment without an active heist, maintain a list of currently replayable heists in playerBD
// NOTES:	This is used to populate the Replayable Heists board and also keep other player's board in sync when they are in the Leader apartment.
PROC Maintain_My_Replayable_Heist_Strands()

	// Am I in my own apartment suitable for Heists?
	BOOL inOwnApartment = Is_Players_Currently_Occupied_Own_Apartment_Suitable_For_Heists()
	IF NOT (inOwnApartment)
		// ...no, so nothing to do
		g_sReplayableHeists.rhInApartment = FALSE
		EXIT
	ENDIF
	
	// In Own Apartment
	// If player is newly in own apartment force do a quick replayable heists update
	IF NOT (g_sReplayableHeists.rhInApartment)
		// ...newly in own apartment, so do a quick timeout
		g_sReplayableHeists.rhInApartment = TRUE
		g_sReplayableHeists.rhTimeout = GET_GAME_TIMER() + REPLAYABLE_HEIST_QUICK_DATA_GATHERING_TIMEOUT_msec
		PRINTLN(".KGM [Heist]: Maintain_My_Replayable_Heist_Strands() - REPLAYS CHECK - JUST ENTERED OWN APARTMENT - SETTING QUICK UPDATE")
		EXIT
	ENDIF
	
	// Been in own apartment for a while, does the data need updated?
	IF (GET_GAME_TIMER() < g_sReplayableHeists.rhTimeout)
		// ...not yet
		EXIT
	ENDIF
	
	// Timeout has occurred. Delay if the initial cloud download hasn't yet occurred - the data will not be ready
	IF NOT (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
		g_sReplayableHeists.rhTimeout = GET_GAME_TIMER() + REPLAYABLE_HEIST_QUICK_DATA_GATHERING_TIMEOUT_msec
		PRINTLN(".KGM [Heist]: Maintain_My_Replayable_Heist_Strands() - REPLAYS CHECK - SHORT DELAY - Waiting for initial cloud data to be available")
		EXIT
	ENDIF
	
	// Create the next timeout time
	g_sReplayableHeists.rhTimeout = GET_GAME_TIMER() + REPLAYABLE_HEIST_DATA_GATHERING_TIMEOUT_msec
	
	// Nothing to do if there is already a Heist available in the apartment
	IF (Is_There_A_Heist_Available_In_Apartment())
		// ...there is a heist available
		PRINTLN(".KGM [Heist]: Maintain_My_Replayable_Heist_Strands() - IGNORE (Heist available)")
		EXIT
	ENDIF
	
	// Gather the details, so clear the current details
	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bitsetReplayableHeists = 0
	
	// Check which heists have been completed as Leader and haven't been recently completed
	INT tempLoop = 0
	INT bitHeistStrand = 0
	INT hashFinaleRCID = 0
	
	PRINTLN(".KGM [Heist]: Maintain_My_Replayable_Heist_Strands() - Gather Replayable Heist details (alphabetical order):")
	
	REPEAT g_numLocalMPHeists tempLoop
		IF (g_sLocalMPHeists[tempLoop].lhsCompletedAsLeader)
			IF NOT (Is_Heist_Strand_Recently_Completed(tempLoop))
				// ...this heist is available for replay, so get the heist is flow order
				hashFinaleRCID = g_sLocalMPHeists[tempLoop].lhsHashFinaleRCID
				bitHeistStrand = Get_Heist_Strand_Flow_Value_From_Finale_Hash_RCID(hashFinaleRCID)
				SET_BIT(GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bitsetReplayableHeists, bitHeistStrand)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [Heist]: ...", tempLoop, " - AVAILABLE.  Flow Order: ", bitHeistStrand, "  RCID: ", hashFinaleRCID)
				#ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		IF (GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bitsetReplayableHeists = 0)
			PRINTLN(".KGM [Heist]: ...NONE")
		ENDIF
	#ENDIF
	
	// Update the F9 screen debug variable
	#IF IS_DEBUG_BUILD
		g_DEBUG_F9BoardReplayBits = ""
		REPEAT g_numLocalMPHeists tempLoop
			IF (IS_BIT_SET(GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bitsetReplayableHeists, tempLoop))
				g_DEBUG_F9BoardReplayBits += "Y"
			ELSE
				g_DEBUG_F9BoardReplayBits += "n"
			ENDIF
		ENDREPEAT
	#ENDIF

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
PROC Maintain_Heist_Testbeds()

//	// Heist Reward Testbed
//	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F9, KEYBOARD_MODIFIER_SHIFT, "test")
//		PRINTLN(".KGM [Heist][SHIFT-F9]: SHIFT-F9 pressed: Heist Reward Unlocks testbed")
//		g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks = HEIST_UNLOCKS_ALL_CLEAR
//		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_2_AVAILABLE)
//		CLEAR_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_SMS_SENT)
//		Set_Current_Heist_Reward_Stage(HEIST_REWARD_STAGE_NO_REWARD, 10000)
//	ENDIF

//	// Heist Versus Triggering from Pause Menu Help Text Testbed
//	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F9, KEYBOARD_MODIFIER_SHIFT, "test")
//		PRINTLN(".KGM [Heist][SHIFT-F9]: SHIFT-F9 pressed: Heist Versus Triggering from Pause Menu help text testbed")
//		g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks = HEIST_UNLOCKS_ALL_CLEAR
//		SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_ALLOW_DISPLAY_VERSUS_PAUSE_HELP)
//	ENDIF

//	// Heist Completion Phonecall Testbed
//	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F9, KEYBOARD_MODIFIER_SHIFT, "test")
//		PRINTLN(".KGM [Heist][SHIFT-F9]: SHIFT-F9 pressed: Heist Completion Phonecall testbed")
//		Store_Heist_Completion_Phonecall(Get_Tutorial_Heist_RCID_Hash())
//		Activate_Heist_Completion_Phonecall()
//	ENDIF

//	// Heist Replay Reminder Help Text Testbed
//	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F9, KEYBOARD_MODIFIER_SHIFT, "test")
//		PRINTLN(".KGM [Heist][SHIFT-F9]: SHIFT-F9 pressed: Heist Replay Reminder Help Text testbed")
//		Store_Heist_Replay_Reminder_Help(Get_Tutorial_Heist_RCID_Hash())
//		Activate_Heist_Replay_Reminder_Help()
//		g_sHeistHelpText.hhtHeistReplayReminderTimeout = GET_GAME_TIMER() + 30000
//	ENDIF

ENDPROC
#ENDIF	// IS_DEBUG_BUILD
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
PROC Maintain_Heist_Debug_Widgets()

	// Output Same-session Inviter gamerhandle each frame
	IF (g_DEBUG_widgetEachFrameOutputInvitorGH)
		PRINTLN(".KGM [Heist]: DEBUG WIDGET - g_DEBUG_widgetClearAllReplayTimers TRUE. Outputting g_sJoblistHeistApartmentDetails.ownerHandle (see logs):")
		DEBUG_PRINT_GAMER_HANDLE(g_sJoblistHeistApartmentDetails.ownerHandle)
	ENDIF

	// Clear all replay timers?
	IF NOT (g_DEBUG_widgetClearAllReplayTimers)
		EXIT
	ENDIF
	
	PRINTLN(".KGM [Heist]: DEBUG WIDGET - g_DEBUG_widgetClearAllReplayTimers requested. Clearing all Replay Delay timers.")
	g_DEBUG_widgetClearAllReplayTimers = FALSE
		
	INT	tempLoop	= 0
	
	REPEAT MAX_HEISTS_WITH_REPLAY_DELAYS tempLoop
		#IF IS_DEBUG_BUILD
			IF (g_sHeistReplayDelay[tempLoop].hrdDelayTimeout != 0)
				PRINTLN(".KGM [Heist]: ...Delay timer active. Clearing. FinaleHashRCID: ", g_sHeistReplayDelay[tempLoop].hrdFinaleHashRCID)
			ENDIF
		#ENDIF
		
		g_sHeistReplayDelay[tempLoop].hrdDelayTimeout	= 0
		g_sHeistReplayDelay[tempLoop].hrdFinaleHashRCID	= 0
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		Debug_Output_Active_Heist_Replay_Delays()
	#ENDIF
	
	g_sReplayableHeists.rhTimeout = GET_GAME_TIMER() + REPLAYABLE_HEIST_QUICK_DATA_GATHERING_TIMEOUT_msec
	PRINTLN(".KGM [Heist]: Maintain_Heist_Debug_Widgets() - REPLAYS CHECK - SETTING SHORT UPDATE DELAY - Widget used to clear any active delay timers")

ENDPROC
#ENDIF	// IS_DEBUG_BUILD
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Activity Selector Heist Group Client
PROC Maintain_MP_Activity_Selector_Heist_Group_Client()

	// KGM 12/5/14: Maintain the Heists - Client

	// Testbeds?
	#IF IS_DEBUG_BUILD
		Maintain_Heist_Testbeds()
		Maintain_Heist_Debug_Widgets()
	#ENDIF
	
	// KGM 21/2/15 [BUG 2246472]: Update the 'replay board active' flag?
	IF (g_updateFlag_HasBoardBeenUnlocked)
		g_updateFlag_HasBoardBeenUnlocked = FALSE
		PRINTLN(".KGM [Heist]: g_updateFlag_HasBoardBeenUnlocked is TRUE. Updating status of the g_hasReplayBoardBeenUnlocked flag")
		Update_Optimisation_Global_Heist_Replay_Board_Unlocked()
	ENDIF
	
	Maintain_MP_Selector_Group_Current_Heist_Stage()
	Maintain_MP_Selector_Group_My_Heist_Corona()
	Maintain_MP_Selector_Group_Other_Heist_Coronas()
	Maintain_MP_Selector_Group_Heist_Rewards()
	Maintain_MP_Selector_Group_Heists_Not_Unlocked()
	Maintain_Heist_Completion_Phonecall()
	Maintain_Another_Heist_Soon_Help_Text()
	Maintain_Heist_Replay_Reminder_Help_Text()
	Maintain_Versus_Pause_Menu_Triggering_Help_Text()
	Maintain_My_Replayable_Heist_Strands()

ENDPROC
	// FEATURE_HEIST_PLANNING






// ===========================================================================================================
//      Functions required by Alastair to get Heists generically setup
// ===========================================================================================================

PROC TEMP_Force_Cleanup_Heist_Finale_Corona_With_ContentID(TEXT_LABEL_23 paramFinaleContentID)

	PRINTLN("...KGM MP [ActSelect][Heist]: TEMP_Force_Cleanup_Heist_Finale_Corona_With_ContentID(): ", paramFinaleContentID)
	
	Mark_MP_Mission_At_Coords_With_This_ContentID_For_Delete(paramFinaleContentID)
	
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

FUNC BOOL Is_This_ContentID_Available_As_A_Heist(TEXT_LABEL_23 paramHeistContentID)

	PRINTLN("...KGM MP [ActSelect][Heist]: Checking if this Heist Finale contentID is available: ", paramHeistContentID)
	
	// Find this contentID
	INT						slotLoop			= 0

	REPEAT g_numLocalMPHeists slotLoop
		IF (ARE_STRINGS_EQUAL(paramHeistContentID, g_sLocalMPHeists[slotLoop].lhsMissionIdData.idCloudFilename))
			PRINTLN("...KGM MP [ActSelect][Heist]: FOUND A MATCH - Heist Finale is available")
				
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC
	// FEATURE_HEIST_PLANNING
