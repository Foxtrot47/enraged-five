//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Neil McWilliam				Date: 18/09/19			    │
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						     Drill Minigame							            │
//│																				│
//│						Controls the Vault drill minigame                       │
//|			                            										│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛
//
USING "timer_public.sch"

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "net_include.sch"

USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "selector_public.sch"
USING "timer_public.sch"

USING "LineActivation.sch"

USING "MP_globals_arcade_cabinet_consts.sch"
USING "net_objective_text.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF

CONST_INT VAULT_DRILL_MINIGAME_STATE_INITIALISE						0
CONST_INT VAULT_DRILL_MINIGAME_STATE_CREATE_PROPS					1
CONST_INT VAULT_DRILL_MINIGAME_STATE_PLAY_INTRO						2
CONST_INT VAULT_DRILL_MINIGAME_STATE_HIDE_BAG_AND_WAIT_FOR_ANIM		3
CONST_INT VAULT_DRILL_MINIGAME_STATE_WAIT_FOR_INTRO					4
CONST_INT VAULT_DRILL_MINIGAME_STATE_DRILLING						5
CONST_INT VAULT_DRILL_MINIGAME_STATE_LASERING						6
CONST_INT VAULT_DRILL_MINIGAME_STATE_WAIT_FOR_OUTRO					7
CONST_INT VAULT_DRILL_MINIGAME_STATE_CLEANUP						8
CONST_INT VAULT_DRILL_MINIGAME_STATE_MESSEDUP						99
CONST_INT VAULT_DRILL_MINIGAME_STATE_FAILSAFE						100

CONST_INT VAULT_DRILL_BITSET_IS_IN_SWEET_SPOT						0
CONST_INT VAULT_DRILL_BITSET_IS_MINIGAME_COMPLETE					1
CONST_INT VAULT_DRILL_BITSET_DID_PLAYER_MESS_UP						2
CONST_INT VAULT_DRILL_BITSET_DID_PLAYER_MESS_UP_ON_LEFT_SIDE		4
CONST_INT VAULT_DRILL_BITSET_DID_PLAYER_BACK_OUT					5
CONST_INT VAULT_DRILL_BITSET_SKIP_WALK_TO_POSITION					6
CONST_INT VAULT_DRILL_BITSET_HAS_BROADCAST_ASSET_REQUEST			7
CONST_INT VAULT_DRILL_BITSET_USE_LASER								8
CONST_INT VAULT_DRILL_BITSET_RESERVED_DRILL							9
CONST_INT VAULT_DRILL_BITSET_RESERVED_BAG							10
CONST_INT VAULT_DRILL_BITSET_RESERVED_HOLE							11
CONST_INT VAULT_DRILL_BITSET_MESS_UP_TRIGGERED_FOR_TELEMETRY		12
CONST_INT VAULT_DRILL_BITSET_HAS_PLAYER_PASSED_MINIGAME				13
CONST_INT VAULT_DRILL_BITSET_HAS_PLAYER_SET_TO_BACK_OUT				14
CONST_INT VAULT_DRILL_BITSET_IS_BACK_OUT_COMPLETE					15
CONST_INT VAULT_DRILL_BITSET_SHOULD_SKIP_OUTRO						16
CONST_INT VAULT_DRILL_BITSET_DRILL_ABOVE_STANDARD_HEAT_LEVEL		17
CONST_INT VAULT_DRILL_BITSET_TRIGGER_REVEAL							18
CONST_INT VAULT_DRILL_BITSET_ALREADY_STARTED						19
CONST_INT VAULT_DRILL_BITSET_OVERHEAT_PENALTY_ACTIVE				20 		//	Allow overheat penalty to persist after the player has got control back.



CONST_FLOAT VAULT_DRILL_MAX_SWEET_SPOT_SIZE							0.25 
CONST_FLOAT VAULT_DRILL_MIN_SWEET_SPOT_SIZE							0.15
CONST_FLOAT VAULT_DRILL_MAX_PROGRESS_SPEED							0.025
CONST_FLOAT VAULT_DRILL_MIN_PROGRESS_SPEED							0.0
CONST_FLOAT VAULT_DRILL_COOLDOWN_SPEED								0.08
CONST_FLOAT VAULT_DRILL_OVERHEAT_SPEED								0.35 
CONST_FLOAT VAULT_LASER_OVERHEAT_SPEED								0.5
CONST_FLOAT VAULT_LASER_COOLDOWN_SPEED								0.3
CONST_FLOAT VAULT_DRILL_LEAN_INTERP_SPEED							0.2
CONST_FLOAT VAULT_DRILL_POS_SCALE_MODIFIER							0.82
CONST_FLOAT VAULT_DRILL_FIRST_OBSTACLE								0.33
CONST_FLOAT VAULT_DRILL_MAX_OBSTACLE_DIFF							0.445
CONST_FLOAT VAULT_DRILL_PASS_POINT									0.775
CONST_FLOAT VAULT_DRILL_START_POINT									0.08
CONST_FLOAT VAULT_DRILL_PIN_BREAK_POWER_MODIFER_MAX					0.5
CONST_FLOAT VAULT_DRILL_PIN_BREAK_POWER_MODIFIER_DECEL				0.4




/// PURPOSE: Manage drill minigame
///    
///    
CONST_INT VAULT_DRILL_STATE_INITIALISE			0
CONST_INT VAULT_DRILL_STATE_BEGIN				1
CONST_INT VAULT_DRILL_STATE_WAITING_FOR_ASSETS	2
CONST_INT VAULT_DRILL_STATE_STARTING_INTRO		3
CONST_INT VAULT_DRILL_STATE_REMOVE_BAG			4
CONST_INT VAULT_DRILL_STATE_WAITING_FOR_INTRO	5
CONST_INT VAULT_DRILL_STATE_DRILLING			6
CONST_INT VAULT_DRILL_STATE_WAITING_FOR_OUTRO	7
CONST_INT VAULT_DRILL_STATE_DO_BAG_SWAP			8
CONST_INT VAULT_DRILL_STATE_CLEANUP				9




STRUCT OBSTRUCTION_BOUNDS
	FLOAT fStartBound		=	0.0
	FLOAT fEndBound			=	0.0
ENDSTRUCT



///////////////////////////////////////////
///    DEBUG START 
///////////////////////////////////////////

#IF IS_DEBUG_BUILD
	PROC DRAW_STRING_TO_VAULT_DRILL_DEBUG(S_VAULT_DRILL_DATA &sVaultDrillData, STRING strDebug)			
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.04 + (0.04 * sVaultDrillData.iDebugNumPrintsThisFrame), 0.0>>, 0, 0, 255, 255)
		sVaultDrillData.iDebugNumPrintsThisFrame++
	ENDPROC

	PROC DRAW_INT_TO_VAULT_DRILL_DEBUG(S_VAULT_DRILL_DATA &sVaultDrillData, INT iDebug, STRING strLabel)
		TEXT_LABEL_63 strConcat = strLabel
		strConcat += iDebug
		
		DRAW_STRING_TO_VAULT_DRILL_DEBUG(sVaultDrillData, strConcat)
	ENDPROC
	
	PROC DRAW_FLOAT_TO_VAULT_DRILL_DEBUG(S_VAULT_DRILL_DATA &sVaultDrillData, FLOAT fDebug, STRING strLabel)
		TEXT_LABEL_63 strConcat = strLabel
		strConcat += " "
		strConcat += GET_STRING_FROM_FLOAT(fDebug)
		
		DRAW_STRING_TO_VAULT_DRILL_DEBUG(sVaultDrillData, strConcat)
	ENDPROC

	PROC DUMP_VAULT_DRILL_MINIGAME_STATE_TO_CONSOLE(S_VAULT_DRILL_DATA &sVaultDrillData)	
		INT iCurrentTime = GET_GAME_TIMER()
		
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] Dumping script state, current game time = ", iCurrentTime)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] iCurrentState = ", sVaultDrillData.iCurrentState)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] iBitset = ", sVaultDrillData.iBitSet)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] iHUDFlashTimer = ", sVaultDrillData.iHUDFlashTimer)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] fCurrentDrillPosY = ", sVaultDrillData.fCurrentDrillPosY)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] fCurrentDrillBasePosX = ", sVaultDrillData.fCurrentDrillBasePosX)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] fCurrentDrillBitPos = ", sVaultDrillData.fCurrentDrillBitPos)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] fDrillBitShakeOffset = ", sVaultDrillData.fDrillBitShakeOffset)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] fCurrentLeanPos = ", sVaultDrillData.fCurrentLeanPos)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] fCurrentForwardLeanPos = ", sVaultDrillData.fCurrentForwardLeanPos)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] fCurrentDrillPower = ", sVaultDrillData.fCurrentDrillPower)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] fCurrentProgress = ", sVaultDrillData.fCurrentProgress)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] fCurrentSweetSpotHeight = ", sVaultDrillData.fCurrentSweetSpotHeight)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] fCurrentSweetSpotMin = ", sVaultDrillData.fCurrentSweetSpotMin)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] fCurrentSweetSpotMax = ", sVaultDrillData.fCurrentSweetSpotMax)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] fCurrentHeatLevel = ", sVaultDrillData.fCurrentHeatLevel)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] fCurrentProgressIncreaseSpeed = ", sVaultDrillData.fCurrentProgressIncreaseSpeed)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] fHudPosX = ", sVaultDrillData.fHudPosX)
		CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] fHudPosY = ", sVaultDrillData.fHudPosY)
	ENDPROC

	PROC UPDATE_VAULT_DRILL_MINIGAME_DEBUG(S_VAULT_DRILL_DATA &sVaultDrillData)
		//Dump the state of all variables if someone enters a bug.
		IF IS_KEYBOARD_KEY_PRESSED(KEY_SPACE)
		OR IS_KEYBOARD_KEY_PRESSED(KEY_RBRACKET)
			DUMP_VAULT_DRILL_MINIGAME_STATE_TO_CONSOLE(sVaultDrillData)
		ENDIF
		
		//Instant pass.
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
			sVaultDrillData.fCurrentProgress = 1.0
		ENDIF
		
		//Incremental increase
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD9)
			sVaultDrillData.fCurrentProgress += 0.005
		ENDIF
		
		//Incremental decrease
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD3)
			sVaultDrillData.fCurrentProgress -= 0.005
		ENDIF
		
		//Display some debug
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
			sVaultDrillData.bDisplayDebug = NOT sVaultDrillData.bDisplayDebug
		ENDIF
		
		//Move the HUD around
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
			sVaultDrillData.fHudPosX -= 0.01
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD6)
			sVaultDrillData.fHudPosX += 0.01
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD8)
			sVaultDrillData.fHudPosY -= 0.01
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
			sVaultDrillData.fHudPosY += 0.01
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_HAS_PLAYER_PASSED_MINIGAME)
		ENDIF
		
		IF sVaultDrillData.bDisplayDebug
			sVaultDrillData.iDebugNumPrintsThisFrame = 0
		
			DRAW_INT_TO_VAULT_DRILL_DEBUG(sVaultDrillData, sVaultDrillData.iCurrentState, "iCurrentState")
			DRAW_FLOAT_TO_VAULT_DRILL_DEBUG(sVaultDrillData, sVaultDrillData.fCurrentDrillBasePosX, "fCurrentDrillBasePosX")
			DRAW_FLOAT_TO_VAULT_DRILL_DEBUG(sVaultDrillData, sVaultDrillData.fCurrentLeanPos, "fCurrentLeanPos")
			DRAW_FLOAT_TO_VAULT_DRILL_DEBUG(sVaultDrillData, sVaultDrillData.fCurrentForwardLeanPos, "fCurrentForwardLeanPos")
			DRAW_FLOAT_TO_VAULT_DRILL_DEBUG(sVaultDrillData, sVaultDrillData.fCurrentDrillPosY, "fCurrentDrillPosY")
			DRAW_FLOAT_TO_VAULT_DRILL_DEBUG(sVaultDrillData, sVaultDrillData.fCurrentDrillBitPos, "fCurrentDrillBitPos")
			DRAW_FLOAT_TO_VAULT_DRILL_DEBUG(sVaultDrillData, sVaultDrillData.fDrillBitShakeOffset, "fDrillBitShakeOffset")
			DRAW_FLOAT_TO_VAULT_DRILL_DEBUG(sVaultDrillData, sVaultDrillData.fCurrentDrillPower, "fCurrentDrillPower")
			DRAW_FLOAT_TO_VAULT_DRILL_DEBUG(sVaultDrillData, sVaultDrillData.fPinBreakPowerModifier, "fPinBreakPowerModifier")
			DRAW_FLOAT_TO_VAULT_DRILL_DEBUG(sVaultDrillData, sVaultDrillData.fCurrentDrillAnimPower, "fCurrentDrillAnimPower")
			DRAW_FLOAT_TO_VAULT_DRILL_DEBUG(sVaultDrillData, sVaultDrillData.fCurrentProgress, "fCurrentProgress")
			DRAW_FLOAT_TO_VAULT_DRILL_DEBUG(sVaultDrillData, sVaultDrillData.fCurrentSweetSpotHeight, "fCurrentSweetSpotHeight")
			DRAW_FLOAT_TO_VAULT_DRILL_DEBUG(sVaultDrillData, sVaultDrillData.fCurrentSweetSpotMin, "fCurrentSweetSpotMin")
			DRAW_FLOAT_TO_VAULT_DRILL_DEBUG(sVaultDrillData, sVaultDrillData.fCurrentSweetSpotMax, "fCurrentSweetSpotMax")
			DRAW_FLOAT_TO_VAULT_DRILL_DEBUG(sVaultDrillData, sVaultDrillData.fCurrentHeatLevel, "fCurrentHeatLevel")
			DRAW_FLOAT_TO_VAULT_DRILL_DEBUG(sVaultDrillData, sVaultDrillData.fCurrentProgressIncreaseSpeed, "fCurrentProgressIncreaseSpeed")
			
			DRAW_RECT(0.8, 0.5, 0.05, sVaultDrillData.fCurrentSweetSpotHeight * 0.5, 255, 0, 0, 255)
		ENDIF
	ENDPROC
#ENDIF

///****************************************
///    DEBUG END 
///****************************************


///////////////////////////////////////////
///    PC CONTROLS START 
///////////////////////////////////////////

// PC CONTROL FOR DRILLING MINIGAME

PROC SETUP_PC_VAULT_DRILLING_CONTROLS(S_VAULT_DRILL_DATA &sVaultDrillData, INT iLocalPartic)
	UNUSED_PARAMETER(sVaultDrillData)
	IF IS_PC_VERSION()
		IF NOT IS_BIT_SET(iVaultDrillLocalBitSet[iLocalPartic], VAULT_DRILL_LOCAL_BITSET_PC_KEYBOARD_CONTROLS)    
			CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME]- Setup PC controls" )
			INIT_PC_SCRIPTED_CONTROLS("MP DRILL MINIGAME")
			SET_BIT(iVaultDrillLocalBitSet[iLocalPartic], VAULT_DRILL_LOCAL_BITSET_PC_KEYBOARD_CONTROLS)    
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_PC_VAULT_DRILLING_CONTROLS(S_VAULT_DRILL_DATA &sVaultDrillData, INT iLocalPartic)
	UNUSED_PARAMETER(sVaultDrillData)
	IF IS_PC_VERSION()
		IF  IS_BIT_SET(iVaultDrillLocalBitSet[iLocalPartic], VAULT_DRILL_LOCAL_BITSET_PC_KEYBOARD_CONTROLS)
			CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME]- Cleanup PC controls" )
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			CLEAR_BIT(iVaultDrillLocalBitSet[iLocalPartic], VAULT_DRILL_LOCAL_BITSET_PC_KEYBOARD_CONTROLS)    
		ENDIF
	ENDIF
ENDPROC

///****************************************
///    PC CONTROLS END 
///****************************************

PROC SETUP_DRILL_TYPE(S_VAULT_DRILL_DATA &sVaultDrillData, BOOL bUseLaser = FALSE, INT iNumberOfObstacles = 4, FLOAT fObstacleResistance = 0.5, BOOL bShouldSkipOutro = FALSE)

	sVaultDrillData.iObstructions 	=  iNumberOfObstacles
	sVaultDrillData.fDifficulty	 	=  fObstacleResistance
	
	IF bUseLaser
		SET_BIT(sVaultDrillData.iBitset , VAULT_DRILL_BITSET_USE_LASER)
	ELSE
		CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)	
	ENDIF
	
	IF bShouldSkipOutro
		SET_BIT(sVaultDrillData.iBitset , VAULT_DRILL_BITSET_SHOULD_SKIP_OUTRO)
	ELSE
		CLEAR_BIT(sVaultDrillData.iBitset , VAULT_DRILL_BITSET_SHOULD_SKIP_OUTRO)
	ENDIF
	
ENDPROC


///////////////////////////////////////////
///    INIT START
///////////////////////////////////////////   

PROC INITIALISE_VAULT_MINIGAME_VALUES(S_VAULT_DRILL_DATA &sVaultDrillData)
	IF NOT IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_DID_PLAYER_MESS_UP)
		IF NOT IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_DID_PLAYER_BACK_OUT)
		AND NOT IS_BIT_SET(sVaultDrillData.iBitset,VAULT_DRILL_BITSET_ALREADY_STARTED)
			PRINTLN("[VAULT_DRILLING] INITIALISE_VAULT_MINIGAME_VALUES : Set init values")
			sVaultDrillData.fCurrentProgress 	= VAULT_DRILL_START_POINT
			sVaultDrillData.iCurrentObstruction = 0
		ENDIF
		
		sVaultDrillData.fCurrentHeatLevel = 0.0
	ENDIF
	
	sVaultDrillData.iHUDFlashTimer = 0
	sVaultDrillData.iCurrentState = 0
	sVaultDrillData.fCurrentDrillPosY = 0.0
	sVaultDrillData.fCurrentDrillBitPos = 0.0
	sVaultDrillData.fDrillBitShakeOffset = 0.0
	sVaultDrillData.fCurrentDrillBasePosX = 0.0
	sVaultDrillData.fCurrentDrillPower = 0.0
	sVaultDrillData.fCurrentProgressIncreaseSpeed = 0.0
	sVaultDrillData.fCurrentForwardLeanPos = 0.0
	sVaultDrillData.fForwardLeanExtraAmountForPins = 0.0
	sVaultDrillData.fPinBreakPowerModifier = 0.0
	
	sVaultDrillData.fCurrentSweetSpotHeight = VAULT_DRILL_MAX_SWEET_SPOT_SIZE
	sVaultDrillData.fCurrentSweetSpotMax = sVaultDrillData.fCurrentProgress
	sVaultDrillData.fCurrentSweetSpotMin = sVaultDrillData.fCurrentSweetSpotMax - VAULT_DRILL_MAX_SWEET_SPOT_SIZE
	
	IF sVaultDrillData.fCurrentSweetSpotMin < 0.0
		sVaultDrillData.fCurrentSweetSpotMin = 0.0
	ENDIF
	
	CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_IS_BACK_OUT_COMPLETE)
	CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_IS_MINIGAME_COMPLETE)
	CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_DID_PLAYER_MESS_UP)
	CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_HAS_PLAYER_SET_TO_BACK_OUT)
	CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_TRIGGER_REVEAL)
ENDPROC

PROC RESET_VAULT_DRILL_MINIGAME_DATA(S_VAULT_DRILL_DATA &sVaultDrillData)	
	sVaultDrillData.iBitset = 0
	sVaultDrillData.iHUDFlashTimer = 0
	sVaultDrillData.iCurrentState = 0
	sVaultDrillData.iCurrentObstruction = 0
	sVaultDrillData.fCurrentDrillPosY = 0.0
	sVaultDrillData.fCurrentDrillBitPos = 0.0
	sVaultDrillData.fDrillBitShakeOffset = 0.0
	sVaultDrillData.fCurrentDrillBasePosX = 0.0
	sVaultDrillData.fCurrentDrillPower = 0.0
	sVaultDrillData.fCurrentProgress = 0.0
	sVaultDrillData.fCurrentSweetSpotHeight = VAULT_DRILL_MAX_SWEET_SPOT_SIZE
	sVaultDrillData.fCurrentSweetSpotMin = 0.0
	sVaultDrillData.fCurrentSweetSpotMax = 0.0
	sVaultDrillData.fCurrentHeatLevel = 0.0
	sVaultDrillData.fCurrentProgressIncreaseSpeed = 0.0
	sVaultDrillData.fForwardLeanExtraAmountForPins = 0.0
	sVaultDrillData.fPinBreakPowerModifier = 0.0
ENDPROC

///****************************************
///    INIT END
///****************************************    

///
///    HELPERS AND VARIABLES
///    

///
///    These are values that might be required outside of the creator, so putting here as grab functions.
///    

FUNC VECTOR VAULT_DRILL__GET_OFFSET_FOR_MINIGAME_ROOT()
	RETURN  <<0.015, -0.05, -0.0>> 
ENDFUNC

FUNC VECTOR VAULT_DRILL__GET_OFFSET_FOR_MINIGAME_ROOT_LASER()
	RETURN  <<0.015, -0.172, -0.0>> 
ENDFUNC

FUNC VECTOR VAULT_DRILL__GET_OFFSET_PTFX_FROM_CENTRE_OBJECT()
	RETURN   <<-0.05, 0.15, -0.2>>
ENDFUNC

FUNC VECTOR VAULT_DRILL__GET_ROTATION_FOR_PTFX()
	RETURN   <<90.0, 0.0, 0.0>>
ENDFUNC

FUNC STRING VAULT_DRILL__GET_ANIM_DICTIONARY_LASER()
	RETURN  "anim_heist@hs3f@ig9_vault_drill@laser_drill@"
ENDFUNC

FUNC STRING VAULT_DRILL__GET_ANIM_DICTIONARY_DRILL()
	RETURN  "anim_heist@hs3f@ig9_vault_drill@drill@"
ENDFUNC

FUNC BOOL VAULT_DRILL_HAS_TIMER_PASSED( INT iStartTime, INT iDelay )
	
	IF GET_GAME_TIMER() - iStartTime > iDelay
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC REMOVE_VAULT_DRILL_PLAYER_BAG()
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
ENDPROC

///////////////////////////////////////////
///   ASSET REQUESTS START
///////////////////////////////////////////


/// REQUEST_DRILL_MINIGAME_ASSETS()
/// NOTES: 
/// PURPOSE:
/// checking assets are loaded
/// PARAMS:
/// RETURNS: 
PROC REQUEST_VAULT_DRILL_MINIGAME_ASSETS(S_VAULT_DRILL_DATA &sVaultDrillData, MODEL_NAMES mnBag = HEI_P_M_BAG_VAR22_ARM_S)

	IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
		REQUEST_ANIM_DICT(VAULT_DRILL__GET_ANIM_DICTIONARY_LASER())
		REQUEST_MODEL(CH_PROP_LASERDRILL_01A )
		
		IF sVaultDrillData.sfDrillHud = NULL
			sVaultDrillData.sfDrillHud = REQUEST_SCALEFORM_MOVIE("VAULT_LASER")
		ENDIF
		
		REQUEST_SCRIPT_AUDIO_BANK("DLC_HEIST3\\HEIST_FINALE_LASER_DRILL")
		
		PRINTLN("[VAULT_DRILLING] REQUEST_VAULT_DRILL_MINIGAME_ASSETS - New Laser drill audio requested")
	ELSE
		REQUEST_ANIM_DICT(VAULT_DRILL__GET_ANIM_DICTIONARY_DRILL())
		REQUEST_MODEL(CH_PROP_CH_HEIST_DRILL)
		
		IF sVaultDrillData.sfDrillHud = NULL
			sVaultDrillData.sfDrillHud = REQUEST_SCALEFORM_MOVIE("VAULT_DRILL")
		ENDIF
		
		REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL")
		REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL_2")
		
		PRINTLN("[VAULT_DRILLING] REQUEST_VAULT_DRILL_MINIGAME_ASSETS - DRILL AUDIO REQUESTED")
	ENDIF
	
	
	REQUEST_MODEL(mnBag)
	REQUEST_MODEL(HEI_PROP_HEI_DRILL_HOLE)

	REQUEST_NAMED_PTFX_ASSET("scr_ch_finale")
	REQUEST_ADDITIONAL_TEXT("HACK", MINIGAME_TEXT_SLOT)
	
ENDPROC

/// HAVE_DRILL_ASSETS_LOADED()
/// NOTES: 
/// PURPOSE:
/// checking assets are loaded
/// PARAMS:
/// RETURNS: 
FUNC BOOL HAVE_VAULT_DRILL_ASSETS_LOADED(S_VAULT_DRILL_DATA &sVaultDrillData, MODEL_NAMES mnBag = HEI_P_M_BAG_VAR22_ARM_S)
	IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
		REQUEST_ANIM_DICT(VAULT_DRILL__GET_ANIM_DICTIONARY_LASER())
		REQUEST_MODEL(CH_PROP_LASERDRILL_01A)
	ELSE
		REQUEST_ANIM_DICT(VAULT_DRILL__GET_ANIM_DICTIONARY_DRILL())
		REQUEST_MODEL(CH_PROP_CH_HEIST_DRILL)
	ENDIF
	
	REQUEST_MODEL(HEI_PROP_HEI_DRILL_HOLE)
	REQUEST_MODEL(mnBag)
	
	BOOL bAssetsReady = TRUE
	
	IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
		PRINTLN("[VAULT_DRILLING] HAVE_VAULT_DRILL_ASSETS_LOADED - LASER")
		
		IF NOT HAS_ANIM_DICT_LOADED(VAULT_DRILL__GET_ANIM_DICTIONARY_LASER())
		AND NOT HAS_MODEL_LOADED(CH_PROP_LASERDRILL_01A)
			PRINTLN("[VAULT_DRILLING] Laser anim not loaded")
			bAssetsReady =  FALSE
		ENDIF
		
		IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_HEIST3\\HEIST_FINALE_LASER_DRILL")
			PRINTLN("[VAULT_DRILLING] HAVE_VAULT_DRILL_ASSETS_LOADED - New Laser drill audio requested")
			bAssetsReady =  FALSE
		ENDIF
	ELSE
		PRINTLN("[VAULT_DRILLING] HAVE_VAULT_DRILL_ASSETS_LOADED - DRILL")
		
		IF NOT HAS_ANIM_DICT_LOADED(VAULT_DRILL__GET_ANIM_DICTIONARY_DRILL())
			PRINTLN("[VAULT_DRILLING] New Drill anim not loaded")
			bAssetsReady = FALSE
		ENDIF
		
		IF NOT HAS_MODEL_LOADED(CH_PROP_CH_HEIST_DRILL)
			PRINTLN("[VAULT_DRILLING] Model not loaded")
			bAssetsReady = FALSE
		ENDIF
		
		
		IF NOT  REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL")
			PRINTLN("[VAULT_DRILLING] Audio bank 1 not loaded")
			bAssetsReady = FALSE
		ENDIF
		
		IF NOT  REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL_2")
			PRINTLN("[VAULT_DRILLING] Audio bank 2 not loaded")
			bAssetsReady = FALSE
		ENDIF
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(sVaultDrillData.sfDrillHud)
		PRINTLN("[VAULT_DRILLING] Scaleform not loaded")
		bAssetsReady = FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(mnBag)
		PRINTLN("[VAULT_DRILLING] New Drill anim not loaded")
		bAssetsReady = FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(HEI_PROP_HEI_DRILL_HOLE)
		PRINTLN("[VAULT_DRILLING] HEI_PROP_HEI_DRILL_HOLE not loaded")
		bAssetsReady = FALSE
	ENDIF
	
	IF	NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_ch_finale")
		PRINTLN("[VAULT_DRILLING] HAS_NAMED_PTFX_ASSET_LOADED(scr_ch_finale) not loaded")
		bAssetsReady = FALSE
	ENDIF
	
	
	IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED("HACK", MINIGAME_TEXT_SLOT)
		PRINTLN("[VAULT_DRILLING] additional hack text not loaded")
		bAssetsReady = FALSE
	ENDIF
	
	RETURN bAssetsReady
ENDFUNC



FUNC BOOL REMOTE_ASSETS_REQUEST_AND_LOAD(BOOL bIsLaser, MODEL_NAMES mnBag = HEI_P_M_BAG_VAR22_ARM_S)
	IF bIsLaser
		REQUEST_ANIM_DICT(VAULT_DRILL__GET_ANIM_DICTIONARY_LASER())
		REQUEST_MODEL(CH_PROP_LASERDRILL_01A )
		
		REQUEST_SCRIPT_AUDIO_BANK("DLC_HEIST3\\HEIST_FINALE_LASER_DRILL")
		
		PRINTLN("[VAULT_DRILLING] REMOTE_ASSETS_REQUEST_AND_LOAD -  LASER")
	ELSE
		PRINTLN("[VAULT_DRILLING] REMOTE_ASSETS_REQUEST_AND_LOAD -  DRILL")
		REQUEST_ANIM_DICT(VAULT_DRILL__GET_ANIM_DICTIONARY_DRILL())
		REQUEST_MODEL(CH_PROP_CH_HEIST_DRILL)
		REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL")
		REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL_2")
	ENDIF
	
	
	REQUEST_MODEL(mnBag)
	REQUEST_MODEL(HEI_PROP_HEI_DRILL_HOLE)
	REQUEST_NAMED_PTFX_ASSET("scr_ch_finale")
	REQUEST_ANIM_DICT("anim@heists@fleeca_bank@drilling")

	BOOL bAssetsReady = TRUE
	
	IF bIsLaser
		PRINTLN("[VAULT_DRILLING] REMOTE_ASSETS_REQUEST_AND_LOAD -  LASER")
	
		IF NOT HAS_ANIM_DICT_LOADED(VAULT_DRILL__GET_ANIM_DICTIONARY_LASER())
		AND NOT HAS_MODEL_LOADED(CH_PROP_LASERDRILL_01A)
		
			PRINTLN("[VAULT_DRILLING] New Drill anim not loaded")
			bAssetsReady =  FALSE
		ENDIF
	
		IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_HEIST3\\HEIST_FINALE_LASER_DRILL")	
			PRINTLN("[VAULT_DRILLING] REMOTE_ASSETS_REQUEST_AND_LOAD -  Audio bank LASER not loaded")
			bAssetsReady = FALSE
		ENDIF
	ELSE
		PRINTLN("[VAULT_DRILLING] REMOTE_ASSETS_REQUEST_AND_LOAD -  DRILL")
		IF NOT HAS_ANIM_DICT_LOADED(VAULT_DRILL__GET_ANIM_DICTIONARY_DRILL())
			PRINTLN("[VAULT_DRILLING] New Drill anim not loaded")
			bAssetsReady = FALSE
		ENDIF
		
		IF NOT HAS_MODEL_LOADED(CH_PROP_CH_HEIST_DRILL)
			PRINTLN("[VAULT_DRILLING] Model not loaded")
			bAssetsReady = FALSE
		ENDIF
		
		IF NOT  REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL")
			PRINTLN("[VAULT_DRILLING] Audio bank 1 not loaded")
			bAssetsReady = FALSE
		ENDIF
		
		IF NOT  REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL_2")
			PRINTLN("[VAULT_DRILLING] Audio bank 2 not loaded")
			bAssetsReady = FALSE
		ENDIF
		
	ENDIF
		
	IF NOT 	HAS_ANIM_DICT_LOADED("anim@heists@fleeca_bank@drilling")
		bAssetsReady = FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(mnBag)
		PRINTLN("[VAULT_DRILLING] New Drill anim not loaded")
		bAssetsReady = FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(HEI_PROP_HEI_DRILL_HOLE)
		PRINTLN("[VAULT_DRILLING] HEI_PROP_HEI_DRILL_HOLE not loaded")
		bAssetsReady = FALSE
	ENDIF
	
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_ch_finale")
		PRINTLN("[VAULT_DRILLING] HAS_NAMED_PTFX_ASSET_LOADED(scr_ch_finale) not loaded")
		bAssetsReady = FALSE
	ENDIF
		
	IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED("HACK", MINIGAME_TEXT_SLOT)
		PRINTLN("[VAULT_DRILLING] additional hack text not loaded")
		bAssetsReady = FALSE
	ENDIF
	
	RETURN bAssetsReady
ENDFUNC


PROC REMOTE_CLEAR_ASSETS(BOOL bIsLaser, MODEL_NAMES mnBag = HEI_P_M_BAG_VAR22_ARM_S)
	
	
	SET_MODEL_AS_NO_LONGER_NEEDED(HEI_PROP_HEI_DRILL_HOLE)
	
	IF HAS_NAMED_PTFX_ASSET_LOADED("scr_ch_finale")
		REMOVE_NAMED_PTFX_ASSET("scr_ch_finale")
	ENDIF
	
	IF bIsLaser
		REMOVE_ANIM_DICT(VAULT_DRILL__GET_ANIM_DICTIONARY_DRILL())
		SET_MODEL_AS_NO_LONGER_NEEDED(CH_PROP_LASERDRILL_01A)
		
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_HEIST3\\HEIST_FINALE_LASER_DRILL")
		
	ELSE
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL_2")
		REMOVE_ANIM_DICT(VAULT_DRILL__GET_ANIM_DICTIONARY_DRILL())
		SET_MODEL_AS_NO_LONGER_NEEDED(CH_PROP_CH_HEIST_DRILL)
	ENDIF
	
	REMOVE_ANIM_DICT("anim@heists@fleeca_bank@drilling")
	SET_MODEL_AS_NO_LONGER_NEEDED(mnBag)
	
ENDPROC

///****************************************
///    ASSET REQUEST END 
///****************************************


///////////////////////////////////////////
///    PROP CREATION START 
///////////////////////////////////////////

PROC SET_VAULT_DRILL_SETTINGS(S_VAULT_DRILL_DATA &sVaultDrillData, NETWORK_INDEX &niMinigameObjects[],  PED_INDEX LocalPlayerPed, BOOL bSetInvisible = FALSE)
	FREEZE_ENTITY_POSITION(NET_TO_OBJ(niMinigameObjects[0]), TRUE)
	SET_ENTITY_COLLISION(NET_TO_OBJ(niMinigameObjects[0]), TRUE)
	ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(niMinigameObjects[0]),LocalPlayerPed,GET_PED_BONE_INDEX(LocalPlayerPed,BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>)
	SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niMinigameObjects[0]), TRUE)
	SET_NETWORK_ID_CAN_MIGRATE(niMinigameObjects[0], FALSE)
	SET_MODEL_AS_NO_LONGER_NEEDED(CH_PROP_CH_HEIST_DRILL)
	
	SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[0]), NOT bSetInvisible)
	
	CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_RESERVED_DRILL)
ENDPROC


FUNC BOOL CREATE_VAULT_DRILL_OBJECT__DRILL(S_VAULT_DRILL_DATA &sVaultDrillData, NETWORK_INDEX &niMinigameObjects[],  PED_INDEX LocalPlayerPed, BOOL bIsLocalPlayerHost , BOOL bSetInvisible = FALSE)
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[0])
		RETURN TRUE
	ENDIF
		
	REQUEST_MODEL(CH_PROP_CH_HEIST_DRILL)
		
	IF HAS_MODEL_LOADED(CH_PROP_CH_HEIST_DRILL)
	
		IF NOT IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_RESERVED_DRILL)
			IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
			ELSE
				RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS() + 1)
			ENDIF
			SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_RESERVED_DRILL)
		ENDIF
		
		IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_RESERVED_DRILL)
			IF CAN_REGISTER_MISSION_OBJECTS(1)
				IF CREATE_NET_OBJ(niMinigameObjects[0],CH_PROP_CH_HEIST_DRILL,GET_ENTITY_COORDS(LocalPlayerPed), bIsLocalPlayerHost)
					SET_VAULT_DRILL_SETTINGS(sVaultDrillData, niMinigameObjects, LocalPlayerPed ,bSetInvisible)	
					RETURN TRUE
				ENDIF						
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
	
ENDFUNC


FUNC BOOL CREATE_VAULT_DRILL_OBJECT__LASER(S_VAULT_DRILL_DATA &sVaultDrillData,NETWORK_INDEX &niMinigameObjects[],  PED_INDEX LocalPlayerPed, BOOL bIsLocalPlayerHost,  BOOL bSetInvisible = FALSE)
	

	IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[0])
		RETURN TRUE
	ENDIF
	
	REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_LaserDrill_01a")))
	
	IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_LaserDrill_01a")))
	
		IF NOT IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_RESERVED_DRILL)
			IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
			ELSE
				RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS() + 1)
			ENDIF
			SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_RESERVED_DRILL)
		ENDIF
		
		IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_RESERVED_DRILL)
			IF CAN_REGISTER_MISSION_OBJECTS(1)
				IF CREATE_NET_OBJ(niMinigameObjects[0],INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_LaserDrill_01a")),GET_ENTITY_COORDS(LocalPlayerPed), bIsLocalPlayerHost)
					SET_VAULT_DRILL_SETTINGS(sVaultDrillData, niMinigameObjects, LocalPlayerPed ,bSetInvisible)	
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	
	RETURN FALSE
	
ENDFUNC



FUNC BOOL CREATE_VAULT_DRILL_OBJECT(S_VAULT_DRILL_DATA &sVaultDrillData, NETWORK_INDEX &niMinigameObjects[], PED_INDEX LocalPlayerPed, BOOL bIsLocalPlayerHost,  BOOL bSetInvisible = FALSE)
	

	IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
		IF CREATE_VAULT_DRILL_OBJECT__LASER(sVaultDrillData, niMinigameObjects, LocalPlayerPed, bIsLocalPlayerHost, bSetInvisible)
			RETURN TRUE
		ENDIF
	ELSE
		IF CREATE_VAULT_DRILL_OBJECT__DRILL(sVaultDrillData, niMinigameObjects, LocalPlayerPed, bIsLocalPlayerHost, bSetInvisible)
			RETURN TRUE
		ENDIF
	ENDIF
	
	
	RETURN FALSE
	
ENDFUNC



FUNC BOOL CREATE_VAULT_DRILL_BAG_OBJECT(S_VAULT_DRILL_DATA &sVaultDrillData, NETWORK_INDEX &niMinigameObjects[], PED_INDEX LocalPlayerPed, BOOL bIsLocalPlayerHost, BOOL bSetInvisible = FALSE, BOOL bLaunchFromFMMC = TRUE, MODEL_NAMES mnBag = HEI_P_M_BAG_VAR22_ARM_S)
	IF NOT bLaunchFromFMMC					
		RETURN TRUE
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[1])
		RETURN TRUE
	ENDIF
	
	REQUEST_MODEL(mnBag)
	IF HAS_MODEL_LOADED(mnBag)
		IF NOT IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_RESERVED_BAG)
			IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
			ELSE
				RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS() + 1)
			ENDIF
			SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_RESERVED_BAG)
		ENDIF
		
		IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_RESERVED_BAG)
			IF CAN_REGISTER_MISSION_OBJECTS(1)
				IF CREATE_NET_OBJ(niMinigameObjects[1],mnBag,GET_ENTITY_COORDS(LocalPlayerPed), bIsLocalPlayerHost)
					FREEZE_ENTITY_POSITION(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
					SET_ENTITY_COLLISION(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
					SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
					SET_NETWORK_ID_CAN_MIGRATE(niMinigameObjects[1], FALSE)
					SET_MODEL_AS_NO_LONGER_NEEDED(mnBag)
					
					SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), NOT bSetInvisible)
					CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_RESERVED_BAG)
					
					RETURN TRUE
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF

	
	RETURN FALSE
	
ENDFUNC





///Creates a small hole object that's placed at the point of contact.
FUNC BOOL CREATE_VAULT_DRILL_HOLE_OBJECT(S_VAULT_DRILL_DATA &sVaultDrillData, NETWORK_INDEX &niMinigameObjects[], OBJECT_INDEX oiCentre, PED_INDEX LocalPlayerPed, BOOL bIsLocalPlayerHost, VECTOR vPos, VECTOR vRot)
	UNUSED_PARAMETER(oiCentre)
	IF  NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[3])
		PRINTLN("[DRILLING] - [RCC MISSION] [CREATE_DRILL_HOLE_OBJECT] - OBJECT EXISTS")
		RETURN TRUE
	ENDIF
	
	REQUEST_MODEL(HEI_PROP_HEI_DRILL_HOLE)
	
	IF NOT HAS_MODEL_LOADED(HEI_PROP_HEI_DRILL_HOLE)
		PRINTLN("[DRILLING] - [RCC MISSION] [CREATE_DRILL_HOLE_OBJECT] - AWAITING DRILL HOLE")
		RETURN FALSE
	ENDIF
	
	
	IF NOT IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_RESERVED_HOLE)
		IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
		ELSE
			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS() + 1)
		ENDIF	
		SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_RESERVED_HOLE)
		
		PRINTLN("[DRILLING] - [RCC MISSION] [CREATE_DRILL_HOLE_OBJECT] - RESERVING HOLE")
	ENDIF
		
	IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_RESERVED_HOLE)
		IF CAN_REGISTER_MISSION_OBJECTS(1)
			IF CREATE_NET_OBJ(niMinigameObjects[3],HEI_PROP_HEI_DRILL_HOLE, vPos, bIsLocalPlayerHost)
				SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(niMinigameObjects[3]), vPos)
				SET_ENTITY_ROTATION(NET_TO_OBJ(niMinigameObjects[3]), vRot)
				FREEZE_ENTITY_POSITION(NET_TO_OBJ(niMinigameObjects[3]), TRUE)
				SET_ENTITY_COLLISION(NET_TO_OBJ(niMinigameObjects[3]), FALSE)
				SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niMinigameObjects[3]), TRUE)
				SET_NETWORK_ID_CAN_MIGRATE(niMinigameObjects[3], FALSE)
				SET_MODEL_AS_NO_LONGER_NEEDED(HEI_PROP_HEI_DRILL_HOLE)
				
				SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[3]), FALSE)
				
				PRINTLN("[DRILLING] - [RCC MISSION] [CREATE_DRILL_HOLE_OBJECT] - Object created at ", vPos, " ", vRot)
				
				INTERIOR_INSTANCE_INDEX interiorCurrent
				INT iCurrentRoom
				
				interiorCurrent = GET_INTERIOR_FROM_ENTITY(LocalPlayerPed)
				iCurrentRoom = GET_ROOM_KEY_FROM_ENTITY(LocalPlayerPed)
				
				IF interiorCurrent != NULL
				AND iCurrentRoom != 0
					IF IS_INTERIOR_READY(interiorCurrent)
						PRINTLN("[DRILLING] - [RCC MISSION] [CREATE_DRILL_HOLE_OBJECT] - Forcing room for object.")					
						FORCE_ROOM_FOR_ENTITY(NET_TO_OBJ(niMinigameObjects[3]), interiorCurrent, iCurrentRoom)
					ENDIF	
				ENDIF
				
				CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_RESERVED_HOLE)
				
				RETURN TRUE
			
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
	
ENDFUNC




///****************************************
///    PROP CREATION END 
///****************************************


///////////////////////////////////////////
///    PROP DELETION START 
///////////////////////////////////////////


PROC DELETE_VAULT_DRILL_OBJECT(NETWORK_INDEX &niMinigameObjects[])
	OBJECT_INDEX oiCentreObject
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[0])
		oiCentreObject = NET_TO_OBJ(niMinigameObjects[0])
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niMinigameObjects[0])
			PRINTLN("[DRILLING] - [RCC MISSION] DELETE_DRILL_OBJECT: deleted object.")
			DELETE_OBJECT(oiCentreObject)
		ELSE
			PRINTLN("[DRILLING] - [RCC MISSION] DELETE_DRILL_OBJECT: set object as no longer needed.")
			CLEANUP_NET_ID(niMinigameObjects[0])
		ENDIF
		
		IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) - 1)
		ELSE
			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS() -1)
		ENDIF	
	ENDIF
ENDPROC



PROC DELETE_VAULT_DRILL_BAG_OBJECT(NETWORK_INDEX &niMinigameObjects[])
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[1])
		OBJECT_INDEX oiCentreObject = NET_TO_OBJ(niMinigameObjects[1])
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niMinigameObjects[1])
			PRINTLN("[VAULT DRILLING] - [RCC MISSION] DELETE_DRILL_BAG_OBJECT: deleted object.")
			DELETE_OBJECT(oiCentreObject)
		ELSE
			PRINTLN("[VAULT DRILLING] - [RCC MISSION] DELETE_DRILL_BAG_OBJECT: set object as no longer needed.")
			CLEANUP_NET_ID(niMinigameObjects[1])
		ENDIF
		
		IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) - 1)
		ELSE
			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS() -1)
		ENDIF	
	ENDIF
ENDPROC


PROC DELETE_VAULT_DRILL_HOLE_OBJECT(NETWORK_INDEX &niMinigameObjects[] )
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[3])
		OBJECT_INDEX oiCentreObject = NET_TO_OBJ(niMinigameObjects[3])
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niMinigameObjects[3])
			PRINTLN("[DRILLING] [RCC MISSION] DELETE_DRILL_HOLE_OBJECT: deleted object.")
			DELETE_OBJECT(oiCentreObject)
		ELSE
			PRINTLN("[DRILLING] [RCC MISSION] DELETE_DRILL_HOLE_OBJECT: set object as no longer needed.")
			CLEANUP_NET_ID(niMinigameObjects[3])
		ENDIF
		
		IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) - 1)
		ELSE
			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS() -1)
		ENDIF	
	ENDIF
ENDPROC


///****************************************
///    ASSET REQUEST END 
///****************************************



///
///    HELPERS START
///    


FUNC BOOL MANAGE_VAULT_DRILL_TIMER(INT &iStartTime, INT iTimeToWait) 
	RETURN (GET_GAME_TIMER() - iStartTime > iTimeToWait)
ENDFUNC

PROC FORCE_QUIT_FAIL_VAULT_DRILL_MINIGAME_KEEP_DATA(S_VAULT_DRILL_DATA  &drill_data, BOOL bResetData = TRUE)
	IF IS_MINIGAME_IN_PROGRESS()
		SET_MINIGAME_IN_PROGRESS(FALSE)
	ENDIF
	CLEAR_HELP()
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	DISABLE_CELLPHONE(FALSE)
	DISABLE_SCRIPT_HUD_THIS_FRAME_RESET()
	
	IF bResetData
		RESET_VAULT_DRILL_MINIGAME_DATA(drill_data)
	ENDIF
ENDPROC

PROC FORCE_QUIT_PASS_VAULT_DRILL_MINIGAME_KEEP_DATA(S_VAULT_DRILL_DATA  &drill_data, INT iLocalParti, BOOL bResetData = TRUE, BOOL bClearTasks = TRUE, BOOL bRestoreControl = TRUE, BOOL bKickOutDueToOtherPlayer = FALSE)
	IF IS_MINIGAME_IN_PROGRESS()
	AND NOT bKickOutDueToOtherPlayer
		SET_MINIGAME_IN_PROGRESS(FALSE)
	ENDIF
	
	IF bClearTasks
		CLEAR_PED_TASKS(PLAYER_PED_ID())
	ENDIF
	
	CLEAR_HELP()
	IF bRestoreControl
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	DISABLE_CELLPHONE(FALSE)
	DISABLE_SCRIPT_HUD_THIS_FRAME_RESET()
	
	CLEANUP_PC_VAULT_DRILLING_CONTROLS(drill_data, iLocalParti )
	
	IF bResetData
		RESET_VAULT_DRILL_MINIGAME_DATA(drill_data)
	ENDIF
ENDPROC


/// PURPOSE:
///    Checks if it's okay to run the drilling minigame.
FUNC BOOL IS_SAFE_TO_PLAY_VAULT_DRILL_MINIGAME()
	//Currently just one check: may be expanded in the future.
	IF NOT IS_CUTSCENE_PLAYING()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL HAS_PLAYER_BEAT_VAULT_DRILL_MINIGAME(S_VAULT_DRILL_DATA &sVaultDrillData,  INT iLocalParti,  BOOL bForceQuit = FALSE)
	IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_IS_MINIGAME_COMPLETE)
		IF bForceQuit 
			FORCE_QUIT_PASS_VAULT_DRILL_MINIGAME_KEEP_DATA(sVaultDrillData, iLocalParti)
		ENDIF

		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC




PROC SET_PLAYER_BACKED_OUT_OF_VAULT_DRILLING(S_VAULT_DRILL_DATA &sVaultDrillData)
	SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_DID_PLAYER_BACK_OUT)
ENDPROC



PROC PLAYER_MESSED_UP_VAULT_DRILLING(S_VAULT_DRILL_DATA &sVaultDrillData)
	SET_CONTROL_SHAKE(PLAYER_CONTROL, 2000, 220)
	
	SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_DID_PLAYER_MESS_UP)
	
	IF sVaultDrillData.fCurrentDrillBasePosX <= 0.0
		SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_DID_PLAYER_MESS_UP_ON_LEFT_SIDE)
	ELSE
		CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_DID_PLAYER_MESS_UP_ON_LEFT_SIDE)
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_MESSED_UP_VAULT_DRILLING(S_VAULT_DRILL_DATA &sVaultDrillData)
	RETURN IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_DID_PLAYER_MESS_UP)	
ENDFUNC

/// PURPOSE:
///    Returns whether the player was leaning on the left side when they messed up the drilling minigame. Useful for 
///    working out which transition anim to play.
FUNC BOOL DID_PLAYER_MESS_UP_VAULT_DRILLING_ON_LEFT_SIDE(S_VAULT_DRILL_DATA &sVaultDrillData)
	RETURN IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_DID_PLAYER_MESS_UP_ON_LEFT_SIDE)	
ENDFUNC


PROC SHAKE_VAULT_DRILL_BIT(S_VAULT_DRILL_DATA &sVaultDrillData)
	IF sVaultDrillData.fCurrentDrillPosY > sVaultDrillData.fCurrentSweetSpotMax
		IF ABSF(sVaultDrillData.fDesiredDrillBitShakeOffset - sVaultDrillData.fDrillBitShakeOffset) < 0.002
			sVaultDrillData.fDesiredDrillBitShakeOffset = GET_RANDOM_FLOAT_IN_RANGE(-0.04, 0.0)
		ENDIF
	ELSE
		sVaultDrillData.fDesiredDrillBitShakeOffset = 0.0
	ENDIF

	sVaultDrillData.fDrillBitShakeOffset = LERP_FLOAT(sVaultDrillData.fDrillBitShakeOffset, sVaultDrillData.fDesiredDrillBitShakeOffset, 1.0)
ENDPROC

PROC SHAKE_VAULT_DRILL_PLAYER(S_VAULT_DRILL_DATA &sVaultDrillData)
	
	//Work out the anim values: these are just interpolations of the progress and sideways movement values, and are used for smoothing out the anims.
	sVaultDrillData.fCurrentLeanPos 		= COSINE_INTERP_FLOAT(sVaultDrillData.fCurrentLeanPos, sVaultDrillData.fCurrentDrillBasePosX, VAULT_DRILL_LEAN_INTERP_SPEED)
	sVaultDrillData.fCurrentForwardLeanPos 	= COSINE_INTERP_FLOAT(sVaultDrillData.fCurrentForwardLeanPos, sVaultDrillData.fCurrentDrillBitPos, VAULT_DRILL_LEAN_INTERP_SPEED * 0.5)
	sVaultDrillData.fCurrentDrillAnimPower 	= COSINE_INTERP_FLOAT(sVaultDrillData.fCurrentDrillAnimPower, sVaultDrillData.fCurrentDrillPower, 0.4)
	
ENDPROC




PROC VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(OBSTRUCTION_BOUNDS &obstBound, FLOAT fStart, FLOAT fEnd)
	obstBound.fStartBound  	= fStart
	obstBound.fEndBound		= fEnd
	
ENDPROC


FUNC OBSTRUCTION_BOUNDS VAULT_DRILL_GET_OBSTRUCTION_BOUNDS(S_VAULT_DRILL_DATA &sVaultDrillData, INT iObstruction = -1) 
	OBSTRUCTION_BOUNDS obstBound
	
	IF iObstruction = -1
		iObstruction = sVaultDrillData.iCurrentObstruction
	ENDIF
	
	
	SWITCH sVaultDrillData.iObstructions
		CASE 2		
			SWITCH iObstruction
				CASE 0 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.213592233009709, 0.344660194174757) BREAK
				CASE 1 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.504854368932039,	0.635922330097087) BREAK
			ENDSWITCH
		BREAK
		
		CASE 3		
			SWITCH iObstruction
				CASE 0 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.213592233009709,	0.344660194174757) BREAK
				CASE 1 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.504854368932039,	0.635922330097087) BREAK
				CASE 2 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.796116504854369,	0.927184466019417) BREAK
			ENDSWITCH
		BREAK
		
		CASE 4	
			SWITCH iObstruction
				CASE 0 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.165413533834586,	0.266917293233083) BREAK
				CASE 1 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.390977443609023,	0.492481203007519) BREAK
				CASE 2 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.616541353383459,	0.718045112781955) BREAK
				CASE 3 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.842105263157895,	0.943609022556391) BREAK
			ENDSWITCH
		BREAK
		
		CASE 5	
			SWITCH iObstruction
				CASE 0 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.134969325153374,	0.217791411042945) BREAK
				CASE 1 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.319018404907975,	0.401840490797546) BREAK
				CASE 2 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.503067484662577,	0.585889570552147) BREAK
				CASE 3 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.687116564417178,	0.769938650306749) BREAK
				CASE 4 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.871165644171779,	0.95398773006135) BREAK
			ENDSWITCH
		BREAK
		
		CASE 6	
			SWITCH iObstruction
				CASE 0 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.113989637305699,	0.183937823834197) BREAK
				CASE 1 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.269430051813472,	0.339378238341969) BREAK
				CASE 2 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.424870466321244,	0.494818652849741) BREAK
				CASE 3 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.580310880829015,	0.650259067357513) BREAK
				CASE 4 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.735751295336788,	0.805699481865285) BREAK
				CASE 5 	VAULT_DRILL_SET_OBSTRUCTION_BOUNDS(obstBound,0.89119170984456,	0.961139896373057) BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN obstBound
ENDFUNC


PROC MANAGE_DRILL_HOLE(S_VAULT_DRILL_DATA &sVaultDrillData, NETWORK_INDEX &niMinigameObjects[])
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niMinigameObjects[3])
		IF NOT IS_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[3]))
			OBSTRUCTION_BOUNDS obs =  VAULT_DRILL_GET_OBSTRUCTION_BOUNDS(sVaultDrillData, 0)
			
			IF sVaultDrillData.fCurrentDrillPower > 0.05
			AND sVaultDrillData.fCurrentProgress > obs.fStartBound
				SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[3]), TRUE)
				
				VECTOR vCoord = GET_ENTITY_COORDS(NET_TO_OBJ(niMinigameObjects[3]))
				VECTOR vDevice =  GET_ENTITY_COORDS(NET_TO_OBJ(niMinigameObjects[0]))
				vCoord.z 	  = vDevice.z + 0.0225
				
				SET_ENTITY_COORDS(NET_TO_OBJ(niMinigameObjects[3]),vCoord )
			ENDIF
			
		ENDIF
		
//		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//		DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(NET_TO_OBJ(niMinigameObjects[3])), 0.5, 0, 0, 255, 30)
	ENDIF
	
	
ENDPROC




///
///    HELPERS END
///    


///////////////////////////////////////////
///    CAMERAS START 
///////////////////////////////////////////

PROC CREATE_VAULT_DRILL_CAM(S_VAULT_DRILL_DATA &sVaultDrillData, PED_INDEX 	LocalPlayerPed)
	IF NOT DOES_CAM_EXIST(sVaultDrillData.cam)
	
		sVaultDrillData.cam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
		sVaultDrillData.fCamRotOffset = 0.0
		sVaultDrillData.fDesiredCamRotOffset = 0.0
		sVaultDrillData.fCamHeightOffset = 0.0
		sVaultDrillData.fDesiredCamHeightOffset = 0.0
	
		VECTOR vCamOffset = <<0.8808, -0.5966, 0.5838>>
	
		vCamOffset *= 1.15
	
		sVaultDrillData.vCamStartPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(LocalPlayerPed, vCamOffset)
		sVaultDrillData.vCamPointPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(LocalPlayerPed, <<0.1376, 0.4819, 0.4162>>)
		
		sVaultDrillData.fCurrentCamShake = 0.1
		SET_CAM_COORD(sVaultDrillData.cam, sVaultDrillData.vCamStartPos)
		POINT_CAM_AT_COORD(sVaultDrillData.cam, sVaultDrillData.vCamPointPos)
		SET_CAM_FOV(sVaultDrillData.cam, 35.9280)
		SHAKE_CAM(sVaultDrillData.cam, "HAND_SHAKE", sVaultDrillData.fCurrentCamShake)
		SET_USE_HI_DOF()
		SET_CAM_DOF_PLANES(sVaultDrillData.cam, 0.0, 0.0, 1.645, 2.070)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)		
	ENDIF
ENDPROC




///PURPOSE: Handles updates to the position and shaking of the camera while drilling.
PROC UPDATE_VAULT_DRILLING_CAMERA(S_VAULT_DRILL_DATA &sVaultDrillData)

	IF DOES_CAM_EXIST(sVaultDrillData.cam)
	
		//Block player cam options whilst Cam running
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
	
		SET_USE_HI_DOF()
	
		//Apply shake based on drill power.
		sVaultDrillData.fCurrentCamShake += ((0.1 + (sVaultDrillData.fCurrentDrillPower * 0.9)) - sVaultDrillData.fCurrentCamShake) * 0.15
		SET_CAM_SHAKE_AMPLITUDE(sVaultDrillData.cam, sVaultDrillData.fCurrentCamShake) 
		
	ENDIF
ENDPROC


///****************************************
///    CAMERAS END 
///****************************************





///////////////////////////////////////////
///    ANIMS START 
///////////////////////////////////////////


/// PURPOSE:
///    Handles playback of the drill bit spinning anim.
/// PARAMS:
///    fCurrentSpeed - The desired anim speed (0.0 - 1.0).
PROC UPDATE_VAULT_DRILL_BIT_ANIM(NETWORK_INDEX &niMinigameObjects[],  FLOAT fCurrentSpeed)
	STRING sVaultDrillingAnimDict 		=	"anim_heist@hs3f@ig9_vault_drill@drill@"

	IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[0])
		IF NOT IS_ENTITY_PLAYING_ANIM(NET_TO_OBJ(niMinigameObjects[0]), sVaultDrillingAnimDict , "drill_straight_idle_drill_bit")
			PLAY_ENTITY_ANIM(NET_TO_OBJ(niMinigameObjects[0]), "drill_straight_idle_drill_bit", sVaultDrillingAnimDict,
							 INSTANT_BLEND_IN, TRUE, FALSE, DEFAULT, DEFAULT)	
		ELSE
			IF fCurrentSpeed > 1.0
				fCurrentSpeed = 1.0
			ENDIF

			SET_ENTITY_ANIM_SPEED(NET_TO_OBJ(niMinigameObjects[0]),  sVaultDrillingAnimDict, "drill_straight_idle_drill_bit", fCurrentSpeed)
		ENDIF
	ENDIF
ENDPROC


///****************************************
///    ANIMS END 
///****************************************


FUNC BOOL RETURN_IS_PLAYER_SPECTATOR_ONLY(PLAYER_INDEX playerId)

	IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerId)
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_SCTV(playerId)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC INT RETURN_ORDERED_PARTICIPANT_NUMBER(INT iParticipantNumber)
	INT i
	INT iReturn = -1
	FOR i = 0 TO NUM_NETWORK_PLAYERS-1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND NOT RETURN_IS_PLAYER_SPECTATOR_ONLY(INT_TO_PLAYERINDEX(i))
			iReturn++
			IF i = iParticipantNumber
				PRINTLN("[RCC MISSION] RETURN_ORDERED_PARTICIPANT_NUMBER - Participant ", iParticipantNumber, " Ordered Number: ", iReturn)
				RETURN iReturn
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN -1
			
ENDFUNC


///////////////////////////////////////////
///    PTFX START 
///////////////////////////////////////////

PROC HANDLE_UPDATE_PTFX_EVOLUTION__VAULT_LASER(S_VAULT_DRILL_DATA &sVaultDrillData, INT iLocalPart, INT iIterator)
	
	//If drill game is current player
	IF (iLocalPart = iIterator)
		
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sVaultDrillData.ptfxVaultLaser[iIterator])
			
			IF sVaultDrillData.fCurrentDrillPosY > 0
				SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultLaser[iIterator], "intensity", sVaultDrillData.fCurrentDrillPower)
			ELSE
				SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultLaser[iIterator], "intensity", 0.0)
			ENDIF
		ENDIF
		
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sVaultDrillData.ptfxVaultDrill[iIterator])
			
			IF sVaultDrillData.fCurrentDrillPosY > 0
				SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultDrill[iIterator], "intensity", sVaultDrillData.fCurrentDrillPower)
			ELSE
				SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultDrill[iIterator], "intensity", 0.0)
			ENDIF
		ENDIF
		
		//Second effect: plays when the drill is overheating.
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sVaultDrillData.ptfxVaultOverheat[iIterator])
			IF sVaultDrillData.fCurrentDrillPosY > sVaultDrillData.fCurrentSweetSpotMax
			AND sVaultDrillData.fCurrentDrillPower > 0.0
				IF sVaultDrillData.fCurrentHeatLevel > 1.0
					sVaultDrillData.fCurrentHeatLevel = 1.0
				ENDIF
				
				SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultOverheat[iIterator], "heat", sVaultDrillData.fCurrentHeatLevel)
			ELSE
				SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultOverheat[iIterator], "heat", 0.0)
			ENDIF
		ENDIF
		
	//If an external player
	ELSE
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sVaultDrillData.ptfxVaultLaser[iIterator])
			SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultLaser[iIterator], "intensity", 0.5)			
		ENDIF
	
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sVaultDrillData.ptfxVaultDrill[iIterator])
			
			SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultDrill[iIterator], "power", 0.5)

		ENDIF
		
		//Second effect: plays when the drill is overheating.
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sVaultDrillData.ptfxVaultOverheat[iIterator])
			
			SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultOverheat[iIterator], "heat", 0.1)
			
		ENDIF
		
	ENDIF

ENDPROC




PROC HANDLE_UPDATE_PTFX_EVOLUTION__VAULT_DRILL(S_VAULT_DRILL_DATA &sVaultDrillData, INT iLocalPart, INT iIterator)
	IF (iLocalPart = iIterator)
		
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sVaultDrillData.ptfxVaultDrill[iIterator])
			
		
			IF sVaultDrillData.fCurrentDrillPosY >= sVaultDrillData.fCurrentSweetSpotMin
			AND sVaultDrillData.fCurrentDrillPosY <= sVaultDrillData.fCurrentSweetSpotMax
				IF sVaultDrillData.fCurrentDrillPower > 1.0
					sVaultDrillData.fCurrentDrillPower = 1.0
				ENDIF
				
				SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultDrill[iIterator], "power", sVaultDrillData.fCurrentDrillPower)
			ELSE
				SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultDrill[iIterator], "power", 0.0)
			ENDIF
		ENDIF
		
		//Second effect: plays when the drill is overheating.
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sVaultDrillData.ptfxVaultOverheat[iIterator])
			IF sVaultDrillData.fCurrentDrillPosY > sVaultDrillData.fCurrentSweetSpotMax
			AND sVaultDrillData.fCurrentDrillPower > 0.0
				IF sVaultDrillData.fCurrentHeatLevel > 1.0
					sVaultDrillData.fCurrentHeatLevel = 1.0
				ENDIF
				
				SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultOverheat[iIterator], "heat", sVaultDrillData.fCurrentHeatLevel)
			ELSE
				SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultOverheat[iIterator], "heat", 0.0)
			ENDIF
		ENDIF
	ELSE
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sVaultDrillData.ptfxVaultDrill[iIterator])
			
			SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultDrill[iIterator], "power", 0.5)

		ENDIF
		
		//Second effect: plays when the drill is overheating.
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sVaultDrillData.ptfxVaultOverheat[iIterator])
			
			SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultOverheat[iIterator], "heat", 0.1)
			
		ENDIF
		
	ENDIF

ENDPROC




PROC VAULT_DRILL_STOP_AND_CLEANUP_PTFX_AND_SOUND(S_VAULT_DRILL_DATA &sVaultDrillData, INT iPartToStop)
	
	//Normally the minigame objects will have been deleted before this point, but do it again just in case.
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sVaultDrillData.ptfxVaultDrill[iPartToStop])
		CDEBUG1LN(DEBUG_MISSION, "[VAULT_DRILL_PTFX] UPDATE_LOCAL_VAULT_DRILL_PTFX Destroying : ptfxDrill[i]: ", iPartToStop)
		STOP_PARTICLE_FX_LOOPED(sVaultDrillData.ptfxVaultDrill[iPartToStop])
		sVaultDrillData.ptfxVaultDrill[iPartToStop] = NULL
	ENDIF	

	IF DOES_PARTICLE_FX_LOOPED_EXIST(sVaultDrillData.ptfxVaultOverheat[iPartToStop])
		CDEBUG1LN(DEBUG_MISSION, "[VAULT_DRILL_PTFX] UPDATE_LOCAL_VAULT_DRILL_PTFX Destroying : ptfxOverheat[i]: ", iPartToStop)
		STOP_PARTICLE_FX_LOOPED(sVaultDrillData.ptfxVaultOverheat[iPartToStop])
		sVaultDrillData.ptfxVaultOverheat[iPartToStop] = NULL
	ENDIF	
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sVaultDrillData.ptfxVaultLaser[iPartToStop])
		CDEBUG1LN(DEBUG_MISSION, "[VAULT_DRILL_PTFX] UPDATE_LOCAL_VAULT_DRILL_PTFX Destroying : ptfxVaultLaser[i]: ", iPartToStop)
		STOP_PARTICLE_FX_LOOPED(sVaultDrillData.ptfxVaultLaser[iPartToStop])
		sVaultDrillData.ptfxVaultLaser[iPartToStop] = NULL
	ENDIF	
	
	STOP_AUDIO_SCENE("DLC_HEIST_MINIGAME_FLEECA_DRILLING_SCENE")
	
ENDPROC






PROC PROCESS_LOCAL_VAULT_DRILL_PTFX(S_VAULT_DRILL_DATA &sVaultDrillData, OBJECT_INDEX oiVaultDrillLocalTarget, INT iLocalPart, INT i)
	
	
//	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//	DRAW_DEBUG_SPHERE(vTarget, 0.05, 255, 0,0, 128)
//	DRAW_DEBUG_SPHERE(vDevice, 0.10, 0, 255, 0, 128)
	
	IF IS_BIT_SET(iVaultDrillLocalBitSet[i], VAULT_DRILL_LOCAL_BITSET_CREATE_LOCAL_DRILL_PTFX) 
//		VECTOR vDrillPTFXRotation					=	VAULT_DRILL__GET_ROTATION_FOR_PTFX()
	
//		VECTOR vTarget = GET_ENTITY_COORDS(oiVaultDrillLocalTarget) 
//		VECTOR vDevice = GET_ENTITY_COORDS(oiVaultDrillLocalDevice[i] )

	
		CLEAR_BIT(iVaultDrillLocalBitSet[i], VAULT_DRILL_LOCAL_BITSET_STOP_PTFX)	
		BOOL bAllCreated = TRUE
		
		REQUEST_NAMED_PTFX_ASSET("scr_ch_finale")

		
		IF 	HAS_NAMED_PTFX_ASSET_LOADED("scr_ch_finale")
	
			IF IS_BIT_SET(iVaultDrillLocalBitSet[i], VAULT_DRILL_LOCAL_BITSET_IS_LASER)   
				IF (sVaultDrillData.ptfxVaultLaser[i] = NULL)
				AND DOES_ENTITY_EXIST(oiVaultDrillLocalDevice[i])
				AND NOT DOES_PARTICLE_FX_LOOPED_EXIST(sVaultDrillData.ptfxVaultLaser[i])
					VECTOR vOffset =  <<-0.00375, -0.3, 0.015>>
					USE_PARTICLE_FX_ASSET("scr_ch_finale")

					sVaultDrillData.ptfxVaultLaser[i] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ch_finale_laser", oiVaultDrillLocalDevice[i] , vOffset, <<0,0,-90.0>>)				
					CDEBUG1LN(DEBUG_MISSION, "[VAULT_DRILL_PTFX] UPDATE_LOCAL_VAULT_DRILL_PTFX Laser Created . Placed by Ordered Participant: ", i)
					bAllCreated = FALSE
				ENDIF
				
				
				IF (sVaultDrillData.ptfxVaultDrill[i] = NULL)
				AND NOT DOES_PARTICLE_FX_LOOPED_EXIST(sVaultDrillData.ptfxVaultDrill[i])	
					USE_PARTICLE_FX_ASSET("scr_ch_finale")
					VECTOR vCoord = GET_ENTITY_COORDS(oiVaultDrillLocalTarget)
					VECTOR vDevice =  GET_ENTITY_COORDS(oiVaultDrillLocalDevice[i])
					vCoord.z 	  = vDevice.z + 0.0225
					VECTOR vROT   = GET_ENTITY_ROTATION(oiVaultDrillLocalTarget) + <<90, 0.0,0>>
					
					sVaultDrillData.ptfxVaultDrill[i] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_ch_finale_laser_sparks", vCoord, vROT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)				
					
					SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultDrill[i], "power", 0.0)
					bAllCreated = FALSE
				ENDIF
				
			ELSE
				
				IF (sVaultDrillData.ptfxVaultDrill[i] = NULL)
				AND NOT DOES_PARTICLE_FX_LOOPED_EXIST(sVaultDrillData.ptfxVaultDrill[i])
					USE_PARTICLE_FX_ASSET("scr_ch_finale")
					VECTOR vCoord = GET_ENTITY_COORDS(oiVaultDrillLocalTarget)
					VECTOR vDevice =  GET_ENTITY_COORDS(oiVaultDrillLocalDevice[i])
					vCoord.z 	  = vDevice.z + 0.0225
					VECTOR vROT   = GET_ENTITY_ROTATION(oiVaultDrillLocalTarget) + <<90, 0.0,0>>
					
					sVaultDrillData.ptfxVaultDrill[i] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_ch_finale_drill_sparks", vCoord, vROT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)				
					CDEBUG1LN(DEBUG_MISSION, "[VAULT_DRILL_PTFX] UPDATE_LOCAL_VAULT_DRILL_PTFX debris Created . Placed by Ordered Participant: ", i)
				
					SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultDrill[i], "power", 0.0)
					bAllCreated = FALSE
				ENDIF
			ENDIF
		
			
			
			
			IF (sVaultDrillData.ptfxVaultOverheat[i] = NULL) 
			AND NOT DOES_PARTICLE_FX_LOOPED_EXIST(sVaultDrillData.ptfxVaultOverheat[i])
				USE_PARTICLE_FX_ASSET("scr_ch_finale")
				VECTOR vCoord = GET_ENTITY_COORDS(oiVaultDrillLocalTarget)
				VECTOR vDevice =  GET_ENTITY_COORDS(oiVaultDrillLocalDevice[i])
				vCoord.z 	  = vDevice.z + 0.0225
				VECTOR vROT   = GET_ENTITY_ROTATION(oiVaultDrillLocalTarget) + <<90, 0.0,0>>
				
				sVaultDrillData.ptfxVaultOverheat[i] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_ch_finale_drill_overheat", vCoord, vROT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)				
			
				SET_PARTICLE_FX_LOOPED_EVOLUTION(sVaultDrillData.ptfxVaultOverheat[i], "heat", 0.0)
				
				CDEBUG1LN(DEBUG_MISSION, "[VAULT_DRILL_PTFX] UPDATE_LOCAL_VAULT_DRILL_PTFX heat Created . Placed by Ordered Participant: ", i)
				bAllCreated = FALSE
			ENDIF
			
			IF bAllCreated
				CLEAR_BIT(iVaultDrillLocalBitSet[i], VAULT_DRILL_LOCAL_BITSET_CREATE_LOCAL_DRILL_PTFX)	
				SET_BIT(iVaultDrillLocalBitSet[i], VAULT_DRILL_LOCAL_BITSET_DRILL_PTFX_ACTIVE)
				
				CDEBUG1LN(DEBUG_MISSION, "[VAULT_DRILL_PTFX] UPDATE_LOCAL_VAULT_DRILL_PTFX Created . Placed by Ordered Participant: ", i)
			ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	
	IF IS_BIT_SET(iVaultDrillLocalBitSet[i], VAULT_DRILL_LOCAL_BITSET_DRILL_PTFX_ACTIVE) 
		CDEBUG1LN(DEBUG_MISSION, "[VAULT_DRILL_PTFX] VAULT_DRILL_LOCAL_BITSET_DRILL_PTFX_ACTIVE set on:  ", i)
		
		IF IS_BIT_SET(iVaultDrillLocalBitSet[i], VAULT_DRILL_LOCAL_BITSET_IS_LASER) 
			HANDLE_UPDATE_PTFX_EVOLUTION__VAULT_LASER(sVaultDrillData, iLocalPart, i)
		ELSE
			HANDLE_UPDATE_PTFX_EVOLUTION__VAULT_DRILL(sVaultDrillData, iLocalPart, i)
		ENDIF
		
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iVaultDrillLocalBitSet[i], VAULT_DRILL_LOCAL_BITSET_STOP_PTFX) 
		VAULT_DRILL_STOP_AND_CLEANUP_PTFX_AND_SOUND(sVaultDrillData, i)
	ENDIF

ENDPROC



/// PURPOSE:
///    Handles creating and updating the local (not networked) versions of the thermite PTFX.
PROC UPDATE_LOCAL_VAULT_DRILL_PTFX__FMMC(S_VAULT_DRILL_DATA &sVaultDrillData, INT iLocalPart, NETWORK_INDEX &CreatorObjects[])
	
	
	INT i
	REPEAT ciMAX_LOCAL_DRILL_PTFX i
		IF CreatorObjects[iVaultDrillLocalCentreObjectIndex_FMMC[i]] = NULL
			EXIT	
		ENDIF
		
		OBJECT_INDEX oiVaultDrillLocalTarget
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(CreatorObjects[iVaultDrillLocalCentreObjectIndex_FMMC[i]])
			oiVaultDrillLocalTarget = NET_TO_OBJ(CreatorObjects[iVaultDrillLocalCentreObjectIndex_FMMC[i]])
		ENDIF
		
		IF oiVaultDrillLocalTarget = NULL
			
			EXIT
		ENDIF

		PROCESS_LOCAL_VAULT_DRILL_PTFX(sVaultDrillData, oiVaultDrillLocalTarget, iLocalPart, i)
		
		
		IF IS_BIT_SET(iVaultDrillLocalBitSet[i], VAULT_DRILL_LOCAL_BITSET_STOP_PTFX) 
			CLEAR_BIT(iVaultDrillLocalBitSet[i], VAULT_DRILL_LOCAL_BITSET_CREATE_LOCAL_DRILL_PTFX)	
			CLEAR_BIT(iVaultDrillLocalBitSet[i], VAULT_DRILL_LOCAL_BITSET_DRILL_PTFX_ACTIVE)
			
			VAULT_DRILL_STOP_AND_CLEANUP_PTFX_AND_SOUND(sVaultDrillData, i)
			
		ENDIF

		
	ENDREPEAT

ENDPROC

/// PURPOSE:
///    Handles creating and updating the local (not networked) versions of the thermite PTFX.
PROC UPDATE_LOCAL_VAULT_DRILL_PTFX_ARCADE(S_VAULT_DRILL_DATA &sVaultDrillData, INT iLocalPart, OBJECT_INDEX ArcadeObjects, INT iPlayer)
	IF ArcadeObjects = NULL
		CDEBUG1LN(DEBUG_MISSION, "[VAULT_DRILL_PTFX] UPDATE_LOCAL_VAULT_DRILL_PTFX__ARCADE Object missing ")
		EXIT
	ENDIF

	PROCESS_LOCAL_VAULT_DRILL_PTFX(sVaultDrillData, ArcadeObjects, iLocalPart, iPlayer)
ENDPROC

///****************************************
///    PTFX END 
///****************************************


///****************************************
///    PTFX EVENTS START 
///****************************************


PROC HANDLE_VAULT_LASER_PTFX(S_VAULT_DRILL_DATA &sVaultDrillData,  NETWORK_INDEX netDevice,  INT iTargetIndex, INT iLocalPart)
	
	INT iOrderedPart = RETURN_ORDERED_PARTICIPANT_NUMBER(iLocalPart)
	
	
	IF HAS_PLAYER_MESSED_UP_VAULT_DRILLING(sVaultDrillData)
		IF NOT IS_BIT_SET(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_STOP_PTFX) 
			BROADCAST_VAULT_DRILL_EFFECT_EVENT(iOrderedPart, iTargetIndex, netDevice , FALSE, TRUE)
		ENDIF
	ELSE
		IF (sVaultDrillData.fCurrentDrillPower > 0)
		
			IF NOT IS_BIT_SET(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_CREATE_LOCAL_DRILL_PTFX) 
			AND NOT IS_BIT_SET(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_DRILL_PTFX_ACTIVE)  
				BROADCAST_VAULT_DRILL_EFFECT_EVENT(iOrderedPart, iTargetIndex, netDevice, TRUE, TRUE)
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_STOP_PTFX) 
				BROADCAST_VAULT_DRILL_EFFECT_EVENT(iOrderedPart, iTargetIndex, netDevice, FALSE, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC HANDLE_VAULT_DRILL_PTFX(S_VAULT_DRILL_DATA &sVaultDrillData, NETWORK_INDEX netDevice, INT iTargetIndex,    INT iLocalPart)
	
	INT iOrderedPart = RETURN_ORDERED_PARTICIPANT_NUMBER(iLocalPart)
	
	IF HAS_PLAYER_MESSED_UP_VAULT_DRILLING(sVaultDrillData)
		IF NOT IS_BIT_SET(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_STOP_PTFX) 
			BROADCAST_VAULT_DRILL_EFFECT_EVENT(iOrderedPart, iTargetIndex, netDevice , FALSE, FALSE)
		ENDIF
	ELSE
		IF (sVaultDrillData.fCurrentDrillPower > 0)
		
			IF NOT IS_BIT_SET(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_CREATE_LOCAL_DRILL_PTFX) 
			AND NOT IS_BIT_SET(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_DRILL_PTFX_ACTIVE)  
				BROADCAST_VAULT_DRILL_EFFECT_EVENT(iOrderedPart, iTargetIndex, netDevice , TRUE, FALSE)
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_STOP_PTFX) 
				BROADCAST_VAULT_DRILL_EFFECT_EVENT(iOrderedPart, iTargetIndex, netDevice , FALSE, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC



PROC HANDLE_VAULT_DRILL_AND_LASER_PTFX(S_VAULT_DRILL_DATA &sVaultDrillData, NETWORK_INDEX &niMinigameObjects[], INT iTargetIndex, INT iLocalPart)
	
	
	//Laser also requires beam effect on screen.
	IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
		HANDLE_VAULT_LASER_PTFX(sVaultDrillData, niMinigameObjects[0], iTargetIndex,  iLocalPart)
	ELSE
		HANDLE_VAULT_DRILL_PTFX(sVaultDrillData, niMinigameObjects[0],  iTargetIndex , iLocalPart)
	ENDIF
ENDPROC


///****************************************
///    PTFX EVENTS END 
///****************************************




///
///    EVENTS START
///    


PROC PROCESS_SCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT(SCRIPT_EVENT_DATA_VAULT_DRILL_EFFECT_EVENT EventData, INT iOrderedPart )

	CDEBUG1LN(DEBUG_MISSION, "[VAULT_DRILL_PTFX] PROCESS_SCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT Setting VAULT_DRILL_LOCAL_BITSET_CREATE_LOCAL_DRILL_PTFX : iOrderedPart: " , iOrderedPart)
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(EventData.niDevice)
		oiVaultDrillLocalDevice[iOrderedPart] = NET_TO_OBJ(EventData.niDevice)
	ENDIF
	
	CDEBUG1LN(DEBUG_MISSION, "[VAULT_DRILL_PTFX] PROCESS_SCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT Setting VAULT_DRILL_LOCAL_BITSET_CREATE_LOCAL_DRILL_PTFX : iVaultDrillLocalCentreObjectIndex: " , EventData.iCentreObjectIndex)
	
	IF EventData.bRunning
		SET_BIT(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_CREATE_LOCAL_DRILL_PTFX)
		CLEAR_BIT(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_STOP_PTFX)	
		CLEAR_BIT(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_DRILL_PTFX_ACTIVE)
		CDEBUG1LN(DEBUG_MISSION, "[VAULT_DRILL_PTFX] PROCESS_SCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT Setting VAULT_DRILL_LOCAL_BITSET_CREATE_LOCAL_DRILL_PTFX for Ordered Participant: ", iOrderedPart)
	ELSE
		CLEAR_BIT(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_CREATE_LOCAL_DRILL_PTFX)	
		CLEAR_BIT(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_DRILL_PTFX_ACTIVE)
		SET_BIT(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_STOP_PTFX)
		CDEBUG1LN(DEBUG_MISSION, "[VAULT_DRILL_PTFX] PROCESS_SCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT Setting VAULT_DRILL_LOCAL_BITSET_STOP_PTFX for Ordered Participant: ", iOrderedPart)
	ENDIF
	
	IF EventData.bLaser
		SET_BIT(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_IS_LASER)
	ELSE
		CLEAR_BIT(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_IS_LASER)
	ENDIF		

	
ENDPROC


PROC PROCESS_SCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT__ARCADE(INT iEventID)
	SCRIPT_EVENT_DATA_VAULT_DRILL_EFFECT_EVENT EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT
			INT iOrderedPart = EventData.iOrderedParticipant 
			iVaultDrillLocalCentreObjectIndex_ARCADE[iOrderedPart] = EventData.iCentreObjectIndex	
			PROCESS_SCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT(EventData, iOrderedPart )
		ENDIF
	ENDIF
	
ENDPROC



PROC PROCESS_SCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT__FMMC(INT iEventID)
	SCRIPT_EVENT_DATA_VAULT_DRILL_EFFECT_EVENT EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT
			INT iOrderedPart = EventData.iOrderedParticipant 
			iVaultDrillLocalCentreObjectIndex_FMMC[iOrderedPart] = EventData.iCentreObjectIndex	
			PROCESS_SCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT(EventData, iOrderedPart )
		ENDIF
	ENDIF
	
ENDPROC




///
///    EVENTS END
///    



///
///    HUD START
///    

//New version of HUD using scaleform movie.
PROC DRAW_VAULT_LASER_MINIGAME_SCALEFORM_HUD(S_VAULT_DRILL_DATA &sVaultDrillData)
	IF IS_SAFE_TO_PLAY_VAULT_DRILL_MINIGAME()
		
		IF NOT IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_TRIGGER_REVEAL)
			CALL_SCALEFORM_MOVIE_METHOD(sVaultDrillData.sfDrillHud , "REVEAL")
			SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_TRIGGER_REVEAL)
			CPRINTLN(DEBUG_MISSION, "[RUN VAULT_DRILLING] - SET_HOLE_DEPTH: ", sVaultDrillData.fCurrentProgress- 0.09)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sVaultDrillData.sfDrillHud, "SET_HOLE_DEPTH", sVaultDrillData.fCurrentProgress - 0.09)
		ENDIF
		
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		THEFEED_HIDE_THIS_FRAME()
		
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sVaultDrillData.sfDrillHud, "SET_LASER_VISIBLE")
			IF sVaultDrillData.fCurrentDrillPower > 0.01 
			AND sVaultDrillData.iCurrentState != VAULT_DRILL_MINIGAME_STATE_MESSEDUP
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
			ENDIF
		END_SCALEFORM_MOVIE_METHOD() 
		
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sVaultDrillData.sfDrillHud, "SET_POSITION", 1.0, 0.725)
		
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sVaultDrillData.sfDrillHud, "SET_NUM_DISCS", TO_FLOAT(sVaultDrillData.iObstructions))
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sVaultDrillData.sfDrillHud, "SET_SPEED", sVaultDrillData.fCurrentDrillPower * 0.79)
		
		IF sVaultDrillData.iCurrentState != VAULT_DRILL_MINIGAME_STATE_MESSEDUP
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sVaultDrillData.sfDrillHud, "SET_LASER_WIDTH", sVaultDrillData.fCurrentDrillPower)
		ENDIF
		
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sVaultDrillData.sfDrillHud, "SET_DRILL_POSITION", sVaultDrillData.fCurrentProgress)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sVaultDrillData.sfDrillHud, "SET_TEMPERATURE", sVaultDrillData.fCurrentHeatLevel)
		
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(sVaultDrillData.sfDrillHUD, 255, 255, 255, 255)
	ENDIF
ENDPROC


//New version of HUD using scaleform movie.
PROC DRAW_VAULT_DRILL_MINIGAME_SCALEFORM_HUD(S_VAULT_DRILL_DATA &sVaultDrillData)
	IF IS_SAFE_TO_PLAY_VAULT_DRILL_MINIGAME()
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		THEFEED_HIDE_THIS_FRAME() 
		
		IF NOT IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_TRIGGER_REVEAL)
			CPRINTLN(DEBUG_MISSION, "[RUN VAULT_DRILLING] - SET_HOLE_DEPTH: ", sVaultDrillData.fCurrentProgress - 0.09 )
			CALL_SCALEFORM_MOVIE_METHOD(sVaultDrillData.sfDrillHud , "REVEAL")
			SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_TRIGGER_REVEAL)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sVaultDrillData.sfDrillHud, "SET_HOLE_DEPTH", sVaultDrillData.fCurrentProgress - 0.09)
		
		ENDIF
		
		FLOAT fDrillPos  = sVaultDrillData.fCurrentDrillBitPos + sVaultDrillData.fDrillBitShakeOffset
		
		IF fDrillPos > 1.0
			fDrillPos = 1.0
		ENDIF
		
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sVaultDrillData.sfDrillHud, "SET_POSITION", 1.0, 0.725)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sVaultDrillData.sfDrillHud, "SET_NUM_DISCS", TO_FLOAT(sVaultDrillData.iObstructions))
		
		IF sVaultDrillData.iCurrentState != VAULT_DRILL_MINIGAME_STATE_MESSEDUP
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sVaultDrillData.sfDrillHud, "SET_SPEED", sVaultDrillData.fCurrentDrillPower * 0.79)
		ENDIF
		
		
		IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_IS_IN_SWEET_SPOT)
		AND (sVaultDrillData.fCurrentDrillPower > 0)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sVaultDrillData.sfDrillHud, "SET_DRILL_POSITION", sVaultDrillData.fCurrentProgress)
		ELSE
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sVaultDrillData.sfDrillHud, "SET_DRILL_POSITION", fDrillPos)
		ENDIF
		
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(sVaultDrillData.sfDrillHud, "SET_TEMPERATURE", sVaultDrillData.fCurrentHeatLevel)
		
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(sVaultDrillData.sfDrillHUD, 255, 255, 255, 255)
		
	ENDIF
ENDPROC



///
///    HUD END
/// 

///
///   Core loop helpers 
///    


PROC HANDLE_VAULT_DRILL_HEAT_LEVEL__DRILL(S_VAULT_DRILL_DATA &sVaultDrillData)
	
	FLOAT fFluctuatedCap
	
	//Decrease the overheat meter over time, if the player has let go of the power then cool down quicker (or if they overheated completely).
	IF sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_MESSEDUP
		IF sVaultDrillData.fCurrentDrillPower > 0.0
			sVaultDrillData.fCurrentHeatLevel = sVaultDrillData.fCurrentHeatLevel -@ (VAULT_DRILL_COOLDOWN_SPEED * 2.45)
		ELSE
			sVaultDrillData.fCurrentHeatLevel = sVaultDrillData.fCurrentHeatLevel -@ (VAULT_DRILL_COOLDOWN_SPEED * 4.0)
		ENDIF
	ELIF sVaultDrillData.fCurrentDrillPower > 0.0

		IF sVaultDrillData.fCurrentDrillPosY <= sVaultDrillData.fCurrentSweetSpotMax	
			IF NOT IS_BIT_SET(sVaultDrillData.iBitSet, VAULT_DRILL_BITSET_DRILL_ABOVE_STANDARD_HEAT_LEVEL)
				sVaultDrillData.fCurrentHeatLevel = sVaultDrillData.fCurrentHeatLevel +@ VAULT_DRILL_COOLDOWN_SPEED
			ELSE
				sVaultDrillData.fCurrentHeatLevel = sVaultDrillData.fCurrentHeatLevel -@ (VAULT_DRILL_COOLDOWN_SPEED *1.2 )
			ENDIF
		ENDIF
	ELSE
		sVaultDrillData.fCurrentHeatLevel = sVaultDrillData.fCurrentHeatLevel -@ (VAULT_DRILL_COOLDOWN_SPEED * 1.6)
	ENDIF
	
	IF sVaultDrillData.fCurrentHeatLevel > 0.2
		SET_BIT(sVaultDrillData.iBitSet, VAULT_DRILL_BITSET_DRILL_ABOVE_STANDARD_HEAT_LEVEL)
	ELIF sVaultDrillData.fCurrentHeatLevel < 0.1
		CLEAR_BIT(sVaultDrillData.iBitSet, VAULT_DRILL_BITSET_DRILL_ABOVE_STANDARD_HEAT_LEVEL)
	ENDIF
	
	IF sVaultDrillData.fCurrentDrillPosY <= sVaultDrillData.fCurrentSweetSpotMax	
		IF NOT IS_BIT_SET(sVaultDrillData.iBitSet, VAULT_DRILL_BITSET_DRILL_ABOVE_STANDARD_HEAT_LEVEL)
			fFluctuatedCap = GET_RANDOM_FLOAT_IN_RANGE(0.08, 0.13)
			
			IF sVaultDrillData.fCurrentHeatLevel > fFluctuatedCap
				sVaultDrillData.fCurrentHeatLevel = fFluctuatedCap
			ENDIF
		ENDIF
	ENDIF
	
	//Cap the heat value.
	IF sVaultDrillData.fCurrentHeatLevel > 1.0
		sVaultDrillData.fCurrentHeatLevel = 1.0
	ELIF sVaultDrillData.fCurrentHeatLevel < 0.0
		sVaultDrillData.fCurrentHeatLevel = 0.0
	ENDIF

ENDPROC

PROC HANDLE_VAULT_DRILL_HEAT_LEVEL__LASER(S_VAULT_DRILL_DATA &sVaultDrillData)
	
	//Decrease the overheat meter over time, if the player has let go of the power then cool down quicker (or if they overheated completely).
	IF sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_MESSEDUP
		SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_OVERHEAT_PENALTY_ACTIVE)
	
		IF sVaultDrillData.fCurrentDrillPower > 0.0
			sVaultDrillData.fCurrentHeatLevel = sVaultDrillData.fCurrentHeatLevel -@ (VAULT_LASER_COOLDOWN_SPEED * 0.3)
		ELSE
			sVaultDrillData.fCurrentHeatLevel = sVaultDrillData.fCurrentHeatLevel -@ (VAULT_DRILL_COOLDOWN_SPEED * 0.4)
		ENDIF
	ELSE
		
	
	
		IF sVaultDrillData.fCurrentDrillPower < 0.05
			IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_OVERHEAT_PENALTY_ACTIVE)
				sVaultDrillData.fCurrentHeatLevel = sVaultDrillData.fCurrentHeatLevel -@ VAULT_LASER_COOLDOWN_SPEED * 0.4
				
				IF sVaultDrillData.fCurrentHeatLevel <= 0.45
					CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_OVERHEAT_PENALTY_ACTIVE)
				ENDIF
			ELSE
				sVaultDrillData.fCurrentHeatLevel = sVaultDrillData.fCurrentHeatLevel -@ VAULT_LASER_COOLDOWN_SPEED
			ENDIF
		ENDIF
	ENDIF

	//Cap the heat value.
	IF sVaultDrillData.fCurrentHeatLevel > 1.0
		sVaultDrillData.fCurrentHeatLevel = 1.0
	ELIF sVaultDrillData.fCurrentHeatLevel < 0.0
		sVaultDrillData.fCurrentHeatLevel = 0.0
	ENDIF

ENDPROC

PROC HANDLE_VAULT_DRILL_HEAT_LEVEL(S_VAULT_DRILL_DATA &sVaultDrillData)
	IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
		HANDLE_VAULT_DRILL_HEAT_LEVEL__LASER(sVaultDrillData)
	ELSE
		HANDLE_VAULT_DRILL_HEAT_LEVEL__DRILL(sVaultDrillData)
	ENDIF

ENDPROC


PROC PROCESS_DRILLING__DRILL(S_VAULT_DRILL_DATA &sVaultDrillData, OBSTRUCTION_BOUNDS obs)
	UNUSED_PARAMETER(obs)
	
	IF sVaultDrillData.fCurrentDrillPower > 0.0
		IF sVaultDrillData.fCurrentDrillPosY < sVaultDrillData.fCurrentSweetSpotMin
			//Drill is too far back: nothing happens.
			SET_CONTROL_SHAKE(FRONTEND_CONTROL, 200, 15 + ROUND(sVaultDrillData.fCurrentDrillPower * 45.0) + ROUND(sVaultDrillData.fPinBreakPowerModifier * 45.0))
		ELIF sVaultDrillData.fCurrentDrillPosY > sVaultDrillData.fCurrentSweetSpotMax
			//Drill is too far forward: overheat it. Increase the rate of overheating based on drill power.
			sVaultDrillData.fCurrentHeatLevel = sVaultDrillData.fCurrentHeatLevel +@ (VAULT_DRILL_OVERHEAT_SPEED + (sVaultDrillData.fCurrentDrillPower * 0.2))
			SET_CONTROL_SHAKE(FRONTEND_CONTROL, 200, 200)
		ELSE
			sVaultDrillData.fCurrentProgressIncreaseSpeed = (VAULT_DRILL_MAX_PROGRESS_SPEED  * sVaultDrillData.fCurrentDrillPower)
			
			//Drill is in the sweet spot.
			
			sVaultDrillData.fCurrentProgress = sVaultDrillData.fCurrentProgress +@ (sVaultDrillData.fCurrentProgressIncreaseSpeed  * sVaultDrillData.fDifficulty) 
			
			IF sVaultDrillData.fCurrentProgress > 1.0
				sVaultDrillData.fCurrentProgress = 1.0
			ENDIF
		
			SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_IS_IN_SWEET_SPOT)
			SET_CONTROL_SHAKE(FRONTEND_CONTROL, 200, 30 + ROUND(sVaultDrillData.fCurrentDrillPower * 60.0) + ROUND(sVaultDrillData.fPinBreakPowerModifier * 60.0))
		ENDIF
	ELSE
		//Shake slightly if the player is pushing the drill too hard.
		IF sVaultDrillData.fCurrentDrillPosY > sVaultDrillData.fCurrentSweetSpotMax
			SET_CONTROL_SHAKE(FRONTEND_CONTROL, 200, 30)
		ENDIF
	ENDIF
	
	
	
ENDPROC



PROC PROCESS_DRILLING__LASER(S_VAULT_DRILL_DATA &sVaultDrillData, OBSTRUCTION_BOUNDS obs)
	UNUSED_PARAMETER(obs)

	IF sVaultDrillData.fCurrentDrillPower  > 0
	
		//Laser is in the sweet spot.			
		//Laser should always be overheating
		sVaultDrillData.fCurrentHeatLevel = sVaultDrillData.fCurrentHeatLevel +@ (VAULT_LASER_OVERHEAT_SPEED * sVaultDrillData.fCurrentDrillPower)
		
		// Whilst active, laser always cutting - player needs to watch out for overheating.
		
		FLOAT fModifier = 1.0 + (sVaultDrillData.fCurrentDrillPower *0.5)
		
		sVaultDrillData.fCurrentProgressIncreaseSpeed = (VAULT_DRILL_MAX_PROGRESS_SPEED  * sVaultDrillData.fCurrentDrillPower)
		sVaultDrillData.fCurrentProgress = sVaultDrillData.fCurrentProgress +@(fModifier * 2.0 *(sVaultDrillData.fCurrentProgressIncreaseSpeed  * sVaultDrillData.fDifficulty) )
	
	
		IF sVaultDrillData.fCurrentProgress >= 1.0
			sVaultDrillData.fCurrentProgress = 1.0
		ENDIF
	
		SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_IS_IN_SWEET_SPOT)
		SET_CONTROL_SHAKE(FRONTEND_CONTROL, 200, 30 + ROUND(sVaultDrillData.fCurrentDrillPower * 60.0) + ROUND(sVaultDrillData.fPinBreakPowerModifier * 60.0))
	ENDIF

ENDPROC



/// PURPOSE:
///    
/// PARAMS:
///    fNextObstacle - 
PROC HANDLE_OBSTRUCTION_BREAKING(S_VAULT_DRILL_DATA &sVaultDrillData, OBSTRUCTION_BOUNDS  obNextObstruction)
	IF sVaultDrillData.iCurrentObstruction < sVaultDrillData.iObstructions
		IF sVaultDrillData.fCurrentProgress >= obNextObstruction.fEndBound - 0.0125
			sVaultDrillData.fForwardLeanExtraAmountForPins += ( (sVaultDrillData.iCurrentObstruction * 0.06) - sVaultDrillData.fForwardLeanExtraAmountForPins) * 0.5
			
			IF sVaultDrillData.fForwardLeanExtraAmountForPins > 1.0 
				sVaultDrillData.fForwardLeanExtraAmountForPins  = 1.0
			ENDIF
			
			CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] Playing drill pin break sound ", sVaultDrillData.iCurrentObstruction)
			IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
				PLAY_SOUND_FRONTEND( -1, "laser_pin_break", "dlc_ch_heist_finale_laser_drill_sounds")
			ELSE
				PLAY_SOUND_FRONTEND( -1, "Drill_Pin_Break", "DLC_HEIST_FLEECA_SOUNDSET")
			ENDIF
			sVaultDrillData.fPinBreakPowerModifier = 1.0
			sVaultDrillData.iCurrentObstruction++
			
			OBSTRUCTION_BOUNDS obNext   = VAULT_DRILL_GET_OBSTRUCTION_BOUNDS(sVaultDrillData)
			sVaultDrillData.fCurrentProgress = obNext.fStartBound
		ENDIF
	ENDIF
ENDPROC







PROC VAULT_DRILL_HANDLE_INPUTS(S_VAULT_DRILL_DATA &sVaultDrillData)
	
	IF IS_PAUSE_MENU_ACTIVE()
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_HAS_PLAYER_PASSED_MINIGAME)
		
		//Grab the inputs
		IF NOT IS_SAFE_TO_PLAY_VAULT_DRILL_MINIGAME()
			sVaultDrillData.fCurrentDrillBasePosX = 0.0
			sVaultDrillData.fCurrentDrillPosY = 0.0
			sVaultDrillData.fCurrentDrillPower = 0.0
		ELSE
			sVaultDrillData.fCurrentDrillBasePosX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
			
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				
				IF IS_MOUSE_LOOK_INVERTED()
					sVaultDrillData.fCurrentDrillPosY += ( GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) * 0.25 )
				ELSE
					sVaultDrillData.fCurrentDrillPosY -= ( GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) * 0.25 )
				ENDIF
			ELSE
				sVaultDrillData.fCurrentDrillPosY = -GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y) 
			ENDIF
			sVaultDrillData.fCurrentDrillPower = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RT)
		ENDIF
			
		//Floor negative values of the Y position as they're not relevant.
		IF sVaultDrillData.fCurrentDrillPosY < 0.0
			sVaultDrillData.fCurrentDrillPosY = 0.0
		ENDIF

		//2081557 - Allow some room in the Y-position so the game doesn't get stuck due to a dodgy analog stick.
		IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			sVaultDrillData.fCurrentDrillPosY = sVaultDrillData.fCurrentDrillPosY / VAULT_DRILL_POS_SCALE_MODIFIER
		ENDIF

		IF sVaultDrillData.fCurrentDrillPosY > 1.0
			sVaultDrillData.fCurrentDrillPosY = 1.0
		ENDIF

		// No need to dead-zone on mouse
		IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			//If the power is a really small value then floor it (to protect against dodgy pad triggers).
			IF sVaultDrillData.fCurrentDrillPower < 0.03
				sVaultDrillData.fCurrentDrillPower = 0.0
			ENDIF
		ENDIF

	ENDIF
ENDPROC

//PURPOSE: Stops the sync scene
PROC VAULT_DRILL_STOP_SYNC_SCENE(INT &ThisSynchScene)
	INT iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(ThisSynchScene)
	
	IF iThisLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)	
			NETWORK_STOP_SYNCHRONISED_SCENE(iThisLocalSceneID)
			ThisSynchScene = -1
		ENDIF
	ENDIF
ENDPROC


PROC VAULT_DRILL_PLAY_OUTRO(S_VAULT_DRILL_DATA &sVaultDrillData, NETWORK_INDEX &niMinigameObjects[],  OBJECT_INDEX oiCentreObject, VECTOR vVaultDrill_SceneRoot, PED_INDEX 	localPlayerPed, BOOL bLaunchFromFMMC = TRUE )
	//Setup the outro scene.
	FLOAT fSceneRootRotation	
	fSceneRootRotation	= GET_ENTITY_HEADING(oiCentreObject)
		
	VAULT_DRILL_STOP_SYNC_SCENE(sVaultDrillData.iSyncScene)
	
	sVaultDrillData.iSyncScene 	= NETWORK_CREATE_SYNCHRONISED_SCENE(vVaultDrill_SceneRoot, <<0.0, 0.0, fSceneRootRotation>>, DEFAULT, TRUE)
	
	IF sVaultDrillData.iSyncScene != -1
		IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
			
			IF bLaunchFromFMMC
				NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[1]), sVaultDrillData.iSyncScene,
											  			VAULT_DRILL__GET_ANIM_DICTIONARY_LASER(), "bag_exit", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
														
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, sVaultDrillData.iSyncScene, VAULT_DRILL__GET_ANIM_DICTIONARY_LASER(), "exit", 
												  INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
												  SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_ABORT_ON_DEATH, 
		  										  RBF_PLAYER_IMPACT)
				
				NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(sVaultDrillData.iSyncScene, VAULT_DRILL__GET_ANIM_DICTIONARY_LASER(), "exit_cam")
			ELSE
				NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(sVaultDrillData.iSyncScene, VAULT_DRILL__GET_ANIM_DICTIONARY_LASER(), "exit_nobag_cam")
				
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, sVaultDrillData.iSyncScene, VAULT_DRILL__GET_ANIM_DICTIONARY_LASER(), "exit_nobag", 
												  INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
												  SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_ABORT_ON_DEATH, 
		  										  RBF_PLAYER_IMPACT)
			ENDIF
			
			
		ELSE
			
			IF bLaunchFromFMMC
				NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[1]), sVaultDrillData.iSyncScene,
												  			VAULT_DRILL__GET_ANIM_DICTIONARY_DRILL(), "bag_exit", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
															
				NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(sVaultDrillData.iSyncScene, VAULT_DRILL__GET_ANIM_DICTIONARY_DRILL(), "exit_cam")
				
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, sVaultDrillData.iSyncScene, VAULT_DRILL__GET_ANIM_DICTIONARY_DRILL(), "exit", 
												  INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
												  SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_ABORT_ON_DEATH, 
		  										  RBF_PLAYER_IMPACT)
			ELSE	
				NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(sVaultDrillData.iSyncScene, VAULT_DRILL__GET_ANIM_DICTIONARY_DRILL(), "exit_nobag_cam")
				
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, sVaultDrillData.iSyncScene, VAULT_DRILL__GET_ANIM_DICTIONARY_DRILL(), "exit_nobag", 
												  INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
												  SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_ABORT_ON_DEATH, 
		  										  RBF_PLAYER_IMPACT)
			ENDIF
			
			
		ENDIF
		
		NETWORK_FORCE_LOCAL_USE_OF_SYNCED_SCENE_CAMERA(sVaultDrillData.iSyncScene)										
		NETWORK_START_SYNCHRONISED_SCENE(sVaultDrillData.iSyncScene)
	ENDIF
	
ENDPROC



PROC VAULT_DRILL_SET_CURRENT_SWEET_SPOT(S_VAULT_DRILL_DATA &sVaultDrillData, OBSTRUCTION_BOUNDS obNextObstruction)
	//Determine the current sweet spot height and progress speed we should progress (both are based on drill power).	
	sVaultDrillData.fCurrentSweetSpotHeight = (obNextObstruction.fEndBound -obNextObstruction.fStartBound) * 2
	
	//Get the min of the obstruction, however create a small buffer to make it easier for players
	sVaultDrillData.fCurrentSweetSpotMin = obNextObstruction.fStartBound - 0.0125
	
	sVaultDrillData.fCurrentSweetSpotMax = sVaultDrillData.fCurrentSweetSpotMin + sVaultDrillData.fCurrentSweetSpotHeight
ENDPROC



FUNC BOOL HAS_PLAYER_BACKED_OUT_OF_VAULT_DRILLING(S_VAULT_DRILL_DATA &sVaultDrillData, PED_INDEX localPlayerPed)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		
		IF sVaultDrillData.iCurrentObstruction >=  sVaultDrillData.iObstructions
			RETURN FALSE
		ENDIF

		
		IF IS_PAUSE_MENU_ACTIVE()
			RETURN FALSE
		ENDIF

		IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
		AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(LocalPlayerPed) 
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL VAULT_DRILL_HAS_PLAYER_COMPLETED(S_VAULT_DRILL_DATA &sVaultDrillData, PED_INDEX localPlayerPed, INT iLocalPart)
	IF HAS_PLAYER_BEAT_VAULT_DRILL_MINIGAME(sVaultDrillData, iLocalPart)
	OR IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_HAS_PLAYER_PASSED_MINIGAME)
		IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
		AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(LocalPlayerPed) 
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


PROC VAULT_DRILL_TRIGGER_OUTRO(S_VAULT_DRILL_DATA &sVaultDrillData, NETWORK_INDEX &niMinigameObjects[], INT &iSafetyTimer, OBJECT_INDEX oiCentreObject, PED_INDEX localPlayerPed, VECTOR vVaultDrill_SceneRoot, BOOL bLaunchFromFMMC = TRUE)
	//The box is being treated as outside the interior, so force the right interior room here.
	INTERIOR_INSTANCE_INDEX interiorCurrent
	INT iCurrentRoom
	
	interiorCurrent = GET_INTERIOR_FROM_ENTITY(LocalPlayerPed)
	iCurrentRoom = GET_ROOM_KEY_FROM_ENTITY(LocalPlayerPed)
	
	IF bLaunchFromFMMC
		IF (interiorCurrent != NULL  AND iCurrentRoom != 0)
			IF IS_INTERIOR_READY(interiorCurrent)
				FORCE_ROOM_FOR_ENTITY(NET_TO_OBJ(niMinigameObjects[1]), interiorCurrent, iCurrentRoom)
			ENDIF	
		ENDIF

		//Bag has to be detached before starting the scene.
		DETACH_ENTITY(NET_TO_OBJ(niMinigameObjects[1]))
	ENDIF

	
	VAULT_DRILL_PLAY_OUTRO(sVaultDrillData, niMinigameObjects,  oiCentreObject, vVaultDrill_SceneRoot, localPlayerPed, bLaunchFromFMMC)					
	
	Pause_Objective_Text()
	sVaultDrillData.fDrillBitAnimSpeed = 0.99
	iSafetyTimer = GET_GAME_TIMER()
ENDPROC



PROC VAULT_DRILL_HANDLE_DRILL_AUDIO(S_VAULT_DRILL_DATA &sVaultDrillData, PED_INDEX localPlayerPed)
	//Update the drill audio: the drill is silent when no power is applied, if power is applied the sound depends on whether the drill is in the sweet spot or not.
	IF sVaultDrillData.iDrillSoundID != -1
		IF sVaultDrillData.fCurrentDrillPower = 0.0
			IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER) 
				SET_VARIABLE_ON_SOUND(sVaultDrillData.iDrillSoundID, "DrillState", 0.0)
				SET_VARIABLE_ON_SOUND(sVaultDrillData.iDrillSoundID, "DrillHeat", sVaultDrillData.fCurrentHeatLevel)
			ELSE
				IF NOT HAS_SOUND_FINISHED(sVaultDrillData.iDrillSoundID)
					STOP_SOUND(sVaultDrillData.iDrillSoundID)
				ENDIF
			ENDIF
		ELSE
			IF HAS_SOUND_FINISHED(sVaultDrillData.iDrillSoundID)
			
				IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
					PLAY_SOUND_FROM_ENTITY(sVaultDrillData.iDrillSoundID, "laser_drill", LocalPlayerPed, "dlc_ch_heist_finale_laser_drill_sounds", TRUE, 0)
					CPRINTLN(DEBUG_MISSION,"VAULT_DRILL_HANDLE_DRILL_AUDIO: Triggering audio again")
				ELSE
					PLAY_SOUND_FROM_ENTITY(sVaultDrillData.iDrillSoundID, "Drill", LocalPlayerPed, "DLC_HEIST_FLEECA_SOUNDSET", TRUE, 0)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER) 
				SET_VARIABLE_ON_SOUND(sVaultDrillData.iDrillSoundID, "DrillState", sVaultDrillData.fCurrentDrillPower)
				SET_VARIABLE_ON_SOUND(sVaultDrillData.iDrillSoundID, "DrillHeat", sVaultDrillData.fCurrentHeatLevel)
				
				CPRINTLN(DEBUG_MISSION,"VAULT_DRILL_HANDLE_DRILL_AUDIO: HEAT: ", sVaultDrillData.fCurrentHeatLevel, " Strength: ", sVaultDrillData.fCurrentDrillPower)
			ELSE
				IF sVaultDrillData.fCurrentDrillPosY < sVaultDrillData.fCurrentSweetSpotMin
					SET_VARIABLE_ON_SOUND(sVaultDrillData.iDrillSoundID, "DrillState", 0.0)
				ELIF sVaultDrillData.fCurrentDrillPosY <= sVaultDrillData.fCurrentSweetSpotMin
					SET_VARIABLE_ON_SOUND(sVaultDrillData.iDrillSoundID, "DrillState", 0.5)
				ELSE
					SET_VARIABLE_ON_SOUND(sVaultDrillData.iDrillSoundID, "DrillState", 1.0)
				ENDIF
				
			ENDIF
		ENDIF 
	ELSE
		IF NOT IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
			
			PLAY_SOUND_FROM_ENTITY(sVaultDrillData.iDrillSoundID, "Drill", LocalPlayerPed, "DLC_HEIST_FLEECA_SOUNDSET", TRUE, 0)
		ENDIF
	ENDIF

ENDPROC



PROC VAULT_DRILL_DEBUG_DISPLAY_VARIABLES(S_VAULT_DRILL_DATA &sVaultDrillData)
	UNUSED_PARAMETER(sVaultDrillData)
	#IF IS_DEBUG_BUILD 
		
		TEXT_LABEL_63 State = ""
		
	
		State = "sVaultDrillData.iCurrentObstruction:  " 
		State += CONVERT_INT_TO_STRING(sVaultDrillData.iCurrentObstruction) 
		
		SET_TEXT_SCALE(0.3,0.3)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05,0.15, "STRING", State)		
		
		State = "sVaultDrillData.fCurrentDrillPower: "
		State += FLOAT_TO_STRING(sVaultDrillData.fCurrentDrillPower, 5) 		
		SET_TEXT_SCALE(0.3,0.3)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05,0.20, "STRING", State)	
		
		
		
		State = "sVaultDrillData.fCurrentProgress: " 
		State += FLOAT_TO_STRING(sVaultDrillData.fCurrentProgress, 5) 		
		
		SET_TEXT_SCALE(0.3,0.3)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05,0.25 , "STRING", State)	
		
		
		State = "sVaultDrillData.fCurrentDrillBitPos:  " 
		State += FLOAT_TO_STRING(sVaultDrillData.fCurrentDrillBitPos, 5) 		
		SET_TEXT_SCALE(0.3,0.3)
		
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05,0.30 , "STRING", State)	
		
		State = "sVaultDrillData.fCurrentSweetSpotMin:  " 
		State += FLOAT_TO_STRING(sVaultDrillData.fCurrentSweetSpotMin, 5) 		
		SET_TEXT_SCALE(0.3,0.3)
		
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05,0.35 , "STRING", State)	
		
		
		State = "sVaultDrillData.fCurrentSweetSpotMax:  " 
		State += FLOAT_TO_STRING(sVaultDrillData.fCurrentSweetSpotMax, 5) 		
		SET_TEXT_SCALE(0.3,0.3)
		
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05,0.40 , "STRING", State)	
		
		
		State = "sVaultDrillData.fCurrentProgressIncreaseSpeed :  " 
		State += FLOAT_TO_STRING(sVaultDrillData.fCurrentProgressIncreaseSpeed, 5) 		
		SET_TEXT_SCALE(0.3,0.3)
		
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05,0.45 , "STRING", State)	
		
		
		State = "VAULT_DRILL_BITSET_IS_IN_SWEET_SPOT: " 
		IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_IS_IN_SWEET_SPOT)
			State += "TRUE"
		ELSE
			State += "FALSE"
		ENDIF
		
		SET_TEXT_SCALE(0.3,0.3)
		
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05,0.50 , "STRING", State)	
		
	#ENDIF
ENDPROC



PROC VAULT_DRILL__PRELOAD_BAG(PED_INDEX LocalPlayerPed, MP_HEIST_GEAR_ENUM mnBag)
	MP_HEIST_GEAR_ENUM eBag = HEIST_GEAR_SPORTS_BAG
	
	IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
		eBag = mnBag
	ENDIF

	PRELOAD_MP_HEIST_GEAR(LocalPlayerPed, eBag)
ENDPROC


PROC VAULT_DRILL__APPLY_DUFFEL_BAG_HEIST_GEAR(PED_INDEX piPed, MP_HEIST_GEAR_ENUM mnBag)
	
	MP_HEIST_GEAR_ENUM eBag = HEIST_GEAR_SPORTS_BAG
	
	IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
		eBag = mnBag
	ENDIF
	
	SET_MP_HEIST_GEAR(piPed, eBag) 

ENDPROC



PROC VAULT_DRILL_GAME_ENDING_SKIP(S_VAULT_DRILL_DATA &sVaultDrillData, NETWORK_INDEX &niMinigameObjects[], INT &iSafetyTimer, PED_INDEX localPlayerPed, INT iLocalPart, MP_HEIST_GEAR_ENUM eBag = HEIST_GEAR_SPORTS_BAG, BOOL bLaunchFromFMMC = TRUE)
	
	UNLOCK_MINIMAP_ANGLE()
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
	
	iSafetyTimer = GET_GAME_TIMER()
	
	IF DOES_CAM_EXIST(sVaultDrillData.cam)
		DESTROY_CAM(sVaultDrillData.cam)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDIF
	
	
	VAULT_DRILL_STOP_AND_CLEANUP_PTFX_AND_SOUND(sVaultDrillData, iLocalPart)
	
	DELETE_VAULT_DRILL_OBJECT(niMinigameObjects)
	IF bLaunchFromFMMC
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niMinigameObjects[1])
			DETACH_ENTITY(NET_TO_OBJ(niMinigameObjects[1]))
		ENDIF
		
		DELETE_VAULT_DRILL_BAG_OBJECT(niMinigameObjects)
	ENDIF
	
	IF sVaultDrillData.iDrillSoundID != -1
		
		STOP_SOUND(sVaultDrillData.iDrillSoundID)
		RELEASE_SOUND_ID(sVaultDrillData.iDrillSoundID)
		sVaultDrillData.iDrillSoundID = -1
	ENDIF
	
	IF sVaultDrillData.iDrillOverheatSoundID != -1
		STOP_SOUND(sVaultDrillData.iDrillOverheatSoundID)
	ENDIF
	
	IF bLaunchFromFMMC
		VAULT_DRILL__PRELOAD_BAG(LocalPlayerPed, eBag)
		VAULT_DRILL__APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, eBag)
	ELSE
	//	REMOVE_VAULT_DRILL_PLAYER_BAG()
	ENDIF
	
ENDPROC

PROC VAULT_DRILL_ANIMATE_BAG__IDLE(S_VAULT_DRILL_DATA &sVaultDrillData, NETWORK_INDEX &niMinigameObjects[], PED_INDEX LocalPlayerPed)
	IF NOT IS_ENTITY_ATTACHED(NET_TO_OBJ(niMinigameObjects[1]))
		ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(niMinigameObjects[1]),LocalPlayerPed,GET_PED_BONE_INDEX(LocalPlayerPed,BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>)
	ENDIF
	
	IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
		
		PLAY_ENTITY_ANIM(NET_TO_OBJ(niMinigameObjects[1]), "bag_drill_straight_idle", VAULT_DRILL__GET_ANIM_DICTIONARY_LASER(),
						 INSTANT_BLEND_IN, TRUE, FALSE, DEFAULT,DEFAULT,ENUM_TO_INT(AF_USE_KINEMATIC_PHYSICS))
	ELSE
		PLAY_ENTITY_ANIM(NET_TO_OBJ(niMinigameObjects[1]), "bag_drill_straight_idle", VAULT_DRILL__GET_ANIM_DICTIONARY_DRILL(),
						 INSTANT_BLEND_IN, TRUE, FALSE, DEFAULT, DEFAULT, ENUM_TO_INT(AF_USE_KINEMATIC_PHYSICS))

	ENDIF
	
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_OBJ(niMinigameObjects[1]))		
ENDPROC



PROC VAULT_DRILL_ANIMATE_BAG__MESSED_UP(S_VAULT_DRILL_DATA &sVaultDrillData, NETWORK_INDEX &niMinigameObjects[])
	
	IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
		
		PLAY_ENTITY_ANIM(NET_TO_OBJ(niMinigameObjects[1]), "BAG_DRILL_STRAIGHT_FAIL", VAULT_DRILL__GET_ANIM_DICTIONARY_LASER(),
						 INSTANT_BLEND_IN, TRUE, FALSE, DEFAULT,DEFAULT,ENUM_TO_INT(AF_USE_KINEMATIC_PHYSICS))
	ELSE
		PLAY_ENTITY_ANIM(NET_TO_OBJ(niMinigameObjects[1]), "BAG_DRILL_STRAIGHT_FAIL", VAULT_DRILL__GET_ANIM_DICTIONARY_DRILL(),
						 INSTANT_BLEND_IN, TRUE, FALSE, DEFAULT, DEFAULT, ENUM_TO_INT(AF_USE_KINEMATIC_PHYSICS))

	ENDIF
	
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_OBJ(niMinigameObjects[1]))		
ENDPROC



///********************************************************************************
///    
///    CORE LOOP	CORE LOOP	CORE LOOP	CORE LOOP	CORE LOOP 	CORE LOOP
///    
///********************************************************************************    


PROC RUN_VAULT_DRILL_MINIGAME(S_VAULT_DRILL_DATA &sVaultDrillData, NETWORK_INDEX &niMinigameObjects[], INT &iSafetyTimer, OBJECT_INDEX oiCentreObject, INT iLocalPart, INT iNumberOfVaults = -1, INT iObj = -1, BOOL bLaunchFromFMMC = TRUE, MODEL_NAMES mnBag = HEI_P_M_BAG_VAR22_ARM_S, MP_HEIST_GEAR_ENUM eBag = HEIST_GEAR_SPORTS_BAG)
	#IF IS_DEBUG_BUILD
		UPDATE_VAULT_DRILL_MINIGAME_DEBUG(sVaultDrillData)
		
		//Comment out for check ins
		//VAULT_DRILL_DEBUG_DISPLAY_VARIABLES(sVaultDrillData)
	#ENDIF
	
	///Control mods
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
	
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
	
	
	///Local variables
	PED_INDEX 	LocalPlayerPed  	= PLAYER_PED_ID()
	BOOL 		bIsLocalPlayerHost 	= NETWORK_IS_HOST_OF_THIS_SCRIPT()
	FLOAT 		fRot
	
	STRING sVaultDrillingAnimDict 				=	VAULT_DRILL__GET_ANIM_DICTIONARY_DRILL()
	STRING sVaultDrillingAnimDict_Laser 		= 	VAULT_DRILL__GET_ANIM_DICTIONARY_LASER()

	OBSTRUCTION_BOUNDS obNextObstruction
	
	VECTOR vVaultDrill_SceneNodeOffsetFromObj, vVaultDrill_SceneRoot 				
	
	//stand back so we can see the beam
	IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
		vVaultDrill_SceneNodeOffsetFromObj 		=	VAULT_DRILL__GET_OFFSET_FOR_MINIGAME_ROOT_LASER()
		vVaultDrill_SceneRoot 					= 	GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiCentreObject, vVaultDrill_SceneNodeOffsetFromObj)
	ELSE
		vVaultDrill_SceneNodeOffsetFromObj 	= 	VAULT_DRILL__GET_OFFSET_FOR_MINIGAME_ROOT()
		vVaultDrill_SceneRoot 				= 	GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiCentreObject, vVaultDrill_SceneNodeOffsetFromObj)
	ENDIF
	
	
//	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//	DRAW_DEBUG_SPHERE(vVaultDrill_SceneRoot, 0.5, 0, 0, 255, 30)
	
	SWITCH sVaultDrillData.iCurrentState
		CASE VAULT_DRILL_MINIGAME_STATE_INITIALISE
			INITIALISE_VAULT_MINIGAME_VALUES(sVaultDrillData)
			
			IF bLaunchFromFMMC
				IF NOT IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_ALREADY_STARTED)
					sVaultDrillData.iCurrentObstruction  = 0
					obNextObstruction 					 = VAULT_DRILL_GET_OBSTRUCTION_BOUNDS(sVaultDrillData)
					sVaultDrillData.fCurrentProgress 	 = obNextObstruction.fStartBound
					PRINTLN("[RUN VAULT_DRILLING] - Setting progress to default FMMC.")
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_DID_PLAYER_BACK_OUT)	
					obNextObstruction 					 = VAULT_DRILL_GET_OBSTRUCTION_BOUNDS(sVaultDrillData)
					sVaultDrillData.fCurrentProgress 	 = obNextObstruction.fStartBound
					PRINTLN("[RUN VAULT_DRILLING] - Setting progress to default ARCADE.")
				ENDIF
			ENDIF
			
			//If this is a second attempt at the game then reset the backing out flag.
			CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_DID_PLAYER_BACK_OUT)
			CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_ALREADY_STARTED)
			
			SETUP_PC_VAULT_DRILLING_CONTROLS(sVaultDrillData, iLocalPart)
	
			//Next State
			PRINTLN("[RUN VAULT_DRILLING] - VAULT_DRILL_MINIGAME_STATE_CREATE_PROPS")
			sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_CREATE_PROPS
			
		BREAK
		
		
		CASE VAULT_DRILL_MINIGAME_STATE_CREATE_PROPS
			IF CREATE_VAULT_DRILL_OBJECT(sVaultDrillData, niMinigameObjects, localPlayerPed, bIsLocalPlayerHost, TRUE)
			AND CREATE_VAULT_DRILL_BAG_OBJECT(sVaultDrillData, niMinigameObjects, localPlayerPed, bIsLocalPlayerHost, TRUE, bLaunchFromFMMC, mnBag)	
			AND CREATE_VAULT_DRILL_HOLE_OBJECT(sVaultDrillData, niMinigameObjects, oiCentreObject, localPlayerPed , bIsLocalPlayerHost, GET_ENTITY_COORDS(oiCentreObject) , GET_ENTITY_ROTATION(oiCentreObject) + <<0.0, 0.0, 0.0>>)

				PRINTLN("[RUN VAULT_DRILLING] - VAULT_DRILL_MINIGAME_STATE_PLAY_INTRO")
				sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_PLAY_INTRO			
			ENDIF
		BREAK
		
		CASE VAULT_DRILL_MINIGAME_STATE_PLAY_INTRO
			
		
			fRot = GET_ENTITY_HEADING(oiCentreObject)
			MOVE_INITIAL_PARAMETERS moveNetParams
			
			IF NOT bLaunchFromFMMC
				moveNetParams.boolParamName0 = 	"No_Bag"
				moveNetParams.boolParamValue0 = TRUE
			ENDIF
			
			
			
			IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
				moveNetParams.clipSetHash0 = HASH("clipset@ANIM_HEIST@HS3F@IG9_VAULT_DRILL@LASER_DRILL@")
				
				
				TASK_MOVE_NETWORK_ADVANCED_BY_NAME_WITH_INIT_PARAMS(LocalPlayerPed, "Heist3_minigame_drill_vault", moveNetParams, vVaultDrill_SceneRoot, <<0.0, 0.0, fRot>>, DEFAULT,
											   0.5, DEFAULT, sVaultDrillingAnimDict_Laser, MOVE_USE_KINEMATIC_PHYSICS)
			ELSE
				moveNetParams.clipSetHash0 = HASH("clipset@ANIM_HEIST@HS3F@IG9_VAULT_DRILL@DRILL@")
				TASK_MOVE_NETWORK_ADVANCED_BY_NAME_WITH_INIT_PARAMS(LocalPlayerPed, "Heist3_minigame_drill_vault",moveNetParams, vVaultDrill_SceneRoot, <<0.0, 0.0, fRot>>, DEFAULT,
											   0.5, DEFAULT, sVaultDrillingAnimDict, MOVE_USE_KINEMATIC_PHYSICS)
			ENDIF

			//2119705 - Freezing the player stops issues with physics affecting the move network and causing the player to not line up with the door.
			FREEZE_ENTITY_POSITION(LocalPlayerPed, TRUE)
			SET_USE_KINEMATIC_PHYSICS(LocalPlayerPed, TRUE)
			
			IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
				
				SET_TASK_MOVE_NETWORK_ENABLE_COLLISION_ON_NETWORK_CLONE_WHEN_FIXED(LocalPlayerPed, TRUE)
				
				VAULT_DRILL_STOP_SYNC_SCENE(sVaultDrillData.iSyncScene)
				
				//This makes sure the move network phase is in sync with the bag: bForceZeroTimestep needs to be TRUE otherwise the move network ends up ahead.
				FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, DEFAULT, TRUE) 
				
				sVaultDrillData.iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vVaultDrill_SceneRoot, <<0.0, 0.0, fRot>>, DEFAULT, TRUE)
			
				IF bLaunchFromFMMC
					FREEZE_ENTITY_POSITION(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
										
					IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)				   
						NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[1]), sVaultDrillData.iSyncScene,
														  			sVaultDrillingAnimDict_Laser, "bag_intro", INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)
						
						NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(sVaultDrillData.iSyncScene, sVaultDrillingAnimDict_Laser, "intro_cam")
					ELSE										
						NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[1]), sVaultDrillData.iSyncScene,
														  			sVaultDrillingAnimDict, "bag_intro", INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)
																	
						NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(sVaultDrillData.iSyncScene, sVaultDrillingAnimDict, "intro_cam")
					ENDIF
				ELSE
					IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)				   
						NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(sVaultDrillData.iSyncScene, sVaultDrillingAnimDict_Laser, "intro_nobag_cam")
					ELSE										
						NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(sVaultDrillData.iSyncScene, sVaultDrillingAnimDict, "intro_nobag_cam")
					ENDIF
				ENDIF
				
				
				NETWORK_FORCE_LOCAL_USE_OF_SYNCED_SCENE_CAMERA(sVaultDrillData.iSyncScene)
				NETWORK_START_SYNCHRONISED_SCENE(sVaultDrillData.iSyncScene)
				
				IF bLaunchFromFMMC
					//Set the props to be visible again just in case something happened with the anim.
					IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(niMinigameObjects[1]))
						
						REMOVE_VAULT_DRILL_PLAYER_BAG()
						
						SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
					ENDIF
				ENDIF
				SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[0]), FALSE) 
				
				
				PRINTLN("[RUN VAULT_DRILLING] - VAULT_DRILL_MINIGAME_STATE_HIDE_BAG_AND_WAIT_FOR_ANIM")
				sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_HIDE_BAG_AND_WAIT_FOR_ANIM
			
			ENDIF
		BREAK
		
		CASE VAULT_DRILL_MINIGAME_STATE_HIDE_BAG_AND_WAIT_FOR_ANIM
			IF NOT (IS_SCRIPTED_CONVERSATION_ONGOING() AND IS_SUBTITLE_PREFERENCE_SWITCHED_ON() AND IS_MESSAGE_BEING_DISPLAYED())
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
			ENDIF
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			THEFEED_HIDE_THIS_FRAME() 
		
			CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(LocalPlayerPed), 3.0, TRUE)
			
			
			//Hide player's weapon.
			SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
			SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, FALSE, FALSE)
			
			
			IF bLaunchFromFMMC
				REMOVE_VAULT_DRILL_PLAYER_BAG()
			ENDIF
			PRINTLN("[RUN VAULT_DRILLING] - VAULT_DRILL_MINIGAME_STATE_WAIT_FOR_INTRO")
			sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_WAIT_FOR_INTRO
			
		BREAK
		
		
		CASE VAULT_DRILL_MINIGAME_STATE_WAIT_FOR_INTRO
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			THEFEED_HIDE_THIS_FRAME() 
			
			
			//Handle making the drill visible: needs to be synced with the player pulling the drill out the bag.
			
			
			IF bLaunchFromFMMC
				IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sVaultDrillData.iSyncScene))
					//Do the bag swap once the scene is playing and the component bag is gone.
					IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(niMinigameObjects[1]))
					AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(LocalPlayerPed) 
						SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
					ENDIF
					
					IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sVaultDrillData.iSyncScene)) >= 0.36
						SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[0]), TRUE)
					ENDIF
				ENDIF
			ELSE
				IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sVaultDrillData.iSyncScene))
					IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sVaultDrillData.iSyncScene)) >= 0.16
						SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[0]), TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF (IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed) AND GET_TASK_MOVE_NETWORK_EVENT(LocalPlayerPed, "IntroFinished"))
				IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
				AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(LocalPlayerPed) 
					IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(LocalPlayerPed, "Cutting")
						//Set the props to be visible again just in case something happened with the anim.
						
						IF bLaunchFromFMMC
							IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(niMinigameObjects[1]))
								REMOVE_VAULT_DRILL_PLAYER_BAG()
								SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
							ENDIF
						ENDIF
						SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[0]), TRUE) 
						
						NETWORK_STOP_SYNCHRONISED_SCENE(sVaultDrillData.iSyncScene)											
						sVaultDrillData.iSyncScene = -1
						
						
						
						IF bLaunchFromFMMC		
							VAULT_DRILL_ANIMATE_BAG__IDLE(sVaultDrillData, niMinigameObjects, LocalPlayerPed)	
						ENDIF
											
						CREATE_VAULT_DRILL_CAM(sVaultDrillData, LocalPlayerPed)

						IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
							DISPLAY_HELP_TEXT_THIS_FRAME("MC_LASER_3",TRUE)
						ELSE
							DISPLAY_HELP_TEXT_THIS_FRAME("MC_DRILL_3",TRUE)
						ENDIF
						
						sVaultDrillData.iDrillSoundID = GET_SOUND_ID()
						
						IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
							PLAY_SOUND_FROM_ENTITY(-1, "laser_power_up", LocalPlayerPed, "dlc_ch_heist_finale_laser_drill_sounds", TRUE, 20)
							IF sVaultDrillData.iDrillSoundID = -1
								
								PLAY_SOUND_FROM_ENTITY(sVaultDrillData.iDrillSoundID, "laser_drill", LocalPlayerPed, "dlc_ch_heist_finale_laser_drill_sounds", TRUE, 0)
								SET_VARIABLE_ON_SOUND(sVaultDrillData.iDrillSoundID, "DrillState", 0.0)
								SET_VARIABLE_ON_SOUND(sVaultDrillData.iDrillSoundID, "DrillHeat", 0.0)
							ENDIF
						ENDIF
						
						PRINTLN("[RUN VAULT_DRILLING] - VAULT_DRILL_MINIGAME_STATE_DRILLING")
						sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_DRILLING
					ELSE
						PRINTLN("[RUN VAULT_DRILLING] - VAULT_DRILL_MINIGAME_STATE_DRILLING fail 3")
					ENDIF
				ELSE
					PRINTLN("[RUN VAULT_DRILLING] - VAULT_DRILL_MINIGAME_STATE_DRILLING fail 2")
				ENDIF
			ELSE
				PRINTLN("[RUN VAULT_DRILLING] - VAULT_DRILL_MINIGAME_STATE_DRILLING fail 1")
			ENDIF
			
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
			
	
		BREAK
		
		
		
		CASE VAULT_DRILL_MINIGAME_STATE_DRILLING
			
			
			///
			///       	MAIN INPUTS
			///       
			
			VAULT_DRILL_HANDLE_INPUTS(sVaultDrillData)
			
			///
			///       	SETTING CORE VARIABLES
			///       
			CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_IS_IN_SWEET_SPOT)
			obNextObstruction = VAULT_DRILL_GET_OBSTRUCTION_BOUNDS(sVaultDrillData)
			
			//sVaultDrillData.fDrillBitAnimSpeed = sVaultDrillData.fDrillBitAnimSpeed + ((sVaultDrillData.fCurrentDrillPower - sVaultDrillData.fDrillBitAnimSpeed) * 0.8)
			sVaultDrillData.fDrillBitAnimSpeed = sVaultDrillData.fCurrentDrillPower 
			
			//Have a power modifier value that kicks in when a pin breaks and gradually dies down. If the player stops drilling it dies down more quickly.
			sVaultDrillData.fPinBreakPowerModifier = sVaultDrillData.fPinBreakPowerModifier -@ (VAULT_DRILL_PIN_BREAK_POWER_MODIFIER_DECEL + (0.2 * (1.0 - sVaultDrillData.fCurrentDrillPower)))
			
			//Multiplier for the drill power, with the 
			sVaultDrillData.fCurrentDrillPower *= (1.0 + (sVaultDrillData.fPinBreakPowerModifier * VAULT_DRILL_PIN_BREAK_POWER_MODIFER_MAX))

			//Set the visible drill bit position: the drill can't go further than the current progress. If the player is trying to do this then shake the drill bit.
			sVaultDrillData.fCurrentDrillBitPos = sVaultDrillData.fCurrentDrillBitPos + ((sVaultDrillData.fCurrentDrillPosY - sVaultDrillData.fCurrentDrillBitPos) * 0.5)
			
			VAULT_DRILL_SET_CURRENT_SWEET_SPOT(sVaultDrillData, obNextObstruction)			
			
			///
			///			USING CORE VARIABLES       
			///       
			
			//Set the weights for the drilling move network. Network games are limited to 2 signals per frame so we need to stagger them.
			IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
				FLOAT fDrillZAxis
				fDrillZAxis = sVaultDrillData.fCurrentForwardLeanPos + sVaultDrillData.fForwardLeanExtraAmountForPins
				
				IF fDrillZAxis >0.9999
					fDrillZAxis = 0.9999
				ENDIF
								
				SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(LocalPlayerPed, "z_axis", fDrillZAxis)
				SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(LocalPlayerPed, "drill_force", 1.0 - sVaultDrillData.fCurrentDrillAnimPower)
			ENDIF
			
			//Attach the drill hole object to the door once the player has drilled a little bit into the lock.
			IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[3])
				OBJECT_INDEX objDrillHole
				objDrillHole = NET_TO_OBJ(niMinigameObjects[3])
			
				IF sVaultDrillData.fCurrentProgress > VAULT_DRILL_START_POINT
					SET_ENTITY_VISIBLE(objDrillHole, TRUE)
				ENDIF
			ENDIF
			
			//DRILL/ LASER EFFECTS
			HANDLE_VAULT_DRILL_AND_LASER_PTFX(sVaultDrillData, niMinigameObjects, iObj, iLocalPart)
			
			IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
				IF sVaultDrillData.fCurrentSweetSpotMax > 1.0
					sVaultDrillData.fCurrentSweetSpotMax = 1.0
				ENDIF
			ELSE
				IF sVaultDrillData.fCurrentSweetSpotMax > 0.98
					sVaultDrillData.fCurrentSweetSpotMax = 0.98
				ENDIF
			ENDIF	
			
			
			//Handle progress and overheat updates 
			IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
				PROCESS_DRILLING__LASER(sVaultDrillData, obNextObstruction)
			ELSE
				PROCESS_DRILLING__DRILL(sVaultDrillData, obNextObstruction)
			ENDIF
		
			HANDLE_VAULT_DRILL_HEAT_LEVEL(sVaultDrillData)
					
			SHAKE_VAULT_DRILL_BIT(sVaultDrillData)
			SHAKE_VAULT_DRILL_PLAYER(sVaultDrillData)
			 
			HANDLE_OBSTRUCTION_BREAKING(sVaultDrillData, obNextObstruction)
			MANAGE_DRILL_HOLE(sVaultDrillData, niMinigameObjects)
			//Passing logic
			IF (sVaultDrillData.fCurrentProgress >= obNextObstruction.fEndBound AND sVaultDrillData.iCurrentObstruction = sVaultDrillData.iObstructions)
				IF NOT IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_HAS_PLAYER_PASSED_MINIGAME)
					//Guarantee that we;ve gone past the point where the drill HUD cracks.
					sVaultDrillData.fCurrentProgress = 0.99
				
					IF GET_GAME_TIMER() - sVaultDrillData.iPassTimer > 500
						CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] Progress complete, passing minigame.")
						SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_HAS_PLAYER_PASSED_MINIGAME)
					ENDIF
				ENDIF
			ELSE
				sVaultDrillData.iPassTimer = GET_GAME_TIMER()
			ENDIF
						
			
			///
			///       	CAPPING CORE VARIABLES
			///       
			IF sVaultDrillData.fPinBreakPowerModifier 	> 1.0
				sVaultDrillData.fPinBreakPowerModifier 	= 1.0
			ENDIF
			
			IF sVaultDrillData.fPinBreakPowerModifier 	< 0.0
				sVaultDrillData.fPinBreakPowerModifier	= 0.0
			ENDIF
			
			IF sVaultDrillData.fCurrentDrillPower 		> 1.0
				sVaultDrillData.fCurrentDrillPower 		= 1.0
			ENDIF
			
			IF sVaultDrillData.fCurrentDrillBitPos 		> sVaultDrillData.fCurrentProgress
				sVaultDrillData.fCurrentDrillBitPos 	= sVaultDrillData.fCurrentProgress
			ENDIF
			
			IF sVaultDrillData.fCurrentDrillBitPos 		> 1.0
				sVaultDrillData.fCurrentDrillBitPos 	= 1.0
			ENDIF
			
			IF sVaultDrillData.fCurrentProgress		 	> 1.0
				sVaultDrillData.fCurrentProgress 		= 1.0
			ENDIF
			
			//Capping sweet spots
			IF sVaultDrillData.fCurrentSweetSpotMin 	< 0.0
				sVaultDrillData.fCurrentSweetSpotMin 	= 0.0
			ENDIF
			
			IF sVaultDrillData.fCurrentHeatLevel 		> 1.0
				sVaultDrillData.fCurrentHeatLevel 		= 1.0
			ENDIF
			
			///
			///       ANIMS
			///       
			
			UPDATE_VAULT_DRILLING_CAMERA(sVaultDrillData)
			IF NOT IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
				UPDATE_VAULT_DRILL_BIT_ANIM(niMinigameObjects, sVaultDrillData.fDrillBitAnimSpeed)
			ENDIF
			
			VAULT_DRILL_HANDLE_DRILL_AUDIO(sVaultDrillData, localPlayerPed)
			
			///
			///       HUD
			///       
			IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
				DISPLAY_HELP_TEXT_THIS_FRAME("MC_LASER_3",TRUE)
			ELSE
				DISPLAY_HELP_TEXT_THIS_FRAME("MC_DRILL_3",TRUE)
			ENDIF
			
			//Update HUD
			IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
				DRAW_VAULT_LASER_MINIGAME_SCALEFORM_HUD(sVaultDrillData)
			ELSE
				DRAW_VAULT_DRILL_MINIGAME_SCALEFORM_HUD(sVaultDrillData)
			ENDIF
			
			///
			///       BREAKOUTS
			///       
			IF HAS_PLAYER_BACKED_OUT_OF_VAULT_DRILLING(sVaultDrillData, localPlayerPed)
				INT iOrderedPart 
				iOrderedPart = RETURN_ORDERED_PARTICIPANT_NUMBER(iLocalPart)
	
				IF NOT IS_BIT_SET(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_STOP_PTFX) 
					IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
						BROADCAST_VAULT_DRILL_EFFECT_EVENT(iOrderedPart, iObj, niMinigameObjects[0] , FALSE, TRUE)
					ELSE
						BROADCAST_VAULT_DRILL_EFFECT_EVENT(iOrderedPart, iObj, niMinigameObjects[0] , FALSE, FALSE)
					ENDIF
				ENDIF
				
				VAULT_DRILL_TRIGGER_OUTRO(sVaultDrillData, niMinigameObjects, iSafetyTimer, oiCentreObject, localPlayerPed, vVaultDrill_SceneRoot, bLaunchFromFMMC)
				
				IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
					PLAY_SOUND_FROM_ENTITY(-1, "laser_power_down", LocalPlayerPed, "dlc_ch_heist_finale_laser_drill_sounds", TRUE, 20)
				ELSE
					IF NOT HAS_SOUND_FINISHED(sVaultDrillData.iDrillSoundID)
						STOP_SOUND(sVaultDrillData.iDrillSoundID)
					ENDIF
				ENDIF
								
				
				IF bLaunchFromFMMC
					VAULT_DRILL__PRELOAD_BAG(LocalPlayerPed, eBag)
					
					SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_HAS_PLAYER_SET_TO_BACK_OUT)
				ELSE
					
					SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_HAS_PLAYER_SET_TO_BACK_OUT)
				ENDIF
				
				sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_WAIT_FOR_OUTRO
				EXIT
			ENDIF
			
			//Work out which transition to play and play it.
			IF HAS_PLAYER_MESSED_UP_VAULT_DRILLING(sVaultDrillData)
				IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
				AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(LocalPlayerPed) 	
				
					IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(LocalPlayerPed, "LeftFailure")
						IF sVaultDrillData.iDrillSoundID != -1
							SET_VARIABLE_ON_SOUND(sVaultDrillData.iDrillSoundID, "DrillState", 0.0)
						ENDIF
						
						SET_EXPECTED_CLONE_NEXT_TASK_MOVE_NETWORK_STATE(LocalPlayerPed, "Cutting")

						IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
							sVaultDrillData.iDrillOverheatSoundID = GET_SOUND_ID()
						
							PLAY_SOUND_FROM_ENTITY(sVaultDrillData.iDrillOverheatSoundID, "laser_overheat", LocalPlayerPed, "dlc_ch_heist_finale_laser_drill_sounds", TRUE, 20)
						ELSE
							PLAY_SOUND_FROM_ENTITY(-1, "Drill_Jam", LocalPlayerPed, "DLC_HEIST_FLEECA_SOUNDSET", TRUE, 20)
							
							IF NOT HAS_SOUND_FINISHED(sVaultDrillData.iDrillSoundID)
								STOP_SOUND(sVaultDrillData.iDrillSoundID)
							ENDIF
						ENDIF
										
						PRINTLN("[RCC MISSION][TELEMETRY] HACKING (DRILLING) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 1")
						SET_BIT(sVaultDrillData.iBitset , VAULT_DRILL_BITSET_MESS_UP_TRIGGERED_FOR_TELEMETRY)
						sVaultDrillData.fDrillBitAnimSpeed = 1.0
						
						IF bLaunchFromFMMC
							VAULT_DRILL_ANIMATE_BAG__MESSED_UP(sVaultDrillData, niMinigameObjects)
						ENDIF
						sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_MESSEDUP
						EXIT
					ENDIF
				ENDIF
			ENDIF
		
			
			//If we win then play an outro.
			IF VAULT_DRILL_HAS_PLAYER_COMPLETED(sVaultDrillData, localPlayerPed, iLocalPart)
				INT iOrderedPart 
				iOrderedPart = RETURN_ORDERED_PARTICIPANT_NUMBER(iLocalPart)
				
				IF NOT IS_BIT_SET(iVaultDrillLocalBitSet[iOrderedPart], VAULT_DRILL_LOCAL_BITSET_STOP_PTFX) 
					IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
						BROADCAST_VAULT_DRILL_EFFECT_EVENT(iOrderedPart, iObj, niMinigameObjects[0] , FALSE, TRUE)
					ELSE
						BROADCAST_VAULT_DRILL_EFFECT_EVENT(iOrderedPart, iObj, niMinigameObjects[0] , FALSE, FALSE)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(sVaultDrillData.iBitset , VAULT_DRILL_BITSET_SHOULD_SKIP_OUTRO) AND iNumberOfVaults <= 1
					VAULT_DRILL_GAME_ENDING_SKIP(sVaultDrillData, niMinigameObjects, iSafetyTimer, localPlayerPed, iLocalPart, eBag, bLaunchFromFMMC)
					
					sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_CLEANUP
					EXIT
				ELSE
					VAULT_DRILL_TRIGGER_OUTRO(sVaultDrillData, niMinigameObjects, iSafetyTimer, oiCentreObject, localPlayerPed, vVaultDrill_SceneRoot, bLaunchFromFMMC)
										
					IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
						IF sVaultDrillData.iDrillSoundID != -1
							STOP_SOUND(sVaultDrillData.iDrillSoundID)
							
							RELEASE_SOUND_ID(sVaultDrillData.iDrillSoundID)
							sVaultDrillData.iDrillSoundID = -1
						ENDIF
					
						PLAY_SOUND_FROM_ENTITY(-1, "laser_power_down", LocalPlayerPed, "dlc_ch_heist_finale_laser_drill_sounds", TRUE, 20)
					ENDIF
					
					sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_WAIT_FOR_OUTRO
					EXIT
				ENDIF
			ENDIF
			
			//If the heat level reaches max then put us in the overheat state.
			IF sVaultDrillData.fCurrentHeatLevel >= 0.99
				CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] Player messed up.")
				PLAYER_MESSED_UP_VAULT_DRILLING(sVaultDrillData)
				EXIT
			ENDIF
									
			//If the player loses their move network task then reset.
			IF NOT IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
				DELETE_VAULT_DRILL_OBJECT(niMinigameObjects)
				iSafetyTimer = GET_GAME_TIMER()
				sVaultDrillData.iCurrentState	= VAULT_DRILL_MINIGAME_STATE_FAILSAFE
				EXIT
			ENDIF
		BREAK
		
		
		CASE VAULT_DRILL_MINIGAME_STATE_WAIT_FOR_OUTRO
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			THEFEED_HIDE_THIS_FRAME() 
			IF NOT (IS_SCRIPTED_CONVERSATION_ONGOING() 
					AND IS_SUBTITLE_PREFERENCE_SWITCHED_ON() 
					AND IS_MESSAGE_BEING_DISPLAYED())
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
			ENDIF
		
			IF DOES_CAM_EXIST(sVaultDrillData.cam)
				DESTROY_CAM(sVaultDrillData.cam)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
			ENDIF
			
			VAULT_DRILL_STOP_AND_CLEANUP_PTFX_AND_SOUND(sVaultDrillData, iLocalPart)
			
			
			//Drill bit speed is set to 1.0 at the start of this sequence, gradually bring it down to zero.
			sVaultDrillData.fDrillBitAnimSpeed = sVaultDrillData.fDrillBitAnimSpeed * 0.75
			
			IF sVaultDrillData.fDrillBitAnimSpeed > 1.0
				sVaultDrillData.fDrillBitAnimSpeed = 1.0
			ELIF sVaultDrillData.fDrillBitAnimSpeed < 0.0
				sVaultDrillData.fDrillBitAnimSpeed = 0.0 
			ENDIF
			
			IF NOT IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
				UPDATE_VAULT_DRILL_BIT_ANIM(niMinigameObjects, sVaultDrillData.fDrillBitAnimSpeed)
				
				IF sVaultDrillData.iDrillSoundID != -1
					STOP_SOUND(sVaultDrillData.iDrillSoundID)
					
					RELEASE_SOUND_ID(sVaultDrillData.iDrillSoundID)
					sVaultDrillData.iDrillSoundID = -1
				ENDIF
			ELSE
				VAULT_DRILL_HANDLE_DRILL_AUDIO(sVaultDrillData, localPlayerPed)
			
			ENDIF
			
			//Handle deleting the drill and the safety deposit box: both need to be synched with when they go inside the bag.
			IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sVaultDrillData.iSyncScene))
				IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sVaultDrillData.iSyncScene)) >= 0.38
					DELETE_VAULT_DRILL_OBJECT(niMinigameObjects)
				ENDIF
			ENDIF
	
			IF bLaunchFromFMMC
				VAULT_DRILL__PRELOAD_BAG(LocalPlayerPed, eBag)
			ENDIF
			
			//Bag takes a few frames to appear
			IF bLaunchFromFMMC
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sVaultDrillData.iSyncScene))
				AND GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sVaultDrillData.iSyncScene)) >= 0.8)
				OR VAULT_DRILL_HAS_TIMER_PASSED(iSafetyTimer, 11000)
					VAULT_DRILL__APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, eBag)
				
				ENDIF
			ENDIF
			
			
			IF (IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sVaultDrillData.iSyncScene))
			AND GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sVaultDrillData.iSyncScene)) >= 0.9)
			OR VAULT_DRILL_HAS_TIMER_PASSED(iSafetyTimer, 12000)
				IF bLaunchFromFMMC
					VAULT_DRILL__APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, eBag)
				ENDIF
				UNLOCK_MINIMAP_ANGLE()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
				iSafetyTimer = GET_GAME_TIMER()
				sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_CLEANUP
			ENDIF
		BREAK

		CASE VAULT_DRILL_MINIGAME_STATE_CLEANUP
			IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_HAS_PLAYER_SET_TO_BACK_OUT)
				SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_IS_BACK_OUT_COMPLETE)
				SET_PLAYER_BACKED_OUT_OF_VAULT_DRILLING(sVaultDrillData)
			ELSE
				SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_IS_MINIGAME_COMPLETE)
			ENDIF
			
			IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
				SET_TASK_MOVE_NETWORK_ENABLE_COLLISION_ON_NETWORK_CLONE_WHEN_FIXED(LocalPlayerPed, FALSE)
			ENDIF
			
			CALL_SCALEFORM_MOVIE_METHOD(sVaultDrillData.sfDrillHud , "RESET")
						
			VAULT_DRILL_STOP_AND_CLEANUP_PTFX_AND_SOUND(sVaultDrillData, iLocalPart)
			
			DELETE_VAULT_DRILL_OBJECT(niMinigameObjects)
			DELETE_VAULT_DRILL_BAG_OBJECT(niMinigameObjects)
			
			//This should only get hit if the mission failed while drilling, during normal flow the camera has already been destroyed.
			IF DOES_CAM_EXIST(sVaultDrillData.cam)
				DESTROY_CAM(sVaultDrillData.cam)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
			ENDIF
			
			UNLOCK_MINIMAP_ANGLE()
			
			BROADCAST_SCRIPT_EVENT_FMMC_VAULT_DRILL_ASSET(ciEVENT_VAULT_DRILL_ASSET_CLEANUP)
			
			IF sVaultDrillData.iDrillSoundID != -1
				STOP_SOUND(sVaultDrillData.iDrillSoundID)
				
				RELEASE_SOUND_ID(sVaultDrillData.iDrillSoundID)
				sVaultDrillData.iDrillSoundID = -1
			ENDIF
			
			IF sVaultDrillData.iDrillOverheatSoundID != -1
				IF NOT HAS_SOUND_FINISHED(sVaultDrillData.iDrillOverheatSoundID)
					STOP_SOUND(sVaultDrillData.iDrillOverheatSoundID)
				ENDIF
				
				RELEASE_SOUND_ID(sVaultDrillData.iDrillOverheatSoundID)
				sVaultDrillData.iDrillOverheatSoundID = -1
			ENDIF
			
						
			IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_HEIST3\\HEIST_FINALE_LASER_DRILL")
			ELSE
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL")
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL_2")
			ENDIF
			
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sVaultDrillData.sfDrillHud)
			PRINTLN("RELEASE_SCRIPT_AUDIO_BANK - Called from netVaultDrill.sch ")
			
			CLEANUP_PC_VAULT_DRILLING_CONTROLS(sVaultDrillData, iLocalPart)

			VAULT_DRILL_STOP_SYNC_SCENE(sVaultDrillData.iSyncScene)

			FREEZE_ENTITY_POSITION(LocalPlayerPed, FALSE)
			
			CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
		
			IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_IS_MINIGAME_COMPLETE)
				PRINTLN("[DRILLING] - [RCC MISSION] DRILL_STATE_DO_BAG_SWAP - Tasking player to turn towards exit.")
				TASK_TURN_PED_TO_FACE_COORD(LocalPlayerPed, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(LocalPlayerPed, <<4.0, 0.0, 0.0>>))
			ENDIF
			
			
			IF sVaultDrillData.iSyncScene != -1
				IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sVaultDrillData.iSyncScene))
					NETWORK_STOP_SYNCHRONISED_SCENE(sVaultDrillData.iSyncScene)
				ENDIF
				sVaultDrillData.iSyncScene = -1
			ENDIF
			
			CDEBUG1LN(DEBUG_MISSION, "[RUN_VAULT_DRILL_MINIGAME] Reset to init.")
			sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_INITIALISE	
		BREAK
		
		
		///
		///      SPECIAL STATES
		///      
		
		CASE VAULT_DRILL_MINIGAME_STATE_MESSEDUP
			UPDATE_VAULT_DRILLING_CAMERA(sVaultDrillData)
			
			sVaultDrillData.fCurrentForwardLeanPos = 0.0
			
			VAULT_DRILL_HANDLE_INPUTS(sVaultDrillData)
			
			//Set the visible drill bit position: the drill can't go further than the current progress. If the player is trying to do this then shake the drill bit.
			sVaultDrillData.fCurrentDrillBitPos = sVaultDrillData.fCurrentDrillBitPos + ((sVaultDrillData.fCurrentDrillPosY - sVaultDrillData.fCurrentDrillBitPos) * 0.5)
			
			IF sVaultDrillData.fCurrentDrillBitPos > sVaultDrillData.fCurrentProgress
				sVaultDrillData.fCurrentDrillBitPos = sVaultDrillData.fCurrentProgress
			ENDIF
			
			IF sVaultDrillData.iDrillOverheatSoundID != -1
				IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)			
					SET_VARIABLE_ON_SOUND(sVaultDrillData.iDrillOverheatSoundID, "DrillHeat", sVaultDrillData.fCurrentHeatLevel)
				ENDIF
			ENDIF

			IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
				SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(LocalPlayerPed, "z_axis", sVaultDrillData.fCurrentForwardLeanPos + sVaultDrillData.fForwardLeanExtraAmountForPins)
			ENDIF
			
			//Update HUD
			IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
				DRAW_VAULT_LASER_MINIGAME_SCALEFORM_HUD(sVaultDrillData)
				DISPLAY_HELP_TEXT_THIS_FRAME("MC_LASER_4",TRUE)
			ELSE
				DRAW_VAULT_DRILL_MINIGAME_SCALEFORM_HUD(sVaultDrillData)
				DISPLAY_HELP_TEXT_THIS_FRAME("MC_DRILL_4",TRUE)
			ENDIF
		
			HANDLE_VAULT_DRILL_HEAT_LEVEL(sVaultDrillData)
			VAULT_DRILL_STOP_AND_CLEANUP_PTFX_AND_SOUND(sVaultDrillData, iLocalPart)
			
			//Drill bit speed is set to 1.0 at the start of this sequence, gradually bring it down to zero.
			sVaultDrillData.fDrillBitAnimSpeed = sVaultDrillData.fDrillBitAnimSpeed * 0.85
			
			IF NOT IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
				UPDATE_VAULT_DRILL_BIT_ANIM(niMinigameObjects, sVaultDrillData.fDrillBitAnimSpeed)
			ENDIF
			
			
			IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
			OR VAULT_DRILL_HAS_TIMER_PASSED(iSafetyTimer,6000)
				IF GET_TASK_MOVE_NETWORK_EVENT(LocalPlayerPed, "LeftFailFinish")
					IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
					AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(LocalPlayerPed) 
						IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(LocalPlayerPed, "Cutting")
							
							CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_DID_PLAYER_MESS_UP)	
							IF bLaunchFromFMMC
								VAULT_DRILL_ANIMATE_BAG__IDLE(sVaultDrillData, niMinigameObjects, LocalPlayerPed)
							ENDIF
							sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_DRILLING
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
		BREAK
		
		
		CASE VAULT_DRILL_MINIGAME_STATE_FAILSAFE //Enter this case if something goes wrong (i.e. the player is knocked out of their move network tasks, or the mission failed while drilling).
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			THEFEED_HIDE_THIS_FRAME() 
			
			IF bLaunchFromFMMC
				VAULT_DRILL__APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, eBag)
			ENDIF
			iSafetyTimer = GET_GAME_TIMER()
			sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_CLEANUP
		BREAK
		
	ENDSWITCH
ENDPROC
