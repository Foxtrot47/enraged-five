//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_CLOTHING.sch																					//
// Description: Header file containing functionality for packing and unpacking peds clothing components.				//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		03/02/21																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
USING "net_peds_common.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════╡ PACKING CLOTHING COMPONENTS ╞══════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Resets the binary array passed in to zero.
/// PARAMS:
///    iBinaryArray - Array to reset.
PROC RESET_PED_CLOTHING_COMPONENT_BINARY_ARRAY(INT &iBinaryArray[MAX_NUM_PED_CLOTHING_COMPONENT_BITS])
	
	INT iArrayID = 0
	REPEAT MAX_NUM_PED_CLOTHING_COMPONENT_BITS iArrayID
		iBinaryArray[iArrayID] = 0
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Converts a component value into binary and populates the binary array with it. Uses 8 bits.
/// PARAMS:
///    iBinaryArray - Binary array to populate.
///    iCompValue - Component value (either a drawable or texture). This gets converted into binary and stored in iBinaryArray.
PROC POPULATE_PED_CLOTHING_COMPONENT_BINARY_ARRAY(INT &iBinaryArray[MAX_NUM_PED_CLOTHING_COMPONENT_BITS], INT iCompValue)
	
	INT iArrayID = 0
	WHILE (iCompValue > 0 AND iArrayID < MAX_NUM_PED_CLOTHING_COMPONENT_BITS)
		iBinaryArray[iArrayID] = iCompValue%2
		iCompValue/=2
		iArrayID++
	ENDWHILE
	
ENDPROC

/// PURPOSE:
///    Reads the passed in binary array and sets/clears the bitset accordingly. 1 = set bit. 0 = clear bit.
/// PARAMS:
///    iBitset - Packed variable to set.
///    iBinaryArray - Populated binary array.
///    iStartingBitIndex - The starting index of bits to set/clear. Each clothing component uses 8 bits.
PROC SET_PED_CLOTHING_COMPONENT_BITS(INT &iBitset, INT &iBinaryArray[MAX_NUM_PED_CLOTHING_COMPONENT_BITS], INT iStartingBitIndex)
	
	INT iArrayID = 0
	REPEAT MAX_NUM_PED_CLOTHING_COMPONENT_BITS iArrayID
		IF (iBinaryArray[iArrayID] = 1)
			SET_BIT(iBitset, iStartingBitIndex+iArrayID)
		ELSE
			CLEAR_BIT(iBitset, iStartingBitIndex+iArrayID)
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Packs a peds clothing component config.
///    Each ped clothing component (PED_COMP_HEAD etc.) is stored within the PED_CLOTHING_COMPONENT_CONFIG struct. This function takes each value and converts it into binary, setting each binary value in 8 bit sections.
///    Why 8 bit sections?
///    		-The max drawable and texture values differ from ped pack to ped pack. They can be anything from 0-x. They typically max out around 20 (this means there are 20 variations of a t-shirt for example). 
///    		 Numbers 0-15 when converted into binary use 4 bits. Numbers 16-255 use 8 bits, and as drawables and textures can typically surpass 15 we need to use 8 bits to future proof any large ped packs.		
///    		-There are 12 clothing components in total: PED_COMP_HEAD->PED_COMP_JBIB. An INT contains 32 bits, and since we are using 8 bits per clothing component that means we can store 4 clothing components in 1 INT.
///    		 Therefore in order to store all 12 clothing components, we need to use 3 INTs (all 12 clothing components / 4 clothing components per INT = 3 INTs needed). We need to save the drawable AND texture values so thats 6 INTs in total per ped.
///    
///    The below function converts each drawable and texture value into binary and stores each digit of the binary number into an array.
///    The binary array is then iterated over, and if the value is a 1 the bit is set, 0 and the bit is cleared. In doing so we 'pack' the clothing component into the correct 8 bit section.
///    When we 'unpack' the clothing components we do the reverse; read the binary and convert it back into an integer.
///    Ultimately we do this as a way to save variables and more efficiently store the clothing data for each ped.
/// PARAMS:
///    iPackedDrawable - Packed drawable array which holds all the peds clothing component drawables.
///    iPackedTexture - Packed texture array which holds all the peds clothing component textures.
///    ComponentConfig - Struct that contains the ped clothing config values. This data gets packed into iPackedDrawable & iPackedTexture.
PROC PACK_PED_CLOTHING_COMPONENT_CONFIG(INT &iPackedDrawable[MAX_NUM_PED_CLOTHING_COMPONENT_VARS], INT &iPackedTexture[MAX_NUM_PED_CLOTHING_COMPONENT_VARS], PED_CLOTHING_COMPONENT_CONFIG ComponentConfig)
	
	INT iVar
	INT iComp
	INT iCurrentComp
	INT iStartingBitIndex
	
	INT iDrawableBinaryArray[MAX_NUM_PED_CLOTHING_COMPONENT_BITS]
	INT iTextureBinaryArray[MAX_NUM_PED_CLOTHING_COMPONENT_BITS]
	
	REPEAT MAX_NUM_PED_CLOTHING_COMPONENT_VARS iVar
		REPEAT MAX_NUM_PED_CLOTHING_COMPONENT_PER_VAR iComp
			iStartingBitIndex = (iComp*MAX_NUM_PED_CLOTHING_COMPONENT_BITS)
			
			RESET_PED_CLOTHING_COMPONENT_BINARY_ARRAY(iDrawableBinaryArray)
			RESET_PED_CLOTHING_COMPONENT_BINARY_ARRAY(iTextureBinaryArray)
			
			iCurrentComp = iComp+(iVar*MAX_NUM_PED_CLOTHING_COMPONENT_PER_VAR)
			POPULATE_PED_CLOTHING_COMPONENT_BINARY_ARRAY(iDrawableBinaryArray, ComponentConfig.ComponentData[iCurrentComp].iDrawable)
			POPULATE_PED_CLOTHING_COMPONENT_BINARY_ARRAY(iTextureBinaryArray, ComponentConfig.ComponentData[iCurrentComp].iTexture)
			
			SET_PED_CLOTHING_COMPONENT_BITS(iPackedDrawable[iVar], iDrawableBinaryArray, iStartingBitIndex)
			SET_PED_CLOTHING_COMPONENT_BITS(iPackedTexture[iVar], iTextureBinaryArray, iStartingBitIndex)	
		ENDREPEAT
	ENDREPEAT
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════╡ UNPACKING CLOTHING COMPONENTS ╞═════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Converts a binary number into an integer.
/// PARAMS:
///    iBitset - Packed variable to query.
///    iStartingBitIndex - The starting index of bits to set/clear. Each clothing component uses 8 bits.
/// RETURNS: The clothing component as an integer. Converted from binary.
FUNC INT GET_PED_CLOTHING_COMPONENT_VALUE(INT iBitset, INT iStartingBitIndex)
	
	INT iBit
	INT iValue = 0
	
	REPEAT MAX_NUM_PED_CLOTHING_COMPONENT_BITS iBit
		IF IS_BIT_SET(iBitset, iStartingBitIndex+iBit)
			iValue += FLOOR(POW(2.0, TO_FLOAT(iBit)))
		ENDIF
	ENDREPEAT
	
	RETURN iValue
ENDFUNC

/// PURPOSE:
///    Unpacks a peds clothing component config.
///    Iterates over the packed drawable and texture vars and converts the binary numbers back into integers, populating the PED_CLOTHING_COMPONENT_CONFIG struct.
///    We can then apply the values in the PED_CLOTHING_COMPONENT_CONFIG struct to a ped via the function: APPLY_PED_CLOTHING_COMPONENT_CONFIG
/// PARAMS:
///    iPackedDrawable - Packed drawable array which holds all the packed peds clothing component drawables.
///    iPackedTexture - Packed texture array which holds all the packed peds clothing component textures.
///    ComponentConfig - Struct that gets populated with the ped clothing config values. This data is unpacked from iPackedDrawable & iPackedTexture vars.
PROC UNPACK_PED_CLOTHING_COMPONENT_CONFIG(INT &iPackedDrawable[MAX_NUM_PED_CLOTHING_COMPONENT_VARS], INT &iPackedTexture[MAX_NUM_PED_CLOTHING_COMPONENT_VARS], PED_CLOTHING_COMPONENT_CONFIG &ComponentConfig)
	
	INT iVar
	INT iComp
	INT iCurrentComp
	INT iStartingBitIndex
	
	REPEAT MAX_NUM_PED_CLOTHING_COMPONENT_VARS iVar
		REPEAT MAX_NUM_PED_CLOTHING_COMPONENT_PER_VAR iComp
			iStartingBitIndex = (iComp*MAX_NUM_PED_CLOTHING_COMPONENT_BITS)
			iCurrentComp = iComp+(iVar*MAX_NUM_PED_CLOTHING_COMPONENT_PER_VAR)
			
			ComponentConfig.ComponentData[iCurrentComp].iDrawable = GET_PED_CLOTHING_COMPONENT_VALUE(iPackedDrawable[iVar], iStartingBitIndex)
			ComponentConfig.ComponentData[iCurrentComp].iTexture = GET_PED_CLOTHING_COMPONENT_VALUE(iPackedTexture[iVar], iStartingBitIndex)	
		ENDREPEAT
	ENDREPEAT
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════╡ APPLY CLOTHING COMPONENTS ╞═══════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Wrapper function to query if a peds clothing component config is valid.
/// PARAMS:
///    iPackedDrawable - Packed drawable array which holds all the packed peds clothing component drawables.
///    iPackedTexture - Packed texture array which holds all the packed peds clothing component textures.
/// RETURNS: TRUE if a peds clothing component config is valid.
FUNC BOOL IS_PED_CLOTHING_COMPONENT_CONFIG_VALID(INT &iPackedDrawable[MAX_NUM_PED_CLOTHING_COMPONENT_VARS], INT &iPackedTexture[MAX_NUM_PED_CLOTHING_COMPONENT_VARS])
	
	INT iVar
	BOOL bValidConfig = TRUE
	
	REPEAT MAX_NUM_PED_CLOTHING_COMPONENT_VARS iVar
		IF (iPackedDrawable[iVar] = -1 OR iPackedTexture[iVar] = -1)
			bValidConfig = FALSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN bValidConfig
ENDFUNC

/// PURPOSE:
///    Resets all the peds clothing components to zero.
/// PARAMS:
///    PedID - The ped to reset clothing components for.
PROC RESET_PED_CLOTHING_COMPONENTS(PED_INDEX &PedID)
	
	INT iComp
	REPEAT NUM_PED_COMPONENTS iComp
		SET_PED_COMPONENT_VARIATION(PedID, INT_TO_ENUM(PED_COMPONENT, iComp), 0, 0)
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Applies a clothing component config to a ped.
/// PARAMS:
///    PedID - The ped to apply the clothing components for.
///    ComponentConfig - Struct that contains all the clothing component values (drawables and textures).
PROC APPLY_PED_CLOTHING_COMPONENT_CONFIG(PED_INDEX &PedID, PED_CLOTHING_COMPONENT_CONFIG &ComponentConfig)
	
	INT iComp
	REPEAT NUM_PED_COMPONENTS iComp
		SET_PED_COMPONENT_VARIATION(PedID, INT_TO_ENUM(PED_COMPONENT, iComp), ComponentConfig.ComponentData[iComp].iDrawable, ComponentConfig.ComponentData[iComp].iTexture)
	ENDREPEAT
	
ENDPROC
#ENDIF	// FEATURE_HEIST_ISLAND
