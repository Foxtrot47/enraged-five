//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_DANCING.sch																					//
// Description: Header file containing functionality for dancing peds. Handles transitions between music intensities.	//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		24/06/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
USING "net_peds_common.sch"
//USING "fmc_data_crawler.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ FUNCTIONS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Initialise the peds dancing music intensities.
/// PARAMS:
///    ServerBD - Server ped data to set.
///    LocalData - Local ped data to set.
PROC INITIALISE_PED_DANCING_MUSIC_INTENSITY(SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	IF (LocalData.DancingData.eMusicIntensity != g_clubMusicData.eClubMusicIntensity)
		LocalData.DancingData.eMusicIntensity = g_clubMusicData.eClubMusicIntensity
		
		INT iPed
		IF (ServerBD.iMaxLocalPeds > 0)
			REPEAT ServerBD.iMaxLocalPeds iPed
				LocalData.PedData[iPed].dancingData.ePedMusicIntensity = g_clubMusicData.eClubMusicIntensity
			ENDREPEAT
		ENDIF
		
		IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
			PRINTLN("[AM_MP_PEDS] INITIALISE_PED_DANCING_MUSIC_INTENSITY - Set Intensity & Local Ped Intensities to: ", GET_CLUB_MUSIC_INTENSITY_NAME(g_clubMusicData.eClubMusicIntensity))
			EXIT
		ENDIF
		
		IF (ServerBD.iMaxNetworkPeds > 0)
			REPEAT ServerBD.iMaxNetworkPeds iPed
				ServerBD.NetworkPedData[iPed].Data.dancingData.ePedMusicIntensity = g_clubMusicData.eClubMusicIntensity
			ENDREPEAT
		ENDIF
		
		PRINTLN("[AM_MP_PEDS] INITIALISE_PED_DANCING_MUSIC_INTENSITY - Set Intensity & All Ped Intensities to: ", GET_CLUB_MUSIC_INTENSITY_NAME(g_clubMusicData.eClubMusicIntensity))
	ENDIF
	
	IF (LocalData.DancingData.eNextMusicIntensity != g_clubMusicData.eNextClubMusicIntensity)
		LocalData.DancingData.eNextMusicIntensity = g_clubMusicData.eNextClubMusicIntensity
		PRINTLN("[AM_MP_PEDS] INITIALISE_PED_DANCING_MUSIC_INTENSITY - Set Next Intensity to: ", GET_CLUB_MUSIC_INTENSITY_NAME(g_clubMusicData.eNextClubMusicIntensity))
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets the ped dancing data.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    DancingData - Dancing data to set.
PROC SET_PED_DANCING_DATA(PED_LOCATIONS ePedLocation, DANCING_DATA &DancingData)
	DancingData.bPedLocationUsesDancingPeds = DOES_PED_LOCATION_USE_DANCING_PEDS(ePedLocation)
	IF (GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR)
		DancingData.bPedLocationUsesDancingPeds = FALSE
	ENDIF
ENDPROC

/// PURPOSE:
///    Determines if the global dancing data has been initialised.
///    Updates the local ped dancing data to match.
/// PARAMS:
///    ServerBD - Server ped data to set.
///    LocalData - Local ped data to set.
/// RETURNS: TRUE if the ped dancing data has been initialised. FALSE otherwise.
FUNC BOOL HAS_PED_DANCING_DATA_BEEN_INITIALISED(SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	IF (LocalData.DancingData.bPedLocationUsesDancingPeds)
		IF g_iTimeUntilPedDanceTransitionSecs != -1 
		AND g_clubMusicData.eClubMusicIntensity != CLUB_MUSIC_INTENSITY_NULL
			INITIALISE_PED_DANCING_MUSIC_INTENSITY(ServerBD, LocalData)
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the dancing transition state.
/// PARAMS:
///    DancingTransitionState - Dancing transition state.
///    eNewState - New transition state.
PROC SET_PED_DANCING_TRANSITION_STATE(PED_DANCING_TRANSITION_STATES &DancingTransitionState, PED_DANCING_TRANSITION_STATES eNewState)
	IF (DancingTransitionState != eNewState)
		DancingTransitionState = eNewState
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ MAINTAIN ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Maintains the local the music intensity data and ensures it matches the global data.
/// PARAMS:
///    DancingData - Dancing data to query.
PROC MAINTAIN_PED_DANCING_INTENSITY(DANCING_DATA &DancingData)
	
	IF NOT DancingData.bPedLocationUsesDancingPeds
		EXIT
	ENDIF
	
	IF (DancingData.eMusicIntensity != g_clubMusicData.eClubMusicIntensity)
		DancingData.eMusicIntensity = g_clubMusicData.eClubMusicIntensity
	ENDIF
	
	IF (DancingData.eNextMusicIntensity != g_clubMusicData.eNextClubMusicIntensity)
		DancingData.eNextMusicIntensity = g_clubMusicData.eNextClubMusicIntensity
	ENDIF
	
ENDPROC

FUNC BOOL DOES_DANCING_TRANSITION_EXIST_BETWEEN_INTENSITIES(PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL)
	
	// There is no transition between CLUB_MUSIC_INTENSITY_HIGH and CLUB_MUSIC_INTENSITY_HIGH_HANDS for any activity
	IF (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH AND eMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS)
	OR (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS AND eMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH)
		Data.dancingData.ePedMusicIntensity = eMusicIntensity
		RETURN FALSE
	ENDIF
	
	// Ambient dancing
	IF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
		// Low <-> Medium
		IF (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_LOW AND eMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
		OR (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM AND eMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
		// Low <-> Trance
		OR (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_LOW AND eMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
		OR (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE AND eMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
		// Medium <-> Trance
		OR (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM AND eMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
		OR (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE AND eMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
			Data.dancingData.ePedMusicIntensity = eMusicIntensity
			RETURN FALSE
		ENDIF
	
	// Face DJ, Single Prop & Group dancing
	ELIF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
	OR IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
	OR IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
		// Low <-> Trance
		IF (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_LOW AND eMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
		OR (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE AND eMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
			Data.dancingData.ePedMusicIntensity = eMusicIntensity
			RETURN FALSE
		ENDIF
	
	// Beach Prop dancing
	ELIF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH_PROP)
		// Medium <-> High
		IF (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM AND eMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH)
		OR (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH AND eMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
		// Medium <-> High Hands
		OR (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM AND eMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS)
		OR (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS AND eMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
			Data.dancingData.ePedMusicIntensity = eMusicIntensity
			RETURN FALSE
		ENDIF
	
	// DJ Dancers
	ELIF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_DJ_DANCER)
		// Low <-> Trance
		IF (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_LOW AND eMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
		OR (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE AND eMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
		// Medium <-> High
		OR (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM AND eMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH)
		OR (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH AND eMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
		// Medium <-> High Hands
		OR (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM AND eMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS)
		OR (Data.dancingData.ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS AND eMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
			Data.dancingData.ePedMusicIntensity = eMusicIntensity
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC UPDATE_PED_DANCING_TRANSITION(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL)
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_NO_ANIMATION)
		EXIT
	ENDIF
	
	PED_ANIM_DATA pedAnimData
	GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, DEFAULT, eMusicIntensity, Data.dancingData.ePedMusicIntensity)
	
	IF NOT IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
	OR (Data.dancingData.ePedMusicIntensity = eMusicIntensity AND Data.dancingData.eTransitionState = PED_DANCING_TRANS_STATE_SET_TRANSITION)
		EXIT
	ENDIF
	
	SWITCH Data.dancingData.eTransitionState
		CASE PED_DANCING_TRANS_STATE_SET_TRANSITION
			
			// Are we already on the music intensity it is switching to e.g. high intensity -> high intensity
			IF (Data.dancingData.ePedMusicIntensity = eMusicIntensity)
				EXIT
			ENDIF
			
			// Some dancing activities do not have transitions between intensities e.g. there is no transition between high <-> high hands
			IF NOT DOES_DANCING_TRANSITION_EXIST_BETWEEN_INTENSITIES(Data, pedAnimData, eMusicIntensity)
				EXIT
			ENDIF
			
			// Set ped transition bit so next anim is a transition anim.
			// This bit is cleared when the transition anim is task played.
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_DANCING_TRANSITION)
			
			// Cache the music intensity the ped is transitioning to. 
			// Possibility the intensity changes during the transition and will need to transition again.
			Data.dancingData.eCachedPedMusicIntensity = eMusicIntensity
			SET_PED_DANCING_TRANSITION_STATE(Data.dancingData.eTransitionState, PED_DANCING_TRANS_STATE_FINALISE_TRANSITION)
				
		BREAK
		CASE PED_DANCING_TRANS_STATE_FINALISE_TRANSITION
			
			// The ped transition bit has been cleared so the transition anim has been task played.
			IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_DANCING_TRANSITION)
				
				Data.animData.iPrevClip = 0
				Data.dancingData.iDancingTransitionClipID = -1
				
				// The intensity has changed during the transition.
				// The ped needs to transition again into the new intensity.
				IF (Data.dancingData.eCachedPedMusicIntensity != eMusicIntensity)
					SET_PED_DANCING_TRANSITION_STATE(Data.dancingData.eTransitionState, PED_DANCING_TRANS_STATE_SET_TRANSITION)
				ELSE
					// The dancing transition has finished.
					Data.dancingData.ePedMusicIntensity = eMusicIntensity
					SET_PED_DANCING_TRANSITION_STATE(Data.dancingData.eTransitionState, PED_DANCING_TRANS_STATE_SET_TRANSITION)
				ENDIF
			ELSE
				// There is a chance the intensity can change back while waiting for the current animation to finish playing.
				// This ensures the data is reset if the transition intensity changes back to the peds current intensity.
				IF (Data.dancingData.ePedMusicIntensity = eMusicIntensity)
				OR NOT DOES_DANCING_TRANSITION_EXIST_BETWEEN_INTENSITIES(Data, pedAnimData, eMusicIntensity)
					Data.dancingData.eCachedPedMusicIntensity = eMusicIntensity
					CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_DANCING_TRANSITION)
					SET_PED_DANCING_TRANSITION_STATE(Data.dancingData.eTransitionState, PED_DANCING_TRANS_STATE_SET_TRANSITION)
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
	
ENDPROC
#ENDIF	// FEATURE_HEIST_ISLAND
