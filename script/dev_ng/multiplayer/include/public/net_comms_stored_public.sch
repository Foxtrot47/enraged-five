USING "rage_builtins.sch"
USING "globals.sch"

USING "net_comms_stored.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   net_comms_stored_public.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   A public interface for all the different types of stored communications that can be triggered by MP.
//		NOTES			:	A stored communication is a 'fire and forget' communication. The stored communication maintenance routine
//								deals with constantly requesting the message to be played by the communication system.
//							This is intended to be used ONLY when a script can't do all this itself - in general scripts should
//								communicate with the the communication system directly to play cellphone or radio communications.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************





// ===========================================================================================================
//
//									Stored Communication Public Access Functions
//
// ===========================================================================================================

// PURPOSE:	Allows a MP script to pass in the details of a General Stored Communication.
//
// INPUT PARAMS:	paramCharacter		The Character ID string (from the 'Character' dropdown box in dialogueStar)
//					paramSpeakerID		The Speaker ID string (from the 'Speaker' value in dialogueStar)
//					paramCharSheetID	The CharSheetID for the character starting the call
//					paramGroupID		The Subtitle Group ID string for the Conversation (from the 'Subtitle Group ID' value in dialogueStar)
//					paramRootID			The Root ID string for the Conversation (from the 'Root' value in dialogueStar)
//					postHelp			The help text to display after the conversation has finished
// NOTES:	The details will be stored and the Comms system will trigger this speech using the appropriate method (cellphone or radio).
PROC Store_MP_General_Communication_With_Help(STRING paramCharacter, STRING paramSpeakerID, enumCharacterList paramCharSheetID, STRING paramGroupID, STRING paramRootID,STRING postHelp)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MPComms]: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_PRINT(" requested a new Stored Communication") NET_NL()
	#ENDIF

	g_eMPStoredCommsTypes	theStoredCommsType	= SCT_GENERAL_COMMS
	IF (Add_New_MP_Stored_Communication(theStoredCommsType, paramCharacter, paramSpeakerID, paramCharSheetID, paramGroupID, paramRootID,postHelp,0))
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MPComms]: Store General Communication - DENIED.") NET_NL()
	#ENDIF
	
ENDPROC


// PURPOSE:	Allows a MP script to pass in the details of a General Stored Communication.
//
// INPUT PARAMS:	paramCharacter		The Character ID string (from the 'Character' dropdown box in dialogueStar)
//					paramSpeakerID		The Speaker ID string (from the 'Speaker' value in dialogueStar)
//					paramCharSheetID	The CharSheetID for the character starting the call
//					paramGroupID		The Subtitle Group ID string for the Conversation (from the 'Subtitle Group ID' value in dialogueStar)
//					paramRootID			The Root ID string for the Conversation (from the 'Root' value in dialogueStar)
// NOTES:	The details will be stored and the Comms system will trigger this speech using the appropriate method (cellphone or radio).
PROC Store_MP_General_Communication(STRING paramCharacter, STRING paramSpeakerID, enumCharacterList paramCharSheetID, STRING paramGroupID, STRING paramRootID)
	
	Store_MP_General_Communication_With_Help(paramCharacter, paramSpeakerID, paramCharSheetID, paramGroupID, paramRootID,"")
	
ENDPROC







