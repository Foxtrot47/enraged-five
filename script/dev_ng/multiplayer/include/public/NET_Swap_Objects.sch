USING "globals.sch"
USING "net_include.sch"

 
//returns the model of map object that are to be swapped
FUNC MODEL_NAMES MP_SwapObject_ORIGINAL_MODEL_NAME(enumMP_SWAPPING_MAP_OBJECTS ObjectPassed)
	SWITCH ObjectPassed
		CASE MP_swapObjects_HOLD_UP_TILL_1			RETURN prop_till_01		//v_ret_gc_cashreg
		CASE MP_swapObjects_HOLD_UP_TILL_2			RETURN prop_till_01
		CASE MP_swapObjects_HOLD_UP_TILL_3			RETURN prop_till_01
		CASE MP_swapObjects_HOLD_UP_TILL_4			RETURN prop_till_01
		CASE MP_swapObjects_HOLD_UP_TILL_5			RETURN prop_till_01
		CASE MP_swapObjects_HOLD_UP_TILL_6			RETURN prop_till_01
		CASE MP_swapObjects_HOLD_UP_TILL_7			RETURN prop_till_01
		CASE MP_swapObjects_HOLD_UP_TILL_8			RETURN prop_till_01
		CASE MP_swapObjects_HOLD_UP_TILL_9			RETURN prop_till_01
		CASE MP_swapObjects_HOLD_UP_TILL_10			RETURN prop_till_01
		CASE MP_swapObjects_HOLD_UP_TILL_11			RETURN prop_till_01
		CASE MP_swapObjects_HOLD_UP_TILL_12			RETURN prop_till_01
		CASE MP_swapObjects_HOLD_UP_TILL_13			RETURN prop_till_01
		CASE MP_swapObjects_HOLD_UP_TILL_14			RETURN prop_till_01
		CASE MP_swapObjects_HOLD_UP_TILL_15			RETURN prop_till_01
		CASE MP_swapObjects_HOLD_UP_TILL_16			RETURN prop_till_01		//PROP_TILL_02
		CASE MP_swapObjects_HOLD_UP_TILL_17			RETURN prop_till_01		//PROP_TILL_02
		CASE MP_swapObjects_HOLD_UP_TILL_18			RETURN prop_till_01		//PROP_TILL_02
		CASE MP_swapObjects_HOLD_UP_TILL_19			RETURN prop_till_01		//PROP_TILL_02
		CASE MP_swapObjects_HOLD_UP_TILL_20			RETURN prop_till_01		//PROP_TILL_02
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

//returns the NEW model of map object that are to be swapped
FUNC MODEL_NAMES MP_SwapObject_SWAP_MODEL_NAME(enumMP_SWAPPING_MAP_OBJECTS ObjectPassed)
	SWITCH ObjectPassed		
		//Hold Ups
		CASE MP_swapObjects_HOLD_UP_TILL_1				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_2				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_3				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_4				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_5				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_6				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_7				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_8				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_9				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_10				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_11				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_12				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_13				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_14				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_15				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_16				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_17				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_18				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_19				RETURN P_TILL_01_S
		CASE MP_swapObjects_HOLD_UP_TILL_20				RETURN P_TILL_01_S
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

//returns the coordinates of map object that are to be swapped
FUNC VECTOR MP_SwapObject_VECTOR(enumMP_SWAPPING_MAP_OBJECTS ObjectPassed)
	SWITCH ObjectPassed		
		//Hold Ups
		CASE MP_swapObjects_HOLD_UP_TILL_1			RETURN <<1393.10, 3605.89, 35.20-0.2>>
		CASE MP_swapObjects_HOLD_UP_TILL_2			RETURN <<-3041.36, 584.28, 8.03>>
		CASE MP_swapObjects_HOLD_UP_TILL_3			RETURN <<-3244.56, 1000.74, 13.03-0.2>>
		CASE MP_swapObjects_HOLD_UP_TILL_4			RETURN <<548.82, 2668.93, 42.36-0.2>>
		CASE MP_swapObjects_HOLD_UP_TILL_5			RETURN <<2554.88, 381.47, 108.82-0.2>>
		CASE MP_swapObjects_HOLD_UP_TILL_6			RETURN <<2676.26, 3281.04, 55.44-0.2>>
		CASE MP_swapObjects_HOLD_UP_TILL_7			RETURN <<1729.40, 6417.08, 35.24-0.2>>
		CASE MP_swapObjects_HOLD_UP_TILL_8			RETURN <<1959.40, 3742.33, 32.54-0.2>>
		CASE MP_swapObjects_HOLD_UP_TILL_9			RETURN <<25.03, -1344.96, 29.69-0.2>>
		CASE MP_swapObjects_HOLD_UP_TILL_10			RETURN <<373.68, 328.56, 103.77-0.2>>
		CASE MP_swapObjects_HOLD_UP_TILL_11			RETURN <<1165.96, 2710.20, 38.25-0.2>>
		CASE MP_swapObjects_HOLD_UP_TILL_12			RETURN <<-2967.03, 390.90, 15.23-0.2>>
		CASE MP_swapObjects_HOLD_UP_TILL_13			RETURN <<-1222.33, -907.82, 12.52-0.2>>
		CASE MP_swapObjects_HOLD_UP_TILL_14			RETURN <<1134.81, -982.36, 46.60-0.2>>
		CASE MP_swapObjects_HOLD_UP_TILL_15			RETURN <<-1486.67, -378.46, 40.35-0.2>>
		CASE MP_swapObjects_HOLD_UP_TILL_16			RETURN <<1698.33, 4923.33, 42.12-0.06>>
		CASE MP_swapObjects_HOLD_UP_TILL_17			RETURN <<-706.69, -913.69, 19.27-0.06>>
		CASE MP_swapObjects_HOLD_UP_TILL_18			RETURN <<-47.23, -1757.64, 29.48-0.06>>
		CASE MP_swapObjects_HOLD_UP_TILL_19			RETURN <<1164.16, -322.90, 69.26-0.06>>
		CASE MP_swapObjects_HOLD_UP_TILL_20			RETURN <<-1820.50, 793.78, 138.32-0.16>>
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC
//returns the coordinates of map object that are to be swapped
FUNC BOOL MP_SwapObject_IS_ITEM_OBJECT()
//	SWITCH ObjectPassed		
//		//Hold Ups
//		CASE MP_swapObjects_HOLD_UP_TILL_1			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_2			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_3			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_4			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_5			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_6			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_7			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_8			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_9			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_10			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_11			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_12			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_13			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_14			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_15			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_16			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_17			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_18			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_19			RETURN TRUE
//		CASE MP_swapObjects_HOLD_UP_TILL_20			RETURN TRUE
//	ENDSWITCH
	RETURN TRUE
ENDFUNC

//PURPOSE: Returns whether the object should actually be hidden instead of swapped
FUNC BOOL MP_SwapObject_JUST_HIDE_THE_OBJECT(enumMP_SWAPPING_MAP_OBJECTS ObjectPassed)
	SWITCH ObjectPassed
		//Hold Ups
		CASE MP_swapObjects_HOLD_UP_TILL_1				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_2				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_3				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_4				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_5				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_6				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_7				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_8				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_9				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_10				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_11				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_12				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_13				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_14				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_15				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_16				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_17				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_18				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_19				RETURN TRUE
		CASE MP_swapObjects_HOLD_UP_TILL_20				RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//Returns true is the map object is to be swapped and has been swapped. 
FUNC BOOL HAS_MAP_OBJECT_BEEN_SWAPPED(enumMP_SWAPPING_MAP_OBJECTS mapObjectPassed)
	//Calculate the bit
	INT iBit = ENUM_TO_INT(mapObjectPassed)
	INT iArray = iBit/32
	iBit = iBit % 32 
	#IF IS_DEBUG_BUILD
		IF iArray >= MAX_NUMBER_OF_MP_SWAP_OBJECT_BITSETS 
			SCRIPT_ASSERT("Increase the size of MAX_NUMBER_OF_MP_SWAP_OBJECT_BITSETS -  Tell Bobby")
			PRINTSTRING("Increase the size of MAX_NUMBER_OF_MP_SWAP_OBJECT_BITSETS")PRINTNL()
		ENDIF
	#ENDIF
	IF IS_BIT_SET(g_iMP_SwapObjects_SwitchDone[iArray], iBit)
		RETURN IS_BIT_SET(g_iMP_SwapObjects_SwitchDone[iArray], iBit)
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL HAS_SERVER_MAP_OBJECT_SWAPPED(enumMP_SWAPPING_MAP_OBJECTS mapObjectPassed)
	//Calculate the bit
	INT iBit = ENUM_TO_INT(mapObjectPassed)
	INT iArray = iBit/32
	iBit = iBit % 32 
	#IF IS_DEBUG_BUILD
		IF iArray >= MAX_NUMBER_OF_MP_SWAP_OBJECT_BITSETS 
			SCRIPT_ASSERT("Increase the size of MAX_NUMBER_OF_MP_SWAP_OBJECT_BITSETS -  Tell Bobby")
			PRINTSTRING("Increase the size of MAX_NUMBER_OF_MP_SWAP_OBJECT_BITSETS")PRINTNL()
		ENDIF
	#ENDIF
	RETURN IS_BIT_SET(GlobalServerBD_FM.iIPLbitSet[iArray], iBit)
ENDFUNC



FUNC STRING MP_SwapObject_STRING_NAME()
	RETURN ""
ENDFUNC

//this turns back on a hidden map object.
PROC private_ReSet_objectState(enumMP_SWAPPING_MAP_OBJECTS ObjectPassed, BOOL bIsObject)
	IF bIsObject
		VECTOR 		vPos 			= MP_SwapObject_VECTOR(ObjectPassed)
		MODEL_NAMES mnOriginalModel	= MP_SwapObject_ORIGINAL_MODEL_NAME(ObjectPassed)
		MODEL_NAMES mnNewModel		= MP_SwapObject_SWAP_MODEL_NAME(ObjectPassed)
		REMOVE_MODEL_HIDE(vPos, 2.0, mnOriginalModel)
		CREATE_MODEL_HIDE(vPos, 2.0, mnNewModel, TRUE)
	ELSE
		REMOVE_IPL(MP_SwapObject_STRING_NAME())
	ENDIF
ENDPROC

CONST_INT MAX_NUMBER_OF_BITS					32

PROC SERVER_SET_iIslandRandIPL()
	IF GlobalServerBD.iIslandRandIPL = 0
		INT iRand = GET_RANDOM_INT_IN_RANGE(0,1000)
		IF iRand < 500
			GlobalServerBD.iIslandRandIPL = 1
		ELSE
			GlobalServerBD.iIslandRandIPL = 2
		ENDIF
		PRINTLN("MAIN32_MAINTAIN_IPLS: assigned GlobalServerBroadcastData.iIslandRandIPL = ",GlobalServerBD.iIslandRandIPL)
	ENDIF
ENDPROC

PROC MAIN32_MAINTAIN_IPLS(INT iParticipant, INT &iServerIPLbitSet[])

	
	INT iBitSet, iBit		
	//Repeat 32
	FOR iBitSet = 0 TO (COUNT_OF(iServerIPLbitSet) - 1)
		FOR iBit = 0 TO 31	//(ENUM_TO_INT(MP_SwapObject_MAX_NUMBER_OF_SWAP_OBJECTS) - 1)
			IF IS_BIT_SET(GlobalServerBD_FM.iIPLbitSet[iBitSet], iBit)
				//Check to see if the player is away from the map object
				IF NOT IS_BIT_SET(GlobalplayerBD_FM[iParticipant].iIPLbitSet[iBitSet], iBit)	
					//Some one is still at the map object
					SET_BIT(iServerIPLbitSet[iBitSet], iBit)
				ENDIF
				
				IF iParticipant = (NUM_NETWORK_PLAYERS -1)
					//-- Been through every particpant
					IF NOT IS_BIT_SET(iServerIPLbitSet[iBitSet], iBit)
						//Check to see if the player is away from the map object
						CLEAR_BIT(GlobalServerBD_FM.iIPLbitSet[iBitSet], iBit)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
	
	IF iParticipant = (NUM_NETWORK_PLAYERS -1)
		//reset the ints, ready for next loop through participants.
		FOR iBitSet = 0 TO (COUNT_OF(iServerIPLbitSet) - 1)
			iServerIPLbitSet[iBitSet] = 0
		ENDFOR
	ENDIF
ENDPROC

PROC SERVER_MAINTAIN_IPLS(INT iParticipant, INT &iServerIPLbitSet[])
	INT iBitSet, iBit
	IF iParticipant = (NETWORK_GET_MAX_NUM_PARTICIPANTS() -1)
		FOR iBitSet = 0 TO (COUNT_OF(iServerIPLbitSet) - 1)
			FOR iBit = 0 TO 31	//(ENUM_TO_INT(MP_SwapObject_MAX_NUMBER_OF_SWAP_OBJECTS) - 1)
				IF IS_BIT_SET(GlobalServerBD_FM.iIPLbitSet[iBitSet], iBit)
					//Check to see if the player is away from the map object
					IF NOT IS_BIT_SET(iServerIPLbitSet[iBitSet], iBit)
						CLEAR_BIT(GlobalServerBD_FM.iIPLbitSet[iBitSet], iBit)
					ENDIF
				ENDIF
			ENDFOR
		ENDFOR
		
		//reset the ints.
		FOR iBitSet = 0 TO (COUNT_OF(iServerIPLbitSet) - 1)
			iServerIPLbitSet[iBitSet] = 0
		ENDFOR
	ENDIF
ENDPROC

//This Proc controlls the sawpping of map objects for CNC
PROC MP_CONTROL_SWAPPED_MAP_OBJETS(INT &iBitArrayPassed[])
	IF MPGlobalsAmbience.g_bMP_SwapObjects_RunPROC
		INT iBitSet, iBit
		BOOL bShouldProcRun
		CONST_FLOAT cfRADIUS 2.0
		CONST_INT ciMAX_BITS	32
		FOR iBitSet = 0 TO (MAX_NUMBER_OF_MP_SWAP_OBJECT_BITSETS - 1)
			FOR iBit = 0 TO (ciMAX_BITS -1) //(ENUM_TO_INT(MP_SwapObject_MAX_NUMBER_OF_SWAP_OBJECTS) - 1)
				//If the model Should be swapped
				IF IS_BIT_SET(g_iMP_SwapObjects_SwitchDone[iBitSet], iBit)
					
					//Set a bool to say that the proc should RUN
					bShouldProcRun = TRUE		
					
					//Set up the varaiables that are needed for swapping
					INT 							iCurrentEnum 		= (iBit + (iBitSet* ciMAX_BITS))
					enumMP_SWAPPING_MAP_OBJECTS 	ObjectToSwap 		= INT_TO_ENUM(enumMP_SWAPPING_MAP_OBJECTS, iCurrentEnum)
					BOOL 							bIsItemAnObject 	= MP_SwapObject_IS_ITEM_OBJECT()
					BOOL							bJustHideTheObject 	= MP_SwapObject_JUST_HIDE_THE_OBJECT(ObjectToSwap)
					VECTOR 							vPos 				= MP_SwapObject_VECTOR(ObjectToSwap)
					MODEL_NAMES						mnOriginalModel		= DUMMY_MODEL_FOR_SCRIPT
					MODEL_NAMES						mnNewModel			= DUMMY_MODEL_FOR_SCRIPT
					STRING							stIPLname			= ""
															
					//Find out if it is a map object or a section of the map
					IF bIsItemAnObject
						mnOriginalModel	= MP_SwapObject_ORIGINAL_MODEL_NAME(ObjectToSwap)
						mnNewModel		= MP_SwapObject_SWAP_MODEL_NAME(ObjectToSwap)
					ELSE 	
						stIPLname 		= MP_SwapObject_STRING_NAME()
					ENDIF
					
					PRINTSTRING("BWW... MP_CONTROL_SWAPPED_MAP_OBJETS - CALLED A - ")PRINTINT(iCurrentEnum)PRINTNL()
					
					//If it is a map object to be swapped out
					IF bIsItemAnObject
						//if the object has not been sawpped
						IF NOT IS_BIT_SET(iBitArrayPassed[iBitSet], iBit)
							//If it is a map object to be hidden
							IF bJustHideTheObject
								CREATE_MODEL_HIDE(vPos, cfRADIUS/2, mnOriginalModel, TRUE)
								PRINTSTRING("BWW... MP_CONTROL_SWAPPED_MAP_OBJETS - CREATE_MODEL_HIDE ")PRINTINT(iCurrentEnum)PRINTNL()
							ELSE
								CREATE_MODEL_SWAP(vPos, cfRADIUS, mnOriginalModel, mnNewModel, TRUE)
								PRINTSTRING("BWW... MP_CONTROL_SWAPPED_MAP_OBJETS - CREATE_MODEL_SWAP ")PRINTINT(iCurrentEnum)PRINTNL()
							ENDIF
							SET_BIT(iBitArrayPassed[iBitSet], iBit)
						ENDIF	
						
					//if the map  has not been sawpped
					ELSE
						IF NOT IS_BIT_SET(iBitArrayPassed[iBitSet], iBit)
							PRINTSTRING("BWW... MP_CONTROL_SWAPPED_MAP_OBJETS - Requesting Map IPL  ")PRINTINT(iCurrentEnum)PRINTNL()
							REQUEST_IPL(stIPLname)
							//Set the bit so we don't come back in here. 
							SET_BIT(iBitArrayPassed[iBitSet], iBit)
						ENDIF
					ENDIF
				ELSE
					INT iMyParticipantID = NATIVE_TO_INT(PLAYER_ID())
					
					PRINTSTRING("BWW... MP_CONTROL_SWAPPED_MAP_OBJETS - CALLED B - ")PRINTNL()
					
					//if the object has been set up the unset it up.
					IF IS_BIT_SET(iBitArrayPassed[iBitSet], iBit)		
						INT 							iCurrentEnum 		= (iBit + (iBitSet* ciMAX_BITS))
						enumMP_SWAPPING_MAP_OBJECTS 	ObjectToSwap 	= INT_TO_ENUM(enumMP_SWAPPING_MAP_OBJECTS, iCurrentEnum)
						BOOL 							bIsItemAnObject = MP_SwapObject_IS_ITEM_OBJECT()
						BOOL							bJustHideTheObject 	= MP_SwapObject_JUST_HIDE_THE_OBJECT(ObjectToSwap)
						VECTOR 							vPos 			= MP_SwapObject_VECTOR(ObjectToSwap)
						IF bIsItemAnObject							
							//Set a bool to say that the proc should RUN
							bShouldProcRun = TRUE	
							IF IS_BIT_SET(GlobalServerBD_FM.iIPLbitSet[iBitSet], iBit)
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPos, <<200.0, 200.0, 200.0>>, FALSE)
									IF IS_BIT_SET(GlobalplayerBD_FM[iMyParticipantID].iIPLbitSet[iBitSet], iBit)		
										CLEAR_BIT(GlobalplayerBD_FM[iMyParticipantID].iIPLbitSet[iBitSet], iBit)	
										PRINTSTRING("BWW... MP_CONTROL_SWAPPED_MAP_OBJETS - Player at location clearing IPL bit - ")PRINTINT(iCurrentEnum)PRINTNL()
									ENDIF
								ELSE
									IF NOT IS_BIT_SET(GlobalplayerBD_FM[iMyParticipantID].iIPLbitSet[iBitSet], iBit)		
										SET_BIT(GlobalplayerBD_FM[iMyParticipantID].iIPLbitSet[iBitSet], iBit)	
										PRINTSTRING("BWW... MP_CONTROL_SWAPPED_MAP_OBJETS - Player NOT at location Setting IPL bit - ")PRINTINT(iCurrentEnum)PRINTNL()
									ENDIF
								ENDIF						
							ENDIF
							IF NOT IS_BIT_SET(GlobalServerBD_FM.iIPLbitSet[iBitSet], iBit)
								CLEAR_BIT(GlobalplayerBD_FM[iMyParticipantID].iIPLbitSet[iBitSet], iBit)	
								CLEAR_BIT(iBitArrayPassed[iBitSet], iBit)									
								MODEL_NAMES mnOriginalModel	= MP_SwapObject_ORIGINAL_MODEL_NAME(ObjectToSwap)
								MODEL_NAMES mnNewModel		= MP_SwapObject_SWAP_MODEL_NAME(ObjectToSwap)
								IF bJustHideTheObject
									REMOVE_MODEL_HIDE(vPos, cfRADIUS/2, mnOriginalModel)
									PRINTSTRING("BWW... MP_CONTROL_SWAPPED_MAP_OBJETS - EveryBody away from Object - REMOVE_MODEL_HIDE ")PRINTINT(iCurrentEnum)PRINTNL()
								ELSE
									REMOVE_MODEL_SWAP(vPos, cfRADIUS, mnOriginalModel, mnNewModel)
									PRINTSTRING("BWW... MP_CONTROL_SWAPPED_MAP_OBJETS - EveryBody away from Object - REMOVE_MODEL_SWAP ")PRINTINT(iCurrentEnum)PRINTNL()
								ENDIF
							ENDIF
						ELSE						
							//Set a bool to say that the proc should RUN
							bShouldProcRun = TRUE										
							IF IS_BIT_SET(GlobalServerBD_FM.iIPLbitSet[iBitSet], iBit)
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPos, <<500.0, 500.0, 500.0>>, FALSE)
									IF IS_BIT_SET(GlobalplayerBD_FM[iMyParticipantID].iIPLbitSet[iBitSet], iBit)		
										CLEAR_BIT(GlobalplayerBD_FM[iMyParticipantID].iIPLbitSet[iBitSet], iBit)	
										PRINTSTRING("BWW... MP_CONTROL_SWAPPED_MAP_OBJETS - Player at location clearing IPL bit - ")PRINTINT(iCurrentEnum)PRINTNL()
									ENDIF
								ELSE
									IF NOT IS_BIT_SET(GlobalplayerBD_FM[iMyParticipantID].iIPLbitSet[iBitSet], iBit)		
										SET_BIT(GlobalplayerBD_FM[iMyParticipantID].iIPLbitSet[iBitSet], iBit)	
										PRINTSTRING("BWW... MP_CONTROL_SWAPPED_MAP_OBJETS - Player NOT at location Setting IPL bit - ")PRINTINT(iCurrentEnum)PRINTNL()
									ENDIF
								ENDIF
								//PRINTSTRING("BWW... MP_CONTROL_SWAPPED_MAP_OBJETS - IS_BIT_SET(GlobalServerBD_FM.iIPLbitSet[iBitSet], iBit)- ")PRINTINT(iCurrentEnum)PRINTNL()							
							ENDIF
							IF NOT IS_BIT_SET(GlobalServerBD_FM.iIPLbitSet[iBitSet], iBit)
								CLEAR_BIT(GlobalplayerBD_FM[iMyParticipantID].iIPLbitSet[iBitSet], iBit)	
								CLEAR_BIT(iBitArrayPassed[iBitSet], iBit)
								PRINTSTRING("BWW... MP_CONTROL_SWAPPED_MAP_OBJETS - EveryBody away from IPL - ")PRINTINT(iCurrentEnum)PRINTNL()
								private_ReSet_objectState(ObjectToSwap, FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDFOR
		
		//If the proc should stop running the quit out of the proc
		IF bShouldProcRun = FALSE
		//	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				INT 							iCurrentEnum 		= (iBit + (iBitSet* ciMAX_BITS))
				MPGlobalsAmbience.g_bMP_SwapObjects_RunPROC = FALSE
				PRINTSTRING("BWW... MP_CONTROL_SWAPPED_MAP_OBJETS - Setting MPGlobalsAmbience.g_bMP_SwapObjects_RunPROC to FALSE - ")PRINTINT(iCurrentEnum)PRINTNL()
		//	ENDIF
		ENDIF
	ELSE
		INT i 
		FOR i = 0 TO (MAX_NUMBER_OF_MP_SWAP_OBJECT_BITSETS - 1)
			IF g_iMP_SwapObjects_SwitchDone[i] != GlobalServerBD_FM.iIPLbitSet[i]
				g_iMP_SwapObjects_SwitchDone[i] = GlobalServerBD_FM.iIPLbitSet[i]
				MPGlobalsAmbience.g_bMP_SwapObjects_RunPROC = TRUE
			ENDIF			
		ENDFOR
	ENDIF
ENDPROC


