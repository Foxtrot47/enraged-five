//////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_BRU_BOX.sch															//
// Description: Player calls for a special box from Brucie.								//
// Written by:  Ryan Baker																//
// Date: 23/01/2013																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
//USING "script_player.sch"
//USING "commands_path.sch"
//USING "building_control_public.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_mission.sch"



//PURPOSE: Controls the launching of Bru Box
FUNC BOOL MAINTAIN_BRU_BOX_CLIENT()
		
	//LAUNCH IF PLAYER HAS REQUESTED BRU BOX FROM PHONE
	IF MPGlobalsAmbience.bLaunchBruBox
		//IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_BRU_BOX")) <= 0
		//IF NOT IS_PLAYER_RUNNING_MP_MISSION_INSTANCE(PLAYER_ID(), eAM_BRU_BOX, NATIVE_TO_INT(PLAYER_ID()))
		IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_BRU_BOX", NATIVE_TO_INT(PLAYER_ID()), TRUE)
			//IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
				MP_MISSION_DATA amBruBoxLaunch
				
				amBruBoxLaunch.mdID.idMission = eAM_BRU_BOX
				amBruBoxLaunch.iInstanceId = NATIVE_TO_INT(PLAYER_ID())
				
				IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(amBruBoxLaunch)
					#IF IS_DEBUG_BUILD NET_NL() NET_PRINT(" -- MAINTAIN_BRU_BOX_CLIENT - I STARTED - LAUNCHED SCRIPT AM_BRU_BOX.sc - INSTANCE = ") NET_PRINT_INT(amBruBoxLaunch.iInstanceId) NET_NL() #ENDIF
					MPGlobalsAmbience.bLaunchBruBox = FALSE
				ENDIF
			//ELSE
			//	MPGlobalsAmbience.bLaunchBruBox = FALSE
			//	#IF IS_DEBUG_BUILD NET_NL() NET_PRINT(" -- MAINTAIN_BRU_BOX_CLIENT - PLAYER IN TUTORIAL SESSION") NET_NL() #ENDIF
			//ENDIF
			RETURN TRUE
			
		ELSE
			MPGlobalsAmbience.bLaunchBruBox = FALSE
			#IF IS_DEBUG_BUILD NET_NL() NET_PRINT(" -- MAINTAIN_BRU_BOX_CLIENT - SCRIPT ALREADY RUNNING AM_BRU_BOX.sc") NET_NL() #ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC	



//PURPOSE: Launches the Brucie Box script and creates the box using the passed in  Vector and Heading
PROC SET_DEATHMATCH_BRU_BOX(VECTOR vPos, FLOAT fHeading, INT iArrayPos = 0)
	MPGlobalsAmbience.BruBoxDataDM.bLaunchBruBox = TRUE
	MPGlobalsAmbience.BruBoxDataDM.vBruBoxPosition = vPos
	MPGlobalsAmbience.BruBoxDataDM.fBruBoxHeading = fHeading
	MPGlobalsAmbience.BruBoxDataDM.iArrayPos = iArrayPos
	#IF IS_DEBUG_BUILD NET_NL() NET_PRINT(" -- MAINTAIN_BRU_BOX_DEATHMATCH - DM - SET_DEATHMATCH_BRU_BOX") NET_NL() #ENDIF
ENDPROC

//PURPOSE: Clears the Deathmatch Bru Box Data
PROC CLEAR_DM_BRU_BOX_DATA()
	MPGlobalsAmbience.BruBoxDataDM.bLaunchBruBox = FALSE
	MPGlobalsAmbience.BruBoxDataDM.vBruBoxPosition = <<0, 0, 0>>
	MPGlobalsAmbience.BruBoxDataDM.fBruBoxHeading = 0
	MPGlobalsAmbience.BruBoxDataDM.iArrayPos = 0
	CLEAR_BIT(MPGlobalsAmbience.BruBoxDataDM.iBitSet, ciGB_BRU_BOX_ON)
	 //#IF FEATURE_GANG_BOSS
	
	#IF IS_DEBUG_BUILD NET_NL() NET_PRINT(" -- MAINTAIN_BRU_BOX_DEATHMATCH - DM - CLEAR_DM_BRU_BOX_DATA") NET_NL() #ENDIF
ENDPROC

//PURPOSE: Sets the Deathmatch Brucie Box to cleanup
PROC CLEANUP_DM_BRU_BOX()
	MPGlobalsAmbience.BruBoxDataDM.vBruBoxPosition = <<0, 0, 0>>
	#IF IS_DEBUG_BUILD NET_NL() NET_PRINT(" -- MAINTAIN_BRU_BOX_DEATHMATCH - DM - CLEANUP_DM_BRU_BOX") NET_NL() #ENDIF
ENDPROC

//PURPOSE: Checks to see if the Brucie Box should Launch
PROC MAINTAIN_BRU_BOX_DEATHMATCH(INT iInstance)

	INT iInstanceId
	MP_MISSION_DATA amBruBoxLaunch
	
	//LAUNCH IF DEATHMATCH HAS REQUESTED ONE
	IF MPGlobalsAmbience.BruBoxDataDM.bLaunchBruBox
		iInstanceId = (iInstance + NUM_NETWORK_PLAYERS)
		//IF NOT IS_PLAYER_RUNNING_MP_MISSION_INSTANCE(PLAYER_ID(), eAM_BRU_BOX, (iInstance + NUM_NETWORK_PLAYERS))
		IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_BRU_BOX", iInstanceId, TRUE)
			amBruBoxLaunch.mdID.idMission = eAM_BRU_BOX
			amBruBoxLaunch.iInstanceId = iInstanceId
			amBruBoxLaunch.mdGenericInt = MPGlobalsAmbience.BruBoxDataDM.iArrayPos
			RESET_NET_TIMER(MPGlobalsAmbience.BruBoxDataDM.iBruBoxLaunchCheckDelay)
			
			PRINTLN("2588666, MAINTAIN_BRU_BOX_DEATHMATCH, iInstanceId = ", iInstanceId)
			PRINTLN("2588666, MAINTAIN_BRU_BOX_DEATHMATCH, iArrayPos   = ", amBruBoxLaunch.mdGenericInt)
			
			IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(amBruBoxLaunch)
				#IF IS_DEBUG_BUILD NET_NL() NET_PRINT(" -- MAINTAIN_BRU_BOX_DEATHMATCH - DM - I STARTED - LAUNCHED SCRIPT AM_BRU_BOX.sc - INSTANCE = ") NET_PRINT_INT(amBruBoxLaunch.iInstanceId) NET_NL() #ENDIF
				MPGlobalsAmbience.BruBoxDataDM.bLaunchBruBox = FALSE
			ENDIF
		ELSE
			PRINTLN("2588666, MAINTAIN_BRU_BOX_DEATHMATCH, ERROR, iInstanceId  = ", iInstanceId)
			MPGlobalsAmbience.BruBoxDataDM.bLaunchBruBox = FALSE
			#IF IS_DEBUG_BUILD NET_NL() NET_PRINT(" -- MAINTAIN_BRU_BOX_DEATHMATCH - DM - SCRIPT ALREADY RUNNING AM_BRU_BOX.sc") NET_NL() #ENDIF
		ENDIF
	ENDIF

	//LAUNCH IF SOMEBODY ELSE IS RUNNING THE SCRIPT IN DEATHMATCH
	IF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.BruBoxDataDM.iBruBoxLaunchCheckDelay, 2000)
		//IF NOT IS_PLAYER_RUNNING_MP_MISSION_INSTANCE(PLAYER_ID(), eAM_BRU_BOX, (iInstance + NUM_NETWORK_PLAYERS))
		IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_BRU_BOX", iInstanceId, TRUE)
			IF NETWORK_IS_SCRIPT_ACTIVE("AM_BRU_BOX", iInstanceId)
				amBruBoxLaunch.mdID.idMission = eAM_BRU_BOX
				amBruBoxLaunch.iInstanceId = iInstanceId
				
				IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(amBruBoxLaunch)
					#IF IS_DEBUG_BUILD NET_NL() NET_PRINT(" -- MAINTAIN_BRU_BOX_DEATHMATCH - DM - SOMEONE ELSE STARTED - LAUNCHED SCRIPT AM_BRU_BOX.sc - INSTANCE = ") NET_PRINT_INT(amBruBoxLaunch.iInstanceId) NET_NL() #ENDIF
				ENDIF
			ENDIF
		ENDIF
		RESET_NET_TIMER(MPGlobalsAmbience.BruBoxDataDM.iBruBoxLaunchCheckDelay)
	ENDIF
ENDPROC
