// name:        network_ticker_logic.sch
// description: Logic and functionality for the ticker system. 
//              Ticker logic is completely separate from  ticker drawing. 
//              Key ticker data held in globals so we can easily add 
//              tickers from multiple scripts.

USING "globals.sch"

USING "commands_network.sch"
USING "net_include.sch"
USING "FMMC_SCTV_TICKER.sch"
USING "net_mp_intro.sch"

FUNC FLOAT CONVERT_METRES_TO_MILES(FLOAT fMetres)
    RETURN(fMetres * 0.000621371192)
ENDFUNC

FUNC BOOL IS_TICKER_STRING_EMPTY(STRING inStr)
    IF IS_STRING_NULL(inStr)
        PRINTSTRING("IS_TICKER_STRING_EMPTY - string is null")
        PRINTNL()
        RETURN(TRUE)
    ELSE
        IF ARE_STRINGS_EQUAL(inStr, "")
            PRINTSTRING("IS_TICKER_STRING_EMPTY - string is equal to nothing ")
            PRINTSTRING(inStr)
            PRINTNL()
            RETURN(TRUE)
        ENDIF
    ENDIF
    RETURN(FALSE)
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC INT PRINT_TEST_TICKER(STRING TestString)
    
    INT iReturn = -1

    BEGIN_TEXT_COMMAND_THEFEED_POST(TestString)
        
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    RETURN(iReturn)
    
ENDFUNC

FUNC INT PRINT_DEBUG_TICKER(STRING strLiteral)
    INT iReturn = -1
    
    BEGIN_TEXT_COMMAND_THEFEED_POST("STRING")
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strLiteral)    
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    RETURN(iReturn)
    
ENDFUNC

#ENDIF

FUNC STRING TLToString(STRING strIn)
    RETURN(strIn)
ENDFUNC

FUNC STRING APPEND_TAGS_TO_PLAYER_NAME(STRING PlayerName)

    TEXT_LABEL_63 str63
      
    str63 = "<C>"
    str63 += PlayerName
    str63 += "</C>~s~" // added the ~s~ to make sure the default colour returns after the player name. 

    //str63 = PlayerName

    //NET_PRINT("APPEND_TAGS_TO_PLAYER_NAME - returning ") NET_PRINT(TLToString(str63)) NET_NL()

    RETURN(TLToString(str63))
    
ENDFUNC

FUNC INT PRINT_WEAPON_TICKER(STRING TextLabel)

    INT iReturn = -1

    BEGIN_TEXT_COMMAND_THEFEED_POST("FM_PICKED")        
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(TextLabel)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
	
    NET_PRINT("PRINT_TICKER - called with text label ") NET_PRINT(TextLabel) NET_NL()
	
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WEAPON_TICKER, "FM_PICKED", ciSCTV_TICKER_PRIORITY_LOW, TextLabel)
    
    RETURN(iReturn)
    
ENDFUNC

/// PURPOSE:
///    Prints a ticker with the 'Clothing Unlock' title and clothing icon
/// PARAMS:
///    TextLabel - The label for the main body of the ticker
/// RETURNS:
///    Unique id of the ticker which can be used to manipulate it whilst onscreen
FUNC INT PRINT_CLOTHING_UNLOCK_TICKER(STRING TextLabel)
	INT iReturn = -1

   	BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
	iReturn = END_TEXT_COMMAND_THEFEED_POST_UNLOCK_TU("CLOTHES_UNLOCK", 7, TextLabel, TRUE)
     
    PRINTLN("PRINT_CLOTHING_UNLOCK_TICKER - called with text label ", TextLabel, " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(TextLabel), ")")
    
    RETURN(iReturn)
ENDFUNC

/// PURPOSE:
///    Prints a ticker with the 'Weapon Unlock' title and weapon icon
/// PARAMS:
///    TextLabel - The label for the main body of the ticker
/// RETURNS:
///    Unique id of the ticker which can be used to manipulate it whilst onscreen
FUNC INT PRINT_WEAPON_UNLOCK_TICKER(STRING TextLabel)
	INT iReturn = -1
	
	BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
	iReturn = END_TEXT_COMMAND_THEFEED_POST_UNLOCK_TU("WEAPON_UNLOCK", 2, TextLabel, TRUE)
    
	PRINTLN("PRINT_WEAPON_UNLOCK_TICKER - called with text label ", TextLabel, " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(TextLabel), ")")

	RETURN(iReturn)
ENDFUNC

/// PURPOSE:
///    Prints a ticker with the 'New stock notification' title and icon from the given vehicle website
/// PARAMS:
///    vehModel - The model for the vehicle to be inserted into the body of the ticker
/// RETURNS:
///    Unique id of the ticker which can be used to manipulate it whilst onscreen
FUNC INT PRINT_VEHICLE_AVAILABLE_TICKER(MODEL_NAMES vehModel, VEHICLE_WEBSITES VehSite)
	INT iReturn = -1
	STRING Txd = ""
	
	SWITCH VehSite
		CASE VEHICLEWEBSITE_LEGENDARY_MOTORSPORT								Txd = "CHAR_CARSITE"		BREAK
		CASE VEHICLEWEBSITE_SOUTHERN_SAN_ANDREAS_SUPER_AUTOS					Txd = "CHAR_CARSITE2"		BREAK
		CASE VEHICLEWEBSITE_BENNYS_ORIGINAL_MOTORWORKS							Txd = "CHAR_CARSITE3"		BREAK
		CASE VEHICLEWEBSITE_ARENA_WAR											Txd = "CHAR_CARSITE4"		BREAK
		CASE VEHICLEWEBSITE_ELITAS_TRAVEL										Txd = "CHAR_PLANESITE"		BREAK
		CASE VEHICLEWEBSITE_WARSTOCK_CACHE_AND_CARRY							Txd = "CHAR_MILSITE"		BREAK
		CASE VEHICLEWEBSITE_DOCKTEASE											Txd = "CHAR_BOATSIDE"		BREAK
		CASE VEHICLEWEBSITE_PEDAL_AND_METAL_CYCLES								Txd = "CHAR_BIKESITE"		BREAK
	ENDSWITCH
	
	BEGIN_TEXT_COMMAND_THEFEED_POST("NVTICK_BODY")
	ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_MAKE_NAME_FROM_VEHICLE_MODEL(vehModel))        
	ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(vehModel))        
	iReturn = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT(Txd, Txd, FALSE, TEXT_ICON_BLANK, "NVTICK_SUB")

	RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER(STRING TextLabel, BOOL bHighPriority = FALSE)

    INT iReturn = -1
	
	INT ipriority = ciSCTV_TICKER_PRIORITY_MEDIUM
	
	IF bHighPriority
		ipriority = ciSCTV_TICKER_PRIORITY_HIGH
	ENDIF

    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)      
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
	
    NET_PRINT("PRINT_TICKER - called with text label ") NET_PRINT(TextLabel) NET_NL()
	PRINTLN("PRINT_TICKER -priority =  ", ipriority)
	DEBUG_PRINTCALLSTACK()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_TICKER, TextLabel, ipriority)
    
    RETURN(iReturn)
    
ENDFUNC

FUNC INT PRINT_TICKER_WITH_INT(STRING TextLabel, INT iNumber=LOWEST_INT, BOOL bhighpriority = FALSE)
    INT iReturn = -1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_INTEGER(iNumber)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
	
	
    NET_PRINT("PRINT_TICKER_WITH_INT - called with text label ") NET_PRINT(TextLabel) NET_NL()
	IF bhighpriority
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_INT, TextLabel, ciSCTV_TICKER_PRIORITY_HIGH, "", iNumber)
		 NET_PRINT("PRINT_TICKER_WITH_INT - High Priority ") 
	ELSE
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_INT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, "", iNumber)
	ENDIF
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_TWO_INTS(STRING TextLabel, INT iNumber1=LOWEST_INT, INT iNumber2=LOWEST_INT)
    INT iReturn = -1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        ADD_TEXT_COMPONENT_INTEGER(iNumber2)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_TWO_INTS - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_INTS, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, "", iNumber1, iNumber2)
	
    RETURN(iReturn)
ENDFUNC


FUNC INT PRINT_TICKER_WITH_THREE_INTS(STRING TextLabel, INT iNumber1=LOWEST_INT, INT iNumber2=LOWEST_INT  ,INT iNumber3=LOWEST_INT)
    INT iReturn = -1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        ADD_TEXT_COMPONENT_INTEGER(iNumber2)
		ADD_TEXT_COMPONENT_INTEGER(iNumber3)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_THREE_INTS - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_THREE_INTS, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, "", iNumber1, iNumber2, iNumber3)
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_FOUR_INTS(STRING TextLabel, INT iNumber1=LOWEST_INT, INT iNumber2=LOWEST_INT  ,INT iNumber3=LOWEST_INT  ,INT iNumber4=LOWEST_INT)
    INT iReturn = -1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        ADD_TEXT_COMPONENT_INTEGER(iNumber2)
		ADD_TEXT_COMPONENT_INTEGER(iNumber3)
		ADD_TEXT_COMPONENT_INTEGER(iNumber4)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_FOUR_INTS - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_THREE_INTS, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, "", iNumber1, iNumber2, iNumber3)
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_FIVE_INTS(STRING TextLabel, INT iNumber1=LOWEST_INT, INT iNumber2=LOWEST_INT  ,INT iNumber3=LOWEST_INT  ,INT iNumber4=LOWEST_INT  ,INT iNumber5=LOWEST_INT)
    INT iReturn = -1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        ADD_TEXT_COMPONENT_INTEGER(iNumber2)
		ADD_TEXT_COMPONENT_INTEGER(iNumber3)
		ADD_TEXT_COMPONENT_INTEGER(iNumber4)
		ADD_TEXT_COMPONENT_INTEGER(iNumber5)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_FIVE_INTS - called with text label ") NET_PRINT(TextLabel) NET_NL()
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_SIX_INTS(STRING TextLabel, INT iNumber1=LOWEST_INT, INT iNumber2=LOWEST_INT  ,INT iNumber3=LOWEST_INT  ,INT iNumber4=LOWEST_INT  ,INT iNumber5=LOWEST_INT, INT iNumber6=LOWEST_INT)
    INT iReturn = -1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        ADD_TEXT_COMPONENT_INTEGER(iNumber2)
		ADD_TEXT_COMPONENT_INTEGER(iNumber3)
		ADD_TEXT_COMPONENT_INTEGER(iNumber4)
		ADD_TEXT_COMPONENT_INTEGER(iNumber5)
		ADD_TEXT_COMPONENT_INTEGER(iNumber6)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_FIVE_INTS - called with text label ") NET_PRINT(TextLabel) NET_NL()
	
    RETURN(iReturn)
ENDFUNC


FUNC INT PRINT_TICKER_WITH_FLOAT_AND_INT(STRING TextLabel, FLOAT fNumber1, INT iDecimalPoints , INT iNumber2=LOWEST_INT)
    INT iReturn = -1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_FLOAT(fNumber1, iDecimalPoints)
        ADD_TEXT_COMPONENT_INTEGER(iNumber2)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_FLOAT_AND_INT - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_FLOAT_AND_INT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, "", iNumber2, 0, 0, fNumber1, iDecimalPoints)

    RETURN(iReturn)
ENDFUNC

/// NOTE: This converts the float into 2 ints, so your text label must have 2 numbers with a decimal point in between like this ~1~.~1~
FUNC INT PRINT_TICKER_WITH_FLOAT(STRING TextLabel, FLOAT fNumber, INT iDecimalPoints)
    INT iReturn = -1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_FLOAT(fNumber, iDecimalPoints)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_FLOAT - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_FLOAT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, "", 0, 0, 0, fNumber, iDecimalPoints)
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_STRING(STRING TextLabel, STRING InsertedTextLabel, HUD_COLOURS Colour1 = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Colour2 = HUD_COLOUR_PURE_WHITE , BOOL bShowClanInfo = FALSE)
    INT iReturn = -1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        IF NOT (Colour1 = HUD_COLOUR_PURE_WHITE)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour1) // this wont actually do anything
        ENDIf
        IF NOT (Colour2 = HUD_COLOUR_PURE_WHITE)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour2)
        ENDIF
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel)
    IF NOT (bShowClanInfo)
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
    ELSE
    
        //NETWORK_CLAN_DESC ClanInfo
        //GAMER_HANDLE GamerHandle =  GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
		g_GamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID()) 
        IF NETWORK_CLAN_PLAYER_GET_DESC(g_ClanDescription, SIZE_OF(NETWORK_CLAN_DESC), g_GamerHandle)

            BOOL bIsLeader = FALSE
            IF ARE_STRINGS_EQUAL(g_ClanDescription.RankName, "Leader")
            AND g_ClanDescription.RankOrder = 0
                bIsLeader = TRUE
            ENDIF
            BOOL bIsPrivate
            IF (g_ClanDescription.IsOpenClan>0)
                bIsPrivate = FALSE
            ELSE
                bIsPrivate = TRUE
            ENDIF
            #IF IS_DEBUG_BUILD
                IF (bUseOldCrewTicker)
                    iReturn = END_TEXT_COMMAND_THEFEED_POST_CREWTAG(bIsPrivate, NETWORK_CLAN_IS_ROCKSTAR_CLAN(g_ClanDescription, SIZE_OF(g_ClanDescription)), g_ClanDescription.ClanTag, g_ClanDescription.RankOrder, bIsLeader, FALSE, g_ClanDescription.Id, g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)   
                ELSE
            #ENDIF
                iReturn = END_TEXT_COMMAND_THEFEED_POST_CREWTAG_WITH_GAME_NAME(bIsPrivate, NETWORK_CLAN_IS_ROCKSTAR_CLAN(g_ClanDescription, SIZE_OF(g_ClanDescription)), g_ClanDescription.ClanTag, g_ClanDescription.RankOrder, bIsLeader, FALSE, g_ClanDescription.Id, GET_PLAYER_NAME(PLAYER_ID()), g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)
            #IF IS_DEBUG_BUILD
                ENDIF
            #ENDIF              
        ELSE
            NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - could not get clan info ") NET_NL()
            iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
        ENDIF   
    ENDIF
	
    
    NET_PRINT("PRINT_TICKER_WITH_STRING - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_STRING, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, InsertedTextLabel)
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_STRING_AND_INT(STRING TextLabel, STRING InsertedTextLabel, INT iNumber1 = LOWEST_INT)
    INT iReturn = -1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel)
        ADD_TEXT_COMPONENT_INTEGER(iNumber1)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
    
    NET_PRINT("PRINT_TICKER_WITH_STRING_AND_INT - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_STRING_AND_INT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, InsertedTextLabel, iNumber1)
	
    RETURN(iReturn) 
ENDFUNC

FUNC INT PRINT_TICKER_WITH_STRING_AND_TWO_INTS(STRING TextLabel, STRING InsertedTextLabel, INT iNumber1 = LOWEST_INT, INT iNumber2 = LOWEST_INT)
    INT iReturn=-1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel)
        ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        ADD_TEXT_COMPONENT_INTEGER(iNumber2)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_STRING_AND_TWO_INTS - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_STRING_AND_TWO_INTS, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, InsertedTextLabel, iNumber1, iNumber2)
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_STRING_AND_FOUR_INTS(STRING TextLabel, STRING InsertedTextLabel, INT iNumber1 = LOWEST_INT, INT iNumber2 = LOWEST_INT, INT iNumber3 = LOWEST_INT, INT iNumber4 = LOWEST_INT)
    INT iReturn=-1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel)
        ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        ADD_TEXT_COMPONENT_INTEGER(iNumber2)
		ADD_TEXT_COMPONENT_INTEGER(iNumber3)
		ADD_TEXT_COMPONENT_INTEGER(iNumber4)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_STRING_AND_FOUR_INTS - called with text label ") NET_PRINT(TextLabel) NET_NL()
	
    RETURN(iReturn)
ENDFUNC


/// NOTE: This converts the float into 2 ints, so your text label must have 2 numbers with a decimal point in between like this ~1~.~1~
FUNC INT PRINT_TICKER_WITH_STRING_AND_FLOAT(STRING TextLabel, STRING InsertedTextLabel, FLOAT fNumber, INT iDecimalPoints)
    INT iReturn = -1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel)
        ADD_TEXT_COMPONENT_FLOAT(fNumber, iDecimalPoints)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)   

    NET_PRINT("PRINT_TICKER_WITH_STRING_AND_FLOAT - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_STRING_AND_FLOAT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, InsertedTextLabel, 0, 0, 0, fNumber, iDecimalPoints)
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_TWO_STRINGS(STRING TextLabel, STRING InsertedTextLabel1, STRING InsertedTextLabel2, HUD_COLOURS Colour1 = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Colour2 = HUD_COLOUR_PURE_WHITE)
    INT iReturn = -1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        IF NOT (Colour1 = HUD_COLOUR_PURE_WHITE)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour1)
        ENDIF
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel1)
        IF NOT (Colour2 = HUD_COLOUR_PURE_WHITE)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour2)
        ENDIF
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel2)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_TWO_STRINGS - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_STRINGS, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, InsertedTextLabel1, 0, 0, 0, 0.0, 0, InsertedTextLabel2)
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_THREE_STRINGS(STRING TextLabel, STRING InsertedTextLabel1, STRING InsertedTextLabel2, STRING InsertedTextLabel3, HUD_COLOURS Colour1 = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Colour2 = HUD_COLOUR_PURE_WHITE, HUD_COLOURS Colour3 = HUD_COLOUR_PURE_WHITE)
    INT iReturn = -1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        IF NOT (Colour1 = HUD_COLOUR_PURE_WHITE)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour1)
        ENDIF
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel1)
        IF NOT (Colour2 = HUD_COLOUR_PURE_WHITE)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour2)
        ENDIF
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel2)
		 IF NOT (Colour3 = HUD_COLOUR_PURE_WHITE)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour2)
        ENDIF
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel3)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_THREE_STRINGS - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_STRINGS, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, InsertedTextLabel1, 0, 0, 0, 0.0, 0, InsertedTextLabel2, InsertedTextLabel3)
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_TWO_STRINGS_AND_TWO_INTS(STRING TextLabel, STRING InsertedTextLabel1, STRING InsertedTextLabel2, INT iNumber1 = LOWEST_INT, INT iNumber2 = LOWEST_INT)
    INT iReturn=-1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel1)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel2)
        ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        ADD_TEXT_COMPONENT_INTEGER(iNumber2)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_TWO_STRINGS_AND_TWO_INTS - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_STRINGS_AND_TWO_INTS, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, InsertedTextLabel1, iNumber1, iNumber2, 0, 0.0, 0, InsertedTextLabel2)
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_TWO_STRINGS_AND_AN_INT(STRING TextLabel, STRING InsertedTextLabel1, STRING InsertedTextLabel2, INT iNumber1 = LOWEST_INT)
    INT iReturn=-1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel1)
		ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel2)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_TWO_STRINGS_AND_AN_INT - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_STRINGS_AND_INT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, InsertedTextLabel1, iNumber1, 0, 0, 0.0, 0, InsertedTextLabel2)
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_THREE_STRINGS_AND_AN_INT(STRING TextLabel, STRING InsertedTextLabel1, STRING InsertedTextLabel2, STRING InsertedTextLabel3, INT iNumber1 = LOWEST_INT)
    INT iReturn=-1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel1)
		ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel2)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel3)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_THREE_STRINGS_AND_AN_INT - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_STRINGS_AND_INT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, InsertedTextLabel1, iNumber1, 0, 0, 0.0, 0, InsertedTextLabel2, InsertedTextLabel3)
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_TWO_STRINGS_AND_A_FLOAT(STRING TextLabel, STRING InsertedTextLabel1, STRING InsertedTextLabel2, FLOAT fNumber)
    INT iReturn=-1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel1)
		ADD_TEXT_COMPONENT_FLOAT(fNumber, 1)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel2)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_TWO_STRINGS_AND_A_FLOAT - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_STRINGS_AND_FLOAT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, InsertedTextLabel1, 0, 0, 0, fNumber, 1, InsertedTextLabel2)
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_INT_AND_STRING(STRING TextLabel,PLAYER_INDEX playerID, STRING InsertedTextLabel1, INT iNumber1 = LOWEST_INT)
    INT iReturn=-1
	
	TEXT_LABEL_63 PlayerName
    PlayerName = GET_PLAYER_NAME(PlayerID)
    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
		SCRIPT_ASSERT("PRINT_TICKER_WITH_PLAYER_NAME_INT_AND_STRING - Invalid player name")
	ENDIF

	
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID, DEFAULT, TRUE))
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
		ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel1)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME_INT_AND_STRING - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_INT_AND_STRING, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, iNumber1, 0, 0, 0.0, 0, InsertedTextLabel1)
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_FLOAT_AND_STRING(STRING TextLabel,PLAYER_INDEX playerID, STRING InsertedTextLabel1, FLOAT fNumber)
    INT iReturn=-1
	
	TEXT_LABEL_63 PlayerName
    PlayerName = GET_PLAYER_NAME(PlayerID)
    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
		SCRIPT_ASSERT("PRINT_TICKER_WITH_PLAYER_NAME_FLOAT_AND_STRING - Invalid player name")
	ENDIF

	
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID, DEFAULT, TRUE))
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
		ADD_TEXT_COMPONENT_FLOAT(fNumber, 1)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel1)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME_INT_AND_STRING - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_FLOAT_AND_STRING, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, 0, 0, 0, fNumber, 1, InsertedTextLabel1)
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_TWO_STRINGS_AND_INT(STRING TextLabel,PLAYER_INDEX playerID, STRING InsertedTextLabel1, STRING InsertedTextLabel2, INT iNumber1 = LOWEST_INT)
    INT iReturn=-1
	
	TEXT_LABEL_63 PlayerName
    PlayerName = GET_PLAYER_NAME(PlayerID)
    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
		SCRIPT_ASSERT("PRINT_TICKER_WITH_PLAYER_NAME_TWO_STRINGS_AND_INT - Invalid player name")
	ENDIF

	
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID, DEFAULT, TRUE))
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
		ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel1)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel2)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_TWO_STRINGS_INT_AND_PLAYER_NAME - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_STRINGS_INT_AND_PLAYER_NAME, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, iNumber1, 0, 0, 0.0, 0, InsertedTextLabel2,InsertedTextLabel1)
	
    RETURN(iReturn)
ENDFUNC


FUNC INT PRINT_TICKER_WITH_TWO_STRINGS_FLOAT_AND_PLAYER_NAME(STRING TextLabel,PLAYER_INDEX playerID, STRING InsertedTextLabel1, STRING InsertedTextLabel2, FLOAT fNumber)
    INT iReturn=-1
	
	TEXT_LABEL_63 PlayerName
    PlayerName = GET_PLAYER_NAME(PlayerID)
    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
		SCRIPT_ASSERT("PRINT_TICKER_WITH_TWO_STRINGS_FLOAT_AND_PLAYER_NAME - Invalid player name")
	ENDIF
	
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID, DEFAULT, TRUE))
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
		ADD_TEXT_COMPONENT_FLOAT(fNumber, 1)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel1)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel2)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)

    NET_PRINT("PRINT_TICKER_WITH_TWO_STRINGS_INT_AND_PLAYER_NAME - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_STRINGS_FLOAT_AND_PLAYER_NAME, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, 0, 0, 0, fNumber, 1, InsertedTextLabel2,InsertedTextLabel1)
	
    RETURN(iReturn)
ENDFUNC

 FUNC TEXT_LABEL_63 PREFIX_SOCIAL_CLUB_COLOUR(STRING label)
    
    TEXT_LABEL_63 tl31_Temp
    
    tl31_Temp = "<C>"
    tl31_Temp += "~HUD_COLOUR_SOCIAL_CLUB~"
    tl31_Temp += label
    tl31_Temp += "</C>"
    
    RETURN tl31_Temp
    
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR(STRING TextLabel, PLAYER_INDEX PlayerID, HUD_COLOURS nameColour = HUD_COLOUR_WHITE, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE, BOOL bShowClanInfo = FALSE,  BOOL bUseGameName = TRUE, BOOL bUseSocialClubColour = FALSE)
    INT iReturn=-1
    
    NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - called with text label ") NET_PRINT(TextLabel) NET_NL()
    
    IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), playerID)
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName
        IF NOT (bDontUseCodeName)
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ELSE
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ENDIF
        IF IS_STRING_NULL_OR_EMPTY(PlayerName)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR - Invalid player name")
		ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(nameColour)
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
        IF NOT (bShowClanInfo)
            iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)   
        ELSE
            //NETWORK_CLAN_DESC ClanInfo
            //GAMER_HANDLE GamerHandle =  GET_GAMER_HANDLE_PLAYER(PlayerID)
			g_GamerHandle = GET_GAMER_HANDLE_PLAYER(PlayerID)
            IF NETWORK_CLAN_PLAYER_GET_DESC(g_ClanDescription, SIZE_OF(NETWORK_CLAN_DESC), g_GamerHandle)
                BOOL bIsLeader = FALSE
                IF ARE_STRINGS_EQUAL(g_ClanDescription.RankName, "Leader")
                AND g_ClanDescription.RankOrder = 0
                    bIsLeader = TRUE
                ENDIF
                BOOL bIsPrivate
                IF (g_ClanDescription.IsOpenClan>0)
                    bIsPrivate = FALSE
                ELSE
                    bIsPrivate = TRUE
                ENDIF
                #IF IS_DEBUG_BUILD
                    IF (bUseOldCrewTicker)
                        iReturn = END_TEXT_COMMAND_THEFEED_POST_CREWTAG(bIsPrivate, NETWORK_CLAN_IS_ROCKSTAR_CLAN(g_ClanDescription, SIZE_OF(g_ClanDescription)), g_ClanDescription.ClanTag, g_ClanDescription.RankOrder, bIsLeader, FALSE, g_ClanDescription.Id, g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)        
                        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - called with END_TEXT_COMMAND_THEFEED_POST_CREWTAG ")  NET_NL()
                    ELSE
                #ENDIF
                    IF bUseGameName
                        IF bUseSocialClubColour
                            PlayerName = PREFIX_SOCIAL_CLUB_COLOUR(PlayerName)
                        ENDIF
                        iReturn = END_TEXT_COMMAND_THEFEED_POST_CREWTAG_WITH_GAME_NAME(bIsPrivate, NETWORK_CLAN_IS_ROCKSTAR_CLAN(g_ClanDescription, SIZE_OF(g_ClanDescription)), g_ClanDescription.ClanTag, g_ClanDescription.RankOrder, bIsLeader, FALSE, g_ClanDescription.Id, PlayerName, g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)       
                        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - called with END_TEXT_COMMAND_THEFEED_POST_CREWTAG_WITH_GAME_NAME ")  NET_NL()
                    ELSE
                        iReturn = END_TEXT_COMMAND_THEFEED_POST_CREWTAG(bIsPrivate, NETWORK_CLAN_IS_ROCKSTAR_CLAN(g_ClanDescription, SIZE_OF(g_ClanDescription)), g_ClanDescription.ClanTag, g_ClanDescription.RankOrder, bIsLeader, FALSE, g_ClanDescription.Id, g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)
                        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - called with END_TEXT_COMMAND_THEFEED_POST_CREWTAG ")  NET_NL()
                    ENDIF
                #IF IS_DEBUG_BUILD
                    ENDIF
                #ENDIF
            ELSE            
                NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - could not get clan info ") NET_NL()
                iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
            ENDIF
        ENDIF
		
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName)
        
    ELSE
        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - not in same tut session ") NET_NL()      
    ENDIF
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR_AND_TIME(STRING TextLabel, PLAYER_INDEX PlayerID, HUD_COLOURS nameColour = HUD_COLOUR_WHITE, INT iTimeInMS = 0, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE, BOOL bShowClanInfo = FALSE,  BOOL bUseGameName = TRUE, BOOL bUseSocialClubColour = FALSE, BOOL bUseSingleDotForMsDivider = FALSE)
    INT iReturn=-1
    
    NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - called with text label ") NET_PRINT(TextLabel) NET_NL()
    
    IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), playerID)
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName
        IF NOT (bDontUseCodeName)
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ELSE
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ENDIF
        IF IS_STRING_NULL_OR_EMPTY(PlayerName)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR_AND_TIME - Invalid player name")
		ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(nameColour)
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
			IF NOT bUseSingleDotForMsDivider
				ADD_TEXT_COMPONENT_SUBSTRING_TIME(iTimeInMS, TIME_FORMAT_MILLISECONDS | TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES)
			ELSE
				ADD_TEXT_COMPONENT_SUBSTRING_TIME(iTimeInMS, TIME_FORMAT_MILLISECONDS | TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES | TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER)
			ENDIf
        IF NOT (bShowClanInfo)
            iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)   
        ELSE
            //NETWORK_CLAN_DESC ClanInfo
            //GAMER_HANDLE GamerHandle =  GET_GAMER_HANDLE_PLAYER(PlayerID)
			g_GamerHandle = GET_GAMER_HANDLE_PLAYER(PlayerID)
            IF NETWORK_CLAN_PLAYER_GET_DESC(g_ClanDescription, SIZE_OF(NETWORK_CLAN_DESC), g_GamerHandle)
                BOOL bIsLeader = FALSE
                IF ARE_STRINGS_EQUAL(g_ClanDescription.RankName, "Leader")
                AND g_ClanDescription.RankOrder = 0
                    bIsLeader = TRUE
                ENDIF
                BOOL bIsPrivate
                IF (g_ClanDescription.IsOpenClan>0)
                    bIsPrivate = FALSE
                ELSE
                    bIsPrivate = TRUE
                ENDIF
                #IF IS_DEBUG_BUILD
                    IF (bUseOldCrewTicker)
                        iReturn = END_TEXT_COMMAND_THEFEED_POST_CREWTAG(bIsPrivate, NETWORK_CLAN_IS_ROCKSTAR_CLAN(g_ClanDescription, SIZE_OF(g_ClanDescription)), g_ClanDescription.ClanTag, g_ClanDescription.RankOrder, bIsLeader, FALSE, g_ClanDescription.Id, g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)        
                        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - called with END_TEXT_COMMAND_THEFEED_POST_CREWTAG ")  NET_NL()
                    ELSE
                #ENDIF
                    IF bUseGameName
                        IF bUseSocialClubColour
                            PlayerName = PREFIX_SOCIAL_CLUB_COLOUR(PlayerName)
                        ENDIF
                        iReturn = END_TEXT_COMMAND_THEFEED_POST_CREWTAG_WITH_GAME_NAME(bIsPrivate, NETWORK_CLAN_IS_ROCKSTAR_CLAN(g_ClanDescription, SIZE_OF(g_ClanDescription)), g_ClanDescription.ClanTag, g_ClanDescription.RankOrder, bIsLeader, FALSE, g_ClanDescription.Id, PlayerName, g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)       
                        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - called with END_TEXT_COMMAND_THEFEED_POST_CREWTAG_WITH_GAME_NAME ")  NET_NL()
                    ELSE
                        iReturn = END_TEXT_COMMAND_THEFEED_POST_CREWTAG(bIsPrivate, NETWORK_CLAN_IS_ROCKSTAR_CLAN(g_ClanDescription, SIZE_OF(g_ClanDescription)), g_ClanDescription.ClanTag, g_ClanDescription.RankOrder, bIsLeader, FALSE, g_ClanDescription.Id, g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)
                        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - called with END_TEXT_COMMAND_THEFEED_POST_CREWTAG ")  NET_NL()
                    ENDIF
                #IF IS_DEBUG_BUILD
                    ENDIF
                #ENDIF
            ELSE            
                NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - could not get clan info ") NET_NL()
                iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
            ENDIF
        ENDIF
		
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName)        
    ELSE
        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - not in same tut session ") NET_NL()      
    ENDIF
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR_AND_INT(STRING TextLabel, PLAYER_INDEX PlayerID, HUD_COLOURS nameColour = HUD_COLOUR_WHITE, INT iInsertedInt = 0, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE, BOOL bShowClanInfo = FALSE,  BOOL bUseGameName = TRUE, BOOL bUseSocialClubColour = FALSE)
    INT iReturn=-1
    
    NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - called with text label ") NET_PRINT(TextLabel) NET_NL()
    
    IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), playerID)
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName
        IF NOT (bDontUseCodeName)
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ELSE
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ENDIF
        IF IS_STRING_NULL_OR_EMPTY(PlayerName)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR_AND_INT - Invalid player name")
		ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(nameColour)
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
			ADD_TEXT_COMPONENT_INTEGER(iInsertedInt)
        IF NOT (bShowClanInfo)
            iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)   
        ELSE
            //NETWORK_CLAN_DESC ClanInfo
            //GAMER_HANDLE GamerHandle =  GET_GAMER_HANDLE_PLAYER(PlayerID)
			g_GamerHandle = GET_GAMER_HANDLE_PLAYER(PlayerID)
            IF NETWORK_CLAN_PLAYER_GET_DESC(g_ClanDescription, SIZE_OF(NETWORK_CLAN_DESC), g_GamerHandle)
                BOOL bIsLeader = FALSE
                IF ARE_STRINGS_EQUAL(g_ClanDescription.RankName, "Leader")
                AND g_ClanDescription.RankOrder = 0
                    bIsLeader = TRUE
                ENDIF
                BOOL bIsPrivate
                IF (g_ClanDescription.IsOpenClan>0)
                    bIsPrivate = FALSE
                ELSE
                    bIsPrivate = TRUE
                ENDIF
                #IF IS_DEBUG_BUILD
                    IF (bUseOldCrewTicker)
                        iReturn = END_TEXT_COMMAND_THEFEED_POST_CREWTAG(bIsPrivate, NETWORK_CLAN_IS_ROCKSTAR_CLAN(g_ClanDescription, SIZE_OF(g_ClanDescription)), g_ClanDescription.ClanTag, g_ClanDescription.RankOrder, bIsLeader, FALSE, g_ClanDescription.Id, g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)        
                        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - called with END_TEXT_COMMAND_THEFEED_POST_CREWTAG ")  NET_NL()
                    ELSE
                #ENDIF
                    IF bUseGameName
                        IF bUseSocialClubColour
                            PlayerName = PREFIX_SOCIAL_CLUB_COLOUR(PlayerName)
                        ENDIF
                        iReturn = END_TEXT_COMMAND_THEFEED_POST_CREWTAG_WITH_GAME_NAME(bIsPrivate, NETWORK_CLAN_IS_ROCKSTAR_CLAN(g_ClanDescription, SIZE_OF(g_ClanDescription)), g_ClanDescription.ClanTag, g_ClanDescription.RankOrder, bIsLeader, FALSE, g_ClanDescription.Id, PlayerName, g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)       
                        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - called with END_TEXT_COMMAND_THEFEED_POST_CREWTAG_WITH_GAME_NAME ")  NET_NL()
                    ELSE
                        iReturn = END_TEXT_COMMAND_THEFEED_POST_CREWTAG(bIsPrivate, NETWORK_CLAN_IS_ROCKSTAR_CLAN(g_ClanDescription, SIZE_OF(g_ClanDescription)), g_ClanDescription.ClanTag, g_ClanDescription.RankOrder, bIsLeader, FALSE, g_ClanDescription.Id, g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)
                        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - called with END_TEXT_COMMAND_THEFEED_POST_CREWTAG ")  NET_NL()
                    ENDIF
                #IF IS_DEBUG_BUILD
                    ENDIF
                #ENDIF
            ELSE            
                NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - could not get clan info ") NET_NL()
                iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
            ENDIF
        ENDIF
		
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName)
        
    ELSE
        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - not in same tut session ") NET_NL()      
    ENDIF
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME(STRING TextLabel, PLAYER_INDEX PlayerID, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE, BOOL bShowClanInfo = FALSE,  BOOL bUseGameName = TRUE, BOOL bUseSocialClubColour = FALSE)
    INT iReturn=-1
	INT iTeam = GET_PLAYER_TEAM(PlayerID)
    
    NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - called with text label ") NET_PRINT(TextLabel) NET_NL()
    
    IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), playerID)
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName
        IF NOT (bDontUseCodeName)
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ELSE
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ENDIF
        IF IS_STRING_NULL_OR_EMPTY(PlayerName)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_PLAYER_NAME - Invalid player name")
		ENDIF
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
			IF iTeam != -1 
			AND NETWORK_IS_ACTIVITY_SESSION()
			AND iTeam < FMMC_MAX_TEAMS
				IF g_FMMC_STRUCT.iTeamColourOverride[iTeam] != -1
					NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - iTeamColourOverride has been set ") NET_NL()
					SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam,PlayerID))
				ELSE
	            	SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID, DEFAULT, TRUE))
				ENDIF
			ELSE
				SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID, DEFAULT, TRUE))
			ENDIF
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
        IF NOT (bShowClanInfo)
            iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)   
        ELSE
            //NETWORK_CLAN_DESC ClanInfo
            //GAMER_HANDLE GamerHandle =  GET_GAMER_HANDLE_PLAYER(PlayerID)
			g_GamerHandle = GET_GAMER_HANDLE_PLAYER(PlayerID)
            IF NETWORK_CLAN_PLAYER_GET_DESC(g_ClanDescription, SIZE_OF(NETWORK_CLAN_DESC), g_GamerHandle)
                BOOL bIsLeader = FALSE
                IF ARE_STRINGS_EQUAL(g_ClanDescription.RankName, "Leader")
                AND g_ClanDescription.RankOrder = 0
                    bIsLeader = TRUE
                ENDIF
                BOOL bIsPrivate
                IF (g_ClanDescription.IsOpenClan>0)
                    bIsPrivate = FALSE
                ELSE
                    bIsPrivate = TRUE
                ENDIF
                #IF IS_DEBUG_BUILD
                    IF (bUseOldCrewTicker)
                        iReturn = END_TEXT_COMMAND_THEFEED_POST_CREWTAG(bIsPrivate, NETWORK_CLAN_IS_ROCKSTAR_CLAN(g_ClanDescription, SIZE_OF(g_ClanDescription)), g_ClanDescription.ClanTag, g_ClanDescription.RankOrder, bIsLeader, FALSE, g_ClanDescription.Id, g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)        
                        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - called with END_TEXT_COMMAND_THEFEED_POST_CREWTAG ")  NET_NL()
                    ELSE
                #ENDIF
                    IF bUseGameName
                        IF bUseSocialClubColour
                            PlayerName = PREFIX_SOCIAL_CLUB_COLOUR(PlayerName)
                        ENDIF
                        iReturn = END_TEXT_COMMAND_THEFEED_POST_CREWTAG_WITH_GAME_NAME(bIsPrivate, NETWORK_CLAN_IS_ROCKSTAR_CLAN(g_ClanDescription, SIZE_OF(g_ClanDescription)), g_ClanDescription.ClanTag, g_ClanDescription.RankOrder, bIsLeader, FALSE, g_ClanDescription.Id, PlayerName, g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)       
                        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - called with END_TEXT_COMMAND_THEFEED_POST_CREWTAG_WITH_GAME_NAME ")  NET_NL()
                    ELSE
                        iReturn = END_TEXT_COMMAND_THEFEED_POST_CREWTAG(bIsPrivate, NETWORK_CLAN_IS_ROCKSTAR_CLAN(g_ClanDescription, SIZE_OF(g_ClanDescription)), g_ClanDescription.ClanTag, g_ClanDescription.RankOrder, bIsLeader, FALSE, g_ClanDescription.Id, g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)
                        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - called with END_TEXT_COMMAND_THEFEED_POST_CREWTAG ")  NET_NL()
                    ENDIF
                #IF IS_DEBUG_BUILD
                    ENDIF
                #ENDIF
            ELSE            
                NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - could not get clan info ") NET_NL()
                iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
            ENDIF
        ENDIF
		
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName)
        
    ELSE
        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - not in same tut session ") NET_NL()      
    ENDIF
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_STRING(STRING TextLabel, STRING PlayerName)
    
    INT iReturn=-1
    
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)   
    
    NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_STRING, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName)
        
    RETURN(iReturn)
    
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_AND_TIME(STRING TextLabel, PLAYER_INDEX PlayerID, INT iTimeInMS,  BOOL bIgnoreTutorialCheck = FALSE, BOOL bShowClanInfo = FALSE, BOOL bUseSocialClubColour = FALSE)
    
    INT iReturn = -1
    IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), playerID)
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName = GET_PLAYER_NAME(PlayerID)
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_PLAYER_NAME_AND_TIME - Invalid player name")
		ENDIF

        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
            ADD_TEXT_COMPONENT_SUBSTRING_TIME(iTimeInMS, TIME_FORMAT_MILLISECONDS | TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES)              
        IF NOT (bShowClanInfo)
            iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)   
        ELSE
            //NETWORK_CLAN_DESC ClanInfo
            //GAMER_HANDLE GamerHandle =  GET_GAMER_HANDLE_PLAYER(PlayerID)
			g_GamerHandle = GET_GAMER_HANDLE_PLAYER(PlayerID)
            IF NETWORK_CLAN_PLAYER_GET_DESC(g_ClanDescription, SIZE_OF(NETWORK_CLAN_DESC), g_GamerHandle)
                BOOL bIsLeader = FALSE
                IF ARE_STRINGS_EQUAL(g_ClanDescription.RankName, "Leader")
                AND g_ClanDescription.RankOrder = 0
                    bIsLeader = TRUE
                ENDIF
                BOOL bIsPrivate
                IF (g_ClanDescription.IsOpenClan>0)
                    bIsPrivate = FALSE
                ELSE
                    bIsPrivate = TRUE
                ENDIF
                #IF IS_DEBUG_BUILD
                    IF (bUseOldCrewTicker)
                        iReturn = END_TEXT_COMMAND_THEFEED_POST_CREWTAG(bIsPrivate, NETWORK_CLAN_IS_ROCKSTAR_CLAN(g_ClanDescription, SIZE_OF(g_ClanDescription)), g_ClanDescription.ClanTag, g_ClanDescription.RankOrder, bIsLeader, FALSE, g_ClanDescription.Id, g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)   
                    ELSE
                #ENDIF
                    IF bUseSocialClubColour
                        PlayerName = PREFIX_SOCIAL_CLUB_COLOUR(PlayerName)
                    ENDIF
                    iReturn = END_TEXT_COMMAND_THEFEED_POST_CREWTAG_WITH_GAME_NAME(bIsPrivate, NETWORK_CLAN_IS_ROCKSTAR_CLAN(g_ClanDescription, SIZE_OF(g_ClanDescription)), g_ClanDescription.ClanTag, g_ClanDescription.RankOrder, bIsLeader, FALSE, g_ClanDescription.Id, PlayerName, g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)       
                #IF IS_DEBUG_BUILD
                    ENDIF
                #ENDIF
            ELSE
                NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME_AND_TIME - could not get clan info ") NET_NL()
                iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
            ENDIF
        ENDIF
        

        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME_AND_TIME - called with text label ") NET_PRINT(TextLabel) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_AND_TIME, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, iTimeInMS)
    ENDIF
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_TIME(STRING TextLabel, STRING PlayerName, INT iTimeInMS) //)
    
    INT iReturn = -1
    
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
	
	
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
        ADD_TEXT_COMPONENT_SUBSTRING_TIME(iTimeInMS, TIME_FORMAT_MILLISECONDS | TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES)              
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)   
	
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_AND_TIME, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, iTimeInMS)
        
    RETURN(iReturn)
    
ENDFUNC

FUNC INT PRINT_TICKER_WITH_TIME(STRING TextLabel, INT iTimeInMS, BOOL bUseSingleDotForMsDivider = FALSE) //)
    
    INT iReturn = -1
    TEXT_LABEL_63 PlayerName1
	
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
		IF NOT bUseSingleDotForMsDivider
       		ADD_TEXT_COMPONENT_SUBSTRING_TIME(iTimeInMS, TIME_FORMAT_MILLISECONDS | TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES)    
		ELSE
			ADD_TEXT_COMPONENT_SUBSTRING_TIME(iTimeInMS, TIME_FORMAT_MILLISECONDS | TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES | TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER )   
		ENDIF
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)   
	
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_AND_TIME, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName1, iTimeInMS)
        
    RETURN(iReturn)
    
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_STRING_TIME_AND_STRING(STRING TextLabel, STRING PlayerName, INT iTimeInMS, STRING InsertedTextLabel, BOOL bUseSocialClubColour) //)
    
    INT iReturn = -1
    TEXT_LABEL_63 tl63_playerName
    
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(InsertedTextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_TIME(iTimeInMS, TIME_FORMAT_MILLISECONDS | TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES)  
        IF bUseSocialClubColour
            tl63_playerName = PREFIX_SOCIAL_CLUB_COLOUR(PlayerName)
            iReturn = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, tl63_playerName)
        ELSE
            iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
        ENDIF
    //iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)   
	
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_TIME_AND_STRING, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, iTimeInMS, 0, 0, 0.0, 0, InsertedTextLabel)
        
    RETURN(iReturn)
    
ENDFUNC


//Added by Steve T. Same as above but allows the calling script to specify the length of time the feed message appears on screen via END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT_TU
FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_STRING_TIME_AND_STRING_AND_DURATION_MODIFIER(STRING TextLabel, STRING PlayerName, INT iTimeInMS, STRING InsertedTextLabel, BOOL bUseSocialClubColour, FLOAT DurationModifier = 1.0) //)
    
    INT iReturn = -1
    TEXT_LABEL_63 tl63_playerName
    
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(InsertedTextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_TIME(iTimeInMS, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TIME_FORMAT_MILLISECONDS|TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER) //#1628813 
        IF bUseSocialClubColour
            tl63_playerName = PREFIX_SOCIAL_CLUB_COLOUR(PlayerName)
            iReturn = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT_TU ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, tl63_playerName, "", DurationModifier)
        ELSE
            iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
        ENDIF
    //iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)   
	
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_TIME_AND_STRING, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, iTimeInMS, 0, 0, 0.0, 0, InsertedTextLabel)
        
    RETURN(iReturn)
    
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_STRING_TIME_STRING_AND_INT(STRING TextLabel, STRING PlayerName, INT iTimeInMS, STRING InsertedTextLabel, INT iInsertedInt) //)
    
    DUMMY_REFERENCE_STRING(InsertedTextLabel)
    
    INT iReturn = -1
    
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
        ADD_TEXT_COMPONENT_INTEGER(iInsertedInt)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(InsertedTextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_TIME(iTimeInMS, TIME_FORMAT_MILLISECONDS | TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)   
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_TIME_STRING_AND_INT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, iTimeInMS, iInsertedInt, 0, 0.0, 0, InsertedTextLabel)
        
    RETURN(iReturn)
    
ENDFUNC

FUNC INT PRINT_TICKER_WITH_STRING_INT_PLAYER_NAME_AND_TIME(STRING TextLabel, PLAYER_INDEX PlayerID, STRING InsertedTextLabel, INT iTimeInMS, INT iInsertedInt) //)
    
    INT iReturn = -1
    TEXT_LABEL_63 PlayerName1
    
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        PlayerName1 = GET_PLAYER_NAME(PlayerID)
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName1)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_STRING_INT_PLAYER_NAME_AND_TIME - Invalid player name 1")
		ENDIF
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName1))
        ADD_TEXT_COMPONENT_INTEGER(iInsertedInt)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_TIME(iTimeInMS, TIME_FORMAT_MILLISECONDS | TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)   
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_STRING_INT_PLAYER_NAME_AND_TIME, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName1, iTimeInMS, iInsertedInt, 0, 0.0, 0, InsertedTextLabel)
       
    RETURN(iReturn)
    
ENDFUNC

FUNC INT PRINT_TICKER_WITH_STRING_PLAYER_NAME_AND_TIME(STRING TextLabel, PLAYER_INDEX PlayerID, STRING InsertedTextLabel, INT iTimeInMS) //)
    
    INT iReturn = -1
    TEXT_LABEL_63 PlayerName1
    
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        PlayerName1 = GET_PLAYER_NAME(PlayerID)
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName1)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_STRING_PLAYER_NAME_AND_TIME - Invalid player name 1")
		ENDIF
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName1))
        ADD_TEXT_COMPONENT_SUBSTRING_TIME(iTimeInMS, TIME_FORMAT_MILLISECONDS | TIME_FORMAT_SECONDS | TIME_FORMAT_MINUTES)
        ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)   
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_STRING_PLAYER_NAME_AND_TIME, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName1, iTimeInMS, 0, 0, 0.0, 0, InsertedTextLabel)
        
    RETURN(iReturn)
    
ENDFUNC

FUNC INT PRINT_TICKER_WITH_TWO_PLAYER_NAMES_WITH_HUD_COLOUR(STRING TextLabel, PLAYER_INDEX PlayerID1, PLAYER_INDEX PlayerID2, HUD_COLOURS name1Colour = HUD_COLOUR_WHITE,  HUD_COLOURS name2Colour = HUD_COLOUR_WHITE, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn = -1
    IF (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID1)
    AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID2))
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName1
        TEXT_LABEL_63 PlayerName2 
        IF NOT (bDontUseCodeName)
            PlayerName1 = GET_PLAYER_NAME(PlayerID1)
            PlayerName2 = GET_PLAYER_NAME(PlayerID2)
        ELSE
            PlayerName1 = GET_PLAYER_NAME(PlayerID1)
            PlayerName2 = GET_PLAYER_NAME(PlayerID2)
        ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName1)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_TWO_PLAYER_NAMES_WITH_HUD_COLOUR - Invalid player name 1")
		ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName2)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_TWO_PLAYER_NAMES_WITH_HUD_COLOUR - Invalid player name 2")
		ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(name1Colour)
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName1))
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(name2Colour)
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName2))
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)           
        
        NET_PRINT("PRINT_TICKER_WITH_TWO_PLAYER_NAMES - called with text label ") NET_PRINT(TextLabel) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_PLAYER_NAMES, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName1, 0, 0, 0, 0.0, 0, PlayerName2)
    ENDIF
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_TWO_PLAYER_NAMES(STRING TextLabel, PLAYER_INDEX PlayerID1, PLAYER_INDEX PlayerID2, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn = -1
	INT iTeam1 = GET_PLAYER_TEAM(PlayerID1)
	INT iTeam2 = GET_PLAYER_TEAM(PlayerID2)
	
    IF (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID1)
    AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID2))
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName1
        TEXT_LABEL_63 PlayerName2 
        IF NOT (bDontUseCodeName)
            PlayerName1 = GET_PLAYER_NAME(PlayerID1)
            PlayerName2 = GET_PLAYER_NAME(PlayerID2)
        ELSE
            PlayerName1 = GET_PLAYER_NAME(PlayerID1)
            PlayerName2 = GET_PLAYER_NAME(PlayerID2)
        ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName1)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_TWO_PLAYER_NAMES - Invalid player name 1")
		ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName2)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_TWO_PLAYER_NAMES - Invalid player name 2")
		ENDIF
		
		BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
		IF iTeam1 != -1 
		AND NETWORK_IS_ACTIVITY_SESSION()
		AND iTeam1 < FMMC_MAX_TEAMS
			IF g_FMMC_STRUCT.iTeamColourOverride[iTeam1] != -1
				SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam1,PlayerID1))
			ELSE
            	SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID1, DEFAULT, TRUE))
			ENDIF
		ELSE
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID1, DEFAULT, TRUE))
		ENDIF
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName1))
		
		IF iTeam2 != -1 
		AND NETWORK_IS_ACTIVITY_SESSION()
		AND iTeam2 < FMMC_MAX_TEAMS
			IF g_FMMC_STRUCT.iTeamColourOverride[iTeam2] != -1
				SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam2,PlayerID2))
			ELSE
            	SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID2, DEFAULT, TRUE))
			ENDIF
		ELSE
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID2, DEFAULT, TRUE))
		ENDIF
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName2))

        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)           
        
        NET_PRINT("PRINT_TICKER_WITH_TWO_PLAYER_NAMES - called with text label ") NET_PRINT(TextLabel) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_PLAYER_NAMES, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName1, 0, 0, 0, 0.0, 0, PlayerName2)
    ENDIF
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_THREE_PLAYER_NAMES(STRING TextLabel, PLAYER_INDEX PlayerID1, PLAYER_INDEX PlayerID2, PLAYER_INDEX PlayerID3, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn=-1
    IF (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID1)
    AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID2)
    AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID3))
    OR (bIgnoreTutorialCheck)
        
        TEXT_LABEL_63 PlayerName1
        TEXT_LABEL_63 PlayerName2 
        TEXT_LABEL_63 PlayerName3
        IF NOT (bDontUseCodeName)
            PlayerName1 = GET_PLAYER_NAME(PlayerID1)
            PlayerName2 = GET_PLAYER_NAME(PlayerID2)
            PlayerName3 = GET_PLAYER_NAME(PlayerID3)
        ELSE
            PlayerName1 = GET_PLAYER_NAME(PlayerID1)
            PlayerName2 = GET_PLAYER_NAME(PlayerID2)
            PlayerName3 = GET_PLAYER_NAME(PlayerID3)
        ENDIF 
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName1)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_THREE_PLAYER_NAMES - Invalid player name 1")
		ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName2)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_THREE_PLAYER_NAMES - Invalid player name 2")
		ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName3)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_THREE_PLAYER_NAMES - Invalid player name 3")
		ENDIF  
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID1, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName1))
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID2, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName2))
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID3, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName3))
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)       
        
        NET_PRINT("PRINT_TICKER_WITH_THREE_PLAYER_NAMES - called with text label ") NET_PRINT(TextLabel) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_THREE_PLAYER_NAMES, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName1, 0, 0, 0, 0.0, 0, PlayerName2, PlayerName3)
    ENDIF
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_FOUR_PLAYER_NAMES(STRING TextLabel, PLAYER_INDEX PlayerID1, PLAYER_INDEX PlayerID2, PLAYER_INDEX PlayerID3, PLAYER_INDEX PlayerID4, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn = -1
    IF (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID1)
    AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID2)
    AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID3)
    AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID4))
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName1
        TEXT_LABEL_63 PlayerName2 
        TEXT_LABEL_63 PlayerName3
        TEXT_LABEL_63 PlayerName4
        IF NOT (bDontUseCodeName)
            PlayerName1 = GET_PLAYER_NAME(PlayerID1)
            PlayerName2 = GET_PLAYER_NAME(PlayerID2)
            PlayerName3 = GET_PLAYER_NAME(PlayerID3)
            PlayerName4 = GET_PLAYER_NAME(PlayerID4)
        ELSE
            PlayerName1 = GET_PLAYER_NAME(PlayerID1)
            PlayerName2 = GET_PLAYER_NAME(PlayerID2)
            PlayerName3 = GET_PLAYER_NAME(PlayerID3)
            PlayerName4 = GET_PLAYER_NAME(PlayerID4)
        ENDIF   
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName1)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_FOUR_PLAYER_NAMES - Invalid player name 1")
		ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName2)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_FOUR_PLAYER_NAMES - Invalid player name 2")
		ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName3)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_FOUR_PLAYER_NAMES - Invalid player name 3")
		ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName4)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_FOUR_PLAYER_NAMES - Invalid player name 4")
		ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID1, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName1))
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID2, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName2))
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID3, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName3))
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID4, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName4))
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)       
        
        NET_PRINT("PRINT_TICKER_WITH_FOUR_PLAYER_NAMES - called with text label ") NET_PRINT(TextLabel) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_FOUR_PLAYER_NAMES, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName1, 0, 0, 0, 0.0, 0, PlayerName2, PlayerName3, PlayerName4)
    ENDIF
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_VARIABLE_PLAYER_NAMES(STRING TextLabel1, STRING TextLabel2, STRING TextLabel3, STRING TextLabel4, PLAYER_INDEX PlayerID1, PLAYER_INDEX PlayerID2, PLAYER_INDEX PlayerID3, PLAYER_INDEX PlayerID4, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE)

    INT iReturn=-1

    NET_PRINT("PRINT_TICKER_WITH_VARIABLE_PLAYER_NAMES - PlayerID1 = ") 
    NET_PRINT_INT(NATIVE_TO_INT(PlayerID1))
    NET_PRINT(" PlayerID2 = ")
    NET_PRINT_INT(NATIVE_TO_INT(PlayerID2))
    NET_PRINT(" PlayerID3 = ")
    NET_PRINT_INT(NATIVE_TO_INT(PlayerID3))
    NET_PRINT(" PlayerID4 = ")
    NET_PRINT_INT(NATIVE_TO_INT(PlayerID4))
    NET_NL()

    PLAYER_INDEX Plyr[4]
    INT iCount = 0
    INT i
    REPEAT 4 i
        Plyr[i] = INVALID_PLAYER_INDEX()
    ENDREPEAT
    
    IF NOT (PlayerID1 = INVALID_PLAYER_INDEX())
    AND IS_NET_PLAYER_OK(PlayerID1, FALSE)
    AND (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID1) OR (bIgnoreTutorialCheck))
        IF NOT (PlayerID1 = PlayerID2)
        AND NOT (PlayerID1 = PlayerID3)
        AND NOT (PlayerID1 = PlayerID4)
            Plyr[iCount] = PlayerID1
            iCount++
        ENDIF
    ENDIF
    IF NOT (PlayerID2 = INVALID_PLAYER_INDEX())
    AND IS_NET_PLAYER_OK(PlayerID2, FALSE)
    AND (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID2) OR (bIgnoreTutorialCheck))
        IF NOT (PlayerID2 = PlayerID1)
        AND NOT (PlayerID2 = PlayerID3)
        AND NOT (PlayerID2 = PlayerID4)
            Plyr[iCount] = PlayerID2
            iCount++
        ENDIF
    ENDIF
    IF NOT (PlayerID3 = INVALID_PLAYER_INDEX())
    AND IS_NET_PLAYER_OK(PlayerID3, FALSE)
    AND (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID3) OR (bIgnoreTutorialCheck))
        IF NOT (PlayerID3 = PlayerID1)
        AND NOT (PlayerID3 = PlayerID2)
        AND NOT (PlayerID3 = PlayerID4)
            Plyr[iCount] = PlayerID3
            iCount++
        ENDIF
    ENDIF
    IF NOT (PlayerID4 = INVALID_PLAYER_INDEX())
    AND IS_NET_PLAYER_OK(PlayerID4, FALSE)
    AND (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID4) OR (bIgnoreTutorialCheck))
        IF NOT (PlayerID4 = PlayerID1)
        AND NOT (PlayerID4 = PlayerID2)
        AND NOT (PlayerID4 = PlayerID3)
            Plyr[iCount] = PlayerID4
            iCount++
        ENDIF
    ENDIF
    
    SWITCH iCount
        CASE 0
            // do nothing
            NET_PRINT("PRINT_TICKER_WITH_VARIABLE_PLAYER_NAMES - do nothing") NET_NL()
        BREAK
        CASE 1
            NET_PRINT("PRINT_TICKER_WITH_VARIABLE_PLAYER_NAMES - PRINT_TICKER_WITH_PLAYER_NAME") NET_NL()
            iReturn = PRINT_TICKER_WITH_PLAYER_NAME(TextLabel1, Plyr[0], bDontUseCodeName, bIgnoreTutorialCheck, DEFAULT)
        BREAK
        CASE 2
            NET_PRINT("PRINT_TICKER_WITH_VARIABLE_PLAYER_NAMES - PRINT_TICKER_WITH_TWO_PLAYER_NAMES") NET_NL()
            iReturn = PRINT_TICKER_WITH_TWO_PLAYER_NAMES(TextLabel2, Plyr[0], Plyr[1], bDontUseCodeName, bIgnoreTutorialCheck)
        BREAK
        CASE 3
            NET_PRINT("PRINT_TICKER_WITH_VARIABLE_PLAYER_NAMES - PRINT_TICKER_WITH_THREE_PLAYER_NAMES") NET_NL()
            iReturn = PRINT_TICKER_WITH_THREE_PLAYER_NAMES(TextLabel3, Plyr[0], Plyr[1], Plyr[2], bDontUseCodeName, bIgnoreTutorialCheck)
        BREAK
        CASE 4
            NET_PRINT("PRINT_TICKER_WITH_VARIABLE_PLAYER_NAMES - PRINT_TICKER_WITH_FOUR_PLAYER_NAMES") NET_NL()
            iReturn = PRINT_TICKER_WITH_FOUR_PLAYER_NAMES(TextLabel4, Plyr[0], Plyr[1], Plyr[2], Plyr[3], bDontUseCodeName, bIgnoreTutorialCheck)
        BREAK
    ENDSWITCH
    
    RETURN(iReturn)
ENDFUNC


FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING(STRING TextLabel, PLAYER_INDEX PlayerID, STRING InsertedTextLabel, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn=-1
    IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName
        IF NOT (bDontUseCodeName)
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ELSE
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING - Invalid player name")
		ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
            IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
            ENDIF
            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel)
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)       
        
        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING - called with text label ") NET_PRINT(TextLabel) NET_NL()
		
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_AND_STRING, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, 0, 0, 0, 0.0, 0, InsertedTextLabel)
    ENDIF
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING_WITH_HUD_COLOUR(STRING TextLabel, PLAYER_INDEX PlayerID, STRING InsertedTextLabel, HUD_COLOURS nameColour = HUD_COLOUR_WHITE, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn=-1
    IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName
        IF NOT (bDontUseCodeName)
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ELSE
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING_WITH_HUD_COLOUR - Invalid player name")
		ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(nameColour)
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
            IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
            ENDIF
            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel)
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)       
        
        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING - called with text label ") NET_PRINT(TextLabel) NET_NL()
		
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_AND_STRING, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, 0, 0, 0, 0.0, 0, InsertedTextLabel) 
    ENDIF
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_AND_LITERAL_STRING_WITH_HUD_COLOUR(STRING TextLabel, PLAYER_INDEX PlayerID, STRING InsertedLiteralString, HUD_COLOURS nameColour = HUD_COLOUR_WHITE, HUD_COLOURS ColorOfInsertedLiteralString = HUD_COLOUR_PURE_WHITE, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn=-1
    IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName
        IF NOT (bDontUseCodeName)
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ELSE
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_PLAYER_NAME_AND_LITERAL_STRING_WITH_HUD_COLOUR - Invalid player name")
		ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(nameColour)
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
            IF NOT (ColorOfInsertedLiteralString = HUD_COLOUR_PURE_WHITE)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedLiteralString)
            ENDIF
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(InsertedLiteralString)
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)       
        
        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME_AND_LITERAL_STRING_WITH_HUD_COLOUR - called with text label ") NET_PRINT(TextLabel) NET_NL()
		
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_AND_STRING, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, 0, 0, 0, 0.0, 0, InsertedLiteralString)
    ENDIF
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_AND_TWO_STRINGS(STRING TextLabel, PLAYER_INDEX PlayerID, STRING InsertedTextLabel, STRING InsertedTextLabel2, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, HUD_COLOURS ColorOfInsertedTextLabel2 = HUD_COLOUR_PURE_WHITE, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn=-1
    IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName
        IF NOT (bDontUseCodeName)
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ELSE
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_PLAYER_NAME_AND_TWO_STRINGS - Invalid player name")
		ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
            IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
            ENDIF
            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel)
            IF NOT (ColorOfInsertedTextLabel2 = HUD_COLOUR_PURE_WHITE)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel2)
            ENDIF
            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel2)
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)       
        
        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME_AND_TWO_STRINGS - called with text label ") NET_PRINT(TextLabel) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_AND_TWO_STRINGS, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, 0, 0, 0, 0.0, 0, InsertedTextLabel, InsertedTextLabel2)         

    ENDIF
    RETURN(iReturn)
ENDFUNC

PROC PRINT_HELP_WITH_MORE_THAN_TWO_STRINGS(STRING TextLabel, STRING SubStringTextLabel1, STRING SubStringTextLabel2, STRING SubStringTextLabel3 = NULL, STRING SubStringTextLabel4 = NULL, STRING SubStringTextLabel5 = NULL
											, STRING SubStringTextLabel6 = NULL, STRING SubStringTextLabel7 = NULL, STRING SubStringTextLabel8 = NULL, INT iOverrideTime = -1)
   BEGIN_TEXT_COMMAND_DISPLAY_HELP(TextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel1)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel2)
		IF !IS_STRING_NULL(SubStringTextLabel3)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel3)
		ENDIF
		IF !IS_STRING_NULL(SubStringTextLabel4)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel4)
		ENDIF
		IF !IS_STRING_NULL(SubStringTextLabel5)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel5)
		ENDIF
		IF !IS_STRING_NULL(SubStringTextLabel6)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel6)
		ENDIF
		IF !IS_STRING_NULL(SubStringTextLabel7)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel7)
		ENDIF
		IF !IS_STRING_NULL(SubStringTextLabel8)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel8)
		ENDIF
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, TRUE,iOverrideTime)
ENDPROC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_AND_INT(STRING TextLabel, PLAYER_INDEX PlayerID,INT iNumber1 = LOWEST_INT, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn=-1
    IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName
        IF NOT (bDontUseCodeName)
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ELSE
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_PLAYER_NAME_AND_INT - Invalid player name")
		ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
            IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
            ENDIF
            ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)       
        
        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME_AND_INT - called with text label ") NET_PRINT(TextLabel) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_AND_INT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, iNumber1)
    ENDIF
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_AND_INT_WITH_HUD_COLOUR(STRING TextLabel, PLAYER_INDEX PlayerID,INT iNumber1 = LOWEST_INT, HUD_COLOURS nameColour = HUD_COLOUR_WHITE, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn=-1
    IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName
        IF NOT (bDontUseCodeName)
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ELSE
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_PLAYER_NAME_AND_INT_WITH_HUD_COLOUR - Invalid player name")
		ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(nameColour)
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
            IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
            ENDIF
            ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)       
        
        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME_AND_INT - called with text label ") NET_PRINT(TextLabel) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_AND_INT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, iNumber1)      

    ENDIF
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_AND_FLOAT(STRING TextLabel, PLAYER_INDEX PlayerID, FLOAT fNumber1 = 0.0, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE, BOOL bUseSocialClubColour = FALSE)
    INT iReturn=-1
    IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName
        IF NOT (bDontUseCodeName)

        ELSE
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_PLAYER_NAME_AND_FLOAT - Invalid player name")
		ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
            IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
            ENDIF
            ADD_TEXT_COMPONENT_FLOAT(fNumber1, 1)
            IF bUseSocialClubColour
                PlayerName = PREFIX_SOCIAL_CLUB_COLOUR(PlayerName)
                iReturn = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, PlayerName)
            ELSE
                iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
            ENDIF   
        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME_AND_FLOAT - called with text label ") NET_PRINT(TextLabel) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_AND_FLOAT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, 0, 0, 0, fNumber1, 1)
    ENDIF
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_INT(STRING TextLabel, STRING PlayerName, INT iNumber1 = LOWEST_INT, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, BOOL bUseSocialClubColour = FALSE)
    
    INT iReturn=-1
    TEXT_LABEL_63 tl63_PlayerName
    
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
        IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
        ENDIF
        ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        IF bUseSocialClubColour
            tl63_PlayerName = PREFIX_SOCIAL_CLUB_COLOUR(PlayerName)
            iReturn = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, tl63_PlayerName)
        ELSE
            iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
        ENDIF
        
    NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME_AND_INT - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_AND_INT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, iNumber1)    
    
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_STRING_INT_AND_STRING(STRING TextLabel, STRING PlayerName, STRING InsertedTextLabel, INT iNumber1 = LOWEST_INT, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, BOOL bUseSocialClubColour = FALSE)
    
    INT iReturn=-1
    TEXT_LABEL_63 tl63_PlayerName
    
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
        IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
        ENDIF
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(InsertedTextLabel)
        ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        IF bUseSocialClubColour
            tl63_PlayerName = PREFIX_SOCIAL_CLUB_COLOUR(PlayerName)
            iReturn = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, tl63_PlayerName)
        ELSE
            iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
        ENDIF
    NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME_STRING_INT_AND_STRING - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_INT_AND_STRING, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, iNumber1, 0,0, 0.0, 0, InsertedTextLabel)  
    
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_MISSION_NAME(STRING TextLabel, STRING PlayerName, STRING InsertedTextLabel, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, BOOL bUseSocialClubColour = FALSE)
    INT iReturn=-1
    TEXT_LABEL_63 tl63_PlayerName
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
        IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
        ENDIF
        ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(InsertedTextLabel)
        IF bUseSocialClubColour
            tl63_PlayerName = PREFIX_SOCIAL_CLUB_COLOUR(PlayerName)
            iReturn = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, tl63_PlayerName)
        ELSE
            iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
        ENDIF   
    
    NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_STRING - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_AND_MISSION_NAME, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, InsertedTextLabel, 0, 0,0, 0.0, 0, PlayerName)       
    
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_CUSTOM_STRING_AND_INT(STRING TextLabel, STRING sSubString, INT iNumber1 = 0 )
    INT iReturn=-1

    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sSubString)
        IF iNumber1 !=0
            ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        ENDIF
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)       
    
    NET_PRINT("PRINT_TICKER_WITH_CUSTOM_STRING_AND_INT - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_CUSTOM_STRING_AND_INT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, sSubString, iNumber1)      
    
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_ONE_CUSTOM_STRING(STRING TextLabel, STRING sSubString)
    INT iReturn=-1

    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sSubString)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
    
    NET_PRINT("ciSCTV_TICKER_WITH_ONE_CUSTOM_STRING - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_ONE_CUSTOM_STRING, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, sSubString)        
    
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_ONE_CUSTOM_STRING_WITH_HUD_COLOUR(STRING TextLabel, STRING sSubString, HUD_COLOURS hcStringColour = HUD_COLOUR_WHITE)
    INT iReturn=-1

    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(hcStringColour)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sSubString)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
    
    NET_PRINT("PRINT_TICKER_WITH_ONE_CUSTOM_STRING_WITH_HUD_COLOUR - called with text label ") NET_PRINT(TextLabel) NET_NL()
	NET_PRINT("PRINT_TICKER_WITH_ONE_CUSTOM_STRING_WITH_HUD_COLOUR - called with sSubString ") NET_PRINT(sSubString) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_ONE_CUSTOM_STRING_WITH_HUD_COLOUR, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, sSubString)        
    
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_TWO_CUSTOM_STRINGS(STRING TextLabel, STRING sSubString, STRING sSubString2 )
    INT iReturn=-1

    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sSubString)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sSubString2)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
    
    NET_PRINT("PRINT_TICKER_WITH_TWO_CUSTOM_STRINGS - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_CUSTOM_STRINGS, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, sSubString, 0, 0, 0, 0.0, 0, sSubString2)        
    
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_TWO_CUSTOM_STRINGS_WITH_HUD_COLOURS(STRING TextLabel, STRING sSubString, STRING sSubString2, HUD_COLOURS hcStringColour = HUD_COLOUR_WHITE, HUD_COLOURS hcStringColour2 = HUD_COLOUR_WHITE)
    INT iReturn=-1

    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(hcStringColour)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sSubString)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(hcStringColour2)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sSubString2)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
    
    NET_PRINT("PRINT_TICKER_WITH_TWO_CUSTOM_STRINGS - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_CUSTOM_STRINGS, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, sSubString, 0, 0, 0, 0.0, 0, sSubString2)     
    
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME(STRING TextLabel, STRING sSubString, PLAYER_INDEX PlayerID,HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, BOOL bUseTeam = FALSE)
    INT iReturn=-1


    TEXT_LABEL_63 PlayerName
    PlayerName = GET_PLAYER_NAME(PlayerID)
    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
		SCRIPT_ASSERT("PRINT_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME - Invalid player name")
	ENDIF
    
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
		IF bUseTeam
        	SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID, GET_PLAYER_TEAM(PlayerID), TRUE))
		ELSE
        	SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID, DEFAULT, TRUE))		
		ENDIF
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
        SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sSubString)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)       
    
    NET_PRINT("PRINT_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, 0, 0, 0, 0.0, 0, sSubString)        
    
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME_CUSTOM_HUD_COLOUR(STRING TextLabel, STRING sSubString, PLAYER_INDEX PlayerID,HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
    INT iReturn=-1


    TEXT_LABEL_63 PlayerName
    PlayerName = GET_PLAYER_NAME(PlayerID)
    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
		SCRIPT_ASSERT("PRINT_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME_CUSTOM_HUD_COLOUR - Invalid player name")
	ENDIF
    
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
        SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sSubString)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)       
    
    NET_PRINT("PRINT_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, 0, 0, 0, 0.0, 0, sSubString)      
    
    RETURN(iReturn)
ENDFUNC


FUNC INT PRINT_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME_AND_INT(STRING TextLabel, STRING sSubString, PLAYER_INDEX PlayerID, INT iNumber1 = 0, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
    INT iReturn=-1


    TEXT_LABEL_63 PlayerName
    PlayerName = GET_PLAYER_NAME(PlayerID)
    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
		SCRIPT_ASSERT("PRINT_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME_AND_INT - Invalid player name")
	ENDIF
    
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID, DEFAULT, TRUE))
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
        SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
        IF iNumber1 !=0
            ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        ENDIF
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sSubString)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)       
    
    NET_PRINT("PRINT_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME_AND_INT - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME_AND_INT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, sSubString, iNumber1, 0, 0, 0.0, 0, PlayerName)         
    
    RETURN(iReturn)
ENDFUNC

// KGM 4/12/13	: KGM_CTF_TICKER: Added for CTF to display an icon and to flash the message using team colours
//				: iIconID seems to be: 10 (bag), 11 (base)
//				: ColorOfFlash should be: teamcolour
// iIconID use eICON_TYPE enum
FUNC INT PRINT_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME_AND_ICON_AND_FLASH_COLOUR(STRING TextLabel, STRING sSubString, PLAYER_INDEX PlayerID, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, INT iIconID = 0, HUD_COLOURS ColorOfFlash = HUD_COLOUR_WHITE)
    INT iReturn=-1

    TEXT_LABEL_63 PlayerName
    PlayerName = GET_PLAYER_NAME(PlayerID)
    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
		SCRIPT_ASSERT("PRINT_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME_AND_ICON_AND_FLASH_COLOUR - Invalid player name")
	ENDIF
	
	// Make sure this message flashes with the team colour
    INT r, g, b, a
    GET_HUD_COLOUR(ColorOfFlash, r, g, b, a)
    THEFEED_SET_RGBA_PARAMETER_FOR_NEXT_MESSAGE(r, g, b, a)
    THEFEED_SET_FLASH_DURATION_PARAMETER_FOR_NEXT_MESSAGE(1)
	
	// Display the Feed message (the 'Begin' Text Label refers to the body of the text which accepts a literal string)
	BOOL isImportant = TRUE
	BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
		IF NOT (IS_STRING_NULL_OR_EMPTY(sSubString))
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_WHITE)
			ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(sSubString)
		ENDIF
	END_TEXT_COMMAND_THEFEED_POST_UNLOCK_TU_WITH_COLOR(PlayerName, iIconID, "", isImportant, ColorOfInsertedTextLabel)
	
	THEFEED_RESET_ALL_PARAMETERS()
    
    NET_PRINT("PRINT_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME_AND_ICON_AND_FLASH_COLOUR - called with text label ") NET_PRINT(TextLabel)
	NET_PRINT("[") NET_PRINT(GET_STRING_FROM_TEXT_FILE(TextLabel)) NET_PRINT("]") NET_NL()
	
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME_AND_INT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, sSubString, iIconID, 0, 0, 0.0, 0, PlayerName)
	
	IF NOT (IS_STRING_NULL_OR_EMPTY(sSubString))
		NET_PRINT(" and custom string ") NET_PRINT(sSubString)
	ENDIF
	NET_NL()
    
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_STRING_AND_PLAYER_NAME(STRING TextLabel, STRING InsertedTextLabel, PLAYER_INDEX PlayerID, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE) 
    INT iReturn=-1
    IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName
        IF NOT (bDontUseCodeName)
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ELSE
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_STRING_AND_PLAYER_NAME - Invalid player name")
		ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
            ENDIF
            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))            
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)       

        NET_PRINT("PRINT_TICKER_WITH_STRING_AND_PLAYER_NAME - called with text label ") NET_PRINT(TextLabel) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_STRING_AND_PLAYER_NAME, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, InsertedTextLabel, 0, 0, 0, 0.0, 0, PlayerName)
    ENDIF
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_STRING_INT_AND_PLAYER_NAME(STRING TextLabel, STRING InsertedTextLabel, INT iNumber1, PLAYER_INDEX PlayerID, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE)
    
    INT iReturn=-1
    IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName
        IF NOT (bDontUseCodeName)
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ELSE
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_STRING_INT_AND_PLAYER_NAME - Invalid player name")
		ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
            ENDIF
            ADD_TEXT_COMPONENT_INTEGER(iNumber1)
            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(InsertedTextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))            
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)       

        NET_PRINT("PRINT_TICKER_WITH_STRING_INT_AND_PLAYER_NAME - called with text label ") NET_PRINT(TextLabel) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_STRING_INT_AND_PLAYER_NAME, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, InsertedTextLabel, iNumber1, 0, 0, 0.0, 0, PlayerName)

    ENDIF
    RETURN(iReturn)
    
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_AND_TWO_INTS(STRING TextLabel, PLAYER_INDEX PlayerID, INT iNumber1 = LOWEST_INT, INT iNumber2 = LOWEST_INT, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn = -1
    IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)
    OR bIgnoreTutorialCheck
    
        TEXT_LABEL_63 PlayerName
        IF NOT (bDontUseCodeName)
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ELSE
            PlayerName = GET_PLAYER_NAME(PlayerID)
        ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_PLAYER_NAME_AND_TWO_INTS - Invalid player name")
		ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
            IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
            ENDIF
            ADD_TEXT_COMPONENT_INTEGER(iNumber1)
            IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
            ENDIF
            ADD_TEXT_COMPONENT_INTEGER(iNumber2)
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)           
        
        NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME_AND_TWO_INTS - called with text label ") NET_PRINT(TextLabel) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_AND_TWO_INTS, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, iNumber1, iNumber2)    
    ENDIF
	
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_TWO_INTS(STRING TextLabel, STRING PlayerName, INT iNumber1 = LOWEST_INT, INT iNumber2 = LOWEST_INT, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, BOOL bUseSocialClubColour = FALSE)
    INT iReturn = -1
    TEXT_LABEL_63 tl63_PlayerName
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName))
        IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
        ENDIF
        ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
        ENDIF
        ADD_TEXT_COMPONENT_INTEGER(iNumber2)
        IF bUseSocialClubColour
            tl63_PlayerName = PREFIX_SOCIAL_CLUB_COLOUR(PlayerName)
            iReturn = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, tl63_PlayerName)
        ELSE
            END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
        ENDIF
        
    NET_PRINT("PRINT_TICKER_WITH_PLAYER_NAME_AND_TWO_INTS - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_PLAYER_NAME_STRING_AND_TWO_INTS, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName, iNumber1, iNumber2)
    
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_TWO_INTS(STRING TextLabel, PLAYER_INDEX PlayerID1, PLAYER_INDEX PlayerID2, INT iNumber1 = LOWEST_INT, INT iNumber2 = LOWEST_INT, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn = -1
    IF (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID1)
    AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID2))
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName1
        TEXT_LABEL_63 PlayerName2 
        IF NOT (bDontUseCodeName)
            PlayerName1 = GET_PLAYER_NAME(PlayerID1)
            PlayerName2 = GET_PLAYER_NAME(PlayerID2)
        ELSE
            PlayerName1 = GET_PLAYER_NAME(PlayerID1)
            PlayerName2 = GET_PLAYER_NAME(PlayerID2)
        ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName1)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_TWO_INTS - Invalid player name 1")
		ENDIF
	    IF IS_STRING_NULL_OR_EMPTY(PlayerName2)
			SCRIPT_ASSERT("PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_TWO_INTS - Invalid player name 2")
		ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID1, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName1))
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID2, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName2))
            IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
            ENDIF
            ADD_TEXT_COMPONENT_INTEGER(iNumber1)
            IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
            ENDIF
            ADD_TEXT_COMPONENT_INTEGER(iNumber2)
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)           
        
        NET_PRINT("PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_TWO_INTS - called with text label ") NET_PRINT(TextLabel) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_PLAYER_NAMES_AND_TWO_INTS, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName1, iNumber1, iNumber2, 0, 0.0, 1, PlayerName2)    
    ENDIF
    RETURN(iReturn)
ENDFUNC


FUNC INT PRINT_TICKER_WITH_TWO_PLAYER_NAME_STRING_AND_INT(STRING TextLabel, STRING PlayerName1, STRING PlayerName2, INT iNumber1 = LOWEST_INT, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
    INT iReturn=-1

    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName1))
        IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
        ENDIF
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName2))
        IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
        ENDIF
        ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
        
    NET_PRINT("PRINT_TICKER_WITH_TWO_PLAYER_NAME_STRING_AND_INT - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_PLAYER_NAME_STRING_AND_INT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName1, iNumber1, 0, 0, 0.0, 1, PlayerName2)        
    
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_INT(STRING TextLabel, PLAYER_INDEX PlayerID1, PLAYER_INDEX PlayerID2, INT iNumber1 = LOWEST_INT, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn = -1
    IF (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID1)
    AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID2))
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName1
        TEXT_LABEL_63 PlayerName2 
        IF NOT (bDontUseCodeName)
            PlayerName1 = GET_PLAYER_NAME(PlayerID1)
            PlayerName2 = GET_PLAYER_NAME(PlayerID2)
        ELSE
            PlayerName1 = GET_PLAYER_NAME(PlayerID1)
            PlayerName2 = GET_PLAYER_NAME(PlayerID2)
        ENDIF

        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID1, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName1))
                        
            IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
            ENDIF
            ADD_TEXT_COMPONENT_INTEGER(iNumber1)
            
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID2, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName2))
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)           
        
        NET_PRINT("PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_INT - called with text label ") NET_PRINT(TextLabel) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_PLAYER_NAMES_AND_INT, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName1, iNumber1, 0, 0, 0.0, 1, PlayerName2)    
    ENDIF
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_TEXT_LABEL(STRING TextLabel, PLAYER_INDEX PlayerID1, PLAYER_INDEX PlayerID2, STRING TextLabel2, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn = -1
    IF (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID1)
    AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID2))
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName1
        TEXT_LABEL_63 PlayerName2 
        IF NOT (bDontUseCodeName)
            PlayerName1 = GET_PLAYER_NAME(PlayerID1)
            PlayerName2 = GET_PLAYER_NAME(PlayerID2)
        ELSE
            PlayerName1 = GET_PLAYER_NAME(PlayerID1)
            PlayerName2 = GET_PLAYER_NAME(PlayerID2)
        ENDIF

        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID1, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName1))
                        
            IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
            ENDIF
            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(TextLabel2)
            
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID2, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName2))
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)           
        
        NET_PRINT("PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_TEXT_LABEL - called with text label ") NET_PRINT(TextLabel) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_PLAYER_NAMES_AND_TEXT_LABEL, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName1, 0, 0, 0, 0.0, 1, TextLabel2, PlayerName2)    
    ENDIF
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_TWO_TEXT_LABELS(STRING TextLabel, PLAYER_INDEX PlayerID1, PLAYER_INDEX PlayerID2, STRING TextLabel2, STRING TextLabel3, HUD_COLOURS ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE, BOOL bDontUseCodeName=FALSE, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn = -1
    IF (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID1)
    AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID2))
    OR (bIgnoreTutorialCheck)
    
        TEXT_LABEL_63 PlayerName1
        TEXT_LABEL_63 PlayerName2 
        IF NOT (bDontUseCodeName)
            PlayerName1 = GET_PLAYER_NAME(PlayerID1)
            PlayerName2 = GET_PLAYER_NAME(PlayerID2)
        ELSE
            PlayerName1 = GET_PLAYER_NAME(PlayerID1)
            PlayerName2 = GET_PLAYER_NAME(PlayerID2)
        ENDIF

        BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
            
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(TextLabel2)
			
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID1, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName1))
                        
            IF NOT (ColorOfInsertedTextLabel = HUD_COLOUR_PURE_WHITE)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(ColorOfInsertedTextLabel)
            ENDIF
			
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(TextLabel3)
            
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(PlayerID2, DEFAULT, TRUE))
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(PlayerName2))
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)           
        
        NET_PRINT("PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_TWO_TEXT_LABELS - called with text label ") NET_PRINT(TextLabel) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_PLAYER_NAMES_AND_TEXT_LABEL, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, PlayerName1, 0, 0, 0, 0.0, 1, TextLabel2, PlayerName2)    
    ENDIF
    RETURN(iReturn)
ENDFUNC


FUNC INT PRINT_TICKER_WITH_TEAM_NAME(STRING TextLabel, INT iTeam, BOOL bPlural = FALSE, BOOL bAppendThe = FALSE, BOOL bStartOfSentence = FALSE)
    
    RETURN PRINT_TICKER_WITH_STRING(TextLabel, GET_TEAM_NAME(iTeam, bPlural, bAppendThe, bStartOfSentence), GET_TEAM_HUD_COLOUR(iTeam) , HUD_COLOUR_PURE_WHITE)  

ENDFUNC


PROC PRINT_CASH_TICKER(INT iCash, BOOL bIsCop=FALSE)

    IF (bIsCop)
        // do nothing any more, use to be used to not display dollar sign for cops
    ENDIF
    
    iCash = iCash

    #IF IS_DEBUG_BUILD
    NET_PRINT("PRINT_CASH_TICKER - called with ") NET_PRINT_INT(iCash) NET_PRINT(" - NO LONGER DISPLAYS ANYTHING") NET_NL()
    #ENDIF

// Removed with 808335 so code can control display of this.
//  IF (iCash < 0)      
//      iCash *= -1
//      BEGIN_TEXT_COMMAND_SHORT_HUD_MESSAGE("T_NEG_DOLLAR")
//          //SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_RED) // colour is set in the text file
//          ADD_TEXT_COMPONENT_INTEGER(iCash)           
//      END_TEXT_COMMAND_SHORT_HUD_MESSAGE()                        
//  ELSE
//      BEGIN_TEXT_COMMAND_SHORT_HUD_MESSAGE("T_DOLLAR")
//          //SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_GREEN) // colour is set in the text file
//          ADD_TEXT_COMPONENT_INTEGER(iCash)           
//      END_TEXT_COMMAND_SHORT_HUD_MESSAGE()
//  ENDIF
    
ENDPROC



/// PURPOSE:
///    Add a ticker for a player who has killed themself.
/// PARAMS:
///    joiningPlayer - The player who has just joined.
FUNC INT ADD_TICKER_PLAYER_DIED(PLAYER_INDEX deadPlayer, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn = -1
    // If the player is not active, bail.
    IF NOT IS_NET_PLAYER_OK(deadPlayer,FALSE,FALSE)
    OR (deadPlayer = PLAYER_ID())
        PRINTSTRING("ADD_TICKER_PLAYER_DIED - player not ok") PRINTNL()
        RETURN(iReturn)
    ENDIF
    
    IF NOT (g_bCompKillTickers) // check rowan isn't dealing with the tickers
	AND NOT g_bNoDeathTickers
        IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), deadPlayer)
        OR (bIgnoreTutorialCheck)
        
            #IF IS_DEBUG_BUILD
                NET_PRINT_TIME() NET_PRINT_THREAD_ID()
                PRINTSTRING("ADD_TICKER_PLAYER_DIED Player = ") PRINTSTRING(GET_PLAYER_NAME(deadPlayer)) PRINTNL()
            #ENDIF
                
            iReturn = PRINT_TICKER_WITH_PLAYER_NAME("TICK_DIED", deadPlayer, DEFAULT, DEFAULT, DEFAULT)
        
        ENDIF
    ENDIF
    RETURN(iReturn)
ENDFUNC



/// PURPOSE:
///    Add a ticker for a player who has killed themself.
/// PARAMS:
///    joiningPlayer - The player who has just joined.
FUNC INT ADD_TICKER_PLAYER_SUICIDE(PLAYER_INDEX deadPlayer, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn = -1
    // If the player is not active, bail.
    IF NOT IS_NET_PLAYER_OK(deadPlayer,FALSE,FALSE)
    OR (deadPlayer = PLAYER_ID())
        PRINTSTRING("ADD_TICKER_PLAYER_SUICIDE - player not ok") PRINTNL()
        RETURN(iReturn)
    ENDIF
    
    IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), deadPlayer)
    OR (bIgnoreTutorialCheck)
    
        #IF IS_DEBUG_BUILD
            NET_PRINT_TIME() NET_PRINT_THREAD_ID()
            PRINTSTRING("ADD_TICKER_PLAYER_SUICIDE Player = ") PRINTSTRING(GET_PLAYER_NAME(deadPlayer)) PRINTNL()
        #ENDIF
            
        iReturn = PRINT_TICKER_WITH_PLAYER_NAME("TICK_SUICIDE", deadPlayer, DEFAULT, DEFAULT, DEFAULT)    
    
        //play sound
        PLAY_SOUND_FRONTEND(-1, "SUICIDE", "HUD_MINI_GAME_SOUNDSET") 
    
    ENDIF
    RETURN(iReturn)
ENDFUNC


/// PURPOSE:
///    Add a ticker for a player who has just joined.
/// PARAMS:
///    joiningPlayer - The player who has just joined.
FUNC INT ADD_TICKER_PLAYER_JOINED(PLAYER_INDEX joiningPlayer, BOOL bIgnoreTutorialCheck = FALSE, BOOL bShowClanInfo=FALSE)
    
    INT iReturn = -1
    
    // If the you are the player who joined, bail.
    IF NOT IS_NET_PLAYER_OK(joiningPlayer,FALSE,FALSE)
    OR (joiningPlayer = PLAYER_ID())
        RETURN(iReturn)
    ENDIF   
    
    IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), joiningPlayer)
    OR (bIgnoreTutorialCheck)
    
        #IF IS_DEBUG_BUILD
            PRINTSTRING("ADD_TICKER_PLAYER_JOINED Player = ") PRINTSTRING(GET_PLAYER_NAME(joiningPlayer)) PRINTNL()
        #ENDIF
    
        iReturn = PRINT_TICKER_WITH_PLAYER_NAME("TICK_JOIN", joiningPlayer, DEFAULT, bIgnoreTutorialCheck, bShowClanInfo, FALSE)

    ENDIF
    
    RETURN(iReturn)
ENDFUNC

#IF FEATURE_GEN9_EXCLUSIVE
/// PURPOSE:
///    Add a ticker for a player who has just joined.
/// PARAMS:
///    joiningPlayer - The player who has just joined.
FUNC INT ADD_TICKER_BLOCKED_PLAYER_JOINED(PLAYER_INDEX joiningPlayer)
    PRINTLN("[BLOCKED_PLAYER] ADD_TICKER_BLOCKED_PLAYER_JOINED Player = ",GET_PLAYER_NAME(joiningPlayer))
	//[FM_BLK_PLYR_TICK]
	//Blocked Gamer: ~n~ ~a~ ~n~~s~is currently in this session.
    RETURN PRINT_TICKER_WITH_PLAYER_NAME("FM_BLK_PLYR_TICK", joiningPlayer, DEFAULT, FALSE, FALSE, FALSE)
ENDFUNC
#ENDIF

/// PURPOSE:
///    Add a ticker for a player who has just left the game.
/// PARAMS:
///    sPlayerWhoLeft - The name of the player who left.
///    iTeam - The team the player was on.
FUNC INT ADD_TICKER_PLAYER_LEFT_GAME(STRING sPlayerWhoLeft, INT iTeam)//, BOOL bIgnoreTutorialCheck = FALSE)
    
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO()
    	NET_PRINT("ADD_TICKER_PLAYER_LEFT_GAME - stopped, player is currently on the gen9 intro mission ") NET_NL()
		RETURN -1
	ENDIF
	#ENDIF
	
    NET_PRINT("ADD_TICKER_PLAYER_LEFT_GAME - start, called for ") NET_PRINT(sPlayerWhoLeft) NET_NL()

    INT iReturn = -1
    //PRINT_TICKER_WITH_LITERAL_STRING(eTICKER_LEFT, "TICK_LEFT", sPlayerWhoLeft, GET_TEAM_HUD_COLOUR(iTeam), -1)
    //IF NOT  NETWORK_IS_IN_TUTORIAL_SESSION()
    //OR (bIgnoreTutorialCheck)
        
        IF (iTeam = 0)
            // redundant
        ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST("TICK_LEFT")
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_WHITE)
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(sPlayerWhoLeft)         )
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)       
    
        NET_PRINT("ADD_TICKER_PLAYER_LEFT_GAME - called for ") NET_PRINT(sPlayerWhoLeft) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_PLAYER_LEFT_GAME, "TICK_LEFT", ciSCTV_TICKER_PRIORITY_MEDIUM, sPlayerWhoLeft)
        
        DEBUG_PRINTCALLSTACK() 
        
        //private_PRINT_NEW_TICKER(tickerMessages, eTICKER_LEFT, "TICK_LEFT", "", "", sPlayerWhoLeft, "", "", "", GET_TEAM_HUD_COLOUR(iTeam))
    //ENDIF
    RETURN(iReturn)
ENDFUNC


/// PURPOSE:
///    Add a ticker for a player with bounty who has just left the game.
/// PARAMS:
///    sPlayerWhoLeft - The name of the player who left.
///    iTeam - The team the player was on.
FUNC INT ADD_TICKER_PLAYER_LEFT_GAME_CHEATING(STRING sPlayerWhoLeft)
    INT iReturn = -1
	STRING textLabel = "TICK_LEFT"
    
	SWITCH g_sMPTunables.iKick_Feed_SCAdminBan
		
		CASE 0
			ADD_TICKER_PLAYER_LEFT_GAME(sPlayerWhoLeft, 0)
		BREAK
		
		CASE 1
			textLabel = "TICK_LEFTKICK" //~a~~HUD_COLOUR_WHITE~ has been kicked from the session.
		BREAK
		
		CASE 2
			textLabel = "TICK_LEFTCHEAT" //~a~~HUD_COLOUR_WHITE~ was detected cheating and has been removed from the session.
		BREAK
		
		CASE 3
			 DEBUG_PRINTCALLSTACK() 
			 RETURN iReturn
		BREAK
	ENDSWITCH
        
    BEGIN_TEXT_COMMAND_THEFEED_POST(textLabel)
        SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_WHITE)
        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(sPlayerWhoLeft))
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)       

    NET_PRINT("ADD_TICKER_PLAYER_LEFT_GAME_CHEATING - called for ") NET_PRINT(sPlayerWhoLeft) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_PLAYER_LEFT_GAME, textLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, sPlayerWhoLeft)
    
    DEBUG_PRINTCALLSTACK() 


    RETURN(iReturn)
ENDFUNC


/// PURPOSE:
///    Add a ticker for a player with bounty who has just left the game.
/// PARAMS:
///    sPlayerWhoLeft - The name of the player who left.
///    iTeam - The team the player was on.
FUNC INT ADD_CUSTOM_TICKER_PLAYER_LEFT_GAME(STRING textLabel, STRING sPlayerWhoLeft, INT iTeam) //, BOOL bIgnoreTutorialCheck = FALSE)
    INT iReturn = -1
    //PRINT_TICKER_WITH_LITERAL_STRING(eTICKER_LEFT, "TICK_LEFT", sPlayerWhoLeft, GET_TEAM_HUD_COLOUR(iTeam), -1)
    //IF NOT  NETWORK_IS_IN_TUTORIAL_SESSION()
    //OR (bIgnoreTutorialCheck)
        
        IF (iTeam = 0)
            // redundant
        ENDIF
        
        BEGIN_TEXT_COMMAND_THEFEED_POST(textLabel)
            SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_WHITE)
            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(sPlayerWhoLeft))
        iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)       
    
        NET_PRINT("ADD_CUSTOM_TICKER_PLAYER_LEFT_GAME - called for ") NET_PRINT(sPlayerWhoLeft) NET_NL()
		ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_PLAYER_LEFT_GAME, textLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, sPlayerWhoLeft)
        
        DEBUG_PRINTCALLSTACK() 

        //private_PRINT_NEW_TICKER(tickerMessages, eTICKER_LEFT, "TICK_LEFT", "", "", sPlayerWhoLeft, "", "", "", GET_TEAM_HUD_COLOUR(iTeam))
    //ENDIF
    RETURN(iReturn)
ENDFUNC

//// PURPOSE:
 ///    Add a ticker for when one player kills another.
 /// PARAMS:
 ///    deadPlayer - The player who got killed.
 ///    killer - The player who done the killing.
FUNC INT ADD_TICKER_PLAYER_KILLED(PLAYER_INDEX deadPlayer, PLAYER_INDEX killer, BOOL bIgnoreTutorialCheck=FALSE)
    INT iReturn = -1    
    // If either player is not active, bail.
    IF NOT IS_NET_PLAYER_OK(deadPlayer,FALSE,FALSE)
    OR NOT IS_NET_PLAYER_OK(killer,FALSE,FALSE)
	
		#IF IS_DEBUG_BUILD
		IF NOT IS_NET_PLAYER_OK(deadPlayer,FALSE,FALSE)
			NET_PRINT("ADD_TICKER_PLAYER_KILLED: IS_NET_PLAYER_OK(deadPlayer,FALSE,FALSE) = FALSE, not registering with ticker") NET_NL()
		ENDIF
		#ENDIF
		
        RETURN(iReturn)
    ENDIF
    IF NOT (g_bCompKillTickers) // check rowan isn't dealing with the tickers
	AND NOT (g_bNoDeathTickers)
        IF (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), deadPlayer) AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), killer))
        OR (bIgnoreTutorialCheck)
        
            #IF IS_DEBUG_BUILD
                NET_PRINT_TIME() NET_PRINT_THREAD_ID()
                PRINTSTRING("ADD_TICKER_PLAYER_KILLED Dead Player = ") PRINTSTRING(GET_PLAYER_NAME(deadPlayer)) PRINTNL()
            #ENDIF
            
//			// "~a~~HUD_COLOUR_WHITE~ infected ~a~."
//			////////////////////////////////////
//			#IF FEATURE_NEW_AMBIENT_EVENTS
//			IF NOT GlobalplayerBD_FM[NATIVE_TO_INT(deadPlayer)].bAmInfected
//			AND GlobalplayerBD_FM[NATIVE_TO_INT(killer)].bAmInfected
//			AND GlobalplayerBD_FM[NATIVE_TO_INT(deadPlayer)].bInfectionParticipant
//				iReturn = PRINT_TICKER_WITH_TWO_PLAYER_NAMES("INF_TICK_3", killer, deadPlayer #IF IS_DEBUG_BUILD, FALSE #ENDIF )
//				PRINTLN("     ---------->     INFECTION - ADD_TICKER_PLAYER_KILLED - Modifiying kill feed to read infected.")
//			ELSE
//			#ENDIF

			IF IS_NET_PLAYER_OK(killer)
			AND IS_BIT_SET(GlobalPlayerBD[NATIVE_TO_INT(killer)].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CANNON)

				// ~a~~HUD_COLOUR_WHITE~ was obliterated by the Orbital Cannon.
				IF killer = PLAYER_ID()
					iReturn = PRINT_TICKER_WITH_PLAYER_NAME("ORB_REMOT2", deadPlayer)
				ELSE
					iReturn = PRINT_TICKER_WITH_PLAYER_NAME("ORB_REMOT", deadPlayer)
				ENDIF
			ELSE
			
         	iReturn = PRINT_TICKER_WITH_TWO_PLAYER_NAMES("TICK_KILL", killer, deadPlayer #IF IS_DEBUG_BUILD, FALSE #ENDIF )
			   
			ENDIF
            
//			#IF FEATURE_NEW_AMBIENT_EVENTS
//			ENDIF
//			#ENDIF
		ELSE
		#IF IS_DEBUG_BUILD
		IF NOT (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), deadPlayer) AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), killer))
			NET_PRINT("ADD_TICKER_PLAYER_KILLED: NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION = FALSE, not registering with ticker") NET_NL()
		ENDIF
		
		IF (bIgnoreTutorialCheck)
			NET_PRINT("ADD_TICKER_PLAYER_KILLED: bIgnoreTutorialCheck = FALSE, not registering with ticker") NET_NL()
		ENDIF
		#ENDIF
        ENDIF   
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("ADD_TICKER_PLAYER_KILLED: g_bCompKillTickers = ",g_bCompKillTickers,", g_bNoDeathTickers = ",g_bNoDeathTickers,", not registering with ticker")
		#ENDIF
    ENDIF
    RETURN(iReturn)
ENDFUNC


FUNC INT ADD_TICKER_PLAYER_KILLED_PLAYER_HEADSHOT(PLAYER_INDEX deadPlayer, PLAYER_INDEX killer, BOOL bIgnoreTutorialCheck=FALSE)
    INT iReturn = -1    
    // If either player is not active, bail.
    IF NOT IS_NET_PLAYER_OK(deadPlayer,FALSE,FALSE)
    OR NOT IS_NET_PLAYER_OK(killer,FALSE,FALSE)
	
		#IF IS_DEBUG_BUILD
		IF NOT IS_NET_PLAYER_OK(deadPlayer,FALSE,FALSE)
			NET_PRINT("ADD_TICKER_PLAYER_KILLED_PLAYER_HEADSHOT: IS_NET_PLAYER_OK(deadPlayer,FALSE,FALSE) = FALSE, not registering with ticker") NET_NL()
		ENDIF
		#ENDIF
		
        RETURN(iReturn)
    ENDIF
    IF NOT (g_bCompKillTickers) // check rowan isn't dealing with the tickers
	AND NOT g_bNoDeathTickers
        IF (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), deadPlayer) AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), killer))
        OR (bIgnoreTutorialCheck)
        
            #IF IS_DEBUG_BUILD
                NET_PRINT_TIME() NET_PRINT_THREAD_ID()
                PRINTSTRING("ADD_TICKER_PLAYER_KILLED_PLAYER_HEADSHOT Dead Player = ") PRINTSTRING(GET_PLAYER_NAME(deadPlayer)) PRINTNL()
            #ENDIF
			
            iReturn = PRINT_TICKER_WITH_TWO_PLAYER_NAMES("TICK_HEAD", killer, deadPlayer #IF IS_DEBUG_BUILD, FALSE #ENDIF )
		ELSE
		#IF IS_DEBUG_BUILD
		IF NOT (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), deadPlayer) AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), killer))
			NET_PRINT("ADD_TICKER_PLAYER_KILLED_PLAYER_HEADSHOT: NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION = FALSE, not registering with ticker") NET_NL()
		ENDIF
		
		IF (bIgnoreTutorialCheck)
			NET_PRINT("ADD_TICKER_PLAYER_KILLED_PLAYER_HEADSHOT: bIgnoreTutorialCheck = FALSE, not registering with ticker") NET_NL()
		ENDIF
		#ENDIF
        ENDIF   
	ELSE
		#IF IS_DEBUG_BUILD
			NET_PRINT("ADD_TICKER_PLAYER_KILLED_PLAYER_HEADSHOT: g_bCompKillTickers = FALSE, not registering with ticker") NET_NL()
		#ENDIF
    ENDIF
    RETURN(iReturn)
ENDFUNC

 // #IF FEATURE_NEW_AMBIENT_EVENTS

//// PURPOSE:
 ///    Add a ticker for when the beast kills a player, to hide beast player's name.
 /// PARAMS:
 ///    deadPlayer - The player who got killed.
FUNC INT ADD_TICKER_PLAYER_KILLED_BY_BEAST(PLAYER_INDEX deadPlayer, PLAYER_INDEX killer, BOOL bIgnoreTutorialCheck=FALSE)
    INT iReturn = -1    
    // If either player is not active, bail.
    IF NOT IS_NET_PLAYER_OK(deadPlayer,FALSE,FALSE)
    OR NOT IS_NET_PLAYER_OK(killer,FALSE,FALSE)
        RETURN(iReturn)
    ENDIF
    IF NOT (g_bCompKillTickers) // check rowan isn't dealing with the tickers
        IF (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), deadPlayer) AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), killer))
        OR (bIgnoreTutorialCheck)
        
            #IF IS_DEBUG_BUILD
                NET_PRINT_TIME() NET_PRINT_THREAD_ID()
                PRINTSTRING("ADD_TICKER_PLAYER_KILLED Dead Player = ") PRINTSTRING(GET_PLAYER_NAME(deadPlayer)) PRINTNL()
            #ENDIF
            
            iReturn = PRINT_TICKER_WITH_PLAYER_NAME("AMHB_TICK1", deadPlayer)
        ENDIF   
    ENDIF
    RETURN(iReturn)
ENDFUNC

//// PURPOSE:
 ///    Add a ticker for when a player is knocked out on a mode
 /// PARAMS:
 ///    deadPlayer - The player who got killed.
FUNC INT ADD_TICKER_PLAYER_KNOCKED_OUT(PLAYER_INDEX deadPlayer, PLAYER_INDEX killer, BOOL bIgnoreTutorialCheck=FALSE)
    INT iReturn = -1    
    // If either player is not active, bail.
    IF NOT IS_NET_PLAYER_OK(deadPlayer,FALSE,FALSE)
    OR NOT IS_NET_PLAYER_OK(killer,FALSE,FALSE)
        RETURN(iReturn)
    ENDIF
    IF NOT (g_bCompKillTickers) // check rowan isn't dealing with the tickers
        IF (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), deadPlayer) AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), killer))
        OR (bIgnoreTutorialCheck)
        
            #IF IS_DEBUG_BUILD
                NET_PRINT_TIME() NET_PRINT_THREAD_ID()
                PRINTSTRING("ADD_TICKER_PLAYER_KNOCKED_OUT - Dead Player = ") PRINTSTRING(GET_PLAYER_NAME(deadPlayer)) PRINTNL()
            #ENDIF
			IF deadPlayer = killer
				IF deadPlayer = PLAYER_ID()
					PRINT_TICKER("BBTI_KOd")
				ELSE
					PRINT_TICKER_WITH_PLAYER_NAME("BBTI_KOa", deadPlayer)
				ENDIF
            ELIF deadPlayer = PLAYER_ID()
				iReturn = PRINT_TICKER_WITH_PLAYER_NAME("BBTI_KOc", killer)
			ELSE
           		iReturn = PRINT_TICKER_WITH_TWO_PLAYER_NAMES("BBTI_KOb", killer, deadPlayer)
			ENDIF
        ENDIF   
    ENDIF
    RETURN(iReturn)
ENDFUNC

//// PURPOSE:
 ///    Add a ticker for when a player kills the beast, to hide beast player's name.
 /// PARAMS:
 ///    deadPlayer - The player who got killed.
FUNC INT ADD_TICKER_BEAST_KILLED(PLAYER_INDEX deadPlayer, PLAYER_INDEX killer, BOOL bIgnoreTutorialCheck=FALSE)
    INT iReturn = -1    
    // If either player is not active, bail.
    IF NOT IS_NET_PLAYER_OK(deadPlayer,FALSE,FALSE)
    OR NOT IS_NET_PLAYER_OK(killer,FALSE,FALSE)
        RETURN(iReturn)
    ENDIF
    IF NOT (g_bCompKillTickers) // check rowan isn't dealing with the tickers
        IF (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), deadPlayer) AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), killer))
        OR (bIgnoreTutorialCheck)
        
            #IF IS_DEBUG_BUILD
                NET_PRINT_TIME() NET_PRINT_THREAD_ID()
                PRINTSTRING("ADD_TICKER_PLAYER_KILLED Dead Player = ") PRINTSTRING(GET_PLAYER_NAME(deadPlayer)) PRINTNL()
            #ENDIF

            iReturn = PRINT_TICKER_WITH_PLAYER_NAME("AMHB_TICK2", killer)
   
        ENDIF   
    ENDIF
    RETURN(iReturn)
ENDFUNC

FUNC INT PRINT_TICKER_WITH_TWO_INTS_AND_TOKENS(STRING TextLabel, INT iNumber1=LOWEST_INT, INT iNumber2=LOWEST_INT)
    INT iReturn = -1
    BEGIN_TEXT_COMMAND_THEFEED_POST(TextLabel)
        ADD_TEXT_COMPONENT_INTEGER(iNumber1)
        ADD_TEXT_COMPONENT_INTEGER(iNumber2)
    iReturn = END_TEXT_COMMAND_THEFEED_POST_TICKER_WITH_TOKENS(TRUE,FALSE)

    NET_PRINT("PRINT_TICKER_WITH_TWO_INTS - called with text label ") NET_PRINT(TextLabel) NET_NL()
	ADD_TICKER_TO_SCTV_TICKER_QUEUE(ciSCTV_TICKER_WITH_TWO_INTS, TextLabel, ciSCTV_TICKER_PRIORITY_MEDIUM, "", iNumber1, iNumber2)

    RETURN(iReturn)
ENDFUNC

FUNC STRING sGANG_ROLE(PLAYER_INDEX playerId)

	STRING sReturn
	
	IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(playerId)
		sReturn = "GB_TICK_RLE1"
	ELIF GB_IS_PLAYER_BOSS_OF_A_GANG(playerId)
		IF DOES_PLAYER_OWN_OFFICE(playerId)
			sReturn = "GB_TICK_RLE2"
		ELSE
			sReturn = "GB_TICK_RLE3"
		ENDIF
	ENDIF
	
	RETURN sReturn
ENDFUNC

FUNC INT ADD_TICKER_PLAYER_KILLED_BOSS(PLAYER_INDEX deadPlayer, PLAYER_INDEX killer, BOOL bIgnoreTutorialCheck=FALSE)
    INT iReturn = -1    
    // If either player is not active, bail.
    IF NOT IS_NET_PLAYER_OK(deadPlayer,FALSE,FALSE)
    OR NOT IS_NET_PLAYER_OK(killer,FALSE,FALSE)
	
		#IF IS_DEBUG_BUILD
		IF NOT IS_NET_PLAYER_OK(deadPlayer,FALSE,FALSE)
			NET_PRINT("ADD_TICKER_PLAYER_KILLED_BOSS: IS_NET_PLAYER_OK(deadPlayer,FALSE,FALSE) = FALSE, not registering with ticker") NET_NL()
		ENDIF
		#ENDIF
		
        RETURN(iReturn)
    ENDIF
    IF NOT (g_bCompKillTickers) // check rowan isn't dealing with the tickers
	AND NOT g_bNoDeathTickers
        IF (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), deadPlayer) AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), killer))
        OR (bIgnoreTutorialCheck)
        
            #IF IS_DEBUG_BUILD
                NET_PRINT_TIME() NET_PRINT_THREAD_ID()
                PRINTSTRING("ADD_TICKER_PLAYER_KILLED_BOSS Dead Player = ") PRINTSTRING(GET_PLAYER_NAME(deadPlayer)) PRINTNL()
            #ENDIF
			IF GB_IS_PLAYER_BOSS_OF_A_GANG(killer)
			AND GB_IS_PLAYER_BOSS_OF_A_GANG(deadPlayer)
			
				iReturn = PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_TWO_TEXT_LABELS("GB_TICK_BSSV", killer, deadPlayer, sGANG_ROLE(killer), sGANG_ROLE(deadPlayer))
			
			ELIF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(deadPlayer)
			
				iReturn = PRINT_TICKER_WITH_TWO_PLAYER_NAMES("GB_TICK_K_MC_P", killer, deadPlayer #IF IS_DEBUG_BUILD, FALSE #ENDIF )
			ELSE
				IF DOES_PLAYER_OWN_OFFICE(deadPlayer)
	            	iReturn = PRINT_TICKER_WITH_TWO_PLAYER_NAMES("GB_TICK_KILLC", killer, deadPlayer #IF IS_DEBUG_BUILD, FALSE #ENDIF )
	            ELSE
					iReturn = PRINT_TICKER_WITH_TWO_PLAYER_NAMES("GB_TICK_KILLB", killer, deadPlayer #IF IS_DEBUG_BUILD, FALSE #ENDIF )
				ENDIF
			ENDIF
		ELSE
		#IF IS_DEBUG_BUILD
		IF NOT (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), deadPlayer) AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), killer))
			NET_PRINT("ADD_TICKER_PLAYER_KILLED_BOSS: NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION = FALSE, not registering with ticker") NET_NL()
		ENDIF
		
		IF (bIgnoreTutorialCheck)
			NET_PRINT("ADD_TICKER_PLAYER_KILLED_BOSS: bIgnoreTutorialCheck = FALSE, not registering with ticker") NET_NL()
		ENDIF
		#ENDIF
        ENDIF   
	ELSE
		#IF IS_DEBUG_BUILD
			IF g_bCompKillTickers
				PRINTLN("ADD_TICKER_PLAYER_KILLED_BOSS: FALSE, not registering with ticker because g_bCompKillTickers ")
			ELIF g_bNoDeathTickers
				PRINTLN("ADD_TICKER_PLAYER_KILLED_BOSS: FALSE, not registering with ticker because g_bNoDeathTickers ")
			ENDIF
		#ENDIF
    ENDIF
    RETURN(iReturn)
ENDFUNC

FUNC INT ADD_TICKER_PLAYER_KILLED_GOON(PLAYER_INDEX deadPlayer, PLAYER_INDEX killer, BOOL bIgnoreTutorialCheck=FALSE)
    INT iReturn = -1    
    // If either player is not active, bail.
    IF NOT IS_NET_PLAYER_OK(deadPlayer,FALSE,FALSE)
    OR NOT IS_NET_PLAYER_OK(killer,FALSE,FALSE)
	
		#IF IS_DEBUG_BUILD
		IF NOT IS_NET_PLAYER_OK(deadPlayer,FALSE,FALSE)
			NET_PRINT("ADD_TICKER_PLAYER_KILLED_GOON: IS_NET_PLAYER_OK(deadPlayer,FALSE,FALSE) = FALSE, not registering with ticker") NET_NL()
		ENDIF
		#ENDIF
		
        RETURN(iReturn)
    ENDIF
    IF NOT (g_bCompKillTickers) // check rowan isn't dealing with the tickers
	AND NOT g_bNoDeathTickers
        IF (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), deadPlayer) AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), killer))
        OR (bIgnoreTutorialCheck)
        
            #IF IS_DEBUG_BUILD
                NET_PRINT_TIME() NET_PRINT_THREAD_ID()
                PRINTSTRING("ADD_TICKER_PLAYER_KILLED_GOON Dead Player = ") PRINTSTRING(GET_PLAYER_NAME(deadPlayer)) PRINTNL()
            #ENDIF
			
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(deadPlayer)
				GB_GANG_ROLE gRole
				gRole = GB_GET_PLAYER_GANG_ROLE(deadPlayer)
				IF gRole = GR_ENFORCER
					iReturn = PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_TEXT_LABEL("GB_TICKKILLGBN", killer, deadPlayer, "PI_BIK_E_T", DEFAULT #IF IS_DEBUG_BUILD, FALSE #ENDIF )
				ELSE
					iReturn = PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_TEXT_LABEL("GB_TICKKILLGB", killer, deadPlayer, GB_GET_PLAYER_BIKER_ROLE_TEXT_LABEL(deadPlayer, TRUE), DEFAULT #IF IS_DEBUG_BUILD, FALSE #ENDIF )
				ENDIF
			ELSE
	            IF DOES_PLAYER_OWN_OFFICE(GB_GET_THIS_PLAYER_GANG_BOSS(deadPlayer))
					iReturn = PRINT_TICKER_WITH_TWO_PLAYER_NAMES("GB_TICKKILLGC", killer, deadPlayer #IF IS_DEBUG_BUILD, FALSE #ENDIF )
				ELSE
				   	iReturn = PRINT_TICKER_WITH_TWO_PLAYER_NAMES("GB_TICK_KILLG", killer, deadPlayer #IF IS_DEBUG_BUILD, FALSE #ENDIF )
				ENDIF
			ENDIF
		ELSE
		#IF IS_DEBUG_BUILD
		IF NOT (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), deadPlayer) AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), killer))
			NET_PRINT("ADD_TICKER_PLAYER_KILLED_GOON: NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION = FALSE, not registering with ticker") NET_NL()
		ENDIF
		
		IF (bIgnoreTutorialCheck)
			NET_PRINT("ADD_TICKER_PLAYER_KILLED_GOON: bIgnoreTutorialCheck = FALSE, not registering with ticker") NET_NL()
		ENDIF
		#ENDIF
        ENDIF   
	ELSE
		#IF IS_DEBUG_BUILD
			NET_PRINT("ADD_TICKER_PLAYER_KILLED_GOON: g_bCompKillTickers = FALSE, not registering with ticker") NET_NL()
		#ENDIF
    ENDIF
    RETURN(iReturn)
ENDFUNC

