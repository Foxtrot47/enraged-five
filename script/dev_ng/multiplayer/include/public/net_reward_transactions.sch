//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		DESCRIPTION		:	Controller that manages a queue of reward items that must	//
//							be aquired using cash transactions            				//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "MP_globals_reward_transactions_definitions.sch"

USING "shop_public.sch"
USING "fm_in_corona_header.sch"

/// PURPOSE:
///    Get a corresponding catalog key for an item hash
/// PARAMS:
///    iLabelHash - 
/// RETURNS:
///    
FUNC INT GET_CATALOG_KEY_FOR_REWARD_CLO_ITEM(INT iLabelHash)
	SWITCH iLabelHash
		CASE HASH("CLO_LTSMH_0_1") // Gray Bulletproof Helmet
			RETURN HASH("M_CL_CLO_LTSMH_0_1_t14_v2")
		BREAK
	ENDSWITCH
	
	RETURN HASH("")
ENDFUNC

/// PURPOSE:
///    Get a stat corresponding to a given slot in the transaction queue
/// PARAMS:
///    iSlotID - 
/// RETURNS:
///    
FUNC MP_INT_STATS GET_MP_STAT_FOR_REWARD_TRANSACTIONS_QUEUE_SLOT(INT iSlotID)
	SWITCH iSlotID
		CASE 0
			RETURN MP_STAT_REWARDTRANSACTION_CATKEY_0
		BREAK
		CASE 1
			RETURN MP_STAT_REWARDTRANSACTION_CATKEY_1
		BREAK
		CASE 2
			RETURN MP_STAT_REWARDTRANSACTION_CATKEY_2
		BREAK
		CASE 3
			RETURN MP_STAT_REWARDTRANSACTION_CATKEY_3
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("GET_MP_STAT_FOR_REWARD_TRANSACTIONS_QUEUE_SLOT: slotID out of range!")
	
	RETURN MP_STAT_REWARDTRANSACTION_CATKEY_0
ENDFUNC

FUNC BOOL DOES_REWARD_TRANSACTION_QUEUE_NEEDS_SYNC()
	// If any element of the queue needs sync then all of them do
	RETURN REWARD_TRANSACTIONS_QUEUE_SIZE > 0 AND g_rewardTransactionsQueue[0].status = REWARD_TRANSACTION_NEEDS_SYNC
ENDFUNC

/// PURPOSE:
///    Gets values for catalog items keys in the queue from the stats.
PROC SYNC_REWARD_TRANSACTION_QUEUE_WITH_MP_STATS()
	#IF IS_DEBUG_BUILD
		CDEBUG3LN(DEBUG_SHOPS, "[SYNC_REWARD_TRANSACTION_QUEUE_WITH_MP_STATS] - Syncing queue with stats")
	#ENDIF
		
	INT i, iCatalogKey
	
	// Check if saved stats contain any sensible values or just defaults
	IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_REWARDTRANSACTION_SYNCED)
		REPEAT REWARD_TRANSACTIONS_QUEUE_SIZE i
			SET_MP_INT_CHARACTER_STAT(GET_MP_STAT_FOR_REWARD_TRANSACTIONS_QUEUE_SLOT(i), HASH(""))
			g_rewardTransactionsQueue[i].status = REWARD_TRANSACTION_INVALID
		ENDREPEAT
		
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_REWARDTRANSACTION_SYNCED, TRUE)
		
		#IF IS_DEBUG_BUILD
			CDEBUG3LN(DEBUG_SHOPS, "[SYNC_REWARD_TRANSACTION_QUEUE_WITH_MP_STATS] - Queue mp stats contained defaults so initialised it")
		#ENDIF
	ELSE
		// Stats have been synced so queue saved in mp stats contains actual item hashes (or empty hashes)
		REPEAT REWARD_TRANSACTIONS_QUEUE_SIZE i
			IF g_rewardTransactionsQueue[i].status = REWARD_TRANSACTION_NEEDS_SYNC
				#IF IS_DEBUG_BUILD
					CDEBUG3LN(DEBUG_SHOPS, "[SYNC_REWARD_TRANSACTION_QUEUE_WITH_MP_STATS] - Slot ", i, " needs syncing")
				#ENDIF
					
				iCatalogKey = GET_MP_INT_CHARACTER_STAT(GET_MP_STAT_FOR_REWARD_TRANSACTIONS_QUEUE_SLOT(i))
				
				IF iCatalogKey != HASH("")
					g_rewardTransactionsQueue[i].status = REWARD_TRANSACTION_PENDING
					
					#IF IS_DEBUG_BUILD
						CDEBUG3LN(DEBUG_SHOPS, "[SYNC_REWARD_TRANSACTION_QUEUE_WITH_MP_STATS] - Found saved queue item at slot id: ", i, " item cat key: ", iCatalogKey)
					#ENDIF
				ELSE
					g_rewardTransactionsQueue[i].status = REWARD_TRANSACTION_INVALID
				ENDIF
			ENDIF
		ENDREPEAT
	
	ENDIF
ENDPROC

/// PURPOSE:
///    Call this to reward the specified item using a transaction
/// PARAMS:
///    iItemHash - 
/// RETURNS:
///    TRUE if item has been added to the queue and will be awarded, FALSE otherwise
///    
FUNC BOOL REWARD_ITEM_THROUGH_TRANSACTION(INT iItemHash)

	IF NOT USE_SERVER_TRANSACTIONS()
		RETURN TRUE
	ENDIF
	
	IF DOES_REWARD_TRANSACTION_QUEUE_NEEDS_SYNC()
		SYNC_REWARD_TRANSACTION_QUEUE_WITH_MP_STATS()
	ENDIF
	
	INT i
	INT iFreeSlot = -1
	
	REPEAT REWARD_TRANSACTIONS_QUEUE_SIZE i
		// Look for an empty slot in the queue
		IF g_rewardTransactionsQueue[i].status = REWARD_TRANSACTION_INVALID
			iFreeSlot = i
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	IF iFreeSlot = -1
		// Queue is full
		#IF IS_DEBUG_BUILD
			CDEBUG3LN(DEBUG_SHOPS, "[REWARD_ITEM_THROUGH_TRANSACTION] - FAIL: Queue is full!")
		#ENDIF
	
		RETURN FALSE
	ENDIF
	 
	INT iCatalogItemKey = GET_CATALOG_KEY_FOR_REWARD_CLO_ITEM(iItemHash)
	
	IF iCatalogItemKey != HASH("")
		SET_MP_INT_CHARACTER_STAT(GET_MP_STAT_FOR_REWARD_TRANSACTIONS_QUEUE_SLOT(iFreeSlot), iCatalogItemKey)
		
		//g_rewardTransactionsQueue[iFreeSlot].iCatalogItemKey = iCatalogItemKey
		g_rewardTransactionsQueue[iFreeSlot].status = REWARD_TRANSACTION_PENDING
		
		#IF IS_DEBUG_BUILD
			CDEBUG3LN(DEBUG_SHOPS, "[REWARD_ITEM_THROUGH_TRANSACTION] - Item with hash ", iItemHash, " pushed on reward queue at position ", iFreeSlot, ", cat item key: ", iCatalogItemKey)
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		CDEBUG3LN(DEBUG_SHOPS, "[REWARD_ITEM_THROUGH_TRANSACTION] - FAIL: Item with hash ", iItemHash, " doesn't have a corresponding free catalog item")
	#ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_REWARD_TRANSACTIONS_QUEUE()

	IF NOT USE_SERVER_TRANSACTIONS() OR NOT NETWORK_IS_GAME_IN_PROGRESS()
		// Bail if not in MP or we're not using server transactions (only PC uses server transactions)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_DebugRewardHelmetViaTransaction
			REWARD_ITEM_THROUGH_TRANSACTION(HASH("CLO_LTSMH_0_1"))
			g_DebugRewardHelmetViaTransaction = FALSE
		ENDIF
	#ENDIF

	// Run this every 1000 frames
	IF NOT (GET_FRAME_COUNT() % 1500 = 0)
		EXIT
	ENDIF
	
	IF DOES_REWARD_TRANSACTION_QUEUE_NEEDS_SYNC()
		SYNC_REWARD_TRANSACTION_QUEUE_WITH_MP_STATS()
	ENDIF
	
	/*
	// Check if the catalog is loaded, request it if not
	IF NOT NET_GAMESERVER_CATALOG_IS_VALID() // AND NOT NET_GAMESERVER_RETRIEVE_CATALOG_STATUS()
		NET_GAMESERVER_REFRESH_SERVER_CATALOG()
		#IF IS_DEBUG_BUILD
			CDEBUG3LN(DEBUG_SHOPS, "[PROCESS_REWARD_TRANSACTIONS_QUEUE] - Catalog not valid, retrieving")
		#ENDIF
		EXIT
	ENDIF
	*/
	
	#IF IS_DEBUG_BUILD
		CDEBUG3LN(DEBUG_SHOPS, "[PROCESS_REWARD_TRANSACTIONS_QUEUE] - Doing this now")
	#ENDIF
	
	INT i, iCatalogKey
	BOOL bItemsArePending, bBasketIsReady
	
	// First see if we have any items pending
	REPEAT REWARD_TRANSACTIONS_QUEUE_SIZE i
		IF g_rewardTransactionsQueue[i].status = REWARD_TRANSACTION_PENDING
			bItemsArePending = TRUE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	// Check if we can use the basket
	IF bItemsArePending AND NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_PLAYER_IN_ANY_SHOP() AND NOT IS_PLAYER_IN_CORONA()
		IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() = -1
			bBasketIsReady = TRUE
		ELSE
			#IF IS_DEBUG_BUILD
				CDEBUG3LN(DEBUG_SHOPS, "[PROCESS_REWARD_TRANSACTIONS_QUEUE] - GET_BASKET_TRANSACTION_SCRIPT_INDEX != -1, bBasketIsReady = false")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			CDEBUG3LN(DEBUG_SHOPS, "[PROCESS_REWARD_TRANSACTIONS_QUEUE] - No items pending or player in shop or injured or in corona, bBasketIsReady = false")
		#ENDIF
	ENDIF
	
	IF bBasketIsReady
		REPEAT REWARD_TRANSACTIONS_QUEUE_SIZE i
			
			IF g_rewardTransactionsQueue[i].status = REWARD_TRANSACTION_PENDING
				
				iCatalogKey = GET_MP_INT_CHARACTER_STAT(GET_MP_STAT_FOR_REWARD_TRANSACTIONS_QUEUE_SLOT(i))
				
				// Check if the item is valid
				IF NOT NET_GAMESERVER_CATALOG_ITEM_KEY_IS_VALID(iCatalogKey)
					#IF IS_DEBUG_BUILD
						CDEBUG3LN(DEBUG_SHOPS, "[PROCESS_REWARD_TRANSACTIONS_QUEUE] - FAIL: Catalog item key at slot ", i, " is invalid! catalog key: ", iCatalogKey)
					#ENDIF
					g_rewardTransactionsQueue[i].status = REWARD_TRANSACTION_FAILED
					RELOOP
				ENDIF
				
				// Only free items (that are specifically set up to have price 0, since they are rewards) can be awarded
				IF NET_GAMESERVER_GET_PRICE(iCatalogKey, CATEGORY_CLOTH, 1) != 0
					#IF IS_DEBUG_BUILD
						CDEBUG3LN(DEBUG_SHOPS, "[PROCESS_REWARD_TRANSACTIONS_QUEUE] - FAIL: Catalog item key at slot ", i, " is not free!")
					#ENDIF
					g_rewardTransactionsQueue[i].status = REWARD_TRANSACTION_FAILED
					RELOOP
				ENDIF
				
				// Put the item in the basket
				IF NOT NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CLOTH, iCatalogKey, NET_SHOP_ACTION_SPEND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, 0, CTPF_AUTO_PROCESS_REPLY)
					#IF IS_DEBUG_BUILD
						CDEBUG3LN(DEBUG_SHOPS, "[PROCESS_REWARD_TRANSACTIONS_QUEUE] - FAIL: Failed to put item from slot ", i, " into the basket!")
					#ENDIF
					g_rewardTransactionsQueue[i].status = REWARD_TRANSACTION_FAILED
					RELOOP	
				ELSE
					#IF IS_DEBUG_BUILD
						CDEBUG3LN(DEBUG_SHOPS, "[PROCESS_REWARD_TRANSACTIONS_QUEUE] - item from slot ", i, " is now in the basket, catalog key: ", iCatalogKey)
					#ENDIF
				ENDIF
			
			ENDIF
			
		ENDREPEAT
		
		IF NOT NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
			DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
			
			REPEAT REWARD_TRANSACTIONS_QUEUE_SIZE i
				IF g_rewardTransactionsQueue[i].status = REWARD_TRANSACTION_PENDING
					g_rewardTransactionsQueue[i].status = REWARD_TRANSACTION_FAILED
				ENDIF
			ENDREPEAT
			
			#IF IS_DEBUG_BUILD
				CDEBUG3LN(DEBUG_SHOPS, "[PROCESS_REWARD_TRANSACTIONS_QUEUE] - FAIL: Transaction fail")
			#ENDIF
		ELSE
			
			REPEAT REWARD_TRANSACTIONS_QUEUE_SIZE i
				IF g_rewardTransactionsQueue[i].status = REWARD_TRANSACTION_PENDING
					g_rewardTransactionsQueue[i].status = REWARD_TRANSACTION_DONE
				ENDIF
			ENDREPEAT
			
			#IF IS_DEBUG_BUILD
				CDEBUG3LN(DEBUG_SHOPS, "[PROCESS_REWARD_TRANSACTIONS_QUEUE] - REWARD_TRANSACTION_DONE for the whole basket")
			#ENDIF
		ENDIF
		
		// Cleanup finished transitions
		REPEAT REWARD_TRANSACTIONS_QUEUE_SIZE i
			IF g_rewardTransactionsQueue[i].status = REWARD_TRANSACTION_FAILED
			OR g_rewardTransactionsQueue[i].status = REWARD_TRANSACTION_DONE
				g_rewardTransactionsQueue[i].status = REWARD_TRANSACTION_INVALID
				SET_MP_INT_CHARACTER_STAT(GET_MP_STAT_FOR_REWARD_TRANSACTIONS_QUEUE_SLOT(i), HASH(""))
				
				#IF IS_DEBUG_BUILD
					CDEBUG3LN(DEBUG_SHOPS, "[PROCESS_REWARD_TRANSACTIONS_QUEUE] - Cleaned up slot ", i)
				#ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC
