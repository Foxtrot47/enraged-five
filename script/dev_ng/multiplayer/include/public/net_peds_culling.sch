//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_CULLING.sch																					//
// Description: Header file containing culling functionality for peds.													//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		26/02/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
USING "net_peds_creation.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ FUNCTIONS ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_PED_CULLING_ACTIVE(INT &iPedDataBS[PEDS_DATA_BITSET_ARRAY_SIZE])
	RETURN IS_PEDS_BIT_SET(iPedDataBS, BS_PED_DATA_CULLING_ACTIVE)
ENDFUNC

FUNC BOOL IS_PED_CULLING_AREA_VALID(PED_AREAS ePedArea)
	RETURN (ePedArea != PED_AREA_INVALID AND ePedArea != PED_AREA_TOTAL)
ENDFUNC

PROC SET_PED_CULLING_STATE(INT iPed, PED_CULLING_STATES &eCullingState, PED_CULLING_STATES eNewState)
	UNUSED_PARAMETER(iPed)
	IF (eCullingState != eNewState)
		eCullingState = eNewState
		#IF IS_DEBUG_BUILD
		PRINTLN("[AM_MP_PEDS] SET_PED_CULLING_STATE - Ped: ", iPed,  " Updating state to: ", DEBUG_GET_PED_CULLING_STATE_STRING(eNewState))
		#ENDIF
	ENDIF
ENDPROC

PROC RESET_PED_CULLING_LOCATE_DATA(PED_CULLING_LOCATE_DATA &pedCullingLocateData)
	pedCullingLocateData.vLocateVectorOne 		= <<0.0, 0.0, 0.0>>
	pedCullingLocateData.vLocateVectorTwo		= <<0.0, 0.0, 0.0>>
	pedCullingLocateData.fLocateWidth			= 1.0
	pedCullingLocateData.bHighlightArea 		= FALSE
	pedCullingLocateData.bCheck3D 				= TRUE
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_IN_CULLING_LOCATE(PED_LOCATIONS ePedLocation, PED_AREAS ePedArea, PED_CULLING_LOCATE_DATA pedCullingLocateData)
	
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	RESET_PED_CULLING_LOCATE_DATA(pedCullingLocateData)
	GET_PED_CULLING_LOCATE_DATA(ePedLocation, ePedArea, pedCullingLocateData)
	
	IF (VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), pedCullingLocateData.vLocateVectorOne) < MAX_PED_CULLING_LOCATE_PROCESS_DISTANCE)
		RETURN IS_POINT_IN_ANGLED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), pedCullingLocateData.vLocateVectorOne, pedCullingLocateData.vLocateVectorTwo, pedCullingLocateData.fLocateWidth)
	ENDIF
	
	RETURN FALSE
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ MAINTAIN ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC UPDATE_PED_CULLING_LOCATES(PED_LOCATIONS ePedLocation, PED_AREAS &eCullingArea)
	
	IF NOT PROCESS_FUNCTION_THIS_FRAME(MAX_NUM_CULLING_PROCESSING_FRAMES)
		EXIT
	ENDIF
	
	INT iArea
	PED_CULLING_LOCATE_DATA pedCullingLocateData
	
	REPEAT PED_AREA_TOTAL iArea
		PED_AREAS ePedArea = INT_TO_ENUM(PED_AREAS, iArea)
		IF IS_PED_CULLING_AREA_VALID(ePedArea)
			IF IS_LOCAL_PLAYER_IN_CULLING_LOCATE(ePedLocation, ePedArea, pedCullingLocateData)
				IF (eCullingArea != ePedArea)
					eCullingArea = ePedArea
					PRINTLN("[AM_MP_PEDS] UPDATE_PED_CULLING_LOCATES - Updating culling area to: ", DEBUG_GET_PED_AREA_STRING(ePedArea))
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC UPDATE_PED_CULLING(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, PED_AREAS eCullingArea, SCRIPT_PED_LOITER_DATA &LoiterData, INT iPed, INT iLayout #IF IS_DEBUG_BUILD, PED_DEBUG_DATA &DebugData #ENDIF)
	
	// Permanent peds do not cull
	IF (Data.cullingData.ePedArea = PED_AREA_PERMANENT)
		EXIT
	ENDIF
	
	// Reset ped culling flow if player has changed area again mid-cull.
	IF (Data.cullingData.eCullingState > PED_CULL_STATE_IDLE AND Data.cullingData.ePedArea != eCullingArea)
		SET_PED_CULLING_STATE(iPed, Data.cullingData.eCullingState, PED_CULL_STATE_IDLE)
	ENDIF
	
	SWITCH Data.cullingData.eCullingState
		CASE PED_CULL_STATE_IDLE
			
			// Update ped area and set culling as active
			IF (Data.cullingData.ePedArea != eCullingArea)
				Data.cullingData.ePedArea = eCullingArea
				SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CULLING_ACTIVE)
				SET_PED_CULLING_STATE(iPed, Data.cullingData.eCullingState, PED_CULL_STATE_SET_DATA)
				#IF IS_DEBUG_BUILD
				Data.PedDebugData.iCullingArea = ENUM_TO_INT(eCullingArea)-1
				#ENDIF
			ENDIF
			
		BREAK
		CASE PED_CULL_STATE_SET_DATA
			
			// Store current ped activity to compare with new data
			INT iCurrentPedActivity
			iCurrentPedActivity = Data.iActivity
			
			// Updated ped data based on new ped area
			RESET_PED_DATA(Data)
			SET_PED_DATA(ePedLocation, Data, iPed, iLayout, FALSE)
//			SET_PED_PATROL_DATA(ePedLocation, Data, iPed)
			
			// Ped has to be recreated if model has changed.
			// Delete ped here and let main loop recreate ped.
			IF (GET_ENTITY_MODEL(Data.PedID) != GET_PEDS_MODEL(INT_TO_ENUM(PED_MODELS, Data.iPedModel)))
				DELETE_LOCAL_SCRIPT_PED(Data)
			
			// Need to update ped props if activity has changed.
			ELIF (iCurrentPedActivity != Data.iActivity)
				DELETE_LOCAL_SCRIPT_PED_PROPS(Data)
			ENDIF
			
			SET_PED_CULLING_STATE(iPed, Data.cullingData.eCullingState, PED_CULL_STATE_MOVE_PED)
			
		BREAK
		CASE PED_CULL_STATE_MOVE_PED
			
			// Ensure ped has been created
			IF NOT IS_ENTITY_ALIVE(Data.PedID)
				EXIT
			ENDIF
			
			// Ensure ped props have been created
			CREATE_LOCAL_SCRIPT_PED_PROPS(ePedLocation, Data)
			IF NOT HAS_PED_PROPS_BEEN_CREATED(Data)
				EXIT
			ENDIF
			
			// Move ped if their coords/rotation have updated.
			// This isn't needed for peds that have been recreated.
			IF NOT ARE_VECTORS_EQUAL(GET_ENTITY_COORDS(Data.PedID), Data.vPosition, TRUE)
				SET_PED_CLOTHING_COMPONENTS(Data.PedID, Data.iBS, Data.iPackedDrawable, Data.iPackedTexture)
				SET_LOCAL_PED_PROPERTIES(ePedLocation, Data.PedID)
				SET_PED_PROP_INDEXES(ePedLocation, Data.PedID, iPed, iLayout, INT_TO_ENUM(PED_MODELS, Data.iPedModel))
				
				SET_ENTITY_COORDS_NO_OFFSET(Data.PedID, Data.vPosition)
				SET_ENTITY_ROTATION(Data.PedID, Data.vRotation, DEFAULT, FALSE)
			ENDIF
			
			SET_PED_CULLING_STATE(iPed, Data.cullingData.eCullingState, PED_CULL_STATE_RESET_PED_DATA)
			
		BREAK
		CASE PED_CULL_STATE_RESET_PED_DATA
			
			// Clear the culling active flag and the main loop
			// will start playing anims on this ped again.
			CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_CULLING_ACTIVE)
			
			// Reset anim data so anims play from the start
			ANIM_PED_DATA blankAnimPedData
			Data.animData = blankAnimPedData
			
			// Reset speech data
			RESET_NET_TIMER(LoiterData.stLoiterTimer)
			
			#IF IS_DEBUG_BUILD
			DebugData.SpeechData.iLoiterPedID = -1
			DebugData.SpeechData.iGameTimeLoiterSpeechSet = 0
			#ENDIF
			
			SET_PED_CULLING_STATE(iPed, Data.cullingData.eCullingState, PED_CULL_STATE_UNLOAD_ASSETS)
			
		BREAK
		CASE PED_CULL_STATE_UNLOAD_ASSETS
			
			// [SAR]
			// We havent recreated the ped here.
			// So we are unloading something we havent even requested yet.
			
			// Need to unload after all peds created and culling finished
			// Unload all assets
			// 10 peds a frame
			
			
			
			
			//UNLOAD_PED_ASSETS(INT_TO_ENUM(PED_MODELS, Data.iPedModel), INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
			SET_PED_CULLING_STATE(iPed, Data.cullingData.eCullingState, PED_CULL_STATE_IDLE)
			
		BREAK
	ENDSWITCH
	
ENDPROC
#ENDIF	// FEATURE_HEIST_ISLAND
