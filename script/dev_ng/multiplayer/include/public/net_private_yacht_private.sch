USING "net_private_yacht.sch"
USING "net_realty_new.sch"


//TWEAK_FLOAT YACHT_OBJECT_CREATE_DIST 1000.0
TWEAK_FLOAT YACHT_OBJECT_CREATE_BUFFER 100.0

#IF IS_DEBUG_BUILD

PROC ADD_WIDGET_FOR_YACHT_APPEARANCE(YACHT_APPEARANCE &Appearance)
	START_WIDGET_GROUP("Appearance")
		ADD_WIDGET_INT_SLIDER("iOption", Appearance.iOption, -1, 255, 1)	
		ADD_WIDGET_INT_SLIDER("iTint", Appearance.iTint, -1, 255, 1)	
		ADD_WIDGET_INT_SLIDER("iLighting", Appearance.iLighting, -1, 255, 1)
		ADD_WIDGET_INT_SLIDER("iRailing", Appearance.iRailing, -1, 255, 1)	
		ADD_WIDGET_INT_SLIDER("iFlag", Appearance.iFlag, -1, 255, 1)	
		ADD_WIDGET_INT_SLIDER("iNameAsHash", Appearance.iNameAsHash, LOWEST_INT, HIGHEST_INT, 1)
	STOP_WIDGET_GROUP()
ENDPROC

PROC ADD_WIDGET_FOR_YACHT_DATA(YACHT_DATA &YachtData)
	START_WIDGET_GROUP("YachtData")
		ADD_WIDGET_INT_SLIDER("iLocation", YachtData.iLocation, -1, NUMBER_OF_PRIVATE_YACHTS-1, 1)	
		ADD_WIDGET_INT_SLIDER("iNavMeshBlockingObject", YachtData.iNavMeshBlockingObject, -1, HIGHEST_INT, 1)	
		ADD_WIDGET_FOR_YACHT_APPEARANCE(YachtData.Appearance)
		ADD_WIDGET_BOOL("bObjectsCreated", YachtData.bObjectsCreated)
		ADD_WIDGET_BOOL("bHullObjectsCreated", YachtData.bHullObjectsCreated)
		ADD_WIDGET_BOOL("bColourSet", YachtData.bColourSet)
		ADD_WIDGET_BOOL("bUpdateObjects", YachtData.bUpdateObjects)
		ADD_WIDGET_BOOL("bCreateObjects", YachtData.bCreateObjects)	
	STOP_WIDGET_GROUP()
ENDPROC

PROC SETUP_PRIVATE_YACHT_WIDGETS(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	
	INT i, j
	//INT iArray
	TEXT_LABEL_63 str
	
	LocalData.wgYachtWidget = START_WIDGET_GROUP("Private Yachts")	
	
		START_WIDGET_GROUP("LocalData")
			REPEAT NUMBER_OF_PRIVATE_YACHTS i
				str = "iYachtSpawnState["
				str += i
				str += "]"
				ADD_WIDGET_INT_SLIDER(str, LocalData.iYachtSpawnState[i], -1, HIGHEST_INT, 1)
				
//				str = "iYachtDummyState["
//				str += i
//				str += "]"
//				ADD_WIDGET_INT_SLIDER(str, LocalData.iYachtDummyState[i], -1, HIGHEST_INT, 1)												
				
				str = "bDoneIPLRemovalCheck["
				str += i
				str += "]"
				ADD_WIDGET_BOOL(str, LocalData.bDoneIPLRemovalCheck[i])
				
				str = "YachtData["
				str += i
				str += "]"
				START_WIDGET_GROUP(str)
					ADD_WIDGET_FOR_YACHT_DATA(LocalData.YachtData[i])
				STOP_WIDGET_GROUP()
				
			ENDREPEAT
			
			REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES i
				str = "iVehicleSpawnState["
				str += i
				str += "]"
				ADD_WIDGET_INT_SLIDER(str, LocalData.iVehicleSpawnState[i], -1, HIGHEST_INT, 1)			
			ENDREPEAT						
			
			ADD_WIDGET_INT_SLIDER("iClientYachtStagger", LocalData.iClientYachtStagger, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iClientPlayerStagger", LocalData.iClientPlayerStagger, -1, HIGHEST_INT, 1)			
			ADD_WIDGET_INT_SLIDER("iServerYachtStagger", LocalData.iServerYachtStagger, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iDebugStagger", LocalData.iDebugStagger, -1, HIGHEST_INT, 1)
			ADD_BIT_FIELD_WIDGET("iBS_PlayersOnMyYacht", LocalData.iBS_PlayersOnMyYacht)
			ADD_BIT_FIELD_WIDGET("iBS_PlayersNearMyYacht", LocalData.iBS_PlayersNearMyYacht)
			ADD_WIDGET_FLOAT_SLIDER("Docking Force", LocalData.fForceAppliedWidget, -100, 100, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Docking Heading", LocalData.fForceAppliedHeadingWidget, -100, 100, 0.001)
					
			ADD_WIDGET_FLOAT_SLIDER("fYachtCamShake", fYachtCamShake, 0.0, 1.0, 0.01)
			
			ADD_WIDGET_BOOL("bWantedLevelIsSuppressed", LocalData.bWantedLevelIsSuppressed)
			ADD_WIDGET_INT_SLIDER("iStoredMaxWantedLevel", LocalData.iStoredMaxWantedLevel, -1, HIGHEST_INT, 1)
			
			ADD_WIDGET_INT_SLIDER("iYachtToFade", LocalData.iYachtToFade, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iYachtFadeState", LocalData.iYachtFadeState, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iYachtFadeOldOption", LocalData.iYachtFadeOldOption, -1, HIGHEST_INT, 1)
			
			ADD_WIDGET_INT_SLIDER("iLockStateForVehicles", LocalData.iLockStateForVehicles, -1, HIGHEST_INT, 1)
			
			ADD_WIDGET_BOOL("bResetVehicleColours", LocalData.bResetVehicleColours)
			ADD_WIDGET_BOOL("bCleanupAllStoredYachtVehicles", LocalData.bCleanupAllStoredYachtVehicles)
			
			ADD_WIDGET_INT_SLIDER("iTotalYachtObjectsCreated", g_iTotalYachtObjectsCreated, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iMaxYachtObjectsCreated", g_iMaxYachtObjectsCreated, -1, HIGHEST_INT, 1)
			
			ADD_WIDGET_INT_SLIDER("iWarpBehindYacht", LocalData.iWarpBehindYacht, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iWarpBehindYachtState", LocalData.iWarpBehindYachtState, -1, HIGHEST_INT, 1)
			ADD_WIDGET_VECTOR_SLIDER("vWarpBehindCoords", LocalData.vWarpBehindCoords, -99999.9, 99999.9, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("fWarpBehindHeading", LocalData.fWarpBehindHeading, -360.0, 360.0, 0.01)
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("GlobalplayerBD")
			REPEAT NUM_NETWORK_PLAYERS i
				str = "["
				str += i
				str += "]"
				
				START_WIDGET_GROUP(str)
				
					str = "PrivateYachtDetails.bOwnsPrivateYacht"
					ADD_WIDGET_BOOL(str, GlobalplayerBD[i].PrivateYachtDetails.bOwnsPrivateYacht)
										
					str = "PrivateYachtDetails.vDesiredCoords"
					ADD_WIDGET_VECTOR_SLIDER(str, GlobalplayerBD[i].PrivateYachtDetails.vDesiredCoords, -9999999.9, 9999999.9, 0.001)
					
					str = "PrivateYachtDetails.iWarpState"
					ADD_WIDGET_INT_SLIDER(str, GlobalplayerBD[i].PrivateYachtDetails.iWarpState, -1, HIGHEST_INT, 1)
					
					str = "PrivateYachtDetails.bWarpActive"
					ADD_WIDGET_BOOL(str, GlobalplayerBD[i].PrivateYachtDetails.bWarpActive)
					
					str = "PrivateYachtDetails.iYachtIAmOn"
					ADD_WIDGET_INT_SLIDER(str, GlobalplayerBD[i].PrivateYachtDetails.iYachtIAmOn, -2, HIGHEST_INT, 1)
					
					str = "PrivateYachtDetails.iYachtIAmOnIncludingVehicles"
					ADD_WIDGET_INT_SLIDER(str, GlobalplayerBD[i].PrivateYachtDetails.iYachtIAmOnIncludingVehicles, -2, HIGHEST_INT, 1)
					
					str = "PrivateYachtDetails.iYachtIAmNearest"
					ADD_WIDGET_INT_SLIDER(str, GlobalplayerBD[i].PrivateYachtDetails.iYachtIAmNearest, -2, HIGHEST_INT, 1)
					
					str = "PrivateYachtDetails.iYachtSpaceIAmIn"
					ADD_WIDGET_INT_SLIDER(str, GlobalplayerBD[i].PrivateYachtDetails.iYachtSpaceIAmIn, -2, HIGHEST_INT, 1)
					
					str = "PrivateYachtDetails.iYachtCamShakeStage"
					ADD_WIDGET_INT_SLIDER(str, GlobalplayerBD[i].PrivateYachtDetails.iYachtCamShakeStage, -1, HIGHEST_INT, 1)
					
					ADD_WIDGET_FOR_YACHT_APPEARANCE(GlobalplayerBD[i].PrivateYachtDetails.Appearance)									
					
					str = "PrivateYachtDetails.iPlayerHash"
					ADD_WIDGET_INT_SLIDER(str, GlobalplayerBD[i].PrivateYachtDetails.iPlayerHash, LOWEST_INT, HIGHEST_INT, 1)
					
					str = "PrivateYachtDetails.iAccess"
					ADD_WIDGET_INT_SLIDER(str, GlobalplayerBD[i].PrivateYachtDetails.iAccess, -1, 7, 1)
					
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Buoys")
			REPEAT NUMBER_OF_PRIVATE_YACHT_BUOY i
				str = "g_PrivateYachtBuoysCoords["
				str += i
				str += "]"
				ADD_WIDGET_VECTOR_SLIDER(str, g_PrivateYachtBuoysCoords[i], -100.0, 100.0, 0.01)
			ENDREPEAT
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("GlobalServerBD.")
		
			ADD_WIDGET_INT_SLIDER("iTotalExistingPrivateYachts", GlobalServerBD_BlockC.iTotalExistingPrivateYachts, 0, NUMBER_OF_PRIVATE_YACHTS-1, 1)
		
			START_WIDGET_GROUP("PYPlayerDetails")
				REPEAT NUM_NETWORK_PLAYERS i
					str = "["
					str += i
					str += "]"
						
					START_WIDGET_GROUP(str)
						str = "iPrivateYachtAssignedToPlayer"	
						ADD_WIDGET_INT_SLIDER(str, GlobalServerBD_BlockC.PYPlayerDetails[i].iPrivateYachtAssignedToPlayer, -1, NUMBER_OF_PRIVATE_YACHTS-1, 1)
						
						str = "iPrivateYachtReservedForPlayer"	
						ADD_WIDGET_INT_SLIDER(str, GlobalServerBD_BlockC.PYPlayerDetails[i].iPrivateYachtReservedForPlayer, -1, NUMBER_OF_PRIVATE_YACHTS-1, 1)
						
						str = "iYachtPlayerIsOn"	
						ADD_WIDGET_INT_SLIDER(str, GlobalServerBD_BlockC.PYPlayerDetails[i].iYachtPlayerIsOn, -1, NUMBER_OF_PRIVATE_YACHTS-1, 1)
		
					STOP_WIDGET_GROUP()
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("PYYachtDetails")
			
				
				REPEAT NUMBER_OF_PRIVATE_YACHTS i
					
					str = "["
					str += i
					str += "]"
					
					START_WIDGET_GROUP(str)
						
						str = "iPlayerAssignedToPrivateYacht"	
						ADD_WIDGET_INT_SLIDER(str, GlobalServerBD_BlockC.PYYachtDetails[i].iPlayerAssignedToPrivateYacht, -1, NUM_NETWORK_PLAYERS-1, 1)				

						str = "iPlayerPrivateYachtiIsReservedFor"	
						ADD_WIDGET_INT_SLIDER(str, GlobalServerBD_BlockC.PYYachtDetails[i].iPlayerPrivateYachtiIsReservedFor, -1, NUM_NETWORK_PLAYERS-1, 1)	
						
						str = "iLastPlayerAssignedToPrivateYacht"	
						ADD_WIDGET_INT_SLIDER(str, GlobalServerBD_BlockC.PYYachtDetails[i].iLastPlayerAssignedToPrivateYacht, -1, NUM_NETWORK_PLAYERS-1, 1)				

						str = "iBS_PlayersOnYacht"	
						ADD_WIDGET_INT_SLIDER(str, GlobalServerBD_BlockC.PYYachtDetails[i].iBS_PlayersOnYacht, -2, HIGHEST_INT, 1)				

						str = "bYachtIsActive"	
						ADD_WIDGET_BOOL(str, GlobalServerBD_BlockC.PYYachtDetails[i].bYachtIsActive)				

						str = "bYachtIsMarkedForCleanup"	
						ADD_WIDGET_BOOL(str, GlobalServerBD_BlockC.PYYachtDetails[i].bYachtIsMarkedForCleanup)		
						
						ADD_WIDGET_FOR_YACHT_APPEARANCE(GlobalServerBD_BlockC.PYYachtDetails[i].Appearance)											
						
						str = "bForceCleanup"
						ADD_WIDGET_BOOL(str, GlobalServerBD_BlockC.PYYachtDetails[i].bForceCleanup)
						
						str = "bYachtJacuzziUseSwimwear"
						ADD_WIDGET_BOOL(str, GlobalServerBD_BlockC.PYYachtDetails[i].bYachtJacuzziUseSwimwear)
						
						str = "iHashOfOwner"	
						ADD_WIDGET_INT_SLIDER(str, GlobalServerBD_BlockC.PYYachtDetails[i].iHashOfOwner, LOWEST_INT, HIGHEST_INT, 1)
						
						str = "iHashOfLastOwner"	
						ADD_WIDGET_INT_SLIDER(str, GlobalServerBD_BlockC.PYYachtDetails[i].iHashOfLastOwner, LOWEST_INT, HIGHEST_INT, 1)
						
					STOP_WIDGET_GROUP()
				ENDREPEAT			
				
			STOP_WIDGET_GROUP()
		
			
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("Fixed Global Positions")
			START_WIDGET_GROUP("g_PrivateYachtDetails")
			
				REPEAT NUMBER_OF_PRIVATE_YACHTS i
				
					str = "Yacht "
					str += i	
					START_WIDGET_GROUP(str)
						
						str = "["
						str += i
						str += "].vInteriorCoords"	
						ADD_WIDGET_VECTOR_SLIDER(str, g_PrivateYachtDetails[i].vInteriorCoords, -99999.9, 99999.9, 0.01)
						
						str = "["
						str += i
						str += "].fInteriorHeading"	
						ADD_WIDGET_FLOAT_SLIDER(str, g_PrivateYachtDetails[i].fInteriorHeading, -9999.9, 9999.0, 0.1)	
						
						str = "["
						str += i
						str += "].vCoords"	
						ADD_WIDGET_VECTOR_SLIDER(str, g_PrivateYachtDetails[i].vCoords, -99999.9, 99999.9, 0.01)
						
						str = "["
						str += i
						str += "].fHeading"	
						ADD_WIDGET_FLOAT_SLIDER(str, g_PrivateYachtDetails[i].fHeading, -9999.9, 9999.0, 0.1)
						
						str = "["
						str += i
						str += "].vModelCoords"	
						ADD_WIDGET_VECTOR_SLIDER(str, g_PrivateYachtDetails[i].vModelCoords, -99999.9, 99999.9, 0.01)
						
						str = "["
						str += i
						str += "].fModelHeading"	
						ADD_WIDGET_FLOAT_SLIDER(str, g_PrivateYachtDetails[i].fModelHeading, -9999.9, 9999.0, 0.1)
						
						str = "["
						str += i
						str += "].vBlipCoords"	
						ADD_WIDGET_VECTOR_SLIDER(str, g_PrivateYachtDetails[i].vBlipCoords, -99999.9, 99999.9, 0.01)
											
						START_WIDGET_GROUP("ShoreSpawn")
							REPEAT 3 j
								str = "["
								str += j
								str += "].vCoords"
								ADD_WIDGET_VECTOR_SLIDER(str, g_PrivateYachtDetails[i].ShoreSpawn[j].vCoords, -99999.9, 99999.9, 0.01)	
								str = "["
								str += j
								str += "].fHeading"
								ADD_WIDGET_FLOAT_SLIDER(str, g_PrivateYachtDetails[i].ShoreSpawn[j].fHeading, -99999.9, 99999.9, 0.01)
							ENDREPEAT						
						STOP_WIDGET_GROUP()
						
					STOP_WIDGET_GROUP()
					
				ENDREPEAT
				

				
				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("g_PrivateYachtSpawnLocationOffest.")
				REPEAT NUMBER_OF_SPAWN_LOCATIONS_ON_YACHT_EXTERIOR i
					str = "["
					str += i
					str += "].vPlayerLoc"	
					ADD_WIDGET_VECTOR_SLIDER(str, g_PrivateYachtSpawnLocationOffest[i].vPlayerLoc, -99999.9, 99999.9, 0.01)
					
					str = "["
					str += i
					str += "].fPlayerHeading"	
					ADD_WIDGET_FLOAT_SLIDER(str, g_PrivateYachtSpawnLocationOffest[i].fPlayerHeading, -9999.9, 9999.0, 0.1)
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("g_PrivateYachtSpawnLocationApartmentOffest.")
				REPEAT NUMBER_OF_SPAWN_LOCATIONS_YACHT_APARTMENT i
					str = "["
					str += i
					str += "].vPlayerLoc"	
					ADD_WIDGET_VECTOR_SLIDER(str, g_PrivateYachtSpawnLocationApartmentOffest[i].vPlayerLoc, -99999.9, 99999.9, 0.01)
					
					str = "["
					str += i
					str += "].fPlayerHeading"	
					ADD_WIDGET_FLOAT_SLIDER(str, g_PrivateYachtSpawnLocationApartmentOffest[i].fPlayerHeading, -9999.9, 9999.0, 0.1)
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("gPrivateYachtSpawnVehicleDetails.")
				REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES i
					str = "["
					str += i
					str += "].vOffsetCoords"	
					ADD_WIDGET_VECTOR_SLIDER(str, gPrivateYachtSpawnVehicleDetails[i].vOffsetCoords, -99999.9, 99999.9, 0.01)
					
					str = "["
					str += i
					str += "].fOffsetHeading"	
					ADD_WIDGET_FLOAT_SLIDER(str, gPrivateYachtSpawnVehicleDetails[i].fOffsetHeading, -9999.9, 9999.0, 0.1)
					
					str = "["
					str += i
					str += "].fClearRadius"	
					ADD_WIDGET_FLOAT_SLIDER(str, gPrivateYachtSpawnVehicleDetails[i].fClearRadius, -9999.9, 9999.0, 0.1)
					
					str = "["
					str += i
					str += "].iFlags"	
					ADD_WIDGET_INT_SLIDER(str, gPrivateYachtSpawnVehicleDetails[i].iFlags, 0, HIGHEST_INT, 1)										
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("g_PrivateYachtZoneCoords")
				REPEAT iMAX_PRIVATE_YACHT_SPAWN_ZONES i 
					str = "g_PrivateYachtZoneCoords["
					str += i
					str += "]"	
					ADD_WIDGET_VECTOR_SLIDER(str, g_PrivateYachtZoneCoords[i], -99999.9, 99999.9, 0.01)
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Move Camera")
			ADD_WIDGET_BOOL("bEnableTool", LocalData.rag_net_yacht_scene.bEnableTool)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("yacht warping")
			ADD_WIDGET_INT_SLIDER("g_SpawnData.iYachtToWarpTo", g_SpawnData.iYachtToWarpTo, -1, NUMBER_OF_PRIVATE_YACHTS-1, 1)
			ADD_WIDGET_INT_SLIDER("g_SpawnData.iYachtToWarpFrom", g_SpawnData.iYachtToWarpFrom, -1, NUMBER_OF_PRIVATE_YACHTS-1, 1)
			ADD_WIDGET_INT_SLIDER("g_SpawnData.iYachtZoneToWarpTo", g_SpawnData.iYachtZoneToWarpTo, -1, 100, 1)	
			ADD_WIDGET_FLOAT_SLIDER("g_SpawnData.fNearestYachtDist", g_SpawnData.fNearestYachtDist, -1.0, 9999999999.9, 0.01)
			REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES i
				str = "bYachtVehicleSpawnBlocked["
				str += i
				str += "]"
				ADD_WIDGET_BOOL(str, g_SpawnData.bYachtVehicleSpawnBlocked[i])
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("g_bEditSynchScene")
			ADD_WIDGET_BOOL("g_bEditSynchSceneOffsets", g_bEditSynchSceneOffsets)
			ADD_WIDGET_FLOAT_SLIDER("g_vSynchScenePhase", g_vSynchScenePhase, 0, 1.0, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("g_vSynchSceneCoordOffset", g_vSynchSceneCoordOffset, -150, 150, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("g_vSynchSceneRotOffset", g_vSynchSceneRotOffset, -150, 150, 0.1)
		
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("MPGlobalsPrivateYacht")
			ADD_WIDGET_BOOL("bBlipAllPrivateYachts", MPGlobalsPrivateYacht.bBlipAllPrivateYachts)
			REPEAT NUMBER_OF_PRIVATE_YACHTS i
				str = "bPrivateYachtIsActive["
				str += i
				str += "]"
				ADD_WIDGET_BOOL(str, MPGlobalsPrivateYacht.bPrivateYachtIsActive[i])
			ENDREPEAT
			REPEAT NUMBER_OF_PRIVATE_YACHTS i
				str = "bBlipDisabledForYacht["
				str += i
				str += "]"
				ADD_WIDGET_BOOL(str, MPGlobalsPrivateYacht.bBlipDisabledForYacht[i])
			ENDREPEAT
			ADD_WIDGET_BOOL("bRefreshShoreSound", MPGlobalsPrivateYacht.bRefreshShoreSound)
			
			ADD_WIDGET_BOOL("bPlayerDoingYachtFade", MPGlobalsPrivateYacht.bPlayerDoingYachtFade)
			ADD_WIDGET_BOOL("bFlushYachtObjects", MPGlobalsPrivateYacht.bFlushYachtObjects)
			//ADD_WIDGET_BOOL("bRemoveAllPrivateYachtIPLObjectsNow", MPGlobalsPrivateYacht.bRemoveAllPrivateYachtIPLObjectsNow)		
			
			REPEAT NUMBER_OF_PRIVATE_YACHTS i
				str = "bPrivateYachtRequested["
				str += i
				str += "]"
				ADD_WIDGET_BOOL(str, MPGlobalsPrivateYacht.bPrivateYachtRequested[i])
			ENDREPEAT
			ADD_WIDGET_INT_SLIDER("iYachtRequests", MPGlobalsPrivateYacht.iYachtRequests, -1, 100, 1)	
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Yacht Air Defence")
			ADD_WIDGET_BOOL("turn on warning shots", g_bTurnOnWarningShots)	
			ADD_WIDGET_INT_SLIDER("sphere", g_b_WarningShots_SphereIndex, 0, 20, 1)	
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Debug")		
			
			START_WIDGET_GROUP("Mission Creator")				
				ADD_WIDGET_FOR_YACHT_DATA(g_MCYachtData)				
				ADD_WIDGET_INT_SLIDER("g_iMCYachtState", g_iMCYachtState, 0, 99, 1)
				ADD_WIDGET_BOOL("create", g_bMCYachtCreate)
				ADD_WIDGET_BOOL("update", g_bMCYachtUpdate)
				ADD_WIDGET_BOOL("delete", g_bMCYachtDelete)
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_INT_SLIDER("g_iReassignZone", g_iReassignZone, -1, 100, 1)
			ADD_WIDGET_BOOL("g_bDoWarpToReAssign", g_bDoWarpToReAssign)			
			
			START_WIDGET_GROUP("BUY YACHT")	
				ADD_WIDGET_FOR_YACHT_APPEARANCE(g_BuyYachtAppearance)
				ADD_WIDGET_BOOL("g_BuyPrivateYacht", g_BuyPrivateYacht)
				ADD_WIDGET_BOOL("g_BuyPrivateTruck", g_BuyPrivateTruck)
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_FLOAT_SLIDER("g_sMPTunables.fdistanceyachtcreation ", g_sMPTunables.fdistanceyachtcreation, 0.0, 10000.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("YACHT_OBJECT_CREATE_BUFFER", YACHT_OBJECT_CREATE_BUFFER, 0.0, 10000.0, 1.0)
			
			ADD_WIDGET_BOOL("g_RemovePrivateYacht", g_RemovePrivateYacht)
			ADD_WIDGET_BOOL("g_RemovePrivateTruck", g_RemovePrivateTruck)
			ADD_WIDGET_BOOL("g_bWarpToMyYacht", g_bWarpToMyYacht)
			ADD_WIDGET_BOOL("g_bActivateAllPrivateYachts", g_bActivateAllPrivateYachts)
			ADD_WIDGET_BOOL("g_bMakeDefaultYachtFullyPimped", g_bMakeDefaultYachtFullyPimped)	
			ADD_WIDGET_BOOL("g_SetYachtAsRespawnLocation", g_SetYachtAsRespawnLocation)
			ADD_WIDGET_BOOL("g_DrawYachtBoundingBoxes", g_DrawYachtBoundingBoxes)
			ADD_WIDGET_BOOL("g_DrawYachtTopToFlag", g_DrawYachtTopToFlag)
			ADD_WIDGET_BOOL("g_GetPlayerCoordsAsYachtOffset", g_GetPlayerCoordsAsYachtOffset)
			ADD_WIDGET_BOOL("g_DrawYachtZones", g_DrawYachtZones)
			ADD_WIDGET_BOOL("g_bOutputYachtDetails", g_bOutputYachtDetails)
			
			ADD_WIDGET_INT_SLIDER("g_iPrioritiseThisYacht", g_iPrioritiseThisYacht, -1, NUMBER_OF_PRIVATE_YACHTS-1, 1)
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_BLIP_SCALE", PRIVATE_YACHT_BLIP_SCALE, 0.0, 100.0, 0.01)
			ADD_WIDGET_INT_SLIDER("PRIVATE_YACHT_BLIP_ALPHA", PRIVATE_YACHT_BLIP_ALPHA, 0, 255, 1)
			
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_LENGTH", PRIVATE_YACHT_LENGTH, 0.0, 999.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_WIDTH", PRIVATE_YACHT_WIDTH, 0.0, 999.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_HEIGHT", PRIVATE_YACHT_HEIGHT, 0.0, 999.0, 0.01)
		
//			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_TOP_OFFSET_X", PRIVATE_YACHT_TOP_OFFSET_X, -999.9, 999.0, 0.01)
//			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_TOP_OFFSET_Y", PRIVATE_YACHT_TOP_OFFSET_Y, -999.9, 999.0, 0.01)
//			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_TOP_OFFSET_Z", PRIVATE_YACHT_TOP_OFFSET_Z, -999.9, 999.0, 0.01)
//			
//			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_FLAG_OFFSET_X", PRIVATE_YACHT_FLAG_OFFSET_X, -999.9, 999.0, 0.01)
//			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_FLAG_OFFSET_Y", PRIVATE_YACHT_FLAG_OFFSET_Y, -999.9, 999.0, 0.01)
//			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_FLAG_OFFSET_Z", PRIVATE_YACHT_FLAG_OFFSET_Z, -999.9, 999.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_MAIN_HEADING_OFFSET", PRIVATE_YACHT_MAIN_HEADING_OFFSET, 0.0, 360.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_MAIN_COORDS_OFFSET_X", PRIVATE_YACHT_MAIN_COORDS_OFFSET_X, -100.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_MAIN_COORDS_OFFSET_Y", PRIVATE_YACHT_MAIN_COORDS_OFFSET_Y, -100.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_MAIN_COORDS_OFFSET_Z", PRIVATE_YACHT_MAIN_COORDS_OFFSET_Z, -100.0, 100.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_HEADING_OFFSET_FOR_MODEL", PRIVATE_YACHT_MODLE_HEADING_OFFSET, -360.0, 360.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("PRIVATE_YACHT_SEAT_WIDTH", PRIVATE_YACHT_SEAT_WIDTH, 0, 3, 0.01)
						
			ADD_WIDGET_BOOL("g_bUseDebugOffsetsForYacht", g_bUseDebugOffsetsForYacht)
			ADD_WIDGET_BOOL("g_bDrawExteriorYachtSpawnLocations", g_bDrawExteriorYachtSpawnLocations)
			ADD_WIDGET_BOOL("g_bDrawVehicleSpawnLocations", g_bDrawVehicleSpawnLocations)
			
			ADD_WIDGET_BOOL("g_bSpawnInStandardSupervolito", g_bSpawnInStandardSupervolito)

			START_WIDGET_GROUP("Yachts")								
				REPEAT NUMBER_OF_PRIVATE_YACHTS i
					
					str = "Yacht "
					str += i	
					START_WIDGET_GROUP(str)
					
						str = "bActivateThisYacht["
						str += i	
						str += "]"
						ADD_WIDGET_BOOL(str, LocalData.bActivateThisYacht[i])
					
						str = "bWarpToYachtCoords["
						str += i
						str += "]"
						ADD_WIDGET_BOOL(str, LocalData.bWarpToYachtCoords[i])
						
						str = "bBlipIsActive["
						str += i
						str += "]"
						ADD_WIDGET_BOOL(str, LocalData.bBlipIsActive[i])		
						
						str = "bShouldYachtBeActive["
						str += i	
						str += "]"
						ADD_WIDGET_BOOL(str, LocalData.bShouldYachtBeActive[i])
						
					STOP_WIDGET_GROUP()
				ENDREPEAT	
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("SET_MY_PRIVATE_YACHT_VEHICLE_SPAWN_ON_LAND")
				ADD_WIDGET_INT_SLIDER("iVehicle", g_iSpawnOnLandVehicle, 0, NUMBER_OF_PRIVATE_YACHT_VEHICLES-1, 1)
				ADD_WIDGET_BOOL("bSet", g_bSpawnOnLandSet)
				ADD_WIDGET_BOOL("Call now", g_bSpawnOnLandCall)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()		
	
	STOP_WIDGET_GROUP()
	
ENDPROC

#ENDIF

FUNC BOOL SHOULD_YACHT_OBJECTS_BE_DISABLED()
	IF IS_PLAYER_IN_OFFICE_PROPERTY(PLAYER_ID())
	AND NOT IS_PLAYER_SPECTATING(PLAYER_ID())
	AND NOT FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_PRIVATE_YACHT_ACTIVE(INT iYachtID, BOOL bSet)
	CPRINTLN(DEBUG_YACHT, "SET_PRIVATE_YACHT_ACTIVE - iYachtID ", iYachtID, " to ", bSet)
	
	MPGlobalsPrivateYacht.bPrivateYachtIsActive[iYachtID] = bSet	
ENDPROC

FUNC BOOL SHOULD_CHECK_FOR_EMPTY_YACHT_SPACE()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	AND NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
	AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_EMPTY_YACHT_SPACE(INT iYachtID)
	IF SHOULD_CHECK_FOR_EMPTY_YACHT_SPACE()
		IF (g_SpawnData.bInEmptyYachtSpace)
			IF IS_PLAYER_ON_YACHT(PLAYER_ID(), iYachtID)
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC START_WARP_FROM_EMPTY_SPACE(INT iYachtID, PRIVATE_YACHT_LOCAL_DATA &LocalData)
	CPRINTLN(DEBUG_YACHT, "START_WARP_FROM_EMPTY_SPACE - called with yacht ", iYachtID)
	LocalData.iWarpBehindYacht = iYachtID
	LocalData.iWarpBehindYachtState = 0
ENDPROC

PROC CLEANUP_WARP_FROM_EMPTY_YACHT_SPACE(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	CPRINTLN(DEBUG_YACHT, "CLEANUP_WARP_FROM_EMPTY_YACHT_SPACE - called ")
	LocalData.iWarpBehindYacht = -1	
	LocalData.iWarpBehindYachtState = -1
ENDPROC

PROC UPDATE_WARP_FROM_EMPTY_YACHT_SPACE(PRIVATE_YACHT_LOCAL_DATA &LocalData)

	IF NOT (LocalData.iWarpBehindYacht = -1)
		
		IF SHOULD_CHECK_FOR_EMPTY_YACHT_SPACE()
	
			// grab coords
			IF (LocalData.iWarpBehindYachtState = 0)
			
				LocalData.vWarpBehindCoords = GET_COORDS_AS_OFFSET_FROM_YACHT_GIVEN_WORLD_COORDS(LocalData.iWarpBehindYacht, GET_ENTITY_COORDS(PLAYER_PED_ID()))
				LocalData.fWarpBehindHeading = GET_HEADING_AS_OFFSET_FROM_YACHT_GIVEN_WORLD_HEADING(LocalData.iWarpBehindYacht, GET_ENTITY_HEADING(PLAYER_PED_ID()))				
			
				// get offset 100m in front or behind yacht
				IF (LocalData.vWarpBehindCoords.y > 0.0)
					LocalData.vWarpBehindCoords.y += 100.0
				ELSE
					LocalData.vWarpBehindCoords.y += -100.0
				ENDIF
				
				LocalData.vWarpBehindCoords = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(LocalData.iWarpBehindYacht, LocalData.vWarpBehindCoords)
				LocalData.fWarpBehindHeading = GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(LocalData.iWarpBehindYacht, LocalData.fWarpBehindHeading)
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_USE_PLAYER_FADE_OUT)
			
				IF NOT IS_BROWSER_OPEN()
					DO_SCREEN_FADE_OUT(500)
				ENDIF
				
				CPRINTLN(DEBUG_YACHT, "UPDATE_WARP_FROM_EMPTY_YACHT_SPACE - got new coords ", LocalData.vWarpBehindCoords)
			
				LocalData.iWarpBehindYachtState++
			ENDIF
			
			// wait for fade out
			IF (LocalData.iWarpBehindYachtState = 1)
				IF IS_SCREEN_FADED_OUT()
				OR IS_BROWSER_OPEN()
					LocalData.iWarpBehindYachtState++					
					CPRINTLN(DEBUG_YACHT, "UPDATE_WARP_FROM_EMPTY_YACHT_SPACE - done fade out.")
				ELSE
					IF NOT IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_OUT(500)
					ENDIF
					CPRINTLN(DEBUG_YACHT, "UPDATE_WARP_FROM_EMPTY_YACHT_SPACE - fading out")
				ENDIF
			ENDIF
			
			// do warp
			IF (LocalData.iWarpBehindYachtState = 2)
				CPRINTLN(DEBUG_YACHT, "UPDATE_WARP_FROM_EMPTY_YACHT_SPACE - doing warp ")
				IF NET_WARP_TO_COORD(LocalData.vWarpBehindCoords, LocalData.fWarpBehindHeading, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)					
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF	
					CLEANUP_WARP_FROM_EMPTY_YACHT_SPACE(LocalData)
				ENDIF
			ENDIF
			
		ELSE
			CLEANUP_WARP_FROM_EMPTY_YACHT_SPACE(LocalData)
		ENDIF
		
	ENDIF

ENDPROC

FUNC BOOL IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(PRIVATE_YACHT_LOCAL_DATA &LocalData, INT iHelpFlag)
	IF IS_BIT_SET(LocalData.HelpTextBitset, iHelpFlag)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC SET_PRIVATE_YACHT_HELP_TEXT_FLAG(PRIVATE_YACHT_LOCAL_DATA &LocalData, INT iHelpFlag, BOOL bSet)	
	CPRINTLN(DEBUG_YACHT, "SET_PRIVATE_YACHT_HELP_TEXT_FLAG - called with ", iHelpFlag, ", ", bSet)
	DEBUG_PRINTCALLSTACK()	
	IF (bSet)
		IF NOT IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, iHelpFlag)
			SET_BIT(LocalData.HelpTextBitset, iHelpFlag)
		ELSE
			CPRINTLN(DEBUG_YACHT, "SET_PRIVATE_YACHT_HELP_TEXT_FLAG - flag is already set!")
		ENDIF
	ELSE
		IF IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, iHelpFlag)
			CLEAR_BIT(LocalData.HelpTextBitset, iHelpFlag)
		ELSE
			CPRINTLN(DEBUG_YACHT, "SET_PRIVATE_YACHT_HELP_TEXT_FLAG - flag is already unset!")
		ENDIF	
	ENDIF
ENDPROC



PROC UPDATE_PRIVATE_YACHT_VEHICLE_LOCKED_STATE(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	INT iLockStateOverride = -1
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		iLockStateOverride = 0
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		LocalData.iLockStateForVehicles = 0
	ELSE
		// no need to spam the get_stat every frame
		IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), LocalData.LockStateTimer)) > 1000	
			INT iLockState = GET_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_VEHICLE_ACCESS)
			IF NOT (iLockState = LocalData.iLockStateForVehicles)
				LocalData.iLockStateForVehicles = iLockState
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_LOCKED_STATE - iLockStateForVehicles changed to ", LocalData.iLockStateForVehicles)
			ENDIF
			LocalData.LockStateTimer = GET_NETWORK_TIME()
		ENDIF
	ENDIF
	
	INT iVehicle
	
	// should we do a refresh?
	IF (MPGlobalsAmbience.bRefreshYachtVehicleLockStates)
		REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES iVehicle
			LocalData.iStoredAllowIntoYV[iVehicle] = -1
		ENDREPEAT
		MPGlobalsAmbience.bRefreshYachtVehicleLockStates = FALSE
	ENDIF
	
//	NETWORK_INDEX aVehNet
	VEHICLE_INDEX aVeh
	REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES iVehicle
		IF LocalData.iStoredAllowIntoYV[iVehicle] != LocalData.iLockStateForVehicles
//			aVehNet = GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.netVehicleID[iVehicle]
//			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(aVehNet)
//				aVeh = NET_TO_VEH(aVehNet)

				aVeh = LocalData.StoredVehicleID[iVehicle]
				IF DOES_ENTITY_EXIST(aVeh)
					IF IS_VEHICLE_DRIVEABLE(aVeh)
						IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
						AND NOT IS_PLAYER_SCTV(PLAYER_ID())
						AND NOT IS_A_SPECTATOR_CAM_RUNNING()
							IF NETWORK_GET_ENTITY_IS_NETWORKED(aVeh)
								
								IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(aVeh)
									NETWORK_REQUEST_CONTROL_OF_ENTITY(aVeh)									
									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_LOCKED_STATE - requesting control of vehicle ", iVehicle)
								ENDIF
								
								IF NETWORK_HAS_CONTROL_OF_ENTITY(aVeh)
									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_LOCKED_STATE - updating lock state for vehicle ", iVehicle)
									SET_PERSONAL_VEHICLE_LOCK_STATE(aVeh, MP_STAT_YACHT_VEHICLE_ACCESS, iLockStateOverride)	
									LocalData.iStoredAllowIntoYV[iVehicle] = LocalData.iLockStateForVehicles
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
//			ENDIF
		ENDIF
	ENDREPEAT


ENDPROC

FUNC BOOL IS_VEHICLE_AN_OLD_YACHT_VEHICLE(VEHICLE_INDEX VehicleID)
	IF DOES_ENTITY_EXIST(VehicleID)
		INT iHashOwner = -1
		INT iVehicle = -1
		INT iYachtID = -1
		IF DECOR_IS_REGISTERED_AS_TYPE("PYV_Owner", DECOR_TYPE_INT)
			IF DECOR_EXIST_ON(VehicleID, "PYV_Owner")
				iHashOwner = DECOR_GET_INT(VehicleID, "PYV_Owner")
			ENDIF
		ENDIF
		IF DECOR_IS_REGISTERED_AS_TYPE("PYV_Vehicle", DECOR_TYPE_INT)
			IF DECOR_EXIST_ON(VehicleID, "PYV_Vehicle")
				iVehicle = DECOR_GET_INT(VehicleID, "PYV_Vehicle")
			ENDIF
		ENDIF
		IF DECOR_IS_REGISTERED_AS_TYPE("PYV_Yacht", DECOR_TYPE_INT)
			IF DECOR_EXIST_ON(VehicleID, "PYV_Yacht")
				iYachtID = DECOR_GET_INT(VehicleID, "PYV_Yacht")
			ENDIF
		ENDIF
		IF NOT (iHashOwner = -1)
		AND NOT (iVehicle = -1)
		AND NOT (iYachtID = -1)
			PLAYER_INDEX OwnerID
			OwnerID = GET_PLAYER_INDEX_FROM_HASH(iHashOwner, TRUE)
			IF NOT (OwnerID = INVALID_PLAYER_INDEX())
			AND NETWORK_IS_PLAYER_CONNECTED(OwnerID)
				IF NOT IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(OwnerID)
					// does another network vehicle already exist for this?
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].PrivateYachtDetails.netVehicleID[iVehicle])
					AND NOT (VehicleID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].PrivateYachtDetails.netVehicleID[iVehicle]))
						CPRINTLN(DEBUG_YACHT, "IS_VEHICLE_AN_OLD_YACHT_VEHICLE - returning true, entity ids mismatch. iHashOwner ", iHashOwner, ", OwnerID ", NATIVE_TO_INT(OwnerID), ", iVehicle ", iVehicle, " iYachtID ", iYachtID)
						RETURN(TRUE)	
					ENDIF	
					// has owner been assigned a different yacht location?
					IF (GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(OwnerID) != iYachtID)
						CPRINTLN(DEBUG_YACHT, "IS_VEHICLE_AN_OLD_YACHT_VEHICLE - returning true, yacht ids mismatch. iHashOwner ", iHashOwner, ", OwnerID ", NATIVE_TO_INT(OwnerID), ", iVehicle ", iVehicle, " iYachtID ", iYachtID)
						RETURN(TRUE)
					ENDIF					
				ELSE
					CPRINTLN(DEBUG_YACHT, "IS_VEHICLE_AN_OLD_YACHT_VEHICLE - owner is currently doing a yacht warp, ignore. iHashOwner ", iHashOwner, ", OwnerID ", NATIVE_TO_INT(OwnerID), ", iVehicle ", iVehicle, " iYachtID ", iYachtID)				
				ENDIF
			ELSE
				IF NETWORK_IS_ACTIVITY_SESSION()
				AND FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
					RETURN(FALSE)
				ENDIF
				
				CPRINTLN(DEBUG_YACHT, "IS_VEHICLE_AN_OLD_YACHT_VEHICLE - returning true, player no longer active. iHashOwner ", iHashOwner, ", OwnerID ", NATIVE_TO_INT(OwnerID), ", iVehicle ", iVehicle, " iYachtID ", iYachtID)
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC SET_YACHT_VEHICLE_AS_UNUSABLE(VEHICLE_INDEX VehicleID, BOOL bDelete)
	
	DEBUG_PRINTCALLSTACK()

	BOOL bSetAsMissionEntity
	IF CAN_EDIT_THIS_ENTITY(VehicleID, bSetAsMissionEntity)
		
		// remove yacht vehicle decorators. and set a personal vehicle decorator of -1, this will kick anyone out.
		IF DECOR_IS_REGISTERED_AS_TYPE("PYV_Owner", DECOR_TYPE_INT)
			DECOR_SET_INT(VehicleID, "PYV_Owner", -1)	
		ENDIF
		IF DECOR_IS_REGISTERED_AS_TYPE("PYV_Vehicle", DECOR_TYPE_INT)
			DECOR_SET_INT(VehicleID, "PYV_Vehicle", -1)	
		ENDIF
		IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
			DECOR_SET_INT(VehicleID, "Player_Vehicle", -1)
		ENDIF
		
		// lock doors 
		IF NOT IS_ENTITY_DEAD(VehicleID)
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(VehicleID, TRUE)
			LOCK_DOORS_WHEN_NO_LONGER_NEEDED(VehicleID)
			FREEZE_ENTITY_POSITION(VehicleID, FALSE)
		ENDIF
		
		IF (bDelete)
		AND IS_VEHICLE_EMPTY(VehicleID)
			DELETE_VEHICLE(VehicleID)
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
		ENDIF	
		
	ELSE
		IF (bSetAsMissionEntity)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_OLD_YACHT_VEHICLES()
	VEHICLE_INDEX tempVeh
	VEHICLE_INDEX nearbyVehicles[1]
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
	ELSE
		GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehicles)
		tempVeh = nearbyVehicles[0]
	ENDIF
	IF IS_VEHICLE_AN_OLD_YACHT_VEHICLE(tempVeh)
		

		IF NOT IS_ENTITY_DEAD(tempVeh)
		
			// dont kick people out if in the sky
			IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(tempVeh))
			OR IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(tempVeh))
				IF NOT IS_VEHICLE_ON_ALL_WHEELS(tempVeh)
					CPRINTLN(DEBUG_YACHT, "CLEANUP_OLD_YACHT_VEHICLES - vehicle is in air so not kicking player out.")
					EXIT
				ENDIF
			ENDIF
		
			// dont kick people out if in the water
			IF IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(tempVeh))
				IF IS_ENTITY_IN_WATER(tempVeh)
					CPRINTLN(DEBUG_YACHT, "CLEANUP_OLD_YACHT_VEHICLES - vehicle is in water so not kicking player out..")
					EXIT
				ENDIF
			ENDIF
		ENDIF
				
		SET_YACHT_VEHICLE_AS_UNUSABLE(tempVeh, FALSE)
	ENDIF
ENDPROC


FUNC FLOAT GetModelHeightOffset(MODEL_NAMES ModelName)
	VECTOR vMin, vMax
	SAFE_GET_MODEL_DIMENSIONS(ModelName, vMin, vMax)
	RETURN (vMax.z - vMin.z) * 0.5
ENDFUNC

#IF IS_DEBUG_BUILD
PROC INCREMENT_YACHT_OBJECTS(INT iIncrementStep = 1)
	g_iTotalYachtObjectsCreated += iIncrementStep
	IF (g_iTotalYachtObjectsCreated > g_iMaxYachtObjectsCreated)
		g_iMaxYachtObjectsCreated = g_iTotalYachtObjectsCreated
		CPRINTLN(DEBUG_YACHT, "INCREMENT_YACHT_OBJECTS - new max of ", g_iMaxYachtObjectsCreated)
	ENDIF
	CPRINTLN(DEBUG_YACHT, "INCREMENT_YACHT_OBJECTS - current total is ", g_iTotalYachtObjectsCreated )
ENDPROC
PROC DECREMENT_YACHT_OBJECTS(INT iDecrementStep = -1)
	g_iTotalYachtObjectsCreated += iDecrementStep
	CPRINTLN(DEBUG_YACHT, "DECREMENT_YACHT_OBJECTS - current total is ", g_iTotalYachtObjectsCreated )
ENDPROC
#ENDIF

PROC CREATE_BUOYS(INT iYachtID, PRIVATE_YACHT_LOCAL_DATA &LocalData)
	
	IF NOT LocalData.bAreBuoysCreated
		IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
			INT I
			MODEL_NAMES aModel 
			BOOL haveAllmodelsLoaded = TRUE
			FOR I = 0 TO NUMBER_OF_PRIVATE_YACHT_BUOY-1
				aModel = GET_BUOY_MODEL(iYachtID, I)
				REQUEST_MODEL(aModel)
				IF NOT HAS_MODEL_LOADED(aModel)
					haveAllmodelsLoaded = FALSE
					CPRINTLN(DEBUG_YACHT, "[YACHTBUOY] CREATE_BUOYS - all loaded ")
				ENDIF
			ENDFOR
			
			IF haveAllmodelsLoaded
			
				CPRINTLN(DEBUG_YACHT, "[YACHTBUOY] CREATE_BUOYS - create buoys ")
				
				Vector position
				FOR I = 0 TO NUMBER_OF_PRIVATE_YACHT_BUOY-1
					

					IF NOT IS_PRIVATE_YACHT_BUOY_FLAG_SET(iYachtID, I, PYVF_DONT_SPAWN)
						IF NOT DOES_ENTITY_EXIST(LocalData.oBouy[I])		
							aModel = GET_BUOY_MODEL(iYachtID, I)
							position = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, GET_BUOY_POSITION_OFFSET_TO_YACHTS(I))
							GET_WATER_HEIGHT(position, position.z)
							LocalData.oBouy[I] = CREATE_OBJECT(aModel,  position, FALSE, FALSE)
							#IF IS_DEBUG_BUILD
								INCREMENT_YACHT_OBJECTS()
							#ENDIF
							CPRINTLN(DEBUG_YACHT, "[YACHTBUOY] CREATE_BUOYS - CREATE_OBJECT I ", I, " position = ", position )
						ENDIF
					ENDIF
				ENDFOR
				FOR I = 0 TO NUMBER_OF_PRIVATE_YACHT_BUOY-1
					aModel = GET_BUOY_MODEL(iYachtID, I)
					SET_MODEL_AS_NO_LONGER_NEEDED(aModel)
				ENDFOR
				LocalData.bYachtWithBuoys = iYachtID
				LocalData.bAreBuoysCreated = TRUE
				CPRINTLN(DEBUG_YACHT, "[YACHTBUOY] CREATE_BUOYS - done for iYachtID ", iYachtID, " ")
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC REMOVE_BUOYS(INT iYachtID, PRIVATE_YACHT_LOCAL_DATA &LocalData)

	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		IF LocalData.bAreBuoysCreated
		AND LocalData.bYachtWithBuoys  = iYachtID
			INT I
			FOR I = 0 TO NUMBER_OF_PRIVATE_YACHT_BUOY-1
				IF DOES_ENTITY_EXIST(LocalData.oBouy[I])		
					DELETE_OBJECT(LocalData.oBouy[I])
					#IF IS_DEBUG_BUILD
						DECREMENT_YACHT_OBJECTS()
					#ENDIF
				ENDIF
			ENDFOR
			CPRINTLN(DEBUG_YACHT, "[YACHTBUOY] REMOVE_BUOYS - called for iYachtID ", iYachtID, " ")

			LocalData.bYachtWithBuoys = 0
			LocalData.bAreBuoysCreated = FALSE
		ENDIF
	ENDIF

ENDPROC

PROC SETUP_SUPERVOLITO_EXTRAS(VEHICLE_INDEX& aVeh)
	
	BOOL SpawnCarbonSupervolito = TRUE
	#IF IS_DEBUG_BUILD
	IF g_bSpawnInStandardSupervolito
		SpawnCarbonSupervolito = FALSE
		CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] SETUP_SUPERVOLITO_EXTRAS -g_bSpawnInStandardSupervolito = TRUE ")
	ENDIF
	#ENDIF

	
	SET_VEHICLE_EXTRA(aVeh, 3, TRUE) 
	SET_VEHICLE_EXTRA(aVeh, 4, TRUE) 
	SET_VEHICLE_EXTRA(aVeh, 5, TRUE) 
	SET_VEHICLE_EXTRA(aVeh, 6, TRUE)
	IF SpawnCarbonSupervolito
		//CARBON VERSION
		
	
	 
	 	//CARBON ON
		SET_VEHICLE_EXTRA(aVeh, 7, FALSE) 
		SET_VEHICLE_EXTRA(aVeh, 8, FALSE) 
		SET_VEHICLE_EXTRA(aVeh, 9, FALSE)
		
		SET_VEHICLE_EXTRA(aVeh, 10, TRUE)
		SET_VEHICLE_EXTRA(aVeh, 11, TRUE)
		SET_VEHICLE_EXTRA(aVeh, 12, TRUE)
		CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] SETUP_SUPERVOLITO_EXTRAS -SUPERVOLITO Carbon = TRUE ")

	ELSE
		
		//DEFAULT VERSION
		
		//CARBON OFF
		SET_VEHICLE_EXTRA(aVeh, 7, TRUE)
		SET_VEHICLE_EXTRA(aVeh, 8, TRUE) 
		SET_VEHICLE_EXTRA(aVeh, 9, TRUE)
		
		//if you turn on extra 1/2/7 you should see the default heli
		SET_VEHICLE_EXTRA(aVeh, 1, FALSE) 
		SET_VEHICLE_EXTRA(aVeh, 2, FALSE)
		SET_VEHICLE_EXTRA(aVeh, 7, FALSE) 
		
		//MISCe, MISCf, MISCg ON
		SET_VEHICLE_EXTRA(aVeh, 10, TRUE)
		SET_VEHICLE_EXTRA(aVeh, 11, TRUE)
		SET_VEHICLE_EXTRA(aVeh, 12, TRUE)
		CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] SETUP_SUPERVOLITO_EXTRAS -SUPERVOLITO Carbon = FALSE ")
	ENDIF
ENDPROC


FUNC BOOL IS_FADE_HAPPENING_FOR_YACHT_LOADER(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	IF (LocalData.iYachtFadeState > 0)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC


PROC SET_FLAGS_FOR_YACHT_VEHICLE_CLEANUP(INT iVehicle) 
	CPRINTLN(DEBUG_YACHT, " SET_FLAGS_FOR_YACHT_VEHICLE_CLEANUP - iVehicle ", iVehicle)	
	// clear up flags
	SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_CREATED, FALSE)
	SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_CREATED_ON_LAND, FALSE)
	SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_HAS_TAKEN, FALSE)
	SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_NOT_DRIVEABLE, FALSE)
	SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_RESET_COLOUR, FALSE)						
	SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_CLEANUP, FALSE)
	SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_DELETE, FALSE)
ENDPROC


PROC UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING(PRIVATE_YACHT_LOCAL_DATA &LocalData, INT iVehicle)

	INT iYachtID = LOCAL_PLAYER_PRIVATE_YACHT_ID()
	VECTOR vCoords, vCoords2
	FLOAT fHeading
	BOOL bRunCleanupStateCheck = TRUE
	BOOL bVehicleSpawningActive = FALSE
	BOOL bDoVisibleChecks
	BOOL bDoEnitySafeForCreationCheck = TRUE
	INT Colour1, Colour2, Colour3, Colour4, Colour5, Colour6
	BOOL bDontDoVisibleChecksThisFrame = FALSE
	BOOL bCreateVehicle
	
	CLEANUP_OLD_YACHT_VEHICLES()
	
	// unset cleanup and delete flags if the vehicle doesnt exist
	IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_CLEANUP)
	OR IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_DELETE)
		IF NOT DOES_ENTITY_EXIST(LocalData.StoredVehicleID[iVehicle])
			CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - vehicle doesn't exist, unsetting cleanup flags. ", iVehicle)
			SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_CLEANUP, FALSE)
			SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_DELETE, FALSE)	
			LocalData.iVehicleSpawnState[iVehicle] = 0 
		ENDIF
	ENDIF
	
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
	AND IS_PRIVATE_YACHT_ACTIVE(iYachtID)
	
		// local player must be near his yacht
		IF IS_PLAYER_ON_YACHT(PLAYER_ID(), iYachtID)
		OR IS_PLAYER_NEAR_YACHT(PLAYER_ID(), iYachtID, gPrivateYachtSpawnVehicleDetails[iVehicle].fSpawnInDistance)
		OR FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
			bVehicleSpawningActive = TRUE
		ELSE
			IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SPAWN_ON_LAND)
				bVehicleSpawningActive = TRUE
			ENDIF
		ENDIF
		
		
		
		
		IF (bVehicleSpawningActive)
		OR NOT (LocalData.iBS_PlayersOnMyYacht = 0)
		
				
			
			IF NOT IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_CREATED)
			AND (HAS_YACHT_COLLISION_LOADED() OR IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SPAWN_ON_LAND))
			
				
				bRunCleanupStateCheck = FALSE

				// should we try and create the vehicle?
				IF (LocalData.iVehicleSpawnState[iVehicle] = 0)
					
					// dont create any vehicles if player is already in a vehicle, he might be wanting to land his heli here for instance.
					//IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					
					// should we create this vehicle?
					
					bCreateVehicle = FALSE
					
					IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_ONLY_WHEN_REQUESTED)
						IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_REQUESTED)
							bCreateVehicle = TRUE
							CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - requested vehicle has been requested ", iVehicle)
						ELSE
							CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - requested vehicle has not been requested ", iVehicle)
						ENDIF
					ELSE
						CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - non requested vehicle ", iVehicle)
						bCreateVehicle = TRUE	
					ENDIF
					
					// if this is the taxi heli, but the heli
					IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_DONT_SPAWN_IF_PLAYER_IN_TAXI_HELI)
					AND IS_LOCAL_PLAYER_IN_RUNNING_YACHT_TAXI_HELI()
						CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - running taxi heli ", iVehicle)
						bCreateVehicle = FALSE
					ENDIF
					
					// dont spawn if player is approaching in a boat
					IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_DONT_SPAWN_IF_PLAYER_ON_BOAT)
					AND IS_LOCAL_PLAYER_IN_A_BOAT()
						CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - local player in a boat, dont create. ", iVehicle)
						bCreateVehicle = FALSE	
					ENDIF
					
					IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_DONT_SPAWN)
						CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - a lower level yacht has been bought, dont create. ", iVehicle)
						LocalData.iVehicleSpawnState[iVehicle] = 2 // so this doesn't get spammed
						bCreateVehicle = FALSE	
					ENDIF
					
					IF DOES_ENTITY_EXIST(LocalData.StoredVehicleID[iVehicle])
					AND NOT IS_ENTITY_DEAD(LocalData.StoredVehicleID[iVehicle])					
						IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SPAWN_ON_LAND)
						OR (FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT() AND NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS())
						OR (FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT() AND IS_SKYSWOOP_IN_SKY())
							CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - entity already exists, lockign it.  ", iVehicle)
							SET_YACHT_VEHICLE_AS_UNUSABLE(LocalData.StoredVehicleID[iVehicle], TRUE)
						ELSE
							bCreateVehicle = FALSE
							CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - entity still exists. ", iVehicle)
						ENDIF						
					ENDIF
					
					// if player is currently doing a yacht warp dont create vehicles until it they have moved the warped vehicles first.
					IF DOES_ENTITY_EXIST(g_SpawnData.YachtWarpVehicleID)
					AND NOT IS_ENTITY_DEAD(g_SpawnData.YachtWarpVehicleID)
						bCreateVehicle = FALSE	
						CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - g_SpawnData.YachtWarpVehicleID exists. ", iVehicle)	
					ENDIF
					
					// don't create if a network cutscene is running or else they wont be networked
					IF NETWORK_IS_IN_MP_CUTSCENE()
						bCreateVehicle = FALSE
						CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - NETWORK_IS_IN_MP_CUTSCENE. ", iVehicle)	
					ENDIF
					
					IF FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
						IF NOT SHOULD_FMMC_YACHT_EXTRA_VEHICLE_BE_SPAWNED(ciYACHT_LOBBY_HOST_YACHT_INDEX, GET_PRIVATE_YACHT_SPAWN_VEHICLE_MODEL(iVehicle))
							bCreateVehicle = FALSE
							CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN][HostOwnedYacht_Spam] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - SHOULD_FMMC_YACHT_EXTRA_VEHICLE_BE_SPAWNED is returning FALSE for extra vehicle ", iVehicle)	
						ENDIF
					ENDIF
					
					// don't create if tunables are switched off
					
					// boats
					IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SET_ANCHOR)
						IF (g_sMPTunables.byacht_disable_spawned_boats)
							bCreateVehicle = FALSE
							CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - boat tunable enabled. ", iVehicle)	
						ENDIF
					// helis
					ELSE	
						IF (g_sMPTunables.byacht_disable_spawned_helis)
							bCreateVehicle = FALSE
							CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - heli tunable enabled. ", iVehicle)	
						ENDIF
					ENDIF

					// good to create vehicle
					IF (bCreateVehicle)
						// if minimum time has passed then start trying to respawn vehicle.		
						IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), LocalData.VehicleSpawnTimer[iVehicle])) > gPrivateYachtSpawnVehicleDetails[iVehicle].iRespawnTime)
						OR IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SPAWN_ON_LAND)
						OR NOT IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_COOLDOWN_TIMER_ACTIVE)
						OR (FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT() AND NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS())
							CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - we should try and create vehicle on yacht ", iVehicle)						
							REQUEST_MODEL(gPrivateYachtSpawnVehicleDetails[iVehicle].ModelName)
							LocalData.VehicleSpawnTimer[iVehicle] = GET_NETWORK_TIME()
							LocalData.iVehicleSpawnState[iVehicle]++	
						ELSE
							CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - waiting on respawn timer ", iVehicle)	
						ENDIF
						
					ENDIF
					
				ENDIF
				
				// wait for the model to load
				IF (LocalData.iVehicleSpawnState[iVehicle] = 1)
					
					bCreateVehicle = TRUE // we should be good to create now.
				
					REQUEST_MODEL(gPrivateYachtSpawnVehicleDetails[iVehicle].ModelName)
					IF HAS_MODEL_LOADED(gPrivateYachtSpawnVehicleDetails[iVehicle].ModelName)
					
						// wait to create if spawn point is blocked and yacht warp is active.
						// note: we do this here and not above as we need the model to be loaded in so can create immediately at end of yacht warp.
						IF (g_SpawnData.bYachtVehicleSpawnBlocked[iVehicle])
						AND IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(PLAYER_ID())
							// block creation
							bCreateVehicle = FALSE
							CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - spawn area block and doing yacht warp. ", iVehicle)		
						ELSE
							g_SpawnData.bYachtVehicleSpawnBlocked[iVehicle] = FALSE	
							bDontDoVisibleChecksThisFrame = TRUE
							CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - no longer doing yacht warp, unsetting bYachtVehicleSpawnBlocked. ", iVehicle)
						ENDIF
						
						// still good to create?
						IF (bCreateVehicle)
					
							GET_PRIVATE_YACHT_SPAWN_VEHICLE_LOCATION(iVehicle, iYachtID, vCoords, fHeading, IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SPAWN_ON_LAND))
						
							// dont do visible checks if during a yacht warp
							bDoVisibleChecks = TRUE
							IF IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(PLAYER_ID())
							OR IS_FADE_HAPPENING_FOR_YACHT_LOADER(LocalData)
							OR (bDontDoVisibleChecksThisFrame)
								CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - bDoVisibleChecks = FALSE ", iVehicle)	
								bDoVisibleChecks = FALSE
							ENDIF
						
							// should we skip the entity creation check?
							bDoEnitySafeForCreationCheck = TRUE
							IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SPAWN_ON_LAND)
								IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), LocalData.VehicleSpawnTimer[iVehicle])) > 20000)	
									CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - bDoEnitySafeForCreationCheck = FALSE ", iVehicle)
									CLEAR_AREA_OF_VEHICLES(vCoords, 10.0, FALSE, FALSE, TRUE, TRUE, TRUE)
									bDoEnitySafeForCreationCheck = FALSE
								ENDIF
							ENDIF

						
							IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(<<vCoords.x, vCoords.y, vCoords.z + GetModelHeightOffset(gPrivateYachtSpawnVehicleDetails[iVehicle].ModelName)>>, 
																	gPrivateYachtSpawnVehicleDetails[iVehicle].fClearRadius, 
																	gPrivateYachtSpawnVehicleDetails[iVehicle].fClearRadius,
																	0.0,
																	gPrivateYachtSpawnVehicleDetails[iVehicle].fClearRadius * 2.0,
																	bDoVisibleChecks,
																	bDoVisibleChecks,
																	bDoVisibleChecks,
																	60.0)
								OR (bDoEnitySafeForCreationCheck = FALSE)
								OR ((iVehicle = 0) AND FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT() AND (IS_SKYSWOOP_IN_SKY() OR NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()))

																	
								CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - good to create vehicle ", iVehicle)									
								IF CREATE_NET_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.netVehicleID[iVehicle], gPrivateYachtSpawnVehicleDetails[iVehicle].ModelName, vCoords, fHeading, FALSE, TRUE, FALSE)
									INT iLockStateOverride = -1
									
									IF NETWORK_IS_ACTIVITY_SESSION()
										iLockStateOverride = 0
									ENDIF
									
									LocalData.StoredVehicleID[iVehicle] = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.netVehicleID[iVehicle])
									
									SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(LocalData.StoredVehicleID[iVehicle], TRUE)
									
									// apply any settings to this vehicle.
									IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SPAWN_ON_LAND)
										SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_CREATED_ON_LAND, TRUE)
										
										// give it a blip so we can find it
										LocalData.YachtVehicleBlipID[iVehicle] = ADD_BLIP_FOR_ENTITY(LocalData.StoredVehicleID[iVehicle])
										
										IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SET_ANCHOR)
											SET_BLIP_SPRITE(LocalData.YachtVehicleBlipID[iVehicle], RADAR_TRACE_BOAT)
										ELSE
											SET_BLIP_SPRITE(LocalData.YachtVehicleBlipID[iVehicle], RADAR_TRACE_HELICOPTER)
										ENDIF
										
									ENDIF
									SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_CREATED, TRUE)
									SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_NOT_DRIVEABLE, FALSE)
									SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_HAS_TAKEN, FALSE)
									SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_COOLDOWN_TIMER_ACTIVE, FALSE)
									SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_CLEANUP, FALSE)
									SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_DELETE, FALSE)
									SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_RESET_COLOUR, FALSE)
									
									IF NOT (gPrivateYachtSpawnVehicleDetails[iVehicle].iLivery = -1)
										SET_VEHICLE_LIVERY(LocalData.StoredVehicleID[iVehicle], gPrivateYachtSpawnVehicleDetails[iVehicle].iLivery)
									ENDIF
																	
									GET_PRIVATE_YACHT_VEHICLE_CAR_COLOURS(gPrivateYachtSpawnVehicleDetails[iVehicle].ModelName, LocalData.YachtData[iYachtID].Appearance.iTint, Colour1, Colour2, Colour3, Colour4, Colour5, Colour6)
									SET_VEHICLE_COLOURS(LocalData.StoredVehicleID[iVehicle], Colour1, Colour2)
									SET_VEHICLE_EXTRA_COLOURS(LocalData.StoredVehicleID[iVehicle], Colour3, Colour4)
									SET_VEHICLE_EXTRA_COLOUR_5(LocalData.StoredVehicleID[iVehicle], Colour5)
									SET_VEHICLE_EXTRA_COLOUR_6(LocalData.StoredVehicleID[iVehicle], Colour6)
									
									
									
	//								IF gPrivateYachtSpawnVehicleDetails[iVehicle].ModelName = SUPERVOLITO2
	//									SETUP_SUPERVOLITO_EXTRAS(LocalData.StoredVehicleID[iVehicle])
	//								ENDIF
									
									// Anchor?
									IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SET_ANCHOR)
										IF CAN_ANCHOR_BOAT_HERE(LocalData.StoredVehicleID[iVehicle])
											SET_BOAT_ANCHOR(LocalData.StoredVehicleID[iVehicle], TRUE)	
											CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - setting anchor on vehicle. ", iVehicle)
										ELSE
											CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - can't anchor on vehicle. ", iVehicle)
										ENDIF
									ENDIF			
									
											
									// clear request flag
									IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_ONLY_WHEN_REQUESTED)
										CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - clearing requested flag. ", iVehicle)
										SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_REQUESTED, FALSE)
									ENDIF
									
									// this vehicle will always exist for local player
									SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.netVehicleID[iVehicle], PLAYER_ID(), TRUE)
									
									// so it is visible when entering property.
									//SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.netVehicleID[iVehicle], TRUE, TRUE)
									
									// make sure engine is off
									SET_VEHICLE_ENGINE_ON(LocalData.StoredVehicleID[iVehicle], FALSE, FALSE)
									
									// blowing up these vehicles shouldn't influence wanted level. 2568865
									SET_VEHICLE_INFLUENCES_WANTED_LEVEL(LocalData.StoredVehicleID[iVehicle], FALSE)
																	
									SET_PERSONAL_VEHICLE_LOCK_STATE(LocalData.StoredVehicleID[iVehicle], MP_STAT_YACHT_VEHICLE_ACCESS, iLockStateOverride)
									LocalData.iStoredAllowIntoYV[iVehicle] = GET_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_VEHICLE_ACCESS)
									
									// add decorators to combat duping.
									IF DECOR_IS_REGISTERED_AS_TYPE("PYV_Owner", DECOR_TYPE_INT)
										DECOR_SET_INT(LocalData.StoredVehicleID[iVehicle], "PYV_Owner", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))								
									ENDIF
									IF DECOR_IS_REGISTERED_AS_TYPE("PYV_Vehicle", DECOR_TYPE_INT)
										DECOR_SET_INT(LocalData.StoredVehicleID[iVehicle], "PYV_Vehicle", iVehicle)								
									ENDIF
									IF DECOR_IS_REGISTERED_AS_TYPE("PYV_Yacht", DECOR_TYPE_INT)
										DECOR_SET_INT(LocalData.StoredVehicleID[iVehicle], "PYV_Yacht", iYachtID)		
									ENDIF
									
									IF iVehicle = 0
									AND FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
										SET_VEHICLE_ON_GROUND_PROPERLY(LocalData.StoredVehicleID[iVehicle])
									ENDIF
									
									LocalData.iVehicleSpawnState[iVehicle]++
									
								ELSE
									CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - failed to create vehicle ", iVehicle)
								ENDIF
																	
							ELSE
								CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - waiting for point to be safe ", iVehicle)					
							ENDIF
							
						ELSE
							CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - waiting for point to be safe ", iVehicle)											
						ENDIF
							
					ELSE
						CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - loading model for vehicle ", iVehicle)
					ENDIF
				ENDIF
				
				// we have successfully created vehicle
				IF (LocalData.iVehicleSpawnState[iVehicle] = 2)
					// do nothing		
				ENDIF
			ELSE
				// has it gone out of range of the yacht? if so we can mark it as no longer needed.			
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.netVehicleID[iVehicle])
					VEHICLE_INDEX tempVeh = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.netVehicleID[iVehicle])
					IF NOT IS_VEHICLE_DRIVEABLE(tempVeh)
						CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - vehicle is not driveable, cleanup ", iVehicle)
						SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_NOT_DRIVEABLE, TRUE)
						SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_CLEANUP, TRUE)
					ELSE
								
						// has anyone taken this vehicle? i.e. vehicle is no longer where it got created.
						GET_PRIVATE_YACHT_SPAWN_VEHICLE_LOCATION(iVehicle, iYachtID, vCoords, fHeading, IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_CREATED_ON_LAND))
						vCoords2 = GET_ENTITY_COORDS(tempVeh)
						// ignore z cause of waves
						vCoords.z = 0.0
						vCoords2.z = 0.0

						IF NOT IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_HAS_TAKEN)		
							
							// should we try and re-achor?
							IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SET_ANCHOR)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
									IF (IS_PRIVATE_YACHT_ID_VALID(iYachtID) AND NOT IS_POINT_IN_YACHT_BOUNDING_BOX(GET_ENTITY_COORDS(tempVeh, FALSE), iYachtID))
									OR NOT IS_PRIVATE_YACHT_ID_VALID(iYachtID)
										IF NOT IS_BOAT_ANCHORED(tempVeh)	
											// check there is no driver	
											IF (GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_DRIVER) = NULL)
												IF CAN_ANCHOR_BOAT_HERE(tempVeh)
													SET_BOAT_ANCHOR(tempVeh, TRUE)											
													CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - managed to re anchor. ", iVehicle)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
							IF (VDIST(vCoords, vCoords2) > gPrivateYachtSpawnVehicleDetails[iVehicle].fClearRadius)
								CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - setting vehicle as taken ", iVehicle)
								SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_HAS_TAKEN, TRUE)
							ENDIF	
						ELSE
							IF NOT (VDIST(vCoords, vCoords2) > gPrivateYachtSpawnVehicleDetails[iVehicle].fClearRadius)	
								CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - setting vehicle no longer taken ", iVehicle)
								SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_HAS_TAKEN, FALSE)	
							ENDIF
						ENDIF
						
						// cleanup if vehicle is no longer near where it was created
						IF VDIST(vCoords, vCoords2) > 100.0
							CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - vehicle has gone out of range, cleanup ", iVehicle)							
							SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_CLEANUP, TRUE)
							IF NOT IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_HAS_TAKEN)
								CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - setting vehicle as taken (2) ", iVehicle)
								SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_HAS_TAKEN, TRUE)
							ENDIF
						ENDIF
						
						// cleanup if the spawn_on_land state has changed from when it was created.
						IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_CREATED_ON_LAND)
							IF NOT IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SPAWN_ON_LAND)
								CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - spawn on land not set, cleaning up ", iVehicle)							
								SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_CLEANUP, TRUE)	
							ENDIF
						ELSE
							IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SPAWN_ON_LAND)
								CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - spawn on land is set, cleaning up ", iVehicle)							
								SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_CLEANUP, TRUE)
							ENDIF
						ENDIF
						
						// if it has a blip should we rotate the it?
						IF DOES_BLIP_EXIST(LocalData.YachtVehicleBlipID[iVehicle])
							
							IF SHOULD_BLIP_BE_MANUALLY_ROTATED(LocalData.YachtVehicleBlipID[iVehicle])
								IF DOES_ENTITY_EXIST(tempVeh)
								AND NOT IS_ENTITY_DEAD(tempVeh)
									SET_BLIP_ROTATION(LocalData.YachtVehicleBlipID[iVehicle], ROUND(GET_ENTITY_HEADING_FROM_EULERS(tempVeh)))
								ENDIF
							ENDIF
							
							IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_HAS_TAKEN)
							OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempVeh)
								REMOVE_BLIP(LocalData.YachtVehicleBlipID[iVehicle])
							ENDIF
						ENDIF

						

					ENDIF
					
					IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_DONT_SPAWN_IF_PLAYER_IN_TAXI_HELI)
					AND IS_LOCAL_PLAYER_IN_RUNNING_YACHT_TAXI_HELI()
						IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_CREATED)
							CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - heli taxi is now active, cleanup ", iVehicle)
							SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_CLEANUP, TRUE)
						ENDIF
					ENDIF
					
				ELSE
				
					// for some reason the entity no longer exists, perhaps something else has deleted it, so we need to cleanup the flags
					IF NOT DOES_ENTITY_EXIST(LocalData.StoredVehicleID[iVehicle])
						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - something has deleted vehicle, cleaning up flags. ", iVehicle)
						SET_FLAGS_FOR_YACHT_VEHICLE_CLEANUP(iVehicle)	
					ENDIF
					
				ENDIF
			ENDIF
		ELSE
			// will only cleanup vehicles if no one else is left near the yacht
			IF (LocalData.iBS_PlayersNearMyYacht = 0)
			AND (LocalData.iBS_PlayersOnMyYacht = 0)
				IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_CREATED)
					CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - cleanup as no one is near(1) ", iVehicle)
					SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_CLEANUP, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_CREATED)
			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - yacht is no longer active, we should delete ", iVehicle)
			IF NOT IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_HAS_TAKEN)	
				SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_DELETE, TRUE)
			ELSE
				SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_CLEANUP, TRUE)
			ENDIF
		ENDIF
	ENDIf




	// try and cleanup the network vehicle.
	IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_CLEANUP)
	OR IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_DELETE)
	
		IF (DOES_ENTITY_EXIST(g_SpawnData.YachtWarpVehicleID) AND NOT (g_SpawnData.YachtWarpVehicleID = LocalData.StoredVehicleID[iVehicle]))
		OR NOT DOES_ENTITY_EXIST(g_SpawnData.YachtWarpVehicleID)		
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.netVehicleID[iVehicle])
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.netVehicleID[iVehicle])
				
					// if the player is warping his yacht and this vehicle still belongs to the 'old' yacht, dont delete until new yacht
					// has been assigned.
					IF IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(PLAYER_ID())
					AND DECOR_IS_REGISTERED_AS_TYPE("PYV_Yacht", DECOR_TYPE_INT)
					AND (DECOR_EXIST_ON(LocalData.StoredVehicleID[iVehicle], "PYV_Yacht") AND DECOR_GET_INT(LocalData.StoredVehicleID[iVehicle], "PYV_Yacht") = iYachtID)
						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - cleaned - vehicle is in warp cutscene and still belongs to old yacht ", iVehicle)	
					ELSE
				
						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - cleaning up ", iVehicle)	
						
						SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.netVehicleID[iVehicle], FALSE)
						
						// reset respawn timer if the vehicle was taken.
						IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_HAS_TAKEN)
						OR IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_NOT_DRIVEABLE)
						
							// dont respawn on land if it's been taken.
							IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SPAWN_ON_LAND)
							AND IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_CREATED_ON_LAND)
								CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - cleanup - land spawned vehicle was taken, so will now respawn back on yacht. ", iVehicle)	
								SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_SPAWN_ON_LAND, FALSE)
							ENDIF
						
							// dont reset timer if requested to spawn on land but hasnt been created on land
							IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_SPAWN_ON_LAND)
							AND NOT IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_CREATED_ON_LAND)
								CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - cleanup - not resetting timer as requested to spawn on land but not yet created there. ", iVehicle)	
							ELSE
								LocalData.VehicleSpawnTimer[iVehicle] = GET_NETWORK_TIME()
								SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_COOLDOWN_TIMER_ACTIVE, TRUE)
							ENDIF
							
						ENDIF
						
						
																					
						IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_ONLY_WHEN_REQUESTED)
							IF NOT IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_HAS_TAKEN)										
								CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - only when request vehicle wasnt taken, re-requesting. ", iVehicle)
								SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_REQUESTED, TRUE)
							ENDIF
						ENDIF
						
						IF NATIVE_TO_INT(PLAYER_ID()) > -1
							VEHICLE_INDEX aVeh = LocalData.StoredVehicleID[iVehicle]
							IF IS_VEHICLE_DRIVEABLE(aVeh)
								SET_VEHICLE_EXCLUSIVE_DRIVER(aVeh, NULL)
								SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(aVeh, FALSE)
								SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(aVeh, FALSE)
								CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - cleanup - unlocking vehicle for all players. ", iVehicle)	
							ENDIF
						ENDIF
						
						IF DOES_BLIP_EXIST(LocalData.YachtVehicleBlipID[iVehicle])
							REMOVE_BLIP(LocalData.YachtVehicleBlipID[iVehicle])
						ENDIF						
						
						IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_DELETE)
						AND ((IS_VEHICLE_EMPTY(LocalData.StoredVehicleID[iVehicle], TRUE)) OR IS_VEHICLE_ON_YACHT(LocalData.StoredVehicleID[iVehicle], iYachtID))
							DELETE_NET_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.netVehicleID[iVehicle])	
						ELSE
							CLEANUP_NET_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.netVehicleID[iVehicle])
						ENDIF	
						
						// clear up flags
						SET_FLAGS_FOR_YACHT_VEHICLE_CLEANUP(iVehicle)
						
						// if this is a yacht warp we definately want to create the vehicle at the new location
						IF IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(PLAYER_ID())
							LocalData.StoredVehicleID[iVehicle] = NULL
							CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - yacht warp - clearing StoredVehicleID. ", iVehicle)
						ENDIF
					
					ENDIF
					
				ELSE
					// Only request control if nobody is driving that vehicle, otherwise 
					// ownership just goes back and forth and this causes vehicles to lag for people who drive them
					// (url:bugstar:2593630)
					IF GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.netVehicleID[iVehicle]), VS_DRIVER) = NULL
					OR (IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.netVehicleID[iVehicle]))
						AND GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.netVehicleID[iVehicle]), VS_DRIVER) = NULL)
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.netVehicleID[iVehicle])
						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - requesting control of vehicle ", iVehicle)
					ENDIF
				ENDIF
			ELSE
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - NETWORK_DOES_NETWORK_ID_EXIST = FALSE ", iVehicle)	
			ENDIF	
		ELSE
			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - vehicle to delete is g_SpawnData.YachtWarpVehicleID , not deleting. ", iVehicle)
		ENDIF
		
	ENDIF
	

	
	
	// unload models etc. if we are no longer trying to create
	IF (bRunCleanupStateCheck)
		IF NOT (LocalData.iVehicleSpawnState[iVehicle] = 0)
		
			IF NATIVE_TO_INT(PLAYER_ID()) > -1
				VEHICLE_INDEX aVeh = LocalData.StoredVehicleID[iVehicle]
				IF IS_VEHICLE_DRIVEABLE(aVeh)
					SET_VEHICLE_EXCLUSIVE_DRIVER(aVeh, NULL)
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(aVeh, FALSE)
					SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(aVeh, FALSE)
					CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - bRunCleanupStateCheck - unlocking vehicle for all players. ", iVehicle)	
				ENDIF
			ENDIF
		
			SET_MODEL_AS_NO_LONGER_NEEDED(gPrivateYachtSpawnVehicleDetails[iVehicle].ModelName)
			LocalData.iVehicleSpawnState[iVehicle] = 0
			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING - bRunCleanupStateCheck has tidied up ", iVehicle)
		ENDIF
	ENDIF


ENDPROC


PROC UPDATE_PLAYERS_ON_OR_NEAR_MY_YACHT(INT iPlayer, PRIVATE_YACHT_LOCAL_DATA &LocalData)

	IF (iPlayer < NUM_NETWORK_PLAYERS)
		PLAYER_INDEX PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		
		INT iYachtID = LOCAL_PLAYER_PRIVATE_YACHT_ID()
		
		IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
			IF IS_NET_PLAYER_OK(PlayerID)
				IF IS_PLAYER_ON_YACHT(PlayerID, iYachtID)
					SET_BIT(LocalData.iBS_PlayersOnMyYacht, iPlayer)
				ELSE
					CLEAR_BIT(LocalData.iBS_PlayersOnMyYacht, iPlayer)	
				ENDIF
				IF IS_PLAYER_NEAR_YACHT(PlayerID, iYachtID)
					SET_BIT(LocalData.iBS_PlayersNearMyYacht, iPlayer)
				ELSE
					CLEAR_BIT(LocalData.iBS_PlayersNearMyYacht, iPlayer)	
				ENDIF
			ENDIF
		ELSE
			LocalData.iBS_PlayersOnMyYacht = 0	
			LocalData.iBS_PlayersNearMyYacht = 0
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL AreVehiclesIncludingMissionOnesAtPoint(VECTOR vPoint)

	INT iNumVehicles
	INT i

	iNumVehicles = GET_ALL_VEHICLES(g_PoolVehicles)

	BOOL IsOccupied = FALSE
	REPEAT iNumVehicles i
	    IF DOES_ENTITY_EXIST(g_PoolVehicles[i])
			IF IS_VEHICLE_DRIVEABLE(g_PoolVehicles[i])
				IF NOT (GET_ENTITY_POPULATION_TYPE(g_PoolVehicles[i]) = PT_RANDOM_PERMANENT) // so we don't include trains etc. see 1984133
					IF IsPointWithinModelBounds(vPoint, GET_ENTITY_COORDS(g_PoolVehicles[i], FALSE), GET_ENTITY_HEADING(g_PoolVehicles[i]), GET_ENTITY_MODEL(g_PoolVehicles[i]),1 )
						IsOccupied = TRUE
					ENDIF
				ENDIF
			ENDIF
	    ENDIF
	ENDREPEAT
    RETURN IsOccupied
ENDFUNC

PROC UPDATE_YACHTS_CAM_SHAKE()

	IF IS_BIT_SET(g_iMissionMiscBitset1, ciMissionMiscBitset1_OverridingYachtCamShake)
		EXIT
	ENDIF
	
	BOOL bIsPlayerAimingOnMission = FALSE
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND NETWORK_IS_ACTIVITY_SESSION()
	AND IS_PLAYER_FREE_AIMING(PLAYER_ID())
		bIsPlayerAimingOnMission = TRUE
	ENDIF
	
	FLOAT fDrunkCamTimeoutMult = Get_Drunk_Cam_Timeout_Multiplier()
	FLOAT fDrunkCamHitMult = Get_Drunk_Cam_Hit_Count_Multiplier()	
	FLOAT fCurrentDrunkShake = g_drunkCameraActualAmplitudeScalar * fDrunkCamTimeoutMult * fDrunkCamHitMult
	
	WEAPON_TYPE l_wtTempWeapon = WEAPONTYPE_INVALID
	
	IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtCamShakeStage = 0
		IF IS_LOCAL_PLAYER_ON_ANY_YACHT()
		AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT bIsPlayerAimingOnMission
		AND NOT (g_drunkCameraActive AND (fCurrentDrunkShake > fYachtCamShake))
		AND NOT (IS_FIRST_PERSON_AIM_CAM_ACTIVE() AND GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), l_wtTempWeapon)
		AND NOT g_sMPTunables.byacht_disable_camera_shake
				AND IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID()))
			
			IF NOT IS_GAMEPLAY_CAM_SHAKING()
				SHAKE_GAMEPLAY_CAM("DRUNK_SHAKE", fYachtCamShake)
			ENDIF
			SET_GAMEPLAY_CAM_SHAKE_AMPLITUDE(fYachtCamShake)
			
			CPRINTLN(DEBUG_YACHT, "UPDATE_YACHTS_CAM_SHAKE - player is on yacht, start cam shake")
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtCamShakeStage = 1
		ENDIF
	ENDIF
	IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtCamShakeStage = 1
		IF (GET_GAME_TIMER() % 250) = 0
			IF NOT IS_GAMEPLAY_CAM_SHAKING()
				SHAKE_GAMEPLAY_CAM("DRUNK_SHAKE", fYachtCamShake)
			ENDIF
			SET_GAMEPLAY_CAM_SHAKE_AMPLITUDE(fYachtCamShake)
			
			CDEBUG1LN(DEBUG_YACHT, "UPDATE_YACHTS_CAM_SHAKE - set gameplay cam shake amp:", fYachtCamShake)
		ENDIF
		
		IF g_sMPTunables.byacht_disable_camera_shake
			STOP_GAMEPLAY_CAM_SHAKING()
			CPRINTLN(DEBUG_YACHT, "UPDATE_YACHTS_CAM_SHAKE - byacht_disable_camera_shake, stop cam shake")
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtCamShakeStage = 0
		ELIF (g_drunkCameraActive)
		AND (fCurrentDrunkShake > fYachtCamShake)
			CPRINTLN(DEBUG_YACHT, "UPDATE_YACHTS_CAM_SHAKE - player drunk (", fCurrentDrunkShake, "), let it handle the cam shake")
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtCamShakeStage = 2
		ELIF NOT IS_LOCAL_PLAYER_ON_ANY_YACHT()
		OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			STOP_GAMEPLAY_CAM_SHAKING()
			CPRINTLN(DEBUG_YACHT, "UPDATE_YACHTS_CAM_SHAKE - player is off yacht/in vehicle, stop cam shake")
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtCamShakeStage = 0
		ELIF IS_FIRST_PERSON_AIM_CAM_ACTIVE()
		AND GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), l_wtTempWeapon)
		AND IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID())
			STOP_GAMEPLAY_CAM_SHAKING()
			CPRINTLN(DEBUG_YACHT, "UPDATE_YACHTS_CAM_SHAKE - player is sniping, stop cam shake")
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtCamShakeStage = 0
		ELIF bIsPlayerAimingOnMission
			STOP_GAMEPLAY_CAM_SHAKING()
			CPRINTLN(DEBUG_YACHT, "UPDATE_YACHTS_CAM_SHAKE - player is aiming on mission, stop cam shake")
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtCamShakeStage = 0
		ENDIF
	ENDIF
	IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtCamShakeStage = 2
		IF NOT (g_drunkCameraActive)
		OR g_sMPTunables.byacht_disable_camera_shake
			CPRINTLN(DEBUG_YACHT, "UPDATE_YACHTS_CAM_SHAKE - player sober(", fCurrentDrunkShake, "), rehandle cam shake checks")
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtCamShakeStage = 1
		ENDIF
	ENDIF
ENDPROC



PROC UPDATE_YACHT_HORN(PRIVATE_YACHT_LOCAL_DATA &LocalData)

	IF g_sMPTunables.byacht_disable_horn
		g_bSoundYachtHorn = FALSE
	ENDIF

	IF g_bSoundYachtHorn
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_Yacht_01")
			IF NOT HAS_NET_TIMER_STARTED(LocalData.iYachtHornTimer)
				INT iCurrentYacht = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
				IF iCurrentYacht != -1
				AND DOES_LOCAL_PLAYER_OWN_YACHT(iCurrentYacht)
				
					IF LocalData.iYachtHornSoundID != -1
						STOP_SOUND(LocalData.iYachtHornSoundID)
					ENDIF
					LocalData.iYachtHornSoundID = GET_SOUND_ID()
				
					MP_PROP_OFFSET_STRUCT sPropOffset
					GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_HORN, sPropOffset)
					PLAY_SOUND_FROM_COORD(LocalData.iYachtHornSoundID, "HORN", sPropOffset.vLoc, "DLC_Apt_Yacht_Ambient_Soundset", TRUE, 250)
				ENDIF
				
				START_NET_TIMER(LocalData.iYachtHornTimer)
			ENDIF
		ENDIF
		
		
		IF (HAS_NET_TIMER_STARTED(LocalData.iYachtHornTimer) AND (HAS_NET_TIMER_EXPIRED(LocalData.iYachtHornTimer, 1750) OR (LocalData.iYachtHornSoundID != -1 AND HAS_SOUND_FINISHED(LocalData.iYachtHornSoundID))))
		
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_Yacht_01")
			RESET_NET_TIMER(LocalData.iYachtHornTimer)
			g_bSoundYachtHorn = FALSE
			
			IF LocalData.iYachtHornSoundID != -1
				STOP_SOUND(LocalData.iYachtHornSoundID)
				RELEASE_SOUND_ID(LocalData.iYachtHornSoundID)
				LocalData.iYachtHornSoundID = -1
			ENDIF
		ENDIF
	ELSE
		IF LocalData.iYachtHornSoundID != -1
			STOP_SOUND(LocalData.iYachtHornSoundID)
			RELEASE_SOUND_ID(LocalData.iYachtHornSoundID)
			LocalData.iYachtHornSoundID = -1
		ENDIF
	ENDIF
ENDPROC


PROC matrixIntoQuaternions(float m11, float m12, float m13,
							float m21, float m22, float m23,
							float m31, float m32, float m33,
							float &x, float &Y,float &Z,float &w) 


  
            // Determine which of w,x,y, or z has the largest absolute value
            float fourWSquaredMinus1 = m11 + m22 + m33
            float fourXSquaredMinus1 = m11 - m22 - m33
            float fourYSquaredMinus1 = m22 - m11 - m33
            float fourZSquaredMinus1 = m33 - m11 - m22

            int biggestIndex = 0
            float fourBiggestSquaredMinus1 = fourWSquaredMinus1

            if(fourXSquaredMinus1 > fourBiggestSquaredMinus1) 
                fourBiggestSquaredMinus1 = fourXSquaredMinus1
                biggestIndex = 1
            endif
            if (fourYSquaredMinus1 > fourBiggestSquaredMinus1) 
                fourBiggestSquaredMinus1 = fourYSquaredMinus1
                biggestIndex = 2
            endif
            if (fourZSquaredMinus1 > fourBiggestSquaredMinus1) 
                fourBiggestSquaredMinus1 = fourZSquaredMinus1
                biggestIndex = 3
            endif
            // Per form square root and division
            float biggestVal = sqrt (fourBiggestSquaredMinus1 + 1.0 ) * 0.5
            float mult = 0.25 / biggestVal

            // Apply table to compute quaternion values
            switch (biggestIndex) 
                case 0
                    w = biggestVal
                    x = (m23 - m32) * mult
                    y = (m31 - m13) * mult
                    z = (m12 - m21) * mult
                    break
                case 1
                    x = biggestVal
                    w = (m23 - m32) * mult
                    y = (m12 + m21) * mult
                    z = (m31 + m13) * mult
                    break
                case 2
                    y = biggestVal
                    w = (m31 - m13) * mult
                    x = (m12 + m21) * mult
                    z = (m23 + m32) * mult
                    break
                case 3
                    z = biggestVal
                    w = (m12 - m21) * mult
                    x = (m31 + m13) * mult
                    y = (m23 + m32) * mult
                    break
                
            endswitch
ENDPROC

FUNC VECTOR ABSV(VECTOR aVec)
	RETURN <<ABSF(aVec.x), ABSF(aVec.z), ABSF(aVec.z)>>
ENDFUNC

PROC UpForwardToEuler(Vector vUp, Vector vForward, VECTOR vSide, Vector& Rotation)

	//heading = Math.atan2(-m.m20,m.m00);
	//bank = Math.atan2(-m.m12,m.m11);
	//attitude = Math.asin(m.m10);

//	Rotation.x = atan2(-vSide.x,vUp.x);
//	Rotation.y = atan2(-vForward.z,vForward.y);
//	Rotation.z = asin(vForward.x);
	
	//double Z1xy = sqrt(Z1x * Z1x + Z1y * Z1y);
	//pre = atan2(Y1x * Z1y - Y1y*Z1x, X1x * Z1y - X1y * Z1x);
   //nut = atan2(Z1xy, Z1z);
   //rot = -atan2(-Z1x, Z1y);
	
	
	
	FLOAT X1x = vUp.x
	FLOAT X1y = vUp.y
//	FLOAT X1z = vUp.z
	
	FLOAT Y1x = vForward.x
	FLOAT Y1y = vForward.y
//	FLOAT Y1z = vForward.z
	
	FLOAT Z1x = vSide.x
	FLOAT Z1y = vSide.y
	FLOAT Z1z = vSide.z
	
	FLOAT Z1xy = sqrt(Z1x * Z1x + Z1y * Z1y)
	
	FLOAT pre = atan2(Y1x * Z1y - Y1y*Z1x, X1x * Z1y - X1y * Z1x)
	FLOAT nut = atan2(Z1xy, Z1z)
	FLOAT rot = -atan2(-Z1x, Z1y)

	Rotation = <<pre+270, nut+90, 	rot>>	

ENDPROC

//ENTITY_INDEX aGhostVeh

FUNC VECTOR GET_ENTITY_BOAT_TILT_ROTATION(ENTITY_INDEX aVehicle)

	FLOAT Zcoord
	VECTOR GroundNormal
	VECTOR vEntityCoords = GET_ENTITY_COORDS(aVehicle)
	FLOAT fEntityHeading = GET_ENTITY_HEADING(aVehicle)
	MODEL_NAMES mnEntitymodel = GET_ENTITY_MODEL(aVehicle)
	GET_GROUND_Z_AND_NORMAL_FOR_3D_COORD(vEntityCoords, Zcoord, GroundNormal)
//	VECTOR Entity_rotation = GET_ENTITY_ROTATION(aVehicle, EULER_XYZ)

	
	FLOAT ReturnX,ReturnY, ReturnZ, ReturnW
	GET_ENTITY_QUATERNION(aVehicle, ReturnX, ReturnY,ReturnZ, ReturnW )
	
	VECTOR vMin, vMax
	SAFE_GET_MODEL_DIMENSIONS(mnEntitymodel, vMin, vMax)
	FLOAT fLength, fWidth //, fHeight
	
	fLength = ABSF(vMax.y - vMin.y)
	fWidth = ABSF(vMax.x - vMin.x)
//	fHeight = ABSF(vMax.z - vMin.z)

	VECTOR FrontOfBoat = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vEntityCoords, fEntityHeading,<<0,(fLength/2.0),0>> )
	VECTOR RightSideOfBoat = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vEntityCoords, fEntityHeading,<<fWidth/2.0,0,0>> )
	VECTOR BackOfBoat = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vEntityCoords, fEntityHeading,<<0,(-fLength/2.0),0>> )
	VECTOR LeftSideOfBoat = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vEntityCoords, fEntityHeading,<<(-fWidth/2.0),0,0>> )
	
//	VECTOR WaveDirFront, WaveDirBack
	
	FLOAT FrontHeight, BackHeight, LeftHeight, RightHeight
	GET_WATER_HEIGHT(FrontOfBoat, FrontHeight)
	GET_WATER_HEIGHT(BackOfBoat, BackHeight)
	GET_WATER_HEIGHT(LeftSideOfBoat, LeftHeight)
	GET_WATER_HEIGHT(RightSideOfBoat, RightHeight)
	

//	PRINTLN("RUN_ENTITY_BOAT_TILT: FrontHeight = ", FrontHeight, " ")
//	PRINTLN("RUN_ENTITY_BOAT_TILT: BackHeight = ", BackHeight, " ")
//	PRINTLN("RUN_ENTITY_BOAT_TILT: LeftHeight = ", LeftHeight, " ")
//	PRINTLN("RUN_ENTITY_BOAT_TILT: RightHeight = ", RightHeight, " ")
	
	
	VECTOR vSeaFront = <<FrontOfBoat.x,FrontOfBoat.y,FrontHeight>>
	VECTOR vSeaRight = <<RightSideOfBoat.x,RightSideOfBoat.y,RightHeight>>
	VECTOR vSeaLeft = <<LeftSideOfBoat.x,LeftSideOfBoat.y,LeftHeight>>
	VECTOR vSeaBack = <<BackOfBoat.x,BackOfBoat.y,BackHeight>>
	
	
	
//	DRAW_DEBUG_SPHERE(vSeaFront  , 0.25)
//	DRAW_DEBUG_SPHERE(vSeaRight , 0.25,0 , 255, 0)
//	
//	DRAW_DEBUG_SPHERE( vSeaBack, 0.25, 255, 0, 0)
//	DRAW_DEBUG_SPHERE( vSeaLeft, 0.25,0 , 255, 255)
	
	

	
	VECTOR NormalisedForward = vSeaFront-vSeaBack
	VECTOR NormalisedSide = vSeaLeft-vSeaRight
	
	NormalisedForward = NORMALISE_VECTOR(NormalisedForward)
	NormalisedSide = NORMALISE_VECTOR(NormalisedSide)
	
	
//	PRINTLN("RUN_ENTITY_BOAT_TILT: NormalisedForward = ", NormalisedForward, " ")
//	PRINTLN("RUN_ENTITY_BOAT_TILT: NormalisedSide = ", NormalisedSide, " ")

	
	
	VECTOR vUpVector = 	CROSS_PRODUCT(NormalisedForward, NormalisedSide)
	
//	PRINTLN("RUN_ENTITY_BOAT_TILT: vUpVector = ", vUpVector, " ")
	
	VECTOR vSVector = CROSS_PRODUCT(NormalisedForward, vUpVector)
	
//	PRINTLN("RUN_ENTITY_BOAT_TILT: vSVector = ", vSVector, " ")
	
	VECTOR vUnVector = CROSS_PRODUCT(NormalisedForward, vSVector)
	
//	PRINTLN("RUN_ENTITY_BOAT_TILT: vUnVector = ", vUnVector, " ")
	
	
//	VECTOR WorldUp = <<0,0,1>>
//	FLOAT angleFixedUpAndUp = Atan2(WorldUp.Y, WorldUp.z) - Atan2(vUpVector.Y, vUpVector.z) 
	
//	PRINTLN("RUN_ENTITY_BOAT_TILT: angleFixedUpAndUp = ", angleFixedUpAndUp, " ")
	
	
	
//	FLOAT NewX, NewY, NewZ, NewW
	
	
		
	//So take the cross product of your direction vector D and up vector U for the side vector S 
	//then cross D and S for a new Un. Then use D, Un, S as rows (or columns depending on how your 
	//calculation rule is set up) as a matrix. Matrix to quaternion is well known math.
//	matrixIntoQuaternions(NormalisedForward.x, NormalisedForward.y, NormalisedForward.z,
//							vUnVector.x, vUnVector.y, vUnVector.z,
//							vSVector.x, vSVector.y, vSVector.z,
//							NewX, NewY, NewZ, NewW)
							

//	Vector ExtraLongvUp	= (vUpVector)	
//	ExtraLongvUp *= 3.0
//	DRAW_DEBUG_LINE(vEntityCoords, vEntityCoords+ExtraLongvUp)							//Blue
//	Vector ExtraLongEntRot	= (Entity_rotation)	
//	ExtraLongEntRot *= 3.0
//	DRAW_DEBUG_LINE(vEntityCoords, vEntityCoords+ExtraLongEntRot, 255, 0, 255)				//Purple	
							
							
//	PRINTLN("RUN_ENTITY_BOAT_TILT: CALCUL Quart: NewX = ", NewX, " NewY = ", NewY, " NewZ = ", NewZ, " NewW = ", NewW)
//	PRINTLN("RUN_ENTITY_BOAT_TILT: PLAYER Quart: ReturnX = ", ReturnX, " ReturnY = ", ReturnY, " ReturnZ = ", ReturnZ, " ReturnW = ", ReturnW)

	
	Vector CalculatedRotation
	
//	CalculatedRotation.x = asin(ExtraLongvUp.x)
//	CalculatedRotation.y = asin(ExtraLongvUp.y)
//	CalculatedRotation.z = asin(ExtraLongvUp.z)
//	DRAW_DEBUG_LINE(vEntityCoords, vEntityCoords+CalculatedRotation, 0, 0, 0)
//	PRINTLN("RUN_ENTITY_BOAT_TILT: ROT CALCUL asin CalculatedRotation = ", CalculatedRotation)

	

	UpForwardToEuler(vUnVector, NormalisedForward, vSVector, CalculatedRotation)
	
//	PRINTLN("RUN_ENTITY_BOAT_TILT: ROT CALCUL ToEulerQ CalculatedRotation = ", CalculatedRotation)
//	DRAW_DEBUG_LINE(vEntityCoords, vEntityCoords+CalculatedRotation, 0, 0, 0)				//BLACK

	CalculatedRotation.z= fEntityHeading
	
	
	
	
//	REQUEST_MODEL(GET_ENTITY_MODEL(aVehicle))
//	IF HAS_MODEL_LOADED(GET_ENTITY_MODEL(aVehicle))
//		IF NOT DOES_ENTITY_EXIST(aGhostVeh)
//			aGhostVeh = CREATE_VEHICLE(GET_ENTITY_MODEL(aVehicle), <<vEntityCoords.x, vEntityCoords.y, vEntityCoords.z+2>>, fEntityHeading)
//			SET_ENTITY_COLLISION(aGhostVeh, FALSE)
//			SET_ENTITY_ALPHA(aGhostVeh, 150, TRUE)
//		ELSE
//			SET_ENTITY_COORDS(aGhostVeh, <<vEntityCoords.x, vEntityCoords.y, vEntityCoords.z+2>>, FALSE)
////			SET_ENTITY_HEADING(aGhostVeh, fEntityHeading)
////			SET_ENTITY_QUATERNION(aGhostVeh, NewX, NewY, NewZ, NewW)
//			SET_ENTITY_ROTATION(aGhostVeh, CalculatedRotation)
//		ENDIF
//	
//	ENDIF
	
//	Vector ExtraLongCalc	= (CalculatedRotation)	
//	ExtraLongCalc *= 3.0
//	DRAW_DEBUG_LINE(vEntityCoords, vEntityCoords+ExtraLongCalc, 255, 255, 255)				//white	
//	
//	PRINTLN("RUN_ENTITY_BOAT_TILT: ROT PLAYER Entity_rotation = ", Entity_rotation)

	CalculatedRotation.x -= 180
	CalculatedRotation.y -= 180

	FLOAT SwapX = CalculatedRotation.x
	FLOAT SwapY = CalculatedRotation.y
	
	CalculatedRotation.x = SwapY
	CalculatedRotation.y = SwapX

	VECTOR EntityRotation = GET_ENTITY_ROTATION(aVehicle)
//	CPRINTLN(DEBUG_YACHT, "GET_ENTITY_BOAT_TILT_ROTATION - ROT GET_ENTITY_ROTATION = ",EntityRotation, " ")	
//	CPRINTLN(DEBUG_YACHT, "GET_ENTITY_BOAT_TILT_ROTATION - ROT CalculatedRotation = ",CalculatedRotation, " ")	

	VECTOR DiffVector = EntityRotation-CalculatedRotation
	
//	CPRINTLN(DEBUG_YACHT, "GET_ENTITY_BOAT_TILT_ROTATION - ROT DiffVector = ",DiffVector, " ")	
	DiffVector.x /= 1.05
	DiffVector.y /= 1.05
	DiffVector.z /= 1.05
	
//	CPRINTLN(DEBUG_YACHT, "GET_ENTITY_BOAT_TILT_ROTATION - ROT NEW DiffVector = ",DiffVector, " ")	

	VECTOR CalculatedRotationOffset = CalculatedRotation
	
	CalculatedRotationOffset += DiffVector 

//	CPRINTLN(DEBUG_YACHT, "GET_ENTITY_BOAT_TILT_ROTATION - ROT CalculatedRotationOffset = ",CalculatedRotationOffset, " ")	


	RETURN CalculatedRotationOffset
	
ENDFUNC


/// PURPOSE: Check if the player is the driver of a vehicle
FUNC BOOL IS_LOCAL_PLAYER_DRIVING_ENTITY(ENTITY_INDEX anEnt)
	IF IS_ENTITY_A_VEHICLE(anEnt)
		VEHICLE_INDEX aVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(anEnt)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF GET_PED_IN_VEHICLE_SEAT(aVeh) = PLAYER_PED_ID()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC 

PROC GET_ENTITY_DOCKING_POSITIONS(ENTITY_INDEX aPlayerVeh, VECTOR TargetVector,FLOAT TargetHeading, VECTOR& BoatRotation, FLOAT& fPlayerHeading,VECTOR& PlayerVehCoord , BOOL bSkipReposition = FALSE, BOOL bSkipReheading = FALSE, FLOAT OverrideSpeed = 2.5)
	
	PlayerVehCoord = GET_ENTITY_COORDS(aPlayerVeh, FALSE)
	fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
	
	FLOAT zCoordWater
	VECTOR VectorforceApplied
	
	GET_GROUND_Z_FOR_3D_COORD(PlayerVehCoord, zCoordWater, TRUE)
	TargetVector.z = zCoordWater
	
	VectorforceApplied.x = (TargetVector.x-PlayerVehCoord.x)
	VectorforceApplied.y = (TargetVector.y-PlayerVehCoord.y)	
	VectorforceApplied.z = (TargetVector.z-PlayerVehCoord.z)	
	
//		CPRINTLN(DEBUG_YACHT, "[YACHTDELTA] GET_ENTITY_DOCKING_POSITIONS - Start ")
//		CPRINTLN(DEBUG_YACHT, "[YACHTDELTA] GET_ENTITY_DOCKING_POSITIONS - bSkipReposition ",bSkipReposition, " ")
//		CPRINTLN(DEBUG_YACHT, "[YACHTDELTA] GET_ENTITY_DOCKING_POSITIONS - bSkipReheading ",bSkipReheading, " ")
//		CPRINTLN(DEBUG_YACHT, "[YACHTDELTA] GET_ENTITY_DOCKING_POSITIONS - OverrideSpeed ",OverrideSpeed, " ")
	
//	FLOAT fPlayerHeadingReduced =  GET_HEADING_BETWEEN_VECTORS_2D(PlayerVehCoord, TargetVector )
//	FLOAT HeadDiff = fPlayerHeadingReduced-fPlayerHeading
//	CPRINTLN(DEBUG_YACHT, "GET_ENTITY_DOCKING_POSITIONS - ")
//	CPRINTLN(DEBUG_YACHT, "GET_ENTITY_DOCKING_POSITIONS - before adjustment HeadDiff ",HeadDiff, " ")
//	CPRINTLN(DEBUG_YACHT, "GET_ENTITY_DOCKING_POSITIONS - before adjustment fPlayerHeadingReduced ",fPlayerHeadingReduced, " ")
//	CPRINTLN(DEBUG_YACHT, "GET_ENTITY_DOCKING_POSITIONS - before adjustment fPlayerHeading ",fPlayerHeading, " ")
//	IF HeadDiff > 180
//		HeadDiff-=360
//	ENDIF
//	IF HeadDiff < -180
//		HeadDiff+=360
//	ENDIF
	
	
	
	
	VECTOR DirectionVector = VectorforceApplied
		
	MODEL_NAMES mnEntitymodel = GET_ENTITY_MODEL(aPlayerVeh)
	VECTOR vMin, vMax
	SAFE_GET_MODEL_DIMENSIONS(mnEntitymodel, vMin, vMax)
	FLOAT fLength
	fLength = ABSF(vMax.y - vMin.y)
		
	VECTOR FrontOfBoat = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(PlayerVehCoord, fPlayerHeading,<<0,(fLength/2.0),0>> )
	VECTOR BackOfBoat = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(PlayerVehCoord, fPlayerHeading,<<0,(-fLength/2.0),0>> )

	
	
	VECTOR NormalisedPlayerForward = FrontOfBoat-BackOfBoat
//	
//
	NormalisedPlayerForward = NORMALISE_VECTOR(NormalisedPlayerForward)
	DirectionVector = NORMALISE_VECTOR(DirectionVector)
//	
	FLOAT fDotProduct = DOT_PRODUCT(NormalisedPlayerForward, DirectionVector)
	
	
//	CPRINTLN(DEBUG_YACHT, "GET_ENTITY_DOCKING_POSITIONS - NormalisedPlayerForward ",NormalisedPlayerForward, " ")
//	CPRINTLN(DEBUG_YACHT, "GET_ENTITY_DOCKING_POSITIONS - DirectionVector ",DirectionVector, " ")
	
//	CPRINTLN(DEBUG_YACHT, "GET_ENTITY_DOCKING_POSITIONS - vCrossProduct ",vCrossProduct, " ")
//	CPRINTLN(DEBUG_YACHT, "GET_ENTITY_DOCKING_POSITIONS - HeadDiff ",HeadDiff, " ")
//	CPRINTLN(DEBUG_YACHT, "GET_ENTITY_DOCKING_POSITIONS - fDotProduct ",fDotProduct, " ")

	
	
//	CPRINTLN(DEBUG_YACHT, "GET_ENTITY_DOCKING_POSITIONS - fDirection ",fDirection, " ")
//	CPRINTLN(DEBUG_YACHT, "GET_ENTITY_DOCKING_POSITIONS - HeadDiff ",HeadDiff, " ")
		

	IF fDotProduct < 0
		//REVERSE
		VECTOR VectorforceAppliedBoat
		VECTOR HeadingDirection =  GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(PlayerVehCoord, fPlayerHeading, << 0.0, -2, 0.0>>)
		VectorforceAppliedBoat.x = (HeadingDirection.x-PlayerVehCoord.x)
		VectorforceAppliedBoat.y = (HeadingDirection.y-PlayerVehCoord.y)	
		VectorforceApplied = VectorforceAppliedBoat
//		Vector ExtraLongVectorforceAppliedBoat	= (VectorforceAppliedBoat)	
//		ExtraLongVectorforceAppliedBoat *= 3.0
//		DRAW_DEBUG_LINE(PlayerVehCoord, PlayerVehCoord+ExtraLongVectorforceAppliedBoat, 255, 0, 255)				//Purple
//		DRAW_DEBUG_SPHERE(VectorforceAppliedBoat, 0.25, 255, 0, 0)	
		FLOAT IdealHeadingBoat =GET_HEADING_BETWEEN_VECTORS_2D(PlayerVehCoord, TargetVector )
		TargetHeading = IdealHeadingBoat
//		CPRINTLN(DEBUG_YACHT, "GET_ENTITY_DOCKING_POSITIONS -REVERSE TargetHeading =  ", TargetHeading, " ")
//		CPRINTLN(DEBUG_YACHT, "GET_ENTITY_DOCKING_POSITIONS -REVERSE VectorforceAppliedBoat =  ", VectorforceAppliedBoat, " ")
		IF IS_LOCAL_PLAYER_DRIVING_ENTITY(aPlayerVeh)
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LOOK_AT_COORD) != PERFORMING_TASK
				TASK_LOOK_AT_COORD(PLAYER_PED_ID(), HeadingDirection, 2000)	
			ENDIF
		ENDIF

	ELSE

		//FORWARD	
		VECTOR VectorforceAppliedBoat
		VECTOR HeadingDirection =  GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(PlayerVehCoord, fPlayerHeading, << 0.0, 2, 0.0>>)
		VectorforceAppliedBoat.x = (HeadingDirection.x-PlayerVehCoord.x)
		VectorforceAppliedBoat.y = (HeadingDirection.y-PlayerVehCoord.y)	
		VectorforceApplied = VectorforceAppliedBoat
//		Vector ExtraLongVectorforceAppliedBoat	= (VectorforceAppliedBoat)	
//		ExtraLongVectorforceAppliedBoat *= 3.0
//		DRAW_DEBUG_LINE(PlayerVehCoord, PlayerVehCoord+ExtraLongVectorforceAppliedBoat, 255, 0, 255)				//Purple
//		DRAW_DEBUG_SPHERE(VectorforceAppliedBoat, 0.25, 255, 0, 0)	
		FLOAT IdealHeadingBoat = GET_HEADING_BETWEEN_VECTORS_2D(PlayerVehCoord, TargetVector )
		TargetHeading = IdealHeadingBoat
//		CPRINTLN(DEBUG_YACHT, "GET_ENTITY_DOCKING_POSITIONS -FORWARD TargetHeading =  ", TargetHeading, " ")
//		CPRINTLN(DEBUG_YACHT, "GET_ENTITY_DOCKING_POSITIONS -FORWARD VectorforceAppliedBoat =  ", VectorforceAppliedBoat, " ")
		IF IS_LOCAL_PLAYER_DRIVING_ENTITY(aPlayerVeh)
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LOOK_AT_COORD) != PERFORMING_TASK
				TASK_LOOK_AT_COORD(PLAYER_PED_ID(), HeadingDirection, 2000)	
			ENDIF
		ENDIF
		
		
		
		
		
	ENDIF
	
	

	VECTOR vec = VectorforceApplied
	vec /= VMAG(vec)
	
	
	
	
	FLOAT ForceMulti = OverrideSpeed //2.5 //3.0
	
//	#IF IS_DEBUG_BUILD
//		IF LocalData.fForceAppliedWidget != 0
//			ForceMulti += LocalData.fForceAppliedWidget
//		ENDIF
//	#ENDIF
	
	vec *= ForceMulti * TIMESTEP()
	
	IF bSkipReposition = FALSE
		PlayerVehCoord += vec
	ENDIF
	
//							DRAW_DEBUG_LINE(PlayerVehCoordBefore, PlayerVehCoord)	
	
	BoatRotation = GET_ENTITY_BOAT_TILT_ROTATION(aPlayerVeh)

	PlayerVehCoord.z = zCoordWater-0.05

//	CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - ROT CurrentRotation = ",CurrentRotation, " ")	
//	CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - ROT BoatRotation = ",BoatRotation, " ")	
	
	

//	CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - COORD VectorforceApplied = ",VectorforceApplied, " ")	
//	CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - COORD TargetVector = ",TargetVector, " ")	
//							
//	CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - COORD vec = ",vec, " ")
//	CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - COORD TIMESTEP = ",TIMESTEP(), " ")
//					
//	CPRINTLN(DEBUG_YACHT, "[YACHTDELTA] UPDATE_PRIVATE_YACHT_DOCKING - COORD AFTER PlayerVehCoord = ",PlayerVehCoord, " ")
	
	
	FLOAT TargetHeadingRev = TargetHeading-180
	IF TargetHeadingRev < -180
		TargetHeadingRev+=360
	ENDIF
	IF TargetHeadingRev > 180
		TargetHeadingRev-=360
	ENDIF
	
	IF TargetHeading < -180
		TargetHeading+=360
	ENDIF
	IF TargetHeading > 180
		TargetHeading-=360
	ENDIF
	IF fPlayerHeading < -180
		fPlayerHeading+=360
	ENDIF
	IF fPlayerHeading > 180
		fPlayerHeading-=360
	ENDIF
	
	
	
	FLOAT YachtOffsetHeading = TargetHeading-fPlayerHeading
	FLOAT YachtOffsetHeadingRev = TargetHeadingRev-fPlayerHeading
	FLOAT OffsetHeading
 
	
	IF YachtOffsetHeading < -180
		YachtOffsetHeading+=360
	ENDIF
	IF YachtOffsetHeading > 180
		YachtOffsetHeading-=360
	ENDIF
	IF YachtOffsetHeadingRev < -180
		YachtOffsetHeadingRev+=360
	ENDIF
	IF YachtOffsetHeadingRev > 180
		YachtOffsetHeadingRev-=360
	ENDIF

	
	IF ABSI(FLOOR(YachtOffsetHeadingRev))>ABSI(FLOOR(YachtOffsetHeading))
		OffsetHeading = YachtOffsetHeading
	ELSE
		OffsetHeading = YachtOffsetHeadingRev
	ENDIF
	
	
	
	FLOAT ForceMultiHeading = 40.0  //65.0
	
//	#IF IS_DEBUG_BUILD
//		IF LocalData.fForceAppliedHeadingWidget != 0
//			ForceMultiHeading += LocalData.fForceAppliedHeadingWidget
//		ENDIF
//	#ENDIF
	
	
	FLOAT m = 2.125
	INT x = ABSI(FLOOR(OffsetHeading))
	FLOAT b = -16.25
	ForceMultiHeading = m*x+b

//	CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - HEAD   ")
//	CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - HEAD BEFORE fPlayerHeading = ",fPlayerHeading, " ")
//
//	
//	CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - HEAD TargetHeading = ",TargetHeading, " ")	
//	CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - HEAD TargetHeadingRev = ",TargetHeadingRev, " ")
//	
//
//	CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - HEAD YachtOffsetHeading = ",YachtOffsetHeading, " ")	
//	CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - HEAD YachtOffsetHeadingRev = ",YachtOffsetHeadingRev, " ")	
//	
//	CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - HEAD OffsetHeading = ",OffsetHeading, " ")	
	
	


	IF bSkipReheading = FALSE
		
		IF OffsetHeading > 2 AND OffsetHeading < 358
		OR (OffsetHeading < -2 AND OffsetHeading > -358)
			IF OffsetHeading > 0
			
				fPlayerHeading += ForceMultiHeading * TIMESTEP()
//				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - fPlayerHeading += ")
			ELSE
				fPlayerHeading -= ForceMultiHeading * TIMESTEP()
//				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - fPlayerHeading -= ")
			ENDIF
		ELSE
//			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - in range, no update ")
		ENDIF
	ELSE
//		CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - ANCHORED, no update ")
	ENDIF
	
	
	WHILE fPlayerHeading < 0.0
		fPlayerHeading += 360.0
	ENDWHILE
	WHILE fPlayerHeading >= 360.0
		fPlayerHeading -= 360.0
	ENDWHILE	
	
	
	BoatRotation.z = 	fPlayerHeading
		
		

		
//							CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - HEAD BoatRotation.z = ",BoatRotation.z, " ")
//	CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - HEAD ForceMultiHeading = ",ForceMultiHeading, " ")
//	
//	CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - HEAD ForceMultiHeading * TIMESTEP() = ",ForceMultiHeading * TIMESTEP(), " ")
//	CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - HEAD AFTER fPlayerHeading = ",fPlayerHeading, " ")



ENDPROC




PROC GET_ENTITY_BUOY_POSITIONS(ENTITY_INDEX aPlayerVeh, VECTOR TargetVector, VECTOR& BoatRotation, VECTOR& PlayerVehCoord , BOOL bSkipReposition = FALSE, FLOAT OverrideSpeed = 0.5)
	
	IF NOT DOES_ENTITY_EXIST(aPlayerVeh)
		EXIT
	ENDIF
	
	PlayerVehCoord = GET_ENTITY_COORDS(aPlayerVeh, FALSE)
	
	FLOAT zCoordWater
	VECTOR VectorforceApplied
	VectorforceApplied.x = (TargetVector.x-PlayerVehCoord.x)
	VectorforceApplied.y = (TargetVector.y-PlayerVehCoord.y)	
	
//	CPRINTLN(DEBUG_YACHT, "[YACHTDELTA] GET_ENTITY_BUOY_POSITIONS - Start ")
		
		
	GET_GROUND_Z_FOR_3D_COORD(PlayerVehCoord, zCoordWater, TRUE)
	TargetVector.z = zCoordWater
	
	VectorforceApplied.z = (TargetVector.z-PlayerVehCoord.z)	

	VECTOR vec = VectorforceApplied
	vec /= VMAG(vec)
	

	
	vec *= OverrideSpeed * TIMESTEP()
	

//	CPRINTLN(DEBUG_YACHT, "[YACHTDELTA] GET_ENTITY_BUOY_POSITIONS - COORD BEFORE vec = ",vec, " ")	
	
	IF bSkipReposition = FALSE
	
		PlayerVehCoord += vec
	ENDIF

	
	BoatRotation = GET_ENTITY_BOAT_TILT_ROTATION(aPlayerVeh)
	
	
	PlayerVehCoord.z = zCoordWater-0.05

//					
//	CPRINTLN(DEBUG_YACHT, "[YACHTDELTA] GET_ENTITY_BUOY_POSITIONS - COORD AFTER PlayerVehCoord = ",PlayerVehCoord, " ")
		
	
	

ENDPROC


FUNC BOOL HAS_PLAYER_QUIT_DOCKING()


//Handle player breaking out
							
	INT  ileft_x, ileft_y

	ileft_x = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X) - 128
	ileft_y = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y) - 128

	IF ((ileft_x >= -128) AND (ileft_x < -50))
	OR ((ileft_x <= 128) AND (ileft_x > 50))
	OR ((ileft_y >= -128) AND (ileft_y < -50))
	OR ((ileft_y <= 128) AND (ileft_y > 50))
		RETURN TRUE
	ENDIF
	
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC UPDATE_PRIVATE_YACHT_DOCKING(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	
	IF IS_PLAYER_NEAR_YACHT(GET_PLAYER_INDEX(), LocalData.iClientYachtStagger)
		LocalData.iYachtIDWeAreDockingOn = LocalData.iClientYachtStagger
	ENDIF
	
	IF IS_PRIVATE_YACHT_ID_VALID(LocalData.iYachtIDWeAreDockingOn)
	AND IS_PRIVATE_YACHT_ACTIVE(LocalData.iYachtIDWeAreDockingOn)
		PED_INDEX playerPed = PLAYER_PED_ID()
		// Player must be driving a boat in order to be able to dock it
		// Also he must be an owner or have owner aboard
		IF IS_LOCAL_PLAYER_IN_A_BOAT()
		AND IS_PED_DRIVING_ANY_VEHICLE(playerPed)
		AND IS_PLAYER_IN_VEHICLE_WITH_YACHT_OWNER(LocalData.iYachtIDWeAreDockingOn)
		
		ELSE
			EXIT
		ENDIF
	ENDIF
	
	EXIT
				
	INT iYachtID = LocalData.iYachtIDWeAreDockingOn //GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID())
	VECTOR vCoords
	FLOAT fHeading
	BOOL bVehicleSpawningActive = FALSE
	VECTOR  PlayerVehCoord
	VEHICLE_INDEX aPlayerVeh
	FLOAT fPlayerHeading
	
	FLOAT fPlayerHeadingReduced
	FLOAT fHeadingReduced
	VECTOR vCoordFirstBasePoint
	VECTOR vActivation1
	VECTOR vActivation2
	VECTOR vActivation3
	VECTOR TargetVector
	VECTOR BoatRotation
	BOOL isOnBoat
	DRIVINGMODE DockingMode
	FLOAT HeadDiff
	
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
	AND IS_PRIVATE_YACHT_ACTIVE(iYachtID)
	
		// local player must be near his yacht
		IF IS_PLAYER_NEAR_YACHT(PLAYER_ID(), iYachtID)
			bVehicleSpawningActive = TRUE
		ELSE
			
			IF IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_DOCKING_VEHICLE)
				SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_DOCKING_VEHICLE, FALSE)
			ENDIF
			
		ENDIF
		
		BOOL canDockSEASHARK3 = FALSE
	
		IF bVehicleSpawningActive
				
			IF LocalData.DockingStages = 0
				
				aPlayerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					
				IF LocalData.iDockingStagger = LocalData.bDockingInputLoopedNumber
					LocalData.bDockingInputReceived = FALSE
				ENDIF
				
				
				IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_CONTEXT)
					LocalData.bDockingInputReceived = TRUE
					LocalData.bDockingInputLoopedNumber = LocalData.iDockingStagger
				ENDIF
				
				
				INT WhichPositiontoCheck = LocalData.iDockingStagger
				
				IF IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_DOCKING_VEHICLE)
					WhichPositiontoCheck = LocalData.VehicleDocking //already printed help for this one
				ENDIF
				
				GET_PRIVATE_YACHT_DOCKING_VEHICLE_LOCATION(WhichPositiontoCheck, iYachtID, vCoords, fHeading)
				
				IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vCoords, 
					gPrivateYachtDockingOffsets[WhichPositiontoCheck].fClearRadius, 
					gPrivateYachtDockingOffsets[WhichPositiontoCheck].fClearRadius,
					0.0,
					gPrivateYachtDockingOffsets[WhichPositiontoCheck].fClearRadius,
					FALSE,
					FALSE,
					FALSE)
					
					IF NOT AreVehiclesIncludingMissionOnesAtPoint(<<vCoords.x,vCoords.y, vCoords.z+1.5>> )
				
						vCoordFirstBasePoint = vCoords
								
						aPlayerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						canDockSEASHARK3 = TRUE
						isOnBoat = TRUE
						LocalData.VehicleDocking = WhichPositiontoCheck

					ENDIF

				ENDIF
				
			ELSE
		
				IF IS_LOCAL_PLAYER_IN_A_BOAT()	
					aPlayerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					GET_PRIVATE_YACHT_DOCKING_VEHICLE_LOCATION(LocalData.VehicleDocking, iYachtID, vCoords, fHeading)
					vCoordFirstBasePoint = vCoords
					aPlayerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					canDockSEASHARK3 = TRUE
					isOnBoat = TRUE
//							iVehicle = LocalData.VehicleDocking

				ENDIF
	
			ENDIF
	
		ENDIF

	
		SWITCH LocalData.DockingStages
			CASE 0
			
				IF bVehicleSpawningActive
				
					IF canDockSEASHARK3	



		

						vActivation1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords, fHeading, << 0.0, -10, -10.0>>)
						vActivation2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords, fHeading, << 0.0, 15, 20.0>>)
						vActivation3 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords, fHeading, << 0.0, 3, 20.0>>)

//						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - LOC vActivation1 = ",vActivation1, " ")
//						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - LOC vActivation2 = ",vActivation2, " ")
//						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - LOC vCoords = ",vCoords, " ")
//						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - LOC fHeading = ",fHeading, " ")
//						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - LOC PRIVATE_YACHT_WIDTH/2 = ",PRIVATE_YACHT_WIDTH/2.0, " ")

				
	
						IF IS_ENTITY_IN_ANGLED_AREA(aPlayerVeh, vActivation1 , vActivation2, PRIVATE_YACHT_WIDTH/2.0, FALSE, FALSE)
//						IF IS_ENTITY_AT_COORD(aPlayerVeh,vActivation1, <<7, 7, 7>> )

							IF IS_PRIVATE_YACHT_ACTIVE ( LOCAL_PLAYER_PRIVATE_YACHT_ID() )
							
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									IF NOT IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_DOCKING_VEHICLE)
										PRINT_HELP("YACHT_HELP_01")
										CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - PRINT_HELP for this docking location = ",LocalData.VehicleDocking, " ")
										SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_DOCKING_VEHICLE, TRUE)
									ENDIF
								ENDIF

								IF LocalData.bDockingInputReceived
									NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
									
									IF IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_DOCKING_VEHICLE)
										SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_DOCKING_VEHICLE, FALSE)
										IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("YACHT_HELP_01")
											CLEAR_HELP()
											CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - CLEAR_HELP as we're now docking in this docking location = ",LocalData.VehicleDocking, " ")

										ENDIF
									ENDIF
									
									
									
									////////////SCRIPT VERSION\\\\\\\\\\\\\\\\
									TargetVector = vCoordFirstBasePoint
									GET_ENTITY_DOCKING_POSITIONS(aPlayerVeh, TargetVector, fHeading, BoatRotation, fPlayerHeading, PlayerVehCoord)
									LocalData.DockingStages = 2
									RESET_NET_TIMER(LocalData.st_DockingCooldownTimer)
									
									IF GET_ENTITY_MODEL(aPlayerVeh) = SEASHARK3
										PLAY_SOUND_FROM_ENTITY(-1,"Moor_SEASHARK3_Engine",aPlayerVeh,"DLC_Apt_Yacht_Ambient_Soundset",TRUE,0)
									ELSE
										PLAY_SOUND_FROM_ENTITY(-1,"Moor_Boat_Engine",aPlayerVeh,"DLC_Apt_Yacht_Ambient_Soundset",TRUE,0)
									ENDIF
									
									////////////CODE VERSION\\\\\\\\\\\\\\\\
//									DockingMode = DF_DriveInReverse //Backwards								
//									fPlayerHeadingReduced =fPlayerHeading
//									IF fPlayerHeadingReduced > 180
//										fPlayerHeadingReduced -=360
//									ENDIF
//									fHeadingReduced =fHeading
//									IF fHeadingReduced > 180
//										fHeadingReduced-=360
//									ENDIF
//									HeadDiff = fHeadingReduced - fPlayerHeadingReduced
////									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - fHeadingReduced ",fHeadingReduced, " ")
////									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - fPlayerHeadingReduced ",fPlayerHeadingReduced, " ")
//									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - before adjustment HeadDiff ",HeadDiff, " ")
//									IF HeadDiff > 180
//										HeadDiff-=360
//									ENDIF
//									IF HeadDiff < -180
//										HeadDiff+=360
//									ENDIF
//									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - HeadDiff ",HeadDiff, " ")
//									IF (HeadDiff > 90 OR HeadDiff < -90)
//										DockingMode = DRIVINGMODE_PLOUGHTHROUGH //Forwards
//										CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - DockingMode = DRIVINGMODE_PLOUGHTHROUGH ", " ")
//									ELSE
//										CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - DockingMode = DF_DriveInReverse ", " ")
//									ENDIF
//									VECTOR DirectionVector 
//									DirectionVector.x = TargetVector.x-PlayerVehCoord.x
//									DirectionVector.y = TargetVector.y-PlayerVehCoord.y
//									DirectionVector.z = PlayerVehCoord.z
//									FLOAT fDirectionVector  
//									fDirectionVector = VMAG(DirectionVector)
//									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - DirectionVector ",DirectionVector, " ")
//									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - fDirectionVector ",fDirectionVector, " ")
//									FLOAT m
//									m = GET_DISTANCE_BETWEEN_COORDS(TargetVector, PlayerVehCoord)
//									float b
//									B = 0.5
//									Float x
//									x = 0.15
//									float speed
//									speed = m*x+b
//									
//									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - m ",m, " ")
//									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - speed ",speed, " ")
//									
//									
//									vActivation3 = vActivation3
//									vActivation3.z = PlayerVehCoord.z
//									
//									
//									
////									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - vActivation3 ",vActivation3, " ")
//									CLEAR_PED_TASKS(PLAYER_PED_ID())
//									TASK_BOAT_MISSION(PLAYER_PED_ID(),aPlayerVeh, NULL, NULL, vActivation3, MISSION_PARK_PERPENDICULAR,speed, DockingMode, 1.5, BCF_OPENOCEANSETTINGS  )
//									LocalData.DockingStages++


									
									CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - Parking started ", " ")
						
								ENDIF
							ENDIF
						ELSE
							IF IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_DOCKING_VEHICLE)
								SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_DOCKING_VEHICLE, FALSE)
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("YACHT_HELP_01")
									CLEAR_HELP()
//									CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - CLEAR_HELP as we're no longer in docking location = ",LocalData.VehicleDocking, " ")

								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_DOCKING_VEHICLE)
							SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_DOCKING_VEHICLE, FALSE)
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("YACHT_HELP_01")
								CLEAR_HELP()
//							CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - CLEAR_HELP as we aren't allowed to dock in location due to vehicle check = ",LocalData.VehicleDocking, " ")

							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_DOCKING_VEHICLE)
						SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_DOCKING_VEHICLE, FALSE)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("YACHT_HELP_01")
							CLEAR_HELP()
//							CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - CLEAR_HELP as we aren't allowed to dock in location due to inactive = ",LocalData.VehicleDocking, " ")

						ENDIF
					ENDIF
				ENDIF

			BREAK
			
			CASE 1
			
				GET_ENTITY_DOCKING_POSITIONS(aPlayerVeh, vCoordFirstBasePoint, fHeading, BoatRotation, fPlayerHeading, PlayerVehCoord)
				vActivation3 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords, fHeading, << 0.0, 3, 20.0>>)
				vActivation3 = vActivation3
				vActivation3.z = PlayerVehCoord.z
				
//				DRAW_DEBUG_SPHERE(vActivation3, 0.25, 255, 0, 255)
			
				TargetVector = vActivation3
				IF NOT IS_VECTOR_ZERO(TargetVector)
					IF IS_ENTITY_AT_COORD(aPlayerVeh, TargetVector, <<2.0, 2.0, 5.0>>)
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						
						DockingMode = DF_DriveInReverse //Backwards								
						fPlayerHeadingReduced =fPlayerHeading
						IF fPlayerHeadingReduced > 180
							fPlayerHeadingReduced -=360
						ENDIF
						fHeadingReduced =fHeading
						IF fHeadingReduced > 180
							fHeadingReduced-=360
						ENDIF
						HeadDiff = fHeadingReduced - fPlayerHeadingReduced
						IF HeadDiff > 180
							HeadDiff-=360
						ENDIF
						IF HeadDiff < -180
							HeadDiff+=360
						ENDIF
						CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - fHeadingReduced ",fHeadingReduced, " ")
						CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - fPlayerHeadingReduced ",fPlayerHeadingReduced, " ")
						CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - HeadDiff ",HeadDiff, " ")
						IF HeadDiff > 90 OR HeadDiff < -90
							DockingMode = DRIVINGMODE_PLOUGHTHROUGH //Forwards
							CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - DockingMode = DRIVINGMODE_PLOUGHTHROUGH refine ", " ")
						ELSE
							CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - DockingMode = DF_DriveInReverse refine ", " ")
						ENDIF
						TargetVector = vCoordFirstBasePoint

						TASK_BOAT_MISSION(PLAYER_PED_ID(),aPlayerVeh, NULL, NULL, TargetVector, MISSION_PARK_PERPENDICULAR,0.5, DockingMode, 1.5, BCF_OPENOCEANSETTINGS  )

						RESET_NET_TIMER(LocalData.st_DockingCooldownTimer)
						LocalData.DockingStages++
						CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - Parking refined started ", " ")
					ELSE
						
							
//							TASK_BOAT_MISSION(PLAYER_PED_ID(),aPlayerVeh, NULL, NULL, TargetVector, MISSION_PARK_PERPENDICULAR,5.0, DF_DriveInReverse, 1.0, BCF_OPENOCEANSETTINGS  )

							GET_ENTITY_DOCKING_POSITIONS(aPlayerVeh, TargetVector, fHeading, BoatRotation, fPlayerHeading, PlayerVehCoord)
							
//							RUN_ENTITY_BOAT_TILT(PlayerVehCoord)
//							SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerHeading)

//							SET_ENTITY_COORDS_NO_OFFSET(aPlayerVeh, PlayerVehCoord, DEFAULT, DEFAULT, FALSE)
//							SET_ENTITY_ROTATION(aPlayerVeh ,BoatRotation, DEFAULT, FALSE)
							
	
							IF HAS_NET_TIMER_EXPIRED(LocalData.st_DockingCooldownTimer, 200) //

								IF HAS_PLAYER_QUIT_DOCKING()
								OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_MISSION) = WAITING_TO_START_TASK
									CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - player broke out parking ", " ")
									NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
									CLEAR_PED_TASKS(PLAYER_PED_ID())
									LocalData.DockingStages = 0
									LocalData.VehicleDocking = 0
									RESET_NET_TIMER(LocalData.st_DockingCooldownTimer)
								ENDIF
							ENDIF
							
					ENDIF
				ELSE
					CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - LOC TargetVector = zero ")
				ENDIF
			
			BREAK
			
			CASE 2 //Apply docking
			
//				GET_ENTITY_DOCKING_POSITIONS(aPlayerVeh, vCoordFirstBasePoint, fHeading, BoatRotation, fPlayerHeading, PlayerVehCoord)
//				vActivation3 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords, fHeading, << 0.0, 3, 20.0>>)
//				vActivation3 = vActivation3
//				vActivation3.z = PlayerVehCoord.z
				
				TargetVector = vCoordFirstBasePoint
				
//				DRAW_DEBUG_SPHERE(TargetVector, 0.25, 255, 0, 0)
				
				IF NOT IS_VECTOR_ZERO(TargetVector)
					IF IS_ENTITY_AT_COORD(aPlayerVeh, TargetVector, <<1.0, 1.0, 5.0>>)
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForceIntoStandPoseOnJetski , FALSE)
						LocalData.DockingStages++
						CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - Parking force finished ", " ")
					ELSE
						

							
							
//							TASK_BOAT_MISSION(PLAYER_PED_ID(),aPlayerVeh, NULL, NULL, TargetVector, MISSION_PARK_PERPENDICULAR,5.0, DF_DriveInReverse, 1.0, BCF_OPENOCEANSETTINGS  )

							GET_ENTITY_DOCKING_POSITIONS(aPlayerVeh, TargetVector, fHeading, BoatRotation, fPlayerHeading, PlayerVehCoord)
							
//							RUN_ENTITY_BOAT_TILT(PlayerVehCoord)
//							SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerHeading)

							SET_ENTITY_COORDS_NO_OFFSET(aPlayerVeh, PlayerVehCoord, DEFAULT, DEFAULT, FALSE)
							SET_ENTITY_ROTATION(aPlayerVeh ,BoatRotation, DEFAULT, FALSE)
							SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForceIntoStandPoseOnJetski , TRUE)
							
							IF HAS_NET_TIMER_EXPIRED(LocalData.st_DockingCooldownTimer, 200) //

								IF HAS_PLAYER_QUIT_DOCKING()
									CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - player broke out parking 2 ", " ")
									NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
									CLEAR_PED_TASKS(PLAYER_PED_ID())
									LocalData.DockingStages = 0
									LocalData.VehicleDocking = 0
									RESET_NET_TIMER(LocalData.st_DockingCooldownTimer)
									SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForceIntoStandPoseOnJetski , FALSE)
								ENDIF
							ENDIF
							
					ENDIF
				ELSE
					CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - LOC TargetVector = zero ")
				ENDIF
			
			BREAK
			
			CASE 3	//Finish dock
				
//				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - running with vehicle ", LocalData.VehicleDocking, " ")
					
												
				CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - docking vehicle. ", LocalData.VehicleDocking, " vCoords = ", vCoords, " fHeading = ", fHeading, " " )
//				SET_ENTITY_COORDS(aPlayerVeh, vCoords)
//				SET_ENTITY_HEADING(aPlayerVeh, fHeading)
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

				
				// Anchor?
//				CAN_ANCHOR_BOAT_HERE 
				
				
				IF CAN_ANCHOR_BOAT_HERE(aPlayerVeh)
					IF NOT IS_POINT_IN_YACHT_BOUNDING_BOX(PlayerVehCoord, iYachtID)
						SET_BOAT_ANCHOR(aPlayerVeh, TRUE)	
						CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - setting anchor on vehicle. ", LocalData.VehicleDocking)
					ELSE
						CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - can't anchor as vehicle is in bounding box. ", LocalData.VehicleDocking)
					ENDIF
				ELSE
					CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - can't anchor on vehicle. ", LocalData.VehicleDocking)
				ENDIF

				
				// so it is visible when entering property.
//				SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(aPlayerVeh, TRUE, TRUE)
				
				// make sure engine is off
				SET_VEHICLE_ENGINE_ON(aPlayerVeh, FALSE, FALSE, TRUE)
				
//				SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PRIVATE_YACHT_VEHICLE_FLAG_HAS_TAKEN, FALSE)
				RESET_NET_TIMER(LocalData.st_DockingCooldownTimer)
				LocalData.DockingStages++
			
			BREAK
			
			CASE 4
			
//				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DOCKING - keeping vehicle in place ", LocalData.VehicleDocking, " ")
			
				IF isOnBoat
					TargetVector = vCoordFirstBasePoint
					GET_ENTITY_DOCKING_POSITIONS(aPlayerVeh, TargetVector,fHeading, BoatRotation, fPlayerHeading, PlayerVehCoord, TRUE, TRUE)
					SET_ENTITY_COORDS_NO_OFFSET(aPlayerVeh, PlayerVehCoord, DEFAULT, DEFAULT, FALSE)
					SET_ENTITY_ROTATION(aPlayerVeh ,BoatRotation, DEFAULT, FALSE)
				ENDIF
			
				
				IF HAS_NET_TIMER_EXPIRED(LocalData.st_DockingCooldownTimer, 200) //

					IF HAS_PLAYER_QUIT_DOCKING()
					OR NOT isOnBoat
					
	//					ANCHOR_ALL_BOATS_NEARBY_WITH_NOONE_ON_THEM()
						CPRINTLN(DEBUG_YACHT, "[YACHTDOCK] UPDATE_PRIVATE_YACHT_DOCKING - player broke out idling ", " ")
						LocalData.DockingStages = 0
						LocalData.VehicleDocking = 0
						RESET_NET_TIMER(LocalData.st_DockingCooldownTimer)
					ENDIF
				ENDIF

			BREAK
		
		ENDSWITCH
		
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_UPDATE_YACHT_SEATS()
	IF FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
		RETURN NOT IS_BIT_SET(g_FMMC_STRUCT.sMissionYachtInfo[ciYACHT_LOBBY_HOST_YACHT_INDEX].iMYacht_Bitset, ciYACHT_BITSET__BLOCK_SEATS)
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC UPDATE_YACHT_SEATS(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	
	IF NOT SHOULD_UPDATE_YACHT_SEATS()
		EXIT
	ENDIF
	
	VECTOR SeatPositionOffsets[NUM_SEAT_ON_YACHT]
	FLOAT SeatWidth = 0.45
	INT iYachtID = GET_YACHT_LOCAL_PLAYER_IS_ON()

	
	
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		SeatPositionOffsets[0] 	= <<2.8015, 18.4765, -2.4325>>
		SeatPositionOffsets[1] 	= <<-1.0390, -12.2494, -5.5186>>
		//doorway
		SeatPositionOffsets[2] 	= <<0.9362, -12.2872, -5.5186>>
		SeatPositionOffsets[3] 	= <<3.0035, -0.1245, -2.4325>>
		//Bedroom
		SeatPositionOffsets[4] 	= <<3.6890, 1.7864, -5.5195>>
		SeatPositionOffsets[5] 	= <<3.8266, 0.3847, -5.5195>>
		//Bar
		SeatPositionOffsets[6] 	= <<-3.5778, 14.5726, -2.4324>>
		SeatPositionOffsets[7] 	= <<3.1796, 17.1303, -2.4325>> 
		SeatPositionOffsets[8] 	= <<-2.9361, 18.7018, -2.4340>>
		SeatPositionOffsets[9] 	= <<3.2598, 14.4800, -2.4325>>
		SeatPositionOffsets[10] = <<-6.2295, 13.6637, -5.5218>>
		SeatPositionOffsets[11] = <<-6.1702, 15.6147, -5.5227>>
		SeatPositionOffsets[12] = <<-5.9192, 25.0510, -5.5217>>
		SeatPositionOffsets[13] = <<5.8320, 22.9855, -5.5193>>
		SeatPositionOffsets[14] = <<5.8458, 21.6857, -5.5193>>
		SeatPositionOffsets[15] = <<-2.9413, 0.0343, -2.4325>>

		//Offsets created by: Ship transform - world position in front of seat
		//Front
		SeatPositionOffsets[16] =<< 52.27,-18.48,2.14028>> 	//<<	-1512.81,	-3742.99,	7.57023	>>
		SeatPositionOffsets[17] =<< 52.83,-19.52,2.14028>> 	//<<	-1513.37,	-3741.95,	7.57023	>>
		//Top							  		 							
		SeatPositionOffsets[18] =<< -6.66,-3.87,5.47745	>> 	//<<	-1453.88,	-3757.6	,	10.9074	>>
		SeatPositionOffsets[19] =<< -4.33,3.24,	5.47745	>> 	//<<	-1456.21,	-3764.71,	10.9074	>>
		SeatPositionOffsets[20] =<< -8.05,-3.44,5.47745	>> 	//<<	-1452.49,	-3758.03,	10.9074	>>
		SeatPositionOffsets[21] =<< -5.72,3.75,	5.47745	>> 	//<<	-1454.82,	-3765.22,	10.9074	>>
		SeatPositionOffsets[22] =<< -9.69,-0.17,5.47745	>> 	//<<	-1450.85,	-3761.3	,	10.9074	>>
		SeatPositionOffsets[23] =<< -8.81,1.95,	5.47745	>> 	//<<	-1451.73,	-3763.42,	10.9074	>>
		//Back				   			  		 							
		SeatPositionOffsets[24] =<<-36.05,4.82 ,2.54165	>> 	//<<	-1424.49,	-3766.29,	7.9716	>>
		SeatPositionOffsets[25] =<<-36.42,6.20 ,2.54165>> 	//<<	-1424.12,	-3767.67,	7.9716	>>									   
		SeatPositionOffsets[26] =<<-32.15,14.02 ,2.54165>> 	//<<	-1428.39,	-3775.49,	7.9716	>>
		SeatPositionOffsets[27] =<<-33.39,13.39 ,2.54165>> 	//<<	-1427.15,	-3774.86,	7.9716	>>
		
		//boat 14 coords = <<-1460.54,-3761.47,5.42995>>
		
		
		
		INT iSeat
		IF LocalData.iSeatStagger = LocalData.bSeatInputLoopedNumber
			LocalData.bSeatInputReceived = FALSE
		ENDIF

		IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_CONTEXT)
			LocalData.bSeatInputReceived = TRUE
			LocalData.bSeatInputLoopedNumber = LocalData.iSeatStagger
		ENDIF
		
		VECTOR CurrentSeatPos
		iSeat = LocalData.iSeatStagger
		
		
		
		IF NOT IS_VECTOR_ZERO(SeatPositionOffsets[iSeat])
			if iSeat <16 //interior seats
				CurrentSeatPos = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, SeatPositionOffsets[iSeat])				
			ELSE //exterior seats
				SeatWidth = PRIVATE_YACHT_SEAT_WIDTH
				CurrentSeatPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS ( GET_MODEL_COORDS_OF_YACHT(iYachtID), (g_PrivateYachtDetails[iYachtID].fModelHeading+PRIVATE_YACHT_MODLE_HEADING_OFFSET), SeatPositionOffsets[iSeat] )				
			ENDIF	
			CurrentSeatPos.z += 1
			IF NOT PED_HAS_USE_SCENARIO_TASK(PLAYER_PED_ID())
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), CurrentSeatPos, <<SeatWidth, SeatWidth, SeatWidth>>)
					IF DOES_SCENARIO_EXIST_IN_AREA(CurrentSeatPos, 2.0, TRUE)	
					
						IF NOT IS_SCENARIO_OCCUPIED(CurrentSeatPos, 2.0, TRUE)
							IF IS_BIT_SET(LocalData.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_STAND)
								CLEAR_BIT(LocalData.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_STAND)
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_STAND")
									CLEAR_HELP()
								ENDIF
							ENDIF
						
							IF iSeat = 0
							AND g_B_Is_Interior_stripper_Active_this_frame
								PRINTLN("[YACHTSEAT] UPDATE_YACHT_SEATS: Stripper is in room now, don't get in her seat ")
								EXIT
							ENDIF
						
							IF NOT IS_BIT_SET(LocalData.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_SIT)
								SET_BIT(LocalData.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_SIT)
								PRINT_HELP("MPTV_WALK")
							ENDIF
							
//								IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_CONTEXT)
							IF LocalData.bSeatInputReceived	
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseGoToPointForScenarioNavigation, TRUE)
								TASK_USE_NEAREST_SCENARIO_TO_COORD(PLAYER_PED_ID(), CurrentSeatPos, 2.0, 5000)
								PRINTLN("[YACHTSEAT] UPDATE_YACHT_SEATS: TASK_USE_NEAREST_SCENARIO_TO_COORD CurrentSeatPos = ", CurrentSeatPos, " ")
								LocalData.bSeatInputReceived = FALSE
								LocalData.bSeatInputLoopedNumber = -1
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_WALK")
									CLEAR_HELP()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(LocalData.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_SIT)
						CLEAR_BIT(LocalData.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_SIT)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_WALK")
							CLEAR_HELP()
						ENDIF
					ENDIF
					IF IS_BIT_SET(LocalData.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_STAND)
						CLEAR_BIT(LocalData.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_STAND)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_STAND")
							CLEAR_HELP()
						ENDIF
					ENDIF
				ENDIF
			ELSE
			
				IF IS_BIT_SET(LocalData.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_SIT)
					CLEAR_BIT(LocalData.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_SIT)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_WALK")
						CLEAR_HELP()
					ENDIF
				ENDIF
				
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			
				IF IS_SCENARIO_OCCUPIED(CurrentSeatPos, 2.0, TRUE)
					IF NOT IS_BIT_SET(LocalData.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_STAND)
						SET_BIT(LocalData.bSeatHelpTextBitset[iSeat], YACHT_HELP_TEXT_SEAT_STAND)
						PRINT_HELP("MPTV_STAND")
					ENDIF
	
					IF iSeat = 0
					AND g_B_Is_Interior_stripper_Active_this_frame
						PRINTLN("[YACHTSEAT] UPDATE_YACHT_SEATS: Stripper is in room now, get up out her seat ")
						LocalData.bSeatInputReceived = TRUE
					ENDIF
	
					IF LocalData.bSeatInputReceived
						SET_PED_SHOULD_IGNORE_SCENARIO_EXIT_COLLISION_CHECKS(PLAYER_PED_ID(), TRUE)
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						PRINTLN("[YACHTSEAT] UPDATE_YACHT_SEATS: CLEAR_PED_TASKS ")
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseGoToPointForScenarioNavigation, FALSE)
						LocalData.bSeatInputReceived = FALSE
						LocalData.bSeatInputLoopedNumber = -1
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_STAND")
							CLEAR_HELP()
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	
	ENDIF
	

ENDPROC


PROC RUN_BUOYS_ON_WATER(PRIVATE_YACHT_LOCAL_DATA &LocalData )
	
	INT I
	VECTOR Targetvector 
	VECTOR ReturnRotation
	VECTOR ReturnPosition

	INT iYachtID = GET_YACHT_PLAYER_IS_NEAR(PLAYER_ID())
	
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
	
		IF (IS_PLAYER_ON_YACHT(PLAYER_ID(), iYachtID) OR IS_PLAYER_NEAR_YACHT(PLAYER_ID(), iYachtID, 300))
		AND NOT ARE_YACHT_BUOYS_DISABLED()
			IF (HAS_YACHT_MODEL_LOADED() )
				CREATE_BUOYS(iYachtID, LocalData)
			ENDIF
		ELSE
			REMOVE_BUOYS(iYachtID, LocalData)
		ENDIF

		IF NOT ARE_YACHT_BUOYS_DISABLED()
			FOR I = 0 TO NUMBER_OF_PRIVATE_YACHT_BUOY-1
				IF DOES_ENTITY_EXIST(LocalData.oBouy[i])
					Targetvector = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(LocalData.bYachtWithBuoys, GET_BUOY_POSITION_OFFSET_TO_YACHTS(I))
	//				IF IS_ENTITY_AT_COORD(LocalData.oBouy[I], Targetvector, <<0.25, 0.25, 2.0>>)
	//					GET_ENTITY_BUOY_POSITIONS(LocalData.oBouy[I], Targetvector, ReturnRotation, ReturnPosition, TRUE )
	//				ELSE
						GET_ENTITY_BUOY_POSITIONS(LocalData.oBouy[I], Targetvector, ReturnRotation, ReturnPosition, FALSE, 1 )
	//				ENDIF
					ReturnPosition.z -= 0.3
					SET_ENTITY_COORDS_NO_OFFSET(LocalData.oBouy[I], ReturnPosition, DEFAULT, DEFAULT, FALSE)
					SET_ENTITY_ROTATION(LocalData.oBouy[I] ,ReturnRotation, DEFAULT, FALSE)
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
		
ENDPROC

PROC FIRE_WARNING_SHOT(SCRIPT_TIMER& atimer, INT iSphereIndex, INT iYachtID, BOOL& HasRequestedToLoaded)
	
	REQUEST_NAMED_PTFX_ASSET("scr_apartment_mp")
	HasRequestedToLoaded = TRUE
	IF HAS_NAMED_PTFX_ASSET_LOADED("scr_apartment_mp")			
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(atimer, 4000)
			INT RandomX = GET_RANDOM_INT_IN_RANGE(-20, 20)
			INT Randomy = GET_RANDOM_INT_IN_RANGE(20, 40) //always in front
			INT Randomz = GET_RANDOM_INT_IN_RANGE(-10, 20)
			VECTOR OffsetToPlayer = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<RandomX, Randomy, Randomz>>)
			
			IF IS_PLAYER_USING_DRONE(PLAYER_ID()) AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE
				OffsetToPlayer = GET_ENTITY_COORDS(GET_LOCAL_DRONE_PROP_OBJ_INDEX(), FALSE)
			ENDIF
			
			FIRE_AIR_DEFENCE_SPHERE_WEAPON_AT_POSITION(iSphereIndex, OffsetToPlayer)
//			USE_PARTICLE_FX_ASSET("scr_apartment_mp")	
			BROADCAST_PRIVATE_YACHT_AIRDEF_EXPLOSION(OffsetToPlayer, OffsetToPlayer, iSphereIndex, iYachtID) //Get gun coord. 
//			START_PARTICLE_FX_NON_LOOPED_AT_COORD("exp_yacht_defence_plane", OffsetToPlayer, <<0,0,0>>)
//			PLAY_SOUND_FROM_COORD(-1,"Missile_Defence_Near_Miss",OffsetToPlayer,"DLC_Apt_Yacht_Ambient_Soundset",FALSE)
			CPRINTLN(DEBUG_YACHT, "[AIRDEF] FIRE_WARNING_SHOT - Fire warning shot iSphereIndex ", iSphereIndex, " iYachtID = ", iYachtID, " ")
			
		ENDIF
	ENDIF		

ENDPROC

PROC FIRE_WARNING_SHOT_DIRECT(VECTOR& ExplosionCoord, VECTOR& GunCoord, INT& iSphereIndex, BOOL& HasRequestedToLoaded)
	
	REQUEST_NAMED_PTFX_ASSET("scr_apartment_mp")
	HasRequestedToLoaded = TRUE
	IF HAS_NAMED_PTFX_ASSET_LOADED("scr_apartment_mp")			
//		USE_PARTICLE_FX_ASSET("scr_apartment_mp")			
		IF GunCoord.x = 0
		ENDIF
		FIRE_AIR_DEFENCE_SPHERE_WEAPON_AT_POSITION(iSphereIndex, ExplosionCoord)
//		PLAY_SOUND_FROM_COORD(-1,"Missile_Defence_Near_Miss",ExplosionCoord,"DLC_Apt_Yacht_Ambient_Soundset",FALSE)
//		START_PARTICLE_FX_NON_LOOPED_AT_COORD("exp_yacht_defence_plane", ExplosionCoord, <<0,0,0>>)
		CPRINTLN(DEBUG_YACHT, "[AIRDEF] FIRE_WARNING_SHOT_DIRECT - Fire warning shot at ", ExplosionCoord," iSphereIndex = ", iSphereIndex, " " )
		
		ExplosionCoord = <<0,0,0>>
		GunCoord = <<0,0,0>>
		iSphereIndex = 0
	ENDIF		

ENDPROC

FUNC BOOL IS_THIS_MODEL_A_RESTRICTED_HELI(MODEL_NAMES eModel)
	IF g_bBypassYachtDefencesForSparrow
		IF eModel = SEASPARROW2
		OR eModel = SEASPARROW3
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN IS_THIS_MODEL_A_HELI(eModel)
ENDFUNC

FUNC BOOL IS_PED_IN_ANY_RESTRICTED_HELI(PED_INDEX pPed)
	IF g_bBypassYachtDefencesForSparrow
		IF IS_PED_IN_ANY_VEHICLE(pPed)
			IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(pPed), SEASPARROW2)
			OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(pPed), SEASPARROW3)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN IS_PED_IN_ANY_HELI(pPed)
ENDFUNC

FUNC BOOL IS_PED_IN_ANY_RESTRICTED_VEHICLE(PED_INDEX pPed)
	IF IS_PED_IN_ANY_VEHICLE(pPed)
		IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(pPed), DELUXO)
		OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(pPed), OPPRESSOR)
		OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(pPed), OPPRESSOR2)
		OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(pPed), VOLTIC2)
		OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(pPed), VIGILANTE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_AREA_OF_YACHT_AIR_DEFENCES_BY_OWNER(PRIVATE_YACHT_LOCAL_DATA &LocalData, PLAYER_INDEX YachtOwner, INT Radius)

	IF YachtOwner != INVALID_PLAYER_INDEX()
		INT iYachtID = GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(YachtOwner)
		IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
			IF LocalData.bCreatedAirDefence[iYachtID]
				IF DOES_PLAYER_OWN_PRIVATE_YACHT(YachtOwner)
					IF GET_PLAYER_BD_YACHT_DEFENCE_SETTING(YachtOwner) != 0
						IF NATIVE_TO_INT(PLAYER_ID()) > -1
							IF LocalData.bAirDefence_WillAttackMe[iYachtID] 
								VECTOR YachtPosition = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, <<0,0,0>> )
								IF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
								OR IS_PED_IN_ANY_RESTRICTED_HELI(PLAYER_PED_ID())
								OR IS_PED_IN_ANY_RESTRICTED_VEHICLE(PLAYER_PED_ID())
								OR IS_PLAYER_USING_DRONE(PLAYER_ID()) AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE
									IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), YachtPosition, <<Radius, Radius, Radius>>, FALSE, TRUE, TM_ANY)
									OR (IS_ENTITY_AT_COORD(GET_LOCAL_DRONE_PROP_OBJ_INDEX(), YachtPosition, <<Radius, Radius, Radius>>, FALSE, TRUE, TM_ANY) AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE)
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC



FUNC BOOL IS_LOCAL_PLAYER_A_PASSENGER_OF_YACHT_AIR_DEFENCES_BY_OWNER(PRIVATE_YACHT_LOCAL_DATA &LocalData, PLAYER_INDEX YachtOwner, INT Radius)

	IF YachtOwner != INVALID_PLAYER_INDEX()
		INT iYachtID = GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(YachtOwner)
		IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
			IF LocalData.bCreatedAirDefence[iYachtID]
				IF DOES_PLAYER_OWN_PRIVATE_YACHT(YachtOwner)
					IF GET_PLAYER_BD_YACHT_DEFENCE_SETTING(YachtOwner) != 0
						IF NATIVE_TO_INT(PLAYER_ID()) > -1
							IF LocalData.bAirDefence_WillAttackMe[iYachtID] = FALSE
								VECTOR YachtPosition = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, <<0,0,0>> )
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), YachtPosition, <<Radius, Radius, Radius>>, FALSE, TRUE, TM_ANY)
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


FUNC BOOL IS_LOCAL_PLAYER_A_PASSENGER_OF_YACHT_AIR_DEFENCES_BY_ID(PRIVATE_YACHT_LOCAL_DATA &LocalData, INT iYachtID, INT Radius, BOOL bSkipAttackCheck = FALSE)

	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		IF LocalData.bCreatedAirDefence[iYachtID]
			PLAYER_INDEX YachtOwner = GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)		
			IF YachtOwner != INVALID_PLAYER_INDEX()
				IF NATIVE_TO_INT(PLAYER_ID()) > -1
					IF DOES_PLAYER_OWN_PRIVATE_YACHT(YachtOwner)
						IF GET_PLAYER_BD_YACHT_DEFENCE_SETTING(YachtOwner) != 0
							IF NATIVE_TO_INT(PLAYER_ID()) > -1
								IF LocalData.bAirDefence_WillAttackMe[iYachtID] = FALSE
								OR bSkipAttackCheck
									VECTOR YachtPosition = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, <<0,0,0>> )
									IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), YachtPosition, <<Radius, Radius, Radius>>, FALSE, TRUE, TM_ANY)
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_AREA_OF_YACHT_AIR_DEFENCES_BY_ID(PRIVATE_YACHT_LOCAL_DATA &LocalData, INT iYachtID, INT Radius)

	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		IF LocalData.bCreatedAirDefence[iYachtID]
			PLAYER_INDEX YachtOwner = GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)		
			IF YachtOwner != INVALID_PLAYER_INDEX()
				IF NATIVE_TO_INT(PLAYER_ID()) > -1
					IF DOES_PLAYER_OWN_PRIVATE_YACHT(YachtOwner)
						IF GET_PLAYER_BD_YACHT_DEFENCE_SETTING(YachtOwner) != 0		
							IF LocalData.bAirDefence_WillAttackMe[iYachtID] 
								VECTOR YachtPosition = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, <<0,0,0>> )
								IF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
								OR IS_PED_IN_ANY_RESTRICTED_HELI(PLAYER_PED_ID())
								OR IS_PED_IN_ANY_RESTRICTED_VEHICLE(PLAYER_PED_ID())
								OR IS_PLAYER_USING_DRONE(PLAYER_ID()) AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE
//									VEHICLE_INDEX aVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//									IF (GET_PED_IN_VEHICLE_SEAT(aVeh, VS_DRIVER) = PLAYER_PED_ID())
										IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), YachtPosition, <<Radius, Radius, Radius>>, FALSE, TRUE, TM_ANY)
										OR (DOES_ENTITY_EXIST(GET_LOCAL_DRONE_PROP_OBJ_INDEX()) AND (IS_ENTITY_AT_COORD(GET_LOCAL_DRONE_PROP_OBJ_INDEX(), YachtPosition, <<Radius, Radius, Radius>>, FALSE, TRUE, TM_ANY) AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE))
											RETURN TRUE
										ENDIF
//									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC




FUNC BOOL IS_LOCAL_PLAYER_YACHT_AIR_DEFENCES_ACTIVE_BY_ID(PRIVATE_YACHT_LOCAL_DATA &LocalData, INT iYachtID)

	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		IF LocalData.bCreatedAirDefence[iYachtID]
			PLAYER_INDEX YachtOwner = GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)		
			IF YachtOwner != INVALID_PLAYER_INDEX()
				IF NATIVE_TO_INT(PLAYER_ID()) > -1
					IF DOES_PLAYER_OWN_PRIVATE_YACHT(YachtOwner)
						IF GET_PLAYER_BD_YACHT_DEFENCE_SETTING(YachtOwner) != 0			
							IF LocalData.bAirDefence_WillAttackMe[iYachtID] 
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
									VEHICLE_INDEX aVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
									IF DOES_ENTITY_EXIST(aVeh)
										MODEL_NAMES aVehModel = GET_ENTITY_MODEL(aVeh)
										IF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
										OR IS_PED_IN_ANY_RESTRICTED_HELI(PLAYER_PED_ID())
										OR IS_PED_IN_ANY_RESTRICTED_VEHICLE(PLAYER_PED_ID())
										OR IS_THIS_MODEL_A_PLANE(aVehModel)
										OR IS_THIS_MODEL_A_RESTRICTED_HELI(aVehModel)
										OR IS_VEHICLE_MODEL(aVeh, DELUXO)
										OR IS_VEHICLE_MODEL(aVeh, OPPRESSOR)
										OR IS_VEHICLE_MODEL(aVeh, OPPRESSOR2)
										OR IS_VEHICLE_MODEL(aVeh, VOLTIC2)
										OR IS_VEHICLE_MODEL(aVeh, VIGILANTE)
										OR IS_PLAYER_USING_DRONE(PLAYER_ID()) AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE
											RETURN TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC



FUNC BOOL IS_PLAYER_IN_AREA_OF_YACHT_AIR_DEFENCES_BY_ID(PRIVATE_YACHT_LOCAL_DATA &LocalData, PLAYER_INDEX PlayerInRange, INT iYachtID, INT Radius)

	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		IF LocalData.bCreatedAirDefence[iYachtID]
			PLAYER_INDEX YachtOwner = GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)	
			PED_INDEX aPed = GET_PLAYER_PED(PlayerInRange)
			IF YachtOwner != INVALID_PLAYER_INDEX()
				IF NATIVE_TO_INT(PlayerInRange) > -1
					IF DOES_PLAYER_OWN_PRIVATE_YACHT(YachtOwner)
						IF GET_PLAYER_BD_YACHT_DEFENCE_SETTING(YachtOwner) != 0
							IF LocalData.bAirDefence_WillAttackMe[iYachtID] 
								VECTOR YachtPosition = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, <<0,0,0>> )
								IF DOES_ENTITY_EXIST(aPed)
									IF IS_PED_IN_ANY_PLANE(aPed)
									OR IS_PED_IN_ANY_RESTRICTED_HELI(aPed)
									OR IS_PED_IN_ANY_RESTRICTED_VEHICLE(aPed)
									OR IS_PLAYER_USING_DRONE(PlayerInRange) AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PlayerInRange)].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE
	//									VEHICLE_INDEX aVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	//									IF (GET_PED_IN_VEHICLE_SEAT(aVeh, VS_DRIVER) = PLAYER_PED_ID())
											IF IS_ENTITY_AT_COORD(aPed, YachtPosition, <<Radius, Radius, Radius>>, FALSE, TRUE, TM_ANY)
											OR IS_ENTITY_AT_COORD(GlobalplayerBD_FM_4[NATIVE_TO_INT(PlayerInRange)].objID_remoteDrone[NATIVE_TO_INT(PlayerInRange)], YachtPosition, <<Radius, Radius, Radius>>, FALSE, TRUE, TM_ANY)
												RETURN TRUE
											ENDIF
	//									ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_PLAYER_OWN_YACHT_AND_ON_YACHT_ATTACK(PLAYER_INDEX aPlayer)
	
	IF aPlayer != INVALID_PLAYER_INDEX()
		IF DOES_PLAYER_OWN_PRIVATE_YACHT(aPlayer)
		AND GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(aPlayer)
		AND GB_GET_LOCAL_PLAYER_MISSION_HOST()= aPlayer
		AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(aPlayer) = FMMC_TYPE_GB_YACHT_ROBBERY
			RETURN TRUE
		ENDIF	
	ENDIF
	RETURN FALSE

ENDFUNC



PROC SETUP_CAPTAIN_DIALOGUE(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	
	IF NOT IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[LocalData.iClientYachtStagger ], YACHT_DIALOGUE_AIR_DEF_CAPTAIN_SETUP)

		PRINTLN("[AIRDEFDIA] SETUP_CAPTAIN_DIALOGUE called. ")
	//	ADD_PED_FOR_DIALOGUE(LocalData.yachtDialogueStruct.captainConversation, ConvertSingleCharacter("C"), NULL, "YACHTCAPTAIN")
		ADD_PED_FOR_DIALOGUE(LocalData.yachtDialogueStruct.captainConversation, 2, NULL, "YACHTCAPTAIN")
	ENDIF
ENDPROC

//purpose: Set the g_bUse_MP_DLC_Dialogue before running a new conversation
PROC SETUP_DIALOGUE_FLAG(PRIVATE_YACHT_LOCAL_DATA &LocalData)

	LocalData.bDialogueSwitchedtoDLC = !g_bUse_MP_DLC_Dialogue
		
	IF g_bUse_MP_DLC_Dialogue = FALSE
		g_bUse_MP_DLC_Dialogue = TRUE
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[AIRDEFDIA] SETUP_DIALOGUE g_bUse_MP_DLC_Dialogue = TRUE ")
	ENDIF
	
ENDPROC

PROC TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		SETUP_CAPTAIN_DIALOGUE(LocalData)
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE called. ")
		
		SETUP_DIALOGUE_FLAG(LocalData)
		
		CREATE_CONVERSATION(LocalData.yachtDialogueStruct.captainConversation, "APAIR", "APAIR_ACTIVE", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
	ELSE
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE blocked. ")
	ENDIF
ENDPROC

PROC TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_THIRD(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_THIRD called. ")
		
		SETUP_DIALOGUE_FLAG(LocalData)
		
		CREATE_CONVERSATION(LocalData.yachtDialogueStruct.captainConversation, "APAIR", "APAIR_THIRD", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)

	ELSE
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_THIRD blocked. ")
	ENDIF
ENDPROC

PROC TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_ATTACK(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_ATTACK called. ")
		
		SETUP_DIALOGUE_FLAG(LocalData)
		
		CREATE_CONVERSATION(LocalData.yachtDialogueStruct.captainConversation, "APAIR", "APAIR_ATTACK", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
	ELSE
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_ATTACK blocked. ")
	ENDIF
ENDPROC

PROC TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_CONT(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_CONT called. ")
		
		SETUP_DIALOGUE_FLAG(LocalData)
		
		CREATE_CONVERSATION(LocalData.yachtDialogueStruct.captainConversation, "APAIR", "APAIR_CONT", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
		
	ELSE
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_CONT blocked. ")
	ENDIF
ENDPROC

PROC TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_GENERAL(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_GENERAL called. ")
		
		SETUP_DIALOGUE_FLAG(LocalData)
		
		CREATE_CONVERSATION(LocalData.yachtDialogueStruct.captainConversation, "APAIR", "APAIR_GEN", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
	ELSE
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_GENERAL blocked. ")
	ENDIF
ENDPROC



PROC TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_INTRO_PASSENGERS(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		SETUP_CAPTAIN_DIALOGUE(LocalData)
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_INTRO_PASSENGERS called. ")
		
		SETUP_DIALOGUE_FLAG(LocalData)
		
		CREATE_CONVERSATION(LocalData.yachtDialogueStruct.captainConversation, "APAIR", "APAIR_INTRO", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
	ELSE
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_INTRO_PASSENGERS blocked. ")
	ENDIF
ENDPROC

PROC TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_ANNOUNCE_PASSENGERS(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_ANNOUNCE_PASSENGERS called. ")
		
		SETUP_DIALOGUE_FLAG(LocalData)
		
		CREATE_CONVERSATION(LocalData.yachtDialogueStruct.captainConversation, "APAIR", "APAIR_ANNOU", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
	ELSE
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_ANNOUNCE_PASSENGERS blocked. ")
	ENDIF
ENDPROC

PROC TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_ANNOUNCE_ATTACK_PASSENGERS(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_ANNOUNCE_ATTACK_PASSENGERS called. ")
			
		SETUP_DIALOGUE_FLAG(LocalData)
		
		CREATE_CONVERSATION(LocalData.yachtDialogueStruct.captainConversation, "APAIR", "APAIR_ANNOU2", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
	ELSE
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_ANNOUNCE_ATTACK_PASSENGERS blocked. ")
	ENDIF
ENDPROC

PROC TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_AGGRES(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_AGGRES called. ")
		
		SETUP_DIALOGUE_FLAG(LocalData)
		
		CREATE_CONVERSATION(LocalData.yachtDialogueStruct.captainConversation, "APAIR", "APAIR_AGGRES", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)
		
		
	ELSE
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_AGGRES blocked. ")
	ENDIF
ENDPROC

PROC TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_GENTLE(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_GENTLE called. ")
		
		SETUP_DIALOGUE_FLAG(LocalData)
		
		CREATE_CONVERSATION(LocalData.yachtDialogueStruct.captainConversation, "APAIR", "APAIR_GENTLE", CONV_PRIORITY_AMBIENT_HIGH, DISPLAY_SUBTITLES)

	ELSE
		PRINTLN("[AIRDEFDIA] TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_GENTLE blocked. ")
	ENDIF

ENDPROC




PROC RUN_AIR_DEFENCE_RELATIONSHIPS(PRIVATE_YACHT_LOCAL_DATA &LocalData)

	INT iYachtIDLoop
	
	INT MyPlayerNum = NATIVE_TO_INT(PLAYER_ID())
	PLAYER_INDEX OwnerPlayerIndex
	GAMER_HANDLE GamerHandle
	BOOL WillAirDefenceAttackPlayer
	INT YachtDefenceSetting
	

	
	FOR iYachtIDLoop = 0 TO NUMBER_OF_PRIVATE_YACHTS-1
	
		OwnerPlayerIndex = GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtIDLoop)		
			
		IF OwnerPlayerIndex != INVALID_PLAYER_INDEX()
			LocalData.bAirDefence_WillAttackMe[iYachtIDLoop] = FALSE
			
			IF LocalData.bCreatedAirDefence[iYachtIDLoop]	
		
				WillAirDefenceAttackPlayer = TRUE
				LocalData.bAirDefence_WillAttackMe[iYachtIDLoop] = TRUE
			
				YachtDefenceSetting = GET_PLAYER_BD_YACHT_DEFENCE_SETTING(OwnerPlayerIndex)
			
				IF LocalData.bAirDefence_Setting_LastFrame[iYachtIDLoop] != YachtDefenceSetting
					REINIT_NET_TIMER(LocalData.st_AirDefenceWarmUpTimer[iYachtIDLoop])
					
					
					CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS Owner has changed their setting, restart the timer. me = ", GET_PLAYER_NAME(PLAYER_ID()), " OwnerPlayerIndex = ", GET_PLAYER_NAME(OwnerPlayerIndex), " ")
				ENDIF
				LocalData.bAirDefence_Setting_LastFrame[iYachtIDLoop] = YachtDefenceSetting
				
			
				IF YachtDefenceSetting = 1  //ON
				OR YachtDefenceSetting = 8	//ON AND NO ONE IS EXCLUDED
					
					//Me vs eveyone else
					IF NETWORK_IS_PLAYER_ACTIVE(PLAYER_ID())
					AND NETWORK_IS_PLAYER_ACTIVE(OwnerPlayerIndex)
						IF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID()) 
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
						ENDIF
						
						IF OwnerPlayerIndex = PLAYER_ID()
							WillAirDefenceAttackPlayer = FALSE
							
							#IF IS_DEBUG_BUILD
							IF LocalData.bAirDefence_WillAttackMe_LastFrame[iYachtIDLoop] != WillAirDefenceAttackPlayer
								CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS - ON - My Yacht! Don't attack ", GET_PLAYER_NAME(OwnerPlayerIndex), " " )
							ENDIF
							#ENDIF
						ELIF ARE_PLAYER_IN_SAME_VEHICLE(OwnerPlayerIndex, PLAYER_ID())
							WillAirDefenceAttackPlayer = FALSE
							
							RESET_NET_TIMER(LocalData.st_AirDefenceWarmUpTimer[iYachtIDLoop]) //for when the owner jumps out 2596361
							
							#IF IS_DEBUG_BUILD
							IF LocalData.bAirDefence_WillAttackMe_LastFrame[iYachtIDLoop] != WillAirDefenceAttackPlayer
								CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS - ON - This player is a passenger, don't attack me = ", GET_PLAYER_NAME(PLAYER_ID()), " OwnerPlayerIndex = ", GET_PLAYER_NAME(OwnerPlayerIndex), " ")
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
					
				ELIF YachtDefenceSetting >= 2 //Off for friends
				
					IF NETWORK_IS_PLAYER_ACTIVE(PLAYER_ID())
					AND NETWORK_IS_PLAYER_ACTIVE(OwnerPlayerIndex)
						IF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID()) 
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
						ENDIF
						
						GamerHandle = GET_GAMER_HANDLE_PLAYER(OwnerPlayerIndex)
						
						IF OwnerPlayerIndex = PLAYER_ID()
							WillAirDefenceAttackPlayer = FALSE
						
							#IF IS_DEBUG_BUILD
							IF LocalData.bAirDefence_WillAttackMe_LastFrame[iYachtIDLoop] != WillAirDefenceAttackPlayer
								CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS - OFF FOR FRIENDS - My Yacht! Don't attack ", GET_PLAYER_NAME(OwnerPlayerIndex), " " )
							ENDIF
							#ENDIF
						ELIF ARE_PLAYER_IN_SAME_VEHICLE(OwnerPlayerIndex, PLAYER_ID())
							WillAirDefenceAttackPlayer = FALSE
						
							RESET_NET_TIMER(LocalData.st_AirDefenceWarmUpTimer[iYachtIDLoop]) //for when the owner jumps out 2596361
							
							#IF IS_DEBUG_BUILD
							IF LocalData.bAirDefence_WillAttackMe_LastFrame[iYachtIDLoop] != WillAirDefenceAttackPlayer
								CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS - OFF FOR FRIENDS - This player is a passenger, don't attack  me = ", GET_PLAYER_NAME(PLAYER_ID()), " OwnerPlayerIndex = ", GET_PLAYER_NAME(OwnerPlayerIndex), " ")
							ENDIF
							#ENDIF
						ELIF YachtDefenceSetting = 2 AND NETWORK_IS_FRIEND(GamerHandle) AND NOT IS_PLAYER_SCTV(OwnerPlayerIndex)
							
							WillAirDefenceAttackPlayer = FALSE
							RESET_NET_TIMER(LocalData.st_AirDefenceWarmUpTimer[iYachtIDLoop])
							
							#IF IS_DEBUG_BUILD
							IF LocalData.bAirDefence_WillAttackMe_LastFrame[iYachtIDLoop] != WillAirDefenceAttackPlayer
							CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS - OFF FOR FRIENDS - I'm a friend of this yacht owner, don't attack me = ", GET_PLAYER_NAME(PLAYER_ID()), " OwnerPlayerIndex = ", GET_PLAYER_NAME(OwnerPlayerIndex), " ")
							ENDIF
							#ENDIF
						ELIF YachtDefenceSetting >= 3 AND NOT IS_PLAYER_SCTV(OwnerPlayerIndex)
							
							IF YachtDefenceSetting = 7 //Off for Associates
								IF NETWORK_IS_FRIEND(GamerHandle)
								OR ARE_PLAYERS_ON_SAME_ACTIVE_CLAN(PLAYER_ID(), OwnerPlayerIndex)
								OR GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), OwnerPlayerIndex) 
									WillAirDefenceAttackPlayer = FALSE
									RESET_NET_TIMER(LocalData.st_AirDefenceWarmUpTimer[iYachtIDLoop])
									
									#IF IS_DEBUG_BUILD
									IF LocalData.bAirDefence_WillAttackMe_LastFrame[iYachtIDLoop] != WillAirDefenceAttackPlayer
									CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS - OFF FOR Associates - I'm a friend of this yacht owner, don't attack me = ", GET_PLAYER_NAME(PLAYER_ID()), " OwnerPlayerIndex = ", GET_PLAYER_NAME(OwnerPlayerIndex), " ")
									ENDIF
									#ENDIF
								ENDIF
							ELIF YachtDefenceSetting = 6 //Off for Motorcycle Club
								IF GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), OwnerPlayerIndex) 
									WillAirDefenceAttackPlayer = FALSE
									RESET_NET_TIMER(LocalData.st_AirDefenceWarmUpTimer[iYachtIDLoop])
									
									#IF IS_DEBUG_BUILD
									IF LocalData.bAirDefence_WillAttackMe_LastFrame[iYachtIDLoop] != WillAirDefenceAttackPlayer
									CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS - OFF FOR Motorcycle Club - I'm a friend of this yacht owner, don't attack me = ", GET_PLAYER_NAME(PLAYER_ID()), " OwnerPlayerIndex = ", GET_PLAYER_NAME(OwnerPlayerIndex), " ")
									ENDIF
									#ENDIF
								ENDIF
							ELIF YachtDefenceSetting = 5 //Off for Organization
								IF GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), OwnerPlayerIndex) 
									WillAirDefenceAttackPlayer = FALSE
									RESET_NET_TIMER(LocalData.st_AirDefenceWarmUpTimer[iYachtIDLoop])
									
									#IF IS_DEBUG_BUILD
									IF LocalData.bAirDefence_WillAttackMe_LastFrame[iYachtIDLoop] != WillAirDefenceAttackPlayer
									CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS - OFF FOR Organization - I'm a friend of this yacht owner, don't attack me = ", GET_PLAYER_NAME(PLAYER_ID()), " OwnerPlayerIndex = ", GET_PLAYER_NAME(OwnerPlayerIndex), " ")
									ENDIF
									#ENDIF
								ENDIF
							ELIF YachtDefenceSetting = 4 //Off for Friends & Crew
								IF NETWORK_IS_FRIEND(GamerHandle)
								OR ARE_PLAYERS_ON_SAME_ACTIVE_CLAN(PLAYER_ID(), OwnerPlayerIndex)
									WillAirDefenceAttackPlayer = FALSE
									RESET_NET_TIMER(LocalData.st_AirDefenceWarmUpTimer[iYachtIDLoop])
									
									#IF IS_DEBUG_BUILD
									IF LocalData.bAirDefence_WillAttackMe_LastFrame[iYachtIDLoop] != WillAirDefenceAttackPlayer
									CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS - OFF FOR Friends & Crew - I'm a friend of this yacht owner, don't attack me = ", GET_PLAYER_NAME(PLAYER_ID()), " OwnerPlayerIndex = ", GET_PLAYER_NAME(OwnerPlayerIndex), " ")
									ENDIF
									#ENDIF
								ENDIF
							ELIF YachtDefenceSetting = 3 // Off for Crew
								IF ARE_PLAYERS_ON_SAME_ACTIVE_CLAN(PLAYER_ID(), OwnerPlayerIndex)
									WillAirDefenceAttackPlayer = FALSE
									RESET_NET_TIMER(LocalData.st_AirDefenceWarmUpTimer[iYachtIDLoop])
									
									#IF IS_DEBUG_BUILD
									IF LocalData.bAirDefence_WillAttackMe_LastFrame[iYachtIDLoop] != WillAirDefenceAttackPlayer
									CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS - OFF FOR Crew - I'm a friend of this yacht owner, don't attack me = ", GET_PLAYER_NAME(PLAYER_ID()), " OwnerPlayerIndex = ", GET_PLAYER_NAME(OwnerPlayerIndex), " ")
									ENDIF
									#ENDIF
								ENDIF							
							ENDIF
						ENDIF

					ENDIF 

				ELIF YachtDefenceSetting = 0  //OFF
					
					WillAirDefenceAttackPlayer = FALSE
					#IF IS_DEBUG_BUILD
					IF LocalData.bAirDefence_WillAttackMe_LastFrame[iYachtIDLoop] != WillAirDefenceAttackPlayer
						CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS Yacht Defence Off for this yacht me = ", GET_PLAYER_NAME(PLAYER_ID()), " OwnerPlayerIndex = ", GET_PLAYER_NAME(OwnerPlayerIndex), " ")
					ENDIF
					#ENDIF
				ENDIF
				
				BOOL InstantlyUpdateOneFrame = FALSE

				//Just got in a vehicle and haven't had the warm up time or warning.  	
				IF LocalData.bAirDefence_InAreaAndVehicle_LastFrame[iYachtIDLoop] != IS_LOCAL_PLAYER_YACHT_AIR_DEFENCES_ACTIVE_BY_ID(LocalData, iYachtIDLoop)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtIDLoop, <<0,0,0>> ), <<80,80,80>> )
						IF LocalData.bAirDefence_InAreaAndVehicle_LastFrame[iYachtIDLoop] = FALSE
							RESET_NET_TIMER(LocalData.st_AirDefenceWarmUpTimer[iYachtIDLoop])
							CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS RESET_NET_TIMER 1 me = ", GET_PLAYER_NAME(PLAYER_ID()), " OwnerPlayerIndex = ", GET_PLAYER_NAME(OwnerPlayerIndex), " ")
							WillAirDefenceAttackPlayer = FALSE
							InstantlyUpdateOneFrame = TRUE
							CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS I'm just now in danger, give me a moment before you destroy me. me = ", GET_PLAYER_NAME(PLAYER_ID()), " OwnerPlayerIndex = ", GET_PLAYER_NAME(OwnerPlayerIndex), " VDIST = ", VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_COORDS_OF_PRIVATE_YACHT( iYachtIDLoop)))
						ELSE
							CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS I've just now left danger, don't wait but close this off. me = ", GET_PLAYER_NAME(PLAYER_ID()), " OwnerPlayerIndex = ", GET_PLAYER_NAME(OwnerPlayerIndex), " VDIST = ", VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_COORDS_OF_PRIVATE_YACHT( iYachtIDLoop)))
						ENDIF
					ELSE
						CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS I'm not in the yacht's airspace, don't wait but close this off. me = ", GET_PLAYER_NAME(PLAYER_ID()), " OwnerPlayerIndex = ", GET_PLAYER_NAME(OwnerPlayerIndex), " VDIST = ", VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_COORDS_OF_PRIVATE_YACHT( iYachtIDLoop)))
					ENDIF
					LocalData.bAirDefence_InAreaAndVehicle_LastFrame[iYachtIDLoop]  = IS_LOCAL_PLAYER_YACHT_AIR_DEFENCES_ACTIVE_BY_ID(LocalData, iYachtIDLoop)
					
				ENDIF
				
				IF g_bBypassYachtDefencesForSparrow
				AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
				AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), SEASPARROW2)
					OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), SEASPARROW3)
						WillAirDefenceAttackPlayer = FALSE
					ENDIF
				ENDIF
				
				BOOL ChangeRelationship = TRUE
				LocalData.bRunnningWarmUp[iYachtIDLoop] = FALSE
				
				IF WillAirDefenceAttackPlayer = TRUE AND NOT HAS_NET_TIMER_EXPIRED(LocalData.st_AirDefenceWarmUpTimer[iYachtIDLoop], 30000)
					ChangeRelationship = FALSE
				ENDIF
				
				
				IF InstantlyUpdateOneFrame = TRUE
					ChangeRelationship = TRUE
				ENDIF
				
				IF WillAirDefenceAttackPlayer
					IF IS_PLAYER_NEAR_YACHT(PLAYER_ID(), iYachtIDLoop)
						g_B_Disable_HELI_CAM_Shooting_This_Frame = TRUE
					ENDIF
				ENDIF
				
				
				IF ChangeRelationship
					#IF IS_DEBUG_BUILD
					IF LocalData.bAirDefence_WillAttackMe_LastFrame[iYachtIDLoop] != WillAirDefenceAttackPlayer
						CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS update iYachtIDLoop = ", iYachtIDLoop, " owned by player ", GET_PLAYER_NAME(OwnerPlayerIndex), " me = ", GET_PLAYER_NAME(PLAYER_ID())," MyPlayerNum = ", MyPlayerNum, " WillAirDefenceAttackPlayer = ", WillAirDefenceAttackPlayer, " iAirDefenceSphereIndex[iYachtIDLoop] = ", LocalData.iAirDefenceSphereIndex[iYachtIDLoop])
						LocalData.bAirDefence_WillAttackMe_LastFrame[iYachtIDLoop] = WillAirDefenceAttackPlayer
					ENDIF
					#ENDIF
					
					LocalData.bAirDefence_WillAttackMe[iYachtIDLoop] = WillAirDefenceAttackPlayer
					#IF IS_DEBUG_BUILD
					LocalData.bAirDefence_WillAttackMe_LastFrame[iYachtIDLoop] = WillAirDefenceAttackPlayer
					#ENDIF
					SET_PLAYER_TARGETTABLE_FOR_AIR_DEFENCE_SPHERE(MyPlayerNum, LocalData.iAirDefenceSphereIndex[iYachtIDLoop], WillAirDefenceAttackPlayer)
					
				ELSE
					LocalData.bRunnningWarmUp[iYachtIDLoop] = TRUE
					CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS Running warm up.... for iYachtIDLoop ", iYachtIDLoop, " owned by player ", GET_PLAYER_NAME(OwnerPlayerIndex), " me = ", GET_PLAYER_NAME(PLAYER_ID()), " ")
				ENDIF
			ENDIF
		

		ENDIF

	ENDFOR
	
	
ENDPROC


PROC REMOVE_YACHT_DEFENCE(PRIVATE_YACHT_LOCAL_DATA &LocalData, INT iYachtID)

	IF LocalData.bCreatedAirDefence[iYachtID]	
		IF REMOVE_AIR_DEFENCE_SPHERE(LocalData.iAirDefenceSphereIndex[iYachtID])								
			CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - REMOVE_AIR_DEFENCE_SPHERE as player has Sold or Removed Defence iYachtID = ",iYachtID, " DONE! ")
		ELSE
			CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - REMOVE_AIR_DEFENCE_SPHERE as player has Sold or Removed Defence iYachtID = ",iYachtID, " FAILED! ")
		ENDIF
		LocalData.bCreatedAirDefence[iYachtID]	= FALSE
		LocalData.iAirDefenceSphereIndex[iYachtID] = 0
	ENDIF

ENDPROC

PROC UPDATE_YACHT_DEFENCE(PRIVATE_YACHT_LOCAL_DATA &LocalData)

	INT iYachtID
	FLOAT Radius
	VECTOR YachtPosition
	VECTOR GunPosition

	//Loop through the YachtIDs
	
	BOOL refreshAllDefenceSpheres = FALSE

	INT StaggerPlayerNum = LocalData.iClientPlayerStagger
	PLAYER_INDEX StaggerPlayerIndex = INT_TO_NATIVE(PLAYER_INDEX, StaggerPlayerNum)

	INT StaggerYachtID =  LocalData.iClientYachtStagger 
	PLAYER_INDEX StaggerYachtOwner = GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(StaggerYachtID)		
	
	//Cleanup as owner has left. 
	IF StaggerYachtOwner = INVALID_PLAYER_INDEX()
	AND IS_PRIVATE_YACHT_ID_VALID(StaggerYachtID)
		IF LocalData.bCreatedAirDefence[StaggerYachtID]	
			CPRINTLN(DEBUG_YACHT, "[AIRDEF] Yacht Owner has left Session, cleanup AirDef ", StaggerYachtID," ")
			REMOVE_YACHT_DEFENCE(LocalData, StaggerYachtID)
		ENDIF
		EXIT
	ENDIF
	
	IF IS_YACHT_DEFENCE_DISABLED()
		IF IS_PRIVATE_YACHT_ID_VALID(StaggerYachtID)
			IF LocalData.bCreatedAirDefence[StaggerYachtID]	
				CPRINTLN(DEBUG_YACHT, "[AIRDEF] Yacht Disable AirDef tunable is on, cleanup AirDef ", StaggerYachtID, " StaggerYachtOwner = ", GET_PLAYER_NAME(StaggerYachtOwner), " ")
				REMOVE_YACHT_DEFENCE(LocalData, StaggerYachtID)
			ENDIF
		ENDIF
		EXIT
	ENDIF
	
	//RUN IN AREA HELP TEXT		
	IF IS_LOCAL_PLAYER_IN_AREA_OF_YACHT_AIR_DEFENCES_BY_ID(LocalData, StaggerYachtID, 300)
		
		IF LocalData.i_inYachtIDAreaHelpText != StaggerYachtID
		OR LocalData.i_inYachtIDAreaHelpText = -1
			CPRINTLN(DEBUG_YACHT, "[AIRDEF] In players air space, Help text active for players StaggerYachtID ", StaggerYachtID, " StaggerYachtOwner = ", GET_PLAYER_NAME(StaggerYachtOwner), " ")
			LocalData.i_inYachtIDAreaHelpText = StaggerYachtID
		ENDIF
								
	ELSE
		
		IF LocalData.i_inYachtIDAreaHelpText > -1
			IF LocalData.i_inYachtIDAreaHelpText = StaggerYachtID
				CPRINTLN(DEBUG_YACHT, "[AIRDEF] Left players air space, Help text Turned off players StaggerYachtID  = ", StaggerYachtID, " StaggerYachtOwner = ", GET_PLAYER_NAME(StaggerYachtOwner), " " )
				LocalData.i_inYachtIDAreaHelpText = -1
			ENDIF
		ENDIF
	
	ENDIF
	
	//RUN WARNING SHOTS
	IF IS_LOCAL_PLAYER_IN_AREA_OF_YACHT_AIR_DEFENCES_BY_ID(LocalData, StaggerYachtID, 300)
		IF LocalData.i_inYachtIDAreaWarningShot != StaggerYachtID
		OR LocalData.i_inYachtIDAreaWarningShot = -1
		
			CPRINTLN(DEBUG_YACHT, "[AIRDEF] In players air space, warning shots active for players StaggerYachtID ", StaggerYachtID, " StaggerYachtOwner = ", GET_PLAYER_NAME(StaggerYachtOwner), " ")
			LocalData.i_inYachtIDAreaWarningShot = StaggerYachtID

		ENDIF
								
	ELSE
		IF LocalData.i_inYachtIDAreaWarningShot != -1
			IF LocalData.i_inYachtIDAreaWarningShot = StaggerYachtID
				
				CPRINTLN(DEBUG_YACHT, "[AIRDEF] Left players air space, warning shots Turned off players StaggerYachtID  = ", StaggerYachtID, " StaggerYachtOwner = ", GET_PLAYER_NAME(StaggerYachtOwner), " " )
				LocalData.i_inYachtIDAreaWarningShot = -1
			ENDIF
		ENDIF
	ENDIF

	//RUN MISSION DISABLE HELP TEXT
	IF SHOULD_PLAYER_DISABLE_YACHT_DEFENCES(PLAYER_ID())
	
		IF GET_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_DEFENCE_SETTING) != 0
			LocalData.bTurnOffYachtAirDefForMissionPreviousSetting =  GET_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_DEFENCE_SETTING)
			LocalData.bTurnOffYachtAirDefForMissionPlayer = NATIVE_TO_INT(StaggerYachtOwner)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_DEFENCE_SETTING, 0)
			CPRINTLN(DEBUG_YACHT, "[YDEFMENU][AIRDEFDIS] UPDATE_YACHT_DEFENCE - Turning MP_STAT_YACHT_DEFENCE_SETTING off for me = ", GET_PLAYER_NAME(PLAYER_ID()))
			UPDATE_LOCAL_PLAYER_BD_YACHT_AIR_DEFENCE_SETTING()
			refreshAllDefenceSpheres = TRUE
		ENDIF
								
	ELSE
		IF LocalData.bTurnOffYachtAirDefForMissionPreviousSetting > 0
		AND LocalData.bTurnOffYachtAirDefForMissionPlayer = NATIVE_TO_INT(PLAYER_ID())
			
			SET_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_DEFENCE_SETTING, LocalData.bTurnOffYachtAirDefForMissionPreviousSetting)
			CPRINTLN(DEBUG_YACHT, "[YDEFMENU][AIRDEFDIS] UPDATE_YACHT_DEFENCE - Turning MP_STAT_YACHT_DEFENCE_SETTING back on for me = ", GET_PLAYER_NAME(PLAYER_ID()), " put setting back to ", LocalData.bTurnOffYachtAirDefForMissionPreviousSetting, " ") 
			UPDATE_LOCAL_PLAYER_BD_YACHT_AIR_DEFENCE_SETTING()
			LocalData.bTurnOffYachtAirDefForMissionPreviousSetting = 0
			LocalData.bTurnOffYachtAirDefForMissionPlayer = -1
			refreshAllDefenceSpheres = TRUE
	
		ENDIF
	ENDIF

	IF StaggerPlayerIndex != INVALID_PLAYER_INDEX()
		//RUN REFRESH
		//Player buys or sells a yacht
		BOOL DoesPlayerOwnYacht = DOES_PLAYER_OWN_PRIVATE_YACHT(StaggerPlayerIndex)
		BOOL DoesPlayerOwnYachtLastFrame = IS_BIT_SET(LocalData.bCreatedAirDefence_OwnYachtLastFrameBS, StaggerPlayerNum) 
		
		IF DoesPlayerOwnYacht != DoesPlayerOwnYachtLastFrame
			refreshAllDefenceSpheres = TRUE
			CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - Yacht just purchased for player ", StaggerPlayerNum, " ", GET_PLAYER_NAME(StaggerPlayerIndex))
		ENDIF
		
		IF DoesPlayerOwnYacht
			IF DoesPlayerOwnYachtLastFrame = FALSE
				SET_BIT(LocalData.bCreatedAirDefence_OwnYachtLastFrameBS, StaggerPlayerNum)
			ENDIF
		ELSE
			IF DoesPlayerOwnYachtLastFrame
				CLEAR_BIT(LocalData.bCreatedAirDefence_OwnYachtLastFrameBS, StaggerPlayerNum)
			ENDIF
		ENDIF
	ELSE
		IF LocalData.iAirDefenceSettingLastFrame[StaggerPlayerNum] != 0
			LocalData.iAirDefenceSettingLastFrame[StaggerPlayerNum] = 0
			CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - Player has left session run refresh ", StaggerPlayerNum, " ")

			refreshAllDefenceSpheres = TRUE
		ENDIF
		IF IS_BIT_SET(LocalData.bCreatedAirDefence_OwnYachtLastFrameBS, StaggerPlayerNum) 
			CLEAR_BIT(LocalData.bCreatedAirDefence_OwnYachtLastFrameBS, StaggerPlayerNum)
			CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - Player has left session run refresh ", StaggerPlayerNum, " ")
			refreshAllDefenceSpheres = TRUE
		ENDIF
	ENDIF


	//Player moves their yacht
	IF LocalData.bRefreshAirDefenceDueToMove
		refreshAllDefenceSpheres = TRUE
		LocalData.bRefreshAirDefenceDueToMove = FALSE
		CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - Yacht just purchased for player ", StaggerPlayerNum, " ", GET_PLAYER_NAME(StaggerPlayerIndex))
	ENDIF
	
	
	IF IS_NET_PLAYER_OK(StaggerPlayerIndex, FALSE) != LocalData.bAirDefencePlayerInSessionLastFrame[StaggerPlayerNum]
		refreshAllDefenceSpheres = TRUE
		CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - Yacht Defence Player has left session ", StaggerPlayerNum, " ", GET_PLAYER_NAME(StaggerPlayerIndex))
	ENDIF
	LocalData.bAirDefencePlayerInSessionLastFrame[StaggerPlayerNum] = IS_NET_PLAYER_OK(StaggerPlayerIndex, FALSE)
	 
	 
	IF StaggerYachtOwner != INVALID_PLAYER_INDEX()
		//Player changes their setting
		IF GET_PLAYER_BD_YACHT_DEFENCE_SETTING(StaggerYachtOwner) != LocalData.iAirDefenceSettingLastFrame[StaggerYachtID]
			refreshAllDefenceSpheres = TRUE
			CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - Yacht Defence Setting has changed for yacht ", StaggerYachtID, " owner = ", GET_PLAYER_NAME(StaggerYachtOwner), 
			 			" old setting = ", LocalData.iAirDefenceSettingLastFrame[StaggerYachtID], " new setting = ", GET_PLAYER_BD_YACHT_DEFENCE_SETTING(StaggerYachtOwner), " ")

		ENDIF
		LocalData.iAirDefenceSettingLastFrame[StaggerYachtID] = GET_PLAYER_BD_YACHT_DEFENCE_SETTING(StaggerYachtOwner)
	ENDIF

	
	//RUN IN AREA FUNCTIONALITY
	IF LocalData.i_inYachtIDAreaHelpText > -1
	//I am in this players yacht air space and in danger of being shot down.
	
	
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			IF NOT IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_WARNING)
				SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_WARNING, TRUE)
				PRINT_HELP("AIRDEF_WARN")
				CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - Print help warning ")
			ENDIF
		ENDIF
		
	ELSE
		
		IF IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_WARNING)
			SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_WARNING, FALSE)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AIRDEF_WARN")
				CLEAR_HELP()
				CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - Clear help warning ")
			ENDIF
		ENDIF
	
	ENDIF
	
	IF g_AirDefDia_Gentle = TRUE
		CPRINTLN(DEBUG_YACHT, "[AIRDEFDIA] g_AirDefDia_Gentle - play gentle dialogue from event ")
		TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_GENTLE(LocalData)
		
		g_AirDefDia_Gentle = FALSE
	ENDIF
	
	IF g_AirDefDia_Aggro = TRUE
	
		CPRINTLN(DEBUG_YACHT, "[AIRDEFDIA] g_AirDefDia_Aggro - play aggro dialogue from event ")
		TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_AGGRES(LocalData)
		
		g_AirDefDia_Aggro = FALSE
	ENDIF
	
	IF g_AirDefDia_Warning = TRUE
		
		CPRINTLN(DEBUG_YACHT, "[AIRDEFDIA] g_AirDefDia_Warning - play Warning dialogue from event ")
		TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_THIRD(LocalData)
		g_AirDefDia_Warning = FALSE
		
	ENDIF

	IF g_AirDefDia_ActiveForYachtId > -1
		IF IS_LOCAL_PLAYER_A_PASSENGER_OF_YACHT_AIR_DEFENCES_BY_ID(LocalData, g_AirDefDia_ActiveForYachtId, 300, TRUE)
			IF IS_PLAYER_ON_YACHT(PLAYER_ID(), g_AirDefDia_ActiveForYachtId)
				CPRINTLN(DEBUG_YACHT, "[AIRDEFDIA] g_AirDefDia_ActiveForYachtId - play Warning dialogue from event for yacht ", g_AirDefDia_ActiveForYachtId, " ")
				TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE(LocalData)
			ENDIF
		ENDIF
		SET_BIT(LocalData.bAirDefence_BSRunDialogueWarning[g_AirDefDia_ActiveForYachtId], YACHT_DIALOGUE_AIR_DEF_ACTIVE)
		g_AirDefDia_ActiveForYachtId = -1
	ENDIF

	//DIALOGUE OF AIR DEFENCE
	IF LocalData.bAirDefence_WillAttackMe[StaggerYachtID]
		

		IF IS_LOCAL_PLAYER_IN_AREA_OF_YACHT_AIR_DEFENCES_BY_OWNER(LocalData, StaggerYachtOwner, 300)
			IF NOT IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_FIRST_SHOT)
				IF NETWORK_IS_PLAYER_ACTIVE(StaggerYachtOwner)
					GAMER_HANDLE OwnerGamerHandle = GET_GAMER_HANDLE_PLAYER(StaggerYachtOwner)
					IF NETWORK_IS_FRIEND(OwnerGamerHandle)
						TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_GENTLE(LocalData)
						IF IS_PED_IN_ANY_RESTRICTED_HELI(PLAYER_PED_ID())
						OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
						OR IS_PED_IN_ANY_RESTRICTED_VEHICLE(PLAYER_PED_ID())
						OR (IS_PLAYER_USING_DRONE(PLAYER_ID()) AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE)
							VEHICLE_INDEX aVehStaggerPlayerIsIn
							
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								aVehStaggerPlayerIsIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							ENDIF
							
							IF IS_VEHICLE_DRIVEABLE(aVehStaggerPlayerIsIn)
							OR (IS_PLAYER_USING_DRONE(PLAYER_ID()) AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE)
								BROADCAST_PRIVATE_YACHT_AIRDEF_DIALOGUE(aVehStaggerPlayerIsIn, TRUE, FALSE, FALSE)	
							ENDIF
						ENDIF
					ELSE
						TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_AGGRES(LocalData)
						
						IF IS_PED_IN_ANY_RESTRICTED_HELI(PLAYER_PED_ID())
						OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
						OR IS_PED_IN_ANY_RESTRICTED_VEHICLE(PLAYER_PED_ID())
						OR (IS_PLAYER_USING_DRONE(PLAYER_ID()) AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE)
							VEHICLE_INDEX aVehStaggerPlayerIsIn
							
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								aVehStaggerPlayerIsIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							ENDIF
							
							IF IS_VEHICLE_DRIVEABLE(aVehStaggerPlayerIsIn)
							OR (IS_PLAYER_USING_DRONE(PLAYER_ID()) AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE)
								BROADCAST_PRIVATE_YACHT_AIRDEF_DIALOGUE(aVehStaggerPlayerIsIn, FALSE, TRUE, FALSE)	
							ENDIF
						ENDIF
					ENDIF
								
					SET_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_FIRST_SHOT)
				ENDIF
			ENDIF
			
		ELSE
		
			IF IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_FIRST_SHOT)
				IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(LocalData.st_AirDefenceDialogueAggroTimer, 30000)
					CLEAR_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_FIRST_SHOT)
				ENDIF
			ENDIF
		
			IF StaggerYachtOwner != INVALID_PLAYER_INDEX()
				IF NETWORK_IS_PLAYER_ACTIVE(StaggerYachtOwner)
					GAMER_HANDLE OwnerGamerHandle = GET_GAMER_HANDLE_PLAYER(StaggerYachtOwner)
					IF NOT NETWORK_IS_FRIEND(OwnerGamerHandle)  //after speaking to Scott we agreed this should only be for non-friends. 
						IF IS_PLAYER_NEAR_YACHT(PLAYER_ID(), StaggerYachtID, 100)
							IF (NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
							AND NOT IS_PED_IN_ANY_RESTRICTED_HELI(PLAYER_PED_ID())
							AND NOT IS_PED_IN_ANY_RESTRICTED_VEHICLE(PLAYER_PED_ID())
							AND NOT IS_PLAYER_ON_YACHT(PLAYER_ID(), StaggerYachtID))
							OR (IS_PLAYER_USING_DRONE(PLAYER_ID()) AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE)
								IF NOT IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_SWIMNEAR)
									TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_GENERAL(LocalData)
									SET_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_SWIMNEAR)
								ENDIF
							ELSE
								IF IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_SWIMNEAR)
									CLEAR_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_SWIMNEAR)
								ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_SWIMNEAR)
								CLEAR_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_SWIMNEAR)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_SWIMNEAR)
					CLEAR_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_SWIMNEAR)
				ENDIF
			ENDIF
		ENDIF

		IF IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_THIRD_SHOT)
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(LocalData.st_AirDefenceDialogueTimer[StaggerYachtID], 30000)	
				CLEAR_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_THIRD_SHOT)
			ENDIF
		ENDIF

		IF IS_LOCAL_PLAYER_IN_AREA_OF_YACHT_AIR_DEFENCES_BY_OWNER(LocalData, StaggerYachtOwner, 200)
			IF LocalData.i_inYachtIDAreaWarningShot > -1 //Warning shots are shooting me
				IF NOT IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_THIRD_SHOT)
					TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_THIRD(LocalData)
					
					IF IS_PED_IN_ANY_RESTRICTED_HELI(PLAYER_PED_ID())
					OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
					OR IS_PED_IN_ANY_RESTRICTED_VEHICLE(PLAYER_PED_ID())
					OR (IS_PLAYER_USING_DRONE(PLAYER_ID()) AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE)
						VEHICLE_INDEX aVehStaggerPlayerIsIn 
						
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							aVehStaggerPlayerIsIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						ENDIF
	
						IF IS_VEHICLE_DRIVEABLE(aVehStaggerPlayerIsIn)
						OR (IS_PLAYER_USING_DRONE(PLAYER_ID()) AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE)
							BROADCAST_PRIVATE_YACHT_AIRDEF_DIALOGUE(aVehStaggerPlayerIsIn, FALSE, FALSE, TRUE)	
						ENDIF
					ENDIF
					
					SET_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_THIRD_SHOT)
				ENDIF
			ENDIF
		ENDIF
	
	ELSE
		IF IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_FIRST_SHOT)
			CLEAR_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_FIRST_SHOT)
		ENDIF
		IF IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_SWIMNEAR)
			CLEAR_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_SWIMNEAR)
		ENDIF
		IF IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_THIRD_SHOT)
			CLEAR_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_THIRD_SHOT)
		ENDIF
		
					
		IF IS_LOCAL_PLAYER_A_PASSENGER_OF_YACHT_AIR_DEFENCES_BY_ID(LocalData, StaggerYachtID, 300, TRUE)
			IF IS_PLAYER_ON_YACHT(PLAYER_ID(), StaggerYachtID)
				IF NOT IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_ACTIVE)
					IF StaggerYachtOwner = PLAYER_ID()
						TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE(LocalData)
					
						BROADCAST_PRIVATE_YACHT_AIRDEF_DIALOGUE(NULL, FALSE, FALSE, FALSE, StaggerYachtID)
						SET_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_ACTIVE)
					ENDIF
					
				ENDIF
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(LocalData.st_AirDefenceDialogueAttackTimer[StaggerYachtID])
				RESET_NET_TIMER(LocalData.st_AirDefenceDialogueAttackTimer[StaggerYachtID])
			ENDIF
			IF IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_ATTACK_PASSENGER)
				CLEAR_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_ATTACK_PASSENGER)
			ENDIF
			IF IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_REASSURE_PASSENGER)
				CLEAR_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_REASSURE_PASSENGER)
			ENDIF
			IF IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_PASSENGER_NOW)
				CLEAR_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_PASSENGER_NOW)
			ENDIF
			IF IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_INTRO_PASSENGER)
				CLEAR_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_INTRO_PASSENGER)
			ENDIF
		ENDIF

		IF IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_ATTACK_PASSENGER)
		OR IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_REASSURE_PASSENGER)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(LocalData.st_AirDefenceDialogueAttackTimer[StaggerYachtID], 30000)
				OR IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[g_AirDefYachtID] ,YACHT_DIALOGUE_AIR_DEF_PASSENGER_NOW)
					
					IF IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_ATTACK_PASSENGER)
						TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_ANNOUNCE_ATTACK_PASSENGERS(LocalData)
					ELIF IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_REASSURE_PASSENGER)
						TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_ANNOUNCE_PASSENGERS(LocalData)
					ENDIF
					CLEAR_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_ATTACK_PASSENGER)
					CLEAR_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_REASSURE_PASSENGER)
					CLEAR_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_PASSENGER_NOW)
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	//PASSENGERS ON YACHT WITH AIR DEF ACTIVE
	IF IS_PRIVATE_YACHT_ID_VALID(g_AirDefYachtID)
		IF LocalData.bAirDefence_WillAttackMe[g_AirDefYachtID] = FALSE
			IF NOT IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[g_AirDefYachtID], YACHT_DIALOGUE_AIR_DEF_ATTACK_PASSENGER)
			AND NOT IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[g_AirDefYachtID], YACHT_DIALOGUE_AIR_DEF_REASSURE_PASSENGER)
				
				IF IS_PLAYER_ON_YACHT(PLAYER_ID(), g_AirDefYachtID)			
					IF IS_VECTOR_ZERO(g_AirDefExplosionCoords) = FALSE
						IF g_AirDefSphereIndex = LocalData.iAirDefenceSphereIndex[g_AirDefYachtID] //This yacht is firing.
						
							IF NOT IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[g_AirDefYachtID], YACHT_DIALOGUE_AIR_DEF_INTRO_PASSENGER)
								TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_INTRO_PASSENGERS(LocalData)
								SET_BIT(LocalData.bAirDefence_BSRunDialogueWarning[g_AirDefYachtID], YACHT_DIALOGUE_AIR_DEF_INTRO_PASSENGER)
								SET_BIT(LocalData.bAirDefence_BSRunDialogueWarning[g_AirDefYachtID] ,YACHT_DIALOGUE_AIR_DEF_PASSENGER_NOW)
							ENDIF			
							
							//see explosions but not at them. 
							IF IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), WEAPONTYPE_VEHICLE_ROCKET, 300)
							OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), WEAPONTYPE_VEHICLE_WEAPON_PLANE_ROCKET, 300)
							OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), WEAPONTYPE_VEHICLE_PLAYER_LASER, 300)
							OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), WEAPONTYPE_RPG, 300)
							OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), WEAPONTYPE_DLC_VEHICLE_SUB_MISSILE_HOMING, 300)
							OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), WEAPONTYPE_DLC_VEHICLE_KOSATKA_TORPEDO, 300)
							OR IS_ENTITY_ALIVE(GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 300, INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_AirMissile_01a"))))
							//OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), WEAPONTYPE_VEHICLE_PLAYER_BULLET, 300)	
							//OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), WEAPONTYPE_VEHICLE_PLAYER_BUZZARD, 300)	
								CPRINTLN(DEBUG_YACHT, "[AIRDEFDIA] Setting YACHT_DIALOGUE_AIR_DEF_ATTACK_PASSENGER ")
								SET_BIT(LocalData.bAirDefence_BSRunDialogueWarning[g_AirDefYachtID], YACHT_DIALOGUE_AIR_DEF_ATTACK_PASSENGER)
							ELSE
								CPRINTLN(DEBUG_YACHT, "[AIRDEFDIA] Setting YACHT_DIALOGUE_AIR_DEF_REASSURE_PASSENGER ")
								SET_BIT(LocalData.bAirDefence_BSRunDialogueWarning[g_AirDefYachtID], YACHT_DIALOGUE_AIR_DEF_REASSURE_PASSENGER)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF NATIVE_TO_INT(PLAYER_ID()) > -1
		
		INT YachtIDLocalPlayerIsAttacking = GET_YACHT_PLAYER_IS_NEAR(PLAYER_ID())

		IF IS_PRIVATE_YACHT_ID_VALID(YachtIDLocalPlayerIsAttacking)
			IF LocalData.bAirDefence_WillAttackMe[YachtIDLocalPlayerIsAttacking]
				IF IS_LOCAL_PLAYER_IN_AREA_OF_YACHT_AIR_DEFENCES_BY_ID(LocalData, YachtIDLocalPlayerIsAttacking, 300)
					IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
					OR (IS_ENTITY_AT_COORD(GET_LOCAL_DRONE_PROP_OBJ_INDEX(), YachtPosition, <<Radius, Radius, Radius>>, FALSE, TRUE, TM_ANY) AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE)
					
						//removed for todo 2621823
//						IF NOT IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_ATTACKED_YACHT)
//							TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_ATTACK(LocalData)
//							SET_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_ATTACKED_YACHT)
//						ELSE
						
							IF NOT IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[YachtIDLocalPlayerIsAttacking], YACHT_DIALOGUE_AIR_DEF_ATTACKED_YACHT)
								TRIGGER_YACHT_AIR_DEFENCE_WARNING_ACTIVE_CONT(LocalData)
								SET_BIT(LocalData.bAirDefence_BSRunDialogueWarning[YachtIDLocalPlayerIsAttacking], YACHT_DIALOGUE_AIR_DEF_ATTACKED_YACHT)
							ENDIF
//						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_ATTACKED_YACHT)
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(LocalData.st_AirDefenceDialogueAttackingTimer, 30000)
			CLEAR_BIT(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_ATTACKED_YACHT)
		ENDIF
	ENDIF

	IF IS_VECTOR_ZERO(g_AirDefExplosionCoords)
	AND LocalData.i_inYachtIDAreaWarningShot = -1
	
		IF LocalData.HasLoadedPTFXWarningShot
			IF HAS_NAMED_PTFX_ASSET_LOADED("scr_apartment_mp")
				REMOVE_NAMED_PTFX_ASSET("scr_apartment_mp")	
				LocalData.HasLoadedPTFXWarningShot = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VECTOR_ZERO(g_AirDefExplosionCoords) = FALSE
		IF DOES_AIR_DEFENCE_SPHERE_EXIST(g_AirDefSphereIndex)
			FIRE_WARNING_SHOT_DIRECT( g_AirDefExplosionCoords, g_AirDefGunCoords, g_AirDefSphereIndex, LocalData.HasLoadedPTFXWarningShot)
		ELSE
			g_AirDefExplosionCoords = <<0,0,0>>
			g_AirDefGunCoords = <<0,0,0>>
			g_AirDefSphereIndex = 0
		ENDIF
	ENDIF
	
	IF LocalData.i_inYachtIDAreaWarningShot > -1
	
	//I am in this players yacht air space and in danger of being shot down.
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND (IS_PED_IN_ANY_PLANE(PLAYER_PED_ID()) OR IS_PED_IN_ANY_RESTRICTED_HELI(PLAYER_PED_ID())  OR IS_PED_IN_ANY_RESTRICTED_VEHICLE(PLAYER_PED_ID()) OR (IS_PLAYER_USING_DRONE(PLAYER_ID()) AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE))
		AND IS_BIT_SET(LocalData.bAirDefence_BSRunDialogueWarning[StaggerYachtID], YACHT_DIALOGUE_AIR_DEF_THIRD_SHOT)
			FIRE_WARNING_SHOT(LocalData.st_AirDefenceWarningShotTimer, LocalData.iAirDefenceSphereIndex[LocalData.i_inYachtIDAreaWarningShot], LocalData.i_inYachtIDAreaWarningShot, LocalData.HasLoadedPTFXWarningShot)	
		ENDIF
	ENDIF
	
	IF LocalData.i_inYachtIDMissionHelpText > -1
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			IF NOT IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_MISSION)
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_GB_BOSS_DEATHMATCH
				OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_SALVAGE
					IF IS_PLAYER_ON_THEIR_YACHT(PLAYER_ID())
						SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_MISSION, TRUE)
						PRINT_HELP("AIRDEF_MS_DM")
						CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - Print help warning AIRDEF_MS_DM Air defence is off because Boss vs Boss DM active & player on their yacht ")
					ENDIF
					
				ELIF DOES_PLAYER_OWN_YACHT_AND_ON_YACHT_ATTACK(PLAYER_ID())
					SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_MISSION, TRUE)
					PRINT_HELP("AIRDEF_MISS")
					CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - Print help warning AIRDEF_MISS Air defence is off because Yacht Attack active ")
					
				ELIF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					IF IS_PLAYER_ON_THEIR_YACHT(PLAYER_ID())
						SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_MISSION, TRUE)
						SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_WANTED_LEVEL, TRUE)
						PRINT_HELP("AIRDEF_W_LVL")
						CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - Print help warning AIRDEF_W_LVL Air defence is off because player has Wanted Level ")
					ENDIF
				ELSE
					IF IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_WANTED_LEVEL)
						SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_WANTED_LEVEL, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			IF IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_MISSION)
			AND NOT IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_WANTED_LEVEL)
				SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_MISSION, FALSE)
				PRINT_HELP("AIRDEF_REVER")
				CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - Print help warning AIRDEF_REVER Air defence is back to what you had previously because you ended mission ")
			ELSE
				IF IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_MISSION)
					SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_MISSION, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// WANTED LEVEL HANDLING B* 2620298
	IF StaggerYachtOwner != INVALID_PLAYER_INDEX()
	AND StaggerYachtOwner != PLAYER_ID()
		IF GET_PLAYER_BD_YACHT_DEFENCE_SETTING(StaggerYachtOwner) != 0
			IF IS_PLAYER_ON_YACHT(PLAYER_ID(), StaggerYachtID)
			OR IS_PLAYER_NEAR_YACHT(PLAYER_ID(), StaggerYachtID, 150)
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 2
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
					CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - Capped Wanted Level at 2. Player is on another players yacht with active air defences ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	IF GET_YACHT_PLAYER_IS_ON(PLAYER_ID()) != -1
	AND GET_YACHT_PLAYER_IS_ON(PLAYER_ID()) = StaggerYachtID
		IF GET_PLAYER_BD_YACHT_DEFENCE_SETTING(StaggerYachtOwner) != 0
			IF NOT IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_DISABLE_WEAPONS)
				SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_DISABLE_WEAPONS, TRUE)
			ENDIF
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_DISABLE_WEAPONS_WHEEL)
				SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_DISABLE_WEAPONS_WHEEL, TRUE)
				PRINT_HELP("AIRDEF_WEAP")
				CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - Print help warning AIRDEF_WEAP While Yacht Defense is set to On you are unable to access your weapons ")
			ENDIF
		ELSE
			IF IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_DISABLE_WEAPONS)
				SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_DISABLE_WEAPONS, FALSE)
			ENDIF
			IF IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_DISABLE_WEAPONS_WHEEL)
				SET_PRIVATE_YACHT_HELP_TEXT_FLAG(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_DISABLE_WEAPONS_WHEEL, FALSE)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AIRDEF_WEAP")
					CLEAR_HELP()
					CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - Clear AIRDEF_WEAP help text ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RUN_AIR_DEFENCE_RELATIONSHIPS(LocalData)

	IF refreshAllDefenceSpheres 
//		REQUEST_NAMED_PTFX_ASSET("scr_apartment_mp")
	
		INT I 
		CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - refreshAllDefenceSpheres = TRUE ")
		PLAYER_INDEX PlayerIndex
		FOR I = 0 TO NUM_NETWORK_PLAYERS-1
			PlayerIndex = INT_TO_NATIVE(PLAYER_INDEX, I)
			IF PlayerIndex != INVALID_PLAYER_INDEX()
				iYachtID = GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PlayerIndex)
				IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
					IF DOES_PLAYER_OWN_PRIVATE_YACHT(PlayerIndex)
						IF GET_PLAYER_BD_YACHT_DEFENCE_SETTING(PlayerIndex) != 0
							IF NOT LocalData.bCreatedAirDefence[iYachtID]
								Radius = 150
								YachtPosition = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, <<0,0,0>> )
								GunPosition = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, <<-0.3468, -10.3565, 4.6446>> )
								LocalData.iAirDefenceSphereIndex[iYachtID] = CREATE_AIR_DEFENCE_SPHERE(YachtPosition, Radius, GunPosition)
								SET_PLAYER_TARGETTABLE_FOR_AIR_DEFENCE_SPHERE(I, LocalData.iAirDefenceSphereIndex[iYachtID], FALSE)
								CPRINTLN(DEBUG_YACHT, "[AIRDEF] UPDATE_YACHT_DEFENCE - CREATE_AIR_DEFENCE_SPHERE YachtPosition = ",YachtPosition, " radius ",Radius, " GunPosition = ", GunPosition, " iYachtID = ", iYachtID, " ")
								CPRINTLN(DEBUG_YACHT, "[AIRDEF] RUN_AIR_DEFENCE_RELATIONSHIPS - CREATE_AIR_DEFENCE_SPHERE iYachtID = ",iYachtID, " don't attack me by default SphereIndex[iYachtID] = ", LocalData.iAirDefenceSphereIndex[iYachtID], " owner = ", GET_PLAYER_NAME(PlayerIndex), " ")
								#IF IS_DEBUG_BUILD
								LocalData.bAirDefence_WillAttackMe_LastFrame[iYachtID] = FALSE
								#ENDIF
								CLEAR_BIT(LocalData.bAirDefence_BSRunDialogueWarning[iYachtID], YACHT_DIALOGUE_AIR_DEF_ACTIVE)
								REINIT_NET_TIMER(LocalData.st_AirDefenceWarmUpTimer[iYachtID])
								LocalData.bCreatedAirDefence[iYachtID] = TRUE
								SETUP_CAPTAIN_DIALOGUE(LocalData)
							ENDIF
						ELSE
							REMOVE_YACHT_DEFENCE(LocalData, iYachtID)
						ENDIF
					ELSE
						REMOVE_YACHT_DEFENCE(LocalData, iYachtID)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
	ENDIF

ENDPROC



FUNC BOOL SHOULD_HIDE_YACHT_BLIP()

	IF NOT CAN_PLAYER_USE_PROPERTY()
	OR (GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].propertyDetails.iCurrentlyInsideProperty = PROPERTY_YACHT_APT_1_BASE)
	OR IS_PLAYER_ON_ANY_FM_JOB(MPGlobals.LocalPlayerID)
	OR SHOULD_PROPERTY_BLIPS_BE_HIDDEN(TRUE)
		RETURN(TRUE)
	ENDIF

	RETURN(FALSE)
	
ENDFUNC

FUNC BOOL SHOULD_PRIVATE_YACHT_BE_ACTIVE(INT iYachtID, PRIVATE_YACHT_LOCAL_DATA &LocalData)
	
	#IF IS_DEBUG_BUILD
	IF (g_bActivateAllPrivateYachts = TRUE)
		RETURN(TRUE)
	ENDIF
	#ENDIF	
	
	BOOL bReturn = FALSE
	
	// so it compiles in relase
	IF (LocalData.iClientYachtStagger = 0)
		// do nothing	
	ENDIF
	
	IF SHOULD_PRIVATE_YACHTS_BE_ACTIVE()
		IF (GlobalServerBD_BlockC.PYYachtDetails[iYachtID].bYachtIsActive)
		#IF IS_DEBUG_BUILD
		OR (LocalData.bActivateThisYacht[iYachtID])
		#ENDIF
			bReturn = TRUE
		ENDIF
	ENDIF
	
	IF FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
	AND GET_FMMC_LOBBY_LEADER() != INVALID_PLAYER_INDEX()
		IF GET_FMMC_YACHT_ID(ciYACHT_LOBBY_HOST_YACHT_INDEX) = iYachtID
			PRINTLN("[HostOwnedYacht] Creating yacht in position ", iYachtID, " due to FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT & this being the location specified in the Creator")
			RETURN TRUE
		ELSE
			PRINTLN("[HostOwnedYacht_Spam] Blocking yacht in position ", iYachtID, " due to FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT & this location not being the one specified in the Creator")
			RETURN FALSE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF (LocalData.bShouldYachtBeActive[iYachtID])
		IF NOT (bReturn)
			CPRINTLN(DEBUG_YACHT, "SHOULD_PRIVATE_YACHT_BE_ACTIVE - yacht ", iYachtID, " switching to inactive")	
		ENDIF
	ELSE
		IF (bReturn)
			CPRINTLN(DEBUG_YACHT, "SHOULD_PRIVATE_YACHT_BE_ACTIVE - yacht ", iYachtID, " switching to active")		
		ENDIF
	ENDIF
	LocalData.bShouldYachtBeActive[iYachtID] = bReturn
	#ENDIF
	
	RETURN(bReturn)
ENDFUNC

//
//PROC SET_YACHT_OBJECTS_ALPHA(INT iYachtID, PRIVATE_YACHT_LOCAL_DATA &LocalData, INT iAlpha, BOOL bUseSmoothAlpha)
//
//	CPRINTLN(DEBUG_YACHT, "SET_YACHT_OBJECTS_ALPHA - called for yacht ", iYachtID, " iAlpha ", iAlpha)
//	
//	// jacuzzi
//	IF DOES_ENTITY_EXIST(LocalData.YachtData[iYachtID].JacuzziObjectID)
//		SET_ENTITY_ALPHA(LocalData.YachtData[iYachtID].JacuzziObjectID, iAlpha, bUseSmoothAlpha)
//	ENDIF
//	
//	// radar
//	INT i
//	REPEAT 3 i
//		IF DOES_ENTITY_EXIST(LocalData.YachtData[iYachtID].RadarObjectID[i])
//			SET_ENTITY_ALPHA(LocalData.YachtData[iYachtID].RadarObjectID[i], iAlpha, bUseSmoothAlpha)
//		ENDIF
//	ENDREPEAT
//	
//	// lighting
//	IF DOES_ENTITY_EXIST(LocalData.YachtData[iYachtID].LightingObjectID)
//		SET_ENTITY_ALPHA(LocalData.YachtData[iYachtID].LightingObjectID, iAlpha, bUseSmoothAlpha)
//	ENDIF
//	
//	// railing
//	IF DOES_ENTITY_EXIST(LocalData.YachtData[iYachtID].RailingObjectID)
//		SET_ENTITY_ALPHA(LocalData.YachtData[iYachtID].RailingObjectID, iAlpha, bUseSmoothAlpha)
//	ENDIF
//	
//	// flagpole
//	IF DOES_ENTITY_EXIST(LocalData.YachtData[iYachtID].PoleObjectID)
//		SET_ENTITY_ALPHA(LocalData.YachtData[iYachtID].PoleObjectID, iAlpha, bUseSmoothAlpha)
//	ENDIF
//	
//	// flag
//	IF DOES_ENTITY_EXIST(LocalData.YachtData[iYachtID].FlagObjectID)
//		SET_ENTITY_ALPHA(LocalData.YachtData[iYachtID].FlagObjectID, iAlpha, bUseSmoothAlpha)
//	ENDIF
//
//ENDPROC


PROC DELETE_OBJECT_FOR_YACHT(OBJECT_INDEX &ObjectID)
//	FREEZE_ENTITY_POSITION(ObjectID, TRUE) 
//	SET_ENTITY_COLLISION(ObjectID, FALSE)
//	SET_ENTITY_DYNAMIC(ObjectID, FALSE)
//	SET_ENTITY_VISIBLE(ObjectID, FALSE)
//	CLEAR_ROOM_FOR_ENTITY(ObjectID)
	DELETE_OBJECT(ObjectID)
	ObjectID = NULL
ENDPROC

PROC DELETE_HULL_OBJECTS_FOR_YACHT(YACHT_DATA &YachtData)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_YACHT, "DELETE_HULL_OBJECTS_FOR_YACHT - iYachtID ", YachtData.iLocation)
	PRINTLN_YACHT_APPEARANCE(YachtData.Appearance)
	#ENDIF
	INT i
	REPEAT ENUM_TO_INT(HP_TOTAL_HULL_PARTS) i
		IF DOES_ENTITY_EXIST(YachtData.OptionObjectID[i])
			CPRINTLN(DEBUG_YACHT, "DELETE_HULL_OBJECTS_FOR_YACHT - OptionObjectID ", i)
			DELETE_OBJECT_FOR_YACHT(YachtData.OptionObjectID[i])
			#IF IS_DEBUG_BUILD
				DECREMENT_YACHT_OBJECTS()
			#ENDIF	
		ENDIF
	ENDREPEAT	
	YachtData.bHullObjectsCreated = FALSE
	YachtData.bColourSet = FALSE
ENDPROC

PROC DELETE_OBJECTS_FOR_YACHT(YACHT_DATA &YachtData)

	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_YACHT, "DELETE_OBJECTS_FOR_YACHT - iYachtID ", YachtData.iLocation)
	PRINTLN_YACHT_APPEARANCE(YachtData.Appearance)
	#ENDIF
	
	INT i
	
	IF DOES_ENTITY_EXIST(YachtData.JacuzziObjectID)
		CPRINTLN(DEBUG_YACHT, "DELETE_OBJECTS_FOR_YACHT - JacuzziObjectID ")
		DELETE_OBJECT_FOR_YACHT(YachtData.JacuzziObjectID)	
		#IF IS_DEBUG_BUILD
			DECREMENT_YACHT_OBJECTS()
		#ENDIF	
	ENDIF
	
	REPEAT 3 i
		IF DOES_ENTITY_EXIST(YachtData.RadarObjectID[i])
			CPRINTLN(DEBUG_YACHT, "DELETE_OBJECTS_FOR_YACHT - RadarObjectID ", i)
			DELETE_OBJECT_FOR_YACHT(YachtData.RadarObjectID[i])	
			#IF IS_DEBUG_BUILD
				DECREMENT_YACHT_OBJECTS()
			#ENDIF	
		ENDIF		
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(YachtData.LightingObjectID)
		CPRINTLN(DEBUG_YACHT, "DELETE_OBJECTS_FOR_YACHT - LightingObjectID ")
		DELETE_OBJECT_FOR_YACHT(YachtData.LightingObjectID)
		#IF IS_DEBUG_BUILD
			DECREMENT_YACHT_OBJECTS()
		#ENDIF	
	ENDIF	

	IF DOES_ENTITY_EXIST(YachtData.RailingObjectID)
		CPRINTLN(DEBUG_YACHT, "DELETE_OBJECTS_FOR_YACHT - RailingObjectID")
		DELETE_OBJECT_FOR_YACHT(YachtData.RailingObjectID)
		#IF IS_DEBUG_BUILD
			DECREMENT_YACHT_OBJECTS()
		#ENDIF			
	ENDIF	

	IF DOES_ENTITY_EXIST(YachtData.FlagObjectID)
		CPRINTLN(DEBUG_YACHT, "DELETE_OBJECTS_FOR_YACHT - FlagObjectID")
		DELETE_OBJECT_FOR_YACHT(YachtData.FlagObjectID)
		#IF IS_DEBUG_BUILD
			DECREMENT_YACHT_OBJECTS()
		#ENDIF			
	ENDIF	

	IF DOES_ENTITY_EXIST(YachtData.PoleObjectID)
		CPRINTLN(DEBUG_YACHT, "DELETE_OBJECTS_FOR_YACHT - PoleObjectID")
		DELETE_OBJECT_FOR_YACHT(YachtData.PoleObjectID)
		#IF IS_DEBUG_BUILD
			DECREMENT_YACHT_OBJECTS()
		#ENDIF			
	ENDIF	
	
	// doors
	REPEAT 3 i
		IF DOES_ENTITY_EXIST(YachtData.DoorObjectID[i])
			CPRINTLN(DEBUG_YACHT, "DELETE_OBJECTS_FOR_YACHT - DoorObjectID ", i)
			DELETE_OBJECT_FOR_YACHT(YachtData.DoorObjectID[i])			
		ENDIF	
	ENDREPEAT
	
	REMOVE_ASSETS_FOR_YACHT(YachtData.Appearance)
	
	YachtData.bObjectsCreated = FALSE

ENDPROC

///// PURPOSE:
/////    State machine for controlling dummy prop used for fading in/fading the yacht
///// PARAMS:
/////    iYachtID - 
/////    LocalData - 
/////    dummyYacht - 
//PROC UPDATE_PRIVATE_YACHT_DUMMY(INT iYachtID, PRIVATE_YACHT_LOCAL_DATA &LocalData, OBJECT_INDEX &dummyYacht, OBJECT_INDEX &dummyYachtMast)
//	
//	MODEL_NAMES eYachtModel = GET_YACHT_MODEL()
//	MODEL_NAMES eYachtModelMast = MODEL_NAME_FOR_OPTION(LocalData.YachtData[iYachtID].Appearance.iOption, HP_HULL)
//	
//	IF NOT IS_PRIVATE_YACHT_ID_VALID(iYachtID)
//		CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DUMMY :: Yacht ID invalid")
//		EXIT
//	ENDIF
//	
//	BOOL bYachtIsActive = FALSE
//	
//	IF SHOULD_PRIVATE_YACHT_BE_ACTIVE(iYachtID, LocalData)
//		bYachtIsActive = TRUE
//	ENDIF
//	
//	// TODO Hardcoded offset
//	//VECTOR vCoords = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, << -2.500, -1.500, -5.0 >>)
//	//FLOAT fHeading = GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(iYachtID, 0.0) + 90.0
//	//IF (fHeading > 360.0)
//	//	fHeading = fHeading - 360.0
//	//ENDIF
//	
//	VECTOR vCoords = GET_MODEL_COORDS_OF_YACHT(iYachtID)
//	FLOAT fHeading = GET_MODEL_HEADING_OF_YACHT(iYachtID)
//	
//	IF LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_IDLE
//		/*
//		// FADING IN disabled for now
//		IF bYachtIsActive AND NOT LocalData.bYachtActiveInLastFrame[iYachtID]
//			// Yacht just been activated
//			LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_NEEDED
//			#IF IS_DEBUG_BUILD
//				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DUMMY :: Yacht has just been activated")
//			#ENDIF
//		ENDIF
//		*/
//		
//		//IF NOT bYachtIsActive AND LocalData.bYachtActiveInLastFrame[iYachtID]
//		IF NOT bYachtIsActive AND IS_PRIVATE_YACHT_ACTIVE(iYachtID)
//		
//			// Neil F: added this check as we only need the dummy if yachts in general are active, for example we have switched game modes then we just want a hard removal. 
//			IF SHOULD_PRIVATE_YACHTS_BE_ACTIVE()
//		
//				// Yacht just been deactivated
//				LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_NEEDED
//				#IF IS_DEBUG_BUILD
//					CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DUMMY :: Yacht has just been deactivated")
//				#ENDIF
//				
//			ENDIF
//		ENDIF
//		
//		// If player is on the yacht we can go straight to done
//		IF LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_NEEDED
//			IF IS_PLAYER_ON_YACHT(GET_PLAYER_INDEX(), iYachtID)
//			OR IS_PLAYER_DOING_A_YACHT_WARP(GET_PLAYER_INDEX())
//				#IF IS_DEBUG_BUILD
//					IF IS_PLAYER_DOING_A_YACHT_WARP(GET_PLAYER_INDEX())
//						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DUMMY :: Player is involved in yacht wrap, no need to fade it")
//					ENDIF
//					
//					IF IS_PLAYER_ON_YACHT(GET_PLAYER_INDEX(), iYachtID)
//						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DUMMY :: Player is on that yacht, no need to fade it")
//					ENDIF
//				#ENDIF
//			
//				LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_DONE
//				EXIT
//			ENDIF
//		ENDIF
//		
//		
//	ENDIF
//	
//	IF LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_NEEDED
//		IF NOT DOES_ENTITY_EXIST(dummyYacht)
//		OR NOT DOES_ENTITY_EXIST(dummyYachtMast)
//		
//			REQUEST_MODEL(eYachtModel)
//			REQUEST_MODEL(eYachtModelMast)
//			
//			#IF IS_DEBUG_BUILD
//				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DUMMY :: Waiting for the yacht model to load")
//			#ENDIF
//			
//			IF HAS_MODEL_LOADED(eYachtModel)
//			AND HAS_MODEL_LOADED(eYachtModelMast)
//			
//				// Check if the yacht would even be visible, otherwise skip dummy creation
//				IF WOULD_ENTITY_BE_OCCLUDED(eYachtModel, vCoords)
//					#IF IS_DEBUG_BUILD
//						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DUMMY :: Yacht not visible, skipping fade-in")
//					#ENDIF
//				
//					LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_DONE
//					EXIT
//				ENDIF
//					
//				#IF IS_DEBUG_BUILD
//					CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DUMMY :: Model loaded, trying to create object")
//				#ENDIF
//				
//				dummyYacht = CREATE_OBJECT_NO_OFFSET(eYachtModel, vCoords, FALSE, FALSE)
//				dummyYachtMast = CREATE_OBJECT_NO_OFFSET(eYachtModelMast, <<vCoords.x, vCoords.y, 20.0>>, FALSE, FALSE)
//										
//				SET_ENTITY_ROTATION(dummyYacht, <<0.0, 0.0, fHeading>>)	
//				SET_ENTITY_VISIBLE(dummyYacht, FALSE)
//				FREEZE_ENTITY_POSITION(dummyYacht, TRUE)
//				SET_ENTITY_ALPHA(dummyYacht, 0, FALSE)
//				SET_ENTITY_COLLISION(dummyYacht, FALSE, TRUE)
//				
//				SET_ENTITY_LOD_DIST(dummyYacht, 2400)
//				SET_ENTITY_ALWAYS_PRERENDER(dummyYacht, TRUE)
//				SET_ENTITY_COLLISION(dummyYacht, FALSE, TRUE)
//				
//				IF NOT bYachtIsActive
//					SET_ENTITY_VISIBLE(dummyYacht, TRUE)
//					SET_ENTITY_ALPHA(dummyYacht, 255, FALSE)
//					
//					SET_OBJECT_TINT_INDEX(dummyYacht, LocalData.YachtData[iYachtID].Appearance.iTint)	// set correct colour
//				ENDIF
//				
//				#IF IS_DEBUG_BUILD
//					INCREMENT_YACHT_OBJECTS()
//				#ENDIF
//				
//				SET_ENTITY_ROTATION(dummyYachtMast, <<0.0, 0.0, fHeading>>)	
//				SET_ENTITY_VISIBLE(dummyYachtMast, FALSE)
//				FREEZE_ENTITY_POSITION(dummyYachtMast, TRUE)
//				SET_ENTITY_ALPHA(dummyYachtMast, 0, FALSE)
//				SET_ENTITY_COLLISION(dummyYachtMast, FALSE, TRUE)
//				
//				SET_ENTITY_LOD_DIST(dummyYachtMast, 2400)
//				SET_ENTITY_ALWAYS_PRERENDER(dummyYachtMast, TRUE)
//				SET_ENTITY_COLLISION(dummyYachtMast, FALSE, TRUE)
//				
//				IF NOT bYachtIsActive
//					SET_ENTITY_VISIBLE(dummyYachtMast, TRUE)
//					SET_ENTITY_ALPHA(dummyYachtMast, 255, FALSE)
//					
//					SET_OBJECT_TINT_INDEX(dummyYachtMast, LocalData.YachtData[iYachtID].Appearance.iTint)	// set correct colour
//				ENDIF
//				
//				#IF IS_DEBUG_BUILD
//					INCREMENT_YACHT_OBJECTS()
//				#ENDIF
//			ENDIF
//		ELSE
//			#IF IS_DEBUG_BUILD
//				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DUMMY :: Dummy created")
//			#ENDIF
//			
//			LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_SPAWNED
//		ENDIF
//	ENDIF
//	
//	IF LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_SPAWNED
//		IF GET_ENTITY_MODEL(dummyYacht) = eYachtModel
//			// Fading starts!
//			#IF IS_DEBUG_BUILD
//				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DUMMY :: Model swap successful, starting the fade")
//			#ENDIF
//			
//			SET_ENTITY_VISIBLE(dummyYacht, TRUE)
//			SET_ENTITY_LOD_DIST(dummyYacht, 2400)
//			SET_ENTITY_ALWAYS_PRERENDER(dummyYacht, TRUE)
//			SET_ENTITY_COLLISION(dummyYacht, FALSE, TRUE)
//			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(dummyYacht)
//			
//			LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_WAITING
//			
//			IF bYachtIsActive
//				// If yacht is active then we can go straight to animation, no need to wait for IPLS
//				LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_ANIMATING
//				LocalData.iYachtFadeTimer[iYachtID] = GET_GAME_TIMER()
//			ELSE
//				SET_ENTITY_VISIBLE(dummyYacht, TRUE)
//				SET_ENTITY_ALPHA(dummyYacht, 255, FALSE)
//				
//				SET_ENTITY_VISIBLE(dummyYachtMast, TRUE)
//				SET_ENTITY_ALPHA(dummyYachtMast, 255, FALSE)
//			ENDIF
//		ELSE
//			// If someone uses old data which didnt allow for yacht prop creation then skip to done
//			LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_DONE
//		ENDIF
//	ENDIF
//	
//	IF LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_WAITING
//		IF NOT bYachtIsActive
//			// If yacht isnt active we need to wait for the IPL to unload before we start animation
//			IF NOT ARE_IPLS_ACTIVE_FOR_PRIVATE_YACHT(iYachtID)
//				LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_ANIMATING
//				LocalData.iYachtFadeTimer[iYachtID] = GET_GAME_TIMER()
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	IF LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_ANIMATING
//		
//	ENDIF
//	
//	IF LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_DONE
//		IF bYachtIsActive
//			IF ARE_IPLS_ACTIVE_FOR_PRIVATE_YACHT(iYachtID)
//				#IF IS_DEBUG_BUILD
//					CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DUMMY :: IPLS are active, safe to remove the dummy")
//				#ENDIF
//				LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_CLEANUP
//				EXIT
//			ENDIF
//		ELSE
//			#IF IS_DEBUG_BUILD
//				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DUMMY :: Dummy faded out, yacht is not active so safe to remove")
//			#ENDIF
//			LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_CLEANUP
//			EXIT
//		ENDIF
//	ENDIF
//	
//	IF LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_CLEANUP
//		IF DOES_ENTITY_EXIST(dummyYacht)
//
//			BOOL bDoCleanup = FALSE
//			
//			IF bYachtIsActive
//				// Give IPLs a sec to start streaming before killing the dummy
//				//IF LocalData.iYachtFadeTimer[iYachtID] - PRIVATE_YACHT_FADE_IN_DURATION > 2500
//				bDoCleanup = TRUE
//				//ENDIF
//			ELSE
//				bDoCleanup = TRUE
//			ENDIF
//		
//			IF bDoCleanup
//				#IF IS_DEBUG_BUILD
//					CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DUMMY :: Removing the dummy")
//				#ENDIF
//				DELETE_OBJECT(dummyYacht)
//				SET_MODEL_AS_NO_LONGER_NEEDED(eYachtModel)
//				
//				DELETE_OBJECT(dummyYachtMast)
//				SET_MODEL_AS_NO_LONGER_NEEDED(eYachtModelMast)
//				
//				#IF IS_DEBUG_BUILD
//					CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DUMMY :: Cleanup done! Returning to idle")
//				#ENDIF
//				
//				#IF IS_DEBUG_BUILD
//					DECREMENT_YACHT_OBJECTS()
//				#ENDIF
//				#IF IS_DEBUG_BUILD
//					DECREMENT_YACHT_OBJECTS()
//				#ENDIF
//				
//				DELETE_OBJECTS_FOR_YACHT(LocalData.YachtData[iYachtID])
//				
//				LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_IDLE
//			ENDIF
//		ELSE
//			#IF IS_DEBUG_BUILD
//				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DUMMY :: Dummy reached state of cleanup with non-existent dummy model... Something probably went wrong")
//			#ENDIF
//			LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_IDLE
//		ENDIF
//	ENDIF
//	
//	//LocalData.bYachtActiveInLastFrame[iYachtID] = bYachtIsActive
//	
//ENDPROC

/// PURPOSE:
///    Update yacht dummy alpha animation for fading in/out
/// PARAMS:
///    LocalData - 
//PROC UPDATE_PRIVATE_YACHT_DUMMY_ANIMATIONS(PRIVATE_YACHT_LOCAL_DATA &LocalData)
//	INT i
//	ENTITY_INDEX dummyYacht
//	ENTITY_INDEX dummyYachtMast
//	INT iAlpha
//	
//	REPEAT NUMBER_OF_PRIVATE_YACHTS i
//		//UPDATE_PRIVATE_YACHT_DUMMY(i, LocalData, LocalData.oYachtDummies[i])
//		
//		IF LocalData.iYachtDummyState[i] = PRIVATE_YACHT_DUMMY_ANIMATING
//			dummyYacht = LocalData.oYachtDummies[i]
//			dummyYachtMast = LocalData.oYachtDummiesMast[i]
//			
//			FLOAT fDelta = TO_FLOAT(GET_GAME_TIMER() - LocalData.iYachtFadeTimer[i]) / TO_FLOAT(PRIVATE_YACHT_FADE_IN_DURATION)
//				
//			IF fDelta > 1.0
//				LocalData.iYachtDummyState[i] = PRIVATE_YACHT_DUMMY_DONE
//				
//				#IF IS_DEBUG_BUILD
//				 	CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DUMMY_ANIMATIONS :: Transition done for yacht ", i)
//				#ENDIF
//			ELSE
//				// Fade in if the yacht is active, otherwise fade out
//				IF SHOULD_PRIVATE_YACHT_BE_ACTIVE(i, LocalData)
//					iAlpha = ROUND(INTERP_FLOAT(0.0, 255.0, fDelta))
//				ELSE
//					iAlpha = ROUND(INTERP_FLOAT(255.0, 0.0, fDelta))
//				ENDIF
//				
//				SET_ENTITY_ALPHA(dummyYacht, iAlpha, FALSE)
//				SET_ENTITY_ALPHA(dummyYachtMast, iAlpha, FALSE)
//				SET_YACHT_OBJECTS_ALPHA(i, LocalData, iAlpha, FALSE)
//				
//				#IF IS_DEBUG_BUILD
//					CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_DUMMY_ANIMATIONS :: Setting yacht dummy alpha ", i, " ", iAlpha)
//				#ENDIF
//			ENDIF
//		ENDIF
//	ENDREPEAT
//ENDPROC


//PROC RELEASE_HULL_IPL_OBJECTS(INT iYachtID, PRIVATE_YACHT_LOCAL_DATA &LocalData)
//	IF DOES_ENTITY_EXIST(LocalData.YachtData[iYachtID].HullObjectID)
//		IF IS_ENTITY_A_MISSION_ENTITY(LocalData.YachtData[iYachtID].HullObjectID)
//			SET_OBJECT_AS_NO_LONGER_NEEDED(LocalData.YachtData[iYachtID].HullObjectID)
//			CPRINTLN(DEBUG_YACHT, "RELEASE_HULL_IPL_OBJECTS - releasing for yacht ", iYachtID)
//		ELSE
//			CPRINTLN(DEBUG_YACHT, "RELEASE_HULL_IPL_OBJECTS - not a mission entity for yacht ", iYachtID)
//		ENDIF
//	ELSE
//		CPRINTLN(DEBUG_YACHT, "RELEASE_HULL_IPL_OBJECTS - does not exist for yacht ", iYachtID)
//	ENDIF
//ENDPROC

FUNC BOOL DoPrivateYachtRemoveIPLCheck(INT iYachtID, PRIVATE_YACHT_LOCAL_DATA &LocalData)
	// if for some reason the ipl's are already loaded then unload them
	IF NOT (LocalData.bDoneIPLRemovalCheck[iYachtID])
		IF ARE_ANY_IPLS_ACTIVE_FOR_PRIVATE_YACHT(iYachtID)
			CPRINTLN(DEBUG_YACHT, "DoPrivateYachtRemoveIPLCheck - ipl were loaded, removing. ", iYachtID)					
			//RELEASE_HULL_IPL_OBJECTS(iYachtID, LocalData)	
			REMOVE_IPLS_FOR_PRIVATE_YACHT(iYachtID)
			RETURN(TRUE)
		ELSE
			CPRINTLN(DEBUG_YACHT, "DoPrivateYachtRemoveIPLCheck - ipl were NOT loaded ", iYachtID)	
		ENDIF					
		LocalData.bDoneIPLRemovalCheck[iYachtID] = TRUE	
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC SET_OBJECT_SETTINGS_FOR_YACHT(ENTITY_INDEX EntityID)
	IF NOT IS_ENTITY_ATTACHED(EntityID)
		FREEZE_ENTITY_POSITION(EntityID, TRUE) 
	ENDIF
	SET_ENTITY_DYNAMIC(EntityID, FALSE)
	SET_ENTITY_VISIBLE(EntityID, TRUE)
	CLEAR_ROOM_FOR_ENTITY(EntityID)
ENDPROC

//PROC UpdateAllYachtObjects(INT iYachtID, PRIVATE_YACHT_LOCAL_DATA &LocalData)
PROC UpdateAllYachtObjects(YACHT_DATA &YachtData)	
	
	CPRINTLN(DEBUG_YACHT, "UpdateAllYachtObjects - called for yacht ")
	
	INT i
	REPEAT ENUM_TO_INT(HP_TOTAL_HULL_PARTS) i
		IF DOES_ENTITY_EXIST(YachtData.OptionObjectID[i])
			SET_OBJECT_SETTINGS_FOR_YACHT(YachtData.OptionObjectID[i])
			
			// apply colour tint
			IF (i = ENUM_TO_INT(HP_HULL))
				SET_OBJECT_TINT_INDEX(YachtData.OptionObjectID[i], YachtData.Appearance.iTint)	
			ENDIF
			
		ENDIF
	ENDREPEAT
	
	// jacuzzi
	IF DOES_ENTITY_EXIST(YachtData.JacuzziObjectID)
		SET_OBJECT_SETTINGS_FOR_YACHT(YachtData.JacuzziObjectID)			
		// apply colour tint
		SET_OBJECT_TINT_INDEX(YachtData.JacuzziObjectID, YachtData.Appearance.iTint)
	ENDIF
	
	// radar
	REPEAT 3 i
		IF DOES_ENTITY_EXIST(YachtData.RadarObjectID[i])
			SET_OBJECT_SETTINGS_FOR_YACHT(YachtData.RadarObjectID[i])
			
			// apply colour tint
			SET_OBJECT_TINT_INDEX(YachtData.RadarObjectID[i], YachtData.Appearance.iTint)
		ENDIF
	ENDREPEAT
	
	// lighting
	IF DOES_ENTITY_EXIST(YachtData.LightingObjectID)
		SET_OBJECT_SETTINGS_FOR_YACHT(YachtData.LightingObjectID)
		
		SET_ENTITY_WATER_REFLECTION_FLAG(YachtData.LightingObjectID, FALSE) 
	ENDIF
	
	// railing
	IF DOES_ENTITY_EXIST(YachtData.RailingObjectID)
		SET_OBJECT_SETTINGS_FOR_YACHT(YachtData.RailingObjectID)
	ENDIF
	
	// flagpole
	IF DOES_ENTITY_EXIST(YachtData.PoleObjectID)
		SET_OBJECT_SETTINGS_FOR_YACHT(YachtData.PoleObjectID)
		SET_ENTITY_VISIBLE(YachtData.PoleObjectID, FALSE)
	ENDIF
	
	// flag
	IF DOES_ENTITY_EXIST(YachtData.FlagObjectID)
		SET_OBJECT_SETTINGS_FOR_YACHT(YachtData.FlagObjectID)
	ENDIF
	
	// doors
	REPEAT 3 i
		IF DOES_ENTITY_EXIST(YachtData.DoorObjectID[i])
			SET_OBJECT_TINT_INDEX(YachtData.DoorObjectID[i], YachtData.Appearance.iTint)
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC  UPDATE_YACHT_OBJECTS(YACHT_DATA &YachtData)
	
	BOOL bPerformUpdate = FALSE

	// should we do an update?
	IF IS_NEW_LOAD_SCENE_ACTIVE()
	OR IS_PLAYER_TELEPORT_ACTIVE()
	OR NOT IS_SKYSWOOP_AT_GROUND()
	OR (YachtData.bUpdateObjects)
	
		#IF IS_DEBUG_BUILD
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				CPRINTLN(DEBUG_YACHT, "UPDATE_YACHT_OBJECTS - IS_NEW_LOAD_SCENE_ACTIVE - yacht ")
			ENDIF
			IF IS_PLAYER_TELEPORT_ACTIVE()
				CPRINTLN(DEBUG_YACHT, "UPDATE_YACHT_OBJECTS - IS_PLAYER_TELEPORT_ACTIVE - yacht ")
			ENDIF
			IF NOT IS_SKYSWOOP_AT_GROUND()
				CPRINTLN(DEBUG_YACHT, "UPDATE_YACHT_OBJECTS - NOT IS_SKYSWOOP_AT_GROUND() - yacht ")
			ENDIF
			IF (YachtData.bUpdateObjects)
				CPRINTLN(DEBUG_YACHT, "UPDATE_YACHT_OBJECTS - bUpdateObjects - yacht ")
			ENDIF
		#ENDIF
	
		bPerformUpdate = TRUE	
	ENDIF
	
	IF (bPerformUpdate)
		UpdateAllYachtObjects(YachtData)
	ENDIF
	
	YachtData.bUpdateObjects = FALSE

ENDPROC


PROC UPDATE_YACHT_COLOUR(YACHT_DATA &YachtData, BOOL bIgnoreRequestCount=FALSE )
	VECTOR vCoords
	INT iAlpha
	// set colour of hull
	IF (YachtData.bColourSet = FALSE)
		IF HAS_YACHT_MODEL_LOADED(bIgnoreRequestCount)
			MODEL_NAMES eYachtModel = GET_YACHT_MODEL()
			
			IF YachtData.iLocation = 36
				eYachtModel = INT_TO_ENUM(MODEL_NAMES, HASH("AC_MP_APA_YACHT"))
			ELIF YachtData.iLocation = 37
				eYachtModel = INT_TO_ENUM(MODEL_NAMES, HASH("H4_MP_APA_YACHT"))
			ELIF YachtData.iLocation = 38
				eYachtModel = INT_TO_ENUM(MODEL_NAMES, HASH("H4_MP_APA_YACHT"))
			ELIF YachtData.iLocation = 39
				eYachtModel = INT_TO_ENUM(MODEL_NAMES, HASH("H4_MP_APA_YACHT"))
			#IF FEATURE_FIXER
			ELIF YachtData.iLocation = 40
				eYachtModel = INT_TO_ENUM(MODEL_NAMES, HASH("SF_MP_APA_YACHT"))
			ELIF YachtData.iLocation = 41
				eYachtModel = INT_TO_ENUM(MODEL_NAMES, HASH("SF_MP_APA_YACHT"))
			#ENDIF
			ENDIF
			
			vCoords = GET_MODEL_COORDS_OF_YACHT(YachtData.iLocation)
			YachtData.HullObjectID = GET_CLOSEST_OBJECT_OF_TYPE(vCoords, 100.0, eYachtModel, FALSE, FALSE, FALSE) 
			IF DOES_ENTITY_EXIST(YachtData.HullObjectID)		
				iAlpha = GET_ENTITY_ALPHA(YachtData.HullObjectID)
				IF (iAlpha = 255)
					CPRINTLN(DEBUG_YACHT, "UPDATE_YACHT_COLOUR - setting hull colour at ", vCoords, ",  to colour ", YachtData.Appearance.iTint)
					SET_OBJECT_TINT_INDEX(YachtData.HullObjectID, YachtData.Appearance.iTint)	
					YachtData.bColourSet = TRUE
					YachtData.bUpdateObjects = TRUE // update the objects
					
					GB_SET_GLOBAL_NON_BD_BIT1(eGB_GLOBAL_NON_BD_BITSET_1_DISABLE_UPDATE_YACHT_COLOUR)
				ELSE
					CPRINTLN(DEBUG_YACHT, "UPDATE_YACHT_COLOUR - alhpa ", iAlpha, " for yacht ", YachtData.iLocation)
				eNDIF
			ELSE
				//CPRINTLN(DEBUG_YACHT, "UPDATE_YACHT_COLOUR - cant get closest object for yacht ", YachtData.iLocation, " coords ", vCoords)
			ENDIF	
		ENDIF
	ELSE
		// if objects have been deleted then this flag should be reset
		IF NOT (YachtData.bHullObjectsCreated)
			YachtData.bColourSet = FALSE
			CPRINTLN(DEBUG_YACHT, "UPDATE_YACHT_COLOUR - objects no longer exist for yacht ", YachtData.iLocation)
		ENDIF
		IF DOES_ENTITY_EXIST(YachtData.HullObjectID)
			iAlpha = GET_ENTITY_ALPHA(YachtData.HullObjectID)
			IF NOT (iAlpha = 255)
				YachtData.bColourSet = FALSE
				CPRINTLN(DEBUG_YACHT, "UPDATE_YACHT_COLOUR - alpha no longer 255.  ", YachtData.iLocation)
			ENDIF
		ELSE
			YachtData.bColourSet = FALSE
			CPRINTLN(DEBUG_YACHT, "UPDATE_YACHT_COLOUR - entity doesnt exist ", YachtData.iLocation)
		ENDIF
	ENDIF
	
ENDPROC




PROC UPDATE_CLEANUP_ALL_STORED_YACHT_VEHICLES(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	INT iVehicle
	INT iCount
	BOOL bSetAsMissionEntity
	IF (LocalData.bCleanupAllStoredYachtVehicles)
	
		INT iYachtID = LOCAL_PLAYER_PRIVATE_YACHT_ID()
	
		REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES iVehicle
			IF DOES_ENTITY_EXIST(LocalData.StoredVehicleID[iVehicle])
			
				IF IS_VEHICLE_ON_YACHT(LocalData.StoredVehicleID[iVehicle], iYachtID, YACHT_INCLUDING_VEHICLES_DISTANCE)
					
					IF NETWORK_HAS_CONTROL_OF_ENTITY(LocalData.StoredVehicleID[iVehicle])
						IF CAN_EDIT_THIS_ENTITY (LocalData.StoredVehicleID[iVehicle], bSetAsMissionEntity)
							IF IS_VEHICLE_EMPTY(LocalData.StoredVehicleID[iVehicle])
								DELETE_VEHICLE(LocalData.StoredVehicleID[iVehicle])
								CPRINTLN(DEBUG_YACHT, "UPDATE_CLEANUP_ALL_STORED_YACHT_VEHICLES - deleting vehicle ", iVehicle)
							ELSE
								SET_VEHICLE_AS_NO_LONGER_NEEDED(LocalData.StoredVehicleID[iVehicle])
								CPRINTLN(DEBUG_YACHT, "UPDATE_CLEANUP_ALL_STORED_YACHT_VEHICLES - set veh as not needed ", iVehicle)
							ENDIF
							
						ELSE
							IF (bSetAsMissionEntity)
								IF IS_VEHICLE_EMPTY(LocalData.StoredVehicleID[iVehicle])
									DELETE_VEHICLE(LocalData.StoredVehicleID[iVehicle])
									CPRINTLN(DEBUG_YACHT, "UPDATE_CLEANUP_ALL_STORED_YACHT_VEHICLES - cannot edit but deleting vehicle ", iVehicle)
								ELSE
									SET_VEHICLE_AS_NO_LONGER_NEEDED(LocalData.StoredVehicleID[iVehicle])
									CPRINTLN(DEBUG_YACHT, "UPDATE_CLEANUP_ALL_STORED_YACHT_VEHICLES - cannot edit but set veh as not needed ", iVehicle)
								ENDIF
							ELSE
								CPRINTLN(DEBUG_YACHT, "UPDATE_CLEANUP_ALL_STORED_YACHT_VEHICLES - cannot edit or make mission entity. ", iVehicle)														
							ENDIF
						ENDIF
						LocalData.StoredVehicleID[iVehicle] = NULL	
					ELSE
						iCount++
						NETWORK_REQUEST_CONTROL_OF_ENTITY(LocalData.StoredVehicleID[iVehicle])
						CPRINTLN(DEBUG_YACHT, "UPDATE_CLEANUP_ALL_STORED_YACHT_VEHICLES - requesting control. ", iVehicle)
					ENDIF
				
				ELSE
					LocalData.StoredVehicleID[iVehicle] = NULL
					CPRINTLN(DEBUG_YACHT, "UPDATE_CLEANUP_ALL_STORED_YACHT_VEHICLES - nulling vehicle id, off yacht. ", iVehicle)
				ENDIF

			ELSE
				CPRINTLN(DEBUG_YACHT, "UPDATE_CLEANUP_ALL_STORED_YACHT_VEHICLES - does not exist. ", iVehicle)
			ENDIF
		ENDREPEAT
		
		IF (iCount = 0)
			
			// make sure all the state are reset back to 0
			REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES iVehicle
				LocalData.iVehicleSpawnState[iVehicle] = 0
			ENDREPEAT
		
			LocalData.bCleanupAllStoredYachtVehicles = FALSE
			CPRINTLN(DEBUG_YACHT, "UPDATE_CLEANUP_ALL_STORED_YACHT_VEHICLES - finished updating all vehicles.")
		ELSE
			CPRINTLN(DEBUG_YACHT, "UPDATE_CLEANUP_ALL_STORED_YACHT_VEHICLES - icount = ", iCount)
		ENDIF
		
	ENDIF
ENDPROC

PROC CLEANUP_ALL_STORED_YACHT_VEHICLES(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	CPRINTLN(DEBUG_YACHT, "CLEANUP_ALL_STORED_YACHT_VEHICLES - called.")
	INT iVehicle
	REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES iVehicle
		IF DOES_ENTITY_EXIST(LocalData.StoredVehicleID[iVehicle])
			SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_CLEANUP, TRUE)
		ELSE
			LocalData.iVehicleSpawnState[iVehicle] = 0
		ENDIF
		
		// update vehicle now so they get deleted qicker
		UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING(LocalData, iVehicle)

	ENDREPEAT
	
	LocalData.bCleanupAllStoredYachtVehicles = TRUE
	UPDATE_CLEANUP_ALL_STORED_YACHT_VEHICLES(LocalData)
		
ENDPROC

PROC RESET_VEHICLES_ON_MY_YACHT(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	CPRINTLN(DEBUG_YACHT, " RESET_VEHICLES_ON_MY_YACHT - called.")
	INT iYachtID = LOCAL_PLAYER_PRIVATE_YACHT_ID()
	IF (iYachtID > -1)
		INIT_YACHT_VEHICLE_FLAGS(GET_OPTION_OF_PRIVATE_YACHT(iYachtID))		
		CLEANUP_ALL_STORED_YACHT_VEHICLES(LocalData)
	ELSE
		CPRINTLN(DEBUG_YACHT, " RESET_VEHICLES_ON_MY_YACHT - my yacht id = -1")	
	ENDIF
ENDPROC

PROC CREATE_HULL_OBJECTS_FOR_YACHT(INT iYachtLocation, YACHT_DATA &YachtData)
	VECTOR vCoords
	FLOAT fHeading
	
	#IF IS_DEBUG_BUILD	
		DEBUG_PRINTCALLSTACK()
		CPRINTLN(DEBUG_YACHT, "CREATE_HULL_OBJECTS_FOR_YACHT - iYachtLocation ", iYachtLocation)
		PRINTLN_YACHT_APPEARANCE(YachtData.Appearance)
		
		IF (iYachtLocation != YachtData.iLocation)
		AND (YachtData.iLocation != -1)
			CPRINTLN(DEBUG_YACHT, "CREATE_HULL_OBJECTS_FOR_YACHT - location has changed from ", YachtData.iLocation)	
		ENDIF
	#ENDIF
	
	YachtData.iLocation = iYachtLocation
	
	IF (YachtData.bHullObjectsCreated)
		DELETE_HULL_OBJECTS_FOR_YACHT(YachtData)
	ENDIF	
	
	// hull 
	INT i
	REPEAT ENUM_TO_INT(HP_TOTAL_HULL_PARTS) i
		IF DOES_OPTION_HAVE_HULL_PART(YachtData.Appearance.iOption, INT_TO_ENUM(HULL_PARTS, i))			
//			IF DOES_HULL_PART_USE_OFFSETS(INT_TO_ENUM(HULL_PARTS, i))
//				GET_OFFSET_FOR_HULL_PART(INT_TO_ENUM(HULL_PARTS, i), vOffset, fOffsetHeading)
//				vCoords = GET_OFFSET_FROM_YACHT_MODEL_IN_WORLD_COORDS(iYachtID, vOffset)
//				fHeading = GET_OFFSET_HEADING_FROM_YACHT_MODEL_AS_WORLD_HEADING(iYachtID, fOffsetHeading)
//			ELSE
				vCoords = GET_MODEL_COORDS_OF_YACHT(iYachtLocation)
				vCoords.z = 20.0 // this is now fixed.
				fHeading = GET_MODEL_HEADING_OF_YACHT(iYachtLocation)
//			ENDIF			
			CPRINTLN(DEBUG_YACHT, "CREATE_HULL_OBJECTS_FOR_YACHT - OptionObjectID ", i, " at ", vCoords, ", ", fHeading, " || ", GET_MODEL_NAME_FOR_DEBUG(MODEL_NAME_FOR_OPTION(YachtData.Appearance.iOption, INT_TO_ENUM(HULL_PARTS, i))))
			YachtData.OptionObjectID[i] = CREATE_OBJECT_NO_OFFSET(MODEL_NAME_FOR_OPTION(YachtData.Appearance.iOption, INT_TO_ENUM(HULL_PARTS, i)),  vCoords, FALSE, FALSE)
			SET_ENTITY_HEADING(YachtData.OptionObjectID[i], fHeading)
			SET_OBJECT_SETTINGS_FOR_YACHT(YachtData.OptionObjectID[i])
			#IF IS_DEBUG_BUILD
				INCREMENT_YACHT_OBJECTS()
			#ENDIF
		ENDIF
	ENDREPEAT	
	
	YachtData.bHullObjectsCreated = TRUE
	
ENDPROC


FUNC MODEL_NAMES DOOR_MODEL_FOR_YACHT(YACHT_DATA &YachtData)
	IF YachtData.Appearance.iRailing = 0
		RETURN INT_TO_ENUM(MODEL_NAMES,HASH("apa_mp_apa_yacht_door"))
	ELSE
		RETURN INT_TO_ENUM(MODEL_NAMES,HASH("apa_mp_apa_yacht_door2"))
	ENDIF
ENDFUNC


PROC CREATE_OBJECTS_FOR_YACHT(INT iYachtLocation, YACHT_DATA &YachtData, BOOL bObjectsDisabled, BOOL bCreateDoors=FALSE)
	
	VECTOR vCoords
	FLOAT fHeading
	VECTOR vOffset, vOffsetRot
	FLOAT fOffsetHeading	
	
	#IF IS_DEBUG_BUILD	
		DEBUG_PRINTCALLSTACK()
		CPRINTLN(DEBUG_YACHT, "CREATE_OBJECTS_FOR_YACHT - iYachtLocation ", iYachtLocation)
		PRINTLN_YACHT_APPEARANCE(YachtData.Appearance)

		IF (iYachtLocation != YachtData.iLocation)
		AND (YachtData.iLocation != -1)
			CPRINTLN(DEBUG_YACHT, "CREATE_OBJECTS_FOR_YACHT - location has changed from ", YachtData.iLocation)	
		ENDIF		
		
	#ENDIF
	
	YachtData.iLocation = iYachtLocation
	
	IF (YachtData.bObjectsCreated)
		DELETE_OBJECTS_FOR_YACHT(YachtData)
	ENDIF	
	
	// make sure the hull objects are created
	IF NOT (YachtData.bHullObjectsCreated)
		CREATE_HULL_OBJECTS_FOR_YACHT(iYachtLocation, YachtData)
	ENDIF
	
	IF NOT (bObjectsDisabled)
	
		// jacuzzi
		IF (YachtData.Appearance.iOption > 0)
		#IF FEATURE_FIXER
		AND YachtData.iLocation != 40
		AND YachtData.iLocation != 41
		#ENDIF
			GET_OFFSET_FOR_JACUZZI(vOffset, fOffsetHeading)
			vCoords = GET_OFFSET_FROM_YACHT_MODEL_IN_WORLD_COORDS(iYachtLocation, vOffset)
			fHeading = GET_OFFSET_HEADING_FROM_YACHT_MODEL_AS_WORLD_HEADING(iYachtLocation, fOffsetHeading)
			CPRINTLN(DEBUG_YACHT, "CREATE_OBJECTS_FOR_YACHT - JacuzziObjectID at ", vCoords, ", ", fHeading)
			YachtData.JacuzziObjectID = CREATE_OBJECT_NO_OFFSET(MODEL_NAME_FOR_JACUZZI(),  vCoords, FALSE, FALSE)
			SET_ENTITY_HEADING(YachtData.JacuzziObjectID, fHeading)		
			SET_OBJECT_SETTINGS_FOR_YACHT(YachtData.JacuzziObjectID)
			#IF IS_DEBUG_BUILD
				INCREMENT_YACHT_OBJECTS()
			#ENDIF		
		ENDIF	

		// radar
		INT i
		REPEAT 3 i
			GET_OFFSET_FOR_RADAR(YachtData.Appearance.iOption, i, vOffset, fOffsetHeading)
			vCoords = GET_OFFSET_FROM_YACHT_MODEL_IN_WORLD_COORDS(iYachtLocation, vOffset)
			fHeading = GET_OFFSET_HEADING_FROM_YACHT_MODEL_AS_WORLD_HEADING(iYachtLocation, fOffsetHeading)
			CPRINTLN(DEBUG_YACHT, "CREATE_OBJECTS_FOR_YACHT - RadarObjectID ", i, " at ", vCoords, ", ", fHeading)
			YachtData.RadarObjectID[i] = CREATE_OBJECT_NO_OFFSET(MODEL_NAME_FOR_RADAR(),  vCoords, FALSE, FALSE)
			SET_ENTITY_HEADING(YachtData.RadarObjectID[i], fHeading)		
			SET_OBJECT_SETTINGS_FOR_YACHT(YachtData.RadarObjectID[i])
			#IF IS_DEBUG_BUILD
				INCREMENT_YACHT_OBJECTS()
			#ENDIF
		ENDREPEAT
		
		// lighting
		vCoords = GET_MODEL_COORDS_OF_YACHT(iYachtLocation)
		fHeading = GET_MODEL_HEADING_OF_YACHT(iYachtLocation)
		vCoords.z = 20.005
		CPRINTLN(DEBUG_YACHT, "CREATE_OBJECTS_FOR_YACHT - LightingObjectID at ", vCoords, ", ", fHeading)
		YachtData.LightingObjectID = CREATE_OBJECT_NO_OFFSET(MODEL_NAME_FOR_LIGHTING(YachtData.Appearance.iOption, YachtData.Appearance.iLighting),  vCoords, FALSE, FALSE)		
		SET_ENTITY_HEADING(YachtData.LightingObjectID, fHeading)
		SET_OBJECT_SETTINGS_FOR_YACHT(YachtData.LightingObjectID)
		#IF IS_DEBUG_BUILD
			INCREMENT_YACHT_OBJECTS()
		#ENDIF	
		
		// railing
		vCoords = GET_MODEL_COORDS_OF_YACHT(iYachtLocation)
		fHeading = GET_MODEL_HEADING_OF_YACHT(iYachtLocation)
		vCoords.z = 20.0
		CPRINTLN(DEBUG_YACHT, "CREATE_OBJECTS_FOR_YACHT - RailingObjectID at ", vCoords, ", ", fHeading)
		YachtData.RailingObjectID = CREATE_OBJECT_NO_OFFSET(MODEL_NAME_FOR_RAILING(YachtData.Appearance.iOption, YachtData.Appearance.iRailing),  vCoords, FALSE, FALSE)	
		SET_ENTITY_HEADING(YachtData.RailingObjectID, fHeading)
		SET_OBJECT_SETTINGS_FOR_YACHT(YachtData.RailingObjectID)
		#IF IS_DEBUG_BUILD
			INCREMENT_YACHT_OBJECTS()
		#ENDIF	
		
		// flagpole
		IF (YachtData.Appearance.iFlag > -1)
			GET_OFFSET_FOR_FLAGPOLE(vOffset, vOffsetRot)
			vCoords = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtLocation, vOffset)
			fHeading = GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(iYachtLocation, vOffsetRot.z)
			CPRINTLN(DEBUG_YACHT, "CREATE_OBJECTS_FOR_YACHT - PoleObjectID at ", vCoords, ", ", fHeading)
			YachtData.PoleObjectID = CREATE_OBJECT_NO_OFFSET(MODEL_NAME_FOR_YACHT_FLAGPOLE(), vCoords, FALSE, FALSE)	
			SET_ENTITY_ROTATION(YachtData.PoleObjectID, <<vOffsetRot.x, vOffsetRot.y, fHeading>>)
			SET_OBJECT_SETTINGS_FOR_YACHT(YachtData.PoleObjectID)
			SET_ENTITY_VISIBLE(YachtData.PoleObjectID, FALSE)
			#IF IS_DEBUG_BUILD
				INCREMENT_YACHT_OBJECTS()
			#ENDIF
			
			// flag
			CPRINTLN(DEBUG_YACHT, "CREATE_OBJECTS_FOR_YACHT - FlagObjectID ", YachtData.Appearance.iFlag, " - ", GET_MODEL_NAME_FOR_DEBUG(MODEL_NAME_FOR_YACHT_FLAG(YachtData.Appearance.iFlag)))
			YachtData.FlagObjectID = CREATE_OBJECT_NO_OFFSET(MODEL_NAME_FOR_YACHT_FLAG(YachtData.Appearance.iFlag), vCoords, FALSE, FALSE)	
			ATTACH_ENTITY_TO_ENTITY(YachtData.FlagObjectID, YachtData.PoleObjectID, 0, <<0,0,0>>, <<0,0,0>>)
			SET_OBJECT_SETTINGS_FOR_YACHT(YachtData.FlagObjectID)
			#IF IS_DEBUG_BUILD
				INCREMENT_YACHT_OBJECTS()
			#ENDIF	
		ENDIF
		
		// doors
		IF (bCreateDoors)
			
			MODEL_NAMES doorModel = DOOR_MODEL_FOR_YACHT(YachtData)

			// door to appartment
			YachtData.DoorObjectID[0] = CREATE_OBJECT_NO_OFFSET(doorModel,mpYachts[YachtData.iLocation].doorLocDetails.vLoc,FALSE,FALSE,TRUE)
			CDEBUG1LN(DEBUG_YACHT, "CREATE_YACHT_DOOR: created at ",mpYachts[YachtData.iLocation].doorLocDetails.vRot)
			SET_ENTITY_ROTATION(YachtData.DoorObjectID[0],mpYachts[YachtData.iLocation].doorLocDetails.vRot)
			FREEZE_ENTITY_POSITION(YachtData.DoorObjectID[0],TRUE)
			SET_ENTITY_PROOFS(YachtData.DoorObjectID[0],TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
			SET_ENTITY_INVINCIBLE(YachtData.DoorObjectID[0],TRUE)
			SET_ENTITY_DYNAMIC(YachtData.DoorObjectID[0],FALSE)
			PRINTLN("CREATE_OBJECTS_FOR_YACHT: door created")			
			
			
			// doors to bridge
			MP_PROP_OFFSET_STRUCT sPropOffsetA
			VECTOR returnMin
			VECTOR returnMax
			GET_POSITION_AS_OFFSET_FOR_YACHT(YachtData.iLocation, MP_PROP_ELEMENT_YACHT_BRIDGE_DOOR, sPropOffsetA)
			GET_MODEL_DIMENSIONS(doorModel, returnMin, returnMax)
			
			REPEAT 2 i
			
				IF i = 0
					YachtData.DoorObjectID[i+1] = CREATE_OBJECT_NO_OFFSET(doorModel, sPropOffsetA.vLoc, FALSE, FALSE, TRUE)
					SET_ENTITY_ROTATION(YachtData.DoorObjectID[i+1], sPropOffsetA.vRot)
				ELSE
					YachtData.DoorObjectID[i+1] = CREATE_OBJECT_NO_OFFSET(doorModel, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(YachtData.DoorObjectID[1], << (returnMax.x-returnMin.x)*2.0, 0.0, 0.0 >>), FALSE, FALSE, TRUE)
					SET_ENTITY_ROTATION(YachtData.DoorObjectID[i+1], sPropOffsetA.vRot-<< 0.0, 0.0, 180.0 >>)
				ENDIF
				
				FREEZE_ENTITY_POSITION(YachtData.DoorObjectID[i+1], TRUE)
				SET_ENTITY_PROOFS(YachtData.DoorObjectID[i+1], TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
				SET_ENTITY_INVINCIBLE(YachtData.DoorObjectID[i+1], TRUE)
				SET_ENTITY_DYNAMIC(YachtData.DoorObjectID[i+1], FALSE)
			
				PRINTLN("CREATE_OBJECTS_FOR_YACHT: door created for bridge")
			
			ENDREPEAT	
			
		ENDIF
	
	ELSE
		CPRINTLN(DEBUG_YACHT, "CREATE_OBJECTS_FOR_YACHT - objects disabled")
	ENDIF
	
	YachtData.bObjectsCreated = TRUE
	YachtData.bUpdateObjects = TRUE
	
	
ENDPROC

PROC REQUEST_ASSETS_FOR_MISSION_CREATOR_YACHT(YACHT_DATA &YachtData)
	REQUEST_IPLS_FOR_PRIVATE_YACHT(YachtData.iLocation, TRUE)
	REQUEST_ASSETS_FOR_PRIVATE_YACHT(YachtData.Appearance, FALSE)
	REQUEST_MODEL(DOOR_MODEL_FOR_YACHT(YachtData))
ENDPROC

FUNC BOOL HAVE_ASSETS_LOADED_FOR_MISSION_CREATOR_YACHT(YACHT_DATA &YachtData)
	IF HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT(YachtData.Appearance, FALSE)
	AND ARE_IPLS_ACTIVE_FOR_PRIVATE_YACHT(YachtData.iLocation)
	AND HAS_MODEL_LOADED(DOOR_MODEL_FOR_YACHT(YachtData))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DELETE_AND_CLEANUP_ASSETS_FOR_MISSION_CREATOR_YACHT(YACHT_DATA &YachtData, BOOL bRemoveIPLS = TRUE)
	DELETE_OBJECTS_FOR_YACHT(YachtData)
	DELETE_HULL_OBJECTS_FOR_YACHT(YachtData)
	
	IF bRemoveIPLS
		REMOVE_IPLS_FOR_PRIVATE_YACHT(YachtData.iLocation, (NOT FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()))
	ENDIF
	
	REMOVE_ASSETS_FOR_YACHT(YachtData.Appearance)
	SET_MODEL_AS_NO_LONGER_NEEDED(DOOR_MODEL_FOR_YACHT(YachtData))
ENDPROC


FLOAT fJacuzziActualWaterLevel
FLOAT fJacuzziRelativeWaterLevel = -4.840
VECTOR vJacuzziCentre
FLOAT fJacuzziInnerSphereSize = 3.5
FLOAT fJacuzziOutterSphereSize = 4.5

INT iJacuzziBitSet
CONST_INT JAC_BS_EXIT_HELP_TEXT_DISPLAYED   					1
CONST_INT JAC_BS_SERVER_DATA_GENERATED		   					2
CONST_INT JAC_BS_PLAYER_WEARING_SWIMSUIT	   					3
#IF IS_DEBUG_BUILD
CONST_INT JAC_BS_SERVER_DATA_REINIT								4
CONST_INT JAC_BS_SERVER_DATA_REINITED							5
#ENDIF
CONST_INT JAC_BS_PLAYER_INSIDE_OUTTER_AND_OUTSIDE_INNER_SPHERE	6
CONST_INT JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE					7
CONST_INT JAC_BS_PLAYER_OUTSIDE_OUTTER_JAC_SPHERE				8
CONST_INT JAC_BS_SWIMWEAR_REQUIRED								9
CONST_INT JAC_BS_PREVENT_FIRST_PERSON_MODE						10
CONST_INT JAC_BS_POSITIONS_CALCULATED							11

/// Stuff relating to yacht effects
CONST_INT YACHT_EFFECTS_BS_EXTERIOR_EFFECTS_STARTED		0
CONST_INT YACHT_EFFECTS_BS_WADE_STARTED					1
CONST_INT YACHT_EFFECTS_BS_DRIPS_STARTED				2
CONST_INT YACHT_EFFECTS_BS_DRIPS_NEEDED					3
CONST_INT YACHT_EFFECTS_BS_PLAYER_SUBMERGED				4
CONST_INT YACHT_EFFECTS_BS_SPLASH_STARTED				5
CONST_INT YACHT_EFFECTS_BS_SPLASH_NEEDED				6
CONST_INT YACHT_EFFECTS_BS_SOUND_SPLASH_NEEDED			7
CONST_INT YACHT_EFFECTS_BS_WATER_EXIT_SOUND_STARTED		8
CONST_INT YACHT_EFFECTS_BS_GOT_SOUND_IDS				9

STRUCT MP_YACHT_EFFECTS
	INT iBS = 0
	INT iJacuzziSoundID = -1
	INT iEngineSoundID = -1
	INT iLowerDeckSoundID = -1
	INT iHigherDeckSoundID = -1
	PTFX_ID ptfxJacuzziSteam
	PTFX_ID ptfxJacuzziWade
	PTFX_ID ptfxJacuzziSplash
	PTFX_ID ptfxJacuzziDripsLHand
	PTFX_ID ptfxJacuzziDripsRHand
	#IF IS_DEBUG_BUILD
		VECTOR vJacuzziSteamOffset
	#ENDIF
	TIME_DATATYPE tdJacuzziDripsTimer
	INT iJacuzziSplashType = -1
	INT iSplashSoundID = -1
	TIME_DATATYPE tdSplashTimer
	INT iWaterExitSoundID = -1
	VECTOR vPlayerPositionLastFrame
ENDSTRUCT
MP_YACHT_EFFECTS mpYachtEffects
// end of stuff relating to yacht effects

///    Runs all routines which calculate different positions (seats, lean, center, water) used by jacuzzi logic
PROC STORE_JACUZZI_POSITIONS()

	MP_PROP_OFFSET_STRUCT tempOffset
	
	// Get jacuzzi center
	GET_POSITION_AS_OFFSET_FOR_YACHT(PROPERTY_HIGH_APT_8, MP_PROP_ELEMENT_YACHT_JACUZZI_CENTRE, tempOffset)
	vJacuzziCentre = tempOffset.vLoc
	
	// Get jacuzzi absolute water level
	VECTOR vWaterLevel = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(PROPERTY_HIGH_APT_8, <<0.0, 0.0, fJacuzziRelativeWaterLevel>>)
	fJacuzziActualWaterLevel = vWaterLevel.Z
		
	SET_BIT(iJacuzziBitSet, JAC_BS_POSITIONS_CALCULATED)

ENDPROC
PROC CREATE_YACHT_FOR_MISSION_CREATOR(YACHT_DATA &YachtData)
	CREATE_HULL_OBJECTS_FOR_YACHT(YachtData.iLocation, YachtData)
	CREATE_OBJECTS_FOR_YACHT(YachtData.iLocation, YachtData, FALSE, TRUE)
	STORE_JACUZZI_POSITIONS()
ENDPROC

PROC RUN_JACUZZI_EFFECT_WADE(VECTOR &vPos)
	IF NOT IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WADE_STARTED)
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_apartment_mp")
			USE_PARTICLE_FX_ASSET("scr_apartment_mp")
			mpYachtEffects.ptfxJacuzziWade = START_PARTICLE_FX_LOOPED_AT_COORD(
				"scr_apa_jacuzzi_wade", 
				vPos,
				<<0.0, 0.0, 0.0>>, 
				1.0, 
				FALSE, 
				FALSE, 
				FALSE,
				TRUE
			)
			SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WADE_STARTED)
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "START_JACUZZI_EFFECT_WADE - started")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC STOP_JACUZZI_EFFECT_WADE()
	IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WADE_STARTED)
		STOP_PARTICLE_FX_LOOPED(mpYachtEffects.ptfxJacuzziWade, TRUE)
		CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WADE_STARTED)
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SAFEHOUSE, "STOP_JACUZZI_EFFECT_WADE - stopped")
		#ENDIF
	ENDIF
ENDPROC

PROC RUN_JACUZZI_EFFECT_DRIPS()
	IF NOT IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_STARTED)
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_apartment_mp")
		
			USE_PARTICLE_FX_ASSET("scr_apartment_mp")
			mpYachtEffects.ptfxJacuzziDripsLHand = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(
				"scr_apa_jacuzzi_drips", 
				PLAYER_PED_ID(), 
				<<0.0, 0.0, 0.0>>, 
				<<0.0, 0.0, 0.0>>, 
				GET_PED_BONE_INDEX(PLAYER_PED_ID(),BONETAG_L_HAND)
			)
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "START_JACUZZI_EFFECT_DRIPS - started left hand drips at bone ", GET_PED_BONE_INDEX(PLAYER_PED_ID(),BONETAG_L_HAND))
			#ENDIF
			
			USE_PARTICLE_FX_ASSET("scr_apartment_mp")
			mpYachtEffects.ptfxJacuzziDripsRHand = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(
				"scr_apa_jacuzzi_drips", 
				PLAYER_PED_ID(), 
				<<0.0, 0.0, 0.0>>, 
				<<0.0, 0.0, 0.0>>, 
				GET_PED_BONE_INDEX(PLAYER_PED_ID(),BONETAG_R_HAND)
			)
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "START_JACUZZI_EFFECT_DRIPS - started right hand drips at bone ", GET_PED_BONE_INDEX(PLAYER_PED_ID(),BONETAG_R_HAND))
			#ENDIF
			
			SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_STARTED)
			mpYachtEffects.tdJacuzziDripsTimer = GET_NETWORK_TIME()
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "START_JACUZZI_EFFECT_DRIPS - started drips")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC STOP_JACUZZI_EFFECT_DRIPS()
	IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_STARTED)
		STOP_PARTICLE_FX_LOOPED(mpYachtEffects.ptfxJacuzziDripsLHand, TRUE)
		STOP_PARTICLE_FX_LOOPED(mpYachtEffects.ptfxJacuzziDripsRHand, TRUE)
		CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_STARTED)
	ENDIF
ENDPROC

PROC RUN_INNER_OUTTER_SPHERE_CHECKS()
	IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), vJacuzziCentre) <= fJacuzziInnerSphereSize
		CDEBUG3LN(DEBUG_SAFEHOUSE, "Inner Check: Player is ", GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), vJacuzziCentre), ", of vJacuzziCentre: ", vJacuzziCentre," fJacuzziInnerSphereSize = ", fJacuzziInnerSphereSize)
		
		// Just entered the jacuzzi
		IF NOT g_BInYachtJacuzzi
			g_BInYachtJacuzzi = TRUE
			
			// url:bugstar:2628378
			IF NOT IS_PLAYER_SCTV(PLAYER_ID())
				SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE)
				NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)
			ENDIF
		ENDIF
		
		SET_BIT(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE)
	ELSE
		CLEAR_BIT(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_OUTTER_AND_OUTSIDE_INNER_SPHERE)
		CLEAR_BIT(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE)
	ENDIF

	IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), vJacuzziCentre) > fJacuzziOutterSphereSize
		IF g_BInYachtJacuzzi
			g_BInYachtJacuzzi = FALSE
		ENDIF
		
		CLEAR_BIT(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_OUTTER_AND_OUTSIDE_INNER_SPHERE)
		CDEBUG3LN(DEBUG_SAFEHOUSE, "JAC_LOCATECHECK: CLearing bit JAC_BS_PLAYER_INSIDE_OUTTER_AND_OUTSIDE_INNER_SPHERE")
	
		SET_BIT(iJacuzziBitSet, JAC_BS_PLAYER_OUTSIDE_OUTTER_JAC_SPHERE)
		CDEBUG3LN(DEBUG_SAFEHOUSE, "Outter Check: Player is ", GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), vJacuzziCentre), ", of vJacuzziCentre: ", vJacuzziCentre, ", fJacuzziOutterSphereSize = ", fJacuzziOutterSphereSize)
	ELSE
		CLEAR_BIT(iJacuzziBitSet, JAC_BS_PLAYER_OUTSIDE_OUTTER_JAC_SPHERE)
		IF NOT IS_BIT_SET(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE)
		AND NOT IS_BIT_SET(iJacuzziBitSet, JAC_BS_PLAYER_OUTSIDE_OUTTER_JAC_SPHERE)
			SET_BIT(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_OUTTER_AND_OUTSIDE_INNER_SPHERE)
			CDEBUG3LN(DEBUG_SAFEHOUSE, "JAC_LOCATECHECK: Setting bit JAC_BS_PLAYER_INSIDE_OUTTER_AND_OUTSIDE_INNER_SPHERE")
		ENDIF
	ENDIF
ENDPROC
PROC MAINTAIN_JACUZZI_EFFECTS()

	RUN_INNER_OUTTER_SPHERE_CHECKS()
	
	// Request sound IDs as soon as player is near the jacuzzi
	IF NOT IS_BIT_SET(iJacuzziBitSet, JAC_BS_PLAYER_OUTSIDE_OUTTER_JAC_SPHERE)
		IF mpYachtEffects.iSplashSoundID = -1
			mpYachtEffects.iSplashSoundID = GET_SOUND_ID()
		ENDIF
		
		IF mpYachtEffects.iWaterExitSoundID = -1
			mpYachtEffects.iWaterExitSoundID = GET_SOUND_ID()
		ENDIF
	ELSE
		IF mpYachtEffects.iSplashSoundID != -1
			RELEASE_SOUND_ID(mpYachtEffects.iSplashSoundID)
			mpYachtEffects.iSplashSoundID = -1
		ENDIF
		
		IF mpYachtEffects.iWaterExitSoundID != -1
			RELEASE_SOUND_ID(mpYachtEffects.iWaterExitSoundID)
			mpYachtEffects.iWaterExitSoundID = -1
		ENDIF
	ENDIF

	IF IS_BIT_SET(iJacuzziBitSet, JAC_BS_PLAYER_INSIDE_INNER_JAC_SPHERE)
		
		FLOAT fWadeScale = 0.0
		FLOAT fPlayerSubmersion
		FLOAT fPlayerSpeed = GET_ENTITY_SPEED(PLAYER_PED_ID())
		FLOAT fSubmersionFactor // 0.0 - not submerged, 1.0 - fully submerged
		VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		IF fPlayerSpeed > 0.0
			VECTOR vPlayerFootCoords = GET_WORLD_POSITION_OF_ENTITY_BONE(PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_L_FOOT))
			
			FLOAT fMaxSubmersion = fJacuzziActualWaterLevel - vJacuzziCentre.Z
			fPlayerSubmersion = FMAX(0.0, ABSF(fJacuzziActualWaterLevel - (vPlayerFootCoords.Z))) // by how much the player is submerged in jacuzzi water
			
			fSubmersionFactor = fPlayerSubmersion / fMaxSubmersion
			
			fWadeScale = FMIN(1.0, (fSubmersionFactor + (fPlayerSpeed * 0.5)) * 0.5)
		ENDIF
				
		IF fPlayerSpeed > 0.1 AND fWadeScale > 0.0
			VECTOR vWadePosition = <<vPlayerCoords.X, vPlayerCoords.Y, fJacuzziActualWaterLevel>>
			
			RUN_JACUZZI_EFFECT_WADE(vWadePosition)
			
			SET_PARTICLE_FX_LOOPED_OFFSETS(mpYachtEffects.ptfxJacuzziWade, vWadePosition, <<0.0, 0.0, 0.0>>)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(mpYachtEffects.ptfxJacuzziWade, "size", fWadeScale, TRUE)
		ELSE
			STOP_JACUZZI_EFFECT_WADE()
		ENDIF
		
		IF NOT IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_NEEDED)
			SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_NEEDED)
		ENDIF
		
		// Detect when to play sounds for splashes of water
		// First check if player is submerged
		INT i
		VECTOR vBonePosition
		BOOL bSubmerged = FALSE
		PED_BONETAG iBoneTags[6]
		iBoneTags[0] = BONETAG_L_FOOT
		iBoneTags[1] = BONETAG_R_FOOT
		iBoneTags[2] = BONETAG_L_FOREARM
		iBoneTags[3] = BONETAG_R_FOREARM
		iBoneTags[4] = BONETAG_ROOT
		iBoneTags[5] = BONETAG_HEAD
		
		// Check some critical bones, if any of them is in the water then at leasst some part of the player is under water for sure
		REPEAT 6 i
			vBonePosition = GET_WORLD_POSITION_OF_ENTITY_BONE(PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), iBoneTags[i]))
			IF vBonePosition.Z <= fJacuzziActualWaterLevel
				bSubmerged = TRUE
				BREAKLOOP
			ENDIF
		ENDREPEAT
		
		IF bSubmerged
			IF NOT IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_PLAYER_SUBMERGED)
				// Player just touched the water surface
				// See if their speed is large enough to trigger splashes
				VECTOR vPlayerVelocity = GET_ENTITY_VELOCITY(PLAYER_PED_ID())
				FLOAT fDownwardSpeed = ABSF(vPlayerVelocity.Z)
				
				#IF IS_DEBUG_BUILD
					CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - WATER CONTACT! fDownwardSpeed: ", fDownwardSpeed)
				#ENDIF
				
				IF fDownwardSpeed > 5.5
					mpYachtEffects.iJacuzziSplashType = 3
				ELIF fDownwardSpeed > 4.0
					mpYachtEffects.iJacuzziSplashType = 2
				ELIF fDownwardSpeed > 2.0
					mpYachtEffects.iJacuzziSplashType = 1
				ELIF fDownwardSpeed > 0.5
					mpYachtEffects.iJacuzziSplashType = 0
				ELSE
					mpYachtEffects.iJacuzziSplashType = -1
				ENDIF
				
				SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_PLAYER_SUBMERGED)
				SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SOUND_SPLASH_NEEDED)
			ELSE
				// Only allow other splash to happen if the previous one already stopped (both particles and sound)
				IF NOT IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_STARTED)
				AND HAS_SOUND_FINISHED(mpYachtEffects.iSplashSoundID)
					
					// Detect any sudden changes in position which mean that we might need to do another splash (player jumping or being pushed etc.)
					FLOAT fZChange = vPlayerCoords.Z - mpYachtEffects.vPlayerPositionLastFrame.Z
					
					IF fZChange < -0.04
						mpYachtEffects.iJacuzziSplashType = 0
						IF fZChange < -0.06
							mpYachtEffects.iJacuzziSplashType = 1
						ENDIF
						
						#IF IS_DEBUG_BUILD
							CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - Player jump around, need splash")
						#ENDIF
						
						SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SOUND_SPLASH_NEEDED)
					ENDIF
				ENDIF
				
				// Monitor player leaving the water through climbing out so we can play water exit sound
				IF IS_PED_CLIMBING(PLAYER_PED_ID())
					IF NOT IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WATER_EXIT_SOUND_STARTED)

						STOP_SOUND(mpYachtEffects.iWaterExitSoundID)

						PLAY_SOUND_FROM_ENTITY(mpYachtEffects.iWaterExitSoundID, "ExitWater", PLAYER_PED_ID(), "GTAO_Hot_Tub_PED_INSIDE_WATER", TRUE)
						SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WATER_EXIT_SOUND_STARTED)
						
						#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - Playing water exit sound")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		mpYachtEffects.vPlayerPositionLastFrame = vPlayerCoords
		
		// Do we need to play a splash sound?
		IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SOUND_SPLASH_NEEDED)
			SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_NEEDED) // along with the sound go particles
			
			STOP_SOUND(mpYachtEffects.iSplashSoundID)
			
			STRING sndName = ""
			SWITCH mpYachtEffects.iJacuzziSplashType
				CASE 0 sndName = "FallingInWaterSmall" BREAK
				CASE 1 sndName = "FallingInWaterMedium" BREAK
				CASE 2 sndName = "FallingInWaterHeavy" BREAK
				CASE 3 sndName = "DiveInWater" BREAK
			ENDSWITCH
		
			PLAY_SOUND_FROM_ENTITY(mpYachtEffects.iSplashSoundID, sndName, PLAYER_PED_ID(), "PED_INSIDE_WATER", TRUE)
			
			#IF IS_DEBUG_BUILD
				CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - Playing splash sound: ", sndName)
			#ENDIF
			
			CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SOUND_SPLASH_NEEDED)
		ENDIF
		
		// Do we need to show splash effect?
		IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_NEEDED)
		AND NOT IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_STARTED)
		AND mpYachtEffects.iJacuzziSplashType > 0
			IF HAS_NAMED_PTFX_ASSET_LOADED("scr_apartment_mp")
			
				VECTOR vSplashPosition = vPlayerCoords
				vSplashPosition.Z = fJacuzziActualWaterLevel + 0.1
				
				FLOAT fScale = 1.0
				SWITCH mpYachtEffects.iJacuzziSplashType
					CASE 1 fScale = 1.25 BREAK
					CASE 2 fScale = 1.66 BREAK
					CASE 3 fScale = 2.0 BREAK
				ENDSWITCH
			
				USE_PARTICLE_FX_ASSET("scr_apartment_mp")
				mpYachtEffects.ptfxJacuzziSplash = START_PARTICLE_FX_LOOPED_AT_COORD("scr_apa_jacuzzi_wade", vSplashPosition, <<0.0, 0.0, 0.0>>, fScale, FALSE, FALSE, FALSE, TRUE)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(mpYachtEffects.ptfxJacuzziSplash, "size", 1.0, TRUE)
				
				#IF IS_DEBUG_BUILD
					CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - splash particles started")
				#ENDIF
				
				mpYachtEffects.tdSplashTimer = GET_NETWORK_TIME()
				SET_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_STARTED)
				CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_NEEDED)
			ENDIF
		ENDIF
	ELSE
		STOP_JACUZZI_EFFECT_WADE()
		
		IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_NEEDED)
		
			IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_STARTED)
				mpYachtEffects.tdJacuzziDripsTimer = GET_NETWORK_TIME()
			ENDIF
		
			RUN_JACUZZI_EFFECT_DRIPS()
			
			IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_STARTED)
				CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_NEEDED)
			ENDIF
			
			CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_PLAYER_SUBMERGED)
		ENDIF
		
		CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_PLAYER_SUBMERGED)
	ENDIF
	
	// Water drips timer
	IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_DRIPS_STARTED)
		IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), mpYachtEffects.tdJacuzziDripsTimer)) > 7000
			STOP_JACUZZI_EFFECT_DRIPS()
			
			#IF IS_DEBUG_BUILD
				CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - stopped drips")
			#ENDIF
		ENDIF
	ENDIF
	
	// Big jump splash timer
	IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_STARTED)
		IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), mpYachtEffects.tdSplashTimer)) > 920
			STOP_PARTICLE_FX_LOOPED(mpYachtEffects.ptfxJacuzziSplash)
			CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_SPLASH_STARTED)
			
			#IF IS_DEBUG_BUILD
				CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_JACUZZI_EFFECTS - stopped splash particles")
			#ENDIF
		ENDIF
	ENDIF
	
	// Water exiting sound
	IF IS_BIT_SET(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WATER_EXIT_SOUND_STARTED)
		IF HAS_SOUND_FINISHED(mpYachtEffects.iWaterExitSoundID)
			CLEAR_BIT(mpYachtEffects.iBS, YACHT_EFFECTS_BS_WATER_EXIT_SOUND_STARTED)
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_YACHT_FOR_MISSION_CREATOR(YACHT_DATA &YachtData)
	IF NOT GB_IS_GLOBAL_NON_BD_BIT1_SET(eGB_GLOBAL_NON_BD_BITSET_1_DISABLE_UPDATE_YACHT_COLOUR)
		UPDATE_YACHT_COLOUR(YachtData, TRUE)
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
		IF NOT GB_IS_GLOBAL_NON_BD_BIT1_SET(eGB_GLOBAL_NON_BD_BITSET_1_SCREEN_FADED_OUT_FOR_YACHT_UPDATE)
			GB_CLEAR_GLOBAL_NON_BD_BIT1(eGB_GLOBAL_NON_BD_BITSET_1_DISABLE_UPDATE_YACHT_COLOUR)
			
			GB_SET_GLOBAL_NON_BD_BIT1(eGB_GLOBAL_NON_BD_BITSET_1_SCREEN_FADED_OUT_FOR_YACHT_UPDATE)
		ENDIF
	ELIF IS_SCREEN_FADED_IN()
		IF (DOES_ENTITY_EXIST(YachtData.HullObjectID) AND GET_OBJECT_TINT_INDEX(YachtData.HullObjectID) != YachtData.Appearance.iTint)
		OR GB_IS_GLOBAL_NON_BD_BIT1_SET(eGB_GLOBAL_NON_BD_BITSET_1_SCREEN_FADED_OUT_FOR_YACHT_UPDATE)
			GB_CLEAR_GLOBAL_NON_BD_BIT1(eGB_GLOBAL_NON_BD_BITSET_1_DISABLE_UPDATE_YACHT_COLOUR)
			GB_CLEAR_GLOBAL_NON_BD_BIT1(eGB_GLOBAL_NON_BD_BITSET_1_SCREEN_FADED_OUT_FOR_YACHT_UPDATE)
		ENDIF
	ENDIF
	
	UPDATE_YACHT_OBJECTS(YachtData)
	
	#IF FEATURE_FIXER
	IF YachtData.iLocation != 40
	AND YachtData.iLocation != 41
	#ENDIF
		MAINTAIN_JACUZZI_EFFECTS()
	#IF FEATURE_FIXER
	ENDIF
	#ENDIF
	
	IF NOT GB_IS_GLOBAL_NON_BD_BIT1_SET(eGB_GLOBAL_NON_BD_BITSET_1_DISABLE_UPDATE_YACHT_MAP_FOR_MISSION_CREATOR)
	AND (NOT FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT() OR GET_YACHT_PLAYER_IS_ON(PLAYER_ID()) = -1)
		// Just checking that we're on ANY yacht. Then we're using the data we already have (YachtData.iLocation)
		MP_PROP_OFFSET_STRUCT tempMapLocation = GET_YACHT_EXTERIOR_MAP_LOCATION(YachtData.iLocation) 
		
		VECTOR vYachtPos 	= tempMapLocation.vLoc
		INT IYachtRot 		= ROUND(tempMapLocation.vRot.Z)
		
		PED_INDEX pedIndex = GET_PLAYER_PED(PLAYER_ID())
		
		IF NOT IS_PED_INJURED(pedIndex)
			VECTOR vPos = GET_ENTITY_COORDS(pedIndex)
			VECTOR vYachtCentre = <<-774.75055, 6565.89795, 14.85115>>
			
			FLOAT fDistance2 = VDIST2(<<vPos.x, vPos.y, 0>>, <<vYachtCentre.x, vYachtCentre.y, 0>>)

			IF fDistance2 < (70 * 70)
				SET_RADAR_ZOOM_PRECISE(40)
			ELIF fDistance2 > (80 * 80)
				SET_RADAR_ZOOM_PRECISE(0)
			ENDIF

			INT iFloor = 0
			BOOL bInternal
			
			IF (fDistance2 <= (75 * 75))
				iFloor = 0		
				
				//IS_ENTITY_IN_ANGLED_AREA( PedIndex, <<-829.253723,6538.872559,-10.741039>>, <<-721.733826,6592.729004,21.303493>>, 15.000000)
				
				IF IS_POINT_IN_ANGLED_AREA(vPos, <<-829.253723,6538.872559,-10.741039>>, <<-721.733826,6592.729004,21.303493>>, 15.0)
					bInternal = TRUE
				ENDIF
			ENDIF
			
			IF iFloor >= 0
				SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("apa_mpapa_yachtexterior"), vYachtPos.x, vYachtPos.y, IYachtRot, iFloor)
				
				IF NOT bInternal
					SET_RADAR_AS_EXTERIOR_THIS_FRAME()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_CREATOR_YACHT_RENDER_TARGET(MODEL_NAMES mModel)
	SWITCH mModel
		CASE APA_PROP_AP_PORT_TEXT   RETURN "port_text"   BREAK
		CASE APA_PROP_AP_STARB_TEXT  RETURN "starb_text"  BREAK
		CASE APA_PROP_AP_STERN_TEXT  RETURN "stern_text"  BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC REGISTER_AND_LINK_CREATOR_YACHT_RENDER_TARGET(INT &iRenderTargetID, STRING sRenderTarget, MODEL_NAMES mModel)
	IF NOT IS_NAMED_RENDERTARGET_REGISTERED(sRenderTarget)
		REGISTER_NAMED_RENDERTARGET(sRenderTarget)
		
		IF NOT IS_NAMED_RENDERTARGET_LINKED(mModel)
			LINK_NAMED_RENDERTARGET(mModel)
			
			IF iRenderTargetID = -1
				iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID(sRenderTarget)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_MISSION_CREATOR_YACHT_NAME(YACHT_NAME_DATA &YachtNameData)
	IF HAS_SCALEFORM_MOVIE_LOADED(YachtNameData.sYachtNameOverlay)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(YachtNameData.sYachtNameOverlay)
	ENDIF
	
	IF HAS_SCALEFORM_MOVIE_LOADED(YachtNameData.sYachtNameSternOverlay)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(YachtNameData.sYachtNameSternOverlay)
	ENDIF
	
	IF IS_NAMED_RENDERTARGET_REGISTERED("port_text")
		RELEASE_NAMED_RENDERTARGET("port_text")
	ENDIF
	
	IF IS_NAMED_RENDERTARGET_REGISTERED("starb_text")
		RELEASE_NAMED_RENDERTARGET("starb_text")
	ENDIF
	
	IF IS_NAMED_RENDERTARGET_REGISTERED("stern_text")
		RELEASE_NAMED_RENDERTARGET("stern_text")
	ENDIF
	
	IF DOES_ENTITY_EXIST(YachtNameData.oYachtNamePort)
		SAFE_DELETE_OBJECT(YachtNameData.oYachtNamePort)
	ENDIF
	
	IF DOES_ENTITY_EXIST(YachtNameData.oYachtNameStarb)
		SAFE_DELETE_OBJECT(YachtNameData.oYachtNameStarb)
	ENDIF
	
	IF DOES_ENTITY_EXIST(YachtNameData.oYachtNameStern)
		SAFE_DELETE_OBJECT(YachtNameData.oYachtNameStern)
	ENDIF
	
	YachtNameData.iPortRenderID = -1
	YachtNameData.iStarbRenderID = -1
	YachtNameData.iSternRenderID = -1
	
	YachtNameData.iNameControl = 0
ENDPROC

PROC UPDATE_YACHT_NAME_FOR_MISSION_CREATOR(YACHT_DATA &YachtData, YACHT_NAME_DATA &YachtNameData)
	IF IS_STRING_NULL_OR_EMPTY(YachtData.Appearance.sName)
		EXIT
	ENDIF
	
	BOOL bYachtWhiteText = FALSE
	
	INT iCurrentYacht = YachtData.iLocation
	
	TEXT_LABEL_63 sNameToUse = YachtData.Appearance.sName
	
	MP_PROP_OFFSET_STRUCT sPropOffset
	
	IF NOT g_bMissionEnding
	AND GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), mpYachts[iCurrentYacht].vBaseLocation) <= 150
		SWITCH YachtNameData.iNameControl
			CASE 0
				IF CAN_REGISTER_MISSION_OBJECTS(3)
					IF NOT DOES_ENTITY_EXIST(YachtNameData.oYachtNamePort)
						REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("SF_PROP_AP_PORT_TEXT")))
						
						IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("SF_PROP_AP_PORT_TEXT")))
							GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_NAME_PORT, sPropOffset)
							
							YachtNameData.oYachtNamePort = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("SF_PROP_AP_PORT_TEXT")), sPropOffset.vLoc, FALSE, FALSE, TRUE)
							
							SET_ENTITY_ROTATION(YachtNameData.oYachtNamePort, sPropOffset.vRot)
							
							REGISTER_AND_LINK_CREATOR_YACHT_RENDER_TARGET(YachtNameData.iPortRenderID, GET_CREATOR_YACHT_RENDER_TARGET(APA_PROP_AP_PORT_TEXT), INT_TO_ENUM(MODEL_NAMES, HASH("SF_PROP_AP_PORT_TEXT")))
							
							SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("SF_PROP_AP_PORT_TEXT")))
						ENDIF
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(YachtNameData.oYachtNameStarb)
						REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("SF_PROP_AP_STARB_TEXT")))
						
						IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("SF_PROP_AP_STARB_TEXT")))
							GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_NAME_STARB, sPropOffset)
							
							YachtNameData.oYachtNameStarb = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("SF_PROP_AP_STARB_TEXT")), sPropOffset.vLoc, FALSE, FALSE, TRUE)
							
							SET_ENTITY_ROTATION(YachtNameData.oYachtNameStarb, sPropOffset.vRot)
							
							REGISTER_AND_LINK_CREATOR_YACHT_RENDER_TARGET(YachtNameData.iStarbRenderID, GET_CREATOR_YACHT_RENDER_TARGET(APA_PROP_AP_STARB_TEXT), INT_TO_ENUM(MODEL_NAMES, HASH("SF_PROP_AP_STARB_TEXT")))
							
							SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("SF_PROP_AP_STARB_TEXT")))
						ENDIF
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(YachtNameData.oYachtNameStern)
						REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("SF_PROP_AP_STERN_TEXT")))
						IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("SF_PROP_AP_STERN_TEXT")))
							GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_NAME_STERN, sPropOffset)
							
							IF iCurrentYacht = 40
								sPropOffset.vLoc.x += 0.01
								sPropOffset.vRot.z -= 0.02
							ELIF iCurrentYacht = 41
								sPropOffset.vLoc.x -= 0.01
								sPropOffset.vRot.z -= 0.02
							ENDIF
							
							YachtNameData.oYachtNameStern = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("SF_PROP_AP_STERN_TEXT")), sPropOffset.vLoc, FALSE, FALSE, TRUE)
							
							SET_ENTITY_ROTATION(YachtNameData.oYachtNameStern, sPropOffset.vRot)
							
							REGISTER_AND_LINK_CREATOR_YACHT_RENDER_TARGET(YachtNameData.iSternRenderID, GET_CREATOR_YACHT_RENDER_TARGET(APA_PROP_AP_STERN_TEXT), INT_TO_ENUM(MODEL_NAMES, HASH("SF_PROP_AP_STERN_TEXT")))
							
							SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("SF_PROP_AP_STERN_TEXT")))
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(YachtNameData.oYachtNamePort)
					AND DOES_ENTITY_EXIST(YachtNameData.oYachtNameStarb)
					AND DOES_ENTITY_EXIST(YachtNameData.oYachtNameStern)
						YachtNameData.iNameControl = 1
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF YachtData.Appearance.iTint = 1
				OR YachtData.Appearance.iTint = 4
				OR YachtData.Appearance.iTint = 5
				OR YachtData.Appearance.iTint = 6
				OR YachtData.Appearance.iTint = 9
				OR YachtData.Appearance.iTint = 11
				OR YachtData.Appearance.iTint = 15
					bYachtWhiteText = TRUE
				ENDIF
				
				IF HAS_SCALEFORM_MOVIE_LOADED(YachtNameData.sYachtNameOverlay)
					BEGIN_SCALEFORM_MOVIE_METHOD(YachtNameData.sYachtNameOverlay, "SET_YACHT_NAME")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sNameToUse)
						
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bYachtWhiteText)
						
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
					END_SCALEFORM_MOVIE_METHOD()
					
					SET_SCALEFORM_MOVIE_TO_USE_LARGE_RT(YachtNameData.sYachtNameOverlay, TRUE)
					
					YachtNameData.iNameControl = 2
				ELSE
					YachtNameData.sYachtNameOverlay = REQUEST_SCALEFORM_MOVIE("YACHT_NAME")
				ENDIF
			BREAK
			
			CASE 2
				IF YachtData.Appearance.iTint = 1
				OR YachtData.Appearance.iTint = 4
				OR YachtData.Appearance.iTint = 5
				OR YachtData.Appearance.iTint = 6
				OR YachtData.Appearance.iTint = 9
				OR YachtData.Appearance.iTint = 11
				OR YachtData.Appearance.iTint = 15
					bYachtWhiteText = TRUE
				ENDIF
				
				TEXT_LABEL_15 tlFlagCountry
				
				IF HAS_SCALEFORM_MOVIE_LOADED(YachtNameData.sYachtNameSternOverlay)
					BEGIN_SCALEFORM_MOVIE_METHOD(YachtNameData.sYachtNameSternOverlay, "SET_YACHT_NAME")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sNameToUse)
						
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bYachtWhiteText)
						
						tlFlagCountry = "FLAG_CNTRY_"
						tlFlagCountry += YachtData.Appearance.iFlag
						
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlFlagCountry)
					END_SCALEFORM_MOVIE_METHOD()
					
					SET_SCALEFORM_MOVIE_TO_USE_LARGE_RT(YachtNameData.sYachtNameSternOverlay, TRUE)
					
					YachtNameData.iNameControl = 3
				ELSE
					YachtNameData.sYachtNameSternOverlay = REQUEST_SCALEFORM_MOVIE("YACHT_NAME_STERN")
				ENDIF
			BREAK
			
			CASE 3
				SET_TEXT_RENDER_ID(YachtNameData.iPortRenderID)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
				DRAW_SCALEFORM_MOVIE(YachtNameData.sYachtNameOverlay, 0.395, 0.350, 0.500, 1.000, 255, 255, 255, 255)
				
				SET_TEXT_RENDER_ID(YachtNameData.iStarbRenderID)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
				DRAW_SCALEFORM_MOVIE(YachtNameData.sYachtNameOverlay, 0.315, 0.350, 0.500, 1.000, 255, 255, 255, 255)
				
				SET_TEXT_RENDER_ID(YachtNameData.iSternRenderID)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
				DRAW_SCALEFORM_MOVIE(YachtNameData.sYachtNameSternOverlay, 0.400, 0.350, 0.700, 0.750, 255, 255, 255, 255)
				
				SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
			BREAK
		ENDSWITCH
	ELSE
		CLEANUP_MISSION_CREATOR_YACHT_NAME(YachtNameData)
	ENDIF
ENDPROC

//FUNC BOOL IS_YACHT_DUMMY_ANIMATING(INT iYachtID, PRIVATE_YACHT_LOCAL_DATA &LocalData)
//	IF (LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_IDLE)
//	OR (LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_DONE)
//		RETURN(FALSE)
//	ENDIF
//	RETURN(TRUE)
//ENDFUNC

PROC UPDATE_CREATE_OBJECTS_FOR_YACHT(INT iYachtID, PRIVATE_YACHT_LOCAL_DATA &LocalData, BOOL bIgnoreFadeCheck=FALSE)
	FLOAT fDist

	IF (LocalData.YachtData[iYachtID].bCreateObjects)
	
		IF NOT (LocalData.YachtData[iYachtID].bObjectsCreated)
			IF NOT IS_FADE_HAPPENING_FOR_YACHT_LOADER(LocalData)
			OR (bIgnoreFadeCheck)
				IF HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT(LocalData.YachtData[iYachtID].Appearance, SHOULD_YACHT_OBJECTS_BE_DISABLED())
					fDist = VDIST(GET_FINAL_RENDERED_CAM_COORD(), GET_MODEL_COORDS_OF_YACHT(iYachtID))
					IF (fDist < g_sMPTunables.fdistanceyachtcreation)		
						IF NOT IS_PLAYER_TELEPORT_ACTIVE()
							CPRINTLN(DEBUG_YACHT, "UPDATE_CREATE_OBJECTS_FOR_YACHT - good to create objects. iYachtID ", iYachtID, " fDist = ", fDist)
							CREATE_OBJECTS_FOR_YACHT(iYachtID, LocalData.YachtData[iYachtID], SHOULD_YACHT_OBJECTS_BE_DISABLED())
							REMOVE_ASSETS_FOR_YACHT(LocalData.YachtData[iYachtID].Appearance)	
						ELSE
							CPRINTLN(DEBUG_YACHT, "UPDATE_CREATE_OBJECTS_FOR_YACHT - player teleport is active. iYachtID ", iYachtID, " fDist = ", fDist)	
						ENDIF
					ELSE
						CPRINTLN(DEBUG_YACHT, "UPDATE_CREATE_OBJECTS_FOR_YACHT - too far to create objects. iYachtID ", iYachtID, " fDist = ", fDist)	
					ENDIF
				ENDIF
			ENDIF
		ELSE
			fDist = VDIST(GET_FINAL_RENDERED_CAM_COORD(), GET_MODEL_COORDS_OF_YACHT(iYachtID))
			IF (fDist > (g_sMPTunables.fdistanceyachtcreation + YACHT_OBJECT_CREATE_BUFFER))
			OR IS_PLAYER_TELEPORT_ACTIVE()
			OR SHOULD_YACHT_OBJECTS_BE_DISABLED()
				CPRINTLN(DEBUG_YACHT, "UPDATE_CREATE_OBJECTS_FOR_YACHT - gone too far from yacht, deleting objects. iYachtID ", iYachtID, " fDist = ", fDist)				
				DELETE_OBJECTS_FOR_YACHT(LocalData.YachtData[iYachtID])	
			ENDIF
		ENDIF
	
	ELSE
	
		IF (LocalData.YachtData[iYachtID].bObjectsCreated)
			CPRINTLN(DEBUG_YACHT, "UPDATE_CREATE_OBJECTS_FOR_YACHT - no longer active, deleting objects. iYachtID ", iYachtID, " fDist = ", fDist)
			
			// only delete the objects if we are not fading the yacht out.
			//IF NOT IS_YACHT_DUMMY_ANIMATING(iYachtID, LocalData)
				DELETE_OBJECTS_FOR_YACHT(LocalData.YachtData[iYachtID])
			//ELSE
			//	CPRINTLN(DEBUG_YACHT, "UPDATE_CREATE_OBJECTS_FOR_YACHT - doing a dummy fade out, so not deleting. iYachtID ", iYachtID, " fDist = ", fDist)	
			//ENDIF
		ENDIF
	
	ENDIF
	
ENDPROC

PROC DO_FLUSH_ALL_OBJECTS_FOR_YACHTS(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	IF (MPGlobalsPrivateYacht.bFlushYachtObjects)
		CPRINTLN(DEBUG_YACHT, "FLUSH_ALL_OBJECTS_FOR_YACHTS - called ")
		INT iYacht
		// delete all objects first
		REPEAT NUMBER_OF_PRIVATE_YACHTS iYacht
			IF (LocalData.YachtData[iYacht].bObjectsCreated)	
				DELETE_OBJECTS_FOR_YACHT(LocalData.YachtData[iYacht])
			ENDIF
		ENDREPEAT
		// recreate any objects
		IF NOT MPGlobalsPrivateYacht.bUseYachtObjectsOptimisation
			REPEAT NUMBER_OF_PRIVATE_YACHTS iYacht
				UPDATE_CREATE_OBJECTS_FOR_YACHT(iYacht, LocalData, TRUE)
			ENDREPEAT
		ENDIF
		MPGlobalsPrivateYacht.bFlushYachtObjects = FALSE
	ENDIF
ENDPROC


PROC SET_YACHT_OBJECTS_TO_BE_CREATED(INT iYachtID, BOOL bSet, PRIVATE_YACHT_LOCAL_DATA &LocalData)
	IF (bSet)
		IF NOT (LocalData.YachtData[iYachtID].bCreateObjects)
			LocalData.YachtData[iYachtID].bCreateObjects = TRUE	
			CPRINTLN(DEBUG_YACHT, "SET_YACHT_OBJECTS_TO_BE_CREATED - settting to TRUE for iYacht  ", iYachtID)			
		ENDIF
	ELSE
		IF (LocalData.YachtData[iYachtID].bCreateObjects)
			LocalData.YachtData[iYachtID].bCreateObjects = FALSE	
			CPRINTLN(DEBUG_YACHT, "SET_YACHT_OBJECTS_TO_BE_CREATED - settting to FALSE for iYacht  ", iYachtID)			
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL DO_YACHT_APPEARANCES_MATCH(YACHT_APPEARANCE Appearance1, YACHT_APPEARANCE Appearance2)
	IF (Appearance1.iOption = Appearance2.iOption)
	AND (Appearance1.iTint = Appearance2.iTint)
	AND (Appearance1.iLighting = Appearance2.iLighting)
	AND (Appearance1.iRailing = Appearance2.iRailing)
	AND (Appearance1.iFlag = Appearance2.iFlag)
	AND (Appearance1.iNameAsHash = Appearance2.iNameAsHash)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC


PROC START_FADE_FOR_YACHT_LOADER(INT iYachtID, PRIVATE_YACHT_LOCAL_DATA &LocalData, INT iOldOption)
	CPRINTLN(DEBUG_YACHT, "START_FADE_FOR_YACHT_LOADER - called with yacht ", iYachtID)
	LocalData.iYachtToFade = iYachtID
	LocalData.iYachtFadeState = 0
	LocalData.iYachtFadeOldOption = iOldOption
ENDPROC

PROC CLEANUP_FADE_FOR_YACHT_LOADER(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	CPRINTLN(DEBUG_YACHT, "CLEANUP_FADE_FOR_YACHT_LOADER - called")
	LocalData.iYachtToFade = -1
	LocalData.iYachtFadeState = -1
	LocalData.iYachtFadeOldOption = -1
	g_SpawnData.YachtWarpVehicleID = NULL
ENDPROC

PROC DeleteLastVehicleOnYacht_ForFade(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	VEHICLE_INDEX VehicleID = GET_PLAYERS_LAST_VEHICLE()
	BOOL bSetAsMissionEntity
	IF DOES_ENTITY_EXIST(VehicleID)
		IF IS_VEHICLE_ON_YACHT(VehicleID, LocalData.iYachtToFade, YACHT_INCLUDING_VEHICLES_DISTANCE)	
			IF IS_VEHICLE_EMPTY(VehicleID)
				IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(VehicleID))
				OR IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(VehicleID))
					IF NOT IS_VEHICLE_ON_ALL_WHEELS(VehicleID)			
						IF NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)		
							IF CAN_EDIT_THIS_ENTITY(VehicleID, bSetAsMissionEntity)
								DELETE_VEHICLE(VehicleID)
								CPRINTLN(DEBUG_YACHT, "DeleteLastVehicleOnYacht_ForFade - deleting vehicle ")
							ELSE
								IF (bSetAsMissionEntity)
									DELETE_VEHICLE(VehicleID)
									CPRINTLN(DEBUG_YACHT, "DeleteLastVehicleOnYacht_ForFade - deleting vehicle ")
								ENDIF
							ENDIF
						ELSE
							NETWORK_REQUEST_CONTROL_OF_ENTITY(VehicleID)
							CPRINTLN(DEBUG_YACHT, "DeleteLastVehicleOnYacht_ForFade - requesting control of vehicle ")
						ENDIF
					ELSE
						CPRINTLN(DEBUG_YACHT, "DeleteLastVehicleOnYacht_ForFade - last vehicle not in air ")
					ENDIF
				ELSE
					CPRINTLN(DEBUG_YACHT, "DeleteLastVehicleOnYacht_ForFade - last vehicle not a heli or plane ")	
				ENDIF
			ELSE
				CPRINTLN(DEBUG_YACHT, "DeleteLastVehicleOnYacht_ForFade - last vehicle not empty ")	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DoWarpOfLastVehicleOnYacht_ForFade(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	// if we are removing the heli pad from over the jacuzzi, we need to move/delete any vehicles.
	IF DOES_ENTITY_EXIST(g_SpawnData.YachtWarpVehicleID)
		VECTOR vCoordsJacuzzi = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(LocalData.iYachtToFade, <<0.0, -53.2421, -2.6820>>)
		VECTOR vCoordsVehicle = GET_ENTITY_COORDS(g_SpawnData.YachtWarpVehicleID, FALSE)
		VECTOR vFrontHeliPad = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(LocalData.iYachtToFade, <<0.0, 34.3857, -1.1412>>)
		
		IF NETWORK_HAS_CONTROL_OF_ENTITY(g_SpawnData.YachtWarpVehicleID)	
		
			// is vehicle at jacuzzi
			IF (VDIST(vCoordsJacuzzi, vCoordsVehicle) < 5.0)
				CPRINTLN(DEBUG_YACHT, "DoWarpOfLastVehicleOnYacht_ForFade - last vehicle is in jacuzzi pos")		
				// if we are removing the heli pad from over the jacuzzi, we need to move/delete any vehicles.
				IF (LocalData.iYachtFadeOldOption = 0)
					// check warp to location is safe
					IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vFrontHeliPad, 
														3.5, 
														3.5,
														0.0,
														7.0,
														FALSE,
														FALSE,
														FALSE,
														60.0)
														
							
						FLOAT fModelHeight = GET_MODEL_HEIGHT(GET_ENTITY_MODEL(g_SpawnData.YachtWarpVehicleID))
														
						SET_ENTITY_COORDS_NO_OFFSET(g_SpawnData.YachtWarpVehicleID, <<vFrontHeliPad.x, vFrontHeliPad.y, vFrontHeliPad.z + (fModelHeight * 0.5)>>)
						CPRINTLN(DEBUG_YACHT, "DoWarpOfLastVehicleOnYacht_ForFade - warped to front heli pad")
					
					ELSE
						// delete
						DELETE_VEHICLE(g_SpawnData.YachtWarpVehicleID)
						CPRINTLN(DEBUG_YACHT, "DoWarpOfLastVehicleOnYacht_ForFade - deleted vehicle")
					ENDIF
				ELSE
					// if we are creating a helipad over the jacuzzi then clear any vehicles from here.
					IF LocalData.YachtData[LocalData.iYachtToFade].Appearance.iOption = 0
						// delete
						DELETE_VEHICLE(g_SpawnData.YachtWarpVehicleID)
						CPRINTLN(DEBUG_YACHT, "DoWarpOfLastVehicleOnYacht_ForFade - deleted vehicle from new helipad location")	
					ENDIF
				ENDIF
			ENDIF	
			
			// is vehicle on front helipad
			IF (VDIST(vFrontHeliPad, vCoordsVehicle) < 5.0)				
				CPRINTLN(DEBUG_YACHT, "DoWarpOfLastVehicleOnYacht_ForFade - last vehicle is on front heli pad")		
				// if we are removing the front heli pad
				IF (LocalData.YachtData[LocalData.iYachtToFade].Appearance.iOption = 0)						
					DELETE_VEHICLE(g_SpawnData.YachtWarpVehicleID)
					CPRINTLN(DEBUG_YACHT, "DoWarpOfLastVehicleOnYacht_ForFade - deleted vehicle, removed front helipad")
				ELSE
					// if we are creating the front helipad clear any vehicles from here.
					IF (LocalData.iYachtFadeOldOption = 0)
						// delete
						DELETE_VEHICLE(g_SpawnData.YachtWarpVehicleID)
						CPRINTLN(DEBUG_YACHT, "DoWarpOfLastVehicleOnYacht_ForFade - deleted vehicle from new front helipad location")	
					ENDIF
				ENDIF						
			ENDIF
			
			
		ELSE
			CPRINTLN(DEBUG_YACHT, "DoWarpOfLastVehicleOnYacht_ForFade - does not have control")
		ENDIF
			
		
		
		
		
	ENDIF

	DeleteLastVehicleOnYacht_ForFade(LocalData)

ENDPROC

PROC UpdateStoreLastVehicleOnYacht_ForFade(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	// store last vehicle (if it is on yacht)
	g_SpawnData.YachtWarpVehicleID = GET_PLAYERS_LAST_VEHICLE()
	IF DOES_ENTITY_EXIST(g_SpawnData.YachtWarpVehicleID)
		IF IS_VEHICLE_ON_YACHT(g_SpawnData.YachtWarpVehicleID, LocalData.iYachtToFade, YACHT_INCLUDING_VEHICLES_DISTANCE)	
		
			// if it is an plane or heli, check it has landed.
			IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(g_SpawnData.YachtWarpVehicleID))
			OR IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(g_SpawnData.YachtWarpVehicleID))
				IF NOT IS_VEHICLE_ON_ALL_WHEELS(g_SpawnData.YachtWarpVehicleID)
					g_SpawnData.YachtWarpVehicleID = NULL
					CPRINTLN(DEBUG_YACHT, "UpdateStoreLastVehicleOnYacht_ForFade - last vehicle is plane or heli and not landed.")		
					EXIT
				ENDIF
			ENDIF
		
			#IF IS_DEBUG_BUILD
			VECTOR vCoords = GET_ENTITY_COORDS(g_SpawnData.YachtWarpVehicleID, FALSE)			
			CPRINTLN(DEBUG_YACHT, "UpdateStoreLastVehicleOnYacht_ForFade - last vehicle on yacht at ", vCoords)	
			#ENDIF
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(g_SpawnData.YachtWarpVehicleID)	
				CPRINTLN(DEBUG_YACHT, "UpdateStoreLastVehicleOnYacht_ForFade - got control of vehicle ")	
			ELSE
				// Only request control if nobody is driving that vehicle, otherwise 
				// ownership just goes back and forth and this causes vehicles to lag for people who drive them
				// (url:bugstar:2593630)
				IF GET_PED_IN_VEHICLE_SEAT(g_SpawnData.YachtWarpVehicleID, VS_DRIVER) = NULL
				OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_SpawnData.YachtWarpVehicleID)
					CPRINTLN(DEBUG_YACHT, "UpdateStoreLastVehicleOnYacht_ForFade - requesting control of vehicle ")
					NETWORK_REQUEST_CONTROL_OF_ENTITY(g_SpawnData.YachtWarpVehicleID)
				ENDIF
			ENDIF			
			
		ELSE
			g_SpawnData.YachtWarpVehicleID = NULL	
			CPRINTLN(DEBUG_YACHT, "UpdateStoreLastVehicleOnYacht_ForFade - last vehicle NOT on yacht.")	
		ENDIF
	ENDIF	

ENDPROC

PROC UPDATE_FADE_FOR_YACHT_LOADER(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	
	IF IS_PRIVATE_YACHT_ID_VALID(LocalData.iYachtToFade)
			
		IF (LocalData.iYachtFadeState > -1)
			UpdateStoreLastVehicleOnYacht_ForFade(LocalData)
		ENDIF
		
		// should we start?
		IF (LocalData.iYachtFadeState = 0)
			IF IS_PLAYER_ON_YACHT(PLAYER_ID(), LocalData.iYachtToFade, DEFAULT, TRUE)
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE )
				IF NOT IS_BROWSER_OPEN()
					DO_SCREEN_FADE_OUT(500)
				ENDIF
				CPRINTLN(DEBUG_YACHT, "UPDATE_FADE_FOR_YACHT_LOADER - starting, for yacht ", LocalData.iYachtToFade)
				LocalData.iYachtFadeState++
			ELSE
				CLEANUP_FADE_FOR_YACHT_LOADER(LocalData)
				EXIT
			ENDIF
		ENDIF
		
		// wait for fade out
		IF (LocalData.iYachtFadeState = 1)
			IF IS_SCREEN_FADED_OUT()
			OR IS_BROWSER_OPEN()
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_USE_PLAYER_FADE_OUT)
				LocalData.iYachtFadeState++
				g_SpawnData.iYachtToWarpTo = LocalData.iYachtToFade
				//g_SpawnData.bHasAccessToYachtWarp = DOES_LOCAL_PLAYER_HAVE_ACCESS_TO_YACHT(g_SpawnData.iYachtToWarpTo)
				g_SpawnData.bHasAccessToYachtWarp = TRUE // don't kick anyone to shore for just changing model.
				
				CPRINTLN(DEBUG_YACHT, "UPDATE_FADE_FOR_YACHT_LOADER - faded out, iYachtToWarpTo ", g_SpawnData.iYachtToWarpTo, ", g_SpawnData.bHasAccessToYachtWarp = ", g_SpawnData.bHasAccessToYachtWarp )
			ELSE
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(500)
				ENDIF
				CPRINTLN(DEBUG_YACHT, "UPDATE_FADE_FOR_YACHT_LOADER - fading out")
			ENDIF
		ENDIF
		
		// delete objects on yacht
		IF (LocalData.iYachtFadeState = 2)
			
			DoWarpOfLastVehicleOnYacht_ForFade(LocalData)
		
			IF HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT(LocalData.YachtData[LocalData.iYachtToFade].Appearance, SHOULD_YACHT_OBJECTS_BE_DISABLED())
			
				DELETE_HULL_OBJECTS_FOR_YACHT(LocalData.YachtData[LocalData.iYachtToFade])
				DELETE_OBJECTS_FOR_YACHT(LocalData.YachtData[LocalData.iYachtToFade])
			
				CPRINTLN(DEBUG_YACHT, "UPDATE_FADE_FOR_YACHT_LOADER - creating objects")
				
				CREATE_HULL_OBJECTS_FOR_YACHT(LocalData.iYachtToFade, LocalData.YachtData[LocalData.iYachtToFade])
				CREATE_OBJECTS_FOR_YACHT(LocalData.iYachtToFade, LocalData.YachtData[LocalData.iYachtToFade], SHOULD_YACHT_OBJECTS_BE_DISABLED())
				REMOVE_ASSETS_FOR_YACHT(LocalData.YachtData[LocalData.iYachtToFade].Appearance)				
				
				LocalData.iYachtFadeState++
				EXIT
			ELSE
				
				IF NOT IS_BROWSER_OPEN()
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_OUT(0)
					ENDIF
				ENDIF
			
				CPRINTLN(DEBUG_YACHT, "UPDATE_FADE_FOR_YACHT_LOADER - loading assets...")
			ENDIF
		ENDIF	
		
		// do warp
		IF (LocalData.iYachtFadeState = 3)
		
			DoWarpOfLastVehicleOnYacht_ForFade(LocalData)
			IF (g_SpawnData.bHasAccessToYachtWarp)
				IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_PRIVATE_FRIEND_YACHT, FALSE)	
					LocalData.YachtData[LocalData.iYachtToFade].bUpdateObjects = TRUE
					GET_APPEARANCE_OF_YACHT(LocalData.iYachtToFade, LocalData.YachtData[LocalData.iYachtToFade].Appearance) 				
					LocalData.iYachtFadeState++
					LocalData.YachtFadeTimer = GET_NETWORK_TIME()					
				ELSE
					CPRINTLN(DEBUG_YACHT, "UPDATE_FADE_FOR_YACHT_LOADER - doing warp")	
				ENDIF
			ELSE
				IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_PRIVATE_YACHT_NEAR_SHORE, FALSE)	
					LocalData.YachtData[LocalData.iYachtToFade].bUpdateObjects = TRUE
					GET_APPEARANCE_OF_YACHT(LocalData.iYachtToFade, LocalData.YachtData[LocalData.iYachtToFade].Appearance) 
					LocalData.iYachtFadeState++
					LocalData.YachtFadeTimer = GET_NETWORK_TIME()
				ELSE
					CPRINTLN(DEBUG_YACHT, "UPDATE_FADE_FOR_YACHT_LOADER - doing warp")	
				ENDIF
			
			ENDIF
			
		ENDIF
		
		// wait a seconds after the warp has completed to allow vehicles to spawn in
		IF (LocalData.iYachtFadeState = 4)
			IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), LocalData.YachtFadeTimer)) > 2000
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
				CLEANUP_FADE_FOR_YACHT_LOADER(LocalData)	
			ELSE
				CPRINTLN(DEBUG_YACHT, "UPDATE_FADE_FOR_YACHT_LOADER - waiting to do fade")	
			ENDIF
		ENDIF
		
	ELSE
		IF NOT (LocalData.iYachtFadeState = -1)
			CPRINTLN(DEBUG_YACHT, "UPDATE_FADE_FOR_YACHT_LOADER - invalid yacht id, resetting state ")
			CLEANUP_FADE_FOR_YACHT_LOADER(LocalData)
		ENDIF
	ENDIF
	
	IF NOT (LocalData.iYachtFadeState = -1)
		MPGlobalsPrivateYacht.bPlayerDoingYachtFade = TRUE
	ELSE
		MPGlobalsPrivateYacht.bPlayerDoingYachtFade = FALSE
	ENDIF
	
ENDPROC

/// PURPOSE:
///    This is the only place that yacht objects (hull + railings etc.) should be managed.
/// PARAMS:
///    LocalData - 
///    iYachtID - 
///    bIgnoreFadeCheck - 
 PROC UPDATE_PRIVATE_YACHT_HULL_OBJECTS(PRIVATE_YACHT_LOCAL_DATA &LocalData, INT iYachtID, BOOL bIgnoreFadeCheck=FALSE)
	IF NOT IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		EXIT
	ENDIF
	
	BOOL bUnloadHull
	FLOAT fDist //= VDIST(GET_FINAL_RENDERED_CAM_COORD(), GET_MODEL_COORDS_OF_YACHT(iYachtID))
	IF IS_PLAYER_SCTV(PLAYER_ID())
	OR IS_PLAYER_SPECTATING(PLAYER_ID())
	OR IS_BIT_SET(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_GLOBAL_DATA_PLAYER_USING_PERISCOPE)
		fDist = VDIST(GET_FINAL_RENDERED_CAM_COORD(), GET_MODEL_COORDS_OF_YACHT(iYachtID))
	ELSE
		fDist = VDIST(GET_PLAYER_COORDS(PLAYER_ID()), GET_COORDS_OF_PRIVATE_YACHT(iYachtID))
	ENDIF
	
	IF LocalData.iYachtSpawnState[iYachtID] = PRIVATE_YACHT_STATE_LOADED
		IF (fDist < g_sMPTunables.fdistanceyachtcreation OR FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT())
		AND	LocalData.iYachtHullState[iYachtID] = PRIVATE_YACHT_PROPS_STATE_NULL
		AND NOT SHOULD_YACHT_OBJECTS_BE_DISABLED()
			// Yacht IPLS are loaded but hull isnt, start loading the hull
			REQUEST_ASSETS_FOR_PRIVATE_YACHT(LocalData.YachtData[iYachtID].Appearance, SHOULD_YACHT_OBJECTS_BE_DISABLED())
			PRINTLN("[YACHT_HULL] UPDATE_PRIVATE_YACHT_HULL_OBJECTS - Requested all yacht assets")
			LocalData.iYachtHullState[iYachtID] = PRIVATE_YACHT_PROPS_STATE_REQUESTED
		ENDIF
		
		IF LocalData.iYachtHullState[iYachtID] = PRIVATE_YACHT_PROPS_STATE_REQUESTED
			
			IF HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT(LocalData.YachtData[iYachtID].Appearance, SHOULD_YACHT_OBJECTS_BE_DISABLED())
			AND (NOT IS_FADE_HAPPENING_FOR_YACHT_LOADER(LocalData) OR bIgnoreFadeCheck)
			AND NOT IS_PLAYER_TELEPORT_ACTIVE()
				CREATE_HULL_OBJECTS_FOR_YACHT(iYachtID, LocalData.YachtData[iYachtID])
				CREATE_OBJECTS_FOR_YACHT(iYachtID, LocalData.YachtData[iYachtID], SHOULD_YACHT_OBJECTS_BE_DISABLED())
				PRINTLN("[YACHT_HULL] UPDATE_PRIVATE_YACHT_HULL_OBJECTS - Created hull objects and yacht objects.")
				
				UpdateAllYachtObjects(LocalData.YachtData[iYachtID])
				
				REMOVE_ASSETS_FOR_YACHT(LocalData.YachtData[iYachtID].Appearance)
				PRINTLN("[YACHT_HULL] UPDATE_PRIVATE_YACHT_HULL_OBJECTS - Removed requested assets")
				
				LocalData.iYachtHullState[iYachtID] = PRIVATE_YACHT_PROPS_STATE_LOADED
			ELSE
				PRINTLN("[YACHT_HULL] UPDATE_PRIVATE_YACHT_HULL_OBJECTS - Waiting for hull objects to load")
			ENDIF
		
		ELIF LocalData.iYachtHullState[iYachtID] = PRIVATE_YACHT_PROPS_STATE_LOADED
			
			YACHT_APPEARANCE Appearance, OldAppearance
			
			// Check different conditions for unloading the objects and hull
			IF fDist > g_sMPTunables.fdistanceyachtcreation + YACHT_OBJECT_CREATE_BUFFER
			OR SHOULD_YACHT_OBJECTS_BE_DISABLED()
				PRINTLN("[YACHT_HULL] UPDATE_PRIVATE_YACHT_HULL_OBJECTS - Player moved away, will remove propos.")
				bUnloadHull = TRUE
			ELSE
			
				IF NOT LocalData.YachtData[iYachtID].bObjectsCreated
					PRINTLN("[YACHT_HULL] UPDATE_PRIVATE_YACHT_HULL_OBJECTS - Something unloaded the objects, reloading hull.")
					bUnloadHull = TRUE
				ENDIF
				
				IF NOT LocalData.YachtData[iYachtID].bHullObjectsCreated
					PRINTLN("[YACHT_HULL] UPDATE_PRIVATE_YACHT_HULL_OBJECTS - Something unloaded the hull, reloading hull.")
					bUnloadHull = TRUE
				ENDIF
				
				IF NOT IS_PLAYER_DOING_A_YACHT_WARP(PLAYER_ID())
				
					UPDATE_YACHT_OBJECTS(LocalData.YachtData[iYachtID])
					
					// has the appearance changed? if so delete yacht objects and recreate 				
					IF NOT (LocalData.YachtData[iYachtID].bHullObjectsCreated)
						IF NOT IS_FADE_HAPPENING_FOR_YACHT_LOADER(LocalData)
							IF HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT(LocalData.YachtData[iYachtID].Appearance, SHOULD_YACHT_OBJECTS_BE_DISABLED())
								CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_HULL_OBJECTS - recreating hull objects after stagger. ", iYachtID, " for player ", GET_PLAYER_NAME(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)), " creating new assets...")
								CREATE_HULL_OBJECTS_FOR_YACHT(iYachtID, LocalData.YachtData[iYachtID])
								SET_YACHT_OBJECTS_TO_BE_CREATED(iYachtID, TRUE, LocalData)
							ENDIF
						ENDIF
					ELSE
						GET_APPEARANCE_OF_YACHT(iYachtID, Appearance)
						IF NOT DO_YACHT_APPEARANCES_MATCH(Appearance, LocalData.YachtData[iYachtID].Appearance)
						
							// am i on this yacht? and the option or railings have change then we need to do a fade.
							IF IS_PLAYER_ON_YACHT(PLAYER_ID(), iYachtID, DEFAULT, TRUE)
							AND ((Appearance.iOption != LocalData.YachtData[iYachtID].Appearance.iOption) OR (Appearance.iRailing != LocalData.YachtData[iYachtID].Appearance.iRailing))
							AND NOT FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
								OldAppearance = LocalData.YachtData[iYachtID].Appearance		
								// store new appearance
								LocalData.YachtData[iYachtID].Appearance = Appearance
								CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_HULL_OBJECTS - appearances didnt match for yacht ", iYachtID, " for player ", GET_PLAYER_NAME(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)), " and i am on this yacht")
								IF NOT IS_FADE_HAPPENING_FOR_YACHT_LOADER(LocalData)
									START_FADE_FOR_YACHT_LOADER(iYachtID, LocalData, OldAppearance.iOption)		
								ELSE
									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_HULL_OBJECTS - fade is happening. yacht ", iYachtID, " for player ", GET_PLAYER_NAME(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)), " and i am on this yacht")								
								ENDIF
							ELSE
								IF NOT IS_FADE_HAPPENING_FOR_YACHT_LOADER(LocalData)
									IF HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT(Appearance, SHOULD_YACHT_OBJECTS_BE_DISABLED())
										CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_HULL_OBJECTS - recreating objects right away ", iYachtID, " for player ", GET_PLAYER_NAME(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)), " creating new assets...")
										DELETE_HULL_OBJECTS_FOR_YACHT(LocalData.YachtData[iYachtID])
										DELETE_OBJECTS_FOR_YACHT(LocalData.YachtData[iYachtID])
										REMOVE_ASSETS_FOR_YACHT(LocalData.YachtData[iYachtID].Appearance)
										// store new appearance
										LocalData.YachtData[iYachtID].Appearance = Appearance
										LocalData.iYachtHullState[iYachtID] = PRIVATE_YACHT_PROPS_STATE_NULL
									ELSE
										CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_HULL_OBJECTS - appearances didnt match, loading new assets. for yacht ", iYachtID, " for player ", GET_PLAYER_NAME(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)), " and i am on this yacht")									
									ENDIF
								ELSE
									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_HULL_OBJECTS - appearances didnt match, fade is happening. for yacht ", iYachtID, " for player ", GET_PLAYER_NAME(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)), " and i am on this yacht")																		
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ELSE
					// player is doing a yacht warp but appearance doesnt match, can do a quick change if the screen is faded out. fix for 2628503 
					IF IS_SCREEN_FADED_OUT()
						GET_APPEARANCE_OF_YACHT(iYachtID, Appearance)
						IF NOT DO_YACHT_APPEARANCES_MATCH(Appearance, LocalData.YachtData[iYachtID].Appearance)
							CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_HULL_OBJECTS - during yacht warp - appearances didnt match. for yacht ", iYachtID, " for player ", GET_PLAYER_NAME(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)))																
							IF HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT(Appearance, SHOULD_YACHT_OBJECTS_BE_DISABLED())
								CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_HULL_OBJECTS - during yacht warp - recreating objects right away ", iYachtID, " for player ", GET_PLAYER_NAME(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)), " creating new assets...")
								DELETE_HULL_OBJECTS_FOR_YACHT(LocalData.YachtData[iYachtID])
								DELETE_OBJECTS_FOR_YACHT(LocalData.YachtData[iYachtID])
								REMOVE_ASSETS_FOR_YACHT(LocalData.YachtData[iYachtID].Appearance)
								// store new appearance
								LocalData.YachtData[iYachtID].Appearance = Appearance
								LocalData.iYachtHullState[iYachtID] = PRIVATE_YACHT_PROPS_STATE_NULL
							ELSE
								CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_HULL_OBJECTS - during yacht warp - appearances didnt match, loading new assets. for yacht ", iYachtID, " for player ", GET_PLAYER_NAME(GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)), " and i am on this yacht")																
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
			ENDIF
		ENDIF
	ELSE
		// If IPLs state is anything other than loaded then unload hull
		IF LocalData.iYachtHullState[iYachtID] = PRIVATE_YACHT_PROPS_STATE_REQUESTED
		OR LocalData.iYachtHullState[iYachtID] = PRIVATE_YACHT_PROPS_STATE_LOADED
			PRINTLN("[YACHT_HULL] UPDATE_PRIVATE_YACHT_HULL_OBJECTS - Yacht stats has changed, will unload propos and hull.")
			IF LocalData.iYachtHullState[iYachtID] = PRIVATE_YACHT_PROPS_STATE_LOADED
				bUnloadHull = TRUE
				PRINTLN("[YACHT_HULL] UPDATE_PRIVATE_YACHT_HULL_OBJECTS - Propos were already loaded, unloading.")
			ENDIF
			
			IF HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT(LocalData.YachtData[iYachtID].Appearance, SHOULD_YACHT_OBJECTS_BE_DISABLED())
				REMOVE_ASSETS_FOR_YACHT(LocalData.YachtData[iYachtID].Appearance)
				PRINTLN("[YACHT_HULL] UPDATE_PRIVATE_YACHT_HULL_OBJECTS - Removing propos resources.")
			ENDIF
		ENDIF
	ENDIF
	
	IF bUnloadHull
	AND LocalData.iYachtHullState[iYachtID] != PRIVATE_YACHT_PROPS_STATE_NULL
		PRINTLN("[YACHT_HULL] UPDATE_PRIVATE_YACHT_HULL_OBJECTS - Unloading the hull.")
		DELETE_HULL_OBJECTS_FOR_YACHT(LocalData.YachtData[iYachtID])		
		DELETE_OBJECTS_FOR_YACHT(LocalData.YachtData[iYachtID])
		LocalData.iYachtHullState[iYachtID] = PRIVATE_YACHT_PROPS_STATE_NULL
	ENDIF
	
	IF NOT MPGlobalsPrivateYacht.bPrivateYachtIsFullyLoaded[iYachtID]
		IF LocalData.YachtData[iYachtID].bObjectsCreated
		AND LocalData.YachtData[iYachtID].bHullObjectsCreated
			MPGlobalsPrivateYacht.bPrivateYachtIsFullyLoaded[iYachtID] = TRUE
			PRINTLN("[YACHT_HULL] UPDATE_PRIVATE_YACHT_HULL_OBJECTS - Setting MPGlobalsPrivateYacht.bPrivateYachtIsFullyLoaded[", iYachtID, "] to TRUE")
		ENDIF
	ELSE
		IF NOT LocalData.YachtData[iYachtID].bObjectsCreated
		OR NOT LocalData.YachtData[iYachtID].bHullObjectsCreated
			MPGlobalsPrivateYacht.bPrivateYachtIsFullyLoaded[iYachtID] = FALSE
			PRINTLN("[YACHT_HULL] UPDATE_PRIVATE_YACHT_HULL_OBJECTS - Setting MPGlobalsPrivateYacht.bPrivateYachtIsFullyLoaded[", iYachtID, "] to FALSE")
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_PRIVATE_YACHT_LOADER(PRIVATE_YACHT_LOCAL_DATA &LocalData, INT iYachtID, BOOL bUpdateAppearance = FALSE)
	
	IF (iYachtID = LocalData.iAlreadyProcessedYachtThisFrame)
		CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - already processed this frame. ", iYachtID)
		EXIT
	ENDIF
	
	INT i	
	PLAYER_INDEX PlayerIDAssignedToYacht
	VECTOR vCoords
	YACHT_APPEARANCE Appearance
	YACHT_APPEARANCE OldAppearance
	
	IF NOT (iYachtID = LocalData.YachtData[iYachtID].iLocation)
		LocalData.YachtData[iYachtID].iLocation = iYachtID
		CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - setting iLocation to ", iYachtID)
	ENDIF
	
	BOOL bProcessYacht = TRUE
	
	IF FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
	AND NOT MPGlobalsPrivateYacht.bPrivateYachtRequested[iYachtID]
	AND LocalData.iYachtSpawnState[iYachtID] = PRIVATE_YACHT_STATE_LOADED
		bProcessYacht = FALSE
	ENDIF
	
	// --------- create / remove yachts ----------	
	IF SHOULD_PRIVATE_YACHT_BE_ACTIVE(iYachtID, LocalData)
	AND bProcessYacht
		PlayerIDAssignedToYacht = GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)
		BOOL bCanLoadIPL = TRUE		
	
		// make sure we've requested the yacht
		SWITCH LocalData.iYachtSpawnState[iYachtID]
			CASE PRIVATE_YACHT_STATE_NULL
			
				#IF IS_DEBUG_BUILD
					IF MPGlobalsPrivateYacht.bUseYachtObjectsOptimisation
						PRINTLN("UPDATE_PRIVATE_YACHT_LOADER bUseYachtObjectsOptimisation is TRUE")
					ELSE
						PRINTLN("UPDATE_PRIVATE_YACHT_LOADER bUseYachtObjectsOptimisation is FALSE")
					ENDIF
				#ENDIF

				// can't request ipl's when warping or doing load scene. 2573013
				IF IS_NEW_LOAD_SCENE_ACTIVE()
				OR IS_PLAYER_TELEPORT_ACTIVE()
					bCanLoadIPL = FALSE
				ENDIF
				
				IF IS_LOCAL_PLAYER_IN_EMPTY_YACHT_SPACE(iYachtID)
					bCanLoadIPL = FALSE	
					START_WARP_FROM_EMPTY_SPACE(iYachtID, LocalData)	
				ENDIF
				
							
				IF bCanLoadIPL			
				
					// if for some reason the ipl's are already loaded then unload them
					IF DoPrivateYachtRemoveIPLCheck(iYachtID, LocalData)				
						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - DoPrivateYachtRemoveIPLCheck true(1).  yacht ", iYachtID)
					ENDIF
				
					IF NOT (PlayerIDAssignedToYacht = INVALID_PLAYER_INDEX())
					OR (GlobalServerBD_BlockC.PYYachtDetails[iYachtID].bYachtIsMarkedForCleanup)
					#IF IS_DEBUG_BUILD
					OR SHOULD_PRIVATE_YACHT_BE_ACTIVE(iYachtID, LocalData)
					#ENDIF
					
						GET_APPEARANCE_OF_YACHT(iYachtID, LocalData.YachtData[iYachtID].Appearance)
					
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - requesting yacht ", iYachtID, " for player ", NATIVE_TO_INT(PlayerIDAssignedToYacht))		
							IF (GlobalServerBD_BlockC.PYYachtDetails[iYachtID].bYachtIsMarkedForCleanup)
								CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - requesting yacht thats marked for cleanup ", iYachtID)	
							ENDIF
						#ENDIF
						REQUEST_IPLS_FOR_PRIVATE_YACHT(iYachtID)	
						LocalData.iYachtSpawnState[iYachtID] = PRIVATE_YACHT_STATE_REQUESTED
						
						IF NOT MPGlobalsPrivateYacht.bUseYachtObjectsOptimisation
							REQUEST_ASSETS_FOR_PRIVATE_YACHT(LocalData.YachtData[iYachtID].Appearance, SHOULD_YACHT_OBJECTS_BE_DISABLED())
						ENDIF
						
						
						// if this is local player's yacht being created then reset some stuff.
						IF (PlayerIDAssignedToYacht = PLAYER_ID())
							CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - local players yacht is being recreated. ")
							
							REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES i
								SET_PRIVATE_YACHT_VEHICLE_FLAG(i, PYVF_COOLDOWN_TIMER_ACTIVE, FALSE)
							ENDREPEAT
							
							IF iYachtID < 36
								// only update saved stat if the yachtid is same zone as our current desired coords
								IF (GET_ZONE_YACHT_IS_IN(iYachtID) = GET_CLOSEST_PRIVATE_YACHT_ZONE_FOR_COORDS(GET_PLAYERS_DESIRED_YACHT_COORDS(PLAYER_ID())))
								AND (VMAG(GET_PLAYERS_DESIRED_YACHT_COORDS(PLAYER_ID())) > 0.01)
									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - local players assigned yacht is in desired zone. saving stat. yacht ", iYachtID)					
									SET_MP_INT_CHARACTER_STAT(MP_STAT_YACHTPREFERREDAREA, iYachtID+1)
								ELSE
									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - local players assigned yacht is NOT in desired zone. yacht ", iYachtID)													
								ENDIF
							ENDIF
							
							SYNC_PLAYER_BD_WITH_YACHT_USE_SWIMWEAR_SETTING()
							
						ENDIF
						
						
						
					ELSE
						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - yacht doesn't have a valid player assigned ", iYachtID, " for player ", NATIVE_TO_INT(PlayerIDAssignedToYacht))					
					ENDIF
				ELSE
					CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - waiting for loadscene or teleport to finish before requesting ipls. Yacht ", iYachtID, " for player ", NATIVE_TO_INT(PlayerIDAssignedToYacht))
				ENDIF
			BREAK
			CASE PRIVATE_YACHT_STATE_REQUESTED
				IF ARE_IPLS_ACTIVE_FOR_PRIVATE_YACHT(iYachtID)
				AND (MPGlobalsPrivateYacht.bUseYachtObjectsOptimisation OR (NOT MPGlobalsPrivateYacht.bUseYachtObjectsOptimisation AND HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT(LocalData.YachtData[iYachtID].Appearance, SHOULD_YACHT_OBJECTS_BE_DISABLED())))
				
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - loaded yacht ", iYachtID, " for player ", NATIVE_TO_INT(PlayerIDAssignedToYacht))
					#ENDIF
					LocalData.iYachtSpawnState[iYachtID] = PRIVATE_YACHT_STATE_LOADED
					
					// add a nav mesh blocking area
					vCoords = GET_COORDS_OF_PRIVATE_YACHT(iYachtID)
					vCoords.z = -10.0 
					
					IF NETWORK_IS_ACTIVITY_SESSION()
					AND FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - skipping nav mesh blocker for yacht ", iYachtID, " due to mission")
					ELSE
						LocalData.YachtData[iYachtID].iNavMeshBlockingObject = ADD_NAVMESH_BLOCKING_OBJECT( vCoords, <<PRIVATE_YACHT_WIDTH, PRIVATE_YACHT_LENGTH, PRIVATE_YACHT_HEIGHT>>, DEG_TO_RAD(GET_HEADING_OF_PRIVATE_YACHT(iYachtID)))
						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - added nav mesh blocker for yacht ", iYachtID, " for nav mesh blocker id =  ", LocalData.YachtData[iYachtID].iNavMeshBlockingObject)
					ENDIF
					
					// create objects
					//CREATE_OBJECTS_FOR_YACHT(iYachtID, LocalData.YachtData[iYachtID])
					//REMOVE_ASSETS_FOR_YACHT(LocalData.YachtData[iYachtID].Appearance)
					IF NOT MPGlobalsPrivateYacht.bUseYachtObjectsOptimisation
						CREATE_HULL_OBJECTS_FOR_YACHT(iYachtID, LocalData.YachtData[iYachtID])
						SET_YACHT_OBJECTS_TO_BE_CREATED(iYachtID, TRUE, LocalData)
					ENDIF
					
					SET_PRIVATE_YACHT_ACTIVE(iYachtID, TRUE)

				ENDIF
			BREAK
			CASE PRIVATE_YACHT_STATE_LOADED				
				
				IF NOT MPGlobalsPrivateYacht.bUseYachtObjectsOptimisation
					// The whole block below has been moved to UPDATE_PRIVATE_YACHT_HULL_OBJECTS which runs if optimisations are enabled
					
					// moved this from outside the warp check for 2615507 
					UPDATE_YACHT_OBJECTS(LocalData.YachtData[iYachtID])
					
					// don't do shit if player is in the middle of warping a yacht. 2607871
					IF NOT IS_PLAYER_DOING_A_YACHT_WARP(PLAYER_ID())
					
						// has the appearance changed? if so delete yacht objects and recreate 				
						IF NOT (LocalData.YachtData[iYachtID].bHullObjectsCreated)
	//						IF NOT IS_FADE_HAPPENING_FOR_YACHT_LOADER(LocalData)
	//							IF HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT(LocalData.YachtData[iYachtID].Appearance, SHOULD_YACHT_OBJECTS_BE_DISABLED())
	//								CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - recreating objects after stagger. ", iYachtID, " for player ", NATIVE_TO_INT(PlayerIDAssignedToYacht), " creating new assets...")
	//								CREATE_OBJECTS_FOR_YACHT(iYachtID, LocalData.YachtData[iYachtID])
	//								REMOVE_ASSETS_FOR_YACHT(LocalData.YachtData[iYachtID].Appearance)
	//							ENDIF
	//						ENDIF
							
							IF NOT IS_FADE_HAPPENING_FOR_YACHT_LOADER(LocalData)
								IF HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT(LocalData.YachtData[iYachtID].Appearance, SHOULD_YACHT_OBJECTS_BE_DISABLED())
									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - recreating hull objects after stagger. ", iYachtID, " for player ", NATIVE_TO_INT(PlayerIDAssignedToYacht), " creating new assets...")
									CREATE_HULL_OBJECTS_FOR_YACHT(iYachtID, LocalData.YachtData[iYachtID])
									SET_YACHT_OBJECTS_TO_BE_CREATED(iYachtID, TRUE, LocalData)
								ENDIF
							ENDIF
							
						ELSE
							GET_APPEARANCE_OF_YACHT(iYachtID, Appearance)
							IF NOT DO_YACHT_APPEARANCES_MATCH(Appearance, LocalData.YachtData[iYachtID].Appearance)
							
								// am i on this yacht? and the option or railings have change then we need to do a fade.
								IF IS_PLAYER_ON_YACHT(PLAYER_ID(), iYachtID, DEFAULT, TRUE)
								AND ((Appearance.iOption != LocalData.YachtData[iYachtID].Appearance.iOption) 
									OR (Appearance.iRailing != LocalData.YachtData[iYachtID].Appearance.iRailing))	
									OldAppearance = LocalData.YachtData[iYachtID].Appearance		
									// store new appearance
									LocalData.YachtData[iYachtID].Appearance = Appearance
									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - appearances didnt match for yacht ", iYachtID, " for player ", NATIVE_TO_INT(PlayerIDAssignedToYacht), " and i am on this yacht")
									IF NOT IS_FADE_HAPPENING_FOR_YACHT_LOADER(LocalData)
										START_FADE_FOR_YACHT_LOADER(iYachtID, LocalData, OldAppearance.iOption)		
									ELSE
										CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - fade is happening. yacht ", iYachtID, " for player ", NATIVE_TO_INT(PlayerIDAssignedToYacht), " and i am on this yacht")								
									ENDIF
								ELSE
									IF NOT IS_FADE_HAPPENING_FOR_YACHT_LOADER(LocalData)
										IF HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT(Appearance, SHOULD_YACHT_OBJECTS_BE_DISABLED())
											CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - recreating objects right away ", iYachtID, " for player ", NATIVE_TO_INT(PlayerIDAssignedToYacht), " creating new assets...")
											DELETE_HULL_OBJECTS_FOR_YACHT(LocalData.YachtData[iYachtID])
											DELETE_OBJECTS_FOR_YACHT(LocalData.YachtData[iYachtID])										
											// store new appearance
											LocalData.YachtData[iYachtID].Appearance = Appearance
											//CREATE_OBJECTS_FOR_YACHT(iYachtID, LocalData.YachtData[iYachtID])
											//REMOVE_ASSETS_FOR_YACHT(LocalData.YachtData[iYachtID].Appearance)
											CREATE_HULL_OBJECTS_FOR_YACHT(iYachtID, LocalData.YachtData[iYachtID])
											SET_YACHT_OBJECTS_TO_BE_CREATED(iYachtID, TRUE, LocalData)
										ELSE
											CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - appearances didnt match, loading new assets. for yacht ", iYachtID, " for player ", NATIVE_TO_INT(PlayerIDAssignedToYacht), " and i am on this yacht")									
										ENDIF
									ELSE
										CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - appearances didnt match, fade is happening. for yacht ", iYachtID, " for player ", NATIVE_TO_INT(PlayerIDAssignedToYacht), " and i am on this yacht")																		
									ENDIF
								ENDIF
								
							ENDIF
						ENDIF
					
					ELSE
						// player is doing a yacht warp but appearance doesnt match, can do a quick change if the screen is faded out. fix for 2628503 
						IF IS_SCREEN_FADED_OUT()
							GET_APPEARANCE_OF_YACHT(iYachtID, Appearance)
							IF NOT DO_YACHT_APPEARANCES_MATCH(Appearance, LocalData.YachtData[iYachtID].Appearance)
								CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - during yacht warp - appearances didnt match. for yacht ", iYachtID, " for player ", NATIVE_TO_INT(PlayerIDAssignedToYacht))																
								IF HAVE_ASSETS_LOADED_FOR_PRIVATE_YACHT(Appearance, SHOULD_YACHT_OBJECTS_BE_DISABLED())
									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - during yacht warp - recreating objects right away ", iYachtID, " for player ", NATIVE_TO_INT(PlayerIDAssignedToYacht), " creating new assets...")
									DELETE_HULL_OBJECTS_FOR_YACHT(LocalData.YachtData[iYachtID])
									DELETE_OBJECTS_FOR_YACHT(LocalData.YachtData[iYachtID])										
									// store new appearance
									LocalData.YachtData[iYachtID].Appearance = Appearance
									CREATE_HULL_OBJECTS_FOR_YACHT(iYachtID, LocalData.YachtData[iYachtID])
									SET_YACHT_OBJECTS_TO_BE_CREATED(iYachtID, TRUE, LocalData)	
								ELSE
									CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - during yacht warp - appearances didnt match, loading new assets. for yacht ", iYachtID, " for player ", NATIVE_TO_INT(PlayerIDAssignedToYacht), " and i am on this yacht")																
								ENDIF
							ENDIF
						ENDIF
					
					ENDIF
				ENDIF
				
				// safe to update colour whenever.
				UPDATE_YACHT_COLOUR(LocalData.YachtData[iYachtID])
				
			BREAK
			
		ENDSWITCH
		
		// add blip						
		BOOL bProcessBlip = FALSE
		BOOL bMakeBlipShortRange = FALSE	
		
		IF (PlayerIDAssignedToYacht = MPGlobals.LocalPlayerID)
		OR (MPGlobalsPrivateYacht.bBlipAllPrivateYachts)
			bProcessBlip = TRUE					
		ENDIF
		
		// should the yacht blip be short range.
		IF (PlayerIDAssignedToYacht = MPGlobals.LocalPlayerID)
		AND NOT IS_PLAYER_IN_BUNKER(PLAYER_ID())
			//IF IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(MPGlobals.LocalPlayerID)
			//	bMakeBlipShortRange = TRUE
			//ENDIF
		ELSE
			bMakeBlipShortRange = TRUE
		ENDIF
		
		// show friend blips
		IF (PlayerIDAssignedToYacht != MPGlobals.LocalPlayerID)
		AND NOT IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(MPGlobals.LocalPlayerID)
			IF NOT (PlayerIDAssignedToYacht = INVALID_PLAYER_INDEX())
			AND IS_NET_PLAYER_OK(PlayerIDAssignedToYacht, FALSE, FALSE)
				g_GamerHandle = GET_GAMER_HANDLE_PLAYER(PlayerIDAssignedToYacht)
				IF IS_GAMER_HANDLE_VALID(g_GamerHandle)
					IF NETWORK_IS_FRIEND(g_GamerHandle)	
						bProcessBlip = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NETWORK_IS_ACTIVITY_SESSION()
		AND FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
		AND iYachtID = GET_FMMC_YACHT_ID(ciYACHT_LOBBY_HOST_YACHT_INDEX)
			bProcessBlip = TRUE
		ENDIF
		
		#IF IS_DEBUG_BUILD	
			IF (LocalData.bBlipIsActive[iYachtID])
				bProcessBlip = TRUE
			ENDIF
		#ENDIF
		
		//IF SHOULD_HIDE_YACHT_BLIP()
		IF IS_BLIP_HIDDEN_FOR_PRIVATE_YACHT(iYachtID)
		OR IS_PLAYER_ON_YACHT(PLAYER_ID(), iYachtID)
		#IF FEATURE_HEIST_ISLAND
		OR IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
		#ENDIF
			bProcessBlip = FALSE
		ENDIF
		
		IF ARE_TEMP_SHORT_RANGE_BLIPS_ACTIVE()
			bMakeBlipShortRange = TRUE
		ENDIF
		
		IF (bProcessBlip)
			IF NOT DOES_BLIP_EXIST(LocalData.YachtBlipID[iYachtID])
				LocalData.YachtBlipID[iYachtID] = ADD_BLIP_FOR_COORD(GET_PRIVATE_YACHT_BLIP_COORDS(iYachtID))
				SET_BLIP_SPRITE(LocalData.YachtBlipID[iYachtID], RADAR_TRACE_YACHT)
				//SET_BLIP_ROTATION(LocalData.YachtBlipID[iYachtID], ROUND(GET_HEADING_OF_PRIVATE_YACHT(iYachtID)))
				SET_BLIP_SCALE(LocalData.YachtBlipID[iYachtID], PRIVATE_YACHT_BLIP_SCALE)
				SET_BLIP_ALPHA(LocalData.YachtBlipID[iYachtID], PRIVATE_YACHT_BLIP_ALPHA)
								
				IF (PlayerIDAssignedToYacht = MPGlobals.LocalPlayerID)
					SET_BLIP_COLOUR(LocalData.YachtBlipID[iYachtID], BLIP_COLOUR_WHITE)	
					BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PY_OWNED")
					END_TEXT_COMMAND_SET_BLIP_NAME(LocalData.YachtBlipID[iYachtID])						
				ELIF PlayerIDAssignedToYacht != INVALID_PLAYER_INDEX()
					SET_BLIP_COLOUR(LocalData.YachtBlipID[iYachtID], BLIP_COLOUR_BLUE)
					BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PY_FRIEND")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_PLAYER_NAME(PlayerIDAssignedToYacht))
					END_TEXT_COMMAND_SET_BLIP_NAME(LocalData.YachtBlipID[iYachtID])
				ELIF FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
					SET_BLIP_COLOUR(LocalData.YachtBlipID[iYachtID], BLIP_COLOUR_BLUE)
					BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PM_SPAWN_Y")
					END_TEXT_COMMAND_SET_BLIP_NAME(LocalData.YachtBlipID[iYachtID])
				ENDIF
				
				SET_BLIP_AS_SHORT_RANGE(LocalData.YachtBlipID[iYachtID], bMakeBlipShortRange)
				SET_BLIP_PRIORITY(LocalData.YachtBlipID[iYachtID], BLIPPRIORITY_LOW_LOWEST)
			ELSE
				
				IF (PlayerIDAssignedToYacht = MPGlobals.LocalPlayerID)
					IF NOT (GET_BLIP_COLOUR(LocalData.YachtBlipID[iYachtID]) = BLIP_COLOUR_WHITE) 
						SET_BLIP_COLOUR(LocalData.YachtBlipID[iYachtID], BLIP_COLOUR_WHITE)
					ENDIF
				ELSE
					IF NOT (GET_BLIP_COLOUR(LocalData.YachtBlipID[iYachtID]) = BLIP_COLOUR_BLUE) 
						SET_BLIP_COLOUR(LocalData.YachtBlipID[iYachtID], BLIP_COLOUR_BLUE)
					ENDIF
				ENDIF
				
				IF NOT (bMakeBlipShortRange = IS_BLIP_SHORT_RANGE(LocalData.YachtBlipID[iYachtID]))
					SET_BLIP_AS_SHORT_RANGE(LocalData.YachtBlipID[iYachtID], bMakeBlipShortRange)
				ENDIF

				#IF IS_DEBUG_BUILD			
					//SET_BLIP_ROTATION(LocalData.YachtBlipID[iYachtID], ROUND(GET_HEADING_OF_PRIVATE_YACHT(iYachtID)))
					SET_BLIP_SCALE(LocalData.YachtBlipID[iYachtID], PRIVATE_YACHT_BLIP_SCALE)
					SET_BLIP_ALPHA(LocalData.YachtBlipID[iYachtID], PRIVATE_YACHT_BLIP_ALPHA)
				#ENDIF
				
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(LocalData.YachtBlipID[iYachtID])
				REMOVE_BLIP(LocalData.YachtBlipID[iYachtID])
			ENDIF
		ENDIF
		
	ELSE
		IF bUpdateAppearance
			GET_APPEARANCE_OF_YACHT(iYachtID, LocalData.YachtData[iYachtID].Appearance)
		ENDIF
		
		BOOL bCanUnloadIPL = FALSE
		
		// can't request ipl's when warping or doing load scene. 2573013
		IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
		AND NOT IS_PLAYER_TELEPORT_ACTIVE()
			//IF LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_WAITING
			//OR LocalData.iYachtDummyState[iYachtID] = PRIVATE_YACHT_DUMMY_DONE
			//OR (SHOULD_PRIVATE_YACHTS_BE_ACTIVE() = FALSE)
			//OR (IS_PRIVATE_YACHT_ACTIVE(iYachtID) = FALSE)
			//	bCanUnloadIPL = TRUE
			//ENDIF	
			bCanUnloadIPL = TRUE
		ENDIF		
		
		IF (bCanUnloadIPL)
			
			// make sure ipl's are definately gone.
			IF DoPrivateYachtRemoveIPLCheck(iYachtID, LocalData)
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - DoPrivateYachtRemoveIPLCheck true(2).  yacht ", iYachtID)
			ENDIF		
			
			// remove yachts
			IF IS_PRIVATE_YACHT_ACTIVE(iYachtID)
			OR (LocalData.iYachtSpawnState[iYachtID] != PRIVATE_YACHT_STATE_NULL)

				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - removing yacht ", iYachtID)
				#ENDIF
				
				LocalData.bRefreshAirDefenceDueToMove = TRUE
				REMOVE_YACHT_DEFENCE(LocalData, iYachtID)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_YACHT, "[AIRDEF] LocalData.bRefreshAirDefenceDueToMove = TRUE ")
				#ENDIF
				
				IF NOT MPGlobalsPrivateYacht.bUseYachtObjectsOptimisation
					DELETE_HULL_OBJECTS_FOR_YACHT(LocalData.YachtData[iYachtID])		
					// only delete the objects if we are not fading the yacht out.
					//IF NOT IS_YACHT_DUMMY_ANIMATING(iYachtID, LocalData)					
						DELETE_OBJECTS_FOR_YACHT(LocalData.YachtData[iYachtID])
					//ENDIF
					// REMOVE_ASSETS_FOR_YACHT(LocalData.YachtData[iYachtID].Appearance)
							
					// SET_YACHT_OBJECTS_TO_BE_CREATED(iYachtID, FALSE, LocalData)
				ENDIF
				
				//RELEASE_HULL_IPL_OBJECTS(iYachtID, LocalData)	
				REMOVE_IPLS_FOR_PRIVATE_YACHT(iYachtID)	
				SET_PRIVATE_YACHT_ACTIVE(iYachtID, FALSE)
				
				// clear area to remove any vehicles marked as no longer needed.
				CLEAR_AREA(GET_COORDS_OF_PRIVATE_YACHT(iYachtID), 100.0, FALSE)
				
				// remove nav mesh blocker
				IF NOT (LocalData.YachtData[iYachtID].iNavMeshBlockingObject = -1)
					CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_LOADER - removed nav mesh blocker for yacht ", iYachtID, " for nav mesh blocker id =  ", LocalData.YachtData[iYachtID].iNavMeshBlockingObject)
					REMOVE_NAVMESH_BLOCKING_OBJECT(LocalData.YachtData[iYachtID].iNavMeshBlockingObject)
					LocalData.YachtData[iYachtID].iNavMeshBlockingObject = -1					
				ENDIF
				
				LocalData.iYachtSpawnState[iYachtID] = PRIVATE_YACHT_STATE_NULL	

			ENDIF				
		ENDIF
		

		
		// remove blip
		IF DOES_BLIP_EXIST(LocalData.YachtBlipID[iYachtID])
			REMOVE_BLIP(LocalData.YachtBlipID[iYachtID])
		ENDIF
		
	ENDIF
ENDPROC

FUNC BOOL ShouldSuppressWantedLevelForBeingNearFriendlyYacht()
	PLAYER_INDEX OwnerID
	INT iYachtID = GET_NEAREST_ACTIVE_PRIVATE_YACHT_TO_PLAYER(PLAYER_ID())
	IF IS_PRIVATE_YACHT_ID_VALID(iYachtID)
		IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0 )			
			IF IS_PLAYER_NEAR_YACHT(PLAYER_ID(), iYachtID, 150.0)			
				OwnerID = GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)			
				// is the owner a friend, crew member or in same gang?
				IF NOT (OwnerID = INVALID_PLAYER_INDEX())
				AND IS_NET_PLAYER_OK(OwnerID, FALSE, FALSE)
					IF ((OwnerID = PLAYER_ID())
					OR IsPlayerFriendOrCrewMember(OwnerID))
					AND GET_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_DEFENCE_SETTING) != 0
						RETURN(TRUE)
					ENDIF	
				ENDIF
			ENDIF			
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC UPDATE_SUPPRESSED_WANTED_LEVEL_FOR_YACHTS(PRIVATE_YACHT_LOCAL_DATA &LocalData)

	IF NOT (LocalData.bWantedLevelIsSuppressed)
		IF ShouldSuppressWantedLevelForBeingNearFriendlyYacht()
			LocalData.iStoredMaxWantedLevel = GET_MAX_WANTED_LEVEL()
			LocalData.bWantedLevelIsSuppressed = TRUE
			SET_MAX_WANTED_LEVEL(0)
			CPRINTLN(DEBUG_YACHT, "UPDATE_SUPPRESSED_WANTED_LEVEL_FOR_YACHTS - suppressing wanted level. storing max level of ", LocalData.iStoredMaxWantedLevel)
		ENDIF
	ELSE
		IF NOT ShouldSuppressWantedLevelForBeingNearFriendlyYacht()
			IF (LocalData.iStoredMaxWantedLevel > -1)
				SET_MAX_WANTED_LEVEL(LocalData.iStoredMaxWantedLevel)
			ELSe
				SET_MAX_WANTED_LEVEL(5)
			ENDIF
			CPRINTLN(DEBUG_YACHT, "UPDATE_SUPPRESSED_WANTED_LEVEL_FOR_YACHTS - restoring wanted level. stored max level of ", LocalData.iStoredMaxWantedLevel)
			LocalData.bWantedLevelIsSuppressed = FALSE
			LocalData.iStoredMaxWantedLevel = -1			
		ENDIF
	ENDIF


ENDPROC

PROC UPDATE_RESET_YACHT_VEHICLE_COLOURS(PRIVATE_YACHT_LOCAL_DATA &LocalData)

	INT iVehicle
	INT iCount
	INT Colour1, Colour2, Colour3, Colour4, Colour5, Colour6
	
	IF (LocalData.bResetVehicleColours)
		
		INT iYachtID = LOCAL_PLAYER_PRIVATE_YACHT_ID()
	
		REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES iVehicle
	
			// try and reset the colour
			IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_RESET_COLOUR)
				
				iCount++
			
				IF DOES_ENTITY_EXIST(LocalData.StoredVehicleID[iVehicle])
				AND NOT IS_ENTITY_DEAD(LocalData.StoredVehicleID[iVehicle])
					IF NETWORK_HAS_CONTROL_OF_ENTITY(LocalData.StoredVehicleID[iVehicle])				
						IF NOT (gPrivateYachtSpawnVehicleDetails[iVehicle].iLivery = -1)
							SET_VEHICLE_LIVERY(LocalData.StoredVehicleID[iVehicle], gPrivateYachtSpawnVehicleDetails[iVehicle].iLivery)
						ENDIF												
						GET_PRIVATE_YACHT_VEHICLE_CAR_COLOURS(gPrivateYachtSpawnVehicleDetails[iVehicle].ModelName, LocalData.YachtData[iYachtID].Appearance.iTint, Colour1, Colour2, Colour3, Colour4, Colour5, Colour6)
						SET_VEHICLE_COLOURS(LocalData.StoredVehicleID[iVehicle], Colour1, Colour2)
						SET_VEHICLE_EXTRA_COLOURS(LocalData.StoredVehicleID[iVehicle], Colour3, Colour4)
						SET_VEHICLE_EXTRA_COLOUR_5(LocalData.StoredVehicleID[iVehicle], Colour5)
						SET_VEHICLE_EXTRA_COLOUR_6(LocalData.StoredVehicleID[iVehicle], Colour6)				
						SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_RESET_COLOUR, FALSE)		
						
						CPRINTLN(DEBUG_YACHT, "UPDATE_RESET_YACHT_VEHICLE_COLOURS - change colour, success. ", iVehicle)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(LocalData.StoredVehicleID[iVehicle])
						CPRINTLN(DEBUG_YACHT, "UPDATE_RESET_YACHT_VEHICLE_COLOURS - change colour, requesting control... ", iVehicle)
					ENDIF
				ELSE
					SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_RESET_COLOUR, FALSE)	
					CPRINTLN(DEBUG_YACHT, "UPDATE_RESET_YACHT_VEHICLE_COLOURS - change colour, vehicle is dead or doesn't exist. ", iVehicle)
				ENDIF
			ENDIF	
		
		ENDREPEAT
		
		IF (iCount = 0)
			LocalData.bResetVehicleColours = FALSE
			CPRINTLN(DEBUG_YACHT, "UPDATE_RESET_YACHT_VEHICLE_COLOURS - finished updating all vehicle colours.")
		ELSE
			CPRINTLN(DEBUG_YACHT, "UPDATE_RESET_YACHT_VEHICLE_COLOURS - icount = ", iCount)
		ENDIF
		
	ENDIF
	
ENDPROC

PROC RESET_ALL_STORED_YACHT_VEHICLE_COLOURS(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	CPRINTLN(DEBUG_YACHT, "[YACHTSPAWN] RESET_ALL_STORED_YACHT_VEHICLE_COLOURS - called.")
	INT iVehicle
	REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES iVehicle
		IF DOES_ENTITY_EXIST(LocalData.StoredVehicleID[iVehicle])
			SET_PRIVATE_YACHT_VEHICLE_FLAG(iVehicle, PYVF_RESET_COLOUR, TRUE)
		ENDIF
	ENDREPEAT
	LocalData.bResetVehicleColours = TRUE
ENDPROC


PROC REFRESH_VEHICLE_FLAGS(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	
	INT iYachtID = LOCAL_PLAYER_PRIVATE_YACHT_ID()
	// do an update of the loader?
	LocalData.iAlreadyProcessedYachtThisFrame = -1
	IF (iYachtID > -1) 
	
		IF NOT (LocalData.YachtData[iYachtID].iLocation = iYachtID)
			CPRINTLN(DEBUG_YACHT, "REFRESH_VEHICLE_FLAGS - setting iLocation to ", iYachtID)
			LocalData.YachtData[iYachtID].iLocation = iYachtID
		ENDIF
		
		IF (LocalData.AppearanceHasChanged.iOption = 1)
		OR (LocalData.AppearanceHasChanged.iTint = 1)
		OR (LocalData.AppearanceHasChanged.iLighting = 1)
		OR (LocalData.AppearanceHasChanged.iRailing = 1)
		OR (LocalData.AppearanceHasChanged.iFlag = 1)
		OR (LocalData.AppearanceHasChanged.iNameAsHash = 1)
		OR (LocalData.bHasChangedYachtAssignment)
			CPRINTLN(DEBUG_YACHT, "REFRESH_VEHICLE_FLAGS - calling loader for yacht ", iYachtID)
			UPDATE_PRIVATE_YACHT_LOADER(LocalData, iYachtID, TRUE)
			LocalData.iAlreadyProcessedYachtThisFrame = iYachtID
		ENDIF
	ENDIF
	
	IF (LocalData.AppearanceHasChanged.iOption = 1)
	OR (LocalData.bHasChangedYachtAssignment)
		IF (iYachtID > -1)
			CPRINTLN(DEBUG_YACHT, "REFRESH_VEHICLE_FLAGS - refreshing ")
			RESET_VEHICLES_ON_MY_YACHT(LocalData)
		ENDIF
	ENDIF
	IF (LocalData.AppearanceHasChanged.iTint = 1)
		CPRINTLN(DEBUG_YACHT, "REFRESH_VEHICLE_FLAGS - change colour ")
		RESET_ALL_STORED_YACHT_VEHICLE_COLOURS(LocalData)
	ENDIF
ENDPROC

PROC UPDATE_JUST_CHANGED_FLAGS(PRIVATE_YACHT_LOCAL_DATA &LocalData)

	INT iYachtID = LOCAL_PLAYER_PRIVATE_YACHT_ID()

	// yacht assignment
	LocalData.bHasChangedYachtAssignment = FALSE
	IF (iYachtID != LocalData.iAssignYachtLastFrame)
		LocalData.bHasChangedYachtAssignment = TRUE
		CPRINTLN(DEBUG_YACHT, "UPDATE_JUST_CHANGED_FLAGS - assignment has just changed ")
	ENDIF
	LocalData.iAssignYachtLastFrame = iYachtID


	YACHT_APPEARANCE Appearance
	YACHT_APPEARANCE EmptyAppearance
	
	LocalData.AppearanceHasChanged = EmptyAppearance

	IF (iYachtID > -1) 
		GET_APPEARANCE_OF_YACHT(iYachtID, Appearance)
		IF NOT DO_YACHT_APPEARANCES_MATCH(Appearance, LocalData.AppearanceLastFrame)
		
			IF (Appearance.iOption != LocalData.AppearanceLastFrame.iOption)
				LocalData.AppearanceHasChanged.iOption = 1
			ELSE
				LocalData.AppearanceHasChanged.iOption = 0	
			ENDIF
			
			IF (Appearance.iTint != LocalData.AppearanceLastFrame.iTint)
				LocalData.AppearanceHasChanged.iTint = 1
			ELSE
				LocalData.AppearanceHasChanged.iTint = 0	
			ENDIF
			
			IF (Appearance.iLighting != LocalData.AppearanceLastFrame.iLighting)
				LocalData.AppearanceHasChanged.iLighting = 1
			ELSE
				LocalData.AppearanceHasChanged.iLighting = 0	
			ENDIF
			
			IF (Appearance.iRailing != LocalData.AppearanceLastFrame.iRailing)
				LocalData.AppearanceHasChanged.iRailing = 1
			ELSE
				LocalData.AppearanceHasChanged.iRailing = 0	
			ENDIF
			
			IF (Appearance.iFlag != LocalData.AppearanceLastFrame.iFlag)
				LocalData.AppearanceHasChanged.iFlag = 1
			ELSE
				LocalData.AppearanceHasChanged.iFlag = 0	
			ENDIF
			
			IF (Appearance.iNameAsHash != LocalData.AppearanceLastFrame.iNameAsHash)
				LocalData.AppearanceHasChanged.iNameAsHash = 1
			ELSE
				LocalData.AppearanceHasChanged.iNameAsHash = 0	
			ENDIF
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_YACHT, "UPDATE_JUST_CHANGED_FLAGS - appearance has just changed: ")
				PRINTLN_YACHT_APPEARANCE(LocalData.AppearanceHasChanged)
			#ENDIF

		ENDIF
	ELSE
		LocalData.AppearanceHasChanged = EmptyAppearance
	ENDIF
	IF (iYachtID > -1) 
		GET_APPEARANCE_OF_YACHT(iYachtID, LocalData.AppearanceLastFrame)
	ENDIF

ENDPROC

PROC REFRESH_YACHT_SHORE_SOUND()
	IF MPGlobalsPrivateYacht.bRefreshShoreSound
		IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobalsPrivateYacht.RefreshShoreSoundTime)) > 250)
			CPRINTLN(DEBUG_YACHT, "REFRESH_YACHT_SHORE_SOUND - refreshing now.")	
			MPGlobalsPrivateYacht.bRefreshShoreSound = FALSE
			REFRESH_CLOSEST_OCEAN_SHORELINE()
		ELSE
			CPRINTLN(DEBUG_YACHT, "REFRESH_YACHT_SHORE_SOUND - waiting for timer...")	
		ENDIF
	ENDIF
ENDPROC

//PROC UPDATE_YACHT_VEHICLE_VISIBILITY_FOR_NETWORK_CUTSCENE()
//	IF NETWORK_IS_IN_MP_CUTSCENE()
//		IF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
//			INT iYachtID = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
//			PLAYER_INDEX OwnerID = GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)
//			IF NOT (OwnerID = INVALID_PLAYER_INDEX())
//				INT iVehicle
//				REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES iVehicle
//					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].PrivateYachtDetails.netVehicleID[iVehicle])
//						SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].PrivateYachtDetails.netVehicleID[iVehicle], TRUE, TRUE)
//						CPRINTLN(DEBUG_YACHT, "UPDATE_YACHT_VEHICLE_VISIBILITY_FOR_NETWORK_CUTSCENE - iYachtID ", iYachtID, " OwnerID ", NATIVE_TO_INT(OwnerID), " iVehicle ", iVehicle)
//					ELSE
//						CPRINTLN(DEBUG_YACHT, "UPDATE_YACHT_VEHICLE_VISIBILITY_FOR_NETWORK_CUTSCENE - net id not exist. iYachtID ", iYachtID, " OwnerID ", NATIVE_TO_INT(OwnerID), " iVehicle ", iVehicle)
//					ENDIF
//				ENDREPEAT
//			ELSE
//				CPRINTLN(DEBUG_YACHT, "UPDATE_YACHT_VEHICLE_VISIBILITY_FOR_NETWORK_CUTSCENE - owner invalid. iYachtID ", iYachtID, " OwnerID ", NATIVE_TO_INT(OwnerID))
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC


PROC UPDATE_YACHT_VEHICLES_VISIBITY_FOR_NETWORK_CUTSCENES(INT iYachtID)
	
	
	IF NOT NETWORK_IS_IN_MP_CUTSCENE() // dont make changes during a cutscene
	
		INT iVehicle
		VEHICLE_INDEX VehicleID
		BOOL bHasDriver
		
		// does this yacht have an owner?
		PLAYER_INDEX OwnerID = GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID)
		IF NOT (OwnerID = INVALID_PLAYER_INDEX())

			// is the local player on or near the yacht? 
			IF IS_PLAYER_ON_YACHT(PLAYER_ID(), iYachtID, YACHT_INCLUDING_VEHICLES_DISTANCE)
			
				// turn on visibility for yacht vehicles.
				REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES iVehicle
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].PrivateYachtDetails.netVehicleID[iVehicle])
						
						// does this vehicle have a driver? 
						bHasDriver = FALSE
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].PrivateYachtDetails.netVehicleID[iVehicle])
							VehicleID = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].PrivateYachtDetails.netVehicleID[iVehicle])
							IF NOT IS_ENTITY_DEAD(VehicleID)
								IF NOT IS_VEHICLE_SEAT_FREE(VehicleID, VS_DRIVER)
									bHasDriver = TRUE	
								ENDIF
							ENDIF
						ENDIF
					
						// only make visible if there is no driver
						IF (bHasDriver)
							SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].PrivateYachtDetails.netVehicleID[iVehicle], FALSE)
						ELSE
							SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].PrivateYachtDetails.netVehicleID[iVehicle], TRUE, TRUE)
						ENDIF
						
					ENDIF
				ENDREPEAT
				
			ELSE // the player is no longer near the yacht
				
				// turn off visibility for yacht vehicles.
				REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES iVehicle
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].PrivateYachtDetails.netVehicleID[iVehicle])
						SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(GlobalplayerBD[NATIVE_TO_INT(OwnerID)].PrivateYachtDetails.netVehicleID[iVehicle], FALSE)
					ENDIF
				ENDREPEAT
			
			ENDIF

		ENDIF

	ENDIF


ENDPROC


PROC UPDATE_DISABLED_ACTIONS(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	
	IF IS_PRIVATE_YACHT_HELP_TEXT_FLAG_SET(LocalData, YACHT_HELP_TEXT_AIR_DEFENCE_DISABLE_WEAPONS)
	AND GET_YACHT_PLAYER_IS_ON(PLAYER_ID()) != -1
		SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_UNARMED, TRUE)
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SELECT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SELECT_PREV_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
	ENDIF

ENDPROC
//PROC DO_REMOVE_ALL_PRIVATE_YACHT_IPL_OBJECTS_NOW(PRIVATE_YACHT_LOCAL_DATA &LocalData)
//	IF (MPGlobalsPrivateYacht.bRemoveAllPrivateYachtIPLObjectsNow)
//		CPRINTLN(DEBUG_YACHT, "DO_REMOVE_ALL_PRIVATE_YACHT_IPL_OBJECTS_NOW - running update ")
//		INT iYacht
//		REPEAT NUMBER_OF_PRIVATE_YACHTS iYacht
//			RELEASE_HULL_IPL_OBJECTS(iYacht, LocalData)	
//		ENDREPEAT
//		MPGlobalsPrivateYacht.bRemoveAllPrivateYachtIPLObjectsNow = FALSE
//	ENDIF
//ENDPROC

PROC UPDATE_COLOUR_DURING_WARP(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	IF IS_PLAYER_DOING_A_YACHT_WARP(PLAYER_ID())
	AND (g_SpawnData.iYachtToWarpTo != -1)
		UPDATE_YACHT_COLOUR(LocalData.YachtData[g_SpawnData.iYachtToWarpTo])
		UPDATE_YACHT_OBJECTS(LocalData.YachtData[g_SpawnData.iYachtToWarpTo])
	ENDIF
ENDPROC

PROC HIDE_UNSIDE_JACUZZI_SURFACE(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	// Get jacuzzi absolute water level
//	FLOAT fJacuzziRelativeWaterLevel = -4.840
	IF LOCAL_PLAYER_PRIVATE_YACHT_ID() > -1
		IF DOES_ENTITY_EXIST(LocalData.YachtData[LOCAL_PLAYER_PRIVATE_YACHT_ID()].JacuzziObjectID)
			FLOAT fJacuzziActualWaterLevelb
			VECTOR vWaterLevel = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(LOCAL_PLAYER_PRIVATE_YACHT_ID(), <<0.0, 0.0, -4.840>>)
			fJacuzziActualWaterLevelb = vWaterLevel.Z
			
			VECTOR vCamPosition = GET_FINAL_RENDERED_CAM_COORD()
			
			CDEBUG2LN(DEBUG_SAFEHOUSE, "HIDE_UNSIDE_JACUZZI_SURFACE: vCamPosition.Z: ", vCamPosition.Z)
			CDEBUG2LN(DEBUG_SAFEHOUSE, "HIDE_UNSIDE_JACUZZI_SURFACE: fJacuzziActualWaterLevelb: ", fJacuzziActualWaterLevelb)
			
			IF vCamPosition.Z < fJacuzziActualWaterLevelb
				SET_ENTITY_VISIBLE(LocalData.YachtData[LOCAL_PLAYER_PRIVATE_YACHT_ID()].JacuzziObjectID, FALSE)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "HIDE_UNSIDE_JACUZZI_SURFACE: Camera underwater")
			ELSE
				SET_ENTITY_VISIBLE(LocalData.YachtData[LOCAL_PLAYER_PRIVATE_YACHT_ID()].JacuzziObjectID, TRUE)
				CDEBUG2LN(DEBUG_SAFEHOUSE, "HIDE_UNSIDE_JACUZZI_SURFACE: Camera above water")
		//		SET_ENTITY_LOCALLY_INVISIBLE(LocalData.YachtData[iYachtID].JacuzziObjectID)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CHECK_LOCAL_PLAYER_HAS_DESIRED_COORDS_SET(PRIVATE_YACHT_LOCAL_DATA &LocalData)
	// check server isn't waiting for me to set desired coords
	IF (LocalData.iClientPlayerStagger = NATIVE_TO_INT(PLAYER_ID()))
		IF DOES_PLAYER_OWN_PRIVATE_YACHT(PLAYER_ID())
			IF NOT HAS_PLAYER_BEEN_ASSIGNED_PRIVATE_YACHT(PLAYER_ID())
			AND NOT NETWORK_IS_ACTIVITY_SESSION()
				// still waiting for yacht to be assigned
				CPRINTLN(DEBUG_YACHT, "CHECK_LOCAL_PLAYER_HAS_DESIRED_COORDS_SET - still waiting for yacht to be assigned. local player is ", NATIVE_TO_INT(PLAYER_ID()))	
			
				#IF IS_DEBUG_BUILD
					IF IS_PLAYER_SCTV(PLAYER_ID())
						CPRINTLN(DEBUG_YACHT, "CHECK_LOCAL_PLAYER_HAS_DESIRED_COORDS_SET - player is sctv ")						
					ENDIF
					IF NOT NETWORK_IS_PLAYER_CONNECTED(PLAYER_ID())
						CPRINTLN(DEBUG_YACHT, "CHECK_LOCAL_PLAYER_HAS_DESIRED_COORDS_SET - player is not connected ")						
					ENDIF
				#ENDIF
			
				// check if valid desired coords have been set
				IF (VMAG(GET_PLAYERS_DESIRED_YACHT_COORDS(PLAYER_ID())) < 0.01)
						
					CPRINTLN(DEBUG_YACHT, "CHECK_LOCAL_PLAYER_HAS_DESIRED_COORDS_SET - no desired coords set.")
								
					// if player owns a yacht set the desired coords.
					INT iLastYachtID
					iLastYachtID = GET_MP_INT_CHARACTER_STAT(MP_STAT_YACHTPREFERREDAREA)
					iLastYachtID += -1 // the stat default is zero, so we use this as NULL
					PRINTLN("TRANSITION_SPAWN_PLAYER_FM - iLastYachtID = ", iLastYachtID)	
					IF (iLastYachtID = -1)
						SET_LOCAL_PLAYER_BD_DESIRED_YACHT_LOCATION(GET_MP_VECTOR_CHARACTER_STAT(MP_STAT_FM_SPAWN_POSITION))	
					ELSE
						SET_LOCAL_PLAYER_BD_DESIRED_YACHT_LOCATION(GET_COORDS_OF_PRIVATE_YACHT(iLastYachtID))	
					ENDIF							
				
				ELSE
					CPRINTLN(DEBUG_YACHT, "CHECK_LOCAL_PLAYER_HAS_DESIRED_COORDS_SET - local player has properly set desired coords of ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.vDesiredCoords, " player hash ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iPlayerHash)	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_PRIVATE_YACHT_CLIENT(PRIVATE_YACHT_LOCAL_DATA &LocalData)
		
	CHECK_LOCAL_PLAYER_HAS_DESIRED_COORDS_SET(LocalData)
	
	HIDE_UNSIDE_JACUZZI_SURFACE(LocalData)
	
	DO_FLUSH_ALL_OBJECTS_FOR_YACHTS(LocalData)
	
	UPDATE_JUST_CHANGED_FLAGS(LocalData)
	
	REFRESH_VEHICLE_FLAGS(LocalData)
	
	UPDATE_PLAYERS_ON_OR_NEAR_MY_YACHT(LocalData.iClientPlayerStagger, LocalData)
	
	UPDATE_PRIVATE_YACHT_VEHICLE_SPAWNING(LocalData, LocalData.iVehicleStagger)
	
	UPDATE_PRIVATE_YACHT_DOCKING(LocalData)
	
	UPDATE_YACHTS_I_AM_ON_OR_NEAR(LocalData.iClientYachtStagger)
		
	UPDATE_YACHTS_CAM_SHAKE()
	
	UPDATE_YACHT_HORN(LocalData)
	
	//UPDATE_PRIVATE_YACHT_DUMMY(LocalData.iClientYachtStagger, LocalData, LocalData.oYachtDummies[LocalData.iClientYachtStagger], LocalData.oYachtDummiesMast[LocalData.iClientYachtStagger])
	//UPDATE_PRIVATE_YACHT_DUMMY_ANIMATIONS(LocalData)
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		IF GET_FMMC_LOBBY_LEADER() != INVALID_PLAYER_INDEX()
		AND HAS_PLAYER_BEEN_ASSIGNED_PRIVATE_YACHT(GET_FMMC_LOBBY_LEADER())
			IF LocalData.iClientYachtStagger != GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(GET_FMMC_LOBBY_LEADER())
				UPDATE_PRIVATE_YACHT_LOADER(LocalData, GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(GET_FMMC_LOBBY_LEADER()))
			ENDIF
			
			UPDATE_PRIVATE_YACHT_LOADER(LocalData, LocalData.iClientYachtStagger)
		ELSE
			UPDATE_PRIVATE_YACHT_LOADER(LocalData, LocalData.iClientYachtStagger)
		ENDIF
	ELSE
		UPDATE_PRIVATE_YACHT_LOADER(LocalData, LocalData.iClientYachtStagger)
	ENDIF
	
	IF MPGlobalsPrivateYacht.bUseYachtObjectsOptimisation
		UPDATE_PRIVATE_YACHT_HULL_OBJECTS(LocalData, LocalData.iClientYachtStagger)
	ENDIF
	
	UPDATE_YACHT_SEATS(LocalData)
	
	UPDATE_PRIVATE_YACHT_VEHICLE_LOCKED_STATE(LocalData)
	
	UPDATE_SUPPRESSED_WANTED_LEVEL_FOR_YACHTS(LocalData)
	
	UPDATE_YACHT_DEFENCE(LocalData)
	
	UPDATE_DISABLED_ACTIONS(LocalData)
	
	RUN_BUOYS_ON_WATER(LocalData)
	
	UPDATE_FADE_FOR_YACHT_LOADER(LocalData)
	
	REFRESH_YACHT_SHORE_SOUND()
	
	//UPDATE_YACHT_VEHICLE_VISIBILITY_FOR_NETWORK_CUTSCENE()
	UPDATE_YACHT_VEHICLES_VISIBITY_FOR_NETWORK_CUTSCENES(LocalData.iClientYachtStagger)
	
	UPDATE_RESET_YACHT_VEHICLE_COLOURS(LocalData)
	
	UPDATE_CLEANUP_ALL_STORED_YACHT_VEHICLES(LocalData)
	
	IF NOT MPGlobalsPrivateYacht.bUseYachtObjectsOptimisation
		UPDATE_CREATE_OBJECTS_FOR_YACHT(LocalData.iClientYachtStagger, LocalData)
	ENDIF
	
	//DO_REMOVE_ALL_PRIVATE_YACHT_IPL_OBJECTS_NOW(LocalData)
	
	UPDATE_COLOUR_DURING_WARP(LocalData)
	
	UPDATE_WARP_FROM_EMPTY_YACHT_SPACE(LocalData)
	
	// update staggers
	LocalData.iClientPlayerStagger++
	IF (LocalData.iClientPlayerStagger >= NUM_NETWORK_PLAYERS)
		LocalData.iClientPlayerStagger = 0
	ENDIF
	LocalData.iClientYachtStagger++
	IF (LocalData.iClientYachtStagger >= NUMBER_OF_PRIVATE_YACHTS)
		LocalData.iClientYachtStagger = 0
	ENDIF
	LocalData.iSeatStagger++
	IF (LocalData.iSeatStagger >= NUM_SEAT_ON_YACHT)
		LocalData.iSeatStagger = 0
	ENDIF
	LocalData.iBuoyStagger++
	IF (LocalData.iBuoyStagger >= NUMBER_OF_PRIVATE_YACHT_BUOY)
		LocalData.iBuoyStagger = 0
	ENDIF
	LocalData.iDockingStagger++
	IF (LocalData.iDockingStagger >= NUMBER_OF_PRIVATE_YACHT_VEHICLE_DOCKS)
		LocalData.iDockingStagger = 0
	ENDIF
	LocalData.iVehicleStagger++
	IF (LocalData.iVehicleStagger >= NUMBER_OF_PRIVATE_YACHT_VEHICLES)
		LocalData.iVehicleStagger = 0	
	ENDIF
	
	
	//STEVE TAY wants this kept on. 
	
	IF LocalData.bDialogueSwitchedtoDLC = TRUE
		IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE)
			LocalData.bPlayerIsDead = TRUE
		ENDIF
		
		IF NOT IS_LOCAL_PLAYER_NEAR_ANY_AIR_DEF()
		OR LocalData.bPlayerIsDead
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PRINTLN("[AIRDEFDIA] Player isn't near any air defence. g_bUse_MP_DLC_Dialogue =FALSE ")
				g_bUse_MP_DLC_Dialogue =FALSE
				LocalData.bDialogueSwitchedtoDLC = FALSE
				LocalData.bPlayerIsDead = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF MPGlobalsPrivateYacht.bFMMC_RemoveAllYachtIPLS
		IF IS_SKYSWOOP_IN_SKY()
			PRINTLN("[MYACHT] UPDATE_PRIVATE_YACHT_CLIENT | bFMMC_RemoveAllYachtIPLS is set - removing Yacht IPLS now!")
			REMOVE_ALL_PRIVATE_YACHTS_IPLS_NOW()
			MPGlobalsPrivateYacht.bFMMC_RemoveAllYachtIPLS = FALSE
			CLEAR_BIT(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_FMMCYachts)
		ELSE
			PRINTLN("[MYACHT] UPDATE_PRIVATE_YACHT_CLIENT | bFMMC_RemoveAllYachtIPLS is set - waiting for skycam!")
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_PRIVATE_YACHT_SERVER(PRIVATE_YACHT_LOCAL_DATA &LocalData, INT iServerPlayerStagger, PLAYER_INDEX StaggeredPlayerID, BOOL bPlayerActive)

	
	BOOL bYachtActive = TRUE
	
	IF MPGlobals.sFreemodeCache.bNetworkIsActivitySession
		IF NOT FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
		OR GET_FMMC_LOBBY_LEADER() != StaggeredPlayerID
			bYachtActive = FALSE
		ENDIF
	ENDIF
	
	// assign yachts to players
	IF bYachtActive
	AND bPlayerActive
	AND DOES_PLAYER_OWN_PRIVATE_YACHT(StaggeredPlayerID)
	AND NETWORK_IS_PLAYER_CONNECTED(StaggeredPlayerID) // unassign if player leaves session
	AND NOT IS_PLAYER_SCTV(StaggeredPlayerID) // sctv players dont get yachts
		IF NOT HAS_PLAYER_BEEN_ASSIGNED_PRIVATE_YACHT(StaggeredPlayerID)
			IF GET_PLAYERS_DESIRED_YACHT_ID(StaggeredPlayerID) != -1
				SERVER_ASSIGN_FREE_YACHT_TO_PLAYER(iServerPlayerStagger, GlobalplayerBD[iServerPlayerStagger].PrivateYachtDetails.iDesiredYachtID, GlobalplayerBD[iServerPlayerStagger].PrivateYachtDetails.Appearance)
			ELIF VMAG(GET_PLAYERS_DESIRED_YACHT_COORDS(StaggeredPlayerID)) > 0.0
				SERVER_ASSIGN_FREE_YACHT_TO_PLAYER_NEAR_COORDS(iServerPlayerStagger, GlobalplayerBD[iServerPlayerStagger].PrivateYachtDetails.vDesiredCoords, GlobalplayerBD[iServerPlayerStagger].PrivateYachtDetails.Appearance)
			ELSE
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_SERVER - waiting for desired coords to be set for player ", iServerPlayerStagger)			
			ENDIF
		ELSE
			// check the players ipls match the server
			IF NOT DO_YACHT_APPEARANCES_MATCH(GlobalplayerBD[iServerPlayerStagger].PrivateYachtDetails.Appearance, GlobalServerBD_BlockC.PYYachtDetails[GlobalServerBD_BlockC.PYPlayerDetails[iServerPlayerStagger].iPrivateYachtAssignedToPlayer].Appearance)
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_SERVER - updating yacht appearance for player ", iServerPlayerStagger)	
				GlobalServerBD_BlockC.PYYachtDetails[GlobalServerBD_BlockC.PYPlayerDetails[iServerPlayerStagger].iPrivateYachtAssignedToPlayer].Appearance = GlobalplayerBD[iServerPlayerStagger].PrivateYachtDetails.Appearance
			ENDIF
			
			IF NETWORK_IS_ACTIVITY_SESSION()
			AND FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
			AND GET_FMMC_LOBBY_LEADER() = StaggeredPlayerID
			AND HAS_PLAYER_BEEN_ASSIGNED_PRIVATE_YACHT(StaggeredPlayerID)
			AND GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(StaggeredPlayerID) != GET_FMMC_YACHT_ID(ciYACHT_LOBBY_HOST_YACHT_INDEX)
				SERVER_REMOVE_YACHT_FROM_PLAYER(StaggeredPlayerID)	
			ENDIF
		ENDIF
		
		// check if the yacht owner changed the setting for yacht swimwear.
		// if they did we need to update server bd so other players can read that setting even if the yacht owner quits the session
		IF NOT (GlobalServerBD_BlockC.PYPlayerDetails[iServerPlayerStagger].iPrivateYachtAssignedToPlayer = -1)
			IF GlobalServerBD_BlockC.PYYachtDetails[GlobalServerBD_BlockC.PYPlayerDetails[iServerPlayerStagger].iPrivateYachtAssignedToPlayer].bYachtJacuzziUseSwimwear != GlobalplayerBD[iServerPlayerStagger].PrivateYachtDetails.bYachtJacuzziUseSwimwear
				GlobalServerBD_BlockC.PYYachtDetails[GlobalServerBD_BlockC.PYPlayerDetails[iServerPlayerStagger].iPrivateYachtAssignedToPlayer].bYachtJacuzziUseSwimwear = GlobalplayerBD[iServerPlayerStagger].PrivateYachtDetails.bYachtJacuzziUseSwimwear
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_SERVER - owner of the yacht changed jacuzzi swimwear setting ", iServerPlayerStagger)	
			ENDIF
		ENDIF
	ELSE
		IF HAS_PLAYER_BEEN_ASSIGNED_PRIVATE_YACHT(StaggeredPlayerID)
			SERVER_REMOVE_YACHT_FROM_PLAYER(StaggeredPlayerID)	
		ENDIF
	ENDIF
	
	// update who is on what yacht
	INT iYachtID = GET_YACHT_PLAYER_IS_ON(StaggeredPlayerID)
	IF NOT (GlobalServerBD_BlockC.PYPlayerDetails[iServerPlayerStagger].iYachtPlayerIsOn = iYachtID)	
	
		// player has just left a yacht
		IF NOT (GlobalServerBD_BlockC.PYPlayerDetails[iServerPlayerStagger].iYachtPlayerIsOn = -1)
			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_SERVER - player ", iServerPlayerStagger, " has just left yacht ", GlobalServerBD_BlockC.PYPlayerDetails[iServerPlayerStagger].iYachtPlayerIsOn)
			CLEAR_BIT(GlobalServerBD_BlockC.PYYachtDetails[GlobalServerBD_BlockC.PYPlayerDetails[iServerPlayerStagger].iYachtPlayerIsOn].iBS_PlayersOnYacht, iServerPlayerStagger)	
		ENDIF

		// player has just got on a yacht
		IF NOT (iYachtID = -1)
			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_SERVER - player ", iServerPlayerStagger, " has just got on yacht ", iYachtID)
			SET_BIT(GlobalServerBD_BlockC.PYYachtDetails[iYachtID].iBS_PlayersOnYacht, iServerPlayerStagger)
		ENDIF
		
		GlobalServerBD_BlockC.PYPlayerDetails[iServerPlayerStagger].iYachtPlayerIsOn = iYachtID		
	ENDIF

	
	// should abandoned yachts be cleaned up?
	IF (GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].bYachtIsMarkedForCleanup)
		IF (GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].iBS_PlayersOnYacht = 0)
			IF NOT CAN_ANY_PLAYER_SEE_POINT(GET_COORDS_OF_PRIVATE_YACHT(LocalData.iServerYachtStagger), 75.0, DEFAULT, DEFAULT, 300.0)
			AND NOT IS_ANY_PLAYER_NEAR_POINT(GET_COORDS_OF_PRIVATE_YACHT(LocalData.iServerYachtStagger), 300.0)	
			OR (GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].bForceCleanup)
			OR (GlobalServerBD_BlockC.iTotalExistingPrivateYachts >= (NUMBER_OF_PRIVATE_YACHTS - 3)) // there are a limited number of yacht left, time for aggressive cleaning up. 
			
				GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].bYachtIsMarkedForCleanup = FALSE
				GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].bYachtIsActive = FALSE
				GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].iHashOfLastOwner = -1
				GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].iHashOfOwner = -1
				
				GlobalServerBD_BlockC.iTotalExistingPrivateYachts = COUNT_NUMBER_OF_EXISTING_YACHTS()
					
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_SERVER - deleting yacht marked for cleanup ", LocalData.iServerYachtStagger)	
			ENDIF
		ELSE
			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_SERVER - trying to cleanup yacht, but players are still on it. ", LocalData.iServerYachtStagger)	
		ENDIF
	ENDIF
	
	// should yachts be unreserved?
	IF NOT (GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].iPlayerPrivateYachtiIsReservedFor = -1)		
		IF NOT (GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].iPlayerAssignedToPrivateYacht = -1)
			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_SERVER - unreserving as player has been assigned. yacht ", LocalData.iServerYachtStagger, " reserved player ", GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].iPlayerPrivateYachtiIsReservedFor, " assigned player ", GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].iPlayerAssignedToPrivateYacht)
			SERVER_UNRESERVE_YACHT_FOR_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].iPlayerPrivateYachtiIsReservedFor))
		ELSE	
			IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].ReservedAtTime)) > 60000)
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_SERVER - time to unreserve yacht ", LocalData.iServerYachtStagger, " for player ", GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].iPlayerPrivateYachtiIsReservedFor)
				SERVER_UNRESERVE_YACHT_FOR_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].iPlayerPrivateYachtiIsReservedFor))
			ELSE
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_SERVER - yacht ", LocalData.iServerYachtStagger, " is still reserved for player ", GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].iPlayerPrivateYachtiIsReservedFor)
			ENDIF
		ENDIF
	ENDIF
	
	// clear last player assigned to yacht if that player is no longer doing a yacht warp.
	IF NOT (GET_LAST_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(LocalData.iServerYachtStagger) = INVALID_PLAYER_INDEX())
		IF NOT IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(GET_LAST_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(LocalData.iServerYachtStagger))
			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_SERVER - clearing last owner of yacht ", LocalData.iServerYachtStagger, " as they are no longer doing a yacht warp. last player ", GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].iLastPlayerAssignedToPrivateYacht)	
			GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].iLastPlayerAssignedToPrivateYacht = -1				
//			YACHT_APPEARANCE EmptyAppearance
//			GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].Appearance = EmptyAppearance
		ELSE
			// do a force cleanup when the player has finished doing his warp
			IF NOT (GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].bForceCleanup)
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_SERVER -  last owner of yacht doing a warp ", LocalData.iServerYachtStagger, " so setting a force cleanup. last player ", GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].iLastPlayerAssignedToPrivateYacht)				
				GlobalServerBD_BlockC.PYYachtDetails[LocalData.iServerYachtStagger].bForceCleanup = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	LocalData.iServerYachtStagger++
	IF (LocalData.iServerYachtStagger >= NUMBER_OF_PRIVATE_YACHTS)
		LocalData.iServerYachtStagger = 0
	ENDIF	

ENDPROC

#IF IS_DEBUG_BUILD
PROC UPDATE_PRIVATE_YACHT_DEBUG(PRIVATE_YACHT_LOCAL_DATA &LocalData, SceneYachtFunc customYachtCutscene)

	INT iStagger = LocalData.iDebugStagger

	INT i
	VECTOR vCoords1, vCoords2
	FLOAT fWidth, fHeading
	TEXT_LABEL_63 str
	INT iYachtID
	
	// make sure players yacht appearance broadcast data is safe
	MAKE_YACHT_APPEARANCE_SAFE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.Appearance)

	// ----------- NON Staggered stuff -------------------
	IF (g_BuyPrivateYacht)	
		SET_LOCAL_PLAYER_OWNS_PRIVATE_YACHT(TRUE, g_BuyYachtAppearance.iOption, g_BuyYachtAppearance.iTint, g_BuyYachtAppearance.iLighting, g_BuyYachtAppearance.iRailing, g_BuyYachtAppearance.iFlag)
		SET_BIG_ASS_VEHICLE_BS(GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_DLC_BIG_YACHT))
		g_BuyPrivateYacht = FALSE
	ENDIF
	IF (g_RemovePrivateYacht)
		SET_LOCAL_PLAYER_OWNS_PRIVATE_YACHT(FALSE)
		CLEAR_BIG_ASS_VEHICLE_BS(GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_DLC_BIG_YACHT))
		MP_BIG_ASS_VEHICLES_STORE_DATA_IN_STATS()
		g_RemovePrivateYacht = FALSE
	ENDIF
	
	IF (g_BuyPrivateTruck)	
		//SET_LOCAL_PLAYER_OWNS_PRIVATE_TRUCK(TRUE, g_BuyTRUCKAppearance.iOption, g_BuyTRUCKAppearance.iTint, g_BuyTRUCKAppearance.iLighting, g_BuyTRUCKAppearance.iRailing, g_BuyTRUCKAppearance.iFlag)
		SET_BIG_ASS_VEHICLE_BS(GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_DLC_BIG_TRUCK))
		g_BuyPrivateTruck = FALSE
	ENDIF
	IF (g_RemovePrivateTruck)
		//SET_LOCAL_PLAYER_OWNS_PRIVATE_TRUCK(FALSE)
		CLEAR_BIG_ASS_VEHICLE_BS(GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_DLC_BIG_TRUCK))
		MP_BIG_ASS_VEHICLES_STORE_DATA_IN_STATS()
		g_RemovePrivateTruck = FALSE
	ENDIF
	
	IF (g_SetYachtAsRespawnLocation)
//		SET_MP_SPAWN_POINT_SETTING(MP_SETTING_SPAWN_PRIVATE_YACHT)
		CPRINTLN(DEBUG_PAUSE_MENU, "SET_MP_SPAWN_POINT_SETTING - new setting: ", ENUM_TO_INT(MP_SETTING_SPAWN_PRIVATE_YACHT))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_SPAWN_LOCATION_SETTING, ENUM_TO_INT(MP_SETTING_SPAWN_PRIVATE_YACHT))
		NETWORK_SET_CURRENT_SPAWN_LOCATION_OPTION(GET_HASH_KEY("MP_SETTING_SPAWN_PRIVATE_YACHT"))
		g_SetYachtAsRespawnLocation = FALSE
	ENDIF
	
	IF (g_DrawYachtBoundingBoxes)
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF
		REPEAT NUMBER_OF_PRIVATE_YACHTS i
			GET_YACHT_BOUNDING_BOX(i, vCoords1, vCoords2, fWidth)	
			DRAW_DEBUG_ANGLED_AREA(vCoords1, vCoords2, fWidth, 15,200,200,128)
		ENDREPEAT
	ENDIF
	
	IF (g_DrawYachtTopToFlag)
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF
		REPEAT NUMBER_OF_PRIVATE_YACHTS i		
			vCoords1 = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(i, <<PRIVATE_YACHT_TOP_OFFSET_X, PRIVATE_YACHT_TOP_OFFSET_Y, PRIVATE_YACHT_TOP_OFFSET_Z>>)
			vCoords2 = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(i, <<PRIVATE_YACHT_FLAG_OFFSET_X, PRIVATE_YACHT_FLAG_OFFSET_Y, PRIVATE_YACHT_FLAG_OFFSET_Z>>)					
			DRAW_DEBUG_SPHERE(vCoords1, 0.05, 255, 0, 0, 255)
			DRAW_DEBUG_SPHERE(vCoords2, 0.05, 0, 255, 0, 255)
			DRAW_DEBUG_LINE (vCoords1, vCoords2)
		ENDREPEAT		
	ENDIF
	
	IF (g_DrawYachtBoundingBoxes)
	OR (g_DrawYachtTopToFlag)
		REPEAT NUMBER_OF_PRIVATE_YACHTS i
			vCoords1 = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(i, <<PRIVATE_YACHT_TOP_OFFSET_X, PRIVATE_YACHT_TOP_OFFSET_Y, PRIVATE_YACHT_TOP_OFFSET_Z + 1.0>>)
			str = "Private Yacht "
			str += i
			DRAW_DEBUG_TEXT(str, vCoords1, 255, 100, 255, 255)
		ENDREPEAT
	ENDIF
	
	IF (g_DrawYachtZones)
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF	
		REPEAT iMAX_PRIVATE_YACHT_SPAWN_ZONES i
		
			DRAW_DEBUG_SPHERE(g_PrivateYachtZoneCoords[i], 200.0, 128, 50, 16, 64)
			
			str = "g_PrivateYachtZoneCoords["
			str += i
			str += "]"
			DRAW_DEBUG_TEXT(str, g_PrivateYachtZoneCoords[i], 128, 50, 16, 255)
		
//			IF NOT DOES_BLIP_EXIST(g_YachtZoneBlipID[i])
//				g_YachtZoneBlipID[i] = ADD_BLIP_FOR_COORD(g_PrivateYachtZoneCoords[i])
//				//SHOW_NUMBER_ON_BLIP(g_YachtZoneBlipID[i], i)
//				SET_BLIP_COLOUR(g_YachtZoneBlipID[i], BLIP_COLOUR_PINK)
//				SET_BLIP_SCALE(g_YachtZoneBlipID[i], 2.0)
//			ELSE
//				SET_BLIP_COORDS(g_YachtZoneBlipID[i], g_PrivateYachtZoneCoords[i])
//			ENDIF
		ENDREPEAT
		g_bCreatedYachtZoneBlips = TRUE
	ELSE
		IF (g_bCreatedYachtZoneBlips)
			REPEAT iMAX_PRIVATE_YACHT_SPAWN_ZONES i
				IF DOES_BLIP_EXIST(g_YachtZoneBlipID[i])
					REMOVE_BLIP(g_YachtZoneBlipID[i])
				ENDIF
			ENDREPEAT	
			g_bCreatedYachtZoneBlips = FALSE
		ENDIF
	ENDIF
	
	IF (g_bWarpToMyYacht)
		IF (LOCAL_PLAYER_PRIVATE_YACHT_ID() > -1)
			LocalData.bWarpToYachtCoords[LOCAL_PLAYER_PRIVATE_YACHT_ID()] = TRUE
		ENDIF
		g_bWarpToMyYacht = FALSE
	ENDIF
	
	
//	// setup a data structure for your yacht
//	YACHT_DATA YachtData	
//	YachtData.iLocation = 34 			// Paleto Bay
//	YachtData.Appearance.iOption = 2 	// The Aquaris
//	YachtData.Appearance.iTint = 12 	// Vintage
//	YachtData.Appearance.iLighting = 5 	// Blue Vivacious
//	YachtData.Appearance.iRailing = 1 	// Gold Fittings
//	YachtData.Appearance.iFlag = -1 	// no flag 
//
//	// Request the assets for the yacht - note this will also request the ipl's which will create the 'building' (hull)
//	// part of the yacht as soon as it's loaded, so you'll want to create the rest of the yacht as soon as the the rest of the
//	// assets have loaded
//	REQUEST_ASSETS_FOR_MISSION_CREATOR_YACHT(YachtData)
//	
//	// create the rest (this includes the radar, railings, doors, jacuzzi etc.)
//	IF HAVE_ASSETS_LOADED_FOR_MISSION_CREATOR_YACHT(YachtData)
//		CREATE_YACHT_FOR_MISSION_CREATOR(YachtData)
//	ENDIF
//	
//	// unfortunately you'll need to call an update function each frame when the yacht has been created. This is to apply the 
//	// tint to the ipl model as it streams in and out when the player gets near and the colour has to be re-applied each
//	// time
//	UPDATE_YACHT_FOR_MISSION_CREATOR(YachtData)
//	
//	// to cleanup and remove call
//	DELETE_AND_CLEANUP_ASSETS_FOR_MISSION_CREATOR_YACHT(YachtData)
	

	IF (g_bMCYachtCreate)
		IF (g_iMCYachtState = 0)
			REQUEST_ASSETS_FOR_MISSION_CREATOR_YACHT(g_MCYachtData)
			g_iMCYachtState++
		ENDIF
		IF (g_iMCYachtState = 1)
			// wait for stuff to be loaded.
			IF HAVE_ASSETS_LOADED_FOR_MISSION_CREATOR_YACHT(g_MCYachtData)
				// create the yacht
				CREATE_YACHT_FOR_MISSION_CREATOR(g_MCYachtData)
				g_bMCYachtCreate = FALSE	
				g_bMCYachtUpdate = TRUE
			ENDIF
		ENDIF
	ELSE
		IF NOT (g_iMCYachtState = 0)	
			g_iMCYachtState = 0
		ENDIF
	ENDIF
	
	IF (g_bMCYachtUpdate)
		UPDATE_YACHT_FOR_MISSION_CREATOR(g_MCYachtData)
	ENDIF
	
	IF (g_bMCYachtDelete)
		DELETE_AND_CLEANUP_ASSETS_FOR_MISSION_CREATOR_YACHT(g_MCYachtData)
		g_bMCYachtDelete = FALSE
	ENDIF
	
	IF (g_GetPlayerCoordsAsYachtOffset)
		
		vCoords1 = GET_PLAYER_COORDS(PLAYER_ID())
		vCoords1.z += -1.0
		
		i = GET_NEAREST_PRIVATE_YACHT_TO_POINT(GET_PLAYER_COORDS(PLAYER_ID()), FALSE)		
		vCoords2 = GET_COORDS_AS_OFFSET_FROM_YACHT_GIVEN_WORLD_COORDS(i, vCoords1)	
		fHeading = GET_HEADING_AS_OFFSET_FROM_YACHT_GIVEN_WORLD_HEADING(i, GET_ENTITY_HEADING(PLAYER_PED_ID()))
		
		WHILE fHeading < 0.0
			fHeading += 360.0
		ENDWHILE
		WHILE fHeading >= 360.0
			fHeading -= 360.0
		ENDWHILE
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Coord offset from private yacht ")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE(" <<")
		SAVE_FLOAT_TO_DEBUG_FILE(vCoords2.x)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vCoords2.y)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vCoords2.z)
		SAVE_STRING_TO_DEBUG_FILE(">>, ")
		SAVE_FLOAT_TO_DEBUG_FILE(fHeading)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		g_GetPlayerCoordsAsYachtOffset = FALSE
	ENDIF
	
	IF (g_bOutputYachtDetails)
		OUTPUT_ALL_YACHT_DETAILS_TO_DEBUG_FILE()
		g_bOutputYachtDetails = FALSE
	ENDIF
	
	IF (g_bSpawnOnLandCall)
		SET_MY_PRIVATE_YACHT_VEHICLE_SPAWN_ON_LAND(g_iSpawnOnLandVehicle, g_bSpawnOnLandSet)
		g_bSpawnOnLandCall = FALSE
	ENDIF
	
	IF (g_bDrawExteriorYachtSpawnLocations)
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF
		
		iYachtID = GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtIAmOn
		IF NOT (iYachtID = -1)
			REPEAT NUMBER_OF_SPAWN_LOCATIONS_ON_YACHT_EXTERIOR i
				DRAW_DEBUG_SPAWN_POINT(GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, g_PrivateYachtSpawnLocationOffest[i].vPlayerLoc), GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(iYachtID, g_PrivateYachtSpawnLocationOffest[i].fPlayerHeading), HUD_COLOUR_BLUE	)
			ENDREPEAT
		ENDIF
	ENDIF
	
	IF (g_bDrawVehicleSpawnLocations)
		
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF
		
		REPEAT NUMBER_OF_PRIVATE_YACHTS iYachtID
			// land
			REPEAT 3 i
				GET_PRIVATE_YACHT_SPAWN_VEHICLE_LOCATION(i, iYachtID, vCoords1, fHeading, TRUE)
				str = "LndSpwn:Ycht "
				str += iYachtID
				str += " veh "
				str += i		
			
				DRAW_DEBUG_TEXT(str, vCoords1, 178, 0, 255, 255)
				
				DRAW_DEBUG_SPHERE(vCoords1, 0.05, 178, 0, 255, 255)
				
				vCoords1.z += GetModelHeightOffset(gPrivateYachtSpawnVehicleDetails[i].ModelName)
				DRAW_DEBUG_SPHERE(vCoords1, gPrivateYachtSpawnVehicleDetails[i].fClearRadius, 0, 178, 255, 255)
			ENDREPEAT
		
			// sea
			REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES i
				GET_PRIVATE_YACHT_SPAWN_VEHICLE_LOCATION(i, iYachtID, vCoords1, fHeading, FALSE)
				str = "Yacht "
				str += iYachtID
				str += " Veh Spawn "
				str += i
				DRAW_DEBUG_TEXT(str, vCoords1, 178, 0, 255, 255)			
				
				DRAW_DEBUG_SPHERE(vCoords1, 0.05, 178, 0, 255, 255)
				
				vCoords1.z += GetModelHeightOffset(gPrivateYachtSpawnVehicleDetails[i].ModelName)
				DRAW_DEBUG_SPHERE(vCoords1, gPrivateYachtSpawnVehicleDetails[i].fClearRadius, 0, 178, 255, 255)
			ENDREPEAT
			
		ENDREPEAT
	
	ENDIF
	
	// ----------- Staggered stuff -------------------

	// warp to coords
	IF (LocalData.bWarpToYachtCoords[iStagger])
		IF NET_WARP_TO_COORD(GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iStagger, g_PrivateYachtSpawnLocationOffest[0].vPlayerLoc), GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(iStagger, g_PrivateYachtSpawnLocationOffest[0].fPlayerHeading), FALSE, FALSE, FALSE, FALSE, TRUE, TRUE, FALSE)
		//g_SpawnData.iYachtToWarpTo = iStagger
		//IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_PRIVATE_FRIEND_YACHT, FALSE)
			LocalData.bWarpToYachtCoords[iStagger] = FALSE
		ENDIF
	ENDIF
	
	LocalData.iDebugStagger++
	IF (LocalData.iDebugStagger >= NUMBER_OF_PRIVATE_YACHTS)
		LocalData.iDebugStagger = 0
	ENDIF
	
	IF LocalData.rag_net_yacht_scene.bEnableTool
		i = GET_NEAREST_PRIVATE_YACHT_TO_POINT(GET_PLAYER_COORDS(PLAYER_ID()), FALSE)		
		Private_Get_NET_YACHT_SCENE(i, LocalData.sMoveScene
				#IF IS_DEBUG_BUILD
				, &GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS
				, &GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING
				#ENDIF
				)
		Private_Edit_NET_YACHT_SCENE(LocalData.rag_net_yacht_scene, i, LocalData.sMoveScene, customYachtCutscene, LocalData.wgYachtWidget)
		LocalData.rag_net_yacht_scene.bEnableTool = FALSE
		

//SAVE_STRING_TO_DEBUG_FILE("mStart.vPos = ")
//SAVE_VECTOR_TO_DEBUG_FILE(GET_COORDS_AS_OFFSET_FROM_YACHT_GIVEN_WORLD_COORDS(14, <<3968.9324, 675.7486, 21.3159>>))
//SAVE_NEWLINE_TO_DEBUG_FILE()
//
//SAVE_STRING_TO_DEBUG_FILE("mStart.vRot = <<-1.9523, -0.1204, ")
//SAVE_FLOAT_TO_DEBUG_FILE(GET_HEADING_AS_OFFSET_FROM_YACHT_GIVEN_WORLD_HEADING(14, 179.0610))
//SAVE_STRING_TO_DEBUG_FILE(">>")
//SAVE_NEWLINE_TO_DEBUG_FILE()
//
//SAVE_STRING_TO_DEBUG_FILE("mEnd.vPos = ")
//SAVE_VECTOR_TO_DEBUG_FILE(GET_COORDS_AS_OFFSET_FROM_YACHT_GIVEN_WORLD_COORDS(14, <<3966.0464, 672.5682, 21.2119>>))
//SAVE_NEWLINE_TO_DEBUG_FILE()
//
//SAVE_STRING_TO_DEBUG_FILE("mStart.vRot = <<-1.9523, -0.1204, ")
//SAVE_FLOAT_TO_DEBUG_FILE(GET_HEADING_AS_OFFSET_FROM_YACHT_GIVEN_WORLD_HEADING(14, 179.8436))
//SAVE_STRING_TO_DEBUG_FILE(">>")
//SAVE_NEWLINE_TO_DEBUG_FILE()
//SAVE_NEWLINE_TO_DEBUG_FILE()

		
		
		
	ENDIF
	
ENDPROC
#ENDIF







