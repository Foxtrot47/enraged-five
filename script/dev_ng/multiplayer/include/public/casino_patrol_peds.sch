USING "globals.sch"
USING "rage_builtins.sch"
USING "net_include.sch"
USING "net_spawn_move.sch"
USING "net_casino_peds_vars.sch"



FUNC BOOL ShouldRarePedTrigger(CASINO_STRUCT &Data, BOOL bCheckNow=FALSE)

	INT iDiff = GET_GAME_TIMER() - Data.PatrolPedsLocalData.iRareGuardCheckTimer
	//PRINTLN("[casino_patrol] ShouldRarePedTrigger - iDiff ", iDiff)	
	
	IF (iDiff > 10000)
	OR (bCheckNow)
		
		Data.PatrolPedsLocalData.iRareGuardCheckTimer = GET_GAME_TIMER()

		INT iCloudTime = GET_CLOUD_TIME_AS_INT()
		INT iTriggerDiff = (iCloudTime - g_iTimeLastTriggeredRareGuard)
		
		IF (iTriggerDiff > g_sMPtunables.VC_Penthouse_Movie_Window)
		
			Data.PatrolPedsLocalData.iTimeUntilNextWindow = g_sMPtunables.VC_Penthouse_Movie_Interval - (iCloudTime % g_sMPtunables.VC_Penthouse_Movie_Interval)
			
			#IF IS_DEBUG_BUILD
			UGC_DATE remaining
			CONVERT_POSIX_TIME(Data.PatrolPedsLocalData.iTimeUntilNextWindow, remaining)
			PRINTLN("[casino_patrol] ShouldRarePedTrigger - time until next trigger ", remaining.nHour, " hours, ", remaining.nMinute, " mins, ", remaining.nSecond, " seconds")	
			#ENDIF
			
			IF ((g_sMPtunables.VC_Penthouse_Movie_Interval - Data.PatrolPedsLocalData.iTimeUntilNextWindow) < g_sMPtunables.VC_Penthouse_Movie_Window)
				g_iTimeLastTriggeredRareGuard = iCloudTime
				PRINTLN("[casino_patrol] ShouldRarePedTrigger - returning TRUE")	
				RETURN TRUE
			ENDIF		
			
		ELSE
			PRINTLN("[casino_patrol] ShouldRarePedTrigger - too close to g_iTimeLastTriggeredRareGuard, iTriggerDiff = ", iTriggerDiff)
		ENDIF
		
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_CASHIER_RARE_GUARD_ACTIVE(SERVER_BROADCAST_DATA &ServerBD)
	IF (ServerBD.sPatrolPedsBDData.bRareGuardActive)
		IF ServerBD.sPatrolPedsBDData.iRareGuardVariation = 1
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC INIT_RARE_GUARD_STARTING_LOCATION(CASINO_STRUCT &Data, INT iForceVariation)
	
	PRINTLN("[casino_patrol] INIT_RARE_GUARD_STARTING_LOCATION called")
	DEBUG_PRINTCALLSTACK()

	IF (iForceVariation > -1)
	AND (g_sMPtunables.VC_Penthouse_Movie_Variation = -1)
		Data.PatrolPedsLocalData.iStartLoc[PP_RARE_GUARD] = iForceVariation
	ELSE
		IF (g_sMPtunables.VC_Penthouse_Movie_Variation > -1)
			Data.PatrolPedsLocalData.iStartLoc[PP_RARE_GUARD] = g_sMPtunables.VC_Penthouse_Movie_Variation
		ELSE
			Data.PatrolPedsLocalData.iStartLoc[PP_RARE_GUARD] = GET_RANDOM_INT_IN_RANGE(0, 2)
		ENDIF
	ENDIF

	PRINTLN("[casino_patrol] INIT_RARE_GUARD_STARTING_LOCATION iStartLoc = ", Data.PatrolPedsLocalData.iStartLoc[PP_RARE_GUARD])

	
ENDPROC

PROC MAINTAIN_RARE_GUARD_TRIGGER(SERVER_BROADCAST_DATA &ServerBD, CASINO_STRUCT &Data, BOOL bCheckNow, INT iForceVariation)
	
	// should we trigger the rare guard?	
	IF (ServerBD.sPatrolPedsBDData.bRareGuardHasBeenActive = FALSE)
		IF ShouldRarePedTrigger(Data, bCheckNow)
			INIT_RARE_GUARD_STARTING_LOCATION(Data, iForceVariation)		
			ServerBD.sPatrolPedsBDData.bRareGuardActive = TRUE
			ServerBD.sPatrolPedsBDData.bRareGuardHasBeenActive = TRUE
			ServerBD.sPatrolPedsBDData.iRareGuardVariation = Data.PatrolPedsLocalData.iStartLoc[PP_RARE_GUARD]
			PRINTLN("[casino_patrol] MAINTAIN_RARE_GUARD_TRIGGER setting ServerBD.sPatrolPedsBDData.bRareGuardHasBeenActive to TRUE ")
		ENDIF
	ENDIF
	
ENDPROC



FUNC VECTOR GET_NAV_POINT_FOR_PED(INT pedID, INT routeid, INT pointID)

	SWITCH pedID
		CASE PP_WAITRESS // waitress
			SWITCH routeID
				CASE 0
					SWITCH pointID
						CASE 0 RETURN <<1083.8975, 212.8353, -50.2004>>
						CASE 1 RETURN <<1085.4458, 214.4323, -50.2003>>
					ENDSWITCH					
				BREAK
				CASE 1
					SWITCH pointID
						CASE 0 RETURN <<1090.5410, 215.0921, -50.2001>>
						CASE 1 RETURN <<1102.5027, 209.0089, -50.4401>>				
						CASE 2 RETURN <<1108.7422, 215.7435, -50.4401>>
						CASE 3 RETURN <<1110.9210, 224.1998, -50.8403>>
						CASE 4 RETURN <<1117.5, 234.5, -50.4408>>
						CASE 5 RETURN <<1136.6, 237.1, -51.4408>>
						CASE 6 RETURN <<1133.4572, 251.3474, -52.0409>>
						CASE 7 RETURN <<1138.5376, 260.7007, -52.4408>>
						CASE 8 RETURN <<1122.1483, 264.4829, -52.0408>>
						CASE 9 RETURN <<1119.4313, 266.9419, -52.0408>>
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH pointID
						CASE 0 RETURN <<1119.4313, 269.2793, -53.0407>>
						CASE 1 RETURN <<1119.2745, 266.1892, -52.0408>>
					ENDSWITCH					
				BREAK
				CASE 3
					SWITCH pointID
						CASE 0 RETURN <<1134.5107, 264.4656, -52.0408>>
						CASE 1 RETURN <<1134.0173, 251.8178, -52.0409>>
						CASE 2 RETURN <<1122.2549, 256.0220, -51.4407>>
						CASE 3 RETURN <<1118.1177, 252.1854, -51.4407>>
						CASE 4 RETURN <<1114.9, 230.4, -50.8405>>
						CASE 5 RETURN <<1111.2, 223.9, -50.8408>>
						CASE 6 RETURN <<1108.4865, 215.2737, -50.4401>>					
						CASE 7 RETURN <<1102.5027, 209.0089, -50.4401>>
						CASE 8 RETURN <<1089.3425, 215.3053, -50.2000>>
						CASE 9 RETURN <<1085.4458, 214.4323, -50.2003>>
					ENDSWITCH
				BREAK
				
			ENDSWITCH		
		BREAK
		CASE PP_SECURITY // security
			SWITCH routeID
				CASE 0
					SWITCH pointID
						CASE 0 RETURN <<1117.6730, 216.5296, -50.4402>>
					ENDSWITCH		
				BREAK
				CASE 1
					SWITCH pointID
						CASE 0 RETURN <<1112.2841, 215.4711, -50.4399>>
						CASE 1 RETURN <<1106.7634, 208.1734, -50.4399>>
						CASE 2 RETURN <<1092.6716, 212.0716, -49.9996>>
						CASE 3 RETURN <<1089.1505, 216.5024, -50.2001>>
						CASE 4 RETURN <<1090.8816, 222.2579, -50.2003>>
						CASE 5 RETURN <<1090.8816, 222.2579, -50.2003>>
					ENDSWITCH					
				BREAK
				CASE 2
					SWITCH pointID
						CASE 0 RETURN <<1090.6488, 216.5621, -50.2003>>
						CASE 1 RETURN <<1093.6948, 213.9819, -49.9998>>
						CASE 2 RETURN <<1100.6810, 213.6179, -49.9900>>
						CASE 3 RETURN <<1103.7559, 215.5322, -49.9999>>
						CASE 4 RETURN <<1108.2385, 231.1443, -50.8405>>
						CASE 5 RETURN <<1119.7551, 235.6862, -50.8407>>
					ENDSWITCH					
				BREAK
				CASE 3
					SWITCH pointID
						CASE 0 RETURN <<1119.9242, 229.2624, -50.8405>> 
						CASE 1 RETURN <<1115.0432, 222.4329, -50.4395>>
						CASE 2 RETURN <<1114.2593, 220.0584, -50.4395>>
						CASE 3 RETURN <<1115.0265, 217.5052, -50.4395>>
					ENDSWITCH					
				BREAK
			ENDSWITCH
		BREAK
		CASE PP_SECURITY_BOSS // boss
			SWITCH routeID
				CASE 0
					SWITCH pointID
						CASE 0 RETURN <<1145.8082, 257.6537, -52.4409>>
					ENDSWITCH		
				BREAK
				CASE 1
					SWITCH pointID
						CASE 0 RETURN <<1133.7939, 252.1198, -52.0408>>
						CASE 1 RETURN <<1107.8353, 253.1357, -51.4408>>
						CASE 2 RETURN <<1099.2440, 257.7371, -52.2411>>
					ENDSWITCH					
				BREAK
				CASE 2
					SWITCH pointID
						CASE 0 RETURN <<1103.7633, 248.6074, -51.4408>>
						CASE 1 RETURN <<1103.1868, 236.7922, -50.8407>>
						CASE 2 RETURN <<1120.1851, 238.3214, -51.4408>>
						CASE 3 RETURN <<1126.5623, 237.7684, -51.4408>>
						CASE 4 RETURN <<1132.7158, 235.5832, -51.4408>>
						CASE 5 RETURN <<1136.7111, 236.9481, -51.4408>>
						CASE 6 RETURN <<1139.0070, 240.8363, -51.4406>>
						CASE 7 RETURN <<1134.0657, 250.1825, -52.0408>>
						CASE 8 RETURN <<1136.7739, 253.8676, -52.0408>> 
					ENDSWITCH					
				BREAK
			ENDSWITCH
		BREAK
		CASE PP_RARE_GUARD 
			SWITCH routeID
				CASE 0
					SWITCH pointID
						CASE 0 RETURN <<1094.8025, 211.3901, -49.9998>>
						CASE 1 RETURN <<1102.5027, 209.0089, -50.4401>>
						CASE 2 RETURN <<1106.5309, 212.5513, -50.4399>>						
						CASE 3 RETURN <<1109.6748, 217.3111, -50.4399>>
						CASE 4 RETURN <<1110.9210, 224.1998, -50.8403>>
						CASE 5 RETURN <<1121.5034, 238.7709, -51.4407>>
						CASE 6 RETURN <<1118.7870, 254.3507, -51.4407>>
						CASE 7 RETURN <<1131.8569, 256.8680, -52.0308>>
						CASE 8 RETURN <<1123.1338, 264.4388, -52.0407>>
						CASE 9 RETURN <<1119.4543, 266.7539, -52.0407>>
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH pointID
						CASE 0 RETURN <<1119.4313, 269.2793, -53.0407>>
						CASE 1 RETURN <<1119.2745, 266.1892, -52.0408>>
					ENDSWITCH					
				BREAK	
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>

ENDFUNC

#IF IS_DEBUG_BUILD
PROC OutputNavPoints(PATROL_PEDS_LOCAL_DATA &LData)
	
	INT iRoute, iPoint
	
	STRING sPath = "X:/gta5/titleupdate/dev_ng/"
	VECTOR vThisPoint
	
	INT iYear, iMonth, iDay, iHour, iMins, iSec
	GET_LOCAL_TIME(iYear, iMonth, iDay, iHour, iMins, iSec)
	
	TEXT_LABEL_63 sFile
	sFile = "casino_patrol_ped_routes_"
	IF (iHour < 10)
		sFile += 0
	ENDIF
	sFile += iHour
	sFile += "."
	IF (iMins < 10)
		sFile += 0
	ENDIF
	sFile += iMins
	sFile += "."
	IF (iSec < 10)
		sFile += 0
	ENDIF
	sFile += iSec
	sFile += ".txt"

	CLEAR_NAMED_DEBUG_FILE(sPath, sFile)
	OPEN_NAMED_DEBUG_FILE(sPath, sFile)
	
	// ------------------------------- WAITRESS -----------------------------------------
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("CASE PP_WAITRESS // waitress", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	SWITCH routeID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	
	REPEAT NUM_NAV_ROUTES iRoute
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		CASE ", sPath, sFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(iRoute,sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			SWITCH pointID ", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
		REPEAT NUM_NAV_POINTS iPoint
			vThisPoint = LData.vWaitressNavPoints[iRoute][iPoint]
			IF (VMAG(vThisPoint) > 0.0)	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				CASE ", sPath, sFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(iPoint,sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(" RETURN ", sPath, sFile)
	SAVE_VECTOR_TO_NAMED_DEBUG_FILE(vThisPoint, sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)					
			ENDIF
		ENDREPEAT
		
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)		
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)		
	
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)		
	SAVE_STRING_TO_NAMED_DEBUG_FILE("BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// ------------------------------- security -----------------------------------------
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("CASE PP_SECURITY // security", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	SWITCH routeID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	
	REPEAT NUM_NAV_ROUTES iRoute
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		CASE ", sPath, sFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(iRoute,sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			SWITCH pointID ", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
		REPEAT NUM_NAV_POINTS iPoint
			vThisPoint = LData.vSecurityNavPoints[iRoute][iPoint]
			IF (VMAG(vThisPoint) > 0.0)	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				CASE ", sPath, sFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(iPoint,sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(" RETURN ", sPath, sFile)
	SAVE_VECTOR_TO_NAMED_DEBUG_FILE(vThisPoint, sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)					
			ENDIF
		ENDREPEAT
		
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)		
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)		
	
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
	// ------------------------------- boss -----------------------------------------
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("CASE PP_SECURITY_BOSS // boss", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	SWITCH routeID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	
	REPEAT NUM_NAV_ROUTES iRoute
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		CASE ", sPath, sFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(iRoute,sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			SWITCH pointID ", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
		REPEAT NUM_NAV_POINTS iPoint
			vThisPoint = LData.vBossNavPoints[iRoute][iPoint]
			IF (VMAG(vThisPoint) > 0.0)	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				CASE ", sPath, sFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(iPoint,sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(" RETURN ", sPath, sFile)
	SAVE_VECTOR_TO_NAMED_DEBUG_FILE(vThisPoint, sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)					
			ENDIF
		ENDREPEAT
		
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)		
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)		
	
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)		

	// ------------------------------- rare guard -----------------------------------------
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("CASE PP_RARE_GUARD // rare guard", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	SWITCH routeID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	
	REPEAT NUM_NAV_ROUTES iRoute
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		CASE ", sPath, sFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(iRoute,sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			SWITCH pointID ", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
		REPEAT NUM_NAV_POINTS iPoint
			vThisPoint = LData.vRareGuardNavPoints[iRoute][iPoint]
			IF (VMAG(vThisPoint) > 0.0)	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				CASE ", sPath, sFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(iPoint,sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(" RETURN ", sPath, sFile)
	SAVE_VECTOR_TO_NAMED_DEBUG_FILE(vThisPoint, sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)					
			ENDIF
		ENDREPEAT
		
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)		
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)		
	
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
	CLOSE_DEBUG_FILE()
ENDPROC

PROC FillNavPoints(PATROL_PEDS_LOCAL_DATA &LData)
	INT iRoute, iPoint
	
	// waitress
	REPEAT NUM_NAV_ROUTES iRoute
		REPEAT NUM_NAV_POINTS iPoint
			LData.vWaitressNavPoints[iRoute][iPoint] = GET_NAV_POINT_FOR_PED(PP_WAITRESS, iRoute, iPoint)
		ENDREPEAT
	ENDREPEAT
	// security
	REPEAT NUM_NAV_ROUTES iRoute
		REPEAT NUM_NAV_POINTS iPoint
			LData.vSecurityNavPoints[iRoute][iPoint] = GET_NAV_POINT_FOR_PED(PP_SECURITY, iRoute, iPoint)
		ENDREPEAT
	ENDREPEAT	
	// boss
	REPEAT NUM_NAV_ROUTES iRoute
		REPEAT NUM_NAV_POINTS iPoint
			LData.vBossNavPoints[iRoute][iPoint] = GET_NAV_POINT_FOR_PED(PP_SECURITY_BOSS, iRoute, iPoint)
		ENDREPEAT
	ENDREPEAT		
	// rare guard
	REPEAT NUM_NAV_ROUTES iRoute
		REPEAT NUM_NAV_POINTS iPoint
			LData.vRareGuardNavPoints[iRoute][iPoint] = GET_NAV_POINT_FOR_PED(PP_RARE_GUARD, iRoute, iPoint)
		ENDREPEAT
	ENDREPEAT		
ENDPROC

PROC DrawNavPoints(PATROL_PEDS_LOCAL_DATA &LData)
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)

	VECTOR vThisPoint
	VECTOR vLastPoint
	TEXT_LABEL_63 str
	INT iRoute, iPoint

	REPEAT NUM_NAV_ROUTES iRoute		
		vLastPoint = <<0,0,0>>	
		REPEAT NUM_NAV_POINTS iPoint		
			vThisPoint = LData.vWaitressNavPoints[iRoute][iPoint]
			IF (VMAG(vThisPoint) > 0.0)			
				vThisPoint.z += 1.0 + (TO_FLOAT(iRoute) * 0.2)
				DRAW_MARKER(MARKER_SPHERE, vThisPoint, <<0,0,0>>, <<0,0,0>>, <<0.1, 0.1, 0.1>>, 255,0,0,255)
				str = "waitress["
				str += iRoute
				str += "]["
				str += iPoint
				str += "]"
				DRAW_DEBUG_TEXT(str, vThisPoint)
				IF (VMAG(vLastPoint) > 0.0)
					DRAW_LINE (vThisPoint, vLastPoint, 255,0,0,255)
				ENDIF						
			ENDIF
			vLastPoint = vThisPoint		
		ENDREPEAT
	ENDREPEAT
	
	REPEAT NUM_NAV_ROUTES iRoute		
		vLastPoint = <<0,0,0>>	
		REPEAT NUM_NAV_POINTS iPoint		
			vThisPoint = LData.vSecurityNavPoints[iRoute][iPoint]
			IF (VMAG(vThisPoint) > 0.0)			
				vThisPoint.z += 1.0 + (TO_FLOAT(iRoute) * 0.2)
				DRAW_MARKER(MARKER_SPHERE, vThisPoint, <<0,0,0>>, <<0,0,0>>, <<0.1, 0.1, 0.1>>, 0,255,0,255)
				str = "security["
				str += iRoute
				str += "]["
				str += iPoint
				str += "]"
				DRAW_DEBUG_TEXT(str, vThisPoint)
				IF (VMAG(vLastPoint) > 0.0)
					DRAW_LINE (vThisPoint, vLastPoint, 0,255,0,255)
				ENDIF							
			ENDIF
			vLastPoint = vThisPoint	
		ENDREPEAT
	ENDREPEAT	
	
	REPEAT NUM_NAV_ROUTES iRoute		
		vLastPoint = <<0,0,0>>	
		REPEAT NUM_NAV_POINTS iPoint		
			vThisPoint = LData.vBossNavPoints[iRoute][iPoint]
			IF (VMAG(vThisPoint) > 0.0)			
				vThisPoint.z += 1.0 + (TO_FLOAT(iRoute) * 0.2)
				DRAW_MARKER(MARKER_SPHERE, vThisPoint, <<0,0,0>>, <<0,0,0>>, <<0.1, 0.1, 0.1>>, 0,0,255,255)
				str = "boss["
				str += iRoute
				str += "]["
				str += iPoint
				str += "]"
				DRAW_DEBUG_TEXT(str, vThisPoint)
				IF (VMAG(vLastPoint) > 0.0)
					DRAW_LINE (vThisPoint, vLastPoint, 0,0,255,255)
				ENDIF								
			ENDIF
			vLastPoint = vThisPoint
		ENDREPEAT
	ENDREPEAT	
	
	REPEAT NUM_NAV_ROUTES iRoute		
		vLastPoint = <<0,0,0>>	
		REPEAT NUM_NAV_POINTS iPoint		
			vThisPoint = LData.vRareGuardNavPoints[iRoute][iPoint]
			IF (VMAG(vThisPoint) > 0.0)			
				vThisPoint.z += 1.0 + (TO_FLOAT(iRoute) * 0.2)
				DRAW_MARKER(MARKER_SPHERE, vThisPoint, <<0,0,0>>, <<0,0,0>>, <<0.1, 0.1, 0.1>>, 128,0,128,255)
				str = "rareguard["
				str += iRoute
				str += "]["
				str += iPoint
				str += "]"
				DRAW_DEBUG_TEXT(str, vThisPoint)
				IF (VMAG(vLastPoint) > 0.0)
					DRAW_LINE (vThisPoint, vLastPoint, 128,0,128,255)
				ENDIF								
			ENDIF
			vLastPoint = vThisPoint
		ENDREPEAT
	ENDREPEAT	

ENDPROC
#ENDIF

CONST_FLOAT PP_MAX_ELEVATOR_MOVEMENT 0.4

FLOAT PP_fElevatorDoorMovementAmt = 0.4

FUNC FLOAT PP_GET_ELEVATOR_CLOSED_Y(INT iDoorIndex)
	SWITCH iDoorIndex
		CASE 0	RETURN 213.3851		
		CASE 1	RETURN 214.4458		
		CASE 2 	RETURN 267.6397	
		CASE 3 	RETURN 267.6397
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT PP_GET_ELEVATOR_CLOSED_X(INT iDoorIndex)
	SWITCH iDoorIndex
		CASE 0	RETURN 1085.4629	
		CASE 1	RETURN 1084.4022	
		CASE 2 	RETURN 1118.6393
		CASE 3 	RETURN 1120.1393
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT PP_GET_ELEVATOR_OPEN_Y(INT iDoorIndex)
	SWITCH iDoorIndex
		CASE 0	RETURN 212.8900		
		CASE 1	RETURN 214.9408	
		CASE 2 	RETURN 267.6397	
		CASE 3 	RETURN 267.6397
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT PP_GET_ELEVATOR_OPEN_X(INT iDoorIndex)
	SWITCH iDoorIndex
		CASE 0	RETURN 1085.9604	
		CASE 1	RETURN 1083.9030	
		CASE 2 	RETURN 1118.6393 - (0.74)
		CASE 3 	RETURN 1120.1393 + (0.74)
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_ELEVATOR_SOUND_POSITION(INT iElevatorIndex)
	IF (iElevatorIndex = 0)
		RETURN <<1085.0, 214.0, -48.0>>
	ELSE
		RETURN <<1119.4, 267.5, -50.7>>
	ENDIF
ENDFUNC

PROC PP_CLOSE_ELEVATOR_DOOR(PATROL_PEDS_LOCAL_DATA &LData, INT iDoorIndex, INT iElevatorIndex)

	IF NOT (LData.bDoorClosed[iDoorIndex])

		LData.bDoorOpened[iDoorIndex] = FALSE

		FLOAT xPosDis = PP_GET_ELEVATOR_CLOSED_X(iDoorIndex) - PP_GET_ELEVATOR_OPEN_X(iDoorIndex)
		FLOAT yPosDis = PP_GET_ELEVATOR_CLOSED_Y(iDoorIndex) - PP_GET_ELEVATOR_OPEN_Y(iDoorIndex)
		
		FLOAT fDivisionRatioX = 1
		FLOAT fDivisionRatioY = 1
		
		IF ABSF(xPosDis) > ABSF(yPosDis)
			fDivisionRatioY = ABSF(yPosDis) / ABSF(xPosDis)
		ELIF ABSF(yPosDis) > ABSF(xPosDis)
			fDivisionRatioX = ABSF(xPosDis) / ABSF(yPosDis)
		ENDIF
		
		LData.bPlayedSound_Opened[iElevatorIndex] = FALSE
		LData.bPlayedSound_Opening[iElevatorIndex] = FALSE
		
		IF DOES_ENTITY_EXIST(g_CasinoElevatorDoor[iDoorIndex])
			VECTOR vCoords = GET_ENTITY_COORDS(g_CasinoElevatorDoor[iDoorIndex])
			
			IF (iDoorIndex = 0) OR (iDoorIndex = 2)
				IF vCoords.x = PP_GET_ELEVATOR_CLOSED_X(iDoorIndex)
				AND vCoords.y = PP_GET_ELEVATOR_CLOSED_Y(iDoorIndex)
					IF (LData.bPlayedSound_Closed[iElevatorIndex] = FALSE)
						PLAY_SOUND_FROM_COORD(-1, "CLOSED", GET_ELEVATOR_SOUND_POSITION(iElevatorIndex), "MP_PROPERTIES_ELEVATOR_DOORS")
						LData.bPlayedSound_Closed[iElevatorIndex] = TRUE
					ENDIF
				ENDIF
			
				IF vCoords.x = PP_GET_ELEVATOR_OPEN_X(iDoorIndex)
				AND vCoords.y = PP_GET_ELEVATOR_OPEN_Y(iDoorIndex)
					IF (LData.bPlayedSound_Closing[iElevatorIndex] = FALSE)
						PLAY_SOUND_FROM_COORD(-1, "CLOSING", GET_ELEVATOR_SOUND_POSITION(iElevatorIndex), "MP_PROPERTIES_ELEVATOR_DOORS")
						LData.bPlayedSound_Closing[iElevatorIndex] = TRUE
					ENDIF
				ENDIF
			ENDIF
				
			PP_fElevatorDoorMovementAmt = PP_MAX_ELEVATOR_MOVEMENT * fDivisionRatioX
			
			IF xPosDis < 0
				vCoords.x = vCoords.x -@ PP_fElevatorDoorMovementAmt
				
				IF vCoords.x < PP_GET_ELEVATOR_CLOSED_X(iDoorIndex)
					vCoords.x = PP_GET_ELEVATOR_CLOSED_X(iDoorIndex)
				ENDIF
			ENDIF
			
			IF xPosDis > 0
				vCoords.x = vCoords.x +@ PP_fElevatorDoorMovementAmt
				
				IF vCoords.x > PP_GET_ELEVATOR_CLOSED_X(iDoorIndex)
					vCoords.x = PP_GET_ELEVATOR_CLOSED_X(iDoorIndex)
				ENDIF
			ENDIF
			
			PP_fElevatorDoorMovementAmt = PP_MAX_ELEVATOR_MOVEMENT * fDivisionRatioY
			
			IF yPosDis < 0
				vCoords.y = vCoords.y -@ PP_fElevatorDoorMovementAmt
				
				IF vCoords.y < PP_GET_ELEVATOR_CLOSED_Y(iDoorIndex)
					vCoords.y = PP_GET_ELEVATOR_CLOSED_Y(iDoorIndex)
				ENDIF
			ENDIF
			
			IF yPosDis > 0
				vCoords.y = vCoords.y +@ PP_fElevatorDoorMovementAmt
				
				IF vCoords.y > PP_GET_ELEVATOR_CLOSED_Y(iDoorIndex)
					vCoords.y = PP_GET_ELEVATOR_CLOSED_Y(iDoorIndex)
				ENDIF
			ENDIF
			
			// has door fully closed?
			IF vCoords.x = PP_GET_ELEVATOR_CLOSED_X(iDoorIndex)
			AND vCoords.y = PP_GET_ELEVATOR_CLOSED_Y(iDoorIndex)
				LData.bDoorClosed[iDoorIndex] = TRUE
				PRINTLN("[casino_patrol] PP_CLOSE_ELEVATOR_DOOR - setting door as closed.  door ", iDoorIndex)
			ENDIF
			
			
			PRINTLN("[casino_patrol] PP_CLOSE_ELEVATOR_DOOR - set door ", iDoorIndex, " to Coords ", vCoords)
			SET_ENTITY_COORDS(g_CasinoElevatorDoor[iDoorIndex], vCoords)
		ELSE
			PRINTLN("[casino_patrol] PP_CLOSE_ELEVATOR_DOOR - door does not exist ", iDoorIndex)	
		ENDIF
	ENDIF
ENDPROC

PROC PP_OPEN_ELEVATOR_DOOR(PATROL_PEDS_LOCAL_DATA &LData, INT iDoorIndex, INT iElevatorIndex)
	
	IF NOT (LData.bDoorOpened[iDoorIndex])
	
		LData.bDoorClosed[iDoorIndex] = FALSE
	
		FLOAT xPosDis = PP_GET_ELEVATOR_CLOSED_X(iDoorIndex) - PP_GET_ELEVATOR_OPEN_X(iDoorIndex)
		FLOAT yPosDis = PP_GET_ELEVATOR_CLOSED_Y(iDoorIndex) - PP_GET_ELEVATOR_OPEN_Y(iDoorIndex)
		
		FLOAT fDivisionRatioX = 1
		FLOAT fDivisionRatioY = 1
		
		IF ABSF(xPosDis) > ABSF(yPosDis)
			fDivisionRatioY = ABSF(yPosDis) / ABSF(xPosDis)
		ELIF ABSF(yPosDis) > ABSF(xPosDis)
			fDivisionRatioX = ABSF(xPosDis) / ABSF(yPosDis)
		ENDIF
		
		LData.bPlayedSound_Closed[iElevatorIndex] = FALSE
		LData.bPlayedSound_Closing[iElevatorIndex] = FALSE
		
		IF DOES_ENTITY_EXIST(g_CasinoElevatorDoor[iDoorIndex])
			VECTOR vCoords = GET_ENTITY_COORDS(g_CasinoElevatorDoor[iDoorIndex])
			
			IF (iDoorIndex = 0) OR (iDoorIndex = 2)
				IF vCoords.x = PP_GET_ELEVATOR_CLOSED_X(iDoorIndex)
				AND vCoords.y = PP_GET_ELEVATOR_CLOSED_Y(iDoorIndex)
					IF (LData.bPlayedSound_Opening[iElevatorIndex] = FALSE)
						PLAY_SOUND_FROM_COORD(-1, "OPENING", GET_ELEVATOR_SOUND_POSITION(iElevatorIndex), "MP_PROPERTIES_ELEVATOR_DOORS")
						LData.bPlayedSound_Opening[iElevatorIndex] = TRUE
					ENDIF
				ENDIF
				
				IF vCoords.x = PP_GET_ELEVATOR_OPEN_X(iDoorIndex)
				AND vCoords.y = PP_GET_ELEVATOR_OPEN_Y(iDoorIndex)
					IF (LData.bPlayedSound_Opened[iElevatorIndex] = FALSE)
						PLAY_SOUND_FROM_COORD(-1, "OPENED", GET_ELEVATOR_SOUND_POSITION(iElevatorIndex), "MP_PROPERTIES_ELEVATOR_DOORS")
						LData.bPlayedSound_Opened[iElevatorIndex] = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			PP_fElevatorDoorMovementAmt = PP_MAX_ELEVATOR_MOVEMENT * fDivisionRatioX
			
			IF xPosDis > 0
				vCoords.x = vCoords.x -@ PP_fElevatorDoorMovementAmt
				
				IF vCoords.x < PP_GET_ELEVATOR_OPEN_X(iDoorIndex)
					vCoords.x = PP_GET_ELEVATOR_OPEN_X(iDoorIndex)
				ENDIF
			ENDIF
			
			IF xPosDis < 0
				vCoords.x = vCoords.x +@ PP_fElevatorDoorMovementAmt
				
				IF vCoords.x > PP_GET_ELEVATOR_OPEN_X(iDoorIndex)
					vCoords.x = PP_GET_ELEVATOR_OPEN_X(iDoorIndex)
				ENDIF
			ENDIF
			
			PP_fElevatorDoorMovementAmt = PP_MAX_ELEVATOR_MOVEMENT * fDivisionRatioY
			
			IF yPosDis > 0
				vCoords.y = vCoords.y -@ PP_fElevatorDoorMovementAmt
				
				IF vCoords.y < PP_GET_ELEVATOR_OPEN_Y(iDoorIndex)
					vCoords.y = PP_GET_ELEVATOR_OPEN_Y(iDoorIndex)
				ENDIF
			ENDIF
			
			IF yPosDis < 0
				vCoords.y = vCoords.y +@ PP_fElevatorDoorMovementAmt
				
				IF vCoords.y > PP_GET_ELEVATOR_OPEN_Y(iDoorIndex)
					vCoords.y = PP_GET_ELEVATOR_OPEN_Y(iDoorIndex)
				ENDIF
			ENDIF
			
			// has door fully opened?
			IF vCoords.x = PP_GET_ELEVATOR_OPEN_X(iDoorIndex)
			AND vCoords.y = PP_GET_ELEVATOR_OPEN_Y(iDoorIndex)
				LData.bDoorOpened[iDoorIndex] = TRUE
				PRINTLN("[casino_patrol] PP_CLOSE_ELEVATOR_DOOR - setting door as open.  door ", iDoorIndex)
			ENDIF		
			
			PRINTLN("[casino_patrol] PP_OPEN_ELEVATOR_DOOR - set door ", iDoorIndex, " to Coords ", vCoords)
			SET_ENTITY_COORDS(g_CasinoElevatorDoor[iDoorIndex], vCoords)
		ELSE
			PRINTLN("[casino_patrol] PP_OPEN_ELEVATOR_DOOR - door does not exist ", iDoorIndex)	
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL HAS_STAND_GUARD_LOADED()
	REQUEST_ANIM_DICT("amb@world_human_stand_guard@male@base")
	REQUEST_ANIM_DICT("amb@world_human_stand_guard@male@enter")
	REQUEST_ANIM_DICT("amb@world_human_stand_guard@male@exit")
	REQUEST_ANIM_DICT("amb@world_human_stand_guard@male@idle_a")
	REQUEST_ANIM_DICT("amb@world_human_stand_guard@male@idle_b")
	IF HAS_ANIM_DICT_LOADED("amb@world_human_stand_guard@male@base")
	AND HAS_ANIM_DICT_LOADED("amb@world_human_stand_guard@male@enter")
	AND HAS_ANIM_DICT_LOADED("amb@world_human_stand_guard@male@exit")
	AND HAS_ANIM_DICT_LOADED("amb@world_human_stand_guard@male@idle_a")
	AND HAS_ANIM_DICT_LOADED("amb@world_human_stand_guard@male@idle_b")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC MODEL_NAMES TrayModel(BOOL bWithBucket = TRUE)
	IF (bWithBucket)
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_VW_Ice_Bucket_01a"))
	ELSE
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_VW_Tray_01a")) 
	ENDIF
ENDFUNC

FUNC BOOL IsBrawlMissionRunning()
	//PRINTLN("g_FMMC_STRUCT.iCasinoPedLayout = ", g_FMMC_STRUCT.iCasinoPedLayout)
	IF NETWORK_IS_ACTIVITY_SESSION()
		IF g_FMMC_STRUCT.iCasinoPedLayout = 2
			RETURN TRUE
		ENDIF
	ENDIF 
	RETURN FALSE
ENDFUNC

PROC UNLOAD_ALL_ASSETS(PATROL_PEDS_LOCAL_DATA &LData)
	REMOVE_ANIM_DICT("amb@world_human_stand_guard@male@base")
	REMOVE_ANIM_DICT("amb@world_human_stand_guard@male@enter")
	REMOVE_ANIM_DICT("amb@world_human_stand_guard@male@exit")
	REMOVE_ANIM_DICT("amb@world_human_stand_guard@male@idle_a")
	REMOVE_ANIM_DICT("amb@world_human_stand_guard@male@idle_b")
	REMOVE_ANIM_DICT("anim@move_f@waitress")
	REMOVE_ANIM_DICT("anim@move_m@security_guard")
	REMOVE_ANIM_DICT("random@security_van")
	IF NOT IsBrawlMissionRunning()	
		REMOVE_ANIM_DICT("ANIM@AMB@CASINO@PEDS@")
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(TrayModel(TRUE))
	SET_MODEL_AS_NO_LONGER_NEEDED(TrayModel(FALSE))
	SET_MODEL_AS_NO_LONGER_NEEDED(hei_prop_hei_security_case)
	SET_MODEL_AS_NO_LONGER_NEEDED(P_CS_CLIPBOARD)
	INT i
	REPEAT NUMBER_OF_CASINO_PATROL_PEDS i
		IF NOT (LData.eModelName[i] = DUMMY_MODEL_FOR_SCRIPT)
			SET_MODEL_AS_NO_LONGER_NEEDED(LData.eModelName[i])
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL DoesTrayHaveBucket(ENTITY_INDEX ObjectID)
	IF DOES_ENTITY_EXIST(ObjectID)
		IF GET_ENTITY_MODEL(ObjectID) = TrayModel(TRUE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_WAITRESS_ASSETS_LOADED()
	REQUEST_ANIM_DICT("anim@move_f@waitress")
	REQUEST_MODEL(TrayModel(TRUE))
	REQUEST_MODEL(TrayModel(FALSE))
	IF HAS_ANIM_DICT_LOADED("anim@move_f@waitress")
	AND HAS_MODEL_LOADED(TrayModel(TRUE))
	AND HAS_MODEL_LOADED(TrayModel(FALSE))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC UNLOAD_WAITRESS_ASSETS()
	REMOVE_ANIM_DICT("anim@move_f@waitress")
	SET_MODEL_AS_NO_LONGER_NEEDED(TrayModel(TRUE))
	SET_MODEL_AS_NO_LONGER_NEEDED(TrayModel(FALSE))
ENDPROC

FUNC BOOL HAS_SECURITY_ASSETS_LOADED()
	REQUEST_ANIM_DICT("anim@move_m@security_guard")
	IF HAS_STAND_GUARD_LOADED()
	AND HAS_ANIM_DICT_LOADED("anim@move_m@security_guard")
	AND g_bSecurityGuardPedDataSet
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_SECURITY_BOSS_ASSETS_LOADED()
	IF HAS_STAND_GUARD_LOADED()
	AND HAS_ANIM_DICT_LOADED("anim@move_m@security_guard")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_RARE_GUARD_ASSETS_LOADED(INT iVariation)

	IF (iVariation = 0)
		REQUEST_MODEL(hei_prop_hei_security_case)
		REQUEST_ANIM_DICT("random@security_van")
		IF HAS_MODEL_LOADED(hei_prop_hei_security_case)
		AND HAS_ANIM_DICT_LOADED("random@security_van")
			RETURN TRUE
		ENDIF
	ELSE
		REQUEST_MODEL(P_CS_CLIPBOARD)
		REQUEST_ANIM_DICT("ANIM@AMB@CASINO@PEDS@")
		IF HAS_MODEL_LOADED(P_CS_CLIPBOARD)
		AND HAS_ANIM_DICT_LOADED("ANIM@AMB@CASINO@PEDS@")
			RETURN TRUE
		ENDIF	
	ENDIF
	RETURN FALSE
ENDFUNC

PROC UNLOAD_RARE_GUARD_ASSETS()
	REMOVE_ANIM_DICT("random@security_van")
	IF NOT IsBrawlMissionRunning()	
		REMOVE_ANIM_DICT("ANIM@AMB@CASINO@PEDS@")
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(hei_prop_hei_security_case)
	SET_MODEL_AS_NO_LONGER_NEEDED(P_CS_CLIPBOARD)
ENDPROC

FUNC BOOL HAVE_ASSETS_LOADED_FOR_PATROL_PED(PATROL_PEDS_LOCAL_DATA &LData, INT iID)
	IF (LData.eModelName[iID] != DUMMY_MODEL_FOR_SCRIPT)
		REQUEST_MODEL(LData.eModelName[iID])
		IF HAS_MODEL_LOADED(LData.eModelName[iID])
			// load additiona anims
			SWITCH iID
				CASE PP_WAITRESS RETURN HAS_WAITRESS_ASSETS_LOADED()
				CASE PP_SECURITY RETURN HAS_SECURITY_ASSETS_LOADED()
				CASE PP_SECURITY_BOSS RETURN HAS_SECURITY_BOSS_ASSETS_LOADED()
				CASE PP_RARE_GUARD RETURN HAS_RARE_GUARD_ASSETS_LOADED(LData.iStartLoc[PP_RARE_GUARD])
			ENDSWITCH
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


PROC TASK_HOLD_TRAY(PATROL_PEDS_LOCAL_DATA &LData)

	ANIM_DATA PlayAnim
	ANIM_DATA none
	INT iFlags
	
	//PRINTLN("[casino_patrol] TASK_HOLD_TRAY called ")
	
	IF (IS_ENTITY_PLAYING_ANIM(LData.PedID[PP_WAITRESS], "anim@move_f@waitress", "idle_var_05") = FALSE)
	
		PlayAnim.dictionary0 = "anim@move_f@waitress"
		
		iFlags = 0

		PlayAnim.type = APT_SINGLE_ANIM
		
		iFlags += ENUM_TO_INT(AF_SECONDARY)
		iFlags += ENUM_TO_INT(AF_NOT_INTERRUPTABLE)
		iFlags += ENUM_TO_INT(AF_UPPERBODY)
	
		PlayAnim.anim0 = "idle_var_05"
		iFlags += ENUM_TO_INT(AF_LOOPING)
		PlayAnim.filter =  GET_HASH_KEY("BONEMASK_HEAD_NECK_AND_R_ARM") //GET_HASH_KEY("BONEMASK_ARMONLY_R") 
		//PlayAnim.filter = GET_HASH_KEY("UpperbodyFeathered_filter")
		
		PlayAnim.blendInDelta = INSTANT_BLEND_IN // REALLY_SLOW_BLEND_IN
		PlayAnim.blendOutDelta = INSTANT_BLEND_OUT
		
		PlayAnim.flags = INT_TO_ENUM(ANIMATION_FLAGS, iFlags)
		PlayAnim.rate0 = 1.0
		
		TASK_SCRIPTED_ANIMATION(LData.PedID[PP_WAITRESS], PlayAnim, none, none)
		
		PRINTLN("[casino_patrol] TASK_HOLD_TRAY giving  TASK_SCRIPTED_ANIMATION")
		
	ENDIF

ENDPROC

FUNC BOOL AttachTray(BOOL bWithBucket, PATROL_PEDS_BD_DATA &BData, PED_INDEX PedID)
	
	PRINTLN("[casino_patrol] calling AttachTray")

	BOOL bCreatedDuringCutscene
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(BData.NetObjectID)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(BData.NetObjectID)
			PRINTLN("[casino_patrol] AttachTray - deleting previous tray")
			DELETE_NET_ID(BData.NetObjectID)
		ENDIF	
	ENDIF

	IF NETWORK_IS_IN_MP_CUTSCENE()
		SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
		bCreatedDuringCutscene = TRUE
	ENDIF

	IF CREATE_NET_OBJ(BData.NetObjectID, TrayModel(bWithBucket), GET_PED_BONE_COORDS(PedID, BONETAG_PH_R_HAND, <<0,0,0>>))
		PRINTLN("[casino_patrol] created object")
		OBJECT_INDEX ObjID = NET_TO_OBJ(BData.NetObjectID)
		SET_NETWORK_ID_CAN_MIGRATE(BData.NetObjectID, FALSE)
		ATTACH_ENTITY_TO_ENTITY(ObjID, PedID, GET_PED_BONE_INDEX(PedID, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
		RETURN TRUE
	ENDIF
	
	IF (bCreatedDuringCutscene)
		SET_NETWORK_CUTSCENE_ENTITIES(FALSE)	
	ENDIF
	
	PRINTLN("[casino_patrol] returning FALSE")
	RETURN FALSE
	
ENDFUNC

PROC TASK_HOLD_CASE(PATROL_PEDS_LOCAL_DATA &LData)

	ANIM_DATA PlayAnim
	ANIM_DATA none
	INT iFlags
	
	//PRINTLN("[casino_patrol] TASK_HOLD_TRAY called ")
	
	IF (IS_ENTITY_PLAYING_ANIM(LData.PedID[PP_RARE_GUARD], "random@security_van", "sec_walk_calm") = FALSE)
	
		PlayAnim.dictionary0 = "random@security_van"
		
		iFlags = 0

		PlayAnim.type = APT_SINGLE_ANIM
		
		iFlags += ENUM_TO_INT(AF_SECONDARY)
		iFlags += ENUM_TO_INT(AF_NOT_INTERRUPTABLE)
		iFlags += ENUM_TO_INT(AF_UPPERBODY)
	
		PlayAnim.anim0 = "sec_walk_calm"
		iFlags += ENUM_TO_INT(AF_LOOPING)
		PlayAnim.filter =  GET_HASH_KEY("BONEMASK_HEAD_NECK_AND_R_ARM") 
		
		PlayAnim.blendInDelta = INSTANT_BLEND_IN // REALLY_SLOW_BLEND_IN
		PlayAnim.blendOutDelta = INSTANT_BLEND_OUT
		
		PlayAnim.flags = INT_TO_ENUM(ANIMATION_FLAGS, iFlags)
		PlayAnim.rate0 = 1.0
		
		TASK_SCRIPTED_ANIMATION(LData.PedID[PP_RARE_GUARD], PlayAnim, none, none)
		
		PRINTLN("[casino_patrol] TASK_HOLD_CASE giving  TASK_SCRIPTED_ANIMATION")
		
	ELSE
		IF NETWORK_HAS_CONTROL_OF_ENTITY(LData.PedID[PP_RARE_GUARD])
			IF IS_PED_WALKING(LData.PedID[PP_RARE_GUARD])
				SET_ANIM_RATE(LData.PedID[PP_RARE_GUARD], 1.0, DEFAULT, TRUE)	
			ELSE
				SET_ANIM_RATE(LData.PedID[PP_RARE_GUARD], 0.1, DEFAULT, TRUE)
			ENDIF
		ELSE
			PRINTLN("[casino_patrol] TASK_HOLD_CASE - NETWORK_HAS_CONTROL_OF_ENTITY = FALSE, not setting anim_rate")
		ENDIF
	ENDIF


ENDPROC

FUNC BOOL AttachCase(PATROL_PEDS_BD_DATA &BData, PED_INDEX PedID) //, PATROL_PEDS_LOCAL_DATA &LData)
	
	PRINTLN("[casino_patrol] calling AttachCase")

	BOOL bCreatedDuringCutscene

	IF NETWORK_IS_IN_MP_CUTSCENE()
		SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
		bCreatedDuringCutscene = TRUE
	ENDIF

	IF CREATE_NET_OBJ(BData.NetObjectID, hei_prop_hei_security_case, GET_PED_BONE_COORDS(PedID, BONETAG_PH_R_HAND, <<0,0,0>>))
		PRINTLN("[casino_patrol] created object")
		OBJECT_INDEX ObjID = NET_TO_OBJ(BData.NetObjectID)
		SET_NETWORK_ID_CAN_MIGRATE(BData.NetObjectID, FALSE)
		ATTACH_ENTITY_TO_ENTITY(ObjID, PedID, GET_PED_BONE_INDEX(PedID, BONETAG_PH_R_HAND), <<0.0, 0.0, -0.160>>, <<0,0,0>>, TRUE, TRUE)
		RETURN TRUE
	ENDIF
	
	IF (bCreatedDuringCutscene)
		SET_NETWORK_CUTSCENE_ENTITIES(FALSE)	
	ENDIF
	
	PRINTLN("[casino_patrol] returning FALSE")
	RETURN FALSE
	
ENDFUNC

PROC SET_PED_ALTERNATE_MOVEMENT(PATROL_PEDS_LOCAL_DATA &LData, INT iID)
	IF HAVE_ASSETS_LOADED_FOR_PATROL_PED(LData, iID)
		PRINTLN("[casino_patrol] SET_PED_ALTERNATE_MOVEMENT - setting for ", iID)
		SWITCH iID
			CASE PP_WAITRESS
				SET_PED_ALTERNATE_MOVEMENT_ANIM(LData.PedID[iID], AAT_IDLE, "anim@move_f@waitress", "idle")
				SET_PED_ALTERNATE_MOVEMENT_ANIM(LData.PedID[iID], AAT_WALK, "anim@move_f@waitress", "walk") 			
			BREAK
			CASE PP_SECURITY
			CASE PP_SECURITY_BOSS
				SET_PED_ALTERNATE_MOVEMENT_ANIM(LData.PedID[iID], AAT_IDLE, "anim@move_m@security_guard", "idle")
				SET_PED_ALTERNATE_MOVEMENT_ANIM(LData.PedID[iID], AAT_WALK, "anim@move_m@security_guard", "walk")
			BREAK
			CASE PP_RARE_GUARD
				SET_PED_ALTERNATE_MOVEMENT_ANIM(LData.PedID[iID], AAT_IDLE, "random@security_van", "sec_idle")
				SET_PED_ALTERNATE_MOVEMENT_ANIM(LData.PedID[iID], AAT_WALK, "random@security_van", "sec_walk_calm")			
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL IsRareGuardPatrolling(PATROL_PEDS_LOCAL_DATA &LData)
	IF LData.iStartLoc[PP_RARE_GUARD] = 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SetPatrolPedBehaviours(PED_INDEX PedID)
	SET_ENTITY_CAN_BE_DAMAGED(PedID, FALSE)
	SET_PED_AS_ENEMY(PedID, FALSE)
	SET_CURRENT_PED_WEAPON(PedID, WEAPONTYPE_UNARMED, TRUE)
	SET_ENTITY_INVINCIBLE(PedID, TRUE)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedID, TRUE)
	SET_PED_RESET_FLAG(PedID, PRF_DisablePotentialBlastReactions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromExplosions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
	
	SET_PED_CAN_EVASIVE_DIVE(PedID, FALSE)
	SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(PedID, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PedID, FALSE)
	SET_PED_CAN_RAGDOLL(PedID, FALSE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DisableExplosionReactions, TRUE)	
ENDPROC

PROC CREATE_LOCAL_RARE_GUARD(PATROL_PEDS_LOCAL_DATA &LData)
	IF HAVE_ASSETS_LOADED_FOR_PATROL_PED(LData, PP_RARE_GUARD)
		IF NOT DOES_ENTITY_EXIST(LData.PedID[PP_RARE_GUARD])
			LData.PedID[PP_RARE_GUARD] = CREATE_PED(PEDTYPE_MISSION,  LData.eModelName[PP_RARE_GUARD], LData.vStartPos[PP_RARE_GUARD], LData.fStartHeading[PP_RARE_GUARD], FALSE, FALSE)	
			PRINTLN("[casino_patrol] CREATE_PATROL_PED - created local guard")
			
			SET_PED_PROP_INDEX(LData.PedID[PP_RARE_GUARD], ANCHOR_HEAD, 1)
			
			// attach clipboard
			LData.ObjectID = CREATE_OBJECT(P_CS_CLIPBOARD, GET_PED_BONE_COORDS(LData.PedID[PP_RARE_GUARD], BONETAG_PH_L_HAND, <<0,0,0>>), FALSE, FALSE)
			ATTACH_ENTITY_TO_ENTITY(LData.ObjectID, LData.PedID[PP_RARE_GUARD], GET_PED_BONE_INDEX(LData.PedID[PP_RARE_GUARD], BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0,0,0>>, TRUE, TRUE)
		ENDIF
	ENDIF	
ENDPROC

PROC CREATE_PATROL_PED(PATROL_PEDS_BD_DATA &BData, PATROL_PEDS_LOCAL_DATA &LData, INT iID)

	IF (BData.bRareGuardActive)
		IF IsRareGuardPatrolling(LData)
			IF (iID = PP_WAITRESS)
				PRINTLN("[casino_patrol] CREATE_PATROL_PED - bRareGuardActive - dont create waitress")
				EXIT
			ENDIF
			IF (iID = PP_RARE_GUARD)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(BData.NetPedID[PP_WAITRESS])	
					PRINTLN("[casino_patrol] CREATE_PATROL_PED - bRareGuardActive - waitress still exists")
					EXIT
				ELSE
					HAVE_ASSETS_LOADED_FOR_PATROL_PED(LData, iID)
					IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(LData.vStartPos[iID], 
												DEFAULT, // fVehRadius = 6.0, 
												DEFAULT, // FLOAT fPedRadius = 1.0, 
												DEFAULT, // FLOAT fObjRadius = 1.0,
												DEFAULT, // FLOAT fViewRadius = 5.0,
												DEFAULT, // BOOL bCheckThisPlayerSight = TRUE,
												DEFAULT, // BOOL bDoAnyPlayerSeePointCheck = TRUE,
												DEFAULT, // BOOL bCheckVisibilityForOwnTeam = TRUE,
												60.0, // FLOAT fVisibleDistance = 120.0,
												DEFAULT, // BOOL bIgnoreLocalPlayerChecks=FALSE, 
												DEFAULT, // INT iTeamToIgnore=-1,
												DEFAULT, // BOOL bDoFireCheck=TRUE,
												10.0, // FLOAT fPlayerRadius = 0.0,
												DEFAULT, // BOOL bIgnoreTeammatesForExclusionRadius=FALSE,
												DEFAULT, // FLOAT fTeammateVisibleRange=0.0,
												DEFAULT, // BOOL bConsiderVisiblePlayersOnly=FALSE,
												DEFAULT // BOOL bConsiderScriptCreatedObjectsOnly=FALSE
												)
						PRINTLN("[casino_patrol] CREATE_PATROL_PED - rare guard not safe to create rare guard")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[casino_patrol] CREATE_PATROL_PED - create local guard")
			EXIT
		ENDIF
	ELSE
		IF (iID = PP_RARE_GUARD)
			PRINTLN("[casino_patrol] CREATE_PATROL_PED - not time to create rare guard")
			EXIT
		ENDIF		
	ENDIF
	

	PRINTLN("[casino_patrol] CREATE_PATROL_PED - called with iID = ", iID)
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(BData.NetPedID[iID])		
		IF HAVE_ASSETS_LOADED_FOR_PATROL_PED(LData, iID)					
			IF CREATE_NET_PED(BData.NetPedID[iID], PEDTYPE_MISSION, LData.eModelName[iID], LData.vStartPos[iID], LData.fStartHeading[iID])
				
				SET_NETWORK_ID_CAN_MIGRATE(BData.NetPedID[iID], FALSE)
				
				LData.PedID[iID] = NET_TO_PED(BData.NetPedID[iID])	
							
				SetPatrolPedBehaviours(LData.PedID[iID])
				
				IF (iID = PP_WAITRESS)
					SET_PED_ALTERNATE_MOVEMENT(LData, iId)
					TASK_HOLD_TRAY(LData)

					IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(BData.NetObjectID)		
						
						IF (LData.iStartLoc[0] = 0)
							IF AttachTray(TRUE, BData, LData.PedID[iID])
								BData.iState[iID] = 3 // start at the management end // 2 // start in lift 2
							ENDIF
						ELSE
							IF AttachTray(FALSE, BData, LData.PedID[iID])
								BData.iState[iID] = 1 // start at the entrance end // 0 // start in lift 1
							ENDIF
						ENDIF						
						
					ENDIF	
					
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_HEAD,5,1,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_BERD,0,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_HAIR,5,1,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_TORSO,2,2,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_LEG,1,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_HAND,0,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_FEET,0,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_TEETH,0,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_SPECIAL,2,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_SPECIAL2,0,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_DECL,0,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_JBIB,0,0,0)					
					
					SET_PED_PROP_INDEX(LData.PedID[iID], ANCHOR_EYES, 0)
					
					SET_PED_VOICE_GROUP(LData.PedID[iID], GET_HASH_KEY("A_F_Y_SOUCENT_02_BLACK_FULL_01"))

 				ENDIF			
				
				IF (iID = PP_SECURITY)
				
					SET_PED_ALTERNATE_MOVEMENT(LData, iId)
 										
					GET_SECURITY_GUARD_PED_DATA(g_eSecurityGuardPeds[ciCASINO_PATROL_SECURITY_GUARD_ID], LData.PedID[iID])								
					GET_SECURITY_GUARD_PED_VOICE_GROUP(g_eSecurityGuardPeds[ciCASINO_PATROL_SECURITY_GUARD_ID], LData.PedID[iID])
					
					// store guard ped id: url:bugstar:6065906 - Scope Out - Can I please have a way of grabbing a hold of either the ped indexes or the coords of the security guards in the casino?
					g_CasinoSecurityGuardPedId[5] = LData.PedID[iID]
					
 				ENDIF				
				
				IF (iID = PP_SECURITY_BOSS)
				
					SET_PED_ALTERNATE_MOVEMENT(LData, iId)
					
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_HEAD,0,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_BERD,0,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_HAIR,0,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_TORSO,0,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_LEG,0,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_HAND,0,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_FEET,0,0,1)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_TEETH,0,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_SPECIAL,0,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_SPECIAL2,0,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_DECL,0,0,0)
					SET_PED_COMPONENT_VARIATION(LData.PedID[iID], PED_COMP_JBIB,0,0,0)	
					
					SET_PED_VOICE_GROUP(LData.PedID[iID], GET_HASH_KEY("CAS_SECURITY_MANAGER"))
					
					// store guard ped id: url:bugstar:6065906 - Scope Out - Can I please have a way of grabbing a hold of either the ped indexes or the coords of the security guards in the casino?
					g_CasinoSecurityGuardPedId[6] = LData.PedID[iID]
					
				ENDIF				
				
				
				IF (iID = PP_RARE_GUARD)
		
					SET_PED_ALTERNATE_MOVEMENT(LData, iId)
					TASK_HOLD_CASE(LData)

					SET_PED_PROP_INDEX(LData.PedID[iID], ANCHOR_HEAD, 1)
					
					IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(BData.NetObjectID)		
						
						AttachCase(BData, LData.PedID[iID])						
						
					ENDIF						
					
				ENDIF		
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DELETE_PATROL_PED(PATROL_PEDS_BD_DATA &BData, PATROL_PEDS_LOCAL_DATA &LData, INT iID)
	IF DOES_ENTITY_EXIST(LData.PedID[iID])
			
		// request control if needed
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(LData.PedID[iID])
			NETWORK_REQUEST_CONTROL_OF_ENTITY(LData.PedID[iID])
		ENDIF
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(LData.ObjectID)
			NETWORK_REQUEST_CONTROL_OF_ENTITY(LData.ObjectID)
		ENDIF
		IF NETWORK_HAS_CONTROL_OF_ENTITY(LData.PedID[iID])
			DELETE_PED(LData.PedID[iID])
		ENDIF
		
		IF (iID = PP_WAITRESS)
			UNLOAD_WAITRESS_ASSETS()
			DELETE_NET_ID(BData.NetObjectID)
		ENDIF
		
		IF (iID = PP_RARE_GUARD)
			UNLOAD_RARE_GUARD_ASSETS()
			DELETE_NET_ID(BData.NetObjectID)	
		ENDIF
		
	ENDIF
ENDPROC

PROC ChangeTray(PATROL_PEDS_BD_DATA &BData, PATROL_PEDS_LOCAL_DATA &LData)
	// change bucket when inside lift
	INT iProgress = GET_SEQUENCE_PROGRESS(LData.PedID[PP_WAITRESS])
	
	PRINTLN("[casino_patrol] ChangeTray - iprogress = ", iProgress)	
	
	IF iProgress = 1
		LData.iTrayTimer = GET_GAME_TIMER()
	ENDIF
	
	IF iProgress = 2
						
		IF (BData.bSwitchedTray = FALSE)
	
			IF ((GET_GAME_TIMER() - LData.iTrayTimer) > 3000)
				
				// should we delete?
				IF (BData.bRareGuardActive)
					DELETE_PATROL_PED(BData, LData, PP_WAITRESS)
				ELSE
					
					IF DoesTrayHaveBucket(LData.ObjectID)
						AttachTray(FALSE, BData, LData.PedID[PP_WAITRESS])
					ELSE
						AttachTray(TRUE, BData, LData.PedID[PP_WAITRESS])
					ENDIF						
					
					BData.bSwitchedTray = TRUE 		
				
				ENDIF
			ENDIF
		ENDIF
	ENDIF			
			
ENDPROC



PROC UPDATE_WAITRESS(PATROL_PEDS_BD_DATA &BData, PATROL_PEDS_LOCAL_DATA &LData)

	SCRIPTTASKSTATUS TaskStatus 


	INT i
	REPEAT 2 i
		IF DOES_ENTITY_EXIST(LData.PedID[PP_WAITRESS])
		AND DOES_ENTITY_EXIST(g_CasinoElevatorDoor[i])
			SET_ENTITY_NO_COLLISION_ENTITY(LData.PedID[PP_WAITRESS], g_CasinoElevatorDoor[i], FALSE) 
		ENDIF
	ENDREPEAT
	
	PRINTLN("[casino_patrol] BData.iState[PP_WAITRESS] = ", BData.iState[PP_WAITRESS])
	TaskStatus = GET_SCRIPT_TASK_STATUS(LData.PedID[PP_WAITRESS], SCRIPT_TASK_PERFORM_SEQUENCE)
	PRINTLN("[casino_patrol] UPDATE_WAITRESS, TaskStatus = ", ENUM_TO_INT(TaskStatus))
	
	VECTOR vPos
	
	SWITCH BData.iState[PP_WAITRESS]
	
		// give task to enter lift
		CASE 0
			IF (TaskStatus = FINISHED_TASK)
				TASK_PERFORM_SEQUENCE(LData.PedID[PP_WAITRESS], LData.seqWaitressEnterLift1)
				BData.iState[PP_WAITRESS]++
				BData.bSwitchedTray = FALSE
			ELSE
				// waiting to arrive at lift
				vPos = GET_ENTITY_COORDS(LData.PedID[PP_WAITRESS], FALSE)
				IF (vPos.z > 0.0)
					vPos.z = -52.0
					SET_ENTITY_COORDS(LData.PedID[PP_WAITRESS], vPos, FALSE, TRUE)
					PRINTLN("[casino_patrol] UPDATE_WAITRESS resetting coords to  ", vPos)
				ENDIF
			ENDIF
		BREAK
		
		// walk through casino route 1
		CASE 1
			IF (TaskStatus = FINISHED_TASK)
				TASK_PERFORM_SEQUENCE(LData.PedID[PP_WAITRESS], LData.seqWaitressRoute1)
				BData.iState[PP_WAITRESS]++
			ELSE
				// waiting to leave lift
				ChangeTray(BData, LData)
			ENDIF		
		BREAK
		
		// enter lift 2
		CASE 2
			IF (TaskStatus = FINISHED_TASK)
				TASK_PERFORM_SEQUENCE(LData.PedID[PP_WAITRESS], LData.seqWaitressEnterLift2)
				BData.iState[PP_WAITRESS]++
				BData.bSwitchedTray = FALSE
			ELSE
				// waiting to finish route 1
			ENDIF				
		BREAK
		
		// route 2
		CASE 3
			IF (TaskStatus = FINISHED_TASK)
				TASK_PERFORM_SEQUENCE(LData.PedID[PP_WAITRESS], LData.seqWaitressRoute2)
				BData.iState[PP_WAITRESS] = 0
			ELSE
				// waiting to exit lift
				ChangeTray(BData, LData)
			ENDIF				
		BREAK		
		
	ENDSWITCH
	

ENDPROC

PROC UPDATE_RARE_GUARD(PATROL_PEDS_BD_DATA &BData, PATROL_PEDS_LOCAL_DATA &LData)

	//EXIT

	SCRIPTTASKSTATUS TaskStatus 

	INT i
	
	IF LData.iStartLoc[PP_RARE_GUARD] = 0
	
		REPEAT 2 i
			IF DOES_ENTITY_EXIST(LData.PedID[PP_RARE_GUARD])
			AND DOES_ENTITY_EXIST(g_CasinoElevatorDoor[i])
				SET_ENTITY_NO_COLLISION_ENTITY(LData.PedID[PP_RARE_GUARD], g_CasinoElevatorDoor[i], FALSE) 
			ENDIF
		ENDREPEAT
		
		PRINTLN("[casino_patrol] BData.iState[PP_RARE_GUARD] - walk route = ", BData.iState[PP_RARE_GUARD])
		
		SWITCH BData.iState[PP_RARE_GUARD]
		
			// walk through casino
			CASE 0
				TaskStatus = GET_SCRIPT_TASK_STATUS(LData.PedID[PP_RARE_GUARD], SCRIPT_TASK_PERFORM_SEQUENCE)
				IF (TaskStatus = FINISHED_TASK)
					TASK_PERFORM_SEQUENCE(LData.PedID[PP_RARE_GUARD], LData.seqRareGuardRoute1)
					BData.iState[PP_RARE_GUARD]++
				ENDIF
			BREAK
			
			// enter lift 2
			CASE 1
				TaskStatus = GET_SCRIPT_TASK_STATUS(LData.PedID[PP_RARE_GUARD], SCRIPT_TASK_PERFORM_SEQUENCE)			
				IF (TaskStatus = FINISHED_TASK)
					TASK_PERFORM_SEQUENCE(LData.PedID[PP_RARE_GUARD], LData.seqRareGuardEnterLift2)
					BData.iState[PP_RARE_GUARD]++
				ELSE			
					// waiting to get to lift	
				ENDIF		
			BREAK
			
			// close door
			CASE 2
				TaskStatus = GET_SCRIPT_TASK_STATUS(LData.PedID[PP_RARE_GUARD], SCRIPT_TASK_PERFORM_SEQUENCE)
				IF (TaskStatus = FINISHED_TASK)
					TASK_STAND_STILL(LData.PedID[PP_RARE_GUARD], 5000)
					BData.iState[PP_RARE_GUARD]++
				ELSE
					// waiting to finish entering
				ENDIF				
			BREAK	
			
			// delete
			CASE 3
				TaskStatus = GET_SCRIPT_TASK_STATUS(LData.PedID[PP_RARE_GUARD], SCRIPT_TASK_STAND_STILL)
				IF (TaskStatus = FINISHED_TASK)
					DELETE_PATROL_PED(BData, LData, PP_RARE_GUARD)
					BData.bRareGuardActive = FALSE
				ELSE
					// waiting to finish entering
				ENDIF				
			BREAK		
			
		ENDSWITCH

	ENDIF

ENDPROC

PROC TASK_SECURITY_BOSS_IDLE(PED_INDEX PedId, PATROL_PEDS_LOCAL_DATA &LData)

	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 5)
	
	SWITCH iRand
		CASE 0 
			TASK_PERFORM_SEQUENCE(PedId, LData.seqBossIdle0)
		BREAK
		CASE 1 
			TASK_PERFORM_SEQUENCE(PedId, LData.seqBossIdle1)
		BREAK
		CASE 2
			TASK_PERFORM_SEQUENCE(PedId, LData.seqBossIdle2)
		BREAK
		CASE 3 
			TASK_PERFORM_SEQUENCE(PedId, LData.seqBossIdle3)
		BREAK
		CASE 4 
			TASK_PERFORM_SEQUENCE(PedId, LData.seqBossIdle4)
		BREAK
	ENDSWITCH

ENDPROC

PROC TASK_SECURITY_FIDGET(PED_INDEX &PedID)

	ANIM_DATA PlayAnim
	ANIM_DATA none
	INT iFlags
	
	PlayAnim.dictionary0 = "anim@move_m@security_guard"
	
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
	SWITCH iRand
		CASE 0
			PlayAnim.anim0 = "walk_var_01"
		BREAK
		CASE 1
			PlayAnim.anim0 = "walk_var_02"
		BREAK
		CASE 2
			PlayAnim.anim0 = "walk_var_03"
		BREAK
		CASE 3
			PlayAnim.anim0 = "walk_var_04"
		BREAK
	ENDSWITCH
	
	
	PlayAnim.type = APT_SINGLE_ANIM
	
	iFlags = 0
	iFlags += ENUM_TO_INT(AF_SECONDARY)
	iFlags += ENUM_TO_INT(AF_NOT_INTERRUPTABLE)
	iFlags += ENUM_TO_INT(AF_UPPERBODY)
	//iFlags += ENUM_TO_INT(AF_TAG_SYNC_CONTINUOUS)	
	
	PlayAnim.filter =  GET_HASH_KEY("UpperbodyFeathered_filter")
	
	PlayAnim.blendInDelta = WALK_BLEND_IN // NORMAL_BLEND_IN
	PlayAnim.blendOutDelta = WALK_BLEND_OUT // NORMAL_BLEND_OUT
	
	PlayAnim.flags = INT_TO_ENUM(ANIMATION_FLAGS, iFlags)
	PlayAnim.rate0 = 1.0
	
	TASK_SCRIPTED_ANIMATION(PedID, PlayAnim, none, none)
	PRINTLN("[casino_patrol] TASK_SECURITY_FIDGET giving anim ", PlayAnim.anim0) 

ENDPROC

PROC UPDATE_SECURITY(PATROL_PEDS_BD_DATA &BData, PATROL_PEDS_LOCAL_DATA &LData)


	//INT i
	SCRIPTTASKSTATUS TaskStatus = GET_SCRIPT_TASK_STATUS(LData.PedID[PP_SECURITY], SCRIPT_TASK_PERFORM_SEQUENCE)
	
	IF (TaskStatus = FINISHED_TASK)
		
		SWITCH BData.iState[PP_SECURITY]
			CASE 0 
				TASK_PERFORM_SEQUENCE(LData.PedID[PP_SECURITY], LData.seqSecurityRoute0)
				BData.iState[PP_SECURITY]++
			BREAK
			CASE 1
				TASK_PERFORM_SEQUENCE(LData.PedID[PP_SECURITY], LData.seqGuardIdle)
				BData.iState[PP_SECURITY]++
			BREAK
			CASE 2
				TASK_PERFORM_SEQUENCE(LData.PedID[PP_SECURITY], LData.seqSecurityRoute1)
				BData.iState[PP_SECURITY]++
			BREAK
			CASE 3
				TASK_PERFORM_SEQUENCE(LData.PedID[PP_SECURITY], LData.seqGuardIdle)
				BData.iState[PP_SECURITY]++
			BREAK
			CASE 4
				TASK_PERFORM_SEQUENCE(LData.PedID[PP_SECURITY], LData.seqSecurityRoute2)
				BData.iState[PP_SECURITY]++
			BREAK
			CASE 5
				TASK_SECURITY_BOSS_IDLE(LData.PedID[PP_SECURITY], LData)
				BData.iState[PP_SECURITY]++
			BREAK
			CASE 6
				TASK_PERFORM_SEQUENCE(LData.PedID[PP_SECURITY], LData.seqSecurityRoute3)
				BData.iState[PP_SECURITY]=0
			BREAK
		ENDSWITCH			

	
	ENDIF
			
ENDPROC



PROC UPDATE_FIDGET_FOR_SECURITY_BOSS(PATROL_PEDS_BD_DATA &BData, PATROL_PEDS_LOCAL_DATA &LData)
	// give random fidgets while walking
	SWITCH BData.iState[PP_SECURITY_BOSS]
		CASE 0
		CASE 1
		CASE 3
		
			IF (GET_GAME_TIMER() - LData.iWalkTimer[PP_SECURITY_BOSS] > 20000)
			
				TASK_SECURITY_FIDGET(LData.PedID[PP_SECURITY_BOSS])								
				LData.iWalkTimer[PP_SECURITY_BOSS] = GET_GAME_TIMER()
			ENDIF
		BREAK
		DEFAULT
			LData.iWalkTimer[PP_SECURITY_BOSS] = GET_GAME_TIMER() - 5000
		BREAK
	ENDSWITCH	
ENDPROC

PROC UPDATE_SECURITY_BOSS(PATROL_PEDS_BD_DATA &BData, PATROL_PEDS_LOCAL_DATA &LData)

	SCRIPTTASKSTATUS TaskStatus = GET_SCRIPT_TASK_STATUS(LData.PedID[PP_SECURITY_BOSS], SCRIPT_TASK_PERFORM_SEQUENCE)
	
	IF (TaskStatus = FINISHED_TASK)
		
		SWITCH BData.iState[PP_SECURITY_BOSS]
			CASE 0 
				TASK_PERFORM_SEQUENCE(LData.PedID[PP_SECURITY_BOSS], LData.seqBossRoute0)
				BData.iState[PP_SECURITY_BOSS]++
			BREAK
			CASE 1
				TASK_SECURITY_BOSS_IDLE(LData.PedID[PP_SECURITY_BOSS], LData)
				BData.iState[PP_SECURITY_BOSS]++
			BREAK
			CASE 2
				TASK_PERFORM_SEQUENCE(LData.PedID[PP_SECURITY_BOSS], LData.seqBossRoute1)
				BData.iState[PP_SECURITY_BOSS]++
			BREAK
			CASE 3
				TASK_SECURITY_BOSS_IDLE(LData.PedID[PP_SECURITY_BOSS], LData)
				BData.iState[PP_SECURITY_BOSS]++
			BREAK
			CASE 4
				TASK_PERFORM_SEQUENCE(LData.PedID[PP_SECURITY_BOSS], LData.seqBossRoute2)
				BData.iState[PP_SECURITY_BOSS] = 0
			BREAK
		ENDSWITCH			
		
	ELSE
	
//		// give random fidgets while walking
//		SWITCH BData.iState[PP_SECURITY_BOSS]
//			CASE 0
//			CASE 1
//			CASE 3
//				IF (GET_GAME_TIMER() - LData.iWalkTimer[PP_SECURITY_BOSS] > 10000)
//					TASK_SECURITY_FIDGET(LData.PedID[PP_SECURITY_BOSS])								
//					LData.iWalkTimer[PP_SECURITY_BOSS] = GET_GAME_TIMER()
//				ENDIF
//			BREAK
//			DEFAULT
//				LData.iWalkTimer[PP_SECURITY_BOSS] = GET_GAME_TIMER() - 5000
//			BREAK
//		ENDSWITCH	
	
	ENDIF	
			
ENDPROC


PROC UPDATE_PATROL_PED(PATROL_PEDS_BD_DATA &BData, PATROL_PEDS_LOCAL_DATA &LData, INT iID)

	IF DOES_ENTITY_EXIST(LData.PedID[iID])
	AND NOT IS_ENTITY_DEAD(LData.PedID[iID])
		
		IF HAVE_ASSETS_LOADED_FOR_PATROL_PED(LData, iID) // double check to make sure assets are loaded.
		
			// request control if needed
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(LData.PedID[iID])
				NETWORK_REQUEST_CONTROL_OF_ENTITY(LData.PedID[iID])		
			ENDIF
			IF DOES_ENTITY_EXIST(LData.ObjectID)
				IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(LData.ObjectID)
					NETWORK_REQUEST_CONTROL_OF_ENTITY(LData.ObjectID)		
				ENDIF		
			ENDIF
		
			IF NETWORK_HAS_CONTROL_OF_ENTITY(LData.PedID[iID])
						
				SET_NETWORK_ID_CAN_MIGRATE(BData.NetPedID[iID], FALSE)
				
				SWITCH iID
					CASE PP_WAITRESS 	
						UPDATE_WAITRESS(BData, LData)	
					BREAK
					CASE PP_SECURITY 
						UPDATE_SECURITY(BData, LData)
					BREAK
					CASE PP_SECURITY_BOSS		
						UPDATE_SECURITY_BOSS(BData, LData)
					BREAK
					CASE PP_RARE_GUARD
						UPDATE_RARE_GUARD(BData, LData)	
					BREAK
				ENDSWITCH
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC CREATE_PATROL_PED_WIDGET(PATROL_PEDS_BD_DATA &BData, PATROL_PEDS_LOCAL_DATA &LData)
	INT i
	TEXT_LABEL_63 str
	INT iRoute
	INT iPoint
	PRINTLN("[casino_patrol] Casino Patrol Peds widget creating")
	START_WIDGET_GROUP("Casino Patrol Peds")
		START_WIDGET_GROUP("LData")
			ADD_WIDGET_BOOL("bBlipPeds", LData.bBlipPeds)
			ADD_WIDGET_INT_SLIDER("iLiftState", LData.iLiftState, -1, 99, 1)		
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Rare Guard")
			ADD_WIDGET_INT_SLIDER("g_sMPtunables.VC_Penthouse_Movie_Interval", g_sMPtunables.VC_Penthouse_Movie_Interval, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("g_sMPtunables.VC_Penthouse_Movie_Window", g_sMPtunables.VC_Penthouse_Movie_Window, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("g_sMPtunables.VC_Penthouse_Movie_Variation", g_sMPtunables.VC_Penthouse_Movie_Variation, -1, 1, 1)
			ADD_WIDGET_INT_READ_ONLY("iTimeUntilNextWindow", LData.iTimeUntilNextWindow)
			ADD_WIDGET_INT_READ_ONLY("g_iTimeLastTriggeredRareGuard", g_iTimeLastTriggeredRareGuard)
			ADD_WIDGET_BOOL("BData.bRareGuardActive", BData.bRareGuardActive)
			ADD_WIDGET_BOOL("BData.bRareGuardHasBeenActive", BData.bRareGuardHasBeenActive)
			ADD_WIDGET_INT_SLIDER("BData.iRareGuardVariation", BData.iRareGuardVariation, 0, 1, 1)
			ADD_WIDGET_BOOL("bDeleteWaitress", LData.bDeleteWaitress)
			//ADD_WIDGET_VECTOR_SLIDER("vOffset", LData.vOffset, -10.0, 10.0, 0.01)
			//ADD_WIDGET_VECTOR_SLIDER("vRotation", LData.vRotation, -9999.9, 9999.9, 0.01)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("BData")
			ADD_WIDGET_BOOL("bRareGuardActive", BData.bRareGuardActive)
			ADD_WIDGET_INT_SLIDER("iRareGuardVariation", BData.iRareGuardVariation, 0, 1, 1)
			REPEAT NUMBER_OF_CASINO_PATROL_PEDS i
				str = "iState["
				str += i
				str += "]"
				ADD_WIDGET_INT_SLIDER(str, BData.iState[i], -1, 99, 1)
			ENDREPEAT
			ADD_WIDGET_BOOL("bSwitchedTray", BData.bSwitchedTray)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("nav points")
			ADD_WIDGET_BOOL("bShowRoutes", LData.bShowRoutes)		
			ADD_WIDGET_BOOL("bOutputToFile", LData.bOutputToFile)
			START_WIDGET_GROUP("waitress")
				REPEAT NUM_NAV_ROUTES iRoute			
					REPEAT NUM_NAV_POINTS iPoint
						str = "["
						str += iRoute
						str += "]["
						str += iPoint
						str += "]"
						ADD_WIDGET_VECTOR_SLIDER(str, LData.vWaitressNavPoints[iRoute][iPoint], -9999.9, 9999.9, 0.01)
					ENDREPEAT
				ENDREPEAT
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("security")
				REPEAT NUM_NAV_ROUTES iRoute			
					REPEAT NUM_NAV_POINTS iPoint
						str = "["
						str += iRoute
						str += "]["
						str += iPoint
						str += "]"
						ADD_WIDGET_VECTOR_SLIDER(str, LData.vSecurityNavPoints[iRoute][iPoint], -9999.9, 9999.9, 0.01)
					ENDREPEAT
				ENDREPEAT
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("boss")
				REPEAT NUM_NAV_ROUTES iRoute			
					REPEAT NUM_NAV_POINTS iPoint
						str = "["
						str += iRoute
						str += "]["
						str += iPoint
						str += "]"
						ADD_WIDGET_VECTOR_SLIDER(str, LData.vBossNavPoints[iRoute][iPoint], -9999.9, 9999.9, 0.01)
					ENDREPEAT
				ENDREPEAT
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

PROC UPDATE_WIDGETS(PATROL_PEDS_BD_DATA &BData, PATROL_PEDS_LOCAL_DATA &LData)
	
	UNUSED_PARAMETER(BData)
	INT i

	IF LData.bBlipPeds
		REPEAT NUMBER_OF_CASINO_PATROL_PEDS i	
			IF DOES_ENTITY_EXIST(LData.PedID[i])
				IF NOT DOES_BLIP_EXIST(LData.BlipId[i])
					LData.BlipId[i] = ADD_BLIP_FOR_ENTITY(LData.PedID[i])
					SHOW_NUMBER_ON_BLIP(LData.BlipId[i], i)
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		REPEAT NUMBER_OF_CASINO_PATROL_PEDS i	
			IF DOES_ENTITY_EXIST(LData.PedID[i])
				IF DOES_BLIP_EXIST(LData.BlipId[i])
					REMOVE_BLIP(LData.BlipId[i])
				ENDIF
			ENDIF
		ENDREPEAT		
	ENDIF
	
	IF (LData.bDeleteWaitress)
		DELETE_PATROL_PED(BData, LData, PP_WAITRESS)
		LData.bDeleteWaitress = FALSE
	ENDIF
	
	IF LData.bShowRoutes
		DrawNavPoints(LData)
	ENDIF
	
	IF LData.bOutputToFile
		OutputNavPoints(LData)
		LData.bOutputToFile = FALSE
	ENDIF	

ENDPROC

#ENDIF


PROC INIT_DATA(PATROL_PEDS_LOCAL_DATA &LData)
	
	
	IF NOT (LData.bInitialised)
	
		#IF IS_DEBUG_BUILD
			FillNavPoints(LData)
		#ENDIF
		
		NAVDATA navInfo
		navInfo.m_fMaxSlopeNavigable = 10.0
		
		OPEN_SEQUENCE_TASK(LData.seqWaitressEnterLift1)
			TASK_STAND_STILL(NULL, 2000)
			TASK_GO_STRAIGHT_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,0,0),  PEDMOVEBLENDRATIO_WALK, 5000, 305.6856)
			TASK_STAND_STILL(NULL, 20000)
			TASK_GO_STRAIGHT_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,0,1),  PEDMOVEBLENDRATIO_WALK, 5000, 305.6856)
		CLOSE_SEQUENCE_TASK(LData.seqWaitressEnterLift1)

		OPEN_SEQUENCE_TASK(LData.seqWaitressRoute1)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,1,0), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,1,1), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING) 
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,1,2), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING) 
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,1,3), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,1,4), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,1,5), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,1,6), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING) 	
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,1,7), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING) 	
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,1,8), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			navInfo.m_fSlideToCoordHeading = 0.0
			TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,1,9), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_ADV_SLIDE_TO_COORD_AND_ACHIEVE_HEADING_AT_END, navInfo)			
		CLOSE_SEQUENCE_TASK(LData.seqWaitressRoute1)
		
		OPEN_SEQUENCE_TASK(LData.seqWaitressEnterLift2)
			TASK_STAND_STILL(NULL, 2000)
			TASK_GO_STRAIGHT_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,2,0),  PEDMOVEBLENDRATIO_WALK, 5000, 180.0)
			TASK_STAND_STILL(NULL, 20000)
			TASK_GO_STRAIGHT_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,2,1),  PEDMOVEBLENDRATIO_WALK, 5000, 180.0)
		CLOSE_SEQUENCE_TASK(LData.seqWaitressEnterLift2)
		
		
		OPEN_SEQUENCE_TASK(LData.seqWaitressRoute2)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,3,0), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING) 
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,3,1), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING) 	
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,3,2), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,3,3), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,3,4), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,3,5), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,3,6), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,3,7), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,3,8), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			navInfo.m_fSlideToCoordHeading = 132.5267
			TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED(NULL, GET_NAV_POINT_FOR_PED(PP_WAITRESS,3,9), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_ADV_SLIDE_TO_COORD_AND_ACHIEVE_HEADING_AT_END, navInfo)
		CLOSE_SEQUENCE_TASK(LData.seqWaitressRoute2)
			
		OPEN_SEQUENCE_TASK(LData.seqSecurityRoute0)
			navInfo.m_fSlideToCoordHeading = 125.6788
			TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,0,0), PEDMOVEBLENDRATIO_WALK , -1, 0.3, ENAV_ADV_SLIDE_TO_COORD_AND_ACHIEVE_HEADING_AT_END, navInfo)		
		CLOSE_SEQUENCE_TASK(LData.seqSecurityRoute0)
		
		OPEN_SEQUENCE_TASK(LData.seqSecurityRoute1)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,1,0), PEDMOVEBLENDRATIO_WALK , -1, 0.5)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,1,1), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,1,2), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,1,3), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			//navInfo.m_fSlideToCoordHeading = 164.5949
			//TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,1,4), PEDMOVEBLENDRATIO_WALK , -1, 0.3, ENAV_ADV_SLIDE_TO_COORD_AND_ACHIEVE_HEADING_AT_END, navInfo) 
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,1,4), PEDMOVEBLENDRATIO_WALK , -1)
			TASK_ACHIEVE_HEADING(NULL, 164.5949) 
		CLOSE_SEQUENCE_TASK(LData.seqSecurityRoute1)
		
		OPEN_SEQUENCE_TASK(LData.seqSecurityRoute2)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,2,0), PEDMOVEBLENDRATIO_WALK , -1, 0.5) 
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,2,1), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING) 
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,2,2), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING) 
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,2,3), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)				
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,2,4), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
				//navInfo.m_fSlideToCoordHeading = 106.4715
				//TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,2,5), PEDMOVEBLENDRATIO_WALK , -1, 0.3, ENAV_ADV_SLIDE_TO_COORD_AND_ACHIEVE_HEADING_AT_END, navInfo) 
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,2,5), PEDMOVEBLENDRATIO_WALK , -1)
				TASK_ACHIEVE_HEADING(NULL, 106.4715) 
		CLOSE_SEQUENCE_TASK(LData.seqSecurityRoute2)		
		
		OPEN_SEQUENCE_TASK(LData.seqSecurityRoute3)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,3,0), PEDMOVEBLENDRATIO_WALK , -1, 0.5)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,3,1), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,3,2), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY,3,3), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
		CLOSE_SEQUENCE_TASK(LData.seqSecurityRoute3)		
		
		OPEN_SEQUENCE_TASK(LData.seqBossRoute0)
			//navInfo.m_fSlideToCoordHeading = 347.0402
			//TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY_BOSS,0,0), PEDMOVEBLENDRATIO_WALK , -1, 0.3, ENAV_ADV_SLIDE_TO_COORD_AND_ACHIEVE_HEADING_AT_END, navInfo)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY_BOSS,0,0), PEDMOVEBLENDRATIO_WALK , -1)
			TASK_ACHIEVE_HEADING(NULL, 347.0402) 
		CLOSE_SEQUENCE_TASK(LData.seqBossRoute0)	
	
	
		OPEN_SEQUENCE_TASK(LData.seqBossRoute1)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY_BOSS,1,0), PEDMOVEBLENDRATIO_WALK , -1, 0.5)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY_BOSS,1,1), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
				//navInfo.m_fSlideToCoordHeading = 44.3343
				//TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY_BOSS,1,2), PEDMOVEBLENDRATIO_WALK , -1, 0.3, ENAV_ADV_SLIDE_TO_COORD_AND_ACHIEVE_HEADING_AT_END, navInfo) 
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY_BOSS,1,2), PEDMOVEBLENDRATIO_WALK , -1)
				TASK_ACHIEVE_HEADING(NULL, 44.3343) 
		CLOSE_SEQUENCE_TASK(LData.seqBossRoute1)
		
		OPEN_SEQUENCE_TASK(LData.seqBossRoute2)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY_BOSS,2,0), PEDMOVEBLENDRATIO_WALK , -1, 0.5)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY_BOSS,2,1), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY_BOSS,2,2), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY_BOSS,2,3), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY_BOSS,2,4), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY_BOSS,2,5), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY_BOSS,2,6), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY_BOSS,2,7), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_SECURITY_BOSS,2,8), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
		CLOSE_SEQUENCE_TASK(LData.seqBossRoute2)		
		
		OPEN_SEQUENCE_TASK(LData.seqRareGuardRoute1)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_RARE_GUARD,0,0), PEDMOVEBLENDRATIO_WALK , -1, 0.5)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_RARE_GUARD,0,1), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING) 
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_RARE_GUARD,0,2), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING) 
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_RARE_GUARD,0,3), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_RARE_GUARD,0,4), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_RARE_GUARD,0,5), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_RARE_GUARD,0,6), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING) 	
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_RARE_GUARD,0,7), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING) 	
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_RARE_GUARD,0,8), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_NO_STOPPING)
			navInfo.m_fSlideToCoordHeading = 0.0
			TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED(NULL, GET_NAV_POINT_FOR_PED(PP_RARE_GUARD,0,9), PEDMOVEBLENDRATIO_WALK , -1, 0.5, ENAV_ADV_SLIDE_TO_COORD_AND_ACHIEVE_HEADING_AT_END, navInfo)			
		CLOSE_SEQUENCE_TASK(LData.seqRareGuardRoute1)
			
		
		OPEN_SEQUENCE_TASK(LData.seqRareGuardEnterLift2)
			TASK_STAND_STILL(NULL, 2000)
			TASK_GO_STRAIGHT_TO_COORD(NULL, GET_NAV_POINT_FOR_PED(PP_RARE_GUARD,1,0),  PEDMOVEBLENDRATIO_WALK, 5000, 180.0)
			TASK_STAND_STILL(NULL, 5000)
		CLOSE_SEQUENCE_TASK(LData.seqRareGuardEnterLift2)		
		
		OPEN_SEQUENCE_TASK(LData.seqGuardIdle)
			TASK_PLAY_ANIM(NULL, "amb@world_human_stand_guard@male@enter",  "enter", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "amb@world_human_stand_guard@male@base",  "base", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "amb@world_human_stand_guard@male@idle_a",  "idle_a", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "amb@world_human_stand_guard@male@idle_b",  "idle_d", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "amb@world_human_stand_guard@male@exit",  "exit", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
		CLOSE_SEQUENCE_TASK(LData.seqGuardIdle)
		
		OPEN_SEQUENCE_TASK(LData.seqBossIdle0)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle_var_01", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle_var_02", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
		CLOSE_SEQUENCE_TASK(LData.seqBossIdle0)		
		
		OPEN_SEQUENCE_TASK(LData.seqBossIdle1)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle_var_02", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle_var_03", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
		CLOSE_SEQUENCE_TASK(LData.seqBossIdle1)		
		
		OPEN_SEQUENCE_TASK(LData.seqBossIdle2)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle_var_03", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle_var_04", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
		CLOSE_SEQUENCE_TASK(LData.seqBossIdle2)		

		OPEN_SEQUENCE_TASK(LData.seqBossIdle3)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle_var_04", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle_var_05", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
		CLOSE_SEQUENCE_TASK(LData.seqBossIdle3)			
		
		OPEN_SEQUENCE_TASK(LData.seqBossIdle4)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle_var_05", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle_var_01", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "anim@move_m@security_guard",  "idle", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
		CLOSE_SEQUENCE_TASK(LData.seqBossIdle4)	

		LData.iStartLoc[PP_WAITRESS] = GET_RANDOM_INT_IN_RANGE(0, 2)
		
		// far elevator
//		IF (LData.iStartLoc[PP_WAITRESS] = 0)
//			LData.vStartPos[PP_WAITRESS] = <<1119.4313, 269.2793, -53.0407>> 
//			LData.fStartHeading[PP_WAITRESS] =  180.0
//		ELSE
//			LData.vStartPos[PP_WAITRESS] = <<1083.8975, 212.8353, -50.2004>> 
//			LData.fStartHeading[PP_WAITRESS] = 305.6856
//		ENDIF
		IF (LData.iStartLoc[PP_WAITRESS] = 0)
			LData.vStartPos[PP_WAITRESS] = <<1119.6226, 265.8025, -52.0408>> 
			LData.fStartHeading[PP_WAITRESS] = 189.7997
		ELSE
			LData.vStartPos[PP_WAITRESS] = <<1089.8228, 217.5844, -50.2001>> 
			LData.fStartHeading[PP_WAITRESS] = 205.3200 
		ENDIF
		LData.eModelName[PP_WAITRESS] = INT_TO_ENUM(MODEL_NAMES, HASH("s_f_y_casino_01"))
		

		LData.vStartPos[PP_SECURITY] = <<1117.6730, 216.5296, -50.4402>>
		LData.fStartHeading[PP_SECURITY] = 125.6788
		LData.eModelName[PP_SECURITY] = INT_TO_ENUM(MODEL_NAMES, HASH("S_M_Y_Casino_01"))
 
		
		LData.vStartPos[PP_SECURITY_BOSS] = <<1145.8082, 257.6537, -52.4409>>
		LData.fStartHeading[PP_SECURITY_BOSS] = 347.0402	
		LData.eModelName[PP_SECURITY_BOSS] = INT_TO_ENUM(MODEL_NAMES, HASH("S_M_Y_Casino_01"))

		LData.iStartLoc[PP_RARE_GUARD] = 0 //GET_RANDOM_INT_IN_RANGE(0, 2)		
		
		HAVE_ASSETS_LOADED_FOR_PATROL_PED(LData, PP_WAITRESS)
		HAVE_ASSETS_LOADED_FOR_PATROL_PED(LData, PP_SECURITY)
		HAVE_ASSETS_LOADED_FOR_PATROL_PED(LData, PP_SECURITY_BOSS)
		
		LData.bInitialised = TRUE
	ENDIF
ENDPROC


PROC ManageLiftDoors(INT iElevatorID, PATROL_PEDS_LOCAL_DATA &LData, INT iId)
	INT iProgress = GET_SEQUENCE_PROGRESS(LData.PedID[iId] )
	
	INT iDoor1, iDoor2
	IF (iElevatorID = 0)
		iDoor1 = 0
		iDoor2 = 1
	ELSE
		iDoor1 = 2
		iDoor2 = 3
	ENDIF
	
	PRINTLN("[casino_patrol] waitress iProgress = ", iProgress)	
	PRINTLN("[casino_patrol] LData.iLiftState = ", LData.iLiftState)
	
	
	// open door 
	IF (iProgress = 0) // TASK_STAND_STILL(NULL, 2000)
		PP_OPEN_ELEVATOR_DOOR(LData, iDoor1, iElevatorID)
		PP_OPEN_ELEVATOR_DOOR(LData, iDoor2, iElevatorID)
		LData.iLiftState = 0
		LData.iLiftTimer = GET_GAME_TIMER()
	ENDIF
	
	// walking inside 
	IF (iProgress = 1) // TASK_GO_STRAIGHT_TO_COORD(NULL, <<1083.8975, 212.8353, -50.2004>>,  PEDMOVEBLENDRATIO_WALK, DEFAULT, 305.6856)
		LData.iLiftState = 0
		LData.iLiftTimer = GET_GAME_TIMER()
	ENDIF
	
	// wait until walked inside and waiting
	IF (iProgress = 2) // TASK_STAND_STILL(NULL, 10000)
	 
	 	// close door
		IF LData.iLiftState = 0
			IF (GET_GAME_TIMER() - LData.iLiftTimer) > 1000
				IF (GET_GAME_TIMER() - LData.iLiftTimer) < 4000
					PP_CLOSE_ELEVATOR_DOOR(LData, iDoor1, iElevatorID)
					PP_CLOSE_ELEVATOR_DOOR(LData, iDoor2, iElevatorID)	
				ELSE
					LData.iLiftTimer = GET_GAME_TIMER()
					LData.iLiftState++								
				ENDIF
			ENDIF
		ENDIF
	
		// open door
		IF LData.iLiftState = 1
			IF (GET_GAME_TIMER() - LData.iLiftTimer) > 17000
				PP_OPEN_ELEVATOR_DOOR(LData, iDoor1, iElevatorID)
				PP_OPEN_ELEVATOR_DOOR(LData, iDoor2, iElevatorID)	
			ENDIF
		ENDIF
		

	ENDIF
	
	// walking out the lift
	IF (iProgress = 3) // TASK_GO_STRAIGHT_TO_COORD(NULL, <<1085.4458, 214.4323, -50.2003>>,  PEDMOVEBLENDRATIO_WALK, DEFAULT, 305.6856)
	
		PP_OPEN_ELEVATOR_DOOR(LData, iDoor1, iElevatorID)
		PP_OPEN_ELEVATOR_DOOR(LData, iDoor2, iElevatorID)	
		
	ENDIF
ENDPROC

PROC INIT_RARE_GUARD_DATA(PATROL_PEDS_BD_DATA &BData, PATROL_PEDS_LOCAL_DATA &LData)
	IF (BData.bRareGuardActive)
		IF (LData.eModelName[PP_RARE_GUARD] = DUMMY_MODEL_FOR_SCRIPT)
			
			LData.iStartLoc[PP_RARE_GUARD] =  BData.iRareGuardVariation
			
			IF LData.iStartLoc[PP_RARE_GUARD] = 0
				LData.vStartPos[PP_RARE_GUARD] = <<1090.6194, 206.8463, -49.9998>>
				LData.fStartHeading[PP_RARE_GUARD] = 341.8599	
			ELSE
				LData.vStartPos[PP_RARE_GUARD] = <<1119.8268, 218.2046, -50.4301>>
				LData.fStartHeading[PP_RARE_GUARD] = 264.0840	
			ENDIF	
			
			LData.eModelName[PP_RARE_GUARD] = INT_TO_ENUM(MODEL_NAMES, HASH("s_m_m_armoured_01"))
			
			PRINTLN("[casino_patrol] INIT_RARE_GUARD_DATA - init with loc ", LData.iStartLoc[PP_RARE_GUARD])
		ENDIF
	ENDIF
ENDPROC
	

	
PROC UPDATE_LOCAL_RARE_GUARD(PATROL_PEDS_LOCAL_DATA &LData)
	IF HAVE_ASSETS_LOADED_FOR_PATROL_PED(LData, PP_RARE_GUARD)
		IF NOT IS_ENTITY_DEAD(LData.PedID[PP_RARE_GUARD])
			IF NOT IS_ENTITY_PLAYING_ANIM(LData.PedID[PP_RARE_GUARD], "ANIM@AMB@CASINO@PEDS@",  "amb_world_human_clipboard_male_idle_a")
				TASK_PLAY_ANIM(LData.PedID[PP_RARE_GUARD], "ANIM@AMB@CASINO@PEDS@",  "amb_world_human_clipboard_male_idle_a", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, DEFAULT, AF_LOOPING | AF_NOT_INTERRUPTABLE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DELETE_LOCAL_RARE_GUARD(PATROL_PEDS_LOCAL_DATA &LData)
	IF DOES_ENTITY_EXIST(LData.PedID[PP_RARE_GUARD])
		DELETE_PED(LData.PedID[PP_RARE_GUARD])
	ENDIF
	IF DOES_ENTITY_EXIST(LData.ObjectID)
		DELETE_OBJECT(LData.ObjectID)
	ENDIF	
ENDPROC


FUNC BOOL CAN_PLAY_PATROL_PED_SPEECH(PATROL_PEDS_LOCAL_DATA &LData, INT iId)

	IF IS_SCREEN_FADING_OUT()
	OR IS_SCREEN_FADED_OUT()
		PRINTLN("PLAY_PATROL_PED_SPEECH- Fading out")
		RETURN FALSE
	ENDIF
	
	IF IS_AMBIENT_SPEECH_PLAYING(LData.PedID[iId])
	OR IS_SCRIPTED_SPEECH_PLAYING(LData.PedID[iId])
		PRINTLN("[casino_patrol] PLAY_PATROL_PED_SPEECH - IS_AMBIENT_SPEECH_PLAYING")
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
	OR IS_PHONE_ONSCREEN()
		PRINTLN("[casino_patrol] PLAY_PATROL_PED_SPEECH - browser or phone")
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_TYPE_OF_CUTSCENE_PLAYING()
		PRINTLN("[casino_patrol] PLAY_PATROL_PED_SPEECH - IS_ANY_TYPE_OF_CUTSCENE_PLAYING()")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC 

PROC PLAY_PATROL_PED_SPEECH_SECURITY_BOSS(PATROL_PEDS_LOCAL_DATA &LData)

	STRING strContext
	FLOAT fDist = VDIST(GET_PLAYER_COORDS(PLAYER_ID()), GET_ENTITY_COORDS(LData.PedID[PP_SECURITY_BOSS], FALSE))
	
	PRINTLN("[casino_patrol] PLAY_PATROL_PED_SPEECH fDist = ", fDist)
		
	IF NOT (LData.bGreetJustPlayed)
		IF fDist < 3.0
		AND IS_PED_FACING_PED(LData.PedID[PP_SECURITY_BOSS], PLAYER_PED_ID(), 90.0)
			
			IF (GET_GAME_TIMER() - LData.iLastDialogueTime[PP_SECURITY_BOSS] < 10000)
				PRINTLN("[casino_patrol] PLAY_PATROL_PED_SPEECH - PPDC_GREET too soon")
				EXIT	
			ENDIF		
		
			IF LData.iGreetVariation = 0
				strContext = "WELCOME"
			ELSE
				strContext = "WELCOME_BACK"	
			ENDIF
				
			PRINTLN("[casino_patrol] PLAY_PATROL_PED_SPEECH - playing ", strContext)
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(LData.PedID[PP_SECURITY_BOSS], strContext, "CAS_SECURITY_MANAGER", AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_STANDARD), FALSE)
			LData.bGreetJustPlayed = TRUE
			LData.iLastDialogueTime[PP_SECURITY_BOSS] = GET_GAME_TIMER()
			LData.iGreetVariation++
			
		ENDIF
	ENDIF
	
	IF IS_PED_DRUNK(PLAYER_PED_ID())
	AND fDist < 2.0
		IF (GET_GAME_TIMER() - LData.iLastDialogueTime[PP_SECURITY_BOSS] < 10000)
			PRINTLN("[casino_patrol] PLAY_PATROL_PED_SPEECH - drunk too soon")
			EXIT	
		ENDIF	
		PRINTLN("[casino_patrol] PLAY_PATROL_PED_SPEECH - playing drunk")
		PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(LData.PedID[PP_SECURITY_BOSS], "COMMENT_DRUNK", "CAS_SECURITY_MANAGER", AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_STANDARD), FALSE)			
		LData.iLastDialogueTime[PP_SECURITY_BOSS] = GET_GAME_TIMER()	
	ENDIF

	IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), LData.PedID[PP_SECURITY_BOSS])
	
		IF (GET_GAME_TIMER() - LData.iLastDialogueTime[PP_SECURITY_BOSS] < 5000)
			PRINTLN("[casino_patrol] PLAY_PATROL_PED_SPEECH - BUMP too soon")
			EXIT	
		ENDIF					
	
		IF LData.iBumpVariation < 3
			strContext = "BUMP"	
		ELIF LData.iBumpVariation < 6
			strContext = "WHATS_YOUR_PROBLEM"	
		ELIF LData.iBumpVariation < 9
			strContext = "DEFUSE_RESPONSE"				
		ELSE
			strContext = "STORM_OFF"
		ENDIF
	
		PRINTLN("[casino_patrol] PLAY_PATROL_PED_SPEECH - playing BUMP")
		PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(LData.PedID[PP_SECURITY_BOSS], strContext, "CAS_SECURITY_MANAGER", AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_STANDARD), FALSE)			
		LData.iLastDialogueTime[PP_SECURITY_BOSS] = GET_GAME_TIMER()
		LData.iBumpVariation++
		
		IF LData.iBumpVariation > 11
			LData.iBumpVariation = 0
		ENDIF
		
	ELSE
		IF fDist < 1.2		
		
			IF (GET_GAME_TIMER() - LData.iLastDialogueTime[PP_SECURITY_BOSS] < 10000)
				PRINTLN("[casino_patrol] PLAY_PATROL_PED_SPEECH - LOITERING too soon")
				EXIT	
			ENDIF		
			
			IF LData.iLoiteringVariation < 3
				strContext = "PLAYER_LOITERING"
			ELIF  LData.iLoiteringVariation < 6
				strContext = "PLAYER_FOLLOWING"	
			ELSE 
				strContext = "IGNORING_YOU"	
			ENDIF
		
			PRINTLN("[casino_patrol] PLAY_PATROL_PED_SPEECH - playing PLAYER_LOITERING")
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(LData.PedID[PP_SECURITY_BOSS], strContext, "CAS_SECURITY_MANAGER", AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_STANDARD), FALSE)								
			LData.iLastDialogueTime[PP_SECURITY_BOSS] = GET_GAME_TIMER()
			LData.iLoiteringVariation++
			
			IF LData.iLoiteringVariation > 8
				LData.iLoiteringVariation = 0
			ENDIF
		
		ENDIF				
	ENDIF
	
	IF fDist > 15.0
		IF (LData.bGreetJustPlayed)
			LData.bGreetJustPlayed = FALSE	
			PRINTLN("[casino_patrol] PLAY_PATROL_PED_SPEECH - clearing bGreetJustPlayed")	
		ENDIF
		LData.iBumpVariation = 0
		LData.iLoiteringVariation = 0
	ENDIF	
	
ENDPROC

PROC MAINTAIN_SPEECH(PATROL_PEDS_LOCAL_DATA &LData)

	IF CAN_PLAY_PATROL_PED_SPEECH(LData, PP_SECURITY_BOSS)
		IF NOT IS_ENTITY_DEAD(LData.PedID[PP_SECURITY_BOSS])
			PLAY_PATROL_PED_SPEECH_SECURITY_BOSS(LData)
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_PATROL_PEDS_CLIENT(PATROL_PEDS_BD_DATA &BData, PATROL_PEDS_LOCAL_DATA &LData)

	INIT_DATA(LData)
	INIT_RARE_GUARD_DATA(BData, LData)

	LData.iStartLoc[PP_RARE_GUARD] = BData.iRareGuardVariation
	IF (IsRareGuardPatrolling(LData) = FALSE)
		IF NOT IsBrawlMissionRunning()			
			// rare guard at cashier desk is handled by scotts stuff
			IF NOT DOES_ENTITY_EXIST(LData.PedID[PP_RARE_GUARD])
				IF (BData.bRareGuardActive)
					CREATE_LOCAL_RARE_GUARD(LData)
				ENDIF
			ELSE
				UPDATE_LOCAL_RARE_GUARD(LData)
			ENDIF
		ELSE
			DELETE_LOCAL_RARE_GUARD(LData)
		ENDIF
	ENDIF
	

	INT i
	PED_INDEX PedID
	REPEAT NUMBER_OF_CASINO_PATROL_PEDS i
		IF NOT DOES_ENTITY_EXIST(LData.PedID[i])
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(BData.NetPedID[i])
				IF HAVE_ASSETS_LOADED_FOR_PATROL_PED(LData, i)				
					PedID = NET_TO_PED(BData.NetPedID[i])	
					IF DOES_ENTITY_EXIST(PedID)
					AND NOT IS_ENTITY_DEAD(PedID)
						LData.PedID[i] = PedID
						SET_PED_ALTERNATE_MOVEMENT(LData, i)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_DEAD(LData.PedID[i])
			AND NOT (i = PP_WAITRESS)
				IF IS_PED_WALKING(LData.PedID[i])
					SET_PED_CAPSULE(LData.PedID[i], 0.15)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// tray or briefcase
	IF NOT DOES_ENTITY_EXIST(LData.ObjectID)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(BData.NetObjectID)	
			LData.ObjectID = NET_TO_OBJ(BData.NetObjectID)	
		ENDIF
	ENDIF
	
//	// update offset
//	IF DOES_ENTITY_EXIST(LData.ObjectID)
//	AND DOES_ENTITY_EXIST(LData.PedID[PP_RARE_GUARD])
//		ATTACH_ENTITY_TO_ENTITY(LData.ObjectID, LData.PedID[PP_RARE_GUARD], GET_PED_BONE_INDEX(LData.PedID[PP_RARE_GUARD], BONETAG_PH_R_HAND), LData.vOffset, LData.vRotation, TRUE, TRUE)		
//	ENDIF

	// hide stuff for cutscenes
	IF IS_ANY_TYPE_OF_CUTSCENE_PLAYING()
		
		REPEAT NUMBER_OF_CASINO_PATROL_PEDS i
			IF DOES_ENTITY_EXIST(LData.PedID[i])
			AND NOT IS_ENTITY_DEAD(LData.PedID[i])
			AND NETWORK_GET_ENTITY_IS_NETWORKED(LData.PedID[i])
				SET_ENTITY_LOCALLY_INVISIBLE(LData.PedID[i]) 
			ENDIF
		ENDREPEAT	
		
		IF DOES_ENTITY_EXIST(LData.ObjectID)
		AND NETWORK_GET_ENTITY_IS_NETWORKED(LData.ObjectID)
			SET_ENTITY_LOCALLY_INVISIBLE(LData.ObjectID) 
		ENDIF
	ENDIF
	
	// waitress
	IF DOES_ENTITY_EXIST(LData.PedID[PP_WAITRESS])
	AND NOT IS_ENTITY_DEAD(LData.PedID[PP_WAITRESS])	
	
		TASK_HOLD_TRAY(LData)
		
		IF NOT NETWORK_IS_IN_MP_CUTSCENE()
	
			// check if waitress is enter / leave lift 1
			IF (BData.iState[PP_WAITRESS] = 1)	
				IF GET_SCRIPT_TASK_STATUS(LData.PedID[PP_WAITRESS], SCRIPT_TASK_PERFORM_SEQUENCE ) = PERFORMING_TASK
					ManageLiftDoors(0, LData, PP_WAITRESS)	
				ENDIF
			ENDIF
			
			// waiting to finish route 1
			IF (BData.iState[PP_WAITRESS] = 2)
				PP_CLOSE_ELEVATOR_DOOR(LData, 0, 0)
				PP_CLOSE_ELEVATOR_DOOR(LData, 1, 0)
			ENDIF
			
			// check if waitress is enter / leave lift 2
			IF (BData.iState[PP_WAITRESS] = 3)
				IF GET_SCRIPT_TASK_STATUS(LData.PedID[PP_WAITRESS], SCRIPT_TASK_PERFORM_SEQUENCE ) = PERFORMING_TASK
					ManageLiftDoors(1, LData, PP_WAITRESS)	
				ENDIF
			ENDIF			
			
			// waiting to finish route 2
			IF (BData.iState[PP_WAITRESS] = 0)
				PP_CLOSE_ELEVATOR_DOOR(LData, 2, 1)
				PP_CLOSE_ELEVATOR_DOOR(LData, 3, 1)
			ENDIF			
			
		ENDIF
		
	ENDIF
	
	// rare guard
	IF DOES_ENTITY_EXIST(LData.PedID[PP_RARE_GUARD])
	AND NOT IS_ENTITY_DEAD(LData.PedID[PP_RARE_GUARD])	
	AND HAVE_ASSETS_LOADED_FOR_PATROL_PED(LData, PP_RARE_GUARD)
	
		IF IsRareGuardPatrolling(LData)
			TASK_HOLD_CASE(LData)
		ENDIF
	
		IF NOT NETWORK_IS_IN_MP_CUTSCENE()
	
			// check if waitress is enter / leave lift 2
			IF (BData.iState[PP_RARE_GUARD] = 2)	
				IF GET_SCRIPT_TASK_STATUS(LData.PedID[PP_RARE_GUARD], SCRIPT_TASK_PERFORM_SEQUENCE ) = PERFORMING_TASK
					ManageLiftDoors(1, LData, PP_RARE_GUARD)	
				ENDIF
			ENDIF	
			
			// make sure doors are closed
			IF (BData.iState[PP_RARE_GUARD] = 3)	
				PP_CLOSE_ELEVATOR_DOOR(LData, 2, 1)
				PP_CLOSE_ELEVATOR_DOOR(LData, 3, 1)			
			ENDIF
			
		ENDIF	
	ENDIF
	
	// stop player from entering elevators
	IF NOT NETWORK_IS_IN_MP_CUTSCENE()
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		
			VECTOR vPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
			//FLOAT fHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
			vPos.z += -1.0
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1119.40, 267.375, -52.125>>, <<1119.4, 270.3, -49.0>>, 2.6)
				MovePointOutsideAngledArea(vPos, <<1119.40, 267.375, -52.125>>, <<1119.4, 270.3, -49.0>>, 2.6, 0.01)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos, DEFAULT, TRUE, TRUE)
			ENDIF
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1085.075, 214.15, -52.125>>, <<1082.85, 211.9, -47.725>>, 2.6)
				MovePointOutsideAngledArea(vPos, <<1085.075, 214.15, -52.125>>, <<1082.85, 211.9, -47.725>>, 2.6, 0.01)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos, DEFAULT, TRUE, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	// security boss
	IF DOES_ENTITY_EXIST(LData.PedID[PP_SECURITY_BOSS])
	AND NOT IS_ENTITY_DEAD(LData.PedID[PP_SECURITY_BOSS])		
		UPDATE_FIDGET_FOR_SECURITY_BOSS(BData, LData)
	ENDIF
	
	MAINTAIN_SPEECH(LData)
	
	// unload all assets for brawl mission
	IF IsBrawlMissionRunning()	
		UNLOAD_ALL_ASSETS(LData)
	ENDIF

	#IF IS_DEBUG_BUILD
	UPDATE_WIDGETS(BData, LData)
	#ENDIF
	
ENDPROC

	
PROC MAINTAIN_PATROL_PEDS(PATROL_PEDS_BD_DATA &BData, PATROL_PEDS_LOCAL_DATA &LData)

	INIT_DATA(LData)
	
	INT i = LData.iStagger
	
	IF NOT IsBrawlMissionRunning()
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(BData.NetPedID[i])
			// create 
			CREATE_PATROL_PED(BData, LData, i)
		ELSE
			// update
			UPDATE_PATROL_PED(BData, LData, i)
		ENDIF
	ELSE
		DELETE_PATROL_PED(BData, LData, i)
	ENDIF

	
	// stop them from being pushed
	REPEAT NUMBER_OF_CASINO_PATROL_PEDS i
		IF DOES_ENTITY_EXIST(LData.PedID[i])
		AND NOT IS_ENTITY_DEAD(LData.PedID[i])
			IF NETWORK_HAS_CONTROL_OF_ENTITY(LData.PedID[i])
				SET_PED_RESET_FLAG(LData.PedID[i], PRF_UseKinematicPhysics, TRUE)	
			ENDIF
		ENDIF
	ENDREPEAT				
			
	
	LData.iStagger++
	IF (LData.iStagger >= NUMBER_OF_CASINO_PATROL_PEDS)
		LData.iStagger = 0 
	ENDIF
ENDPROC

