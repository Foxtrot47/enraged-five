//////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_mp_intro.sch														//
// Description: Public header containing helper functions for the new MP Intro			//
// Written by:  Martin McMillan															//
// Date:        11/08/2021																//
//////////////////////////////////////////////////////////////////////////////////////////  


//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"

USING "net_include.sch"
USING "net_realty_businesses.sch"

#IF FEATURE_GEN9_EXCLUSIVE
//----------------------
//	HELPER FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD
FUNC STRING MPINTRO_GET_STAT_BIT_NAME(MPINTRO_STAT_BITSET eBit)
	SWITCH eBit
		CASE eMPINTROSTATBITSET_COMPLETED_INTRO					RETURN "eMPINTROSTATBITSET_COMPLETED_INTRO"
		CASE eMPINTROSTATBITSET_COMPLETED_INTRO_MISSION			RETURN "eMPINTROSTATBITSET_COMPLETED_INTRO_MISSION"

		CASE eMPINTROSTATBITSET_END								RETURN "eMPINTROSTATBITSET_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING MPINTRO_GET_BIT_NAME(eMPINTRO_BITSET eState)
	SWITCH eState
		CASE eMPINTROBITSET_COMPLETED_SETUP			RETURN "eMPINTROBITSET_COMPLETED_SETUP"
		CASE eMPINTROBITSET_SHOWN_ENDING_HELP		RETURN "eMPINTROBITSET_SHOWN_ENDING_HELP"
		CASE eMPINTROBITSET_SHOWN_ENDING_HELP_2		RETURN "eMPINTROBITSET_SHOWN_ENDING_HELP_2"
		CASE eMPINTROBITSET_SET_AS_BOSS				RETURN "eMPINTROBITSET_SET_AS_BOSS"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC
#ENDIF

PROC MPINTRO_SET_STAT_BIT(MPINTRO_STAT_BITSET eBit #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF )

	INT iBitset = GET_MP_INT_CHARACTER_STAT(MP_STAT_GEN9_INTRO_BS)
	SET_BIT(iBitset, ENUM_TO_INT(eBit))
	SET_MP_INT_CHARACTER_STAT(MP_STAT_GEN9_INTRO_BS, iBitset)
	
	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		PRINTLN("[MPINTRO] MPINTRO_SET_STAT_BIT - ", MPINTRO_GET_STAT_BIT_NAME(eBit), " - BS = ",GET_MP_INT_CHARACTER_STAT(MP_STAT_GEN9_INTRO_BS))
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
ENDPROC

PROC MPINTRO_CLEAR_STAT_BIT(MPINTRO_STAT_BITSET eBit #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF )

	INT iBitset = GET_MP_INT_CHARACTER_STAT(MP_STAT_GEN9_INTRO_BS)
	CLEAR_BIT(iBitset, ENUM_TO_INT(eBit))
	SET_MP_INT_CHARACTER_STAT(MP_STAT_GEN9_INTRO_BS, iBitset)
	
	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		PRINTLN("[MPINTRO] MPINTRO_CLEAR_STAT_BIT - ", MPINTRO_GET_STAT_BIT_NAME(eBit), " - BS = ",GET_MP_INT_CHARACTER_STAT(MP_STAT_GEN9_INTRO_BS))
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
ENDPROC

// Bit set functions
FUNC BOOL MPINTRO_IS_BIT_SET(eMPINTRO_BITSET eBitset)
	RETURN IS_ARRAYED_BIT_ENUM_SET(GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].sMPIntro.iBitset, eBitset)
ENDFUNC

PROC MPINTRO_SET_BIT(eMPINTRO_BITSET eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)
	IF SET_ARRAYED_BIT_ENUM(GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].sMPIntro.iBitset, eBitset)
		#IF IS_DEBUG_BUILD
		IF NOT bBlockLogPrints
			STRING strTemp = MPINTRO_GET_BIT_NAME(eBitset)
			PRINTLN("[MPINTRO] MPINTRO_SET_BIT - ", strTemp)
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC MPINTRO_CLEAR_BIT(eMPINTRO_BITSET eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)
	IF CLEAR_ARRAYED_BIT_ENUM(GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].sMPIntro.iBitset, eBitset)
		#IF IS_DEBUG_BUILD
		IF NOT bBlockLogPrints
			STRING strTemp = MPINTRO_GET_BIT_NAME(eBitset)
			PRINTLN("[MPINTRO] MPINTRO_CLEAR_BIT - ", strTemp)
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

FUNC MPINTRO_STATE MPINTRO_GET_STATE()
	RETURN GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].sMPIntro.eState
ENDFUNC

FUNC MPINTRO_STATE MPINTRO_GET_REMOTE_PLAYER_STATE(PLAYER_INDEX playerID)
	RETURN GlobalplayerBD_FM_4[NATIVE_TO_INT(playerID)].sMPIntro.eState
ENDFUNC

FUNC CHARACTER_CAREER_TYPE MPINTRO_GET_CAREER_BASED_ON_OWNED_PROPERTY()

	IF DOES_PLAYER_OWN_CLUBHOUSE(PLAYER_ID())
		RETURN CHARACTER_CAREER_BIKER
	ELIF DOES_PLAYER_OWN_A_NIGHTCLUB(PLAYER_ID())
		RETURN CHARACTER_CAREER_NIGHTCLUB_OWNER
	ELIF DOES_PLAYER_OWN_A_BUNKER(PLAYER_ID())
		RETURN CHARACTER_CAREER_GUNRUNNER
	ELIF DOES_PLAYER_OWN_OFFICE(PLAYER_ID())
		RETURN CHARACTER_CAREER_EXECUTIVE
	ENDIF

	RETURN CHARACTER_CAREER_NONE

ENDFUNC

FUNC BOOL MPINTRO_IS_STATE_ON_INTRO(MPINTRO_STATE eState)
	RETURN (eState > eMPINTRO_STATE_INIT)
		AND (eState < eMPINTRO_STATE_COMPLETE)
ENDFUNC

FUNC BOOL MPINTRO_IS_STATE_ON_INTRO_MISSION(MPINTRO_STATE eState)
	RETURN (eState > eMPINTRO_STATE_INIT)
		AND (eState < eMPINTRO_STATE_POST_INTRO)
ENDFUNC

//----------------------
//	PUBLIC FUNCTIONS
//----------------------

FUNC CHARACTER_CAREER_TYPE GET_CHARACTER_CAREER()
	RETURN GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].sMPIntro.eCareer
ENDFUNC

FUNC STRING GET_CURRENT_CHARACTER_CAREER_NAME_FOR_DEBUG()
	RETURN GET_CHARACTER_CAREER_NAME_FOR_DEBUG(GET_CHARACTER_CAREER())
ENDFUNC

/// PURPOSE:
///    Check if player is on any part of the instanced Gen 9 intro flow
/// PARAMS:
///    bCheckStats - Only to be used if we are transitioning into FM and have not yet set up globals
/// RETURNS:
///    TRUE or FALSE
FUNC BOOL IS_PLAYER_ON_MP_INTRO(BOOL bCheckStats = FALSE)

	IF bCheckStats
		RETURN GET_STAT_IS_PLAYER_ON_MP_INTRO()
	ENDIF

	RETURN MPINTRO_IS_STATE_ON_INTRO(MPINTRO_GET_STATE())
ENDFUNC
/// PURPOSE:
///    Check if player is on the initial Gen 9 intro mission that takes you to your property/business
/// RETURNS:
///    TRUE or FALSE
FUNC BOOL IS_PLAYER_ON_MP_INTRO_MISSION()
	RETURN MPINTRO_IS_STATE_ON_INTRO_MISSION(MPINTRO_GET_STATE())
ENDFUNC

/// PURPOSE:
///    Check if player has played through and completed the Gen 9 intro flow
/// RETURNS:
///    TRUE or FALSE
FUNC BOOL HAS_PLAYER_COMPLETED_MP_INTRO()
	RETURN MPINTRO_GET_STATE() = eMPINTRO_STATE_COMPLETE
ENDFUNC

/// PURPOSE:
///    Check if player has bypassed Gen 9 intro flow due to not being eligible
/// RETURNS:
///    TRUE or FALSE
FUNC BOOL HAS_PLAYER_BYPASSED_MP_INTRO()
	RETURN MPINTRO_GET_STATE() = eMPINTRO_STATE_BYPASSED
ENDFUNC

/// PURPOSE:
///    Check if a remote player is on any part of the instanced Gen 9 intro flow
/// PARAMS:
///    playerID - The player to check
/// RETURNS:
///    TRUE or FALSE
FUNC BOOL IS_REMOTE_PLAYER_ON_MP_INTRO(PLAYER_INDEX playerID)
	RETURN MPINTRO_IS_STATE_ON_INTRO(MPINTRO_GET_REMOTE_PLAYER_STATE(playerID))
ENDFUNC

/// PURPOSE:
///    Check if player is on the initial Gen 9 intro mission that takes you to your property/business
/// PARAMS:
///    playerID - The player to check
/// RETURNS:
///    TRUE or FALSE
FUNC BOOL IS_REMOTE_PLAYER_ON_MP_INTRO_MISSION(PLAYER_INDEX playerID)
	RETURN MPINTRO_IS_STATE_ON_INTRO_MISSION(MPINTRO_GET_REMOTE_PLAYER_STATE(playerID))
ENDFUNC

/// PURPOSE:
///    Check if a remote player has played through and completed the Gen 9 intro flow
/// PARAMS:
///    playerID - The player to check
/// RETURNS:
///    TRUE or FALSE
FUNC BOOL HAS_REMOTE_PLAYER_COMPLETED_MP_INTRO(PLAYER_INDEX playerID)
	RETURN MPINTRO_GET_REMOTE_PLAYER_STATE(playerID) = eMPINTRO_STATE_COMPLETE
ENDFUNC

/// PURPOSE:
///    Check if a remote player has bypassed Gen 9 intro flow due to not being eligible
/// PARAMS:
///    playerID - The player to check
/// RETURNS:
///    TRUE or FALSE
FUNC BOOL HAS_REMOTE_PLAYER_BYPASSED_MP_INTRO(PLAYER_INDEX playerID)
	RETURN MPINTRO_GET_REMOTE_PLAYER_STATE(playerID) = eMPINTRO_STATE_BYPASSED
ENDFUNC

/// PURPOSE:
///    Check if player is running the business setup mission for their chosen career during the Gen 9 intro flow
/// RETURNS:
///    
FUNC BOOL IS_PLAYER_ON_MP_INTRO_SETUP_MISSION()
	
	IF NOT IS_PLAYER_ON_MP_INTRO()
		RETURN FALSE
	ENDIF
	
	IF NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	INT iMission = GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID())
	INT iVariation = GB_GET_CONTRABAND_VARIATION_PLAYER_IS_ON(PLAYER_ID())
	
	SWITCH GET_CHARACTER_CAREER()
		CASE CHARACTER_CAREER_EXECUTIVE				RETURN iMission = FMMC_TYPE_GB_CONTRABAND_BUY
														AND iVariation = ENUM_TO_INT(CBV_VEHICLE_COLLECTION)
		
		CASE CHARACTER_CAREER_BIKER					RETURN iMission = FMMC_TYPE_BIKER_BUY
														AND (iVariation = ENUM_TO_INT(IGV_WEED_SETUP)
														OR iVariation = ENUM_TO_INT(IGV_COKE_SETUP)
														OR iVariation = ENUM_TO_INT(IGV_FAKE_ID_SETUP)
														OR iVariation = ENUM_TO_INT(IGV_COUNTERFEIT_SETUP)
														OR iVariation = ENUM_TO_INT(IGV_METH_SETUP))
														
		CASE CHARACTER_CAREER_GUNRUNNER				RETURN iMission = FMMC_TYPE_GUNRUNNING_BUY
														AND iVariation = ENUM_TO_INT(GRV_SETUP)
														
		CASE CHARACTER_CAREER_NIGHTCLUB_OWNER		RETURN iMission = FMMC_TYPE_FMBB_SELL
														AND iVariation = ENUM_TO_INT(MBV_COLLECT_STAFF)								
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC SET_MP_INTRO_SETUP_MISSION_COMPLETE()

	IF NOT IS_PLAYER_ON_MP_INTRO()
		EXIT
	ENDIF

	IF NOT MPINTRO_IS_BIT_SET(eMPINTROBITSET_COMPLETED_SETUP)
		MPINTRO_SET_BIT(eMPINTROBITSET_COMPLETED_SETUP)
		MPINTRO_SET_STAT_BIT(eMPINTROSTATBITSET_COMPLETED_INTRO)
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_BUSINESSSETUPCOMP, TRUE)
		REQUEST_SAVE(SSR_REASON_TUTORIAL_COMPLETE, STAT_SAVETYPE_CRITICAL)
		PRINTLN("[MPINTRO] SET_MP_INTRO_SETUP_MISSION_COMPLETE - TRUE")
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_BLOCKED_EXITING_PROPERTY_FOR_INTRO() 

	IF NOT IS_PLAYER_ON_MP_INTRO()
		RETURN FALSE
	ENDIF
	
	IF MPINTRO_GET_STATE() >= eMPINTRO_STATE_SETUP_MISSION
		RETURN FALSE
	ENDIF

	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_BLOCK_BUNKER_HELP_DURING_GUNRUNING_CAREER_INTRO()
	IF GET_CHARACTER_CAREER() = CHARACTER_CAREER_GUNRUNNER
	AND IS_PLAYER_ON_MP_INTRO()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL IS_PLAYER_RUNNING_A_BUSINESS_APP()

	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appSecuroServ")) > 0
	OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appBikerBusiness")) > 0
	OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appBusinessHub")) > 0
	OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appBunkerBusiness")) > 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC FACTORY_ID GET_FACTORY_ID_OF_MP_INTRO_BIKER_BUSINESS()

	IF DOES_PLAYER_OWN_FACTORY_OF_TYPE(PLAYER_ID(), FACTORY_TYPE_WEED)
		PRINTLN("GET_FACTORY_ID_OF_MP_INTRO_BIKER_BUSINESS - owns a weed factory")
		RETURN GET_FACTORY_ID_OF_OWNED_FACTORY_TYPE(PLAYER_ID(), FACTORY_TYPE_WEED)
	ELIF DOES_PLAYER_OWN_FACTORY_OF_TYPE(PLAYER_ID(), FACTORY_TYPE_FAKE_IDS)
		PRINTLN("GET_FACTORY_ID_OF_MP_INTRO_BIKER_BUSINESS - owns a fake id factory")
		RETURN GET_FACTORY_ID_OF_OWNED_FACTORY_TYPE(PLAYER_ID(), FACTORY_TYPE_FAKE_IDS)
	ELIF DOES_PLAYER_OWN_FACTORY_OF_TYPE(PLAYER_ID(), FACTORY_TYPE_FAKE_MONEY)
		PRINTLN("GET_FACTORY_ID_OF_MP_INTRO_BIKER_BUSINESS - owns a fake money factory")
		RETURN GET_FACTORY_ID_OF_OWNED_FACTORY_TYPE(PLAYER_ID(), FACTORY_TYPE_FAKE_MONEY)
	ELIF DOES_PLAYER_OWN_FACTORY_OF_TYPE(PLAYER_ID(), FACTORY_TYPE_METH)
		PRINTLN("GET_FACTORY_ID_OF_MP_INTRO_BIKER_BUSINESS - owns a meth factory")
		RETURN GET_FACTORY_ID_OF_OWNED_FACTORY_TYPE(PLAYER_ID(), FACTORY_TYPE_METH)
	ELIF DOES_PLAYER_OWN_FACTORY_OF_TYPE(PLAYER_ID(), FACTORY_TYPE_CRACK)
		PRINTLN("GET_FACTORY_ID_OF_MP_INTRO_BIKER_BUSINESS - owns a crack factory")
		RETURN GET_FACTORY_ID_OF_OWNED_FACTORY_TYPE(PLAYER_ID(), FACTORY_TYPE_CRACK)
	ENDIF	
	
	RETURN FACTORY_ID_INVALID

ENDFUNC

FUNC SIMPLE_INTERIORS MP_INTRO_GET_BIKER_BUSINESS_SIMPLE_INTERIOR()

	SIMPLE_INTERIORS eInterior = SIMPLE_INTERIOR_INVALID
	
	FACTORY_ID eFactoryId = GET_FACTORY_ID_OF_MP_INTRO_BIKER_BUSINESS()
	
	eInterior = GET_SIMPLE_INTERIOR_ID_FROM_FACTORY_ID(eFactoryId)

	PRINTLN("MP_INTRO_GET_BIKER_BUSINESS_SIMPLE_INTERIOR - returning simple interior ", ENUM_TO_INT(eInterior))

	RETURN eInterior

ENDFUNC

// note: needs to be called at least 1 frame before the warp.
PROC ACTIVATE_SIMPLE_INTERIOR_FOR_BIKER_BUSINESS_PRE_WARP()
	PRINTLN("calling ACTIVATE_SIMPLE_INTERIOR_FOR_BIKER_BUSINESS_POST_WARP()")
	DEBUG_PRINTCALLSTACK()
	APPLY_BIKER_FACTORY_NON_SETUP_ENTITY_SETS(GET_FACTORY_ID_OF_MP_INTRO_BIKER_BUSINESS(), PLAYER_ID())
ENDPROC

// note: needs to be called just after moving the player (spawning or warping). Needs to be same frame.
PROC ACTIVATE_SIMPLE_INTERIOR_FOR_BIKER_BUSINESS_POST_WARP()
	PRINTLN("calling ACTIVATE_SIMPLE_INTERIOR_FOR_BIKER_BUSINESS_POST_WARP()")
	DEBUG_PRINTCALLSTACK()
	REQUEST_RELAUCHING_OF_SIMPLE_INTERIOR_INT_SCRIPT(MP_INTRO_GET_BIKER_BUSINESS_SIMPLE_INTERIOR())
ENDPROC

// To prevent needing to wrap all checks
#IF NOT FEATURE_GEN9_EXCLUSIVE
FUNC BOOL IS_PLAYER_ON_MP_INTRO(BOOL bCheckStats = FALSE)
	UNUSED_PARAMETER(bCheckStats)
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_ON_MP_INTRO_MISSION()
	RETURN FALSE
ENDFUNC
#ENDIF
