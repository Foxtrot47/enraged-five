USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_network.sch"

USING "net_prints.sch"
USING "net_app_public.sch"
USING "transition_crews.sch"
USING "net_mission.sch"
USING "ply_to_ply_calls.sch"
USING "net_realty_new.sch"
USING "net_ambient_manager.sch"
USING "net_race_to_point.sch"

CONST_INT MP_NUM_FRIENDS_FOR_CONTACTS   100

CONST_INT APP_MP_CONTACTS_ID        18

CONST_INT OPTION_CALL               1
CONST_INT OPTION_SEND_MESSAGE       2
//CONST_INT OPTION_PARTY              2
CONST_INT OPTION_FRIEND             3
CONST_INT OPTION_PROFILE            4
CONST_INT OPTION_CREW               5
//CONST_INT OPTION_RACE_TO_POINT      6
//CONST_INT OPTION_INVITE_CINEMA        7
CONST_INT OPTION_TAG_PLAYER         7
//CONST_INT OPTION_REQUEST_PICKUP       8

CONST_INT MAX_OPTIONS 8

CONST_INT VOICE_SESSION_TIMEOUT     60000


CONST_INT PLAYER_INTERACTION_STAGE_SELECT       0
CONST_INT PLAYER_INTERACTION_STAGE_IN_CALL      1
CONST_INT PLAYER_INTERACTION_STAGE_SEND_MESSAGE 2

CONST_INT FAIL_REASON_TARGET_PLAYER_NOT_ONLINE      0
CONST_INT FAIL_REASON_TARGET_PLAYER_NOT_IN_GAME     1
CONST_INT FAIL_REASON_LOCAL_PLAYER_NOT_ONLINE       2

// The Playerlist Details array
STRUCT m_structPlayerList
    PLAYER_INDEX    plPlayers[NUM_NETWORK_PLAYERS]  // Array of PlayerIDs currently active
    INT             plNumPlayers                    // Number of Player details listed
    GAMER_INFO      plFriends[100] //dont think we can store all friend data at once so testing with 10
    INT             plTotalNumContacts
    INT             m_selectedPlayerSlot
    INT             optionList[MAX_OPTIONS]
    INT             m_selectedOptionSlot        = 0
    TEXT_LABEL_63   M_selectedPlayerClanName

    GAMER_HANDLE    selectedPlayerHandle
    BOOL            bConfirmedInteraction
    BOOL            bCallRequestSent
    //BOOL          bHostingParty
    BOOl            bBusy
    SCRIPT_TIMER voiceSessionTimer
    BOOL            bLocalNotOnline
    BOOL            bButtonReleased
    //SCRIPT_TIMER partyTimeOut
    
    TEXT_LABEL_63   selectedPlayerName
    INT             iPlayerInteractionStage = 0
ENDSTRUCT

FUNC STRING MP_PL_TEXT_LABEL_TO_STRING(STRING theTextLabel)
    RETURN theTextLabel
ENDFUNC

FUNC INT GET_INDEX_OF_FRIEND_ARRAY(INT iLoopIndex,m_structPlayerList& m_sPlayers)
    INT iIndex = iLoopIndex - m_sPlayers.plNumPlayers
    IF iIndex >= 100
        iIndex = 99
    ENDIF
    RETURN iIndex
ENDFUNC

// PURPOSE: Clear the PlayerList array
PROC Clear_PlayerList_Details(m_structPlayerList& m_sPlayers)

    INT tempLoop = 0
    GAMER_INFO emptyGamer
    REPEAT NUM_NETWORK_PLAYERS tempLoop
        m_sPlayers.plPlayers[tempLoop]  = INVALID_PLAYER_INDEX()
    ENDREPEAT
    tempLoop = 0
    REPEAT 100 tempLoop
        m_sPlayers.plFriends[tempLoop]  = emptyGamer
    ENDREPEAT
    m_sPlayers.m_selectedPlayerSlot = 0
    m_sPlayers.plNumPlayers = 0
    
    m_sPlayers.plTotalNumContacts = 0
    tempLoop = 0
    REPEAT MAX_OPTIONS tempLoop
        m_sPlayers.optionList[tempLoop] = 0
    ENDREPEAT
    m_sPlayers.m_selectedOptionSlot        = 0
    m_sPlayers.M_selectedPlayerClanName = ""
    m_sPlayers.selectedPlayerHandle = emptyGamer.Handle
    m_sPlayers.bConfirmedInteraction = FALSE
    m_sPlayers.bCallRequestSent = FALSE
    //m_sPlayers.bHostingParty = FALSE
    RESET_NET_TIMER(m_sPlayers.voiceSessionTimer)
    //RESET_NET_TIMER(m_sPlayers.partyTimeOut)
    m_sPlayers.iPlayerInteractionStage = 0
    m_sPlayers.selectedPlayerName = ""
    #IF IS_DEBUG_BUILD
        NET_PRINT("...CDM MP: MP PlayerList for Players App cleared") NET_NL()
    #ENDIF

ENDPROC



FUNC BOOL IS_SELECTION_FROM_PLAYER_LIST(m_structPlayerList& m_sPlayers)
    IF m_sPlayers.m_selectedPlayerSlot  < m_sPlayers.plNumPlayers
        RETURN TRUE
    ENDIF
    RETURN FALSE
ENDFUNC

FUNC BOOL IS_CURRENT_SELECTION_VALID(m_structPlayerList& m_sPlayers, INT &iFailReason)
    IF NOT NETWORK_IS_SIGNED_ONLINE()
        iFailReason = FAIL_REASON_LOCAL_PLAYER_NOT_ONLINE
    ELSE
        IF m_sPlayers.m_selectedPlayerSlot  < m_sPlayers.plNumPlayers
            IF IS_NET_PLAYER_OK(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot],FALSE)
                RETURN TRUE
            ELSE
                iFailReason = FAIL_REASON_LOCAL_PLAYER_NOT_ONLINE
            ENDIF
        ELSE
            INT iIndex = GET_INDEX_OF_FRIEND_ARRAY(m_sPlayers.m_selectedPlayerSlot,m_sPlayers)
            IF NETWORK_IS_FRIEND_HANDLE_ONLINE(m_sPlayers.plFriends[iIndex].Handle)
                RETURN TRUE
            ELSE
                iFailReason = FAIL_REASON_TARGET_PLAYER_NOT_ONLINE
                PRINTLN("NETWORK_IS_FRIEND_ONLINE: False for player: ",m_sPlayers.plFriends[iIndex].Name)
            ENDIF
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

// PURPOSE: Fill the player interaction screen
PROC Fill_PlayerInteraction_Screen_Slots(m_structPlayerList& m_sPlayers)
    
    INT             slotsUsed       = 0
    NETWORK_CLAN_DESC clanDesc
    
    INT iValidSelectionFailReason
    //#IF IS_DEBUG_BUILD
    STRING sPlayerName
   // #ENDIF

    BOOL bNextOptionValid
    #IF IS_DEBUG_BUILD
        NET_PRINT("...CDM MP: Fill_PlayerInteraction_Screen_Slots() - clearing [view state: ") NET_PRINT_INT(18) NET_PRINT("]") NET_NL()
    #ENDIF
    IF IS_CURRENT_SELECTION_VALID(m_sPlayers,iValidSelectionFailReason)
        IF IS_SELECTION_FROM_PLAYER_LIST(m_sPlayers)
            m_sPlayers.selectedPlayerHandle = GET_GAMER_HANDLE_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
           // #IF IS_DEBUG_BUILD
            sPlayerName = GET_PLAYER_NAME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
            //#ENDIF
        ELSE
            m_sPlayers.selectedPlayerHandle = m_sPlayers.plFriends[GET_INDEX_OF_FRIEND_ARRAY(m_sPlayers.m_selectedPlayerSlot,m_sPlayers)].Handle
            //#IF IS_DEBUG_BUILD
            sPlayerName = MP_PL_TEXT_LABEL_TO_STRING(m_sPlayers.plFriends[GET_INDEX_OF_FRIEND_ARRAY(m_sPlayers.m_selectedPlayerSlot,m_sPlayers)].Name)
           // #ENDIF
        ENDIF
        m_sPlayers.selectedPlayerName = sPlayerName
        //-------------------- CALL INTERACTION ------------------
        //check if other player is on phone?
        bNextOptionValid = FALSE

        #if IS_DEBUG_BUILD

            PRINTNL()
            PRINTSTRING("AppMPPlayerListFunctions - Checking human contact for inclusion of Call Option.")
            PRINTNL()

        #endif
                           
        IF NETWORK_HAS_HEADSET()
        //IF NETWORK_PLAYER_HAS_HEADSET(PLAYER_ID())
            IF NOT g_b_On_TEAM_RACE

                IF IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID())


                    #if IS_DEBUG_BUILD

                        PRINTNL()
                        PRINTSTRING("AppMPPlayerListFunctions - Call option check - Preliminary checks passed. Setting next option valid to TRUE for Call.")
                        PRINTNL()

                    #endif




                    bNextOptionValid = TRUE

                ELSE
                    
                    #if IS_DEBUG_BUILD

                        PRINTNL()
                        PRINTSTRING("AppMPPlayerListFunctions - Call option check - IS_ACCOUNT_OVER_17_FOR_CHAT returned FALSE! Not including Call option.")
                        PRINTNL()

                    #endif
                

                ENDIF
          
            ELSE
          
                #if IS_DEBUG_BUILD

                    PRINTNL()
                    PRINTSTRING("AppMPPlayerListFunctions - Call option check - g_b_On_TEAM_RACE returned TRUE! Not including Call option.")
                    PRINTNL()

                #endif
          
            ENDIF



        ELSE

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING("AppMPPlayerListFunctions - Call option check - NETWORK_HAS_HEADSET returns FALSE! Not including Call option.")
                PRINTNL()

            #endif      

        ENDIF
        

        
        IF IS_SELECTION_FROM_PLAYER_LIST(m_sPlayers) //1600510 - left out of loop directly above as this may need to be removed quickly when I'm off.
            
            IF NETWORK_IS_PLAYER_MUTED_BY_ME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
            OR NETWORK_IS_PLAYER_BLOCKED_BY_ME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
            OR NETWORK_AM_I_BLOCKED_BY_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
            OR NETWORK_AM_I_MUTED_BY_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])


                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring("AppContacts - Call option unavailable for remote player as he has been muted / blocked by you or he has muted / blocked you.")
                    cdPrintnl()


                    PRINTNL()
                    PRINTSTRING("AppMPPlayerListFunctions - Call option check - Secondary checks failed. Setting next option valid to FALSE for Call.")
                    PRINTNL()
            
                #endif 

                bNextOptionValid = FALSE //Prevent the "call" option from appearing when the player is selected from the contact list.

            ENDIF

        ENDIF


        IF NOT NETWORK_HAVE_ONLINE_PRIVILEGES()         
            bNextOptionValid = FALSE //Prevent the "call" option from appearing when the player does not have online privileges
            PRINTNL()
            PRINTSTRING("AppMPPlayerListFunctions - Call option check - No online privileges. Setting next option valid to FALSE for Call.")
            PRINTNL()
        ENDIF
                 


        //Vita Remote Play problem #1993655
        IF IS_USING_REMOTE_PLAY (PLAYER_CONTROL)
        
            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring("AppContacts - Call option unavailable for remote player as player is using Vita Remote Play. See #1993655")
                cdPrintnl()


                PRINTNL()
                PRINTSTRING("AppMPPlayerListFunctions - Call option check - player is using Vita Remote Play. Setting next option valid to FALSE for Call. See #1993655")
                PRINTNL()
        
            #endif 

            bNextOptionValid = FALSE //Prevent the "call" option from appearing when the contact is selected from the contact list.


        ENDIF

        IF NETWORK_IS_GAMER_MUTED_BY_ME(m_sPlayers.selectedPlayerHandle)
        OR NETWORK_IS_GAMER_BLOCKED_BY_ME(m_sPlayers.selectedPlayerHandle)
            PRINTNL()
            PRINTSTRING("AppMPPlayerListFunctions - Call option check - remote player muted by you. Setting next option valid to FALSE for Call.")
            PRINTNL()
            bNextOptionValid = FALSE
        ENDIF
        
        IF NETWORK_AM_I_MUTED_BY_GAMER(m_sPlayers.selectedPlayerHandle)
        OR NETWORK_AM_I_BLOCKED_BY_GAMER(m_sPlayers.selectedPlayerHandle)
            PRINTNL()
            PRINTSTRING("AppMPPlayerListFunctions - Call option check - remote player mute you. Setting next option valid to FALSE for Call.")
            PRINTNL()
            bNextOptionValid = FALSE
        ENDIF
        
        IF NETWORK_IS_IN_PLATFORM_PARTY()
            IF IS_XBOX_PLATFORM()
                IF NETWORK_IS_IN_PLATFORM_PARTY_CHAT()
                    PRINTNL()
                    PRINTSTRING("AppMPPlayerListFunctions - Call option check - player is in xbox party chat. Setting next option valid to FALSE for Call.")
                    PRINTNL()
                    bNextOptionValid = FALSE
                ENDIF
            ELIF IS_PLAYSTATION_PLATFORM()
                PRINTNL()
                PRINTSTRING("AppMPPlayerListFunctions - Call option check - player is in PS4 party. Setting next option valid to FALSE for Call.")
                PRINTNL()
                bNextOptionValid = FALSE
            ENDIF
        ENDIF
        
        IF bNextOptionValid


            IF g_bInMultiplayer

                BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT_EMPTY")
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotsUsed)
                END_SCALEFORM_MOVIE_METHOD()
                // Store the details in the Scaleform slot
                BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(APP_MP_CONTACTS_ID)
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotsUsed)
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                    IF NOT voiceSession.bInVoiceSession
                    OR voiceSession.bAwaitingEndEvent
                        BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_MP_1000")//call
                        END_TEXT_COMMAND_SCALEFORM_STRING()
                    ELSE
                        BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_MP_1000b")//end call
                        END_TEXT_COMMAND_SCALEFORM_STRING()
                    ENDIF
                END_SCALEFORM_MOVIE_METHOD()
                m_sPlayers.optionList[slotsUsed] = OPTION_CALL
                slotsUsed++
                #IF IS_DEBUG_BUILD
                    NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
                    NET_PRINT("- Added call option for player in MP  for  ") NET_PRINT(sPlayerName) NET_NL()
                #ENDIF

            ELSE

                //In SP, only allow the call option to appear off mission #1617499

                IF g_OnMissionState = MISSION_TYPE_OFF_MISSION //EXILE thing?

                    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT_EMPTY")
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotsUsed)
                    END_SCALEFORM_MOVIE_METHOD()
                    // Store the details in the Scaleform slot
                    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(APP_MP_CONTACTS_ID)
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotsUsed)
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                        IF NOT voiceSession.bInVoiceSession
                        OR voiceSession.bAwaitingEndEvent
                            BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_MP_1000")//call
                            END_TEXT_COMMAND_SCALEFORM_STRING()
                        ELSE
                            BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_MP_1000b")//end call
                            END_TEXT_COMMAND_SCALEFORM_STRING()
                        ENDIF
                    END_SCALEFORM_MOVIE_METHOD()
                    m_sPlayers.optionList[slotsUsed] = OPTION_CALL
                    slotsUsed++
                    #if IS_DEBUG_BUILD
                        cdPrintstring("CDM MP: Fill_PlayerInteraction_Screen_Slots")
                        cdPrintstring("- Added call option for player in MP for remote user ") 
                        cdPrintstring(sPlayerName)
                        cdPrintnl()
                    #endif


                ELSE
                    
                    #if IS_DEBUG_BUILD

                        cdPrintstring("CDM MP: Player on mission")
                        cdPrintstring("- not adding call option for player in SP for remote user ") 
                        cdPrintstring(sPlayerName)
                        cdPrintnl()

                    #endif

                ENDIF

            ENDIF


        ELSE
            #IF IS_DEBUG_BUILD
                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
                NET_PRINT("- Can't call ") NET_PRINT(sPlayerName) NET_NL()
            #ENDIF
        ENDIF
        
        
        
        //-------------------- SEND TEXT MESSAGE ------------------
        bNextOptionValid = FALSE
        
        IF IS_SELECTION_FROM_PLAYER_LIST(m_sPlayers)                            
        //AND NOT NETWORK_AM_I_BLOCKED_BY_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
			GAMER_HANDLE gamerHandle = GET_GAMER_HANDLE_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
            IF IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID())
            AND IS_ACCOUNT_OVER_17_FOR_CHAT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
            //AND NOT NETWORK_IS_PLAYER_MUTED_BY_ME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
            //AND NOT NETWORK_IS_PLAYER_BLOCKED_BY_ME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
           // AND NOT NETWORK_AM_I_BLOCKED_BY_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
			AND NETWORK_CAN_TEXT_CHAT_WITH_GAMER(gamerHandle)
            //AND NOT NETWORK_AM_I_MUTED_BY_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
            //OR (IS_GAMER_HANDLE_VALID(m_sPlayers.selectedPlayerHandle) AND NETWORK_IS_FRIEND(m_sPlayers.selectedPlayerHandle))
            //IF NOT HAS_NET_TIMER_STARTED(MPGlobalsAmbience.iSendMessageToPlayerTimer[NATIVE_TO_INT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])])
            //  bNextOptionValid = TRUE
            //ELIF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.iSendMessageToPlayerTimer[NATIVE_TO_INT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])], SEND_MESSAGE_COOLDOWN)
                bNextOptionValid = TRUE
            //ENDIF

            ELSE

                #if IS_DEBUG_BUILD  //extra debug prints sent to the cellphone channel by Steve Taylor to help diagnose #2333703
                    
                    IF NOT IS_ACCOUNT_OVER_17_FOR_CHAT(PLAYER_ID())
                        cdPrintstring("AppMPPlayerListfunctions - not including Send Message option as NULL passed account over 17 for chat has returned FALSE.")
                        cdPrintnl()
                    ENDIF

                    IF NOT IS_ACCOUNT_OVER_17_FOR_CHAT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
                        cdPrintstring("AppMPPlayerListfunctions - not including Send Message option as account over 17 for chat has returned FALSE.")
                        cdPrintnl()
                    ENDIF

                    IF NETWORK_IS_PLAYER_MUTED_BY_ME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
                        cdPrintstring("AppMPPlayerListfunctions - not including Send Message option as target contact is muted by the calling player.")
                        cdPrintnl()
                    ENDIF

                    IF NOT NETWORK_CAN_TEXT_CHAT_WITH_GAMER(gamerHandle)
                        cdPrintstring("AppMPPlayerListfunctions - not including Send Message option as NETWORK_CAN_TEXT_CHAT_WITH_GAMER(gamerHandle) = FALSE")
                        cdPrintnl()
                    ENDIF
//
//                    IF NETWORK_IS_PLAYER_BLOCKED_BY_ME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
//                        cdPrintstring("AppMPPlayerListfunctions - not including Send Message option as target contact is blocked by the calling player.")
//                        cdPrintnl()
//                    ENDIF
//
//                    IF NETWORK_AM_I_MUTED_BY_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
//                        cdPrintstring("AppMPPlayerListfunctions - not including Send Message option as target contact has muted the calling player at the target end.")
//                        cdPrintnl()
//                    ENDIF

                    IF NETWORK_AM_I_BLOCKED_BY_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
                        cdPrintstring("AppMPPlayerListfunctions - not including Send Message option as target contact has blocked the calling player at the target end.")
                        cdPrintnl()
                    ENDIF

                
                    NET_PRINT("appMPPlayerListFunctions - not including Send Message option as chat permissions (age, block or mute) prevents communication.")
                      
                    //Adds to dedicated phone debug filter...
                    cdPrintstring("appMPPlayerListFunctions - not including Send Message option as chat permissions (age, block or mute) prevents communication.")
                    cdPrintnl()

                #endif
                                 
            ENDIF
        ELSE

            #if IS_DEBUG_BUILD
                
                NET_PRINT("appMPPlayerListFunctions - not including Send Message option as IS_SELECTION_FROM_PLAYER_LIST is returning false.")
    
                //Adds to dedicated phone debug filter...
                cdPrintstring("appMPPlayerListFunctions - not including Send Message option as IS_SELECTION_FROM_PLAYER_LIST is returning false.")
                cdPrintnl()

            #endif
            
        ENDIF
        
        IF bNextOptionValid
            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT_EMPTY")
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotsUsed)
            END_SCALEFORM_MOVIE_METHOD()
            // Store the details in the Scaleform slot
            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(APP_MP_CONTACTS_ID)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotsUsed)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                
                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_MP_1009") //Send Message
                END_TEXT_COMMAND_SCALEFORM_STRING()
            END_SCALEFORM_MOVIE_METHOD()
            m_sPlayers.optionList[slotsUsed] = OPTION_SEND_MESSAGE
            slotsUsed++
            #IF IS_DEBUG_BUILD
                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
                NET_PRINT("- Added Send Message option for  ") NET_PRINT(sPlayerName) NET_NL()
            #ENDIF
        ELSE
            #IF IS_DEBUG_BUILD
                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
                NET_PRINT("- Can't Send Message to ") NET_PRINT(sPlayerName) NET_NL()
            #ENDIF
        ENDIF
        
        
//        //-------------------- PARTY INTERACTION ------------------
//      
//        bNextOptionValid = FALSE   
//      IF IS_SELECTION_FROM_PLAYER_LIST(m_sPlayers)
//          IF IS_PS3_VERSION() 
//          OR IS_PLAYSTATION_PLATFORM()
//              //check player is not already in party with local player
//              IF NETWORK_IS_IN_PARTY()
//                  IF NETWORK_IS_PARTY_INVITABLE()
//                      IF NOT NETWORK_IS_PARTY_MEMBER(m_sPlayers.selectedPlayerHandle)
//                          bNextOptionValid = TRUE
//                      ENDIF
//                  ENDIF
//              ELSE
//                  bNextOptionValid = TRUE 
//              ENDIF
//          ENDIF
//      ENDIF
//
//        IF bNextOptionValid
//            // Store the details in the Scaleform slot
//            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(APP_MP_CONTACTS_ID)
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotsUsed)
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//                
//                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_MP_1001")
//                END_TEXT_COMMAND_SCALEFORM_STRING()
//            END_SCALEFORM_MOVIE_METHOD()
//            m_sPlayers.optionList[slotsUsed] = OPTION_PARTY
//            slotsUsed++
//            #IF IS_DEBUG_BUILD
//                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
//                NET_PRINT("- Added party option for  ") NET_PRINT(sPlayerName) NET_NL()
//            #ENDIF
//        ELSE
//            #IF IS_DEBUG_BUILD
//                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
//                NET_PRINT("- Can't invite ") NET_PRINT(sPlayerName) NET_PRINT(" to party ") NET_NL()
//            #ENDIF
//        ENDIF
//        
        //-------------------- FRIEND INTERACTION ------------------
        bNextOptionValid = FALSE    
        IF IS_SELECTION_FROM_PLAYER_LIST(m_sPlayers)
            IF NETWORK_GET_FRIEND_COUNT() < NETWORK_GET_MAX_FRIENDS()
                IF NOT NETWORK_IS_FRIEND(m_sPlayers.selectedPlayerHandle)
                    bNextOptionValid = TRUE
                ENDIF
            ENDIF
        ENDIF    
        IF bNextOptionValid
            // Store the details in the Scaleform slot
            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(APP_MP_CONTACTS_ID)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotsUsed)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                
                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PIM_DFRQ2")
                END_TEXT_COMMAND_SCALEFORM_STRING()
            END_SCALEFORM_MOVIE_METHOD()
            m_sPlayers.optionList[slotsUsed] = OPTION_FRIEND
            slotsUsed++
            #IF IS_DEBUG_BUILD
                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
                NET_PRINT("- Added friend option for  ") NET_PRINT(sPlayerName) NET_NL()
            #ENDIF
        ELSE
            #IF IS_DEBUG_BUILD
                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
                NET_PRINT("- Can't add ") NET_PRINT(sPlayerName) NET_PRINT(" as a friend ") NET_NL()
            #ENDIF
        ENDIF
        
        //-------------------- VIEW PROFILE ------------------
        bNextOptionValid = FALSE    
        //IF NETWORK_IS_FRIEND(m_sPlayers.selectedPlayerHandle)
        bNextOptionValid = TRUE
       // ENDIF
        
        IF bNextOptionValid
            // Store the details in the Scaleform slot
            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(APP_MP_CONTACTS_ID)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotsUsed)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)

                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_MP_1007")
                
                END_TEXT_COMMAND_SCALEFORM_STRING()
            END_SCALEFORM_MOVIE_METHOD()
            m_sPlayers.optionList[slotsUsed] = OPTION_PROFILE
            slotsUsed++
            #IF IS_DEBUG_BUILD
                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
                NET_PRINT("- Added profile option for  ") NET_PRINT(sPlayerName) NET_NL()
            #ENDIF
        ELSE
            #IF IS_DEBUG_BUILD
                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
                NET_PRINT("- Can't add ") NET_PRINT(sPlayerName) NET_PRINT(" profile option ") NET_NL()
            #ENDIF
        ENDIF

        //-------------------- CREW INTERACTION ------------------
        bNextOptionValid = FALSE        
        IF NETWORK_CLAN_SERVICE_IS_VALID()
            IF NETWORK_CLAN_PLAYER_IS_ACTIVE(m_sPlayers.selectedPlayerHandle)
                NETWORK_CLAN_PLAYER_GET_DESC(clanDesc, SIZE_OF(clanDesc), m_sPlayers.selectedPlayerHandle)
            ENDIF
        ENDIF
        IF clanDesc.Id != -1
            IF clanDesc.IsOpenClan = 1
                IF NETWORK_HAS_SOCIAL_CLUB_ACCOUNT()
                AND NOT IS_PLAYER_ALREADY_IN_THIS_CLAN(clanDesc.Id)
                    bNextOptionValid = TRUE 
                    m_sPlayers.M_selectedPlayerClanName = clanDesc.ClanName 
                ENDIF
            ENDIF
        ENDIF
        
        IF bNextOptionValid
            // Store the details in the Scaleform slot
            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(APP_MP_CONTACTS_ID)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotsUsed)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                
                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_MP_1003")
                END_TEXT_COMMAND_SCALEFORM_STRING()
            END_SCALEFORM_MOVIE_METHOD()
            m_sPlayers.optionList[slotsUsed] = OPTION_CREW
            slotsUsed++
            #IF IS_DEBUG_BUILD
                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
                NET_PRINT("- Adding option of joining clan of  ") NET_PRINT(sPlayerName) NET_NL()
            #ENDIF
        ELSE
            #IF IS_DEBUG_BUILD
                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
                NET_PRINT("- Can't join clan of ") NET_PRINT(sPlayerName) NET_NL()
            #ENDIF
        ENDIF
        
        //-------------------- RACE TO POINT ------------------
//        bNextOptionValid = FALSE    
//      IF IS_NET_PLAYER_OK(PLAYER_ID())
//          IF NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(),TRUE)
//              IF IS_SELECTION_FROM_PLAYER_LIST(m_sPlayers)
//                  IF CAN_DO_RACE_TO_POINT(TRUE,TRUE)
//                      IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), GET_PLAYER_COORDS(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])) <= R2P_INVITE_RADIUS
//                          IF NOT IS_BIT_SET(MPGlobalsAmbience.R2Pdata.iInvitedFromPlayerListBS,NATIVE_TO_INT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]) )
//                              bNextOptionValid = TRUE
//                          ENDIF
//                      ENDIF
//                  ENDIF
//              ENDIF
//          ENDIF
//      ENDIF
//        IF bNextOptionValid
//            // Store the details in the Scaleform slot
//            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(APP_MP_CONTACTS_ID)
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotsUsed)
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//                
//                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_MP_1004")
//                END_TEXT_COMMAND_SCALEFORM_STRING()
//            END_SCALEFORM_MOVIE_METHOD()
//            m_sPlayers.optionList[slotsUsed] = OPTION_RACE_TO_POINT
//            slotsUsed++
//            #IF IS_DEBUG_BUILD
//                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
//                NET_PRINT("- Adding option of doing race to point  ") NET_PRINT(sPlayerName) NET_NL()
//            #ENDIF
//        ELSE
//            #IF IS_DEBUG_BUILD
//                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
//                NET_PRINT("-NOT  Adding option of doing race to point ") NET_PRINT(sPlayerName) NET_NL()
//            #ENDIF
//        ENDIF
        
//      //-------------------- INVITE ------------------
//        bNextOptionValid = FALSE    
//        IF IS_NET_PLAYER_OK(PLAYER_ID())
//          IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(),m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]) 
//              IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(),MPAM_TYPE_CINEMA)
//                  IF NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot],MPAM_TYPE_CINEMA)
//                  //AND NOT HAS_PLAYER_BEEN_INVITES
//                      bNextOptionValid = TRUE
//                  ENDIF
//              ENDIF
//            ENDIF
//        ENDIF
//        
//        IF bNextOptionValid
//            // Store the details in the Scaleform slot
//            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(APP_MP_CONTACTS_ID)
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotsUsed)
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(33)
//                
//                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_MP_1006")
//                END_TEXT_COMMAND_SCALEFORM_STRING()
//            END_SCALEFORM_MOVIE_METHOD()
//            m_sPlayers.optionList[slotsUsed] = OPTION_INVITE_CINEMA
//            slotsUsed++
//            #IF IS_DEBUG_BUILD
//                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
//                NET_PRINT("- Adding option of invite to cinema  ") NET_PRINT(sPlayerName) NET_NL()
//            #ENDIF
//        ELSE
//            #IF IS_DEBUG_BUILD
//                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
//                NET_PRINT("- NOT Adding option of invite to cinema ") NET_PRINT(sPlayerName) NET_NL()
//            #ENDIF
//        ENDIF

//      //-------------------- REQUEST_PICKUP ------------------
//      
//        bNextOptionValid = FALSE  
//      IF NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(),TRUE)
//          IF NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(),MPAM_TYPE_CINEMA)
//              IF IS_SELECTION_FROM_PLAYER_LIST(m_sPlayers)
//                  IF IS_NET_PLAYER_OK(PLAYER_ID())
//                      IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(),m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]) 
//                          IF IS_PLAYER_ON_MY_TEAM(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
//                          AND NOT NETWORK_AM_I_BLOCKED_BY_PLAYER(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
//                              IF MPGlobalsAmbience.R2Pdata.iLaunchStage = -1
//                                  IF NOT HAS_NET_TIMER_STARTED(MPGlobalsAmbience.requestPickupDelay[NATIVE_TO_INT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])])
//                                  OR HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.requestPickupDelay[NATIVE_TO_INT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])],30000)
//                                      //AND NOT HAS_PLAYER_BEEN_INVITES
//                                      bNextOptionValid = TRUE
//                                  ENDIF
//                              ENDIF
//                          ENDIF
//                      ENDIF
//                  ENDIF
//              ENDIF
//          ENDIF
//        ENDIF
//        IF bNextOptionValid
//            // Store the details in the Scaleform slot
//            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(APP_MP_CONTACTS_ID)
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotsUsed)
//                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//                
//                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_MP_1006")
//                END_TEXT_COMMAND_SCALEFORM_STRING()
//            END_SCALEFORM_MOVIE_METHOD()
//            m_sPlayers.optionList[slotsUsed] = OPTION_REQUEST_PICKUP
//            slotsUsed++
//            #IF IS_DEBUG_BUILD
//                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
//                NET_PRINT("- Adding option of request pick up  ") NET_PRINT(sPlayerName) NET_NL()
//            #ENDIF
//        ELSE
//            #IF IS_DEBUG_BUILD
//                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
//                NET_PRINT("- NOT Adding option of request pick up ") NET_PRINT(sPlayerName) NET_NL()
//            #ENDIF
//        ENDIF
        
        //-------------------- TAG PLAYER ------------------

        CLEAR_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_HUMAN_CONTACT_HIGHLIGHT_AVAILABLE)

        bNextOptionValid = FALSE    
        IF IS_SELECTION_FROM_PLAYER_LIST(m_sPlayers)
            IF IS_NET_PLAYER_OK(PLAYER_ID())
                IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(),m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]) 
                    IF NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot],MPAM_TYPE_CINEMA)
                        IF NOT IS_BIT_SET(  GlobalplayerBD[NATIVE_TO_INT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])].iSpecInfoBitset, 
                                            SPEC_INFO_BS_SPECTATOR_CAM_IS_RUNNING)  //#  1543225
                            bNextOptionValid = TRUE

                            SET_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_HUMAN_CONTACT_HIGHLIGHT_AVAILABLE)

                            #if IS_DEBUG_BUILD

                                cdPrintnl()
                                cdPrintstring ("appMP_PLF_Header - Setting g_BSTHIRD_HUMAN_CONTACT_HIGHLIGHT_AVAILABLE bit")
                                cdPrintnl()

                            #endif
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
			IF m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot] != INVALID_PLAYER_INDEX()
			AND IS_NET_PLAYER_OK(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot],FALSE,FALSE)
				IF IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot], FMMC_TYPE_HUNT_THE_BEAST)
					bNextOptionValid = FALSE
					  #if IS_DEBUG_BUILD

		                cdPrintnl()
		                cdPrintstring ("appMP_PLF_Header - Beast player - making highlight option addition invalid.")
		                cdPrintnl()

		            #endif
				ENDIF
			ENDIF
	    ENDIF
        
		

        //Adding in easy-to-remove Death Match clause for #1602355
        IF g_b_On_Deathmatch
        OR g_bFM_ON_TEAM_DEATHMATCH

            //#if IS_DEBUG_BUILD
            //  OR g_DumpDisableEveryFrameCaller - Comment in to test.
            //#endif 

            bNextOptionValid = FALSE

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring ("appMP_PLF_Header - On Deathmatch or Team Deathmatch - making highlight option addition invalid.")
                cdPrintnl()

            #endif

        ENDIF



        IF bNextOptionValid
            // Store the details in the Scaleform slot
            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(APP_MP_CONTACTS_ID)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotsUsed)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                IF NOT IS_BIT_SET(MPGlobals.iTaggedPlayersBitSet, NATIVE_TO_INT(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot]))
                    BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_MP_1008")
                    #IF IS_DEBUG_BUILD
                        NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
                        NET_PRINT("- Adding option of tagging player  ") NET_PRINT(sPlayerName) NET_NL()
                    #ENDIF
                ELSE
                    BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_MP_1008b")
                    #IF IS_DEBUG_BUILD
                        NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
                        NET_PRINT("- Adding option of removing tag from player  ") NET_PRINT(sPlayerName) NET_NL()
                    #ENDIF
                ENDIF
                END_TEXT_COMMAND_SCALEFORM_STRING()
            END_SCALEFORM_MOVIE_METHOD()
            m_sPlayers.optionList[slotsUsed] = OPTION_TAG_PLAYER
            slotsUsed++
            #IF IS_DEBUG_BUILD
                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
                NET_PRINT("- Adding option of tagging player  ") NET_PRINT(sPlayerName) NET_NL()
            #ENDIF
        ELSE
            #IF IS_DEBUG_BUILD
                NET_PRINT("CDM MP: Fill_PlayerInteraction_Screen_Slots")
                NET_PRINT("- NOT adding Option of tagging player ") NET_PRINT(sPlayerName) NET_NL()
            #ENDIF
        ENDIF
    
    ELSE


        //#1608514 - Note. It is this that is returning the "*invalid*" string
        /*
        STRING thePlayerName
        IF IS_SELECTION_FROM_PLAYER_LIST(m_sPlayers)
            thePlayerName = GET_PLAYER_NAME(m_sPlayers.plPlayers[m_sPlayers.m_selectedPlayerSlot])
        ELSE
            thePlayerName = MP_PL_TEXT_LABEL_TO_STRING(m_sPlayers.plFriends[GET_INDEX_OF_FRIEND_ARRAY(m_sPlayers.m_selectedPlayerSlot,m_sPlayers)].Name)
        ENDIF
        */



        BEGIN_TEXT_COMMAND_THEFEED_POST("CELL_LEFT_SESS")
            //ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(thePlayerName)
        END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)  

        //"The selected contact is no longer available." 


        //#1608514 
        
        HANG_UP_AND_PUT_AWAY_PHONE()


       // Display_PlayerList_Screen()
       // m_currentStage = PLS_PLAYERLIST
        #IF IS_DEBUG_BUILD
            NET_PRINT("...CDM MP: moving back to player list selected player not IS_CURRENT_SELECTION_VALID() - Hanging up phone")
            NET_NL()
        #ENDIF
        EXIT
    ENDIF
ENDPROC





FUNC BOOL IS_INITIAL_CALL_PROCESSING_COMPLETE(m_structPlayerList& m_sPlayers,STRING playerName)
    
    IF voiceSession.bInVoiceSession

        IF NETWORK_SESSION_IS_IN_VOICE_SESSION()
            NETWORK_SESSION_VOICE_CONNECT_TO_PLAYER(m_sPlayers.selectedPlayerHandle)
            voiceSession.gamerHandle = m_sPlayers.selectedPlayerHandle
            voiceSession.gamerName = playerName
            PRINTLN("IS_INITIAL_CALL_PROCESSING_COMPLETE: trying to add player to voice session")
            RETURN TRUE
        ELSE

            #IF IS_DEBUG_BUILD
                PRINTLN("IS_INITIAL_CALL_PROCESSING_COMPLETE returned false!")
            #ENDIF


        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC


//EOF
