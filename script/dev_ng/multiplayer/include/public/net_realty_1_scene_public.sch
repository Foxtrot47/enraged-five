USING "SceneTool_public.sch"

USING "cutscene_public.sch"
USING "rc_helper_functions.sch"

#IF IS_DEBUG_BUILD
USING "net_realty_scene_debug.sch"
#ENDIF

ENUM enumNET_REALTY_1_SCENEPans
	NET_REALTY_1_SCENE_PAN_establishing,
	NET_REALTY_1_SCENE_PAN_MAX
ENDENUM

ENUM enumNET_REALTY_1_SCENECuts
	NET_REALTY_1_SCENE_CUT_null = 0,
	NET_REALTY_1_SCENE_CUT_MAX
ENDENUM

ENUM enumNET_REALTY_1_SCENEMarkers
	NET_REALTY_1_SCENE_MARKER_playerWalk0 = 0,
	NET_REALTY_1_SCENE_MARKER_playerWalk1,
	NET_REALTY_1_SCENE_MARKER_playerWalk2,
	NET_REALTY_1_SCENE_MARKER_MAX
ENDENUM

ENUM enumNET_REALTY_1_SCENEPlacers
	NET_REALTY_1_SCENE_PLACER_playerPos = 0,
	NET_REALTY_1_SCENE_PLACER_MAX
ENDENUM

STRUCT STRUCT_NET_REALTY_1_SCENE
	structSceneTool_Pan		mPans[NET_REALTY_1_SCENE_PAN_MAX]
	structSceneTool_Cut		mCuts[NET_REALTY_1_SCENE_CUT_MAX]
	
	structSceneTool_Marker	mMarkers[NET_REALTY_1_SCENE_MARKER_MAX]
	structSceneTool_Placer	mPlacers[NET_REALTY_1_SCENE_PLACER_MAX]
	
	BOOL					bEnablePans[NET_REALTY_1_SCENE_PAN_MAX]
	FLOAT					fExitDelay
ENDSTRUCT

FUNC BOOL Private_Get_NET_REALTY_1_SCENE(INT iBuildingID, STRUCT_NET_REALTY_1_SCENE& scene	, INT iEntrance 	#IF ACTIVATE_PROPERTY_CS_DEBUG	,	BOOL bIgnoreAssert = FALSE	#ENDIF	)
	SWITCH iBuildingID
		CASE 0	RETURN FALSE	BREAK
		
		CASE MP_PROPERTY_BUILDING_1
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-764.146790,287.016663,88.399513>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<2.117963,-0.000593,31.722597>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-764.115967,286.966858,89.982582>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<67.312279,-0.000594,30.308628>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 38.958
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 6.500
			scene.bEnablePans[NET_REALTY_1_SCENE_CUT_null] = FALSE
			scene.mCuts[NET_REALTY_1_SCENE_CUT_null].mCam.vPos = <<0,0,0>>
			scene.mCuts[NET_REALTY_1_SCENE_CUT_null].mCam.vRot = <<0,0,0>>
			scene.mCuts[NET_REALTY_1_SCENE_CUT_null].fFov = 0.0
			scene.mCuts[NET_REALTY_1_SCENE_CUT_null].fShake = 0.0
			scene.mMarkers[NET_REALTY_1_SCENE_MARKER_playerWalk0].vPos = <<-777.1078, 312.9173, 84.6992>>
			scene.mMarkers[NET_REALTY_1_SCENE_MARKER_playerWalk1].vPos = <<-777.1355, 318.5040, 84.6690>>
			scene.mMarkers[NET_REALTY_1_SCENE_MARKER_playerWalk2].vPos = <<-781.8685, 317.8841, 84.6690>>
			
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = GET_MP_PROPERTY_BUILDING_WORLD_POINT(iBuildingID)
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 0
			
			scene.fExitDelay = 0.5
			RETURN TRUE
		BREAK
		
		//Joe Rubino, Wed 11/09/2013 18:12
		CASE MP_PROPERTY_BUILDING_9          //Med apt 2
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<16.4329, 6.8270, 80.5267>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-7.5722, 0.0000, 21.8734>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<5.9640, 9.5679, 80.5267>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<-5.0091, 0.0000, -1.2863>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 50.0000
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<9.7400, 84.6906, 84.8975>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-89.4999, 0.0000, 94.2595>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<3.7647, 37.9219, 70.5326>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<4.4901, 40.2624, 70.5325>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<4.7016, 41.4096, 70.5324>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<3.1573, 35.8189, 70.5353>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 2.8800
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
//		//Joe Rubino, Saturday, August 24, 2013 7:19 PM
//		CASE MP_PROPERTY_BUILDING_17        //Low apt 1
//			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<36.6608, -11.5344, 77.4773>>
//			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-0.8903, 0.0000, 38.5711>>
//			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<34.6375, -8.9738, 77.4266>>
//			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<-2.2895, 0.0000, 34.4435>>
//			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 33.4450
//			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.2500
//			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 6.5000
//			scene.mCuts[NET_REALTY_1_SCENE_CUT_null].mCam.vPos = <<18.0530, -5.4515, 71.5144>>
//			scene.mCuts[NET_REALTY_1_SCENE_CUT_null].mCam.vRot = <<-7.4857, 0.0000, 32.7084>>
//			scene.mCuts[NET_REALTY_1_SCENE_CUT_null].fFov = 50.0000
//			scene.mCuts[NET_REALTY_1_SCENE_CUT_null].fShake = 0.2500
//			scene.mMarkers[NET_REALTY_1_SCENE_MARKER_playerWalk0].vPos = <<-42.1289, -54.0925, 62.2531>>
//			scene.mMarkers[NET_REALTY_1_SCENE_MARKER_playerWalk1].vPos = scene.mMarkers[NET_REALTY_1_SCENE_MARKER_playerWalk0].vPos
//			scene.mMarkers[NET_REALTY_1_SCENE_MARKER_playerWalk2].vPos = scene.mMarkers[NET_REALTY_1_SCENE_MARKER_playerWalk1].vPos
//			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-45.1289, -57.0925, 62.2531>>
//			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 0.0000
//			scene.bEnablePans[0] = FALSE
//			scene.fExitDelay = 0.5000
//			RETURN TRUE
//		BREAK
		
		//Joe Rubino, Fri 06/09/2013 22:35
		CASE MP_PROPERTY_BUILDING_10        //Med apt 3
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<5.9445, 97.0624, 85.2171>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-28.0555, 0.0000, -161.6681>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<5.9445, 97.0624, 85.2171>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<22.9757, -0.0000, -165.5721>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 35.9431
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-12.3328, 90.4359, 78.9943>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-3.1404, 0.0000, 0.0000>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<9.3664, 80.3693, 77.4446>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<8.9422, 77.8170, 77.4445>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<7.5691, 76.7449, 77.4450>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<10.2835, 81.9529, 77.4349>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 142.2000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		
		//Joe Rubino, Wed 18/09/2013 16:06
		CASE MP_PROPERTY_BUILDING_18	//Low apt 2
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-209.0646, 182.8417, 83.5620>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-8.7027, -0.0000, -57.9412>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-209.5238, 182.5246, 85.3932>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<18.0955, 0.0000, -55.3760>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 45.4706
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 3.2000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-212.4597, 186.8262, 81.7795>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-8.9058, 0.0000, 2.0662>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-200.9720, 185.8880, 79.3274>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-191.8559, 187.7168, 79.6304>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-201.5801, 186.2485, 79.3274>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-200.9754, 185.1715, 79.3133>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 360.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		
		//Forrest Karbowski, Mon 26/08/2013 23:25
		CASE MP_PROPERTY_BUILDING_19        //Low apt 3
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-818.4136, -995.9948, 15.8156>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-0.2251, 0.0000, -13.4816>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-818.0875, -994.8571, 16.1372>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<17.7022, -0.0000, -22.7164>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 42.4130
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.0000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-814.1236, -984.7809, 14.7478>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-8.0001, 0.0000, 0.0000>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-812.1517, -980.2053, 13.2756>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-811.2305, -979.7562, 13.2278>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-809.3616, -978.3578, 13.2278>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-813.2254, -979.8819, 13.1845>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 293.7600
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		//Forrest Karbowski, Mon 26/08/2013 23:25
		CASE MP_PROPERTY_BUILDING_20        //Low apt 4
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-668.7176, -847.0402, 26.7036>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-8.1205, 0.0000, -126.9468>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-672.0388, -844.1733, 33.1015>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<37.1171, -0.0000, -139.4427>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 50.0000
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-665.9950, -848.7388, 26.3021>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-24.2122, -0.0000, -154.1999>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-662.4749, -854.2733, 23.4632>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-662.4390, -855.0770, 23.5232>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-662.4626, -858.1933, 23.5232>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-663.7423, -853.7128, 23.4364>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 246.9600
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		
		//Joe Rubino, Wed 11/09/2013 18:12
		CASE MP_PROPERTY_BUILDING_21        //Low apt 5
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-1544.1926, -325.6087, 48.7006>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<0.9878, 0.0000, -93.1602>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-1544.1074, -325.2982, 52.6245>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<14.1037, 0.0000, -87.8281>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 46.9647
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-1539.0712, -323.6113, 49.1943>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-15.6880, 0.0000, -117.8159>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-1533.1915, -326.5664, 46.9112>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-1531.5039, -328.6764, 46.9200>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-1528.2477, -326.8056, 46.9200>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-1532.7437, -325.4603, 46.9112>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 190.8000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		
		//Forrest Karbowski, Mon 26/08/2013 23:25
		CASE MP_PROPERTY_BUILDING_22        //Low apt 6
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-1550.9618, -411.5307, 43.5845>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-4.1345, -0.0000, 81.7934>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-1550.7887, -411.5194, 46.7358>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<36.4638, 0.0000, 87.4149>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 45.7458
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-1558.9783, -414.2136, 43.6826>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-13.7780, 0.0000, 42.6711>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-1564.4786, -406.3255, 41.3890>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-1565.5349, -405.6029, 41.3882>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-1567.6156, -403.6811, 41.3882>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-1562.1353, -404.7448, 41.3890>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 117.7200
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		
		//Joe Rubino, Thu 12/09/2013 22:21
		CASE MP_PROPERTY_BUILDING_23        //Low apt 7
            scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-1615.7172, -427.2462, 43.8714>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-24.2332, -0.0608, -111.2071>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-1615.7172, -427.2462, 43.8714>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<39.3052, -0.0608, -109.2756>>
            scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 50.0000
            scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
            scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 5.0000
            scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-1612.4561, -427.1014, 42.2432>>
            scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-17.0852, 0.0000, -125.5306>>
            scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
            scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-1606.4282, -433.1473, 39.4353>>
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-1603.5750, -435.2336, 39.4220>>
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-1602.1851, -436.2353, 39.4220>>
            scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-1608.5460, -433.6591, 39.4307>>
            scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 302.7600
            scene.bEnablePans[0] = FALSE
            scene.fExitDelay = 0.5000
            RETURN TRUE
		BREAK
		
		//Luke Howard, Thu 12/09/2013 13:22
		CASE MP_PROPERTY_BUILDING_6          //High apt 14,15
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-952.8615, -373.9132, 41.3722>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-1.4572, -0.0000, -109.5390>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-953.1790, -373.8005, 44.2973>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<66.6251, -0.0000, -109.5086>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 40.1924
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-934.6458, -384.7574, 40.1453>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-18.1743, 0.0000, -11.0527>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-933.2696, -384.0754, 37.9613>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-932.9, -383.7, 37.9613>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-932.9, -383.7, 37.9613>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-935.9081, -380.9029, 37.9613>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 207.3600
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		//Forrest Karbowski, Wed 28/08/2013 20:38
		CASE MP_PROPERTY_BUILDING_7          //High apt 16,17
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-606.7625, 5.8908, 46.1434>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-3.9545, -0.0000, 17.6827>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-606.7625, 5.8908, 46.1434>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<55.9204, 0.0000, 7.3095>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 50.0000
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-619.4422, 27.8255, 44.8775>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-10.8012, 0.0000, -19.0543>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-614.8010, 37.9139, 42.5897>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-617.2769, 44.2665, 42.5806>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-616.1315, 40.8841, 42.5883>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-615.9416, 36.8541, 42.5735>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 35.2800
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		
		//Blake Buck , Thu 29/08/2013 00:35
		CASE MP_PROPERTY_BUILDING_5          //High apt 12,13
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-63.2719, -554.3776, 42.5900>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-1.5637, 0.0773, -157.8888>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-63.5758, -553.6505, 48.4018>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<40.1273, 0.0773, -157.8888>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 37.4999
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-52.0226, -586.4724, 38.4119>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-17.3572, -0.0000, -136.2297>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-47.6852, -587.6509, 36.9580>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-45.3853, -588.2827, 37.1663>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-44.0858, -582.7677, 37.1674>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-48.8711, -589.5564, 36.9580>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 0.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		
		//Luke Howard , 17-09-2013 16:38:49 (1636944)
		CASE MP_PROPERTY_BUILDING_2	//High apt 5,6
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-262.4320, -984.2408, 34.1312>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-4.8896, 0.0000, 3.6011>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-262.4470, -985.7540, 34.6655>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<63.9671, 0.0000, 8.5645>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 50.0000
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.9600
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-252.5713, -949.9199, 37.7210>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-89.4999, 0.0000, 94.2595>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-260.8901, -970.2086, 30.2196>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-264.0789, -963.5338, 30.2235>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-268.8266, -955.2571, 30.2234>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-259.1245, -968.8276, 30.2196>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 135.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		
		//Joe Rubino , Thu 29/08/2013 01:24
		CASE MP_PROPERTY_BUILDING_3          //High apt 7,8
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-1427.9330, -556.7278, 36.0702>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-6.2601, -0.0000, 54.5506>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-1428.3942, -556.3994, 36.0081>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<54.5161, 0.0000, 54.5506>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 43.3970
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-1443.0938, -544.7684, 41.2424>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-89.4999, 0.0000, 94.2595>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-1442.4517, -544.0094, 33.7424>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-1444.6846, -541.8195, 33.7417>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-1446.4171, -538.6528, 33.7410>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-1441.3108, -545.0703, 33.7424>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 66.2400
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		//Joe Rubino , Thu 29/08/2013 01:24
		CASE MP_PROPERTY_BUILDING_4          //High apt 9,10,11
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-930.8760, -486.5101, 39.8948>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-4.6055, -0.0000, -30.7434>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-930.8760, -486.5101, 39.8948>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<59.5121, -0.0000, -27.2118>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 38.9142
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-917.3011, -461.1729, 40.8489>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-26.9735, 0.0000, -33.8730>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-912.8500, -454.1392, 38.5999>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-911.8500, -453.1392, 38.5999>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-910.8500, -452.1392, 38.5999>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-913.6371, -456.8211, 38.5999>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 0.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		
		//Forrest Karbowski, 27-08-2013 17:11:29
		CASE MP_PROPERTY_BUILDING_12	//Med apt 5
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-198.8816, 97.0476, 70.6552>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<0.8232, 0.0000, -170.7582>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-199.0013, 97.7828, 76.3453>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<6.7146, 0.0000, -170.7582>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 50.0000
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-194.2464, 94.1687, 71.6105>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-19.6197, -0.0000, 128.6171>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-198.0717, 83.7918, 68.7585>>	//<<-197.8564, 85.8970, 68.7564>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-198.0717, 83.7918, 68.7585>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-198.2247, 82.5191, 68.7601>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-198.3362, 87.4109, 68.7477>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 189.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		
		//Forrest Karbowski, Thu 29/08/2013 16:59
		CASE MP_PROPERTY_BUILDING_17        //Low apt 1
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-48.5784, -38.9737, 66.0594>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-6.9087, -0.0000, -157.8717>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-48.5784, -38.9737, 66.0594>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<37.2662, 0.0000, -152.1534>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 48.0867
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<18.0530, -5.4515, 71.5144>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-7.4857, 0.0000, 32.7084>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-38.7890, -59.3940, 63.0680>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-38.1290, -59.6640, 63.0680>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-37.5590, -59.9040, 63.0680>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-42.2142, -59.2807, 62.4963>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 274.3200
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		
		//Joe Rubino, Fri 06/09/2013 22:35
		CASE MP_PROPERTY_BUILDING_8          //Med apt 1
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<275.4461, -162.8499, 66.2759>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-4.9207, 0.0000, -78.5758>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<275.4461, -162.8499, 66.2759>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<38.4836, -0.0000, -58.1826>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 49.3615
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<282.3360, -159.6951, 63.3730>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<33.6005, -0.0000, -105.0994>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<285.8218, -160.2860, 63.6221>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<286.4932, -160.2142, 63.6743>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<291.0940, -163.7814, 61.3492>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<284.6325, -162.2803, 63.6221>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 332.6400
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		//Joe Rubino, Fri 06/09/2013 22:35
		CASE MP_PROPERTY_BUILDING_11        //Med apt 4
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-526.1783, 121.8094, 65.2405>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-0.0565, 0.0000, -134.5127>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-523.8516, 119.5216, 66.0724>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<16.3573, -0.0000, -134.5127>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 46.8403
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 7.4050
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-511.4594, 108.6869, 64.6722>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-5.9833, -0.0000, 14.1879>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-511.7757, 105.7855, 62.8006>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-511.4152, 101.6653, 62.8006>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-513.4710, 100.7747, 62.8006>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-510.7062, 108.8691, 62.8006>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 142.2000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		//Joe Rubino, Fri 06/09/2013 22:35
		CASE MP_PROPERTY_BUILDING_13        //Med apt 6
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-653.9388, 172.6451, 68.3956>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-14.5896, 0.0000, -112.5694>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-651.1877, 171.5017, 68.5470>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<11.5287, 0.0000, -112.5694>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 43.8951
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-629.6102, 167.6119, 61.8764>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-3.0000, 0.0000, 0.0000>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-626.4092, 169.4724, 60.1626>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-624.9139, 171.3835, 61.9539>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-625.3212, 168.8364, 60.1651>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-628.9289, 168.8015, 60.1413>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 285.1200
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		//Joe Rubino, Wed 11/09/2013 18:12
		CASE MP_PROPERTY_BUILDING_14        //Med apt 7
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-981.1505, -1437.6874, 9.1368>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-9.2981, 0.0000, -52.3394>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-979.6581, -1437.6234, 9.1775>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<15.9431, 0.0000, -15.7977>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 42.5590
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-980.7847, -1437.7778, 8.6966>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-6.3937, 0.0000, -67.4498>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-970.3165, -1431.2484, 6.6791>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-967.7848, -1430.0485, 6.7684>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-968.8149, -1426.9413, 6.7684>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-970.1749, -1432.2048, 6.6791>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 299.8800
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		//Joe Rubino, Fri 06/09/2013 22:35
		CASE MP_PROPERTY_BUILDING_15        //Med apt 8
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-832.9249, -835.3940, 22.9385>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-5.8753, -0.0000, -173.0395>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-824.9551, -833.4756, 26.3226>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<12.8298, -0.0000, -170.1190>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 35.0493
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-831.3647, -860.8909, 21.0378>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-11.5852, 0.0000, 6.5266>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-831.4977, -863.8859, 19.7126>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-830.8679, -865.4865, 19.7126>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-831.1386, -865.9618, 19.7126>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-832.0834, -861.9040, 19.6946>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 193.3200
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
		//Joe Rubino, Fri 06/09/2013 22:35
		CASE MP_PROPERTY_BUILDING_16        //Med apt 9
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-751.3406, -750.4796, 29.9035>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-12.5433, 0.0000, 121.1872>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-751.3406, -750.4796, 29.9035>>
			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<56.5823, -0.0000, 123.7904>>
			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 61.8447
			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-759.8932, -758.0070, 28.5537>>
			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-1.9886, -0.0000, 5.3909>>
			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-765.9852, -754.1449, 26.8752>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-766.9714, -755.4404, 26.8752>>
			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-766.6912, -758.4532, 26.8752>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-762.8427, -753.8799, 26.8736>>
			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 105.8400
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.5000
			RETURN TRUE
		BREAK
//		//CDM 18/6/2014
//		CASE MP_PROPERTY_BUILDING_57	//PROPERTY_IND_DAY_MEDIUM_1
//			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-1500.7656, 518.8423, 119.5781>>
//			scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-11.1074, -0.0000, -9.3328>>
//			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-1502.6135, 514.5117, 123.3311>>
//			scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<-16.3814, 0.0000, -31.3875>>
//			scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 49.9959
//			scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
//			scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
//			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-1496.0000, 512.0843, 118.6189>>
//			scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-3.0001, 0.0000, 0.0000>>
//			scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
//			scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
//			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-1500.9650, 523.1985, 117.2722>>
//			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-1500.9650, 523.1990, 117.2722>>
//			scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-1500.9650, 523.1990, 117.2722>>
//			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-1499.9050, 521.2685, 117.2722>>
//			scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 0.0000
//			scene.bEnablePans[0] = FALSE
//			scene.fExitDelay = 0.5000
//			RETURN TRUE
//		BREAK

		CASE MP_PROPERTY_BUILDING_57        //IND_DAY_MEDIUM_1
            scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-1404.5686, 505.8876, 124.2054>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<0.2675, -0.0000, -10.9241>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-1403.1555, 505.5369, 124.2054>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<0.2675, -0.0000, -23.8402>>
            scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 50.0000
            scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
            scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.0000
            scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-1496.0000, 512.0843, 118.6189>>
            scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-3.0001, 0.0000, 0.0000>>
            scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
            scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-1404.9149, 526.9042, 122.8440>>
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-1404.9150, 526.9040, 122.8440>>
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-1404.9150, 526.9040, 122.8440>>
            scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-1406.1467, 526.8649, 122.8361>>
            scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 273.7500
            scene.bEnablePans[0] = FALSE
            scene.fExitDelay = 0.5000
            RETURN TRUE
		BREAK
		
		CASE MP_PROPERTY_BUILDING_58        //IND_DAY_MEDIUM_2
	        scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<1327.9758, -1580.9358, 54.7918>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-1.6230, 0.0000, -73.0287>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<1327.7219, -1580.2666, 54.7918>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<-1.6230, 0.0000, -54.7927>>
            scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 50.0000
            scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
            scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 4.5000
            scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-1501.4905, 522.9186, 119.0109>>
            scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-2.6974, 0.0000, -144.7947>>
            scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
            scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<1336.6704, -1578.5664, 53.5760>>
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<1336.6700, -1578.5660, 53.5760>>
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<1336.6700, -1578.5660, 53.5760>>
            scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<1337.1830, -1579.1577, 53.2443>>
            scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 41.2500
            scene.bEnablePans[0] = FALSE
            scene.fExitDelay = 0.5000
            RETURN TRUE
		BREAK
		
		CASE MP_PROPERTY_BUILDING_59       //IND_DAY_MEDIUM_3
            scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-117.4990, 6533.6177, 30.8231>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-0.9024, -0.0000, -119.5713>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-117.7792, 6532.7847, 30.8262>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<-1.5503, -0.0000, -125.3187>>
            scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 45.2616
            scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
            scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
            scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-15.4012, 6609.5630, 34.0445>>
            scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-42.3207, -0.0000, 9.8353>>
            scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
            scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-105.8921, 6528.1489, 29.4110>>
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-105.8920, 6000.0000, 29.4110>>
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-105.8920, 6000.0000, 29.4110>>
            scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-105.4947, 6528.7666, 29.1719>>
            scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 135.0000
            scene.bEnablePans[0] = FALSE
            scene.fExitDelay = 0.5000
            RETURN TRUE
		BREAK
		
    	CASE MP_PROPERTY_BUILDING_60        //IND_DAY_MEDIUM_4
            scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-314.3491, 6329.5425, 33.1484>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<4.3643, -0.0000, -110.6641>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-314.4063, 6328.8486, 33.1631>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<2.7551, -0.0000, -117.1005>>
            scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 50.0000
            scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
            scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.0000
            scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-309.1423, 6342.8843, 32.0011>>
            scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-4.7469, -0.0000, -157.6043>>
            scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
            scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-302.4787, 6326.3696, 31.8928>>
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-302.4790, 6000.0000, 31.8930>>
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-302.4790, 6000.0000, 31.8930>>
            scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-302.7712, 6327.0127, 31.8919>>
            scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 217.5000
            scene.bEnablePans[0] = FALSE
            scene.fExitDelay = 0.5000
            RETURN TRUE
		BREAK
		
		CASE MP_PROPERTY_BUILDING_61        //IND_DAY_LOW_1
            scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<-4.6936, 6544.4785, 34.3108>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-0.8994, 0.1541, 41.0393>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<-5.2879, 6543.8721, 34.3131>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<0.6296, 0.1541, 61.8894>>
            scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 45.0000
            scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
            scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 6.5000
            scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-105.1235, 6527.9424, 30.9723>>
            scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-7.6816, 0.0000, 14.5907>>
            scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
            scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<-15.6618, 6557.0786, 32.2676>>
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<-15.7333, 6557.1499, 32.2559>>
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<-15.7876, 6557.2124, 32.2455>>
            scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<-14.7801, 6557.9116, 32.2455>>
            scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 138.7500
            scene.bEnablePans[0] = FALSE
            scene.fExitDelay = 0.5000
            RETURN TRUE
		BREAK

		CASE MP_PROPERTY_BUILDING_62        //IND_DAY_LOW_2
            scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<1904.7861, 3770.0391, 33.7531>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-5.7096, -0.0000, 35.7448>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<1903.9667, 3769.2939, 33.7531>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<-5.7096, -0.0000, 49.9815>>
            scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 50.0000
            scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
            scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 5.5000
            scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<-15.3187, 6557.1484, 33.4866>>
            scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-13.0784, -0.0000, -48.8424>>
            scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
            scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<1898.5499, 3781.5476, 31.9342>>
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<1898.5500, 3781.5481, 31.9340>>
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<1898.5500, 3781.5481, 31.9340>>
            scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<1899.1631, 3781.8655, 31.8834>>
            scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 116.2500
            scene.bEnablePans[0] = FALSE
            scene.fExitDelay = 0.5000
            RETURN TRUE
		BREAK

		CASE MP_PROPERTY_BUILDING_63        //IND_DAY_LOW_3
            scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vPos = <<1668.0369, 4758.7124, 43.3325>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mStart.vRot = <<-1.9164, -0.1183, 35.1509>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vPos = <<1667.5472, 4758.5771, 43.3257>>
            scene.mPans[net_realty_1_scene_PAN_establishing].mEnd.vRot = <<-1.9164, -0.1183, 43.3979>>
            scene.mPans[net_realty_1_scene_PAN_establishing].fFov = 36.4464
            scene.mPans[net_realty_1_scene_PAN_establishing].fShake = 0.2500
            scene.mPans[net_realty_1_scene_PAN_establishing].fDuration = 5.0000
            scene.mCuts[net_realty_1_scene_CUT_null].mCam.vPos = <<1899.1578, 3780.7351, 33.6846>>
            scene.mCuts[net_realty_1_scene_CUT_null].mCam.vRot = <<-4.6976, 0.0000, -41.3041>>
            scene.mCuts[net_realty_1_scene_CUT_null].fFov = 50.0000
            scene.mCuts[net_realty_1_scene_CUT_null].fShake = 0.2500
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos = <<1661.8740, 4776.1196, 41.3705>>
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos = <<1661.8740, 4776.1201, 41.3710>>
            scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos = <<1661.8740, 4776.1201, 41.3710>>
            scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = <<1663.1089, 4776.2847, 41.0075>>
            scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 101.2500
            scene.bEnablePans[0] = FALSE
            scene.fExitDelay = 0.5000
            RETURN TRUE
		BREAK

		CASE MP_PROPERTY_STILT_BUILDING_5_BASE_A
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<133.2934, 584.3945, 190.0845>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-10.8508, -0.0000, 160.7334>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<132.8789, 583.0829, 188.2512>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<-8.9635, 0.0000, 160.7334>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 36.9913
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5
			RETURN TRUE
		BREAK
		
		CASE MP_PROPERTY_STILT_BUILDING_1_BASE_B
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-208.0829, 518.3535, 140.3316>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-7.3226, 0.1861, -130.1492>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-206.5799, 517.0856, 140.0791>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<-6.6921, 0.1861, -130.1492>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 30.7081
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5	
			RETURN TRUE
		BREAK
		
		CASE MP_PROPERTY_STILT_BUILDING_2_B
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<353.9353, 462.4018, 150.4586>> 
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-2.5531, -0.0000, 153.1456>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<353.2027, 460.9551, 150.3837>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<-2.9072, -0.0000, 153.1456>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 35.8575
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 6.500
			RETURN TRUE
		BREAK
		
		CASE MP_PROPERTY_STILT_BUILDING_3_B
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-741.6832, 641.0106, 154.7754>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-23.8965, 0.0035, 138.6701>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-740.3902, 642.4799, 149.1488>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<-9.0033, 0.0035, 138.6701>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 35.6885
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5
			RETURN TRUE
		BREAK
		
		CASE MP_PROPERTY_STILT_BUILDING_4_B
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-711.5103, 600.7102, 146.8317>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot =  <<-3.8913, -0.0000, -96.5459>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-707.7958, 600.6136, 146.5823>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<-2.9451, -0.0000, -106.0029>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 41.1816
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5
			RETURN TRUE
		BREAK
		
		CASE MP_PROPERTY_STILT_BUILDING_7_A
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-566.6136, 682.6280, 153.9417>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-13.2295, -0.0000, -176.9261>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-566.4597, 679.9078, 152.9502>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<-21.0684, -0.0000, -176.9261>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 44.4597
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5
			RETURN TRUE
		BREAK
		
		CASE MP_PROPERTY_STILT_BUILDING_8_A
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-714.3430, 600.4932, 145.6452>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-5.1117, -0.0000, 93.0870>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-716.1815, 600.2896, 145.4803>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<-5.1117, -0.0000, 93.5199>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 40.9374
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5
			RETURN TRUE
			
		BREAK
		CASE MP_PROPERTY_STILT_BUILDING_10_A
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-871.8802, 714.9831, 157.9014>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-12.9560, 0.0000, -153.2849>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-871.4766, 714.3058, 153.0697>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<-5.1549, -0.0000, -152.8134>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 40.7390
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5	
			RETURN TRUE
		BREAK
//		CASE MP_PROPERTY_STILT_BUILDING_11_A
//			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-909.2755, 708.6140, 155.5324>>
//			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-5.9275, 0.0005, 170.4684>>
//			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-928.3853, 708.7055, 155.8599>>
//			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<-7.0448, 0.0000, -139.4973>>
//			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 38.1862
//			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
//			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 6.500	
//			RETURN TRUE	
//		BREAK

		CASE MP_PROPERTY_STILT_BUILDING_12_A
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-1308.7963, 466.9584, 101.2854>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-6.2008, -0.0000, -137.3542>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-1302.7015, 470.1645, 101.1989>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<-6.9115, -0.0000, -151.9336>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 33.0663
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5		
			RETURN TRUE
		BREAK
		
		CASE MP_PROPERTY_STILT_BUILDING_13_A
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<360.6288, 447.7085, 152.3981>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-14.2309, -0.0000, -150.1516>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<364.2911, 449.7356, 149.9966>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<-9.9722, -0.0000, -156.8911>>
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 38.3855
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
			scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5		
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_OFFICE_BUILDING_1
			IF iEntrance = 0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-1604.4607, -524.7966, 41.0270>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<3.8238, -0.0000, -145.4550>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-1604.4607, -524.7966, 41.0270>> 
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<39.3718, 0.0000, -144.2074>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 50
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 4
				RETURN TRUE
			ELIF iEntrance = 1
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos =  <<-1562.7817, -535.1185, 119.8220>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-11.5371, -0.0000, 162.9484>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-1560.7837, -528.6019, 112.0111>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<-17.6105, 0.0000, 162.9484>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 50
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 4
				RETURN TRUE
			ENDIF
			
		BREAK
		
		CASE MP_PROPERTY_OFFICE_BUILDING_2
			IF iEntrance = 0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-1361.4659, -530.5521, 37.4391>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<3.6438, -0.0000, 9.0420>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-1361.4659, -530.5521, 37.4391>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<26.3500, 0.0000, 9.0420>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 50
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5
				RETURN TRUE
			ELIF iEntrance = 1
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos =  <<-1340.9651, -485.9737, 91.8395>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-17.3985, -0.0000, 66.7450>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-1341.0265, -485.9550, 84.4150>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<-12.1969, -0.0000, 70.2023>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 50
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5
				RETURN TRUE
			ENDIF
			
		BREAK
		
		CASE MP_PROPERTY_OFFICE_BUILDING_3
			IF iEntrance = 0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-68.4012, -642.7999, 49.4706>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<5.8340, -1.2990, 50.6573>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-68.4012, -642.7999, 49.4706>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<44.8900, -1.2990, 55.8225>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 47.2556
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 6.5	
			ELIF iEntrance = 1
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-80.4670, -671.7493, 156.2275>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<13.4637, 0.1570, 36.1166>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-79.5971, -673.3090, 150.2293>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<-17.6748, 0.1570, 31.9479>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 34.8060
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 6.5	
			ENDIF
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_OFFICE_BUILDING_4
			IF iEntrance = 0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<97.5429, -742.6163, 119.7490>> //<<-89.0406, -754.8719, 55.5483>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-16.6061, 0.4860, 112.4649>>//<<1.7601, 0.0000, -170.0249>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<95.9027, -743.2773, 121.1036>>// <<-91.2010, -741.3782, 372.1989>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<37.4712, 0.4860, 113.4889>>//<<-48.5945, -0.0000, -174.2083>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 30
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 6.6	
			ELIF iEntrance = 1
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-96.4946, -872.8673, 331.5534>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-22.4994, -0.0000, -21.6700>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-93.9497, -870.1461, 238.8003>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<33.8407, -0.0000, -18.3067>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 50
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 4
			ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	SWITCH iBuildingID
		CASE MP_PROPERTY_CLUBHOUSE_BUILDING_1
			IF iEntrance = 0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<276.3126, -1830.5745, 27.4641>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<10.3317, -0.0000, 29.4192>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos =<<275.9980, -1830.0762, 28.3617>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<13.6228, -0.0000, 29.1940>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 62.8023
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 6.6
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_CLUBHOUSE_BUILDING_2
			IF iEntrance = 0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-1476.5219, -959.1437, 9.9776>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<7.0914, 0.0000, 9.3966>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-1476.8148, -957.3717, 10.2890>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<12.0257, -0.0000, 9.3966>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 50.0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_CLUBHOUSE_BUILDING_3
			IF iEntrance = 0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<25.4904, -1034.8164, 29.4169>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<0.2223, 0.0000, -75.4860>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<27.5923, -1034.2714, 29.7021>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<13.0847, -0.0000, -75.4860>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 50.0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_CLUBHOUSE_BUILDING_4
			IF iEntrance = 0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<38.5308, 2782.2214, 57.8536>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<0.8513, -0.0000, -52.9510>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<39.6345, 2783.0642, 57.9778>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<8.4851, -0.0000, -52.9510>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 50.0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_CLUBHOUSE_BUILDING_5
			IF iEntrance = 0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-341.4055, 6090.9282, 39.1533>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-15.2138, 0.0000, 167.3142>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-342.4233, 6086.4106, 37.8931>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<-15.2138, 0.0000, 167.3142>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 50.0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_CLUBHOUSE_BUILDING_6
			IF iEntrance = 0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<1743.4716, 3723.8005, 34.5536>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<0.7364, -0.0000, 163.9551>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<1742.2789, 3721.3467, 34.6222>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<5.5424, -0.0000, 163.9551>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 50.0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5
			ENDIF
			
			RETURN TRUE
		BREAK
		
		CASE MP_PROPERTY_CLUBHOUSE_BUILDING_7
			IF iEntrance = 0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<915.3560, -1434.9849, 31.1141>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<2.4655, -0.0000, -135.6969>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<917.1609, -1436.8337, 31.3592>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<6.0013, -0.0000, -135.6969>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 36.6601
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 6.6
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_CLUBHOUSE_BUILDING_8
			IF iEntrance = 0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<197.7034, 294.2755, 109.8201>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-11.2896, -0.0000, 36.2861>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<195.6434, 297.0814, 109.1805>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<-7.6567, 0.0000, 36.2861>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 50.0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_CLUBHOUSE_BUILDING_9
			IF iEntrance = 0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-39.5358, -202.4035, 54.0587>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<5.7660, 0.0000, -55.5984>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-38.6302, -201.9416, 54.1626>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<8.3564, -0.0000, -55.5984>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 44.2655
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_CLUBHOUSE_BUILDING_10
			IF iEntrance = 0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<2509.3657, 4109.6470, 37.9957>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<2.7399, 0.0000, 107.1840>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<2508.2510, 4109.2876, 38.0515>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<5.9493, -0.0000, 107.1839>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 34.4001
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_CLUBHOUSE_BUILDING_11
			IF iEntrance = 0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-36.2847, 6405.1216, 31.9714>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<-1.4549, -0.0000, 15.0229>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-36.9731, 6407.6880, 32.2795>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<9.6539, 0.0000, 15.0229>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 50.0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_CLUBHOUSE_BUILDING_12
			IF iEntrance = 0
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = <<-1149.9884, -1598.9075, 4.3410>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = <<2.4955, -0.0000, -6.7014>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = <<-1149.8022, -1597.3248, 4.4105>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = <<4.8661, -0.0000, -6.7014>>
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = 32.3628
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
				scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 5
			ENDIF
			
			RETURN TRUE
		BREAK
	ENDSWITCH
	scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vPos = GET_GAMEPLAY_CAM_COORD()
	scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mStart.vRot = GET_GAMEPLAY_CAM_ROT()
	scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos = GET_GAMEPLAY_CAM_COORD()
	scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot = GET_GAMEPLAY_CAM_ROT()
	scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov = GET_GAMEPLAY_CAM_FOV()
	scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake = 0.25
	scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fDuration = 6.500
	scene.mCuts[NET_REALTY_1_SCENE_CUT_null].mCam.vPos = scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vPos
	scene.mCuts[NET_REALTY_1_SCENE_CUT_null].mCam.vRot = scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].mEnd.vRot
	scene.mCuts[NET_REALTY_1_SCENE_CUT_null].fFov = scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fFov
	scene.mCuts[NET_REALTY_1_SCENE_CUT_null].fShake = scene.mPans[NET_REALTY_1_SCENE_PAN_establishing].fShake
	scene.bEnablePans[NET_REALTY_1_SCENE_CUT_null] = FALSE
	
	scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos = GET_MP_PROPERTY_BUILDING_WORLD_POINT(iBuildingID)
	scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].fRot = 0
	
	scene.mMarkers[NET_REALTY_1_SCENE_MARKER_playerWalk0].vPos = scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos].vPos	+ <<1.0,1.0,0.0>>
	scene.mMarkers[NET_REALTY_1_SCENE_MARKER_playerWalk1].vPos = scene.mMarkers[NET_REALTY_1_SCENE_MARKER_playerWalk0].vPos	+ <<1.0,1.0,0.0>>
	scene.mMarkers[NET_REALTY_1_SCENE_MARKER_playerWalk2].vPos = scene.mMarkers[NET_REALTY_1_SCENE_MARKER_playerWalk1].vPos	+ <<1.0,1.0,0.0>>
	
	scene.fExitDelay = 0.5
	
	#IF ACTIVATE_PROPERTY_CS_DEBUG
	IF NOT bIgnoreAssert
		TEXT_LABEL_63 str
		str = "Private_Get_NET_REALTY_1_SCENE() invalid ID "
		str += GET_STRING_FROM_STRING(Private_Get_Net_Realty_Building_Name(iBuildingID), GET_LENGTH_OF_LITERAL_STRING("MP_PROPERTY_"), GET_LENGTH_OF_LITERAL_STRING(Private_Get_Net_Realty_Building_Name(iBuildingID)))
		SCRIPT_ASSERT(str)
	ENDIF
	#ENDIF
	RETURN FALSE
ENDFUNC

#IF ACTIVATE_PROPERTY_CS_DEBUG
USING "net_realty_1_scene_debug.sch"
#ENDIF
