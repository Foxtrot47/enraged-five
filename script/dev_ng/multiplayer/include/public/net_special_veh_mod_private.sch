//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_special_veh_mod_private.sc																//
// Description: All the private special veh mod stuff, none of these should be used by other scripts than	//		
// 				AM_MP_SPECIAL_VEH_MOD.sc																	//
// Written by:  Ata																							//
// Date:  		15/03/2017																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch" 
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "commands_task.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_interiors.sch"
USING "carmod_shop_private.sch"
USING "cellphone_public.sch"
USING "LineActivation.sch"
USING "net_ambience.sch"
USING "vehicle_public.sch"
USING "menu_public.sch"
USING "net_mission_trigger_overview.sch"
USING "vehicle_gen_public.sch"
USING "flow_public_game.sch"
USING "locates_public.sch"
USING "fmmc_launcher_menu.sch"
USING "net_spawn.sch"
USING "net_saved_vehicles.sch"
USING "net_freemode_cut.sch"
USING "player_scene_private.sch"
USING "cheat_handler.sch"
USING "achievement_public.sch"
USING "net_transition_sessions_private.sch"
USING "net_realty_new.sch"
USING "net_save_create_eom_vehicle.sch"
USING "net_pv_rate_limiter.sch"
USING "net_fps_cam.sch"
USING "store_trigger.sch"
USING "net_wait_zero.sch"
USING "net_mod_value.sch"
USING "net_plate_dupes.sch"


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ VARIABLES  ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

ENUM SPEC_VEH_MOD_STAGE_ENUM
	SPEC_VEH_MOD_STAGE_WAIT_FOR_TRIGGER = 0,	// Checks for player triggered modding
	SPEC_VEH_MOD_STAGE_INITIALISE,				// Load all the required assets
	SPEC_VEH_MOD_STAGE_BROWSE_ITEMS,			// Player browsing list of items
	SPEC_VEH_MOD_STAGE_BAIL
ENDENUM

ENUM SPEC_VEH_MOD_MENU_NAME
	SVM_INVALID_MENU = -1,
	SVM_VEH_SELECTION = 0,
	SVM_MAIN_MENU,	
	SVM_ARMOUR_MENU,			
	SVM_RESPRAY_MENU,		
	SVM_ENGINE_MENU,
	SVM_LIVERY_MENU,
	SVM_TURRET_MENU,
	SVM_TRANSMISSION_MENU,
	SVM_TYRE_MENU,
	SVM_TURBO_MENU
ENDENUM

STRUCT SPEC_VEH_MOD_MENU_INPUT
	INT iLeftX, iLeftY, iRightX, iRightY, iMoveUp, iCursorValue
	BOOL bAccept
	BOOL bBack
	BOOL bPrevious
	BOOL bNext
ENDSTRUCT

STRUCT SPEC_VEH_MOD_RESPRAY_VAR
	INT iCol1, iCol2, iCol3, iCol4
	INT iCol1a, iCol2a, iCol3a, iCol4a
	INT iCol1b, iCol2b
	INT iCost, iDisplayCost
	INT iRed, iGreen, iBlue
ENDSTRUCT

STRUCT SPEC_VEH_MOD_MENU_BROWSE_STRUCT
	INT iMenuDepth
	INT iMenuOptions[(MAX_MENU_OPTIONS/32) + 1]
	INT iMenuOption[MAX_MENU_DEPTHS]
	INT iTopOption[MAX_MENU_DEPTHS]
	INT iOptionCost[MAX_MENU_OPTIONS]
	BOOL bMenuInitialised
	BOOL bRestoreCurrentItem
	BOOL bItemPurchased
	BOOL bConfirmShopExit
	INT iCurrentVehicleModIndex[MAX_VEHICLE_MOD_SLOTS]
	INT iCurrentItem			// The current item that is selected
	INT iCurrentSubItem			// The current sub item that is selected
	INT iCurrentGroup			// The current group that is selected
	INT iControl				// So we can control the browse process
	INT iColourGroupControl, iColourSlotControl
	INT iPreloadTimer
	INT iPreviousItem
	BOOL bGetPreviousItem
ENDSTRUCT

STRUCT SPE_VEH_CAMERA_STRUCT
	FLOAT fRotationAngle
	FLOAT fZCamOffset = 0.0
	
	FLOAT fMinGndHeight = 0.7		// min height camera can be above ground (ground being z ground coord of car position)
	FLOAT fMaxGndHeight = 4.0		// max height camera can be above ground (ground being z ground coord of car position) - this will usually be the cars height plus some value
	FLOAT fMaxHRadius = 8.0			// camera position will always get clamp so that horizontal distance to the target point can never be bigger than this
	
	VECTOR vTargetOffset			// offset relative to the car we are looking at - (used to shift camera to stop the clipping)
	VECTOR vPanOffset , vLastDefaultLookAtOffset, vBrowseCamLookAt
	FLOAT fModVehicleWidth, fModVehicleLength, fLastDefaultYOffset, fLastDefaultZOffset, fCachedZOffset, fBrowseCamZOffset_Interp, fBrowseCamFinalRot_Interp
	FLOAT fBrowseCamFinalRot, fBrowseCamZOffset, fCamZoomAlpha, fCamZoomFov, fBrowseCamHeading
	MOD_SHOP_CAM_ENUM eCurrentBrowseCam
	BOOL bBrowseCamSetup, bForceZoomForMod, bLastDefaultOffsetStored, bZoomToggle
	INT iLastDefaultOffsetStoredGameTime
ENDSTRUCT

STRUCT SPEC_VEH_MOD_STRUCT
	SPEC_VEH_MOD_STAGE_ENUM eMainStages	
	SPEC_VEH_MOD_SHOP_NAME_ENUM eSpecVehModName
	SPEC_VEH_MOD_MENU_INPUT sInputData
	VEHICLE_INDEX specVehMod
	SPEC_VEH_MOD_MENU_NAME	eMenu
	MOD_TYPE eModSlot
	SPEC_VEH_MOD_MENU_BROWSE_STRUCT sMenuInfo
	SHOP_KEEPER_STRUCT sShopKeeper 	// The ped used to run the shop
	SPE_VEH_CAMERA_STRUCT sSpeVehModCamera
	SHOP_CAMERA_STRUCT	sCameras[MAX_CAMS_PER_SHOP] // The cameras we use for intros/outros and browsing
	MOD_COLOR_TYPE eCurrentModColType
	TIME_DATATYPE scrollDelay
	INT iCurrentCam 			// The current cam data we are using
	BOOL bUpdateHelpKeys, bShowBuyButton, bPreloadMods
	INT iTriggerContextID = -1
	INT iDpadPressed
	INT iTransactionState = 0
	BOOL bHasCrewEmblem
	INT iLocalBS
	CONTROL_ACTION	caZoomInput	= INPUT_FRONTEND_LT // Input used for zooming - different on PC.
ENDSTRUCT

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ CONST  ╞═════════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

CONST_INT	CONTROL_UP_PRESSED		0
CONST_INT	CONTROL_DOWN_PRESSED	1

// iLocalBS
CONST_INT SPE_VEH_LOCAL_BS_CAB_MENU_SELECTED				0
CONST_INT SPE_VEH_LOCAL_BS_TRAILER_MENU_SELECTED			1
CONST_INT SPE_VEH_LOCAL_BS_MOVED_FROM_VEH_SELECTION_MENU	2

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PROC/FUNC  ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC STRING DEBUG_GET_SPEC_VEH_MOD_SHOP_NAME(SPEC_VEH_MOD_SHOP_NAME_ENUM eSpecVehModShopName)
	SWITCH eSpecVehModShopName
		CASE EMPTY_SPEC_VEH_MOD_SHOP 			RETURN "EMPTY_SPEC_VEH_MOD_SHOP" 
		CASE SPEC_VEH_MOD_ARMORY_TRUCK 			RETURN "SPEC_VEH_MOD_ARMORY_TRUCK" 
	ENDSWITCH
 	RETURN ""
ENDFUNC

FUNC STRING DEBUG_GET_SPEC_VEH_MOD_MENU_NAME(SPEC_VEH_MOD_MENU_NAME eMenuName)
	SWITCH eMenuName
		CASE SVM_MAIN_MENU 			RETURN "SVM_MAIN_MENU" 
		CASE SVM_ARMOUR_MENU 		RETURN "SVM_ARMOUR_MENU" 
		CASE SVM_RESPRAY_MENU		RETURN "SVM_RESPRAY_MENU"	
		CASE SVM_ENGINE_MENU		RETURN "SVM_ENGINE_MENU"	
		CASE SVM_LIVERY_MENU		RETURN "SVM_LIVERY_MENU"
		CASE SVM_TURRET_MENU		RETURN "SVM_TURRET_MENU"
		CASE SVM_TRANSMISSION_MENU	RETURN "SVM_TRANSMISSION_MENU"
		CASE SVM_TURBO_MENU			RETURN "SVM_TURBO_MENU"
		CASE SVM_VEH_SELECTION		RETURN "SVM_VEH_SELECTION"
		CASE SVM_TYRE_MENU			RETURN "SVM_TYRE_MENU"
	ENDSWITCH
 	RETURN ""
ENDFUNC

PROC PRINT_SPEC_VEH_MOD_DEBUG_LINE(STRING sDebugStr)
	IF IS_STRING_NULL_OR_EMPTY(sDebugStr)
		EXIT
	ENDIF
	
	PRINTLN(GET_THIS_SCRIPT_NAME(), ": ", sDebugStr)
ENDPROC

FUNC BOOL IS_THIS_MOD_SHOP_ALLOW_PLAYER_VEHICLE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF GET_PLAYERS_GUNRUNNING_TRUCK_MODEL(PLAYER_ID()) = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			RETURN TRUE	
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC INT GET_SPEC_VEH_MOD_MENU_DISPLAY_INDEX(SPEC_VEH_MOD_MENU_NAME eMenu)
	INT iMenuIndex = -1
	
	SWITCH eMenu
		CASE SVM_VEH_SELECTION 		iMenuIndex = 0		BREAK
		CASE SVM_MAIN_MENU			iMenuIndex = 1		BREAK
		CASE SVM_ARMOUR_MENU 		iMenuIndex = 2		BREAK
		CASE SVM_ENGINE_MENU		iMenuIndex = 3		BREAK
		CASE SVM_LIVERY_MENU		iMenuIndex = 4		BREAK
		CASE SVM_RESPRAY_MENU		iMenuIndex = 5		BREAK
		CASE SVM_TURRET_MENU		iMenuIndex = 6		BREAK
		CASE SVM_TYRE_MENU			iMenuIndex = 7		BREAK
	ENDSWITCH
	
	RETURN iMenuIndex
ENDFUNC

FUNC SPEC_VEH_MOD_MENU_NAME GET_SPEC_VEH_MOD_MAIN_MENU_ITEM_FROM_DISPLAY_INDEX(INT iMenuIndex)

	SWITCH iMenuIndex
		CASE  0	RETURN SVM_VEH_SELECTION
		CASE  1	RETURN SVM_MAIN_MENU
		CASE  2	RETURN SVM_ARMOUR_MENU
		CASE  3	RETURN SVM_ENGINE_MENU
		CASE  4	RETURN SVM_LIVERY_MENU
		CASE  5	RETURN SVM_RESPRAY_MENU
		CASE  6	RETURN SVM_TURRET_MENU
		CASE  7 RETURN SVM_TYRE_MENU
	ENDSWITCH
	
	RETURN SVM_INVALID_MENU
ENDFUNC

FUNC SPEC_VEH_MOD_MENU_NAME GET_SPEC_VEH_MOD_MENU_FROM_DISPLAY_INDEX(INT iMenuIndex)

	INT iMenu
	FOR iMenu = 0 TO MAX_MENU_ROWS-1 
		IF GET_SPEC_VEH_MOD_MENU_DISPLAY_INDEX(INT_TO_ENUM(SPEC_VEH_MOD_MENU_NAME, iMenu)) = iMenuIndex
			RETURN INT_TO_ENUM(SPEC_VEH_MOD_MENU_NAME, iMenu)
		ENDIF
	ENDFOR
	
	RETURN SVM_MAIN_MENU
ENDFUNC

PROC CAP_SPEC_VEH_MOD_MENU_OPTIONS(INT &iOptionCount)
	IF iOptionCount > MAX_MENU_OPTIONS-1
		iOptionCount = MAX_MENU_OPTIONS-1
	ENDIF
ENDPROC

FUNC SPEC_VEH_MOD_MENU_NAME GET_SPEC_VEH_MENU_NAME_FOR_MOD_SLOT(MOD_TYPE eModType, VEHICLE_INDEX vehIndex)
	
	IF HAS_VEHICLE_GOT_MOD(vehIndex, eModType)
			
		SWITCH eModType
			CASE MOD_ARMOUR							RETURN SVM_ARMOUR_MENU
			CASE MOD_ENGINE							RETURN SVM_ENGINE_MENU
			CASE MOD_LIVERY							RETURN SVM_LIVERY_MENU
			CASE MOD_ROOF							RETURN SVM_TURRET_MENU
		ENDSWITCH
		
	ENDIF
	
	RETURN SVM_MAIN_MENU
ENDFUNC

PROC CLEAR_SPEC_VEH_MOD_MENU_OPTIONS(SPEC_VEH_MOD_STRUCT &sSpecVehModData)
	INT i
	REPEAT COUNT_OF(sSpecVehModData.sMenuInfo.iMenuOptions) i
		sSpecVehModData.sMenuInfo.iMenuOptions[i] = 0
	ENDREPEAT
	
	REPEAT COUNT_OF(sSpecVehModData.sMenuInfo.iOptionCost) i
		sSpecVehModData.sMenuInfo.iOptionCost[i] = 0
	ENDREPEAT
	
	REPEAT COUNT_OF(g_sShopSettings.tlMenuItem_label) i
		g_sShopSettings.tlMenuItem_label[i] = ""
	ENDREPEAT
ENDPROC

PROC SET_SPEC_VEH_MOD_MENU_OPTION_AVAILABLE(SPEC_VEH_MOD_STRUCT &sSpecVehModData, INT iOption, BOOL bAvailable = TRUE)
	IF bAvailable
		SET_BIT(sSpecVehModData.sMenuInfo.iMenuOptions[(iOption/32)], (iOption%32))
	ELSE
		CLEAR_BIT(sSpecVehModData.sMenuInfo.iMenuOptions[(iOption/32)], (iOption%32))
	ENDIF
ENDPROC

FUNC BOOL IS_SPEC_VEH_MOD_MENU_OPTION_AVAILABLE(SPEC_VEH_MOD_STRUCT &sSpecVehModData, INT iOption)
	RETURN IS_BIT_SET(sSpecVehModData.sMenuInfo.iMenuOptions[(iOption/32)], (iOption%32))
ENDFUNC

/// PURPOSE: Checks to see if the mod item has a text label defined by vehicle artists
FUNC BOOL SETUP_SPEC_VEH_MOD_ITEM_LABEL(SPEC_VEH_MOD_STRUCT &sSpecVehModData, TEXT_LABEL_15 &sRetLabel, MOD_TYPE eModType, INT iIndex)
	
	IF DOES_ENTITY_EXIST(sSpecVehModData.specVehMod)
	AND IS_VEHICLE_DRIVEABLE(sSpecVehModData.specVehMod)	
	
		IF eModType = MOD_HORN
			sRetLabel = GET_HORN_LABEL_FROM_MOD_INDEX(iIndex)
			RETURN (NOT IS_STRING_NULL_OR_EMPTY(sRetLabel))
		ELSE

			iIndex--

			IF iIndex >= 0
				sRetLabel = GET_MOD_TEXT_LABEL(sSpecVehModData.specVehMod, eModType, iIndex)
				IF GET_HASH_KEY(sRetLabel) != 0
					RETURN TRUE
				ENDIF		
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_NUMBER_OF_ITEM_IN_CURRENT_VEH_MOD_MENU(SPEC_VEH_MOD_STRUCT &sSpecVehModData)
	INT iCount, iTemp
	REPEAT MAX_MENU_OPTIONS iTemp
		IF IS_SPEC_VEH_MOD_MENU_OPTION_AVAILABLE(sSpecVehModData, iTemp)
			iCount++
		ENDIF
	ENDREPEAT
	RETURN iCount
ENDFUNC

FUNC INT GET_SPEC_VEH_MENU_ITEM_INDEX(MODEL_NAMES eVehModel, SPEC_VEH_MOD_MENU_NAME eMenu , INT iOption)
	INT iOffset = 0
	
	IF eVehModel = TRAILERLARGE
		IF (eMenu = SVM_ARMOUR_MENU  	AND iOption >= 1 AND iOption <= 6)	RETURN iOffset+iOption ENDIF 
		iOffset += 7
		
		IF (eMenu = SVM_ENGINE_MENU  	AND iOption >= 1 AND iOption <= 5)	RETURN iOffset+iOption ENDIF 
		iOffset += 6
		
		IF (eMenu = SVM_TURRET_MENU  	AND iOption >= 1 AND iOption <= 2)	RETURN iOffset+iOption ENDIF 
		iOffset += 3
	ENDIF
	
	RETURN 0
ENDFUNC

PROC GET_VEHICLE_STAT_NAME_FOR_SPEC_VEH_MOD(MODEL_NAMES eVehModel, MP_INT_STATS& OutStatName)
	SWITCH eVehModel
		CASE TRAILERLARGE
			OutStatName = MP_STAT_TRUCK_MOD_STAR
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_MENU_ITEM_STAR_STAT_SET(MP_INT_STATS statName, INT iStarItemIndex)
	
	INT iBitSet = GET_MP_INT_CHARACTER_STAT(statName)
	
	IF iStarItemIndex >-1 AND iStarItemIndex < 32
		PRINTLN("IS_MENU_ITEM_STAR_STAT_SET - IS_BIT_SET(iBitSet, iStarItemIndex): ", IS_BIT_SET(iBitSet, iStarItemIndex), " iStarItemIndex: ", iStarItemIndex, " iBitSet: ", iBitSet )
		RETURN IS_BIT_SET(iBitSet, iStarItemIndex)
	ENDIF
	
	RETURN TRUE
ENDFUNC 

FUNC MOD_TYPE GET_SPEC_VEH_MOD_TYPE_FROM_MENU_NAME(SPEC_VEH_MOD_MENU_NAME eMenu)
	SWITCH eMenu
		CASE SVM_ARMOUR_MENU							RETURN  MOD_ARMOUR
		CASE SVM_ENGINE_MENU							RETURN  MOD_ENGINE
		CASE SVM_LIVERY_MENU							RETURN  MOD_LIVERY
		CASE SVM_TURRET_MENU							RETURN  MOD_ROOF
	ENDSWITCH
	
	RETURN INT_TO_ENUM(MOD_TYPE, -1)
ENDFUNC

FUNC INT GET_NUMBER_OF_ITEMS_IN_MENU(VEHICLE_INDEX specVehMod, SPEC_VEH_MOD_MENU_NAME eMenu)
	INT iNumItems
	
	IF GET_SPEC_VEH_MOD_TYPE_FROM_MENU_NAME(eMenu) != INT_TO_ENUM(MOD_TYPE, -1)
		iNumItems = GET_NUM_VEHICLE_MODS(specVehMod, GET_SPEC_VEH_MOD_TYPE_FROM_MENU_NAME(eMenu))
	ENDIF
	
	IF eMenu != SVM_MAIN_MENU 
	AND eMenu != SVM_VEH_SELECTION
		iNumItems += 1
	ENDIF	
	
	PRINTLN(GET_THIS_SCRIPT_NAME() ," GET_NUMBER_OF_ITEMS_IN_MENU: ", DEBUG_GET_SPEC_VEH_MOD_MENU_NAME(eMenu) , " iNumItems: ", iNumItems )
	
	RETURN iNumItems
ENDFUNC

FUNC BOOL SET_SPEC_VEH_ITEM_AS_VIWED(SPEC_VEH_MOD_STRUCT &sSpecVehModData,INT iOption)
	
	MODEL_NAMES eVehModel
	MP_INT_STATS statName
	
	IF DOES_ENTITY_EXIST(sSpecVehModData.specVehMod)
		eVehModel = GET_ENTITY_MODEL(sSpecVehModData.specVehMod)
	ENDIF
	
	GET_VEHICLE_STAT_NAME_FOR_SPEC_VEH_MOD(eVehModel,statName)
	
	INT iBitSet = GET_MP_INT_CHARACTER_STAT(statName)
	
	INT iStarItemIndex = GET_SPEC_VEH_MENU_ITEM_INDEX(eVehModel, sSpecVehModData.eMenu , iOption)
	
	IF iStarItemIndex >-1 AND iStarItemIndex < 32
		IF NOT IS_BIT_SET(iBitSet, iStarItemIndex)
			SET_BIT(iBitSet, iStarItemIndex)
			PRINTLN("SET_SPEC_VEH_ITEM_AS_VIWED - iOption: ", iOption, " iStarItemIndex: ", iStarItemIndex, " iBitSet: ", iBitSet )
			SET_MP_INT_CHARACTER_STAT(statName, iBitSet)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_SPEC_VEH_ITEM_BEEN_VIEWED(VEHICLE_INDEX specVehMod, SPEC_VEH_MOD_MENU_NAME eMenu, INT iOption)
	
	MODEL_NAMES eVehModel
	MP_INT_STATS statName
	IF DOES_ENTITY_EXIST(specVehMod)
		eVehModel = GET_ENTITY_MODEL(specVehMod)
	ENDIF
	
	IF eMenu = SVM_RESPRAY_MENU
	
		RETURN TRUE
		
	ELIF eMenu != SVM_MAIN_MENU
	AND eMenu != SVM_VEH_SELECTION
	
		INT iStarItemIndex = GET_SPEC_VEH_MENU_ITEM_INDEX(eVehModel, eMenu , iOption)

		GET_VEHICLE_STAT_NAME_FOR_SPEC_VEH_MOD(eVehModel,statName)
	
		RETURN IS_MENU_ITEM_STAR_STAT_SET(statName, iStarItemIndex)

	ELSE
	
		INT i

		FOR i = 0 TO GET_NUMBER_OF_ITEMS_IN_MENU(specVehMod, GET_SPEC_VEH_MOD_MAIN_MENU_ITEM_FROM_DISPLAY_INDEX(iOption)) - 1
			IF !HAS_SPEC_VEH_ITEM_BEEN_VIEWED(specVehMod, GET_SPEC_VEH_MOD_MAIN_MENU_ITEM_FROM_DISPLAY_INDEX(iOption), i)
				RETURN FALSE
			ENDIF
		ENDFOR

	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_SPEC_VEH_ALL_MENU_ITEMS_BEEN_VIEWED(VEHICLE_INDEX specVehMod, SPEC_VEH_MOD_MENU_NAME eMenu)
	INT i

	FOR i = 0 TO GET_NUMBER_OF_ITEMS_IN_MENU(specVehMod, eMenu)
		IF !HAS_SPEC_VEH_ITEM_BEEN_VIEWED(specVehMod, eMenu, i)
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

PROC CLEAR_SPEC_VEH_ITEM_VIEWED_STATS(SPEC_VEH_MOD_STRUCT &sSpecVehModData)
	
	MODEL_NAMES eVehModel
	MP_INT_STATS statName
	
	IF DOES_ENTITY_EXIST(sSpecVehModData.specVehMod)
		eVehModel = GET_ENTITY_MODEL(sSpecVehModData.specVehMod)
		GET_VEHICLE_STAT_NAME_FOR_SPEC_VEH_MOD(eVehModel,statName)
	
		INT iBitSet = GET_MP_INT_CHARACTER_STAT(statName)
		
		INT i 
		FOR i = 0 TO 31
			IF IS_BIT_SET(iBitSet, i)
				CLEAR_BIT(iBitSet, i)
			ENDIF
		ENDFOR	
		
		SET_MP_INT_CHARACTER_STAT(statName, iBitSet)
		
		PRINTLN("CLEAR_SPEC_VEH_ITEM_VIEWED_STATS - called..")
	ELSE
		SCRIPT_ASSERT("Get in vehicle and start modding first!")
		PRINTLN("CLEAR_SPEC_VEH_ITEM_VIEWED_STATS - called.. player not in mod shop")
	ENDIF
ENDPROC

PROC DISPLAY_SPEC_VEH_MENU_ITEM(SPEC_VEH_MOD_STRUCT &sSpecVehModData, INT iOption, TEXT_LABEL_15 &tlLabel, INT iSubComponents = 0)	
	UNUSED_PARAMETER(sSpecVehModData)

//	IF NOT HAS_SPEC_VEH_ITEM_BEEN_VIEWED(sSpecVehModData.specVehMod,sSpecVehModData.eMenu, iOption)
//	OR bForceShowStar
//		ADD_MENU_ITEM_TEXT(iOption, tlLabel, iSubComponents+1)
//		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
//	ELIF SHOULD_DISCOUNT_BE_DISPLAYED_FOR_CARMOD(sSpecVehModData.vehMod, tlLabel, sSpecVehModData.eMenu, sSpecVehModData.eModSlot, sSpecVehModData.sBrowseInfo.iControl, "DISPLAY_ITEM_WITH_UNLOCKED_CHECK", iSlot)
//		ADD_MENU_ITEM_TEXT(iSlot, tlLabel, iSubComponents+1)
//		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
//		
//		IF (iSlot = sSpecVehModData.sBrowseInfo.iCurrentItem)
//			bDiscountsPresent = TRUE
//			bShowDiscountInfo = TRUE
//		ENDIF
//	ELSE
		ADD_MENU_ITEM_TEXT(iOption, tlLabel, iSubComponents)
//	ENDIF
ENDPROC

PROC DISPLAY_SPEC_VEH_MENU_PRICE(SPEC_VEH_MOD_STRUCT &sSpecVehModData, INT iSlot)
//	IF IS_CURRENT_CARMOD_MENU_OPTION_PURCHASED(iSlot)
//		ADD_MENU_ITEM_TEXT(iSlot, "", 1)
//		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_TICK)
//		
//		IF (iSlot = sSpecVehModData.sBrowseInfo.iCurrentItem)
//			bShowBuyButton = FALSE
//			bUpdateHelpKeys = TRUE
//		ENDIF
//		
	IF sSpecVehModData.sMenuInfo.iOptionCost[iSlot] = 0
		ADD_MENU_ITEM_TEXT(iSlot, "ITEM_FREE")
		IF (iSlot = sSpecVehModData.sMenuInfo.iCurrentItem)
			//bShowBuyButton = TRUE
			sSpecVehModData.bUpdateHelpKeys = TRUE
		ENDIF
	ELSE
		ADD_MENU_ITEM_TEXT(iSlot, "ITEM_COST", 1)
		ADD_MENU_ITEM_TEXT_COMPONENT_INT(sSpecVehModData.sMenuInfo.iOptionCost[iSlot])
	ENDIF
ENDPROC

PROC DISPLAY_PRICE_OR_TICK_FOR_SPEC_VEH_MOD_ITEM(SPEC_VEH_MOD_STRUCT &sSpecVehModData, INT iSlot)
//	IF IS_CURRENT_CARMOD_MENU_OPTION_PURCHASED(iSlot)
//		ADD_MENU_ITEM_TEXT(iSlot, "", 1)
//		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_TICK)
//		
//		IF (iSlot = sData.sBrowseInfo.iCurrentItem)
//			bShowBuyButton = FALSE
//			bUpdateHelpKeys = TRUE
//		ENDIF
//		
	IF sSpecVehModData.sMenuInfo.iOptionCost[iSlot] = 0
		ADD_MENU_ITEM_TEXT(iSlot, "ITEM_FREE")
		IF (iSlot = sSpecVehModData.sMenuInfo.iCurrentItem)
			sSpecVehModData.bShowBuyButton = TRUE
			sSpecVehModData.bUpdateHelpKeys = TRUE
		ENDIF
	ELSE
		ADD_MENU_ITEM_TEXT(iSlot, "ITEM_COST", 1)
		ADD_MENU_ITEM_TEXT_COMPONENT_INT(sSpecVehModData.sMenuInfo.iOptionCost[iSlot])
	ENDIF
ENDPROC

PROC DISPLAY_SPEC_VEH_MOD_FITTED_ICON_UPDATE_CURRENT_MOD_INDEX(SPEC_VEH_MOD_STRUCT &sSpecVehModData, INT iSlot, INT &iCurrentMod)
	ADD_MENU_ITEM_TEXT(iSlot, "", 1)
	IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(sSpecVehModData.specVehMod))
		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_BIKE)
	ELSE
		ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_CAR)
	ENDIF
	//SET_CURRENT_CARMOD_MENU_OPTION_PURCHASED(iSlot)
	IF iCurrentMod = -1
		iCurrentMod = iSlot
	ENDIF
ENDPROC

// Only preload a few mods.
FUNC BOOL IS_SPEC_VEH_MOD_SAFE_TO_PRELOAD(VEHICLE_INDEX vehID, MOD_TYPE eMod)
	IF eMod = MOD_BUMPER_F
	OR eMod = MOD_BUMPER_R
	OR eMod = MOD_SKIRT
	OR eMod = MOD_EXHAUST
	OR eMod = MOD_CHASSIS
	OR eMod = MOD_GRILL
	OR eMod = MOD_BONNET
	OR eMod = MOD_WING_L
	OR eMod = MOD_WING_R
	OR eMod = MOD_ROOF
	OR eMod = MOD_SPOILER
	OR eMod = MOD_WHEELS
	
	OR eMod = MOD_PLTHOLDER
	OR eMod = MOD_PLTVANITY
	OR eMod = MOD_INTERIOR1
	OR eMod = MOD_INTERIOR2
	OR eMod = MOD_INTERIOR3
	OR eMod = MOD_INTERIOR4
	OR eMod = MOD_INTERIOR5
	OR eMod = MOD_SEATS
	OR eMod = MOD_STEERING
	OR eMod = MOD_KNOB
	OR eMod = MOD_PLAQUE
	OR eMod = MOD_ICE
	OR eMod = MOD_TRUNK
	OR eMod = MOD_HYDRO
	OR eMod = MOD_ENGINEBAY1
	OR eMod = MOD_ENGINEBAY2
	OR eMod = MOD_ENGINEBAY3
	OR eMod = MOD_CHASSIS2
	OR eMod = MOD_CHASSIS3
	OR eMod = MOD_CHASSIS4
	OR eMod = MOD_CHASSIS5
	OR eMod = MOD_DOOR_L
	OR eMod = MOD_DOOR_R
	
		RETURN TRUE
	ELIF eMod = MOD_REAR_WHEELS
		IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehID))
		RETURN TRUE
	ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

///////////////////////////////////////////////////////////////
///    
///    NAVIGATION PROCS
///    
/// PURPOSE: Menu navigation
FUNC BOOL GET_FIRST_MENU_ITEM(SPEC_VEH_MOD_STRUCT &sSpecVehModData)


	// Something broke with the menu depth so set it to 0.
	IF sSpecVehModData.sMenuInfo.iMenuDepth < 0
		sSpecVehModData.sMenuInfo.iMenuDepth = 0
	ENDIF

	SET_TOP_MENU_ITEM(sSpecVehModData.sMenuInfo.iTopOption[sSpecVehModData.sMenuInfo.iMenuDepth])
	
	IF sSpecVehModData.sMenuInfo.iMenuDepth > -1
		IF sSpecVehModData.sMenuInfo.iMenuOption[sSpecVehModData.sMenuInfo.iMenuDepth] > -1 
			IF IS_SPEC_VEH_MOD_MENU_OPTION_AVAILABLE(sSpecVehModData, sSpecVehModData.sMenuInfo.iMenuOption[sSpecVehModData.sMenuInfo.iMenuDepth])
				SET_CURRENT_MENU_ITEM(sSpecVehModData.sMenuInfo.iMenuOption[sSpecVehModData.sMenuInfo.iMenuDepth], FALSE)
				//SET_CURRENT_MENU_ITEM_DESCRIPTION(sSpecVehModData.sBrowseInfo.sInfoLabel, 4000)	
				//sSpecVehModData.sInfoLabel = ""
				sSpecVehModData.sMenuInfo.bRestoreCurrentItem = FALSE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	
	sSpecVehModData.sMenuInfo.bRestoreCurrentItem = FALSE
	
	// Clear tracker for next menu
	IF sSpecVehModData.sMenuInfo.iMenuDepth < MAX_MENU_DEPTHS-1
		sSpecVehModData.sMenuInfo.iMenuOption[sSpecVehModData.sMenuInfo.iMenuDepth+1] = -1
		sSpecVehModData.sMenuInfo.iTopOption[sSpecVehModData.sMenuInfo.iMenuDepth+1] = 0
	ENDIF
	
	// Restore previous selection
	IF sSpecVehModData.sMenuInfo.iMenuOption[sSpecVehModData.sMenuInfo.iMenuDepth] > -1
	 	IF IS_SPEC_VEH_MOD_MENU_OPTION_AVAILABLE(sSpecVehModData, sSpecVehModData.sMenuInfo.iMenuOption[sSpecVehModData.sMenuInfo.iMenuDepth])
			sSpecVehModData.sMenuInfo.iCurrentItem = sSpecVehModData.sMenuInfo.iMenuOption[sSpecVehModData.sMenuInfo.iMenuDepth]
			SET_TOP_MENU_ITEM(sSpecVehModData.sMenuInfo.iTopOption[sSpecVehModData.sMenuInfo.iMenuDepth])
			SET_CURRENT_MENU_ITEM(sSpecVehModData.sMenuInfo.iCurrentItem)
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Find next set bit
	INT iTemp = 0
	WHILE iTemp < MAX_MENU_OPTIONS
		IF IS_SPEC_VEH_MOD_MENU_OPTION_AVAILABLE(sSpecVehModData, iTemp)
			sSpecVehModData.sMenuInfo.iCurrentItem = iTemp
			SET_CURRENT_MENU_ITEM(sSpecVehModData.sMenuInfo.iCurrentItem)
			sSpecVehModData.sMenuInfo.iMenuOption[sSpecVehModData.sMenuInfo.iMenuDepth] = iTemp // tracker
			sSpecVehModData.sMenuInfo.iTopOption[sSpecVehModData.sMenuInfo.iMenuDepth] = GET_TOP_MENU_ITEM() // tracker
			RETURN TRUE
		ENDIF
		iTemp++
	ENDWHILE
	
	// No available items
	sSpecVehModData.sMenuInfo.iCurrentItem = -1
	sSpecVehModData.sMenuInfo.iMenuOption[sSpecVehModData.sMenuInfo.iMenuDepth] = -1
	sSpecVehModData.sMenuInfo.iTopOption[sSpecVehModData.sMenuInfo.iMenuDepth] = 0
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Menu navigation
FUNC BOOL GET_NEXT_MENU_ITEM(SPEC_VEH_MOD_STRUCT &sSpecVehModData, BOOL bRecursiveLoop = FALSE)
	
	INT iTemp = sSpecVehModData.sMenuInfo.iCurrentItem+1
	IF bRecursiveLoop
		iTemp = 0
	ENDIF
	WHILE iTemp < MAX_MENU_OPTIONS
		IF IS_SPEC_VEH_MOD_MENU_OPTION_AVAILABLE(sSpecVehModData, iTemp)
			sSpecVehModData.sMenuInfo.iCurrentItem = iTemp
			SET_CURRENT_MENU_ITEM(sSpecVehModData.sMenuInfo.iCurrentItem)
			sSpecVehModData.sMenuInfo.iMenuOption[sSpecVehModData.sMenuInfo.iMenuDepth] = iTemp // tracker
			sSpecVehModData.sMenuInfo.iTopOption[sSpecVehModData.sMenuInfo.iMenuDepth] = GET_TOP_MENU_ITEM() // tracker
			sSpecVehModData.sMenuInfo.bRestoreCurrentItem = TRUE
			RETURN TRUE
		ENDIF
		iTemp++
	ENDWHILE
	
	IF NOT bRecursiveLoop
		RETURN GET_NEXT_MENU_ITEM(sSpecVehModData, TRUE)
	ENDIF
	
	RETURN FALSE
ENDFUNC
/// PURPOSE: Menu navigation
FUNC BOOL GET_PREVIOUS_MENU_ITEM(SPEC_VEH_MOD_STRUCT &sSpecVehModData, BOOL bRecursiveLoop = FALSE)
	
	INT iTemp = sSpecVehModData.sMenuInfo.iCurrentItem-1
	IF bRecursiveLoop
		iTemp = MAX_MENU_OPTIONS-1
	ENDIF
	WHILE iTemp >= 0
		IF IS_SPEC_VEH_MOD_MENU_OPTION_AVAILABLE(sSpecVehModData, iTemp)
			sSpecVehModData.sMenuInfo.iCurrentItem = iTemp
			SET_CURRENT_MENU_ITEM(sSpecVehModData.sMenuInfo.iCurrentItem)
			sSpecVehModData.sMenuInfo.iMenuOption[sSpecVehModData.sMenuInfo.iMenuDepth] = iTemp // tracker
			sSpecVehModData.sMenuInfo.iTopOption[sSpecVehModData.sMenuInfo.iMenuDepth] = GET_TOP_MENU_ITEM() // tracker
			sSpecVehModData.sMenuInfo.bRestoreCurrentItem = TRUE
			RETURN TRUE
		ENDIF
		iTemp--
	ENDWHILE
	
	IF NOT bRecursiveLoop
		RETURN GET_PREVIOUS_MENU_ITEM(sSpecVehModData, TRUE)
	ENDIF
	
	RETURN FALSE
ENDFUNC
/// PURPOSE: Menu navigation
FUNC BOOL GET_THIS_MENU_ITEM(SPEC_VEH_MOD_STRUCT &sSpecVehModData, INT iMenuItem)

	IF iMenuItem = -1
		RETURN FALSE
	ENDIF
	
	IF IS_SPEC_VEH_MOD_MENU_OPTION_AVAILABLE(sSpecVehModData, iMenuItem)
		sSpecVehModData.sMenuInfo.iCurrentItem = iMenuItem
		SET_CURRENT_MENU_ITEM(sSpecVehModData.sMenuInfo.iCurrentItem)
		sSpecVehModData.sMenuInfo.iMenuOption[sSpecVehModData.sMenuInfo.iMenuDepth] = iMenuItem // tracker
		sSpecVehModData.sMenuInfo.iTopOption[sSpecVehModData.sMenuInfo.iMenuDepth] = GET_TOP_MENU_ITEM() // tracker
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_SPEC_VEH_MENU_TITLE(SPEC_VEH_MOD_MENU_NAME eMenuName, INT iControl, BOOL bIsCabModification = FALSE)
	SWITCH eMenuName
		CASE SVM_MAIN_MENU 			
			IF bIsCabModification
				RETURN "SVM_VS_CAB_T" 
			ELSE
				RETURN "SVM_VS_TRI_T"
			ENDIF
		BREAK	
		CASE SVM_VEH_SELECTION		RETURN "SVM_MOD_VS_T"
		CASE SVM_ARMOUR_MENU 		RETURN "CMOD_ARM_T" 
		CASE SVM_RESPRAY_MENU		
			SWITCH iControl
				CASE 0 				RETURN "CMOD_COL0_T"	
				CASE 1 				RETURN "CMOD_COL1_T"	
				CASE 2				RETURN "CMOD_COL2_T"
				CASE 3				RETURN "CMOD_COL3_T"
				CASE 4				RETURN "CMOD_COL4_T"
			ENDSWITCH	
		BREAK	
		CASE SVM_ENGINE_MENU		RETURN "CMOD_ENG_T"
		CASE SVM_LIVERY_MENU		RETURN "CMM_MOD_ST23"
		CASE SVM_TURRET_MENU		RETURN "SVM_MOD_TU_T"
		CASE SVM_TYRE_MENU			RETURN "CMOD_TYR2_T"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_SPEC_VEH_MENU_NAME(SPEC_VEH_MOD_MENU_NAME eMenuName, INT iControl)
	SWITCH eMenuName
		CASE SVM_MAIN_MENU 			RETURN "CMOD_MOD_T" 
		CASE SVM_ARMOUR_MENU 		RETURN "CMOD_MOD_ARM" 
		CASE SVM_RESPRAY_MENU		
			SWITCH iControl
				CASE 0				RETURN "CMOD_MOD_COL"	
				CASE 1				RETURN ""	
			ENDSWITCH
		BREAK	
		CASE SVM_ENGINE_MENU		RETURN "CMOD_MOD_ENG"
		CASE SVM_LIVERY_MENU		RETURN "CMOD_COL0_4"
		CASE SVM_TURRET_MENU		RETURN "SVM_MOD_TU_N"
		CASE SVM_TYRE_MENU			RETURN "CMOD_MOD_TYR2" 
	ENDSWITCH
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Get first menu item name
FUNC STRING GET_SPEC_VEH_MENU_FIRST_ITEM_STOCK_NAME(SPEC_VEH_MOD_MENU_NAME eMenuName)
	SWITCH eMenuName
		CASE SVM_TURRET_MENU		RETURN "SVM_MOD_T_1"
		CASE SVM_ENGINE_MENU		RETURN "CMOD_ENG_1"
		CASE SVM_VEH_SELECTION		RETURN "SVM_VS_CAB_N"
		CASE SVM_TYRE_MENU			RETURN "CMOD_TYR_2"
	ENDSWITCH
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Get second menu item name
FUNC STRING GET_SPEC_VEH_MENU_CUSTOM_ITEM_STOCK_NAME(SPEC_VEH_MOD_MENU_NAME eMenuName)
	SWITCH eMenuName
		CASE SVM_TURRET_MENU		RETURN "SVM_MOD_T_2"
		CASE SVM_ENGINE_MENU		RETURN "CMOD_ENG_1"
		CASE SVM_VEH_SELECTION		RETURN "SVM_VS_TRI_N"
	ENDSWITCH
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Get third + menu item name
FUNC STRING GET_SPEC_VEH_MENU_CUSTOM_TWO_ITEM_STOCK_NAME(SPEC_VEH_MOD_MENU_NAME eMenuName)
	SWITCH eMenuName
		CASE SVM_TURRET_MENU		RETURN "SVM_MOD_NONE"
		CASE SVM_ENGINE_MENU		RETURN "CMOD_ENG_2"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC BOOL SET_SPEC_VEH_DEFAULT_Z_OFFSET_RELATIVE_TO_CAR(VEHICLE_INDEX mVeh, FLOAT h, FLOAT &off)
	FLOAT z
	GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(mVeh), z)
	VECTOR bonePos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mVeh, <<0, 0, h>>)
	off = bonePos.z - z
	RETURN TRUE
ENDFUNC


FUNC BOOL SET_SPEC_VEH_DEFAULT_Z_OFFSET_FROM_BONE_NAME(VEHICLE_INDEX mVeh, STRING sBoneName, FLOAT &off)
	FLOAT z
	INT boneIndex
	VECTOR bonePos
	GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(mVeh), z)
	boneIndex = GET_ENTITY_BONE_INDEX_BY_NAME(mVeh, sBoneName)
	IF (boneIndex = -1)
		bonePos = GET_ENTITY_COORDS(mVeh)
		off = (bonePos.z - z) + 0.3
		RETURN FALSE
	ELSE
		bonePos = GET_WORLD_POSITION_OF_ENTITY_BONE(mVeh, boneIndex)
	ENDIF
	
	off = (bonePos.z - z) + 0.3
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///   	Tells what side of a plane a point is on
/// PARAMS:
///     pNorm - plane normal
///    	pOrg - plane origin
///    	v - point we want to check
///	RETURNS:
///     Returns -1 if behind plane, +1 if in front and 0 if on plane
FUNC BOOL IS_POINT_IN_FRONT_PLANE(VECTOR pNorm, VECTOR pOrg, VECTOR v)
	FLOAT dot = DOT_PRODUCT(v - pOrg, pNorm)
	RETURN (dot >= 0)
ENDFUNC

FUNC BOOL USE_SPEC_VEH_REAR_CAM_FOR_CAR_PART_BONE_NAME(VEHICLE_INDEX mVeh, STRING sBoneName)
	INT boneIndex
	
	boneIndex = GET_ENTITY_BONE_INDEX_BY_NAME(mVeh, sBoneName)
	IF (boneIndex = -1)
		RETURN FALSE
	ENDIF
	
	VECTOR bonePos = GET_WORLD_POSITION_OF_ENTITY_BONE(mVeh, boneIndex)
	RETURN NOT IS_POINT_IN_FRONT_PLANE(GET_ENTITY_FORWARD_VECTOR(mVeh), GET_ENTITY_COORDS(mVeh), bonePos)
ENDFUNC

PROC UPDATE_SPEC_VEH_MOD_MENU_KEYS(SPEC_VEH_MOD_MENU_INPUT &sMenuInput, INT &iMenuCurrentItem)

	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(sMenuInput.iLeftX,sMenuInput.iLeftY,sMenuInput.iRightX,sMenuInput.iRightY )
	
	// Set the input flag states
	sMenuInput.bAccept			= (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
	sMenuInput.bBack		 	= (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
	sMenuInput.bPrevious		= ((sMenuInput.iLeftY < -64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)) 
	sMenuInput.bNext			= ((sMenuInput.iLeftY > 64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN))	
	
	RESET_MOUSE_POINTER_FOR_PAUSE_OR_ALERT()
	
	IF LOAD_MENU_ASSETS()
	AND GET_PAUSE_MENU_STATE() = PM_INACTIVE
	AND NOT IS_SYSTEM_UI_BEING_DISPLAYED()
	AND NOT IS_WARNING_MESSAGE_ACTIVE()
	AND NOT g_sShopSettings.bProcessStoreAlert

		// Mouse control support  
		IF IS_PC_VERSION()	
		AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
			IF IS_USING_CURSOR(FRONTEND_CONTROL)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)	
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				IF ((g_iMenuCursorItem = MENU_CURSOR_NO_ITEM) OR (g_iMenuCursorItem = MENU_CURSOR_NO_CAMERA_MOVE) OR (g_iMenuCursorItem = MENU_CURSOR_DRAG_CAM))
					ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
					ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
				ELSE
					DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
					DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
				ENDIF	
				HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
				HANDLE_MENU_CURSOR(FALSE)
			ENDIF
			
			// Mouse select
			IF IS_MENU_CURSOR_ACCEPT_PRESSED()
				IF g_iMenuCursorItem = iMenuCurrentItem
					sMenuInput.bAccept = TRUE
				ELSE
					iMenuCurrentItem = g_iMenuCursorItem
					SET_CURRENT_MENU_ITEM(iMenuCurrentItem)		
				ENDIF		
			ENDIF
		
			// Menu cursor back
			IF IS_MENU_CURSOR_CANCEL_PRESSED()
				sMenuInput.bBack = TRUE
			ENDIF
			
			// Mouse scroll up
			IF IS_MENU_CURSOR_SCROLL_UP_PRESSED()
				sMenuInput.bPrevious = TRUE
			ENDIF
			
			// Mouse scroll down
			IF IS_MENU_CURSOR_SCROLL_DOWN_PRESSED()
				sMenuInput.bNext = TRUE
			ENDIF
			
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Manage arrow click for vault weapon menu
PROC MANAGE_SPEC_VEH_MOD_MENU_ARROW_CLICK(SPEC_VEH_MOD_STRUCT &sSpecVehModData)
	
	IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		sSpecVehModData.bUpdateHelpKeys = TRUE
	ENDIF
	
	// PC uses a different frontend input for zooming
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		sSpecVehModData.caZoomInput = INPUT_FRONTEND_LS
	ELSE
		sSpecVehModData.caZoomInput = INPUT_FRONTEND_LT
	ENDIF
	
	sSpecVehModData.sInputData.iCursorValue = 0
	
	IF sSpecVehModData.eMenu != SVM_MAIN_MENU
		// Cursor is inside the menu
		IF g_iMenuCursorItem > MENU_CURSOR_NO_ITEM	
			// Clicking on an item
			IF sSpecVehModData.sInputData.bAccept 
				// Clicking on currently selected item
				IF g_iMenuCursorItem = sSpecVehModData.sMenuInfo.iCurrentItem
					// Item is a < item > style 
					IF g_iMenuCursorItem = sSpecVehModData.sMenuInfo.iCurrentItem
						sSpecVehModData.sInputData.iCursorValue = GET_CURSOR_MENU_ITEM_VALUE_CHANGE()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
ENDPROC

/// PURPOSE: Takes control away from player
PROC LOCK_PLAYER_FOR_SPEC_VEH_BROWSE()

	CLEAR_PED_TASKS(PLAYER_PED_ID())
	SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
	
	SET_TEMP_PASSIVE_MODE()
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE)

	DISABLE_ALL_MP_HUD()
	
	PRINT_SPEC_VEH_MOD_DEBUG_LINE("LOCK_PLAYER_FOR_SPEC_VEH_BROWSE")
ENDPROC

PROC UNLOCK_PLAYER_FOR_SPEC_VEH_BROWSE(BOOL bResetGameplayCam = TRUE, BOOL bTurnOffScriptCams = TRUE, BOOL bEnableMpPlayerControl = TRUE, BOOL bClearPedTasks = TRUE)
	
	CLEANUP_TEMP_PASSIVE_MODE()

	IF bEnableMpPlayerControl
		IF NOT IS_SKYSWOOP_AT_GROUND()
			// do nothing
		ELIF NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_MP()
		AND NOT SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
		AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
			IF !bClearPedTasks
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, NSPC_DONT_CLEAR_TASKS_ON_RESUME_CONTROL)
			ELSE
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
		ELSE
			IF !bClearPedTasks
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE | NSPC_DONT_CLEAR_TASKS_ON_RESUME_CONTROL)
			ELSE
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE)
			ENDIF
		ENDIF
	ENDIF

	TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	

	IF bTurnOffScriptCams
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDIF
	
	IF bResetGameplayCam
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)	
	ENDIF

	ENABLE_ALL_MP_HUD()
	Unpause_Objective_Text()
	
	PRINT_SPEC_VEH_MOD_DEBUG_LINE("UNLOCK_PLAYER_FOR_SPEC_VEH_BROWSE")
ENDPROC

FUNC STRING GET_SPEC_VEH_SHOP_LOGO_TEXTURE(SPEC_VEH_MOD_SHOP_NAME_ENUM eShop)
	SWITCH eShop
		CASE SPEC_VEH_MOD_ARMORY_TRUCK
			RETURN "ShopUI_Title_IE_ModGarage"
		BREAK	
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC BOOL LOAD_SPEC_VEH_MOD_MENU_LOGO(SPEC_VEH_MOD_SHOP_NAME_ENUM eSpecVehModShopName)

	TEXT_LABEL_63 tlShopLogo = GET_SPEC_VEH_SHOP_LOGO_TEXTURE(eSpecVehModShopName)
	
	IF GET_HASH_KEY(tlShopLogo) != 0
		REQUEST_STREAMED_TEXTURE_DICT(tlShopLogo)
		RETURN HAS_STREAMED_TEXTURE_DICT_LOADED(tlShopLogo)
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC STRING DEBUG_GET_SPEC_VEH_MOD_STAGE(SPEC_VEH_MOD_STAGE_ENUM eStage)
	SWITCH eStage
		CASE SPEC_VEH_MOD_STAGE_INITIALISE 			RETURN "SPEC_VEH_MOD_STAGE_INITIALISE"
		CASE SPEC_VEH_MOD_STAGE_WAIT_FOR_TRIGGER 	RETURN "SPEC_VEH_MOD_STAGE_WAIT_FOR_TRIGGER"
		CASE SPEC_VEH_MOD_STAGE_BROWSE_ITEMS 		RETURN "SPEC_VEH_MOD_STAGE_BROWSE_ITEMS" 
		CASE SPEC_VEH_MOD_STAGE_BAIL 				RETURN "SPEC_VEH_MOD_STAGE_BAIL" 
	ENDSWITCH
 	RETURN ""
ENDFUNC

FUNC BOOL SETUP_SPEC_VEH_CAM(MOD_SHOP_CAM_ENUM eCam, MOD_SHOP_CAM_ENUM &RetCam, FLOAT &fRetYOffset, FLOAT &fRetZOffset, BOOL overrideY = TRUE, BOOL overrideZ = TRUE)

	IF eCam = MOD_CAM_FULL
		RetCam = eCam
		IF (overrideY)
			fRetYOffset = 64.40
		ENDIF
		
		IF (overrideZ)
			fRetZOffset = 0.310
		ENDIF
	ELIF eCam = MOD_CAM_FRONT
		RetCam = eCam
		IF (overrideY)
			fRetYOffset = 15.0
		ENDIF
		
		IF (overrideZ)
			fRetZOffset = 0.350
		ENDIF
	ELIF eCam = MOD_CAM_REAR
		RetCam = eCam
		IF (overrideY)
			fRetYOffset = 15.0
		ENDIF
		
		IF (overrideZ)
			fRetZOffset = 0.350
		ENDIF
	ELIF eCam = MOD_CAM_SIDE_L
		RetCam = eCam
		IF (overrideY)
			fRetYOffset = 18
		ENDIF
		
		IF (overrideZ)
			fRetZOffset = 0.590
		ENDIF
	ELIF eCam = MOD_CAM_SIDE_R
		RetCam = eCam
		IF (overrideY)
			fRetYOffset = 18
		ENDIF
		
		IF (overrideZ)
			fRetZOffset = 0.590
		ENDIF
	ELIF eCam = MOD_CAM_REAR_L
		RetCam = eCam
		IF (overrideY)
			fRetYOffset = 18
		ENDIF
		
		IF (overrideZ)
			fRetZOffset = 0.590
		ENDIF
	ELIF eCam = MOD_CAM_REAR_R
		RetCam = eCam
		IF (overrideY)
			fRetYOffset = 18-60
		ENDIF
		
		IF (overrideZ)
			fRetZOffset = 0.590
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC FLOAT CALCULATE_DIFFERENCE_BETWEEN_ANGLES_FOR_SPEC_VEH_CAM(FLOAT srcAngle, FLOAT tgtAngle)
	FLOAT diff = tgtAngle - srcAngle
        
	WHILE (diff < -180) 
		diff += 360
	ENDWHILE
	
	WHILE (diff > 180) 
		diff -= 360
	ENDWHILE
	
	RETURN DIFF
ENDFUNC

/// PURPOSE: Checks to see if the mod item has a text label defined by vehicle artists
FUNC BOOL SETUP_SPEC_VEH_MOD_COLOUR_LABEL(VEHICLE_INDEX vehIndex, TEXT_LABEL_15 &sRetLabel, BOOL bPrimary)
	
	IF DOES_ENTITY_EXIST(vehIndex)
	AND IS_VEHICLE_DRIVEABLE(vehIndex)	
		IF bPrimary
			sRetLabel = GET_VEHICLE_MOD_COLOR_1_NAME(vehIndex, FALSE)
		ELSE
			sRetLabel = GET_VEHICLE_MOD_COLOR_2_NAME(vehIndex)
		ENDIF
		
		IF GET_HASH_KEY(sRetLabel) != 0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC	

FUNC INT SET_SPEC_VEH_MENU_OPTION_COST(SPEC_VEH_MOD_STRUCT &sSpecVehModData, INT iOption, INT iCost)

	sSpecVehModData.sMenuInfo.iOptionCost[iOption] = iCost 
	g_sShopSettings.bMenuItem_freeForTutorial[iOption] = FALSE
	
	// Tuneables modifier
	sSpecVehModData.sMenuInfo.iOptionCost[iOption] = FLOOR((TO_FLOAT(sSpecVehModData.sMenuInfo.iOptionCost[iOption]) * g_sMPTunables.fCarModShopMultiplier))
	
	RETURN sSpecVehModData.sMenuInfo.iOptionCost[iOption]
ENDFUNC

FUNC FLOAT GET_SPEC_VEH_PRIMARY_COLOUR_TUNEABLE(MOD_COLOR_TYPE colType)
	FLOAT out = 1.0
	
	IF (colType = MCT_CLASSIC)
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorClassic_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorClassic_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorClassic_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorClassic_expenditure_tunable_bike
		ELSE
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorClassic_expenditure_tunable
		ENDIF
	ELIF (colType = MCT_MATTE)
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorMatte_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorMatte_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorMatte_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorMatte_expenditure_tunable_bike
		ELSE
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorMatte_expenditure_tunable
		ENDIF	
	ELIF (colType = MCT_PEARLESCENT)
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorPearlescent_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorPearlescent_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorPearlescent_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorPearlescent_expenditure_tunable_bike
		ELSE
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorPearlescent_expenditure_tunable
		ENDIF
	ELIF (colType = MCT_METALLIC)
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorMetallic_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorMetallic_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorMetallic_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorMetallic_expenditure_tunable_bike
		ELSE
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorMetallic_expenditure_tunable
		ENDIF
	ELIF (colType = MCT_METALS)
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorMetals_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorMetals_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorMetals_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorMetals_expenditure_tunable_bike
		ELSE
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorMetals_expenditure_tunable
		ENDIF
	ELIF (colType = MCT_CHROME)
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorChrome_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorChrome_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorChrome_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorChrome_expenditure_tunable_bike
		ELSE
			out = g_sMPTunables.fcarmod_unlock_PrimaryColorChrome_expenditure_tunable
		ENDIF
	ENDIF
	
	RETURN out
ENDFUNC

FUNC FLOAT GET_SPEC_VEH_SECONDARY_COLOUR_TUNEABLE(MOD_COLOR_TYPE colType)
	FLOAT out = 1.0
	
	IF (colType = MCT_CLASSIC)
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorClassic_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorClassic_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorClassic_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorClassic_expenditure_tunable_bike
		ELSE
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorClassic_expenditure_tunable
		ENDIF
	ELIF (colType = MCT_MATTE)
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorMatte_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorMatte_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorMatte_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorMatte_expenditure_tunable_bike
		ELSE
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorMatte_expenditure_tunable
		ENDIF	
	ELIF (colType = MCT_PEARLESCENT)
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorPearlescent_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorPearlescent_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorPearlescent_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorPearlescent_expenditure_tunable_bike
		ELSE
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorPearlescent_expenditure_tunable
		ENDIF
	ELIF (colType = MCT_METALLIC)
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorMetallic_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorMetallic_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorMetallic_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorMetallic_expenditure_tunable_bike
		ELSE
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorMetallic_expenditure_tunable
		ENDIF
	ELIF (colType = MCT_METALS)
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorMetals_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorMetals_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorMetals_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorMetals_expenditure_tunable_bike
		ELSE
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorMetals_expenditure_tunable
		ENDIF
	ELIF (colType = MCT_CHROME)
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorChrome_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorChrome_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorChrome_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorChrome_expenditure_tunable_bike
		ELSE
			out = g_sMPTunables.fcarmod_unlock_SecondaryColorChrome_expenditure_tunable
		ENDIF
	ENDIF
	
	RETURN out
ENDFUNC

FUNC INT GET_SPEC_VEH_COLOUR_PRICE_FROM_ENUM(STRING strLabel, MOD_COLOR_TYPE colType, BOOL primary)

	FLOAT fCol = TO_FLOAT(GET_BASE_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL(strLabel, colType, primary, FALSE, FALSE, FALSE, FALSE))
	
	IF (primary)
		fCol *= GET_SPEC_VEH_PRIMARY_COLOUR_TUNEABLE(colType)
	ELSE
		fCol *= GET_SPEC_VEH_SECONDARY_COLOUR_TUNEABLE(colType)
		fCol /= 2.0
	ENDIF
	
	RETURN FLOOR(fCol * g_sMPTunableGroups.fpaint_job_group_modifier)
ENDFUNC

PROC CLEAR_SPEC_VEH_PASSENGERS_WANTED_LEVEL()	
	INT i
	PED_INDEX PedInVehicleSeat
	PLAYER_INDEX PlayerInVehicleSeat
	VEHICLE_INDEX viCar

	//Tell other players in my vehicle to join my mission
	viCar = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
	IF IS_VEHICLE_DRIVEABLE(viCar)
		REPEAT GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(viCar) i

			IF NOT IS_VEHICLE_SEAT_FREE(viCar, INT_TO_ENUM(VEHICLE_SEAT, i))
				PedInVehicleSeat = GET_PED_IN_VEHICLE_SEAT(viCar, INT_TO_ENUM(VEHICLE_SEAT, i))
				IF DOES_ENTITY_EXIST(PedInVehicleSeat)
					IF NOT IS_PED_INJURED(PedInVehicleSeat)
						IF IS_PED_A_PLAYER(PedInVehicleSeat)
							IF NOT IS_PED_CUFFED(PedInVehicleSeat)
								PlayerInVehicleSeat = NETWORK_GET_PLAYER_INDEX_FROM_PED(PedInVehicleSeat)
								BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_CLEAR_WANTED_LEVEL, SPECIFIC_PLAYER(PlayerInVehicleSeat))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC BOOL CAN_SPE_VEH_SELECT_BUTTON_BE_PRESSED()
	IF GET_PAUSE_MENU_STATE() != PM_INACTIVE
	OR IS_SYSTEM_UI_BEING_DISPLAYED()
	OR IS_WARNING_MESSAGE_ACTIVE()
	OR g_sShopSettings.bProcessStoreAlert
	OR IS_PLAYER_SWITCH_IN_PROGRESS()
	OR NETWORK_TEXT_CHAT_IS_TYPING()
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_WE_UPDATE_SPE_VEH_MENU()
	
	IF GET_PAUSE_MENU_STATE() != PM_INACTIVE
	OR IS_SYSTEM_UI_BEING_DISPLAYED()
	OR IS_WARNING_MESSAGE_ACTIVE()
	OR IS_PLAYER_SWITCH_IN_PROGRESS()
	OR NETWORK_TEXT_CHAT_IS_TYPING()
		RETURN FALSE
	ENDIF

	RETURN TRUE

ENDFUNC

#IF IS_DEBUG_BUILD
PROC MAINTAIN_ARMORY_TRUCK_MOD_WIDGETS(structSharedDebugVars &paramVars)

	IF IS_PED_INJURED(PLAYER_PED_ID())
		paramVars.vehicleModDisplayCurrentMods = FALSE
		paramVars.vehicleModEnableBasic = FALSE
		paramVars.vehicleModEnableAdvanced = FALSE
		paramVars.vehicleModDataSetBasic = FALSE
		paramVars.vehicleModDataSetAdvanced = FALSE
		EXIT
	ENDIF

	VEHICLE_INDEX vehID 
	
	IF paramVars.vehicleModDisplayCurrentMods 
		IF DOES_ENTITY_EXIST(GET_FOCUS_ENTITY_INDEX())
			vehID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_FOCUS_ENTITY_INDEX())
		ENDIF	
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(vehID)
		paramVars.vehicleModDisplayCurrentMods = FALSE
		paramVars.vehicleModEnableBasic = FALSE
		paramVars.vehicleModEnableAdvanced = FALSE
		paramVars.vehicleModDataSetBasic = FALSE
		paramVars.vehicleModDataSetAdvanced = FALSE
		EXIT
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(vehID)
		paramVars.vehicleModDisplayCurrentMods = FALSE
		paramVars.vehicleModEnableBasic = FALSE
		paramVars.vehicleModEnableAdvanced = FALSE
		paramVars.vehicleModDataSetBasic = FALSE
		paramVars.vehicleModDataSetAdvanced = FALSE
		EXIT
	ENDIF
	
	IF GET_NUM_MOD_KITS(vehID) = 0
		paramVars.vehicleModDisplayCurrentMods = FALSE
		paramVars.vehicleModEnableAdvanced = FALSE
		paramVars.vehicleModDataSetAdvanced = FALSE
		//EXIT
	ENDIF
	
	
	// DISPLAY MODS
	IF paramVars.vehicleModDisplayCurrentMods
	
		IF GET_VEHICLE_MOD_KIT(vehID) != 0
			SET_VEHICLE_MOD_KIT(vehID, 0)
		ENDIF
		
		FLOAT fLineOffset = GET_RENDERED_CHARACTER_HEIGHT(0.4)
		FLOAT fTextX = 0.825+0.005
		FLOAT fTextY = 0.200+0.005
		
		Display_Vehicle_Mod_Debug(vehID, MOD_SPOILER, FALSE, "Spoiler", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_BUMPER_F, FALSE, "Front Bumper", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_BUMPER_R, FALSE, "Rear Bumper", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_SKIRT, FALSE, "Skirt", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_EXHAUST, FALSE, "Exhaust", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_CHASSIS, FALSE, "Chassis", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_GRILL, FALSE, "Grill", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_BONNET, FALSE, "Bonnet", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_WING_L, FALSE, "Left Wing", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_WING_R, FALSE, "Right Wing", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_ROOF, FALSE, "Roof", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_ENGINE, FALSE, "Engine", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_BRAKES, FALSE, "Brakes", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_GEARBOX, FALSE, "Gearbox", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_HORN, FALSE, "Horn", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_SUSPENSION, FALSE, "Suspension", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_ARMOUR, FALSE, "Armour", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_WHEELS, FALSE, "Wheels", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_REAR_WHEELS, FALSE, "Rear Wheel", fTextX, fTextY) fTextY += fLineOffset+fLineOffset
		
		Display_Vehicle_Mod_Debug(vehID, MOD_TOGGLE_NITROUS, TRUE, "Nitrous", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_TOGGLE_TURBO, TRUE, "Turbo", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_TOGGLE_SUBWOOFER, TRUE, "Sub Woofer", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_TOGGLE_HYDRAULICS, TRUE, "Hydraulics", fTextX, fTextY) fTextY += fLineOffset
		Display_Vehicle_Mod_Debug(vehID, MOD_TOGGLE_XENON_LIGHTS, TRUE, "Xenon Lights", fTextX, fTextY) fTextY += fLineOffset
		
		DRAW_RECT_FROM_CORNER(0.825, 0.200, 0.175, fTextY-0.200+0.005, 0, 0, 0, 255)
	ENDIF
	
	// UPDATE BASIC MODS
	IF paramVars.vehicleModEnableBasic
		
		IF NOT paramVars.vehicleModDataSetBasic
		
			GET_VEHICLE_TYRE_SMOKE_COLOR(vehID, paramVars.vehicleModTyreSmokeR, paramVars.vehicleModTyreSmokeG, paramVars.vehicleModTyreSmokeB)
			GET_VEHICLE_TYRE_SMOKE_COLOR(vehID, paramVars.vehicleModTyreSmokeRTemp, paramVars.vehicleModTyreSmokeGTemp, paramVars.vehicleModTyreSmokeBTemp)
			
			paramVars.vehicleLivery = GET_VEHICLE_LIVERY(vehID)
			paramVars.vehicleLiveryTemp = GET_VEHICLE_LIVERY(vehID)
			
			paramVars.vehicleLivery2 = GET_VEHICLE_LIVERY2(vehID)
			paramVars.vehicleLivery2Temp = GET_VEHICLE_LIVERY2(vehID)
			
			paramVars.vehicleModWindowTintColour = GET_VEHICLE_WINDOW_TINT(vehID)
			paramVars.vehicleModWindowTintColourTemp = GET_VEHICLE_WINDOW_TINT(vehID)
			
			paramVars.vehicleModExtraOn[0] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 1)
			paramVars.vehicleModExtraOn[1] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 2)
			paramVars.vehicleModExtraOn[2] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 3)
			paramVars.vehicleModExtraOn[3] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 4)
			paramVars.vehicleModExtraOn[4] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 5)
			paramVars.vehicleModExtraOn[5] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 6)
			paramVars.vehicleModExtraOn[6] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 7)
			paramVars.vehicleModExtraOn[7] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 8)
			paramVars.vehicleModExtraOn[8] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 9)
			
			paramVars.vehicleModExtraOnTemp[0] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 1)
			paramVars.vehicleModExtraOnTemp[1] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 2)
			paramVars.vehicleModExtraOnTemp[2] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 3)
			paramVars.vehicleModExtraOnTemp[3] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 4)
			paramVars.vehicleModExtraOnTemp[4] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 5)
			paramVars.vehicleModExtraOnTemp[5] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 6)
			paramVars.vehicleModExtraOnTemp[6] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 7)
			paramVars.vehicleModExtraOnTemp[7] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 8)
			paramVars.vehicleModExtraOnTemp[8] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 9)
			
			paramVars.vehicleModDataSetBasic = TRUE
		ENDIF
		
		// Tyre smoke
		IF paramVars.vehicleModTyreSmokeRTemp != paramVars.vehicleModTyreSmokeR
		OR paramVars.vehicleModTyreSmokeGTemp != paramVars.vehicleModTyreSmokeG
		OR paramVars.vehicleModTyreSmokeBTemp != paramVars.vehicleModTyreSmokeB
			SET_VEHICLE_TYRE_SMOKE_COLOR(vehID, paramVars.vehicleModTyreSmokeR, paramVars.vehicleModTyreSmokeG, paramVars.vehicleModTyreSmokeB)
			paramVars.vehicleModTyreSmokeRTemp = paramVars.vehicleModTyreSmokeR
			paramVars.vehicleModTyreSmokeGTemp = paramVars.vehicleModTyreSmokeG
			paramVars.vehicleModTyreSmokeBTemp = paramVars.vehicleModTyreSmokeB
		ENDIF
		
		// Window tint
		IF paramVars.vehicleModWindowTintColourTemp != paramVars.vehicleModWindowTintColour
			SET_VEHICLE_WINDOW_TINT(vehID, paramVars.vehicleModWindowTintColour)
			paramVars.vehicleModWindowTintColourTemp = paramVars.vehicleModWindowTintColour
		ENDIF
		
		// Livery
		IF paramVars.vehicleLiveryTemp != paramVars.vehicleLivery
			SET_VEHICLE_LIVERY(vehID, paramVars.vehicleLivery)
			paramVars.vehicleLiveryTemp = paramVars.vehicleLivery
		ENDIF
		
		IF paramVars.vehicleLivery2Temp != paramVars.vehicleLivery2
			SET_VEHICLE_LIVERY2(vehID, paramVars.vehicleLivery2)
			paramVars.vehicleLivery2Temp = paramVars.vehicleLivery2
		ENDIF
		
		// Extras
		INT i
		REPEAT 9 i
			IF paramVars.vehicleModExtraOnTemp[i] != paramVars.vehicleModExtraOn[i]
				SET_VEHICLE_EXTRA(vehID, i+1, !paramVars.vehicleModExtraOn[i])
				paramVars.vehicleModExtraOnTemp[i] = paramVars.vehicleModExtraOn[i]
			ENDIF
		ENDREPEAT
	ELSE
		paramVars.vehicleModDataSetBasic = FALSE
	ENDIF
	
	// UPDATE ADVANCED MODS
	IF paramVars.vehicleModEnableAdvanced
	
		IF GET_VEHICLE_MOD_KIT(vehID) != 0
			SET_VEHICLE_MOD_KIT(vehID, 0)
		ENDIF
		
		INT i, iMaxMods
		MOD_TYPE eMod
		BOOL bToggleMod
		
		IF NOT paramVars.vehicleModDataSetAdvanced
		
			REPEAT MAX_VEHICLE_MOD_SLOTS i
				eMod = INT_TO_ENUM(MOD_TYPE, i)
				IF eMod = MOD_TOGGLE_NITROUS
				OR eMod = MOD_TOGGLE_TURBO
				OR eMod = MOD_TOGGLE_SUBWOOFER
				OR eMod = MOD_TOGGLE_TYRE_SMOKE
				OR eMod = MOD_TOGGLE_HYDRAULICS
				OR eMod = MOD_TOGGLE_XENON_LIGHTS
					bToggleMod = TRUE
				ELSE
					bToggleMod = FALSE
				ENDIF
				
				IF bToggleMod
					IF IS_TOGGLE_MOD_ON(vehID, eMod)
						paramVars.vehicleModSlotIndex[eMod] = 1
					ELSE
						paramVars.vehicleModSlotIndex[eMod] = 0
					ENDIF
				ELSE
					paramVars.vehicleModSlotIndex[eMod] = GET_VEHICLE_MOD(vehID, eMod)
				ENDIF
			ENDREPEAT
			
			paramVars.vehicleModWheelType = ENUM_TO_INT(GET_VEHICLE_WHEEL_TYPE(vehID))
			
			paramVars.vehicleModDataSetAdvanced = TRUE
		ENDIF
		
		// Mods
		REPEAT MAX_VEHICLE_MOD_SLOTS i
			eMod = INT_TO_ENUM(MOD_TYPE, i)
			IF eMod = MOD_TOGGLE_NITROUS
			OR eMod = MOD_TOGGLE_TURBO
			OR eMod = MOD_TOGGLE_SUBWOOFER
			OR eMod = MOD_TOGGLE_TYRE_SMOKE
			OR eMod = MOD_TOGGLE_HYDRAULICS
			OR eMod = MOD_TOGGLE_XENON_LIGHTS
				bToggleMod = TRUE
			ELSE
				bToggleMod = FALSE
			ENDIF
			
			IF bToggleMod
				// Cap
				IF paramVars.vehicleModSlotIndex[eMod] < 0
					paramVars.vehicleModSlotIndex[eMod] = 0
				ELIF paramVars.vehicleModSlotIndex[eMod] > 1
					paramVars.vehicleModSlotIndex[eMod] = 1
				ENDIF
				
				// Turn off
				IF IS_TOGGLE_MOD_ON(vehID, eMod)
					IF paramVars.vehicleModSlotIndex[eMod] = 0
						PRINTLN("Toggling vehicle mod off")
						TOGGLE_VEHICLE_MOD(vehID, eMod, FALSE)
					ENDIF
				
				// Turn on
				ELSE
					IF paramVars.vehicleModSlotIndex[eMod] = 1
						PRINTLN("Toggling vehicle mod on")
						TOGGLE_VEHICLE_MOD(vehID, eMod, TRUE)
					ENDIF
				ENDIF
			ELSE
				// Cap
				iMaxMods = GET_NUM_VEHICLE_MODS(vehID, eMod)
				IF paramVars.vehicleModSlotIndex[eMod] > iMaxMods-1
					paramVars.vehicleModSlotIndex[eMod] = iMaxMods-1
				ENDIF
				
				// Update
				IF GET_VEHICLE_MOD(vehID, eMod) <> paramVars.vehicleModSlotIndex[eMod]
					IF paramVars.vehicleModSlotIndex[eMod] = -1
						PRINTLN("Removing vehicle mod")
						REMOVE_VEHICLE_MOD(vehID, eMod)
					ELSE
						IF GET_NUM_VEHICLE_MODS(vehID, eMod) > paramVars.vehicleModSlotIndex[eMod]
							PRINTLN("Setting vehicle mod")
							SET_VEHICLE_MOD(vehID, eMod, paramVars.vehicleModSlotIndex[eMod])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		IF GET_VEHICLE_WHEEL_TYPE(vehID) != INT_TO_ENUM(MOD_WHEEL_TYPE, paramVars.vehicleModWheelType)
			PRINTLN("Setting wheel type")
			SET_VEHICLE_WHEEL_TYPE(vehID, INT_TO_ENUM(MOD_WHEEL_TYPE, paramVars.vehicleModWheelType))
		ENDIF
	ELSE
		paramVars.vehicleModDataSetAdvanced = FALSE
	ENDIF
	
ENDPROC

PROC MAINTAIN_MODS_WIDGET(structSharedDebugVars &paramVars)
	ADD_WIDGET_BOOL("Display current mods", paramVars.vehicleModDisplayCurrentMods)
	
	START_WIDGET_GROUP("Basic Mods")
		ADD_WIDGET_BOOL("Enable", paramVars.vehicleModEnableBasic)
		ADD_WIDGET_BOOL("Extra[1]", paramVars.vehicleModExtraOn[0])
		ADD_WIDGET_BOOL("Extra[2]", paramVars.vehicleModExtraOn[1])
		ADD_WIDGET_BOOL("Extra[3]", paramVars.vehicleModExtraOn[2])
		ADD_WIDGET_BOOL("Extra[4]", paramVars.vehicleModExtraOn[3])
		ADD_WIDGET_BOOL("Extra[5]", paramVars.vehicleModExtraOn[4])
		ADD_WIDGET_BOOL("Extra[6]", paramVars.vehicleModExtraOn[5])
		ADD_WIDGET_BOOL("Extra[7]", paramVars.vehicleModExtraOn[6])
		ADD_WIDGET_BOOL("Extra[8]", paramVars.vehicleModExtraOn[7])
		ADD_WIDGET_BOOL("Extra[9]", paramVars.vehicleModExtraOn[8])
		ADD_WIDGET_INT_SLIDER("Livery", paramVars.vehicleLivery, 0, 10, 1)
		ADD_WIDGET_INT_SLIDER("Livery2", paramVars.vehicleLivery2, 0, 10, 1)
		ADD_WIDGET_INT_SLIDER("Window Tint Colour", paramVars.vehicleModWindowTintColour, 0, 100, 1)
		ADD_WIDGET_INT_SLIDER("Tyre Smoke Colour R", paramVars.vehicleModTyreSmokeR, 0, 255, 1)
		ADD_WIDGET_INT_SLIDER("Tyre Smoke Colour G", paramVars.vehicleModTyreSmokeG, 0, 255, 1)
		ADD_WIDGET_INT_SLIDER("Tyre Smoke Colour B", paramVars.vehicleModTyreSmokeB, 0, 255, 1)
	STOP_WIDGET_GROUP()
	
	START_WIDGET_GROUP("Advanced Mods")
		ADD_WIDGET_BOOL("Enable", paramVars.vehicleModEnableAdvanced)
		ADD_WIDGET_INT_SLIDER("Wheel Type", paramVars.vehicleModWheelType, 0, 13, 1)
		ADD_WIDGET_INT_SLIDER("MOD_SPOILER", paramVars.vehicleModSlotIndex[MOD_SPOILER], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_BUMPER_F", paramVars.vehicleModSlotIndex[MOD_BUMPER_F], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_BUMPER_R", paramVars.vehicleModSlotIndex[MOD_BUMPER_R], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_SKIRT", paramVars.vehicleModSlotIndex[MOD_SKIRT], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_EXHAUST", paramVars.vehicleModSlotIndex[MOD_EXHAUST], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_CHASSIS", paramVars.vehicleModSlotIndex[MOD_CHASSIS], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_GRILL", paramVars.vehicleModSlotIndex[MOD_GRILL], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_BONNET", paramVars.vehicleModSlotIndex[MOD_BONNET], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_WING_L", paramVars.vehicleModSlotIndex[MOD_WING_L], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_WING_R", paramVars.vehicleModSlotIndex[MOD_WING_R], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_ROOF", paramVars.vehicleModSlotIndex[MOD_ROOF], -1, 250, 1)
		
		ADD_WIDGET_INT_SLIDER("MOD_ENGINE", paramVars.vehicleModSlotIndex[MOD_ENGINE], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_BRAKES", paramVars.vehicleModSlotIndex[MOD_BRAKES], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_GEARBOX", paramVars.vehicleModSlotIndex[MOD_GEARBOX], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_HORN", paramVars.vehicleModSlotIndex[MOD_HORN], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_SUSPENSION", paramVars.vehicleModSlotIndex[MOD_SUSPENSION], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_ARMOUR", paramVars.vehicleModSlotIndex[MOD_ARMOUR], -1, 250, 1)
		
		ADD_WIDGET_INT_SLIDER("MOD_TOGGLE_NITROUS", paramVars.vehicleModSlotIndex[MOD_TOGGLE_NITROUS], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_TOGGLE_TURBO", paramVars.vehicleModSlotIndex[MOD_TOGGLE_TURBO], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_TOGGLE_SUBWOOFER", paramVars.vehicleModSlotIndex[MOD_TOGGLE_SUBWOOFER], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_TOGGLE_TYRE_SMOKE", paramVars.vehicleModSlotIndex[MOD_TOGGLE_TYRE_SMOKE], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_TOGGLE_HYDRAULICS", paramVars.vehicleModSlotIndex[MOD_TOGGLE_HYDRAULICS], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_TOGGLE_XENON_LIGHTS", paramVars.vehicleModSlotIndex[MOD_TOGGLE_XENON_LIGHTS], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_WHEELS", paramVars.vehicleModSlotIndex[MOD_WHEELS], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_REAR_WHEELS", paramVars.vehicleModSlotIndex[MOD_REAR_WHEELS], -1, 250, 1)
		
		ADD_WIDGET_INT_SLIDER("MOD_PLTHOLDER", paramVars.vehicleModSlotIndex[MOD_PLTHOLDER], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_PLTVANITY", paramVars.vehicleModSlotIndex[MOD_PLTVANITY], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_INTERIOR1", paramVars.vehicleModSlotIndex[MOD_INTERIOR1], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_INTERIOR2", paramVars.vehicleModSlotIndex[MOD_INTERIOR2], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_INTERIOR3", paramVars.vehicleModSlotIndex[MOD_INTERIOR3], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_INTERIOR4", paramVars.vehicleModSlotIndex[MOD_INTERIOR4], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_INTERIOR5", paramVars.vehicleModSlotIndex[MOD_INTERIOR5], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_SEATS", paramVars.vehicleModSlotIndex[MOD_SEATS], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_STEERING", paramVars.vehicleModSlotIndex[MOD_STEERING], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_KNOB", paramVars.vehicleModSlotIndex[MOD_KNOB], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_PLAQUE", paramVars.vehicleModSlotIndex[MOD_PLAQUE], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_ICE", paramVars.vehicleModSlotIndex[MOD_ICE], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_TRUNK", paramVars.vehicleModSlotIndex[MOD_TRUNK], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_HYDRO", paramVars.vehicleModSlotIndex[MOD_HYDRO], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_ENGINEBAY1", paramVars.vehicleModSlotIndex[MOD_ENGINEBAY1], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_ENGINEBAY2", paramVars.vehicleModSlotIndex[MOD_ENGINEBAY2], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_ENGINEBAY3", paramVars.vehicleModSlotIndex[MOD_ENGINEBAY3], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_CHASSIS2", paramVars.vehicleModSlotIndex[MOD_CHASSIS2], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_CHASSIS3", paramVars.vehicleModSlotIndex[MOD_CHASSIS3], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_CHASSIS4", paramVars.vehicleModSlotIndex[MOD_CHASSIS4], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_CHASSIS5", paramVars.vehicleModSlotIndex[MOD_CHASSIS5], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_DOOR_L", paramVars.vehicleModSlotIndex[MOD_DOOR_L], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_DOOR_R", paramVars.vehicleModSlotIndex[MOD_DOOR_R], -1, 250, 1)
		ADD_WIDGET_INT_SLIDER("MOD_LIVERY", paramVars.vehicleModSlotIndex[MOD_LIVERY], -1, 250, 1)
		
	STOP_WIDGET_GROUP()
ENDPROC			

PROC DRAW_DEBUG_SPEC_VEH_CAM_TEXT(TEXT_LABEL_63 str, HUD_COLOURS eHudColour, INT &iStrOffset, INT &iColumn)
	INT iRed, iGreen, iBlue, iAlpha
	GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
	
	DRAW_DEBUG_TEXT_2D(str, <<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, iAlpha)	iStrOffset++
	
	CONST_FLOAT  fTextWrap			0.025
	
	SET_TEXT_JUSTIFICATION(FONT_RIGHT)
	SET_TEXT_RIGHT_JUSTIFY(TRUE)
	
	SET_TEXT_DROPSHADOW(1, 0, 0, 0, 255)
	SET_TEXT_EDGE(1, 0, 0, 0, 255)
	
	CONST_INT iMAX_RGB	255
	CONST_FLOAT fRATIO_RGB	0.5
	SET_TEXT_COLOUR(ROUND((TO_FLOAT(iRed)*fRATIO_RGB)+TO_FLOAT(iMAX_RGB)*(1.0-fRATIO_RGB)),
			ROUND((TO_FLOAT(iGreen)*fRATIO_RGB)+TO_FLOAT(iMAX_RGB)*(1.0-fRATIO_RGB)),
			ROUND((TO_FLOAT(iBlue)*fRATIO_RGB)+TO_FLOAT(iMAX_RGB)*(1.0-fRATIO_RGB)),
			iAlpha)
	SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_WRAP(fTextWrap, (1.0-fTextWrap))
	
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.5+(0.0*TO_FLOAT(iColumn)), 0.7+(0.03*TO_FLOAT(iStrOffset)), "STRING", str)
ENDPROC

#ENDIF



