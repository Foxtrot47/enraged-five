USING "net_include.sch"
USING "net_mission.sch"

USING "help_at_location.sch"
USING "commands_task.sch"
USING "commands_decorator.sch"
USING "net_nodes.sch"
USING "net_comms_public.sch"
USING "vehicle_public.sch"
USING "selector_public.sch"
USING "area_checks.sch"
USING "shop_public.sch"
USING "script_blip.sch"
USING "script_ped.sch"
USING "shared_hud_displays.sch"
USING "net_mission_trigger_overview.sch"
USING "net_importexport.sch"
USING "net_saved_vehicles.sch"
USING "net_spawn_vehicle.sch"
USING "FM_Post_Mission_Cleanup_Public.sch"
USING "net_realty_details.sch"
USING "net_common_functions.sch"
USING "net_transition_sessions.sch"
USING "context_control_public.sch"
USING "net_impound.sch"
USING "net_ambience_common.sch"

#IF IS_DEBUG_BUILD
#IF SCRIPT_PROFILER_ACTIVE
USING "profiler.sch"
#ENDIF
#ENDIF

//Used for MP_SAVE_VEHICLE_PUT_VEH_INTO_GARAGE
CONST_INT PUT_VEH_INTO_GARAGE_SUCCESS						1
CONST_INT PUT_VEH_INTO_GARAGE_VEHICLE_NOT_EMPTY				2
CONST_INT PUT_VEH_INTO_GARAGE_VEHICLE_NOT_DV_DELETE			3
CONST_INT PUT_VEH_INTO_GARAGE_VEHICLE_NOT_DV_NOT_EMPTY		4	
CONST_INT PUT_VEH_INTO_GARAGE_DOES_NOT_EXIST				5
CONST_INT PUT_VEH_INTO_GARAGE_CURRENTLY_IMPOUNDED			6




// ---------------- Associated Wanted Levels ----------------




FUNC BOOL IS_PLAYER_IN_PASSENGER_SEAT_WITH_ANOTHER_PLAYER_AS_DRIVER()


	VEHICLE_INDEX vehID
	PED_INDEX pedID
	PLAYER_INDEX playerID
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(vehID)
				
				IF NOT IS_VEHICLE_SEAT_FREE(vehID, VS_DRIVER)
					pedID = GET_PED_IN_VEHICLE_SEAT(vehID, VS_DRIVER)
					IF DOES_ENTITY_EXIST(pedID)
						IF IS_PED_A_PLAYER(pedID)
							IF pedID != PLAYER_PED_ID()
								playerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedID)
								IF IS_NET_PLAYER_OK(playerID)
									#IF IS_DEBUG_BUILD
										NET_PRINT("IS_PLAYER_IN_PASSENGER_SEAT_WITH_ANOTHER_PLAYER_AS_DRIVER = TRUE ") NET_NL()
									#ENDIF
								
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("IS_PLAYER_IN_PASSENGER_SEAT_WITH_ANOTHER_PLAYER_AS_DRIVER = FALSE ") NET_NL()
	#ENDIF
	RETURN FALSE
ENDFUNC

//// PURPOSE:
 ///    EXPENSIVE! Returns true if any of the other players are in the same vehicle as the passed player
 /// PARAMS:
 ///    playerID - Player to test
 ///    bConsiderEnteringAsInVehicle - Will return true if the player is getting in, or any of the other players are getting in, to the vehicle.
 /// RETURNS:
 ///    
FUNC BOOL IS_PLAYER_IN_VEHICLE_WITH_ANOTHER_PLAYER(PLAYER_INDEX playerID, BOOL bConsiderEnteringAsInVehicle = FALSE)
	
	IF IS_NET_PLAYER_OK(playerID)
		PED_INDEX pedPlayer = GET_PLAYER_PED(playerID)
		IF IS_PED_IN_ANY_VEHICLE(pedPlayer, bConsiderEnteringAsInVehicle)
			INT iCount
			PLAYER_INDEX playerCount
			VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(pedPlayer)
			
			REPEAT NUM_NETWORK_PLAYERS iCount
				playerCount = INT_TO_PLAYERINDEX(iCount)
				IF IS_NET_PLAYER_OK(playerCount)
					IF GET_PLAYER_PED(playerCount) != pedPlayer
						IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(playerCount), vehPlayer, bConsiderEnteringAsInVehicle)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_VEHICLE_WITH_ALL_OF_TEAM(INT iTeamMates)
	PLAYER_INDEX LocalPlayer = GET_PLAYER_INDEX()
	PED_INDEX LocalPlayerPed = PLAYER_PED_ID()
	INT iTeamMatesInVeh = 1 //we want to include ourselves.
	
	IF IS_NET_PLAYER_OK(LocalPlayer)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
			INT iCount
			PLAYER_INDEX playerCount
			VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE)
			
			REPEAT NUM_NETWORK_PLAYERS iCount
				playerCount = INT_TO_PLAYERINDEX(iCount)
				IF IS_NET_PLAYER_OK(playerCount)
					IF GET_PLAYER_PED(playerCount) != LocalPlayerPed
						IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(playerCount), vehPlayer, TRUE)
							iTeamMatesInVeh++
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	IF iTeamMatesInVeh = iTeamMates
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC


///PURPOSE:
///		Will Reset / Cancel the "Cops turn blind eye" lester contact ability, resetting the player's max wanted level to its original level.    	
PROC RESET_LESTER_NO_COPS()
	#IF IS_DEBUG_BUILD
		PRINTLN("[dsw] [RESET_LESTER_NO_COPS] Called, Callstack...")
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_ByAffiliation)
		CLEAR_BIT(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_ByAffiliation)
	ENDIF
	
	IF IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Gang)
		CLEAR_BIT(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Gang)
	ENDIF
	
	IF IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_GangVipBribing)
		CLEAR_BIT(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_GangVipBribing)
	ENDIF
	
	CLEAR_BIT(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Activated)
	CLEAR_BIT(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Personal)
	//globalplayerbd_FM[NATIVE_TO_INT(PLAYER_ID())].g_bNoCopsActive = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bNoCopsActive)
	IF MPGlobalsAmbience.iMyMaxWantedLevel > 0
		SET_MAX_WANTED_LEVEL(MPGlobalsAmbience.iMyMaxWantedLevel)
		PRINTLN("[dsw] [RESET_LESTER_NO_COPS] Restoring max wanted level to ",MPGlobalsAmbience.iMyMaxWantedLevel)
	ENDIF
	MPGlobalsAmbience.iLesterDisableCopsProg = 0
ENDPROC

///PURPOSE:
///		Will activate the "Cops turn blind eye" lester contact ability for other players in the same vehicle 
PROC SET_PLAYERS_IN_VEHICLE_IN_NO_COPS_INFLUENCE()
	IF IS_PLAYER_IN_VEHICLE_WITH_ANOTHER_PLAYER(PLAYER_ID())			
		VEHICLE_INDEX wantedCar
		wantedCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		INT iTemp
		FOR iTemp = -1 TO GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(wantedCar)-1
			PED_INDEX pedInCar 
			pedInCar = GET_PED_IN_VEHICLE_SEAT(wantedCar,INT_TO_ENUM(VEHICLE_SEAT,iTemp))
			IF DOES_ENTITY_EXIST(pedInCar)
				IF IS_PED_A_PLAYER(pedInCar)
					PLAYER_INDEX playerInCar
					playerInCar = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedInCar)
					IF NETWORK_IS_PLAYER_ACTIVE(playerInCar)
						IF playerInCar != PLAYER_ID()
							BROADCAST_SET_COPS_DISABLED(SPECIFIC_PLAYER(playerInCar))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

///PURPOSE:
///		Is local player in a vehicle with another player that has activated the "Cops turn blind eye" lester contact ability   
FUNC BOOL IS_PLAYER_IN_NO_COPS_INFLUENCE()
	IF IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Activated)
		IF IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_ByAffiliation)
			IF IS_PLAYER_IN_VEHICLE_WITH_ANOTHER_PLAYER(PLAYER_ID())
				VEHICLE_INDEX wantedCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				INT iTemp
				FOR iTemp = -1 TO GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(wantedCar)-1
					PED_INDEX pedInCar 
					pedInCar = GET_PED_IN_VEHICLE_SEAT(wantedCar,INT_TO_ENUM(VEHICLE_SEAT,iTemp))
					IF DOES_ENTITY_EXIST(pedInCar)
						IF IS_PED_A_PLAYER(pedInCar)
							PLAYER_INDEX playerInCar
							playerInCar = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedInCar)
							IF NETWORK_IS_PLAYER_ACTIVE(playerInCar)
								IF playerInCar != PLAYER_ID()
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerInCar)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bNoCopsActive)
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

///PURPOSE:
///		Will activate the "Off The Radar" lester contact ability for other players in the same vehicle 
PROC SET_PLAYERS_IN_VEHICLE_IN_OFF_THE_RADAR_INFLUENCE()
	IF IS_PLAYER_IN_VEHICLE_WITH_ANOTHER_PLAYER(PLAYER_ID())			
		VEHICLE_INDEX wantedCar
		wantedCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		INT iTemp
		FOR iTemp = -1 TO GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(wantedCar)-1
			PED_INDEX pedInCar 
			pedInCar = GET_PED_IN_VEHICLE_SEAT(wantedCar,INT_TO_ENUM(VEHICLE_SEAT,iTemp))
			IF DOES_ENTITY_EXIST(pedInCar)
				IF IS_PED_A_PLAYER(pedInCar)
					PLAYER_INDEX playerInCar
					playerInCar = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedInCar)
					IF NETWORK_IS_PLAYER_ACTIVE(playerInCar)
						IF playerInCar != PLAYER_ID()
							BROADCAST_REMOVE_PLAYER_BLIP(playerInCar)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

///PURPOSE: 
///		Is local player in a vehicle with another player that has activated the "Off The Radar" lester contact ability   
FUNC BOOL IS_PLAYER_IN_OFF_THE_RADAR_INFLUENCE()
//	IF globalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bOffTheRadar
		IF IS_BIT_SET(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_ByAffiliation)
			IF IS_PLAYER_IN_VEHICLE_WITH_ANOTHER_PLAYER(PLAYER_ID())
				VEHICLE_INDEX wantedCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				INT iTemp
				FOR iTemp = -1 TO GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(wantedCar)-1
					PED_INDEX pedInCar 
					pedInCar = GET_PED_IN_VEHICLE_SEAT(wantedCar,INT_TO_ENUM(VEHICLE_SEAT,iTemp))
					IF DOES_ENTITY_EXIST(pedInCar)
						IF IS_PED_A_PLAYER(pedInCar)
							PLAYER_INDEX playerInCar
							playerInCar = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedInCar)
							IF NETWORK_IS_PLAYER_ACTIVE(playerInCar)
								IF playerInCar != PLAYER_ID()
									IF globalplayerBD[NATIVE_TO_INT(playerInCar)].bOffTheRadar
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ELSE
		//	RETURN TRUE
		ENDIF
	//ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Gang Boss - As the boss, set all goons and yourself to off the radar for a short period of time.
///    Used in the Gang Boss PI menu under "Abilities".
PROC SET_GANG_IS_OFF_RADAR()
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			PRINTLN("[GB][AMEC] SET_GANG_IS_OFF_RADAR - Setting local player (boss) to be off the radar.")
			MPGlobals.g_timeOffTheRadar = GET_NETWORK_TIME()
			SET_BIT(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_Gang)
			GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].bOffTheRadar = TRUE
			GlobalplayerBD[NETWORK_PLAYER_ID_TO_INT()].bOffTheRadarGangMode = TRUE
		
			IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_GONE_OFF_RADAR)
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_GONE_OFF_RADAR, TRUE)
			ENDIF
		
			INT index
			FOR index = 0 TO (NUM_NETWORK_PLAYERS-1)
				
				PLAYER_INDEX pidTarget = INT_TO_PLAYERINDEX(index)
				
				IF (pidTarget != PLAYER_ID())
					IF IS_NET_PLAYER_OK(pidTarget, FALSE, TRUE)
						IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(pidTarget, PLAYER_ID(), FALSE)
							PRINTLN("[GB][AMEC] SET_GANG_IS_OFF_RADAR - Setting player: [",index,", ",GET_PLAYER_NAME(pidTarget),"] to off the radar - gang mode.")
							BROADCAST_REMOVE_PLAYER_BLIP(pidTarget, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[GB][AMEC] SET_GANG_IS_OFF_RADAR - WARNING! Local player attempted to set gang off the radar while NOT a gang boss!")
		#ENDIF
		
		ENDIF
	
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[GB][AMEC] SET_GANG_IS_OFF_RADAR - WARNING! Local player attempted to set gang off the radar while NOT in a gang!")
	#ENDIF
	
	ENDIF
ENDPROC


/// PURPOSE:
///    Is the local player in a gang that is marked as "off the radar".
/// RETURNS:
///    TRUE if gang is off the radar.
FUNC BOOL IS_PLAYER_GANG_OFF_THE_RADAR()
	IF IS_BIT_SET(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_Gang)
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
			PLAYER_INDEX pidGangBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
			IF pidGangBoss != INVALID_PLAYER_INDEX()
				IF globalplayerBD[NATIVE_TO_INT(pidGangBoss)].bOffTheRadar
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Is the local player in a gang that have enabled "Bribe Authorities".
/// RETURNS:
///    TRUE if gang is bribing authorities.
FUNC BOOL IS_PLAYER_GANG_BRIBING_POLICE()
	IF IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Gang)
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
			PLAYER_INDEX pidGangBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
			IF pidGangBoss != INVALID_PLAYER_INDEX()
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(pidGangBoss)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bNoCopsActive)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Remove wanted and bribe police for 2 minutes. 
///    Used in the Gang Boss PI menu under "Abilities".
PROC SET_GANG_BRIBE_POLICE()

	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			PRINTLN("[GB][AMEC] SET_GANG_BRIBE_POLICE - Setting local player (boss) to bribe police.")
			
			// Activate no cops
			SET_BIT(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Gang)
			SET_BIT(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Activated)
			SET_BIT(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_GangVipBribing)
			
			//globalplayerbd_FM[NETWORK_PLAYER_ID_TO_INT()].g_bNoCopsActive = TRUE
			SET_BIT(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bNoCopsActive)
			
			// Clear wanted level here
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			ENDIF
		
			INT index
			FOR index = 0 TO (NUM_NETWORK_PLAYERS-1)
				
				PLAYER_INDEX pidTarget = INT_TO_PLAYERINDEX(index)
				
				IF (pidTarget != PLAYER_ID())
					IF IS_NET_PLAYER_OK(pidTarget, FALSE, TRUE)
						IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(pidTarget, PLAYER_ID(), FALSE)
							PRINTLN("[GB][AMEC] SET_GANG_BRIBE_POLICE - Setting player: [",index,", ",GET_PLAYER_NAME(pidTarget),"] to off the radar - gang mode.")
							BROADCAST_GB_BRIBE_POLICE(pidTarget)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[GB][AMEC] SET_GANG_BRIBE_POLICE - WARNING! Local player attempted to bribe police while NOT a gang boss!")
		#ENDIF
		
		ENDIF
	
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[GB][AMEC] SET_GANG_BRIBE_POLICE - WARNING! Local player attempted to bribe police while NOT in a gang!")
	#ENDIF
	
	ENDIF

ENDPROC


///PURPOSE: 
///		Has anybody else in the session activated the "Off The Radar" lester contact ability   
FUNC INT ARE_OTHER_PLAYERS_OFF_RADAR()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF GlobalplayerBD[i].bOffTheRadar
		AND i != NETWORK_PLAYER_ID_TO_INT()
			RETURN 1
		ENDIF
	ENDREPEAT
	RETURN 0
ENDFUNC

PROC PROCESS_ASSOCIATED_WANTED_LEVEL()

	#IF IS_DEBUG_BUILD
	
		IF GET_COMMANDLINE_PARAM_EXISTS("-scAssocWanted")
			g_DoAssociatedWantedLevelPrints = TRUE
		ENDIF
	
		IF g_DoAssociatedWantedLevelPrints 
			PRINTLN("[Assoc Wanted Lvl] - being called.")
		ENDIF
	#ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(MPGlobalsAmbience.AssociatedWantedLevelTimer)
	OR HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.AssociatedWantedLevelTimer,1000)
		
		#IF IS_DEBUG_BUILD
			IF g_DoAssociatedWantedLevelPrints 
				PRINTLN("[Assoc Wanted Lvl] - timer not started or has expired.")
			ENDIF
		#ENDIF
	
		VEHICLE_INDEX vehID
		INT iSeat
		PED_INDEX pedID
		PLAYER_INDEX playerID
		
		INT iHighestWantedLevel
		INT iTempWantedLevel
		VECTOR vWantedLoc
		BOOL bUpdatePosition
		BOOL bGoonGaveWanted
		BOOL bBoss = GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		PLAYER_INDEX playerWhoGaveID
		VECTOR vNearestWantedPosition
		FLOAT fClosestPosition = 999999.0

		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		
			#IF IS_DEBUG_BUILD
				IF g_DoAssociatedWantedLevelPrints 
					PRINTLN("[Assoc Wanted Lvl] - IS_ENTITY_DEAD(PLAYER_PED_ID()) = FALSE.")
				ENDIF
			#ENDIF
			
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
				#IF IS_DEBUG_BUILD
					IF g_DoAssociatedWantedLevelPrints 
						PRINTLN("[Assoc Wanted Lvl] - IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()) = TRUE.")
					ENDIF
				#ENDIF
			
				vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
				IF IS_VEHICLE_DRIVEABLE(vehID)
					
					#IF IS_DEBUG_BUILD
						IF g_DoAssociatedWantedLevelPrints 
							PRINTLN("[Assoc Wanted Lvl] - players veh is driveable.")
						ENDIF
					#ENDIF
				
					FOR iSeat = ENUM_TO_INT(VS_DRIVER) TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
						
						#IF IS_DEBUG_BUILD
							IF g_DoAssociatedWantedLevelPrints 
								PRINTLN("[Assoc Wanted Lvl] - checking seat ", iSeat)
							ENDIF
						#ENDIF
						
						IF NOT IS_VEHICLE_SEAT_FREE(vehID, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
							
							#IF IS_DEBUG_BUILD
								IF g_DoAssociatedWantedLevelPrints 
									PRINTLN("[Assoc Wanted Lvl] - seat ", iSeat, " has a ped in it.")
								ENDIF
							#ENDIF
							
							pedID = GET_PED_IN_VEHICLE_SEAT(vehID, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
							
							IF DOES_ENTITY_EXIST(pedID)
								
								#IF IS_DEBUG_BUILD
									IF g_DoAssociatedWantedLevelPrints 
										PRINTLN("[Assoc Wanted Lvl] - ped in seat ", iSeat, " exists.")
									ENDIF
								#ENDIF
								
								IF IS_PED_A_PLAYER(pedID)
									
									playerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedID)
									
									#IF IS_DEBUG_BUILD
										IF g_DoAssociatedWantedLevelPrints
											INT iPlayer = NATIVE_TO_INT(playerID)
											PRINTLN("[Assoc Wanted Lvl] - ped in seat ", iSeat, " is a player. Id = ", iPlayer)
										ENDIF
									#ENDIF
									
									IF playerID != PLAYER_ID()
										#IF IS_DEBUG_BUILD
											TEXT_LABEL_63 tl63Name 
											IF g_DoAssociatedWantedLevelPrints 
												 tl63Name = GET_PLAYER_NAME(playerID)
												PRINTLN("[Assoc Wanted Lvl] - player in in seat ", iSeat, " is not the local player. Name = ", tl63Name)
											ENDIF
										#ENDIF
										
										IF IS_NET_PLAYER_OK(playerID)
											
											iTempWantedLevel = GET_PLAYER_WANTED_LEVEL(playerID)
											
											#IF IS_DEBUG_BUILD
												IF g_DoAssociatedWantedLevelPrints 
													PRINTLN("[Assoc Wanted Lvl] - player in in seat ", iSeat, " is net ok. They have a wanted level of ", iTempWantedLevel)
												ENDIF
											#ENDIF
										
											IF iTempWantedLevel > 0
												
												#IF IS_DEBUG_BUILD
													IF g_DoAssociatedWantedLevelPrints 
														PRINTLN("[Assoc Wanted Lvl] - player in in seat ", iSeat, " is net ok. They have a wanted level > 0.")
													ENDIF
												#ENDIF
												//If we're a boss and they are in my gang set a flag to say they might have given me a warning message
												IF bBoss
												AND GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), playerID)
													#IF IS_DEBUG_BUILD
														IF g_DoAssociatedWantedLevelPrints 
															PRINTLN("[GB_WANT] - bGoonGaveWanted = TRUE From player - ", tl63Name, ".")
														ENDIF
													#ENDIF
													bGoonGaveWanted = TRUE
													playerWhoGaveID = playerID
												ENDIF
												
												IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= iTempWantedLevel
													
													#IF IS_DEBUG_BUILD
														IF g_DoAssociatedWantedLevelPrints 
															PRINTLN("[Assoc Wanted Lvl] - player in in seat ", iSeat, " is net ok. They have a wanted level > 0.")
														ENDIF
													#ENDIF
													
													vWantedLoc = GET_PLAYER_WANTED_CENTRE_POSITION(playerID)
													
													IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(playerID),vWantedLoc) < GET_WANTED_LEVEL_RADIUS(iTempWantedLevel)
														
														#IF IS_DEBUG_BUILD
															IF g_DoAssociatedWantedLevelPrints 
																FLOAT fWantedRadius = GET_WANTED_LEVEL_RADIUS(iTempWantedLevel)
																VECTOR playerPos = GET_PLAYER_COORDS(playerID)
																PRINTLN("[Assoc Wanted Lvl] - player in in seat ", iSeat, " is inside their wanted radius.")
																PRINTLN("[Assoc Wanted Lvl] - player in in seat ", iSeat, " wanted radius = ", fWantedRadius)
																PRINTLN("[Assoc Wanted Lvl] - player in in seat ", iSeat, " wanted pos = ", vWantedLoc)
																PRINTLN("[Assoc Wanted Lvl] - player in in seat ", iSeat, " player pos = ", playerPos)
															ENDIF
														#ENDIF
														
														IF iHighestWantedLevel < iTempWantedLevel
															
															#IF IS_DEBUG_BUILD
																IF g_DoAssociatedWantedLevelPrints
																	PRINTLN("[Assoc Wanted Lvl] - player in in seat ", iSeat, " has a wanted level higher than current highest. Highest = ", iHighestWantedLevel, ", player's = ", iTempWantedLevel, ", closest position = ", fClosestPosition)
																ENDIF
															#ENDIF
														
															iHighestWantedLevel = iTempWantedLevel
															
															IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(playerID),vWantedLoc) < fClosestPosition
																
																#IF IS_DEBUG_BUILD
																	IF g_DoAssociatedWantedLevelPrints
																		PRINTLN("[Assoc Wanted Lvl] - player in in seat ", iSeat, " has a wanted level higher than current highest. Highest = ", iHighestWantedLevel)
																	ENDIF
																#ENDIF
															
																fClosestPosition = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(playerID),vWantedLoc)
																vNearestWantedPosition = GET_PLAYER_WANTED_CENTRE_POSITION(playerID)
															ENDIF
														ENDIF
													ELSE
														#IF IS_DEBUG_BUILD
															NET_PRINT("Ignoring associated wanted level as it is currently dropping ") NET_NL()
														#ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF

		// You can get only get wanted for shooting some guy once while he is alive....
		INT iWanted = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
		IF iHighestWantedLevel > iWanted
			
			#IF IS_DEBUG_BUILD
				IF g_DoAssociatedWantedLevelPrints
					PRINTLN("[Assoc Wanted Lvl] - highest wanted level = ", iHighestWantedLevel, ", which is > the local players wanted level of ", iWanted)
				ENDIF
			#ENDIF
																
			//Dave W, reset Lesters "Cops turn blind eye" ability if player in a vehicle with players with a wanted rating.
			RESET_LESTER_NO_COPS()
			
			IF bGoonGaveWanted 															
				#IF IS_DEBUG_BUILD
					IF g_DoAssociatedWantedLevelPrints
						PRINTLN("[GB_WANT] - bGoonGaveWanted = TRUE")
					ENDIF
				#ENDIF
				IF iWanted = 0
					#IF IS_DEBUG_BUILD
						IF g_DoAssociatedWantedLevelPrints
							PRINTLN("[GB_WANT] - iWanted = 0")
						ENDIF
					#ENDIF
					IF NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID(), TRUE)
						#IF IS_DEBUG_BUILD
							IF g_DoAssociatedWantedLevelPrints
								PRINTLN("[GB_WANT] -GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID(), TRUE)")
							ENDIF
						#ENDIF
						IF NOT sGangBossGaveWantedVars.bGangBossWantedHelpPrinted
							#IF IS_DEBUG_BUILD
								IF g_DoAssociatedWantedLevelPrints
									PRINTLN("[GB_WANT] -IF NOT sGangBossGaveWantedVars.bGangBossWantedHelpPrinted")
								ENDIF
							#ENDIF
							INT iPlayer = NATIVE_TO_INT(playerWhoGaveID)
							IF NOT IS_BIT_SET(sGangBossGaveWantedVars.iGaveWantedBitSet, iPlayer)
								SET_BIT(sGangBossGaveWantedVars.iGaveWantedBitSet, iPlayer)
								CPRINTLN(DEBUG_NET_MAGNATE, "[GB_WANT] - SET_BIT(sGangBossGaveWantedVars.iGaveWantedBitSet ,", iPlayer, ") -  ", GET_PLAYER_NAME(playerWhoGaveID), ")") 
							ELSE
								sGangBossGaveWantedVars.piPlayerWhoGaveWanted = playerWhoGaveID
								GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_GOON_GAVE_WANTED)
								CPRINTLN(DEBUG_NET_MAGNATE, "[GB_WANT] - GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_GOON_GAVE_WANTED) -  ", GET_PLAYER_NAME(playerWhoGaveID), ")") 
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),iHighestWantedLevel)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(),vWantedLoc)
			
			SET_BIT(MPGlobalsAmbience.iFmNmhBitSet3, BI_FM_NMH3_INHERIT_WANTED_DSP) // For help text
			
			#IF IS_DEBUG_BUILD
				NET_PRINT_STRING_VECTOR("Setting this player's wanted level  centre to : ",vWantedLoc) NET_NL()
				NET_PRINT("Setting player wanted due to association (in same car as wanted player)") NET_NL()
			#ENDIF
			MPGlobalsAmbience.vLocationWantedLevel = GET_PLAYER_COORDS(PLAYER_ID())
	
		ENDIF
		
		IF iHighestWantedLevel > 0 // if player had a wanted level
			
			#IF IS_DEBUG_BUILD
				IF g_DoAssociatedWantedLevelPrints
					PRINTLN("[Assoc Wanted Lvl] - highest wanted level = ", iHighestWantedLevel, ", which is > 0.")
				ENDIF
			#ENDIF
			
			IF iHighestWantedLevel = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())	// if wanted level is at its highest
			
				#IF IS_DEBUG_BUILD
					IF g_DoAssociatedWantedLevelPrints
						PRINTLN("[Assoc Wanted Lvl] - highest wanted level = player's wanted level.")
					ENDIF
				#ENDIF
			
				IF NOT ARE_VECTORS_EQUAL(vNearestWantedPosition,<<0,0,0>>)
					
					#IF IS_DEBUG_BUILD
						IF g_DoAssociatedWantedLevelPrints
							PRINTLN("[Assoc Wanted Lvl] - highest wanted level = player's wanted level.")
						ENDIF
					#ENDIF
					
					// if player is far from their wanted level centre point 
					IF GET_DISTANCE_BETWEEN_COORDS(vNearestWantedPosition,GET_PLAYER_COORDS(PLAYER_ID())) < GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID()),GET_PLAYER_COORDS(PLAYER_ID()))
						
						#IF IS_DEBUG_BUILD
							IF g_DoAssociatedWantedLevelPrints
								PRINTLN("[Assoc Wanted Lvl] - distance between local player and wanted level position is < than distance between player with highest wanted level anfd their wanted position.")
							ENDIF
						#ENDIF
					
						bUpdatePosition = TRUE	// update the players WANTED_CENTRE_POSITION
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//No one has wanted level
		IF iHighestWantedLevel = 0 AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			IF IS_BIT_SET(MPGlobalsAmbience.iLesterDisableCopsBitset, biNoCops_Activated)
				SET_PLAYERS_IN_VEHICLE_IN_NO_COPS_INFLUENCE()
			ENDIF
		ENDIF
		
		IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bOffTheRadar
			SET_PLAYERS_IN_VEHICLE_IN_OFF_THE_RADAR_INFLUENCE()
		ENDIF
		
		IF bUpdatePosition
			
			#IF IS_DEBUG_BUILD
				IF g_DoAssociatedWantedLevelPrints
					PRINTLN("[Assoc Wanted Lvl] - update wanted position = TRUE, setting local player's wanted position to be same as highest wanted player.")
				ENDIF
			#ENDIF
						
			#IF IS_DEBUG_BUILD
				NET_PRINT_STRING_VECTOR("Players current wanted level location :", GET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID())) NET_NL()
			#ENDIF
		
			SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(),vNearestWantedPosition)
			
			#IF IS_DEBUG_BUILD
				NET_PRINT_STRING_VECTOR("Updating this player's wanted level  centre to : ",vNearestWantedPosition) NET_NL()
				NET_PRINT("Setting player wanted due to association (in same car as wanted player)") NET_NL()
			#ENDIF
		ENDIF
		REINIT_NET_TIMER(MPGlobalsAmbience.AssociatedWantedLevelTimer)
	ENDIF
ENDPROC


/// PURPOSE:
///    Check if the player has kept a 5 star wanted level for a minute (Award XP)
PROC PROCESS_PLAYER_KEEPING_WANTED_LEVEL()

	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
		
		IF MPGlobalsAmbience.playerWantedState != PLAYER_WL_STATE_NULL
		AND MPGlobalsAmbience.playerWantedState != PLAYER_WL_STATE_RESETTING
		AND MPGlobalsAmbience.playerWantedState != PLAYER_WL_STATE_WARPING
			
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) >= 5
				
				IF MPGlobalsAmbience.b5StartWLTimerInit
					
					IF ABSI(GET_TIME_DIFFERENCE(MPGlobalsAmbience.time5WantedLevel, GET_NETWORK_TIME())) > 60000
					  	IF  MPGlobalsAmbience.icurrent_xp_reward_total <= 20 // 20*150 = 3000 xp
							IF GET_DISTANCE_BETWEEN_COORDS(MPGlobalsAmbience.vLocation1minXpRewardWantedLevel,  GET_PLAYER_COORDS(PLAYER_ID()), TRUE) >40.0
							AND NOT ARE_VECTORS_EQUAL(MPGlobalsAmbience.vLocation1minXpRewardWantedLevel,<<0,0,0>>)
								NET_PRINT("[JA@WANTED] PROCESS_PLAYER_KEEPING_WANTED_LEVEL GIVE_LOCAL_PLAYER_FM_XP_WITH_ANIMATION ") NET_NL()
								SET_XP_ANIM_TO_TRACK_PLAYER(TRUE)
								GIVE_LOCAL_PLAYER_FM_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "XPT_KEPT5STAR", XPTYPE_WANTED_LEVEL, XPCATEGORY_WANTED_LEVEL_KEPT_1MIN, ROUND(g_sMPTunables.fxp_tunable_Time_with_5star_Wanted_Level*50))
								MPGlobalsAmbience.icurrent_xp_reward_total++
								MPGlobalsAmbience.vLocation1minXpRewardWantedLevel = GET_PLAYER_COORDS(PLAYER_ID())								
							ELSE
								NET_PRINT("[JA@WANTED] PROCESS_PLAYER_KEEPING_WANTED_LEVEL Player has not moved far enough from last location where tehy were rewarded points ") NET_NL()
							ENDIF
						ELSE
							NET_PRINT("[JA@WANTED] reached 3000 cap ") NET_NL()
						ENDIF
						NET_PRINT("[JA@WANTED] PROCESS_PLAYER_KEEPING_WANTED_LEVEL - 5 star wanted level for 1 min. Award 150 XP ") NET_NL()
						MPGlobalsAmbience.time5WantedLevel = GET_NETWORK_TIME()
					ENDIF
					
				ELSE
				
					NET_PRINT("[JA@WANTED] PROCESS_PLAYER_KEEPING_WANTED_LEVEL - Initialise 5 star wanted level timer") NET_NL()
				
					MPGlobalsAmbience.time5WantedLevel = GET_NETWORK_TIME()
					MPGlobalsAmbience.b5StartWLTimerInit = TRUE
					MPGlobalsAmbience.vLocation1minXpRewardWantedLevel = GET_PLAYER_COORDS(PLAYER_ID())								
					
				ENDIF
				
			ELSE
				IF MPGlobalsAmbience.b5StartWLTimerInit
					NET_PRINT("[JA@WANTED] PROCESS_PLAYER_KEEPING_WANTED_LEVEL - Reset 5 star wanted level timer (no longer 5 stars)") NET_NL()
				
					MPGlobalsAmbience.b5StartWLTimerInit = FALSE
				ENDIF
			ENDIF
		ELSE
			IF MPGlobalsAmbience.b5StartWLTimerInit
				NET_PRINT("[JA@WANTED] PROCESS_PLAYER_KEEPING_WANTED_LEVEL - MPGlobalsAmbience.playerWantedState = PLAYER_WL_STATE_NULL") NET_NL()
			
				MPGlobalsAmbience.b5StartWLTimerInit = FALSE
			ENDIF
		ENDIF
	ELSE
		IF MPGlobalsAmbience.b5StartWLTimerInit
			NET_PRINT("[JA@WANTED] PROCESS_PLAYER_KEEPING_WANTED_LEVEL - Reset 5 star wanted level timer (dead)") NET_NL()
		
			MPGlobalsAmbience.b5StartWLTimerInit = FALSE
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL SHOULD_FORCE_SECURITY_VAN_SCRIPT_CLEANUP()
	
	IF IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


FUNC BOOL IS_PLAYER_IN_XP_FARMING_AREA()

	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<370.0802,-1597.4550,36.9488>>, <<30.0,30.0,20.0>>)			// FIX bug 1919047. [NT] [PUBLIC] [EXPLOIT] Hiding on top of the building next to the impound lot allows players to farm the 200RP for losing a two-star wanted level.	
		NET_PRINT("[JA@WANTED][KW@WANTED]: IS_PLAYER_IN_XP_FARMING_AREA: PLAYER IS FARMING XP on top of that building")NET_NL()
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC bool IS_PLAYER_IN_FREEMODE_OR_MISSION()

	IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_MISSION
		NET_PRINT("[JA@WANTED][KW@WANTED]: IS_PLAYER_IN_FREEMODE_OR_MISSION: PLAYER DOING AN ACTIVITY")NET_NL()
		RETURN FALSE
		
	ELSE
		NET_PRINT("[JA@WANTED][KW@WANTED]: IS_PLAYER_IN_FREEMODE_OR_MISSION: PLAYER NOT DOING AN ACTIVITY")NET_NL()
		RETURN TRUE
	ENDIF
ENDFUNC

PROC GET_WANTED_LEVEL_XP_VALUE(INT &iWantedXP, TEXT_LABEL_15 &sWantedStr)
	FLOAT fwantedxp
	SWITCH MPGlobalsAmbience.iXPWantedLevel
		CASE 1	
			fwantedxp = 100.0 * g_sMPTunables.fxp_tunable_Lose_Wanted_Level_1_star                                 
			sWantedStr = "XPT_LOSTWANTD1"
			iWantedXP = ROUND(fwantedxp)
		BREAK
		CASE 2	
			fwantedxp = 200.0 * g_sMPTunables.fxp_tunable_Lose_Wanted_Level_1_star 
			sWantedStr = "XPT_LOSTWANTD2"
			iWantedXP = ROUND(fwantedxp)
		BREAK
		CASE 3	
			fwantedxp = 300.0 * g_sMPTunables.fxp_tunable_Lose_Wanted_Level_1_star 
			sWantedStr = "XPT_LOSTWANTD3"
			iWantedXP = ROUND(fwantedxp)
		BREAK
		CASE 4	
			fwantedxp = 400.0 * g_sMPTunables.fxp_tunable_Lose_Wanted_Level_1_star 
			sWantedStr = "XPT_LOSTWANTD4"
			iWantedXP = ROUND(fwantedxp)
		BREAK
		CASE 5	
			fwantedxp = 500.0 * g_sMPTunables.fxp_tunable_Lose_Wanted_Level_1_star 
			sWantedStr = "XPT_LOSTWANTD5"
			iWantedXP = ROUND(fwantedxp)
		BREAK
	ENDSWITCH

ENDPROC


PROC PROCESS_GIVE_WANTED_LEVEL_SEEN_IN_STOLEN_VEHICLE(SCRIPT_TIMER &scriptTimer, INT &iStage, INT &iNoWantedTime)
	
	VEHICLE_INDEX tempVeh
	VEHICLE_MODEL_FAIL_ENUM eTempFailReason
	BOOL bDontRuncheck
	
	// Debug.
	#IF IS_DEBUG_BUILD
		STRING sTemp = "Mark current vehicle as not stolen."
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_W, KEYBOARD_MODIFIER_SHIFT, sTemp)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					SET_VEHICLE_IS_STOLEN(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)
					NET_PRINT("[WJK] - PROCESS_GIVE_WANTED_LEVEL_SEEN_IN_STOLEN_VEHICLE - shift+w pressed, setting current vehicle as not stolen.")NET_NL()
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
	// Monitor for being spotted. Once spotted make player unable to be spotted again for a random amunt of time.
	SWITCH iStage
		
		CASE 0
			
			IF NOT HAS_NET_TIMER_STARTED(scriptTimer)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
					AND GET_MAX_WANTED_LEVEL() != 0
						IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CAR_MOD)
							IF HAS_PLAYER_BEEN_SPOTTED_IN_STOLEN_VEHICLE(PLAYER_ID())
								tempVeh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
								IF IS_VEHICLE_SAFE_FOR_MOD_SHOP(tempVeh, FALSE, eTempFailReason)
								AND NOT FM_EVENT_IS_WANTED_LEVEL_SUPRESSED_BY_EVENT()
								OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
								OR Is_Player_Currently_On_MP_Heist(PLAYER_ID())
									IF DECOR_EXIST_ON(tempveh,"Player_Vehicle")
										IF DECOR_GET_INT(tempveh, "Player_Vehicle") = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
										OR DECOR_GET_INT(tempveh, "Player_Vehicle") = -1
											bDontRuncheck = TRUE
										ENDIF
									ENDIF
									IF NOT bDontRuncheck
										IF GET_RANDOM_INT_IN_RANGE(0, 4) = 1
											SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 1)
											SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
											SET_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_DISP_STOLEN_WANTED)
											iNoWantedTime = GET_RANDOM_INT_IN_RANGE(15000, 45000)
											NET_PRINT("[WJK] - PROCESS_GIVE_WANTED_LEVEL_SEEN_IN_STOLEN_VEHICLE - local player been spotted in a stolen vehicle, setting wanted level to 1.")NET_NL()
										ELSE
											iNoWantedTime = 45000
											NET_PRINT("[WJK] - PROCESS_GIVE_WANTED_LEVEL_SEEN_IN_STOLEN_VEHICLE - local player been spotted in a stolen vehicle, but random chance did not fall on 1, no wanted level.")NET_NL()
										ENDIF
										iStage++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 1
			
			IF NOT HAS_NET_TIMER_STARTED(scriptTimer)
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
					START_NET_TIMER(scriptTimer)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(scriptTimer, iNoWantedTime)
					NET_PRINT("[WJK] - PROCESS_GIVE_WANTED_LEVEL_SEEN_IN_STOLEN_VEHICLE - delay timer expired, resetting and going back to stage 0.")NET_NL()
					iNoWantedTime = 0
					RESET_NET_TIMER(scriptTimer)
					iStage = 0
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC MAINTAIN_SETTING_MY_STOLEN_VEHICLE_AS_WANTED(SCRIPT_TIMER &scriptTimer)
	
	PED_INDEX pedId = PLAYER_PED_ID()
	VEHICLE_INDEX tempVeh
	
	IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(scriptTimer, 2000)
		IF DOES_ENTITY_EXIST(pedId)
			IF NOT IS_ENTITY_DEAD(pedId)
				tempVeh = GET_VEHICLE_PED_IS_USING(pedId)
				IF DOES_ENTITY_EXIST(tempVeh)
					IF IS_VEHICLE_DRIVEABLE(tempVeh)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
							IF IS_VEHICLE_STOLEN(tempVeh)
								SET_VEHICLE_IS_WANTED(tempVeh, TRUE)
								NET_PRINT("---> Tempy temp temp.")NET_NL()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//This quickly gets the triathlon start heading.  



PROC MANAGE_CREW_EMBLEM(VEHICLE_INDEX tempveh)
	
	IF NOT DOES_VEHICLE_HAVE_CREW_EMBLEM_OR_ABORTED_DECORATOR(tempveh)
		IF HAS_LOADED_CREW_EMBLEM_FOR_VEHICLE_FOR_PLAYER(tempveh, PLAYER_ID())
			PRINTLN("MANAGE_CREW_EMBLEM: finished adding emblem")	
		ELSE
			PRINTLN("MANAGE_CREW_EMBLEM: adding emblem...")	
		ENDIF
	ENDIF
	
//CREWEMBLEMREQUESTSTATE eRequestState = CERS_NOT_ACTIVE
//
//
//	IF NOT MPGlobals.VehicleData.bCrewEmblem
//		IF NOT MPGlobals.VehicleData.bLoadingEmblem
//			IF NOT DOES_VEHICLE_HAVE_CREW_EMBLEM(tempveh)
//				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempveh)
//					IF ADD_VEHICLE_CREW_EMBLEM_USING_SCRIPT_POSITION(tempveh, PLAYER_PED_ID())
//						PRINTLN("MANAGE_CREW_EMBLEM: adding crew emblem")
//						MPGlobals.VehicleData.bLoadingEmblem = TRUE
//					ENDIF
//				ENDIF
//			ELSE
//				MPGlobals.VehicleData.bCrewEmblem = TRUE
//			ENDIF
//		ELSE
//			eRequestState = GET_VEHICLE_CREW_EMBLEM_REQUEST_STATE(tempveh)
//			PRINTLN("MANAGE_CREW_EMBLEM: checking request state")
//		    IF eRequestState = CERS_SUCCEEDED
//			OR eRequestState >= CERS_FAILED 
//				PRINTLN("MANAGE_CREW_EMBLEM: succeeded or failed")
//				MPGlobals.VehicleData.bLoadingEmblem = FALSE
//				MPGlobals.VehicleData.bCrewEmblem = TRUE
//			ENDIF
//		ENDIF
//	ENDIF
	
ENDPROC


PROC MAINTAIN_MP_PLAYER_POSITION()

	IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			g_TransitionSessionNonResetVars.PlayerMissionStartLocation = GET_ENTITY_COORDS(PLAYER_PED_ID())
			g_TransitionSessionNonResetVars.PlayerMissionStartHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_VEHICLE_IN_PLAYER_GARAGE(VEHICLE_INDEX theVeh)
	VECTOR vLoc = GET_ENTITY_COORDS(theVeh)
	IF GET_DISTANCE_BETWEEN_COORDS(vLoc,mpProperties[PROPERTY_HIGH_APT_1].garage.vMidPoint) < 30
		IF IS_POINT_IN_ANGLED_AREA(vLoc, mpProperties[PROPERTY_HIGH_APT_1].garage.Bounds.vPos1, 
													mpProperties[PROPERTY_HIGH_APT_1].garage.Bounds.vPos2, mpProperties[PROPERTY_HIGH_APT_1].garage.Bounds.fWidth)
			RETURN TRUE
		ENDIF
	ENDIF
	IF GET_DISTANCE_BETWEEN_COORDS(vLoc,mpProperties[PROPERTY_MEDIUM_APT_1].garage.vMidPoint) < 30
		IF IS_POINT_IN_ANGLED_AREA(vLoc, mpProperties[PROPERTY_MEDIUM_APT_1].garage.Bounds.vPos1, 
													mpProperties[PROPERTY_MEDIUM_APT_1].garage.Bounds.vPos2, mpProperties[PROPERTY_MEDIUM_APT_1].garage.Bounds.fWidth)
			RETURN TRUE
		ENDIF
	ENDIF
	IF GET_DISTANCE_BETWEEN_COORDS(vLoc,mpProperties[PROPERTY_LOW_APT_1].garage.vMidPoint) < 30
		IF IS_POINT_IN_ANGLED_AREA(vLoc, mpProperties[PROPERTY_LOW_APT_1].garage.Bounds.vPos1, 
													mpProperties[PROPERTY_LOW_APT_1].garage.Bounds.vPos2, mpProperties[PROPERTY_LOW_APT_1].garage.Bounds.fWidth)
			RETURN TRUE
		ENDIF
	ENDIF
	
	// office garages
	INT i = PROPERTY_OFFICE_1_GARAGE_LVL1
	WHILE (i <= PROPERTY_OFFICE_4_GARAGE_LVL3)
	
		IF GET_DISTANCE_BETWEEN_COORDS(vLoc,mpProperties[i].house.vMidPoint) <= 40
			IF IS_POINT_IN_ANGLED_AREA( vLoc, mpProperties[i].house.Bounds[0].vPos1, 
															mpProperties[i].house.Bounds[0].vPos2, mpProperties[i].house.Bounds[0].fWidth)
			OR IS_POINT_IN_ANGLED_AREA( vLoc, mpProperties[i].house.Bounds[1].vPos1, 
															mpProperties[i].house.Bounds[1].vPos2, mpProperties[i].house.Bounds[1].fWidth)
			OR IS_POINT_IN_ANGLED_AREA( vLoc, mpProperties[i].house.Bounds[2].vPos1, 
															mpProperties[i].house.Bounds[2].vPos2, mpProperties[i].house.Bounds[2].fWidth)
				RETURN TRUE												
			ENDIF
		ENDIF		
	
		i++
	ENDWHILE
	


	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_MP_NON_SIMPLE_INTEIROR_PROPERTY(PLAYER_INDEX playerID, BOOL bOwnedOnly)
	IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty > 0
		IF bOwnedOnly
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_MP_PROPERTY(PLAYER_INDEX playerID, BOOL bOwnedOnly, SIMPLE_INTERIOR_TYPE eExcludeSMPLInteriors = SIMPLE_INTERIOR_TYPE_INVALID)
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty > 0
		IF bOwnedOnly
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF globalPlayerBD[NATIVE_TO_INT(playerID)].SimpleInteriorBD.eCurrentSimpleInterior != SIMPLE_INTERIOR_INVALID
		IF eExcludeSMPLInteriors = SIMPLE_INTERIOR_TYPE_INVALID
		OR GET_SIMPLE_INTERIOR_TYPE(globalPlayerBD[NATIVE_TO_INT(playerID)].SimpleInteriorBD.eCurrentSimpleInterior) != eExcludeSMPLInteriors
			IF bOwnedOnly
				RETURN GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(playerID) = playerID
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_EXITING_PROPERTY(PLAYER_INDEX playerID) 
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)
ENDFUNC

FUNC BOOL IS_PLAYER_ENTERING_PROPERTY(PLAYER_INDEX playerID) 
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
ENDFUNC

FUNC BOOL IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_INDEX playerID) 
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)
		RETURN TRUE
	ENDIF
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC



/// PURPOSE: Maintain player losing a wanted level within FM. They must lose cops line of sight to receive XP
PROC PROCESS_PLAYER_LOSING_WANTED_LEVEL()

	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
	AND GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_PLAYING
	AND NOT IS_TRANSITION_ACTIVE() 
	AND NOT HAS_LOCAL_PLAYER_CHANGED_ACTIVE_CREW()
		INT iWantedXP
		TEXT_LABEL_15 sWLTicker
		VECTOR tempcoords
		#IF IS_DEBUG_BUILD
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F3, KEYBOARD_MODIFIER_NONE, "Cheat Check")
				MPGlobalsAmbience.iXPWantedLevel = 0
				MPGlobalsAmbience.playerWantedState = PLAYER_WL_STATE_NULL
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					SCRIPT_EVENT_DATA_TICKER_MESSAGE cheatTickerEventData
					cheatTickerEventData.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_WANTED_CLEAR
					cheatTickerEventData.playerID = PLAYER_ID()
					BROADCAST_TICKER_EVENT(cheatTickerEventData, ALL_PLAYERS())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_CLEAR_WANTED_LEVEL,ALL_PLAYERS_IN_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),FALSE))
						NET_PRINT("PROCESS_CHEAT_EVENT: Sent cheating event debug cleared wanted level")NET_NL()
					ENDIF
				ENDIF
			ENDIF
			
			//-- Dave W, for 1452709
			IF (GET_COMMANDLINE_PARAM_EXISTS("sc_noWantedLevel"))
				IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F5, KEYBOARD_MODIFIER_SHIFT, "Wanted Toggle")
					IF GET_MAX_WANTED_LEVEL() > 0
						SET_MAX_WANTED_LEVEL(0)
						PRINT_TICKER("WANT_TOGFF")
						PRINTLN("sc_noWantedLevel is set, setting max Level to 0") 
					ELSE
						PRINT_TICKER("WANT_TOGON")
						SET_MAX_WANTED_LEVEL(6)
						PRINTLN("sc_noWantedLevel is set, setting max Level to 6") 
					ENDIF
				ENDIF
			ENDIF
		#ENDIF
		
	
		SWITCH MPGlobalsAmbience.playerWantedState
		
			// Check to see if player is ever losing their wanted level
			CASE PLAYER_WL_STATE_NULL
			
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					MPGlobalsAmbience.iXPWantedLevel = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
					MPGlobalsAmbience.playerWantedState = PLAYER_WL_STATE_WANTED
					NET_NL() NET_PRINT("[JA@WANTED] PLAYER_WL_STATE_NULL WANTED LEVEL = ") 
					NET_PRINT_INT(MPGlobalsAmbience.iXPWantedLevel ) NET_NL()
				ENDIF
					
			BREAK
			
			CASE PLAYER_WL_STATE_WANTED
				IF g_sJoblistWarpMP.jlwIsActive
					UPDATE_PLAYER_WANTED_LEVEL_JOINING_MISSION()
				
				ENDIF
				// update MPGlobalsAmbience.iXPWantedLevel
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > MPGlobalsAmbience.iXPWantedLevel
					
					NET_NL() NET_PRINT("[JA@WANTED] WANTED LEVEL HAS GONE UP OLD = ") 
					NET_PRINT_INT(MPGlobalsAmbience.iXPWantedLevel ) NET_NL()
					
					MPGlobalsAmbience.iXPWantedLevel = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
					
					NET_NL() NET_PRINT("[JA@WANTED] WANTED LEVEL HAS GONE UP NEW = ") 
					NET_PRINT_INT(MPGlobalsAmbience.iXPWantedLevel ) NET_NL()
					
				ENDIF
			
			
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					NET_NL() NET_PRINT("[JA@WANTED] Player lost wanted level without leaving radius or getting out of sight") NET_NL()
					NET_NL() NET_PRINT("[JA@WANTED] PLAYER_WL_STATE_WANTED WANTED LEVEL = ") 
					NET_PRINT_INT(MPGlobalsAmbience.iXPWantedLevel ) NET_NL()
					MPGlobalsAmbience.playerWantedState = PLAYER_WL_STATE_NULL
				ENDIF
			
				IF ARE_PLAYER_FLASHING_STARS_ABOUT_TO_DROP(PLAYER_ID())
			
					NET_NL() NET_PRINT("[JA@WANTED] Player is about to lose a wanted level (stars flashing)") NET_NL()
					MPGlobalsAmbience.playerWantedState = PLAYER_WL_STATE_STARS_FLASHING
					
				ELIF ARE_PLAYER_STARS_GREYED_OUT(PLAYER_ID())
					
					NET_NL() NET_PRINT("[JA@WANTED] Player is about to lose a wanted level (stars greyed out)") NET_NL()
					MPGlobalsAmbience.playerWantedState = PLAYER_WL_STATE_STARS_GREYED
				ENDIF	
			BREAK
			
			// Player is outside their wanted radius
			CASE PLAYER_WL_STATE_STARS_FLASHING
				IF g_sJoblistWarpMP.jlwIsActive
					UPDATE_PLAYER_WANTED_LEVEL_JOINING_MISSION()
				
				ENDIF
				
				
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					
				
					IF NOT g_sJoblistWarpMP.jlwIsActive
					AND (g_bMissionRemovedWanted = FALSE )
					AND NOT IS_TRANSITION_SESSIONS_SETTING_UP_QUICKMATCH()
					AND NOT IS_GLOBAL_FLAG_BIT_SET(eGLOBALFLAGBITSET_INSIDE_MISSION_INTERIOR)
					AND NOT IS_GLOBAL_FLAG_BIT_SET(eGLOBALFLAGBITSET_EXITING_MISSION_INTERIOR)
						GET_WANTED_LEVEL_XP_VALUE(iWantedXP, sWLTicker)
					ELSE
					
						IF g_bMissionRemovedWanted = TRUE
							g_bMissionRemovedWanted = FALSE
							NET_PRINT("[JA@WANTED] Rowans just removed players wanted level in his mission controller No xp awarded") 
						ENDIF
						
						IF g_sJoblistWarpMP.jlwIsActive = TRUE
							NET_PRINT("[JA@WANTED] Player has lost their wanted level. by warping to a job. No xp awarded ") 
						ENDIF
						IF IS_TRANSITION_SESSIONS_SETTING_UP_QUICKMATCH()
							NET_PRINT("[JA@WANTED] Player has lost their wanted level. by warping to a quick job. No xp awarded ") 
						ENDIF
						
						IF IS_GLOBAL_FLAG_BIT_SET(eGLOBALFLAGBITSET_INSIDE_MISSION_INTERIOR)
							NET_PRINT("[JA@WANTED] Player has lost their wanted level. by warping into a mission interior. No xp awarded")
						ENDIF
						
						IF IS_GLOBAL_FLAG_BIT_SET(eGLOBALFLAGBITSET_EXITING_MISSION_INTERIOR)
							NET_PRINT("[JA@WANTED] Player has lost their wanted level. by warping outside the mission interior. No xp awarded")
						ENDIF
						
						iWantedXP = 0
					ENDIF
					
					IF iWantedXP > 0
						IF GET_DISTANCE_BETWEEN_COORDS(MPGlobalsAmbience.vLocationWantedLevel,  GET_PLAYER_COORDS(PLAYER_ID()), TRUE) >40.0
						OR ARE_VECTORS_EQUAL(MPGlobalsAmbience.vLocationWantedLevel,<<0,0,0>>)
						
							IF NOT (IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD))
								IF NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
								AND NOT IS_PLAYER_ENTERING_PROPERTY(PLAYER_ID())
									IF NOT MPGlobalsAmbience.bBlockWantedLevelRP
									
										IF NOT IS_PLAYER_IN_PASSENGER_SEAT_WITH_ANOTHER_PLAYER_AS_DRIVER()

										
											SET_XP_ANIM_TO_TRACK_PLAYER(TRUE)
											//GIVE_LOCAL_PLAYER_FM_XP_WITH_ANIM_AT_VECTOR(eXPTYPE_STANDARD, GET_PLAYER_COORDS(PLAYER_ID()), sWLTicker, iWantedXP)
											
											
											GIVE_LOCAL_PLAYER_FM_XP(eXPTYPE_STANDARD,  sWLTicker, XPTYPE_WANTED_LEVEL, XPCATEGORY_WANTED_LEVEL_LOST, iWantedXP)
											
											#IF IS_DEBUG_BUILD
												NET_PRINT("[JA@WANTED] Player has lost their wanted level a. Award ") NET_PRINT_INT(iWantedXP) NET_PRINT("XP") NET_NL()
											#ENDIF
											
											PRINTLN("[JA@WANTED] MPGlobalsAmbience.iXPWantedLevel = ", MPGlobalsAmbience.iXPWantedLevel)
											PRINTLN("[JA@WANTED] g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDailyObjectiveXValue = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDailyObjectiveXValue)

											IF MPGlobalsAmbience.iXPWantedLevel >= g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDailyObjectiveXValue
												IF NOT MPGlobalsAmbience.bWantedStarsHidden
													SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_ESCAPE_X_WANTED_LEVEL)
												ENDIF
											ENDIF
										
										ELSE
											#IF IS_DEBUG_BUILD
												NET_PRINT("[JA@WANTED] Player has lost their wanted level a. Not given XP as they are in passenger seat ") 
											#ENDIF
										
										ENDIF
									
									ELSE
										#IF IS_DEBUG_BUILD
											NET_PRINT("[JA@WANTED] Player has lost their wanted level a. Not given XP as giving RP was blocked (lester call or invite) ") 
										#ENDIF
									
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
										NET_PRINT("[JA@WANTED] Player has lost their wanted level. Not given XP as they are in a property ") 
									#ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									NET_PRINT("[JA@WANTED] Player has lost their wanted level. Not given XP as they are in a carmod ") 
								#ENDIF
							ENDIF
						ELSE
							
							NET_PRINT("[JA@WANTED] Player has lost their wanted level. Not given any XP because they did not move ") 
						
						ENDIF
					ENDIF
					
					NET_NL() NET_PRINT("[JA@WANTED] PLAYER_WL_STATE_STARS_FLASHING WANTED LEVEL = ") 
					NET_PRINT_INT(MPGlobalsAmbience.iXPWantedLevel ) NET_NL()
					
					MPGlobalsAmbience.bBlockWantedLevelRP = FALSE
					MPGlobalsAmbience.iXPWantedLevel = 0
					MPGlobalsAmbience.playerWantedState = PLAYER_WL_STATE_NULL
				ENDIF
				
				IF NOT ARE_PLAYER_FLASHING_STARS_ABOUT_TO_DROP(PLAYER_ID())
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				
						NET_PRINT("[JA@WANTED] Player no longer losing wanted level (flashing stars) and still has one") NET_NL()
						
						MPGlobalsAmbience.bBlockWantedLevelRP = FALSE
						MPGlobalsAmbience.iXPWantedLevel = 0
						MPGlobalsAmbience.playerWantedState = PLAYER_WL_STATE_NULL
					ENDIF
				ENDIF
				
			BREAK
		
			// Player is out of sight and about to lose wanted level
			CASE PLAYER_WL_STATE_STARS_GREYED		
				IF g_sJoblistWarpMP.jlwIsActive
					UPDATE_PLAYER_WANTED_LEVEL_JOINING_MISSION()
				
				ENDIF
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					GET_WANTED_LEVEL_XP_VALUE(iWantedXP, sWLTicker)
					IF NOT g_sJoblistWarpMP.jlwIsActive
					AND NOT IS_TRANSITION_SESSIONS_SETTING_UP_QUICKMATCH()
					AND NOT IS_GLOBAL_FLAG_BIT_SET(eGLOBALFLAGBITSET_EXITING_MISSION_INTERIOR)
						GET_WANTED_LEVEL_XP_VALUE(iWantedXP, sWLTicker)
					ELSE
						
						IF IS_GLOBAL_FLAG_BIT_SET(eGLOBALFLAGBITSET_EXITING_MISSION_INTERIOR)
							NET_PRINT("[JA@WANTED] Player has lost their wanted level. by exiting (warping) outside the mission interior. No xp awarded ") 
						ELSE
							NET_PRINT("[JA@WANTED] Player has lost their wanted level. by warping to a job. No xp awarded ") 
						ENDIF
						
						iWantedXP = 0
					ENDIF
					IF iWantedXP > 0
					
						IF NOT MPGlobalsAmbience.bBlockWantedLevelRP
						
							IF NOT (IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD))
							AND NOT IS_PLAYER_IN_XP_FARMING_AREA()
							AND IS_PLAYER_IN_FREEMODE_OR_MISSION()
							AND NOT (g_bMissionRemovedWanted AND HAS_FM_CAR_MOD_TUT_BEEN_DONE())
								IF NOT IS_PLAYER_IN_PASSENGER_SEAT_WITH_ANOTHER_PLAYER_AS_DRIVER()
									IF NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
									AND NOT IS_PLAYER_ENTERING_PROPERTY(PLAYER_ID())
										SET_XP_ANIM_TO_TRACK_PLAYER(TRUE)
										GIVE_LOCAL_PLAYER_FM_XP_WITH_ANIM_AT_VECTOR(eXPTYPE_STANDARD, GET_PLAYER_COORDS(PLAYER_ID()), sWLTicker, XPTYPE_WANTED_LEVEL, XPCATEGORY_WANTED_LEVEL_LOST, iWantedXP)
								
										NET_PRINT("[JA@WANTED] 2 Player has lost their wanted level. Award ") NET_PRINT_INT(iWantedXP) NET_PRINT("XP") NET_NL()
									
										PRINTLN("[JA@WANTED] MPGlobalsAmbience.iXPWantedLevel = ", MPGlobalsAmbience.iXPWantedLevel)
										PRINTLN("[JA@WANTED] g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDailyObjectiveXValue = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDailyObjectiveXValue)

									
										IF MPGlobalsAmbience.iXPWantedLevel >= g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDailyObjectiveXValue
											IF NOT MPGlobalsAmbience.bWantedStarsHidden
												SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_ESCAPE_X_WANTED_LEVEL)
											ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
											NET_PRINT("[JA@WANTED] Player has lost their wanted level. PLAYER_WL_STATE_STARS_GREYED	no award in property ") 
										#ENDIF
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
										NET_PRINT("[JA@WANTED] Player has lost their wanted level. Not given XP as they are in passenger seat ") 
									#ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									IF (IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD))
										NET_PRINT("[JA@WANTED] Player has lost their wanted level. Not given XP as they are in a carmod ") 
									ENDIF
								#ENDIF
								
								IF g_bMissionRemovedWanted
									PRINTLN("[JA@WANTED] Did player loose wanted level via mission removal g_bMissionRemovedWanted = TRUE ")
									g_bMissionRemovedWanted = FALSE
								ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								NET_PRINT("[JA@WANTED] Player has lost their wanted level. Not given XP as giving RP was blocked (lester call or invite) ") 
							#ENDIF
						
						ENDIF
					
					ENDIF
				
					NET_NL() NET_PRINT("[JA@WANTED] PLAYER_WL_STATE_STARS_GREYED WANTED LEVEL = ") 
					NET_PRINT_INT(MPGlobalsAmbience.iXPWantedLevel ) NET_NL()
					
					MPGlobalsAmbience.bBlockWantedLevelRP = FALSE
					MPGlobalsAmbience.iXPWantedLevel = 0
					MPGlobalsAmbience.playerWantedState = PLAYER_WL_STATE_NULL					
					MPGlobalsAmbience.icurrent_xp_reward_total = 0
				ENDIF
				
				IF NOT ARE_PLAYER_STARS_GREYED_OUT(PLAYER_ID())
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
						tempcoords = GET_PLAYER_COORDS(PLAYER_ID())
						IF NOT IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(tempcoords)
						AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MISSION)
							NET_PRINT("[JA@WANTED] Player no longer losing wanted level (stars greyed out), check WL next frame") NET_NL()
							MPGlobalsAmbience.playerWantedState = PLAYER_WL_STATE_RESETTING
						ENDIF
					ENDIF
				ENDIF
			
			BREAK
			
			// Resetting state when player loses cops via LOS (takes frame to clear WL)
			CASE PLAYER_WL_STATE_RESETTING
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					GET_WANTED_LEVEL_XP_VALUE(iWantedXP, sWLTicker)
					IF NOT MPGlobalsAmbience.bBlockWantedLevelRP
						
						IF NOT (IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD))
							IF NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
							AND NOT IS_PLAYER_ENTERING_PROPERTY(PLAYER_ID())
								IF iWantedXP > 0
									IF NOT IS_PLAYER_IN_XP_FARMING_AREA()
									AND IS_PLAYER_IN_FREEMODE_OR_MISSION()
										SET_XP_ANIM_TO_TRACK_PLAYER(TRUE)
										GIVE_LOCAL_PLAYER_FM_XP_WITH_ANIM_AT_VECTOR(eXPTYPE_STANDARD, GET_PLAYER_COORDS(PLAYER_ID()), sWLTicker,XPTYPE_WANTED_LEVEL, XPCATEGORY_WANTED_LEVEL_LOST,  iWantedXP)
								
										NET_PRINT("[JA@WANTED] 1 Player has lost their wanted level. Award ") NET_PRINT_INT(iWantedXP) NET_PRINT("XP") NET_NL()
									
										PRINTLN("[JA@WANTED] MPGlobalsAmbience.iXPWantedLevel = ", MPGlobalsAmbience.iXPWantedLevel)
										PRINTLN("[JA@WANTED] g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDailyObjectiveXValue = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDailyObjectiveXValue)

										IF MPGlobalsAmbience.iXPWantedLevel >= g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iDailyObjectiveXValue
											IF NOT MPGlobalsAmbience.bWantedStarsHidden
												SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_ESCAPE_X_WANTED_LEVEL)
											ENDIF
										ENDIF
									
									ELSE
										#IF IS_DEBUG_BUILD
											NET_PRINT("[JA@WANTED] Player has lost their wanted level. Not given XP as they are in a farming area ") 
										#ENDIF
									ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									NET_PRINT("[JA@WANTED] Player has lost their wanted level.LAYER_WL_STATE_RESETTING no award in property ") 
								#ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								NET_PRINT("[JA@WANTED] Player has lost their wanted level. Not given XP as they are in a carmod ") 
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							NET_PRINT("[JA@WANTED] PLAYER_WL_STATE_RESETTING Player has lost their wanted level. Not given XP as giving RP was blocked (lester call or invite) ") 
						#ENDIF
					ENDIF
				ENDIF
				MPGlobalsAmbience.bBlockWantedLevelRP = FALSE
				MPGlobalsAmbience.icurrent_xp_reward_total = 0
				MPGlobalsAmbience.iXPWantedLevel = 0
				MPGlobalsAmbience.playerWantedState = PLAYER_WL_STATE_NULL
			BREAK
			
			CASE PLAYER_WL_STATE_WARPING
				
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					
					NET_PRINT("[JA@WANTED] Player has lost their wanted level via warping to corona / launching mission") NET_NL()
					MPGlobalsAmbience.bBlockWantedLevelRP = FALSE
					MPGlobalsAmbience.icurrent_xp_reward_total = 0
					MPGlobalsAmbience.iXPWantedLevel = 0
					MPGlobalsAmbience.playerWantedState = PLAYER_WL_STATE_NULL
				ENDIF
				
			BREAK

		ENDSWITCH		
	ELSE
		IF MPGlobalsAmbience.playerWantedState != PLAYER_WL_STATE_NULL
			MPGlobalsAmbience.bBlockWantedLevelRP = FALSE
			MPGlobalsAmbience.iXPWantedLevel = 0
			MPGlobalsAmbience.icurrent_xp_reward_total = 0
			MPGlobalsAmbience.playerWantedState = PLAYER_WL_STATE_NULL
		ENDIF
	ENDIF

ENDPROC


PROC MAINTAIN_BOMB_VEHICLES()

	BOOL bInVeh

	VEHICLE_INDEX currentveh
	VEHICLE_INDEX lastveh
	PLAYER_INDEX tempplayer
	INT iplayername
	INT irepeat

	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty > 0
	OR IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
		EXIT
	ENDIF
	
	IF GB_IS_GLOBAL_NON_BD_BIT1_SET(eGB_GLOBAL_NON_BD_BITSET_1_DISABLE_VEHICLE_PHONE_EXPLOSIVE_DEVICE)
		EXIT
	ENDIF
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			bInVeh = TRUE
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehPersonalVehicle)
	AND IS_VEHICLE_DRIVEABLE(MPGlobalsAmbience.vehPersonalVehicle)
		IF DECOR_EXIST_ON(MPGlobalsAmbience.vehPersonalVehicle,"bombowner")
			IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_WARP_IN)
				PRINTLN("MAINTAIN_BOMB_VEHICLES: preventing call CASINO_CAR_PARK_BS_WARP_IN")
				EXIT
			ELIF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_VEH_IN)
				PRINTLN("MAINTAIN_BOMB_VEHICLES: preventing call CASINO_CAR_PARK_BS_VEH_IN")
				EXIT
			ENDIF
			
			#IF FEATURE_TUNER
			IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_WARP_IN)
				PRINTLN("MAINTAIN_BOMB_VEHICLES: preventing call CAR_MEET_CAR_PARK_BS_WARP_IN")
				EXIT
			ELIF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_VEH_IN)
				PRINTLN("MAINTAIN_BOMB_VEHICLES: preventing call CAR_MEET_CAR_PARK_BS_VEH_IN")
				EXIT
			ENDIF
			
			IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_WARP_IN)
				PRINTLN("MAINTAIN_BOMB_VEHICLES: preventing call PRIVATE_CAR_MEET_CAR_PARK_BS_WARP_IN")
				EXIT
			ELIF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_VEH_IN)
				PRINTLN("MAINTAIN_BOMB_VEHICLES: preventing call PRIVATE_CAR_MEET_CAR_PARK_BS_VEH_IN")
				EXIT
			ENDIF
			#ENDIF
		ENDIF
	ENDIF

	
	IF HAS_VEHICLE_PHONE_EXPLOSIVE_DEVICE()
		
		IF IS_CALLING_CONTACT(CHAR_MP_DETONATEPHONE) 
			IF NOT HAS_NET_TIMER_STARTED(tdbombtimer)
				REINIT_NET_TIMER(tdbombtimer)
				PRINTLN(" RCC Starting call for car bomb")
			ENDIF
		ENDIF
		IF HAS_NET_TIMER_STARTED(tdbombtimer)
			IF NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdbombtimer) > 3000
					REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK(CHAR_MP_DETONATEPHONE,MULTIPLAYER_BOOK)
					DISABLE_CELLPHONE_THIS_FRAME_ONLY()
					RESET_NET_TIMER(tdbombtimer)
					PRINTLN(" RCC car bomb calling DETONATE_VEHICLE_PHONE_EXPLOSIVE_DEVICE")
					DETONATE_VEHICLE_PHONE_EXPLOSIVE_DEVICE()
				ENDIF
			ELSE
				RESET_NET_TIMER(tdbombtimer)
				PRINTLN(" RCC entering property, resetting timer.")
			ENDIF
		ENDIF
		
	ELSE
		IF IS_CONTACT_IN_PHONEBOOK(CHAR_MP_DETONATEPHONE,MULTIPLAYER_BOOK)
			PRINTLN(" RCC removing car bomb contact since HAS_VEHICLE_PHONE_EXPLOSIVE_DEVICE is false")
			REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK(CHAR_MP_DETONATEPHONE,MULTIPLAYER_BOOK)
		ENDIF
	ENDIF

	IF bInVeh
		IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = PLAYER_PED_ID()
			currentveh =GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(currentveh)
			AND IS_VEHICLE_DRIVEABLE(currentveh)
				//IF NETWORK_HAS_CONTROL_OF_ENTITY(currentveh)
					IF DECOR_IS_REGISTERED_AS_TYPE("bombdec",DECOR_TYPE_INT)                                           
					AND DECOR_IS_REGISTERED_AS_TYPE("bombowner",DECOR_TYPE_INT)
						IF DECOR_EXIST_ON(currentveh,"bombdec")
						AND DECOR_EXIST_ON(currentveh,"bombowner")
							IF DECOR_GET_INT(currentveh ,"bombdec") = 2							
								iplayername = GET_HASH_KEY(GET_PLAYER_NAME(PLAYER_ID()))
								IF DECOR_GET_INT(currentveh ,"bombowner") != iplayername								
									IF NETWORK_HAS_CONTROL_OF_ENTITY(currentveh)
								
										NET_PRINT("MAINTAIN_BOMB_VEHICLES-ignition decor exists: set to 2 - driver explode") NET_NL()
										FOR irepeat = 0 TO (NUM_NETWORK_PLAYERS-1)
											tempplayer = INT_TO_PLAYERINDEX(irepeat)
											IF IS_NET_PLAYER_OK(tempplayer,FALSE)
												iplayername = GET_HASH_KEY(GET_PLAYER_NAME(tempplayer))
												IF iplayername = DECOR_GET_INT(currentveh ,"bombowner")
													DECOR_REMOVE(currentveh,"bombdec")
													DECOR_REMOVE(currentveh,"bombowner")
													SET_VEHICLE_TIMED_EXPLOSION(currentveh,GET_PLAYER_PED(tempplayer),1)
													EXIT
												ENDIF
											ENDIF
										ENDFOR
										DECOR_REMOVE(currentveh,"bombdec")
										DECOR_REMOVE(currentveh,"bombowner")
										SET_VEHICLE_TIMED_EXPLOSION(currentveh,NULL,1)
										
									ELSE
										NET_PRINT("[personal_vehicle]  Requesting control of saved vehicle to explode") NET_NL()
										NETWORK_REQUEST_CONTROL_OF_ENTITY(currentveh)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				//ELSE
				//	NET_PRINT("[personal_vehicle]  Requesting control of saved vehicle to explode") NET_NL()
				//	NETWORK_REQUEST_CONTROL_OF_ENTITY(currentveh)
				//ENDIF
			ENDIF
		ENDIF
	ELSE
		//GET_LAST_PED_IN_VEHICLE_SEAT
		lastveh = GET_PLAYERS_LAST_VEHICLE()
		IF IS_VEHICLE_DRIVEABLE(lastveh)
			IF IS_VEHICLE_EMPTY(lastveh,TRUE)
				IF DECOR_IS_REGISTERED_AS_TYPE("bombdec",DECOR_TYPE_INT)
					IF DECOR_EXIST_ON(lastveh,"bombdec")
						IF DECOR_GET_INT(lastveh,"bombdec") = 1
							NET_PRINT("MAINTAIN_BOMB_VEHICLES- decor exists on last car: priming explosives") NET_NL()
							DECOR_SET_INT(lastveh,"bombdec",2)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	

ENDPROC

/////////////////////////////////////////////////////////
//--	 Saved Vehicle (the new player vehicle)		 --//
/////////////////////////////////////////////////////////

FUNC BOOL CAN_PLAYER_PURCHASE_MP_SAVED_VEHICLE()
	IF NOT IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_CREATED)
	AND NOT IS_BIT_SET(g_MpSavedVehicles[0].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED)
	//AND GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CHAR_PLYVEH_AVAILABLE)
	//AND GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CHAR_PLYVEH_DESTROYED)
	//AND NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CHAR_PLYVEH_PROCESSING)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



FUNC BOOL CREATE_MP_RACE_SAVED_VEHICLE(NETWORK_INDEX &NetID, VECTOR vCoords, FLOAT fHeading, BOOL bCleanupModel, BOOL bSetOnGround = TRUE)

	VEHICLE_INDEX tempveh
	MODEL_NAMES vehmodel

//	CLEANUP_MP_SAVED_VEHICLE(FALSE)
	
	//UPDATE_STORED_MP_SAVED_VEHICLE_DATA_FROM_TUS()
	
	IF GET_CUSTOM_VEHICLE_MODEL_NAME(g_iMyRaceModelChoice) = DUMMY_MODEL_FOR_SCRIPT
		#IF IS_DEBUG_BUILD
			PRINTLN("[personal_vehicle]  Player does not have a saved vehicle at this point.")
			SCRIPT_ASSERT("[personal_vehicle]  Player does not have a saved vehicle at this point.")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	//Request collision around model
	REQUEST_COLLISION_AT_COORD(vCoords)
	
//	IF NOT IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_CREATED)
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(NetID)
			IF CAN_REGISTER_MISSION_VEHICLES(1)
							
				vehmodel = GET_CUSTOM_VEHICLE_MODEL_NAME(g_iMyRaceModelChoice)

				IF REQUEST_LOAD_MODEL(vehmodel)
				
					// Create the vehicle

					PRINTLN("CREATE_MP_RACE_SAVED_VEHICLE vehmodel = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(vehmodel), " g_iMyRaceModelChoice = ", g_iMyRaceModelChoice)
					
					CREATE_NET_VEHICLE(NetID, vehmodel, vCoords, fHeading,FALSE)
//					SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV,PLAYER_ID(),TRUE) 
					tempveh= NET_TO_VEH(NetID)
					
					IF bSetOnGround
						PRINTLN("CREATE_MP_RACE_SAVED_VEHICLE vehmodel SET_VEHICLE_ON_GROUND_PROPERLY")
						SET_VEHICLE_ON_GROUND_PROPERLY(tempveh)
					ENDIF
					// Now set the specifics
					// Bug #437236 - Set all license plates for all profiles in SP and MP to have the same text when reading from the cloud data.
					//SET_VEHICLE_AS_COPY_OF_SAVED_VEHICLE(tempveh,FALSE)
					
					MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(tempVeh, g_iMyRaceModelChoice)	// 1126554
					
					SET_SETTINGS_FOR_SAVED_VEHICLE(tempveh)
					
					IF DECOR_IS_REGISTERED_AS_TYPE("Veh_Modded_By_Player", DECOR_TYPE_INT)
						PRINTLN("[personal_vehicle]  DECOR_SET_INT Veh_Modded_By_Player")
						DECOR_SET_INT(tempveh, "Veh_Modded_By_Player", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
					ENDIF
					
					INT iMPBitset
					IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
						IF DECOR_EXIST_ON(tempveh, "MPBitset")	
							iMPBitset = DECOR_GET_INT(tempVeh, "MPBitset")
						ENDIF
						SET_BIT(iMPBitset, MP_DECORATOR_BS_RACE_VEHICLE)
						DECOR_SET_INT(tempVeh, "MPBitset", iMPBitset)
					ENDIF
					
					
					// make sure the vehicle is registered to this player. 2074320
					IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
					AND DECOR_EXIST_ON(tempveh, "Player_Vehicle")
						PRINTLN("[personal_vehicle] removing pv decorator for race.")
						DECOR_REMOVE(tempveh, "Player_Vehicle")
					ENDIF
					
					IF bCleanupModel
						SET_MODEL_AS_NO_LONGER_NEEDED(vehmodel)
					ENDIF

					
					RETURN TRUE
				ELSE
					PRINTLN("[personal_vehicle]  Waiting for model to load")
				ENDIF
			ELSE
				PRINTLN("[personal_vehicle]  net id already exists")
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
//	ELSE
//		PRINTLN("[personal_vehicle]  Already created, waiting for cleanup")
//	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL GET_SPAWN_COORDS_FOR_FM_SAVED_VEHICLE(VECTOR &vCoords, FLOAT &fHeading)
	// For now just find the nearest shops parking space...
	RETURN GET_SHOP_PARKING_SPACE(GET_CLOSEST_SHOP_OF_TYPE(GET_ENTITY_COORDS(PLAYER_PED_ID()), SHOP_TYPE_CARMOD), vCoords, fHeading)
ENDFUNC

FUNC BOOL HAS_CLEANED_UP_PREVIOUS_SAVED_VEHICLE_AND_IS_GOOD_TO_CREATE()
	IF NOT IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_CREATION_INIT)
		NET_PRINT("[personal_vehicle] HAS_CLEANED_UP_PREVIOUS_SAVED_VEHICLE_AND_IS_GOOD_TO_CREATE - calling CLEANUP_MP_SAVED_VEHICLE") NET_NL()
		CLEANUP_MP_SAVED_VEHICLE(FALSE)
		SET_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_CREATION_INIT)
	ENDIF
	IF NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
		NET_PRINT("[personal_vehicle] HAS_CLEANED_UP_PREVIOUS_SAVED_VEHICLE_AND_IS_GOOD_TO_CREATE - returning TRUE") NET_NL()
		RETURN(TRUE)
	ENDIF
	NET_PRINT("[personal_vehicle] HAS_CLEANED_UP_PREVIOUS_SAVED_VEHICLE_AND_IS_GOOD_TO_CREATE - returning FALSE") NET_NL()
	RETURN(FALSE)
ENDFUNC

PROC DISABLE_PERSONAL_VEHICLE_CREATION_THIS_FRAME()
	PRINTLN("[personal_vehicle] DISABLE_PERSONAL_VEHICLE_CREATION_THIS_FRAME - called. ")
	DEBUG_PRINTCALLSTACK()
	MPGlobals.VehicleData.bSuspendPVCreationThisFrame = TRUE
ENDPROC

FUNC BOOL IS_PERSONAL_VEHICLE_CREATION_DISABLED_THIS_FRAME()
	RETURN MPGlobals.VehicleData.bSuspendPVCreationThisFrame
ENDFUNC

FUNC BOOL IS_PERSONAL_VEHICLE_CREATION_DISABLED_FOR_TRANSITION()
	RETURN g_bDisablePerVehCreateForTransition
ENDFUNC

PROC DISABLE_PERSONAL_VEHICLE_CREATION_FOR_TRANSITION(BOOL bSet)
	IF NOT (g_bDisablePerVehCreateForTransition	= bSet)
		g_bDisablePerVehCreateForTransition	= bSet
		PRINTLN("[personal_vehicle] DISABLE_PERSONAL_VEHICLE_CREATION_FOR_TRANSITION - changing g_bDisablePerVehCreateForTransition to ", g_bDisablePerVehCreateForTransition)
		DEBUG_PRINTCALLSTACK()
	ENDIF
ENDPROC


FUNC BOOL ARE_ALL_PERSONAL_VEHICLES_DISABLED_FOR_MISSION_STARTUP()
	RETURN g_SpawnData.MissionSpawnPersonalVehicleData.bDisablePersonalVehicleCreationForMissionStartup
ENDFUNC

PROC HIDE_PERSONAL_VEHICLES(BOOL bSet)
	g_SpawnData.MissionSpawnPersonalVehicleData.bHidePersonalVehicles = bSet
	PRINTLN("[personal_vehicle] HIDE_PERSONAL_VEHICLES - called with ", bSet)
	DEBUG_PRINTCALLSTACK()
ENDPROC

PROC SetPersonalVehicleNoSpawnZone(VECTOR vCoords1, VECTOR vCoords2, FLOAT fFloat, INT iShape)

	VECTOR vMin, vMax
	
	// makes sure the points are ordered min and max
	IF (vCoords1.x < vCoords2.x)
		vMin.x = vCoords1.x 
		vMax.x = vCoords2.x
	ELSE
		vMin.x = vCoords2.x 
		vMax.x = vCoords1.x
	ENDIF
	IF (vCoords1.y < vCoords2.y)
		vMin.y = vCoords1.y 
		vMax.y = vCoords2.y
	ELSE
		vMin.y = vCoords2.y 
		vMax.y = vCoords1.y
	ENDIF	
	IF (vCoords1.z < vCoords2.z)
		vMin.z = vCoords1.z 
		vMax.z = vCoords2.z
	ELSE
		vMin.z = vCoords2.z 
		vMax.z = vCoords1.z
	ENDIF		
	
	g_SpawnData.MissionSpawnPersonalVehicleData.vPersonalVehicleNoSpawnZone_Min = vMin
	g_SpawnData.MissionSpawnPersonalVehicleData.vPersonalVehicleNoSpawnZone_Max = vMax
	g_SpawnData.MissionSpawnPersonalVehicleData.fPersonalVehicleNoSpawnFloat = fFloat
	g_SpawnData.MissionSpawnPersonalVehicleData.iPersonalVehicleNoSpawnShape = iShape
	
	g_SpawnData.MissionSpawnPersonalVehicleData.bPersonalVehicleNoSpawnZoneEnabled = TRUE
	PRINTLN("[personal_vehicle] SetPersonalVehicleNoSpawnZone - called with ", vCoords1, vCoords2, fFloat, iShape)
	DEBUG_PRINTCALLSTACK()	
ENDPROC

PROC SET_PERSONAL_VEHICLE_NO_SPAWN_ANGLED_AREA(VECTOR vCoords1, VECTOR vCoords2, FLOAT fFloat)
	SetPersonalVehicleNoSpawnZone(vCoords1, vCoords2, fFloat, SPAWN_AREA_SHAPE_ANGLED)
ENDPROC

PROC SET_PERSONAL_VEHICLE_NO_SPAWN_BOX(VECTOR vCoords1, VECTOR vCoords2)
	SetPersonalVehicleNoSpawnZone(vCoords1, vCoords2, 0.0, SPAWN_AREA_SHAPE_BOX)
ENDPROC

PROC SET_PERSONAL_VEHICLE_NO_SPAWN_SPHERE(VECTOR vCoords1, FLOAT fFloat)
	SetPersonalVehicleNoSpawnZone(vCoords1, <<0.0, 0.0, 0.0>>, fFloat, SPAWN_AREA_SHAPE_CIRCLE)
ENDPROC



PROC CLEAR_PERSONAL_VEHICLE_NO_SPAWN_ZONE()
	g_SpawnData.MissionSpawnPersonalVehicleData.vPersonalVehicleNoSpawnZone_Min = <<0.0, 0.0, 0.0>>
	g_SpawnData.MissionSpawnPersonalVehicleData.vPersonalVehicleNoSpawnZone_Max = <<0.0, 0.0, 0.0>>
	g_SpawnData.MissionSpawnPersonalVehicleData.fPersonalVehicleNoSpawnFloat = 0.0
	g_SpawnData.MissionSpawnPersonalVehicleData.iPersonalVehicleNoSpawnShape = 0
	g_SpawnData.MissionSpawnPersonalVehicleData.bPersonalVehicleNoSpawnZoneEnabled = FALSE
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[personal_vehicle] CLEAR_PERSONAL_VEHICLE_NO_SPAWN_ZONE called")
ENDPROC


PROC DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION(BOOL bSet)
	IF NOT (g_SpawnData.MissionSpawnPersonalVehicleData.bDisablePersonalVehicleCreation = bSet)
		g_SpawnData.MissionSpawnPersonalVehicleData.bDisablePersonalVehicleCreation = bSet
		PRINTLN("[personal_vehicle] DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION - changing g_SpawnData.MissionSpawnPersonalVehicleData.bDisablePersonalVehicleCreation to ", g_SpawnData.MissionSpawnPersonalVehicleData.bDisablePersonalVehicleCreation)
		DEBUG_PRINTCALLSTACK()
	ENDIF
ENDPROC

PROC DISABLE_PERSONAL_VEHICLE_AMBIENT_CREATION_DURING_MISSION(BOOL bSet)
	IF bSet
		IF NOT g_bDisableAmbientSpawnOfPersonalVehicleOnMission
			g_bDisableAmbientSpawnOfPersonalVehicleOnMission = TRUE
			PRINTLN("[personal_vehicle] DISABLE_PERSONAL_VEHICLE_AMBIENT_CREATION_DURING_MISSION - g_bDisableAmbientSpawnOfPersonalVehicleOnMission = TRUE")
		ENDIF
	ELSE
		IF g_bDisableAmbientSpawnOfPersonalVehicleOnMission
			g_bDisableAmbientSpawnOfPersonalVehicleOnMission = FALSE
			PRINTLN("[personal_vehicle] DISABLE_PERSONAL_VEHICLE_AMBIENT_CREATION_DURING_MISSION - g_bDisableAmbientSpawnOfPersonalVehicleOnMission = FALSE")
		ENDIF		
	ENDIF
ENDPROC

PROC DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION_STARTUP(BOOL bSet)
	
	IF NOT (g_SpawnData.MissionSpawnPersonalVehicleData.bDisablePersonalVehicleCreationForMissionStartup_timerInitialised)
		g_SpawnData.MissionSpawnPersonalVehicleData.TimePersonalVehicleCreationForMissionStartup = GET_NETWORK_TIME_ACCURATE()
		g_SpawnData.MissionSpawnPersonalVehicleData.bDisablePersonalVehicleCreationForMissionStartup_timerInitialised = TRUE
		PRINTLN("[personal_vehicle] DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION_STARTUP - initialising timer ")		
	ENDIF

	IF NOT (g_SpawnData.MissionSpawnPersonalVehicleData.bDisablePersonalVehicleCreationForMissionStartup = bSet)
		g_SpawnData.MissionSpawnPersonalVehicleData.bDisablePersonalVehicleCreationForMissionStartup = bSet
		g_SpawnData.MissionSpawnPersonalVehicleData.TimePersonalVehicleCreationForMissionStartup = GET_NETWORK_TIME_ACCURATE()
		PRINTLN("[personal_vehicle] DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION_STARTUP - changing g_SpawnData.MissionSpawnPersonalVehicleData.bDisablePersonalVehicleCreationForMissionStartup to ", g_SpawnData.MissionSpawnPersonalVehicleData.bDisablePersonalVehicleCreationForMissionStartup)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	// mission has enabled the pv creation, but it will need to know when its safe to continue and will check IS_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION
	IF NOT (bSet)
		g_SpawnData.MissionSpawnPersonalVehicleData.bMissionWaitingForPVCreation = TRUE
	ENDIF
ENDPROC

FUNC BOOL IS_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
	IF NOT (g_SpawnData.MissionSpawnPersonalVehicleData.bMissionWaitingForPVCreation)
	OR IS_FAKE_MULTIPLAYER_MODE_SET()
		#IF IS_DEBUG_BUILD
			PRINTLN("[personal_vehicle] IS_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION - returnign TRUE, g_SpawnData.MissionSpawnPersonalVehicleData.bMissionWaitingForPVCreation = ", g_SpawnData.MissionSpawnPersonalVehicleData.bMissionWaitingForPVCreation)
			PRINTLN("[personal_vehicle] IS_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION - returnign TRUE, IS_FAKE_MULTIPLAYER_MODE_SET() = ", IS_FAKE_MULTIPLAYER_MODE_SET())
		#ENDIF		
		RETURN(TRUE)
	ELSE	
		#IF IS_DEBUG_BUILD
			PRINTLN("[personal_vehicle] IS_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION - g_SpawnData.MissionSpawnPersonalVehicleData.bMissionOKToProceedAfterPVCreation = ", g_SpawnData.MissionSpawnPersonalVehicleData.bMissionOKToProceedAfterPVCreation)
		#ENDIF
		IF (g_SpawnData.MissionSpawnPersonalVehicleData.bMissionOKToProceedAfterPVCreation)
			INT iTimeDiff = GET_GAME_TIMER() - g_SpawnData.MissionSpawnPersonalVehicleData.iTimerOKToProceedAfterPVCreation
			#IF IS_DEBUG_BUILD
				PRINTLN("[personal_vehicle] IS_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION - iTimeDiff = ", iTimeDiff)
			#ENDIF
			IF (iTimeDiff > 500)
			OR (iTimeDiff < 0) // in case something has gone wrong
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC	

FUNC BOOL ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
	INT i
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
		IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
			IF NOT IS_PLAYER_SPECTATING(PlayerID)
				IF NOT (GlobalplayerBD[i].bPVSpawnedForMission)
					PRINTLN("[personal_vehicle] ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION - waiting on player ", i)
					RETURN(FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN(TRUE)
ENDFUNC

PROC SET_PERSONAL_VEHICLE_HEIST_PROPERTY_SPAWN(INT iProperty, BOOL bIsQuickRestart)
	IF NOT (iProperty = g_SpawnData.MissionSpawnPersonalVehicleData.iPVPropertySpawn)
		g_SpawnData.MissionSpawnPersonalVehicleData.iPVPropertySpawn = iProperty
		g_SpawnData.MissionSpawnPersonalVehicleData.bPVIsQuickRestart = bIsQuickRestart
		PRINTLN("SET_PERSONAL_VEHICLE_HEIST_PROPERTY_SPAWN - iProperty = ", iProperty, ", bIsQuickRestart = ", bIsQuickRestart)
	ENDIF
ENDPROC

FUNC BOOL IS_OK_TO_SPAWN_VEHICLE_AT_START_OF_HEIST_MISSION()

	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		RETURN(TRUE)
	ENDIF

	IF (g_SpawnData.MissionSpawnPersonalVehicleData.iPVPropertySpawn > 0)
	AND NOT (g_SpawnData.MissionSpawnPersonalVehicleData.bPVIsQuickRestart)
	AND NOT (g_SpawnData.MissionSpawnPersonalVehicleData.bMissionWaitingForPVCreation)
	
		#IF IS_DEBUG_BUILD
			IF NOT (g_SpawnData.MissionSpawnPersonalVehicleData.iPVPropertySpawn > 0)
				PRINTLN("[personal_vehicle] IS_OK_TO_SPAWN_VEHICLE_AT_START_OF_HEIST_MISSION - TRUE - g_SpawnData.MissionSpawnPersonalVehicleData.iPVPropertySpawn = ", g_SpawnData.MissionSpawnPersonalVehicleData.iPVPropertySpawn)
			ENDIF
			IF (g_SpawnData.MissionSpawnPersonalVehicleData.bPVIsQuickRestart)
				PRINTLN("[personal_vehicle] IS_OK_TO_SPAWN_VEHICLE_AT_START_OF_HEIST_MISSION - TRUE - g_SpawnData.MissionSpawnPersonalVehicleData.bPVIsQuickRestart = ", g_SpawnData.MissionSpawnPersonalVehicleData.bPVIsQuickRestart)
			ENDIF
			IF (g_SpawnData.MissionSpawnPersonalVehicleData.bMissionWaitingForPVCreation) 
				PRINTLN("[personal_vehicle] IS_OK_TO_SPAWN_VEHICLE_AT_START_OF_HEIST_MISSION - TRUE - g_SpawnData.MissionSpawnPersonalVehicleData.bMissionWaitingForPVCreation = ", g_SpawnData.MissionSpawnPersonalVehicleData.bMissionWaitingForPVCreation)			
			ENDIF		
		#ENDIF	
	
		RETURN(TRUE)
	ENDIF
	#IF IS_DEBUG_BUILD
		IF NOT (g_SpawnData.MissionSpawnPersonalVehicleData.iPVPropertySpawn > 0)
			PRINTLN("[personal_vehicle] IS_OK_TO_SPAWN_VEHICLE_AT_START_OF_HEIST_MISSION - NOT (g_SpawnData.MissionSpawnPersonalVehicleData.iPVPropertySpawn > 0)")
		ENDIF
		IF (g_SpawnData.MissionSpawnPersonalVehicleData.bPVIsQuickRestart)
			PRINTLN("[personal_vehicle] IS_OK_TO_SPAWN_VEHICLE_AT_START_OF_HEIST_MISSION - (g_SpawnData.MissionSpawnPersonalVehicleData.bPVIsQuickRestart)")
		ENDIF
		IF (g_SpawnData.MissionSpawnPersonalVehicleData.bMissionWaitingForPVCreation) 
			PRINTLN("[personal_vehicle] IS_OK_TO_SPAWN_VEHICLE_AT_START_OF_HEIST_MISSION - g_SpawnData.MissionSpawnPersonalVehicleData.bMissionWaitingForPVCreation")			
		ENDIF		
	#ENDIF
	RETURN(FALSE)
ENDFUNC



FUNC BOOL IS_ON_HEIST_CORONA()
	IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
	OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
		IF GET_CORONA_STATUS() = corona_status_team_dm 
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC REMOVE_PLAYERS_TOP_HAT_IF_IN_A_VEHICLE(BOOL bIgnore_car_check = FALSE)	// sets players none items as unlocked/ Gives player helmet too.

	PED_COMP_ITEM_DATA_STRUCT sItemData
	IF GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD)) != DUMMY_PED_COMP
		sItemData = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD)))
		
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		OR bignore_car_check = TRUE
			VEHICLE_INDEX aVeh 
			IF bignore_car_check = FALSE
				aVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			ENDIF
			
			SWITCH GET_HASH_KEY(sItemData.sLabel)
			      CASE HASH("CLO_BUS_P_1_0")
			      CASE HASH("CLO_BUS_P_1_1")
			      CASE HASH("CLO_BUS_P_1_2")
			      CASE HASH("CLO_BUS_P_1_3")
			      CASE HASH("CLO_BUS_P_1_4")
			      CASE HASH("CLO_BUS_P_1_5")
			      CASE HASH("CLO_BUS_P_1_6")
			      CASE HASH("CLO_BUS_P_1_7")
			      CASE HASH("CLO_BUS_P_1_8")
			      CASE HASH("CLO_BUS_P_1_9")
			      CASE HASH("CLO_BUS_P_1_10")
			      CASE HASH("CLO_BUS_P_1_11")
			      CASE HASH("CLO_BUS_P_1_12")
			      CASE HASH("CLO_BUS_P_1_13")
				CASE HASH("CLO_X2M_HT_0_0") //Classic Tree
				CASE HASH("CLO_X2M_HT_0_1")	//	Purple Tree
				CASE HASH("CLO_X2M_HT_0_2")	//	Holly Tree
				CASE HASH("CLO_X2M_HT_0_3")//	Red Stripy Tree
				CASE HASH("CLO_X2M_HT_0_4")//Green Stripy Tree
				CASE HASH("CLO_X2M_HT_0_5")//Star Tree
				CASE HASH("CLO_X2M_HT_0_6")	//Santa Tree
				CASE HASH("CLO_X2M_HT_0_7")
				
				CASE HASH("CLO_X2F_HT_0_0") //Classic Tree
				CASE HASH("CLO_X2F_HT_0_1")	//	Purple Tree
				CASE HASH("CLO_X2F_HT_0_2")	//	Holly Tree
				CASE HASH("CLO_X2F_HT_0_3")//	Red Stripy Tree
				CASE HASH("CLO_X2F_HT_0_4")//Green Stripy Tree
				CASE HASH("CLO_X2F_HT_0_5")//Star Tree
				CASE HASH("CLO_X2F_HT_0_6")	//Santa Tree
				CASE HASH("CLO_X2F_HT_0_7")
				CASE HASH("HT_FMM_13_0")
				CASE HASH("HT_FMM_13_1")
				CASE HASH("HT_FMM_13_2")
				CASE HASH("HT_FMM_13_3")
				CASE HASH("HT_FMM_13_4")
				CASE HASH("HT_FMM_13_5")
				CASE HASH("HT_FMM_13_6")
				CASE HASH("HT_FMM_13_7")
				CASE HASH("CLO_BBF_P2_0")
				CASE HASH("CLO_BBF_P2_1")
				CASE HASH("CLO_BBF_P2_2")
				CASE HASH("CLO_BBF_P2_3")
				CASE HASH("CLO_BBF_P2_4")
				CASE HASH("CLO_BBF_P2_5")
				CASE HASH("CLO_BBF_P2_6")
			            // top hat

						IF bignore_car_check = FALSE
							IF DOES_VEHICLE_HAVE_ROOF(aVeh)
								CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
								PRINTLN("REMOVE_PLAYERS_TOP_HAT_IF_IN_A_VEHICLE removing hat")
							ELSE
								PRINTLN("REMOVE_PLAYERS_TOP_HAT_IF_IN_A_VEHICLE DOES_VEHICLE_HAVE_ROOF = FALSE")
							ENDIF
						ELSE
							CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
							PRINTLN("REMOVE_PLAYERS_TOP_HAT_IF_IN_A_VEHICLE bignore_car_check = TRUE")
						ENDIF
					
			      BREAK
			ENDSWITCH
		ENDIF
	ELSE
		PRINTLN("REMOVE_PLAYERS_TOP_HAT_IF_IN_A_VEHICLE head returned DUMMY_PED_COMP, therefore do nothing")
	ENDIF
				
ENDPROC

#IF FEATURE_TUNER
FUNC BOOL CAN_THIS_VEHICLE_MODEL_ENTER_CAR_MEET(MODEL_NAMES eModel)
	SWITCH eModel
		CASE OPPRESSOR2
		CASE CARACARA
		CASE ZHABA
		CASE APC
		CASE BARRAGE
		CASE DUNE3
		CASE TAMPA3
		CASE MENACER
		CASE HALFTRACK
		CASE CERBERUS
		CASE CERBERUS2
		CASE CERBERUS3
		CASE SCARAB
		CASE SCARAB2
		CASE SCARAB3
		CASE BRUISER
		CASE BRUISER2
		CASE BRUISER3
		CASE MONSTER3
		CASE MONSTER4
		CASE MONSTER5
		CASE TECHNICAL
		CASE TECHNICAL2
		CASE TECHNICAL3
		CASE INSURGENT
		CASE INSURGENT3
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_PLAYER_STARTED_WARP_INTO_CAR_MEET_CAR_PARK()
	RETURN IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_WARP_IN)
ENDFUNC

FUNC BOOL HAS_PLAYER_STARTED_WARP_INTO_PRIVATE_CAR_MEET_CAR_PARK()
	RETURN IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_WARP_IN)
ENDFUNC
#ENDIF

#IF FEATURE_DLC_2_2022
FUNC BOOL CAN_THIS_VEHICLE_MODEL_ENTER_JUGGALO_HIDEOUT(MODEL_NAMES eModel)
	SWITCH eModel
		CASE JOURNEY
		CASE BRICKADE
		CASE MANCHEZ
		DEFAULT
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL CREATE_MP_SAVED_VEHICLE(VECTOR vCoords, FLOAT fHeading, BOOL bCleanupModel, INT iSaveSlotID, BOOL bUseTheseExactCoordsIfPossible=FALSE, BOOL bForceExactCoords = FALSE, BOOL bStartOfMission=FALSE)

	VEHICLE_INDEX tempveh
	MODEL_NAMES vehmodel
	INT i

	IF NOT IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_CREATION_INIT)
		CLEANUP_MP_SAVED_VEHICLE(FALSE)
		SET_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_CREATION_INIT)
	ENDIF

	IF g_MpSavedVehicles[iSaveSlotID].vehicleSetupMP.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[personal_vehicle]  Player does not have a saved vehicle at this point.")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	
	
	IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlotID].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
		IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
			SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
		ENDIF
	ENDIF
	
	SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_BEING_CREATED)
	
	IF NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
		IF NOT IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_CREATED)
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(PERSONAL_VEHICLE_NET_ID())
				IF CAN_REGISTER_MISSION_VEHICLES(1)
					IF NOT IS_ANY_TYPE_OF_CUTSCENE_PLAYING() // don't create during a cutscene - see 2119400
						IF NOT IS_ON_HEIST_CORONA() // don't create pv until finished selecting which one from heist conrona
							
							// check if the vehicle stored was a commercial version of the vehicle, if so convert it to the street version
							RUN_COMMERCIAL_VEHICLE_IN_SLOT_CHECK(iSaveSlotID)		
							
							vehmodel = g_MpSavedVehicles[iSaveSlotID].vehicleSetupMP.VehicleSetup.eModel
							
							#IF USE_TU_CHANGES
							IF NOT IS_MODEL_VALID_FOR_PERSONAL_VEHICLE(vehmodel)
							OR NOT IS_VEHICLE_AVAILABLE_FOR_GAME(vehmodel, TRUE)
								IF NOT IS_VEHICLE_AVAILABLE_PAST_CURRENT_TIME(vehmodel)
									#IF IS_DEBUG_BUILD
										NET_PRINT("CREATE_MP_SAVED_VEHICLE - trying to create a vehicle that shouldn't be available past current POSIX time.") NET_NL()
										SCRIPT_ASSERT("CREATE_MP_SAVED_VEHICLE: trying to create a vehicle that shouldn't be available past current POSIX time.")
									#ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
										NET_PRINT("CREATE_MP_SAVED_VEHICLE - trying to create an illegal personal vehicle.") NET_NL()
										SCRIPT_ASSERT("CREATE_MP_SAVED_VEHICLE: trying to create an illegal personal vehicle..")
									#ENDIF
									NET_PRINT("CREATE_MP_SAVED_VEHICLE - deleting illegal vehicle.") NET_NL()
									CLEAR_MP_SAVED_VEHICLE_SLOT(iSaveSlotID)
								ENDIF
								RETURN(TRUE)
							ELSE
							#ENDIF
								IF REQUEST_LOAD_MODEL(vehmodel)
								
									NET_PRINT("[personal_vehicle] CREATE_MP_SAVED_VEHICLE(")
									NET_PRINT_VECTOR(vCoords)
									NET_PRINT(", ")
									NET_PRINT_FLOAT(fHeading)
									NET_PRINT(", ")
									NET_PRINT_BOOL(bCleanupModel)
									NET_PRINT(", ")
									NET_PRINT_INT(iSaveSlotID)
									NET_PRINT(", ")
									NET_PRINT_BOOL(bUseTheseExactCoordsIfPossible)
									NET_PRINT(", ")
									NET_PRINT_BOOL(bForceExactCoords)
									NET_PRINT(", ")
									NET_PRINT_BOOL(bStartOfMission)
									NET_PRINT(")") NET_NL()
									

									// Check if we should ignore visibility checks for start of mission spawn...
									BOOL bStartOfMissionSpawn = FALSE
									IF (GET_CLOUD_TIME_AS_INT() - MPGlobals.VehicleData.iMissionStartTime) < 15
										PRINTLN("[personal_vehicle]  CREATE_MP_SAVED_VEHICLE - MPGlobals.VehicleData.iMissionStartTime - this is start of mission.")
										bStartOfMissionSpawn = TRUE
									ENDIF
									IF IS_CORONA_INITIALISING_A_QUICK_RESTART()
										PRINTLN("[personal_vehicle]  CREATE_MP_SAVED_VEHICLE - IS_CORONA_INITIALISING_A_QUICK_RESTART - this is start of mission.")
										bStartOfMissionSpawn = TRUE
									ENDIF
									IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), g_SpawnData.MissionSpawnPersonalVehicleData.TimePersonalVehicleCreationForMissionStartup) < 15000)
										PRINTLN("[personal_vehicle]  CREATE_MP_SAVED_VEHICLE - g_SpawnData.MissionSpawnPersonalVehicleData.TimePersonalVehicleCreationForMissionStartup - time diff = ", GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), g_SpawnData.MissionSpawnPersonalVehicleData.TimePersonalVehicleCreationForMissionStartup))
										g_SpawnData.MissionSpawnPersonalVehicleData.TimePersonalVehicleCreationForMissionStartup = GET_NETWORK_TIME_ACCURATE() // so once we establish that this is the start of a mission it doesn't change midway through getting coords.
										bStartOfMissionSpawn = TRUE
									ENDIF
									PRINTLN("[personal_vehicle]  CREATE_MP_SAVED_VEHICLE - g_SpawnData.MissionSpawnPersonalVehicleData.TimePersonalVehicleCreationForMissionStartup - time diff = ", GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), g_SpawnData.MissionSpawnPersonalVehicleData.TimePersonalVehicleCreationForMissionStartup))										

									// or if override is set
									IF (bStartOfMission)
										PRINTLN("[personal_vehicle]  CREATE_MP_SAVED_VEHICLE - start of mission override flag set.")
										bStartOfMissionSpawn = TRUE
									ENDIF
											
									BOOL bAllowFallbackToInactiveNodes
									IF (g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCleanupGoingToWarpIntoPv)
										bAllowFallbackToInactiveNodes = FALSE
										bStartOfMissionSpawn = TRUE // be less strict with visibility. 2069517
										NET_PRINT("[personal_vehicle]  g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCleanupGoingToWarpIntoPv = TRUE") NET_NL()
									ELSE
										bAllowFallbackToInactiveNodes = TRUE
										NET_PRINT("[personal_vehicle]  g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCleanupGoingToWarpIntoPv = FALSE") 	NET_NL()							
									ENDIF	
									
									BOOL bCheckVisibilityToLocalPlayer = TRUE
									IF IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
									OR (IS_SCREEN_FADED_OUT() OR IS_PLAYER_SWITCH_IN_PROGRESS())
										bCheckVisibilityToLocalPlayer = FALSE
										NET_PRINT("[personal_vehicle]  bCheckVisibilityToLocalPlayer = FALSE") 	NET_NL()	
									ENDIF
								
									// if the post mission cleanup is to warp player into pv then make sure it's not created back at mors. 1984485
									IF (g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCleanupGoingToWarpIntoPv)
										IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlotID].iVehicleBS, MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL)
											CLEAR_BIT(g_MpSavedVehicles[iSaveSlotID].iVehicleBS, MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL)	
											PRINTLN("[personal_vehicle]  post mission cleanup, warping into vehicle, so dont spawn at mors. ")
										ENDIF
									ENDIF
								
									BOOL bFoundCoords = FALSE
									IF (bForceExactCoords)
										bFoundCoords = TRUE
									ELIF IS_BIT_SET(g_MpSavedVehicles[iSaveSlotID].iVehicleBS, MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL)
										
										PRINTLN("[personal_vehicle]  Grabbing spawn coords at Mors Mutual")
										
										VEHICLE_SPAWN_LOCATION_PARAMS Params
										Params.fMaxDistance = 50.0
										Params.bConsiderOnlyActiveNodes = FALSE
										Params.bAvoidSpawningInExclusionZones = FALSE
										Params.bAllowFallbackToInactiveNodes=bAllowFallbackToInactiveNodes
										Params.bStartOfMissionPVSpawn=bStartOfMissionSpawn
										Params.bCheckEntityArea = TRUE
										Params.bCheckOwnVisibility = bCheckVisibilityToLocalPlayer
										Params.bIsForPV = TRUE
										Params.bCheckThisPlayerVehicle = FALSE
										REPEAT MAX_NUM_AVOID_RADIUS i 
											Params.vAvoidCoords[i] = MPGlobals.VehicleData.vAvoidCoords[i]
											Params.fAvoidRadius[i] = MPGlobals.VehicleData.fAvoidRadius[i]
										ENDREPEAT
										
										// West side at Mors
										IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(<<-145.7451, -1167.9371, 24.2819>>,<<0.0, 0.0, 0.0>>, vehmodel, TRUE,vCoords,fHeading, Params)																
											PRINTLN("[personal_vehicle]  Found coords at west side")
											bFoundCoords = TRUE
										// East side at Mors
										ELIF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(<<-234.9622, -1171.1130, 21.8657>>,<<0.0, 0.0, 0.0>>,vehmodel, TRUE,vCoords,fHeading, Params) 
											PRINTLN("[personal_vehicle]  Found coords at east side")
											bFoundCoords = TRUE
										ENDIF										
									
									ELIF IS_BIT_SET(g_MpSavedVehicles[iSaveSlotID].iVehicleBS, MP_SAVED_VEHICLE_IMPOUNDED)
									
										PRINTLN("[personal_vehicle]  Grabbing spawn coords at impound depot")
										
										IF GET_IMPOUND_COORDS_FOR_SAVED_VEHICLE(vCoords, fHeading)
											PRINTLN("[personal_vehicle]  Found coords at impound")
											bFoundCoords = TRUE
										ENDIF
										
									ELSE
										
										IF (bUseTheseExactCoordsIfPossible)
											IF IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(vCoords))	
												// trying to spawn in an interior, not allowed.
												NET_PRINT("[personal_vehicle]  Trying to Create saved Player Vehicle using exact coords, but point is in interior, not allowed -") NET_PRINT_VECTOR(vCoords) NET_NL()
												bUseTheseExactCoordsIfPossible = FALSE
											ENDIF
										ENDIF
									
										
									
										IF NOT (bUseTheseExactCoordsIfPossible)	
										
											VEHICLE_SPAWN_LOCATION_PARAMS Params
											Params.fMinDistFromCoords = 10.0
											Params.bAllowFallbackToInactiveNodes=bAllowFallbackToInactiveNodes
											Params.bStartOfMissionPVSpawn=bStartOfMissionSpawn
											Params.bCheckEntityArea = TRUE
											Params.bCheckOwnVisibility = bCheckVisibilityToLocalPlayer
											Params.bIsForPV = TRUE
											Params.bCheckThisPlayerVehicle = FALSE
											REPEAT MAX_NUM_AVOID_RADIUS i 
												Params.vAvoidCoords[i] = MPGlobals.VehicleData.vAvoidCoords[i]
												Params.fAvoidRadius[i] = MPGlobals.VehicleData.fAvoidRadius[i]
											ENDREPEAT
											
											IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vCoords,<<0.0, 0.0, 0.0>>,vehmodel,TRUE,vCoords,fHeading, Params)
												bFoundCoords = TRUE
											ENDIF
										ELSE

											NET_PRINT("[personal_vehicle]  Trying to Create saved Player Vehicle using exact coords... : ") NET_PRINT_VECTOR(vCoords) NET_NL()

											VEHICLE_SPAWN_LOCATION_PARAMS Params
											Params.bUseExactCoordsIfPossible = bUseTheseExactCoordsIfPossible
											Params.bAllowFallbackToInactiveNodes=bAllowFallbackToInactiveNodes
											Params.bStartOfMissionPVSpawn=bStartOfMissionSpawn
											Params.bCheckEntityArea = TRUE
											Params.bCheckOwnVisibility = bCheckVisibilityToLocalPlayer
											Params.bIsForPV = TRUE
											Params.bCheckThisPlayerVehicle = FALSE
											REPEAT MAX_NUM_AVOID_RADIUS i 
												Params.vAvoidCoords[i] = MPGlobals.VehicleData.vAvoidCoords[i]
												Params.fAvoidRadius[i] = MPGlobals.VehicleData.fAvoidRadius[i]
											ENDREPEAT

											IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vCoords,<<0.0, 0.0, 0.0>>,vehmodel,TRUE,vCoords,fHeading, Params)
												bFoundCoords = TRUE
											ENDIF

										ENDIF
									ENDIF
								
									IF bFoundCoords
										// Create the vehicle
										#IF IS_DEBUG_BUILD
											DEBUG_PRINTCALLSTACK()
											PRINTLN("...IS_PERSONAL_VEHICLE_CREATION_DISABLED_THIS_FRAME() = ", IS_PERSONAL_VEHICLE_CREATION_DISABLED_THIS_FRAME())
											PRINTLN("...IS_PERSONAL_VEHICLE_CREATION_DISABLED_FOR_TRANSITION() = ", IS_PERSONAL_VEHICLE_CREATION_DISABLED_FOR_TRANSITION())
											PRINTLN("...IS_PLAYER_IN_CORONA() = ", IS_PLAYER_IN_CORONA())
											PRINTLN("...Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID()) = ", Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID()))
											PRINTLN("...Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID()) = ", Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID()))
											PRINTLN("...Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID()) = ", Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID()))
											PRINTLN("...IS_ON_RACE_GLOBAL_SET() = ", IS_ON_RACE_GLOBAL_SET())
										#ENDIF
										PRINTLN("[personal_vehicle]  Creating saved Player Vehicle from save slot #",iSaveSlotID ," at : ", vCoords, ", heading = ", fHeading)
										
										CLEAR_AREA(vCoords, 2.0, TRUE)
										
										// clear any ambient vehicle's intersecting my car
																				

										CREATE_NET_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV,vehmodel,vCoords, fHeading,FALSE,TRUE,FALSE)
										SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV,PLAYER_ID(),TRUE) 										DEBUG_PRINTCALLSTACK()
																													
										//SET_NETWORK_ID_PASS_CONTROL_IN_TUTORIAL(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV,TRUE)
										
										tempveh= NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV)
										
										SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempveh, TRUE)
										SET_CLEAR_FREEZE_WAITING_ON_COLLISION_ONCE_PLAYER_ENTERS(tempveh, TRUE)	
										
										SET_VEHICLE_ON_GROUND_PROPERLY(tempveh)
										
										MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(tempveh,iSaveSlotID)
										
										IF IS_PLAYER_LEAVING_SIMPLE_INTERIOR_USING_THE_VALET()
										#IF FEATURE_TUNER
										OR HAS_PLAYER_STARTED_WARP_INTO_CAR_MEET_CAR_PARK()
										OR HAS_PLAYER_STARTED_WARP_INTO_PRIVATE_CAR_MEET_CAR_PARK()
										#ENDIF
											FREEZE_ENTITY_POSITION(tempveh, TRUE)
										ENDIF	
										
										SET_PV_DECORATOR(tempveh,iSaveSlotID)
										
										// fade in entity if coming from a fade, player switch or mission cleanup. 2109755
										IF NOT (bCheckVisibilityToLocalPlayer)
											NETWORK_FADE_IN_ENTITY(tempveh, TRUE)
										ENDIF
										
										// should we flash the blip on creation?
										IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlotID].iVehicleBS, MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL)
											SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
											PRINTLN("[personal_vehicle] Setting pv blip to flash")
										ELSE
											CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
										ENDIF
										
										BOOL bUpdatedSlot
										IF iSaveSlotID >= 0 
				                   		AND iSaveSlotID < MAX_MP_SAVED_VEHICLES 
											SET_BIT(g_MpSavedVehicles[iSaveSlotID].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
											PRINTLN("CDM_MP: Setting vehicle",iSaveSlotID," as out of garage")
											INT iVehSlot
											REPEAT MAX_MP_SAVED_VEHICLES iVehSlot
												bUpdatedSlot = FALSE
												IF iVehSlot != iSaveSlotID
													
													IF IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL)
														PRINTLN("CM_MP - clearing MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL for vehicle in slot ", iVehSlot)
														CLEAR_BIT(g_MpSavedVehicles[iVehSlot].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL)
														bUpdatedSlot = TRUE
													ENDIF
													IF IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
														PRINTLN("CM_MP - clearing MP_SAVED_VEHICLE_OUT_GARAGE for vehicle in slot ", iVehSlot)
														CLEAR_BIT(g_MpSavedVehicles[iVehSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
														bUpdatedSlot = TRUE
													ENDIF
													IF IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
														NET_PRINT("KR_MP - Destroying impounded vehicle as we have a new PV") NET_NL()
														SET_SAVE_VEHICLE_AS_DESTROYED(iVehSlot)
														bUpdatedSlot = TRUE
													ENDIF
													IF bUpdatedSlot
														MP_SAVE_VEHICLE_SLOT_STATS_FROM_SAVEGAME(iVehSlot,g_MpSavedVehicles[iVehSlot] ,TRUE)
													ENDIF
												ENDIF
											ENDREPEAT
										ENDIF
										IF IS_VEHICLE_A_CONVERTIBLE(tempveh)
											IF IS_BIT_SET(g_TransitionSessionNonResetVars.MissionPVBitset,ci_PV_CONV_ROOF_DOWN)
												LOWER_CONVERTIBLE_ROOF(tempveh,TRUE)
											ENDIF
										ENDIF
										MP_SAVE_VEHICLE_SLOT_STATS_FROM_SAVEGAME(iSaveSlotID,g_MpSavedVehicles[iSaveSlotID] ,TRUE)
										SET_SETTINGS_FOR_SAVED_VEHICLE(tempveh)
										
										SET_VEHICLE_DOORS_LOCKED(tempveh,VEHICLELOCK_UNLOCKED)
										
										IF bCleanupModel
											SET_MODEL_AS_NO_LONGER_NEEDED(vehmodel)
										ENDIF
										
										// Update flags
										SET_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_CREATED)
										SET_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_JUST_CREATED)
										CLEAR_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_RADIO_STATION_SET)
										CLEAR_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_CLEANUP)
										CLEAR_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_CLEANUP_DELETE)
										CLEAR_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_DESTROYED_HELP)
										CLEAR_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
										//CLEAR_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_START_OF_MISSION_SPAWN)
										CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_BEING_CREATED)
										CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CLEANUP_RETURN_TO_GARAGE)
										CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_RETURN_FROM_GARAGE_AFTER_MISSION)
										CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CONCEAL_IN_PROPERTY)
										CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CLEANUP_SET_AS_DESTROYED)
										CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CLEANUP_NOT_DURING_RESPAWN)
										
										CLEAR_NEXT_PERSONAL_VEHICLE_CREATION_TO_AVOID_RADIUS()
										
										GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].brequestvehiclespawnslot = FALSE
									
										RETURN TRUE
										
									ELSE
										PRINTLN("[personal_vehicle]  Cannot find spawn coords for vehicle")
									ENDIF
								ELSE
									PRINTLN("[personal_vehicle]  Waiting for model to load")
								ENDIF
							#IF USE_TU_CHANGES
							ENDIF
							#ENDIF
						ELSE	
							PRINTLN("[personal_vehicle]  cannot create heist corona")	
						ENDIF
					ELSE
						PRINTLN("[personal_vehicle]  cannot create during cutscene")
					ENDIF
				ELSE
					PRINTLN("[personal_vehicle]  cannot register vehicle")
				ENDIF
			ELSE
				PRINTLN("[personal_vehicle]  net id already exists")
			ENDIF
		ELSE
			PRINTLN("[personal_vehicle]  Already created, waiting for cleanup")
		ENDIF
	ELSE
		PRINTLN("[personal_vehicle]  waiting for old pv to finish cleaning up")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_WORLD_POINT_OF_PLAYER(PLAYER_INDEX PlayerID)
	INT iProperty
	VECTOR vCoords
	IF g_bRunUpdated_GET_WORLD_POINT_OF_PLAYER
		IF IS_PLAYER_IN_CUSTOM_DISPLACED_INTERIOR(PlayerID)
			vCoords = GET_PLAYER_CUSTOM_DISPLACED_INTERIOR_COORDS(PlayerID)
			IF (VMAG(vCoords)> 0.0)
				RETURN vCoords
			ELSE
				PRINTLN("GET_WORLD_POINT_OF_PLAYER - custom displaced coords are zero")
			ENDIF
		ENDIF
		IF (GlobalplayerBD[NATIVE_TO_INT(PlayerID)].playerBlipData.bInsideDisplacedInterior)	
			IF (VMAG(GlobalplayerBD[NATIVE_TO_INT(PlayerID)].playerBlipData.vDisplacedPerceivedCoords) > 0.0)
				RETURN GlobalplayerBD[NATIVE_TO_INT(PlayerID)].playerBlipData.vDisplacedPerceivedCoords
			ELSE
				PRINTLN("GET_WORLD_POINT_OF_PLAYER - vDisplacedPerceivedCoords coords are zero")	
			ENDIF	
		ENDIF
	ENDIF
		
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PlayerID)].propertyDetails.iCurrentlyInsideProperty > 0
		vCoords = GET_MP_PROPERTY_BUILDING_WORLD_POINT(GET_PROPERTY_BUILDING(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty))
		IF (VMAG(vCoords)> 0.0)
			RETURN vCoords
		ELSE
			PRINTLN("GET_WORLD_POINT_OF_PLAYER - property building world point coords are zero (1) ")
		ENDIF		
	ELSE
		GET_APARTMENT_INTERIOR_I_AM_IN_BASED_ON_COORDS(GET_PLAYER_COORDS(PlayerID),iProperty)
		IF iproperty > 0
			vCoords = GET_MP_PROPERTY_BUILDING_WORLD_POINT(GET_PROPERTY_BUILDING(iProperty))
			IF (VMAG(vCoords)> 0.0)
				RETURN vCoords
			ELSE
				PRINTLN("GET_WORLD_POINT_OF_PLAYER - property building world point coords are zero (2)")
			ENDIF
		ENDIF
	ENDIF	
	IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PlayerID)
		vCoords = GET_BLIP_COORD_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PlayerID)
		IF (VMAG(vCoords)> 0.0)
			RETURN vCoords
		ELSE
			PRINTLN("GET_WORLD_POINT_OF_PLAYER - simple interior coords are zero")
		ENDIF
	ENDIF
	IF IS_PLAYER_DOING_SIMPLE_INTERIOR_AUTOWARP(PlayerID)
		SIMPLE_INTERIORS eWarpingToInterior = GET_SIMPLE_INTERIOR_PLAYER_IS_AUTOWARPING_TO(PlayerID)
		PRINTLN("GET_WORLD_POINT_OF_PLAYER - autowarping to interior ", ENUM_TO_INT(eWarpingToInterior))
		IF (ENUM_TO_INT(eWarpingToInterior) > -1)	
			vCoords = g_SimpleInteriorData.vMidPoints[ENUM_TO_INT(eWarpingToInterior)]
			IF (VMAG(vCoords)> 0.0)
				RETURN vCoords
			ELSE
				PRINTLN("GET_WORLD_POINT_OF_PLAYER - simple interior (autowarp) coords are zero")
			ENDIF			
		ENDIF
	ENDIF
	IF NOT g_bRunUpdated_GET_WORLD_POINT_OF_PLAYER
		IF IS_PLAYER_IN_CUSTOM_DISPLACED_INTERIOR(PlayerID)
			vCoords = GET_PLAYER_CUSTOM_DISPLACED_INTERIOR_COORDS(PlayerID)
			IF (VMAG(vCoords)> 0.0)
				RETURN vCoords
			ELSE
				PRINTLN("GET_WORLD_POINT_OF_PLAYER - custom displaced coords are zero")
			ENDIF
		ENDIF
		IF (GlobalplayerBD[NATIVE_TO_INT(PlayerID)].playerBlipData.bInsideDisplacedInterior)	
			IF (VMAG(GlobalplayerBD[NATIVE_TO_INT(PlayerID)].playerBlipData.vDisplacedPerceivedCoords) > 0.0)
				RETURN GlobalplayerBD[NATIVE_TO_INT(PlayerID)].playerBlipData.vDisplacedPerceivedCoords
			ELSE
				PRINTLN("GET_WORLD_POINT_OF_PLAYER - vDisplacedPerceivedCoords coords are zero")	
			ENDIF	
		ENDIF
	ENDIF
	RETURN GET_PLAYER_COORDS(PlayerID)
ENDFUNC



/// PURPOSE: Gets the coords for a player based on where the local player 'thinks' the other player should be. i.e. their blip position, this takes into account if they are in a displaced apartement.
FUNC VECTOR GET_PLAYER_PERCEIVED_COORDS(PLAYER_INDEX PlayerID)
	VECTOR vCoords
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(PlayerID)])
		vCoords = GET_ACTUAL_BLIP_COORDS(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(PlayerID)])
	ELIF (DOES_BLIP_EXIST(g_PlayerBlipsData.deadPlayerBlips[NATIVE_TO_INT(PlayerID)]) AND NOT (PlayerID = MPGlobals.LocalPlayerID))
		vCoords = GET_ACTUAL_BLIP_COORDS(g_PlayerBlipsData.deadPlayerBlips[NATIVE_TO_INT(PlayerID)])
	ENDIF
	IF VMAG(vCoords) > 0.0
		RETURN(vCoords)
	ENDIF
	RETURN GET_WORLD_POINT_OF_PLAYER(PlayerID)
	//RETURN GET_WORLD_POINT_OF_PLAYER(PlayerID)
ENDFUNC

CONST_INT MAX_NUM_TRUCK_WORK_MISSION_VARIATIONS		10
FUNC VECTOR GET_TRUCK_WORK_MISSION_COORD(INT iFMMC, INT iVariation)
	SWITCH iFMMC
		// CHALLENGES
		CASE FMMC_TYPE_FMBB_SECURITY_VAN
//			SWITCH iVariation
//				CASE 0					
//			ENDSWITCH
		BREAK

		
		// WORKS
		CASE FMMC_TYPE_FMBB_JEWEL_STORE_GRAB
			SWITCH iVariation
				CASE 0			RETURN <<-631.8972, -237.8486, 37.0734>>		
			ENDSWITCH
		BREAK
		CASE FMMC_TYPE_FMBB_BANK_JOB
			SWITCH iVariation
				CASE 0			RETURN <<229.1970, 213.7629, 104.5252>>			// BJ_PACIFIC_STANDARD
				CASE 1			RETURN <<-1215.3552, -325.6002, 36.6804>>		// BJ_ALTA_STREET
				CASE 2			RETURN <<1175.5978, 2698.3982, 36.9947>>		// BJ_SANDY_SHORES
			ENDSWITCH
		BREAK

		CASE FMMC_TYPE_FMBB_DATA_HACK
			SWITCH iVariation
				CASE 0			RETURN <<-1100.5310, -1498.7791, 3.8100>>		// Group 1
				CASE 1			RETURN <<1405.5000, 3668.5181, 33.0210>>		// Group 2
				CASE 2			RETURN <<-16.2960, 6646.2998, 30.1250>>			// Group 3
			ENDSWITCH
		BREAK
		CASE FMMC_TYPE_FMBB_INFILTRATION
			SWITCH iVariation
				CASE 0			RETURN <<-1065.4856, -242.9739, 53.0100>>		// Lifeinvader
			ENDSWITCH

		BREAK
		CASE FMMC_TYPE_FMBB_TARGET_PURSUIT
			SWITCH iVariation
				CASE 0	RETURN <<1067.5010, -3261.6541, 4.8980>>
				CASE 1	RETURN <<68.5188, -2482.4172, 5.0055>>
				CASE 2	RETURN <<-184.2188, 6296.3267, 30.4886>>
				CASE 3	RETURN <<2421.0129, 4780.2002, 33.5041>>
				CASE 4	RETURN <<1566.5809, 3525.0190, 34.7375>>
				CASE 5	 RETURN <<1033.7214, 2510.7542, 46.1121>>
				CASE 6	RETURN <<2823.5044, 1502.5914, 23.5715>>
				CASE 7	RETURN <<1285.3218, 289.7219, 80.9909>>
				CASE 8	RETURN <<-1024.4041, -538.3408, 34.7084>>
				CASE 9	RETURN <<-769.6266, -2630.0410, 12.8285>>
			ENDSWITCH
		BREAK
		
		
		
		
		BREAK
		
		
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_TRUCK_WORK_LAUNCH_CHECK_MIN_RANGE(INT iFMMC, BOOL bForRivals)
	SWITCH iFMMC
		// CHALLENGES
		CASE FMMC_TYPE_FMBB_SECURITY_VAN			IF bForRivals 	RETURN 50.0		ENDIF	RETURN 100.0
		CASE FMMC_TYPE_FMBB_TARGET_PURSUIT			IF bForRivals 	RETURN 100.0	ENDIF	RETURN 500.0
		
		// WORKS
		CASE FMMC_TYPE_FMBB_JEWEL_STORE_GRAB		IF bForRivals 	RETURN 50.0		ENDIF	RETURN 300.0
		CASE FMMC_TYPE_FMBB_BANK_JOB				IF bForRivals 	RETURN 100.0	ENDIF	RETURN 300.0
		CASE FMMC_TYPE_FMBB_DATA_HACK				IF bForRivals 	RETURN 50.0		ENDIF	RETURN 250.0
		CASE FMMC_TYPE_FMBB_INFILTRATION			IF bForRivals 	RETURN 100.0	ENDIF	RETURN 300.0
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_TRUCK_WORK_LAUNCH_CHECK_MAX_RANGE(INT iFMMC)
	SWITCH iFMMC
		CASE FMMC_TYPE_FMBB_TARGET_PURSUIT			RETURN 4500.0
	ENDSWITCH
	RETURN 99999.0
ENDFUNC

FUNC BOOL IS_TRUCK_WORK_VARIATION_DISABLED(INT iFMMC, INT iVariation)
	UNUSED_PARAMETER(iFMMC)
	UNUSED_PARAMETER(iVariation)
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FM_HackerWorksFlowPlaySubvariations")
		SWITCH iFMMC
			CASE FMMC_TYPE_FMBB_BANK_JOB			
				SWITCH iVariation
					CASE 0			RETURN TRUE
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TRUCK_WORK_VARIATION_TOO_FAR(INT iFMMC, INT iVariation)
	VECTOR vPlayer
	
	SWITCH iFMMC
		CASE FMMC_TYPE_FMBB_TARGET_PURSUIT	
			IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(PLAYER_ID())])
				vPlayer = GET_BLIP_COORDS(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(PLAYER_ID())])
				vPlayer.z = GET_APPROX_FLOOR_FOR_POINT(vPlayer.x, vPlayer.y)
			ELSE
				vPlayer = GET_WORLD_POINT_OF_PLAYER(PLAYER_ID())
			ENDIF
			
			IF VDIST2(GET_TRUCK_WORK_MISSION_COORD(iFMMC, iVariation), vPlayer) > (GET_TRUCK_WORK_LAUNCH_CHECK_MAX_RANGE(iFMMC) * GET_TRUCK_WORK_LAUNCH_CHECK_MAX_RANGE(iFMMC))
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

CONST_INT TRUCK_WORK_SELECT_FAILED_FOR_CLOSE_GANG			-1
CONST_INT TRUCK_WORK_SELECT_FAILED_FOR_CLOSE_PLAYER			-2
FUNC BOOL SELECT_TRUCK_WORK_VARIATION(INT iFMMC, INT& iSelectedVariation)
	INT iVariationList[MAX_NUM_TRUCK_WORK_MISSION_VARIATIONS]
	INT iSuitableVariations[MAX_NUM_TRUCK_WORK_MISSION_VARIATIONS]
	
	INT iPlayer, iSuitableCount, iVariation
	PLAYER_INDEX localGangBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	PLAYER_INDEX playerId
	VECTOR vCoord, vPlayer
	BOOL bPlayerIsRival, bGangMemberClose
	
	#IF IS_DEBUG_BUILD
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_FM_HackerWorksFlowPlaySubvariations")
	#ENDIF
	
		REPEAT NUM_NETWORK_PLAYERS iPlayer
			playerId = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)	
			IF IS_NET_PLAYER_OK(playerId)
				bPlayerIsRival = ((GB_GET_THIS_PLAYER_GANG_BOSS(playerId) != localGangBoss) OR (localGangBoss = INVALID_PLAYER_INDEX())) AND (playerId != PLAYER_ID())
				REPEAT MAX_NUM_TRUCK_WORK_MISSION_VARIATIONS iVariation
					IF iVariationList[iVariation] != (-1)
						vCoord = GET_TRUCK_WORK_MISSION_COORD(iFMMC, iVariation)
						IF NOT IS_VECTOR_ZERO(vCoord)
							IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(playerId)])
								vPlayer = GET_BLIP_COORDS(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(playerId)])
								vPlayer.z = GET_APPROX_FLOOR_FOR_POINT(vPlayer.x, vPlayer.y)
							ELSE
								vPlayer = GET_WORLD_POINT_OF_PLAYER(playerId)
							ENDIF
							
							IF NOT IS_VECTOR_ZERO(vPlayer)
								IF VDIST2(vPlayer, vCoord) < (GET_TRUCK_WORK_LAUNCH_CHECK_MIN_RANGE(iFMMC, bPlayerIsRival) * GET_TRUCK_WORK_LAUNCH_CHECK_MIN_RANGE(iFMMC, bPlayerIsRival))	
									PRINTLN("[MB_WORK] SELECT_TRUCK_WORK_VARIATION - ", GANG_BOSS_GET_NAME_FOR_TYPE_FOR_DEBUG_PRINT(iFMMC), " - Variation #", iVariation, " is not suitable. ", GET_PLAYER_NAME(playerId), " is within ", GET_TRUCK_WORK_LAUNCH_CHECK_MIN_RANGE(iFMMC, bPlayerIsRival), "m.")
									PRINTLN("[MB_WORK] SELECT_TRUCK_WORK_VARIATION - ", GANG_BOSS_GET_NAME_FOR_TYPE_FOR_DEBUG_PRINT(iFMMC), " - Variation #", iVariation, ": ", vCoord, " - ", GET_PLAYER_NAME(playerId), ": ", vPlayer)
									iVariationList[iVariation] = -1
									
									IF NOT bPlayerIsRival
										bGangMemberClose = TRUE
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[MB_WORK] SELECT_TRUCK_WORK_VARIATION - ", GANG_BOSS_GET_NAME_FOR_TYPE_FOR_DEBUG_PRINT(iFMMC), " - ", GET_PLAYER_NAME(playerId), " coords are zero somehow.")
							ENDIF
						ELSE
							iVariationList[iVariation] = -1
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDREPEAT
		
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	REPEAT MAX_NUM_TRUCK_WORK_MISSION_VARIATIONS iVariation
		IF iVariationList[iVariation] != (-1)
		AND NOT IS_VECTOR_ZERO(GET_TRUCK_WORK_MISSION_COORD(iFMMC, iVariation))
			IF NOT IS_TRUCK_WORK_VARIATION_DISABLED(iFMMC, iVariation)
				IF NOT IS_TRUCK_WORK_VARIATION_TOO_FAR(iFMMC, iVariation)
					iSuitableVariations[iSuitableCount] = iVariation
					iSuitableCount++
					PRINTLN("[MB_WORK] SELECT_TRUCK_WORK_VARIATION - ", GANG_BOSS_GET_NAME_FOR_TYPE_FOR_DEBUG_PRINT(iFMMC), " - Variation #", iVariation, " is suitable. Adding to pool.")
				ELSE
					PRINTLN("[MB_WORK] SELECT_TRUCK_WORK_VARIATION - ", GANG_BOSS_GET_NAME_FOR_TYPE_FOR_DEBUG_PRINT(iFMMC), " - Variation #", iVariation, " is too far from local player.")
				ENDIF
			ELSE
				PRINTLN("[MB_WORK] SELECT_TRUCK_WORK_VARIATION - ", GANG_BOSS_GET_NAME_FOR_TYPE_FOR_DEBUG_PRINT(iFMMC), " - Variation #", iVariation, " is disabled.")
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iSuitableCount > 0 
		iSelectedVariation = iSuitableVariations[GET_RANDOM_INT_IN_RANGE(0, iSuitableCount)]
		RETURN TRUE
	ELSE
		IF bGangMemberClose
			iSelectedVariation = TRUCK_WORK_SELECT_FAILED_FOR_CLOSE_GANG
		ELSE
			iSelectedVariation = TRUCK_WORK_SELECT_FAILED_FOR_CLOSE_PLAYER
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ANY_PLAYERS_AROUND_TRUCK_WORK_MISSION_COORDS(INT iFMMC)
	INT iSelectedVariation
	IF SELECT_TRUCK_WORK_VARIATION(iFMMC, iSelectedVariation)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC
FUNC BOOL PLAYER_IS_IN_PROPERTY_BUT_WAITING_FOR_BROADCAST_TO_BE_SET(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_PROPERTY(PlayerID, TRUE)
	OR IS_POINT_IN_A_PROPERTY_GARAGE_INTERIOR(GET_PLAYER_COORDS(PlayerID))
		IF NOT (GlobalplayerBD_FM[NATIVE_TO_INT(PlayerID)].propertyDetails.iCurrentlyInsideProperty > 0)
			NET_PRINT("[spawning] PLAYER_IS_IN_PROPERTY_BUT_WAITING_FOR_BROADCAST_TO_BE_SET - in property, but waiting for iCurrentlyInsideProperty to be set") NET_NL()
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN(TRUE)
ENDFUNC

FUNC BOOL ARE_ANY_RIVAL_PLAYERS_NEAR_PLAYER_STAGGERED(PLAYER_INDEX playerToCheck, FLOAT fRange = 200.0)
	IF IS_NET_PLAYER_OK(g_StaggeredPlayer)
	AND IS_NET_PLAYER_OK(playerToCheck)
		PLAYER_INDEX localGangBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		BOOL bPlayerIsRival = ((GB_GET_THIS_PLAYER_GANG_BOSS(g_StaggeredPlayer) != localGangBoss) OR (localGangBoss = INVALID_PLAYER_INDEX())) AND (g_StaggeredPlayer != PLAYER_ID())
		
		IF bPlayerIsRival
			IF VDIST2(GET_ENTITY_COORDS(GET_PLAYER_PED(g_StaggeredPlayer)), GET_ENTITY_COORDS(GET_PLAYER_PED(playerToCheck))) < (fRange*fRange)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY()
	VECTOR vCarPos 
	FLOAT fCarHeading
	VEHICLE_INDEX tempveh
	VECTOR vPlayerPos
	BOOL bStartOfMissionSpawn = FALSE	
	INT iTooNearDist = 100
	
	g_SpawnData.MissionSpawnPersonalVehicleData.iTimerOKToProceedAfterPVCreation = GET_GAME_TIMER()
	
	IF PLAYER_IS_IN_PROPERTY_BUT_WAITING_FOR_BROADCAST_TO_BE_SET(PLAYER_ID()) // if player spawns into the garage, the bd iCurrentlyInsideProperty deosn't get set until a few frames. (see 1918466) 
		IF NOT IS_CURRENT_PERSONAL_VEHICLE_IMPOUNDED()
			IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
			AND NOT (DOES_ENTITY_EXIST(PERSONAL_VEHICLE_ID()) AND IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(PERSONAL_VEHICLE_ID())))
				tempveh= PERSONAL_VEHICLE_ID()
				
				vPlayerPos = GET_WORLD_POINT_OF_PLAYER(PLAYER_ID())
				vCarPos = GET_ENTITY_COORDS(tempveh, FALSE)
				
				NET_PRINT("[personal_vehicle] WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY - vPlayerPos = ") NET_PRINT_VECTOR(vPlayerPos) NET_NL()
				NET_PRINT("[personal_vehicle] WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY - vCarPos = ") NET_PRINT_VECTOR(vCarPos) NET_NL()
				
				IF NOT IS_VECTOR_ZERO(MPGlobals.VehicleData.vOverrideWarpRequestCoord)
					vPlayerPos = MPGlobals.VehicleData.vOverrideWarpRequestCoord
					NET_PRINT("[personal_vehicle] WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY - overiding warp coords to = ") NET_PRINT_VECTOR(vPlayerPos) NET_NL()
				ENDIF
				
				IF ((GET_CLOUD_TIME_AS_INT() - MPGlobals.VehicleData.iMissionStartTime) < 15)
					PRINTLN("[personal_vehicle]  WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY - setting bStartOfMissionSpawn = TRUE")
					bStartOfMissionSpawn = TRUE					
				ENDIF
				
				IF ((IS_SKYSWOOP_AT_GROUND() = FALSE) AND (bStartOfMissionSpawn = TRUE))
					iTooNearDist = 25
					PRINTLN("[personal_vehicle]  WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY - setting iTooNearDist = ", iTooNearDist)
				ENDIF
			
				
				//IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),tempveh) > 100
				IF (MPGlobals.VehicleData.bWarpNear_IgnoreDistanceCheck)
				OR VDIST(vPlayerPos, vCarPos) > iTooNearDist
				
					NET_PRINT("[personal_vehicle] WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY - distance between player and vehicle is: ") NET_PRINT_FLOAT(VDIST(vPlayerPos, vCarPos)) NET_NL()
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempveh)
						
						// add any custom vehicle nodes 
						IF NOT (MPGlobals.VehicleData.bWarpNear_AddedCustomVehicleNodes)
							PRINTLN("[personal_vehicle] WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY - setting custom vehicle nodes for point") 
							SETUP_ANY_CUSTOM_VEHICLE_NODES_NEAR_POINT(vPlayerPos, GET_ENTITY_MODEL(tempveh))
							MPGlobals.VehicleData.bWarpNear_AddedCustomVehicleNodes = TRUE
						ENDIF
						
						
						// is vehicle already at a custom vehicle node for this location?
						IF IS_POINT_ON_ANY_CUSTOM_VEHICLE_NODES_FOR_POINT(vCarPos)
							NET_PRINT("[personal_vehicle] WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY - car already on a custom vehicle node ") NET_NL()
							CLEAR_CUSTOM_VEHICLE_NODES()
							RETURN TRUE
						ELSE
		
		
							VEHICLE_SPAWN_LOCATION_PARAMS Params
							Params.fMinDistFromCoords = 10.0
							Params.bStartOfMissionPVSpawn=bStartOfMissionSpawn
							Params.bIsForPV = TRUE
							Params.bCheckThisPlayerVehicle = FALSE
							
							IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vPlayerPos,<<0.0, 0.0, 0.0>>,GET_ENTITY_MODEL(tempveh),TRUE,vCarPos,fCarHeading,Params)
								SET_ENTITY_COORDS(tempveh,vCarPos)
								SET_ENTITY_HEADING(tempveh,fCarHeading)
								CLEAR_ROOM_FOR_ENTITY(tempveh)
								
								// dont roll down window if at start of mission. url:bugstar:4907872 - Dispatch I - Passenger and Driver side windows are not appearing on PV's.
								IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), g_SpawnData.MissionSpawnPersonalVehicleData.TimePersonalVehicleCreationForMissionStartup) > 15000)
									ROLL_DOWN_WINDOWS(tempveh)
									PRINTLN("[personal_vehicle] WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY - rolling down windows")
								ENDIF
								
								IF (MPGlobals.VehicleData.bFixupVehicleAfterWarp)
									SET_VEHICLE_FIXED(tempveh)	
								ENDIF
								
								// re-enable wheelies - url:bugstar:3716196 - [PUBLIC][REPORTED] No motorcycles can wheelie when Local Player takes his motorbike into the bunker and starts a Resupply mission.
								IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(tempveh))
								OR IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(tempveh))
									SET_WHEELIE_ENABLED(tempveh, TRUE)
									PRINTLN("[personal_vehicle] WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY - reenable wheelies")
								ENDIF
								
								IF MPGlobals.VehicleData.bOverrideFadeInAfter
									NETWORK_FADE_IN_ENTITY(tempveh, TRUE, TRUE)
								ENDIF
								
								IF IS_VEHICLE_MODEL(tempveh, DELUXO)
									SET_DISABLE_HOVER_MODE_FLIGHT(tempveh, FALSE)
								ENDIF
								
								CLEAR_CUSTOM_VEHICLE_NODES()
								CLEAR_REQUEST_GUEST_PARKING_FROM_PLAYER()
								CLEAR_CONCEAL_CURRENT_PERSONAL_VEHICLE()
								NET_PRINT("[personal_vehicle] WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY  Successfully set vehicle position: ") NET_PRINT_VECTOR(vCarPos) NET_NL()
								RETURN TRUE
							ENDIF
						ENDIF
					ELSE
						NET_PRINT("[personal_vehicle] WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY  Requesting control of saved vehicle to warp nearby") NET_NL()
						NETWORK_REQUEST_CONTROL_OF_ENTITY(tempveh)
					ENDIF
				ELSE
					NET_PRINT("[personal_vehicle] WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY - distance between player and vehicle is too near: ") NET_PRINT_FLOAT(VDIST(vPlayerPos, vCarPos)) NET_NL()
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		ELSE
			NET_PRINT("[personal_vehicle] WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY - vehicle is impounded.") NET_NL()
			RETURN TRUE
		ENDIF
	ELSE
		NET_PRINT("[personal_vehicle] WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY - waiting for player to properly be in property.") NET_NL()
	ENDIF
	RETURN FALSE
ENDFUNC



PROC SET_MY_VEHICLE_AS_NON_SAVEABLE()

	IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
	
		VEHICLE_INDEX VehicleID
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())	
			IF NOT DECOR_EXIST_ON(VehicleID,"Player_Vehicle")
			AND NOT DECOR_EXIST_ON(VehicleID,"Player_Truck")
			AND NOT DECOR_EXIST_ON(VehicleID,"Player_Avenger")
			AND NOT DECOR_EXIST_ON(VehicleID,"Player_Hacker_Truck")
				IF NETWORK_GET_ENTITY_IS_NETWORKED(VehicleID)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)	
						IF NOT DECOR_EXIST_ON(VehicleID, "Not_Allow_As_Saved_Veh")
							DECOR_SET_INT(VehicleID, "Not_Allow_As_Saved_Veh", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
							PRINTLN("SET_MY_VEHICLE_AS_NON_SAVEABLE - set decor Not_Allow_As_Saved_Veh on vehicle. set to ", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
						ELSE
							IF (DECOR_GET_INT(VehicleID, "Not_Allow_As_Saved_Veh") =  NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
								PRINTLN("SET_MY_VEHICLE_AS_NON_SAVEABLE - already set as non stealable by this player, setting to 1 ")
								DECOR_SET_INT(VehicleID, "Not_Allow_As_Saved_Veh", 1)
							ENDIF
						ENDIF
					ELSE
						NET_PRINT("SET_MY_VEHICLE_AS_NON_SAVEABLE - doesn't have network control") NET_NL()
					ENDIF
				ELSE
					NET_PRINT("SET_MY_VEHICLE_AS_NON_SAVEABLE - not networked") NET_NL()	
				ENDIF
			ELSE
				NET_PRINT("SET_MY_VEHICLE_AS_NON_SAVEABLE - its a personal vehicle") NET_NL()
			ENDIF
		ELSE
			NET_PRINT("SET_MY_VEHICLE_AS_NON_SAVEABLE - not in any vehicle") NET_NL()	
		ENDIF
	ENDIF

ENDPROC



FUNC BOOL SET_VEHICLE_AS_CURRENT_PERSONAL_VEHICLE(VEHICLE_INDEX vi_new_veh)


	IF GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) != GET_HASH_OF_LAUNCH_SCRIPT_NAME_SCRIPT()
		SCRIPT_ASSERT("SET_VEHICLE_AS_CURRENT_PERSONAL_VEHICLE - This will not work - the personal vehicle can only be referenced in freemode")
		NET_PRINT("[personal_vehicle] SET_VEHICLE_AS_CURRENT_PERSONAL_VEHICLE - This will not work - the personal vehicle can only be referenced in freemode.") NET_NL()
		RETURN FALSE
	ENDIF
	
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(vi_new_veh)
	AND NOT IS_VEHICLE_MY_PERSONAL_VEHICLE(vi_new_veh)
		SCRIPT_ASSERT("SET_VEHICLE_AS_CURRENT_PERSONAL_VEHICLE - trying to set another players personal vehicle as my own!")		
		NET_PRINT("[personal_vehicle] SET_VEHICLE_AS_CURRENT_PERSONAL_VEHICLE - trying to set another players personal vehicle as my own. Owner Hash = ") NET_PRINT_INT(GET_HASH_OF_PERSONAL_VEHICLE(vi_new_veh)) NET_PRINT(", palyer name of owner = ") NET_PRINT(GET_PLAYER_NAME_FROM_HASH(GET_HASH_OF_PERSONAL_VEHICLE(vi_new_veh))) NET_NL()
		RETURN FALSE
	ENDIF
	
	#IF USE_TU_CHANGES
	IF DOES_ENTITY_EXIST(vi_new_veh)
		IF NOT IS_MODEL_VALID_FOR_PERSONAL_VEHICLE(GET_ENTITY_MODEL(vi_new_veh))
			NET_PRINT("[personal_vehicle] SET_VEHICLE_AS_CURRENT_PERSONAL_VEHICLE - This will not work - model is not valid for personal vehicle.") NET_NL()
			SCRIPT_ASSERT("SET_VEHICLE_AS_CURRENT_PERSONAL_VEHICLE - This will not work - model is not valid for personal vehicle.")
			RETURN FALSE
		ENDIF
	ENDIF
	#ENDIF
	
	IF IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
		PRINTLN("[personal_vehicle] SET_VEHICLE_AS_CURRENT_PERSONAL_VEHICLE - previous pv still cleaning up.")
		RETURN(FALSE)
	ELSE
		IF NETWORK_DOES_NETWORK_ID_EXIST(PERSONAL_VEHICLE_NET_ID())
			IF PERSONAL_VEHICLE_ID() != vi_new_veh
				CLEANUP_MP_SAVED_VEHICLE()
				NET_PRINT("[personal_vehicle] SET_VEHICLE_AS_CURRENT_PERSONAL_VEHICLE - called CLEANUP_MP_SAVED_VEHICLE.") NET_NL()	
				RETURN(FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	INT iInstance
	STRING scriptName 
	IF IS_VEHICLE_DRIVEABLE(vi_new_veh)
		IF NETWORK_GET_ENTITY_IS_NETWORKED(vi_new_veh)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(vi_new_veh)
				IF CAN_REGISTER_MISSION_VEHICLES(1)
					scriptName = GET_ENTITY_SCRIPT(vi_new_veh,iInstance)
					PRINTLN("[personal_vehicle] SET_VEHICLE_AS_CURRENT_PERSONAL_VEHICLE: script name = ",scriptName)
					IF NOT IS_STRING_NULL_OR_EMPTY(scriptName)
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)) <= 0
							IF IS_ENTITY_A_MISSION_ENTITY(vi_new_veh)
								NET_PRINT("[personal_vehicle] CDM_MP - waiting for entity to not be a mission entity as script that created it is cleaning up") NET_NL()
								RETURN FALSE
							ELSE
								PRINTLN("[personal_vehicle] SET_VEHICLE_AS_CURRENT_PERSONAL_VEHICLE: NOT a mission Entity")
							ENDIF
						ELSE
							PRINTLN("[personal_vehicle] SET_VEHICLE_AS_CURRENT_PERSONAL_VEHICLE: num scripts running = ",GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)))
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_A_MISSION_ENTITY(vi_new_veh)
						SET_ENTITY_AS_MISSION_ENTITY(vi_new_veh,FALSE,TRUE)
						NET_PRINT("[personal_vehicle] SET_VEHICLE_AS_CURRENT_PERSONAL_VEHICLE - not a mission entity, setting as one.") NET_NL()
					ENDIF
					
					IF IS_ENTITY_A_MISSION_ENTITY(vi_new_veh)
						IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vi_new_veh)						
							SET_ENTITY_AS_MISSION_ENTITY(vi_new_veh,FALSE,TRUE)
							NET_PRINT("[personal_vehicle] SET_VEHICLE_AS_CURRENT_PERSONAL_VEHICLE - waiting for vehicle to belong to this script.") NET_NL()							
						ENDIF
							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV = VEH_TO_NET(vi_new_veh)
							
							SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV,PLAYER_ID(),TRUE) 
							//SET_NETWORK_ID_PASS_CONTROL_IN_TUTORIAL(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV,TRUE)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vi_new_veh,TRUE)
							// Update flags
							SET_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_CREATED)
							SET_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_JUST_CREATED)
							SET_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_RADIO_STATION_SET)
							CLEAR_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_CLEANUP)
							CLEAR_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_CLEANUP_DELETE)
							CLEAR_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_DESTROYED_HELP)
							CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CLEANUP_RETURN_TO_GARAGE)
							CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CLEANUP_SET_AS_DESTROYED)
							CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CLEANUP_NOT_DURING_RESPAWN)
							CLEAR_BIT(g_MpSavedVehicles[0].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AFTER_SESSION)
							CLEAR_BIT(g_MpSavedVehicles[0].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL)							
							IF CURRENT_SAVED_VEHICLE_SLOT() >= 0 
		                    AND CURRENT_SAVED_VEHICLE_SLOT() < MAX_MP_SAVED_VEHICLES 
		                        CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AFTER_SESSION)  
								CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL)
								SET_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
								PRINTLN("[personal_vehicle] CDM_MP: Setting MP_SAVED_VEHICLE_OUT_GARAGE on vehicle in slot ", CURRENT_SAVED_VEHICLE_SLOT())
								// Add explosives if we paid for them
								IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_REMOTE_BOMB)
									CLEAR_REMOTE_BOMBS_FROM_ALL_SAVED_VEHICLES() // make sure only this vehicle has a remote bomb 2536449
									SET_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_REMOTE_BOMB) // re-apply bit 2536449
									RIG_VEHICLE_WITH_EXPLOSIVES_GENERAL(vi_new_veh,TRUE,0)
									ADD_CONTACT_TO_PHONEBOOK(CHAR_MP_DETONATEPHONE, MULTIPLAYER_BOOK, FALSE)
								ENDIF
								MP_SAVE_VEHICLE_SLOT_STATS_FROM_SAVEGAME(CURRENT_SAVED_VEHICLE_SLOT(),
																	g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()] ,TRUE)
							ENDIF
							SET_VEHICLE_DOORS_LOCKED(vi_new_veh,VEHICLELOCK_UNLOCKED)

							SET_SETTINGS_FOR_SAVED_VEHICLE(vi_new_veh)
							
							SET_PV_DECORATOR(vi_new_veh,CURRENT_SAVED_VEHICLE_SLOT())
							// Fix for bug 1434710 - Destroy impound vehicle when we register new PV
							BOOL bUpdated
							INT iVehSlot
							REPEAT MAX_MP_SAVED_VEHICLES iVehSlot
								IF iVehSlot != CURRENT_SAVED_VEHICLE_SLOT()
									bUpdated = FALSE
									IF IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL)
										PRINTLN("[personal_vehicle] CM_MP - clearing MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL for vehicle in slot ", iVehSlot)
										CLEAR_BIT(g_MpSavedVehicles[iVehSlot].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL)
										bUpdated = TRUE
									ENDIF
									IF IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
										PRINTLN("[personal_vehicle] CM_MP - clearing MP_SAVED_VEHICLE_OUT_GARAGE for vehicle in slot ", iVehSlot)
										CLEAR_BIT(g_MpSavedVehicles[iVehSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
										bUpdated = TRUE
									ENDIF
									IF IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
										NET_PRINT("[personal_vehicle] KR_MP - Destroying impounded vehicle as we have a new PV") NET_NL()
										SET_SAVE_VEHICLE_AS_DESTROYED(iVehSlot)
										bUpdated = TRUE
									ENDIF
									
									IF bUpdated = TRUE
										MP_SAVE_VEHICLE_SLOT_STATS_FROM_SAVEGAME(iVehSlot,g_MpSavedVehicles[iVehSlot] ,TRUE)
									ENDIF
								ENDIF
							ENDREPEAT
							
							NET_PRINT("[personal_vehicle] new personal vehicle registered") NET_NL()

							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].brequestvehiclespawnslot = FALSE
							
							CLEAR_PERSONAL_VEHICLE_SPAWN_OVERRIDE_VECTOR() // sometimes the post mission cleanup doesn't reset this.  2949823
							
							RETURN TRUE
						//ENDIF
					ENDIF
				ENDIF
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(vi_new_veh)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//PURPOSE: Performs checks every couple of seconds to see if the MP Saved Vehicle is near the player, empty, etc.
PROC PROCESS_MP_SAVED_VEHICLE_FLAGS(VEHICLE_INDEX tempveh)
	IF HAS_NET_TIMER_EXPIRED(MPGlobals.VehicleData.iChecksTimer, SVD_CHECKS_DELAY)
		
		//Near Player
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempveh)
		OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), tempveh, <<VD_PERSONAL_RANGE, VD_PERSONAL_RANGE, VD_PERSONAL_RANGE>>)
			IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_VEHICLE_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER SET ") NET_NL()
			ENDIF
		ELSE
			IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_VEHICLE_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED ") NET_NL()
			ENDIF
		ENDIF
		
		IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_ENTITY_IN_IMPOUND_YARD(PLAYER_PED_ID())
					IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						NET_PRINT("   ----->   PROCESS_MP_SAVED_VEHICLE_FLAGS - MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND SET ") NET_NL()
					ENDIF
				ELSE
					IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						NET_PRINT("   ----->   PROCESS_MP_SAVED_VEHICLE_FLAGS -i  MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND CLEARED ") NET_NL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		//Empty
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempveh)
		OR NOT IS_VEHICLE_EMPTY(tempveh)
			IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_VEHICLE_FLAGS - MP_SAVED_VEH_FLAG_NOT_EMPTY SET ") NET_NL()
			ENDIF
		ELSE
			IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_VEHICLE_FLAGS - MP_SAVED_VEH_FLAG_NOT_EMPTY CLEARED ") NET_NL()
			ENDIF
		ENDIF
		
		RESET_NET_TIMER(MPGlobals.VehicleData.iChecksTimer)
	ENDIF
ENDPROC

//PURPOSE: Performs checks every couple of seconds to see if the MP Saved Vehicle is near the player, empty, etc.
PROC PROCESS_MP_SAVED_TRUCK_FLAGS(VEHICLE_INDEX truckVeh, VEHICLE_INDEX trailerVeh)
	IF HAS_NET_TIMER_EXPIRED(MPGlobals.VehicleData.iTruckChecksTimer, SVD_CHECKS_DELAY)
		
		//Near Player
		IF NOT IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID())
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(truckVeh)
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), truckVeh)
				IF NOT IS_SAVED_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					SET_SAVED_TRUCK_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					NET_PRINT("   ----->   PROCESS_MP_SAVED_TRUCK_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER SET, player in truck ") NET_NL()
				ENDIF
			ELIF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(trailerVeh)
			AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), trailerVeh, <<VD_PERSONAL_RANGE, VD_PERSONAL_RANGE, VD_PERSONAL_RANGE>>)
				IF NOT IS_SAVED_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					SET_SAVED_TRUCK_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					NET_PRINT("   ----->   PROCESS_MP_SAVED_TRUCK_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER SET, player near trailer") NET_NL()
				ENDIF
			ELSE
				IF IS_SAVED_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					CLEAR_SAVED_TRUCK_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					NET_PRINT("   ----->   PROCESS_MP_SAVED_TRUCK_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED ") NET_NL()
				ENDIF
			ENDIF
		ELSE
			IF IS_SAVED_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				CLEAR_SAVED_TRUCK_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_TRUCK_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED ") NET_NL()
			ENDIF
		ENDIF
		
		IF NOT IS_SAVED_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_ENTITY_IN_IMPOUND_YARD(PLAYER_PED_ID())
					IF NOT IS_SAVED_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						SET_SAVED_TRUCK_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						NET_PRINT("   ----->   PROCESS_MP_SAVED_TRUCK_FLAGS - MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND SET ") NET_NL()
					ENDIF
				ELSE
					IF IS_SAVED_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						CLEAR_SAVED_TRUCK_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						NET_PRINT("   ----->   PROCESS_MP_SAVED_TRUCK_FLAGS -i  MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND CLEARED ") NET_NL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		//Empty
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), truckVeh)
		OR NOT IS_VEHICLE_EMPTY(truckVeh)
			IF NOT IS_SAVED_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				SET_SAVED_TRUCK_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_TRUCK_FLAGS - MP_SAVED_VEH_FLAG_NOT_EMPTY SET ") NET_NL()
			ENDIF
		ELSE
			IF IS_SAVED_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				CLEAR_SAVED_TRUCK_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_TRUCK_FLAGS - MP_SAVED_VEH_FLAG_NOT_EMPTY CLEARED ") NET_NL()
			ENDIF
		ENDIF
		
		RESET_NET_TIMER(MPGlobals.VehicleData.iTruckChecksTimer)
	ENDIF
ENDPROC

//PURPOSE: Performs checks every couple of seconds to see if the MP Saved Vehicle is near the player, empty, etc.
PROC PROCESS_MP_SAVED_AVENGER_FLAGS(VEHICLE_INDEX avengerVeh)
	IF HAS_NET_TIMER_EXPIRED(MPGlobals.VehicleData.iPlaneChecksTimer, SVD_CHECKS_DELAY)
		
		//Near Player
		IF NOT IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(PLAYER_ID())
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(avengerVeh)
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), avengerVeh)
				IF NOT IS_SAVED_AVENGER_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					SET_SAVED_AVENGER_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					NET_PRINT("   ----->   PROCESS_MP_SAVED_AVENGER_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER SET, player in avenger ") NET_NL()
				ENDIF
			ELSE
				IF IS_SAVED_AVENGER_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					CLEAR_SAVED_AVENGER_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					NET_PRINT("   ----->   PROCESS_MP_SAVED_AVENGER_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED ") NET_NL()
				ENDIF
			ENDIF
		ELSE
			IF IS_SAVED_AVENGER_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				CLEAR_SAVED_AVENGER_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_AVENGER_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED ") NET_NL()
			ENDIF
		ENDIF
		
		IF NOT IS_SAVED_AVENGER_FLAG_SET(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_ENTITY_IN_IMPOUND_YARD(PLAYER_PED_ID())
					IF NOT IS_SAVED_AVENGER_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						SET_SAVED_AVENGER_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						NET_PRINT("   ----->   PROCESS_MP_SAVED_AVENGER_FLAGS - MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND SET ") NET_NL()
					ENDIF
				ELSE
					IF IS_SAVED_AVENGER_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						CLEAR_SAVED_AVENGER_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						NET_PRINT("   ----->   PROCESS_MP_SAVED_AVENGER_FLAGS -i  MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND CLEARED ") NET_NL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		//Empty
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), avengerVeh)
		OR NOT IS_VEHICLE_EMPTY(avengerVeh)
			IF NOT IS_SAVED_AVENGER_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				SET_SAVED_AVENGER_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_AVENGER_FLAGS - MP_SAVED_VEH_FLAG_NOT_EMPTY SET ") NET_NL()
			ENDIF
		ELSE
			IF IS_SAVED_AVENGER_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				CLEAR_SAVED_AVENGER_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_AVENGER_FLAGS - MP_SAVED_VEH_FLAG_NOT_EMPTY CLEARED ") NET_NL()
			ENDIF
		ENDIF
		
		RESET_NET_TIMER(MPGlobals.VehicleData.iPlaneChecksTimer)
	ENDIF
ENDPROC

//PURPOSE: Performs checks every couple of seconds to see if the MP Saved Vehicle is near the player, empty, etc.
PROC PROCESS_MP_SAVED_HACKER_TRUCK_FLAGS(VEHICLE_INDEX HackertruckVeh, VEHICLE_INDEX trailerVeh)
	IF HAS_NET_TIMER_EXPIRED(MPGlobals.VehicleData.iHackerTruckChecksTimer, SVD_CHECKS_DELAY)
		
		//Near Player
		IF NOT IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(PLAYER_ID())
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(HackertruckVeh)
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HackertruckVeh)
				IF NOT IS_SAVED_HACKER_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					SET_SAVED_HACKER_TRUCK_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					NET_PRINT("   ----->   PROCESS_MP_SAVED_HACKER_TRUCK_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER SET, player in Truck_1 ") NET_NL()
				ENDIF
			ELIF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(trailerVeh)
			AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), trailerVeh, <<VD_PERSONAL_RANGE, VD_PERSONAL_RANGE, VD_PERSONAL_RANGE>>)
				IF NOT IS_SAVED_HACKER_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					SET_SAVED_HACKER_TRUCK_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					NET_PRINT("   ----->   PROCESS_MP_SAVED_HACKER_TRUCK_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER SET, player near trailer") NET_NL()
				ENDIF
			ELSE
				IF IS_SAVED_HACKER_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					CLEAR_SAVED_HACKER_TRUCK_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					NET_PRINT("   ----->   PROCESS_MP_SAVED_HACKER_TRUCK_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED ") NET_NL()
				ENDIF
			ENDIF
		ELSE
			IF IS_SAVED_HACKER_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				CLEAR_SAVED_HACKER_TRUCK_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_HACKER_TRUCK_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED ") NET_NL()
			ENDIF
		ENDIF
		
		IF NOT IS_SAVED_HACKER_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_ENTITY_IN_IMPOUND_YARD(PLAYER_PED_ID())
					IF NOT IS_SAVED_HACKER_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						SET_SAVED_HACKER_TRUCK_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						NET_PRINT("   ----->   PROCESS_MP_SAVED_HACKER_TRUCK_FLAGS - MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND SET ") NET_NL()
					ENDIF
				ELSE
					IF IS_SAVED_HACKER_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						CLEAR_SAVED_HACKER_TRUCK_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						NET_PRINT("   ----->   PROCESS_MP_SAVED_HACKER_TRUCK_FLAGS -i  MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND CLEARED ") NET_NL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		//Empty
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HackertruckVeh)
		OR NOT IS_VEHICLE_EMPTY(HackertruckVeh)
			IF NOT IS_SAVED_HACKER_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				SET_SAVED_HACKER_TRUCK_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_HACKER_TRUCK_FLAGS - MP_SAVED_VEH_FLAG_NOT_EMPTY SET ") NET_NL()
			ENDIF
		ELSE
			IF IS_SAVED_HACKER_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				CLEAR_SAVED_HACKER_TRUCK_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_HACKER_TRUCK_FLAGS - MP_SAVED_VEH_FLAG_NOT_EMPTY CLEARED ") NET_NL()
			ENDIF
		ENDIF
		
		RESET_NET_TIMER(MPGlobals.VehicleData.iHackerTruckChecksTimer)
	ENDIF
ENDPROC

#IF FEATURE_DLC_2_2022
//PURPOSE: Performs checks every couple of seconds to see if the MP Saved Vehicle is near the player, empty, etc.
PROC PROCESS_MP_SAVED_ACID_LAB_FLAGS(VEHICLE_INDEX AcidLabVeh)
	IF HAS_NET_TIMER_EXPIRED(MPGlobals.VehicleData.iAcidLabChecksTimer, SVD_CHECKS_DELAY)
		
		//Near Player
		IF NOT IS_OWNERS_ACID_LAB_INSIDE_JUGGALO_HIDEOUT(PLAYER_ID())
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(AcidLabVeh)
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), AcidLabVeh)
				IF NOT IS_SAVED_ACID_LAB_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					SET_SAVED_ACID_LAB_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					NET_PRINT("   ----->   PROCESS_MP_SAVED_ACID_LAB_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER SET, player in Truck_1 ") NET_NL()
				ENDIF
//			ELIF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//			AND NOT IS_ENTITY_DEAD(trailerVeh)
//			AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), trailerVeh, <<VD_PERSONAL_RANGE, VD_PERSONAL_RANGE, VD_PERSONAL_RANGE>>)
//				IF NOT IS_SAVED_ACID_LAB_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
//					SET_SAVED_ACID_LAB_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
//					NET_PRINT("   ----->   PROCESS_MP_SAVED_ACID_LAB_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER SET, player near trailer") NET_NL()
//				ENDIF
			ELSE
				IF IS_SAVED_ACID_LAB_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					CLEAR_SAVED_ACID_LAB_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
					NET_PRINT("   ----->   PROCESS_MP_SAVED_ACID_LAB_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED ") NET_NL()
				ENDIF
			ENDIF
		ELSE
			IF IS_SAVED_ACID_LAB_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				CLEAR_SAVED_ACID_LAB_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_ACID_LAB_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED ") NET_NL()
			ENDIF
		ENDIF
		
		IF NOT IS_SAVED_ACID_LAB_FLAG_SET(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_ENTITY_IN_IMPOUND_YARD(PLAYER_PED_ID())
					IF NOT IS_SAVED_ACID_LAB_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						SET_SAVED_ACID_LAB_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						NET_PRINT("   ----->   PROCESS_MP_SAVED_ACID_LAB_FLAGS - MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND SET ") NET_NL()
					ENDIF
				ELSE
					IF IS_SAVED_ACID_LAB_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						CLEAR_SAVED_ACID_LAB_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						NET_PRINT("   ----->   PROCESS_MP_SAVED_ACID_LAB_FLAGS -i  MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND CLEARED ") NET_NL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		//Empty
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), AcidLabVeh)
		OR NOT IS_VEHICLE_EMPTY(AcidLabVeh)
			IF NOT IS_SAVED_ACID_LAB_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				SET_SAVED_ACID_LAB_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_ACID_LAB_FLAGS - MP_SAVED_VEH_FLAG_NOT_EMPTY SET ") NET_NL()
			ENDIF
		ELSE
			IF IS_SAVED_ACID_LAB_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				CLEAR_SAVED_ACID_LAB_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_ACID_LAB_FLAGS - MP_SAVED_VEH_FLAG_NOT_EMPTY CLEARED ") NET_NL()
			ENDIF
		ENDIF
		
		RESET_NET_TIMER(MPGlobals.VehicleData.iAcidLabChecksTimer)
	ENDIF
ENDPROC
#ENDIF

#IF FEATURE_HEIST_ISLAND
//PURPOSE: Performs checks every couple of seconds to see if the MP Saved Vehicle is near the player, empty, etc.
PROC PROCESS_MP_SAVED_SUBMARINE_FLAGS(VEHICLE_INDEX SubmarineVeh)
	IF HAS_NET_TIMER_EXPIRED(MPGlobals.VehicleData.iSubmarineChecksTimer, SVD_CHECKS_DELAY)
		
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(SubmarineVeh)
		AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SubmarineVeh)
			IF NOT IS_SAVED_SUBMARINE_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				SET_SAVED_SUBMARINE_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_SUBMARINE_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER SET, player in Truck_1 ") NET_NL()
			ENDIF
		ELSE
			IF IS_SAVED_SUBMARINE_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				CLEAR_SAVED_SUBMARINE_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_SUBMARINE_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED ") NET_NL()
			ENDIF
		ENDIF

		IF NOT IS_SAVED_SUBMARINE_FLAG_SET(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_ENTITY_IN_IMPOUND_YARD(PLAYER_PED_ID())
					IF NOT IS_SAVED_SUBMARINE_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						SET_SAVED_SUBMARINE_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						NET_PRINT("   ----->   PROCESS_MP_SAVED_SUBMARINE_FLAGS - MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND SET ") NET_NL()
					ENDIF
				ELSE
					IF IS_SAVED_SUBMARINE_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						CLEAR_SAVED_SUBMARINE_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						NET_PRINT("   ----->   PROCESS_MP_SAVED_SUBMARINE_FLAGS -i  MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND CLEARED ") NET_NL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		//Empty
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SubmarineVeh)
		OR NOT IS_VEHICLE_EMPTY(SubmarineVeh)
			IF NOT IS_SAVED_SUBMARINE_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				SET_SAVED_SUBMARINE_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_SUBMARINE_FLAGS - MP_SAVED_VEH_FLAG_NOT_EMPTY SET ") NET_NL()
			ENDIF
		ELSE
			IF IS_SAVED_SUBMARINE_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				CLEAR_SAVED_SUBMARINE_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_SUBMARINE_FLAGS - MP_SAVED_VEH_FLAG_NOT_EMPTY CLEARED ") NET_NL()
			ENDIF
		ENDIF
		
		RESET_NET_TIMER(MPGlobals.VehicleData.iSubmarineChecksTimer)
	ENDIF
ENDPROC
#ENDIF

#IF FEATURE_HEIST_ISLAND
//PURPOSE: Performs checks every couple of seconds to see if the MP Saved Vehicle is near the player, empty, etc.
PROC PROCESS_MP_SAVED_SUBMARINE_DINGHY_FLAGS(VEHICLE_INDEX DinghyVeh)
	IF HAS_NET_TIMER_EXPIRED(MPGlobals.VehicleData.iSubmarineDinghyChecksTimer, SVD_CHECKS_DELAY)
		
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(DinghyVeh)
		AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DinghyVeh)
			IF NOT IS_SAVED_SUBMARINE_DINGHY_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				SET_SAVED_SUBMARINE_DINGHY_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_SUBMARINE_DINGHY_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER SET, player in Truck_1 ") NET_NL()
			ENDIF
		ELSE
			IF IS_SAVED_SUBMARINE_DINGHY_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				CLEAR_SAVED_SUBMARINE_DINGHY_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_SUBMARINE_DINGHY_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED ") NET_NL()
			ENDIF
		ENDIF

		IF NOT IS_SAVED_SUBMARINE_DINGHY_FLAG_SET(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_ENTITY_IN_IMPOUND_YARD(PLAYER_PED_ID())
					IF NOT IS_SAVED_SUBMARINE_DINGHY_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						SET_SAVED_SUBMARINE_DINGHY_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						NET_PRINT("   ----->   PROCESS_MP_SAVED_SUBMARINE_DINGHY_FLAGS - MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND SET ") NET_NL()
					ENDIF
				ELSE
					IF IS_SAVED_SUBMARINE_DINGHY_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						CLEAR_SAVED_SUBMARINE_DINGHY_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						NET_PRINT("   ----->   PROCESS_MP_SAVED_SUBMARINE_DINGHY_FLAGS -i  MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND CLEARED ") NET_NL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		//Empty
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DinghyVeh)
		OR NOT IS_VEHICLE_EMPTY(DinghyVeh)
			IF NOT IS_SAVED_SUBMARINE_DINGHY_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				SET_SAVED_SUBMARINE_DINGHY_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_SUBMARINE_DINGHY_FLAGS - MP_SAVED_VEH_FLAG_NOT_EMPTY SET ") NET_NL()
			ENDIF
		ELSE
			IF IS_SAVED_SUBMARINE_DINGHY_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				CLEAR_SAVED_SUBMARINE_DINGHY_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_SUBMARINE_DINGHY_FLAGS - MP_SAVED_VEH_FLAG_NOT_EMPTY CLEARED ") NET_NL()
			ENDIF
		ENDIF
		
		RESET_NET_TIMER(MPGlobals.VehicleData.iSubmarineDinghyChecksTimer)
	ENDIF
ENDPROC
#ENDIF

#IF FEATURE_DLC_2_2022
//PURPOSE: Performs checks every couple of seconds to see if the MP Saved Vehicle is near the player, empty, etc.
PROC PROCESS_MP_SAVED_SUPPORT_BIKE_FLAGS(VEHICLE_INDEX SupportBikeVeh)
	IF HAS_NET_TIMER_EXPIRED(MPGlobals.VehicleData.iSupportBikeChecksTimer, SVD_CHECKS_DELAY)
		
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(SupportBikeVeh)
		AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SupportBikeVeh)
			IF NOT IS_SAVED_SUPPORT_BIKE_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				SET_SAVED_SUPPORT_BIKE_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_SUPPORT_BIKE_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER SET, player in Support bike ") NET_NL()
			ENDIF
		ELSE
			IF IS_SAVED_SUPPORT_BIKE_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				CLEAR_SAVED_SUPPORT_BIKE_FLAG(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_SUPPORT_BIKE_FLAGS - MP_SAVED_VEH_FLAG_NEAR_PLAYER CLEARED ") NET_NL()
			ENDIF
		ENDIF

		IF NOT IS_SAVED_SUPPORT_BIKE_FLAG_SET(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_ENTITY_IN_IMPOUND_YARD(PLAYER_PED_ID())
					IF NOT IS_SAVED_SUPPORT_BIKE_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						SET_SAVED_SUPPORT_BIKE_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						NET_PRINT("   ----->   PROCESS_MP_SAVED_SUPPORT_BIKE_FLAGS - MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND SET ") NET_NL()
					ENDIF
				ELSE
					IF IS_SAVED_SUPPORT_BIKE_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						CLEAR_SAVED_SUPPORT_BIKE_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						NET_PRINT("   ----->   PROCESS_MP_SAVED_SUPPORT_BIKE_FLAGS -i  MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND CLEARED ") NET_NL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		//Empty
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SupportBikeVeh)
		OR NOT IS_VEHICLE_EMPTY(SupportBikeVeh)
			IF NOT IS_SAVED_SUPPORT_BIKE_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				SET_SAVED_SUPPORT_BIKE_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_SUPPORT_BIKE_FLAGS - MP_SAVED_VEH_FLAG_NOT_EMPTY SET ") NET_NL()
			ENDIF
		ELSE
			IF IS_SAVED_SUPPORT_BIKE_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				CLEAR_SAVED_SUPPORT_BIKE_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   PROCESS_MP_SAVED_SUPPORT_BIKE_FLAGS - MP_SAVED_VEH_FLAG_NOT_EMPTY CLEARED ") NET_NL()
			ENDIF
		ENDIF
		
		RESET_NET_TIMER(MPGlobals.VehicleData.iSupportBikeChecksTimer)
	ENDIF
ENDPROC
#ENDIF

PROC PROCESS_MP_SAVED_VEHICLE_MODS(VEHICLE_INDEX tempveh)
	IF DOES_ENTITY_EXIST(tempveh)
	AND IS_VEHICLE_DRIVEABLE(tempveh)
	AND GET_NUM_MOD_KITS(tempveh) != 0 	// url:bugstar:2641388
		// XMAS HORNS
			IF NOT g_sMPTunables.bTOGGLE_XMAS2015_HORNS
			#IF IS_DEBUG_BUILD
			AND NOT g_bDebugUnlockChristmasRewardItems
			#ENDIF
			
			AND g_sMPTunables.iFillStage = 0
			
				// Do we have xmas horn?
				SWITCH GET_VEHICLE_MOD_IDENTIFIER_HASH(tempveh, MOD_HORN, GET_VEHICLE_MOD(tempveh, MOD_HORN))
					CASE HASH("XM15_HORN_01")		
					CASE HASH("XM15_HORN_02")		
					CASE HASH("XM15_HORN_03")		
					CASE HASH("XM15_HORN_01_PREVIEW")
					CASE HASH("XM15_HORN_02_PREVIEW")
					CASE HASH("XM15_HORN_03_PREVIEW")
						PRINTLN("PROCESS_MP_SAVED_VEHICLE_MODS - removing xmas horn with hash ", GET_VEHICLE_MOD_IDENTIFIER_HASH(tempveh, MOD_HORN, GET_VEHICLE_MOD(tempveh, MOD_HORN)))
						PRINTLN("...g_sTunableLoadingStruct.bRefreshNow = ", g_sTunableLoadingStruct.bRefreshNow)
						PRINTLN("...g_sMPTunables.iFillStage = ", g_sMPTunables.iFillStage)
						
						SCRIPT_ASSERT("PROCESS_MP_SAVED_VEHICLE_MODS - XMAS2015 content not available, removing horn")
						REMOVE_VEHICLE_MOD(tempveh, MOD_HORN)
					BREAK
				ENDSWITCH
			ENDIF
	ENDIF
ENDPROC






FUNC BOOL MP_SAVE_VEHICLE_PUT_VEH_INTO_GARAGE(INT& iReasonTrue,INT iSlot)
	MPGlobals.VehicleData.iWarpBackSlot = iSlot
	MPGlobals.VehicleData.bWarpBackIntoGarage = TRUE
	IF MPGlobals.VehicleData.iWarpBackIntoGarageResult != 0
		iReasonTrue = MPGlobals.VehicleData.iWarpBackIntoGarageResult
		MPGlobals.VehicleData.iWarpBackIntoGarageResult = 0
		MPGlobals.VehicleData.bWarpBackIntoGarage = FALSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_MP_SAVE_VEHICLE_PUT_VEH_INTO_GARAGE()
	IF MPGlobals.VehicleData.bWarpBackIntoGarage
	AND MPGlobals.VehicleData.iWarpBackIntoGarageResult =0
		
		VEHICLE_INDEX tempveh
		IF MPGlobals.VehicleData.iWarpBackSlot < 0
			MPGlobals.VehicleData.iWarpBackIntoGarageResult = PUT_VEH_INTO_GARAGE_DOES_NOT_EXIST
			PRINTLN("MAINTAIN_MP_SAVE_VEHICLE_PUT_VEH_INTO_GARAGE: personal vehicle is does not exist.. SLOT < 0 !!")
		ELSE
			IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
				tempveh= PERSONAL_VEHICLE_ID()
				IF IS_VEHICLE_EMPTY(tempveh,TRUE)
					IF IS_BIT_SET(g_MpSavedVehicles[MPGlobals.VehicleData.iWarpBackSlot].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
						NET_PRINT("MAINTAIN_MP_SAVE_VEHICLE_PUT_VEH_INTO_GARAGE: Requesting control of personal vehicle") NET_NL()
						MPGlobals.VehicleData.iWarpBackIntoGarageResult = PUT_VEH_INTO_GARAGE_CURRENTLY_IMPOUNDED
					ELSE
//						IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempveh)
//							NET_PRINT("MAINTAIN_MP_SAVE_VEHICLE_PUT_VEH_INTO_GARAGE: Requesting control of personal vehicle") NET_NL()
//							NETWORK_REQUEST_CONTROL_OF_ENTITY(tempveh)
//						ELSE
							NET_PRINT("MAINTAIN_MP_SAVE_VEHICLE_PUT_VEH_INTO_GARAGE: deleting setting garage flag") NET_NL()
							CLEAR_BIT(g_MpSavedVehicles[MPGlobals.VehicleData.iWarpBackSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
							PRINTLN("[personal_vehicle] MP_SAVED_VEHICLE_OUT_GARAGE - 4cleared on #",MPGlobals.VehicleData.iWarpBackSlot)
							//DELETE_NET_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV)
							CLEANUP_MP_SAVED_VEHICLE()
							MPGlobals.VehicleData.iWarpBackIntoGarageResult = PUT_VEH_INTO_GARAGE_SUCCESS
//						ENDIF
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_MP_SAVE_VEHICLE_PUT_VEH_INTO_GARAGE: personal vehicle is not empty! Not deleting")
					MPGlobals.VehicleData.iWarpBackIntoGarageResult = PUT_VEH_INTO_GARAGE_VEHICLE_NOT_EMPTY
				ENDIF
			ELSE
				IF NETWORK_DOES_NETWORK_ID_EXIST(PERSONAL_VEHICLE_NET_ID())
					tempveh= PERSONAL_VEHICLE_ID()
					IF IS_VEHICLE_EMPTY(tempveh,TRUE)
//						IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempveh)
//							NET_PRINT("MAINTAIN_MP_SAVE_VEHICLE_PUT_VEH_INTO_GARAGE: Requesting control of non driveable personal vehicle") NET_NL()
//							NETWORK_REQUEST_CONTROL_OF_ENTITY(tempveh)
//						ELSE
							NET_PRINT("MAINTAIN_MP_SAVE_VEHICLE_PUT_VEH_INTO_GARAGE: deleting non driveable personal vehicle") NET_NL()
							//DELETE_NET_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV)
							CLEANUP_MP_SAVED_VEHICLE()
							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].brequestvehiclespawnslot = FALSE
							MPGlobals.VehicleData.iWarpBackIntoGarageResult = PUT_VEH_INTO_GARAGE_VEHICLE_NOT_DV_DELETE
//						ENDIF
					ELSE
						PRINTLN("MAINTAIN_MP_SAVE_VEHICLE_PUT_VEH_INTO_GARAGE: personal vehicle is not empty! Not deleting")
						MPGlobals.VehicleData.iWarpBackIntoGarageResult = PUT_VEH_INTO_GARAGE_VEHICLE_NOT_DV_NOT_EMPTY
					ENDIF
				ELSE
					MPGlobals.VehicleData.iWarpBackIntoGarageResult = PUT_VEH_INTO_GARAGE_DOES_NOT_EXIST
					PRINTLN("MAINTAIN_MP_SAVE_VEHICLE_PUT_VEH_INTO_GARAGE: personal vehicle is does not exist.. complete")
				ENDIF
			ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC INSURE_MP_SAVED_VEHICLE_PRIVATE(INT iSlot)
	IF iSlot >= 0 AND iSlot <= MAX_MP_SAVED_VEHICLES
		SET_BIT(g_MpSavedVehicles[iSlot].iVehicleBS,MP_SAVED_VEHICLE_INSURED)
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_BUY_INSURANCE, TRUE)
		PRINTLN("[personal_vehicle] INSURE_MP_SAVED_VEHICLE- Setting insurance in save game #",iSlot," called from ", GET_THIS_SCRIPT_NAME())
		MP_SAVE_VEHICLE_SLOT_STATS_FROM_SAVEGAME(iSlot,g_MpSavedVehicles[iSlot] ,TRUE)
		IF NOT addedMPInsuranceContact
			ADD_CONTACT_TO_PHONEBOOK(CHAR_MP_MORS_MUTUAL, MULTIPLAYER_BOOK,TRUE) 	
		//	PRINT_HELP("PLYVEH_INS_IN")
			addedMPInsuranceContact = TRUE
		ELSE
			ADD_CONTACT_TO_PHONEBOOK(CHAR_MP_MORS_MUTUAL, MULTIPLAYER_BOOK,FALSE) 
		ENDIF
	ENDIF
ENDPROC

PROC INSURE_MP_SAVE_VEHICLE_AND_SET_FLAG(VEHICLE_INDEX vehID,INT iSlot)
	ADD_INSURANCE_FLAG_TO_VEHICLE(vehID)
	INSURE_MP_SAVED_VEHICLE_PRIVATE(iSlot)
ENDPROC

FUNC BOOL IS_PLAYER_IN_PERSONAL_VEHICLE(PLAYER_INDEX playerID, BOOL ConsiderEnteringAsInVehicle = FALSE)

	IF IS_NET_PLAYER_OK(playerID)
		PED_INDEX pedID = GET_PLAYER_PED(playerID)
		IF IS_PED_IN_ANY_VEHICLE(pedID, ConsiderEnteringAsInVehicle)
			IF IS_VEHICLE_A_PERSONAL_VEHICLE(GET_VEHICLE_PED_IS_IN(pedID, ConsiderEnteringAsInVehicle))
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PLAYER_IN_PERSONAL_TRUCK(PLAYER_INDEX playerID, BOOL ConsiderEnteringAsInVehicle = FALSE)

	IF IS_NET_PLAYER_OK(playerID)
		PED_INDEX pedID = GET_PLAYER_PED(playerID)
		IF IS_PED_IN_ANY_VEHICLE(pedID, ConsiderEnteringAsInVehicle)
			IF IS_VEHICLE_A_PERSONAL_TRUCK(GET_VEHICLE_PED_IS_IN(pedID, ConsiderEnteringAsInVehicle))
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PLAYER_IN_PERSONAL_HACKER_TRUCK(PLAYER_INDEX playerID, BOOL ConsiderEnteringAsInVehicle = FALSE)

	IF IS_NET_PLAYER_OK(playerID)
		PED_INDEX pedID = GET_PLAYER_PED(playerID)
		IF IS_PED_IN_ANY_VEHICLE(pedID, ConsiderEnteringAsInVehicle)
			IF IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(GET_VEHICLE_PED_IS_IN(pedID, ConsiderEnteringAsInVehicle))
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_PERSONAL_VEHICLE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_MY_PERSONAL_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC
PROC MAINTAIN_MP_SAVED_VEHICLE_INSURANCE_CALL()
	IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_DO_DESTROYED_PHONE_CALL_IMPOUND)
		IF NOT SAFE_TO_PRINT_PV_HELP()
			EXIT
		ENDIF
		
		IF NOT IS_LOCAL_PLAYER_IN_PERSONAL_VEHICLE()
			PRINT_HELP("CUST_GAR_VEH_L1")
			PRINTLN("MAINTAIN_MP_SAVED_VEHICLE_INSURANCE_CALL - printing help CUST_GAR_VEH_L1")
		ENDIF
			
		CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS, MP_SAVED_VEHICLE_MUST_PAY_INSURANCE)
		
		g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iPremiumPaidByDestroyer = 0
		
		CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_DO_DESTROYED_PHONE_CALL_IMPOUND_STARTED)
		CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_DO_DESTROYED_PHONE_CALL_IMPOUND)
	ELIF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_DO_DESTROYED_PHONE_CALL_GEN)
		IF NOT SAFE_TO_PRINT_PV_HELP()
			EXIT
		ENDIF
		
		IF NOT IS_LOCAL_PLAYER_IN_PERSONAL_VEHICLE()
		#IF FEATURE_GEN9_EXCLUSIVE
		AND NOT SHOULD_FORCE_RESPAWN_PV()
		#ENDIF
			PRINT_HELP("CUST_GAR_VEH_L3")
			PRINTLN("MAINTAIN_MP_SAVED_VEHICLE_INSURANCE_CALL - printing help CUST_GAR_VEH_L3")
		ENDIF
		
		CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_DO_DESTROYED_PHONE_CALL_GEN)
		CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_DO_DESTROYED_PHONE_CALL_GEN_STARTED)
	ELIF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_DO_DESTROYED_INSURANCE_REMINDER)
		IF NOT SAFE_TO_PRINT_PV_HELP()
			EXIT
		ENDIF
		
		IF NOT IS_LOCAL_PLAYER_IN_PERSONAL_VEHICLE()
			PRINT_HELP("CUST_GAR_VEH_L2")
			PRINTLN("MAINTAIN_MP_SAVED_VEHICLE_INSURANCE_CALL - printing help CUST_GAR_VEH_L2")
		ENDIF
		
		CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_DO_DESTROYED_INSURANCE_REMINDER)
	ELIF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_SVM_DESTROYED)
		IF NOT SAFE_TO_PRINT_PV_HELP()
			EXIT
		ENDIF
		
		// wait until it has been returned to the garage
		IF NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
			IF IS_THIS_MODEL_A_SPECIAL_VEHICLE(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel)
			OR IS_MODEL_A_PERSONAL_TRAILER(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel)
			OR IS_THIS_MODEL_ALLOWED_IN_HANGAR(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel)
				IF IS_MODEL_A_PERSONAL_TRAILER(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel)
					IF DOES_PLAYER_OWN_A_BUNKER(PLAYER_ID())
						PRINT_HELP_WITH_STRING("SVM_DEST_B", GET_SPECIAL_VEHICLE_MODEL_TEXT_LABEL(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel))
					ELSE
						PRINT_HELP_WITH_STRING("SVM_DEST_IM", GET_SPECIAL_VEHICLE_MODEL_TEXT_LABEL(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel))
					ENDIF
				ELIF IS_THIS_MODEL_ALLOWED_IN_HANGAR(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel)
					PRINT_HELP("HANGAR_DEST")
				ELIF IS_THIS_MODEL_A_HUB_DELIVERY_VEHICLE(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel)
					PRINT_HELP_WITH_STRING("HUB_DEST", GET_SPECIAL_VEHICLE_MODEL_TEXT_LABEL(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel))
				ELSE
					IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_IE_WAREHOUSE) > 0
						PRINT_HELP_WITH_STRING("SVM_DEST", GET_SPECIAL_VEHICLE_MODEL_TEXT_LABEL(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel))
					ELSE
						PRINT_HELP_WITH_STRING("SVM_DEST_IM", GET_SPECIAL_VEHICLE_MODEL_TEXT_LABEL(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel))
					ENDIF
				ENDIF
			ELSE
				IF (g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT)
					PRINTLN("MAINTAIN_MP_SAVED_VEHICLE_INSURANCE_CALL - MP_SAVED_VEH_FLAG_SVM_DESTROYED model is dummy")
				ELSE
					PRINTLN("MAINTAIN_MP_SAVED_VEHICLE_INSURANCE_CALL - MP_SAVED_VEH_FLAG_SVM_DESTROYED model is ", GET_MODEL_NAME_FOR_DEBUG(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel))
				ENDIF
			ENDIF
			
			CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_SVM_DESTROYED)
		ENDIF
	ELIF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_SVM_IMPOUNDED)
		IF NOT SAFE_TO_PRINT_PV_HELP()
			EXIT
		ENDIF
		
		// wait until it has been returned to the garage
		IF NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
			IF IS_THIS_MODEL_A_SPECIAL_VEHICLE(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel)
			OR IS_MODEL_A_PERSONAL_TRAILER(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel)
			OR IS_THIS_MODEL_ALLOWED_IN_HANGAR(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel)
				IF IS_MODEL_A_PERSONAL_TRAILER(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel)
					IF DOES_PLAYER_OWN_A_BUNKER(PLAYER_ID())
						PRINT_HELP_WITH_STRING("SVM_IMP_B", GET_SPECIAL_VEHICLE_MODEL_TEXT_LABEL(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel))
					ELSE
						PRINT_HELP_WITH_STRING("SVM_IMP_IM", GET_SPECIAL_VEHICLE_MODEL_TEXT_LABEL(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel))
					ENDIF
				ELIF IS_THIS_MODEL_ALLOWED_IN_HANGAR(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel)
					PRINT_HELP("HANGAR_IMP")
				ELIF IS_THIS_MODEL_A_HUB_DELIVERY_VEHICLE(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel)
					PRINT_HELP_WITH_STRING("HUB_IMP", GET_SPECIAL_VEHICLE_MODEL_TEXT_LABEL(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel))
				ELSE
					IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_IE_WAREHOUSE) > 0
						PRINT_HELP_WITH_STRING("SVM_IMP", GET_SPECIAL_VEHICLE_MODEL_TEXT_LABEL(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel))
					ELSE
						PRINT_HELP_WITH_STRING("SVM_IMP_IM", GET_SPECIAL_VEHICLE_MODEL_TEXT_LABEL(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel))
					ENDIF
				ENDIF
			ELSE
				IF (g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT)
					PRINTLN("MAINTAIN_MP_SAVED_VEHICLE_INSURANCE_CALL - MP_SAVED_VEH_FLAG_SVM_IMPOUNDED model is dummy")
				ELSE
					PRINTLN("MAINTAIN_MP_SAVED_VEHICLE_INSURANCE_CALL - MP_SAVED_VEH_FLAG_SVM_IMPOUNDED model is ", GET_MODEL_NAME_FOR_DEBUG(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel))
				ENDIF
			ENDIF
			
			CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_SVM_IMPOUNDED)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PV_BLIP_BE_HIDDEN_FOR_PROPERTY()
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty > 0
		IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
			IF GET_PROPERTY_SIZE_TYPE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty ) != PROP_SIZE_TYPE_LARGE_APT
				RETURN TRUE
			ELSE
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_A_GARAGE)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_CONCEAL_IN_PROPERTY)
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID())
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

#IF FEATURE_CASINO
FUNC BOOL SHOULD_PV_BLIP_BE_HIDDEN_FOR_CASINO()
	
	
	IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_VEH_IN) // if car is in casino car park
		IF IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR_OF_TYPE(SIMPLE_INTERIOR_TYPE_CASINO_VALET_GARAGE)
			IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())	
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR_OF_TYPE(SIMPLE_INTERIOR_TYPE_CASINO_VALET_GARAGE)
			IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())	
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
		IF IS_PLAYER_IN_CASINO(PLAYER_ID())
		OR IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
			RETURN TRUE
		ENDIF
	ENDIF	
	
	RETURN FALSE

ENDFUNC
#ENDIF

#IF FEATURE_TUNER
FUNC BOOL SHOULD_PV_BLIP_BE_HIDDEN_FOR_CAR_MEET()
	IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_VEH_IN)
		IF IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR_OF_TYPE(SIMPLE_INTERIOR_TYPE_CAR_MEET)
		AND NOT IS_LOCAL_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX()
		AND NOT IS_PLAYER_TEST_DRIVING_A_VEHICLE(PLAYER_ID())
		AND NOT IS_PLAYER_TEST_DRIVING_A_VEHICLE_AS_PASSENGER(PLAYER_ID())
			IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ELIF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_VEH_IN)
		IF IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR_OF_TYPE(SIMPLE_INTERIOR_TYPE_CAR_MEET)
		AND IS_LOCAL_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX()
		AND NATIVE_TO_INT(GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())) = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInPlayerOwnedLocation
		AND NOT IS_PLAYER_TEST_DRIVING_A_VEHICLE(PLAYER_ID())
		AND NOT IS_PLAYER_TEST_DRIVING_A_VEHICLE_AS_PASSENGER(PLAYER_ID())
			IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR_OF_TYPE(SIMPLE_INTERIOR_TYPE_CAR_MEET)
			IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL HAS_PREVIOUS_OWNER_RECREATED_THIS_PV(VEHICLE_INDEX VehicleID)

	IF DECOR_IS_REGISTERED_AS_TYPE("Previous_Owner", DECOR_TYPE_INT)
		IF DECOR_EXIST_ON(VehicleID, "Previous_Owner")

			PLAYER_INDEX PlayerID
			INT i
				
			REPEAT NUM_NETWORK_PLAYERS i
				PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
				IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, i), FALSE, FALSE)
					IF NETWORK_HASH_FROM_PLAYER_HANDLE(PlayerID) = DECOR_GET_INT(VehicleID, "Previous_Owner")
						
						// we've found the previous owner, does he have a pv? 
						IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[i].netID_PV)
						AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[i].netID_PV)
						AND NOT (NET_TO_VEH(GlobalplayerBD[i].netID_PV) = VehicleID)							
						AND DECOR_IS_REGISTERED_AS_TYPE("PV_Slot", DECOR_TYPE_INT)
						AND (DECOR_EXIST_ON(VehicleID, "PV_Slot") AND DECOR_EXIST_ON(NET_TO_VEH(GlobalplayerBD[i].netID_PV), "PV_Slot"))
						AND (DECOR_GET_INT(VehicleID, "PV_Slot") = DECOR_GET_INT(NET_TO_VEH(GlobalplayerBD[i].netID_PV), "PV_Slot"))
										
							// the previous owner has recreated a pv from that slot
							NET_PRINT("HAS_PREVIOUS_OWNER_RECREATED_THIS_PV - returning TRUE") NET_NL()
							
							DEBUG_PRINTCALLSTACK()
							
							RETURN(TRUE)										
							
						ELSE
							RETURN(FALSE)
						ENDIF
						
					ENDIF
				ENDIF
			ENDREPEAT
			
		ENDIF
	ENDIF

	RETURN(FALSE)

ENDFUNC

//FUNC BOOL IS_THIS_VEHICLE_AN_OLD_PV_CLONE(VEHICLE_INDEX VehicleID)
//	IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
//	AND DOES_ENTITY_EXIST(VehicleID)	
//	AND IS_VEHICLE_DRIVEABLE(VehicleID)
//	AND NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)			
//	AND DECOR_EXIST_ON(VehicleID,"Player_Vehicle")
//	AND (DECOR_GET_INT(VehicleID, "Player_Vehicle") = -1)
//	AND IS_VEHICLE_EMPTY(VehicleID)
//	AND NOT IS_ENTITY_A_MISSION_ENTITY(VehicleID)
//		RETURN(TRUE)
//	ENDIF
//	RETURN(FALSE)
//ENDFUNC

//PROC MAINTAIN_UNFREEZE_FOR_COLLISION_CHECK()
//	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//		VEHICLE_INDEX VehicleID
//		VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//		BOOL bSetAsMissionEntity
//		IF CAN_EDIT_THIS_ENTITY(VehicleID, bSetAsMissionEntity)
//			IF GET_IS_ALLOW_FREEZE_WAITING_ON_COLLISION_SET(VehicleID)
//				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(VehicleID, FALSE)	
//				PRINTLN("MAINTAIN_UNFREEZE_FOR_COLLISION_CHECK - unsetting freeze waiting on collision")
//			ENDIF
//		ENDIF
//		IF (bSetAsMissionEntity)
//			SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
//		ENDIF
//	ENDIF
//ENDPROC






PROC SET_THIS_PV_AS_UNUSABLE(VEHICLE_INDEX &VehicleID, BOOL bDelete=FALSE)
	NET_PRINT("[personal_vehicle] SET_THIS_PV_AS_UNUSABLE - called.") NET_NL()
	DEBUG_PRINTCALLSTACK()
	
	BOOL bSetAsMissionEntity
	
	IF CAN_EDIT_THIS_ENTITY(VehicleID, bSetAsMissionEntity)
		
		// if this vehicle is a personal truck.
		IF IS_VEHICLE_ATTACHED_TO_TRAILER(VehicleID)			
			VEHICLE_INDEX TrailerID
			IF GET_VEHICLE_TRAILER_VEHICLE(VehicleID, TrailerID)
				PRINTLN("[personal_vehicle] SET_THIS_PV_AS_UNUSABLE - got trailer.")
			ENDIF			
			PRINTLN("[personal_vehicle] SET_THIS_PV_AS_UNUSABLE - detaching from trailer")
			DETACH_VEHICLE_FROM_TRAILER(VehicleID)					
			IF DOES_ENTITY_EXIST(TrailerID)
				PRINTLN("[personal_vehicle] SET_THIS_PV_AS_UNUSABLE - calling on trailer")
				SET_THIS_PV_AS_UNUSABLE(TrailerID)
			ENDIF
		ENDIF
		
		// if this vehicle is attached to anything, then detach first. 
		IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(VehicleID)
			PRINTLN("[personal_vehicle] SET_THIS_PV_AS_UNUSABLE - detaching from vehicle")
			DETACH_ENTITY(VehicleID)	
		ENDIF
		
		LOCK_PV_AND_REMOVE_DECORATOR(VehicleID)
		

		// delete straigth away if in the impound yard
		IF SHOULD_VEHICLE_BE_DELETED_ON_CLEANUP(VehicleID)
		OR (bDelete)
			NET_PRINT("[personal_vehicle] SET_THIS_PV_AS_UNUSABLE DEL - deleting.") NET_NL()
			//PRINTLN("[personal_vehicle] SET_THIS_PV_AS_UNUSABLE - Impound: ", IS_ENTITY_IN_IMPOUND_YARD(VehicleID), " Clubhouse: ", IS_ENTITY_IN_CLUBHOUSE(VehicleID), 
			//" Visibile: ", IS_ENTITY_VISIBLE(VehicleID), " bDelete: ", bDelete, 
			//" (vPosition.z < -89.0): ", (vPosition.z < -89.0), " IN AREA: ",  IS_ENTITY_IN_AREA(VehicleID, <<1097.5347, -3016.0115, -40.7658>>, <<1109.2977, -2983.6902, -34.1882>>, FALSE, FALSE))
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),VehicleID)
					NET_PRINT("[personal_vehicle] SET_THIS_PV_AS_UNUSABLE - local player is inside vehicle about to be deleted, clearing tasks.") NET_NL()
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				ENDIF
			ENDIF			
			
			DELETE_VEHICLE(VehicleID)
			NET_PRINT("[personal_vehicle] SET_THIS_PV_AS_UNUSABLE - deleting.") NET_NL()
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
			NET_PRINT("[personal_vehicle] SET_THIS_PV_AS_UNUSABLE - deleting 1") NET_NL()
		ENDIF	
		
	ELSE
		IF (bSetAsMissionEntity)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
			NET_PRINT("[personal_vehicle] SET_THIS_PV_AS_UNUSABLE - SET_VEHICLE_AS_NO_LONGER_NEEDED 2") NET_NL()
		ENDIF
	ENDIF
	
ENDPROC


PROC NET_PRINT_VEHICLE_DECORATOR_DETAILS(VEHICLE_INDEX VehicleID)
	IF DECOR_EXIST_ON(VehicleID,"Player_Vehicle")							
		NET_PRINT(", Player_Vehicle = ") NET_PRINT_INT(DECOR_GET_INT(VehicleID, "Player_Vehicle"))  
	ENDIF
	
	IF DECOR_EXIST_ON(VehicleID,"Previous_Owner")	
		NET_PRINT(", Previous_Owner = ") NET_PRINT_INT(DECOR_GET_INT(VehicleID, "Previous_Owner"))  	
	ENDIF					
	
	IF DECOR_EXIST_ON(VehicleID,"PV_Slot")	
		NET_PRINT(", PV_Slot = ") NET_PRINT_INT(DECOR_GET_INT(VehicleID, "PV_Slot"))  	
	ENDIF
ENDPROC


FUNC BOOL IS_THIS_VEHICLE_AN_OLD_PV_CLONE_OF_PLAYER(VEHICLE_INDEX VehicleID, INT iPlayerNameHash, INT iSlot)
	IF DOES_ENTITY_EXIST(VehicleID)	
	//AND IS_VEHICLE_DRIVEABLE(VehicleID) - want to remove old destroyed pv's as well. 2161062
	AND NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)
	AND IS_VEHICLE_EMPTY(VehicleID, TRUE, TRUE, DEFAULT, TRUE)
	AND NOT IS_ENTITY_A_MISSION_ENTITY(VehicleID)	
	AND DECOR_EXIST_ON(VehicleID,"Player_Vehicle")
	AND (DECOR_GET_INT(VehicleID, "Player_Vehicle") = -1)
	AND DECOR_EXIST_ON(VehicleID, "Previous_Owner")
	AND (DECOR_GET_INT(VehicleID, "Previous_Owner") = iPlayerNameHash)
	AND (DECOR_EXIST_ON(VehicleID, "PV_Slot") OR (iSlot=-1))	
	AND ((DECOR_GET_INT(VehicleID, "PV_Slot") = iSlot) OR (iSlot=-1))
		RETURN(TRUE)
	ELSE
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("[personal_vehicle] DOES_ENTITY_EXIST(VehicleID) = ") NET_PRINT_BOOL(DOES_ENTITY_EXIST(VehicleID)) NET_NL()
//			NET_PRINT("[personal_vehicle] NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID) = ") NET_PRINT_BOOL(NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)) NET_NL()
//			NET_PRINT("[personal_vehicle] IS_VEHICLE_EMPTY(VehicleID, TRUE, TRUE) = ") NET_PRINT_BOOL(IS_VEHICLE_EMPTY(VehicleID, TRUE, TRUE)) NET_NL()
//			NET_PRINT("[personal_vehicle] IS_ENTITY_A_MISSION_ENTITY(VehicleID) = ") NET_PRINT_BOOL(IS_ENTITY_A_MISSION_ENTITY(VehicleID)) NET_NL()
//		#ENDIF
	ENDIF		
	RETURN(FALSE)
ENDFUNC

PROC DELETE_PV_CLONE(VEHICLE_INDEX VehicleID)
	PRINTLN("[personal_vehicle] called DELETE_PV_CLONE ") 
	DEBUG_PRINTCALLSTACK()
	
	SET_ENTITY_AS_MISSION_ENTITY(VehicleID, FALSE, TRUE)
	DELETE_VEHICLE(VehicleID)		
ENDPROC


FUNC VEHICLE_INDEX GET_NEARBY_VEHICLE_ATTACHED_TO_TRAILER(VEHICLE_INDEX TrailerID)
	
	INT iTotal
	
	iTotal = GET_ALL_VEHICLES(g_PoolVehicles)
	
	PRINTLN("[personal_vehicle] GET_NEARBY_VEHICLE_ATTACHED_TO_TRAILER - iTotal ", iTotal) 
	
	INT i
	REPEAT iTotal i
		IF DOES_ENTITY_EXIST(g_PoolVehicles[i])
		AND NOT IS_ENTITY_DEAD(g_PoolVehicles[i])		
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[personal_vehicle] GET_NEARBY_VEHICLE_ATTACHED_TO_TRAILER - vehicle ", i, " details:") 
				PRINT_VEHICLE_DETAILS(g_PoolVehicles[i])
			#ENDIF
		
			IF NOT (g_PoolVehicles[i] = TrailerID)
				IF IS_VEHICLE_ATTACHED_TO_TRAILER(g_PoolVehicles[i])				
				AND IS_VEHICLE_A_PERSONAL_TOW_VEHICLE(g_PoolVehicles[i])
					PRINTLN("[personal_vehicle] GET_NEARBY_VEHICLE_ATTACHED_TO_TRAILER - found ") 
					RETURN g_PoolVehicles[i]
				ENDIF
			ENDIF 
		ENDIF
	ENDREPEAT
	
	PRINTLN("[personal_vehicle] GET_NEARBY_VEHICLE_ATTACHED_TO_TRAILER - none found ") 
	RETURN INT_TO_NATIVE(VEHICLE_INDEX, -1)
	
ENDFUNC

PROC SET_REMOTE_PV(VEHICLE_INDEX NewPV, INT iPlayer)			
	// go through all currently stored pv and run the previous owner cleanup.
	IF DECOR_EXIST_ON(NewPV,"Player_Vehicle")
	AND DECOR_EXIST_ON(NewPV, "PV_Slot")	
	
		NET_PRINT("[personal_vehicle] SET_REMOTE_PV - new vehicle has pv details: ")		
		NET_PRINT_VEHICLE_DECORATOR_DETAILS(NewPV)
		NET_NL()
		
		INT i
		REPEAT NUM_NETWORK_PLAYERS i
			IF IS_THIS_VEHICLE_AN_OLD_PV_CLONE_OF_PLAYER(MPGlobals.RemotePV[i], DECOR_GET_INT(NewPV, "Player_Vehicle"), DECOR_GET_INT(NewPV, "PV_Slot") )	
				DELETE_PV_CLONE(MPGlobals.RemotePV[i])
				MPGlobals.RemotePV[i] = NULL
				PRINTLN("[personal_vehicle] SET_REMOTE_PV - deleted previous owned saved vehicle - player slot ", i) 
			ENDIF
		ENDREPEAT
	ENDIF
					
	MPGlobals.RemotePV[iPlayer] = NewPV		
		
	VEHICLE_INDEX TowVehicleID
	IF IS_MODEL_A_PERSONAL_TRAILER(GET_ENTITY_MODEL(MPGlobals.RemotePV[iPlayer]))
		PRINTLN("[personal_vehicle] SET_REMOTE_PV - remove pv is a trailer. player ", iPlayer)
		TowVehicleID = GET_NEARBY_VEHICLE_ATTACHED_TO_TRAILER(MPGlobals.RemotePV[iPlayer])
		IF DOES_ENTITY_EXIST(TowVehicleID)
		AND NOT IS_VEHICLE_A_PERSONAL_VEHICLE(TowVehicleID)
			MPGlobals.RemoteTowVehicle[iPlayer] = TowVehicleID
			PRINTLN("[personal_vehicle] SET_REMOTE_PV - storing tow vehicle for player ", iPlayer) 
		ENDIF
	ENDIF				
ENDPROC

PROC LockNearbyPVCheck(VEHICLE_INDEX VehicleID, PLAYER_INDEX PlayerID, INT iPlayerNameHash)
	IF DOES_ENTITY_EXIST(VehicleID)
	AND NOT IS_ENTITY_DEAD(VehicleID)
	
		#IF IS_DEBUG_BUILD
		NET_PRINT("[personal_vehicle] LockNearbyPVCheck - checking vehicle model ") NET_PRINT(GET_MODEL_NAME_FOR_DEBUG (GET_ENTITY_MODEL(VehicleID)))
		NET_PRINT(" at coords ") NET_PRINT_VECTOR(GET_ENTITY_COORDS(VehicleID))  
		NET_PRINT_VEHICLE_DECORATOR_DETAILS(VehicleID)		
		NET_NL()
		#ENDIF
	
		IF DECOR_EXIST_ON(VehicleID,"Player_Vehicle")	
			IF (DECOR_GET_INT(VehicleID, "Player_Vehicle") = iPlayerNameHash)
				NET_PRINT("[personal_vehicle] LockNearbyPVCheck - found vehicle belonging to player in question. player id= ") NET_PRINT_INT(NATIVE_TO_INT(PlayerID))  NET_NL()		
				
				// store this entity in the MPGlobals.RemotePV[NATIVE_TO_INT(PlayerID)] array - so it will get cleaned up by CLEAR_REMOTE_SAVED_VEHICLE.					
				IF NATIVE_TO_INT(PlayerID) > -1				
					IF NOT (MPGlobals.RemotePV[NATIVE_TO_INT(PlayerID)] = VehicleID)
						IF DOES_ENTITY_EXIST(MPGlobals.RemotePV[NATIVE_TO_INT(PlayerID)])
							NET_PRINT("[personal_vehicle] LockNearbyPVCheck - player already has an entity stored in MPGlobals.RemotePV[].")  NET_NL()
						ELSE
							NET_PRINT("[personal_vehicle] LockNearbyPVCheck - storing in MPGlobals.RemotePV[].")  NET_NL()	
							SET_REMOTE_PV(VehicleID, NATIVE_TO_INT(PlayerID))
						ENDIF
					ELSE
						NET_PRINT("[personal_vehicle] LockNearbyPVCheck - vehicle already = MPGlobals.RemotePV[].")  NET_NL()	
					ENDIF
				ELSE
					NET_PRINT("[personal_vehicle] LockNearbyPVCheck - NOT NATIVE_TO_INT(PlayerID) > -1.")  NET_NL()		
				ENDIF	
			ELSE				
				//NET_PRINT("[personal_vehicle] LockNearbyPVCheck - Player_Vehicle != iPlayerNameHash")  NET_NL()			
			ENDIF
		ELSE
			//NET_PRINT("[personal_vehicle] LockNearbyPVCheck - not a player vehicle.")  NET_NL()	
		ENDIF
			
	ELSE
		//NET_PRINT("[personal_vehicle] LockNearbyPVCheck - vehicle dead or does not exist.")  NET_NL()	
	ENDIF	
ENDPROC

FUNC BOOL HAS_INVALIDATED_MODDED_BY_DECORATOR_ON_VEHICLE(VEHICLE_INDEX &VehicleID)
	BOOL bReturn = FALSE
	INT iValue
	BOOL bSetAsMissionEntity
	IF DOES_ENTITY_EXIST(VehicleID)		
		IF DECOR_EXIST_ON(VehicleID,"Veh_Modded_By_Player")	
			iValue = DECOR_GET_INT(VehicleID, "Veh_Modded_By_Player")		
			IF NOT (iValue = -1)
				IF CAN_EDIT_THIS_ENTITY(VehicleID, bSetAsMissionEntity)
					DECOR_SET_INT(VehicleID, "Veh_Modded_By_Player", -1)	
					IF (bSetAsMissionEntity)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
					ENDIF
					VehicleID = INT_TO_NATIVE(VEHICLE_INDEX, -1)
					PRINTLN("HAS_INVALIDATED_MODDED_BY_DECORATOR_ON_VEHICLE - cleared modded by decorator")
					bReturn = TRUE
				ELSE
					IF (bSetAsMissionEntity)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
					ENDIF
				ENDIF
			ELSE
				VehicleID = INT_TO_NATIVE(VEHICLE_INDEX, -1)
				PRINTLN("HAS_INVALIDATED_MODDED_BY_DECORATOR_ON_VEHICLE - decorator already cleared")
			ENDIF
		ELSE
			VehicleID = INT_TO_NATIVE(VEHICLE_INDEX, -1)
			PRINTLN("HAS_INVALIDATED_MODDED_BY_DECORATOR_ON_VEHICLE - decorator doesnt exist")
		ENDIF
	ENDIF
	RETURN bReturn
ENDFUNC

PROC SET_NEARBY_MODDED_VEHICLES_TO_INVALIDATE_DECORATOR(GAMER_HANDLE hPlayer)
	
	INT i
	INT iNumVehicles
	INT iValue
	INT iFreeSlot
	
	// check any nearby vehicles
	
	iNumVehicles = GET_ALL_VEHICLES(g_PoolVehicles)
	PRINTLN("SET_NEARBY_MODDED_VEHICLES_TO_INVALIDATE_DECORATOR = iNumVehicles = ", iNumVehicles) 
	REPEAT iNumVehicles i
	
		IF DOES_ENTITY_EXIST(g_PoolVehicles[i])
		AND NOT IS_ENTITY_DEAD(g_PoolVehicles[i])
	
			IF DECOR_EXIST_ON(g_PoolVehicles[i],"Veh_Modded_By_Player")															
			
				iValue = DECOR_GET_INT(g_PoolVehicles[i], "Veh_Modded_By_Player")
				IF (iValue = NETWORK_HASH_FROM_GAMER_HANDLE(hPlayer))	
		
					PRINTLN("SET_NEARBY_MODDED_VEHICLES_TO_INVALIDATE_DECORATOR found a vehicle!") 
					
					// try and invalidate it now
					IF HAS_INVALIDATED_MODDED_BY_DECORATOR_ON_VEHICLE(g_PoolVehicles[i])
						
						PRINTLN("SET_NEARBY_MODDED_VEHICLES_TO_INVALIDATE_DECORATOR - managed to invalidate decorator.") 
						
					ELSE // add to list 
										
						// add to a free slot
						REPEAT 3 iFreeSlot
							
							// cleanup previous
							IF DOES_ENTITY_EXIST(MPGlobals.ModVehicleToLockListID[iFreeSlot])
								iValue = DECOR_GET_INT(MPGlobals.ModVehicleToLockListID[iFreeSlot] , "Veh_Modded_By_Player")	
								IF (iValue = -1)
									MPGlobals.ModVehicleToLockListID[iFreeSlot] = INT_TO_NATIVE(VEHICLE_INDEX, -1)
									PRINTLN("SET_NEARBY_MODDED_VEHICLES_TO_INVALIDATE_DECORATOR - cleaned up slot ", iFreeSlot)
								ENDIF
							ENDIF
							
							// add to slot
							IF NOT DOES_ENTITY_EXIST(MPGlobals.ModVehicleToLockListID[iFreeSlot])
								MPGlobals.ModVehicleToLockListID[iFreeSlot] = g_PoolVehicles[i]
								PRINTLN("SET_NEARBY_MODDED_VEHICLES_TO_INVALIDATE_DECORATOR - added vehicle to slot ", iFreeSlot) 
								
								iFreeSlot = 3
							ENDIF
						ENDREPEAT
						
					ENDIF
																					
				ENDIF
			ENDIF		
			
		ENDIF
		
	ENDREPEAT			
			
ENDPROC

PROC UPDATE_NEARBY_MODDED_VEHICLES_TO_INVALIDATE_DECORATOR()
	INT i
	REPEAT 3 i
		IF HAS_INVALIDATED_MODDED_BY_DECORATOR_ON_VEHICLE(MPGlobals.ModVehicleToLockListID[i])
			PRINTLN("UPDATE_NEARBY_MODDED_VEHICLES_TO_INVALIDATE_DECORATOR - managed to invalidate decorator.")
		ENDIF
	ENDREPEAT
ENDPROC

PROC LOCK_NEARBY_PV_BELONGING_TO_PLAYERHASH(PLAYER_INDEX PlayerID, INT iPlayerNameHash)

	NET_PRINT("[personal_vehicle] LOCK_NEARBY_PV_BELONGING_TO_PLAYERHASH - called with PlayerID ") NET_PRINT_INT(NATIVE_TO_INT(PlayerID)) NET_PRINT(" iPlayerNameHash = ") NET_PRINT_INT(iPlayerNameHash) NET_NL()

	// exit if the player slot is now occupied by a different player.
	IF IS_NET_PLAYER_OK(PlayerID, FALSE, FALSE)
	AND (NETWORK_HASH_FROM_PLAYER_HANDLE(PlayerID) != iPlayerNameHash)
		NET_PRINT("[personal_vehicle] LOCK_NEARBY_PV_BELONGING_TO_PLAYERHASH - a different player now exists in this player slot.")  NET_NL()	
		EXIT
	ENDIF
	
	INT i

	// check personal vehicles we already have stored
	REPEAT NUM_NETWORK_PLAYERS i
		//NET_PRINT("[personal_vehicle] LOCK_NEARBY_PV_BELONGING_TO_PLAYERHASH - checking MPGlobals.RemotePV[") NET_PRINT_INT(i) NET_PRINT("]")  NET_NL()
		LockNearbyPVCheck(MPGlobals.RemotePV[i], PlayerID, iPlayerNameHash)
	ENDREPEAT

	// check any nearby vehicles
	INT iNumVehicles
	iNumVehicles = GET_ALL_VEHICLES(g_PoolVehicles)
	NET_PRINT("[personal_vehicle] LOCK_NEARBY_PV_BELONGING_TO_PLAYERHASH = iNumVehicles = ") NET_PRINT_INT(iNumVehicles) NET_NL()
	REPEAT iNumVehicles i
		LockNearbyPVCheck(g_PoolVehicles[i], PlayerID, iPlayerNameHash) 
	ENDREPEAT

ENDPROC
//
//FUNC BOOL IS_THIS_VEHICLE_AN_OLD_PV_CLONE_OF_PLAYER(VEHICLE_INDEX VehicleID, INT iPlayerNameHash, INT iSlot)
//	IF DOES_ENTITY_EXIST(VehicleID)	
//	//AND IS_VEHICLE_DRIVEABLE(VehicleID) - want to remove old destroyed pv's as well. 2161062
//	AND NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)
//	AND IS_VEHICLE_EMPTY(VehicleID, TRUE, TRUE)
//	AND NOT IS_ENTITY_A_MISSION_ENTITY(VehicleID)	
//	AND DECOR_EXIST_ON(VehicleID,"Player_Vehicle")
//	AND (DECOR_GET_INT(VehicleID, "Player_Vehicle") = -1)
//	AND DECOR_EXIST_ON(VehicleID, "Previous_Owner")
//	AND (DECOR_GET_INT(VehicleID, "Previous_Owner") = iPlayerNameHash)
//	AND (DECOR_EXIST_ON(VehicleID, "PV_Slot") OR (iSlot=-1))	
//	AND ((DECOR_GET_INT(VehicleID, "PV_Slot") = iSlot) OR (iSlot=-1))
//		RETURN(TRUE)
//	ELSE
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("[personal_vehicle] DOES_ENTITY_EXIST(VehicleID) = ") NET_PRINT_BOOL(DOES_ENTITY_EXIST(VehicleID)) NET_NL()
//			NET_PRINT("[personal_vehicle] NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID) = ") NET_PRINT_BOOL(NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)) NET_NL()
//			NET_PRINT("[personal_vehicle] IS_VEHICLE_EMPTY(VehicleID, TRUE, TRUE) = ") NET_PRINT_BOOL(IS_VEHICLE_EMPTY(VehicleID, TRUE, TRUE)) NET_NL()
//			NET_PRINT("[personal_vehicle] IS_ENTITY_A_MISSION_ENTITY(VehicleID) = ") NET_PRINT_BOOL(IS_ENTITY_A_MISSION_ENTITY(VehicleID)) NET_NL()
//		#ENDIF
//	ENDIF		
//	RETURN(FALSE)
//ENDFUNC

//PROC DELETE_PV_CLONE(VEHICLE_INDEX VehicleID)
//	PRINTLN("[personal_vehicle] called DELETE_PV_CLONE ") 
//	DEBUG_PRINTCALLSTACK()
//	
//	SET_ENTITY_AS_MISSION_ENTITY(VehicleID, FALSE, TRUE)
//	DELETE_VEHICLE(VehicleID)		
//ENDPROC

FUNC BOOL IS_VEHICLE_ATTACHED_TO_MISSION_VEHICLE(VEHICLE_INDEX VehicleID)
	IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(VehicleID)
		ENTITY_INDEX EntityID = GET_ENTITY_ATTACHED_TO(VehicleID)
		IF DOES_ENTITY_EXIST(EntityID)
		AND IS_ENTITY_A_MISSION_ENTITY(EntityID)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL DELETE_ANY_ATTACHED_AMBIENT_VEHICLE(VEHICLE_INDEX VehicleID)
	IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(VehicleID)
		ENTITY_INDEX EntityID = GET_ENTITY_ATTACHED_TO(VehicleID)	
		IF DOES_ENTITY_EXIST(EntityID)
			IF IS_ENTITY_A_VEHICLE(EntityID)
				VEHICLE_INDEX AttachedVehicleID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(EntityID)			
				IF NETWORK_HAS_CONTROL_OF_ENTITY(AttachedVehicleID)
					SET_ENTITY_AS_MISSION_ENTITY(AttachedVehicleID, FALSE, TRUE)  
					DELETE_VEHICLE(AttachedVehicleID)
					PRINTLN("DELETE_ANY_ATTACHED_AMBIENT_VEHICLE, deleting attached ambient vehicle. ")
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(AttachedVehicleID)
					PRINTLN("DELETE_ANY_ATTACHED_AMBIENT_VEHICLE, requesting control of attached vehicle. ")				
				ENDIF
				RETURN FALSE
			ENDIF	
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC CLEAR_ANY_AMBIENT_VEHICLES_FROM_VEHICLE_BOUNDS(VEHICLE_INDEX ThisVehicleID)

	
	IF DOES_ENTITY_EXIST(ThisVehicleID)
	AND NOT IS_ENTITY_DEAD(ThisVehicleID)

		PRINTLN("CLEAR_ANY_AMBIENT_VEHICLES_FROM_VEHICLE_BOUNDS called...")

		INT iNumVehicles
		INT i
		
		VECTOR vPos1 = GET_ENTITY_COORDS(ThisVehicleID)
		FLOAT fHeading1 = GET_ENTITY_HEADING(ThisVehicleID)
		MODEL_NAMES Model1 = GET_ENTITY_MODEL(ThisVehicleID)
		
		VECTOR vPos2
		FLOAT fHeading2
		MODEL_NAMES Model2
		
	 
		iNumVehicles = GET_ALL_VEHICLES(g_PoolVehicles)

		PRINTLN("CLEAR_ANY_AMBIENT_VEHICLES_FROM_VEHICLE_BOUNDS, iNumVehicles = ", iNumVehicles)
		
		REPEAT iNumVehicles i
		    IF DOES_ENTITY_EXIST(g_PoolVehicles[i])
			AND NOT IS_ENTITY_DEAD(g_PoolVehicles[i])
			
				IF NOT (GET_ENTITY_POPULATION_TYPE(g_PoolVehicles[i]) = PT_RANDOM_PERMANENT) // so we don't delete trains etc. see 1984133
			
					IF NOT IS_ENTITY_A_MISSION_ENTITY(g_PoolVehicles[i])
										
						IF NOT IS_VEHICLE_ATTACHED_TO_MISSION_VEHICLE(g_PoolVehicles[i])
							
							vPos2 = GET_ENTITY_COORDS(g_PoolVehicles[i])
							fHeading2 = GET_ENTITY_HEADING(g_PoolVehicles[i])
							Model2 = GET_ENTITY_MODEL(g_PoolVehicles[i])				
						
							IF DoVehicleBoundsOverlap(vPos1, fHeading1, Model1, vPos2, fHeading2, Model2)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(g_PoolVehicles[i])
									IF DELETE_ANY_ATTACHED_AMBIENT_VEHICLE(g_PoolVehicles[i])
										PRINTLN("CLEAR_ANY_AMBIENT_VEHICLES_FROM_VEHICLE_BOUNDS, vehicle is overlapping, going to delete. i = ", i)
										
										SET_ENTITY_AS_MISSION_ENTITY(g_PoolVehicles[i], FALSE, TRUE)  
										DELETE_VEHICLE(g_PoolVehicles[i])
									ELSE
										PRINTLN("CLEAR_ANY_AMBIENT_VEHICLES_FROM_VEHICLE_BOUNDS, vehicle is overlapping, but deleting attachment first. i = ", i)										
									ENDIF
								ELSE
									NETWORK_REQUEST_CONTROL_OF_ENTITY(g_PoolVehicles[i])
								ENDIF
							
							ENDIF	
						ELSE
							PRINTLN("CLEAR_ANY_AMBIENT_VEHICLES_FROM_VEHICLE_BOUNDS, attached to a mission vehicle. ")	
						ENDIF
					ENDIF
					
				ENDIF
		    ENDIF
		ENDREPEAT
      
	ENDIF

ENDPROC

PROC PREVENT_COLLISIIONS_WITH_NEARBY_VEHICLES(VEHICLE_INDEX viVeh, BOOL bConceal)
	
	DEBUG_PRINTCALLSTACK()
	
	PRINTLN("PREVENT_COLLISIIONS_WITH_NEARBY_VEHICLES - Calling function with bConceal: ", bConceal) 
	
	// check any nearby vehicles
	#IF IS_DEBUG_BUILD
	VECTOR vCoords
	TEXT_LABEL_31 tl31
	#ENDIF
	INT iNumVehicles
	INT i
	
	iNumVehicles = GET_ALL_VEHICLES(g_PoolVehicles)
	
	PRINTLN("PREVENT_COLLISIIONS_WITH_NEARBY_VEHICLES = iNumVehicles = ", iNumVehicles)
	
	FOR i = 0 TO iNumVehicles-1
		IF NOT DOES_ENTITY_EXIST(g_PoolVehicles[i])
			RELOOP
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(viVeh)
			RELOOP
		ENDIF
	
		IF IS_ENTITY_DEAD(viVeh)
			RELOOP
		ENDIF
		
		IF viVeh = g_PoolVehicles[i]
			RELOOP
		ENDIF
		
		IF VDIST2(GET_ENTITY_COORDS(viVeh, FALSE), GET_ENTITY_COORDS(g_PoolVehicles[i], FALSE)) >= POW(30.0, 2.0)
		AND bConceal
			RELOOP
		ENDIF
				
		IF NETWORK_GET_ENTITY_IS_NETWORKED(g_PoolVehicles[i])				
			#IF IS_DEBUG_BUILD
			vCoords = GET_ENTITY_COORDS(g_PoolVehicles[i], FALSE)
			tl31 = "Veh Net ID "
			tl31 += NETWORK_ENTITY_GET_OBJECT_ID(g_PoolVehicles[i])
			PRINTLN("PREVENT_COLLISIIONS_WITH_NEARBY_VEHICLES - found a vehicle at coords = ", vCoords, " Calling NETWORK_CONCEAL_ENTITY on ", tl31)			
			#ENDIF
			
			NETWORK_CONCEAL_ENTITY(g_PoolVehicles[i], bConceal)		
		ENDIF			
		
	ENDFOR
	
ENDPROC

PROC DELETE_ANY_OLD_PV_NEARBY()
	PRINTLN("[personal_vehicle] called DELETE_ANY_OLD_PV_NEARBY ") 
	DEBUG_PRINTCALLSTACK()
	// check any nearby vehicles
	INT iNumVehicles
	INT i
	
	#IF IS_DEBUG_BUILD
	VECTOR vCoords
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[personal_vehicle] DELETE_ANY_OLD_PV_NEARBY - player pos =  ", vCoords) 
	#ENDIF
	
	iNumVehicles = GET_ALL_VEHICLES(g_PoolVehicles)
	
	NET_PRINT("[personal_vehicle] DELETE_ANY_OLD_PV_NEARBY = iNumVehicles = ") NET_PRINT_INT(iNumVehicles) NET_NL()
	REPEAT iNumVehicles i
		IF DOES_ENTITY_EXIST(g_PoolVehicles[i])
		AND DECOR_EXIST_ON(g_PoolVehicles[i],"Player_Vehicle")
		AND (DECOR_GET_INT(g_PoolVehicles[i], "Player_Vehicle") = -1)

			#IF IS_DEBUG_BUILD
			vCoords = GET_ENTITY_COORDS(g_PoolVehicles[i], FALSE)
			PRINTLN("[personal_vehicle] DELETE_ANY_OLD_PV_NEARBY - found an old pv at coords =  ", vCoords) 
			#ENDIF
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(g_PoolVehicles[i])	
				IF NOT IS_ENTITY_A_MISSION_ENTITY(g_PoolVehicles[i])							
					DELETE_PV_CLONE(g_PoolVehicles[i])	
					g_PoolVehicles[i] = NULL
				ELSE
					PRINTLN("[personal_vehicle] DELETE_ANY_OLD_PV_NEARBY - is a mission entity.")
				ENDIF
			ELSE
				PRINTLN("[personal_vehicle] DELETE_ANY_OLD_PV_NEARBY - doesn't have control.")
			ENDIF
			
		ENDIF
	ENDREPEAT
ENDPROC

PROC DELETE_OLD_PV_CLONES_OF_PLAYER(PLAYER_INDEX PlayerID, INT iSlot = -1)

	IF NOT IS_NET_PLAYER_OK(PlayerID, FALSE, FALSE)	
		PRINTLN("DELETE_OLD_PV_CLONES_OF_PLAYER - player ", NATIVE_TO_INT(PlayerID), " does not exist.")
		EXIT
	ENDIF

	NET_PRINT("[personal_vehicle] DELETE_OLD_PV_CLONES_OF_PLAYER called") NET_NL()
	#IF IS_DEBUG_BUILD
	NET_PRINT("[personal_vehicle] DELETE_OLD_PV_CLONES_OF_PLAYER - for player name ") NET_PRINT(GET_PLAYER_NAME(PlayerID)) NET_PRINT(" iSlot = ") NET_PRINT_INT(iSlot) NET_PRINT(", hash of player = ") NET_PRINT_INT(NETWORK_HASH_FROM_PLAYER_HANDLE(PlayerID)) NET_NL()
	#ENDIF	

	IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
	AND DECOR_IS_REGISTERED_AS_TYPE("Previous_Owner", DECOR_TYPE_INT)
	AND  DECOR_IS_REGISTERED_AS_TYPE("PV_Slot", DECOR_TYPE_INT)
		INT iPlayerNameHash = NETWORK_HASH_FROM_PLAYER_HANDLE(PlayerID)
		INT i		
			
		// check personal vehicles we already have stored
		REPEAT NUM_NETWORK_PLAYERS i
			#IF IS_DEBUG_BUILD				
				IF DOES_ENTITY_EXIST(MPGlobals.RemotePV[i])
				AND IS_ENTITY_A_VEHICLE(MPGlobals.RemotePV[i])
					NET_PRINT("[personal_vehicle] DELETE_OLD_PV_CLONES_OF_PLAYER - existing RemotePV ") NET_PRINT(GET_MODEL_NAME_FOR_DEBUG (GET_ENTITY_MODEL(MPGlobals.RemotePV[i])))
					NET_PRINT(" at coords ") NET_PRINT_VECTOR(GET_ENTITY_COORDS(MPGlobals.RemotePV[i], FALSE))  				
					NET_PRINT_VEHICLE_DECORATOR_DETAILS(MPGlobals.RemotePV[i])					
					NET_NL()
				ENDIF				
			#ENDIF					
			IF IS_THIS_VEHICLE_AN_OLD_PV_CLONE_OF_PLAYER(MPGlobals.RemotePV[i], iPlayerNameHash, iSlot)
				DELETE_PV_CLONE(MPGlobals.RemotePV[i])
				MPGlobals.RemotePV[i] = NULL
				NET_PRINT("[personal_vehicle] DELETE_OLD_PV_CLONES_OF_PLAYER - deleting old pv for player ") NET_PRINT(GET_PLAYER_NAME(PlayerID)) NET_NL()
			ENDIF
		ENDREPEAT

		// check any nearby vehicles
		INT iNumVehicles
		iNumVehicles = GET_ALL_VEHICLES(g_PoolVehicles)
		NET_PRINT("[personal_vehicle] DELETE_OLD_PV_CLONES_OF_PLAYER = iNumVehicles = ") NET_PRINT_INT(iNumVehicles) NET_NL()
		REPEAT iNumVehicles i
			IF DOES_ENTITY_EXIST(g_PoolVehicles[i])
				IF IS_THIS_VEHICLE_AN_OLD_PV_CLONE_OF_PLAYER(g_PoolVehicles[i], iPlayerNameHash, -1)
					DELETE_PV_CLONE(g_PoolVehicles[i])
					g_PoolVehicles[i] = NULL
					NET_PRINT("[personal_vehicle] DELETE_OLD_PV_CLONES_OF_PLAYER - deleting old clone for player ") NET_PRINT(GET_PLAYER_NAME(PlayerID)) NET_NL()
				ELSE
				#IF IS_DEBUG_BUILD
					
					NET_PRINT("[personal_vehicle] DELETE_OLD_PV_CLONES_OF_PLAYER - checking vehicle model ") NET_PRINT(GET_MODEL_NAME_FOR_DEBUG (GET_ENTITY_MODEL(g_PoolVehicles[i])))
					NET_PRINT(" at coords ") NET_PRINT_VECTOR(GET_ENTITY_COORDS(g_PoolVehicles[i], FALSE))  									
					NET_PRINT_VEHICLE_DECORATOR_DETAILS(g_PoolVehicles[i])					
					NET_NL()
					
				#ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	
	ELSE
		NET_PRINT("[personal_vehicle] DELETE_OLD_PV_CLONES_OF_PLAYER - decorators not registered.") NET_NL()		
	ENDIF
	
ENDPROC

PROC DELETE_ANY_OLD_PVS()

	PRINTLN("[personal_vehicle] DELETE_ANY_OLD_PVS called")
	DEBUG_PRINTCALLSTACK()
	
	INT iPlayer
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		IF IS_NET_PLAYER_OK(PlayerID, FALSE, FALSE)
			DELETE_OLD_PV_CLONES_OF_PLAYER(PlayerID)
		ENDIF
	ENDREPEAT
	
	DELETE_ANY_OLD_PV_NEARBY()
	
ENDPROC




PROC REMOVE_OLD_PV_CLONE_OF_PLAYER(PLAYER_INDEX PlayerID, INT iSlot)

	NET_PRINT("[personal_vehicle] REMOVE_OLD_PV_CLONE_OF_PLAYER called") NET_NL()
	#IF IS_DEBUG_BUILD
	NET_PRINT("[personal_vehicle] REMOVE_OLD_PV_CLONE_OF_PLAYER - for player name ") NET_PRINT(GET_PLAYER_NAME(PlayerID)) NET_PRINT(" iSlot = ") NET_PRINT_INT(iSlot)  NET_PRINT(", hash of player = ") 
	IF NETWORK_IS_PLAYER_ACTIVE(PlayerID)
		NET_PRINT_INT(NETWORK_HASH_FROM_PLAYER_HANDLE(PlayerID))
	ELSE
		NET_PRINT("(not active)")
	ENDIF
	NET_NL()
	#ENDIF

	DELETE_OLD_PV_CLONES_OF_PLAYER(PlayerID, iSlot)
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PlayerID)].netID_PV)
	AND DOES_ENTITY_EXIST(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PlayerID)].netID_PV))
		IF NOT (MPGlobals.RemotePV[NATIVE_TO_INT(PlayerID)] = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PlayerID)].netID_PV))
			PRINTLN("[personal_vehicle] REMOVE_OLD_PV_CLONE_OF_PLAYER - netID_PV is different from RemotePV .")
			SET_REMOTE_PV(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PlayerID)].netID_PV), NATIVE_TO_INT(PlayerID))
		ELSE
			PRINTLN("[personal_vehicle] REMOVE_OLD_PV_CLONE_OF_PLAYER - RemotePV = netID_PV.")		
		ENDIF			
	ELSE
		// store the current vehicle the player is in in the remotePV array.
		IF IS_NET_PLAYER_OK(PlayerID, TRUE, FALSE)
			IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(PlayerID))
				NET_PRINT("[personal_vehicle] REMOVE_OLD_PV_CLONE_OF_PLAYER storing player ") NET_PRINT_INT(NATIVE_TO_INT(PlayerID)) NET_PRINT("'s current vehicle as MPGlobals.RemotePV")  NET_NL()
				SET_REMOTE_PV(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PlayerID)), NATIVE_TO_INT(PlayerID))
			ELSE
				NET_PRINT("[personal_vehicle] REMOVE_OLD_PV_CLONE_OF_PLAYER player not in any vehicle. ")	NET_NL()
			ENDIF
		ELSE
			NET_PRINT("[personal_vehicle] REMOVE_OLD_PV_CLONE_OF_PLAYER player not ok. ")	NET_NL()
		ENDIF		
	ENDIF

ENDPROC


//FUNC BOOL SHOULD_DELETE_PREVIOUS_OWNED_SAVED_VEHICLE(VEHICLE_INDEX VehicleID)
//	IF IS_THIS_VEHICLE_AN_OLD_PV_CLONE(VehicleID)
//	AND HAS_PREVIOUS_OWNER_RECREATED_THIS_PV(VehicleID)	
//		RETURN(TRUE)
//	ENDIF
//	RETURN(FALSE)
//ENDFUNC
FUNC BOOL IS_PLAYER_HASH_STILL_IN_GAME(INT iHashName)
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i),FALSE,FALSE)
			//IF iHashName = GET_HASH_KEY(GET_PLAYER_NAME(INT_TO_PLAYERINDEX(i)))
			IF iHashName = NETWORK_HASH_FROM_PLAYER_HANDLE(INT_TO_PLAYERINDEX(i))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC CLEANUP_REMOTE_MP_SAVED_VEHICLE(VEHICLE_INDEX &tempVeh, TIME_DATATYPE &Timer)

	IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())

		BOOL bCleanupVehicle = FALSE
		BOOL bPlayerStillInSession = FALSE
		INT iDecoratorValue
		INT i
		PLAYER_INDEX tempPlayer	

		IF DOES_ENTITY_EXIST(tempVeh)
		AND IS_ENTITY_A_VEHICLE(tempVeh)
		AND IS_VEHICLE_DRIVEABLE(tempVeh)
			IF IS_VEHICLE_A_PERSONAL_VEHICLE(tempVeh)
				
				// is this my pv?
				IF (tempVeh = PERSONAL_VEHICLE_ID())
				AND IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
					NET_PRINT("[personal_vehicle] CLEANUP_REMOTE_MP_SAVED_VEHICLE: this is my pv which should be cleaned up by UPDATE_CLEANUP_MP_SAVED_VEHICLE") NET_NL()
					EXIT
				ENDIF			
			
				IF DECOR_GET_INT(tempVeh, "Player_Vehicle") = -1
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempVeh, TRUE)
					AND NOT IS_VEHICLE_IN_PLAYER_GARAGE(tempVeh)
						IF NOT (IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID()) OR IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID()) OR IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(),FALSE)) 
						OR g_bg_allowRemoteKickOnPropertyEntryExit 
							NET_PRINT("[personal_vehicle] CLEANUP_REMOTE_MP_SAVED_VEHICLE: player_vehicle decorator is -1") NET_NL()
							bCleanupVehicle = TRUE
						ENDIF
					ENDIF
				ELSE					
					
					IF IS_VEHICLE_IN_PLAYER_GARAGE(tempVeh)
						EXIT
					ENDIF		
					
					// If the player is no longer in the session.
					REPEAT NUM_NETWORK_PLAYERS i
						tempPlayer = INT_TO_PLAYERINDEX(i)
						IF IS_NET_PLAYER_OK(tempPlayer, FALSE, FALSE)
							IF DECOR_GET_INT(tempveh, "Player_Vehicle") = NETWORK_HASH_FROM_PLAYER_HANDLE(tempPlayer)
								bPlayerStillInSession = TRUE
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF NOT bPlayerStillInSession
						bCleanupVehicle = TRUE
					ENDIF
				ENDIF
			ELIF IS_VEHICLE_A_PERSONAL_TRUCK(tempVeh)
				IF DECOR_GET_INT(tempVeh, "Player_Truck") = -1
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempVeh, TRUE)
					AND NOT IS_VEHICLE_IN_PLAYER_GARAGE(tempVeh)
						NET_PRINT("[personal_vehicle] CLEANUP_REMOTE_MP_SAVED_VEHICLE: player_vehicle decorator is -1") NET_NL()
						bCleanupVehicle = TRUE
					ENDIF
				ELSE
				
					IF IS_VEHICLE_IN_PLAYER_GARAGE(tempVeh)
						EXIT
					ENDIF		
					
					// If the player is no longer in the session.
					REPEAT NUM_NETWORK_PLAYERS i
						tempPlayer = INT_TO_PLAYERINDEX(i)
						IF IS_NET_PLAYER_OK(tempPlayer, FALSE, FALSE)
							IF DECOR_GET_INT(tempveh, "Player_Truck") = NETWORK_HASH_FROM_PLAYER_HANDLE(tempPlayer)
								bPlayerStillInSession = TRUE
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF NOT bPlayerStillInSession
						bCleanupVehicle = TRUE
					ENDIF
				ENDIF
			ELIF IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(tempVeh)
				IF DECOR_GET_INT(tempVeh, "Player_Hacker_Truck") = -1
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempVeh, TRUE)
					AND NOT IS_VEHICLE_IN_PLAYER_GARAGE(tempVeh)
						NET_PRINT("[personal_vehicle] CLEANUP_REMOTE_MP_SAVED_VEHICLE: player_vehicle decorator is -1") NET_NL()
						bCleanupVehicle = TRUE
					ENDIF
				ELSE
				
					IF IS_VEHICLE_IN_PLAYER_GARAGE(tempVeh)
						EXIT
					ENDIF		
					
					// If the player is no longer in the session.
					REPEAT NUM_NETWORK_PLAYERS i
						tempPlayer = INT_TO_PLAYERINDEX(i)
						IF IS_NET_PLAYER_OK(tempPlayer, FALSE, FALSE)
							IF DECOR_GET_INT(tempveh, "Player_Hacker_Truck") = NETWORK_HASH_FROM_PLAYER_HANDLE(tempPlayer)
								bPlayerStillInSession = TRUE
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF NOT bPlayerStillInSession
						bCleanupVehicle = TRUE
					ENDIF
				ENDIF
			ELIF HAS_VEHICLE_BEEN_MODDED(tempVeh)
				iDecoratorValue = DECOR_GET_INT(tempVeh, "Veh_Modded_By_Player")
				IF NOT NETWORK_GET_ENTITY_IS_NETWORKED(tempVeh)
					IF (iDecoratorValue != 0 AND iDecoratorValue != -1)
						PRINTLN("CLEANUP_REMOTE_MP_SAVED_VEHICLE: Veh_Modded_By_Player decorator = ", iDecoratorValue)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
							DECOR_SET_INT(tempVeh, "Veh_Modded_By_Player", -1)
							PRINTLN("CLEANUP_REMOTE_MP_SAVED_VEHICLE: Veh_Modded_By_Player decorator set to -1")
						ENDIF
					ELSE
						bCleanupVehicle = TRUE
					ENDIF	
				ELSE
					IF (iDecoratorValue != 0 AND iDecoratorValue != -1)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
							IF NOT IS_PLAYER_HASH_STILL_IN_GAME(iDecoratorValue)
								PRINTLN("CLEANUP_REMOTE_MP_SAVED_VEHICLE - vehicle found with Veh_Modded_By_Player decorator and player not in game = ", iDecoratorValue)
								DECOR_SET_INT(tempVeh, "Veh_Modded_By_Player", -1)
								PRINTLN("CLEANUP_REMOTE_MP_SAVED_VEHICLE - Veh_Modded_By_Player decorator set to -1")
							ELSE
								PRINTLN("CLEANUP_REMOTE_MP_SAVED_VEHICLE - Modded Player is still active in game.")
							ENDIF
						ENDIF
					ELSE
						bCleanupVehicle = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bCleanupVehicle
			IF NOT IS_VEHICLE_EMPTY(tempVeh, TRUE, TRUE)
			AND NOT bPersonalVehicleCleanupDontLeaveVehicle
				IF NOT IS_PLAYER_ENTERING_PROPERTY(PLAYER_ID())
					NET_PRINT("[personal_vehicle] CLEANUP_REMOTE_MP_SAVED_VEHICLE: Kicking remote players out of old player vehicle so we can cleanup") NET_NL()
					IF ABSI(GET_TIME_DIFFERENCE(Timer, GET_NETWORK_TIME())) >= 1000
						REPEAT NUM_NETWORK_PLAYERS i
							tempPlayer = INT_TO_PLAYERINDEX(i)
							IF IS_NET_PLAYER_OK(tempPlayer, FALSE)
								IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(tempPlayer), tempVeh)
									BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(tempPlayer), TRUE, 0, 0, FALSE, FALSE)
								ENDIF
							ENDIF
						ENDREPEAT
						Timer = GET_NETWORK_TIME()
					ENDIF
				ELSE
					PRINTLN("[personal_vehicle] CLEANUP_REMOTE_MP_SAVED_VEHICLE: NOT Kicking entering garage")
				ENDIF	
			ENDIF
			
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
				IF NOT NETWORK_IS_IN_SPECTATOR_MODE()
					NET_PRINT("[personal_vehicle] CLEANUP_REMOTE_MP_SAVED_VEHICLE: Requesting control of saved vehicle") NET_NL()
					IF IS_VEHICLE_EMPTY(tempveh, TRUE, TRUE, TRUE) //to avoid flickering of ownership.
						NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
					ENDIF
				ELSE
					NET_PRINT("[personal_vehicle] CLEANUP_REMOTE_MP_SAVED_VEHICLE: is in spectator mode so doing nothing.") NET_NL()
				ENDIF
			ELSE
			
				INT iMPBitset
			
				IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
				AND DECOR_EXIST_ON(tempveh, "MPBitset")	
					iMPBitset = DECOR_GET_INT(tempVeh, "MPBitset")
				ENDIF
				
				IF IS_BIT_SET(iMPBitset, MP_DECORATOR_BS_VEHICLE_FADE_OUT)
				AND IS_ENTITY_VISIBLE(tempveh)
					
					IF DECOR_EXIST_ON(tempveh,"Player_Vehicle")
					AND NOT (DECOR_GET_INT(tempveh, "Player_Vehicle") = -1)
						NET_PRINT("[personal_vehicle] CLEANUP_REMOTE_MP_SAVED_VEHICLE - locking and invalidating decorator before fade. ") NET_NL()
						LOCK_PV_AND_REMOVE_DECORATOR(tempveh)						
					ENDIF
				
					IF NOT NETWORK_IS_ENTITY_FADING(tempveh)
						IF IS_VEHICLE_EMPTY(tempveh, TRUE)
							NET_PRINT("[personal_vehicle] CLEANUP_REMOTE_MP_SAVED_VEHICLE - fading out vehicle ") NET_NL()
							NETWORK_FADE_OUT_ENTITY(tempveh, FALSE, TRUE)	
						ELSE
							NET_PRINT("[personal_vehicle] CLEANUP_REMOTE_MP_SAVED_VEHICLE - want to fade, waiting to empty") NET_NL()
						ENDIF
					ELSE
						NET_PRINT("[personal_vehicle] CLEANUP_REMOTE_MP_SAVED_VEHICLE - vehicle is fading out ") NET_NL()	
					ENDIF
					
				ELSE

					
					IF DOES_ENTITY_EXIST(tempVeh)
						IF IS_VEHICLE_MODEL(tempVeh, TERBYTE)
						OR IS_VEHICLE_MODEL(tempVeh, AVENGER)
						OR IS_VEHICLE_MODEL(tempVeh, PHANTOM3)
						OR IS_VEHICLE_MODEL(tempVeh, HAULER2)
							NET_PRINT("[personal_vehicle] CLEANUP_REMOTE_MP_SAVED_VEHICLE: locking doors skip invalidating this vehicle index") NET_NL()
						ELSE
							NET_PRINT("[personal_vehicle] CLEANUP_REMOTE_MP_SAVED_VEHICLE: locking doors and invalidating vehicle decorator") NET_NL()
							SET_THIS_PV_AS_UNUSABLE(tempVeh)
					
							tempVeh = INT_TO_NATIVE(VEHICLE_INDEX, -1)
						ENDIF
					ENDIF	
					
					
				ENDIF

			ENDIF
		ENDIF	
		
	ELSE
		NET_PRINT("CLEANUP_REMOTE_MP_SAVED_VEHICLE: player is spectating, not cleaning up.") NET_NL()
	ENDIF

ENDPROC

#IF FEATURE_DLC_1_2022
FUNC BOOL SHOULD_DISABLE_REMOTE_CLEANUP_ON_MISSION()
	IF g_bForceRemotePVCleanup
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
	AND IS_THIS_ROOT_CONTENT_ID_A_ULP_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF

PROC MAINTAIN_REMOTE_MP_SAVED_VEHICLE(TIME_DATATYPE &Timer)
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty <= 0
	AND NOT IS_PLAYER_ENTERING_PROPERTY(PLAYER_ID())
	//AND NOT IS_PED_INJURED(PLAYER_PED_ID()
	AND NOT bIS_GTA_TEAM_RACE()
	AND NOT IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
	AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
	#IF FEATURE_DLC_1_2022
	AND NOT SHOULD_DISABLE_REMOTE_CLEANUP_ON_MISSION()
	#ENDIF
		VEHICLE_INDEX tempVeh
		VEHICLE_INDEX nearbyVehicles[1]
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
		ELSE
			//tempVeh = GET_PLAYERS_LAST_VEHICLE()
			GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehicles)
			tempVeh = nearbyVehicles[0]
		ENDIF
		
		CLEANUP_REMOTE_MP_SAVED_VEHICLE(tempVeh, Timer)
	ENDIF
	

ENDPROC

PROC CLEAR_REMOTE_SAVED_VEHICLE(INT &iStaggerPlayer, TIME_DATATYPE &Timer)

	PLAYER_INDEX PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iStaggerPlayer)

	IF NOT IS_NET_PLAYER_OK(PlayerID, FALSE)	
		IF DOES_ENTITY_EXIST(MPGlobals.RemotePV[iStaggerPlayer])
			//NET_PRINT("CLEAR_REMOTE_SAVED_VEHICLE - remove pv exists for old player ") NET_PRINT_INT(iStaggerPlayer) NET_NL()
			CLEANUP_REMOTE_MP_SAVED_VEHICLE(MPGlobals.RemotePV[iStaggerPlayer], Timer)	
		ENDIF
		
		IF DOES_ENTITY_EXIST(MPGlobals.RemoteTruckV[iStaggerPlayer][0])
			CLEANUP_REMOTE_MP_SAVED_VEHICLE(MPGlobals.RemoteTruckV[iStaggerPlayer][0], Timer)
		ENDIF
		IF DOES_ENTITY_EXIST(MPGlobals.RemoteAvengerV[iStaggerPlayer])
			CLEANUP_REMOTE_MP_SAVED_VEHICLE(MPGlobals.RemoteAvengerV[iStaggerPlayer], Timer)
		ENDIF
		IF DOES_ENTITY_EXIST(MPGlobals.RemoteHackertruckV[iStaggerPlayer])
			CLEANUP_REMOTE_MP_SAVED_VEHICLE(MPGlobals.RemoteHackertruckV[iStaggerPlayer], Timer)
		ENDIF
		#IF FEATURE_HEIST_ISLAND
		IF DOES_ENTITY_EXIST(MPGlobals.RemoteSubmarineV[iStaggerPlayer])
			CLEANUP_REMOTE_MP_SAVED_VEHICLE(MPGlobals.RemoteSubmarineV[iStaggerPlayer], Timer)
		ENDIF
		IF DOES_ENTITY_EXIST(MPGlobals.RemoteSubmarineDinghyV[iStaggerPlayer])
			CLEANUP_REMOTE_MP_SAVED_VEHICLE(MPGlobals.RemoteSubmarineDinghyV[iStaggerPlayer], Timer)
		ENDIF
		#ENDIF
		#IF FEATURE_DLC_2_2022
		IF DOES_ENTITY_EXIST(MPGlobals.RemoteSupportBikeV[iStaggerPlayer])
			CLEANUP_REMOTE_MP_SAVED_VEHICLE(MPGlobals.RemoteSupportBikeV[iStaggerPlayer], Timer)
		ENDIF
		#ENDIF
	ELSE
		IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_PV)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_PV)
				VEHICLE_INDEX tempVeh = NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_PV)
				
				// a remote player has created a new pv nearby
				IF NOT (MPGlobals.RemotePV[iStaggerPlayer] = tempVeh)
					PRINTLN("[personal_vehicle] CLEAR_REMOTE_SAVED_VEHICLE - remote player created new pv, iStaggerPlayer ", iStaggerPlayer)
					
					SET_REMOTE_PV(tempVeh, iStaggerPlayer)									
				ENDIF
								
				// Monitor if the PV has a tow vehicle
				IF IS_MODEL_A_PERSONAL_TRAILER(GET_ENTITY_MODEL(MPGlobals.RemotePV[iStaggerPlayer]))
				
					// Player broadcast their created tow vehicle
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[iStaggerPlayer].netID_PVTowVeh)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[iStaggerPlayer].netID_PVTowVeh)
							VEHICLE_INDEX tempTowVeh = NET_TO_VEH(GlobalplayerBD[iStaggerPlayer].netID_PVTowVeh)
						
							IF NOT (MPGlobals.RemoteTowVehicle[iStaggerPlayer] = tempTowVeh)
								PRINTLN("[personal_vehicle] CLEAR_REMOTE_SAVED_VEHICLE - remote player's PV has a tow truck, iStaggerPlayer ", iStaggerPlayer)
								
								SET_REMOTE_PV(MPGlobals.RemotePV[iStaggerPlayer], iStaggerPlayer)				
							ENDIF						
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF							
	ENDIF		
		
	iStaggerPlayer += 1
	IF (iStaggerPlayer >= NUM_NETWORK_PLAYERS)
		iStaggerPlayer = 0
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_INDEX thePlayer) //does not include in vehicle
	VEHICLE_INDEX theVeh
	PED_INDEX playerPed
	IF IS_NET_PLAYER_OK(thePlayer)
		playerPed = GET_PLAYER_PED(thePlayer)
		IF GET_IS_TASK_ACTIVE(playerPed,CODE_TASK_EXIT_VEHICLE)
			RETURN TRUE
		ENDIF
		IF IS_PED_IN_ANY_VEHICLE(playerPed,TRUE)
			IF NOT IS_PED_IN_ANY_VEHICLE(playerPed,FALSE)
				RETURN TRUE
			ENDIF
		ELSE
			theVeh = GET_VEHICLE_PED_IS_ENTERING(playerPed)
			IF DOES_ENTITY_EXIST(theVeh)
				RETURN TRUE
			ENDIF
		ENDIF
		IF IS_PED_GETTING_INTO_A_VEHICLE(playerPed)
			RETURN TRUE
		ENDIF
		
		IF GET_SCRIPT_TASK_STATUS(playerPed,SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(playerPed,SCRIPT_TASK_ENTER_VEHICLE) = WAITING_TO_START_TASK
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_SIMPLE_INTERIOR_WITH_VEHICLE_STORAGE(INT playerID)
	IF GlobalPlayerBD[playerID].SimpleInteriorBD.eCurrentSimpleInterior != SIMPLE_INTERIOR_INVALID
		SIMPLE_INTERIOR_TYPE eType = GET_SIMPLE_INTERIOR_TYPE(GlobalPlayerBD[playerID].SimpleInteriorBD.eCurrentSimpleInterior)
		
		IF eType != SIMPLE_INTERIOR_TYPE_ARMORY_AIRCRAFT
		AND eType != SIMPLE_INTERIOR_TYPE_WAREHOUSE
		AND eType != SIMPLE_INTERIOR_TYPE_CASINO
		AND eType != SIMPLE_INTERIOR_TYPE_CASINO_NIGHTCLUB
		AND eType != SIMPLE_INTERIOR_TYPE_SOLOMON_OFFICE
		AND eType != SIMPLE_INTERIOR_TYPE_MUSIC_STUDIO
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// check the personal vehicle I am in is actually my personal vehicle. fix for 1860212
PROC MAINTAIN_CHECK_THAT_PV_I_AM_IN_IS_ACTUALLY_MY_CURRENT_PV()
	PLAYER_INDEX localPlayerID = PLAYER_ID()
	
	INT iLocalPlayerInt = NATIVE_TO_INT(localPlayerID)
	
	IF NOT IS_BIT_SET(GlobalplayerBD_FM[iLocalPlayerInt].propertyDetails.iBS, PROPERTY_BROADCAST_BS_IN_A_GARAGE)
	AND IS_PLAYER_CONTROL_ON(localPlayerID)
	AND NOT IS_CUTSCENE_PLAYING()
	AND IS_SCREEN_FADED_IN()
	AND NOT (g_bPlayerInProcessOfExitingInterior)
	AND NOT (MPGlobals.VehicleData.bAssignToMainScript)
	AND NOT (MPGlobals.VehicleData.bAwaitingAssignToMainScript)
	AND NOT IS_PLAYER_IN_CLUBHOUSE_PROPERTY(localPlayerID)
	AND NOT IS_PLAYER_IN_OFFICE_GARAGE_PROPERTY(localPlayerID)
	AND NOT IS_PROPERTY_OFFICE_MOD_GARAGE(localPlayerID, TRUE)
	AND NOT IS_PLAYER_IN_SIMPLE_INTERIOR_WITH_VEHICLE_STORAGE(iLocalPlayerInt)
	 	NETWORK_INDEX niPersonalVehicleID = PERSONAL_VEHICLE_NET_ID()
		
		PED_INDEX localPlayerPedID = PLAYER_PED_ID()
		
		VEHICLE_INDEX VehicleID
		
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPedID)
			VehicleID = GET_VEHICLE_PED_IS_IN(localPlayerPedID)
			
			IF IS_VEHICLE_MY_PERSONAL_VEHICLE(VehicleID)
				IF IS_VEHICLE_DRIVEABLE(VehicleID)
					IF NETWORK_DOES_NETWORK_ID_EXIST(PERSONAL_VEHICLE_NET_ID())
						IF NOT (PERSONAL_VEHICLE_ID() = VehicleID)
							IF NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
							OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobalsAmbience.iPVCleanupTime) > 30000)
								PRINTLN("[personal_vehicle] MAINTAIN_CHECK_THAT_PV_I_AM_IN_IS_ACTUALLY_MY_CURRENT_PV - this is not actually my pv, setting as unusable")
								
								SET_THIS_PV_AS_UNUSABLE(VehicleID)
								
								SCRIPT_ASSERT("[personal_vehicle] MAINTAIN_CHECK_THAT_PV_I_AM_IN_IS_ACTUALLY_MY_CURRENT_PV. Possible car duping exploit. Add bug for Neil F / Conor.")
							ELSE
								PRINTLN("[personal_vehicle] MAINTAIN_CHECK_THAT_PV_I_AM_IN_IS_ACTUALLY_MY_CURRENT_PV - this is not actually my pv, but actual PV is cleaning up. Time = ", GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobalsAmbience.iPVCleanupTime))
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[personal_vehicle] MAINTAIN_CHECK_THAT_PV_I_AM_IN_IS_ACTUALLY_MY_CURRENT_PV - netID_PV doesn't exist, setting as unusable")
						
						SET_THIS_PV_AS_UNUSABLE(VehicleID)
						
						SCRIPT_ASSERT("[personal_vehicle] MAINTAIN_CHECK_THAT_PV_I_AM_IN_IS_ACTUALLY_MY_CURRENT_PV. Possible car duping exploit. Add bug for Neil F / Conor.")
					ENDIF
				ELSE
					PRINTLN("[personal_vehicle] MAINTAIN_CHECK_THAT_PV_I_AM_IN_IS_ACTUALLY_MY_CURRENT_PV - vehicle id is dead.")
				ENDIF
			ENDIF
		ENDIF
		
		// check if the out of garage flag isn't set. 3115660
		IF (CURRENT_SAVED_VEHICLE_SLOT() > -1)
			IF NETWORK_DOES_NETWORK_ID_EXIST(niPersonalVehicleID)
				IF NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS, MP_SAVED_VEHICLE_OUT_GARAGE)
				AND NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_CONCEAL_IN_PROPERTY)
				AND NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
					PRINTLN("[personal_vehicle] MAINTAIN_CHECK_THAT_PV_I_AM_IN_IS_ACTUALLY_MY_CURRENT_PV - doesn't have MP_SAVED_VEHICLE_OUT_GARAGE set, cleanup.")
					
					CLEANUP_MP_SAVED_VEHICLE(FALSE, FALSE, TRUE, FALSE, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC STORE_CURRENT_MP_SAVED_VEHICLE_INDEX_AS_GLOBAL()
	// store my current personal vehicle id as a global.
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV)
		IF NOT (MPGlobalsAmbience.vehPersonalVehicle = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV))			
			MPGlobalsAmbience.vehPersonalVehicle = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV)
			PRINTLN("[personal_vehicle] - STORE_CURRENT_MP_SAVED_VEHICLE_INDEX_AS_GLOBAL - setting MPGlobalsAmbience.vehPersonalVehicle, veh id ", NATIVE_TO_INT(MPGlobalsAmbience.vehPersonalVehicle))
		ENDIF
	ELSE
		IF NOT (MPGlobalsAmbience.vehPersonalVehicle = INT_TO_NATIVE(VEHICLE_INDEX, -1))
			
			#IF IS_DEBUG_BUILD
				IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehPersonalVehicle)
				AND NOT IS_ENTITY_DEAD(MPGlobalsAmbience.vehPersonalVehicle)
					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(MPGlobalsAmbience.vehPersonalVehicle, FALSE)
						ASSERTLN("[personal_vehicle] - STORE_CURRENT_MP_SAVED_VEHICLE_INDEX_AS_GLOBAL - the personal vehicle no longer belongs to the freemode script. This could lead to duping.")	
					ENDIF
				ENDIF
			#ENDIF
		
			PRINTLN("[personal_vehicle] - STORE_CURRENT_MP_SAVED_VEHICLE_INDEX_AS_GLOBAL - personal vehicle no longer exists, or is not in control of freemode script. clearing MPGlobalsAmbience.vehPersonalVehicle")
			MPGlobalsAmbience.vehPersonalVehicle = INT_TO_NATIVE(VEHICLE_INDEX, -1)			
		ENDIF
	ENDIF
	//MPGlobals.RemotePV[NATIVE_TO_INT(PLAYER_ID())] = MPGlobalsAmbience.vehPersonalVehicle
ENDPROC

FUNC BOOL IS_VEHICLE_ON_FIRE_FROM_WEAPON(ENTITY_INDEX EntityID)
	IF IS_ENTITY_ON_FIRE(EntityID)
		//IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(EntityID, WEAPONTYPE_MOLOTOV)
		//OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(EntityID, WEAPONTYPE_PETROLCAN)
			RETURN(TRUE)
		//ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsCelebrationScreenActiveThatRequiresPVCleanup()
	IF (g_bCelebrationScreenIsActive)
	AND NOT (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_GANGHIDEOUT)
	AND NOT ( GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iClubhouseMGID  >= 0)
	AND NOT g_bIgnoreCelebrationScreenPV
		RETURN(TRUE)
	ENDIF
	#IF IS_DEBUG_BUILD
	IF GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iClubhouseMGID >= 0
		PRINTLN("IsCelebrationScreenActiveThatRequiresPVCleanup false GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iClubhouseMGID = ",GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iClubhouseMGID)
	ENDIF
	#ENDIF
	RETURN(FALSE)
ENDFUNC

#IF IS_DEBUG_BUILD
PROC DebugOutputFailedSafeToProcess()
	NET_PRINT("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE: - not safe to process! Details:") NET_NL()
	NET_PRINT("[personal_vehicle] 	IS_THIS_IS_A_STRAND_MISSION() = ") NET_PRINT_BOOL(IS_THIS_IS_A_STRAND_MISSION()) NET_NL()
	NET_PRINT("[personal_vehicle] 	IS_PERSONAL_VEHICLE_CREATION_DISABLED_THIS_FRAME() = ") NET_PRINT_BOOL(IS_PERSONAL_VEHICLE_CREATION_DISABLED_THIS_FRAME()) NET_NL()	
	NET_PRINT("[personal_vehicle] 	IS_PERSONAL_VEHICLE_CREATION_DISABLED_FOR_TRANSITION() = ") NET_PRINT_BOOL(IS_PERSONAL_VEHICLE_CREATION_DISABLED_FOR_TRANSITION()) NET_NL()						
	NET_PRINT("[personal_vehicle] 	ARE_ALL_PERSONAL_VEHICLES_DISABLED_FOR_MISSION() = ") NET_PRINT_BOOL(ARE_ALL_PERSONAL_VEHICLES_DISABLED_FOR_MISSION()) NET_NL()
	NET_PRINT("[personal_vehicle]	IS_CURRENT_PERSONAL_VEHICLE_DISABLED_FOR_MISSION() = ") NET_PRINT_BOOL(IS_CURRENT_PERSONAL_VEHICLE_DISABLED_FOR_MISSION()) NET_NL()
	NET_PRINT("[personal_vehicle] 	ARE_ALL_PERSONAL_VEHICLES_DISABLED_FOR_MISSION_STARTUP() = ") NET_PRINT_BOOL(ARE_ALL_PERSONAL_VEHICLES_DISABLED_FOR_MISSION_STARTUP()) NET_NL()
	NET_PRINT("[personal_vehicle] 	IS_OK_TO_SPAWN_VEHICLE_AT_START_OF_HEIST_MISSION() = ") NET_PRINT_BOOL(IS_OK_TO_SPAWN_VEHICLE_AT_START_OF_HEIST_MISSION()) NET_NL()
	NET_PRINT("[personal_vehicle] 	IS_PLAYER_IN_CORONA() = ") NET_PRINT_BOOL(IS_PLAYER_IN_CORONA()) NET_NL()
	NET_PRINT("[personal_vehicle]	IS_THIS_A_ROUNDS_MISSION_FOR_CORONA() = ") NET_PRINT_BOOL(IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()) NET_NL()
	NET_PRINT("[personal_vehicle]	IS_ON_HEIST_CORONA() = ") NET_PRINT_BOOL(IS_ON_HEIST_CORONA()) NET_NL()
	NET_PRINT("[personal_vehicle] 	Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID()) = ") NET_PRINT_BOOL(Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())) NET_NL()
	NET_PRINT("[personal_vehicle] 	Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID()) = ") NET_PRINT_BOOL(Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID())) NET_NL()
	NET_PRINT("[personal_vehicle] 	Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID()) = ") NET_PRINT_BOOL(Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())) NET_NL()
	NET_PRINT("[personal_vehicle] 	IS_ON_RACE_GLOBAL_SET() = ") NET_PRINT_BOOL(IS_ON_RACE_GLOBAL_SET()) NET_NL()
	NET_PRINT("[personal_vehicle] 	DID_I_JOIN_MISSION_AS_SPECTATOR() = ") NET_PRINT_BOOL(DID_I_JOIN_MISSION_AS_SPECTATOR()) NET_NL()			
	NET_PRINT("[personal_vehicle] 	SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION() = ") NET_PRINT_BOOL(SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION()) NET_NL()			
	NET_PRINT("[personal_vehicle] 	TRANSITION_SESSIONS_PLAYER_VEHICLE_CLEAN_UP_CHECK() = ") NET_PRINT_BOOL(TRANSITION_SESSIONS_PLAYER_VEHICLE_CLEAN_UP_CHECK()) NET_NL()			
	NET_PRINT("[personal_vehicle] 	IS_A_SPECTATOR_CAM_ACTIVE() = ") NET_PRINT_BOOL(IS_A_SPECTATOR_CAM_ACTIVE()) NET_NL()	
	NET_PRINT("[personal_vehicle] 	IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, FALSE)) = ") NET_PRINT_BOOL(IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, FALSE)) NET_NL()			
	NET_PRINT("[personal_vehicle] 	IS_PLAYER_SCTV(PLAYER_ID()) = ") NET_PRINT_BOOL(IS_PLAYER_SCTV(PLAYER_ID())) NET_NL()			
	NET_PRINT("[personal_vehicle] 	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = ") NET_PRINT_INT(ENUM_TO_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType)) NET_NL()				
	NET_PRINT("[personal_vehicle]	IsCelebrationScreenActiveThatRequiresPVCleanup() = ") NET_PRINT_BOOL(IsCelebrationScreenActiveThatRequiresPVCleanup()) NET_NL()
	NET_PRINT("[personal_vehicle] 	(IS_THIS_TRANSITION_SESSION_A_PLAYLIST() AND (IS_SKYSWOOP_IN_SKY() OR IS_SKYSWOOP_MOVING())) = ") NET_PRINT_BOOL((IS_THIS_TRANSITION_SESSION_A_PLAYLIST() AND (IS_SKYSWOOP_IN_SKY() OR IS_SKYSWOOP_MOVING()))) NET_NL()
ENDPROC
#ENDIF

PROC UPDATE_HIDE_PERSONAL_VEHICLES()
	IF (g_SpawnData.MissionSpawnPersonalVehicleData.bHidePersonalVehicles)
	OR (g_b_JobIntroOpeningCut)
	OR IS_PROPERTY_SKYCAM_DOING_EXTERNAL_SHOT()
	OR IS_PROPERTY_SKYCAM()
	
		#IF IS_DEBUG_BUILD
		IF (g_SpawnData.MissionSpawnPersonalVehicleData.bHidePersonalVehicles)
			PRINTLN("[personal_vehicle] UPDATE_HIDE_PERSONAL_VEHICLES - g_SpawnData.MissionSpawnPersonalVehicleData.bHidePersonalVehicles = TRUE")
		ENDIF
		IF (g_b_JobIntroOpeningCut)
			PRINTLN("[personal_vehicle] UPDATE_HIDE_PERSONAL_VEHICLES - g_b_JobIntroOpeningCut = TRUE")
		ENDIF
		IF IS_PROPERTY_SKYCAM_DOING_EXTERNAL_SHOT()
			PRINTLN("[personal_vehicle] UPDATE_HIDE_PERSONAL_VEHICLES - IS_PROPERTY_SKYCAM = TRUE")
		ENDIF
		#ENDIF
		
		INT i
		PLAYER_INDEX PlayerID
		VEHICLE_INDEX VehicleID
		
		REPEAT NUM_NETWORK_PLAYERS i
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[i].netID_PV)
			AND DOES_ENTITY_EXIST(NET_TO_VEH(GlobalplayerBD[i].netID_PV))
				SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_VEH(GlobalplayerBD[i].netID_PV))
				PRINTLN("[personal_vehicle] UPDATE_HIDE_PERSONAL_VEHICLES - hiding locally for player ", i)
			//ELSE
				//PRINTLN("[personal_vehicle] UPDATE_HIDE_PERSONAL_VEHICLES - pv doesn't exist for player ", i)			
			ENDIF
			IF DOES_ENTITY_EXIST(MPGlobals.RemotePV[i])
				SET_ENTITY_LOCALLY_INVISIBLE(MPGlobals.RemotePV[i])
				PRINTLN("[personal_vehicle] UPDATE_HIDE_PERSONAL_VEHICLES - hiding RemotePV locally for player ", i)
			ENDIF
			
			// if the player is invisible then make sure his vehicle is too.
			IF IS_PROPERTY_SKYCAM_DOING_EXTERNAL_SHOT()
			OR IS_PROPERTY_SKYCAM()
				PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)		
				IF NOT (PlayerID = PLAYER_ID())
					IF IS_NET_PLAYER_OK(PlayerID, FALSE)
						IF DOES_ENTITY_EXIST(GET_PLAYER_PED(PlayerID))
							IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(PlayerID))
								IF NOT IS_ENTITY_VISIBLE(GET_PLAYER_PED(PlayerID))
									IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(PlayerID))
										VehicleID = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PlayerID))
										IF DOES_ENTITY_EXIST(VehicleID)
											SET_ENTITY_LOCALLY_INVISIBLE(VehicleID)	
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
			
		ENDREPEAT	
	ENDIF
	
//	IF IS_PROPERTY_SKYCAM()
//		PRINTLN("[personal_vehicle] spam UPDATE_HIDE_PERSONAL_VEHICLES - IS_PROPERTY_SKYCAM = TRUE")
//	ELSE
//		PRINTLN("[personal_vehicle] spam UPDATE_HIDE_PERSONAL_VEHICLES - IS_PROPERTY_SKYCAM = FALSE")
//	ENDIF
	
ENDPROC

#IF FEATURE_TUNER
PROC SET_BD_OWNER_OF_TEST_DRIVE_PV(PLAYER_INDEX pPlayer)
	IF pPlayer != INVALID_PLAYER_INDEX()
		PRINTLN("SET_BD_OWNER_OF_TEST_DRIVE_PV - Test driving PV owned by: ", NATIVE_TO_INT(pPlayer))
		
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iOwnerOfTestDrivePV = NATIVE_TO_INT(pPlayer)
	ENDIF
ENDPROC

PROC CLEAR_BD_OWNER_OF_TEST_DRIVE_PV()
	PRINTLN("CLEAR_BD_OWNER_OF_TEST_DRIVE_PV - Clearing owner of test drive PV")
	
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iOwnerOfTestDrivePV = -1
ENDPROC

PROC SET_CONCEAL_TEST_DRIVE_PV(BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSFour, PROPERTY_BROADCAST_BS4_CONCEAL_TEST_DRIVE_PV)
			PRINTLN("SET_CONCEAL_TEST_DRIVE_PV - Set")
			
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSFour, PROPERTY_BROADCAST_BS4_CONCEAL_TEST_DRIVE_PV)
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSFour, PROPERTY_BROADCAST_BS4_CONCEAL_TEST_DRIVE_PV)
			PRINTLN("SET_CONCEAL_TEST_DRIVE_PV - Clear")
			
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSFour, PROPERTY_BROADCAST_BS4_CONCEAL_TEST_DRIVE_PV)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CONCEAL_PLAYERS_TEST_DRIVE_PV(PLAYER_INDEX pPlayer)
	IF pPlayer != INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(pPlayer)
		RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(pPlayer)].propertyDetails.iBSFour, PROPERTY_BROADCAST_BS4_CONCEAL_TEST_DRIVE_PV)
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED(INT iPlayerToCheck, VEHICLE_INDEX theVeh,BOOL bPrintReason = TRUE)
	
	IF bPrintReason = TRUE
		PRINTLN("SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED GlobalplayerBD_FM[",iPlayerToCheck,"].propertyDetails.iPVConcealInID = ",GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInID)
		PRINTLN("SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED GlobalplayerBD_FM[",iPlayerToCheck,"].propertyDetails.iPVConcealInType = ",GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInType)
		PRINTLN("SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED GlobalplayerBD_FM[",iPlayerToCheck,"].propertyDetails.iPVConcealInPlayerOwnedLocation = ",GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInPlayerOwnedLocation)
		PRINTLN("SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED IS_BIT_SET(GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iBSTHree,PROPERTY_BROADCAST_BS3_FORCE_CONCEAL_CURRENT_PV) = ",IS_BIT_SET(GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iBSTHree,PROPERTY_BROADCAST_BS3_FORCE_CONCEAL_CURRENT_PV))
		PRINTLN("SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED IS_BIT_SET(concealPersVehEventData.iPlayerVehToConcealBS,iPlayerToCheck) = ",IS_BIT_SET(concealPersVehEventData.iPlayerVehToConcealBS,iPlayerToCheck))
		PRINTLN("SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Local player InID = ",GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior))
		PRINTLN("SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Local player g_ownerOfBunkerPropertyIAmIn = ", NATIVE_TO_INT(g_ownerOfBunkerPropertyIAmIn))
		PRINTLN("SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Local player g_ownerOfArmoryTruckPropertyIAmIn = ", NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn))
		PRINTLN("SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Is player in vehicle = ", IS_PED_IN_VEHICLE(PLAYER_PED_ID(),theVeh,TRUE))
		
	ENDIF
	
	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),theVeh,TRUE)
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_TUNER
	IF SHOULD_CONCEAL_PLAYERS_TEST_DRIVE_PV(INT_TO_PLAYERINDEX(iPlayerToCheck))
		IF bPrintReason = TRUE
			PRINTLN("SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED - SHOULD_CONCEAL_PLAYERS_TEST_DRIVE_PV - ", iPlayerToCheck)
		ENDIF
		
		RETURN TRUE
	ENDIF
	#ENDIF
	
//	//force conceal
	IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iBSTHree,PROPERTY_BROADCAST_BS3_FORCE_CONCEAL_CURRENT_PV)
	OR IS_BIT_SET(concealPersVehEventData.iPlayerVehToConcealBS,iPlayerToCheck)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),theVeh,TRUE)
			IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iBSTHree,PROPERTY_BROADCAST_BS3_FORCE_CONCEAL_CURRENT_PV)
				PRINTLN("SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED PROPERTY_BROADCAST_BS3_FORCE_CONCEAL_CURRENT_PV true")
			ENDIF
			IF IS_BIT_SET(concealPersVehEventData.iPlayerVehToConcealBS,iPlayerToCheck) 
				PRINTLN("SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED concealPersVehEventData.iPlayerVehToConcealBS true")
			ENDIF		
			IF bPrintReason = TRUE
				PRINTLN("SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Vehicle force concealed ")
			ENDIF
			RETURN TRUE
		ELSE
			IF bPrintReason = TRUE
				PRINTLN("SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Vehicle force concealed but player is inside so not concealing")
			ENDIF
		ENDIF
	ENDIF
	
	#IF FEATURE_TUNER
	IF GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInType = PV_CONCEAL_TYPE_CAR_MEET
		IF ENUM_TO_INT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior) != GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInID
			IF bPrintReason
				PRINTLN("A - car meet SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Concealing vehicle for player # ", iPlayerToCheck)
			ENDIF
			
			RETURN TRUE
		ELIF IS_LOCAL_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX()
			IF bPrintReason
				PRINTLN("B - car meet SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Concealing vehicle for player # ", iPlayerToCheck)
			ENDIF
			
			RETURN TRUE
		ENDIF
	ELIF GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInType = PV_CONCEAL_TYPE_PRIVATE_CAR_MEET
		IF ENUM_TO_INT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior) != GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInID
			IF bPrintReason
				PRINTLN("A - car meet SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Concealing vehicle for player # ", iPlayerToCheck)
			ENDIF
			
			RETURN TRUE
		ELIF GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInPlayerOwnedLocation != -1
		AND GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()) != INT_TO_PLAYERINDEX(GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInPlayerOwnedLocation)
			IF bPrintReason
				PRINTLN("C - car meet SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Concealing vehicle for player # ", iPlayerToCheck)
			ENDIF
			
			RETURN TRUE
		ELIF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance != globalPlayerBD[iPlayerToCheck].SimpleInteriorBD.iInstance
		AND globalPlayerBD[iPlayerToCheck].SimpleInteriorBD.iInstance != -1
		AND IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
		AND IS_PLAYER_IN_CAR_MEET_PROPERTY(INT_TO_PLAYERINDEX(iPlayerToCheck))
			IF bPrintReason
				PRINTLN("B - car meet SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Concealing vehicle for player # ", iPlayerToCheck)
			ENDIF
			
			RETURN TRUE
		ENDIF
	ELIF GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInType = PV_CONCEAL_TYPE_AUTO_SHOP
		VEHICLE_INDEX vehToCheck = NET_TO_VEH(GlobalplayerBD[iPlayerToCheck].netID_PV)
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF tempVeh != vehToCheck
				IF bPrintReason
					PRINTLN("A - auto shop SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Concealing vehicle for player # ", iPlayerToCheck)
				ENDIF
				
				RETURN TRUE
			ENDIF
		ELSE
			IF bPrintReason
				PRINTLN("B - auto shop SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Concealing vehicle for player # ", iPlayerToCheck)
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	#ENDIF
	
	IF GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInType = PV_CONCEAL_TYPE_CLUBHOUSE
		//conceal if in different property 
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty > 0
		AND GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInID > 0
		AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty != GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInID
			RETURN TRUE
		ENDIF
		
		//conceal if same property but different owners
		IF INT_TO_PLAYERINDEX(GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInPlayerOwnedLocation) != INVALID_PLAYER_INDEX()
		AND IS_GAMER_HANDLE_VALID(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.requestedGamerForInstance)
		AND NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.requestedGamerForInstance) != INT_TO_PLAYERINDEX(GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInPlayerOwnedLocation)
			RETURN TRUE
		ENDIF
		
		//NOTE: Don't think this is needed but leaving in existing functionality in case there is a reason
		//conceal if local player is in different instance than their concealed vehicle
		IF (NATIVE_TO_INT(PLAYER_ID()) = iPlayerToCheck AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty > 0
		AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty != GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInID)
			RETURN TRUE
		ENDIF
	ENDIF
	IF GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInType = PV_CONCEAL_TYPE_BUNKER
		IF PLAYER_ID() = INT_TO_PLAYERINDEX(iPlayerToCheck)
		AND DONT_CONCEAL_MY_PV_THIS_FRAME()
			PRINTLN("D NOT - bunker SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Don't Conceal vehicle for player # ",iPlayerToCheck)
			RETURN FALSE
		ENDIF
		
		//conceal if in different property 
		IF ENUM_TO_INT(GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior)) != GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInID
			IF IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK() AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND PLAYER_ID() = INT_TO_PLAYERINDEX(iPlayerToCheck) 	// Need this for truck cutscene from bunker to truck
				// skip
			ELSE
				IF NATIVE_TO_INT(PLAYER_ID()) = iPlayerToCheck
				AND IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID())
				AND (IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
				OR IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK())
	//				IF bPrintReason
	//					PRINTLN("AB - bunker NOT SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED player is entering truck ",iPlayerToCheck)
	//				ENDIF
				ELSE
					IF bPrintReason
						PRINTLN("A - bunker SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
					ENDIF
					RETURN TRUE
				ENDIF
			ENDIF	
		//conceal if moving to truck
		ELIF (IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK() AND iPlayerToCheck != NATIVE_TO_INT(PLAYER_ID()) AND IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER))
			IF bPrintReason
				PRINTLN("B - bunker SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
			ENDIF
			RETURN TRUE
		//conceal if same property/instance but different owners i.e. instance re-used
		ELIF GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInPlayerOwnedLocation != -1
		AND g_ownerOfBunkerPropertyIAmIn != INT_TO_PLAYERINDEX(GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInPlayerOwnedLocation)
			IF bPrintReason
				PRINTLN("C - bunker SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	IF GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInType = PV_CONCEAL_TYPE_ARMORY_TRUCK
		IF PLAYER_ID() = INT_TO_PLAYERINDEX(iPlayerToCheck)
		AND DONT_CONCEAL_MY_PV_THIS_FRAME()
			PRINTLN("D NOT - truck SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Don't Conceal vehicle for player # ",iPlayerToCheck)
			RETURN FALSE
		ENDIF
		
		IF ENUM_TO_INT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior) != GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInID
			IF IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK() AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND PLAYER_ID() = INT_TO_PLAYERINDEX(iPlayerToCheck)			// Need this for truck cutscene from bunker to truck
				// skip
			ELSE	
				IF bPrintReason
					PRINTLN("A - truck SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
				ENDIF	
				RETURN TRUE
			ENDIF	
		ELIF GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInPlayerOwnedLocation != -1
		AND g_ownerOfArmoryTruckPropertyIAmIn != INT_TO_PLAYERINDEX(GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInPlayerOwnedLocation)
			IF bPrintReason
				PRINTLN("C - truck SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
			ENDIF
			RETURN TRUE
		ELIF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance != globalPlayerBD[iPlayerToCheck].SimpleInteriorBD.iInstance
		AND globalPlayerBD[iPlayerToCheck].SimpleInteriorBD.iInstance != -1
		AND IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
		AND IS_PLAYER_IN_ARMORY_TRUCK(INT_TO_PLAYERINDEX(iPlayerToCheck))
			IF bPrintReason
				PRINTLN("B - truck SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
			ENDIF	
			RETURN TRUE
		ENDIF
	ENDIF
	IF GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInType = PV_CONCEAL_TYPE_ARMORY_AIRCRAFT
		IF ENUM_TO_INT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior) != GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInID
			IF IS_PLAYER_MOVING_FROM_BASE_TO_AIRCRAFT() AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND PLAYER_ID() = INT_TO_PLAYERINDEX(iPlayerToCheck)				// Need this for truck cutscene from base to aircraft
				// skip
			ELSE
				IF bPrintReason
					PRINTLN("D - aircraft SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
				ENDIF	
				RETURN TRUE
			ENDIF	
		ELIF ENUM_TO_INT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior) != GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInID
			IF bPrintReason
				PRINTLN("A - aircraft SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
			ENDIF	
			RETURN TRUE
		ELIF GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInPlayerOwnedLocation != -1
		AND g_ownerOfArmoryAircraftPropertyIAmIn != INT_TO_PLAYERINDEX(GlobalplayerBD_FM[iPlayerToCheck].propertyDetails.iPVConcealInPlayerOwnedLocation)
			IF bPrintReason
				PRINTLN("C - aircraft SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
			ENDIF
			RETURN TRUE
		ELIF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance != globalPlayerBD[iPlayerToCheck].SimpleInteriorBD.iInstance
		AND globalPlayerBD[iPlayerToCheck].SimpleInteriorBD.iInstance != -1
		AND IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
		AND IS_PLAYER_IN_ARMORY_AIRCRAFT(INT_TO_PLAYERINDEX(iPlayerToCheck))
			IF bPrintReason
				PRINTLN("B - aircraft SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED Concealing vehicle for player # ",iPlayerToCheck)
			ENDIF	
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

#IF FEATURE_TUNER
PROC SET_CONCEAL_CURRENT_PERSONAL_VEHICLE_IN_AUTO_SHOP()
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInPlayerOwnedLocation = -1
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInID = -1
	
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)
	
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInType = PV_CONCEAL_TYPE_AUTO_SHOP
	
	SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CONCEAL_IN_PROPERTY)
	
	PRINTLN("SET_CONCEAL_CURRENT_PERSONAL_VEHICLE_IN_AUTO_SHOP: setting current personal vehicle to be concealed in auto shop")
ENDPROC

PROC CLEAR_CONCEAL_CURRENT_PERSONAL_VEHICLE_IN_AUTO_SHOP()
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInType = PV_CONCEAL_TYPE_AUTO_SHOP
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInType = -1
		
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInPlayerOwnedLocation = -1
		
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)
		
		FORCE_CONCEAL_MY_ACTIVE_PERSONAL_VEHICLE(FALSE)
		SET_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(FALSE)
		
		IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_CONCEAL_IN_PROPERTY)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInID = -1
			
			CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CONCEAL_IN_PROPERTY)
			
			PRINTLN("CLEAR_CONCEAL_CURRENT_PERSONAL_VEHICLE_IN_AUTO_SHOP: clearing conceal of personal vehicle")
		ENDIF
	ENDIF
ENDPROC
#ENDIF

PROC SET_FORCE_CONCEAL_PERSONAL_VEHICLE_EVENT_FLAG(PLAYER_INDEX thePlayer,BOOL bForceConceal)
	IF bForceConceal
		SET_BIT(concealPersVehEventData.iPlayerVehToConcealBS, NATIVE_TO_INT(thePlayer))
	ELSE
		CLEAR_BIT(concealPersVehEventData.iPlayerVehToConcealBS, NATIVE_TO_INT(thePlayer))
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
	
	#IF IS_DEBUG_BUILD
	IF IS_NET_PLAYER_OK(thePlayer, FALSE)
		PRINTLN("SET_FORCE_CONCEAL_PERSONAL_VEHICLE_EVENT_FLAG-: Setting conceal to: ", bForceConceal," for player: ", GET_PLAYER_NAME(thePlayer))
	ELSE
		PRINTLN("SET_FORCE_CONCEAL_PERSONAL_VEHICLE_EVENT_FLAG-: Setting conceal to: ", bForceConceal," for player: ", NATIVE_TO_INT(thePlayer))
	ENDIF
	#ENDIF
ENDPROC

PROC MAINTAIN_FORCE_CONCEAL_EVENT_FLAG()
	IF IS_BIT_SET(concealPersVehEventData.iPlayerVehToConcealBS, concealPersVehEventData.iSlowLoopCounter)
		IF IS_BIT_SET(GlobalplayerBD_FM[concealPersVehEventData.iSlowLoopCounter].propertyDetails.iBSTHree, PROPERTY_BROADCAST_BS3_FORCE_CONCEAL_CURRENT_PV)
			SET_FORCE_CONCEAL_PERSONAL_VEHICLE_EVENT_FLAG(INT_TO_PLAYERINDEX(concealPersVehEventData.iSlowLoopCounter), FALSE)
		ENDIF
	ENDIF
	
	concealPersVehEventData.iSlowLoopCounter++
	
	IF concealPersVehEventData.iSlowLoopCounter >= NUM_NETWORK_PLAYERS
		concealPersVehEventData.iSlowLoopCounter = 0
	ENDIF
ENDPROC

FUNC BOOL IS_NEARBY_VEHICLE_SUITABLE_TO_BE_CONCEALED(VEHICLE_INDEX theVeh, PLAYER_INDEX thePlayer)
	INT iInstance
	INT iDecoratorValue
	IF DOES_ENTITY_EXIST(theVeh)
	AND IS_ENTITY_A_VEHICLE(theVeh)
		IF IS_VEHICLE_A_PERSONAL_VEHICLE(theVeh)
		AND GET_OWNER_OF_PERSONAL_VEHICLE(theVeh) = thePlayer
			IF DECOR_IS_REGISTERED_AS_TYPE("PV_Slot", DECOR_TYPE_INT)
			AND DECOR_EXIST_ON(theVeh, "PV_Slot")
			AND DECOR_GET_INT(theVeh, "PV_Slot") >= 0
				PRINTLN("IS_NEARBY_VEHICLE_SUITABLE_TO_BE_CONCEALED: found a nearby vehicle: Model = ",GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(theVeh)), " and is a player's personal vehicle")
				STRING tempString = GET_ENTITY_SCRIPT(theVeh,iInstance)
				IF NOT IS_STRING_NULL_OR_EMPTY(tempString)
				AND ARE_STRINGS_EQUAL(GET_ENTITY_SCRIPT(theVeh,iInstance),FREEMODE_SCRIPT())
					PRINTLN("IS_NEARBY_VEHICLE_SUITABLE_TO_BE_CONCEALED: found a nearby vehicle and belongs to freemode")
					IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
					AND DECOR_EXIST_ON(theVeh, "MPBitset")
						iDecoratorValue = DECOR_GET_INT(theVeh,"MPBitset")
						PRINTLN("IS_NEARBY_VEHICLE_SUITABLE_TO_BE_CONCEALED: found a nearby vehicle and has bitset")
						IF IS_BIT_SET(iDecoratorValue, MP_DECORATOR_BS_HAD_WEAPONS_APPLIED)
							PRINTLN("IS_NEARBY_VEHICLE_SUITABLE_TO_BE_CONCEALED: found a nearby vehicle and has waepons")
							IF IS_ENTITY_IN_AREA(theVeh, <<1060.325, -3064.239, -100.9374>> , <<821.320, -3255.345, -75.0000>> , FALSE, FALSE)
							OR IS_ENTITY_IN_AREA(theVeh, <<1097.5347, -3016.0115, -40.7658>>, <<1109.2977, -2983.6902, -34.1882>>, FALSE, FALSE)
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("IS_NEARBY_VEHICLE_SUITABLE_TO_BE_CONCEALED: found a nearby vehicle: Model = ",GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(theVeh)), " is a personal vehicle with no slot!")
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("IS_NEARBY_VEHICLE_SUITABLE_TO_BE_CONCEALED: found a nearby vehicle: Model = ",GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(theVeh)), " But not this player's personal vehicle")
		#ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_CONCEALABLE_SIMPLE_INTERIOR(INT playerID)
	IF GlobalPlayerBD[playerID].SimpleInteriorBD.eCurrentSimpleInterior != SIMPLE_INTERIOR_INVALID
		SIMPLE_INTERIOR_TYPE eType = GET_SIMPLE_INTERIOR_TYPE(GlobalPlayerBD[playerID].SimpleInteriorBD.eCurrentSimpleInterior)
		
		IF eType = SIMPLE_INTERIOR_TYPE_BUNKER
		OR eType = SIMPLE_INTERIOR_TYPE_ARMORY_TRUCK
		OR eType = SIMPLE_INTERIOR_TYPE_CAR_MEET
		OR eType = SIMPLE_INTERIOR_TYPE_AUTO_SHOP
		OR eType = SIMPLE_INTERIOR_TYPE_ARMORY_AIRCRAFT
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_CONCEALING_PERSONAL_VEHICLES()
	PLAYER_INDEX localPlayerID = PLAYER_ID()
	
	INT localPlayerInt = NATIVE_TO_INT(localPlayerID)
	
	PED_INDEX localPlayerPedID = PLAYER_PED_ID()
	
	MAINTAIN_FORCE_CONCEAL_EVENT_FLAG()
	
	BOOL bKeepConcealed, bNeedsConcealCheck
	
	BOOL bIsPlayerEnteringExitingProperty = IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(localPlayerID)
	BOOL bIsPlayerWalkingInOutSimpleInterior = IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(localPlayerID)
	
	INT i, iRemoveVehCounter = 0
	
	IF bIsPlayerEnteringExitingProperty
	OR IS_PLAYER_IN_PROPERTY(localPlayerID, TRUE)
	OR IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK()
	OR IS_PLAYER_MOVING_FROM_BASE_TO_AIRCRAFT()
	OR IS_PLAYER_IN_CONCEALABLE_SIMPLE_INTERIOR(localPlayerInt)
		bNeedsConcealCheck = TRUE
	ENDIF
	
	IF IS_BIT_SET(mpPropMaintain.remoteStoredPersVeh.iLocalFlagsBS, REMOTE_STORED_VEHICLE_BS_RETURNED_TO_GARAGE)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(localPlayerID)
		AND IS_PLAYER_CONTROL_ON(localPlayerID)
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		AND !g_bBrowserVisible
		AND IS_SCREEN_FADED_IN()
			PRINT_HELP("BKR_PV_RTRN")
			
			CLEAR_BIT(mpPropMaintain.remoteStoredPersVeh.iLocalFlagsBS, REMOTE_STORED_VEHICLE_BS_RETURNED_TO_GARAGE)
		ENDIF
	ELIF IS_BIT_SET(mpPropMaintain.remoteStoredPersVeh.iLocalFlagsBS, REMOTE_STORED_VEHICLE_BS_RETURNED_TO_BUNKER)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(localPlayerID)
		AND IS_PLAYER_CONTROL_ON(localPlayerID)
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		AND !g_bBrowserVisible
		AND IS_SCREEN_FADED_IN()
			PRINT_HELP("GRU_PV_RTRN")
			
			CLEAR_BIT(mpPropMaintain.remoteStoredPersVeh.iLocalFlagsBS, REMOTE_STORED_VEHICLE_BS_RETURNED_TO_BUNKER)
		ENDIF
	ELIF IS_BIT_SET(mpPropMaintain.remoteStoredPersVeh.iLocalFlagsBS, REMOTE_STORED_VEHICLE_BS_RETURNED_TO_HANGAR)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(localPlayerID)
		AND IS_PLAYER_CONTROL_ON(localPlayerID)
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		AND !g_bBrowserVisible
		AND IS_SCREEN_FADED_IN()
			PRINT_HELP("SMU_PV_RTRN")
			
			CLEAR_BIT(mpPropMaintain.remoteStoredPersVeh.iLocalFlagsBS, REMOTE_STORED_VEHICLE_BS_RETURNED_TO_HANGAR)
		ENDIF
	ENDIF
	
	IF GlobalplayerBD_FM[localPlayerInt].propertyDetails.iPVConcealInPlayerOwnedLocation != -1
		IF NOT bIsPlayerEnteringExitingProperty
		AND NOT bIsPlayerWalkingInOutSimpleInterior
		AND NOT HAS_EXIT_TO_BUNKER_FROM_TRUCK_TRIGGERED()
		AND NOT HAS_EXIT_TO_BASE_FROM_AIRCRAFT_TRIGGERED() 
			IF NOT IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(GlobalplayerBD_FM[localPlayerInt].propertyDetails.iPVConcealInPlayerOwnedLocation), FALSE)
				CLEANUP_MP_SAVED_VEHICLE(TRUE, FALSE, TRUE)
				
				CLEAR_REQUEST_GUEST_PARKING_FROM_PLAYER()
				
				CLEAR_CONCEAL_CURRENT_PERSONAL_VEHICLE()
				
				IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
					IF IS_THIS_MODEL_ALLOWED_IN_HANGAR(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel)
						PRINTLN("HANDLE_CONCEALING_PERSONAL_VEHICLES: Cleaning up requested parking to hangar, player no longer valid")
						
						SET_BIT(mpPropMaintain.remoteStoredPersVeh.iLocalFlagsBS, REMOTE_STORED_VEHICLE_BS_RETURNED_TO_HANGAR)
					ELSE
						PRINTLN("HANDLE_CONCEALING_PERSONAL_VEHICLES: Cleaning up requested parking to garage, player no longer valid")
						
						SET_BIT(mpPropMaintain.remoteStoredPersVeh.iLocalFlagsBS, REMOTE_STORED_VEHICLE_BS_RETURNED_TO_GARAGE)
					ENDIF
				ELSE
					ASSERTLN("HANDLE_CONCEALING_PERSONAL_VEHICLES: Cleaning up requested parking to garage, player no longer valid")
					
					SET_BIT(mpPropMaintain.remoteStoredPersVeh.iLocalFlagsBS, REMOTE_STORED_VEHICLE_BS_RETURNED_TO_GARAGE)
				ENDIF
			ELIF GlobalplayerBD_FM[localPlayerInt].propertyDetails.iPVConcealInID != -1
				IF GlobalplayerBD_FM[localPlayerInt].propertyDetails.iPVConcealInType = PV_CONCEAL_TYPE_CLUBHOUSE
					REPEAT MAX_OWNED_PROPERTIES i
						IF GlobalplayerBD_FM[GlobalplayerBD_FM[localPlayerInt].propertyDetails.iPVConcealInPlayerOwnedLocation].propertyDetails.iOwnedProperty[i] != GlobalplayerBD_FM[localPlayerInt].propertyDetails.iPVConcealInID
							iRemoveVehCounter++
						ENDIF
					ENDREPEAT
					
					IF iRemoveVehCounter = MAX_OWNED_PROPERTIES
					OR NOT GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(localPlayerID, TRUE)
						IF IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(localPlayerID)
							SET_PLAYER_PERSONAL_CAR_MOD_INTRO_BAIL_NOW(TRUE)
							
							PRINTLN("HANDLE_CONCEALING_PERSONAL_VEHICLES: player is using custom mod shop")
						ELSE
							IF !IS_PED_IN_ANY_VEHICLE(localPlayerPedID)
							AND GET_SCRIPT_TASK_STATUS(localPlayerPedID, SCRIPT_TASK_LEAVE_VEHICLE) <> PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(localPlayerPedID, SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> PERFORMING_TASK
								IF bNeedsConcealCheck
									CLEANUP_MP_SAVED_VEHICLE(FALSE, FALSE, TRUE, FALSE, TRUE)
								ELSE
									CLEANUP_MP_SAVED_VEHICLE(TRUE, FALSE, TRUE)
								ENDIF
								
								CLEAR_REQUEST_GUEST_PARKING_FROM_PLAYER()
								
								IF iRemoveVehCounter = MAX_OWNED_PROPERTIES
									PRINTLN("HANDLE_CONCEALING_PERSONAL_VEHICLES: Cleaning up requested parking player no longer owns given property")
								ELSE
									PRINTLN("HANDLE_CONCEALING_PERSONAL_VEHICLES: Cleaning up requested parking player no longer in gang")
								ENDIF
								
								SET_BIT(mpPropMaintain.remoteStoredPersVeh.iLocalFlagsBS, REMOTE_STORED_VEHICLE_BS_RETURNED_TO_GARAGE)
							ELSE
								TASK_LEAVE_ANY_VEHICLE(localPlayerPedID)
							ENDIF
						ENDIF
					ENDIF
				ELIF GlobalplayerBD_FM[localPlayerInt].propertyDetails.iPVConcealInType = PV_CONCEAL_TYPE_BUNKER
					IF GlobalplayerBD_FM[localPlayerInt].propertyDetails.iPVConcealInID != ENUM_TO_INT(GlobalplayerBD_FM[localPlayerInt].propertyDetails.bdFactoryData[BUNKER_SAVE_SLOT ].eFactoryID)
						CLEANUP_MP_SAVED_VEHICLE(TRUE, FALSE, TRUE)
						
						CLEAR_REQUEST_GUEST_PARKING_FROM_PLAYER()
						
						CLEAR_CONCEAL_CURRENT_PERSONAL_VEHICLE()
						
						PRINTLN("HANDLE_CONCEALING_PERSONAL_VEHICLES: Cleaning up requested parking no longer owns bunker - tell player it's being returned to a garage as CLEANUP_MP_SAVED_VEHICLE puts it there.")
						
						SET_BIT(mpPropMaintain.remoteStoredPersVeh.iLocalFlagsBS, REMOTE_STORED_VEHICLE_BS_RETURNED_TO_BUNKER)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	VEHICLE_INDEX theVeh
	VEHICLE_INDEX vehicleArray[15]
	
	BOOL bShouldConceal = FALSE
	
	IF bIsPlayerWalkingInOutSimpleInterior
	OR HAS_PLAYER_ACCEPTED_SIMPLE_INTERIOR_INVITE()
	OR IS_PLAYER_IN_CONCEALABLE_SIMPLE_INTERIOR(localPlayerInt)
	OR IS_PLAYER_IN_HACKER_TRUCK(localPlayerID)
		bShouldConceal = TRUE
	ENDIF
	
	INT iVeh
	REPEAT NUM_NETWORK_PLAYERS i
		PLAYER_INDEX tempPlayer = INT_TO_PLAYERINDEX(i)
		
		bKeepConcealed = FALSE
		
		IF (IS_BIT_SET(GlobalplayerBD_FM[i].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)
		OR IS_BIT_SET(GlobalplayerBD_FM[i].propertyDetails.iBSTHree, PROPERTY_BROADCAST_BS3_FORCE_CONCEAL_CURRENT_PV)
		OR IS_BIT_SET(concealPersVehEventData.iPlayerVehToConcealBS, i))
		AND bNeedsConcealCheck
			theVeh = NULL
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[i].netID_PV)
				theVeh = NET_TO_VEH(GlobalplayerBD[i].netID_PV)
			ELIF g_bAllowNearbyPVConceal
			AND bShouldConceal
				INT iNearbyVehs = GET_PED_NEARBY_VEHICLES(localPlayerPedID, vehicleArray)
				
				iVeh = 0
				
				REPEAT iNearbyVehs iVeh
					IF IS_NEARBY_VEHICLE_SUITABLE_TO_BE_CONCEALED(vehicleArray[iVeh], tempPlayer)
						theVeh = vehicleArray[iVeh]
						
						iVeh = iNearbyVehs
						
						PRINTLN("HANDLE_CONCEALING_PERSONAL_VEHICLES: grabbed remote player vehicle from nearby vehicles")
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF DOES_ENTITY_EXIST(theVeh)
			AND IS_VEHICLE_DRIVEABLE(theVeh)
				IF SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED(i, theVeh)
					bKeepConcealed = TRUE
				ENDIF
				
				IF IS_BIT_SET(GlobalplayerBD_FM[i].propertyDetails.iBSThree, PROPERTY_BROADCAST_BS3_GUEST_PV_CONCEAL_ON_ENTERING)
				AND localPlayerInt != i 
				AND NETWORK_IS_PLAYER_CONCEALED(tempPlayer)
					SET_ENTITY_LOCALLY_INVISIBLE(theVeh)
					
//					#IF IS_DEBUG_BUILD
//					IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i),FALSE)
//						PRINTLN("[personal_vehicle] HANDLE_CONCEALING_PERSONAL_VEHICLES - hiding this frame player ID ", i," name: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(i)), " personal vehicle")
//					ELSE
						PRINTLN("[personal_vehicle] HANDLE_CONCEALING_PERSONAL_VEHICLES - concealing player ", i, " personal vehicle")
//					ENDIF
//					#ENDIF
				ENDIF
				
				IF bKeepConcealed
					IF NOT NETWORK_IS_ENTITY_CONCEALED(theVeh)
//						#IF IS_DEBUG_BUILD
//						SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED(i,theVeh, TRUE)
//						#ENDIF
						
						NETWORK_CONCEAL_ENTITY(theVeh, TRUE)
						
						IF NOT IS_BIT_SET(mpPropMaintain.remoteStoredPersVeh.iConcealedRemotePV_BS, i)
							SET_BIT(mpPropMaintain.remoteStoredPersVeh.iConcealedRemotePV_BS, i)
							
//							#IF IS_DEBUG_BUILD
//							IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i),FALSE)
//								PRINTLN("[personal_vehicle] HANDLE_CONCEALING_PERSONAL_VEHICLES - concealing player ID ", i," name: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(i)), " personal vehicle")
//							ELSE
								PRINTLN("[personal_vehicle] HANDLE_CONCEALING_PERSONAL_VEHICLES - concealing player ", i, " personal vehicle")
//							ENDIF
//							#ENDIF
						ENDIF
					ENDIF
				ENDIF
//			#IF IS_DEBUG_BUILD
//			ELSE
//				IF (IS_BIT_SET(GlobalplayerBD_FM[i].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)
//				OR IS_BIT_SET(GlobalplayerBD_FM[i].propertyDetails.iBSTHree,PROPERTY_BROADCAST_BS3_FORCE_CONCEAL_CURRENT_PV)
//				OR IS_BIT_SET(concealPersVehEventData.iPlayerVehToConcealBS,i))
//					PRINTLN("[personal_vehicle] HANDLE_CONCEALING_PERSONAL_VEHICLES - vehicle should be concealed but does not exist")
//					
//					IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i),FALSE)
//						PRINTLN("[personal_vehicle] HANDLE_CONCEALING_PERSONAL_VEHICLES - vehicle should be concealed but does not exist player ID ", i," name: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(i)), " personal vehicle")
//					ELSE
//						PRINTLN("[personal_vehicle] HANDLE_CONCEALING_PERSONAL_VEHICLES - vehicle should be concealed but does not exist ", i, " personal vehicle")
//					ENDIF
//				ENDIF
//			#ENDIF
			ENDIF
		ENDIF
		
		IF NOT bKeepConcealed
			VEHICLE_INDEX vehToCheck
			
			IF IS_PLAYER_IN_ARMORY_TRUCK(localPlayerID)
			AND NOT g_bUseNetIndexForConceal
				vehToCheck = MPGlobals.RemotePV[i]
			ELSE
				IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[i].netID_PV)
					vehToCheck = NET_TO_VEH(GlobalplayerBD[i].netID_PV)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehToCheck)
				BOOL bDontConcealMyPV = DONT_CONCEAL_MY_PV_THIS_FRAME()
				
				IF IS_VEHICLE_DRIVEABLE(vehToCheck)
				AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(tempPlayer)
				AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(tempPlayer)
				AND NOT HAS_PLAYER_ACCEPTED_SIMPLE_INTERIOR_INVITE()
				AND (NOT HAS_EXIT_TO_BUNKER_FROM_TRUCK_TRIGGERED() OR bDontConcealMyPV)
				AND (NOT HAS_EXIT_TO_BASE_FROM_AIRCRAFT_TRIGGERED() OR bDontConcealMyPV)
				OR IS_PLAYER_MOVING_FROM_SANDBOX_AREA_TO_SHOWROOM(tempPlayer)
					IF IS_BIT_SET(mpPropMaintain.remoteStoredPersVeh.iConcealedRemotePV_BS, i)
						IF NETWORK_IS_ENTITY_CONCEALED(vehToCheck)
//							#IF IS_DEBUG_BUILD
//							SHOULD_PLAYERS_PERSONAL_VEHCILE_BE_CONCEALED(i,vehToCheck,TRUE)
//							#ENDIF
//							
//							IF NOT HAS_EXIT_TO_BUNKER_FROM_TRUCK_TRIGGERED() OR bDontConcealMyPV
//								PRINTLN("[personal_vehicle] HANDLE_CONCEALING_PERSONAL_VEHICLES - player ", i, " personal vehicle already un-concealed necause bDontConcealMyPV: ", bDontConcealMyPV , " HAS_EXIT_TO_BUNKER_FROM_TRUCK_TRIGGERED: ", HAS_EXIT_TO_BUNKER_FROM_TRUCK_TRIGGERED() )
//							ENDIF
							
							NETWORK_CONCEAL_ENTITY(vehToCheck, FALSE)
							
							CLEAR_BIT(mpPropMaintain.remoteStoredPersVeh.iConcealedRemotePV_BS, i)
							
//							IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i),FALSE)
//								PRINTLN("[personal_vehicle] HANDLE_CONCEALING_PERSONAL_VEHICLES - un-concealing player ID ", i," name: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(i)), " personal vehicle")
//							ELSE
								PRINTLN("[personal_vehicle] HANDLE_CONCEALING_PERSONAL_VEHICLES - un-concealing player ", i, " personal vehicle")
//							ENDIF
						ELSE
							CLEAR_BIT(mpPropMaintain.remoteStoredPersVeh.iConcealedRemotePV_BS, i)
							
							PRINTLN("[personal_vehicle] HANDLE_CONCEALING_PERSONAL_VEHICLES - player ", i, " personal vehicle already un-concealed")
						ENDIF
					ELSE
						IF IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[localPlayerInt].propertyDetails.iCurrentlyInsideProperty)
							IF IS_NET_PLAYER_OK(localPlayerID)
								INTERIOR_INSTANCE_INDEX theInterior = GET_INTERIOR_FROM_ENTITY(localPlayerPedID)
								
								IF IS_VALID_INTERIOR(theInterior)
								AND IS_INTERIOR_READY(theInterior)
									IF NOT NETWORK_IS_ENTITY_CONCEALED(vehToCheck)
										IF GlobalplayerBD_FM[localPlayerInt].propertyDetails.iCurrentlyInsideProperty > 0
										AND GlobalplayerBD_FM[localPlayerInt].propertyDetails.iCurrentlyInsideProperty = GlobalplayerBD_FM[i].propertyDetails.iPVConcealInID
						 				AND NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(GlobalplayerBD_FM[localPlayerInt].propertyDetails.requestedGamerForInstance) = INT_TO_PLAYERINDEX(GlobalplayerBD_FM[i].propertyDetails.iPVConcealInPlayerOwnedLocation)
											IF IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[localPlayerInt].propertyDetails.iCurrentlyInsideProperty, PROPERTY_CLUBHOUSE_1_BASE_A)
												IF GET_ROOM_KEY_FROM_ENTITY(vehToCheck) != GET_HASH_KEY("BikerDLC_Int01_GrgRm")
													FORCE_ROOM_FOR_ENTITY(vehToCheck, theInterior, GET_HASH_KEY("BikerDLC_Int01_GrgRm"))
												ENDIF
											ELIF IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[localPlayerInt].propertyDetails.iCurrentlyInsideProperty, PROPERTY_CLUBHOUSE_7_BASE_B)
												IF GET_ROOM_KEY_FROM_ENTITY(vehToCheck) != GET_HASH_KEY("int02_MainRm")
													FORCE_ROOM_FOR_ENTITY(vehToCheck, theInterior, GET_HASH_KEY("int02_MainRm"))
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL IS_VEHICLE_MODEL_ABLE_TO_FLY(MODEL_NAMES eModel)
	IF IS_THIS_MODEL_ALLOWED_IN_HANGAR(eModel)
	OR eModel = THRUSTER
	OR eModel = DELUXO
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PERSONAL_VEHICLE_CLEANUP_ON_ISLAND() 
	IF NOT g_bPersonalVehAllowedOnIsland
		IF IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
			IF DOES_ENTITY_EXIST(PERSONAL_VEHICLE_ID())
			AND IS_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_ID())
				IF GET_DISTANCE_BETWEEN_COORDS(<<5017.422363, -5130.447754, 2.199280>>,GET_ENTITY_COORDS(MPGlobalsAmbience.vehPersonalVehicle)) <1500
					PRINTLN("SHOULD_PERSONAL_VEHICLE_CLEANUP_ON_ISLAND: returning true vehicle should get cleaned up")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IsSavedVehicleSafeToProcess()
	PLAYER_INDEX localPlayerID = PLAYER_ID()
	
	INT iLocalPlayerInt = NATIVE_TO_INT(PLAYER_ID())
	
	INT iJoinedPlayerGBD = GET_TRANSITION_SESSION_JOINED_PLAYER_GBD()

	IF IS_PERSONAL_VEHICLE_CREATION_DISABLED_THIS_FRAME() //no
	OR IS_PERSONAL_VEHICLE_CREATION_DISABLED_FOR_TRANSITION() //no
	OR IS_CURRENT_PERSONAL_VEHICLE_DISABLED_FOR_MISSION() // no
	OR (ARE_ALL_PERSONAL_VEHICLES_DISABLED_FOR_MISSION_STARTUP() AND NOT IS_OK_TO_SPAWN_VEHICLE_AT_START_OF_HEIST_MISSION()) //no
	OR (IS_PLAYER_IN_CORONA() AND NOT IS_THIS_IS_A_STRAND_MISSION()) //no
	OR Is_Player_Currently_On_MP_LTS_Mission(localPlayerID) //no i think
	OR Is_Player_Currently_On_MP_CTF_Mission(localPlayerID) //no i think
	OR Is_Player_Currently_On_MP_Versus_Mission(localPlayerID) //no i think
	OR IS_THIS_A_ROUNDS_MISSION_FOR_CORONA() //no
	OR IS_ON_HEIST_CORONA() //no
	OR IS_ON_RACE_GLOBAL_SET() //don't think so
	OR DID_I_JOIN_MISSION_AS_SPECTATOR() //maybe?? 7597] [Script] [00386369] SET_I_JOIN_MISSION_AS_SPECTATOR
	OR SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION() //no
	OR (TRANSITION_SESSIONS_PLAYER_VEHICLE_CLEAN_UP_CHECK() AND NOT IS_THIS_IS_A_STRAND_MISSION()) //no
	OR (IS_A_SPECTATOR_CAM_ACTIVE() AND NOT IS_NET_PLAYER_OK(localPlayerID, TRUE, FALSE))//player should be ok so no?  // if player is dead and in spectator mode (not just watching from tv or spectating crew) 2008763
	OR IS_PLAYER_SCTV(localPlayerID)
	OR GlobalplayerBD_FM[iLocalPlayerInt].iCurrentMissionType = FMMC_TYPE_MG  //i don't think so for these
	OR GlobalplayerBD_FM[iLocalPlayerInt].iCurrentMissionType = FMMC_TYPE_MG_GOLF
	OR GlobalplayerBD_FM[iLocalPlayerInt].iCurrentMissionType = FMMC_TYPE_MG_TENNIS
	OR GlobalplayerBD_FM[iLocalPlayerInt].iCurrentMissionType = FMMC_TYPE_MG_SHOOTING_RANGE
	OR GlobalplayerBD_FM[iLocalPlayerInt].iCurrentMissionType = FMMC_TYPE_MG_DARTS
	OR GlobalplayerBD_FM[iLocalPlayerInt].iCurrentMissionType = FMMC_TYPE_MG_ARM_WRESTLING
	OR GlobalplayerBD_FM[iLocalPlayerInt].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
	OR GlobalplayerBD_FM[iLocalPlayerInt].iCurrentMissionType = FMMC_TYPE_SURVIVAL
	OR GlobalplayerBD_FM[iLocalPlayerInt].iCurrentMissionType = FMMC_TYPE_BASE_JUMP
	OR GlobalplayerBD_FM[iLocalPlayerInt].iCurrentMissionType = FMMC_TYPE_MG_PILOT_SCHOOL
	OR IsCelebrationScreenActiveThatRequiresPVCleanup()
	OR (IS_THIS_TRANSITION_SESSION_A_PLAYLIST() AND (IS_SKYSWOOP_IN_SKY() OR IS_SKYSWOOP_MOVING()))
	OR SHOULD_PERSONAL_VEHICLE_CLEANUP_ON_ISLAND()
		#IF IS_DEBUG_BUILD
			IF (g_b_IsSavedVehicleSafeToProcess)			
				NET_PRINT("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - just become unsafe to process!") NET_NL()
				DebugOutputFailedSafeToProcess()
				g_b_IsSavedVehicleSafeToProcess = FALSE				
			ENDIF
		#ENDIF
		
		RETURN (FALSE)
	
	ELIF (iJoinedPlayerGBD != -1)
		IF GlobalplayerBD_FM[iJoinedPlayerGBD].iCurrentMissionType = FMMC_TYPE_MG
		OR GlobalplayerBD_FM[iJoinedPlayerGBD].iCurrentMissionType = FMMC_TYPE_MG_GOLF
		OR GlobalplayerBD_FM[iJoinedPlayerGBD].iCurrentMissionType = FMMC_TYPE_MG_TENNIS
		OR GlobalplayerBD_FM[iJoinedPlayerGBD].iCurrentMissionType = FMMC_TYPE_MG_SHOOTING_RANGE
		OR GlobalplayerBD_FM[iJoinedPlayerGBD].iCurrentMissionType = FMMC_TYPE_MG_DARTS
		OR GlobalplayerBD_FM[iJoinedPlayerGBD].iCurrentMissionType = FMMC_TYPE_MG_ARM_WRESTLING
		OR GlobalplayerBD_FM[iJoinedPlayerGBD].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
		OR GlobalplayerBD_FM[iJoinedPlayerGBD].iCurrentMissionType = FMMC_TYPE_SURVIVAL
		OR GlobalplayerBD_FM[iJoinedPlayerGBD].iCurrentMissionType = FMMC_TYPE_BASE_JUMP
		OR GlobalplayerBD_FM[iJoinedPlayerGBD].iCurrentMissionType = FMMC_TYPE_MG_PILOT_SCHOOL
		
		#IF IS_DEBUG_BUILD
			IF (g_b_IsSavedVehicleSafeToProcess)			
				NET_PRINT("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - no longer safe to process! Details:") NET_NL()		
				NET_PRINT("[personal_vehicle] 	GlobalplayerBD_FM[iJoinedPlayerGBD].iCurrentMissionType = ") NET_PRINT_INT(ENUM_TO_INT(GlobalplayerBD_FM[iJoinedPlayerGBD].iCurrentMissionType)) NET_NL()				
				g_b_IsSavedVehicleSafeToProcess = FALSE				
			ENDIF
		#ENDIF			
		
			RETURN(FALSE)
		ENDIF
	ENDIF

	RETURN(TRUE)
ENDFUNC

#IF IS_DEBUG_BUILD
PROC DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(INT iFailReason)
	IF NOT (g_iFailReason_SavedVehicleAvailableForCreation = iFailReason)
		PRINTLN("[personal_vehicle] DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION - changing to ", iFailReason )	
		g_iFailReason_SavedVehicleAvailableForCreation = iFailReason
	ENDIF
ENDPROC
#ENDIF

FUNC INT GET_OWNED_IE_GARAGE()
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_IE_WAREHOUSE]
ENDFUNC

FUNC BOOL IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(BOOL bAllowImpoundedVehicles = FALSE, BOOL bAllowIfInGarage=FALSE, BOOL bAllowSpecialVehicles=TRUE)

    IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
	 	IF g_bDisableAmbientSpawnOfPersonalVehicleOnMission
		AND NETWORK_IS_ACTIVITY_SESSION()
			 #IF IS_DEBUG_BUILD 
				DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(19)
			#ENDIF
			RETURN FALSE
		ENDIF
		
		//IF DOES_VEHICLE_SLOT_BELONG_IN_ANY_GARAGE(CURRENT_SAVED_VEHICLE_SLOT()) // removed for 2851801, as some pv can be in storage.
		
	 
			IF DOES_PLAYER_OWN_ANY_VEHICLE_STORAGE()
			OR globalPropertyEntryData.iCostOfPropertyJustBought != 0
				IF g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT
				    //PRINTLN("[personal_vehicle] IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION FALSE g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT")
				    #IF IS_DEBUG_BUILD 
						DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(1)
					#ENDIF
					RETURN FALSE
				ENDIF
			
				
			  	//IF NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AFTER_SESSION)
			      	IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) 
			           	// PRINTLN("[personal_vehicle] IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION FALSE IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) ")
			            #IF IS_DEBUG_BUILD 
							DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(2)
						#ENDIF
						RETURN FALSE
			       	ENDIF
			  	//ENDIF

				IF NOT (bAllowIfInGarage)
			        IF NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
			           // PRINTLN("[personal_vehicle] IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION FALSE IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE) ")
			            #IF IS_DEBUG_BUILD 
							DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(3)
						#ENDIF
						RETURN FALSE
			       	ENDIF		
				ENDIF
				
			  	IF NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AFTER_SESSION)
			       //PRINTLN("[personal_vehicle] IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION FALSE IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AFTER_SESSION)")
			       
				   	IF NOT bAllowImpoundedVehicles
			           	IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
			                //PRINTLN("[personal_vehicle] IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION FALSE IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED) ")
			                #IF IS_DEBUG_BUILD 
								DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(4) 
							#ENDIF
							RETURN FALSE
			           	ENDIF
				  	ENDIF
			 	ENDIF
			  
			ELSE
				IF CURRENT_SAVED_VEHICLE_SLOT() != 0
				AND CURRENT_SAVED_VEHICLE_SLOT() != DISPLAY_SLOT_AA_TRAILER
				   SET_LAST_USED_VEHICLE_SLOT(0)
				ENDIF
				IF g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT
				//                   PRINTLN("[personal_vehicle] IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION FALSE - DUMMY_MODEL_FOR_SCRIPT ")
				   	#IF IS_DEBUG_BUILD 
						DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(5)
					#ENDIF
				   	RETURN FALSE
				ENDIF
				IF NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AFTER_SESSION)
				   	IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) 
				//                        PRINTLN("[personal_vehicle] IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION FALSE - MP_SAVED_VEHICLE_DESTROYED ")
				        #IF IS_DEBUG_BUILD 
							DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(6)
						#ENDIF
						RETURN FALSE
				   	ENDIF
				   	IF NOT bAllowImpoundedVehicles
				       	IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
				//	                        PRINTLN("[personal_vehicle] IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION FALSE - MP_SAVED_VEHICLE_IMPOUNDED ")
				           	#IF IS_DEBUG_BUILD 
								DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(7)
							#ENDIF
							RETURN FALSE
				       	ENDIF
					ENDIF
				ENDIF
			ENDIF
		  
		  	// is this an illegal dlc vehicle model?
	  		IF NOT IS_VEHICLE_AVAILABLE_PAST_CURRENT_TIME(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel)
				NET_PRINT("[personal_vehicle] IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION - returning FALSE - IS_VEHICLE_AVAILABLE_PAST_CURRENT_TIME = FALSE.") NET_NL()
				#IF IS_DEBUG_BUILD 
					DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(14)
				#ENDIF
				RETURN FALSE
			ENDIF
		  
//		ELSE
//			NET_PRINT("[personal_vehicle] IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION - returning FALSE - vehicle slot not in garage player owns.") NET_NL()
//			#IF IS_DEBUG_BUILD 
//				DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(12) 
//			#ENDIF
//			RETURN FALSE		 
//		ENDIF

		// is there currently a remote bomb being triggered. see 3089472
		IF HAS_VEHICLE_PHONE_EXPLOSIVE_DEVICE()
			NET_PRINT("[personal_vehicle] IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION - returning FALSE - HAS_VEHICLE_PHONE_EXPLOSIVE_DEVICE = true.") NET_NL()
			#IF IS_DEBUG_BUILD 
				DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(15)
			#ENDIF
			RETURN FALSE	
		ENDIF
			
		
	ELSE
        //  PRINTLN("CURRENT_SAVED_VEHICLE_SLOT() < 0 ")
		#IF IS_DEBUG_BUILD 
		DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(8)
		#ENDIF
		RETURN FALSE	
    ENDIF
     
	//Check to see if Vehicle Delivery is in the process of creating the vehicle.
	IF MPGlobalsAmbience.bLaunchVehicleDropPersonal = TRUE
		// PRINTLN("MPGlobalsAmbience.bLaunchVehicleDropPersonal  = TRUE ")
		#IF IS_DEBUG_BUILD 
			DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(9)
		#ENDIF
		RETURN FALSE
	ENDIF
	 
	IF GET_CLIENT_SCRIPT_GAME_STATE(PLAYER_ID()) <= MAIN_GAME_STATE_HOLD_FOR_TRANSITION
	AND NOT g_bFinishedInitialSpawn
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.MissionPVBitset, ciSPAWN_PV_IN_ORIGINAL_LOC)
		AND SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN(FALSE)
			// allow
		ELSE
			PRINTLN("[personal_vehicle] IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION: False - GET_CLIENT_SCRIPT_GAME_STATE(PLAYER_ID()) <= MAIN_GAME_STATE_HOLD_FOR_TRANSITION and NOT g_bFinishedInitialSpawn ")
			#IF IS_DEBUG_BUILD 
				DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(10)
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	 
	IF bTransitionSpawningInProperty
	OR g_SpawnData.bSpawningInProperty
	OR g_SpawnData.bSpawningInSimpleInterior
		PRINTLN("[personal_vehicle] IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION: False - bTransitionSpawningInProperty OR g_SpawnData.bSpawningInProperty = TRUE OR g_SpawnData.bSpawningInSimpleInterior is TRUE")
		#IF IS_DEBUG_BUILD 
			DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(11)
		#ENDIF
		RETURN FALSE
	ENDIF
    
	IF NOT IsSavedVehicleSafeToProcess()
		PRINTLN("[personal_vehicle] IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION: False - IsSavedVehicleSafeToProcess() = FALSE ")
		#IF IS_DEBUG_BUILD 
			DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(13)
		#ENDIF
		RETURN FALSE		
	ENDIF
	
	IF NOT (bAllowSpecialVehicles)
		IF CURRENT_SAVED_VEHICLE_SLOT() > -1
			IF IS_THIS_MODEL_A_SPECIAL_VEHICLE(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel)
			OR IS_MODEL_A_PERSONAL_TRAILER(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel)
			OR IS_THIS_MODEL_ALLOWED_IN_HANGAR(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel)
				IF g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel != OPPRESSOR2
					PRINTLN("[personal_vehicle] IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION: is a special vehicle, and they are not allowed.")
					
					// send back to garage if necessary
					IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
						CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
						PRINTLN("[personal_vehicle] MP_SAVED_VEHICLE_OUT_GARAGE - 5cleared on #",CURRENT_SAVED_VEHICLE_SLOT())
						PRINTLN("[personal_vehicle] IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION: setting special vehicle back in garage.")
					ENDIF
					
					#IF IS_DEBUG_BUILD
						DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(16)
					#ENDIF
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	// if this is an oppressor and we are in cooldown, then dont allow
	IF CURRENT_SAVED_VEHICLE_SLOT() > -1
		IF g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel = OPPRESSOR2
		AND IS_OPPRESSOR2_ON_CD()
		    #IF IS_DEBUG_BUILD 
				DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(17) 
			#ENDIF
			RETURN FALSE		
		ENDIF		
	ENDIF
	
	// if we are on the heist island then dont allow
	#IF FEATURE_HEIST_ISLAND
	IF IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
		PRINTLN("IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION: FALSE -  on heist island")
		#IF IS_DEBUG_BUILD 
			DebugPrintFailReasonFor_IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(18) 
		#ENDIF
		RETURN FALSE
	ENDIF
	#ENDIF	
	
	#IF IS_DEBUG_BUILD
	g_iFailReason_SavedVehicleAvailableForCreation = -1
	#ENDIF
	
	PRINTLN("[personal_vehicle] IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION: TRUE ")
    RETURN TRUE
	
ENDFUNC


FUNC BOOL IS_VEHICLE_BEING_USED_FOR_TIME_TRIAL(VEHICLE_INDEX veh)

	IF DECOR_IS_REGISTERED_AS_TYPE("UsingForTimeTrial", DECOR_TYPE_BOOL)
		IF DECOR_EXIST_ON(veh, "UsingForTimeTrial") 
			RETURN DECOR_GET_BOOL(veh,"UsingForTimeTrial")
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


PROC SET_MAINTAIN_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
	DEBUG_PRINTCALLSTACK()
	g_SpawnData.MissionSpawnPersonalVehicleData.bMissionOKToProceedAfterPVCreation = TRUE
	g_SpawnData.MissionSpawnPersonalVehicleData.iTimerOKToProceedAfterPVCreation = GET_GAME_TIMER()	
ENDPROC

PROC MAINTAIN_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION(BOOL isWarping)
	IF (g_SpawnData.MissionSpawnPersonalVehicleData.bMissionWaitingForPVCreation)	
		IF NOT (g_SpawnData.MissionSpawnPersonalVehicleData.bMissionOKToProceedAfterPVCreation)
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()

				IF IsSavedVehicleSafeToProcess()
					IF NETWORK_DOES_NETWORK_ID_EXIST(PERSONAL_VEHICLE_NET_ID())
					AND DOES_ENTITY_EXIST(PERSONAL_VEHICLE_ID())
						IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
							IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_CLEANUP)
							AND NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_CLEANUP_DELETE)
							AND NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_CLEANUP_RETURN_TO_GARAGE)
							AND NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_CLEANUP_SET_AS_DESTROYED)
								IF NOT (isWarping)
									PRINTLN("[personal_vehicle] MAINTAIN_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION - setting to TRUE, pv exists and is not being cleaned up.")
									SET_MAINTAIN_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
								ELSE
									PRINTLN("[personal_vehicle] MAINTAIN_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION - pv exists, but is currently warping.")
								ENDIF
							ELSE
								PRINTLN("[personal_vehicle] MAINTAIN_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION - pv exists but is being cleaned up.")	
							ENDIF	
						ELSE
							PRINTLN("[personal_vehicle] MAINTAIN_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION - pv exists but is not driveable.")		
						ENDIF
					ELSE				
						IF IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(TRUE)
							PRINTLN("[personal_vehicle] MAINTAIN_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION - waiting for pv to exist")
						ELSE
							PRINTLN("[personal_vehicle] MAINTAIN_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION - setting to TRUE, IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION = FALSE")	
							SET_MAINTAIN_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
						ENDIF
					ENDIF				
				ELSE
					PRINTLN("[personal_vehicle] MAINTAIN_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION - setting to TRUE, IsSavedVehicleSafeToProcess = FALSE.")
					SET_MAINTAIN_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
				ENDIF
			ELSE
				PRINTLN("[personal_vehicle] MAINTAIN_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION - setting to TRUE, IS_FAKE_MULTIPLAYER_MODE_SET = true.")
				SET_MAINTAIN_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
			ENDIF
		ENDIF
	ELSE
		IF (g_SpawnData.MissionSpawnPersonalVehicleData.bMissionOKToProceedAfterPVCreation)
			g_SpawnData.MissionSpawnPersonalVehicleData.bMissionOKToProceedAfterPVCreation = FALSE
			PRINTLN("[personal_vehicle] MAINTAIN_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION - setting bMissionOKToProceedAfterPVCreation to FALSE, bMissionWaitingForPVCreation = FALSE")
		ENDIF
	ENDIF
	
	// set broadcast data for other players
	IF (g_SpawnData.MissionSpawnPersonalVehicleData.bDisablePersonalVehicleCreationForMissionStartup) // if pv creation is currently disabled for mission startup then set to false
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bPVSpawnedForMission = FALSE
	ELSE
		IF (g_SpawnData.MissionSpawnPersonalVehicleData.bMissionWaitingForPVCreation)	
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bPVSpawnedForMission = g_SpawnData.MissionSpawnPersonalVehicleData.bMissionOKToProceedAfterPVCreation
		ELSE
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bPVSpawnedForMission = TRUE
		ENDIF
	ENDIF
	
ENDPROC

PROC REFRESH_PERSONAL_VEHICLE_LOCK_STATES()
	MPGlobalsAmbience.bRefreshYachtVehicleLockStates = TRUE
	MPGlobalsAmbience.iStoredAllowIntoPV = -1
	MPGlobalsAmbience.iStoredAllowIntoTruck = -1
	MPGlobalsAmbience.iStoredAllowIntoAvenger = -1
	MPGlobalsAmbience.iStoredAllowIntoHackerTruck = -1
	DEBUG_PRINTCALLSTACK()
ENDPROC


PROC MAINTAIN_MOVE_VEHICLE_FROM_TRUCK_TO_BUNKER()
	IF IS_PERSONAL_CAR_MOD_TRUCK_LEAVE_ON_FOOT(PLAYER_ID())
	AND IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID())
	AND (NOT IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID()) OR IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR())
		IF DOES_ENTITY_EXIST(g_truckPersonalCarModVeh)
		AND IS_VEHICLE_DRIVEABLE(g_truckPersonalCarModVeh)
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				//IF NOT NETWORK_IS_ENTITY_CONCEALED(g_truckPersonalCarModVeh)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(g_truckPersonalCarModVeh)	
						IF IS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID())
							IF NOT g_bSafeToMovePV
								SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_CHECK_SAFE_TO_MOVE_PV)
								
								EXIT
							ELSE
								g_bSafeToMovePV = FALSE
								
								CLEAR_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_CHECK_SAFE_TO_MOVE_PV)
							ENDIF
							
							SET_ENTITY_COORDS_NO_OFFSET(g_truckPersonalCarModVeh, <<883.0698, -3240.5032, -99.2772>>)	
							SET_ENTITY_HEADING(g_truckPersonalCarModVeh, 180.0)	
							SET_VEHICLE_ON_GROUND_PROPERLY(g_truckPersonalCarModVeh)
							PRINTLN("MAINTAIN_MOVE_VEHICLE_FROM_TRUCK_TO_BUNKER GET_ENTITY_COORD set coords to <<883.0698, -3240.5032, -99.2772>> ")
							GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInPlayerOwnedLocation = NATIVE_TO_INT(PLAYER_ID())
							SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)
							GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInID = ENUM_TO_INT(GET_OWNED_BUNKER(PLAYER_ID()))
							GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInType = PV_CONCEAL_TYPE_BUNKER
						
							SET_PV_ASSIGN_TO_MAIN_SCRIPT(TRUE)
							SET_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(FALSE)
							PRINTLN("MAINTAIN_MOVE_VEHICLE_FROM_TRUCK_TO_BUNKER SET_CONCEAL_CURRENT_PERSONAL_VEHICLE: setting current personal vehicle to be concealed for ",GET_PLAYER_NAME(PLAYER_ID()), " conceal ID #", ENUM_TO_INT(GET_OWNED_BUNKER_SIMPLE_INTERIOR(PLAYER_ID())), " iTypeID = ",PV_CONCEAL_TYPE_BUNKER)
						ELIF IS_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID())	
							GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].pegasusConceal.iConcealInPlayerOwnedLocation = NATIVE_TO_INT(PLAYER_ID())
							SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].pegasusConceal.iBS,CONCEAL_PEGASUS_VEHICLE_BS_CONCEAL_CURRENT)
							GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].pegasusConceal.iConcealInID = ENUM_TO_INT(GET_OWNED_BUNKER(PLAYER_ID()))
							GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].pegasusConceal.iConcealInType = PV_CONCEAL_TYPE_BUNKER
							SET_CLEARNUP_PEGASUS_BEFORE_SUPERMOD(TRUE)
							SET_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(FALSE)
							PRINTLN("MAINTAIN_MOVE_VEHICLE_FROM_TRUCK_TO_BUNKER SET_CONCEAL_CURRENT_PEGASUS_VEHICLE: setting current pegasus vehicle to be concealed for ",GET_PLAYER_NAME(PLAYER_ID()), " conceal ID #",ENUM_TO_INT(GET_OWNED_BUNKER_SIMPLE_INTERIOR(PLAYER_ID())), " iTypeID = ",PV_CONCEAL_TYPE_BUNKER)
						ENDIF			
//						SET_DONT_CONCEAL_MY_PEGASUS_THIS_FRAME(FALSE)
//						SET_DONT_CONCEAL_MY_PV_THIS_FRAME(FALSE)
						SET_PERSONAL_CAR_MOD_TRUCK_LEAVE_ON_FOOT(FALSE)
						PRINTLN("MAINTAIN_MOVE_VEHICLE_FROM_TRUCK_TO_BUNKER: Moved player vehicle")
					ELSE
						IF IS_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID())	
							SET_REMOVE_PEGASUS_INSIDE_TRUCK_PROPERTY(TRUE)
						ENDIF	
						NETWORK_REQUEST_CONTROL_OF_ENTITY(g_truckPersonalCarModVeh)
						PRINTLN("[SIMPLE_INTERIOR] MAINTAIN_MOVE_VEHICLE_FROM_TRUCK_TO_BUNKER NETWORK_REQUEST_CONTROL_OF_ENTITY g_truckPersonalCarModVeh ")
					ENDIF	
//				ELSE
//					IF IS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID())
//						SET_DONT_CONCEAL_MY_PV_THIS_FRAME(TRUE)
//					ELIF IS_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID())	
//						SET_DONT_CONCEAL_MY_PEGASUS_THIS_FRAME(TRUE)
//					ENDIF
//				ENDIF	
			ENDIF	
		ELSE
			PRINTLN("[SIMPLE_INTERIOR] MAINTAIN_MOVE_VEHICLE_FROM_TRUCK_TO_BUNKER g_truckPersonalCarModVeh doesn't exist ")
//			SET_DONT_CONCEAL_MY_PEGASUS_THIS_FRAME(FALSE)
//			SET_DONT_CONCEAL_MY_PV_THIS_FRAME(FALSE)
			IF IS_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID())	
				SET_REMOVE_PEGASUS_INSIDE_TRUCK_PROPERTY(FALSE)
				SET_PERSONAL_CAR_MOD_TRUCK_LEAVE_ON_FOOT(FALSE)
				SET_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(FALSE)
			ENDIF
		ENDIF
	ENDIF
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree ,PROPERTY_BROADCAST_BS3_I_NEED_TO_RENOVATE_TRUCK_TRAILER)
			IF IS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID())
			OR IS_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID()) 
				IF NOT IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
					IF !IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID())
						IF DOES_ENTITY_EXIST(g_truckPersonalCarModVeh)
						AND IS_VEHICLE_DRIVEABLE(g_truckPersonalCarModVeh)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(g_truckPersonalCarModVeh)
								IF NOT IS_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID())
									CLEANUP_MP_SAVED_VEHICLE(TRUE)
									//SET_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(FALSE)
									PRINTLN("MAINTAIN_MOVE_VEHICLE_FROM_TRUCK_TO_BUNKER - move vehicle to freemode after renovation")
								ELSE
									SET_CLEARNUP_PEGASUS_BEFORE_SUPERMOD(TRUE)
									PRINTLN("MAINTAIN_MOVE_VEHICLE_FROM_TRUCK_TO_BUNKER - delete pegasus vehicle after renovation")
								ENDIF
							ELSE
								PRINTLN("MAINTAIN_MOVE_VEHICLE_FROM_TRUCK_TO_BUNKER - NETWORK_HAS_CONTROL_OF_ENTITY false")
								NETWORK_REQUEST_CONTROL_OF_ENTITY(g_truckPersonalCarModVeh)
							ENDIF
						ELSE
							PRINTLN("MAINTAIN_MOVE_VEHICLE_FROM_TRUCK_TO_BUNKER - vehicle doesn't exist")
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(g_truckPersonalCarModVeh)
						AND IS_VEHICLE_DRIVEABLE(g_truckPersonalCarModVeh)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(g_truckPersonalCarModVeh)
								SET_ENTITY_COORDS(g_truckPersonalCarModVeh, <<883.0698, -3240.5032, -99.2772>>)	
								SET_ENTITY_HEADING(g_truckPersonalCarModVeh, 180.0)	
								
								IF IS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID())
									GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInPlayerOwnedLocation = NATIVE_TO_INT(PLAYER_ID())
									SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)
									GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInID = ENUM_TO_INT(GET_OWNED_BUNKER(PLAYER_ID()))
									GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInType = PV_CONCEAL_TYPE_BUNKER
									PRINTLN("MAINTAIN_MOVE_VEHICLE_FROM_TRUCK_TO_BUNKER SET_CONCEAL_CURRENT_PERSONAL_VEHICLE: setting current personal vehicle to be concealed for ",GET_PLAYER_NAME(PLAYER_ID()), " conceal ID #", ENUM_TO_INT(GET_OWNED_BUNKER_SIMPLE_INTERIOR(PLAYER_ID())), " iTypeID = ",PV_CONCEAL_TYPE_BUNKER)
									SET_PV_ASSIGN_TO_MAIN_SCRIPT(TRUE)
									
									SET_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(FALSE)
								ELIF IS_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID())	
								
									GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].pegasusConceal.iConcealInPlayerOwnedLocation = NATIVE_TO_INT(PLAYER_ID())
									SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].pegasusConceal.iBS,CONCEAL_PEGASUS_VEHICLE_BS_CONCEAL_CURRENT)
									GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].pegasusConceal.iConcealInID = ENUM_TO_INT(GET_OWNED_BUNKER(PLAYER_ID()))
									GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].pegasusConceal.iConcealInType = PV_CONCEAL_TYPE_BUNKER
									PRINTLN("SET_CONCEAL_CURRENT_PEGASUS_VEHICLE: setting current personal vehicle to be concealed for ",GET_PLAYER_NAME(PLAYER_ID()), " conceal ID #", ENUM_TO_INT(GET_OWNED_BUNKER_SIMPLE_INTERIOR(PLAYER_ID())), " iTypeID = ",PV_CONCEAL_TYPE_BUNKER)
						
									SET_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(FALSE)
								ENDIF
								
								PRINTLN("MAINTAIN_MOVE_VEHICLE_FROM_TRUCK_TO_BUNKER - move vehicle to bunker after renovation")
							ELSE
								NETWORK_REQUEST_CONTROL_OF_ENTITY(g_truckPersonalCarModVeh)
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC	

FUNC STRING GET_PERSONAL_AIRCRAFT_DELIVERY_MESSAGE(VEHICLE_INDEX tempveh)
	MODEL_NAMES eModel = GET_ENTITY_MODEL(tempVeh)
	
	IF WAM_GET_STEAL_MISSION_UNLOCK_COUNT_FROM_VEHICLE_MODEL(eModel) > 0
		SWITCH eModel
			CASE ALPHAZ1		RETURN "PA_DELIVERY_1"	BREAK
			CASE BOMBUSHKA		RETURN "PA_DELIVERY_2"	BREAK
			CASE HAVOK			RETURN "PA_DELIVERY_3"	BREAK
			CASE HOWARD			RETURN "PA_DELIVERY_4"	BREAK
			CASE HUNTER			RETURN "PA_DELIVERY_5"	BREAK
			CASE MICROLIGHT		RETURN "PA_DELIVERY_6"	BREAK
			CASE MOGUL			RETURN "PA_DELIVERY_7"	BREAK
			CASE MOLOTOK		RETURN "PA_DELIVERY_8"	BREAK
			CASE NOKOTA			RETURN "PA_DELIVERY_9"	BREAK
			CASE PYRO			RETURN "PA_DELIVERY_10"	BREAK
			CASE ROGUE			RETURN "PA_DELIVERY_11"	BREAK
			CASE STARLING		RETURN "PA_DELIVERY_12"	BREAK
			CASE SEABREEZE		RETURN "PA_DELIVERY_13"	BREAK
			CASE TULA			RETURN "PA_DELIVERY_14"	BREAK
		ENDSWITCH
	ELSE
		IF (eModel = AKULA)
			RETURN "PA_DELIVERY_18"
		ELIF (eModel = VOLATOL)
			RETURN "PA_DELIVERY_19"
		ELIF (eModel = SEASPARROW)
			RETURN "PA_DELIVERY_20"		//RADAR_TRACE_ACSR_WP1
		ELIF IS_THIS_MODEL_A_HELI(eModel)
			RETURN "PA_DELIVERY_15"
		ELIF IS_THIS_MODEL_A_FIGHTER_JET(eModel)
			RETURN "PA_DELIVERY_16"
		ELIF IS_THIS_MODEL_A_PLANE(eModel)
			RETURN "PA_DELIVERY_17"
		ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC BOOL IS_VEHICLE_IN_BUNKER_BOUNDS(VEHICLE_INDEX vehIndex)
	IF IS_ENTITY_IN_AREA(vehIndex, <<1060.325, -3064.239, -100.9374>>, <<821.320, -3255.345, -75.0000>>)
		PRINTLN("IS_PLAYER_IN_BUNKER_BOUNDS - Returning TRUE.")
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SIMPLE_INTERIOR_PV_CONCEAL_CHECK()
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInID = -1
	AND DOES_ENTITY_EXIST(MPGlobalsAmbience.vehPersonalVehicle)
	AND IS_VEHICLE_IN_BUNKER_BOUNDS(MPGlobalsAmbience.vehPersonalVehicle)
	AND NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
	AND NOT IS_BIT_SET(g_iBS1_Mission, ciBS1_Mission_AllowPVVehiclesInBunker)
	AND NOT GB_IS_GLOBAL_NON_BD_BIT1_SET(eGB_GLOBAL_NON_BD_BITSET_1_DISABLE_SIMPLE_INTERIOR_PV_CONCEAL_CHECK)
		PRINTLN("MAINTAIN_SIMPLE_INTERIOR_PV_CONCEAL_CHECK - Cleaning up PV in Bunker")
		
		CLEANUP_MP_SAVED_VEHICLE(TRUE, DEFAULT, TRUE)
	ENDIF
ENDPROC

PROC MAINTAIN_CLEANUP_MP_SAVED_VEHICLE_IN_INVALID_INTERIOR()
	BOOL bKeepTimer = FALSE
	//if personal vehicle is in hangar, clean it up
	IF NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		IF IS_ENTITY_IN_AREA(PERSONAL_VEHICLE_ID(),<<-1215.000, -3075.000, -21.830>>,<<-1315.000, -2958.230, -52.000>>)
			bKeepTimer = TRUE
			IF NOT HAS_NET_TIMER_STARTED(MPGlobals.VehicleData.iInvalidInteriorCleanupTimer)
				START_NET_TIMER(MPGlobals.VehicleData.iInvalidInteriorCleanupTimer,TRUE)
				
			ELIF HAS_NET_TIMER_EXPIRED(MPGlobals.VehicleData.iInvalidInteriorCleanupTimer,10000,TRUE)
				CLEANUP_MP_SAVED_VEHICLE(true,FALSE,TRUE)
				RESET_NET_TIMER(MPGlobals.VehicleData.iInvalidInteriorCleanupTimer)
				PRINTLN("MAINTAIN_CLEANUP_MP_SAVED_VEHICLE_IN_INVALID_INTERIOR - Cleaning up PV in Hangar")
			ENDIF
		ENDIF
	ENDIF
	IF NOT bKeepTimer
		IF HAS_NET_TIMER_STARTED(MPGlobals.VehicleData.iInvalidInteriorCleanupTimer)
			RESET_NET_TIMER(MPGlobals.VehicleData.iInvalidInteriorCleanupTimer)
			PRINTLN("MAINTAIN_CLEANUP_MP_SAVED_VEHICLE_IN_INVALID_INTERIOR - reseting timer")
		ENDIF
	ENDIF
ENDPROC

#IF FEATURE_CASINO
PROC GET_CASINO_PARKING_SPOT_LOCATION(INT iLocation, VECTOR &vLoc, FLOAT &fHeading)
	IF iLocation < 0
	OR iLocation > 31
		PRINTLN("GET_CASINO_PARKING_SPOT_LOCATION: INVALID LOCATION!! iLocation = ", iLocation)
		SCRIPT_ASSERT("GET_CASINO_PARKING_SPOT_LOCATION: INVALID LOCATION!!")
	ENDIF
	SWITCH iLocation
		CASE 0
			vLoc = <<1380.2461, 221.1936, -49.8>>
			fHeading = 268.1998
		BREAK
		CASE 1
			vLoc = <<1380.4011, 225.5420, -49.8>>
			fHeading = 90.5987
		BREAK
		CASE 2
			vLoc = <<1394.0240, 204.5864, -49.8>>
			fHeading = 92.1979
		BREAK
		CASE 3
			vLoc = <<1394.2318, 237.6428, -49.8>>
			fHeading = 90.5988
		BREAK
		CASE 4
			vLoc = <<1393.9592, 229.3572, -49.8>>
			fHeading = 93.5988
		BREAK
		CASE 5
			vLoc = <<1366.3976, 237.6949, -49.8>>
			fHeading = 271.1988
		BREAK
		CASE 6
			vLoc = <<1366.1423, 229.3788, -49.8>>
			fHeading = 268.5987
		BREAK
		CASE 7
			vLoc = <<1380.4966, 237.8432, -49.9944>>
			fHeading = 88.3985
		BREAK
		CASE 8
			vLoc = <<1380.3802, 242.2101, -49.9944>>
			fHeading = 270.9987
		BREAK
		CASE 9
			vLoc = <<1380.2729, 208.8658, -49.8>>
			fHeading = 272.3987
		BREAK
		CASE 10
			vLoc = <<1366.0927, 212.7634, -49.8>>
			fHeading = 271.3987
		BREAK
		CASE 11
			vLoc = <<1394.1305, 225.1910, -49.8>>
			fHeading = 91.1988
		BREAK
		CASE 12
			vLoc =<<1394.1263, 221.2237, -49.8>>
			fHeading = 90.3988
		BREAK
		CASE 13
			vLoc = <<1366.1472, 225.3195, -49.8>>
			fHeading = 269.9988
		BREAK
		CASE 14
			vLoc = <<1366.2468, 221.0986, -49.8>>
			fHeading = 270.1987
		BREAK
		CASE 15
			vLoc = <<1366.4098, 233.5972, -49.8>>
			fHeading = 273.1988
		BREAK
		CASE 16
			vLoc = <<1394.0109, 233.4561, -49.8>>
			fHeading = 88.3988
		BREAK
		CASE 17
			vLoc = <<1394.0236, 241.9537, -49.8>>
			fHeading = 91.5988
		BREAK
		CASE 18
			vLoc = <<1394.3064, 246.0886, -49.8>>
			fHeading = 93.9989
		BREAK
		CASE 19
			vLoc = <<1366.3137, 241.8321, -49.8>>
			fHeading = 270.3987
		BREAK
		CASE 20
			vLoc = <<1366.1464, 246.3231, -49.8>>
			fHeading = 265.3989
		BREAK
		CASE 21
			vLoc = <<1366.4662, 250.4494, -49.8>>
			fHeading = 273.1988
		BREAK
		CASE 22
			vLoc = <<1366.0286, 254.7411, -49.8>>
			fHeading = 266.9988
		BREAK
		CASE 23
			vLoc = <<1394.0731, 250.5416, -49.8>>
			fHeading = 89.9988
		BREAK
		CASE 24
			vLoc = <<1394.0795, 254.6635, -49.8>>
			fHeading = 93.7988
		BREAK
		CASE 25
			vLoc = <<1394.1858, 208.5477, -49.8>>
			fHeading = 91.7988
		BREAK
		CASE 26
			vLoc = <<1394.0452, 216.6876, -49.8>>
			fHeading = 88.3988
		BREAK
		CASE 27
			vLoc = <<1366.2700, 217.0924, -49.8>>
			fHeading = 271.9987
		BREAK
		CASE 28
			vLoc = <<1366.0999, 208.6777, -49.8>>
			fHeading = 269.3986
		BREAK
		CASE 29
			vLoc = <<1394.2329, 212.5603, -49.8>>
			fHeading = 91.5988
		BREAK
		
//		CASE 0
//			vLoc = << 1380.5400,245.0000,-49.0000>>
//			fHeading = -115.1000
//		BREAK
//		CASE 1
//			vLoc = << 1380.5400,240.0000,-49.0000>>
//			fHeading = -115.1000
//		BREAK
//		CASE 2
//			vLoc = << 1380.5400,235.0000,-49.0000>>
//			fHeading = -115.1000
//		BREAK
//		CASE 3
//			vLoc = << 1380.5400,228.0000,-49.0000>>
//			fHeading = -115.1000
//		BREAK
//		CASE 4
//			vLoc = << 1380.5400,223.0000,-49.0000>>
//			fHeading = -115.1000
//		BREAK
//		CASE 5
//			vLoc = << 1380.5400,218.0000,-49.0000>>
//			fHeading = -115.1000
//		BREAK
//		CASE 6
//			vLoc = << 1380.5400,211.0000,-49.0000>>
//			fHeading = -115.1000
//		BREAK
//		CASE 7
//			vLoc = << 1380.5400,206.0000,-49.0000>>
//			fHeading = -115.1000
//		BREAK
//		CASE 8
//			vLoc = << 1367.0000,253.0000,-49.0000>>
//			fHeading = -115.0000
//		BREAK
//		CASE 9
//			vLoc = << 1367.0000,246.0000,-49.0000>>
//			fHeading = -115.0000
//		BREAK
//		CASE 10
//			vLoc = << 1367.0000,241.0000,-49.0000>>
//			fHeading = -115.0000
//		BREAK
//		CASE 11
//			vLoc = << 1367.0000,236.0000,-49.0000>>
//			fHeading = -115.0000
//		BREAK
//		CASE 12
//			vLoc = << 1367.0000,229.0000,-49.0000>>
//			fHeading = -115.0000
//		BREAK
//		CASE 13
//			vLoc = << 1367.0000,224.0000,-49.0000>>
//			fHeading = -115.0000
//		BREAK
//		CASE 14
//			vLoc = << 1367.0000,219.0000,-49.0000>>
//			fHeading = -115.0000
//		BREAK
//		CASE 15
//			vLoc = << 1367.5699,212.0000,-49.0000>>
//			fHeading = -115.0000
//		BREAK
//		CASE 16
//			vLoc = << 1367.0000,207.0000,-49.0000>>
//			fHeading = -115.0000
//		BREAK
//		CASE 17
//			vLoc = << 1367.0000,202.0000,-49.0000>>
//			fHeading = -115.0000
//		BREAK
//		CASE 18
//			vLoc = << 1367.0000,194.5000,-49.0000>>
//			fHeading = -90.0000
//		BREAK
//		CASE 19
//			vLoc = << 1393.0000,253.0000,-49.0000>>
//			fHeading = 115.0000
//		BREAK
//		CASE 20
//			vLoc = << 1393.0000,246.0000,-49.0000>>
//			fHeading = 115.0000
//		BREAK
//		CASE 21
//			vLoc = << 1393.0000,241.0000,-49.0000>>
//			fHeading = 115.0000
//		BREAK
//		CASE 22
//			vLoc = << 1393.0000,236.0000,-49.0000>>
//			fHeading = 115.0000
//		BREAK
//		CASE 23
//			vLoc = << 1393.0000,229.0000,-49.0000>>
//			fHeading = 115.0000
//		BREAK
//		CASE 24
//			vLoc = << 1393.0000,224.0000,-49.0000>>
//			fHeading = 115.0000
//		BREAK
//		CASE 25
//			vLoc = << 1393.0000,219.0000,-49.0000>>
//			fHeading = 115.0000
//		BREAK
//		CASE 26
//			vLoc = << 1393.0000,212.0000,-49.0000>>
//			fHeading = 115.0000
//		BREAK
//		CASE 27
//			vLoc = << 1393.0000,207.0000,-49.0000>>
//			fHeading = 115.0000
//		BREAK
//		CASE 28
//			vLoc = << 1393.0000,202.0000,-49.0000>>
//			fHeading = 115.0000
//		BREAK
//		CASE 29
//			vLoc = << 1393.0000,194.5000,-49.0000>>
//			fHeading = 90.0000
//		BREAK
	ENDSWITCH
ENDPROC

PROC REMOVE_PLAYERS_FROM_YOUR_PERSONAL_VEHICLE()
	PLAYER_INDEX tempPlayer	
	INT i
	IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
		IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehPersonalVehicle)	
		AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInID = -1
		AND NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
			IF NOT IS_VEHICLE_EMPTY(MPGlobalsAmbience.vehPersonalVehicle, TRUE, TRUE)
			AND NOT bPersonalVehicleCleanupDontLeaveVehicle
				IF NOT IS_PLAYER_ENTERING_PROPERTY(PLAYER_ID())
					PRINTLN("[personal_vehicle] REMOVE_PLAYERS_FROM_YOUR_PERSONAL_VEHICLE: Kicking remote players out of old player vehicle")
					IF ABSI(GET_TIME_DIFFERENCE(pv_RemovePlayersTimer, GET_NETWORK_TIME())) >= 1000
						REPEAT NUM_NETWORK_PLAYERS i
							tempPlayer = INT_TO_PLAYERINDEX(i)
							IF IS_NET_PLAYER_OK(tempPlayer, FALSE)
								IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(tempPlayer), MPGlobalsAmbience.vehPersonalVehicle)
									BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(tempPlayer), TRUE, 0, 0, FALSE, FALSE)
								ENDIF
							ENDIF
						ENDREPEAT
						pv_RemovePlayersTimer = GET_NETWORK_TIME()
					ENDIF
				ELSE
					PRINTLN("[personal_vehicle] REMOVE_PLAYERS_FROM_YOUR_PERSONAL_VEHICLE: NOT Kicking entering garage")
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CASINO_CAR_PARK_REQUESTS_PRE()
	IF GlobalServerBD.g_iAssignedCasinoParkingSpots[g_iCasinoCarParkRequestCounter] < 0
		IF g_iCasinoCarParkFirstEmpty = -1
			g_iCasinoCarParkFirstEmpty = g_iCasinoCarParkRequestCounter
		ENDIF
	ENDIF
ENDPROC

PROC CASINO_CAR_PARK_REQUESTS_PLAYER_LOOP(INT iPlayer)
	IF g_iCasinoCarParkFirstEmpty >= 0
	AND GlobalServerBD.g_iAssignedCasinoParkingSpots[g_iCasinoCarParkFirstEmpty] < 0
		IF IS_BIT_SET(GlobalplayerBD[iPlayer].iBSTwo, BSTWO_REQUEST_CASINO_CAR_PARK)
		AND NOT IS_BIT_SET(GlobalServerBD.g_iBSCasinoParkingAssigned, iPlayer)
			GlobalServerBD.g_iAssignedCasinoParkingSpots[g_iCasinoCarParkFirstEmpty] = iPlayer
			
			SET_BIT(GlobalServerBD.g_iBSCasinoParkingAssigned, iPlayer)
			
			#IF IS_DEBUG_BUILD
			PRINTLN("MAINTAIN_CASINO_CAR_PARK_REQUESTS: assigning car park slot #",g_iCasinoCarParkFirstEmpty," to player Number: ", iPlayer)
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CASINO_CAR_PARK_REQUESTS_POST()
	BOOL bKeepRequest
	
	PLAYER_INDEX playerID
	
	IF GlobalServerBD.g_iAssignedCasinoParkingSpots[g_iCasinoCarParkRequestCounter] >= 0
		playerID = INT_TO_PLAYERINDEX(GlobalServerBD.g_iAssignedCasinoParkingSpots[g_iCasinoCarParkRequestCounter])
		
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
			IF IS_BIT_SET(GlobalplayerBD[GlobalServerBD.g_iAssignedCasinoParkingSpots[g_iCasinoCarParkRequestCounter]].iBSTwo, BSTWO_REQUEST_CASINO_CAR_PARK)
				bKeepRequest = TRUE
			ENDIF
		ENDIF
		
		IF NOT bKeepRequest
			#IF IS_DEBUG_BUILD
			IF IS_NET_PLAYER_OK(playerID, FALSE)
				PRINTLN("MAINTAIN_CASINO_CAR_PARK_REQUESTS: clearing car park slot #",g_iCasinoCarParkRequestCounter," of player name: ",GET_PLAYER_NAME(playerID)," player Number: ",GlobalServerBD.g_iAssignedCasinoParkingSpots[g_iCasinoCarParkRequestCounter] )
			ELSE
				PRINTLN("MAINTAIN_CASINO_CAR_PARK_REQUESTS: clearing car park slot #",g_iCasinoCarParkRequestCounter," of player #",GlobalServerBD.g_iAssignedCasinoParkingSpots[g_iCasinoCarParkRequestCounter])
			ENDIF
			#ENDIF
			
			CLEAR_BIT(GlobalServerBD.g_iBSCasinoParkingAssigned, GlobalServerBD.g_iAssignedCasinoParkingSpots[g_iCasinoCarParkRequestCounter])
			
			GlobalServerBD.g_iAssignedCasinoParkingSpots[g_iCasinoCarParkRequestCounter] = -1
		ENDIF
	ENDIF
	
	g_iCasinoCarParkRequestCounter++
	
	IF g_iCasinoCarParkRequestCounter >= NUM_NETWORK_PLAYERS
		g_iCasinoCarParkRequestCounter = 0
		g_iCasinoCarParkFirstEmpty = -1
	ENDIF
ENDPROC

FUNC INT GET_CASINO_CAR_PARKING_SPOT_FROM_SERVER()
	INT iSpot
	#IF IS_DEBUG_BUILD
	IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_REQUEST_CASINO_CAR_PARK)
		PRINTLN("GET_CASINO_CAR_PARKING_SPOT_FROM_SERVER: called this frame")
	ENDIF
	#ENDIF
	#IF IS_DEBUG_BUILD
	IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_PARK)
		PRINTLN("Set bit (GlobalplayerBD[PLAYER_ID()].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_PARK)")
	ENDIF
	#ENDIF
	SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_PARK)
	SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_REQUEST_CASINO_CAR_PARK)	
	IF IS_BIT_SET(GlobalServerBD.g_iBSCasinoParkingAssigned,NATIVE_TO_INT(PLAYER_ID()))
		REPEAT NUM_NETWORK_PLAYERS iSpot
			IF GlobalServerBD.g_iAssignedCasinoParkingSpots[iSpot] = NATIVE_TO_INT(PLAYER_ID())
				RETURN iSpot
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN -1
ENDFUNC

PROC WARP_INTO_CASINO_VALET_VEHICLE_RESET()
	IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehPersonalVehicle)
	AND IS_VEHICLE_DRIVEABLE(MPGlobalsAmbience.vehPersonalVehicle)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehPersonalVehicle)
			SET_VEHICLE_HANDBRAKE(MPGlobalsAmbience.vehPersonalVehicle,FALSE)
			//MODIFY_VEHICLE_TOP_SPEED(MPGlobalsAmbience.vehPersonalVehicle,0)
			//SET_VEHICLE_MAX_SPEED(MPGlobalsAmbience.vehPersonalVehicle,-1)
			SET_ENTITY_CAN_BE_DAMAGED(MPGlobalsAmbience.vehPersonalVehicle,TRUE)
			SET_VEHICLE_RADIO_ENABLED(MPGlobalsAmbience.vehPersonalVehicle, TRUE)
			MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark = 0
			PRINTLN("WARP_INTO_CASINO_VALET_VEHICLE_RESET: finished reset")
		ELSE
			PRINTLN("WARP_INTO_CASINO_VALET_VEHICLE_RESET: getting control over the vehicle")
			NETWORK_REQUEST_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehPersonalVehicle)
		ENDIF
	ELSE
		MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark = 0
	ENDIF
ENDPROC

PROC CLEAR_WARP_INTO_CASINO_VALET_CAR_PARK()
	IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE)
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_REQUEST_CASINO_CAR_PARK)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_PARK)
		PRINTLN("Clear bit (GlobalplayerBD[PLAYER_ID()].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_PARK)")
	ENDIF
	#ENDIF
	CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_PARK)
	SET_BIT(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_RESET)
	WARP_INTO_CASINO_VALET_VEHICLE_RESET()
	//DEBUG_PRINTCALLSTACK()
	//PRINTLN("CLEAR_WARP_INTO_CASINO_VALET_CAR_PARK: called this frame")
ENDPROC

PROC INIT_WARP_INTO_CASINO_VALET_CAR_PARK(BOOL bEmptyVeh)
	SET_BIT(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_WARP_IN)
	IF bEmptyVeh
		SET_BIT(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_EMPTY_VEH)
	ELSE
		CLEAR_BIT(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_EMPTY_VEH)
	ENDIF
	DEBUG_PRINTCALLSTACK()
	PRINTLN("INIT_WARP_INTO_CASINO_VALET_CAR_PARK: called this frame with bEmptyVeh = ",bEmptyVeh)
ENDPROC

FUNC BOOL HAS_PLAYER_STARTED_WARP_INTO_CASINO_VALET_CAR_PARK()
	RETURN IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_WARP_IN)
ENDFUNC

PROC CREATE_COLLISION_FOR_CASINO_VALET_CARPARK()
	IF NOT DOES_ENTITY_EXIST(MPGlobals.VehicleData.casinoValetGarCollision)
		IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES,(GET_HASH_KEY("vw_Prop_VW_Garage_Coll_01a"))))
			MPGlobals.VehicleData.casinoValetGarCollision = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES,(GET_HASH_KEY("vw_Prop_VW_Garage_Coll_01a"))),<<1379.2000, 199.4600, -50.85>>,FALSE,FALSE)
			FREEZE_ENTITY_POSITION(MPGlobals.VehicleData.casinoValetGarCollision,TRUE)
			SET_ENTITY_CAN_BE_DAMAGED(MPGlobals.VehicleData.casinoValetGarCollision,FALSE)
			SET_ENTITY_VISIBLE(MPGlobals.VehicleData.casinoValetGarCollision,FALSE)
			SET_ENTITY_PROOFS(MPGlobals.VehicleData.casinoValetGarCollision,TRUE,TRUE,TRUE,FALSE,TRUE,TRUE,TRUE,TRUE)

			//SET_ENTITY_ROTATION(MPGlobals.VehicleData.casinoValetGarCollision,<<0,0,0>>)
			PRINTLN("CREATE_COLLISION_FOR_CASINO_VALET_CARPARK: creating collision beneath valet garage.")
		ELSE
			PRINTLN("CREATE_COLLISION_FOR_CASINO_VALET_CARPARK: collision beneath valet garage waiting for model to be loaded..")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CASINO_CAR_PARK_CHECK_IF_OK_TO_WARP_VEHICLE(VECTOR vCreationPos)
	FLOAT fdistance
	
	INT iLoop
	
	PLAYER_INDEX playerID
	
	VECTOR vPlayerPos

	IF NOT HAS_NET_TIMER_STARTED(MPGlobals.VehicleData.stCasinoCarParkAreaBlocked)
		START_NET_TIMER(MPGlobals.VehicleData.stCasinoCarParkAreaBlocked, TRUE)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(MPGlobals.VehicleData.stCasinoCarParkAreaBlocked, 20000, TRUE)
			PRINTLN("CASINO_CAR_PARK_CHECK_IF_OK_TO_WARP_VEHICLE: hit timeout proceeding")
			RETURN TRUE
		ENDIF
	ENDIF

	REPEAT NUM_NETWORK_PLAYERS iLoop
		playerID = INT_TO_PLAYERINDEX(iLoop)
		
		IF IS_NET_PLAYER_OK(playerID)
			IF IS_PLAYER_IN_CASINO_VALET_GARAGE(playerID)
				IF NOT IS_THIS_PLAYER_ACTIVE_IN_CORONA(playerID)
				AND NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID))
				AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(playerID)
					vPlayerPos = GET_PLAYER_COORDS(playerID)
					fdistance = GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vCreationPos, FALSE)
					
					IF fDistance < 5
						PRINTLN("CASINO_CAR_PARK_CHECK_IF_OK_TO_WARP_VEHICLE - Player ", iLoop, " is in the way.")
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_WARPS_FOR_CASINO_CAR_PARK()
	VECTOR vLoc
	FLOAT fHeading
	
	
	IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
		IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_RESET)
			WARP_INTO_CASINO_VALET_VEHICLE_RESET()
		ELIF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_WARP_IN)
			PRINTLN("MAINTAIN_WARPS_FOR_CASINO_CAR_PARK: trying to warp player's vehicle inside")
			IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehPersonalVehicle)	
			AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInID = -1
			AND NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
				IF NOT IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_EMPTY_VEH)
				OR IS_VEHICLE_EMPTY(MPGlobalsAmbience.vehPersonalVehicle, TRUE, TRUE)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehPersonalVehicle)
						IF NOT IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_FADE_VEH)
							NETWORK_FADE_OUT_ENTITY(MPGlobalsAmbience.vehPersonalVehicle, FALSE, TRUE)	
							SET_BIT(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_FADE_VEH)
						ELSE
							INT iSpot = GET_CASINO_CAR_PARKING_SPOT_FROM_SERVER()
							IF iSpot > -1
							AND NOT NETWORK_IS_ENTITY_FADING(MPGlobalsAmbience.vehPersonalVehicle)
								PRINTLN("MAINTAIN_WARPS_FOR_CASINO_CAR_PARK: trying to warp player's personal vehicle into shared carpark spot #", iSpot)
								GET_CASINO_PARKING_SPOT_LOCATION(iSpot,vLoc, fHeading)
								IF CASINO_CAR_PARK_CHECK_IF_OK_TO_WARP_VEHICLE(vLoc)
									PRINTLN("MAINTAIN_WARPS_FOR_CASINO_CAR_PARK: warping player's personal vehicle into shared carpark spot #", iSpot)
									SET_ENTITY_COORDS(MPGlobalsAmbience.vehPersonalVehicle,vLoc)
									SET_ENTITY_HEADING(MPGlobalsAmbience.vehPersonalVehicle,fHeading)
									PRINTLN("MAINTAIN_WARPS_FOR_CASINO_CAR_PARK: warping player's personal vehicle to ", vLoc)
									FREEZE_ENTITY_POSITION(MPGlobalsAmbience.vehPersonalVehicle,FALSE)
									PRINTLN("MAINTAIN_WARPS_FOR_CASINO_CAR_PARK: unfreezing before setting to be frozen waiting on collision")
									SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(MPGlobalsAmbience.vehPersonalVehicle,TRUE)
									SET_VEHICLE_ON_GROUND_PROPERLY(MPGlobalsAmbience.vehPersonalVehicle,1)
									PRINTLN("MAINTAIN_WARPS_FOR_CASINO_CAR_PARK: warping player's personal vehicle to ", vLoc, " after on ground")
									SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(MPGlobalsAmbience.vehPersonalVehicle,FALSE)
									SET_VEHICLE_FIXED(MPGlobalsAmbience.vehPersonalVehicle) 
							        SET_ENTITY_HEALTH(MPGlobalsAmbience.vehPersonalVehicle, 1000) 
							        SET_VEHICLE_ENGINE_HEALTH(MPGlobalsAmbience.vehPersonalVehicle, 1000) 
							        SET_VEHICLE_PETROL_TANK_HEALTH(MPGlobalsAmbience.vehPersonalVehicle, 1000) 
							        SET_VEHICLE_DIRT_LEVEL(MPGlobalsAmbience.vehPersonalVehicle,0) 
									SET_ENTITY_CAN_BE_DAMAGED(MPGlobalsAmbience.vehPersonalVehicle,FALSE)
									//SET_VEHICLE_ENGINE_ON(MPGlobalsAmbience.vehPersonalVehicle,FALSE,TRUE)
									SET_VEHICLE_RADIO_ENABLED(MPGlobalsAmbience.vehPersonalVehicle, FALSE)
									SET_BIT(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_RADIO_OFF)
									//SET_VEHICLE_MAX_SPEED(MPGlobalsAmbience.vehPersonalVehicle,-1)
									SET_BIT(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_SET_FLAGS)
									//MODIFY_VEHICLE_TOP_SPEED(MPGlobalsAmbience.vehPersonalVehicle,-100)
									IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_FADE_VEH)
										NETWORK_FADE_IN_ENTITY(MPGlobalsAmbience.vehPersonalVehicle, TRUE, FALSE)	
									ENDIF
									SET_ENTITY_COLLISION(MPGlobalsAmbience.vehPersonalVehicle, TRUE)
									SET_VEHICLE_LIGHTS(MPGlobalsAmbience.vehPersonalVehicle, SET_VEHICLE_LIGHTS_OFF)
									CLEAR_BIT(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_FADE_VEH)
									CLEAR_BIT(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_WARP_IN)
									SET_BIT(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_VEH_IN)
									SET_VEHICLE_HANDBRAKE(MPGlobalsAmbience.vehPersonalVehicle,TRUE)
									CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_PARK)
									PRINTLN("Clear bit (GlobalplayerBD[PLAYER_ID()].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_PARK)")
									RESET_NET_TIMER(MPGlobals.VehicleData.stCasinoCarParkAreaBlocked)
									IF NOT IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(),MPGlobalsAmbience.vehPersonalVehicle)
										SET_VEHICLE_ENGINE_ON(MPGlobalsAmbience.vehPersonalVehicle,FALSE,TRUE)	
									ENDIF
								ELSE
									PRINTLN("MAINTAIN_WARPS_FOR_CASINO_CAR_PARK: waiting for spot to be empty #", iSpot)
								ENDIF
							ELSE
								IF iSpot = -1
									PRINTLN("MAINTAIN_WARPS_FOR_CASINO_CAR_PARK: waiting for spot from server")
								ENDIF
							ENDIF
						ENDIF
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehPersonalVehicle)
						PRINTLN("MAINTAIN_WARPS_FOR_CASINO_CAR_PARK: trying to get control of vehicle")
					ENDIF
				ELSE
					REMOVE_PLAYERS_FROM_YOUR_PERSONAL_VEHICLE()
				ENDIF
			ELSE
				PRINTLN("MAINTAIN_WARPS_FOR_CASINO_CAR_PARK: vehicle does not exist, or is being cleaned up/concealed")
				CLEAR_WARP_INTO_CASINO_VALET_CAR_PARK()
			ENDIF
		ELSE
			IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_VEH_IN)
				VECTOR vCurrentLoc = GET_ENTITY_COORDS(MPGlobalsAmbience.vehPersonalVehicle)
				IF vCurrentLoc.z > -30
					PRINTLN("MAINTAIN_WARPS_FOR_CASINO_CAR_PARK: vehicle no longer in garage clear flag")
					CLEAR_WARP_INTO_CASINO_VALET_CAR_PARK()
				ENDIF
			ENDIF
			
//			IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_WARP_OUT)
//				MPGlobals.VehicleData.vOverrideWarpRequestCoord	= <<917.7304, 49.7081, 79.7648>>
//				IF WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY()
//					CLEAR_BIT(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_WARP_OUT)
//					CLEAR_BIT(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_VEH_IN)
//					SET_VEHICLE_HANDBRAKE(MPGlobalsAmbience.vehPersonalVehicle,FALSE)
//					CLEAR_WARP_INTO_CASINO_VALET_CAR_PARK()
//					//FREEZE_ENTITY_POSITION(MPGlobalsAmbience.vehPersonalVehicle,FALSE)
//				ENDIF
//			ENDIF
		ENDIF
	ENDIF
ENDPROC					
#ENDIF
/// PURPOSE:
///    To check older vehicle models for being reward vehicles to apply insurance on PC
/// PARAMS:
///    iSaveGameID - 
/// RETURNS:
///    
FUNC BOOL WAS_VEHICLE_A_LUCKY_WHEEL_REWARD_VEHICLE(INT iSaveGameID)
	SWITCH g_MpSavedVehicles[iSaveGameID].vehicleSetupMP.VehicleSetup.eModel
		CASE THRAX
		CASE PARAGON2
		CASE TURISMO2
		CASE JESTER3
		CASE INFERNUS2
		CASE SCHLAGEN
		CASE TAIPAN
		CASE NERO
		CASE GAUNTLET3
		CASE STAFFORD
		CASE MAMBA
		CASE SWINGER
		CASE DEVESTE
		CASE LOCUST
		CASE CARACARA2
		CASE SANCTUS
		CASE NEO
		CASE STROMBERG
		CASE KRIEGER
		CASE GAUNTLET4
		CASE FLASHGT
			RETURN TRUE
		BREAK
		
		CASE DODO
		CASE TURISMOR
		CASE TOROS
		CASE MONROE
		CASE VELUM
		CASE FUROREGT
		CASE INFERNUS
		//CASE INFERNUS2 covered above
		CASE RUSTON
		CASE MARQUIS
		CASE DEFILER
		CASE PIGALLE
		CASE MASSACRO2
		CASE TORNADO6
		CASE TAMPA
		CASE ISSI3
		CASE JB700
		CASE JESTER2
		CASE KAMACHO
		CASE FAGALOA
		CASE IMPALER
		CASE DOMINATOR2
		CASE MAMMATUS
		CASE JETMAX
		CASE STALION2
		CASE MASSACRO
		CASE ESSKEY
		CASE COGNOSCENTI
		CASE XLS
		CASE SUPERD
		CASE STUNT
		CASE CUBAN800
		CASE VACCA
		CASE JESTER
		CASE GAUNTLET2
		CASE EXEMPLAR
		CASE SLAMVAN2
		CASE GLENDALE
		CASE SQUALO
		CASE CARBONIZZARE
		CASE VIRGO
		CASE COGCABRIO
		CASE DINGHY3
		CASE BLADE
		CASE BULLET
		CASE BRIOSO
		CASE VOLTIC
		CASE VOLTIC2
		CASE ALPHA
		CASE FELTZER2
		CASE CHEBUREK
		CASE RAPIDGT2
		CASE RHAPSODY
		CASE COQUETTE
		CASE COQUETTE2  
		CASE RAPIDGT
		CASE NINEF2
		CASE KURUMA
		CASE NINEF
		CASE SCHAFTER3
		CASE SURANO
		CASE BANSHEE
		CASE KHAMELION
		CASE NIGHTBLADE
		CASE CHINO
		CASE DIABLOUS
		CASE VIRGO3
		CASE FCR
		CASE COMET2
		CASE ZOMBIEA
		CASE FELON2
		CASE BF400
		CASE FELON
		CASE BALLER2
		CASE HOTKNIFE
		CASE MESA3
		CASE PANTO
		CASE HAKUCHOU
		CASE BLAZER4
		CASE F620
		CASE ORACLE2
		CASE BIFTA
		CASE STALION
		CASE BLAZER3
		CASE MANCHEZ
		CASE SCHAFTER2
		CASE ZION2
		CASE DUKES
		CASE JACKAL
		CASE SERRANO
		CASE PATRIOT
		CASE FQ2
		CASE SLAMVAN
		CASE ENDURO
		CASE FAGGIO
		CASE BLISTA2
		CASE CARBONRS
		CASE KALAHARI  
		CASE RATLOADER2
		CASE FACTION
		CASE BUFFALO
		CASE DOMINATOR
		CASE MOONBEAM
		CASE GAUNTLET
		CASE TORNADO
		CASE BUCCANEER
		CASE PRAIRIE
		CASE PENUMBRA
		CASE FUGITIVE
		CASE ISSI2
		CASE SEASHARK
		CASE INTRUDER
		CASE BAGGER
		CASE BFINJECTION
		CASE HEXER
		CASE WASHINGTON
		CASE BATI
		CASE BATI2
		CASE SABREGT
		CASE SULTAN
		CASE NEMESIS
		CASE DOUBLE
		CASE TRIBIKE
		CASE TRIBIKE2
		CASE TRIBIKE3
		CASE RUFFIAN
		CASE RUINER
		CASE PICADOR
		CASE AKUMA
		CASE PCJ
		CASE PRIMO
		CASE FUTO
		CASE BLAZER
		CASE SANCHEZ2
		CASE SANCHEZ
		CASE VOODOO2
		CASE FAGGIO2
		CASE SCORCHER
		CASE CRUISER
		CASE BMX
		CASE MINIVAN
			IF IS_BIT_SET(g_MpSavedVehicles[iSaveGameID].iVehicleBS, MP_SAVED_VEHICLE_FREE_VEHICLE)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//url:bugstar:5900624 - [BG SCRIPT][PUBLIC][REPORTED][PC ONLY] Casino Minigames - Lucky Wheel - There have been reports of players losing the Thrax podium vehicle after winning on the lucky wheel and then rebooting.
PROC FIX_FOR_5900624()
	IF IS_PC_VERSION()
		IF WAS_VEHICLE_A_LUCKY_WHEEL_REWARD_VEHICLE(g_iPCVehInsuranceCounter)
			
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(g_MpSavedVehicles[g_iPCVehInsuranceCounter].iVehicleBS, MP_SAVED_VEHICLE_INSURED)
				PRINTLN("FIX_FOR_5900624: setting save slot ",g_iPCVehInsuranceCounter," to have insurance on PC as it is a reward vehicle")
			ENDIF
			#ENDIF
			SET_BIT(g_MpSavedVehicles[g_iPCVehInsuranceCounter].iVehicleBS, MP_SAVED_VEHICLE_INSURED)
			
		ENDIF
		g_iPCVehInsuranceCounter++
		IF g_iPCVehInsuranceCounter >= MAX_MP_SAVED_VEHICLES
			g_iPCVehInsuranceCounter = 0
		ENDIF
	ENDIF
ENDPROC

#IF FEATURE_TUNER

// PUBLIC CAR MEET

PROC GET_CAR_MEET_PARKING_SPOT_LOCATION(INT iLocation, VECTOR &vLoc, FLOAT &fHeading)
	IF iLocation < 0
	OR iLocation >= 32
		PRINTLN("GET_CAR_MEET_PARKING_SPOT_LOCATION: INVALID LOCATION! iLocation = ", iLocation)
		
		SCRIPT_ASSERT("GET_CAR_MEET_PARKING_SPOT_LOCATION: INVALID LOCATION!")
	ENDIF
	
	SWITCH iLocation
		CASE 0 // 0
			vLoc = <<-2207.2051, 1082.8390, -23.2450>>
			fHeading = 89.8165
		BREAK
		CASE 1 // 6
			vLoc = <<-2207.2764, 1119.3687, -23.2458>>
			fHeading = 94.1574
		BREAK
		CASE 2 // 10
			vLoc = <<-2200.1663, 1129.3817, -23.2459>>
			fHeading = 266.2454
		BREAK
		CASE 3 // 1
			vLoc = <<-2200.1646, 1122.7738, -23.2458>>
			fHeading = 274.9218
		BREAK
		CASE 4 // 9
			vLoc = <<-2199.8408, 1114.1814, -23.2457>>
			fHeading = 289.5377
		BREAK
		CASE 5 // 8
			vLoc = <<-2199.9680, 1085.9100, -23.2449>>
			fHeading = 271.1524
		BREAK
		CASE 6 // 11
			vLoc = <<-2187.2437, 1086.5090, -23.2459>>
			fHeading = 89.2808
		BREAK
		CASE 7 // 12
			vLoc = <<-2187.2229, 1096.2552, -23.2459>>
			fHeading = 90.0973
		BREAK
		CASE 8 // 2
			vLoc = <<-2187.2209, 1109.4031, -23.2460>>
			fHeading = 90.0861
		BREAK
		CASE 9 // 13
			vLoc = <<-2187.2178, 1113.3162, -23.2460>>
			fHeading = 89.9458
		BREAK
		CASE 10 // 14
			vLoc = <<-2187.2432, 1123.4980, -23.2461>>
			fHeading = 87.9961
		BREAK
		CASE 11 // 15
			vLoc = <<-2187.2666, 1133.6580, -23.2462>>
			fHeading = 92.7388
		BREAK
		CASE 12 // 16
			vLoc = <<-2187.3206, 1137.5746, -23.2463>>
			fHeading = 97.1167
		BREAK
		CASE 13 // 5
			vLoc = <<-2177.9417, 1129.5597, -24.3591>>
			fHeading = 263.7643
		BREAK
		CASE 14 // 18
			vLoc = <<-2178.0034, 1122.8582, -24.3587>>
			fHeading = 272.4356
		BREAK
		CASE 15 // 17
			vLoc = <<-2177.9775, 1119.2531, -24.3585>>
			fHeading = 267.4824
		BREAK
		CASE 16 // 3
			vLoc = <<-2177.5938, 1089.4301, -24.3521>>
			fHeading = 265.5634
		BREAK
		CASE 17 // 22
			vLoc = <<-2178.0244, 1085.9397, -24.3524>>
			fHeading = 269.9026
		BREAK
		CASE 18 // 19
			vLoc = <<-2165.5347, 1086.2345, -24.3499>>
			fHeading = 90.0032
		BREAK
		CASE 19 // 20
			vLoc = <<-2165.5386, 1092.8810, -24.3507>>
			fHeading = 89.3829
		BREAK
		CASE 20 // 31
			vLoc = <<-2165.3418, 1119.4221, -24.3580>>
			fHeading = 94.8420
		BREAK
		CASE 21 // 23
			vLoc = <<-2158.2222, 1116.1670, -24.3579>>
			fHeading = 270.0000
		BREAK
		CASE 22 // 4
			vLoc = <<-2157.9243, 1096.0303, -24.3511>>
			fHeading = 271.1076
		BREAK
		CASE 23 // 21
			vLoc = <<-2158.1719, 1082.7079, -24.3496>>
			fHeading = 273.5204
		BREAK
		CASE 24 // 24
			vLoc = <<-2145.0786, 1081.7378, -24.3508>>
			fHeading = 90.1881
		BREAK
		CASE 25 // 25
			vLoc = <<-2144.8984, 1085.2372, -24.3513>>
			fHeading = 90.8336
		BREAK
		CASE 26 // 26
			vLoc = <<-2144.9094, 1094.9059, -24.3526>>
			fHeading = 88.7821
		BREAK
		CASE 27 // 27
			vLoc = <<-2144.8303, 1098.4979, -24.3613>>
			fHeading = 90.1619
		BREAK
		CASE 28 // 7
			vLoc = <<-2144.9502, 1129.4254, -24.3583>>
			fHeading = 89.4786
		BREAK
		CASE 29 // 28
			vLoc = <<-2145.0723, 1133.0488, -24.3583>>
			fHeading = 90.7479
		BREAK
		CASE 30 // 29
			vLoc = <<-2145.0581, 1139.9121, -24.3584>>
			fHeading = 90.0032
		BREAK
		CASE 31 // 30
			vLoc = <<-2144.8838, 1143.4117, -24.3583>>
			fHeading = 90.7666
		BREAK
	ENDSWITCH
ENDPROC

PROC REMOVE_PLAYERS_FROM_YOUR_CAR_MEET_PERSONAL_VEHICLE()
	PLAYER_INDEX tempPlayer
	
	IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
		IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehPersonalVehicle)
		AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInID = -1
		AND NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
			IF NOT IS_VEHICLE_EMPTY(MPGlobalsAmbience.vehPersonalVehicle, TRUE, TRUE)
			AND NOT bPersonalVehicleCleanupDontLeaveVehicle
				IF NOT IS_PLAYER_ENTERING_PROPERTY(PLAYER_ID())
					PRINTLN("[personal_vehicle] REMOVE_PLAYERS_FROM_YOUR_CAR_MEET_PERSONAL_VEHICLE: Kicking remote players out of old player vehicle")
					
					IF ABSI(GET_TIME_DIFFERENCE(pv_RemovePlayersTimer, GET_NETWORK_TIME())) >= 1000
						INT i
						REPEAT NUM_NETWORK_PLAYERS i
							tempPlayer = INT_TO_PLAYERINDEX(i)
							
							IF IS_NET_PLAYER_OK(tempPlayer, FALSE)
								IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(tempPlayer), MPGlobalsAmbience.vehPersonalVehicle)
									BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(tempPlayer), TRUE, 0, 0, FALSE, FALSE)
								ENDIF
							ENDIF
						ENDREPEAT
						
						pv_RemovePlayersTimer = GET_NETWORK_TIME()
					ENDIF
				ELSE
					PRINTLN("[personal_vehicle] REMOVE_PLAYERS_FROM_YOUR_CAR_MEET_PERSONAL_VEHICLE: NOT Kicking entering garage")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC NON_RANDOM_CAR_MEET_CAR_PARK_REQUESTS_PRE()
	IF GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[g_iCarMeetCarParkRequestCounter] < 0
		IF g_iCarMeetCarParkFirstEmpty = -1
			g_iCarMeetCarParkFirstEmpty = g_iCarMeetCarParkRequestCounter
		ENDIF
	ENDIF
ENDPROC

PROC NON_RANDOM_CAR_MEET_CAR_PARK_REQUESTS_PLAYER_LOOP(INT iPlayer)
	IF g_iCarMeetCarParkFirstEmpty >= 0
	AND GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[g_iCarMeetCarParkFirstEmpty] < 0
		IF IS_BIT_SET(GlobalplayerBD[iPlayer].iBSTwo, BSTWO_REQUEST_CAR_MEET_CAR_PARK)
			IF NOT IS_BIT_SET(GlobalServerBD_BlockC.g_iBSCarMeetParkingAssigned, iPlayer)
				GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[g_iCarMeetCarParkFirstEmpty] = iPlayer
				
				SET_BIT(GlobalServerBD_BlockC.g_iBSCarMeetParkingAssigned, iPlayer)
				
				#IF IS_DEBUG_BUILD
				PRINTLN("NON_RANDOM_CAR_MEET_CAR_PARK_REQUESTS_PLAYER_LOOP: assigning car park slot #", g_iCarMeetCarParkFirstEmpty, " to player Number: ", iPlayer)
				#ENDIF
				
				EXIT
			ELIF GlobalplayerBD[iPlayer].iRequestedCarMeetSpace != -1
			AND GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[GlobalplayerBD[iPlayer].iRequestedCarMeetSpace] != iPlayer
				INT i = 0
				REPEAT NUM_NETWORK_PLAYERS i
					IF GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[i] = iPlayer
						GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[i] = -1
					ENDIF
				ENDREPEAT
				
				GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[GlobalplayerBD[iPlayer].iRequestedCarMeetSpace] = iPlayer
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC NON_RANDOM_CAR_MEET_CAR_PARK_REQUESTS_POST()
	BOOL bKeepRequest
	
	PLAYER_INDEX playerID
	
	IF GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[g_iCarMeetCarParkRequestCounter] >= 0
		playerID = INT_TO_PLAYERINDEX(GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[g_iCarMeetCarParkRequestCounter])
		
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
			IF IS_BIT_SET(GlobalplayerBD[GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[g_iCarMeetCarParkRequestCounter]].iBSTwo, BSTWO_REQUEST_CAR_MEET_CAR_PARK)
				bKeepRequest = TRUE
			ENDIF
		ENDIF
		
		IF NOT bKeepRequest
			#IF IS_DEBUG_BUILD
			IF IS_NET_PLAYER_OK(playerID, FALSE)
				PRINTLN("NON_RANDOM_CAR_MEET_CAR_PARK_REQUESTS_POST: clearing car park slot #", g_iCarMeetCarParkRequestCounter, " of player name: ", GET_PLAYER_NAME(playerID), " player Number: ", GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[g_iCarMeetCarParkRequestCounter])
			ELSE
				PRINTLN("NON_RANDOM_CAR_MEET_CAR_PARK_REQUESTS_POST: clearing car park slot #", g_iCarMeetCarParkRequestCounter, " of player #", GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[g_iCarMeetCarParkRequestCounter])
			ENDIF
			#ENDIF
			
			CLEAR_BIT(GlobalServerBD_BlockC.g_iBSCarMeetParkingAssigned, GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[g_iCarMeetCarParkRequestCounter])
			
			GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[g_iCarMeetCarParkRequestCounter] = -1
		ENDIF
	ENDIF
	
	g_iCarMeetCarParkRequestCounter++
	
	IF g_iCarMeetCarParkRequestCounter >= NUM_NETWORK_PLAYERS
		g_iCarMeetCarParkRequestCounter = 0
		g_iCarMeetCarParkFirstEmpty = -1
	ENDIF
ENDPROC

PROC RANDOM_CAR_MEET_CAR_PARK_REQUESTS_PLAYER_LOOP(INT iPlayer)
	IF IS_BIT_SET(GlobalplayerBD[iPlayer].iBSTwo, BSTWO_REQUEST_CAR_MEET_CAR_PARK)
		IF NOT IS_BIT_SET(GlobalServerBD_BlockC.g_iBSCarMeetParkingAssigned, iPlayer)
			INT iRandomSpace = GET_RANDOM_INT_IN_RANGE(0, 32)
			
			WHILE (GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[iRandomSpace] != -1)
				iRandomSpace++
				
				IF iRandomSpace >= NUM_NETWORK_PLAYERS
					iRandomSpace = 0
				ENDIF
			ENDWHILE
			
			GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[iRandomSpace] = iPlayer
			
			SET_BIT(GlobalServerBD_BlockC.g_iBSCarMeetParkingAssigned, iPlayer)
			
			#IF IS_DEBUG_BUILD
			PRINTLN("RANDOM_CAR_MEET_CAR_PARK_REQUESTS_PLAYER_LOOP: assigning car park slot #", iRandomSpace, " to player Number: ", iPlayer)
			#ENDIF
		ELIF GlobalplayerBD[iPlayer].iRequestedCarMeetSpace != -1
		AND GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[GlobalplayerBD[iPlayer].iRequestedCarMeetSpace] != iPlayer
			INT i = 0
			REPEAT NUM_NETWORK_PLAYERS i
				IF GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[i] = iPlayer
					GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[i] = -1
				ENDIF
			ENDREPEAT
			
			GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[GlobalplayerBD[iPlayer].iRequestedCarMeetSpace] = iPlayer
		ENDIF
	ENDIF
ENDPROC

PROC RANDOM_CAR_MEET_CAR_PARK_REQUESTS_POST()
	BOOL bKeepRequest
	
	PLAYER_INDEX playerID
	
	IF GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[g_iCarMeetCarPlayerStagger] >= 0
		playerID = INT_TO_PLAYERINDEX(GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[g_iCarMeetCarPlayerStagger])
		
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
			IF IS_BIT_SET(GlobalplayerBD[GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[g_iCarMeetCarPlayerStagger]].iBSTwo, BSTWO_REQUEST_CAR_MEET_CAR_PARK)
				bKeepRequest = TRUE
			ENDIF
		ENDIF
		
		IF NOT bKeepRequest
			#IF IS_DEBUG_BUILD
			IF IS_NET_PLAYER_OK(playerID, FALSE)
				PRINTLN("RANDOM_CAR_MEET_CAR_PARK_REQUESTS_POST: clearing car park slot #", g_iCarMeetCarPlayerStagger, " of player name: ", GET_PLAYER_NAME(playerID), " player Number: ", GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[g_iCarMeetCarPlayerStagger])
			ELSE
				PRINTLN("RANDOM_CAR_MEET_CAR_PARK_REQUESTS_POST: clearing car park slot #", g_iCarMeetCarPlayerStagger, " of player #", GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[g_iCarMeetCarPlayerStagger])
			ENDIF
			#ENDIF
			
			CLEAR_BIT(GlobalServerBD_BlockC.g_iBSCarMeetParkingAssigned, GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[g_iCarMeetCarPlayerStagger])
			
			GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[g_iCarMeetCarPlayerStagger] = -1
		ENDIF
	ENDIF
	
	g_iCarMeetCarPlayerStagger++
	
	IF g_iCarMeetCarPlayerStagger >= NUM_NETWORK_PLAYERS
		g_iCarMeetCarPlayerStagger = 0
	ENDIF
ENDPROC

PROC CAR_MEET_CAR_PARK_REQUESTS_PRE()
	IF g_sMPTunables.bDisableRandomCarMeetSpaceAssignment
		NON_RANDOM_CAR_MEET_CAR_PARK_REQUESTS_PRE()
	ENDIF
ENDPROC

PROC CAR_MEET_CAR_PARK_REQUESTS_PLAYER_LOOP(INT iPlayer)
	IF g_sMPTunables.bDisableRandomCarMeetSpaceAssignment
		NON_RANDOM_CAR_MEET_CAR_PARK_REQUESTS_PLAYER_LOOP(iPlayer)
	ELSE
		RANDOM_CAR_MEET_CAR_PARK_REQUESTS_PLAYER_LOOP(iPlayer)
	ENDIF
ENDPROC

PROC CAR_MEET_CAR_PARK_REQUESTS_POST()
	
	IF g_sMPTunables.bDisableRandomCarMeetSpaceAssignment
		NON_RANDOM_CAR_MEET_CAR_PARK_REQUESTS_POST()
	ELSE
		RANDOM_CAR_MEET_CAR_PARK_REQUESTS_POST()
	ENDIF
ENDPROC

FUNC INT GET_CAR_MEET_CAR_PARKING_SPOT_FROM_SERVER()
	#IF IS_DEBUG_BUILD
	IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_REQUEST_CAR_MEET_CAR_PARK)
		PRINTLN("GET_CAR_MEET_CAR_PARKING_SPOT_FROM_SERVER: called this frame")
	ENDIF
	
	IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_MEET_CAR_PARK)
		PRINTLN("Set bit (GlobalplayerBD[PLAYER_ID()].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_MEET_CAR_PARK)")
	ENDIF
	#ENDIF
	
	SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_MEET_CAR_PARK)
	SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_REQUEST_CAR_MEET_CAR_PARK)
	
	IF IS_BIT_SET(GlobalServerBD_BlockC.g_iBSCarMeetParkingAssigned, NATIVE_TO_INT(PLAYER_ID()))
		INT iSpot
		REPEAT NUM_NETWORK_PLAYERS iSpot
			IF GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[iSpot] = NATIVE_TO_INT(PLAYER_ID())
				RETURN iSpot
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN -1
ENDFUNC

PROC SET_CONCEAL_CURRENT_PERSONAL_VEHICLE_IN_CAR_MEET()
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInPlayerOwnedLocation = -1
	
	SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CONCEAL_IN_PROPERTY)
	
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)
	
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInID = ENUM_TO_INT(SIMPLE_INTERIOR_CAR_MEET)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInType = PV_CONCEAL_TYPE_CAR_MEET
	
	SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CONCEAL_IN_PROPERTY)
	
	PRINTLN("SET_CONCEAL_CURRENT_PERSONAL_VEHICLE_IN_CAR_MEET: setting current personal vehicle to be concealed in car meet")
ENDPROC

PROC CLEAR_CONCEAL_CURRENT_PERSONAL_VEHICLE_IN_CAR_MEET()
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInType = PV_CONCEAL_TYPE_CAR_MEET
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInID = -1
		
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInType = -1
		
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInPlayerOwnedLocation = -1
		
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)
		
		FORCE_CONCEAL_MY_ACTIVE_PERSONAL_VEHICLE(FALSE)
		SET_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(FALSE)
		
		CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CONCEAL_IN_PROPERTY)
		
		PRINTLN("CLEAR_CONCEAL_CURRENT_PERSONAL_VEHICLE_IN_CAR_MEET: clearing conceal of personal vehicle")
	ENDIF
ENDPROC

PROC WARP_INTO_CAR_MEET_VEHICLE_RESET()
	IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehPersonalVehicle)
	AND IS_VEHICLE_DRIVEABLE(MPGlobalsAmbience.vehPersonalVehicle)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehPersonalVehicle)
			SET_VEHICLE_HANDBRAKE(MPGlobalsAmbience.vehPersonalVehicle, FALSE)
			SET_ENTITY_CAN_BE_DAMAGED(MPGlobalsAmbience.vehPersonalVehicle, TRUE)
			SET_VEHICLE_RADIO_ENABLED(MPGlobalsAmbience.vehPersonalVehicle, TRUE)
			
			MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark = 0
			
			PRINTLN("WARP_INTO_CAR_MEET_VEHICLE_RESET: finished reset")
		ELSE
			PRINTLN("WARP_INTO_CAR_MEET_VEHICLE_RESET: getting control over the vehicle")
			
			NETWORK_REQUEST_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehPersonalVehicle)
		ENDIF
	ELSE
		MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark = 0
	ENDIF
ENDPROC

PROC CLEAR_WARP_INTO_CAR_MEET_CAR_PARK()
	IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_REQUEST_CAR_MEET_CAR_PARK)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_MEET_CAR_PARK)
		PRINTLN("Clear bit (GlobalplayerBD[PLAYER_ID()].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_MEET_CAR_PARK)")
	ENDIF
	#ENDIF
	
	CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_MEET_CAR_PARK)
	
	SET_BIT(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_RESET)
	
	WARP_INTO_CAR_MEET_VEHICLE_RESET()
	
	CLEAR_CONCEAL_CURRENT_PERSONAL_VEHICLE_IN_CAR_MEET()
ENDPROC

PROC INIT_WARP_INTO_CAR_MEET_CAR_PARK(BOOL bEmptyVeh)
	SET_BIT(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_WARP_IN)
	
	IF bEmptyVeh
		SET_BIT(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_EMPTY_VEH)
	ELSE
		CLEAR_BIT(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_EMPTY_VEH)
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
	
	PRINTLN("INIT_WARP_INTO_CAR_MEET_CAR_PARK: called this frame with bEmptyVeh = ", bEmptyVeh)
ENDPROC

PROC CREATE_COLLISION_FOR_CAR_MEET_CARPARK()
	IF NOT DOES_ENTITY_EXIST(MPGlobals.VehicleData.carMeetGarCollision)
		IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("tr_prop_tr_meet_coll_01"))))
			MPGlobals.VehicleData.carMeetGarCollision = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("tr_prop_tr_meet_coll_01"))), <<-2000.000000, 1113.211426, -25.362434>>, FALSE, FALSE)
			
			FREEZE_ENTITY_POSITION(MPGlobals.VehicleData.carMeetGarCollision, TRUE)
			
			SET_ENTITY_CAN_BE_DAMAGED(MPGlobals.VehicleData.carMeetGarCollision, FALSE)
			SET_ENTITY_VISIBLE(MPGlobals.VehicleData.carMeetGarCollision, FALSE)
			SET_ENTITY_PROOFS(MPGlobals.VehicleData.carMeetGarCollision, TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE, TRUE)
			
			PRINTLN("CREATE_COLLISION_FOR_CAR_MEET_CARPARK: creating collision beneath car meet.")
		ELSE
			PRINTLN("CREATE_COLLISION_FOR_CAR_MEET_CARPARK: collision beneath car meet waiting for model to load.")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAR_MEET_CAR_PARK_CHECK_IF_OK_TO_WARP_VEHICLE(VECTOR vCreationPos)
	FLOAT fdistance
	
	PLAYER_INDEX playerID
	
	VECTOR vPlayerPos
	
	IF NOT HAS_NET_TIMER_STARTED(MPGlobals.VehicleData.stCarMeetCarParkAreaBlocked)
		START_NET_TIMER(MPGlobals.VehicleData.stCarMeetCarParkAreaBlocked)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(MPGlobals.VehicleData.stCarMeetCarParkAreaBlocked, 20000)
			PRINTLN("CAR_MEET_CAR_PARK_CHECK_IF_OK_TO_WARP_VEHICLE: hit timeout proceeding")
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	INT iLoop
	REPEAT NUM_NETWORK_PLAYERS iLoop
		playerID = INT_TO_PLAYERINDEX(iLoop)
		
		IF IS_NET_PLAYER_OK(playerID)
			IF IS_PLAYER_IN_CAR_MEET_PROPERTY(playerID)
				IF NOT IS_THIS_PLAYER_ACTIVE_IN_CORONA(playerID)
				AND NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID))
				AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(playerID)
					vPlayerPos = GET_PLAYER_COORDS(playerID)
					fdistance = GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vCreationPos, FALSE)
					
					IF fDistance < 5
						PRINTLN("CAR_MEET_CAR_PARK_CHECK_IF_OK_TO_WARP_VEHICLE - Player ", iLoop, " is in the way.")
						
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_WARPS_FOR_CAR_MEET()
	FLOAT fHeading
	
	VECTOR vLoc
	
	IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
	AND NOT MPGlobalsAmbience.bLaunchVehicleDropPersonal
		IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_RESET)
			WARP_INTO_CAR_MEET_VEHICLE_RESET()
		ELIF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_WARP_IN)
			PRINTLN("MAINTAIN_WARPS_FOR_CAR_MEET: trying to warp player's vehicle inside")
			
			IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehPersonalVehicle)
			AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInID = -1
			AND NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
				IF NOT IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_EMPTY_VEH)
				OR IS_VEHICLE_EMPTY(MPGlobalsAmbience.vehPersonalVehicle, TRUE, TRUE)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehPersonalVehicle)
						IF NOT IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_FADE_VEH)
							NETWORK_FADE_OUT_ENTITY(MPGlobalsAmbience.vehPersonalVehicle, FALSE, TRUE)
							
							SET_BIT(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_FADE_VEH)
						ELSE
							INT iSpot = GET_CAR_MEET_CAR_PARKING_SPOT_FROM_SERVER()
							
							IF iSpot > -1
							AND NOT NETWORK_IS_ENTITY_FADING(MPGlobalsAmbience.vehPersonalVehicle)
								PRINTLN("MAINTAIN_WARPS_FOR_CAR_MEET: trying to warp player's personal vehicle into shared carpark spot #", iSpot)
								
								GET_CAR_MEET_PARKING_SPOT_LOCATION(iSpot, vLoc, fHeading)
								
								IF CAR_MEET_CAR_PARK_CHECK_IF_OK_TO_WARP_VEHICLE(vLoc)
									PRINTLN("MAINTAIN_WARPS_FOR_CAR_MEET: warping player's personal vehicle into shared carpark spot #", iSpot)
									
									SET_ENTITY_COORDS_NO_OFFSET(MPGlobalsAmbience.vehPersonalVehicle, vLoc)
									SET_ENTITY_HEADING(MPGlobalsAmbience.vehPersonalVehicle, fHeading)
									
									PRINTLN("MAINTAIN_WARPS_FOR_CAR_MEET: warping player's personal vehicle to ", vLoc)
									
									FREEZE_ENTITY_POSITION(MPGlobalsAmbience.vehPersonalVehicle, FALSE)
									
									PRINTLN("MAINTAIN_WARPS_FOR_CAR_MEET: unfreezing before setting to be frozen waiting on collision")
									
									SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(MPGlobalsAmbience.vehPersonalVehicle, TRUE)
									
									SET_VEHICLE_ON_GROUND_PROPERLY(MPGlobalsAmbience.vehPersonalVehicle, 1)
									
									PRINTLN("MAINTAIN_WARPS_FOR_CAR_MEET: warping player's personal vehicle to ", vLoc, " after on ground")
									
									SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(MPGlobalsAmbience.vehPersonalVehicle, FALSE)
									SET_VEHICLE_FIXED(MPGlobalsAmbience.vehPersonalVehicle)
							        SET_ENTITY_HEALTH(MPGlobalsAmbience.vehPersonalVehicle, 1000)
							        SET_VEHICLE_ENGINE_HEALTH(MPGlobalsAmbience.vehPersonalVehicle, 1000)
							        SET_VEHICLE_PETROL_TANK_HEALTH(MPGlobalsAmbience.vehPersonalVehicle, 1000)
							        SET_VEHICLE_DIRT_LEVEL(MPGlobalsAmbience.vehPersonalVehicle, 0)
									SET_ENTITY_CAN_BE_DAMAGED(MPGlobalsAmbience.vehPersonalVehicle, FALSE)
									SET_VEHICLE_RADIO_ENABLED(MPGlobalsAmbience.vehPersonalVehicle, FALSE)
									
									SET_BIT(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_RADIO_OFF)
									SET_BIT(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_SET_FLAGS)
									
									IF GET_ENTITY_MODEL(MPGlobalsAmbience.vehPersonalVehicle) = DELUXO
									OR GET_ENTITY_MODEL(MPGlobalsAmbience.vehPersonalVehicle) = OPPRESSOR2
										SET_DISABLE_HOVER_MODE_FLIGHT(MPGlobalsAmbience.vehPersonalVehicle, TRUE)
										SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(MPGlobalsAmbience.vehPersonalVehicle, 0)
									ELIF GET_ENTITY_MODEL(MPGlobalsAmbience.vehPersonalVehicle) = STROMBERG
										TRANSFORM_TO_CAR(MPGlobalsAmbience.vehPersonalVehicle, TRUE)
									ENDIF
									
									IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_FADE_VEH)
										NETWORK_FADE_IN_ENTITY(MPGlobalsAmbience.vehPersonalVehicle, TRUE, FALSE)
									ENDIF
									
									SET_ENTITY_COLLISION(MPGlobalsAmbience.vehPersonalVehicle, TRUE)
									SET_VEHICLE_LIGHTS(MPGlobalsAmbience.vehPersonalVehicle, SET_VEHICLE_LIGHTS_OFF)
									
									CLEAR_BIT(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_FADE_VEH)
									CLEAR_BIT(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_WARP_IN)
									
									SET_BIT(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_VEH_IN)
									
									SET_VEHICLE_HANDBRAKE(MPGlobalsAmbience.vehPersonalVehicle, TRUE)
									
									CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_MEET_CAR_PARK)
									
									PRINTLN("Clear bit (GlobalplayerBD[PLAYER_ID()].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_MEET_CAR_PARK)")
									
									RESET_NET_TIMER(MPGlobals.VehicleData.stCarMeetCarParkAreaBlocked)
									
									IF NOT IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), MPGlobalsAmbience.vehPersonalVehicle)
										SET_VEHICLE_ENGINE_ON(MPGlobalsAmbience.vehPersonalVehicle, FALSE, TRUE)
									ENDIF
									
									SET_CONCEAL_CURRENT_PERSONAL_VEHICLE_IN_CAR_MEET()
								ELSE
									PRINTLN("MAINTAIN_WARPS_FOR_CAR_MEET: waiting for spot to be empty #", iSpot)
								ENDIF
							ELSE
								IF iSpot = -1
									PRINTLN("MAINTAIN_WARPS_FOR_CAR_MEET: waiting for spot from server")
								ENDIF
							ENDIF
						ENDIF
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehPersonalVehicle)
						
						PRINTLN("MAINTAIN_WARPS_FOR_CAR_MEET: trying to get control of vehicle")
					ENDIF
				ELSE
					REMOVE_PLAYERS_FROM_YOUR_CAR_MEET_PERSONAL_VEHICLE()
				ENDIF
			ELSE
				PRINTLN("MAINTAIN_WARPS_FOR_CAR_MEET: vehicle does not exist, or is being cleaned up/concealed")
				
				CLEAR_WARP_INTO_CAR_MEET_CAR_PARK()
			ENDIF
		ELSE
			IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_VEH_IN)
				VECTOR vCurrentLoc = GET_ENTITY_COORDS(MPGlobalsAmbience.vehPersonalVehicle)
				
				IF vCurrentLoc.z > -20
				AND NOT SHOULD_CONCEAL_PLAYERS_TEST_DRIVE_PV(PLAYER_ID())
					PRINTLN("MAINTAIN_WARPS_FOR_CAR_MEET: vehicle no longer in garage clear flag")
					
					CLEAR_WARP_INTO_CAR_MEET_CAR_PARK()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// PRIVATE CAR MEET

PROC GET_PRIVATE_CAR_MEET_PARKING_SPOT_LOCATION(INT iLocation, VECTOR &vLoc, FLOAT &fHeading)
	IF iLocation < 0
	OR iLocation > 75
		PRINTLN("GET_PRIVATE_CAR_MEET_PARKING_SPOT_LOCATION: INVALID LOCATION! iLocation = ", iLocation)
		
		SCRIPT_ASSERT("GET_PRIVATE_CAR_MEET_PARKING_SPOT_LOCATION: INVALID LOCATION!")
	ENDIF
	
	SWITCH iLocation
		CASE 0
			vLoc =	<<-2177.9231, 1107.9287, -24.3563>>
			fHeading =	268.6074
		BREAK
		CASE 1
			vLoc =	<<-2177.8303, 1103.9323, -24.3537>>
			fHeading =	274.4270
		BREAK
		CASE 2
			vLoc =	<<-2165.4531, 1116.0776, -24.3580>>
			fHeading =	90.1466
		BREAK
		CASE 3
			vLoc =	<<-2165.2981, 1112.5342, -24.3572>>
			fHeading =	84.8473
		BREAK
		CASE 4
			vLoc =	<<-2165.4578, 1098.2908, -24.3506>>
			fHeading =	108.7881
		BREAK
		CASE 5
			vLoc =	<<-2165.4221, 1093.5231, -24.3501>>
			fHeading =	76.1524
		BREAK
		CASE 6
			vLoc =	<<-2165.4844, 1119.5422, -24.3574>>
			fHeading =	93.5614
		BREAK
		CASE 7
			vLoc =	<<-2177.5957, 1096.2678, -24.3518>>
			fHeading =	271.8291
		BREAK
		CASE 8
			vLoc =	<<-2200.1125, 1126.0621, -23.2453>>
			fHeading =	270.0032
		BREAK
		CASE 9
			vLoc =	<<-2177.8289, 1116.0764, -24.3584>>
			fHeading =	269.9901
		BREAK
		CASE 10
			vLoc =	<<-2177.5747, 1112.5581, -24.3582>>
			fHeading =	273.7864
		BREAK
		CASE 11
			vLoc =	<<-2177.9053, 1119.5828, -24.3586>>
			fHeading =	264.7997
		BREAK
		CASE 12
			vLoc =	<<-2158.2222, 1116.0182, -24.3580>>
			fHeading =	270.0084
		BREAK
		CASE 13
			vLoc =	<<-2158.2637, 1119.5229, -24.3575>>
			fHeading =	265.0304
		BREAK
		CASE 14
			vLoc =	<<-2158.0806, 1112.5876, -24.3572>>
			fHeading =	273.4794
		BREAK
		CASE 15
			vLoc =	<<-2158.2239, 1096.0313, -24.3512>>
			fHeading =	270.0067
		BREAK
		CASE 16
			vLoc =	<<-2158.1777, 1092.5867, -24.3501>>
			fHeading =	272.9506
		BREAK
		CASE 17
			vLoc =	<<-2158.2400, 1099.5592, -24.3512>>
			fHeading =	264.8289
		BREAK
		CASE 18
			vLoc =	<<-2165.4331, 1086.0717, -24.3499>>
			fHeading =	90.0032
		BREAK
		CASE 19
			vLoc =	<<-2165.3750, 1089.5092, -24.3498>>
			fHeading =	92.9855
		BREAK
		CASE 20
			vLoc =	<<-2165.3408, 1082.5454, -24.3491>>
			fHeading =	84.9266
		BREAK
		CASE 21
			vLoc =	<<-2158.1560, 1088.0164, -24.3497>>
			fHeading =	275.9615
		BREAK
		CASE 22
			vLoc =	<<-2158.2715, 1084.2714, -24.3493>>
			fHeading =	280.2039
		BREAK
		CASE 23
			vLoc =	<<-2165.6335, 1123.9570, -24.3583>>
			fHeading =	94.6246
		BREAK
		CASE 24
			vLoc =	<<-2165.5767, 1127.7930, -24.3584>>
			fHeading =	93.3687
		BREAK
		CASE 25
			vLoc =	<<-2157.9446, 1123.8267, -24.3582>>
			fHeading =	284.7038
		BREAK
		CASE 26
			vLoc =	<<-2157.9692, 1128.2184, -24.3584>>
			fHeading =	256.2780
		BREAK
		CASE 27
			vLoc =	<<-2177.7671, 1123.8770, -24.3588>>
			fHeading =	267.9635
		BREAK
		CASE 28
			vLoc =	<<-2177.7429, 1128.0851, -24.3590>>
			fHeading =	267.4383
		BREAK
		CASE 29
			vLoc =	<<-2144.9077, 1094.9070, -24.3527>>
			fHeading =	88.7829
		BREAK
		CASE 30
			vLoc =	<<-2145.0547, 1091.4614, -24.3521>>
			fHeading =	90.6067
		BREAK
		CASE 31
			vLoc =	<<-2144.8313, 1098.4993, -24.3562>>
			fHeading =	90.1856
		BREAK
		CASE 32
			vLoc =	<<-2144.9692, 1087.9535, -24.3517>>
			fHeading =	89.9745
		BREAK
		CASE 33
			vLoc =	<<-2144.9961, 1084.4882, -24.3513>>
			fHeading =	90.6556
		BREAK
		CASE 34
			vLoc =	<<-2145.0771, 1081.0107, -24.3509>>
			fHeading =	88.6687
		BREAK
		CASE 35
			vLoc =	<<-2145.1831, 1077.5220, -24.3505>>
			fHeading =	88.9730
		BREAK
		CASE 36
			vLoc =	<<-2145.0715, 1133.0488, -24.3584>>
			fHeading =	90.7486
		BREAK
		CASE 37
			vLoc =	<<-2144.8811, 1136.5107, -24.3584>>
			fHeading =	89.5743
		BREAK
		CASE 38
			vLoc =	<<-2145.0581, 1139.9875, -24.3584>>
			fHeading =	90.0032
		BREAK
		CASE 39
			vLoc =	<<-2144.9500, 1129.4253, -24.3584>>
			fHeading =	89.4755
		BREAK
		CASE 40
			vLoc =	<<-2144.8835, 1143.4878, -24.3584>>
			fHeading =	90.7583
		BREAK
		CASE 41
			vLoc =	<<-2145.0391, 1146.9966, -24.3584>>
			fHeading =	92.1478
		BREAK
		CASE 42
			vLoc =	<<-2177.7144, 1134.1493, -24.3593>>
			fHeading =	262.7999
		BREAK
		CASE 43
			vLoc =	<<-2177.5146, 1138.0248, -24.3594>>
			fHeading =	259.8243
		BREAK
		CASE 44
			vLoc =	<<-2177.5696, 1088.3298, -24.3522>>
			fHeading =	274.1133
		BREAK
		CASE 45
			vLoc =	<<-2177.8508, 1084.0895, -24.3526>>
			fHeading =	282.4535
		BREAK
		CASE 46
			vLoc =	<<-2187.4548, 1103.3311, -23.2461>>
			fHeading =	92.3360
		BREAK
		CASE 47
			vLoc =	<<-2187.6548, 1107.0391, -23.2461>>
			fHeading =	92.2579
		BREAK
		CASE 48
			vLoc =	<<-2187.6829, 1110.5950, -23.2461>>
			fHeading =	93.1824
		BREAK
		CASE 49
			vLoc =	<<-2187.5513, 1114.0690, -23.2461>>
			fHeading =	89.2290
		BREAK
		CASE 50
			vLoc =	<<-2187.5925, 1117.5670, -23.2461>>
			fHeading =	89.9956
		BREAK
		CASE 51
			vLoc =	<<-2187.6853, 1121.0276, -23.2462>>
			fHeading =	90.0083
		BREAK
		CASE 52
			vLoc =	<<-2187.3738, 1124.5410, -23.2462>>
			fHeading =	91.5539
		BREAK
		CASE 53
			vLoc =	<<-2187.5918, 1128.0433, -23.2463>>
			fHeading =	91.7091
		BREAK
		CASE 54
			vLoc =	<<-2187.7708, 1131.5052, -23.2463>>
			fHeading =	93.1601
		BREAK
		CASE 55
			vLoc =	<<-2187.5669, 1135.1608, -23.2464>>
			fHeading =	94.9238
		BREAK
		CASE 56
			vLoc =	<<-2187.6206, 1138.7238, -23.2464>>
			fHeading =	97.1151
		BREAK
		CASE 57
			vLoc =	<<-2177.6123, 1092.7548, -24.3519>>
			fHeading =	272.2261
		BREAK
		CASE 58
			vLoc =	<<-2200.0020, 1129.5577, -23.2452>>
			fHeading =	265.2113
		BREAK
		CASE 59
			vLoc =	<<-2199.9753, 1122.6279, -23.2451>>
			fHeading =	272.6231
		BREAK
		CASE 60
			vLoc =	<<-2199.8738, 1118.5548, -23.2458>>
			fHeading =	259.0827
		BREAK
		CASE 61
			vLoc =	<<-2199.7891, 1114.1665, -23.2457>>
			fHeading =	289.0927
		BREAK
		CASE 62
			vLoc =	<<-2187.3303, 1094.1610, -23.2460>>
			fHeading =	90.1848
		BREAK
		CASE 63
			vLoc =	<<-2187.7041, 1090.5686, -23.2459>>
			fHeading =	87.9952
		BREAK
		CASE 64
			vLoc =	<<-2187.4436, 1086.7590, -23.2459>>
			fHeading =	90.8072
		BREAK
		CASE 65
			vLoc =	<<-2187.4590, 1083.1962, -23.2459>>
			fHeading =	88.5089
		BREAK
		CASE 66
			vLoc =	<<-2199.7505, 1088.7549, -23.2451>>
			fHeading =	263.4081
		BREAK
		CASE 67
			vLoc =	<<-2199.9680, 1085.0601, -23.2449>>
			fHeading =	271.1517
		BREAK
		CASE 68
			vLoc =	<<-2207.1748, 1086.0065, -23.2452>>
			fHeading =	90.0083
		BREAK
		CASE 69
			vLoc =	<<-2207.0752, 1089.4840, -23.2453>>
			fHeading =	92.8786
		BREAK
		CASE 70
			vLoc =	<<-2206.9995, 1082.4727, -23.2451>>
			fHeading =	82.8960
		BREAK
		CASE 71
			vLoc =	<<-2207.2773, 1117.6952, -23.2459>>
			fHeading =	83.0709
		BREAK
		CASE 72
			vLoc =	<<-2207.3369, 1114.0239, -23.2459>>
			fHeading =	85.0167
		BREAK
		CASE 73
			vLoc =	<<-2207.2214, 1126.0583, -23.2455>>
			fHeading =	90.0112
		BREAK
		CASE 74
			vLoc =	<<-2207.2012, 1129.4874, -23.2452>>
			fHeading =	92.5523
		BREAK
		CASE 75
			vLoc =	<<-2207.1748, 1122.5454, -23.2454>>
			fHeading =	85.2016
		BREAK
	ENDSWITCH
ENDPROC

PROC REMOVE_PLAYERS_FROM_YOUR_PRIVATE_CAR_MEET_PERSONAL_VEHICLE()
	PLAYER_INDEX tempPlayer
	
	IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
		IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehPersonalVehicle)
		AND NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
			IF NOT IS_VEHICLE_EMPTY(MPGlobalsAmbience.vehPersonalVehicle, TRUE, TRUE)
			AND NOT bPersonalVehicleCleanupDontLeaveVehicle
				IF NOT IS_PLAYER_ENTERING_PROPERTY(PLAYER_ID())
					PRINTLN("[personal_vehicle] REMOVE_PLAYERS_FROM_YOUR_PRIVATE_CAR_MEET_PERSONAL_VEHICLE: Kicking remote players out of old player vehicle")
					
					IF ABSI(GET_TIME_DIFFERENCE(pv_RemovePlayersTimer, GET_NETWORK_TIME())) >= 1000
						INT i
						REPEAT NUM_NETWORK_PLAYERS i
							tempPlayer = INT_TO_PLAYERINDEX(i)
							
							IF IS_NET_PLAYER_OK(tempPlayer, FALSE)
								IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(tempPlayer), MPGlobalsAmbience.vehPersonalVehicle)
									BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(tempPlayer), TRUE, 0, 0, FALSE, FALSE)
								ENDIF
							ENDIF
						ENDREPEAT
						
						pv_RemovePlayersTimer = GET_NETWORK_TIME()
					ENDIF
				ELSE
					PRINTLN("[personal_vehicle] REMOVE_PLAYERS_FROM_YOUR_PRIVATE_CAR_MEET_PERSONAL_VEHICLE: NOT Kicking entering garage")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

CONST_INT NUM_PRIVATE_CAR_MEET_SPACES	76

FUNC BOOL IS_CAR_MEET_SPACE_ALLOWED(INT iSpaceID, MODEL_NAMES eVehicleModel)
	SWITCH iSpaceID
		CASE 0 // 0
		CASE 59 // 1
		CASE 15 // 4
		CASE 75 // 6
		CASE 67 // 8
		CASE 61 // 9
		CASE 58 // 10
		CASE 18 // 19
		CASE 5 // 20
		CASE 22 // 21
		CASE 12 // 23
		CASE 68 // 33
		CASE 69 // 34
		CASE 71 // 35
		CASE 60 // 36
		CASE 19 // 41
		CASE 66 // 43
		CASE 72 // 44
		CASE 73 // 45
		CASE 74 // 46
		CASE 21 // 47
		CASE 8 // 50
		CASE 70 // 53
		CASE 25 // 56
		CASE 26 // 63
		CASE 20 // 64
		CASE 4 // 65
		CASE 16 // 68
		CASE 17 // 69
		CASE 13 // 70
		CASE 14 // 75
			IF eVehicleModel = SLAMTRUCK
			OR eVehicleModel = PATRIOT2
			OR eVehicleModel = CARACARA
			OR eVehicleModel = CARACARA2
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

PROC PRIVATE_CAR_MEET_CAR_PARK_REQUESTS_PLAYER_LOOP(INT iPlayer #IF IS_DEBUG_BUILD , PLAYER_INDEX playerID #ENDIF )
	
	INT iPrivateCarMeet = GlobalplayerBD[iPlayer].iRequestedPrivateCarMeet
	
	IF iPrivateCarMeet > -1
		IF IS_BIT_SET(GlobalplayerBD[iPlayer].iBSTwo, BSTWO_REQUEST_PRIVATE_CAR_MEET_CAR_PARK)
			IF NOT IS_BIT_SET(GlobalServerBD_BlockB.g_iBSPrivateCarMeetParkingAssigned[iPrivateCarMeet], iPlayer)
				INT i
				REPEAT NUM_PRIVATE_CAR_MEET_SPACES i
					INT iBitSet = (i / 32)
					INT iBitVal = (i % 32)
					
					IF NOT IS_BIT_SET(GlobalServerBD_BlockB.g_iBSPrivateCarMeetSpaceBS[iPrivateCarMeet][iBitSet], iBitVal)
					AND IS_CAR_MEET_SPACE_ALLOWED(i, GlobalplayerBD[iPlayer].eCarMeetPVModel)
						GlobalServerBD_BlockB.g_iAssignedPrivateCarMeetParkingSpots[iPlayer] = i
						
						SET_BIT(GlobalServerBD_BlockB.g_iBSPrivateCarMeetParkingAssigned[iPrivateCarMeet], iPlayer)
						SET_BIT(GlobalServerBD_BlockB.g_iBSPrivateCarMeetSpaceBS[iPrivateCarMeet][iBitSet], iBitVal)
						
						#IF IS_DEBUG_BUILD
						PRINTLN("PRIVATE_CAR_MEET_CAR_PARK_REQUESTS_PLAYER_LOOP: assigning car park #", iPrivateCarMeet, " space #", i, " to player name: ", GET_PLAYER_NAME(playerID), " player Number: ", iPlayer)
						#ENDIF
						
						i = NUM_PRIVATE_CAR_MEET_SPACES
					ENDIF
				ENDREPEAT
			ELIF GlobalplayerBD[iPlayer].iRequestedPrivateCarMeetSpace != -1
			AND GlobalServerBD_BlockB.g_iAssignedPrivateCarMeetParkingSpots[iPlayer] != GlobalplayerBD[iPlayer].iRequestedPrivateCarMeetSpace
				INT i = GlobalplayerBD[iPlayer].iRequestedPrivateCarMeetSpace
				INT iBitSet = (i / 32)
				INT iBitVal = (i % 32)
				
				INT iPreviousBitSet = (GlobalServerBD_BlockB.g_iAssignedPrivateCarMeetParkingSpots[iPlayer] / 32)
				INT iPreviousBitVal = (GlobalServerBD_BlockB.g_iAssignedPrivateCarMeetParkingSpots[iPlayer] % 32)
				
				IF NOT IS_BIT_SET(GlobalServerBD_BlockB.g_iBSPrivateCarMeetSpaceBS[iPrivateCarMeet][iBitSet], iBitVal)
				AND IS_CAR_MEET_SPACE_ALLOWED(i, GlobalplayerBD[iPlayer].eCarMeetPVModel)
					GlobalServerBD_BlockB.g_iAssignedPrivateCarMeetParkingSpots[iPlayer] = i
					
					SET_BIT(GlobalServerBD_BlockB.g_iBSPrivateCarMeetParkingAssigned[iPrivateCarMeet], iPlayer)
					SET_BIT(GlobalServerBD_BlockB.g_iBSPrivateCarMeetSpaceBS[iPrivateCarMeet][iBitSet], iBitVal)
					
					CLEAR_BIT(GlobalServerBD_BlockB.g_iBSPrivateCarMeetSpaceBS[iPrivateCarMeet][iPreviousBitSet], iPreviousBitVal)
					
					#IF IS_DEBUG_BUILD
					PRINTLN("PRIVATE_CAR_MEET_CAR_PARK_REQUESTS_PLAYER_LOOP: assigning car park #", iPrivateCarMeet, " space #", i, " to player name: ", GET_PLAYER_NAME(playerID), " player Number: ", iPlayer)
					#ENDIF
					
					i = NUM_PRIVATE_CAR_MEET_SPACES
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PRIVATE_CAR_MEET_CAR_PARK_REQUESTS()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		BOOL bKeepRequest = FALSE
		
		IF GlobalServerBD_BlockB.g_iAssignedPrivateCarMeetParkingSpots[i] >= 0
			PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(i)
			
			IF IS_NET_PLAYER_OK(playerID)
				IF IS_BIT_SET(GlobalplayerBD[i].iBSTwo, BSTWO_REQUEST_PRIVATE_CAR_MEET_CAR_PARK)
					bKeepRequest = TRUE
				ELSE
					bKeepRequest = FALSE
				ENDIF
			ELSE
				bKeepRequest = FALSE
			ENDIF
			
			IF NOT bKeepRequest
				#IF IS_DEBUG_BUILD
				IF IS_NET_PLAYER_OK(playerID, FALSE)
					PRINTLN("MAINTAIN_PRIVATE_CAR_MEET_CAR_PARK_REQUESTS: clearing car park slot #", GlobalServerBD_BlockB.g_iAssignedPrivateCarMeetParkingSpots[i], " of player name: ", GET_PLAYER_NAME(playerID), " player Number: ", i)
				ELSE
					PRINTLN("MAINTAIN_PRIVATE_CAR_MEET_CAR_PARK_REQUESTS: clearing car park slot #", GlobalServerBD_BlockB.g_iAssignedPrivateCarMeetParkingSpots[i], " of player #", i)
				ENDIF
				#ENDIF
				
				INT iBitSet = (GlobalServerBD_BlockB.g_iAssignedPrivateCarMeetParkingSpots[i] / 32)
				INT iBitVal = (GlobalServerBD_BlockB.g_iAssignedPrivateCarMeetParkingSpots[i] % 32)
				
				INT j
				REPEAT NUM_NETWORK_PLAYERS j
					IF IS_BIT_SET(GlobalServerBD_BlockB.g_iBSPrivateCarMeetParkingAssigned[j], i)
						CLEAR_BIT(GlobalServerBD_BlockB.g_iBSPrivateCarMeetParkingAssigned[j], i)
						
						CLEAR_BIT(GlobalServerBD_BlockB.g_iBSPrivateCarMeetSpaceBS[j][iBitSet], iBitVal)
						
						j = NUM_NETWORK_PLAYERS
					ENDIF
				ENDREPEAT
				
				GlobalServerBD_BlockB.g_iAssignedPrivateCarMeetParkingSpots[i] = -1
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_PRIVATE_CAR_MEET_CAR_PARKING_SPOT_FROM_SERVER()
	#IF IS_DEBUG_BUILD
	IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_REQUEST_PRIVATE_CAR_MEET_CAR_PARK)
		PRINTLN("GET_PRIVATE_CAR_MEET_CAR_PARKING_SPOT_FROM_SERVER: called this frame")
	ENDIF
	
	IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_PRIVATE_CAR_MEET_CAR_PARK)
		PRINTLN("Set bit (GlobalplayerBD[PLAYER_ID()].iBSTwo, BSTWO_PV_IS_WARPING_TO_PRIVATE_CAR_MEET_CAR_PARK)")
	ENDIF
	#ENDIF
	
	IF GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()) = INVALID_PLAYER_INDEX()
		RETURN -1
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(MPGlobalsAmbience.vehPersonalVehicle)
		RETURN -1
	ENDIF
	
	SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_PRIVATE_CAR_MEET_CAR_PARK)
	SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_REQUEST_PRIVATE_CAR_MEET_CAR_PARK)
	
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRequestedPrivateCarMeet = NATIVE_TO_INT(GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))
	
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].eCarMeetPVModel = GET_ENTITY_MODEL(MPGlobalsAmbience.vehPersonalVehicle)
	
	IF IS_BIT_SET(GlobalServerBD_BlockB.g_iBSPrivateCarMeetParkingAssigned[GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRequestedPrivateCarMeet], NATIVE_TO_INT(PLAYER_ID()))
		RETURN GlobalServerBD_BlockB.g_iAssignedPrivateCarMeetParkingSpots[NATIVE_TO_INT(PLAYER_ID())]
	ENDIF
	
	RETURN -1
ENDFUNC

PROC SET_CONCEAL_CURRENT_PERSONAL_VEHICLE_IN_PRIVATE_CAR_MEET(INT ownerID)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInPlayerOwnedLocation = ownerID
	
	SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CONCEAL_IN_PROPERTY)
	
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)
	
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInID = ENUM_TO_INT(SIMPLE_INTERIOR_CAR_MEET)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInType = PV_CONCEAL_TYPE_PRIVATE_CAR_MEET
	
	SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CONCEAL_IN_PROPERTY)
	
	PRINTLN("SET_CONCEAL_CURRENT_PERSONAL_VEHICLE_IN_PRIVATE_CAR_MEET: setting current personal vehicle to be concealed for car meet owner #", ownerID)
ENDPROC

PROC CLEAR_CONCEAL_CURRENT_PERSONAL_VEHICLE_IN_PRIVATE_CAR_MEET()
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInType = PV_CONCEAL_TYPE_PRIVATE_CAR_MEET
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInID = -1
		
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInType = -1
		
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInPlayerOwnedLocation = -1
		
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)
		
		FORCE_CONCEAL_MY_ACTIVE_PERSONAL_VEHICLE(FALSE)
		SET_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(FALSE)
		
		CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CONCEAL_IN_PROPERTY)
		
		PRINTLN("CLEAR_CONCEAL_CURRENT_PERSONAL_VEHICLE_IN_PRIVATE_CAR_MEET: clearing conceal of personal vehicle")
	ENDIF
ENDPROC

PROC WARP_INTO_PRIVATE_CAR_MEET_VEHICLE_RESET()
	IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehPersonalVehicle)
	AND IS_VEHICLE_DRIVEABLE(MPGlobalsAmbience.vehPersonalVehicle)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehPersonalVehicle)
			SET_VEHICLE_HANDBRAKE(MPGlobalsAmbience.vehPersonalVehicle, FALSE)
			SET_ENTITY_CAN_BE_DAMAGED(MPGlobalsAmbience.vehPersonalVehicle, TRUE)
			SET_VEHICLE_RADIO_ENABLED(MPGlobalsAmbience.vehPersonalVehicle, TRUE)
			
			MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark = 0
			
			PRINTLN("WARP_INTO_PRIVATE_CAR_MEET_VEHICLE_RESET: finished reset")
		ELSE
			PRINTLN("WARP_INTO_PRIVATE_CAR_MEET_VEHICLE_RESET: getting control over the vehicle")
			
			NETWORK_REQUEST_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehPersonalVehicle)
		ENDIF
	ELSE
		MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark = 0
	ENDIF
ENDPROC

PROC CLEAR_WARP_INTO_PRIVATE_CAR_MEET_CAR_PARK()
	IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_REQUEST_PRIVATE_CAR_MEET_CAR_PARK)
		
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRequestedPrivateCarMeet = -1
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_PRIVATE_CAR_MEET_CAR_PARK)
		PRINTLN("Clear bit (GlobalplayerBD[PLAYER_ID()].iBSTwo, BSTWO_PV_IS_WARPING_TO_PRIVATE_CAR_MEET_CAR_PARK)")
	ENDIF
	#ENDIF
	
	CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_PRIVATE_CAR_MEET_CAR_PARK)
	
	SET_BIT(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_RESET)
	
	WARP_INTO_PRIVATE_CAR_MEET_VEHICLE_RESET()
	
	CLEAR_CONCEAL_CURRENT_PERSONAL_VEHICLE_IN_PRIVATE_CAR_MEET()
ENDPROC

PROC INIT_WARP_INTO_PRIVATE_CAR_MEET_CAR_PARK(BOOL bEmptyVeh)
	SET_BIT(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_WARP_IN)
	
	IF bEmptyVeh
		SET_BIT(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_EMPTY_VEH)
	ELSE
		CLEAR_BIT(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_EMPTY_VEH)
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
	
	PRINTLN("INIT_WARP_INTO_PRIVATE_CAR_MEET_CAR_PARK: called this frame with bEmptyVeh = ", bEmptyVeh)
ENDPROC

FUNC BOOL PRIVATE_CAR_MEET_CAR_PARK_CHECK_IF_OK_TO_WARP_VEHICLE(VECTOR vCreationPos)
	FLOAT fdistance
	
	PLAYER_INDEX playerID
	
	VECTOR vPlayerPos
	
	IF NOT HAS_NET_TIMER_STARTED(MPGlobals.VehicleData.stPrivateCarMeetCarParkAreaBlocked)
		START_NET_TIMER(MPGlobals.VehicleData.stPrivateCarMeetCarParkAreaBlocked)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(MPGlobals.VehicleData.stPrivateCarMeetCarParkAreaBlocked, 20000)
			PRINTLN("PRIVATE_CAR_MEET_CAR_PARK_CHECK_IF_OK_TO_WARP_VEHICLE: hit timeout proceeding")
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	INT iLoop
	REPEAT NUM_NETWORK_PLAYERS iLoop
		playerID = INT_TO_PLAYERINDEX(iLoop)
		
		IF IS_NET_PLAYER_OK(playerID)
			IF IS_PLAYER_IN_CAR_MEET_PROPERTY(playerID)
			AND ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(PLAYER_ID(), playerID, TRUE)
				IF NOT IS_THIS_PLAYER_ACTIVE_IN_CORONA(playerID)
				AND NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID))
				AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(playerID)
					vPlayerPos = GET_PLAYER_COORDS(playerID)
					fdistance = GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vCreationPos, FALSE)
					
					IF fDistance < 5
						PRINTLN("PRIVATE_CAR_MEET_CAR_PARK_CHECK_IF_OK_TO_WARP_VEHICLE - Player ", iLoop, " is in the way.")
						
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_WARPS_FOR_PRIVATE_CAR_MEET()
	FLOAT fHeading
	
	VECTOR vLoc
	
	IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
	AND NOT MPGlobalsAmbience.bLaunchVehicleDropPersonal
		IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_RESET)
			WARP_INTO_PRIVATE_CAR_MEET_VEHICLE_RESET()
		ELIF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_WARP_IN)
			PRINTLN("MAINTAIN_WARPS_FOR_PRIVATE_CAR_MEET: trying to warp player's vehicle inside")
			
			IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehPersonalVehicle)
			AND NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
				IF NOT IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_EMPTY_VEH)
				OR IS_VEHICLE_EMPTY(MPGlobalsAmbience.vehPersonalVehicle, TRUE, TRUE)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehPersonalVehicle)
						IF NOT IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_FADE_VEH)
							NETWORK_FADE_OUT_ENTITY(MPGlobalsAmbience.vehPersonalVehicle, FALSE, TRUE)
							
							SET_BIT(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_FADE_VEH)
						ELSE
							INT iSpot = GET_PRIVATE_CAR_MEET_CAR_PARKING_SPOT_FROM_SERVER()
							
							IF iSpot > -1
							AND NOT NETWORK_IS_ENTITY_FADING(MPGlobalsAmbience.vehPersonalVehicle)
								PRINTLN("MAINTAIN_WARPS_FOR_PRIVATE_CAR_MEET: trying to warp player's personal vehicle into shared carpark spot #", iSpot)
								
								GET_PRIVATE_CAR_MEET_PARKING_SPOT_LOCATION(iSpot, vLoc, fHeading)
								
								IF PRIVATE_CAR_MEET_CAR_PARK_CHECK_IF_OK_TO_WARP_VEHICLE(vLoc)
								AND NOT IS_ANY_VEHICLE_NEAR_POINT(vLoc, 0.5)
									PRINTLN("MAINTAIN_WARPS_FOR_PRIVATE_CAR_MEET: warping player's personal vehicle into shared carpark spot #", iSpot)
									
									SET_ENTITY_COORDS_NO_OFFSET(MPGlobalsAmbience.vehPersonalVehicle, vLoc)
									SET_ENTITY_HEADING(MPGlobalsAmbience.vehPersonalVehicle, fHeading)
									
									PRINTLN("MAINTAIN_WARPS_FOR_PRIVATE_CAR_MEET: warping player's personal vehicle to ", vLoc)
									
									FREEZE_ENTITY_POSITION(MPGlobalsAmbience.vehPersonalVehicle, FALSE)
									
									PRINTLN("MAINTAIN_WARPS_FOR_PRIVATE_CAR_MEET: unfreezing before setting to be frozen waiting on collision")
									
									SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(MPGlobalsAmbience.vehPersonalVehicle, TRUE)
									
									SET_VEHICLE_ON_GROUND_PROPERLY(MPGlobalsAmbience.vehPersonalVehicle, 1)
									
									PRINTLN("MAINTAIN_WARPS_FOR_PRIVATE_CAR_MEET: warping player's personal vehicle to ", vLoc, " after on ground")
									
									SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(MPGlobalsAmbience.vehPersonalVehicle, FALSE)
									SET_VEHICLE_FIXED(MPGlobalsAmbience.vehPersonalVehicle)
							        SET_ENTITY_HEALTH(MPGlobalsAmbience.vehPersonalVehicle, 1000)
							        SET_VEHICLE_ENGINE_HEALTH(MPGlobalsAmbience.vehPersonalVehicle, 1000)
							        SET_VEHICLE_PETROL_TANK_HEALTH(MPGlobalsAmbience.vehPersonalVehicle, 1000)
							        SET_VEHICLE_DIRT_LEVEL(MPGlobalsAmbience.vehPersonalVehicle, 0)
									SET_ENTITY_CAN_BE_DAMAGED(MPGlobalsAmbience.vehPersonalVehicle, FALSE)
									SET_VEHICLE_RADIO_ENABLED(MPGlobalsAmbience.vehPersonalVehicle, FALSE)
									
									SET_BIT(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_RADIO_OFF)
									SET_BIT(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_SET_FLAGS)
									
									IF GET_ENTITY_MODEL(MPGlobalsAmbience.vehPersonalVehicle) = DELUXO
									OR GET_ENTITY_MODEL(MPGlobalsAmbience.vehPersonalVehicle) = OPPRESSOR2
										SET_DISABLE_HOVER_MODE_FLIGHT(MPGlobalsAmbience.vehPersonalVehicle, TRUE)
										SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(MPGlobalsAmbience.vehPersonalVehicle, 0)
									ELIF GET_ENTITY_MODEL(MPGlobalsAmbience.vehPersonalVehicle) = STROMBERG
										TRANSFORM_TO_CAR(MPGlobalsAmbience.vehPersonalVehicle, TRUE)
									ENDIF
									
									IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_FADE_VEH)
										NETWORK_FADE_IN_ENTITY(MPGlobalsAmbience.vehPersonalVehicle, TRUE, FALSE)
									ENDIF
									
									SET_ENTITY_COLLISION(MPGlobalsAmbience.vehPersonalVehicle, TRUE)
									SET_VEHICLE_LIGHTS(MPGlobalsAmbience.vehPersonalVehicle, SET_VEHICLE_LIGHTS_OFF)
									
									CLEAR_BIT(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_FADE_VEH)
									CLEAR_BIT(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_WARP_IN)
									
									SET_BIT(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_VEH_IN)
									
									SET_VEHICLE_HANDBRAKE(MPGlobalsAmbience.vehPersonalVehicle, TRUE)
									
									CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_PRIVATE_CAR_MEET_CAR_PARK)
									
									PRINTLN("Clear bit (GlobalplayerBD[PLAYER_ID()].iBSTwo, BSTWO_PV_IS_WARPING_TO_PRIVATE_CAR_MEET_CAR_PARK)")
									
									RESET_NET_TIMER(MPGlobals.VehicleData.stPrivateCarMeetCarParkAreaBlocked)
									
									IF NOT IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), MPGlobalsAmbience.vehPersonalVehicle)
										SET_VEHICLE_ENGINE_ON(MPGlobalsAmbience.vehPersonalVehicle, FALSE, TRUE)
									ENDIF
									
									SET_CONCEAL_CURRENT_PERSONAL_VEHICLE_IN_PRIVATE_CAR_MEET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRequestedPrivateCarMeet)
								ELSE
									IF IS_ANY_VEHICLE_NEAR_POINT(vLoc, 0.5)
										PRINTLN("MAINTAIN_WARPS_FOR_PRIVATE_CAR_MEET: vehicle is near spot #", iSpot)
									ENDIF
									
									PRINTLN("MAINTAIN_WARPS_FOR_PRIVATE_CAR_MEET: waiting for spot to be empty #", iSpot)
								ENDIF
							ELSE
								IF iSpot = -1
									PRINTLN("MAINTAIN_WARPS_FOR_PRIVATE_CAR_MEET: waiting for spot from server")
								ENDIF
							ENDIF
						ENDIF
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehPersonalVehicle)
						
						PRINTLN("MAINTAIN_WARPS_FOR_PRIVATE_CAR_MEET: trying to get control of vehicle")
					ENDIF
				ELSE
					REMOVE_PLAYERS_FROM_YOUR_PRIVATE_CAR_MEET_PERSONAL_VEHICLE()
				ENDIF
			ELSE
				PRINTLN("MAINTAIN_WARPS_FOR_PRIVATE_CAR_MEET: vehicle does not exist, or is being cleaned up/concealed")
				
				CLEAR_WARP_INTO_PRIVATE_CAR_MEET_CAR_PARK()
			ENDIF
		ELSE
			IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_VEH_IN)
				VECTOR vCurrentLoc = GET_ENTITY_COORDS(MPGlobalsAmbience.vehPersonalVehicle)
				
				IF vCurrentLoc.z > -20
				AND NOT SHOULD_CONCEAL_PLAYERS_TEST_DRIVE_PV(PLAYER_ID())
					PRINTLN("MAINTAIN_WARPS_FOR_PRIVATE_CAR_MEET: vehicle no longer in garage clear flag")
					
					CLEAR_WARP_INTO_PRIVATE_CAR_MEET_CAR_PARK()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CONCEALING_TEST_DRIVE_PV_PLAYER_LOOP(INT iPlayer, BOOL& bConcealed, BOOL& bConcealedThisFrame, BOOL bConcealLocalPlayer)
	IF NOT bConcealLocalPlayer
		IF GlobalplayerBD[iPlayer].iOwnerOfTestDrivePV = NATIVE_TO_INT(PLAYER_ID())
			PRINTLN("CONCEALING_TEST_DRIVE_PV_PLAYER_LOOP - My PV is being test driven")
			
			SET_CONCEAL_TEST_DRIVE_PV(TRUE)
			
			bConcealedThisFrame = TRUE
		ENDIF
	ELSE
		IF GlobalplayerBD[iPlayer].iOwnerOfTestDrivePV = NATIVE_TO_INT(PLAYER_ID())
			PRINTLN("CONCEALING_TEST_DRIVE_PV_PLAYER_LOOP - My PV is still being test driven")
			
			bConcealed = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC CONCEALING_TEST_DRIVE_PV_POST(BOOL bConcealed)
	IF SHOULD_CONCEAL_PLAYERS_TEST_DRIVE_PV(PLAYER_ID())
	AND NOT bConcealed
		FLOAT fHeading
		
		VECTOR vLoc
		
		IF IS_PLAYER_PV_IN_CAR_MEET_GAR(PLAYER_ID())
			INT iSpace = -1
			
			INT j
			REPEAT NUM_NETWORK_PLAYERS j
				IF GlobalServerBD_BlockC.g_iAssignedCarMeetParkingSpots[j] = NATIVE_TO_INT(PLAYER_ID())
					iSpace = j
				ENDIF
			ENDREPEAT
			
			IF iSpace > -1
				PRINTLN("CONCEALING_TEST_DRIVE_PV_POST - Checking before unconcealing PV in Public Car Meet")
				
				GET_CAR_MEET_PARKING_SPOT_LOCATION(iSpace, vLoc, fHeading)
				
				SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_MEET_CAR_PARK)
				
				IF NOT HAS_NET_TIMER_STARTED(MPGlobals.VehicleData.stCarMeetCarParkAreaBlocked)
					REINIT_NET_TIMER(MPGlobals.VehicleData.stCarMeetCarParkAreaBlocked)
				ENDIF
				
				IF CAR_MEET_CAR_PARK_CHECK_IF_OK_TO_WARP_VEHICLE(vLoc)
					PRINTLN("CONCEALING_TEST_DRIVE_PV_POST - Unconcealing PV in Public Car Meet")
					
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_MEET_CAR_PARK)
					
					MPGlobalsAmbience.iStoredAllowIntoPV = -1
					
					RESET_NET_TIMER(MPGlobals.VehicleData.stCarMeetCarParkAreaBlocked)
					
					SET_CONCEAL_TEST_DRIVE_PV(FALSE)
				ENDIF
			ENDIF
		ELIF IS_PLAYER_PV_IN_PRIVATE_CAR_MEET_GAR(PLAYER_ID())
		AND GlobalServerBD_BlockB.g_iAssignedPrivateCarMeetParkingSpots[NATIVE_TO_INT(PLAYER_ID())] != -1
			PRINTLN("CONCEALING_TEST_DRIVE_PV_POST - Checking before unconcealing PV in Private Car Meet")
			
			GET_PRIVATE_CAR_MEET_PARKING_SPOT_LOCATION(GlobalServerBD_BlockB.g_iAssignedPrivateCarMeetParkingSpots[NATIVE_TO_INT(PLAYER_ID())], vLoc, fHeading)
			
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_PRIVATE_CAR_MEET_CAR_PARK)
			
			IF NOT HAS_NET_TIMER_STARTED(MPGlobals.VehicleData.stPrivateCarMeetCarParkAreaBlocked)
				REINIT_NET_TIMER(MPGlobals.VehicleData.stPrivateCarMeetCarParkAreaBlocked)
			ENDIF
			
			IF PRIVATE_CAR_MEET_CAR_PARK_CHECK_IF_OK_TO_WARP_VEHICLE(vLoc)
				PRINTLN("CONCEALING_TEST_DRIVE_PV_POST - Unconcealing PV in Private Car Meet")
				
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_PV_IS_WARPING_TO_PRIVATE_CAR_MEET_CAR_PARK)
				
				MPGlobalsAmbience.iStoredAllowIntoPV = -1
				
				RESET_NET_TIMER(MPGlobals.VehicleData.stPrivateCarMeetCarParkAreaBlocked)
				
				SET_CONCEAL_TEST_DRIVE_PV(FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#ENDIF // FEATURE_TUNER

PROC MAINTAIN_MP_SAVED_VEHICLE()

	VEHICLE_INDEX tempveh
	VECTOR vSpawnCoords
	FLOAT fSpawnHeading
	INT i
	BOOL bUseOldLoc//,bTriggerSave/bAddInsuranceContact
	VECTOR vPvSpawnOverride
	INT iLimit
	
	IF IS_BIT_SET(g_TransitionSessionNonResetVars.MissionPVBitset, ciMISSION_WARPED_INTO_PV)
	AND NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
		CLEAR_BIT(g_TransitionSessionNonResetVars.MissionPVBitset, ciMISSION_WARPED_INTO_PV)
	ENDIF
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE)
		MPGlobals.iImpoundWantedLevel = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
	ELSE
		// dont reset back to zero if player is just dead
		IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			MPGlobals.iImpoundWantedLevel = 0
		ENDIF
	ENDIF
	
	IF IS_PERSONAL_VEHICLE_CREATION_DISABLED_FOR_TRANSITION()
		IF IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
			DISABLE_PERSONAL_VEHICLE_CREATION_FOR_TRANSITION(FALSE)
			PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE : called DISABLE_PERSONAL_VEHICLE_CREATION_FOR_TRANSITION(FALSE)")
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("marker 1")
	#ENDIF
	#ENDIF
	
	// clear the just created flag.
	IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_JUST_CREATED)
	AND IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_CREATED)
		CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_JUST_CREATED)
	ENDIF
	
	// should we return the saved vehicle to the player from the garage if the mission is no longer active?
	IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_RETURN_FROM_GARAGE_AFTER_MISSION)
		IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID()) 
		AND NOT IS_CURRENT_PERSONAL_VEHICLE_DISABLED_FOR_MISSION()
		
			IF NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS, MP_SAVED_VEHICLE_OUT_GARAGE)	
				SET_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS, MP_SAVED_VEHICLE_OUT_GARAGE)	
				PRINTLN("[personal_vehicle] personal vehicle is set back out of garage now that mission is over.")				
			ELSE
				PRINTLN("[personal_vehicle] personal vehicle is already out of garage after mission is over.")	
			ENDIF	
			
			// clear any cleanup flags that might have been set.
			PRINTLN("[personal_vehicle] clearing all cleanup flags.")	
			CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CLEANUP_DELETE)
			CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CLEANUP)
			CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CLEANUP_RETURN_TO_GARAGE)
			CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CLEANUP_FADE_OUT)
			CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CLEANUP_SET_AS_DESTROYED)
			CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CLEANUP_NOT_DURING_RESPAWN)
			
			CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_RETURN_FROM_GARAGE_AFTER_MISSION)	
		ENDIF
	ENDIF	
	

	// if personal vehicle is disabled
	IF ARE_ALL_PERSONAL_VEHICLES_DISABLED_FOR_MISSION()
	    // and player is driving out of garage
		IF (MPGlobals.VehicleData.bAwaitingAssignToMainScript = TRUE)
		OR (MPGlobals.VehicleData.bAssignToMainScript = TRUE)
	        // enable pv creation
	        DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION(FALSE)
	       	PRINTLN("[personal_vehicle] personal vehicle re-enabled after leaving garage.")
	    ENDIF
	ENDIF
		
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("2")
	#ENDIF
	#ENDIF	
	
	// is player in corona and selecting his vehicle?
	IF IS_PLAYER_IN_CORONA()
		IF IS_BIT_SET(g_FMMC_STRUCT.iRuleOptionsBitSet, ciALLOW_VEHICLE_CHOICE)
			IF NOT (g_mnMyRaceModel = DUMMY_MODEL_FOR_SCRIPT)
			AND NOT (g_iMyRaceModelChoice = -1)
			
				IF NOT (g_iMyRaceModelChoice = CURRENT_SAVED_VEHICLE_SLOT())
					
					// if existing pv exists we need to clean it up.
					IF IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
						PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - corona is cleaning up old PV.")	
					ELSE
						// does old pv still exist?
						IF NETWORK_DOES_NETWORK_ID_EXIST(PERSONAL_VEHICLE_NET_ID())
							PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - corona - old vehicle still exists, cleaning up first.")	
							CLEANUP_MP_SAVED_VEHICLE(TRUE, FALSE, TRUE)		
						ELSE
							// is old pv still set to be outside garage?
							IF (CURRENT_SAVED_VEHICLE_SLOT() >= 0)
								IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
									CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)	
									PRINTLN("[personal_vehicle] MP_SAVED_VEHICLE_OUT_GARAGE - 6cleared on #",CURRENT_SAVED_VEHICLE_SLOT())
									PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - clearing corona setting pv out of garage. old slot ", CURRENT_SAVED_VEHICLE_SLOT())
								ENDIF	
							ENDIF
							// set pv to new desired slot.
							PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - corona updating current personal vehicle slot to ", g_iMyRaceModelChoice)
							SET_LAST_USED_VEHICLE_SLOT(g_iMyRaceModelChoice)	
							IF (CURRENT_SAVED_VEHICLE_SLOT() >= 0)
								IF NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
									SET_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)	
									PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - corona setting pv out of garage.")
								ENDIF	
							ENDIF
						ENDIF
					ENDIF	
				ELSE
					// keep existing slot, make sure it is out of garage.
					IF (CURRENT_SAVED_VEHICLE_SLOT() >= 0)
						IF NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
							SET_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)	
							PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - setting existing pv out of garage.")
						ENDIF
					ENDIF
				ENDIF

			ELSE
				// if none has been selected
				IF (g_iMyRaceModelChoice = -1)
						
					// if existing pv exists we need to clean it up.
					IF IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
						PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - corona is cleaning up old PV (set to none).")	
					ELSE
						// does old pv still exist?
						IF NETWORK_DOES_NETWORK_ID_EXIST(PERSONAL_VEHICLE_NET_ID())
							PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - corona - old vehicle still exists, cleaning up first.(set to none)")	
							CLEANUP_MP_SAVED_VEHICLE(TRUE, TRUE, TRUE)		
						ELSE
							// is old pv still set to be outside garage?
							IF (CURRENT_SAVED_VEHICLE_SLOT() >= 0)
								IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
									CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)	
									PRINTLN("[personal_vehicle] MP_SAVED_VEHICLE_OUT_GARAGE - 7cleared on #",CURRENT_SAVED_VEHICLE_SLOT())
									PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - clearing corona setting pv out of garage. (set to none)  old slot ", CURRENT_SAVED_VEHICLE_SLOT())
								ENDIF			
							ENDIF
						ENDIF
					ENDIF							
						
				ENDIF
			
				//PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - g_iMyRaceModelChoice = ", g_iMyRaceModelChoice, ", g_mnMyRaceModel = ", ENUM_TO_INT(g_mnMyRaceModel))	
			ENDIF
		ELSE
			//PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - ciALLOW_VEHICLE_CHOICE bit not set")
		ENDIF
	ELSE
		//PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - IS_PLAYER_IN_CORONA = FALSE")
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("3")
	#ENDIF
	#ENDIF	
	
	// is player in office and asked PA for a vehicle
	IF (MPGlobalsAmbience.bRequestedByOfficePA)
		PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - MPGlobalsAmbience.bRequestedByOfficePA is TRUE, iPVSlotOfficePA = ", MPGlobalsAmbience.iPVSlotOfficePA)
	
		IF NOT (MPGlobalsAmbience.iPVSlotOfficePA = -1)
			IF NOT (MPGlobals.VehicleData.bAssignToMainScript) // fix for url:bugstar:7467344
		
				// if existing pv exists we need to clean it up.
				IF IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
					PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - office - is cleaning up old PV.")	
				ELSE
					// does old pv still exist?
					IF NETWORK_DOES_NETWORK_ID_EXIST(PERSONAL_VEHICLE_NET_ID())
						PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - office - - old vehicle still exists, cleaning up first.")	
						
						IF MPGlobalsAmbience.bForceDeletePV
							CLEANUP_MP_SAVED_VEHICLE(TRUE)	
						ELSE
							CLEANUP_MP_SAVED_VEHICLE(DEFAULT,DEFAULT,DEFAULT,DEFAULT, TRUE)		
						ENDIF
					ELSE
						// is old pv still set to be outside garage?
						IF (CURRENT_SAVED_VEHICLE_SLOT() >= 0)
							IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
								NET_PRINT("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - clearing office - Destroying impounded vehicle as we have a new PV") NET_NL()
								SET_SAVE_VEHICLE_AS_DESTROYED(CURRENT_SAVED_VEHICLE_SLOT())
							ELSE
							
								IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
									CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)	
									PRINTLN("[personal_vehicle] MP_SAVED_VEHICLE_OUT_GARAGE - 8cleared on #",CURRENT_SAVED_VEHICLE_SLOT())
									PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - clearing office - setting pv out of garage. old slot ", CURRENT_SAVED_VEHICLE_SLOT())
								ENDIF	
							ENDIF
							MP_SAVE_VEHICLE_SLOT_STATS_FROM_SAVEGAME(CURRENT_SAVED_VEHICLE_SLOT(),g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()] ,TRUE)
						ENDIF
						// set pv to new desired slot.
						PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - office - updating current personal vehicle slot to ", MPGlobalsAmbience.iPVSlotOfficePA)
						SET_LAST_USED_VEHICLE_SLOT(MPGlobalsAmbience.iPVSlotOfficePA)	
						IF (CURRENT_SAVED_VEHICLE_SLOT() >= 0)
							#IF FEATURE_GEN9_EXCLUSIVE
							IF IS_PLAYER_ON_MP_INTRO()
								IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)
									CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)
									
									PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - office - setting pv as not destroyed.")
								ENDIF
							ENDIF
							#ENDIF
							
							IF NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
								SET_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)	
								PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - office - setting pv out of garage.")
							ENDIF	
							IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL)
								CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL)
								PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - office - clearing create at mors mutual flag.")
							ENDIF
						ENDIF
						IF g_MpSavedVehicles[MPGlobalsAmbience.iPVSlotOfficePA].vehicleSetupMP.VehicleSetup.eModel = OPPRESSOR2
							MPGlobalsAmbience.bForceOppressor2CDOnCreate = TRUE
							//REINIT_NET_TIMER(oppressor2BikeCooldownTimer,TRUE)
							PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - office - setting MPGlobalsAmbience.bForceOppressor2CDOnCreate ")
						ENDIF
						MPGlobalsAmbience.bRequestedByOfficePA = FALSE // done
					ENDIF
				ENDIF	

			ELSE				
				MPGlobalsAmbience.bRequestedByOfficePA = FALSE // done
				PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - office - bAssignToMainScript is true, so clearing bRequestedByOfficePA")
			ENDIF
		ELSE
			
			MPGlobalsAmbience.bRequestedByOfficePA = FALSE // done
			
		ENDIF
	ENDIF	
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("4")
	#ENDIF
	#ENDIF		
	
	MAINTAIN_ASSIGN_TO_MAIN_SCRIPT_DELAYED()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ASSIGN_TO_MAIN_SCRIPT_DELAYED")
	#ENDIF
	#ENDIF			
	
	MAINTAIN_SIMPLE_INTERIOR_PV_CONCEAL_CHECK()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SIMPLE_INTERIOR_PV_CONCEAL_CHECK")
	#ENDIF
	#ENDIF	
	
	REMOVE_UNOWNED_AVENGER()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("REMOVE_UNOWNED_AVENGER")
	#ENDIF
	#ENDIF	
	
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SHUFFLE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
		PRINTLN("[personal_vehicle] disabling shuffle when entering/exiting simple interior")
	ENDIF
	
	IF IsSavedVehicleSafeToProcess()
	
		FIX_FOR_5900624()
		
		#IF IS_DEBUG_BUILD
			IF NOT (g_b_IsSavedVehicleSafeToProcess)
				NET_PRINT("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - is now safe to process.") NET_NL()
				g_b_IsSavedVehicleSafeToProcess = TRUE
			ENDIF
		#ENDIF
	
		RESET_NET_TIMER(MPGlobals.VehicleData.iCleanupTimer)
		
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
		AND NOT IS_TRANSITION_ACTIVE()
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			MAINTAIN_MP_SAVED_VEHICLE_INSURANCE_CALL()
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MP_SAVED_VEHICLE_INSURANCE_CALL")
			#ENDIF
			#ENDIF	
			
			// setup initial flags
			IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_INITIAL_SETUP)
				IF DOES_PLAYER_OWN_ANY_VEHICLE_STORAGE()
			
					SWITCH MPGlobals.VehicleData.iMPSVInitialSetupStage 
						CASE 0
						CASE 1
						CASE 2
						CASE 3
						CASe 4
						CASE 5
						CASE 6
						CASE 7
						CASE 8
						CASE 9
						CASE 10
						CASE 11
						CASE 12
						CASE 13
						CASE 14
						CASE 15
						CASE 16
						CASE 17
						CASE 18 //only add more when above 380
							iLimit = 20*(MPGlobals.VehicleData.iMPSVInitialSetupStage+1)
							IF iLimit >= MAX_MP_SAVED_VEHICLES-1
								iLimit = MAX_MP_SAVED_VEHICLES-1
							ENDIF
							FOR i = 20*(MPGlobals.VehicleData.iMPSVInitialSetupStage) TO iLimit
								IF IS_BIT_SET(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_INSURED)
									MPGlobals.VehicleData.bAddInsuranceContact = TRUE
								ENDIF	
								IF IS_BIT_SET(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_JUST_PURCHASED)
									PRINTLN("[personal_vehicle] Clearing MP_SAVED_VEHICLE_JUST_PURCHASED flag for vehicles when entering a new freemode")
									CLEAR_BIT(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_JUST_PURCHASED)
								ENDIF						
								
								IF IS_BIT_SET(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED)
									PRINTLN("[personal_vehicle] slot number #",i ," is MP_SAVED_VEHICLE_DESTROYED running check on it, Model = ",g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel)
									IF NOT CAN_THIS_PERSONAL_VEHICLE_BE_DESTROYED(i)
										PRINTLN("[personal_vehicle] Clearing MP_SAVED_VEHICLE_DESTROYED flag for undestoyable vehicle: ",g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel)
										CLEAR_BIT(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED)
									ELSE
										IF NOT IS_BIT_SET(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_GIVEN_STATUS_MESSAGE)
											SET_BIT(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_GIVEN_STATUS_MESSAGE)
											MP_SAVE_VEHICLE_SLOT_STATS_FROM_SAVEGAME(i,g_MpSavedVehicles[i],TRUE)
											MPGlobals.VehicleData.bTriggerSave = TRUE
										ENDIF
									ENDIF
								ENDIF
								IF IS_BIT_SET(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
									IF i != CURRENT_SAVED_VEHICLE_SLOT()
									OR g_SpawnData.bSpawningInProperty 
									OR g_SpawnData.bSpawningInGarage
										IF NOT IS_BIT_SET(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED)
										AND NOT IS_BIT_SET(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
											PRINTLN("[personal_vehicle] Putting vehicles back in garage 12412: index: ", i)
											CLEAR_BIT(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
											PRINTLN("[personal_vehicle] MP_SAVED_VEHICLE_OUT_GARAGE - 9cleared on #",i)
											MP_SAVE_VEHICLE_SLOT_STATS_FROM_SAVEGAME(i,g_MpSavedVehicles[i],TRUE)
											MPGlobals.VehicleData.bTriggerSave = TRUE
										ENDIF
									ENDIF
								ENDIF	
							ENDFOR
							MPGlobals.VehicleData.iMPSVInitialSetupStage++
							PRINTLN("MAINTAIN_MP_SAVED_VEHICLE: iMPSVInitialSetupStage = ",MPGlobals.VehicleData.iMPSVInitialSetupStage)
						BREAK
					ENDSWITCH
					
				ELSE
					IF IS_BIT_SET(g_MpSavedVehicles[0].iVehicleBS,MP_SAVED_VEHICLE_INSURED)
						MPGlobals.VehicleData.bAddInsuranceContact = TRUE
					ENDIF				
					IF IS_BIT_SET(g_MpSavedVehicles[0].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED)
						IF NOT IS_BIT_SET(g_MpSavedVehicles[0].iVehicleBS,MP_SAVED_VEHICLE_GIVEN_STATUS_MESSAGE)
							SET_BIT(g_MpSavedVehicles[0].iVehicleBS,MP_SAVED_VEHICLE_GIVEN_STATUS_MESSAGE)
							MP_SAVE_VEHICLE_SLOT_STATS_FROM_SAVEGAME(0,g_MpSavedVehicles[0],TRUE)
							MPGlobals.VehicleData.bTriggerSave = TRUE
						ENDIF				
					ENDIF
						MPGlobals.VehicleData.iMPSVInitialSetupStage = 18
				ENDIF
				IF MPGlobals.VehicleData.iMPSVInitialSetupStage >= 18
					IF MPGlobals.VehicleData.bAddInsuranceContact = TRUE
						ADD_CONTACT_TO_PHONEBOOK(CHAR_MP_MORS_MUTUAL, MULTIPLAYER_BOOK,FALSE) 	
						addedMPInsuranceContact = TRUE
						PRINTLN("[personal_vehicle] Player has insurance adding mors mutual")
					ENDIF
					IF MPGlobals.VehicleData.bTriggerSave
						IF IS_PLAYER_BROWSING_ITEMS_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)
							g_bTriggerCarmodSave = TRUE
						ELSE
							REQUEST_SAVE(SSR_REASON_MP_VEH_MAINTAIN, STAT_SAVETYPE_IMMEDIATE_FLUSH)
						ENDIF
					ENDIF				
					SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_INITIAL_SETUP)	
				ENDIF
			ENDIF			
			
			// should we reset g_i_NumberOfPersonalVehicleReminders ?
			IF (g_i_NumberOfPersonalVehicleReminders > 0)
				BOOL bReset = TRUE
				IF DOES_PLAYER_OWN_ANY_VEHICLE_STORAGE()
					REPEAT MAX_MP_SAVED_VEHICLES i

						IF IS_BIT_SET(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED)
							bReset = FALSE
						ENDIF
						
					ENDREPEAT
				ELSE
					IF IS_BIT_SET(g_MpSavedVehicles[0].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED)
						bReset = FALSE	
					ENDIF
				ENDIF	
				IF (bReset)
					g_i_NumberOfPersonalVehicleReminders = 0
				ENDIF
			ENDIF			
			
			// print first help message. note: all stuff that is not releated to printing help should not be in here.
			IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_FIRST_HELP)
			AND SAFE_TO_PRINT_PV_HELP()
			AND (g_i_NumberOfPersonalVehicleReminders < 1)
				IF CURRENT_SAVED_VEHICLE_SLOT() > -1
				AND IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)
					IF NOT IS_LOCAL_PLAYER_IN_PERSONAL_VEHICLE()
						IF DOES_PLAYER_OWN_ANY_VEHICLE_STORAGE()
							IF IS_THIS_MODEL_ALLOWED_IN_HANGAR(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel)
								PRINT_HELP("HANGAR_DEST")
								PRINTLN("[personal_vehicle]  Telling player their Personal Aircraft was destroyed, call Mors to reclaim")
							ELSE
								IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
									IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_INSURED)
										PRINT_HELP("CUST_GAR_VEH_L1")
										PRINTLN("[personal_vehicle]  Telling player their car was destroyed for being impounded. Can make claim")
									ELSE
										PRINT_HELP("CUST_GAR_VEH_L0")
										PRINTLN("[personal_vehicle]  Telling player their car was destroyed for being impounded. NO claim")	
									ENDIF
								ELSE
									#IF FEATURE_GEN9_EXCLUSIVE
									IF NOT SHOULD_FORCE_RESPAWN_PV()
									#ENDIF
										IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_INSURED)
											PRINT_HELP("CUST_GAR_VEH_L3")
											PRINTLN("[personal_vehicle]  Telling player their car was destroyed. Can make claim")	
										ELSE
											PRINT_HELP("CUST_GAR_VEH_L2")
											PRINTLN("[personal_vehicle]  Telling player their car was destroyed. NO claim")	
										ENDIF
									#IF FEATURE_GEN9_EXCLUSIVE
									ENDIF
									#ENDIF
								ENDIF
							ENDIF
						ELSE
							IF IS_THIS_MODEL_ALLOWED_IN_HANGAR(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel)
								PRINT_HELP("HANGAR_DEST")
								PRINTLN("[personal_vehicle]  Telling player their Personal Aircraft was destroyed, call Mors to reclaim  - no property")
							ELSE
								IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
									IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_INSURED)
										PRINT_HELP("CUST_GAR_VEH_L1")
										PRINTLN("[personal_vehicle]  Telling player their car was destroyed for being impounded. Can make claim - no property")
									ELSE
										PRINT_HELP("CUST_GAR_VEH_L0")
										PRINTLN("[personal_vehicle]  Telling player their car was destroyed for being impounded. NO claim - no property")	
									ENDIF
								ELSE
									#IF FEATURE_GEN9_EXCLUSIVE
									IF NOT SHOULD_FORCE_RESPAWN_PV()
									#ENDIF
										IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_INSURED)
											PRINT_HELP("CUST_GAR_VEH_L3")
											PRINTLN("[personal_vehicle]  Telling player their car was destroyed. Can make claim - no property")	
										ELSE
											PRINT_HELP("CUST_GAR_VEH_L2")
											PRINTLN("[personal_vehicle]  Telling player their car was destroyed. NO claim - no property")	
										ENDIF
									#IF FEATURE_GEN9_EXCLUSIVE
									ENDIF
									#ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				g_i_NumberOfPersonalVehicleReminders++
				
				SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_FIRST_HELP)
			ENDIF
		ELSE	
			
			// clear any vehicles that might be intersecting with this one. only do when in sky cam and player control is off.
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(PERSONAL_VEHICLE_NET_ID())
				IF IS_PLAYER_SWITCH_IN_PROGRESS()
				AND NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())				
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PERSONAL_VEHICLE_ID())										
						CLEAR_ANY_AMBIENT_VEHICLES_FROM_VEHICLE_BOUNDS(PERSONAL_VEHICLE_ID())							
					ENDIF
				ENDIF
			ENDIF
			
		#IF IS_DEBUG_BUILD	
		
			IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_INITIAL_SETUP)
				IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
					PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE: INIT NOT IS_NET_PLAYER_OK")
				ENDIF
				IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
					PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE: INIT NOT IS_PLAYER_CONTROL_ON")
				ENDIF
				IF IS_TRANSITION_ACTIVE()
					PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE: INIT IS_TRANSITION_ACTIVE")
				ENDIF
				IF IS_PLAYER_SWITCH_IN_PROGRESS()
					PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE: INIT IS_PLAYER_SWITCH_IN_PROGRESS")
				ENDIF
			ENDIF
		#ENDIF
		ENDIF
		
		IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE,FALSE)
			MAINTAIN_MP_SAVE_VEHICLE_PUT_VEH_INTO_GARAGE()
			//player is spawning a vehicle from insurance company
			IF morsMutualData.bSpawningVeh
				IF NOT IS_NET_VEHICLE_DRIVEABLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV)
					CLEAR_BIT(g_MpSavedVehicles[morsMutualData.iSlot].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) 
					CLEAR_BIT(g_MpSavedVehicles[morsMutualData.iSlot].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED) 
					CV2_SET_VEHICLE_COUNT_NEEDS_REFRESHED()
					SET_BIT(g_MpSavedVehicles[morsMutualData.iSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE) 
					SET_BIT(g_MpSavedVehicles[morsMutualData.iSlot].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL) 
					MP_SAVE_VEHICLE_SLOT_STATS_FROM_SAVEGAME(morsMutualData.iSlot,g_MpSavedVehicles[morsMutualData.iSlot] ,TRUE)
					PRINTLN("[personal_vehicle] Insurance triggered recreating personal vehicle in slot #",morsMutualData.iSlot)
					morsMutualData.bSpawningVeh = FALSE
				ELSE
					PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE: morsMutualData.bSpawningVeh = TRUE, vehicle is driveable.")
					morsMutualData.bSpawningVeh = FALSE
				ENDIF
			ELIF MPGlobals.VehicleData.bAwaitingAssignToMainScript
				//this is setup for when the player exits property they have created a vehicle and set flags but it doesn't exist to get grabbed yet.
				PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE: MPGlobals.VehicleData.bAwaitingAssignToMainScript = TRUE")
			ELIF MPGlobals.VehicleData.bAssignToMainScript
				
				PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE: MPGlobals.VehicleData.bAssignToMainScript = TRUE")
				IF MPGlobals.VehicleData.bDriveableInFreemode
				AND MPGlobals.VehicleData.bAllowResetWhileAssigning
					IF NOT IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
						MPGlobals.VehicleData.bDriveableInFreemode = FALSE
						PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE: resetting MPGlobals.VehicleData.bDriveableInFreemode while assigning vehicle")
					ENDIF
				ENDIF		
						
				IF HAS_PERSONAL_VEHICLE_BEEN_REQUESTED_TO_WARP_NEAR()
					NET_PRINT("[personal_vehicle]  clearing warp near flag - 1") NET_NL()
					CLEAR_REQUEST_TO_WARP_PERSONAL_VEHICLE_NEAR()
				ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					tempveh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(tempveh)
					AND IS_VEHICLE_DRIVEABLE(tempveh)
					AND NETWORK_GET_ENTITY_IS_NETWORKED(tempveh)
						IF SET_VEHICLE_AS_CURRENT_PERSONAL_VEHICLE(tempveh)
							UPDATE_MP_SAVED_VEHICLE_DECORATOR()
							INT iDecoratorValue
							IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
								IF DECOR_EXIST_ON(tempveh, "MPBitset")	
									iDecoratorValue = DECOR_GET_INT(tempveh, "MPBitset")
								ENDIF
								PRINTLN("[personal_vehicle] clearing MP_DECORATOR_BS_VEHICLE_LEAVING_GARAGE")
								CLEAR_BIT(iDecoratorValue, MP_DECORATOR_BS_VEHICLE_LEAVING_GARAGE)
								DECOR_SET_INT(tempveh, "MPBitset", iDecoratorValue)
							ENDIF
							SET_PV_ASSIGN_TO_MAIN_SCRIPT(FALSE)
							PRINTLN("[personal_vehicle] Clearing bAssignToMainScript because vehicle set as personal vehicle")
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("[personal_vehicle] bAssignToMainScript cleared because...")
							IF NOT DOES_ENTITY_EXIST(tempveh)
								PRINTLN("[personal_vehicle] tempVeh doesn't exist!")
							ENDIF
							IF NOT IS_VEHICLE_DRIVEABLE(tempveh)
								PRINTLN("[personal_vehicle] tempVeh isn't driveable!")
							ENDIF
							IF NOT NETWORK_GET_ENTITY_IS_NETWORKED(tempveh)
								PRINTLN("[personal_vehicle] tempVeh isn't networked!")
							ENDIF
						#ENDIF
						SET_PV_ASSIGN_TO_MAIN_SCRIPT(FALSE)
					ENDIF
				ELSE
					PRINTLN("[personal_vehicle] bAssignToMainScript cleared, not in a vehicle")
					SET_PV_ASSIGN_TO_MAIN_SCRIPT(FALSE)
				ENDIF
				
				//MPGlobals.VehicleData.bCrewEmblem = FALSE

				
			ELIF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())

				MPGlobals.VehicleData.bDriveableInFreemode = TRUE
				CLEAR_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_CREATION_INIT)

				UPDATE_MP_SAVED_VEHICLE_DECORATOR()
				
				tempveh= PERSONAL_VEHICLE_ID()
				
				
				IF IS_VEHICLE_DRIVEABLE(tempveh)
				AND IS_VEHICLE_A_PERSONAL_VEHICLE(tempveh)		
				AND IS_VEHICLE_MY_PERSONAL_VEHICLE(tempveh)
					MAINTAIN_CLEANUP_MP_SAVED_VEHICLE_IN_INVALID_INTERIOR()
					
					#IF FEATURE_CASINO
					MAINTAIN_WARPS_FOR_CASINO_CAR_PARK()
					#ENDIF
					
					#IF FEATURE_TUNER
					MAINTAIN_WARPS_FOR_CAR_MEET()
					
					MAINTAIN_WARPS_FOR_PRIVATE_CAR_MEET()
					#ENDIF
					
					IF MPGlobalsAmbience.bForceOppressor2CDOnCreate 
						IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
							IF g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel = OPPRESSOR2
								REINIT_NET_TIMER(oppressor2BikeCooldownTimer,TRUE)
								PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - office - setting oppressor2BikeCooldownTimer ")
								MPGlobalsAmbience.bForceOppressor2CDOnCreate = FALSE
								PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - MPGlobalsAmbience.bForceOppressor2CDOnCreate = FALSE ")
							ELSE
								PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE - CURRENT VEHICLE NOT AN OPPRESSOR2 MPGlobalsAmbience.bForceOppressor2CDOnCreate = FALSE ")
								MPGlobalsAmbience.bForceOppressor2CDOnCreate = FALSE
							ENDIF
						ENDIF
					ENDIF
					IF IS_VEHICLE_A_CONVERTIBLE(tempveh)
						CONVERTIBLE_ROOF_STATE tempRoofState = GET_CONVERTIBLE_ROOF_STATE(tempveh)
						IF tempRoofState = CRS_LOWERING
						OR tempRoofState = CRS_LOWERED
						OR tempRoofState =CRS_ROOF_STUCK_LOWERED
							SET_BIT(g_TransitionSessionNonResetVars.MissionPVBitset,ci_PV_CONV_ROOF_DOWN)
						ELSE
							CLEAR_BIT(g_TransitionSessionNonResetVars.MissionPVBitset,ci_PV_CONV_ROOF_DOWN)
						ENDIF
					ELSE
						CLEAR_BIT(g_TransitionSessionNonResetVars.MissionPVBitset,ci_PV_CONV_ROOF_DOWN)
					ENDIF

					// Clear up stored vehicle if put in for repair or flagged for cleanup
					IF IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP)
						CLEANUP_MP_SAVED_VEHICLE()
						IF HAS_PERSONAL_VEHICLE_BEEN_REQUESTED_TO_WARP_NEAR()
							NET_PRINT("[personal_vehicle]  clearing warp near flag - 2") NET_NL()
							CLEAR_REQUEST_TO_WARP_PERSONAL_VEHICLE_NEAR()
						ENDIF
					ELIF IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP_DELETE)
						//PRINTLN("Cleaning up personal vehicle as IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP_DELETE)")
						CLEANUP_MP_SAVED_VEHICLE(TRUE)
						IF HAS_PERSONAL_VEHICLE_BEEN_REQUESTED_TO_WARP_NEAR()
							NET_PRINT("[personal_vehicle]  clearing warp near flag - 3") NET_NL()
							CLEAR_REQUEST_TO_WARP_PERSONAL_VEHICLE_NEAR()
						ENDIF
					ELSE
						IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
							IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_HAS_CREW_EMBLEM)
								MANAGE_CREW_EMBLEM(tempveh)
							ENDIF
							
							// Re-apply mods to the vehicle
							IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_REAPPLY_MODS)
								MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(tempveh,CURRENT_SAVED_VEHICLE_SLOT())
								CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_REAPPLY_MODS)
							ENDIF
						ENDIF
						
						// Make sure vehicle doesn't fall through the ground when the switch cam is active.
						IF IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(tempveh)
							AND HAS_COLLISION_LOADED_AROUND_ENTITY(tempveh)
								FREEZE_ENTITY_POSITION(tempveh, FALSE)
								SET_VEHICLE_ON_GROUND_PROPERLY(tempveh)
								CLEAR_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
								NET_PRINT("[personal_vehicle]  collision - unfreezeing vehicle") NET_NL()
							ENDIF
						ELSE
							IF IS_NET_PLAYER_OK(PLAYER_ID())
							AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempveh)
							AND IS_PLAYER_SWITCH_IN_PROGRESS()
							AND NETWORK_HAS_CONTROL_OF_ENTITY(tempveh)
							AND NOT HAS_COLLISION_LOADED_AROUND_ENTITY(tempveh)
								FREEZE_ENTITY_POSITION(tempveh, TRUE)
								SET_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
								NET_PRINT("[personal_vehicle]  collision - freezeing vehicle") NET_NL()
							ENDIF
						ENDIF
						
						// Warp near if we are decending from a switch.
						IF NOT HAS_PERSONAL_VEHICLE_BEEN_REQUESTED_TO_WARP_NEAR()
						AND NOT MPGlobals.VehicleData.bSwitchWarpProcessed
						AND IS_PLAYER_SWITCH_IN_PROGRESS()
						AND GET_PLAYER_SWITCH_STATE() >= SWITCH_STATE_JUMPCUT_DESCENT
						AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), tempveh, FALSE) > 50
						AND NOT IS_CURRENT_PERSONAL_VEHICLE_IMPOUNDED()
							NET_PRINT("[personal_vehicle]  warping near the player for start of session") NET_NL()
							REQUEST_PERSONAL_VEHICLE_TO_WARP_NEAR()
							MPGlobals.VehicleData.bSwitchWarpProcessed = TRUE
						ENDIF
						
						IF MPGlobals.VehicleData.bSwitchWarpProcessed
						AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
							NET_PRINT("[personal_vehicle]  clearing switch warp flag") NET_NL()
							MPGlobals.VehicleData.bSwitchWarpProcessed = FALSE
						ENDIF
						
						// In vehicle so remove blip and unlock doors
//						IF IS_NET_PLAYER_OK(PLAYER_ID())														
							
							//Update Lock State
							IF MPGlobalsAmbience.iStoredAllowIntoPV != GET_MP_INT_CHARACTER_STAT(MP_STAT_PERSONAL_VEHICLE_ACCESS)
							AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)
							AND NOT IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
							AND NOT IS_PLAYER_EXITING_PROPERTY(PLAYER_ID())
							#IF FEATURE_GEN9_EXCLUSIVE
							AND NOT IS_PLAYER_ON_MP_INTRO()
							#ENDIF
								IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
								AND NOT IS_PLAYER_SCTV(PLAYER_ID())
								AND NOT IS_A_SPECTATOR_CAM_RUNNING()
									IF NETWORK_GET_ENTITY_IS_NETWORKED(tempveh)
										//IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempveh)
										//	NET_PRINT("[personal_vehicle]  Requesting control of saved vehicle - B") NET_NL()
										//	NETWORK_REQUEST_CONTROL_OF_ENTITY(tempveh)
										//ELSE
											SET_PERSONAL_VEHICLE_LOCK_STATE(tempveh)
										//ENDIF
									ENDIF
								ENDIF
							ELIF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV) 
							AND IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
							AND NOT IS_PLAYER_EXITING_PROPERTY(PLAYER_ID())
								IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), tempveh)
									IF MPGlobalsAmbience.iStoredAllowIntoPV != APV_CLUBHOUSE_UNLOCK
//										IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempveh)
//											NETWORK_REQUEST_CONTROL_OF_ENTITY(tempveh)
//										ELSE
											SET_PERSONAL_VEHICLE_LOCK_STATE(tempveh, DEFAULT, APV_CLUBHOUSE_UNLOCK)
//										ENDIF
									ENDIF
								ELSE
									IF MPGlobalsAmbience.iStoredAllowIntoPV != APV_CLUBHOUSE_LOCK
//										IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempveh)
//											NETWORK_REQUEST_CONTROL_OF_ENTITY(tempveh)
//										ELSE
											SET_PERSONAL_VEHICLE_LOCK_STATE(tempveh, DEFAULT, APV_CLUBHOUSE_LOCK)
//										ENDIF
									ENDIF
								ENDIF
							ELIF MPGlobalsAmbience.iStoredAllowIntoPV != GET_MP_INT_CHARACTER_STAT(MP_STAT_PERSONAL_VEHICLE_ACCESS)
							AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)
							AND (IS_PV_IN_CAR_MEET_GAR() OR IS_PV_IN_PRIVATE_CAR_MEET_GAR())
								SET_PERSONAL_VEHICLE_LOCK_STATE(tempveh)
							ELSE
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)
								AND NOT (IS_PV_IN_CAR_MEET_GAR() OR IS_PV_IN_PRIVATE_CAR_MEET_GAR())
									IF NOT (MPGlobalsAmbience.iStoredAllowIntoPV = -1)
										MPGlobalsAmbience.iStoredAllowIntoPV = -1	
										PRINTLN("[personal_vehicle] personal vehicle is concealed, setting MPGlobalsAmbience.iStoredAllowIntoPV = -1")
									ENDIF
								ENDIF
							ENDIF
							
							#IF IS_DEBUG_BUILD
								
								IF MPGlobalsAmbience.iStoredAllowIntoPV != GET_MP_INT_CHARACTER_STAT(MP_STAT_PERSONAL_VEHICLE_ACCESS)
									PRINTLN("[personal_vehicle] MPGlobalsAmbience.iStoredAllowIntoPV doesnt match! , MPGlobalsAmbience.iStoredAllowIntoPV = ", MPGlobalsAmbience.iStoredAllowIntoPV)
								
							
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)
										PRINTLN("[personal_vehicle] IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo, PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)")
									ENDIF
									
									IF IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
										PRINTLN("[personal_vehicle] IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)")										
									ENDIF
									
									IF IS_PLAYER_EXITING_PROPERTY(PLAYER_ID())
										PRINTLN("[personal_vehicle] IS_PLAYER_EXITING_PROPERTY(PLAYER_ID())")										
									ENDIF
									
									IF IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
										PRINTLN("[personal_vehicle] IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)")	
									ENDIF
									
								ENDIF									
							#ENDIF							
							
							PROCESS_MP_SAVED_VEHICLE_FLAGS(tempveh)
							
							PROCESS_MP_SAVED_VEHICLE_MODS(tempveh)
							
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempveh)
								
								IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
								
									// Store/restore radio station
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(PERSONAL_VEHICLE_NET_ID())
										IF NOT IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_RADIO_STATION_SET)
											IF NOT IS_STRING_NULL_OR_EMPTY(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].tlRadioStationName)
												SET_VEH_RADIO_STATION(tempveh, g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].tlRadioStationName)
											ENDIF
											SET_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_RADIO_STATION_SET)
										ELSE
											g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].tlRadioStationName = GET_PLAYER_RADIO_STATION_NAME()
										ENDIF
									ENDIF
									
									// Picked up from Mors Mutual
									CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL)
								ENDIF
								
								IF MPGlobals.VehicleData.bLockDoor
									IF NOT MPGlobals.VehicleData.bDoorLocked
										//IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempveh)
										//	NET_PRINT("[personal_vehicle]  Requesting control of saved vehicle to lock door for hooker") NET_NL()
										//	NETWORK_REQUEST_CONTROL_OF_ENTITY(tempveh)
										//ELSE
											SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempveh, TRUE)
											SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(tempveh, PLAYER_ID(), FALSE)
											MPGlobals.VehicleData.bDoorLocked = TRUE
											NET_PRINT("[personal_vehicle]  Doors unlocked for all players as with hooker")NET_NL()
										//ENDIF
									ENDIF
								ELSE
									IF MPGlobals.VehicleData.bDoorLocked
										//IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempveh)
										//	NET_PRINT("[personal_vehicle]  Requesting control of saved vehicle to unlock door after hooker") NET_NL()
										//	NETWORK_REQUEST_CONTROL_OF_ENTITY(tempveh)
										//ELSE
											//SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempveh, FALSE)
											SET_PERSONAL_VEHICLE_LOCK_STATE(tempveh)
											MPGlobals.VehicleData.bDoorLocked = FALSE
											NET_PRINT("[personal_vehicle]  Doors unlocked for all players after hooker")NET_NL()
										//ENDIF
									ENDIF
								ENDIF
								
								IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iPersonalVehicleFlagsBS, PERSONAL_VEHICLE_BD_FLAG_IN_VEHICLE)
									PRINTLN("[personal_vehicle] MP CDM... setting player is in their personal vehicle")
									SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iPersonalVehicleFlagsBS, PERSONAL_VEHICLE_BD_FLAG_IN_VEHICLE)
								ENDIF
								
								IF GlobalServerBD_FM.currentBounties[NATIVE_TO_INT(PLAYER_ID())].bTargeted
									IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iPersonalVehicleFlagsBS, PERSONAL_VEHICLE_BD_FLAG_IN_VEHICLE_WITH_BOUNTY)
										PRINTLN("[personal_vehicle] MP CDM... setting player is in their personal vehicle with bounty set")
										SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iPersonalVehicleFlagsBS, PERSONAL_VEHICLE_BD_FLAG_IN_VEHICLE_WITH_BOUNTY)
									ENDIF
								ELSE
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iPersonalVehicleFlagsBS, PERSONAL_VEHICLE_BD_FLAG_IN_VEHICLE_WITH_BOUNTY)
										PRINTLN("[personal_vehicle] MP CDM... setting player is in their personal vehicle with NO bounty set")
										CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iPersonalVehicleFlagsBS, PERSONAL_VEHICLE_BD_FLAG_IN_VEHICLE_WITH_BOUNTY)
									ENDIF
								ENDIF
								IF Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID())
									SET_BIT(g_TransitionSessionNonResetVars.MissionPVBitset, ciMISSION_WARPED_INTO_PV)
								ELSE
									IF HAS_PERSONAL_VEHICLE_BEEN_REQUESTED_TO_WARP_NEAR()
										NET_PRINT("[personal_vehicle]  clearing warp near flag - 4") NET_NL()
										CLEAR_REQUEST_TO_WARP_PERSONAL_VEHICLE_NEAR()
									ENDIF
								ENDIF
								
								IF HAS_NET_TIMER_STARTED(MPGlobals.VehicleData.ivehwarptime)
									NET_PRINT("[personal_vehicle]  warp timer set to 0 (player in)") NET_NL()
									RESET_NET_TIMER(MPGlobals.VehicleData.ivehwarptime)
								ENDIF
								
								REMOVE_PV_BLIP(2)
								
								
							// Out vehicle	
							ELSE
							
								MPGlobals.VehicleData.bLockDoor = FALSE
								
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iPersonalVehicleFlagsBS, PERSONAL_VEHICLE_BD_FLAG_IN_VEHICLE)
									PRINTLN("[personal_vehicle] MP CDM... setting player is NOT in their personal vehicle")
									CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iPersonalVehicleFlagsBS, PERSONAL_VEHICLE_BD_FLAG_IN_VEHICLE)
									CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iPersonalVehicleFlagsBS, PERSONAL_VEHICLE_BD_FLAG_IN_VEHICLE_WITH_BOUNTY)
								ENDIF
								IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeginningMissionInApartment
									IF Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID())
									AND NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION() 
									AND NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE() 
									AND NOT IS_PLAYER_IN_CORONA()
										IF g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bFinishedInPersonalVehicle
											IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.MissionPVBitset, ciMISSION_WARPED_INTO_PV)
												CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
												IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
													FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
												ENDIF
												TASK_ENTER_VEHICLE(PLAYER_PED_ID(), tempveh, 1, VS_DRIVER, PEDMOVE_RUN, ECF_WARP_PED)
												PRINTLN("[personal_vehicle] TASK_ENTER_VEHICLE bFinishedInPersonalVehicle is true")
												SET_BIT(g_TransitionSessionNonResetVars.MissionPVBitset, ciMISSION_WARPED_INTO_PV)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								// Add/remove blip
								IF (NETWORK_IS_IN_TUTORIAL_SESSION() #IF FEATURE_GEN9_EXCLUSIVE AND NOT IS_PLAYER_ON_MP_INTRO() #ENDIF )
								OR IS_PLAYER_SCTV(PLAYER_ID())
								OR IS_A_SPECTATOR_CAM_RUNNING()
								OR Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
								OR IS_ON_RACE_GLOBAL_SET()
								OR (SHOULD_PV_BLIP_BE_HIDDEN_FOR_PROPERTY() AND NOT IS_PLAYER_IN_BUNKER_THEY_OWN(PLAYER_ID()) #IF FEATURE_TUNER AND NOT IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID()) #ENDIF )
								OR IS_VEHICLE_BEING_USED_FOR_TIME_TRIAL(tempveh)
								OR SHOULD_PV_BLIP_BE_HIDDEN_FOR_MISSION_INTERIOR()
								OR SHOULD_PV_BLIP_BE_HIDDEN_DUE_TO_ZONE()
								#IF FEATURE_CASINO
								OR SHOULD_PV_BLIP_BE_HIDDEN_FOR_CASINO()
								#ENDIF
								#IF FEATURE_TUNER
								OR SHOULD_PV_BLIP_BE_HIDDEN_FOR_CAR_MEET()
								#ENDIF
								//OR IS_SHIPMENT_PLAYER_IN_VEHICLE(tempveh)								
									IF DOES_BLIP_EXIST(MPGlobals.VehicleData.blipID)
										
										#IF IS_DEBUG_BUILD
											IF NETWORK_IS_IN_TUTORIAL_SESSION()
												PRINTLN("[personal_vehicle] KR_MP: Removing blip as player is in tutorial session")
											ELIF IS_PLAYER_SCTV(PLAYER_ID())
												PRINTLN("[personal_vehicle] KR_MP: Removing blip as player is SCTV")
											ELIF IS_A_SPECTATOR_CAM_RUNNING()
												PRINTLN("[personal_vehicle] KR_MP: Removing blip as spectator cam is running")
											ELIF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
												PRINTLN("[personal_vehicle] KR_MP: Removing blip as player is on LTS mission")
											ELIF IS_ON_RACE_GLOBAL_SET()
												PRINTLN("[personal_vehicle] KR_MP: Removing blip as player is on a race")
											ELIF (SHOULD_PV_BLIP_BE_HIDDEN_FOR_PROPERTY() AND NOT IS_PLAYER_IN_BUNKER_THEY_OWN(PLAYER_ID()))
												PRINTLN("[personal_vehicle] KR_MP: SHOULD_PV_BLIP_BE_HIDDEN_FOR_PROPERTY")	
											ELIF IS_VEHICLE_BEING_USED_FOR_TIME_TRIAL(tempveh)
												PRINTLN("[personal_vehicle] KR_MP: IS_VEHICLE_BEING_USED_FOR_TIME_TRIAL")	
											ELIF SHOULD_PV_BLIP_BE_HIDDEN_FOR_MISSION_INTERIOR()
												PRINTLN("[personal_vehicle] KR_MP: SHOULD_PV_BLIP_BE_HIDDEN_FOR_MISSION_INTERIOR")	
											ELIF SHOULD_PV_BLIP_BE_HIDDEN_DUE_TO_ZONE()
												PRINTLN("[personal_vehicle] KR_MP: SHOULD_PV_BLIP_BE_HIDDEN_DUE_TO_ZONE")	
											#IF FEATURE_CASINO
											ELIF SHOULD_PV_BLIP_BE_HIDDEN_FOR_CASINO()
												PRINTLN("[personal_vehicle] KR_MP: SHOULD_PV_BLIP_BE_HIDDEN_FOR_CASINO")	
											#ENDIF
											#IF FEATURE_TUNER
											ELIF SHOULD_PV_BLIP_BE_HIDDEN_FOR_CAR_MEET()
												PRINTLN("[personal_vehicle] KR_MP: SHOULD_PV_BLIP_BE_HIDDEN_FOR_CAR_MEET")
											#ENDIF
											ENDIF
										#ENDIF
										
										REMOVE_PV_BLIP(3)
									ENDIF
								ELSE
									IF NOT DOES_BLIP_EXIST(MPGlobals.VehicleData.blipID)
										
										MPGlobals.VehicleData.blipID = ADD_BLIP_FOR_ENTITY(tempveh)
																				
										SET_BLIP_SPRITE(MPGlobals.VehicleData.blipID, GET_PEGASUS_BLIP_SPRITE_FOR_MODEL(GET_ENTITY_MODEL(tempveh)))
										
//										IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(tempveh))
//											SET_BLIP_SPRITE(MPGlobals.VehicleData.blipID, RADAR_TRACE_HELICOPTER)
//										ELIF IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(tempveh))
//											SET_BLIP_SPRITE(MPGlobals.VehicleData.blipID, RADAR_TRACE_BOAT)
//										ELIF IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(tempveh))
//											SET_BLIP_SPRITE(MPGlobals.VehicleData.blipID, RADAR_TRACE_QUAD)
//										#IF FEATURE_IMPORT_EXPORT
//										ELIF IS_THIS_MODEL_A_SPECIAL_VEHICLE(GET_ENTITY_MODEL(tempveh))
//											SET_BLIP_SPRITE(MPGlobals.VehicleData.blipID, GET_BLIP_SPRITE_FOR_SPECIAL_VEHICLE_MODEL(GET_ENTITY_MODEL(tempveh)))
//										#ENDIF
//										#IF FEATURE_GUNRUNNING
//										ELIF GET_WVM_VEHICLE_INDEX(GET_ENTITY_MODEL(tempveh)) != -1
//											SET_BLIP_SPRITE(MPGlobals.VehicleData.blipID, GET_BLIP_SPRITE_FOR_SPECIAL_VEHICLE_MODEL(GET_ENTITY_MODEL(tempveh)))
//										#ENDIF
//										ELIF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(tempveh))
//											SET_BLIP_SPRITE(MPGlobals.VehicleData.blipID, RADAR_TRACE_GANG_VEHICLE_BIKERS)											
//										ELSE										
//											SET_BLIP_SPRITE(MPGlobals.VehicleData.blipID, RADAR_TRACE_GANG_VEHICLE)
//										ENDIF
										
										SET_BLIP_PRIORITY(MPGlobals.VehicleData.blipID, BLIPPRIORITY_HIGH)
										
										IF IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(tempveh))
											SET_BLIP_NAME_FROM_TEXT_FILE(MPGlobals.VehicleData.blipID, "PAIRCRAFT")
											
											IF MPGlobalsAmbience.bLaunchVehicleDropPersonalAircraft
												PRINT_HELP(GET_PERSONAL_AIRCRAFT_DELIVERY_MESSAGE(tempveh))
											ENDIF
										ELSE
											IF IS_THIS_MODEL_A_SPECIAL_VEHICLE(GET_ENTITY_MODEL(tempveh))
											OR GET_WVM_VEHICLE_INDEX(GET_ENTITY_MODEL(tempveh)) != -1
											OR WAM_GET_STEAL_MISSION_UNLOCK_COUNT_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(tempveh)) > 0
												IF NOT SET_CUSTOM_BLIP_NAME_FROM_MODEL(MPGlobals.VehicleData.blipID, GET_ENTITY_MODEL(tempveh))
													SET_BLIP_NAME_FROM_TEXT_FILE(MPGlobals.VehicleData.blipID,"PVEHICLE")
												ENDIF
											ELSE								
												SET_BLIP_NAME_FROM_TEXT_FILE(MPGlobals.VehicleData.blipID,"PVEHICLE")
											ENDIF
										ENDIF
										
										NET_PRINT("[personal_vehicle] Adding vehicle blip ") NET_PRINT(GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(tempveh)) NET_PRINT(" ") NET_PRINT(GET_BLIP_SPRITE_DEBUG_STRING(GET_BLIP_SPRITE(MPGlobals.VehicleData.blipID))) NET_NL()
									
									ELSE
										// if the blip does exist but is somehow not on this vehicle then remove it. 3161065
										IF (GET_BLIP_INFO_ID_TYPE(MPGlobals.VehicleData.blipID) = BLIPTYPE_VEHICLE)
											IF NOT (tempveh = GET_BLIP_INFO_ID_ENTITY_INDEX(MPGlobals.VehicleData.blipID))
												PRINTLN("[personal_vehicle]  existing blip is not on correct entity, removing.")
												REMOVE_PV_BLIP(4)
											ENDIF
										ELSE
											REMOVE_PV_BLIP(5)
											PRINTLN("[personal_vehicle]  existing blip is wrong type, removing.")
										ENDIF
									ENDIF
									
									// does the blip need rotated?
									IF DOES_BLIP_EXIST(MPGlobals.VehicleData.blipID)
										IF SHOULD_BLIP_BE_MANUALLY_ROTATED(MPGlobals.VehicleData.blipID)
											SET_BLIP_ROTATION(MPGlobals.VehicleData.blipID, ROUND(GET_ENTITY_HEADING_FROM_EULERS(tempveh)))	
										ENDIF
										
										HIDE_VEHICLE_BLIP_ON_CERTAIN_ROOFS(tempveh, MPGlobals.VehicleData.blipID)
									ENDIF
									
									// flash the pv if requested.
									IF DOES_BLIP_EXIST(MPGlobals.VehicleData.blipID)
										IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_FLASH_BLIP)
											SET_BLIP_FLASHES(MPGlobals.VehicleData.blipID, TRUE)
											SET_BLIP_FLASH_TIMER(MPGlobals.VehicleData.blipID, 7000)
											CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
											PRINTLN("[personal_vehicle] started flashing pv blip")
										ENDIF
									ENDIF
									
									// set personal vehicle blip colour
									IF DOES_BLIP_EXIST(MPGlobals.VehicleData.blipID)										
										IF IS_THREAD_ACTIVE(MPGlobals.VehicleData.threadIDCustomColour)
											SET_BLIP_COLOUR(MPGlobals.VehicleData.blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobals.VehicleData.blipCustomColour) )
										ELSE
											IF NOT (MPGlobals.VehicleData.blipCustomColour = HUD_COLOUR_PURE_WHITE)
												PRINTLN("[personal_vehicle] threadIDCustomColour no longer active")
												REMOVE_PV_BLIP(6)
												MPGlobals.VehicleData.blipCustomColour = HUD_COLOUR_PURE_WHITE
											ENDIF
										ENDIF	
									ENDIF
									
									IF DOES_BLIP_EXIST(MPGlobals.VehicleData.blipID)
										IF (g_PlayerBlipsData.bSpriteBlipPlayerIsInMyPersonalVehicle)
										OR (g_PlayerBlipsData.bHidePersonalVehicleBlip)
											SET_BLIP_ALPHA(MPGlobals.VehicleData.blipID, 1)
											PRINTLN("[personal_vehicle] setting blip alpha to 1 because of bSpriteBlipPlayerIsInMyPersonalVehicle")
										ELSE
											SET_BLIP_ALPHA(MPGlobals.VehicleData.blipID, 255)
										ENDIF
									ENDIF
									
									IF HAS_PERSONAL_VEHICLE_BEEN_REQUESTED_TO_WARP_NEAR()
										IF IS_VEHICLE_EMPTY(tempveh,TRUE)
										AND NOT HAS_PLAYER_STARTED_WARP_INTO_CAR_MEET_CAR_PARK()
										AND NOT IS_PV_IN_CAR_MEET_GAR()
											IF WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY()
												NET_PRINT("[personal_vehicle]  clearing warp near flag - 5") NET_NL()
												CLEAR_REQUEST_TO_WARP_PERSONAL_VEHICLE_NEAR()
											ENDIF
										ELSE
											NET_PRINT("[personal_vehicle]  clearing warp near flag - 6") NET_NL()
											CLEAR_REQUEST_TO_WARP_PERSONAL_VEHICLE_NEAR()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
//						ENDIF
					ENDIF
				ELSE
				
					#IF IS_DEBUG_BUILD
						IF NOT IS_VEHICLE_DRIVEABLE(tempveh)
							NET_PRINT("[personal_vehicle] IS_VEHICLE_DRIVEABLE(tempveh) = FALSE") NET_NL()
						ENDIF
						IF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(tempveh)		
							NET_PRINT("[personal_vehicle] IS_VEHICLE_A_PERSONAL_VEHICLE(tempveh) = FALSE") NET_NL()
						ENDIF
						IF NOT IS_VEHICLE_MY_PERSONAL_VEHICLE(tempveh)
							NET_PRINT("[personal_vehicle] IS_VEHICLE_MY_PERSONAL_VEHICLE(tempveh) = FALSE") NET_NL()
						ENDIF						
					#ENDIF
				
					IF HAS_PERSONAL_VEHICLE_BEEN_REQUESTED_TO_WARP_NEAR()
						NET_PRINT("[personal_vehicle]  clearing warp near flag - 7") NET_NL()
						CLEAR_REQUEST_TO_WARP_PERSONAL_VEHICLE_NEAR()
					ENDIF
				ENDIF
				
			ELSE
				
				REMOVE_PV_BLIP(7)
				
				IF HAS_PERSONAL_VEHICLE_BEEN_REQUESTED_TO_WARP_NEAR()
					NET_PRINT("[personal_vehicle]  clearing warp near flag - 8") NET_NL()
					CLEAR_REQUEST_TO_WARP_PERSONAL_VEHICLE_NEAR()
				ENDIF
				//MPGlobals.VehicleData.bCrewEmblem = FALSE
				#IF FEATURE_CASINO
				CLEAR_WARP_INTO_CASINO_VALET_CAR_PARK()
				
				#IF FEATURE_TUNER
				IF NOT MPGlobalsAmbience.bLaunchVehicleDropPersonal
					CLEAR_WARP_INTO_CAR_MEET_CAR_PARK()
					
					CLEAR_WARP_INTO_PRIVATE_CAR_MEET_CAR_PARK()
				ENDIF
				#ENDIF
				
				CLEAR_CONCEAL_CURRENT_PERSONAL_VEHICLE()
				#ENDIF
				// First time the created vehicle is dead
				IF IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_CREATED)
					IF NETWORK_DOES_NETWORK_ID_EXIST(PERSONAL_VEHICLE_NET_ID())
						IF NOT IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID(),TRUE)  //1576601 setup to only process the destroyed stuff is in Freemode or contact mission
						OR (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
						AND NOT Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())
						AND NOT Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
						AND NOT Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID()))
						AND NOT (g_SpawnData.MissionSpawnDetails.bUsePersonalVehicleToSpawn)
							IF NOT IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_DESTROYED_HELP)
								IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
									IF NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED)
										IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
											IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_CLEANUP_RETURN_TO_GARAGE)
												IF NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_INSURED)
													NET_PRINT("[personal_vehicle]  Vehicle has just been destroyed")  NET_NL()
													PRINTLN("[personal_vehicle] MAINTAIN_MP_SAVED_VEHICLE: setting vehicle in slot # ", CURRENT_SAVED_VEHICLE_SLOT(),"DESTROYED not driveable")
													
													IF NOT IS_THIS_MODEL_ALLOWED_IN_HANGAR(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.vehicleSetup.eModel)
														IF CURRENT_SAVED_VEHICLE_SLOT() >= 10
															PRINT_HELP("CUST_GAR_VEH_L5")
														ELSE
															IF NOT MP_SAVE_VEHICLE_IS_SAVE_SLOT_A_CYCLE(CURRENT_SAVED_VEHICLE_SLOT())
															
																PRINT_HELP("CUST_GAR_VEH_L2")
															ENDIF
														ENDIF
													ENDIF
													
													// Remove the vehicle from the CarApp too.
													INT iCarAppSlot
													IF GET_CAR_APP_SLOT_FOR_SAVE_VEHICLE(CURRENT_SAVED_VEHICLE_SLOT(), iCarAppSlot)
														SET_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS, MP_SAVED_VEHICLE_CARAPP_DELETE)
													ENDIF
													SET_SAVE_VEHICLE_AS_DESTROYED(CURRENT_SAVED_VEHICLE_SLOT())
													TRIGGER_CHECK_FOR_VEHICLE_ACCESS_STAT()
													
													
												ELSE
													NET_PRINT("[personal_vehicle]  Vehicle has just been destroyed but its insured")  NET_NL()
													SET_SAVE_VEHICLE_AS_DESTROYED(CURRENT_SAVED_VEHICLE_SLOT())
													IF NOT IS_THIS_MODEL_A_SPECIAL_VEHICLE(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel)
													AND NOT IS_THIS_MODEL_ALLOWED_IN_HANGAR(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel)
														IF NOT MP_SAVE_VEHICLE_IS_SAVE_SLOT_A_CYCLE(CURRENT_SAVED_VEHICLE_SLOT())
															SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_DO_DESTROYED_PHONE_CALL_GEN)
														ENDIF
													ENDIF
												ENDIF
											ELSE
												NET_PRINT("[personal_vehicle]  Vehicle has just been destroyed but it was in process of being put back into garage.")  NET_NL()
											ENDIF
										ELSE
											NET_PRINT("[personal_vehicle]  Vehicle has just been destroyed but it was put back in garage first.")  NET_NL()
										ENDIF
										SET_SAVED_VEHICLE_FLAG( MP_SAVED_VEH_FLAG_DESTROYED_HELP)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						NET_PRINT("[personal_vehicle] created flag is set, but entity does not exist. cleaning up.")  NET_NL()
						CLEANUP_MP_SAVED_VEHICLE()
					ENDIF
				ENDIF
				
				IF MPGlobals.VehicleData.bDriveableInFreemode
					CLEANUP_MP_SAVED_VEHICLE()
					MPGlobals.VehicleData.bDriveableInFreemode = FALSE
				ENDIF	
				
				IF IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION(TRUE, DEFAULT, FALSE)
				
					vSpawnCoords = GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID())
					NET_PRINT("[personal_vehicle] initial entity coords =  ") NET_PRINT_VECTOR(vSpawnCoords) NET_NL()
																		
					IF IS_OK_TO_SPAWN_VEHICLE_AT_START_OF_HEIST_MISSION()
						vSpawnCoords = mpProperties[g_SpawnData.MissionSpawnPersonalVehicleData.iPVPropertySpawn].vBlipLocation[0]
						PRINTLN("[personal_vehicle] spawning at start of heist mission, using iPVPropertySpawn ", g_SpawnData.MissionSpawnPersonalVehicleData.iPVPropertySpawn, ", vSpawnCoords = ", vSpawnCoords)
					ENDIF
					
					vPvSpawnOverride = GET_PERSONAL_VEHICLE_SPAWN_OVERRIDE_VECTOR()
					
					IF VMAG(vPvSpawnOverride) > 0.0
						vSpawnCoords = vPvSpawnOverride
						PRINTLN("[personal_vehicle] using g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPvSpawnPosOverride = ", vPvSpawnOverride)
						fSpawnHeading = GET_PERSONAL_VEHICLE_SPAWN_OVERRIDE_HEADING()
						PRINTLN("[personal_vehicle] using g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPvSpawnHeadingOverride = ", fSpawnHeading)
					ENDIF
					
					IF VMAG(vSpawnCoords) < 0.1
					OR IS_POINT_NEAR_SPECTATOR_HIDE_POSITION(vSpawnCoords)
						PRINTLN("[personal_vehicle] initial coords not goood") 
						IF (VMAG(g_SpawnData.vMyDeadCoords) > 0.1)
							vSpawnCoords = g_SpawnData.vMyDeadCoords
							PRINTLN("[personal_vehicle] using g_SpawnData.vMyDeadCoords = ", vSpawnCoords) 
						ELSE
							vSpawnCoords = GET_MP_VECTOR_CHARACTER_STAT(MP_STAT_FM_SPAWN_POSITION)
							PRINTLN("[personal_vehicle] using MP_STAT_FM_SPAWN_POSITION = ", vSpawnCoords) 
						ENDIF
					ENDIF
					
					// if coords are way under the map (due to player respawning?) then try and grab the ground z
					IF (vSpawnCoords.z < -80.0)			
						NET_PRINT("[personal_vehicle]  z was way below ground. vSpawnCoords=") NET_PRINT_VECTOR(vSpawnCoords) NET_NL()	
						IF GET_GROUND_Z_FOR_3D_COORD(<<vSpawnCoords.x, vSpawnCoords.y, 1000.0>>, vSpawnCoords.z)	
							NET_PRINT("[personal_vehicle] Got new ground z. vSpawnCoords=") NET_PRINT_VECTOR(vSpawnCoords) NET_NL()		
						ENDIF
					ENDIF
					INT iproperty = -1
					IF IS_BIT_SET(g_TransitionSessionNonResetVars.MissionPVBitset, ciSPAWN_PV_IN_ORIGINAL_LOC)
						IF NOT IS_VECTOR_ZERO(g_TransitionSessionNonResetVars.PVStartLocation)
							vSpawnCoords = g_TransitionSessionNonResetVars.PVStartLocation
							fSpawnHeading = g_TransitionSessionNonResetVars.PVStartHeading
							NET_PRINT("[personal_vehicle]  clearing warp near flag - 9") NET_NL()
							CLEAR_REQUEST_TO_WARP_PERSONAL_VEHICLE_NEAR()
							bUseOldLoc = TRUE
						ENDIF
					ENDIF
					
					
					
					IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty > 0
						
						
						IF IS_PROPERTY_OFFICE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
							GET_OFFICE_VEH_DELIVERY_POINT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty,vSpawnCoords,fSpawnHeading)
						ELSE
							vSpawnCoords = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].vBlipLocation[0]
						ENDIF
						
						//vSpawnCoords = GET_MP_PROPERTY_BUILDING_WORLD_POINT(GET_PROPERTY_BUILDING(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty))
						PRINTLN("[personal_vehicle]  Inside property using broadcast data: ",GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty  ," so using spawn coords: ",vSpawnCoords)
						bUseOldLoc = FALSE
					ELSE
						GET_APARTMENT_INTERIOR_I_AM_IN_BASED_ON_COORDS(vSpawnCoords,iproperty)
						IF iproperty > 0
							//vSpawnCoords = GET_MP_PROPERTY_BUILDING_WORLD_POINT(GET_PROPERTY_BUILDING(iproperty))
							vSpawnCoords = mpProperties[iproperty].vBlipLocation[0]
							PRINTLN("[personal_vehicle]  Inside property using backup position vector: ",iproperty  ," so using spawn coords: ",vSpawnCoords)
							bUseOldLoc = FALSE
						ENDIF
					ENDIF
					
					// if i'm in my appartment and on a mission then let my pv spawn just outside
					BOOL bStartOfMission=FALSE
					IF IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
						IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty > 0
						OR iproperty > 0
							bStartOfMission = TRUE
							PRINTLN("[personal_vehicle]  on a mission and inside a property, settign bStartOfMission = TRUE")
						ENDIF
					ENDIF
					

					IF CREATE_MP_SAVED_VEHICLE(vSpawnCoords, fSpawnHeading, TRUE,CURRENT_SAVED_VEHICLE_SLOT(),bUseOldLoc,FALSE,bStartOfMission)
						
						IF IS_BIT_SET(g_TransitionSessionNonResetVars.MissionPVBitset, ciSPAWN_PV_IN_ORIGINAL_LOC)
						AND SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN(FALSE)
							tempveh = PERSONAL_VEHICLE_ID()
							IF DOES_ENTITY_EXIST(tempveh)
							AND IS_VEHICLE_DRIVEABLE(tempveh)
								PRINTLN("[personal_vehicle]  Warping player into newly spawned vehicle for store transition")
								FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
								TASK_ENTER_VEHICLE(PLAYER_PED_ID(), tempveh, 1, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
							ENDIF
						ENDIF
						
						CLEAR_BIT(g_TransitionSessionNonResetVars.MissionPVBitset, ciSPAWN_PV_IN_ORIGINAL_LOC)
						g_TransitionSessionNonResetVars.PVStartLocation = <<0.0,0.0,0.0>>
						g_TransitionSessionNonResetVars.PVStartHeading = 0.0
						IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
							CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AFTER_SESSION)
						ENDIF

					ENDIF

				ENDIF							

			ENDIF
		ENDIF
	ELIF NOT HAS_NET_TIMER_STARTED(MPGlobals.VehicleData.iCleanupTimer)
		START_NET_TIMER(MPGlobals.VehicleData.iCleanupTimer)
	ELIF HAS_NET_TIMER_EXPIRED(MPGlobals.VehicleData.iCleanupTimer, 500)
	
		// Not safe to process so remove blip.
		REMOVE_PV_BLIP(8)
		
		
		IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_CREATED)
			CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_BEING_CREATED)
			IF IS_CURRENT_PERSONAL_VEHICLE_DISABLED_FOR_MISSION()
			OR (ARE_ALL_PERSONAL_VEHICLES_DISABLED_FOR_MISSION_STARTUP() AND NOT IS_OK_TO_SPAWN_VEHICLE_AT_START_OF_HEIST_MISSION())
			
				// make sure mission has been init first before cleaning up pv. 2197725
				IF (g_bMissionInitComplete)
					NET_PRINT("[personal_vehicle]  Vehicle is not safe to be here - disabled for mission, delete.") NET_NL()	
					IF IS_CURRENT_PERSONAL_VEHICLE_DISABLED_FOR_MISSION()
						CLEANUP_MP_SAVED_VEHICLE(TRUE,FALSE,TRUE,TRUE)
					ELSE
						CLEANUP_MP_SAVED_VEHICLE(TRUE,FALSE,FALSE)
					ENDIF
				ELSE
					PRINTLN("[personal_vehicle] waiting for g_bMissionInitComplete")
				ENDIF
			ELSE
				IF NOT IS_PLAYER_IN_CORONA()	
				AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()

					// if the player has just become a spectator on a mission, don't delete their pv. 1850772
					IF  NOT (IS_A_SPECTATOR_CAM_ACTIVE() AND NOT (DID_I_JOIN_MISSION_AS_SPECTATOR() OR IS_PLAYER_SCTV(PLAYER_ID()))) 
						NET_PRINT("[personal_vehicle]  Vehicle is not safe to be here - get rid of it!") NET_NL()
						
						#IF IS_DEBUG_BUILD
						IF NOT IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP_DELETE)
							DebugOutputFailedSafeToProcess()
						ENDIF
						#ENDIF
						
						// if we've just moved into the celebration screen, don't delete
						IF g_bCelebrationScreenIsActive
							PRINTLN("[personal_vehicle] celebration screen is active so just marking as no longer needed")
							CLEANUP_MP_SAVED_VEHICLE(FALSE, TRUE)
						ELSE
							CLEANUP_MP_SAVED_VEHICLE(TRUE)
						ENDIF
						
					ELSE
						IF SHOULD_PERSONAL_VEHICLE_CLEANUP_ON_ISLAND() 
							NET_PRINT("[personal_vehicle]  Vehicle is not safe to be on island - cleanup, delete.") NET_NL()
							
							#IF IS_DEBUG_BUILD
							IF NOT IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP_DELETE)
								DebugOutputFailedSafeToProcess()
							ENDIF
							#ENDIF
							
							CLEANUP_MP_SAVED_VEHICLE(TRUE)
						ELSE
							NET_PRINT("[personal_vehicle]  Vehicle is not safe to be here - cleanup, but not delete.") NET_NL()
							
							#IF IS_DEBUG_BUILD
							IF NOT IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP)
								DebugOutputFailedSafeToProcess()
							ENDIF
							#ENDIF
							
							CLEANUP_MP_SAVED_VEHICLE(FALSE)
						ENDIF
					ENDIF
				ELIF g_bCleanupLeftoverAirPV
				AND CURRENT_SAVED_VEHICLE_SLOT() >= 0
				AND IS_VEHICLE_MODEL_ABLE_TO_FLY(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel)
				AND IS_PLAYER_IN_CORONA()
				AND DOES_ENTITY_EXIST(PERSONAL_VEHICLE_ID())
				AND IS_ENTITY_IN_AIR(PERSONAL_VEHICLE_ID())
					IF NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
						NET_PRINT("[personal_vehicle]  Vehicle was left in the air, cleanup.") NET_NL()
						
						// if its a duluxo then dont send back to garage - url:bugstar:4919461 - Dispatch VI - Player's active Deluxo PV did not spawn on mission launch.
						IF (g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel = DELUXO)
							CLEANUP_MP_SAVED_VEHICLE(FALSE, FALSE, FALSE, FALSE, TRUE)
						ELSE
							CLEANUP_MP_SAVED_VEHICLE(FALSE, FALSE, TRUE, FALSE, TRUE)
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ELSE
			
			// was it in the process of being created? if so clear the search for the vehicle spawn point. fix for 2470859
			IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_BEING_CREATED)
				IF NOT (g_SpawnData.iSpawnVehicleState = SPAWN_VEHICLE_INIT)
					g_SpawnData.iSpawnVehicleState = SPAWN_VEHICLE_INIT
					PRINTLN("[personal_vehicle] vehicle was mid creation when became no longer safe, clearing up.")
				ENDIF
				CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_BEING_CREATED)
			ENDIF
			
		ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("is safe to process")
	#ENDIF
	#ENDIF	

	
	// Warp PV away when player or vehicle is in blocked area
	VECTOR vCoords
	FLOAT fHeading
	BOOL bWarpAway = FALSE
	IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
		tempveh= PERSONAL_VEHICLE_ID()
		
		IF IS_VEHICLE_EMPTY(tempVeh, TRUE)
			// Player in mission coronas
			IF IS_PLAYER_IN_CORONA()
				IF GET_DISTANCE_BETWEEN_COORDS(g_sCoronaFocusVisuals.coronaPosition, GET_ENTITY_COORDS(tempveh), FALSE) < 6
					vCoords = g_sCoronaFocusVisuals.coronaPosition
					bWarpAway = TRUE
					NET_PRINT("[personal_vehicle]  Vehicle is in corona, get it away!") NET_NL()
				ENDIF
					
			// PV in Simeons garage
			ELIF IS_ENTITY_IN_ANGLED_AREA(tempveh, <<1210.437866,-3116.641602,4.280060>>, <<1197.287109,-3116.663086,9.523314>>, 11.500000)
				vCoords = <<1203.34, -3117.10, 4.55>>
				bWarpAway = TRUE
				
				NET_PRINT("[personal_vehicle]  Vehicle is in Simeons garage, get it away!") NET_NL()
				
			ENDIF
			
			// Do the warp.
			IF bWarpAway
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempveh)
					
					VEHICLE_SPAWN_LOCATION_PARAMS Params
					Params.fMinDistFromCoords = 10.0
					Params.bIsForPV = TRUE
				
					IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vCoords,<<0.0, 0.0, 0.0>>,GET_ENTITY_MODEL(tempveh),TRUE,vCoords,fHeading,Params)
						SET_ENTITY_COORDS(tempveh,vCoords)
						SET_ENTITY_HEADING(tempveh,fHeading)
						NET_PRINT("[personal_vehicle]  Successfully set vehicle position away from blocked area: ") NET_PRINT_VECTOR(vCoords) NET_NL()
						bWarpAway = FALSE
					ENDIF
				ELSE
					NET_PRINT("[personal_vehicle]  Requesting control of saved vehicle to warp out of blocked area") NET_NL()
					NETWORK_REQUEST_CONTROL_OF_ENTITY(tempveh)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		#IF FEATURE_CASINO
		CLEAR_WARP_INTO_CASINO_VALET_CAR_PARK()
		#ENDIF
		
		#IF FEATURE_TUNER
		IF NOT MPGlobalsAmbience.bLaunchVehicleDropPersonal
			CLEAR_WARP_INTO_CAR_MEET_CAR_PARK()
		
			CLEAR_WARP_INTO_PRIVATE_CAR_MEET_CAR_PARK()
		ENDIF
		#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("warp away")
	#ENDIF
	#ENDIF	
	
	MAINTAIN_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION(bWarpAway)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MISSION_OK_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION")
	#ENDIF
	#ENDIF		
	
	MAINTAIN_PV_IMPOUND()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PV_IMPOUND")
	#ENDIF
	#ENDIF		
	
	MAINTAIN_CLEANUP_MP_SAVED_VEHICLE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLEANUP_MP_SAVED_VEHICLE")
	#ENDIF
	#ENDIF		
	
	STORE_CURRENT_MP_SAVED_VEHICLE_INDEX_AS_GLOBAL()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("STORE_CURRENT_MP_SAVED_VEHICLE_INDEX_AS_GLOBAL")
	#ENDIF
	#ENDIF	
	
	MAINTAIN_CHECK_THAT_PV_I_AM_IN_IS_ACTUALLY_MY_CURRENT_PV()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CHECK_THAT_PV_I_AM_IN_IS_ACTUALLY_MY_CURRENT_PV")
	#ENDIF
	#ENDIF		
	
	UPDATE_CHECK_IMPOUND_VEHICLE_IS_STILL_IN_IMPOUND()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_CHECK_IMPOUND_VEHICLE_IS_STILL_IN_IMPOUND")
	#ENDIF
	#ENDIF	

	UPDATE_WANTED_LEVEL_IN_IMPOUND_YARD_CHECK()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_WANTED_LEVEL_IN_IMPOUND_YARD_CHECK")
	#ENDIF
	#ENDIF		

	UPDATE_HIDE_PERSONAL_VEHICLES()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_HIDE_PERSONAL_VEHICLES")
	#ENDIF
	#ENDIF		
	
	HANDLE_CONCEALING_PERSONAL_VEHICLES()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("HANDLE_CONCEALING_PERSONAL_VEHICLES")
	#ENDIF
	#ENDIF		
	
	MAINTAIN_REMOVE_PV_FROM_INVENTORY()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_REMOVE_PV_FROM_INVENTORY")
	#ENDIF
	#ENDIF	
	
	MAINTAIN_PETROLTANK_FIRE_CULPRIT_REMOVAL_CHECK()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PETROLTANK_FIRE_CULPRIT_REMOVAL_CHECK")
	#ENDIF
	#ENDIF		
	
	MPGlobals.VehicleData.bSuspendPVCreationThisFrame = FALSE // reset this back to false each frame.
	
	MAINTAIN_MOVE_VEHICLE_FROM_TRUCK_TO_BUNKER()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MOVE_VEHICLE_FROM_TRUCK_TO_BUNKER")
	#ENDIF
	#ENDIF		
	
ENDPROC

PROC MAINTAIN_MP_SAVED_TRUCK()

	VEHICLE_INDEX TruckVeh

		IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE,FALSE)
			//player is spawning a vehicle from insurance company
			IF morsMutualData.bSpawningVeh
				IF NOT IS_NET_VEHICLE_DRIVEABLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
					PRINTLN("[personal_truck] Insurance triggered recreating personal vehicle in slot #",morsMutualData.iSlot)
					morsMutualData.bSpawningVeh = FALSE
				ELSE
					PRINTLN("[personal_truck] MAINTAIN_MP_SAVED_VEHICLE: morsMutualData.bSpawningVeh = TRUE, vehicle is driveable.")
					morsMutualData.bSpawningVeh = FALSE
				ENDIF
			ELIF MPGlobals.VehicleData.bAwaitingAssignToMainScript
				//this is setup for when the player exits property they have created a vehicle and set flags but it doesn't exist to get grabbed yet.
				PRINTLN("[personal_truck] MAINTAIN_MP_SAVED_VEHICLE: MPGlobals.VehicleData.bAwaitingAssignToMainScript = TRUE")
			ELIF MPGlobals.VehicleData.bAssignToMainScript
				
				PRINTLN("[personal_truck] MAINTAIN_MP_SAVED_VEHICLE: MPGlobals.VehicleData.bAssignToMainScript = TRUE")

			ELIF IS_ENTITY_A_VEHICLE(personal_truck_ID())
			AND IS_VEHICLE_DRIVEABLE(personal_truck_ID())
				CLEAR_SAVED_TRUCK_FLAG(MP_SAVED_VEH_FLAG_CREATION_INIT)

				TruckVeh= personal_truck_ID()
				VEHICLE_INDEX trailerVeh= PERSONAL_TRAILER_ID()
				
				IF IS_ENTITY_A_VEHICLE(TruckVeh)
				AND IS_VEHICLE_DRIVEABLE(TruckVeh)
					// Clear up stored vehicle if put in for repair or flagged for cleanup
					IF IS_SAVED_TRUCK_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP)
					ELIF IS_SAVED_TRUCK_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP_DELETE)
					ELSE
//						// Make sure vehicle doesn't fall through the ground when the switch cam is active.
						IF IS_SAVED_TRUCK_FLAG_SET( MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(TruckVeh)
							AND HAS_COLLISION_LOADED_AROUND_ENTITY(TruckVeh)
								FREEZE_ENTITY_POSITION(TruckVeh, FALSE)
								SET_VEHICLE_ON_GROUND_PROPERLY(TruckVeh)
								CLEAR_SAVED_TRUCK_FLAG( MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
								NET_PRINT("[personal_truck]  collision - unfreezeing vehicle") NET_NL()
							ENDIF
						ELSE
							IF IS_NET_PLAYER_OK(PLAYER_ID())
							AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), TruckVeh)
							AND IS_PLAYER_SWITCH_IN_PROGRESS()
							AND NETWORK_HAS_CONTROL_OF_ENTITY(TruckVeh)
							AND NOT HAS_COLLISION_LOADED_AROUND_ENTITY(TruckVeh)
								FREEZE_ENTITY_POSITION(TruckVeh, TRUE)
								SET_SAVED_TRUCK_FLAG( MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
								NET_PRINT("[personal_truck]  collision - freezeing vehicle") NET_NL()
							ENDIF
						ENDIF
						
//						//Update Lock State
						IF MPGlobalsAmbience.iStoredAllowIntoTruck != GET_MP_INT_CHARACTER_STAT(MP_STAT_TRUCK_ACCESS)
						AND NOT IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID())
							IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
							AND NOT IS_PLAYER_SCTV(PLAYER_ID())
							AND NOT IS_A_SPECTATOR_CAM_RUNNING()
								IF NETWORK_GET_ENTITY_IS_NETWORKED(TruckVeh)
									SET_TRUCK_LOCK_STATE(TruckVeh)
									IF DOES_ENTITY_EXIST(trailerVeh)
										SET_TRUCK_LOCK_STATE(trailerVeh)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
//							
						PROCESS_MP_SAVED_TRUCK_FLAGS(TruckVeh, trailerVeh)

					ENDIF
				ENDIF
			ENDIF
		ENDIF
	MAINTAIN_CLEANUP_MP_SAVED_TRUCK()
	
ENDPROC

PROC MAINTAIN_MP_SAVED_AVENGER()

	VEHICLE_INDEX avengerVeh
	IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE,FALSE)
		//player is spawning a vehicle from insurance company
		IF morsMutualData.bSpawningVeh
			IF NOT IS_NET_VEHICLE_DRIVEABLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
				PRINTLN("[personal_plane] Insurance triggered recreating personal vehicle in slot #",morsMutualData.iSlot)
				morsMutualData.bSpawningVeh = FALSE
			ELSE
				PRINTLN("[personal_plane] MAINTAIN_MP_SAVED_VEHICLE: morsMutualData.bSpawningVeh = TRUE, vehicle is driveable.")
				morsMutualData.bSpawningVeh = FALSE
			ENDIF
		ELIF MPGlobals.VehicleData.bAwaitingAssignToMainScript
			//this is setup for when the player exits property they have created a vehicle and set flags but it doesn't exist to get grabbed yet.
			PRINTLN("[personal_plane] MAINTAIN_MP_SAVED_VEHICLE: MPGlobals.VehicleData.bAwaitingAssignToMainScript = TRUE")
		ELIF MPGlobals.VehicleData.bAssignToMainScript
			
			PRINTLN("[personal_plane] MAINTAIN_MP_SAVED_VEHICLE: MPGlobals.VehicleData.bAssignToMainScript = TRUE")

			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				avengerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(avengerVeh)
				AND IS_VEHICLE_DRIVEABLE(avengerVeh)
				AND NETWORK_GET_ENTITY_IS_NETWORKED(avengerVeh)

				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN("[personal_plane] bAssignToMainScript cleared because...")
						IF NOT DOES_ENTITY_EXIST(avengerVeh)
							PRINTLN("[personal_plane] avengerVeh doesn't exist!")
						ENDIF
						IF NOT IS_VEHICLE_DRIVEABLE(avengerVeh)
							PRINTLN("[personal_plane] avengerVeh isn't driveable!")
						ENDIF
						IF NOT NETWORK_GET_ENTITY_IS_NETWORKED(avengerVeh)
							PRINTLN("[personal_plane] avengerVeh isn't networked!")
						ENDIF
					#ENDIF
				ENDIF
			ENDIF

		ELIF IS_ENTITY_A_VEHICLE(PERSONAL_AIRCRAFT_ID()) 
		AND IS_VEHICLE_DRIVEABLE(PERSONAL_AIRCRAFT_ID())
			CLEAR_SAVED_AVENGER_FLAG( MP_SAVED_VEH_FLAG_CREATION_INIT)

			avengerVeh= PERSONAL_AIRCRAFT_ID()
			
			IF IS_VEHICLE_DRIVEABLE(avengerVeh)
				// Clear up stored vehicle if put in for repair or flagged for cleanup
				IF IS_SAVED_AVENGER_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP)
				ELIF IS_SAVED_AVENGER_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP_DELETE)
				ELSE
//					// Make sure vehicle doesn't fall through the ground when the switch cam is active.
					IF IS_SAVED_AVENGER_FLAG_SET( MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(avengerVeh)
						AND HAS_COLLISION_LOADED_AROUND_ENTITY(avengerVeh)
							FREEZE_ENTITY_POSITION(avengerVeh, FALSE)
							SET_VEHICLE_ON_GROUND_PROPERLY(avengerVeh)
							CLEAR_SAVED_AVENGER_FLAG( MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
							NET_PRINT("[personal_plane]  collision - unfreezeing vehicle") NET_NL()
						ENDIF
					ELSE
						IF IS_NET_PLAYER_OK(PLAYER_ID())
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), avengerVeh)
						AND IS_PLAYER_SWITCH_IN_PROGRESS()
						AND NETWORK_HAS_CONTROL_OF_ENTITY(avengerVeh)
						AND NOT HAS_COLLISION_LOADED_AROUND_ENTITY(avengerVeh)
							FREEZE_ENTITY_POSITION(avengerVeh, TRUE)
							SET_SAVED_AVENGER_FLAG( MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
							NET_PRINT("[personal_plane]  collision - freezeing vehicle") NET_NL()
						ENDIF
					ENDIF

//					//Update Lock State
					IF MPGlobalsAmbience.iStoredAllowIntoAvenger != GET_MP_INT_CHARACTER_STAT(MP_STAT_AVENGER_ACCESS)
					AND NOT IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(PLAYER_ID())
						IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
						AND NOT IS_PLAYER_SCTV(PLAYER_ID())
						AND NOT IS_A_SPECTATOR_CAM_RUNNING()
							IF NETWORK_GET_ENTITY_IS_NETWORKED(avengerVeh)
								SET_AVENGER_LOCK_STATE(avengerVeh)
							ENDIF
						ENDIF
					ENDIF
//					
					PROCESS_MP_SAVED_AVENGER_FLAGS(avengerVeh)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	MAINTAIN_CLEANUP_MP_SAVED_AVENGER()
ENDPROC

PROC MAINTAIN_MP_SAVED_HACKER_TRUCK()

	VEHICLE_INDEX HackertruckVeh
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE,FALSE)
		//player is spawning a vehicle from insurance company
		IF morsMutualData.bSpawningVeh
			IF NOT IS_NET_VEHICLE_DRIVEABLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
				PRINTLN("[personal_hackertruck] Insurance triggered recreating personal vehicle in slot #",morsMutualData.iSlot)
				morsMutualData.bSpawningVeh = FALSE
			ELSE
				PRINTLN("[personal_hackertruck] MAINTAIN_MP_SAVED_VEHICLE: morsMutualData.bSpawningVeh = TRUE, vehicle is driveable.")
				morsMutualData.bSpawningVeh = FALSE
			ENDIF
		ELIF MPGlobals.VehicleData.bAwaitingAssignToMainScript
			//this is setup for when the player exits property they have created a vehicle and set flags but it doesn't exist to get grabbed yet.
			PRINTLN("[personal_hackertruck] MAINTAIN_MP_SAVED_VEHICLE: MPGlobals.VehicleData.bAwaitingAssignToMainScript = TRUE")
		ELIF MPGlobals.VehicleData.bAssignToMainScript
			
			PRINTLN("[personal_hackertruck] MAINTAIN_MP_SAVED_VEHICLE: MPGlobals.VehicleData.bAssignToMainScript = TRUE")

			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				HackertruckVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(HackertruckVeh)
				AND IS_VEHICLE_DRIVEABLE(HackertruckVeh)
				AND NETWORK_GET_ENTITY_IS_NETWORKED(HackertruckVeh)

				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN("[personal_hackertruck] bAssignToMainScript cleared because...")
						IF NOT DOES_ENTITY_EXIST(HackertruckVeh)
							PRINTLN("[personal_hackertruck] HackertruckVeh doesn't exist!")
						ENDIF
						IF NOT IS_VEHICLE_DRIVEABLE(HackertruckVeh)
							PRINTLN("[personal_hackertruck] HackertruckVeh isn't driveable!")
						ENDIF
						IF NOT NETWORK_GET_ENTITY_IS_NETWORKED(HackertruckVeh)
							PRINTLN("[personal_hackertruck] HackertruckVeh isn't networked!")
						ENDIF
					#ENDIF
				ENDIF
			ENDIF

		ELIF IS_VEHICLE_DRIVEABLE(PERSONAL_HACKERTRUCK_ID())

			CLEAR_SAVED_HACKER_TRUCK_FLAG( MP_SAVED_VEH_FLAG_CREATION_INIT)

			HackertruckVeh= PERSONAL_HACKERTRUCK_ID()
			VEHICLE_INDEX trailerVeh= PERSONAL_TRAILER_ID()
			
			IF IS_VEHICLE_DRIVEABLE(HackertruckVeh)
				// Clear up stored vehicle if put in for repair or flagged for cleanup
				IF IS_SAVED_HACKER_TRUCK_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP)

				ELIF IS_SAVED_HACKER_TRUCK_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP_DELETE)
					
				ELSE						
					// Make sure vehicle doesn't fall through the ground when the switch cam is active.
					IF IS_SAVED_HACKER_TRUCK_FLAG_SET( MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(HackertruckVeh)
						AND HAS_COLLISION_LOADED_AROUND_ENTITY(HackertruckVeh)
							FREEZE_ENTITY_POSITION(HackertruckVeh, FALSE)
							SET_VEHICLE_ON_GROUND_PROPERLY(HackertruckVeh)
							CLEAR_SAVED_HACKER_TRUCK_FLAG( MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
							NET_PRINT("[personal_hackertruck]  collision - unfreezeing vehicle") NET_NL()
						ENDIF
					ELSE
						IF IS_NET_PLAYER_OK(PLAYER_ID())
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HackertruckVeh)
						AND IS_PLAYER_SWITCH_IN_PROGRESS()
						AND NETWORK_HAS_CONTROL_OF_ENTITY(HackertruckVeh)
						AND NOT HAS_COLLISION_LOADED_AROUND_ENTITY(HackertruckVeh)
							FREEZE_ENTITY_POSITION(HackertruckVeh, TRUE)
							SET_SAVED_HACKER_TRUCK_FLAG( MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
							NET_PRINT("[personal_hackertruck]  collision - freezeing vehicle") NET_NL()
						ENDIF
					ENDIF
					
					//Update Lock State
					IF MPGlobalsAmbience.iStoredAllowIntoHackerTruck != GET_MP_INT_CHARACTER_STAT(MP_STAT_HACKERTRUCK_ACCESS)
					AND NOT IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(PLAYER_ID())
						IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
						AND NOT IS_PLAYER_SCTV(PLAYER_ID())
						AND NOT IS_A_SPECTATOR_CAM_RUNNING()
							IF NETWORK_GET_ENTITY_IS_NETWORKED(HackertruckVeh)
								SET_HACKER_TRUCK_LOCK_STATE(HackertruckVeh)
								IF DOES_ENTITY_EXIST(trailerVeh)
									SET_HACKER_TRUCK_LOCK_STATE(trailerVeh)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					PROCESS_MP_SAVED_HACKER_TRUCK_FLAGS(HackertruckVeh, trailerVeh)
				ENDIF
			ELSE
			
				#IF IS_DEBUG_BUILD
					IF NOT IS_VEHICLE_DRIVEABLE(HackertruckVeh)
						NET_PRINT("[personal_hackertruck] IS_VEHICLE_DRIVEABLE(HackertruckVeh) = FALSE") NET_NL()
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	MAINTAIN_CLEANUP_MP_SAVED_HACKERTRUCK()
ENDPROC

#IF FEATURE_DLC_2_2022
PROC MAINTAIN_MP_SAVED_ACID_LAB()

	VEHICLE_INDEX AcidLabVeh
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE,FALSE)
		//player is spawning a vehicle from insurance company
		IF morsMutualData.bSpawningVeh
			IF NOT IS_NET_VEHICLE_DRIVEABLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
				PRINTLN("[personal_acidlab] Insurance triggered recreating personal vehicle in slot #",morsMutualData.iSlot)
				morsMutualData.bSpawningVeh = FALSE
			ELSE
				PRINTLN("[personal_acidlab] MAINTAIN_MP_SAVED_VEHICLE: morsMutualData.bSpawningVeh = TRUE, vehicle is driveable.")
				morsMutualData.bSpawningVeh = FALSE
			ENDIF
		ELIF MPGlobals.VehicleData.bAwaitingAssignToMainScript
			//this is setup for when the player exits property they have created a vehicle and set flags but it doesn't exist to get grabbed yet.
			PRINTLN("[personal_acidlab] MAINTAIN_MP_SAVED_VEHICLE: MPGlobals.VehicleData.bAwaitingAssignToMainScript = TRUE")
		ELIF MPGlobals.VehicleData.bAssignToMainScript
			
			PRINTLN("[personal_acidlab] MAINTAIN_MP_SAVED_VEHICLE: MPGlobals.VehicleData.bAssignToMainScript = TRUE")

			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AcidLabVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(AcidLabVeh)
				AND IS_VEHICLE_DRIVEABLE(AcidLabVeh)
				AND NETWORK_GET_ENTITY_IS_NETWORKED(AcidLabVeh)

				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN("[personal_acidlab] bAssignToMainScript cleared because...")
						IF NOT DOES_ENTITY_EXIST(AcidLabVeh)
							PRINTLN("[personal_acidlab] AcidLabVeh doesn't exist!")
						ENDIF
						IF NOT IS_VEHICLE_DRIVEABLE(AcidLabVeh)
							PRINTLN("[personal_acidlab] AcidLabVeh isn't driveable!")
						ENDIF
						IF NOT NETWORK_GET_ENTITY_IS_NETWORKED(AcidLabVeh)
							PRINTLN("[personal_acidlab] AcidLabVeh isn't networked!")
						ENDIF
					#ENDIF
				ENDIF
			ENDIF

		ELIF IS_VEHICLE_DRIVEABLE(PERSONAL_ACIDLAB_ID())

			CLEAR_SAVED_ACID_LAB_FLAG( MP_SAVED_VEH_FLAG_CREATION_INIT)

			AcidLabVeh= PERSONAL_ACIDLAB_ID()
			// VEHICLE_INDEX trailerVeh= PERSONAL_TRAILER_ID()
			
			IF IS_VEHICLE_DRIVEABLE(AcidLabVeh)
				// Clear up stored vehicle if put in for repair or flagged for cleanup
				IF IS_SAVED_ACID_LAB_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP)

				ELIF IS_SAVED_ACID_LAB_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP_DELETE)
					
				ELSE						
					// Make sure vehicle doesn't fall through the ground when the switch cam is active.
					IF IS_SAVED_ACID_LAB_FLAG_SET( MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(AcidLabVeh)
						AND HAS_COLLISION_LOADED_AROUND_ENTITY(AcidLabVeh)
							FREEZE_ENTITY_POSITION(AcidLabVeh, FALSE)
							SET_VEHICLE_ON_GROUND_PROPERLY(AcidLabVeh)
							CLEAR_SAVED_ACID_LAB_FLAG( MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
							NET_PRINT("[personal_acidlab]  collision - unfreezeing vehicle") NET_NL()
						ENDIF
					ELSE
						IF IS_NET_PLAYER_OK(PLAYER_ID())
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), AcidLabVeh)
						AND IS_PLAYER_SWITCH_IN_PROGRESS()
						AND NETWORK_HAS_CONTROL_OF_ENTITY(AcidLabVeh)
						AND NOT HAS_COLLISION_LOADED_AROUND_ENTITY(AcidLabVeh)
							FREEZE_ENTITY_POSITION(AcidLabVeh, TRUE)
							SET_SAVED_ACID_LAB_FLAG( MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
							NET_PRINT("[personal_acidlab]  collision - freezeing vehicle") NET_NL()
						ENDIF
					ENDIF
					
					//Update Lock State
					IF MPGlobalsAmbience.iStoredAllowIntoAcidLab != GET_PACKED_STAT_INT(PACKED_MP_INT_ACIDLAB_ACCESS)
					AND NOT IS_OWNERS_ACID_LAB_INSIDE_JUGGALO_HIDEOUT(PLAYER_ID())
						IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
						AND NOT IS_PLAYER_SCTV(PLAYER_ID())
						AND NOT IS_A_SPECTATOR_CAM_RUNNING()
							IF NETWORK_GET_ENTITY_IS_NETWORKED(AcidLabVeh)
								SET_ACID_LAB_LOCK_STATE(AcidLabVeh)
//								IF DOES_ENTITY_EXIST(trailerVeh)
//									SET_HACKER_TRUCK_LOCK_STATE(trailerVeh)
//								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					PROCESS_MP_SAVED_ACID_LAB_FLAGS(AcidLabVeh)
				ENDIF
			ELSE
			
				#IF IS_DEBUG_BUILD
					IF NOT IS_VEHICLE_DRIVEABLE(AcidLabVeh)
						NET_PRINT("[personal_acidlab] IS_VEHICLE_DRIVEABLE(AcidLabVeh) = FALSE") NET_NL()
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	MAINTAIN_CLEANUP_MP_SAVED_ACIDLAB()
ENDPROC
#ENDIF

#IF FEATURE_HEIST_ISLAND
PROC MAINTAIN_MP_SAVED_SUBMARINE()

	VEHICLE_INDEX SubmarineVeh
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE,FALSE)
		//player is spawning a vehicle from insurance company
		IF morsMutualData.bSpawningVeh
			IF NOT IS_NET_VEHICLE_DRIVEABLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV)
				PRINTLN("[personal_submarine] Insurance triggered recreating personal vehicle in slot #",morsMutualData.iSlot)
				morsMutualData.bSpawningVeh = FALSE
			ELSE
				PRINTLN("[personal_submarine] MAINTAIN_MP_SAVED_VEHICLE: morsMutualData.bSpawningVeh = TRUE, vehicle is driveable.")
				morsMutualData.bSpawningVeh = FALSE
			ENDIF
		ELIF MPGlobals.VehicleData.bAwaitingAssignToMainScript
			//this is setup for when the player exits property they have created a vehicle and set flags but it doesn't exist to get grabbed yet.
			PRINTLN("[personal_submarine] MAINTAIN_MP_SAVED_VEHICLE: MPGlobals.VehicleData.bAwaitingAssignToMainScript = TRUE")
		ELIF MPGlobals.VehicleData.bAssignToMainScript
			
			PRINTLN("[personal_submarine] MAINTAIN_MP_SAVED_VEHICLE: MPGlobals.VehicleData.bAssignToMainScript = TRUE")

			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				SubmarineVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(SubmarineVeh)
				AND IS_VEHICLE_DRIVEABLE(SubmarineVeh)
				AND NETWORK_GET_ENTITY_IS_NETWORKED(SubmarineVeh)

				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN("[personal_submarine] bAssignToMainScript cleared because...")
						IF NOT DOES_ENTITY_EXIST(SubmarineVeh)
							PRINTLN("[personal_submarine] SubmarineVeh doesn't exist!")
						ENDIF
						IF NOT IS_VEHICLE_DRIVEABLE(SubmarineVeh)
							PRINTLN("[personal_submarine] SubmarineVeh isn't driveable!")
						ENDIF
						IF NOT NETWORK_GET_ENTITY_IS_NETWORKED(SubmarineVeh)
							PRINTLN("[personal_submarine] SubmarineVeh isn't networked!")
						ENDIF
					#ENDIF
				ENDIF
			ENDIF

		ELIF IS_VEHICLE_DRIVEABLE(PERSONAL_SUBMARINE_ID())

			CLEAR_SAVED_SUBMARINE_FLAG(MP_SAVED_VEH_FLAG_CREATION_INIT)

			SubmarineVeh = PERSONAL_SUBMARINE_ID()

			IF IS_VEHICLE_DRIVEABLE(SubmarineVeh)
				// Clear up stored vehicle if put in for repair or flagged for cleanup
				IF IS_SAVED_SUBMARINE_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP)

				ELIF IS_SAVED_SUBMARINE_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP_DELETE)
					
				ELSE						
					// Make sure vehicle doesn't fall through the ground when the switch cam is active.
					IF IS_SAVED_SUBMARINE_FLAG_SET(MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(SubmarineVeh)
						AND HAS_COLLISION_LOADED_AROUND_ENTITY(SubmarineVeh)
							FREEZE_ENTITY_POSITION(SubmarineVeh, FALSE)
							SET_VEHICLE_ON_GROUND_PROPERLY(SubmarineVeh)
							CLEAR_SAVED_SUBMARINE_FLAG(MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
							NET_PRINT("[personal_submarine]  collision - unfreezeing vehicle") NET_NL()
						ENDIF
					ELSE
						IF IS_NET_PLAYER_OK(PLAYER_ID())
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SubmarineVeh)
						AND IS_PLAYER_SWITCH_IN_PROGRESS()
						AND NETWORK_HAS_CONTROL_OF_ENTITY(SubmarineVeh)
						AND NOT HAS_COLLISION_LOADED_AROUND_ENTITY(SubmarineVeh)
							FREEZE_ENTITY_POSITION(SubmarineVeh, TRUE)
							SET_SAVED_SUBMARINE_FLAG(MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
							NET_PRINT("[personal_submarine]  collision - freezeing vehicle") NET_NL()
						ENDIF
					ENDIF
					
					//Update Lock State
//					IF MPGlobalsAmbience.iStoredAllowIntoSubmarine != GET_MP_INT_CHARACTER_STAT(MP_STAT_SUBMARINE_ACCESS)
//						IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
//						AND NOT IS_PLAYER_SCTV(PLAYER_ID())
//						AND NOT IS_A_SPECTATOR_CAM_RUNNING()
//							IF NETWORK_GET_ENTITY_IS_NETWORKED(SubmarineVeh)
//								SET_SUBMARINE_LOCK_STATE(SubmarineVeh)
//							ENDIF
//						ENDIF
//					ENDIF
					
					PROCESS_MP_SAVED_SUBMARINE_FLAGS(SubmarineVeh)
				ENDIF
			ELSE
			
				#IF IS_DEBUG_BUILD
					IF NOT IS_VEHICLE_DRIVEABLE(SubmarineVeh)
						NET_PRINT("[personal_submarine] IS_VEHICLE_DRIVEABLE(SubmarineVeh) = FALSE") NET_NL()
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Don't allow non-boss members of a gang to have a submarine
	IF IS_PLAYER_SUBMARINE_IN_FREEMODE(PLAYER_ID())
	AND GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE) 
	AND NOT IS_MP_SAVED_SUBMARINE_BEING_CLEANED_UP()
		CLEANUP_MP_SAVED_SUBMARINE(FALSE, FALSE, TRUE, FALSE, TRUE)
	ENDIF
	
ENDPROC
#ENDIF

#IF FEATURE_HEIST_ISLAND
PROC MAINTAIN_MP_SAVED_SUBMARINE_DINGHY()

	VEHICLE_INDEX DinghyVeh
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE,FALSE)
		IF IS_VEHICLE_FUCKED(PERSONAL_SUBMARINE_DINGHY_ID())
			IF IS_SAVED_SUBMARINE_DINGHY_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				CLEAR_SAVED_SUBMARINE_DINGHY_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   MAINTAIN_MP_SAVED_SUBMARINE_DINGHY - MP_SAVED_VEH_FLAG_NOT_EMPTY CLEARED - Does not exist or damaged") NET_NL()
			ENDIF
		ENDIF
	
		//player is spawning a vehicle from insurance company
		IF morsMutualData.bSpawningVeh
			IF NOT IS_NET_VEHICLE_DRIVEABLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV)
				PRINTLN("[personal_submarine] Insurance triggered recreating personal vehicle in slot #",morsMutualData.iSlot)
				morsMutualData.bSpawningVeh = FALSE
			ELSE
				PRINTLN("[personal_submarine] MAINTAIN_MP_SAVED_SUBMARINE_DINGHY: morsMutualData.bSpawningVeh = TRUE, vehicle is driveable.")
				morsMutualData.bSpawningVeh = FALSE
			ENDIF
		ELIF MPGlobals.VehicleData.bAwaitingAssignToMainScript
			//this is setup for when the player exits property they have created a vehicle and set flags but it doesn't exist to get grabbed yet.
			PRINTLN("[personal_submarine] MAINTAIN_MP_SAVED_SUBMARINE_DINGHY: MPGlobals.VehicleData.bAwaitingAssignToMainScript = TRUE")
		ELIF MPGlobals.VehicleData.bAssignToMainScript
			
			PRINTLN("[personal_submarine] MAINTAIN_MP_SAVED_SUBMARINE_DINGHY: MPGlobals.VehicleData.bAssignToMainScript = TRUE")

			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				DinghyVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(DinghyVeh)
				AND IS_VEHICLE_DRIVEABLE(DinghyVeh)
				AND NETWORK_GET_ENTITY_IS_NETWORKED(DinghyVeh)

				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN("[personal_submarine] bAssignToMainScript cleared because...")
						IF NOT DOES_ENTITY_EXIST(DinghyVeh)
							PRINTLN("[personal_submarine] DinghyVeh doesn't exist!")
						ENDIF
						IF NOT IS_VEHICLE_DRIVEABLE(DinghyVeh)
							PRINTLN("[personal_submarine] DinghyVeh isn't driveable!")
						ENDIF
						IF NOT NETWORK_GET_ENTITY_IS_NETWORKED(DinghyVeh)
							PRINTLN("[personal_submarine] DinghyVeh isn't networked!")
						ENDIF
					#ENDIF
				ENDIF
			ENDIF

		ELIF IS_VEHICLE_DRIVEABLE(PERSONAL_SUBMARINE_DINGHY_ID())

			CLEAR_SAVED_SUBMARINE_DINGHY_FLAG(MP_SAVED_VEH_FLAG_CREATION_INIT)

			DinghyVeh = PERSONAL_SUBMARINE_DINGHY_ID()

			IF IS_VEHICLE_DRIVEABLE(DinghyVeh)
				// Clear up stored vehicle if put in for repair or flagged for cleanup
				IF IS_SAVED_SUBMARINE_DINGHY_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP)

				ELIF IS_SAVED_SUBMARINE_DINGHY_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP_DELETE)
					
				ELSE						
					// Make sure vehicle doesn't fall through the ground when the switch cam is active.
					IF IS_SAVED_SUBMARINE_DINGHY_FLAG_SET(MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(DinghyVeh)
						AND HAS_COLLISION_LOADED_AROUND_ENTITY(DinghyVeh)
							FREEZE_ENTITY_POSITION(DinghyVeh, FALSE)
							SET_VEHICLE_ON_GROUND_PROPERLY(DinghyVeh)
							CLEAR_SAVED_SUBMARINE_DINGHY_FLAG(MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
							NET_PRINT("[personal_submarine]  collision - unfreezeing vehicle") NET_NL()
						ENDIF
					ELSE
						IF IS_NET_PLAYER_OK(PLAYER_ID())
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DinghyVeh)
						AND IS_PLAYER_SWITCH_IN_PROGRESS()
						AND NETWORK_HAS_CONTROL_OF_ENTITY(DinghyVeh)
						AND NOT HAS_COLLISION_LOADED_AROUND_ENTITY(DinghyVeh)
							FREEZE_ENTITY_POSITION(DinghyVeh, TRUE)
							SET_SAVED_SUBMARINE_DINGHY_FLAG(MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
							NET_PRINT("[personal_submarine]  collision - freezeing vehicle") NET_NL()
						ENDIF
					ENDIF
					
					//Update Lock State
//					IF MPGlobalsAmbience.iStoredAllowIntoSubmarine != GET_MP_INT_CHARACTER_STAT(MP_STAT_SUBMARINE_ACCESS)
//						IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
//						AND NOT IS_PLAYER_SCTV(PLAYER_ID())
//						AND NOT IS_A_SPECTATOR_CAM_RUNNING()
//							IF NETWORK_GET_ENTITY_IS_NETWORKED(DinghyVeh)
//								SET_SUBMARINE_LOCK_STATE(DinghyVeh)
//							ENDIF
//						ENDIF
//					ENDIF
					
					PROCESS_MP_SAVED_SUBMARINE_DINGHY_FLAGS(DinghyVeh)
				ENDIF
			ELSE
			
				#IF IS_DEBUG_BUILD
					IF NOT IS_VEHICLE_DRIVEABLE(DinghyVeh)
						NET_PRINT("[personal_submarine] IS_VEHICLE_DRIVEABLE(DinghyVeh) = FALSE") NET_NL()
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	MAINTAIN_CLEANUP_MP_SAVED_SUBMARINE_DINGHY()
ENDPROC
#ENDIF


#IF FEATURE_DLC_2_2022
PROC MAINTAIN_MP_SAVED_SUPPORT_BIKE()
	
	VEHICLE_INDEX SupportBikeVeh
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE,FALSE)
		IF IS_VEHICLE_FUCKED(PERSONAL_SUPPORT_BIKE_ID())
			PRINTLN("[personal_support_bike] MAINTAIN_MP_SAVED_SUPPORT_BIKE: vehicle is fcked")
			IF IS_SAVED_SUPPORT_BIKE_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				CLEAR_SAVED_SUPPORT_BIKE_FLAG(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("   ----->   MAINTAIN_MP_SAVED_SUPPORT_BIKE - MP_SAVED_VEH_FLAG_NOT_EMPTY CLEARED - Does not exist or damaged") NET_NL()
			ENDIF
		ENDIF
			
		IF MPGlobals.VehicleData.bAwaitingAssignToMainScript
			//this is setup for when the player exits property they have created a vehicle and set flags but it doesn't exist to get grabbed yet.
			PRINTLN("[personal_support_bike] MAINTAIN_MP_SAVED_SUPPORT_BIKE: MPGlobals.VehicleData.bAwaitingAssignToMainScript = TRUE")
		ELIF MPGlobals.VehicleData.bAssignToMainScript
			PRINTLN("[personal_support_bike] MAINTAIN_MP_SAVED_SUPPORT_BIKE: MPGlobals.VehicleData.bAssignToMainScript = TRUE")
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				SupportBikeVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(SupportBikeVeh)
				AND IS_VEHICLE_DRIVEABLE(SupportBikeVeh)
				AND NETWORK_GET_ENTITY_IS_NETWORKED(SupportBikeVeh)
				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN("[personal_support_bike] bAssignToMainScript cleared because...")
						IF NOT DOES_ENTITY_EXIST(SupportBikeVeh)
							PRINTLN("[personal_support_bike] SupportBikeVeh doesn't exist!")
						ENDIF
						IF NOT IS_VEHICLE_DRIVEABLE(SupportBikeVeh)
							PRINTLN("[personal_support_bike] SupportBikeVeh isn't driveable!")
						ENDIF
						IF NOT NETWORK_GET_ENTITY_IS_NETWORKED(SupportBikeVeh)
							PRINTLN("[personal_support_bike] SupportBikeVeh isn't networked!")
						ENDIF
					#ENDIF
				ENDIF
			ENDIF
		ELIF IS_VEHICLE_DRIVEABLE(PERSONAL_SUPPORT_BIKE_ID())
			CLEAR_SAVED_SUPPORT_BIKE_FLAG(MP_SAVED_VEH_FLAG_CREATION_INIT)
			SupportBikeVeh = PERSONAL_SUPPORT_BIKE_ID()
			
			// Clear up stored vehicle if put in for repair or flagged for cleanup
			IF IS_SAVED_SUPPORT_BIKE_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP)
			ELIF IS_SAVED_SUPPORT_BIKE_FLAG_SET( MP_SAVED_VEH_FLAG_CLEANUP_DELETE)
			ELSE	
				SET_SAVED_SUPPORT_BIKE_FLAG(MP_SAVED_VEH_FLAG_CREATED)
				// Make sure vehicle doesn't fall through the ground when the switch cam is active.
				IF IS_SAVED_SUPPORT_BIKE_FLAG_SET(MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(SupportBikeVeh)
					AND HAS_COLLISION_LOADED_AROUND_ENTITY(SupportBikeVeh)
						FREEZE_ENTITY_POSITION(SupportBikeVeh, FALSE)
						SET_VEHICLE_ON_GROUND_PROPERLY(SupportBikeVeh)
						CLEAR_SAVED_SUPPORT_BIKE_FLAG(MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
						NET_PRINT("[personal_support_bike]  collision - unfreezeing vehicle") NET_NL()
					ENDIF
				ELSE
					IF IS_NET_PLAYER_OK(PLAYER_ID())
					AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SupportBikeVeh)
					AND IS_PLAYER_SWITCH_IN_PROGRESS()
					AND NETWORK_HAS_CONTROL_OF_ENTITY(SupportBikeVeh)
					AND NOT HAS_COLLISION_LOADED_AROUND_ENTITY(SupportBikeVeh)
						FREEZE_ENTITY_POSITION(SupportBikeVeh, TRUE)
						SET_SAVED_SUPPORT_BIKE_FLAG(MP_SAVED_VEH_FLAG_FREEZE_FOR_COLLISION)
						NET_PRINT("[personal_support_bike]  collision - freezeing vehicle") NET_NL()
					ENDIF
				ENDIF	
				PROCESS_MP_SAVED_SUPPORT_BIKE_FLAGS(SupportBikeVeh)
			ENDIF
		ELSE
			IF IS_SAVED_SUPPORT_BIKE_FLAG_SET( MP_SAVED_VEH_FLAG_CREATED)
			AND (NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(PERSONAL_SUPPORT_BIKE_NET_ID())
				 OR NOT IS_VEHICLE_DRIVEABLE(PERSONAL_SUPPORT_BIKE_ID()))
				
				SET_SAVED_SUPPORT_BIKE_FLAG(MP_SAVED_VEH_FLAG_CLEANUP)
				REINIT_NET_TIMER(g_TransitionSessionNonResetVars.contactRequests.stCDTimer[ENUM_TO_INT(REQUEST_SUPPORT_BIKE)], TRUE)
				CONTACT_REQUEST_SET_CD_TIMER(REQUEST_SUPPORT_BIKE)
				NET_PRINT("[personal_support_bike] MAINTAIN_MP_SAVED_SUPPORT_BIKE - bike does not exist or is not drivable") NET_NL()
			ENDIF
		
			#IF IS_DEBUG_BUILD
			NET_PRINT("[personal_support_bike] IS_VEHICLE_DRIVEABLE(PERSONAL_SUPPORT_BIKE_ID()) = FALSE : SET_SAVED_SUPPORT_BIKE_FLAG = MP_SAVED_VEH_FLAG_CLEANUP") NET_NL()
			#ENDIF
		ENDIF
	ELSE
		PRINTLN ("[personal_support_bike] MAINTAIN_MP_SAVED_SUPPORT_BIKE: Player is not OK")
	ENDIF
	
	MAINTAIN_CLEANUP_MP_SAVED_SUPPORT_BIKE()
ENDPROC
#ENDIF

FUNC BOOL IS_PERSONAL_VEHICLE_DRIVEABLE_IN_FREEMODE()
	RETURN MPGlobals.VehicleData.bDriveableInFreemode
ENDFUNC

FUNC BOOL WAS_OWNER_WITH_BOUNTY_IN_PERSONAL_VEHICLE(PLAYER_INDEX ownerID)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(ownerID)].iPersonalVehicleFlagsBS, PERSONAL_VEHICLE_BD_FLAG_IN_VEHICLE_WITH_BOUNTY)
ENDFUNC

FUNC BOOL GET_IS_LOCAL_PARTICIPANT_DOING_HUNT_THE_BEAST_OPT_IN_COUNTDOWN()
	RETURN MPGlobalsAmbience.bDoingHuntTheBeastOptInCountdown
ENDFUNC

//purpose: Check if player is alone in apartment 
FUNC BOOL IS_PLAYER_IN_APARTMENT_WITH_BUDDY()
	PLAYER_INDEX playerToCheck
	INT iPlayerIndex
		REPEAT NUM_NETWORK_PLAYERS iPlayerIndex
			playerToCheck = INT_TO_PLAYERINDEX(iPlayerIndex)
				IF IS_NET_PLAYER_OK(playerToCheck, FALSE)
				AND playerToCheck != PLAYER_ID()
					IF ARE_PLAYERS_IN_SAME_PROPERTY(PLAYER_ID(), playerToCheck, TRUE)
					AND NOT IS_PLAYER_ENTERING_PROPERTY(playerToCheck)
						RETURN TRUE
					ENDIF
				ENDIF	
		ENDREPEAT
RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets number of players who are in same property/office as player 
FUNC INT GET_NUMBER_OF_PLAYERS_IN_SAME_PROPERTY()
	PLAYER_INDEX playerToCheck
	INT iNumPlayers
	INT iPlayerIndex
		REPEAT NUM_NETWORK_PLAYERS iPlayerIndex
			playerToCheck = INT_TO_PLAYERINDEX(iPlayerIndex)
				IF IS_NET_PLAYER_OK(playerToCheck, FALSE)
				AND playerToCheck != PLAYER_ID()
					IF ARE_PLAYERS_IN_SAME_PROPERTY(PLAYER_ID(), playerToCheck, TRUE)
					AND NOT IS_PLAYER_ENTERING_PROPERTY(playerToCheck)
						iNumPlayers++
					ENDIF
				ENDIF	
		ENDREPEAT
	RETURN iNumPlayers
ENDFUNC

/// PURPOSE:
///    Gets number of players who are in same simple interior as player
FUNC INT GET_NUMBER_OF_PLAYERS_IN_SAME_SIMPLE_INTERIOR()
	PLAYER_INDEX playerToCheck
	INT iNumPlayers
	INT iPlayerIndex
		REPEAT NUM_NETWORK_PLAYERS iPlayerIndex
			playerToCheck = INT_TO_PLAYERINDEX(iPlayerIndex)
				IF IS_NET_PLAYER_OK(playerToCheck, FALSE)
				AND playerToCheck != PLAYER_ID()
					IF ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(PLAYER_ID(), playerToCheck, TRUE)
					AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(playerToCheck)
						iNumPlayers++
					ENDIF
				ENDIF	
		ENDREPEAT
	RETURN iNumPlayers
ENDFUNC


PROC SET_LOCAL_PARTICIPANT_DOING_HUNT_THE_BEAST_OPT_IN_COUNTDOWN(BOOL bDoingCountdown)
	#IF IS_DEBUG_BUILD
	IF bDoingCountdown != MPGlobalsAmbience.bDoingHuntTheBeastOptInCountdown
		IF bDoingCountdown
			PRINTLN("[HUNTBEAST] - SET_LOCAL_PARTICIPANT_DOING_HUNT_THE_BEAST_OPT_IN_COUNTDOWN(TRUE) called.")
		ELSE
			PRINTLN("[HUNTBEAST] - SET_LOCAL_PARTICIPANT_DOING_HUNT_THE_BEAST_OPT_IN_COUNTDOWN(FALSE) called.")
		ENDIF
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	MPGlobalsAmbience.bDoingHuntTheBeastOptInCountdown = bDoingCountdown
ENDPROC

PROC MAINTAIN_MC_BIKE_STEALING()
	BOOL bStolenVehicleActive
	VEHICLE_INDEX theVeh
	PLAYER_INDEX theOwner
	IF IS_NET_PLAYER_OK(PLAYER_ID(),TRUE)
	AND MPGlobals.sFreemodeCache.bIsPlayerInAnyVehicle
		theVeh = GET_VEHICLE_PED_IS_IN(MPGlobals.sFreemodeCache.PlayerPedIndex) 
		//PRINTLN("MAINTAIN_MC_BIKE_STEALING: in a vehicle")
		IF MPGlobals.sFreemodeCache.bDoesPlayerVehicleExist
		AND MPGlobals.sFreemodeCache.bIsPlayerVehicleDrivable
		AND (IS_THIS_MODEL_A_BIKE(MPGlobals.sFreemodeCache.PlayerVehicleModel)
		OR IS_THIS_MODEL_A_QUADBIKE(MPGlobals.sFreemodeCache.PlayerVehicleModel))
		AND DOES_ENTITY_EXIST(theVeh)
		AND GET_PED_IN_VEHICLE_SEAT(theVeh) = PLAYER_PED_ID()
			//PRINTLN("MAINTAIN_MC_BIKE_STEALING: drving a bike")
			IF DECOR_EXIST_ON(theVeh,"Player_Vehicle")
			AND DECOR_GET_INT(theVeh, "Player_Vehicle") != -1
				theOwner = GET_OWNER_OF_PERSONAL_VEHICLE(theVeh)
				//PRINTLN("MAINTAIN_MC_BIKE_STEALING: in a personal vehicle")
				IF theOwner != PLAYER_ID()
				AND IS_NET_PLAYER_OK(theOwner,FALSE)
				AND GB_IS_PLAYER_MEMBER_OF_GANG_TYPE(theOwner,TRUE,GT_BIKER)
				AND NOT GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(theOwner,PLAYER_ID())
					bStolenVehicleActive = TRUE
					//PRINTLN("MAINTAIN_MC_BIKE_STEALING: bStolenVehicleActive TRUE")
					IF NOT MPGlobalsAmbience.bStoleMCBikeAwardedPoints
						IF ARE_VECTORS_EQUAL(MPGlobalsAmbience.vStoleMCBikeCoords,<<0,0,0>>)
							MPGlobalsAmbience.vStoleMCBikeCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
							PRINTLN("MAINTAIN_MC_BIKE_STEALING: setting starting vector: ",MPGlobalsAmbience.vStoleMCBikeCoords)
						ELSE
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),MPGlobalsAmbience.vStoleMCBikeCoords,FALSE) > 200 
								IF GB_IS_PLAYER_ACTIVE_AS_BOSS_OF_BIKER_GANG(theOwner)
									BIK_ADD_POINTS_FOR_THIS(BIK_ADD_POINTS_STOLE_RIVAL_PRES_BIKE)	
									PRINTLN("MAINTAIN_MC_BIKE_STEALING: Awarding points for stealing rival president bike")
								ELSE
									BIK_ADD_POINTS_FOR_THIS(BIK_ADD_POINTS_STOLE_RIVAL_BIKE)
									PRINTLN("MAINTAIN_MC_BIKE_STEALING: Awarding points for stealing rival bike")
								ENDIF
								MPGlobalsAmbience.bStoleMCBikeAwardedPoints = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF NOT bStolenVehicleActive
		#IF IS_DEBUG_BUILD
		IF NOT ARE_VECTORS_EQUAL(MPGlobalsAmbience.vStoleMCBikeCoords,<<0,0,0>>)
			PRINTLN("MAINTAIN_MC_BIKE_STEALING: resetting")
		ENDIF
		#ENDIF
		MPGlobalsAmbience.vStoleMCBikeCoords = <<0,0,0>>
		MPGlobalsAmbience.bStoleMCBikeAwardedPoints = FALSE
	ENDIF
ENDPROC

PROC MAINTAIN_RUINER2000(BOOL bDisablePersonalCheck = FALSE)
	//In garage dont electrocute
	IF MPGlobals.sFreemodeCache.bIsPlayerGettingInAnyVehicle
	AND MPGlobals.sFreemodeCache.bIsPlayerVehicleDrivable
	AND MPGlobals.sFreemodeCache.PlayerVehicleModel = RUINER2
			
		IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
			IF NOT IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
				
				VEHICLE_INDEX viVehiclePlayerIsIn =  GET_VEHICLE_PED_IS_USING(MPGlobals.sFreemodeCache.PlayerPedIndex) 
				IF DOES_ENTITY_EXIST(viVehiclePlayerIsIn)
					IF (IS_VEHICLE_A_PERSONAL_VEHICLE(viVehiclePlayerIsIn)
						AND GET_OWNER_OF_PERSONAL_VEHICLE(viVehiclePlayerIsIn) <> PLAYER_ID()
						AND GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(viVehiclePlayerIsIn, PLAYER_ID()))
					OR bDisablePersonalCheck
						SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(viVehiclePlayerIsIn, FALSE)

						IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("HandContactWithDoor"))
							//CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							VECTOR vOffset
							SHOOT_SINGLE_BULLET_BETWEEN_COORDS(
								GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_PELVIS, vOffset), 
								GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_NECK, vOffset), 
								1, TRUE, WEAPONTYPE_STUNGUN, NULL, TRUE, TRUE, -1)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DETERMINE_4_BOTTOM_VECTORS_FOR_VEHICLE_WEAPON(ENTITY_INDEX thisEntity, MODEL_NAMES theModel, VECTOR &FrontBottomLeft, VECTOR &FrontBottomRight, VECTOR &BackBottomLeft, VECTOR &BackBottomRight)
	VECTOR modelMin, modelMax
	
	GET_MODEL_DIMENSIONS(theModel, modelMin, modelMax)
	
	VECTOR tempFrontBottomLeft, tempFrontBottomRight, tempBackBottomLeft, tempBackBottomRight
	
	tempFrontBottomLeft.X = modelMin.X
	tempFrontBottomLeft.Y = modelMax.Y
	tempFrontBottomLeft.Z = modelMin.Z
	
	tempFrontBottomRight.X = modelMax.X
	tempFrontBottomRight.Y = modelMax.Y
	tempFrontBottomRight.Z = modelMin.Z
	
	tempBackBottomLeft.X = modelMin.X
	tempBackBottomLeft.Y = modelMin.Y
	tempBackBottomLeft.Z = modelMin.Z
	
	tempBackBottomRight.X = modelMax.X
	tempBackBottomRight.Y = modelMin.Y
	tempBackBottomRight.Z = modelMin.Z
	
	FrontBottomLeft = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisEntity, tempFrontBottomLeft)
	FrontBottomRight = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisEntity, tempFrontBottomRight)
	BackBottomLeft = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisEntity, tempBackBottomLeft)
	BackBottomRight = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisEntity, tempBackBottomRight)
ENDPROC

FUNC FLOAT GET_INTERP_POINT_FLOAT_FOR_VEHICLE_WEAPON(FLOAT fStartPos, FLOAT fEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	RETURN ((((fEndPos - fStartPos) / (fEndTime - fStartTime)) * (fPointTime - fStartTime)) + fStartPos)
ENDFUNC

FUNC VECTOR GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(VECTOR vStartPos, VECTOR vEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	RETURN <<GET_INTERP_POINT_FLOAT_FOR_VEHICLE_WEAPON(vStartPos.X, vEndPos.X, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT_FOR_VEHICLE_WEAPON(vStartPos.Y, vEndPos.Y, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT_FOR_VEHICLE_WEAPON(vStartPos.Z, vEndPos.Z, fStartTime, fEndTime, fPointTime)>>
ENDFUNC

FUNC BOOL CAN_VEHICLE_HAVE_MINES(VEHICLE_INDEX vVehicle)
	
	IF NETWORK_IS_ACTIVITY_SESSION()
	AND g_bEnablePowerUpMines
		RETURN TRUE
	ENDIF
	
	SWITCH GET_ENTITY_MODEL(vVehicle)
		CASE APC
		CASE DUNE3
		CASE HALFTRACK
		CASE TAMPA3
		CASE INSURGENT3
		CASE TECHNICAL3
		CASE KHANJALI
		CASE SPEEDO4
		CASE MULE4
		CASE POUNDER2
		CASE RCBANDITO
		CASE ISSI4
		CASE SCARAB
		CASE CERBERUS
		CASE IMPALER2
		CASE ZR380
		CASE DOMINATOR4
		CASE IMPERATOR3
		CASE IMPERATOR2
		CASE IMPERATOR
		CASE BRUISER
		CASE SLAMVAN4
		CASE MONSTER3
		CASE ISSI5
		CASE SCARAB2
		CASE CERBERUS2
		CASE IMPALER3
		CASE ZR3802
		CASE DOMINATOR5
		CASE BRUISER2
		CASE SLAMVAN5
		CASE MONSTER4	
		CASE ISSI6
		CASE SCARAB3
		CASE CERBERUS3
		CASE IMPALER4
		CASE ZR3803
		CASE DOMINATOR6
		CASE BRUISER3
		CASE SLAMVAN6
		CASE MONSTER5
		CASE BRUTUS
		CASE BRUTUS2
		CASE BRUTUS3
		CASE JB7002
		CASE CHAMPION
		CASE DEITY
		CASE GRANGER2
		CASE BUFFALO4
		CASE PATRIOT3
		CASE JUBILEE
		#IF FEATURE_DLC_1_2022
		CASE GREENWOOD
		CASE OMNISEGT
		#ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_WEB_BROWSER_OPEN()
	RETURN g_bBrowserVisible
ENDFUNC

FUNC BOOL SHOULD_USE_DPADR_FOR_MINE(VEHICLE_INDEX vehTemp)
	SWITCH GET_ENTITY_MODEL(vehTemp)
		CASE RCBANDITO
		CASE ISSI4
		CASE SCARAB
		CASE CERBERUS
		CASE IMPALER2
		CASE ZR380
		CASE DOMINATOR4
		CASE IMPERATOR3
		CASE IMPERATOR2
		CASE IMPERATOR
		CASE BRUISER
		CASE SLAMVAN4
		CASE MONSTER3
		CASE ISSI5
		CASE SCARAB2
		CASE CERBERUS2
		CASE IMPALER3
		CASE ZR3802
		CASE DOMINATOR5
		CASE BRUISER2
		CASE SLAMVAN5
		CASE MONSTER4	
		CASE ISSI6
		CASE SCARAB3
		CASE CERBERUS3
		CASE IMPALER4
		CASE ZR3803
		CASE DOMINATOR6
		CASE BRUISER3
		CASE SLAMVAN6
		CASE MONSTER5
		CASE BRUTUS
		CASE BRUTUS2
		CASE BRUTUS3
		CASE JB7002
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_VEHICLE_HAVE_MINE_AMO(VEHICLE_INDEX vVehicle)
	IF IS_VEHICLE_MODEL(vVehicle, RCBANDITO)
		IF g_iRCVehicleMineNum = 3
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF SHOULD_USE_DPADR_FOR_MINE(vVehicle)
	AND NOT IS_VEHICLE_MODEL(vVehicle, RCBANDITO)
	AND GET_VEHICLE_BOMB_AMMO(vVehicle) <= 0
	AND (NETWORK_IS_ACTIVITY_SESSION() AND CONTENT_IS_USING_ARENA())
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC 

FUNC BOOL CAN_VEHICLE_USE_MINES(VEHICLE_INDEX vVehicle)
	IF g_bDisableVehicleMines
		PRINTLN("[VEHICLE_MINES] - CAN_VEHICLE_USE_MINES FALSE g_bDisableVehicleMines is true")
		RETURN FALSE
	ENDIF
	
	IF SHOULD_USE_DPADR_FOR_MINE(vVehicle)
		IF IS_PHONE_ONSCREEN()
		OR IS_WEB_BROWSER_OPEN()
		OR IS_PAUSE_MENU_ACTIVE()
			PRINTLN("[VEHICLE_MINES] - CAN_VEHICLE_USE_MINES phone, web or pause FALSE")
			RETURN FALSE
		ENDIF
		
		IF MPGlobals.PlayerInteractionData.iTargetPlayerInt != -1
			PRINTLN("[VEHICLE_MINES] - CAN_VEHICLE_USE_MINES IS_INTERACTION_MENU_OPEN is open FALSE")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_MODEL(vVehicle, RCBANDITO)
		IF NOT IS_LOCAL_PLAYER_USING_RCBANDITO()
		OR NOT IS_SCREEN_FADED_IN()
			PRINTLN("[VEHICLE_MINES] - CAN_VEHICLE_USE_MINES IS_INTERACTION_MENU_OPEN is open FALSE")
			RETURN FALSE
		ENDIF
	ENDIF	
	
	IF CAN_VEHICLE_HAVE_MINES(vVehicle)
	AND GET_VEHICLE_MOD(vVehicle, MOD_WING_R) >= 0
		IF IS_ENTITY_UPRIGHT(vVehicle)
		AND IS_VEHICLE_ON_ALL_WHEELS(vVehicle)
		AND NOT IS_ENTITY_UPSIDEDOWN(vVehicle)
			// Check for the mine mod
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_MOD_SHOP(PLAYER_INDEX playerID)
	IF NATIVE_TO_INT(playerId) > -1
		IF GlobalplayerBD[NATIVE_TO_INT(playerId)].iCurrentShop > -1
			IF GET_SHOP_TYPE_ENUM(INT_TO_ENUM(SHOP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(playerId)].iCurrentShop)) = SHOP_TYPE_CARMOD
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iSpecInfoBitset, SPEC_INFO_BS_IN_MOD_SHOP_TUTORIAL)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_PERSONAL_MOD_SHOP(PLAYER_INDEX playerID)
	IF NATIVE_TO_INT(playerId) > -1
		IF GlobalplayerBD[NATIVE_TO_INT(playerId)].iCurrentShop > -1
			IF GET_SHOP_TYPE_ENUM(INT_TO_ENUM(SHOP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(playerId)].iCurrentShop)) = SHOP_TYPE_PERSONAL_CARMOD
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iSpecInfoBitset, SPEC_INFO_BS_IN_MOD_SHOP_TUTORIAL)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PI_MENU_OPEN()
	IF MPGlobals.PlayerInteractionData.iTargetPlayerInt = -1
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_VEHICLE_HAVE_AIR_BOMBS(VEHICLE_INDEX vVehicle)
	SWITCH GET_ENTITY_MODEL(vVehicle)
		CASE CUBAN800
		CASE MOGUL
		CASE ROGUE
		CASE STARLING
		CASE SEABREEZE
		CASE TULA
		CASE BOMBUSHKA
		CASE HUNTER
		CASE AVENGER
		CASE AKULA
		CASE VOLATOL
		CASE STRIKEFORCE
		#IF FEATURE_HEIST_ISLAND
		CASE ALKONOST
		#ENDIF
			IF GET_VEHICLE_MOD(vVehicle, MOD_WING_R) > -1
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL VEHICLE_SUITABLE_FOR_AIR_FLARES(VEHICLE_INDEX vVehicle)
	SWITCH GET_ENTITY_MODEL(vVehicle)
		CASE CUBAN800
		CASE MOGUL
		CASE ROGUE
		CASE STARLING
		CASE SEABREEZE
		CASE TULA
		CASE BOMBUSHKA
		CASE HUNTER
		CASE NOKOTA
		CASE PYRO
		CASE MOLOTOK
		CASE HAVOK
		CASE ALPHAZ1
		CASE MICROLIGHT
		CASE HOWARD
		CASE AVENGER
		CASE THRUSTER
		CASE VOLATOL
		CASE OPPRESSOR2
		CASE STRIKEFORCE
		CASE BLIMP3
		CASE ALKONOST
		#IF FEATURE_DLC_1_2022
		CASE SEASPARROW2
		CASE SEASPARROW3
		#ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL VEHICLE_SUITABLE_FOR_CHAFF(VEHICLE_INDEX vVehicle)
	SWITCH GET_ENTITY_MODEL(vVehicle)		
		CASE PYRO
		CASE ROGUE
		CASE SEABREEZE
		CASE TULA
		CASE MOGUL
		CASE STARLING
		CASE NOKOTA
		CASE MOLOTOK
		CASE ALPHAZ1
		CASE MICROLIGHT
		CASE HOWARD
		CASE BOMBUSHKA
		CASE HUNTER
		CASE HAVOK
		CASE AVENGER
		CASE THRUSTER
		CASE VOLATOL
		CASE OPPRESSOR2
		CASE STRIKEFORCE
		CASE ALKONOST
		#IF FEATURE_DLC_1_2022
		CASE SEASPARROW2
		CASE SEASPARROW3
		#ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL VEHICLE_SUITABLE_FOR_SMOKE(VEHICLE_INDEX vVehicle)
	SWITCH GET_ENTITY_MODEL(vVehicle)
		CASE CUBAN800
		CASE MOGUL
		CASE ROGUE
		CASE STARLING
		CASE SEABREEZE
		CASE TULA
		CASE BOMBUSHKA
		CASE HUNTER
		CASE NOKOTA
		CASE PYRO
		CASE MOLOTOK
		CASE HAVOK
		CASE ALPHAZ1
		CASE MICROLIGHT
		CASE HOWARD
		CASE AVENGER
		CASE THRUSTER
		CASE VOLATOL
		CASE OPPRESSOR2
		CASE STRIKEFORCE
		CASE ALKONOST
		#IF FEATURE_DLC_1_2022
		CASE SEASPARROW2
		CASE SEASPARROW3
		#ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_VEHICLE_HAVE_AIR_COUNTERMEASURES(VEHICLE_INDEX vVehicle)
	IF VEHICLE_SUITABLE_FOR_AIR_FLARES(vVehicle)
		IF GET_VEHICLE_MOD(vVehicle, MOD_BUMPER_F) =  1
			RETURN TRUE
		ENDIF	
		IF IS_VEHICLE_MODEL(vVehicle, OPPRESSOR2)
			IF GET_VEHICLE_MOD(vVehicle, MOD_GRILL) =  1
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF VEHICLE_SUITABLE_FOR_CHAFF(vVehicle)
		IF GET_VEHICLE_MOD(vVehicle, MOD_BUMPER_F) =  0
			RETURN TRUE
		ENDIF
		IF IS_VEHICLE_MODEL(vVehicle, OPPRESSOR2)
			IF GET_VEHICLE_MOD(vVehicle, MOD_GRILL) =  0
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF VEHICLE_SUITABLE_FOR_SMOKE(vVehicle)
		IF IS_TOGGLE_MOD_ON(vVehicle, MOD_TOGGLE_TYRE_SMOKE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_FIRE_FLARE()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF VEHICLE_SUITABLE_FOR_AIR_FLARES(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				IF GET_VEHICLE_MOD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MOD_BUMPER_F) =  1
					RETURN TRUE
				ENDIF
				IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), OPPRESSOR2)
					IF GET_VEHICLE_MOD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MOD_GRILL) =  1
						RETURN TRUE
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_FIRE_CHAFF(VEHICLE_INDEX vVehicle)
	IF VEHICLE_SUITABLE_FOR_CHAFF(vVehicle)
		IF GET_VEHICLE_MOD(vVehicle, MOD_BUMPER_F) =  0
			RETURN TRUE
		ENDIF	
		IF IS_VEHICLE_MODEL(vVehicle, OPPRESSOR2)
			IF GET_VEHICLE_MOD(vVehicle, MOD_GRILL) =  0
				RETURN TRUE
			ENDIF	
		ENDIF	
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_FIRE_SMOKE()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF VEHICLE_SUITABLE_FOR_AIR_FLARES(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				IF IS_TOGGLE_MOD_ON(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MOD_TOGGLE_TYRE_SMOKE)
				AND GET_VEHICLE_MOD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MOD_BUMPER_F) < 0
					RETURN TRUE
				ENDIF	
				
				IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), OPPRESSOR2)
					IF IS_TOGGLE_MOD_ON(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MOD_TOGGLE_TYRE_SMOKE)
					AND GET_VEHICLE_MOD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MOD_GRILL) < 0
						RETURN TRUE
					ENDIF	
				ENDIF	
			ENDIF
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    This is used to set aricraft bomb type 
/// PARAMS:
///    bombType - WEAPONTYPE_DLC_VEHICLE_BOMB, WEAPONTYPE_DLC_VEHICLE_BOMB_INCENDIARY, WEAPONTYPE_DLC_VEHICLE_BOMB_GAS, WEAPONTYPE_INVALID to turn it off
PROC SET_AIRCRAFT_BOMB_TYPE(VEHICLE_INDEX &vehIndex, WEAPON_TYPE bombType)
	IF DOES_ENTITY_EXIST(vehIndex)
	AND NOT IS_ENTITY_DEAD(vehIndex)
		IF GET_NUM_MOD_KITS(vehIndex) > 0
			SET_VEHICLE_MOD_KIT(vehIndex, 0)
			SWITCH bombType
				CASE WEAPONTYPE_DLC_VEHICLE_BOMB 
					SET_VEHICLE_MOD(vehIndex, MOD_WING_R, 0)
				BREAK
				CASE WEAPONTYPE_DLC_VEHICLE_BOMB_INCENDIARY
					SET_VEHICLE_MOD(vehIndex, MOD_WING_R, 1)
				BREAK
				CASE WEAPONTYPE_DLC_VEHICLE_BOMB_GAS
					SET_VEHICLE_MOD(vehIndex, MOD_WING_R, 2)
				BREAK
				CASE WEAPONTYPE_DLC_VEHICLE_BOMB_CLUSTER
					SET_VEHICLE_MOD(vehIndex, MOD_WING_R, 3)
				BREAK
				CASE WEAPONTYPE_INVALID
					REMOVE_VEHICLE_MOD(vehIndex, MOD_WING_R)
				BREAK	
			ENDSWITCH 
			PRINTLN("SET_AIRCRAFT_BOMB_TYPE Vehicle = ",GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehIndex))," bombType = ",bombType)
		ENDIF
		
		IF GET_ENTITY_MODEL(vehIndex) = INT_TO_ENUM(MODEL_NAMES, HASH("STRIKEFORCE"))
			SET_VEHICLE_MOD(vehIndex, MOD_WING_R, 0)
		ENDIF
	ENDIF
ENDPROC	

/// PURPOSE:
///    This is used to set aricraft countermeasure type 
/// PARAMS:
///    iType - 0 for chaff , 1 for flare and -1 to turn it off
///    IF bSmoke true add colour type:
/// 	TYRE_SMOKE_WHITE
/// 	TYRE_SMOKE_ORANGE
/// 	TYRE_SMOKE_YELLOW
/// 	TYRE_SMOKE_BLUE
///	 	TYRE_SMOKE_RED
/// 	TYRE_SMOKE_BLACK
/// 	TYRE_SMOKE_BUSINESS_PURPLE
/// 	TYRE_SMOKE_BUSINESS_GREEN
/// 	TYRE_SMOKE_HIPSTER_PINK
/// 	TYRE_SMOKE_HIPSTER_BROWN  - THIS IS CYAN SMOKE

PROC SET_AIRCRAFT_COUNTERMEASURE_TYPE(VEHICLE_INDEX &vehIndex, INT iType = -1, BOOL bSmoke = FALSE, TYRE_SMOKE_COLOUR_ENUM smokeColour = TYRE_SMOKE_INVALID)
	IF DOES_ENTITY_EXIST(vehIndex)
	AND NOT IS_ENTITY_DEAD(vehIndex)
		IF GET_NUM_MOD_KITS(vehIndex) > 0
			SET_VEHICLE_MOD_KIT(vehIndex, 0)
			IF !bSmoke
				TOGGLE_VEHICLE_MOD(vehIndex, MOD_TOGGLE_TYRE_SMOKE, FALSE)
				IF IS_VEHICLE_MODEL(vehIndex, OPPRESSOR2)
					SWITCH iType
						CASE 0 
							SET_VEHICLE_MOD(vehIndex, MOD_GRILL, 0)
						BREAK
						CASE 1
							SET_VEHICLE_MOD(vehIndex, MOD_GRILL, 1)
						BREAK
						CASE -1 
							REMOVE_VEHICLE_MOD(vehIndex, MOD_GRILL)
						BREAK
					ENDSWITCH 
				ELSE
					SWITCH iType
						CASE 0 
							SET_VEHICLE_MOD(vehIndex, MOD_BUMPER_F, 0)
						BREAK
						CASE 1
							SET_VEHICLE_MOD(vehIndex, MOD_BUMPER_F, 1)
						BREAK
						CASE -1 
							REMOVE_VEHICLE_MOD(vehIndex, MOD_BUMPER_F)
						BREAK
					ENDSWITCH 
				ENDIF	
			ELSE
				IF IS_VEHICLE_MODEL(vehIndex, OPPRESSOR2)
					REMOVE_VEHICLE_MOD(vehIndex, MOD_GRILL)
				ELSE	
					REMOVE_VEHICLE_MOD(vehIndex, MOD_BUMPER_F)
				ENDIF
				
				TOGGLE_VEHICLE_MOD(vehIndex, MOD_TOGGLE_TYRE_SMOKE, TRUE)
				SET_TYRE_SMOKE_COLOUR_FROM_ENUM(vehIndex, smokeColour)
			ENDIF
			PRINTLN("SET_AIRCRAFT_COUNTERMEASURE_TYPE Vehicle = ",GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehIndex))," iType = ",iType, " bSmoke: ", bSmoke , " smokeColour: ", smokeColour)
		ENDIF
	ENDIF
ENDPROC	

FUNC BOOL IS_LOCAL_PLAYER_AIRCRAFT_SMOKE_ACTIVE()
	RETURN IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_HANGAR_AIRCRAFT_SMOKE_DEPLOYED)	
ENDFUNC

PROC SET_PLAYER_AIRCRAFT_SMOKE_ACTIVE(BOOL bActive)
	IF bActive 
		IF NOT IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_HANGAR_AIRCRAFT_SMOKE_DEPLOYED)	
			SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_HANGAR_AIRCRAFT_SMOKE_DEPLOYED)	
			PRINTLN("SET_PLAYER_AIRCRAFT_SMOKE_ACTIVE - TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_HANGAR_AIRCRAFT_SMOKE_DEPLOYED)	
			CLEAR_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_HANGAR_AIRCRAFT_SMOKE_DEPLOYED)	
			PRINTLN("SET_PLAYER_AIRCRAFT_SMOKE_ACTIVE - FALSE")
		ENDIF
	ENDIF
ENDPROC 

FUNC BOOL HAS_PLAYER_TRIGGERED_AIR_BOMB_DROP()
	IF (IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	OR  IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ATTACK2))
	AND NOT IS_PAUSE_MENU_ACTIVE_EX()
	AND NOT IS_PI_MENU_OPEN()
	AND NOT IS_PHONE_ONSCREEN()
	AND NOT IS_WEB_BROWSER_OPEN()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_PASSIVE_VEHICLE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX vVehicleId = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) != VS_DRIVER
			PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT(vVehicleId)
			
			IF DOES_ENTITY_EXIST(pedDriver)
			AND NOT IS_PED_INJURED(pedDriver)
				PLAYER_INDEX pDriver = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedDriver)
				
				IF IS_NET_PLAYER_OK(pDriver)
				AND IS_PLAYER_IN_PASSIVE_MODE(pDriver)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_STEALTH_MODE_VEHICLE(PLAYER_INDEX player)
	RETURN NATIVE_TO_INT(player)	<> -1
	AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(player)].iBSVehStealthMode, GLOBAL_BS_VEH_STEALTH_MODE_ON)
ENDFUNC

FUNC BOOL CAN_VEHICLE_USE_AIR_BOMBS(VEHICLE_INDEX vVehicle)
	IF g_bDisableVehicleBombs
	OR IS_PLAYER_IN_PASSIVE_MODE(PLAYER_ID())
	OR IS_LOCAL_PLAYER_IN_PASSIVE_VEHICLE()
		RETURN FALSE
	ENDIF
	
	IF CAN_VEHICLE_HAVE_AIR_BOMBS(vVehicle)
		IF IS_ENTITY_UPRIGHT(vVehicle, 50.0)
		AND NOT IS_ENTITY_UPSIDEDOWN(vVehicle)
		AND (GET_ENTITY_HEIGHT_ABOVE_GROUND(vVehicle) >= 1.5  OR IS_VEHICLE_MODEL(vVehicle, VOLATOL) )
		AND (IS_ENTITY_IN_AIR(vVehicle)  OR (IS_VEHICLE_MODEL(vVehicle, VOLATOL) AND NOT IS_VEHICLE_ON_ALL_WHEELS(vVehicle))  )
		AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		AND NOT IS_PLAYER_IN_MOD_SHOP(PLAYER_ID())
		AND NOT g_b_On_Race
		AND NOT IS_PLAYER_IN_STEALTH_MODE_VEHICLE(PLAYER_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_VEHICLE_USE_AIR_COUNTERMEASURES(VEHICLE_INDEX vVehicle)
	IF g_bDisableVehicleCounterMeasures
		RETURN FALSE
	ENDIF
	
	IF CAN_VEHICLE_HAVE_AIR_COUNTERMEASURES(vVehicle)
		IF (GET_ENTITY_HEIGHT_ABOVE_GROUND(vVehicle) >= 1.5  OR IS_VEHICLE_MODEL(vVehicle, VOLATOL) )
		AND (IS_ENTITY_IN_AIR(vVehicle)  OR (IS_VEHICLE_MODEL(vVehicle, VOLATOL) AND NOT IS_VEHICLE_ON_ALL_WHEELS(vVehicle))  )
		AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		AND NOT IS_PLAYER_IN_MOD_SHOP(PLAYER_ID())
		AND NOT g_b_On_Race
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_VEHICLE_USE_AIR_SMOKES(VEHICLE_INDEX vVehicle)
	
	IF g_bDisableVehicleCounterMeasures
		RETURN FALSE
	ENDIF
	
	IF CAN_VEHICLE_HAVE_AIR_COUNTERMEASURES(vVehicle)
		IF (GET_ENTITY_HEIGHT_ABOVE_GROUND(vVehicle) >= 1.5  OR IS_VEHICLE_MODEL(vVehicle, VOLATOL) )
		AND (IS_ENTITY_IN_AIR(vVehicle)  OR (IS_VEHICLE_MODEL(vVehicle, VOLATOL) AND NOT IS_VEHICLE_ON_ALL_WHEELS(vVehicle))  )
		AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		AND NOT IS_PLAYER_IN_MOD_SHOP(PLAYER_ID())
		AND NOT g_b_On_Race
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_ALLOWED_PASSENGER_BOMBS(VEHICLE_INDEX vVehicle)
	SWITCH GET_ENTITY_MODEL(vVehicle)
		CASE ROGUE
		CASE SEABREEZE
		CASE CUBAN800
		CASE VOLATOL
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_ALLOWED_PASSENGER_COUNTERMEASURE(VEHICLE_INDEX vVehicle)
	SWITCH GET_ENTITY_MODEL(vVehicle)
		CASE ROGUE
		CASE SEABREEZE
		CASE TULA
		CASE MOGUL
		CASE PYRO
		CASE AVENGER
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_LOCAL_PLAYER_USE_AIR_BOMBS(VEHICLE_INDEX vVehicle)
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(vVehicle)
	AND IS_VEHICLE_ALLOWED_PASSENGER_BOMBS(vVehicle)
		PLAYER_INDEX pOwner = GET_OWNER_OF_PERSONAL_VEHICLE(vVehicle)
		
		IF IS_NET_PLAYER_OK(pOwner)
			IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
				IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(pOwner)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPassengerBombControl)//GlobalplayerBD_FM[NATIVE_TO_INT(pOwner)].bPassengerBombControl
					RETURN TRUE
				ENDIF
			ELIF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_FRONT_RIGHT
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(pOwner)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPassengerBombControl)//GlobalplayerBD_FM[NATIVE_TO_INT(pOwner)].bPassengerBombControl
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELIF IS_VEHICLE_A_PERSONAL_VEHICLE(vVehicle)
		IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
			RETURN TRUE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF g_bForcePassengerBombControl
			IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_FRONT_RIGHT
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
		#ENDIF
		
		IF IS_VEHICLE_MODEL(vVehicle, AVENGER)
		AND IS_ARMORY_AIRCRAFT_AUTOPILOT_ACTIVE(GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(vVehicle))
			RETURN FALSE
		ENDIF
		
		IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
			IF IS_VEHICLE_MODEL(vVehicle, VOLATOL)
				PED_INDEX pedTemp = GET_PED_IN_VEHICLE_SEAT(vVehicle, VS_FRONT_RIGHT)
				
				IF NOT DOES_ENTITY_EXIST(pedTemp)
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF !MPGlobalsAmbience.bEnablePassengerBombing
				RETURN TRUE
			ENDIF
		ELIF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_FRONT_RIGHT
			IF MPGlobalsAmbience.bEnablePassengerBombing
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ENABLE_COUNTERMEASURE_FOR_MISSION_VEHICLES(BOOL bEnabled)
	IF bEnabled
		IF !MPGlobalsAmbience.bEnableCounterMeasuresForNonePV
			MPGlobalsAmbience.bEnableCounterMeasuresForNonePV = TRUE
			PRINTLN("ENABLE_COUNTERMEASURE_FOR_MISSION_VEHICLES - TRUE")
		ENDIF
	ELSE	
		IF MPGlobalsAmbience.bEnableCounterMeasuresForNonePV
			MPGlobalsAmbience.bEnableCounterMeasuresForNonePV = FALSE
			PRINTLN("ENABLE_COUNTERMEASURE_FOR_MISSION_VEHICLES - FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_ENABLE_COUNTERMEASURE_FOR_MISSION_VEHICLES()
	RETURN MPGlobalsAmbience.bEnableCounterMeasuresForNonePV
ENDFUNC

FUNC BOOL SHOULD_ENABLE_COUNTERMEASURE_FOR_THIS_VEHICLE_MODEL(MODEL_NAMES eModel)
	SWITCH eModel
		CASE SEASPARROW2
		CASE SEASPARROW3
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_LOCAL_PLAYER_USE_COUNTERMEASURE(VEHICLE_INDEX vVehicle)
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(vVehicle)
	AND IS_VEHICLE_ALLOWED_PASSENGER_COUNTERMEASURE(vVehicle)
		IF IS_VEHICLE_MODEL(vVehicle, TULA)
		OR IS_VEHICLE_MODEL(vVehicle, AVENGER)
			IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_FRONT_RIGHT
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
		
		PLAYER_INDEX pOwner = GET_OWNER_OF_PERSONAL_VEHICLE(vVehicle)
		
		IF IS_NET_PLAYER_OK(pOwner)
			IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
				IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(pOwner)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPassengerCounterMeasureControl)//GlobalplayerBD_FM[NATIVE_TO_INT(pOwner)].bPassengerCounterMeasureControl
					RETURN TRUE
				ENDIF
			ELIF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_FRONT_RIGHT
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(pOwner)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPassengerCounterMeasureControl)//GlobalplayerBD_FM[NATIVE_TO_INT(pOwner)].bPassengerCounterMeasureControl
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELIF IS_VEHICLE_A_PERSONAL_VEHICLE(vVehicle)
	OR SHOULD_ENABLE_COUNTERMEASURE_FOR_MISSION_VEHICLES()
	OR SHOULD_ENABLE_COUNTERMEASURE_FOR_THIS_VEHICLE_MODEL(GET_ENTITY_MODEL(vVehicle))
		IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
			RETURN TRUE
		ENDIF
	ELIF IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vVehicle)
		IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
			IF IS_VEHICLE_MODEL(vVehicle, TULA)
			OR IS_VEHICLE_MODEL(vVehicle, AVENGER)
				RETURN FALSE
			ENDIF
			
			IF !MPGlobalsAmbience.bEnablePassengerChaff 
				RETURN TRUE
			ENDIF	
		ELIF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_FRONT_RIGHT
			IF MPGlobalsAmbience.bEnablePassengerChaff 
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_BOMB_STRING(VEHICLE_INDEX vVehicle)
	IF GET_VEHICLE_MOD(vVehicle, MOD_WING_R) = 0
		RETURN "BOMB_NO_AMMO_0"
	ENDIF
	IF GET_VEHICLE_MOD(vVehicle, MOD_WING_R) = 1
		RETURN "BOMB_NO_AMMO_1"
	ENDIF
	IF GET_VEHICLE_MOD(vVehicle, MOD_WING_R) = 2
		RETURN "BOMB_NO_AMMO_2"
	ENDIF
	IF GET_VEHICLE_MOD(vVehicle, MOD_WING_R) = 3
		RETURN "BOMB_NO_AMMO_3"
	ENDIF
	
	RETURN "BOMB_NO_AMMO_0"
ENDFUNC

PROC MAINTAIN_VEHICLE_AIR_BOMBS_HELP_TEXT(VEHICLE_INDEX vVehicle, BOOL bUsingWaterBombs = FALSE)
	
	IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bDisplayBombHelpText)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bDisplayBombHelpText
	AND IS_VEHICLE_DRIVEABLE(vVehicle)
	AND CAN_LOCAL_PLAYER_USE_AIR_BOMBS(vVehicle)
	AND CAN_VEHICLE_HAVE_AIR_BOMBS(vVehicle)
		IF (GET_ENTITY_HEIGHT_ABOVE_GROUND(vVehicle) >= 1.5  OR IS_VEHICLE_MODEL(vVehicle, VOLATOL) )
		AND (IS_ENTITY_IN_AIR(vVehicle)  OR (IS_VEHICLE_MODEL(vVehicle, VOLATOL) AND NOT IS_VEHICLE_ON_ALL_WHEELS(vVehicle))  )
		AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		AND NOT IS_PLAYER_IN_MOD_SHOP(PLAYER_ID())
		AND NOT g_b_On_Race
		AND NOT IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
			IF NOT IS_ENTITY_UPRIGHT(vVehicle, 50.0)
				IF HAS_PLAYER_TRIGGERED_AIR_BOMB_DROP()
					IF GET_PACKED_STAT_INT(PACKED_MP_INT_AIR_BOMB_HELP_TEXT_COUNTER) <= 3
						PRINT_HELP(PICK_STRING(bUsingWaterBombs, "WB_ROT_HELP", "BOMB_ROT_HELP"))
						INT iHelpTextCounter = GET_PACKED_STAT_INT(PACKED_MP_INT_AIR_BOMB_HELP_TEXT_COUNTER)
						SET_PACKED_STAT_INT(PACKED_MP_INT_AIR_BOMB_HELP_TEXT_COUNTER, iHelpTextCounter+1)
						//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bDisplayBombHelpText = TRUE
					ENDIF
					SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bDisplayBombHelpText)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT MPGlobalsAmbience.bDisplayedBombAmmoHelp
		IF IS_VEHICLE_DRIVEABLE(vVehicle)
		AND CAN_VEHICLE_HAVE_AIR_BOMBS(vVehicle)
		AND IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), vVehicle)
		AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
		AND IS_VEHICLE_A_PERSONAL_VEHICLE(vVehicle)
		AND GET_OWNER_OF_PERSONAL_VEHICLE(vVehicle) = PLAYER_ID()
			IF NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
			AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			AND NOT IS_PLAYER_IN_MOD_SHOP(PLAYER_ID())
			AND NOT g_b_On_Race
			AND NOT IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
				IF GET_VEHICLE_BOMB_AMMO(vVehicle) <= 0
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF bUsingWaterBombs
							PRINT_HELP("WB_NO_AMMO")
						ELSE
							PRINT_HELP_WITH_STRING("BOMB_NO_AMMO", GET_BOMB_STRING(vVehicle))
						ENDIF
						
						MPGlobalsAmbience.bDisplayedBombAmmoHelp = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bDisplayBombHelpText)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bDisplayBombHelpText
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(PICK_STRING(bUsingWaterBombs, "WB_ROT_HELP", "BOMB_ROT_HELP"))
			//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bDisplayBombHelpText = FALSE
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bDisplayBombHelpText)
		ENDIF
	ENDIF
	
ENDPROC

FUNC STRING GET_COUNTERMEASURE_STRING(VEHICLE_INDEX vVehicle)
	
	MOD_TYPE modToCheck = MOD_BUMPER_F
	IF IS_VEHICLE_MODEL(vVehicle, OPPRESSOR2)
		modToCheck = MOD_GRILL
	ENDIF
	
	IF VEHICLE_SUITABLE_FOR_AIR_FLARES(vVehicle)
		IF GET_VEHICLE_MOD(vVehicle, modToCheck) =  1
			RETURN "CM_NO_AMMO_FLR"
		ENDIF
	ENDIF
	
	IF VEHICLE_SUITABLE_FOR_CHAFF(vVehicle)
		IF GET_VEHICLE_MOD(vVehicle, modToCheck) =  0
			RETURN "CM_NO_AMMO_CHF"
		ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC

PROC MAINTAIN_VEHICLE_COUNTERMEASURE_HELP_TEXT(VEHICLE_INDEX vVehicle)
	
	IF NOT MPGlobalsAmbience.bDisplayedCountermeasureAmmoHelp
		IF IS_VEHICLE_DRIVEABLE(vVehicle)
		AND CAN_VEHICLE_HAVE_AIR_COUNTERMEASURES(vVehicle)
		AND IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), vVehicle)
		AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
		AND ((IS_VEHICLE_A_PERSONAL_VEHICLE(vVehicle) AND GET_OWNER_OF_PERSONAL_VEHICLE(vVehicle) = PLAYER_ID())
			OR SHOULD_ENABLE_COUNTERMEASURE_FOR_MISSION_VEHICLES() OR SHOULD_ENABLE_COUNTERMEASURE_FOR_THIS_VEHICLE_MODEL(GET_ENTITY_MODEL(vVehicle)))
		AND (SHOULD_VEHICLE_FIRE_FLARE()
		OR SHOULD_VEHICLE_FIRE_CHAFF(vVehicle))
			IF NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
			AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			AND NOT IS_PLAYER_IN_MOD_SHOP(PLAYER_ID())
			AND NOT g_b_On_Race
				IF GET_VEHICLE_COUNTERMEASURE_AMMO(vVehicle) <= 0
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF IS_VEHICLE_MODEL(vVehicle, OPPRESSOR2)
							PRINT_HELP_WITH_STRING("CM_NO_AMMO_OP", GET_COUNTERMEASURE_STRING(vVehicle))
						ELSE
							PRINT_HELP_WITH_STRING("CM_NO_AMMO", GET_COUNTERMEASURE_STRING(vVehicle))
						ENDIF
						MPGlobalsAmbience.bDisplayedCountermeasureAmmoHelp = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC FLOAT GET_CHAFF_WING_OFFSET(VEHICLE_INDEX vehPlane, INT iIndex)
	SWITCH GET_ENTITY_MODEL(vehPlane)
		CASE PYRO
		CASE ROGUE
		CASE SEABREEZE
		CASE TULA
		CASE MOGUL
		CASE STARLING
		CASE NOKOTA
		CASE MOLOTOK
		CASE ALPHAZ1
		CASE MICROLIGHT
		CASE HOWARD
		CASE BOMBUSHKA
		CASE OPPRESSOR2
		CASE STRIKEFORCE
			SWITCH iIndex
				CASE 0	RETURN 0.2	BREAK
				CASE 1	RETURN 0.8	BREAK
				CASE 2	RETURN 0.0	BREAK
				CASE 3	RETURN 1.0	BREAK
			ENDSWITCH
		BREAK
		
		CASE HUNTER
		CASE HAVOK
		CASE AKULA
		CASE THRUSTER
		CASE SEASPARROW2
			SWITCH iIndex
				CASE 0	RETURN 0.4	BREAK
				CASE 1	RETURN 0.6	BREAK
				CASE 2	RETURN 0.35	BREAK
				CASE 3	RETURN 0.65	BREAK
			ENDSWITCH
		BREAK
		
		CASE AVENGER
		CASE VOLATOL
			SWITCH iIndex
				CASE 0	RETURN 0.4	BREAK
				CASE 1	RETURN 0.6	BREAK
				CASE 2	RETURN 0.3	BREAK
				CASE 3	RETURN 0.7	BREAK
			ENDSWITCH
		BREAK
		
		CASE ALKONOST
			SWITCH iIndex
				CASE 0	RETURN 0.2	BREAK
				CASE 1	RETURN 0.8	BREAK
				CASE 2	RETURN 0.1	BREAK
				CASE 3	RETURN 0.9	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_CHAFF_OFFSET(VEHICLE_INDEX vehPlane, INT iIndex)
	SWITCH GET_ENTITY_MODEL(vehPlane)
		CASE PYRO
		CASE MOGUL
		CASE STARLING
		CASE NOKOTA
		CASE MOLOTOK
		CASE ALPHAZ1
		CASE MICROLIGHT
		CASE HOWARD
		CASE HUNTER
		CASE HAVOK
		CASE AKULA
		CASE THRUSTER
		CASE SEASPARROW2
			SWITCH iIndex
				CASE 0	RETURN 0.4	BREAK
				CASE 1	RETURN 0.4	BREAK
				CASE 2	RETURN 0.45	BREAK
				CASE 3	RETURN 0.45	BREAK
			ENDSWITCH
		BREAK
		
		CASE ROGUE
		CASE TULA
			SWITCH iIndex
				CASE 0	RETURN 0.4	BREAK
				CASE 1	RETURN 0.4	BREAK
				CASE 2	RETURN 0.4	BREAK
				CASE 3	RETURN 0.4	BREAK
			ENDSWITCH
		BREAK
		
		CASE SEABREEZE
			SWITCH iIndex
				CASE 0	RETURN 0.45	BREAK
				CASE 1	RETURN 0.45	BREAK
				CASE 2	RETURN 0.45	BREAK
				CASE 3	RETURN 0.45	BREAK
			ENDSWITCH
		BREAK
		
		CASE BOMBUSHKA
			SWITCH iIndex
				CASE 0	RETURN 0.35	BREAK
				CASE 1	RETURN 0.35	BREAK
				CASE 2	RETURN 0.35	BREAK
				CASE 3	RETURN 0.35	BREAK
			ENDSWITCH
		BREAK
		
		CASE AVENGER
		CASE VOLATOL
			SWITCH iIndex
				CASE 0	RETURN 0.4	BREAK
				CASE 1	RETURN 0.4	BREAK
				CASE 2	RETURN 0.4	BREAK
				CASE 3	RETURN 0.4	BREAK
			ENDSWITCH
		BREAK
		
		CASE ALKONOST
			SWITCH iIndex
				CASE 0	RETURN 0.6	BREAK
				CASE 1	RETURN 0.6	BREAK
				CASE 2	RETURN 0.6	BREAK
				CASE 3	RETURN 0.6	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_CHAFF_Z_OFFSET(VEHICLE_INDEX vehPlane, INT iIndex)
	SWITCH GET_ENTITY_MODEL(vehPlane)
		CASE PYRO
		CASE MOGUL
		CASE STARLING
		CASE NOKOTA
		CASE MOLOTOK
		CASE HOWARD
		CASE THRUSTER
			SWITCH iIndex
				CASE 0	RETURN 1.0	BREAK
				CASE 1	RETURN 1.0	BREAK
				CASE 2	RETURN 1.4	BREAK
				CASE 3	RETURN 1.4	BREAK
			ENDSWITCH
		BREAK
		
		CASE ROGUE
			SWITCH iIndex
				CASE 0	RETURN 1.5	BREAK
				CASE 1	RETURN 1.5	BREAK
				CASE 2	RETURN 1.8	BREAK
				CASE 3	RETURN 1.8	BREAK
			ENDSWITCH
		BREAK
		
		CASE SEABREEZE
			SWITCH iIndex
				CASE 0	RETURN 1.0	BREAK
				CASE 1	RETURN 1.0	BREAK
				CASE 2	RETURN 1.0	BREAK
				CASE 3	RETURN 1.0	BREAK
			ENDSWITCH
		BREAK
		
		CASE TULA
			SWITCH iIndex
				CASE 0	RETURN 2.4	BREAK
				CASE 1	RETURN 2.4	BREAK
				CASE 2	RETURN 2.6	BREAK
				CASE 3	RETURN 2.6	BREAK
			ENDSWITCH
		BREAK
		
		CASE ALPHAZ1
		CASE HAVOK
			SWITCH iIndex
				CASE 0	RETURN 0.0	BREAK
				CASE 1	RETURN 0.0	BREAK
				CASE 2	RETURN 0.2	BREAK
				CASE 3	RETURN 0.2	BREAK
			ENDSWITCH
		BREAK
		
		CASE MICROLIGHT
		CASE HUNTER
		CASE AKULA
		CASE SEASPARROW2
			SWITCH iIndex
				CASE 0	RETURN 2.0	BREAK
				CASE 1	RETURN 2.0	BREAK
				CASE 2	RETURN 2.0	BREAK
				CASE 3	RETURN 2.0	BREAK
			ENDSWITCH
		BREAK
		
		CASE BOMBUSHKA
		CASE VOLATOL
			SWITCH iIndex
				CASE 0	RETURN 3.0	BREAK
				CASE 1	RETURN 3.0	BREAK
				CASE 2	RETURN 3.0	BREAK
				CASE 3	RETURN 3.0	BREAK
			ENDSWITCH
		BREAK
		
		CASE AVENGER
			SWITCH iIndex
				CASE 0	RETURN 4.5	BREAK
				CASE 1	RETURN 4.5	BREAK
				CASE 2	RETURN 4.5	BREAK
				CASE 3	RETURN 4.5	BREAK
			ENDSWITCH
		BREAK
		
		CASE ALKONOST
			SWITCH iIndex
				CASE 0	RETURN 0.0	BREAK
				CASE 1	RETURN 0.0	BREAK
				CASE 2	RETURN 0.0	BREAK
				CASE 3	RETURN 0.0	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_CHAFF_WING_ROTATION(VEHICLE_INDEX vehPlane, INT iIndex)
	SWITCH GET_ENTITY_MODEL(vehPlane)
		CASE PYRO
		CASE ROGUE
		CASE SEABREEZE
		CASE TULA
		CASE MOGUL
		CASE STARLING
		CASE NOKOTA
		CASE MOLOTOK
		CASE ALPHAZ1
		CASE MICROLIGHT
		CASE HOWARD
		CASE BOMBUSHKA
		CASE HUNTER
		CASE HAVOK
		CASE AVENGER
		CASE AKULA
		CASE THRUSTER
		CASE VOLATOL
		CASE ALKONOST
		CASE SEASPARROW2
			SWITCH iIndex
				CASE 0	RETURN <<-90.0, -10.0, 0.0>>	BREAK
				CASE 1	RETURN <<-90.0, 10.0, 0.0>>		BREAK
				CASE 2	RETURN <<-180.0, 45.0, 0.0>>	BREAK
				CASE 3	RETURN <<-180.0, -45.0, 0.0>>	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_CHAFF_SCALE(VEHICLE_INDEX vehPlane, INT iIndex)
	SWITCH GET_ENTITY_MODEL(vehPlane)
		CASE PYRO
		CASE ROGUE
		CASE SEABREEZE
		CASE TULA
		CASE MOGUL
		CASE STARLING
		CASE NOKOTA
		CASE MOLOTOK
		CASE ALPHAZ1
		CASE MICROLIGHT
		CASE HOWARD
		CASE HUNTER
		CASE AKULA
		CASE OPPRESSOR2
		CASE STRIKEFORCE
		CASE ALKONOST
		CASE SEASPARROW2
			SWITCH iIndex
				CASE 0
				CASE 1
				CASE 2
				CASE 3
					RETURN 10.0
				BREAK
			ENDSWITCH
		BREAK
		
		CASE BOMBUSHKA
			SWITCH iIndex
				CASE 0
				CASE 1
				CASE 2
				CASE 3
					RETURN 5.0
				BREAK
			ENDSWITCH
		BREAK
		
		CASE HAVOK
			SWITCH iIndex
				CASE 0
				CASE 1
				CASE 2
				CASE 3
					RETURN 7.5
				BREAK
			ENDSWITCH
		BREAK
		
		CASE AVENGER
		CASE VOLATOL
			SWITCH iIndex
				CASE 0
				CASE 1
				CASE 2
				CASE 3
					RETURN 5.0
				BREAK
			ENDSWITCH
		BREAK
		
		CASE THRUSTER
			SWITCH iIndex
				CASE 0
				CASE 1
				CASE 2
				CASE 3
					RETURN 4.0
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

PROC FIRE_SPECIFIED_CHAFF(VEHICLE_INDEX vehPlane, INT iIndex, VECTOR vFrontBottomLeft, VECTOR vFrontBottomRight, VECTOR vBackBottomLeft, VECTOR vBackBottomRight, VECTOR vPlaneRot)
	VECTOR vB1, vB2, vB3
	
	vB1 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vFrontBottomLeft, vFrontBottomRight, 0.0, 1.0, GET_CHAFF_WING_OFFSET(vehPlane, iIndex))
	vB2 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vBackBottomLeft, vBackBottomRight, 0.0, 1.0, GET_CHAFF_WING_OFFSET(vehPlane, iIndex))
	vB3 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vB1, vB2, 0.0, 1.0, GET_CHAFF_OFFSET(vehPlane, iIndex))
	vB3.z += GET_CHAFF_Z_OFFSET(vehPlane, iIndex)
	
	USE_PARTICLE_FX_ASSET("scr_sm_counter")
	START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_sm_counter_chaff", vB3, <<vPlaneRot.X, 0.0, vPlaneRot.Z>> + GET_CHAFF_WING_ROTATION(vehPlane, iIndex), GET_CHAFF_SCALE(vehPlane, iIndex))
ENDPROC

FUNC FLOAT GET_FLARE_X_POS(VEHICLE_INDEX vehPlane, INT iIndex)
	SWITCH iIndex
		CASE 1
			IF GET_ENTITY_MODEL(vehPlane) = HUNTER
			OR GET_ENTITY_MODEL(vehPlane) = AKULA
				RETURN -1.0
			ELIF GET_ENTITY_MODEL(vehPlane) = HAVOK
				RETURN 1.5
			ELIF GET_ENTITY_MODEL(vehPlane) = AVENGER
			OR GET_ENTITY_MODEL(vehPlane) = VOLATOL
				RETURN -4.0
			ELIF GET_ENTITY_MODEL(vehPlane) = THRUSTER
				RETURN 1.0
			ELIF GET_ENTITY_MODEL(vehPlane) = OPPRESSOR2
				RETURN 0.00
			ELSE
				RETURN -3.0
			ENDIF
		BREAK
		
		CASE 2
			IF GET_ENTITY_MODEL(vehPlane) = HUNTER
			OR GET_ENTITY_MODEL(vehPlane) = AKULA
				RETURN -0.5
			ELIF GET_ENTITY_MODEL(vehPlane) = HAVOK
				RETURN 0.5
			ELIF GET_ENTITY_MODEL(vehPlane) = AVENGER
			OR GET_ENTITY_MODEL(vehPlane) = VOLATOL
				RETURN -3.0
			ELIF GET_ENTITY_MODEL(vehPlane) = THRUSTER
				RETURN 0.5
			ELIF GET_ENTITY_MODEL(vehPlane) = OPPRESSOR2
				RETURN 0.0
			ELSE
				RETURN -1.5
			ENDIF
		BREAK
		
		CASE 3
			IF GET_ENTITY_MODEL(vehPlane) = HUNTER
			OR GET_ENTITY_MODEL(vehPlane) = AKULA
				RETURN 0.5
			ELIF GET_ENTITY_MODEL(vehPlane) = HAVOK
				RETURN -0.5
			ELIF GET_ENTITY_MODEL(vehPlane) = AVENGER
			OR GET_ENTITY_MODEL(vehPlane) = VOLATOL
				RETURN 3.0
			ELIF GET_ENTITY_MODEL(vehPlane) = THRUSTER
				RETURN -0.5
			ELIF GET_ENTITY_MODEL(vehPlane) = OPPRESSOR2
				RETURN 0.0
			ELSE
				RETURN 1.5
			ENDIF
		BREAK
		
		CASE 4
			IF GET_ENTITY_MODEL(vehPlane) = HUNTER
			OR GET_ENTITY_MODEL(vehPlane) = AKULA
				RETURN 1.0
			ELIF GET_ENTITY_MODEL(vehPlane) = HAVOK
				RETURN 1.5
			ELIF GET_ENTITY_MODEL(vehPlane) = AVENGER
			OR GET_ENTITY_MODEL(vehPlane) = VOLATOL
				RETURN 4.0
			ELIF GET_ENTITY_MODEL(vehPlane) = THRUSTER
				RETURN -1.0
			ELIF GET_ENTITY_MODEL(vehPlane) = OPPRESSOR2
				RETURN 0.0
			ELSE
				RETURN 3.0
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_FLARE_Y_POS(VEHICLE_INDEX vehPlane)
	SWITCH GET_ENTITY_MODEL(vehPlane)
		CASE CUBAN800
		CASE MOGUL
		CASE ALPHAZ1
		CASE TULA
		CASE BOMBUSHKA
		CASE HOWARD
			RETURN 3.0
		BREAK

		CASE NOKOTA
		CASE PYRO
			RETURN -0.8
		BREAK

		CASE SEABREEZE
		CASE MICROLIGHT
		CASE HAVOK
		CASE THRUSTER
		CASE ALKONOST
			RETURN 1.0
		BREAK
		
		CASE OPPRESSOR2
			RETURN 2.0
		BREAK	
		
		CASE ROGUE
		CASE STARLING
		CASE MOLOTOK
			RETURN 0.0
		BREAK

		CASE HUNTER
		CASE AKULA
			RETURN 4.0
		BREAK
		
		CASE AVENGER
		CASE VOLATOL
			RETURN 3.0
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_FLARE_Z_POS(VEHICLE_INDEX vehPlane)
	SWITCH GET_ENTITY_MODEL(vehPlane)
		CASE CUBAN800
		CASE MOGUL
		CASE ALPHAZ1
		CASE HOWARD
		CASE NOKOTA
		CASE PYRO
		CASE SEABREEZE
		CASE ROGUE
		CASE STARLING
		CASE MOLOTOK
			RETURN -0.3
		BREAK

		CASE TULA
		CASE BOMBUSHKA
			RETURN 2.0
		BREAK
		
		CASE MICROLIGHT
			RETURN 3.0
		BREAK
		
		CASE AVENGER
			RETURN 2.0
		BREAK
		
		CASE VOLATOL
		CASE OPPRESSOR2
		CASE ALKONOST
			RETURN 0.0
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

PROC SET_OVERRIDE_CHAFF_COOLDOWN(BOOL bSet, INT iChaffCooldown = 0)
	IF bSet
		PRINTLN("SET_OVERRIDE_CHAFF_COOLDOWN - overriding chaff cooldown: ", iChaffCooldown)
		
		g_bOverrideChaffCooldown = TRUE
		g_iChaffCooldown = iChaffCooldown
	ELSE
		PRINTLN("SET_OVERRIDE_FLARE_COOLDOWN - resetting chaff override")
		
		g_bOverrideChaffCooldown = FALSE
		g_iChaffCooldown = 0
	ENDIF
ENDPROC

PROC SET_OVERRIDE_FLARE_COOLDOWN(BOOL bSet, INT iFlareCooldown = 0)
	IF bSet
		PRINTLN("SET_OVERRIDE_FLARE_COOLDOWN - overriding flare cooldown: ", iFlareCooldown)
		
		g_bOverrideFlareCooldown = TRUE
		g_iFlareCooldown = iFlareCooldown
	ELSE
		PRINTLN("SET_OVERRIDE_FLARE_COOLDOWN - resetting flare override")
		
		g_bOverrideFlareCooldown = FALSE
		g_iFlareCooldown = 0
	ENDIF
ENDPROC

PROC SET_OVERRIDE_COUNTERMEASURE_AMMO(BOOL bSet)
	IF bSet
		PRINTLN("SET_OVERRIDE_COUNTERMEASURE_AMMO - overriding ammo")
		g_bAllowCountermeasureAmmo = TRUE
	ELSE
		PRINTLN("SET_OVERRIDE_COUNTERMEASURE_AMMO - resetting ammo override")
		g_bAllowCountermeasureAmmo = FALSE
	ENDIF
ENDPROC

PROC RESET_COUNTERMEASURE_OVERRIDES()
	PRINTLN("RESET_COUNTERMEASURE_OVERRIDES - called")
	
	g_bAllowCountermeasureAmmo = FALSE
	g_bOverrideChaffCooldown = FALSE
	g_iChaffCooldown = 0
	g_bOverrideFlareCooldown = FALSE
	g_iFlareCooldown = 0
ENDPROC

FUNC SCRIPT_TIMER GET_FLARE_COOLDOWN_TIMER()
	RETURN g_sCountermeasurePressDelay
ENDFUNC

FUNC SCRIPT_TIMER GET_CHAFF_COOLDOWN_TIMER()
	RETURN g_sLockOnBlock
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ALLOWED_IN_AB()
	IF FM_EVENT_GET_VARIATION_PLAYER_IS_ON(PLAYER_ID(), FMMC_TYPE_SUPER_BUSINESS_BATTLES) = ENUM_TO_INT(BBCV_CAPTURED_UFO)
		RETURN TRUE
	ENDIF

	IF DOES_PLAYER_OWN_AN_AB_HANGAR(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
		PLAYER_INDEX pBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		
		IF pBoss != INVALID_PLAYER_INDEX()
			IF DOES_PLAYER_OWN_AN_AB_HANGAR(pBoss)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		PLAYER_INDEX PlayerID = INT_TO_PLAYERINDEX(i)
		
		IF IS_NET_PLAYER_OK(playerID, FALSE)
			IF (PlayerID != PLAYER_ID())
				
				// Allow access if in same gang as AB hangar owner
				IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
					IF GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), PlayerID)
						IF DOES_PLAYER_OWN_AN_AB_HANGAR(PlayerID)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
				
				// Allow access if player in same vehicle as AB hangar owner & AB hangar owner is driver
				IF ARE_PEDS_IN_THE_SAME_VEHICLE(GET_PLAYER_PED(PLAYER_ID()), GET_PLAYER_PED(PlayerID))
					VEHICLE_INDEX vehicleID = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PlayerID))
					
					IF GET_PED_IN_VEHICLE_SEAT(vehicleID, VS_DRIVER) = GET_PLAYER_PED(PlayerID)
						IF DOES_PLAYER_OWN_AN_AB_HANGAR(PlayerID)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF		
		ENDIF	
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Additional area check to suplement the one managed in area_checks.sch
/// PARAMS:
///    vInput - player's position
///    iUpperZ - adjusting how tall the volume should be
/// RETURNS:
///    True if the player is int he army base
FUNC BOOL IS_COORD_IN_ARMY_BASE_LOCAL_CHECK(VECTOR vInput, INT iUpperZ = 0)
	// USE VDIST2 (VDIST^2) prior to using this check for wanted levels.
	VECTOR vPos1[TOTAL_SUB_AREAS]
	VECTOR vPos2[TOTAL_SUB_AREAS]
	FLOAT  fWidth[TOTAL_SUB_AREAS]
	INT    numberOfSubAreas, i
	BOOL   bHighlight = FALSE
	
	vPos1[0] = <<-1877.435791,2783.150635,31.806177>>
	vPos2[0] = <<-1797.873413,2924.369629,307.110565 + iUpperZ>>
	fWidth[0] = 88.000000
	
	vPos1[1] = <<-1761.115601,2834.651123,31.806217>>
	vPos2[1] = <<-1720.887451,2906.419189,281.806244 + iUpperZ>>
	fWidth[1] = 88.000000
	
	vPos1[2] = <<-1761.493286,2817.679932,32.371250>>
	vPos2[2] = <<-1769.704468,2913.734375,281.806610 + iUpperZ>>
	fWidth[2] = 88.000000
	
	vPos1[3] = <<-1879.296997,2772.652588,29.577950>>
	vPos2[3] = <<-1864.520508,2825.472412,281.806183 + iUpperZ>>
	fWidth[3] = 88.000000
	
	vPos1[4] = <<-2676.739990,3366.987549,29.923937>>
	vPos2[4] = <<-2744.789551,3340.346680,37.803787 + iUpperZ>>
	fWidth[4] = 9.000000
	
	vPos1[5] = <<-2676.739990,3366.987549,29.923937>>
	vPos2[5] = <<-2744.789551,3340.346680,37.803787 + iUpperZ>>
	fWidth[5] = 9.5
	
	vPos1[6] = <<-2807.396729,3264.225586,29.927641>>
	vPos2[6] = <<-2879.805176,3309.009521,37.578644 + iUpperZ>> 
	fWidth[6] = 9.5

	vPos1[7] = <<-2808.2361, 3264.6343,29.927641>>
	vPos2[7] = <<-2747.550293,3187.800049,37.456821 + iUpperZ>> 
	fWidth[7] = 9.5

	vPos1[8] = <<-2530.995850,3064.150879,29.710810>>
	vPos2[8] = <<-2748.5571, 3189.9392,37.456821 + iUpperZ>> 
	fWidth[8] = 9.5

	vPos1[9] = <<-2533.901611,3062.490723,29.707146>>
	vPos2[9] = <<-2520.674072,2996.817383,37.457144 + iUpperZ>> 
	fWidth[9] = 9.5

	vPos1[10] = <<-2500.716309,2927.102783,29.456631>>
	vPos2[10] = <<-2445.696045,2896.657471,37.680088 + iUpperZ>> 
	fWidth[10] = 9.5

	vPos1[11] = <<-2020.542725,2810.746826,29.456579>>
	vPos2[11] = <<-1927.948975,2785.778809,37.670570 + iUpperZ>> 
	fWidth[11] = 9.5

	vPos1[12] = <<-2261.899170,3376.548828,29.778589>>
	vPos2[12] = <<-2141.067383,3376.772949,37.900187 + iUpperZ>> 
	fWidth[12] = 9.5


	numberOfSubAreas = 13
	
	REPEAT numberOfSubAreas i
		IF IS_POINT_IN_ANGLED_AREA(vInput, vPos1[i], vPos2[i], fWidth[i], bHighlight)
			//#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "IS_COORD_IN_SPECIFIED_AREA: Passed point << ", vInput.x, ",", vInput.y, ",", vInput.z, " >> found in volume ", i, " of ", numberOfSubAreas, " for army base") ENDIF #ENDIF
			RETURN TRUE
		ENDIF

	ENDREPEAT
	
	//#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "IS_COORD_IN_SPECIFIED_AREA: Passed point << ", vInput.x, ",", vInput.y, ",", vInput.z, " >> not in area check for army base ") ENDIF #ENDIF
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_NPC_IN_VEHICLE()
	INT iSeat
	
	VEHICLE_INDEX theVeh
	
	PED_INDEX pedIndex
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF IS_VEHICLE_DRIVEABLE(theVeh)
			FOR iSeat = ENUM_TO_INT(VS_DRIVER) TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
				IF NOT IS_VEHICLE_SEAT_FREE(theVeh, INT_TO_ENUM(VEHICLE_SEAT, iSeat), TRUE)
					pedIndex = GET_PED_IN_VEHICLE_SEAT(theVeh, INT_TO_ENUM(VEHICLE_SEAT, iSeat), TRUE)
					
					IF DOES_ENTITY_EXIST(pedIndex)
						IF NOT IS_PED_A_PLAYER(pedIndex)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_ARMORY_TRUCK_OUTSIDE(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_ARMORY_TRUCK(PlayerID)
		PLAYER_INDEX OwnerID = globalPlayerBD[NATIVE_TO_INT(PlayerID)].SimpleInteriorBD.propertyOwner
		
		IF OwnerID != INVALID_PLAYER_INDEX()
			IF NOT IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(OwnerID)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_CREATOR_TRAILER(PlayerID)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_ARMORY_AIRCRAFT_OUTSIDE(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PlayerID)
		PLAYER_INDEX OwnerID = globalPlayerBD[NATIVE_TO_INT(PlayerID)].SimpleInteriorBD.propertyOwner
		
		IF OwnerID != INVALID_PLAYER_INDEX()
			IF NOT IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(OwnerID)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerID)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_HACKER_TRUCK_OUTSIDE(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_HACKER_TRUCK(PlayerID)
		PLAYER_INDEX OwnerID = globalPlayerBD[NATIVE_TO_INT(PlayerID)].SimpleInteriorBD.propertyOwner
		
		IF OwnerID != INVALID_PLAYER_INDEX()
			IF NOT IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(OwnerID)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_CREATOR_TRAILER(PlayerID)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_BE_TARGETTED_BY_ORBITAL_CANNON(PLAYER_INDEX pPlayer, BOOL bIncludeBeast = TRUE, BOOL bIncludeOffRadar = TRUE)
	INT iPlayerIndex = NATIVE_TO_INT(pPlayer)
	IF pPlayer != INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(pPlayer)
		IF (IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(pPlayer)
		AND (GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(pPlayer) = SIMPLE_INTERIOR_ARMORY_AIRCRAFT_1
		OR GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(pPlayer) = SIMPLE_INTERIOR_ARMORY_TRUCK_1
		 OR GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(pPlayer) = SIMPLE_INTERIOR_HACKER_TRUCK
		 #IF FEATURE_HEIST_ISLAND
		 OR GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(pPlayer) = SIMPLE_INTERIOR_SUBMARINE
		 #ENDIF
		 ))
		OR (IS_PLAYER_IN_ARMORY_AIRCRAFT_OUTSIDE(pPlayer)
		OR IS_PLAYER_IN_ARMORY_TRUCK_OUTSIDE(pPlayer)
		OR IS_PLAYER_IN_HACKER_TRUCK_OUTSIDE(pPlayer)
		#IF FEATURE_HEIST_ISLAND
		OR IS_PLAYER_IN_SUBMARINE(pPlayer)
		#ENDIF
		)
		AND NOT IS_PLAYER_IN_DEFUNCT_BASE(pPlayer)
		AND NOT IS_PLAYER_IN_BUNKER(pPlayer)
		AND NOT IS_PLAYER_IN_BUSINESS_HUB(pPlayer)
			IF NOT IS_PLAYER_IN_PASSIVE_MODE(pPlayer)
			AND NOT IS_THIS_PLAYER_IN_CORONA(pPlayer)
			AND NOT (GB_IS_PLAYER_MEMBER_OF_THIS_GANG(pPlayer, GB_GET_LOCAL_PLAYER_GANG_BOSS()))
			AND NOT IS_PLAYER_SPECTATING(pPlayer)
				IF bIncludeBeast
				AND IS_PLAYER_BEAST(pPlayer)
					RETURN FALSE
				ENDIF
				
				IF bIncludeOffRadar
				AND (GlobalplayerBD[iPlayerIndex].bOffTheRadar
				OR GlobalplayerBD[iPlayerIndex].bOffTheRadarPlayerInSameCar
				OR GlobalplayerBD[iPlayerIndex].bOffTheRadarGangMode
				OR IS_ENTITY_A_GHOST(GET_PLAYER_PED(pPlayer)))
					RETURN FALSE
				ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF pPlayer != INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(pPlayer)
	AND NOT IS_PLAYER_IN_PROPERTY(pPlayer, TRUE, TRUE)
	AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(pPlayer)
	AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(pPlayer)
	AND NOT IS_PLAYER_IN_PASSIVE_MODE(pPlayer)
	AND NOT IS_THIS_PLAYER_IN_CORONA(pPlayer)
	AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(pPlayer)
	AND NOT IS_PLAYER_IN_CUTSCENE(pPlayer)
	AND NOT IS_PLAYER_ON_STRAND_MISSION_CUTSCENE(pPlayer)
	AND NOT IS_BIT_SET(GlobalPlayerBD[iPlayerIndex].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_INSIDE_INTERIOR)
	AND NOT IS_BIT_SET(GlobalPlayerBD[iPlayerIndex].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_RUNNING_TRANSACTION)
	AND NOT (GB_IS_PLAYER_MEMBER_OF_THIS_GANG(pPlayer, GB_GET_LOCAL_PLAYER_GANG_BOSS()))
	AND NOT IS_PLAYER_SPECTATING(pPlayer)
	AND NOT IS_PLAYER_SCTV(pPlayer)
	AND NOT IS_BIT_SET(GlobalplayerBD_FM_3[iPlayerIndex].iFmAmbientEventBitSet, ciSET_PLAYER_IS_PENNED_IN_SPECTATOR)
	AND NOT IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR(pPlayer)
	AND NOT GB_IS_THIS_PLAYER_USING_SPECTATE(pPlayer)
	AND (GET_MP_MISSION_PLAYER_IS_ON(PLAYER_ID()) = eNULL_MISSION OR NOT IS_PLAYER_ON_TUTORIAL(pPlayer))
	AND IS_BIT_SET(GlobalplayerBD_FM[iPlayerIndex].iFmTutProgBitset, biTrigTut_CompletedInitalAmbientTut)
	AND NOT IS_BIT_SET(GlobalPlayerBD[iPlayerIndex].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CINEMA)
	AND NOT IS_BIT_SET(GlobalplayerBD[iPlayerIndex].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_VEH_STORAGE_WARN_FULL)
	AND FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(pPlayer) != FMMC_TYPE_PENNED_IN
	AND NOT (GB_IS_PLAYER_CRITICAL_BOSS_ON_JOB(pPlayer) AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(pPlayer) = FMMC_TYPE_GB_HUNT_THE_BOSS)
	AND NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerIndex].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bUsingRollerCoaster)
	AND NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerIndex].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bUsingFerrisWheel)//GlobalplayerBD_FM[iPlayerIndex].g_bUsingFerrisWheel
	AND NOT IS_PLAYER_ON_HEIST_ISLAND(pPlayer)
	#IF FEATURE_GEN9_EXCLUSIVE
	AND NOT IS_REMOTE_PLAYER_ON_MP_INTRO(pPlayer)
	#ENDIF
		IF bIncludeBeast
		AND IS_PLAYER_BEAST(pPlayer)
			RETURN FALSE
		ENDIF
		
		IF bIncludeOffRadar
		AND (GlobalplayerBD[iPlayerIndex].bOffTheRadar
		OR GlobalplayerBD[iPlayerIndex].bOffTheRadarPlayerInSameCar
		OR GlobalplayerBD[iPlayerIndex].bOffTheRadarGangMode
		OR IS_ENTITY_A_GHOST(GET_PLAYER_PED(pPlayer)))
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC FIRE_ORBITAL_CANNON(VECTOR vTargetPosition)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		FLOAT fTempZ
		
		VECTOR vNormal
		VECTOR vTempPedPos
		VECTOR vTempVehiclePos
		
		GET_GROUND_Z_FOR_3D_COORD(vTargetPosition, vTargetPosition.z)
		GET_GROUND_Z_AND_NORMAL_FOR_3D_COORD(vTargetPosition, fTempZ, vNormal)
		
		PED_INDEX pedTemp
		PLAYER_INDEX pTempPlayer
		
		INT k
		REPEAT NUM_NETWORK_PLAYERS k
			pTempPlayer = INT_TO_PLAYERINDEX(k)
			
			IF CAN_PLAYER_BE_TARGETTED_BY_ORBITAL_CANNON(pTempPlayer, FALSE, FALSE)
				pedTemp = GET_PLAYER_PED(pTempPlayer)
				
				IF DOES_ENTITY_EXIST(pedTemp)
					vTempPedPos = GET_ENTITY_COORDS(pedTemp, FALSE)
					
					IF GET_DISTANCE_BETWEEN_COORDS(vTargetPosition, vTempPedPos, FALSE) < 15.0
					AND vTempPedPos.z >= (vTargetPosition.z - 15.0)
						ADD_OWNED_EXPLOSION(PLAYER_PED_ID(), vTempPedPos, EXP_TAG_ORBITAL_CANNON, 1.0)
						
						PRINTLN("FIRE_ORBITAL_CANNON - PED EXPLOSION ", k, " X: ", vTempPedPos.x)
						PRINTLN("FIRE_ORBITAL_CANNON - PED EXPLOSION ", k, " Y: ", vTempPedPos.y)
						PRINTLN("FIRE_ORBITAL_CANNON - PED EXPLOSION ", k, " Z: ", vTempPedPos.z)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		INT iNumberOfVehicles = GET_ALL_VEHICLES(g_PoolVehicles)
		
		INT j
		REPEAT iNumberOfVehicles j
			IF DOES_ENTITY_EXIST(g_PoolVehicles[j])
				IF IS_VEHICLE_A_PERSONAL_VEHICLE(g_PoolVehicles[j], FALSE)
				OR IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(g_PoolVehicles[j], FALSE)
				OR IS_VEHICLE_A_PERSONAL_TRUCK(g_PoolVehicles[j], FALSE)
				OR IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(g_PoolVehicles[j], FALSE)
					vTempVehiclePos = GET_ENTITY_COORDS(g_PoolVehicles[j], FALSE)
					
					IF GET_DISTANCE_BETWEEN_COORDS(vTargetPosition, vTempVehiclePos, FALSE) < 15.0
					AND vTempVehiclePos.z >= (vTargetPosition.z - 15.0)
						ADD_OWNED_EXPLOSION(PLAYER_PED_ID(), vTempVehiclePos, EXP_TAG_ORBITAL_CANNON, 1.0)
						
						PRINTLN("FIRE_ORBITAL_CANNON - VEHICLE EXPLOSION ", j, " X: ", vTempVehiclePos.x)
						PRINTLN("FIRE_ORBITAL_CANNON - VEHICLE EXPLOSION ", j, " Y: ", vTempVehiclePos.y)
						PRINTLN("FIRE_ORBITAL_CANNON - VEHICLE EXPLOSION ", j, " Z: ", vTempVehiclePos.z)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		ADD_OWNED_EXPLOSION(PLAYER_PED_ID(), vTargetPosition, EXP_TAG_ORBITAL_CANNON, 1.0)
		
		PRINTLN("FIRE_ORBITAL_CANNON - NORMAL X: ", vNormal.x)
		PRINTLN("FIRE_ORBITAL_CANNON - NORMAL Y: ", vNormal.y)
		PRINTLN("FIRE_ORBITAL_CANNON - NORMAL Z: ", vNormal.z)
		
		USE_PARTICLE_FX_ASSET("scr_xm_orbital")
		START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_xm_orbital_blast", vTargetPosition, vNormal, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		
		IF NOT g_bDisableOrbitalCannonSound
			PLAY_SOUND_FROM_COORD(-1, "DLC_XM_Explosions_Orbital_Cannon", vTargetPosition, DEFAULT, TRUE)
		ENDIF
		
		PRINTLN("FIRE_ORBITAL_CANNON - MAIN EXPLOSION X: ", vTargetPosition.x)
		PRINTLN("FIRE_ORBITAL_CANNON - MAIN EXPLOSION Y: ", vTargetPosition.y)
		PRINTLN("FIRE_ORBITAL_CANNON - MAIN EXPLOSION Z: ", vTargetPosition.z)
	ENDIF
ENDPROC

PROC MAINTAIN_ORBITAL_CANNON_PRE()
	IF MPGlobals.sFreemodeCache.bNetworkIsActivitySession
		EXIT
	ENDIF
	
	IF IS_BIT_SET(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CANNON)
		IF IS_SYSTEM_UI_BEING_DISPLAYED()
		OR IS_PAUSE_MENU_ACTIVE_EX()
		OR (IS_WARNING_MESSAGE_ACTIVE() AND GET_WARNING_SCREEN_MESSAGE_HASH() != HASH("ORB_CAN_QUIT1"))
			PRINTLN("MAINTAIN_ORBITAL_CANNON_PRE - Disabling Orbital Cannon")
			
			g_iOrbitalCannonRefundBS = 0
			
			g_bDisableOrbitalCannon = TRUE
		ENDIF
	ELSE
		IF g_bDisableOrbitalCannon
			PRINTLN("MAINTAIN_ORBITAL_CANNON_PRE - Enabling Orbital Cannon")
			
			g_bDisableOrbitalCannon = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_ORBITAL_CANNON_ACTIVE_PLAYER_LOOP(INT &iOrbitalCannonBS, INT iPlayer, PLAYER_INDEX playerID)
	IF MPGlobals.sFreemodeCache.bNetworkIsActivitySession
		EXIT
	ENDIF
	
	IF playerID != PLAYER_ID()
		IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerID, GB_GET_LOCAL_PLAYER_GANG_BOSS())
		OR GB_IS_PLAYER_ALLIED_WITH_THIS_PLAYER(playerID, PLAYER_ID())
			IF NOT IS_BIT_SET(iOrbitalCannonBS, iPlayer)
				IF IS_BIT_SET(GlobalPlayerBD[NATIVE_TO_INT(playerID)].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CANNON)
					IF NOT GB_IS_BOSS_FF_TOGGLED_ON()
						SET_PLAYER_CAN_DAMAGE_PLAYER(PLAYER_ID(), playerID, TRUE)
						SET_PLAYER_CAN_DAMAGE_PLAYER(playerID, PLAYER_ID(), TRUE)
					ENDIF
					
					SET_BIT(iOrbitalCannonBS, iPlayer)
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(GlobalPlayerBD[NATIVE_TO_INT(playerID)].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CANNON)
					IF NOT GB_IS_BOSS_FF_TOGGLED_ON()
						SET_PLAYER_CAN_DAMAGE_PLAYER(PLAYER_ID(), playerID, FALSE)
						SET_PLAYER_CAN_DAMAGE_PLAYER(playerID, PLAYER_ID(), FALSE)
					ENDIF
					
					CLEAR_BIT(iOrbitalCannonBS, iPlayer)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iOrbitalCannonBS, iPlayer)
			CLEAR_BIT(iOrbitalCannonBS, iPlayer)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_ORBITAL_CANNON_INACTIVE_PLAYER_LOOP(INT &iOrbitalCannonBS, INT iPlayer)
	IF MPGlobals.sFreemodeCache.bNetworkIsActivitySession
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iOrbitalCannonBS, iPlayer)
		CLEAR_BIT(iOrbitalCannonBS, iPlayer)
	ENDIF
ENDPROC

PROC MAINTAIN_ORBITAL_CANNON_POST()
	IF MPGlobals.sFreemodeCache.bNetworkIsActivitySession
	OR (NOT g_bDelayHeistEventItemFeed
	AND NOT IS_BIT_SET(g_iOrbitalCannonRefundBS, ciH2_ORBITAL_CANNON_AUTOMATIC_PURCHASED)
	AND NOT IS_BIT_SET(g_iOrbitalCannonRefundBS, ciH2_ORBITAL_CANNON_MANUAL_PURCHASED))
		EXIT
	ENDIF
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_SKYSWOOP_AT_GROUND()
	AND GET_CURRENT_GAMEMODE() = GAMEMODE_FM
	AND NOT IS_PLAYER_IN_CORONA()
		#IF FEATURE_HEIST_ISLAND
		IF g_bDelayHeistEventItemFeed
		AND NOT NETWORK_IS_IN_MP_CUTSCENE()
		AND NOT NETWORK_IS_ACTIVITY_SESSION()
			PRINTLN("GIVE_LOCAL_PLAYER_ISLAND_HEIST_EVENT_ITEM_REWARD - Displaying reward ticker for item: ", GET_EVENT_ITEM_TITLE(GET_HASH_FOR_ISLAND_HEIST_EVENT_ITEM(STEAL_PRIMARY_TARGET_AND_COMPLETE_THE_FINALE)))
			
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO, GET_EVENT_ITEM_TITLE(GET_HASH_FOR_ISLAND_HEIST_EVENT_ITEM(STEAL_PRIMARY_TARGET_AND_COMPLETE_THE_FINALE)), "UNLOCK_DESC_SHIRT3", "FeedhitTshirt04", "MPTshirtAwards3", DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
			
			g_bDelayHeistEventItemFeed = FALSE
		ENDIF
		#ENDIF
		
		IF IS_BIT_SET(g_iOrbitalCannonRefundBS, ciH2_ORBITAL_CANNON_AUTOMATIC_PURCHASED)
			IF NOT IS_BIT_SET(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CANNON)
				INT iRefundPrice = g_sMPTunables.IH2_ORBITAL_CANNON_AUTOMATIC_PRICE
				
				IF g_sMPTunables.bOrbitalCannonAutomaticSale
					iRefundPrice = g_sMPTunables.IH2_ORBITAL_CANNON_AUTOMATIC_SALE_PRICE
				ENDIF
				
				IF USE_SERVER_TRANSACTIONS()
					NETWORK_REFUND_CASH_TYPE(iRefundPrice, MP_REFUND_TYPE_ORBITAL_AUTO, MP_REFUND_REASON_NOT_USED, TRUE)
				ELSE
					NETWORK_EARN_TARGET_REFUND(iRefundPrice, 1)
				ENDIF
				
				PRINTLN("[FIX_FOR_4263403] - Refunding Orbital Cannon - Automatic")
				
				g_iOrbitalCannonRefundBS = 0
			ENDIF
		ELIF IS_BIT_SET(g_iOrbitalCannonRefundBS, ciH2_ORBITAL_CANNON_MANUAL_PURCHASED)
			IF NOT IS_BIT_SET(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CANNON)
				INT iRefundPrice = g_sMPTunables.IH2_ORBITAL_CANNON_MANUAL_PRICE
				
				IF g_sMPTunables.bOrbitalCannonManualSale
					iRefundPrice = g_sMPTunables.IH2_ORBITAL_CANNON_MANUAL_SALE_PRICE
				ENDIF
				
				IF USE_SERVER_TRANSACTIONS()
					NETWORK_REFUND_CASH_TYPE(iRefundPrice, MP_REFUND_TYPE_ORBITAL_MANUAL, MP_REFUND_REASON_NOT_USED, TRUE)
				ELSE
					NETWORK_EARN_TARGET_REFUND(iRefundPrice, 0)
				ENDIF
				
				PRINTLN("[FIX_FOR_4263403] - Refunding Orbital Cannon - Manual")
				
				g_iOrbitalCannonRefundBS = 0
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PLAYER_ACTION_COOLDOWN()
	IF HAS_NET_TIMER_STARTED(g_sPlayerActionCooldown)
		IF HAS_NET_TIMER_EXPIRED(g_sPlayerActionCooldown, 30000)
			RESET_NET_TIMER(g_sPlayerActionCooldown)
		ENDIF
	ENDIF
ENDPROC

PROC DISABLE_TELESCOPES(BOOL bDisable = TRUE)
	PRINTLN("DISABLE_TELESCOPES - g_bDisableTelescope = ", bDisable)
	
	g_bDisableTelescope = bDisable
ENDPROC

FUNC BOOL ARE_TELESCOPES_DISABLED()
	RETURN g_bDisableTelescope
ENDFUNC

#IF IS_DEBUG_BUILD
PROC MAINTAIN_DEBUG_ARENA_WARP()
	SWITCH g_iArenaWarpState
		CASE 0
			IF IS_BIT_SET(g_iArenaWarpBS, 0)
			OR IS_BIT_SET(g_iArenaWarpBS, 1)
			OR IS_BIT_SET(g_iArenaWarpBS, 2)
			OR IS_BIT_SET(g_iArenaWarpBS, 6)
				g_iArenaWarpState++
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_SCREEN_FADED_OUT()
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(500)
				ENDIF
			ELSE
				IF IS_BIT_SET(g_iArenaWarpBS, 3)
					REMOVE_IPL("xs_arena_interior")
					UNPIN_INTERIOR(g_iArenaInteriorIndex)
					CLEAR_BIT(g_iArenaWarpBS, 3)
				ENDIF
				
				IF IS_BIT_SET(g_iArenaWarpBS, 4)
					REMOVE_IPL("xs_arena_interior_2")
					UNPIN_INTERIOR(g_iArenaInteriorIndex)
					CLEAR_BIT(g_iArenaWarpBS, 4)
				ENDIF
				
				IF IS_BIT_SET(g_iArenaWarpBS, 5)
					REMOVE_IPL("xs_arena_interior_3")
					UNPIN_INTERIOR(g_iArenaInteriorIndex)
					CLEAR_BIT(g_iArenaWarpBS, 5)
				ENDIF
				
				g_iArenaWarpState++
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_BIT_SET(g_iArenaWarpBS, 6)
				IF IS_BIT_SET(g_iArenaWarpBS, 0)
					REQUEST_IPL("xs_arena_interior")
					g_iArenaInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<2800.0, -3800.0, 100.0>>, "xs_x18_int_01")
				ELIF IS_BIT_SET(g_iArenaWarpBS, 1)
					REQUEST_IPL("xs_arena_interior_2")
					g_iArenaInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<2800.0, -3800.0, 100.0>>, "xs_x18_int_02")
				ELIF IS_BIT_SET(g_iArenaWarpBS, 2)
					REQUEST_IPL("xs_arena_interior_3")
					g_iArenaInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<2800.0, -3800.0, 100.0>>, "xs_x18_int_03")
				ENDIF
				
				PIN_INTERIOR_IN_MEMORY(g_iArenaInteriorIndex)
				CAP_INTERIOR(g_iArenaInteriorIndex, FALSE)
				DISABLE_INTERIOR(g_iArenaInteriorIndex, FALSE)
			ENDIF
			
			g_iArenaWarpState++
		BREAK
		
		CASE 3
			IF NOT IS_BIT_SET(g_iArenaWarpBS, 6)
				NEW_LOAD_SCENE_START_SPHERE(<<2800.0, -3800.0, 100.0>>, 300.0)
			ELSE
				NEW_LOAD_SCENE_START_SPHERE(<<-382.2601, -1862.5212, 19.6631>>, 300.0)
			ENDIF
			
			g_iArenaWarpState++
		BREAK
		
		CASE 4
			IF IS_NEW_LOAD_SCENE_LOADED()
				NEW_LOAD_SCENE_STOP()
				
				IF NOT IS_BIT_SET(g_iArenaWarpBS, 6)
					IF IS_INTERIOR_READY(g_iArenaInteriorIndex)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2799.9829, -3896.1223, 139.0100>>, FALSE)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 0.0)
							
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH()
							
							g_iArenaWarpState++
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-382.2601, -1862.5212, 19.6631>>, FALSE)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 197.6424)
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						
						g_iArenaWarpState++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			DO_SCREEN_FADE_IN(500)
			
			IF IS_BIT_SET(g_iArenaWarpBS, 6)
				g_iArenaWarpBS = 0
			ELSE
				IF IS_BIT_SET(g_iArenaWarpBS, 0)
					CLEAR_BIT(g_iArenaWarpBS, 0)
					SET_BIT(g_iArenaWarpBS, 3)
				ELIF IS_BIT_SET(g_iArenaWarpBS, 1)
					CLEAR_BIT(g_iArenaWarpBS, 1)
					SET_BIT(g_iArenaWarpBS, 4)
				ELIF IS_BIT_SET(g_iArenaWarpBS, 2)
					CLEAR_BIT(g_iArenaWarpBS, 2)
					SET_BIT(g_iArenaWarpBS, 5)
				ENDIF
			ENDIF
			
			g_iArenaWarpState = 0
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF

PROC SET_CUSTOMISE_APARTMENT_BLOCKED(BOOL bBlocked)
	IF bBlocked
		IF NOT g_bBlockedCustomiseApartment
			DEBUG_PRINTCALLSTACK()
			PRINTLN("SET_CUSTOMISE_APARTMENT_BLOCKED: g_bBlockedCustomiseApartment = TRUE")
			g_bBlockedCustomiseApartment = TRUE
		ENDIF
	ELSE
		IF g_bBlockedCustomiseApartment
			DEBUG_PRINTCALLSTACK()
			PRINTLN("SET_CUSTOMISE_APARTMENT_BLOCKED: g_bBlockedCustomiseApartment = FALSE")
			g_bBlockedCustomiseApartment = FALSE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_CUSTOMISE_APARTMENT_BLOCKED()
	RETURN g_bBlockedCustomiseApartment
ENDFUNC

#IF FEATURE_CASINO_HEIST
PROC RESET_ARCADE_FORTUNES()
	PRINTLN("[ARCADE_FORTUNES] - RESET_ARCADE_FORTUNES")
	
	g_iArcadeFortuneBS = 0
	
	RESET_NET_TIMER(g_stArcadeFortuneTimer)
	RESET_NET_TIMER(g_stArcadeFortuneCooldownTimer)
	
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_TRUE_LOVE_FORTUNE)
	ENDIF
	
	IF IS_INCIDENT_VALID(g_iFortuneIncident)
		DELETE_INCIDENT(g_iFortuneIncident)
	ENDIF
ENDPROC

PROC MAINTAIN_ARCADE_FORTUNES()
	IF NOT IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_SPECIAL_FORTUNE_ACTIVE)
		IF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_WEATHER)
			PRINTLN("[ARCADE_FORTUNES] - ARCADE_FORTUNE_GLOBAL_BS_WEATHER")
			
			REINIT_NET_TIMER(g_stArcadeFortuneCooldownTimer)
			
			SET_BIT(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_SPECIAL_FORTUNE_ACTIVE)
		ELIF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_FEDS)
			PRINTLN("[ARCADE_FORTUNES] - ARCADE_FORTUNE_GLOBAL_BS_FEDS")
			
			REINIT_NET_TIMER(g_stArcadeFortuneTimer)
			REINIT_NET_TIMER(g_stArcadeFortuneCooldownTimer)
			
			SET_BIT(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_SPECIAL_FORTUNE_ACTIVE)
		ELIF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_TRUE_LOVE)
			PRINTLN("[ARCADE_FORTUNES] - ARCADE_FORTUNE_GLOBAL_BS_TRUE_LOVE")
			
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_TRUE_LOVE_FORTUNE)
			
			REINIT_NET_TIMER(g_stArcadeFortuneTimer)
			REINIT_NET_TIMER(g_stArcadeFortuneCooldownTimer)
			
			SET_BIT(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_SPECIAL_FORTUNE_ACTIVE)
		ELIF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_BOUNTY)
			PRINTLN("[ARCADE_FORTUNES] - ARCADE_FORTUNE_GLOBAL_BS_BOUNTY")
			
			REINIT_NET_TIMER(g_stArcadeFortuneCooldownTimer)
			
			SET_BIT(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_SPECIAL_FORTUNE_ACTIVE)
		ELIF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_MERRY_WEATHER)
			PRINTLN("[ARCADE_FORTUNES] - ARCADE_FORTUNE_GLOBAL_BS_MERRY_WEATHER")
			
			REINIT_NET_TIMER(g_stArcadeFortuneTimer)
			REINIT_NET_TIMER(g_stArcadeFortuneCooldownTimer)
			
			SET_BIT(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_SPECIAL_FORTUNE_ACTIVE)
		ELIF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_MUGGER)
			PRINTLN("[ARCADE_FORTUNES] - ARCADE_FORTUNE_GLOBAL_BS_MUGGER")
			
			REINIT_NET_TIMER(g_stArcadeFortuneTimer)
			REINIT_NET_TIMER(g_stArcadeFortuneCooldownTimer)
			
			SET_BIT(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_SPECIAL_FORTUNE_ACTIVE)
		ELIF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_AIRSTRIKE)
			PRINTLN("[ARCADE_FORTUNES] - ARCADE_FORTUNE_GLOBAL_BS_AIRSTRIKE")
			
			REINIT_NET_TIMER(g_stArcadeFortuneTimer)
			REINIT_NET_TIMER(g_stArcadeFortuneCooldownTimer)
			
			SET_BIT(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_SPECIAL_FORTUNE_ACTIVE)
		ENDIF
	ELSE
		IF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_WEATHER)
			IF HAS_NET_TIMER_EXPIRED(g_stArcadeFortuneCooldownTimer, 2880000)
				PRINTLN("[ARCADE_FORTUNES] - WEATHER - RESET_ARCADE_FORTUNES")
				
				RESET_ARCADE_FORTUNES()
			ENDIF
		ELIF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_FEDS)
			IF HAS_NET_TIMER_EXPIRED(g_stArcadeFortuneCooldownTimer, 2880000)
				PRINTLN("[ARCADE_FORTUNES] - FEDS - RESET_ARCADE_FORTUNES")
				
				RESET_ARCADE_FORTUNES()
			ELSE
				IF NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
				AND NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
				AND NOT NETWORK_IS_ACTIVITY_SESSION()
				AND NOT FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
				AND NOT GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 5
						PRINTLN("[ARCADE_FORTUNES] - FEDS - Setting wanted level")
						
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 5)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					ENDIF
				ENDIF
			ENDIF
		ELIF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_TRUE_LOVE)
			IF HAS_NET_TIMER_EXPIRED(g_stArcadeFortuneCooldownTimer, 2880000)
				PRINTLN("[ARCADE_FORTUNES] - TRUE_LOVE - RESET_ARCADE_FORTUNES")
				
				RESET_ARCADE_FORTUNES()
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(g_stArcadeFortuneTimer, 120000)
				PRINTLN("[ARCADE_FORTUNES] - TRUE_LOVE - Clearing BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_TRUE_LOVE_FORTUNE")
				
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_TRUE_LOVE_FORTUNE)
				
				RESET_NET_TIMER(g_stArcadeFortuneTimer)
			ENDIF
		ELIF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_BOUNTY)
			IF HAS_NET_TIMER_EXPIRED(g_stArcadeFortuneCooldownTimer, 2880000)
				PRINTLN("[ARCADE_FORTUNES] - BOUNTY - RESET_ARCADE_FORTUNES")
				
				BROADCAST_SET_BOUNTY_ON_PLAYER(ALL_PLAYERS(), PLAYER_ID(), 10000, 1)
				
				RESET_ARCADE_FORTUNES()
			ENDIF
		ELIF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_MERRY_WEATHER)
			IF HAS_NET_TIMER_EXPIRED(g_stArcadeFortuneCooldownTimer, 2880000)
				PRINTLN("[ARCADE_FORTUNES] - MERRY_WEATHER - RESET_ARCADE_FORTUNES")
				
				RESET_ARCADE_FORTUNES()
			ENDIF
			
			IF NOT IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_MERRY_WEATHER_SENT)
				IF HAS_NET_TIMER_EXPIRED(g_stArcadeFortuneTimer, 1440000)
				AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
				AND NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
				AND NOT NETWORK_IS_ACTIVITY_SESSION()
				AND NOT FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
				AND NOT GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
					PRINTLN("[ARCADE_FORTUNES] - MERRY_WEATHER - Sending mercs")
					
					IF CREATE_INCIDENT_WITH_ENTITY(DT_ARMY_VEHICLE, GET_PLAYER_PED(PLAYER_ID()), 4, 0.0, g_iFortuneIncident)
						REINIT_NET_TIMER(g_stArcadeFortuneTimer)
						
						SET_BIT(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_MERRY_WEATHER_SENT)
					ENDIF
				ENDIF
			ELSE
				IF IS_PLAYER_DEAD(PLAYER_ID())
					DELETE_INCIDENT(g_iFortuneIncident)
					
					CLEAR_BIT(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_MERRY_WEATHER_SENT)
					
					RESET_NET_TIMER(g_stArcadeFortuneTimer)
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(g_stArcadeFortuneTimer, 600000)
					DELETE_INCIDENT(g_iFortuneIncident)
					
					CLEAR_BIT(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_MERRY_WEATHER_SENT)
					
					RESET_NET_TIMER(g_stArcadeFortuneTimer)
				ENDIF
			ENDIF
		ELIF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_MUGGER)
			IF HAS_NET_TIMER_EXPIRED(g_stArcadeFortuneCooldownTimer, 2880000)
				PRINTLN("[ARCADE_FORTUNES] - MUGGER - RESET_ARCADE_FORTUNES")
				
				RESET_ARCADE_FORTUNES()
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(g_stArcadeFortuneTimer, 1440000)
			AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
			AND NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
			AND NOT NETWORK_IS_ACTIVITY_SESSION()
			AND NOT FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
			AND NOT GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
				PRINTLN("[ARCADE_FORTUNES] - MUGGER - Sending mugger")
				
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iGangCallOwnerBitset, GANG_CALL_TYPE_SPECIAL_THIEF)
				
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iGangCallTargetID = NATIVE_TO_INT(PLAYER_ID())
				
				RESET_NET_TIMER(g_stArcadeFortuneTimer)
			ENDIF
		ELIF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_AIRSTRIKE)
			IF HAS_NET_TIMER_EXPIRED(g_stArcadeFortuneCooldownTimer, 2880000)
				PRINTLN("[ARCADE_FORTUNES] - MUGGER - RESET_ARCADE_FORTUNES")
				
				RESET_ARCADE_FORTUNES()
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(g_stArcadeFortuneTimer, 1440000)
			AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
			AND NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
			AND NOT NETWORK_IS_ACTIVITY_SESSION()
			AND NOT FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
			AND NOT GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
			AND IS_PED_ON_FOOT(PLAYER_PED_ID())
				PRINTLN("[ARCADE_FORTUNES] - MUGGER - Sending airstrike")
				
				MPGlobalsAmbience.bLaunchAirstrike = TRUE
				
				RESET_NET_TIMER(g_stArcadeFortuneTimer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#ENDIF

#IF FEATURE_HEIST_ISLAND

/// PURPOSE:

/// PURPOSE:
///    Sets if the player is on the heist island
PROC _SET_PLAYER_IS_ON_HEIST_ISLAND(PLAYER_INDEX piPlayer, BOOL bOnIsland)
	INT iPlayerID = NATIVE_TO_INT(piPlayer)
	ENABLE_BIT_ENUM(GlobalplayerBD[iPlayerID].iHeistIslandTravelBS, HITBS__ON_ISLAND, bOnIsland)
	PRINTLN("[HEIST_ISLAND] _SET_PLAYER_IS_ON_HEIST_ISLAND Player: ", iPlayerID, " on island: ", bOnIsland)
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Sets if the local player is on the heist island
PROC _SET_LOCAL_PLAYER_IS_ON_HEIST_ISLAND(BOOL bOnIsland)
	_SET_PLAYER_IS_ON_HEIST_ISLAND(PLAYER_ID(), bOnIsland)
ENDPROC

/// PURPOSE:
///    Sets if the player is at the heist island beach party and not free roaming around the island
/// PARAMS:
///    piPlayer - the player to check
///    bAtParty - if the player is at the party or not
PROC SET_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY(PLAYER_INDEX piPlayer, BOOL bAtParty)
	INT iPlayerID = NATIVE_TO_INT(piPlayer)
	ENABLE_BIT_ENUM(GlobalplayerBD[iPlayerID].iHeistIslandTravelBS, HITBS__AT_BEACH_PARTY, bAtParty)
	PRINTLN("[HEIST_ISLAND] SET_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY Player: ", iPlayerID, " at party: ", bAtParty)
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Sets if the local player is at the heist island beach party and not free roaming around the island
/// PARAMS:
///    bAtParty - if the player is at the party or not
PROC SET_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY(BOOL bAtParty)
	SET_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY(PLAYER_ID(), bAtParty)
ENDPROC

/// PURPOSE:
///    Checks if the player is at the beach party
/// PARAMS:
///    piPlayer - the player to check
/// RETURNS:
///    TRUE if the player is at the beach party
FUNC BOOL IS_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY(PLAYER_INDEX piPlayer)
	INT iPlayerID = NATIVE_TO_INT(piPlayer)
	RETURN IS_BIT_SET_ENUM(GlobalplayerBD[iPlayerID].iHeistIslandTravelBS, HITBS__AT_BEACH_PARTY)
ENDFUNC

/// PURPOSE:
///    Checks if the local player is at the beach party, players at the party are restricted to the
///    party area
/// RETURNS:
///    TRUE if the local player is at the beach party 
FUNC BOOL IS_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY()
	RETURN IS_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY(PLAYER_ID())
ENDFUNC

/// PURPOSE:
///    Sets if the beach party is active, if it is active it will load and unload based on range
/// PARAMS:
///    bActive - if active
PROC SET_BEACH_PARTY_ACTIVE(BOOL bActive)
	ENABLE_BIT_ENUM(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHeistIslandTravelBS, HITBS__IS_BEACH_PARTY_DISABLED, NOT bActive)
	PRINTLN("[HEIST_ISLAND] SET_BEACH_PARTY_ACTIVE Player: ", NATIVE_TO_INT(PLAYER_ID()), " party active: ", bActive)
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Checks if the beach party is active
/// RETURNS:
///    TRUE if the beach party is active
FUNC BOOL IS_BEACH_PARTY_ACTIVE()
	RETURN NOT IS_BIT_SET_ENUM(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHeistIslandTravelBS, HITBS__IS_BEACH_PARTY_DISABLED)
ENDFUNC

/// PURPOSE:
///	   Sets if the player is restricted to the heist island beach party, this will prevent them leaving the area
///    during freeroam
/// PARAMS:
///    piPlayer - the player to check
///    bRestricted - if the player is restricted to the party or not
PROC SET_PLAYER_IS_RESTRICTED_TO_HEIST_ISLAND_BEACH_PARTY(PLAYER_INDEX piPlayer, BOOL bRestricted)
	INT iPlayerID = NATIVE_TO_INT(piPlayer)
	ENABLE_BIT_ENUM(GlobalplayerBD[iPlayerID].iHeistIslandTravelBS, HITBS__RESTRICTED_TO_BEACH_PARTY, bRestricted)
	PRINTLN("[HEIST_ISLAND] SET_PLAYER_IS_RESTRICTED_TO_HEIST_ISLAND_BEACH_PARTY Player: ", iPlayerID, " restricted: ", bRestricted)
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Sets if the local player is restricted to the heist island beach party, this will prevent them leaving the area
///    during freeroam
/// PARAMS:
///    bRestricted - if the player is at the party or not
PROC SET_LOCAL_PLAYER_IS_RESTRICTED_TO_HEIST_ISLAND_BEACH_PARTY(BOOL bRestricted)
	SET_PLAYER_IS_RESTRICTED_TO_HEIST_ISLAND_BEACH_PARTY(PLAYER_ID(), bRestricted)
ENDPROC

/// PURPOSE:
///    Checks if the player is restricted to the beach party area
/// PARAMS:
///    piPlayer - the player to check
/// RETURNS:
///    TRUE if the player is restricted to the beach party
FUNC BOOL IS_PLAYER_RESTRICTED_TO_HEIST_ISLAND_BEACH_PARTY(PLAYER_INDEX piPlayer)
	INT iPlayerID = NATIVE_TO_INT(piPlayer)
	RETURN IS_BIT_SET_ENUM(GlobalplayerBD[iPlayerID].iHeistIslandTravelBS, HITBS__RESTRICTED_TO_BEACH_PARTY)
ENDFUNC

/// PURPOSE:
///    Checks if the local player is restricted to the beach party area
/// RETURNS:
///    TRUE if the local player is restricted to the beach party 
FUNC BOOL IS_LOCAL_PLAYER_RESTRICTED_TO_HEIST_ISLAND_BEACH_PARTY()
	RETURN IS_PLAYER_RESTRICTED_TO_HEIST_ISLAND_BEACH_PARTY(PLAYER_ID())
ENDFUNC

ENUM HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS
	HIBPRA_SEA
	,HIBPRA_LEFT_GATE
	,HIBPRA_RIGHT_GATE
	,HIBPRA_OUTSIDE_PARTY_RADIUS
	,HIBPRA_COUNT
ENDENUM

DEBUGONLY FUNC STRING DEBUG_GET_HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS_AS_STRING(HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS eEnum)
	SWITCH eEnum
		CASE HIBPRA_SEA						RETURN	"HIBPRA_SEA"
		CASE HIBPRA_LEFT_GATE				RETURN	"HIBPRA_LEFT_GATE"
		CASE HIBPRA_RIGHT_GATE				RETURN	"HIBPRA_RIGHT_GATE"
		CASE HIBPRA_OUTSIDE_PARTY_RADIUS	RETURN	"HIBPRA_OUTSIDE_PARTY_RADIUS"
		CASE HIBPRA_COUNT					RETURN	"HIBPRA_COUNT"
	ENDSWITCH

	ASSERTLN("DEBUG_GET_HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS_AS_STRING - Missing name from lookup: ", ENUM_TO_INT(eEnum))
	RETURN ""
ENDFUNC

PROC ENABLE_BEACH_PARTY_RESTRICTED_AREA_FOR_PLAYER(PLAYER_INDEX piPlayer, HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS eRestictedArea, BOOL bEnabled)
	INT iPlayerID = NATIVE_TO_INT(piPlayer)
	ENABLE_BIT_ENUM(GlobalplayerBD[iPlayerID].iIsIslandRestrictedAreaBlocked, eRestictedArea, NOT bEnabled)
	PRINTLN("[HEIST_ISLAND] ENABLE_BEACH_PARTY_RESTRICTED_AREA_FOR_PLAYER - ", DEBUG_GET_HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS_AS_STRING(eRestictedArea), " enabled: ", bEnabled)
	DEBUG_PRINTCALLSTACK()
ENDPROC

PROC ENABLE_BEACH_PARTY_RESTRICTED_AREA_FOR_LOCAL_PLAYER(HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS eRestictedArea, BOOL bEnabled)
	ENABLE_BEACH_PARTY_RESTRICTED_AREA_FOR_PLAYER(PLAYER_ID(), eRestictedArea, bEnabled)
ENDPROC

FUNC BOOL IS_BEACH_PARTY_RESTRICTED_AREA_ENABLED_FOR_PLAYER(PLAYER_INDEX piPlayer, HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS eRestictedArea)
	INT iPlayerID = NATIVE_TO_INT(piPlayer)
	RETURN NOT IS_BIT_SET_ENUM(GlobalplayerBD[iPlayerID].iIsIslandRestrictedAreaBlocked, eRestictedArea)
ENDFUNC

FUNC BOOL IS_BEACH_PARTY_RESTRICTED_AREA_ENABLED_FOR_LOCAL_PLAYER(HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS eRestictedArea)
	RETURN IS_BEACH_PARTY_RESTRICTED_AREA_ENABLED_FOR_PLAYER(PLAYER_ID(), eRestictedArea)
ENDFUNC

FUNC BOOL ARE_ALL_BEACH_PARTY_RESTRICTED_AREAS_ACTIVE_FOR_PLAYER(PLAYER_INDEX piPlayer)
	INT iArea
	INT iMaxArea = ENUM_TO_INT(HIBPRA_COUNT)
	HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS eRestrictedArea
	
	// TODO: change this so we just do a comparision of the int against an int with all the restriciton flags set
	REPEAT iMaxArea iArea
		eRestrictedArea = INT_TO_ENUM(HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS, iArea)
		IF NOT IS_BEACH_PARTY_RESTRICTED_AREA_ENABLED_FOR_PLAYER(piPlayer, eRestrictedArea)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_ALL_BEACH_PARTY_RESTRICTED_AREAS_ACTIVE_FOR_LOCAL_PLAYER()
	RETURN ARE_ALL_BEACH_PARTY_RESTRICTED_AREAS_ACTIVE_FOR_PLAYER(PLAYER_ID())
ENDFUNC

PROC ENABLE_ALL_BEACH_PARTY_RESTRICTED_AREAS_FOR_PLAYER(PLAYER_INDEX piPlayer, BOOl bEnabled)
	INT iArea
	INT iMaxArea = ENUM_TO_INT(HIBPRA_COUNT)
	HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS eRestrictedArea
	
	REPEAT iMaxArea iArea
		eRestrictedArea = INT_TO_ENUM(HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS, iArea)
		ENABLE_BEACH_PARTY_RESTRICTED_AREA_FOR_PLAYER(piPlayer, eRestrictedArea, bEnabled)
	ENDREPEAT
ENDPROC

PROC ENABLE_ALL_BEACH_PARTY_RESTRICTED_AREAS_FOR_LOCAL_PLAYER(BOOl bEnabled)
	ENABLE_ALL_BEACH_PARTY_RESTRICTED_AREAS_FOR_PLAYER(PLAYER_ID(), bEnabled)
ENDPROC

/// PURPOSE:
///    Checks if the given player can be invited to the beach party, this includes
///    check to make sure the local player is on the island and the player to invite is not
///    it also makes sure the invitee is not an animal or RC car
/// PARAMS:
///    playerID - 
/// RETURNS:
///    TRUE if the player can be invited to the island beach party
FUNC BOOL CAN_PLAYER_BE_INVITED_TO_HEIST_ISLAND_BEACH_PARTY(PLAYER_INDEX playerID)
	IF IS_PLAYER_INITIALISING_RCBANDITO(playerID)
	OR IS_PLAYER_USING_RCBANDITO(playerID)
	OR IS_PLAYER_INITIALISING_RC_TANK(playerID)
	OR IS_PLAYER_USING_RC_TANK(PLAYER_ID())
	OR IS_PLAYER_AN_ANIMAL(playerID)
	OR IS_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY(playerID)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC HEIST_ISLAND_TRANSITION_STATE HEIST_ISLAND_TRAVEL__GET_TRANSITION_STATE()
	RETURN g_sHeistIslandTravel.eState
ENDFUNC

FUNC BOOL HEIST_ISLAND_TRAVEL__IS_TRANSITION_IN_PROGRESS()
	RETURN HEIST_ISLAND_TRAVEL__GET_TRANSITION_STATE() = HEIST_ISLAND_TRANSITION_STATE_IN_PROGRESS
ENDFUNC

FUNC BOOL HEIST_ISLAND_TRAVEL__IS_TRANSITION_FINISHED()
	HEIST_ISLAND_TRANSITION_STATE eState = HEIST_ISLAND_TRAVEL__GET_TRANSITION_STATE()
	RETURN eState = HEIST_ISLAND_TRANSITION_STATE_SUCCESS
	OR eState = HEIST_ISLAND_TRANSITION_STATE_FAILED
ENDFUNC

PROC MAINTAIN_SUBMARINE_EXTERIOR_MAP()
	CLEAR_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_STANDING_ON_SUBMARINE)
	
	VEHICLE_INDEX VehicleID[5]
	
	INT iNumVehicles = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), VehicleID)
	
	INT i
	REPEAT iNumVehicles i
		IF DOES_ENTITY_EXIST(VehicleID[i])
		AND NOT IS_ENTITY_DEAD(VehicleID[i])
		AND GET_ENTITY_MODEL(VehicleID[i]) = KOSATKA
		AND IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), VehicleID[i])
			VECTOR vSubCoords = GET_ENTITY_COORDS(VehicleID[i])
			VECTOR vSubRotation = GET_ENTITY_ROTATION(VehicleID[i])
			
			IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
				SET_RADAR_ZOOM_PRECISE(1)
			ELSE
				SET_RADAR_ZOOM_PRECISE(0)
			ENDIF
			
			SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("h4_int_sub_h4"), vSubCoords.x, vSubCoords.y, ROUND(vSubRotation.z), 3)
			
			IF VehicleID[i] = MPGlobalsAmbience.vehSubmarine
				SET_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_STANDING_ON_SUBMARINE)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC SET_ISLAND_HANGAR_NAVMESH_BLOCKING(BOOL bSet)
	PRINTLN("[ISLAND_NAV] - SET_ISLAND_HANGAR_NAVMESH_BLOCKING - bSet = ", bSet)
	
	g_bAddIslandAirStripNavmeshBlocking = bSet
ENDPROC

PROC MAINTAIN_ISLAND_HANGAR_NAVMESH_BLOCKING()
	IF g_bAddIslandAirStripNavmeshBlocking
		IF g_iIslandAirStripNavmeshBlocker1 = -1
			g_iIslandAirStripNavmeshBlocker1 = ADD_NAVMESH_BLOCKING_OBJECT(<<4441.325, -4480.200, 3.825>>, <<8.900, 1.600, 10.950>>, DEG_TO_RAD(20.0))
			
			PRINTLN("[ISLAND_NAV] - MAINTAIN_ISLAND_HANGAR_NAVMESH_BLOCKING - Adding object 1")
		ENDIF
		
		IF g_iIslandAirStripNavmeshBlocker2 = -1
			g_iIslandAirStripNavmeshBlocker2 = ADD_NAVMESH_BLOCKING_OBJECT(<<4451.025, -4476.900, 3.825>>, <<8.900, 1.600, 10.950>>, DEG_TO_RAD(20.0))
			
			PRINTLN("[ISLAND_NAV] - MAINTAIN_ISLAND_HANGAR_NAVMESH_BLOCKING - Adding object 2")
		ENDIF
	ELSE
		IF g_iIslandAirStripNavmeshBlocker1 != -1
			IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(g_iIslandAirStripNavmeshBlocker1)
				REMOVE_NAVMESH_BLOCKING_OBJECT(g_iIslandAirStripNavmeshBlocker1)
			ENDIF
			
			g_iIslandAirStripNavmeshBlocker1 = -1
			
			PRINTLN("[ISLAND_NAV] - MAINTAIN_ISLAND_HANGAR_NAVMESH_BLOCKING - Removing object 1")
		ENDIF
		
		IF g_iIslandAirStripNavmeshBlocker2 != -1
			IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(g_iIslandAirStripNavmeshBlocker2)
				REMOVE_NAVMESH_BLOCKING_OBJECT(g_iIslandAirStripNavmeshBlocker2)
			ENDIF
			
			g_iIslandAirStripNavmeshBlocker2 = -1
			
			PRINTLN("[ISLAND_NAV] - MAINTAIN_ISLAND_HANGAR_NAVMESH_BLOCKING - Removing object 2")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_VEHICLE_MY_MOON_POOL_VEHICLE(VEHICLE_INDEX tempveh, BOOL DoDeadCheck = TRUE)
	IF g_bInMultiplayer
		IF DOES_ENTITY_EXIST(tempveh)
		AND (NOT DoDeadCheck OR IS_VEHICLE_DRIVEABLE(tempveh))
			IF DECOR_EXIST_ON(tempveh, "Player_Moon_Pool")
				IF DECOR_GET_INT(tempveh, "Player_Moon_Pool") = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_USE_SONAR_IN_VEHICLE(VEHICLE_INDEX vehID)
	MODEL_NAMES VehModel = GET_ENTITY_MODEL(vehID)
	IF VehModel = KOSATKA
	AND g_OwnerOfSubmarinePropertyIAmIn = PLAYER_ID()
		RETURN TRUE
	ENDIF
	IF VehModel = AVISA 
		IF NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
			IF IS_VEHICLE_MY_MOON_POOL_VEHICLE(vehID)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	IF VehModel = TOREADOR
		IF NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		AND IS_ENTITY_IN_WATER(vehID)
			IF IS_VEHICLE_MY_PERSONAL_VEHICLE(vehID)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SONAR_SWEEP_WHILE_DRIVING()

	//Default Disable
	BOOL bShowWhileDriving = FALSE
	BOOL bResetHelpText = TRUE
	
	IF IS_PLAYER_SUBMARINE_SONAR_PURCHASED(PLAYER_ID())
		
		PED_INDEX LocalPlayerPed = PLAYER_PED_ID()
		IF DOES_ENTITY_EXIST(LocalPlayerPed)
		AND NOT IS_ENTITY_DEAD(LocalPlayerPed)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				VEHICLE_INDEX Veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		
				//Is vehicle valid?
				IF DOES_ENTITY_EXIST(Veh) AND NOT IS_ENTITY_DEAD(Veh)
					
					//Are we driving it?
					IF GET_PED_IN_VEHICLE_SEAT(Veh) = LocalPlayerPed
						//Does this vehicle support Sonar?
						IF CAN_PLAYER_USE_SONAR_IN_VEHICLE(Veh)
							MODEL_NAMES VehModel = GET_ENTITY_MODEL(Veh)
							IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK) //only disabled in water seems ok trade off
							ENDIF
							bResetHelpText = FALSE
							IF NOT IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet,iNGABI_SONAR_SWEEP_HELP_SHOWN_3)
								IF VehModel != KOSATKA 
									IF NOT IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet,iNGABI_SONAR_SWEEP_HELP_DISPLAYED)
										IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											IF VehModel = AVISA
												IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
													PRINT_HELP("VEH_PC_SONARA")
												ELSE
													PRINT_HELP("VEH_SONARA")
												ENDIF
											ELIF VehModel = TOREADOR
												IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
													PRINT_HELP("VEH_PC_SONARB")
												ELSE
													PRINT_HELP("VEH_SONARB")
												ENDIF
											ENDIF
											
											SET_BIT(MPGlobalsAmbience.iNGAmbBitSet,iNGABI_SONAR_SWEEP_HELP_DISPLAYED)
											IF NOT IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet,iNGABI_SONAR_SWEEP_HELP_SHOWN_1)
												PRINTLN("[MAINTAIN_SONAR_SWEEP_WHILE_DRIVING] printing help to toggle sonar 1")
												SET_BIT(MPGlobalsAmbience.iNGAmbBitSet,iNGABI_SONAR_SWEEP_HELP_SHOWN_1)	
											ELIF NOT IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet,iNGABI_SONAR_SWEEP_HELP_SHOWN_2)
												PRINTLN("[MAINTAIN_SONAR_SWEEP_WHILE_DRIVING] printing help to toggle sonar 2")
												SET_BIT(MPGlobalsAmbience.iNGAmbBitSet,iNGABI_SONAR_SWEEP_HELP_SHOWN_2)
											ELIF NOT IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet,iNGABI_SONAR_SWEEP_HELP_SHOWN_3)
												PRINTLN("[MAINTAIN_SONAR_SWEEP_WHILE_DRIVING] printing help to toggle sonar 3")
												SET_BIT(MPGlobalsAmbience.iNGAmbBitSet,iNGABI_SONAR_SWEEP_HELP_SHOWN_3)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							//Maintain current value while driving
							bShowWhileDriving = IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_SONAR_SWEEP_SHOW_WHILE_DRIVING)
							
							//Toggle on left stick press
							IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
								IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_DUCK)	
								OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_DUCK)
									PRINTLN("[MAINTAIN_SONAR_SWEEP_WHILE_DRIVING] player input in vehicle - toggling iNGABI_SONAR_SWEEP_SHOW_WHILE_DRIVING-1")
									bShowWhileDriving = NOT bShowWhileDriving
								ENDIF
							ELSE
								IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)	
									PRINTLN("[MAINTAIN_SONAR_SWEEP_WHILE_DRIVING] player input in vehicle - toggling iNGABI_SONAR_SWEEP_SHOW_WHILE_DRIVING-3")
									bShowWhileDriving = NOT bShowWhileDriving
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	
	ENDIF
	
	IF bResetHelpText
		IF IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet,iNGABI_SONAR_SWEEP_HELP_DISPLAYED)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEH_SONARA")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEH_SONARB")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEH_PC_SONARA")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEH_PC_SONARB")
				CLEAR_HELP()
				PRINTLN("[MAINTAIN_SONAR_SWEEP_WHILE_DRIVING] clearing vehicle sonar help text")
			ENDIF
			PRINTLN("[MAINTAIN_SONAR_SWEEP_WHILE_DRIVING] resetting iNGABI_SONAR_SWEEP_HELP_DISPLAYED")
			CLEAR_BIT(MPGlobalsAmbience.iNGAmbBitSet,iNGABI_SONAR_SWEEP_HELP_DISPLAYED)
		ENDIF
	ENDIF
	
	
	IF bShowWhileDriving != IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_SONAR_SWEEP_SHOW_WHILE_DRIVING)
		IF bShowWhileDriving
			PRINTLN("[MAINTAIN_SONAR_SWEEP_WHILE_DRIVING] Setting iNGABI_SONAR_SWEEP_SHOW_WHILE_DRIVING")
			SET_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_SONAR_SWEEP_SHOW_WHILE_DRIVING)
		ELSE
			PRINTLN("[MAINTAIN_SONAR_SWEEP_WHILE_DRIVING] Clearing iNGABI_SONAR_SWEEP_SHOW_WHILE_DRIVING")
			CLEAR_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_SONAR_SWEEP_SHOW_WHILE_DRIVING)
		ENDIF
	ENDIF
ENDPROC

//Maintain Sonar behaviour while local player is driving an appropriate vehicle
PROC MAINTAIN_SONAR_SWEEP()
	
	BOOL bShouldShowSonarSweep = FALSE
	
	MAINTAIN_SONAR_SWEEP_WHILE_DRIVING()
	IF IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_SONAR_SWEEP_SHOW_WHILE_DRIVING)
		bShouldShowSonarSweep = TRUE
	ENDIF
	
	IF IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_SONAR_SWEEP_FORCE_SHOW)
		bShouldShowSonarSweep = TRUE
	ENDIF
	
	IF bShouldShowSonarSweep != IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_SONAR_SWEEP_SHOWN)
		IF bShouldShowSonarSweep
			PRINTLN("[MAINTAIN_SONAR_SWEEP] Enabling")
			SET_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_SONAR_SWEEP_SHOWN)
			SET_MINIMAP_SONAR_SWEEP(TRUE)
		ELSE
			PRINTLN("[MAINTAIN_SONAR_SWEEP] Disabling")
			CLEAR_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_SONAR_SWEEP_SHOWN)
			SET_MINIMAP_SONAR_SWEEP(FALSE)
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_SUBMARINE_BEACHED_CHECK()
	
	PLAYER_INDEX LocalPlayerId = PLAYER_ID()
	VEHICLE_INDEX LocalPlayerSubmarine = GET_SUBMARINE_VEHICLE(LocalPlayerId)
	BOOL bIsBeached = FALSE
	
	IF DOES_ENTITY_EXIST(LocalPlayerSubmarine) 
	AND NOT IS_ENTITY_DEAD(LocalPlayerSubmarine)
		IF IS_VEHICLE_STUCK_TIMER_UP(LocalPlayerSubmarine,VEH_STUCK_HUNG_UP,BEACHED_TIME)
		OR IS_VEHICLE_STUCK_TIMER_UP(LocalPlayerSubmarine,VEH_STUCK_JAMMED,BEACHED_TIME)
			bIsBeached = TRUE
		//Maintain beached status if we were made undrivable by beaching
		ELIF IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_LOCAL_PLAYER_SUBMARINE_BEACHED)
		AND NOT IS_VEHICLE_DRIVEABLE(LocalPlayerSubmarine)
			bIsBeached = TRUE
		ENDIF
	ENDIF
	
	IF bIsBeached != IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_LOCAL_PLAYER_SUBMARINE_BEACHED)
		IF bIsBeached
			PRINTLN("[MAINTAIN_SUBMARINE_BEACHED_CHECK] Submarine beached")
			SET_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_LOCAL_PLAYER_SUBMARINE_BEACHED)
			
			PRINT_HELP("HELP_SUB_BEACHED", 10000)
			
			IF MPGlobalsAmbience.bMakeSubmarineUndrivableOnBeached
				SET_VEHICLE_UNDRIVEABLE(LocalPlayerSubmarine, TRUE)
			ENDIF
			
		ELSE
			PRINTLN("[MAINTAIN_SUBMARINE_BEACHED_CHECK] Submarine no longer beached")
			CLEAR_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_LOCAL_PLAYER_SUBMARINE_BEACHED)
		ENDIF
	ENDIF

ENDPROC
	
#ENDIF

PROC SET_HIDE_PV_BLIP(BOOL bSet)
	PRINTLN("[personal_vehicle] SET_HIDE_PV_BLIP - ", bSet)
	
	g_PlayerBlipsData.bHidePersonalVehicleBlip = bSet
	
	IF DOES_BLIP_EXIST(MPGlobals.VehicleData.blipID)
		SET_BLIP_HIDDEN_ON_LEGEND(MPGlobals.VehicleData.blipID, bSet)
	ENDIF
ENDPROC

PROC FLASH_PV_BLIP()
	SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
ENDPROC

FUNC BOOL CAN_REMOTE_PLAYER_START_REMOTE_DRONE_SCRIPT(PLAYER_INDEX playerToCheck)
	IF IS_PLAYER_IN_PROPERTY(playerToCheck, TRUE, TRUE)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(playerToCheck)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(playerToCheck)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

#IF FEATURE_FIXER
FUNC BOOL SHOULD_COMPANY_SUV_CLEANUP()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
		PRINTLN("SHOULD_COMPANY_SUV_CLEANUP - Player is not OK.")
		
		RETURN TRUE
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(g_netCompanySUV))
		PRINTLN("SHOULD_COMPANY_SUV_CLEANUP - Vehicle is destroyed.")
		
		RETURN TRUE
	ENDIF
	
	IF NOT (VDIST2(GET_ENTITY_COORDS(NET_TO_VEH(g_netCompanySUV), FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) <= 1000 * 1000)
		PRINTLN("SHOULD_COMPANY_SUV_CLEANUP - Out of range.")
		
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		PRINTLN("SHOULD_COMPANY_SUV_CLEANUP - Player is in property.")
		
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		PRINTLN("SHOULD_COMPANY_SUV_CLEANUP - Player is entering/exiting property.")
		
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("SHOULD_COMPANY_SUV_CLEANUP - Player is entering simple interior.")
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CREATE_FREEMODE_COMPANY_SUV()
	IF NOT IS_BIT_SET(g_iCompanySuvBS, COMPANY_SUV_GLOBAL_BS_FOUND_SAFE_COORDS)
		VEHICLE_SPAWN_LOCATION_PARAMS sParams
		sParams.fMinDistFromCoords = 10
		sParams.fMaxDistance = 1000
		sParams.bIgnoreCustomNodesForArea = TRUE
		
		IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<0.0, 0.0, 0.0>>, JUBILEE, TRUE, vCompanySuvSafeLocation, fCompanySuvSafeHeading, sParams)
			SET_BIT(g_iCompanySuvBS, COMPANY_SUV_GLOBAL_BS_FOUND_SAFE_COORDS)
		ENDIF
		
		PRINTLN("CREATE_FREEMODE_COMPANY_SUV - Waiting for COMPANY_SUV_GLOBAL_BS_FOUND_SAFE_COORDS")
		
		EXIT
	ENDIF
	
	VEHICLE_INDEX tempVehicle
	
	VEHICLE_SETUP_STRUCT_MP sVehicleSetup
	
	sVehicleSetup.VehicleSetup.eModel = JUBILEE
	sVehicleSetup.VehicleSetup.tlPlateText = "FCP1"
	sVehicleSetup.VehicleSetup.iPlateIndex = 4
	sVehicleSetup.VehicleSetup.iColour1 = 4
	sVehicleSetup.VehicleSetup.iColour2 = 0
	sVehicleSetup.VehicleSetup.iColourExtra1 = 5
	sVehicleSetup.VehicleSetup.iColourExtra2 = 0
	sVehicleSetup.iColour5 = 1
	sVehicleSetup.iColour6 = 132
	sVehicleSetup.iLivery2 = 0
	sVehicleSetup.VehicleSetup.iWindowTintColour = 1
	sVehicleSetup.VehicleSetup.iWheelType = 3
	sVehicleSetup.VehicleSetup.iTyreR = 255
	sVehicleSetup.VehicleSetup.iTyreG = 255
	sVehicleSetup.VehicleSetup.iTyreB = 255
	sVehicleSetup.VehicleSetup.iNeonR = 255
	sVehicleSetup.VehicleSetup.iNeonB = 255
	sVehicleSetup.VehicleSetup.iModIndex[MOD_BUMPER_F] = 14
	sVehicleSetup.VehicleSetup.iModIndex[MOD_BUMPER_R] = 8
	sVehicleSetup.VehicleSetup.iModIndex[MOD_SKIRT] = 6
	sVehicleSetup.VehicleSetup.iModIndex[MOD_EXHAUST] = 6
	sVehicleSetup.VehicleSetup.iModIndex[MOD_BONNET] = 6
	sVehicleSetup.VehicleSetup.iModIndex[MOD_ENGINE] = 2
	sVehicleSetup.VehicleSetup.iModIndex[MOD_BRAKES] = 1
	sVehicleSetup.VehicleSetup.iModIndex[MOD_GEARBOX] = 1
	sVehicleSetup.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
	sVehicleSetup.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
	
	SET_BIT(sVehicleSetup.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
	
	IF HAS_PLAYER_PURCHASED_FIXER_HQ_UPGRADE_VEH_WORKSHOP(PLAYER_ID())
		sVehicleSetup.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(g_netCompanySUV)
	OR NOT IS_NET_VEHICLE_DRIVEABLE(g_netCompanySUV)
		IF IS_MODEL_IN_CDIMAGE(sVehicleSetup.VehicleSetup.eModel)
			IF REQUEST_LOAD_MODEL(sVehicleSetup.VehicleSetup.eModel)
				IF CAN_REGISTER_MISSION_VEHICLES(1)
					IF NETWORK_IS_IN_MP_CUTSCENE()
						SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
					ENDIF
					
					PRINTLN("CREATE_FREEMODE_COMPANY_SUV - Creating vehicle")
					
					tempVehicle = CREATE_VEHICLE(sVehicleSetup.VehicleSetup.eModel, vCompanySuvSafeLocation, fCompanySuvSafeHeading, TRUE, FALSE)
					
					g_netCompanySUV = VEH_TO_NET(tempVehicle)
					
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVehicle, TRUE)
					
					SET_VEHICLE_SETUP_MP(tempVehicle, sVehicleSetup, DEFAULT, DEFAULT, TRUE)
					
					NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(tempVehicle, TRUE)
					SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(tempVehicle, FALSE)
					SET_VEHICLE_FULLBEAM(tempVehicle, FALSE)
					SET_VEHICLE_LIGHTS(tempVehicle, FORCE_VEHICLE_LIGHTS_OFF)
					SET_VEHICLE_FIXED(tempVehicle)
			        SET_ENTITY_HEALTH(tempVehicle, 1000)
			        SET_VEHICLE_ENGINE_HEALTH(tempVehicle, 1000)
			        SET_VEHICLE_PETROL_TANK_HEALTH(tempVehicle, 1000)
					SET_VEHICLE_DIRT_LEVEL(tempVehicle, 0.0)
					
					IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
		            AND NOT DECOR_EXIST_ON(tempVehicle, "Not_Allow_As_Saved_Veh")
						DECOR_SET_INT(tempVehicle, "Not_Allow_As_Saved_Veh", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
					ENDIF
					
					IF DECOR_IS_REGISTERED_AS_TYPE("Company_SUV", DECOR_TYPE_INT)
					AND NOT DECOR_EXIST_ON(tempVehicle, "Company_SUV")
						DECOR_SET_INT(tempVehicle, "Company_SUV", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
					ENDIF
					
					SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(g_netCompanySUV, PLAYER_ID(), FALSE)
					
					IF NETWORK_IS_IN_MP_CUTSCENE()
						SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("CREATE_FREEMODE_COMPANY_SUV - Model not loaded")
				
				EXIT
			ENDIF
		ENDIF
	ELSE
		tempVehicle = NET_TO_VEH(g_netCompanySUV)
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(g_netCompanySUV)
			IF (HAVE_VEHICLE_MODS_STREAMED_IN(tempVehicle)
			AND SET_VEHICLE_ON_GROUND_PROPERLY(tempVehicle))
				SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_FRONT_LEFT, 0.0)
				SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_FRONT_RIGHT, 0.0)
				SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_MID_LEFT, 0.0)
				SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_MID_RIGHT, 0.0)
				SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_REAR_LEFT, 0.0)
				SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_REAR_RIGHT, 0.0)
				SET_CAN_USE_HYDRAULICS(tempVehicle, FALSE)
				SET_MODEL_AS_NO_LONGER_NEEDED(sVehicleSetup.VehicleSetup.eModel)
				
				SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSix, BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_FIXER_HQ_COMPANY_SUV_IN_USE)
				
				RESET_NET_TIMER(g_stCompanySUV)
				
				g_iCompanySuvBS = 0
				
				SET_BIT(g_iCompanySuvBS, COMPANY_SUV_GLOBAL_BS_DELIVERED)
				
				PRINTLN("CREATE_FREEMODE_COMPANY_SUV - Vehicle is created")
			ENDIF
		ELSE
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(g_netCompanySUV)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_COMPANY_SUV_SPAWNING()
	IF IS_BIT_SET(g_iCompanySuvBS, COMPANY_SUV_GLOBAL_BS_ASSIGN_TO_MAIN_SCRIPT)
		VEHICLE_INDEX tempVeh
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF DOES_ENTITY_EXIST(tempVeh)
			AND IS_VEHICLE_DRIVEABLE(tempVeh)
			AND NETWORK_GET_ENTITY_IS_NETWORKED(tempVeh)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
					IF CAN_REGISTER_MISSION_VEHICLES(1)
						IF NOT IS_ENTITY_A_MISSION_ENTITY(tempVeh)
							SET_ENTITY_AS_MISSION_ENTITY(tempVeh, FALSE, TRUE)
							
							PRINTLN("MAINTAIN_COMPANY_SUV_SPAWNING - not a mission entity, setting as one.")
						ENDIF
						
						IF IS_ENTITY_A_MISSION_ENTITY(tempVeh)
							IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(tempVeh)
								SET_ENTITY_AS_MISSION_ENTITY(tempVeh, FALSE, TRUE)
								
								PRINTLN("MAINTAIN_COMPANY_SUV_SPAWNING - waiting for vehicle to belong to this script.")
							ENDIF
							
							IF DECOR_IS_REGISTERED_AS_TYPE("Company_SUV", DECOR_TYPE_INT)
								IF NOT DECOR_EXIST_ON(tempVeh, "Company_SUV")
									DECOR_SET_INT(tempVeh, "Company_SUV", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
								ENDIF
							ENDIF
							
							SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(VEH_TO_NET(tempVeh), PLAYER_ID(), TRUE)
							
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh, TRUE)
							
							g_netCompanySUV = VEH_TO_NET(tempVeh)
							
							PRINTLN("MAINTAIN_COMPANY_SUV_SPAWNING - new SUV registered.")
							
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSix, BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_FIXER_HQ_COMPANY_SUV_IN_USE)
							
							SET_COMPANY_SUV_ASSIGN_TO_MAIN_SCRIPT(FALSE)
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
				ENDIF
			ELSE
				PRINTLN("MAINTAIN_COMPANY_SUV_SPAWNING - g_bAssignCompanySuvMainScript = FALSE 2")
				
				SET_COMPANY_SUV_ASSIGN_TO_MAIN_SCRIPT(FALSE)
			ENDIF
		ELSE
			PRINTLN("MAINTAIN_COMPANY_SUV_SPAWNING - g_bAssignCompanySuvMainScript = FALSE 1")
			
			SET_COMPANY_SUV_ASSIGN_TO_MAIN_SCRIPT(FALSE)
		ENDIF
	ENDIF
	
	BOOL bDeleteBlip = FALSE
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(g_netCompanySUV)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(g_netCompanySUV)
		IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(g_netCompanySUV))
		AND IS_VEHICLE_EMPTY(NET_TO_VEH(g_netCompanySUV))
			IF NOT DOES_BLIP_EXIST(g_blipCompanySUV)
				g_blipCompanySUV = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(g_netCompanySUV))
				
				SET_BLIP_SPRITE(g_blipCompanySUV, RADAR_TRACE_JUBILEE)
				SET_BLIP_NAME_FROM_TEXT_FILE(g_blipCompanySUV, "COMPANY_SUV")
				SET_BLIP_AS_SHORT_RANGE(g_blipCompanySUV, FALSE)
			ELSE
				IF IS_BIT_SET(g_iCompanySuvBS, COMPANY_SUV_GLOBAL_BS_DELIVERED)
				AND NOT IS_BIT_SET(g_iCompanySuvBS, COMPANY_SUV_GLOBAL_BS_FLASH_BLIP)
				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP("COMP_SUV_DEL")
					
					SET_BLIP_FLASHES(g_blipCompanySUV, TRUE)
					SET_BLIP_FLASH_TIMER(g_blipCompanySUV, 5000)
					
					SET_BIT(g_iCompanySuvBS, COMPANY_SUV_GLOBAL_BS_FLASH_BLIP)
				ENDIF
			ENDIF
		ELSE
			bDeleteBlip = TRUE
		ENDIF
	ELSE
		bDeleteBlip = TRUE
	ENDIF
	
	IF bDeleteBlip
		IF DOES_BLIP_EXIST(g_blipCompanySUV)
			REMOVE_BLIP(g_blipCompanySUV)
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(g_netCompanySUV)
		IF SHOULD_COMPANY_SUV_CLEANUP()
			g_iCompanySuvBS = 0
			
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSix, BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_FIXER_HQ_COMPANY_SUV_IN_USE)
			
			CLEANUP_NET_ID(g_netCompanySUV)
			
			g_netCompanySUV = NULL
			
			IF DOES_BLIP_EXIST(g_blipCompanySUV)
				REMOVE_BLIP(g_blipCompanySUV)
			ENDIF
		ENDIF
	ELIF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSix, BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_FIXER_HQ_COMPANY_SUV_IN_USE)
	OR (NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE) AND g_iCompanySuvBS != 0)
		g_iCompanySuvBS = 0
		
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSSix, BS_SIMPLE_INTERIOR_GLOBAL_BS_SIX_FIXER_HQ_COMPANY_SUV_IN_USE)
		
		CLEANUP_NET_ID(g_netCompanySUV)
		
		g_netCompanySUV = NULL
		
		IF DOES_BLIP_EXIST(g_blipCompanySUV)
			REMOVE_BLIP(g_blipCompanySUV)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_iCompanySuvBS, COMPANY_SUV_GLOBAL_BS_CREATE_VEHICLE)
		IF NOT HAS_NET_TIMER_STARTED(g_stCompanySUV)
			START_NET_TIMER(g_stCompanySUV)
		ELIF HAS_NET_TIMER_EXPIRED(g_stCompanySUV, 10000)
			CREATE_FREEMODE_COMPANY_SUV()
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_OUT_OF_SIGHT(OUT_OF_SIGHT_DATA& sOutOfSightData)
	IF PLAYER_ID() = INVALID_PLAYER_INDEX()
		PRINTLN("[OUT_OF_SIGHT] - MAINTAIN_OUT_OF_SIGHT - Not processing while local player ID is invalid.")
		
		EXIT
	ENDIF
	
	SWITCH sOutOfSightData.eOutOfSightStage
		CASE OUT_OF_SIGHT_STAGE_IDLE
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
				PLAYER_INDEX playerBoss
				playerBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
				
				IF playerBoss != INVALID_PLAYER_INDEX()
				AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerBoss)].iOutOfSightBS, GLOBAL_OUT_OF_SIGHT_BS_STARTED_ABILITY)
					PRINTLN("[OUT_OF_SIGHT] - MAINTAIN_OUT_OF_SIGHT - My boss has activated the ability.")
					
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iOutOfSightBS, GLOBAL_OUT_OF_SIGHT_BS_DAMAGED_NON_GANG_MEMBER)
					
					sOutOfSightData.playerOwner = playerBoss
					
					sOutOfSightData.eOutOfSightStage = OUT_OF_SIGHT_STAGE_RUNNING
				ENDIF
			ELSE
				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iOutOfSightBS, GLOBAL_OUT_OF_SIGHT_BS_STARTED_ABILITY)
					PRINTLN("[OUT_OF_SIGHT] - MAINTAIN_OUT_OF_SIGHT - I have activated the ability.")
					
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iOutOfSightBS, GLOBAL_OUT_OF_SIGHT_BS_DAMAGED_NON_GANG_MEMBER)
					
					sOutOfSightData.playerOwner = PLAYER_ID()
					
					sOutOfSightData.eOutOfSightStage = OUT_OF_SIGHT_STAGE_RUNNING
				ENDIF
			ENDIF
		BREAK
		
		CASE OUT_OF_SIGHT_STAGE_RUNNING
			IF NOT IS_NET_PLAYER_OK(sOutOfSightData.playerOwner, FALSE)
				PRINTLN("[OUT_OF_SIGHT] - MAINTAIN_OUT_OF_SIGHT - Owner no longer ok. Cleanup.")
				
				sOutOfSightData.eOutOfSightStage = OUT_OF_SIGHT_STAGE_CLEANUP
				
				EXIT
			ENDIF
			
			IF sOutOfSightData.playerOwner != INVALID_PLAYER_INDEX()
			AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(sOutOfSightData.playerOwner)].iOutOfSightBS, GLOBAL_OUT_OF_SIGHT_BS_STARTED_ABILITY)
				PRINTLN("[OUT_OF_SIGHT] - MAINTAIN_OUT_OF_SIGHT - Owner no longer running the ability. Cleanup.")
				
				sOutOfSightData.eOutOfSightStage = OUT_OF_SIGHT_STAGE_CLEANUP
				
				EXIT
			ENDIF
			
			IF sOutOfSightData.playerOwner != PLAYER_ID()
			AND NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
				PRINTLN("[OUT_OF_SIGHT] - MAINTAIN_OUT_OF_SIGHT - I am no longer in the gang. Cleanup.")
				
				sOutOfSightData.eOutOfSightStage = OUT_OF_SIGHT_STAGE_CLEANUP
				
				EXIT
			ENDIF
			
			IF sOutOfSightData.playerOwner != PLAYER_ID()
			AND GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()) != sOutOfSightData.playerOwner
				PRINTLN("[OUT_OF_SIGHT] - MAINTAIN_OUT_OF_SIGHT - Boss has changed. Cleanup.")
				
				sOutOfSightData.eOutOfSightStage = OUT_OF_SIGHT_STAGE_CLEANUP
				
				EXIT
			ENDIF
			
			IF sOutOfSightData.playerOwner = PLAYER_ID()
			AND GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
				PRINTLN("[OUT_OF_SIGHT] - MAINTAIN_OUT_OF_SIGHT - I have joined a gang. Cleanup.")
				
				sOutOfSightData.eOutOfSightStage = OUT_OF_SIGHT_STAGE_CLEANUP
				
				EXIT
			ENDIF
			
			IF sOutOfSightData.playerOwner = PLAYER_ID()
			AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iOutOfSightBS, GLOBAL_OUT_OF_SIGHT_BS_DAMAGED_NON_GANG_MEMBER)
				PRINTLN("[OUT_OF_SIGHT] - MAINTAIN_OUT_OF_SIGHT - I have damaged a non-gang member player. Cleanup.")
				
				sOutOfSightData.eOutOfSightStage = OUT_OF_SIGHT_STAGE_CLEANUP
				
				EXIT
			ENDIF
			
			PLAYER_INDEX tempPlayer
			tempPlayer = INT_TO_NATIVE(PLAYER_INDEX, sOutOfSightData.iPlayerStagger)
			
			IF IS_NET_PLAYER_OK(tempPlayer)
			AND sOutOfSightData.playerOwner = PLAYER_ID()
			AND GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			AND GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), tempPlayer)
			AND IS_BIT_SET(GlobalplayerBD[sOutOfSightData.iPlayerStagger].iOutOfSightBS, GLOBAL_OUT_OF_SIGHT_BS_DAMAGED_NON_GANG_MEMBER)
				PRINTLN("[OUT_OF_SIGHT] - MAINTAIN_OUT_OF_SIGHT - Another player has damaged a non-gang member player. Cleanup.")
				
				sOutOfSightData.eOutOfSightStage = OUT_OF_SIGHT_STAGE_CLEANUP
				
				EXIT
			ENDIF
			
			VECTOR vPedCoords
			
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				vPedCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
			ENDIF
			
			VECTOR vOutOfSightCoords
			
			IF sOutOfSightData.playerOwner != INVALID_PLAYER_INDEX()
				vOutOfSightCoords = GlobalplayerBD[NATIVE_TO_INT(sOutOfSightData.playerOwner)].vOutOfSightCoords
			ENDIF
			
			FLOAT fDistanceFromCenter
			fDistanceFromCenter = VDIST2(vPedCoords, vOutOfSightCoords)
			
			IF NOT DOES_BLIP_EXIST(sOutOfSightData.blipOutOfSightArea)
				sOutOfSightData.blipOutOfSightArea = CREATE_BLIP_FOR_RADIUS(vOutOfSightCoords, TO_FLOAT(g_sMPTunables.iIMANI_OUT_OF_SIGHT_AREA_SIZE))
				
				SET_BLIP_COLOUR(sOutOfSightData.blipOutOfSightArea, GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_GREY))
			ENDIF
			
			IF NOT DOES_BLIP_EXIST(sOutOfSightData.blipOutOfSightEdge)
				sOutOfSightData.blipOutOfSightEdge = CREATE_BLIP_FOR_COORD(vOutOfSightCoords)
				
				SET_BLIP_SPRITE(sOutOfSightData.blipOutOfSightEdge, RADAR_TRACE_PICKUP_HIDDEN)
				SET_BLIP_COLOUR(sOutOfSightData.blipOutOfSightEdge, GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_GREY))
				SET_BLIP_AS_SHORT_RANGE(sOutOfSightData.blipOutOfSightEdge, FALSE)
				SET_BLIP_ALPHA(sOutOfSightData.blipOutOfSightEdge, 0)
				SET_BLIP_HIDDEN_ON_LEGEND(sOutOfSightData.blipOutOfSightEdge, TRUE)
			ELSE
				FLOAT fThreshold
				fThreshold = 400000.0
				
				IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
					fThreshold = 2250000.0
				ENDIF
				
				IF VDIST2(vPedCoords, vOutOfSightCoords) > fThreshold
					IF GET_BLIP_ALPHA(sOutOfSightData.blipOutOfSightEdge) <= 0
						SET_BLIP_ALPHA(sOutOfSightData.blipOutOfSightEdge, 255)
					ENDIF
				ELSE
					IF GET_BLIP_ALPHA(sOutOfSightData.blipOutOfSightEdge) >= 255
						SET_BLIP_ALPHA(sOutOfSightData.blipOutOfSightEdge, 0)
					ENDIF
				ENDIF
			ENDIF
			
			IF fDistanceFromCenter <= 250000.0
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iOutOfSightBS, GLOBAL_OUT_OF_SIGHT_BS_BLIP_HIDDEN)
					PRINTLN("[OUT_OF_SIGHT] - MAINTAIN_OUT_OF_SIGHT - Hiding my blip.")
					
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iOutOfSightBS, GLOBAL_OUT_OF_SIGHT_BS_BLIP_HIDDEN)
				ENDIF
				
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iOutOfSightBS, GLOBAL_OUT_OF_SIGHT_BS_HELP_SHOWN)
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP("OOS_HELP")
						
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iOutOfSightBS, GLOBAL_OUT_OF_SIGHT_BS_HELP_SHOWN)
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iOutOfSightBS, GLOBAL_OUT_OF_SIGHT_BS_BLIP_HIDDEN)
					PRINTLN("[OUT_OF_SIGHT] - MAINTAIN_OUT_OF_SIGHT - Showing my blip.")
					
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iOutOfSightBS, GLOBAL_OUT_OF_SIGHT_BS_BLIP_HIDDEN)
				ENDIF
				
				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iOutOfSightBS, GLOBAL_OUT_OF_SIGHT_BS_HELP_SHOWN)
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iOutOfSightBS, GLOBAL_OUT_OF_SIGHT_BS_HELP_SHOWN)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE OUT_OF_SIGHT_STAGE_CLEANUP
			IF sOutOfSightData.playerOwner = PLAYER_ID()
				REINIT_NET_TIMER(g_TransitionSessionNonResetVars.contactRequests.stCDTimer[ENUM_TO_INT(REQUEST_OUT_OF_SIGHT)], TRUE)
				
				g_TransitionSessionNonResetVars.contactRequests.iCDTime[ENUM_TO_INT(REQUEST_OUT_OF_SIGHT)] = g_sMPTunables.iIMANI_OUT_OF_SIGHT_COOLDOWN
			ENDIF
			
			IF DOES_BLIP_EXIST(sOutOfSightData.blipOutOfSightArea)
				REMOVE_BLIP(sOutOfSightData.blipOutOfSightArea)
			ENDIF
			
			IF DOES_BLIP_EXIST(sOutOfSightData.blipOutOfSightEdge)
				REMOVE_BLIP(sOutOfSightData.blipOutOfSightEdge)
			ENDIF
			
			sOutOfSightData.playerOwner = INVALID_PLAYER_INDEX()
			
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iOutOfSightBS = 0
			
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vOutOfSightCoords = <<0.0, 0.0, 0.0>>
			
			sOutOfSightData.eOutOfSightStage = OUT_OF_SIGHT_STAGE_IDLE
		BREAK
	ENDSWITCH
	
	sOutOfSightData.iPlayerStagger++
	
	IF sOutOfSightData.iPlayerStagger >= NUM_NETWORK_PLAYERS
		sOutOfSightData.iPlayerStagger = 0
	ENDIF
ENDPROC

/// PURPOSE:
///    url:bugstar:7341595 - Sends a hyped up text message to the player if they renovate their Agency.
PROC MAINTAIN_FIXER_HQ_UPGRADE_TXT_MESSAGES(SCRIPT_TIMER &ref_DelayTimer)
	IF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_BOUGHT_ALL_UPGRADES)
	OR IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_RENOVATED_STYLE)
	AND NOT g_bBrowserVisible
		IF NOT HAS_NET_TIMER_STARTED(ref_DelayTimer)
			START_NET_TIMER(ref_DelayTimer)
		ELIF HAS_NET_TIMER_STARTED_AND_EXPIRED(ref_DelayTimer, 3000)
			STRING strText = "FXR_RENO_TXT2"
			
			IF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_BOUGHT_ALL_UPGRADES)
				strText = "FXR_RENO_TXT1" // Prioritise txt message for purchasing all upgrades. 
			ENDIF
			
			IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_FIXER_FRANKLIN, strText, TXTMSG_UNLOCKED)
				RESET_NET_TIMER(ref_DelayTimer)
				
				IF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_BOUGHT_ALL_UPGRADES)
					CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_BOUGHT_ALL_UPGRADES)
				ENDIF
				
				IF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_RENOVATED_STYLE)
					CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_RENOVATED_STYLE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_LOCAL_PLAYER_COMPLETED_REQUIRED_MISSIONS_FOR_MUSIC_STUDIO_ACCESS()
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_bypassMCforMusicStudio")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF HAS_LOCAL_PLAYER_UNLOCKED_FIXER_SHORT_TRIPS(TRUE)
	AND HAS_PLAYER_COMPLETED_FIXER_SHORT_TRIP(PLAYER_ID(), FST_SHORT_TRIP_1)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#ENDIF // FEATURE_FIXER

#IF FEATURE_DLC_1_2022

FUNC BOOL SHOULD_TONY_LIMO_CLEANUP()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
		PRINTLN("SHOULD_TONY_LIMO_CLEANUP - Player is not OK.")
		
		RETURN TRUE
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(g_sTonyLimo.NetID))
		PRINTLN("SHOULD_TONY_LIMO_CLEANUP - Vehicle is destroyed.")
		
		RETURN TRUE
	ENDIF
	
	IF NOT (VDIST2(GET_ENTITY_COORDS(NET_TO_VEH(g_sTonyLimo.NetID), FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) <= 1000 * 1000)
		PRINTLN("SHOULD_TONY_LIMO_CLEANUP - Out of range.")
		
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		PRINTLN("SHOULD_TONY_LIMO_CLEANUP - Player is in property.")
		
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		PRINTLN("SHOULD_TONY_LIMO_CLEANUP - Player is entering/exiting property.")
		
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("SHOULD_TONY_LIMO_CLEANUP - Player is entering simple interior.")
		
		RETURN TRUE
	ENDIF
	
	SWITCH GB_GET_MEGA_BUSINESS_VARIATION_PLAYER_IS_ON(PLAYER_ID())
		CASE MBV_SNAPMATIC
		CASE MBV_PRIVATE_TAXI
		CASE MBV_COLLECT_STAFF
		CASE MBV_COLLECT_DJ_COLLECTOR
		CASE MBV_COLLECT_DJ_CRASH
		CASE MBV_COLLECT_DJ_HOOKED
		CASE MBV_COLLECT_DJ_STOLEN_BAG
			PRINTLN("SHOULD_TONY_LIMO_CLEANUP - Player is on a mega business variation that uses the limo")
			RETURN TRUE
	ENDSWITCH
	
	SWITCH GB_GET_CLUB_ODD_JOBS_MISSION_PLAYER_IS_ON(PLAYER_ID())
		CASE COJV_PASSED_OUT_VIP
			PRINTLN("SHOULD_TONY_LIMO_CLEANUP - Player is on club odd job that uses the limo")
			RETURN TRUE
	ENDSWITCH
	
	IF g_sTonyLimo.bForceCleanup
		PRINTLN("SHOULD_TONY_LIMO_CLEANUP - g_sTonyLimo.bForceCleanup = TRUE")
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC REMOVE_TONY_LIMO_BLIP()
	IF DOES_BLIP_EXIST(g_sTonyLimo.BlipID)
		REMOVE_BLIP(g_sTonyLimo.BlipID)
		PRINTLN("[TONY_LIMO] REMOVE_TONY_LIMO_BLIP - removed blip.")
	ENDIF
ENDPROC

PROC CLEANUP_TONY_LIMO(BOOL bDelete = FALSE)
	IF g_sTonyLimo.iBitSet = 0
		EXIT
	ENDIF
	
	PRINTLN("[TONY_LIMO] CLEANUP_TONY_LIMO - called.")
	DEBUG_PRINTCALLSTACK()
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(g_sTonyLimo.NetID)
	
		IF bDelete
			REMOVE_TONY_LIMO_BLIP()
			DELETE_NET_ID(g_sTonyLimo.NetID)
			PRINTLN("[TONY_LIMO] CLEANUP_TONY_LIMO - deleted.")
		ELSE
			IF NOT TAKE_CONTROL_OF_NET_ID(g_sTonyLimo.NetID)
				PRINTLN("[TONY_LIMO] CLEANUP_TONY_LIMO - waiting to take control of the vehicle...")
				EXIT
			ENDIF
							
			BROADCAST_LEAVE_VEHICLE(ALL_PLAYERS_IN_VEHICLE(NET_TO_VEH(g_sTonyLimo.NetID)), TRUE, 5.0, 1)
		
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(NET_TO_VEH(g_sTonyLimo.NetID), TRUE)
		
			REMOVE_TONY_LIMO_BLIP()
			CLEANUP_NET_ID(g_sTonyLimo.NetID)
			PRINTLN("[TONY_LIMO] CLEANUP_TONY_LIMO - cleaned up.")
		ENDIF
		
	ENDIF
	
	g_sTonyLimo.iBitSet = 0
	g_sTonyLimo.NetID = NULL
	g_sTonyLimo.bForceCleanup = FALSE
	
ENDPROC

FUNC BOOL CHECK_AMBIENT_NET_VEHICLE_CAN_BE_CREATED_WITH_MODEL(MODEL_NAMES eModel)
	IF IS_MODEL_IN_CDIMAGE(eModel)
	AND CAN_REGISTER_MISSION_VEHICLES(1)
	AND REQUEST_LOAD_MODEL(eModel)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_FREEMODE_TONY_LIMO()
	
	IF g_sTonyLimo.eLimoType = eTONYLIMO_INVALID
		IF GET_RANDOM_BOOL()
			g_sTonyLimo.eLimoType = eTONYLIMO_SCHAFTER
		ELSE
			g_sTonyLimo.eLimoType = eTONYLIMO_PATRIOT_STRETCH
		ENDIF
		PRINTLN("[TONY_LIMO] CREATE_FREEMODE_TONY_LIMO - Limo type was not specified")
	ENDIF

	MODEL_NAMES eModel = GET_TONY_LIMO_MODEL_NAME(g_sTonyLimo.eLimoType)

	IF NOT IS_BIT_SET(g_sTonyLimo.iBitSet, TONY_LIMO_GLOBAL_BS_FOUND_SAFE_COORDS)
		IF CHECK_AMBIENT_NET_VEHICLE_CAN_BE_CREATED_WITH_MODEL(eModel)
			VEHICLE_SPAWN_LOCATION_PARAMS sParams
			sParams.fMinDistFromCoords = 10
			sParams.fMaxDistance = 1000
			sParams.bIgnoreCustomNodesForArea = TRUE
			
			IF NOT HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<0.0, 0.0, 0.0>>, eModel, TRUE, g_sTonyLimo.vSafeLocation, g_sTonyLimo.fSafeHeading, sParams)
				PRINTLN("[TONY_LIMO] CREATE_FREEMODE_TONY_LIMO - Waiting for find safe coords...")
				RETURN FALSE
			ENDIF
			
			IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(g_sTonyLimo.vSafeLocation)
				PRINTLN("[TONY_LIMO] CREATE_FREEMODE_TONY_LIMO - g_sTonyLimo.vSafeLocation is not safe for net entity creation.")
				RETURN FALSE
			ENDIF
			
			PRINTLN("[TONY_LIMO] CREATE_FREEMODE_TONY_LIMO - Found safe coord for Tony Limo at ", g_sTonyLimo.vSafeLocation)
			SET_BIT(g_sTonyLimo.iBitSet, TONY_LIMO_GLOBAL_BS_FOUND_SAFE_COORDS)
		ELSE
			PRINTLN("[TONY_LIMO] CREATE_FREEMODE_TONY_LIMO - Cannot currently create net vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(eModel))
			RETURN FALSE
		ENDIF
	ENDIF
	
	VEHICLE_INDEX tempVehicle
		
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(g_sTonyLimo.NetID)
	OR NOT IS_NET_VEHICLE_DRIVEABLE(g_sTonyLimo.NetID)
		
		IF NETWORK_IS_IN_MP_CUTSCENE()
			SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
		ENDIF
		
		PRINTLN("[TONY_LIMO] CREATE_FREEMODE_TONY_LIMO - Creating vehicle")
		
		tempVehicle = CREATE_VEHICLE(eModel, g_sTonyLimo.vSafeLocation, g_sTonyLimo.fSafeHeading, TRUE, FALSE)
		
		g_sTonyLimo.NetID = VEH_TO_NET(tempVehicle)
		
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVehicle, TRUE)
		
		SWITCH eModel
			CASE SCHAFTER3
				IF GET_NUM_MOD_KITS(tempVehicle) > 0
					SET_VEHICLE_MOD_KIT(tempVehicle, 0)
				ENDIF
				SET_VEHICLE_WINDOW_TINT(tempVehicle, 1)
				SET_VEHICLE_NUMBER_PLATE_TEXT(tempVehicle, "PR1NCE")
				SET_VEHICLE_MOD(tempVehicle, MOD_HORN, 45)
			BREAK
			CASE PATRIOT2
				VEHICLE_SETUP_STRUCT_MP sData
				sData.VehicleSetup.eModel = PATRIOT2
				sData.VehicleSetup.tlPlateText = "PR2NCE"
				sData.VehicleSetup.iPlateIndex = 3
				sData.VehicleSetup.iColour1 = 112
				sData.VehicleSetup.iColour2 = 120
				sData.VehicleSetup.iColourExtra1 = 145
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWindowTintColour = 1
				sData.VehicleSetup.iWheelType = 7
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 35
				sData.VehicleSetup.iNeonG = 1
				sData.VehicleSetup.iNeonB = 255
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
				sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
				sData.VehicleSetup.iModIndex[MOD_HORN] = 45
				sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
				sData.VehicleSetup.iModIndex[MOD_WHEELS] = 21
				SET_VEHICLE_SETUP_MP(tempVehicle, sData, DEFAULT, DEFAULT, TRUE)
			BREAK
		ENDSWITCH
		
		NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(tempVehicle, TRUE)
		SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(tempVehicle, FALSE)
		SET_VEHICLE_FULLBEAM(tempVehicle, FALSE)
		SET_VEHICLE_LIGHTS(tempVehicle, FORCE_VEHICLE_LIGHTS_OFF)
		SET_VEHICLE_FIXED(tempVehicle)
        SET_ENTITY_HEALTH(tempVehicle, 1000)
        SET_VEHICLE_ENGINE_HEALTH(tempVehicle, 1000)
        SET_VEHICLE_PETROL_TANK_HEALTH(tempVehicle, 1000)
		SET_VEHICLE_DIRT_LEVEL(tempVehicle, 0.0)
		SET_VEHICLE_TYRES_CAN_BURST(tempVehicle, FALSE)
		SET_VEHICLE_STRONG(tempVehicle, TRUE)
		SET_VEHICLE_DAMAGE_SCALE(tempVehicle, 0.2)
		SET_VEHICLE_USES_MP_PLAYER_DAMAGE_MULTIPLIER(tempVehicle, FALSE)
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(tempVehicle, FALSE)
		
		IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
        AND NOT DECOR_EXIST_ON(tempVehicle, "Not_Allow_As_Saved_Veh")
			DECOR_SET_INT(tempVehicle, "Not_Allow_As_Saved_Veh", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
			PRINTLN("[TONY_LIMO] CREATE_FREEMODE_TONY_LIMO - set decorator Not_Allow_As_Saved_Veh on vehicle")
		ELSE
			PRINTLN("[TONY_LIMO] CREATE_FREEMODE_TONY_LIMO - Not_Allow_As_Saved_Veh already on vehicle or not registered as DECOR_TYPE_INT")
		ENDIF
		
		INT iDecoratorBS
		IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
			IF DECOR_EXIST_ON(tempVehicle, "MPBitset")
				iDecoratorBS = DECOR_GET_INT(tempVehicle, "MPBitset")
			ENDIF
			SET_BIT(iDecoratorBS, MP_DECORATOR_BS_VEHICLE_LEAVING_GARAGE)
			DECOR_SET_INT(tempVehicle, "MPBitset", iDecoratorBS)
			PRINTLN("[TONY_LIMO] CREATE_FREEMODE_TONY_LIMO - set decorator bit MP_DECORATOR_BS_VEHICLE_LEAVING_GARAGE on vehicle")
		ELSE
			PRINTLN("[TONY_LIMO] CREATE_FREEMODE_TONY_LIMO - decor MPBitset not registered as DECOR_TYPE_INT")
		ENDIF
		
		SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(g_sTonyLimo.NetID, PLAYER_ID(), FALSE)
		
		IF NETWORK_IS_IN_MP_CUTSCENE()
			SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
		ENDIF
	ELSE
		tempVehicle = NET_TO_VEH(g_sTonyLimo.NetID)
		
		IF TAKE_CONTROL_OF_NET_ID(g_sTonyLimo.NetID)
			IF (HAVE_VEHICLE_MODS_STREAMED_IN(tempVehicle)
			AND SET_VEHICLE_ON_GROUND_PROPERLY(tempVehicle))
				SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
				
				PRINTLN("[TONY_LIMO] CREATE_FREEMODE_TONY_LIMO - Vehicle is created")
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_TONY_LIMO_SPAWNING()
	IF IS_BIT_SET(g_sTonyLimo.iBitSet, TONY_LIMO_GLOBAL_BS_ASSIGN_TO_MAIN_SCRIPT)
		VEHICLE_INDEX tempVeh
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF DOES_ENTITY_EXIST(tempVeh)
			AND IS_VEHICLE_DRIVEABLE(tempVeh)
			AND NETWORK_GET_ENTITY_IS_NETWORKED(tempVeh)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
					IF CAN_REGISTER_MISSION_VEHICLES(1)
						IF NOT IS_ENTITY_A_MISSION_ENTITY(tempVeh)
							SET_ENTITY_AS_MISSION_ENTITY(tempVeh, FALSE, TRUE)
							
							PRINTLN("[TONY_LIMO] MAINTAIN_TONY_LIMO_SPAWNING - not a mission entity, setting as one.")
						ENDIF
						
						IF IS_ENTITY_A_MISSION_ENTITY(tempVeh)
							IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(tempVeh)
								SET_ENTITY_AS_MISSION_ENTITY(tempVeh, FALSE, TRUE)
								
								PRINTLN("[TONY_LIMO] MAINTAIN_TONY_LIMO_SPAWNING - waiting for vehicle to belong to this script.")
							ENDIF
							
//							IF DECOR_IS_REGISTERED_AS_TYPE("Company_SUV", DECOR_TYPE_INT)
//								IF NOT DECOR_EXIST_ON(tempVeh, "Company_SUV")
//									DECOR_SET_INT(tempVeh, "Company_SUV", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
//								ENDIF
//							ENDIF
							
							SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(VEH_TO_NET(tempVeh), PLAYER_ID(), TRUE)
							
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh, TRUE)
							
							g_sTonyLimo.NetID = VEH_TO_NET(tempVeh)
							
							PRINTLN("[TONY_LIMO] MAINTAIN_TONY_LIMO_SPAWNING - new SUV registered.")
							
							CLEAR_BIT(g_sTonyLimo.iBitSet, TONY_LIMO_GLOBAL_BS_ASSIGN_TO_MAIN_SCRIPT)
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
				ENDIF
			ELSE
				PRINTLN("[TONY_LIMO] MAINTAIN_TONY_LIMO_SPAWNING - g_bAssignCompanySuvMainScript = FALSE 2")
				
				CLEAR_BIT(g_sTonyLimo.iBitSet, TONY_LIMO_GLOBAL_BS_ASSIGN_TO_MAIN_SCRIPT)
			ENDIF
		ELSE
			PRINTLN("[TONY_LIMO] MAINTAIN_TONY_LIMO_SPAWNING - g_bAssignCompanySuvMainScript = FALSE 1")
			
			CLEAR_BIT(g_sTonyLimo.iBitSet, TONY_LIMO_GLOBAL_BS_ASSIGN_TO_MAIN_SCRIPT)
		ENDIF
	ENDIF
	
	BOOL bDeleteBlip = FALSE
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(g_sTonyLimo.NetID)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(g_sTonyLimo.NetID)
		IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(g_sTonyLimo.NetID))
		AND IS_VEHICLE_EMPTY(NET_TO_VEH(g_sTonyLimo.NetID))
			IF NOT DOES_BLIP_EXIST(g_sTonyLimo.BlipID)
				g_sTonyLimo.BlipID = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(g_sTonyLimo.NetID))
				PRINTLN("[TONY_LIMO] MAINTAIN_TONY_LIMO_SPAWNING - Added blip")
				
				SET_BLIP_SPRITE(g_sTonyLimo.BlipID, RADAR_TRACE_GANG_VEHICLE)
				SET_BLIP_COLOUR(g_sTonyLimo.BlipID, BLIP_COLOUR_HUDCOLOUR_YELLOW)
				SET_BLIP_NAME_FROM_TEXT_FILE(g_sTonyLimo.BlipID, "NCLUB_LIMO")
				SET_BLIP_AS_SHORT_RANGE(g_sTonyLimo.BlipID, FALSE)
			ELSE
				IF IS_BIT_SET(g_sTonyLimo.iBitSet, TONY_LIMO_GLOBAL_BS_DELIVERED)
				AND NOT IS_BIT_SET(g_sTonyLimo.iBitSet, TONY_LIMO_GLOBAL_BS_FLASH_BLIP)
				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF NOT IS_BIT_SET(g_sTonyLimo.iBitSet, TONY_LIMO_GLOBAL_BS_SUPPRESS_BLIP_HELP)
						PRINT_HELP("TONY_LIMO_DEL")
					ENDIF
					PRINTLN("[TONY_LIMO] MAINTAIN_TONY_LIMO_SPAWNING - Flashed blip")
					
					SET_BLIP_FLASHES(g_sTonyLimo.BlipID, TRUE)
					SET_BLIP_FLASH_TIMER(g_sTonyLimo.BlipID, 5000)
					
					SET_BIT(g_sTonyLimo.iBitSet, TONY_LIMO_GLOBAL_BS_FLASH_BLIP)
					CLEAR_BIT(g_sTonyLimo.iBitSet, TONY_LIMO_GLOBAL_BS_SUPPRESS_BLIP_HELP)
				ENDIF
			ENDIF
		ELSE
			bDeleteBlip = TRUE
		ENDIF
	ELSE
		bDeleteBlip = TRUE
	ENDIF
	
	IF bDeleteBlip
		IF DOES_BLIP_EXIST(g_sTonyLimo.BlipID)
			REMOVE_BLIP(g_sTonyLimo.BlipID)
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(g_sTonyLimo.NetID)
		IF SHOULD_TONY_LIMO_CLEANUP()
			PRINTLN("[TONY_LIMO] MAINTAIN_TONY_LIMO_SPAWNING - Cleaning up limo")
			CLEANUP_TONY_LIMO()
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_sTonyLimo.iBitSet, TONY_LIMO_GLOBAL_BS_CREATE_VEHICLE)
		IF NOT HAS_NET_TIMER_STARTED(g_sTonyLimo.Timer)
			START_NET_TIMER(g_sTonyLimo.Timer)
			g_sTonyLimo.iSpawnTime = GET_RANDOM_INT_IN_RANGE(TONY_LIMO_SPAWN_TIME-2000, TONY_LIMO_SPAWN_TIME+2000)
			PRINTLN("[TONY_LIMO] MAINTAIN_TONY_LIMO_SPAWNING - Started spawn timer. Will elapse in ", g_sTonyLimo.iSpawnTime, "ms")
		ELIF HAS_NET_TIMER_EXPIRED(g_sTonyLimo.Timer, g_sTonyLimo.iSpawnTime)
		OR IS_BIT_SET(g_sTonyLimo.iBitSet, TONY_LIMO_GLOBAL_BS_CREATE_IMMEDIATELY)
			IF CREATE_FREEMODE_TONY_LIMO()
				PRINTLN("[TONY_LIMO] MAINTAIN_TONY_LIMO_SPAWNING - Created Tony Limo")
				RESET_NET_TIMER(g_sTonyLimo.Timer)
				BOOL bSuppressHelp = IS_BIT_SET(g_sTonyLimo.iBitSet, TONY_LIMO_GLOBAL_BS_SUPPRESS_BLIP_HELP)
				g_sTonyLimo.iBitSet = 0
				SET_BIT(g_sTonyLimo.iBitSet, TONY_LIMO_GLOBAL_BS_DELIVERED)
				IF bSuppressHelp
					SET_BIT(g_sTonyLimo.iBitSet, TONY_LIMO_GLOBAL_BS_SUPPRESS_BLIP_HELP)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_BUNKER_VEHICLE_CLEANUP()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
		PRINTLN("SHOULD_BUNKER_VEHICLE_CLEANUP - Player is not OK.")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(g_sBunkerVehicle.NetID))
		PRINTLN("SHOULD_BUNKER_VEHICLE_CLEANUP - Vehicle is destroyed.")
		RETURN TRUE
	ENDIF
	
	IF NOT (VDIST2(GET_ENTITY_COORDS(NET_TO_VEH(g_sBunkerVehicle.NetID), FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) <= 1000 * 1000)
		PRINTLN("SHOULD_BUNKER_VEHICLE_CLEANUP - Out of range.")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		PRINTLN("SHOULD_BUNKER_VEHICLE_CLEANUP - Player is in property.")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		PRINTLN("SHOULD_BUNKER_VEHICLE_CLEANUP - Player is entering/exiting property.")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("SHOULD_BUNKER_VEHICLE_CLEANUP - Player is entering simple interior.")
		RETURN TRUE
	ENDIF
	
	IF g_sBunkerVehicle.bForceCleanup
		PRINTLN("SHOULD_BUNKER_VEHICLE_CLEANUP - g_sBunkerVehicle.bForceCleanup = TRUE")
		
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

PROC REMOVE_BUNKER_VEHICLE_BLIP()
	IF DOES_BLIP_EXIST(g_sBunkerVehicle.BlipID)
		REMOVE_BLIP(g_sBunkerVehicle.BlipID)
		PRINTLN("[BUNKER_VEHICLE] REMOVE_BUNKER_VEHICLE_BLIP - removed blip.")
	ENDIF
ENDPROC

PROC CLEANUP_BUNKER_VEHICLE(BOOL bDelete = FALSE)
	IF g_sBunkerVehicle.iBitSet = 0
		EXIT
	ENDIF
	
	PRINTLN("[BUNKER_VEHICLE] CLEANUP_BUNKER_VEHICLE - called.")
	DEBUG_PRINTCALLSTACK()
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(g_sBunkerVehicle.NetID)
	
		IF bDelete
			REMOVE_BUNKER_VEHICLE_BLIP()
			DELETE_NET_ID(g_sBunkerVehicle.NetID)
			PRINTLN("[BUNKER_VEHICLE] CLEANUP_BUNKER_VEHICLE - deleted.")
		ELSE
			IF NOT TAKE_CONTROL_OF_NET_ID(g_sBunkerVehicle.NetID)
				PRINTLN("[BUNKER_VEHICLE] CLEANUP_BUNKER_VEHICLE - waiting to take control of the vehicle...")
				EXIT
			ENDIF
							
			BROADCAST_LEAVE_VEHICLE(ALL_PLAYERS_IN_VEHICLE(NET_TO_VEH(g_sBunkerVehicle.NetID)), TRUE, 5.0, 1)
		
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(NET_TO_VEH(g_sBunkerVehicle.NetID), TRUE)
		
			REMOVE_BUNKER_VEHICLE_BLIP()
			CLEANUP_NET_ID(g_sBunkerVehicle.NetID)
			PRINTLN("[BUNKER_VEHICLE] CLEANUP_BUNKER_VEHICLE - cleaned up.")
		ENDIF
		
	ENDIF
	
	g_sBunkerVehicle.iBitSet = 0
	g_sBunkerVehicle.NetID = NULL
	g_sBunkerVehicle.bForceCleanup = FALSE
	
ENDPROC

FUNC BOOL CREATE_FREEMODE_BUNKER_VEHICLE()
	
	IF g_sBunkerVehicle.eVehType = eBUNKERVEHICLE_INVALID
		g_sBunkerVehicle.eVehType = INT_TO_ENUM(BUNKER_VEHICLE_TYPE, GET_RANDOM_INT_IN_RANGE(ENUM_TO_INT(eBUNKERVEHICLE_INVALID)+1, ENUM_TO_INT(eBUNKERVEHICLE_MAX)))
		PRINTLN("[BUNKER_VEHICLE] CREATE_FREEMODE_BUNKER_VEHICLE - Bunker vehicle type was not specified")
	ENDIF

	MODEL_NAMES eModel = GET_BUNKER_VEHICLE_MODEL_NAME(g_sBunkerVehicle.eVehType)

	IF NOT IS_BIT_SET(g_sBunkerVehicle.iBitSet, BUNKER_VEHICLE_GLOBAL_BS_FOUND_SAFE_COORDS)
		IF CHECK_AMBIENT_NET_VEHICLE_CAN_BE_CREATED_WITH_MODEL(eModel)
			VEHICLE_SPAWN_LOCATION_PARAMS sParams
			sParams.fMinDistFromCoords = 10
			sParams.fMaxDistance = 1000
			sParams.bIgnoreCustomNodesForArea = TRUE
			
			IF NOT HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<0.0, 0.0, 0.0>>, eModel, TRUE, g_sBunkerVehicle.vSafeLocation, g_sBunkerVehicle.fSafeHeading, sParams)
				PRINTLN("[BUNKER_VEHICLE] CREATE_FREEMODE_BUNKER_VEHICLE - Waiting for find safe coords...")
				RETURN FALSE
			ENDIF
			
			IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(g_sBunkerVehicle.vSafeLocation)
				PRINTLN("[BUNKER_VEHICLE] CREATE_FREEMODE_BUNKER_VEHICLE - g_sBunkerVehicle.vSafeLocation is not safe for net entity creation.")
				RETURN FALSE
			ENDIF
			
			PRINTLN("[BUNKER_VEHICLE] CREATE_FREEMODE_BUNKER_VEHICLE - Found safe coord for bunker vehicle at ", g_sBunkerVehicle.vSafeLocation)
			SET_BIT(g_sBunkerVehicle.iBitSet, BUNKER_VEHICLE_GLOBAL_BS_FOUND_SAFE_COORDS)
		ELSE
			PRINTLN("[BUNKER_VEHICLE] CREATE_FREEMODE_BUNKER_VEHICLE - Cannot currently create net vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(eModel))
			RETURN FALSE
		ENDIF
	ENDIF
	
	VEHICLE_INDEX tempVehicle
		
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(g_sBunkerVehicle.NetID)
	OR NOT IS_NET_VEHICLE_DRIVEABLE(g_sBunkerVehicle.NetID)
		
		IF NETWORK_IS_IN_MP_CUTSCENE()
			SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
		ENDIF
		
		PRINTLN("[BUNKER_VEHICLE] CREATE_FREEMODE_BUNKER_VEHICLE - Creating vehicle")
		
		tempVehicle = CREATE_VEHICLE(eModel, g_sBunkerVehicle.vSafeLocation, g_sBunkerVehicle.fSafeHeading, TRUE, FALSE)
		
		g_sBunkerVehicle.NetID = VEH_TO_NET(tempVehicle)
		
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVehicle, TRUE)
		
		VEHICLE_SETUP_STRUCT_MP sData
		
		SWITCH eModel
			CASE ZION3
				sData.VehicleSetup.eModel = ZION3
				sData.VehicleSetup.iPlateIndex = 4
				sData.VehicleSetup.iColour1 = 63
				sData.VehicleSetup.iColour2 = 112
				sData.VehicleSetup.iColourExtra1 = 87
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWindowTintColour = 3
				sData.VehicleSetup.iWheelType = 5
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
				sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
				sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
				sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
				sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
				sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
				sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
				sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
				sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
				sData.VehicleSetup.iModIndex[MOD_WHEELS] = 46
			BREAK
			CASE GRANGER
				sData.VehicleSetup.eModel = GRANGER
				sData.VehicleSetup.iPlateIndex = 4
				sData.VehicleSetup.iColour1 = 147
				sData.VehicleSetup.iColour2 = 147
				sData.VehicleSetup.iColourExtra1 = 147
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWindowTintColour = 1
				sData.VehicleSetup.iWheelType = 11
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_9)
				sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			BREAK
			CASE MESA3
				sData.VehicleSetup.eModel = MESA3
				sData.VehicleSetup.iPlateIndex = 4
				sData.VehicleSetup.iColour1 = 4
				sData.VehicleSetup.iColour2 = 1
				sData.VehicleSetup.iColourExtra1 = 4
				sData.VehicleSetup.iColourExtra2 = 1
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWheelType = 4
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_10)
				sData.VehicleSetup.iModIndex[MOD_WHEELS] = 29
			BREAK
			CASE MANCHEZ
				sData.VehicleSetup.eModel = MANCHEZ
				sData.VehicleSetup.iColour1 = 52
				sData.VehicleSetup.iColour2 = 5
				sData.VehicleSetup.iColourExtra1 = 59
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWheelType = 6
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
				sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
				sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
				sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
				sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
				sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
			BREAK
			CASE VERUS
				sData.VehicleSetup.eModel = VERUS
				sData.VehicleSetup.iPlateIndex = 4
				sData.VehicleSetup.iColour1 = 99
				sData.VehicleSetup.iColour2 = 0
				sData.VehicleSetup.iColourExtra1 = 0
				sData.VehicleSetup.iColourExtra2 = 99
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWheelType = 4
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
				sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
				sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
				sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
				sData.VehicleSetup.iModIndex[MOD_WHEELS] = 24
				sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
			BREAK
			CASE CADDY3
				sData.VehicleSetup.eModel = CADDY3
				sData.VehicleSetup.iColour1 = 4
				sData.VehicleSetup.iColour2 = 0
				sData.VehicleSetup.iColourExtra1 = 63
				sData.VehicleSetup.iColourExtra2 = 0
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iLivery = 0
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			BREAK
		ENDSWITCH
		
		SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
		SET_VEHICLE_SETUP_MP(tempVehicle, sData, DEFAULT, DEFAULT, TRUE)
		
		NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(tempVehicle, TRUE)
		SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(tempVehicle, FALSE)
		SET_VEHICLE_FULLBEAM(tempVehicle, FALSE)
		SET_VEHICLE_LIGHTS(tempVehicle, FORCE_VEHICLE_LIGHTS_OFF)
		SET_VEHICLE_FIXED(tempVehicle)
        SET_ENTITY_HEALTH(tempVehicle, 1000)
        SET_VEHICLE_ENGINE_HEALTH(tempVehicle, 1000)
        SET_VEHICLE_PETROL_TANK_HEALTH(tempVehicle, 1000)
		SET_VEHICLE_DIRT_LEVEL(tempVehicle, 0.0)
		
		IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
        AND NOT DECOR_EXIST_ON(tempVehicle, "Not_Allow_As_Saved_Veh")
			DECOR_SET_INT(tempVehicle, "Not_Allow_As_Saved_Veh", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
			PRINTLN("[BUNKER_VEHICLE] CREATE_FREEMODE_BUNKER_VEHICLE - set decorator Not_Allow_As_Saved_Veh on vehicle")
		ELSE
			PRINTLN("[BUNKER_VEHICLE] CREATE_FREEMODE_BUNKER_VEHICLE - Not_Allow_As_Saved_Veh already on vehicle or not registered as DECOR_TYPE_INT")
		ENDIF
		
		INT iDecoratorBS
		IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
			IF DECOR_EXIST_ON(tempVehicle, "MPBitset")
				iDecoratorBS = DECOR_GET_INT(tempVehicle, "MPBitset")
			ENDIF
			SET_BIT(iDecoratorBS, MP_DECORATOR_BS_VEHICLE_LEAVING_GARAGE)
			DECOR_SET_INT(tempVehicle, "MPBitset", iDecoratorBS)
			PRINTLN("[BUNKER_VEHICLE] CREATE_FREEMODE_BUNKER_VEHICLE - set decorator bit MP_DECORATOR_BS_VEHICLE_LEAVING_GARAGE on vehicle")
		ELSE
			PRINTLN("[BUNKER_VEHICLE] CREATE_FREEMODE_BUNKER_VEHICLE - decor MPBitset not registered as DECOR_TYPE_INT")
		ENDIF
		
		SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(g_sBunkerVehicle.NetID, PLAYER_ID(), FALSE)
		
		IF NETWORK_IS_IN_MP_CUTSCENE()
			SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
		ENDIF
	ELSE
		tempVehicle = NET_TO_VEH(g_sBunkerVehicle.NetID)
		
		IF TAKE_CONTROL_OF_NET_ID(g_sBunkerVehicle.NetID)
			IF (HAVE_VEHICLE_MODS_STREAMED_IN(tempVehicle)
			AND SET_VEHICLE_ON_GROUND_PROPERLY(tempVehicle))
				SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
				
				PRINTLN("[BUNKER_VEHICLE] CREATE_FREEMODE_BUNKER_VEHICLE - Vehicle is created")
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BLIP_SPRITE GET_BUNKER_VEHICLE_BLIP_SPRITE(BUNKER_VEHICLE_TYPE eVehType)
	SWITCH eVehType
		CASE eBUNKERVEHICLE_INVALID			
		CASE eBUNKERVEHICLE_MAX
		CASE eBUNKERVEHICLE_ZION3
		CASE eBUNKERVEHICLE_GRANGER
		CASE eBUNKERVEHICLE_MESA3
		CASE eBUNKERVEHICLE_CADDY3
			RETURN RADAR_TRACE_GANG_VEHICLE
			
		CASE eBUNKERVEHICLE_MANCHEZ
			RETURN RADAR_TRACE_GANG_BIKE
			
		CASE eBUNKERVEHICLE_VERUS
			RETURN RADAR_TRACE_MILITARY_QUAD
	ENDSWITCH
	
	RETURN RADAR_TRACE_GANG_VEHICLE
ENDFUNC

FUNC STRING GET_BUNKER_VEHICLE_BLIP_SPRITE_STRING(BUNKER_VEHICLE_TYPE eVehType)
	SWITCH eVehType
		CASE eBUNKERVEHICLE_INVALID			
		CASE eBUNKERVEHICLE_MAX
		CASE eBUNKERVEHICLE_ZION3
		CASE eBUNKERVEHICLE_GRANGER
		CASE eBUNKERVEHICLE_MESA3
		CASE eBUNKERVEHICLE_CADDY3
			RETURN "AG14_VEH"
			
		CASE eBUNKERVEHICLE_MANCHEZ
			RETURN "AG14_BIKE"
			
		CASE eBUNKERVEHICLE_VERUS
			RETURN "AG14_QUAD"
	ENDSWITCH
	
	RETURN "AG14_VEH"
ENDFUNC

PROC MAINTAIN_BUNKER_VEHICLE_SPAWNING()
	IF IS_BIT_SET(g_sBunkerVehicle.iBitSet, BUNKER_VEHICLE_GLOBAL_BS_ASSIGN_TO_MAIN_SCRIPT)
		VEHICLE_INDEX tempVeh
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF DOES_ENTITY_EXIST(tempVeh)
			AND IS_VEHICLE_DRIVEABLE(tempVeh)
			AND NETWORK_GET_ENTITY_IS_NETWORKED(tempVeh)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
					IF CAN_REGISTER_MISSION_VEHICLES(1)
						IF NOT IS_ENTITY_A_MISSION_ENTITY(tempVeh)
							SET_ENTITY_AS_MISSION_ENTITY(tempVeh, FALSE, TRUE)
							
							PRINTLN("[BUNKER_VEHICLE] MAINTAIN_BUNKER_VEHICLE_SPAWNING - not a mission entity, setting as one.")
						ENDIF
						
						IF IS_ENTITY_A_MISSION_ENTITY(tempVeh)
							IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(tempVeh)
								SET_ENTITY_AS_MISSION_ENTITY(tempVeh, FALSE, TRUE)
								
								PRINTLN("[BUNKER_VEHICLE] MAINTAIN_BUNKER_VEHICLE_SPAWNING - waiting for vehicle to belong to this script.")
							ENDIF
							
//							IF DECOR_IS_REGISTERED_AS_TYPE("Company_SUV", DECOR_TYPE_INT)
//								IF NOT DECOR_EXIST_ON(tempVeh, "Company_SUV")
//									DECOR_SET_INT(tempVeh, "Company_SUV", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
//								ENDIF
//							ENDIF
							
							SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(VEH_TO_NET(tempVeh), PLAYER_ID(), TRUE)
							
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh, TRUE)
							
							g_sBunkerVehicle.NetID = VEH_TO_NET(tempVeh)
							
							PRINTLN("[BUNKER_VEHICLE] MAINTAIN_BUNKER_VEHICLE_SPAWNING - new SUV registered.")
							
							CLEAR_BIT(g_sBunkerVehicle.iBitSet, BUNKER_VEHICLE_GLOBAL_BS_ASSIGN_TO_MAIN_SCRIPT)
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
				ENDIF
			ELSE
				PRINTLN("[BUNKER_VEHICLE] MAINTAIN_BUNKER_VEHICLE_SPAWNING - g_bAssignCompanySuvMainScript = FALSE 2")
				
				CLEAR_BIT(g_sBunkerVehicle.iBitSet, BUNKER_VEHICLE_GLOBAL_BS_ASSIGN_TO_MAIN_SCRIPT)
			ENDIF
		ELSE
			PRINTLN("[BUNKER_VEHICLE] MAINTAIN_BUNKER_VEHICLE_SPAWNING - g_bAssignCompanySuvMainScript = FALSE 1")
			
			CLEAR_BIT(g_sBunkerVehicle.iBitSet, BUNKER_VEHICLE_GLOBAL_BS_ASSIGN_TO_MAIN_SCRIPT)
		ENDIF
	ENDIF
	
	BOOL bDeleteBlip = FALSE
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(g_sBunkerVehicle.NetID)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(g_sBunkerVehicle.NetID)
		IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(g_sBunkerVehicle.NetID))
		AND IS_VEHICLE_EMPTY(NET_TO_VEH(g_sBunkerVehicle.NetID))
			IF NOT DOES_BLIP_EXIST(g_sBunkerVehicle.BlipID)
				g_sBunkerVehicle.BlipID = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(g_sBunkerVehicle.NetID))
				PRINTLN("[BUNKER_VEHICLE] MAINTAIN_BUNKER_VEHICLE_SPAWNING - Added blip")
				
				SET_BLIP_SPRITE(g_sBunkerVehicle.BlipID, GET_BUNKER_VEHICLE_BLIP_SPRITE(g_sBunkerVehicle.eVehType))
				SET_BLIP_COLOUR(g_sBunkerVehicle.BlipID, BLIP_COLOUR_HUDCOLOUR_YELLOW)
				SET_BLIP_NAME_FROM_TEXT_FILE(g_sBunkerVehicle.BlipID, "BUNKER_VEH")
				SET_BLIP_AS_SHORT_RANGE(g_sBunkerVehicle.BlipID, FALSE)
			ELSE
				IF IS_BIT_SET(g_sBunkerVehicle.iBitSet, BUNKER_VEHICLE_GLOBAL_BS_DELIVERED)
				AND NOT IS_BIT_SET(g_sBunkerVehicle.iBitSet, BUNKER_VEHICLE_GLOBAL_BS_FLASH_BLIP)
				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF NOT IS_BIT_SET(g_sBunkerVehicle.iBitSet, BUNKER_VEHICLE_GLOBAL_BS_SUPPRESS_BLIP_HELP)
						PRINT_HELP_WITH_STRING("AG14_VEH_DEL", GET_BUNKER_VEHICLE_BLIP_SPRITE_STRING(g_sBunkerVehicle.eVehType))
					ENDIF
					
					PRINTLN("[BUNKER_VEHICLE] MAINTAIN_BUNKER_VEHICLE_SPAWNING - Flashed blip")
					
					SET_BLIP_FLASHES(g_sBunkerVehicle.BlipID, TRUE)
					SET_BLIP_FLASH_TIMER(g_sBunkerVehicle.BlipID, 5000)
					
					SET_BIT(g_sBunkerVehicle.iBitSet, BUNKER_VEHICLE_GLOBAL_BS_FLASH_BLIP)
					CLEAR_BIT(g_sBunkerVehicle.iBitSet, BUNKER_VEHICLE_GLOBAL_BS_SUPPRESS_BLIP_HELP)
				ENDIF
			ENDIF
		ELSE
			bDeleteBlip = TRUE
		ENDIF
	ELSE
		bDeleteBlip = TRUE
	ENDIF
	
	IF bDeleteBlip
		IF DOES_BLIP_EXIST(g_sBunkerVehicle.BlipID)
			REMOVE_BLIP(g_sBunkerVehicle.BlipID)
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(g_sBunkerVehicle.NetID)
		IF SHOULD_BUNKER_VEHICLE_CLEANUP()
			PRINTLN("[BUNKER_VEHICLE] MAINTAIN_BUNKER_VEHICLE_SPAWNING - Cleaning up vehicle")
			CLEANUP_BUNKER_VEHICLE()
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_sBunkerVehicle.iBitSet, BUNKER_VEHICLE_GLOBAL_BS_CREATE_VEHICLE)
		IF NOT HAS_NET_TIMER_STARTED(g_sBunkerVehicle.Timer)
			START_NET_TIMER(g_sBunkerVehicle.Timer)
			g_sBunkerVehicle.iSpawnTime = GET_RANDOM_INT_IN_RANGE(BUNKER_VEHICLE_SPAWN_TIMER-2000, BUNKER_VEHICLE_SPAWN_TIMER+2000)
			PRINTLN("[BUNKER_VEHICLE] MAINTAIN_BUNKER_VEHICLE_SPAWNING - Started spawn timer. Will elapse in ", g_sTonyLimo.iSpawnTime, "ms")
		ELIF HAS_NET_TIMER_EXPIRED(g_sBunkerVehicle.Timer, g_sBunkerVehicle.iSpawnTime)
		OR IS_BIT_SET(g_sBunkerVehicle.iBitSet, BUNKER_VEHICLE_GLOBAL_BS_CREATE_IMMEDIATELY)
			IF CREATE_FREEMODE_BUNKER_VEHICLE()
				PRINTLN("[BUNKER_VEHICLE] MAINTAIN_BUNKER_VEHICLE_SPAWNING - Created vehicle")
				RESET_NET_TIMER(g_sBunkerVehicle.Timer)
				BOOL bSuppressHelp = IS_BIT_SET(g_sBunkerVehicle.iBitSet, BUNKER_VEHICLE_GLOBAL_BS_SUPPRESS_BLIP_HELP)
				g_sBunkerVehicle.iBitSet = 0
				SET_BIT(g_sBunkerVehicle.iBitSet, BUNKER_VEHICLE_GLOBAL_BS_DELIVERED)
				IF bSuppressHelp
					SET_BIT(g_sBunkerVehicle.iBitSet, BUNKER_VEHICLE_GLOBAL_BS_SUPPRESS_BLIP_HELP)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

CONST_INT BUNKER_DUNELOADER_BS_SPAWN_VEHICLE	0
CONST_INT BUNKER_DUNELOADER_BS_SPAWN_CRATE		1
CONST_INT BUNKER_DUNELOADER_BS_RUNNING_MISSION	2
CONST_INT BUNKER_DUNELOADER_BS_SEND_TEXT		3
CONST_INT BUNKER_DUNELOADER_BS_PRINT_HELP		4
CONST_INT BUNKER_DUNELOADER_BS_TIMER_INIT		5
CONST_INT BUNKER_DUNELOADER_BS_FACTORY_SHUTDOWN	6

FUNC BOOL IS_LOCAL_PLAYER_BUNKER_SHUTDOWN()
	RETURN IS_BIT_SET(GET_MP_INT_CHARACTER_STAT(MP_STAT_BKR_FACTORY_PROD_STOPPED), BUNKER_SAVE_SLOT)
ENDFUNC

FUNC BOOL IS_ANY_PLAYER_IN_MY_BUNKER()
	IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SPAWNING_IN_SIMPLE_INTERIOR)
	OR g_SpawnData.bSpawningInSimpleInterior
		RETURN FALSE
	ENDIF
	
	PLAYER_INDEX pTempPlayer
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		pTempPlayer = INT_TO_PLAYERINDEX(i)
		
		IF pTempPlayer != INVALID_PLAYER_INDEX()
		AND IS_NET_PLAYER_OK(pTempPlayer)
			IF IS_PLAYER_IN_BUNKER(pTempPlayer)
			AND GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(pTempPlayer) = PLAYER_ID()
			AND NOT IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(pTempPlayer)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SPAWN_BUNKER_DUNELOADER()
	IF DOES_PLAYER_OWN_A_BUNKER(PLAYER_ID())
	AND HAS_PLAYER_COMPLETED_FACTORY_SETUP_MISSION(PLAYER_ID(), GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), BUNKER_SAVE_SLOT))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_BUNKER_DUNELOADER()
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_FACTORY_SHUTDOWN)
	AND NOT IS_LOCAL_PLAYER_BUNKER_SHUTDOWN()
		SET_MP_INT_CHARACTER_STAT(MP_STAT_BUNKER_CRATE_COOLDOWN, GET_CLOUD_TIME_AS_INT())
		
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_FACTORY_SHUTDOWN)
	ENDIF
	
	IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_TIMER_INIT)
	AND g_sMPTunables.iSUM2_BUNKER_DUNELOADER_TIMER != 0
	#IF IS_DEBUG_BUILD
	AND NOT g_bOverrideBunkerDuneloaderTime
	#ENDIF
		g_iBunkerDuneloaderTime = g_sMPTunables.iSUM2_BUNKER_DUNELOADER_TIMER
		
		SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_TIMER_INIT)
	ENDIF
	
	IF SHOULD_SPAWN_BUNKER_DUNELOADER()
		IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_RUNNING_MISSION)
			IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SPAWN_VEHICLE)
				SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SPAWN_VEHICLE)
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SPAWN_VEHICLE)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SPAWN_VEHICLE)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SPAWN_CRATE)
		AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_FACTORY_SHUTDOWN)
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SUM22_BUNKER_DUNELOADER_TIMER_STARTED)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_BUNKER_CRATE_COOLDOWN, GET_CLOUD_TIME_AS_INT())
				
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SUM22_BUNKER_DUNELOADER_TIMER_STARTED, TRUE)
			ELIF (GET_CLOUD_TIME_AS_INT() - GET_MP_INT_CHARACTER_STAT(MP_STAT_BUNKER_CRATE_COOLDOWN)) >= (g_iBunkerDuneloaderTime / 1000)
			AND NOT IS_ANY_PLAYER_IN_MY_BUNKER()
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SUM22_BUNKER_DUNELOADER_TEXT_SENT)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SUM22_BUNKER_DUNELOADER_TEXT_SENT, TRUE)
					
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SEND_TEXT)
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_PRINT_HELP)
				ENDIF
				
				PRINT_TICKER("BUNK_DL_TICK")
				
				SET_MP_INT_CHARACTER_STAT(MP_STAT_BUNKER_CRATE_COOLDOWN, 0)
				
				SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SPAWN_CRATE)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SEND_TEXT)
			IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_MP_AGENT_14, "BUNK_DL_SMS", TXTMSG_UNLOCKED)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SEND_TEXT)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_PRINT_HELP)
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP_WITH_COLOURED_STRING("BUNK_DL_HELP1", "BUNK_DL_HELP1a", GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
				
				SET_SIMPLE_INTERIOR_GLOBAL_BIT(SI_BS_FlashBlips, GET_OWNED_BUNKER_SIMPLE_INTERIOR(PLAYER_ID()))
				
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_PRINT_HELP)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SPAWN_VEHICLE)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SPAWN_VEHICLE)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_bSpawnBunkerDuneloaderCrate
		IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SPAWN_CRATE)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_BUNKER_CRATE_COOLDOWN, 0)
			
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SPAWN_CRATE)
			
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SUM22_BUNKER_DUNELOADER_TEXT_SENT)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SUM22_BUNKER_DUNELOADER_TEXT_SENT, TRUE)
				
				SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SEND_TEXT)
				SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_PRINT_HELP)
			ENDIF
			
			PRINT_TICKER("BUNK_DL_TICK")
		ENDIF
		
		g_bSpawnBunkerDuneloaderCrate = FALSE
	ENDIF
	
	IF g_bClearBunkerDuneloaderCrate
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SPAWN_CRATE)
		
		SET_MP_INT_CHARACTER_STAT(MP_STAT_BUNKER_CRATE_COOLDOWN, GET_CLOUD_TIME_AS_INT())
		
		g_bClearBunkerDuneloaderCrate = FALSE
	ENDIF
	
	IF g_bClearBunkerDuneloaderStats
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SUM22_BUNKER_DUNELOADER_TEXT_SENT, FALSE)
		
		g_bClearBunkerDuneloaderStats = FALSE
	ENDIF
	
	IF g_bOverrideBunkerDuneloaderTime
		IF g_iBunkerDuneloaderTime != g_iBunkerDuneloaderOverrideTime
			g_iBunkerDuneloaderTime = g_iBunkerDuneloaderOverrideTime * 1000
			
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_TIMER_INIT)
		ENDIF
	ENDIF
	#ENDIF
	
	IF IS_LOCAL_PLAYER_BUNKER_SHUTDOWN()
		SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_FACTORY_SHUTDOWN)
	ENDIF
ENDPROC

#ENDIF	// FEATURE_DLC_1_2022


#IF FEATURE_GEN9_EXCLUSIVE
FUNC BOOL REQUEST_MP_TUTORIAL_PV(VECTOR vCoords, FLOAT fHeading, BOOL bForcePosition = FALSE, BOOL bForceRadioOff = FALSE)
	IF NOT HAS_PERSONAL_VEHICLE_SPAWN_BEEN_REQUESTED()
	AND NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_JUST_CREATED)
	AND NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_BEING_CREATED)
	AND NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
	
		BOOL bIsBiker = GET_CHARACTER_CAREER() = CHARACTER_CAREER_BIKER
		BOOL bIsBikePV = FALSE
		
		INT iPVSlot
		INT iPVCost = -1
		
		VEHICLE_VALUE_DATA sVehicleValueData
		
		INT i
		REPEAT MAX_MP_SAVED_VEHICLES i
			IF g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
			AND GET_VEHICLE_VALUE(sVehicleValueData, g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel)
			AND IS_VEHICLE_MODEL_VALID_FOR_INTRO(g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel)
			AND IS_MODEL_VALID_FOR_PERSONAL_VEHICLE(g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel)
			AND IS_VEHICLE_AVAILABLE_FOR_GAME(g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel, TRUE)
				IF NOT bIsBiker
					IF sVehicleValueData.iStandardPrice > iPVCost
						iPVSlot = i
						iPVCost = sVehicleValueData.iStandardPrice
					ENDIF
				ELSE
					BOOL bIsModelBike = FALSE
					
					IF IS_THIS_MODEL_A_BIKE(g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel)
					OR IS_THIS_MODEL_A_QUADBIKE(g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel)
						bIsModelBike = TRUE
					ENDIF
					
					IF sVehicleValueData.iStandardPrice > iPVCost
					OR (NOT bIsBikePV AND bIsModelBike)
						IF NOT bIsBikePV
							bIsBikePV = bIsModelBike
							
							iPVSlot = i
							iPVCost = sVehicleValueData.iStandardPrice
						ELIF bIsModelBike
							iPVSlot = i
							iPVCost = sVehicleValueData.iStandardPrice
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		PRINTLN("[personal_vehicle] REQUEST_MP_TUTORIAL_PV - Requesting PV from slot ", iPVSlot)
		
		IF bForcePosition
			SET_BIT(g_TransitionSessionNonResetVars.MissionPVBitset, ciSPAWN_PV_IN_ORIGINAL_LOC)
			
			g_TransitionSessionNonResetVars.PVStartLocation = vCoords
			g_TransitionSessionNonResetVars.PVStartHeading = fHeading
		ELSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPvSpawnPosOverride = vCoords
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPvSpawnHeadingOverride = fHeading
		ENDIF
		
		CLEANUP_HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(FALSE)
		
		MPGlobalsAmbience.bRequestedByOfficePA = TRUE
		MPGlobalsAmbience.iPVSlotOfficePA = iPVSlot
		
		MPGlobalsAmbience.bForceDeletePV = TRUE
	ELSE
		IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_CREATED)
		AND NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
			IF bForceRadioOff
			AND g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed >= 0
			AND g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed < MAX_MP_SAVED_VEHICLES
				g_MpSavedVehicles[g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed].tlRadioStationName = "OFF"
				
				IF DOES_ENTITY_EXIST(PERSONAL_VEHICLE_ID())
				AND IS_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_ID())
					SET_VEH_RADIO_STATION(PERSONAL_VEHICLE_ID(), "OFF")
				ENDIF
			ENDIF
			
			RETURN TRUE
		ELSE
			PRINTLN("[personal_vehicle] REQUEST_MP_TUTORIAL_PV - waiting for vehicle to be created.")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_FORCE_RESPAWN_PV()
	IF HAS_FORCED_PV_RESPAWN_TRIGGERED()
		IF NOT HAS_NET_TIMER_STARTED(g_stForcedPVRespawnDelay)
			START_NET_TIMER(g_stForcedPVRespawnDelay)
		ELIF HAS_NET_TIMER_EXPIRED(g_stForcedPVRespawnDelay, 5000)
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				VECTOR vCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				
				IF REQUEST_MP_TUTORIAL_PV(vCoords, 0.0)
					SET_TRIGGER_RESPAWN_PV(FALSE)
					
					SET_HAS_FORCED_PV_RESPAWN_FINISHED(TRUE)
					
					RESET_NET_TIMER(g_stForcedPVRespawnDelay)
				ENDIF
			ENDIF
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF 	// FEATURE_GEN9

#IF FEATURE_DLC_2_2022
// url:bugstar:7878448 - Xmas 2022 - Please can we set up a scripted menu when phoning Dax (the Juggalo Boss)?

FUNC BOOL SHOULD_JUGALLO_BOSS_VEHICLE_CLEANUP()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
		PRINTLN("SHOULD_JUGALLO_BOSS_VEHICLE_CLEANUP - Player is not OK.")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(g_sJugalloBossVehicle.NetID))
		PRINTLN("SHOULD_JUGALLO_BOSS_VEHICLE_CLEANUP - Vehicle is destroyed.")
		RETURN TRUE
	ENDIF
	
	IF NOT (VDIST2(GET_ENTITY_COORDS(NET_TO_VEH(g_sJugalloBossVehicle.NetID), FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) <= 1000 * 1000)
		PRINTLN("SHOULD_JUGALLO_BOSS_VEHICLE_CLEANUP - Out of range.")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		PRINTLN("SHOULD_JUGALLO_BOSS_VEHICLE_CLEANUP - Player is in property.")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		PRINTLN("SHOULD_JUGALLO_BOSS_VEHICLE_CLEANUP - Player is entering/exiting property.")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("SHOULD_JUGALLO_BOSS_VEHICLE_CLEANUP - Player is entering simple interior.")
		RETURN TRUE
	ENDIF
	
	IF g_sJugalloBossVehicle.bForceCleanup
		PRINTLN("SHOULD_JUGALLO_BOSS_VEHICLE_CLEANUP - g_sJugalloBossVehicle.bForceCleanup = TRUE")
		
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

PROC REMOVE_JUGALLO_BOSS_VEHICLE_BLIP()
	IF DOES_BLIP_EXIST(g_sJugalloBossVehicle.BlipID)
		REMOVE_BLIP(g_sJugalloBossVehicle.BlipID)
		PRINTLN("[JUGALLO_BOSS_VEHICLE] REMOVE_JUGALLO_BOSS_VEHICLE_BLIP - removed blip.")
	ENDIF
ENDPROC

PROC CLEANUP_JUGALLO_BOSS_VEHICLE(BOOL bDelete = FALSE)
	IF g_sJugalloBossVehicle.iBitSet = 0
		EXIT
	ENDIF
	
	PRINTLN("[JUGALLO_BOSS_VEHICLE] CLEANUP_JUGALLO_BOSS_VEHICLE - called.")
	DEBUG_PRINTCALLSTACK()
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(g_sJugalloBossVehicle.NetID)
	
		IF bDelete
			REMOVE_JUGALLO_BOSS_VEHICLE_BLIP()
			DELETE_NET_ID(g_sJugalloBossVehicle.NetID)
			PRINTLN("[JUGALLO_BOSS_VEHICLE] CLEANUP_JUGALLO_BOSS_VEHICLE - deleted.")
		ELSE
			IF NOT TAKE_CONTROL_OF_NET_ID(g_sJugalloBossVehicle.NetID)
				PRINTLN("[JUGALLO_BOSS_VEHICLE] CLEANUP_JUGALLO_BOSS_VEHICLE - waiting to take control of the vehicle...")
				EXIT
			ENDIF
							
			BROADCAST_LEAVE_VEHICLE(ALL_PLAYERS_IN_VEHICLE(NET_TO_VEH(g_sJugalloBossVehicle.NetID)), TRUE, 5.0, 1)
		
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(NET_TO_VEH(g_sJugalloBossVehicle.NetID), TRUE)
		
			REMOVE_JUGALLO_BOSS_VEHICLE_BLIP()
			CLEANUP_NET_ID(g_sJugalloBossVehicle.NetID)
			PRINTLN("[JUGALLO_BOSS_VEHICLE] CLEANUP_JUGALLO_BOSS_VEHICLE - cleaned up.")
		ENDIF
		
	ENDIF
	
	g_sJugalloBossVehicle.iBitSet = 0
	g_sJugalloBossVehicle.NetID = NULL
	g_sJugalloBossVehicle.bForceCleanup = FALSE
	
ENDPROC

FUNC BOOL CREATE_FREEMODE_JUGALLO_BOSS_VEHICLE()

	MODEL_NAMES eModel = GET_JUGALLO_BOSS_VEHICLE_MODEL_NAME()

	IF NOT IS_BIT_SET(g_sJugalloBossVehicle.iBitSet, JUGALLO_BOSS_VEHICLE_GLOBAL_BS_FOUND_SAFE_COORDS)
		IF CHECK_AMBIENT_NET_VEHICLE_CAN_BE_CREATED_WITH_MODEL(eModel)
			VEHICLE_SPAWN_LOCATION_PARAMS sParams
			sParams.fMinDistFromCoords = 10
			sParams.fMaxDistance = 1000
			sParams.bIgnoreCustomNodesForArea = TRUE
			
			IF NOT HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<0.0, 0.0, 0.0>>, eModel, TRUE, g_sJugalloBossVehicle.vSafeLocation, g_sJugalloBossVehicle.fSafeHeading, sParams)
				PRINTLN("[JUGALLO_BOSS_VEHICLE] CREATE_FREEMODE_JUGALLO_BOSS_VEHICLE - Waiting for find safe coords...")
				RETURN FALSE
			ENDIF
			
			IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(g_sJugalloBossVehicle.vSafeLocation)
				PRINTLN("[JUGALLO_BOSS_VEHICLE] CREATE_FREEMODE_JUGALLO_BOSS_VEHICLE - g_sJugalloBossVehicle.vSafeLocation is not safe for net entity creation.")
				RETURN FALSE
			ENDIF
			
			PRINTLN("[JUGALLO_BOSS_VEHICLE] CREATE_FREEMODE_JUGALLO_BOSS_VEHICLE - Found safe coord for bunker vehicle at ", g_sJugalloBossVehicle.vSafeLocation)
			SET_BIT(g_sJugalloBossVehicle.iBitSet, JUGALLO_BOSS_VEHICLE_GLOBAL_BS_FOUND_SAFE_COORDS)
		ELSE
			PRINTLN("[JUGALLO_BOSS_VEHICLE] CREATE_FREEMODE_JUGALLO_BOSS_VEHICLE - Cannot currently create net vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(eModel))
			RETURN FALSE
		ENDIF
	ENDIF
	
	VEHICLE_INDEX tempVehicle
		
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(g_sJugalloBossVehicle.NetID)
	OR NOT IS_NET_VEHICLE_DRIVEABLE(g_sJugalloBossVehicle.NetID)
		
		IF NETWORK_IS_IN_MP_CUTSCENE()
			SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
		ENDIF
		
		PRINTLN("[JUGALLO_BOSS_VEHICLE] CREATE_FREEMODE_JUGALLO_BOSS_VEHICLE - Creating vehicle")
		
		tempVehicle = CREATE_VEHICLE(eModel, g_sJugalloBossVehicle.vSafeLocation, g_sJugalloBossVehicle.fSafeHeading, TRUE, FALSE)
		
		g_sJugalloBossVehicle.NetID = VEH_TO_NET(tempVehicle)
		
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVehicle, TRUE)
		
		VEHICLE_SETUP_STRUCT_MP sData
		
		SWITCH eModel // TODO: Juggalo Boss - Define Vehicle Setup
			CASE MANCHEZ
				sData.VehicleSetup.eModel = MANCHEZ
				sData.VehicleSetup.iColour1 = 52
				sData.VehicleSetup.iColour2 = 5
				sData.VehicleSetup.iColourExtra1 = 59
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWheelType = 6
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
				sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
				sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
				sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
				sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
				sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
			BREAK
		ENDSWITCH
		
		SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
		SET_VEHICLE_SETUP_MP(tempVehicle, sData, DEFAULT, DEFAULT, TRUE)
		
		NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(tempVehicle, TRUE)
		SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(tempVehicle, FALSE)
		SET_VEHICLE_FULLBEAM(tempVehicle, FALSE)
		SET_VEHICLE_LIGHTS(tempVehicle, FORCE_VEHICLE_LIGHTS_OFF)
		SET_VEHICLE_FIXED(tempVehicle)
        SET_ENTITY_HEALTH(tempVehicle, 1000)
        SET_VEHICLE_ENGINE_HEALTH(tempVehicle, 1000)
        SET_VEHICLE_PETROL_TANK_HEALTH(tempVehicle, 1000)
		SET_VEHICLE_DIRT_LEVEL(tempVehicle, 0.0)
		
		IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
        AND NOT DECOR_EXIST_ON(tempVehicle, "Not_Allow_As_Saved_Veh")
			DECOR_SET_INT(tempVehicle, "Not_Allow_As_Saved_Veh", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
			PRINTLN("[JUGALLO_BOSS_VEHICLE] CREATE_FREEMODE_JUGALLO_BOSS_VEHICLE - set decorator Not_Allow_As_Saved_Veh on vehicle")
		ELSE
			PRINTLN("[JUGALLO_BOSS_VEHICLE] CREATE_FREEMODE_JUGALLO_BOSS_VEHICLE - Not_Allow_As_Saved_Veh already on vehicle or not registered as DECOR_TYPE_INT")
		ENDIF
		
		INT iDecoratorBS
		IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
			IF DECOR_EXIST_ON(tempVehicle, "MPBitset")
				iDecoratorBS = DECOR_GET_INT(tempVehicle, "MPBitset")
			ENDIF
			SET_BIT(iDecoratorBS, MP_DECORATOR_BS_VEHICLE_LEAVING_GARAGE)
			DECOR_SET_INT(tempVehicle, "MPBitset", iDecoratorBS)
			PRINTLN("[JUGALLO_BOSS_VEHICLE] CREATE_FREEMODE_JUGALLO_BOSS_VEHICLE - set decorator bit MP_DECORATOR_BS_VEHICLE_LEAVING_GARAGE on vehicle")
		ELSE
			PRINTLN("[JUGALLO_BOSS_VEHICLE] CREATE_FREEMODE_JUGALLO_BOSS_VEHICLE - decor MPBitset not registered as DECOR_TYPE_INT")
		ENDIF
		
		SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(g_sJugalloBossVehicle.NetID, PLAYER_ID(), FALSE)
		
		IF NETWORK_IS_IN_MP_CUTSCENE()
			SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
		ENDIF
	ELSE
		tempVehicle = NET_TO_VEH(g_sJugalloBossVehicle.NetID)
		
		IF TAKE_CONTROL_OF_NET_ID(g_sJugalloBossVehicle.NetID)
			IF (HAVE_VEHICLE_MODS_STREAMED_IN(tempVehicle)
			AND SET_VEHICLE_ON_GROUND_PROPERLY(tempVehicle))
				SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
				
				PRINTLN("[JUGALLO_BOSS_VEHICLE] CREATE_FREEMODE_JUGALLO_BOSS_VEHICLE - Vehicle is created")
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BLIP_SPRITE GET_JUGALLO_BOSS_VEHICLE_BLIP_SPRITE()
	RETURN RADAR_TRACE_GANG_VEHICLE
ENDFUNC

FUNC STRING GET_JUGALLO_BOSS_VEHICLE_BLIP_SPRITE_STRING()
	RETURN "DAX_VEH"
ENDFUNC

PROC MAINTAIN_JUGALLO_BOSS_VEHICLE_SPAWNING()
	IF IS_BIT_SET(g_sJugalloBossVehicle.iBitSet, JUGALLO_BOSS_VEHICLE_GLOBAL_BS_ASSIGN_TO_MAIN_SCRIPT)
		VEHICLE_INDEX tempVeh
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF DOES_ENTITY_EXIST(tempVeh)
			AND IS_VEHICLE_DRIVEABLE(tempVeh)
			AND NETWORK_GET_ENTITY_IS_NETWORKED(tempVeh)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
					IF CAN_REGISTER_MISSION_VEHICLES(1)
						IF NOT IS_ENTITY_A_MISSION_ENTITY(tempVeh)
							SET_ENTITY_AS_MISSION_ENTITY(tempVeh, FALSE, TRUE)
							
							PRINTLN("[JUGALLO_BOSS_VEHICLE] MAINTAIN_JUGALLO_BOSS_VEHICLE_SPAWNING - not a mission entity, setting as one.")
						ENDIF
						
						IF IS_ENTITY_A_MISSION_ENTITY(tempVeh)
							IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(tempVeh)
								SET_ENTITY_AS_MISSION_ENTITY(tempVeh, FALSE, TRUE)
								
								PRINTLN("[JUGALLO_BOSS_VEHICLE] MAINTAIN_JUGALLO_BOSS_VEHICLE_SPAWNING - waiting for vehicle to belong to this script.")
							ENDIF
							
							SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(VEH_TO_NET(tempVeh), PLAYER_ID(), TRUE)
							
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh, TRUE)
							
							g_sJugalloBossVehicle.NetID = VEH_TO_NET(tempVeh)
							
							PRINTLN("[JUGALLO_BOSS_VEHICLE] MAINTAIN_JUGALLO_BOSS_VEHICLE_SPAWNING - new SUV registered.")
							
							CLEAR_BIT(g_sJugalloBossVehicle.iBitSet, JUGALLO_BOSS_VEHICLE_GLOBAL_BS_ASSIGN_TO_MAIN_SCRIPT)
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
				ENDIF
			ELSE
				PRINTLN("[JUGALLO_BOSS_VEHICLE] MAINTAIN_JUGALLO_BOSS_VEHICLE_SPAWNING - g_bAssignCompanySuvMainScript = FALSE 2")
				
				CLEAR_BIT(g_sJugalloBossVehicle.iBitSet, JUGALLO_BOSS_VEHICLE_GLOBAL_BS_ASSIGN_TO_MAIN_SCRIPT)
			ENDIF
		ELSE
			PRINTLN("[JUGALLO_BOSS_VEHICLE] MAINTAIN_JUGALLO_BOSS_VEHICLE_SPAWNING - g_bAssignCompanySuvMainScript = FALSE 1")
			
			CLEAR_BIT(g_sJugalloBossVehicle.iBitSet, JUGALLO_BOSS_VEHICLE_GLOBAL_BS_ASSIGN_TO_MAIN_SCRIPT)
		ENDIF
	ENDIF
	
	BOOL bDeleteBlip = FALSE
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(g_sJugalloBossVehicle.NetID)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(g_sJugalloBossVehicle.NetID)
		IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(g_sJugalloBossVehicle.NetID))
		AND IS_VEHICLE_EMPTY(NET_TO_VEH(g_sJugalloBossVehicle.NetID))
			IF NOT DOES_BLIP_EXIST(g_sJugalloBossVehicle.BlipID)
				g_sJugalloBossVehicle.BlipID = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(g_sJugalloBossVehicle.NetID))
				PRINTLN("[JUGALLO_BOSS_VEHICLE] MAINTAIN_JUGALLO_BOSS_VEHICLE_SPAWNING - Added blip")
				
				SET_BLIP_SPRITE(g_sJugalloBossVehicle.BlipID, GET_JUGALLO_BOSS_VEHICLE_BLIP_SPRITE())
				SET_BLIP_COLOUR(g_sJugalloBossVehicle.BlipID, BLIP_COLOUR_HUDCOLOUR_YELLOW)
				SET_BLIP_NAME_FROM_TEXT_FILE(g_sJugalloBossVehicle.BlipID, "BUNKER_VEH")
				SET_BLIP_AS_SHORT_RANGE(g_sJugalloBossVehicle.BlipID, FALSE)
			ELSE
				IF IS_BIT_SET(g_sJugalloBossVehicle.iBitSet, JUGALLO_BOSS_VEHICLE_GLOBAL_BS_DELIVERED)
				AND NOT IS_BIT_SET(g_sJugalloBossVehicle.iBitSet, JUGALLO_BOSS_VEHICLE_GLOBAL_BS_FLASH_BLIP)
				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF NOT IS_BIT_SET(g_sJugalloBossVehicle.iBitSet, JUGALLO_BOSS_VEHICLE_GLOBAL_BS_SUPPRESS_BLIP_HELP)
						PRINT_HELP_WITH_STRING("DAX_VEH_DEL", GET_JUGALLO_BOSS_VEHICLE_BLIP_SPRITE_STRING())
					ENDIF
					
					PRINTLN("[JUGALLO_BOSS_VEHICLE] MAINTAIN_JUGALLO_BOSS_VEHICLE_SPAWNING - Flashed blip")
					
					SET_BLIP_FLASHES(g_sJugalloBossVehicle.BlipID, TRUE)
					SET_BLIP_FLASH_TIMER(g_sJugalloBossVehicle.BlipID, 5000)
					
					SET_BIT(g_sJugalloBossVehicle.iBitSet, JUGALLO_BOSS_VEHICLE_GLOBAL_BS_FLASH_BLIP)
					CLEAR_BIT(g_sJugalloBossVehicle.iBitSet, JUGALLO_BOSS_VEHICLE_GLOBAL_BS_SUPPRESS_BLIP_HELP)
				ENDIF
			ENDIF
		ELSE
			bDeleteBlip = TRUE
		ENDIF
	ELSE
		bDeleteBlip = TRUE
	ENDIF
	
	IF bDeleteBlip
		IF DOES_BLIP_EXIST(g_sJugalloBossVehicle.BlipID)
			REMOVE_BLIP(g_sJugalloBossVehicle.BlipID)
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(g_sJugalloBossVehicle.NetID)
		IF SHOULD_JUGALLO_BOSS_VEHICLE_CLEANUP()
			PRINTLN("[JUGALLO_BOSS_VEHICLE] MAINTAIN_JUGALLO_BOSS_VEHICLE_SPAWNING - Cleaning up vehicle")
			CLEANUP_JUGALLO_BOSS_VEHICLE()
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_sJugalloBossVehicle.iBitSet, JUGALLO_BOSS_VEHICLE_GLOBAL_BS_CREATE_VEHICLE)
		IF NOT HAS_NET_TIMER_STARTED(g_sJugalloBossVehicle.Timer)
			START_NET_TIMER(g_sJugalloBossVehicle.Timer)
			g_sJugalloBossVehicle.iSpawnTime = GET_RANDOM_INT_IN_RANGE(JUGALLO_BOSS_VEHICLE_SPAWN_TIMER-2000, JUGALLO_BOSS_VEHICLE_SPAWN_TIMER+2000)
			PRINTLN("[JUGALLO_BOSS_VEHICLE] MAINTAIN_JUGALLO_BOSS_VEHICLE_SPAWNING - Started spawn timer. Will elapse in ", g_sTonyLimo.iSpawnTime, "ms")
		ELIF HAS_NET_TIMER_EXPIRED(g_sJugalloBossVehicle.Timer, g_sJugalloBossVehicle.iSpawnTime)
		OR IS_BIT_SET(g_sJugalloBossVehicle.iBitSet, JUGALLO_BOSS_VEHICLE_GLOBAL_BS_CREATE_IMMEDIATELY)
			IF CREATE_FREEMODE_JUGALLO_BOSS_VEHICLE()
				PRINTLN("[JUGALLO_BOSS_VEHICLE] MAINTAIN_JUGALLO_BOSS_VEHICLE_SPAWNING - Created vehicle")
				RESET_NET_TIMER(g_sJugalloBossVehicle.Timer)
				BOOL bSuppressHelp = IS_BIT_SET(g_sJugalloBossVehicle.iBitSet, JUGALLO_BOSS_VEHICLE_GLOBAL_BS_SUPPRESS_BLIP_HELP)
				g_sJugalloBossVehicle.iBitSet = 0
				SET_BIT(g_sJugalloBossVehicle.iBitSet, JUGALLO_BOSS_VEHICLE_GLOBAL_BS_DELIVERED)
				IF bSuppressHelp
					SET_BIT(g_sJugalloBossVehicle.iBitSet, JUGALLO_BOSS_VEHICLE_GLOBAL_BS_SUPPRESS_BLIP_HELP)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#ENDIF // FEATURE_DLC_2_2022

//EOF ////////////////
