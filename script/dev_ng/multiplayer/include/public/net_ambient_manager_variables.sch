
// ====================================================================================
// ====================================================================================
//
// Name:        net_ambient_manager_variables.sch
// Description: Declaration of all MP ambient manager variables
// Written By:  David Gentles
//
// ====================================================================================
// ====================================================================================

//USING "globals.sch"
//USING "net_hud_activating.sch"
//USING "commands_decorator.sch"
USING "types.sch"

// =================
// 	MP Ambient Manager
// =================

//iBitset
CONST_INT MPAM_BS_INITIALISED						0
CONST_INT MPAM_BS_IN_NET_SCRIPT_INTENSIVE_AREA		1


// =================
// 	PRIVATE
// =================
ENUM PRIVATE_MPAM_ENTITY_TYPES
	PRIV_MPAM_PED = 0,
	PRIV_MPAM_VEH,
	PRIV_MPAM_OBJ
ENDENUM
