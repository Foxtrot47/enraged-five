USING "globals.sch"
USING "net_include.sch"
USING "net_ambience.sch"
USING "net_vehicle_mod_help_utility.sch"


ENUM ARENA_VEHICLE_MOD_HELP
	MOD_HELP_BOOST,
	MOD_HELP_SHUNT,
	MOD_HELP_MINE,
	MOD_HELP_JUMP,
	MOD_HELP_FLAMETHROWER,
	MOD_HELP_REMOTE_FLAMETHROWER,
	MOD_HELP_TOMBSTONE
ENDENUM

CONST_INT VEHICLE_HELP_PRINTED_RATLOADER 	0
CONST_INT VEHICLE_HELP_PRINTED_RATLOADER2 	1
CONST_INT VEHICLE_HELP_PRINTED_GLENDALE 	2
CONST_INT VEHICLE_HELP_PRINTED_SLAMVAN 		3
CONST_INT VEHICLE_HELP_PRINTED_DOMINATOR 	4
CONST_INT VEHICLE_HELP_PRINTED_DOMINATOR2 	5
CONST_INT VEHICLE_HELP_PRINTED_IMPALER 		6
CONST_INT VEHICLE_HELP_PRINTED_ISSI3 		7
CONST_INT VEHICLE_HELP_PRINTED_GARGOYLE 	8
CONST_INT VEHICLE_HELP_PRINTED_RC_BANDITO 	9
#IF FEATURE_CASINO_HEIST
CONST_INT VEHICLE_HELP_PRINTED_RC_TANK 		10
#ENDIF

STRUCT ARENA_VEHICLE_HELP_TEXT_STRUCT
	
	INT iModHelpTextPrintedBS = 0
	INT iVehicleHelpTextBS = 0
	
ENDSTRUCT

ENUM ENGINE_BAY_2_ARENA_MODS_ENUM
	MOD_BOOST_1 = 0,
	MOD_BOOST_2,
	MOD_BOOST_3,
	MOD_SHUNT
ENDENUM

ENUM ENGINE_BAY_3_ARENA_MODS_ENUM
	MOD_JUMP_1 = 0,
	MOD_JUMP_2,
	MOD_JUMP_3
ENDENUM

ENUM MOD_ROOF_ARENA_MODS_ENUM
	MOD_REMOTE_FLAMETHROWER = 0
ENDENUM


/// PURPOSE:
///    Displays help text for turning vehicles into arena contenders
///    for a set duration, will only display help text once per boot!
/// RETURNS:
///   FALSE if message has already been displayed once since boot
FUNC BOOL DISPLAY_HELP_TRANSFORM_ARENA_CONTENDER(INT& iHelpTextBS, INT iBitToSet)
	
	// If message has already been displayed leave
	IF IS_BIT_SET(iHelpTextBS, iBitToSet)
		RETURN FALSE
	ENDIF
	
	// Print help only once
	PRINT_HELP("AVMH_TRANSFORM_CAR")
	SET_BIT(iHelpTextBS, iBitToSet)	
	RETURN TRUE
	
ENDFUNC


/// PURPOSE:
///    Trys to display transform arena contender help text if the
///    vehicle model passed in requires it
/// PARAMS:
///    eVehicleModel - the model to check
/// RETURNS:
///    TRUE if the help text has been displayed
FUNC BOOL TRY_DISPLAY_HELP_TRANSFORM_ARENA_CONTENDER(INT& iHelpBS, MODEL_NAMES eVehicleModel)
	
	IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
	OR FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) != -1
	OR IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) != VS_DRIVER
		RETURN FALSE
	ENDIF
	
	// If model matches convertable contender vehicles
	SWITCH eVehicleModel
		CASE RATLOADER
			RETURN DISPLAY_HELP_TRANSFORM_ARENA_CONTENDER(iHelpBS, 
			VEHICLE_HELP_PRINTED_RATLOADER)
		CASE RATLOADER2
			RETURN DISPLAY_HELP_TRANSFORM_ARENA_CONTENDER(iHelpBS,
			VEHICLE_HELP_PRINTED_RATLOADER2)
		CASE GLENDALE
			RETURN DISPLAY_HELP_TRANSFORM_ARENA_CONTENDER(iHelpBS,
			VEHICLE_HELP_PRINTED_GLENDALE)
		CASE SLAMVAN
			RETURN DISPLAY_HELP_TRANSFORM_ARENA_CONTENDER(iHelpBS,
			VEHICLE_HELP_PRINTED_SLAMVAN)
		CASE DOMINATOR
			RETURN DISPLAY_HELP_TRANSFORM_ARENA_CONTENDER(iHelpBS,
			VEHICLE_HELP_PRINTED_DOMINATOR)
		CASE DOMINATOR2
			RETURN DISPLAY_HELP_TRANSFORM_ARENA_CONTENDER(iHelpBS,
			VEHICLE_HELP_PRINTED_DOMINATOR2)
		CASE IMPALER
			RETURN DISPLAY_HELP_TRANSFORM_ARENA_CONTENDER(iHelpBS,
			VEHICLE_HELP_PRINTED_IMPALER)
		CASE ISSI3
			RETURN DISPLAY_HELP_TRANSFORM_ARENA_CONTENDER(iHelpBS,
			VEHICLE_HELP_PRINTED_ISSI3)
		CASE GARGOYLE
			RETURN DISPLAY_HELP_TRANSFORM_ARENA_CONTENDER(iHelpBS,
			VEHICLE_HELP_PRINTED_GARGOYLE)
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC


/// PURPOSE:
///    Adds boost help text to current help message if required
/// PARAMS:
///    sInstance - arena vehicle help instance
/// RETURNS:
///    The help text label if the help text can and should be displayed, returns "" otherwise
FUNC STRING TRY_GET_BOOST_MOD_HELP(INT& iArenaHelpBS[])
	
	// If has any boost mod
	IF CAN_PRINT_MOD_HELP_TEXT(iArenaHelpBS, MOD_ENGINEBAY2, ENUM_TO_INT(MOD_BOOST_1),
	ENUM_TO_INT(MOD_HELP_BOOST)) OR
	CAN_PRINT_MOD_HELP_TEXT(iArenaHelpBS, MOD_ENGINEBAY2, ENUM_TO_INT(MOD_BOOST_2),
	ENUM_TO_INT(MOD_HELP_BOOST)) OR
	CAN_PRINT_MOD_HELP_TEXT(iArenaHelpBS, MOD_ENGINEBAY2, ENUM_TO_INT(MOD_BOOST_3),
	ENUM_TO_INT(MOD_HELP_BOOST))
		
		RETURN "AVMH_NITROS"
	ENDIF
	
	RETURN ""
	
ENDFUNC


/// PURPOSE:
///    Adds mine help text if required
/// PARAMS:
///    sInstance - arena vehicle help instance
/// RETURNS:
///    The help text label if the help text can and should be displayed, returns "" otherwise
FUNC STRING TRY_GET_MINE_HELP(INT& iArenaHelpBS[])

	IF NOT CAN_VEHICLE_HAVE_MINES(GET_PLAYERS_CURRENT_VEHICLE()) 
	OR GET_PLAYERS_CURRENT_VEHICLE_MODEL() = RCBANDITO
		RETURN ""
	ENDIF	
	
	// If any mine mod is active
	IF CAN_PRINT_MODS_HELP_TEXT(iArenaHelpBS, MOD_WING_R, 0,
	ENUM_TO_INT(MOD_HELP_MINE)) 
		
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			RETURN "AVMH_MINE_PC"
		ENDIF
		
		RETURN "AVMH_MINE"	
	ENDIF
	
	RETURN ""
	
ENDFUNC


/// PURPOSE:
///    Adds jump help text if required
/// PARAMS:
///    sInstance - arena vehicle help instance
/// RETURNS:
///    The help text label if the help text can and should be displayed, returns "" otherwise
FUNC STRING TRY_GET_JUMP_HELP(INT& iArenaHelpBS[])
	
	// If any jump mod active
	IF CAN_PRINT_MOD_HELP_TEXT(iArenaHelpBS, MOD_ENGINEBAY3, ENUM_TO_INT(MOD_JUMP_1),
	ENUM_TO_INT(MOD_HELP_JUMP)) OR
	CAN_PRINT_MOD_HELP_TEXT(iArenaHelpBS, MOD_ENGINEBAY3, ENUM_TO_INT(MOD_JUMP_2),
	ENUM_TO_INT(MOD_HELP_JUMP)) OR
	CAN_PRINT_MOD_HELP_TEXT(iArenaHelpBS, MOD_ENGINEBAY3, ENUM_TO_INT(MOD_JUMP_3),
	ENUM_TO_INT(MOD_HELP_JUMP))
		IF CONTENT_IS_USING_ARENA()
			RETURN "AVMH_JUMP_MD"
		ELSE
			RETURN "AVMH_JUMP"
		ENDIF
	ENDIF	
	
	RETURN ""
	
ENDFUNC


/// PURPOSE:
///    Adds shunt help if required
/// PARAMS:
///    sInstance - arena vehicle help instance
/// RETURNS:
///    The help text label if the help text can and should be displayed, returns "" otherwise
FUNC STRING TRY_GET_SHUNT_HELP(INT& iArenaHelpBS[])

	// Vehicle shunt mod is only useable in arena series
	IF NOT CONTENT_IS_USING_ARENA()
		RETURN ""
	ENDIF
	
	// If has shunt mod
	IF CAN_PRINT_MOD_HELP_TEXT(iArenaHelpBS, MOD_ENGINEBAY2, ENUM_TO_INT(MOD_SHUNT),
	ENUM_TO_INT(MOD_HELP_SHUNT))
		
		RETURN "AVMH_SHUNT"
	ENDIF

	RETURN ""
	
ENDFUNC


/// PURPOSE:
///    Trys to get the flamethrower help text for vehicles that have a 
///    flamethrower installed by default
/// PARAMS:
///    sInstance - arena vehicle help instance
/// RETURNS:
///    The help text label if the help text can and should be displayed, returns "" otherwise
FUNC STRING TRY_GET_FLAMETHROWER_HELP(INT& iArenaHelpBS[])
	
	// Flame thrower isn't available during arena race
	IF IS_ARENA_RACE()
		RETURN ""
	ENDIF
	
	// If in cerberus that has flamethrower by default
	IF CAN_PRINT_VEHICLE_DEFAULT_MOD_HELP_TEXT(iArenaHelpBS, CERBERUS, ENUM_TO_INT(MOD_HELP_FLAMETHROWER)) OR
		CAN_PRINT_VEHICLE_DEFAULT_MOD_HELP_TEXT(iArenaHelpBS, CERBERUS2, ENUM_TO_INT(MOD_HELP_FLAMETHROWER)) OR
		CAN_PRINT_VEHICLE_DEFAULT_MOD_HELP_TEXT(iArenaHelpBS, CERBERUS3, ENUM_TO_INT(MOD_HELP_FLAMETHROWER))	
		
		RETURN "AVMH_FLAMETHROWER"
	ENDIF

	RETURN ""
	
ENDFUNC


 /// PURPOSE:
 ///    Checks if the provided vehicle model can have the flamethrower
 ///    mod
 /// PARAMS:
 ///    eVehicleModel - the vehicle model to check
 /// RETURNS:
 ///    TRUE if the vehicle model can have a flamethrower
FUNC BOOL VEHICLE_CAN_HAVE_FLAMETHROWER_MOD(MODEL_NAMES eVehicleModel)	  
	  
	  SWITCH eVehicleModel
	  	CASE CERBERUS
		CASE CERBERUS2
		CASE CERBERUS3		
			RETURN TRUE
	ENDSWITCH
	  
	  RETURN FALSE
	  
ENDFUNC


/// PURPOSE:
///    Trys to retrieve the help text for the remote flamethrower.
///    Adheres to a list of vehicles that can have the flamethrower
/// PARAMS:
///    sInstance - arena vehicle help instance
/// RETURNS:
///    The help text label if the help text can and should be displayed, returns "" otherwise
FUNC STRING TRY_GET_REMOTE_FLAMETHROWER_HELP(INT& iArenaHelpBS[])
	
	IF NOT VEHICLE_CAN_HAVE_FLAMETHROWER_MOD(GET_PLAYERS_CURRENT_VEHICLE_MODEL())
		RETURN ""
	ENDIF
	
	// Flame thrower isn't available during arena race
	IF IS_ARENA_RACE()
		RETURN ""
	ENDIF
	
	// If none is in the driver seat the ped will move into it and will no longer be a passenger
	IF GET_PED_IN_VEHICLE_SEAT(GET_PLAYERS_CURRENT_VEHICLE()) = NULL
		RETURN ""
	ENDIF
	
	// If has secondary flame thrower
	IF CAN_PRINT_MOD_HELP_TEXT(iArenaHelpBS, MOD_ROOF, ENUM_TO_INT(MOD_REMOTE_FLAMETHROWER),
	ENUM_TO_INT(MOD_HELP_REMOTE_FLAMETHROWER), VS_FRONT_RIGHT)	
		RETURN "AVMH_REMOTE_FLAMETHROWER"
	ENDIF

	RETURN ""
	
ENDFUNC


/// PURPOSE:
///    Trys to get the tombstone help text for vehicles that have a 
///    tombstone installed by default
/// PARAMS:
///    sInstance - arena vehicle help instance
/// RETURNS:
///    The help text label if the help text can and should be displayed, returns "" otherwise
FUNC STRING TRY_GET_TOMBSTONE_HELP(INT& iArenaHelpBS[])
	
	// If the vehicles tombstone is broken off, leave
	IF NOT GET_DOES_VEHICLE_HAVE_TOMBSTONE(GET_PLAYERS_CURRENT_VEHICLE())
		RETURN ""
	ENDIF

	IF CAN_PRINT_VEHICLE_DEFAULT_MOD_HELP_TEXT(iArenaHelpBS, DOMINATOR4, ENUM_TO_INT(MOD_HELP_TOMBSTONE)) OR
		CAN_PRINT_VEHICLE_DEFAULT_MOD_HELP_TEXT(iArenaHelpBS, DOMINATOR5, ENUM_TO_INT(MOD_HELP_TOMBSTONE)) OR
		CAN_PRINT_VEHICLE_DEFAULT_MOD_HELP_TEXT(iArenaHelpBS, DOMINATOR6, ENUM_TO_INT(MOD_HELP_TOMBSTONE))	
		
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			RETURN "AVMH_TOMBSTONE_PC"
		ENDIF
		
		RETURN "AVMH_TOMBSTONE"
	ENDIF

	RETURN ""

ENDFUNC


/// PURPOSE:
///    Checks if the vehicle model shouldn't recieve mod help
/// PARAMS:
///    eVehicleModel - the vehicle model to check
/// RETURNS:
///    TRUE if the vehicle shouldn't recieve mod help
FUNC BOOL SHOULD_DISPLAY_MOD_HELP_FOR_VEHICLE(MODEL_NAMES eVehicleModel)
	
	// Add other exceptions to this switch
	SWITCH eVehicleModel
	CASE RCBANDITO	
		// url:bugstar:5547939 RC bandito is not a help text exception during an rc race
		RETURN IS_STUNT_RACE()	
	DEFAULT
		RETURN IS_VEHICLE_AN_ARENA_CONTENDER_VEHICLE(eVehicleModel)
	ENDSWITCH

	RETURN FALSE
	
ENDFUNC


/// PURPOSE:
///    Gets arena vehicle mod help text labels.
/// PARAMS:
///    iArenaHelpBS - the bitset that keeps track of displayed help
///    iIndex - the index of the next label to try and get
/// RETURNS:
///    The help text label at the requested index if it
///    should be displayed
FUNC STRING GET_ARENA_VEHICLE_MODS_HELP(INT& iArenaHelpBS[], INT iIndex)	
	
	MODEL_NAMES eVehicleModel = GET_PLAYERS_CURRENT_VEHICLE_MODEL()
	
	IF NOT SHOULD_DISPLAY_MOD_HELP_FOR_VEHICLE(eVehicleModel) 
		RETURN ""
	ENDIF	
	
	SWITCH INT_TO_ENUM(ARENA_VEHICLE_MOD_HELP, iIndex)
		CASE MOD_HELP_BOOST
			RETURN TRY_GET_BOOST_MOD_HELP(iArenaHelpBS)
		CASE MOD_HELP_MINE
			RETURN TRY_GET_MINE_HELP(iArenaHelpBS)
		CASE MOD_HELP_JUMP
			RETURN TRY_GET_JUMP_HELP(iArenaHelpBS)
		CASE MOD_HELP_SHUNT
			RETURN TRY_GET_SHUNT_HELP(iArenaHelpBS)
		CASE MOD_HELP_FLAMETHROWER
			RETURN TRY_GET_FLAMETHROWER_HELP(iArenaHelpBS)
		CASE MOD_HELP_REMOTE_FLAMETHROWER
			RETURN TRY_GET_REMOTE_FLAMETHROWER_HELP(iArenaHelpBS)
		CASE MOD_HELP_TOMBSTONE
			RETURN TRY_GET_TOMBSTONE_HELP(iArenaHelpBS)
	ENDSWITCH

	RETURN ""
	
ENDFUNC


/// PURPOSE:
///    Trys to display help for purchasing the rc bandito.
///    Will only display help once if the player owns the RC
///    bandito.
/// PARAMS:
///    iGeneralHelpTextPrintedBS - the bitset that keeps track of displayed help
/// RETURNS:
///    TRUE if the message has been displayed
FUNC BOOL TRY_DISPLAY_RC_BANDIT_HELP(INT& iGeneralHelpTextPrintedBS)
	
	// Do not own bandito yet so leave
	IF NOT IS_PLAYER_RC_BANDITO_PURCHASED(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID()) OR 
	FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) != -1
		RETURN FALSE
	ENDIF
	
	IF IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
		RETURN FALSE
	ENDIF
	
	// Message already displayed so leave
	IF IS_BIT_SET(iGeneralHelpTextPrintedBS, VEHICLE_HELP_PRINTED_RC_BANDITO)
		RETURN FALSE
	ENDIF
	
	IF GET_PACKED_STAT_INT(PACKED_INT_RC_CAR_INIT_HELP) >= 3
		RETURN FALSE
	ENDIF
	
	// Display bandit help text and mark as displayed
	SET_BIT(iGeneralHelpTextPrintedBS, VEHICLE_HELP_PRINTED_RC_BANDITO)
	SET_PACKED_STAT_INT(PACKED_INT_RC_CAR_INIT_HELP, GET_PACKED_STAT_INT(PACKED_INT_RC_CAR_INIT_HELP) + 1)
	PRINT_HELP("AVMH_USE_BANDITO")
	RETURN TRUE

ENDFUNC

#IF FEATURE_CASINO_HEIST
/// PURPOSE:
///    Trys to display help for purchasing the rc bandito.
///    Will only display help once if the player owns the RC
///    bandito.
/// PARAMS:
///    iGeneralHelpTextPrintedBS - the bitset that keeps track of displayed help
/// RETURNS:
///    TRUE if the message has been displayed
FUNC BOOL TRY_DISPLAY_RC_TANK_HELP(INT& iGeneralHelpTextPrintedBS)
	// Do not own bandito yet so leave
	IF NOT HAS_PLAYER_PURCHASED_RC_TANK(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
	OR FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) != -1
		RETURN FALSE
	ENDIF
	
	IF IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
		RETURN FALSE
	ENDIF
	
	// Message already displayed so leave
	IF IS_BIT_SET(iGeneralHelpTextPrintedBS, VEHICLE_HELP_PRINTED_RC_TANK)
		RETURN FALSE
	ENDIF
	
	IF GET_PACKED_STAT_INT(PACKED_INT_RC_TANK_INIT_HELP) >= 3
		RETURN FALSE
	ENDIF
	
	// Display bandit help text and mark as displayed
	SET_BIT(iGeneralHelpTextPrintedBS, VEHICLE_HELP_PRINTED_RC_TANK)
	
	SET_PACKED_STAT_INT(PACKED_INT_RC_TANK_INIT_HELP, GET_PACKED_STAT_INT(PACKED_INT_RC_TANK_INIT_HELP) + 1)
	
	PRINT_HELP("AVMH_USE_RCTANK")
	
	RETURN TRUE
ENDFUNC
#ENDIF

/// PURPOSE:
///    Displays arena help text that displays when not in a vehicle
/// PARAMS:
///    iGeneralHelpTextPrintedBS - the bitset that keeps track of displayed help
/// RETURNS:
///    TRUE if a message has been displayed
FUNC BOOL TRY_DISPLAY_ARENA_ON_FOOT_VEHICLE_HELP(INT& iGeneralHelpTextPrintedBS)
	
	IF TRY_DISPLAY_RC_BANDIT_HELP(iGeneralHelpTextPrintedBS)
		RETURN TRUE
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF TRY_DISPLAY_RC_TANK_HELP(iGeneralHelpTextPrintedBS)
		RETURN TRUE
	ENDIF
	#ENDIF
	
	// Add arena specific on foot help here in a similar manner
	
	RETURN FALSE
	
ENDFUNC

