USING "globals.sch"
USING "commands_network.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "net_mission_info.sch"
USING "NeT_Script_Launcher.sch"
USING "net_active_players.sch"

#IF IS_DEBUG_BUILD

CONST_INT PERFORMANCE_TEST_INACTIVE		0
CONST_INT PERFORMANCE_TEST_IDLE 		1
CONST_INT PERFORMANCE_TEST_AI			2
CONST_INT PERFORMANCE_TEST_AI_WANTED	3
CONST_INT PERFORMANCE_TEST_PHYSICS		4
CONST_INT PERFORMANCE_TEST_FINISHED		5

CONST_INT PERFORMANCE_STAGE_INIT				0
CONST_INT PERFORMANCE_STAGE_CREATING			1
CONST_INT PERFORMANCE_STAGE_LOADING_SCENE		2
CONST_INT PERFORMANCE_STAGE_READY				3
CONST_INT PERFORMANCE_STAGE_GRAB_DATA			4
CONST_INT PERFORMANCE_STAGE_FINISHED			5

CONST_INT MAX_HEALTH_VALUE 4000

CONST_INT TEST_TIME_IDLE	60000
CONST_INT TEST_TIME_AI		60000
CONST_INT TEST_TIME_PHYSICS	60000

TWEAK_INT NUMBER_OF_BOTS_FOR_PERFORMANCE_TEST 14

TWEAK_FLOAT PERFORMANCE_TEST_BOTS_RADIUS 3.0 



FUNC BOOL DOES_BOT_EXIST(INT iBot)
	PLAYER_INDEX PlayerID
	PlayerID = NETWORK_BOT_GET_PLAYER_ID(iBot)
	RETURN(NETWORK_BOT_EXISTS(PlayerID))
	RETURN(FALSE)
ENDFUNC
	
FUNC INT GET_BOT_TEAM(INT iBot)
	PLAYER_INDEX PlayerID
	PlayerID = NETWORK_BOT_GET_PLAYER_ID(iBot)	
	IF NETWORK_BOT_EXISTS(PlayerID)
		RETURN(GET_PLAYER_TEAM(PlayerID))
	ENDIF
	RETURN(-1)	
ENDFUNC
	
	
FUNC BOOL IS_BOT_PARTICIPANT(INT iBot)
	PLAYER_INDEX PlayerID
	PlayerID = NETWORK_BOT_GET_PLAYER_ID(iBot)	
	IF NETWORK_BOT_EXISTS(PlayerID)
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerID)
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_BOT_ON_ACTIVE_MISSION(INT iBot)
	PLAYER_INDEX PlayerID
	PlayerID = NETWORK_BOT_GET_PLAYER_ID(iBot)
	IF IS_NET_PLAYER_OK(PlayerID, FALSE)
		IF NOT (GlobalplayerBD[NATIVE_TO_INT(playerID)].activeMission.mdID.idMission = eNULL_MISSION)
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC REMOVE_BOT_FROM_ACTIVE_MISSION(INT iBot)
	PLAYER_INDEX PlayerID
	PlayerID = NETWORK_BOT_GET_PLAYER_ID(iBot)
	IF IS_NET_PLAYER_OK(PlayerID, FALSE)
		GlobalplayerBD[NATIVE_TO_INT(playerID)].activeMission.mdID.idMission	= eNULL_MISSION
		GlobalplayerBD[NATIVE_TO_INT(playerID)].activeMission.iInstanceId		= -1
	ENDIF
ENDPROC

PROC ADD_BOT_TO_ACTIVE_MISSION(INT iBot)
	PLAYER_INDEX PlayerID
	PlayerID = NETWORK_BOT_GET_PLAYER_ID(iBot)
	IF IS_NET_PLAYER_OK(PlayerID, FALSE)
		GlobalplayerBD[NATIVE_TO_INT(playerID)].activeMission.mdID.idMission	= GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].activeMission.mdID.idMission
		GlobalplayerBD[NATIVE_TO_INT(playerID)].activeMission.iInstanceId		= GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].activeMission.iInstanceId
		g_BotsData.iLastBotToJoinScript = iBot
	ENDIF
ENDPROC

FUNC BOOL IS_BOT_ALIVE(INT iBot)
	PLAYER_INDEX PlayerID
	PlayerID = NETWORK_BOT_GET_PLAYER_ID(iBot)
	RETURN(IS_PLAYER_PLAYING(PlayerID))
	RETURN(FALSE)
ENDFUNC


PROC TELL_BOT_TO_JOIN_THIS_SCRIPT(INT iBot)
	SET_BIT(g_BotsData.iBS_BotToldToJoinScript, iBot)
ENDPROC

PROC TELL_BOT_TO_LEAVE_THIS_SCRIPT(INT iBot)
	SET_BIT(g_BotsData.iBS_BotToldToLeaveScript, iBot)
ENDPROC

PROC TELL_BOT_TO_GET_BRAIN(INT iBot)
	SET_BIT(g_BotsData.iBS_BotToldToGetBrain, iBot)
ENDPROC

PROC TELL_BOT_TO_COPY_PLAYERS_BD(INT iBot)
	SET_BIT(g_BotsData.iBS_BotToldToCopyPlayersBD, iBot)
ENDPROC

PROC CREATE_BOT()

	INT iBotID
	iBotID = NETWORK_BOT_ADD()
	
	TELL_BOT_TO_JOIN_THIS_SCRIPT(iBotID)
	TELL_BOT_TO_GET_BRAIN(iBotID)
	TELL_BOT_TO_COPY_PLAYERS_BD(iBotID)
	
	g_BotsData.iLastCreatedBot = iBotID
	g_BotsData.iNumberOfCreatedBots += 1
	
	SET_PLAYER_COUNTS()
	
ENDPROC

PROC CLEANUP_BOT()
	g_BotsData.iNumberOfCreatedBots += -1
	IF DOES_BOT_EXIST(g_BotsData.iNumberOfCreatedBots)
		NETWORK_BOT_REMOVE(g_BotsData.iNumberOfCreatedBots)
	ENDIF			
	WHILE DOES_BOT_EXIST(g_BotsData.iNumberOfCreatedBots)
		WAIT(0)
	ENDWHILE	
ENDPROC

PROC CLEANUP_BOTS()
	WHILE g_BotsData.iNumberOfCreatedBots > 0
		CLEANUP_BOT()
		WAIT(0)
	ENDWHILE
ENDPROC

PROC PROCESS_BOT_WIDGETS()


	BOOL bIsFull
	
	// reset the last created bot
	IF NOT (g_BotsData.iLastCreatedBot = -1)
		
		IF (g_BotsData.bShowBotsDebug)
			NET_PRINT("[net_bots] NETWORK_BOT_GET_PLAYER_ID(g_BotsData.iLastCreatedBot) = ")
			NET_PRINT_INT(NATIVE_TO_INT(NETWORK_BOT_GET_PLAYER_ID(g_BotsData.iLastCreatedBot)))
			NET_NL()
		ENDIF
		
		IF NOT (NETWORK_BOT_GET_PLAYER_ID(g_BotsData.iLastCreatedBot) = INVALID_PLAYER_INDEX())
			IF IS_NET_PLAYER_OK(NETWORK_BOT_GET_PLAYER_ID(g_BotsData.iLastCreatedBot), FALSE)
				g_BotsData.iLastCreatedBot = -1	
			ENDIF
		ENDIF	
		
	ELSE	

		
		IF (g_BotsData.bFillSessionWithBots)
		OR (g_BotsData.bAddSingleBot)
			
			bIsFull = TRUE
			g_BotsData.bRemoveBotsFromSubScripts = FALSE
		

			IF NETWORK_GET_TOTAL_NUM_PLAYERS() + NETWORK_BOT_GET_NUMBER_OF_ACTIVE_BOTS() < NUM_NETWORK_PLAYERS
				NET_PRINT("[net_bots] PROCESS_BOT_WIDGETS - total num players = ")
				NET_PRINT_INT(NETWORK_GET_TOTAL_NUM_PLAYERS() + NETWORK_BOT_GET_NUMBER_OF_ACTIVE_BOTS())
				NET_NL()
				CREATE_BOT()	
				g_BotsData.bAddSingleBot = FALSE
				bIsFull = FALSE
			ENDIF

			
			IF (bIsFull)
				g_BotsData.bFillSessionWithBots = FALSE
				g_BotsData.bFillSubScriptsWithBots = TRUE			
			ENDIF
				
		ENDIF
		
		IF (g_BotsData.bRemoveSingleBot)
			CLEANUP_BOT()
			g_BotsData.bRemoveSingleBot = FALSE
		ENDIF
		
		IF (g_BotsData.bRemoveAllBots)
			g_BotsData.bFillSessionWithBots = FALSE
			CLEANUP_BOTS()
			g_BotsData.bRemoveAllBots = FALSE
			g_BotsData.bRemoveBotsFromSubScripts = TRUE
		ENDIF
		
	ENDIF
	
	
ENDPROC

PROC CREATE_BOT_WIDGET(BOOL bMainScript=FALSE)

	g_BotsData.vPerformanceTestCoords = <<195.0000, -935.0000, 29.6918>>
	g_BotsData.fPerformanceTestHeading = 0.0

	START_WIDGET_GROUP("Bots")
		
		ADD_WIDGET_BOOL("g_BotsData.bBotsActive", g_BotsData.bBotsActive)
		
		IF (bMainScript)
			
			ADD_WIDGET_BOOL("g_BotsData.bFillSessionWithBots", g_BotsData.bFillSessionWithBots)
			ADD_WIDGET_BOOL("g_BotsData.bAddSingleBot", g_BotsData.bAddSingleBot)
			ADD_WIDGET_BOOL("g_BotsData.bFillSubScriptsWithBots", g_BotsData.bFillSubScriptsWithBots)
			ADD_WIDGET_BOOL("g_BotsData.bRemoveBotsFromSubScripts", g_BotsData.bRemoveBotsFromSubScripts)
			
			ADD_WIDGET_BOOL("g_BotsData.bRemoveAllBots", g_BotsData.bRemoveAllBots)	
			ADD_WIDGET_BOOL("g_BotsData.bRemoveSingleBot", g_BotsData.bRemoveSingleBot)	
			ADD_WIDGET_BOOL("g_BotsData.bShowBotsDebug", g_BotsData.bShowBotsDebug)
			ADD_WIDGET_INT_SLIDER("g_BotsData.iNumberOfCreatedBots", g_BotsData.iNumberOfCreatedBots, -1, 99, 1)
			ADD_WIDGET_INT_SLIDER("g_BotsData.iLastCreatedBot", g_BotsData.iLastCreatedBot, -1, 99, 1)
			
			ADD_WIDGET_BOOL("g_BotsData.g_bBotsToJoinHeist", g_BotsData.g_bBotsToJoinHeist)
			
		ENDIF
		
		START_WIDGET_GROUP("BitSets")
			ADD_BIT_FIELD_WIDGET("g_BotsData.iBS_BotToldToJoinScript", g_BotsData.iBS_BotToldToJoinScript)
			ADD_BIT_FIELD_WIDGET("g_BotsData.iBS_BotToldToLeaveScript", g_BotsData.iBS_BotToldToLeaveScript)
			ADD_BIT_FIELD_WIDGET("g_BotsData.iBS_BotToldToGetBrain", g_BotsData.iBS_BotToldToGetBrain)
			ADD_BIT_FIELD_WIDGET("g_BotsData.iBS_BotToldToCopyPlayersBD", g_BotsData.iBS_BotToldToCopyPlayersBD)
			//ADD_BIT_FIELD_WIDGET("iBS_BotToldToJoinPlayersGroup", iBS_BotToldToJoinPlayersGroup)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Perfomance Test")
			ADD_WIDGET_BOOL("bDoPerformanceTest", g_BotsData.bDoPerformanceTest)
			
			ADD_WIDGET_INT_SLIDER("GlobalplayerBD[0].iActivePerformanceTest", GlobalplayerBD[0].iActivePerformanceTest, -1, 10, 1)
			ADD_WIDGET_INT_SLIDER("GlobalplayerBD[0].iPerformanceTestStage", GlobalplayerBD[0].iPerformanceTestStage, -1, 10, 1)
			ADD_WIDGET_INT_SLIDER("GlobalplayerBD[1].iActivePerformanceTest", GlobalplayerBD[1].iActivePerformanceTest, -1, 10, 1)
			ADD_WIDGET_INT_SLIDER("GlobalplayerBD[1].iPerformanceTestStage", GlobalplayerBD[1].iPerformanceTestStage, -1, 2, 10)
		STOP_WIDGET_GROUP()
			
		
   	STOP_WIDGET_GROUP()
ENDPROC

FUNC INT GET_NUMBER_OF_TEAM_MEMBERS_ON_SCRIPT(INT iTeam)
	INT i
	INT iCount
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
		IF IS_NET_PLAYER_OK(PlayerID, FALSE)		
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerID)
				IF (GET_PLAYER_TEAM(PlayerID) = iTeam)
					iCount += 1					
				ENDIF			
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN(iCount)
ENDFUNC



PROC PROCESS_BOTS_FOR_NON_LAUNCH_SCRIPT()

	//NET_PRINT("[net_bots] - PROCESS_BOTS_FOR_LAUNCH_SCRIPT called ")  NET_NL()
	
	INT i

	MP_MISSION ActiveMission
	
	IF (g_BotsData.bFillSubScriptsWithBots)
		
		IF (g_BotsData.iLastBotToJoinScript = -1)

			ActiveMission = GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].activeMission.mdID.idMission
			
			IF NOT (ActiveMission = eNULL_MISSION)
			AND ARE_STRINGS_EQUAL(GET_MP_MISSION_NAME(ActiveMission) , GET_THIS_SCRIPT_NAME())
			
				IF (NETWORK_GET_NUM_PARTICIPANTS() < NETWORK_GET_MAX_NUM_PARTICIPANTS())
								

					// KGM 5/3/13: Removed check for adding to a Heist script
					REPEAT g_BotsData.iNumberOfCreatedBots i
						IF DOES_BOT_EXIST(i)																							
							IF NOT IS_BOT_PARTICIPANT(i)																	
								//IF (GET_BOT_TEAM(i) = iTeam)
								
									NETWORK_BOT_JOIN_THIS_SCRIPT(i)	
									ADD_BOT_TO_ACTIVE_MISSION(i)
									NET_PRINT("[net_bots] Bot ") NET_PRINT_INT(i) NET_PRINT(" told to join script ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL() //NET_PRINT(" of team ") NET_PRINT_INT(iTeam) NET_NL()									
								
									// only add one bot per frame
									EXIT
									
								//ENDIF
							ENDIF
						ENDIF
					ENDREPEAT

					
				ENDIF
						
			ELSE
		
				IF (NETWORK_GET_NUM_PARTICIPANTS() < NETWORK_GET_MAX_NUM_PARTICIPANTS())
				
					REPEAT g_BotsData.iNumberOfCreatedBots i
						IF DOES_BOT_EXIST(i)
							IF NOT IS_BOT_PARTICIPANT(i)
								NETWORK_BOT_JOIN_THIS_SCRIPT(i)	
								NET_PRINT("[net_bots] Bot ") NET_PRINT_INT(i) NET_PRINT(" told to join script ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
								EXIT // only add one bot per frame
							ENDIF
						ENDIF
					ENDREPEAT		

				ENDIF
		
			ENDIF	
		
		ELSE
			IF DOES_BOT_EXIST(g_BotsData.iLastBotToJoinScript)
				IF IS_BOT_PARTICIPANT(g_BotsData.iLastBotToJoinScript)
					g_BotsData.iLastBotToJoinScript = -1	
				ENDIF
			ELSE
				g_BotsData.iLastBotToJoinScript = -1	
			ENDIF
		ENDIF
		
		
	ENDIF
	
	IF (g_BotsData.bRemoveBotsFromSubScripts)
		REPEAT g_BotsData.iNumberOfCreatedBots i
			IF DOES_BOT_EXIST(i)
				IF IS_BOT_PARTICIPANT(i)	
					NETWORK_BOT_LEAVE_THIS_SCRIPT(i)	
					REMOVE_BOT_FROM_ACTIVE_MISSION(i)
					NET_PRINT("[net_bots] Bot ") NET_PRINT_INT(i) NET_PRINT(" told to leave script ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
					EXIT // only remove one bot per frame
				ENDIF
			ENDIF
		ENDREPEAT	
	ENDIF
	
	
ENDPROC

PROC PROCESS_BOTS_FOR_LAUNCH_SCRIPT()
	INT i
	// process bot bit sets
	REPEAT g_BotsData.iNumberOfCreatedBots i	
	
		// process g_BotsData.iBS_BotToldToJoinScript
		IF IS_BIT_SET(g_BotsData.iBS_BotToldToJoinScript, i)
			IF DOES_BOT_EXIST(i)
				NETWORK_BOT_JOIN_THIS_SCRIPT(i)			
				CLEAR_BIT(g_BotsData.iBS_BotToldToJoinScript, i)			
			ENDIF
		ENDIF
		// process g_BotsData.iBS_BotToldToLeaveScript
		IF IS_BIT_SET(g_BotsData.iBS_BotToldToLeaveScript, i)
			IF DOES_BOT_EXIST(i)
				NETWORK_BOT_LEAVE_THIS_SCRIPT(i)
				CLEAR_BIT(g_BotsData.iBS_BotToldToLeaveScript, i)			
			ENDIF
		ENDIF
		// process g_BotsData.iBS_BotToldToGetBrain
		IF IS_BIT_SET(g_BotsData.iBS_BotToldToGetBrain, i)	
			IF DOES_BOT_EXIST(i)
				IF NETWORK_BOT_IS_LOCAL(i)
					NETWORK_BOT_LAUNCH_SCRIPT(i, "net_bot_brain")
					CLEAR_BIT(g_BotsData.iBS_BotToldToGetBrain, i)
				ENDIF
			ENDIF
		ENDIF
		// update some of the global broadcast data to match the player
		IF IS_BIT_SET(g_BotsData.iBS_BotToldToCopyPlayersBD, i)
			IF DOES_BOT_EXIST(i)	
				IF IS_BOT_ALIVE(i)
					IF NETWORK_BOT_IS_LOCAL(i)	
						IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(NETWORK_BOT_GET_PLAYER_ID(i))	
							// info to copy to other 
							GlobalplayerBD[NATIVE_TO_INT(NETWORK_BOT_GET_PLAYER_ID(i))].iGameState = GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iGameState
							GlobalplayerBD[NATIVE_TO_INT(NETWORK_BOT_GET_PLAYER_ID(i))].iRespawnState = GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState
							CLEAR_BIT(g_BotsData.iBS_BotToldToCopyPlayersBD, i)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_BOT_EXIST(i)
				IF NOT IS_BOT_ALIVE(i)
					SET_BIT(g_BotsData.iBS_BotToldToCopyPlayersBD, i)
				ENDIF
			ENDIF	
		ENDIF
		
		// if bot is 'on mission' check player is still on mission
		IF DOES_BOT_EXIST(i)
			IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].activeMission.mdID.idMission = eNULL_MISSION
				IF IS_BOT_ON_ACTIVE_MISSION(i)
					REMOVE_BOT_FROM_ACTIVE_MISSION(i)	
				ENDIF
			ENDIF
		ENDIF
	
		
		IF (g_BotsData.bShowBotsDebug)
		
				
			IF DOES_BOT_EXIST(i)
						
				IF IS_NET_PLAYER_OK(NETWORK_BOT_GET_PLAYER_ID(i),TRUE,TRUE)
					NET_PRINT("[net_bots] - player ")
					NET_PRINT_INT(i)
					NET_PRINT(" is ok")
					NET_NL()
				ELSE
					IF IS_NET_PLAYER_OK(NETWORK_BOT_GET_PLAYER_ID(i),TRUE,FALSE)
						NET_PRINT("[net_bots]  - player ")
						NET_PRINT_INT(i)
						NET_PRINT(" is ok, but not gamestate ok")
						NET_NL()
					ELSE
						IF IS_NET_PLAYER_OK(NETWORK_BOT_GET_PLAYER_ID(i),FALSE,FALSE)
							NET_PRINT("[net_bots]  - player ")
							NET_PRINT_INT(i)
							NET_PRINT(" is not ok nor gamestate ok")
							NET_NL()
						ELSE
							NET_PRINT("[net_bots]  - player ")
							NET_PRINT_INT(i)
							NET_PRINT(" player id ") NET_PRINT_INT(NATIVE_TO_INT(NETWORK_BOT_GET_PLAYER_ID(i)))
							NET_PRINT(" is not recognised.")
							NET_NL()	
							IF NETWORK_IS_PLAYER_ACTIVE(NETWORK_BOT_GET_PLAYER_ID(i))
								NET_PRINT("[net_bots]  - player ")
								NET_PRINT_INT(i)
								NET_PRINT(" player id ") NET_PRINT_INT(NATIVE_TO_INT(NETWORK_BOT_GET_PLAYER_ID(i)))
								NET_PRINT(" NETWORK_IS_PLAYER_ACTIVE = TRUE.")
								NET_NL()	
							ELSE
								NET_PRINT("[net_bots]  - player ")
								NET_PRINT_INT(i)
								NET_PRINT(" player id ") NET_PRINT_INT(NATIVE_TO_INT(NETWORK_BOT_GET_PLAYER_ID(i)))
								NET_PRINT(" NETWORK_IS_PLAYER_ACTIVE = FALSE.")
								NET_NL()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				NET_PRINT("[net_bots]  - player ")
				NET_PRINT_INT(i)
				NET_PRINT(" player id ") NET_PRINT_INT(NATIVE_TO_INT(NETWORK_BOT_GET_PLAYER_ID(i)))
				NET_PRINT(" does not exist.")
				NET_NL()
			ENDIF
		ENDIF
		
	ENDREPEAT

	PROCESS_BOT_WIDGETS()	
	
	
ENDPROC

PROC SET_PLAYERS_HEALTH_HIGH()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_MAX_HEALTH(PLAYER_PED_ID(), MAX_HEALTH_VALUE)
		SET_ENTITY_HEALTH(PLAYER_PED_ID(), MAX_HEALTH_VALUE)
		SET_PLAYER_MAX_ARMOUR(PLAYER_ID(), MAX_HEALTH_VALUE)
		SET_PED_ARMOUR(PLAYER_PED_ID(), MAX_HEALTH_VALUE)
	ENDIF
ENDPROC

FUNC BOOL DO_ALL_BOTS_EXIST_FOR_PERFORMANCE_TEST()
	INT iCount
	INT iBot
	REPEAT g_BotsData.iNumberOfCreatedBots iBot 
		IF DOES_BOT_EXIST(iBot)
			iCount++	
		ENDIF
	ENDREPEAT
	IF (iCount = NUMBER_OF_BOTS_FOR_PERFORMANCE_TEST)
		RETURN(TRUE)
	ENDIF
	PRINTLN("DO_ALL_BOTS_EXIST_FOR_PERFORMANCE_TEST = FALSE, iCount = ", iCount)
	RETURN(FALSE)
ENDFUNC

PROC SET_BOTS_HEALTH_HIGH()
	INT iBot
	PLAYER_INDEX PlayerID
	REPEAT g_BotsData.iNumberOfCreatedBots iBot
		IF DOES_BOT_EXIST(iBot)
			PlayerID = NETWORK_BOT_GET_PLAYER_ID(iBot)
			IF NOT IS_PED_INJURED(GET_PLAYER_PED(PlayerID))	
				SET_PED_MAX_HEALTH(GET_PLAYER_PED(PlayerID), MAX_HEALTH_VALUE)
				SET_ENTITY_HEALTH(GET_PLAYER_PED(PlayerID), MAX_HEALTH_VALUE)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


PROC SET_EVERYONE_SHOOT_AT_PLAYER_0()
	INT iBot
	PLAYER_INDEX PlayerID
	PLAYER_INDEX Player0 = INT_TO_NATIVE(PLAYER_INDEX, 0)
	IF NETWORK_IS_PLAYER_ACTIVE(Player0)
		IF IS_NET_PLAYER_OK(Player0)
			REPEAT g_BotsData.iNumberOfCreatedBots iBot
				IF DOES_BOT_EXIST(iBot)
					PlayerID = NETWORK_BOT_GET_PLAYER_ID(iBot)
					IF NOT IS_PED_INJURED(GET_PLAYER_PED(PlayerID))
						TASK_SHOOT_AT_ENTITY( GET_PLAYER_PED(PlayerID), GET_PLAYER_PED(Player0), 10000, FIRING_TYPE_CONTINUOUS )					
					ENDIF
				ENDIF
			ENDREPEAT
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				TASK_SHOOT_AT_ENTITY( PLAYER_PED_ID(), GET_PLAYER_PED(Player0), 10000, FIRING_TYPE_CONTINUOUS )
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_EVERYONE_ADDS_EXPLOSION()
//	INT iBot
//	PLAYER_INDEX PlayerID
	PLAYER_INDEX Player0 = INT_TO_NATIVE(PLAYER_INDEX, 0)
	IF NETWORK_IS_PLAYER_ACTIVE(Player0)
		IF IS_NET_PLAYER_OK(Player0)
//			REPEAT g_BotsData.iNumberOfCreatedBots iBot
//				IF DOES_BOT_EXIST(iBot)
//					PlayerID = NETWORK_BOT_GET_PLAYER_ID(iBot)
//					IF NOT IS_PED_INJURED(GET_PLAYER_PED(PlayerID))
//						ADD_EXPLOSION(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_PLAYER_PED(PlayerID), << 0.0, 0.5, 0.0>>), EXP_TAG_GRENADE, 1.0)		
//						PRINTLN("ADD_EXPLOSION called") 
//					ENDIF
//				ENDIF
//			ENDREPEAT
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				ADD_EXPLOSION(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), << 0.0, 0.5, 0.0>>), EXP_TAG_GRENADE, 1.0)	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_0_AT_TEST_COORDS()
	PLAYER_INDEX Player0 = INT_TO_NATIVE(PLAYER_INDEX, 0)
	IF NETWORK_IS_PLAYER_ACTIVE(Player0)
		IF IS_NET_PLAYER_OK(Player0)
			IF IS_ENTITY_AT_COORD (GET_PLAYER_PED(Player0), g_BotsData.vPerformanceTestCoords, <<0.2, 0.2, 1.5>>)
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC POSITION_PLAYER_INFRONT_OF_PLAYER_0()
	
	VECTOR vPlayerPos
	VECTOR vPos
	FLOAT fAngle 
	FLOAT fHeading
	
	IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_NATIVE(PLAYER_INDEX, 0))
		IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, 0))	
		
			vPlayerPos = GET_ENTITY_COORDS(GET_PLAYER_PED(INT_TO_NATIVE(PLAYER_INDEX, 0)))
			fAngle = 360.0/(NUMBER_OF_BOTS_FOR_PERFORMANCE_TEST+1)
			
			vPos = vPlayerPos
			vPos.x += PERFORMANCE_TEST_BOTS_RADIUS * SIN(fAngle*NUMBER_OF_BOTS_FOR_PERFORMANCE_TEST)
			vPos.y += PERFORMANCE_TEST_BOTS_RADIUS * COS(fAngle*NUMBER_OF_BOTS_FOR_PERFORMANCE_TEST)
			
			fHeading = 180.0 - (fAngle * NUMBER_OF_BOTS_FOR_PERFORMANCE_TEST)
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos, FALSE, FALSE)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeading)	
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 999999, TRUE, TRUE)
		ENDIF
	ENDIF
		
ENDPROC

FUNC BOOL RESET_ALL_POSITIONS_FOR_PERFORMANCE_TEST()

	INT iBot
	VECTOR vPlayerPos
	VECTOR vBotPos
	FLOAT fBotHeading
	FLOAT fAngle
	PLAYER_INDEX PlayerID

	// make sure we have 15 bots
	IF NOT (g_BotsData.iNumberOfCreatedBots = NUMBER_OF_BOTS_FOR_PERFORMANCE_TEST)
		IF (g_BotsData.iNumberOfCreatedBots > NUMBER_OF_BOTS_FOR_PERFORMANCE_TEST)
			CLEANUP_BOT()	
		ELIF (g_BotsData.iNumberOfCreatedBots < NUMBER_OF_BOTS_FOR_PERFORMANCE_TEST)
			CREATE_BOT()	
		ENDIF
	ELSE
		// position all bots around player 0
		IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_NATIVE(PLAYER_INDEX, 0))
			IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, 0))				
			
				vPlayerPos = GET_ENTITY_COORDS(GET_PLAYER_PED(INT_TO_NATIVE(PLAYER_INDEX, 0)))
				fAngle = 360.0/(NUMBER_OF_BOTS_FOR_PERFORMANCE_TEST+1 )
				
				REPEAT g_BotsData.iNumberOfCreatedBots iBot
					IF DOES_BOT_EXIST(iBot)
					
						PlayerID = NETWORK_BOT_GET_PLAYER_ID(iBot)
						
						vBotPos = vPlayerPos
						vBotPos.x += PERFORMANCE_TEST_BOTS_RADIUS * SIN((fAngle*iBot))
						vBotPos.y += PERFORMANCE_TEST_BOTS_RADIUS * COS((fAngle*iBot))
						
						fBotHeading = 180.0 - (fAngle * iBot)
												
						SET_ENTITY_COORDS(GET_PLAYER_PED(PlayerID), vBotPos, FALSE, FALSE)
						SET_ENTITY_HEADING(GET_PLAYER_PED(PlayerID), fBotHeading)
						
						CLEAR_PED_TASKS_IMMEDIATELY( GET_PLAYER_PED(PlayerID) )
						
						// give bots weapons
						IF (iBot < 3)
							GIVE_WEAPON_TO_PED(GET_PLAYER_PED(PlayerID), WEAPONTYPE_PISTOL, 999999, TRUE, TRUE)
						ELIF (iBot < 6)
							GIVE_WEAPON_TO_PED(GET_PLAYER_PED(PlayerID), WEAPONTYPE_MICROSMG, 999999, TRUE, TRUE)
						ELIF (iBot < 9)
							GIVE_WEAPON_TO_PED(GET_PLAYER_PED(PlayerID), WEAPONTYPE_ASSAULTRIFLE, 999999, TRUE, TRUE)
						ELIF (iBot < 12)
							GIVE_WEAPON_TO_PED(GET_PLAYER_PED(PlayerID), WEAPONTYPE_MG, 999999, TRUE, TRUE)
						ELSE
							GIVE_WEAPON_TO_PED(GET_PLAYER_PED(PlayerID), WEAPONTYPE_PUMPSHOTGUN, 999999, TRUE, TRUE)
						ENDIF
						
					ENDIF
				ENDREPEAT
				
				// finished
				IF DO_ALL_BOTS_EXIST_FOR_PERFORMANCE_TEST()
					RETURN(TRUE)
				ENDIF

			ENDIF
		ENDIF			
	ENDIF

	RETURN(FALSE)

ENDFUNC


PROC START_PERFORMANCE_TEST(INT iTestToStart)
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iActivePerformanceTest = iTestToStart
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPerformanceTestStage = 0
	g_BotsData.bBotsActive = TRUE
ENDPROC


PROC PROCESS_PERFORMANCE_TEST_IDLE()

	IF (NATIVE_TO_INT(PLAYER_ID()) = 0)
		SWITCH GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPerformanceTestStage
			CASE PERFORMANCE_STAGE_INIT			
				SET_ENTITY_COORDS(PLAYER_PED_ID(), g_BotsData.vPerformanceTestCoords)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), g_BotsData.fPerformanceTestHeading)	
				CLEAR_AREA(g_BotsData.vPerformanceTestCoords, 1000.0, TRUE)
				INSTANTLY_FILL_VEHICLE_POPULATION()
				INSTANTLY_FILL_PED_POPULATION()
				NEW_LOAD_SCENE_START_SPHERE(g_BotsData.vPerformanceTestCoords, 10.0)
				g_BotsData.PerformanceTimer = GET_NETWORK_TIME()
				GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_LOADING_SCENE
			BREAK
			CASE PERFORMANCE_STAGE_LOADING_SCENE
				IF IS_NEW_LOAD_SCENE_LOADED()
				OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_BotsData.PerformanceTimer) > 5000) 
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
					ENDIF
					GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_READY
				ENDIF
			BREAK
			CASE PERFORMANCE_STAGE_READY
				// wait for player 2 to become ready
				IF GlobalplayerBD[1].iPerformanceTestStage = PERFORMANCE_STAGE_READY
					g_BotsData.PerformanceTimer = GET_NETWORK_TIME()
					GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_GRAB_DATA					
					METRICS_ZONE_START("NetTest_Idle")					
				ENDIF
			BREAK
			CASE PERFORMANCE_STAGE_GRAB_DATA
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_BotsData.PerformanceTimer) > TEST_TIME_IDLE)					
					METRICS_ZONE_STOP()
					METRICS_ZONE_SAVE_TO_FILE("NetTest_Idle")
					METRICS_ZONES_CLEAR()				
					GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_FINISHED	
				ENDIF
			BREAK
			CASE PERFORMANCE_STAGE_FINISHED
				START_PERFORMANCE_TEST(PERFORMANCE_TEST_AI)	
			BREAK
		ENDSWITCH		
	ENDIF

	IF (NATIVE_TO_INT(PLAYER_ID()) = 1)
		SWITCH GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPerformanceTestStage
			CASE PERFORMANCE_STAGE_INIT
				IF GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_READY
					POSITION_PLAYER_INFRONT_OF_PLAYER_0()				
					GlobalplayerBD[1].iPerformanceTestStage = PERFORMANCE_STAGE_CREATING
				ENDIF
			BREAK
			CASE PERFORMANCE_STAGE_CREATING
				// create all bots
				IF RESET_ALL_POSITIONS_FOR_PERFORMANCE_TEST()
					POSITION_PLAYER_INFRONT_OF_PLAYER_0()	
					GlobalplayerBD[1].iPerformanceTestStage = PERFORMANCE_STAGE_READY
				ENDIF				
			BREAK
			CASE PERFORMANCE_STAGE_READY
				IF GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_GRAB_DATA
					GlobalplayerBD[1].iPerformanceTestStage = PERFORMANCE_STAGE_FINISHED
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF

ENDPROC

PROC PROCESS_PERFORMANCE_TEST_AI()

	IF (NATIVE_TO_INT(PLAYER_ID()) = 0)
		SWITCH GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPerformanceTestStage
			CASE PERFORMANCE_STAGE_INIT			
				SET_ENTITY_COORDS(PLAYER_PED_ID(), g_BotsData.vPerformanceTestCoords)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), g_BotsData.fPerformanceTestHeading)	
				CLEAR_AREA(g_BotsData.vPerformanceTestCoords, 1000.0, TRUE)
				INSTANTLY_FILL_VEHICLE_POPULATION()
				INSTANTLY_FILL_PED_POPULATION()
				NEW_LOAD_SCENE_START_SPHERE(g_BotsData.vPerformanceTestCoords, 10.0)
				g_BotsData.PerformanceTimer = GET_NETWORK_TIME()
				GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_LOADING_SCENE
			BREAK
			CASE PERFORMANCE_STAGE_LOADING_SCENE
				IF IS_NEW_LOAD_SCENE_LOADED()
				OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_BotsData.PerformanceTimer) > 5000) 
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
					ENDIF
					GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_READY
				ENDIF
			BREAK			
			CASE PERFORMANCE_STAGE_READY
				// wait for player 2 to become ready
				IF GlobalplayerBD[1].iPerformanceTestStage = PERFORMANCE_STAGE_READY
					g_BotsData.PerformanceTimer = GET_NETWORK_TIME()
					GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_GRAB_DATA					
					METRICS_ZONE_START("NetTest_AI")					
				ENDIF
			BREAK
			CASE PERFORMANCE_STAGE_GRAB_DATA
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_BotsData.PerformanceTimer) > TEST_TIME_AI)				
					METRICS_ZONE_STOP()
					METRICS_ZONE_SAVE_TO_FILE("NetTest_AI")
					METRICS_ZONES_CLEAR()				
					GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_FINISHED	
				ENDIF
			BREAK
			CASE PERFORMANCE_STAGE_FINISHED
				START_PERFORMANCE_TEST(PERFORMANCE_TEST_AI_WANTED)	
			BREAK
		ENDSWITCH	
	ENDIF

	IF (NATIVE_TO_INT(PLAYER_ID()) = 1)
		SWITCH GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPerformanceTestStage
			CASE PERFORMANCE_STAGE_INIT
				IF GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_READY
					POSITION_PLAYER_INFRONT_OF_PLAYER_0()				
					GlobalplayerBD[1].iPerformanceTestStage = PERFORMANCE_STAGE_CREATING
				ENDIF
			BREAK		
			CASE PERFORMANCE_STAGE_CREATING
				// create all bots
				IF RESET_ALL_POSITIONS_FOR_PERFORMANCE_TEST()
					POSITION_PLAYER_INFRONT_OF_PLAYER_0()	
					GlobalplayerBD[1].iPerformanceTestStage = PERFORMANCE_STAGE_READY
				ENDIF				
			BREAK
			CASE PERFORMANCE_STAGE_READY
				IF GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_GRAB_DATA
					SET_EVERYONE_SHOOT_AT_PLAYER_0()
					GlobalplayerBD[1].iPerformanceTestStage = PERFORMANCE_STAGE_FINISHED
				ENDIF
			BREAK
		ENDSWITCH				
	ENDIF

ENDPROC

PROC PROCESS_PERFORMANCE_TEST_AI_WANTED()

	IF (NATIVE_TO_INT(PLAYER_ID()) = 0)
		SWITCH GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPerformanceTestStage
			CASE PERFORMANCE_STAGE_INIT			
				SET_ENTITY_COORDS(PLAYER_PED_ID(), g_BotsData.vPerformanceTestCoords)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), g_BotsData.fPerformanceTestHeading)	
				CLEAR_AREA(g_BotsData.vPerformanceTestCoords, 1000.0, TRUE)
				INSTANTLY_FILL_VEHICLE_POPULATION()
				INSTANTLY_FILL_PED_POPULATION()
				NEW_LOAD_SCENE_START_SPHERE(g_BotsData.vPerformanceTestCoords, 10.0)
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 5)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				g_BotsData.PerformanceTimer = GET_NETWORK_TIME()
				GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_LOADING_SCENE
			BREAK
			CASE PERFORMANCE_STAGE_LOADING_SCENE
				IF IS_NEW_LOAD_SCENE_LOADED()
				OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_BotsData.PerformanceTimer) > 5000) 
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
					ENDIF
					GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_READY
				ENDIF
			BREAK			
			CASE PERFORMANCE_STAGE_READY
				// wait for player 2 to become ready
				IF GlobalplayerBD[1].iPerformanceTestStage = PERFORMANCE_STAGE_READY
					g_BotsData.PerformanceTimer = GET_NETWORK_TIME()
					GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_GRAB_DATA					
					METRICS_ZONE_START("NetTest_Wanted")					
				ENDIF
			BREAK
			CASE PERFORMANCE_STAGE_GRAB_DATA
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_BotsData.PerformanceTimer) > TEST_TIME_AI)				
					METRICS_ZONE_STOP()
					METRICS_ZONE_SAVE_TO_FILE("NetTest_Wanted")
					METRICS_ZONES_CLEAR()				
					GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_FINISHED	
				ENDIF
			BREAK
			CASE PERFORMANCE_STAGE_FINISHED
				START_PERFORMANCE_TEST(PERFORMANCE_TEST_PHYSICS)	
			BREAK
		ENDSWITCH	
	ENDIF

	IF (NATIVE_TO_INT(PLAYER_ID()) = 1)
		SWITCH GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPerformanceTestStage
			CASE PERFORMANCE_STAGE_INIT
				IF GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_READY
					POSITION_PLAYER_INFRONT_OF_PLAYER_0()				
					GlobalplayerBD[1].iPerformanceTestStage = PERFORMANCE_STAGE_CREATING
				ENDIF
			BREAK		
			CASE PERFORMANCE_STAGE_CREATING
				// create all bots
				IF RESET_ALL_POSITIONS_FOR_PERFORMANCE_TEST()
					POSITION_PLAYER_INFRONT_OF_PLAYER_0()	
					GlobalplayerBD[1].iPerformanceTestStage = PERFORMANCE_STAGE_READY
				ENDIF				
			BREAK
			CASE PERFORMANCE_STAGE_READY
				IF GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_GRAB_DATA
					SET_EVERYONE_SHOOT_AT_PLAYER_0()
					GlobalplayerBD[1].iPerformanceTestStage = PERFORMANCE_STAGE_FINISHED
				ENDIF
			BREAK
		ENDSWITCH				
	ENDIF

ENDPROC


PROC PROCESS_PERFORMANCE_TEST_PHYSICS()

	IF (NATIVE_TO_INT(PLAYER_ID()) = 0)
		SWITCH GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPerformanceTestStage
			CASE PERFORMANCE_STAGE_INIT
				SET_ENTITY_COORDS(PLAYER_PED_ID(), g_BotsData.vPerformanceTestCoords)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), g_BotsData.fPerformanceTestHeading)
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				CLEAR_AREA(g_BotsData.vPerformanceTestCoords, 1000.0, TRUE)
				INSTANTLY_FILL_VEHICLE_POPULATION()
				INSTANTLY_FILL_PED_POPULATION()
				NEW_LOAD_SCENE_START_SPHERE(g_BotsData.vPerformanceTestCoords, 10.0)
				g_BotsData.PerformanceTimer = GET_NETWORK_TIME()
				GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_LOADING_SCENE
			BREAK
			CASE PERFORMANCE_STAGE_LOADING_SCENE
				IF IS_NEW_LOAD_SCENE_LOADED()
				OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_BotsData.PerformanceTimer) > 5000) 
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
					ENDIF
					GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_READY
				ENDIF
			BREAK			
			CASE PERFORMANCE_STAGE_READY
				// wait for player 2 to become ready
				IF GlobalplayerBD[1].iPerformanceTestStage = PERFORMANCE_STAGE_READY
					g_BotsData.PerformanceTimer = GET_NETWORK_TIME()
					GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_GRAB_DATA					
					METRICS_ZONE_START("NetTest_Physics")
				ENDIF
			BREAK
			CASE PERFORMANCE_STAGE_GRAB_DATA
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_BotsData.PerformanceTimer) > TEST_TIME_PHYSICS)										
					METRICS_ZONE_STOP()
					METRICS_ZONE_SAVE_TO_FILE("NetTest_Physics")
					METRICS_ZONES_CLEAR()	
					GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_FINISHED					
				ENDIF
			BREAK
			CASE PERFORMANCE_STAGE_FINISHED
				// totally finished
				START_PERFORMANCE_TEST(PERFORMANCE_TEST_FINISHED)
			BREAK
		ENDSWITCH	
	ENDIF

	IF (NATIVE_TO_INT(PLAYER_ID()) = 1)
		SWITCH GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPerformanceTestStage
			CASE PERFORMANCE_STAGE_INIT
				IF GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_READY
					POSITION_PLAYER_INFRONT_OF_PLAYER_0()				
					GlobalplayerBD[1].iPerformanceTestStage = PERFORMANCE_STAGE_CREATING
				ENDIF
			BREAK
			CASE PERFORMANCE_STAGE_CREATING
				// create all bots
				IF RESET_ALL_POSITIONS_FOR_PERFORMANCE_TEST()
					POSITION_PLAYER_INFRONT_OF_PLAYER_0()	
					GlobalplayerBD[1].iPerformanceTestStage = PERFORMANCE_STAGE_READY
				ENDIF				
			BREAK
			CASE PERFORMANCE_STAGE_READY
				// wait for player 0 to start recording data
				IF GlobalplayerBD[0].iPerformanceTestStage = PERFORMANCE_STAGE_GRAB_DATA
					SET_EVERYONE_ADDS_EXPLOSION()
					GlobalplayerBD[1].iPerformanceTestStage = PERFORMANCE_STAGE_FINISHED
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF

ENDPROC

PROC PROCESS_PERFORMANCE_TEST()

	// if player 1 is not on same test as player 0 then reset player 1
	IF (NATIVE_TO_INT(PLAYER_ID()) = 1)
		IF NOT (GlobalplayerBD[0].iActivePerformanceTest = GlobalplayerBD[1].iActivePerformanceTest)
			START_PERFORMANCE_TEST(GlobalplayerBD[0].iActivePerformanceTest)
		ENDIF
	ENDIF

	SWITCH GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iActivePerformanceTest
		CASE PERFORMANCE_TEST_INACTIVE
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_botperformancetest")			
				START_PERFORMANCE_TEST(GET_COMMANDLINE_PARAM_INT("sc_botperformancetest"))
			ELIF (g_BotsData.bDoPerformanceTest)
				START_PERFORMANCE_TEST(PERFORMANCE_TEST_IDLE)				
			ENDIF
		BREAK
		CASE PERFORMANCE_TEST_IDLE 	
			PROCESS_PERFORMANCE_TEST_IDLE()	
		BREAK
		CASE PERFORMANCE_TEST_AI		
			PROCESS_PERFORMANCE_TEST_AI()
		BREAK
		CASE PERFORMANCE_TEST_AI_WANTED	
			PROCESS_PERFORMANCE_TEST_AI_WANTED()
		BREAK
		CASE PERFORMANCE_TEST_PHYSICS	
			PROCESS_PERFORMANCE_TEST_PHYSICS()
		BREAK
		CASE PERFORMANCE_TEST_FINISHED
			IF (g_BotsData.bDoPerformanceTest)
				START_PERFORMANCE_TEST(PERFORMANCE_TEST_IDLE)	
			ENDIF
		BREAK
	ENDSWITCH
	
	IF NOT (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iActivePerformanceTest = PERFORMANCE_TEST_INACTIVE)
		SET_PLAYERS_HEALTH_HIGH()
		IF (NATIVE_TO_INT(PLAYER_ID()) = 1)
			SET_BOTS_HEALTH_HIGH()
		ENDIF
	ENDIF
		
ENDPROC


PROC PROCESS_BOTS()
	IF (g_BotsData.bBotsActive)
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF (ARE_STRINGS_EQUAL(GET_LAUNCH_SCRIPT_NAME(), GET_THIS_SCRIPT_NAME()))
				PROCESS_BOTS_FOR_LAUNCH_SCRIPT()
			ELSE
				PROCESS_BOTS_FOR_NON_LAUNCH_SCRIPT()
			ENDIF
			NET_PRINT("[net_bots] - GET_THIS_SCRIPT_NAME() = ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL() 
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				PROCESS_PERFORMANCE_TEST()
			ENDIF
		ENDIF		
	ENDIF
ENDPROC
#ENDIF
