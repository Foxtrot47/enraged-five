USING "globals.sch"
USING "net_simple_interior_private.sch"


USING "kd_tree.sch"

FUNC FLOAT _GET_SIMPLE_INTERIOR_COORDS_FOR_KD_TREE(INT iElementID, INT iAxis = 0)
	IF iElementID >= 0 AND iElementID < ENUM_TO_INT(SIMPLE_INTERIOR_END) AND iAxis >= 0 and iAxis < 2
		VECTOR vResult = g_SimpleInteriorData.vMainEntryLocates[iElementID]
		//GET_SIMPLE_INTERIOR_ENTRY_LOCATE(INT_TO_ENUM(SIMPLE_INTERIORS, iElementID), vResult)
		
		IF iAxis = 0
			RETURN vResult.x
		ELSE
			RETURN vResult.y
		ENDIF
	ENDIF
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT _GET_LOCAL_PLAYER_COORDS_FOR_KD_TREE_SEARCH(INT iAxis = 0)
	VECTOR vResult
	
	vResult = GET_PLAYER_COORDS(PLAYER_ID())
	
	IF iAxis = 0
		RETURN vResult.x
	ELSE
		RETURN vResult.y
	ENDIF
	
	RETURN 0.0
ENDFUNC

PROC BUILD_SIMPLE_INTERIOR_KD_TREE(KD_TREE &out)
	INT iIndices[SIMPLE_INTERIOR_END]
	INT i
	INT iCount
	REPEAT SIMPLE_INTERIOR_END i
		IF NOT IS_VECTOR_ZERO(g_SimpleInteriorData.vMainEntryLocates[i])
		AND NOT IS_SIMPLE_INTERIOR_ENTRANCE_LOCATION_DYNAMIC(i)
			iIndices[iCount] = i
			iCount += 1
			PRINTLN("[SIMPLE_INTERIOR][KDTREE] BUILD_SIMPLE_INTERIOR_KD_TREE - Added interior with ID ", i, " Name: ", GET_SIMPLE_INTERIOR_DEBUG_NAME(INT_TO_ENUM(SIMPLE_INTERIORS, i)), " at coord: ", g_SimpleInteriorData.vMainEntryLocates[i])
		ELSE
			PRINTLN("[SIMPLE_INTERIOR][KDTREE] BUILD_SIMPLE_INTERIOR_KD_TREE - Not adding interior with ID ", i, " is main entry locate is 0? ", IS_VECTOR_ZERO(g_SimpleInteriorData.vMainEntryLocates[i]))
		ENDIF
	ENDREPEAT
	
	CONSTRUCT_KD_TREE(out, &_GET_SIMPLE_INTERIOR_COORDS_FOR_KD_TREE, 2, iIndices, iCount)
	
	PRINTLN("[SIMPLE_INTERIOR][KDTREE] BUILD_SIMPLE_INTERIOR_KD_TREE - Done.")
ENDPROC

PROC GET_SIMPLE_INTERIORS_IN_RANGE_OF_LOCAL_PLAYER_USING_KD_TREE(KD_TREE &kdtree, FLOAT fSearchRadius, INT &iResultsCount, SIMPLE_INTERIORS &results[])
	INT iClosestPoints[10]
	iResultsCount = 0
	
	FIND_ELEMENTS_IN_KD_TREE_IN_RADIUS(kdtree, kdtree.nodePool[0], &_GET_LOCAL_PLAYER_COORDS_FOR_KD_TREE_SEARCH, fSearchRadius, iClosestPoints, iResultsCount)
	
	INT i
	REPEAT IMIN(iResultsCount, COUNT_OF(results)) i
		results[i] = INT_TO_ENUM(SIMPLE_INTERIORS, iClosestPoints[i])
	ENDREPEAT
ENDPROC

#IF IS_DEBUG_BUILD
/*
PROC _PUT_PIXEL(INT x, INT y, INT r, INT g, INT b, INT a)
	INT width, height
	GET_ACTUAL_SCREEN_RESOLUTION(width, height)
	
	DRAW_RECT_FROM_CORNER(TO_FLOAT(x) / TO_FLOAT(width), TO_FLOAT(y) / TO_FLOAT(height), 1.0 / TO_FLOAT(width), 1.0 / TO_FLOAT(height), r, g, b, a)
ENDPROC

PROC _PLOT_LINE_2D(INT x0, INT y0, INT x1, INT y1, INT r, INT g, INT b, INT a)
	INT dx = x1 - x0
	INT dy = y1 - y0
	INT D = 2*dy - dx
	INT y = y0
	INT x

	FOR x = x0 TO x1
		_PUT_PIXEL(x, y, r, g, b, a)
		
		IF D > 0
    		y = y + 1
    		D = D - dx
		ENDIF
		
		D = D + dy
	ENDFOR
ENDPROC

PROC _PLOT_LINE_2D_NORMALISED(FLOAT x0, FLOAT y0, FLOAT x1, FLOAT y1, INT r, INT g, INT b, INT a)
	INT width, height
	GET_ACTUAL_SCREEN_RESOLUTION(width, height)
	_PLOT_LINE_2D(ROUND(x0 * width), ROUND(y0 * height), ROUND(x1 * width), ROUND(y1 * height), r, g, b, a)
ENDPROC
*/

PROC DEBUG_DRAW_SIMPLE_INTERIOR_KD_TREE(KD_TREE &kdtree, SIMPLE_INTERIOR_LOCAL_DATA &localData)
	INT i, j
	VECTOR vMaxCoord = <<-9999.9, -9999.9, 0.0>>
	VECTOR vMinCoord = <<9999.9, 9999.9, 0.0>>
	VECTOR vCurrent
	
	FLOAT fNormalisedX, fNormalisedY
	FLOAT fXScale = 1.0 / GET_ASPECT_RATIO(FALSE)
	
	REPEAT kdtree.iNodesCount i
		vCurrent.X = CALL kdtree.getValue(kdtree.nodePool[i].iElementID, 0)
		vCurrent.Y = CALL kdtree.getValue(kdtree.nodePool[i].iElementID, 1)
		
		IF vCurrent.X > vMaxCoord.X
			vMaxCoord.X = vCurrent.X
		ENDIF
		
		IF vCurrent.Y > vMaxCoord.Y
			vMaxCoord.Y = vCurrent.Y
		ENDIF
		
		IF vCurrent.X < vMinCoord.X
			vMinCoord.X = vCurrent.X
		ENDIF
		
		IF vCurrent.Y < vMinCoord.Y
			vMinCoord.Y = vCurrent.Y
		ENDIF
	ENDREPEAT
	
	// Draw player position
	vCurrent = GET_PLAYER_COORDS(PLAYER_ID())
	
	fNormalisedX = (vCurrent.X - vMinCoord.X) / ABSF(vMaxCoord.X - vMinCoord.X)
	fNormalisedY = (1.0 - (vCurrent.Y - vMinCoord.Y) / ABSF(vMaxCoord.Y - vMinCoord.Y))
	
	FLOAT fPlayerX = (fNormalisedX * 0.8 + 0.1) * fXScale
	FLOAT fPlayerY = (fNormalisedY * 0.8 + 0.1)
	
	INT iR, iG, iB
	BOOL bInRange//, bScriptRunning
	
	DRAW_RECT(fPlayerX, fPlayerY, 0.007 * fXScale, 0.007, 0, 255, 0, 255)
	
	FLOAT fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID()) + 180.0
	
	DRAW_LINE_2D(fPlayerX, fPlayerY, fPlayerX + (SIN(fPlayerHeading) * 0.8 + 0.1) * fXScale * 0.03, fPlayerY + (COS(fPlayerHeading) * 0.8 + 0.1) * 0.03, 0.0005, 0, 255, 0, 255)
	//_PLOT_LINE_2D_NORMALISED(fPlayerX, fPlayerY, fPlayerX + (SIN(fPlayerHeading) * 0.8 + 0.1) * fXScale * 0.05, fPlayerY + (COS(fPlayerHeading) * 0.8 + 0.1) * 0.05, 0, 255, 0, 255)
	
	// Draw tree points
	REPEAT kdtree.iNodesCount i
		vCurrent.X = CALL kdtree.getValue(kdtree.nodePool[i].iElementID, 0)
		vCurrent.Y = CALL kdtree.getValue(kdtree.nodePool[i].iElementID, 1)
		
		fNormalisedX = (vCurrent.X - vMinCoord.X) / ABSF(vMaxCoord.X - vMinCoord.X)
		fNormalisedY = (1.0 - (vCurrent.Y - vMinCoord.Y) / ABSF(vMaxCoord.Y - vMinCoord.Y))
		
		#IF IS_DEBUG_BUILD
			IF g_SimpleInteriorData.bVerboseKDTree
				PRINTLN("[KDTREE] DEBUG_DRAW_KD_TREE - Drawing node ", i, " which is ", kdtree.nodePool[i].iElementID, ", fNormalisedX: ", fNormalisedX, ", fNormalisedY: ", fNormalisedY, ", vMinCoord: ", vMinCoord, ", vMaxCoord: ", vMaxCoord, ", vCurrent: ", vCurrent)
			ENDIF
		#ENDIF
		
		bInRange = FALSE
		//bScriptRunning = FALSE
		
		REPEAT localData.iClosestSimpleInteriorsCount j
			IF localData.closestSimpleInteriors[j] = INT_TO_ENUM(SIMPLE_INTERIORS, kdtree.nodePool[i].iElementID)
				bInRange = TRUE
			ENDIF
		ENDREPEAT
		
		/*
		IF IS_SIMPLE_INTERIOR_EXT_SCRIPT_RUNNING(INT_TO_ENUM(SIMPLE_INTERIORS, kdtree.nodePool[i].iElementID))
			bScriptRunning = TRUE
		ENDIF
		*/
		
		iR = 0
		iG = 0
		iB = 255
		
		IF bInRange
			iR = 255
		ENDIF
		
		//IF bScriptRunning
			//iG = 255
		//ENDIF
		
		DRAW_RECT( (fNormalisedX * 0.8 + 0.1) * fXScale, (fNormalisedY * 0.8 + 0.1), 0.007 * fXScale, 0.007, iR, iG, iB, 255)
	ENDREPEAT
	
	
ENDPROC
#ENDIF

