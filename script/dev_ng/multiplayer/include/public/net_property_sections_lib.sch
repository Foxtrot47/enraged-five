///////////////////////////////////////////////////////////////////////////////////
// Name:        net_property_sections_lib.sch                                      //
// Description: Swap interiors													 //
// Written By:   Aleksej Leskin                                    				 //
///////////////////////////////////////////////////////////////////////////////////
 USING "globals.sch"

USING "model_enums.sch"
USING "commands_streaming.sch"
USING "commands_debug.sch"
USING "commands_object.sch"
USING "commands_entity.sch"

USING "net_property_sections.sch"
///////////////////////////////////////////////////////////////////////////////////////////////////////////
///    										FUNC/PROC													//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Add section to section list, sections must be added before CREATE_PROPERTY_SECTIONS can be called.
/// PARAMS:
///    sPropertySectionArray - holds the number of possible sections
///    iPropertySectionCount - represents the number of currently added/active/usable sections
///    sAnchorPointsArray - holds the number of possible anchors
///    ActiveAnchorPoints - represents the number of currently added/active/usable anchors
///    sNewSectionDescription - New description to add to the sPropertySectionArray array
/// RETURNS:
///    
FUNC BOOL ADD_PROPERTY_SECTION(PROPERTY_SECTION_STRUCT & sPropertySectionArray[], INT & iPropertySectionCount, ANCHOR_POINT_STRUCT &sAnchorPointsArray[], INT ActiveAnchorPoints, PROPERTY_SECTION_STRUCT sNewSectionDescription, INT iSectionID)
		IF iPropertySectionCount >= COUNT_OF(sPropertySectionArray)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AS - ADD_PROPERTY_SECTION - cant add more sections(Section array is full): COUNT_OF(sPropertySectionArray): ", COUNT_OF(sPropertySectionArray), " iPropertySectionCount: ", iPropertySectionCount)
			RETURN FALSE
		ENDIF
		
		IF iPropertySectionCount >= ActiveAnchorPoints
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AS - ADD_PROPERTY_SECTION - cant add more sections(not enough anchors): ActiveAnchorPoints: ", ActiveAnchorPoints, " iPropertySectionCount: ", iPropertySectionCount)
			RETURN FALSE
		ENDIF
		
		IF sNewSectionDescription.mSection = DUMMY_MODEL_FOR_SCRIPT
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AS - ADD_PROPERTY_SECTION - cant add more sections(default model):  sNewSectionDescription.mSection invalid model")
			RETURN FALSE
		ENDIF
		
		IF sAnchorPointsArray[iPropertySectionCount].mModel = DUMMY_MODEL_FOR_SCRIPT
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AS - ADD_PROPERTY_SECTION - cant add more sections(default model):  sAnchorPointsArray[iPropertySectionCount].mModel invalid model")
			RETURN FALSE
		ENDIF
		
		sPropertySectionArray[iPropertySectionCount] = sNewSectionDescription
		sPropertySectionArray[iPropertySectionCount].vPosition = sAnchorPointsArray[iPropertySectionCount].vPosition
		sPropertySectionArray[iPropertySectionCount].mAnchor = sAnchorPointsArray[iPropertySectionCount].mModel
		sPropertySectionArray[iPropertySectionCount].iSectionID = iSectionID
		iPropertySectionCount++
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AS - ADD_PROPERTY_SECTION - Section added succesfully!")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Swaps all anchors with desired sections
/// PARAMS:
///    sPropertySectionArray - 
///    iPropertySectionCount - 
PROC CREATE_PROPERTY_SECTIONS(PROPERTY_SECTION_STRUCT & sPropertySectionArray[], INT & iPropertySectionCount)
	INT iSectionID
	
	FOR iSectionID = 0 TO iPropertySectionCount - 1
		CREATE_MODEL_SWAP(sPropertySectionArray[iSectionID].vPosition, 1.0, sPropertySectionArray[iSectionID].mAnchor, sPropertySectionArray[iSectionID].mSection, TRUE)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CS - CREATE_PROPERTY_SECTIONS - Section Swap. ID: ", iSectionID,
		" Position: ", sPropertySectionArray[iSectionID].vPosition,
		" AnchorModel: ", GET_MODEL_NAME_FOR_DEBUG(sPropertySectionArray[iSectionID].mAnchor),
		" SectionMode: ", GET_MODEL_NAME_FOR_DEBUG(sPropertySectionArray[iSectionID].mSection))
	ENDFOR
ENDPROC

/// PURPOSE:
///    Remove current model swap , return anchors back.
/// PARAMS:
///    sPropertySectionArray - 
///    iPropertySectionCount - 
PROC DESTROY_PROPERTY_SECTIONS(PROPERTY_SECTION_STRUCT & sPropertySectionArray[], INT iPropertySectionCount)
	INT iSectionID
	
	FOR iSectionID = 0 TO iPropertySectionCount - 1
		REMOVE_MODEL_SWAP(sPropertySectionArray[iSectionID].vPosition, 1.0, sPropertySectionArray[iSectionID].mAnchor, sPropertySectionArray[iSectionID].mSection)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "DS - DESTROY_PROPERTY_SECTIONS - Section Swap. ID: ", iSectionID,
		" Position: ", sPropertySectionArray[iSectionID].vPosition,
		" AnchorModel: ", GET_MODEL_NAME_FOR_DEBUG(sPropertySectionArray[iSectionID].mAnchor),
		" SectionMode: ", GET_MODEL_NAME_FOR_DEBUG(sPropertySectionArray[iSectionID].mSection))
		
		IF DOES_ENTITY_EXIST(sPropertySectionArray[iSectionID].lightRig)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "DS - lightrig exists, destroying.")
			DELETE_ENTITY(sPropertySectionArray[iSectionID].lightRig)
		ENDIF
	ENDFOR
	CDEBUG1LN(DEBUG_SAFEHOUSE, "DS - DESTROY_PROPERTY_SECTIONS - All property sections removed, swaped back to anchors")
ENDPROC

/// PURPOSE:
///    If property needs to be rebuilt, section swapped. this must be called first to remove current sections.
/// PARAMS:
///    sPropertySectionArray - 
///    iPropertySectionCount - 
PROC REMOVE_PROPERTY_DESCRIPTION(PROPERTY_SECTION_STRUCT & sPropertySectionArray[], INT & iPropertySectionCount)
	INT iSectionID
	
	FOR iSectionID = 0 TO COUNT_OF(sPropertySectionArray) - 1
		sPropertySectionArray[iSectionID].mSection = DUMMY_MODEL_FOR_SCRIPT
		sPropertySectionArray[iSectionID].vPosition = <<0.0, 0.0, 0.0>>
		sPropertySectionArray[iSectionID].mAnchor = DUMMY_MODEL_FOR_SCRIPT
		sPropertySectionArray[iSectionID].iSectionType = 0
	ENDFOR
	
	iPropertySectionCount = 0
	CDEBUG1LN(DEBUG_SAFEHOUSE, "RD - REMOVE_PROPERTY_DESCRIPTION - All property description removed")
ENDPROC

/// PURPOSE: Restore anchor models, remove property models. clear section descriptions.
///    FULL CLEANUP OF SECTIONS
PROC CLEANUP_PROPERTY_SECTIONS(PROPERTY_SECTION_STRUCT & sPropertySectionArray[], INT & iPropertySectionCount)
	//Remove model swap
	DESTROY_PROPERTY_SECTIONS(sPropertySectionArray, iPropertySectionCount)
	//Remove Description
	REMOVE_PROPERTY_DESCRIPTION(sPropertySectionArray, iPropertySectionCount)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "CP - CLEANUP_PROPERTY_SECTIONS - All property sections cleaned up!")
ENDPROC

//////////////////////////////////////////////// ANCHORS ////////////////////////////////////////////////

PROC CLEAR_INTERIOR_ANCHORS(ANCHOR_POINT_STRUCT & sAnchorPoints[], INT & ActiveAnchorPoints)
	INT iAnchorID
	
	REPEAT COUNT_OF(sAnchorPoints) iAnchorID
		sAnchorPoints[iAnchorID].vPosition = <<0.0, 0.0, 0.0>>
		sAnchorPoints[iAnchorID].mModel = DUMMY_MODEL_FOR_SCRIPT
	ENDREPEAT
	ActiveAnchorPoints = 0
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "CA - CLEAR_INTERIOR_ANCHORS - interior anchors cleared")
ENDPROC

/// PURPOSE:
///    Private, add new anchor to the anchor array.
/// PARAMS:
///    vAnchorPoints - 
///    ActiveAnchorPoints - 
///    vNewAnchorPoint - position, where to search for the anchor
///    mNewAnchorPoint - model of anchor taht will be model swaped.
/// RETURNS:
///    
FUNC BOOL _ADD_INTERIOR_ANCHOR_(ANCHOR_POINT_STRUCT &vAnchorPoints[], INT &ActiveAnchorPoints, VECTOR vNewAnchorPoint, MODEL_NAMES mNewAnchorPoint)
	IF ActiveAnchorPoints >= COUNT_OF(vAnchorPoints)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AA - ADD_INTERIOR_ANCHOR - cant add more anchor points: COUNT_OF(vAnchorPoints): ", COUNT_OF(vAnchorPoints), " ActiveAnchorPoints: ", ActiveAnchorPoints)
		RETURN FALSE
	ENDIF
	
	vAnchorPoints[ActiveAnchorPoints].vPosition = vNewAnchorPoint
	vAnchorPoints[ActiveAnchorPoints].mModel = mNewAnchorPoint
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AA - ADD_INTERIOR_ANCHOR - New anchor added at Index: ", ActiveAnchorPoints, " Position: ", vNewAnchorPoint)
	ActiveAnchorPoints++
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Output for debug, print all anchors that were grabed from an interior
/// PARAMS:
///    vAnchorPoints - 
///    ActiveAnchorPoints - 
PROC PRINT_INTERIOR_ANCHORS(ANCHOR_POINT_STRUCT &vAnchorPoints[], INT ActiveAnchorPoints)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "")
	INT iAnchorID
	CDEBUG1LN(DEBUG_SAFEHOUSE, "PA - PRINT_INTERIOR_ANCHORS: Active Anchor Point Count: ", ActiveAnchorPoints)
	REPEAT ActiveAnchorPoints iAnchorID
		CDEBUG1LN(DEBUG_SAFEHOUSE, "PA - PRINT_INTERIOR_ANCHORS: AnchorID: ", iAnchorID, " AnchorPosition: ", vAnchorPoints[iAnchorID].vPosition, " AnchorModel: ", GET_MODEL_NAME_FOR_DEBUG(vAnchorPoints[iAnchorID].mModel))
	ENDREPEAT
	CDEBUG1LN(DEBUG_SAFEHOUSE, "")
ENDPROC

/// PURPOSE:
///    Output for debug currentnly asembled sections
/// PARAMS:
///    sPropertySectionArray - 
///    iPropertySectionCount - 
PROC PRINT_PROPERTY_SECTIONS(PROPERTY_SECTION_STRUCT & sPropertySectionArray[], INT & iPropertySectionCount)
	INT iSectionID
	CDEBUG1LN(DEBUG_SAFEHOUSE, "")
	CDEBUG1LN(DEBUG_SAFEHOUSE, "$$$$$$$$$$$$$$$$$$$$$")
	CDEBUG1LN(DEBUG_SAFEHOUSE, "PS - PRINT_PROPERTY_SECTIONS - SectionArraySize: ", COUNT_OF(sPropertySectionArray), " iPropertySectionCount: ", iPropertySectionCount)
	FOR iSectionID = 0 TO iPropertySectionCount - 1
		IF iSectionID <> 0
			CDEBUG1LN(DEBUG_SAFEHOUSE, "")
		ENDIF
		CDEBUG1LN(DEBUG_SAFEHOUSE, "PS - PRINT_PROPERTY_SECTIONS - CurrentSection. ID: ", iSectionID,
		" Position: ", sPropertySectionArray[iSectionID].vPosition,
		" AnchorModel: ", GET_MODEL_NAME_FOR_DEBUG(sPropertySectionArray[iSectionID].mAnchor),
		" SectionMode: ", GET_MODEL_NAME_FOR_DEBUG(sPropertySectionArray[iSectionID].mSection),
		" SectionTYPE: ", sPropertySectionArray[iSectionID].iSectionType)
	ENDFOR
	CDEBUG1LN(DEBUG_SAFEHOUSE, "$$$$$$$$$$$$$$$$$$$$$")
	CDEBUG1LN(DEBUG_SAFEHOUSE, "")
ENDPROC

