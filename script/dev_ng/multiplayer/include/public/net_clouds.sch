////////////////////////////////////////////////////////////////////////////////////
///  Speirs, Jan 2015 for 2193949
///    

USING "globals.sch"

FUNC BOOL ARE_CLOUDS_FADING_IN()
	
	RETURN g_TransitionSessionNonResetVars.sCloudData.eCloudState = ENUMCLOUDSTATE_FADING_IN
ENDFUNC

FUNC BOOL ARE_CLOUDS_FADING_OUT()
	
	RETURN g_TransitionSessionNonResetVars.sCloudData.eCloudState = ENUMCLOUDSTATE_FADING_OUT
ENDFUNC

FUNC BOOL ARE_CLOUDS_FADED_IN()
	
	RETURN g_TransitionSessionNonResetVars.sCloudData.eCloudState = ENUMCLOUDSTATE_ON
ENDFUNC

FUNC BOOL ARE_CLOUDS_FADED_OUT()
	
	RETURN g_TransitionSessionNonResetVars.sCloudData.eCloudState = ENUMCLOUDSTATE_OFF
ENDFUNC

PROC ACTIVATE_CLOUD_FADE_IN(BOOL bActivated)
	
	#IF IS_DEBUG_BUILD
	IF g_TransitionSessionNonResetVars.sCloudData.bFadeInActivated <> bActivated
		PRINTLN("NET_CLOUDS - bFadeInActivated = ", bActivated)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	g_TransitionSessionNonResetVars.sCloudData.bFadeInActivated = bActivated
ENDPROC

PROC ACTIVATE_CLOUD_FADE_OUT(BOOL bActivated)

	#IF IS_DEBUG_BUILD
	IF g_TransitionSessionNonResetVars.sCloudData.bFadeOutActivated <> bActivated
		PRINTLN("NET_CLOUDS - bFadeOutActivated = ", bActivated)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	g_TransitionSessionNonResetVars.sCloudData.bFadeOutActivated = bActivated
ENDPROC

FUNC BOOL SHOULD_FADE_CLOUDS_IN()
	RETURN g_TransitionSessionNonResetVars.sCloudData.bFadeInActivated
ENDFUNC

FUNC BOOL SHOULD_FADE_CLOUDS_OUT()
	RETURN g_TransitionSessionNonResetVars.sCloudData.bFadeOutActivated
ENDFUNC

FUNC BOOL FADE_CLOUDS(BOOL bFadeIn)
	
	IF bFadeIn
		g_TransitionSessionNonResetVars.sCloudData.fAlpha = (g_TransitionSessionNonResetVars.sCloudData.fAlpha + cfDEFAULT_CLOUDS__ADJUST)
	ELSE
		g_TransitionSessionNonResetVars.sCloudData.fAlpha = (g_TransitionSessionNonResetVars.sCloudData.fAlpha - cfDEFAULT_CLOUDS__ADJUST)
	ENDIF
	
	// Caps
	IF g_TransitionSessionNonResetVars.sCloudData.fAlpha > cfDEFAULT_CLOUDS_MAX_ALPHA
		g_TransitionSessionNonResetVars.sCloudData.fAlpha = cfDEFAULT_CLOUDS_MAX_ALPHA
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sCloudData.fAlpha < cfDEFAULT_CLOUDS_MIN_ALPHA
		g_TransitionSessionNonResetVars.sCloudData.fAlpha = cfDEFAULT_CLOUDS_MIN_ALPHA
	ENDIF
	
	SET_CLOUDS_ALPHA(g_TransitionSessionNonResetVars.sCloudData.fAlpha)
	
	IF bFadeIn
		IF g_TransitionSessionNonResetVars.sCloudData.fAlpha >= cfDEFAULT_CLOUDS_MAX_ALPHA
			RETURN TRUE
		ENDIF
	ELSE
		IF g_TransitionSessionNonResetVars.sCloudData.fAlpha <= cfDEFAULT_CLOUDS_MIN_ALPHA
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_CLOUD_STATE(ENUM_CLOUD_STATE eCloudStageGoTo)

	#IF IS_DEBUG_BUILD
		PRINTNL()
		SWITCH g_TransitionSessionNonResetVars.sCloudData.eCloudState
			CASE ENUMCLOUDSTATE_ON 			PRINTLN(" [NET_CLOUDS] CURRENT_CLOUD_STATE = ENUMCLOUDSTATE_ON ")			BREAK
			CASE ENUMCLOUDSTATE_OFF			PRINTLN(" [NET_CLOUDS] CURRENT_CLOUD_STATE = ENUMCLOUDSTATE_OFF ")			BREAK
			CASE ENUMCLOUDSTATE_FADING_IN	PRINTLN(" [NET_CLOUDS] CURRENT_CLOUD_STATE = ENUMCLOUDSTATE_FADING_IN ")	BREAK
			CASE ENUMCLOUDSTATE_FADING_OUT	PRINTLN(" [NET_CLOUDS] CURRENT_CLOUD_STATE = ENUMCLOUDSTATE_FADING_OUT ")	BREAK
		ENDSWITCH
		PRINTNL()
		SWITCH eCloudStageGoTo
			CASE ENUMCLOUDSTATE_ON 			PRINTLN(" [NET_CLOUDS] SET_CLOUD_STATE = ENUMCLOUDSTATE_ON ")				BREAK
			CASE ENUMCLOUDSTATE_OFF			PRINTLN(" [NET_CLOUDS] SET_CLOUD_STATE = ENUMCLOUDSTATE_OFF ")				BREAK
			CASE ENUMCLOUDSTATE_FADING_IN	PRINTLN(" [NET_CLOUDS] SET_CLOUD_STATE = ENUMCLOUDSTATE_FADING_IN ")		BREAK
			CASE ENUMCLOUDSTATE_FADING_OUT	PRINTLN(" [NET_CLOUDS] SET_CLOUD_STATE = ENUMCLOUDSTATE_FADING_OUT ")		BREAK
		ENDSWITCH
		PRINTNL()
	#ENDIF

	g_TransitionSessionNonResetVars.sCloudData.eCloudState = eCloudStageGoTo
ENDPROC

PROC MAINTAIN_CLOUD_ALPHA()

	// Debug assert
	#IF IS_DEBUG_BUILD
		IF SHOULD_FADE_CLOUDS_IN()
		AND SHOULD_FADE_CLOUDS_OUT()
			PRINTLN("[NET_CLOUDS] - bFadeInActivated and bFadeOutActivated. No.")
			SCRIPT_ASSERT("[NET_CLOUDS] - bFadeInActivated and bFadeOutActivated. No.")
			g_TransitionSessionNonResetVars.sCloudData.bFadeOutActivated = FALSE
			g_TransitionSessionNonResetVars.sCloudData.bFadeInActivated = TRUE
		ENDIF
	#ENDIF
	
	// SP cleanup
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_SP
		IF NOT ARE_CLOUDS_FADED_OUT()
		AND NOT ARE_CLOUDS_FADED_OUT()
			PRINTLN("[NET_CLOUDS] - GET_CURRENT_GAMEMODE() = GAMEMODE_SP - need to fade out clouds.")
			ACTIVATE_CLOUD_FADE_OUT(FALSE)
			ACTIVATE_CLOUD_FADE_IN(TRUE)
		ENDIF
	ENDIF
	
	// Cloud logic states
	SWITCH g_TransitionSessionNonResetVars.sCloudData.eCloudState
	
		// Default clouds state
		CASE ENUMCLOUDSTATE_ON
		
			// Set clouds to default
			IF GET_CLOUDS_ALPHA() <> cfDEFAULT_CLOUDS_MAX_ALPHA
				SET_CLOUDS_ALPHA(cfDEFAULT_CLOUDS_MAX_ALPHA)
			ENDIF

			IF SHOULD_FADE_CLOUDS_OUT()
				SET_CLOUD_STATE(ENUMCLOUDSTATE_FADING_OUT)
			ENDIF
		BREAK
		
		CASE ENUMCLOUDSTATE_OFF
		
			// Set clouds to off
			IF GET_CLOUDS_ALPHA() <> cfDEFAULT_CLOUDS_MIN_ALPHA
				SET_CLOUDS_ALPHA(cfDEFAULT_CLOUDS_MIN_ALPHA)
			ENDIF

			IF SHOULD_FADE_CLOUDS_IN()
				SET_CLOUD_STATE(ENUMCLOUDSTATE_FADING_IN)
			ENDIF
		BREAK
		
		CASE ENUMCLOUDSTATE_FADING_IN
		
			IF FADE_CLOUDS(TRUE)
				SET_CLOUD_STATE(ENUMCLOUDSTATE_ON)
			ENDIF
			
			IF SHOULD_FADE_CLOUDS_OUT()
				SET_CLOUD_STATE(ENUMCLOUDSTATE_FADING_OUT)
			ENDIF
		BREAK
		
		CASE ENUMCLOUDSTATE_FADING_OUT
		
			IF FADE_CLOUDS(FALSE)
				SET_CLOUD_STATE(ENUMCLOUDSTATE_OFF)
			ENDIF
			
			IF SHOULD_FADE_CLOUDS_IN()
				SET_CLOUD_STATE(ENUMCLOUDSTATE_FADING_IN)
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

