USING "SceneTool_public.sch"

USING "cutscene_public.sch"
USING "rc_helper_functions.sch"
USING "net_cutscene.sch"

USING "net_spawn.sch"
USING "net_realty_details.sch"

#IF IS_DEBUG_BUILD
USING "net_realty_scene_debug.sch"
#ENDIF

ENUM enumNET_REALTY_5_SCENEPans
	NET_REALTY_5_SCENE_PAN_null,
	NET_REALTY_5_SCENE_PAN_MAX
ENDENUM

ENUM enumNET_REALTY_5_SCENECuts
	NET_REALTY_5_SCENE_CUT_shot = 0,
	NET_REALTY_5_SCENE_CUT_MAX
ENDENUM

ENUM enumNET_REALTY_5_SCENEMarkers
	NET_REALTY_5_SCENE_MARKER_walkTo,
	NET_REALTY_5_SCENE_MARKER_MAX
ENDENUM

ENUM enumNET_REALTY_5_SCENEPlacers
	NET_REALTY_5_SCENE_PLACER_startCoords_0,
	NET_REALTY_5_SCENE_PLACER_startCoords_1,
	NET_REALTY_5_SCENE_PLACER_MAX
ENDENUM

STRUCT STRUCT_NET_REALTY_5_SCENE
	structSceneTool_Pan		mPans[NET_REALTY_5_SCENE_PAN_MAX]
	structSceneTool_Cut		mCuts[NET_REALTY_5_SCENE_CUT_MAX]
	
	structSceneTool_Marker	mMarkers[NET_REALTY_5_SCENE_MARKER_MAX]
	structSceneTool_Placer	mPlacers[NET_REALTY_5_SCENE_PLACER_MAX]
	
	BOOL					bEnablePans[NET_REALTY_5_SCENE_PAN_MAX]
	VECTOR 					vDoorPos1
	VECTOR 					vDoorPos2
	VECTOR 					vDoorPos3
	FLOAT					fExitDelay
ENDSTRUCT

CONST_INT FAKE_PED_EXIT_GARAGE_WARP_POSSIBLE		0
CONST_INT FAKE_PED_EXIT_GARAGE_WARP_DONE			1
CONST_INT FAKE_PED_EXIT_GARAGE_ADDED_CUST_SPAWN		2

STRUCT FAKE_PED_EXIT_GARAGE_DATA
	PED_INDEX fakeExitPed
	MP_DOOR_DETAILS garageDoor
	INT iBS
ENDSTRUCT

FUNC BOOL Private_Get_NET_REALTY_5_SCENE(INT iBuildingID, STRUCT_NET_REALTY_5_SCENE& scene	#IF IS_DEBUG_BUILD	,	BOOL bIgnoreAssert = FALSE	#ENDIF	) 
	SWITCH iBuildingID
		CASE 0	RETURN FALSE	BREAK
		
		//Luke Howard, Thu 12/09/2013 15:53
		CASE MP_PROPERTY_BUILDING_1          //High apt 1,2,3,4
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vPos = <<0.0000, 0.0000, 0.0000>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vRot = <<0.0000, 0.0000, 0.0000>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vPos = <<0.0000, 0.0000, 0.0000>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vRot = <<0.0000, 0.0000, 0.0000>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fFov = 0.0000
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fShake = 0.0000
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fDuration = 0.0000
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vPos = <<-797.2457, 320.9005, 86.5710>>
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vRot = <<-1.4479, 0.0000, -173.3266>>
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fFov = 50.0000
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fShake = 0.2500
//			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vPos = <<-800.4786, 308.6443, 88.1763>>
//			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vRot = <<-17.3271, -0.0000, -26.6875>>
//			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fFov = 50.0000
//			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fShake = 0.2500
			scene.mMarkers[NET_REALTY_5_SCENE_MARKER_walkTo].vPos = <<0.0000, 0.0000, 0.0000>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos = <<-795.9908, 320.5264, 84.6913>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].fRot = 180.9339
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos = <<-796.4648, 308.5788, 84.7048>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].fRot = 180.0000
			scene.vDoorPos1 = <<-798.967651,313.991486,84.705750>> 
			scene.vDoorPos2 = <<-795.849182,314.308655,84.701675>>
			scene.vDoorPos3 = <<-792.878845,314.504852,84.699097>>
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Forrest Karbowski, Thu 12/09/2013 23:29
		CASE MP_PROPERTY_BUILDING_6          //High apt 14,15
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vPos = <<-932.7474, -383.9246, 42.9613>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vRot = <<89.4999, 0.0000, 94.2595>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vPos = <<-932.7474, -383.9246, 45.4613>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vRot = <<-89.4999, 0.0000, 94.2595>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fFov = 50.0000
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fShake = 0.2500
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fDuration = 6.5000
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vPos = <<-878.9250, -355.4584, 35.7946>>
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vRot = <<6.6901, -0.0000, -167.9745>>
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fFov = 50.0000
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fShake = 0.2500
			scene.mMarkers[NET_REALTY_5_SCENE_MARKER_walkTo].vPos = <<-876.2924, -364.0820, 35.5610>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos = <<-881.0963, -354.6912, 34.2618>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].fRot = 204.4800
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos = <<-876.3240, -364.1094, 35.5636>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].fRot = 207.3600
			scene.vDoorPos1 = <<-876.418823,-358.469299,34.920353>> 
			scene.vDoorPos2 = <<-879.781738,-360.224945,34.828461>> 
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Forrest Karbowski, Thu 12/09/2013 23:29
		CASE MP_PROPERTY_BUILDING_7          //High apt 16,17
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vPos = <<-619.1315, 37.8841, 47.5883>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vRot = <<89.4999, 0.0000, 94.2595>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vPos = <<-619.1315, 37.8841, 50.0883>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vRot = <<-89.4999, 0.0000, 94.2595>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fFov = 50.0000
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fShake = 0.2500
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fDuration = 6.5000
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vPos = <<-626.4656, 59.2381, 44.0759>>
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vRot = <<1.5216, 0.0000, 107.2253>>
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fFov = 50.0000
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fShake = 0.2500
			scene.mMarkers[NET_REALTY_5_SCENE_MARKER_walkTo].vPos = <<-635.0194, 56.9403, 42.7605>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos = <<-623.6274, 57.5262, 42.7295>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].fRot = 99.0000
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos = <<-634.6904, 57.2514, 42.7774>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].fRot = 0.0000
			scene.vDoorPos1 = <<-629.773926,59.266457,42.724987>>
			scene.vDoorPos2 = <<-629.822937,55.832752,42.724960>> 
			scene.vDoorPos3 = <<-629.732056,52.372917,42.725010>> 
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Forrest Karbowski, Thu 12/09/2013 23:29
		CASE MP_PROPERTY_BUILDING_5	//High apt 12,13
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vPos = <<-47.3145, -585.9766, 41.9593>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vRot = <<89.4999, 0.0000, 94.2595>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vPos = <<-47.3145, -585.9766, 44.4593>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vRot = <<-89.4999, 0.0000, 94.2595>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fFov = 50.0000
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fShake = 0.2500
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fDuration = 6.5000
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vPos = <<-37.4006, -619.0651, 35.6844>>
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vRot = <<0.1928, 0.0000, -127.8392>>
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fFov = 50.0000
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fShake = 0.2500
			scene.mMarkers[NET_REALTY_5_SCENE_MARKER_walkTo].vPos = <<-27.0036, -623.9375, 34.4608>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos = <<-37.4249, -620.4834, 34.0698>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].fRot = 253.8000
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos = <<-32.5265, -622.1819, 34.1244>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].fRot = 253.8000
			scene.vDoorPos1 = <<-33.880001,-621.538513,34.038071>> 
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Forrest Karbowski, Thu 12/09/2013 23:29
		CASE MP_PROPERTY_BUILDING_2          //High apt 5,6
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vPos = <<-252.5713, -949.9199, 35.2210>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vRot = <<89.4999, 0.0000, 94.2595>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vPos = <<-252.5713, -949.9199, 37.7210>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vRot = <<-89.4999, 0.0000, 94.2595>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fFov = 50.0000
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fShake = 0.2500
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fDuration = 6.5000
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vPos = <<-286.3485, -991.8517, 24.5451>>
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vRot = <<-0.5902, -0.0000, -124.2111>>
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fFov = 50.0000
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fShake = 0.2500
			scene.mMarkers[NET_REALTY_5_SCENE_MARKER_walkTo].vPos = <<-276.9672, -997.3885, 24.0628>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos = <<-287.2802, -993.2685, 23.1368>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].fRot = 251.2500
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos = <<-276.7173, -997.4412, 24.1063>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].fRot = 247.5000
			scene.vDoorPos1 = <<-282.026947,-993.643066,23.303766>> 
			scene.vDoorPos2 = <<-283.157166,-996.661743,23.483461>>
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke Howard, 18-09-2013 20:36:39
		CASE MP_PROPERTY_BUILDING_3          //High apt 7,8
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vPos = <<-1443.0938, -544.7684, 38.7424>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vRot = <<89.4999, 0.0000, 94.2595>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vPos = <<-1443.0938, -544.7684, 41.2424>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vRot = <<-89.4999, 0.0000, 94.2595>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fFov = 50.0000
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fShake = 0.2500
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fDuration = 6.5000
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vPos = <<-1454.1522, -509.1251, 32.2253>>
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vRot = <<-2.2188, 0.0000, 19.8287>>
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fFov = 50.0000
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fShake = 0.2500
			scene.mMarkers[NET_REALTY_5_SCENE_MARKER_walkTo].vPos = <<-1457.6260, -500.9148, 31.4627>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos = <<-1452.4513, -508.5775, 30.9113>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].fRot = 29.1600
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos = <<-1452.4985, -508.4085, 30.9163>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].fRot = 30.6000
			scene.vDoorPos1 = <<-1456.828247,-505.130096,31.242815>>
			scene.vDoorPos2 = <<-1454.213867,-503.342804,30.570770>>
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Forrest Karbowski, Thu 12/09/2013 23:29
		CASE MP_PROPERTY_BUILDING_4          //High apt 9,10,11
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vPos = <<-913.8500, -455.1392, 43.5999>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vRot = <<89.4999, 0.0000, 94.2595>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vPos = <<-913.8500, -455.1392, 46.0999>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vRot = <<-89.4999, 0.0000, 94.2595>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fFov = 50.0000
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fShake = 0.2500
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fDuration = 6.5000
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vPos = <<-815.3887, -433.1594, 36.5962>>
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vRot = <<6.3730, 0.0000, 123.6414>>
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fFov = 50.0000
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fShake = 0.2500
			scene.mMarkers[NET_REALTY_5_SCENE_MARKER_walkTo].vPos = <<-824.7270, -438.2999, 35.6399>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos = <<-818.2505, -431.6903, 35.0843>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].fRot = 136.8000
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos = <<-824.8032, -438.3822, 35.6399>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].fRot = 133.9200
			scene.vDoorPos1 = <<-819.302307,-438.809143,35.626488>> 
			scene.vDoorPos2 = <<-821.377380,-435.120636,35.794361>> 
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
	
//		CASE MP_PROPERTY_BUILDING_19	//Low apt 3
//			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vPos = <<-811.7045, -984.1961, 18.1538>>
//			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vRot = <<89.4999, 0.0000, 94.2595>>
//			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vPos = <<-811.7045, -984.1961, 20.6538>>
//			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vRot = <<-89.4999, 0.0000, 94.2595>>
//			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fFov = 50.0000
//			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fShake = 0.2500
//			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fDuration = 6.5000
//			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vPos = <<-812.1573, -980.0467, 15.5363>>
//			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vRot = <<-23.5946, 0.0000, 146.8649>>
//			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fFov = 50.0000
//			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fShake = 0.2500
//			scene.mMarkers[NET_REALTY_5_SCENE_MARKER_walkTo].vPos = <<-813.8054, -983.1201, 13.1123>>
//			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos = <<-812.2863, -980.0726, 13.2775>>
//			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].fRot = 165.0000
//			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos = <<-813.8625, -983.2641, 13.1069>>
//			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].fRot = 180.0000
//			scene.bEnablePans[0] = FALSE
//			scene.fExitDelay = 0.0000
//			//SCRIPT_ASSERT("Private_Get_NET_REALTY_5_SCENE() placeholder for #1616772")
//			RETURN TRUE
//		BREAK
//		CASE MP_PROPERTY_BUILDING_15	//Med apt 8
//			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vPos = <<-829.1362, -855.0104, 23.6297>>
//			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vRot = <<89.4999, 0.0000, 94.2595>>
//			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vPos = <<-829.1362, -855.0104, 26.1297>>
//			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vRot = <<-89.4999, 0.0000, 94.2595>>
//			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fFov = 50.0000
//			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fShake = 0.2500
//			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fDuration = 6.5000
//			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vPos = <<-831.1851, -862.8480, 22.8113>>
//			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vRot = <<-22.5707, -0.0000, -4.8663>>
//			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fFov = 50.0000
//			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fShake = 0.2500
//			scene.mMarkers[NET_REALTY_5_SCENE_MARKER_walkTo].vPos = <<-831.6716, -856.0882, 18.5957>>
//			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos = <<-831.4265, -862.6751, 19.6946>>
//			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].fRot = 0.0000
//			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos = <<-831.6147, -855.9597, 18.5963>>
//			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].fRot = 0.0000
//			scene.bEnablePans[0] = FALSE
//			scene.fExitDelay = 0.0000
//			//SCRIPT_ASSERT("Private_Get_NET_REALTY_5_SCENE() placeholder for #1615201")
//			RETURN TRUE
//		BREAK
		
		//Luke Howard, 02/09/2013 12:24 (#1618527)
		CASE MP_PROPERTY_BUILDING_23	//Low apt 7
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vPos = <<-1608.8514, -429.1840, 44.4390>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vRot = <<89.4999, 0.0000, 94.2595>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vPos = <<-1608.8514, -429.1840, 46.9390>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vRot = <<-89.4999, 0.0000, 94.2595>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fFov = 50.0000
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fShake = 0.2500
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fDuration = 6.5000
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vPos = <<-1601.8047, -442.4136, 39.4131>>
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vRot = <<-6.7813, -0.0000, 142.8609>>
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fFov = 50.0000
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fShake = 0.2500
			scene.mMarkers[NET_REALTY_5_SCENE_MARKER_walkTo].vPos = <<-1609.8853, -452.7733, 37.1659>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos = <<-1601.3816, -442.5541, 37.2166>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].fRot = 142.5000
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos = <<-1610.0961, -453.1568, 37.1576>>
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].fRot = 142.5000
			scene.vDoorPos1 = <<-1606.561279,-445.916870,37.216648>>
			scene.vDoorPos2 = <<-1603.390503,-448.632263,37.216648>> 
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		DEFAULT
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vPos = GET_MP_PROPERTY_BUILDING_WORLD_POINT(iBuildingID)			+ <<0.0, 0.0, 5.0>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mStart.vRot = <<89.4999, -0.0000, 94.2595>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vPos = GET_MP_PROPERTY_BUILDING_WORLD_POINT(iBuildingID)			+ <<0.0, 0.0, 7.5>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vRot = <<-89.4999, -0.0000, 94.2595>>
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fFov = GET_GAMEPLAY_CAM_FOV()
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fShake = 0.25
			scene.mPans[NET_REALTY_5_SCENE_PAN_null].fDuration = 6.500
			scene.bEnablePans[NET_REALTY_5_SCENE_PAN_null] = FALSE
			
			scene.fExitDelay = 0.0000
			
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vPos = scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vPos
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].mCam.vRot = scene.mPans[NET_REALTY_5_SCENE_PAN_null].mEnd.vRot
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fFov = scene.mPans[NET_REALTY_5_SCENE_PAN_null].fFov
			scene.mCuts[NET_REALTY_5_SCENE_CUT_shot].fShake = scene.mPans[NET_REALTY_5_SCENE_PAN_null].fShake
			
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos = GET_MP_PROPERTY_BUILDING_WORLD_POINT(iBuildingID)
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].fRot = 0
			
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos = GET_MP_PROPERTY_BUILDING_WORLD_POINT(iBuildingID)
			scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].fRot = 0
			
			scene.mMarkers[NET_REALTY_5_SCENE_PAN_null].vPos = scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos			+ <<1.0, 0.0, 0.0>>
			
			#IF IS_DEBUG_BUILD
			IF NOT bIgnoreAssert
//				TEXT_LABEL_63 str
//				str = "Private_Get_NET_REALTY_5_SCENE() invalid ID "
//				str += GET_STRING_FROM_STRING(Private_Get_Net_Realty_Building_Name(iBuildingID), GET_LENGTH_OF_LITERAL_STRING("MP_PROPERTY_"), GET_LENGTH_OF_LITERAL_STRING(Private_Get_Net_Realty_Building_Name(iBuildingID)))
//				SCRIPT_ASSERT(str)
			ENDIF
			#ENDIF
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


ENUM GARAGE_PED_LEAVE_CUTSCENE_STAGE
	GARAGE_PED_LEAVE_CUTSCENE_INIT = 0,
	GARAGE_PED_LEAVE_CUTSCENE_TRIGGER_CUT,
	GARAGE_PED_LEAVE_CUTSCENE_WAIT_FOR_LOAD,
	GARAGE_PED_LEAVE_CUTSCENE_WAIT_FOR_OPEN_DOOR,
	GARAGE_PED_LEAVE_CUTSCENE_WAIT_FOR_EXIT,
	
	GARAGE_PED_WARP_TO_UNOCCUPIED_AREA,
	GARAGE_PED_SET_IN_UNOCCUPIED_AREA,
	
	GARAGE_PED_LEAVE_CUTSCENE_COMPLETE
ENDENUM

FUNC BOOL NET_REALTY_5_SCENE_IS_DOOR_BLOCKED(STRUCT_NET_REALTY_5_SCENE &scene)
	FLOAT fRadius = 2.0
	IF NOT ARE_VECTORS_EQUAL(<<0,0,0>>,scene.vDoorPos1)
		IF IS_POSITION_OCCUPIED(
						scene.vDoorPos1,
						fRadius,
						FALSE, TRUE, FALSE, FALSE, FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	IF NOT ARE_VECTORS_EQUAL(<<0,0,0>>,scene.vDoorPos2)
		IF IS_POSITION_OCCUPIED(
						scene.vDoorPos2,
						fRadius,
						FALSE, TRUE, FALSE, FALSE, FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	IF NOT ARE_VECTORS_EQUAL(<<0,0,0>>,scene.vDoorPos1)
		IF IS_POSITION_OCCUPIED(
						scene.vDoorPos3,
						fRadius,
						FALSE, TRUE, FALSE, FALSE, FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_FOR_CUTSCENE_OK(  GARAGE_PED_LEAVE_CUTSCENE_STAGE &garagePedLeaveCutStage, INT iCurrentProperty, FAKE_PED_EXIT_GARAGE_DATA &fakeExitData  )
	IF GET_PROPERTY_SIZE_TYPE(iCurrentProperty) = PROP_SIZE_TYPE_LARGE_APT //GET_PROPERTY_BUILDING(iCurrentProperty) = MP_PROPERTY_BUILDING_1
		IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
			RETURN FALSE
		ENDIF
		IF garagePedLeaveCutStage > GARAGE_PED_LEAVE_CUTSCENE_INIT
		AND garagePedLeaveCutStage != GARAGE_PED_LEAVE_CUTSCENE_COMPLETE
			IF IS_PED_INJURED(fakeExitData.fakeExitPed)
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
	
	RETURN IS_NET_PLAYER_OK(PLAYER_ID())
	
	ENDIF
	RETURN TRUE
ENDFUNC

PROC GET_PED_EXIT_GARAGE_COORDS(MP_PROP_OFFSET_STRUCT &mpOffset,INT iCurrentProperty,INT iIndex) //WARPING WORK CDM 176870
	SWITCH iIndex 
		CASE 17
		BREAK
		DEFAULT
			mpOffset.vLoc = mpProperties[iCurrentProperty].garage.vExitPlayerPos
			mpOffset.vRot = <<0,0,0>>
			mpOffset.vRot.z = mpProperties[iCurrentProperty].garage.fFromHousePlayerHeading
		BREAK
	ENDSWITCH
ENDPROC

PROC DO_GARAGE_PED_LEAVE_WARP(INT iCurrentProperty,FAKE_PED_EXIT_GARAGE_DATA &fakeExitData)
	IF IS_BIT_SET(fakeExitData.iBS,FAKE_PED_EXIT_GARAGE_WARP_POSSIBLE)
	AND NOT IS_BIT_SET(fakeExitData.iBS,FAKE_PED_EXIT_GARAGE_WARP_DONE)
		IF NOT IS_BIT_SET(fakeExitData.iBS,FAKE_PED_EXIT_GARAGE_ADDED_CUST_SPAWN)
			INT i
			MP_PROP_OFFSET_STRUCT tempOffset

			USE_CUSTOM_SPAWN_POINTS(TRUE,FALSE,FALSE)
			REPEAT NUM_NETWORK_PLAYERS i
				GET_PED_EXIT_GARAGE_COORDS(tempOffset,iCurrentProperty,i)
				ADD_CUSTOM_SPAWN_POINT(tempOffset.vLoc,tempOffset.vRot.z,1.0-(i/NUM_NETWORK_PLAYERS))	
			ENDREPEAT
			SET_BIT(fakeExitData.iBS,FAKE_PED_EXIT_GARAGE_ADDED_CUST_SPAWN)
		ELSE
			IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS,FALSE,FALSE,FALSE,TRUE)
				#IF IS_DEBUG_BUILD
				PRINTLN("DO_GARAGE_PED_LEAVE_WARP: NETWORK_FADE_IN_ENTITY -1276 ")
				#ENDIF
				NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE)
				CLEAR_CUSTOM_SPAWN_POINTS()
				SET_BIT(fakeExitData.iBS,FAKE_PED_EXIT_GARAGE_WARP_DONE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
FUNC BOOL DO_GARAGE_PED_LEAVE_THIS_CUTSCENE(STRUCT_NET_REALTY_5_SCENE &scene, INT iCurrentProperty,
		GARAGE_PED_LEAVE_CUTSCENE_STAGE &garagePedLeaveCutStage,
		INT &iGarageCutsceneBitset, INT &iGarageCutTimer, BOOL &bDisabledRadar,
		CAMERA_INDEX &camGarageCutscene
		,FAKE_PED_EXIT_GARAGE_DATA &fakeExitData
		 )
	BOOL bReset_garage_ped_leave_cutscene = FALSE
	
	CONST_INT GARAGE_CUT_SIMULATE_PLAYER_WALK		10000
	CONST_INT GARAGE_CUT_GAMECAM_INTERP				6000
	
	CONST_INT GARAGE_CUT_SKIP_TIME_SHORT			5000
	CONST_INT GARAGE_CUT_SKIP_TIME_LONG				13000
	
	CONST_INT GARAGE_CUT_BS_INTERPOLATE_STARTED		0
	CONST_INT GARAGE_CUT_BS_CONTROL_RETURNED		1
	
//	#IF IS_DEBUG_BUILD
//	#IF FEATURE_HEIST_PLANNING
//	
//	IF GET_PROPERTY_BUILDING(iCurrentProperty) = MP_PROPERTY_BUILDING_1
//		IF garagePedLeaveCutStage >GARAGE_PED_LEAVE_CUTSCENE_TRIGGER_CUT
//			IF IS_DOOR_REGISTERED_WITH_SYSTEM(fakeExitData.garageDoor.iDoorHash)
//				IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(fakeExitData.garageDoor.iDoorHash) = DOORSTATE_INVALID
//					IF DOOR_SYSTEM_GET_DOOR_STATE(fakeExitData.garageDoor.iDoorHash) != DOORSTATE_UNLOCKED
//						DOOR_SYSTEM_SET_DOOR_STATE(fakeExitData.garageDoor.iDoorHash,DOORSTATE_UNLOCKED,FALSE,TRUE)
//						DOOR_SYSTEM_SET_HOLD_OPEN(fakeExitData.garageDoor.iDoorHash,TRUE)
//						PRINTLN("MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: CUTSCENE Setting door (STATE NOT UNLOCKED) # to be open")
//					ENDIF
//				ELSE
//					IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(fakeExitData.garageDoor.iDoorHash) != DOORSTATE_UNLOCKED
//						DOOR_SYSTEM_SET_DOOR_STATE(fakeExitData.garageDoor.iDoorHash,DOORSTATE_UNLOCKED,FALSE,TRUE)
//						DOOR_SYSTEM_SET_HOLD_OPEN(fakeExitData.garageDoor.iDoorHash,TRUE)
//						PRINTLN("MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: CUTSCENE Setting door (STATE NOT PENDING UNLOCKED) # to be open")
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	#ENDIF
//	#ENDIF

	IF IS_PED_FOR_CUTSCENE_OK(  garagePedLeaveCutStage,iCurrentProperty, fakeExitData  )
		DO_GARAGE_PED_LEAVE_WARP(iCurrentProperty,fakeExitData)
		SWITCH garagePedLeaveCutStage
			CASE GARAGE_PED_LEAVE_CUTSCENE_INIT
				IF GET_PROPERTY_SIZE_TYPE(iCurrentProperty) = PROP_SIZE_TYPE_LARGE_APT //GET_PROPERTY_BUILDING(iCurrentProperty) = MP_PROPERTY_BUILDING_1
					SET_BIT(fakeExitData.iBS,FAKE_PED_EXIT_GARAGE_WARP_POSSIBLE)
					IF IS_BIT_SET(fakeExitData.iBS,FAKE_PED_EXIT_GARAGE_WARP_DONE)
						garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_COMPLETE
						RETURN TRUE
					ELSE
						PRINTLN("=== CUTSCENE === garagePedLeaveCutStage waiting for warp to custom spawn")
					ENDIF
//					IF NOT IS_PLAYER_IN_CUTSCENE(PLAYER_ID())
//						SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE,TRUE)
//						START_MP_CUTSCENE(TRUE)
//					ENDIF
//					IF IS_PED_MALE(PLAYER_PED_ID()) 
//						fakeExitData.fakeExitPed = CREATE_PED(PEDTYPE_CIVMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
//					ELSE
//						fakeExitData.fakeExitPed = CREATE_PED(PEDTYPE_CIVFEMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
//					ENDIF
//					CLONE_PED_TO_TARGET(PLAYER_PED_ID(), fakeExitData.fakeExitPed)
//
//					iGarageCutTimer = GET_GAME_TIMER()
//					garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_WAIT_FOR_LOAD
//
//					NEW_LOAD_SCENE_START_SPHERE(scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos,30)
//					//SET_FOCUS_POS_AND_VEL(mpProperties[iCurrentProperty].house.exits[0].vOutPlayerLoc,<<0,0,0>>)
//					SET_FOCUS_POS_AND_VEL(scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos,<<0,0,0>>)
				ELSE
				garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_TRIGGER_CUT
				ENDIF
			BREAK
		
			CASE GARAGE_PED_LEAVE_CUTSCENE_WAIT_FOR_LOAD
				IF (NOT IS_NEW_LOAD_SCENE_ACTIVE() OR IS_NEW_LOAD_SCENE_LOADED())
				OR GET_GAME_TIMER() - iGarageCutTimer > 20000
					iGarageCutTimer = GET_GAME_TIMER()
					garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_TRIGGER_CUT
				ELSE
					PRINTLN("GARAGE_PED_LEAVE- waiting for load scene")
				ENDIF
			BREAK
		
			CASE GARAGE_PED_LEAVE_CUTSCENE_TRIGGER_CUT
				
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF GET_PROPERTY_SIZE_TYPE(iCurrentProperty) = PROP_SIZE_TYPE_LARGE_APT //GET_PROPERTY_BUILDING(iCurrentProperty) = MP_PROPERTY_BUILDING_1
						//  #1638192 //  //  //  //  //  //
												//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos
						//NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
						iGarageCutsceneBitset = 0
					
						iGarageCutTimer = GET_GAME_TIMER()
					
						SET_ENTITY_COORDS(fakeExitData.fakeExitPed, scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos)
						SET_ENTITY_HEADING(fakeExitData.fakeExitPed, scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].fRot)
						
						
//						SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos)
//						SET_ENTITY_HEADING(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].fRot)
//						NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(),TRUE)

						SceneTool_ExecuteCut(scene.mCuts[NET_REALTY_5_SCENE_CUT_shot], camGarageCutscene)
						
						IF NOT IS_SCREEN_FADED_IN()
						AND NOT IS_SCREEN_FADING_IN()
							DO_SCREEN_FADE_IN(1000)
						ENDIF
						SET_BIT(mpPropIntRequestExtDoor.iBS,MP_PROP_INT_REQUEST_EXT_DOOR_OPEN)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_WAIT_FOR_OPEN_DOOR")
						#ENDIF
						CLEAR_SPECIFIC_SPAWN_LOCATION()
						garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_WAIT_FOR_OPEN_DOOR
					ELSE
					//  #1638192 //  //  //  //  //  //
					FLOAT fRadius
					fRadius = VDIST(scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos, scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos)
					fRadius += 2.0
					IF IS_POSITION_OCCUPIED(
							scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos,
							fRadius,
							FALSE, TRUE, FALSE, FALSE, FALSE)
					OR NET_REALTY_5_SCENE_IS_DOOR_BLOCKED(scene)
						//SCRIPT_ASSERT("area '5' occupied!")
						PRINTLN("area '5' occupied!")
						SET_PLAYER_IN_PROPERTY(FALSE,FALSE,FALSE)
						SETUP_SPECIFIC_SPAWN_LOCATION(mpProperties[iCurrentProperty].garage.vExitPlayerPos,mpProperties[iCurrentProperty].garage.fExitPlayerHeading, 10, FALSE)
						
						IF IS_NEW_LOAD_SCENE_ACTIVE()
							garagePedLeaveCutStage = GARAGE_PED_SET_IN_UNOCCUPIED_AREA
							
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garagePedLeaveCutStage = GARAGE_PED_SET_IN_UNOCCUPIED_AREA (fRadius: ", fRadius, ")")
							#ENDIF
						ELSE
							garagePedLeaveCutStage = GARAGE_PED_WARP_TO_UNOCCUPIED_AREA
							
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garagePedLeaveCutStage = GARAGE_PED_WARP_TO_UNOCCUPIED_AREA (fRadius: ", fRadius, ")")
							#ENDIF
						ENDIF
						RETURN FALSE
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garagePedLeaveCutStage = unoccupied (fRadius: ", fRadius, ")")
					ENDIF
					//  //  //  //  //  //  //  //  // 
					
											//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
					iGarageCutsceneBitset = 0
				
					iGarageCutTimer = GET_GAME_TIMER()
				
					SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].fRot)
					
//					camGarageCutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//					SET_CAM_ACTIVE(camGarageCutscene, TRUE)
//					SET_CAM_PARAMS(	camGarageCutscene, 
//									<<-796.1364, 322.6179, 87.7481>>, <<-1.1931, -0.0000, -179.8559>>, 
//									50.0000)
//					SHAKE_CAM(camGarageCutscene, "HAND_SHAKE", 0.25)
//					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					SceneTool_ExecuteCut(scene.mCuts[NET_REALTY_5_SCENE_CUT_shot], camGarageCutscene)
					
					IF NOT IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(1000)
					ENDIF
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_WAIT_FOR_OPEN_DOOR")
					#ENDIF
					CLEAR_SPECIFIC_SPAWN_LOCATION()
					garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_WAIT_FOR_OPEN_DOOR
					ENDIF
				ELSE
					bReset_garage_ped_leave_cutscene = TRUE
				ENDIF
				
			BREAK
			
			CASE GARAGE_PED_LEAVE_CUTSCENE_WAIT_FOR_OPEN_DOOR
				IF GET_PROPERTY_SIZE_TYPE(iCurrentProperty) = PROP_SIZE_TYPE_LARGE_APT //GET_PROPERTY_BUILDING(iCurrentProperty) = MP_PROPERTY_BUILDING_1
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_WAIT_FOR_EXIT")
					#ENDIF
					TASK_GO_STRAIGHT_TO_COORD(fakeExitData.fakeExitPed, scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos, PEDMOVE_WALK)	
					iGarageCutTimer = GET_GAME_TIMER()
							
					garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_WAIT_FOR_EXIT
				ELSE
				IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_SHORT
					SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].fRot)
					garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_COMPLETE
				ENDIF
				
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					
//					REQUEST_GARAGE_DOOR_OPEN()
//					
//					IF IS_GARAGE_DOOR_UNLOCKED()
//						IF IS_GARAGE_DOOR_OPEN()
						
							SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, GARAGE_CUT_SIMULATE_PLAYER_WALK)
							
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH()
							RENDER_SCRIPT_CAMS(FALSE, TRUE, GARAGE_CUT_GAMECAM_INTERP)
							
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_WAIT_FOR_EXIT")
							#ENDIF
							
							iGarageCutTimer = GET_GAME_TIMER()
							
							garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_WAIT_FOR_EXIT
//						ENDIF
//					ENDIF
				ELSE
					bReset_garage_ped_leave_cutscene = TRUE
				ENDIF
				
				ENDIF
			BREAK
			
			CASE GARAGE_PED_LEAVE_CUTSCENE_WAIT_FOR_EXIT
				IF GET_PROPERTY_SIZE_TYPE(iCurrentProperty) = PROP_SIZE_TYPE_LARGE_APT //GET_PROPERTY_BUILDING(iCurrentProperty) = MP_PROPERTY_BUILDING_1
					SET_BIT(fakeExitData.iBS,FAKE_PED_EXIT_GARAGE_WARP_POSSIBLE)
					CONST_FLOAT fCONST_DIST_FROM_startCoords_1	8.0 //4.0
					IF VDIST2(GET_ENTITY_COORDS(fakeExitData.fakeExitPed), scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos) > (fCONST_DIST_FROM_startCoords_1*fCONST_DIST_FROM_startCoords_1)
					OR 	VDIST2(GET_ENTITY_COORDS(fakeExitData.fakeExitPed), scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos) < (3*3)
						IF NOT IS_BIT_SET(iGarageCutsceneBitset, GARAGE_CUT_BS_CONTROL_RETURNED)
							CLEANUP_MP_CUTSCENE(FALSE,FALSE)
													//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							IF bDisabledRadar
								DISPLAY_RADAR(TRUE)
								bDisabledRadar = FALSE
							ENDIF
							SET_BIT(iGarageCutsceneBitset, GARAGE_CUT_BS_CONTROL_RETURNED)
						ENDIF
						
						CLEAR_BIT(mpPropIntRequestExtDoor.iBS,MP_PROP_INT_REQUEST_EXT_DOOR_OPEN)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						
						IF DOES_CAM_EXIST(camGarageCutscene)
							DESTROY_CAM(camGarageCutscene)
						ENDIF
						DELETE_PED(fakeExitData.fakeExitPed)
//						REQUEST_GARAGE_DOOR_CLOSE()
//						
//						IF IS_GARAGE_DOOR_CLOSED()
							//IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === 123 garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_COMPLETE")
								#ENDIF
								
								garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_COMPLETE
							//ENDIF
//						ENDIF
					ENDIF
				ELSE
				IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_LONG
					SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].fRot)
					garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_COMPLETE
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === GARAGE_CUT_SKIP_TIME_LONG expired garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_COMPLETE")
					#ENDIF
				ENDIF
				
				//#1572659
				IF GET_GAME_TIMER() - iGarageCutTimer > (GARAGE_CUT_SKIP_TIME_LONG / 2.0)
					//
				ELSE
//					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_DOWN_ONLY)
//					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LEFT_ONLY)
//					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_RIGHT_ONLY)
				ENDIF
				
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
				
PRINTSTRING("VDIST: ")
PRINTFLOAT(VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos))
PRINTNL()
				
					CONST_FLOAT fCONST_DIST_FROM_startCoords_1	8.0 //4.0
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos) > (fCONST_DIST_FROM_startCoords_1*fCONST_DIST_FROM_startCoords_1)
					OR 	VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos) < (3*3)
						IF NOT IS_BIT_SET(iGarageCutsceneBitset, GARAGE_CUT_BS_CONTROL_RETURNED)
							
													//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							IF bDisabledRadar
								DISPLAY_RADAR(TRUE)
								bDisabledRadar = FALSE
							ENDIF
							SET_BIT(iGarageCutsceneBitset, GARAGE_CUT_BS_CONTROL_RETURNED)
						ENDIF
						
//						REQUEST_GARAGE_DOOR_CLOSE()
//						
//						IF IS_GARAGE_DOOR_CLOSED()
							//IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_COMPLETE")
								#ENDIF
								
								garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_COMPLETE
							//ENDIF
//						ENDIF
					ENDIF
				ELSE
					bReset_garage_ped_leave_cutscene = TRUE
				ENDIF
				ENDIF
			BREAK
			
			CASE GARAGE_PED_WARP_TO_UNOCCUPIED_AREA
			CASE GARAGE_PED_SET_IN_UNOCCUPIED_AREA
	//			IF IS_SCREEN_FADED_OUT()
					BOOL bWarpWhenBlocked
					bWarpWhenBlocked = FALSE
					
					IF garagePedLeaveCutStage = GARAGE_PED_SET_IN_UNOCCUPIED_AREA
						PRINTLN("=== CUTSCENE === garagePedLeaveCutStage - IN HERE-")
						bWarpWhenBlocked = TRUE
						IF (NOT IS_NEW_LOAD_SCENE_ACTIVE() OR IS_NEW_LOAD_SCENE_LOADED())
							PRINTLN("=== CUTSCENE === garagePedLeaveCutStage - IF (NOT IS_NEW_LOAD_SCENE_ACTIVE() OR NOT IS_NEW_LOAD_SCENE_LOADED()) -")
						ELSE
							PRINTLN("=== CUTSCENE === garagePedLeaveCutStage - waiting for new load scene to be finished before setting player coords -")
							RETURN FALSE
						ENDIF
					ENDIF
					IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_AT_SPECIFIC_COORDS_IF_POSSIBLE, FALSE, FALSE, FALSE,bWarpWhenBlocked)
					
						IF NOT IS_SCREEN_FADED_IN()
						AND NOT IS_SCREEN_FADING_IN()
							DO_SCREEN_FADE_IN(500)
						ENDIF
						
						
						IF NOT IS_BIT_SET(iGarageCutsceneBitset, GARAGE_CUT_BS_CONTROL_RETURNED)
							
													//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							IF bDisabledRadar
								DISPLAY_RADAR(TRUE)
								bDisabledRadar = FALSE
							ENDIF
							SET_BIT(iGarageCutsceneBitset, GARAGE_CUT_BS_CONTROL_RETURNED)
						ENDIF

						
						garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_COMPLETE
						PRINTLN("=== CUTSCENE === garagePedLeaveCutStage - garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_COMPLETE - C")
					ELSE
						PRINTLN("=== CUTSCENE === garagePedLeaveCutStage - Trying to warp to spawn location-")
					ENDIF
	//			ENDIF
			BREAK
				
			CASE GARAGE_PED_LEAVE_CUTSCENE_COMPLETE
				IF GET_PROPERTY_SIZE_TYPE(iCurrentProperty) = PROP_SIZE_TYPE_LARGE_APT //GET_PROPERTY_BUILDING(iCurrentProperty) = MP_PROPERTY_BUILDING_1
					IF IS_BIT_SET(fakeExitData.iBS,FAKE_PED_EXIT_GARAGE_WARP_DONE)
						RETURN TRUE
					ELSE
						PRINTLN("=== CUTSCENE === garagePedLeaveCutStage waiting for warp to custom spawn")
					ENDIF
				ELSE
				
				RETURN TRUE
				ENDIF
			BREAK
		
		ENDSWITCH
	ENDIF
	
	IF bReset_garage_ped_leave_cutscene
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === RESET_GARAGE_PED_LEAVE_CUTSCENE()")
		#ENDIF

		//BOOL bClearTasks = TRUE
		BOOL bTurnOffRenderScriptCams = TRUE
		
		IF bTurnOffRenderScriptCams
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		ENDIf
		
		IF DOES_CAM_EXIST(camGarageCutscene)
			DESTROY_CAM(camGarageCutscene)
		ENDIF
			IF NOT IS_PED_INJURED(fakeExitData.fakeExitPed)
				DELETE_PED(fakeExitData.fakeExitPed)
			ENDIF
			CLEAR_BIT(mpPropIntRequestExtDoor.iBS,MP_PROP_INT_REQUEST_EXT_DOOR_OPEN)
		iGarageCutsceneBitset = 0
		
								//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos
		NET_SET_PLAYER_CONTROL(	PLAYER_ID(),TRUE )
		
		
	//	#IF IS_DEBUG_BUILD
	//		bDebugLaunchGaragePedLeaveCutscene = FALSE
	//		bDebugSeenGaragePedLeaveCutscene = FALSE
	//	#ENDIF
		
		garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_INIT
	ENDIF

	
	RETURN FALSE
ENDFUNC

#IF ACTIVATE_PROPERTY_CS_DEBUG
USING "NET_REALTY_5_SCENE_debug.sch"
#ENDIF
