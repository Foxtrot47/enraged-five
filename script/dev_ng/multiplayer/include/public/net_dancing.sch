//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Author: 	Orlando																							//
// Team:	Online Tech																						//
// Info:	Stuff to help with dancing implementations that can't live in the interior .sc 					//
//    		because they're required in other places can be put here.										//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "stats_helper_packed.sch"
USING "net_interactions_public.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Nightclub    																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

ENUM DANCE_CONTROL_SCHEME
	DANCE_CONTROL_SCHEME_STANDARD,
	DANCE_CONTROL_SCHEME_ALTERNATE_1,
	DANCE_CONTROL_SCHEME_ALTERNATE_2,
	DANCE_CONTROL_SCHEME_ALTERNATE_3,
	DANCE_CONTROL_SCHEME_ALTERNATE_4,
	DANCE_CONTROL_SCHEME_ALTERNATE_5
ENDENUM

/// PURPOSE: 
///    Available dance styles, DO NOT CHANGE ORDER! these are saved to stats in the pi menu settings
///    values have been explicitly assigned as a deterrent
ENUM DANCE_STYLE
	DANCE_STYLE_INVALID = -1
	,DANCE_STYLE_FEMALE_A = 0
	,DANCE_STYLE_FEMALE_B = 1
	,DANCE_STYLE_MALE_B = 2
	,DANCE_STYLE_MALE_A = 3

	#IF FEATURE_HEIST_ISLAND_DANCES
	,DANCE_STYLE_JUMPER = 4
	,DANCE_STYLE_TECHNO_MONKEY = 5
	,DANCE_STYLE_SHUFFLE = 6
	,DANCE_STYLE_TECHNO_KARATE = 7
	,DANCE_STYLE_BEACH_BOXING = 8
	,DANCE_STYLE_SAND_TRIP = 9
	#ENDIF
ENDENUM

ENUM DUAL_DANCE_CLIP
	DDC_INVALID = -1
	,DDC_DUAL_DANCE_INTRO
	,DDC_DUAL_DANCE_STANDING_INTRO
	,DDC_DUAL_DANCE_IDLE
	,DDC_DUAL_DANCE_EXIT
	,DDC_DUAL_DANCE_STANDING_EXIT
ENDENUM

STRUCT DANCE_CLIPSET
	STRING sDisplayNameLabel
	STRING sClipset
	STRING sDictionary
ENDSTRUCT

STRUCT DUAL_DANCE_CLIP_DATA
	STRING strClip
	FLOAT fBlendIn = WALK_BLEND_IN
	FLOAT fBlendOut = WALK_BLEND_OUT
ENDSTRUCT

STRUCT DUAL_DANCE_ANIM
	TEXT_LABEL_63 tlDisplayName
	STRING strDict
	DUAL_DANCE_CLIP_DATA sPedAClips[COUNT_OF(DUAL_DANCE_CLIP)]
	DUAL_DANCE_CLIP_DATA sPedBClips[COUNT_OF(DUAL_DANCE_CLIP)]
ENDSTRUCT

/// PURPOSE:
///    To be called in freemode startup.
///    Set default dance style for nightclub dancing.
PROC DANCE_SET_DEFAULT_STYLE_ON_FIRST_BOOT()
	// Assign a default menu option for dance style if this is our first time
	IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_SET_DEFAULT_DANCE)
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_SET_DEFAULT_DANCE, TRUE)
		INT iDanceStyle
		IF IS_PED_FEMALE(PLAYER_PED_ID())
			iDanceStyle = ENUM_TO_INT(DANCE_STYLE_FEMALE_A)
		ELSE
			iDanceStyle = ENUM_TO_INT(DANCE_STYLE_MALE_B)
		ENDIF
		SET_MP_INT_CHARACTER_STAT(MP_STAT_NIGHTCLUBDANCSSETTING, iDanceStyle)
		CDEBUG1LN(DEBUG_NET_DANCING, "[PLAYER_DANCE] Assigning default dance style... ", GET_MP_INT_CHARACTER_STAT(MP_STAT_NIGHTCLUBDANCSSETTING))
	ENDIF
ENDPROC

FUNC BOOL DANCING_GET_PLAYER_DANCE_ANIM_DATA(INT index, DANCE_CLIPSET& ref_data)
	SWITCH INT_TO_ENUM(DANCE_STYLE, index)
		CASE DANCE_STYLE_FEMALE_A 	/*ciDANCE_STYLE_F_A = DD_MOVE_DANCE_F_VAR_A*/
			ref_data.sDisplayNameLabel 	= "PIMNCLDANSTY0"
			ref_data.sClipset 			= "NIGHTCLUB@DANCE_MINIGAME@DANCE_SOLO@FEMALE@VAR_A"
			ref_data.sDictionary 		= "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_SOLO@FEMALE@VAR_A@"
			BREAK
		CASE DANCE_STYLE_FEMALE_B 	/*ciDANCE_STYLE_F_B = DD_MOVE_DANCE_F_VAR_B*/
			ref_data.sDisplayNameLabel 	= "PIMNCLDANSTY1"
			ref_data.sClipset 			= "NIGHTCLUB@DANCE_MINIGAME@DANCE_SOLO@FEMALE@VAR_B"
			ref_data.sDictionary 		= "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_SOLO@FEMALE@VAR_B@"
			BREAK
		CASE DANCE_STYLE_MALE_B 	/*ciDANCE_STYLE_M_A = DD_MOVE_DANCE_M_VAR_B | A and B swapped! B* 4987005 */
			ref_data.sDisplayNameLabel 	= "PIMNCLDANSTY2"
			ref_data.sClipset 			= "NIGHTCLUB@DANCE_MINIGAME@DANCE_SOLO@MALE@VAR_B"
			ref_data.sDictionary 		= "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_SOLO@MALE@VAR_B@"
			BREAK
		CASE DANCE_STYLE_MALE_A 	/*ciDANCE_STYLE_M_B = DD_MOVE_DANCE_M_VAR_A | A and B swapped! B* 4987005 */
			ref_data.sDisplayNameLabel 	= "PIMNCLDANSTY3"
			ref_data.sClipset 			= "NIGHTCLUB@DANCE_MINIGAME@DANCE_SOLO@MALE@VAR_A"
			ref_data.sDictionary 		= "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_SOLO@MALE@VAR_A@"
			BREAK
		CASE DANCE_STYLE_JUMPER
			ref_data.sDisplayNameLabel 	= "PIMNCLDANSTY4"
			ref_data.sClipset 			= "NIGHTCLUB@DANCE_MINIGAME@DANCE_SOLO@JUMPER"
			ref_data.sDictionary 		= "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_SOLO@JUMPER@"		
			BREAK
		CASE DANCE_STYLE_TECHNO_MONKEY
			ref_data.sDisplayNameLabel 	= "PIMNCLDANSTY5"
			ref_data.sClipset 			= "NIGHTCLUB@DANCE_MINIGAME@DANCE_SOLO@TECHNO_MONKEY"
			ref_data.sDictionary 		= "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_SOLO@TECHNO_MONKEY@"	
			BREAK
		CASE DANCE_STYLE_SHUFFLE
			ref_data.sDisplayNameLabel 	= "PIMNCLDANSTY6"
			ref_data.sClipset 			= "NIGHTCLUB@DANCE_MINIGAME@DANCE_SOLO@SHUFFLE"
			ref_data.sDictionary 		= "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_SOLO@SHUFFLE@"	
			BREAK
		CASE DANCE_STYLE_TECHNO_KARATE
			ref_data.sDisplayNameLabel 	= "PIMNCLDANSTY7"
			ref_data.sClipset 			= "NIGHTCLUB@DANCE_MINIGAME@DANCE_SOLO@TECHNO_KARATE"
			ref_data.sDictionary 		= "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_SOLO@TECHNO_KARATE@"	
			BREAK
		CASE DANCE_STYLE_BEACH_BOXING
			ref_data.sDisplayNameLabel 	= "PIMNCLDANSTY8"
			ref_data.sClipset 			= "NIGHTCLUB@DANCE_MINIGAME@DANCE_SOLO@BEACH_BOXING"
			ref_data.sDictionary 		= "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_SOLO@BEACH_BOXING@"	
			BREAK
		CASE DANCE_STYLE_SAND_TRIP
			ref_data.sDisplayNameLabel 	= "PIMNCLDANSTY9"
			ref_data.sClipset 			= "NIGHTCLUB@DANCE_MINIGAME@DANCE_SOLO@SAND_TRIP"
			ref_data.sDictionary 		= "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_SOLO@SAND_TRIP@"	
			BREAK
			
		DEFAULT RETURN FALSE
	ENDSWITCH	
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DANCING_GET_PLAYER_DUAL_DANCE_ANIM_DATA(INT iIndex, DUAL_DANCE_ANIM &sAnimOut)
	// For now all use the same
	sAnimOut.sPedAClips[DDC_DUAL_DANCE_INTRO].strClip = "PED_A_DANCE_INTRO"
	sAnimOut.sPedAClips[DDC_DUAL_DANCE_INTRO].fBlendIn = WALK_BLEND_IN
	sAnimOut.sPedAClips[DDC_DUAL_DANCE_INTRO].fBlendOut = INSTANT_BLEND_OUT
	
	sAnimOut.sPedAClips[DDC_DUAL_DANCE_STANDING_INTRO].strClip = "PED_A_STANDING_INTRO"
	sAnimOut.sPedAClips[DDC_DUAL_DANCE_STANDING_INTRO].fBlendIn = WALK_BLEND_IN
	sAnimOut.sPedAClips[DDC_DUAL_DANCE_STANDING_INTRO].fBlendOut = INSTANT_BLEND_OUT
	
	sAnimOut.sPedAClips[DDC_DUAL_DANCE_IDLE].strClip = "PED_A_DANCE_IDLE"
	sAnimOut.sPedAClips[DDC_DUAL_DANCE_IDLE].fBlendIn = INSTANT_BLEND_IN
	sAnimOut.sPedAClips[DDC_DUAL_DANCE_IDLE].fBlendOut = SLOW_BLEND_OUT
	
	sAnimOut.sPedAClips[DDC_DUAL_DANCE_EXIT].strClip = "PED_A_DANCE_EXIT"
	sAnimOut.sPedAClips[DDC_DUAL_DANCE_IDLE].fBlendIn = SLOW_BLEND_IN
	sAnimOut.sPedAClips[DDC_DUAL_DANCE_IDLE].fBlendOut = WALK_BLEND_OUT
	
	sAnimOut.sPedAClips[DDC_DUAL_DANCE_STANDING_EXIT].strClip = "PED_A_STANDING_EXIT"
	sAnimOut.sPedAClips[DDC_DUAL_DANCE_STANDING_EXIT].fBlendIn = SLOW_BLEND_IN
	sAnimOut.sPedAClips[DDC_DUAL_DANCE_STANDING_EXIT].fBlendOut = WALK_BLEND_OUT
	
	// Ped B
	sAnimOut.sPedBClips[DDC_DUAL_DANCE_INTRO].strClip = "PED_B_DANCE_INTRO"
	sAnimOut.sPedBClips[DDC_DUAL_DANCE_INTRO].fBlendIn = WALK_BLEND_IN
	sAnimOut.sPedBClips[DDC_DUAL_DANCE_INTRO].fBlendOut = INSTANT_BLEND_OUT
	
	sAnimOut.sPedBClips[DDC_DUAL_DANCE_STANDING_INTRO].strClip = "PED_B_STANDING_INTRO"
	sAnimOut.sPedBClips[DDC_DUAL_DANCE_STANDING_INTRO].fBlendIn = WALK_BLEND_IN
	sAnimOut.sPedBClips[DDC_DUAL_DANCE_STANDING_INTRO].fBlendOut = INSTANT_BLEND_OUT
	
	sAnimOut.sPedBClips[DDC_DUAL_DANCE_IDLE].strClip = "PED_B_DANCE_IDLE"
	sAnimOut.sPedBClips[DDC_DUAL_DANCE_IDLE].fBlendIn = INSTANT_BLEND_IN
	sAnimOut.sPedBClips[DDC_DUAL_DANCE_IDLE].fBlendOut = SLOW_BLEND_OUT
	
	sAnimOut.sPedBClips[DDC_DUAL_DANCE_EXIT].strClip = "PED_B_DANCE_EXIT"
	sAnimOut.sPedBClips[DDC_DUAL_DANCE_IDLE].fBlendIn = SLOW_BLEND_IN
	sAnimOut.sPedBClips[DDC_DUAL_DANCE_IDLE].fBlendOut = WALK_BLEND_OUT
	
	sAnimOut.sPedBClips[DDC_DUAL_DANCE_STANDING_EXIT].strClip = "PED_B_STANDING_EXIT"
	sAnimOut.sPedBClips[DDC_DUAL_DANCE_STANDING_EXIT].fBlendIn = SLOW_BLEND_IN
	sAnimOut.sPedBClips[DDC_DUAL_DANCE_STANDING_EXIT].fBlendOut = WALK_BLEND_OUT	
	
	SWITCH iIndex
		CASE 0 
			sAnimOut.strDict = "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_PAIRED@DANCE_A@"
			RETURN TRUE
		CASE 1 
			sAnimOut.strDict = "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_PAIRED@DANCE_B@"
			RETURN TRUE
		CASE 2 	
			sAnimOut.strDict = "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_PAIRED@DANCE_D@"
			RETURN TRUE
		CASE 3
			sAnimOut.strDict = "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_PAIRED@DANCE_E@"
			RETURN TRUE
		CASE 4
			sAnimOut.strDict = "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_PAIRED@DANCE_F@"
			RETURN TRUE
		CASE 5
			sAnimOut.strDict = "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_PAIRED@DANCE_H@"
			RETURN TRUE
		CASE 6
			sAnimOut.strDict = "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_PAIRED@DANCE_J@"
			RETURN TRUE
		CASE 7
			sAnimOut.strDict = "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_PAIRED@DANCE_K@"
			RETURN TRUE
		CASE 8
			sAnimOut.strDict = "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_PAIRED@DANCE_L@"
			RETURN TRUE
		CASE 9
			sAnimOut.strDict = "ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_PAIRED@DANCE_M@"
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

