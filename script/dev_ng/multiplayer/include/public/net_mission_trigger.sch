USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_network.sch"

USING "net_comms_public.sch"
USING "net_mission_details_overlay.sch"
USING "net_mission_trigger_overview.sch"
USING "net_mission_control_broadcasts.sch"
USING "net_cloud_mission_loader_public.sch"
USING "net_missions_shared_cloud_data_public.sch"

USING "net_mission.sch"				// For checking if player is on any mission
USING "shop_public.sch"				// For checking if the player is in a shop
USING "Cheat_Handler.sch"			//For telling the cheat tracker a mission has started
//USING "screen_gang_on_mission.sch"		// for resetting mission inventory

// KEITH TEMP 24/8/11: CnC Specific, so should be checked outwith the common functionality within this generic header

// For end of mission cleanup
USING "net_hud_displays.sch"
USING "screen_gang_on_mission.sch"

USING "net_spawn.sch"
USING "achievement_public.sch"

#IF IS_DEBUG_BUILD
	USING "net_mission_trigger_debug.sch"
	USING "net_debug_log.sch"
#ENDIF
// For rich presence Xbox
STRUCT RP_MP_MISSION
	INT index
ENDSTRUCT


// *****************************************************************************************************************************************************
// *****************************************************************************************************************************************************
// *****************************************************************************************************************************************************
//
//      MISSION NAME    :   net_mission_trigger.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all generic mission triggering functionality.
//
// *****************************************************************************************************************************************************
// *****************************************************************************************************************************************************
// *****************************************************************************************************************************************************




// ===========================================================================================================
//      Mission Triggering Global Control Variable Check, Set, and Clear Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Mission Triggering State Access Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets all MT bitflags to FALSE
PROC Clear_All_MP_Mission_Triggering_Bitflags()
	g_sTriggerMP.mtBitflags = CLEAR_ALL_MP_MP_TRIGGER_BITFLAGS
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets the bitflag that will force a player onto a mission without YES/NO choice
PROC Set_MT_Bitflag_As_Force_Onto_Mission()

	SET_BIT(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_FORCE_ONTO_MISSION)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Set Mission Trigger Bitflag: Force Onto Mission (don't offer YES/NO choice)") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player is being forced onto a mission without YES/NO choice
//
// RETURN VALUE:		BOOL			TRUE if the player is being forced onto a mission, otherwise FALSE
FUNC BOOL Is_Player_Being_Forced_Onto_Mission()
	RETURN (IS_BIT_SET(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_FORCE_ONTO_MISSION))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets the bitflag that indicates Mission Control has classed the mission as Finished
PROC Set_MT_Bitflag_As_Mission_Finished()

	SET_BIT(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_MISSION_FINISHED)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Set Mission Trigger Bitflag: Mission Control has broadcast Mission Finished") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if Mission Control has marked the mission as Finished
//
// RETURN VALUE:		BOOL			TRUE if Mission Control has broadcast that the mission is Finished, otherwise FALSE
FUNC BOOL Has_Mission_Finished_Event_Been_Received()
	RETURN (IS_BIT_SET(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_MISSION_FINISHED))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets the bitflag that indicates Mission Control has classed the mission as Failed To Launch
PROC Set_MT_Bitflag_As_Mission_Failed()

	SET_BIT(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_MISSION_FAILED)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Set Mission Trigger Bitflag: Mission Control has broadcast Mission Failed") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if Mission Control has marked the mission as Failed
//
// RETURN VALUE:		BOOL			TRUE if Mission Control has broadcast that the mission is Failed, otherwise FALSE
FUNC BOOL Has_Mission_Failed_Event_Been_Received()
	RETURN (IS_BIT_SET(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_MISSION_FAILED))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets the bitflag that indicates Mission Control has classed the mission as Full for this Player's Team
PROC Set_MT_Bitflag_As_Mission_Full()

	SET_BIT(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_MISSION_FULL)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Set Mission Trigger Bitflag: Mission Control has broadcast Mission Full") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if Mission Control has marked the mission as Full
//
// RETURN VALUE:		BOOL			TRUE if Mission Control has broadcast that the mission is Full, otherwise FALSE
FUNC BOOL Has_Mission_Full_Event_Been_Received()
	RETURN (IS_BIT_SET(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_MISSION_FULL))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets the bitflag that indicates Mission Control has broadcast that it is still ok for this player to join the mission
PROC Set_MT_Bitflag_As_Confirmed_Ok_To_Join()

	SET_BIT(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_RECEIVED_OK_TO_JOIN)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Set Mission Trigger Bitflag: Mission Control has broadcast Confirmation Mission Ok To Join") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if Mission Control has Confirmed that the mission is still ok to join
//
// RETURN VALUE:		BOOL			TRUE if Mission Control has broadcast that the mission is Still Ok To Join, otherwise FALSE
FUNC BOOL Has_Mission_Been_Confirmed_As_Ok_To_Join()
	RETURN (IS_BIT_SET(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_RECEIVED_OK_TO_JOIN))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets the bitflag that requests the mission to force cleanup pre-mission
PROC Set_MT_Bitflag_As_Force_Quit_PreMission()

	SET_BIT(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_FORCE_QUIT_PREMISSION)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Set Mission Trigger Bitflag: Force Quit_PreMission") NET_NL()
	#ENDIF

ENDPROC

FUNC INT GET_FREEMODE_MISSION_RICH_PRESENCE_INDEX(MP_MISSION mission_index)
	
	NET_PRINT("GET_FREEMODE_MISSION_RICH_PRESENCE_INDEX: ") NET_PRINT_INT(ENUM_TO_INT(mission_index))
	SWITCH (mission_index)
		// FM Activities (data not held on cloud) - Keith 12/11/12 (to get FM to use Mission Controller)
		CASE eFM_ARM_WRESTLING	RETURN 1
		CASE eFM_DARTS			RETURN 2
		CASE eFM_GOLF			RETURN 3	//100
		CASE eFM_SHOOTING_RANGE	RETURN 4
		
		
		// FM Activities (data held on cloud) - Keith 4/1/13 (to allow FM to use Mission Controller)
		CASE eFM_BASEJUMP_CLOUD		RETURN 5
		CASE eFM_DEATHMATCH_CLOUD	RETURN 6
		CASE eFM_GANG_ATTACK_CLOUD	RETURN 7
		CASE eFM_MISSION_CLOUD		RETURN 8
		CASE eFM_RACE_CLOUD			RETURN 9
		CASE eFM_SURVIVAL_CLOUD		RETURN 10 //108
		CASE eFM_TENNIS				RETURN 11
		CASE eFM_PILOT_SCHOOL		RETURN 12
	ENDSWITCH

	
	SCRIPT_ASSERT("GET_FREEMODE_MISSION_RICH_PRESENCE_INDEX - INVALID RICH PRESENCE. Tell Kevin to add matching missing rich presence number")
	RETURN 13
ENDFUNC


PROC SETTING_RICH_PRESENCES_VALUES_MISSION_CREATOR()
// Set console rich presence values as we go on mission.

	INT mission_index = 90
	

	IF IS_XBOX360_VERSION()
	OR IS_XBOX_PLATFORM()
	    RP_MP_MISSION mRPMission
	    mRPMission.index = ENUM_TO_INT(mission_index)

	    NETWORK_SET_RICH_PRESENCE(PRESENCE_PRES_7, mRPMission, size_of(mRPMission ), 1)
		CPRINTLN(DEBUG_FLOW, "Set XBOX rich presence to label for creator.") NET_PRINT_INT(mRPMission.index)
	ELIF IS_PS3_VERSION()
	OR IS_PLAYSTATION_PLATFORM()
	//	#IF IS_DEBUG_BUILD		//fixing assert. wait as we need GET_STRING_FROM_TEXT_FILE to work in release build
		    TEXT_LABEL_23 tRichPresence = "MPM_"
		    //tRichPresence += ENUM_TO_INT(mission_index)
			tRichPresence += mission_index
		    tRichPresence += "_STR"
		
			
			NETWORK_SET_RICH_PRESENCE_STRING(PRESENCE_PRES_7,tRichPresence) //GET_STRING_FROM_TEXT_FILE (tRichPresence))
			CPRINTLN(DEBUG_FLOW, "Set PS3 rich presence to label for creator [", tRichPresence, "].")
		
	//	#ENDIF
	
	ENDIF
		

ENDPROC

PROC SETTING_RICH_PRESENCES_VALUES(MP_MISSION mission_index, INT iMissionSubType = 0)
// Set console rich presence values as we go on mission.

	IF ENUM_TO_INT(mission_index) = 0
	
	ENDIF

	IF IS_XBOX360_VERSION()
	OR IS_XBOX_PLATFORM()
	    RP_MP_MISSION mRPMission
	    //mRPMission.index = ENUM_TO_INT(mission_index)
		mRPMission.index =GET_FREEMODE_MISSION_RICH_PRESENCE_INDEX(mission_index)
		
		
		// change to captre flag mission
		//g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION_CTF
			//FMMC_MISSION_TYPE_CTF 


		IF mRPMission.index =  12
			mRPMission.index = 92 // number for 'Pilot School'
		ENDIF

		IF mRPMission.index = 8 // if its an online mission
		 	IF iMissionSubType =  FMMC_MISSION_TYPE_CTF 
 				mRPMission.index = 91 // number for 'capture'
				
			ENDIF
			
			
			// uncomment this when miguel sets up heist labels
			IF iMissionSubType =  FMMC_MISSION_TYPE_PLANNING
				mRPMission.index = 94 // number for 'Heist planning'
			ENDIF
			
			IF iMissionSubType =  FMMC_MISSION_TYPE_HEIST
				mRPMission.index = 93 // number for 'Heist finale'
			ENDIF
		
		ENDIF
	
		
	    NETWORK_SET_RICH_PRESENCE(PRESENCE_PRES_7, mRPMission, size_of(mRPMission ), 1)
		CPRINTLN(DEBUG_FLOW, "Set XBOX rich presence to label.") NET_PRINT_INT(mRPMission.index)
	ELIF IS_PS3_VERSION()
	OR IS_PLAYSTATION_PLATFORM()
	//	#IF IS_DEBUG_BUILD		//fixing assert. wait as we need GET_STRING_FROM_TEXT_FILE to work in release build
		    
			
			INT Activity_number = GET_FREEMODE_MISSION_RICH_PRESENCE_INDEX(mission_index)
			
			IF Activity_number =  12
				Activity_number = 92 // number for 'Pilot School'
			ENDIF
			
			
			IF Activity_number = 8 // if its an online mission
			 	IF iMissionSubType =  FMMC_MISSION_TYPE_CTF 
	 				Activity_number = 91 // number for 'capture'
				ENDIF
				
				IF iMissionSubType =  FMMC_MISSION_TYPE_PLANNING
					Activity_number = 94 // number for 'Heist planning'
				ENDIF
				
				IF iMissionSubType =  FMMC_MISSION_TYPE_HEIST
					Activity_number = 93 // number for 'Heist finale'
				ENDIF
			
			ENDIF
	
			
			
			TEXT_LABEL_23 tRichPresence = "MPM_"
		    //tRichPresence += ENUM_TO_INT(mission_index)
			tRichPresence += Activity_number
		    tRichPresence += "_STR"
		
			
			NETWORK_SET_RICH_PRESENCE_STRING(PRESENCE_PRES_7,tRichPresence) //GET_STRING_FROM_TEXT_FILE (tRichPresence))
			CPRINTLN(DEBUG_FLOW, "Set PS3 rich presence to label [", tRichPresence, "].")
		
	//	#ENDIF
	
	ENDIF
		

ENDPROC

PROC RESETTING_RICH_PRESENCES_VALUES()
// Set console rich presence values as we go on mission.
	g_I_MAX_XP_CAP_DISPLAY = FALSE
	IF IS_XBOX360_VERSION()
	OR IS_XBOX_PLATFORM()
	    RP_MP_MISSION mRPMission
	    mRPMission.index = ENUM_TO_INT(12)
	    NETWORK_SET_RICH_PRESENCE(PRESENCE_PRES_7, mRPMission, SIZE_OF(mRPMission), 1)
		CPRINTLN(DEBUG_FLOW, "Resetting XBOX rich presence to label.") NET_PRINT_INT(mRPMission.index)
	ELIF IS_PS3_VERSION()
	OR IS_PLAYSTATION_PLATFORM()
	//	#IF IS_DEBUG_BUILD		//fixing assert. wait as we need GET_STRING_FROM_TEXT_FILE to work in release build
		    //TEXT_LABEL_23 tRichPresence = "HUD_TITLE1"
			
			TEXT_LABEL_23 tRichPresence = "MPM_12_STR"

			
			NETWORK_SET_RICH_PRESENCE_STRING(PRESENCE_PRES_7,tRichPresence) //GET_STRING_FROM_TEXT_FILE (tRichPresence))
			CPRINTLN(DEBUG_FLOW, "Resetting PS3 rich presence to label [", tRichPresence, "].")
		
	//	#ENDIF
	
	ENDIF
	

ENDPROC




// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a force cleanup during pre-0mission phase requested
//
// RETURN VALUE:		BOOL			TRUE if the mission should cleanup pre-mission, otherwise FALSE
FUNC BOOL Has_Force_Quit_PreMission_Been_Received()
	RETURN (IS_BIT_SET(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_FORCE_QUIT_PREMISSION))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets the bitflag that will allow the mission details box to be displayed or not
PROC Set_MT_Bitflag_Display_Mission_Details_Box()

	SET_BIT(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_DISPLAY_MISSION_DETAILS)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Set Mission Trigger Bitflag: Display Mission Details Box") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Mission details Box should be displayed
//
// RETURN VALUE:		BOOL			TRUE if the Mission details box should be displayed, otherwise FALSE
FUNC BOOL Is_Mission_Details_Box_Required()
	RETURN (IS_BIT_SET(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_DISPLAY_MISSION_DETAILS))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets the bitflag that will allow the mission to show a pre-mission cutscene
PROC Set_MT_Bitflag_Show_PreMission_Cutscene()

	SET_BIT(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_PREMISSION_CUTSCENE)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Set Mission Trigger Bitflag: Show PreMission Cutscene") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a pre-mission cutscene should be displayed
//
// RETURN VALUE:		BOOL			TRUE if a pre-mission cutscene should be displayed, otherwise FALSE
FUNC BOOL Is_PreMission_Cutscene_Required()
	RETURN (IS_BIT_SET(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_PREMISSION_CUTSCENE))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a Mission Triggering request is already in progress
//
// RETURN VALUE:		BOOL			TRUE if a request is in progress, otherwise FALSE
FUNC BOOL Is_Mission_Triggering_Request_In_Progress()

	IF (g_sTriggerMP.mtState = NO_CURRENT_MP_TRIGGER_REQUEST)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets the bitflag that will use an intro Txtmsg instead of a phonecall or radio message
PROC Set_MT_Bitflag_As_Use_Txtmsg()

	SET_BIT(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFIELD_USE_TXTMSG)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Set Mission Trigger Bitflag: Use Intro Text Message") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if an intro Txtmsg is being used instead of a phonecall or radio message
//
// RETURN VALUE:		BOOL			TRUE if using an intro txtmsg, otherwise FALSE
FUNC BOOL Is_Using_Intro_Txtmsg()
	RETURN (IS_BIT_SET(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFIELD_USE_TXTMSG))
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Triggering Mission Over State Access Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clears the Mission Over State to the default MOS_OVER
// NOTES:	It will get set directly by an external function
PROC Clear_Mission_Triggering_Mission_Over_State()
	g_sTriggerMP.mtMissionOverState = MOS_OVER
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Get the Mission Over state
FUNC MISSION_OVER_STATE Get_Mission_Triggering_Mission_Over_State()
	RETURN (g_sTriggerMP.mtMissionOverState)
ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Mission Trigger Offer Timeout Access Functions
//		(on timeout the 'offer' phonecall will terminate)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Clears the Mission Triggering Offfer Timeout to 0
PROC Clear_MP_Mission_Triggering_Offer_Timeout()
	g_sTriggerMP.mtOfferTimeoutInitialised = FALSE
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the MP Comms timeout timer to a standard value
PROC Set_MP_Mission_Triggering_Comms_Timeout()

	g_sTriggerMP.mtOfferTimeout	= GET_TIME_OFFSET(g_iMPTimer , MISSION_OFFER_TIMEOUT_msec)
	g_sTriggerMP.mtOfferTimeoutInitialised = TRUE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Setting Mission Triggering Offer Timeout (") NET_PRINT_INT(MISSION_OFFER_TIMEOUT_msec) NET_PRINT(" msec)") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the MP Comms timeout timer has expired
//
// RETURN VALUE:		BOOL			TRUE if the timer has expired
FUNC BOOL Check_For_MP_Mission_Triggering_Comms_Timeout()
	RETURN (IS_TIME_MORE_THAN(g_iMPTimer , g_sTriggerMP.mtOfferTimeout) OR (g_sTriggerMP.mtOfferTimeoutInitialised = FALSE))
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Triggering State access functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set current STATE as 'No Triggering Request'
PROC Clear_Mission_Triggering_State_To_No_Request()

	g_sTriggerMP.mtState = NO_CURRENT_MP_TRIGGER_REQUEST
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: Clearing MP Mission Triggering State: NO_CURRENT_MP_TRIGGER_REQUEST") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the current STATE is 'No Request'
FUNC BOOL Is_Mission_Triggering_State_No_Request()
	RETURN (g_sTriggerMP.mtState = NO_CURRENT_MP_TRIGGER_REQUEST)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set current STATE as 'Start PreMission Comms'
PROC Set_Mission_Triggering_State_To_Start_PreMission_Comms()

	g_sTriggerMP.mtState = MP_TRIGGER_START_PREMISSION_COMMS
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Setting MP Mission Triggering State: MP_TRIGGER_START_PREMISSION_COMMS") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set current STATE as 'Waiting For PreMission Comms To End'
PROC Set_Mission_Triggering_State_To_Waiting_For_PreMission_Comms_To_End()

	g_sTriggerMP.mtState = MP_TRIGGER_WAITING_FOR_PREMISSION_COMMS_TO_END
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Setting MP Mission Triggering State: MP_TRIGGER_WAITING_FOR_PREMISSION_COMMS_TO_END") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the current STATE is 'Waiting for PreMission Comms to End'
FUNC BOOL Is_Mission_Triggering_State_Waiting_For_PreMission_Comms_To_End()
	RETURN (g_sTriggerMP.mtState = MP_TRIGGER_WAITING_FOR_PREMISSION_COMMS_TO_END)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set current STATE as 'Launch_Cutscene'
PROC Set_Mission_Triggering_State_To_Launch_Cutscene()

	g_sTriggerMP.mtState = MP_TRIGGER_LAUNCH_CUTSCENE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Setting MP Mission Triggering State: MP_TRIGGER_LAUNCH_CUTSCENE") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set current STATE as 'Launching_Cutscene'
PROC Set_Mission_Triggering_State_To_Launching_Cutscene()

	g_sTriggerMP.mtState = MP_TRIGGER_LAUNCHING_CUTSCENE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Setting MP Mission Triggering State: MP_TRIGGER_LAUNCHING_CUTSCENE") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set current STATE as 'Cutscene Active'
PROC Set_Mission_Triggering_State_To_Cutscene_Active()

	g_sTriggerMP.mtState = MP_TRIGGER_CUTSCENE_ACTIVE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Setting MP Mission Triggering State: MP_TRIGGER_CUTSCENE_ACTIVE") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the current STATE is 'Cutscene Active'
FUNC BOOL Is_Mission_Triggering_State_Cutscene_Active()
	RETURN (g_sTriggerMP.mtState = MP_TRIGGER_CUTSCENE_ACTIVE)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set current STATE as 'Check Still Available'
PROC Set_Mission_Triggering_State_To_Check_Still_Available()

	g_sTriggerMP.mtState = MP_TRIGGER_CHECK_STILL_AVAILABLE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Setting MP Mission Triggering State: MP_TRIGGER_CHECK_STILL_AVAILABLE") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set current STATE as 'Awaiting Confirmation'
PROC Set_Mission_Triggering_State_To_Awaiting_Confirmation_From_Server()

	g_sTriggerMP.mtState = MP_TRIGGER_AWAITING_CONFIRMATION
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Setting MP Mission Triggering State: MP_TRIGGER_AWAITING_CONFIRMATION") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set current STATE as 'Launch_Mission'
PROC Set_Mission_Triggering_State_To_Launch_Mission()

	g_sTriggerMP.mtState = MP_TRIGGER_LAUNCH_MISSION
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Setting MP Mission Triggering State: MP_TRIGGER_LAUNCH_MISSION") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set current STATE as 'Launching_Mission'
PROC Set_Mission_Triggering_State_To_Launching_Mission()

	g_sTriggerMP.mtState = MP_TRIGGER_LAUNCHING_MISSION
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Setting MP Mission Triggering State: MP_TRIGGER_LAUNCHING_MISSION") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set current STATE as 'Active'
PROC Set_Mission_Triggering_State_To_Mission_Active()

	g_sTriggerMP.mtState = MP_TRIGGER_MISSION_ACTIVE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Setting MP Mission Triggering State: MP_TRIGGER_MISSION_ACTIVE") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the current STATE is 'Active'
FUNC BOOL Is_Mission_Triggering_State_Mission_Active()
	RETURN (g_sTriggerMP.mtState = MP_TRIGGER_MISSION_ACTIVE)
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Clear Mission Triggering Data
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear out all the current Mission Triggering Values values
//
// INPUT PARAMS:	paramBeingInitialised		TRUE if this is being called during initialisation [default = FALSE]
PROC Clear_MP_Mission_Triggering_Data(BOOL paramBeingInitialised = FALSE)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: Clear_MP_Mission_Triggering_Data() - Clearing All Details") NET_NL()
	#ENDIF

    // Unlock and Delete the txtmsg if appropriate
	IF NOT (paramBeingInitialised)
		IF (Is_Using_Intro_Txtmsg())
	        Kill_This_MP_Txtmsg_Comms(g_sTriggerMP.mtRoot)
		ENDIF
	ENDIF

	Clear_All_MP_Mission_Triggering_Bitflags()
	Clear_MP_Mission_Triggering_Offer_Timeout()
	Clear_Mission_Triggering_State_To_No_Request()
	// Conversation Variables
	g_sTriggerMP.mtCharacter		= ""
	g_sTriggerMP.mtSpeaker			= ""
	g_sTriggerMP.mtCharSheetAsInt	= ENUM_TO_INT(NO_CHARACTER)
	g_sTriggerMP.mtGroup			= ""
	g_sTriggerMP.mtRoot				= ""
	// Also clear out the individual variables that represent a copy of the MP_MISSION_DATA struct
	// NOTE: To clear it out: create an empty instance of the struct and copy it into the stored version
	MP_MISSION_DATA	emptyMissionDataStruct
	g_sTriggerMP.mtMissionData		= emptyMissionDataStruct
	
	// Don't update any widgets if being initialised - the widgets don't yet exist
	IF (paramBeingInitialised)
		EXIT
	ENDIF
	
	// This is where I'll clear out the mission triggering widgets if we introduce any

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      PlayStats Triggering Data Function
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Generate the PlayStats Variant value
//
// RETURN VALUE:			INT				The variant value
FUNC INT Get_MP_Triggering_Playstats_Variant()

	CONST_INT	DEFAULT_VARIANT_VALUE		0
	
	IF (g_sTriggerMP.mtMissionData.mdID.idVariation < 0)
		RETURN DEFAULT_VARIANT_VALUE
	ENDIF
	
	RETURN (g_sTriggerMP.mtMissionData.mdID.idVariation)

ENDFUNC

//Should we use g_FMMC_STRUCT.tl63MissionName
FUNC BOOL SHOULD_USE_MISSION_NAME_FROM_UGC(MP_MISSION mission)
	SWITCH mission
		//If it's a mission from the cloud
		CASE eFM_BASEJUMP_CLOUD			
		CASE eFM_DEATHMATCH_CLOUD		
		CASE eFM_GANG_ATTACK_CLOUD		
		CASE eFM_MISSION_CLOUD		
		CASE eFM_RACE_CLOUD				
		CASE eFM_SURVIVAL_CLOUD		
			//and the mission name is not null
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63MissionName)
				RETURN TRUE	
			//it is NULL but it probably shouldn't be, do a assert!
			ELSE
				PRINTLN("SHOULD_USE_MISSION_NAME_FROM_UGC - g_FMMC_STRUCT.tl63MissionName = NULL")
				SCRIPT_ASSERT("SHOULD_USE_MISSION_NAME_FROM_UGC - g_FMMC_STRUCT.tl63MissionName = NULL")
				RETURN FALSE				
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC


/// PURPOSE: Returns the totla number of players entering current job
FUNC INT GET_NUMBER_OF_PLAYERS_ENTERING_JOB()
	
	INT iTempplayerjoinbitset = Get_Bitfield_Of_Players_On_Or_Joining_Mission(Get_UniqueID_For_This_Players_Mission(PLAYER_ID()))
    NET_PRINT("GET_NUMBER_OF_PLAYERS_ENTERING_JOB - iTempplayerjoinbitset = ") NET_PRINT_INT(iTempplayerjoinbitset) NET_NL()

    //return the number
    RETURN SUM_BITS(iTempplayerjoinbitset)
ENDFUNC



// ===========================================================================================================
//      Strand Mission Data Storage Functions
//		To allow a Part Two to Quick Restart at Part One if required
// ===========================================================================================================

// PURPOSE:	Store header details for this mission if a Strand Mission Part One in case it is needed to Quick Restart after Part Two, or clear the data
PROC Update_Stored_Strand_Mission_Part_One_Details()

	// If this isn't a Strand Mission then clear out any stored data
	IF NOT (IS_THIS_IS_A_STRAND_MISSION())
		PRINTLN(".KGM [Triggering][Heist]: This is not a Strand Mission, so clearing any Strand Mission Part One Header Data.")
		Clear_Strand_Mission_Part_One_Header_Data()
		
		EXIT
	ENDIF

	// If this is a Part One mission, then store the details specifically grabbing the focus mission's trigger coords
	IF NOT (IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[0]))
	AND NOT (CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE() AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciSTART_AT_FIRST_OF_STRAND))
		PRINTLN(".KGM [Triggering][Heist]: This is a Strand Mission Part One, so storing Strand Mission Part One Header Data for a Part Two Quick Restart.")
		Store_Strand_Mission_Part_One_Header_Data(Get_Focus_Mission_Coordinates(), g_sTriggerMP.mtMissionData.mdID)
		
		EXIT
	ENDIF
	
	// Do nothing if this isn't a Part One strand mission
	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Triggering][Heist]: This must be a Strand Mission Part Two, so leaving the Strand Mission Part One Header Data untouched:")
		Debug_Output_Strand_Mission_Part_One_Header_Data()
	#ENDIF

ENDPROC



// ===========================================================================================================
//      Rounds Mission Data Storage Functions
//		To ensure an overlapped mission played in Rounds always re-triggers the correct mission
// ===========================================================================================================

// PURPOSE:	Store the ContentID Hash of this mission if it is a Rounds mission
PROC Update_Stored_Rounds_Mission_Details()

	IF NOT (IS_THIS_A_ROUNDS_MISSION())
		PRINTLN(".KGM [Triggering][Rounds]: This is not a mission being played in Rounds, so clear any Rounds Triggering Details.")
		g_roundsMissionHashCID = 0
		EXIT
	ENDIF
	
	g_roundsMissionHashCID = GET_HASH_KEY(g_sTriggerMP.mtMissionData.mdID.idCloudFilename)
	PRINTLN(".KGM [Triggering][Rounds]: Mission being played in Rounds [", g_sTriggerMP.mtMissionData.mdID.idCloudFilename, "]. Storing ContentID Hash: ", g_roundsMissionHashCID)

ENDPROC



// ===========================================================================================================
//      Mission Triggering External Calls To Other Systems
// ===========================================================================================================

// PURPOSE: Place any external mission cleanup function calls in here that need performed when a mission starts
PROC Call_Any_External_Mission_Routines_When_MP_Mission_Starts()

	// KW save players rank for bug 1322590
	
	g_rank_before_activity	= GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), FALSE) //GET_PLAYER_FM_RANK(PLAYER_ID())
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("\n Call_Any_External_Mission_Routines_When_MP_Mission_Starts g_rank_before_activity = ")
		NET_PRINT_INT(g_rank_before_activity)
					
		NET_PRINT("\n Call_Any_External_Mission_Routines_When_MP_Mission_Starts MPGlobals.g_rank_after_activity = ")
		NET_PRINT_INT(MPGlobals.g_rank_after_activity)
	#ENDIF
	
	
	// Increment the 'joined by phonecall' stat
	IF NOT (Is_Player_Being_Forced_Onto_Mission())
		INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_JOBS_JOINED_ON_PHONE, 1)
	ENDIF
	
	SETTING_RICH_PRESENCES_VALUES(g_sTriggerMP.mtMissionData.mdID.idMission, g_FMMC_STRUCT.iMissionSubType)
	g_I_MAX_XP_CAP_COUNTER = 0	// reset  the max xp cap for activities
	
	// KW Need to store players rank at start of activities. For bug 1322590
	
	
	// Let code know the text-label for the mission title (for display in the brief screen)
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: Let code know the active mission title for the brief screen:")
		NET_NL()
		NET_PRINT("           [")
		NET_PRINT(GET_MP_MISSION_NAME_TEXT_LABEL(g_sTriggerMP.mtMissionData.mdID.idMission, GET_PLAYER_TEAM(PLAYER_ID())))
		NET_PRINT("] ")
		NET_PRINT(GET_STRING_FROM_TEXT_FILE(GET_MP_MISSION_NAME_TEXT_LABEL(g_sTriggerMP.mtMissionData.mdID.idMission, GET_PLAYER_TEAM(PLAYER_ID()))))
		NET_NL()
	#ENDIF
	
	IF SHOULD_USE_MISSION_NAME_FROM_UGC(g_sTriggerMP.mtMissionData.mdID.idMission)		
		PRINTLN("SET_MISSION_NAME_FOR_UGC_MISSION(TRUE, ", g_FMMC_STRUCT.tl63MissionName, ")")
		IF g_FMMC_STRUCT.iMissionType != FMMC_TYPE_GANGHIDEOUT
			SET_MISSION_NAME_FOR_UGC_MISSION(TRUE, g_FMMC_STRUCT.tl63MissionName)
			SET_DESCRIPTION_FOR_UGC_MISSION_EIGHT_STRINGS(TRUE,
															g_FMMC_STRUCT.tl63MissionDecription[0],
															g_FMMC_STRUCT.tl63MissionDecription[1],
															g_FMMC_STRUCT.tl63MissionDecription[2],
															g_FMMC_STRUCT.tl63MissionDecription[3],
															g_FMMC_STRUCT.tl63MissionDecription[4],
															g_FMMC_STRUCT.tl63MissionDecription[5],
															g_FMMC_STRUCT.tl63MissionDecription[6],
															g_FMMC_STRUCT.tl63MissionDecription[7])
		ENDIF
		INFORM_CODE_OF_CONTENT_ID_OF_CURRENT_UGC_MISSION(g_FMMC_STRUCT.tl31LoadedContentID)
	ELSE
 		SET_MISSION_NAME(TRUE, GET_MP_MISSION_NAME_TEXT_LABEL(g_sTriggerMP.mtMissionData.mdID.idMission, GET_PLAYER_TEAM(PLAYER_ID())))
	ENDIF
	// Increment the Mission Started counter
	// NOTE: Some missions should be ignored (specifically added to avoid cutscene-only missions)
	SWITCH (g_sTriggerMP.mtMissionData.mdID.idMission)
		// Ignore these
		CASE eCOC_Do_Cutscene
			BREAK
		
		// Update Stats on all the others
		DEFAULT
			INCREMENT_SPECIFIC_MISSION_STARTED_COUNTER(g_sTriggerMP.mtMissionData.mdID.idMission)
			BREAK
	ENDSWITCH
	
//	REQUEST_SAVE(STAT_SAVETYPE_START_MATCH)
	
	
	// KGM 28/3/12: Start Metrics Collection for Mission
	#IF IS_DEBUG_BUILD
		Debug_Metrics_MP_Mission_Started(g_sTriggerMP.mtMissionData.mdID.idMission, g_sTriggerMP.mtMissionData.mdID.idVariation)
	#ENDIF
	
	
	// Increase Crew Heist stat
	IF (Is_FM_Cloud_Loaded_Activity_A_Heist(g_sTriggerMP.mtMissionData.mdID))
		GAMER_HANDLE player_handle
		player_handle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
		
		IF (NETWORK_CLAN_SERVICE_IS_VALID())
			IF (NETWORK_CLAN_PLAYER_IS_ACTIVE(player_handle))
				INCREMENT_BY_MP_INT_PLAYER_STAT(GET_PLAYER_CREW_NUMBER_HEISTS_PLAYER_ID(PLAYER_ID()), 1)
			ENDIF
		ENDIF
	ENDIF
	
	// Pay for starting mission - FM specific at the moment
	INT FM_MissionType	= Convert_MissionID_To_FM_Mission_Type(g_sTriggerMP.mtMissionData.mdID.idMission)
	INT missionSubtype	= Get_SubType_For_FM_Cloud_Loaded_Activity(g_sTriggerMP.mtMissionData.mdID)
	IF (DO_I_NEED_TO_PAY_TO_PLAY_THIS_FM_ACTIVITY(FM_MissionType, missionSubtype))
	//AND NOT IS_THIS_TRANSITION_SESSION_IS_A_QUALIFYING_PLAYLIST()
	//AND NOT IS_PLAYLIST_IS_A_QUALIFYING_PLAYLIST()
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: For Code - Paying Match Entry Fee: $")
			NET_PRINT_INT(GET_FM_ACTIVITY_COST(FM_MissionType, missionSubtype))
			NET_PRINT("   [UniqueMatchID: ")
			NET_PRINT(GET_MP_MISSION_NAME(g_sTriggerMP.mtMissionData.mdID.idMission))
			NET_PRINT("  Subtype: ")
			NET_PRINT_INT(missionSubtype)
			NET_PRINT("]")
			NET_NL()
		#ENDIF
		g_bPlayerHasToPayForMission = TRUE
		
		INT iNumberOfPlayers = GET_NUMBER_OF_PLAYERS_ENTERING_JOB()
		
		g_iMissionCost 				= GET_FM_ACTIVITY_COST(FM_MissionType, missionSubtype, FALSE, iNumberOfPlayers)
		//g_tl61CurrentMissionName 	= GET_MP_MISSION_NAME(g_sTriggerMP.mtMissionData.mdID.idMission)
	ELSE
		g_bPlayerHasToPayForMission = FALSE		
	ENDIF
	
	IF g_sTriggerMP.mtMissionData.mdID.idMission != eFM_GANG_ATTACK_CLOUD	//For Bug 1583610
		// KGM 17/12/12: Flush the Feed on mission start
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: Flushing the Feed before Mission: ") NET_PRINT(GET_MP_MISSION_NAME(g_sTriggerMP.mtMissionData.mdID.idMission)) NET_NL()
		#ENDIF
	 	g_b_ReapplyStickySaveFailedFeed = FALSE		
		THEFEED_FLUSH_QUEUE()
	ENDIF
	
	// BUG 2068131: Hide the 'invite', etc. signifiers when on a mission
	PRINTLN(".KGM [Triggering]: Hide Cellphone Signifiers at start of mission.")
	HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE(TRUE)
	
	// KGM 2/11/14 [BUG 2060589]: Update the Stored Strand Mission Part One Header details as appropriate
	Update_Stored_Strand_Mission_Part_One_Details()
	
	// KGM 11/11/14 [BUG 1965563]: Spectators need to store the 'play again' variables based on the full mission details
	// NOTE: When a player joins as a spectator the 'IS_PLAYER_SPECTATOR' flag isn't set yet, but a spectator won't have a Focus Mission
	IF NOT (Is_Mission_Being_Triggered_By_Matc())
		PRINTLN(".KGM [Triggering]: Mission is not being triggered by Matc. Assume Spectator. Store Mission Data for Replay if needed.")
		
		// KGM 30/1/15 [BUG 2182584]: If this is a spectator on a Finale Part2, need to update the mission subtype because it is stored in the cloud as 'prep'
		PRINTLN(".KGM [Triggering]: ...rootContentID: ", g_FMMC_STRUCT.iRootContentIDHash, ".  RCID PacificStandard Part2: ", g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job2)
		IF (g_FMMC_STRUCT.iRootContentIDHash = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job2)
			g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_HEIST
			PRINTLN(".KGM [Triggering]: ...This is the RCID of Pacific Standard Part2, so updating subtype from Planning to Heist.")
		ENDIF
		
		Store_Copy_Registered_Data_From_Full_Mission_Data(g_sTriggerMP.mtMissionData.mdID)
	ENDIF
	
	// KGM 24/6/15 [BUG 2365221]: Ned to store details of a Rounds mission being triggered to ensure the same mission gets triggered for the next round (only needed for overlapped missions)
	Update_Stored_Rounds_Mission_Details()
	
	// *** Do a dead ped check before changing anything Player Ped related ***
	IF (IS_PED_INJURED(PLAYER_PED_ID()))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: Player Ped is dead so can't update any ped-related flags") NET_NL()
		#ENDIF
	ELSE
		// Do any ped-related tasks in here following the dead check
		// ...prevent the player flying through windscreens
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: Changing Ped flag State for player when mission starts: PCF_WillFlyThroughWindscreen = FALSE") NET_NL()
		#ENDIF
		
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Place any external mission cleanup function calls in here that need performed when a mission ends
PROC Call_Any_External_Mission_Routines_When_MP_Mission_Ends()

	// clear inventry box
	flag_display_UPDATE_INVENTORY_BOX_CONTENT[0] = 0
	flag_display_UPDATE_INVENTORY_BOX_CONTENT[1] = 0
	flag_display_UPDATE_INVENTORY_BOX_CONTENT[2] = 0
	flag_display_UPDATE_INVENTORY_BOX_CONTENT[3] = 0
	flag_display_UPDATE_INVENTORY_BOX_CONTENT[4] = 0
	flag_display_UPDATE_INVENTORY_BOX_CONTENT[5] = 0
	flag_display_UPDATE_INVENTORY_BOX_CONTENT[6] = 0
	
	RESETTING_RICH_PRESENCES_VALUES()
			
	// BC: Clear the player roles given to them during a mission 
//	RESET_PLAYER_ROLE_HUD()
	
	//Clean up the players inventory
	CLEAR_INVENTORY_BOX_CONTENT()
		
	// if there was a spawning area setup for this mission then clear it. 
	CLEAR_SPAWN_AREA()
	
	// if there was an off mission spawn exclusion area set up then clear it. 
	CLEAR_SPAWN_OCCLUSION_AREA_FOR_PLAYERS_NOT_ON_THIS_MISSION()
	
	// Delete any waypoints added by this player when a mission ends
	DELETE_WAYPOINTS_FROM_THIS_PLAYER()	


	//Resets any afterlife settings to just bring up the hud when the player dies
	IF GET_GAME_STATE_ON_DEATH() != AFTERLIFE_SET_SPECTATORCAM
	AND GET_GAME_STATE_ON_DEATH() != AFTERLIFE_MANUAL_RESPAWN
	AND GET_GAME_STATE_ON_DEATH() != AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP
		SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
	ENDIF
	
	// KGM 28/3/12: Stop Metrics Collection for Mission
	#IF IS_DEBUG_BUILD
		Debug_Metrics_MP_Mission_Ended(g_sTriggerMP.mtMissionData.mdID.idMission, g_sTriggerMP.mtMissionData.mdID.idVariation)
	#ENDIF
	
	// Let code know there is no longer a mission title to display
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: Let code know there is no active mission title for display on the brief screen") NET_NL()
	#ENDIF
	
	//Clean up the mission names
	SET_MISSION_NAME(FALSE)
	SET_MISSION_NAME_FOR_UGC_MISSION(FALSE)	
	SET_DESCRIPTION_FOR_UGC_MISSION_EIGHT_STRINGS(FALSE)
	INFORM_CODE_OF_CONTENT_ID_OF_CURRENT_UGC_MISSION("")
	
	// Set the last mission name stat
	IF (STAT_SET_GXT_LABEL(MP_LAST_MISSION_NAME, GET_MP_MISSION_NAME_TEXT_LABEL(g_sTriggerMP.mtMissionData.mdID.idMission, TEAM_INVALID)))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: Setting Last Mission Name Stat to ")
			NET_PRINT(GET_MP_MISSION_NAME_TEXT_LABEL(g_sTriggerMP.mtMissionData.mdID.idMission, TEAM_INVALID))
			NET_PRINT(" [") NET_PRINT(GET_STRING_FROM_TEXT_FILE(GET_MP_MISSION_NAME_TEXT_LABEL(g_sTriggerMP.mtMissionData.mdID.idMission, TEAM_INVALID))) NET_PRINT("]")
			NET_NL()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: ERROR: Failed to Set Last Mission Name Stat to ")
			NET_PRINT(GET_MP_MISSION_NAME_TEXT_LABEL(g_sTriggerMP.mtMissionData.mdID.idMission, TEAM_INVALID))
			NET_PRINT(" [") NET_PRINT(GET_STRING_FROM_TEXT_FILE(GET_MP_MISSION_NAME_TEXT_LABEL(g_sTriggerMP.mtMissionData.mdID.idMission, TEAM_INVALID))) NET_PRINT("]")
			NET_NL()
		#ENDIF
	ENDIF
	
	// BUG 2068131: Reveal the 'invite', etc. signifiers when off a mission
	PRINTLN(".KGM [Triggering]: Reveal Cellphone Signifiers at end of mission.")
	HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE(FALSE)
	
	// *** Do a dead ped check before changing anything Player Ped related ***
	IF (IS_PED_INJURED(PLAYER_PED_ID()))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: Player Ped is dead so can't update any ped-related flags") NET_NL()
		#ENDIF
	ELSE
		// Do any ped-related tasks in here following the dead check
		// ...allow the player to fly through windscreens again when not on-mission
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: Changing Ped flag State for player when mission ends: PCF_WillFlyThroughWindscreen = TRUE") NET_NL()
		#ENDIF
	
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, TRUE)
	ENDIF
	/*
	// KW save players rank for bug 1322590
	MPGlobals.g_rank_after_activity	= GET_PLAYER_FM_RANK(PLAYER_ID())
	IF MPGlobals.g_rank_after_activity	!= g_rank_before_activity
		int i
		FOR i = g_rank_before_activity+1 to MPGlobals.g_rank_after_activity
			PRINT_UNLOCK_HELP_TICKERS(i)
		ENDFOR
	ENDIF
	*/
	
	// KGM 6/7/14: If this is a Heist-related mission, then start a CoolDown period
	IF (Is_FM_Cloud_Loaded_Activity_A_Heist(g_sTriggerMP.mtMissionData.mdID))
	OR (Is_FM_Cloud_Loaded_Activity_Heist_Planning(g_sTriggerMP.mtMissionData.mdID))
		Set_New_Heist_Strand_Cooldown_Period()
	ENDIF
		// FEATURE_HEIST_PLANNING
	
ENDPROC




// ===========================================================================================================
//      Additional Player details when a mission starts and ends
// ===========================================================================================================

// PURPOSE:	Store any additional Player details about this mission when it starts
PROC Store_Mission_Extra_Player_Details()

	// KGM 31/7/14: Need to store the Type and Subtype in case this mission has a 'Part 2' that needs setup the same way
	g_sMDStrandMissionsMP.dfsmFmmcType	= g_FMMC_STRUCT.iMissionType
	g_sMDStrandMissionsMP.dfsmSubtype	= g_FMMC_STRUCT.iMissionSubType
	
	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Triggering]: Storing Type and Subtype details in case needed for a Part 2 mission. Mission Type: ", g_sMDStrandMissionsMP.dfsmFmmcType, ". Subtype: ", g_sMDStrandMissionsMP.dfsmSubtype)
	#ENDIF

	// KGM 20/5/14: Nasty Hardcoded Hack to store the contactID for Planning missions (for Bug 1814463)
	BOOL isHeistPlanning = FALSE
		// FEATURE_HEIST_PLANNING

	// Store Extra details about the type of this mission
	// KGM 6/5/13: NASTY METHOD, BUT FAST
	IF (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION)
		IF (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CONTACT)
		OR (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_FLOW_MISSION)
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_CONTACT_MISSION
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [Triggering]: Storing 'Contact Mission' as extra mission details") NET_NL()
			#ENDIF
		ELIF (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_HEIST)
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_HEIST
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [Triggering]: Storing 'Heist' as extra mission details") NET_NL()
			#ENDIF
		ELIF (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_PLANNING)
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_HEIST_PLANNING
			
			// KGM 20/5/14: Part of a Nasty Hardcoded Hack to store the contactID for Planning missions (for Bug 1814463)
			isHeistPlanning = TRUE
				// FEATURE_HEIST_PLANNING
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [Triggering]: Storing 'Heist Planning' as extra mission details") NET_NL()
			#ENDIF
		ELIF (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_RANDOM)
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_RANDOM_EVENT
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [Triggering]: Storing 'Random Event' as extra mission details") NET_NL()
			#ENDIF
		ELIF (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_VERSUS)
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_VERSUS_MISSION
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [Triggering]: Storing 'Versus Mission' as extra mission details") NET_NL()
			#ENDIF
		ELIF (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_COOP)
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_COOP_MISSION
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [Triggering]: Storing 'Coop Mission' as extra mission details") NET_NL()
			#ENDIF
		ELIF (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS)
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_LTS_MISSION
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [Triggering]: Storing 'LTS Mission' as extra mission details") NET_NL()
			#ENDIF
		ELIF (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CTF)
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_CTF_MISSION
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [Triggering]: Storing 'CTF Mission' as extra mission details") NET_NL()
			#ENDIF
		ELSE
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_GENERIC_ACTIVITY
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [Triggering]: Storing 'Generic Activity' as extra mission details - unknown Subtype of Mission Type") NET_NL()
			#ENDIF
		ENDIF
	ELSE
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_GENERIC_ACTIVITY
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: Storing 'Generic Activity' as extra mission details - this job is not a Subtype of a Mission Type") NET_NL()
		#ENDIF
	ENDIF
	
	// KGM 20/5/14: Part of a Nasty Hardcoded Hack to store the contactID for Planning missions (for Bug 1814463)
	// This is used by Conor to prevent the player phoning for any special abilities from that contact during their mission
	IF (isHeistPlanning)
		PRINTLN(".KGM MP [Triggering][Hardcoded]: TEMP: Storing 'CHAR_LESTER' as the contact for a Planning Mission - this needs handled correctly")
		g_eCurrentMissionContact = CHAR_LESTER
		EXIT
	ENDIF
		// FEATURE_HEIST_PLANNING
		
	// Store the mission's contact
	g_eCurrentMissionContact = Get_Contact_For_FM_Cloud_Loaded_Activity(g_sTriggerMP.mtMissionData.mdID)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear any additional Player details about this mission when it ends
PROC Clear_Mission_Extra_Player_Details()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: Clearing extra mission details") NET_NL()
	#ENDIF

	// Clear any addition activity type info
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sMissionExtraMP.satmpSpecialActivityType = SATMP_NO_ACTIVITY
	
	// Clear the mission's contact
	g_eCurrentMissionContact = NO_CHARACTER
	
ENDPROC





// ===========================================================================================================
//      Mission Triggering Condition Checking routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      One-Off Checks Prior To Accepting Trigger Request
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check for one-off game conditions that should prevent the player trying to trigger a mission
//
// INPUT PARAMS:		paramMissionData	The Mission Triggering Data
// RETURN VALUE:		BOOL				TRUE if there is a one-off restriction that stops an attempt to trigger a mission, otherwise FALSE
FUNC BOOL Check_For_PreAcceptance_Restriction(MP_MISSION_DATA paramMissionData)

	// Is the mission NULL?
	IF (paramMissionData.mdID.idMission = eNULL_MISSION)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........RESTRICTION, mission is eNULL_MISSION.") NET_NL()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// Is the mission disabled?
	IF (IS_MP_MISSION_DISABLED(paramMissionData.mdID.idMission))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........RESTRICTION, mission is DISABLED.") NET_NL()
		#ENDIF
		
		RETURN TRUE
	ENDIF

	// Is another mission triggering request active?
	IF (Is_Mission_Triggering_Request_In_Progress())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........RESTRICTION, mission launch already requested.") NET_NL()
		#ENDIF
		
		RETURN TRUE
	ENDIF

	
	// Don't offer a mission if player is inside a shop (ignoring safehouse Car Mod shops)
// KGM 20/12/12: Convert this check to 'browsing in a shop' - this was blocking the shooting range from triggering
//	BOOL IncludeCheckForSafehouseCarMods = FALSE
//	IF (IS_PLAYER_IN_ANY_SHOP(IncludeCheckForSafehouseCarMods))
	IF (IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........RESTRICTION, player is browsing items in a shop.") NET_NL()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// No one-off pre-acceptance restriction exists
	RETURN FALSE
	
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      One-Off Checks Prior To Issuing Comms
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Continuous Checks for game conditions that should cancel the Mission Triggering attempt (prior to acceptance, or during pre-launch comms)
//
// RETURN VALUE:		BOOL			TRUE if there is a one-off restriction that stops an attempt to issue a comms to the player, otherwise FALSE
FUNC BOOL Check_For_PreComms_Restriction()

	IF (Is_MP_Comms_Disabled())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........RESTRICTION, player's Comms Devices are currently disabled.") NET_NL()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// No one-off pre-comms restriction exists
	RETURN FALSE

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Continuous Checks While Mission Trigger Comms Is Ongoing (also called prior to accepting request)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Continuous Checks for game conditions that should cancel the Mission Triggering attempt (prior to acceptance, or during pre-launch comms)
//
// RETURN VALUE:		BOOL			TRUE if there is a one-off restriction that stops an attempt to trigger a mission, otherwise FALSE
FUNC BOOL Check_For_PreLaunch_Restriction()
	
	// Is Player OK?
	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID()))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........RESTRICTION, player is not OK.") NET_NL()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// Has mission launching been temporarily blocked (usually because an ambient activity is in the process of launching)?
	// NOTE: This also gets checked in the function below, so I'll only check this in debug to get specific console log messages
	#IF IS_DEBUG_BUILD
		IF (Is_This_Player_Using_Temporary_Mission_Block())
			NET_PRINT("...........RESTRICTION, there is a temporary mission block active - probably because an ambient activity is in the process of launching.") NET_NL()
			
			RETURN TRUE
		ENDIF
	#ENDIF
	
	// Is player launching a mission?
	// KGM 14/10/11: Use existing function for now but introduce an MT OVERVIEW flag that gets set within these new routines
	IF (IS_MP_MISSION_LAUNCH_IN_PROGRESS())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........RESTRICTION, player is launching a mission.") NET_NL()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// Is player already on a mission?
	// KGM 14/10/11: Use existing function for now but introduce an MT OVERVIEW flag that gets set within these new routines
	IF (IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID(), FALSE))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........RESTRICTION, player is already on a mission.") NET_NL()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// Mission Control has marked the mission as Finished?
	IF (Has_Mission_Finished_Event_Been_Received())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........RESTRICTION, Mission Control has broadcast 'Mission Finished'.") NET_NL()
		#ENDIF
		
		RETURN TRUE
	ENDIF

	// Mission Control has marked the mission as Failed To launch?
	IF (Has_Mission_Failed_Event_Been_Received())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........RESTRICTION, Mission Control has broadcast 'Mission Failed' (ie: failed to launch).") NET_NL()
		#ENDIF
		
		RETURN TRUE
	ENDIF

	// Mission Control has marked the mission as Full?
	IF (Has_Mission_Full_Event_Been_Received())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........RESTRICTION, Mission Control has broadcast 'Mission Full' for this player's team.") NET_NL()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// Has the mission been forced to quit during pre-mission phase?
	IF (Has_Force_Quit_PreMission_Been_Received())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........RESTRICTION, Force Quit PreMission has been received (this may be to fix a host migration problem).") NET_NL()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// No pre-launch restriction exists
	RETURN FALSE

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Continuous Checks While Mission Trigger Comms Is Ongoing (also called prior to accepting request)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check for any condition that should delay the pre-launch Comms
//
// RETURN VALUE:		BOOL			TRUE if there is an active condition that should delay (but not kill) the pre-launch phonecall, otherwise FALSE
FUNC BOOL Check_For_PreLaunch_Comms_Delay_Condition()

	//BC: 16/07/2012 - Removing as this is the lowest priority to display in the space. so it could hold up the missions for ages. 
	// Delay if the EOM screen is still on display from a previous mission
//	IF (IS_EOM_SCREEN_BEING_DISPLAYED())
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("...........DELAY PRE-MISSION COMMS, End Of Mission Screen is being displayed.") NET_NL()
//		#ENDIF
//			
//		RETURN TRUE
//	ENDIF
	
	// There is no pre-launch Comms delay
	RETURN FALSE

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Performs all pre-acceptance checks
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check all pre-acceptance conditions
//
// INPUT PARAMS:		paramMissionData	The Mission Triggering Data
// RETURN VALUE:		BOOL				TRUE if there is any restriction that stops an attempt to trigger a mission, otherwise FALSE
FUNC BOOL Check_For_Any_Restriction_That_Prevents_Mission_Triggering(MP_MISSION_DATA paramMissionData)
	
	// Perform PreAcceptance Mission Triggering Restriction Checks
	IF (Check_For_PreAcceptance_Restriction(paramMissionData))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Pre-Acceptance Mission Triggering Restriction Check - FAILED") NET_NL()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// Perform PreLaunch Mission Triggering Restriction Checks (prior to acceptance)
	IF (Check_For_PreLaunch_Restriction())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Pre-Launch Mission Triggering Restriction Check (prior to acceptance) - FAILED") NET_NL()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// There is no restriction
	RETURN FALSE
	
ENDFUNC





// ===========================================================================================================
//      Mission Triggering Setup routines
// ===========================================================================================================

// PURPOSE:	Setup the Mission Details
//
// INPUT PARAMS:		paramMissionData			Struct containing the mission ID and the Instance ID of the mission to be joined
PROC Setup_Mission_Triggering_Details(MP_MISSION_DATA paramMissionData)

	// Clear some details
	Clear_All_MP_Mission_Triggering_Bitflags()
	Clear_MP_Mission_Triggering_Offer_Timeout()
	Clear_Mission_Triggering_Mission_Over_State()
	
	// Store the Mission Data
	g_sTriggerMP.mtMissionData = paramMissionData

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Setup the pre-Launch Comms Details
//
// INPUT PARAMS:		paramCharacter				The Character ID string (from the 'Character' dropdown box in dialogueStar)
//						paramSpeakerID				The Speaker ID string (from the 'Speaker' value in dialogueStar)
//						paramCharSheetID			The CharSheetID for the contact making the phonecall
//						paramGroupID				The Subtitle Group ID string for the Conversation (from the 'Subtitle Group ID' value in dialogueStar)
//						paramRootID					The Root ID string for the Conversation (from the 'Root' value in dialogueStar)
//						paramUseTxtmsg				[DEFAULT = FALSE] TRUE if the intro communication should be a text message, FALSE if not
PROC Setup_Mission_Triggering_Comms(STRING paramCharacter, STRING paramSpeakerID, enumCharacterList paramCharSheetID, STRING paramGroupID, STRING paramRootID, BOOL paramUseTxtmsg = FALSE)
	
	g_sTriggerMP.mtCharSheetAsInt	= ENUM_TO_INT(paramCharSheetID)
	g_sTriggerMP.mtRoot				= paramRootID
	
	IF (paramUseTxtmsg)
		Set_MT_Bitflag_As_Use_Txtmsg()
		EXIT
	ENDIF
	
	// Using a phonecall or radio message
	g_sTriggerMP.mtCharacter		= paramCharacter
	g_sTriggerMP.mtSpeaker			= paramSpeakerID
	g_sTriggerMP.mtGroup			= paramGroupID
	
ENDPROC





// ===========================================================================================================
//      Generic Mission Triggering Functions
// ===========================================================================================================

// PURPOSE:	Store the details that allows a mission to be offered to the player, and then launched if the player accepts.
//
// INPUT PARAMS:		paramMissionData			Struct containing the mission ID and the Instance ID of the mission to be joined
//						paramCharacter				The Character ID string (from the 'Character' dropdown box in dialogueStar)
//						paramSpeakerID				The Speaker ID string (from the 'Speaker' value in dialogueStar)
//						paramCharSheetID			The CharSheetID for the contact making the phonecall
//						paramGroupID				The Subtitle Group ID string for the Conversation (from the 'Subtitle Group ID' value in dialogueStar)
//						paramRootID					The Root ID string for the Conversation (from the 'Root' value in dialogueStar)
//						paramUseTxtmsg				[DEFAULT = FALSE] TRUE if the mission intro communication is a text message, otherwise FALSE
PROC Offer_To_Launch_Mission(MP_MISSION_DATA paramMissionData, STRING paramCharacter, STRING paramSpeakerID, enumCharacterList paramCharSheetID, STRING paramGroupID, STRING paramRootID, BOOL paramUseTxtmsg = FALSE)

	#IF IS_DEBUG_BUILD
		NET_NL()
		NET_PRINT("...KGM MP [Triggering]: ")
		NET_PRINT_TIME()
		NET_PRINT(" - Offer_To_Launch_Mission")
		NET_NL()
	#ENDIF
	
	// Perform pre-acceptance checks
	IF (Check_For_Any_Restriction_That_Prevents_Mission_Triggering(paramMissionData))
		// Broadcast that player failed to join the mission - before cleanup
		Broadcast_Reply_Player_Failed_To_Join_Mission(paramMissionData)
		
		EXIT
	ENDIF

	// Setup the Mission Triggering Information
	Setup_Mission_Triggering_Details(paramMissionData)
	Setup_Mission_Triggering_Comms(paramCharacter, paramSpeakerID, paramCharSheetID, paramGroupID, paramRootID, paramUseTxtmsg)
	Clear_Cloud_Loaded_Mission_Data()
	
	// Allow Mission Details Box to be displayed?
	IF (Should_MP_Mission_Team_Display_Mission_Details_Box(paramMissionData.mdID.idMission, GET_PLAYER_TEAM(PLAYER_ID())))
		// Allow display of the Mission details box
		Set_MT_Bitflag_Display_Mission_Details_Box()
	ENDIF
		
	// Set the state to be 'Start PreMission Communication'
	Set_Mission_Triggering_State_To_Start_PreMission_Comms()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Offer_To_Launch_Mission(): Mission accepted for launch attempt.")
		NET_PRINT("  [uniqueID=")
		NET_PRINT_INT(g_sTriggerMP.mtMissionData.mdUniqueID)
		NET_PRINT("]")
		NET_NL()
		Debug_Output_Stored_Mission_Triggering_Details()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the details that forces a mission to launch AFTER a pre-launch Communication.
//
// INPUT PARAMS:		paramMissionData			Struct containing the mission ID and the Instance ID of the mission to be joined
//						paramCharacter				The Character ID string (from the 'Character' dropdown box in dialogueStar)
//						paramSpeakerID				The Speaker ID string (from the 'Speaker' value in dialogueStar)
//						paramCharSheetID			The CharSheetID for the contact making the phonecall
//						paramGroupID				The Subtitle Group ID string for the Conversation (from the 'Subtitle Group ID' value in dialogueStar)
//						paramRootID					The Root ID string for the Conversation (from the 'Root' value in dialogueStar)
//						paramUseTxtmsg				[DEFAULT = FALSE] TRUE if the mission intro communication is a text message, otherwise FALSE
//						paramShowCutscene			[DEFAULT = FALSE] TRUE if the mission should show a pre-mission cutscene, otherwise FALSE
PROC Force_Launch_Mission_After_Comms(MP_MISSION_DATA paramMissionData, STRING paramCharacter, STRING paramSpeakerID, enumCharacterList paramCharSheetID, STRING paramGroupID, STRING paramRootID, BOOL paramUseTxtmsg = FALSE, BOOL paramShowCutscene = FALSE)

	#IF IS_DEBUG_BUILD
		NET_NL()
		NET_PRINT("...KGM MP [Triggering]: ")
		NET_PRINT_TIME()
		NET_PRINT(" - Force_Launch_Mission_After_Comms")
		NET_NL()
	#ENDIF
	
	// Perform pre-acceptance checks
	IF (Check_For_Any_Restriction_That_Prevents_Mission_Triggering(paramMissionData))
		// Broadcast that player failed to join the mission - before cleanup
		Broadcast_Reply_Player_Failed_To_Join_Mission(paramMissionData)
		
		EXIT
	ENDIF

	// Setup the Mission Triggering Information
	Setup_Mission_Triggering_Details(paramMissionData)
	Setup_Mission_Triggering_Comms(paramCharacter, paramSpeakerID, paramCharSheetID, paramGroupID, paramRootID, paramUseTxtmsg)
	Clear_Cloud_Loaded_Mission_Data()
	
	// The Mission launch needs to be forced, not optional
	Set_MT_Bitflag_As_Force_Onto_Mission()
	
	// Show a pre-mission cutscene if required
	IF (paramShowCutscene)
		Set_MT_Bitflag_Show_PreMission_Cutscene()
	ENDIF
	
	// Allow Mission Details Box to be displayed?
	IF (Should_MP_Mission_Team_Display_Mission_Details_Box(paramMissionData.mdID.idMission, GET_PLAYER_TEAM(PLAYER_ID())))
		// Allow display of the Mission details box
		Set_MT_Bitflag_Display_Mission_Details_Box()
	ENDIF
		
	// Set the state to be 'Start PreMission Communication'
	Set_Mission_Triggering_State_To_Start_PreMission_Comms()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: Force_Launch_Mission_After_Comms(): Mission accepted for launch attempt.")
		NET_PRINT("  [uniqueID=")
		NET_PRINT_INT(g_sTriggerMP.mtMissionData.mdUniqueID)
		NET_PRINT("]")
		NET_NL()
		Debug_Output_Stored_Mission_Triggering_Details()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the details that forces a mission to launch WITHOUT a pre-launch Communication.
//
// INPUT PARAMS:		paramMissionData			Struct containing the mission ID and the Instance ID of the mission to be joined
//						paramShowCutscene			[DEFAULT = FALSE] TRUE if the mission should show a pre-mission cutscene, otherwise FALSE
PROC Force_Launch_Mission(MP_MISSION_DATA paramMissionData, BOOL paramShowCutscene = FALSE, BOOL bSetCopyStructData = FALSE)

	#IF IS_DEBUG_BUILD
		NET_NL()
		NET_PRINT("...KGM MP [Triggering]: ")
		NET_PRINT_TIME()
		NET_PRINT(" - Force_Launch_Mission")
		NET_NL()
	#ENDIF
	
	// Perform pre-acceptance checks
	IF (Check_For_Any_Restriction_That_Prevents_Mission_Triggering(paramMissionData))
		// Broadcast that player failed to join the mission - before cleanup
		Broadcast_Reply_Player_Failed_To_Join_Mission(paramMissionData)
		
		EXIT
	ENDIF

	// Setup the Mission Triggering Information
	Setup_Mission_Triggering_Details(paramMissionData)
	Clear_Cloud_Loaded_Mission_Data()
	
	// The Mission launch needs to be forced, not optional
	Set_MT_Bitflag_As_Force_Onto_Mission()
	
	// Set the initial triggering stage
	IF (paramShowCutscene)
		// Set the state to be 'Launch Cutscene' - ignores communication stage
		Set_MT_Bitflag_Show_PreMission_Cutscene()
		Set_Mission_Triggering_State_To_Launch_Cutscene()
	ELSE
		// Set the state to be 'Check if available' - ignores communication and cutscene stages
		// KGM 31/8/12: This used to go straight to launch, but that can allow too many players on the mission if players are forced onto a non-team-based mission
		Set_Mission_Triggering_State_To_Check_Still_Available()
	ENDIF
	
	// Allow Mission Details Box to be displayed?
	IF (Should_MP_Mission_Team_Display_Mission_Details_Box(paramMissionData.mdID.idMission, GET_PLAYER_TEAM(PLAYER_ID())))
		// Allow display of the Mission details box
		Set_MT_Bitflag_Display_Mission_Details_Box()
	ENDIF
	IF bSetCopyStructData
	
		INT paramSlot = -1
		INT iLoop
		REPEAT MAX_MISSIONS_SHARED_CLOUD_DATA iLoop
			IF GlobalServerBD_MissionsShared.cloudDetails[iLoop].mscInUse
			AND NOT IS_STRING_NULL_OR_EMPTY(GlobalServerBD_MissionsShared.cloudDetails[iLoop].mscHeaderData.tlName)
			AND ARE_STRINGS_EQUAL(GlobalServerBD_MissionsShared.cloudDetails[iLoop].mscHeaderData.tlName, paramMissionData.mdID.idCloudFilename)
				paramSlot = iLoop
				BREAKLOOP
			ENDIF
		ENDREPEAT
		
		IF paramSlot != -1
			Maintain_Shared_Mission_Data_Local_Copy(paramSlot, TRUE)
			MP_MISSION idMission = Convert_FM_Mission_Type_To_MissionID(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iType)
			
			INT paramAtCoordsMPSlot = -1
			REPEAT MAX_NUM_MP_MISSIONS_AT_COORDS iLoop
				IF g_sAtCoordsMP[iLoop].matcMissionIdData.idMission = idMission
				AND ARE_STRINGS_EQUAL(g_sAtCoordsMP[iLoop].matcMissionIdData.idCloudFilename, GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlName)
					paramAtCoordsMPSlot = iLoop
					BREAKLOOP
				ENDIF
			ENDREPEAT
			
			IF paramAtCoordsMPSlot != -1
				Set_MissionsAtCoords_Mission_Invite_Has_Been_Accepted(paramAtCoordsMPSlot)
			ENDIF
		ENDIF
		
		
	ENDIF
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: Force_Launch_Mission(): Mission accepted for launch attempt.")
		NET_PRINT("  [uniqueID=")
		NET_PRINT_INT(g_sTriggerMP.mtMissionData.mdUniqueID)
		NET_PRINT("]")
		NET_NL()
		Debug_Output_Stored_Mission_Triggering_Details(FALSE)
	#ENDIF

ENDPROC




// ===========================================================================================================
//      Mission Triggering Cleanup routines
// ===========================================================================================================

// PURPOSE:	Clears the Mission Details overlay and the triggering data
PROC Cleanup_Mission_Triggering()

	// If the Mission Details Overlay is still on display, switch it off, or extend it if the mission was full
	IF (Has_Mission_Full_Event_Been_Received())
		Extend_MP_Mission_Details_Overlay_Display_Time_When_Mission_Full()
	ELSE
		Switch_Off_MP_Mission_Details_Overlay(g_sTriggerMP.mtMissionData.mdID.idMission, g_sTriggerMP.mtMissionData.iInstanceId)
	ENDIF
	
	// Clear the Mission Trigger details
	Clear_MP_Mission_Triggering_Data()

ENDPROC




// ===========================================================================================================
//      Cutscene Data routines
// ===========================================================================================================

// PURPOSE:	Gather data for the cutscene launch
//
// RETURN VALUE:		MP_MISSION_DATA			A Mission Data struct filled with Cutscene data
//
// NOTES:	This will currently always use the Do Flow Cutscene mission script passing in the missionID as the variation and the generic int as the mission variation
//			The generic Int is passed into the cutscene script as a cutscene variation ID - the way cutscenes are triggered may change
FUNC MP_MISSION_DATA Gather_Cutscene_Launching_Data()

	MP_MISSION_DATA theCutsceneData
	theCutsceneData.mdID.idMission				= eCOC_Do_Cutscene
	theCutsceneData.iInstanceId			= g_sTriggerMP.mtMissionData.iInstanceId
	theCutsceneData.mdID.idVariation	= ENUM_TO_INT(g_sTriggerMP.mtMissionData.mdID.idMission)
	theCutsceneData.mdGenericInt		= g_sTriggerMP.mtMissionData.mdID.idVariation
	
	RETURN theCutsceneData

ENDFUNC




// ===========================================================================================================
//      Post Launch routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Hide Hud and Radar Functions
//		(1/7/13: Added to cover a gap where these re-display as teh transition ends and the mission starts)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set up a maintenance routine with a safety timeout that will continue to hide the hud and radar after the mission launches for a short time
PROC Setup_Hide_Hud_And_Radar_Post_Launch()

	// Setup the control variables
	g_sTriggerPostLaunchMP.plHideHudActive	= TRUE
	g_sTriggerPostLaunchMP.plHideHudTimeout	= GET_TIME_OFFSET(GET_NETWORK_TIME(), HIDE_HUD_SAFETY_TIMEOUT_msec)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]:")
		NET_PRINT_TIME()
		NET_PRINT("Setup_Hide_Hud_And_Radar_Post_Launch. Timeout: ")
		NET_PRINT(GET_TIME_AS_STRING(g_sTriggerPostLaunchMP.plHideHudTimeout))
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Hide Hud and Radar controls - the mission triggerer doesn't need to do this anymore
PROC Clear_Hide_Hud_And_Radar_Post_Launch()

	g_sTriggerPostLaunchMP.plHideHudActive	= FALSE
	g_sTriggerPostLaunchMP.plHideHudTimeout	= GET_NETWORK_TIME()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	If necessary, continue to hide the hud and the radar post-launch
PROC Maintain_Hide_Hud_And_Radar_Post_Launch()

	IF NOT (g_sTriggerPostLaunchMP.plHideHudActive)
		EXIT
	ENDIF
	
	// Still hiding HUD, so check if the activity script still wants the Mission Triggerer to control this
	IF NOT (SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED())
		// ...no
		Clear_Hide_Hud_And_Radar_Post_Launch()
	
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: Hide Hud And Radar no longer required: SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED() returned FALSE")
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Still hiding HUD, so check for safety timeout
	IF (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_sTriggerPostLaunchMP.plHideHudTimeout))
		// ...safety timeout has occurred
		Clear_Hide_Hud_And_Radar_Post_Launch()
	
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: Hide Hud And Radar no longer required: Safety Timeout Time has elapsed.")
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Still need to hide the HUD and the radar
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	// url:bugstar:2205394 - Players are able to open the Pause menu
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]:")
		NET_PRINT_TIME()
		NET_PRINT("Hide Hud And Radar still required.")
		NET_NL()
	#ENDIF
		
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Current Mission Description Functions
//		(13/9/13: Retains the current mission description while the mission is ongoing)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	If necessary, continue to request the loading of the mission description
PROC Maintain_Current_Mission_Description()

	IF NOT (g_sCurrentMissionDesc.isInUse)
		EXIT
	ENDIF
	
	IF NOT (g_sCurrentMissionDesc.isLoading)
		EXIT
	ENDIF
	
	IF (REQUEST_AND_LOAD_CACHED_DESCRIPTION(g_sCurrentMissionDesc.descHash, g_sCurrentMissionDesc.descLoadVars))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [Triggering]: ")
            NET_PRINT_TIME()
            NET_PRINT("Current Mission Description Retrieved.")
			NET_NL()
			NET_PRINT("      - ")
			NET_PRINT(UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(g_sCurrentMissionDesc.descHash, INVITE_MAX_DESCRIPTION_LENGTH))
			NET_NL()
        #ENDIF
		
		// ...description is now ready
		g_sCurrentMissionDesc.isLoading = FALSE
		
		EXIT
	ENDIF

	// ...description is not yet ready, so keep waiting
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [Triggering]: ")
        NET_PRINT_TIME()
        NET_PRINT("Retrieving Current Mission Description. Still Waiting.")
		NET_NL()
    #ENDIF
			
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Setup the loading of the mission description
PROC Setup_Current_Mission_Description()

	// Setup the control variables
	INT descHash = Get_Mission_Dec_Hash_For_FM_Cloud_Loaded_Activity(g_sTriggerMP.mtMissionData.mdID)
	IF (descHash = 0)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]:")
			NET_PRINT_TIME()
			NET_PRINT("Setup Current Mission Description. DescHash = 0, so ignoring.")
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	g_sCurrentMissionDesc.isInUse					= TRUE
	g_sCurrentMissionDesc.isLoading					= TRUE
	g_sCurrentMissionDesc.descHash					= descHash
	g_sCurrentMissionDesc.descLoadVars.nHashOld		= 0
	g_sCurrentMissionDesc.descLoadVars.iRequest		= 0
	g_sCurrentMissionDesc.descLoadVars.bSucess		= FALSE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]:")
		NET_PRINT_TIME()
		NET_PRINT("Setup Current Mission Description. DescHash: ")
		NET_PRINT_INT(g_sCurrentMissionDesc.descHash)
		NET_NL()
	#ENDIF
	
	// Do an immediate maintenance on it
	Maintain_Current_Mission_Description()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// Release the Current Mission Description when no longer neede
PROC Release_Current_Mission_Description()
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]:")
		NET_PRINT_TIME()
		NET_PRINT("Release Current Mission Description. DescHash: ")
		NET_PRINT_INT(g_sCurrentMissionDesc.descHash)
		NET_NL()
	#ENDIF

	IF NOT (g_sCurrentMissionDesc.isInUse)
		#IF IS_DEBUG_BUILD
			NET_PRINT("      - BUT: Description was not in use. Nothing to release.")
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	UGC_RELEASE_CACHED_DESCRIPTION(g_sCurrentMissionDesc.descHash)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      - Current Mission Description Released.")
		NET_NL()
	#ENDIF
	
	// Clear the variables
	g_sCurrentMissionDesc.isInUse					= FALSE
	g_sCurrentMissionDesc.isLoading					= FALSE
	g_sCurrentMissionDesc.descHash					= 0
	g_sCurrentMissionDesc.descLoadVars.nHashOld		= 0
	g_sCurrentMissionDesc.descLoadVars.iRequest		= 0
	g_sCurrentMissionDesc.descLoadVars.bSucess		= FALSE
	
ENDPROC




// ===========================================================================================================
//      The Main MP Communications Control Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Initialisation Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	A one-off Mission Triggering initialisation.
PROC Initialise_MP_Mission_Triggering()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: Initialise_MP_Mission_Triggering") NET_NL()
	#ENDIF

	BOOL beingInitialised = TRUE
	Clear_MP_Mission_Triggering_Data(beingInitialised)
	
	// Also clear out any Mission Triggering Overview variables - game information used when making mission triggering decisions
	Clear_This_Players_Mission_Overview_Variables()
	
	// Clear any extra mission triggering details
	Clear_Mission_Extra_Player_Details()

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Maintenance Helper Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Cancel an active PreLaunch Comms if it has been ongoing for too long
//
// RETURN VALUE:		BOOL			TRUE if the call was cancelled
FUNC BOOL Cancel_MP_PreMission_Comms_If_Timeout()

	// Should the Comms Timeout?
	IF NOT (Check_For_MP_Mission_Triggering_Comms_Timeout())
		RETURN FALSE
	ENDIF
	
	// The Comms has timed out
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: The MP PreMission Comms Timer Has Expired. Cancelling the Comms. Hanging up the phone.") NET_NL()
	#ENDIF
	
	HANG_UP_AND_PUT_AWAY_PHONE()
	
	// Broadcast that player failed to join the mission - before cleanup
	Broadcast_Reply_Player_Failed_To_Join_Mission(g_sTriggerMP.mtMissionData)
	
	// Cleanup
	Cleanup_Mission_Triggering()
	
	// Timer expired
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Cancel an active PreLaunch Comms if a preLaunch restriction now exists
//
// RETURN VALUE:		BOOL			TRUE if the call was cancelled
FUNC BOOL Cancel_MP_PreMission_Comms_If_PreLaunch_Restriction_Exists()

	// Check if a preLaunch restriction exists
	IF NOT (Check_For_PreLaunch_Restriction())
		RETURN FALSE
	ENDIF
	
	// PreLaunch restriction now exists
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: A preLaunch restriction now exists. Cancelling the Comms. Hanging up the phone.") NET_NL()
	#ENDIF
	
	HANG_UP_AND_PUT_AWAY_PHONE()
	
	// Broadcast that player failed to join the mission - before cleanup
	Broadcast_Reply_Player_Failed_To_Join_Mission(g_sTriggerMP.mtMissionData)
	
	// Cleanup
	Cleanup_Mission_Triggering()
	
	// PreLaunch restriction exists
	RETURN TRUE
			
ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Cloud Download Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	If the mission requires cloud loaded mission data that isn't yet available, download it
//
// REFERENCE PARAMS:		refDownloadedOk		TRUE if the download ended ok and the game can progress, FALSE if the game should quit trying to trigger the mission
// RETURN VALUE:			BOOL				TRUE if cloud-loaded mission data is required but hasn't yet downloaded, FALSE if data isn't required, or if it is already downloaded
FUNC BOOL Has_Cloud_Loaded_Mission_Data_Still_To_Be_Downloaded(BOOL &refDownloadedOk)

	// Set a return value that, by default, allows triggering to dontinue
	refDownloadedOk = TRUE

	MP_MISSION_ID_DATA	theMissionIdData	= g_sTriggerMP.mtMissionData.mdID

	// Does this mission require cloud-loaded mission data?
	IF NOT (Does_MP_Mission_Variation_Get_Data_From_The_Cloud(theMissionIdData.idMission, theMissionIdData.idVariation))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: Don't need to download cloud mission data. The mission doesn't get its data from the cloud.") NET_NL()
		#ENDIF
	
		refDownloadedOk = TRUE
		RETURN FALSE
	ENDIF
	
	// Safety Check - relaunch the script if it isn't launched (this should never be the case)
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("net_cloud_mission_loader")) = 0)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: net_cloud_mission_loader.sc is being relaunched.") NET_NL()
		#ENDIF
		
		REQUEST_SCRIPT("net_cloud_mission_loader")
    	IF (HAS_SCRIPT_LOADED("net_cloud_mission_loader"))
		    START_NEW_SCRIPT("net_cloud_mission_loader", FRIEND_STACK_SIZE)
		    SET_SCRIPT_AS_NO_LONGER_NEEDED("net_cloud_mission_loader")
		ELSE
			#IF IS_DEBUG_BUILD
        		NET_PRINT("       Still waiting for script to launch: net_cloud_mission_loader") NET_NL()
			#ENDIF
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	// It looks like the cloud loaded mission data function will return TRUE if the data is already downloaded, so just going to request it everytime
	// 	(The Clear Previous Data flag enforces a fresh download, leave it FALSE here so we know to only download if the mission wasn't already downloaded by the cloud)
	TEXT_LABEL_31	ignoreOfflineContentID	= ""
	BOOL			useOfflineContentID		= FALSE
	BOOL			clearPrevUGCData		= FALSE
	
	IF (Load_Cloud_Loaded_Mission_Data(theMissionIdData, ignoreOfflineContentID, useOfflineContentID, clearPrevUGCData))
		// ...it has now downloaded, so return FALSE (it the data is no longer waiting to be downloaded)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: The Cloud-Loaded Mission Data load has ended.") NET_NL()
		#ENDIF
	
		refDownloadedOk = Did_Cloud_Loaded_Mission_Data_Load_Successfully()
		
		#IF IS_DEBUG_BUILD
			IF NOT (refDownloadedOk)
				NET_PRINT("...KGM MP [Triggering]: BUT: the Cloud-Loaded Mission Data failed to download") NET_NL()
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Still waiting for it to download
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: Still waiting for the Cloud-Loaded Mission Data to be available.") NET_NL()
	#ENDIF
		
	RETURN TRUE

ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Existing Instances Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if any instances of the same mission exist
//
// RETURN VALUE:			BOOL		TRUE if instances exist, otherwise FALSE
//
// NOTES:	KGM 31/10/14: This is called from NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS, but I also now need to pre-check this from mission triggerer.
FUNC BOOL Do_Any_Instances_Of_This_Script_Still_Exist()

	TEXT_LABEL_31 theScriptName = GET_MP_MISSION_NAME(g_sTriggerMP.mtMissionData.mdID.idMission)

	IF (IS_STRING_NULL_OR_EMPTY(theScriptName))
		RETURN FALSE
	ENDIF

	IF NOT (DOES_SCRIPT_EXIST(theScriptName))
		RETURN FALSE
	ENDIF

	BOOL	localCheckOnly	= TRUE
	INT		oldInstanceLoop	= 0
	
	REPEAT MAX_NUM_MISSIONS_ALLOWED oldInstanceLoop
		IF (NETWORK_IS_SCRIPT_ACTIVE(theScriptName, oldInstanceLoop, localCheckOnly))
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Maintenance Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Try to Perform the Pre-Mission Communication.
PROC Maintain_MP_Mission_Triggering_State_Start_PreMission_Comms()

	// Perform PreLaunch Mission Triggering Restriction Checks
	IF (Check_For_PreLaunch_Restriction())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Pre-Launch Mission Triggering Restriction Check (during START_PREMISSION_COMMS) - FAILED") NET_NL()
		#ENDIF
		
		// Broadcast that player failed to join the mission - before cleanup
		Broadcast_Reply_Player_Failed_To_Join_Mission(g_sTriggerMP.mtMissionData)
			
		// ...cancel the request
		Clear_MP_Mission_Triggering_Data()
			
		EXIT
	ENDIF

	// Perform PreComms Mission Triggering Restriction Checks
	IF (Check_For_PreComms_Restriction())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Pre-Comms Mission Triggering Restriction Check - FAILED") NET_NL()
		#ENDIF
		
		// Broadcast that player failed to join the mission - before cleanup
		Broadcast_Reply_Player_Failed_To_Join_Mission(g_sTriggerMP.mtMissionData)
			
		// ...cancel the request
		Clear_MP_Mission_Triggering_Data()
			
		EXIT
	ENDIF

	// Delay the request if a condition exists that should delay, but not kill, the pre-launch comms
	IF (Check_For_PreLaunch_Comms_Delay_Condition())
		EXIT
	ENDIF
		
	// Set up the Pre-Launch MP Comms details
	enumCharacterList theCaller = INT_TO_ENUM(enumCharacterList, g_sTriggerMP.mtCharSheetAsInt)
	
	IF (Is_Using_Intro_Txtmsg())
		// Using a txtmsg - try to issue the intro text message
		INT commsModifiers = ALL_MP_COMMS_MODIFIERS_CLEAR
// KGM NOTE: 6/6/12 - Uncomment the line below to force the txtmsg onscreen when sent
//		SET_BIT(commsModifiers, MP_COMMS_MODIFIER_COMMS_FORCE_ONSCREEN)
		
		BOOL requireYesNoReply = FALSE
		
		// If the player isn't being forced onto the mission then require a YesNo reply
		IF NOT (Is_Player_Being_Forced_Onto_Mission())
			// ...not forced onto mission, so require a YesNo reply
			requireYesNoReply = TRUE
		ENDIF
		
		IF NOT (Request_MP_Comms_Txtmsg(theCaller, g_sTriggerMP.mtRoot, requireYesNoReply, commsModifiers))
			EXIT
		ENDIF
	ELSE
		// Using a phonecall or radio message
		structPedsForConversation conversationStruct
		ADD_PED_FOR_DIALOGUE(conversationStruct, ConvertSingleCharacter(g_sTriggerMP.mtSpeaker), NULL, g_sTriggerMP.mtCharacter)
		
		// Try to make the appropriate MP Comms
		IF (Is_Player_Being_Forced_Onto_Mission())
			// Force the player to join the mission
			IF NOT (Request_MP_Comms_Force_Onto_Mission(conversationStruct, theCaller, g_sTriggerMP.mtGroup, g_sTriggerMP.mtRoot))
				EXIT
			ENDIF
		ELSE
			// Give the player the offer of joining the mission
			IF NOT (Request_MP_Comms_Offer_Mission(conversationStruct, theCaller, g_sTriggerMP.mtGroup, g_sTriggerMP.mtRoot))
				EXIT
			ENDIF
		ENDIF
	ENDIF
		
	// The Mission Offer Communication is now playing
	#IF IS_DEBUG_BUILD
		NET_NL()
		NET_PRINT("...KGM MP [Triggering]: Maintain_MP_Mission_Triggering(): Mission Launch Communication is now playing.")
		IF (Is_Using_Intro_Txtmsg())
			NET_PRINT(" [USING TXTMSG]")
		ENDIF
		NET_NL()
	#ENDIF
	
	// When the call becomes active, allow the call to be on offer for the default time
	Set_MP_Mission_Triggering_Comms_Timeout()

	// Display Mission Details Overlay - but don't display it until after a pre-mission cutscene
	IF (Is_Mission_Details_Box_Required())
		IF NOT (Is_PreMission_Cutscene_Required())
			Switch_On_MP_Mission_Details_Overlay(g_sTriggerMP.mtMissionData)
		ENDIF
	ENDIF
	
	// Call is now playing, so move on to next stage
	Set_Mission_Triggering_State_To_Waiting_For_PreMission_Comms_To_End()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Waits for the phonecall to end, and continues if accepted or if player is being forced onto the mission
PROC Maintain_MP_Mission_Triggering_State_Waiting_For_PreMission_Comms_To_End()

	// Wait for the call to be over
	IF (Is_MP_Comms_Still_Playing())
		// Cancelled because of timeout?
		IF (Cancel_MP_PreMission_Comms_If_Timeout())
			EXIT
		ENDIF
		
		// Cancelled because a pre-launch restriction has become true?
		IF (Cancel_MP_PreMission_Comms_If_PreLaunch_Restriction_Exists())
			EXIT
		ENDIF
					
		// Comms still in progress
		EXIT
	ENDIF
		
	// Comms is over
	// Is the player being forced onto the mission?
	IF (Is_Player_Being_Forced_Onto_Mission())
		// ...player being forced onto the mission, so don't need to check for YES/NO from Comms system
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: Maintain_MP_Mission_Triggering_State_Waiting_For_PreMission_Comms_To_End(): Comms Ended. Player being forced onto mission after ")
			IF (Is_Using_Intro_Txtmsg())
				NET_PRINT("text message.")
			ELSE
				NET_PRINT("phonecall.")
			ENDIF
			NET_NL()
		#ENDIF
		
		// Move on to the 'Check if available' stage
		// KGM 31/8/12: This used to go straight to launch, but that can allow too many players on the mission if players are forced onto a non-team-based mission
		Set_Mission_Triggering_State_To_Check_Still_Available()
		
		EXIT
	ENDIF
		
	// The player isn't being forced onto the mission
	// Check if the Player accepted the offer to join the mission
	IF (Did_Player_Reply_Yes_To_MP_Comms())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: Maintain_MP_Mission_Triggering_State_Waiting_For_PreMission_Comms_To_End(): Call Ended. Player accepted offer to start mission.") NET_NL()
		#ENDIF
		
		// Is a pre-mission cutscene required?
		IF (Is_PreMission_Cutscene_Required())
			// ...cutscene is required, so launch it
			Set_Mission_Triggering_State_To_Launch_Cutscene()
			
			EXIT
		ENDIF
		
		// Launch the mission
		Set_Mission_Triggering_State_To_Check_Still_Available()
		
		EXIT
	ENDIF
	
	// The player must have rejected the offer to join the mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: Maintain_MP_Mission_Triggering_State_Waiting_For_PreMission_Comms_To_End(): Call Ended. Player rejected offer to start mission.") NET_NL()
	#ENDIF
	
	// ...broadcast that player failed to join the mission - before cleanup
	BOOL rejectedMission = TRUE
	Broadcast_Reply_Player_Failed_To_Join_Mission(g_sTriggerMP.mtMissionData, rejectedMission)
	
	// ...cleanup
	Cleanup_Mission_Triggering()
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Try to launch the cutscene
PROC Maintain_MP_Mission_Triggering_State_Launch_Cutscene()

	// KGM TEMP 31/8/12: Set the LEGACY 'mission launching' global that other parts of the game read and react to
	// In the big cleanup of the old mission launching routines this should be removed
	MPGlobals.g_IsMissionLaunching		= TRUE
	
	// KGM TEMP 31/8/12: Clear the LEGACY mission launching data variables so that the old MAINTAIN_MISSION_LAUNCH function doesn't intervene
	MP_MISSION_DATA emptyData
	MPGlobals.g_MyMissionToLaunchInfo	= emptyData

	// Cutscene launch attempt successful
	#IF IS_DEBUG_BUILD
		// Generate the cutscene mission data
		MP_MISSION_DATA cutsceneData = Gather_Cutscene_Launching_Data()

		NET_PRINT("...KGM MP [Triggering]: Maintain_MP_Mission_Triggering_State_Launch_Cutscene(): Cutscene launch request has been accepted - waiting for cutscene to be active.")
		NET_NL()
		NET_PRINT("           Mission: ")
		NET_PRINT(GET_MP_MISSION_NAME(cutsceneData.mdID.idMission))
		NET_NL()
		NET_PRINT("           InstanceID: ")
		NET_PRINT_INT(cutsceneData.iInstanceId)
		NET_NL()
		NET_PRINT("           Variation: ")
		NET_PRINT_INT(cutsceneData.mdID.idVariation)
		NET_NL()
	#ENDIF
	
	// Update the launch stage to be 'Launching Cutscene"
	Set_Mission_Triggering_State_To_Launching_Cutscene()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Launching the Cutscene - waiting for it to be active
PROC Maintain_MP_Mission_Triggering_State_Launching_Cutscene()

	// Generate the cutscene mission data
	MP_MISSION_DATA cutsceneData = Gather_Cutscene_Launching_Data()

	// Try to launch the mission
	IF NOT (NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(cutsceneData, TRUE))
		EXIT
	ENDIF
	
	// Cutscene has now launched
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: Maintain_MP_Mission_Triggering_State_Launching_Cutscene(): Cutscene has successfully launched.") NET_NL()
	#ENDIF
	
	// KGM TEMP 31/8/12: Maintain the LEGACY variables still used by parts of the game to get the active mission, etc
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].activeMission = cutsceneData
	
	// KGM TEMP 31/8/12: Cleanup the LEGACY globals since the mission is no longer launching
	MP_MISSION_DATA emptyData
	MPGlobals.g_MyMissionToLaunchInfo	= emptyData	
	MPGlobals.g_IsMissionLaunching		= FALSE
			
	// Make sure we clear the abandon flags so we don't instantly end the mission.				
	CLEAR_ABANDON_MP_MISSION_FLAG()

	// Move on to the 'check if available' stage
	Set_Mission_Triggering_State_To_Cutscene_Active()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	The Cutscene is now active - waiting for it to end
PROC Maintain_MP_Mission_Triggering_State_Cutscene_Active()

	// Is player still on an MP mission?
	IF (IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID(), FALSE))
		EXIT
	ENDIF
	
	// Cutscene must have ended
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: Maintain_MP_Mission_Triggering_State_Cutscene_Active(): Player is no longer on cutscene - cutscene must have ended.") NET_NL()
	#ENDIF
		
	// Move on to the 'check if available' stage
	Set_Mission_Triggering_State_To_Check_Still_Available()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Issues a broadcast to the server to make sure the mission is still available for this player
PROC Maintain_MP_Mission_Triggering_State_Confirm_Mission_Still_Available()

	// Perform PreLaunch Mission Triggering Restriction Checks
	IF (Check_For_PreLaunch_Restriction())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: Pre-Launch Mission Triggering Restriction Check (during CONFIRM MISSION STILL AVAILABLE) - FAILED") NET_NL()
		#ENDIF
		
		// Broadcast that player failed to join the mission - before cleanup
		Broadcast_Reply_Player_Failed_To_Join_Mission(g_sTriggerMP.mtMissionData)
		
		// Cleanup
		Cleanup_Mission_Triggering()
			
		EXIT
	ENDIF
	
	// KGM 11/2/13: Don't get confirmation if still teleporting
	IF (IS_PLAYER_TELEPORT_ACTIVE())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: TELEPORTING: Delay Sending Request To Check If Mission Still Available") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Before requesting confirmation, ensure aany previous instances of the mission have properly cleaned up in code
	IF (Do_Any_Instances_Of_This_Script_Still_Exist())
		PRINTLN(".KGM [Triggering]: EXISTING INSTANCE OF SCRIPT STILL EXISTS: Delay Sending Request To Check If Mission Still Available")
		
		EXIT
	ENDIF

	// KGM 11/2/13: For missions with cloud-loaded mission data, request the mission data to be loaded
	BOOL successfulDownload = TRUE
	IF (Has_Cloud_Loaded_Mission_Data_Still_To_Be_Downloaded(successfulDownload))
		EXIT
	ENDIF
	
	// The download has ended, but bomb out if the data failed to download successfully
	IF NOT (successfulDownload)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [Triggering]: Cloud Download Ended - FAILED TO DOWNLOAD MISSION DATA") NET_NL()
		#ENDIF
		
		// Broadcast that player failed to join the mission - before cleanup
		Broadcast_Reply_Player_Failed_To_Join_Mission(g_sTriggerMP.mtMissionData)
		
		// Cleanup
		Cleanup_Mission_Triggering()
			
		EXIT
	ENDIF
	
	// This is using the new Mission Control system, so request confirmation from the server
	// Broadcast a request for confirmation from the server before attempting to join
	Broadcast_Check_If_Still_Ok_To_Join_Mission(g_sTriggerMP.mtMissionData)
	
	// Await Confirmation
	Set_Mission_Triggering_State_To_Awaiting_Confirmation_From_Server()
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Waits for a confirmation message from server - either 'ok' or 'full', 'finished', etc.
PROC Maintain_MP_Mission_Triggering_State_Awaiting_Confirmation()

	// Perform PreLaunch Mission Triggering Restriction Checks
	IF (Check_For_PreLaunch_Restriction())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Pre-Launch Mission Triggering Restriction Check (during AWAITING CONFIRMATION FROM SERVER) - FAILED") NET_NL()
		#ENDIF
		
		// Broadcast that player failed to join the mission - before cleanup
		Broadcast_Reply_Player_Failed_To_Join_Mission(g_sTriggerMP.mtMissionData)
		
		// Cleanup
		Cleanup_Mission_Triggering()
			
		EXIT
	ENDIF

	IF NOT (Has_Mission_Been_Confirmed_As_Ok_To_Join())
		// Keep Waiting
		EXIT
	ENDIF
	
	// Confirmation has been received, launch the mission
	Set_Mission_Triggering_State_To_Launch_Mission()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Try to launch the mission
PROC Maintain_MP_Mission_Triggering_State_Launch_Mission()

	// Perform PreLaunch Mission Triggering Restriction Checks
	IF (Check_For_PreLaunch_Restriction())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Pre-Launch Mission Triggering Restriction Check (during LAUNCH MISSION) - FAILED") NET_NL()
		#ENDIF
		
		// Broadcast that player failed to join the mission - before cleanup
		Broadcast_Reply_Player_Failed_To_Join_Mission(g_sTriggerMP.mtMissionData)
		
		// Cleanup
		Cleanup_Mission_Triggering()
			
		EXIT
	ENDIF

	// KGM TEMP 31/8/12: Set the LEGACY 'mission launching' global that other parts of the game read and react to
	// In the big cleanup of the old mission launching routines this should be removed
	MPGlobals.g_IsMissionLaunching		= TRUE
	
	// KGM TEMP 31/8/12: Clear the LEGACY mission launching data variables so that the old MAINTAIN_MISSION_LAUNCH function doesn't intervene
	MP_MISSION_DATA emptyData
	MPGlobals.g_MyMissionToLaunchInfo	= emptyData

	// Mission launch attempt successful
	// Some debug output - needs to be more detailed
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: Maintain_MP_Mission_Triggering_State_Launch_Mission(): Mission launch request has been accepted - waiting for mission to be active.") NET_NL()
	#ENDIF
		
	// Extend the Mission Details Overlay now that the player has requested the mission to launch
	// NOTE: Switch the overlay on in case it wasn't on
	IF (Is_Mission_Details_Box_Required())
		Switch_On_MP_Mission_Details_Overlay(g_sTriggerMP.mtMissionData)
		Extend_MP_Mission_Details_Overlay_Display_Time_When_Joining_Mission()
	ENDIF
	
	// Update the launch stage to be 'Launching Mission"
	Set_Mission_Triggering_State_To_Launching_Mission()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Launching the Mission - waiting for it to be active
PROC Maintain_MP_Mission_Triggering_State_Launching_Mission()

	// Try to launch the mission
	IF NOT (NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(g_sTriggerMP.mtMissionData, TRUE))
		EXIT
	ENDIF
	
	// Mission has now launched
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: Maintain_MP_Mission_Triggering_State_Launching_Mission(): Mission has successfully launched.") NET_NL()
		Debug_Output_Stored_Mission_Triggering_Details(FALSE)
	#ENDIF
	
//	// Let the other players know that this player has launched a mission (this is used for ticker messages about players joining, etc)
//	BROADCAST_JOINED_MP_MISSION_EVENT(g_sTriggerMP.mtMissionData.mdID.idMission)
	
	// KGM TEMP 31/8/12: Maintain the LEGACY variables still used by parts of the game to get the active mission, etc
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].activeMission = g_sTriggerMP.mtMissionData
	
	// KGM TEMP 31/8/12: Cleanup the LEGACY globals since the mission is no longer launching
	MP_MISSION_DATA emptyData
	MPGlobals.g_MyMissionToLaunchInfo	= emptyData	
	MPGlobals.g_IsMissionLaunching		= FALSE
			
	// Make sure we clear the abandon flags so we don't instantly end the mission.				
	CLEAR_ABANDON_MP_MISSION_FLAG()
	
	// Any external routines that need called when a missions starts
	Call_Any_External_Mission_Routines_When_MP_Mission_Starts()
	
	// Broadcast that player joined the mission
	Broadcast_Reply_Player_Joined_Mission(g_sTriggerMP.mtMissionData)
	
	// Store any additional Player details about this mission
	Store_Mission_Extra_Player_Details()
	
	// Set up a 'hide hud and radar' period of time to cover a period during a mission launch where these aren't getting suppressed for a few frames
	Setup_Hide_Hud_And_Radar_Post_Launch()
	
	// Setup the loading of the current mission description - retain it throughout the mission - this is mainly so it is available for the 'current mission' joblist screen
	Setup_Current_Mission_Description()

	Set_Mission_Triggering_State_To_Mission_Active()
	
	// Send a net debug log message - joined mission
	#IF IS_DEBUG_BUILD
		Net_Log_System("Joined Mission", g_sTriggerMP.mtMissionData.mdID.idMission, g_sTriggerMP.mtMissionData.iInstanceId)
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	The Mission is now active - waiting for it to end
PROC Maintain_MP_Mission_Triggering_State_Mission_Active()

	// Maintain the Hide Hud and Radar fucntionality for a short period post-launch
	Maintain_Hide_Hud_And_Radar_Post_Launch()
	
	// Maintain the loading of the Current Mission Description
	Maintain_Current_Mission_Description()

	// Is player still locally running the MP Mission script?
	// KGM MP: The MAINTAIN_MY_MISSION_TERMINATION_CHECKS() usually clears the .activeMission variable for cops and crooks, but for Freemode it'll be up to this routine to clear it
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_MP_MISSION_NAME_HASH(g_sTriggerMP.mtMissionData.mdID.idMission)) > 0)
		EXIT
	ENDIF
	
	IF (IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID(), FALSE))
		BOOL localCheckOnly = TRUE
		IF (NETWORK_IS_SCRIPT_ACTIVE(GET_MP_MISSION_NAME(g_sTriggerMP.mtMissionData.mdID.idMission), g_sTriggerMP.mtMissionData.iInstanceId, localCheckOnly))
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [Triggering]: Maintain_MP_Mission_Triggering_State_Mission_Active(): Mission Ending - delaying while script is still locally active.") NET_NL()
			#ENDIF
			
			EXIT
		ENDIF
	ENDIF
	
	// Mission must have ended
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: Maintain_MP_Mission_Triggering_State_Mission_Active(): Mission Has Ended.") NET_NL()
	#ENDIF
	
	// Copy the 'Terminate Mission' functionality from MAINTAIN_MY_MISSION_TERMINATION_CHECKS() - this will be duplicate calls for CnC so will be ignored, but required here for Freemode
	CLEAR_PLAYER_MP_MISSION_DATA()
	SET_RESPAWN_DELAY(0)
	CLEAR_ABANDON_MP_MISSION_FLAG()
	
	// Release the current mission description
	Release_Current_Mission_Description()
	
	// Clean up any mission-specific information that needs reset when a mission ends
	Call_Any_External_Mission_Routines_When_MP_Mission_Ends()
	
	// Broadcast that player ended the mission - before cleanup
	Broadcast_Reply_Player_Ended_Mission(g_sTriggerMP.mtMissionData)
	
	// Clear the mission extra player details
	Clear_Mission_Extra_Player_Details()
		
	// Send a net debug log message - left mission
	#IF IS_DEBUG_BUILD
		Net_Log_System("Ended Mission", g_sTriggerMP.mtMissionData.mdID.idMission, g_sTriggerMP.mtMissionData.iInstanceId)
	#ENDIF
			
	// Cleanup
	Cleanup_Mission_Triggering()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Performs any regular pre-maintenance checks when a Mission Triggering request is being carried out
PROC Maintain_MP_Mission_Triggering_PreMaintenance_Checks()

	// Check if Mission Control has broadcast 'Mission Finished'
	IF (Has_MP_Mission_Request_Finished(g_sTriggerMP.mtMissionData.mdUniqueID))
		Set_MT_Bitflag_As_Mission_Finished()
	ENDIF
	
	// Check if Mission Control has broadcast 'Mission Failed'
	IF (Has_MP_Mission_Request_Failed(g_sTriggerMP.mtMissionData.mdUniqueID))
		Set_MT_Bitflag_As_Mission_Failed()
	ENDIF
	
	// Check if Mission Control has broadcast 'Mission Full'
	IF (Has_MP_Mission_Request_Just_Broadcast_Mission_Full(g_sTriggerMP.mtMissionData.mdUniqueID))
		Set_MT_Bitflag_As_Mission_Full()
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Performs any regular mission triggering updates
PROC Maintain_MP_Mission_Triggering()

	// Perform any maintenance functions that must process each frame
	Maintain_Temporary_Mission_Block()
	
	// Anything to do?
	IF NOT (Is_Mission_Triggering_Request_In_Progress())
		EXIT
	ENDIF
	
	// Perform any regular checks needed when a Mission Triggering request is being carried out
	Maintain_MP_Mission_Triggering_PreMaintenance_Checks()
	
	// Mission Triggering Stages
	SWITCH (g_sTriggerMP.mtState)
		// Try to start the pre-launch Comms to the player
		CASE MP_TRIGGER_START_PREMISSION_COMMS
			Maintain_MP_Mission_Triggering_State_Start_PreMission_Comms()
			EXIT
			
		// Waiting for the preMission Comms to end
		CASE MP_TRIGGER_WAITING_FOR_PREMISSION_COMMS_TO_END
			Maintain_MP_Mission_Triggering_State_Waiting_For_PreMission_Comms_To_End()
			EXIT
			
		// Launch the cutscene
		CASE MP_TRIGGER_LAUNCH_CUTSCENE
			Maintain_MP_Mission_Triggering_State_Launch_Cutscene()
			EXIT
			
		// Waiting for the cutscene to launch
		CASE MP_TRIGGER_LAUNCHING_CUTSCENE
			Maintain_MP_Mission_Triggering_State_Launching_Cutscene()
			EXIT
			
		// Player is on cutscene - waiting for it to end
		CASE MP_TRIGGER_CUTSCENE_ACTIVE
			Maintain_MP_Mission_Triggering_State_Cutscene_Active()
			EXIT
			
		// Confirm with server that Mission is still OK to join
		CASE MP_TRIGGER_CHECK_STILL_AVAILABLE
			Maintain_MP_Mission_Triggering_State_Confirm_Mission_Still_Available()
			EXIT
			
		// Wait from confirmation with server that Mission is still OK to join
		CASE MP_TRIGGER_AWAITING_CONFIRMATION
			Maintain_MP_Mission_Triggering_State_Awaiting_Confirmation()
			EXIT
			
		// Launch the mission
		CASE MP_TRIGGER_LAUNCH_MISSION
			Maintain_MP_Mission_Triggering_State_Launch_Mission()
			EXIT
			
		// Waiting for the mission to launch
		CASE MP_TRIGGER_LAUNCHING_MISSION
			Maintain_MP_Mission_Triggering_State_Launching_Mission()
			EXIT
			
		// Player is on mission - waiting for it to end
		CASE MP_TRIGGER_MISSION_ACTIVE
			Maintain_MP_Mission_Triggering_State_Mission_Active()
			EXIT
			
		// There is no current mission triggering request
		CASE NO_CURRENT_MP_TRIGGER_REQUEST
			SCRIPT_ASSERT("Maintain_MP_Mission_Triggering: Reached NO_CURRENT_MP_TRIGGER_REQUEST, but this should have been checked before entering the SWITCH. Tell Keith.")
			EXIT
	ENDSWITCH
	
	// Should hopefully never reach here - but just in case
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: Maintain_MP_Mission_Triggering(): ERROR - Unknown Mission Triggering State.")
	#ENDIF

	// Cleanup
	Cleanup_Mission_Triggering()
			
	SCRIPT_ASSERT("Maintain_MP_Mission_Triggering(): Unknown Mission Triggering STATE. New STATE must need added to SWITCH statement. Clearing Data. Tell Keith.")

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Terminate Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	A Mission Triggering termination.
PROC Terminate_MP_Mission_Triggering()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Triggering]: Terminate_MP_Mission_Triggering") NET_NL()
	#ENDIF
	
	// Release the current mission description if there is one
	Release_Current_Mission_Description()

ENDPROC




