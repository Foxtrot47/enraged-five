USING "rage_builtins.sch"
USING "globals.sch"

USING "net_prints.sch"
USING "net_mission_control_broadcasts.sch"

// KGM 31/10/11: This should be extracted into a mode-specific header
USING "net_mission_trigger_overview.sch"
USING "net_mission_control_info.sch"
USING "Net_Entity_Controller.sch"

#IF IS_DEBUG_BUILD
	USING "net_debug.sch"
	USING "net_mission_control_debug.sch"
#ENDIF




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Mission_Control.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   SERVER FUNCTIONALITY controls the launching of missions and the allocation of players on missions.
//		NOTES			:	It's now possible for a mission to be played where all players are individuals (rather than in teams).
//								If the mission is not team-based then any player (whether in a team or not) can join it.
//								If the mission is team-based then only players in teams can join it.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************





// ===========================================================================================================
//      CONSTS and Variables and Structs
// ===========================================================================================================

// Indicates an empty array position
CONST_INT		NO_FREE_MISSION_REQUEST_SLOT		-1
CONST_INT		FAILED_TO_FIND_MISSION				-1


// -----------------------------------------------------------------------------------------------------------

// Debug Bitfield of 'Unavailable Player Reasons'
CONST_INT	BITS_UNAVAILABLE_REASON_SPECTATOR		0


// -----------------------------------------------------------------------------------------------------------

// Struct containing bitfields of player data used for decision making
STRUCT m_structPlayerAndTeamInfoMC
	// Players
	INT		bitsBusyPlayers				= ALL_MISSION_CONTROL_BITS_CLEAR		// BITS: Players actively on missions, or reserved for missions
	INT		bitsUnavailablePlayers		= ALL_MISSION_CONTROL_BITS_CLEAR		// BITS: Players unavailable for missions because of an external condition (Bad Cop, etc)
	INT		bitsAllowIfRequiredPlayers	= ALL_MISSION_CONTROL_BITS_CLEAR		// BITS: Players unavailable unless specifically required for a mission (ie: those doing pre-game tutorials)
	INT		bitsFreePlayers				= ALL_MISSION_CONTROL_BITS_CLEAR		// BITS: Players free for missions
	INT		numPlayersFree			= 0											// Number of Players free for missions
	// Teams
//	INT		bitsFreeTeamMembers[MAX_NUM_TEAMS]									// BITS: Array of Players Per Team free for missions
//	INT		numTeamMembersFree[MAX_NUM_TEAMS]									// Number of players per team free for missions
//	INT		bitsAllTeamMembers[MAX_NUM_TEAMS]									// BITS: Array of all players per team
//	INT		numTeamMembers[MAX_NUM_TEAMS]										// Number of players per team
	// Debug Only
	#IF IS_DEBUG_BUILD
		INT		bitsUnavailableReasons[NUM_NETWORK_PLAYERS]						// This is used to output additional debug information in the console log about why a player is unavailable
	#ENDIF
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Struct used when working out the closest players to a mission location
STRUCT m_structClosestPlayers
	INT		playerIndexAsInt		// The player Index for this player as an INT
	INT		requiredSpaces			// This is used to indicate the number of players that need to join the mission together (ie: Cop + Partner = 2): the partner should appear next in the array
	FLOAT	theDistance				// The distance from the mission location for the player in this location
	BOOL	reservedByPlayer		// TRUE if this is a 'RESERVED_BY_PLAYER' player, otherwise FALSE
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Struct used for calcualtions based on player locations when generating a fair spawn location
STRUCT m_structFSSpawn
	INT		numPlayers		= 0						// The number of players in this teadm involved in the calculation
	FLOAT	distFromOffset	= 0.0					// The sum of player distances from the current test set of offset coords
	FLOAT	avgDistance		= 0.0					// The average distance per player from the current test set of offset coords
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Struct used per-team when generating a fair spawn location
STRUCT m_structFSTeamSpawn
	BOOL				useTeam			= FALSE		// TRUE if this team should form part of the calculation
	VECTOR				teamSafehouse				// The position of the team safehouse used when generating an exclusion zone
	m_structFSSpawn		spawnInfo					// The average distance per team player from the current test set of offset coords
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Struct holding active mission details used during Dynamic Allocation
STRUCT structDynamicAllocationData
	INT		arrayPos							// Mission Request array position of mission that can still take more players
//	INT		numActiveFromTeam[MAX_NUM_TEAMS]	// Number of team members already on this mission
//	INT		numNeededFromTeam[MAX_NUM_TEAMS]	// Number of team members that can still be added to this mission
//	INT		pcActiveFromTeam[MAX_NUM_TEAMS]		// Percentage of how full this mission is with this team's members (100% means no more players are needed)
ENDSTRUCT




// ===========================================================================================================
//      Player And Team Checking And Debug Output Routines
// ===========================================================================================================

#IF IS_DEBUG_BUILD
// PURPOSE: Output all the reasons why a player is unavailable for mission triggering
//
// INPUT PARAMS:		paramPlayerAsInt		PlayerIndex as Int
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
PROC Debug_Output_Player_Unavailable_Reasons(m_structPlayerAndTeamInfoMC &paramInfo, INT paramPlayerAsInt)

	INT bitsReasons = paramInfo.bitsUnavailableReasons[paramPlayerAsInt]
	
	// Spectator?
	IF (IS_BIT_SET(bitsReasons, BITS_UNAVAILABLE_REASON_SPECTATOR))
		NET_PRINT("  [SPECTATOR]")
		CLEAR_BIT(bitsReasons, BITS_UNAVAILABLE_REASON_SPECTATOR)
	ENDIF
	
	// Safety Check
	IF NOT (bitsReasons = ALL_MISSION_CONTROL_BITS_CLEAR)
		NET_PRINT("  [UNKNOWN - ADD NEW REASON DESCRIPTION]")
	ENDIF

ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
// PURPOSE: Output all the Player and Team details to the console log
//
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
PROC Debug_Output_Player_And_Team_Details(m_structPlayerAndTeamInfoMC &paramInfo)

	INT tempLoop = 0
	PLAYER_INDEX thePlayer
		
	// Busy Players
	NET_PRINT("...KGM TEST: Current Player And Team Data Structure Contents") NET_NL()
	NET_PRINT("   BUSY PLAYERS")
	IF (paramInfo.bitsBusyPlayers = 0)
		NET_PRINT(": None")
	ENDIF
	NET_NL()
	Debug_Output_Player_Names_From_Bitfield(paramInfo.bitsBusyPlayers)

	// Unavailable Players and Reasons for being Unavailable
	NET_PRINT("   UNAVAILABLE PLAYERS")
	IF (paramInfo.bitsUnavailablePlayers = 0)
		NET_PRINT(": None")
		NET_NL()
	ELSE
		NET_NL()
		
		// Outputting this manually instead of with the standard function so that I can tag on reasons
		REPEAT NUM_NETWORK_PLAYERS tempLoop
			IF (IS_BIT_SET(paramInfo.bitsUnavailablePlayers, tempLoop))
				thePlayer = INT_TO_PLAYERINDEX(tempLoop)
				
				Debug_Output_Player_Name_And_Team(thePlayer, tempLoop)
				Debug_Output_Player_Unavailable_Reasons(paramInfo, tempLoop)
				NET_NL()
			ENDIF
		ENDREPEAT
	ENDIF
//	Debug_Output_Player_Names_From_Bitfield(paramInfo.bitsUnavailablePlayers)
	
	// Available If Required Players
	NET_PRINT("   AVAILABLE IF REQUIRED PLAYERS")
	IF (paramInfo.bitsAllowIfRequiredPlayers = 0)
		NET_PRINT(": None")
	ENDIF
	NET_NL()
	Debug_Output_Player_Names_From_Bitfield(paramInfo.bitsAllowIfRequiredPlayers)
	
	// Team Members
	BOOL areAnyPlayersInTeams = FALSE
	NET_PRINT("   TEAM MEMBERS") NET_NL()
//	REPEAT MAX_NUM_TEAMS tempLoop
//		IF (paramInfo.numTeamMembers[tempLoop] > 0)
//			NET_PRINT("     ") NET_PRINT(Convert_Team_To_String(tempLoop)) NET_NL()
//			NET_PRINT("        Team Members: ") NET_PRINT_INT(paramInfo.numTeamMembers[tempLoop]) NET_NL()
//			Debug_Output_Player_Names_From_Bitfield(paramInfo.bitsAllTeamMembers[tempLoop])
//			NET_PRINT("        Free Team Members: ") NET_PRINT_INT(paramInfo.numTeamMembersFree[tempLoop]) NET_NL()
//			Debug_Output_Player_Names_From_Bitfield(paramInfo.bitsFreeTeamMembers[tempLoop])
//			areAnyPlayersInTeams = TRUE
//		ENDIF
//	ENDREPEAT
	IF NOT (areAnyPlayersInTeams)
		NET_PRINT("           No Players In Teams") NET_NL()
	ENDIF
	
	// Free Players
	NET_PRINT("   FREE PLAYERS FOR MISSIONS: ") NET_PRINT_INT(paramInfo.numPlayersFree) NET_NL()
	Debug_Output_Player_Names_From_Bitfield(paramInfo.bitsFreePlayers)

ENDPROC
#ENDIF




// ===========================================================================================================
//      Mission Control Global Player Variable Check, Set, and Clear Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Clear All Mission Player Data Routine
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear all the MP Mission Player data
PROC Clear_All_MP_Mission_Player_Data()

	INT tempLoop = 0
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		GlobalServerBD_BlockB.missionPlayerData[tempLoop].mcpState	= MP_PLAYER_STATE_NONE
		GlobalServerBD_BlockB.missionPlayerData[tempLoop].mcpSlot	= MPMCP_NO_MISSION_SLOT
	ENDREPEAT

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Set and Clear Individual Player Data
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Player Data to a specified player state
//
// INPUT PARAMS:		paramPlayerAsInt		Player ID as an INT
//						paramStatus				Player Status
//						paramSlot				Mission Slot ID
//
// NOTES:	Player data should be clear prior to being set
PROC Set_Mission_Control_Player_State(INT paramPlayerAsInt, g_eMPMissionPlayerStatus paramStatus, INT paramSlot)

	#IF IS_DEBUG_BUILD
		g_eMPMissionPlayerStatus existingStatus = GlobalServerBD_BlockB.missionPlayerData[paramPlayerAsInt].mcpState
		IF NOT (existingStatus = MP_PLAYER_STATE_NONE)
			IF NOT (existingStatus = paramStatus)
				NET_PRINT("...KGM MP [MControl]: Set_Mission_Control_Player_State() - setting a player to a new state without it being cleared first") NET_NL()
				NET_PRINT("           Existing Status: ") NET_PRINT(Convert_Player_Status_To_String(GlobalServerBD_BlockB.missionPlayerData[paramPlayerAsInt].mcpState)) NET_NL()
				NET_PRINT("           Required Status: ") NET_PRINT(Convert_Player_Status_To_String(paramStatus)) NET_NL()
				SCRIPT_ASSERT("Set_Mission_Control_Player_State(): Player Mission Control Data State is setting the data without first clearing it - this could be dodgy. See Console Log. Tell Keith.")
			ENDIF
		ENDIF
		
		IF (paramStatus = MP_PLAYER_STATE_NONE)
			NET_PRINT("...KGM MP [MControl]: Set_Mission_Control_Player_State() - setting a player to a new state of NONE - use the 'clear' function") NET_NL()
			SCRIPT_ASSERT("Set_Mission_Control_Player_State(): Player Mission Control Data State is setting the data to NONE - use the clear function. Tell Keith.")
		ENDIF
	#ENDIF
	
	GlobalServerBD_BlockB.missionPlayerData[paramPlayerAsInt].mcpState = paramStatus
	GlobalServerBD_BlockB.missionPlayerData[paramPlayerAsInt].mcpSlot	= paramSlot

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Player Data to a specified player state
//
// INPUT PARAMS:		paramPlayerAsInt		Player ID as an INT
PROC Clear_Mission_Control_Player_State(INT paramPlayerAsInt)
	
	GlobalServerBD_BlockB.missionPlayerData[paramPlayerAsInt].mcpState = MP_PLAYER_STATE_NONE
	GlobalServerBD_BlockB.missionPlayerData[paramPlayerAsInt].mcpSlot	= MPMCP_NO_MISSION_SLOT

ENDPROC




// ===========================================================================================================
//      Mission Control Global Control Variable Check, Set, and Clear Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Mission Request 'Reserved Players' Access Functions
//		NOTE: Reserved Players are all players allocated to a mission, but not yet offered the mission
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Reserved Players bitfield for this Mission Request slot
//
// INPUT PARAMS:		paramArrayPos			The Mission request Slot
// RETURN VALUE:		INT						The 'all reserved players' bitfield for this mission
FUNC INT Get_MP_Mission_Request_Reserved_Players(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersAll)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if there are any players Reserved for this specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
// RETURN VALUE:		BOOL					TRUE if there are any players reserved this mission
//
// NOTES:	Assumes the mission slot isn't empty
FUNC BOOL Are_Any_Players_Reserved_For_This_MP_Mission_Request_Slot(INT paramArrayPos)

	IF (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersAll = ALL_MISSION_CONTROL_BITS_CLEAR)
		RETURN FALSE
	ENDIF
	
	// There are reserved players
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if this player is Reserved for this specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
// RETURN VALUE:		BOOL					TRUE if the player is reserved for this mission
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
FUNC BOOL Is_Player_Reserved_For_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersAll, playerIdAsInt))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the Player on the Reserved Players bitfield
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
//						paramPlayerAsInt		PlayerIndex as Int of Reserved Player
//
// NOTES:	PRIVATE FUNCTION - Should only be called by the subset reservation routines
//			Assumes playerIndex has been checked for validity
PROC PRIVATE_Set_Player_As_Reserved_On_MP_Mission_Request_Slot(INT paramArrayPos, INT paramPlayerAsInt)

	// We should be clearing the old Reserved State before setting the new Reserved State
	#IF IS_DEBUG_BUILD
		IF (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersAll, paramPlayerAsInt))
			SCRIPT_ASSERT("PRIVATE_Set_Player_As_Reserved_On_MP_Mission_Request_Slot() - POSSIBLE ERROR: Player Is Already Reserved For This Mission.")
		ENDIF
	#ENDIF

	// Update the existing bitfield
	SET_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersAll, paramPlayerAsInt)
	
	// Set the Player Data
	Set_Mission_Control_Player_State(paramPlayerAsInt, MP_PLAYER_STATE_RESERVED, paramArrayPos)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Remove Player from Reserved list for specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerAsInt		PlayerIndex as Int of Reserved Player
//
// NOTES:	PRIVATE FUNCTION - Should only be called by the subset reservation routines
//			Assumes the mission slot isn't empty and that the player index is valid
PROC PRIVATE_Clear_Player_As_Reserved_On_MP_Mission_Request_Slot(INT paramArrayPos, INT paramPlayerAsInt)

	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersAll, paramPlayerAsInt)
	
	// Clear the Player Data
	Clear_Mission_Control_Player_State(paramPlayerAsInt)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Mission Request 'Reserved_By_Player' Access Functions
//		NOTE: Reserved_By_Player are all players that have actively chosen to participate in the mission
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Reserved_By_Player bitfield for this Mission Request slot
//
// INPUT PARAMS:		paramArrayPos			The Mission request Slot
// RETURN VALUE:		INT						The Reserved_By_Player subset bitfield for this mission
FUNC INT Get_MP_Mission_Request_Reserved_By_Player_Players(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByPlayer)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if there are any players Reserved_By_Player for this specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
// RETURN VALUE:		BOOL					TRUE if there are any players Reserved_By_Player for this mission
//
// NOTES:	Assumes the mission slot isn't empty
FUNC BOOL Are_Any_Players_Reserved_By_Player_For_This_MP_Mission_Request_Slot(INT paramArrayPos)

	IF (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByPlayer = ALL_MISSION_CONTROL_BITS_CLEAR)
		RETURN FALSE
	ENDIF
	
	// There are Reserved_By_Player players
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if this player is Reserved_By_Player for this specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
// RETURN VALUE:		BOOL					TRUE if the player is Reserved_By_Player for this mission
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
FUNC BOOL Is_Player_Reserved_By_Player_For_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByPlayer, playerIdAsInt))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the Player on the Reserved_By_Player subset bitfield and on the Reserved bitfield
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
//						paramPlayerIndex		PlayerIndex of Reserved_By_Player Player
//
// NOTES:	Assumes playerIndex has been checked for validity
PROC Set_Player_As_Reserved_By_Player_On_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerIndex)

	// Update the existing bitfield
	INT PlayerIndexAsInt = NATIVE_TO_INT(paramPlayerIndex)
	SET_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByPlayer, PlayerIndexAsInt)

	// Also set the Reserved bit
	PRIVATE_Set_Player_As_Reserved_On_MP_Mission_Request_Slot(paramArrayPos, PlayerIndexAsInt)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Remove Player from Reserved_By_Player subset bitfield for specific mission and from the Reserved bitfield
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
PROC Clear_Player_As_Reserved_By_Player_On_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByPlayer, playerIdAsInt)

	// Also clear the Reserved bit
	PRIVATE_Clear_Player_As_Reserved_On_MP_Mission_Request_Slot(paramArrayPos, playerIdAsInt)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if all players in the passed in bitfield are marked as Reserved for a specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
// RETURN PARAMS:		paramPlayerBits			A bitfield of players that can be updated if there are changes
// RETURN VALUE:		BOOL					TRUE if all listed players were Reserved, FALSE if not
//
// NOTES:	TRUE will be returned if there are more Reserved players than there are in the bitfield as long as all bitfield players are represented.
FUNC BOOL Ensure_All_Listed_Players_Are_Reserved_For_Mission(INT paramArrayPos, INT &paramPlayerBits)

	// If no player bits are set then return TRUE (it probably won't reach here if no bits are set anyway)
	IF (paramPlayerBits = ALL_MISSION_CONTROL_BITS_CLEAR)
		RETURN TRUE
	ENDIF
	
	// Best way is to AND the Reserved bitfield with the player bitfield parameter,
	//	if the result equals the player bitfield parameter then all are represented within the Reserved bitfield.
	INT reservedPlayers = Get_MP_Mission_Request_Reserved_Players(paramArrayPos)
	
	IF ((reservedPlayers & paramPlayerBits) = paramPlayerBits)
		// ...the playerBits are fully represented within the Reserved bitfield
		RETURN TRUE
	ENDIF
	
	// Some players aren't represented within the Reserved bitfield for the mission, so update the player list
	paramPlayerBits = (reservedPlayers & paramPlayerBits)
	RETURN FALSE

ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Mission Request 'Reserved_By_Leader' Access Functions
//		NOTE: Reserved_By_Leader are all players allocated to a mission because another player may need them
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Reserved_By_Leader bitfield for this Mission Request slot
//
// INPUT PARAMS:		paramArrayPos			The Mission request Slot
// RETURN VALUE:		INT						The Reserved_By_Leader subset bitfield for this mission
FUNC INT Get_MP_Mission_Request_Reserved_By_Leader_Players(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByLeader)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if there are any players Reserved_By_Leader for this specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
// RETURN VALUE:		BOOL					TRUE if there are any players Reserved_By_Leader for this mission
//
// NOTES:	Assumes the mission slot isn't empty
FUNC BOOL Are_Any_Players_Reserved_By_Leader_For_This_MP_Mission_Request_Slot(INT paramArrayPos)

	IF (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByLeader = ALL_MISSION_CONTROL_BITS_CLEAR)
		RETURN FALSE
	ENDIF
	
	// There are Reserved_By_Leader players
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if this player is Reserved_By_Leader for this specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
// RETURN VALUE:		BOOL					TRUE if the player is Reserved_By_Leader for this mission
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
FUNC BOOL Is_Player_Reserved_By_Leader_For_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByLeader, playerIdAsInt))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the Player on the Reserved_By_Leader subset bitfield and on the Reserved bitfield
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
//						paramPlayerIndex		PlayerIndex of Reserved_By_Leader Player
//
// NOTES:	Assumes playerIndex has been checked for validity
PROC Set_Player_As_Reserved_By_Leader_On_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerIndex)

	// Update the existing bitfield
	INT PlayerIndexAsInt = NATIVE_TO_INT(paramPlayerIndex)
	SET_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByLeader, PlayerIndexAsInt)

	// Also set the Reserved bit
	PRIVATE_Set_Player_As_Reserved_On_MP_Mission_Request_Slot(paramArrayPos, PlayerIndexAsInt)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Remove Player from Reserved_By_Leader subset bitfield for specific mission and from the Reserved bitfield
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
PROC Clear_Player_As_Reserved_By_Leader_On_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByLeader, playerIdAsInt)

	// Also clear the Reserved bit
	PRIVATE_Clear_Player_As_Reserved_On_MP_Mission_Request_Slot(paramArrayPos, playerIdAsInt)
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Mission Request 'Reserved_By_Player' and 'Reserved_By_Leader' Access Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Reserved_By_Player and Reserved_By_Leader bitfield for this Mission Request slot
//
// INPUT PARAMS:		paramArrayPos			The Mission request Slot
// RETURN VALUE:		INT						The Reserved_By_Player and Reserved_By_Leader subset bitfield for this mission
FUNC INT Get_MP_Mission_Request_Reserved_By_Player_Or_Leader_Players(INT paramArrayPos)

	INT playerReserved = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByPlayer
	INT leaderReserved = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByLeader
	
	// Merge the two bitfields
	playerReserved |= leaderReserved
	
	RETURN (playerReserved)
	
ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Mission Request 'Reserved_By_Game' Access Functions
//		NOTE: Reserved_By_Game are all players that the game has allocated to a mission
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Reserved_By_Game bitfield for this Mission Request slot
//
// INPUT PARAMS:		paramArrayPos			The Mission request Slot
// RETURN VALUE:		INT						The Reserved_By_Game subset bitfield for this mission
FUNC INT Get_MP_Mission_Request_Reserved_By_Game_Players(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByGame)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if there are any players Reserved_By_Game for this specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
// RETURN VALUE:		BOOL					TRUE if there are any players Reserved_By_Game for this mission
//
// NOTES:	Assumes the mission slot isn't empty
FUNC BOOL Are_Any_Players_Reserved_By_Game_For_This_MP_Mission_Request_Slot(INT paramArrayPos)

	IF (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByGame = ALL_MISSION_CONTROL_BITS_CLEAR)
		RETURN FALSE
	ENDIF
	
	// There are Reserved_By_Game players
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if this player is Reserved_By_Game for this specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
// RETURN VALUE:		BOOL					TRUE if the player is Reserved_By_Game for this mission
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
FUNC BOOL Is_Player_Reserved_By_Game_For_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByGame, playerIdAsInt))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the Player on the Reserved_By_Game subset bitfield and on the Reserved bitfield
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
//						paramPlayerIndex		PlayerIndex of Reserved_By_Game Player
//
// NOTES:	Assumes playerIndex has been checked for validity
PROC Set_Player_As_Reserved_By_Game_On_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerIndex)

	// Update the existing bitfield
	INT PlayerIndexAsInt = NATIVE_TO_INT(paramPlayerIndex)
	SET_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByGame, PlayerIndexAsInt)

	// Also set the Reserved bit
	PRIVATE_Set_Player_As_Reserved_On_MP_Mission_Request_Slot(paramArrayPos, PlayerIndexAsInt)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Remove Player from Reserved_By_Game subset bitfield for specific mission and from the Reserved bitfield
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
PROC Clear_Player_As_Reserved_By_Game_On_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByGame, playerIdAsInt)

	// Also clear the Reserved bit
	PRIVATE_Clear_Player_As_Reserved_On_MP_Mission_Request_Slot(paramArrayPos, playerIdAsInt)
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Mission Request General Reservation Access Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Remove PlayerAsInt as Reserved from all reserved bitfields
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerAsInt		The PlayerIndex as an INT of the player being removed
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
PROC Remove_PlayerAsInt_From_All_Reserved_Bitfields_On_MP_Mission_Request_Slot(INT paramArrayPos, INT paramPlayerAsInt)

	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByPlayer, paramPlayerAsInt)
	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByLeader, paramPlayerAsInt)
	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByGame, paramPlayerAsInt)
	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersAll, paramPlayerAsInt)
	
	// Clear the Player Data
	Clear_Mission_Control_Player_State(paramPlayerAsInt)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Remove Player as Reserved from all reserved bitfields
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being removed
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
PROC Remove_Player_From_All_Reserved_Bitfields_On_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerIndex)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerIndex)
	Remove_PlayerAsInt_From_All_Reserved_Bitfields_On_MP_Mission_Request_Slot(paramArrayPos, playerIdAsInt)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Mission Request 'Active Players' Access Functions
//		NOTE: Active Players are all players that are now actively on the mission
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Active Players bitfield for this Mission Request slot
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
// RETURN VALUE:		INT						The 'active players' bitfield for this mission
FUNC INT Get_MP_Mission_Request_Active_Players(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsActivePlayers)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if there are any players Active on this specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
// RETURN VALUE:		BOOL					TRUE if there are any players active on this mission
//
// NOTES:	Assumes the mission slot isn't empty
FUNC BOOL Are_Any_Players_Active_On_This_MP_Mission_Request_Slot(INT paramArrayPos)

	IF (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsActivePlayers = ALL_MISSION_CONTROL_BITS_CLEAR)
		RETURN FALSE
	ENDIF
	
	// There are active players
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if this player is Active on a specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
// RETURN VALUE:		BOOL					TRUE if the player is active on this mission
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
FUNC BOOL Is_Player_Active_On_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsActivePlayers, playerIdAsInt))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Add Player to Active list for specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
PROC Set_Player_As_Active_On_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	SET_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsActivePlayers, playerIdAsInt)
	
	// Set the Player Data
	Set_Mission_Control_Player_State(playerIdAsInt, MP_PLAYER_STATE_ACTIVE, paramArrayPos)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Remove Player from Active list for specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
PROC Clear_Player_As_Active_On_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsActivePlayers, playerIdAsInt)
	
	// Clear the Player Data
	Clear_Mission_Control_Player_State(playerIdAsInt)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Mission Request 'Confirmed Players' Access Functions
//		NOTE: Confirmed Players are all players that checked if it is still ok to join the mission
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Confirmed Players bitfield for this Mission Request slot
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
// RETURN VALUE:		INT						The 'confirmed players' bitfield for this mission
FUNC INT Get_MP_Mission_Request_Confirmed_Players(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsConfirmedPlayers)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if there are any players Confirmed for this specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
// RETURN VALUE:		BOOL					TRUE if there are any players confirmed for this mission
//
// NOTES:	Assumes the mission slot isn't empty
FUNC BOOL Are_Any_Players_Confirmed_For_This_MP_Mission_Request_Slot(INT paramArrayPos)

	IF (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsConfirmedPlayers = ALL_MISSION_CONTROL_BITS_CLEAR)
		RETURN FALSE
	ENDIF
	
	// There are confirmed players
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if this player is Confirmed for a specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
// RETURN VALUE:		BOOL					TRUE if the player is confirmed for this mission
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
FUNC BOOL Is_Player_Confirmed_For_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsConfirmedPlayers, playerIdAsInt))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Add Player to Confirmed list for specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
PROC Set_Player_As_Confirmed_For_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	SET_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsConfirmedPlayers, playerIdAsInt)
	
	// Set the Player Data (use 'Joining' to represent Confirmed)
	Set_Mission_Control_Player_State(playerIdAsInt, MP_PLAYER_STATE_JOINING, paramArrayPos)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Remove Player from Confirmed list for specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
PROC Clear_Player_As_Confirmed_For_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsConfirmedPlayers, playerIdAsInt)
	
	// Clear the Player Data
	Clear_Mission_Control_Player_State(playerIdAsInt)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Mission Request 'Joining Players' Access Functions
//		NOTE: Joining Players are all players that are on pre-mission activities (ie: phonecall, cutscene)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Joining Players bitfield for this Mission Request slot
//
// INPUT PARAMS:		paramArrayPos			The Mission request Slot
// RETURN VALUE:		INT						The 'joining players' bitfield for this mission
FUNC INT Get_MP_Mission_Request_Joining_Players(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsJoiningPlayers)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if there are any players Joining this specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
// RETURN VALUE:		BOOL					TRUE if there are any players joining this mission
//
// NOTES:	Assumes the mission slot isn't empty
FUNC BOOL Are_Any_Players_Joining_This_MP_Mission_Request_Slot(INT paramArrayPos)

	IF (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsJoiningPlayers = ALL_MISSION_CONTROL_BITS_CLEAR)
		RETURN FALSE
	ENDIF
	
	// There are joining players
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if this player is Joining a specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
// RETURN VALUE:		BOOL					TRUE if the player is joining this mission
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
FUNC BOOL Is_Player_Joining_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsJoiningPlayers, playerIdAsInt))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Add Player to Joining list for specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
PROC Set_Player_As_Joining_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	SET_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsJoiningPlayers, playerIdAsInt)
	
	// Set the Player Data
	Set_Mission_Control_Player_State(playerIdAsInt, MP_PLAYER_STATE_JOINING, paramArrayPos)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Remove Player from Joining list for specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
PROC Clear_Player_As_Joining_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsJoiningPlayers, playerIdAsInt)
	
	// Clear the Player Data
	Clear_Mission_Control_Player_State(playerIdAsInt)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Mission Request 'Excluded Players' Access Functions
//		NOTE: Excluded Players have been on the mission already so can never re-join it.
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Excluded Players bitfield for this Mission Request slot
//
// INPUT PARAMS:		paramArrayPos			The Mission request Slot
// RETURN VALUE:		INT						The 'excluded players' bitfield for this mission
FUNC INT Get_MP_Mission_Request_Excluded_Players(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsExcludedPlayers)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if there are any players Excluded from this specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
// RETURN VALUE:		BOOL					TRUE if there are any players excluded from this mission
//
// NOTES:	Assumes the mission slot isn't empty
FUNC BOOL Are_Any_Players_Excluded_From_This_MP_Mission_Request_Slot(INT paramArrayPos)

	IF (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsExcludedPlayers = ALL_MISSION_CONTROL_BITS_CLEAR)
		RETURN FALSE
	ENDIF
	
	// There are excluded players
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if this player is Excluded from a specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
// RETURN VALUE:		BOOL					TRUE if the player is excluded from this mission
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
FUNC BOOL Is_Player_Excluded_From_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsExcludedPlayers, playerIdAsInt))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Add Player to Excluded list for specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
PROC Set_Player_As_Excluded_From_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	SET_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsExcludedPlayers, playerIdAsInt)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Remove Player from Excluded list for specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
PROC Clear_Player_As_Excluded_From_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsExcludedPlayers, playerIdAsInt)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Mission Request 'Rejected Players' Access Functions
//		NOTE: Rejected Players have been offered the mission but actively turned it down.
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Rejected Players bitfield for this Mission Request slot
//
// INPUT PARAMS:		paramArrayPos			The Mission request Slot
// RETURN VALUE:		INT						The 'rejected players' bitfield for this mission
FUNC INT Get_MP_Mission_Request_Rejected_Players(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsRejectedPlayers)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if there are any players Rejected from this specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
// RETURN VALUE:		BOOL					TRUE if there are any players that have rejected this mission
//
// NOTES:	Assumes the mission slot isn't empty
FUNC BOOL Are_Any_Players_Rejected_From_This_MP_Mission_Request_Slot(INT paramArrayPos)

	IF (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsRejectedPlayers = ALL_MISSION_CONTROL_BITS_CLEAR)
		RETURN FALSE
	ENDIF
	
	// There are rejected players
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if this player is Rejected from a specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
// RETURN VALUE:		BOOL					TRUE if the player has rejected this mission
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
FUNC BOOL Is_Player_Rejected_From_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsRejectedPlayers, playerIdAsInt))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Add Player to Rejected list for specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
PROC Set_Player_As_Rejected_From_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	SET_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsRejectedPlayers, playerIdAsInt)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Remove Player from Rejected list for specific mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The PlayerIndex of the player being checked
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
PROC Clear_Player_As_Rejected_From_MP_Mission_Request_Slot(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	INT playerIdAsInt = NATIVE_TO_INT(paramPlayerID)
	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsRejectedPlayers, playerIdAsInt)

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Mission Request Options Access Functions - use pre-mission cutscene
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the mission slot 'Pre-Mission Cutscene' option
//
// INPUT PARAMS:		paramArrayPos			Array Position of data
PROC Store_MP_Mission_Request_Option_PreMission_Cutscene(INT paramArrayPos)

	SET_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsOptions, MPMR_OPTION_BITFLAG_PREMISSION_CUTSCENE)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Setting Mission Request Option 'PreMission Cutscene' for slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the mission slot 'Pre-Mission Cutscene' option
//
// INPUT PARAMS:		paramArrayPos			Array Position of data
PROC Clear_MP_Mission_Request_Option_PreMission_Cutscene(INT paramArrayPos)

	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsOptions, MPMR_OPTION_BITFLAG_PREMISSION_CUTSCENE)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Clearing Mission Request Option 'PreMission Cutscene' for slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this mission request has a pre-mission cutscene
//
// INPUT PARAMS:		paramArrayPos			Array Position for the slot to store the struct
// RETURN VALUE:		BOOL					TRUE if there is a pre-mission cutscene, otherwise FALSE
FUNC BOOL Does_MP_Mission_Request_Have_Cutscene(INT paramArrayPos)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsOptions, MPMR_OPTION_BITFLAG_PREMISSION_CUTSCENE))
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Request Options Access Functions - first gang joined mission
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the mission slot 'First Crook Gang Joining Mission' option
//
// INPUT PARAMS:		paramArrayPos			Array Position of data
PROC Set_MP_Mission_Request_Option_First_Crook_Gang_Joining(INT paramArrayPos)

	SET_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsOptions, MPMR_OPTION_BITFLAG_FIRST_CROOK_GANG_JOINING)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Setting Mission Request Option 'First Crook Gang Joining' for slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the mission slot 'First Crook Gang Joining Mission' option
//
// INPUT PARAMS:		paramArrayPos			Array Position of data
PROC Clear_MP_Mission_Request_Option_First_Crook_Gang_Joining(INT paramArrayPos)

	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsOptions, MPMR_OPTION_BITFLAG_FIRST_CROOK_GANG_JOINING)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Clearing Mission Request Option 'First Crook Gang Joining' for slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the first crook gang is already joining this mission
//
// INPUT PARAMS:		paramArrayPos			Array Position for the slot to store the struct
// RETURN VALUE:		BOOL					TRUE if the first crook gang is already joining this mission, otherwise FALSE
FUNC BOOL Is_First_Crook_Gang_Joining_MP_Mission_Request(INT paramArrayPos)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsOptions, MPMR_OPTION_BITFLAG_FIRST_CROOK_GANG_JOINING))
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Request Options Access Functions - players can join mission
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the mission slot 'Players Can Join Mission' option
//
// INPUT PARAMS:		paramArrayPos			Array Position of data
//
// NOTES:	Used for missions that aren't team-based
PROC Store_MP_Mission_Request_Option_Players_Can_Join_Mission(INT paramArrayPos)

	IF (Check_If_Players_Are_In_Teams_For_Mission(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idMission))
		#IF IS_DEBUG_BUILD
			NET_PRINT("      Ignoring Mission Request Option 'Players Can Join Mission' for slot: ")
			NET_PRINT_INT(paramArrayPos)
			NET_PRINT(" - This Is Intended Only To Be Used By Missions That Are Not Team-Based")
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF

	SET_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsOptions, MPMR_OPTION_BITFLAG_PLAYERS_CAN_JOIN_MISSION)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Setting Mission Request Option 'Players Can Join Mission' for slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the mission slot 'Players Can Join Mission' option
//
// INPUT PARAMS:		paramArrayPos			Array Position of data
//
// NOTES:	Used for missions that aren't team-based
PROC Clear_MP_Mission_Request_Option_Players_Can_Join_Mission(INT paramArrayPos)

	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsOptions, MPMR_OPTION_BITFLAG_PLAYERS_CAN_JOIN_MISSION)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Clearing Mission Request Option 'Players Can Join Mission' for slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this mission request still allows players to join the mission
//
// INPUT PARAMS:		paramArrayPos			Array Position for the slot to store the struct
// RETURN VALUE:		BOOL					TRUE if players can still join the mission, otherwise FALSE
//
// NOTES:	Used for missions that aren't team-based
FUNC BOOL Does_MP_Mission_Request_Allow_Players_To_Join_Mission(INT paramArrayPos)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsOptions, MPMR_OPTION_BITFLAG_PLAYERS_CAN_JOIN_MISSION))
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Request Options Access Functions - mission is closed to Walk-Ins (matchmaking closed)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the mission slot 'Closed To Walk-Ins' option
//
// INPUT PARAMS:		paramArrayPos			Array Position of data
//
// NOTES:	Used for avoid players being able to walk-in and join the mission
PROC Store_MP_Mission_Request_Option_Closed_To_WalkIns(INT paramArrayPos)
	
	IF (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsOptions, MPMR_OPTION_BITFLAG_CLOSED_FOR_WALK_INS))
		EXIT
	ENDIF

	SET_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsOptions, MPMR_OPTION_BITFLAG_CLOSED_FOR_WALK_INS)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Setting Mission Request Option 'Closed To Walk-Ins' for slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the mission slot 'Open To Walk-Ins' option
//
// INPUT PARAMS:		paramArrayPos			Array Position of data
//
// NOTES:	Used for allow players to walk-in and join the mission
PROC Store_MP_Mission_Request_Option_Open_To_WalkIns(INT paramArrayPos)
	
	IF NOT (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsOptions, MPMR_OPTION_BITFLAG_CLOSED_FOR_WALK_INS))
		EXIT
	ENDIF

	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsOptions, MPMR_OPTION_BITFLAG_CLOSED_FOR_WALK_INS)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Setting Mission Request Option 'Open To Walk-Ins' for slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this mission request is Closed To Walk-Ins
//
// INPUT PARAMS:		paramArrayPos			Array Position for the slot to store the struct
// RETURN VALUE:		BOOL					TRUE if the mission is closed to Walk-Ins, FALSE if open to walk-ins
FUNC BOOL Is_MP_Mission_Request_Mission_Closed_To_Walk_Ins(INT paramArrayPos)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsOptions, MPMR_OPTION_BITFLAG_CLOSED_FOR_WALK_INS))
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Request Debug Options Access Functions - allow launch without checking minimum player requirements
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the mission slot 'Allow One-Player Launch' option
//
// INPUT PARAMS:		paramArrayPos			Array Position of data
#IF IS_DEBUG_BUILD
PROC Store_MP_Mission_Request_Debug_Option_Allow_One_Player_Launch(INT paramArrayPos)

	SET_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsOptions, MPMR_OPTION_DEBUG_BITFLAG_ONE_PLAYER_LAUNCH)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Setting Mission Request Debug Option 'Allow One Player Launch' for slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_NL()
	#ENDIF
	
ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the mission slot 'Allow One-Player Launch' option
//
// INPUT PARAMS:		paramArrayPos			Array Position of data
#IF IS_DEBUG_BUILD
PROC Clear_MP_Mission_Request_Debug_Option_Allow_One_Player_Launch(INT paramArrayPos)

	CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsOptions, MPMR_OPTION_DEBUG_BITFLAG_ONE_PLAYER_LAUNCH)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Clearing Mission Request Debug Option 'Allow One Player Launch' for slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_NL()
	#ENDIF
	
ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this mission request can launch with only one player
//
// INPUT PARAMS:		paramArrayPos			Array Position for the slot to store the struct
// RETURN VALUE:		BOOL					TRUE if mission slot can launch with only one player, otherwise FALSE
#IF IS_DEBUG_BUILD
FUNC BOOL Does_MP_Mission_Request_Allow_One_Player_Launch_In_Debug(INT paramArrayPos)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsOptions, MPMR_OPTION_DEBUG_BITFLAG_ONE_PLAYER_LAUNCH))
ENDFUNC
#ENDIF




// -----------------------------------------------------------------------------------------------------------
//      Mission Request 'Requested Teams' Access Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the Team on the Requested Teams bitfield
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
//						paramTeam				Team ID
PROC Store_MP_Mission_Request_Requested_Team(INT paramArrayPos, INT paramTeam)

	// Invalid Teams means team is not required
	IF (paramTeam = TEAM_INVALID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Player Belongs To No Team for Mission Request Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Error Checking
	IF (paramTeam < 0)
		SCRIPT_ASSERT("Store_MP_Mission_Request_Requested_Team(): ERROR - Team ID is less than 0 and is not the TEAM_INVALID ID. Ignorable. Tell Keith.")
		EXIT
	ENDIF

//	IF (paramTeam >= MAX_NUM_TEAMS)
//		SCRIPT_ASSERT("Store_MP_Mission_Request_Requested_Team(): ERROR - Team ID is greater than or equal to MAX_NUM_TEAMS. Ignorable. Tell Keith.")
//		EXIT
//	ENDIF

	// Update the existing bitfield
	SET_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsRequestedTeams, paramTeam)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Adding Team to Requested Teams for Mission Request Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
		Debug_Output_Team_Names_From_Bitfield(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsRequestedTeams)
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the Requested Teams bitfield
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
// RETURN VALUE:		INT						Requested Teams bitfield
FUNC INT Get_MP_Mission_Request_Requested_Teams(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsRequestedTeams)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Requested Teams bitfield
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
PROC Clear_MP_Mission_Request_Requested_Teams_Bitfield(INT paramArrayPos)
	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsRequestedTeams = ALL_MISSION_CONTROL_BITS_CLEAR
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a specific team was requested for a specific mission
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
//						paramTeam				Team ID to be checked
// RETURN VALUE:		BOOL					TRUE if team was requested for mission, otherwise FALSE
FUNC BOOL Is_Team_Requested_For_Mission(INT paramArrayPos, INT paramTeam)

	IF (paramTeam = TEAM_INVALID)
		RETURN FALSE
	ENDIF

	INT requestedTeams = Get_MP_Mission_Request_Requested_Teams(paramArrayPos)
	RETURN (IS_BIT_SET(requestedTeams, paramTeam))

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Request 'Reserved Teams' Access Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the Team on the Specific Teams bitfield
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
//						paramTeam				Team ID
PROC Store_MP_Mission_Request_Reserved_Team(INT paramArrayPos, INT paramTeam)

	// Invalid Teams means team is not required
	IF (paramTeam = TEAM_INVALID)
		EXIT
	ENDIF
	
	// Error Checking
	IF (paramTeam < 0)
		SCRIPT_ASSERT("Store_MP_Mission_Request_Reserved_Team(): ERROR - Team ID is less than 0 and is not the TEAM_INVALID ID. Ignorable. Tell Keith.")
		EXIT
	ENDIF

//	IF (paramTeam >= MAX_NUM_TEAMS)
//		SCRIPT_ASSERT("Store_MP_Mission_Request_Reserved_Team(): ERROR - Team ID is greater than or equal to MAX_NUM_TEAMS. Ignorable. Tell Keith.")
//		EXIT
//	ENDIF

	// Update the existing bitfield
	SET_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedTeams, paramTeam)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Adding Team to Reserved Teams for Mission Request Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
		Debug_Output_Team_Names_From_Bitfield(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedTeams)
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the Reserved Teams bitfield
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
// RETURN VALUE:		INT						Reserved Teams bitfield
FUNC INT Get_MP_Mission_Request_Reserved_Teams(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedTeams)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Reserved Teams bitfield
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
PROC Clear_MP_Mission_Request_Reserved_Teams_Bitfield(INT paramArrayPos)
	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedTeams = ALL_MISSION_CONTROL_BITS_CLEAR
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a specific team is reserved for a specific mission
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
//						paramTeam				Team ID to be checked
// RETURN VALUE:		BOOL					TRUE if team is reserved for mission, otherwise FALSE
FUNC BOOL Is_Team_Reserved_For_Mission(INT paramArrayPos, INT paramTeam)

	INT reservedTeams = Get_MP_Mission_Request_Reserved_Teams(paramArrayPos)
	RETURN (IS_BIT_SET(reservedTeams, paramTeam))

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Request 'Joinable Teams' Access Functions
//		(During Initial Mission Details Selection the 'joinable teams' bitfield is used to temp store
//		the 'free teams for mission' bitfield that contains the pool of available teams for this mission
//		from which the 'reserved teams' will be selected).
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the Team on the Joinable Teams bitfield
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
//						paramTeam				Team ID
PROC Store_MP_Mission_Request_Joinable_Team(INT paramArrayPos, INT paramTeam)

	// Invalid Teams means team is not required
	IF (paramTeam = TEAM_INVALID)
		EXIT
	ENDIF
	
	// Error Checking
	IF (paramTeam < 0)
		SCRIPT_ASSERT("Store_MP_Mission_Request_Joinable_Team(): ERROR - Team ID is less than 0 and is not the TEAM_INVALID ID. Ignorable. Tell Keith.")
		EXIT
	ENDIF

//	IF (paramTeam >= MAX_NUM_TEAMS)
//		SCRIPT_ASSERT("Store_MP_Mission_Request_Joinable_Team(): ERROR - Team ID is greater than or equal to MAX_NUM_TEAMS. Ignorable. Tell Keith.")
//		EXIT
//	ENDIF

	// Update the existing bitfield
	SET_BIT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsJoinableTeams, paramTeam)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Adding Team to Joinable Teams for Mission Request Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
		Debug_Output_Team_Names_From_Bitfield(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsJoinableTeams)
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Replace the whole Joinable Teams bitfield
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
//						paramTeamsBitfield		Joinable Teams Bitfield
PROC Replace_MP_Mission_Request_Joinable_Teams_Bitfield(INT paramArrayPos, INT paramTeamsBitfield)

	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsJoinableTeams = paramTeamsBitfield
	
	#IF IS_DEBUG_BUILD
		IF (Check_If_Players_Are_In_Teams_For_Mission(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idMission))
			NET_PRINT("...KGM MP [MControl]: Replacing Joinable Teams Bitfield for Mission Request Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
			Debug_Output_Team_Names_From_Bitfield(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsJoinableTeams)
		ENDIF
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the Joinable Teams bitfield
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
// RETURN VALUE:		INT						Joinable Teams bitfield
FUNC INT Get_MP_Mission_Request_Joinable_Teams(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsJoinableTeams)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Joinable Teams bitfield
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
PROC Clear_MP_Mission_Request_Joinable_Teams_Bitfield(INT paramArrayPos)
	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsJoinableTeams = ALL_MISSION_CONTROL_BITS_CLEAR
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a specific team is joinable for a specific mission
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
//						paramTeam				Team ID to be checked
// RETURN VALUE:		BOOL					TRUE if team is joinable for mission, otherwise FALSE
FUNC BOOL Is_Team_Joinable_For_Mission(INT paramArrayPos, INT paramTeam)

	INT joinableTeams = Get_MP_Mission_Request_Joinable_Teams(paramArrayPos)
	RETURN (IS_BIT_SET(joinableTeams, paramTeam))

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Request Status Access Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the mission slot status as 'Reserved'
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
PROC Store_MP_Mission_Request_Status_As_Reserved(INT paramArrayPos)

	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrStatusID = MP_MISSION_STATE_RESERVED
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Setting Mission Request Status as 'RESERVED' for slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the mission slot status as 'Received'
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
PROC Store_MP_Mission_Request_Status_As_Received(INT paramArrayPos)

	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrStatusID = MP_MISSION_STATE_RECEIVED
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Setting Mission Request Status as 'RECEIVED' for slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the mission slot status as 'Offered'
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
PROC Store_MP_Mission_Request_Status_As_Offered(INT paramArrayPos)

	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrStatusID = MP_MISSION_STATE_OFFERED
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Setting Mission Request Status as 'OFFERED' for slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the mission slot status as 'Active'
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
PROC Store_MP_Mission_Request_Status_As_Active(INT paramArrayPos)

	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrStatusID = MP_MISSION_STATE_ACTIVE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Setting Mission Request Status as 'ACTIVE' for slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the mission slot status as 'Active, Add Secondary Players'
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
PROC Store_MP_Mission_Request_Status_As_Active_Add_Secondary_Players(INT paramArrayPos)

	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrStatusID = MP_MISSION_STATE_ACTIVE_SECONDARY
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Setting Mission Request Status as 'ACTIVE SECONDARY' for slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the mission slot status as 'Empty'
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
PROC Store_MP_Mission_Request_Status_As_Empty(INT paramArrayPos)

	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrStatusID = NO_MISSION_REQUEST_RECEIVED
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Setting Mission Request Status as 'EMPTY' for slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the Status for one slot
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be returned
// RETURN VALUE:		g_eMPMissionStatus		The status of this Mission Request Slot
FUNC g_eMPMissionStatus Get_MP_Mission_Request_Slot_Status(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrStatusID)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Slot Status is RESERVED
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
// RETURN VALUE:		BOOL					TRUE if the slot contains a 'Reserved' request, otherwise FALSE
FUNC BOOL Is_MP_Mission_Request_Slot_Status_Reserved(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrStatusID = MP_MISSION_STATE_RESERVED)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Slot Status is RECEIVED
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
// RETURN VALUE:		BOOL					TRUE if the slot contains a newly 'Received' request, otherwise FALSE
FUNC BOOL Is_MP_Mission_Request_Slot_Status_Received(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrStatusID = MP_MISSION_STATE_RECEIVED)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Slot Status is OFFERED
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
// RETURN VALUE:		BOOL					TRUE if the slot contains a newly 'Offered' request, otherwise FALSE
FUNC BOOL Is_MP_Mission_Request_Slot_Status_Offered(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrStatusID = MP_MISSION_STATE_OFFERED)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Slot Status is ACTIVE
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
// RETURN VALUE:		BOOL					TRUE if the slot contains a newly 'Active' request, otherwise FALSE
FUNC BOOL Is_MP_Mission_Request_Slot_Status_Active(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrStatusID = MP_MISSION_STATE_ACTIVE)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Slot Status is ACTIVE SECONDARY
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
// RETURN VALUE:		BOOL					TRUE if the slot contains a newly 'Active, Add Secondary Players' request, otherwise FALSE
FUNC BOOL Is_MP_Mission_Request_Slot_Status_Active_Add_Secondary_Players(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrStatusID = MP_MISSION_STATE_ACTIVE_SECONDARY)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Slot is EMPTY
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
// RETURN VALUE:		BOOL					TRUE if the slot is empty, otherwise FALSE
//
// NOTES:	KGM 25/4/12: Now forwarded to the more publically available function
FUNC BOOL Is_MP_Mission_Request_Slot_Empty(INT paramArrayPos)
	RETURN (Is_MP_Mission_Request_Slot_Available_For_New_Data(paramArrayPos))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the Slot Status ID
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
// RETURN VALUE:		g_eMPMissionStatus		The Slot Status ID
FUNC g_eMPMissionStatus Get_MP_Mission_Request_Slot_StatusID(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrStatusID)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check the Mission Request slots to see if any have active data
//
// RETURN VALUE:		BOOL			TRUE if all slots are empty, FALSE if any slot isn't empty
FUNC BOOL Are_All_MP_Mission_Request_Slots_Empty()

	INT tempLoop = 0
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		IF NOT (Is_MP_Mission_Request_Slot_Empty(temploop))
			// ...slot is not empty
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	// All slots are empty
	RETURN TRUE

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Request Source Access Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the Mission Request Source
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
//						paramSourceID			Mission Request Source ID
PROC Store_MP_Mission_Request_Source(INT paramArrayPos, g_eMPMissionSource paramSourceID)

	#IF IS_DEBUG_BUILD
		IF (paramSourceID = UNKNOWN_MISSION_SOURCE)
			SCRIPT_ASSERT("Store_MP_Mission_Request_Source() - Trying to store Source as UNKNOWN_MISSION_SOURCE. Debug Assert for Info. Ignorable. Tell Keith.")
		ENDIF
	#ENDIF

	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrSourceID = paramSourceID
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the Mission Request SourceID
//
// INPUT PARAMS:		paramArrayPos				Array Position of data to be checked
// RETURN VALUE:		g_eMPMissionSource			Mission Request Source ID
FUNC g_eMPMissionSource Get_MP_Mission_Request_Source(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrSourceID)
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Request Mission Type Access Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the Mission Request Mission Type
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
//						paramTypeID				Mission Request Mission Type ID
PROC Store_MP_Mission_Mission_Type(INT paramArrayPos, MP_MISSION_TYPE paramTypeID)

	#IF IS_DEBUG_BUILD
		IF (paramTypeID = UNKNOWN_MISSION_TYPE)
			NET_PRINT("...KGM MP [MControl]: Store_MP_Mission_Mission_Type() - Requested Mission Type is UNKNOWN_MISSION_TYPE") NET_NL()
			SCRIPT_ASSERT("Store_MP_Mission_Mission_Type() - Trying to store mission type as UNKNOWN_MISSION_TYPE. Debug Assert for Info. Ignorable. Tell Keith.")
		ENDIF
	#ENDIF

	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionTypeID = paramTypeID
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the Mission Request Mission Type
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
// RETURN VALUE:		MP_MISSION_TYPE			Mission Requests Mission Type ID
FUNC MP_MISSION_TYPE Get_MP_Mission_Mission_Type(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionTypeID)
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Request Requester Access Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the Mission Request Requester
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
//						paramPlayerID			Mission Request Requester
//						paramIsHost				TRUE if the request is from the player as a Host, otherwise from the player as an individual
//
// NOTES:	Assumes the playerID has already been checked for validity
PROC Store_MP_Mission_Requester(INT paramArrayPos, PLAYER_INDEX paramPlayerID, BOOL paramIsHost)

	// If the player is a host then the playerIndex is being stored, but it shouldn't be needed
	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrRequester = paramPlayerID

	// Store that the requester is a script host, if applicable
	IF (paramIsHost)
		GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrRequesterIsHost	= paramIsHost
		
		EXIT
	ENDIF
	
	// Requester is an individual player, so ensure player is marked as Reserved By Player for the mission
	Set_Player_As_Reserved_By_Player_On_MP_Mission_Request_Slot(paramArraypos, paramPlayerID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Adding Player as Reserved By Player for Mission Request Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
		NET_PRINT("           ")
		NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
		NET_NL()
	#ENDIF
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Mission Request Specific Tutorial SessionID Access Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the Specific Tutorial SessionID that players need to be in before joining this Mission Request
//
// INPUT PARAMS:		paramArrayPos				Array Position of data to be checked
//						paramTutorialSessionID		Mission Request Specific Tutorial Session ID
//
// NOTES:	KGM 23/1/13: Added for Dave's Freemode tutorials where the players are allocated tutorial sessions prior to requesting the tutorial race or deathmatch
PROC Store_MP_Mission_Request_Specific_Tutorial_SessionID(INT paramArrayPos, INT paramTutorialSessionID)

	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrTutorialSessionID = paramTutorialSessionID
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Storing Specific Tutorial Session ID for mission: ")
		IF (paramTutorialSessionID = TUTORIAL_SESSION_NOT_ACTIVE)
			NET_PRINT("Players don't need to be pre-allocated to a Specific Tutorial Session ID")
		ELSE
			NET_PRINT_INT(paramArrayPos)
		ENDIF
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the Specific Tutorial SessionID that players need to be in before joining this Mission Request
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
// RETURN VALUE:		INT						Mission Request Specific Tutorial Session ID
FUNC INT Get_MP_Mission_Request_Specific_Tutorial_SessionID(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrTutorialSessionID)
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Request Mission Data Access Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the contents of the Mission Data struct
//
// INPUT PARAMS:		paramArrayPos			Array Position for the slot to store the struct
//						paramMissionData		The Data to be copied
PROC Store_MP_Mission_Mission_Data(INT paramArrayPos, MP_MISSION_DATA paramMissionData)

	// Store the Data
	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData	= paramMissionData
	
	// Store the Mission Type
	Store_MP_Mission_Mission_Type(paramArrayPos, GET_MP_MISSION_TYPE(paramMissionData.mdID.idMission))
	
	// Output the contents of the data being copied
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Copying this Mission Data into Mission Request Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
		Debug_Output_Full_Contents_Of_MP_Mission_Data_To_Console_Log(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData)
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the contents of the MissionID Data struct
//
// INPUT PARAMS:		paramArrayPos			Array Position for the slot to store the struct
//						paramMissionIdData		The MissionID Data to be copied
PROC Store_MP_Mission_MissionID_Data(INT paramArrayPos, MP_MISSION_ID_DATA &paramMissionIdData)

	// Store the Data
	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID	= paramMissionIdData
	
	// Store the Mission Type
	Store_MP_Mission_Mission_Type(paramArrayPos, GET_MP_MISSION_TYPE(paramMissionIdData.idMission))
	
	// Output the contents of the data being copied
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Copying this Mission Id Data into Mission Request Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
		Debug_Output_Full_Contents_Of_MP_Mission_Data_To_Console_Log(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData)
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the Mission Data associated with this Mission Request Slot
//
// INPUT PARAMS:		paramArrayPos			Array Position for the slot to store the struct
// RETURN VALUE:		MP_MISSION_DATA			The Mission Data contained in this slot
//
// NOTES:	Assumes the slot contains valid data
FUNC MP_MISSION_DATA Get_MP_Mission_Request_Slot_Mission_Data(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the MissionID Data associated with this Mission Request Slot
//
// INPUT PARAMS:		paramArrayPos			Array Position for the slot to store the struct
// RETURN VALUE:		MP_MISSION_ID_DATA		The MissionID Data contained in this slot
//
// NOTES:	Assumes the slot contains valid data
FUNC MP_MISSION_ID_DATA Get_MP_Mission_Request_MissionID_Data(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the MP Mission as part of the Mission Data
//
// INPUT PARAMS:		paramArrayPos			Array Position for the slot to store the struct
//						paramMissionID			The chosen MissionID for this mission request
//
// NOTES:	This only needs done for a request for a mission of type once a mission has been randomly selected
PROC Store_MP_Mission_MissionID_On_MissionData(INT paramArrayPos, MP_MISSION paramMissionID)
	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idMission = paramMissionID
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the MP Mission stored as part of the Mission Data
//
// INPUT PARAMS:		paramArrayPos			Array Position for the slot to store the struct
// RETURN VALUE:		MP_MISSION				The MissionID stored in this array position
FUNC MP_MISSION Get_MP_Mission_Request_MissionID(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idMission)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the MP Mission stored as part of the Mission Data
//
// INPUT PARAMS:		paramArrayPos			Array Position for the slot to store the struct
// RETURN VALUE:		INT						The MissionVariation stored in this array position
FUNC INT Get_MP_Mission_Request_MissionVariation(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idVariation)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the MP Mission Creator stored as part of the Mission Data
//
// INPUT PARAMS:		paramArrayPos			Array Position for the slot to store the struct
// RETURN VALUE:		INT						The CreatorID stored in this array position
FUNC INT Get_MP_Mission_Request_CreatorID(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idCreator)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a Specific MP Mission has been requested
//
// INPUT PARAMS:		paramArrayPos			Array Position for the slot containing the data
// RETURN VALUE:		BOOL					TRUE if the Mission ID is anything except eNULL_MISSION, otherwiase FALSE
FUNC BOOL Has_A_Specific_MP_Mission_Been_Requested(INT paramArrayPos)
	
	IF (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idMission = eNULL_MISSION)
		RETURN FALSE
	ENDIF
	
	// A mission is stored in the slot
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the uniqueID as part of the Mission Data
//
// INPUT PARAMS:		paramArrayPos			Array Position for the slot to store the struct
//						paramUniqueID			The UniqueID for this mission request
//
// NOTES:	This only needs done for a request for a mission of type - Specific Mission requests will have already stored the unique ID on the Mission Data
PROC Store_MP_Mission_UniqueID_On_MissionData(INT paramArrayPos, INT paramUniqueID)
	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdUniqueID = paramUniqueID
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the uniqueID for a Mission Request
//
// INPUT PARAMS:		paramArrayPos			Array Position for the slot to store the struct
// RETURN VALUE:		INT						The UniqueID stored in this array position
FUNC INT Get_MP_Mission_Request_UniqueID(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdUniqueID)
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Request Options Update Functions - first gang joined mission
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets the 'First Gang On Mission' flag if this team may use Primary or Secondary comms for the mission
//
// INPUT PARAMS:		paramArrayPos			Array Position for the slot to store the struct
//						paramTeamID				The teamID being checked
PROC Update_First_Gang_On_Mission_Flag_When_Primary_And_Secondary_Comms(INT paramArrayPos, INT paramTeamID)

	IF (paramTeamID = TEAM_INVALID)
		EXIT
	ENDIF
	
	// Ignore if the flag is already set
	IF (Is_First_Crook_Gang_Joining_MP_Mission_Request(paramArrayPos))
		EXIT
	ENDIF
	
	// Does this team for this mission use Primary and Secondary comms?
	MP_MISSION thisMissionID	= Get_MP_Mission_Request_MissionID(paramArrayPos)
	INT thisMissionVar			= Get_MP_Mission_Request_MissionVariation(paramArrayPos)
	
	IF NOT (Does_MP_Mission_Variation_Use_Secondary_Comms(thisMissionID, thisMissionVar, paramTeamID))
		EXIT
	ENDIF
	
	// If it reaches here then set this is the First Crook Gang on mission for a mission that has Primary and Secondary comms
	Set_MP_Mission_Request_Option_First_Crook_Gang_Joining(paramArrayPos)

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Cleanup Functions after Player Left MP
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check all mission requests to see if the player is part of the mission and remove the player if so
//
// INPUT PARAMS:		paramPlayerAsInt		The PlayerID as an INT
PROC Remove_Player_From_MP_Mission_Requests(INT paramPlayerAsInt)

	INT tempLoop = 0
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		IF NOT (Is_MP_Mission_Request_Slot_Empty(temploop))
			// Player Active On Mission?
			IF (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsActivePlayers, paramPlayerAsInt))
				CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsActivePlayers, paramPlayerAsInt)
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("           Player As Int (")
					NET_PRINT_INT(paramPlayerAsInt)
					NET_PRINT(") removed as ACTIVE for mission in slot: ")
					NET_PRINT_INT(tempLoop)
					NET_PRINT("  [")
					NET_PRINT_INT(Get_MP_Mission_Request_UniqueID(tempLoop))
					NET_PRINT("]")
					NET_NL()
				#ENDIF
			ENDIF
			
			// Player Confirmed Mission?
			IF (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsConfirmedPlayers, paramPlayerAsInt))
				CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsConfirmedPlayers, paramPlayerAsInt)
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("           Player As Int (")
					NET_PRINT_INT(paramPlayerAsInt)
					NET_PRINT(") removed as CONFIRMED for mission in slot: ")
					NET_PRINT_INT(tempLoop)
					NET_PRINT("  [")
					NET_PRINT_INT(Get_MP_Mission_Request_UniqueID(tempLoop))
					NET_PRINT("]")
					NET_NL()
				#ENDIF
			ENDIF
			
			// Player Joining Mission?
			IF (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsJoiningPlayers, paramPlayerAsInt))
				CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsJoiningPlayers, paramPlayerAsInt)
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("           Player As Int (")
					NET_PRINT_INT(paramPlayerAsInt)
					NET_PRINT(") removed as JOINING for mission in slot: ")
					NET_PRINT_INT(tempLoop)
					NET_PRINT("  [")
					NET_PRINT_INT(Get_MP_Mission_Request_UniqueID(tempLoop))
					NET_PRINT("]")
					NET_NL()
				#ENDIF
			ENDIF
			
			// Player Reserved for Mission (this is the all-encompassing bitfield)?
			IF (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsReservedPlayersAll, paramPlayerAsInt))
				CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsReservedPlayersAll, paramPlayerAsInt)
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("           Player As Int (")
					NET_PRINT_INT(paramPlayerAsInt)
					NET_PRINT(") removed as RESERVED for mission in slot: ")
					NET_PRINT_INT(tempLoop)
					NET_PRINT("  [")
					NET_PRINT_INT(Get_MP_Mission_Request_UniqueID(tempLoop))
					NET_PRINT("]")
					NET_NL()
				#ENDIF
			ENDIF
				
			// Player Reserved by Player Choice for Mission (subset of RESERVED)?
			IF (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsReservedPlayersByPlayer, paramPlayerAsInt))
				CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsReservedPlayersByPlayer, paramPlayerAsInt)
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("           Player As Int (")
					NET_PRINT_INT(paramPlayerAsInt)
					NET_PRINT(") removed as RESERVED_BY_PLAYER for mission in slot: ")
					NET_PRINT_INT(tempLoop)
					NET_PRINT("  [")
					NET_PRINT_INT(Get_MP_Mission_Request_UniqueID(tempLoop))
					NET_PRINT("]")
					NET_NL()
				#ENDIF
			ENDIF
			
			// Player Reserved by Leader Choice for Mission (subset of RESERVED)?
			IF (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsReservedPlayersByLeader, paramPlayerAsInt))
				CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsReservedPlayersByLeader, paramPlayerAsInt)
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("           Player As Int (")
					NET_PRINT_INT(paramPlayerAsInt)
					NET_PRINT(") removed as RESERVED_BY_LEADER for mission in slot: ")
					NET_PRINT_INT(tempLoop)
					NET_PRINT("  [")
					NET_PRINT_INT(Get_MP_Mission_Request_UniqueID(tempLoop))
					NET_PRINT("]")
					NET_NL()
				#ENDIF
			ENDIF
			
			// Player Reserved by Game for Mission (subset of RESERVED)?
			IF (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsReservedPlayersByGame, paramPlayerAsInt))
				CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsReservedPlayersByGame, paramPlayerAsInt)
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("           Player As Int (")
					NET_PRINT_INT(paramPlayerAsInt)
					NET_PRINT(") removed as RESERVED_BY_GAME for mission in slot: ")
					NET_PRINT_INT(tempLoop)
					NET_PRINT("  [")
					NET_PRINT_INT(Get_MP_Mission_Request_UniqueID(tempLoop))
					NET_PRINT("]")
					NET_NL()
				#ENDIF
			ENDIF
			
			// Player Excluded for Mission?
			IF (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsExcludedPlayers, paramPlayerAsInt))
				CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsExcludedPlayers, paramPlayerAsInt)
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("           Player As Int (")
					NET_PRINT_INT(paramPlayerAsInt)
					NET_PRINT(") removed as EXCLUDED for mission in slot: ")
					NET_PRINT_INT(tempLoop)
					NET_PRINT("  [")
					NET_PRINT_INT(Get_MP_Mission_Request_UniqueID(tempLoop))
					NET_PRINT("]")
					NET_NL()
				#ENDIF
			ENDIF
			
			// Player Rejected Mission?
			IF (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsRejectedPlayers, paramPlayerAsInt))
				CLEAR_BIT(GlobalServerBD_MissionRequest.missionRequests[temploop].mrBitsRejectedPlayers, paramPlayerAsInt)
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("           Player As Int (")
					NET_PRINT_INT(paramPlayerAsInt)
					NET_PRINT(") removed as REJECTED for mission in slot: ")
					NET_PRINT_INT(tempLoop)
					NET_PRINT("  [")
					NET_PRINT_INT(Get_MP_Mission_Request_UniqueID(tempLoop))
					NET_PRINT("]")
					NET_NL()
				#ENDIF
			ENDIF
			
			// Clean up the player's 'status' details
			Clear_Mission_Control_Player_State(paramPlayerAsInt)
		ENDIF
	ENDREPEAT
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Mission Control Data Routines
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear all the MP Mission Control control data
PROC Clear_MP_Mission_Control_Data()

	GlobalServerBD_BlockB.missionControlData.mcBitsPreviousPlayers				= ALL_MISSION_CONTROL_BITS_CLEAR
	GlobalServerBD_BlockB.missionControlData.mcHost							= INVALID_PLAYER_INDEX()
	GlobalServerBD_BlockB.missionControlData.mcDynamicAllocTimeout				= GET_NETWORK_TIME()
	GlobalServerBD_BlockB.missionControlData.mcRestartDynamicAllocationTimer	= TRUE

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Mission Request Data Routines
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear out all the MP Control Request values
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be cleared
PROC Clear_One_MP_Mission_Request(INT paramArrayPos)

	g_structMissionRequestMP emptyStruct
	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos] = emptyStruct

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear out all the MP Control Request values, but first ensure the player states for all players on this mission get cleared
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be cleared
PROC Clear_One_MP_Mission_Request_And_Clear_Player_States(INT paramArrayPos)

	// Get all the player bits for this mission
	INT activePlayers		= Get_MP_Mission_Request_Active_Players(paramArrayPos)
	INT confirmedPlayers	= Get_MP_Mission_Request_Confirmed_Players(paramArrayPos)
	INT joiningPlayers		= Get_MP_Mission_Request_Joining_Players(paramArrayPos)
	INT reservedPlayers		= Get_MP_Mission_Request_Reserved_Players(paramArrayPos)

	// Clear the states of any players involved with this mission
	INT tempLoop = 0
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(reservedPlayers, tempLoop))
		OR (IS_BIT_SET(joiningPlayers, tempLoop))
		OR (IS_BIT_SET(confirmedPlayers, tempLoop))
		OR (IS_BIT_SET(activePlayers, tempLoop))
			Clear_Mission_Control_Player_State(tempLoop)
		ENDIF
	ENDREPEAT
	
	// Clear the Data
	Clear_One_MP_Mission_Request(paramArrayPos)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear out all the MP Control Request values
//
// INPUT PARAMS:	paramBeingInitialised		TRUE if this is being called during initialisation [default = FALSE]
PROC Clear_All_MP_Mission_Requests(BOOL paramBeingInitialised = FALSE)

	INT tempLoop = 0
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		Clear_One_MP_Mission_Request(tempLoop)
	ENDREPEAT
	
	// Don't update the widgets if being initialised - the widgets don't yet exist
	IF (paramBeingInitialised)
		EXIT
	ENDIF

	// This is where any widgets will get initialised

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Find an empty mission request slot
//
// RETURN VALUE:		INT			Empty array position, or NO_FREE_MISSION_REQUEST_SLOT
FUNC INT Find_Free_MP_Mission_Request_Slot()

	// Initially just look for slots marked as EMPTY
	
	INT tempLoop = 0
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		IF (Is_MP_Mission_Request_Slot_Empty(tempLoop))
			#IF IS_DEBUG_BUILD
				NET_PRINT("...........Find_Free_MP_Mission_Request_Slot() - Found Free Slot: ") NET_PRINT_INT(tempLoop) NET_NL()
			#ENDIF
	
			RETURN (tempLoop)
		ENDIF
	ENDREPEAT

	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Find_Free_MP_Mission_Request_Slot() - FAILED TO FIND A FREE SLOT") NET_NL()
		Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
	#ENDIF

	RETURN NO_FREE_MISSION_REQUEST_SLOT

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Find the slot containing this MP Mission Data based on the UniqueID
//
// INPUT PARAMS:		paramUniqueID			The UniqueID
// RETURN VALUE:		INT						The array position, or FAILED_TO_FIND_MISSION
FUNC INT Find_Slot_Containing_This_MP_Mission(INT paramUniqueID)

	INT				tempLoop	= 0
	MP_MISSION_DATA	testData
	
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		IF NOT (Is_MP_Mission_Request_Slot_Empty(tempLoop))
			// ...this slot contains data, so check if it contains the required mission data
			testData = Get_MP_Mission_Request_Slot_Mission_Data(tempLoop)
			IF (paramUniqueID = testData.mdUniqueID)
				RETURN tempLoop
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find the data
	RETURN FAILED_TO_FIND_MISSION

ENDFUNC




// ===========================================================================================================
//      Store Details for new Mission Requests
// ===========================================================================================================

// PURPOSE:	Store Details for a Request for a New Mission of Type
// 
// INPUT PARAMS:		paramSlot				The pre-calculated Free Mission Request Slot
//						paramPlayerID			The PlayerIndex of the requesting player
//						paramIsHost				TRUE if the request is as a Script Host, FALSE if for the player as an individual
//						paramFromTeam			The TeamID for any team that should get priority for joining the mission
//						paramSourceID			The source of the Mission Request
//						paramMissionType		The Type of Mission being requested
//						paramUniqueID			The UniqueID for this mission request
PROC Store_MP_Mission_Request_For_Mission_Of_Type(INT paramSlot, PLAYER_INDEX paramPlayerID, BOOL paramIsHost, INT paramFromTeam, g_eMPMissionSource paramSourceID, MP_MISSION_TYPE paramMissionType, INT paramUniqueID)

	// Error Checking - Unless the request is from a Host, ensure the Player is still OK
	IF NOT (paramIsHost)
		IF NOT (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MControl]: Store_Mission_Request_For_Mission_Of_Type(): Request for a New Mission of Type for an individual player that is no longer running the game. Ignoring Request.") NET_NL()
			#ENDIF
		
			EXIT
		ENDIF
	ENDIF
	
	// Store the details
	Store_MP_Mission_Request_Source(paramSlot, paramSourceID)
	Store_MP_Mission_Requester(paramSlot, paramPlayerID, paramIsHost)
	Store_MP_Mission_Request_Requested_Team(paramSlot, paramFromTeam)
	Store_MP_Mission_Mission_Type(paramSlot, paramMissionType)
	Store_MP_Mission_UniqueID_On_MissionData(paramSlot, paramUniqueID)
	Store_MP_Mission_Request_Status_As_Received(paramSlot)

	// Console Log output
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Storing New Mission Request for Mission Of Type") NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(paramSlot)
	#ENDIF

ENDPROC

// PURPOSE: Check if this SPECIFIC player is Excluded from a specific mission
//
// INPUT PARAMS:		paramPlayerID 			PLAYER_INDEX of player you wish to check is excluded or not (most often actual local player)
//						paramUniqueID			The unique ID of the related mission that the player was invited to.
//
// RETURN VALUE:		BOOL					TRUE if the player is excluded from this mission
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid

FUNC BOOL Is_Player_Excluded_From_MP_Mission(PLAYER_INDEX paramPlayerID, INT paramUniqueID)
	// Get mission slot from UniqueID
	INT missionSlot   = Find_Slot_Containing_This_MP_Mission(paramUniqueID)
	
	INT	returnBits	= ALL_MISSION_CONTROL_BITS_CLEAR	
	
	// Return bits all clear if uniqueID doesn't exist
	IF (missionSlot = FAILED_TO_FIND_MISSION)
	    #IF IS_DEBUG_BUILD
	          NET_PRINT("      BUT: UniqueID Not Found. Players Returned:") NET_NL()
	          Debug_Output_Player_Names_From_Bitfield(returnBits)
	    #ENDIF

	    RETURN FALSE
	ENDIF
	
	INT excludedPlayersBitfield = Get_MP_Mission_Request_Excluded_Players(missionSlot)	
	INT playerAsBit = NATIVE_TO_INT(paramPlayerID)
	
	IF IS_BIT_SET(excludedPlayersBitfield, playerAsBit)
		PRINTLN("[MMM] Player is excluded")
		RETURN TRUE
	ENDIF 
	PRINTLN("[MMM] Player is not excluded")
	RETURN FALSE
ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store Details for a Request for a New Specific Mission
// 
// INPUT PARAMS:		paramSlot				The pre-calculated Free Mission Request Slot
//						paramPlayerID			The PlayerIndex of the requesting player
//						paramIsHost				TRUE if the request is as a Script Host, FALSE if for the player as an individual
//						paramFromTeam			The TeamID for any team that should get priority for joining the mission
//						paramSourceID			The source of the Mission Request
//						paramMissionData		Any pre-determined Mission Data
PROC Store_MP_Mission_Request_For_Specific_Mission(INT paramSlot, PLAYER_INDEX paramPlayerID, BOOL paramIsHost, INT paramFromTeam, g_eMPMissionSource paramSourceID, MP_MISSION_DATA paramMissionData)

	// Error Checking - Unless the request is from a Host, ensure the Player is still OK
	IF NOT (paramIsHost)
		IF NOT (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MControl]: Store_MP_Mission_Request_For_Specific_Mission(): Request for a New Specific Mission for an individual player that is no longer running the game. Ignoring Request.") NET_NL()
			#ENDIF
		
			EXIT
		ENDIF
	ENDIF

	// Error Checking - The Mission Data must include a valid missionID
	IF (paramMissionData.mdID.idMission = eNULL_MISSION)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Store_MP_Mission_Request_For_Specific_Mission(): The requested mission is eNULL_MISSION. Ignoring Request.") NET_NL()
			SCRIPT_ASSERT("Store_MP_Mission_Request_For_Specific_Mission() - ERROR: Requested Mission was eNULL_MISSION. Ignoring Request. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Store the details
	Store_MP_Mission_Request_Source(paramSlot, paramSourceID)
	Store_MP_Mission_Requester(paramSlot, paramPlayerID, paramIsHost)
	Store_MP_Mission_Request_Requested_Team(paramSlot, paramFromTeam)
	Store_MP_Mission_Mission_Data(paramSlot, paramMissionData)
	Store_MP_Mission_Request_Status_As_Received(paramSlot)

	// Console Log output
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Storing New Mission Request for Specific Mission") NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(paramSlot)
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store Details for a Request for a Reserved Mission
// 
// INPUT PARAMS:		paramSlot				The pre-calculated Free Mission Request Slot
//						paramPlayerID			The PlayerIndex of the requesting player
//						paramFromTeam			The TeamID for any team that should get priority for joining the mission
//						paramSourceID			The source of the Mission Request
//						paramMissionData		Any pre-determined Mission Data
//						paramHasCutscene		TRUE if this mission has a pre-mission cutscene, otherwise FALSE
//						paramTutorialSessionID	[DEFAULT=TUTORIAL_SESSION_NOT_ACTIVE] If required, this mission will only accept players already in a specific tutorial instance
//
// NOTES:	Assumes the player is OK and that the mission is valid
PROC Store_MP_Mission_Request_To_Reserve_Mission(INT paramSlot, PLAYER_INDEX paramPlayerID, INT paramFromTeam, g_eMPMissionSource paramSourceID, MP_MISSION_DATA paramMissionData, BOOL paramHasCutscene, INT paramTutorialSessionID = TUTORIAL_SESSION_NOT_ACTIVE)
	
	BOOL isHost = FALSE
	
	// Store the details
	Store_MP_Mission_Request_Source(paramSlot, paramSourceID)
	Store_MP_Mission_Requester(paramSlot, paramPlayerID, isHost)
	Store_MP_Mission_Request_Requested_Team(paramSlot, paramFromTeam)
	Store_MP_Mission_Mission_Data(paramSlot, paramMissionData)
	Store_MP_Mission_Request_Status_As_Reserved(paramSlot)
	Store_MP_Mission_Request_Specific_Tutorial_SessionID(paramSlot, paramTutorialSessionID)
	
	IF (paramHasCutscene)
		Store_MP_Mission_Request_Option_PreMission_Cutscene(paramSlot)
	ENDIF

	// Console Log output
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Storing Request for New Reserved Mission") NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(paramSlot)
	#ENDIF

ENDPROC



// ===========================================================================================================
//      Mission Control Player and Team Info Routines
// ===========================================================================================================

// PURPOSE:	Clears the Player and Team Info temporary data storage
//
// RETURN PARAMS:		paramInfo			The array of player and team bits and values
PROC Clear_Player_And_Team_Info(m_structPlayerAndTeamInfoMC &paramInfo)

	// Players
	paramInfo.bitsBusyPlayers				= ALL_MISSION_CONTROL_BITS_CLEAR
	paramInfo.bitsUnavailablePlayers		= ALL_MISSION_CONTROL_BITS_CLEAR
	paramInfo.bitsAllowIfRequiredPlayers	= ALL_MISSION_CONTROL_BITS_CLEAR
	paramInfo.bitsFreePlayers				= ALL_MISSION_CONTROL_BITS_CLEAR
	paramInfo.numPlayersFree				= 0
	
//	INT tempLoop = 0
//	REPEAT MAX_NUM_TEAMS tempLoop
//		paramInfo.bitsFreeTeamMembers[tempLoop]		= ALL_MISSION_CONTROL_BITS_CLEAR
//		paramInfo.numTeamMembersFree[tempLoop]		= 0
//		paramInfo.bitsAllTeamMembers[tempLoop]		= ALL_MISSION_CONTROL_BITS_CLEAR
//		paramInfo.numTeamMembers[tempLoop]			= 0
//	ENDREPEAT
	
//	#IF IS_DEBUG_BUILD
//		REPEAT NUM_NETWORK_PLAYERS tempLoop
//			paramInfo.bitsUnavailableReasons[tempLoop]	= ALL_MISSION_CONTROL_BITS_CLEAR
//		ENDREPEAT
//	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Fill the Busy Players bits based on players that are actively on mission, or reserved for missions
//
// RETURN PARAMS:		paramInfo			The array of player and team bits and values
PROC Generate_Busy_Players_Data(m_structPlayerAndTeamInfoMC &paramInfo)

	INT tempLoop = 0
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		SWITCH (Get_MP_Mission_Request_Slot_Status(tempLoop))
			// Active, Offered, Received, and Reserved missions have 'busy' players
			CASE MP_MISSION_STATE_ACTIVE
			CASE MP_MISSION_STATE_ACTIVE_SECONDARY
			CASE MP_MISSION_STATE_OFFERED
			CASE MP_MISSION_STATE_RECEIVED
			CASE MP_MISSION_STATE_RESERVED
				paramInfo.bitsBusyPlayers |= Get_MP_Mission_Request_Active_Players(tempLoop)
				paramInfo.bitsBusyPlayers |= Get_MP_Mission_Request_Confirmed_Players(tempLoop)
				paramInfo.bitsBusyPlayers |= Get_MP_Mission_Request_Joining_Players(tempLoop)
				paramInfo.bitsBusyPlayers |= Get_MP_Mission_Request_Reserved_Players(tempLoop)
				BREAK
		ENDSWITCH
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a player is busy because offered a mission or active on a mission
//
// INPUT PARAMS:		paramPlayerAsInt	The PLAYER_INDEX as an INT
// RETURN PARAMS:		paramInfo			The array of player and team bits and values
// RETURN VALUE:		BOOL				TRUE if the player is busy, otherwise FALSE
FUNC BOOL Is_MP_Mission_Request_Player_Busy(INT paramPlayerAsInt, m_structPlayerAndTeamInfoMC &paramInfo)
	RETURN (IS_BIT_SET(paramInfo.bitsBusyPlayers, paramPlayerAsInt))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a player is unavailable because of some other game condition
//
// INPUT PARAMS:		paramPlayerAsInt	The PLAYER_INDEX as an INT
// RETURN PARAMS:		paramInfo			The array of player and team bits and values
// RETURN VALUE:		BOOL				TRUE if the player is unavailable, otherwise FALSE
FUNC BOOL Is_MP_Mission_Request_Player_Unavailable(INT paramPlayerAsInt, m_structPlayerAndTeamInfoMC &paramInfo)
	RETURN (IS_BIT_SET(paramInfo.bitsUnavailablePlayers, paramPlayerAsInt))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a player is only available if specifically required for a mission (ie: because doing pre-game tutorials)
//
// INPUT PARAMS:		paramPlayerAsInt	The PLAYER_INDEX as an INT
// RETURN PARAMS:		paramInfo			The array of player and team bits and values
// RETURN VALUE:		BOOL				TRUE if the player is available if specifically required, otherwise FALSE
FUNC BOOL Is_MP_Mission_Request_Player_Only_Available_If_Required(INT paramPlayerAsInt, m_structPlayerAndTeamInfoMC &paramInfo)
	RETURN (IS_BIT_SET(paramInfo.bitsAllowIfRequiredPlayers, paramPlayerAsInt))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a player is free to be offered a mission
//
// INPUT PARAMS:		paramPlayerAsInt	The PLAYER_INDEX as an INT
// RETURN PARAMS:		paramInfo			The array of player and team bits and values
// RETURN VALUE:		BOOL				TRUE if the player is free, otherwise FALSE
FUNC BOOL Is_MP_Mission_Request_Player_Free(INT paramPlayerAsInt, m_structPlayerAndTeamInfoMC &paramInfo)
	RETURN (IS_BIT_SET(paramInfo.bitsFreePlayers, paramPlayerAsInt))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player has completed the pre-game introduction
//
// INPUT PARAMS:		paramPlayerID		The Player Index
// RETURN VALUE:		BOOL				TRUE if the pre-game introduction has been completed, otherwise FALSE if it is still ongoing
FUNC BOOL Has_Player_Completed_PreGame_Introduction(PLAYER_INDEX paramPlayerID)

	// KGM 5/3/13: Freemode doesn't use the Pre-Game Introduction flag - if it does we need to add something generic or move this function out of the mission controller

	paramPlayerID  = paramPlayerID 

	// Just assume the player has completed the introduction
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Fill the Unavailable Players data required for making mission decisions
//
// RETURN PARAMS:		paramInfo			The array of player and team bits and values
PROC Generate_Unavailable_Players_Data(m_structPlayerAndTeamInfoMC &paramInfo)

	// Need to go through all players in the game and build up team member data
	// This isn't added to the 32 player repeat because it may not be required every frame
	INT				playerAsInt	= 0
	PLAYER_INDEX	thisPlayer
	
	REPEAT NUM_NETWORK_PLAYERS playerAsInt
		thisPlayer = INT_TO_PLAYERINDEX(playerAsInt)
		IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
			//Added a check to make sure that the player is in the GBD array to stop an array overrun. - BWW 21/02/12
			IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(thisPlayer)
				// Check if the Player is available to join a mission
				BOOL playerAvailable = TRUE
				
				IF (IS_PLAYER_PLAYING(thisPlayer))
					// KGM 14/9/12: Check if player is on the MP HUD
					IF (IS_PLAYER_SCTV(thisPlayer))
						#IF IS_DEBUG_BUILD
							SET_BIT(paramInfo.bitsUnavailableReasons[playerAsInt], BITS_UNAVAILABLE_REASON_SPECTATOR)
						#ENDIF
						
						playerAvailable = FALSE
					ENDIF
				ENDIF
				
				IF NOT (playerAvailable)
					// This player is unavailable to join missions
					SET_BIT(paramInfo.bitsUnavailablePlayers, playerAsInt)
				ENDIF
				
				// Check if the player is only available to join the mission if specifically required for the mission
				IF NOT (Has_Player_Completed_PreGame_Introduction(thisPlayer))
					// This player is on the pre-game tutorials, so unavailable unless required for a mission
					SET_BIT(paramInfo.bitsAllowIfRequiredPlayers, playerAsInt)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Fill the Free Players data required for making mission decisions
//
// RETURN PARAMS:		paramInfo			The array of player and team bits and values
PROC Generate_Free_Players_Data(m_structPlayerAndTeamInfoMC &paramInfo)

	// Go through all players in the game and build up the free players data
	// This isn't added to the 32 player repeat because it may not be required every frame
	INT				playerAsInt		= 0
	PLAYER_INDEX	thisPlayer
	
	REPEAT NUM_NETWORK_PLAYERS playerAsInt
		thisPlayer = INT_TO_PLAYERINDEX(playerAsInt)
		
		IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
			// Found an active player, so check if the player is free to join missions
			IF NOT (Is_MP_Mission_Request_Player_Busy(playerAsInt, paramInfo))
			AND NOT (Is_MP_Mission_Request_Player_Unavailable(playerAsInt, paramInfo))
			AND NOT (Is_MP_Mission_Request_Player_Only_Available_If_Required(playerAsInt, paramInfo))
				// This player is available to join missions
				SET_BIT(paramInfo.bitsFreePlayers, playerAsInt)
				paramInfo.numPlayersFree += 1
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Fill the Team Members data required for making mission decisions
//
// RETURN PARAMS:		paramInfo			The array of player and team bits and values
PROC Generate_Team_Member_Data(m_structPlayerAndTeamInfoMC &paramInfo)

	// KGM 18/1/13: Nasty Freemode Fix because Team Deathmatches and missions can set the player on team numbers above the Max CnC Team
	IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
	OR (GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY)
	OR (GET_CURRENT_GAMEMODE() = GAMEMODE_SP)
		EXIT
	ENDIF

	// Need to go through all players in the game and build up team member data
	// This isn't added to the 32 player repeat because it may not be required every frame
	INT				playerAsInt		= 0
	INT				playerTeam		= TEAM_INVALID
	PLAYER_INDEX	thisPlayer
	
	REPEAT NUM_NETWORK_PLAYERS playerAsInt
		thisPlayer = INT_TO_PLAYERINDEX(playerAsInt)
		
		IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
			// Found an active player, so update the team info for the player if the player is in a team
			playerTeam = GET_PLAYER_TEAM(thisPlayer)
			
			// Update data for player's team if applicable
			IF (IS_TEAM_AN_ACTIVE_TEAM(playerTeam))
//				// Update the Total Number of Members in each Team
//				SET_BIT(paramInfo.bitsAllTeamMembers[playerTeam], playerAsInt)
//				paramInfo.numTeamMembers[playerTeam] += 1
				
				// If the player is free to join missions then update the free team members data too
				IF (IS_BIT_SET(paramInfo.bitsFreePlayers, playerAsInt))
					// Set that the Team Member is free to join a mission
//					SET_BIT(paramInfo.bitsFreeTeamMembers[playerTeam], playerAsInt)
//					paramInfo.numTeamMembersFree[playerTeam] += 1
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Generate the Player And Team Info data - this is temporary data used for decision making during this frame
//
// RETURN PARAMS:		paramInfo			The array of player and team bits and values
PROC Fill_Current_Player_And_Team_Info(m_structPlayerAndTeamInfoMC &paramInfo)

	// Clear the data fields
	Clear_Player_And_Team_Info(paramInfo)

	// Fill the data fields with relevant data
	Generate_Busy_Players_Data(paramInfo)
	Generate_Unavailable_Players_Data(paramInfo)
	Generate_Free_Players_Data(paramInfo)
	Generate_Team_Member_Data(paramInfo)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	In the initial player analysis, Reserved By Player Players for a mission get marked as busy - they need to become free when the mission is being processed
//
// INPUT PARAMS:			paramMissionSlot		The Mission Request slot
// RETURN PARAMS:			paramInfo				The array of player and team bits and values
//
// NOTES:	Assumes the mission is in the 'received' state and that this has been verified prior to calling this function.
PROC Set_Any_Reserved_By_Player_Players_For_This_Mission_As_Not_Busy(INT paramMissionSlot, m_structPlayerAndTeamInfoMC &paramInfo)

	// Get the Reserved By Player players for this mission
	INT reservedByPlayerPlayers = Get_MP_Mission_Request_Reserved_By_Player_Players(paramMissionSlot)
	IF (reservedByPlayerPlayers = ALL_MISSION_CONTROL_BITS_CLEAR)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL()
		NET_PRINT("...KGM MP [MControl]: Found Reserved_By_Player Players that are going to be freed for this mission request. Slot: ")
		NET_PRINT_INT(paramMissionSlot)
		NET_NL()
		Debug_Output_Player_Names_From_Bitfield(reservedByPlayerPlayers)
	#ENDIF
	
	// Free up the required players
	INT				playerAsInt	= 0
	INT				playerTeam	= TEAM_INVALID
	PLAYER_INDEX	thisPlayer
	
	REPEAT NUM_NETWORK_PLAYERS playerAsInt
		IF (IS_BIT_SET(reservedByPlayerPlayers, playerAsInt))
			// This player is Reserved By Player for this mission
			
			// Do some debug consistency checking
			#IF IS_DEBUG_BUILD
				IF NOT (Is_MP_Mission_Request_Player_Busy(playerAsInt, paramInfo))
				AND NOT (Is_MP_Mission_Request_Player_Only_Available_If_Required(playerAsInt, paramInfo))
					SCRIPT_ASSERT("Set_Any_Reserved_By_Player_Players_For_This_Mission_As_Not_Busy: Player is Reserved By Player but is not classed as BUSY, or AVAILABLE IF RESERVED BY PLAYER. See Console Log. Tell Keith.")
					EXIT
				ENDIF
				
				IF (Is_MP_Mission_Request_Player_Free(playerAsInt, paramInfo))
					SCRIPT_ASSERT("Set_Any_Reserved_By_Player_Players_For_This_Mission_As_Not_Busy: Player is Reserved By Player but is classed as FREE. See Console Log. Tell Keith.")
					EXIT
				ENDIF
			#ENDIF
			
			// Mark this player as Not Busy
			CLEAR_BIT(paramInfo.bitsBusyPlayers, playerAsInt)
			
			// Remove the 'allow if reserved BY Player' flag because the player is required and so is going to be freed for this mission
			CLEAR_BIT(paramInfo.bitsAllowIfRequiredPlayers, playerAsInt)
			
			// Mark this player as Free unless the player is unavailable
			IF NOT (Is_MP_Mission_Request_Player_Unavailable(playerAsInt, paramInfo))
				thisPlayer = INT_TO_PLAYERINDEX(playerAsInt)
				
				IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
					// This player is free to join the mission
					SET_BIT(paramInfo.bitsFreePlayers, playerAsInt)
					paramInfo.numPlayersFree += 1
					
					// Found an active player, so get the player's team and mark the player as free within the team
					playerTeam = GET_PLAYER_TEAM(thisPlayer)
					
					IF NOT (playerTeam = TEAM_INVALID)
//						SET_BIT(paramInfo.bitsFreeTeamMembers[playerTeam], playerAsInt)
//						paramInfo.numTeamMembersFree[playerTeam] += 1
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		Debug_Output_Player_And_Team_Details(paramInfo)
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Remove Player as Reserved from all reserved bitfields if the new reservation request is a higher priority reservation
//
// INPUT PARAMS:		paramPlayerIndex		The PlayerIndex of the player
//						paramPlayerIdAsInt		The PlayerID as an INT of the player being checked
//						paramReservationDesc	The description of the new reservation type
// RETURN PARAMS:		paramInfo				The player and Team Info
//
// NOTES:	Assumes the mission slot isn't empty and that the player index is valid
PROC Unreserve_Player_For_Higher_Priority_Reservation_And_Generate_New_Player_And_Team_Data(PLAYER_INDEX paramPlayerIndex, INT paramPlayerIdAsInt, g_eMPMissionReservationDescs paramReservationDesc, m_structPlayerAndTeamInfoMC &paramInfo)

	// If the player is unavailable then nothing to do
	IF (Is_MP_Mission_Request_Player_Unavailable(paramPlayerIdAsInt, paramInfo))
		EXIT
	ENDIF
	
	// If the player is only available if required then do nothing here
	IF (Is_MP_Mission_Request_Player_Only_Available_If_Required(paramPlayerIdAsInt, paramInfo))
		EXIT
	ENDIF
	
	// If the player is not allocated to any mission request slot, then nothing to do here
	INT playerMissionSlot = GlobalServerBD_BlockB.missionPlayerData[paramPlayerIdAsInt].mcpSlot
	IF (playerMissionSlot = MPMCP_NO_MISSION_SLOT)
		EXIT
	ENDIF
	
	// If the player is not reserved for this slot, then nothing to do here
	IF NOT (Is_Player_Reserved_For_MP_Mission_Request_Slot(playerMissionSlot, paramPlayerIndex))
		EXIT
	ENDIF
	
	// The player is reserved for this slot, so check if it is a lower priority reservation
	BOOL reservedByPlayer	= Is_Player_Reserved_By_Player_For_MP_Mission_Request_Slot(playerMissionSlot, paramPlayerIndex)
	BOOL reservedByLeader	= Is_Player_Reserved_By_Leader_For_MP_Mission_Request_Slot(playerMissionSlot, paramPlayerIndex)
	BOOL reservedByGame		= Is_Player_Reserved_By_Game_For_MP_Mission_Request_Slot(playerMissionSlot, paramPlayerIndex)
	
	#IF IS_DEBUG_BUILD
		g_eMPMissionReservationDescs	existingReservationDesc = NO_MP_RESERVATION_DESCRIPTION
		IF (reservedByPlayer)
			existingReservationDesc = MP_RESERVED_BY_PLAYER
		ELSE
			IF (reservedByLeader)
				existingReservationDesc = MP_RESERVED_BY_LEADER
			ELSE
				IF (reservedByGame)
					existingReservationDesc = MP_RESERVED_BY_GAME
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
	SWITCH (paramReservationDesc)
		// Highest Priority Reservation Type
		CASE MP_RESERVED_BY_PLAYER
			IF (reservedByPlayer)
				// Already reserved with equal reservation priority, so leave as-is
				EXIT
			ENDIF
			BREAK
	
		// Medium Priority Reservation Type
		CASE MP_RESERVED_BY_LEADER
			IF (reservedByPlayer)
			OR (reservedByLeader)
				// Already reserved with equal or higher reservation priority, so leave as-is
				EXIT
			ENDIF
			BREAK
			
		// Lowest Priority Reservation Type
		// NOTE: Doing the same checks as above for consistency in case we introduce more priorities later
		CASE MP_RESERVED_BY_GAME
			IF (reservedByPlayer)
			OR (reservedByLeader)
			OR (reservedByGame)
				EXIT
			ENDIF
			BREAK
			
		// Error
		DEFAULT
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MControl]: Unreserve_Player_For_Higher_Priority_Reservation_And_Generate_New_Player_And_Team_Data() - called with ")
				NET_PRINT(Convert_Reservation_Description_To_String(paramReservationDesc))
				NET_NL()
				SCRIPT_ASSERT("Unreserve_Player_For_Higher_Priority_Reservation_And_Generate_New_Player_And_Team_Data() called with illegal reservationDesc. See console log. Tell Keith.")
			#ENDIF
			EXIT
	ENDSWITCH
	
	// Found a higher priority reservation, so remove the old reservation and regenerate the team and player data struct
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Player is currently ")
		NET_PRINT(Convert_Reservation_Description_To_String(existingReservationDesc))
		NET_PRINT(" for slot: ")
		NET_PRINT_INT(playerMissionSlot)
		NET_PRINT(" [")
		NET_PRINT(GET_MP_MISSION_NAME(Get_MP_Mission_Request_MissionID(playerMissionSlot)))
		NET_PRINT("] - Removing player reservation to allow player to become ")
		NET_PRINT(Convert_Reservation_Description_To_String(paramReservationDesc))
		NET_NL()
	#ENDIF
				
	Remove_Player_From_All_Reserved_Bitfields_On_MP_Mission_Request_Slot(playerMissionSlot, paramPlayerIndex)
	Fill_Current_Player_And_Team_Info(paramInfo)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Reserve the bitlist of players on the specified mission reequest slot as 'Reserved By Leader' if possible
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramBitsPlayers		The Players to be reserved
// RETURN PARAMS:		paramInfo				The player and Team Info
//
// NOTES:	The Player and Team details are re-generated using the 'Unreserve' function if anything changes
PROC Reserve_Players_By_Leader_For_Mission(INT paramArrayPos, INT paramBitsPlayers, m_structPlayerAndTeamInfoMC &paramInfo)

	INT				tempLoop	= 0
	PLAYER_INDEX	thisPlayer
	
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(paramBitsPlayers, tempLoop))
			// ...found a player
			#IF IS_DEBUG_BUILD
				NET_PRINT("        Found Player To Be Reserved: ")
			#ENDIF
			
			thisPlayer = INT_TO_PLAYERINDEX(tempLoop)
			IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
				#IF IS_DEBUG_BUILD
					NET_PRINT(GET_PLAYER_NAME(thisPlayer))
					NET_NL()
				#ENDIF
				
				// Unreserve this player if necessary
				Unreserve_Player_For_Higher_Priority_Reservation_And_Generate_New_Player_And_Team_Data(thisPlayer, tempLoop, MP_RESERVED_BY_LEADER, paramInfo)
				
				// If the player is now free, the player becomes 'Reserved By Leader' for the mission
				IF (Is_MP_Mission_Request_Player_Free(tempLoop, paramInfo))
					Set_Player_As_Reserved_By_Leader_On_MP_Mission_Request_Slot(paramArrayPos, thisPlayer)
					
					#IF IS_DEBUG_BUILD
						NET_PRINT("           ")
						NET_PRINT(GET_PLAYER_NAME(thisPlayer))
						NET_PRINT(" - NOW RESERVED BY LEADER")
						NET_NL()
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_PRINT("           ")
						NET_PRINT(GET_PLAYER_NAME(thisPlayer))
						IF (thisPlayer = PLAYER_ID())
							NET_PRINT(" - ALREADY RESERVED AS LEADER")
						ELSE
							NET_PRINT(" - FAILED - PLAYER IS BUSY")
						ENDIF
						NET_NL()
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					NET_PRINT("[Player Number ")
					NET_PRINT_INT(tempLoop)
					NET_PRINT("] is no longer OK - not reserving")
					NET_NL()
				#ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC




// ===========================================================================================================
//      Failed and Finished Mission Requests
// ===========================================================================================================

// PURPOSE:	To clean up after a Mission Request failed
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
PROC MP_Mission_Request_Failed(INT paramArrayPos)

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Processing MP_Mission_Request_Failed. Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
	#ENDIF

	Broadcast_Mission_Request_Failed(paramArrayPos)
	Clear_One_MP_Mission_Request_And_Clear_Player_States(paramArrayPos)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Contents of all Mission Request Slots after a Mission Request Failed. Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
		Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To clean up after a Mission Request finished
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
PROC MP_Mission_Request_Finished(INT paramArrayPos)

	Broadcast_Mission_Request_Finished(paramArrayPos)
	Clear_One_MP_Mission_Request_And_Clear_Player_States(paramArrayPos)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Contents of all Mission Request Slots after a Mission Request Finished. Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
		Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To broadcast the reason why a mission request failed to all Reserved By Player players
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramReasonID			The Reason for Mission Request failure
PROC MP_Mission_Request_Failure_Reason(INT paramArrayPos, m_eFailedToLaunchReasons paramReasonID)
	
	INT					tempLoop					= 0
	BOOL				isHost						= FALSE
	INT					reservedByPlayerPlayers		= Get_MP_Mission_Request_Reserved_By_Player_Players(paramArrayPos)
	g_eMPMissionSource	theSource					= Get_MP_Mission_Request_Source(paramArrayPos)
	MP_MISSION			theMission					= Get_MP_Mission_Request_MissionID(paramArrayPos)
	PLAYER_INDEX		thisPlayer
	
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(reservedByPlayerPlayers, tempLoop))
			thisPlayer = INT_TO_PLAYERINDEX(tempLoop)
			IF (IS_NET_PLAYER_OK(thisPlayer))
				Broadcast_Reason_For_Mission_Request_Failure(isHost, thisPlayer, theSource, paramReasonID, theMission)
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC




// ===========================================================================================================
//      Fill Mission Data For New Mission Request
// ===========================================================================================================

// PURPOSE:	Check for any mission-specific restrictions
//
// INPUT PARAMS:			paramArrayPos		The array position for this mission
//							paramMissionID		The MissionID to be checked
// RETURN VALUE:			BOOL				TRUE if there is a restriction, otherwise FALSE
//
// NOTES:	TEMPORARY IMPLEMENTATION: If these restrictions get set up as bitflags then it should be possible to write a routine that checks all restrictions at once with a bitwise-AND
FUNC BOOL Check_For_Mission_Specific_Launch_Restrictions(INT paramArrayPos, MP_MISSION paramMissionID)

	INT tempLoop = 0
	
	// The test data
	g_eMPMissionStatus activeMissionStatus
	MP_MISSION activeMission
	INT activeVariation
	
	// Additional Data for mission being checked
	INT testVariation = Get_MP_Mission_Request_MissionVariation(paramArrayPos)
	
	// Check Instances?
	g_eMPInstances instanceRestriction	= Get_MP_Mission_Instance_Restrictions(paramMissionID)
	BOOL checkInstances = TRUE
	IF NOT (instanceRestriction = MP_INST_ANY)
		checkInstances = TRUE
	ENDIF
	
	// Check for existing Prison mission?
	BOOL checkPrison = Does_MP_Mission_Use_Prison(paramMissionID)

	// Search all active missions for any mission specific restrictions
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		activeMissionStatus	= Get_MP_Mission_Request_Slot_Status(tempLoop)
		
		SWITCH (activeMissionStatus)
			// Check these active missions for any restrictions
			CASE MP_MISSION_STATE_ACTIVE
			CASE MP_MISSION_STATE_ACTIVE_SECONDARY
			CASE MP_MISSION_STATE_OFFERED
				activeMission	= Get_MP_Mission_Request_MissionID(tempLoop)
				activeVariation	= Get_MP_Mission_Request_MissionVariation(tempLoop)
				
				// If this Mission uses the PRISON then ensure there isn't already another active mission that uses the prison
				IF (checkPrison)
					IF (Does_MP_Mission_Use_Prison(activeMission))
						// An existing mission already uses the PRISON
						#IF IS_DEBUG_BUILD
							NET_PRINT("           REJECTED: ")
							NET_PRINT(GET_MP_MISSION_NAME(activeMission))
							NET_PRINT(" is active and uses the PRISON so can't start a new mission that uses the PRISON")
							NET_NL()
						#ENDIF
						
						MP_Mission_Request_Failure_Reason(paramArrayPos, FTLR_PRISON_MISSION_ALREADY_EXISTS)
						RETURN TRUE
					ENDIF
				ENDIF
				
				// If this mission has instance restrictions then check if the restriction exists
				IF (checkInstances)
					IF (paramMissionID = activeMission)
						// The requested mission is the same as the active mission so potential restriction exists
						SWITCH (instanceRestriction)
							// Only one instance allowed
							CASE MP_INST_ONE
								// An existing instance of this mission already exists
								#IF IS_DEBUG_BUILD
									NET_PRINT("           REJECTED: ")
									NET_PRINT(GET_MP_MISSION_NAME(activeMission))
									NET_PRINT(" is already active and only one instance of the mission is allowed")
									NET_NL()
								#ENDIF
								
								MP_Mission_Request_Failure_Reason(paramArrayPos, FTLR_MISSION_INSTANCE_ACTIVE)
								RETURN TRUE
								
							// Multiple Instances allowed as long as they are different variations
							CASE MP_INST_UNIQUE
								// An existing instance of this mission already exists, so check if the variations also match
								IF (testVariation = activeVariation)
									#IF IS_DEBUG_BUILD
										NET_PRINT("           REJECTED: ")
										NET_PRINT(GET_MP_MISSION_NAME(activeMission))
										NET_PRINT(" (")
										NET_PRINT_INT(activeVariation)
										NET_PRINT(") with the same variation is already active and only one instance of the mission variation is allowed")
										NET_NL()
									#ENDIF
									
									MP_Mission_Request_Failure_Reason(paramArrayPos, FTLR_MISSION_VARIATION_INSTANCE_ACTIVE)
									RETURN TRUE
								ENDIF
								BREAK
						ENDSWITCH
					ENDIF
				ENDIF
				BREAK
				
			// Ignore these missions - they aren't active
			CASE MP_MISSION_STATE_RECEIVED
			CASE MP_MISSION_STATE_RESERVED
			CASE NO_MISSION_REQUEST_RECEIVED
				BREAK
				
			// Catchall
			DEFAULT
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("Check_For_Mission_Specific_Launch_Restrictions(): Need to add new MP MISSION REQUEST STATE ID to the SWITCH. Tell Keith.")
					NET_PRINT("...KGM MP [MControl]: Check_For_Mission_Specific_Launch_Restrictions() Missing MP MISSION REQUEST STATE ID - Add to SWITCH.")
					NET_NL()
				#ENDIF
				BREAK
		ENDSWITCH
	ENDREPEAT
	
	// No restrictions
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check that the minimum team requirements for the mission are available
//
// INPUT PARAMS:		paramMissionID			The Mission ID for this request
//						paramArrayPos			Array Position containing mission to be checked
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
//						paramFreeTeams			A bitfield INT containing the free teams for this mission
// RETURN VALUE:		BOOL					TRUE if the mission is available for launch, otherwise FALSE
FUNC BOOL Are_Minimum_Team_Requirements_For_MP_Mission_Available(MP_MISSION paramMissionID, INT paramArrayPos, m_structPlayerAndTeamInfoMC &paramInfo, INT &paramFreeTeams)

	IF paramInfo.bitsBusyPlayers=0
		// fool compiler
	ENDIF

	// Return TRUE if the mission is not team-based
	IF NOT (Check_If_Players_Are_In_Teams_For_Mission(paramMissionID))
		paramFreeTeams = ALL_MISSION_CONTROL_BITS_CLEAR
		RETURN TRUE
	ENDIF

	// Set up a Teams bitfield
	INT bitsTeamsAllowedForMission = ALL_MISSION_CONTROL_BITS_CLEAR
	
	// Go through all teams, initially set teams with free players to TRUE, otherwise FALSE
//	INT tempLoop = 0
//	REPEAT MAX_NUM_TEAMS tempLoop
//		IF (paramInfo.numTeamMembersFree[tempLoop] > 0)
//			SET_BIT(bitsTeamsAllowedForMission, tempLoop)
//		ENDIF
//	ENDREPEAT

	IF (bitsTeamsAllowedForMission = ALL_MISSION_CONTROL_BITS_CLEAR)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           ")
			NET_PRINT(GET_MP_MISSION_NAME(paramMissionID))
			NET_PRINT(" - EXCLUDED (There are no teams with free players)")
			NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
//	// Exclude any teams that are not allowed on this mission
//	REPEAT MAX_NUM_TEAMS tempLoop
//		IF (IS_BIT_SET(bitsTeamsAllowedForMission, tempLoop))
//			IF NOT (Is_This_Team_Allowed_On_This_MP_Mission(paramMissionID, tempLoop))
//				CLEAR_BIT(bitsTeamsAllowedForMission, tempLoop)
//			ENDIF
//		ENDIF
//	ENDREPEAT

	IF (bitsTeamsAllowedForMission = ALL_MISSION_CONTROL_BITS_CLEAR)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           ")
			NET_PRINT(GET_MP_MISSION_NAME(paramMissionID))
			NET_PRINT(" - EXCLUDED (Any teams with free players are not the teams allowed on this mission)")
			NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
//	// Exclude any teams that don't have the minimum number of free team players for the mission
//	INT minimumTeamMembers = 0
//	REPEAT MAX_NUM_TEAMS tempLoop
//		IF (IS_BIT_SET(bitsTeamsAllowedForMission, tempLoop))
//			minimumTeamMembers = Get_Minimum_Number_Of_Team_Members_Required_For_MP_Mission(paramMissionID, tempLoop)
//			
//			// Debug: Allow launch without having minimum player requirements?
//			#IF IS_DEBUG_BUILD
//				IF (Does_MP_Mission_Request_Allow_One_Player_Launch_In_Debug(paramArrayPos))
//					IF (minimumTeamMembers > 1)
//						minimumTeamMembers = 1
//					ENDIF
//				ENDIF
//			#ENDIF
//			
//			IF (paramInfo.numTeamMembersFree[tempLoop] < minimumTeamMembers)
//				CLEAR_BIT(bitsTeamsAllowedForMission, tempLoop)
//			ENDIF
//		ENDIF
//	ENDREPEAT

	IF (bitsTeamsAllowedForMission = ALL_MISSION_CONTROL_BITS_CLEAR)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           ")
			NET_PRINT(GET_MP_MISSION_NAME(paramMissionID))
			NET_PRINT(" - EXCLUDED (No teams exist with minimum free players available for this mission)")
			NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
		
	// Make sure any teams specifically requested for this mission are available to join the mission
	// This should be tested after all the exclusions have been completed.
	INT requestedTeamsForMission = Get_MP_Mission_Request_Requested_Teams(paramArrayPos)
	IF NOT (requestedTeamsForMission = ALL_MISSION_CONTROL_BITS_CLEAR)
		// Perform bitwise AND between the Requested Teams bitfield and the Available Teams bitfield and ensure all requested teams are represented
		IF NOT ((requestedTeamsForMission & bitsTeamsAllowedForMission) = requestedTeamsForMission)
			// ...all requested teams aren't represented, so exclude this mission
			#IF IS_DEBUG_BUILD
				NET_PRINT("           ")
				NET_PRINT(GET_MP_MISSION_NAME(paramMissionID))
				NET_PRINT(" - EXCLUDED (All requested teams for the mission are not available)")
				NET_NL()
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Ensure the teams still available to start the mission match the Team Requirements for the mission (ie: COP TEAM + ONE CROOK TEAM, etc)
	INT 				theVariation		= Get_MP_Mission_Request_MissionVariation(paramArrayPos)
	MP_MISSION_ID_DATA	missionIdData		= Get_MP_Mission_Request_MissionID_Data(paramArrayPos)
	
	IF NOT (Does_Teams_Bitfield_Match_Teams_Required_For_Mission(paramMissionID, theVariation, bitsTeamsAllowedForMission))
		#IF IS_DEBUG_BUILD
			NET_PRINT("           ")
			NET_PRINT(GET_MP_MISSION_NAME(paramMissionID))
			NET_PRINT(" - EXCLUDED (The Available Teams don't meet the Team Configuration required for this mission)")
			NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF

	// Ensure the number of free players in the teams allowed for the mission match the minimum total players required for the mission
	INT minPlayersNeededForMission = Get_Minimum_Players_Required_For_MP_Mission_Request(missionIdData)
	
	#IF IS_DEBUG_BUILD
		IF (Does_MP_Mission_Request_Allow_One_Player_Launch_In_Debug(paramArrayPos))
			IF (minPlayersNeededForMission > 1)
				minPlayersNeededForMission = 1
			ENDIF
		ENDIF
	#ENDIF
	
	INT numAvailablePlayersInTeamsAllowedForMission	= 0
	
//	REPEAT MAX_NUM_TEAMS tempLoop
//		IF (IS_BIT_SET(bitsTeamsAllowedForMission, tempLoop))
//			numAvailablePlayersInTeamsAllowedForMission += paramInfo.numTeamMembersFree[tempLoop]
//		ENDIF
//	ENDREPEAT
	
	IF (numAvailablePlayersInTeamsAllowedForMission < minPlayersNeededForMission)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           ")
			NET_PRINT(GET_MP_MISSION_NAME(paramMissionID))
			NET_PRINT(" - EXCLUDED (The Available Teams don't have enough total free players to match the minimum total players required for the mission)")
			NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF

	// All good, so store the 'free teams' bitfield in the return parameter, and return
	paramFreeTeams = bitsTeamsAllowedForMission
	
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check that the mission fulfills all the launch requirements
//
// INPUT PARAMS:		paramMissionID			The Mission ID for this request
//						paramArrayPos			Array Position containing mission to be checked
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
//						paramFreeTeams			A bitfield INT containing the free teams for this mission
// RETURN VALUE:		BOOL					TRUE if the mission is available for launch, otherwise FALSE
FUNC BOOL Check_If_MP_Mission_Is_Available_To_Launch(MP_MISSION paramMissionID, INT paramArrayPos, m_structPlayerAndTeamInfoMC &paramInfo, INT &paramFreeTeams)

	g_eMPMissionSource	theRequestSourceID	= Get_MP_Mission_Request_Source(paramArrayPos)
	MP_MISSION_ID_DATA	missionIdData		= Get_MP_Mission_Request_MissionID_Data(paramArrayPos)
			
	
	// Grouped Expiry Missions should ignore disabled missions
	// (Missions Launched by debug should allow disabled missions)
	// (Mission Flow should check for disabled missions before requesting the mission, so that the mission can be passed to allow the flow to continue)
	IF (theRequestSourceID = MP_MISSION_SOURCE_DEBUG_GROUPED)
	OR (theRequestSourceID = MP_MISSION_SOURCE_GROUPED)
		IF (IS_MP_MISSION_DISABLED(paramMissionID))
			#IF IS_DEBUG_BUILD
				NET_PRINT("           ")
				NET_PRINT(GET_MP_MISSION_NAME(paramMissionID))
				NET_PRINT(" - EXCLUDED (Mission Is Disabled)")
				NET_NL()
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Check for Minimum Players for mission not being met - this is an early bomb out if there aren't enough players
	INT minimumPlayersRequired = Get_Minimum_Players_Required_For_MP_Mission_Request(missionIdData)
	
	#IF IS_DEBUG_BUILD	
		IF (Does_MP_Mission_Request_Allow_One_Player_Launch_In_Debug(paramArrayPos))
			IF (minimumPlayersRequired > 1)
				minimumPlayersRequired = 1
			ENDIF
		ENDIF	
		IF DEBUG_IS_SOLO_STRAND_ENABLED()
			RETURN TRUE
		ENDIF
	#ENDIF
	
	IF (paramInfo.numPlayersFree < minimumPlayersRequired)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           ")
			NET_PRINT(GET_MP_MISSION_NAME(paramMissionID))
			NET_PRINT(" (Var: ")
			NET_PRINT_INT(Get_MP_Mission_Request_MissionVariation(paramArrayPos))
			NET_PRINT(") - EXCLUDED (There are not enough total players free - Required: ")
			NET_PRINT_INT(Get_Minimum_Players_Required_For_MP_Mission_Request(missionIdData))
			NET_PRINT(")")
			NET_NL()
		#ENDIF
			
		MP_Mission_Request_Failure_Reason(paramArrayPos, FTLR_TEAM_REQUIREMENTS_NOT_MET)
		RETURN FALSE
	ENDIF
	
	// Check for any special restrictions that prevent mission launch
	IF (Check_For_Mission_Specific_Launch_Restrictions(paramArrayPos, paramMissionID))
		// A restriction exists
		// NOTE: Any required reasons will be issued within this function
		RETURN FALSE
	ENDIF
	
	// Ensure the correct teams and minimum team players required for the mission are available
	IF NOT (Are_Minimum_Team_Requirements_For_MP_Mission_Available(paramMissionID, paramArrayPos, paramInfo, paramFreeTeams))
		MP_Mission_Request_Failure_Reason(paramArrayPos, FTLR_TEAM_REQUIREMENTS_NOT_MET)
		RETURN FALSE
	ENDIF
	
	// Mission is still available to launch
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Choose a mission to launch
//
// INPUT PARAMS:		paramArrayPos			Array Position containing mission to be checked
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
// RETURN VALUE:		BOOL					TRUE if a mission has been chosen, otherwise FALSE
FUNC BOOL Choose_MP_Mission_For_MP_Mission_Request_Slot(INT paramArrayPos, m_structPlayerAndTeamInfoMC &paramInfo)

	// Create an array of BOOLS used to determine which missions are available, and an array of INTs to hold Team Bitfield for the teams available for this mission
	BOOL availableMissionsMP[eMAX_NUM_MP_MISSION]
	INT bitsFreeTeamsForMissionsMP[eMAX_NUM_MP_MISSION]
	MP_MISSION theMission = eNULL_MISSION
	INT numAvailableMissions = 0
	
	MP_MISSION_TYPE requiredMissionType = Get_MP_Mission_Mission_Type(paramArrayPos)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........List of MP Missions of Required Type: ")
		NET_PRINT(Convert_Mission_Type_To_String(requiredMissionType))
		NET_NL()
	#ENDIF
	
	// Set the initial state of the arrays
	//	availableMissionsMP = TRUE if the mission is of the correct type, otherwise FALSE
	//	bitsFreeTeamsForMissionsMP = ALL BITS CLEAR
	INT tempLoop = 0
	REPEAT eMAX_NUM_MP_MISSION tempLoop
		// Clear 'free teams' bits
		bitsFreeTeamsForMissionsMP[tempLoop] = ALL_MISSION_CONTROL_BITS_CLEAR
		
		// Set the initial 'available mission' state
		theMission = INT_TO_ENUM(MP_MISSION, tempLoop)
		IF (GET_MP_MISSION_TYPE(theMission) = requiredMissionType)
			// ...mission of the correct type
			availableMissionsMP[tempLoop] = TRUE
			numAvailableMissions++
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("           ")
				NET_PRINT(GET_MP_MISSION_NAME(theMission))
				NET_NL()
			#ENDIF
		ELSE
			// ...mission is of the wrong type
			availableMissionsMP[tempLoop] = FALSE
		ENDIF
	ENDREPEAT
	
	// Error Checking: Ensure there are missions of the correct type
	IF (numAvailableMissions = 0)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           FAILED: there are no missions of the correct type.") NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF

	// Output the number of missions available after this initial check
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Number Of Missions Initially Available ")
		NET_PRINT_INT(numAvailableMissions)
		NET_NL()
	#ENDIF
	
	// Go through all remaining missions and exclude any that don't meet the necessary play conditions
	INT thisMissionsFreeTeams = ALL_MISSION_CONTROL_BITS_CLEAR
	REPEAT eMAX_NUM_MP_MISSION tempLoop
		IF (availableMissionsMP[tempLoop])
			theMission = INT_TO_ENUM(MP_MISSION, tempLoop)
			
			// ...this mission is currently available, so check if there are any exclusions
			// KGM 28/8/12: I don't think anything calls this 'random pick' routine anymore, so I'm going to assume the requirement is for a mission with players in teams so I can make minimal changes
			IF NOT (Check_If_Players_Are_In_Teams_For_Mission(theMission))
				// ...exclude this mission - it's not team-based
				availableMissionsMP[tempLoop] = FALSE
				numAvailableMissions--
			ELSE
				// ...this mission is team-based
				// Check for any other availablilty exclusions
				IF NOT (Check_If_MP_Mission_Is_Available_To_Launch(theMission, paramArrayPos, paramInfo, thisMissionsFreeTeams))
					// ...an exclusion must exist
					availableMissionsMP[tempLoop] = FALSE
					numAvailableMissions--
				ELSE
					// ...mission is still open for selection, so store the Free Teams for this mission so that we don't need to re-calculate it
					bitsFreeTeamsForMissionsMP[tempLoop] = thisMissionsFreeTeams
				ENDIF
			ENDIF
		ENDIF
		
		// Ensure there are still available missions left, quit if all previously available missions are now excluded
		IF (numAvailableMissions = 0)
			#IF IS_DEBUG_BUILD
				NET_PRINT("           FAILED: All Available Missions have been excluded.") NET_NL()
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDREPEAT

	// Randomly choose one of the available missions
	INT chosenMissionPosition = GET_RANDOM_INT_IN_RANGE(0, numAvailableMissions)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Number Of Missions Left To Choose From After Exclusions : ")
		NET_PRINT_INT(numAvailableMissions)
		NET_NL()
		
		REPEAT eMAX_NUM_MP_MISSION tempLoop
			IF (availableMissionsMP[tempLoop])
				theMission = INT_TO_ENUM(MP_MISSION, tempLoop)
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("           ")
					NET_PRINT(GET_MP_MISSION_NAME(theMission))
					NET_NL()
				#ENDIF
			ENDIF
		ENDREPEAT
		
		NET_PRINT("      Choosing Mission At This Position In List: ")
		NET_PRINT_INT(chosenMissionPosition)
		NET_NL()
		
		INT holdRandomValueInCaseOfError = chosenMissionPosition
	#ENDIF
	
	// From the missions still available, find the one in the chosenMission position
	REPEAT eMAX_NUM_MP_MISSION tempLoop
		IF (availableMissionsMP[tempLoop])
			IF (chosenMissionPosition = 0)
				// Found the required mission, so store it
				theMission = INT_TO_ENUM(MP_MISSION, tempLoop)
				Store_MP_Mission_MissionID_On_MissionData(paramArrayPos, theMission)
				
				// Store the Available Teams for this Mission
				NET_PRINT("      Storing all Free Teams that can join this mission in the Joinable Teams bitfield.") NET_NL()
				Replace_MP_Mission_Request_Joinable_Teams_Bitfield(paramArrayPos, bitsFreeTeamsForMissionsMP[tempLoop])
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("      Randomly Picked This Mission [")
					NET_PRINT(GET_MP_MISSION_NAME(theMission))
					NET_PRINT("] of type: ")
					NET_PRINT(Convert_Mission_Type_To_String(GET_MP_MISSION_TYPE(theMission)))
					NET_NL()
					
					NET_PRINT("      Teams available to play this mission: ") NET_NL()
					Debug_Output_Team_Names_From_Bitfield(bitsFreeTeamsForMissionsMP[tempLoop])
				#ENDIF
	
				RETURN TRUE
			ENDIF
			
			// Not this mission
			chosenMissionPosition--
		ENDIF
	ENDREPEAT
	
	// Shouldn't reach here - The mission is supposed to contain available missions, but the random number failed to be in the range of missions available
	#IF IS_DEBUG_BUILD
		INT tempErrorCheckNumMissionsAvailable = 0
		
		NET_PRINT("...KGM MP [MControl]: Choose_MP_Mission_For_MP_Mission_Request_Slot() - INCONSISTENCY: There are supposed to be missions available, but one wasn't found.") NET_NL()
		NET_PRINT("...........Outputting mission selection details used to choose mission") NET_NL()
		REPEAT eMAX_NUM_MP_MISSION tempLoop
			NET_PRINT("           ")
			IF (availableMissionsMP[tempLoop])
				NET_PRINT("[AVAILABLE]  : ")
				tempErrorCheckNumMissionsAvailable++
			ELSE
				NET_PRINT("not available: ")
			ENDIF
			theMission = INT_TO_ENUM(MP_MISSION, tempLoop)
			NET_PRINT(GET_MP_MISSION_NAME(theMission))
			NET_NL()
		ENDREPEAT
		NET_NL()
		
		NET_PRINT("           ")
		NET_PRINT("Number Of Missions Thought To Be Available: ")
		NET_PRINT_INT(numAvailableMissions)
		NET_NL()
		
		NET_PRINT("           ")
		NET_PRINT("Number Of Missions Actually Available     : ")
		NET_PRINT_INT(tempErrorCheckNumMissionsAvailable)
		IF NOT (numAvailableMissions = tempErrorCheckNumMissionsAvailable)
			NET_PRINT("   **** MISMATCH ****")
		ENDIF
		NET_NL()
		
		NET_PRINT("           ")
		NET_PRINT("Randomly Chosen Position within Available Missions (NOT array position): ")
		NET_PRINT_INT(holdRandomValueInCaseOfError)
		NET_NL()
	#ENDIF
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Generate a unique Instance ID for this mission request
//
// INPUT PARAMS:		paramArrayPos			Array Position containing mission to be checked
// RETURN VALUE:		BOOL					TRUE if a unique instanceID was selected, otherwise FALSE
FUNC BOOL Generate_Unique_InstanceID_For_MP_Mission_Request_Slot(INT paramArrayPos)

	// Use the ArraySlot as the Instance ID for all networked missions - ensures Instance ID is unique for a second instance of a mission.
	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.iInstanceId = paramArrayPos

	// This shouldn't be a duplicate - but make sure
	IF (NETWORK_IS_SCRIPT_ACTIVE(GET_MP_MISSION_NAME(Get_MP_Mission_Request_MissionID(paramArrayPos)), GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.iInstanceId))
		#IF IS_DEBUG_BUILD
			NET_PRINT("           FAILED: ")
			NET_PRINT(GET_MP_MISSION_NAME(Get_MP_Mission_Request_MissionID(paramArrayPos)))
			NET_PRINT(" [Inst = ")
			NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.iInstanceId)
			NET_PRINT("] already exists.")
			NET_NL()
		#ENDIF
	ENDIF

	// All good
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Choose a random location from the available variations
//
// INPUT PARAMS:		paramArrayPos			Array Position containing mission to be checked
// RETURN VALUE:		BOOL					TRUE if the variation details were stored, otherwise FALSE
FUNC BOOL Choose_Random_Location_From_Variations_For_MP_Mission_Request_Slot(INT paramArrayPos)
	
	// Are there any locations stored?
	INT numMissionVariations = MPGlobals.g_sStartLocations.numLocations
	IF (numMissionVariations = 0)
		GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idVariation = NO_MISSION_VARIATION
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("      THERE ARE NO VARIATIONS TO CHOOSE FROM.") NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Randomly choose a Mission Location and store it.
	// Need to take Mission Instance Restrictions into account - the mission may not allow multiple instances of the same variation to be active.
	// 		I'll handle this by creating a list of all the variations then randomly swapping entries around to create a random order. I'll then
	//		gather a bitfield representing all variations of the mission currently active. I'll then work through the randomised list of variations
	//		from start to end looking for the first variation that isn't already active and use that.
	
	// Get the instance restriction
	MP_MISSION thisMissionID = Get_MP_Mission_Request_MissionID(paramArrayPos)
	g_eMPInstances instanceRestriction	= Get_MP_Mission_Instance_Restrictions(thisMissionID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Choose Random Variation - Mission Instance Restriction: ")
		NET_PRINT(Convert_Instance_Restrictions_To_String(instanceRestriction))
		NET_PRINT(" (Number Of Variations Setup: ")
		NET_PRINT_INT(numMissionVariations)
		NET_PRINT(")")
		NET_NL()
	#ENDIF
	
	// Do we need to worry about the already active mission instances?
	// ...the instance ID should always be setup, but just in case
	IF (instanceRestriction = NO_MP_INST)
		instanceRestriction = MP_INST_ONE
	ENDIF
	
	// If we don't need to worry about instance restrictions then just choose a random variation from the available restrictions
	IF (instanceRestriction = MP_INST_ANY)
		INT nRandomInt = GET_RANDOM_INT_IN_RANGE(0, numMissionVariations)
		GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idVariation = nRandomInt
		
		RETURN TRUE
	ENDIF
	
	// Create a bitfield of mission variations already in progress if mission instance restrictions exist
	INT tempLoop = 0
	INT activeVariationsBitfield = ALL_MISSION_CONTROL_BITS_CLEAR
	
	MP_MISSION activeMission = eNULL_MISSION
	INT activeVariation = NO_MISSION_VARIATION
	
	// There are mission instance restrictions in place, so deal with them
	SWITCH (instanceRestriction)
		CASE MP_INST_ONE
		CASE MP_INST_UNIQUE
			// ...yes, so generate a bitfield of already active variations of the mission
			REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
				// Ignore the mission being setup
				IF NOT (paramArrayPos = tempLoop)
					SWITCH (Get_MP_Mission_Request_Slot_Status(tempLoop))
						// Check these missions to see if they are duplicate variations
						CASE MP_MISSION_STATE_ACTIVE
						CASE MP_MISSION_STATE_ACTIVE_SECONDARY
						CASE MP_MISSION_STATE_OFFERED
							activeMission	= Get_MP_Mission_Request_MissionID(tempLoop)
							
							// Is this the same mission as the one trying to launch?
							IF (thisMissionID = activeMission)
								// ...yes, but if this mission only allows one instance then another instance definitely isn't allowed to run
								IF (instanceRestriction = MP_INST_ONE)
									// An existing instance of this mission is already active
									#IF IS_DEBUG_BUILD
										NET_PRINT("           REJECTED: ")
										NET_PRINT(GET_MP_MISSION_NAME(activeMission))
										NET_PRINT(" is already active and only one instance of the mission is allowed")
										NET_NL()
									#ENDIF
									
									MP_Mission_Request_Failure_Reason(paramArrayPos, FTLR_MISSION_INSTANCE_ACTIVE)
									RETURN FALSE
								ENDIF

								// ...if there is only one mission variation setup for the mission then we can't launch another instance because it will be definitely be the same variation
								IF (numMissionVariations = 1)
									// An existing instance of this mission is already active
									#IF IS_DEBUG_BUILD
										NET_PRINT("           REJECTED: ")
										NET_PRINT(GET_MP_MISSION_NAME(activeMission))
										NET_PRINT(" is already active and only one variation of the mission has been setup")
										NET_NL()
									#ENDIF
									
									MP_Mission_Request_Failure_Reason(paramArrayPos, FTLR_MISSION_INSTANCE_ACTIVE)
									RETURN FALSE
								ENDIF
								
								// There is more than one variation setup for this mission, so add this variation to the bitfield
								activeVariation	= Get_MP_Mission_Request_MissionVariation(tempLoop)
								
								IF (activeVariation >= 0)
								AND (activeVariation <= 31)
									// ...valid variation number - so store it in the bitfield
									SET_BIT(activeVariationsBitfield, activeVariation)
								ELSE
									// ...invalid variation, so output details
									#IF IS_DEBUG_BUILD
										IF (activeVariation > 31)
											NET_PRINT("...KGM MP [MControl]: Variation value too big for bitfield: ") NET_PRINT_INT(activeVariation) NET_PRINT("  for Mission: ") NET_PRINT(GET_MP_MISSION_NAME(thisMissionID)) NET_NL()
											SCRIPT_ASSERT("Choose_Random_Location_From_Variations_For_MP_Mission_Request_Slot(): Variation greater than 31 so won't fit in a bitfield. Look at console log. Tell Keith.")
										ENDIF
										
										IF (activeVariation < 0)
											// This may not be an error, it may just be something I need to deal with. Assume an error for now.
											NET_PRINT("...KGM MP [MControl]: Variation value is negative: ") NET_PRINT_INT(activeVariation) NET_PRINT("  for Mission: ") NET_PRINT(GET_MP_MISSION_NAME(thisMissionID)) NET_NL()
											SCRIPT_ASSERT("Choose_Random_Location_From_Variations_For_MP_Mission_Request_Slot(): Variation is less than 0. Look at console log. Tell Keith.")
										ENDIF
									#ENDIF
								ENDIF
							ENDIF
							BREAK
							
						// Ignore these
						CASE MP_MISSION_STATE_RECEIVED
						CASE MP_MISSION_STATE_RESERVED
						CASE NO_MISSION_REQUEST_RECEIVED
							BREAK
							
						DEFAULT
							NET_PRINT("...KGM MP [MControl]: Choose_Random_Location_From_Variations_For_MP_Mission_Request_Slot() needs a new mission state added to the SWITCH statement.") NET_NL()
							SCRIPT_ASSERT("Choose_Random_Location_From_Variations_For_MP_Mission_Request_Slot() needs a new mission state added to the SWITCH statement. Tell Keith")
							BREAK
					ENDSWITCH
				ENDIF
			ENDREPEAT
			BREAK
			
		// Ignore these
		CASE MP_INST_ANY
		CASE NO_MP_INST
			BREAK
			
		DEFAULT
			NET_PRINT("...KGM MP [MControl]: Choose_Random_Location_From_Variations_For_MP_Mission_Request_Slot() needs a new instance restriction ID added to the SWITCH statement.") NET_NL()
			SCRIPT_ASSERT("Choose_Random_Location_From_Variations_For_MP_Mission_Request_Slot() needs a new instance restriction ID added to the SWITCH statement. Tell Keith")
			BREAK
	ENDSWITCH
	
	// Generate a random order for choosing the variations
	INT randomOrder[MAX_LOCATIONS_FOR_ONE_MISSION]
	
	// Set up the initial array with variations in ascending order
	REPEAT MAX_LOCATIONS_FOR_ONE_MISSION tempLoop
		IF (tempLoop < numMissionVariations)
			randomOrder[tempLoop] = tempLoop
		ELSE
			randomOrder[tempLoop] = -1
		ENDIF
	ENDREPEAT
	
	// If there is more than one location then randomise the order (using a basic array position swap algorithm)
	IF (numMissionVariations > 1)
		INT numChanges = (numMissionVariations * 2)
		INT theTo = 0
		INT theFrom = 0
		INT tempHolder = 0
		
		REPEAT numChanges tempLoop
			theFrom	= GET_RANDOM_INT_IN_RANGE(0, numMissionVariations)
			theTo	= GET_RANDOM_INT_IN_RANGE(0, numMissionVariations)
			
			// If the two random array positions are different then swap the order
			IF NOT (theTo = theFrom)
				tempHolder = randomOrder[theTo]
				randomOrder[theTo] = randomOrder[theFrom]
				randomOrder[theFrom] = tempHolder
			ENDIF
		ENDREPEAT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Randomised Variation Order:")
		NET_NL()
		
		REPEAT numMissionVariations tempLoop
			INT thisVariation = randomOrder[tempLoop]
			NET_PRINT("           ") NET_PRINT_INT(thisVariation)
			IF (IS_BIT_SET(activeVariationsBitfield, thisVariation))
				NET_PRINT(" - ALREADY IN USE")
			ELSE
				NET_PRINT(" - free")
			ENDIF
			NET_NL()
		ENDREPEAT
	#ENDIF

	// Find the first variation in the random array that isn't already an active mission variation
	INT thisVariation = 0
	REPEAT numMissionVariations tempLoop
		thisVariation = randomOrder[tempLoop]
		IF NOT (IS_BIT_SET(activeVariationsBitfield, thisVariation))
			// ...this variation is good to go
			GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idVariation = thisVariation
			
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	// Failed to find a useable variation
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Choose_Random_Location_From_Variations_For_MP_Mission_Request_Slot(): Didn't find a mission variation to use for the mission that obeyed the Instance Restrictions.")
		NET_NL()
	#ENDIF
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Choose a fair location from the available variations using Rowan's Fair Spawn header
//
// INPUT PARAMS:		paramArrayPos			Array Position containing mission to be checked
// RETURN VALUE:		BOOL					TRUE if the variation details were stored, otherwise FALSE
FUNC BOOL Choose_Fair_Location_From_Variations_For_MP_Mission_Request_Slot(INT paramArrayPos)
	
	// KGM 5/3/13: We need to separate out variations to into separate 'variations' and 'locations' values
	
	// Are there any locations stored?
	IF (MPGlobals.g_sStartLocations.numLocations = 0)
		GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idVariation = NO_MISSION_VARIATION
		RETURN FALSE
	ENDIF
	
	// Cal the choose random location fucntions
	RETURN (Choose_Random_Location_From_Variations_For_MP_Mission_Request_Slot(paramArrayPos))
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Choose a fair location using Rowan's Fair Spawn header
//
// INPUT PARAMS:		paramArrayPos			Array Position containing mission to be checked
// RETURN VALUE:		BOOL					TRUE if the variation details were stored, otherwise FALSE
FUNC BOOL Choose_Fair_Location_For_MP_Mission_Request_Slot(INT paramArrayPos)
	
	// KGM NOTES 22/5/12: In order to avoid the cyclic header issues when trying to implement the routines within net_fair_spawn.sch
	//				I'm going to copy Rowan's functionality here. The only initial difference is that I'll use a team array.
	
//	m_structFSTeamSpawn	sFSTeam[MAX_NUM_TEAMS]	// Used to gather team-based information for players
	m_structFSSpawn		sFSPlayers				// Used for player information (only used for decision-making for missions that aren't team-based)
	
	// Generate the fair spawn location - this bit is a modified copy of Rowan's GET_FAIR_MISSION_SPAWN_POSITION routine
	PLAYER_INDEX	thisPlayer
	VECTOR			centralPos			= << 0.0, 0.0, 0.0 >>
	INT				playerLoop			= 0
	INT				playerTeam			= TEAM_INVALID
	INT				bitsReservedPlayers = Get_MP_Mission_Request_Reserved_Players(paramArrayPos)
	BOOL			playersAreInTeams	= Check_If_Players_Are_In_Teams_For_Mission(Get_MP_Mission_Request_MissionID(paramArrayPos))
	
	// Calculate the central coordinates
	REPEAT NUM_NETWORK_PLAYERS playerLoop
		IF (IS_BIT_SET(bitsReservedPlayers, playerLoop))
			// ...this player is reserved for the mission, so include it in the fair spawn calculation
			thisPlayer = INT_TO_PLAYERINDEX(playerLoop)
			IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
				centralPos += GET_ENTITY_COORDS(GET_PLAYER_PED(thisPlayer), FALSE)
				
				// Is the mission team-based?
				IF (playersAreInTeams)
					// ...team-based, so gather team details
					playerTeam = GET_PLAYER_TEAM(thisPlayer)
					IF NOT (playerTeam = TEAM_INVALID)
						//sFSTeam[playerTeam].spawnInfo.numPlayers = sFSTeam[playerTeam].spawnInfo.numPlayers + 1
						
						sFSPlayers.numPlayers++
						
//						IF NOT (sFSTeam[playerTeam].useTeam)
//							// ...first valid team player generates safehouse location
//							sFSTeam[playerTeam].teamSafehouse = GET_ENTITY_COORDS(GET_PLAYER_PED(thisPlayer))		// KGM 5/3/13: Replacement since there are currently no team safehouses
//						ENDIF
						
//						sFSTeam[playerTeam].useTeam		= TRUE
		
						#IF IS_DEBUG_BUILD
							NET_PRINT("         ")
							NET_PRINT(GET_PLAYER_NAME(thisPlayer))
							NET_PRINT("  [") NET_PRINT(GET_STRING_FROM_TEXT_FILE(GET_TEAM_NAME(playerTeam))) NET_PRINT("]")
							NET_PRINT("   ") NET_PRINT_VECTOR(GET_ENTITY_COORDS(GET_PLAYER_PED(thisPlayer), FALSE))
							NET_NL()
						#ENDIF
					ENDIF
				ELSE
					// ...not team-based, so just gather player details
					sFSPlayers.numPlayers++
		
					#IF IS_DEBUG_BUILD
						NET_PRINT("         ")
						NET_PRINT(GET_PLAYER_NAME(thisPlayer))
						NET_PRINT("  [") NET_PRINT(GET_STRING_FROM_TEXT_FILE(GET_TEAM_NAME(playerTeam))) NET_PRINT("]")
						NET_PRINT("   ") NET_PRINT_VECTOR(GET_ENTITY_COORDS(GET_PLAYER_PED(thisPlayer), FALSE))
						NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	centralPos = centralPos / TO_FLOAT(sFSPlayers.numPlayers)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("         CENTRAL POSITION  ")
		NET_PRINT_VECTOR(centralPos)
		NET_NL()
	#ENDIF
	
	// Check the 12 points on the circumference of a circle to get a fair spawn location
	CONST_INT	RADIUS_CHECK_POINTS		12
	CONST_INT	DEGREES_IN_CIRCLE		360
	CONST_FLOAT	DISTANCE_FROM_CENTRE_m	500.0		// NOTE: May need to store this in net_mission_info
	CONST_FLOAT	SAFEHOUSE_EXCLUSION_m	200.0
	
	INT			anglePerSegment			= DEGREES_IN_CIRCLE / RADIUS_CHECK_POINTS
	VECTOR		offsetPos				= << 0.0, 0.0, 0.0 >>
	VECTOR		playerPos				= << 0.0, 0.0, 0.0 >>
	INT			testRadiusLoop			= 0
	//INT			teamLoop				= 0
	//INT			firstTeam				= 0
	//INT			secondTeam				= 0
	FLOAT		checkDistance			= 0.0
	FLOAT		smallestDistance		= 99999.9
	FLOAT		playerDistance			= 0.0
	VECTOR		fairPos					= << 0.0, 0.0, 0.0 >>
	BOOL		excludeThisOffset		= FALSE
		
	REPEAT RADIUS_CHECK_POINTS testRadiusLoop
		offsetPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(centralPos, 0.0, << (-1.0 * DISTANCE_FROM_CENTRE_m * SIN(TO_FLOAT(testRadiusLoop) * anglePerSegment)), (DISTANCE_FROM_CENTRE_m * COS(TO_FLOAT(testRadiusLoop) * anglePerSegment)), 0.0 >>)
		
//		// Reset the variable team data used as storage at each test position
//		REPEAT MAX_NUM_TEAMS teamLoop
//			sFSTeam[teamLoop].spawnInfo.distFromOffset	= 0.0000
//			sFSTeam[teamLoop].spawnInfo.avgDistance		= 0.0000
//		ENDREPEAT
		
		// Reset the variable player data used as storage when the mission is not team-based
		sFSPlayers.distFromOffset	= 0.0000
		sFSPlayers.avgDistance		= 0.0000
		
		// Get distance of all players against this offset position
		REPEAT NUM_NETWORK_PLAYERS playerLoop
			// Get the distance between each player and the offset position and update a team distance
			IF (IS_BIT_SET(bitsReservedPlayers, playerLoop))
				// ...this player is reserved for the mission, so include in the fair spawn calculation
				thisPlayer		= INT_TO_PLAYERINDEX(playerLoop)
				playerPos		= GET_ENTITY_COORDS(GET_PLAYER_PED(thisPlayer), FALSE)
				playerDistance	= GET_DISTANCE_BETWEEN_COORDS(offsetPos, playerPos, FALSE)
				
				IF (playersAreInTeams)
					// ...gather team information for team-based missions
					playerTeam = GET_PLAYER_TEAM(thisPlayer)
					IF NOT (playerTeam = TEAM_INVALID)
						//sFSTeam[playerTeam].spawnInfo.distFromOffset += playerDistance
					ENDIF
				ELSE
					// ...gather player information if the mission isn't team-based
					sFSPlayers.distFromOffset += playerDistance
				ENDIF
			ENDIF
		ENDREPEAT
		
		// Generate the average distance from the offset position per player (for each team if team-based)
		IF (playersAreInTeams)
//			REPEAT MAX_NUM_TEAMS teamLoop
//				IF (sFSTeam[teamLoop].useTeam)
//					sFSTeam[teamLoop].spawnInfo.avgDistance = sFSTeam[teamLoop].spawnInfo.distFromOffset / sFSTeam[teamLoop].spawnInfo.numPlayers
//				ENDIF
//			ENDREPEAT
		ELSE
			sFSPlayers.avgDistance = sFSPlayers.distFromOffset / sFSPlayers.numPlayers
		ENDIF
		
		// If team-based, sum the distances from the offset position of all the teams involved (ie: if there were 4 teams: 0v1, 0v2, 0v3, 1v2 1v3, 2v3)
		checkDistance = 0.0
		IF (playersAreInTeams)
//			// ...players are in teams, so gather a check distance from this offset that takes team distances into account
//			REPEAT MAX_NUM_TEAMS firstTeam
//				IF (sFSTeam[firstTeam].useTeam)
//					secondTeam = firstTeam + 1
//					WHILE (secondTeam < MAX_NUM_TEAMS)
//						IF (sFSTeam[secondTeam].useTeam)
//							checkDistance += ABSF(sFSTeam[firstTeam].spawnInfo.avgDistance - sFSTeam[secondTeam].spawnInfo.avgDistance)
//						ENDIF
//						
//						secondTeam++
//					ENDWHILE
//				ENDIF
//			ENDREPEAT
		ELSE
			// ...players are not in teams, so the check distance is just the avgDistance calculated already for all players
			checkDistance = sFSPlayers.avgDistance
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("         Offset: ")
			NET_PRINT_INT(testRadiusLoop)
			NET_PRINT(" ")
			NET_PRINT_VECTOR(offsetPos)
			NET_PRINT(" Sum of distances from offset: ")
			NET_PRINT_FLOAT(checkDistance)
			IF (playersAreInTeams)
				NET_PRINT("  - NOTE: This will be 0 if only one team")
			ENDIF
			NET_NL()
		#ENDIF
		
		// If the distance from this test position is the smallest distance overall then use it unless it's too close to a ganghouse
		IF (checkDistance < smallestDistance)
			// ...this is the best position so far, so make sure it isn't too close to a ganghouse for team-based missions
			#IF IS_DEBUG_BUILD
				NET_PRINT("         ---> This offset is best so far")
			#ENDIF
	
			excludeThisOffset = FALSE
			
			IF (playersAreInTeams)
//				REPEAT MAX_NUM_TEAMS teamLoop
//					IF NOT (excludeThisOffset)
//						IF (sFSTeam[teamLoop].useTeam)
//							IF (GET_DISTANCE_BETWEEN_COORDS(offsetPos, sFSTeam[teamLoop].teamSafehouse) < SAFEHOUSE_EXCLUSION_m)
//								excludeThisOffset = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDREPEAT
				
				#IF IS_DEBUG_BUILD
					IF (excludeThisOffset)
						NET_PRINT(" - BUT TOO CLOSE TO AN ACTIVE SAFEHOUSE")
					ENDIF
				#ENDIF
			ENDIF
			
			IF NOT (excludeThisOffset)
				// ...not too close to a safehouse so update the closest distance and best offset vector
				smallestDistance	= checkDistance
				fairPos				= offsetPos
				
				#IF IS_DEBUG_BUILD
					NET_PRINT(" - CURRENT BEST POSITION ")
					NET_PRINT_VECTOR(fairPos)
					NET_PRINT(" (distance = ")
					NET_PRINT_FLOAT(smallestDistance)
					NET_PRINT(")")
				#ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				NET_NL()
			#ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("         Smallest Distance From Offset: ")
		NET_PRINT_FLOAT(smallestDistance)
		NET_PRINT("  [offset: ")
		NET_PRINT_VECTOR(fairPos)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
	// Store the details
	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idVariation	= MISSION_VARIATION_USE_VECTOR
	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdPrimaryCoords		= fairPos
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: TEMP TO ALLOW SOME MISSION SPECIFIC WORKAROUNDS UNTIL THE MISSION DATA IS SORTED OUT
PROC TEMP_Fill_CrimLocation_For_NO_LOCATION_Workarounds(INT paramArrayPos)

	UNUSED_PARAMETER(paramArrayPos)

//	// CleanArea uses an inclusion radius so only players within range get put on the mission, so I needed to add mission variations to get the safehouse vectors
//	// KGM 29/8/12: Because this is a temp workaround I'm going to leave the team checks unchanged because these missions are not active in Freemode
//	IF (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idMission = eAM_CR_CLEANAREA)
//		IF (Is_Team_Reserved_For_Mission(paramArrayPos, TEAM_CRIM_GANG_1))
//			// Vagos
//			GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idVariation = 0
//			#IF IS_DEBUG_BUILD
//				NET_PRINT("      TEMP WORKAROUND: Choose CLEAN AREA VAGOS Variation: ")
//			#ENDIF
//		ELSE
//			// Lost
//			GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idVariation = 1
//			#IF IS_DEBUG_BUILD
//				NET_PRINT("      TEMP WORKAROUND: Choose CLEAN AREA LOST Variation: ")
//			#ENDIF
//		ENDIF
//		
//		#IF IS_DEBUG_BUILD
//			NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idVariation)
//			NET_NL()
//		#ENDIF
//		
//		EXIT
//	ENDIF
//	// End CleanArea workaround

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Choose a unique variation of this mission, and fill the Crooks Location and Cops Patrol Location
//
// INPUT PARAMS:		paramArrayPos			Array Position containing mission to be checked
// RETURN VALUE:		BOOL					TRUE if the variation details were selected, otherwise FALSE
FUNC BOOL Choose_Variation_For_MP_Mission_Request_Slot(INT paramArrayPos)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Choose_Variation_For_MP_Mission_Request_Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
	#ENDIF

	// Fill the Mission Location Globals
	MP_MISSION theMission = Get_MP_Mission_Request_MissionID(paramArrayPos)
	Fill_MP_Potential_Mission_Locations(theMission)
	
	
	// *** IT DOESN'T MAKE SENSE TO WORK OUT THE VARIATION OR FAIR SPAWN LOCATION UNTIL AT LEAST ONE PLAYER ACCEPTS THE MISSION
	//		DOING THIS WORK TOO EARLY JUST MEANS THE GENERATED POSITION MAY BE OUT OF DATE WHEN IT NEEDS TO BE USED
	

	// Choose a Variation (and fill the Mission Location and the Patrol Location)
	IF (Get_MP_Mission_Request_MissionVariation(paramArrayPos) = NO_MISSION_VARIATION)
		// KGM NOTE 17/7/12: As part of the mission data re-structure, the location selection method is going to have to take the variation as a parameter.
		//					I've delayed doing this because it's just going to cause problems until we treat the variations and different locations as
		//					separate pieces of data - currently they're all mashed up and treated as 'variations'. Once we have separated that data, then
		//					the location selection method will need to be stored 'per variation' so that, for example, the standard version of 'CarBlowUp'
		//					gets a 'random' location selection method, but the other version gets a 'fixed' location method. NOTE also that each variation
		//					may have to be setup with different locations, so it may be possible for both variations of a mission to use 'random selection'
		//					but for the choice of locations to be different per variation.
		g_eMPLocMethod theLocationMethod = Get_MP_Mission_Variation_Location_Selection_Method(theMission, NO_MISSION_VARIATION)
	
		#IF IS_DEBUG_BUILD
			NET_PRINT("      A specific mission variation has not been pre-selected, so using this Location Calculation Method: ")
			NET_PRINT(Convert_Location_Selection_Method_To_String(theLocationMethod))
			NET_NL()
		#ENDIF
		
		SWITCH (theLocationMethod)
			// Randomly Choose a Mission Location from the variations available
			CASE MP_LOC_RND_VAR
				IF NOT (Choose_Random_Location_From_Variations_For_MP_Mission_Request_Slot(paramArrayPos))
					RETURN FALSE
				ENDIF
				BREAK
				
			// Choose a Fair Location from the variations available
			CASE MP_LOC_FAIR_VAR
				IF NOT (Choose_Fair_Location_From_Variations_For_MP_Mission_Request_Slot(paramArrayPos))
					RETURN FALSE
				ENDIF
				BREAK
				
			// Choose a Fair Vector
			CASE MP_LOC_FAIR_XYZ
				IF NOT (Choose_Fair_Location_For_MP_Mission_Request_Slot(paramArrayPos))
					RETURN FALSE
				ENDIF
				BREAK
				
			// Special Cases - for now do nothing
			CASE MP_LOC_SPECIAL
				// REMOVE THIS WORKAROUND FUNCTION WHEN THE MISSION DATA IS SETUP CORRECTLY
				TEMP_Fill_CrimLocation_For_NO_LOCATION_Workarounds(paramArrayPos)
				BREAK
				
			// Unknown Location Method - for now do nothing, but may be able to assert
			CASE NO_MP_LOC
				BREAK
		ENDSWITCH
	ELSE
		#IF IS_DEBUG_BUILD
			NET_PRINT("           A specific mission variation has been pre-selected: ")
			NET_PRINT_INT(Get_MP_Mission_Request_MissionVariation(paramArrayPos))
			NET_NL()
		#ENDIF
	ENDIF
	
	// If there is no mission variation set up then assume the Mission Location vector has already been filled
	INT theVariation = Get_MP_Mission_Request_MissionVariation(paramArrayPos)
	
	BOOL variationIsUsedForDifferentMissionLocations = TRUE
	IF (theVariation >= MPGlobals.g_sStartLocations.numLocations)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: ERROR: Mission Variation is greater than the number of locations set up for the mission.") NET_NL()
			NET_PRINT("           theVariation: ") NET_PRINT_INT(theVariation)
			NET_PRINT("  numVariations: ") NET_PRINT_INT(MPGlobals.g_sStartLocations.numLocations)
			NET_PRINT("  [MISSION = ") NET_PRINT(GET_MP_MISSION_NAME(theMission)) NET_PRINT("]")
			NET_NL()
// TEMP: EXTRA CONSOLE LOG OUTPUT
			NET_PRINT("           (NOTE: This should be an Assert but at the moment 'variations' are being used for both 'location choice' or other variations, like 'vehicle type'")
			NET_NL()
			NET_PRINT("           Just letting this variation pass through 'as-is')")
			NET_NL()
// TEMP: DON'T ASSERT: We need to introduce 'locations' and 'variations' as two separate things - currently variations are being used for both
//			SCRIPT_ASSERT("Choose_Variation_For_MP_Mission_Request_Slot() - Variation is greater than the num start locations for the mission. Using variation 0. Look at console log. Tell Keith.")
		#ENDIF

// TEMP: Use this while 'variations' refer to locations only - allows the variation number passed in to be forwarded unchanged so it can be used by the mission script
		variationIsUsedForDifferentMissionLocations = FALSE
// TEMP: Use this again when the variations are sorted out and split int 'locations' and 'variations'		
//		theVariation = 0
	ENDIF
	
	IF NOT (theVariation = NO_MISSION_VARIATION)
	AND NOT (theVariation < 0)							// TEMP CONDITION while we have negative variations
	AND (variationIsUsedForDifferentMissionLocations)	// TEMP FLAG ensures the location is only stored if the variation represents a different location
		// Fill the Criminal Location
		GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdPrimaryCoords = MPGlobals.g_sStartLocations.missionLocations[theVariation]
	ENDIF
	
	// Generate an exclusion zone around an offset from the mission location
	CONST_FLOAT	EXCLUSION_ZONE_m		50.0		// A 'dead zone' at the mission location
	CONST_FLOAT ZONE_MAX_RADIUS_m		150.0		// The absolute max radius for a nearby offset
	FLOAT exclusionRange = ZONE_MAX_RADIUS_m - EXCLUSION_ZONE_m + 1.0
	
	FLOAT xOffset = EXCLUSION_ZONE_m + GET_RANDOM_FLOAT_IN_RANGE(0.0, exclusionRange)
	IF (GET_RANDOM_INT_IN_RANGE(0, 100) < 50)
		xOffset *= -1.0
	ENDIF
	
	FLOAT yOffset = EXCLUSION_ZONE_m + GET_RANDOM_FLOAT_IN_RANGE(0.0, exclusionRange)
	IF (GET_RANDOM_INT_IN_RANGE(0, 100) < 50)
		yOffset *= -1.0
	ENDIF
	
	VECTOR missionLocation = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdPrimaryCoords
	missionLocation.x += xOffset
	missionLocation.y += yOffset
	
	GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdSecondaryCoords = missionLocation
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Filled the Variation and Location Data for Mission Request Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
		Debug_Output_Full_Contents_Of_MP_Mission_Data_To_Console_Log(Get_MP_Mission_Request_Slot_Mission_Data(paramArrayPos))
	#ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Fill out the MP_MISSION_DATA for this mission request
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be filled
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
// RETURN VALUE:		BOOL					TRUE if all the required mission details were generated, otherwise FALSE
//
// NOTES:	Doesn't include Mission Variation and Location - this will be decided once the reserved teams have been selected.
FUNC BOOL Fill_Out_MP_Mission_Request_Slot_Details(INT paramArrayPos, m_structPlayerAndTeamInfoMC &paramInfo)

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Filling Out Mission Details in slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
	#ENDIF

	IF (Has_A_Specific_MP_Mission_Been_Requested(paramArrayPos))
		// Ensure the specified mission is available to trigger
		MP_MISSION theMission = Get_MP_Mission_Request_MissionID(paramArrayPos)
		INT availableTeams = ALL_MISSION_CONTROL_BITS_CLEAR
		IF NOT (Check_If_MP_Mission_Is_Available_To_Launch(theMission, paramArrayPos, paramInfo, availableTeams))
			RETURN FALSE
		ELSE
			// ...mission is available, so store the already calculated available teams so that the player reservation routine doesn't need to re-calculate it
			Replace_MP_Mission_Request_Joinable_Teams_Bitfield(paramArrayPos, availableTeams)
		ENDIF
	ELSE
		// Choose the mission
		IF NOT (Choose_MP_Mission_For_MP_Mission_Request_Slot(paramArrayPos, paramInfo))
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Generate a unique Instance ID
	IF NOT (Generate_Unique_InstanceID_For_MP_Mission_Request_Slot(paramArrayPos))
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Filled Out Mission Data for Mission Request Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
		NET_PRINT("           NOTE: If not pre-selected, Variation and Location details will be generated after the teams and players have been reserved.") NET_NL()
		Debug_Output_Full_Contents_Of_MP_Mission_Data_To_Console_Log(Get_MP_Mission_Request_Slot_Mission_Data(paramArrayPos))
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC




// ===========================================================================================================
//      Player Reservations for Missions
// ===========================================================================================================

// PURPOSE:	Usin ghte passed in parameters, check if the mission can still accept the offered team. If it can, reserve this team for this mission request.
//
// INPUT PARAMS:			paramArrayPos			Mission Request array position
//							paramTeam				Team ID to try to reserve for the mission
//							paramCopsNecessary		TRUE if cops are necessary for this mission
//							paramMaxCrooks			Max number of Crook Gangs allowed on mission, or TEAM_ILLEGAL to represent to ignore Crook Specific checks
//							paramMaxAnyTeams		Max number of Teams allowed on mission where the team can optionally be either Cop or Crook, or TEAM_ILLEGAL if not applicable
// RETURN PARAMS:			paramCopsOnMission		TRUE if Cops are on this mission
//							paramGangsOnMission		The Number of Crook Gangs on this mission
PROC Store_Team_If_Mission_Is_Not_Fully_Configured(INT paramArrayPos, INT paramTeam, BOOL paramCopsNecessary, INT paramMaxCrookGangs, INT paramMaxAnyTeams, BOOL &paramCopsOnMission, INT &paramGangsOnMission)

	INT teamsOnMission = paramGangsOnMission
	IF (paramCopsOnMission)
		teamsOnMission++
	ENDIF
	
	
	IF (paramCopsNecessary)
		// fool compiler
	ENDIF
	
//	BOOL thisIsCopTeam = FALSE
//	IF (paramTeam = TEAM_COP)
//		thisIsCopTeam = TRUE
//	ENDIF
	
	// If the mission accepts Any Teams (regardless of permutations of Cops and Crooks), then add the team to the mission if there is room
	IF NOT (paramMaxAnyTeams = TEAM_INVALID)
		IF (teamsOnMission < paramMaxAnyTeams)
			// ...there's room on the mission
			Store_MP_Mission_Request_Reserved_Team(paramArrayPos, paramTeam)
			
//			IF (thisIsCopTeam)
//				paramCopsOnMission = TRUE
//			ELSE
				paramGangsOnMission++
//			ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
//	// Check if the mission is capable of taking Cops
//	IF (thisIsCopTeam)
//		IF (paramCopsNecessary)
//			// ...there's room on the mission
//			Store_MP_Mission_Request_Reserved_Team(paramArrayPos, paramTeam)
//			paramCopsOnMission = TRUE
//		ENDIF
//		
//		EXIT
//	ENDIF
	
	// Store Crooks if the mission can still take more Crooks
	IF NOT (paramMaxCrookGangs = TEAM_INVALID)
		IF (paramGangsOnMission < paramMaxCrookGangs)
			// ...there's room on the mission
			Store_MP_Mission_Request_Reserved_Team(paramArrayPos, paramTeam)
			paramGangsOnMission++
		ENDIF
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Reserve the Teams for this mission
//
// INPUT PARAMS:		paramArrayPos			Array Position for the mission
// RETURN VALUE:		BOOL					TRUE if all the required mission details were generated, otherwise FALSE
//
// NOTES:	The 'joinable teams' bitfield should hold the already-calculated pool of teams available to join the mission
//				to prevent having to re-calculate this. This data is temporary and should be replaced by this function
//				with the teams selected to play the mission.
//
// NOTE:	Various checks have already taken place prior to this function.
//			Assume that the requiredTeams is a full subset of availableTeams.
//			A 'Required Team' is one that the mission requester specifically required to be on the mission.
FUNC BOOL Reserve_Teams_For_This_MP_Mission_Request(INT paramArrayPos)

	// Get the requested teams and the available teams
	INT availableTeamsBitfield	= Get_MP_Mission_Request_Joinable_Teams(paramArrayPos)
//	INT requestedTeamsBitfield	= Get_MP_Mission_Request_Requested_Teams(paramArrayPos)
	
	// Output some info to the console log
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Pool of Teams available to be reserved for mission") NET_NL()
		Debug_Output_Team_Names_From_Bitfield(availableTeamsBitfield)
		
		NET_PRINT("      Teams Requested To Be On This Mission")
//		IF (requestedTeamsBitfield = ALL_MISSION_CONTROL_BITS_CLEAR)
//			NET_PRINT(": NONE")
//		ENDIF
		NET_NL()
		//Debug_Output_Team_Names_From_Bitfield(requestedTeamsBitfield)
	#ENDIF
	
	// Clear the joinable teams bitfield which will be re-filled when the starting teams are being placed on the mission
	// Also clear the Reserved Teams bitfield ready for data
	// NOTE: Leave the Requested Tems bitfield as-is - this is used later for prioritising teams if a random choice is required
	Clear_MP_Mission_Request_Joinable_Teams_Bitfield(paramArrayPos)
	Clear_MP_Mission_Request_Reserved_Teams_Bitfield(paramArrayPos)
	
	// Create an array of INTS to hold Teams in a re-arranged order
	// First add all the Requested Teams to the list, then add any other available teams in random order
//	INT reorderedTeams[MAX_NUM_TEAMS]
	
	//INT arrayLoop = 0
	//INT storagePosition = 0
	
//	// Clear the reorderedTeams array
//	REPEAT MAX_NUM_TEAMS arrayLoop
//		reorderedTeams[arrayLoop] = TEAM_INVALID
//	ENDREPEAT

	// Add any Requested Teams in team order - they get first dibs at being added to a mission
	// NOTE: In theory there will be only one Requested Team at most - the mission requester may use it to try to get a specific team on the mission
//	REPEAT MAX_NUM_TEAMS arrayLoop
//		IF (IS_BIT_SET(requestedTeamsBitfield, arrayLoop))
//			// ...this is a requested team, so store it
//			reorderedTeams[storagePosition] = arrayLoop
//			storagePosition++
//			
//			// Update the local control bitfields
//			CLEAR_BIT(requestedTeamsBitfield, arrayLoop)
//			
//			// Sanity Check - the requestedTeams should be a full subset of the availableTeams, so the availableTeams bit should also be SET
//			#IF IS_DEBUG_BUILD
//				IF NOT (IS_BIT_SET(availableTeamsBitfield, arrayLoop))
//					NET_PRINT("...KGM MP [MControl]: A Requested Team was not on the Available Teams list: ") NET_PRINT(Convert_Team_To_String(arrayLoop)) NET_NL()
//					SCRIPT_ASSERT("Reserve_Teams_For_This_MP_Mission_Request() - Inconsistency with Team Information. Should be ignorable. See Console Log. Tell Keith.")
//				ENDIF
//			#ENDIF
//			
//			CLEAR_BIT(availableTeamsBitfield, arrayLoop)
//		ENDIF
//	ENDREPEAT
	
	// Now add any other available teams to the storage array in random order
	INT numAvailableTeams = Get_Number_Of_Teams_In_Team_Bitfield(availableTeamsBitfield)
	
	INT thisTeam = TEAM_INVALID
	WHILE (numAvailableTeams > 0)
		thisTeam = Get_Random_Team_From_Team_Bitfield(availableTeamsBitfield)
		IF (thisTeam = TEAM_INVALID)
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MControl]: TEAM_INVALID returned from Get_Random_Team_From_Team_Bitfield() but teams available = ") NET_PRINT_INT(numAvailableTeams) NET_NL()
				Debug_Output_Team_Names_From_Bitfield(availableTeamsBitfield)
				SCRIPT_ASSERT("Reserve_Teams_For_This_MP_Mission_Request() - failed to find a random team. Should be ignorable. May pop up a few times. See console log. Tell Keith.")
			#ENDIF
		ELSE
			// ...found a random team, so add it to the storage
			//reorderedTeams[storagePosition] = thisTeam
			//storagePosition++
			
			// Update the local control bitfields
			CLEAR_BIT(availableTeamsBitfield, thisTeam)
		ENDIF
	
		numAvailableTeams--
	ENDWHILE

	// Debug Output the reordered teams array to the console log
	#IF IS_DEBUG_BUILD
		NET_PRINT("      The Order Teams will be offered to the mission (Requested Teams first, random order for remaining teams):") NET_NL()
		
//		REPEAT MAX_NUM_TEAMS arrayLoop
//			NET_PRINT("           ")
//			NET_PRINT(Convert_Team_To_String(reorderedTeams[arrayLoop]))
//			NET_NL()
//		ENDREPEAT
	#ENDIF
	
	// Get the Team Configuration Requirements for the mission
	MP_MISSION		theMission		= Get_MP_Mission_Request_MissionID(paramArrayPos)
	INT				theVariation	= Get_MP_Mission_Request_MissionVariation(paramArrayPos)
	g_eMPTeamConfig	missionConfig	= Get_MP_Mission_Variation_Team_Configuration(theMission, theVariation)
	
	// Sanity Check - ensure the team configuration for the mission has been stored
	IF (missionConfig = NO_MP_TEAM_CONFIG)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: The Team Configuration hasn't been set up for mission: ")
			NET_PRINT(GET_MP_MISSION_NAME(theMission))
			NET_NL()
			
			SCRIPT_ASSERT("Reserve_Teams_For_This_MP_Mission_Request() - ERROR: Team configuration for mission not setup. Ignoring this mission request. Tell Keith.")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Set up some 'on mission' temporary variables
//	BOOL	copsOnMission		= FALSE
//	INT		crookGangsOnMission	= 0
	
	// Set up some checking variables
	//BOOL	areCopsNecessary	= FALSE
	//INT		maxNecessaryCrooks	= TEAM_INVALID
	//INT		maxAnyTeams			= TEAM_INVALID
	
//	// Fill the checking variables based on the mission configuration requirements
//	SWITCH (missionConfig)
//		CASE MP_TEAMS_COPS
//			areCopsNecessary	= TRUE
//			maxNecessaryCrooks	= TEAM_INVALID
//			maxAnyTeams			= TEAM_INVALID
//			BREAK
//			
//		CASE MP_TEAMS_COPS_1GANG
//			areCopsNecessary	= TRUE
//			maxNecessaryCrooks	= 1
//			maxAnyTeams			= TEAM_INVALID
//			BREAK
//			
//		CASE MP_TEAMS_COPS_1GANG_MIN
//			areCopsNecessary	= TRUE
//			maxNecessaryCrooks	= MAX_NUM_TEAMS
//			maxAnyTeams			= TEAM_INVALID
//			BREAK
//			
//		CASE MP_TEAMS_COPS_1OR2GANGS
//			areCopsNecessary	= TRUE
//			maxNecessaryCrooks	= 2
//			maxAnyTeams			= TEAM_INVALID
//			BREAK
//			
//		CASE MP_TEAMS_1TEAM
//			areCopsNecessary	= FALSE
//			maxNecessaryCrooks	= TEAM_INVALID
//			maxAnyTeams			= 1
//			BREAK
//			
//		CASE MP_TEAMS_2TEAMS_MIN
//			areCopsNecessary	= FALSE
//			maxNecessaryCrooks	= TEAM_INVALID
//			maxAnyTeams			= MAX_NUM_TEAMS
//			BREAK
//			
//		CASE MP_TEAMS_1GANG
//			areCopsNecessary	= FALSE
//			maxNecessaryCrooks	= 1
//			maxAnyTeams			= TEAM_INVALID
//			BREAK
//			
//		CASE MP_TEAMS_2GANGS
//			areCopsNecessary	= FALSE
//			maxNecessaryCrooks	= 2
//			maxAnyTeams			= TEAM_INVALID
//			BREAK
//			
//		CASE MP_TEAMS_1GANG_MIN
//			areCopsNecessary	= FALSE
//			maxNecessaryCrooks	= MAX_NUM_TEAMS
//			maxAnyTeams			= TEAM_INVALID
//			BREAK
//			
//		DEFAULT
//			#IF IS_DEBUG_BUILD
//				NET_PRINT("...KGM MP [MControl]: Unknown Team Configuration: ")
//				NET_PRINT(Convert_Team_Configuration_To_String(missionConfig))
//				NET_PRINT("  [")
//				NET_PRINT(GET_MP_MISSION_NAME(theMission))
//				NET_PRINT("] - Add them to the SWITCH statement")
//				NET_NL()
//				
//				SCRIPT_ASSERT("Reserve_Teams_For_This_MP_Mission_Request() - ERROR: Team configuration for mission are not recognised. Ignoring this mission request. Tell Keith.")
//			#ENDIF
//			
//			RETURN FALSE
//	ENDSWITCH
	
//	// Feed the teams onto the mission in the re-ordered team order until the mission is fully configured
//	REPEAT MAX_NUM_TEAMS arrayLoop
//		thisTeam = reorderedTeams[arrayLoop]
//		IF NOT (thisTeam = TEAM_INVALID)
//			Store_Team_If_Mission_Is_Not_Fully_Configured(paramArrayPos, thisTeam, areCopsNecessary, maxNecessaryCrooks, maxAnyTeams, copsOnMission, crookGangsOnMission)
//		ENDIF
//	ENDREPEAT
	
	// Indicate that the debug flag is active
	#IF IS_DEBUG_BUILD
	#ENDIF

	// Dump some output to the console log
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Teams Chosen for Mission with Team Configuration: ")
		NET_PRINT(Convert_Team_Configuration_To_String(missionConfig))
		NET_NL()
		IF (GlobalServerBD.g_launchMissionsWithOneTeam)
			NET_PRINT("      NOTE: DEBUG FLAG ACTIVE - Allows Missions To Launch With Only One Team - Ignores Team Configuration Restrictions")
			NET_NL()
		ENDIF
		IF (NETWORK_IS_IN_CODE_DEVELOPMENT_MODE())
			NET_PRINT("      NOTE: CODE DEVELOPMENT MODE ACTIVE - Ignores Team Configuration Restrictions")
			NET_NL()
		ENDIF

		Debug_Output_Team_Names_From_Bitfield(Get_MP_Mission_Request_Reserved_Teams(paramArrayPos))
	#ENDIF

	// Safety Check - ensure the mission is fully configured
	IF NOT (Does_Teams_Bitfield_Match_Teams_Required_For_Mission(theMission, theVariation, Get_MP_Mission_Request_Reserved_Teams(paramArrayPos)))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: INCONSISTENCY: Mission did not end up fully configured with the required teams: ")
			NET_PRINT(Convert_Team_Configuration_To_String(missionConfig))
			NET_NL()
			Debug_Output_Team_Names_From_Bitfield(Get_MP_Mission_Request_Reserved_Teams(paramArrayPos))
			
			SCRIPT_ASSERT("Reserve_Teams_For_This_MP_Mission_Request() - Mission wasn't configured with the required teams. Ditching this mission request. tell Keith.")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// All good
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Reserve this player (by Game) for this mission and update the player control details
//
// INPUT PARAMS:		paramArrayPos			Array Position for the mission
//						paramPlayerID			Player Index
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
PROC Reserve_Player_By_Game_For_MP_Mission_Request_Slot_And_Update_Player_Control_Data(INT paramArrayPos, PLAYER_INDEX paramPlayerID, m_structPlayerAndTeamInfoMC &paramInfo)

	Set_Player_As_Reserved_By_Game_On_MP_Mission_Request_Slot(paramArrayPos, paramPlayerID)
	
	// Update the Team control variables
	INT playerIndexAsInt = NATIVE_TO_INT(paramPlayerID)
	INT theTeamID = GET_PLAYER_TEAM(paramPlayerID)
	IF (theTeamID != TEAM_INVALID)
//		CLEAR_BIT(paramInfo.bitsFreeTeamMembers[theTeamID], playerIndexAsInt)
//		paramInfo.numTeamMembersFree[theTeamID]--
	ENDIF
	
	// Error Checking - This bit should also be set on the 'free players' control variables, so just make sure
	IF NOT (IS_BIT_SET(paramInfo.bitsFreePlayers, playerIndexAsInt))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: DATA INCONSISTENCY: Found a player on a Team's 'free' list that isn't on the AllPlayers 'free' list.") NET_NL()
			NET_PRINT("      MissionID: ") NET_PRINT(GET_MP_MISSION_NAME(Get_MP_Mission_Request_MissionID(paramArrayPos))) NET_NL()
			NET_PRINT("      Team     : ") NET_PRINT(Convert_Team_To_String(theTeamID)) NET_NL()
			NET_PRINT("      Player   : ") NET_PRINT(GET_PLAYER_NAME(paramPlayerID)) NET_NL()
			NET_PRINT("      List of Free Players") NET_NL()
			Debug_Output_Player_Names_From_Bitfield(paramInfo.bitsFreePlayers)
			
			SCRIPT_ASSERT("Reserve_Player_By_Game_For_MP_Mission_Request_Slot_And_Update_Player_Control_Data() - ERROR: Player should be 'free' but isn't. See Console Log. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Update the Player control variables
	CLEAR_BIT(paramInfo.bitsFreePlayers, playerIndexAsInt)
	paramInfo.numPlayersFree--
	
	SET_BIT(paramInfo.bitsBusyPlayers, playerIndexAsInt)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Reserve the players from the specified team for this mission
//
// INPUT PARAMS:		paramArrayPos			Array Position for the mission
//						paramTeamID				The Team for which players are being reserved
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
// RETURN VALUE:		BOOL					TRUE if the players were successfully reserved for this mission, otherwise FALSE
FUNC BOOL Reserve_Players_From_This_Team_For_MP_Mission_Request_Slot(INT paramArrayPos, INT paramTeamID, m_structPlayerAndTeamInfoMC &paramInfo)

	MP_MISSION			theMission				= Get_MP_Mission_Request_MissionID(paramArrayPos)
	MP_MISSION_ID_DATA	missionIdData			= Get_MP_Mission_Request_MissionID_Data(paramArrayPos)
	BOOL				playersShouldBeInTeams	= TRUE
	
	// Error Checking - ensure valid team (this should just be being over-cautious)
	IF (paramTeamID = TEAM_INVALID)
		IF (Check_If_Players_Are_In_Teams_For_Mission(theMission))
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MControl]: Reserve_Players_From_This_Team_For_MP_Mission_Request_Slot() - request was for TEAM_INVALID but mission expects players to be in teams")
				NET_NL()
				
				SCRIPT_ASSERT("Reserve_Players_From_This_Team_For_MP_Mission_Request_Slot() - ERROR: Illegal Team: TEAM_INVALID. See Console Log. Tell Keith.")
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		playersShouldBeInTeams = FALSE
	ENDIF
	
//	IF (paramTeamID >= MAX_NUM_TEAMS)
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("...KGM MP [MControl]: Reserve_Players_From_This_Team_For_MP_Mission_Request_Slot() - request was for this illegal team number: ")
//			NET_PRINT_INT(paramTeamID)
//			NET_NL()
//			
//			SCRIPT_ASSERT("Reserve_Players_From_This_Team_For_MP_Mission_Request_Slot() - ERROR: Illegal Team. See Console Log. Tell Keith.")
//		#ENDIF
//		
//		RETURN FALSE
//	ENDIF
	
	// Error Checking - ensure the number of free players for the Team matches the minimum required for the mission
	// NOTE: This is just being over-cautious, this should all have been confirmed earlier in the selection process
	INT minimumPlayersForMission	= 0
	INT	numFreePlayers				= 0
	INT bitsFreePlayers				= ALL_MISSION_CONTROL_BITS_CLEAR
	
	IF (playersShouldBeInTeams)
//		numFreePlayers				= paramInfo.numTeamMembersFree[paramTeamID]
//		bitsFreePlayers				= paramInfo.bitsFreeTeamMembers[paramTeamID]
		//minimumPlayersForMission	= Get_Minimum_Number_Of_Team_Members_Required_For_MP_Mission(theMission, paramTeamID)
	ELSE
		numFreePlayers				= paramInfo.numPlayersFree
		bitsFreePlayers				= paramInfo.bitsFreePlayers
		minimumPlayersForMission	= Get_Minimum_Players_Required_For_MP_Mission_Request(missionIdData)
	ENDIF
	
	// Debug: Allow launch without having minimum player requirements?
	#IF IS_DEBUG_BUILD
		IF (Does_MP_Mission_Request_Allow_One_Player_Launch_In_Debug(paramArrayPos))
			IF (minimumPlayersForMission > 1)
				minimumPlayersForMission = 1
			ENDIF
		ENDIF
		IF DEBUG_IS_SOLO_STRAND_ENABLED()
			RETURN TRUE
		ENDIF
	#ENDIF
	
	IF (numFreePlayers < minimumPlayersForMission)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Reserve_Players_From_This_Team_For_MP_Mission_Request_Slot() - There are not enough free players for mission") NET_NL()
			NET_PRINT("      MissionID         : ") NET_PRINT(GET_MP_MISSION_NAME(theMission)) NET_NL()
			NET_PRINT("      Team              : ") NET_PRINT(Convert_Team_To_String(paramTeamID)) NET_NL()
			NET_PRINT("      Min Players Needed: ")
			NET_PRINT_INT(minimumPlayersForMission)
			IF (Does_MP_Mission_Request_Allow_One_Player_Launch_In_Debug(paramArrayPos))
				NET_PRINT(" [DEBUG: Ignored Minimum Player Requirements for launch]")
			ENDIF
			NET_NL()
			NET_PRINT("      Free Players      : ") NET_PRINT_INT(numFreePlayers) NET_NL()
			Debug_Output_Player_Names_From_Bitfield(bitsFreePlayers)
			NET_NL()
			
			SCRIPT_ASSERT("Reserve_Players_From_This_Team_For_MP_Mission_Request_Slot() - ERROR: There are not enough free players to meet the minimum mission requirements. See Console Log. Tell Keith.")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Get the reservation routine required for the mission
	BOOL reserveSpecificPlayersOnly = Should_MP_Mission_Team_Allow_Specific_Players_Only(theMission, paramTeamID)
	
	// Get the free team members from the team
	INT reservedByPlayerPlayers = Get_MP_Mission_Request_Reserved_By_Player_Players(paramArrayPos)
	
	INT tempLoop = 0
	PLAYER_INDEX thePlayer
	BOOL reserveThisPlayer = FALSE
	INT numReservedPlayers = 0
	
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(bitsFreePlayers, tempLoop))
			// Found a free player so check if the player should be reserved for the mission
			thePlayer = INT_TO_PLAYERINDEX(tempLoop)
			
			reserveThisPlayer = TRUE
			IF (reserveSpecificPlayersOnly)
				IF NOT (IS_BIT_SET(reservedByPlayerPlayers, tempLoop))
					reserveThisPlayer = FALSE
				ENDIF
			ENDIF
			
			IF (reserveThisPlayer)
				// Player may already by reserved (by player), so don't try to reserve by game
				IF NOT (Is_Player_Reserved_For_MP_Mission_Request_Slot(paramArrayPos, thePlayer))
					Reserve_Player_By_Game_For_MP_Mission_Request_Slot_And_Update_Player_Control_Data(paramArrayPos, thePlayer, paramInfo)
				ENDIF
				
				// Even if already reserved, still need to increment this counter
				numReservedPlayers++
				
				#IF IS_DEBUG_BUILD
					IF (Is_Player_Reserved_By_Player_For_MP_Mission_Request_Slot(paramArrayPos, thePlayer))
						NET_PRINT("      Already Reserved_By_Player for mission: ")
					ELIF (Is_Player_Reserved_By_Leader_For_MP_Mission_Request_Slot(paramArrayPos, thePlayer))
						NET_PRINT("      Already Reserved_By_Leader for mission: ")
					ELSE
						NET_PRINT("      Setting Player as Reserved_By_Game for mission: ")
					ENDIF
					NET_PRINT(GET_PLAYER_NAME(thePlayer))
					NET_PRINT("  (")
					NET_PRINT(Convert_Team_To_String(paramTeamID))
					NET_PRINT(") - ")
					IF (reserveSpecificPlayersOnly)
						NET_PRINT("Using: Reserve Specific Players Only")
					ELSE
						NET_PRINT("Using: Reserve All Players")
					ENDIF
					NET_NL()
				#ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Ensure the number of players actually reserved still matches the minimum player requirements for the mission
	IF (numReservedPlayers < minimumPlayersForMission)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Reserve_Players_From_This_Team_For_MP_Mission_Request_Slot() - There are not enough players on Team reserved for mission") NET_NL()
			NET_PRINT("      MissionID           : ") NET_PRINT(GET_MP_MISSION_NAME(theMission)) NET_NL()
			NET_PRINT("      Team                : ") NET_PRINT(Convert_Team_To_String(paramTeamID)) NET_NL()
			NET_PRINT("      Min Players Needed  : ") NET_PRINT_INT(minimumPlayersForMission) NET_NL()
			NET_PRINT("      Num Players Reserved: ") NET_PRINT_INT(numReservedPlayers) NET_NL()
			Debug_Output_Player_Names_From_Bitfield(Get_MP_Mission_Request_Reserved_Players(paramArrayPos))
			NET_NL()
			
			SCRIPT_ASSERT("Reserve_Players_From_This_Team_For_MP_Mission_Request_Slot() - ERROR: There are not enough reserved players from this team to meet the minimum mission requirements. See Console Log. Tell Keith.")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// More (probably over-cautious) error checking, but let the mission continue
	#IF IS_DEBUG_BUILD
		IF (paramTeamID != TEAM_INVALID)
//			IF (paramInfo.numTeamMembersFree[paramTeamID] < 0)
//				NET_PRINT("...KGM MP [MControl]: DATA INCONSISTENCY: Number Of Team Members free is now less than 0. Team: ") NET_PRINT(Convert_Team_To_String(paramTeamID)) NET_NL()
//				SCRIPT_ASSERT("Reserve_Players_From_This_Team_For_MP_Mission_Request_Slot() - ERROR: The NUMBER of free team members didn't match the LIST of free team members. Probably ignorable for now. See Console Log. Tell Keith.")
//			ENDIF
		ENDIF
	#ENDIF
	
	// More (probably over-cautious) error checking, but let the mission continue
	#IF IS_DEBUG_BUILD
		IF (paramInfo.numPlayersFree < 0)
			NET_PRINT("...KGM MP [MControl]: DATA INCONSISTENCY: Number Of Players free is now less than 0.") NET_NL()
			SCRIPT_ASSERT("Reserve_Players_From_This_Team_For_MP_Mission_Request_Slot() - ERROR: The NUMBER of free players didn't match the list of FREE players. Probably ignorable for now. See Console Log. Tell Keith.")
		ENDIF
	#ENDIF
	
	// All OK
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Reserve the participants needed for this mission
//
// INPUT PARAMS:		paramArrayPos			Array Position for the mission
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
// RETURN VALUE:		BOOL					TRUE if the players were successfully reserved for this mission, otherwise FALSE
FUNC BOOL Reserve_Participants_For_MP_Mission_Request_Slot(INT paramArrayPos, m_structPlayerAndTeamInfoMC &paramInfo)

	INT tempLoop = 0

	// If the mission is team-based then reserve teams for the mission
	IF (Check_If_Players_Are_In_Teams_For_Mission(Get_MP_Mission_Request_MissionID(paramArrayPos)))
		// Reserve the Teams for this Mission
		IF NOT (Reserve_Teams_For_This_MP_Mission_Request(paramArrayPos))
			RETURN FALSE
		ENDIF
		
		// Reserve Players from the Chosen Teams for the mission
		//INT reservedTeamsBitfield = Get_MP_Mission_Request_Reserved_Teams(paramArrayPos)
		
		// List of Reserved By Player Players for this mission
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: List of Reserved_By_Player Players for Mission Request Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
			Debug_Output_Player_Names_From_Bitfield(Get_MP_Mission_Request_Reserved_By_Player_Players(paramArrayPos))
		#ENDIF
		
//		REPEAT MAX_NUM_TEAMS tempLoop
//			IF (IS_BIT_SET(reservedTeamsBitfield, tempLoop))
//				IF NOT (Reserve_Players_From_This_Team_For_MP_Mission_Request_Slot(paramArrayPos, tempLoop, paramInfo))
//					RETURN FALSE
//				ENDIF
//			ENDIF
//		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("      Player Details After Team and Player Reservations for Mission Request. Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
			Debug_Output_Missions_Player_Details(paramArrayPos)
		#ENDIF
	ELSE
		// Reserve all valid players for this mission ignoring teams
		IF NOT (Reserve_Players_From_This_Team_For_MP_Mission_Request_Slot(paramArrayPos, TEAM_INVALID, paramInfo))
			RETURN FALSE
		ENDIF
	ENDIF

	// Perform Reserved Player checks
	INT reservedPlayers = Get_MP_Mission_Request_Reserved_Players(paramArrayPos)
	
	// Consistency checking
	#IF IS_DEBUG_BUILD
		INT reservedByPlayerPlayers = Get_MP_Mission_Request_Reserved_By_Player_Players(paramArrayPos)
		INT inconsistentReservations = ALL_MISSION_CONTROL_BITS_CLEAR
		
		REPEAT NUM_NETWORK_PLAYERS tempLoop
			IF (IS_BIT_SET(reservedByPlayerPlayers, tempLoop))
			AND NOT (IS_BIT_SET(reservedPlayers, tempLoop))
				SET_BIT(inconsistentReservations, tempLoop)
			ENDIF
		ENDREPEAT
	
		IF NOT (inconsistentReservations = ALL_MISSION_CONTROL_BITS_CLEAR)
			// A player was Reserved By Player that has not been tagged as Reserved
			NET_PRINT("      FAILED TO LAUNCH: Players that were Reserved By Player but that have not been Reserved (this will cause the Mission Request to fail). Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
			Debug_Output_Player_Names_From_Bitfield(inconsistentReservations)
		
			RETURN FALSE
		ENDIF
	#ENDIF
	
	// Ensure the number of reserved players meets the minimum players for mission requirement
	// NOTE: This was added for RACES which allows any combination of players from different teams to vote to launch the mission, but which still needs a minimum number of players
	INT numReservedPlayers = 0
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(reservedPlayers, tempLoop))
			numReservedPlayers++
		ENDIF
	ENDREPEAT
	
	MP_MISSION_ID_DATA	missionIdData					= Get_MP_Mission_Request_MissionID_Data(paramArrayPos)
	INT					minimumPlayersNeededForMission	= Get_Minimum_Players_Required_For_MP_Mission_Request(missionIdData)
	
	// Debug: Allow launch without having minimum player requirements?
	#IF IS_DEBUG_BUILD
		IF (Does_MP_Mission_Request_Allow_One_Player_Launch_In_Debug(paramArrayPos))
			NET_PRINT("      DEBUG: Ignore minimum number of total players required for launch") NET_NL()
			
			IF (minimumPlayersNeededForMission > 1)
				minimumPlayersNeededForMission = 1
			ENDIF
		ENDIF
		IF DEBUG_IS_SOLO_STRAND_ENABLED()
			RETURN TRUE
		ENDIF		
	#ENDIF

	IF (numReservedPlayers < minimumPlayersNeededForMission)
		// The total number of reserved players hasn't reached the minimum requirements for the mission
		#IF IS_DEBUG_BUILD
			NET_PRINT("      FAILED TO LAUNCH: The minimum number of total players required for the mission have not been met: Required ") NET_PRINT_INT(minimumPlayersNeededForMission) NET_NL()
			Debug_Output_Player_Names_From_Bitfield(reservedPlayers)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is FULL for a Team
//
// INPUT PARAMS:		paramArrayPos		Array Position for the mission
//						paramTeamID			The Team ID
// RETURN VALUE:		BOOL				TRUE if the mission is full for this Team, otherwise FALSE
//
// NOTES:	Treat Confirmed Players as Active for this check - a Confirmed Player is one that has accepted the offer to join the mission and has checked if it is still ok to launch the mission
//			Also can be used with non-team-based missions
FUNC BOOL Check_If_Mission_Or_Team_Full(INT paramArrayPos, INT paramTeamID)

	// Check both max players and, if appropriate, max players in team
	MP_MISSION			theMission			= Get_MP_Mission_Request_MissionID(paramArrayPos)
	BOOL				playersAreInTeams	= Check_If_Players_Are_In_Teams_For_Mission(theMission)
	MP_MISSION_ID_DATA	missionIdData		= Get_MP_Mission_Request_MissionID_Data(paramArrayPos)
	INT					maxPlayersAllowed	= Get_Maximum_Players_Required_For_MP_Mission_Request(missionIdData)
	INT					maxAllowedFromTeam	= Get_Maximum_Team_Members_For_MP_Mission_Request(missionIdData, paramTeamID)
	INT					thisPlayerTeam		= TEAM_INVALID
	INT					numActivePlayers	= 0
	INT					numActiveFromTeam	= 0
	INT					playerAsInt			= 0
	PLAYER_INDEX		thisPlayer
	
	// Check if the number of Active players (on this Team) has hit the maximum
	INT activePlayerBitfield	= Get_MP_Mission_Request_Active_Players(paramArrayPos)
	INT confirmedPlayerBitfield	= Get_MP_Mission_Request_Confirmed_Players(paramArrayPos)
	
	REPEAT NUM_NETWORK_PLAYERS playerAsInt
		thisPlayer = INT_TO_PLAYERINDEX(playerAsInt)
		IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
			// Found an active player, so check the number of players (use teams if the mission is team-based)
			IF (IS_BIT_SET(activePlayerBitfield, playerAsInt))
			OR (IS_BIT_SET(confirmedPlayerBitfield, playerAsInt))
				// Check for max players on mission limit being reached
				numActivePlayers++
		
				IF (numActivePlayers >= maxPlayersAllowed)
					// ...reached the maximum players allowed on this mission
					#IF IS_DEBUG_BUILD
						NET_PRINT("...KGM MP [MControl]: Mission has reached maximum number of active and/or confirmed players [")
						NET_PRINT(GET_MP_MISSION_NAME(theMission))
						NET_PRINT("]: ")
						NET_PRINT_INT(maxPlayersAllowed)
						NET_NL()
					#ENDIF
					
					// Reached the maximum
					RETURN TRUE
				ENDIF
			
				// If appropriate, do the team full check
				IF (playersAreInTeams)
					thisPlayerTeam = GET_PLAYER_TEAM(thisPlayer)
					
					IF (thisPlayerTeam = paramTeamID)
						numActiveFromTeam++
						
						IF (numActiveFromTeam >= maxAllowedFromTeam)
							// ...reached the maximum players from team allowed on this mission
							#IF IS_DEBUG_BUILD
								NET_PRINT("...KGM MP [MControl]:")
								NET_PRINT(" Team [")
								NET_PRINT(Convert_Team_To_String(paramTeamID))
								NET_PRINT("]")
								NET_PRINT(" has reached maximum number of active and/or confirmed players from this team for mission [")
								NET_PRINT(GET_MP_MISSION_NAME(theMission))
								NET_PRINT("]: ")
								NET_PRINT_INT(maxAllowedFromTeam)
								NET_NL()
							#ENDIF
							
							// Reached the maximum
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Must still be room on mission for more players
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Free player because mission full
//
// INPUT PARAMS:		paramArrayPos		Array Position for the mission
//						paramPlayerID		Player Index
//
// NOTES:	Joining Players should be told the mission is full before being freed from the mission
PROC Free_Player_Because_Mission_Or_Team_Full(INT paramArrayPos, PLAYER_INDEX paramPlayerID)

	IF (Is_Player_Joining_MP_Mission_Request_Slot(paramArrayPos, paramPlayerID))
		Broadcast_Mission_Request_Mission_Full(paramPlayerID, paramArrayPos)
		Clear_Player_As_Joining_MP_Mission_Request_Slot(paramArrayPos, paramPlayerID)
	ENDIF
	
	// If the player was 'Reserved By Player' then clear the bitflag and broadcast 'mission full'
	IF (Is_Player_Reserved_By_Player_For_MP_Mission_Request_Slot(paramArrayPos, paramPlayerID))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Player Reserved By Player never made it onto mission. Clearing the Reserved By Player Bit and broadcasting Mission Full: ")
			NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
			NET_NL()
		#ENDIF
		
		Remove_Player_From_All_Reserved_Bitfields_On_MP_Mission_Request_Slot(paramArrayPos, paramPlayerID)
		Broadcast_Mission_Request_Mission_Full(paramPlayerID, paramArrayPos)
		
		EXIT
	ENDIF
	
	// If the player is still reserved for the mission, then release the player back into the free population
	IF (Is_Player_Reserved_For_MP_Mission_Request_Slot(paramArrayPos, paramPlayerID))
		Remove_Player_From_All_Reserved_Bitfields_On_MP_Mission_Request_Slot(paramArrayPos, paramPlayerID)
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Free up any non-active players on this team
//
// INPUT PARAMS:		paramArrayPos		Array Position for the mission
//						paramTeamID			The Team ID
//
// NOTES:	Joining Players should be told the mission is full before being freed from the mission
//			Treat 'Confirmed' players as 'Active' - so don't free them
PROC Free_Non_Active_Players_For_Mission_Or_Team(INT paramArrayPos, INT paramTeamID)
	
	BOOL			playersAreInTeams	= Check_If_Players_Are_In_Teams_For_Mission(Get_MP_Mission_Request_MissionID(paramArrayPos))
	BOOL			tryToFreeThisPlayer	= FALSE
	INT				playerTeam			= TEAM_INVALID
	INT				playerAsInt			= 0
	PLAYER_INDEX	thisPlayer
	
	REPEAT NUM_NETWORK_PLAYERS playerAsInt
		thisPlayer = INT_TO_PLAYERINDEX(playerAsInt)
		IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
			// Found an active player, try to free the player if the mission isn't team-based because the mission must be full,
			//			otherwise try to free the player if in the specified team if the mission is team-based
			tryToFreeThisPlayer = FALSE
			IF (playersAreInTeams)
				// ...the mission is team-based, so only try to free this player if in the passed in team
				playerTeam = GET_PLAYER_TEAM(thisPlayer)
				
				IF (playerTeam = paramTeamID)
					tryToFreeThisPlayer = TRUE
				ENDIF
			ELSE
				// ...the mission is not team-based so the mission must be full, so try to free this player
				tryToFreeThisPlayer = TRUE
			ENDIF

			// If required, try to free this player - the player will be freed if not already actively on the mission
			IF (tryToFreeThisPlayer)
				Free_Player_Because_Mission_Or_Team_Full(paramArrayPos, thisPlayer)
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Players details after Freeing Non-Active Players On Team: ") NET_PRINT(Convert_Team_To_String(paramTeamID)) NET_NL()
		Debug_Output_Missions_Player_Details(paramArrayPos)
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: A reserved mission is allowed to be oversubscribed with Reserved players - any excess players may need freed when the mission is started
//
// INPUT PARAMS:		paramArrayPos			Array Position for the mission
//						paramPlayerBits			Bitfield of players required to be on the mission.
//
// NOTES:	Assumes all playerBits have been verified as being in the Reserved Players list
PROC Free_Any_Excess_Players_When_Starting_A_Reserved_Mission_If_Mission_Full(INT paramArrayPos, INT paramPlayerBits)

	INT reservedPlayers = Get_MP_Mission_Request_Reserved_Players(paramArrayPos)
	
	// If all Reserved players are in the starting list then nothing more needs to be done
	IF (reservedPlayers = paramPlayerBits)
		EXIT
	ENDIF
	
	// Is the mission team-based?
	MP_MISSION			theMission			= Get_MP_Mission_Request_MissionID(paramArrayPos)
	MP_MISSION_ID_DATA	missionIdData		= Get_MP_Mission_Request_MissionID_Data(paramArrayPos)
	BOOL				playersAreInTeams	= Check_If_Players_Are_In_Teams_For_Mission(theMission)
	
	// There are excess players that need dealt with
//	INT playersOnEachTeam[MAX_NUM_TEAMS]
	INT totalPlayersOnMission = 0
	
	INT tempLoop = 0
//	REPEAT MAX_NUM_TEAMS tempLoop
//		playersOnEachTeam[tempLoop] = 0
//	ENDREPEAT
	
	// Go through the playerBits and update their team counters if relevant, and total players on the mission, then remove them from the local copy of the reserved players bitfield
	// This will leave only the excess players on the reserved players bitfield - those players that weren't passed as a starting player along with the mission start call
	// NOTE: If a playerID is no longer OK just ignore it here - this will need dealt with elsewhere
//	INT playerTeam = TEAM_INVALID
	PLAYER_INDEX thePlayer
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(paramPlayerBits, tempLoop))
			// Found a player in the playerBits list
			thePlayer = INT_TO_PLAYERINDEX(tempLoop)
			IF (IS_NET_PLAYER_OK(thePlayer, FALSE))
				totalPlayersOnMission++
			
				// If the mission is team-based then gather team information
				IF (playersAreInTeams)
					//playerTeam = GET_PLAYER_TEAM(thePlayer)
					//playersOnEachTeam[playerTeam] += 1
				ENDIF
			ENDIF
			
			// Remove the player from the reserved players bitfield to leave only excess players on the bitfield
			CLEAR_BIT(reservedPlayers, tempLoop)
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("    Dealing with these Excess Players in the Reserved bitfield for the mission") NET_NL()
	#ENDIF
	
//	// Any excess players marked as Reserved but not in the starting list need to be freed if the mission is full,
//	//		or they can be left to join the mission if there is room
//	INT	maxPlayersInTeam[MAX_NUM_TEAMS]
//		
//	REPEAT MAX_NUM_TEAMS tempLoop
//		maxPlayersInTeam[tempLoop] = Get_Maximum_Team_Members_For_MP_Mission_Request(missionIdData, tempLoop)
//	ENDREPEAT
	
	// First, if the mission is team-based, remove excess reserved players from each team
	// NOTE: This doesn't update the reservedPlayers bitfield because they may survive the team-full cull but get removed in the mission-full cull
	IF (playersAreInTeams)
		REPEAT NUM_NETWORK_PLAYERS tempLoop
			IF (IS_BIT_SET(reservedPlayers, tempLoop))
				// Found player in the Reserved Players for Mission list, so this is an excess player that wasn't in the playerBits list
				thePlayer = INT_TO_PLAYERINDEX(tempLoop)
				IF (IS_NET_PLAYER_OK(thePlayer, FALSE))
//					playerTeam = GET_PLAYER_TEAM(thePlayer)
//					IF (playersOnEachTeam[playerTeam] >= maxPlayersInTeam[playerTeam])
//						// Mission Full for Team
//						#IF IS_DEBUG_BUILD
//							NET_PRINT("           ")
//							NET_PRINT(GET_PLAYER_NAME(thePlayer))
//							NET_PRINT(" [")
//							NET_PRINT(Convert_Team_To_String(playerTeam))
//							NET_PRINT("]: - To Be Freed - Team Full")
//							NET_NL()
//						#ENDIF
//						
//						Remove_Player_From_All_Reserved_Bitfields_On_MP_Mission_Request_Slot(paramArrayPos, thePlayer)
//						Broadcast_Mission_Request_Mission_Full(thePlayer, paramArrayPos)
//					ELSE
//						// Room for player on team
//						// NOTE: Don't update the 'total players' value here - we need it at it's original level for the mission-full checks below
//						maxPlayersInTeam[playerTeam] += 1
//					ENDIF
				ENDIF

				#IF IS_DEBUG_BUILD
					IF NOT (IS_NET_PLAYER_OK(thePlayer, FALSE))
						NET_PRINT("           Player Number: ") NET_PRINT_INT(tempLoop) NET_PRINT(" - NOT OK") NET_NL()
					ENDIF
				#ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	// May still need to remove the excess reserved players if there are too many players reserved for the mission
	INT					maxPlayersForMission	= Get_Maximum_Players_Required_For_MP_Mission_Request(missionIdData)
	
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(reservedPlayers, tempLoop))
			// Found player in the Reserved Players for Mission list, so this is an excess player that wasn't in the playerBits list
			thePlayer = INT_TO_PLAYERINDEX(tempLoop)
			IF (IS_NET_PLAYER_OK(thePlayer, FALSE))
				// Is there room on the mission for this player?
				IF (totalPlayersOnMission >= maxPlayersForMission)
					// ...no room on mission, so remove this player
					#IF IS_DEBUG_BUILD
						NET_PRINT("           ")
						NET_PRINT(GET_PLAYER_NAME(thePlayer))
						NET_PRINT(" [")
						//NET_PRINT(Convert_Team_To_String(playerTeam))
						NET_PRINT("]: - To Be Freed - Mission Full")
						NET_NL()
					#ENDIF
					
					Remove_Player_From_All_Reserved_Bitfields_On_MP_Mission_Request_Slot(paramArrayPos, thePlayer)
					Broadcast_Mission_Request_Mission_Full(thePlayer, paramArrayPos)
				ELSE
					// ...there is room, so update the control variables
					totalPlayersOnMission++
						
					#IF IS_DEBUG_BUILD
						NET_PRINT("           ")
						NET_PRINT(GET_PLAYER_NAME(thePlayer))
						NET_PRINT(" [")
						//NET_PRINT(Convert_Team_To_String(playerTeam))
						NET_PRINT("]: - Allowed On Mission - Mission is Not Full")
						IF (playersAreInTeams)
							NET_PRINT(" and Team is Not Full")
						ENDIF
						NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC


// ===========================================================================================================
//      Player Invitations onto Missions
// ===========================================================================================================

// PURPOSE:	Use the appropriate Broadcast method to get a player onto a mission
//
// INPUT PARAMS:		paramArrayPos			Array Position for the mission
//						paramPlayerIndex		The Player Index
//						paramInvitePlayer		TRUE if the player should be invited onto the mission, FALSE if forced on
//						paramUseComms			TRUE if the player should receive pre-mission communication, FALSE if no communication
//
// NOTES:	Initially I'm going to assume that only missions that force a player onto the mission without a phonecall has the possibility of showing a cutscene
PROC Issue_Broadcast_For_Player_To_Join_MP_Mission_Request(INT paramArrayPos, PLAYER_INDEX paramPlayerID, BOOL paramInvitePlayer, BOOL paramUseComms)

	BOOL showCutscene = FALSE
	IF NOT (paramInvitePlayer)
	AND NOT (paramUseComms)
		showCutscene = Does_MP_Mission_Request_Have_Cutscene(paramArrayPos)
	ENDIF
	
	IF (ParamInvitePlayer)
		// ...player gets Invited onto the mission
		Broadcast_Invite_Player_Onto_Mission(paramPlayerID, paramArrayPos)
	ELSE
		// Player gets Forced onto the mission
		Broadcast_Force_Player_Onto_Mission(paramPlayerID, paramArrayPos, paramUseComms, showCutscene)
	ENDIF

	// Change Reserved Players to Joining Players
	Remove_Player_From_All_Reserved_Bitfields_On_MP_Mission_Request_Slot(paramArrayPos, paramPlayerID)
	Set_Player_As_Joining_MP_Mission_Request_Slot(paramArrayPos, paramPlayerID)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Gather the Capacity Details for this mission, used to check if there is room for more Team Players
//
// INPUT PARAMS:		paramArrayPos			Array Position for the mission
//						paramTeamID				Team ID being requested to join mission
// RETURN PARAMS:		paramNumActive			Returns the number of Active Players from this Team on the mission
//						paramNumConfirmed		Returns the number of Confirmed Players from this Team on the mission
//						paramNumJoining			Returns the number of Joining Players from this Team on the mission
//						paramInfo				The struct containing Player and Team Info used during decision making
PROC Gather_MP_Mission_Request_Capacity_Info_For_Team(INT paramArrayPos, INT paramTeamID, INT &paramNumActive, INT &paramNumConfirmed, INT &paramNumJoining, m_structPlayerAndTeamInfoMC &paramInfo)
	
	IF (paramInfo.bitsBusyPlayers=0)
		// fool compiler
	ENDIF

	INT confirmedPlayersBitfield	= Get_MP_Mission_Request_Confirmed_Players(paramArrayPos)
	INT joiningPlayersBitfield		= Get_MP_Mission_Request_Joining_Players(paramArrayPos)
	INT activePlayersBitfield		= Get_MP_Mission_Request_Active_Players(paramArrayPos)
	
	INT tempLoop = 0
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (paramTeamID = TEAM_INVALID)
		//OR (IS_BIT_SET(paramInfo.bitsAllTeamMembers[paramTeamID], tempLoop))
			// Player is on this team
			// Check if Active
			IF (IS_BIT_SET(activePlayersBitfield, tempLoop))
				paramNumActive++
			ENDIF
			
			// Check if Confirmed
			IF (IS_BIT_SET(confirmedPlayersBitfield, tempLoop))
				paramNumConfirmed++
			ENDIF
			
			// Check If Joining
			IF (IS_BIT_SET(joiningPlayersBitfield, tempLoop))
				paramNumJoining++
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To check if any more players from this Team can be selected for the mission
//
// INPUT PARAMS:		paramArrayPos			Array Position for the mission
//						paramTeamID				Team ID being requested to join mission
//						paramInviteOntoMission	TRUE if the player gets invited onto the mission, otherwise FALSE
//						paramNumActive			Number Of Active Players on Mission
//						paramNumConfirmed		Number of Confirmed Players For Mission
//						paramNumJoining			Number Of Joining Players On Mission
//						paramMaxAllowed			Maximum Players Allowed on mission from Team
// RETURN VALUE:		BOOL					TRUE if more players can be selected for Mission, FALSE if not
//
// NOTES:	If players are being forced onto the mission then player selection should stop when Active + Confirmed + Joining players = MaxAllowed
//			If players are invited onto the mission then player selection only needs to stop when Active + Confirmed players = MaxAllowed
FUNC BOOL Can_Any_More_Players_Be_Selected_To_Join_MP_Mission_Request(INT paramArrayPos, INT paramTeamID, BOOL paramInviteOntoMission, INT paramNumActive, INT paramNumConfirmed, INT paramNumJoining, INT paramMaxAllowed)
	
	BOOL canMoreJoin = TRUE
	
	// Treat Active and Confirmed Players as the same thing
	// NOTE: A confirmed player is a player that was offered the mission, has accepted, has checked for confirmation that it is still ok to launch the mission,
	//			and is in the process of launching the mission and becoming an active player
	INT numActivePlusConfirmed = paramNumActive + paramNumConfirmed
	
	// Always Quit if the number of active players has reached the Max Allowed
	IF (numActivePlusConfirmed >= paramMaxAllowed)
		canMoreJoin = FALSE
	ENDIF
	
	IF (canMoreJoin)
		// If players are being forced onto the mission then quit if the Active + Joining players has reached Max Allowed
		IF NOT (paramInviteOntoMission)
			IF ((numActivePlusConfirmed + paramNumJoining) >= paramMaxAllowed)
				canMoreJoin = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// If more can still join, then return TRUE
	IF (canMoreJoin)
		RETURN TRUE
	ENDIF
	
	// A couple of the input parameters are only needed for debug - so need to fool the compiler to prevent unreferenced variables
	#IF NOT IS_DEBUG_BUILD
		paramArrayPos = paramArrayPos
		paramTeamID = paramTeamID
	#ENDIF
	
	// More Can't Join, so output some console log output
	#IF IS_DEBUG_BUILD
		NET_PRINT("      No More Players Can Join Mission")
		IF (Check_If_Players_Are_In_Teams_For_Mission(Get_MP_Mission_Request_MissionID(paramArrayPos)))
			NET_PRINT(" from Team: ")
			NET_PRINT(Convert_Team_To_String(paramTeamID))
		ENDIF
		NET_NL()
		NET_PRINT("      Capacity: ") NET_PRINT_INT(paramMaxAllowed) NET_NL()
		NET_PRINT("      Active Players On Mission")
		IF (paramNumActive = 0)
			NET_PRINT(": None")
		ENDIF
		NET_NL()
		Debug_Output_Player_Names_From_Bitfield(Get_MP_Mission_Request_Active_Players(paramArrayPos))
		NET_PRINT("      Confirmed Players For Mission")
		IF (paramNumConfirmed = 0)
			NET_PRINT(": None")
		ENDIF
		NET_NL()
		Debug_Output_Player_Names_From_Bitfield(Get_MP_Mission_Request_Confirmed_Players(paramArrayPos))
		NET_PRINT("      Joining Players For Mission")
		IF (paramNumJoining = 0)
			NET_PRINT(": None")
		ENDIF
		NET_NL()
		Debug_Output_Player_Names_From_Bitfield(Get_MP_Mission_Request_Joining_Players(paramArrayPos))
	#ENDIF

	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this mission should place Reserved By Player players on the mission first
//
// INPUT PARAMS:		paramArrayPos			Array Position for the mission
//
// NOTES:	Missions with a pre-mission cutscene, and some specific missions will do this
FUNC BOOL Check_If_Reserved_By_Player_Players_Get_Join_Priority_At_Launch(INT paramArrayPos)

	// Missions with a pre-mission cutscene need Reserved By Player players to join first
	IF (Does_MP_Mission_Request_Have_Cutscene(paramArrayPos))
		RETURN TRUE
	ENDIF
	
	// Check if this mission specifically needs Reserved By Player players to join the mission first
	IF (Should_MP_Mission_Variation_Reserved_By_Players_Join_First(Get_MP_Mission_Request_MissionID(paramArrayPos), Get_MP_Mission_Request_MissionVariation(paramArrayPos)))
		RETURN TRUE
	ENDIF
	
	// All team members join together
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	The Join Request goes to the closest players in the team
//
// INPUT PARAMS:		paramArrayPos			Array Position for the mission
//						paramTeamID				Team ID being requested to join mission
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
PROC Select_Closest_Players_From_Team_For_MP_Mission_Request(INT paramArrayPos, INT paramTeamID, m_structPlayerAndTeamInfoMC &paramInfo)

	MP_MISSION			theMission			= Get_MP_Mission_Request_MissionID(paramArrayPos)
	INT					theVariation		= Get_MP_Mission_Request_MissionVariation(paramArrayPos)
	MP_MISSION_ID_DATA	missionIdData		= Get_MP_Mission_Request_MissionID_Data(paramArrayPos)
	BOOL 				inviteOntoMission	= Should_MP_Mission_Team_Invite_Player_Onto_Mission(theMission, paramTeamID)
	
	BOOL usePreMissionComms = FALSE
	IF (Should_MP_Mission_Team_Use_A_PreMission_Txtmsg(theMission, paramTeamID))
	OR (Should_MP_Mission_Team_Use_A_PreMission_Phonecall(theMission, paramTeamID))
		usePreMissionComms = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Select_Closest_Players_From_Team_For_MP_Mission_Request - called for team: ")
		NET_PRINT(Convert_Team_To_String(paramTeamID))
		NET_PRINT(" [Join Method:")
		IF (inviteOntoMission)
			NET_PRINT(" (Use Invitation)")
		ELSE
			NET_PRINT(" (Force Onto Mission)")
		ENDIF
		IF (Should_MP_Mission_Team_Use_A_PreMission_Txtmsg(theMission, paramTeamID))
			NET_PRINT(" (Use TxtMsg)")
		ENDIF
		IF (Should_MP_Mission_Team_Use_A_PreMission_Phonecall(theMission, paramTeamID))
			NET_PRINT(" (Use Phonecall)")
		ENDIF
		NET_PRINT("]")
		NET_NL()
	#ENDIF

	// Initial Capacity Checking for all players on this team
	INT numActivePlayers	= 0
	INT numConfirmedPlayers	= 0
	INT numJoiningPlayers	= 0
	INT maxAllowedFromTeam	= Get_Maximum_Team_Members_For_MP_Mission_Request(missionIdData, paramTeamID)
	
	Gather_MP_Mission_Request_Capacity_Info_For_Team(paramArrayPos, paramTeamID, numActivePlayers, numConfirmedPlayers, numJoiningPlayers, paramInfo)
	
	// Check if any more players are allowed to be offered this mission
	IF NOT (Can_Any_More_Players_Be_Selected_To_Join_MP_Mission_Request(paramArrayPos, paramTeamID, inviteOntoMission, numActivePlayers, numConfirmedPlayers, numJoiningPlayers, maxAllowedFromTeam))
		EXIT
	ENDIF

	// Get the arrays of players reserved and required for this mission
	INT reservedPlayersBitfield		= Get_MP_Mission_Request_Reserved_Players(paramArrayPos)
	INT reservedByPlayerBitfield	= Get_MP_Mission_Request_Reserved_By_Player_Players(paramArrayPos)
	
	// Make sure all reserved players are still available
	BOOL			playerNowNotAvailable	= FALSE
	INT				tempLoop				= 0
	PLAYER_INDEX	thePlayer
	
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (paramTeamID = TEAM_INVALID)
		//OR (IS_BIT_SET(paramInfo.bitsAllTeamMembers[paramTeamID], tempLoop))
			// Check if the player has been reserved to go on the mission
			IF (IS_BIT_SET(reservedPlayersBitfield, tempLoop))
				// Make sure this player isn't already on the mission or joining the mission
				thePlayer = INT_TO_PLAYERINDEX(tempLoop)
				
				IF NOT (Is_Player_Joining_MP_Mission_Request_Slot(paramArrayPos, thePlayer))
				AND NOT (Is_Player_Confirmed_For_MP_Mission_Request_Slot(paramArrayPos, thePlayer))
				AND NOT (Is_Player_Active_On_MP_Mission_Request_Slot(paramArrayPos, thePlayer))
					// Player isn't on the mission yet, so check if still available
					IF (IS_BIT_SET(paramInfo.bitsUnavailablePlayers, tempLoop))
						// ...reserved player is no longer available, so free the player from the mission
						#IF IS_DEBUG_BUILD
							NET_PRINT("         Reserved Player is now unavailable, so freeing player: ") NET_PRINT(GET_PLAYER_NAME(thePlayer)) NET_NL()
						#ENDIF
						
						Free_Player_Because_Mission_Or_Team_Full(paramArrayPos, thePlayer)
						playerNowNotAvailable = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// If a player is no longer available, then regrab the 'reserved players' list.
	// This will ensure that freed players are then ignored by the rest of this routine.
	IF (playerNowNotAvailable)
		reservedPlayersBitfield = Get_MP_Mission_Request_Reserved_Players(paramArrayPos)
		
		// Also debug output the local details again which should display why a player became unavailable
		#IF IS_DEBUG_BUILD
			Debug_Output_Player_And_Team_Details(paramInfo)
		#ENDIF
	ENDIF

	// Go through all players (on this team only, if relevant) and get their distance from the mission location
	// NOTE: Some players are 'in pairs' and should only join the mission if there is space for both
	
	// Stored in the 'requiredSpaces' field to indicate this player's space is already reserved
	CONST_INT	ALREADY_RESERVED_FOR_MISSION		-1
	CONST_INT	IGNORE_ARRAY_POSITION				-1
	
	CONST_FLOAT	WORKAROUND_ADDITION_FOR_NON_RESERVED_BY_PLAYER_PLAYERS_m		10000.0
	
	m_structClosestPlayers closestPlayers[NUM_NETWORK_PLAYERS]
	
	// Get the Mission Location
	MP_MISSION_DATA	theMissionData	= Get_MP_Mission_Request_Slot_Mission_Data(paramArrayPos)
	VECTOR			missionLocation	= theMissionData.mdPrimaryCoords
	
	// Fill the array with the distances for all relevant Players from the mission location
	FLOAT thisDistance = 999.9
	
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		closestPlayers[tempLoop].playerIndexAsInt	= IGNORE_ARRAY_POSITION
		closestPlayers[tempLoop].reservedByPlayer	= FALSE
	
		IF (paramTeamID = TEAM_INVALID)
		//OR (IS_BIT_SET(paramInfo.bitsAllTeamMembers[paramTeamID], tempLoop))
			// Check if this player has been reserved to go on the mission
			IF (IS_BIT_SET(reservedPlayersBitfield, tempLoop))
				thePlayer = INT_TO_PLAYERINDEX(tempLoop)
				
				// Make sure this player isn't already on the mission or joining the mission
				IF NOT (Is_Player_Joining_MP_Mission_Request_Slot(paramArrayPos, thePlayer))
				AND NOT (Is_Player_Confirmed_For_MP_Mission_Request_Slot(paramArrayPos, thePlayer))
				AND NOT (Is_Player_Active_On_MP_Mission_Request_Slot(paramArrayPos, thePlayer))
					// Store the initial distances in the array in their playerIndex arrayPosition
					closestPlayers[tempLoop].playerIndexAsInt	= tempLoop
					closestPlayers[tempLoop].requiredSpaces		= 1
					
					// Get the distance between the player and the Mission Location
					thisDistance = GET_DISTANCE_BETWEEN_COORDS(missionLocation, GET_PLAYER_COORDS(thePlayer))
					closestPlayers[tempLoop].theDistance = thisDistance
					
					// If this player is NOT Reserved By Player, then add a big value to the distance
					// THIS IS A CHEAT METHOD: It leaves required players with much smaller distances than non-required players
					//		and this should allow the rest of this routine to continue to work as is while still priortising
					//		Reserved By Player players in a closest first with 'paired player' method
					IF (IS_BIT_SET(reservedByPlayerBitfield, tempLoop))
						// ...required player
						closestPlayers[tempLoop].reservedByPlayer = TRUE
					ELSE
						// ...not a required player
						closestPlayers[tempLoop].theDistance += WORKAROUND_ADDITION_FOR_NON_RESERVED_BY_PLAYER_PLAYERS_m
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Select_Closest_Players_From_Team_For_MP_Mission_Request. Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
		NET_PRINT("......Gather Distances (players that are not 'Reserved By Player' get ")
		NET_PRINT_FLOAT(WORKAROUND_ADDITION_FOR_NON_RESERVED_BY_PLAYER_PLAYERS_m)
		NET_PRINT("m added to distance so that Reserved_By_Player players are sorted to the top")
		NET_NL()
		
		REPEAT NUM_NETWORK_PLAYERS tempLoop
			IF NOT (closestPlayers[tempLoop].playerIndexAsInt = IGNORE_ARRAY_POSITION)
				NET_PRINT("      ArrayPos: ")
				NET_PRINT_INT(tempLoop)
				NET_PRINT("  [")
				thePlayer = INT_TO_PLAYERINDEX(tempLoop)
				NET_PRINT(GET_PLAYER_NAME(thePlayer))
				NET_PRINT(" (")
				NET_PRINT(Convert_Team_To_String(GET_PLAYER_TEAM(thePlayer)))
				NET_PRINT(")]   Distance: ")
				NET_PRINT_FLOAT(closestPlayers[tempLoop].theDistance)
				NET_PRINT("  ")
				IF (closestPlayers[tempLoop].reservedByPlayer)
					NET_PRINT("[RESERVED BY PLAYER]")
				ENDIF
				NET_NL()
			ENDIF
		ENDREPEAT	
	#ENDIF
	
	// Ignore any Group routines if the mission requires specific players only
	BOOL missionRequiresSpecificPlayersOnly = Should_MP_Mission_Team_Allow_Specific_Players_Only(theMission, paramTeamID)
		
	#IF IS_DEBUG_BUILD
		IF (missionRequiresSpecificPlayersOnly)
			NET_PRINT("......NOT DOING PAIRED PLAYERS CHECK: Mission is for specific players only") NET_NL()
		ENDIF
	#ENDIF
	
	// Tag a player's paired player unless the mission requires specific players only
	INT				thePairedPlayerAsInt	= 0
	PLAYER_INDEX	thePairedPlayer
	BOOL			teamAllowsPairedPlayers	= FALSE		// KGM 5/3/13 - always FALSE for Freemode
	BOOL			playerHasPairedPlayer	= FALSE		// KGM 5/3/13 - always FALSE for Freemode
	
	IF (teamAllowsPairedPlayers)
	AND NOT (missionRequiresSpecificPlayersOnly)
		REPEAT NUM_NETWORK_PLAYERS tempLoop
			IF NOT (closestPlayers[tempLoop].playerIndexAsInt = IGNORE_ARRAY_POSITION)
			AND NOT(closestPlayers[tempLoop].requiredSpaces > 1)
				// NOTE: Can still use tempLoop because the data is currently still stored in playerIndex order
				thePlayer = INT_TO_PLAYERINDEX(tempLoop)
				
				// ...found some valid player data, so check for the player's 'paired player'
				IF (playerHasPairedPlayer)
					thePairedPlayer 		= INVALID_PLAYER_INDEX()		// KGM 5/3/13 - no paired players for Freemode
					thePairedPlayerAsInt	= NATIVE_TO_INT(thePairedPlayer)
					
					// Is the paired player's data stored?
					IF (closestPlayers[thePairedPlayerAsInt].playerIndexAsInt = IGNORE_ARRAY_POSITION)
						// ...the paired player's data isn't stored
						// If the paired player is already on the mission then set this player's distance to 0 to get the player on the mission as a priority
						// NOTE: not sure how this could happen, just covering all bases
						IF (Is_Player_Joining_MP_Mission_Request_Slot(paramArrayPos, thePairedPlayer))
						OR (Is_Player_Confirmed_For_MP_Mission_Request_Slot(paramArrayPos, thePairedPlayer))
						OR (Is_Player_Active_On_MP_Mission_Request_Slot(paramArrayPos, thePairedPlayer))
							// ...the paired player is already on the mission (somehow)
							#IF IS_DEBUG_BUILD
								NET_PRINT("           Players's Paired Player is already on the mission, so allow this Player to join the mission as a priority: ")
								NET_PRINT(GET_PLAYER_NAME(thePlayer))
								NET_PRINT("   (Paired Player: ")
								NET_PRINT(GET_PLAYER_NAME(thePairedPlayer))
								NET_PRINT(")")
								NET_NL()
							#ENDIF
							
							// Set this Player distance as 0, to represent being at the mission location and to give him priority for going on the mission
							closestPlayers[tempLoop].theDistance = 0.0
						ELSE
							// ...the paired player is not on the mission
							// Is the paired player available to join the mission?
							IF (IS_BIT_SET(paramInfo.bitsFreePlayers, thePairedPlayerAsInt))
								// ...yes, the paired player is available to join the mission, so reserve him for the mission
								#IF IS_DEBUG_BUILD
									NET_PRINT("           Players's Paired Player isn't reserved for the mission but is free, so reserve the paired player to join the mission: ")
									NET_PRINT(GET_PLAYER_NAME(thePlayer))
									NET_PRINT("   (Paired Player: ")
									NET_PRINT(GET_PLAYER_NAME(thePairedPlayer))
									NET_PRINT(")")
									NET_NL()
								#ENDIF
								
								// Reserve the Paired Player for the mission
								Reserve_Player_By_Game_For_MP_Mission_Request_Slot_And_Update_Player_Control_Data(paramArrayPos, thePairedPlayer, paramInfo)
								
								// Generate Distance data
								closestPlayers[thePairedPlayerAsInt].playerIndexAsInt	= thePairedPlayerAsInt
								closestPlayers[thePairedPlayerAsInt].requiredSpaces		= 1
								
								// Get the distance between the player and the Mission Location
								thisDistance = GET_DISTANCE_BETWEEN_COORDS(missionLocation, GET_PLAYER_COORDS(thePairedPlayer))
								closestPlayers[thePairedPlayerAsInt].theDistance = thisDistance + WORKAROUND_ADDITION_FOR_NON_RESERVED_BY_PLAYER_PLAYERS_m
							ELSE
								// ...no, the paired player is not available to join the mission, so this player can't join it either
								#IF IS_DEBUG_BUILD
									NET_PRINT("           Player's Paired Player isn't reserved for the mission, so don't allow this Player to join the mission: ")
									NET_PRINT(GET_PLAYER_NAME(thePlayer))
									NET_PRINT("   (Paired Player: ")
									NET_PRINT(GET_PLAYER_NAME(thePairedPlayer))
									NET_PRINT(")")
									NET_NL()
								#ENDIF
								
								// Clear the Players's data since the Paired Player's data isn't stored
								closestPlayers[tempLoop].playerIndexAsInt = IGNORE_ARRAY_POSITION
								
								// Free this player as reserved for this mission
								// Don't think I need to bother updating the player and team control bits - they'll get sorted out next frame when they are generated again
								Free_Player_Because_Mission_Or_Team_Full(paramArrayPos, thePlayer)
							ENDIF
						ENDIF
					ENDIF
					
					// Check again if the paired player's data is stored - it may have just been updated
					IF NOT (closestPlayers[thePairedPlayerAsInt].playerIndexAsInt = IGNORE_ARRAY_POSITION)
						// ...the paired player's data is stored, so both can join the mission
						// Give both players the same distance and update their requiredSpaces
						IF (closestPlayers[tempLoop].theDistance < closestPlayers[thePairedPlayerAsInt].theDistance)
							// ...this Player's distance is shorter, so update the Paired Player's distance
							closestPlayers[thePairedPlayerAsInt].theDistance = closestPlayers[tempLoop].theDistance
						ELSE
							// ...the Paired Player's distance is shorter, so update this Player's distance
							closestPlayers[tempLoop].theDistance = closestPlayers[thePairedPlayerAsInt].theDistance
						ENDIF
						
						// Mark both this Player's and Paired Player's requiredSpaces to 2 (after sorting into distance order one of these will be set to ALREADY_RESERVED_FOR_MISSION)
						closestPlayers[thePairedPlayerAsInt].requiredSpaces	= 2
						closestPlayers[tempLoop].requiredSpaces				= 2
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("......Tagging Player's Paired Players") NET_NL()
			
			REPEAT NUM_NETWORK_PLAYERS tempLoop
				IF NOT (closestPlayers[tempLoop].playerIndexAsInt = IGNORE_ARRAY_POSITION)
					NET_PRINT("      ArrayPos: ")
					NET_PRINT_INT(tempLoop)
					NET_PRINT("  [")
					thePlayer = INT_TO_PLAYERINDEX(tempLoop)
					NET_PRINT(GET_PLAYER_NAME(thePlayer))
					NET_PRINT(" (")
					NET_PRINT(Convert_Team_To_String(GET_PLAYER_TEAM(thePlayer)))
					NET_PRINT(")]   Distance: ")
					NET_PRINT_FLOAT(closestPlayers[tempLoop].theDistance)
					NET_PRINT("  ")
					IF (closestPlayers[tempLoop].reservedByPlayer)
						NET_PRINT("[RESERVED BY PLAYER]")
					ENDIF
					NET_NL()
				ENDIF
			ENDREPEAT	
		#ENDIF
	ENDIF
	
	// Sort the array into distance order
	// ...start by packing the data into a contiguous list
	INT numClosestPlayers = 0
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF NOT (closestPlayers[tempLoop].playerIndexAsInt = IGNORE_ARRAY_POSITION)
			closestPlayers[numClosestPlayers] = closestPlayers[tempLoop]
			numClosestPlayers++
		ENDIF
	ENDREPEAT
	
	// ...quick exit if there are no players left
	IF (numClosestPlayers = 0)
		#IF IS_DEBUG_BUILD
			NET_PRINT("      QUITTING - THERE ARE NOW NO VALID PLAYERS TO BE PLACED ON THE MISSION.") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	// ...re-arrange the data into distance order (there shouldn't be many, so use a simple sort)
	m_structClosestPlayers	holdingData
	BOOL					continueSorting	= TRUE
	INT						tempLoopPlusOne	= 0
	
	WHILE (continueSorting)
		continueSorting = FALSE
		
		REPEAT numClosestPlayers tempLoop
			tempLoopPlusOne = tempLoop + 1
			IF (tempLoopPlusOne < numClosestPlayers)
				// Swap?
				IF (closestPlayers[tempLoopPlusOne].theDistance < closestPlayers[tempLoop].theDistance)
					// Yes, Swap.
					holdingData = closestPlayers[tempLoopPlusOne]
					closestPlayers[tempLoopPlusOne] = closestPlayers[tempLoop]
					closestPlayers[tempLoop] = holdingData
					
					// Another Pass will be required if the data order has changed
					continueSorting = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDWHILE
	
	// ...finally, look for players that need to join the mission together
	BOOL	processingTogetherPlayers	= FALSE
	INT		numTogetherPlayers			= 0
	
	REPEAT numClosestPlayers tempLoop
		IF (processingTogetherPlayers)
			// This is more of a set of together players, so mark this player as already reserved for the mission
			closestPlayers[tempLoop].requiredSpaces = ALREADY_RESERVED_FOR_MISSION
			numTogetherPlayers--
			
			IF (numTogetherPlayers <= 0)
				// ...this is the last of the current set of together players
				processingTogetherPlayers = FALSE
			ENDIF
		ELSE
			IF (closestPlayers[tempLoop].requiredSpaces > 1)
				numTogetherPlayers = closestPlayers[tempLoop].requiredSpaces
				processingTogetherPlayers = TRUE
				
				// This is the first of a set of players that need to enter the mission together, so leave the requiredSpaces set
				numTogetherPlayers--
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("......After sorting and tying together players that join the mission together") NET_NL()
		
		REPEAT numClosestPlayers tempLoop
			NET_PRINT("      ArrayPos: ")
			NET_PRINT_INT(tempLoop)
			// Indicate a player that joins with the previous player
			NET_PRINT("  ")
			IF (closestPlayers[tempLoop].requiredSpaces = ALREADY_RESERVED_FOR_MISSION)
				NET_PRINT("...")
			ENDIF
			NET_PRINT("[")
			thePlayer = INT_TO_PLAYERINDEX(closestPlayers[tempLoop].playerIndexAsInt)
			NET_PRINT(GET_PLAYER_NAME(thePlayer))
			NET_PRINT(" (")
			NET_PRINT(Convert_Team_To_String(GET_PLAYER_TEAM(thePlayer)))
			NET_PRINT(")]")
			// Display distance unless a member of a Group (except primary member)
			IF NOT (closestPlayers[tempLoop].requiredSpaces = ALREADY_RESERVED_FOR_MISSION)
				NET_PRINT("       Distance: ")
				NET_PRINT_FLOAT(closestPlayers[tempLoop].theDistance)
				NET_PRINT("  ")
				IF (closestPlayers[tempLoop].reservedByPlayer)
					NET_PRINT("[RESERVED BY PLAYER]")
				ENDIF
			ENDIF
			// Indicate a player that joins with the previous player
			IF (closestPlayers[tempLoop].requiredSpaces = ALREADY_RESERVED_FOR_MISSION)
				NET_PRINT("      - JOINS WITH PLAYER ABOVE")
				IF (closestPlayers[tempLoop].reservedByPlayer)
					NET_PRINT("  [RESERVED BY PLAYER]")
				ENDIF
			ENDIF
			NET_NL()
		ENDREPEAT	
	#ENDIF

	// Distribute as many of these players onto the mission as it will take, taking 'together players' into account
	// ...work out how many players can still join the mission (if players aren't forced onto the mission then set this to a high value)
	IF (inviteOntoMission)
		maxAllowedFromTeam = 100
	ENDIF
	
	// Choose the Join method for Required players
	// (Generally obey normal joining rules, but Required Players joining a Mission Flow 'at blip' mission should autojoin with no phonecall)
	BOOL reservedByPlayerPlayersGetInvitation	= inviteOntoMission
	BOOL reservedByPlayerPlayersGetComms		= usePreMissionComms
	
	IF (Get_MP_Mission_Request_Source(paramArrayPos) = MP_MISSION_SOURCE_MISSION_FLOW_BLIP)
		reservedByPlayerPlayersGetInvitation	= FALSE
		reservedByPlayerPlayersGetComms			= FALSE
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("      This mission is a mission flow 'at blip' mission, so any Reserved By Player players have voted to start it and should be forced on with no communication.")
			NET_NL()
		#ENDIF
	ENDIF
	
	// Should Reserved By Player Players be the only ones that initially join the mission?
	// (At launch, this will be true for missions with a pre-mission cutscene, or for missions that specifically request this)
	BOOL reservedByPlayerPlayersOnly = FALSE
	IF (Is_MP_Mission_Request_Slot_Status_Received(paramArrayPos))
		reservedByPlayerPlayersOnly = Check_If_Reserved_By_Player_Players_Get_Join_Priority_At_Launch(paramArrayPos)
	ENDIF

	#IF IS_DEBUG_BUILD
		IF (reservedByPlayerPlayersOnly)
			NET_PRINT("      This mission only allows Reserved By Player players to initially join the mission.") NET_NL()
			IF (Does_MP_Mission_Request_Have_Cutscene(paramArrayPos))
				NET_PRINT("         - because mission has a pre-mission cutscene") NET_NL()
			ENDIF
			IF (Should_MP_Mission_Variation_Reserved_By_Players_Join_First(theMission, theVariation))
				NET_PRINT("         - because mission specifies that Reserved By Player players should join first]") NET_NL()
			ENDIF
		ENDIF
	#ENDIF
	
	INT		currentInvitedPlayers		= numActivePlayers + numConfirmedPlayers + numJoiningPlayers
	INT		playersPlusJoiningGroup		= 0
	INT		playerAsInt					= 0
	BOOL	ignoreThisGroup				= FALSE
	BOOL	freeThisPlayer				= FALSE
	BOOL	playersMustBeWithinRange	= FALSE
	FLOAT	playerInclusionRadius		= Get_MP_Mission_Variation_Max_Radius_For_Including_Players(theMission, theVariation)
	
	IF (playerInclusionRadius >= 0.0)
		playersMustBeWithinRange = TRUE
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("      THIS MISSION ONLY TAKES PLAYERS WITHIN A CERTAIN RADIUS. [Location = ")
			NET_PRINT_VECTOR(missionLocation)
			NET_PRINT("]  [Radius = ")
			NET_PRINT_FLOAT(playerInclusionRadius)
			NET_PRINT("]")
			NET_NL()
		#ENDIF
	ENDIF
	
	// NOTE: A group will be ignored if the whole group can't make it onto the mission (ie: Player + Paired Player, or mission full for team)
	BOOL thisPlayerGetsInvitation	= inviteOntoMission
	BOOL thisPlayerGetsComms		= usePreMissionComms
	BOOL allowThisPlayerToJoin		= FALSE
	
	REPEAT numClosestPlayers tempLoop
		playerAsInt		= closestPlayers[tempLoop].playerIndexAsInt
		thePlayer		= INT_TO_PLAYERINDEX(playerAsInt)
		freeThisPlayer	= FALSE
		
		// If ignoring this Group, check if all of the Group has now been ignored
		IF (ignoreThisGroup)
			IF NOT (closestPlayers[tempLoop].requiredSpaces = ALREADY_RESERVED_FOR_MISSION)
				ignoreThisGroup = FALSE
			ENDIF
		ENDIF
		
		// Only do this if the group is not being ignored
		IF NOT (ignoreThisGroup)
			// Group is not being ignored
			// Only do this test for individual players or for group 'leaders'
			// Need to test if there is room for this player (and his paired players, if applicable)
			IF NOT (closestPlayers[tempLoop].requiredSpaces = ALREADY_RESERVED_FOR_MISSION)
				playersPlusJoiningGroup = currentInvitedPlayers + closestPlayers[tempLoop].requiredSpaces
				IF (playersPlusJoiningGroup > maxAllowedFromTeam)
					#IF IS_DEBUG_BUILD
						NET_PRINT("           There is no space on the mission for this player")
						IF (closestPlayers[tempLoop].requiredSpaces > 1)
							NET_PRINT("'s whole group")
						ENDIF
						NET_PRINT(": ")
						NET_PRINT(GET_PLAYER_NAME(thePlayer))
						IF (closestPlayers[tempLoop].requiredSpaces > 1)
							NET_PRINT(" - IGNORING GROUP")
						ELSE
							NET_PRINT(" - IGNORING PLAYER")
						ENDIF
						NET_NL()
					#ENDIF
					
					ignoreThisGroup = TRUE
				ENDIF
			ENDIF
			
			IF NOT (ignoreThisGroup)
				// The basic details for when all reserved players can join, not just Reserved by Player players
				allowThisPlayerToJoin		= TRUE
				thisPlayerGetsInvitation	= inviteOntoMission
				thisPlayerGetsComms			= usePreMissionComms
				
				// If Reserved By Player players only are being added to the mission then only add this player if Reserved By Player
				// NOTE: I've used the distance here instead of the reservedByPlayer flag. Can't remember exactly why, but I think it's so that Paired Players also get immediately put on even if not 'reservedByPlayer'
				IF (reservedByPlayerPlayersOnly)
					IF NOT (closestPlayers[tempLoop].theDistance < WORKAROUND_ADDITION_FOR_NON_RESERVED_BY_PLAYER_PLAYERS_m)
						// ...this is not a Reserved By Player player, so the player can't join at the moment
						allowThisPlayerToJoin = FALSE
					ENDIF
				ENDIF
				
				// If the player is still allowed to join, perform the distance check if required
				IF (allowThisPlayerToJoin)
					IF (playersMustBeWithinRange)
						// Non-Reserved_By_Player players must be within range (taking workaround distance into account)
						// Reserved By Player players should always join
						IF (closestPlayers[tempLoop].theDistance >= WORKAROUND_ADDITION_FOR_NON_RESERVED_BY_PLAYER_PLAYERS_m)
							// ...player is not tagged as 'Reserved By Player' so need to take the workaround value into account
							IF (closestPlayers[tempLoop].theDistance > (playerInclusionRadius + WORKAROUND_ADDITION_FOR_NON_RESERVED_BY_PLAYER_PLAYERS_m))
								allowThisPlayerToJoin	= FALSE
								freeThisPlayer			= TRUE
								
								#IF IS_DEBUG_BUILD
									NET_PRINT("      This player is outwith the inclusion radius - IGNORING PLAYER") NET_NL()
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				IF (allowThisPlayerToJoin)
					// If this is a Reserved By Player player, then choose the joining method
					// NOTE: In this case, I think I'm using the flag so that the joining method can be different between a Reserved By Player player and a non-Reserved_By_Player Paired Player
					//       BUT - this may just be an inconsistency that needs addressed if a problem occurs. I'm leaving it as-is for now.
					IF (closestPlayers[tempLoop].reservedByPlayer)
						// ...this is a Reserved By Player player, so use the appropriate join method
						thisPlayerGetsInvitation	= reservedByPlayerPlayersGetInvitation
						thisPlayerGetsComms			= reservedByPlayerPlayersGetComms
					ENDIF
					
					// The player has been reserved for this mission and is allowed to join at the moment, so get the player onto the mission
					Issue_Broadcast_For_Player_To_Join_MP_Mission_Request(paramArrayPos, thePlayer, thisPlayerGetsInvitation, thisPlayerGetsComms)
					currentInvitedPlayers++
				ENDIF
			ENDIF
		ENDIF
		
		// If this player is in a group that is being ignored, then free this player from the mission
		IF (ignoreThisGroup)
		OR (freeThisPlayer)
			// Free this player as reserved for this mission
			// Don't think I need to bother updating the player and team control bits - they'll get sorted out next frame when they are generated again
			Free_Player_Because_Mission_Or_Team_Full(paramArrayPos, thePlayer)
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Send join requests to all reserved players of a specific Team
//
// INPUT PARAMS:		paramArrayPos			Array Position for the mission
//						paramTeamID				Team ID to send invitations to
//						paramAtLaunch			TRUE if the Team is Joining at Launch, FALSE if joining later in mission
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
// RETURN VALUE:		BOOL					TRUE if the Team Join Requests were sent OK, otherwise FALSE
FUNC BOOL Send_Team_Join_Requests_For_MP_Mission_Request(INT paramArrayPos, INT paramTeamID, BOOL paramAtLaunch, m_structPlayerAndTeamInfoMC &paramInfo)

	// Although now unreferenced - the paramAtLaunch flag has potential to be used, so I'll leave it as-is for now
	paramAtlaunch = paramAtLaunch

//	// Shouldn't receive an illegal ID, but just in case
//	// KGM 29/8/12: TEAM_INVALID is no longer an illegal ID - required by missions that aren't team-based.
//	IF (paramTeamID >= MAX_NUM_TEAMS)
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("...KGM MP [MControl]: Send_Team_Join_Requests_For_MP_Mission_Request() - ERROR: TeamID is illegal.") NET_NL()
//			SCRIPT_ASSERT("Send_Team_Join_Requests_For_MP_Mission_Request(): Illegal Team ID. Tell Keith.")
//		#ENDIF
//	ENDIF
	
	// The team join method is valid
	Select_Closest_Players_From_Team_For_MP_Mission_Request(paramArrayPos, paramTeamID, paramInfo)
	
	// All OK
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Choose one of the available Crook Teams not already on the mission to send the Join request to
//
// INPUT PARAMS:		paramArrayPos			Array Position for the mission
//						paramAtLaunch			TRUE if the Team is Joining at Launch, FALSE if joining later in mission
//						paramAllowCops			TRUE if Cops are allowed to be one of the random teams, FALSE if Crooks only
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
//						paramChosenTeam			Team ID of the Randomly Chosen Team
// RETURN VALUE:		BOOL					TRUE if the Team Join Requests were sent OK, otherwise FALSE
FUNC BOOL Send_Team_Join_Requests_For_MP_Mission_Request_To_Random_Team(INT paramArrayPos, BOOL paramAtLaunch, BOOL paramAllowCops, m_structPlayerAndTeamInfoMC &paramInfo, INT &paramChosenTeam)

	IF (paramAllowCops)
	// fool compiler
	ENDIF

	// Find out how many Crook teams are Reserved for this mission but are not yet on the mission
//	INT requestedTeamsBitfield	= Get_MP_Mission_Request_Requested_Teams(paramArrayPos)
//	INT reservedTeamsBitfield	= Get_MP_Mission_Request_Reserved_Teams(paramArrayPos)
//	INT joiningTeamsBitfield	= Get_MP_Mission_Request_Joinable_Teams(paramArrayPos)
	
	INT numTeamsNotJoined = 0
	//INT teamsNotJoined[MAX_NUM_TEAMS]
	
	INT numRequestedTeamsNotJoined = 0
	//INT requestedTeamsNotJoined[MAX_NUM_TEAMS]
	
	// Clear my working array
//	INT tempLoop = 0
//	REPEAT MAX_NUM_TEAMS tempLoop
//		requestedTeamsNotJoined[tempLoop]	= TEAM_INVALID
//		teamsNotJoined[tempLoop]			= TEAM_INVALID
//	ENDREPEAT
	
	// Fill the working array
//	BOOL includeThisTeam = TRUE
//	REPEAT MAX_NUM_TEAMS tempLoop
//		includeThisTeam = TRUE
//		
////		IF NOT (paramAllowCops)
////		AND (tempLoop = TEAM_COP)
////			includeThisTeam = FALSE
////		ENDIF
//		
//		IF (includeThisTeam)
//			IF (IS_BIT_SET(reservedTeamsBitfield, tempLoop))
//				IF NOT (IS_BIT_SET(joiningTeamsBitfield, tempLoop))
//					// Team is reserved but not joined, so update my working array
//					teamsNotJoined[numTeamsNotJoined] = tempLoop
//					numTeamsNotJoined++
//					
//					// Requested Teams not joined
//					IF (IS_BIT_SET(requestedTeamsBitfield, tempLoop))
//						requestedTeamsNotJoined[numRequestedTeamsNotJoined] = tempLoop
//						numRequestedTeamsNotJoined++
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDREPEAT
	
	// Sanity Check - Ensure there are some Teams that can be picked
	IF (numTeamsNotJoined = 0)
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("...KGM MP [MControl]: Send_Team_Join_Requests_For_MP_Mission_Request_To_Random_Team() - ERROR: There are no Teams reserved but not joined.") NET_NL()
//			NET_PRINT("      Teams Reserved") NET_NL()
//			Debug_Output_Team_Names_From_Bitfield(reservedTeamsBitfield)
//			NET_PRINT("      Teams Joined") NET_NL()
//			Debug_Output_Team_Names_From_Bitfield(joiningTeamsBitfield)
//			NET_PRINT("      Teams Requested") NET_NL()
//			Debug_Output_Team_Names_From_Bitfield(requestedTeamsBitfield)
//			NET_PRINT("      Cops Allowed? ")
//			IF (paramAllowCops)
//				NET_PRINT("YES")
//			ELSE
//				NET_PRINT("NO")
//			ENDIF
//		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// If there are requested teams, then choose from them first
	#IF IS_DEBUG_BUILD
		BOOL choseRequestedTeam = FALSE
	#ENDIF
	
//	INT randomInt = 0
	IF (numRequestedTeamsNotJoined > 0)
		// ...there are requested teamd not joined, so give priority to one of those
//		randomInt = GET_RANDOM_INT_IN_RANGE(0, numRequestedTeamsNotJoined)
		//paramChosenTeam = requestedTeamsNotJoined[randomInt]
		
		#IF IS_DEBUG_BUILD
			choseRequestedTeam = TRUE
		#ENDIF
	ELSE
		// ...choose one of the other teams
		//randomInt = GET_RANDOM_INT_IN_RANGE(0, numTeamsNotJoined)
		//paramChosenTeam = teamsNotJoined[randomInt]
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Send_Team_Join_Requests_For_MP_Mission_Request_To_Random_Team() randomly chose this ")
		IF (choseRequestedTeam)
			NET_PRINT("requested team: ")
		ELSE
			NET_PRINT("team: ")
		ENDIF
		NET_PRINT(Convert_Team_To_String(paramChosenTeam))
		IF (paramAllowCops)
			NET_PRINT("   [NOTE: Cops Allowed]")
		ELSE
			NET_PRINT("   [NOTE: Crook Teams Only]")
		ENDIF
		NET_NL()
	#ENDIF
	
	// Forward this Team on to the Player Selection Routine
	IF NOT (Send_Team_Join_Requests_For_MP_Mission_Request(paramArrayPos, paramChosenTeam, paramAtLaunch, paramInfo))
		RETURN FALSE
	ENDIF
	
	// All ok
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Send the Join Requests to the Launch Teams for this mission
//
// INPUT PARAMS:		paramArrayPos			Array Position for the mission
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
// RETURN VALUE:		BOOL					TRUE if all the required mission details were generated, otherwise FALSE
//
// NOTES:	This is used to get the Launch Teams onto the mission.
FUNC BOOL Issue_Launch_Join_Requests_For_MP_Mission_Request(INT paramArrayPos, m_structPlayerAndTeamInfoMC &paramInfo)
	
	MP_MISSION	theMission		= Get_MP_Mission_Request_MissionID(paramArrayPos)
	INT			theVariation	= Get_MP_Mission_Request_MissionVariation(paramArrayPos)
	
	g_eMPLaunchTeams theLaunchTeams = Get_MP_Mission_Variation_Launch_Teams(theMission, theVariation)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcasting Initial Set Of Join Requests For Mission. Slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_PRINT("  [Launch Teams: ")
		NET_PRINT(Convert_Launch_Teams_To_String(theLaunchTeams))
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
	// Sanity Checks
	SWITCH (theLaunchTeams)
		// Illegal value
		CASE NO_MP_LAUNCH_TEAMS
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MControl]: The Launch Teams haven't been set up for mission: ")
				NET_PRINT(GET_MP_MISSION_NAME(theMission))
				NET_NL()
				
				SCRIPT_ASSERT("Issue_Launch_Join_Requests_For_MP_Mission_Request() - ERROR: Launch Teams for mission not setup. Ignoring this mission request. Tell Keith.")
			#ENDIF
			
			RETURN FALSE
			
		// Team-Based Missions Only should use these
		CASE MP_LAUNCH_ALL_TEAMS
		CASE MP_LAUNCH_ALL_COPS
		CASE MP_LAUNCH_ALL_GANGS
		CASE MP_LAUNCH_ONE_TEAM
		CASE MP_LAUNCH_ONE_GANG
			IF NOT (Check_If_Players_Are_In_Teams_For_Mission(theMission))
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MControl]: Mission isn't team-based (")
					NET_PRINT(GET_MP_MISSION_NAME(theMission))
					NET_PRINT(") but launch team config is a team-based value: ")
					NET_PRINT(Convert_Launch_Teams_To_String(theLaunchTeams))
					NET_NL()
					
					SCRIPT_ASSERT("Issue_Launch_Join_Requests_For_MP_Mission_Request() - ERROR: Launch Teams ID is illegal for a mission that isn't team-based. Changing to MP_LAUNCH_ALL_PLAYERS. Tell Keith.")
				#ENDIF
				
				theLaunchTeams = MP_LAUNCH_ALL_PLAYERS
			ENDIF
			BREAK
			
		// Only Missions that are not Team-Based should use these
		CASE MP_LAUNCH_ALL_PLAYERS
			IF (Check_If_Players_Are_In_Teams_For_Mission(theMission))
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MControl]: Mission is team-based (")
					NET_PRINT(GET_MP_MISSION_NAME(theMission))
					NET_PRINT(") but launch team config is for a mission that isn't team-based: ")
					NET_PRINT(Convert_Launch_Teams_To_String(theLaunchTeams))
					NET_NL()
					
					SCRIPT_ASSERT("Issue_Launch_Join_Requests_For_MP_Mission_Request() - ERROR: Launch Teams ID is illegal for a team-based mission. Changing to MP_LAUNCH_ALL_TEAMS. Tell Keith.")
				#ENDIF
				
				theLaunchTeams = MP_LAUNCH_ALL_TEAMS
			ENDIF
			BREAK
			
		// Unknown Team Join ID
		DEFAULT
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MControl]: Unknown Launch Teams: ")
				NET_PRINT(Convert_Launch_Teams_To_String(theLaunchTeams))
				NET_PRINT("  [")
				NET_PRINT(GET_MP_MISSION_NAME(theMission))
				NET_PRINT("] - Add them to the SWITCH statement")
				NET_NL()
				
				SCRIPT_ASSERT("Issue_Launch_Join_Requests_For_MP_Mission_Request() - ERROR: Launch Teams for mission are not recognised. Ignoring this mission request. Tell Keith.")
			#ENDIF
			
			RETURN FALSE
	ENDSWITCH
	
	// Get the Reserved and Requested Teams bitfield - this will be ignored if not required
//	INT reservedTeamsBitfield	= Get_MP_Mission_Request_Reserved_Teams(paramArrayPos)
//	INT requestedTeamsBitfield	= Get_MP_Mission_Request_Requested_Teams(paramArrayPos)
	BOOL missionIsLaunching = TRUE
	INT chosenTeam = TEAM_INVALID
	BOOL allowCopsAsRandomTeam = FALSE
	
	// Send the initial set of invitations - launch requested teams that have been reserved first
//	INT tempLoop = 0
	SWITCH (theLaunchTeams)
		CASE MP_LAUNCH_ALL_PLAYERS
			IF NOT (Send_Team_Join_Requests_For_MP_Mission_Request(paramArrayPos, TEAM_INVALID, missionIsLaunching, paramInfo))
				RETURN FALSE
			ENDIF
			BREAK
		
		CASE MP_LAUNCH_ALL_TEAMS
			// Start with Requested Teams that have also been Reserved
//			REPEAT MAX_NUM_TEAMS tempLoop
//				IF (IS_BIT_SET(requestedTeamsBitfield, tempLoop))
//					IF (IS_BIT_SET(reservedTeamsBitfield, tempLoop))
//						IF NOT (Send_Team_Join_Requests_For_MP_Mission_Request(paramArrayPos, tempLoop, missionIsLaunching, paramInfo))
//							RETURN FALSE
//						ENDIF
//						
//						Store_MP_Mission_Request_Joinable_Team(paramArrayPos, tempLoop)
//						
//						// Clear this team from the local Reserved Bitfield so that the loop below doesn't try to launch these teams again
//						CLEAR_BIT(reservedTeamsBitfield, tempLoop)
//				
//						// Update the First Gang On Mission flag if there are Primary and Secondary pre-mission communications
//						Update_First_Gang_On_Mission_Flag_When_Primary_And_Secondary_Comms(paramArrayPos, tempLoop)
//					ENDIF
//				ENDIF
//			ENDREPEAT
			
			// Any remaining Reserved Teams
//			REPEAT MAX_NUM_TEAMS tempLoop
//				IF (IS_BIT_SET(reservedTeamsBitfield, tempLoop))
//					IF NOT (Send_Team_Join_Requests_For_MP_Mission_Request(paramArrayPos, tempLoop, missionIsLaunching, paramInfo))
//						RETURN FALSE
//					ENDIF
//					
//					Store_MP_Mission_Request_Joinable_Team(paramArrayPos, tempLoop)
//				
//					// Update the First Gang On Mission flag if there are Primary and Secondary pre-mission communications
//					Update_First_Gang_On_Mission_Flag_When_Primary_And_Secondary_Comms(paramArrayPos, tempLoop)
//				ENDIF
//			ENDREPEAT
			BREAK
			
//		CASE MP_LAUNCH_ALL_COPS
//			IF NOT (Send_Team_Join_Requests_For_MP_Mission_Request(paramArrayPos, TEAM_COP, missionIsLaunching, paramInfo))
//				RETURN FALSE
//			ENDIF
//			
//			Store_MP_Mission_Request_Joinable_Team(paramArrayPos, TEAM_COP)
//				
//			// Update the First Gang On Mission flag if there are Primary and Secondary pre-mission communications
//			Update_First_Gang_On_Mission_Flag_When_Primary_And_Secondary_Comms(paramArrayPos, tempLoop)
//			BREAK
			
//		CASE MP_LAUNCH_ALL_GANGS
//			// Start with Requested Crook Gangs that have also been Reserved
//			REPEAT MAX_NUM_TEAMS tempLoop
//				IF NOT (tempLoop = TEAM_COP)
//					IF (IS_BIT_SET(requestedTeamsBitfield, tempLoop))
//						IF (IS_BIT_SET(reservedTeamsBitfield, tempLoop))
//							IF NOT (Send_Team_Join_Requests_For_MP_Mission_Request(paramArrayPos, tempLoop, missionIsLaunching, paramInfo))
//								RETURN FALSE
//							ENDIF
//							
//							Store_MP_Mission_Request_Joinable_Team(paramArrayPos, tempLoop)
//							
//							// Clear this team from the local Reserved Bitfield so that the loop below doesn't try to launch these gangs again
//							CLEAR_BIT(reservedTeamsBitfield, tempLoop)
//				
//							// Update the First Gang On Mission flag if there are Primary and Secondary pre-mission communications
//							Update_First_Gang_On_Mission_Flag_When_Primary_And_Secondary_Comms(paramArrayPos, tempLoop)
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDREPEAT
//			
//			// Any remaining Reserved Teams
//			REPEAT MAX_NUM_TEAMS tempLoop
//				IF NOT (tempLoop = TEAM_COP)
//					IF (IS_BIT_SET(reservedTeamsBitfield, tempLoop))
//						IF NOT (Send_Team_Join_Requests_For_MP_Mission_Request(paramArrayPos, tempLoop, missionIsLaunching, paramInfo))
//							RETURN FALSE
//						ENDIF
//						
//						Store_MP_Mission_Request_Joinable_Team(paramArrayPos, tempLoop)
//				
//						// Update the First Gang On Mission flag if there are Primary and Secondary pre-mission communications
//						Update_First_Gang_On_Mission_Flag_When_Primary_And_Secondary_Comms(paramArrayPos, tempLoop)
//					ENDIF
//				ENDIF
//			ENDREPEAT
//			BREAK
			
		CASE MP_LAUNCH_ONE_TEAM
			allowCopsAsRandomTeam = TRUE
			IF NOT (Send_Team_Join_Requests_For_MP_Mission_Request_To_Random_Team(paramArrayPos, missionIsLaunching, allowCopsAsRandomTeam, paramInfo, chosenTeam))
				RETURN FALSE
			ENDIF
			
			Store_MP_Mission_Request_Joinable_Team(paramArrayPos, chosenTeam)
				
			// Update the First Gang On Mission flag if there are Primary and Secondary pre-mission communications
			Update_First_Gang_On_Mission_Flag_When_Primary_And_Secondary_Comms(paramArrayPos, chosenTeam)
			BREAK
			
		CASE MP_LAUNCH_ONE_GANG
			allowCopsAsRandomTeam = FALSE
			IF NOT (Send_Team_Join_Requests_For_MP_Mission_Request_To_Random_Team(paramArrayPos, missionIsLaunching, allowCopsAsRandomTeam, paramInfo, chosenTeam))
				RETURN FALSE
			ENDIF
			
			Store_MP_Mission_Request_Joinable_Team(paramArrayPos, chosenTeam)
				
			// Update the First Gang On Mission flag if there are Primary and Secondary pre-mission communications
			Update_First_Gang_On_Mission_Flag_When_Primary_And_Secondary_Comms(paramArrayPos, chosenTeam)
			BREAK
			
		DEFAULT
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MControl]: Unknown Launch Teams: ")
				NET_PRINT(Convert_Launch_Teams_To_String(theLaunchTeams))
				NET_PRINT("  [")
				NET_PRINT(GET_MP_MISSION_NAME(theMission))
				NET_PRINT("] - Add them to the SWITCH statement")
				NET_NL()
				
				SCRIPT_ASSERT("Issue_Launch_Join_Requests_For_MP_Mission_Request() - ERROR: Launch Teams for mission are not recognised. Ignoring this mission request. Tell Keith.")
			#ENDIF
			
			RETURN FALSE
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Players Details After Launch Join requests. Slot: ") NET_PRINT_INT(paramArrayPos) NET_NL()
		Debug_Output_Missions_Player_Details(paramArrayPos)
	#ENDIF
	
	// All OK
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Reissue the Join Requests to all Launch Teams for this mission
//
// INPUT PARAMS:		paramArrayPos			Array Position for the mission
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
//
// NOTES:	This is used to get other players from the Launch Teams onto the mission if Required Players were shown a pre-mission cutscene.
PROC Reissue_Join_Requests_To_Launch_Teams_For_MP_Mission_Request(INT paramArrayPos, m_structPlayerAndTeamInfoMC &paramInfo)
	
	// This is still treated as 'at launch'
	BOOL missionIsLaunching = TRUE
	
	// If the mission is not team-based, re-issue the join request to all players
	IF NOT (Check_If_Players_Are_In_Teams_For_Mission(Get_MP_Mission_Request_MissionID(paramArrayPos)))
		// Players are not in teams
		// Ignore the return value
		Send_Team_Join_Requests_For_MP_Mission_Request(paramArrayPos, TEAM_INVALID, missionIsLaunching, paramInfo)
		
		EXIT
	ENDIF
	
	// Send Join Requests to all Teams that are already joinable for the mission
//	INT joinableTeamsBitfield = Get_MP_Mission_Request_Joinable_Teams(paramArrayPos)
	
//	INT tempLoop = 0
//	REPEAT MAX_NUM_TEAMS tempLoop
//		IF (IS_BIT_SET(joinableTeamsBitfield, tempLoop))
//			// Ignore the return value - there will be messages in the console log to indicate what is going wrong
//			Send_Team_Join_Requests_For_MP_Mission_Request(paramArrayPos, tempLoop, missionIsLaunching, paramInfo)
//		ENDIF
//	ENDREPEAT
			
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Send the secondary set of Join Requests to participants for this mission
//
// INPUT PARAMS:		paramArrayPos			Array Position for the mission
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
//
// NOTES:	This is used when the mission requests that the mid-mission teams join the mission.
PROC Issue_Secondary_Set_Of_Join_Requests_For_MP_Mission_Request(INT paramArrayPos, m_structPlayerAndTeamInfoMC &paramInfo)
	
	BOOL missionIsLaunching = FALSE
	
	// If the mission is not team-based, just send the request to all players
	IF (Check_If_Players_Are_In_Teams_For_Mission(Get_MP_Mission_Request_MissionID(paramArrayPos)))
		// ...mission is team-based
		// Send Join Requests to all Teams that are on the mission
		// It should be okay to send this to the Launch Teams also - the request will be ignored if the players are already all active
//		INT reservedTeamsBitfield	= Get_MP_Mission_Request_Reserved_Teams(paramArrayPos)
//		INT requestedTeamsBitfield	= Get_MP_Mission_Request_Requested_Teams(paramArrayPos)
		
//		INT tempLoop = 0
		
		// Initially Issue to all the Requested Teams first
//		REPEAT MAX_NUM_TEAMS tempLoop
//			IF (IS_BIT_SET(requestedTeamsBitfield, tempLoop))
//				IF (IS_BIT_SET(reservedTeamsBitfield, tempLoop))
//					// Ignore the return value when adding Secondary teams - there will be messages in the console log to indicate what is going wrong
//					Send_Team_Join_Requests_For_MP_Mission_Request(paramArrayPos, tempLoop, missionIsLaunching, paramInfo)
//					
//					// Clear the local Reserved Teams bitfield to ensure the function below ignores requested teams
//					CLEAR_BIT(reservedTeamsBitfield, tempLoop)
//					
//					// Update the First Gang On Mission flag if there are Primary and Secondary pre-mission communications
//					Update_First_Gang_On_Mission_Flag_When_Primary_And_Secondary_Comms(paramArrayPos, tempLoop)
//				ENDIF
//			ENDIF
//		ENDREPEAT
		
		// Issue to all the remaining Reserved Teams now
//		REPEAT MAX_NUM_TEAMS tempLoop
//			IF (IS_BIT_SET(reservedTeamsBitfield, tempLoop))
//				// Ignore the return value when adding Secondary teams - there will be messages in the console log to indicate what is going wrong
//				Send_Team_Join_Requests_For_MP_Mission_Request(paramArrayPos, tempLoop, missionIsLaunching, paramInfo)
//					
//				// Update the First Gang On Mission flag if there are Primary and Secondary pre-mission communications
//				Update_First_Gang_On_Mission_Flag_When_Primary_And_Secondary_Comms(paramArrayPos, tempLoop)
//			ENDIF
//		ENDREPEAT
	ELSE
		// ...mission is not team-based, so just issue the request to all players
		// Ignore the return value when adding Secondary teams - there will be messages in the console log to indicate what is going wrong
		Send_Team_Join_Requests_For_MP_Mission_Request(paramArrayPos, TEAM_INVALID, missionIsLaunching, paramInfo)
	ENDIF
	
	// Mark the State as 'active' again, instead of 'active, add Secondary Players'
	Store_MP_Mission_Request_Status_As_Active(paramArrayPos)
			
ENDPROC




// ===========================================================================================================
//      Players joining an existing Mission Request
// ===========================================================================================================

// PURPOSE: Check if this player can be added to an Offered or Active mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The player trying to join the mission
//						paramSourceID			The Mission Request Source
//						paramMissionID			The Mission ID that the player has requested to join
// RETURN PARAMS:		paramInfo				Additional general info about current player activities
// RETURN VALUE:		BOOL					TRUE if the player joined this mission, otherwise FALSE
//
// NOTES:	Assumes the Player is still OK
//			A player will only join an active mission if his team is already joinable on the mission
FUNC BOOL Add_Player_To_Existing_Mission_Request_Offered_Or_Active(INT paramArrayPos, PLAYER_INDEX paramPlayerID, g_eMPMissionSource paramSourceID, MP_MISSION paramMissionID, m_structPlayerAndTeamInfoMC &paramInfo)

	BOOL playersAreInTeams = Check_If_Players_Are_In_Teams_For_Mission(paramMissionID)
	
	// If players are not in teams, check if it is still joinable for players
	IF NOT (playersAreInTeams)
		IF NOT (Does_MP_Mission_Request_Allow_Players_To_Join_Mission(paramArrayPos))
			#IF IS_DEBUG_BUILD
				NET_PRINT("           (FAILED TO JOIN MISSION: Mission that is not team-based is not currently joinable for players") NET_NL()
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF

	// Ensure the player's team is currently joinable for this mission
	INT myTeam = TEAM_INVALID
	
	IF (playersAreInTeams)
		myTeam = GET_PLAYER_TEAM(paramPlayerID)
		IF NOT (Is_Team_Joinable_For_Mission(paramArrayPos, myTeam))
			#IF IS_DEBUG_BUILD
				NET_PRINT("           (FAILED TO JOIN MISSION: Mission is not currently joinable for this player's team)") NET_NL()
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Check if this mission is full for this player's team
	INT numActivePlayers	= 0
	INT numConfirmedPlayers	= 0
	INT numJoiningPlayers	= 0
	Gather_MP_Mission_Request_Capacity_Info_For_Team(paramArrayPos, myTeam, numActivePlayers, numConfirmedPlayers, numJoiningPlayers, paramInfo)
	
	MP_MISSION_ID_DATA	missionIdData		= Get_MP_Mission_Request_MissionID_Data(paramArrayPos)
	INT					maxPlayersAllowed	= Get_Maximum_Team_Members_For_MP_Mission_Request(missionIdData, myTeam)
	BOOL 				inviteOntoMission	= Should_MP_Mission_Team_Invite_Player_Onto_Mission(paramMissionID, myTeam)
	
	// Check if any more players are allowed to be offered this mission
	IF NOT (Can_Any_More_Players_Be_Selected_To_Join_MP_Mission_Request(paramArrayPos, myTeam, inviteOntoMission, numActivePlayers, numConfirmedPlayers, numJoiningPlayers, maxPlayersAllowed))
		#IF IS_DEBUG_BUILD
			NET_PRINT("           (FAILED TO JOIN MISSION: Mission or Player's Team is full)") NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// As a final test, check if the player is excluded from this mission
	// Ignore this test for some mission request sources and clear the excluded flag
	#IF IS_DEBUG_BUILD
		BOOL playerExclusionRemoved = FALSE
	#ENDIF
	
	IF (Is_Player_Excluded_From_MP_Mission_Request_Slot(paramArrayPos, paramPlayerID))
		// Player is excluded, but ignore this from some sources
		SWITCH (paramSourceID)
			// Ignore (and remove) excluded flag for these Mission Request sources unless the mission is not joinable for this player's team
			CASE MP_MISSION_SOURCE_DEBUG_MENU
				IF (Is_Team_Joinable_For_Mission(paramArrayPos, myTeam))
					#IF IS_DEBUG_BUILD
						playerExclusionRemoved = TRUE
					#ENDIF
			
					Clear_Player_As_Excluded_From_MP_Mission_Request_Slot(paramArrayPos, paramPlayerID)
				ELSE
					#IF IS_DEBUG_BUILD
						NET_PRINT("           (FAILED TO JOIN MISSION: Player's Team is not joinable for this mission)") NET_NL()
					#ENDIF
			
					RETURN FALSE
				ENDIF
				BREAK
			
			// Don't join the mission if player is excluded for all other sources
			DEFAULT
				#IF IS_DEBUG_BUILD
					NET_PRINT("           (FAILED TO JOIN MISSION: Player is excluded from this mission)") NET_NL()
				#ENDIF
		
				RETURN FALSE
		ENDSWITCH
	ENDIF
	
	// Player can join this mission instance
	#IF IS_DEBUG_BUILD
		NET_PRINT("           (PLAYER CAN JOIN THIS MISSION")
		IF (playerExclusionRemoved)
			NET_PRINT(" - Player was excluded but the exclusion has been removed (player has probably tried to join using the debug menu)")
		ENDIF
		NET_PRINT(")")
		NET_NL()
	#ENDIF
	
	// Allow the player to join the mission - use the team join method unless the request comes from the joblist where the player should be forced on
	BOOL useInvitation	= inviteOntoMission
	BOOL useComms		= FALSE
	
	IF (paramSourceID = MP_MISSION_SOURCE_JOBLIST)
		// Using Joblist, so force onto mission with no communication
		useInvitation = FALSE
	ELSE
		// Not using Joblist, so use standard joining requirements
		IF (Should_MP_Mission_Team_Use_A_PreMission_Phonecall(paramMissionID, myTeam))
		OR (Should_MP_Mission_Team_Use_A_PreMission_Txtmsg(paramMissionID, myTeam))
			useComms = TRUE
		ENDIF
	ENDIF
	Issue_Broadcast_For_Player_To_Join_MP_Mission_Request(paramArrayPos, paramPlayerID, useInvitation, useComms)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      The Player Details after Player added to an existing mission: ") NET_PRINT(GET_PLAYER_NAME(paramPlayerID)) NET_NL()
		Debug_Output_Missions_Player_Details(paramArrayPos)
	#ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if this reserved mission is full for this player (taking team into account if necessary)
//
// INPUT PARAMS:			paramArrayPos		The Mission Request Array Position
//							paramMissionID		The Mission ID
//							paramVariation		The Mission Variation
//							paramTeam			The Player's Team
// RETURN PARAMS:			paramInfo			Additional general info about current player activities
// RETURN VALUE:			BOOL				TRUE if this player can't join the mission either because too many teammates are reserved, or too many total players are reserved, FALSE if they can join
FUNC BOOL Check_If_Reserved_Mission_Is_Full_For_This_Player(INT paramArrayPos, MP_MISSION paramMissionID, INT paramTeam, m_structPlayerAndTeamInfoMC &paramInfo)

	IF (paramInfo.bitsBusyPlayers=0)
		// fool compiler
	ENDIF

	INT					bitsReservedByPlayerOnMission	= Get_MP_Mission_Request_Reserved_By_Player_Players(paramArrayPos)
	INT					numPlayersInOtherSessions		= GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrNumPlayersInOtherSessions
	MP_MISSION_ID_DATA	missionIdData					= Get_MP_Mission_Request_MissionID_Data(paramArrayPos)
	INT					tempLoop						= 0
	INT					numOnMission					= 0
		
	// If the mission rquires players are in teams, then perform team-based checks
	IF (Check_If_Players_Are_In_Teams_For_Mission(paramMissionID))
		// ...do team based checks
		IF (paramTeam = TEAM_INVALID)
			#IF IS_DEBUG_BUILD
				NET_PRINT("           (FAILED TO JOIN MISSION: Mission requires players to be in teams and this player is not in a team.)") NET_NL()
			#ENDIF

			// Treat as FULL for this player
			RETURN TRUE
		ENDIF
		
		INT					maxAllowedFromTeam	= Get_Maximum_Team_Members_For_MP_Mission_Request(missionIdData, paramTeam)
		//INT					bitsPlayersInTeam	= paramInfo.bitsAllTeamMembers[paramTeam]
		
//		REPEAT NUM_NETWORK_PLAYERS tempLoop
//			IF (IS_BIT_SET(bitsPlayersInTeam, tempLoop))
//				IF (IS_BIT_SET(bitsReservedByPlayerOnMission, tempLoop))
//					numOnMission++
//				ENDIF
//			ENDIF
//		ENDREPEAT
		
		// Check if the mission is not oversubscribed for this team
		IF (numOnMission >= maxAllowedFromTeam)
			// Already Full
			#IF IS_DEBUG_BUILD
				NET_PRINT("           (FAILED TO JOIN MISSION: Player's Team is full for this mission.)") NET_NL()
			#ENDIF

			// Full for this team
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Do general mission full checks that aren't team-based
	INT maxAllowedOnMission = Get_Maximum_Players_Required_For_MP_Mission_Request(missionIdData)
	
	numOnMission = 0
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(bitsReservedByPlayerOnMission, tempLoop))
			numOnMission++
		ENDIF
	ENDREPEAT
	
	// KGM 14/6/13: Add on players in other sessions on this mission as being rserved for the mission too when doing the 'mission full' check
	numOnMission += numPlayersInOtherSessions
	
	// Check if the mission already has too many reserved players
	IF (numOnMission >= maxAllowedOnMission)
		// Already Full
		#IF IS_DEBUG_BUILD
			NET_PRINT("           (FAILED TO JOIN MISSION: This mission is already full of players")
			IF (numPlayersInOtherSessions > 0)
				NET_PRINT(" [")
				NET_PRINT_INT(numPlayersInOtherSessions)
				NET_PRINT(" reserved in other sessions]")
			ENDIF
			NET_PRINT(".)")
			NET_NL()
		#ENDIF

		// Already full of players
		RETURN TRUE
	ENDIF
	
	// Mission isn't full so player can join it
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if this player can be added to a Received or Reserved mission
//
// INPUT PARAMS:		paramArrayPos			The Mission Request Slot
//						paramPlayerID			The player trying to join the mission
//						paramMissionID			The Mission ID that the player has requested to join
//						paramCheckMissionFull	[DEFAULT=TRUE] Optionally perform the Mission Full check - there are no players on mission yet, but any required players may fill the mission
//						paramAlreadyOnTS		[DEFAULT = FALSE] TRUE if the joining player is already on the inviting player's Transistion session (perhaps a JIP cross-session), otherwise FALSE
// RETURN PARAMS:		paramInfo				Additional general info about current player activities
// RETURN VALUE:		BOOL					TRUE if the player joined this mission, otherwise FALSE
//
// NOTES:	Assumes the Player is still OK
FUNC BOOL Add_Player_To_Existing_Mission_Request_Received_Or_Reserved(INT paramArrayPos, PLAYER_INDEX paramPlayerID, MP_MISSION paramMissionID, m_structPlayerAndTeamInfoMC &paramInfo, BOOL paramCheckMissionFull = TRUE, BOOL paramAlreadyOnTS = FALSE)
	
	// Check if the player is excluded from this mission
	IF (Is_Player_Excluded_From_MP_Mission_Request_Slot(paramArrayPos, paramPlayerID))
		// Player is excluded
		#IF IS_DEBUG_BUILD
			NET_PRINT("           (FAILED TO JOIN MISSION: Player is excluded from this mission)") NET_NL()
		#ENDIF

		RETURN FALSE
	ENDIF

	INT theVariation  = Get_MP_Mission_Request_MissionVariation(paramArrayPos)
	
	// If players are in teams, check if the player's team is currently reserved for this mission
	INT myTeam = GET_PLAYER_TEAM(paramPlayerID)
	
	IF (Check_If_Players_Are_In_Teams_For_Mission(paramMissionID))
		// A player with no team can't join this mission
		IF (myTeam = TEAM_INVALID)
			#IF IS_DEBUG_BUILD
				NET_PRINT("           (FAILED TO JOIN MISSION: Player is not in a team and the mission is team-based)") NET_NL()
			#ENDIF

			RETURN FALSE
		ENDIF
		
		// Check if player's team can join the mission
		IF NOT (Is_Team_Requested_For_Mission(paramArrayPos, myTeam))
			// My team is not requested for this mission
			// Check if my team is allowed on this mission
			INT requestedTeams = Get_MP_Mission_Request_Requested_Teams(paramArrayPos)
			IF NOT (Check_If_Team_Can_Join_Mission(paramMissionID, theVariation, requestedTeams, myTeam))
				#IF IS_DEBUG_BUILD
					NET_PRINT("           (FAILED TO JOIN MISSION: Player's team cannot join mission based on the team configuration)") NET_NL()
				#ENDIF

				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// If required, perform the mission full check (if not required, the player can be added to the mission)
	IF (paramCheckMissionFull)
		IF NOT (paramAlreadyOnTS)
			IF (Check_If_Reserved_Mission_Is_Full_For_This_Player(paramArrayPos, paramMissionID, myTeam, paramInfo))
				// Mission is full for this player, so can't add the player to the mission
				RETURN FALSE
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				NET_PRINT("              - Ignored Mission Full check. Joining Player is already on Inviting Player's Transition Session.") NET_NL()
			#ENDIF
		ENDIF
	ENDIF
	
	// Reserve the player for the mission
	Set_Player_As_Reserved_By_Player_On_MP_Mission_Request_Slot(paramArraypos, paramPlayerID)
	
	// Request the Player's Team for the mission
	IF (Check_If_Players_Are_In_Teams_For_Mission(paramMissionID))
		IF NOT (Is_Team_Requested_For_Mission(paramArrayPos, myTeam))
			Store_MP_Mission_Request_Requested_Team(paramArraypos, myTeam)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           (JOINED MISSION - UniqueID: ") NET_PRINT_INT(Get_MP_Mission_Request_UniqueID(paramArrayPos)) NET_PRINT(")") NET_NL()
		NET_PRINT("      The Player Details after Player added to a reserved or received mission: ") NET_PRINT(GET_PLAYER_NAME(paramPlayerID)) NET_NL()
		Debug_Output_Missions_Player_Details(paramArrayPos)
	#ENDIF
		
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Look through all current Mission Requests and add this player to an existing request if appropriate
//
// INPUT PARAMS:		paramPlayerID			The player trying to join the mission
//						paramSourceID			The Mission Request Source
//						paramMissionID			The Mission ID Data that the player has requested to join
//						paramInstanceID			[default=DEFAULT_MISSION_INSTANCE] The Mission Instance to join (if important)
// RETURN PARAMS:		paramInfo				Additional general info about current player activities
// RETURN VALUE:		INT						The Mission Request Slot containing the mission that was joined, or NO_FREE_MISSION_REQUEST_SLOT
//
// NOTES:	Assumes the Player is still OK
FUNC INT Add_Player_To_Existing_Mission_Request(m_structPlayerAndTeamInfoMC &paramInfo, g_eMPMissionSource paramSourceID, PLAYER_INDEX paramPlayerID, MP_MISSION_ID_DATA &refMissionIdData, INT paramInstanceID = DEFAULT_MISSION_INSTANCE)

	// Ensure the MissionID is valid
	IF (refMissionIdData.idMission = eNULL_MISSION)
		RETURN NO_FREE_MISSION_REQUEST_SLOT
	ENDIF

	// Find a mission that matches these details
	INT tempLoop = 0
	BOOL investigateThisMissionRequest = FALSE
	BOOL missionUsesCloudData = FALSE
	MP_MISSION_DATA theMissionData
	g_eMPMissionStatus theMissionRequestStatus
	
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		// Ignore empty or 'Reserved' mission slots (Reserved mission slots are added to using a special broadcast event)
		theMissionRequestStatus	= Get_MP_Mission_Request_Slot_Status(tempLoop)
		theMissionData			= Get_MP_Mission_Request_Slot_Mission_Data(tempLoop)
		
		#IF IS_DEBUG_BUILD
			IF (theMissionRequestStatus = MP_MISSION_STATE_RESERVED)
				NET_PRINT("           IGNORE SLOT ") NET_PRINT_INT(tempLoop) NET_PRINT(" - Mission is reserved: ") NET_PRINT(GET_MP_MISSION_NAME(theMissionData.mdID.idMission)) NET_NL()
			ENDIF
		#ENDIF

		IF NOT (theMissionRequestStatus = NO_MISSION_REQUEST_RECEIVED)
		AND NOT (theMissionRequestStatus = MP_MISSION_STATE_RESERVED)
			// This mission slot contains potentially valid data
			investigateThisMissionRequest = TRUE
			
			// Check if the Mission ID matches the requested mission
			IF NOT (theMissionData.mdID.idMission = refMissionIdData.idMission)
				investigateThisMissionRequest = FALSE
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("           IGNORE SLOT ") NET_PRINT_INT(tempLoop) NET_PRINT(" - Mission doesn't match: ") NET_PRINT(GET_MP_MISSION_NAME(theMissionData.mdID.idMission)) NET_NL()
				#ENDIF
			ENDIF
			
			// Check if the Instance ID is important and is a match
			// If the instanceID is important then the request must have come from a source that has already checked the Mission Request data
			//		and knows exactly which mission it wants to join (ie: the joblist).
			IF (investigateThisMissionRequest)
				IF NOT (paramInstanceID = DEFAULT_MISSION_INSTANCE)
					// The InstanceID is important
					IF (theMissionData.iInstanceId = DEFAULT_MISSION_INSTANCE)
						// ...ignore this mission request since the instanceID isn't set up
						investigateThisMissionRequest = FALSE
				
						#IF IS_DEBUG_BUILD
							NET_PRINT("           IGNORE SLOT ") NET_PRINT_INT(tempLoop) NET_PRINT(" - Instance ID in slot isn't setup") NET_NL()
						#ENDIF
					ELSE
						IF NOT (theMissionData.iInstanceId = paramInstanceID)
							// ...ignore this mission request since the instanceID doesn't match
							investigateThisMissionRequest = FALSE
				
							#IF IS_DEBUG_BUILD
								NET_PRINT("           IGNORE SLOT ") NET_PRINT_INT(tempLoop) NET_PRINT(" - Instance ID in slot doesn't match: ") NET_PRINT_INT(theMissionData.iInstanceId) NET_NL()
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// Check the Variation ID or contentID if required
			IF (investigateThisMissionRequest)
				missionUsesCloudData = Does_MP_Mission_Variation_Get_Data_From_The_Cloud(refMissionIdData.idMission, refMissionIdData.idVariation)
			
				// For Cloud Loaded Missions, check if the filenames are the same
				IF (missionUsesCloudData)
					IF NOT (ARE_STRINGS_EQUAL(theMissionData.mdID.idCloudFilename, refMissionIdData.idCloudFilename))
						// ..ignore this mission request since it is for a different contentID
						investigateThisMissionRequest = FALSE
						
						#IF IS_DEBUG_BUILD
							NET_PRINT("              IGNORE SLOT ") NET_PRINT_INT(tempLoop) NET_PRINT(" - CloudFilename doesn't match: ") NET_PRINT(theMissionData.mdID.idCloudFilename) NET_NL()
						#ENDIF
					ENDIF
				ENDIF
				
				// If Mission isn't cloud-loaded, the Variation ID may be important for hardcoded mission
				IF NOT (missionUsesCloudData)
					IF NOT (refMissionIdData.idVariation = NO_MISSION_VARIATION)
					AND NOT (theMissionData.mdID.idVariation = NO_MISSION_VARIATION)
						// The variation ID is important
						IF NOT (theMissionData.mdID.idVariation = refMissionIdData.idVariation)
							// ...ignore this mission request since it is for a different mission variation
							investigateThisMissionRequest = FALSE
							
							#IF IS_DEBUG_BUILD
								NET_PRINT("           IGNORE SLOT ") NET_PRINT_INT(tempLoop) NET_PRINT(" - Variation ID in slot doesn't match: ") NET_PRINT_INT(theMissionData.mdID.idVariation) NET_NL()
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF (investigateThisMissionRequest)
				// Found a potential mission candidate, so check if this player is allowed to join the mission
				#IF IS_DEBUG_BUILD
					NET_PRINT("           POTENTIAL MISSION MATCH IN SLOT ") NET_PRINT_INT(tempLoop) NET_PRINT(": ") NET_PRINT(GET_MP_MISSION_NAME(theMissionData.mdID.idMission)) NET_NL()
				#ENDIF
		
				SWITCH (theMissionRequestStatus)
					CASE MP_MISSION_STATE_ACTIVE
					CASE MP_MISSION_STATE_ACTIVE_SECONDARY
					CASE MP_MISSION_STATE_OFFERED
						IF (Add_Player_To_Existing_Mission_Request_Offered_Or_Active(tempLoop, paramPlayerID, paramSourceID, refMissionIdData.idMission, paramInfo))
							RETURN tempLoop
						ENDIF
						BREAK

					CASE MP_MISSION_STATE_RECEIVED
						// NOTE: This should perform the 'mission full' check (performed by default)
						IF (Add_Player_To_Existing_Mission_Request_Received_Or_Reserved(tempLoop, paramPlayerID, refMissionIdData.idMission, paramInfo))
							RETURN tempLoop
						ENDIF
						BREAK
						
					CASE NO_MISSION_REQUEST_RECEIVED
						SCRIPT_ASSERT("Add_Player_To_Existing_Mission_Request(): Found CASE: NO_MISSION_REQUEST_RECEIVED, but getting in here should be impossible. Tell Keith.")
						BREAK
						
					CASE MP_MISSION_STATE_RESERVED
						SCRIPT_ASSERT("Add_Player_To_Existing_Mission_Request(): Found CASE: MP_MISSION_STATE_RESERVED, but getting in here should be impossible. Tell Keith.")
						BREAK
						
					DEFAULT
						SCRIPT_ASSERT("Add_Player_To_Existing_Mission_Request(): Unknown Mission Request Slot Status. Add it to SWITCH statement. Tell Keith.")
						BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN NO_FREE_MISSION_REQUEST_SLOT

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check for matching mission details
//
// INPUT PARAMS:			paramSlot					The Mission Request array position containing hte stored mission being checked against
//							paramSourceID				The Mission Request Source of the mission being checked for
//							paramTutorialSessionID		The tutorial session ID that the other players reserved for the mission being checked for
// REFERENCE PARAMS:		refMissionIdData			The MissionID Data of the mission being checked for
// RETURN VALUE:			BOOL						TRUE if these mission's match, FALSE if they don't match
FUNC BOOL Check_If_These_Mission_Details_Match_Reserved_Mission_In_Slot(INT paramSlot, g_eMPMissionSource paramSourceID, INT paramTutorialSessionID, MP_MISSION_ID_DATA &refMissionIdData)

	// Do the tutorial session IDs match (if important)?
	INT missionTutorialSessionID = Get_MP_Mission_Request_Specific_Tutorial_SessionID(paramSlot)
	
	IF (missionTutorialSessionID != TUTORIAL_SESSION_NOT_ACTIVE)
		// ...the mission request requires players to be in a specific Tutorial Session
		IF (missionTutorialSessionID != paramTutorialSessionID)
			#IF IS_DEBUG_BUILD
				NET_PRINT("              IGNORE SLOT ")
				NET_PRINT_INT(paramSlot)
				NET_PRINT(" - Player is not in the mission's required tutorial sessionID: ")
				NET_PRINT_INT(missionTutorialSessionID)
				NET_PRINT(" - Looking for ")
				NET_PRINT_INT(paramTutorialSessionID)
				NET_NL()
			#ENDIF
			
			// Not a match
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Check if the mission source is the same (but allow an Invite request or a map but not walk-in request to match any source because this will be a pre-reservation request prior to accepting an invite)
	IF (paramSourceID != MP_MISSION_SOURCE_INVITE)
	AND (paramSourceID != MP_MISSION_SOURCE_MAP_BUT_NOT_WALK_IN)
		IF (Get_MP_Mission_Request_Source(paramSlot) != paramSourceID)
			#IF IS_DEBUG_BUILD
				NET_PRINT("              IGNORE SLOT ") NET_PRINT_INT(paramSlot) NET_PRINT(" - Mission Request Source doesn't match: ") NET_PRINT(Convert_Mission_Source_To_String(Get_MP_Mission_Request_Source(paramSlot))) NET_NL()
			#ENDIF
			
			// Not a match
			RETURN FALSE
		ENDIF
	ENDIF
			
	// Check if the Mission ID matches the requested mission
	MP_MISSION_DATA theMissionData = Get_MP_Mission_Request_Slot_Mission_Data(paramSlot)
	IF (theMissionData.mdID.idMission != refMissionIdData.idMission)
		#IF IS_DEBUG_BUILD
			NET_PRINT("              IGNORE SLOT ") NET_PRINT_INT(paramSlot) NET_PRINT(" - Mission doesn't match: ") NET_PRINT(GET_MP_MISSION_NAME(theMissionData.mdID.idMission)) NET_NL()
		#ENDIF
		
		// Not a match
		RETURN FALSE
	ENDIF

	BOOL missionUsesCloudData = Does_MP_Mission_Variation_Get_Data_From_The_Cloud(refMissionIdData.idMission, refMissionIdData.idVariation)
			
	// For Cloud Loaded Missions, check if the filenames are the same
	IF (missionUsesCloudData)
		IF NOT (ARE_STRINGS_EQUAL(theMissionData.mdID.idCloudFilename, refMissionIdData.idCloudFilename))
			#IF IS_DEBUG_BUILD
				NET_PRINT("              IGNORE SLOT ") NET_PRINT_INT(paramSlot) NET_PRINT(" - CloudFilename doesn't match: ") NET_PRINT(theMissionData.mdID.idCloudFilename) NET_NL()
			#ENDIF
			
			// Not a match
			RETURN FALSE
		ENDIF
	ENDIF
	
	// If Mission isn't cloud-loaded, the Variation ID may be important for hardcoded mission
	IF NOT (missionUsesCloudData)
		IF NOT (refMissionIdData.idVariation = NO_MISSION_VARIATION)
		AND NOT (theMissionData.mdID.idVariation = NO_MISSION_VARIATION)
			// The variation ID is important
			IF NOT (theMissionData.mdID.idVariation = refMissionIdData.idVariation)
				// ...ignore this mission request since it is for a different mission variation
				#IF IS_DEBUG_BUILD
					NET_PRINT("              IGNORE SLOT ") NET_PRINT_INT(paramSlot) NET_PRINT(" - Variation ID in slot doesn't match: ") NET_PRINT_INT(theMissionData.mdID.idVariation) NET_NL()
				#ENDIF
				
				// Not a match
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// This is a match
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Look through all current Mission Requests and add this player to a matching reserved mission
//
// INPUT PARAMS:		paramSourceID				The Mission Request Source
//						paramPlayerID				The player trying to join the mission
//						paramTutorialSessionID		The tutorial session ID that the other players reserved for the mission need to be allocated to [this was needed for Dave's freemode tutorial]
//						paramInvitingPlayer			The player that invited this player to become reserved for the mission, or INVALID_PLAYER_INDEX() if none
//						paramInInvitingPlayerTS		TRUE if the joining player is already in teh Inviting Player's Transistion Session, otherwise FALSE
// REFERENCE PARAMS:	refInfo						Additional general info about current player activities
//						refMissionIdData			The MissionID Data that the player has requested to join
//						refInvitingPlayerFullSlot	Return the inviting player's slot if the joining player can't join because it is full
// RETURN VALUE:		INT							The Mission Request Slot containing the mission that was joined, or NO_FREE_MISSION_REQUEST_SLOT
//
// NOTES:	Assumes the Player is still OK and that the mission ID is valid
FUNC INT Add_Player_To_Existing_Reserved_Mission(m_structPlayerAndTeamInfoMC &refInfo, g_eMPMissionSource paramSourceID, PLAYER_INDEX paramPlayerID, MP_MISSION_ID_DATA &refMissionIdData, INT paramTutorialSessionID, PLAYER_INDEX paramInvitingPlayer, BOOL paramInInvitingPlayerTS, INT &refInvitingPlayerFullSlot)

	// Find a reserved mission that matches these details
	BOOL			investigateThisMissionRequest	= FALSE
	BOOL			playersAreInTeamsForMission		= Check_If_Players_Are_In_Teams_For_Mission(refMissionIdData.idMission)
	INT				tempLoop						= 0
	
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		// Investigate only 'Reserved' missions
		#IF IS_DEBUG_BUILD
			MP_MISSION_DATA		theMissionData			= Get_MP_Mission_Request_Slot_Mission_Data(tempLoop)
			g_eMPMissionStatus	theMissionRequestStatus = Get_MP_Mission_Request_Slot_Status(tempLoop)
			
			IF (theMissionRequestStatus != NO_MISSION_REQUEST_RECEIVED)
			AND (theMissionRequestStatus != MP_MISSION_STATE_RESERVED)
				NET_PRINT("              IGNORE SLOT ") NET_PRINT_INT(tempLoop) NET_PRINT(" - Mission Status is not RESERVED: ") NET_PRINT(GET_MP_MISSION_NAME(theMissionData.mdID.idMission)) NET_NL()
			ENDIF
		#ENDIF

		IF (Is_MP_Mission_Request_Slot_Status_Reserved(tempLoop))
			// This mission slot contains potentially valid data
			investigateThisMissionRequest = TRUE
			
			// KGM 29/8/13: Check if a mission request is closed to walk-ins
			IF (investigateThisMissionRequest)
				IF NOT (paramSourceID = MP_MISSION_SOURCE_INVITE)
				AND NOT (paramSourceID = MP_MISSION_SOURCE_MAP_BUT_NOT_WALK_IN)
					IF (Is_MP_Mission_Request_Mission_Closed_To_Walk_Ins(tempLoop))
						investigateThisMissionRequest = FALSE
						
						#IF IS_DEBUG_BUILD
							NET_PRINT("              IGNORE SLOT ") NET_PRINT_INT(tempLoop) NET_PRINT(" - Mission is not open to Walk-Ins (Matchmaking Closed, probably)") NET_NL()
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// KGM 25/2/13: Check if this mission is still accepting more player reservations
			IF (investigateThisMissionRequest)
				IF NOT (playersAreInTeamsForMission)
					IF NOT (Does_MP_Mission_Request_Allow_Players_To_Join_Mission(tempLoop))
						investigateThisMissionRequest = FALSE
						
						#IF IS_DEBUG_BUILD
							NET_PRINT("              IGNORE SLOT ") NET_PRINT_INT(tempLoop) NET_PRINT(" - Mission is no longer joinable for players") NET_NL()
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// Check if the Players are already on specific Tutorial Sessions that the current player needs to obey
			IF (investigateThisMissionRequest)
				investigateThisMissionRequest = Check_If_These_Mission_Details_Match_Reserved_Mission_In_Slot(tempLoop, paramSourceID, paramTutorialSessionID, refMissionIdData)
			ENDIF

			// If there is an inviting player then make sure that player is reserved for this mission
			BOOL invitingPlayersMission = FALSE
			IF (investigateThisMissionRequest)
				IF (paramInvitingPlayer != INVALID_PLAYER_INDEX())
					IF NOT (Is_Player_Reserved_For_MP_Mission_Request_Slot(tempLoop, paramInvitingPlayer))
						investigateThisMissionRequest = FALSE
					
						#IF IS_DEBUG_BUILD
							NET_PRINT("              IGNORE SLOT ") NET_PRINT_INT(tempLoop) NET_PRINT(" - Inviting Player is not reserved for this mission: ") NET_PRINT(GET_PLAYER_NAME(paramInvitingPlayer)) NET_NL()
						#ENDIF
					ELSE
						invitingPlayersMission = TRUE
					ENDIF
				ENDIF
			ENDIF
		
			IF (investigateThisMissionRequest)
				// Found a potential mission candidate, so check if this player is allowed to join the mission
				#IF IS_DEBUG_BUILD
					NET_PRINT("              POTENTIAL MISSION MATCH IN SLOT ") NET_PRINT_INT(tempLoop) NET_PRINT(": ") NET_PRINT(GET_MP_MISSION_NAME(theMissionData.mdID.idMission)) NET_NL()
				#ENDIF
				
				// NOTE: This shouldn't perform the 'Mission Full' check - Reserved missions can get oversubscribed and the chosen players are decided when the mission is started
				// KGM 25/1/13: Don't allow missions to get over-subscribed with reserved players anymore. FM needs this nailed down pre-vote and CNC can probably work well this way too.
				BOOL performMissionFullCheck = TRUE
				IF (Add_Player_To_Existing_Mission_Request_Received_Or_Reserved(tempLoop, paramPlayerID, refMissionIdData.idMission, refInfo, performMissionFullCheck, paramInInvitingPlayerTS))
					RETURN tempLoop
				ENDIF
				
				// KGM 13/1/15: If this was the inviting player's mission and the player didn't get to join it, then store the mission slot in the return param
				IF (invitingPlayersMission)
					refInvitingPlayerFullSlot = tempLoop
					
					#IF IS_DEBUG_BUILD
						NET_PRINT("              ...SLOT ") NET_PRINT_INT(tempLoop) NET_PRINT(": Inviting player is reserved for this mission, but joining player isn't able to join it") NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN NO_FREE_MISSION_REQUEST_SLOT

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the player is already reserved by player for a reserved mission matching these details, if not then return NO_FREE_MISSION_REQUEST_SLOT
//
// INPUT PARAMS:			paramSourceID				The Mission Request Source
//							paramPlayerID				The player trying to join the mission
//							paramTutorialSessionID		The tutorial session ID that the other players reserved for the mission need to be allocated to [this was needed for Dave's freemode tutorial]
// REFERENCE PARAMS:		refMissionIdData			The MissionID Data that the player has requested to join
// RETURN VALUE:			INT							The Mission Request Slot containing the mission that the player is already reserved for, or NO_FREE_MISSION_REQUEST_SLOT
//
// NOTES:	Assumes the Player is still OK and that the mission ID is valid
//			If the player is allocated to a slot in another way other than 'reserved by player' return NO_FREE_MISSION_REQUEST_SLOT and let the rest of the calling function sort things out.
FUNC INT Get_Slot_If_Player_Already_Reserved_By_Player_With_These_Details(g_eMPMissionSource paramSourceID, PLAYER_INDEX paramPlayerID, MP_MISSION_ID_DATA &refMissionIdData, INT paramTutorialSessionID)

	INT tempLoop = 0
	
	#IF IS_DEBUG_BUILD
		MP_MISSION_DATA	theMissionData
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Check If already Reserved By Player for a Reserved Mission with the same details (to overwrite a pre-reservation for an invite):") NET_NL()
	#ENDIF
	
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		// Is the player ReservedByPlayer for this mission request slot?
		IF (Is_Player_Reserved_By_Player_For_MP_Mission_Request_Slot(tempLoop, paramPlayerID))
			// ...player is Reserved By Player for this mission request slot
			#IF IS_DEBUG_BUILD
				theMissionData = Get_MP_Mission_Request_Slot_Mission_Data(tempLoop)
				NET_PRINT("              CHECKING SLOT ") NET_PRINT_INT(tempLoop) NET_PRINT(" - Player is RESERVED BY PLAYER: ") NET_PRINT(GET_MP_MISSION_NAME(theMissionData.mdID.idMission)) NET_NL()
			#ENDIF
			
			// Check if the Players are already on specific Tutorial Sessions that the current player needs to obey
			IF NOT (Check_If_These_Mission_Details_Match_Reserved_Mission_In_Slot(tempLoop, paramSourceID, paramTutorialSessionID, refMissionIdData))
				RETURN NO_FREE_MISSION_REQUEST_SLOT
			ENDIF
			
			// Found player ReservedByPlayer for mission and the details match, so returning Mision Request slot
			#IF IS_DEBUG_BUILD
				NET_PRINT("                 SUCCESS: Player found as Reserved by Player and the Mission Details match, so quitting checks.") NET_NL()
			#ENDIF
			
			RETURN tempLoop
		ELSE
			// ...player is not Reserved By Player for this mission request slot
			#IF IS_DEBUG_BUILD
				IF (Get_MP_Mission_Request_Slot_Status(tempLoop) != NO_MISSION_REQUEST_RECEIVED)
					theMissionData = Get_MP_Mission_Request_Slot_Mission_Data(tempLoop)
					NET_PRINT("              IGNORE SLOT ") NET_PRINT_INT(tempLoop) NET_PRINT(" - Player is not RESERVED BY PLAYER: ") NET_PRINT(GET_MP_MISSION_NAME(theMissionData.mdID.idMission)) NET_NL()
				ENDIF
			#ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN NO_FREE_MISSION_REQUEST_SLOT

ENDFUNC



// ===========================================================================================================
//      The Main MP Mission Control Routines - Server
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Initialisation Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	A one-off Mission Control initialisation.
PROC Initialise_MP_Mission_Control_Server()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Initialise_MP_Mission_Control") NET_NL()
	#ENDIF

	BOOL beingInitialised = TRUE
	Clear_All_MP_Mission_Requests(beingInitialised)
	Clear_MP_Mission_Control_Data()
	Clear_All_MP_Mission_Player_Data()

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Maintenance Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintain a Received Mission Request
//
// INPUT PARAMS:		paramArrayPos			The array position containing the Active Mission.
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
PROC Maintain_Received_MP_Mission_Request(INT paramArrayPos, m_structPlayerAndTeamInfoMC &paramInfo)

	#IF IS_DEBUG_BUILD
		NET_NL()
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Processing Received Mission Request in slot: ")
		NET_PRINT_INT(paramArrayPos)
		IF (Check_If_Players_Are_In_Teams_For_Mission(Get_MP_Mission_Request_MissionID(paramArrayPos)))
			NET_PRINT("  [TEAM-BASED]")
		ELSE
			NET_PRINT("  [NOT TEAM-BASED]")
		ENDIF
		NET_PRINT("  [")
		Debug_Output_Basic_MP_Mission_Details_As_A_Line_Segment(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData)
		NET_PRINT("]")
		NET_NL()
		Debug_Output_Player_And_Team_Details(paramInfo)
	#ENDIF
	
	// Players Reserved By Player for this mission are currently marked as BUSY to ensure they don't get taken for any other missions - free them up now
	Set_Any_Reserved_By_Player_Players_For_This_Mission_As_Not_Busy(paramArrayPos, paramInfo)
	
	// If there are no players available then immediately fail the request
	IF (paramInfo.numPlayersFree = 0)
	#IF IS_DEBUG_BUILD
	AND NOT DEBUG_IS_SOLO_STRAND_ENABLED()
	#ENDIF
		// ...failed because there are no players available
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........FAILED: There are no free players available to play a new mission") NET_NL()
		#ENDIF
	
		MP_Mission_Request_Failure_Reason(paramArrayPos, FTLR_TEAM_REQUIREMENTS_NOT_MET)
		MP_Mission_Request_Failed(paramArrayPos)
		EXIT
	ENDIF

	// Fill out the Mission Details
	IF NOT (Fill_Out_MP_Mission_Request_Slot_Details(paramArrayPos, paramInfo))
		// ...failed to generate all the details for the mission
		// NOTE: If a reason for failure requires to be broadcast it will be sent from within the function
		MP_Mission_Request_Failed(paramArrayPos)
		EXIT
	ENDIF
	
	// Choose the Players and Teams for the mission
	IF NOT (Reserve_Participants_For_MP_Mission_Request_Slot(paramArrayPos, paramInfo))
		// ...failed to reserve all the participants required to launch the mission
		MP_Mission_Request_Failure_Reason(paramArrayPos, FTLR_DATA_SETUP_ERROR)
		MP_Mission_Request_Failed(paramArrayPos)
		EXIT
	ENDIF
	
	// Choose a Mission Variation and fill the Crook and Cop Location vectors if necessary
	IF NOT (Choose_Variation_For_MP_Mission_Request_Slot(paramArrayPos))
		// ...failed to choose a suitable variation and/or mission location
		// NOTE: The reason for failure may need to be broadcast from inside the functions
		MP_Mission_Request_Failure_Reason(paramArrayPos, FTLR_DATA_SETUP_ERROR)
		MP_Mission_Request_Failed(paramArrayPos)
		EXIT
	ENDIF
	
	// Get the Launch players onto the mission - if there is a cutscene then this will be the Required Players initially
	IF NOT (Issue_Launch_Join_Requests_For_MP_Mission_Request(paramArrayPos, paramInfo))
		// ...failed to issue the participants required to launch the mission
		MP_Mission_Request_Failure_Reason(paramArrayPos, FTLR_DATA_SETUP_ERROR)
		MP_Mission_Request_Failed(paramArrayPos)
		EXIT
	ENDIF
	
	// The mission places only Reserved_By_Player Players from the launch teams onto the mission first
	// If no players meet this criteria then clear the cutscene flag and launch the mission as normal,
	//		otherwise the rest of the launch team will get on after a player has watched the cutscene and joined the mission
	IF (Check_If_Reserved_By_Player_Players_Get_Join_Priority_At_Launch(paramArrayPos))
		IF NOT (Are_Any_Players_Joining_This_MP_Mission_Request_Slot(paramArrayPos))
			#IF IS_DEBUG_BUILD
				NET_PRINT("...........FAILED to add any players to a mission that places Reserved_By_Player Players on the mission first") NET_NL()
				NET_PRINT("           so place all launch team players onto the mission instead.") NET_NL()
			#ENDIF
	
			// Remove the cutscene and try again to get all the players from the launch teams onto the mission
			Clear_MP_Mission_Request_Option_PreMission_Cutscene(paramArrayPos)
			
			IF NOT (Issue_Launch_Join_Requests_For_MP_Mission_Request(paramArrayPos, paramInfo))
				// ...failed to issue the participants required to launch the mission
				MP_Mission_Request_Failure_Reason(paramArrayPos, FTLR_DATA_SETUP_ERROR)
				MP_Mission_Request_Failed(paramArrayPos)
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
// KGM 25/2/13: Missions can now become 'not joinable' during the reservation phase, so moving this to get called immediately a new mission request is created
//	// Indicate that the mission is now joinable (used by missions that aren't team-based)
//	IF NOT (Check_If_Players_Are_In_Teams_For_Mission(Get_MP_Mission_Request_MissionID(paramArrayPos)))
//		Store_MP_Mission_Request_Option_Players_Can_Join_Mission(paramArrayPos)
//	ENDIF
	
	// This mission has been successfully set up
	Store_MP_Mission_Request_Status_As_Offered(paramArrayPos)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintain an Offered Mission Request
//
// INPUT PARAMS:		paramArrayPos			The array position containing the Active Mission.
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
// RETURN VALUE:		BOOL					TRUE if more players were requested to join the mission, otherwise FALSE
FUNC BOOL Maintain_Offered_MP_Mission_Request(INT paramArrayPos, m_structPlayerAndTeamInfoMC &paramInfo)

	// If any players have now been added to the 'active players' list then change the mission state to be active
	IF (Are_Any_Players_Active_On_This_MP_Mission_Request_Slot(paramArrayPos))
		// ...there are now active players
		Store_MP_Mission_Request_Status_As_Active(paramArrayPos)
		
		// Broadcast and Information Event that can be read by any other part of the game that needs it to indicate a mission has just launched
		Broadcast_Mission_Request_Info_Mission_Just_Started(paramArrayPos)
		
		// If this mission gives join priority to Reserved By Player Players from the Launch team then only they will be on the mission
		// So now remove the cutscene flag and get all the rest of the reserved players from the launch teams onto the mission
		IF (Check_If_Reserved_By_Player_Players_Get_Join_Priority_At_Launch(paramArrayPos))
			#IF IS_DEBUG_BUILD
				NET_PRINT("           This mission put Reserved_By_Player Players on the mission first, so now get all reserved players from the Launch Teams onto the mission.")
				NET_NL()
			#ENDIF
			
			// Clear the 'pre-mission cutscene' flag
			Clear_MP_Mission_Request_Option_PreMission_Cutscene(paramArrayPos)
		
			// Send the Launch set of join instructions again - this will now go to all reserved players in the launch teams now that the cutscene flag has been removed
			// Ignore Return Value - we don't really care if more players make it on or not because the mission is already active
			Reissue_Join_Requests_To_Launch_Teams_For_MP_Mission_Request(paramArrayPos, paramInfo)
			
			// The mission became active, so output more details
			#IF IS_DEBUG_BUILD
				Debug_Output_Missions_Player_Details(paramArrayPos)
			#ENDIF
			
			// More players were requested to join the mission
			RETURN TRUE
		ENDIF
			
		// The mission became Active, so output more details
		#IF IS_DEBUG_BUILD
			Debug_Output_Missions_Player_Details(paramArrayPos)
		#ENDIF
		
		// More players were not needed
		RETURN FALSE
	ENDIF
	
	// There are no active players
	// Check if there are any 'joining' players or 'confirmed' players
	IF NOT (Are_Any_Players_Joining_This_MP_Mission_Request_Slot(paramArrayPos))
	AND NOT (Are_Any_Players_Confirmed_For_This_MP_Mission_Request_Slot(paramArrayPos))
		// ...there are no joining or confirmed players either, so mark this mission as failed - it never launched
		MP_Mission_Request_Failed(paramArrayPos)
		
		// The status changed, so output the updated player details
		#IF IS_DEBUG_BUILD
			Debug_Output_Missions_Player_Details(paramArrayPos)
		#ENDIF
	ENDIF
	
	// More players were not needed
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintain an Active Mission Request
//
// INPUT PARAMS:		paramArrayPos			The array position containing the Active Mission.
PROC Maintain_Active_MP_Mission_Request(INT paramArrayPos)

	// If there are no Active players on mission, then mark the mission as finished (because there are no active players left on the mission)
	IF NOT (Are_Any_Players_Active_On_This_MP_Mission_Request_Slot(paramArrayPos))
		// ...there are no active players so mark this mission as finished
		//    (the mission itself has been launched since it is 'active', so all active players have left the mission)
		MP_Mission_Request_Finished(paramArrayPos)
	ENDIF

	// If the status of this mission changed then output the updated player details
	#IF IS_DEBUG_BUILD
		IF NOT (Is_MP_Mission_Request_Slot_Status_Active(paramArrayPos))
			Debug_Output_Missions_Player_Details(paramArrayPos)
		ENDIF
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintain an Active Mission Request
//
// INPUT PARAMS:		paramArrayPos			The array position containing the Active Mission ready for Secondary players.
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
PROC Maintain_Active_Secondary_MP_Mission_Request(INT paramArrayPos, m_structPlayerAndTeamInfoMC &paramInfo)

	// Check if there are any active players left on the mission
	IF NOT (Are_Any_Players_Active_On_This_MP_Mission_Request_Slot(paramArrayPos))
		// ...there are no active players so mark this mission as finished
		//    (the mission itself has been launched since it is 'active', so all active players have left the mission)
		MP_Mission_Request_Finished(paramArrayPos)
	ELSE
		// ...add Secondary Players to the mission
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Maintain Active Secondary Request in slot: ")
			NET_PRINT_INT(paramArrayPos)
			NET_NL()
		#ENDIF
		
		Issue_Secondary_Set_Of_Join_Requests_For_MP_Mission_Request(paramArrayPos, paramInfo)
	ENDIF

	// If the status of this mission changed then output the updated player details
	#IF IS_DEBUG_BUILD
		IF NOT (Is_MP_Mission_Request_Slot_Status_Active(paramArrayPos))
			Debug_Output_Missions_Player_Details(paramArrayPos)
		ENDIF
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintain a Reserved Mission Request
//
// INPUT PARAMS:		paramArrayPos			The array position containing the Active Mission ready for Secondary players.
PROC Maintain_Reserved_MP_Mission_Request(INT paramArrayPos)

	// If there are no Reserved By Player players on mission, then mark the mission as failed (because there are no players left that specifically requested to play the mission)
	IF (Are_Any_Players_Reserved_By_Player_For_This_MP_Mission_Request_Slot(paramArrayPos))
		EXIT
	ENDIF
	
	// There are no players specifically reserved for this reserved mission so mark it as failed
	MP_Mission_Request_Failed(paramArrayPos)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Updates a bitArray of active MP players and tidys up mission requests if a player quits MP
PROC Maintain_MP_Mission_Control_Players()

	INT bitsNetworkPlayers = ALL_MISSION_CONTROL_BITS_CLEAR
	
	// Need to build up a bitField of all players in the game, removing from mission requests any player that has quit the game
	// KGM 17/2/12: When the script host swaps teams he must clear his MPGlobals array including the already generated bitfield: MPGlobals.g_iActivePlayingPlayers.
	//					For probably only one frame, this means the host thinks the other players aren't actively playing and so the Mission Controller treats them
	//					as being off mission too and cleans up accordingly. To fix this, I need to go straight to the source data instead of the MPGlobals data,
	//					so my Net_Player_Ok check won't do the 'players in running state' check which uses the MPGlobals data, I'll check it manually.
	INT				playerAsInt	= 0
	PLAYER_INDEX	thisPlayer
	
// KGM 20/9/13: There have been a couple of subtle bugs with this recently where the players are classed as having left for a few frames (11, and 15 as seen in bugs)
//				and then come back in, but they'd been removed as reserved for the mission. Code looked at logs and verified that the player hadn't left the session,
//				so one of our script checks failed. Probably something was wiping the data (similar to the comment above), and then maybe taking a few frames (a staggered
//				loop somewhere?) to update the data again. My new method for detecting this is below.
//	REPEAT NUM_NETWORK_PLAYERS playerAsInt
//		thisPlayer = INT_TO_PLAYERINDEX(playerAsInt)
//		IF (IS_NET_PLAYER_OK(thisPlayer, FALSE, FALSE))
//			// Net player is Ok, but still need to manually check if the player is in the running state (see notes above)
//			IF (IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(thisPlayer))
//				IF (GlobalplayerBD[NATIVE_TO_INT(ThisPlayer)].iGameState = MAIN_GAME_STATE_RUNNING)
//					// Found an MP player in a valid playing state, so update the data
//					SET_BIT(bitsNetworkPlayers, playerAsInt)
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDREPEAT

	// KGM 20/9/13: To class a player as 'active' I'm going to ensure they are in the game and in the running state, but once active they'll only be classed
	//				as not active if they are no longer in the game. This should mean players only get removed if they actually have left.
	//				This means the 'previous players' bitfield starts to take an active part in the decision rather than just being used as a check afterwards.
	INT		bitsPreviousPlayers = GlobalServerBD_BlockB.missionControlData.mcBitsPreviousPlayers
	BOOL	activePlayer		= FALSE
	
	REPEAT NUM_NETWORK_PLAYERS playerAsInt
		thisPlayer = INT_TO_PLAYERINDEX(playerAsInt)
		activePlayer = NETWORK_IS_PLAYER_ACTIVE(thisPlayer)
			
		// Was this player active last frame?
		IF (IS_BIT_SET(bitsPreviousPlayers, playerAsInt))
			// ...yes, player was active last frame
			// If the player is still active then keep as active
			IF (activePlayer)
				SET_BIT(bitsNetworkPlayers, playerAsInt)
			ELSE
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MControl][Players]: Previous Player not found in slot: ")
					NET_PRINT_INT(playerAsInt)
					NET_NL()
				#ENDIF
			ENDIF
		ELSE
			// ...no, player was not active last frame
			// If the player is now active AND in the running state, then mark as active
			IF (activePlayer)
				IF (GlobalplayerBD[playerAsInt].iGameState = MAIN_GAME_STATE_RUNNING)
					SET_BIT(bitsNetworkPlayers, playerAsInt)
					
					#IF IS_DEBUG_BUILD
						NET_PRINT("...KGM MP [MControl][Players]: Player *****Joined*****: ")
						NET_PRINT_INT(playerAsInt)
						NET_PRINT(": ")
						NET_PRINT(GET_PLAYER_NAME(thisPlayer))
						NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Nothing more to do if this matches the previous MP players
	IF (bitsNetworkPlayers = GlobalServerBD_BlockB.missionControlData.mcBitsPreviousPlayers)
		EXIT
	ENDIF
	
	// The number of players has changed, so need to clean up after players that have quit MP
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl][Players]:")
		NET_PRINT_TIME()
		NET_PRINT("Mission Control has detected that the MP players has changed.")
		NET_NL()
	#ENDIF
	
	REPEAT NUM_NETWORK_PLAYERS playerAsInt
		IF NOT (IS_BIT_SET(bitsNetworkPlayers, playerAsInt))
			// ...this player is not in the game, so check if this player was previously in the game
			IF (IS_BIT_SET(GlobalServerBD_BlockB.missionControlData.mcBitsPreviousPlayers, playerAsInt))
				#IF IS_DEBUG_BUILD
					NET_PRINT("       Player Number Has Left The Game: ") NET_PRINT_INT(playerAsInt) NET_NL()
				#ENDIF
				
				Remove_Player_From_MP_Mission_Requests(playerAsInt)
			ENDIF
		ELSE
			// ...this player has joined the game, call to remove them from all old requests
			IF NOT (IS_BIT_SET(GlobalServerBD_BlockB.missionControlData.mcBitsPreviousPlayers, playerAsInt))
				IF NOT NETWORK_IS_ACTIVITY_SESSION()
					Remove_Player_From_MP_Mission_Requests(playerAsInt)
				ENDIF
				#IF IS_DEBUG_BUILD
					NET_PRINT("Player Number Has Joined The Game: ")
				// ...this player is in the game - don't need to do anything special even if they have just joined
				ELSE
					NET_PRINT("Player Number Is Still In The Game: ")
				#ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD
				// ...only interested in players that have joined the game or have stayed in the game for debug output purposes only
				thisPlayer = INT_TO_PLAYERINDEX(playerAsInt)
				NET_PRINT("       ")
				NET_PRINT_INT(playerAsInt)
				NET_PRINT("  [")
				NET_PRINT(GET_PLAYER_NAME(thisPlayer))
				NET_PRINT("]")
				NET_NL()
			#ENDIF
		ENDIF
	ENDREPEAT
	
	// Store the new player bitfield as the previous bitfield
	GlobalServerBD_BlockB.missionControlData.mcBitsPreviousPlayers = bitsNetworkPlayers

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Gather the number of players that are reserved for this mission but are in another transition session
PROC Maintain_MP_Mission_Control_Players_In_Other_Sessions()

	INT				playerAsInt		= 0
	INT				missionLoop		= 0
	INT				numOtherPlayers	= 0
	INT				theSLot			= 0
	PLAYER_INDEX	thisPlayer
	INT				otherPlayersPerMission[MAX_NUM_MISSIONS_ALLOWED]
	
	// Clear the temp storage data per mission
	REPEAT MAX_NUM_MISSIONS_ALLOWED missionLoop
		otherPlayersPerMission[missionLoop] = 0
	ENDREPEAT
	
	// For all players on a mission, gather the number of players on the same mission but in other sessions
	// NOTE: It may be possible for two players in the same mission to temporarily have different data so favour the higher value for safety
	REPEAT NUM_NETWORK_PLAYERS playerAsInt
		thisPlayer = INT_TO_PLAYERINDEX(playerAsInt)
		IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
			IF NOT (GlobalServerBD_BlockB.missionPlayerData[playerAsInt].mcpState = MP_PLAYER_STATE_NONE)
				// ...this player is active on a mission, so find out how many players are on other sessions for this player's mission
				numOtherPlayers = GET_PLAYERS_NUMBER_OF_FAKE_PLAYERS_NEEDED_FOR_MAC(thisPlayer)
				// ...if this is higher than any other player so far on this mission then store it
				theSlot = GlobalServerBD_BlockB.missionPlayerData[playerAsInt].mcpSlot
				IF (numOtherPlayers > otherPlayersPerMission[theSlot])
					otherPlayersPerMission[theSlot] = numOtherPlayers
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Update the mission data if it has changed
	REPEAT MAX_NUM_MISSIONS_ALLOWED missionLoop
		IF (GlobalServerBD_MissionRequest.missionRequests[missionLoop].mrNumPlayersInOtherSessions != otherPlayersPerMission[missionLoop])
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MControl]: The number of players on this mission in other sessions has changed. Slot: ")
				NET_PRINT_INT(missionLoop)
				NET_PRINT(" [from: ")
				NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[missionLoop].mrNumPlayersInOtherSessions)
				NET_PRINT("  to: ")
				NET_PRINT_INT(otherPlayersPerMission[missionLoop])
				NET_PRINT("]")
				NET_NL()
			#ENDIF
			
			// Update the data
			GlobalServerBD_MissionRequest.missionRequests[missionLoop].mrNumPlayersInOtherSessions = otherPlayersPerMission[missionLoop]
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	There are free players, so dynamically allocate them if any missions require more players
//
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
PROC Dynamically_Allocate_Free_Players(m_structPlayerAndTeamInfoMC &paramInfo)

	NET_NL()
	NET_PRINT("...KGM MP [MControl]: There are free players - attempting a dynamic allocation.") NET_NL()
	
	structDynamicAllocationData	sDynamicAlloc[MAX_NUM_MISSIONS_ALLOWED]
	
	// Fill the dynamic array struct
	CONST_INT			NO_DETAILS_IN_ARRAY_POSITION		-1
	
	// FIRST PASS - ONLY INTERESTED IN ALREADY ACTIVE MISSIONS WITH JOINABLE TEAMS (this will need refined)
	INT					theSlot				= 0
	INT					theTeam				= TEAM_INVALID
	INT					numMissions			= 0
//	INT					bitsJoinableTeams	= ALL_MISSION_CONTROL_BITS_CLEAR
	MP_MISSION			thisMissionID		= eNULL_MISSION
	BOOL				storeThisMission	= FALSE
//	MP_MISSION_ID_DATA	missionIdData

	NET_PRINT("   Dynamic Allocation: Attempting To Find Missions With Joinable Teams") NET_NL()	
	
	// Store details on the Dynamic Allocation array of any Active Mission Requests with a joinable team
	// NOTE: This is currently still in the same array pos as in the Mission request array to reduce processing when working out how many active players per team are on a joinable mission
	// Also store the first piece of information - the maximum players allowed on the mission from a joinable team
	// NOTE 3/9/12: Only dynamically allocate to team-based missions (this may change)
	REPEAT MAX_NUM_MISSIONS_ALLOWED theSlot
		// Ignore this array position later unless data is discovered below
		sDynamicAlloc[theSlot].arrayPos = NO_DETAILS_IN_ARRAY_POSITION
		
		IF (Is_MP_Mission_Request_Slot_Status_Active(theSlot))
			// Only deal with missions where players are in teams (this may change)
			IF (Check_If_Players_Are_In_Teams_For_Mission(Get_MP_Mission_Request_MissionID(theSlot)))
				// ...players are in teams
				// Check if this mission requires any more players
//				bitsJoinableTeams	= Get_MP_Mission_Request_Joinable_Teams(theSlot)
//				thisMissionID		= Get_MP_Mission_Request_MissionID(theSlot)
//				missionIdData		= Get_MP_Mission_Request_MissionID_Data(theSlot)
//				storeThisMission	= FALSE
				
//				REPEAT MAX_NUM_TEAMS theTeam
//					// Initialise the maximum players needed from this team to 0
//					sDynamicAlloc[theSlot].numNeededFromTeam[theTeam] = 0
//						
//					// Is this team allowed on the mission and is currently joinable?
//					IF (IS_BIT_SET(bitsJoinableTeams, theTeam))
//						// ...team active on mission
//						// Don't dynamically allocate new team members to missions that require specific players only
//						IF NOT (Should_MP_Mission_Team_Allow_Specific_Players_Only(thisMissionID, theTeam))
//							// Store the maximum players from team allowed on the mission
//							sDynamicAlloc[theSlot].numNeededFromTeam[theTeam] = Get_Maximum_Team_Members_For_MP_Mission_Request(missionIdData, theTeam)
//							
//							#IF IS_DEBUG_BUILD
//								IF NOT (storeThisMission)
//									NET_PRINT("      Mission Request Slot Needs Players For These Teams: ")
//									NET_PRINT_INT(theSlot)
//									NET_PRINT(" [")
//									NET_PRINT(GET_MP_MISSION_NAME(Get_MP_Mission_Request_MissionID(theSlot)))
//									NET_PRINT("]")
//									NET_NL()
//								ENDIF
//								
//								NET_PRINT("         ")
//								NET_PRINT(Convert_Team_To_String(theTeam))
//								NET_NL()
//							#ENDIF
//							
//							// This mission may require players
//							storeThisMission = TRUE
//						ENDIF
//					ENDIF
//					
//					// Clear the rest of this team's details at this storage position so that we get clean decision making if needed
//					sDynamicAlloc[theSlot].numActiveFromTeam[theTeam]	= 0
//					sDynamicAlloc[theSlot].pcActiveFromTeam[theTeam]	= 0
//				ENDREPEAT
				
				// If any team on this mission request may require players, then add the team details stored above on the dynamic allocation data array
//				IF (storeThisMission)
//					sDynamicAlloc[theSlot].arrayPos = theSlot
//					numMissions++
//				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Nothing to do if no missions meet the criteria
	IF (numMissions = 0)
		NET_PRINT("      <NONE>") NET_NL()
		
		EXIT
	ENDIF
	
	// Gather the second piece of information - for all missions now in the Dynamic Allocation array, work out how many active players there are per joinable team
	// NOTE: This information is currently not stored anywhere but calculated when required, so I'm going to gather this by going through all players and updating data where
	//			the player is on a joinable team on a mission listed in the Dynamic Allocation array.
	INT thePlayer = 0
	
	PLAYER_INDEX	thePlayerIndex

	NET_PRINT("   Dynamic Allocation: Finding Out Which Missions Have Players Already Allocated") NET_NL()	
	
	REPEAT NUM_NETWORK_PLAYERS thePlayer
		IF NOT (GlobalServerBD_BlockB.missionPlayerData[thePlayer].mcpState = MP_PLAYER_STATE_NONE)
			// ...this player is active on a mission, so check if the mission request slot the player is on is in the dynamic allocation array
			theSlot = GlobalServerBD_BlockB.missionPlayerData[thePlayer].mcpSlot
			IF NOT (sDynamicAlloc[theSlot].arrayPos = NO_DETAILS_IN_ARRAY_POSITION)
				// ...this player's mission is in the Dynamic Allocation array, so get this player's team and update the details if the player's team is joinable on the mission
				thePlayerIndex	= INT_TO_PLAYERINDEX(thePlayer)
				theTeam 		= GET_PLAYER_TEAM(thePlayerIndex)
				
//				// If the Dynamic Allocation array contains a number for needed players for this player's team then update the numPlayers details
//				IF (sDynamicAlloc[theSlot].numNeededFromTeam[theTeam] > 0)
//					// Update the slot details for this team
//					sDynamicAlloc[theSlot].numActiveFromTeam[theTeam] = sDynamicAlloc[theSlot].numActiveFromTeam[theTeam] + 1
//					
//					NET_PRINT("      ")
//					NET_PRINT(GET_PLAYER_NAME(thePlayerIndex))
//					NET_PRINT(" is on mission request slot: ")
//					NET_PRINT_INT(theSlot)
//					NET_PRINT(" [")
//					NET_PRINT(GET_MP_MISSION_NAME(Get_MP_Mission_Request_MissionID(theSlot)))
//					NET_PRINT("]")
//					NET_NL()
//				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Gather the third piece of information - work out how full a mission is for a team as a percentage
	// If the mission contains any team that isn't full then keep the data and move it up the dynamic allocation array - otherwise ditch the data
//	INT maxPlayers = 0
//	INT numPlayers = 0
	
	numMissions = 0

	NET_PRINT("   Dynamic Allocation: Finding Out The Percentage Full Each Team Is On A Mission That Still Needs Players, and making the array contiguous") NET_NL()	
	
	REPEAT MAX_NUM_MISSIONS_ALLOWED theSlot
		IF NOT (sDynamicAlloc[theSlot].arrayPos = NO_DETAILS_IN_ARRAY_POSITION)
			// ...this slot contains data, so go through all the teams and check if any can take more players
			storeThisMission = FALSE
			
//			REPEAT MAX_NUM_TEAMS theTeam
//				maxPlayers = sDynamicAlloc[theSlot].numNeededFromTeam[theTeam]
//				numPlayers = sDynamicAlloc[theSlot].numActiveFromTeam[theTeam]
//				
//				IF (maxPlayers = 0)
//				OR (numPlayers >= maxPlayers)
//					// ...mission doesn't need any more players for this team
//					sDynamicAlloc[theSlot].pcActiveFromTeam[theTeam] = 100
//				ELSE
//					// ...calculate how full of players this team is as an (INT) percentage
//					// NOTE: using brackets to enforce calculation order, but I'm sure it's unnecessary
//					sDynamicAlloc[theSlot].pcActiveFromTeam[theTeam] = ((numPlayers * 100) / maxPlayers)
//					
//					// Ensure this mission is stored
//					storeThisMission = TRUE
//				ENDIF
//			ENDREPEAT
			
			// If this mission should be stored, then move this mission's data up the array
			IF (storeThisMission)
				sDynamicAlloc[numMissions] = sDynamicAlloc[theSlot]
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("      ")
					NET_PRINT_INT(numMissions)
					NET_PRINT(": ")
					NET_PRINT(GET_MP_MISSION_NAME(Get_MP_Mission_Request_MissionID(theSlot)))
					NET_PRINT(" -")
//					REPEAT MAX_NUM_TEAMS theTeam
//						NET_PRINT(" [")
//						NET_PRINT(Convert_Team_To_String(theTeam))
//						NET_PRINT(" ")
//						NET_PRINT_INT(sDynamicAlloc[numMissions].pcActiveFromTeam[theTeam])
//						NET_PRINT("%]")
//					ENDREPEAT
					NET_NL()
				#ENDIF

				numMissions++
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Nothing more to do if there are no missions stored
	IF (numMissions = 0)
		EXIT
	ENDIF
	
	// All the data should now be filled out, and the array should be contiguous containing only missions where at least one team needs more players
	// Start to distribute free players to existing missions that need more players
	// FIRST PASS - a basic distribution algorithm - each free player will be added to the mission with the smallest percentage of active members of that team
	INT bitsFreePlayers 	= paramInfo.bitsFreePlayers
//	INT smallestPercent		= 100
	INT smallestLocation	= 0
//	INT thisPercent			= 0
	INT thisRequestArrayPos	= 0
	
	BOOL invitePlayerOntoMission	= FALSE
	BOOL usePreMissionComms			= FALSE

	NET_PRINT("   Dynamic Allocation: Distributing the free players to the mission") NET_NL()	
	
	REPEAT NUM_NETWORK_PLAYERS thePlayer
		IF (IS_BIT_SET(bitsFreePlayers, thePlayer))
			// Found a free player
			//smallestPercent		= 100
			smallestLocation	= NO_DETAILS_IN_ARRAY_POSITION
			
			thePlayerIndex		= INT_TO_PLAYERINDEX(thePlayer)
			theTeam 			= GET_PLAYER_TEAM(thePlayerIndex)
			
			NET_PRINT("      ")
			NET_PRINT(GET_PLAYER_NAME(thePlayerIndex))
			NET_PRINT(" [")
			NET_PRINT(Convert_Team_To_String(theTeam))
			NET_PRINT("]  -  ")
			
//			// Go through all the mission requests that require players to find the one that needs most players from this team
//			REPEAT numMissions theSlot
//				thisPercent = sDynamicAlloc[theSlot].pcActiveFromTeam[theTeam]
//				IF (thisPercent < smallestPercent)
//					// Ignore this mission if the player has been excluded from it, or has actively rejected it
//					IF NOT (Is_Player_Rejected_From_MP_Mission_Request_Slot(sDynamicAlloc[theSlot].arrayPos, thePlayerIndex))
//					AND NOT (Is_Player_Excluded_From_MP_Mission_Request_Slot(sDynamicAlloc[theSlot].arrayPos, thePlayerIndex))
//						// ...this is the smallest percentage so far and the player is not excluded from the mission
//						smallestPercent		= thisPercent
//						smallestLocation	= theSlot
//					ENDIF
//				ENDIF
//			ENDREPEAT
			
			// If a location was found for this player, add the player to the mission
			IF NOT (smallestLocation = NO_DETAILS_IN_ARRAY_POSITION)
				// Add the player to the mission
				NET_PRINT("ADDING PLAYER TO THIS DYNAMIC ALLOCATION SLOT: ")
				NET_PRINT_INT(smallestLocation)
				NET_NL()
				
				thisRequestArrayPos			= sDynamicAlloc[smallestLocation].arrayPos
				thisMissionID				= Get_MP_Mission_Request_MissionID(thisRequestArrayPos)
				invitePlayerOntoMission		= Should_MP_Mission_Team_Invite_Player_Onto_Mission(thisMissionID, theTeam)
				
				IF (Should_MP_Mission_Team_Use_A_PreMission_Phonecall(thisMissionID, theTeam))
				OR (Should_MP_Mission_Team_Use_A_PreMission_Txtmsg(thisMissionID, theTeam))
					usePreMissionComms = TRUE
				ENDIF
				
				Issue_Broadcast_For_Player_To_Join_MP_Mission_Request(thisRequestArrayPos, thePlayerIndex, invitePlayerOntoMission, usePreMissionComms)
				
				// Update the team numbers and percentage for this mission
				//sDynamicAlloc[smallestLocation].numActiveFromTeam[theTeam] = sDynamicAlloc[smallestLocation].numActiveFromTeam[theTeam] + 1
				//sDynamicAlloc[smallestLocation].pcActiveFromTeam[theTeam] = ((sDynamicAlloc[smallestLocation].numActiveFromTeam[theTeam] * 100) / sDynamicAlloc[smallestLocation].numNeededFromTeam[theTeam])
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF (smallestLocation = NO_DETAILS_IN_ARRAY_POSITION)
					NET_PRINT("PLAYER CANNOT BE ADDED TO ANY SLOT (Perhaps Player Rejected or Excluded from a mission?)")
					NET_NL()
				ENDIF
			#ENDIF
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain dynamic player allocation if necessary
//
// RETURN PARAMS:		paramInfo				The struct containing Player and Team Info used during decision making
PROC Maintain_Dynamic_Player_Allocation(m_structPlayerAndTeamInfoMC &paramInfo)

	// TEMP - Ensure dynamic allocation is allowed
	IF NOT (GlobalServerBD.g_allowDynamicAllocation)
		EXIT
	ENDIF
	
	// Check if the Dynamic Allocation needs restarted whether or not it has expired
	IF (GlobalServerBD_BlockB.missionControlData.mcRestartDynamicAllocationTimer)
		GlobalServerBD_BlockB.missionControlData.mcRestartDynamicAllocationTimer	= FALSE
		GlobalServerBD_BlockB.missionControlData.mcDynamicAllocTimeout				= GET_TIME_OFFSET(GET_NETWORK_TIME(), DYNAMIC_ALLOCATION_DELAY_msec)
		
		NET_PRINT("...KGM MP [MControl]: Forced Restart Dynamic Allocation Timer") NET_NL()
		
		EXIT
	ENDIF
	
	// Check for Dynamic Allocation timeout
	IF (IS_TIME_LESS_THAN(GET_NETWORK_TIME(), GlobalServerBD_BlockB.missionControlData.mcDynamicAllocTimeout))
		// Network time is still less than stored time, so not timed out
		EXIT
	ENDIF
	
	// Timeout has occurred, so reset the timer
	GlobalServerBD_BlockB.missionControlData.mcDynamicAllocTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), DYNAMIC_ALLOCATION_DELAY_msec)
	
	// Perform Dynamic Allocation
	IF (paramInfo.numPlayersFree > 0)
		Dynamically_Allocate_Free_Players(paramInfo)
	ENDIF
	
	// This is where the dynamic allocation of already reserved players occurs (and possibly already active players?)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Performs any regular mission control updates
// NOTES:	Can't sort the array because it is global broadcast data.
PROC Maintain_MP_Mission_Control_Server()
	
	INT tempLoop = 0
	
	// If the host is this player but the 'host attempted host migration' flag is set then this player must have attempted to pass on hosting duties
	//			but it failed, so force a host migration resync
	BOOL forceResync = FALSE
	IF (g_hostAttemptedHostMigration)
		IF (NETWORK_IS_HOST_OF_THIS_SCRIPT())
			g_hostAttemptedHostMigration						= FALSE
			forceResync											= TRUE
			GlobalServerBD_BlockB.forceMissionControlResyncID++
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT("(SERVER) Force Resync, there must have been a failed attempt to pass on Hosting - g_hostAttemptedHostMigration is TRUE")
				NET_NL()
			#ENDIF
		ENDIF
	ENDIF
	
	// Maintain Mission Control Host Player
	IF NOT (GlobalServerBD_BlockB.missionControlData.mcHost = PLAYER_ID())
	OR (forceResync)
		GlobalServerBD_BlockB.missionControlData.mcHost = PLAYER_ID()
			
		#IF IS_DEBUG_BUILD
			PLAYER_INDEX thisPlayer
			IF (forceResync)
				NET_PRINT("...KGM MP [MControl]: Mission Control HOST has returned to be this player after a failed attempt to pass to a new Host. Forcing Resync: ")
			ELSE
				NET_PRINT("...KGM MP [MControl]: Mission Control HOST has changed to be this player: ")
			ENDIF
			NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
			NET_NL()
			NET_PRINT("   Mission Control has detected these players were in the game with previous server at host migration:") NET_NL()
			REPEAT NUM_NETWORK_PLAYERS tempLoop
				IF (IS_BIT_SET(GlobalServerBD_BlockB.missionControlData.mcBitsPreviousPlayers, tempLoop))
					NET_PRINT("      ")
					NET_PRINT_INT(tempLoop)
					IF (tempLoop < 10)
						NET_PRINT(" ")
					ENDIF
					NET_PRINT(":  ")
					thisPlayer = INT_TO_PLAYERINDEX(tempLoop)
					IF (IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(thisPlayer))
					AND (GlobalplayerBD[NATIVE_TO_INT(ThisPlayer)].iGameState = MAIN_GAME_STATE_RUNNING)
					AND (IS_NET_PLAYER_OK(thisPlayer, FALSE, FALSE))
						NET_PRINT(GET_PLAYER_NAME(thisPlayer))
					ELSE
						NET_PRINT("Player in 'previous players' list does not appear to be in-game")
					ENDIF
					NET_NL()
				ENDIF
			ENDREPEAT
			NET_PRINT("   Mission Control has detected these mission requests and players were in the game with previous server at host migration:") NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_And_Players_To_Console_Log()
		#ENDIF
		
		// Just to be safe, clear this now
		g_hostAttemptedHostMigration = FALSE
	ENDIF
	
	// Maintain Active Players - cleanup if players quit MP - do this every frame now because Transition Sessions mean the data changes
	// 		more frequently and bug 1331209 may be related to this not being fully up to date
	Maintain_MP_Mission_Control_Players()

	// Check if there are any active mission requests, if not then no processing needs done
	IF (Are_All_MP_Mission_Request_Slots_Empty())
		// Ensure Dynamic Allocation timer gets restarted next time a mission request is received
		GlobalServerBD_BlockB.missionControlData.mcRestartDynamicAllocationTimer = TRUE
		
		EXIT
	ENDIF
	
	// Maintain the number of players on a mission that are in other sessions
	Maintain_MP_Mission_Control_Players_In_Other_Sessions()

	// Generate the Player and Team decision making details
	m_structPlayerAndTeamInfoMC playerAndTeamInfo
	Fill_Current_Player_And_Team_Info(playerAndTeamInfo)
	
	// Loop through all mission requests maintaining any that need it
	CONST_INT	RECEIVED_MISSION_NOT_FOUND	-1
	
	INT		arrayPosFirstReceivedMission	= RECEIVED_MISSION_NOT_FOUND
	BOOL	expensiveProcessingPerformed	= FALSE
	BOOL	regeneratePlayerAndTeamData		= FALSE

	g_eMPMissionStatus slotStatusID = NO_MISSION_REQUEST_RECEIVED

	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		slotStatusID = Get_MP_Mission_Request_Slot_StatusID(tempLoop)
		
		SWITCH (slotStatusID)
			CASE MP_MISSION_STATE_ACTIVE
				Maintain_Active_MP_Mission_Request(tempLoop)
				BREAK
				
			CASE MP_MISSION_STATE_ACTIVE_SECONDARY
				Maintain_Active_Secondary_MP_Mission_Request(tempLoop, playerAndTeamInfo)
				regeneratePlayerAndTeamData		= TRUE
				expensiveProcessingPerformed	= TRUE
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MControl]: Expensive Function Performed: Maintain_Active_Secondary_MP_Mission_Request() requested more players.")
					NET_NL()
				#ENDIF
				BREAK
				
			CASE MP_MISSION_STATE_OFFERED
				IF (Maintain_Offered_MP_Mission_Request(tempLoop, playerAndTeamInfo))
					regeneratePlayerAndTeamData		= TRUE
					expensiveProcessingPerformed	= TRUE
					
					#IF IS_DEBUG_BUILD
						NET_PRINT("...KGM MP [MControl]: Expensive Function Performed: Maintain_Offered_MP_Mission_Request() became an Active mission and requested more players.")
						NET_NL()
					#ENDIF
				ENDIF
				BREAK
				
			CASE MP_MISSION_STATE_RECEIVED
				// We'll only do one of these per update, so store the array position of the first one
				IF (arrayPosFirstReceivedMission = RECEIVED_MISSION_NOT_FOUND)
					arrayPosFirstReceivedMission = tempLoop
				ENDIF
				BREAK
				
			CASE MP_MISSION_STATE_RESERVED
				Maintain_Reserved_MP_Mission_Request(tempLoop)
				BREAK
				
			CASE NO_MISSION_REQUEST_RECEIVED
				// Nothing to do
				BREAK
				
			DEFAULT
				NET_PRINT("...KGM MP: Maintain_MP_Mission_Control_Server() - FAILED TO FIND SLOT STATUS ID - ADD TO SWITCH: ")
				NET_PRINT(Convert_Mission_Status_To_String(slotStatusID))
				NET_NL()
				SCRIPT_ASSERT("Maintain_MP_Mission_Control_Server() - UNKNOWN MISSION REQUEST SLOT STATUS ID - Add To Switch. Check Console Log. Tell Keith.")
				BREAK
		ENDSWITCH
		
		IF (regeneratePlayerAndTeamData)
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MControl]: Regenerating Player and Team Data.")
				NET_NL()
			#ENDIF
		
			Fill_Current_Player_And_Team_Info(playerAndTeamInfo)
			regeneratePlayerAndTeamData = FALSE
		ENDIF
	ENDREPEAT
	
	// If an expensive function was performed then nothing more should be done this frame
	IF (expensiveProcessingPerformed)
		#IF IS_DEBUG_BUILD
			IF NOT (arrayPosFirstReceivedMission = RECEIVED_MISSION_NOT_FOUND)
				NET_PRINT("...KGM MP [MControl]: There was a Received Mission to process, waiting until next frame - an expensive function has already ben performed.")
				NET_NL()
			ENDIF
		#ENDIF
	
		EXIT
	ENDIF
	
	// An expensive function wasn't performed, so check if there is a received mission to be processed this update
	IF NOT (arrayPosFirstReceivedMission = RECEIVED_MISSION_NOT_FOUND)
		// ...there is a received mission to update
		Maintain_Received_MP_Mission_Request(arrayPosFirstReceivedMission, playerAndTeamInfo)
		
		EXIT
	ENDIF
	
	// Nothing expensive has been done this frame, so maintain dynamic allocation
	Maintain_Dynamic_Player_Allocation(playerAndTeamInfo)
	
ENDPROC




// ===========================================================================================================
//      The Main MP Mission Control Routines - Client
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Client Broadcast Retain Functions
//		(Allows server broadcasts to be retained on a Client for a full frame)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the uniqueID for a mission has changed.
//
// INPUT PARAMS:		paramUniqueID			The 'old' unique ID to check for
// RETURN VALUE:		INT						The 'new' unique ID (this will be the same as the old one if it hasn't changed)
FUNC INT Get_MP_Mission_Request_Changed_UniqueID(INT paramUniqueID)

	IF (paramUniqueID = NO_UNIQUE_ID)
		RETURN NO_UNIQUE_ID
	ENDIF

	INT ReturnUniqueID = paramUniqueID
	
	INT tempLoop = 0
	REPEAT g_numMCBroadcasts tempLoop
		IF (g_sMCBroadcasts[tempLoop].MCB_Type = MCCBT_CHANGE_UNIQUE_ID)
			IF (g_sMCBroadcasts[tempLoop].uniqueID = paramUniqueID)
				// ...return the updated uniqueID
				RETURN (g_sMCBroadcasts[tempLoop].generalInt1)
			ENDIF
		ENDIF
	ENDREPEAT
	
	// No Change
	RETURN ReturnUniqueID

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the reservation has been confirmed and retrieve the associated player data.
//
// INPUT PARAMS:		paramUniqueID			The unique ID
// RETURN PARAMS:		paramNumPlayers			The Number of Players reserved for the mission
//						paramBitsPlayers		The Bitfield of the players reserved for the mission
// RETURN VALUE:		BOOL					TRUE if confirmation has been received, otherwise FALSE
FUNC BOOL Has_MP_Mission_Request_Just_Broadcast_Confirm_Reservation(INT paramUniqueID, INT &paramNumPlayers, int &paramBitsPlayers)

	IF (paramUniqueID = NO_UNIQUE_ID)
		RETURN FALSE
	ENDIF

	paramNumPlayers = 0
	paramBitsPlayers = ALL_MISSION_CONTROL_BITS_CLEAR
	
	INT tempLoop = 0
	REPEAT g_numMCBroadcasts tempLoop
		IF (g_sMCBroadcasts[tempLoop].MCB_Type = MCCBT_CONFIRMED_RESERVED)
			IF (g_sMCBroadcasts[tempLoop].uniqueID = paramUniqueID)
				// ...store the details and return TRUE
				paramNumPlayers = g_sMCBroadcasts[tempLoop].generalInt1
				paramBitsPlayers = g_sMCBroadcasts[tempLoop].generalInt2
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// No confirmation
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player is on the mission with this uniqueID.
//
// INPUT PARAMS:		paramUniqueID			The unique ID to check for
// RETURN VALUE:		BOOL					TRUE if the player is active on the mission request with this Unique ID
FUNC BOOL Is_Player_On_Mission_On_MP_Mission_Request(INT paramUniqueID)

	IF (paramUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Is_Player_On_Mission_On_MP_Mission_Request() - IGNORING: Checked with NO_UNIQUE_ID by: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
			
			SCRIPT_ASSERT("Is_Player_On_Mission_On_MP_Mission_Request() - called with NO_UNIQUE_ID")
		#ENDIF
		
		RETURN FALSE
	ENDIF

	INT tempLoop = 0
	REPEAT g_numMCBroadcasts tempLoop
		IF (g_sMCBroadcasts[tempLoop].MCB_Type = MCCBT_PLAYER_ON_MISSION)
			IF (g_sMCBroadcasts[tempLoop].uniqueID = paramUniqueID)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Not Found
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store a set of MC Broadcast data received.
//
// INPUT PARAMS:		paramType				The broadcast type ID
// 						paramUniqueID			The unique ID
//						paramInt1				[DEFAULT = 0] A general purpose INT used by some broadcast data types
//						paramInt2				[DEFAULT = 0] A general purpose INT used by some broadcast data types
PROC Store_MP_Mission_Request_Received_Broadcast(g_eMCBroadcastType paramType, INT paramUniqueID, INT paramInt1 = 0, INT paramInt2 = 0)

	// Always forward this broadcast details to the Resync Array so that appropriate events can be removed from the resync array
	Update_Resync_Storage_Based_On_Replies(paramType, paramUniqueID)

	IF (g_numMCBroadcasts >= MAX_MISSION_CONTROL_BROADCAST_ARRAY)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Store_MP_Mission_Request_Received_Broadcast() - FAILED TO FIND FREE SLOT ON CLIENT MC BROADCAST ARRAY") NET_NL()
			Debug_Output_Mission_Control_Broadcast_Array()
		#ENDIF
		
		SCRIPT_ASSERT("Store_MP_Mission_Request_Received_Broadcast() - Failed to Find Free slot on 'Broadcast' array. Increase MAX_MISSION_CONTROL_BROADCAST_ARRAY. See Console Log. Tell Keith.")
		EXIT
	ENDIF
	
	// Store the data
	g_sMCBroadcasts[g_numMCBroadcasts].MCB_Type		= paramType
	g_sMCBroadcasts[g_numMCBroadcasts].uniqueID		= paramUniqueID
	g_sMCBroadcasts[g_numMCBroadcasts].generalInt1	= paramInt1
	g_sMCBroadcasts[g_numMCBroadcasts].generalInt2	= paramInt2
	g_sMCBroadcasts[g_numMCBroadcasts].frameRetain	= TRUE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Store_MP_Mission_Request_Received_Broadcast") NET_NL()
		Debug_Output_One_Mission_Control_Broadcast(g_numMCBroadcasts)
	#ENDIF
	
	g_numMCBroadcasts++

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear out the Broadcast Storage Array
PROC MP_Mission_Control_Client_Clear_Broadcast_Storage_Data()

	INT tempLoop = 0
	REPEAT MAX_MISSION_CONTROL_BROADCAST_ARRAY tempLoop
		g_sMCBroadcasts[tempLoop].MCB_Type		= MCCBT_NONE
		g_sMCBroadcasts[tempLoop].uniqueID		= NO_UNIQUE_ID
		g_sMCBroadcasts[tempLoop].generalInt1	= 0
		g_sMCBroadcasts[tempLoop].generalInt2	= 0
		g_sMCBroadcasts[tempLoop].frameRetain	= FALSE
	ENDREPEAT
	
	g_numMCBroadcasts = 0

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Broadcast Storage Array
// NOTES:	On the first update the bit gets set to FALSE and the entry is shuffled up the array to overwrite any old data, on the second update the entry gets cleared
PROC Maintain_MP_Mission_Control_Client_Broadcast_Storage_Data()

	IF (g_numMCBroadcasts = 0)
		EXIT
	ENDIF
	
	// There is data on the array
	INT fromPos = 0
	INT toPos = 0
	REPEAT g_numMCBroadcasts fromPos
		// If the retainFrame flag is set then clear it, otherwise delete the entry
		IF (g_sMCBroadcasts[fromPos].frameRetain)
			// ...retain flag is set, so clear it
			g_sMCBroadcasts[fromPos].frameRetain = FALSE
			
			// ...shuffle this entry up the array overwriting old entries, if necessary
			IF NOT (fromPos = ToPos)
				// ...shuffle up the array
				g_sMCBroadcasts[toPos] = g_sMCBroadcasts[fromPos]
				
				// ...clear the entry at the old position
				g_sMCBroadcasts[fromPos].MCB_Type = MCCBT_NONE
			ENDIF
			
			// ...update to the next position to be overwritten
			toPos++
		ELSE
			// ...retain flag is clear, so this data will get overwritten
			g_sMCBroadcasts[fromPos].MCB_Type = MCCBT_NONE
		ENDIF
	ENDREPEAT
	
	// The number of entries remaining in the array should equal the new 'to' position value
	g_numMCBroadcasts = toPos
	
	// Display the array if there is data in it
	#IF IS_DEBUG_BUILD
		Debug_Output_Mission_Control_Broadcast_Array()
	#ENDIF
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Client State Checking Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this player is allocated to any mission
//
// INPUT PARAMS:		paramPlayerID		Player Index
//
// NOTES:	Assumes the PlayerIndex is valid
FUNC BOOL Is_Player_Allocated_For_Any_Mission(PLAYER_INDEX paramPlayerID)
	
	INT playerIndexAsInt = NATIVE_TO_INT(paramPlayerID)
	
	// If the player state is NONE then the player is not allocated to any mission
	IF (GlobalServerBD_BlockB.missionPlayerData[playerIndexAsInt].mcpState = MP_PLAYER_STATE_NONE)
		// ...not allocated to any mission
		RETURN FALSE
	ENDIF
	
	// ...allocated to a mission - this may just be 'reserved' or 'required' rather than actively on the mission
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the uniqueID for a player's mission
//
// INPUT PARAMS:		paramPlayerID		Player Index
// RETURN VALUE:		INT					UniqueID of player's mission
//
// NOTES:	Assumes the PlayerIndex is valid
FUNC INT Get_UniqueID_For_This_Players_Mission(PLAYER_INDEX paramPlayerID)
	
	INT playerIndexAsInt = NATIVE_TO_INT(paramPlayerID)
	
	// If the player state is NONE then the player is not allocated to any mission
	IF (GlobalServerBD_BlockB.missionPlayerData[playerIndexAsInt].mcpState = MP_PLAYER_STATE_NONE)
		// ...not allocated to any mission
		RETURN NO_UNIQUE_ID
	ENDIF
	
	// Player is allocated to a mission, so ensure the mission slot is valid
	INT missionSlot = GlobalServerBD_BlockB.missionPlayerData[playerIndexAsInt].mcpSlot
	IF (missionSlot = MPMCP_NO_MISSION_SLOT)
		RETURN NO_UNIQUE_ID
	ENDIF
	
	// Found an active mission slot, so return the uniqueID associated with the slot
	RETURN (Get_MP_Mission_Request_UniqueID(missionSlot))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the missionID for a player's mission
//
// INPUT PARAMS:		paramPlayerID		Player Index
// RETURN VALUE:		MP_MISSION			MissionID of player's mission, or eNULL_MISSION
//
// NOTES:	Assumes the PlayerIndex is valid
FUNC MP_MISSION Get_MissionID_For_This_Players_Mission(PLAYER_INDEX paramPlayerID)
	
	INT playerIndexAsInt = NATIVE_TO_INT(paramPlayerID)
	
	// If the player state is NONE then the player is not allocated to any mission
	IF (GlobalServerBD_BlockB.missionPlayerData[playerIndexAsInt].mcpState = MP_PLAYER_STATE_NONE)
		// ...not allocated to any mission
		RETURN eNULL_MISSION
	ENDIF
	
	// Player is allocated to a mission, so ensure the mission slot is valid
	INT missionSlot = GlobalServerBD_BlockB.missionPlayerData[playerIndexAsInt].mcpSlot
	IF (missionSlot = MPMCP_NO_MISSION_SLOT)
		RETURN eNULL_MISSION
	ENDIF
	
	// Found an active mission slot, so return the missionID associated with the slot
	RETURN (Get_MP_Mission_Request_MissionID(missionSlot))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if another player can be reserved on the specified player's activity
//
// INPUT PARAMS:		paramPlayerID		Player Index
// RETURN VALUE:		BOOL				TRUE if there is space for another player to be reserved for this mission, otherwise FALSE
//
// NOTES:	KGM 25/1/13: To simplify this I'm not taking team info into account at the moment, but I may need to for CnC in future.
FUNC BOOL Can_Another_Player_Be_Reserved_On_This_Players_Mission(PLAYER_INDEX paramPlayerID)

	MP_MISSION playersMissionID = Get_MissionID_For_This_Players_Mission(paramPlayerID)

	IF (Check_If_Players_Are_In_Teams_For_Mission(playersMissionID))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: This function isn't yet set up to deal with players in teams.") NET_NL()
			SCRIPT_ASSERT("Can_Another_Player_Be_Reserved_On_This_Players_Mission(): ERROR - This function isn't set up to deal with team-based missions yet. Returning TRUE. Tell Keith.")
		#ENDIF
		
		// For now, allow another player to join a team-based mission
		RETURN TRUE
	ENDIF

	// Non-team-based missions
	INT playerIndexAsInt				= NATIVE_TO_INT(paramPlayerID)
	INT playersMissionSlot				= GlobalServerBD_BlockB.missionPlayerData[playerIndexAsInt].mcpSlot
	
	// Check if the player is still allocated to a slot and return false if not
	IF (playersMissionSlot = MPMCP_NO_MISSION_SLOT)
		RETURN FALSE
	ENDIF
	
	// Is the mission still reserved?
	IF NOT (Is_MP_Mission_Request_Slot_Status_Reserved(playersMissionSlot))
		RETURN FALSE
	ENDIF
	
	// KGM 25/2/13: Is the mission still joinable
	IF NOT (Does_MP_Mission_Request_Allow_Players_To_Join_Mission(playersMissionSlot))
		RETURN FALSE
	ENDIF
		
	// Do general mission full checks that aren't team-based
	MP_MISSION_ID_DATA	missionIdData					= Get_MP_Mission_Request_MissionID_Data(playersMissionSlot)
	INT					maxAllowedOnMission 			= Get_Maximum_Players_Required_For_MP_Mission_Request(missionIdData)
	INT					bitsReservedByPlayerOnMission	= Get_MP_Mission_Request_Reserved_By_Player_Players(playersMissionSlot)
	INT					numPlayersInOtherSessions		= GlobalServerBD_MissionRequest.missionRequests[playersMissionSlot].mrNumPlayersInOtherSessions
	INT					tempLoop						= 0
	INT					numOnMission					= 0
	
	numOnMission = 0
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(bitsReservedByPlayerOnMission, tempLoop))
			numOnMission++
		ENDIF
	ENDREPEAT
	
	// KGM 14/6/13: Add on players in other sessions on this mission as being rserved for the mission too when doing the 'mission full' check
	numOnMission += numPlayersInOtherSessions
	
	// Don't allow more players if the mission is already full of reserved players
	IF (numOnMission >= maxAllowedOnMission)
		RETURN FALSE
	ENDIF
	
	// Mission isn't full of reserved players, so another player can join it
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return a Player Bitfield containing all players that are actively on the mission or actively joining the mission
//
// INPUT PARAMS:		paramUniqueID			The UniqueID for this mission
// RETURN VALUE:		INT						A bitfield of players Active, Confirmed, or Joining mission, or ALL_MISSION_CONTROL_BITS_CLEAR if there are none
FUNC INT Get_Bitfield_Of_Players_On_Or_Joining_Mission(INT paramUniqueID)

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Get_Bitfield_Of_Players_On_Or_Joining_Mission() requested by ")
		NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT(" for UniqueID: ")
		NET_PRINT_INT(paramUniqueID)
		NET_NL()
	#ENDIF

	INT	returnBits	= ALL_MISSION_CONTROL_BITS_CLEAR
	
	// Return bits all clear if uniqueID isn't set
	IF (paramUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("      BUT: UniqueID is NO_UNIQUE_ID. Players Returned:") NET_NL()
			Debug_Output_Player_Names_From_Bitfield(returnBits)
		#ENDIF
	
		RETURN (returnBits)
	ENDIF
	
	// Get mission slot from UniqueID
	INT missionSlot	= Find_Slot_Containing_This_MP_Mission(paramUniqueID)
	
	// Return bits all clear if uniqueID doesn't exist
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		#IF IS_DEBUG_BUILD
			NET_PRINT("      BUT: UniqueID Not Found. Players Returned:") NET_NL()
			Debug_Output_Player_Names_From_Bitfield(returnBits)
		#ENDIF
	
		RETURN (returnBits)
	ENDIF
	
	// Found the mission slot, so gather the details
	INT	activePlayers		= Get_MP_Mission_Request_Active_Players(missionSlot)
	INT	confirmedPlayers	= Get_MP_Mission_Request_Confirmed_Players(missionSlot)
	INT	joiningPlayers		= Get_MP_Mission_Request_Joining_Players(missionSlot)
	
	returnBits	= 	activePlayers
	returnBits	|=	confirmedPlayers
	returnBits	|=	joiningPlayers
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      SUCCESS: Returning These Players:") NET_NL()
		Debug_Output_Player_Names_From_Bitfield(returnBits)
		NET_NL()
		NET_PRINT("      [Player State Breakdown]:") NET_NL()
		NET_PRINT("      Active Players: ") NET_NL()
		Debug_Output_Player_Names_From_Bitfield(activePlayers)
		NET_PRINT("      Confirmed Players: ") NET_NL()
		Debug_Output_Player_Names_From_Bitfield(confirmedPlayers)
		NET_PRINT("      Joining Players: ") NET_NL()
		Debug_Output_Player_Names_From_Bitfield(joiningPlayers)
	#ENDIF
	
	RETURN (returnBits)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return a Player Bitfield containing all players that are Reserved (By Player) for the mission
//
// INPUT PARAMS:		paramUniqueID			The UniqueID for this mission
// RETURN VALUE:		INT						A bitfield of players Reserved By Player for the mission, or ALL_MISSION_CONTROL_BITS_CLEAR if there are none
FUNC INT Get_Bitfield_Of_Players_That_Reserved_Themselves_For_The_Mission(INT paramUniqueID)

	INT	returnBits	= ALL_MISSION_CONTROL_BITS_CLEAR
	
	// Return bits all clear if uniqueID isn't set
	IF (paramUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Get_Bitfield_Of_Players_That_Reserved_Themselves_For_The_Mission() requested by ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_PRINT(" for UniqueID: ")
			NET_PRINT_INT(paramUniqueID)
			NET_NL()
			NET_PRINT("      BUT: UniqueID is NO_UNIQUE_ID. Players Returned:") NET_NL()
			Debug_Output_Player_Names_From_Bitfield(returnBits)
		#ENDIF
	
		RETURN (returnBits)
	ENDIF
	
	// Get mission slot from UniqueID
	INT missionSlot	= Find_Slot_Containing_This_MP_Mission(paramUniqueID)
	
	// Return bits all clear if uniqueID doesn't exist
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Get_Bitfield_Of_Players_That_Reserved_Themselves_For_The_Mission() requested by ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_PRINT(" for UniqueID: ")
			NET_PRINT_INT(paramUniqueID)
			NET_NL()
			NET_PRINT("      BUT: UniqueID Not Found. Players Returned:") NET_NL()
			Debug_Output_Player_Names_From_Bitfield(returnBits)
		#ENDIF
	
		RETURN (returnBits)
	ENDIF
	
	// Found the mission slot, so gather the details
	returnBits = Get_MP_Mission_Request_Reserved_By_Player_Players(missionSlot)
	
	RETURN (returnBits)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the number of players that are Reserved (By Player) for the mission
//
// INPUT PARAMS:		paramUniqueID			The UniqueID for this mission
// RETURN VALUE:		INT						The number of players that reserved themselves for the mission
FUNC INT Get_Number_Of_Players_That_Reserved_Themselves_For_The_Mission(INT paramUniqueID)

	INT reservedBitfield = Get_Bitfield_Of_Players_That_Reserved_Themselves_For_The_Mission(paramUniqueID)
	
	IF (reservedBitfield = ALL_PLAYER_BITS_CLEAR)
		RETURN 0
	ENDIF
	
	INT				numberOfPlayers	= 0
	INT				tempLoop		= 0
	
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(reservedBitfield, tempLoop))
			numberOfPlayers++
		ENDIF
	ENDREPEAT
	
	RETURN numberOfPlayers

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission has started (this is just a more user-friendly version of 'is mission active')
//
// INPUT PARAMS:		paramUniqueID			The UniqueID for this mission
// RETURN VALUE:		BOOL					TRUE if the mission has started, otherwise FALSE
FUNC BOOL Has_This_Mission_Request_Mission_Started(INT paramUniqueID)

	// NOTE: Returning FALSE may not be the right thing here - perhaps an assert is needed?
	IF (paramUniqueID = NO_UNIQUE_ID)
		RETURN FALSE
	ENDIF
	
	// Get mission slot from UniqueID
	INT missionSlot	= Find_Slot_Containing_This_MP_Mission(paramUniqueID)
	
	// NOTE: Returning FALSE may not be the right thing to do here
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		RETURN FALSE
	ENDIF
	
	// Check if the mission is active
	IF (Is_MP_Mission_Request_Slot_Status_Active(missionSlot))
	OR (Is_MP_Mission_Request_Slot_Status_Active_Add_Secondary_Players(missionSlot))
		RETURN TRUE
	ENDIF
	
	// Not Active
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is still joinable
//
// INPUT PARAMS:		paramUniqueID			The UniqueID for this mission
// RETURN VALUE:		BOOL					TRUE if the mission is joinable, otherwise FALSE
//
// NOTES:	I'm just doing the individual players check at the moment, this may need to change for CnC teams
FUNC BOOL Is_This_Mission_Request_Mission_Still_Joinable(INT paramUniqueID)

	// NOTE: Returning FALSE may not be the right thing here - perhaps an assert is needed?
	IF (paramUniqueID = NO_UNIQUE_ID)
		RETURN FALSE
	ENDIF
	
	// Get mission slot from UniqueID
	INT missionSlot	= Find_Slot_Containing_This_MP_Mission(paramUniqueID)
	
	// NOTE: Returning FALSE may not be the right thing to do here
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		RETURN FALSE
	ENDIF
	
	// Check if the mission is still joinable
	IF (Does_MP_Mission_Request_Allow_Players_To_Join_Mission(missionSlot))
		RETURN TRUE
	ENDIF
	
	// Not Joinable
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a mission matching these details is active and joinable
//
// RETURN PARAMS:		paramMissionIdData			The MissionID Data
// RETURN VALUE:		BOOL						TRUE if a mission with these details is active and joinable
FUNC BOOL Is_This_Mission_Request_Active_And_Joinable(MP_MISSION_ID_DATA &paramMissionIdData)

	// Need to loop through all Mission Requests to see if this one exists
	INT					tempLoop					= 0
	MP_MISSION_ID_DATA	thisMissionIdData
	BOOL				passedMissionIsCloudLoaded	= Does_MP_Mission_Variation_Get_Data_From_The_Cloud(paramMissionIdData.idMission, paramMissionIdData.idVariation)
	
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		IF NOT (Is_MP_Mission_Request_Slot_Empty(temploop))
			thisMissionIdData = Get_MP_Mission_Request_MissionID_Data(tempLoop)
			IF (thisMissionIdData.idMission = paramMissionIdData.idMission)
				// ...same missionID
				IF (passedMissionIsCloudLoaded = Does_MP_Mission_Variation_Get_Data_From_The_Cloud(thisMissionIdData.idMission, thisMissionIdData.idVariation))
					// ...both cloud loaded (or both not cloud loaded)
					IF (passedMissionIsCloudLoaded)
						// ...both cloud loaded, so compare filenames
						IF (ARE_STRINGS_EQUAL(thisMissionIdData.idCloudFilename, paramMissionIdData.idCloudFilename))
							// ...filenames are the same, so treat as same mission so check if joinable and end the search
							RETURN (Is_This_Mission_Request_Mission_Still_Joinable(Get_MP_Mission_Request_UniqueID(tempLoop)))
						ENDIF
					ELSE
						// ...both hardcoded, so compare variations
						IF (thisMissionIdData.idVariation = paramMissionIdData.idVariation)
							// ...variations are the same, so treat as same mission so check if joinable and end the search
							RETURN (Is_This_Mission_Request_Mission_Still_Joinable(Get_MP_Mission_Request_UniqueID(tempLoop)))
						ENDIF
					ENDIF
				ENDIF				
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find mission
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a mission matching these details is reserved and joinable
//
// RETURN PARAMS:		paramMissionIdData			The MissionID Data
// RETURN VALUE:		BOOL						TRUE if a mission with these details is reserved and joinable
//
// NOTES:	The corona matchmaking must also be Open for Walk-Ins
FUNC BOOL Is_This_Mission_Request_Reserved_And_Joinable(MP_MISSION_ID_DATA &paramMissionIdData)

	// Need to loop through all Mission Requests to see if this one exists
	INT					tempLoop					= 0
	MP_MISSION_ID_DATA	thisMissionIdData
	BOOL				passedMissionIsCloudLoaded	= Does_MP_Mission_Variation_Get_Data_From_The_Cloud(paramMissionIdData.idMission, paramMissionIdData.idVariation)
	
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		IF NOT (Is_MP_Mission_Request_Slot_Empty(temploop))
			IF (Is_MP_Mission_Request_Slot_Status_Reserved(tempLoop))
				IF NOT (Is_MP_Mission_Request_Mission_Closed_To_Walk_Ins(tempLoop))
					thisMissionIdData = Get_MP_Mission_Request_MissionID_Data(tempLoop)
					IF (thisMissionIdData.idMission = paramMissionIdData.idMission)
						// ...same missionID
						IF (passedMissionIsCloudLoaded = Does_MP_Mission_Variation_Get_Data_From_The_Cloud(thisMissionIdData.idMission, thisMissionIdData.idVariation))
							// ...both cloud loaded (or both not cloud loaded)
							IF (passedMissionIsCloudLoaded)
								// ...both cloud loaded, so compare filenames
								IF (ARE_STRINGS_EQUAL(thisMissionIdData.idCloudFilename, paramMissionIdData.idCloudFilename))
									// ...filenames are the same, so treat as same mission so check if joinable and end the search
									RETURN (Is_This_Mission_Request_Mission_Still_Joinable(Get_MP_Mission_Request_UniqueID(tempLoop)))
								ENDIF
							ELSE
								// ...both hardcoded, so compare variations
								IF (thisMissionIdData.idVariation = paramMissionIdData.idVariation)
									// ...variations are the same, so treat as same mission so check if joinable and end the search
									RETURN (Is_This_Mission_Request_Mission_Still_Joinable(Get_MP_Mission_Request_UniqueID(tempLoop)))
								ENDIF
							ENDIF
						ENDIF				
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find mission
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Host of a mission matching these details is being reserved and joinable
//
// RETURN PARAMS:		paramMissionIdData			The MissionID Data
// RETURN VALUE:		PLAYER_INDEX				The player index of the host of the mission request (or INVALID_PLAYER_INDEX)
FUNC PLAYER_INDEX Get_Host_Of_Matching_Reserved_And_Joinable_Mission(MP_MISSION_ID_DATA &paramMissionIdData)

	// Need to loop through all Mission Requests to see if this one exists
	INT					tempLoop					= 0
	MP_MISSION_ID_DATA	thisMissionIdData
	BOOL				passedMissionIsCloudLoaded	= Does_MP_Mission_Variation_Get_Data_From_The_Cloud(paramMissionIdData.idMission, paramMissionIdData.idVariation)
	PLAYER_INDEX		theMissionRequestHost		= INVALID_PLAYER_INDEX()
	
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		IF NOT (Is_MP_Mission_Request_Slot_Empty(temploop))
			IF (Is_MP_Mission_Request_Slot_Status_Reserved(tempLoop))
				thisMissionIdData = Get_MP_Mission_Request_MissionID_Data(tempLoop)
				IF (thisMissionIdData.idMission = paramMissionIdData.idMission)
					// ...same missionID
					IF (passedMissionIsCloudLoaded = Does_MP_Mission_Variation_Get_Data_From_The_Cloud(thisMissionIdData.idMission, thisMissionIdData.idVariation))
						// ...both cloud loaded (or both not cloud loaded)
						IF (passedMissionIsCloudLoaded)
							// ...both cloud loaded, so compare filenames
							IF (ARE_STRINGS_EQUAL(thisMissionIdData.idCloudFilename, paramMissionIdData.idCloudFilename))
								// ...filenames are the same, so treat as same mission so check if joinable, end the search, and retrieve the requsting player
								IF (Is_This_Mission_Request_Mission_Still_Joinable(Get_MP_Mission_Request_UniqueID(tempLoop)))
								AND NOT (Is_MP_Mission_Request_Mission_Closed_To_Walk_Ins(tempLoop))
									// ...get the player that requested the mission (but if the request is from a script host then there is no host name to display but the mission can still be joined)
									IF (GlobalServerBD_MissionRequest.missionRequests[tempLoop].mrRequesterIsHost)
										// ...the requesting player was a script host and so may not be personally on the mission, so just return 'no host'
										RETURN (INVALID_PLAYER_INDEX())
									ENDIF
									
									// If the requesting player is still ok then return the player index, otherwise return no host
									theMissionRequestHost = GlobalServerBD_MissionRequest.missionRequests[tempLoop].mrRequester
									
									IF (IS_NET_PLAYER_OK(theMissionRequestHost, FALSE))
										// ...host is still ok
										RETURN (theMissionRequestHost)
									ENDIF
									
									// Host is no longer ok
									RETURN (INVALID_PLAYER_INDEX())
								ENDIF
							ENDIF
						ELSE
							// ...both hardcoded, so compare variations
							IF (thisMissionIdData.idVariation = paramMissionIdData.idVariation)
								// ...variations are the same, so treat as same mission so check if joinable and end the search
								IF (Is_This_Mission_Request_Mission_Still_Joinable(Get_MP_Mission_Request_UniqueID(tempLoop)))
								AND NOT (Is_MP_Mission_Request_Mission_Closed_To_Walk_Ins(tempLoop))
									// ...get the player that requested the mission (but if the request is from a script host then there is no host name to display but the mission can still be joined)
									IF (GlobalServerBD_MissionRequest.missionRequests[tempLoop].mrRequesterIsHost)
										// ...the requesting player was a script host and so may not be personally on the mission, so just return 'no host'
										RETURN (INVALID_PLAYER_INDEX())
									ENDIF
									
									// If the requesting player is still ok then return the player index, otherwise return no host
									theMissionRequestHost = GlobalServerBD_MissionRequest.missionRequests[tempLoop].mrRequester
									
									IF (IS_NET_PLAYER_OK(theMissionRequestHost, FALSE))
										// ...host is still ok
										RETURN (theMissionRequestHost)
									ENDIF
									
									// Host is no longer ok
									RETURN (INVALID_PLAYER_INDEX())
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find mission
	RETURN (INVALID_PLAYER_INDEX())

ENDFUNC





// -----------------------------------------------------------------------------------------------------------
//      Client Resync after Host Migration Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clears all the Triggerer Resync local globals
PROC MP_Mission_Control_Client_Clear_Triggerer_Resync_Data()

	g_sMCResync.mccrTriggeringTimeoutInitialised = FALSE
	g_sMCResync.mccrActivityOnClient	= MCCRA_NO_ACTIVITY
	g_sMCResync.mccrUniqueIDOnClient	= NO_UNIQUE_ID
	g_sMCResync.mccrStateOnServer		= MCCRHS_NO_ACTIVE_STATE
	g_sMCResync.mccrUniqueIDOnServer	= NO_UNIQUE_ID

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clears all the External Resync local globals
PROC MP_Mission_Control_Client_Clear_External_Resync_Data()

	g_sMCResync.mccrExternalTimeoutInitialised		= FALSE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clears all the Resync local globals
PROC MP_Mission_Control_Client_Clear_Resync_Data()

	MP_Mission_Control_Client_Clear_Triggerer_Resync_Data()
	MP_Mission_Control_Client_Clear_External_Resync_Data()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the player is allocated to this mission request slot
//
// INPUT PARAM:			paramArrayPos		Mission Request Array Position
// RETURN VALUE:		BOOL				TRUE if the player is allocated to this mission request slot in some way
//
// NOTES:	EXPENSIVE: USED FOR HOST MIGRATION RESYNC - Needs to get the details held about this player by the server by doing a number of bit searches
//			Assumes the slot is not empty
FUNC BOOL Is_This_Player_Allocated_To_This_MP_Mission_Request_Slot_Expensive_Function(INT paramArrayPos)
	
	IF (Is_Player_Active_On_MP_Mission_Request_Slot(paramArrayPos, PLAYER_ID()))
	OR (Is_Player_Confirmed_For_MP_Mission_Request_Slot(paramArrayPos, PLAYER_ID()))
	OR (Is_Player_Joining_MP_Mission_Request_Slot(paramArrayPos, PLAYER_ID()))
	OR (Is_Player_Reserved_For_MP_Mission_Request_Slot(paramArrayPos, PLAYER_ID()))
		RETURN TRUE
	ENDIF
	
	// Failed to find player allocated to this slot
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks all mission request array positions to find the array position this player is allocated to
//
// RETURN VALUE:		INT			The Mission Request array position
//
// NOTES:	EXPENSIVE: USED FOR HOST MIGRATION RESYNC - Needs to get the details held about this player by the server by doing a number of bit searches in all missions
FUNC INT Get_Array_Position_For_This_Player_Using_Expensive_Function()
	
	INT tempLoop = 0
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		IF NOT (Is_MP_Mission_Request_Slot_Empty(temploop))
			// ...slot is not empty, so check if this player is allocated to the mission in this slot
			IF (Is_This_Player_Allocated_To_This_MP_Mission_Request_Slot_Expensive_Function(tempLoop))
				RETURN tempLoop
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find player allocated to any mission
	RETURN ILLEGAL_ARRAY_POSITION

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get an Activity ID to represent the current activity for this Client
//
// RETURN VALUE:		g_eMCCResyncActivity			Current Client Activity
FUNC g_eMCCResyncActivity Get_MP_Mission_Control_Client_Activity_For_Resync()

	// Get the current activity from the Mission Triggering state
	SWITCH (g_sTriggerMP.mtState)
		// Current Activity: Pre-Mission
		CASE MP_TRIGGER_START_PREMISSION_COMMS					// Preparing to play pre-mission phonecall
		CASE MP_TRIGGER_WAITING_FOR_PREMISSION_COMMS_TO_END		// Pre-Mission phonecall is active
		CASE MP_TRIGGER_LAUNCH_CUTSCENE							// Preparing to Launch Cutscene
		CASE MP_TRIGGER_LAUNCHING_CUTSCENE						// Waiting for Cutscene script to launch
		CASE MP_TRIGGER_CUTSCENE_ACTIVE							// Cutscene is playing
		CASE MP_TRIGGER_CHECK_STILL_AVAILABLE					// Ready to perform the 'check if still ok' broadcast
			RETURN MCCRA_PREMISSION
			
		// Current Activity: Waiting for 'still ok' confirmation from server
		CASE MP_TRIGGER_AWAITING_CONFIRMATION					// Waiting for server response: OK to join
			RETURN MCCRA_AWAITING_SERVER_OK
			
		// Current Activity: launching Mission
		CASE MP_TRIGGER_LAUNCH_MISSION							// Start Mission launch
		CASE MP_TRIGGER_LAUNCHING_MISSION						// Waiting for Mission to Launch
			RETURN MCCRA_LAUNCHING_MISSION
			
		// Current Activity: Mission Active
		CASE MP_TRIGGER_MISSION_ACTIVE							// Mission is running
			RETURN MCCRA_MISSION_ACTIVE
	
		// Current Activity: NO_ACTIVITY
		CASE NO_CURRENT_MP_TRIGGER_REQUEST						// No mission triggering activity
			RETURN MCCRA_NO_ACTIVITY
	ENDSWITCH
	
	// Failed to find the activity
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Get_MP_Mission_Control_Client_Activity_For_Resync(): Unknown Mission Triggering state. Add to SWITCH statement.") NET_NL()
		SCRIPT_ASSERT("Get_MP_Mission_Control_Client_Activity_For_Resync(): Unknown Mission Triggering State. Add to SWITCH statement. Tell Keith.")
	#ENDIF
	
	RETURN MCCRA_NO_ACTIVITY
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the server's current representation of this player's state and uniqueID
//
// RETURN PARAM:		paramServerUniqueID			The uniqueID of the mission the server thinks the player is performing
// RETURN VALUE:		g_eMCCResyncHostState		The player state that the server thinks the player is in
//
// NOTES:	Calls an EXPENSIVE FUNCTION
FUNC g_eMCCResyncHostState Get_MP_Mission_Control_Server_State_and_UniqueID_For_Resync(INT &paramServerUniqueID)

	INT missionRequestArrayPos = Get_Array_Position_For_This_Player_Using_Expensive_Function()
	
	// If the player is not on a mission then return no data
	IF (missionRequestArrayPos = ILLEGAL_ARRAY_POSITION)
		paramServerUniqueID = NO_UNIQUE_ID
		RETURN MCCRHS_NO_ACTIVE_STATE
	ENDIF
	
	// Store the return data
	// ...uniqueID
	paramServerUniqueID = Get_MP_Mission_Request_UniqueID(missionRequestArrayPos)
	
	// Player State (based on the actual mission request bit data rather than the player state ID data)
	IF (Is_Player_Active_On_MP_Mission_Request_Slot(missionRequestArrayPos, PLAYER_ID()))
		RETURN MCCRHS_ACTIVE
	ENDIF
	
	IF (Is_Player_Joining_MP_Mission_Request_Slot(missionRequestArrayPos, PLAYER_ID()))
		RETURN MCCRHS_JOINING
	ENDIF
	
	IF (Is_Player_Confirmed_For_MP_Mission_Request_Slot(missionRequestArrayPos, PLAYER_ID()))
		RETURN MCCRHS_CONFIRMED
	ENDIF
	
	// Not on mission, or Reserved
	RETURN MCCRHS_NO_ACTIVE_STATE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Calculate the next resync timeout time and return it
//
// RETURN VALUE:		INT				Next Resync Timeout value
FUNC TIME_DATATYPE Get_New_MP_Mission_Control_Client_Resync_Timeout()
	TIME_DATATYPE nextTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), MCRESYNC_TIMER_DELAY_msec)
	RETURN nextTimeout
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Calculate the next quick re-check resync timeout time and return it
//
// RETURN VALUE:		INT				Next Resync Timeout value
FUNC TIME_DATATYPE Get_Quick_Recheck_MP_Mission_Control_Client_Resync_Timeout()
	TIME_DATATYPE nextTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), MCRESYNC_TIMER_QUICK_RECHECK_DELAY_msec)
	RETURN nextTimeout
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Broadcast an event to remove a player from all missions requests he is allocated to
// NOTES:	In theory, player should only be allocated to one mission
//			Send individual events to remove the player from each mission request he is allocated to - this avoids him being removed from a new mission request by accident
PROC Resync_Force_Remove_Player_From_All_Mission_Requests()

	INT tempLoop = 0
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		IF NOT (Is_MP_Mission_Request_Slot_Empty(tempLoop))
			IF (Is_This_Player_Allocated_To_This_MP_Mission_Request_Slot_Expensive_Function(tempLoop))
				Broadcast_Force_Player_From_Mission_Request(Get_MP_Mission_Request_UniqueID(tempLoop))
			ENDIF
		ENDIF
	ENDREPEAT
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Do a resync check - the mission triggerer has stabilised with the player doing pre-mission activities
//
// RETURN VALUE:		BOOL			TRUE if the Sync checking should end, FALSE if another re-sync is required
FUNC BOOL Client_Resync_Check_Triggerer_PreMission()

	#IF IS_DEBUG_BUILD
		NET_PRINT("          Mission Triggerer has stabilised doing PreMission activity after Host Migration") NET_NL()
	#ENDIF
	
	// Check if the Controller and Triggerer are in sync
	// NOTE: They are in sync if the server thinks the player is joining the same mission
	IF (g_sMCResync.mccrUniqueIDOnClient = g_sMCResync.mccrUniqueIDOnServer)
		// ...the controller and triggerer agree that the player is on the same mission request
		// If the controller thinks the player is joining the mission then the data is in sync
		IF (g_sMCResync.mccrStateOnServer = MCCRHS_JOINING)
			// ...in sync, so cancel resync checking
			#IF IS_DEBUG_BUILD
				NET_PRINT("          IN SYNC: Controller also thinks the player is joining the mission") NET_NL()
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	// The mission triggerer and the mission controller don't agree, so set up another resync check
	// (Pre-Mission Launch on Triggerer should always advance to a new state when cutscene or phonecall finishes)
	g_sMCResync.mccrTriggeringTimeout = Get_Quick_Recheck_MP_Mission_Control_Client_Resync_Timeout()
	g_sMCResync.mccrTriggeringTimeoutInitialised = TRUE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("          NOT IN SYNC: but the triggerer should always move on from the pre-mission state, so just do another resync check in ")
		NET_PRINT_INT(MCRESYNC_TIMER_QUICK_RECHECK_DELAY_msec)
		NET_PRINT("msec)")
		NET_NL()
	#ENDIF
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Do a resync check - the mission triggerer has stabilised with the player checking if it is ok to join the mission
//
// RETURN VALUE:		BOOL			TRUE if the Sync checking should end, FALSE if another re-sync is required
FUNC BOOL Client_Resync_Check_Triggerer_Awaiting_Server_OK()

	#IF IS_DEBUG_BUILD
		NET_PRINT("          Mission Triggerer has stabilised waiting for confirmation of OK to join after Host Migration") NET_NL()
	#ENDIF
	
	// Check if the Controller and Triggerer are in sync
	// NOTE: If the mission triggerer is stuck in this state then the Controller and Triggerer are very likely out of sync and some corrective action will be needed
	IF (g_sMCResync.mccrUniqueIDOnClient = g_sMCResync.mccrUniqueIDOnServer)
		// ...the controller and triggerer agree that the player is on the same mission request
		// If the controller thinks the player is joining the mission then the 'check ok to join' broadcast to the server has been lost, so need to re-send it
		IF (g_sMCResync.mccrStateOnServer = MCCRHS_JOINING)
			// ...server didn't receive the 'check ok to join' broadcast, so re-send it
			#IF IS_DEBUG_BUILD
				NET_PRINT("          NOT IN SYNC: Controller hasn't received the 'Check If Ok To Join' broadcast from triggerer - resending and setting up another resync check in ")
				NET_PRINT_INT(MCRESYNC_TIMER_DELAY_msec)
				NET_PRINT("msec)")
				NET_NL()
			#ENDIF
			
			Broadcast_Check_If_Still_Ok_To_Join_Mission(g_sTriggerMP.mtMissionData)
			g_sMCResync.mccrTriggeringTimeout = Get_New_MP_Mission_Control_Client_Resync_Timeout()
			g_sMCResync.mccrTriggeringTimeoutInitialised = TRUE
			
			// Corrective action was taken, so request logs to check the resync worked ok
			#IF IS_DEBUG_BUILD
				g_debugRequestTriggerResyncLogs = TRUE
			#ENDIF
			
			RETURN FALSE
		ENDIF

		// If the controller thinks the player is confirmed for the mission then the 'confirmed ok' broadcast to the client has been lost, so need to re-send it
		IF (g_sMCResync.mccrStateOnServer = MCCRHS_CONFIRMED)
			// ...server received the 'check ok to join' broadcast and must have broadcast an 'ok' reply which has been lost, so set the 'ok to join' flag directly for this triggerer
			#IF IS_DEBUG_BUILD
				NET_PRINT("          NOT IN SYNC: Triggerer hasn't received the 'Confirmed OK' broadcast from controller - directly setting the 'received ok to join' flag on the Triggerer") NET_NL()
				NET_PRINT("                       setting up another resync check in ")
				NET_PRINT_INT(MCRESYNC_TIMER_DELAY_msec)
				NET_PRINT("msec)")
				NET_NL()
			#ENDIF
			
			SET_BIT(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_RECEIVED_OK_TO_JOIN)
			g_sMCResync.mccrTriggeringTimeout = Get_New_MP_Mission_Control_Client_Resync_Timeout()
			g_sMCResync.mccrTriggeringTimeoutInitialised = TRUE
			
			// Corrective action was taken, so request logs to check the resync worked ok
			#IF IS_DEBUG_BUILD
				g_debugRequestTriggerResyncLogs = TRUE
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF

	// The mission triggerer and the mission controller don't agree
	#IF IS_DEBUG_BUILD
		NET_PRINT("          NOT IN SYNC: broadcasting force cleanup to both server and client, and do another resync check in ")
		NET_PRINT_INT(MCRESYNC_TIMER_DELAY_msec)
		NET_PRINT("msec)")
		NET_NL()
	#ENDIF
	
	// Force the player to be removed from all mission requests on the controller (this should be just one request)
	// Force the mission triggerer to end this mission trigger
	
	Resync_Force_Remove_Player_From_All_Mission_Requests()
	Broadcast_Force_Player_Quit_PreMission(g_sMCResync.mccrUniqueIDOnClient)
	
	g_sMCResync.mccrTriggeringTimeout = Get_New_MP_Mission_Control_Client_Resync_Timeout()
	g_sMCResync.mccrTriggeringTimeoutInitialised = TRUE
			
	// Corrective action was taken, so request logs to check the resync worked ok
	#IF IS_DEBUG_BUILD
		g_debugRequestTriggerResyncLogs = TRUE
	#ENDIF
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Do a resync check - the mission triggerer has stabilised with the player doing pre-mission activities
//
// RETURN VALUE:		BOOL			TRUE if the Sync checking should end, FALSE if another re-sync is required
FUNC BOOL Client_Resync_Check_Triggerer_Launching_Mission()

	#IF IS_DEBUG_BUILD
		NET_PRINT("          Mission Triggerer has stabilised doing Launch Mission after Host Migration") NET_NL()
	#ENDIF
	
	// A launching mission should always complete (unless there is an unrelated issue somewhere), so just set up another resync check to give it time to sort itself out
	g_sMCResync.mccrTriggeringTimeout = Get_Quick_Recheck_MP_Mission_Control_Client_Resync_Timeout()
	g_sMCResync.mccrTriggeringTimeoutInitialised = TRUE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("          NOT IN SYNC: but a mission should always launch unless there is another unrelated problem, so just do another resync check in ")
		NET_PRINT_INT(MCRESYNC_TIMER_QUICK_RECHECK_DELAY_msec)
		NET_PRINT("msec)")
		NET_NL()
	#ENDIF
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Do a resync check - the mission triggerer has stabilised with the player playing the mission
//
// RETURN VALUE:		BOOL			TRUE if the Sync checking should end, FALSE if another re-sync is required
FUNC BOOL Client_Resync_Check_Triggerer_Mission_Active()

	#IF IS_DEBUG_BUILD
		NET_PRINT("          Mission Triggerer has stabilised with the player playing the mission after Host Migration") NET_NL()
	#ENDIF
	
	// Check if the Controller and Triggerer are in sync
	IF (g_sMCResync.mccrUniqueIDOnClient = g_sMCResync.mccrUniqueIDOnServer)
		// ...the controller and triggerer agree that the player is on the same mission request
		// If the controller thinks the player is active on the mission then both controller and triggerer are in sync
		IF (g_sMCResync.mccrStateOnServer = MCCRHS_ACTIVE)
			// ...in sync, so cancel resync checking
			#IF IS_DEBUG_BUILD
				NET_PRINT("          IN SYNC: Controller also thinks the player is active on the mission") NET_NL()
			#ENDIF
			
			RETURN TRUE
		ENDIF

		// If the controller thinks the player is joining or confirmed for the mission then the 'on mission' broadcast to the client has been lost, so need to re-send it
		IF (g_sMCResync.mccrStateOnServer = MCCRHS_JOINING)
		OR (g_sMCResync.mccrStateOnServer = MCCRHS_CONFIRMED)
			// ...not in sync, the controller thinks the player is still joining or confirmed for the mission, so resend the 'player joined mission' event to the controller
			#IF IS_DEBUG_BUILD
				NET_PRINT("          NOT IN SYNC: Controller hasn't received the 'Player Joined Mission' broadcast from triggerer - so re-sending and starting a new resync check in ")
				NET_PRINT_INT(MCRESYNC_TIMER_DELAY_msec)
				NET_PRINT("msec)")
				NET_NL()
			#ENDIF
			
			Broadcast_Reply_Player_Joined_Mission(g_sTriggerMP.mtMissionData)
			g_sMCResync.mccrTriggeringTimeout = Get_New_MP_Mission_Control_Client_Resync_Timeout()
			g_sMCResync.mccrTriggeringTimeoutInitialised = TRUE
			
			// Corrective action was taken, so request logs to check the resync worked ok
			#IF IS_DEBUG_BUILD
				g_debugRequestTriggerResyncLogs = TRUE
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF

	// The mission triggerer and the mission controller don't agree
	// If the triggering is active but the server is 'not active' then the player is locally on the mission without the server's knowledge
	//		- just let this play out, when the mission is over the player should get back in sync with the server again
	IF (g_sMCResync.mccrStateOnServer = MCCRHS_NO_ACTIVE_STATE)
		// ...the server is not active so the mission troggering must be active (because it must have a uniqueID), so not in sync, but no corrective action needed.
		#IF IS_DEBUG_BUILD
			NET_PRINT("          NOT IN SYNC: the player is active on a mission that the server is not aware of.")
			NET_PRINT("             BUT DO NOTHING: Let the mission finish and this should automatically re-sync. No corrective action taken.")
			NET_NL()
		#ENDIF
		
		// No corrective action needed
		RETURN TRUE
	ENDIF
	
	// The server thinks the player is active but there are inconsistencies with the client state, so forcing cleanup on the server
	#IF IS_DEBUG_BUILD
		NET_PRINT("          NOT IN SYNC: broadcasting force cleanup to server, and do another resync check in ")
		NET_PRINT_INT(MCRESYNC_TIMER_DELAY_msec)
		NET_PRINT("msec)")
		NET_NL()
	#ENDIF
	
	// Force the player to be removed from all mission requests on the controller (this should be just one request)
	// Leave the Mission Triggerer alone to let the player finish the mission
	
	Resync_Force_Remove_Player_From_All_Mission_Requests()
	
	g_sMCResync.mccrTriggeringTimeout = Get_New_MP_Mission_Control_Client_Resync_Timeout()
	g_sMCResync.mccrTriggeringTimeoutInitialised = TRUE
	
	// Corrective action was taken, so request logs to check the resync worked ok
	#IF IS_DEBUG_BUILD
		g_debugRequestTriggerResyncLogs = TRUE
	#ENDIF
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Do a resync check - the mission triggerer has stabilised with the player not on a mission
//
// RETURN VALUE:		BOOL			TRUE if the Sync checking should end, FALSE if another re-sync is required
FUNC BOOL Client_Resync_Check_Triggerer_No_Activity()

	#IF IS_DEBUG_BUILD
		NET_PRINT("          Mission Triggerer has stabilised with the player not performing any mission after Host Migration") NET_NL()
	#ENDIF
	
	// Check if the Controller and Triggerer are in sync
	// If the controller thinks the player is not on an active mission then Controller and Triggerer are in sync
	// NOTE: The server may have the player 'reserved' for a mission, so the Controller may have a uniqueID while the Triggerer won't have one
	IF (g_sMCResync.mccrStateOnServer = MCCRHS_NO_ACTIVE_STATE)
		// ...in sync, so cancel resync checking
		#IF IS_DEBUG_BUILD
			NET_PRINT("          IN SYNC: Controller also thinks the player is not active on a mission") NET_NL()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// The mission triggerer and the mission controller don't agree, so corrective action is required
	#IF IS_DEBUG_BUILD
		NET_PRINT("          NOT IN SYNC: broadcasting force cleanup to server, and do another resync check in ")
		NET_PRINT_INT(MCRESYNC_TIMER_DELAY_msec)
		NET_PRINT("msec)")
		NET_NL()
	#ENDIF

	// Force the player to be removed from all mission requests on the controller (this should be just one request)
	// The Mission Triggerer doesn't have the player performing a mission, so nothing to do
	
	Resync_Force_Remove_Player_From_All_Mission_Requests()
	
	g_sMCResync.mccrTriggeringTimeout = Get_New_MP_Mission_Control_Client_Resync_Timeout()
	g_sMCResync.mccrTriggeringTimeoutInitialised = TRUE
			
	// Corrective action was taken, so request logs to check the resync worked ok
	#IF IS_DEBUG_BUILD
		g_debugRequestTriggerResyncLogs = TRUE
	#ENDIF
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Do the Triggerer resync check against the resync check data
// NOTES:	When the mission triggering state has remained stable since the last resync check, compare the server data to ensure it agrees with the triggering state.
PROC Perform_MP_Mission_Control_Client_Triggerer_Resync_Check()
			
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT("Performing Triggerer Resync Check (after recent Host migration). Host: ") Print_MC_Host_Name_To_Console_Log() NET_NL()
	#ENDIF
	
	// Clear the timeout variable to stop the resync process
	g_sMCResync.mccrTriggeringTimeoutInitialised = FALSE
	
	// Get the current client activity and uniqueID
	g_eMCCResyncActivity		thisActivity	= Get_MP_Mission_Control_Client_Activity_For_Resync()
	INT							thisUniqueID	= g_sTriggerMP.mtMissionData.mdUniqueID
	
	// Get the current server state and uniqueID for player
	INT							thisServerUID	= 0
	g_eMCCResyncHostState		thisStateID		= Get_MP_Mission_Control_Server_State_and_UniqueID_For_Resync(thisServerUID)
		
	#IF IS_DEBUG_BUILD
		// KGM 13/11/14 [BUG 2123584]: In Scott Butchard's console_1.log (and probably Jamie's and Thomas's logs too), at line 834745 there is output to show the mission request array
		//					and the player status array on the server are out of sync. They shouldn't be. The fix would be to clean up all player reserved missions on the server, but
		//					first I'm going to add an assert when this situation crops up to ensure it is ALWAYS wrong (I think it is) so that I don't actually cause a problem with my fix.
		BOOL requestArrayAndPlayerDataInconsistent = FALSE
		BOOL requestArrayThinksPlayerActive = FALSE
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		// Mission Triggerer Previous Activity details
		// (this is what the mission triggerer thought the player was up to on the previous resync check)
		NET_PRINT("      CURRENT PLAYER DETAILS HELD IN MISSION TRIGGERER AND ON MISSION CONTROLLER (FOR INFO)")
		NET_NL()
		NET_PRINT("          Previous Triggerer Activity and UniqueID: ")
		NET_PRINT("[Activity: ")
		NET_PRINT(Convert_Resync_Activity_To_String(g_sMCResync.mccrActivityOnClient))
		NET_PRINT("]  [uniqueID: ")
		NET_PRINT_INT(g_sMCResync.mccrUniqueIDOnClient)
		NET_PRINT("]")
		NET_NL()
		// Mission Triggerer Current Activity details
		// (this is what the mission triggerer thinks the player was up to now)
		NET_PRINT("          Current Triggerer Activity and UniqueID : ")
		NET_PRINT("[Activity: ")
		NET_PRINT(Convert_Resync_Activity_To_String(thisActivity))
		NET_PRINT("]  [uniqueID: ")
		NET_PRINT_INT(thisUniqueID)
		NET_PRINT("]")
		NET_NL()
		// Mission Controller Previous Player Details held on server
		// (this is the previous details taken directly from the player state bits from the first - and hopefully only - entry for this player on the mission request array)
		NET_PRINT("          Previous Controller State and UniqueID: ")
		NET_PRINT("[State: ")
		NET_PRINT(Convert_Resync_Host_State_To_String(g_sMCResync.mccrStateOnServer))
		NET_PRINT("]  [uniqueID: ")
		NET_PRINT_INT(g_sMCResync.mccrUniqueIDOnServer)
		NET_PRINT("]")
		NET_NL()
		// Mission Controller Current Player Details held on server
		// (this is the current details taken directly from the player state bits from the first - and hopefully only - entry for this player on the mission request array)
		NET_PRINT("          Current Controller State and UniqueID : ")
		NET_PRINT("[State: ")
		NET_PRINT(Convert_Resync_Host_State_To_String(thisStateID))
		NET_PRINT("]  [uniqueID: ")
		NET_PRINT_INT(thisServerUID)
		NET_PRINT("]")
		NET_NL()
		// Server BD Mission Controller Details held about this player
		// (This is state of all entries in the mission request array and the player's perceived status on each entry - the player should only appear in one entry)
		NET_PRINT("          Full Current Mission Controller Server Broadcast Details Held For This Player (this may be prior to stabilisation so there may be inconsistencies)") NET_NL()
		NET_PRINT("          PLAYER ACTIVITY ON MISSION REQUEST_ARRAY: ") NET_NL()
		
		INT tempLoop = 0
		REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
			NET_PRINT("             ") NET_PRINT_INT(tempLoop) NET_PRINT(")   ")
			IF (Is_MP_Mission_Request_Slot_Empty(tempLoop))
				NET_PRINT("empty")
			ELSE
				NET_PRINT(GET_MP_MISSION_NAME(Get_MP_Mission_Request_MissionID(tempLoop)))
				IF NOT (Is_This_Player_Allocated_To_This_MP_Mission_Request_Slot_Expensive_Function(tempLoop))
					NET_PRINT("   - player not on or triggering this mission")
				ELSE
					// KGM 13/11/14 [BUG 2123584]: Tracking to check for inconsistencies as highlighted in this bug
					requestArrayThinksPlayerActive = TRUE
					
					IF (Is_Player_Active_On_MP_Mission_Request_Slot(tempLoop, PLAYER_ID()))
						NET_PRINT("   [PLAYER ACTIVE]")
					ENDIF
					IF (Is_Player_Confirmed_For_MP_Mission_Request_Slot(tempLoop, PLAYER_ID()))
						NET_PRINT("   [PLAYER CONFIRMED]")
					ENDIF
					IF (Is_Player_Joining_MP_Mission_Request_Slot(tempLoop, PLAYER_ID()))
						NET_PRINT("   [PLAYER JOINING]")
					ENDIF
					IF (Is_Player_Reserved_For_MP_Mission_Request_Slot(tempLoop, PLAYER_ID()))
						NET_PRINT("   [PLAYER RESERVED]")
					ENDIF
					IF (Is_Player_Reserved_By_Player_For_MP_Mission_Request_Slot(tempLoop, PLAYER_ID()))
						NET_PRINT(" (BY PLAYER)")
					ENDIF
					IF (Is_Player_Reserved_By_Leader_For_MP_Mission_Request_Slot(tempLoop, PLAYER_ID()))
						NET_PRINT(" (BY LEADER)")
					ENDIF
					IF (Is_Player_Reserved_By_Game_For_MP_Mission_Request_Slot(tempLoop, PLAYER_ID()))
						NET_PRINT(" (BY GAME)")
					ENDIF
				ENDIF
			ENDIF
			NET_NL()
		ENDREPEAT
		// Server BD Mission Controller Details - stored player details
		// (this is the state of the player as recorded in the server's player details array - this should match the player's state in the mission request array)
		INT playerAsInt = NATIVE_TO_INT(PLAYER_ID())
		NET_PRINT("          PLAYER ACTIVITY AS STORED ON PLAYER DETAILS ARRAY (these should be consistent with the array details above): ") NET_NL()
		IF (GlobalServerBD_BlockB.missionPlayerData[playerAsInt].mcpSlot = MPMCP_NO_MISSION_SLOT)
			NET_PRINT("             - player not considered to be on any slot")
			
			// KGM 13/11/14 [BUG 2123584]: Ensure this is consistent with the array status above (specific consistency check for bug, not a general consistency check)
			IF (requestArrayThinksPlayerActive)
				// ...data is inconsistent
				requestArrayAndPlayerDataInconsistent = TRUE
			ENDIF
		ELSE
			NET_PRINT("             - Slot: ") NET_PRINT_INT(GlobalServerBD_BlockB.missionPlayerData[playerAsInt].mcpSlot)
			NET_PRINT("   State: ") NET_PRINT(Convert_Player_Status_To_String(GlobalServerBD_BlockB.missionPlayerData[playerAsInt].mcpState))
		ENDIF
		NET_NL()
	#ENDIF
	
	// If the details haven't stabilised then store the new details and wait for another resync check period
	IF NOT (thisActivity	= g_sMCResync.mccrActivityOnClient)
	OR NOT (thisUniqueID	= g_sMCResync.mccrUniqueIDOnClient)
	OR NOT (thisStateID		= g_sMCResync.mccrStateOnServer)
	OR NOT (thisServerUID	= g_sMCResync.mccrUniqueIDOnServer)
		// ...the details haven't stabilised, so store these new details and restart the timer for another check
		g_sMCResync.mccrActivityOnClient	= thisActivity
		g_sMCResync.mccrUniqueIDOnClient	= thisUniqueID
		g_sMCResync.mccrStateOnServer		= thisStateID
		g_sMCResync.mccrUniqueIDOnServer	= thisServerUID
		g_sMCResync.mccrTriggeringTimeout	= Get_Quick_Recheck_MP_Mission_Control_Client_Resync_Timeout()
		g_sMCResync.mccrTriggeringTimeoutInitialised = TRUE
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("          Mission Triggering or Mission Controller Details haven't stabilised. Setting up another Client/Server Resync Check in ")
			NET_PRINT_INT(MCRESYNC_TIMER_QUICK_RECHECK_DELAY_msec)
			NET_PRINT("msec.")
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF

	// The details have stabilised
	#IF IS_DEBUG_BUILD
		NET_PRINT("          Mission Triggering and Mission Controller Details have stabilised. Checking Consistency Between Controller and Triggerer.") NET_NL()
	#ENDIF
	
	// Update the stored details, in case they are still needed
	g_sMCResync.mccrActivityOnClient	= thisActivity
	g_sMCResync.mccrUniqueIDOnClient	= thisUniqueID
	g_sMCResync.mccrStateOnServer		= thisStateID
	g_sMCResync.mccrUniqueIDOnServer	= thisServerUID
	g_sMCResync.mccrTriggeringTimeoutInitialised = FALSE
	
	// Check if the client Mission Triggering state agrees with the Mission Controller state for this player
	SWITCH (thisActivity)
		CASE MCCRA_PREMISSION
			IF NOT (Client_Resync_Check_Triggerer_PreMission())
				EXIT
			ENDIF
			BREAK
			
		CASE MCCRA_AWAITING_SERVER_OK
			IF NOT (Client_Resync_Check_Triggerer_Awaiting_Server_OK())
				EXIT
			ENDIF
			BREAK
			
		CASE MCCRA_LAUNCHING_MISSION
			IF NOT (Client_Resync_Check_Triggerer_Launching_Mission())
				EXIT
			ENDIF
			BREAK
			
		CASE MCCRA_MISSION_ACTIVE
			IF NOT (Client_Resync_Check_Triggerer_Mission_Active())
				EXIT
			ENDIF
			BREAK
		
		CASE MCCRA_NO_ACTIVITY
			IF NOT (Client_Resync_Check_Triggerer_No_Activity())
				EXIT
			ENDIF
			BREAK
			
		DEFAULT
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MControl]: ERROR: Perform_MP_Mission_Control_Client_Triggerer_Resync_Check(): Unknown Activity ID. Add to SWITCH statement.") NET_NL()
				SCRIPT_ASSERT("Perform_MP_Mission_Control_Client_Triggerer_Resync_Check(): Unknown Activity ID. Add to SWITCH statement. Cancelling this Resync. Tell Keith.")
			#ENDIF
			BREAK
	ENDSWITCH

	// Cancel the Triggerer resync
	MP_Mission_Control_Client_Clear_Triggerer_Resync_Data()	
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("          Controller and Triggerer should be in sync after recent Host Migration") NET_NL()
	#ENDIF

	// If necessary, request console logs to check corrective action
	#IF IS_DEBUG_BUILD
		IF (g_debugRequestTriggerResyncLogs)
// KGM 9/12/14: Haven't seen any issues for a long time, so switching off the assert
//			SCRIPT_ASSERT("MISSION CONTROL HOST MIGRATION TRIGGERING SYSTEMS: Please add a bug with full logs and F9 screenshot for Keith. (This is not a bug - I need to check the logs to ensure that a rare condition has been handled properly)")
			PRINTLN("MISSION CONTROL HOST MIGRATION TRIGGERING SYSTEMS: Switched off assert, but it would have triggered here")
		ENDIF
	
		// ...clear the 'request logs' flag
		g_debugRequestTriggerResyncLogs = FALSE
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		// KGM 13/11/14 [BUG 2123584]: Assert if the mission request array thinks the player is active on a mission but the player data doesn't (the specific inconsistency logged in the bug)
		// I'm using this assert to find out if this inconsistency always highlights a problem that should be fixed (probably by calling Resync_Force_Remove_Player_From_All_Mission_Requests())
		//		or whether it could crop up without being an issue (unlikely) - and also how frequently the inconsistency crops up.
		IF (requestArrayAndPlayerDataInconsistent)
			PRINTLN("DATA INCONSISTENCY: Add a bug with local logs for Keith (host migration, for bug 2123584).")
			SCRIPT_ASSERT("DATA INCONSISTENCY: Add a bug with local logs for Keith (host migration, for bug 2123584).")
			requestArrayAndPlayerDataInconsistent = FALSE
		ENDIF
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a mission request originating from this player to the mission controller succeeded
//
// INPUT PARAMS:			paramResyncDataPos			The array position of this data on the resync array
// RETURN VALUE:			BOOL						TRUE if some form of corrective resync action was needed, FALSE if not.
//
// NOTES:	If this player has issued a request for a new mission and a mission request with the uniqueID doesn't exist, then send fail for this unique ID to the player, so the fail will be ignored.
//			This should be safe, because if the player was put on someone else's instance of the same mission then the player's unique ID will have changed.
//			If the broadcast was lost then sending FAIL should allow the originating system to move on.
FUNC BOOL Check_Resync_Of_External_Broadcast_Mission_Requested(INT paramResyncDataPos)

	#IF IS_DEBUG_BUILD
		NET_PRINT("          ")
		NET_PRINT_INT(paramResyncDataPos)
		NET_PRINT(") ")
		IF (paramResyncDataPos < 10)
			NET_PRINT(" ")
		ENDIF
		NET_PRINT("CHECK MISSION REQUEST EXISTS: ")
	#ENDIF

	// Get the unique ID sent with this broadcast
	INT uniqueID = g_sMCResyncComms[paramResyncDataPos].mcrbUniqueID
	
	// Check if there is an active mission request containing this uniqueID
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(uniqueID)
	IF NOT (missionSlot = FAILED_TO_FIND_MISSION)
		// ...mission exists, so nothing more to do
		#IF IS_DEBUG_BUILD
			NET_PRINT("[EXISTS - SO IN SYNC]")
			NET_NL()
		#ENDIF
		
		// No action taken, so don't trigger the assert that requests logs
		RETURN FALSE
	ENDIF
	
	// Mission doesn't exist, so send fail to this player to 
	#IF IS_DEBUG_BUILD
		NET_PRINT("[DOESN'T EXIST - SEND FAIL FOR UNIQUE ID: ") NET_PRINT_INT(uniqueID) NET_PRINT("]")
		NET_PRINT(" - CORRECTIVE ACTION")
		NET_NL()
	#ENDIF
	
	Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(uniqueID, PLAYER_ID())
	
	// Corrective action taken, so trigger the assert that requests logs
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a request from this player to join a mission succeeded
//
// INPUT PARAMS:			paramResyncDataPos			The array position of this data on the resync array
// RETURN VALUE:			BOOL						TRUE if some form of corrective resync action was needed, FALSE if not.
//
// NOTES:	If a mission request with this unique ID doesn't exist, then send fail for this unique ID to the player.
//			If a mission request does exist but the player isn't on it, then send fail for this uniqueID to the player.
//			This should be safe, because if the player was put on someone else's instance of the same mission then the player's unique ID will have changed, so the fail will be ignored.
//			If the broadcast was lost then sending FAIL should allow the originating system to move on.
FUNC BOOL Check_Resync_Of_External_Broadcast_Join_Request(INT paramResyncDataPos)

	#IF IS_DEBUG_BUILD
		NET_PRINT("          ")
		NET_PRINT_INT(paramResyncDataPos)
		NET_PRINT(") ")
		IF (paramResyncDataPos < 10)
			NET_PRINT(" ")
		ENDIF
		NET_PRINT("CHECK JOIN/RESERVE MISSION: ")
	#ENDIF

	// Get the unique ID sent with this broadcast
	INT uniqueID = g_sMCResyncComms[paramResyncDataPos].mcrbUniqueID
	
	// Check if there is an active mission request containing this uniqueID
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(uniqueID)
	IF NOT (missionSlot = FAILED_TO_FIND_MISSION)
		// ...mission exists
		#IF IS_DEBUG_BUILD
			NET_PRINT("[EXISTS]")
		#ENDIF
		
		// Check if the player is on the mission
		IF (Is_This_Player_Allocated_To_This_MP_Mission_Request_Slot_Expensive_Function(missionSlot))
			// ...player is allocated to the mission, so nothing more to do
			#IF IS_DEBUG_BUILD
				NET_PRINT("   [ON MISSION - SO IN SYNC]")
				NET_NL()
			#ENDIF
			
			// No action taken, so don't trigger the assert that requests logs
			RETURN FALSE
		ENDIF
		
		// Not allocated to the mission, so will need to send FAIL
		#IF IS_DEBUG_BUILD
			NET_PRINT("   [NOT ON MISSION - SEND FAIL FOR UNIQUE ID: ") NET_PRINT_INT(uniqueID) NET_PRINT("]")
			NET_PRINT(" - CORRECTIVE ACTION")
			NET_NL()
		#ENDIF
	
		// Send Fail to this player for this uniqueID
		Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(uniqueID, PLAYER_ID())
		
		// Corrective action taken, so trigger the assert that requests logs
		RETURN TRUE
	ENDIF
	
	// Mission doesn't exist, so need to send fail
	// NOTE: Even for Reservation_By_Leader, just need to send fail to one player - the leader. Other players wouldn't be on it unless the rquest had succeeeded.
	#IF IS_DEBUG_BUILD
		NET_PRINT("[DOESN'T EXIST - SEND FAIL FOR UNIQUE ID: ") NET_PRINT_INT(uniqueID) NET_PRINT("]")
		NET_PRINT(" - CORRECTIVE ACTION")
		NET_NL()
	#ENDIF
	
	// Send Fail to this player for this uniqueID
	Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(uniqueID, PLAYER_ID())
	
	// Corrective action taken, so trigger the assert that requests logs
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a request from this player to cancel a mission reservation succeeded
//
// INPUT PARAMS:			paramResyncDataPos			The array position of this data on the resync array
// RETURN VALUE:			BOOL						TRUE if some form of corrective resync action was needed, FALSE if not.
//
// NOTES:	If the mission doesn't exist then this broadcast can be ignored.
//			If the mission does exist and the player is on it, then need to re-send the cancel reservation broadcast.
//			This should be safe because even if the player has since tried to reserve the mission again that request will fail because the player will still be classed as reserved for a mission,
//				so it should all sort itself out over time.
FUNC BOOL Check_Resync_Of_External_Broadcast_Cancel_Join(INT paramResyncDataPos)

	#IF IS_DEBUG_BUILD
		NET_PRINT("          ")
		NET_PRINT_INT(paramResyncDataPos)
		NET_PRINT(") ")
		IF (paramResyncDataPos < 10)
			NET_PRINT(" ")
		ENDIF
		NET_PRINT("CHECK CANCEL RESERVATION: ")
	#ENDIF

	// Get the unique ID sent with this broadcast
	INT uniqueID = g_sMCResyncComms[paramResyncDataPos].mcrbUniqueID
	
	// Check if there is an active mission request containing this uniqueID
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(uniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// ...mission doesn't exist, so nothing to do 
		#IF IS_DEBUG_BUILD
			NET_PRINT("[DOESN'T EXIST - SO IN SYNC]")
			NET_NL()
		#ENDIF
		
		// No action taken, so don't trigger the assert that requests logs
		RETURN FALSE
	ENDIF
	
	// Mission exists, so check if the mission is still 'reserved'
	#IF IS_DEBUG_BUILD
		NET_PRINT("[EXISTS]")
	#ENDIF
	
	IF NOT (Is_MP_Mission_Request_Slot_Status_Reserved(missionSlot))
		// ...mission is no longer reserved, so it's too late to try to unreserve this player if the player is still on it
		IF (Get_UniqueID_For_This_Players_Mission(PLAYER_ID()) = uniqueID)
			#IF IS_DEBUG_BUILD
				NET_PRINT("[MISSION BEING ACTIVATED AND PLAYER IS STILL ON IT SO TOO LATE TO UNRESERVE THE PLAYER - LEAVE AS-IS - TREAT AS IN SYNC]")
				NET_NL()
			#ENDIF
			
			// No action taken, but still examine the logs to make sure everything works out ok
			RETURN TRUE
		ENDIF
		
		// Player is no longer on the mission, so nothing to do
		#IF IS_DEBUG_BUILD
			NET_PRINT("[MISSION BEING ACTIVATED AND PLAYER IS NOT ON IT - SO IN SYNC]")
			NET_NL()
		#ENDIF
		
		// No action taken, so don't trigger the assert that requests logs
		RETURN FALSE
	ENDIF
	
	// Mission is still in reserved state, so check if the player is required for the mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("   [MISSION RESERVED]")
	#ENDIF
	
	// NOTE: While a mission is 'reserved', players that enter the corona are classed as 'Reserved By Player' because they actively want to be on the mission
	IF NOT (Is_Player_Reserved_By_Player_For_MP_Mission_Request_Slot(missionSlot, PLAYER_ID()))
		// ...not Reserved By Player, so ignore this
		#IF IS_DEBUG_BUILD
			NET_PRINT("   [PLAYER NOT 'RESERVED BY PLAYER' - SO TREAT AS IN SYNC]")
			NET_NL()
		#ENDIF
		
		// No action taken, but still examine the logs to make sure everything works out ok
		RETURN TRUE
	ENDIF
	
	// Reserved_By_Player, so need to re-send the cancellation broadcast
	#IF IS_DEBUG_BUILD
		NET_PRINT("   [PLAYER STILL RESERVED-BY-PLAYER FOR MISSION - SO RE-SEND CANCELLATION BROADCAST FOR UNIQUE ID: ") NET_PRINT_INT(uniqueID) NET_PRINT("]")
		NET_PRINT(" - CORRECTIVE ACTION")
		NET_NL()
	#ENDIF
	
	// Resend broadcast to cancel the mission
	Broadcast_Cancel_Mission_Reservation_By_Player(uniqueID)
	
	// Corrective action taken, so trigger the assert that requests logs
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a mission reserved by leader is being cancelled
//
// INPUT PARAMS:			paramResyncDataPos			The array position of this data on the resync array
// RETURN VALUE:			BOOL						TRUE if some form of corrective resync action was needed, FALSE if not.
//
// NOTES:	If the mission doesn't exist then this broadcast can be ignored.
//			If the mission does exist, then need to re-send the cancel reservation broadcast.
//			This should be safe because even if the player has since tried to reserve the mission again that request will fail because the player will still be classed as reserved for a mission,
//				so it should all sort itself out over time.
FUNC BOOL Check_Resync_Of_External_Broadcast_Cancel_Leader_Reserve(INT paramResyncDataPos)

	#IF IS_DEBUG_BUILD
		NET_PRINT("          ")
		NET_PRINT_INT(paramResyncDataPos)
		NET_PRINT(") ")
		IF (paramResyncDataPos < 10)
			NET_PRINT(" ")
		ENDIF
		NET_PRINT("CHECK CANCEL RESERVED BY LEADER: ")
	#ENDIF

	// Get the unique ID sent with this broadcast
	INT uniqueID = g_sMCResyncComms[paramResyncDataPos].mcrbUniqueID
	
	// Check if there is an active mission request containing this uniqueID
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(uniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// ...mission doesn't exist, so nothing to do 
		#IF IS_DEBUG_BUILD
			NET_PRINT("[DOESN'T EXIST - SO IN SYNC]")
			NET_NL()
		#ENDIF
		
		// No action taken, so don't trigger the assert that requests logs
		RETURN FALSE
	ENDIF
	
	// Mission exists, so check if the mission is still 'reserved'
	#IF IS_DEBUG_BUILD
		NET_PRINT("[EXISTS]")
	#ENDIF
	
	IF NOT (Is_MP_Mission_Request_Slot_Status_Reserved(missionSlot))
		// ...mission is no longer reserved, so it's too late to try to unreserve this player 
		#IF IS_DEBUG_BUILD
			NET_PRINT("[MISSION BEING ACTIVATED - SO TREAT AS IN SYNC]")
			NET_NL()
		#ENDIF
		
		// No action taken, but still examine the logs to make sure everything works out ok
		RETURN TRUE
	ENDIF
	
	// Mission is still in reserved state, so re-send the cancellation event
	#IF IS_DEBUG_BUILD
		NET_PRINT("   [MISSION STILL RESERVED - SO RE-SEND CANCELLATION BROADCAST FOR UNIQUE ID: ") NET_PRINT_INT(uniqueID) NET_PRINT("]")
		NET_PRINT(" - CORRECTIVE ACTION")
		NET_NL()
	#ENDIF
	
	// Resend broadcast to cancel the mission
	Broadcast_Cancel_Mission_Reserved_By_Leader(uniqueID)
	
	// Corrective action taken, so trigger the assert that requests logs
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a request from this player to start a reserved mission succeeded
//
// INPUT PARAMS:			paramResyncDataPos			The array position of this data on the resync array
// RETURN VALUE:			BOOL						TRUE if some form of corrective resync action was needed, FALSE if not.
//
// NOTES:	If a mission request with this unique ID doesn't exist, then send fail for this unique ID to the player.
//			If a mission request does exist but the mission is already active, then ignore this broadcast.
//			If a mission request does exist but is still reserved, then resend the request.
//			Resending the request should be safe even if the player is no longer reserved for the mission because the request would just be ignored.
FUNC BOOL Check_Resync_Of_External_Broadcast_Start_Reserved_Mission(INT paramResyncDataPos)

	#IF IS_DEBUG_BUILD
		NET_PRINT("          ")
		NET_PRINT_INT(paramResyncDataPos)
		NET_PRINT(") ")
		IF (paramResyncDataPos < 10)
			NET_PRINT(" ")
		ENDIF
		NET_PRINT("CHECK START RESERVED MISSION: ")
	#ENDIF

	// Get the unique ID sent with this broadcast
	INT uniqueID = g_sMCResyncComms[paramResyncDataPos].mcrbUniqueID
	
	// Check if there is an active mission request containing this uniqueID
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(uniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// ...mission doesn't exist, so need to send fail 
		#IF IS_DEBUG_BUILD
			NET_PRINT("[DOESN'T EXIST - SEND FAIL FOR UNIQUE ID: ") NET_PRINT_INT(uniqueID) NET_PRINT("]")
			NET_PRINT(" - CORRECTIVE ACTION")
			NET_NL()
		#ENDIF
		
		// Send Fail to this player for this uniqueID
		Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(uniqueID, PLAYER_ID())
		
		// Corrective action taken, so trigger the assert that requests logs
		RETURN TRUE
	ENDIF
	
	// Mission exists, so check if it is already being activated
	#IF IS_DEBUG_BUILD
		NET_PRINT("[EXISTS]")
	#ENDIF
	
	IF NOT (Is_MP_Mission_Request_Slot_Status_Reserved(missionSlot))
		// ...mission is not reserved so must be being activated, so nothing more to do
		#IF IS_DEBUG_BUILD
			NET_PRINT("   [MISSION NOT RESERVED SO MUST BE GETTING ACTIVATED - SO IN SYNC]")
			NET_NL()
		#ENDIF
		
		// No action taken, so don't trigger the assert that requests logs
		RETURN FALSE
	ENDIF
	
	// Mission is reserved, so need to resend the broadcast
	#IF IS_DEBUG_BUILD
		NET_PRINT("   [MISSION RESERVED - RE-SEND START RESERVED FOR UNIQUE ID: ") NET_PRINT_INT(uniqueID) NET_PRINT("]")
		NET_PRINT(" - CORRECTIVE ACTION")
		NET_NL()
	#ENDIF

	// Re-send Start Reserved Mission for this uniqueID - need to send a bitfield representing the reserved players, just use this player in the bitfield
	INT bitsPlayers = ALL_MISSION_CONTROL_BITS_CLEAR
	SET_BIT(bitsPlayers, NATIVE_TO_INT(PLAYER_ID()))
	Broadcast_Start_Reserved_Mission(uniqueID, bitsPlayers)
	
	// Corrective action taken, so trigger the assert that requests logs
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a request to change the mission details succeeded
//
// INPUT PARAMS:			paramResyncDataPos			The array position of this data on the resync array
// RETURN VALUE:			BOOL						TRUE if some form of corrective resync action was needed, FALSE if not.
//
// NOTES:	If a mission request with this unique ID doesn't exist, then ignore this request - assume something else has handled it not being there anymore.
//			If a mission request does exist but the mission is already active, then ignore this broadcast.
//			If a mission request does exist and is still reserved but the mission details are different, the re-send the broadcast.
FUNC BOOL Check_Resync_Of_External_Broadcast_Change_Reserved_Mission(INT paramResyncDataPos)

	#IF IS_DEBUG_BUILD
		NET_PRINT("          ")
		NET_PRINT_INT(paramResyncDataPos)
		NET_PRINT(") ")
		IF (paramResyncDataPos < 10)
			NET_PRINT(" ")
		ENDIF
		NET_PRINT("CHANGE RESERVED MISSION: ")
	#ENDIF

	// Get the unique ID sent with this broadcast
	INT uniqueID = g_sMCResyncComms[paramResyncDataPos].mcrbUniqueID
	
	// Check if there is an active mission request containing this uniqueID
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(uniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// ...mission doesn't exist, so assume something else has handled this and ignore
		#IF IS_DEBUG_BUILD
			NET_PRINT("[DOESN'T EXIST - SO IGNORE AND TREAT AS IN SYNC]")
			NET_NL()
		#ENDIF
		
		// No action taken, so don't trigger the assert that requests logs
		RETURN FALSE
	ENDIF
	
	// Mission exists, so check if it is already being activated
	#IF IS_DEBUG_BUILD
		NET_PRINT("[EXISTS]")
	#ENDIF
	
	IF NOT (Is_MP_Mission_Request_Slot_Status_Reserved(missionSlot))
		// ...mission is not reserved so must be being activated, so nothing more to do
		#IF IS_DEBUG_BUILD
			NET_PRINT("   [MISSION NOT RESERVED SO MUST BE GETTING ACTIVATED - SO TREAT AS IN SYNC]")
			NET_NL()
		#ENDIF
		
		// No action taken, so don't trigger the assert that requests logs
		RETURN FALSE
	ENDIF
	
	// Mission is reserved, so check if the details have changed
	IF (Get_MP_Mission_Request_MissionID(missionSlot) = g_sMCResyncComms[paramResyncDataPos].mcrbMissionID)
	AND (Get_MP_Mission_Request_MissionVariation(missionSlot) = g_sMCResyncComms[paramResyncDataPos].mcrbVariation)
		// ...mission details haven't changed, so nothing more to do
		#IF IS_DEBUG_BUILD
			NET_PRINT("   [MISSION DETAILS ARE THE SAME - SO IN SYNC]")
			NET_NL()
		#ENDIF
		
		// No action taken, so don't trigger the assert that requests logs
		RETURN FALSE
	ENDIF
	
	// Mission details have changed, so re-send the broadcast
	// KGM 15/3/13 - This isn't complete enough now - should also store creatorID, sharedRegID, and then re-grab the cloud filename. Bug Added.
	BOOL				actingAsScriptHost	= FALSE
	MP_MISSION_ID_DATA	theChangedIdData
	
	theChangedIdData.idMission	= g_sMCResyncComms[paramResyncDataPos].mcrbMissionID
	theChangedIdData.idVariation	= g_sMCResyncComms[paramResyncDataPos].mcrbVariation
	Broadcast_Change_Reserved_Mission_Details(uniqueID, theChangedIdData, actingAsScriptHost)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("   [MISSION DETAILS DIFFER - RE-SEND CHANGE MISSION DETAILS FOR UNIQUE ID: ") NET_PRINT_INT(uniqueID) NET_PRINT("]")
		NET_PRINT(" - CORRECTIVE ACTION")
		NET_NL()
	#ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a mission request to make a mission 'not joinable' has succeeded
//
// INPUT PARAMS:			paramResyncDataPos			The array position of this data on the resync array
// RETURN VALUE:			BOOL						TRUE if the Resync Assert should trigger (so I can check the logs), FALSE if not.
//
// NOTES:	This request is sent from the mission server, but it gets sent to all players so that all players can store it in case the mission server also bombs out.
//			If the mission is active and still joinable, then this player should re-send the request but ONLY if this player is the lowest PLAYER ID in the game (so it only gets sent once).
//			POTENTIAL PROBLEMS: If the mission requests secondary teams and not joinable within the host migration period then the state of the mission may end up wrong
FUNC BOOL Check_Resync_Of_External_Broadcast_Reissue_Not_Joinable(INT paramResyncDataPos)

	#IF IS_DEBUG_BUILD
		NET_PRINT("          ")
		NET_PRINT_INT(paramResyncDataPos)
		NET_PRINT(") ")
		IF (paramResyncDataPos < 10)
			NET_PRINT(" ")
		ENDIF
		NET_PRINT("CHECK MISSION NOT JOINABLE: ")
	#ENDIF

	// Get the unique ID sent with this broadcast
	INT uniqueID = g_sMCResyncComms[paramResyncDataPos].mcrbUniqueID
	
	// Check if there is an active mission request containing this uniqueID
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(uniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// ...mission doesn't exist, so nothing more to do
		#IF IS_DEBUG_BUILD
			NET_PRINT("[DOESN'T EXIST - SO TREAT AS IN SYNC]")
			NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Mission exists, so check if it is active
	#IF IS_DEBUG_BUILD
		NET_PRINT("[EXISTS]")
	#ENDIF
	
	// Ignore the request if the mission request is not active
	IF NOT (Is_MP_Mission_Request_Slot_Status_Active(missionSlot))
		// ...mission isn't active, so nothing more to do
		#IF IS_DEBUG_BUILD
			NET_PRINT("   [MISSION ISN'T ACTIVE - SO IGNORE THIS BROADCAST]")
			NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Mission active, so check if it is joinable by any team
	#IF IS_DEBUG_BUILD
		NET_PRINT("   [MISSION ACTIVE]")
	#ENDIF
	
	IF (Check_If_Players_Are_In_Teams_For_Mission(Get_MP_Mission_Request_MissionID(missionSlot)))
		// ...players are in teams, so check the joinable teams bitfield
		INT bitsJoinableTeams = Get_MP_Mission_Request_Joinable_Teams(missionSlot)
		
		IF (bitsJoinableTeams = ALL_MISSION_CONTROL_BITS_CLEAR)
			// ...mission is not joinable by any teams, so nothing more to do
			#IF IS_DEBUG_BUILD
				NET_PRINT("   [MISSION ISN'T JOINABLE BY ANY TEAMS - SO IN SYNC]")
				NET_NL()
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ELSE
		// ...players are not in teams, so check the mission bitflag
		IF NOT (Does_MP_Mission_Request_Allow_Players_To_Join_Mission(missionSlot))
			// ...mission is not joinable by players, so nothing more to do
			#IF IS_DEBUG_BUILD
				NET_PRINT("   [MISSION ISN'T JOINABLE BY PLAYERS FOR A MISSION THAT ISN'T TEAM-BASED - SO IN SYNC]")
				NET_NL()
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Mission is still joinable by at least one team, so re-send the broadcast if this player has the lowest player ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("   [MISSION STILL JOINABLE]")
	#ENDIF
	
	INT playerLoop = 0
	PLAYER_INDEX testPlayerIndex
	
	// ...only looking for the first valid player to compare with this player
	REPEAT NUM_NETWORK_PLAYERS playerLoop
		testPlayerIndex = INT_TO_PLAYERINDEX(playerLoop)
		IF (IS_NET_PLAYER_OK(testPlayerIndex, FALSE))
			// ...is the lowest player ID this player?
			IF (testPlayerIndex = PLAYER_ID())
				// ...this player has lowest player Index, so re-sending the broadcast
				#IF IS_DEBUG_BUILD
					NET_PRINT("   [THIS PLAYER HAS LOWEST PLAYER ID - RE-SENDING 'NOT JOINABLE' FOR UNIQUE ID: ") NET_PRINT_INT(uniqueID) NET_PRINT("]")
					NET_PRINT(" - CORRECTIVE ACTION")
					NET_NL()
				#ENDIF
				
				Broadcast_Mission_Not_Joinable(Get_MP_Mission_Request_Slot_Mission_Data(missionSlot))
		
				RETURN TRUE
			ENDIF
			
			// This player doesn't have the lowest player ID, so don't re-send the broadcast
			NET_PRINT("   [THIS PLAYER DOESN'T HAVE LOWEST PLAYER ID - IGNORING 'NOT JOINABLE' FOR UNIQUE ID: ") NET_PRINT_INT(uniqueID) NET_PRINT("]")
			NET_NL()
			
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("   [FAILED TO RE-ISSUE 'NOT JOINABLE' FOR UNIQUE ID: ") NET_PRINT_INT(uniqueID) NET_PRINT("]") NET_NL()
		SCRIPT_ASSERT("Check_Resync_Of_External_Broadcast_Reissue_Not_Joinable(): Failed to re-issue the 'not joinable' broadcast. Tell Keith.")
	#ENDIF
	
	// Return TRUE because this will output the resync assert and allow investigation
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a mission request to make a mission 'joinable again for players' has succeeded
//
// INPUT PARAMS:			paramResyncDataPos			The array position of this data on the resync array
// RETURN VALUE:			BOOL						TRUE if the Resync Assert should trigger (so I can check the logs), FALSE if not.
//
// NOTES:	This request is sent from the mission server, but it gets sent to all players so that all players can store it in case the mission server also bombs out.
//			If the mission is active and still joinable, then this player should re-send the request but ONLY if this player is the lowest PLAYER ID in the game (so it only gets sent once).
//			POTENTIAL PROBLEMS: If the mission requests secondary teams and not joinable within the host migration period then the state of the mission may end up wrong
FUNC BOOL Check_Resync_Of_External_Broadcast_Reissue_Joinable_For_Players(INT paramResyncDataPos)

	#IF IS_DEBUG_BUILD
		NET_PRINT("          ")
		NET_PRINT_INT(paramResyncDataPos)
		NET_PRINT(") ")
		IF (paramResyncDataPos < 10)
			NET_PRINT(" ")
		ENDIF
		NET_PRINT("CHECK MISSION JOINABLE FOR PLAYERS: ")
	#ENDIF

	// Get the unique ID sent with this broadcast
	INT uniqueID = g_sMCResyncComms[paramResyncDataPos].mcrbUniqueID
	
	// Check if there is an active mission request containing this uniqueID
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(uniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// ...mission doesn't exist, so nothing more to do
		#IF IS_DEBUG_BUILD
			NET_PRINT("[DOESN'T EXIST - SO TREAT AS IN SYNC]")
			NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Mission exists, so check if it is active
	#IF IS_DEBUG_BUILD
		NET_PRINT("[EXISTS]")
	#ENDIF
	
	// Ignore the request if the mission request is not active
	IF NOT (Is_MP_Mission_Request_Slot_Status_Active(missionSlot))
		// ...mission isn't active, so nothing more to do
		#IF IS_DEBUG_BUILD
			NET_PRINT("   [MISSION ISN'T ACTIVE - SO IGNORE THIS BROADCAST]")
			NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Mission active, so check if it is joinable by any team
	#IF IS_DEBUG_BUILD
		NET_PRINT("   [MISSION ACTIVE]")
	#ENDIF
	
	IF (Check_If_Players_Are_In_Teams_For_Mission(Get_MP_Mission_Request_MissionID(missionSlot)))
		// ...players are in teams, which is not supported yet
		#IF IS_DEBUG_BUILD
			NET_PRINT("   [PLAYERS ARE IN TEAMS WHICH IS NOT SUPPORTED BY 'JOINABLE AGAIN' - SO IN SYNC]")
			NET_NL()
		#ENDIF
		
		RETURN FALSE
	ELSE
		// ...players are not in teams, so check the mission bitflag
		IF (Does_MP_Mission_Request_Allow_Players_To_Join_Mission(missionSlot))
			// ...mission is already joinable by players, so nothing more to do
			#IF IS_DEBUG_BUILD
				NET_PRINT("   [MISSION IS ALREADY JOINABLE BY PLAYERS FOR A MISSION THAT ISN'T TEAM-BASED - SO IN SYNC]")
				NET_NL()
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Mission is not joinable by players, so re-send the broadcast if this player has the lowest player ID
	#IF IS_DEBUG_BUILD
		NET_PRINT("   [MISSION NOT JOINABLE BY PLAYERS]")
	#ENDIF
	
	INT playerLoop = 0
	PLAYER_INDEX testPlayerIndex
	
	// ...only looking for the first valid player to compare with this player
	REPEAT NUM_NETWORK_PLAYERS playerLoop
		testPlayerIndex = INT_TO_PLAYERINDEX(playerLoop)
		IF (IS_NET_PLAYER_OK(testPlayerIndex, FALSE))
			// ...is the lowest player ID this player?
			IF (testPlayerIndex = PLAYER_ID())
				// ...this player has lowest player Index, so re-sending the broadcast
				#IF IS_DEBUG_BUILD
					NET_PRINT("   [THIS PLAYER HAS LOWEST PLAYER ID - RE-SENDING 'JOINABLE AGAIN' FOR UNIQUE ID: ") NET_PRINT_INT(uniqueID) NET_PRINT("]")
					NET_PRINT(" - CORRECTIVE ACTION")
					NET_NL()
				#ENDIF
				
				Broadcast_Mission_Joinable_Again_For_Players(Get_MP_Mission_Request_Slot_Mission_Data(missionSlot))
		
				RETURN TRUE
			ENDIF
			
			// This player doesn't have the lowest player ID, so don't re-send the broadcast
			NET_PRINT("   [THIS PLAYER DOESN'T HAVE LOWEST PLAYER ID - IGNORING 'JOINABLE AGAIN' FOR UNIQUE ID: ") NET_PRINT_INT(uniqueID) NET_PRINT("]")
			NET_NL()
			
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("   [FAILED TO RE-ISSUE 'JOINABLE AGAIN' FOR UNIQUE ID: ") NET_PRINT_INT(uniqueID) NET_PRINT("]") NET_NL()
		SCRIPT_ASSERT("Check_Resync_Of_External_Broadcast_Reissue_Not_Joinable(): Failed to re-issue the 'joinable again' broadcast. Tell Keith.")
	#ENDIF
	
	// Return TRUE because this will output the resync assert and allow investigation
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a mission request to make a mission 'request secondary teams' has succeeded
//
// INPUT PARAMS:			paramResyncDataPos			The array position of this data on the resync array
// RETURN VALUE:			BOOL						TRUE if the Resync Assert should trigger (so I can check the logs), FALSE if not.
//
// NOTES:	This request is sent from the mission server, but it gets sent to all players so that all players can store it in case the mission server also bombs out.
//			If the mission is active and still joinable, then this player should re-send the request but ONLY if this player is the lowest PLAYER ID in the game (so it only gets sent once).
//			POTENTIAL PROBLEMS: If the mission requests secondary teams and not joinable within the host migration period then the state of the mission may end up wrong
FUNC BOOL Check_Resync_Of_External_Broadcast_Reissue_Secondary_Teams(INT paramResyncDataPos)

	#IF IS_DEBUG_BUILD
		NET_PRINT("          ")
		NET_PRINT_INT(paramResyncDataPos)
		NET_PRINT(") ")
		IF (paramResyncDataPos < 10)
			NET_PRINT(" ")
		ENDIF
		NET_PRINT("CHECK REQUEST SECONDARY_TEAMS: ")
	#ENDIF

	// Get the unique ID sent with this broadcast
	INT uniqueID = g_sMCResyncComms[paramResyncDataPos].mcrbUniqueID
	
	// Check if there is an active mission request containing this uniqueID
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(uniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// ...mission doesn't exist, so nothing more to do
		#IF IS_DEBUG_BUILD
			NET_PRINT("[DOESN'T EXIST - SO TREAT AS IN SYNC]")
			NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Mission exists, so check if it is active
	#IF IS_DEBUG_BUILD
		NET_PRINT("[EXISTS]")
	#ENDIF
	
	// Ignore the request if the mission request is not active
	IF NOT (Is_MP_Mission_Request_Slot_Status_Active(missionSlot))
		// ...mission isn't active, so nothing more to do
		#IF IS_DEBUG_BUILD
			NET_PRINT("   [MISSION ISN'T ACTIVE - SO IGNORE THIS BROADCAST]")
			NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Mission active, so re-issue the broadcast (this should be safe in the majority of circumstances)
	#IF IS_DEBUG_BUILD
		NET_PRINT("   [MISSION ACTIVE]")
	#ENDIF
	
	INT playerLoop = 0
	PLAYER_INDEX testPlayerIndex
	
	// ...only looking for the first valid player to compare with this player
	REPEAT NUM_NETWORK_PLAYERS playerLoop
		testPlayerIndex = INT_TO_PLAYERINDEX(playerLoop)
		IF (IS_NET_PLAYER_OK(testPlayerIndex, FALSE))
			// ...is the lowest player ID this player?
			IF (testPlayerIndex = PLAYER_ID())
				// ...this player has lowest player Index, so re-sending the broadcast
				#IF IS_DEBUG_BUILD
					NET_PRINT("   [THIS PLAYER HAS LOWEST PLAYER ID - RE-SENDING 'SECONDARY TEAMS' FOR UNIQUE ID: ") NET_PRINT_INT(uniqueID) NET_PRINT("]")
					NET_PRINT(" - CORRECTIVE ACTION")
					NET_NL()
				#ENDIF
				
				Broadcast_Mission_Ready_For_Secondary_Teams(Get_MP_Mission_Request_Slot_Mission_Data(missionSlot))
		
				RETURN TRUE
			ENDIF
			
			// This player doesn't have the lowest player ID, so don't re-send the broadcast
			NET_PRINT("   [THIS PLAYER DOESN'T HAVE LOWEST PLAYER ID - IGNORING 'SECONDARY TEAMS' FOR UNIQUE ID: ") NET_PRINT_INT(uniqueID) NET_PRINT("]")
			NET_NL()
			
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("   [FAILED TO RE-ISSUE 'SECONDARY TEAMS' FOR UNIQUE ID: ") NET_PRINT_INT(uniqueID) NET_PRINT("]") NET_NL()
		SCRIPT_ASSERT("Check_Resync_Of_External_Broadcast_Reissue_Secondary_Teams(): Failed to re-issue the 'secondary teams' broadcast. Tell Keith.")
	#ENDIF
	
	// Return TRUE because this will output the resync assert and allow investigation
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Do the External resync check against the resync check data
// NOTES:	Check for any external broadcasts to the Mission Controller that may have been lost.
PROC Perform_MP_Mission_Control_Client_External_Resync_Check()
			
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT("Performing External Resync Check (after recent Host migration): ") Print_MC_Host_Name_To_Console_Log() NET_NL()
	#ENDIF
	
	// Clear the timeout variable to stop the resync process
	g_sMCResync.mccrExternalTimeoutInitialised = FALSE
	
	// Output the currently stored details
	#IF IS_DEBUG_BUILD
		Debug_Output_Mission_Control_Resync_Array()
		NET_NL()
		NET_PRINT("      RESYNC CHECKING OF EXTERNAL BROADCASTS: ")
		NET_NL()
	#ENDIF
	
	// Check if any resyncing of the broadcasts from external systems is required
	#IF IS_DEBUG_BUILD
		BOOL safetyCheckRequestLogs = FALSE
	#ENDIF
	
	INT tempLoop = 0
	REPEAT g_numMCResyncBroadcasts tempLoop
		// Is this entry part of this resync check?
		IF (g_sMCResyncComms[tempLoop].mcrbInUseByActiveResync)
			SWITCH (g_sMCResyncComms[tempLoop].mcrbCommsTypeID)
				// Check Only if the mission request exists, send FAIL if not
				CASE MCCRCH_REQUEST_SPECIFIC_MISSION
				CASE MCCRCH_REQUEST_MISSION_OF_TYPE
					IF (Check_Resync_Of_External_Broadcast_Mission_Requested(tempLoop))
						#IF IS_DEBUG_BUILD
							// KGM 28/4/12: This is probably a rare occurance, so request logs to make sure everything is in order
							//				NOTE: Remove this flag when I am happy it is working properly
							safetyCheckRequestLogs = TRUE
						#ENDIF
					ENDIF
					BREAK
					
				// Check if the mission request exists and the player is allocated to the mission
				CASE MCCRCH_REQUEST_JOIN_MISSION
				CASE MCCRCH_RESERVE_MISSION_BY_PLAYER
				CASE MCCRCH_RESERVE_MISSION_BY_LEADER
					IF (Check_Resync_Of_External_Broadcast_Join_Request(tempLoop))
						#IF IS_DEBUG_BUILD
							// KGM 28/4/12: This is probably a rare occurance, so request logs to make sure everything is in order
							//				NOTE: Remove this flag when I am happy it is working properly
							safetyCheckRequestLogs = TRUE
						#ENDIF
					ENDIF
					BREAK
					
				// Check if the mission request exists and the player is not on the mission
				CASE MCCRCH_CANCEL_RESERVATION_BY_PLAYER
					IF (Check_Resync_Of_External_Broadcast_Cancel_Join(tempLoop))
						#IF IS_DEBUG_BUILD
							// KGM 28/4/12: This is probably a rare occurance, so request logs to make sure everything is in order
							//				NOTE: Remove this flag when I am happy it is working properly
							safetyCheckRequestLogs = TRUE
						#ENDIF
					ENDIF
					BREAK
					
				// Check if the mission request exists and the event needs broadcast again
				CASE MCCRCH_CANCEL_RESERVED_BY_LEADER
					IF (Check_Resync_Of_External_Broadcast_Cancel_Leader_Reserve(tempLoop))
						#IF IS_DEBUG_BUILD
							// KGM 26/6/12: This is probably a rare occurance, so request logs to make sure everything is in order
							//				NOTE: Remove this flag when I am happy it is working properly
							safetyCheckRequestLogs = TRUE
						#ENDIF
					ENDIF
					BREAK
					
				// Check if the mission request exists and is active
				CASE MCCRCH_START_RESERVED_MISSION
					IF (Check_Resync_Of_External_Broadcast_Start_Reserved_Mission(tempLoop))
						#IF IS_DEBUG_BUILD
							// KGM 28/4/12: This is probably a rare occurance, so request logs to make sure everything is in order
							//				NOTE: Remove this flag when I am happy it is working properly
							safetyCheckRequestLogs = TRUE
						#ENDIF
					ENDIF
					BREAK
					
				// Check if the mission request exists and the details are correct
				CASE MCCRCH_CHANGE_RESERVED_MISSION
					IF (Check_Resync_Of_External_Broadcast_Change_Reserved_Mission(tempLoop))
						#IF IS_DEBUG_BUILD
							// KGM 28/4/12: This is probably a rare occurance, so request logs to make sure everything is in order
							//				NOTE: Remove this flag when I am happy it is working properly
							safetyCheckRequestLogs = TRUE
						#ENDIF
					ENDIF
					BREAK
					
				// Check if the mission request is active but not joinable
				CASE MCCRCH_MISSION_NOT_JOINABLE
					IF (Check_Resync_Of_External_Broadcast_Reissue_Not_Joinable(tempLoop))
						#IF IS_DEBUG_BUILD
							// KGM 28/4/12: This is probably a rare occurance, so request logs to make sure everything is in order
							//				NOTE: Remove this flag when I am happy it is working properly
							safetyCheckRequestLogs = TRUE
						#ENDIF
					ENDIF
					BREAK
					
				// Check if the mission request is joinable again for players
				CASE MCCRCH_MISSION_JOINABLE_FOR_PLAYERS
					IF (Check_Resync_Of_External_Broadcast_Reissue_Joinable_For_Players(tempLoop))
						#IF IS_DEBUG_BUILD
							// KGM 28/4/12: This is probably a rare occurance, so request logs to make sure everything is in order
							//				NOTE: Remove this flag when I am happy it is working properly
							safetyCheckRequestLogs = TRUE
						#ENDIF
					ENDIF
					BREAK
					
				// Check if the mission request is active and all players are on it
				CASE MCCRCH_READY_FOR_SECONDARY_TEAMS
					IF (Check_Resync_Of_External_Broadcast_Reissue_Secondary_Teams(tempLoop))
						#IF IS_DEBUG_BUILD
							// KGM 28/4/12: This is probably a rare occurance, so request logs to make sure everything is in order
							//				NOTE: Remove this flag when I am happy it is working properly
							safetyCheckRequestLogs = TRUE
						#ENDIF
					ENDIF
					BREAK
					
				// Ignore this
				CASE MCCRCH_NO_COMMS
					#IF IS_DEBUG_BUILD
						NET_PRINT("          Perform_MP_Mission_Control_Client_External_Resync_Check(): Found MCCRCH_NO_COMMS in the external resync array") NET_NL()
						SCRIPT_ASSERT("Perform_MP_Mission_Control_Client_External_Resync_Check(): Found MCCRCH_NO_COMMS in the external Resync array. Tell Keith.")
					#ENDIF
					BREAK
			ENDSWITCH
		ELSE
			#IF IS_DEBUG_BUILD
				NET_PRINT("          ")
				NET_PRINT_INT(tempLoop)
				NET_PRINT(") ")
				IF (tempLoop < 10)
					NET_PRINT(" ")
				ENDIF
				NET_PRINT("IGNORED - THIS EVENT WAS RECEIVED AFTER THIS RESYNC CHECK STARTED")
				NET_NL()
			#ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("          <END>") NET_NL()
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		// KGM: TEMPORARY ASSERT REQUESTING LOGS TO CHECK THAT THE RESYNC CODE IS WORKING PROPERLY
		IF (safetyCheckRequestLogs)
// KGM 9/12/14: Haven't seen any issues for a long time, so switching off the assert
//			SCRIPT_ASSERT("MISSION CONTROL HOST MIGRATION EXTERNAL SYSTEMS: Please add a bug with full logs and F9 screenshot for Keith.")
			PRINTLN("MISSION CONTROL HOST MIGRATION EXTERNAL SYSTEMS: Switched off assert, but it would have triggered here.")
		ENDIF
	#ENDIF
	
	// Cancel the External resync
	MP_Mission_Control_Client_Clear_External_Resync_Data()	
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("          Controller and External Systems should be in sync after recent Host Migration") NET_NL()
	#ENDIF
	
	// Get rid of obsolete events in the resync storage array
	Clear_Old_MC_Broadcasts_From_Resync_Storage()
	
	// Output the new state of the array after resync
	#IF IS_DEBUG_BUILD
		Debug_Output_Mission_Control_Resync_Array()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Start a Resync after a Host Migration to ensure server and client are still in sync
PROC MP_Mission_Control_Client_Start_Resync()

	BOOL doResync = TRUE
	IF (g_sMCResync.mccrKnownHost = INVALID_PLAYER_INDEX())
		doResync = FALSE
	ENDIF

	// Store the new Host
	g_sMCResync.mccrKnownHost = GlobalServerBD_BlockB.missionControlData.mcHost
	
	#IF IS_DEBUG_BUILD
		Store_New_MC_Host_For_F9_Screen(g_sMCResync.mccrKnownHost)
	#ENDIF

	MP_Mission_Control_Client_Clear_Resync_Data()
			
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT("Mission Control Client detected KNOWN HOST has changed to be this player: ")
		IF (IS_NET_PLAYER_OK(GlobalServerBD_BlockB.missionControlData.mcHost, FALSE))
			NET_PRINT(GET_PLAYER_NAME(GlobalServerBD_BlockB.missionControlData.mcHost))
		ELSE
			NET_PRINT("UNKNOWN - NEW HOST MUST HAVE IMMEDIATELY LEFT SO HOST MUST BE JUST ABOUT TO CHANGE AGAIN")
		ENDIF
		NET_NL()
	#ENDIF
	
	// If the new host is not this player, then clear the global 'host attempted host migration' flag - it is only needed if a host tried to migrate, failed, and resumed hosting duties
	IF (g_sMCResync.mccrKnownHost != PLAYER_ID())
	AND (g_hostAttemptedHostMigration)
		g_hostAttemptedHostMigration = FALSE
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT("New host is not this player, so clearing g_hostAttemptedHostMigration - this must have been a previous host")
			NET_NL()
		#ENDIF
	ENDIF
	
	IF NOT (doResync)
		#IF IS_DEBUG_BUILD
			NET_PRINT("          A Client/Server resync is not required (looks like this player has just entered MP) - a resync is only needed on Host Migration") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Start a Resync (assume the new host is ok - if not then another new host should be allocated soon and all this will start again)
	// Triggering resync data
	g_sMCResync.mccrActivityOnClient	= Get_MP_Mission_Control_Client_Activity_For_Resync()
	g_sMCResync.mccrUniqueIDOnClient	= g_sTriggerMP.mtMissionData.mdUniqueID
	g_sMCResync.mccrStateOnServer		= Get_MP_Mission_Control_Server_State_and_UniqueID_For_Resync(g_sMCResync.mccrUniqueIDOnServer)
	g_sMCResync.mccrTriggeringTimeout	= Get_New_MP_Mission_Control_Client_Resync_Timeout()
	g_sMCResync.mccrTriggeringTimeoutInitialised = TRUE 
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("          Setting up a Client/Server Resync Check in ")
		NET_PRINT_INT(MCRESYNC_TIMER_DELAY_msec)
		NET_PRINT("msec.")
		NET_NL()
		NET_PRINT("             [Client Current Activity: ")
		NET_PRINT(Convert_Resync_Activity_To_String(g_sMCResync.mccrActivityOnClient))
		NET_PRINT("]  [client uniqueID: ")
		NET_PRINT_INT(g_sMCResync.mccrUniqueIDOnClient)
		NET_PRINT("]")
		NET_NL()
		NET_PRINT("             [Player State on Server: ")
		NET_PRINT(Convert_Resync_Host_State_To_String(g_sMCResync.mccrStateOnServer))
		NET_PRINT("]  [UniqueID on Server: ")
		NET_PRINT_INT(g_sMCResync.mccrUniqueIDOnServer)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
	// External systems resync data
	#IF IS_DEBUG_BUILD
		NET_PRINT("          Setting up an External Events Resync Check in ")
		NET_PRINT_INT(MCRESYNC_TIMER_DELAY_msec)
		NET_PRINT("msec.")
		NET_NL()
	
		// Output the new state of the array after resync
		#IF IS_DEBUG_BUILD
			NET_PRINT("          CURRENT STATE OF THE RESYNC ARRAY AT RESYNC ACTIVATION BEFORE CLEANUP") NET_NL()
			Debug_Output_Mission_Control_Resync_Array()
		#ENDIF
	#ENDIF

	// ...first, clear out any old entries in the resync storage array
	BOOL alsoClearActiveResyncEvents = FALSE
	Clear_Old_MC_Broadcasts_From_Resync_Storage(alsoClearActiveResyncEvents)
	
	// ...then mark any remaining entries as active entries to be checked during this resync
	//		(this allows these entries to be identified so that new entries can be ignored during resync checks)
	Mark_All_MC_Broadcasts_In_Resync_Storage_As_Being_Checked()
	
	// ...store the timeout time for completing the External Events resync check
	g_sMCResync.mccrExternalTimeout		= g_sMCResync.mccrTriggeringTimeout
	g_sMCResync.mccrExternalTimeoutInitialised = TRUE
		
	#IF IS_DEBUG_BUILD
		NET_PRINT("          NEW STATE OF THE RESYNC ARRAY AT RESYNC ACTIVATION AFTER CLEANUP") NET_NL()
		Debug_Output_Mission_Control_Resync_Array()
	#ENDIF
	
	// ...clear the 'request logs' flag
	#IF IS_DEBUG_BUILD
		g_debugRequestTriggerResyncLogs = FALSE
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain any ongoing Resync to ensure the Mission Controller and the Mission Triggerer are still in sync
PROC Maintain_MP_Mission_Control_Client_Triggerer_Resync()

	// Is a resync in progress?
	IF (g_sMCResync.mccrTriggeringTimeoutInitialised = FALSE)
		EXIT
	ENDIF
	
	// A resync is in progress, so maintain it
	TIME_DATATYPE netTimer = GET_NETWORK_TIME()
	
	// Check for initial resync check timeout
	IF IS_TIME_LESS_THAN(netTimer , g_sMCResync.mccrTriggeringTimeout)
		// ...timer hasn't expired
		EXIT
	ENDIF
			
	// Timer has expired, so do resync processing
	Perform_MP_Mission_Control_Client_Triggerer_Resync_Check()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain any ongoing Resync to ensure the Mission Controller and external systems are still in sync
PROC Maintain_MP_Mission_Control_Client_External_Resync()

	// Is a resync in progress?
	IF (g_sMCResync.mccrExternalTimeoutInitialised = FALSE)
		EXIT
	ENDIF
	
	// A resync is in progress, so maintain it
	TIME_DATATYPE netTimer = GET_NETWORK_TIME()
	
	// Check for initial resync check timeout
	IF IS_TIME_LESS_THAN(netTimer , g_sMCResync.mccrExternalTimeout)
		// ...timer hasn't expired
		EXIT
	ENDIF
			
	// Timer has expired, so do resync processing
	Perform_MP_Mission_Control_Client_External_Resync_Check()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain any ongoing Resync to ensure this client and the Mission Control server are still in sync
PROC Maintain_MP_Mission_Control_Client_Resync()

	// Resync checking between mission controller and mission triggerer
	Maintain_MP_Mission_Control_Client_Triggerer_Resync()
	
	// Resync checking between mission controller and external systems
	Maintain_MP_Mission_Control_Client_External_Resync()

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Client Initialisation Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	A one-off Mission Control Client initialisation.
PROC Initialise_MP_Mission_Control_Client()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Initialise_MP_Mission_Control_Client") NET_NL()
	#ENDIF

	// Clear out the Client Broadcast storage array
	MP_Mission_Control_Client_Clear_Broadcast_Storage_Data()
	
	// Resync data - follows a Host Migration to ensure Client and Server and still in sync and no events have been lost
	g_sMCResync.mccrKnownHost	= INVALID_PLAYER_INDEX()
	MP_Mission_Control_Client_Clear_Resync_Data()
	Clear_All_MC_Broadcast_Resync_Storage()
	
	#IF IS_DEBUG_BUILD
		Clear_F9_Screen_MC_Hosts_Data()
		Clear_Tutorial_Session_Debug_Variables()
	#ENDIF
	
	// Sync the local known resyncID with the server version
	g_localKnownMissionControlResyncID = GlobalServerBD_BlockB.forceMissionControlResyncID

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Client Maintenance Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Mission Control Client maintenance.
PROC Maintain_MP_Mission_Control_Client()

	// Check if the Known Host has changed
	// KGM 12/9/13: or if the local known resyncID differs from the server resyncID meaning a forces resync is required
	IF NOT (g_sMCResync.mccrKnownHost = GlobalServerBD_BlockB.missionControlData.mcHost)
	OR (GlobalServerBD_BlockB.forceMissionControlResyncID != g_localKnownMissionControlResyncID)
		// The known host has changed, so start a resync after host migration
		#IF IS_DEBUG_BUILD
			IF (g_localKnownMissionControlResyncID != GlobalServerBD_BlockB.forceMissionControlResyncID)
				NET_PRINT("...KGM MP [MControl]: Server has forced a resync")
				NET_PRINT(" [Server: ")
				NET_PRINT_INT(GlobalServerBD_BlockB.forceMissionControlResyncID)
				NET_PRINT("  Local: ")
				NET_PRINT_INT(g_localKnownMissionControlResyncID)
				NET_PRINT("]")
				NET_NL()
			ENDIF
		#ENDIF
		
		g_localKnownMissionControlResyncID = GlobalServerBD_BlockB.forceMissionControlResyncID
		
		MP_Mission_Control_Client_Start_Resync()
	ENDIF
	
	// Maintain any ongoing Host Migration Resync operations on the client
	Maintain_MP_Mission_Control_Client_Resync()

	// Maintain the entries on the Broadcast Storage array
	Maintain_MP_Mission_Control_Client_Broadcast_Storage_Data()
	
	#IF IS_DEBUG_BUILD
		Debug_Mission_Control_F9_Screen()
		Debug_Current_Tutorial_Session_Details()
	#ENDIF

ENDPROC



