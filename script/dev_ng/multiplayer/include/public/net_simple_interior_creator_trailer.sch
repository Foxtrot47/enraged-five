//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_simple_interior_creator_trailer.sch																//
// Description: This is implementation of Armory Truck as simple interior. As such this does not expose any			//
//				public functions. All functions to manipulate simple interiors are in net_simple_interior			//
// Written by:  Ata																								//
// Date:  		29/04/2017																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_simple_interior_common.sch"
USING "net_include.sch"
USING "cutscene_help.sch"
USING "net_property_sections_armory_truck.sch"
USING "net_simple_interior_cs_header_include.sch"


CONST_INT SIMPLE_INTERIOR_CREATOR_TRAILER_ACCESS_BS_OWNER 				0 // We entered armory truck as owner (but not boss)
CONST_INT SIMPLE_INTERIOR_CREATOR_TRAILER_ACCESS_BS_NON_GANG_MEMBER		1 // We entered armory truck as guest (not member of owner's organization or gang)
CONST_INT SIMPLE_INTERIOR_CREATOR_TRAILER_ACCESS_BS_OWNER_LEFT_GAME		2 	// The owner has left the game
CONST_INT SIMPLE_INTERIOR_CREATOR_TRAILER_ACCESS_BS_EXIT_OWNER_JOINED_GANG 3

//Blip data
CONST_INT MAX_CREATOR_TRAILER_BLIPS	1


FUNC BOOL DOES_CREATOR_TRAILER_USE_EXTERIOR_SCRIPT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN TRUE
ENDFUNC

PROC GET_CREATOR_TRAILER_TYPE_AND_POSITION(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtType, VECTOR &vPosition, FLOAT &fHeading  , BOOL bUseSecondaryInteriorDetails )
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_CREATOR_TRAILER_1
			vPosition = <<1103.562378, -3000.0, -40.0>>
			fHeading = 0.0
			txtType = "gr_grdlc_int_01"
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_CREATOR_TRAILER_LOAD_SECONDARY_INTERIOR(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF IS_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER_SET()
		SET_INTERIOR_FLOOR_INDEX(GET_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER())
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VEHICLE_INDEX GET_CREATOR_TRAILER_VEH_INDEX()
	
	// if player is part of gang
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		IF PLAYER_ID() != GB_GET_LOCAL_PLAYER_GANG_BOSS()
			IF NOT DOES_ENTITY_EXIST(MPGlobalsAmbience.vehCreatorTrailer)
				INT iVeh
				VEHICLE_INDEX nearbyVehs[25]
				
				INT iNearbyVehs = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs)
				
				REPEAT iNearbyVehs iVeh
					IF DOES_ENTITY_EXIST(nearbyVehs[iVeh])
					AND IS_ENTITY_A_VEHICLE(nearbyVehs[iVeh])
						IF NOT IS_ENTITY_DEAD(nearbyVehs[iVeh])
							IF DECOR_IS_REGISTERED_AS_TYPE("Creator_Trailer", DECOR_TYPE_INT)
								IF IS_VEHICLE_MODEL(nearbyVehs[iVeh], TRAILERLARGE)
									IF GET_OWNER_OF_CREATOR_TRAILER(nearbyVehs[iVeh]) = GB_GET_LOCAL_PLAYER_GANG_BOSS()
										#IF IS_DEBUG_BUILD
										IF GET_FRAME_COUNT() % 60 = 0
											PRINTLN("GET_CREATOR_TRAILER_VEH_INDEX - I'm in gang returning ", GET_PLAYER_NAME(GB_GET_LOCAL_PLAYER_GANG_BOSS()), " vehicle") 
										ENDIF
										#ENDIF
										MPGlobalsAmbience.vehCreatorTrailer = nearbyVehs[iVeh]
										RETURN nearbyVehs[iVeh]
									ENDIF
								ENDIF		
							ENDIF	
						ENDIF
					ENDIF
				ENDREPEAT
			ELSE
				PRINTLN("GET_CREATOR_TRAILER_VEH_INDEX - I'm in gang returning MPGlobalsAmbience.vehCreatorTrailer") 
				RETURN MPGlobalsAmbience.vehCreatorTrailer
			ENDIF
		ENDIF
	ELIF PLAYER_ID() = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	AND NOT DOES_ENTITY_EXIST(MPGlobalsAmbience.vehCreatorTrailer)
		INT iVeh
		VEHICLE_INDEX nearbyVehs[25]
		
		INT iNearbyVehs = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs)
		
		REPEAT iNearbyVehs iVeh
			IF DOES_ENTITY_EXIST(nearbyVehs[iVeh])
			AND IS_ENTITY_A_VEHICLE(nearbyVehs[iVeh])
				IF NOT IS_ENTITY_DEAD(nearbyVehs[iVeh])
					IF DECOR_IS_REGISTERED_AS_TYPE("Creator_Trailer", DECOR_TYPE_INT)
						IF IS_VEHICLE_MODEL(nearbyVehs[iVeh], TRAILERLARGE) 
							IF GET_OWNER_OF_CREATOR_TRAILER(nearbyVehs[iVeh]) = PLAYER_ID()
								#IF IS_DEBUG_BUILD
								IF GET_FRAME_COUNT() % 60 = 0
									PRINTLN("GET_CREATOR_TRAILER_VEH_INDEX - I'm boss returning ", GET_PLAYER_NAME(PLAYER_ID()), " vehicle") 
								ENDIF
								#ENDIF
								MPGlobalsAmbience.vehCreatorTrailer = nearbyVehs[iVeh]
								RETURN nearbyVehs[iVeh]
							ENDIF
						ENDIF		
					ENDIF	
				ENDIF
			ENDIF
		ENDREPEAT
	
	ENDIF

	RETURN MPGlobalsAmbience.vehCreatorTrailer
ENDFUNC

FUNC VEHICLE_INDEX GET_CREATOR_TRAILER_CAB_VEH_INDEX()
	
	IF NOT DOES_ENTITY_EXIST(MPGlobalsAmbience.vehTruckVehicle[0])
		IF (DOES_ENTITY_EXIST(GET_CREATOR_TRAILER_VEH_INDEX()) AND NOT IS_ENTITY_DEAD(GET_CREATOR_TRAILER_VEH_INDEX()))
			IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(GET_CREATOR_TRAILER_VEH_INDEX())
				ENTITY_INDEX entityId = GET_ENTITY_ATTACHED_TO(GET_CREATOR_TRAILER_VEH_INDEX())
				IF IS_ENTITY_A_VEHICLE(entityId)
					MPGlobalsAmbience.vehTruckVehicle[0] = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityId)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN MPGlobalsAmbience.vehTruckVehicle[0]
ENDFUNC

FUNC PLAYER_INDEX GET_OWNER_OF_CREATOR_TRAILER_VEH_INDEX()
	RETURN GET_OWNER_OF_CREATOR_TRAILER(GET_CREATOR_TRAILER_VEH_INDEX())
ENDFUNC	

FUNC SIMPLE_INTERIOR_DETAILS_BS GET_CREATOR_TRAILER_PROPERTIES_BS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	SIMPLE_INTERIOR_DETAILS_BS structReturn
	
	//BS1
	
	//BS2
	SET_BIT(structReturn.iBS[1], BS2_SIMPLE_INTERIOR_DETAILS_PARENT_INT_SCRIPT_DOES_LOAD_SCENE)
	SET_BIT(structReturn.iBS[1], BS2_SIMPLE_INTERIOR_DETAILS_RUNS_IN_ACTIVITY_SESSION)
	SET_BIT(structReturn.iBS[1], BS2_SIMPLE_INTERIOR_DETAILS_RUNS_ONLY_IN_ACTIVITY_SESSION)
	
	RETURN structReturn
ENDFUNC

PROC GET_CREATOR_TRAILER_DETAILS(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS &details, BOOL bUseSecondaryInteriorDetails, BOOL bSMPLIntPreCache)
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	UNUSED_PARAMETER(bSMPLIntPreCache)
	details.sProperties = GET_CREATOR_TRAILER_PROPERTIES_BS(eSimpleInteriorID)
	
	//Generic data that is the same for all armory truck
	details.strInteriorChildScript = "AM_MP_CREATOR_TRAILER"
	
	details.entryAnim.strOnEnterSoundNames[0] = "PUSH"
	details.entryAnim.strOnEnterSoundNames[1] = "LIMIT"
	details.entryAnim.fOnEnterSoundPhases[0] = 0.271
	details.entryAnim.fOnEnterSoundPhases[1] = 0.411
	details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_EXEC_WH_DOOR_GENERIC_SOUNDS"
	details.bInteriorIsSeamless = FALSE
	
	// Exit data
	details.exitData.vLocateCoords1[0] = <<-2.300,9.690,0.060>>
	details.exitData.vLocateCoords2[0] = <<2.300,9.690,2.305>>
	details.exitData.fLocateWidths[0] = 1.000
	details.exitData.fHeadings[0] = 349.3822
	
	SWITCH eSimpleInteriorID
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_CREATOR_TRAILER_1 // pass 1
			IF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = 0
			OR IS_PLAYER_IN_BUNKER(PLAYER_ID())	
				PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_DETAILS entrance 0 fade out")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC STRING GET_CREATOR_TRAILER_NAME(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_CREATOR_TRAILER_1 RETURN "CREATOR_TRAILER_1"
	ENDSWITCH
	
	RETURN "MISSING"
ENDFUNC

PROC GET_CREATOR_TRAILER_INSIDE_BOUNDING_BOX(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX &bbox  , BOOL bUseSecondaryInteriorDetails )
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_CREATOR_TRAILER_1
			bbox.vInsideBBoxMin = <<1097.5347, -3016.0115, -40.7658>>
			bbox.vInsideBBoxMax = <<1109.2977, -2983.6902, -34.1882>>
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CREATOR_TRAILER_ENTRY_LOCATE(SIMPLE_INTERIORS eSimpleInteriorID, VECTOR &vLocate)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_CREATOR_TRAILER_1
			IF DOES_ENTITY_EXIST(GET_CREATOR_TRAILER_VEH_INDEX())
			AND NOT IS_ENTITY_DEAD(GET_CREATOR_TRAILER_VEH_INDEX())
				IF GET_ENTITY_MODEL(GET_CREATOR_TRAILER_VEH_INDEX()) = TRAILERLARGE
					FLOAT fGroundZ
					vLocate = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(GET_CREATOR_TRAILER_VEH_INDEX()), GET_ENTITY_HEADING(GET_CREATOR_TRAILER_VEH_INDEX()), <<0.0,-8.9,-1.5>>)
					IF GET_GROUND_Z_FOR_3D_COORD(vLocate,fGroundZ)
						vLocate = <<vLocate.x,vLocate.y,fGroundZ>>
					ENDIF
					//PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_ENTRY_LOCATE for SIMPLE_INTERIOR_CREATOR_TRAILER_1 = vLocate: ", vLocate)
				ENDIF
			ENDIF	
		BREAK
	ENDSWITCH	
ENDPROC

PROC GET_CREATOR_TRAILER_HEADING(SIMPLE_INTERIORS eSimpleInteriorID, FLOAT &fHeading)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_CREATOR_TRAILER_1
			IF DOES_ENTITY_EXIST(GET_CREATOR_TRAILER_VEH_INDEX())
			AND NOT IS_ENTITY_DEAD(GET_CREATOR_TRAILER_VEH_INDEX())
				IF GET_ENTITY_MODEL(GET_CREATOR_TRAILER_VEH_INDEX()) = TRAILERLARGE
					
					fHeading = GET_ENTITY_HEADING(GET_CREATOR_TRAILER_VEH_INDEX())					
					//PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_ENTRY_LOCATE for SIMPLE_INTERIOR_CREATOR_TRAILER_1 = vLocate: ", vLocate)
				ENDIF
			ENDIF	
		BREAK
	ENDSWITCH	
ENDPROC

PROC BROADCAST_CREATOR_TRAILER_EXIT_SPAWN_POINT(BOOL bBroadCastNow = FALSE)
	IF GET_FRAME_COUNT() % 60 = 0
	OR bBroadCastNow
		IF DOES_ENTITY_EXIST(GET_CREATOR_TRAILER_VEH_INDEX())
		AND NOT IS_ENTITY_DEAD(GET_CREATOR_TRAILER_VEH_INDEX())
			IF GET_OWNER_OF_CREATOR_TRAILER(GET_CREATOR_TRAILER_VEH_INDEX()) = PLAYER_ID()
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.vMobileInteriorExitSpawnPoint = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(GET_CREATOR_TRAILER_VEH_INDEX()), GET_ENTITY_HEADING(GET_CREATOR_TRAILER_VEH_INDEX()), <<2,-17, 0>>)
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

PROC GET_CREATOR_TRAILER_ENTRY_LOCATE_COLOUR(SIMPLE_INTERIORS eSimpleInteriorID, INT &iR, INT &iG, INT &iB, INT &iA)
	UNUSED_PARAMETER(eSimpleInteriorID)
	GET_HUD_COLOUR(HUD_COLOUR_BLUE,iR, iG, iB, iA)
ENDPROC

FUNC BOOL SHOULD_THIS_CREATOR_TRAILER_CORONA_BE_HIDDEN(SIMPLE_INTERIORS eSimpleInteriorID #IF IS_DEBUG_BUILD , BOOL bPrintReason = FALSE #ENDIF)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	#IF IS_DEBUG_BUILD
	UNUSED_PARAMETER(bPrintReason)
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_THIS_CREATOR_TRAILER_LOCATE_BE_HIDDEN(SIMPLE_INTERIORS eSimpleInteriorID #IF IS_DEBUG_BUILD , BOOL bPrintReason = FALSE #ENDIF)
	UNUSED_PARAMETER(eSimpleInteriorID)
	#IF IS_DEBUG_BUILD
	UNUSED_PARAMETER(bPrintReason)
	#ENDIF
	
	IF IS_ENTITY_DEAD(GET_CREATOR_TRAILER_VEH_INDEX())
		PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] - SHOULD_THIS_CREATOR_TRAILER_LOCATE_BE_HIDDEN true IS_ENTITY_DEAD true")
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(GET_CREATOR_TRAILER_VEH_INDEX())
		IF !IS_ENTITY_VISIBLE(GET_CREATOR_TRAILER_VEH_INDEX())
			PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] - SHOULD_THIS_CREATOR_TRAILER_LOCATE_BE_HIDDEN true IS_ENTITY_VISIBLE false")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(GET_CREATOR_TRAILER_VEH_INDEX())
	AND NOT IS_ENTITY_DEAD(GET_CREATOR_TRAILER_VEH_INDEX())
		IF GET_ENTITY_SPEED(GET_CREATOR_TRAILER_VEH_INDEX()) > 0.5
			SET_OVERRIDE_SIMPLE_INTERIOR_ENTRY_CORONA_COLOUR(FALSE,eSimpleInteriorID)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] - SHOULD_THIS_CREATOR_TRAILER_LOCATE_BE_HIDDEN true player is in corona")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_MOBILE_INTERIOR_CORONA_POS_BLOCKED)
		PRINTLN("SHOULD_THIS_CREATOR_TRAILER_LOCATE_BE_HIDDEN TRUE BS_SIMPLE_INTERIOR_GLOBAL_DATA_MOBILE_INTERIOR_CORONA_POS_BLOCKED true")
		IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
			SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(GET_CREATOR_TRAILER_VEH_INDEX())
	AND NOT IS_ENTITY_DEAD(GET_CREATOR_TRAILER_VEH_INDEX())
		IF IS_ENTITY_IN_AIR(GET_CREATOR_TRAILER_VEH_INDEX())
			PRINTLN("SHOULD_THIS_CREATOR_TRAILER_LOCATE_BE_HIDDEN TRUE trailer in air")
			IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
				SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_bPlayerViewingTurretHud  
		PRINTLN("SHOULD_THIS_CREATOR_TRAILER_LOCATE_BE_HIDDEN TRUE g_bPlayerViewingTurretHud is true")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		CLEAR_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_ENTER_CREATOR_TRAILER(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID, INT iEntranceID)
	UNUSED_PARAMETER(iEntranceID)
	UNUSED_PARAMETER(playerID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_PLAYER_ENTER_CREATOR_TRAILER_IN_VEHICLE(SIMPLE_INTERIORS eSimpleInteriorID, MODEL_NAMES eModel)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(eModel)
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_USE_CREATOR_TRAILER_ENTRY_IN_CURRENT_VEHICLE(SIMPLE_INTERIOR_ENTRANCE_DATA &data, SIMPLE_INTERIOR_DETAILS &details, BOOL &bReturnSetExitCoords)
	UNUSED_PARAMETER(bReturnSetExitCoords)
	UNUSED_PARAMETER(data)
	UNUSED_PARAMETER(details)
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_INSIDE_CREATOR_TRAILER_ENTRY_AREA()
	
	IF DOES_ENTITY_EXIST(GET_CREATOR_TRAILER_VEH_INDEX())
	AND NOT IS_ENTITY_DEAD(GET_CREATOR_TRAILER_VEH_INDEX())
		IF GET_ENTITY_MODEL(GET_CREATOR_TRAILER_VEH_INDEX()) = TRAILERLARGE

			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID()
			, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(GET_CREATOR_TRAILER_VEH_INDEX()), GET_ENTITY_HEADING(GET_CREATOR_TRAILER_VEH_INDEX()), <<0.0,-15,-7.0>>)
			, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(GET_CREATOR_TRAILER_VEH_INDEX()), GET_ENTITY_HEADING(GET_CREATOR_TRAILER_VEH_INDEX()), <<0.0,-6.5,2.5>>)
			, GET_MODEL_WIDTH(TRAILERLARGE) * 2.5)
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_CREATOR_TRAILER_REASON_FOR_BLOCKED_ENTRY(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, INT iEntranceUsed)
	UNUSED_PARAMETER(iEntranceUsed)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(txtExtraString)
	
//	IF IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
//	AND (PLAYER_ID() = GET_OWNER_OF_CREATOR_TRAILER_VEH_INDEX()
//	OR GB_GET_LOCAL_PLAYER_GANG_BOSS() = GET_OWNER_OF_CREATOR_TRAILER_VEH_INDEX())
//		RETURN "JUG_BLOCK_MOC"
//	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		// You are unable to access the the Mobile Operations Center in this area. Move the Mobile Operations Center to suitable area.
		RETURN "GR_TRUCK_SUTAR"
	ENDIF
	
	IF IS_NPC_IN_VEHICLE()
		// You cannot enter whilst an NPC is in the vehicle.
		RETURN "NPC_BLOCK"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_CREATOR_TRAILER_REASON_FOR_BLOCKED_EXIT(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, INT iExitUsed)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(txtExtraString)
	UNUSED_PARAMETER(iExitUsed)
	
	RETURN ""
ENDFUNC

FUNC BOOL CAN_PLAYER_INVITE_OTHERS_TO_CREATOR_TRAILER(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(playerID)
	
	IF PLAYER_ID() = GET_OWNER_OF_CREATOR_TRAILER_VEH_INDEX()
		IF g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty = -1	//Only invited players will have this set whilst they autowarp so block invites
			IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_CREATOR_TRAILER_ACCESS_BS_OWNER)		
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_BE_INVITED_TO_CREATOR_TRAILER(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF playerID != INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(playerID)
	AND NOT IS_PED_WEARING_JUGGERNAUT_SUIT(GET_PLAYER_PED(playerID))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_LOCAL_PLAYER_BE_INVITED_TO_CREATOR_TRAILER_BY_PLAYER(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX pInvitingPlayer)
	UNUSED_PARAMETER(eSimpleInteriorID)
	IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
		IF ARE_PLAYERS_IN_SAME_PROPERTY(PLAYER_ID(), pInvitingPlayer, FALSE, TRUE)
		OR HAVE_I_GOT_TRUCK_INVITE_INSTANCE()
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC GET_CREATOR_TRAILER_ENTRANCE_MENU_TITLE_AND_OPTIONS(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], SIMPLE_INTERIOR_LOCAL_EVENTS &eventOptions[], INT &iOptionsCount)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_CREATOR_TRAILER_1
			txtTitle = "ARMORY_TITLE"
		BREAK
	ENDSWITCH
	
	strOptions[0] = "BWH_ENTRM_ALONE"
	eventOptions[0] = SI_EVENT_ENTRY_MENU_ENTER_ALONE
	
	//Enter with org members
	strOptions[1] = "PROP_HEI_E_3"
	//Enter with nearby friends & crew
	strOptions[2] = "PROP_HEI_E_2"
	
	eventOptions[1] = SI_EVENT_ENTRY_MENU_ENTER_WITH_NEARBY
	eventOptions[2] = SI_EVENT_ENTRY_MENU_ENTER_WITH_NEARBY
	iOptionsCount = 3
ENDPROC

PROC SET_CREATOR_TRAILER_ACCESS_BS(SIMPLE_INTERIORS eSimpleInteriorID, INT iInvitingPlayer)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iInvitingPlayer)
	
	globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX()
	globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iPlayersToJoinBS = 0
	PLAYER_INDEX playerBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
		
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
	
		IF playerBoss != INVALID_PLAYER_INDEX()			
			//Set this so we know who the owner is
			globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = playerBoss
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][CREATOR_TRAILER] SET_CREATOR_TRAILER_ACCESS_BS - We want to join our boss: ", GET_PLAYER_NAME(playerBoss))
		ENDIF
		g_ownerOfArmoryTruckPropertyIAmIn = playerBoss
		SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_CREATOR_TRAILER_ACCESS_BS_NON_GANG_MEMBER)
	ELIF PLAYER_ID() = GET_OWNER_OF_CREATOR_TRAILER_VEH_INDEX()
	OR PLAYER_ID() = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())

		g_ownerOfArmoryTruckPropertyIAmIn = PLAYER_ID()

		SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_CREATOR_TRAILER_ACCESS_BS_OWNER)
		
		PRINTLN("[SIMPLE_INTERIOR][CREATOR_TRAILER] SET_CREATOR_TRAILER_ACCESS_BS - we are entering a truck that we own. Non gang entry")
		globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = g_ownerOfArmoryTruckPropertyIAmIn
	ENDIF
	
	CLEAR_SIMPLE_INTERIOR_AUTOWARP_OVERRIDE()
	PRINTLN("[SIMPLE_INTERIOR][CREATOR_TRAILER] SET_CREATOR_TRAILER_ACCESS_BS - clearing iOverrideAutoWarpToPlayersProperty.")
	
ENDPROC

PROC TRIGGER_LOCAL_PLAYER_CREATOR_TRAILER_ENTRY(SIMPLE_INTERIORS eSimpleInteriorID)
	SET_CREATOR_TRAILER_ACCESS_BS(eSimpleInteriorID, -1)
			
	// We need to set this manually so DO_SIMPLE_INTERIOR_AUTOWARP will work, 
	// otherwise it will return TRUE straight away as it checks if local player is in the interior, 
	// it's a bit of a hack but a really small, harmless one.
	g_SimpleInteriorData.eCurrentInteriorID = SIMPLE_INTERIOR_INVALID 
			
	SET_SIMPLE_INTERIOR_EVENT_AUTOWARP_ACTIVE(TRUE, eSimpleInteriorID)
	g_SimpleInteriorData.bEventAutowarpOverrideCoords = FALSE
	g_SimpleInteriorData.bEventAutowarpSetInInterior = TRUE
ENDPROC

PROC PROCESS_CREATOR_TRAILER_ENTRANCE_MENU(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_MENU &menu)
		
	CONST_INT MAX_NEARBY_PEDS_TO_CHECK 10
	BOOL bFriendsNearby, bGangMembersNearby
	INT i, iPeds
	INT iLocalCrewID = -1
	PED_INDEX mPed[MAX_NEARBY_PEDS_TO_CHECK]
	VECTOR vEntryLocate
	
	IF CAN_PLAYER_INVITE_OTHERS_TO_CREATOR_TRAILER(PLAYER_ID(), eSimpleInteriorID)
		
		IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
			iLocalCrewID = GET_LOCAL_PLAYER_CLAN_ID()
		ENDIF
		
		GET_CREATOR_TRAILER_ENTRY_LOCATE(eSimpleInteriorID, vEntryLocate)
		
		iPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), mPed)
		
		IF iPeds > 0
			REPEAT iPeds i 
				IF NOT IS_ENTITY_DEAD(mPed[i])
				AND IS_PED_A_PLAYER(mPed[i])
				
					IF VDIST(GET_ENTITY_COORDS(mPed[i]), vEntryLocate) <= GET_SIMPLE_INTERIOR_INVITE_RADIUS(eSimpleInteriorID)
						
						PLAYER_INDEX piPlayer 		= NETWORK_GET_PLAYER_INDEX_FROM_PED(mPed[i])
						GAMER_HANDLE playerHandle 	= GET_GAMER_HANDLE_PLAYER(piPlayer)
						
						IF NOT bFriendsNearby
							IF iLocalCrewID != -1
							AND iLocalCrewID = GET_MP_PLAYER_CLAN_ID(playerHandle)
							OR NETWORK_IS_FRIEND(playerHandle)
								IF NOT IS_PED_WEARING_JUGGERNAUT_SUIT(GET_PLAYER_PED(piPlayer))
								AND NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(piPlayer, TRUE)
								AND NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(piPlayer)
									bFriendsNearby = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT bGangMembersNearby
						AND GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), piPlayer)
						AND NOT IS_PED_WEARING_JUGGERNAUT_SUIT(GET_PLAYER_PED(piPlayer))
						AND NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(piPlayer, TRUE)
						AND NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(piPlayer)
							bGangMembersNearby = TRUE
						ENDIF
					ENDIF
					
					//The list of peds in order of proximity to the local player
					//If the current ped isn't close enough none of the others will be
					//Also if we've found both gang members & friends close enough we don't need to search anymore
					IF bGangMembersNearby 
					AND bFriendsNearby
						BREAKLOOP
					ENDIF
				ENDIF
			ENDREPEAT		
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_INTERIOR][CREATOR_TRAILER] PROCESS_CREATOR_TRAILER_ENTRANCE_MENU - bGangMembersNearby: ", bGangMembersNearby, " bFriendsNearby: ", bFriendsNearby)
		#ENDIF
		
		IF bGangMembersNearby
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 1, TRUE)
		ELSE
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 1, FALSE)
		ENDIF
		
		IF bFriendsNearby
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 2, TRUE)
		ELSE
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 2, FALSE)
		ENDIF
		
		//We have to do this here, otherwise we'll end up with the server giving us the incorrect instance
		IF (bGangMembersNearby OR bFriendsNearby)
		AND NOT IS_SIMPLE_INTERIOR_INT_SCRIPT_RUNNING()
			SET_CREATOR_TRAILER_ACCESS_BS(eSimpleInteriorID, -1)
		ENDIF
		
		BOOL bFromBunkerToTruck = IS_PLAYER_NEED_TO_CHECK_FOR_NEARBY_PLAYERS(PLAYER_ID())
		
		IF WAS_SIMPLE_INTERIOR_MENU_OPTION_ACCEPTED(0, menu)
			SET_CREATOR_TRAILER_ACCESS_BS(eSimpleInteriorID, -1)
		ELIF WAS_SIMPLE_INTERIOR_MENU_OPTION_ACCEPTED(1, menu)
			IF bGangMembersNearby
				BROADCAST_CREATOR_TRAILER_EXIT_SPAWN_POINT(TRUE)
				BROADCAST_PLAYERS_WARP_INSIDE_SIMPLE_INTERIOR(PLAYER_ID(), eSimpleInteriorID, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(GET_CREATOR_TRAILER_VEH_INDEX()), GET_ENTITY_HEADING(GET_CREATOR_TRAILER_VEH_INDEX()), <<2,-13, 0>>)
																, FALSE, FALSE, TRUE, bFromBunkerToTruck , globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance)
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_INTERIOR][CREATOR_TRAILER] PROCESS_CREATOR_TRAILER_ENTRANCE_MENU - Not sending broadcast as there's no gang members around entrance.")
				#ENDIF
			ENDIF
			
			TRIGGER_LOCAL_PLAYER_CREATOR_TRAILER_ENTRY(eSimpleInteriorID)
			
		ELIF WAS_SIMPLE_INTERIOR_MENU_OPTION_ACCEPTED(2, menu)
			IF bFriendsNearby
				BROADCAST_CREATOR_TRAILER_EXIT_SPAWN_POINT(TRUE)
				BROADCAST_PLAYERS_WARP_INSIDE_SIMPLE_INTERIOR(PLAYER_ID(), eSimpleInteriorID,GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(GET_CREATOR_TRAILER_VEH_INDEX()), GET_ENTITY_HEADING(GET_CREATOR_TRAILER_VEH_INDEX()), <<2,-13, 0>>)
																, FALSE, FALSE, TRUE, bFromBunkerToTruck  , globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance)
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_INTERIOR][CREATOR_TRAILER] PROCESS_BUNKER_ENTRANCE_MENU - Not sending broadcast as there's no friends and crew around entrance.")
				#ENDIF
			ENDIF
			
			TRIGGER_LOCAL_PLAYER_CREATOR_TRAILER_ENTRY(eSimpleInteriorID)
		ENDIF
	ELSE
		SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 1, FALSE)
		SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 2, FALSE)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_CREATOR_TRAILER(SIMPLE_INTERIORS eSimpleInteriorID, INT iInvitingPlayer = -1)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iInvitingPlayer)
	
	IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.iBS,  BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYERS_OUT_OF_ARMORY_TRUCK_INTERIOR)
		//AND PLAYER_ID() != g_ownerOfArmoryTruckPropertyIAmIn
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
			ELSE	
				PRINTLN("[SIMPLE_INTERIOR][CREATOR_TRAILER] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_CREATOR_TRAILER TRUE all players kicked out")
				RETURN TRUE
			ENDIF	
		ENDIF
		
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
			ELSE	
				PRINTLN("[SIMPLE_INTERIOR][CREATOR_TRAILER] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_CREATOR_TRAILER TRUE owner kicking everyone out")
				RETURN TRUE
			ENDIF	
		ENDIF
		
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_LEAVE_ARMORY_TRUCK_TRAILER_ON_DELIVER)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
			ELSE	
				PRINTLN("[SIMPLE_INTERIOR][CREATOR_TRAILER] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_ARMORY_TRUCK TRUE remove all including owner on delivery")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(VECTOR &vSpawnPoint, INT iIndex)
	FLOAT fDist, fSpawnPointsDist
	fSpawnPointsDist = 3.5
	VECTOR vCentre
	FLOAT fAngle
	
	IF IS_NET_PLAYER_OK(g_ownerOfArmoryTruckPropertyIAmIn)
	
		vCentre = GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint

		IF (iIndex = 0)
			vSpawnPoint =  vCentre	
		ELSE
			fAngle = (TO_FLOAT(iIndex) * 90.0)										
			SWITCH iIndex
				CASE 1
				CASE 2
				CASE 3
				CASE 4
					fAngle += 22.5
					fDist = fSpawnPointsDist * 1.0
				BREAK
				CASE 5
				CASE 6
				CASE 7
				CASE 8
					fAngle += 67.5
					fDist = fSpawnPointsDist * (1.4142 * 1.0)
				BREAK
				CASE 9
				CASE 10
				CASE 11
				CASE 12
					fAngle += 22.5
					fDist = fSpawnPointsDist * 2.0
				BREAK
				CASE 13
				CASE 14
				CASE 15
				CASE 16
					fAngle += 67.5
					fDist = fSpawnPointsDist * (1.4142 * 2.0)
				BREAK
			ENDSWITCH
			
			vSpawnPoint.x = (SIN((fAngle ))) * fDist
			vSpawnPoint.y = (COS((fAngle ))) * fDist
			vSpawnPoint.z = 0.0
			
			vSpawnPoint += vCentre
			vSpawnPoint.z += 1.0						
		ENDIF
		
		IF NOT GET_GROUND_Z_FOR_3D_COORD(vSpawnPoint, vSpawnPoint.z)
			vSpawnPoint.z = vCentre.z
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_CREATOR_TRAILER_KICK_OUT_REASON(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, TEXT_LABEL_31 &txtPlayerNameString, INT iInvitingPlayer = -1)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iInvitingPlayer)
	UNUSED_PARAMETER(txtExtraString)
	UNUSED_PARAMETER(txtPlayerNameString)
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_CREATOR_TRAILER_ACCESS_BS_OWNER_LEFT_GAME)
		RETURN "MP_TRUCK_KICKi"
	ENDIF
	

	RETURN ""
ENDFUNC

FUNC BOOL GET_CREATOR_TRAILER_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bSpawnInVehicle  , BOOL bUseSecondaryInteriorDetails = FALSE )
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	UNUSED_PARAMETER(bSpawnInVehicle)

	//Assign Default Positions, Truck section 3 middle line.
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_CREATOR_TRAILER_1
			SWITCH iSpawnPoint
			CASE 0
			    vSpawnPoint = <<1.0796, 7.4929, 0.0124>>
			    fSpawnHeading = 171.0729
			    
			BREAK
			CASE 1
			    vSpawnPoint = <<0.7036, 6.0105, 0.0501>>
			    fSpawnHeading = 174.0729
			    
			BREAK
			CASE 2
			    vSpawnPoint = <<-0.2024, 4.8689, 0.0124>>
			    fSpawnHeading = 184.0729
			    
			BREAK
			CASE 3
			    vSpawnPoint = <<-1.0444, 7.2759, 0.0124>>
			    fSpawnHeading = 180.0729
			    
			BREAK
			CASE 4
			    vSpawnPoint = <<-0.1373, 4.0171, 0.0124>>
			    fSpawnHeading = 181.0729
			    
			BREAK
			CASE 5
			    vSpawnPoint = <<0.0206, 6.9861, 0.0124>>
			    fSpawnHeading = 184.0729
			    
			BREAK
			CASE 6
			    vSpawnPoint = <<-0.8264, 5.8521, 0.0124>>
			    fSpawnHeading = 184.0729
			BREAK
			CASE 7
			    vSpawnPoint = <<0.1387, 8.3999, 0.0124>>
			    fSpawnHeading = 184.0729
			BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_CREATOR_TRAILER_1
			IF g_ownerOfArmoryTruckPropertyIAmIn = INVALID_PLAYER_INDEX()
				PRINTLN("[SIMPLE_INTERIOR][CREATOR_TRAILER] GET_CREATOR_TRAILER_SPAWN_POINT - owner is invalid, assigning default.")
				//Just  , already assigned default.
				RETURN TRUE
			ELSE
				PRINTLN("[SIMPLE_INTERIOR][CREATOR_TRAILER] GET_CREATOR_TRAILER_SPAWN_POINT - owner is valid, picking a location based on setup. owner Name: ", GET_PLAYER_NAME(g_ownerOfArmoryTruckPropertyIAmIn))
			IF !bSpawnInVehicle
				IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, g_ownerOfArmoryTruckPropertyIAmIn) = AT_ST_EMPTY_DOUBLE
					SWITCH iSpawnPoint
						CASE 0
						    vSpawnPoint = <<1.0796, 7.7930, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 1
						    vSpawnPoint = <<0.9036, 6.0105, 0.0501>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 2
						    vSpawnPoint = <<-1.1024, 4.4690, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 3
						    vSpawnPoint = <<-1.1444, 7.2759, 0.0124>>
						    fSpawnHeading = 180.0729
						    RETURN TRUE
						BREAK
						CASE 4
						    vSpawnPoint = <<0.6626, 2.6169, 0.0124>>
						    fSpawnHeading = 181.0729
						    RETURN TRUE
						BREAK
						CASE 5
						    vSpawnPoint = <<0.9207, 3.9861, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 6
						    vSpawnPoint = <<-0.8264, 5.8521, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 7
						    vSpawnPoint = <<0.1387, 8.3999, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK

					ENDSWITCH
				ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, g_ownerOfArmoryTruckPropertyIAmIn) = AT_ST_CARMOD
					SWITCH iSpawnPoint
						CASE 0
						    vSpawnPoint = <<2.3196, 6.7981, 0.0124>>
						    fSpawnHeading = 179.0729
						    RETURN TRUE
						BREAK
						CASE 1
						    vSpawnPoint = <<-1.3563, -0.2839, 0.0501>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 2
						    vSpawnPoint = <<1.0366, -1.8259, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 3
						    vSpawnPoint = <<-2.1044, 6.2808, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 4
						    vSpawnPoint = <<1.1027, 8.5220, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 5
						    vSpawnPoint = <<1.6606, -0.5081, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 6
						    vSpawnPoint = <<-1.2874, -1.9431, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 7
						    vSpawnPoint = <<-0.7214, 8.3049, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
					ENDSWITCH
				ELSE
					SWITCH GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, g_ownerOfArmoryTruckPropertyIAmIn)
						CASE AT_ST_LIVING_ROOM
							SWITCH iSpawnPoint
								CASE 0
								    vSpawnPoint = <<0.0338, 9.7378, 0.0013>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 1
								    vSpawnPoint = <<0.0186, 8.2380, 0.0013>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 2
								    vSpawnPoint = <<-0.5964, 7.0381, 0.0013>>
								    fSpawnHeading = 194.5643
								    RETURN TRUE
								BREAK
								CASE 3
								    vSpawnPoint = <<0.4877, 6.5381, 0.0013>>
								    fSpawnHeading = 164.5643
								    RETURN TRUE
								BREAK
								CASE 4
								    vSpawnPoint = <<-0.0272, 5.3379, 0.0013>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 5
								    vSpawnPoint = <<-0.0425, 4.3379, 0.0013>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 6
								    vSpawnPoint = <<-0.0577, 3.3379, 0.0013>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
							ENDSWITCH
						BREAK
						CASE AT_ST_GUNMOD
							SWITCH iSpawnPoint
								CASE 0
								    vSpawnPoint = <<0.0326, 8.8320, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 1
								    vSpawnPoint = <<0.9302, 7.6123, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 2
								    vSpawnPoint = <<-0.5931, 6.1360, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 3
								    vSpawnPoint = <<0.9066, 6.1125, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 4
								    vSpawnPoint = <<-0.6167, 4.6362, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 5
								    vSpawnPoint = <<0.8831, 4.6128, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 6
								    vSpawnPoint = <<-0.6403, 3.1365, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 7
								    vSpawnPoint = <<0.8595, 3.1130, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
							ENDSWITCH
						BREAK
						CASE AT_ST_EMPTY_SINGLE
							SWITCH iSpawnPoint
								CASE 0
								    vSpawnPoint = <<-0.9473, 9.3569, 0.0023>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 1
								    vSpawnPoint = <<1.0527, 9.3418, 0.0023>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 2
								    vSpawnPoint = <<-0.9625, 7.3569, 0.0023>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 3
								    vSpawnPoint = <<1.0375, 7.3418, 0.0023>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 4
								    vSpawnPoint = <<-0.9778, 5.6570, 0.0023>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 5
								    vSpawnPoint = <<1.0222, 5.6421, 0.0023>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 6
								    vSpawnPoint = <<-0.9930, 3.9570, 0.0023>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 7
								    vSpawnPoint = <<1.0070, 3.9419, 0.0023>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
							ENDSWITCH	
						BREAK
						CASE AT_ST_COMMAND_CENTER
							SWITCH iSpawnPoint
								CASE 0
								    vSpawnPoint = <<1.0796, 7.4929, 0.0124>>
								    fSpawnHeading = 171.0729
								    RETURN TRUE
								BREAK
								CASE 1
								    vSpawnPoint = <<0.7036, 6.0105, 0.0501>>
								    fSpawnHeading = 174.0729
								    RETURN TRUE
								BREAK
								CASE 2
								    vSpawnPoint = <<-0.2024, 4.8689, 0.0124>>
								    fSpawnHeading = 184.0729
								    RETURN TRUE
								BREAK
								CASE 3
								    vSpawnPoint = <<-1.0444, 7.2759, 0.0124>>
								    fSpawnHeading = 180.0729
								    RETURN TRUE
								BREAK
								CASE 4
								    vSpawnPoint = <<-0.1373, 4.0171, 0.0124>>
								    fSpawnHeading = 181.0729
								    RETURN TRUE
								BREAK
								CASE 5
								    vSpawnPoint = <<0.0206, 6.9861, 0.0124>>
								    fSpawnHeading = 184.0729
								    RETURN TRUE
								BREAK
								CASE 6
								    vSpawnPoint = <<-0.8264, 5.8521, 0.0124>>
								    fSpawnHeading = 184.0729
								    RETURN TRUE
								BREAK
								CASE 7
								    vSpawnPoint = <<0.1387, 8.3999, 0.0124>>
								    fSpawnHeading = 184.0729
								    RETURN TRUE
								BREAK
							ENDSWITCH
						BREAK
						CASE AT_ST_VEHICLE_STORAGE
							SWITCH iSpawnPoint
								CASE 0
								    vSpawnPoint = <<-1.4929, 9.4475, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 1
								    vSpawnPoint = <<1.6067, 9.3989, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 2
								    vSpawnPoint = <<-1.5415, 7.5481, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 3
								    vSpawnPoint = <<1.5581, 7.3989, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 4
								    vSpawnPoint = <<-1.5901, 5.8479, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 5
								    vSpawnPoint = <<1.5095, 5.8000, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 6
								    vSpawnPoint = <<-1.6387, 4.3491, 0.0013>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 7
								    vSpawnPoint = <<1.4609, 4.2000, 0.0013>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
							ENDSWITCH
						BREAK

						DEFAULT
							SWITCH iSpawnPoint
								CASE 0
								    vSpawnPoint = <<0.6499, -0.5879, 0.0013>>
								    fSpawnHeading = 188.2607
								    RETURN TRUE
								BREAK
								CASE 1
								    vSpawnPoint = <<-0.1263, -2.4700, 0.0501>>
								    fSpawnHeading = 188.2607
								    RETURN TRUE
								BREAK
								CASE 2
								    vSpawnPoint = <<-0.0323, -8.5120, 0.0013>>
								    fSpawnHeading = 188.2607
								    RETURN TRUE
								BREAK
								CASE 3
								    vSpawnPoint = <<-0.0743, -7.1050, 0.0013>>
								    fSpawnHeading = 188.2607
								    RETURN TRUE
								BREAK
								CASE 4
								    vSpawnPoint = <<-0.6674, -1.7639, 0.0013>>
								    fSpawnHeading = 310.3200
								    RETURN TRUE
								BREAK
								CASE 5
								    vSpawnPoint = <<-0.1094, -10.1951, 0.0501>>
								    fSpawnHeading = 351.7200
								    RETURN TRUE
								BREAK
								CASE 6
								    vSpawnPoint = <<0.1437, -3.9290, 0.0013>>
								    fSpawnHeading = 0.0000
								    RETURN TRUE
								BREAK
								CASE 7
								    vSpawnPoint = <<-0.1914, 0.6189, 0.0013>>
								    fSpawnHeading = 188.2607
								    RETURN TRUE
								BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				ENDIF	
			ELSE
				IF !IS_PLAYER_PURCHASED_ANY_VEHICLE_STORAGE_SECTION_FOR_TRUCK(g_ownerOfArmoryTruckPropertyIAmIn)
					SWITCH iSpawnPoint
						CASE 0
						   vSpawnPoint = <<0.0967, 2.9719, -0.0>>
							fSpawnHeading = 180.0
							RETURN TRUE
						BREAK
					ENDSWITCH
				ELSE
					SWITCH iSpawnPoint
						CASE 0
						   vSpawnPoint = <<-0.1050, 7.1421, 0.0000>>
							fSpawnHeading = 180.0
							RETURN TRUE
						BREAK
						CASE 1
							vSpawnPoint = <<1103.5580, -2993.8879, -38.8634>>
							fSpawnHeading = 180.0000
//					    vSpawnPoint = <<-0.0044, 6.1121, 1.1366>>
//    					fSpawnHeading = 180.0000
   							RETURN FALSE //so this is not considered for normal vehicle spawning
						BREAK
					ENDSWITCH
				ENDIF
//				SWITCH iSpawnPoint
//					CASE 0
//					   vSpawnPoint = <<0.0967, 2.9719, -0.0>>
//						fSpawnHeading = 180.0
//						RETURN TRUE
//					BREAK
//					CASE 1
//						vSpawnPoint = <<1103.5580, -2993.8879, -38.8634>>
//						fSpawnHeading = 180.0000
////					    vSpawnPoint = <<-0.0044, 6.1121, 1.1366>>
////    					fSpawnHeading = 180.0000
//   						RETURN FALSE //so this is not considered for normal vehicle spawning
//					BREAK
//				ENDSWITCH	
			ENDIF
			ENDIF
		BREAK
	ENDSWITCH
		
	RETURN FALSE
ENDFUNC

FUNC SIMPLE_INTERIOR_CUSTOM_INTERIOR_WARP_PARAMS GET_CREATOR_TRAILER_CUSTOM_INTERIOR_WARP_PARAMS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	SIMPLE_INTERIOR_CUSTOM_INTERIOR_WARP_PARAMS eParams
	
	eParams.bIgnoreObstructions 	= FALSE
	eParams.bIgnoreDistanceChecks 	= FALSE
	
	RETURN eParams
ENDFUNC

FUNC BOOL GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bSpawnInVehicle)
	UNUSED_PARAMETER(bSpawnInVehicle)

	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_CREATOR_TRAILER_1
		    SWITCH iSpawnPoint
		        CASE 0
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(vSpawnPoint, 0)
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 0 -  vSpawnPoint: ", vSpawnPoint)				
						fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 0 - fSpawnHeading: ", fSpawnHeading)
						RETURN TRUE
					ENDIF	
		        BREAK
				CASE 1
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(vSpawnPoint, 1)
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 1 -  vSpawnPoint: ", vSpawnPoint)				
						fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 1 - fSpawnHeading: ", fSpawnHeading)
						RETURN TRUE
					ENDIF	
		        BREAK
				CASE 2
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(vSpawnPoint, 2)
						//vSpawnPoint = GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint 
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 2 -  vSpawnPoint: ", vSpawnPoint)				
						fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 2 - fSpawnHeading: ", fSpawnHeading)
						RETURN TRUE
					ENDIF	
		        BREAK
				CASE 3
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(vSpawnPoint, 3)
						//vSpawnPoint = GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint 
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 3 -  vSpawnPoint: ", vSpawnPoint)				
						fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 3 - fSpawnHeading: ", fSpawnHeading)
						RETURN TRUE
					ENDIF	
		        BREAK
				CASE 4
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(vSpawnPoint, 4)
						//vSpawnPoint = GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint 
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 4 -  vSpawnPoint: ", vSpawnPoint)				
						fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 4 - fSpawnHeading: ", fSpawnHeading)
						RETURN TRUE
					ENDIF	
		        BREAK
				CASE 5
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(vSpawnPoint, 5)
						//vSpawnPoint = GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint 
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 5 -  vSpawnPoint: ", vSpawnPoint)				
						fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 5 - fSpawnHeading: ", fSpawnHeading)
						RETURN TRUE
					ENDIF	
		        BREAK
				CASE 6
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(vSpawnPoint, 6)
						//vSpawnPoint = GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint 
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 6 -  vSpawnPoint: ", vSpawnPoint)				
						fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 6 - fSpawnHeading: ", fSpawnHeading)
						RETURN TRUE
					ENDIF	
		        BREAK
				CASE 7
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(vSpawnPoint, 0)
						//vSpawnPoint = GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint 
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 7 -  vSpawnPoint: ", vSpawnPoint)				
						fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 7 - fSpawnHeading: ", fSpawnHeading)
						RETURN TRUE
					ENDIF	
		        BREAK
				CASE 8
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(vSpawnPoint, 8)
						//vSpawnPoint = GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint 
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 8 -  vSpawnPoint: ", vSpawnPoint)				
						fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 8 - fSpawnHeading: ", fSpawnHeading)
						RETURN TRUE
					ENDIF	
		        BREAK
				CASE 9
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(vSpawnPoint, 9)
						//vSpawnPoint = GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint 
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 9 -  vSpawnPoint: ", vSpawnPoint)				
						fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 9 - fSpawnHeading: ", fSpawnHeading)
						RETURN TRUE
					ENDIF	
		        BREAK
				CASE 10
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(vSpawnPoint, 10)
						//vSpawnPoint = GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint 
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 10 -  vSpawnPoint: ", vSpawnPoint)				
						fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 10 - fSpawnHeading: ", fSpawnHeading)
						RETURN TRUE
					ENDIF	
		        BREAK
				CASE 11
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(vSpawnPoint, 11)
						//vSpawnPoint = GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint 
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 11 -  vSpawnPoint: ", vSpawnPoint)				
						fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 11 - fSpawnHeading: ", fSpawnHeading)
						RETURN TRUE
					ENDIF	
		        BREAK
				CASE 12
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(vSpawnPoint, 12)
						//vSpawnPoint = GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint 
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 12 -  vSpawnPoint: ", vSpawnPoint)				
						fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 0 - fSpawnHeading: ", fSpawnHeading)
						RETURN TRUE
					ENDIF	
		        BREAK
				CASE 13
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(vSpawnPoint, 13)
						//vSpawnPoint = GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint 
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 13 -  vSpawnPoint: ", vSpawnPoint)				
						fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 13 - fSpawnHeading: ", fSpawnHeading)
						RETURN TRUE
					ENDIF	
		        BREAK
				CASE 14
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(vSpawnPoint, 14)
						//vSpawnPoint = GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint 
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 14 -  vSpawnPoint: ", vSpawnPoint)				
						fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT 14 - fSpawnHeading: ", fSpawnHeading)
						RETURN TRUE
					ENDIF	
		        BREAK
				CASE 15
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(vSpawnPoint, 15)				
						fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180	
						RETURN TRUE
					ENDIF	
		        BREAK
				CASE 16
					IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						GET_CREATOR_TRAILER_CUSTOM_SPAWN_POINTS(vSpawnPoint, 16)				
						fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
						RETURN TRUE
					ENDIF	
		        BREAK
		    ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC ASYNC_GRID_SPAWN_LOCATIONS GET_CREATOR_TRAILER_ASYNC_SPAWN_GRID_LOCATION(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN GRID_SPAWN_LOCATION_INVALID
ENDFUNC

FUNC ASYNC_GRID_SPAWN_LOCATIONS GET_CREATOR_TRAILER_ASYNC_SPAWN_GRID_INSIDE_LOCATION(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN GRID_SPAWN_LOCATION_INVALID
ENDFUNC

FUNC VECTOR GET_CREATOR_TRAILER_TELEPORT_IN_SVM_COORD(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC GET_CREATOR_TRAILER_EXIT_MENU_TITLE_AND_OPTIONS(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], INT &iOptionsCount, INT iCurrentExit)
	UNUSED_PARAMETER(iCurrentExit)
	IF COUNT_OF(strOptions) != SIMPLE_INTERIOR_MENU_MAX_OPTIONS
		CASSERTLN(DEBUG_PROPERTY, "GET_CLUBHOUSE_EXIT_MENU_TITLE_AND_OPTIONS - options array has wrong size.")
		EXIT
	ENDIF
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_CREATOR_TRAILER_1
			txtTitle = "ARMORY_TITLE"
		BREAK
		DEFAULT
			txtTitle = "ARMORY_TITLE"
		BREAK
	ENDSWITCH
	
	iOptionsCount = 2
	strOptions[0] = "GR_TRUCK_EX1"
	
	IF g_SimpleInteriorData.bTruckCabOwnerAccess
		strOptions[1] = "GR_TRUCK_EX3"
	ELSE
		strOptions[1] = "GR_TRUCK_EX2"
	ENDIF
ENDPROC

FUNC BOOL CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_CREATOR_TRAILER(SIMPLE_INTERIORS eSimpleInteriorID, INT iExitId)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iExitId)
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_CREATOR_TRAILER_ACCESS_BS_OWNER)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CREATOR_TRAILER_MAX_BLIPS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN MAX_CREATOR_TRAILER_BLIPS
ENDFUNC
   
FUNC BOOL SHOULD_CREATOR_TRAILER_BE_BLIPPED_THIS_FRAME(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex)
	UNUSED_PARAMETER(iBlipIndex)
	
	IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF GET_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN() = eSimpleInteriorID
		RETURN FALSE
	ENDIF
	
	
	// ALWAYS RETURN FALSE FOR NOW UNTIL WE HAVE DECISION ON DESIGN !!!!!
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CREATOR_TRAILER_BLIP_SHORT_RANGE(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	RETURN TRUE
ENDFUNC

FUNC BLIP_SPRITE GET_CREATOR_TRAILER_BLIP_SPRITE(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	RETURN RADAR_TRACE_WAREHOUSE
ENDFUNC

FUNC VECTOR GET_CREATOR_TRAILER_BLIP_COORDS(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(iBlipIndex)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_CREATOR_TRAILER_1 
			IF DOES_ENTITY_EXIST(GET_CREATOR_TRAILER_VEH_INDEX())
			AND NOT IS_ENTITY_DEAD(GET_CREATOR_TRAILER_VEH_INDEX())
				IF GET_ENTITY_MODEL(GET_CREATOR_TRAILER_VEH_INDEX()) = TRAILERLARGE
					RETURN GET_ENTITY_COORDS(GET_CREATOR_TRAILER_VEH_INDEX())
				ENDIF
			ENDIF
		BREAK	
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BLIP_PRIORITY GET_CREATOR_TRAILER_BLIP_PRIORITY(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	RETURN BLIPPRIORITY_MED
ENDFUNC

PROC MAINTAIN_CREATOR_TRAILER_BLIP_EXTRA_FUNCTIONALITY(SIMPLE_INTERIORS eSimpleInteriorID, BLIP_INDEX &blipIndex, BLIP_INDEX &OverlayBlipIndex, INT &iBlipType, INT &iBlipNameHash, INT iBlipIndex)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(blipIndex)
	UNUSED_PARAMETER(iBlipType)
	UNUSED_PARAMETER(iBlipNameHash)
	UNUSED_PARAMETER(OverlayBlipIndex)
	UNUSED_PARAMETER(iBlipIndex)
ENDPROC

FUNC INT GET_CREATOR_TRAILER_BLIP_COLOUR(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	RETURN BLIP_COLOUR_PINK
ENDFUNC

FUNC VECTOR GET_CREATOR_TRAILER_MAP_MIDPOINT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)

	IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
		IF g_ownerOfArmoryTruckPropertyIAmIn != PLAYER_ID()
			RETURN GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint
		ELSE
			IF DOES_ENTITY_EXIST(GET_CREATOR_TRAILER_VEH_INDEX())
			AND NOT IS_ENTITY_DEAD(GET_CREATOR_TRAILER_VEH_INDEX())
				RETURN GET_ENTITY_COORDS(GET_CREATOR_TRAILER_VEH_INDEX())
			ELSE
				RETURN GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint
			ENDIF	
		ENDIF	
	ELSE
		IF DOES_ENTITY_EXIST(GET_CREATOR_TRAILER_VEH_INDEX())
		AND NOT IS_ENTITY_DEAD(GET_CREATOR_TRAILER_VEH_INDEX())
			RETURN GET_ENTITY_COORDS(GET_CREATOR_TRAILER_VEH_INDEX())
		ELSE
			IF GET_OWNER_OF_CREATOR_TRAILER(GET_CREATOR_TRAILER_VEH_INDEX()) != INVALID_PLAYER_INDEX()
				RETURN GlobalplayerBD[NATIVE_TO_INT(GET_OWNER_OF_CREATOR_TRAILER(GET_CREATOR_TRAILER_VEH_INDEX()))].SimpleInteriorBD.vMobileInteriorExitSpawnPoint
			ENDIF	
		ENDIF
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC MAINTAIN_CREATOR_TRAILER_OUTSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_EXT_SCRIPT_STATE eState, BOOL &bExtScriptMustRun)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(eState)
	UNUSED_PARAMETER(bExtScriptMustRun)
	UNUSED_PARAMETER(details)
ENDPROC

PROC RESET_CREATOR_TRAILER_OUTSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(details)
	UNUSED_PARAMETER(eSimpleInteriorID)
ENDPROC

PROC MAINTAIN_CREATOR_TRAILER_INSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(details)
ENDPROC

PROC RESET_CREATOR_TRAILER_INSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(details)
	UNUSED_PARAMETER(eSimpleInteriorID)
ENDPROC

FUNC BOOL DOES_CREATOR_TRAILER_BLOCK_WEAPONS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_CREATOR_TRAILER_RESTRICT_PLAYER_MOVEMENT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN TRUE
ENDFUNC

PROC GET_CREATOR_TRAILER_CAR_GENS_BLOCK_AREA(SIMPLE_INTERIORS eSimpleInteriorID, BOOL &bDoBlockCarGen, VECTOR &vMin, VECTOR &vMax)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(vMin)
	UNUSED_PARAMETER(vMax)
	bDoBlockCarGen = FALSE
ENDPROC

FUNC BOOL GET_CREATOR_TRAILER_OUTSIDE_MODEL_HIDE(SIMPLE_INTERIORS eSimpleInteriorID, INT iIndex, VECTOR &vPos, FLOAT &fRadius, FLOAT &fActivationRadius, MODEL_NAMES &eModel, BOOL &bSurviveMapReload)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iIndex)
	UNUSED_PARAMETER(vPos)
	UNUSED_PARAMETER(fRadius)
	UNUSED_PARAMETER(fActivationRadius)
	UNUSED_PARAMETER(eModel)
	UNUSED_PARAMETER(bSurviveMapReload)
	RETURN FALSE
ENDFUNC

PROC GET_CREATOR_TRAILER_SCENARIO_PED_REMOVAL_SPHERE(SIMPLE_INTERIORS eSimpleInteriorID, BOOL &bDoRemoveScenarioPeds, VECTOR &vCoords, FLOAT &fRadius)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(vCoords)
	UNUSED_PARAMETER(fRadius)
	bDoRemoveScenarioPeds = FALSE
ENDPROC

FUNC BOOL SHOULD_TRIGGER_EXIT_IN_CAR_FOR_CREATOR_TRAILER(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX interiorOwner, BOOL bOwnerDrivingOut)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(interiorOwner)
	UNUSED_PARAMETER(bOwnerDrivingOut)
	RETURN FALSE
ENDFUNC

FUNC INT GET_CREATOR_TRAILER_ENTRANCES_COUNT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN 2
ENDFUNC

FUNC INT GET_CREATOR_TRAILER_INVITE_ENTRY_POINT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN -1
ENDFUNC

PROC GET_CREATOR_TRAILER_ENTRANCE_DATA(SIMPLE_INTERIORS eSimpleInteriorID, INT iEntranceID, SIMPLE_INTERIOR_ENTRANCE_DATA &data)
	SWITCH iEntranceID
		CASE 0
			GET_CREATOR_TRAILER_ENTRY_LOCATE(eSimpleInteriorID, data.vPosition)
			
			data.vLocatePos1 = <<1, 1, 2>>
			
			SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_DRAWS_LOCATE)
			SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_ON_FOOT_ONLY)
			SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IS_DYNAMIC)
			GET_HUD_COLOUR(HUD_COLOUR_BLUE, data.iLocateColourR, data.iLocateColourG, data.iLocateColourB, data.iLocateColourA)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_PLAYER_INSIDE_CREATOR_TRAILER_ANGLED_AREA()
	IF DOES_ENTITY_EXIST(GET_CREATOR_TRAILER_VEH_INDEX())
	AND NOT IS_ENTITY_DEAD(GET_CREATOR_TRAILER_VEH_INDEX())
		IF GET_ENTITY_MODEL(GET_CREATOR_TRAILER_VEH_INDEX()) = TRAILERLARGE

			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID()
				, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(GET_CREATOR_TRAILER_VEH_INDEX()), GET_ENTITY_HEADING(GET_CREATOR_TRAILER_VEH_INDEX()), <<0.0,6,-1.5>>)
				, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(GET_CREATOR_TRAILER_VEH_INDEX()), GET_ENTITY_HEADING(GET_CREATOR_TRAILER_VEH_INDEX()), <<0.0,-5.9,2.5>>)
				, GET_MODEL_WIDTH(TRAILERLARGE))
					RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF	
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_FACING_CREATOR_TRAILER_DOOR(VECTOR vLookAt)
	VECTOR vToTrail = NORMALISE_VECTOR((vLookAt - GET_ENTITY_COORDS(PLAYER_PED_ID())))
	FLOAT fDot = DOT_PRODUCT(GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID()), vToTrail)
	
	RETURN fDot >= 0
ENDFUNC


FUNC BOOL IS_CREATOR_TRAILER_SPECIAL_CUT_SCENE_ASSETS_READY(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS &details)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(details)
	RETURN FALSE
ENDFUNC

PROC GET_CREATOR_TRAILER_ENTRANCE_SCENE_DETAILS(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS& details, INT iEntranceID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iEntranceID)
	UNUSED_PARAMETER(details)
ENDPROC

PROC MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_CREATOR_TRAILER_ACCESS(SIMPLE_INTERIOR_DETAILS &details)
	UNUSED_PARAMETER(details)
ENDPROC	

FUNC SIMPLE_INTERIORS GET_OWNED_SMPL_INT_OF_TYPE_CREATOR_TRAILER(PLAYER_INDEX playerID, INT iPropertySlot, INT iFactoryType)
	UNUSED_PARAMETER(playerID)
	UNUSED_PARAMETER(iFactoryType)
	UNUSED_PARAMETER(iPropertySlot)
	RETURN SIMPLE_INTERIOR_INVALID
ENDFUNC

PROC CREATOR_TRAILER_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX veh, BOOL bSetOnGround)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(veh)
	UNUSED_PARAMETER(bSetOnGround)
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Below is the function that builds the interface.
// Because look up table needs to be build on every call as an optimisation
// the function retrieves a pointer to one function at a time so that one call = one lookup
// Functions are grouped thematically.

PROC BUILD_CREATOR_TRAILER_LOOK_UP_TABLE(SIMPLE_INTERIOR_INTERFACE &interface, SIMPLE_INTERIOR_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		//════════════════════════════════════════════════════════════════════
		CASE E_DOES_SIMPLE_INTERIOR_USE_EXTERIOR_SCRIPT
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_CREATOR_TRAILER_USE_EXTERIOR_SCRIPT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION
			interface.getSimpleInteriorInteriorTypeAndPosition = &GET_CREATOR_TRAILER_TYPE_AND_POSITION
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_DETAILS
			interface.getSimpleInteriorDetails = &GET_CREATOR_TRAILER_DETAILS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_DETAILS_BS
			interface.getSimpleInteriorDetailsPropertiesBS = &GET_CREATOR_TRAILER_PROPERTIES_BS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_NAME
			interface.simpleInteriorFP_ReturnS_ParamID = &GET_CREATOR_TRAILER_NAME
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX
			interface.getSimpleInteriorInsideBoundingBox = &GET_CREATOR_TRAILER_INSIDE_BOUNDING_BOX
		BREAK
		CASE E_GET_OWNED_SIMPLE_INTERIOR_OF_TYPE
			interface.getOwnedSimpleInteriorOfType = &GET_OWNED_SMPL_INT_OF_TYPE_CREATOR_TRAILER
		BREAK
		CASE E_SIMPLE_INTERIOR_LOAD_SECONDARY_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamID = &SHOULD_CREATOR_TRAILER_LOAD_SECONDARY_INTERIOR
		BREAK
		//════════════════════════════════════════════════════════════════════
		/*
		CASE E_GET_SIMPLE_INTERIOR_ENTRY_LOCATE
			interface.getSimpleInteriorEntryLocate = &GET_CREATOR_TRAILER_ENTRY_LOCATE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ENTRY_LOCATE_COLOUR
			interface.getSimpleInteriorEntryLocateColour = &GET_CREATOR_TRAILER_ENTRY_LOCATE_COLOUR
		BREAK
		*/
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCES_COUNT
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_CREATOR_TRAILER_ENTRANCES_COUNT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_DATA
			interface.getSimpleInteriorEntranceData = &GET_CREATOR_TRAILER_ENTRANCE_DATA
		BREAK	
		CASE E_GET_SIMPLE_INTERIOR_INVITE_ENTRY_POINT
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_CREATOR_TRAILER_INVITE_ENTRY_POINT
		BREAK			
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_SCENE_DETAILS
			interface.getSimpleInteriorEntranceSceneDetails = &GET_CREATOR_TRAILER_ENTRANCE_SCENE_DETAILS
		BREAK
		CASE E_SHOULD_THIS_SIMPLE_INTERIOR_LOCATE_BE_HIDDEN
			interface.simpleInteriorFP_ReturnB_ParamIDAndDebugB = &SHOULD_THIS_CREATOR_TRAILER_LOCATE_BE_HIDDEN
		BREAK
		CASE E_SHOULD_THIS_SIMPLE_INTERIOR_CORONA_BE_HIDDEN
			interface.simpleInteriorFP_ReturnB_ParamIDAndDebugB = &SHOULD_THIS_CREATOR_TRAILER_CORONA_BE_HIDDEN
		BREAK
		CASE E_CAN_PLAYER_ENTER_SIMPLE_INTERIOR_IN_VEHICLE
			interface.simpleInteriorFP_ReturnB_ParamPIDAndModel = &CAN_PLAYER_ENTER_CREATOR_TRAILER_IN_VEHICLE
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_CAN_PLAYER_ENTER_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPID_ID_AndInt = &CAN_PLAYER_ENTER_CREATOR_TRAILER
		BREAK
		CASE E_CAN_PLAYER_USE_SIMPLE_INTERIOR_ENTRY_IN_CURRENT_VEHICLE
			interface.canPlayerUseEntryInCurrentVeh = &CAN_PLAYER_USE_CREATOR_TRAILER_ENTRY_IN_CURRENT_VEHICLE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_REASON_FOR_BLOCKED_ENTRY
			interface.simpleInteriorFP_ReturnS_ParamID_TL63_AndInt = &GET_CREATOR_TRAILER_REASON_FOR_BLOCKED_ENTRY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_REASON_FOR_BLOCKED_EXIT
			interface.simpleInteriorFP_ReturnS_ParamID_TL63_AndInt = &GET_CREATOR_TRAILER_REASON_FOR_BLOCKED_EXIT
		BREAK
		CASE E_CAN_PLAYER_INVITE_OTHERS_TO_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPIDAndID = &CAN_PLAYER_INVITE_OTHERS_TO_CREATOR_TRAILER
		BREAK
		CASE E_CAN_PLAYER_BE_INVITED_TO_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPIDAndID = &CAN_PLAYER_BE_INVITED_TO_CREATOR_TRAILER
		BREAK
		CASE E_CAN_LOCAL_PLAYER_BE_INVITED_TO_SIMPLE_INTERIOR_BY_PLAYER
			interface.intCanLocalPlayerBeInvitedToSimpleInteriorByPlayer = &CAN_LOCAL_PLAYER_BE_INVITED_TO_CREATOR_TRAILER_BY_PLAYER
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_MENU_TITLE_AND_OPTIONS
			interface.simpleInteriorFPProc_ParamIDAndCustomMenu = &GET_CREATOR_TRAILER_ENTRANCE_MENU_TITLE_AND_OPTIONS
		BREAK
		CASE E_PROCESS_SIMPLE_INTERIOR_ENTRANCE_MENU
			interface.simpleInteriorFPProc_ParamIDAndEMenu = &PROCESS_CREATOR_TRAILER_ENTRANCE_MENU
		BREAK
		CASE E_SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_CREATOR_TRAILER
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_KICK_OUT_REASON
			interface.getSimpleInteriorKickOutReason = &GET_CREATOR_TRAILER_KICK_OUT_REASON
		BREAK
		CASE E_SET_SIMPLE_INTERIOR_ACCESS_BS
			interface.setSimpleInteriorAccessBS = &SET_CREATOR_TRAILER_ACCESS_BS
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_GET_SIMPLE_INTERIOR_SPAWN_POINT
			interface.getSimpleInteriorSpawnPoint = &GET_CREATOR_TRAILER_SPAWN_POINT
		BREAK
		CASE E_GET_SI_CUSTOM_INTERIOR_WARP_PARAMS
			interface.getSimpleInteriorCustomInteriorWarpParams = &GET_CREATOR_TRAILER_CUSTOM_INTERIOR_WARP_PARAMS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_OUTSIDE_SPAWN_POINT
			interface.getSimpleInteriorOutsideSpawnPoint = &GET_CREATOR_TRAILER_OUTSIDE_SPAWN_POINT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ASYNC_SPAWN_GRID_LOCATION
			interface.simpleInteriorFP_ReturnEGW_ParamID = &GET_CREATOR_TRAILER_ASYNC_SPAWN_GRID_LOCATION
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ASYNC_SPAWN_GRID_INSIDE_LOCATION
			interface.simpleInteriorFP_ReturnEGW_ParamID = &GET_CREATOR_TRAILER_ASYNC_SPAWN_GRID_INSIDE_LOCATION
		BREAK
		CASE E_MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_ACCESS
			interface.maintainLeaveentryVehIfNoAccess = &MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_CREATOR_TRAILER_ACCESS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_TELE_IN_SVM_COORD
			interface.simpleInteriorFP_ReturnV_ParamID = &GET_CREATOR_TRAILER_TELEPORT_IN_SVM_COORD
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_GET_SIMPLE_INTERIOR_EXIT_MENU_TITLE_AND_OPTIONS
			interface.getSimpleInteriorExitMenuTitleAndOptions = &GET_CREATOR_TRAILER_EXIT_MENU_TITLE_AND_OPTIONS
		BREAK
		CASE E_CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_CREATOR_TRAILER
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_GET_SIMPLE_INTERIOR_MAX_BLIPS
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_CREATOR_TRAILER_MAX_BLIPS
		BREAK
		CASE E_SHOULD_SIMPLE_INTERIOR_BE_BLIPPED_THIS_FRAME
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &SHOULD_CREATOR_TRAILER_BE_BLIPPED_THIS_FRAME
		BREAK
		CASE E_IS_SIMPLE_INTERIOR_BLIP_SHORT_RANGE
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &IS_CREATOR_TRAILER_BLIP_SHORT_RANGE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_SPRITE
			interface.simpleInteriorFP_ReturnEBS_ParamIDAndInt = &GET_CREATOR_TRAILER_BLIP_SPRITE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_COORDS
			interface.simpleInteriorFP_ReturnV_ParamIDAndInt = &GET_CREATOR_TRAILER_BLIP_COORDS
		BREAK
		CASE E_MAINTAIN_SIMPLE_INTERIOR_BLIP_EXTRA_FUNCTIONALITY
			interface.maintainSimpleInteriorBlipExtraFunctionality = &MAINTAIN_CREATOR_TRAILER_BLIP_EXTRA_FUNCTIONALITY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_PRIORITY
			interface.getSimpleInteriorBlipPriority	= &GET_CREATOR_TRAILER_BLIP_PRIORITY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_COLOUR
			interface.simpleInteriorFP_ReturnI_ParamIDAndInt = &GET_CREATOR_TRAILER_BLIP_COLOUR
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_MAP_MIDPOINT
			interface.simpleInteriorFP_ReturnV_ParamID = &GET_CREATOR_TRAILER_MAP_MIDPOINT
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_MAINTAIN_SIMPLE_INTERIOR_OUTSIDE_GAMEPLAY_MODIFIERS
			interface.maintainSimpleInteriorOutsideGameplayModifiers = &MAINTAIN_CREATOR_TRAILER_OUTSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_RESET_SIMPLE_INTERIOR_OUTSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &RESET_CREATOR_TRAILER_OUTSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_MAINTAIN_SIMPLE_INTERIOR_INSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &MAINTAIN_CREATOR_TRAILER_INSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_RESET_SIMPLE_INTERIOR_INSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &RESET_CREATOR_TRAILER_INSIDE_GAMEPLAY_MODIFIERS
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_DOES_SIMPLE_INTERIOR_BLOCK_WEAPONS
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_CREATOR_TRAILER_BLOCK_WEAPONS
		BREAK
		CASE E_DOES_SIMPLE_INTERIOR_RESTRICT_PLAYER_MOVEMENT
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_CREATOR_TRAILER_RESTRICT_PLAYER_MOVEMENT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_CAR_GENS_BLOCK_AREA
			interface.getSimpleInteriorCarGensBlockArea = &GET_CREATOR_TRAILER_CAR_GENS_BLOCK_AREA
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_OUTSIDE_MODEL_HIDE
			interface.getSimpleInteriorOutsideModelHide = &GET_CREATOR_TRAILER_OUTSIDE_MODEL_HIDE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_SCENARIO_PED_REMOVAL_SPHERE
			interface.getSimpleInteriorScenarioPedRemovalSphere = &GET_CREATOR_TRAILER_SCENARIO_PED_REMOVAL_SPHERE
		BREAK
		CASE E_SHOULD_SIMPLE_INTERIOR_TRIGGER_EXIT_IN_CAR
			interface.simpleInteriorFP_ReturnB_ParamID_PID_AndB = &SHOULD_TRIGGER_EXIT_IN_CAR_FOR_CREATOR_TRAILER
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_IS_SPECIAL_CUT_SCENE_ASSETS_READY
			interface.isSpecialCutSceneAssetsReady = &IS_CREATOR_TRAILER_SPECIAL_CUT_SCENE_ASSETS_READY
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_SML_INT_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT
			interface.DoCustomActionForStateWarpToSpawnPoint = &CREATOR_TRAILER_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT
		BREAK
	ENDSWITCH
ENDPROC
