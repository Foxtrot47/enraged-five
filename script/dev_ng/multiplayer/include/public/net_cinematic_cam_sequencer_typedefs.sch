USING "cinematic_cam_behaviour.sch"

CONST_INT ciCINEMATIC_CAM_SEQUENCE__MAX_DYNAMIC_CAMS 30

#IF IS_DEBUG_BUILD


STRUCT CINEMATIC_CAM_BEHAVIOUR_DEBUG
	CINEMATIC_CAM_BEHAVIOUR_STRUCT sCam	
	BOOL bAdded = FALSE
	BOOL bPreview
	BOOL bRemoveCam	
	BOOL bCopyBasicSettings
	BOOL bCopyStartTransform
	BOOL bCopyEndTransform
	BOOL bCopyStartFov
	BOOL bCopyEndFov
	BOOL bCopyOrbitDistance
	BOOL bCopyOrbitOrigin	
	BOOL bCopyLookatCoord
	BOOL bRemoveBehaviour[COUNT_OF(CINEMATIC_CAM_BEHAVIOUR_TYPE)] 
	
	TEXT_LABEL_63 strName
	INT iBehaviourType
	BOOL bAddBehaviour
	WIDGET_GROUP_ID wgiRootID
	WIDGET_GROUP_ID wgiBehaviourGroupID
ENDSTRUCT

TYPEDEF PROC DANCE_CINEMATIC_CAM_ADD_WIDGET()

STRUCT CINEMATIC_CAM_SEQUENCE_DEBUG
	CINEMATIC_CAM_BEHAVIOUR_DEBUG sDebugCams[ciCINEMATIC_CAM_SEQUENCE__MAX_DYNAMIC_CAMS]
	DANCE_CINEMATIC_CAM_ADD_WIDGET fpCustomWidget
	
	INT iCamCount = 0
	INT iNextID = 0
	BOOL bAddCam
	BOOL bDrawDebug
	BOOL bRebuild = TRUE
	BOOL bResetToScriptCams = TRUE
	BOOL bEdit = FALSE
	BOOL bLoadXML
	BOOL bSaveXML
	TEXT_WIDGET_ID twiXMLSaveName
	TEXT_WIDGET_ID twiXMLLoadName
	BOOL bOutputScript
	INT iPreviewCam = -1
	WIDGET_GROUP_ID wgiRootID
	WIDGET_GROUP_ID wgiCamGroupID
	OBJECT_INDEX objHelper
ENDSTRUCT
#ENDIF

TYPEDEF FUNC CINEMATIC_CAM_BEHAVIOUR_STRUCT GET_CINEMATIC_CAMERA_BEHAVIOUR(INT iBehaviourIndex)

/// PURPOSE: 
///    Struct for holding variables for a specific cinematic instance
STRUCT CINEMATIC_CAM_SEQUENCE_STRUCT

	BOOL bInitialised = FALSE
	BOOL bRandomise = FALSE
	BOOL bUseWeightings = FALSE
	FLOAT fTotalAccumulatedWeights = 0.0

	INT iCurrentCamBehaviourIndex = 0
	
	GET_CINEMATIC_CAMERA_BEHAVIOUR fpCamLookup
	INT iMaxCam // You should not access this directly use _CINEMATIC_CAM_SEQUENCE__GET_MAX_CAMERA so it respects to the debug cam editor
	
	CAMERA_INDEX ciCamera
	CINEMATIC_CAM_BEHAVIOUR_STRUCT sCurrentBehaviour
	
	#IF IS_DEBUG_BUILD
		CINEMATIC_CAM_SEQUENCE_DEBUG sDebug
	#ENDIF
	
ENDSTRUCT

FUNC INT _CINEMATIC_CAM_SEQUENCE__GET_MAX_CAMERA(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	
	#IF IS_DEBUG_BUILD
		IF sInst.sDebug.bEdit
			RETURN sInst.sDebug.iCamCount
		ENDIF
	#ENDIF
	
	RETURN sInst.iMaxCam
	
ENDFUNC

FUNC FLOAT _CINEMATIC_CAM_SEQUENCE__GET_ACCUMULATED_WEIGHTINGS_FOR_CURRENT_CAMERA_LOOKUP(CINEMATIC_CAM_SEQUENCE_STRUCT& sInst)
	
	IF sInst.fpCamLookup = NULL
		RETURN 0.0
	ENDIF
	
	INT iCam = 0
	FLOAT fTotalWeights = 0.0
	INT iMaxWeights = sInst.iMaxCam
	
	#IF IS_DEBUG_BUILD
		IF sInst.sDebug.bEdit
			iMaxWeights = COUNT_OF(sInst.sDebug.sDebugCams)// In debug loop through all cams, because they may be unsorted
		ENDIF
	#ENDIF
	
	CINEMATIC_CAM_BEHAVIOUR_STRUCT sCam
	
	REPEAT iMaxWeights iCam
		
		// If using the debug editor we need to accumulaate from the dynamic debug cams instead
		#IF IS_DEBUG_BUILD
			IF sInst.sDebug.bEdit
				// Because debug cams are recycled we need to only accumultate weights for added cams
				IF sInst.sDebug.sDebugCams[iCam].bAdded
					fTotalWeights += sInst.sDebug.sDebugCams[iCam].sCam.fWeighting
				ENDIF
				
				RELOOP
			ENDIF
		#ENDIF	
		
		// accumulate weightings so everything is proportional
		sCam = CALL sInst.fpCamLookup(iCam)
		fTotalWeights += sCam.fWeighting
		
	ENDREPEAT
	
	RETURN fTotalWeights
	
ENDFUNC


PROC CINEMATIC_CAM_SEQUENCE__REFRESH_CAM_WEIGHTINGS(CINEMATIC_CAM_SEQUENCE_STRUCT& sInst)

	sInst.fTotalAccumulatedWeights = _CINEMATIC_CAM_SEQUENCE__GET_ACCUMULATED_WEIGHTINGS_FOR_CURRENT_CAMERA_LOOKUP(sInst)
	
ENDPROC

