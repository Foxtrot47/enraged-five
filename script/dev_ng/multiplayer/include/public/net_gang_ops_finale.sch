/* ===================================================================================== *\
		MISSION NAME	:	net_gang_ops_finale.sch
		AUTHOR			:	Alastair Costley - 2017/07/19
		DESCRIPTION		:	Header for the Gang Ops Finale board.
\* ===================================================================================== */

// INCLUDE FILES.
USING "globals.sch"
USING "commands_network.sch"
USING "net_include.sch"
USING "net_FPS_cam.sch"
USING "net_gang_ops_helpers.sch"
USING "net_gang_ops_scaleform.sch"
USING "MP_Scaleform_Functions.sch"
USING "hud_drawing.sch"
USING "fmmc_vars.sch"
USING "fmmc_corona_menu.sch"


/* ===================================================================================== *\
			DEBUG ONLY
\* ===================================================================================== */

#IF IS_DEBUG_BUILD

PROC PRIVATE_PROCESS_GANG_OPS_FINALE_WIDGET_COMMANDS(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	IF sClientData.sDebugData.bStart
	
		IF sClientData.sStateData.eState != GOBS_IDLE
			sClientData.sStateData.eState = GOBS_IDLE
		ENDIF
		
		sClientData.sStateData.eState = GOBS_INIT
		sClientData.sDebugData.bStart = FALSE
		
		CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] FSM - Debug Starting Planning")
	ENDIF
	
	sClientData.sCameraData.ePositionId = INT_TO_ENUM(GANG_OPS_CAMERA_LOCATION, sClientData.sCameraData.iPositionId)
	
ENDPROC

PROC PRIVATE_GANG_OPS_FINALE_WIDGET_UPDATE(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	IF NOT sClientData.sDebugData.bCreatedWidgets
		IF DOES_WIDGET_GROUP_EXIST(sClientData.sDebugData.wgParentGroup)
			SET_CURRENT_WIDGET_GROUP(sClientData.sDebugData.wgParentGroup)
			
			sClientData.sDebugData.wgActiveGroup = START_WIDGET_GROUP("  Gang Ops (Finale)") 
			
				ADD_WIDGET_STRING(" - DEBUG SETTINGS - ")
				ADD_WIDGET_BOOL("Start", sClientData.sDebugData.bStart)
				ADD_WIDGET_BOOL("Reset", sClientData.sDebugData.bReset)
				ADD_WIDGET_BOOL("Player Visibility", sClientData.sDebugData.bPlayerVisible)
				ADD_WIDGET_BOOL("Lock Rendering to Player", sClientData.sDebugData.bLockToPlayer)
				
				ADD_WIDGET_STRING(" - DEBUG ACTIONS - ")
				ADD_WIDGET_BOOL("Interact", sClientData.sStateData.bInteract)
				ADD_WIDGET_BOOL("Add Player", sClientData.sDebugData.bAddFakePlayer)
				
				START_WIDGET_GROUP("Component Toggle")
					ADD_WIDGET_BOOL("Component Toggle: Scaleform", sClientData.sScaleformData.bEnableScaleform)
					ADD_WIDGET_BOOL("Component Toggle: Camera", sClientData.sCameraData.bEnableCamera)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("Scaleform")
					ADD_WIDGET_BOOL("Verbose Scaleform Logging", sClientData.sScaleformData.bVerboseDebugging)
	//				
	//				ADD_WIDGET_VECTOR_SLIDER("Main Board - World Pos", sClientData.sScaleformData.vMainBoardPosition, -9999.9, 9999.9, 1.0)
	//				ADD_WIDGET_VECTOR_SLIDER("Main Board - World Rot", sClientData.sScaleformData.vMainBoardRotation, -1000.0, 1000.0, 1.0)
	//				ADD_WIDGET_VECTOR_SLIDER("Main Board - Size", sClientData.sScaleformData.vMainBoardSize, 0.0, 100.0, 1.0)
	//				ADD_WIDGET_VECTOR_SLIDER("Main Board - World Size", sClientData.sScaleformData.vMainBoardWorldSize, 0.0, 100.0, 1.0)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Cameras")
				
					ADD_WIDGET_INT_SLIDER("Cam Position", sClientData.sCameraData.iPositionId, 0, 10, 1)
					ADD_WIDGET_VECTOR_SLIDER("Cam Initial Pos Offset", sClientData.sCameraData.vCamInitialOffset, -100.0, 100.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("Cam Map Pos Offset", sClientData.sCameraData.vCamMapOffset, -100.0, 100.0, 0.1)
					
					ADD_WIDGET_VECTOR_SLIDER("Cam Initial Rot Offset", sClientData.sCameraData.vCamInitialOffsetRot, -180.0, 180.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("Cam Map Rot Offset", sClientData.sCameraData.vCamMapOffsetRot, -180.0, 180.0, 0.1)
				
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Highlight / Cursor Data") 
					ADD_WIDGET_INT_SLIDER("iHighlight: ", 		sClientData.sCursorData.iHighlight, -100, 100, 1)
					ADD_WIDGET_INT_SLIDER("iSubItemSelection: ",sClientData.sCursorData.iSubItemSelection, -100, 100, 1)
				STOP_WIDGET_GROUP()
				
			STOP_WIDGET_GROUP()
			
			CLEAR_CURRENT_WIDGET_GROUP(sClientData.sDebugData.wgParentGroup)
			
			sClientData.sDebugData.bCreatedWidgets = TRUE
		ENDIF
	ENDIF

	PRIVATE_PROCESS_GANG_OPS_FINALE_WIDGET_COMMANDS(sClientData)

ENDPROC

#ENDIF



/* ===================================================================================== *\
			Methods, Wrappers / Misc functionality.
\* ===================================================================================== */

PROC PRIVATE_REQUEST_GANG_OPS_FINALE_SCALEFORM(GANG_OPS_SCALEFORM_DATA &sScaleformData)
	sScaleformData.siMainBoard = REQUEST_SCALEFORM_MOVIE("IAA_HEIST_BOARD")
	sScaleformData.siButtons = REQUEST_SCALEFORM_MOVIE("INSTRUCTIONAL_BUTTONS")
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Request Assets - Requested scaleform assets.")
ENDPROC

///    Clean the headshot out of memory.
/// PARAMS:
///    index - Player whose headshot is getting cleaned up.
PROC PRIVATE_CLEAN_SINGLE_GANG_OPS_FINALE_HEADSHOT(GANG_OPS_PED_HEADSHOT &sHeadshotData, BOOL bUnregister = TRUE)

	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Headshots - Cleaning specific player's headshot: ", GET_PLAYER_NAME(sHeadshotData.playerID))

	IF bUnregister
		IF sHeadshotData.eHeadshotState = GOHS_REQUESTING
		OR sHeadshotData.eHeadshotState = GOHS_AVAILABLE
			IF IS_PEDHEADSHOT_VALID(sHeadshotData.pedHeadshotID)
				UNREGISTER_PEDHEADSHOT(sHeadshotData.pedHeadshotID)
			ENDIF
		ENDIF
	ENDIF
	
	sHeadshotData.playerID = INVALID_PLAYER_INDEX()
	sHeadshotData.eHeadshotState = GOHS_NULL
	sHeadshotData.pedHeadshotID = NULL
	sHeadshotData.sHeadshotText = "NULL"
ENDPROC

PROC PRIVATE_CLEAN_ALL_GANG_OPS_FINALE_HEADSHOTS(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	INT index

	FOR index = 0 TO (MAX_GANG_OPS_PLAYERS-1)
		// Clean the headshot array.
		IF sClientData.sFinaleData.sHeadshotData[index].playerID != INVALID_PLAYER_INDEX()
			PRIVATE_CLEAN_SINGLE_GANG_OPS_FINALE_HEADSHOT(sClientData.sFinaleData.sHeadshotData[index], TRUE)
		ENDIF
	ENDFOR

ENDPROC

/// PURPOSE:
///    Find available slot to load headshot into - index corresponds to value passed to scaleform
/// PARAMS:
///    playerID - Player to store ready for a headshot.
/// RETURNS:
///   	INT - The index of the stored player. -1 if it falls through.
FUNC INT PRIVATE_INIT_GANG_OPS_FINALE_HEADSHOT_REQUEST(GANG_OPS_CLIENT_FINALE_DATA &sClientData, PLAYER_INDEX playerID)
	
	IF playerID = INVALID_PLAYER_INDEX() 
		RETURN GANG_OPS_INVALID
	ENDIF
	
	INT i
	
	REPEAT MAX_GANG_OPS_PLAYERS i
		IF sClientData.sFinaleData.sHeadshotData[i].playerID = playerID
			// Player already exists in request array, no need to coninue.
			RETURN i	
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_GANG_OPS_PLAYERS i
		IF sClientData.sFinaleData.sHeadshotData[i].playerID = INVALID_PLAYER_INDEX()
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Headshots - Resetting headshot request for player: ", 
				GET_PLAYER_NAME(playerID))
				
			sClientData.sFinaleData.sHeadshotData[i].playerID = playerID
			sClientData.sFinaleData.sHeadshotData[i].eHeadshotState = GOHS_NULL
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN GANG_OPS_INVALID
ENDFUNC

/// PURPOSE:
///    Check to see if we are already requesting and loading a ped headshot.
/// RETURNS:
///    BOOLEAN - True if requesting or loading.
FUNC BOOL PRIVATE_ARE_ANY_GANG_OPS_FINALE_HEADSHOTS_LOADING(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	INT i
	FOR i = 0 TO (MAX_GANG_OPS_PLAYERS-1)
		IF sClientData.sFinaleData.sHeadshotData[i].eHeadshotState = GOHS_REQUESTING
			RETURN TRUE
		ENDIF
	ENDFOR

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Loads ped headshot into hesitHeadshots array. Script requests texture first before loading it into the scaleform
/// PARAMS:
///    GANG_OPS_CLIENT_FINALE_DATA - General gang ops data struct
///    GANG_OPS_PED_HEADSHOT - Specific headshot data to verify
/// RETURNS:
///    BOOLEAN - True when headshot is ready for use. 
FUNC BOOL PRIVATE_IS_GANG_OPS_FINALE_HEADSHOT_READY(GANG_OPS_CLIENT_FINALE_DATA &sClientData, GANG_OPS_PED_HEADSHOT &sHeadshotData)	

	STRING 	sHeadshotTexture
	INT 	index
	BOOL 	bHasFound = FALSE
	
	SWITCH sHeadshotData.eHeadshotState
		
		CASE GOHS_NULL
		
			IF NOT PRIVATE_ARE_ANY_GANG_OPS_FINALE_HEADSHOTS_LOADING(sClientData)
				sHeadshotData.eHeadshotState = GOHS_REQUESTING
				CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Headshots - Requesting headshot for player: ", GET_PLAYER_NAME(sHeadshotData.playerID))
			ENDIF
			
			BREAK
		
		// Script must initially request the headshot
		CASE GOHS_REQUESTING
		
			IF IS_PED_INJURED(GET_PLAYER_PED(sHeadshotData.playerID))
				RETURN FALSE
			ENDIF
			
			IF sHeadshotData.pedHeadshotID = NULL
				sHeadshotData.pedHeadshotID = Get_HeadshotID_For_Player(sHeadshotData.playerID)
			ELSE
			
				sHeadshotData.sHeadshotText = GET_PEDHEADSHOT_TXD_STRING(sHeadshotData.pedHeadshotID)					
				
				REPEAT g_numGeneratedHeadshotsMP index
					IF (g_sGeneratedHeadshotsMP[index].hsdPlayerID = sHeadshotData.playerID)
						bHasFound = (sHeadshotData.pedHeadshotID = g_sGeneratedHeadshotsMP[index].hsdHeadshotID)
						BREAKLOOP
					ENDIF
				ENDREPEAT
				
				IF NOT bHasFound
					CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Headshots - Error! Player doesn't have a headshot request in the global request array. Player: ", 
						GET_PLAYER_NAME(sHeadshotData.playerID))
								
					sHeadshotData.pedHeadshotID = NULL
					RETURN FALSE
				ENDIF
				
				IF IS_STRING_NULL_OR_EMPTY(sHeadshotData.sHeadshotText)
				OR ARE_STRINGS_EQUAL(" ", sHeadshotData.sHeadshotText)
					CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Headshots - Error! Headshot system returned an empty string, retrying.")
					sHeadshotData.pedHeadshotID = NULL
					RETURN FALSE
				ENDIF
				
				CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Headshots - Global headshot data has loaded, moving to available.")
				sHeadshotData.eHeadshotState = GOHS_AVAILABLE	
				
				PUBLIC_SET_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
				
				RETURN TRUE
			ENDIF
			
			BREAK
		// Now we can use the headshot
		CASE GOHS_AVAILABLE
			
			sHeadshotTexture = GET_PEDHEADSHOT_TXD_STRING(sHeadshotData.pedHeadshotID)
			sHeadshotData.sHeadshotText = sHeadshotTexture
			//CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Headshots - Player ",GET_PLAYER_NAME(sHeadshotData.playerID),"'s headshot is ready, returning texture: ", sHeadshotTexture)
			
			RETURN TRUE
			
	ENDSWITCH

	RETURN FALSE
	
ENDFUNC

FUNC BOOL PRIVATE_ARE_ALL_GANG_OPS_FINALE_HEADSHOTS_LOADED(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	INT index
	BOOL bAllReady = TRUE

	FOR index = 0 TO (MAX_GANG_OPS_PLAYERS-1)
	
		IF sClientData.sFinaleData.iPlayerOrder[index] = GANG_OPS_INVALID
			RELOOP
		ENDIF
		
		IF NOT IS_NET_PLAYER_OK(sClientData.sFinaleData.sHeadshotData[index].playerID)
			RELOOP
		ENDIF
			
		IF sClientData.sFinaleData.sHeadshotData[index].eHeadshotState != GOHS_AVAILABLE
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Headshots - Client ", index, "'s headshot isn't ready.")
			bAllReady = FALSE
			BREAKLOOP
		ENDIF
		
	ENDFOR
	
	RETURN bAllReady

ENDFUNC

/// PURPOSE:
///    Get headshot for a heist member. All headshots must be laoded fully before we display a single one, so this will
///    return an empty label until that happens.
/// PARAMS:
///    playerID - The member whose headshot to retrieve.
/// RETURNS:
///    Headshot TXD string (already preloaded).
FUNC TEXT_LABEL_23 PRIVATE_GET_GANG_OPS_FINALE_HEADSHOT(GANG_OPS_CLIENT_FINALE_DATA &sClientData, PLAYER_INDEX playerID)

	TEXT_LABEL_23 sPortraitSlot
	
	INT index = PRIVATE_INIT_GANG_OPS_FINALE_HEADSHOT_REQUEST(sClientData, playerID)

	IF index = GANG_OPS_INVALID
		RETURN sPortraitSlot
	ENDIF
	
	IF sClientData.sFinaleData.sHeadshotData[index].bHeadshotsLoaded
		RETURN sClientData.sFinaleData.sHeadshotData[index].sHeadshotText 
	ENDIF
	
	IF PRIVATE_IS_GANG_OPS_FINALE_HEADSHOT_READY(sClientData, sClientData.sFinaleData.sHeadshotData[index])
		IF sClientData.sFinaleData.bInCorona
			IF PRIVATE_ARE_ALL_GANG_OPS_FINALE_HEADSHOTS_LOADED(sClientData)				
				sPortraitSlot = sClientData.sFinaleData.sHeadshotData[index].sHeadshotText
				
				IF NOT sClientData.sFinaleData.sHeadshotData[index].bHeadshotsLoaded
					sClientData.sFinaleData.sHeadshotData[index].bHeadshotsLoaded = TRUE
					PUBLIC_SET_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
					CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Headshots - All headshots have been loaded")
				ENDIF
			ENDIF
		ELSE
			sPortraitSlot = sClientData.sFinaleData.sHeadshotData[index].sHeadshotText
			//CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Headshots - Leader headshot has been loaded, returning texture: ", sPortraitSlot)
		ENDIF
	ENDIF

	RETURN sPortraitSlot
ENDFUNC

/// PURPOSE:
///    Get the role (and icon) for the provided player.
/// PARAMS:
///    index - Index of the selected player.
///    playerID - ID of the selected player.
///    iRoleIcon - BYREF role icon enum.
/// RETURNS:
///    TEXT_LABEL_15 - StringTable label for the specific player's role.
FUNC TEXT_LABEL_15 PRIVATE_GET_GANG_OPS_FINALE_ROLE(GANG_OPS_CLIENT_FINALE_DATA &sClientData,
													INT index)
	
	INT iPlayerTeam
	INT iTeamTotals[FMMC_MAX_TEAMS]
	BOOL bUseSingular = TRUE
	
	iPlayerTeam = sClientData.sFinaleData.iSelectedRole[index]
	//GET_ROLE_TOTAL_FROM_TEAMS(iTeamTotals, TRUE)
	
	IF iPlayerTeam != GANG_OPS_INVALID	
	AND iPlayerTeam < FMMC_MAX_TEAMS
		IF iTeamTotals[iPlayerTeam] > 1
			bUseSingular = FALSE
		ENDIF
	ENDIF
	
	TEXT_LABEL_15 sRoleLabel = GET_CORONA_TEAM_NAME(iPlayerTeam, bUseSingular)
		
	RETURN sRoleLabel
	
ENDFUNC

/// PURPOSE:
///    Get the status for a specific player.
/// PARAMS:
///    playerID - ID of the player
/// RETURNS:
///    BOOL - TRUE for ready, FALSE for not ready
FUNC BOOL PRIVATE_GET_GANG_OPS_FINALE_STATUS(GANG_OPS_CLIENT_FINALE_DATA &sClientData, PLAYER_INDEX piTarget)

	IF sClientData.sFinaleData.piLeader = piTarget
		RETURN TRUE
	ENDIF
	
	INT index
	FOR index = 0 TO (MAX_GANG_OPS_PLAYERS-1)
		IF INT_TO_PLAYERINDEX(sClientData.sFinaleData.iPlayerOrder[index]) != piTarget 
			RELOOP
		ENDIF
		
		IF sClientData.sFinaleData.iReadyState[index] = GANG_OPS_FINALE_READY
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Check the passed player's ready status (net safe).
/// PARAMS:
///    piTarget - Player to check
/// RETURNS:
///    TRUE if READY
FUNC BOOL PRIVATE_IS_GANG_OPS_FINALE_PLAYER_READY(GANG_OPS_CLIENT_FINALE_DATA &sClientData, PLAYER_INDEX piTarget)

	INT index
	FOR index = 0 TO (MAX_GANG_OPS_PLAYERS-1)
		IF INT_TO_PLAYERINDEX(sClientData.sFinaleData.iPlayerOrder[index]) != piTarget 
			RELOOP
		ENDIF
		
		RETURN sClientData.sFinaleData.iReadyState[index] = GANG_OPS_FINALE_READY
	ENDFOR
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Get the cut percentage for a specific player.
/// PARAMS:
///    index - Index for the player whose cut to get (0 = Leader, etc).
/// RETURNS:
///    INT - Cut percent.
FUNC INT PRIVATE_GET_GANG_OPS_FINALE_CUT(GANG_OPS_FINALE_DATA &sFinaleData, INT index)
	RETURN sFinaleData.iRewardPercentages[index]	
ENDFUNC

FUNC INT PRIVATE_GET_GANG_OPS_CUT_VALUE(GANG_OPS_FINALE_DATA &sFinaleData, INT index)

	INT iPayout = (sFinaleData.iTotalPay / 100) * PRIVATE_GET_GANG_OPS_FINALE_CUT(sFinaleData, index)
	RETURN iPayout
	
ENDFUNC

/// PURPOSE:
///    Reaplce and existing playerOrder array with fresh data.
/// PARAMS:
///    iTargetArray - Old array to replace.
FUNC INT BUILD_FINALE_PLAYER_ORDER_LIST(GANG_OPS_CLIENT_FINALE_DATA &sClientData, INT &iTargetArray[])

	PLAYER_INDEX thisPlayer = INVALID_PLAYER_INDEX()
	INT index
	
	FOR index = 0 TO (MAX_GANG_OPS_PLAYERS-1)
		iTargetArray[index] = -1
	ENDFOR
	
	INT iPointer = 0

	// Only process the full loop when in an activity session, otherwise only show the leader on the board
	IF NETWORK_IS_ACTIVITY_SESSION()

		FOR index = 0 TO (NUM_NETWORK_PLAYERS-1)
		    thisPlayer = INT_TO_PLAYERINDEX(index)
		    IF (IS_NET_PLAYER_OK(thisPlayer))
				IF thisPlayer <> INVALID_PLAYER_INDEX()
					IF NOT IS_THIS_PLAYER_SCTV_OR_JOINING_AS_SCTV(thisPlayer) 
						IF iPointer > (MAX_GANG_OPS_PLAYERS-1)
							CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Setup - Error! Attempted to add more clients than allowed to planning board.")
						ELSE
							// Add player to the player order array.
					        iTargetArray[iPointer] = NATIVE_TO_INT(thisPlayer)
							iPointer++ // Player added to array, manually increment the pointer.
						ENDIF
					ENDIF
			    ENDIF
			ENDIF
		ENDFOR
		
	ELSE
		iTargetArray[iPointer] = NATIVE_TO_INT(sClientData.sFinaleData.piLeader)
		iPointer++ // Player added to array, manually increment the pointer.
	ENDIF
	
	RETURN iPointer

ENDFUNC

/// PURPOSE:
///    Configure various misc client & server data required for the initialisation of the board.
/// PARAMS:
///    bIsLeader - 
/// RETURNS:
///    TRUE when finished.
FUNC BOOL PRIVATE_HAVE_CONFIGURED_GANG_OPS_FINALE_SETUP_DATA(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	sClientData.sFinaleData.iTotalPlayers = BUILD_FINALE_PLAYER_ORDER_LIST(sClientData, sClientData.sFinaleData.iPlayerOrder)

	IF sClientData.sFinaleData.piLeader != PLAYER_ID()
		RETURN TRUE
	ENDIF

	INT index
	
	SWITCH sClientData.sFinaleData.iStrandId
		CASE ciGANGOPS_MISSION_STRAND_IAA
			FOR index = 0 TO (MAX_GANG_OPS_PLAYERS-1)
				sClientData.sFinaleData.iSelectedRole[index] = GANG_OPS_TEAM_0
			ENDFOR
		BREAK
		
		CASE ciGANGOPS_MISSION_STRAND_SUBMARINE
			SWITCH sClientData.sFinaleData.iTotalPlayers
				CASE 1
					// Shouldnt be able to get here, but if we do, set them all to -1
					FOR index = 0 TO (MAX_GANG_OPS_PLAYERS-1)
						sClientData.sFinaleData.iSelectedRole[index] = GANG_OPS_INVALID
					ENDFOR
				BREAK
				
				CASE 2
					sClientData.sFinaleData.iSelectedRole[0] = GANG_OPS_TEAM_0
					sClientData.sFinaleData.iSelectedRole[1] = GANG_OPS_TEAM_1
					sClientData.sFinaleData.iSelectedRole[2] = GANG_OPS_INVALID
					sClientData.sFinaleData.iSelectedRole[3] = GANG_OPS_INVALID
				BREAK
				
				CASE 3
					sClientData.sFinaleData.iSelectedRole[0] = GANG_OPS_TEAM_0
					sClientData.sFinaleData.iSelectedRole[1] = GANG_OPS_TEAM_0
					sClientData.sFinaleData.iSelectedRole[2] = GANG_OPS_TEAM_1
					sClientData.sFinaleData.iSelectedRole[3] = GANG_OPS_INVALID
				BREAK
				
				CASE 4
					sClientData.sFinaleData.iSelectedRole[0] = GANG_OPS_TEAM_0
					sClientData.sFinaleData.iSelectedRole[1] = GANG_OPS_TEAM_0
					sClientData.sFinaleData.iSelectedRole[2] = GANG_OPS_TEAM_1
					sClientData.sFinaleData.iSelectedRole[3] = GANG_OPS_TEAM_1
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciGANGOPS_MISSION_STRAND_MISSILE_SILO
			FOR index = 0 TO (MAX_GANG_OPS_PLAYERS-1)
				sClientData.sFinaleData.iSelectedRole[index] = GANG_OPS_TEAM_0
			ENDFOR
		BREAK
	ENDSWITCH
	
	SWITCH sClientData.sFinaleData.iTotalPlayers
		CASE 1
			sClientData.sFinaleData.iRewardPercentages[0] = 100
			sClientData.sFinaleData.iRewardPercentages[1] = 0
			sClientData.sFinaleData.iRewardPercentages[2] = 0
			sClientData.sFinaleData.iRewardPercentages[3] = 0
			sClientData.sFinaleData.iRewardPercentages[4] = 0	// zero the kitty
		BREAK
		CASE 2
			sClientData.sFinaleData.iRewardPercentages[0] = g_sMPTunables.iH2_FINALE_CUT_TWO_PLAYERS_LEADER
			sClientData.sFinaleData.iRewardPercentages[1] = g_sMPTunables.iH2_FINALE_CUT_TWO_PLAYERS_MEMBER
			sClientData.sFinaleData.iRewardPercentages[2] = 0
			sClientData.sFinaleData.iRewardPercentages[3] = 0
			sClientData.sFinaleData.iRewardPercentages[4] = 0	// zero the kitty
		BREAK
		CASE 3
			sClientData.sFinaleData.iRewardPercentages[0] = g_sMPTunables.iH2_FINALE_CUT_THREE_PLAYERS_LEADER
			sClientData.sFinaleData.iRewardPercentages[1] = g_sMPTunables.iH2_FINALE_CUT_THREE_PLAYERS_MEMBER
			sClientData.sFinaleData.iRewardPercentages[2] = g_sMPTunables.iH2_FINALE_CUT_THREE_PLAYERS_MEMBER
			sClientData.sFinaleData.iRewardPercentages[3] = 0
			sClientData.sFinaleData.iRewardPercentages[4] = 0	// zero the kitty
		BREAK
		
		CASE 4
			sClientData.sFinaleData.iRewardPercentages[0] = g_sMPTunables.iH2_FINALE_CUT_FOUR_PLAYERS_LEADER
			sClientData.sFinaleData.iRewardPercentages[1] = g_sMPTunables.iH2_FINALE_CUT_FOUR_PLAYERS_MEMBER
			sClientData.sFinaleData.iRewardPercentages[2] = g_sMPTunables.iH2_FINALE_CUT_FOUR_PLAYERS_MEMBER
			sClientData.sFinaleData.iRewardPercentages[3] = g_sMPTunables.iH2_FINALE_CUT_FOUR_PLAYERS_MEMBER
			sClientData.sFinaleData.iRewardPercentages[4] = 0	// zero the kitty
		BREAK
	ENDSWITCH
	
	RETURN TRUE

ENDFUNC

/// PURPOSE:
///    Get the currently assigned team for the selected heist member.
/// PARAMS:
///    iMemberIndex - 0-7 which heist member.
/// RETURNS:
///    INT member's team. -1 for unassigned, -2 for fail.
FUNC INT PRIVATE_GET_GANG_OPS_FINALE_MEMBERS_TEAM(GANG_OPS_CLIENT_FINALE_DATA &sClientData, INT iMemberIndex)

	IF iMemberIndex < 0
	OR iMemberIndex > (MAX_GANG_OPS_PLAYERS-1)
		RETURN GANG_OPS_INVALID
	ENDIF

	RETURN sClientData.sFinaleData.iSelectedRole[iMemberIndex]

ENDFUNC

/// PURPOSE:
///    Maintain the scaleform instructional movie (bottom right). This handles all of it's own state and is mostly automated. If a new state is added to the
///    planning board, support must also be extended here.
/// PARAMS:
///    sClientData.sScaleformData.sButtons - 
PROC PRIVATE_UPDATE_GANG_OPS_FINALE_INSTR_BTNS(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	
	SPRITE_PLACEMENT spPlacementLocation = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
	
	// Don't render the buttons if the player is typing in network chat.
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF
	
	// Don't render the buttons if the player is running the tutorial
	IF sClientData.sTutorialData.bTutorialInProgress
		EXIT
	ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		EXIT
	ENDIF
	
	IF NOT sClientData.sScaleformData.bUpdateInstructions
	
		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sClientData.sScaleformData.siButtons,
											spPlacementLocation,
											sClientData.sScaleformData.sButtons,
											FALSE)
	
		EXIT
	ENDIF
	
	INT iSelectedMember = sClientData.sCursorData.iHighlight
	
	IF NOT GET_IS_WIDESCREEN()
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
	      SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(sClientData.sScaleformData.sButtons, 0.5)
	ELSE
	      SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(sClientData.sScaleformData.sButtons, 0.7)
	ENDIF
	
	REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sClientData.sScaleformData.sButtons)
	
	// TODO: Replace all the labels in here.
	
	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_INT_AS_TIME("", "HEIST_IB_CONT", sClientData.sFinaleData.iForceLaunchTime, sClientData.sScaleformData.sButtons)
	
	SWITCH sClientData.sCameraData.ePositionId
	
		CASE GOCAM_MAIN
		
			SWITCH sClientData.sStateData.eSubState
		
				CASE GOBSS_VIEWING
		
					IF sClientData.sFinaleData.piLeader = PLAYER_ID()
					
						IF sClientData.sCursorData.iHighlight < 100
					
							IF sClientData.sFinaleData.bLaunchButtonEnabled = TRUE
							AND sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_LAUNCH_BUTTON
								ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
									"HEIST_IB_ACCPT", sClientData.sScaleformData.sButtons, TRUE)
							ELSE
								ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
									"HEIST_IB_CONFPL", sClientData.sScaleformData.sButtons, TRUE)
							ENDIF
							
						ENDIF
						
					ELSE
						IF PRIVATE_IS_GANG_OPS_FINALE_PLAYER_READY(sClientData, PLAYER_ID())
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
								"HEIST_IB_UNRDY", sClientData.sScaleformData.sButtons, TRUE)
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
								"HEIST_IB_RDY", sClientData.sScaleformData.sButtons, TRUE)
						ENDIF
					ENDIF
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
						"HEIST_IB_QUIT", sClientData.sScaleformData.sButtons, TRUE) // Go back.
							
					IF iSelectedMember != GANG_OPS_FINALE_ROW_LAUNCH
					AND sClientData.sCursorData.iHighlight < 100
						IF IS_PS3_VERSION() OR IS_PLAYSTATION_PLATFORM() OR IS_PC_VERSION()
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT, 
								"HEIST_IB_PSN", sClientData.sScaleformData.sButtons, TRUE)
						ELIF IS_XBOX360_VERSION() OR IS_XBOX_PLATFORM()
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(
								GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT), 
									"HEIST_IB_XBL", sClientData.sScaleformData.sButtons)
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(
								GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT), 
									"HEIST_IB_PSNXBL", sClientData.sScaleformData.sButtons)
						ENDIF
						
					ENDIF
								
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
						"HP_OVERVIEW", sClientData.sScaleformData.sButtons, TRUE) // View overview.
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_RB, 
						"HP_SURVEIL", sClientData.sScaleformData.sButtons, TRUE) // View surveillance.
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_LB, 
						"HP_MAP", sClientData.sScaleformData.sButtons, TRUE) // View map.
									
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
							"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
					ELSE
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_TRIGGERS, 
							"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons) // Zoom 
					ENDIF
						
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
						"HEIST_IB_LOOK", sClientData.sScaleformData.sButtons) // Look Around

					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
						"HEIST_IB_NAV", sClientData.sScaleformData.sButtons) // Navigate controls.
				BREAK
				
				CASE GOBSS_EDITING
				
					IF sClientData.sFinaleData.piLeader != PLAYER_ID()
						EXIT
					ENDIF
									
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
					"HEIST_IB_SAVE", sClientData.sScaleformData.sButtons, TRUE) // Save changes

					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
						"HEIST_IB_UNSAVE", sClientData.sScaleformData.sButtons, TRUE) // Cancel change	
						
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
							"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
					ELSE
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_TRIGGERS, 
							"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons) // Zoom 
					ENDIF
						
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
						"HEIST_IB_LOOK", sClientData.sScaleformData.sButtons) // Look Around

					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
						"HEIST_IB_NAV", sClientData.sScaleformData.sButtons) // Navigate controls.
				BREAK
				
			ENDSWITCH
			
		BREAK
		
		CASE GOCAM_OVERVIEW
		
			SWITCH sClientData.sStateData.eSubState
		
				CASE GOBSS_VIEWING
		
					IF sClientData.sFinaleData.piLeader = PLAYER_ID()
					
						IF sClientData.sCursorData.iHighlight < 100
					
							IF sClientData.sFinaleData.bLaunchButtonEnabled = TRUE
							AND sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_LAUNCH_BUTTON
								ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
									"HEIST_IB_ACCPT", sClientData.sScaleformData.sButtons, TRUE)
							ELSE
								ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
									"HEIST_IB_CONFPL", sClientData.sScaleformData.sButtons, TRUE)
							ENDIF
							
						ENDIF
						
					ELSE
						IF PRIVATE_IS_GANG_OPS_FINALE_PLAYER_READY(sClientData, PLAYER_ID())
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
								"HEIST_IB_UNRDY", sClientData.sScaleformData.sButtons, TRUE)
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
								"HEIST_IB_RDY", sClientData.sScaleformData.sButtons, TRUE)
						ENDIF
					ENDIF
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
						"HEIST_IB_QUIT", sClientData.sScaleformData.sButtons, TRUE) // Go back.
											
					IF iSelectedMember != GANG_OPS_FINALE_ROW_LAUNCH
					AND sClientData.sCursorData.iHighlight < 100
						IF IS_PS3_VERSION() OR IS_PLAYSTATION_PLATFORM() OR IS_PC_VERSION()
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT, 
								"HEIST_IB_PSN", sClientData.sScaleformData.sButtons, TRUE)
						ELIF IS_XBOX360_VERSION() OR IS_XBOX_PLATFORM()
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(
								GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT), 
									"HEIST_IB_XBL", sClientData.sScaleformData.sButtons)
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(
								GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT), 
									"HEIST_IB_PSNXBL", sClientData.sScaleformData.sButtons)
						ENDIF
						
					ENDIF
					
					SWITCH sClientData.sCameraData.eCachedPositionId
						CASE GOCAM_MAIN
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
							"HP_MEMBERS", sClientData.sScaleformData.sButtons, TRUE) // View members
						BREAK
						
						CASE GOCAM_MAP
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
							"HP_MAP", sClientData.sScaleformData.sButtons, TRUE) // View map
						BREAK
						
						CASE GOCAM_IMAGES
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
							"HP_SURVEIL", sClientData.sScaleformData.sButtons, TRUE) // View surveillance
						BREAK
					ENDSWITCH
						
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
							"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
					ELSE
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_RT, 
							"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
					ENDIF
						
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
						"HEIST_IB_LOOK", sClientData.sScaleformData.sButtons) // Look 
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
						"HEIST_IB_NAV", sClientData.sScaleformData.sButtons) // Navigate controls.
					
				BREAK
				
				CASE GOBSS_EDITING
				
					IF sClientData.sFinaleData.piLeader != PLAYER_ID()
						EXIT
					ENDIF
										
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
					"HEIST_IB_SAVE", sClientData.sScaleformData.sButtons, TRUE) // Save changes

					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
						"HEIST_IB_UNSAVE", sClientData.sScaleformData.sButtons, TRUE) // Cancel change	
										
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
							"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
					ELSE
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_TRIGGERS, 
							"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons) // Zoom 
					ENDIF
						
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
						"HEIST_IB_LOOK", sClientData.sScaleformData.sButtons) // Look 
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
						"HEIST_IB_NAV", sClientData.sScaleformData.sButtons) // Navigate controls.
				BREAK
				
			ENDSWITCH
		
		BREAK
		
		CASE GOCAM_MAP
		
			SWITCH sClientData.sStateData.eSubState
		
				CASE GOBSS_VIEWING
		
					IF sClientData.sFinaleData.piLeader = PLAYER_ID()
					
						IF sClientData.sCursorData.iHighlight < 100
					
							IF sClientData.sFinaleData.bLaunchButtonEnabled = TRUE
							AND sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_LAUNCH_BUTTON
								ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
									"HEIST_IB_ACCPT", sClientData.sScaleformData.sButtons, TRUE)
							ELSE
								ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
									"HEIST_IB_CONFPL", sClientData.sScaleformData.sButtons, TRUE)
							ENDIF
							
						ENDIF
						
					ELSE
						IF PRIVATE_IS_GANG_OPS_FINALE_PLAYER_READY(sClientData, PLAYER_ID())
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
								"HEIST_IB_UNRDY", sClientData.sScaleformData.sButtons, TRUE)
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
								"HEIST_IB_RDY", sClientData.sScaleformData.sButtons, TRUE)
						ENDIF
					ENDIF
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
						"HEIST_IB_QUIT", sClientData.sScaleformData.sButtons, TRUE) // Go back.
							
					IF iSelectedMember != GANG_OPS_FINALE_ROW_LAUNCH
					AND sClientData.sCursorData.iHighlight < 100
						IF IS_PS3_VERSION() OR IS_PLAYSTATION_PLATFORM() OR IS_PC_VERSION()
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT, 
								"HEIST_IB_PSN", sClientData.sScaleformData.sButtons, TRUE)
						ELIF IS_XBOX360_VERSION() OR IS_XBOX_PLATFORM()
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(
								GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT), 
									"HEIST_IB_XBL", sClientData.sScaleformData.sButtons)
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(
								GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT), 
									"HEIST_IB_PSNXBL", sClientData.sScaleformData.sButtons)
						ENDIF
						
					ENDIF
								
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
						"HP_OVERVIEW", sClientData.sScaleformData.sButtons, TRUE) // View overview.
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_RB, 
						"HP_MEMBERS", sClientData.sScaleformData.sButtons, TRUE) // View members.
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_LB, 
						"HP_SURVEIL", sClientData.sScaleformData.sButtons, TRUE) // View surveillance.
									
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
							"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
					ELSE
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_TRIGGERS, 
							"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons) // Zoom 
					ENDIF
						
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
						"HEIST_IB_LOOK", sClientData.sScaleformData.sButtons) // Look Around

					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
						"HEIST_IB_NAV", sClientData.sScaleformData.sButtons) // Navigate controls.
				BREAK
				
				CASE GOBSS_EDITING
				
					IF sClientData.sFinaleData.piLeader != PLAYER_ID()
						EXIT
					ENDIF
									
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
					"HEIST_IB_SAVE", sClientData.sScaleformData.sButtons, TRUE) // Save changes

					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
						"HEIST_IB_UNSAVE", sClientData.sScaleformData.sButtons, TRUE) // Cancel change	
						
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
							"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
					ELSE
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_TRIGGERS, 
							"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons) // Zoom 
					ENDIF
						
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
						"HEIST_IB_LOOK", sClientData.sScaleformData.sButtons) // Look Around

					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
						"HEIST_IB_NAV", sClientData.sScaleformData.sButtons) // Navigate controls.
				BREAK
				
			ENDSWITCH
				
		BREAK
			
		CASE GOCAM_IMAGES
		
			SWITCH sClientData.sStateData.eSubState
		
				CASE GOBSS_VIEWING
		
					IF sClientData.sFinaleData.piLeader = PLAYER_ID()
					
						IF sClientData.sCursorData.iHighlight < 100
					
							IF sClientData.sFinaleData.bLaunchButtonEnabled = TRUE
							AND sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_LAUNCH_BUTTON
								ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
									"HEIST_IB_ACCPT", sClientData.sScaleformData.sButtons, TRUE)
							ELSE
								ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
									"HEIST_IB_CONFPL", sClientData.sScaleformData.sButtons, TRUE)
							ENDIF
							
						ENDIF
						
					ELSE
						IF PRIVATE_IS_GANG_OPS_FINALE_PLAYER_READY(sClientData, PLAYER_ID())
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
								"HEIST_IB_UNRDY", sClientData.sScaleformData.sButtons, TRUE)
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
								"HEIST_IB_RDY", sClientData.sScaleformData.sButtons, TRUE)
						ENDIF
					ENDIF
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
						"HEIST_IB_QUIT", sClientData.sScaleformData.sButtons, TRUE) // Go back.
							
					IF iSelectedMember != GANG_OPS_FINALE_ROW_LAUNCH
					AND sClientData.sCursorData.iHighlight < 100
						IF IS_PS3_VERSION() OR IS_PLAYSTATION_PLATFORM() OR IS_PC_VERSION()
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT, 
								"HEIST_IB_PSN", sClientData.sScaleformData.sButtons, TRUE)
						ELIF IS_XBOX360_VERSION() OR IS_XBOX_PLATFORM()
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(
								GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT), 
									"HEIST_IB_XBL", sClientData.sScaleformData.sButtons)
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(
								GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT), 
									"HEIST_IB_PSNXBL", sClientData.sScaleformData.sButtons)
						ENDIF
						
					ENDIF
								
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
						"HP_OVERVIEW", sClientData.sScaleformData.sButtons, TRUE) // View overview.
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_RB, 
						"HP_MAP", sClientData.sScaleformData.sButtons, TRUE) // View map.
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_LB, 
						"HP_MEMBERS", sClientData.sScaleformData.sButtons, TRUE) // View members.
									
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
							"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
					ELSE
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_TRIGGERS, 
							"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons) // Zoom 
					ENDIF
						
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
						"HEIST_IB_LOOK", sClientData.sScaleformData.sButtons) // Look Around

					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
						"HEIST_IB_NAV", sClientData.sScaleformData.sButtons) // Navigate controls.
				BREAK
				
				CASE GOBSS_EDITING
				
					IF sClientData.sFinaleData.piLeader != PLAYER_ID()
						EXIT
					ENDIF
									
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
					"HEIST_IB_SAVE", sClientData.sScaleformData.sButtons, TRUE) // Save changes

					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
						"HEIST_IB_UNSAVE", sClientData.sScaleformData.sButtons, TRUE) // Cancel change	
						
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
							"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
					ELSE
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_TRIGGERS, 
							"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons) // Zoom 
					ENDIF
						
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
						"HEIST_IB_LOOK", sClientData.sScaleformData.sButtons) // Look Around

					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
						"HEIST_IB_NAV", sClientData.sScaleformData.sButtons) // Navigate controls.
				BREAK
				
			ENDSWITCH

		BREAK
	
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Buttons - Updated instructional buttons for position: ", ENUM_TO_INT(sClientData.sCameraData.ePositionId))
	
	RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sClientData.sScaleformData.siButtons,
										spPlacementLocation,
										sClientData.sScaleformData.sButtons,
										sClientData.sScaleformData.bUpdateInstructions)
											
	PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, FALSE)
								
ENDPROC

PROC PRIVATE_RENDER_GANG_OPS_FINALE_SCALEFORM(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	IF sClientData.sScaleformData.bDrawBoard
	AND g_bPlayingGangOpsCutscene = FALSE
		IF HAS_SCALEFORM_MOVIE_LOADED(sClientData.sScaleformData.siMainBoard)
									
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			SET_TEXT_RENDER_ID(sClientData.sScaleformData.iRenderTargetID)
			SET_SCALEFORM_MOVIE_TO_USE_SUPER_LARGE_RT(sClientData.sScaleformData.siMainBoard, TRUE)
			DRAW_SCALEFORM_MOVIE(sClientData.sScaleformData.siMainBoard, cfHEIST_SCREEN_CENTRE_X, cfHEIST_SCREEN_CENTRE_Y, cfHEIST_SCREEN_WIDTH, cfHEIST_SCREEN_HEIGHT, 255, 255, 255, 255)
			SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
					
			IF NOT sClientData.sAudioData.bStartedLoopingBackgroundSound
				sClientData.sAudioData.iLoopingBackgroundSoundId = GET_SOUND_ID()
				IF sClientData.sAudioData.iLoopingBackgroundSoundId != -1
					PLAY_SOUND_FROM_COORD(sClientData.sAudioData.iLoopingBackgroundSoundId, "Background", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Render - Started looping background sound as we've started drawing the screen. Sound ID: ", sClientData.sAudioData.iLoopingBackgroundSoundId)
					sClientData.sAudioData.bStartedLoopingBackgroundSound = TRUE
				ENDIF
			ENDIF		
							
			IF sClientData.sScaleformData.bVerboseDebugging
				CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Render - Index: ", NATIVE_TO_INT(sClientData.sScaleformData.siMainBoard))
			ENDIF
		ENDIF
	ENDIF
	
	IF sClientData.sScaleformData.bDrawInstructions	
		PRIVATE_UPDATE_GANG_OPS_FINALE_INSTR_BTNS(sClientData)
	ENDIF

ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_FINALE_CUTS(GANG_OPS_FINALE_DATA &sFinaleData, INT iTarget, INT iModifier)

	INT iMinimumCut = g_sMPTunables.IH2_FINALE_MIN_CUT_AMOUNT
	INT iMaxCut = g_sMPTunables.IH2_FINALE_MAX_CUT_AMOUNT
	
	IF iModifier < 0 	
	
		// DECREMENT TARGET, INCREASE POT.
		INT iTargetCut = sFinaleData.iRewardPercentages[iTarget]
		IF iTargetCut < ABSI(iModifier)
			iModifier = iTargetCut
		ENDIF
		
		IF sFinaleData.iRewardPercentages[iTarget] < iMinimumCut

			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Rewards - Cannot decrease target cut! Target already less cut than allowed minimum! Target cut: ", 
				sFinaleData.iRewardPercentages[iTarget], ", Minimum: ", iMinimumCut)
			
			// Because the target has somehow ended up with less than the minimum, calculate how much under they are and add it back onto the target.
			INT iRem
			iRem = iMinimumCut - sFinaleData.iRewardPercentages[iTarget]
			sFinaleData.iRewardPercentages[iTarget] += iRem
			
			CDEBUG3LN(DEBUG_GANG_OPS_FIN, "Rewards - Added missing remainder: ", iRem, ", to target total: ", 
				(sFinaleData.iRewardPercentages[iTarget] - iRem), " to rebalance player cuts.")

			PLAY_SOUND_FROM_COORD(-1,"Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())

		ELIF sFinaleData.iRewardPercentages[iTarget] = iMinimumCut

			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Rewards - Cannot decrease target cut, target already at minimum: ", iMinimumCut)
			
			PLAY_SOUND_FROM_COORD(-1,"Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			
		ELSE
		
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Rewards - Decrease target cut. iModifier: ", iModifier)
			
			CDEBUG3LN(DEBUG_GANG_OPS_FIN, "  ... DECREASE - Pre cut modifications: ")
			CDEBUG3LN(DEBUG_GANG_OPS_FIN, "  ... Pot(%): 	", sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS])
			CDEBUG3LN(DEBUG_GANG_OPS_FIN, "  ... Target(%): 	", sFinaleData.iRewardPercentages[iTarget])
			
			// Validate that the values are legal (i.e. within 0 and 100).
			IF sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS] <= (100 - ABSI(iModifier))
				sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS] = sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS] + ABSI(iModifier)
				sFinaleData.iRewardPercentages[iTarget] = sFinaleData.iRewardPercentages[iTarget] - ABSI(iModifier)
			ENDIF
		
			CDEBUG3LN(DEBUG_GANG_OPS_FIN, "  ... DECREASE - Post cut modifications: ")
			CDEBUG3LN(DEBUG_GANG_OPS_FIN, "  ... Pot(%): 	", sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS])
			CDEBUG3LN(DEBUG_GANG_OPS_FIN, "  ... Target(%): 	", sFinaleData.iRewardPercentages[iTarget])
	
			IF iModifier = 0
				PLAY_SOUND_FROM_COORD(-1,"Error", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ELSE
				PLAY_SOUND_FROM_COORD(-1,"Decrease_Loot_Share", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ENDIF

		ENDIF
		
		IF sFinaleData.iRewardPercentages[iTarget] <= iMinimumCut
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_6") // TODO: Replace label
				CLEAR_HELP()
				PRINT_HELP("HEIST_NOTE_6", GANG_OPS_FINALE_CUT_WARN_TIME) // TODO: Replace label
			ENDIF
		ENDIF
		
	ELSE 			
		// INCREMENT TARGET, DECREASE POT.
		INT iPotCut = sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS]
		IF iPotCut < ABSI(iModifier)
			iModifier = iPotCut
		ENDIF

		CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Rewards - Increase target cut. iModifier: ", iModifier)
		
		IF sFinaleData.iRewardPercentages[iTarget] > iMaxCut

			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Rewards - Cannot increase target cut! Already greater cut than allowed maximum! Target cut: ", 
				sFinaleData.iRewardPercentages[iTarget], ", Maximum: ", iMaxCut)
			
			// Because the target has somehow ended up with more than the maximum, calculate how much over they are and 
			// add it back onto the pot.
			INT iRem
			iRem = sFinaleData.iRewardPercentages[iTarget] - iMaxCut
			sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS] += iRem

			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Rewards - Added remainder: ", iRem, ", to pot total: ", 
				(sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS] - iRem), " to rebalance player cuts.")
			
			PLAY_SOUND_FROM_COORD(-1,"Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			
		ELIF sFinaleData.iRewardPercentages[iTarget] = iMaxCut
		
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Rewards - Cannot increase target cut, already at maximum: ", iMaxCut)
			
			PLAY_SOUND_FROM_COORD(-1,"Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			
		ELSE
		
			CDEBUG3LN(DEBUG_GANG_OPS_FIN, "  ... INCREASE - Pre cut modifications: ")
			CDEBUG3LN(DEBUG_GANG_OPS_FIN, "  ... Pot(%): 	", sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS])
			CDEBUG3LN(DEBUG_GANG_OPS_FIN, "  ... Target(%): 	", sFinaleData.iRewardPercentages[iTarget])
		
			// Validate that the values are legal (i.e. within 0 and 100).
			IF sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS] >= ABSI(iModifier)
				sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS] = sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS] - ABSI(iModifier)
				sFinaleData.iRewardPercentages[iTarget] = sFinaleData.iRewardPercentages[iTarget] + ABSI(iModifier)
			ENDIF
			
			CDEBUG3LN(DEBUG_GANG_OPS_FIN, "  ... INCREASE - Post cut modifications: ")
			CDEBUG3LN(DEBUG_GANG_OPS_FIN, "  ... Pot(%): 	", sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS])
			CDEBUG3LN(DEBUG_GANG_OPS_FIN, "  ... Target(%): 	", sFinaleData.iRewardPercentages[iTarget])
			
			IF iModifier = 0
				PLAY_SOUND_FROM_COORD(-1,"Error", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ELSE
				PLAY_SOUND_FROM_COORD(-1,"Increase_Loot_Share", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ENDIF
			
		ENDIF
		
		IF sFinaleData.iRewardPercentages[iTarget] >= iMaxCut
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_5") // TODO: Replace labels
				CLEAR_HELP()
				PRINT_HELP("HEIST_NOTE_5", GANG_OPS_FINALE_CUT_WARN_TIME) // TODO: Replace labels
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

PROC PRIVATE_INIT_GANG_OPS_FINALE_CAMERA(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	
	sClientData.sCameraData.vBaseRotation.X = 0.0
	sClientData.sCameraData.vBaseRotation.Y = 0.0
	sClientData.sCameraData.vBaseRotation.Z = -180.0
	
	VECTOR vPosOffset = PUBLIC_GET_GANG_OPS_POSITION_OFFSET(sClientData.sCameraData, GOVEC_CAM_INITIAL)
	VECTOR vCamPos = PUBLIC_GET_GANG_OPS_CAMERA_POSITION_FOR_POSITION_ID(sClientData.sCameraData)
	
	VECTOR vRotOffset = PUBLIC_GET_GANG_OPS_ROTATION_OFFSET(sClientData.sCameraData, GOVEC_CAM_INITIAL)
	VECTOR vCamRot = PUBLIC_GET_GANG_OPS_CAMERA_ROTATION_FOR_POSITION_ID(sClientData.sCameraData)
	
	INIT_FIRST_PERSON_CAMERA(	sClientData.sCameraData.sFPSCam, 
								vCamPos,
								vCamRot, // TODO: Re-add for rotation.
								60, // TODO: This needs to be done properly 
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, 0, -1, FALSE)

	PUBLIC_SET_GANG_OPS_FPS_CAM_FAR(sClientData.sCameraData)

	SET_WIDESCREEN_BORDERS(TRUE,0)	
	
	GANG_OPS_VECTORS eNewLoc = PUBLIC_GET_GANG_OPS_POSITION_FOR_CAM_STATE(sClientData.sCameraData.ePositionId)

	vPosOffset = PUBLIC_GET_GANG_OPS_POSITION_OFFSET(sClientData.sCameraData, eNewLoc)
	vRotOffset = PUBLIC_GET_GANG_OPS_ROTATION_OFFSET(sClientData.sCameraData, eNewLoc)

	sClientData.sCameraData.ePositionId = GOCAM_MAIN
	sClientData.sCameraData.vCamCurrentPos = PUBLIC_GET_GANG_OPS_CAMERA_POSITION_FOR_POSITION_ID(sClientData.sCameraData)
	sClientData.sCameraData.vCamCurrentRot = PUBLIC_GET_GANG_OPS_CAMERA_ROTATION_FOR_POSITION_ID(sClientData.sCameraData)
	
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Camera - Started FPS camera.")
	CDEBUG3LN(DEBUG_GANG_OPS_FIN, "   ... Cam Pos: ", vCamPos)
	CDEBUG3LN(DEBUG_GANG_OPS_FIN, "   ... Cam Rot: ", vCamRot)
	CDEBUG3LN(DEBUG_GANG_OPS_FIN, "   ... Cam Current Pos: ", sClientData.sCameraData.vCamCurrentPos)
	CDEBUG3LN(DEBUG_GANG_OPS_FIN, "   ... Cam Current Rot: ", sClientData.sCameraData.vCamCurrentRot)
	CDEBUG3LN(DEBUG_GANG_OPS_FIN, "   ... Pos Offset: ", vPosOffset)
	CDEBUG3LN(DEBUG_GANG_OPS_FIN, "   ... Rot Offset: ", vRotOffset)
	CDEBUG3LN(DEBUG_GANG_OPS_FIN, "   ... Base Rot: ", sClientData.sCameraData.vBaseRotation)

ENDPROC

PROC PRIVATE_CLEAR_GANG_OPS_FINALE_CAMERA(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	CLEAR_FIRST_PERSON_CAMERA(sClientData.sCameraData.sFPSCam)
	SET_WIDESCREEN_BORDERS(FALSE,0)	

	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Camera - Cleared FPS camera.")
	
ENDPROC

PROC PRIVATE_SET_GANG_OPS_FINALE_CAMERA_AND_FOCUS(GANG_OPS_CLIENT_FINALE_DATA &sClientData, GANG_OPS_CAMERA_LOCATION eLocation)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_SET_GANG_OPS_FINALE_CAMERA_AND_FOCUS - Setting camera focus to: ", ENUM_TO_INT(eLocation))
	sClientData.sCameraData.ePositionId = eLocation
	sClientData.sCameraData.iPositionId = ENUM_TO_INT(eLocation)
	sClientData.sCursorData.iHighlightCached = sClientData.sCursorData.iHighlight
	PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_FINALE_CAMERA_AND_FOCUS_FOR_INPUT(	GANG_OPS_CLIENT_FINALE_DATA &sClientData, 
																GANG_OPS_CONTROL_TYPE eControl)

	// TODO: Do we want to cancel transitions midway?
	IF sClientData.sCameraData.bTransitioning
		EXIT
	ENDIF
	
	// Can't change camera positions while mid edit.
	IF sClientData.sStateData.eSubState = GOBSS_EDITING
		EXIT
	ENDIF

	SWITCH eControl
	
		CASE GOCT_LB
		
			IF sClientData.sCameraData.ePositionId = GOCAM_MAIN
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_MAP")
				sClientData.sCameraData.ePositionId = GOCAM_MAP
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_MAP)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ELIF sClientData.sCameraData.ePositionId = GOCAM_MAP
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_IMAGES")
				sClientData.sCameraData.ePositionId = GOCAM_IMAGES
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_IMAGES)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ELIF sClientData.sCameraData.ePositionId = GOCAM_IMAGES
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_MAIN")
				sClientData.sCameraData.ePositionId = GOCAM_MAIN
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_MAIN)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ENDIF
		
			BREAK
		
		CASE GOCT_RB
		
			IF sClientData.sCameraData.ePositionId = GOCAM_MAIN
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_IMAGES")
				sClientData.sCameraData.ePositionId = GOCAM_IMAGES
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_IMAGES)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ELIF sClientData.sCameraData.ePositionId = GOCAM_MAP
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_MAIN")
				sClientData.sCameraData.ePositionId = GOCAM_MAIN
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_MAIN)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ELIF sClientData.sCameraData.ePositionId = GOCAM_IMAGES
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_MAP")
				sClientData.sCameraData.ePositionId = GOCAM_MAP
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_MAP)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ENDIF
		
			BREAK
			
		CASE GOCT_Y
		
			IF sClientData.sCameraData.ePositionId != GOCAM_OVERVIEW
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_MAIN")
				sClientData.sCameraData.eCachedPositionId = sClientData.sCameraData.ePositionId
				sClientData.sCameraData.iCachedPositionId = sClientData.sCameraData.iPositionId
				sClientData.sCameraData.ePositionId = GOCAM_OVERVIEW
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_OVERVIEW)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ELIF sClientData.sCameraData.ePositionId = GOCAM_OVERVIEW
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_OVERVIEW")
				sClientData.sCameraData.ePositionId = sClientData.sCameraData.eCachedPositionId
				sClientData.sCameraData.iPositionId = sClientData.sCameraData.iCachedPositionId
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ENDIF
		
			BREAK
	
	ENDSWITCH

ENDPROC

PROC PRIVATE_TOGGLE_GANG_OPS_FINALE_READY_STATE(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	
	INT index
	FOR index = 0 TO (MAX_GANG_OPS_PLAYERS-1)
		IF INT_TO_PLAYERINDEX(sClientData.sFinaleData.iPlayerOrder[index]) != PLAYER_ID()
			RELOOP
		ENDIF
		
		IF sClientData.sFinaleData.iReadyState[index] = GANG_OPS_FINALE_READY
			sClientData.sFinaleData.iReadyState[index] = GANG_OPS_FINALE_NOT_READY
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Ready - Set local state to: GANG_OPS_FINALE_NOT_READY")
			PLAY_SOUND_FROM_COORD(-1,"Back", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
		ELSE
			sClientData.sFinaleData.iReadyState[index] = GANG_OPS_FINALE_READY
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Ready - Set local state to: GANG_OPS_FINALE_READY")
			PLAY_SOUND_FROM_COORD(-1,"Select", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
		ENDIF
		
	ENDFOR
	
	PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
	
ENDPROC

PROC PRIVATE_MONITOR_GANG_OPS_FINALE_LAUNCH(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	IF sClientData.sFinaleData.piLeader = PLAYER_ID()
		EXIT
	ENDIF
	
	INT index
	BOOL bShowTutorialHelp = FALSE
	
	FOR index = 0 TO (MAX_GANG_OPS_PLAYERS-1)
		IF sClientData.sFinaleData.iPlayerOrder[index] = GANG_OPS_INVALID
			RELOOP
		ENDIF

		IF sClientData.sFinaleData.bRunningTutorial[index] = TRUE
			bShowTutorialHelp = TRUE
			BREAKLOOP
		ENDIF
	ENDFOR
	
	IF NOT sClientData.sTutorialData.bTutorialInProgress
		IF bShowTutorialHelp
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_TUT_WAIT")
				PRINT_HELP_FOREVER("HP_TUT_WAIT")
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_TUT_WAIT")
				CLEAR_HELP()
			ENDIF	
		ENDIF
	ENDIF

//	IF GlobalplayerBD_FM_2[NATIVE_TO_INT(sClientData.sFinaleData.piLeader)].GangOps.iReadyState[0] = GANG_OPS_FINALE_READY
//	
//		// TODO: Add the rest of the logic for launching.
//		CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] LAUNCH - GO GO GO - LEADER HAS AUTHORISED LAUNCH!")
//		PUBLIC_PROGRESS_GANG_OPS_FSM(sClientData.sStateData)
//		
//	ENDIF

ENDPROC

PROC PRIVATE_LAUNCH_GANG_OPS_FINALE(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	
	IF sClientData.sFinaleData.piLeader != PLAYER_ID()
		EXIT
	ENDIF
	
	sClientData.sFinaleData.iReadyState[0] = GANG_OPS_FINALE_READY
	GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iReadyState[0] = GANG_OPS_FINALE_READY
	
	PLAY_SOUND_FROM_COORD(-1,"Launch_Mission", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
	
	KILL_FINALE_READY_SOUND(sClientData)
	
//	// TODO: Add the rest of the logic for launching.
//	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] LAUNCH - GO GO GO - AUTHORISING LAUNCH FOR ALL CLIENTS!")
//	PUBLIC_REGRESS_GANG_OPS_FSM(sClientData.sStateData)
	
ENDPROC

PROC PRIVATE_START_GANG_OPS_FINALE_EDIT_MODE(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	IF sClientData.sStateData.eSubState = GOBSS_EDITING
		EXIT
	ENDIF
	
	sClientData.sStateData.eSubState = GOBSS_EDITING
	sClientData.sScaleformData.bUpdateInstructions = TRUE
	
	INT index
	FOR index = 0 TO MAX_GANG_OPS_PLAYERS
	
		IF index >= MAX_GANG_OPS_PLAYERS
			sClientData.sFinaleData.iRewardPercentagesCache[MAX_GANG_OPS_PLAYERS] = sClientData.sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS]
			RELOOP
		ENDIF
	
		sClientData.sFinaleData.iRewardPercentagesCache[index] = sClientData.sFinaleData.iRewardPercentages[index]
		sClientData.sFinaleData.iSelectedRoleCache[index] = sClientData.sFinaleData.iSelectedRole[index]
	ENDFOR
	
	PUBLIC_GANG_OPS_SET_FINALE_ITEM_SELECTED(sClientData.sScaleformData.siMainBoard, TRUE)
	
	PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
	
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Editing - Moving into leader edit mode.")

ENDPROC

PROC PRIVATE_FINISH_GANG_OPS_FINALE_EDIT_MODE(GANG_OPS_CLIENT_FINALE_DATA &sClientData, BOOL bCancel)

	IF sClientData.sStateData.eSubState = GOBSS_VIEWING
		EXIT
	ENDIF
	
	IF bCancel
	
		INT index
		FOR index = 0 TO MAX_GANG_OPS_PLAYERS
		
			IF index >= MAX_GANG_OPS_PLAYERS
				sClientData.sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS] = sClientData.sFinaleData.iRewardPercentagesCache[MAX_GANG_OPS_PLAYERS]
				RELOOP
			ENDIF
		
			sClientData.sFinaleData.iRewardPercentages[index] = sClientData.sFinaleData.iRewardPercentagesCache[index]		
			sClientData.sFinaleData.iSelectedRole[index] = sClientData.sFinaleData.iSelectedRoleCache[index]
		ENDFOR
		
	ENDIF
	
	sClientData.sStateData.eSubState = GOBSS_VIEWING
	sClientData.sScaleformData.bUpdateInstructions = TRUE
	
	PUBLIC_GANG_OPS_SET_FINALE_ITEM_SELECTED(sClientData.sScaleformData.siMainBoard, FALSE)
	
	PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
	
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Editing - Moving out of leader edit mode.")

ENDPROC

PROC PRIVATE_CONFIRM_GANG_OPS_FINALE_EDITS(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	PRIVATE_FINISH_GANG_OPS_FINALE_EDIT_MODE(sClientData, FALSE)
	GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iSyncId++

ENDPROC

PROC PRIVATE_REVOKE_GANG_OPS_FINALE_SELECTED_MEMBERS_TEAM(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	
	IF sClientData.sFinaleData.piLeader != PLAYER_ID()
		EXIT
	ENDIF
	
	sClientData.sFinaleData.iSelectedRole[sClientData.sCursorData.iSelectedMember] = GANG_OPS_INVALID

	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Inputs - Revoked member ",sClientData.sCursorData.iSelectedMember,"'s team.")

ENDPROC

PROC PRIVATE_REVOKE_GANG_OPS_FINALE_SELECTED_MEMBERS_CUT(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	INT iModifier = sClientData.sFinaleData.iRewardPercentages[sClientData.sCursorData.iSelectedMember] * (-1)	
	PRIVATE_UPDATE_GANG_OPS_FINALE_CUTS(sClientData.sFinaleData, sClientData.sCursorData.iSelectedMember, iModifier)

	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Inputs - Revoked member ",sClientData.sCursorData.iSelectedMember,"'s reward percentage.")

ENDPROC

FUNC INT PRIVATE_GANG_OPS_GET_SELECTED_MEMBER_FROM_SELECTION_ID(INT iSelectionID)

	SWITCH iSelectionID
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_1_ROLE
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_1_CASH
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_1_STATUS
			RETURN 0
		
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_2_ROLE
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_2_CASH
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_2_STATUS
			RETURN 1
		
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_3_ROLE
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_3_CASH
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_3_STATUS
			RETURN 2
			
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_4_ROLE
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_4_CASH
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_4_STATUS
			RETURN 3
	ENDSWITCH

	// Return the leader by default
	RETURN 0
ENDFUNC

PROC PRIVATE_SET_GANG_OPS_FINALE_HIGHLIGHT_UPDATE_REQUIRED(GANG_OPS_CLIENT_FINALE_DATA &sClientData, BOOL bUpdate)
	sClientData.sCursorData.bUpdateHighlight = bUpdate
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_SET_GANG_OPS_FINALE_HIGHLIGHT_UPDATE_REQUIRED - bUpdate: ", GET_STRING_FROM_BOOL(bUpdate))
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_FINALE_PLAN_STEPS_DATA(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	TEXT_LABEL_63 tlStep, tlBlank
	INT iLoop
	
	g_GangOpsFinale.sFinaleData.iTeamStepsBeingDisplayed = sClientData.sFinaleData.iSelectedRole[sClientData.sCursorData.iSelectedMember]
	FOR iLoop = 0 TO (MAX_GANG_OPS_FINALE_STEPS-1)
		tlStep = "HP_SUB_STEP_"
		IF g_GangOpsFinale.sFinaleData.iTeamStepsBeingDisplayed = GANG_OPS_TEAM_0
			tlStep += "A"
		ELSE
			tlStep += "B"
		ENDIF
		tlStep += iLoop
		IF DOES_TEXT_LABEL_EXIST(tlStep)
			g_GangOpsFinale.sFinaleData.tlFinaleSteps[iLoop] = tlStep
		ELSE
			g_GangOpsFinale.sFinaleData.tlFinaleSteps[iLoop] = tlBlank
		ENDIF
	ENDFOR
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_FINALE_HIGHLIGHT(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	IF sClientData.sCursorData.bUpdateHighlight
	AND sClientData.sCursorData.bGetSelectionRequested
		CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_HIGHLIGHT - New input, scrap the current get selection query and grab the new one")
		sClientData.sCursorData.bGetSelectionRequested = FALSE
	ENDIF

	IF NOT sClientData.sCursorData.bGetSelectionRequested
		IF sClientData.sCursorData.bUpdateHighlight
			BEGIN_SCALEFORM_MOVIE_METHOD(sClientData.sScaleformData.siMainBoard, "GET_CURRENT_SELECTION")
			sClientData.sCursorData.currentSelectionReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
			sClientData.sCursorData.bUpdateHighlight = FALSE
			sClientData.sCursorData.bGetSelectionRequested = TRUE
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_HIGHLIGHT - Started to grab current selection, return index: ", NATIVE_TO_INT(sClientData.sCursorData.currentSelectionReturnIndex))
		ENDIF
	ELSE
		IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(sClientData.sCursorData.currentSelectionReturnIndex)
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_HIGHLIGHT - Scaleform movie method return ", NATIVE_TO_INT(sClientData.sCursorData.currentSelectionReturnIndex)," ready!")
			INT iNewHighlight = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(sClientData.sCursorData.currentSelectionReturnIndex)
			IF sClientData.sCursorData.iHighlight != iNewHighlight
				sClientData.sCursorData.iHighlight = iNewHighlight
				sClientData.sCursorData.iSelectedMember = PRIVATE_GANG_OPS_GET_SELECTED_MEMBER_FROM_SELECTION_ID(sClientData.sCursorData.iHighlight)
				
				// Submarine strand has 2 teams, we need to update the finale steps based on the team that the selected member is on
				IF sClientData.sFinaleData.iStrandId = ciGANGOPS_MISSION_STRAND_SUBMARINE
					IF sClientData.sFinaleData.iSelectedRole[sClientData.sCursorData.iSelectedMember] != sClientData.sFinaleData.iTeamStepsBeingDisplayed
						CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_HIGHLIGHT - Selected member is different to iTeamStepsBeingDisplayed, updating plan steps")
						PRIVATE_UPDATE_GANG_OPS_FINALE_PLAN_STEPS_DATA(sClientData)
						PUBLIC_SET_GANG_OPS_FINALE_STEPS_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
					ENDIF
				ENDIF
				
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
				CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_HIGHLIGHT - new highlight: ", sClientData.sCursorData.iHighlight, ", selected member: ", sClientData.sCursorData.iSelectedMember)
			ELSE
				CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_HIGHLIGHT - Highlight hasnt changed")
			ENDIF
			sClientData.sCursorData.bGetSelectionRequested = FALSE
		ELSE
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_HIGHLIGHT - Waiting for scaleform movie method return ", NATIVE_TO_INT(sClientData.sCursorData.currentSelectionReturnIndex)," to be ready...")
		ENDIF
	ENDIF
ENDPROC

PROC PRIVATE_SET_GANG_OPS_FINALE_PLAYER_ROLE_UPDATE_REQUIRED(GANG_OPS_SCALEFORM_DATA &sScaleformData, BOOL bUpdate)
	IF sScaleformData.bFinalePlayerRoleUpdate != bUpdate
		sScaleformData.bFinalePlayerRoleUpdate = bUpdate
		CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_SET_GANG_OPS_FINALE_PLAYER_ROLE_UPDATE_REQUIRED - bUpdate: ", GET_STRING_FROM_BOOL(bUpdate))
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_GANG_OPS_IS_FINALE_PLAYER_ROLE_UPDATE_REQUIRED(GANG_OPS_SCALEFORM_DATA &sScaleformData)
	RETURN sScaleformData.bFinalePlayerRoleUpdate
ENDFUNC

PROC PRIVATE_SET_GANG_OPS_FINALE_CASH_DISTRIBUTION_UPDATE_REQUIRED(GANG_OPS_SCALEFORM_DATA &sScaleformData, BOOL bUpdate)
	IF sScaleformData.bFinaleCashDistributionUpdate != bUpdate
		sScaleformData.bFinaleCashDistributionUpdate = bUpdate
		CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_SET_GANG_OPS_FINALE_CASH_DISTRIBUTION_UPDATE_REQUIRED - bUpdate: ", GET_STRING_FROM_BOOL(bUpdate))
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_GANG_OPS_IS_FINALE_CASH_DISTRIBUTION_UPDATE_REQUIRED(GANG_OPS_SCALEFORM_DATA &sScaleformData)
	RETURN sScaleformData.bFinaleCashDistributionUpdate
ENDFUNC

PROC PRIVATE_SET_GANG_OPS_FINALE_PLAYER_STATUS_UPDATE_REQUIRED(GANG_OPS_SCALEFORM_DATA &sScaleformData, BOOL bUpdate)
	IF sScaleformData.bFinalePlayerStatusUpdate != bUpdate
		sScaleformData.bFinalePlayerStatusUpdate = bUpdate
		CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_SET_GANG_OPS_FINALE_PLAYER_STATUS_UPDATE_REQUIRED - bUpdate: ", GET_STRING_FROM_BOOL(bUpdate))
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_GANG_OPS_IS_FINALE_PLAYER_STATUS_UPDATE_REQUIRED(GANG_OPS_SCALEFORM_DATA &sScaleformData)
	RETURN sScaleformData.bFinalePlayerStatusUpdate
ENDFUNC

/// PURPOSE:
///    Handle updates to the data in the current selection (player role, cut % etc)
/// PARAMS:
///    sClientData - 
PROC PRIVATE_UPDATE_GANG_OPS_FINALE_CURRENT_SELECTION(GANG_OPS_CLIENT_FINALE_DATA &sClientData, GANG_OPS_CONTROL_TYPE eControl)
	SWITCH sClientData.sCursorData.iHighlight
		// Player roles
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_1_ROLE
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_2_ROLE
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_3_ROLE
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_4_ROLE
		
			IF sClientData.sCursorData.iSelectedMember < 0
			OR sClientData.sCursorData.iSelectedMember > (MAX_GANG_OPS_PLAYERS-1)
				EXIT
			ENDIF
						
			INT iCurrentRole, iCachedRole
			iCurrentRole = GANG_OPS_INVALID	
			iCurrentRole = sClientData.sFinaleData.iSelectedRole[sClientData.sCursorData.iSelectedMember]	
			iCachedRole = iCurrentRole
			
			SWITCH eControl 
			
				CASE GOCT_LEFT
				
					IF iCurrentRole <= 0
						iCurrentRole = (g_FMMC_STRUCT.iNumberOfTeams-1)
					ELSE
						iCurrentRole--
					ENDIF
				
				BREAK
				
				CASE GOCT_RIGHT
				
					IF iCurrentRole < GANG_OPS_INVALID
						iCurrentRole = iCurrentRole
					ENDIF
					
					IF iCurrentRole >= (g_FMMC_STRUCT.iNumberOfTeams-1)
						iCurrentRole = 0
					ELSE
						iCurrentRole++
					ENDIF
				
				BREAK
				
			ENDSWITCH
			
			IF iCachedRole != iCachedRole
				iCachedRole = iCachedRole
				PLAY_SOUND_FROM_COORD(-1,"Nav_Left_Right", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ELSE
				PLAY_SOUND_FROM_COORD(-1,"Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ENDIF
		
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Inputs (edit) - Changed team from: ",
				iCachedRole, " to: ", iCurrentRole, " for member: ", sClientData.sCursorData.iSelectedMember)
		
			sClientData.sFinaleData.iSelectedRole[sClientData.sCursorData.iSelectedMember] = iCurrentRole
			
			// Submarine strand has 2 teams, we need to update the finale steps based on the team that the selected member is on
			IF sClientData.sFinaleData.iStrandId = ciGANGOPS_MISSION_STRAND_SUBMARINE
				IF sClientData.sFinaleData.iSelectedRole[sClientData.sCursorData.iSelectedMember] != sClientData.sFinaleData.iTeamStepsBeingDisplayed
					CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_CURRENT_SELECTION - Selected member's new team/role is different to iTeamStepsBeingDisplayed, updating plan steps")
					PRIVATE_UPDATE_GANG_OPS_FINALE_PLAN_STEPS_DATA(sClientData)
					PUBLIC_SET_GANG_OPS_FINALE_STEPS_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
				ENDIF
			ENDIF
			
			PRIVATE_SET_GANG_OPS_FINALE_PLAYER_ROLE_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
		BREAK
		
		// Cash distributions
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_1_CASH
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_2_CASH
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_3_CASH
		CASE GANG_OPS_FINALE_SELECTION_ID_PLAYER_4_CASH
			INT iModifier
		
			SWITCH eControl 
			
				CASE GOCT_LEFT
				
					iModifier = (GANG_OPS_FINALE_CUT_STEP) * (-1) // flip to negative for decrement.
				
				BREAK
				
				CASE GOCT_RIGHT
				
					// Check that the pot [0,1,2,3,4] has at least positive percent for the amount that will be added to the highlighted player.
					//							   ^
					
					IF sClientData.sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS] >= GANG_OPS_FINALE_CUT_STEP
						iModifier = (GANG_OPS_FINALE_CUT_STEP)
					ENDIF
				
				BREAK
				
			ENDSWITCH
			
			PRIVATE_UPDATE_GANG_OPS_FINALE_CUTS(sClientData.sFinaleData, sClientData.sCursorData.iSelectedMember, iModifier)
			
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Inputs (edit) - Changed cut with modifier: ", 
				iModifier, " for member: ", sClientData.sCursorData.iSelectedMember)
				
			INT index
			FOR index = 0 TO MAX_GANG_OPS_PLAYERS
				CDEBUG3LN(DEBUG_GANG_OPS_FIN, "  ... iRewardPercentage[",index,"] = ", sClientData.sFinaleData.iRewardPercentages[index])
			ENDFOR
				
			PRIVATE_SET_GANG_OPS_FINALE_CASH_DISTRIBUTION_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Handle updates to dpad inputs - either passing out inputs to scaleform in viewing mode, or handling updates to player roles/cash distributions whilst in edit mode.
/// PARAMS:
///    sClientData - 
///    eControl - 
PROC PRIVATE_UPDATE_GANG_OPS_FINALE_CONTROL_SELECTIONS(GANG_OPS_CLIENT_FINALE_DATA &sClientData, GANG_OPS_CONTROL_TYPE eControl)

	SWITCH eControl
	
		CASE GOCT_DOWN
		
			SWITCH sClientData.sStateData.eSubState
					
				CASE GOBSS_VIEWING
				
					PUBLIC_GANG_OPS_SET_INPUT_EVENT(sClientData.sScaleformData.siMainBoard, INPUT_FRONTEND_DOWN)

					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - DOWN.")
					
					IF sClientData.sFinaleData.piLeader = PLAYER_ID()
						IF sClientData.sFinaleData.bLaunchButtonEnabled
							IF sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_LAUNCH_BUTTON
								PLAY_SOUND_FROM_COORD(-1,"Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
							ELSE
								PLAY_SOUND_FROM_COORD(-1,"Nav_Up_Down", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
							ENDIF
						ELSE
							IF sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_1_CASH
							OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_2_CASH
							OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_3_CASH
							OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_4_CASH
								PLAY_SOUND_FROM_COORD(-1,"Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
							ELSE
								PLAY_SOUND_FROM_COORD(-1,"Nav_Up_Down", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
							ENDIF
						ENDIF
					ELSE
						IF sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_1_STATUS
						OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_2_STATUS
						OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_3_STATUS
						OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_4_STATUS
							PLAY_SOUND_FROM_COORD(-1,"Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
						ELSE
							PLAY_SOUND_FROM_COORD(-1,"Nav_Up_Down", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
						ENDIF
					ENDIF
				BREAK
				
				CASE GOBSS_EDITING
				
					// We dont need to do anything when you press down whilst editing
					PLAY_SOUND_FROM_COORD(-1,"Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					
				BREAK
			
			ENDSWITCH
		
		BREAK
			
		CASE GOCT_UP
		
			SWITCH sClientData.sStateData.eSubState
					
				CASE GOBSS_VIEWING
				
					PUBLIC_GANG_OPS_SET_INPUT_EVENT(sClientData.sScaleformData.siMainBoard, INPUT_FRONTEND_UP)
			
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - UP.")
					
					IF sClientData.sCursorData.iHighlight = 100
					OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_1_ROLE
					OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_2_ROLE
					OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_3_ROLE
					OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_4_ROLE
						PLAY_SOUND_FROM_COORD(-1,"Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					ELSE
						PLAY_SOUND_FROM_COORD(-1,"Nav_Up_Down", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					ENDIF
				
				BREAK
				
				CASE GOBSS_EDITING
				
					// We dont need to do anything when you press up whilst editing
					PLAY_SOUND_FROM_COORD(-1,"Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
				
				BREAK
			
			ENDSWITCH
			
		BREAK
			
		CASE GOCT_LEFT
		
			SWITCH sClientData.sStateData.eSubState
					
				CASE GOBSS_VIEWING
				
					PUBLIC_GANG_OPS_SET_INPUT_EVENT(sClientData.sScaleformData.siMainBoard, INPUT_FRONTEND_LEFT)
		
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - LEFT.")
					
					IF sClientData.sCursorData.iHighlight >= 100
						PLAY_SOUND_FROM_COORD(-1,"Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					ELSE
						PLAY_SOUND_FROM_COORD(-1,"Nav_Left_Right", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					ENDIF
				
				BREAK
				
				CASE GOBSS_EDITING
				
					PRIVATE_UPDATE_GANG_OPS_FINALE_CURRENT_SELECTION(sClientData, eControl)
				
				BREAK
			
			ENDSWITCH
			
		BREAK
			
			
		CASE GOCT_RIGHT
		
			SWITCH sClientData.sStateData.eSubState
					
				CASE GOBSS_VIEWING
				
					PUBLIC_GANG_OPS_SET_INPUT_EVENT(sClientData.sScaleformData.siMainBoard, INPUT_FRONTEND_RIGHT)
		
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - RIGHT.")
					
					SWITCH sClientData.sFinaleData.iTotalPlayers
						CASE 1
							IF sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_1_CASH
							OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_1_ROLE
							OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_1_STATUS
							OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_LAUNCH_BUTTON
								PLAY_SOUND_FROM_COORD(-1,"Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
							ELSE
								PLAY_SOUND_FROM_COORD(-1,"Nav_Left_Right", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
							ENDIF
						BREAK
						
						CASE 2
							IF sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_2_CASH
							OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_2_ROLE
							OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_2_STATUS
							OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_LAUNCH_BUTTON
								PLAY_SOUND_FROM_COORD(-1,"Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
							ELSE
								PLAY_SOUND_FROM_COORD(-1,"Nav_Left_Right", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
							ENDIF
						BREAK
						
						CASE 3
							IF sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_3_CASH
							OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_3_ROLE
							OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_3_STATUS
							OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_LAUNCH_BUTTON
								PLAY_SOUND_FROM_COORD(-1,"Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
							ELSE
								PLAY_SOUND_FROM_COORD(-1,"Nav_Left_Right", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
							ENDIF
						BREAK
						
						CASE 4
							IF sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_4_CASH
							OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_4_ROLE
							OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_PLAYER_4_STATUS
							OR sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_LAUNCH_BUTTON
								PLAY_SOUND_FROM_COORD(-1,"Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
							ELSE
								PLAY_SOUND_FROM_COORD(-1,"Nav_Left_Right", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
							ENDIF
						BREAK
					ENDSWITCH
				
				BREAK
				
				CASE GOBSS_EDITING
				
					PRIVATE_UPDATE_GANG_OPS_FINALE_CURRENT_SELECTION(sClientData, eControl)
				
				BREAK
			
			ENDSWITCH
			
		BREAK
			
	ENDSWITCH

ENDPROC

PROC PRIVATE_SET_GANG_OPS_FINALE_NAVIGATION(GANG_OPS_CLIENT_FINALE_DATA &sClientData, BOOL bEnable)
	IF bEnable
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_SET_GANG_OPS_NAVIGATION - bEnable")
		CALL_SCALEFORM_MOVIE_METHOD(sClientData.sScaleformData.siMainBoard, "ENABLE_NAVIGATION")
	ELSE
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_SET_GANG_OPS_NAVIGATION - !bEnable")
		CALL_SCALEFORM_MOVIE_METHOD(sClientData.sScaleformData.siMainBoard, "DISABLE_NAVIGATION")
	ENDIF
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_FINALE_ACTIVE_CHECKS(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	
	IF sClientData.sFinaleData.bInCorona
	AND g_GangOpsFinale.sFinaleData.piLeader = PLAYER_ID()
	
		INT index

		FOR index = 0 TO (sClientData.sFinaleData.iTotalPlayers-1)
			IF sClientData.sFinaleData.iPlayerOrder[index] != GANG_OPS_INVALID
				IF NOT NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(sClientData.sFinaleData.iPlayerOrder[index]))
					GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iReadyState[0] = GANG_OPS_FINALE_PLAYER_QUIT
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_ACTIVE_CHECKS - player at index ", index, " is inactive. GANG_OPS_FINALE_PLAYER_QUIT")
				ENDIF
			ENDIF
		ENDFOR
		
	ENDIF
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_FINALE_TUTORIAL(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	
	IF sClientData.sFinaleData.piLeader = PLAYER_ID()
		IF NOT HAS_PLAYER_COMPLETED_GANG_OPS_FINALE_TUTORIAL_AS_LEADER(PLAYER_ID())
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_TUTORIAL - Processing leader tutorial. Stage: ", sClientData.sTutorialData.iTutorialStage)
			SWITCH sClientData.sTutorialData.iTutorialStage
				CASE 0
					// [BOSS]
					// This is the Heist Finale screen. Here you can set what percentage take each member gets, what role they will perform and the overall plan for the job.
					PRINT_HELP("HP_FIN_LTUT1", 10000)
					GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.bRunningTutorial = TRUE
					sClientData.sTutorialData.bTutorialInProgress = TRUE
					PRIVATE_SET_GANG_OPS_FINALE_NAVIGATION(sClientData, FALSE)
					sClientData.sTutorialData.iTutorialStage++
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_TUTORIAL - HP_FIN_LTUT1 done, advancing tutorial stage")
				BREAK
				
				CASE 1
					// [BOSS AND GOON] -  CAMERA MOVE TO BAR
					// This bar shows the total take for the Heist, and how much each member will receive from the total take. The leader has a larger cut by default as they covered the initial cost of the Heist.
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_FIN_LTUT1")
					OR HAS_NET_TIMER_EXPIRED(sClientData.sTutorialData.stTutorialStageTimer, 10000)
						PRIVATE_SET_GANG_OPS_FINALE_CAMERA_AND_FOCUS(sClientData, GOCAM_CUT)
						PRINT_HELP("HP_FIN_TUT2", 10000)
						RESET_NET_TIMER(sClientData.sTutorialData.stTutorialStageTimer)
						sClientData.sTutorialData.iTutorialStage++
						CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_TUTORIAL - HP_FIN_TUT2 done, advancing tutorial stage")
					ENDIF
				BREAK
				
				CASE 2
					// [BOSS] - CAMERA MOVE TO ROLES
					// Here you can assign the role that each Heist member will perform and their percentage cut of the total take.
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_FIN_TUT2")
					OR HAS_NET_TIMER_EXPIRED(sClientData.sTutorialData.stTutorialStageTimer, 10000)
						PRIVATE_SET_GANG_OPS_FINALE_CAMERA_AND_FOCUS(sClientData, GOCAM_ROLES)
						PRINT_HELP("HP_FIN_LTUT3", 10000)
						RESET_NET_TIMER(sClientData.sTutorialData.stTutorialStageTimer)
						sClientData.sTutorialData.iTutorialStage++
						CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_TUTORIAL - HP_FIN_LTUT3 done, advancing tutorial stage")
					ENDIF
				BREAK
				
				CASE 3
					// [BOSS AND GOON] - CAMERA MOVE TO PLAN
					// You can view the plan for the selected Heist member's tasks here. These will be shown when a player has been assigned a role.
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_FIN_LTUT3")
					OR HAS_NET_TIMER_EXPIRED(sClientData.sTutorialData.stTutorialStageTimer, 10000)
						PRIVATE_SET_GANG_OPS_FINALE_CAMERA_AND_FOCUS(sClientData, GOCAM_PLAN)
						PRINT_HELP("HP_FIN_TUT4", 10000)
						RESET_NET_TIMER(sClientData.sTutorialData.stTutorialStageTimer)
						sClientData.sTutorialData.iTutorialStage++
						CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_TUTORIAL - HP_FIN_TUT4 done, advancing tutorial stage")
					ENDIF
				BREAK
				
				CASE 4
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_FIN_TUT4")
					OR HAS_NET_TIMER_EXPIRED(sClientData.sTutorialData.stTutorialStageTimer, 10000)
						PRIVATE_SET_GANG_OPS_FINALE_CAMERA_AND_FOCUS(sClientData, GOCAM_MAIN)
						SET_PLAYER_HAS_COMPLETED_GANG_OPS_FINALE_TUTORIAL_AS_LEADER()
						PRIVATE_SET_GANG_OPS_FINALE_NAVIGATION(sClientData, TRUE)
						sClientData.sTutorialData.bTutorialInProgress = FALSE
						sClientData.sTutorialData.iTutorialStage++
						sClientData.sScaleformData.bDrawInstructions = TRUE
						PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
						RESET_NET_TIMER(sClientData.sTutorialData.stTutorialStageTimer)
						GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.bRunningTutorial = FALSE
						CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_TUTORIAL - Tutorial as leader finished!")
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			IF sClientData.sTutorialData.bTutorialInProgress
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_TUTORIAL - Player has completed tutorial as leader, sClientData.sTutorialData.bTutorialInProgress = FALSE")
				sClientData.sTutorialData.bTutorialInProgress = FALSE
				PRIVATE_SET_GANG_OPS_FINALE_NAVIGATION(sClientData, TRUE)
				sClientData.sScaleformData.bDrawInstructions = TRUE
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
				GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.bRunningTutorial = FALSE
			ENDIF
		ENDIF
	ELSE
		IF NOT HAS_PLAYER_COMPLETED_GANG_OPS_FINALE_TUTORIAL_AS_MEMBER(PLAYER_ID())
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_TUTORIAL - Processing member tutorial. Stage: ", sClientData.sTutorialData.iTutorialStage)
			SWITCH sClientData.sTutorialData.iTutorialStage
				CASE 0
					// [GOON]
					// This is the Heist Finale screen. Here you can view the set-up of the Heist, and how each member will be involved.
					PRINT_HELP("HP_FIN_MTUT1", 10000)
					GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.bRunningTutorial = TRUE
					sClientData.sTutorialData.bTutorialInProgress = TRUE
					PRIVATE_SET_GANG_OPS_FINALE_NAVIGATION(sClientData, FALSE)
					sClientData.sTutorialData.iTutorialStage++
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_TUTORIAL - HP_FIN_MTUT1 done, advancing tutorial stage")
				BREAK
				
				CASE 1
					// [BOSS AND GOON] -  CAMERA MOVE TO BAR
					// This bar shows the total take for the Heist, and how much each member will receive from the total take. The leader has a larger cut by default as they covered the initial cost of the Heist.
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_FIN_MTUT1")
					OR HAS_NET_TIMER_EXPIRED(sClientData.sTutorialData.stTutorialStageTimer, 10000)
						PRIVATE_SET_GANG_OPS_FINALE_CAMERA_AND_FOCUS(sClientData, GOCAM_CUT)
						PRINT_HELP("HP_FIN_TUT2", 10000)
						RESET_NET_TIMER(sClientData.sTutorialData.stTutorialStageTimer)
						sClientData.sTutorialData.iTutorialStage++
						CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_TUTORIAL - HP_FIN_TUT2 done, advancing tutorial stage")
					ENDIF
				BREAK
				
				CASE 2
					// [OR IF GOON] - CAMERA MOVE TO ROLES
					// This section shows your Heist role configuration and your percentage cut of the total take. This is set by the Heist leader.
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_FIN_TUT2")
					OR HAS_NET_TIMER_EXPIRED(sClientData.sTutorialData.stTutorialStageTimer, 10000)
						PRIVATE_SET_GANG_OPS_FINALE_CAMERA_AND_FOCUS(sClientData, GOCAM_ROLES)
						PRINT_HELP("HP_FIN_MTUT3", 10000)
						RESET_NET_TIMER(sClientData.sTutorialData.stTutorialStageTimer)
						sClientData.sTutorialData.iTutorialStage++
						CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_TUTORIAL - HP_FIN_MTUT3 done, advancing tutorial stage")
					ENDIF
				BREAK
				
				CASE 3
					// [BOSS AND GOON] - CAMERA MOVE TO PLAN
					// You can view the plan for the selected Heist member's tasks here. These will be shown when a player has been assigned a role.
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_FIN_MTUT3")
					OR HAS_NET_TIMER_EXPIRED(sClientData.sTutorialData.stTutorialStageTimer, 10000)
						PRIVATE_SET_GANG_OPS_FINALE_CAMERA_AND_FOCUS(sClientData, GOCAM_PLAN)
						PRINT_HELP("HP_FIN_TUT4", 10000)
						RESET_NET_TIMER(sClientData.sTutorialData.stTutorialStageTimer)
						sClientData.sTutorialData.iTutorialStage++
						CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_TUTORIAL - HP_FIN_TUT4 done, advancing tutorial stage")
					ENDIF
				BREAK
				
				CASE 4
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_FIN_TUT4")
					OR HAS_NET_TIMER_EXPIRED(sClientData.sTutorialData.stTutorialStageTimer, 10000)
						PRIVATE_SET_GANG_OPS_FINALE_CAMERA_AND_FOCUS(sClientData, GOCAM_MAIN)
						SET_PLAYER_HAS_COMPLETED_GANG_OPS_FINALE_TUTORIAL_AS_MEMBER()
						PRIVATE_SET_GANG_OPS_FINALE_NAVIGATION(sClientData, TRUE)
						sClientData.sTutorialData.bTutorialInProgress = FALSE
						sClientData.sTutorialData.iTutorialStage++
						sClientData.sScaleformData.bDrawInstructions = TRUE
						PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
						RESET_NET_TIMER(sClientData.sTutorialData.stTutorialStageTimer)
						GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.bRunningTutorial = FALSE
						CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_TUTORIAL - Tutorial as member finished!")
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			IF sClientData.sTutorialData.bTutorialInProgress
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_TUTORIAL - Player has completed tutorial as member, sClientData.sTutorialData.bTutorialInProgress = FALSE")
				sClientData.sTutorialData.bTutorialInProgress = FALSE
				PRIVATE_SET_GANG_OPS_FINALE_NAVIGATION(sClientData, TRUE)
				sClientData.sScaleformData.bDrawInstructions = TRUE
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
				GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.bRunningTutorial = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF sClientData.sTutorialData.iTutorialStage < 0
	OR sClientData.sTutorialData.iTutorialStage > 4
		IF sClientData.sTutorialData.bTutorialInProgress
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_TUTORIAL - Safety catch - Players tutorial stage was out of range, clearing sClientData.sTutorialData.bTutorialInProgress and restoring navigational controls")
			sClientData.sTutorialData.bTutorialInProgress = FALSE
			PRIVATE_SET_GANG_OPS_FINALE_NAVIGATION(sClientData, TRUE)
			sClientData.sScaleformData.bDrawInstructions = TRUE
			PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.bRunningTutorial = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_FINALE_INPUTS(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	GANG_OPS_CONTROL_TYPE eControlType = PUBLIC_GET_GANG_OPS_INPUTS_THIS_FRAME_ONLY(sClientData.sInputData)
	
	IF sClientData.sFinaleData.iWarningScreen != -1
	OR sClientData.sFinaleData.iWarningScreenBuffer != -1
		PRINTLN("[finaleinput - 0]")
		EXIT
	ENDIF
	
	IF GET_CORONA_STATUS() = CORONA_STATUS_TEAM_DM
		PRINTLN("[finaleinput - 1]")
		EXIT
	ENDIF
	
	IF sClientData.sTutorialData.bTutorialInProgress
		PRINTLN("[finaleinput - 2]")
		EXIT
	ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		PRINTLN("[finaleinput - 3]")
		EXIT
	ENDIF
	
	// Dont accept inputs if the camera is moving
	IF sClientData.sCameraData.bTransitioning
		PRINTLN("[finaleinput - 4]")
		EXIT
	ENDIF
	
	SWITCH eControlType
	
		// Member Select / Map post-it select	
		CASE GOCT_UP
		CASE GOCT_DOWN
		CASE GOCT_LEFT
		CASE GOCT_RIGHT
			
			PRIVATE_UPDATE_GANG_OPS_FINALE_CONTROL_SELECTIONS(sClientData, eControlType)
			PRIVATE_SET_GANG_OPS_FINALE_HIGHLIGHT_UPDATE_REQUIRED(sClientData, TRUE)
		
			BREAK
		
		// Camera Pan
		CASE GOCT_LB
		CASE GOCT_RB
		CASE GOCT_Y
		
			PRIVATE_UPDATE_GANG_OPS_FINALE_CAMERA_AND_FOCUS_FOR_INPUT(sClientData, eControlType)
		
			BREAK
		
		// Back / Cancel Modify
		CASE GOCT_B
		
			SWITCH sClientData.sStateData.eSubState
			
				CASE GOBSS_VIEWING
				
					// TODO: Exit the whole board
					sClientData.sFinaleData.iWarningScreen = GANG_OPS_WARNING_SCREEN_LEAVE_FINALE
				
				BREAK
				
				CASE GOBSS_EDITING
				
					PRIVATE_FINISH_GANG_OPS_FINALE_EDIT_MODE(sClientData, TRUE)
				
				BREAK
			
			ENDSWITCH

			PRIVATE_UPDATE_GANG_OPS_FINALE_CONTROL_SELECTIONS(sClientData, eControlType)
			
//			PUBLIC_SET_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
			
			BREAK
			
		// Accept / Modify / Ready / Unready
		CASE GOCT_A
		
			IF sClientData.sFinaleData.piLeader = PLAYER_ID()
		
				IF sClientData.sFinaleData.bLaunchButtonEnabled = TRUE
				AND sClientData.sCursorData.iHighlight = GANG_OPS_FINALE_SELECTION_ID_LAUNCH_BUTTON
			
					PRIVATE_LAUNCH_GANG_OPS_FINALE(sClientData)
				
				ELSE
			
					IF sClientData.sCameraData.ePositionId != GOCAM_MAIN
					AND sClientData.sCameraData.ePositionId != GOCAM_OVERVIEW
						EXIT
					ENDIF
					
					// Dont allow the modify/confirm option when looking at the plan
					IF sClientData.sCursorData.iHighlight >= 100
						EXIT
					ENDIF
					
					SWITCH sClientData.sStateData.eSubState
					
						CASE GOBSS_VIEWING
						
							PRIVATE_START_GANG_OPS_FINALE_EDIT_MODE(sClientData)
						
						BREAK
						
						CASE GOBSS_EDITING
						
							PRIVATE_CONFIRM_GANG_OPS_FINALE_EDITS(sClientData)
						
						BREAK
					
					ENDSWITCH
				
				ENDIF
			
			ELSE
				PRIVATE_TOGGLE_GANG_OPS_FINALE_READY_STATE(sClientData)
			ENDIF
			
//			PUBLIC_SET_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)

			BREAK
			
		// View Profile
		CASE GOCT_SELECT
	
			IF sClientData.sStateData.eSubState != GOBSS_EDITING
				
				PLAYER_INDEX playerID
				GAMER_HANDLE ghSelectedPlayer	
				playerID = INT_TO_PLAYERINDEX(sClientData.sFinaleData.iPlayerOrder[sClientData.sCursorData.iSelectedMember])

				CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Inputs - Loading player profile: ", GET_PLAYER_NAME(playerID))
				
	            ghSelectedPlayer = GET_GAMER_HANDLE_PLAYER(playerID)
	            PLAY_SOUND_FROM_COORD(-1, "Select", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
	            NETWORK_SHOW_PROFILE_UI(ghSelectedPlayer)
				
			ENDIF

		BREAK
		
		#IF IS_DEBUG_BUILD
		CASE GOCT_SOLO_FINALE
		
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Inputs - Shift Numpad 1 - force launch the finale")
		
			sClientData.sFinaleData.iReadyState[0] = GANG_OPS_FINALE_READY
			GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iReadyState[0] = GANG_OPS_FINALE_READY
		
		BREAK
		#ENDIF
		
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Get the headset/voice state of the player passed in. Based loosely on the voice controls used in the corona.
FUNC INT PRIVATE_GET_GANG_OPS_FINALE_PLAYER_HEADSET_STATUS(PLAYER_INDEX playerID)
		
	GAMER_HANDLE ghTargetPlayer = GET_GAMER_HANDLE_PLAYER(playerID)
	
	// Is the gamer we are going to check valid?
	IF IS_GAMER_HANDLE_VALID(ghTargetPlayer)
	
		// Check to see if we can communicate with the target player.
		IF NETWORK_CAN_COMMUNICATE_WITH_GAMER(ghTargetPlayer)
			IF NETWORK_IS_GAMER_TALKING(ghTargetPlayer)
				RETURN ENUM_TO_INT(ACTIVE_HEADSET)
			ELIF NETWORK_IS_GAMER_MUTED_BY_ME(ghTargetPlayer)
				RETURN ENUM_TO_INT(MUTED_HEADSET)
			ELSE
				RETURN ENUM_TO_INT(INACTIVE_HEADSET)
			ENDIF
		ELSE
			IF NETWORK_IS_GAMER_MUTED_BY_ME(ghTargetPlayer)
				RETURN ENUM_TO_INT(MUTED_HEADSET)
			ELSE
				RETURN ENUM_TO_INT(ICON_EMPTY)
			ENDIF
		ENDIF
	ENDIF	
	
	RETURN ENUM_TO_INT(ICON_EMPTY)
	
ENDFUNC

/// PURPOSE:
///    Get the total number of players whose role is X.
/// PARAMS:
///    eRole - The role to count.
/// RETURNS:
///    INT - Total number of players marked with that role.
PROC PRIVATE_GET_GANG_OPS_FINALE_TOTAL_IN_TEAM(GANG_OPS_CLIENT_FINALE_DATA &sClientData, INT &iTeamNumbers[])

	INT index

	FOR index = 0 TO (MAX_GANG_OPS_PLAYERS-1)
		IF sClientData.sFinaleData.iPlayerOrder[index] != -1
			SWITCH (sClientData.sFinaleData.iSelectedRole[index])
				CASE 0 iTeamNumbers[0]++ BREAK
				CASE 1 iTeamNumbers[1]++ BREAK
				CASE 2 iTeamNumbers[2]++ BREAK
				CASE 3 iTeamNumbers[3]++ BREAK
			ENDSWITCH
		ENDIF
	ENDFOR
	
ENDPROC


/// PURPOSE:
///    Get the remaining number of roles for the provided team.
/// PARAMS:
///    iTeam - Team to check.
/// RETURNS:
///    INT number of slots available on the team.
FUNC INT PRIVATE_GET_GANG_OPS_REMAINING_ROLES_FOR_TEAM(GANG_OPS_CLIENT_FINALE_DATA &sClientData, INT iTeam)

	IF iTeam < 0
	OR iTeam > (FMMC_MAX_TEAMS-1)
		RETURN 0
	ENDIF

	// Check for the minimum team requirements.
	INT iTeamTotals[FMMC_MAX_TEAMS]
	INT iRemainingTeam, index
	
	PRIVATE_GET_GANG_OPS_FINALE_TOTAL_IN_TEAM(sClientData, iTeamTotals)
	
	FOR index = 0 TO (FMMC_MAX_TEAMS-1)
		IF index < g_FMMC_STRUCT.iNumberOfTeams
			IF index = iTeam
				// Found the players team, it is also valid.
				iRemainingTeam = (g_FMMC_STRUCT.iNumPlayersPerTeam[index] - iTeamTotals[iTeam])
			ENDIF
		ENDIF
	ENDFOR
	
	IF iRemainingTeam < 0
		iRemainingTeam = 0
	ENDIF
	
	RETURN iRemainingTeam

ENDFUNC

FUNC INT PRIVATE_GET_GANG_OPS_FINALE_TEAM_VALIDITY(GANG_OPS_CLIENT_FINALE_DATA &sClientData, INT iPlayerTeam)
	
	IF iPlayerTeam < 0
	OR iPlayerTeam > (FMMC_MAX_TEAMS-1)
		RETURN GANG_OPS_FINALE_TEAM_STATUS_INVALID
	ENDIF
	
	// Check for the minimum team requirements.
	INT iTeamTotals[FMMC_MAX_TEAMS]
	
	PRIVATE_GET_GANG_OPS_FINALE_TOTAL_IN_TEAM(sClientData, iTeamTotals)
	
	IF iTeamTotals[iPlayerTeam] <= g_FMMC_STRUCT.iNumPlayersPerTeam[iPlayerTeam]
		RETURN GANG_OPS_FINALE_TEAM_STATUS_VALID
	ELSE
		RETURN GANG_OPS_FINALE_TEAM_STATUS_INVALID
	ENDIF

ENDFUNC 

PROC PRIVATE_UPDATE_GANG_OPS_FINALE_MEMBERS(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	PLAYER_INDEX playerID
	TEXT_LABEL_23 tlPortrait
	TEXT_LABEL_15 tlPlayerRole
	INT index, iMarker
	FLOAT fZoomScale
	VECTOR vMarkerCoords, vZoomCoords
	BOOL bIsLocal, bStatus
	GANG_OPS_MISSION_ENUM eMission
	
	BOOL bUpdatePlayerRoles		
	BOOL bUpdateCashDistribution
	BOOL bUpdatePlayerStatus	
	BOOL bUpdateAll				
	BOOL bUpdateAny 
	BOOL bDoFinalUpdate = FALSE
		
	FOR index = 0 TO (MAX_GANG_OPS_PLAYERS-1)
	
		IF sClientData.sFinaleData.iPlayerOrder[index] = GANG_OPS_INVALID
			RELOOP
		ENDIF
		
		playerID = INT_TO_PLAYERINDEX(sClientData.sFinaleData.iPlayerOrder[index])
		
		// Retrieve the portrait for the player.
		tlPortrait = PRIVATE_GET_GANG_OPS_FINALE_HEADSHOT(sClientData, playerID)
		
		// Retrieve role information
		tlPlayerRole = PRIVATE_GET_GANG_OPS_FINALE_ROLE(sClientData, index)
		
		// Retrieve status of player (either READY or NOT READY).
		bStatus = PRIVATE_GET_GANG_OPS_FINALE_STATUS(sClientData, playerID)
				
		// Is this a local player?
		bIsLocal = (playerID = PLAYER_ID())
		
		bUpdatePlayerRoles		= PRIVATE_GANG_OPS_IS_FINALE_PLAYER_ROLE_UPDATE_REQUIRED(sClientData.sScaleformData)
		bUpdateCashDistribution	= PRIVATE_GANG_OPS_IS_FINALE_CASH_DISTRIBUTION_UPDATE_REQUIRED(sClientData.sScaleformData)		
		bUpdatePlayerStatus		= PRIVATE_GANG_OPS_IS_FINALE_PLAYER_STATUS_UPDATE_REQUIRED(sClientData.sScaleformData)		
		bUpdateAll				= PUBLIC_IS_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData)		
		bUpdateAny 				= bUpdatePlayerRoles OR bUpdateCashDistribution OR bUpdatePlayerStatus OR bUpdateAll		

		IF NOT bUpdateAny
			RELOOP
		ELSE
			bDoFinalUpdate = TRUE
		ENDIF
		
		IF bUpdateAll
			PUBLIC_GANG_OPS_UPDATE_FINALE_PLAYER_DETAILS(sClientData.sScaleformData.siMainBoard,
												index, GET_PLAYER_NAME(playerID),
												tlPortrait, bIsLocal)
												
			PUBLIC_GANG_OPS_UPDATE_FLAVOUR_IMAGES(	sClientData.sScaleformData.siMainBoard,
												GET_TEXTURE_DICT(sClientData.sFinaleData.iStrandID),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_TOP, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_1, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_2, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_3, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_4, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_5, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_RIGHT_1, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_RIGHT_2, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_RIGHT_3, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_BOTTOM, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE)		)
		ENDIF
		
		IF bUpdatePlayerRoles
		OR bUpdateAll
			PUBLIC_GANG_OPS_UPDATE_FINALE_PLAYER_ROLE(sClientData.sScaleformData.siMainBoard,
												index, tlPlayerRole)
		ENDIF

		IF bUpdatePlayerStatus
		OR bUpdateAll
			PUBLIC_GANG_OPS_UPDATE_FINALE_PLAYER_STATUS(sClientData.sScaleformData.siMainBoard,
														index, bStatus)
		ENDIF
		
	ENDFOR
	
	IF bDoFinalUpdate
		PUBLIC_GANG_OPS_UPDATE_FINALE_PLAYER_CASH_DISTRIBUTION(sClientData.sScaleformData.siMainBoard,
																PRIVATE_GET_GANG_OPS_CUT_VALUE(sClientData.sFinaleData, 0),
																PRIVATE_GET_GANG_OPS_CUT_VALUE(sClientData.sFinaleData, 1),
																PRIVATE_GET_GANG_OPS_CUT_VALUE(sClientData.sFinaleData, 2),
																PRIVATE_GET_GANG_OPS_CUT_VALUE(sClientData.sFinaleData, 3),
																PRIVATE_GET_GANG_OPS_CUT_VALUE(sClientData.sFinaleData, 4))
	ENDIF
	
	IF PUBLIC_IS_GANG_OPS_MAP_MARKERS_UPDATE_REQUIRED(sClientData.sScaleformData)
		eMission = GET_GANG_OPS_STRAND_FINALE(GET_GANG_OPS_MISSION_STRAND_FROM_CONST(sClientData.sFinaleData.iStrandId))
		
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MAP_MARKER] PRIVATE_UPDATE_GANG_OPS_FINALE_MEMBERS - Setting up map zooming/scaling for ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMission))
		
		// Zoom the map if necessary
		
		vZoomCoords = GET_GANG_OPS_MISSION_MAP_ZOOM_COORDS(eMission)
		fZoomScale = GET_GANG_OPS_MISSION_MAP_ZOOM_SCALE(eMission)
		
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MAP_MARKER] PRIVATE_UPDATE_GANG_OPS_FINALE_MEMBERS - Zoom Coords: ", vZoomCoords, "; Zoom scale: ", fZoomScale)
		
		PUBLIC_GANG_OPS_SET_MAP_DISPLAY(sClientData.sScaleformData.siMainBoard,
										CEIL(vZoomCoords.x),
										CEIL(vZoomCoords.y),
										fZoomScale,
										FALSE)
		
		IF NOT ARE_VECTORS_EQUAL(vZoomCoords, sClientData.sCameraData.vLastMapZoomCoords)
			IF sClientData.sAudioData.bStartedLoopingBackgroundSound
				IF sClientData.sAudioData.iLoopingBackgroundSoundId != -1
					SET_VARIABLE_ON_SOUND(sClientData.sAudioData.iLoopingBackgroundSoundId, "x", vZoomCoords.x)
					SET_VARIABLE_ON_SOUND(sClientData.sAudioData.iLoopingBackgroundSoundId, "y", vZoomCoords.y)
					SET_VARIABLE_ON_SOUND(sClientData.sAudioData.iLoopingBackgroundSoundId, "scale", fZoomScale)
				ENDIF
			ENDIF
			sClientData.sCameraData.vLastMapZoomCoords = vZoomCoords
		ENDIF
		
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MAP_MARKER] PRIVATE_UPDATE_GANG_OPS_FINALE_MEMBERS - Setting up markers for ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMission))
		
		PUBLIC_GANG_OPS_REMOVE_ALL_MAP_MARKERS(sClientData.sScaleformData.siMainBoard)
		
		FOR iMarker = 0 TO (MAX_GANG_OPS_MAP_MARKERS-1)
			vMarkerCoords = GET_GANG_OPS_MISSION_MAP_MARKER_COORDS(eMission, iMarker)
			IF NOT IS_VECTOR_ZERO(vMarkerCoords)
				PUBLIC_GANG_OPS_ADD_MAP_MARKER(sClientData.sScaleformData.siMainBoard, iMarker, vMarkerCoords, iMarker)
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MAP_MARKER] PRIVATE_UPDATE_GANG_OPS_FINALE_MEMBERS - Adding marker ", iMarker, " at coords: ", vMarkerCoords)
			ENDIF
		ENDFOR
		
		
		PUBLIC_SET_GANG_OPS_MAP_MARKERS_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)
	ENDIF
	
	IF PUBLIC_IS_GANG_OPS_FINALE_STEPS_UPDATE_REQUIRED(sClientData.sScaleformData)
	
		PUBLIC_GANG_OPS_UPDATE_FINALE_STEPS(sClientData.sScaleformData.siMainBoard, sClientData.sFinaleData.tlFinaleSteps)
	
		PUBLIC_SET_GANG_OPS_FINALE_STEPS_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)
	ENDIF
	
	PRIVATE_SET_GANG_OPS_FINALE_PLAYER_ROLE_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)
	PRIVATE_SET_GANG_OPS_FINALE_CASH_DISTRIBUTION_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)
	PRIVATE_SET_GANG_OPS_FINALE_PLAYER_STATUS_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)
	PUBLIC_SET_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)

ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_FINALE_LEADER_ONLY(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	PLAYER_INDEX playerID
	TEXT_LABEL_23 tlPortrait
	TEXT_LABEL_15 tlPlayerRole
	BOOL bIsLocal, bStatus
	
	BOOL bUpdatePlayerRoles			= PRIVATE_GANG_OPS_IS_FINALE_PLAYER_ROLE_UPDATE_REQUIRED(sClientData.sScaleformData)
	BOOL bUpdateCashDistribution	= PRIVATE_GANG_OPS_IS_FINALE_CASH_DISTRIBUTION_UPDATE_REQUIRED(sClientData.sScaleformData)
	BOOL bUpdatePlayerStatus		= PRIVATE_GANG_OPS_IS_FINALE_PLAYER_STATUS_UPDATE_REQUIRED(sClientData.sScaleformData)
	BOOL bUpdateAll					= PUBLIC_IS_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData)
	BOOL bUpdateAny 				= bUpdatePlayerRoles OR bUpdateCashDistribution OR bUpdatePlayerStatus OR bUpdateAll
			
	playerID = sClientData.sFinaleData.piLeader
	
	// Retrieve the portrait for the player.
	tlPortrait = PRIVATE_GET_GANG_OPS_FINALE_HEADSHOT(sClientData, playerID)
	
	// Retrieve role information
	tlPlayerRole = "HEIST_RL_NONE"
	
	// Retrieve status of player (either READY or NOT READY).
	bStatus = FALSE
			
	// Is this a local player?
	bIsLocal = (playerID = PLAYER_ID())
	
	IF NOT bUpdateAny
		EXIT
	ENDIF
	
	IF bUpdateAll
		PUBLIC_GANG_OPS_UPDATE_FINALE_PLAYER_DETAILS(sClientData.sScaleformData.siMainBoard,
											0, GET_PLAYER_NAME(playerID),
											tlPortrait, bIsLocal)
											
		PUBLIC_GANG_OPS_REMOVE_FINALE_PLAYER(sClientData.sScaleformData.siMainBoard, 1)
		PUBLIC_GANG_OPS_REMOVE_FINALE_PLAYER(sClientData.sScaleformData.siMainBoard, 2)
		PUBLIC_GANG_OPS_REMOVE_FINALE_PLAYER(sClientData.sScaleformData.siMainBoard, 3)
		
		PUBLIC_GANG_OPS_UPDATE_FLAVOUR_IMAGES(	sClientData.sScaleformData.siMainBoard,
												GET_TEXTURE_DICT(sClientData.sFinaleData.iStrandID),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_TOP, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_1, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_2, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_3, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_4, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_5, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_RIGHT_1, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_RIGHT_2, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_RIGHT_3, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_BOTTOM, sClientData.sCursorData.iHighlight, sClientData.sFinaleData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_FINALE)		)
	ENDIF
	
	IF bUpdatePlayerRoles
	OR bUpdateAll
		PUBLIC_GANG_OPS_UPDATE_FINALE_PLAYER_ROLE(sClientData.sScaleformData.siMainBoard,
											0, tlPlayerRole)
	ENDIF

	IF bUpdatePlayerStatus
	OR bUpdateAll
		PUBLIC_GANG_OPS_UPDATE_FINALE_PLAYER_STATUS(sClientData.sScaleformData.siMainBoard,
													0, bStatus)
	ENDIF
	
	PRIVATE_SET_GANG_OPS_FINALE_PLAYER_ROLE_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)
	PRIVATE_SET_GANG_OPS_FINALE_CASH_DISTRIBUTION_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)
	PRIVATE_SET_GANG_OPS_FINALE_PLAYER_STATUS_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)
	PUBLIC_SET_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)

ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_FINALE_BOARD(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	IF sClientData.sFinaleData.bInCorona
		PRIVATE_UPDATE_GANG_OPS_FINALE_MEMBERS(sClientData)
	ELSE
		PRIVATE_UPDATE_GANG_OPS_FINALE_LEADER_ONLY(sClientData)
	ENDIF
	
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_FINALE_WARNING_SCREENS(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	IF sClientData.sFinaleData.iWarningScreen = -1
		EXIT
	ENDIF

	BOOL bConfirm
	IF PUBLIC_GANG_OPS_MAINTAIN_WARNING_SCREEN(sClientData.sFinaleData.iWarningScreen, bConfirm)
		CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_WARNING_SCREENS - Warning screen done!")
	
		IF bConfirm
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_WARNING_SCREENS - Player has opted to quit the finale board")
			GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].GangOps.bQuitFromFinaleBoard = TRUE
			KILL_FINALE_READY_SOUND(sClientData)
		ELSE
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_WARNING_SCREENS - Player has opted to remain")
		ENDIF
		
		sClientData.sFinaleData.iWarningScreen = -1
		sClientData.sFinaleData.iWarningScreenBuffer = 0
	ENDIF
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_FINALE_LAUNCH_REQUIREMENTS(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	
	IF sClientData.sFinaleData.piLeader != PLAYER_ID()
		EXIT
	ENDIF
	
	BOOL bCanLaunch = TRUE
	BOOL bAllReady = TRUE
	BOOL bShowTutorialHelp = FALSE
	INT index

	// Leader has already selected to launch, no need to run checks.
	IF sClientData.sFinaleData.iReadyState[0] = GANG_OPS_FINALE_READY
		EXIT
	ENDIF

	// Requirement checks in order of computational expense.

	IF sClientData.sStateData.eSubState = GOBSS_EDITING
		bCanLaunch = FALSE
		
		CDEBUG2LN(DEBUG_GANG_OPS_FIN, "Launch Verify - FAILED - In Edit mode.")
		
	ENDIF
	
	IF bCanLaunch
		
		FOR index = 0 TO (MAX_GANG_OPS_PLAYERS-1)
		
			IF sClientData.sFinaleData.iPlayerOrder[index] = GANG_OPS_INVALID
				RELOOP
			ENDIF
		
			IF sClientData.sFinaleData.bRunningTutorial[index] = TRUE
				bCanLaunch = FALSE
				bShowTutorialHelp = TRUE
				
				CDEBUG2LN(DEBUG_GANG_OPS_FIN, "Launch Verify - FAILED - Player is running tutorial: ", index)
				
				BREAKLOOP
			ENDIF
		
		ENDFOR
		
	ENDIF
	
	IF NOT sClientData.sTutorialData.bTutorialInProgress
		IF bShowTutorialHelp
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_TUT_WAIT")
				PRINT_HELP_FOREVER("HP_TUT_WAIT")
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_TUT_WAIT")
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
	
	IF bCanLaunch
	
		IF sClientData.sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS] > 0
		
			IF NOT IS_BIT_SET(sClientData.iFinaleBitSet, GANG_OPS_BITSET_SHOWN_CUT_PERCENTAGE_HELP)
				PRINT_HELP("HP_FIN_EXPLCUT")
				SET_BIT(sClientData.iFinaleBitSet, GANG_OPS_BITSET_SHOWN_CUT_PERCENTAGE_HELP)
			ENDIF
		
			bCanLaunch = FALSE
			
			CDEBUG2LN(DEBUG_GANG_OPS_FIN, "Launch Verify - FAILED - Money still in kitty.")
		ENDIF
		
	ENDIF
	
	IF bCanLaunch
	
		FOR index = 1 TO (MAX_GANG_OPS_PLAYERS-1)
			
			IF sClientData.sFinaleData.iPlayerOrder[index] = GANG_OPS_INVALID
				RELOOP
			ENDIF
			
			IF NOT PRIVATE_IS_GANG_OPS_FINALE_PLAYER_READY(	sClientData, 
															INT_TO_PLAYERINDEX(sClientData.sFinaleData.iPlayerOrder[index]))
				bCanLaunch = FALSE
				bAllReady = FALSE
				
				CDEBUG2LN(DEBUG_GANG_OPS_FIN, "Launch Verify - FAILED - Player undready: ", index)
				
				BREAKLOOP
			ENDIF
				
		ENDFOR
	
	ENDIF

	IF bCanLaunch
	
		FOR index = 0 TO (MAX_GANG_OPS_PLAYERS-1)
		
			IF sClientData.sFinaleData.iPlayerOrder[index] = GANG_OPS_INVALID
				RELOOP
			ENDIF
		
			IF sClientData.sFinaleData.iSelectedRole[index] = GANG_OPS_INVALID
				bCanLaunch = FALSE
				
				CDEBUG2LN(DEBUG_GANG_OPS_FIN, "Launch Verify - FAILED - Player has no role: ", index)
				
				BREAKLOOP
			ENDIF
		
		ENDFOR
	
	ENDIF

	IF bCanLaunch
	AND bAllReady
	
		INT iTeamTotals[FMMC_MAX_TEAMS]
		PRIVATE_GET_GANG_OPS_FINALE_TOTAL_IN_TEAM(sClientData, iTeamTotals)

		FOR index = 0 TO (FMMC_MAX_TEAMS-1)
		
			IF index >= g_FMMC_STRUCT.iNumberOfTeams
				RELOOP
			ENDIF
			
			IF iTeamTotals[index] < g_FMMC_STRUCT.iNumPlayersPerTeam[index]
			OR iTeamTotals[index] > g_FMMC_STRUCT.iMaxNumPlayersPerTeam[index]	
			
//				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_3") // TODO: Replace label
//					PRINT_HELP_FOREVER("HEIST_NOTE_3") // TODO: Replace label
//				ENDIF
				
				CDEBUG2LN(DEBUG_GANG_OPS_FIN, "Launch Verify - FAILED - Failed team limit check.")	
				
				bCanLaunch = FALSE
		
			ENDIF
		
		ENDFOR

	ENDIF
	
	IF bCanLaunch
	
		IF NOT bAllReady
			bCanLaunch = FALSE	
		ENDIF
		
	ENDIF
			
	// Requirements processed, perform actions based on the result.
	
	IF bCanLaunch
		IF NOT sClientData.sFinaleData.bLaunchButtonEnabled
			PLAY_FINALE_READY_SOUND(sClientData)
			PUBLIC_GANG_OPS_SET_FINALE_LAUNCH_BUTTON_ENABLED(sClientData.sScaleformData.siMainBoard, TRUE, FALSE)
			sClientData.sFinaleData.bLaunchButtonEnabled = TRUE
			PRINT_HELP("HPFIN_RTL")
			PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
		ENDIF
	ELSE
		IF sClientData.sFinaleData.bLaunchButtonEnabled
			KILL_FINALE_READY_SOUND(sClientData)
			PUBLIC_GANG_OPS_SET_FINALE_LAUNCH_BUTTON_ENABLED(sClientData.sScaleformData.siMainBoard, FALSE, FALSE)
			sClientData.sFinaleData.bLaunchButtonEnabled = FALSE
			PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
		ENDIF
	ENDIF
	
ENDPROC

PROC PRIVATE_UNLOAD_GANG_OPS_FINALE_SCRIPT_RESOURCES(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_PLANNING_BOARD")
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sClientData.sScaleformData.siMainBoard)
	
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Cleanup - Released all script assets and finished cleanup.")

ENDPROC


PROC PRIVATE_UPDATE_GANG_OPS_FINALE_NET_DATA(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	IF NOT IS_PLAYER_IN_CORONA()
		EXIT
	ENDIF
	
	IF GET_CORONA_STATUS() = CORONA_STATUS_GANG_OPS_HEIST_PLANNING
		IF g_GangOpsFinale.sFinaleData.piLeader = INVALID_PLAYER_INDEX()
		OR NOT NETWORK_IS_PLAYER_ACTIVE(g_GangOpsFinale.sFinaleData.piLeader)
			EXIT
		ENDIF
	ENDIF
	
	INT iLocalPlayer, iTracker, iLeader, iPlayer
	iLocalPlayer = NETWORK_PLAYER_ID_TO_INT()

	IF sClientData.sFinaleData.piLeader = PLAYER_ID()

		BOOL bUpdateRequired = FALSE
		BOOL bUpdatePlayerRoleRequired = FALSE
		BOOL bUpdatePlayerStatusRequired = FALSE
		BOOL bUpdateCashDistributionRequired = FALSE
	
		FOR iTracker = 0 TO MAX_GANG_OPS_PLAYERS
			
			// Stuff that needs 5 array slots
			IF iTracker >= MAX_GANG_OPS_PLAYERS
				IF GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iRewardPercentages[MAX_GANG_OPS_PLAYERS] != sClientData.sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS]
					GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iRewardPercentages[MAX_GANG_OPS_PLAYERS] = sClientData.sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS]
					bUpdateCashDistributionRequired = TRUE
					CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] bUpdateCashDistributionRequired = TRUE [1]")
				ENDIF
				
				RELOOP
			ENDIF
			
			// Stuff that needs 4 array slots
			IF GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iRewardPercentages[iTracker] != sClientData.sFinaleData.iRewardPercentages[iTracker]
				GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iRewardPercentages[iTracker] = sClientData.sFinaleData.iRewardPercentages[iTracker]
				IF sClientData.sFinaleData.iPlayerOrder[iTracker] = NETWORK_PLAYER_ID_TO_INT()
					g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent = GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iRewardPercentages[iTracker]
					g_sJobGangopsInfo.m_infos.m_leaderscashcutpercentage = g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent
					g_sJobGangopsInfo.m_infos.m_cashcutpercentage = g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent
					CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] g_sJobGangopsInfo.m_infos.m_leaderscashcutpercentage = ", g_sJobGangopsInfo.m_infos.m_leaderscashcutpercentage)
					CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] g_sJobGangopsInfo.m_infos.m_cashcutpercentage = ", g_sJobGangopsInfo.m_infos.m_cashcutpercentage)
				ENDIF
				bUpdateCashDistributionRequired = TRUE
				CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] bUpdateCashDistributionRequired = TRUE [2]")
			ENDIF
			
			IF GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iPlayerOrder[iTracker] != sClientData.sFinaleData.iPlayerOrder[iTracker]
				GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iPlayerOrder[iTracker] = sClientData.sFinaleData.iPlayerOrder[iTracker]
				bUpdateRequired = TRUE
				CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] bUpdateRequired = TRUE ")
			ENDIF
			
			IF GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iSelectedRole[iTracker] != sClientData.sFinaleData.iSelectedRole[iTracker]
				GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iSelectedRole[iTracker] = sClientData.sFinaleData.iSelectedRole[iTracker]
				IF iLocalPlayer = iTracker
					IF sClientData.sFinaleData.iSelectedRole[iTracker] != -1
					AND NOT IS_CORONA_BIT_SET(CORONA_HOST_HAS_LOCKED_TEAMS)
						GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = sClientData.sFinaleData.iSelectedRole[iTracker]
						CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Update corona team chosen (leader): GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
					ENDIF
				ENDIF
				bUpdatePlayerRoleRequired = TRUE
				CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] bUpdatePlayerRoleRequired = TRUE ")
			ENDIF
			
			// Ready state is set by the client, so the leader must read instead of write.
			FOR iPlayer = 0 TO (NUM_NETWORK_PLAYERS-1)
				IF sClientData.sFinaleData.iPlayerOrder[iTracker] != iPlayer
				OR sClientData.sFinaleData.iPlayerOrder[iTracker] = NATIVE_TO_INT(sClientData.sFinaleData.piLeader)
					RELOOP
				ENDIF
					
				IF sClientData.sFinaleData.iReadyState[iTracker] != GlobalplayerBD_FM_2[iPlayer].GangOps.iReadyState[iTracker]
					sClientData.sFinaleData.iReadyState[iTracker] = GlobalplayerBD_FM_2[iPlayer].GangOps.iReadyState[iTracker]
					GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iReadyState[iTracker] = sClientData.sFinaleData.iReadyState[iTracker]
					bUpdatePlayerStatusRequired = TRUE
					CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] bUpdatePlayerStatusRequired = TRUE ")
				ENDIF
				
				IF sClientData.sFinaleData.bRunningTutorial[iTracker] != GlobalplayerBD_FM_2[iPlayer].GangOps.bRunningTutorial
					sClientData.sFinaleData.bRunningTutorial[iTracker] = GlobalplayerBD_FM_2[iPlayer].GangOps.bRunningTutorial
					GlobalplayerBD_FM_2[iLocalPlayer].GangOps.bTutorialInProgress[iTracker] = sClientData.sFinaleData.bRunningTutorial[iTracker]
					CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] member ", iTracker, " is doing tutorial")
					GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iSyncId++
				ENDIF
			ENDFOR
			
		ENDFOR
		
		IF bUpdateRequired
			GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iSyncId++
			PUBLIC_SET_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Net Sync (L) - Net update all available, updating iSyncId: ", GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iSyncId)
		ENDIF
		
		IF bUpdatePlayerRoleRequired
			GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iSyncId++
			PRIVATE_SET_GANG_OPS_FINALE_PLAYER_ROLE_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Net Sync (L) - Net update player role available, updating iSyncId: ", GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iSyncId)
		ENDIF
		
		IF bUpdateCashDistributionRequired
			GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iSyncId++
			PRIVATE_SET_GANG_OPS_FINALE_CASH_DISTRIBUTION_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Net Sync (L) - Net update cash distribution available, updating iSyncId: ", GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iSyncId)
		ENDIF
		
		IF bUpdatePlayerStatusRequired
			GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iSyncId++
			PRIVATE_SET_GANG_OPS_FINALE_PLAYER_STATUS_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Net Sync (L) - Net update player status available, updating iSyncId: ", GlobalplayerBD_FM_2[iLocalPlayer].GangOps.iSyncId)
		ENDIF
	
	ELSE
	
		INT iMySlot
	
		iLeader = NATIVE_TO_INT(sClientData.sFinaleData.piLeader)
	
		IF iLeader <= GANG_OPS_INVALID
			EXIT
		ENDIF
		
		// TODO: Does this need a search loop?
		// Ready state is set by the client, so push the data ready for the leader to pick it up.
		FOR iTracker = 0 TO (MAX_GANG_OPS_PLAYERS-1)
			IF sClientData.sFinaleData.iPlayerOrder[iTracker] != NETWORK_PLAYER_ID_TO_INT()
				RELOOP
			ENDIF
			
			GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iReadyState[iTracker] = sClientData.sFinaleData.iReadyState[iTracker]
			iMySlot = iTracker
		ENDFOR
		
		IF GlobalplayerBD_FM_2[iLeader].GangOps.iSyncId > sClientData.sFinaleData.iSyncId
			
			FOR iTracker = 0 TO MAX_GANG_OPS_PLAYERS
						
				// Stuff that needs 5 array slots
				IF iTracker >= MAX_GANG_OPS_PLAYERS
					IF sClientData.sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS] != GlobalplayerBD_FM_2[iLeader].GangOps.iRewardPercentages[MAX_GANG_OPS_PLAYERS]
						sClientData.sFinaleData.iRewardPercentages[MAX_GANG_OPS_PLAYERS] = GlobalplayerBD_FM_2[iLeader].GangOps.iRewardPercentages[MAX_GANG_OPS_PLAYERS]
					ENDIF
					
					RELOOP
				ENDIF

				// Stuff that needs 4 array slots
				IF sClientData.sFinaleData.iRewardPercentages[iTracker] != GlobalplayerBD_FM_2[iLeader].GangOps.iRewardPercentages[iTracker]
					sClientData.sFinaleData.iRewardPercentages[iTracker] = GlobalplayerBD_FM_2[iLeader].GangOps.iRewardPercentages[iTracker]
					
					IF sClientData.sFinaleData.iPlayerOrder[iTracker] = iLeader
						g_sJobGangopsInfo.m_infos.m_leaderscashcutpercentage = sClientData.sFinaleData.iRewardPercentages[iTracker]
						CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Update leader cut percent (not leader): ", g_sJobGangopsInfo.m_infos.m_leaderscashcutpercentage)
					ENDIF
					
					IF iMySlot = iTracker
						g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent = GlobalplayerBD_FM_2[iLeader].GangOps.iRewardPercentages[iTracker]
						g_sJobGangopsInfo.m_infos.m_cashcutpercentage = g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent
						CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Update my cut percent (not leader): ", g_sJobGangopsInfo.m_infos.m_cashcutpercentage)
					ENDIF
				ENDIF
				
				IF sClientData.sFinaleData.iPlayerOrder[iTracker] != GlobalplayerBD_FM_2[iLeader].GangOps.iPlayerOrder[iTracker]
					sClientData.sFinaleData.iPlayerOrder[iTracker] = GlobalplayerBD_FM_2[iLeader].GangOps.iPlayerOrder[iTracker]
				ENDIF
				
				IF sClientData.sFinaleData.iReadyState[iTracker] != GlobalplayerBD_FM_2[iLeader].GangOps.iReadyState[iTracker]
					IF iMySlot != iTracker
						sClientData.sFinaleData.iReadyState[iTracker] = GlobalplayerBD_FM_2[iLeader].GangOps.iReadyState[iTracker]
					ENDIF
				ENDIF
				
				IF sClientData.sFinaleData.bRunningTutorial[iTracker] != GlobalplayerBD_FM_2[iLeader].GangOps.bTutorialInProgress[iTracker]
					IF iMySlot != iTracker
						sClientData.sFinaleData.bRunningTutorial[iTracker] = GlobalplayerBD_FM_2[iLeader].GangOps.bTutorialInProgress[iTracker]
					ENDIF
				ENDIF
				
				IF sClientData.sFinaleData.iSelectedRole[iTracker] != GlobalplayerBD_FM_2[iLeader].GangOps.iSelectedRole[iTracker]
					sClientData.sFinaleData.iSelectedRole[iTracker] = GlobalplayerBD_FM_2[iLeader].GangOps.iSelectedRole[iTracker]
					
					IF iMySlot = iTracker
						IF sClientData.sFinaleData.iSelectedRole[iTracker] != -1
						AND NOT IS_CORONA_BIT_SET(CORONA_HOST_HAS_LOCKED_TEAMS)
							GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = sClientData.sFinaleData.iSelectedRole[iTracker]
							CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Update corona team chosen (not leader): GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
						ENDIF
					ENDIF
				ENDIF
				
			ENDFOR
			
			sClientData.sFinaleData.iSyncId = GlobalplayerBD_FM_2[iLeader].GangOps.iSyncId
			PUBLIC_SET_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Net Sync (M) - Net update available, updating iSyncId: ", sClientData.sFinaleData.iSyncId)

		ENDIF

	ENDIF

ENDPROC

PROC PRIVATE_SET_ROLES_AND_FINALE_CUTS_FOR_FORCE_LAUNCH(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	IF NOT IS_BIT_SET(sClientData.iFinaleBitSet, GANG_OPS_BITSET_ROLES_AND_CUTS_SET_FOR_FORCE_LAUNCH)
		CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_SET_ROLES_AND_FINALE_CUTS_FOR_FORCE_LAUNCH - Starting.")
		
		INT index 
		INT iMySlot = -1
		
		SWITCH sClientData.sFinaleData.iStrandId
			CASE ciGANGOPS_MISSION_STRAND_IAA
			CASE ciGANGOPS_MISSION_STRAND_MISSILE_SILO
				// All players on same team
				FOR index = 0 TO (MAX_GANG_OPS_PLAYERS-1)
					sClientData.sFinaleData.iSelectedRole[index] = GANG_OPS_TEAM_0
				ENDFOR
				
				CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_SET_ROLES_AND_FINALE_CUTS_FOR_FORCE_LAUNCH - All players placed on team 0 (IAA or Silo Finale)")
			BREAK
			
			CASE ciGANGOPS_MISSION_STRAND_SUBMARINE
				SWITCH sClientData.sFinaleData.iTotalPlayers
					CASE 2
						sClientData.sFinaleData.iSelectedRole[0] = GANG_OPS_TEAM_0
						sClientData.sFinaleData.iSelectedRole[1] = GANG_OPS_TEAM_1
						sClientData.sFinaleData.iSelectedRole[2] = GANG_OPS_INVALID
						sClientData.sFinaleData.iSelectedRole[3] = GANG_OPS_INVALID
						
						CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_SET_ROLES_AND_FINALE_CUTS_FOR_FORCE_LAUNCH - 2 players. Player 0 on team 0, player 1 on team 1.")
					BREAK
					CASE 3
						sClientData.sFinaleData.iSelectedRole[0] = GANG_OPS_TEAM_0
						sClientData.sFinaleData.iSelectedRole[1] = GANG_OPS_TEAM_0
						sClientData.sFinaleData.iSelectedRole[2] = GANG_OPS_TEAM_1
						sClientData.sFinaleData.iSelectedRole[3] = GANG_OPS_INVALID
						
						CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_SET_ROLES_AND_FINALE_CUTS_FOR_FORCE_LAUNCH - 3 players. Players 0 and 1 on team 0, player 2 on team 1.")
					BREAK
					CASE 4
						sClientData.sFinaleData.iSelectedRole[0] = GANG_OPS_TEAM_0
						sClientData.sFinaleData.iSelectedRole[1] = GANG_OPS_TEAM_0
						sClientData.sFinaleData.iSelectedRole[2] = GANG_OPS_TEAM_1
						sClientData.sFinaleData.iSelectedRole[3] = GANG_OPS_TEAM_1
						
						CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_SET_ROLES_AND_FINALE_CUTS_FOR_FORCE_LAUNCH - 4 players. Players 0 and 1 on team 0, players 2 and 3 on team 1.")
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
		
		SWITCH sClientData.sFinaleData.iTotalPlayers
			CASE 2
				sClientData.sFinaleData.iRewardPercentages[0] = g_sMPTunables.iH2_FINALE_CUT_TWO_PLAYERS_LEADER
				sClientData.sFinaleData.iRewardPercentages[1] = g_sMPTunables.iH2_FINALE_CUT_TWO_PLAYERS_MEMBER
				sClientData.sFinaleData.iRewardPercentages[2] = 0
				sClientData.sFinaleData.iRewardPercentages[3] = 0
				sClientData.sFinaleData.iRewardPercentages[4] = 0	// zero the kitty
				
				g_sJobGangopsInfo.m_infos.m_leaderscashcutpercentage = g_sMPTunables.iH2_FINALE_CUT_TWO_PLAYERS_LEADER
				IF sClientData.sFinaleData.piLeader = PLAYER_ID()
					g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent = g_sMPTunables.iH2_FINALE_CUT_TWO_PLAYERS_LEADER
					g_sJobGangopsInfo.m_infos.m_cashcutpercentage = g_sMPTunables.iH2_FINALE_CUT_TWO_PLAYERS_LEADER
				ELSE
					g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent = g_sMPTunables.iH2_FINALE_CUT_TWO_PLAYERS_MEMBER
					g_sJobGangopsInfo.m_infos.m_cashcutpercentage = g_sMPTunables.iH2_FINALE_CUT_TWO_PLAYERS_MEMBER
				ENDIF
			BREAK
			CASE 3
				sClientData.sFinaleData.iRewardPercentages[0] = g_sMPTunables.iH2_FINALE_CUT_THREE_PLAYERS_LEADER
				sClientData.sFinaleData.iRewardPercentages[1] = g_sMPTunables.iH2_FINALE_CUT_THREE_PLAYERS_MEMBER
				sClientData.sFinaleData.iRewardPercentages[2] = g_sMPTunables.iH2_FINALE_CUT_THREE_PLAYERS_MEMBER
				sClientData.sFinaleData.iRewardPercentages[3] = 0
				sClientData.sFinaleData.iRewardPercentages[4] = 0	// zero the kitty
				
				g_sJobGangopsInfo.m_infos.m_leaderscashcutpercentage = g_sMPTunables.iH2_FINALE_CUT_THREE_PLAYERS_LEADER
				IF sClientData.sFinaleData.piLeader = PLAYER_ID()
					g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent = g_sMPTunables.iH2_FINALE_CUT_THREE_PLAYERS_LEADER
					g_sJobGangopsInfo.m_infos.m_cashcutpercentage = g_sMPTunables.iH2_FINALE_CUT_THREE_PLAYERS_LEADER
				ELSE
					g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent = g_sMPTunables.iH2_FINALE_CUT_THREE_PLAYERS_MEMBER
					g_sJobGangopsInfo.m_infos.m_cashcutpercentage = g_sMPTunables.iH2_FINALE_CUT_THREE_PLAYERS_MEMBER
				ENDIF
			BREAK
			
			CASE 4
				sClientData.sFinaleData.iRewardPercentages[0] = g_sMPTunables.iH2_FINALE_CUT_FOUR_PLAYERS_LEADER
				sClientData.sFinaleData.iRewardPercentages[1] = g_sMPTunables.iH2_FINALE_CUT_FOUR_PLAYERS_MEMBER
				sClientData.sFinaleData.iRewardPercentages[2] = g_sMPTunables.iH2_FINALE_CUT_FOUR_PLAYERS_MEMBER
				sClientData.sFinaleData.iRewardPercentages[3] = g_sMPTunables.iH2_FINALE_CUT_FOUR_PLAYERS_MEMBER
				sClientData.sFinaleData.iRewardPercentages[4] = 0	// zero the kitty
				
				g_sJobGangopsInfo.m_infos.m_leaderscashcutpercentage = g_sMPTunables.iH2_FINALE_CUT_FOUR_PLAYERS_LEADER
				IF sClientData.sFinaleData.piLeader = PLAYER_ID()
					g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent = g_sMPTunables.iH2_FINALE_CUT_FOUR_PLAYERS_LEADER
					g_sJobGangopsInfo.m_infos.m_cashcutpercentage = g_sMPTunables.iH2_FINALE_CUT_FOUR_PLAYERS_LEADER
				ELSE
					g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent = g_sMPTunables.iH2_FINALE_CUT_FOUR_PLAYERS_MEMBER
					g_sJobGangopsInfo.m_infos.m_cashcutpercentage = g_sMPTunables.iH2_FINALE_CUT_FOUR_PLAYERS_MEMBER
				ENDIF
			BREAK
		ENDSWITCH
		
		CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_SET_ROLES_AND_FINALE_CUTS_FOR_FORCE_LAUNCH - Final value for force launch:")
		
		FOR index = 0 TO (MAX_GANG_OPS_PLAYERS-1)
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_SET_ROLES_AND_FINALE_CUTS_FOR_FORCE_LAUNCH - sClientData.sFinaleData.iSelectedRole[",index,"] = ", sClientData.sFinaleData.iSelectedRole[index])
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_SET_ROLES_AND_FINALE_CUTS_FOR_FORCE_LAUNCH - sClientData.sFinaleData.iRewardPercentages[",index,"] = ", sClientData.sFinaleData.iRewardPercentages[index])
		
			IF sClientData.sFinaleData.iPlayerOrder[index] = NETWORK_PLAYER_ID_TO_INT()
				iMySlot = index
			ENDIF
		ENDFOR
		
		IF iMySlot != -1
			IF sClientData.sFinaleData.iSelectedRole[iMySlot] != -1
			AND NOT IS_CORONA_BIT_SET(CORONA_HOST_HAS_LOCKED_TEAMS)
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = sClientData.sFinaleData.iSelectedRole[iMySlot]
				CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_SET_ROLES_AND_FINALE_CUTS_FOR_FORCE_LAUNCH - Update corona team chosen (not leader): GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
			ENDIF
		ENDIF
		
		CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_SET_ROLES_AND_FINALE_CUTS_FOR_FORCE_LAUNCH - Finished.")
		SET_BIT(sClientData.iFinaleBitSet, GANG_OPS_BITSET_ROLES_AND_CUTS_SET_FOR_FORCE_LAUNCH)
	ENDIF
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_FINALE_FORCE_LAUNCH(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	IF sClientData.sFinaleData.bInCorona
		INT iLeader = NATIVE_TO_INT(sClientData.sFinaleData.piLeader)
	
		IF iLeader != -1
			sClientData.sFinaleData.iForceLaunchTime = (g_sMPTunables.iH2_FINALE_FORCE_LAUNCH_TIME - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(GlobalplayerBD_FM_2[iLeader].GangOps.stFinaleLaunchTimer))
			
			IF sClientData.sFinaleData.iForceLaunchTime > 0
				// Update instructional buttons once per second to update the timer
				IF sClientData.sFinaleData.iForceLaunchTime < (sClientData.sFinaleData.iLastSecondCached-1000)
					PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
					sClientData.sFinaleData.iLastSecondCached = sClientData.sFinaleData.iForceLaunchTime
				ENDIF
			ELSE
				sClientData.sFinaleData.iForceLaunchTime = 0
				
				PRIVATE_SET_ROLES_AND_FINALE_CUTS_FOR_FORCE_LAUNCH(sClientData)
				
				IF sClientData.sFinaleData.piLeader = PLAYER_ID()
					sClientData.sFinaleData.iReadyState[0] = GANG_OPS_FINALE_READY
					GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iReadyState[0] = GANG_OPS_FINALE_READY
					GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.bForceLaunched = TRUE
					
					CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_FORCE_LAUNCH - Force launch timer has expired. Fo")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PRIVATE_PREPARE_GANG_OPS_FINALE_HEADSHOTS(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	INT i = 0

	REPEAT MAX_GANG_OPS_PLAYERS i
		sClientData.sFinaleData.sHeadshotData[i].playerID = INVALID_PLAYER_INDEX()
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Headshots - Prepared headshot system ready for requests.")

ENDPROC

/* ===================================================================================== *\
			STATE MACHINE - STATE METHODS AND MAIN UPDATE
\* ===================================================================================== */

PROC PRIVATE_GANG_OPS_FINALE_IDLE_UPDATE(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	PUBLIC_PROGRESS_GANG_OPS_FSM(sClientData.sStateData)
ENDPROC



PROC PRIVATE_GANG_OPS_FINALE_INIT_ENTRY(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] FSM - INIT ENTRY")
	
	UNUSED_PARAMETER(sClientData)
ENDPROC

PROC PRIVATE_GANG_OPS_FINALE_INIT_UPDATE(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	PUBLIC_PROGRESS_GANG_OPS_FSM(sClientData.sStateData)
ENDPROC

PROC PRIVATE_GANG_OPS_FINALE_INIT_EXIT(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] FSM - INIT EXIT")
//	PUBLIC_SET_GANG_OPS_HIGHLIGHTER_STATE(sClientData.sCursorData, GANG_OPS_FINALE_HIGHLIGHT_NULL, TRUE)
	PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
ENDPROC



PROC PRIVATE_GANG_OPS_FINALE_LOADING_ENTRY(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] FSM - LOADING ENTRY")
	PRIVATE_REQUEST_GANG_OPS_FINALE_SCALEFORM(sClientData.sScaleformData)
	PRIVATE_PREPARE_GANG_OPS_FINALE_HEADSHOTS(sClientData)
ENDPROC

PROC PRIVATE_GANG_OPS_FINALE_LOADING_UPDATE(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	
	BOOL bReady = TRUE
	
	IF NOT PUBLIC_HAVE_GANG_OPS_ASSETS_LOADED(sClientData.sScaleformData)
		CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Loading - Waiting for assets to load.")
		bReady = FALSE
	ENDIF
	
	IF NOT PUBLIC_HAS_GANG_OPS_AUDIO_LOADED()
		CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Loading - Waiting for audio assets to load.")
		bReady = FALSE
	ENDIF
	
	IF NOT PRIVATE_HAVE_CONFIGURED_GANG_OPS_FINALE_SETUP_DATA(sClientData)
		CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Loading - Waiting for data setup to complete.")
		bReady = FALSE
	ENDIF
	
	IF bReady
		CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] Loading - Moving to create Gang Ops board with ",sClientData.sFinaleData.iTotalPlayers," members.")
	
		PUBLIC_PROGRESS_GANG_OPS_FSM(sClientData.sStateData)
	ENDIF
	
ENDPROC

PROC PRIVATE_GANG_OPS_FINALE_LOADING_EXIT(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] FSM - LOADING EXIT")
	
	PUBLIC_SET_GANG_OPS_MAP_MARKERS_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
	PRIVATE_UPDATE_GANG_OPS_FINALE_NET_DATA(sClientData)
	PRIVATE_UPDATE_GANG_OPS_FINALE_BOARD(sClientData)
	PUBLIC_GANG_OPS_UPDATE_FINALE_SCREEN(sClientData.sScaleformData.siMainBoard, 
											sClientData.sScaleformData.bSkipLoading, sClientData.sFinaleData.tlStrandName, 
											GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(sClientData.sFinaleData.piLeader #IF IS_DEBUG_BUILD , TRUE #ENDIF ), 
											(sClientData.sFinaleData.piLeader = PLAYER_ID()),
											sClientData.sFinaleData.tlFinaleSteps)
											
	IF sClientData.sScaleformData.bSkipLoading
		PLAY_SOUND_FROM_COORD(-1, "Draw_Board", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
	ELSE
		PLAY_SOUND_FROM_COORD(-1, "Bootup", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
	ENDIF
	
ENDPROC



PROC PRIVATE_GANG_OPS_FINALE_DISPLAYING_ENTRY(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] FSM - DISPLAYING ENTRY")
	
	UNUSED_PARAMETER(sClientData)
	
	IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
		SET_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
		CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] PRIVATE_GANG_OPS_FINALE_DISPLAYING_ENTRY - SET_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)")
	ENDIF
ENDPROC

PROC PRIVATE_GANG_OPS_FINALE_DISPLAYING_UPDATE(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	
	PRIVATE_UPDATE_GANG_OPS_FINALE_NET_DATA(sClientData)
	PRIVATE_RENDER_GANG_OPS_FINALE_SCALEFORM(sClientData)
	PRIVATE_UPDATE_GANG_OPS_FINALE_BOARD(sClientData)
	
	IF sClientData.sStateData.bInteract
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		PUBLIC_PROGRESS_GANG_OPS_FSM(sClientData.sStateData)
	ENDIF
	
ENDPROC

PROC PRIVATE_GANG_OPS_FINALE_DISPLAYING_EXIT(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] FSM - DISPLAYING EXIT")
	
	UNUSED_PARAMETER(sClientData)
ENDPROC



PROC PRIVATE_GANG_OPS_FINALE_INUSE_ENTRY(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] FSM - INUSE ENTRY")
	
	PRIVATE_INIT_GANG_OPS_FINALE_CAMERA(sClientData)
	
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] INUSE ENTRY - Setting camera focus to: GOCAM_MAIN")
	sClientData.sCameraData.ePositionId = GOCAM_MAIN
	sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_MAIN)
	
	IF sClientData.sFinaleData.piLeader = PLAYER_ID()
		IF NOT HAS_PLAYER_COMPLETED_GANG_OPS_FINALE_TUTORIAL_AS_LEADER(PLAYER_ID())
			sClientData.sTutorialData.bTutorialInProgress = TRUE
			sClientData.sTutorialData.iTutorialStage = 0
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] INUSE ENTRY - Player is the leader and hasnt completed the leader tutorial yet, sClientData.sTutorialData.bTutorialInProgress = TRUE")
		ENDIF
	ELSE
		IF NOT HAS_PLAYER_COMPLETED_GANG_OPS_FINALE_TUTORIAL_AS_MEMBER(PLAYER_ID())
			sClientData.sTutorialData.bTutorialInProgress = TRUE
			sClientData.sTutorialData.iTutorialStage = 0
			CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] INUSE ENTRY - Player is the not leader and hasnt completed the member tutorial yet, sClientData.sTutorialData.bTutorialInProgress = TRUE")
		ENDIF
	ENDIF
	
	PUBLIC_SET_GANG_OPS_MAP_MARKERS_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
	
	sClientData.sScaleformData.bDrawInstructions = TRUE
ENDPROC

PROC PRIVATE_GANG_OPS_FINALE_INUSE_UPDATE(GANG_OPS_CLIENT_FINALE_DATA &sClientData)

	PUBLIC_MAINTAIN_EXCLUSIVE_INPUT()
	PRIVATE_UPDATE_GANG_OPS_FINALE_FORCE_LAUNCH(sClientData)
	PRIVATE_UPDATE_GANG_OPS_FINALE_TUTORIAL(sClientData)
	PRIVATE_UPDATE_GANG_OPS_FINALE_ACTIVE_CHECKS(sClientData)
	PRIVATE_UPDATE_GANG_OPS_FINALE_NET_DATA(sClientData)
	PRIVATE_RENDER_GANG_OPS_FINALE_SCALEFORM(sClientData)
	PRIVATE_UPDATE_GANG_OPS_FINALE_HIGHLIGHT(sClientData)
	PRIVATE_UPDATE_GANG_OPS_FINALE_WARNING_SCREENS(sClientData)
	PRIVATE_UPDATE_GANG_OPS_FINALE_INPUTS(sClientData)
	PRIVATE_UPDATE_GANG_OPS_FINALE_BOARD(sClientData)
	PUBLIC_UPDATE_GANG_OPS_CAMERA(sClientData.sCameraData)
	PUBLIC_GANG_OPS_HIDE_HUD()
	sClientData.sFinaleData.iWarningScreenBuffer = -1
	
	IF sClientData.sFinaleData.piLeader = PLAYER_ID()
		PRIVATE_UPDATE_GANG_OPS_FINALE_LAUNCH_REQUIREMENTS(sClientData)
	ELSE
		PRIVATE_MONITOR_GANG_OPS_FINALE_LAUNCH(sClientData)
	ENDIF
	
	IF NOT sClientData.sStateData.bInteract
		IF IS_SKYSWOOP_AT_GROUND()
		AND NOT IS_PLAYER_TELEPORT_ACTIVE()
		AND NOT IS_PLAYER_IN_CORONA()
		AND IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		SET_BIT(sClientData.iFinaleBitSet, GANG_OPS_BITSET_SUPPRESS_HELP_SOUND)
		PRIVATE_CLEAR_GANG_OPS_FINALE_CAMERA(sClientData)
		PUBLIC_REGRESS_GANG_OPS_FSM(sClientData.sStateData)
	ENDIF
	
ENDPROC

PROC PRIVATE_GANG_OPS_FINALE_INUSE_EXIT(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] FSM - INUSE EXIT")
	
	sClientData.sScaleformData.bDrawInstructions = FALSE
	
ENDPROC

PROC PRIVATE_GANG_OPS_FINALE_CLEANUP_ENTRY(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS_FIN, "[GANGOPS_FLOW] FSM - CLEANUP ENTRY")
	
	IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
		CLEAR_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
	ENDIF
	
	PUBLIC_GANG_OPS_DRAW_RECTANGLE(sClientData.sScaleformData)
ENDPROC

PROC PRIVATE_GANG_OPS_FINALE_CLEANUP_UPDATE(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	PUBLIC_GANG_OPS_DRAW_RECTANGLE(sClientData.sScaleformData)
	PUBLIC_CLEAN_GANG_OPS_SCALEFORM_CACHE()
	PRIVATE_CLEAR_GANG_OPS_FINALE_CAMERA(sClientData)
	PUBLIC_PROGRESS_GANG_OPS_FSM(sClientData.sStateData)
ENDPROC

PROC PRIVATE_GANG_OPS_FINALE_UPDATE(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	
	SWITCH (sClientData.sStateData.eState)
	
		CASE GOBS_IDLE
		
			PRIVATE_GANG_OPS_FINALE_IDLE_UPDATE(sClientData)
		
		BREAK
		
		CASE GOBS_INIT
		
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_ENTRY(sClientData.sStateData)
				PRIVATE_GANG_OPS_FINALE_INIT_ENTRY(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_ENTRY(sClientData.sStateData)
			ENDIF
			
			PRIVATE_GANG_OPS_FINALE_IDLE_UPDATE(sClientData)
			
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_EXIT(sClientData.sStateData)
				PRIVATE_GANG_OPS_FINALE_INIT_EXIT(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_EXIT(sClientData.sStateData)
			ENDIF
			
		BREAK
		
		CASE GOBS_LOADING
		
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_ENTRY(sClientData.sStateData)
				PRIVATE_GANG_OPS_FINALE_LOADING_ENTRY(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_ENTRY(sClientData.sStateData)
			ENDIF
			
			PRIVATE_GANG_OPS_FINALE_LOADING_UPDATE(sClientData)
			
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_EXIT(sClientData.sStateData)
				PRIVATE_GANG_OPS_FINALE_LOADING_EXIT(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_EXIT(sClientData.sStateData)
			ENDIF
		
		BREAK
		
		CASE GOBS_DISPLAYING
		
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_ENTRY(sClientData.sStateData)
				PRIVATE_GANG_OPS_FINALE_DISPLAYING_ENTRY(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_ENTRY(sClientData.sStateData)
			ENDIF
			
			PRIVATE_GANG_OPS_FINALE_DISPLAYING_UPDATE(sClientData)
			
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_EXIT(sClientData.sStateData)
				PRIVATE_GANG_OPS_FINALE_DISPLAYING_EXIT(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_EXIT(sClientData.sStateData)
			ENDIF
		
		BREAK
		
		CASE GOBS_INUSE
		
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_ENTRY(sClientData.sStateData)
				PRIVATE_GANG_OPS_FINALE_INUSE_ENTRY(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_ENTRY(sClientData.sStateData)
			ENDIF
			
			PRIVATE_GANG_OPS_FINALE_INUSE_UPDATE(sClientData)
			
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_EXIT(sClientData.sStateData)
				PRIVATE_GANG_OPS_FINALE_INUSE_EXIT(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_EXIT(sClientData.sStateData)
			ENDIF
		
		BREAK
		
		CASE GOBS_CLEANUP
		
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_ENTRY(sClientData.sStateData)
				PRIVATE_GANG_OPS_FINALE_CLEANUP_ENTRY(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_ENTRY(sClientData.sStateData)
			ENDIF
			
			PRIVATE_GANG_OPS_FINALE_CLEANUP_UPDATE(sClientData)
		
		BREAK
		
		CASE GOBS_FINISHED
			PRIVATE_UNLOAD_GANG_OPS_FINALE_SCRIPT_RESOURCES(sClientData)
		BREAK
	
	ENDSWITCH

ENDPROC



PROC MAINTAIN_GANG_OPS_FINALE(GANG_OPS_CLIENT_FINALE_DATA &sClientData)
	
	#IF IS_DEBUG_BUILD
	PRIVATE_GANG_OPS_FINALE_WIDGET_UPDATE(sClientData)
	#ENDIF

	PRIVATE_GANG_OPS_FINALE_UPDATE(sClientData)
	
ENDPROC

// EOF

