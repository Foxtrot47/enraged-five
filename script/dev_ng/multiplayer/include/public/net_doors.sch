// V 
//Author- Conor McGuire
//21/8/12
USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "net_include.sch"
USING "net_mission.sch"

CONST_INT NUM_DOORS_CHECKED_PER_FRAME_ON_INIT	40

FUNC MP_DOOR_DETAILS GET_MP_DOOR_DETAILS_FROM_ENUM(INT iIndex, INT iGameMode )
	MP_DOOR_DETAILS doorDetails

	SWITCH INT_TO_ENUM(MP_DN_ENUM,iIndex)
		//-- Door hashkeys
		///////////////////////////////////////////////////////////////////////////////////////////	
		//COMMON DOORS
		///////////////////////////////////////////////////////////////////////////////////////////	
		CASE MP_DN_SIMEON_GAR
			doorDetails.iDoorHash = MP_DOOR_SIMEON_GAR_DOOR
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_SIMEON_GAR_DOOR"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_COM_GAR_DOOR_01
			doorDetails.vCoords = <<1204.57,-3110.40,6.57>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
		BREAK
		
		
		CASE MP_DN_POLICE_SIDE_LHS
			doorDetails.iDoorHash = DOOR_POLICE_SIDE_LHS
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName  = "DOOR_POLICE_SIDE_LHS"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_rc_door2
			doorDetails.vCoords = <<467.3716, -1014.4520, 26.5362>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_POLICE_SIDE_RHS
			doorDetails.iDoorHash = DOOR_POLICE_SIDE_RHS
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "DOOR_POLICE_SIDE_RHS"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_rc_door2
			doorDetails.vCoords = <<469.9679, -1014.4520, 26.5362>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_POLICE_ROOF 
			doorDetails.iDoorHash = DOOR_POLICE_ROOF
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "DOOR_POLICE_ROOF"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_gtdoor02
			doorDetails.vCoords = <<464.3613,-984.6779,43.8344>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_POLICE_FRONT_1 
			doorDetails.iDoorHash = DOOR_POLICE_FRONT_1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "DOOR_POLICE_FRONT_1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_PH_DOOR01
			doorDetails.vCoords = <<434.75,-980.61,30.84>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_POLICE_FRONT_2
			doorDetails.iDoorHash = DOOR_POLICE_FRONT_2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "DOOR_POLICE_FRONT_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_PH_DOOR002
			doorDetails.vCoords = <<434.75,-983.22,30.84>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_SHP_BIO_GARAGE_L
			doorDetails.iDoorHash = MP_DOOR_SHP_BIO_GARAGE_L
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_SHP_BIO_GARAGE_L"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_BIOLAB_G_DOOR
			doorDetails.vCoords = <<3589.1, 3671.5, 35.0>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_SHP_BIO_GARAGE_R
			doorDetails.iDoorHash = MP_DOOR_SHP_BIO_GARAGE_R
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_SHP_BIO_GARAGE_R"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_BIOLAB_G_DOOR
			doorDetails.vCoords = <<3587.6, 3663.3, 35.0>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_CHEMICAL_PLANT1 
			doorDetails.iDoorHash = MP_DOOR_CHEMICAL_PLANT1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CHEMICAL_PLANT1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_BL_SHUTTER2
			doorDetails.vCoords = <<3627.71, 3746.72, 27.69>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_CHEMICAL_PLANT2 
			doorDetails.iDoorHash = MP_DOOR_CHEMICAL_PLANT2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CHEMICAL_PLANT2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_BL_SHUTTER2
			doorDetails.vCoords = <<3620.84, 3751.53, 27.69>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_ABBATOIR_L
			doorDetails.iDoorHash = MP_DOOR_ABBATOIR_L
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_ABBATOIR_L"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_abbmaindoor
			doorDetails.vCoords = << 962.10, -2183.83, 31.06 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_ABBATOIR_R
			doorDetails.iDoorHash = MP_DOOR_ABBATOIR_R
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_ABBATOIR_R"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_abbmaindoor2
			doorDetails.vCoords = << 961.79, -2187.08, 31.06 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_AIRPORT_R_1
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_R_1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_R_1"
			#ENDIF
			doorDetails.doorModel = PROP_GATE_AIRPORT_01
			doorDetails.vCoords = <<-1138.47,-2730.45,12.95>>
			IF iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bJustAdd = TRUE
				doorDetails.bRaceDoor = TRUE
				doorDetails.iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bPermanentState = FALSE
			ELSE
				doorDetails.iGameMode = FAKE_GAME_MODE_ALL
				doorDetails.bUseStateSystem = TRUE
				doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
				doorDetails.bPermanentState = FALSE
			ENDIF
		BREAK
		CASE MP_DN_AIRPORT_R_2
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_R_2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_R_2"
			#ENDIF
			doorDetails.doorModel = PROP_GATE_AIRPORT_01
			doorDetails.vCoords = <<-1151.2043,-2723.1023,12.9460>>
			IF iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bJustAdd = TRUE
				doorDetails.bRaceDoor = TRUE
				doorDetails.iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bPermanentState = FALSE
			ELSE
				doorDetails.iGameMode = FAKE_GAME_MODE_ALL
				doorDetails.bUseStateSystem = TRUE
				doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
				doorDetails.bPermanentState = FALSE
			ENDIF
		BREAK
		
		CASE MP_DN_AIRPORT_L_1  //2160728
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_L_1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_L_1"
			#ENDIF
			doorDetails.doorModel = PROP_GATE_AIRPORT_01
			doorDetails.vCoords = <<-977.5174,-2837.2644,12.9549>>
			IF iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bJustAdd = TRUE
				doorDetails.bRaceDoor = TRUE
				doorDetails.iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bPermanentState = FALSE
			ELSE
				doorDetails.iGameMode = FAKE_GAME_MODE_ALL
				doorDetails.bUseStateSystem = TRUE
				doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
				//doorDetails.fRatio = 1.0
				doorDetails.bPermanentState = FALSE
			ENDIF
		BREAK
		CASE MP_DN_AIRPORT_L_2
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_L_2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_L_2"
			#ENDIF
			doorDetails.doorModel = PROP_GATE_AIRPORT_01
			doorDetails.vCoords = <<-990.2963, -2829.8872, 12.9499>>
			IF iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bJustAdd = TRUE
				doorDetails.bRaceDoor = TRUE
				doorDetails.iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bPermanentState = FALSE
			ELSE
				doorDetails.iGameMode = FAKE_GAME_MODE_ALL
				doorDetails.bUseStateSystem = TRUE
				doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
				//doorDetails.fRatio = 1.0
				doorDetails.bPermanentState = FALSE
			ENDIF
		BREAK
		CASE MP_DN_AIRPORT_GATE_OTHER
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_OTHER
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_OTHER"
			#ENDIF
			doorDetails.doorModel = PROP_FACGATE_01
			doorDetails.vCoords = <<-1213.4,-2079.3003,12.907>>
			IF iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bJustAdd = TRUE
				doorDetails.bRaceDoor = TRUE
				doorDetails.iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bPermanentState = FALSE
			ELSE
				doorDetails.iGameMode = FAKE_GAME_MODE_ALL
				doorDetails.bUseStateSystem = TRUE
				doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
				doorDetails.bPermanentState = FALSE
			ENDIF
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_1
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_02gate6_r
			doorDetails.vCoords = <<-967.4473,-2778.4951,14.409>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_2
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_02gate6_l
			doorDetails.vCoords = <<-974.5734,-2774.3809,14.4099>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_AIRPORT_GATE_CW_3
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_3
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_3"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_02gate6_r
			doorDetails.vCoords = <<-971.1018,-2776.3855,14.409>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_4
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_4
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_4"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_02gate6_l
			doorDetails.vCoords = <<-970.9188,-2776.4907,14.409>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_5
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_5
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_5"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_02gate6_r
			doorDetails.vCoords = <<-935.2114,-2767.3967,14.3882>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_6
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_6
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_6"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_02gate6_l
			doorDetails.vCoords = <<-933.1581,-2763.9546,14.3882>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_7
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_7
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_7"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_02gate6_r
			doorDetails.vCoords = <<-933.0535,-2763.7793,14.3882>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_8
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_8
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_8"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_02gate6_l
			doorDetails.vCoords = <<-931.0002,-2760.3372,14.3882>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_9
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_9
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_9"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_02gate6_r
			doorDetails.vCoords = <<-773.2438,-2842.6775,14.2715>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_10
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_10
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_10"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_02gate6_l
			doorDetails.vCoords = <<-769.7721,-2844.6819,14.2715>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_11
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_11
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_11"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_02gate6_r
			doorDetails.vCoords = <<-769.6071,-2844.7771,14.2715>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_12
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_12
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_12"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_02gate6_l
			doorDetails.vCoords = <<-766.1354,-2846.7815,14.2715>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_13
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_13
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_13"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_09gate1
			doorDetails.vCoords = <<-828.9456,-2964.3037,14.2758>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_14
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_14
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_14"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_09gate1
			doorDetails.vCoords = <<-907.7999,-3100.8740,14.2808>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_15
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_15
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_15"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_09gate1
			doorDetails.vCoords = <<-833.4395,-3186.7095,14.2670>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_16
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_16
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_16"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_09gate1
			doorDetails.vCoords = <<-830.0544,-3391.1628,14.1972>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_17
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_17
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_17"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_09gate1
			doorDetails.vCoords = <<-913.0834,-3534.9697,14.1924>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_18
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_18
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_18"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_09gate1
			doorDetails.vCoords = <<-971.6149,-3549.1521,14.2727>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_19
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_19
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_19"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_09gate1
			doorDetails.vCoords = <<-1017.6921,-3563.2173,14.2767>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_20
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_20
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_20"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_09gate1
			doorDetails.vCoords = <<-1146.8314,-3546.6382,14.2595>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_21
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_21
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_21"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_09gate1
			doorDetails.vCoords = <<-1260.8966,-3480.7639,14.1721>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_22
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_22
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_22"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_09gate1
			doorDetails.vCoords = <<-1351.1328,-3404.1616,14.1721>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_23
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_23
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_23"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_09gate1
			doorDetails.vCoords = <<-1409.5073,-3370.4614,14.2068>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_24
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_24
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_24"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_09gate1
			doorDetails.vCoords = <<-1831.8040,-3224.9663,14.3119>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_25
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_25
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_25"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_09gate1
			doorDetails.vCoords = <<-1942.2893,-3161.1904,14.2981>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_26
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_26
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_26"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_09gate1
			doorDetails.vCoords = <<-1927.5635,-3076.2690,14.4569>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_27
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_27
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_27"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_09gate1
			doorDetails.vCoords = <<-1951.6956,-3003.8457,14.4418>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_28
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_28
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_28"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_09gate1
			doorDetails.vCoords = <<-1836.8750,-2804.9690,14.4557>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_29
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_29
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_29"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_09gate1
			doorDetails.vCoords = <<-1802.6915,-2745.7615,14.4480>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_30
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_30
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_30"
			#ENDIF
			doorDetails.doorModel = prop_facgate_01
			doorDetails.vCoords = <<-1099.5308,-2020.8026,12.1745>>
			IF iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bJustAdd = TRUE
				doorDetails.bRaceDoor = TRUE
				doorDetails.iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bPermanentState = FALSE
			ELSE
				doorDetails.iGameMode = FAKE_GAME_MODE_ALL
				doorDetails.bUseStateSystem = TRUE
				doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
				doorDetails.bPermanentState = TRUE
			ENDIF
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_31
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_31
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_31"
			#ENDIF
			doorDetails.doorModel = prop_facgate_01
			doorDetails.vCoords = <<-994.4996,-2341.6482,12.9448>>
			IF iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bJustAdd = TRUE
				doorDetails.bRaceDoor = TRUE
				doorDetails.iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bPermanentState = FALSE
			ELSE
				doorDetails.iGameMode = FAKE_GAME_MODE_ALL
				doorDetails.bUseStateSystem = TRUE
				doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
				doorDetails.bPermanentState = FALSE
			ENDIF
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_32
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_32
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_32"
			#ENDIF
			doorDetails.doorModel = prop_facgate_01
			doorDetails.vCoords = <<-984.0709,-2348.4001,12.9448>>
			IF iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bJustAdd = TRUE
				doorDetails.bRaceDoor = TRUE
				doorDetails.iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bPermanentState = FALSE
			ELSE
				doorDetails.iGameMode = FAKE_GAME_MODE_ALL
				doorDetails.bUseStateSystem = TRUE
				doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
				doorDetails.bPermanentState = FALSE
			ENDIF
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_33
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_33
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_33"
			#ENDIF
			doorDetails.doorModel = prop_gate_airport_01
			doorDetails.vCoords = <<-1008.0708,-2406.7510,12.9770>>
			IF iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bJustAdd = TRUE
				doorDetails.bRaceDoor = TRUE
				doorDetails.iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bPermanentState = FALSE
			ELSE
				doorDetails.iGameMode = FAKE_GAME_MODE_ALL
				doorDetails.bUseStateSystem = TRUE
				doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
				doorDetails.bPermanentState = FALSE
			ENDIF
		BREAK
		CASE MP_DN_AIRPORT_GATE_CW_34
			doorDetails.iDoorHash = MP_DOOR_AIRPORT_GATE_CW_34
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIRPORT_GATE_CW_34"
			#ENDIF
			doorDetails.doorModel = prop_gate_airport_01
			doorDetails.vCoords = <<-1015.4854,-2419.5828,12.9586>>
			IF iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bJustAdd = TRUE
				doorDetails.bRaceDoor = TRUE
				doorDetails.iGameMode = FAKE_GAME_MODE_RACES
				doorDetails.bPermanentState = FALSE
			ELSE
				doorDetails.iGameMode = FAKE_GAME_MODE_ALL
				doorDetails.bUseStateSystem = TRUE
				doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
				doorDetails.bPermanentState = FALSE
			ENDIF
		BREAK
		
		CASE MP_DN_CS_ONLY_INT_LESTER 
			doorDetails.iDoorHash = MP_DOOR_CS_ONLY_INT_LESTER
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CS_ONLY_INT_LESTER"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_LESTER_DOORFRONT
			doorDetails.vCoords = <<1273.82,-1720.70,54.92>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		CASE MP_DN_CS_ONLY_INT_TORTURE 
			doorDetails.iDoorHash = MP_DOOR_CS_ONLY_INT_TORTURE
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CS_ONLY_INT_TORTURE"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_TORT_DOOR
			doorDetails.vCoords = <<134.40,-2204.10,7.52>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_CS_ONLY_INT_JANITOR 
			doorDetails.iDoorHash = MP_DOOR_CS_ONLY_INT_JANITOR
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CS_ONLY_INT_JANITOR"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_JANITOR_FRONTDOOR
			doorDetails.vCoords = <<-107.5373,-9.0181,70.6708>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_CS_ONLY_INT_DEVIANT
			doorDetails.iDoorHash = MP_DOOR_CS_ONLY_INT_DEVIANT
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CS_ONLY_INT_DEVIANT"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_DEVIANTFRONTDOOR
			doorDetails.vCoords = <<-128.33,-1457.17,37.94>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_CS_ONLY_INT_PSYCHOFF 
			doorDetails.iDoorHash = MP_DOOR_CS_ONLY_INT_PSYCHOFF
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CS_ONLY_INT_PSYCHOFF"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_PO_DOOR
			doorDetails.vCoords = <<-1910.58,-576.01,19.25>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_CS_ONLY_INT_RANCH0 
			doorDetails.iDoorHash = MP_DOOR_CS_ONLY_INT_RANCH0
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CS_ONLY_INT_RANCH0"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_RA_DOOR3
			doorDetails.vCoords = <<1395.92,1142.90,114.79>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_CS_ONLY_INT_RANCH1 
			doorDetails.iDoorHash = MP_DOOR_CS_ONLY_INT_RANCH1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CS_ONLY_INT_RANCH1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_RA_DOOR3
			doorDetails.vCoords = <<1395.92,1140.70,114.79>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_CS_ONLY_INT_RANCH2 
			doorDetails.iDoorHash = MP_DOOR_CS_ONLY_INT_RANCH2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CS_ONLY_INT_RANCH2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_RA_DOOR1_L
			doorDetails.vCoords = <<1390.52,1163.44,114.38>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_CS_ONLY_INT_RANCH3
			doorDetails.iDoorHash = MP_DOOR_CS_ONLY_INT_RANCH3
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CS_ONLY_INT_RANCH3"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_RA_DOOR1_R
			doorDetails.vCoords = <<1390.52,1161.24,114.38>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_CS_ONLY_INT_RANCH4 
			doorDetails.iDoorHash = MP_DOOR_CS_ONLY_INT_RANCH4
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CS_ONLY_INT_RANCH4"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_RA_DOOR1_L
			doorDetails.vCoords = <<1408.07,1158.97,114.48>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_CS_ONLY_INT_RANCH5 
			doorDetails.iDoorHash = MP_DOOR_CS_ONLY_INT_RANCH5
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CS_ONLY_INT_RANCH5"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_RA_DOOR1_R
			doorDetails.vCoords = <<1408.07,1161.17,114.48>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_CS_ONLY_INT_RANCH6 
			doorDetails.iDoorHash = MP_DOOR_CS_ONLY_INT_RANCH6
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CS_ONLY_INT_RANCH6"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_RA_DOOR1_L
			doorDetails.vCoords = <<1409.29,1146.25,114.49>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_CS_ONLY_INT_RANCH7 
			doorDetails.iDoorHash = MP_DOOR_CS_ONLY_INT_RANCH7
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CS_ONLY_INT_RANCH7"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_RA_DOOR1_R
			doorDetails.vCoords = <<1409.29,1148.45,114.49>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_CS_ONLY_INT_BAHAMA0
			doorDetails.iDoorHash = MP_DOOR_CS_ONLY_INT_BAHAMA0
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CS_ONLY_INT_BAHAMA0"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_BL_DOOR_L
			doorDetails.vCoords = <<-1387.05,-586.58,30.47>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_CS_ONLY_INT_BAHAMA1 
			doorDetails.iDoorHash = MP_DOOR_CS_ONLY_INT_BAHAMA1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CS_ONLY_INT_BAHAMA1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_BL_DOOR_R
			doorDetails.vCoords = <<-1389.24,-588.00,30.47>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_UNDERGROUND_CARPARK0
			doorDetails.iDoorHash = MP_DOOR_UNDERGROUND_CARPARK0
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_UNDERGROUND_CARPARK0"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_SEC_GATE_01C
			doorDetails.vCoords = <<25.03,-664.60,31.04>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_FORCE_OPEN_THIS_FRAME
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_UNDERGROUND_CARPARK1
			doorDetails.iDoorHash = MP_DOOR_UNDERGROUND_CARPARK1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_UNDERGROUND_CARPARK1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_SEC_GATE_01D
			doorDetails.vCoords = <<-72.75,-682.17,33.27>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_FORCE_OPEN_THIS_FRAME
			doorDetails.bPermanentState = TRUE
		BREAK
		///////////////////////////////////////////////////////////////////////////////////////////	
		//FREEMODE DOORS
		///////////////////////////////////////////////////////////////////////////////////////////	
//		CASE MP_DN_LIVE_MUSIC 
//			doorDetails.iDoorHash = MP_DOOR_LIVE_MUSIC
//			doorDetails.sDoorName = "MP_DOOR_LIVE_MUSIC"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = V_ILEV_SS_DOOR5_l
//			doorDetails.vCoords = <<-565.17,276.63,83.28>>
//			doorDetails.bUseStateSystem = TRUE
//			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//		BREAK
//		CASE MP_DN_LIVE_MUSIC_BACK 
//			doorDetails.iDoorHash = MP_DOOR_LIVE_MUSIC_BACK
//			doorDetails.sDoorName = "MP_DOOR_LIVE_MUSIC_BACK"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel =  V_ILEV_SS_DOOR5_l
//			doorDetails.vCoords = <<-561.29,293.50,87.78>>
//			doorDetails.bUseStateSystem = TRUE
//			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//		BREAK
		///////////////////////////////////////////////////////////////////////////////////////////
		CASE MP_DN_BLAINE_BANK0	
			doorDetails.iDoorHash = MP_DOOR_BLAINE_BANK0
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_BLAINE_BANK0"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_BANK4DOOR02
			doorDetails.vCoords = <<-111.48,6463.9399,31.9850>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_BLAINE_BANK1
			doorDetails.iDoorHash = MP_DOOR_BLAINE_BANK1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_BLAINE_BANK1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_BANK4DOOR01
			doorDetails.vCoords = <<-109.650,6462.1099,31.9850>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_ORN_BANK_1	
			doorDetails.iDoorHash = MP_DOOR_ORN_BANK_1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_ORN_BANK_1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = HEI_V_ILEV_BK_GATE_PRIS
			doorDetails.vCoords = <<256.3116,220.6579,106.4296>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		CASE MP_DN_ORN_BANK_2
			doorDetails.iDoorHash = MP_DOOR_ORN_BANK_2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_ORN_BANK_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = HEI_V_ILEV_BK_GATE2_PRIS
			doorDetails.vCoords = <<262.1981,222.5188,106.4296 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK

//		CASE MP_DN_ORN_BANK_1	
//			doorDetails.iDoorHash = MP_DOOR_ORN_BANK_1
//			#IF IS_DEBUG_BUILD
//			doorDetails.sDoorName = "MP_DOOR_ORN_BANK_1"
//			#ENDIF
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = prop_ld_bankdoors_01
//			doorDetails.vCoords = <<232, 216, 106>>
//			doorDetails.bUseStateSystem = TRUE
//			//doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//		BREAK
//		CASE MP_DN_ORN_BANK_2
//			doorDetails.iDoorHash = MP_DOOR_ORN_BANK_2
//			#IF IS_DEBUG_BUILD
//			doorDetails.sDoorName = "MP_DOOR_ORN_BANK_2"
//			#ENDIF
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = prop_ld_bankdoors_01
//			doorDetails.vCoords = <<233, 214, 106 >>
//			doorDetails.bUseStateSystem = TRUE
//			//doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//		BREAK
//		CASE MP_DN_ORN_BANK_3 
//			doorDetails.iDoorHash = MP_DOOR_ORN_BANK_3
//			#IF IS_DEBUG_BUILD
//			doorDetails.sDoorName = "MP_DOOR_ORN_BANK_3"
//			#ENDIF
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = V_ILEV_J2_DOOR
//			doorDetails.vCoords = <<258, 204, 106>>
//			doorDetails.bUseStateSystem = TRUE
//			//doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//		BREAK
//		CASE MP_DN_ORN_BANK_4
//			doorDetails.iDoorHash = MP_DOOR_ORN_BANK_4
//			#IF IS_DEBUG_BUILD
//			doorDetails.sDoorName = "MP_DOOR_ORN_BANK_4"
//			#ENDIF
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = V_ILEV_J2_DOOR
//			doorDetails.vCoords = <<261, 203, 106 >>
//			doorDetails.bUseStateSystem = TRUE
//			//doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//		BREAK			
		CASE MP_DN_BANK_MAZE_1
			doorDetails.iDoorHash = MP_DOOR_BANK_MAZE_1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_BANK_MAZE_1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_GENBANKDOOR1
			doorDetails.vCoords = <<-2965.821,481.630,16.048>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK	
		CASE MP_DN_BANK_MAZE_2
			doorDetails.iDoorHash = MP_DOOR_BANK_MAZE_2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_BANK_MAZE_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_GENBANKDOOR2
			doorDetails.vCoords = <<-2965.710,484.219,16.048>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK	
		CASE MP_DN_BANK_FLEECA_1
			doorDetails.iDoorHash = MP_DOOR_BANK_FLEECA_1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_BANK_FLEECA_1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_genbankdoor1
			doorDetails.vCoords = << 1176.49, 2703.61, 38.44 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK	
		CASE MP_DN_BANK_FLEECA_2
			doorDetails.iDoorHash = MP_DOOR_BANK_FLEECA_2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_FLEECA_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_genbankdoor2
			doorDetails.vCoords = << 1173.90, 2703.61, 38.44 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK	
		CASE MP_DN_BANK_OLDW_1
			doorDetails.iDoorHash = MP_DOOR_BANK_OLDW_1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_OLDW_1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_genbankdoor1
			doorDetails.vCoords = << 1656.25, 4852.24, 42.35 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK	
		CASE MP_DN_BANK_OLDW_2
			doorDetails.iDoorHash = MP_DOOR_BANK_OLDW_2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_OLDW_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_genbankdoor2
			doorDetails.vCoords = << 1656.57, 4849.66, 42.35 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK	
		CASE MP_DN_BANK_CITYW_1
			doorDetails.iDoorHash = MP_DOOR_BANK_CITYW_1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CITYW_1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_genbankdoor1
			doorDetails.vCoords = << -1215.39, -328.52, 38.13 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK	
		CASE MP_DN_BANK_CITYW_2
			doorDetails.iDoorHash = MP_DOOR_BANK_CITYW_2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CITYW_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_genbankdoor2
			doorDetails.vCoords = << -1213.07, -327.35, 38.13 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK	
		CASE MP_DN_BANK_CENTRE_1
			doorDetails.iDoorHash = MP_DOOR_BANK_CENTRE_1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_BANK_CENTRE_1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_genbankdoor1
			doorDetails.vCoords = << 149.63, -1037.23, 29.72 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK	
		CASE MP_DN_BANK_CENTRE_2
			doorDetails.iDoorHash = MP_DOOR_BANK_CENTRE_2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_BANK_CENTRE_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_genbankdoor2
			doorDetails.vCoords = << 152.06, -1038.12, 29.72 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK	
		CASE MP_DN_BANK_CITYNE_1
			doorDetails.iDoorHash = MP_DOOR_BANK_CITYNE_1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_BANK_CITYNE_1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_genbankdoor1
			doorDetails.vCoords = << 313.96, -275.60, 54.52 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK	
		CASE MP_DN_BANK_CITYNE_2
			doorDetails.iDoorHash = MP_DOOR_BANK_CITYNE_2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CITYNE_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_genbankdoor2
			doorDetails.vCoords = << 316.39, -276.49, 54.52 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK	
		CASE MP_DN_BANK_CITYN_1
			doorDetails.iDoorHash = MP_DOOR_BANK_CITYN_1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_BANK_CITYN_1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_genbankdoor2
			doorDetails.vCoords = << -348.8109, -47.2621, 49.3876 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK	
		CASE MP_DN_BANK_CITYN_2
			doorDetails.iDoorHash = MP_DOOR_BANK_CITYN_2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CITYN_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_genbankdoor1
			doorDetails.vCoords = << -351.2598,-46.4122,49.3876 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK	
//		CASE MP_DN_PRISON_GATE_FRONT
//			doorDetails.iDoorHash = DOOR_PRISON_GATE_FRONT
//			doorDetails.sDoorName = "DOOR_PRISON_GATE_FRONT"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = PROP_GATE_PRISON_01
//			doorDetails.vCoords = <<1845.0, 2604.8, 44.6 >>
//			doorDetails.bUseStateSystem = TRUE
//			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//		BREAK	
//		CASE MP_DN_PRISON_GATE_INNER_RIGHT
//			doorDetails.iDoorHash = DOOR_PRISON_GATE_INNER_RIGHT
//			doorDetails.sDoorName = "DOOR_PRISON_GATE_INNER_RIGHT"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = PROP_GATE_PRISON_01
//			doorDetails.vCoords = <<1799.7, 2617.0, 44.6>>
//			doorDetails.bUseStateSystem = TRUE
//			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//		BREAK	
//		CASE MP_DN_PRISON_GATE_INNER
//			doorDetails.iDoorHash = DOOR_PRISON_GATE_INNER
//			doorDetails.sDoorName = "DOOR_PRISON_GATE_INNER"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = PROP_GATE_PRISON_01
//			doorDetails.vCoords = <<1818.5, 2604.8, 44.6 >>
//			doorDetails.bUseStateSystem = TRUE
//			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//		BREAK	
		CASE MP_DN_FBI_OFFICE_ENTRY_1
			doorDetails.iDoorHash = DOOR_FBI_OFFICE_ENTRY_1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "DOOR_FBI_OFFICE_ENTRY_1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_FIBL_DOOR02
			doorDetails.vCoords =  <<106.38, -742.70, 46.18>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK	
		CASE MP_DN_FBI_OFFICE_ENTRY_2
			doorDetails.iDoorHash = DOOR_FBI_OFFICE_ENTRY_2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "DOOR_FBI_OFFICE_ENTRY_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_FIBL_DOOR01
			doorDetails.vCoords =  <<105.76,-746.65,46.18>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_OMEGA_SHED1
			doorDetails.iDoorHash = MP_DOOR_OMEGA_SHED1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_OMEGA_SHED1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_ch3_01_trlrdoor_l
			doorDetails.vCoords =  <<2333.23, 2574.97, 47.03>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK	
		CASE MP_DN_OMEGA_SHED2
			doorDetails.iDoorHash = MP_DOOR_OMEGA_SHED2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_OMEGA_SHED2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_ch3_01_trlrdoor_r
			doorDetails.vCoords =  <<2329.65, 2576.64, 47.03>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK	
		CASE MP_DN_FRANK_SHOP_DOOR
			doorDetails.iDoorHash = MP_DOOR_FRANK_SHOP_DOOR
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_FRANK_SHOP_DOOR"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_FH_FRNTDOOR
			doorDetails.vCoords =  <<0.2169,-1823.3031,29.6391>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK	
		CASE MP_DN_ARMYBASE_LIFT_L
			doorDetails.iDoorHash = MP_DOOR_ARMYBASE_LIFT_L
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_ARMYBASE_LIFT_L"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_bl_doorel_l
			doorDetails.vCoords =  <<-2053.16, 3239.49, 30.50>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK	
		CASE MP_DN_ARMYBASE_LIFT_R
			doorDetails.iDoorHash = MP_DOOR_ARMYBASE_LIFT_R
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_ARMYBASE_LIFT_R"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_bl_doorel_r
			doorDetails.vCoords =  << -2054.39, 3237.23, 30.50 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK	
		
		CASE MP_DN_RAND_TRAILER_1
			doorDetails.iDoorHash = MP_DOOR_RAND_TRAILER_1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_RAND_TRAILER_1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_cs4_05_tdoor
			doorDetails.vCoords =  << 31.9180,3666.8535,40.8586 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_RAND_TRAILER_2
			doorDetails.iDoorHash = MP_DOOR_RAND_TRAILER_2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_RAND_TRAILER_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_magenta_door
			doorDetails.vCoords =  << 29.1020,3661.4893,40.8547 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		
		
		CASE MP_DN_H_INS_B
			doorDetails.iDoorHash = MP_DOOR_H_INS_B
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_H_INS_B"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = INT_TO_ENUM(MODEL_NAMES, HASH("hei_prop_heist_cutscene_doorb"))//hei_prop_heist_cutscene_doorb//replaced for 2610547
			doorDetails.vCoords = <<776.80, 4184.64, 41.91>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_H_VAL_A
			doorDetails.iDoorHash = MP_DOOR_H_VAL_A
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_H_VAL_A"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = INT_TO_ENUM(MODEL_NAMES, HASH("apa_Prop_apa_CutScene_DoorA")) //hei_prop_heist_cutscene_doora//replaced for 2610547
			doorDetails.vCoords = <<722.399,4187.952,41.231>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_H_SERA_1
			doorDetails.iDoorHash = MP_DOOR_H_SERA_1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_H_SERA_1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = hei_prop_heist_cutscene_doorc_r
			doorDetails.vCoords = <<610.594,-421.830,24.979>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_H_SERA_2
			doorDetails.iDoorHash = MP_DOOR_H_SERA_2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_H_SERA_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = hei_prop_heist_cutscene_doorc_l
			doorDetails.vCoords = <<610.874,-419.365,24.979>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			
			doorDetails.bPermanentState = FALSE
		BREAK
		
		
		CASE MP_DN_MERRYW_1
			doorDetails.iDoorHash = MP_DOOR_MERRYWEATHER_BASE_1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_MERRYWEATHER_BASE_1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_GATE_DOCKS_LD
			doorDetails.vCoords = <<492, -3116, 5>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.fRatio = 0.0
		BREAK
		
		CASE MP_DN_MERRYW_2
			doorDetails.iDoorHash = MP_DOOR_MERRYWEATHER_BASE_2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_MERRYWEATHER_BASE_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_GATE_DOCKS_LD
			doorDetails.vCoords = <<476, -3116, 5>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.fRatio = 0.0
			

		BREAK
		 // FEATURE_GANG_BOSS
		CASE MP_DN_LR_FS_1
			doorDetails.iDoorHash = MP_DOOR_LR_FS_1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_LR_FS_1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = INT_TO_ENUM(MODEL_NAMES, HASH("lr_prop_boathousedoor_l"))
			doorDetails.vCoords = <<1527.7452,3778.1309,34.7106>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_LR_FS_2
			doorDetails.iDoorHash = MP_DOOR_LR_FS_2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_LR_FS_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = INT_TO_ENUM(MODEL_NAMES, HASH("lr_prop_boathousedoor_r"))
			doorDetails.vCoords = <<1530.7625, 3780.2439, 34.7104>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
	ENDSWITCH
	SWITCH INT_TO_ENUM(MP_DN_ENUM,iIndex)
		//Barriers that will open when races are ongoing
//		CASE MP_DN_RACE_DOOR_0
//			doorDetails.iDoorHash = MP_RACE_DOOR_0
//			#IF IS_DEBUG_BUILD
//			doorDetails.sDoorName = "MP_RACE_DOOR_0"
//			#ENDIF
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = prop_gate_airport_01 
//			doorDetails.vCoords = <<-1008.07,-2406.75,12.97>>
//			doorDetails.bJustAdd = TRUE
//			doorDetails.bRaceDoor = TRUE
//		BREAK
//		CASE MP_DN_RACE_DOOR_1
//			doorDetails.iDoorHash = MP_RACE_DOOR_1
//			#IF IS_DEBUG_BUILD
//			doorDetails.sDoorName = "MP_RACE_DOOR_1"
//			#ENDIF
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = prop_gate_airport_01 
//			doorDetails.vCoords = <<-1015.49,-2419.59,12.95>>
//			doorDetails.bJustAdd = TRUE
//			doorDetails.bRaceDoor = TRUE
//		BREAK
//		CASE MP_DN_RACE_DOOR_2
//			doorDetails.iDoorHash = MP_RACE_DOOR_2
//			doorDetails.sDoorName = "MP_RACE_DOOR_2"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = prop_facgate_01 
//			doorDetails.vCoords = <<-1213.4,-2079.3,12.9>>
//			doorDetails.bJustAdd = TRUE
//			doorDetails.bRaceDoor = TRUE
//		BREAK
//		CASE MP_DN_RACE_DOOR_3
//			doorDetails.iDoorHash = MP_RACE_DOOR_3
//			#IF IS_DEBUG_BUILD
//			doorDetails.sDoorName = "MP_RACE_DOOR_3"
//			#ENDIF
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = prop_facgate_01 
//			doorDetails.vCoords = <<-984.08,-2348.4,12.94>>
//			doorDetails.bJustAdd = TRUE
//			doorDetails.bRaceDoor = TRUE
//		BREAK
//		CASE MP_DN_RACE_DOOR_4
//			doorDetails.iDoorHash = MP_RACE_DOOR_4
//			#IF IS_DEBUG_BUILD
//			doorDetails.sDoorName = "MP_RACE_DOOR_4"
//			#ENDIF
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = prop_facgate_01 
//			doorDetails.vCoords = <<-994.5,-2341.65,12.94>>
//			doorDetails.bJustAdd = TRUE
//			doorDetails.bRaceDoor = TRUE
//		BREAK
		CASE MP_DN_RACE_DOOR_5
			doorDetails.iDoorHash = MP_RACE_DOOR_5
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_5"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_03gate2 
			doorDetails.vCoords = <<-687.73,-2458.82,12.9>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_6
			doorDetails.iDoorHash = MP_RACE_DOOR_6
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_6"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_03gate2 
			doorDetails.vCoords = <<-697.82,-1226.5,12.91>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_7
			doorDetails.iDoorHash = MP_RACE_DOOR_7
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_7"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_03gate2 
			doorDetails.vCoords = <<-692.77,-2455.93,12.9>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
//		CASE MP_DN_RACE_DOOR_8
//			doorDetails.iDoorHash = MP_RACE_DOOR_8
//			doorDetails.sDoorName = "MP_RACE_DOOR_8"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = prop_gate_airport_01 
//			doorDetails.vCoords = <<-1151.21,-2723.09,12.95>>
//			doorDetails.bJustAdd = TRUE
//			doorDetails.bRaceDoor = TRUE
//		BREAK
		CASE MP_DN_RACE_DOOR_9
			doorDetails.iDoorHash = MP_RACE_DOOR_9
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_9"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_airport_01 
			doorDetails.vCoords = <<-891.93,-2748.71,12.95>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_10
			doorDetails.iDoorHash = MP_RACE_DOOR_10
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_10"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_airport_01
			doorDetails.vCoords = <<-896.46,-2746.42,12.95>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
//		CASE MP_DN_RACE_DOOR_11
//			doorDetails.iDoorHash = MP_RACE_DOOR_11
//			doorDetails.sDoorName = "MP_RACE_DOOR_11"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = prop_gate_airport_01 
//			doorDetails.vCoords = <<-1138.47,-2730.44,12.95>>
//			doorDetails.bJustAdd = TRUE
//			doorDetails.bRaceDoor = TRUE
//		BREAK
		CASE MP_DN_RACE_DOOR_12
			doorDetails.iDoorHash = MP_RACE_DOOR_12
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_12"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_airport_01 
			doorDetails.vCoords = <<-859.73,-2683.9,12.76>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
//		CASE MP_DN_RACE_DOOR_13
//			doorDetails.iDoorHash = MP_RACE_DOOR_13
//			#IF IS_DEBUG_BUILD
//			doorDetails.sDoorName = "MP_RACE_DOOR_13"
//			#ENDIF
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = prop_facgate_01 
//			doorDetails.vCoords = <<-1099.53,-2020.8,12.17>>
//			doorDetails.bJustAdd = TRUE
//			doorDetails.bRaceDoor = TRUE
//		BREAK
		CASE MP_DN_RACE_DOOR_14
			doorDetails.iDoorHash = MP_RACE_DOOR_14
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_14"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<-1019.61,-1897.02,13.37>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_15
			doorDetails.iDoorHash = MP_RACE_DOOR_15
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_15"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<-160.88,-2636.2,5.03>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_16
			doorDetails.iDoorHash = MP_RACE_DOOR_16
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_16"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<-148.71,-2636.2,5.03>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_17
			doorDetails.iDoorHash = MP_RACE_DOOR_17
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_17"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<237.78,-2936.96,5.05>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_18
			doorDetails.iDoorHash = MP_RACE_DOOR_18
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_18"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_docks_ld 
			doorDetails.vCoords = <<-188.28,-2204.61,9.3>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_19
			doorDetails.iDoorHash = MP_RACE_DOOR_19
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_19"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_docks_ld 
			doorDetails.vCoords = <<-127.56,-2185.29,9.3>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_20
			doorDetails.iDoorHash = MP_RACE_DOOR_20
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_20"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_docks_ld
			doorDetails.vCoords = <<-33.22,-2141.27,9.3>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_21
			doorDetails.iDoorHash = MP_RACE_DOOR_21
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_21"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<1000.68,-2454.99,27.57>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_22
			doorDetails.iDoorHash = MP_RACE_DOOR_22
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_22"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01
			doorDetails.vCoords = <<1012.91,-2456.23,27.53>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_23
			doorDetails.iDoorHash = MP_RACE_DOOR_23
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_23"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<1028.08,-2364.72,29.52>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_24
			doorDetails.iDoorHash = MP_RACE_DOOR_24
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_24"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<1033.22,-2299.1,29.52>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_25
			doorDetails.iDoorHash = MP_RACE_DOOR_25
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_25"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<1022.4,-2417.13,28.13>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_26
			doorDetails.iDoorHash = MP_RACE_DOOR_26
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_26"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_military_01 
			doorDetails.vCoords = <<2485.44,-432.71,91.97>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_27
			doorDetails.iDoorHash = MP_RACE_DOOR_27
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_27"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<2485.09,-335.84,91.98>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_28
			doorDetails.iDoorHash = MP_RACE_DOOR_28
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_28"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<2491.97,-303.48,91.99>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_29
			doorDetails.iDoorHash = MP_RACE_DOOR_29
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_29"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<1944.86,-957.69,78.15>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_30
			doorDetails.iDoorHash = MP_RACE_DOOR_30
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_30"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<1956.74,-956.86,78.15>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_31
			doorDetails.iDoorHash = MP_RACE_DOOR_31
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_31"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1845,2604.81,44.64>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_32
			doorDetails.iDoorHash = MP_RACE_DOOR_32
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_32"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1818.55,2604.81,44.6>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_33
			doorDetails.iDoorHash = MP_RACE_DOOR_33
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_33"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1799.74,2616.98,44.61>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_34
			doorDetails.iDoorHash = MP_RACE_DOOR_34
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_34"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1813.4,2488.63,44.47>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_35
			doorDetails.iDoorHash = MP_RACE_DOOR_35
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_35"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1808.82,2474.88,44.47>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_36
			doorDetails.iDoorHash = MP_RACE_DOOR_36
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_36"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1762.19,2426.73,44.44>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_37
			doorDetails.iDoorHash = MP_RACE_DOOR_37
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_37"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1749.22,2420.28,44.43>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_38
			doorDetails.iDoorHash = MP_RACE_DOOR_38
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_38"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1667.61,2408.11,44.42>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_39
			doorDetails.iDoorHash = MP_RACE_DOOR_39
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_39"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1661.76,2748.32,44.43>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_40
			doorDetails.iDoorHash = MP_RACE_DOOR_40
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_40"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1653.21,2409.71,44.42>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_41
			doorDetails.iDoorHash = MP_RACE_DOOR_41
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_41"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1558.14,2469.87,44.4>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_42
			doorDetails.iDoorHash = MP_RACE_DOOR_42
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_42"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1551.18,2482.58,44.4>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_43
			doorDetails.iDoorHash = MP_RACE_DOOR_43
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_43"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1547.41,2576.6,44.51>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_44
			doorDetails.iDoorHash = MP_RACE_DOOR_44
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_44"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1547.99,2591.08,44.51>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_45
			doorDetails.iDoorHash = MP_RACE_DOOR_45
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_45"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1576.19,2667.23,44.51>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_46
			doorDetails.iDoorHash = MP_RACE_DOOR_46
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_46"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1584.63,2679.02,44.51>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_47
			doorDetails.iDoorHash = MP_RACE_DOOR_47
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_47"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1648.95,2741.55,44.45>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_48
			doorDetails.iDoorHash = MP_RACE_DOOR_48
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_48"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1762.58,2751.9,44.45>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_49
			doorDetails.iDoorHash = MP_RACE_DOOR_49
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_49"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1776.26,2747.1,44.43>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_50
			doorDetails.iDoorHash = MP_RACE_DOOR_50
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_50"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1829.8,2702.92,44.45>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_51
			doorDetails.iDoorHash = MP_RACE_DOOR_51
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_51"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gate_prison_01 
			doorDetails.vCoords = <<1834.92,2689.37,44.45>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_52
			doorDetails.iDoorHash = MP_RACE_DOOR_52
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_52"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_03gate1 
			doorDetails.vCoords = <<222.07,-2013.99,18.41>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_53
			doorDetails.iDoorHash = MP_RACE_DOOR_53
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_53"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_03gate1 
			doorDetails.vCoords = <<210.93,-2022.58,17.65>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_54
			doorDetails.iDoorHash = MP_RACE_DOOR_54
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_54"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01b 
			doorDetails.vCoords = <<459.7,-2002.94,22.07>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_55
			doorDetails.iDoorHash = MP_RACE_DOOR_55
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_55"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<451.45,-1994.01,22.07>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_56
			doorDetails.iDoorHash = MP_RACE_DOOR_56
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_56"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01b 
			doorDetails.vCoords = <<455.74,-1944.85,23.66>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_57
			doorDetails.iDoorHash = MP_RACE_DOOR_57
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_57"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<466.76,-1939.65,23.66>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_58
			doorDetails.iDoorHash = MP_RACE_DOOR_58
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_58"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01b 
			doorDetails.vCoords = <<539.83,-1901.88,24.22>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_59
			doorDetails.iDoorHash = MP_RACE_DOOR_59
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_59"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<550.87,-1896.75,24.15>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_60
			doorDetails.iDoorHash = MP_RACE_DOOR_60
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_60"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<564.12,-1903,23.71>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_61
			doorDetails.iDoorHash = MP_RACE_DOOR_61
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_61"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01b
			doorDetails.vCoords = <<570.6271,-1913.3320,23.7104>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_62
			doorDetails.iDoorHash = MP_RACE_DOOR_62
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_62"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_03gate1 
			doorDetails.vCoords = <<1081.65,-1818.42,36.43>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_63
			doorDetails.iDoorHash = MP_RACE_DOOR_63
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_63"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<987.52,-1176.82,24.55>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_64
			doorDetails.iDoorHash = MP_RACE_DOOR_64
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_64"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<705.64,-1319.67,24.96>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_65
			doorDetails.iDoorHash = MP_RACE_DOOR_65
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_65"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<712.95,-1329.42,24.99>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_66
			doorDetails.iDoorHash = MP_RACE_DOOR_66
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_66"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<796.46,-921.49,24.4>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_67
			doorDetails.iDoorHash = MP_RACE_DOOR_67
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_67"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<795.78,-909.33,24.36>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_68
			doorDetails.iDoorHash = MP_RACE_DOOR_68
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_68"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<795.16,-896.64,24.1>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_69
			doorDetails.iDoorHash = MP_RACE_DOOR_69
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_69"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_01 
			doorDetails.vCoords = <<794.48,-884.48,24.06>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_70
			doorDetails.iDoorHash = MP_RACE_DOOR_70
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_70"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_03gate2 
			doorDetails.vCoords = <<546.17,-1869.94,24.53>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_71
			doorDetails.iDoorHash = MP_RACE_DOOR_71
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_71"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_03gate1 
			doorDetails.vCoords = <<492.33,-1410.47,28.43>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_72
			doorDetails.iDoorHash = MP_RACE_DOOR_72
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_72"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_03gate1
			doorDetails.vCoords = <<484.57,-1408.5,28.48>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_73
			doorDetails.iDoorHash = MP_RACE_DOOR_73
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_73"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_03gate1 
			doorDetails.vCoords = <<469.33,-1272.7,28.82>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_74
			doorDetails.iDoorHash = MP_RACE_DOOR_74
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_74"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_03gate1 
			doorDetails.vCoords = <<487.87,-1272.59,28.86>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_75
			doorDetails.iDoorHash = MP_RACE_DOOR_75
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_75"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_08 
			doorDetails.vCoords = <<488.89,-1011.67,27.14>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_76
			doorDetails.iDoorHash = MP_RACE_DOOR_76
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_76"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_03gate1 
			doorDetails.vCoords = <<527.55,-1597.33,28.39>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_77
			doorDetails.iDoorHash = MP_RACE_DOOR_77
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_77"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_03gate1 
			doorDetails.vCoords = <<544.94,-1649.75,27.49>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_78
			doorDetails.iDoorHash = MP_RACE_DOOR_78
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_78"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_03gate2 
			doorDetails.vCoords = <<151.35,-1668.64,28.77>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_79
			doorDetails.iDoorHash = MP_RACE_DOOR_79
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_79"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_fnclink_03gate1 
			doorDetails.vCoords = <<47.2,-1448.3,28.44>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_80
			doorDetails.iDoorHash = MP_RACE_DOOR_80
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_80"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_lrggate_03a 
			doorDetails.vCoords = <<-1876.37,194.85,83.33>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_81
			doorDetails.iDoorHash = MP_RACE_DOOR_81
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_81"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_lrggate_03a
			doorDetails.vCoords = <<-1868.31,183.79,83.34>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_82
			doorDetails.iDoorHash = MP_RACE_DOOR_82
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_82"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_lrggate_03a 
			doorDetails.vCoords = <<-1452.43,37.03,51.75>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_83
			doorDetails.iDoorHash = MP_RACE_DOOR_83
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_83"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_lrggate_03a 
			doorDetails.vCoords = <<-1483.1,46.93,53.26>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
//		CASE MP_DN_RACE_DOOR_84
//			doorDetails.iDoorHash = MP_RACE_DOOR_84
//			doorDetails.sDoorName = "MP_RACE_DOOR_84"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = prop_lrggate_02_ld 
//			doorDetails.vCoords = <<-1474.13,68.39,52.52>>
//			doorDetails.bJustAdd = TRUE
//			doorDetails.bRaceDoor = TRUE
//		BREAK
		CASE MP_DN_RACE_DOOR_85
			doorDetails.iDoorHash = MP_RACE_DOOR_85
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_85"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_lrggate_03a 
			doorDetails.vCoords = <<-1507.47,39.16,54.41>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_86
			doorDetails.iDoorHash = MP_RACE_DOOR_86
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_86"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_lrggate_03a 
			doorDetails.vCoords = <<-1528.36,35.77,55.78>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_87
			doorDetails.iDoorHash = MP_RACE_DOOR_87
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_87"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_lrggate_02_ld 
			doorDetails.vCoords = <<-1583.28,40.14,59.32>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_88
			doorDetails.iDoorHash = MP_RACE_DOOR_88
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_88"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_lrggate_02_ld 
			doorDetails.vCoords = <<-1616.23,79.78,60.78>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_89
			doorDetails.iDoorHash = MP_RACE_DOOR_89
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_89"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_lrggate_03a 
			doorDetails.vCoords = <<-924.98,-9.03,43.28>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_90
			doorDetails.iDoorHash = MP_RACE_DOOR_90
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_90"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_lrggate_03a 
			doorDetails.vCoords = <<-905.7,14.66,45.66>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		CASE MP_DN_RACE_DOOR_91
			doorDetails.iDoorHash = MP_RACE_DOOR_91
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_91"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_lrggate_02_ld 
			doorDetails.vCoords = <<-844.05,155.96,66.03>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
//		CASE MP_DN_RACE_DOOR_92
//			doorDetails.iDoorHash = MP_RACE_DOOR_92
//			doorDetails.sDoorName = "MP_RACE_DOOR_92"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = bh1_48_b_gate2 
//			doorDetails.vCoords = <<-846.23,119.73,54.64>>
//			doorDetails.bJustAdd = TRUE
//			doorDetails.bRaceDoor = TRUE
//		BREAK
//		CASE MP_DN_RACE_DOOR_93
//			doorDetails.iDoorHash = MP_RACE_DOOR_93
//			doorDetails.sDoorName = "MP_RACE_DOOR_93"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = bh1_48_b_gate003 
//			doorDetails.vCoords = <<-841.21,97.76,51.8>>
//			doorDetails.bJustAdd = TRUE
//			doorDetails.bRaceDoor = TRUE
//		BREAK
		CASE MP_DN_RACE_DOOR_92
			doorDetails.iDoorHash = MP_RACE_DOOR_92
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_RACE_DOOR_92"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_lrggate_04a 
			doorDetails.vCoords = <<-914.32,184.54,68.42>>
			doorDetails.bJustAdd = TRUE
			doorDetails.bRaceDoor = TRUE
		BREAK
		
		CASE MP_DN_TREV_FRONT
			doorDetails.iDoorHash = MP_DOOR_TREV_FRONT
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_TREV_FRONT"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_TREV_DOORFRONT 
			doorDetails.vCoords = <<-1149.71,-1521.09,10.79>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_AIR_TOWER_EXT1
			doorDetails.iDoorHash = MP_DOOR_AIR_TOWER_EXT1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIR_TOWER_EXT1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel =V_ILEV_CT_DOOR01
			doorDetails.vCoords = << -2343.53, 3265.37, 32.96 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_AIR_TOWER_EXT2
			doorDetails.iDoorHash = MP_DOOR_AIR_TOWER_EXT2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_AIR_TOWER_EXT2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_CT_DOOR01 
			doorDetails.vCoords = << -2342.23, 3267.62, 32.96 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
//		CASE MP_DN_LO_APT_DOOR
//			doorDetails.iDoorHash = MP_DOOR_LO_APT_DOOR
//			#IF IS_DEBUG_BUILD
//			doorDetails.sDoorName = "MP_DOOR_LO_APT_DOOR"
//			#ENDIF
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = V_ILEV_JANITOR_FRONTDOOR 
//			doorDetails.vCoords = << 265.8279,-1001.5776,-98.8587 >>
//			doorDetails.bUseStateSystem = TRUE
//			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//		BREAK

		CASE MP_DN_MED_APT_DOOR
			doorDetails.iDoorHash = MP_DOOR_MED_APT_DOOR
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_MED_APT_DOOR"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_housedoor1
			doorDetails.vCoords = << 347.8678,-1003.3159,-99.0952>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		// Bathroom doors
		CASE MP_DN_LOW_APT_BATH_DOOR
			doorDetails.iDoorHash = MP_LOW_APT_BATH_DOOR
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_LOW_APT_BATH_DOOR"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_MM_DOOR
			doorDetails.vCoords = <<257.2896, -1001.2546, -98.8587>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_MED_APT_BATH_DOOR
			doorDetails.iDoorHash = MP_MED_APT_BATH_DOOR
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_MED_APT_BATH_DOOR"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_MM_DOORW
			doorDetails.vCoords = <<348.2157, -993.1122, -99.0430>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_HIGH_APT_BATH_DOOR
			doorDetails.iDoorHash = MP_HIGH_APT_BATH_DOOR
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_HIGH_APT_BATH_DOOR"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_FH_BEDRMDOOR
			doorDetails.vCoords = <<-789.3017, 332.0119, 201.5596>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_MICH_GAR
			doorDetails.iDoorHash = MP_DOOR_MICH_GAR
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_MICH_GAR"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_LD_GARAGED_01
			doorDetails.vCoords = << -815.3282,185.9571,72.99>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_MICH_WIND
			doorDetails.iDoorHash = MP_DOOR_MICH_WIND
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_MICH_WIND"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_MM_WINDOWWC
			doorDetails.vCoords = <<-802.7333,167.5041,77.5824 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_MICH_FRONT1
			doorDetails.iDoorHash = MP_DOOR_MICH_FRONT1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_MICH_FRONT1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_MM_DOORM_L
			doorDetails.vCoords = <<-816.7160,179.0980,72.84 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_MICH_FRONT2
			doorDetails.iDoorHash = MP_DOOR_MICH_FRONT2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_MICH_FRONT2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_MM_DOORM_R
			doorDetails.vCoords = <<-816.1068,177.5109,72.8274 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_MICH_BACK1
			doorDetails.iDoorHash = MP_DOOR_MICH_BACK1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_MICH_BACK1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_BH1_48_BACKDOOR_L
			doorDetails.vCoords = <<-796.5657,177.2214,73.0405 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_MICH_BACK2
			doorDetails.iDoorHash = MP_DOOR_MICH_BACK2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_MICH_BACK2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_BH1_48_BACKDOOR_R
			doorDetails.vCoords = <<-794.5051,178.0124,73.0405 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_MICH_BACK3
			doorDetails.iDoorHash = MP_DOOR_MICH_BACK3
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_MICH_BACK3"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_BH1_48_BACKDOOR_L  
			doorDetails.vCoords = << -793.3943,180.5075,73.0405>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_MICH_BACK4
			doorDetails.iDoorHash = MP_DOOR_MICH_BACK4
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_MICH_BACK4"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_BH1_48_BACKDOOR_R 
			doorDetails.vCoords = <<-794.1853,182.5680,73.0405 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_MICH_GAR_INT
			doorDetails.iDoorHash = MP_DOOR_MICH_GAR_INT
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_MICH_GAR_INT"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_mm_door
			doorDetails.vCoords = <<-806.2817,187.0246,72.6240 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_CULT_OUTPOST1
			doorDetails.iDoorHash = MP_DOOR_CULT_OUTPOST1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CULT_OUTPOST1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_GATE_CULT_01_L
			doorDetails.vCoords = << -1041.2676,4906.0967,209.2002>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_FORCE_LOCKED_THIS_FRAME
			doorDetails.fRatio = -1.0
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_CULT_OUTPOST2
			doorDetails.iDoorHash = MP_DOOR_CULT_OUTPOST2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CULT_OUTPOST2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_GATE_CULT_01_R
			doorDetails.vCoords = <<-1044.7490,4914.9717,209.1932 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_FORCE_LOCKED_THIS_FRAME
			doorDetails.fRatio = 1.0
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_TREV_TRAILER
			doorDetails.iDoorHash = MP_DOOR_TREV_TRAILER
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_TREV_TRAILER"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_TREVTRAILDR
			doorDetails.vCoords = <<1972.7689,3815.3660,33.6633 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_FA_FRONTDOOR
			doorDetails.iDoorHash = MP_DOOR_FA_FRONTDOOR
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_FA_FRONTDOOR"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_FA_FRONTDOOR
			doorDetails.vCoords = <<-14.8689,-1441.1821,31.192>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_SWEATS_LEFT
			doorDetails.iDoorHash = MP_DOOR_SWEATS_LEFT
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_SWEATS_LEFT"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_SS_DOOR8
			doorDetails.vCoords = <<716.7808,-975.4207,25.057>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//			#IF IS_DEBUG_BUILD
//			#IF FEATURE_HEIST_PLANNING
//				doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
//			#ENDIF
//			#ENDIF
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_SWEATS_RIGHT
			doorDetails.iDoorHash = MP_DOOR_SWEATS_RIGHT
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_SWEATS_RIGHT"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_SS_DOOR7
			doorDetails.vCoords = <<719.3818,-975.4185,25.0057>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//			#IF IS_DEBUG_BUILD
//			#IF FEATURE_HEIST_PLANNING
//				doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
//			#ENDIF
//			#ENDIF
			doorDetails.bPermanentState = TRUE
		BREAK
		
		
		CASE MP_DN_SWEATS_OFFICE
			doorDetails.iDoorHash = MP_DOOR_SWEATS_OFFICE
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_SWEATS_OFFICE"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_SS_DOOR02
			doorDetails.vCoords = <<710, -964, 31 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_BIOLAB_LOWER_LIFT_L
			doorDetails.iDoorHash = MP_DOOR_BIOLAB_LOWER_LIFT_L
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_BIOLAB_LOWER_LIFT_L"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILev_BL_DoorEL_L
			doorDetails.vCoords = <<3539.0, 3673.7, 20.0>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_BIOLAB_LOWER_LIFT_R
			doorDetails.iDoorHash = MP_DOOR_BIOLAB_LOWER_LIFT_R
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_BIOLAB_LOWER_LIFT_R"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILev_BL_DoorEL_R
			doorDetails.vCoords = <<3541.6, 3673.2, 20.0>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_SWEATS_OFFICE2
			doorDetails.iDoorHash = MP_DOOR_SWEATS_OFFICE2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_SWEATS_OFFICE2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_STORE_DOOR
			doorDetails.vCoords = <<708, -962, 31 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			
			doorDetails.bPermanentState = TRUE
		BREAK
		
		
		CASE MP_DN_JEWEL_LEFT
			doorDetails.iDoorHash = MP_DOOR_JEWEL_LEFT
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_JEWEL_LEFT"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = P_JEWEL_DOOR_L
			doorDetails.vCoords = <<-631.9554,-236.3333,38.2065>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_JEWEL_RIGHT
			doorDetails.iDoorHash = MP_DOOR_JEWEL_RIGHT
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_JEWEL_RIGHT"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = P_JEWEL_DOOR_R1
			doorDetails.vCoords = <<-630.4265,-238.4375,38.2065>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_METH_LAB0
			doorDetails.iDoorHash = MP_DOOR_METH_LAB0
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_METH_LAB0"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_SS_DOOR04
			doorDetails.vCoords = <<1395.6134,3609.3269,35.1308>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_METH_LAB1
			doorDetails.iDoorHash = MP_DOOR_METH_LAB1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_METH_LAB1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_SS_DOOREXT
			doorDetails.vCoords = <<1388.4987,3614.8276,39.0919>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_METH_LAB2
			doorDetails.iDoorHash = MP_DOOR_METH_LAB2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_METH_LAB2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_SS_DOOREXT
			doorDetails.vCoords = <<1399.6996,3607.7629,39.0919>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_CHOPS_GAR
			doorDetails.iDoorHash = MP_DOOR_CHOPS_GAR
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CHOPS_GAR"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_COM_GAR_DOOR_01
			doorDetails.vCoords = <<484.5642,-1315.5740,30.2033>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_CHOPS_DOOR
			doorDetails.iDoorHash = MP_DOOR_CHOPS_DOOR
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CHOPS_DOOR"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_CS_DOOR
			doorDetails.vCoords = <<482.8112,-1311.9530,29.3506>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_STORE_ROOM
			doorDetails.iDoorHash = MP_DOOR_STORE_ROOM
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_STORE_ROOM"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_epsstoredoor
			doorDetails.vCoords = <<241.3621, 361.0471, 105.003>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_FH_FRONT
			doorDetails.iDoorHash = MP_DOOR_FH_FRONT
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_FH_FRONT"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_FH_FRONTDOOR
			doorDetails.vCoords = <<7.5179,539.5260,176.1781>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_FH_GAR
			doorDetails.iDoorHash = MP_DOOR_FH_GAR
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_FH_GAR"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_CH_025C_G_DOOR_01
			doorDetails.vCoords = <<18.6504,546.3401,176.3448>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
//		CASE MP_DN_FH_SLIDE1
//			doorDetails.iDoorHash = MP_DOOR_FH_SLIDE1
//			doorDetails.sDoorName = "MP_DOOR_FH_SLIDE1"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = V_ILEV_FH_SLIDINGDOOR
//			doorDetails.vCoords = <<-9.7962,514.4293,173.6281>>
//			doorDetails.bUseStateSystem = TRUE
//			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//		BREAK
//		
//		CASE MP_DN_FH_SLIDE2
//			doorDetails.iDoorHash = MP_DOOR_FH_SLIDE2
//			doorDetails.sDoorName = "MP_DOOR_FH_SLIDE2"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = V_ILEV_CM_DOOR1
//			doorDetails.vCoords = <<241.3574,361.0488,105.8963>>
//			doorDetails.bUseStateSystem = TRUE
//			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//		BREAK

//		CASE MP_DN_GUN_LEFT
//			doorDetails.iDoorHash = MP_DOOR_GUN_LEFT
//			doorDetails.sDoorName = "MP_DOOR_GUN_LEFT"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = V_ILEV_GC_DOOR04
//			doorDetails.vCoords = <<813.1779,-2148.2695,29.7689>>
//			doorDetails.bUseStateSystem = TRUE
//			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//		BREAK
//		
//		CASE MP_DN_GUN_RIGHT
//			doorDetails.iDoorHash = MP_DOOR_GUN_RIGHT
//			doorDetails.sDoorName = "MP_DOOR_GUN_RIGHT"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = V_ILEV_GC_DOOR03
//			doorDetails.vCoords = <<810.5769,-2148.2695,29.7689>>
//			doorDetails.bUseStateSystem = TRUE
//			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//		BREAK
		
		CASE MP_DN_CARSHOW_1L
			doorDetails.iDoorHash = MP_DOOR_CARSHOW_1L
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CARSHOW_1L"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_CSR_DOOR_L
			doorDetails.vCoords = <<-59.8930,-1092.9518,26.8836>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_CARSHOW_1R
			doorDetails.iDoorHash = MP_DOOR_CARSHOW_1R
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CARSHOW_1R"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_CSR_DOOR_R
			doorDetails.vCoords = <<-60.5458,-1094.7489,26.8887>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_CARSHOW_2L
			doorDetails.iDoorHash = MP_DOOR_CARSHOW_2L
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CARSHOW_2L"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_CSR_DOOR_L
			
			doorDetails.vCoords = <<-38.4640,-1108.4462,26.7198>>
			doorDetails.bUseStateSystem = TRUE
			#IF FEATURE_DLC_1_2022
			IF NOT g_sMPTunables.bDISABLE_SIMEON_SHOWROOM
				doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			ELSE
			#ENDIF
				doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
				doorDetails.bPermanentState = TRUE
			#IF FEATURE_DLC_1_2022
			ENDIF
			#ENDIF
			
		BREAK
		
		CASE MP_DN_CARSHOW_2R
			doorDetails.iDoorHash = MP_DOOR_CARSHOW_2R
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_CARSHOW_2R"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_CSR_DOOR_R
			doorDetails.vCoords = <<-36.6615,-1109.1016,26.7198>>
			doorDetails.bUseStateSystem = TRUE
			#IF FEATURE_DLC_1_2022
			IF NOT g_sMPTunables.bDISABLE_SIMEON_SHOWROOM
				doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			ELSE
			#ENDIF
				doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
				doorDetails.bPermanentState = TRUE
			#IF FEATURE_DLC_1_2022
			ENDIF
		BREAK
		
		CASE MP_DN_SALVAGE_1L
			doorDetails.iDoorHash = MP_DOOR_SALVAGE_1L
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_SALVAGE_1L"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_RC_DOOR3_L
			doorDetails.vCoords = <<-608.7289,-1610.3153,27.1589>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_SALVAGE_1R
			doorDetails.iDoorHash = MP_DOOR_SALVAGE_1R
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_SALVAGE_1R"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_RC_DOOR3_R
			doorDetails.vCoords = <<-611.3200,-1610.0886,27.1589>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_SALVAGE_2L
			doorDetails.iDoorHash = MP_DOOR_SALVAGE_2L
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_SALVAGE_2L"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_RC_DOOR3_L
			doorDetails.vCoords = <<-592.9376,-1631.5770,27.1593>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_SALVAGE_2R
			doorDetails.iDoorHash = MP_DOOR_SALVAGE_2R
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_SALVAGE_2R"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_RC_DOOR3_R
			doorDetails.vCoords = <<-592.7109,-1628.9860,27.1593>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_SHERRIF1_L1
			doorDetails.iDoorHash = MP_DOOR_SHERRIF1_L1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_SHERRIF1_L1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_SHRF2DOOR
			doorDetails.vCoords = <<-442.6600,6015.2217,31.8663>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			//doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_SHERRIF1_R1
			doorDetails.iDoorHash = MP_DOOR_SHERRIF1_R1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_SHERRIF1_R1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_SHRF2DOOR
			doorDetails.vCoords = <<-444.4985,6017.0601,31.8663>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			//doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_SHERRIF2
			doorDetails.iDoorHash = MP_DOOR_SHERRIF2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_SHERRIF2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_SHRFDOOR
			doorDetails.vCoords = <<1855.6848,3683.9302,34.5928>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			//doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_RAND_GARAGE1
			doorDetails.iDoorHash = MP_DOOR_RAND_GARAGE1
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_RAND_GARAGE1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_GAR_DOOR_01
			doorDetails.vCoords = <<-1067.0024,-1665.6091,4.7898>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_RAND_GARAGE2
			doorDetails.iDoorHash = MP_DOOR_RAND_GARAGE2
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_RAND_GARAGE2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_GAR_DOOR_02
			doorDetails.vCoords = <<-1064.7632,-1668.7623,4.8084>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_RAND_GARAGE3
			doorDetails.iDoorHash = MP_DOOR_RAND_GARAGE3
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_RAND_GARAGE3"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_GAR_DOOR_03_ld
			doorDetails.vCoords = <<-1074.6482,-1676.1313,4.684>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_RAND_GARAGE4
			doorDetails.iDoorHash = MP_DOOR_RAND_GARAGE4
			#IF IS_DEBUG_BUILD
			doorDetails.sDoorName = "MP_DOOR_RAND_GARAGE4"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_GAR_DOOR_05
			doorDetails.vCoords = <<201.400,-153.3652,57.8522>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
//		CASE MP_DN_WINDFARM_GAR1
//			doorDetails.iDoorHash = MP_DOOR_WINDFARM_GAR1
//			#IF IS_DEBUG_BUILD
//			doorDetails.sDoorName = "MP_DOOR_WINDFARM_GAR1"
//			#ENDIF
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = PROP_fnclink_02Gate3
//			doorDetails.vCoords = <<2484.0889,1566.9988,31.7453>>
//			doorDetails.bUseStateSystem = TRUE
//			doorDetails.theDoorDefaultState = DOORSTATE_FORCE_OPEN_THIS_FRAME
//			doorDetails.bPermanentState = TRUE
//		BREAK
//		CASE MP_DN_WINDFARM_GAR2
//			doorDetails.iDoorHash = MP_DOOR_WINDFARM_GAR2
//			#IF IS_DEBUG_BUILD
//			doorDetails.sDoorName = "MP_DOOR_WINDFARM_GAR2"
//			#ENDIF
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = PROP_fnclink_02Gate3
//			doorDetails.vCoords = <<2494.2651,1587.5674,31.7398>>
//			doorDetails.bUseStateSystem = TRUE
//			doorDetails.theDoorDefaultState = DOORSTATE_FORCE_OPEN_THIS_FRAME
//			doorDetails.bPermanentState = TRUE
//		BREAK
		
//		CASE MP_DN_GOV_FAC_GATE1
//			doorDetails.iDoorHash = MP_DOOR_GOV_FAC_GATE1
//			doorDetails.sDoorName = "MP_DOOR_GOV_FAC_GATE1"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = PROP_FACGATE_05_R
//			doorDetails.vCoords = <<2569.2212,-325.5664,94.1236>>
//			doorDetails.bUseStateSystem = TRUE
//			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//		BREAK
//		
//		CASE MP_DN_GOV_FAC_GATE2
//			doorDetails.iDoorHash = MP_DOOR_GOV_FAC_GATE2
//			doorDetails.sDoorName = "MP_DOOR_GOV_FAC_GATE2"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = PROP_FACGATE_05_R
//			doorDetails.vCoords = <<2559.6140,-325.5558,94.1236>>
//			doorDetails.bUseStateSystem = TRUE
//			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//		BREAK
//		
//		CASE MP_DN_GOV_FAC_GATE3
//			doorDetails.iDoorHash = MP_DOOR_GOV_FAC_GATE3
//			doorDetails.sDoorName = "MP_DOOR_GOV_FAC_GATE3"
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = PROP_FACGATE_01
//			doorDetails.vCoords = <<2491.8701,-303.4876,91.9854>>
//			doorDetails.bUseStateSystem = TRUE
//			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
//		BREAK
		
//		CASE MP_DN_MOD_SHOP_OUTER1
//			doorDetails.iDoorHash = MP_DOOR_MOD_SHOP_OUTER1
//			#IF IS_DEBUG_BUILD
//			doorDetails.sDoorName = "MP_DOOR_MOD_SHOP_OUTER1"
//			#ENDIF
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = v_ilev_carmod3Door
//			doorDetails.vCoords = <<108.8502,6617.8770,32.6731>>
//			doorDetails.bUseStateSystem = FALSE
//			doorDetails.bHoldOpen = TRUE
//			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
//			doorDetails.bPermanentState = TRUE
//		BREAK
//		
//		CASE MP_DN_MOD_SHOP_OUTER2
//			doorDetails.iDoorHash = MP_DOOR_MOD_SHOP_OUTER2
//			#IF IS_DEBUG_BUILD
//				doorDetails.sDoorName = "MP_DOOR_MOD_SHOP_OUTER2"
//			#ENDIF
//			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
//			doorDetails.doorModel = v_ilev_carmod3Door
//			doorDetails.vCoords = <<114.3135,6623.2334,32.671>>
//			doorDetails.bUseStateSystem = FALSE
//			doorDetails.bHoldOpen = TRUE
//			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
//			doorDetails.bPermanentState = TRUE
//		BREAK
		
		CASE MP_DN_LIFE_INV_FRONT_L
			doorDetails.iDoorHash = MP_DOOR_LIFE_INV_FRONT_L
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_LIFE_INV_FRONT_L"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_FB_DOORSHORTL
			doorDetails.vCoords = << -1045.1199, -232.0040, 39.4379 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		CASE MP_DN_LIFE_INV_FRONT_R
			doorDetails.iDoorHash = MP_DOOR_LIFE_INV_FRONT_R
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_LIFE_INV_FRONT_R"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_FB_DOORSHORTR
			doorDetails.vCoords = << -1046.5161, -229.3581, 39.4379 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		CASE MP_DN_LIFE_INV_REAR_L
			doorDetails.iDoorHash = MP_DOOR_LIFE_INV_REAR_L
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_LIFE_INV_REAR_L"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_FB_DOOR01
			doorDetails.vCoords = << -1083.6201, -260.4167, 38.1867 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		CASE MP_DN_LIFE_INV_REAR_R
			doorDetails.iDoorHash = MP_DOOR_LIFE_INV_REAR_R
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_LIFE_INV_REAR_R"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_FB_DOOR02
			doorDetails.vCoords = << -1080.9744, -259.0204, 38.1867 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		CASE MP_DN_LIFE_INV_SIDE_01
			doorDetails.iDoorHash = MP_DOOR_LIFE_INV_SIDE_01
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_LIFE_INV_SIDE_01"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_GTDOOR
			doorDetails.vCoords = << -1042.57, -240.60, 38.11 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		CASE MP_DN_LIFE_INV_ORANGE
			doorDetails.iDoorHash = MP_DOOR_LIFE_INV_ORANGE
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_LIFE_INV_ORANGE"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_door_orange
			doorDetails.vCoords = <<-1063.804,-240.832,39.883>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		CASE MP_DN_LIFE_INV_ORANGESOLID
			doorDetails.iDoorHash = MP_DOOR_LIFE_INV_ORANGESOLID
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_LIFE_INV_ORANGESOLID"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_DOOR_ORANGESOLID
			doorDetails.vCoords = <<-1055.958,-236.425,44.171>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_TEQUILA_CLUB_DOOR_F
			doorDetails.iDoorHash = MP_DOOR_TEQUILA_CLUB_DOOR_F
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_TEQUILA_CLUB_DOOR_F"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_ROC_DOOR4
			doorDetails.vCoords = << -565.1712, 276.6259, 83.2863 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_TEQUILA_CLUB_DOOR_R
			doorDetails.iDoorHash = MP_DOOR_TEQUILA_CLUB_DOOR_R
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_TEQUILA_CLUB_DOOR_R"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_ROC_DOOR4
			doorDetails.vCoords = << -561.2863, 293.5043, 87.7771 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_FOUNDRY1
			doorDetails.iDoorHash = MP_DOOR_FOUNDRY1
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_FOUNDRY1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_RON_DOOR_01
			doorDetails.vCoords = << 1065.2372,-2006.0791,32.2329 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_FOUNDRY2
			doorDetails.iDoorHash = MP_DOOR_FOUNDRY2
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_FOUNDRY2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_RON_DOOR_01
			doorDetails.vCoords = << 1083.5472,-1975.4354,31.622 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_FOUNDRY3
			doorDetails.iDoorHash = MP_DOOR_FOUNDRY3
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_FOUNDRY3"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_RON_DOOR_01
			doorDetails.vCoords = << 1085.3070,-2018.5614,41.6289 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_FRANK1_DEALER_DOOR
			doorDetails.iDoorHash = MP_DOOR_FRANK1_DEALER_DOOR
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_FRANK1_DEALER_DOOR"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_HOUSEDOOR1
			doorDetails.vCoords = << 86.6624,-1959.4771,21.2152 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_ABATTOIR_EXIT
			doorDetails.iDoorHash = MP_DOOR_ABATTOIR_EXIT
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_ABATTOIR_EXIT"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_ABAT_SLIDE
			doorDetails.vCoords = <<962.9084, -2105.8137, 34.6432>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_GARAGE_JETSTEAL
			doorDetails.iDoorHash = MP_DOOR_GARAGE_JETSTEAL
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_GARAGE_JETSTEAL"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_gar_door_04
			doorDetails.vCoords = << 778.31, -1867.49, 30.66 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_NOSE_REAR_L
			doorDetails.iDoorHash = MP_DOOR_NOSE_REAR_L
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_NOSE_REAR_L"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_ch3_04_door_01l
			doorDetails.vCoords = <<2514.32, -317.34, 93.32>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_NOSE_REAR_R
			doorDetails.iDoorHash = MP_DOOR_NOSE_REAR_R
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_NOSE_REAR_R"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_ch3_04_door_01r
			doorDetails.vCoords = <<2512.42, -319.26, 93.32>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_RAND_APT_0
			doorDetails.iDoorHash = MP_DOOR_RAND_APT_0
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_RAND_APT_0"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_sc1_12_door
			doorDetails.vCoords = <<-58.47, -1530.51, 34.54>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_KORTZ0
			doorDetails.iDoorHash = MP_DOOR_KORTZ0
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_KORTZ0"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_ch1_07_door_01l
			doorDetails.vCoords = <<-2255.1938,322.2593,184.9264>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_KORTZ1
			doorDetails.iDoorHash = MP_DOOR_KORTZ1
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_KORTZ1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_ch1_07_door_01r
			doorDetails.vCoords = <<-2254.0557,319.7009,184.9264>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_KORTZ2
			doorDetails.iDoorHash = MP_DOOR_KORTZ2
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_KORTZ2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_ch1_07_door_01l
			doorDetails.vCoords = <<-2280.597,265.432,184.926>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		CASE MP_DN_KORTZ3
			doorDetails.iDoorHash = MP_DOOR_KORTZ3
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_KORTZ3"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_ch1_07_door_01r
			doorDetails.vCoords = <<-2278.039,266.570,184.926>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_IMPOUND_GATE_1
			doorDetails.iDoorHash = MP_DOOR_IMPOUND_GATE_1
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_IMPOUND_GATE_1"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_08
			doorDetails.vCoords = <<413.3649,-1620.0333,28.3416>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_IMPOUND_GATE_2
			doorDetails.iDoorHash = MP_DOOR_IMPOUND_GATE_2
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_IMPOUND_GATE_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_08
			doorDetails.vCoords = <<418.2896,-1651.3962,28.2951>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_IMPOUND_GATE_3
			doorDetails.iDoorHash = MP_DOOR_IMPOUND_GATE_3
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_IMPOUND_GATE_3"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_facgate_07b
			doorDetails.vCoords = <<397.8846,-1607.3838, 28.3301>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_FRANK_GAR
			doorDetails.iDoorHash = MP_DOOR_FRANK_GAR
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_FRANK_GAR"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_SC1_21_G_DOOR_01
			doorDetails.vCoords = << -25.28, -1431.06, 30.84 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_GENTRY_GAR
			doorDetails.iDoorHash = MP_DOOR_GENTRY_GAR
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_GENTRY_GAR"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_ss1_14_garage_door
			doorDetails.vCoords = << -62.380,352.7173,113.2499 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
			//doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_RAND_MOTEL_DOOR
			doorDetails.iDoorHash = MP_DOOR_RAND_MOTEL_DOOR
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_RAND_MOTEL_DOOR"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_MOTEL_DOOR_09
			doorDetails.vCoords = << 549.2567,-1773.115,33.7309 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
			//doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_LES_BIANCO_L
			doorDetails.iDoorHash = MP_DOOR_LES_BIANCO_L
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_LES_BIANCO_L"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_SS1_10_DOOR_L
			doorDetails.vCoords = << -720.39, 256.86, 80.29 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_LES_BIANCO_R
			doorDetails.iDoorHash = MP_DOOR_LES_BIANCO_R
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_LES_BIANCO_R"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = PROP_SS1_10_DOOR_R
			doorDetails.vCoords = << -718.42, 257.79, 80.29 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_COP_CELLDR
			doorDetails.iDoorHash = MP_DOOR_POLICE_CELL
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_POLICE_CELL"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_ph_cellgate
			doorDetails.vCoords = <<461.8065, -994.4086, 25.0644 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
		//	doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_CF_OFFICE
			doorDetails.iDoorHash = MP_DOOR_CF_OFFICE
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_CF_OFFICE"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_CF_OFFICEDOOR
			doorDetails.vCoords = <<-70.5223, 6254.5840, 31.2331 >>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
		BREAK
//		eAutoDoor = AUTODOOR_MIL_BASE_BARRIER_IN
//	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_MIL_BASE_BARRIER_IN")
//	g_sAutoDoorData[eAutoDoor].coords = 						<<-1588.27, 2794.21, 16.85>>
//	g_sAutoDoorData[eAutoDoor].model = 							prop_sec_barrier_ld_01a
//	g_sAutoDoorData[eAutoDoor].checkRange =						18*18
//	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
//	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			6.75
//	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<-1585.348633,2796.764648,15.333453>>
//	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-1573.873657,2783.339111,20.003242>>
//	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
//	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
//	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
//	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
//	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
//	
//	eAutoDoor = AUTODOOR_MIL_BASE_BARRIER_OUT
//	g_sAutoDoorData[eAutoDoor].doorID =							HASH("AUTODOOR_MIL_BASE_BARRIER_OUT")
//	g_sAutoDoorData[eAutoDoor].coords = 						<<-1589.58, 2793.67, 16.86>>
//	g_sAutoDoorData[eAutoDoor].model = 							prop_sec_barrier_ld_01a
//	g_sAutoDoorData[eAutoDoor].checkRange =						18*18
//	g_sAutoDoorData[eAutoDoor].blockedRange =					3*3
//	g_sAutoDoorData[eAutoDoor].activationLocate1Width =			6.75
//	g_sAutoDoorData[eAutoDoor].activationLocate1PosA = 			<<-1592.551758,2791.039795,15.326007>>
//	g_sAutoDoorData[eAutoDoor].activationLocate1PosB =			<<-1604.540039,2805.557617,20.174604>>
//	g_sAutoDoorData[eAutoDoor].openRate = 						0.55
//	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_TWO_LOCATES)
//	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_REVERSE_OPEN_RATIO)
//	SET_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,			BIT_AUTODOOR_DOOR_FULLY_CLOSED)
//	CLEAR_BIT(g_sAutoDoorData[eAutoDoor].settingsBitset,		BIT_AUTODOOR_LINKED_DOORS)
//		
		
		CASE MP_DN_MBASE_IN_BARRIER
			doorDetails.iDoorHash = MP_DOOR_MBASE_IN_BARRIER
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_MBASE_IN_BARRIER"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_sec_barrier_ld_01a
			doorDetails.vCoords = <<-1588.27, 2794.21, 16.85>>
			//doorDetails.bUseStateSystem = TRUE
			//doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bJustAdd = TRUE
			//doorDetails.fAutoOpenRange = 5.0
			//doorDetails.fAutoOpenRate = 0.1
			//doorDetails.bHoldOpen = TRUE
			//doorDetails.fRatio = 1.0
		BREAK
		
		CASE MP_DN_MBASE_OUT_BARRIER
			doorDetails.iDoorHash = MP_DOOR_MBASE_OUT_BARRIER
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_MBASE_OUT_BARRIER"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_sec_barrier_ld_01a
			doorDetails.vCoords = <<-1589.58, 2793.67, 16.86>>
			//doorDetails.bUseStateSystem = TRUE
			//doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bJustAdd = TRUE
			//doorDetails.fAutoOpenRange = 5.0

		BREAK
		CASE MP_DN_MBASE_W_IN_BARRIER
			doorDetails.iDoorHash = MP_DOOR_MBASE_W_IN_BARRIER
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_MBASE_W_IN_BARRIER"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_sec_barrier_ld_01a
			doorDetails.vCoords = <<-2309.1069, 3382.550, 29.9703>>
			//doorDetails.bUseStateSystem = TRUE
			//doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bJustAdd = TRUE
			//doorDetails.fAutoOpenRange = 5.0
			//doorDetails.fAutoOpenRate = 0.1
			//doorDetails.bHoldOpen = TRUE
			//doorDetails.fRatio = 1.0
		BREAK
		
		CASE MP_DN_MBASE_W_OUT_BARRIER
			doorDetails.iDoorHash = MP_DOOR_MBASE_W_OUT_BARRIER
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_MBASE_W_OUT_BARRIER"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = prop_sec_barrier_ld_01a
			doorDetails.vCoords = <<-2299.5391, 3384.9495,31.0658>>
			//doorDetails.bUseStateSystem = TRUE
			//doorDetails.theDoorDefaultState = DOORSTATE_UNLOCKED
			doorDetails.bJustAdd = TRUE
			//doorDetails.fAutoOpenRange = 5.0

		BREAK
		
		CASE MP_DN_ROCKFORD_VAULT_2
			doorDetails.iDoorHash = MP_DOOR_ROCKFORD_VAULT_2
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_ROCKFORD_VAULT_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_gb_vaubar
			doorDetails.vCoords = <<-1207.328,-335.129,38.079>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_GSENORA_VAULT_2
			doorDetails.iDoorHash = MP_DOOR_GSENORA_VAULT_2
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_GSENORA_VAULT_2"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_gb_vaubar 
			doorDetails.vCoords = <<1175.542,2710.861,38.227>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_JEWELSTORE_OFF
			doorDetails.iDoorHash = MP_DOOR_JEWELSTORE_OFF
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_JEWELSTORE_OFF"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = v_ilev_j2_door
			doorDetails.vCoords = <<-629.134,-230.152,38.207>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = FALSE
		BREAK
		
		CASE MP_DN_FIB_TURNSTILE_1_L
			doorDetails.iDoorHash = MP_DOOR_FIB_TURNSTILE_1_L
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_FIB_TURNSTILE_1_L"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_FBISECGATE
			doorDetails.vCoords = <<116.2367, -754.6212, 44.8285>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_FIB_TURNSTILE_1_R
			doorDetails.iDoorHash = MP_DOOR_FIB_TURNSTILE_1_R
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_FIB_TURNSTILE_1_R"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_FBISECGATE
			doorDetails.vCoords = <<115.9029, -755.5384, 44.8285>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_FIB_TURNSTILE_2_L
			doorDetails.iDoorHash = MP_DOOR_FIB_TURNSTILE_2_L
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_FIB_TURNSTILE_2_L"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_FBISECGATE
			doorDetails.vCoords = <<115.8078, -755.7997, 44.8285>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_FIB_TURNSTILE_2_R
			doorDetails.iDoorHash = MP_DOOR_FIB_TURNSTILE_2_R
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_FIB_TURNSTILE_2_R"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_FBISECGATE
			doorDetails.vCoords = <<115.4740, -756.7169, 44.8285>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_FIB_TURNSTILE_3_L
			doorDetails.iDoorHash = MP_DOOR_FIB_TURNSTILE_3_L
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_FIB_TURNSTILE_3_L"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_FBISECGATE
			doorDetails.vCoords = <<115.3789, -756.9781, 44.8285>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
		
		CASE MP_DN_FIB_TURNSTILE_3_R
			doorDetails.iDoorHash = MP_DOOR_FIB_TURNSTILE_3_R
			#IF IS_DEBUG_BUILD
				doorDetails.sDoorName = "MP_DOOR_FIB_TURNSTILE_3_R"
			#ENDIF
			doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			doorDetails.doorModel = V_ILEV_FBISECGATE
			doorDetails.vCoords = <<115.0451, -757.8953, 44.8285>>
			doorDetails.bUseStateSystem = TRUE
			doorDetails.theDoorDefaultState = DOORSTATE_LOCKED
			doorDetails.bPermanentState = TRUE
		BREAK
	ENDSWITCH
	RETURN doorDetails
ENDFUNC

FUNC BOOL CHECK_ALL_PREVIOUS_NET_DOORS_REMOVED(INT& iSlowLoopDoorIndex, INT iGameMode)
	MP_DOOR_DETAILS doorDetails
	//REPEAT ENUM_TO_INT(DUMMY_MP_DOOR_NAME)  i
	INT i
	
	REPEAT NUM_DOORS_CHECKED_PER_FRAME_ON_INIT i
		IF iSlowLoopDoorIndex < ENUM_TO_INT(DUMMY_MP_DOOR_NAME)
			doorDetails = GET_MP_DOOR_DETAILS_FROM_ENUM(iSlowLoopDoorIndex,iGameMode)
			IF doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			OR doorDetails.iGameMode = iGameMode
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(doorDetails.iDoorHash)
					#IF IS_DEBUG_BUILD
						IF g_DB_AllowGarageOutput
							NET_PRINT("WAITING FOR ")
							NET_PRINT(doorDetails.sDoorName)
							NET_PRINT(" to be removed") NET_NL()
							NET_PRINT("Forcable removing the door! ")
						ENDIF
					#ENDIF
					REMOVE_DOOR_FROM_SYSTEM(doorDetails.iDoorHash)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
		iSlowLoopDoorIndex++
	ENDREPEAT
	//ENDREPEAT
	
	IF iSlowLoopDoorIndex < ENUM_TO_INT(DUMMY_MP_DOOR_NAME)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_ALL_NET_DOORS_BEEN_REGISTERED(INT& iSlowLoopDoorIndex, INT iGameMode)

	MP_DOOR_DETAILS doorDetails
	INT i
	
	REPEAT NUM_DOORS_CHECKED_PER_FRAME_ON_INIT i
		IF iSlowLoopDoorIndex < ENUM_TO_INT(DUMMY_MP_DOOR_NAME)
			doorDetails = GET_MP_DOOR_DETAILS_FROM_ENUM( iSlowLoopDoorIndex,iGameMode)
			IF doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			OR doorDetails.iGameMode = iGameMode
				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(doorDetails.iDoorHash)
					#IF IS_DEBUG_BUILD
						IF g_DB_AllowGarageOutput
							NET_PRINT("WAITING FOR ")
							NET_PRINT(doorDetails.sDoorName)
							NET_PRINT(" to be registered") NET_NL()
						ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
		iSlowLoopDoorIndex++
	ENDREPEAT
	IF iSlowLoopDoorIndex < ENUM_TO_INT(DUMMY_MP_DOOR_NAME)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC SETUP_MP_DOOR(MP_DOOR_DETAILS doorDetails, BOOL bNetwork = FALSE)
	IF doorDetails.iDoorHash != 0
		NET_PRINT("Registering ")
		PRINTLN(doorDetails.sDoorName)
		PRINTLN("Door Hash = ",doorDetails.iDoorHash )
		PRINTLN("bNetwork = ", bNetwork)
		PRINTLN("vCoords = ", doorDetails.vCoords)
		ADD_DOOR_TO_SYSTEM(doorDetails.iDoorHash,doorDetails.doorModel,doorDetails.vCoords,FALSE, bNetwork ,doorDetails.bPermanentState)
		PRINTLN("Setting permanent state = ", doorDetails.bPermanentState)
		
		IF doorDetails.bHoldOpen
			DOOR_SYSTEM_SET_HOLD_OPEN(doorDetails.iDoorHash,TRUE)
			NET_PRINT("Set to hold open ")
			NET_PRINT(doorDetails.sDoorName) NET_NL()
		ENDIF
		
		IF doorDetails.fRatio != 0
			DOOR_SYSTEM_SET_OPEN_RATIO(doorDetails.iDoorHash,doorDetails.fRatio,FALSE, bNetwork )
			PRINTLN(doorDetails.sDoorName, " doorDetails.fRatio = ", doorDetails.fRatio)
		ENDIF

		IF NOT doorDetails.bJustAdd
			IF doorDetails.bUseStateSystem
				DOOR_SYSTEM_SET_DOOR_STATE(doorDetails.iDoorHash,doorDetails.theDoorDefaultState, bNetwork ,FALSE)
				NET_PRINT("Set to default state ")
				NET_PRINT_INT(ENUM_TO_INT(doorDetails.theDoorDefaultState)) NET_NL()
			ENDIF
		ENDIF
		IF doorDetails.bRaceDoor
			DOOR_SYSTEM_SET_DOOR_OPEN_FOR_RACES(doorDetails.iDoorHash,TRUE)
			NET_PRINT("Set door as one that should open for races ")
			NET_PRINT(doorDetails.sDoorName) NET_NL()
		ENDIF
		IF doorDetails.fAutoOpenRate != 0
			DOOR_SYSTEM_SET_AUTOMATIC_RATE(doorDetails.iDoorHash, doorDetails.fAutoOpenRate, bNetwork)
			PRINTLN(doorDetails.sDoorName, " doorDetails.fAutoOpenRate = ", doorDetails.fAutoOpenRate)
		ENDIF

		IF doorDetails.fAutoOpenRange != 0
			DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE(doorDetails.iDoorHash, doorDetails.fAutoOpenRange, bNetwork)
			PRINTLN(doorDetails.sDoorName, " doorDetails.fAutoOpenRange = ",doorDetails.fAutoOpenRange)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL INIT_NET_DOORS(INT& iSlowLoopDoorIndex, INT iGameMode)

	MP_DOOR_DETAILS doorDetails
	INT i
	
	REPEAT NUM_DOORS_CHECKED_PER_FRAME_ON_INIT i
		IF iSlowLoopDoorIndex < ENUM_TO_INT(DUMMY_MP_DOOR_NAME)
			doorDetails = GET_MP_DOOR_DETAILS_FROM_ENUM(iSlowLoopDoorIndex,iGameMode)
			IF doorDetails.iGameMode = FAKE_GAME_MODE_ALL
			OR doorDetails.iGameMode = iGameMode
				SETUP_MP_DOOR(doorDetails)
			ELSE
				NET_PRINT_STRING_INT("Not registering door for i = ", iSlowLoopDoorIndex) NET_NL()
			ENDIF
			iSlowLoopDoorIndex++
		ENDIF
	ENDREPEAT
	
	IF iSlowLoopDoorIndex < ENUM_TO_INT(DUMMY_MP_DOOR_NAME)
		RETURN FALSE
	ENDIF
	PRINTLN("SET_INITIAL_GLOBAL_DOOR_STATES - DONE from ", GET_THIS_SCRIPT_NAME())
	RETURN TRUE
ENDFUNC

PROC REMOVE_ALL_NET_DOORS(INT iGameMode)
	MP_DOOR_DETAILS doorDetails
	INT i
	REPEAT ENUM_TO_INT(DUMMY_MP_DOOR_NAME)  i
		doorDetails = GET_MP_DOOR_DETAILS_FROM_ENUM(i,iGameMode)
		IF doorDetails.iGameMode = FAKE_GAME_MODE_ALL
		OR doorDetails.iGameMode = iGameMode
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(doorDetails.iDoorHash)
				#IF IS_DEBUG_BUILD
					IF g_DB_AllowGarageOutput
						NET_PRINT("Removing ")
						NET_PRINT(doorDetails.sDoorName) NET_NL()
					ENDIF
				#ENDIF
				REMOVE_DOOR_FROM_SYSTEM(doorDetails.iDoorHash)
			ENDIF
		ENDIF
	ENDREPEAT
	NET_PRINT("REMOVE_ALL_NET_DOORS - DONE") NET_NL()
ENDPROC

PROC REMOVE_ALL_PERMANENT_DOORS(INT iGameMode)
	MP_DOOR_DETAILS doorDetails
	INT i
	REPEAT ENUM_TO_INT(DUMMY_MP_DOOR_NAME)  i
		doorDetails = GET_MP_DOOR_DETAILS_FROM_ENUM(i,iGameMode)
		IF doorDetails.iGameMode = FAKE_GAME_MODE_ALL
		OR doorDetails.iGameMode = iGameMode
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(doorDetails.iDoorHash)
				IF doorDetails.bPermanentState
					#IF IS_DEBUG_BUILD
						IF g_DB_AllowGarageOutput
							NET_PRINT("Removing ")
							NET_PRINT(doorDetails.sDoorName) NET_NL()
						ENDIF
					#ENDIF
					REMOVE_DOOR_FROM_SYSTEM(doorDetails.iDoorHash)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	NET_PRINT("REMOVE_ALL_PERMANENT_DOORS - DONE") NET_NL()
ENDPROC

PROC MAINTAIN_AM_DOORS_LAUNCHING()
	IF NOT MPGlobalsAmbience.bInitAMDoorsLaunch
		MP_MISSION_DATA amDoorsToLaunch
		IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_DOORS",MPGlobalsAmbience.iAMDoorsLastInstance,TRUE)
		//IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_DOORS")) = 0
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			OR NOT NETWORK_IS_IN_TUTORIAL_SESSION()
				amDoorsToLaunch.mdID.idMission = eAM_DOORS
				amDoorsToLaunch.iInstanceId = -1
				NET_PRINT("Relaunching AM_DOORS NOT NETWORK_IS_IN_TUTORIAL_SESSION") NET_NL()
				IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(amDoorsToLaunch)
					NET_PRINT("Relaunched AM_DOORS not in tutorial") NET_NL()
					NET_PRINT_STRING_INT("Relaunched AM_DOORS not in tutorial instance is: ", MPGlobalsAmbience.iAMDoorsLastInstance) NET_NL()
					MPGlobalsAmbience.iAMDoorsLastInstance = amDoorsToLaunch.iInstanceId 
					MPGlobalsAmbience.bInitAMDoorsLaunch = TRUE
				ENDIF
			ELSE
				IF NETWORK_GET_PLAYER_TUTORIAL_SESSION_INSTANCE(PLAYER_ID()) = 32
					amDoorsToLaunch.mdID.idMission = eAM_DOORS
					amDoorsToLaunch.iInstanceId = AM_DOORS_RESERVED_INSTANCES + NATIVE_TO_INT(PLAYER_ID())
					NET_PRINT("Relaunching AM_DOORS in tutorial sesson = 32") NET_NL()
					IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(amDoorsToLaunch)
						NET_PRINT("Relaunched AM_DOORS in tutorial sesson = 32") NET_NL()
						NET_PRINT_STRING_INT("Relaunched AM_DOORS in tutorial sesson = 32 instance is: ", MPGlobalsAmbience.iAMDoorsLastInstance) NET_NL()
						MPGlobalsAmbience.iAMDoorsLastInstance = amDoorsToLaunch.iInstanceId 
						MPGlobalsAmbience.bInitAMDoorsLaunch = TRUE
					ENDIF
				ELSE		
					amDoorsToLaunch.mdID.idMission = eAM_DOORS
					amDoorsToLaunch.iInstanceId = NETWORK_GET_PLAYER_TUTORIAL_SESSION_INSTANCE(PLAYER_ID())
					NET_PRINT("Relaunching AM_DOORS in tutorial session != 32") NET_NL()
					IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(amDoorsToLaunch)
						NET_PRINT("Relaunche AM_DOORS in tutorial session != 32") NET_NL()
						NET_PRINT_STRING_INT("Relaunched  AM_DOORS in tutorial session != 32 instance is: ", MPGlobalsAmbience.iAMDoorsLastInstance) NET_NL()
						MPGlobalsAmbience.iAMDoorsLastInstance = amDoorsToLaunch.iInstanceId 
						MPGlobalsAmbience.bInitAMDoorsLaunch = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC GET_MP_DOOR_ONE_WAY_DETAILS(INT iIndex, MP_DOOR_ONE_WAY_DETAILS &Details)
	SWITCH iIndex
		CASE AIRPORT_LEFT_SIDE_GATES
			Details.iDoorHash[0] = MP_DOOR_AIRPORT_L_1
			Details.iDoorHash[1] = MP_DOOR_AIRPORT_L_2
			Details.vArea1 = <<-984.040649,-2833.844971,18.297964>>
			Details.vArea2 = <<-990.199463,-2844.138428,10.964787>> 
			Details.fWidth = 15.250000
			Details.vInArea1 = <<-983.708191,-2833.199463,12.839787>>
			Details.vInArea2 = <<-977.912964,-2823.237305,19.230995>> 
			Details.fInWidth = 15.250000
		BREAK
		
		CASE AIRPORT_RIGHT_SIDE_GATES
			Details.iDoorHash[0] = MP_DOOR_AIRPORT_R_1
			Details.iDoorHash[1] = MP_DOOR_AIRPORT_R_2
			Details.vArea1 = <<-1144.961548,-2726.897949,18.298054>>
			Details.vArea2 = <<-1151.851563,-2739.108887,10.952897>> 
			Details.fWidth = 15.250000
			Details.vInArea1 = <<-1144.677612,-2726.473145,12.868383>>
			Details.vInArea2 = <<-1138.071289,-2714.977783,18.428009>> 
			Details.fInWidth = 15.250000
		BREAK
		
		CASE CAR_IMPOUND_GATE_1
			Details.iDoorHash[0] = MP_DOOR_IMPOUND_GATE_1
			Details.vArea1 = <<415.243561,-1621.925781,28.042774>>
			Details.vArea2 = <<410.353729,-1628.049438,34.042778>> 
			Details.fWidth = 6.500000
		BREAK
		CASE CAR_IMPOUND_GATE_2
			Details.iDoorHash[0] = MP_DOOR_IMPOUND_GATE_2
			Details.vArea1 = <<416.416992,-1653.291016,28.042776>>
			Details.vArea2 = <<410.672577,-1648.681519,34.042778>> 
			Details.fWidth = 6.500000
		BREAK
		CASE CAR_IMPOUND_GATE_3
			Details.iDoorHash[0] = MP_DOOR_IMPOUND_GATE_3
			Details.vArea1 = <<400.634003,-1609.808105,28.042776>>
			Details.vArea2 = <<394.490723,-1617.194336,34.042778>> 
			Details.fWidth = 8.250000
		BREAK
		
		CASE AIRPORT_FAR_RIGHT_SIDE_GATES
			Details.iDoorHash[0] = MP_DOOR_AIRPORT_GATE_CW_33
			Details.iDoorHash[1] = MP_DOOR_AIRPORT_GATE_CW_34
			Details.vArea1 = <<-1012.158936,-2412.959473,12.694675>>
			Details.vArea2 = <<-1026.245850,-2405.063477,18.757206>>
			Details.fWidth = 15.375000
			Details.vInArea1 = <<-1012.158936,-2412.959473,12.694675>>
			Details.vInArea2 = <<-999.975952,-2419.571045,18.757151>>
			Details.fInWidth = 15.375000
		BREAK
	ENDSWITCH
ENDPROC


FUNC INT GET_ONE_WAY_DOOR_ID(INT iDoorHash)
	SWITCH iDoorHash
		CASE MP_DOOR_AIRPORT_L_1
		CASE MP_DOOR_AIRPORT_L_2
			RETURN AIRPORT_LEFT_SIDE_GATES
		BREAK
		CASE MP_DOOR_AIRPORT_R_1
		CASE MP_DOOR_AIRPORT_R_2
			RETURN AIRPORT_RIGHT_SIDE_GATES
		BREAK
		CASE MP_DOOR_IMPOUND_GATE_1
			RETURN CAR_IMPOUND_GATE_1
		BREAK
		CASE MP_DOOR_IMPOUND_GATE_2
			RETURN CAR_IMPOUND_GATE_2
		BREAK
		CASE MP_DOOR_IMPOUND_GATE_3
			RETURN CAR_IMPOUND_GATE_3
		BREAK
		CASE MP_DOOR_AIRPORT_GATE_CW_33
		CASE MP_DOOR_AIRPORT_GATE_CW_34
			RETURN AIRPORT_FAR_RIGHT_SIDE_GATES
		BREAK
	ENDSWITCH
	RETURN -1
ENDFUNC

/// PURPOSE:
///    To override the normal door controls of doors managed bt AM_DOORS for missions.
/// PARAMS:
///    iDoorHash - hash of the door to override
///    overrideState - state to override the door to
PROC MISSION_CONTROL_NET_DOOR_OVERRIDE(INT iDoorHash, MISSION_CONTROL_NET_DOOR_OVERRIDE_ENUM overrideState)
	IF GET_ONE_WAY_DOOR_ID(iDoorHash) >= 0
		SWITCH overrideState
			CASE MC_DOOR_OVERRIDE_UNLOCKED
				SET_BIT(g_MissionNetDoorOverride.iOneWayDoorOpenRequests,GET_ONE_WAY_DOOR_ID(iDoorHash))
			BREAK
			
			CASE MC_DOOR_OVERRIDE_LOCKED
				SET_BIT(g_MissionNetDoorOverride.iOneWayDoorClosedRequests,GET_ONE_WAY_DOOR_ID(iDoorHash))
			BREAK
			
			CASE MC_DOOR_OVERRIDE_FORCED_OPEN
				SET_BIT(g_MissionNetDoorOverride.iOneWayForcedOpenRequests,GET_ONE_WAY_DOOR_ID(iDoorHash))
			BREAK
		ENDSWITCH
	ENDIF
	PRINTLN("MISSION_CONTROL_NET_DOOR_OVERRIDE: setting one way door: ",GET_ONE_WAY_DOOR_ID(iDoorHash)," to be in state: ",ENUM_TO_INT(overrideState))
ENDPROC

/// PURPOSE:
///    Clears individual override set with the command MISSION_CONTROL_NET_DOOR_OVERRIDE
/// PARAMS:
///    iDoorHash - hash of door to clear override for
PROC CLEAR_MISSION_CONTROL_NET_DOOR_OVERRIDE(INT iDoorHash)
	IF GET_ONE_WAY_DOOR_ID(iDoorHash) >= 0
		CLEAR_BIT(g_MissionNetDoorOverride.iOneWayDoorOpenRequests,GET_ONE_WAY_DOOR_ID(iDoorHash))
		CLEAR_BIT(g_MissionNetDoorOverride.iOneWayDoorClosedRequests,GET_ONE_WAY_DOOR_ID(iDoorHash))
		CLEAR_BIT(g_MissionNetDoorOverride.iOneWayForcedOpenRequests,GET_ONE_WAY_DOOR_ID(iDoorHash))
	ENDIF
	PRINTLN("MISSION_CONTROL_NET_DOOR_OVERRIDE: Clearing override of one way door: ",GET_ONE_WAY_DOOR_ID(iDoorHash))
ENDPROC

/// PURPOSE:
///    Clears all overrides set with the command MISSION_CONTROL_NET_DOOR_OVERRIDE
PROC CLEAR_ALL_MISSION_CONTROL_NET_DOOR_OVERRIDE()
	g_MissionNetDoorOverride.iOneWayDoorOpenRequests = 0
	g_MissionNetDoorOverride.iOneWayDoorClosedRequests = 0
	g_MissionNetDoorOverride.iOneWayForcedOpenRequests = 0
	PRINTLN("MISSION_CONTROL_NET_DOOR_OVERRIDE: Clearing ALL overrides")
ENDPROC

PROC UNLOCK_AIRPORT_GATES()
	g_bUnlockAirportGates = TRUE
	
	IF NOT IS_BIT_SET(g_MissionNetDoorOverride.iOneWayDoorOpenRequests, AIRPORT_LEFT_SIDE_GATES)
		SET_BIT(g_MissionNetDoorOverride.iOneWayDoorOpenRequests, AIRPORT_LEFT_SIDE_GATES)
		SET_BIT(g_iUnlockAirportGateOverrideOneWayDoors,AIRPORT_LEFT_SIDE_GATES)
	ENDIF
	IF NOT IS_BIT_SET(g_MissionNetDoorOverride.iOneWayDoorOpenRequests, AIRPORT_RIGHT_SIDE_GATES)
		SET_BIT(g_MissionNetDoorOverride.iOneWayDoorOpenRequests, AIRPORT_RIGHT_SIDE_GATES)
		SET_BIT(g_iUnlockAirportGateOverrideOneWayDoors,AIRPORT_RIGHT_SIDE_GATES)
	ENDIF
	IF NOT IS_BIT_SET(g_MissionNetDoorOverride.iOneWayDoorOpenRequests, AIRPORT_FAR_RIGHT_SIDE_GATES)
		SET_BIT(g_MissionNetDoorOverride.iOneWayDoorOpenRequests, AIRPORT_FAR_RIGHT_SIDE_GATES)
		SET_BIT(g_iUnlockAirportGateOverrideOneWayDoors,AIRPORT_FAR_RIGHT_SIDE_GATES)
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_AIRPORT_GATE_OTHER)
		DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_AIRPORT_GATE_OTHER,DOORSTATE_UNLOCKED, FALSE ,FALSE)
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_AIRPORT_GATE_CW_31)
		DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_AIRPORT_GATE_CW_31,DOORSTATE_UNLOCKED, FALSE ,FALSE)
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_AIRPORT_GATE_CW_32)
		DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_AIRPORT_GATE_CW_32,DOORSTATE_UNLOCKED, FALSE ,FALSE)
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_AIRPORT_GATE_CW_9)
		DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_AIRPORT_GATE_CW_9,DOORSTATE_UNLOCKED, FALSE ,FALSE)
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_AIRPORT_GATE_CW_10)
		DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_AIRPORT_GATE_CW_10,DOORSTATE_UNLOCKED, FALSE ,FALSE)
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_AIRPORT_GATE_CW_11)
		DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_AIRPORT_GATE_CW_11,DOORSTATE_UNLOCKED, FALSE ,FALSE)
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_AIRPORT_GATE_CW_12)
		DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_AIRPORT_GATE_CW_12,DOORSTATE_UNLOCKED, FALSE ,FALSE)
	ENDIF
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	PRINTLN("UNLOCK_AIRPORT_GATES- Called this frame")
ENDPROC

PROC RESET_UNLOCK_AIRPORT_GATES()
	IF g_bUnlockAirportGates
		IF IS_BIT_SET(g_iUnlockAirportGateOverrideOneWayDoors,AIRPORT_LEFT_SIDE_GATES)
			CLEAR_BIT(g_MissionNetDoorOverride.iOneWayDoorOpenRequests, AIRPORT_LEFT_SIDE_GATES)
		ENDIF
		IF IS_BIT_SET(g_iUnlockAirportGateOverrideOneWayDoors,AIRPORT_RIGHT_SIDE_GATES)
			CLEAR_BIT(g_MissionNetDoorOverride.iOneWayDoorOpenRequests, AIRPORT_RIGHT_SIDE_GATES)
		ENDIF
		IF IS_BIT_SET(g_iUnlockAirportGateOverrideOneWayDoors,AIRPORT_FAR_RIGHT_SIDE_GATES)
			CLEAR_BIT(g_MissionNetDoorOverride.iOneWayDoorOpenRequests, AIRPORT_FAR_RIGHT_SIDE_GATES)
		ENDIF
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_AIRPORT_GATE_OTHER)
			DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_AIRPORT_GATE_OTHER,DOORSTATE_LOCKED, FALSE ,FALSE)
		ENDIF
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_AIRPORT_GATE_CW_31)
			DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_AIRPORT_GATE_CW_31,DOORSTATE_LOCKED, FALSE ,FALSE)
		ENDIF
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_AIRPORT_GATE_CW_32)
			DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_AIRPORT_GATE_CW_32,DOORSTATE_LOCKED, FALSE ,FALSE)
		ENDIF
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_AIRPORT_GATE_CW_9)
			DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_AIRPORT_GATE_CW_9,DOORSTATE_LOCKED, FALSE ,FALSE)
		ENDIF
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_AIRPORT_GATE_CW_10)
			DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_AIRPORT_GATE_CW_10,DOORSTATE_LOCKED, FALSE ,FALSE)
		ENDIF
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_AIRPORT_GATE_CW_11)
			DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_AIRPORT_GATE_CW_11,DOORSTATE_LOCKED, FALSE ,FALSE)
		ENDIF
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_AIRPORT_GATE_CW_12)
			DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_AIRPORT_GATE_CW_12,DOORSTATE_LOCKED, FALSE ,FALSE)
		ENDIF
		g_iUnlockAirportGateOverrideOneWayDoors = 0
	ENDIF
	g_bUnlockAirportGates = FALSE
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	PRINTLN("RESET_UNLOCK_AIRPORT_GATES- Called this frame")
ENDPROC
