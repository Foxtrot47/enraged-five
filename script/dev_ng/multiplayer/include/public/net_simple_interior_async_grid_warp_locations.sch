//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_simple_interior_async_grid_warp_locations.sch											//
// Description: Location details for async grid warp.														//
// Written by:  Tymon																						//
// Date:  		5/08/2016																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "mp_globals_new_features_tu.sch"
USING "MP_globals_simple_interior_consts.sch"
USING "net_simple_interior_base.sch"


FUNC ASYNC_GRID_SPAWN_LOCATION_DETAILS GET_ASYNC_GRID_LOCATION_DETAILS(ASYNC_GRID_SPAWN_LOCATIONS location)
	ASYNC_GRID_SPAWN_LOCATION_DETAILS details
	SWITCH location
		#IF IS_DEBUG_BUILD
		CASE _DBG_GRID_SPAWN_LOCATION_CREATOR
			details = g_AsyncGridSpawnData.db_gridCreator
		BREAK
		#ENDIF
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_METH_1
			details.vStartCoord = <<51.7513, 6339.8462, 31.2258>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 1.8100
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.7100
			details.fGridHeading = 29.1600
			details.iGridWidth = 13
			details.iGridHeight = 9
			details.iExcludedPointsBS[0] = 0
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			SET_BIT(details.iExcludedPointsBS[3], 11)
			SET_BIT(details.iExcludedPointsBS[3], 13)
			SET_BIT(details.iExcludedPointsBS[3], 15)
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_WEED_1
			details.vStartCoord = <<418.4608, 6520.7202, 27.7201>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 1.8100
			details.fOffsetZ = 0.1300
			details.fProbeRadius = 0.8000
			details.fGridHeading = 263.5200
			details.iGridWidth = 15
			details.iGridHeight = 7
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 13)
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			SET_BIT(details.iExcludedPointsBS[2], 11)
			SET_BIT(details.iExcludedPointsBS[2], 13)
			SET_BIT(details.iExcludedPointsBS[2], 14)
			SET_BIT(details.iExcludedPointsBS[2], 15)
			SET_BIT(details.iExcludedPointsBS[2], 16)
			SET_BIT(details.iExcludedPointsBS[2], 26)
			SET_BIT(details.iExcludedPointsBS[2], 27)
			SET_BIT(details.iExcludedPointsBS[2], 28)
			SET_BIT(details.iExcludedPointsBS[2], 30)
			SET_BIT(details.iExcludedPointsBS[2], 31)
			details.iExcludedPointsBS[3] = 0
			SET_BIT(details.iExcludedPointsBS[3], 1)
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_CRACK_1
			details.vStartCoord = <<52.7514, 6487.1538, 31.4267>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 1.4200
			details.fOffsetZ = 0.1300
			details.fProbeRadius = 0.8000
			details.fGridHeading = 314.9000
			details.iGridWidth = 31
			details.iGridHeight = 2
			details.iSpawnOriginPointID = 0
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 18)
			SET_BIT(details.iExcludedPointsBS[0], 20)
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			SET_BIT(details.iExcludedPointsBS[2], 23)
			SET_BIT(details.iExcludedPointsBS[2], 25)
			SET_BIT(details.iExcludedPointsBS[2], 27)
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_CASH_1
			details.vStartCoord = <<-408.6213, 6168.9912, 31.4782>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 1.580
			details.fOffsetZ = 0.1300
			details.fProbeRadius = 0.8000
			details.fGridHeading = 314.9000
			details.iGridWidth = 19
			details.iGridHeight = 5
			details.iSpawnOriginPointID = 8
			details.iExcludedPointsBS[0] = 0
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			SET_BIT(details.iExcludedPointsBS[2], 8)
			SET_BIT(details.iExcludedPointsBS[2], 10)
			SET_BIT(details.iExcludedPointsBS[2], 25)
			SET_BIT(details.iExcludedPointsBS[2], 27)
			SET_BIT(details.iExcludedPointsBS[2], 29)
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_FAKEID_1
			details.vStartCoord = <<-160.5925, 6333.7998, 31.5808>>
			details.fSpacingX = 1.4700
			details.fSpacingY = 16.0700
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.8000
			details.fGridHeading = 316.0800
			details.iGridWidth = 27
			details.iGridHeight = 2
			details.iSpawnOriginPointID = 4
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 22)
			SET_BIT(details.iExcludedPointsBS[0], 24)
			SET_BIT(details.iExcludedPointsBS[0], 26)
			details.iExcludedPointsBS[1] = 0
			SET_BIT(details.iExcludedPointsBS[1], 1)
			SET_BIT(details.iExcludedPointsBS[1], 3)
			SET_BIT(details.iExcludedPointsBS[1], 5)
			SET_BIT(details.iExcludedPointsBS[1], 7)
			SET_BIT(details.iExcludedPointsBS[1], 8)
			SET_BIT(details.iExcludedPointsBS[1], 9)
			SET_BIT(details.iExcludedPointsBS[1], 10)
			SET_BIT(details.iExcludedPointsBS[1], 11)
			SET_BIT(details.iExcludedPointsBS[1], 13)
			SET_BIT(details.iExcludedPointsBS[1], 15)
			SET_BIT(details.iExcludedPointsBS[1], 17)
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_METH_2
			details.vStartCoord = <<1443.5260, -1652.7200, 66.0608>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 1.8300
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.8000
			details.fGridHeading = 20.8800
			details.iGridWidth = 18
			details.iGridHeight = 6
			details.iSpawnOriginPointID = 11
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 0)
			SET_BIT(details.iExcludedPointsBS[0], 1)
			SET_BIT(details.iExcludedPointsBS[0], 14)
			SET_BIT(details.iExcludedPointsBS[0], 16)
			details.iExcludedPointsBS[1] = 0
			SET_BIT(details.iExcludedPointsBS[1], 2)
			SET_BIT(details.iExcludedPointsBS[1], 20)
			details.iExcludedPointsBS[2] = 0
			SET_BIT(details.iExcludedPointsBS[2], 4)
			SET_BIT(details.iExcludedPointsBS[2], 6)
			SET_BIT(details.iExcludedPointsBS[2], 15)
			SET_BIT(details.iExcludedPointsBS[2], 17)
			SET_BIT(details.iExcludedPointsBS[2], 19)
			SET_BIT(details.iExcludedPointsBS[2], 21)
			SET_BIT(details.iExcludedPointsBS[2], 23)
			SET_BIT(details.iExcludedPointsBS[2], 25)
			SET_BIT(details.iExcludedPointsBS[2], 31)
			details.iExcludedPointsBS[3] = 0
			SET_BIT(details.iExcludedPointsBS[3], 1)
			SET_BIT(details.iExcludedPointsBS[3], 3)
			SET_BIT(details.iExcludedPointsBS[3], 5)
			SET_BIT(details.iExcludedPointsBS[3], 7)
			SET_BIT(details.iExcludedPointsBS[3], 9)
			SET_BIT(details.iExcludedPointsBS[3], 11)
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_WEED_2
			details.vStartCoord = <<101.7318, 174.0827, 104.5913>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 1.8100
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 1.000
			details.fGridHeading = 160.9200
			details.iGridWidth = 19
			details.iGridHeight = 7
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 3)
			SET_BIT(details.iExcludedPointsBS[0], 4)
			SET_BIT(details.iExcludedPointsBS[0], 5)
			SET_BIT(details.iExcludedPointsBS[0], 6)
			SET_BIT(details.iExcludedPointsBS[0], 7)
			SET_BIT(details.iExcludedPointsBS[0], 8)
			SET_BIT(details.iExcludedPointsBS[0], 9)
			SET_BIT(details.iExcludedPointsBS[0], 10)
			SET_BIT(details.iExcludedPointsBS[0], 11)
			SET_BIT(details.iExcludedPointsBS[0], 12)
			SET_BIT(details.iExcludedPointsBS[0], 14)
			SET_BIT(details.iExcludedPointsBS[0], 16)
			SET_BIT(details.iExcludedPointsBS[0], 18)
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			SET_BIT(details.iExcludedPointsBS[2], 31)
			details.iExcludedPointsBS[3] = 0
			SET_BIT(details.iExcludedPointsBS[3], 0)
			SET_BIT(details.iExcludedPointsBS[3], 1)
			SET_BIT(details.iExcludedPointsBS[3], 3)
			SET_BIT(details.iExcludedPointsBS[3], 5)
			SET_BIT(details.iExcludedPointsBS[3], 7)
			SET_BIT(details.iExcludedPointsBS[3], 9)
			SET_BIT(details.iExcludedPointsBS[3], 15)
			SET_BIT(details.iExcludedPointsBS[3], 17)
			SET_BIT(details.iExcludedPointsBS[3], 19)
			details.iExcludedPointsBS[4] = 0
			SET_BIT(details.iExcludedPointsBS[4], 2)
			SET_BIT(details.iExcludedPointsBS[4], 4)
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_CRACK_2
			details.vStartCoord = <<-1461.5582, -382.7768, 38.7483>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 1.690
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.8000
			details.fGridHeading = 225.7200
			details.iGridWidth = 20
			details.iGridHeight = 5
			details.iSpawnOriginPointID = 0
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 12)
			SET_BIT(details.iExcludedPointsBS[0], 14)
			SET_BIT(details.iExcludedPointsBS[0], 16)
			SET_BIT(details.iExcludedPointsBS[0], 18)
			details.iExcludedPointsBS[1] = 0
			SET_BIT(details.iExcludedPointsBS[1], 2)
			SET_BIT(details.iExcludedPointsBS[1], 4)
			SET_BIT(details.iExcludedPointsBS[1], 6)
			SET_BIT(details.iExcludedPointsBS[1], 26)
			details.iExcludedPointsBS[2] = 0
			SET_BIT(details.iExcludedPointsBS[2], 11)
			SET_BIT(details.iExcludedPointsBS[2], 13)
			SET_BIT(details.iExcludedPointsBS[2], 15)
			SET_BIT(details.iExcludedPointsBS[2], 16)
			SET_BIT(details.iExcludedPointsBS[2], 17)
			SET_BIT(details.iExcludedPointsBS[2], 19)
			SET_BIT(details.iExcludedPointsBS[2], 21)
			SET_BIT(details.iExcludedPointsBS[2], 23)
			SET_BIT(details.iExcludedPointsBS[2], 25)
			SET_BIT(details.iExcludedPointsBS[2], 27)
			SET_BIT(details.iExcludedPointsBS[2], 29)
			SET_BIT(details.iExcludedPointsBS[2], 31)
			details.iExcludedPointsBS[3] = 0
			SET_BIT(details.iExcludedPointsBS[3], 1)
			SET_BIT(details.iExcludedPointsBS[3], 3)
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_CASH_2
			details.vStartCoord = <<-1176.4139, -1382.7950, 4.8572>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 1.8300
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.8000
			details.fGridHeading = 35.6400
			details.iGridWidth = 11
			details.iGridHeight = 8
			details.iSpawnOriginPointID = 7
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 9)
			SET_BIT(details.iExcludedPointsBS[0], 20)
			details.iExcludedPointsBS[1] = 0
			SET_BIT(details.iExcludedPointsBS[1], 30)
			details.iExcludedPointsBS[2] = 0
			SET_BIT(details.iExcludedPointsBS[2], 0)
			SET_BIT(details.iExcludedPointsBS[2], 2)
			SET_BIT(details.iExcludedPointsBS[2], 3)
			SET_BIT(details.iExcludedPointsBS[2], 5)
			SET_BIT(details.iExcludedPointsBS[2], 7)
			SET_BIT(details.iExcludedPointsBS[2], 9)
			SET_BIT(details.iExcludedPointsBS[2], 11)
			SET_BIT(details.iExcludedPointsBS[2], 12)
			SET_BIT(details.iExcludedPointsBS[2], 13)
			SET_BIT(details.iExcludedPointsBS[2], 14)
			SET_BIT(details.iExcludedPointsBS[2], 15)
			SET_BIT(details.iExcludedPointsBS[2], 16)
			SET_BIT(details.iExcludedPointsBS[2], 18)
			SET_BIT(details.iExcludedPointsBS[2], 20)
			SET_BIT(details.iExcludedPointsBS[2], 22)
			SET_BIT(details.iExcludedPointsBS[2], 23)
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_FAKEID_2
			details.vStartCoord = <<300.6130, -759.0207, 29.3106>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 2.4100
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.8000
			details.fGridHeading = 252.3600
			details.iGridWidth = 25
			details.iGridHeight = 2
			details.iSpawnOriginPointID = 0
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 2)
			SET_BIT(details.iExcludedPointsBS[0], 4)
			SET_BIT(details.iExcludedPointsBS[0], 5)
			SET_BIT(details.iExcludedPointsBS[0], 6)
			SET_BIT(details.iExcludedPointsBS[0], 7)
			SET_BIT(details.iExcludedPointsBS[0], 8)
			SET_BIT(details.iExcludedPointsBS[0], 9)
			SET_BIT(details.iExcludedPointsBS[0], 10)
			SET_BIT(details.iExcludedPointsBS[0], 11)
			SET_BIT(details.iExcludedPointsBS[0], 12)
			SET_BIT(details.iExcludedPointsBS[0], 13)
			SET_BIT(details.iExcludedPointsBS[0], 14)
			SET_BIT(details.iExcludedPointsBS[0], 15)
			SET_BIT(details.iExcludedPointsBS[0], 17)
			SET_BIT(details.iExcludedPointsBS[0], 28)
			SET_BIT(details.iExcludedPointsBS[0], 30)
			details.iExcludedPointsBS[1] = 0
			SET_BIT(details.iExcludedPointsBS[1], 0)
			SET_BIT(details.iExcludedPointsBS[1], 11)
			SET_BIT(details.iExcludedPointsBS[1], 13)
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_METH_3
			details.vStartCoord = <<202.4223, 2460.4331, 55.7011>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 1.8000
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.8000
			details.fGridHeading = 200.1600
			details.iGridWidth = 18
			details.iGridHeight = 6
			details.iSpawnOriginPointID = 0
			details.iExcludedPointsBS[0] = 0
			details.iExcludedPointsBS[1] = 0
			SET_BIT(details.iExcludedPointsBS[1], 3)
			SET_BIT(details.iExcludedPointsBS[1], 19)
			SET_BIT(details.iExcludedPointsBS[1], 21)
			SET_BIT(details.iExcludedPointsBS[1], 29)
			SET_BIT(details.iExcludedPointsBS[1], 31)
			details.iExcludedPointsBS[2] = 0
			SET_BIT(details.iExcludedPointsBS[2], 1)
			SET_BIT(details.iExcludedPointsBS[2], 3)
			SET_BIT(details.iExcludedPointsBS[2], 5)
			SET_BIT(details.iExcludedPointsBS[2], 7)
			SET_BIT(details.iExcludedPointsBS[2], 8)
			SET_BIT(details.iExcludedPointsBS[2], 9)
			SET_BIT(details.iExcludedPointsBS[2], 10)
			SET_BIT(details.iExcludedPointsBS[2], 11)
			SET_BIT(details.iExcludedPointsBS[2], 12)
			SET_BIT(details.iExcludedPointsBS[2], 13)
			SET_BIT(details.iExcludedPointsBS[2], 14)
			SET_BIT(details.iExcludedPointsBS[2], 15)
			SET_BIT(details.iExcludedPointsBS[2], 16)
			SET_BIT(details.iExcludedPointsBS[2], 17)
			SET_BIT(details.iExcludedPointsBS[2], 18)
			SET_BIT(details.iExcludedPointsBS[2], 19)
			SET_BIT(details.iExcludedPointsBS[2], 21)
			SET_BIT(details.iExcludedPointsBS[2], 23)
			SET_BIT(details.iExcludedPointsBS[2], 25)
			SET_BIT(details.iExcludedPointsBS[2], 26)
			SET_BIT(details.iExcludedPointsBS[2], 27)
			SET_BIT(details.iExcludedPointsBS[2], 28)
			SET_BIT(details.iExcludedPointsBS[2], 29)
			SET_BIT(details.iExcludedPointsBS[2], 30)
			SET_BIT(details.iExcludedPointsBS[2], 31)
			details.iExcludedPointsBS[3] = 0
			SET_BIT(details.iExcludedPointsBS[3], 0)
			SET_BIT(details.iExcludedPointsBS[3], 1)
			SET_BIT(details.iExcludedPointsBS[3], 2)
			SET_BIT(details.iExcludedPointsBS[3], 3)
			SET_BIT(details.iExcludedPointsBS[3], 4)
			SET_BIT(details.iExcludedPointsBS[3], 5)
			SET_BIT(details.iExcludedPointsBS[3], 6)
			SET_BIT(details.iExcludedPointsBS[3], 7)
			SET_BIT(details.iExcludedPointsBS[3], 8)
			SET_BIT(details.iExcludedPointsBS[3], 9)
			SET_BIT(details.iExcludedPointsBS[3], 11)
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_WEED_3
			details.vStartCoord = <<2846.7419, 4449.6250, 48.5186>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 1.5600
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.8000
			details.fGridHeading = 106.9200
			details.iGridWidth = 19
			details.iGridHeight = 5
			details.iSpawnOriginPointID = 0
			details.iExcludedPointsBS[0] = 0
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			SET_BIT(details.iExcludedPointsBS[2], 23)
			SET_BIT(details.iExcludedPointsBS[2], 25)
			SET_BIT(details.iExcludedPointsBS[2], 27)
			SET_BIT(details.iExcludedPointsBS[2], 29)
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_CRACK_3
			details.vStartCoord = <<387.8527, 3586.8630, 33.2922>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 1.5600
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.8000
			details.fGridHeading = 349.9100
			details.iGridWidth = 19
			details.iGridHeight = 5
			details.iSpawnOriginPointID = 0
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 17)
			details.iExcludedPointsBS[1] = 0
			SET_BIT(details.iExcludedPointsBS[1], 2)
			SET_BIT(details.iExcludedPointsBS[1], 4)
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_CASH_3
			details.vStartCoord = <<644.2907, 2783.8979, 41.9584>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 1.6200
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.8000
			details.fGridHeading = 183.9600
			details.iGridWidth = 13
			details.iGridHeight = 8
			details.iSpawnOriginPointID = 9
			details.iExcludedPointsBS[0] = 0
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			SET_BIT(details.iExcludedPointsBS[2], 30)
			details.iExcludedPointsBS[3] = 0
			SET_BIT(details.iExcludedPointsBS[3], 0)
			SET_BIT(details.iExcludedPointsBS[3], 2)
			SET_BIT(details.iExcludedPointsBS[3], 3)
			SET_BIT(details.iExcludedPointsBS[3], 4)
			SET_BIT(details.iExcludedPointsBS[3], 5)
			SET_BIT(details.iExcludedPointsBS[3], 6)
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_FAKEID_3
			details.vStartCoord = <<1658.2980, 4851.9312, 41.9609>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 2.8500
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.8000
			details.fGridHeading = 277.2000
			details.iGridWidth = 27
			details.iGridHeight = 2
			details.iSpawnOriginPointID = 0
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 24)
			SET_BIT(details.iExcludedPointsBS[0], 26)
			details.iExcludedPointsBS[1] = 0
			SET_BIT(details.iExcludedPointsBS[1], 4)
			SET_BIT(details.iExcludedPointsBS[1], 6)
			SET_BIT(details.iExcludedPointsBS[1], 8)
			SET_BIT(details.iExcludedPointsBS[1], 10)
			SET_BIT(details.iExcludedPointsBS[1], 12)
			SET_BIT(details.iExcludedPointsBS[1], 13)
			SET_BIT(details.iExcludedPointsBS[1], 14)
			SET_BIT(details.iExcludedPointsBS[1], 15)
			SET_BIT(details.iExcludedPointsBS[1], 16)
			SET_BIT(details.iExcludedPointsBS[1], 17)
			SET_BIT(details.iExcludedPointsBS[1], 18)
			SET_BIT(details.iExcludedPointsBS[1], 19)
			SET_BIT(details.iExcludedPointsBS[1], 20)
			SET_BIT(details.iExcludedPointsBS[1], 21)
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_METH_4
			details.vStartCoord = <<1180.1219, -3116.5891, 6.0280>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 1.4100
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.8000
			details.fGridHeading = 90.0000
			details.iGridWidth = 19
			details.iGridHeight = 5
			details.iSpawnOriginPointID = 3
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 4)
			SET_BIT(details.iExcludedPointsBS[0], 6)
			SET_BIT(details.iExcludedPointsBS[0], 8)
			SET_BIT(details.iExcludedPointsBS[0], 10)
			SET_BIT(details.iExcludedPointsBS[0], 12)
			SET_BIT(details.iExcludedPointsBS[0], 14)
			SET_BIT(details.iExcludedPointsBS[0], 25)
			SET_BIT(details.iExcludedPointsBS[0], 27)
			SET_BIT(details.iExcludedPointsBS[0], 29)
			SET_BIT(details.iExcludedPointsBS[0], 31)
			details.iExcludedPointsBS[1] = 0
			SET_BIT(details.iExcludedPointsBS[1], 1)
			SET_BIT(details.iExcludedPointsBS[1], 3)
			SET_BIT(details.iExcludedPointsBS[1], 12)
			SET_BIT(details.iExcludedPointsBS[1], 14)
			SET_BIT(details.iExcludedPointsBS[1], 16)
			SET_BIT(details.iExcludedPointsBS[1], 18)
			SET_BIT(details.iExcludedPointsBS[1], 20)
			details.iExcludedPointsBS[2] = 0
			SET_BIT(details.iExcludedPointsBS[2], 11)
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_WEED_4
			details.vStartCoord = <<137.9712, -2473.5381, 6.0000>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 1.6100
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.8000
			details.fGridHeading = 235.4400
			details.iGridWidth = 15
			details.iGridHeight = 9
			details.iSpawnOriginPointID = 0
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 6)
			SET_BIT(details.iExcludedPointsBS[0], 8)
			SET_BIT(details.iExcludedPointsBS[0], 10)
			SET_BIT(details.iExcludedPointsBS[0], 12)
			SET_BIT(details.iExcludedPointsBS[0], 14)
			SET_BIT(details.iExcludedPointsBS[0], 21)
			SET_BIT(details.iExcludedPointsBS[0], 23)
			SET_BIT(details.iExcludedPointsBS[0], 25)
			SET_BIT(details.iExcludedPointsBS[0], 27)
			SET_BIT(details.iExcludedPointsBS[0], 28)
			SET_BIT(details.iExcludedPointsBS[0], 29)
			details.iExcludedPointsBS[1] = 0
			SET_BIT(details.iExcludedPointsBS[1], 2)
			SET_BIT(details.iExcludedPointsBS[1], 4)
			SET_BIT(details.iExcludedPointsBS[1], 6)
			SET_BIT(details.iExcludedPointsBS[1], 7)
			SET_BIT(details.iExcludedPointsBS[1], 8)
			SET_BIT(details.iExcludedPointsBS[1], 9)
			SET_BIT(details.iExcludedPointsBS[1], 10)
			SET_BIT(details.iExcludedPointsBS[1], 11)
			SET_BIT(details.iExcludedPointsBS[1], 12)
			SET_BIT(details.iExcludedPointsBS[1], 17)
			SET_BIT(details.iExcludedPointsBS[1], 19)
			SET_BIT(details.iExcludedPointsBS[1], 21)
			SET_BIT(details.iExcludedPointsBS[1], 22)
			SET_BIT(details.iExcludedPointsBS[1], 23)
			SET_BIT(details.iExcludedPointsBS[1], 24)
			SET_BIT(details.iExcludedPointsBS[1], 25)
			SET_BIT(details.iExcludedPointsBS[1], 26)
			SET_BIT(details.iExcludedPointsBS[1], 27)
			details.iExcludedPointsBS[2] = 0
			SET_BIT(details.iExcludedPointsBS[2], 0)
			SET_BIT(details.iExcludedPointsBS[2], 2)
			SET_BIT(details.iExcludedPointsBS[2], 4)
			SET_BIT(details.iExcludedPointsBS[2], 5)
			SET_BIT(details.iExcludedPointsBS[2], 6)
			SET_BIT(details.iExcludedPointsBS[2], 7)
			SET_BIT(details.iExcludedPointsBS[2], 8)
			SET_BIT(details.iExcludedPointsBS[2], 9)
			SET_BIT(details.iExcludedPointsBS[2], 10)
			SET_BIT(details.iExcludedPointsBS[2], 18)
			SET_BIT(details.iExcludedPointsBS[2], 19)
			SET_BIT(details.iExcludedPointsBS[2], 20)
			SET_BIT(details.iExcludedPointsBS[2], 21)
			SET_BIT(details.iExcludedPointsBS[2], 22)
			SET_BIT(details.iExcludedPointsBS[2], 23)
			SET_BIT(details.iExcludedPointsBS[2], 24)
			SET_BIT(details.iExcludedPointsBS[2], 25)
			details.iExcludedPointsBS[3] = 0
			SET_BIT(details.iExcludedPointsBS[3], 0)
			SET_BIT(details.iExcludedPointsBS[3], 2)
			SET_BIT(details.iExcludedPointsBS[3], 4)
			SET_BIT(details.iExcludedPointsBS[3], 5)
			SET_BIT(details.iExcludedPointsBS[3], 6)
			SET_BIT(details.iExcludedPointsBS[3], 7)
			SET_BIT(details.iExcludedPointsBS[3], 8)
			SET_BIT(details.iExcludedPointsBS[3], 9)
			SET_BIT(details.iExcludedPointsBS[3], 10)
			SET_BIT(details.iExcludedPointsBS[3], 11)
			SET_BIT(details.iExcludedPointsBS[3], 13)
			SET_BIT(details.iExcludedPointsBS[3], 15)
			SET_BIT(details.iExcludedPointsBS[3], 17)
			SET_BIT(details.iExcludedPointsBS[3], 19)
			SET_BIT(details.iExcludedPointsBS[3], 20)
			SET_BIT(details.iExcludedPointsBS[3], 21)
			SET_BIT(details.iExcludedPointsBS[3], 22)
			SET_BIT(details.iExcludedPointsBS[3], 23)
			SET_BIT(details.iExcludedPointsBS[3], 24)
			SET_BIT(details.iExcludedPointsBS[3], 26)
			SET_BIT(details.iExcludedPointsBS[3], 28)
			SET_BIT(details.iExcludedPointsBS[3], 30)
			details.iExcludedPointsBS[4] = 0
			SET_BIT(details.iExcludedPointsBS[4], 0)
			SET_BIT(details.iExcludedPointsBS[4], 1)
			SET_BIT(details.iExcludedPointsBS[4], 2)
			SET_BIT(details.iExcludedPointsBS[4], 3)
			SET_BIT(details.iExcludedPointsBS[4], 4)
			SET_BIT(details.iExcludedPointsBS[4], 5)
			SET_BIT(details.iExcludedPointsBS[4], 6)
			SET_BIT(details.iExcludedPointsBS[4], 7)
			SET_BIT(details.iExcludedPointsBS[4], 9)
			SET_BIT(details.iExcludedPointsBS[4], 11)
			SET_BIT(details.iExcludedPointsBS[4], 13)
			SET_BIT(details.iExcludedPointsBS[4], 14)
			SET_BIT(details.iExcludedPointsBS[4], 15)
			SET_BIT(details.iExcludedPointsBS[4], 16)
			SET_BIT(details.iExcludedPointsBS[4], 17)
			SET_BIT(details.iExcludedPointsBS[4], 18)
			SET_BIT(details.iExcludedPointsBS[4], 19)
			SET_BIT(details.iExcludedPointsBS[4], 20)
			SET_BIT(details.iExcludedPointsBS[4], 21)
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_CRACK_4
			details.vStartCoord = <<-254.9883, -2586.8770, 6.0006>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 1.6100
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.8000
			details.fGridHeading = 90.0000
			details.iGridWidth = 15
			details.iGridHeight = 9
			details.iSpawnOriginPointID = 6
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 8)
			SET_BIT(details.iExcludedPointsBS[0], 10)
			SET_BIT(details.iExcludedPointsBS[0], 12)
			SET_BIT(details.iExcludedPointsBS[0], 14)
			SET_BIT(details.iExcludedPointsBS[0], 25)
			SET_BIT(details.iExcludedPointsBS[0], 27)
			SET_BIT(details.iExcludedPointsBS[0], 29)
			details.iExcludedPointsBS[1] = 0
			SET_BIT(details.iExcludedPointsBS[1], 6)
			SET_BIT(details.iExcludedPointsBS[1], 8)
			SET_BIT(details.iExcludedPointsBS[1], 10)
			SET_BIT(details.iExcludedPointsBS[1], 12)
			SET_BIT(details.iExcludedPointsBS[1], 21)
			SET_BIT(details.iExcludedPointsBS[1], 23)
			SET_BIT(details.iExcludedPointsBS[1], 25)
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_CASH_4
			details.vStartCoord = <<669.6413, -2667.5061, 6.0812>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 1.5500
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.8000
			details.fGridHeading = 90.0000
			details.iGridWidth = 15
			details.iGridHeight = 9
			details.iSpawnOriginPointID = 0
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 5)
			SET_BIT(details.iExcludedPointsBS[0], 7)
			SET_BIT(details.iExcludedPointsBS[0], 9)
			SET_BIT(details.iExcludedPointsBS[0], 11)
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_FAKEID_4
			details.vStartCoord = <<-334.9665, -2777.5010, 5.0002>>
			details.fSpacingX = 1.4100
			details.fSpacingY = 1.5500
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.8000
			details.fGridHeading = 90.0000
			details.iGridWidth = 15
			details.iGridHeight = 9
			details.iSpawnOriginPointID = 2
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 14)
			details.iExcludedPointsBS[1] = 0
			SET_BIT(details.iExcludedPointsBS[1], 12)
			SET_BIT(details.iExcludedPointsBS[1], 27)
			details.iExcludedPointsBS[2] = 0
			SET_BIT(details.iExcludedPointsBS[2], 10)
			details.iExcludedPointsBS[3] = 0
			SET_BIT(details.iExcludedPointsBS[3], 3)
			SET_BIT(details.iExcludedPointsBS[3], 5)
			SET_BIT(details.iExcludedPointsBS[3], 18)
			SET_BIT(details.iExcludedPointsBS[3], 20)
			SET_BIT(details.iExcludedPointsBS[3], 22)
			details.iExcludedPointsBS[4] = 0
			SET_BIT(details.iExcludedPointsBS[4], 3)
			SET_BIT(details.iExcludedPointsBS[4], 5)
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_CRACK_INSIDE
			details.vStartCoord = <<1088.7280, -3188.1799, -38.9935>>
			details.fSpacingX = 1.3500
			details.fSpacingY = 1.4500
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.6400
			details.fGridHeading = 180.0000
			details.iGridWidth = 7
			details.iGridHeight = 8
			details.iSpawnOriginPointID = 0
			details.fProbeHeight = 1.0000
			details.bHumanizePos = TRUE
			details.fSinSpread = 8.8200
			details.fSinAmp = 0.5800
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 2)
			SET_BIT(details.iExcludedPointsBS[0], 3)
			SET_BIT(details.iExcludedPointsBS[0], 4)
			SET_BIT(details.iExcludedPointsBS[0], 5)
			SET_BIT(details.iExcludedPointsBS[0], 6)
			SET_BIT(details.iExcludedPointsBS[0], 10)
			SET_BIT(details.iExcludedPointsBS[0], 12)
			SET_BIT(details.iExcludedPointsBS[0], 13)
			SET_BIT(details.iExcludedPointsBS[0], 17)
			SET_BIT(details.iExcludedPointsBS[0], 18)
			SET_BIT(details.iExcludedPointsBS[0], 19)
			SET_BIT(details.iExcludedPointsBS[0], 20)
			SET_BIT(details.iExcludedPointsBS[0], 25)
			SET_BIT(details.iExcludedPointsBS[0], 26)
			SET_BIT(details.iExcludedPointsBS[0], 27)
			SET_BIT(details.iExcludedPointsBS[0], 31)
			details.iExcludedPointsBS[1] = 0
			SET_BIT(details.iExcludedPointsBS[1], 1)
			SET_BIT(details.iExcludedPointsBS[1], 4)
			SET_BIT(details.iExcludedPointsBS[1], 5)
			SET_BIT(details.iExcludedPointsBS[1], 6)
			SET_BIT(details.iExcludedPointsBS[1], 7)
			SET_BIT(details.iExcludedPointsBS[1], 8)
			SET_BIT(details.iExcludedPointsBS[1], 9)
			SET_BIT(details.iExcludedPointsBS[1], 12)
			SET_BIT(details.iExcludedPointsBS[1], 13)
			SET_BIT(details.iExcludedPointsBS[1], 14)
			SET_BIT(details.iExcludedPointsBS[1], 15)
			SET_BIT(details.iExcludedPointsBS[1], 16)
			SET_BIT(details.iExcludedPointsBS[1], 20)
			SET_BIT(details.iExcludedPointsBS[1], 22)
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_METH_INSIDE
			details.vStartCoord = <<997.2940, -3197.5840, -36.3937>>
			details.fSpacingX = 2.0100
			details.fSpacingY = 0.5800
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.6000
			details.fGridHeading = 270.0000
			details.iGridWidth = 6
			details.iGridHeight = 12
			details.iSpawnOriginPointID = 7
			details.fProbeHeight = 1.0000
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 0)
			SET_BIT(details.iExcludedPointsBS[0], 4)
			SET_BIT(details.iExcludedPointsBS[0], 5)
			SET_BIT(details.iExcludedPointsBS[0], 9)
			SET_BIT(details.iExcludedPointsBS[0], 10)
			SET_BIT(details.iExcludedPointsBS[0], 11)
			SET_BIT(details.iExcludedPointsBS[0], 12)
			SET_BIT(details.iExcludedPointsBS[0], 16)
			SET_BIT(details.iExcludedPointsBS[0], 17)
			SET_BIT(details.iExcludedPointsBS[0], 18)
			SET_BIT(details.iExcludedPointsBS[0], 19)
			SET_BIT(details.iExcludedPointsBS[0], 20)
			SET_BIT(details.iExcludedPointsBS[0], 22)
			SET_BIT(details.iExcludedPointsBS[0], 23)
			SET_BIT(details.iExcludedPointsBS[0], 24)
			SET_BIT(details.iExcludedPointsBS[0], 25)
			SET_BIT(details.iExcludedPointsBS[0], 26)
			SET_BIT(details.iExcludedPointsBS[0], 27)
			SET_BIT(details.iExcludedPointsBS[0], 28)
			SET_BIT(details.iExcludedPointsBS[0], 29)
			SET_BIT(details.iExcludedPointsBS[0], 30)
			SET_BIT(details.iExcludedPointsBS[0], 31)
			details.iExcludedPointsBS[1] = 0
			SET_BIT(details.iExcludedPointsBS[1], 0)
			SET_BIT(details.iExcludedPointsBS[1], 1)
			SET_BIT(details.iExcludedPointsBS[1], 2)
			SET_BIT(details.iExcludedPointsBS[1], 3)
			SET_BIT(details.iExcludedPointsBS[1], 4)
			SET_BIT(details.iExcludedPointsBS[1], 5)
			SET_BIT(details.iExcludedPointsBS[1], 6)
			SET_BIT(details.iExcludedPointsBS[1], 7)
			SET_BIT(details.iExcludedPointsBS[1], 8)
			SET_BIT(details.iExcludedPointsBS[1], 9)
			SET_BIT(details.iExcludedPointsBS[1], 10)
			SET_BIT(details.iExcludedPointsBS[1], 12)
			SET_BIT(details.iExcludedPointsBS[1], 14)
			SET_BIT(details.iExcludedPointsBS[1], 15)
			SET_BIT(details.iExcludedPointsBS[1], 20)
			SET_BIT(details.iExcludedPointsBS[1], 21)
			SET_BIT(details.iExcludedPointsBS[1], 26)
			SET_BIT(details.iExcludedPointsBS[1], 27)
			details.iExcludedPointsBS[2] = 0
			SET_BIT(details.iExcludedPointsBS[2], 0)
			SET_BIT(details.iExcludedPointsBS[2], 1)
			SET_BIT(details.iExcludedPointsBS[2], 4)
			SET_BIT(details.iExcludedPointsBS[2], 6)
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_WEED_INSIDE
			details.vStartCoord = <<1062.9910, -3182.5020, -39.1633>>
			details.fSpacingX = 1.3500
			details.fSpacingY = 1.3500
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.6400
			details.fGridHeading = 180.0000
			details.iGridWidth = 4
			details.iGridHeight = 8
			details.iSpawnOriginPointID = 0
			details.fProbeHeight = 1.0000
			details.bHumanizePos = TRUE
			details.fSinSpread = 10.0000
			details.fSinAmp = 0.5700
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 7)
			SET_BIT(details.iExcludedPointsBS[0], 14)
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_CASH_INSIDE
			details.vStartCoord = <<1138.4510, -3197.3069, -39.6657>>
			details.fSpacingX = 1.2000
			details.fSpacingY = 1.2000
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.6400
			details.fGridHeading = 90.0000
			details.iGridWidth = 4
			details.iGridHeight = 10
			details.iSpawnOriginPointID = 0
			details.fProbeHeight = 1.0000
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 1)
			SET_BIT(details.iExcludedPointsBS[0], 5)
			SET_BIT(details.iExcludedPointsBS[0], 7)
			SET_BIT(details.iExcludedPointsBS[0], 9)
			SET_BIT(details.iExcludedPointsBS[0], 12)
			SET_BIT(details.iExcludedPointsBS[0], 17)
			SET_BIT(details.iExcludedPointsBS[0], 18)
			SET_BIT(details.iExcludedPointsBS[0], 21)
			SET_BIT(details.iExcludedPointsBS[0], 22)
			SET_BIT(details.iExcludedPointsBS[0], 24)
			SET_BIT(details.iExcludedPointsBS[0], 25)
			SET_BIT(details.iExcludedPointsBS[0], 26)
			SET_BIT(details.iExcludedPointsBS[0], 28)
			SET_BIT(details.iExcludedPointsBS[0], 30)
			details.iExcludedPointsBS[1] = 0
			SET_BIT(details.iExcludedPointsBS[1], 0)
			SET_BIT(details.iExcludedPointsBS[1], 1)
			SET_BIT(details.iExcludedPointsBS[1], 2)
			SET_BIT(details.iExcludedPointsBS[1], 5)
			SET_BIT(details.iExcludedPointsBS[1], 6)
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_FACTORY_FAKEID_INSIDE
			details.vStartCoord = <<1172.9250, -3196.6030, -39.0080>>
			details.fSpacingX = 1.2000
			details.fSpacingY = 1.2000
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 0.6400
			details.fGridHeading = 90.0000
			details.iGridWidth = 7
			details.iGridHeight = 10
			details.iSpawnOriginPointID = 0
			details.fProbeHeight = 1.0000
			details.bHumanizePos = TRUE
			details.fSinSpread = 10.6300
			details.fSinAmp = 0.5000
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 5)
			SET_BIT(details.iExcludedPointsBS[0], 6)
			SET_BIT(details.iExcludedPointsBS[0], 12)
			SET_BIT(details.iExcludedPointsBS[0], 13)
			SET_BIT(details.iExcludedPointsBS[0], 18)
			SET_BIT(details.iExcludedPointsBS[0], 19)
			SET_BIT(details.iExcludedPointsBS[0], 20)
			SET_BIT(details.iExcludedPointsBS[0], 21)
			SET_BIT(details.iExcludedPointsBS[0], 22)
			SET_BIT(details.iExcludedPointsBS[0], 25)
			SET_BIT(details.iExcludedPointsBS[0], 26)
			SET_BIT(details.iExcludedPointsBS[0], 27)
			SET_BIT(details.iExcludedPointsBS[0], 28)
			SET_BIT(details.iExcludedPointsBS[0], 29)
			SET_BIT(details.iExcludedPointsBS[0], 30)
			details.iExcludedPointsBS[1] = 0
			SET_BIT(details.iExcludedPointsBS[1], 0)
			SET_BIT(details.iExcludedPointsBS[1], 1)
			SET_BIT(details.iExcludedPointsBS[1], 2)
			SET_BIT(details.iExcludedPointsBS[1], 6)
			SET_BIT(details.iExcludedPointsBS[1], 8)
			SET_BIT(details.iExcludedPointsBS[1], 9)
			SET_BIT(details.iExcludedPointsBS[1], 10)
			SET_BIT(details.iExcludedPointsBS[1], 12)
			SET_BIT(details.iExcludedPointsBS[1], 15)
			SET_BIT(details.iExcludedPointsBS[1], 16)
			SET_BIT(details.iExcludedPointsBS[1], 17)
			SET_BIT(details.iExcludedPointsBS[1], 18)
			SET_BIT(details.iExcludedPointsBS[1], 19)
			SET_BIT(details.iExcludedPointsBS[1], 21)
			SET_BIT(details.iExcludedPointsBS[1], 23)
			SET_BIT(details.iExcludedPointsBS[1], 24)
			SET_BIT(details.iExcludedPointsBS[1], 26)
			SET_BIT(details.iExcludedPointsBS[1], 30)
			details.iExcludedPointsBS[2] = 0
			SET_BIT(details.iExcludedPointsBS[2], 5)
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		/*CASE GRID_SPAWN_LOCATION_IE_WAREHOUSE_1
			details.vStartCoord = <<-633.3323, -1778.0320, 24.0924>>
			details.fSpacingX = 2.0000
			details.fSpacingY = 2.0000
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 1.0000
			details.fGridHeading = 128.5200
			details.iGridWidth = 4
			details.iGridHeight = 4
			details.iSpawnOriginPointID = 0
			details.fProbeHeight = 1.5000
			details.iExcludedPointsBS[0] = 0
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_IE_WAREHOUSE_2
			details.vStartCoord = <<1003.1100, -1856.5320, 31.0398>>
			details.fSpacingX = 2.0000
			details.fSpacingY = 2.0000
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 1.0000
			details.fGridHeading = 174.6000
			details.iGridWidth = 4
			details.iGridHeight = 4
			details.iSpawnOriginPointID = 0
			details.fProbeHeight = 1.5000
			details.iExcludedPointsBS[0] = 0
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_IE_WAREHOUSE_3
			details.vStartCoord = <<-70.8791, -1821.3910, 26.9420>>
			details.fSpacingX = 2.0000
			details.fSpacingY = 2.0000
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 1.0000
			details.fGridHeading = 229.3200
			details.iGridWidth = 4
			details.iGridHeight = 4
			details.iSpawnOriginPointID = 0
			details.fProbeHeight = 1.5000
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 12)
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_IE_WAREHOUSE_4
			details.vStartCoord = <<37.8692, -1284.1758, 29.2777>>
			details.fSpacingX = 2.0000
			details.fSpacingY = 2.0000
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 1.0000
			details.fGridHeading = 271.8000
			details.iGridWidth = 4
			details.iGridHeight = 4
			details.iSpawnOriginPointID = 0
			details.fProbeHeight = 1.5000
			details.iExcludedPointsBS[0] = 0
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_IE_WAREHOUSE_5
			details.vStartCoord = <<1213.0129, -1260.4940, 35.2268>>
			details.fSpacingX = 2.0000
			details.fSpacingY = 2.0000
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 1.0000
			details.fGridHeading = 90.3600
			details.iGridWidth = 4
			details.iGridHeight = 4
			details.iSpawnOriginPointID = 0
			details.fProbeHeight = 1.5000
			details.iExcludedPointsBS[0] = 0
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_IE_WAREHOUSE_6
			details.vStartCoord = <<805.2272, -2220.7910, 29.4644>>
			details.fSpacingX = 2.0000
			details.fSpacingY = 2.0000
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 1.0000
			details.fGridHeading = 174.6000
			details.iGridWidth = 4
			details.iGridHeight = 4
			details.iSpawnOriginPointID = 0
			details.fProbeHeight = 1.5000
			details.iExcludedPointsBS[0] = 0
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_IE_WAREHOUSE_7
			details.vStartCoord = <<1762.6980, -1647.0581, 112.6455>>
			details.fSpacingX = 2.0000
			details.fSpacingY = 2.0000
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 1.0000
			details.fGridHeading = 189.7200
			details.iGridWidth = 4
			details.iGridHeight = 4
			details.iSpawnOriginPointID = 0
			details.fProbeHeight = 1.5000
			details.iExcludedPointsBS[0] = 0
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_IE_WAREHOUSE_8
			details.vStartCoord = <<142.9376, -3004.0879, 7.0309>>
			details.fSpacingX = 2.0000
			details.fSpacingY = 2.0000
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 1.0000
			details.fGridHeading = 0.7200
			details.iGridWidth = 4
			details.iGridHeight = 4
			details.iSpawnOriginPointID = 0
			details.fProbeHeight = 1.5000
			details.iExcludedPointsBS[0] = 0
			SET_BIT(details.iExcludedPointsBS[0], 2)
			SET_BIT(details.iExcludedPointsBS[0], 7)
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_IE_WAREHOUSE_9
			details.vStartCoord = <<-514.7808, -2200.8831, 6.3940>>
			details.fSpacingX = 2.0000
			details.fSpacingY = 2.0000
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 1.0000
			details.fGridHeading = 319.6800
			details.iGridWidth = 4
			details.iGridHeight = 4
			details.iSpawnOriginPointID = 0
			details.fProbeHeight = 1.5000
			details.iExcludedPointsBS[0] = 0
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_IE_WAREHOUSE_10
			details.vStartCoord = <<-1151.7271, -2171.9939, 13.2693>>
			details.fSpacingX = 2.0000
			details.fSpacingY = 2.0000
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 1.0000
			details.fGridHeading = 136.0000
			details.iGridWidth = 4
			details.iGridHeight = 4
			details.iSpawnOriginPointID = 0
			details.fProbeHeight = 1.5000
			details.iExcludedPointsBS[0] = 0
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK*/
		//════════════════════════════════════════════════════════════════════
		CASE GRID_SPAWN_LOCATION_IE_WAREHOUSE_INSIDE
			details.vStartCoord = <<972.0000, -2989.0000, -39.6471>>
			details.fSpacingX = 2.0000
			details.fSpacingY = 2.0000
			details.fOffsetZ = 0.1000
			details.fProbeRadius = 1.0000
			details.fGridHeading = 178.5600
			details.iGridWidth = 4
			details.iGridHeight = 4
			details.iSpawnOriginPointID = 0
			details.fProbeHeight = 1.5000
			details.iExcludedPointsBS[0] = 0
			details.iExcludedPointsBS[1] = 0
			details.iExcludedPointsBS[2] = 0
			details.iExcludedPointsBS[3] = 0
			details.iExcludedPointsBS[4] = 0
			details.iExcludedPointsBS[5] = 0
			details.iExcludedPointsBS[6] = 0
			details.iExcludedPointsBS[7] = 0
		BREAK
//		#IF FEATURE_GUNRUNNING
//		//════════════════════════════════════════════════════════════════════
//		CASE GRID_SPAWN_LOCATION_BUNKER_1
//			details.vStartCoord = <<502.4750, 3031.9250, 40.0000>>
//			details.fSpacingX = 1.5800
//			details.fSpacingY = 1.6075
//			details.fOffsetZ = 0.1000
//			details.fProbeRadius = 0.7000
//			details.fGridHeading = 317.8800
//			details.iGridWidth = 14
//			details.iGridHeight = 8
//			details.iSpawnOriginPointID = 0
//			details.fProbeHeight = 1.5000
//			details.bHumanizePos = TRUE
//			details.fSinSpread = -2.8000
//			details.fSinAmp = 1.8000
//			details.iExcludedPointsBS[0] = 0
//			SET_BIT(details.iExcludedPointsBS[0], 27)
//			details.iExcludedPointsBS[1] = 0
//			details.iExcludedPointsBS[2] = 0
//			details.iExcludedPointsBS[3] = 0
//			SET_BIT(details.iExcludedPointsBS[3], 14)
//			details.iExcludedPointsBS[4] = 0
//			details.iExcludedPointsBS[5] = 0
//			details.iExcludedPointsBS[6] = 0
//			details.iExcludedPointsBS[7] = 0
//		BREAK
//		//════════════════════════════════════════════════════════════════════
//		CASE GRID_SPAWN_LOCATION_BUNKER_2
//			details.vStartCoord = <<847.6078, 3103.3035, 40.3357>>
//			details.fSpacingX = 1.9350
//			details.fSpacingY = 2.3450
//			details.fOffsetZ = 0.1000
//			details.fProbeRadius = 0.7000
//			details.fGridHeading = 176.0400
//			details.iGridWidth = 16
//			details.iGridHeight = 6
//			details.iSpawnOriginPointID = 0
//			details.fProbeHeight = 1.5000
//			details.bHumanizePos = TRUE
//			details.fSinSpread = 2.2000
//			details.fSinAmp = -5.0000
//			details.iExcludedPointsBS[0] = 0
//			SET_BIT(details.iExcludedPointsBS[0], 14)
//			details.iExcludedPointsBS[1] = 0
//			details.iExcludedPointsBS[2] = 0
//			details.iExcludedPointsBS[3] = 0
//			details.iExcludedPointsBS[4] = 0
//			details.iExcludedPointsBS[5] = 0
//			details.iExcludedPointsBS[6] = 0
//			details.iExcludedPointsBS[7] = 0
//		BREAK
//		//════════════════════════════════════════════════════════════════════
//		CASE GRID_SPAWN_LOCATION_BUNKER_3
//			details.vStartCoord = <<433.5970, 2603.8650, 43.5934>>
//			details.fSpacingX = 1.9350
//			details.fSpacingY = 2.3450
//			details.fOffsetZ = 0.1000
//			details.fProbeRadius = 0.7000
//			details.fGridHeading = 7.2000
//			details.iGridWidth = 16
//			details.iGridHeight = 6
//			details.iSpawnOriginPointID = 0
//			details.fProbeHeight = 1.5000
//			details.bHumanizePos = TRUE
//			details.fSinSpread = 3.2000
//			details.fSinAmp = -3.8000
//			details.iExcludedPointsBS[0] = 0
//			details.iExcludedPointsBS[1] = 0
//			details.iExcludedPointsBS[2] = 0
//			SET_BIT(details.iExcludedPointsBS[2], 16)
//			SET_BIT(details.iExcludedPointsBS[2], 17)
//			SET_BIT(details.iExcludedPointsBS[2], 18)
//			SET_BIT(details.iExcludedPointsBS[2], 19)
//			SET_BIT(details.iExcludedPointsBS[2], 20)
//			SET_BIT(details.iExcludedPointsBS[2], 21)
//			details.iExcludedPointsBS[3] = 0
//			details.iExcludedPointsBS[4] = 0
//			details.iExcludedPointsBS[5] = 0
//			details.iExcludedPointsBS[6] = 0
//			details.iExcludedPointsBS[7] = 0
//		BREAK
//		//════════════════════════════════════════════════════════════════════
//		CASE GRID_SPAWN_LOCATION_BUNKER_4
//			details.vStartCoord = <<1572.7679, 2215.5210, 78.6775>>
//			details.fSpacingX = 1.9350
//			details.fSpacingY = 2.3450
//			details.fOffsetZ = 0.1000
//			details.fProbeRadius = 0.7000
//			details.fGridHeading = 179.6400
//			details.iGridWidth = 15
//			details.iGridHeight = 7
//			details.iSpawnOriginPointID = 0
//			details.fProbeHeight = 1.5000
//			details.bHumanizePos = TRUE
//			details.fSinSpread = 15.8000
//			details.fSinAmp = 1.1500
//			details.iExcludedPointsBS[0] = 0
//			details.iExcludedPointsBS[1] = 0
//			details.iExcludedPointsBS[2] = 0
//			SET_BIT(details.iExcludedPointsBS[2], 10)
//			SET_BIT(details.iExcludedPointsBS[2], 25)
//			details.iExcludedPointsBS[3] = 0
//			SET_BIT(details.iExcludedPointsBS[3], 8)
//			details.iExcludedPointsBS[4] = 0
//			details.iExcludedPointsBS[5] = 0
//			details.iExcludedPointsBS[6] = 0
//			details.iExcludedPointsBS[7] = 0
//		BREAK
//		//════════════════════════════════════════════════════════════════════
//		CASE GRID_SPAWN_LOCATION_BUNKER_5
//			details.vStartCoord = <<2099.1960, 3318.0029, 45.3677>>
//			details.fSpacingX = 1.9350
//			details.fSpacingY = 2.3450
//			details.fOffsetZ = 0.2000
//			details.fProbeRadius = 0.7000
//			details.fGridHeading = 109.8000
//			details.iGridWidth = 10
//			details.iGridHeight = 12
//			details.iSpawnOriginPointID = 0
//			details.fProbeHeight = 1.5000
//			details.bHumanizePos = TRUE
//			details.fSinSpread = 5.2000
//			details.fSinAmp = -3.2000
//			details.iExcludedPointsBS[0] = 0
//			SET_BIT(details.iExcludedPointsBS[0], 9)
//			SET_BIT(details.iExcludedPointsBS[0], 19)
//			SET_BIT(details.iExcludedPointsBS[0], 26)
//			SET_BIT(details.iExcludedPointsBS[0], 28)
//			SET_BIT(details.iExcludedPointsBS[0], 29)
//			details.iExcludedPointsBS[1] = 0
//			SET_BIT(details.iExcludedPointsBS[1], 4)
//			SET_BIT(details.iExcludedPointsBS[1], 6)
//			SET_BIT(details.iExcludedPointsBS[1], 14)
//			SET_BIT(details.iExcludedPointsBS[1], 16)
//			details.iExcludedPointsBS[2] = 0
//			details.iExcludedPointsBS[3] = 0
//			details.iExcludedPointsBS[4] = 0
//			details.iExcludedPointsBS[5] = 0
//			details.iExcludedPointsBS[6] = 0
//			details.iExcludedPointsBS[7] = 0
//		BREAK
//		//════════════════════════════════════════════════════════════════════
//		CASE GRID_SPAWN_LOCATION_BUNKER_6
//			details.vStartCoord = <<2487.9370, 3184.6770, 49.6790>>
//			details.fSpacingX = 1.9350
//			details.fSpacingY = 2.3450
//			details.fOffsetZ = 0.1000
//			details.fProbeRadius = 0.7000
//			details.fGridHeading = 3.6000
//			details.iGridWidth = 12
//			details.iGridHeight = 11
//			details.iSpawnOriginPointID = 0
//			details.fProbeHeight = 1.5000
//			details.bHumanizePos = TRUE
//			details.fSinSpread = 9.0000
//			details.fSinAmp = -2.6000
//			details.iExcludedPointsBS[0] = 0
//			SET_BIT(details.iExcludedPointsBS[0], 5)
//			SET_BIT(details.iExcludedPointsBS[0], 7)
//			SET_BIT(details.iExcludedPointsBS[0], 9)
//			SET_BIT(details.iExcludedPointsBS[0], 11)
//			SET_BIT(details.iExcludedPointsBS[0], 14)
//			SET_BIT(details.iExcludedPointsBS[0], 17)
//			SET_BIT(details.iExcludedPointsBS[0], 19)
//			SET_BIT(details.iExcludedPointsBS[0], 21)
//			SET_BIT(details.iExcludedPointsBS[0], 23)
//			SET_BIT(details.iExcludedPointsBS[0], 29)
//			SET_BIT(details.iExcludedPointsBS[0], 31)
//			details.iExcludedPointsBS[1] = 0
//			SET_BIT(details.iExcludedPointsBS[1], 1)
//			SET_BIT(details.iExcludedPointsBS[1], 2)
//			SET_BIT(details.iExcludedPointsBS[1], 3)
//			SET_BIT(details.iExcludedPointsBS[1], 12)
//			SET_BIT(details.iExcludedPointsBS[1], 13)
//			SET_BIT(details.iExcludedPointsBS[1], 15)
//			SET_BIT(details.iExcludedPointsBS[1], 24)
//			SET_BIT(details.iExcludedPointsBS[1], 25)
//			SET_BIT(details.iExcludedPointsBS[1], 26)
//			SET_BIT(details.iExcludedPointsBS[1], 27)
//			details.iExcludedPointsBS[2] = 0
//			SET_BIT(details.iExcludedPointsBS[2], 4)
//			SET_BIT(details.iExcludedPointsBS[2], 5)
//			SET_BIT(details.iExcludedPointsBS[2], 6)
//			SET_BIT(details.iExcludedPointsBS[2], 7)
//			SET_BIT(details.iExcludedPointsBS[2], 14)
//			SET_BIT(details.iExcludedPointsBS[2], 19)
//			details.iExcludedPointsBS[3] = 0
//			details.iExcludedPointsBS[4] = 0
//			details.iExcludedPointsBS[5] = 0
//			details.iExcludedPointsBS[6] = 0
//			details.iExcludedPointsBS[7] = 0
//		BREAK
//		//════════════════════════════════════════════════════════════════════
//		CASE GRID_SPAWN_LOCATION_BUNKER_7
//			details.vStartCoord = <<2044.8210, 5115.7729, 45.8406>>
//			details.fSpacingX = 1.9350
//			details.fSpacingY = 2.3450
//			details.fOffsetZ = 0.1000
//			details.fProbeRadius = 0.7000
//			details.fGridHeading = 46.4400
//			details.iGridWidth = 14
//			details.iGridHeight = 8
//			details.iSpawnOriginPointID = 0
//			details.fProbeHeight = 1.5000
//			details.bHumanizePos = TRUE
//			details.fSinSpread = -4.2000
//			details.fSinAmp = 2.6000
//			details.iExcludedPointsBS[0] = 0
//			SET_BIT(details.iExcludedPointsBS[0], 11)
//			SET_BIT(details.iExcludedPointsBS[0], 13)
//			SET_BIT(details.iExcludedPointsBS[0], 25)
//			SET_BIT(details.iExcludedPointsBS[0], 27)
//			details.iExcludedPointsBS[1] = 0
//			SET_BIT(details.iExcludedPointsBS[1], 9)
//			details.iExcludedPointsBS[2] = 0
//			details.iExcludedPointsBS[3] = 0
//			details.iExcludedPointsBS[4] = 0
//			details.iExcludedPointsBS[5] = 0
//			details.iExcludedPointsBS[6] = 0
//			details.iExcludedPointsBS[7] = 0
//		BREAK
//		//════════════════════════════════════════════════════════════════════
//		CASE GRID_SPAWN_LOCATION_BUNKER_8
//			details.vStartCoord = <<296.1891, 6754.5908, 16.1853>>
//			details.fSpacingX = 1.9350
//			details.fSpacingY = 2.3450
//			details.fOffsetZ = 0.1000
//			details.fProbeRadius = 0.7000
//			details.fGridHeading = 231.4800
//			details.iGridWidth = 14
//			details.iGridHeight = 8
//			details.iSpawnOriginPointID = 0
//			details.fProbeHeight = 1.5000
//			details.bHumanizePos = TRUE
//			details.fSinSpread = -4.2000
//			details.fSinAmp = 2.6000
//			details.iExcludedPointsBS[0] = 0
//			details.iExcludedPointsBS[1] = 0
//			details.iExcludedPointsBS[2] = 0
//			details.iExcludedPointsBS[3] = 0
//			details.iExcludedPointsBS[4] = 0
//			details.iExcludedPointsBS[5] = 0
//			details.iExcludedPointsBS[6] = 0
//			details.iExcludedPointsBS[7] = 0
//		BREAK
//		//════════════════════════════════════════════════════════════════════
//		CASE GRID_SPAWN_LOCATION_BUNKER_9
//			details.vStartCoord = <<-758.8856, 5942.7910, 20.0769>>
//			details.fSpacingX = 1.9350
//			details.fSpacingY = 2.3450
//			details.fOffsetZ = 0.1000
//			details.fProbeRadius = 0.7000
//			details.fGridHeading = 285.8400
//			details.iGridWidth = 14
//			details.iGridHeight = 8
//			details.iSpawnOriginPointID = 0
//			details.fProbeHeight = 1.5000
//			details.bHumanizePos = TRUE
//			details.fSinSpread = -4.2000
//			details.fSinAmp = 2.6000
//			details.iExcludedPointsBS[0] = 0
//			details.iExcludedPointsBS[1] = 0
//			details.iExcludedPointsBS[2] = 0
//			details.iExcludedPointsBS[3] = 0
//			details.iExcludedPointsBS[4] = 0
//			details.iExcludedPointsBS[5] = 0
//			details.iExcludedPointsBS[6] = 0
//			details.iExcludedPointsBS[7] = 0
//		BREAK
//		//════════════════════════════════════════════════════════════════════
//		CASE GRID_SPAWN_LOCATION_BUNKER_10
//			details.vStartCoord = <<-387.4280, 4338.3340, 56.1499>>
//			details.fSpacingX = 1.9350
//			details.fSpacingY = 2.3450
//			details.fOffsetZ = 0.1000
//			details.fProbeRadius = 0.7000
//			details.fGridHeading = 182.8800
//			details.iGridWidth = 15
//			details.iGridHeight = 9
//			details.iSpawnOriginPointID = 0
//			details.fProbeHeight = 1.5000
//			details.bHumanizePos = TRUE
//			details.fSinSpread = -4.2000
//			details.fSinAmp = 2.6000
//			details.iExcludedPointsBS[0] = 0
//			SET_BIT(details.iExcludedPointsBS[0], 5)
//			SET_BIT(details.iExcludedPointsBS[0], 7)
//			SET_BIT(details.iExcludedPointsBS[0], 9)
//			SET_BIT(details.iExcludedPointsBS[0], 11)
//			SET_BIT(details.iExcludedPointsBS[0], 13)
//			SET_BIT(details.iExcludedPointsBS[0], 18)
//			SET_BIT(details.iExcludedPointsBS[0], 20)
//			SET_BIT(details.iExcludedPointsBS[0], 22)
//			SET_BIT(details.iExcludedPointsBS[0], 24)
//			SET_BIT(details.iExcludedPointsBS[0], 26)
//			SET_BIT(details.iExcludedPointsBS[0], 28)
//			details.iExcludedPointsBS[1] = 0
//			SET_BIT(details.iExcludedPointsBS[1], 3)
//			SET_BIT(details.iExcludedPointsBS[1], 5)
//			SET_BIT(details.iExcludedPointsBS[1], 7)
//			SET_BIT(details.iExcludedPointsBS[1], 9)
//			SET_BIT(details.iExcludedPointsBS[1], 11)
//			SET_BIT(details.iExcludedPointsBS[1], 12)
//			SET_BIT(details.iExcludedPointsBS[1], 20)
//			SET_BIT(details.iExcludedPointsBS[1], 21)
//			SET_BIT(details.iExcludedPointsBS[1], 22)
//			SET_BIT(details.iExcludedPointsBS[1], 23)
//			SET_BIT(details.iExcludedPointsBS[1], 24)
//			SET_BIT(details.iExcludedPointsBS[1], 25)
//			SET_BIT(details.iExcludedPointsBS[1], 26)
//			SET_BIT(details.iExcludedPointsBS[1], 27)
//			details.iExcludedPointsBS[2] = 0
//			SET_BIT(details.iExcludedPointsBS[2], 3)
//			SET_BIT(details.iExcludedPointsBS[2], 5)
//			SET_BIT(details.iExcludedPointsBS[2], 6)
//			SET_BIT(details.iExcludedPointsBS[2], 7)
//			SET_BIT(details.iExcludedPointsBS[2], 8)
//			SET_BIT(details.iExcludedPointsBS[2], 9)
//			SET_BIT(details.iExcludedPointsBS[2], 10)
//			SET_BIT(details.iExcludedPointsBS[2], 18)
//			SET_BIT(details.iExcludedPointsBS[2], 20)
//			SET_BIT(details.iExcludedPointsBS[2], 22)
//			SET_BIT(details.iExcludedPointsBS[2], 23)
//			SET_BIT(details.iExcludedPointsBS[2], 24)
//			SET_BIT(details.iExcludedPointsBS[2], 25)
//			details.iExcludedPointsBS[3] = 0
//			SET_BIT(details.iExcludedPointsBS[3], 6)
//			SET_BIT(details.iExcludedPointsBS[3], 8)
//			details.iExcludedPointsBS[4] = 0
//			details.iExcludedPointsBS[5] = 0
//			details.iExcludedPointsBS[6] = 0
//			details.iExcludedPointsBS[7] = 0
//		BREAK
//		//════════════════════════════════════════════════════════════════════
//		CASE GRID_SPAWN_LOCATION_BUNKER_11
//			details.vStartCoord = <<-3034.1450, 3334.0830, 10.0467>>
//			details.fSpacingX = 1.9350
//			details.fSpacingY = 2.3450
//			details.fOffsetZ = 0.1000
//			details.fProbeRadius = 0.7000
//			details.fGridHeading = 277.2000
//			details.iGridWidth = 13
//			details.iGridHeight = 8
//			details.iSpawnOriginPointID = 0
//			details.fProbeHeight = 1.5000
//			details.bHumanizePos = TRUE
//			details.fSinSpread = -4.2000
//			details.fSinAmp = 1.7500
//			details.iExcludedPointsBS[0] = 0
//			details.iExcludedPointsBS[1] = 0
//			SET_BIT(details.iExcludedPointsBS[1], 2)
//			SET_BIT(details.iExcludedPointsBS[1], 4)
//			details.iExcludedPointsBS[2] = 0
//			details.iExcludedPointsBS[3] = 0
//			details.iExcludedPointsBS[4] = 0
//			details.iExcludedPointsBS[5] = 0
//			details.iExcludedPointsBS[6] = 0
//			details.iExcludedPointsBS[7] = 0
//		BREAK
//		//════════════════════════════════════════════════════════════════════
//		CASE GRID_SPAWN_LOCATION_BUNKER_12
//			details.vStartCoord = <<-3158.2949, 1379.6870, 16.5246>>
//			details.fSpacingX = 1.9350
//			details.fSpacingY = 2.3450
//			details.fOffsetZ = 0.1000
//			details.fProbeRadius = 0.7000
//			details.fGridHeading = 280.0800
//			details.iGridWidth = 17
//			details.iGridHeight = 6
//			details.iSpawnOriginPointID = 0
//			details.fProbeHeight = 1.5000
//			details.bHumanizePos = TRUE
//			details.fSinSpread = -7.0000
//			details.fSinAmp = 1.7500
//			details.iExcludedPointsBS[0] = 0
//			SET_BIT(details.iExcludedPointsBS[0], 13)
//			SET_BIT(details.iExcludedPointsBS[0], 14)
//			SET_BIT(details.iExcludedPointsBS[0], 15)
//			SET_BIT(details.iExcludedPointsBS[0], 16)
//			SET_BIT(details.iExcludedPointsBS[0], 26)
//			SET_BIT(details.iExcludedPointsBS[0], 28)
//			SET_BIT(details.iExcludedPointsBS[0], 30)
//			SET_BIT(details.iExcludedPointsBS[0], 31)
//			details.iExcludedPointsBS[1] = 0
//			SET_BIT(details.iExcludedPointsBS[1], 0)
//			SET_BIT(details.iExcludedPointsBS[1], 1)
//			SET_BIT(details.iExcludedPointsBS[1], 8)
//			SET_BIT(details.iExcludedPointsBS[1], 11)
//			SET_BIT(details.iExcludedPointsBS[1], 13)
//			SET_BIT(details.iExcludedPointsBS[1], 15)
//			SET_BIT(details.iExcludedPointsBS[1], 16)
//			SET_BIT(details.iExcludedPointsBS[1], 17)
//			SET_BIT(details.iExcludedPointsBS[1], 18)
//			SET_BIT(details.iExcludedPointsBS[1], 28)
//			SET_BIT(details.iExcludedPointsBS[1], 30)
//			SET_BIT(details.iExcludedPointsBS[1], 31)
//			details.iExcludedPointsBS[2] = 0
//			SET_BIT(details.iExcludedPointsBS[2], 0)
//			SET_BIT(details.iExcludedPointsBS[2], 1)
//			SET_BIT(details.iExcludedPointsBS[2], 2)
//			SET_BIT(details.iExcludedPointsBS[2], 3)
//			SET_BIT(details.iExcludedPointsBS[2], 7)
//			SET_BIT(details.iExcludedPointsBS[2], 9)
//			SET_BIT(details.iExcludedPointsBS[2], 11)
//			SET_BIT(details.iExcludedPointsBS[2], 13)
//			SET_BIT(details.iExcludedPointsBS[2], 15)
//			SET_BIT(details.iExcludedPointsBS[2], 16)
//			SET_BIT(details.iExcludedPointsBS[2], 17)
//			SET_BIT(details.iExcludedPointsBS[2], 18)
//			SET_BIT(details.iExcludedPointsBS[2], 19)
//			SET_BIT(details.iExcludedPointsBS[2], 20)
//			SET_BIT(details.iExcludedPointsBS[2], 21)
//			SET_BIT(details.iExcludedPointsBS[2], 22)
//			SET_BIT(details.iExcludedPointsBS[2], 24)
//			SET_BIT(details.iExcludedPointsBS[2], 26)
//			SET_BIT(details.iExcludedPointsBS[2], 28)
//			SET_BIT(details.iExcludedPointsBS[2], 30)
//			details.iExcludedPointsBS[3] = 0
//			SET_BIT(details.iExcludedPointsBS[3], 0)
//			SET_BIT(details.iExcludedPointsBS[3], 1)
//			SET_BIT(details.iExcludedPointsBS[3], 2)
//			SET_BIT(details.iExcludedPointsBS[3], 3)
//			SET_BIT(details.iExcludedPointsBS[3], 5)
//			details.iExcludedPointsBS[4] = 0
//			details.iExcludedPointsBS[5] = 0
//			details.iExcludedPointsBS[6] = 0
//			details.iExcludedPointsBS[7] = 0
//		BREAK
//		#ENDIF
	ENDSWITCH
	
	RETURN details
ENDFUNC

FUNC BOOL GET_ASYNC_GRID_LOCATION_HEADING_OVERRIDE_ZONE(ASYNC_GRID_SPAWN_LOCATIONS location, INT iIndex, VECTOR &vPos1, VECTOR &vPos2, FLOAT &fWidth, FLOAT &fHeading)
	SWITCH location
		CASE GRID_SPAWN_LOCATION_FACTORY_METH_INSIDE
			SWITCH iIndex
				CASE 0
					vPos1 = <<998.028931,-3202.442871,-38.863091>>
					vPos2 = <<998.048584,-3196.208008,-35.643162>> 
					fWidth = 3.500000
					fHeading = 0.0
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

