
USING "net_heist_planning_generic_support.sch"

FUNC STRING NET_HEIST_PLANNING_GENERIC_CAMERA_LOCATION_TO_STRING(NET_HEIST_PLANNING_GENERIC_CAMERA_LOCATION eLocation)
	SWITCH eLocation
		CASE NHPG_CAMLOCATION__INVALID				RETURN "NHPG_CAMLOCATION__INVALID"
		CASE NHPG_CAMLOCATION__PRIMARY				RETURN "NHPG_CAMLOCATION__PRIMARY"
		CASE NHPG_CAMLOCATION__SECONDARY			RETURN "NHPG_CAMLOCATION__SECONDARY"
		CASE NHPG_CAMLOCATION__TERTIARY				RETURN "NHPG_CAMLOCATION__TERTIARY"
		CASE NHPG_CAMLOCATION__QUATERNARY			RETURN "NHPG_CAMLOCATION__QUATERNARY"
		CASE NHPG_CAMLOCATION__QUINARY				RETURN "NHPG_CAMLOCATION__QUINARY"
		CASE NHPG_CAMLOCATION__SENARY				RETURN "NHPG_CAMLOCATION__SENARY"
		CASE NHPG_CAMLOCATION__SEPTENARY			RETURN "NHPG_CAMLOCATION__SEPTENARY"
		CASE NHPG_CAMLOCATION__OCTONARY				RETURN "NHPG_CAMLOCATION__OCTONARY"
		CASE NHPG_CAMLOCATION__NONARY				RETURN "NHPG_CAMLOCATION__NONARY"
		CASE NHPG_CAMLOCATION__DENARY				RETURN "NHPG_CAMLOCATION__DENARY"
		CASE NHPG_CAMLOCATION__MAX					RETURN "NHPG_CAMLOCATION__MAX"
	ENDSWITCH 
	RETURN ""
ENDFUNC

PROC SET_CAMERA_POSITION(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, NET_HEIST_PLANNING_GENERIC_CAMERA_LOCATION ePosition)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | SET_CAMERA_POSITION - Setting to ", ePosition)
	DEBUG_PRINTCALLSTACK()
	sData.sCameraData.ePositionId = ePosition
ENDPROC

PROC SET_CAMERA_POSITION_CACHED(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, NET_HEIST_PLANNING_GENERIC_CAMERA_LOCATION ePosition)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | SET_CAMERA_POSITION_CACHED - Setting to ", ePosition)
	DEBUG_PRINTCALLSTACK()
	sData.sCameraData.eCachedPositionId = ePosition
ENDPROC

PROC SET_FPS_CAM_FAR(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, FLOAT fFOV)
	sData.sCameraData.sFPSCam.fBaseCamFov 	= fFOV
	sData.sCameraData.sFPSCam.iLookXLimit 	= ci_HEIST_CAM_X_LOOK_LIMIT_FAR
	sData.sCameraData.sFPSCam.iLookYLimit 	= ci_HEIST_CAM_Y_LOOK_LIMIT_FAR
	sData.sCameraData.sFPSCam.iRollLimit 	= ci_HEIST_CAM_ROLL_LIMIT_FAR	
	sData.sCameraData.sFPSCam.fMaxZoom 		= 10.0
ENDPROC

FUNC VECTOR GET_CAMERA_POSITION_FOR_POSITION_ID(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	RETURN CALL logic.Camera.Position(sData.sCameraData.ePositionId)
ENDFUNC

FUNC VECTOR GET_CAMERA_ROTATION_FOR_POSITION_ID(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	RETURN CALL logic.Camera.Rotation(sData.sCameraData.ePositionId)
ENDFUNC

FUNC FLOAT GET_CAMERA_FOV_FOR_POSITION_ID(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	RETURN CALL logic.Camera.Fov(sData.sCameraData.ePositionId)
ENDFUNC

FUNC BOOL IS_ZOOM_AVAILABLE()
	IF logic.Camera.Zoom.Enable != NULL
		RETURN CALL logic.Camera.Zoom.Enable()
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ZOOM_IN_ONLY()
	IF logic.Camera.Zoom.OnlyIn != NULL
		RETURN CALL logic.Camera.Zoom.OnlyIn()
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL USE_TRIGGERS_TO_ZOOM()
	IF logic.Camera.Zoom.UseTriggers != NULL
		RETURN CALL logic.Camera.Zoom.UseTriggers()
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL IS_STICK_LOOK_AVAILABLE()
	IF logic.Camera.StickLook != NULL
		RETURN CALL logic.Camera.StickLook()
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SHAKE_ENABLED()
	IF logic.Camera.Shake.Enable != NULL
		RETURN CALL logic.Camera.Shake.Enable()
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC STRING GET_SHAKE_NAME()
	IF logic.Camera.Shake.Name != NULL
		RETURN CALL logic.Camera.Shake.Name()
	ENDIF

	RETURN "HAND_SHAKE"
ENDFUNC

FUNC FLOAT GET_SHAKE_AMPLITUDE()
	IF logic.Camera.Shake.Amplitude != NULL
		RETURN CALL logic.Camera.Shake.Amplitude()
	ENDIF
	
	RETURN 1.0
ENDFUNC

PROC UPDATE_CAMERA(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)

	IF sData.sCameraData.eCachedPositionId != sData.sCameraData.ePositionId
		
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | UPDATE_CAMERA - New position: ", NET_HEIST_PLANNING_GENERIC_CAMERA_LOCATION_TO_STRING(sData.sCameraData.ePositionId), 
			" from old position: ", NET_HEIST_PLANNING_GENERIC_CAMERA_LOCATION_TO_STRING(sData.sCameraData.eCachedPositionId))
		
		DEBUG_PRINTCALLSTACK()
		
		SET_CAMERA_POSITION_CACHED(sData, sData.sCameraData.ePositionId)
		sData.sCameraData.bTransitioning = TRUE
			
		VECTOR vPosOffset = << 0.0, 0.0, 0.0 >>
		VECTOR vRotOffset = << 0.0, 0.0, 0.0 >>
		
		sData.sCameraData.vCamTargetPos = GET_CAMERA_POSITION_FOR_POSITION_ID(sData)
		sData.sCameraData.vCamTargetRot = GET_CAMERA_ROTATION_FOR_POSITION_ID(sData)
		sData.sCameraData.sFPSCam.fCamFOVTarget = GET_CAMERA_FOV_FOR_POSITION_ID(sData)
		
		sData.sCameraData.vCamCurrentPos = GET_CAM_COORD(sData.sCameraData.sFPSCam.theCam)
		sData.sCameraData.vCamCurrentRot = GET_CAM_ROT(sData.sCameraData.sFPSCam.theCam)
		sData.sCameraData.sFPSCam.fCamFOVCurrent = GET_CAM_FOV(sData.sCameraData.sFPSCam.theCam)
		
		sData.sCameraData.sFPSCam.vCamRotOffsetCurrent = <<0.0, 0.0, 0.0>>
		
		sData.sCameraData.iTransitionSpeed = 750 // STC
				
		IF IS_SHAKE_ENABLED()
			SHAKE_CAM(sData.sCameraData.sFPSCam.theCam, GET_SHAKE_NAME(), GET_SHAKE_AMPLITUDE())
		ENDIF
		
		SET_CAM_PARAMS(	sData.sCameraData.sFPSCam.theCam, 
						sData.sCameraData.vCamCurrentPos,
						sData.sCameraData.vCamCurrentRot, 
						sData.sCameraData.sFPSCam.fCamFOVCurrent, 
						0)
						
		SET_CAM_PARAMS(	sData.sCameraData.sFPSCam.theCam, 
						sData.sCameraData.vCamTargetPos, 
						sData.sCameraData.vCamTargetRot, 
						sData.sCameraData.sFPSCam.fCamFOVTarget, 
						sData.sCameraData.iTransitionSpeed, 
						GRAPH_TYPE_SIN_ACCEL_DECEL)
		
		CDEBUG3LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "   ... Current Pos: ", sData.sCameraData.vCamCurrentPos)
		CDEBUG3LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "   ... Current Rot: ", sData.sCameraData.vCamCurrentRot)
		CDEBUG3LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "   ... Target Pos: ", sData.sCameraData.vCamTargetPos)
		CDEBUG3LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "   ... Target Rot: ", sData.sCameraData.vCamTargetRot)
		CDEBUG3LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "   ... Pos Offset: ", vPosOffset)
		CDEBUG3LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "   ... Rot Offset: ", vRotOffset)
		
		
	ENDIF
	
	IF sData.sCameraData.bTransitioning 
	
		sData.sCameraData.sFPSCam.fCamFOVCurrent = GET_CAMERA_FOV_FOR_POSITION_ID(sData)
	
		IF NOT IS_CAM_INTERPOLATING(sData.sCameraData.sFPSCam.theCam)
			
			sData.sCameraData.vCamCurrentPos = sData.sCameraData.vCamTargetPos
			sData.sCameraData.vCamCurrentRot = sData.sCameraData.vCamTargetRot
			sData.sCameraData.sFPSCam.vInitCamPos = sData.sCameraData.vCamTargetPos
			sData.sCameraData.sFPSCam.vInitCamRot = sData.sCameraData.vCamTargetRot
			
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | UPDATE_CAMERA - Finished transition:")
			CDEBUG3LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "   ... Current Pos: ", sData.sCameraData.vCamCurrentPos)
			CDEBUG3LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "   ... Current Rot: ", sData.sCameraData.vCamCurrentRot)
			
			IF IS_SHAKE_ENABLED()
				SHAKE_CAM(sData.sCameraData.sFPSCam.theCam, GET_SHAKE_NAME(), GET_SHAKE_AMPLITUDE())
			ENDIF
			
			sData.sCameraData.bTransitioning = FALSE
			
			SET_FPS_CAM_FAR(sData, sData.sCameraData.sFPSCam.fCamFOVCurrent)
			
			UPDATE_FIRST_PERSON_CAMERA(sData.sCameraData.sFPSCam, FALSE, FALSE, TRUE)
			
		ENDIF
	ELSE
		UPDATE_FIRST_PERSON_CAMERA(sData.sCameraData.sFPSCam, IS_ZOOM_AVAILABLE(), IS_STICK_LOOK_AVAILABLE(), IS_ZOOM_IN_ONLY(), DEFAULT, DEFAULT, DEFAULT, DEFAULT, USE_TRIGGERS_TO_ZOOM())
	ENDIF
	
//	IF NETWORK_IS_ACTIVITY_SESSION()
//	AND IS_CORONA_BIT_SET(CORONA_FADED_SCREEN_OUT)
//		IF NOT IS_SCREEN_FADED_IN()
//		AND NOT IS_SCREEN_FADING_IN()
//			DO_SCREEN_FADE_IN(500)
//		ENDIF
//		CLEAR_CORONA_BIT(CORONA_FADED_SCREEN_OUT)
//		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | UPDATE_CAMERA - Faded in screen.")
//	ENDIF
	
ENDPROC

PROC INITIALISE_CAMERA(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	
	sData.sCameraData.vBaseRotation.X = 0.0
	sData.sCameraData.vBaseRotation.Y = 0.0
	sData.sCameraData.vBaseRotation.Z = -180.0
	
	SET_CAMERA_POSITION(sData, NHPG_CAMLOCATION__PRIMARY)
	
	INIT_FIRST_PERSON_CAMERA(	sData.sCameraData.sFPSCam, 
								GET_CAMERA_POSITION_FOR_POSITION_ID(sData),
								GET_CAMERA_ROTATION_FOR_POSITION_ID(sData), // TODO: Re-add for rotation.
								GET_CAMERA_FOV_FOR_POSITION_ID(sData), // TODO: This needs to be done properly 
								DEFAULT, DEFAULT, DEFAULT, 10.0, TRUE, 0, -1, TRUE)

	IF IS_SHAKE_ENABLED()
		SHAKE_CAM(sData.sCameraData.sFPSCam.theCam, GET_SHAKE_NAME(), GET_SHAKE_AMPLITUDE())
	ENDIF

	SET_FPS_CAM_FAR(sData, GET_CAMERA_FOV_FOR_POSITION_ID(sData))

	SET_WIDESCREEN_BORDERS(TRUE, 0)
		
	sData.sCameraData.vCamCurrentPos = GET_CAMERA_POSITION_FOR_POSITION_ID(sData)
	sData.sCameraData.vCamCurrentRot = GET_CAMERA_ROTATION_FOR_POSITION_ID(sData)
	
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | INITIALISE_CAMERA - Started FPS camera.")
	CDEBUG3LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "   ... Cam Current Pos: ", sData.sCameraData.vCamCurrentPos)
	CDEBUG3LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "   ... Cam Current Rot: ", sData.sCameraData.vCamCurrentRot)
	CDEBUG3LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "   ... Base Rot: ", sData.sCameraData.vBaseRotation)
	
	IF NOT IS_SCREEN_FADED_IN()
	AND NOT IS_SCREEN_FADING_IN()
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | INITIALISE_CAMERA - Fading in.")
	ENDIF
ENDPROC

PROC CLEANUP_CAMERA(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)

	CLEAR_FIRST_PERSON_CAMERA(sData.sCameraData.sFPSCam)
	SET_WIDESCREEN_BORDERS(FALSE,0)	

	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CLEANUP_CAMERA - Cleared FPS camera.")
	
ENDPROC



