
#IF FEATURE_HEIST_ISLAND

#IF IS_DEBUG_BUILD

USING "net_heist_planning_generic_support.sch"


PROC CREATE_WIDGETS()

ENDPROC

PROC MAINTAIN_WIDGETS(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)

	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD1, KEYBOARD_MODIFIER_CTRL_SHIFT, "down")
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | MAINTAIN_WIDGETS - ctrl-shift numpad 1 pressed")
		IF sData.sCameraData.eRotOrder > EULER_XYZ
			sData.sCameraData.eRotOrder = INT_TO_ENUM(EULER_ROT_ORDER, ENUM_TO_INT(sData.sCameraData.eRotOrder)-1)
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | MAINTAIN_WIDGETS - sData.sCameraData.eRotOrder = ", ENUM_TO_INT(sData.sCameraData.eRotOrder))
		ENDIF
	ENDIF

	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD2, KEYBOARD_MODIFIER_CTRL_SHIFT, "up")
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | MAINTAIN_WIDGETS - ctrl-shift numpad 2 pressed")
		IF sData.sCameraData.eRotOrder < EULER_ZYX
			sData.sCameraData.eRotOrder = INT_TO_ENUM(EULER_ROT_ORDER, ENUM_TO_INT(sData.sCameraData.eRotOrder)+1)
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | MAINTAIN_WIDGETS - sData.sCameraData.eRotOrder = ", ENUM_TO_INT(sData.sCameraData.eRotOrder))
		ENDIF
	ENDIF
	
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_VerbosePlanningDebug")
	AND !sData.sScaleformData.bVerboseDebugging
		sData.sScaleformData.bVerboseDebugging = TRUE
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | MAINTAIN_WIDGETS - sData.sScaleformData.bVerboseDebugging = TRUE")
	ENDIF

ENDPROC

#ENDIF


#ENDIF

