
USING "net_heist_planning_generic_structs.sch"

USING "commands_network.sch"
USING "net_include.sch"
USING "net_FPS_cam.sch"
USING "MP_Scaleform_Functions.sch"
USING "hud_drawing.sch"
USING "net_render_targets.sch"
USING "net_simple_interior.sch"
USING "FMMC_Corona_Controller.SCH"

//----------------------
//	SCRIPT STAGE
//----------------------

FUNC INT GET_SCRIPT_STAGE()
	RETURN iScriptStage
ENDFUNC

PROC SET_SCRIPT_STAGE(INT iStage)
	IF iScriptStage != iStage
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | SET_SCRIPT_STAGE - Setting stage to ", iStage)
	ENDIF
	iScriptStage = iStage
ENDPROC

//----------------------
//	HELP TEXT
//----------------------

FUNC STRING GET_HEIST_BOARD_HELP_STRING(HEIST_BOARD_HELP eHelp)
	STRING strReturn = ""

	IF logic.HelpText.Label != NULL
		strReturn = CALL logic.HelpText.Label(eHelp)
	ENDIF

	IF NOT IS_STRING_NULL_OR_EMPTY(strReturn)
		RETURN strReturn
	ENDIF

	SWITCH eHelp
		CASE HBH_BECOME_BOSS			RETURN "NHPG_HELP_BBOSS"
		CASE HBH_BECOME_GOON			RETURN "NHPG_HELP_BGOON"
	ENDSWITCH
	
	RETURN strReturn
ENDFUNC

PROC PRINT_HEIST_BOARD_HELP(HEIST_BOARD_HELP eHelp)
	STRING strHelp = GET_HEIST_BOARD_HELP_STRING(eHelp)
	PRINT_HELP(strHelp)
ENDPROC

PROC CLEAR_HEIST_BOARD_HELP(HEIST_BOARD_HELP eHelp)
	STRING strHelp = GET_HEIST_BOARD_HELP_STRING(eHelp)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(strHelp)
		CLEAR_HELP()
	ENDIF
ENDPROC

PROC CLEAR_ALL_HEIST_BOARD_HELP()
	HEIST_BOARD_HELP eHelp
	REPEAT HBH_MAX eHelp
		CLEAR_HEIST_BOARD_HELP(eHelp)
	ENDREPEAT
ENDPROC

//----------------------
//	HELPER METHODS
//----------------------

PROC HEIST_BOARD_SET_PLAYER_CONTROL(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, BOOL bEnableControl)
	IF bEnableControl
		IF sData.bPlayerControlRemoved
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			sData.bPlayerControlRemoved = FALSE
		ENDIF
	ELSE
		IF !sData.bPlayerControlRemoved
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			sData.bPlayerControlRemoved = TRUE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_INTERACTING_SET(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	RETURN sData.sStateData.bInteract
ENDFUNC

PROC START_INTERACTION(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | START_INTERACTION - called")
	DEBUG_PRINTCALLSTACK()
	sData.sStateData.bInteract = TRUE
ENDPROC

PROC END_INTERACTION(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | END_INTERACTION - called")
	DEBUG_PRINTCALLSTACK()
	sData.sStateData.bInteract = FALSE
ENDPROC

// ---

FUNC BOOL IS_EDITIING_SET(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	RETURN (sData.sStateData.eSubState = NHPG_BOARDSUBSTATE__EDITING)
ENDFUNC

PROC START_EDITING(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | START_EDITING - called")
	DEBUG_PRINTCALLSTACK()
	sData.sStateData.eSubState = NHPG_BOARDSUBSTATE__EDITING
ENDPROC

PROC END_EDITING(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | END_EDITING - called")
	DEBUG_PRINTCALLSTACK()
	sData.sStateData.eSubState = NHPG_BOARDSUBSTATE__VIEWING
ENDPROC

// ---

FUNC BOOL IS_HIGHLIGHT_UPDATE_REQUIRED(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	RETURN sData.sCursorData.bUpdateHighlight
ENDFUNC

PROC SET_HIGHLIGHT_UPDATE_REQUIRED(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, BOOL bRequired)
	IF sData.sCursorData.bUpdateHighlight != bRequired
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | SET_HIGHLIGHT_UPDATE_REQUIRED - bRequired = ", GET_STRING_FROM_BOOL(bRequired))
		DEBUG_PRINTCALLSTACK()
	ENDIF
	sData.sCursorData.bUpdateHighlight = bRequired
ENDPROC

// ---

FUNC BOOL IS_INSTRUCTIONAL_BUTTON_UPDATE_REQUIRED(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	RETURN sData.sScaleformData.bUpdateInstructions
ENDFUNC

PROC SET_INSTRUCTIONAL_BUTTON_UPDATE_REQUIRED(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, BOOL bRequired)
	IF sData.sScaleformData.bUpdateInstructions != bRequired
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | SET_INSTRUCTIONAL_BUTTON_UPDATE_REQUIRED - bRequired = ", GET_STRING_FROM_BOOL(bRequired))
		DEBUG_PRINTCALLSTACK()
	ENDIF
	sData.sScaleformData.bUpdateInstructions = bRequired
ENDPROC

// ---

FUNC BOOL IS_INPUT_BUFFER_SET(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	RETURN sData.sInputData.bInputBuffer
ENDFUNC

PROC SET_INPUT_BUFFER(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, BOOL bOn)
	IF sData.sInputData.bInputBuffer != bOn
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | SET_INPUT_BUFFER - bOn = ", GET_STRING_FROM_BOOL(bOn))
		DEBUG_PRINTCALLSTACK()
	ENDIF
	sData.sInputData.bInputBuffer = bOn
ENDPROC

//----------------------
//	STATE MACHINE
//----------------------

FUNC STRING NET_HEIST_PLANNING_GENERIC_BOARD_STATE_TO_STRING(NET_HEIST_PLANNING_GENERIC_BOARD_STATE eState)
	SWITCH eState
		CASE NHPG_BOARDSTATE__IDLE						RETURN "NHPG_BOARDSTATE__IDLE"
		CASE NHPG_BOARDSTATE__INIT						RETURN "NHPG_BOARDSTATE__INIT"
		CASE NHPG_BOARDSTATE__LOADING					RETURN "NHPG_BOARDSTATE__LOADING"
		CASE NHPG_BOARDSTATE__DISPLAYING				RETURN "NHPG_BOARDSTATE__DISPLAYING"
		CASE NHPG_BOARDSTATE__INUSE						RETURN "NHPG_BOARDSTATE__INUSE"
		CASE NHPG_BOARDSTATE__CLEANUP					RETURN "NHPG_BOARDSTATE__CLEANUP"
		CASE NHPG_BOARDSTATE__FINISHED					RETURN "NHPG_BOARDSTATE__FINISHED"
		CASE NHPG_BOARDSTATE__MAX						RETURN "NHPG_BOARDSTATE__MAX"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC BOOL HAS_HEIST_BOARD_STATE_RUN_ENTRY(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	RETURN IS_BIT_SET(sData.sStateData.bsStateEntry, ENUM_TO_INT(sData.sStateData.eState))
ENDFUNC

PROC SET_HEIST_BOARD_STATE_HAS_RUN_ENTRY(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	SET_BIT(sData.sStateData.bsStateEntry, ENUM_TO_INT(sData.sStateData.eState))
ENDPROC

FUNC BOOL HAS_HEIST_BOARD_STATE_RUN_EXIT(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	RETURN NOT IS_BIT_SET(sData.sStateData.bsStateExit, ENUM_TO_INT(sData.sStateData.eCachedState))
ENDFUNC

PROC SET_HEIST_BOARD_STATE_HAS_RUN_EXIT(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	CLEAR_BIT(sData.sStateData.bsStateExit, ENUM_TO_INT(sData.sStateData.eCachedState))
ENDPROC

PROC HEIST_BOARD_STATE_PROGRESS(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	
	sData.sStateData.eCachedState = sData.sStateData.eState
		
	SET_BIT(sData.sStateData.bsStateExit, ENUM_TO_INT(sData.sStateData.eCachedState))
	CLEAR_BIT(sData.sStateData.bsStateEntry, ENUM_TO_INT(sData.sStateData.eCachedState))
		
	INT iState = ENUM_TO_INT(sData.sStateData.eState)
	iState++
	sData.sStateData.eState = INT_TO_ENUM(NET_HEIST_PLANNING_GENERIC_BOARD_STATE, iState)
	
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | HEIST_BOARD_STATE_PROGRESS - Move State (Progress) - Prev: ",NET_HEIST_PLANNING_GENERIC_BOARD_STATE_TO_STRING(sData.sStateData.eCachedState),
		", Next: ", NET_HEIST_PLANNING_GENERIC_BOARD_STATE_TO_STRING(sData.sStateData.eState))
	
	DEBUG_PRINTCALLSTACK()
	
	// Check to make sure we haven't reached the max state.
	IF sData.sStateData.eState = NHPG_BOARDSTATE__MAX
		sData.sStateData.eState = NHPG_BOARDSTATE__CLEANUP
	ENDIF
ENDPROC

PROC HEIST_BOARD_STATE_REGRESS(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	
	sData.sStateData.eCachedState = sData.sStateData.eState
		
	SET_BIT(sData.sStateData.bsStateExit, ENUM_TO_INT(sData.sStateData.eCachedState))
	CLEAR_BIT(sData.sStateData.bsStateEntry, ENUM_TO_INT(sData.sStateData.eCachedState))
		
	INT iState = ENUM_TO_INT(sData.sStateData.eState)
	iState--
	sData.sStateData.eState = INT_TO_ENUM(NET_HEIST_PLANNING_GENERIC_BOARD_STATE, iState)
	
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | HEIST_BOARD_STATE_REGRESS - Move State (Regress) - Prev: ",NET_HEIST_PLANNING_GENERIC_BOARD_STATE_TO_STRING(sData.sStateData.eCachedState),
		", Next: ", NET_HEIST_PLANNING_GENERIC_BOARD_STATE_TO_STRING(sData.sStateData.eState))
	
	DEBUG_PRINTCALLSTACK()
	
	// Check to make sure we haven't reached the min state.
	IF sData.sStateData.eState = NHPG_BOARDSTATE__IDLE
		sData.sStateData.eState = NHPG_BOARDSTATE__INIT
	ENDIF
ENDPROC

PROC HEIST_BOARD_SET_STATE(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, NET_HEIST_PLANNING_GENERIC_BOARD_STATE eState)
	IF sData.sStateData.eState != eState
		sData.sStateData.eCachedState = sData.sStateData.eState
			
		SET_BIT(sData.sStateData.bsStateExit, ENUM_TO_INT(sData.sStateData.eCachedState))
		CLEAR_BIT(sData.sStateData.bsStateEntry, ENUM_TO_INT(sData.sStateData.eCachedState))
			
		sData.sStateData.eState = eState
		
		DEBUG_PRINTCALLSTACK()
		
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | STATE MACHINE - Set State - Prev: ", NET_HEIST_PLANNING_GENERIC_BOARD_STATE_TO_STRING(sData.sStateData.eCachedState),
			", Next: ", NET_HEIST_PLANNING_GENERIC_BOARD_STATE_TO_STRING(sData.sStateData.eState))
		
		// Check to make sure we haven't reached the max state.
		IF sData.sStateData.eState = NHPG_BOARDSTATE__MAX
			sData.sStateData.eState = NHPG_BOARDSTATE__CLEANUP
		ENDIF
	ENDIF
ENDPROC

//----------------------
//	WARNING SCREEN
//----------------------

FUNC BOOL RUN_HEIST_BOARD_WARNING_SCREEN(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, INT iWarningScreen, BOOL &bConfirm)
	UNUSED_PARAMETER(sData)

	// Call this every frame
	TEXT_LABEL_63 tlWarning, tlWarningText, tlSubLabel, tlSubLabel2, tlWarningSubText
	BOOL bInsertNumber = FALSE
	INT iNumberToInsert
	FE_WARNING_FLAGS feFlags = (FE_WARNING_YES | FE_WARNING_NO)
	
	tlWarning = "HPWARN_CONF"

	SWITCH iWarningScreen
		
		CASE HEIST_BOARD_WARNING_SCREEN__PAY_FOR_SETUP
			tlWarningText = CALL logic.HeistStatus.WarningText()
			tlWarningSubText = "HEIST_WARN_2"
			bInsertNumber = TRUE
			iNumberToInsert = CALL logic.HeistStatus.SetupCost()
		BREAK
		
		CASE HEIST_BOARD_WARNING_SCREEN__QUIT_FINALE
			tlWarning = "FM_CSC_QUIT"
			tlWarningText = "FM_CSC_QUIT1"
		BREAK
		
		CASE HEIST_BOARD_WARNING_SCREEN__CUSTOM
			IF logic.WarningScreen.Warning != NULL
				tlWarning = CALL logic.WarningScreen.Warning(sData)
			ENDIF
			IF logic.WarningScreen.WarningText != NULL
				tlWarningText = CALL logic.WarningScreen.WarningText(sData)
			ENDIF
			IF logic.WarningScreen.Flags != NULL
				feFlags = CALL logic.WarningScreen.Flags(sData)
			ENDIF
			IF logic.WarningScreen.Subtext != NULL
				tlWarningSubText = CALL logic.WarningScreen.Subtext(sData)
			ENDIF
			IF logic.WarningScreen.ShouldInsertNumber != NULL
				bInsertNumber = CALL logic.WarningScreen.ShouldInsertNumber(sData)
			ENDIF
			IF logic.WarningScreen.Number != NULL
				iNumberToInsert = CALL logic.WarningScreen.Number(sData)
			ENDIF
			IF logic.WarningScreen.SubLabelOne != NULL
				tlSubLabel = CALL logic.WarningScreen.SubLabelOne(sData)
			ENDIF
			IF logic.WarningScreen.SubLabelTwo != NULL
				tlSubLabel2 = CALL logic.WarningScreen.SubLabelTwo(sData)
			ENDIF
		BREAK
		
	ENDSWITCH
	
	SET_WARNING_MESSAGE_WITH_HEADER(tlWarning, tlWarningText, feFlags, tlWarningSubText, bInsertNumber, iNumberToInsert, tlSubLabel, tlSubLabel2)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | RUN_HEIST_BOARD_WARNING_SCREEN - SET_WARNING_MESSAGE_WITH_HEADER")
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
	ENDIF
	
	// Cancel quit
	IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | RUN_HEIST_BOARD_WARNING_SCREEN - Pressed cancel ")
							
		bConfirm = FALSE
		
		RETURN TRUE
	ENDIF
	
	// Confirm 
	IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | RUN_HEIST_BOARD_WARNING_SCREEN - Pressed confirm ")
										
		bConfirm = TRUE
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_WARNING(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	IF sData.iWarningScreen = HEIST_BOARD_WARNING_SCREEN__NONE
		EXIT
	ENDIF
	
	BOOL bConfirm
	IF RUN_HEIST_BOARD_WARNING_SCREEN(sData, sData.iWarningScreen, bConfirm)
		IF bConfirm
			SWITCH sData.iWarningScreen
				CASE HEIST_BOARD_WARNING_SCREEN__QUIT_FINALE
					CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | UPDATE_WARNING - Confirmed quit from finale, GlobalPlayerBD_NetHeistPlanningGeneric[NETWORK_PLAYER_ID_TO_INT()].bQuitFromFinaleScreen = TRUE")
					GlobalPlayerBD_NetHeistPlanningGeneric[NETWORK_PLAYER_ID_TO_INT()].bQuitFromFinaleScreen = TRUE
				BREAK
				
				CASE HEIST_BOARD_WARNING_SCREEN__CUSTOM
					CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | UPDATE_WARNING - Confirmed on custom warning screen!")
					IF logic.WarningScreen.OnAccept != NULL
						CALL logic.WarningScreen.OnAccept(sData)
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			SWITCH sData.iWarningScreen
				CASE HEIST_BOARD_WARNING_SCREEN__CUSTOM
					CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | UPDATE_WARNING - Canceled on custom warning screen!")
					IF logic.WarningScreen.OnDecline != NULL
						CALL logic.WarningScreen.OnDecline(sData)
					ENDIF
				BREAK
				
				DEFAULT
					CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | UPDATE_WARNING - Canceled warning screen, do nothing ")
				BREAK
			ENDSWITCH
			
		ENDIF
		sData.iWarningScreen = HEIST_BOARD_WARNING_SCREEN__NONE
		sData.iCustomWarningID = HEIST_BOARD_WARNING_SCREEN__NONE
		SET_INPUT_BUFFER(sData, TRUE)
	ENDIF
ENDPROC

//----------------------
//	INTERACTION
//----------------------

FUNC BOOL IS_PAUSE_MENU_BEING_ACTIONED()
	BOOL bPausePressed = (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE) OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE))
								
	IF bPausePressed
		bDisablePlanningScreenShortly = TRUE
		PRINTLN("IS_PAUSE_MENU_BEING_ACTIONED - bPausePressed - Disable planning screen")
		RETURN TRUE
	ELSE
		IF bDisablePlanningScreenShortly
			PRINTLN("IS_PAUSE_MENU_BEING_ACTIONED - bDisablePlanningScreenShortly - Disable planning screen")
			IF HAS_NET_TIMER_EXPIRED(stDisablePlanningScreenTimer, 1000)
				RESET_NET_TIMER(stDisablePlanningScreenTimer)
				bDisablePlanningScreenShortly = FALSE
				PRINTLN("IS_PAUSE_MENU_BEING_ACTIONED - bDisablePlanningScreenShortly - Timer expired, can be used again")
				RETURN FALSE
			ELSE
				PRINTLN("IS_PAUSE_MENU_BEING_ACTIONED - bDisablePlanningScreenShortly - Waiting for timer to expire")
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_BOARD_ONLY_BE_USED_BY_LEADER(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	SWITCH sData.eBoardType
		CASE NHPG_BOARD__HEIST_ISLAND
		CASE NHPG_BOARD__TUNER
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_A_FAILED_TO_LAUNCH_HELP_DISPLAYING()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H4PREP_UNVLB")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_USE_BOARD(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)

	#IF IS_DEBUG_BUILD
	BOOL bInteractionLogging = GET_COMMANDLINE_PARAM_EXISTS("sc_EnableNHPGInteractionLogging")
	#ENDIF

	#IF IS_DEBUG_BUILD
	IF sData.sInputData.bBlockInteraction
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - interaction blocked via widget")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF sData.iSetupStage > HEIST_BOARD_SETUP_STAGE_PROMPT
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - setup in progress")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NATIVE_TO_INT(sData.piLeader) != -1
	AND GlobalPlayerBD_NetHeistPlanningGeneric[NATIVE_TO_INT(sData.piLeader)].bLaunchedPrepFromBoard
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - leader has launched a heist prep from a NHPG planning board/screen")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - NOT IS_NET_PLAYER_OK")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF CAN_BOARD_ONLY_BE_USED_BY_LEADER(sData)
	AND sData.piLeader != PLAYER_ID()
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - Player is not the leader")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sData.iBitSet, NHPG_BITSET_LAUNCHED_MISSION)
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - A mission has been launched from the board")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sData.iBitSet, NHPG_BITSET_LAUNCH_FINALE)
	OR IS_BIT_SET(sData.iBitSet, NHPG_BITSET_LAUNCHED_FINALE)
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - Finale mission has been launched from the board")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - This is an activity session.")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - A cutscene is playing")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_COMPLETED_INITIAL_GANG_BOSS_DATA_SETUP)
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - Initial gang boss data setup has not been completed")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_BEING_ACTIONED()
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - The pause menu is being actioned")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF

	IF IS_BROWSER_OPEN()
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - The browser is open")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
	AND GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - Player is permanent to boss work")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
			
	IF sData.sStateData.eState != NHPG_BOARDSTATE__DISPLAYING
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - A planning board is not in display state")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - Player is in a corona")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_TRANSITION_SESSION_LAUNCHING()
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - A transition session is launching.")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_TRANSITION_SESSION_RESTARTING()
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - A transition session is restarting")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
			
	IF GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_NPC_INVITE
	OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_INVITE_FROM_MP
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - Accepted invite to job")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - Phone on screen")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF logic.Interaction.CanInteract != NULL
		IF NOT CALL logic.Interaction.CanInteract()
			#IF IS_DEBUG_BUILD
			IF bInteractionLogging
				CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - logic.Interaction.CanInteract() = FALSE")
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_A_FAILED_TO_LAUNCH_HELP_DISPLAYING()
		#IF IS_DEBUG_BUILD
		IF bInteractionLogging
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CAN_USE_BOARD - FALSE - A 'failed to launch' help message is being displayed.")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_PLAYER_TRIGGERED_INTERACTION()
	STRING strPrompt = "NHPG_PROMPT"
	IF logic.Interaction.Prompt != NULL
		strPrompt = CALL logic.Interaction.Prompt()
	ENDIF
	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(strPrompt)
		PRINT_HELP_FOREVER(strPrompt)
	ENDIF
	RETURN IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
ENDFUNC

PROC CLEAR_INTERACT_HELP()
	STRING strPrompt = "NHPG_PROMPT"
	IF logic.Interaction.Prompt != NULL
		strPrompt = CALL logic.Interaction.Prompt()
	ENDIF
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(strPrompt)
		CLEAR_HELP()
	ENDIF
ENDPROC

FUNC BOOL IS_HEIST_ON_COOLDOWN()
	IF logic.HeistStatus.OnCooldown != NULL
		RETURN CALL logic.HeistStatus.OnCooldown()
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SHOW_COOLDOWN_HELP()
	IF logic.HeistStatus.OnCooldownHelp != NULL
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(CALL logic.HeistStatus.OnCooldownHelp())
			PRINT_HELP_FOREVER(CALL logic.HeistStatus.OnCooldownHelp())
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_COOLDOWN_HELP()
	IF logic.HeistStatus.OnCooldownHelp != NULL
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(CALL logic.HeistStatus.OnCooldownHelp())
			CLEAR_HELP()
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL IS_HEIST_SETUP_REQUIRED(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	IF sData.piLeader = PLAYER_ID()
		IF CALL logic.HeistStatus.SetupRequired()
		AND NOT CALL logic.HeistStatus.IsActive()
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_AFFORD_HEIST_SETUP(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	IF sData.piLeader = PLAYER_ID()
		IF logic.HeistStatus.SetupCost != NULL
		AND NETWORK_CAN_SPEND_MONEY(CALL logic.HeistStatus.SetupCost(), FALSE, TRUE, FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SHOW_INSUFFICIENT_FUNDS_HELP()
	IF logic.HeistStatus.NotEnoughCashForSetupHelp != NULL
	AND logic.HeistStatus.SetupCost != NULL
		STRING strHelp = CALL logic.HeistStatus.NotEnoughCashForSetupHelp()
		INT iNumber = CALL logic.HeistStatus.SetupCost()
		IF NOT IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED(strHelp, iNumber)
			PRINT_HELP_FOREVER_WITH_NUMBER(strHelp, iNumber)
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_INSUFFICIENT_FUNDS_HELP()
	IF logic.HeistStatus.NotEnoughCashForSetupHelp != NULL
	AND logic.HeistStatus.SetupCost != NULL
		IF IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED(CALL logic.HeistStatus.NotEnoughCashForSetupHelp(), CALL logic.HeistStatus.SetupCost())
			CLEAR_HELP()
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_SETUP_HELP()
	IF logic.HeistStatus.SetupContext != NULL
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(CALL logic.HeistStatus.SetupContext())
			CLEAR_HELP()
		ENDIF
	ENDIF
ENDPROC

PROC PROMPT_SETUP_HEIST(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	STRING strSetupContext = CALL logic.HeistStatus.SetupContext()
	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(strSetupContext)
		PRINT_HELP_FOREVER(strSetupContext)
	ENDIF
	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
		CLEAR_SETUP_HELP()
		HEIST_BOARD_SET_PLAYER_CONTROL(sData, FALSE)
		sData.iSetupStage = HEIST_BOARD_SETUP_STAGE_WARNING
	ENDIF
ENDPROC

PROC MAINTAIN_SETUP_HEIST(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	SWITCH sData.iSetupStage
		CASE HEIST_BOARD_SETUP_STAGE_PROMPT
			// Do nothing in here
		BREAK
		CASE HEIST_BOARD_SETUP_STAGE_WARNING
			BOOL bConfirm
			IF RUN_HEIST_BOARD_WARNING_SCREEN(sData, HEIST_BOARD_WARNING_SCREEN__PAY_FOR_SETUP, bConfirm)
				IF bConfirm
					CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | MAINTAIN_SETUP_HEIST - bConfirm = TRUE, move to payment")
					sData.iSetupStage = HEIST_BOARD_SETUP_STAGE_PAYMENT
				ELSE
					CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | MAINTAIN_SETUP_HEIST - bConfirm = FALSE, move back to prompt")
					sData.iSetupStage = HEIST_BOARD_SETUP_STAGE_PROMPT
				ENDIF
				HEIST_BOARD_SET_PLAYER_CONTROL(sData, TRUE)
			ENDIF
		BREAK
		CASE HEIST_BOARD_SETUP_STAGE_PAYMENT
			// TO DO
			IF CALL logic.HeistStatus.PayForSetup()
				CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | MAINTAIN_SETUP_HEIST - Payment taken, move to activate")
				sData.iSetupStage = HEIST_BOARD_SETUP_STAGE_ACTIVATE
			ENDIF
		BREAK
		CASE HEIST_BOARD_SETUP_STAGE_ACTIVATE
			IF NOT CALL logic.HeistStatus.IsActive()
				CALL logic.HeistStatus.SetActive()
				
				CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | MAINTAIN_SETUP_HEIST - Heist activated, move to done")
				sData.iSetupStage = HEIST_BOARD_SETUP_STAGE_DONE
			ELSE
				CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | MAINTAIN_SETUP_HEIST - Heist already activated (may cause issues), move to done")
				sData.iSetupStage = HEIST_BOARD_SETUP_STAGE_DONE
			ENDIF
		BREAK
		CASE HEIST_BOARD_SETUP_STAGE_DONE
			IF logic.Interaction.OnStart != NULL
				CALL logic.Interaction.OnStart(sData)
			ELSE
				START_INTERACTION(sData)
			ENDIF
			
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | MAINTAIN_SETUP_HEIST - Done... done! Move back to prompt")
			sData.iSetupStage = HEIST_BOARD_SETUP_STAGE_PROMPT
		BREAK
	ENDSWITCH
ENDPROC


PROC MAINTAIN_INTERACTION(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	
	MAINTAIN_SETUP_HEIST(sData)
	
	IF NOT CAN_USE_BOARD(sData)
		CLEAR_INTERACT_HELP()
		CLEAR_COOLDOWN_HELP()
		CLEAR_SETUP_HELP()
		CLEAR_INSUFFICIENT_FUNDS_HELP()
		EXIT
	ENDIF
	
	IF sData.piLeader = PLAYER_ID()
		IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			PRINT_HEIST_BOARD_HELP(HBH_BECOME_BOSS)
			EXIT
		ENDIF		
	ELIF NOT CAN_BOARD_ONLY_BE_USED_BY_LEADER(sData)
		IF NOT GB_IS_PLAYER_BOSS_OF_A_GANG(sData.piLeader)
			PRINT_HEIST_BOARD_HELP(HBH_BECOME_GOON)
			EXIT
		ENDIF		
	ENDIF
	
	IF IS_HEIST_SETUP_REQUIRED(sData)
		IF IS_HEIST_ON_COOLDOWN()
			SHOW_COOLDOWN_HELP()
		ELIF NOT CAN_PLAYER_AFFORD_HEIST_SETUP(sData)
			SHOW_INSUFFICIENT_FUNDS_HELP()
		ELSE
			PROMPT_SETUP_HEIST(sData)
		ENDIF
		EXIT
	ENDIF
	
	IF HAS_PLAYER_TRIGGERED_INTERACTION()
		SET_INPUT_BUFFER(sData, TRUE)
		CLEAR_INTERACT_HELP()
		
		IF logic.Interaction.OnStart != NULL
			CALL logic.Interaction.OnStart(sData)
		ELSE
			START_INTERACTION(sData)
		ENDIF
	ENDIF
	
ENDPROC


//----------------------
//	DIALOGUE
//----------------------

FUNC BOOL IS_DIALOGUE_ENABLED()

	IF logic.Dialogue.Enable != NULL
		IF NOT CALL logic.Dialogue.Enable()
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL IS_DIALOGUE_TRIGGERED_SET(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, NET_HEIST_PLANNING_GENERIC_DIALOGUE eDialogue)
	UNUSED_PARAMETER(sData)
	RETURN IS_BIT_SET(sDialogueData.iDialogueDoneBitSet[(ENUM_TO_INT(eDialogue) / 32)], (ENUM_TO_INT(eDialogue) % 32))
ENDFUNC

PROC SET_DIALOGUE_HAS_TRIGGERED(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, NET_HEIST_PLANNING_GENERIC_DIALOGUE eDialogue)
	UNUSED_PARAMETER(sData)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | [DIALOGUE] SET_DIALOGUE_HAS_TRIGGERED - Setting dialogue ", eDialogue, " as done.")
	SET_BIT(sDialogueData.iDialogueDoneBitSet[(ENUM_TO_INT(eDialogue) / 32)], (ENUM_TO_INT(eDialogue) % 32))
	
	IF logic.Dialogue.OnTriggered != NULL
		CALL logic.Dialogue.OnTriggered(eDialogue)
	ENDIF
ENDPROC

FUNC BOOL IS_DIALOGUE_TRIGGER_SET(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, NET_HEIST_PLANNING_GENERIC_DIALOGUE eDialogue)
	UNUSED_PARAMETER(sData)
	RETURN IS_BIT_SET(sDialogueData.iDialogueTriggerBitSet[(ENUM_TO_INT(eDialogue) / 32)], (ENUM_TO_INT(eDialogue) % 32))
ENDFUNC

FUNC STRING GET_DIALOGUE_ROOT(NET_HEIST_PLANNING_GENERIC_DIALOGUE eDialogue)
	IF logic.Dialogue.Root != NULL
		RETURN CALL logic.Dialogue.Root(eDialogue)
	ENDIF
	RETURN ""
ENDFUNC

FUNC STRING GET_DIALOGUE_BLOCK(NET_HEIST_PLANNING_GENERIC_DIALOGUE eDialogue)
	IF logic.Dialogue.Block != NULL
		RETURN CALL logic.Dialogue.Block(eDialogue)
	ENDIF
	RETURN ""
ENDFUNC

FUNC INT GET_DIALOGUE_SPEAKER_ID(NET_HEIST_PLANNING_GENERIC_DIALOGUE eDialogue)
	IF logic.Dialogue.SpeakerID != NULL
		RETURN CALL logic.Dialogue.SpeakerID(eDialogue)
	ENDIF
	RETURN -1
ENDFUNC

FUNC PED_INDEX GET_DIALOGUE_PED_ID(NET_HEIST_PLANNING_GENERIC_DIALOGUE eDialogue)
	IF logic.Dialogue.PedID != NULL
		RETURN CALL logic.Dialogue.PedID(eDialogue)
	ENDIF
	RETURN NULL
ENDFUNC

FUNC STRING GET_DIALOGUE_VOICE_ID(NET_HEIST_PLANNING_GENERIC_DIALOGUE eDialogue)
	IF logic.Dialogue.VoiceID != NULL
		RETURN CALL logic.Dialogue.VoiceID(eDialogue)
	ENDIF
	RETURN ""
ENDFUNC

FUNC BOOL PLAY_DIALOGUE(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, NET_HEIST_PLANNING_GENERIC_DIALOGUE eDialogue)
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		TEXT_LABEL_15 dialogueLabel = GET_DIALOGUE_ROOT(eDialogue)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(dialogueLabel)
		
			ADD_PED_FOR_DIALOGUE(sDialogueData.sPedStruct, GET_DIALOGUE_SPEAKER_ID(eDialogue), GET_DIALOGUE_PED_ID(eDialogue), GET_DIALOGUE_VOICE_ID(eDialogue))
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | [DIALOGUE] PLAY_DIALOGUE - ADD_PED_FOR_DIALOGUE - speaker: ",GET_DIALOGUE_SPEAKER_ID(eDialogue),", voice: ", GET_DIALOGUE_VOICE_ID(eDialogue))
			
			IF CREATE_CONVERSATION(sDialogueData.sPedStruct, GET_DIALOGUE_BLOCK(eDialogue), dialogueLabel, CONV_PRIORITY_VERY_HIGH)
				SET_DIALOGUE_HAS_TRIGGERED(sData, eDialogue)
				CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | [DIALOGUE] PLAY_DIALOGUE - Playing dialogue with CREATE_CONVERSATION: ", eDialogue, ", ",dialogueLabel)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC UPDATE_DIALOGUE(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	
	NET_HEIST_PLANNING_GENERIC_DIALOGUE eDialogue
	NET_HEIST_PLANNING_GENERIC_DIALOGUE eCurrentDialogue = NHPG_DIALOGUE__INVALID

	REPEAT NHPG_DIALOGUE__MAX eDialogue
		IF IS_DIALOGUE_TRIGGER_SET(sData, eDialogue)
		AND NOT IS_DIALOGUE_TRIGGERED_SET(sData, eDialogue)
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | [DIALOGUE] UPDATE_DIALOGUE - Dialogue that should trigger: ", eDialogue)
			IF PLAY_DIALOGUE(sData, eDialogue)
				eCurrentDialogue = eDialogue
				BREAKLOOP
			ENDIF
		ELSE
			IF IS_DIALOGUE_TRIGGERED_SET(sData, eDialogue)
				TEXT_LABEL_23 tlCurrent = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				TEXT_LABEL_23 sRoot = GET_DIALOGUE_ROOT(eDialogue)
				IF NOT IS_STRING_NULL_OR_EMPTY(tlCurrent)
				AND ARE_STRINGS_EQUAL(tlCurrent, sRoot)
					eCurrentDialogue = eDialogue
					CDEBUG3LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | [DIALOGUE] UPDATE_DIALOGUE - ", sRoot, " (", ENUM_TO_INT(eCurrentDialogue),") is still playing")
					BREAKLOOP
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	IF sDialogueData.eDialogueCurrentlyPlaying != eCurrentDialogue		
		sDialogueData.eDialogueCurrentlyPlaying = eCurrentDialogue
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | [DIALOGUE] UPDATE_DIALOGUE - new currently playing dialogue: ", eCurrentDialogue)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF eCurrentDialogue != NHPG_DIALOGUE__INVALID
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_RIGHT, KEYBOARD_MODIFIER_NONE, "Skip Dialogue")
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | [DIALOGUE] UPDATE_DIALOGUE - Right pressed, killing dialogue.")
		ENDIF
	ENDIF
	#ENDIF
ENDPROC

//----------------------
//	FORCE LAUNCH
//----------------------

PROC UPDATE_FORCE_LAUNCH(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		EXIT
	ENDIF
	
	INT iLeader = NATIVE_TO_INT(sData.piLeader)
	
	IF iLeader = -1
		EXIT
	ENDIF
	
	IF logic.ForceLaunch.Enable = NULL
	OR NOT CALL logic.ForceLaunch.Enable(sData)
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(GlobalPlayerBD_NetHeistPlanningGeneric[iLeader].stFinaleLaunchTimer)
		IF sData.piLeader = PLAYER_ID()
			IF CALL logic.ForceLaunch.ShouldStart(sData)
				START_NET_TIMER(GlobalPlayerBD_NetHeistPlanningGeneric[iLeader].stFinaleLaunchTimer)
				CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | UPDATE_FORCE_LAUNCH - Started force launch timer.")
			ENDIF
		ENDIF
	ELSE
		sData.sFinaleData.iForceLaunchTime = (CALL logic.ForceLaunch.Time() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(GlobalPlayerBD_NetHeistPlanningGeneric[iLeader].stFinaleLaunchTimer))
		
		IF sData.sFinaleData.iForceLaunchTime > 0
			// Update instructional buttons once per second to update the timer
			IF sData.sFinaleData.iForceLaunchTime < (sData.sFinaleData.iLastSecondCached-1000)
				CALL logic.ForceLaunch.OnSecondsUpdate(sData)
				sData.sFinaleData.iLastSecondCached = sData.sFinaleData.iForceLaunchTime
			ENDIF
		ELSE
			sData.sFinaleData.iForceLaunchTime = 0
			
			CALL logic.ForceLaunch.OnLaunch(sData)
		ENDIF
	ENDIF

ENDPROC


//----------------------
//	HUD
//----------------------

PROC SET_ALL_REMOTE_PLAYERS_INVISIBILE()
	INT iPlayer
	PLAYER_INDEX playerID
	
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(iPlayer))
			playerID = INT_TO_PLAYERINDEX(iPlayer)
			IF playerID != INVALID_PLAYER_INDEX()
			AND playerID != PLAYER_ID()
				SET_PLAYER_INVISIBLE_LOCALLY(playerID)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC UPDATE_PLAYER_VISIBILITY()
	SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
	SET_ALL_REMOTE_PLAYERS_INVISIBILE()
ENDPROC

PROC HIDE_HUD()
	THEFEED_HIDE_THIS_FRAME()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
	INVALIDATE_IDLE_CAM()
	UPDATE_PLAYER_VISIBILITY()
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_MAP)
ENDPROC

//----------------------
//	PLAYER CONTROL
//----------------------

PROC TOGGLE_PLAYER_CONTROL(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, BOOL bOn, BOOL bForce = FALSE)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | TOGGLE_PLAYER_CONTROL - bOn = ", GET_STRING_FROM_BOOL(bOn),"; bForce = ", GET_STRING_FROM_BOOL(bForce))
	DEBUG_PRINTCALLSTACK()
	
	IF bOn
		IF (sData.bPlayerControlRemoved
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID()))
		OR bForce
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			sData.bPlayerControlRemoved = FALSE
		ENDIF
	ELSE
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		sData.bPlayerControlRemoved = TRUE
	ENDIF
ENDPROC

//----------------------
//	CLEANUP
//----------------------

PROC CLEANUP_SOUNDS(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	IF sData.sAudioData.bBackgroundAudioStarted
		IF sData.sAudioData.iBackgroundAudioSoundID != -1
			STOP_SOUND(sData.sAudioData.iBackgroundAudioSoundID)
			RELEASE_SOUND_ID(sData.sAudioData.iBackgroundAudioSoundID)
			sData.sAudioData.iBackgroundAudioSoundID = -1
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CLEANUP_SOUNDS - sData.sAudioData.iBackgroundAudioSoundID Cleared")
			sData.sAudioData.bBackgroundAudioStarted = FALSE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CLEANUP(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)

	IF IS_BIT_SET(sData.iBitSet, NHPG_BITSET_CLEANUP)
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | SHOULD_CLEANUP - NHPG_BITSET_CLEANUP is set.")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sData.iBitSet, NHPG_BITSET_CLEANUP_FOR_WALK_OUT)
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | SHOULD_CLEANUP - NHPG_BITSET_CLEANUP_FOR_WALK_OUT is set.")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT()
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | SHOULD_CLEANUP - SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT()")
		RETURN TRUE
	ENDIF

	IF logic.Cleanup.ShouldCleanup != NULL
		IF CALL logic.Cleanup.ShouldCleanup()
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | SHOULD_CLEANUP - logic.Cleanup.ShouldCleanup() = TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CLEANUP_IMMEDIATELY()
	IF !NETWORK_IS_GAME_IN_PROGRESS()
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | SHOULD_CLEANUP_IMMEDIATELY - !NETWORK_IS_GAME_IN_PROGRESS()")
		RETURN TRUE
	ENDIF
	
	IF (IS_TRANSITION_SESSION_LAUNCHING() AND GET_SKYFREEZE_STAGE() = SKYFREEZE_FROZEN)
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | SHOULD_CLEANUP_IMMEDIATELY - (IS_TRANSITION_SESSION_LAUNCHING() AND GET_SKYFREEZE_STAGE() = SKYFREEZE_FROZEN)")
		RETURN TRUE
	ENDIF
	
	IF g_sNetHeistPlanningGenericClientData.bCleanupImmediately
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | SHOULD_CLEANUP_IMMEDIATELY - g_sNetHeistPlanningGenericClientData.bCleanupImmediately = TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_SCALEFORM_DATA(NET_HEIST_PLANNING_GENERIC_SCALEFORM_DATA &sData)
	NET_HEIST_PLANNING_GENERIC_SCALEFORM_DATA data
	sData = data
ENDPROC

PROC CLEANUP_FSM_DATA(NET_HEIST_PLANNING_GENERIC_FSM_DATA &sData)
	NET_HEIST_PLANNING_GENERIC_FSM_DATA data
	sData = data
ENDPROC

PROC CLEANUP_POSITION_DATA(NET_HEIST_PLANNING_GENERIC_POSITION_DATA &sData)
	NET_HEIST_PLANNING_GENERIC_POSITION_DATA data
	sData = data
ENDPROC

PROC CLEANUP_CAMERA_DATA(NET_HEIST_PLANNING_GENERIC_CAMERA_DATA &sData)
	NET_HEIST_PLANNING_GENERIC_CAMERA_DATA data
	sData = data
ENDPROC

PROC CLEANUP_CURSOR_DATA(NET_HEIST_PLANNING_GENERIC_CURSOR_DATA &sData)
	NET_HEIST_PLANNING_GENERIC_CURSOR_DATA data
	sData = data
ENDPROC

PROC CLEANUP_INPUT_DATA(NET_HEIST_PLANNING_GENERIC_INPUT_DATA &sData)
	NET_HEIST_PLANNING_GENERIC_INPUT_DATA data
	sData = data
ENDPROC

PROC CLEANUP_NHPG_GLOBAL_DATA()
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CLEANUP_NHPG_GLOBAL_DATA - Cleaning up.")

	CLEANUP_SCALEFORM_DATA(g_sNetHeistPlanningGenericClientData.sScaleformData)
	CLEANUP_FSM_DATA(g_sNetHeistPlanningGenericClientData.sStateData)
	CLEANUP_POSITION_DATA(g_sNetHeistPlanningGenericClientData.sPositionData)
	
	CLEANUP_CAMERA_DATA(g_sNetHeistPlanningGenericClientData.sCameraData)
	CLEANUP_CURSOR_DATA(g_sNetHeistPlanningGenericClientData.sCursorData)
	CLEANUP_INPUT_DATA(g_sNetHeistPlanningGenericClientData.sInputData)
	
	g_sNetHeistPlanningGenericClientData.eBoardType = NHPG_BOARD__INVALID
	g_sNetHeistPlanningGenericClientData.piLeader = INVALID_PLAYER_INDEX()
	g_sNetHeistPlanningGenericClientData.iBitSet = 0
	
	g_sNetHeistPlanningGenericClientData.bCoronaPlanningRunning = FALSE
	g_sNetHeistPlanningGenericClientData.eBoardTypeToStart = NHPG_BOARD__INVALID
	g_sNetHeistPlanningGenericClientData.bCleanupImmediately = FALSE
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CLEANUP_NHPG_GLOBAL_DATA - g_sNetHeistPlanningGenericClientData.bCoronaPlanningRunning = FALSE")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CLEANUP_NHPG_GLOBAL_DATA - g_sNetHeistPlanningGenericClientData.eBoardTypeToStart = NHPG_BOARD__INVALID")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CLEANUP_NHPG_GLOBAL_DATA - g_sNetHeistPlanningGenericClientData.bCleanupImmediately = FALSE")
ENDPROC






