USING "globals.sch"
USING "rage_builtins.sch"
USING "net_spawn.sch"
USING "net_mission.sch"
USING "net_include.sch"
USING "net_ambience.sch"
USING "am_common_ui.sch"
USING "net_events.sch"
USING "net_gang_boss.sch"
USING "DM_Leaderboard.sch"
USING "fmmc_camera.sch"
USING "net_office.sch"
USING "net_render_targets.sch"

#IF IS_DEBUG_BUILD
USING "profiler.sch"
#ENDIF

PROC EMPTY()
ENDPROC

FUNC BOOL EMPTY_BOOL()
	RETURN FALSE
ENDFUNC

PROC EMPTY_INT(INT i)
	UNUSED_PARAMETER(i)
ENDPROC


