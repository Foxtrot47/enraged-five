
USING "net_heist_planning_generic_support.sch"


FUNC STRING GET_MAIN_BOARD_SCALEFORM_MOVIE(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	UNUSED_PARAMETER(sData)
	
	IF logic.Scaleform.MainBoard.Movie != NULL
		RETURN CALL logic.Scaleform.Mainboard.Movie()
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_INSTRUCTIONAL_BUTTONS_SCALEFORM_MOVIE(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	UNUSED_PARAMETER(sData)
	
	IF logic.Scaleform.InstructionalButtons.Movie != NULL
		RETURN CALL logic.Scaleform.InstructionalButtons.Movie()
	ENDIF
	
	RETURN "INSTRUCTIONAL_BUTTONS"
ENDFUNC

PROC REQUEST_SCALEFORM_ASSETS(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	sData.sScaleformData.siMainBoard = REQUEST_SCALEFORM_MOVIE(GET_MAIN_BOARD_SCALEFORM_MOVIE(sData))
	sData.sScaleformData.siButtons = REQUEST_SCALEFORM_MOVIE(GET_INSTRUCTIONAL_BUTTONS_SCALEFORM_MOVIE(sData))
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | REQUEST_SCALEFORM_ASSETS - Requested scaleform assets.")
ENDPROC

FUNC BOOL HAVE_SCALEFORM_ASSETS_LOADED(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(sData.sScaleformData.siMainBoard)
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | HAVE_SCALEFORM_ASSETS_LOADED - Waiting for main board")
		RETURN FALSE
	ENDIF
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(sData.sScaleformData.siButtons)
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | HAVE_SCALEFORM_ASSETS_LOADED - Waiting for instructional buttons")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC CLEANUP_SCALEFORM_BUTTON_ASSET(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	IF HAS_SCALEFORM_MOVIE_LOADED(sData.sScaleformData.siButtons)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sData.sScaleformData.siButtons)
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | CLEANUP_SCALEFORM_BUTTON_ASSET - cleaning instructional buttons")
	ENDIF
ENDPROC

//---------------------------
//	RENDER TARGETS
//---------------------------

FUNC BOOL INITIALISE_RENDERTARGET(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)

	IF NOT IS_BIT_SET(sData.iBitSet, NHPG_BITSET_BOARD_RENDERTARGET_INITIALISED)
		
		STRING rtName = CALL logic.Scaleform.RenderTarget.Name()
		MODEL_NAMES rtModel = CALL logic.Scaleform.RenderTarget.Model()
		
		IF IS_STRING_NULL_OR_EMPTY(rtName)
			SCRIPT_ASSERT("[NHPG] | INITIALISE_BOARD - render target string is null or empty")
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | INITIALISE_RENDERTARGET - render target string is null or empty")
			RETURN FALSE
		ENDIF
		
		IF rtModel = DUMMY_MODEL_FOR_SCRIPT
			SCRIPT_ASSERT("[NHPG] | INITIALISE_BOARD - render target model is invalid or dummy")
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | INITIALISE_RENDERTARGET - render target model is invalid or dummy")
			RETURN FALSE
		ENDIF
		
		IF REGISTER_AND_LINK_RENDER_TARGET(rtName, rtModel)
			sData.sScaleformData.iRenderTargetID = GET_RENDER_TARGET_ID(rtName)
			
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | INITIALISE_RENDERTARGET - rendertarget successfully registered and linked. id: ", sData.sScaleformData.iRenderTargetID,"; string: ", rtName, "; model hash: ", ENUM_TO_INT(rtModel))
			
			SET_BIT(sData.iBitSet, NHPG_BITSET_BOARD_RENDERTARGET_INITIALISED)
		ENDIF
	
	ENDIF
	
	RETURN IS_BIT_SET(sData.iBitSet, NHPG_BITSET_BOARD_RENDERTARGET_INITIALISED)
ENDFUNC

PROC CLEANUP_RENDERTARGET()
	IF logic.Scaleform.RenderTarget.Name != NULL
		RELEASE_REGISTERED_RENDER_TARGET(CALL logic.Scaleform.RenderTarget.Name())
	ENDIF
ENDPROC

//--------------------------
//	INSTRUCTIONAL BUTTONS
//--------------------------

FUNC BOOL SHOULD_DRAW_INSTRUCTIONAL_BUTTONS(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	RETURN sData.sScaleformData.bDrawInstructions
ENDFUNC

PROC SET_SHOULD_DRAW_INSTRUCTIONAL_BUTTONS(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, BOOL bDraw)
	IF sData.sScaleformData.bDrawInstructions != bDraw
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | SET_SHOULD_DRAW_INSTRUCTIONAL_BUTTONS - bDraw = ", GET_STRING_FROM_BOOL(bDraw))
	ENDIF
	sData.sScaleformData.bDrawInstructions = bDraw
ENDPROC


#IF NOT DEFINED(BUILD_INSTRUCTIONAL_BUTTONS)
PROC BUILD_INSTRUCTIONAL_BUTTONS(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//		Info -  THIS IS SET UP AS AN EXAMPE, EACH HEIST BOARD WILL NEED IT'S OWN SET 
	//				OF INSTRUCTIONAL BUTTONS AS THEY ARE ALL DIFFERENT.
	//
	//		- When adding instructional buttons, they will ordered from right to left in the order that they are build
	//
	//		- You will need to add conditions to display each instructional button that may otherwise not display.
	//				i.e.  You will need to add a check for if the current item can be purchased 
	//					  before displaying the purchase option.
	//
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	BOOL bLocalIsLeader = (sData.piLeader = PLAYER_ID())

	SWITCH sData.sStateData.eSubState
		CASE NHPG_BOARDSUBSTATE__VIEWING
			
			// Only the leader can launch/select items whilst viewing
			IF bLocalIsLeader
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
					"CH_INSTR_SEL", sData.sScaleformData.sButtons, TRUE) // Select.
			ENDIF
			
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
				"CH_INSTR_QUIT", sData.sScaleformData.sButtons, TRUE) // Quit.
		
			// Only the leader can purhcase items for the heist
			IF bLocalIsLeader
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_X, 
					"CH_INSTR_PURCH", sData.sScaleformData.sButtons, TRUE)	// Purchase
			ENDIF
			
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
				"CH_INSTR_INFO", sData.sScaleformData.sButtons, TRUE) // More info.
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_RB, 
				"CH_INSTR_PREP", sData.sScaleformData.sButtons, TRUE) // Prep Board.	
							
			// Can't look around or zoom when using mouse on the planning board.
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
					"CH_INSTR_ZOOM", sData.sScaleformData.sButtons, TRUE) // Zoom.
			ELSE
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_TRIGGERS, 
					"CH_INSTR_ZOOM", sData.sScaleformData.sButtons) // Zoom. 
			ENDIF
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
				"HEIST_IB_LOOK", sData.sScaleformData.sButtons) // Look Around

			ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
				"HEIST_IB_NAV", sData.sScaleformData.sButtons) // Navigate controls.
		BREAK
		
		CASE NHPG_BOARDSUBSTATE__EDITING
				
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
				"CH_INSTR_BACK", sData.sScaleformData.sButtons, TRUE) // Back.
				
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
				"HEIST_IB_LOOK", sData.sScaleformData.sButtons) // Look Around
				
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_LR, 
				"CH_INSTR_CHACP", sData.sScaleformData.sButtons) // Change Access Point.
		BREAK
	ENDSWITCH

ENDPROC
#ENDIF

#IF NOT DEFINED(HANDLE_DIALOGUE_INSTRUCTIONS)
PROC HANDLE_DIALOGUE_INSTRUCTIONS(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	UNUSED_PARAMETER(sData)
ENDPROC
#ENDIF


PROC UPDATE_INSTRUCTIONAL_BUTTONS(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	
	SPRITE_PLACEMENT spPlacementLocation = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
	
	// Don't render the buttons if the player is typing in network chat.
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF
	
	IF sDialogueData.eDialogueCurrentlyPlaying != NHPG_DIALOGUE__INVALID
		HANDLE_DIALOGUE_INSTRUCTIONS(sData)
		EXIT
	ELSE
		IF bBuiltDialogueInstructions
			SET_INSTRUCTIONAL_BUTTON_UPDATE_REQUIRED(sData, TRUE)
			bBuiltDialogueInstructions = FALSE
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | UPDATE_INSTRUCTIONAL_BUTTONS - Instructions were rebuild for dialogue that is no longer playing, we need to update.")
		ENDIF
	ENDIF
	
	IF NOT IS_INSTRUCTIONAL_BUTTON_UPDATE_REQUIRED(sData)
	
		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sData.sScaleformData.siButtons,
											spPlacementLocation,
											sData.sScaleformData.sButtons,
											FALSE)
	
		EXIT
	ENDIF
	
	IF NOT GET_IS_WIDESCREEN()
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
	    SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(sData.sScaleformData.sButtons, 0.5)
	ELSE
	    SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(sData.sScaleformData.sButtons, 0.7)
	ENDIF
	
	REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sData.sScaleformData.sButtons)
	
	// Instructional buttons are configured in here
	// You will most likely need to override this method in the heist pack script as the default
	//     ones in here are very basic. Look inside this method to see how instructions should be added.
	BUILD_INSTRUCTIONAL_BUTTONS(sData)
	
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | UPDATE_INSTRUCTIONAL_BUTTONS - Updated instructional buttons for position: ", ENUM_TO_INT(sData.sCameraData.ePositionId))
	
	RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sData.sScaleformData.siButtons,
										spPlacementLocation,
										sData.sScaleformData.sButtons,
										sData.sScaleformData.bUpdateInstructions)
											
	SET_INSTRUCTIONAL_BUTTON_UPDATE_REQUIRED(sData, FALSE)
								
ENDPROC

//----------------------
//	MAIN BOARD
//----------------------

FUNC BOOL SHOULD_DRAW_MAIN_BOARD(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	RETURN sData.sScaleformData.bDrawBoard
ENDFUNC

FUNC BOOL SHOULD_DRAW_ALT(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	IF logic.Scaleform.MainBoard.DrawAlt != NULL
		RETURN CALL logic.Scaleform.MainBoard.DrawAlt(sData)
	ENDIF
	
	RETURN sData.sScaleformData.bDrawAlt
ENDFUNC

PROC SET_SHOULD_DRAW_MAIN_BOARD(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, BOOL bDraw)
	IF sData.sScaleformData.bDrawBoard != bDraw
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | SET_SHOULD_DRAW_MAIN_BOARD - bDraw = ", GET_STRING_FROM_BOOL(bDraw))
	ENDIF
	sData.sScaleformData.bDrawBoard = bDraw
ENDPROC

FUNC HEIST_BOARD_RENDER_TYPE GET_BOARD_RENDER_TYPE()
	IF logic.Scaleform.MainBoard.RenderType != NULL
		RETURN CALL logic.Scaleform.MainBoard.RenderType()
	ENDIF
	
	RETURN HBRT_SUPERLARGE
ENDFUNC

FUNC FLOAT GET_BOARD_CENTRE_X()
	IF logic.Scaleform.MainBoard.BoardCentreX != NULL
		RETURN CALL logic.Scaleform.MainBoard.BoardCentreX()
	ENDIF
	
	RETURN HEIST_BOARD_DEFAULT_CENTRE_X
ENDFUNC

FUNC FLOAT GET_BOARD_CENTRE_Y()
	IF logic.Scaleform.MainBoard.BoardCentreY != NULL
		RETURN CALL logic.Scaleform.MainBoard.BoardCentreY()
	ENDIF
	
	RETURN HEIST_BOARD_DEFAULT_CENTRE_Y
ENDFUNC

FUNC FLOAT GET_BOARD_WIDTH()
	IF logic.Scaleform.MainBoard.BoardWidth != NULL
		RETURN CALL logic.Scaleform.MainBoard.BoardWidth()
	ENDIF
	
	RETURN HEIST_BOARD_DEFAULT_WIDTH
ENDFUNC

FUNC FLOAT GET_BOARD_HEIGHT()
	IF logic.Scaleform.MainBoard.BoardHeight != NULL
		RETURN CALL logic.Scaleform.MainBoard.BoardHeight()
	ENDIF
	
	RETURN HEIST_BOARD_DEFAULT_HEIGHT
ENDFUNC

PROC UPDATE_SCALEFORM(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	IF SHOULD_DRAW_MAIN_BOARD(sData)
		IF HAS_SCALEFORM_MOVIE_LOADED(sData.sScaleformData.siMainBoard)

			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			SET_TEXT_RENDER_ID(sData.sScaleformData.iRenderTargetID)
			
			SWITCH GET_BOARD_RENDER_TYPE()
				CASE HBRT_SUPERLARGE 	SET_SCALEFORM_MOVIE_TO_USE_SUPER_LARGE_RT(sData.sScaleformData.siMainBoard, TRUE)	BREAK
				CASE HBRT_LARGE 		SET_SCALEFORM_MOVIE_TO_USE_LARGE_RT(sData.sScaleformData.siMainBoard, TRUE)			BREAK
			ENDSWITCH
			
			DRAW_SCALEFORM_MOVIE(sData.sScaleformData.siMainBoard, 
								GET_BOARD_CENTRE_X(), 
								GET_BOARD_CENTRE_Y(), 
								GET_BOARD_WIDTH(), 
								GET_BOARD_HEIGHT(), 
								HEIST_BOARD_RED, HEIST_BOARD_BLUE, HEIST_BOARD_GREEN, HEIST_BOARD_ALPHA)
								
			SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)				
									
			IF sData.sScaleformData.bVerboseDebugging
				CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | UPDATE_SCALEFORM - Index: ", NATIVE_TO_INT(sData.sScaleformData.siMainBoard))
			ENDIF
		ENDIF
	ELIF SHOULD_DRAW_ALT(sData)
		IF logic.Scaleform.MainBoard.AltRender != NULL
			CALL logic.Scaleform.MainBoard.AltRender(sData)
		ENDIF
	ENDIF
	
	IF SHOULD_DRAW_INSTRUCTIONAL_BUTTONS(sData)
		UPDATE_INSTRUCTIONAL_BUTTONS(sData)
	ENDIF
ENDPROC
