///    Author:		Orlando 	
///    Team:		Online Tech
///    Description:	Header can be used to set up a "scanner" on a camera that displays some stats about players.
///    				Based on, and uses parts of, gb_vehicle_export_scanner.sch		
///    	
///    How to use:	In script using the scanner: 				
///    				Add a COMBO_SCANNER_STRUT to script local data
///    				Call INIT_COMBO_SCANNER( COMBO_SCANNER_STRUT& data, CAMERA_INDEX cameraToUse )
///    				Call MAINTAIN_COMBO_SCANNER( COMBO_SCANNER_STRUCT& data ) every frame
///    				
///    				In freemode.sc:
///    				Call MAINTAIN_UPDATING_MY_COMBO_SCANNER_INFO() every frame to keep local player stats updated for other players to access through broadcast data...
///    				...this has an internal timer so that it doesn't spam update the global broadcast data.

USING "rage_builtins.sch"
USING "net_include.sch"
USING "globals.sch"
USING "script_ped.sch"
USING "gb_vehicle_export_scanner.sch"
USING "net_scoring_common.sch"
USING "net_vehicle_setup.sch"

CONST_INT ciCOMBO_SCANNER_BS_SHOWING_RESULTS 	 	0//bShowingResults

CONST_INT ciCOMBO_SCANNER_BS_LOADED_TEXTURES 	 	2//bLoadedTextureDict
CONST_INT ciCOMBO_SCANNER_BS_STARTED_SCAN_TIMER	3
CONST_INT ciCOMBO_SCANNER_BS_STARTED_RESULTS_TIMER	4
CONST_INT ciCOMBO_SCANNER_BS_STARTED_LOS_TIMER		5
CONST_INT ciCOMBO_SCANNER_BS_STARTED_SCAN_TEXT_TIMER 6
CONST_INT ciCOMBO_SCANNER_BS_STARTED_OUT_OF_RANGE_TIMER 7

CONST_INT ciCOMBO_SCANNER_PLAYER_BS_HAS_BEEN_SCANNED 	0
CONST_INT ciCOMBO_SCANNER_PLAYER_BS_IS_TARGET			1

CONST_INT ciCOMBO_SCANNER_SCAN_TIME 			5000	
CONST_INT ciCOMBO_SCANNER_INFO_UPDATE_INTERVAL 2000
CONST_INT ciCOMBO_SCANNER_NEW_TARGET_TIME 		1200
CONST_INT ciCOMBO_SCANNER_LOS_BREAK_TIME 		1200
CONST_INT ciCOMBO_SCANNER_OUT_OF_RANGE_TIME 	1200

CONST_FLOAT ciCOMBO_SCANNER_HUD_SCAN_DISTANCE	0.005

//STRUCT COMBO_SCANNER_STRUCT
//	INT iBsPlayerHasBeenScanned			//Each bit represents whether the player of that index has been scanned already.					
//	INT iBs
//	
//
//	
//	PLAYER_INDEX piCurrentScanTarget	
//	
//	TIME_DATATYPE tScanTime				//ScanTimer
//	TIME_DATATYPE tScanTextTime			//ScanningTextTimer
//	TIME_DATATYPE tResultsTime			//ResultsTimer
//	TIME_DATATYPE tLosBreakTime			//tLosBreakTime
//	TIME_DATATYPE tNewTargetTime		//Time that we found a target closer to centre screen
//	TIME_DATATYPE tOutOfRangeTime		//Time the current target got out of range
//	
//	CAMERA_INDEX camera 				//Camera we are using as scanner
//	SCANNER_AUDIO_STRUCT 	sAudio		//Audio data for scanning
//	SCALEFORM_INDEX			scaleform
//	FLOAT 					fScanRange = -1.0
//	
//	VEHICLE_SCANNER_STRUCT  sVehicleScannerStruct
//ENDSTRUCT


FUNC BOOL IS_SAFE_FOR_COMBO_SCANNER(COMBO_SCANNER_STRUCT& ref_comboScanner)
	
	IF NOT IS_BIT_SET(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_LOADED_TEXTURES)
		REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
		REQUEST_STREAMED_TEXTURE_DICT("droneHUD")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("helicopterhud")
		AND HAS_STREAMED_TEXTURE_DICT_LOADED("droneHUD")
			PRINTLN("[IS_SAFE_FOR_COMBO_SCANNER] - IS_SAFE_FOR_COMBO_SCANNER - hud has been loaded, now safe")
			SET_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_LOADED_TEXTURES)
			ref_comboScanner.sVehicleScannerStruct.bLoadedTextureDict = TRUE
		ENDIF
	ENDIF
	
	// Add some other checks here
//	IF NOT g_heligunCamActive
//		RETURN FALSE
//	ENDIF
	
	IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
		IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())) != POLMAV
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN IS_BIT_SET(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_LOADED_TEXTURES)
ENDFUNC

PROC MAINTAIN_DISABLED_CONTROLS()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON) 
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
ENDPROC	

PROC MAINTAIN_DISABLED_HUD()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
ENDPROC

FUNC BOOL IS_PLAYER_IN_TARGET_RECT(FLOAT fX, FLOAT fY)
	fX = ABSF(fX - 0.5)
	fY = ABSF(fY - 0.5)
	RETURN fX <= 0.2 AND fY <= 0.2
ENDFUNC

FUNC BOOL HAS_NETWORK_TIMER_EXPIRED(TIME_DATATYPE tStart, INT iDuration)
	RETURN ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tStart)) > iDuration
ENDFUNC

FUNC BOOL CAN_SCAN_THIS_PLAYER(COMBO_SCANNER_STRUCT& ref_comboScanner, PLAYER_INDEX player)
	IF player = INVALID_PLAYER_INDEX()
		//no valid player
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_IS_PLAYER_ACTIVE(player)
		PRINTLN("[COMBO_SCANNER] CAN_SCAN_THIS_PLAYER - player isn't active") //@@Either link these to a widget or remove eventually.They will be spammy.
		RETURN FALSE
	ENDIF
	
	FLOAT fX, fY	
	PED_INDEX targetPed = GET_PLAYER_PED(player)
	
	IF NOT DOES_ENTITY_EXIST(targetPed)
	OR IS_ENTITY_DEAD(targetPed)
		PRINTLN("[COMBO_SCANNER] CAN_SCAN_THIS_PLAYER - player's ped doesn't exist or is dead")
		RETURN FALSE
	ENDIF
	
	VECTOR vTargetPlayerCoords = GET_ENTITY_COORDS(targetPed, FALSE)
	IF GET_DISTANCE_BETWEEN_COORDS(vTargetPlayerCoords, GET_CAM_COORD(ref_comboScanner.camera)) > 250
		PRINTLN("[COMBO_SCANNER] - Target player too far away")
		RETURN FALSE
	ENDIF
	
	IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vTargetPlayerCoords, fX, fY)
		PRINTLN("[COMBO_SCANNER] - cant get screen coord from world coord")
		RETURN FALSE
	ENDIF

	IF IS_PLAYER_IN_TARGET_RECT(fX, fY)
		IF player = ref_comboScanner.piCurrentScanTarget
			CLEAR_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_OUT_OF_RANGE_TIMER)
		ENDIF
	ELSE
		IF player <> ref_comboScanner.piCurrentScanTarget
			RETURN FALSE
		ENDIF
		
		IF IS_BIT_SET(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_OUT_OF_RANGE_TIMER)
			IF HAS_NETWORK_TIMER_EXPIRED(ref_comboScanner.tOutOfRangeTime, ciCOMBO_SCANNER_OUT_OF_RANGE_TIME)
				CLEAR_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_OUT_OF_RANGE_TIMER)
				PRINTLN("[COMBO_SCANNER] - Target too far from screen center")
				RETURN FALSE
			ENDIF 
		ELSE
			SET_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_OUT_OF_RANGE_TIMER)
			ref_comboScanner.tOutOfRangeTime = GET_NETWORK_TIME()
		ENDIF
	ENDIF
			
	//Should use the cam somehow@@?
	//IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(PLAYER_PED_ID(), GET_PLAYER_PED(ref_comboScanner.piCurrentScanTarget), SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
	//Doesn't work with vehicles but seems to work much better than func above ^ 
	IF NOT IS_ENTITY_OCCLUDED(targetPed)
		CLEAR_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_LOS_TIMER)
	ELSE
		IF ref_comboScanner.piCurrentScanTarget = INT_TO_NATIVE(PLAYER_INDEX, -1)
			PRINTLN("[COMBO_SCANNER] - no line of sight")
			RETURN FALSE
		ELSE
			IF IS_BIT_SET(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_LOS_TIMER)
				IF HAS_NETWORK_TIMER_EXPIRED(ref_comboScanner.tLosBreakTime, ciCOMBO_SCANNER_LOS_BREAK_TIME)
					PRINTLN("[COMBO_SCANNER] - no line of sight")
					RETURN FALSE
				ENDIF
			ELSE
				SET_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_LOS_TIMER)
				ref_comboScanner.tLosBreakTime = GET_NETWORK_TIME()
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns the size the height of the box that surrounds the current scan target.
///    Does not take into account if the ped is in a vehicle/fallen over etc. Want this? @@
FUNC FLOAT GET_ENTITY_BOX_SIZE(ENTITY_INDEX target, CAMERA_INDEX cam, FLOAT fEntityHeight = 1.65)
// from http://paulbourke.net/miscellaneous/lens/  ...
// horizontal_field_of_view = 2 * atan(0.5 * width / focal_length)     
// from https://www.scantips.com/lights/subjectdistance.html ...
// Object_height_on_sensor / focal_length = Real_Object_height / Distance_to_Object    

//    FOV = 2 ATAN ( X / F_LEN )
//    TAN ( FOV / 2 ) = X / F_LEN
//	  X / ( TAN ( FOV / 2 ) = F_LEN 
//
//    F_LEN = X / ( TAN ( FOV / 2 )

//	  H / FLEN = RH / D
//
//    H = FLEN * RH / D
	VECTOR vCamCoords = GET_CAM_COORD(cam)
	VECTOR vTargetCoords = GET_ENTITY_COORDS(target)
	FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(vCamCoords, vTargetCoords)

	FLOAT fFov = GET_CAM_FOV(cam)
	FLOAT fX = 1.0 //arbitrary
	FLOAT fFocalLen = fX / ( TAN(fFov / 2.0))
	FLOAT fHeight = fFocalLen * fEntityHeight / fDist
	
	RETURN fHeight
ENDFUNC	

/// PURPOSE:
///    Draw some scan lines moving through the box, where the draw origin has been set to centre of the box.
PROC DRAW_SCAN_LINES_ON_TARGET(INT iScanTime, FLOAT fBoxWidth, FLOAT fBoxHeight, INT r, INT g, INT b)	
	INT iSegmentCount = 15
	INT iMaxSegmentIndex = iSegmentCount - 1
	FLOAT fSegmentHeight = fBoxHeight / TO_FLOAT(iSegmentCount)
	FLOAT fDist = fBoxHeight * 2
	FLOAT fSpeed = fDist / ciCOMBO_SCANNER_SCAN_TIME
	FLOAT fTopY =  (-fBoxHeight/2)
	FLOAT fYPos =  fTopY + (fSpeed * iScanTime)
	FLOAT fAlpha
	INT i
	IF iScanTime <= ciCOMBO_SCANNER_SCAN_TIME
		FOR i = 0 TO iMaxSegmentIndex
			fYPos -= fSegmentHeight
			fAlpha = 230.0 * (1.0 - (TO_FLOAT(i) / TO_FLOAT(iSegmentCount)))
			
			FLOAT fFinalY = fyPos
			FLOAT fFinalHeight = fSegmentHeight
			
			//Hide segments above the box
			IF fFinalY < fTopY
				fFinalHeight = fSegmentHeight - (fTopY - fFinalY)
				IF fFinalHeight <= 0 
					EXIT
				ENDIF
				fFinalY = fTopY
			ENDIF
			
			//Hide segments below the box
			IF fFinalY + fFinalHeight > fTopY + fBoxHeight
				fFinalHeight = (fTopY + fBoxHeight) - fFinalY
				IF fFinalHeight <= 0
					RELOOP
				ENDIF
			ENDIF
			
			DRAW_RECT_FROM_CORNER(-fBoxWidth / 2, fFinalY, fBoxWidth, fFinalHeight, r, g, b, CEIL(fAlpha))
		ENDFOR	
	ENDIF
ENDPROC

PROC FORMAT_TEXT_FOR_PLAYER_INFO(FLOAT fTextSize)
	SET_TEXT_SCALE(fTextSize, fTextSize)
	INT r, g, b , a
	GET_HUD_COLOUR(HUD_COLOUR_WHITE, r, g, b, a)
	SET_TEXT_COLOUR(r, g, b, a)
	SET_TEXT_DROPSHADOW(0,0,0,0,0)
	SET_TEXT_WRAP(-2.0, 2.0)
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC

/// PURPOSE:
///    Grab an INT stat based off the PLAYER_SCAN_INT_STATS index
///    (Done this way for ease of maintaining)    
//FUNC INT GET_SCAN_INT_STAT(PLAYER_SCAN_INT_STATS eStat)
//	SWITCH eStat
//		CASE PSIS_ACCURACY 				RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_SHOOTING_ABILITY)
//		CASE PSIS_DEATHS 				RETURN GET_MP_INT_PLAYER_STAT(MPPLY_DEATHS_PLAYER)
//		CASE PSIS_FAV_RADIO 			RETURN GET_MP_INT_PLAYER_STAT(MPPLY_MOST_FAVORITE_STATION)
//		CASE PSIS_FAV_VEHICLE 			RETURN 0 //??@@
//		CASE PSIS_KILLS 				RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KILLS_PLAYERS)
//		CASE PSIS_PRIVATE_DANCES 		RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_LAP_DANCED_BOUGHT)
//		CASE PSIS_RANK 					RETURN GET_PLAYER_RANK(PLAYER_ID()) //This one doesn't need to be stored in the struct.. maybe have another function for getting stored stats
//		CASE PSIS_SEX_ACTS_PURCHASED 	RETURN 0//??@@ what stat is this?
//		CASE PSIS_TOTAL_EARN 			RETURN GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_EVC)
//	ENDSWITCH
//	RETURN 0
//ENDFUNC

/// PURPOSE:
///    Retuns the label to be used when printing scan results for the eStat stat.
//FUNC STRING GET_SCAN_INT_OUTPUT_LABEL(PLAYER_SCAN_INT_STATS eStat)
//	SWITCH eStat
//		CASE PSIS_ACCURACY 				RETURN "SCAN_ACC"
//		CASE PSIS_DEATHS 				RETURN "SCAN_DEATH"
//		CASE PSIS_FAV_RADIO 			RETURN "SCAN_RADIO"
//		CASE PSIS_FAV_VEHICLE 			RETURN "SCAN_VEH"
//		CASE PSIS_KILLS 				RETURN "SCAN_KILLS"
//		CASE PSIS_PRIVATE_DANCES 		RETURN "SCAN_DANCES"
//		CASE PSIS_RANK 					RETURN "SCAN_RANK"
//		CASE PSIS_SEX_ACTS_PURCHASED 	RETURN "SCAN_SEX_ACTS"
//		CASE PSIS_TOTAL_EARN 			RETURN "SCAN_EARN"
//		DEFAULT RETURN "Unknown stat"
//	ENDSWITCH
//ENDFUNC

/// PURPOSE:
/////    Handles printing a player_scan_int_stats stat to the screen. Loop over stats and call this to print each.
//PROC DISPLAY_SCAN_INT_STAT(PLAYER_SCAN_INFO& ref_info, FLOAT fX, FLOAT fY, FLOAT fTextSize, INT iStat)
//	PLAYER_SCAN_INT_STATS eStat = INT_TO_ENUM(PLAYER_SCAN_INT_STATS, iStat)
//	STRING sLabel = GET_SCAN_INT_OUTPUT_LABEL(INT_TO_ENUM(PLAYER_SCAN_INT_STATS, iStat))
//	
//	FORMAT_TEXT_FOR_PLAYER_INFO(fTextSize)
//	SWITCH eStat
//		CASE PSIS_FAV_RADIO 
//			println("COMBO_SCANNER_stats]  ref_info.iIntStats[iStat] == ",ref_info.iIntStats[iStat])
//			IF ref_info.iIntStats[iStat] >= 0 	 //Sometimes OFF index is -1, sometimes 255..
//			AND ref_info.iIntStats[iStat] <= 245 //...and uninitialised stats can be anything
//				DISPLAY_TEXT_WITH_STRING(fX, fY, sLabel, GET_RADIO_STATION_NAME(ref_info.iIntStats[iStat])) 
//			ELSE
//				DISPLAY_TEXT(fX, fY, "SCAN_RADIO_N") //None
//			ENDIF
//			BREAK
//		//CASE PSIS_FAV_VEHICLE 	DISPLAY_TEXT_WITH_STRING(vPos.x, vPos.y, sLabel, (ref_info.iIntStats[iStat])) BREAK <<-- convert model name to vehicle name or something@@
//		DEFAULT 				DISPLAY_TEXT_WITH_NUMBER(fX, fY, sLabel, ref_info.iIntStats[iStat]) BREAK
//	ENDSWITCH 
//ENDPROC

/// PURPOSE:
///    Handles drawing the player name + stats scan results
//PROC DRAW_PLAYER_INFO(VECTOR vNamePos, VECTOR vStart, VECTOR vSpacing, FLOAT fTextSize, PLAYER_INDEX target, PLAYER_SCAN_INFO& ref_info)	
//	VECTOR vPos = vStart
//	
//	TEXT_LABEL_15 txtName = GET_PLAYER_NAME(target)		
//	FORMAT_TEXT_FOR_PLAYER_INFO(fTextSize* 1.5)
//	DISPLAY_TEXT_WITH_PLAYER_NAME(vNamePos.x, vNamePos.y, "SCAN_PLAYER", txtName, HUD_COLOUR_WHITE)
//		
//	INT iStat
//	REPEAT PSIS_COUNT iStat
//		DISPLAY_SCAN_INT_STAT(ref_info, vPos.x, vPos.y, fTextSize, iStat)
//		vPos += vSpacing
//	ENDREPEAT
//ENDPROC

PROC CLEAR_SCALEFORM_DATA_BOX(COMBO_SCANNER_STRUCT& ref_comboScanner)
	BEGIN_SCALEFORM_MOVIE_METHOD(ref_comboScanner.scaleform, "SET_INFO_LIST_IS_VISIBLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Handles drawing the scan box around the current target.
PROC DRAW_SCAN_BOX_AROUND_TARGET_PLAYER(COMBO_SCANNER_STRUCT& ref_comboScanner, BOOL bDoScanLines = TRUE, HUD_COLOURS eColour = HUD_COLOUR_WHITE, BOOL bFlash = FALSE)

	FLOAT  fBoxDrawSizeX, fBoxDrawSizeY
	VECTOR vTargetPos
	INT r, g, b, a//, iTextTimer
	//TEXT_LABEL_15 tl15_boxText
	
	
//	IF bDoScanLines
//		tl15_boxText = "VEX_SCAN"
//	ENDIF
	
	// So box doesn't overlay hud elements (i.e. radar).
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	
	PED_INDEX targetPed = GET_PLAYER_PED(ref_comboScanner.piCurrentScanTarget)
	vTargetPos = GET_ENTITY_COORDS(targetPed)
	
	// So box doesn't lag.
	SET_DRAW_ORIGIN(vTargetPos)
	
	// Get box data.
	fBoxDrawSizeY = GET_ENTITY_BOX_SIZE(GET_PLAYER_PED(ref_comboScanner.piCurrentScanTarget), ref_comboScanner.camera)
	fBoxDrawSizeX = fBoxDrawSizeY/ 4.0
	GET_HUD_COLOUR(eColour, r, g, b, a)
	
	// Draw box.
	bFlash = bFlash 
			OR IS_BIT_SET(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_OUT_OF_RANGE_TIMER)
			OR IS_BIT_SET(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_LOS_TIMER)
	INT iBoxAlpha = PICK_INT(bFlash AND (GET_GAME_TIMER() % 300) < 150, CEIL(TO_FLOAT(a)/4.0), a)
	DRAW_SPRITE("helicopterhud", "hud_outline", 0.0, 0.0, fBoxDrawSizeX, fBoxDrawSizeY, 0, r, g, b, iBoxAlpha)

	// If we want scan lines, draw them.
	// If doing scan lines, set box test to "SCANNING".
	IF bDoScanLines
		INT iScanTime = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), ref_comboScanner.tScanTime))
		DRAW_SCAN_LINES_ON_TARGET(iScanTime, 0.85*fBoxDrawSizeX, 0.85*fBoxDrawSizeY, r, g, b)
		IF NOT IS_BIT_SET(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_SCAN_TEXT_TIMER)
			SET_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_SCAN_TEXT_TIMER)
			ref_comboScanner.tScanTextTime = GET_NETWORK_TIME()
//		ELSE
//			iTextTimer = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), ref_comboScanner.tScanTextTime))
		ENDIF
	ENDIF
	
	// Draw text underneath box. Make flash if doing scanning.
	
//	IF bDoScanLines
//		IF ( iTextTimer % 1000 <= 500 )
//			FLOAT fTextScale = 8 * fBoxDrawSizeY
//			DRAW_BOX_TEXT(tl15_boxText, -fBoxDrawSizeX/2, (fBoxDrawSizeY* 0.1) + (fBoxDrawSizeY / 2.0), r, g, b, fTextScale, FALSE)
//		ENDIF
//	ENDIF
	
	IF IS_BIT_SET(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_SHOWING_RESULTS)
	AND ref_comboScanner.fScanRange <= ciCOMBO_SCANNER_HUD_SCAN_DISTANCE
	AND ref_comboScanner.fScanRange != -1
	AND IS_NET_PLAYER_OK(ref_comboScanner.piCurrentScanTarget)
		FLOAT fAccuracy
		BEGIN_SCALEFORM_MOVIE_METHOD(ref_comboScanner.scaleform, "SET_INFO_LIST_DATA")
			INT iPlayer = NATIVE_TO_INT(ref_comboScanner.piCurrentScanTarget)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GlobalplayerBD_FM[iPlayer].scoreData.iRank) //-rank
			//PRINTLN("CDM: 999- GlobalplayerBD_FM[iPlayer].scoreData.iRank ",GlobalplayerBD_FM[iPlayer].scoreData.iRank)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GlobalplayerBD_FM[iPlayer].scoreData.iEarnings) //-iEarnings
			//PRINTLN("CDM: 999- GlobalplayerBD_FM[iPlayer].scoreData.iCash ",GlobalplayerBD_FM[iPlayer].scoreData.iCash)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GlobalplayerBD_FM[iPlayer].scoreData.iKills) //-iKills
			///PRINTLN("CDM: 999- GlobalplayerBD_FM[iPlayer].scoreData.iKills ",GlobalplayerBD_FM[iPlayer].scoreData.iKills)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GlobalplayerBD_FM[iPlayer].scoreData.iDeaths) //-iDeaths
			//PRINTLN("CDM: 999- GlobalplayerBD_FM[iPlayer].scoreData.iDeaths ",GlobalplayerBD_FM[iPlayer].scoreData.iDeaths)
			//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GlobalplayerBD_FM[iPlayer].scoreData.iSuicides)//-iSuicides
			PRINTLN("CDM: 999- mnFavouriteVehicle is ",GlobalplayerBD_FM[iPlayer].scoreData.mnFavouriteVehicle )
			//PRINTLN("CDM: 999- mnFavouriteVehicle is ",GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GlobalplayerBD_FM[iPlayer].scoreData.mnFavouriteVehicle))
			IF GlobalplayerBD_FM[iPlayer].scoreData.mnFavouriteVehicle = DUMMY_MODEL_FOR_SCRIPT
			OR NOT IS_MODEL_IN_CDIMAGE(GlobalplayerBD_FM[iPlayer].scoreData.mnFavouriteVehicle)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("SCAN_NONE")
			ELSE
				PRINTLN("CDM: 999- mnFavouriteVehicle 2 is ",GlobalplayerBD_FM[iPlayer].scoreData.mnFavouriteVehicle )
				PRINTLN("CDM: 999- mnFavouriteVehicle 2 is ",GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GlobalplayerBD_FM[iPlayer].scoreData.mnFavouriteVehicle))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GlobalplayerBD_FM[iPlayer].scoreData.mnFavouriteVehicle)) //-sFavouritVeh
			ENDIF
			fAccuracy = TO_FLOAT(ROUND(GlobalplayerBD_FM[iPlayer].scoreData.fAccuracy*100))
			fAccuracy = fAccuracy/100
			PRINTLN("CDM: 999- Accuracy is ",fAccuracy)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fAccuracy)//-fAccuracy
			IF ARE_STRINGS_EQUAL(GET_RADIO_STATION_NAME(FIND_RADIO_STATION_INDEX(GlobalplayerBD_FM[iPlayer].scoreData.iFavouriteStation)),"OFF")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("SCAN_NONE")
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_RADIO_STATION_NAME(FIND_RADIO_STATION_INDEX(GlobalplayerBD_FM[iPlayer].scoreData.iFavouriteStation))) //-sRadioStation
			ENDIF
			IF ARE_STRINGS_EQUAL(GET_WEAPON_NAME(GlobalplayerBD_FM[iPlayer].scoreData.mnFavouriteWeapon),"WT_INVALID")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("SCAN_NONE")
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_WEAPON_NAME(GlobalplayerBD_FM[iPlayer].scoreData.mnFavouriteWeapon)) //-sWeaponName
			ENDIF
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GlobalplayerBD_FM[iPlayer].scoreData.iLapDances)  //-iLapDances
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GlobalplayerBD_FM[iPlayer].scoreData.iSexActs)  //-iNumSexActs
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GET_PLAYER_NAME(ref_comboScanner.piCurrentScanTarget))
			//PRINTLN("CDM: 999- name is ",GET_PLAYER_NAME(ref_comboScanner.piCurrentScanTarget))
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(ref_comboScanner.scaleform, "SET_INFO_LIST_IS_VISIBLE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		END_SCALEFORM_MOVIE_METHOD()
	ELSE
		CLEAR_SCALEFORM_DATA_BOX(ref_comboScanner)
	ENDIF
	
//	IF NOT bDoScanLines
//		FLOAT fTextScale = 8 * fBoxDrawSizeY * 0.3 // DRAW_BOX_TEXT does a funky thing
//		VECTOR vInfoTextPos = <<(fBoxDrawSizeX* 0.1)+fBoxDrawSizeX/2,  -(fBoxDrawSizeY / 2.0), 0.0 >>
//		VECTOR vSpacing = <<0.0, fBoxDrawSizeY * 0.11, 0.0>>
//		VECTOR vNamePos = <<-fBoxDrawSizeX/2, - (vSpacing.y * 2) - (fBoxDrawSizeY / 2.0), 0.0 >>
//		INT iPlayerIndex = NATIVE_TO_INT(ref_comboScanner.piCurrentScanTarget)
//		DRAW_PLAYER_INFO(vNamePos, vInfoTextPos, vSpacing, fTextScale, ref_comboScanner.piCurrentScanTarget, GlobalplayerBD_FM[iPlayerIndex].sPlayerScanInfo)
//	ENDIF
	
	CLEAR_DRAW_ORIGIN()
ENDPROC

/// PURPOSE:
///    To be called once a player has been fully scanned & is being targeted.
///    Returns true when the results shouldn't be displayed anymore (legacy, should  be removed @@).
FUNC BOOL SHOW_SCAN_RESULTS(COMBO_SCANNER_STRUCT& ref_comboScanner, BOOL bBoxShouldFlash = FALSE)

	DRAW_SCAN_BOX_AROUND_TARGET_PLAYER(ref_comboScanner, FALSE, HUD_COLOUR_GREEN, bBoxShouldFlash)
	START_SCAN_RESULT_AUDIO(ref_comboScanner.sAudio, TRUE) 
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Reset all the timers used for scanning players.
PROC RESET_COMBO_SCANNER_TIMERS(COMBO_SCANNER_STRUCT& ref_comboScanner)
	CLEAR_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_RESULTS_TIMER)
	CLEAR_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_SCAN_TIMER)
	CLEAR_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_LOS_TIMER)
	CLEAR_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_SCAN_TEXT_TIMER)
	CLEAR_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_OUT_OF_RANGE_TIMER)
	CLEAR_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_SHOWING_RESULTS)
ENDPROC

/// PURPOSE:
///  @@
PROC INIT_COMBO_SCANNER(COMBO_SCANNER_STRUCT& ref_comboScanner, CAMERA_INDEX camera)
	ref_comboScanner.camera = camera
ENDPROC

/// PURPOSE:
///    Returns true once the player has been fully scanned & is ready to display results.
///    Handles drawing the scan box & audio.
FUNC BOOL SCAN_TARGET_PLAYER(COMBO_SCANNER_STRUCT& ref_comboScanner, BOOL bBoxShouldFlash = FALSE)
	IF IS_BIT_SET(ref_comboScanner.iBsPlayerHasBeenScanned, NATIVE_TO_INT(ref_comboScanner.piCurrentScanTarget))
		SET_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_SHOWING_RESULTS)
		//STOP_SCANNING_LOOP_AUDIO(ref_comboScanner.sAudio)
		RETURN TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_SCAN_TIMER)
		SET_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_SCAN_TIMER)
		CLEAR_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_SHOWING_RESULTS)
		ref_comboScanner.tScanTime = GET_NETWORK_TIME()
		PRINTLN("SCAN_TARGET_PLAYER: starting timer")
	ELIF HAS_NETWORK_TIMER_EXPIRED(ref_comboScanner.tScanTime, ciCOMBO_SCANNER_SCAN_TIME)
		CLEAR_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_STARTED_SCAN_TIMER)
		SET_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_SHOWING_RESULTS)
		PRINTLN("SCAN_TARGET_PLAYER: finished scanning")
		STOP_SCANNING_LOOP_AUDIO(ref_comboScanner.sAudio)
		RETURN TRUE
	ELSE
		CLEAR_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_SHOWING_RESULTS)
		DRAW_SCAN_BOX_AROUND_TARGET_PLAYER(ref_comboScanner, DEFAULT, DEFAULT, bBoxShouldFlash)
		START_SCANNING_LOOP_AUDIO(ref_comboScanner.sAudio)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Call to update the player stats from local stat data.
/// PARAMS:
///    ref_scanInfo - 
//PROC STORE_PLAYER_SCAN_INFO(PLAYER_SCAN_INFO& ref_scanInfo)
//	PRINTLN("[COMBO_SCANNER] Updating display stats...")
//	INT iStat
//	REPEAT PSIS_COUNT iStat
//		PLAYER_SCAN_INT_STATS eStat = INT_TO_ENUM(PLAYER_SCAN_INT_STATS, iStat)
//		ref_scanInfo.iIntStats[iStat] = GET_SCAN_INT_STAT(eStat)
//	ENDREPEAT
//ENDPROC

/// PURPOSE:
///    To be called in freemode so that all player keep a
///    broadcast data cache of their scan stats.
//PROC MAINTAIN_UPDATING_MY_COMBO_SCANNER_INFO( BOOL bForceUpdateNow = FALSE )
//	IF bForceUpdateNow
//	OR HAS_NETWORK_TIMER_EXPIRED( GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlayerScanInfo.tLastUpdate, ciCOMBO_SCANNER_INFO_UPDATE_INTERVAL )
//		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlayerScanInfo.tLastUpdate = GET_NETWORK_TIME()
//		STORE_PLAYER_SCAN_INFO( GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlayerScanInfo )	
//		PRINTLN("[COMBO_SCANNER] Updating my player info for any scanners...")
//	ENDIF
//ENDPROC

/// PURPOSE:
///    Finds the player closest to the centre of the screen.
///    Returns -1 if no players are valid.
///    
///    fRange - Filled with distance of player to screen centre
FUNC PLAYER_INDEX FIND_PLAYER_CLOSEST_TO_CENTRE(FLOAT &fRange)
	PLAYER_INDEX piClosest
	FLOAT fSmallestDist2 = ciCOMBO_SCANNER_HUD_SCAN_DISTANCE
	BOOL bFound = FALSE
	VECTOR vPos
	fRange = -1.0
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX player = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		
		IF NOT NETWORK_IS_PLAYER_ACTIVE(player)
		OR NOT IS_NET_PLAYER_OK(player)
			RELOOP
		ENDIF
		
		PED_INDEX ped = GET_PLAYER_PED(player)
		vPos = GET_ENTITY_COORDS(ped)
		IF GET_SCREEN_COORD_FROM_WORLD_COORD(vPos, vPos.x, vPos.y)
			vPos -= <<0.5,0.5,0>> //We want dist from centre of screen
			vPos.z = 0
			FLOAT fDist2 = VMAG2(vPos)
			//PRINTLN("[COMBO_SCANNER] - FIND_PLAYER_CLOSEST_TO_CENTRE - Closest player distance = ",fDist2)
			IF fDist2 < fSmallestDist2
				piClosest = player
				fSmallestDist2 = fDist2
				fRange = fSmallestDist2
				bFound = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF bFound
		PRINTLN("[COMBO_SCANNER] - FIND_PLAYER_CLOSEST_TO_CENTRE - Closest player == ", NATIVE_TO_INT(piClosest))
		RETURN piClosest
	ENDIF
	
	RETURN INT_TO_NATIVE(PLAYER_INDEX, -1)
ENDFUNC

/// PURPOSE:
///    To be called every frame by any script using a camera that is meant to be able to scan players.
///    Call INIT_COMBO_SCANNER() once before calling this, and again if the camera_index changes.
PROC MAINTAIN_COMBO_SCANNER(COMBO_SCANNER_STRUCT& ref_comboScanner)
	IF IS_SAFE_FOR_COMBO_SCANNER(ref_comboScanner)
		
		MAINTAIN_DISABLED_CONTROLS()
		MAINTAIN_DISABLED_HUD()
		START_SCAN_BACKGROUND_LOOP_AUDIO(ref_comboScanner.sAudio)
		IF GB_IS_PLAYER_IN_LAUNCH_GANG_FOR_MISSION(PLAYER_ID(),FMMC_TYPE_FMBB_TARGET_PURSUIT) 
			PRINTLN("[COMBO_SCANNER] - not doing scans for target pursuit mission")
		ELSE
			PLAYER_INDEX piClosest = FIND_PLAYER_CLOSEST_TO_CENTRE(ref_comboScanner.fScanRange)
			IF ref_comboScanner.piCurrentScanTarget = INT_TO_NATIVE(PLAYER_INDEX, -1)
			AND ref_comboScanner.sVehicleScannerStruct.iCurrentScanVeh = -1
				IF CAN_SCAN_THIS_PLAYER(ref_comboScanner, piClosest)
					ref_comboScanner.piCurrentScanTarget = piClosest
					TRIGGER_TARGET_FOUND_BLEEP()
				ELSE
					INT iVehLoop
					REPEAT MAX_SCANNER_VEHS iVehLoop
						IF VEHICLE_SCAN_CHECKS(ref_comboScanner.sVehicleScannerStruct, iVehLoop,50,0.1)
							TRIGGER_TARGET_FOUND_BLEEP()
							// We have a scan vehicle
							ref_comboScanner.sVehicleScannerStruct.iCurrentScanVeh = iVehLoop
							PRINTLN("[COMBO_SCANNER] - MAINTAIN_COMBO_SCANNER - New Current scan veh! ref_comboScanner.sVehicleScannerStruct.iCurrentScanVeh = ", iVehLoop)
						ENDIF
					ENDREPEAT
				ENDIF
			ELSE		
				//Keep track of the latest time that the closest = current
				//but must be piClosest must be valid 
				IF ref_comboScanner.piCurrentScanTarget != INT_TO_NATIVE(PLAYER_INDEX, -1)
					IF ref_comboScanner.piCurrentScanTarget = piClosest
					OR NOT CAN_SCAN_THIS_PLAYER(ref_comboScanner, piClosest)
						ref_comboScanner.tNewTargetTime = GET_NETWORK_TIME()
					ELSE
						PRINTLN("[COMBO_SCANNER] Keeping player ", NATIVE_TO_INT(ref_comboScanner.piCurrentScanTarget), " in focus for ",ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), ref_comboScanner.tNewTargetTime))," seconds before player ", NATIVE_TO_INT(piClosest), " takes over.")
					ENDIF
					
					IF CAN_SCAN_THIS_PLAYER(ref_comboScanner, ref_comboScanner.piCurrentScanTarget)
					AND NOT HAS_NETWORK_TIMER_EXPIRED(ref_comboScanner.tNewTargetTime, ciCOMBO_SCANNER_NEW_TARGET_TIME)
						BOOL bBoxShouldFlash = ref_comboScanner.piCurrentScanTarget <> piClosest
						IF SCAN_TARGET_PLAYER(ref_comboScanner, bBoxShouldFlash)
							//Mark this player as has-been-scanned
							SET_BIT(ref_comboScanner.iBsPlayerHasBeenScanned, NATIVE_TO_INT(ref_comboScanner.piCurrentScanTarget))
						
							// Target has been scanned, time to show the results of the scan to the player
							IF SHOW_SCAN_RESULTS(ref_comboScanner, bBoxShouldFlash)
								//Right now this never gets hit! @@ 
								CLEAR_BIT(ref_comboScanner.iBs, ciCOMBO_SCANNER_BS_SHOWING_RESULTS)
							ENDIF
						ENDIF		
					ELSE
						//RESET_NET_TIMER(ref_comboScanner.tNewTargetTime)
						CLEAR_SCALEFORM_DATA_BOX(ref_comboScanner)
						RESET_COMBO_SCANNER_TIMERS(ref_comboScanner)
						STOP_SCAN_RESULT_AUDIO(ref_comboScanner.sAudio)
						STOP_SCANNING_LOOP_AUDIO(ref_comboScanner.sAudio)
						ref_comboScanner.piCurrentScanTarget = INT_TO_NATIVE(PLAYER_INDEX,-1)
						PRINTLN("[COMBO_SCANNER] - MAINTAIN_COMBO_SCANNER - No longer looking at player to scan! ref_comboScanner.piCurrentScanTarget = -1")
					ENDIF
				ELIF ref_comboScanner.sVehicleScannerStruct.iCurrentScanVeh != -1
					IF VEHICLE_SCAN_CHECKS(ref_comboScanner.sVehicleScannerStruct, ref_comboScanner.sVehicleScannerStruct.iCurrentScanVeh,50,0.1)
					// We have a scan vehicle, lets to some scanning stuff
						IF SCAN_TARGET(ref_comboScanner.sVehicleScannerStruct)
							
							// Set that this vehicle has been scanned, so we now get thrown back to the outside loop after the results have been shown
							IF NOT HAS_VEHICLE_BEEN_SCANNED_WITH_VEHICLE_SCANNER(ref_comboScanner.sVehicleScannerStruct, ref_comboScanner.sVehicleScannerStruct.iCurrentScanVeh)
								SET_VEHICLE_HAS_BEEN_SCANNED_WITH_VEHICLE_SCANNER(ref_comboScanner.sVehicleScannerStruct, ref_comboScanner.sVehicleScannerStruct.iCurrentScanVeh)
							ENDIF
						
							// Target has been scanned, time to show the results of the scan to the player
							IF SHOW_RESULTS(ref_comboScanner.sVehicleScannerStruct)
								ref_comboScanner.sVehicleScannerStruct.bShowingResults = FALSE
							ENDIF
						ENDIF
					ELSE
						// We're no longer scanning this vehicle, lets to back to looking for another to scan
						RESET_SCAN_DATA(ref_comboScanner.sVehicleScannerStruct)
						STOP_ALL_SOUNDS(ref_comboScanner.sAudio)
						STOP_ALL_SOUNDS(ref_comboScanner.sVehicleScannerStruct.sAudio)
						ref_comboScanner.sVehicleScannerStruct.iCurrentScanVeh = -1
						PRINTLN("[VEHICLE_SCANNER] - MAINTAIN_VEHICLE_SCANNER - No longer looking at vehicle to scan! ref_comboScanner.sVehicleScannerStruct.iCurrentScanVeh = -1")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		CLEAR_SCALEFORM_DATA_BOX(ref_comboScanner)
		RESET_COMBO_SCANNER_TIMERS(ref_comboScanner)
		STOP_ALL_SOUNDS(ref_comboScanner.sAudio)
		STOP_ALL_SOUNDS(ref_comboScanner.sVehicleScannerStruct.sAudio)
		RESET_SCAN_DATA(ref_comboScanner.sVehicleScannerStruct)
		IF ref_comboScanner.sVehicleScannerStruct.iCurrentScanVeh = -1
			ref_comboScanner.sVehicleScannerStruct.iCurrentScanVeh = -1
		ENDIF
		ref_comboScanner.piCurrentScanTarget = INT_TO_NATIVE(PLAYER_INDEX,-1)
		PRINTLN("[COMBO_SCANNER] - MAINTAIN_COMBO_SCANNER - No longer safe for combo scanner")
	ENDIF
ENDPROC
