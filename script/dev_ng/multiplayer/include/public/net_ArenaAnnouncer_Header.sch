//╔═════════════════════════════════════════════════════════════════════════════╗
//║		net_ArenaAnnouncer_Header.sch											║
//║		Authored: Luke Moffoot													║
//╚═════════════════════════════════════════════════════════════════════════════╝

USING "net_include.sch"
USING "freemode_header.sch"

// [ArenaAnnouncer] START ========================================================================================

// I think quicker than using the native.
FUNC TEXT_LABEL_15 GET_LETTER_FOR_ARENA_ANNOUNCER_TL(INT i)
	TEXT_LABEL_15 tl15 = "a"	
	SWITCH i
		CASE 1			 tl15 = "A"		BREAK
		CASE 2			 tl15 = "B"		BREAK
		CASE 3			 tl15 = "C"		BREAK
		CASE 4			 tl15 = "D"		BREAK
		CASE 5			 tl15 = "E"		BREAK
		CASE 6			 tl15 = "F"		BREAK
		CASE 7			 tl15 = "G"		BREAK
		CASE 8			 tl15 = "H"		BREAK
		CASE 9			 tl15 = "I"		BREAK
		CASE 10			 tl15 = "J"		BREAK
		CASE 11			 tl15 = "K"		BREAK
		CASE 12			 tl15 = "L"		BREAK
		CASE 13			 tl15 = "M"		BREAK
		CASE 14			 tl15 = "N"		BREAK
		CASE 15			 tl15 = "O"		BREAK
		CASE 16			 tl15 = "P"		BREAK
		CASE 17			 tl15 = "Q"		BREAK
		CASE 18			 tl15 = "R"		BREAK
		CASE 19			 tl15 = "S"		BREAK
		CASE 20			 tl15 = "T"		BREAK
		CASE 21			 tl15 = "U"		BREAK
		CASE 22			 tl15 = "V"		BREAK
		CASE 23			 tl15 = "W"		BREAK
		CASE 24			 tl15 = "X"		BREAK
		CASE 25			 tl15 = "Y"		BREAK
		CASE 26			 tl15 = "Z"		BREAK
	ENDSWITCH
	RETURN tl15
ENDFUNC

// Get the number of how many random variations this sound / dialogue has. // How many announcers or A/B/C/D variations of a root
FUNC INT GET_GENERIC_SOUND_VARIATIONS(INT iSoundToPlay) 
	SWITCH iSoundToPlay
		CASE g_iAA_PlaySound_Generic_CountdownFFA						RETURN 5
		CASE g_iAA_PlaySound_Generic_CountdownTeams						RETURN 7
		CASE g_iAA_PlaySound_Generic_CountdownRace						RETURN 3
		CASE g_iAA_PlaySound_Generic_RoundAlmostOver					RETURN 1
		CASE g_iAA_PlaySound_Generic_RemoteGunTowerUseRedeemerKill		RETURN 1
		CASE g_iAA_PlaySound_Generic_RCCarKills							RETURN 1		
		CASE g_iAA_PlaySound_Generic_RemoteGunTowerAvailable 			RETURN 0
		CASE g_iAA_PlaySound_Generic_TrapCameraKill 					RETURN 1		
		CASE g_iAA_PlaySound_Generic_DestroyedCerberus					RETURN 1
		CASE g_iAA_PlaySound_Generic_BinkReactions_BeerSmash			RETURN 1
		CASE g_iAA_PlaySound_Generic_BinkReactions_Birth				RETURN 1
		CASE g_iAA_PlaySound_Generic_BinkReactions_Fight				RETURN 1
		CASE g_iAA_PlaySound_Generic_BinkReactions_Fight2				RETURN 1
		CASE g_iAA_PlaySound_Generic_BinkReactions_Rapper				RETURN 1
		CASE g_iAA_PlaySound_Generic_BinkReactions_Crazy				RETURN 1
		CASE g_iAA_PlaySound_Generic_BinkReactions_Divorce				RETURN 1
		CASE g_iAA_PlaySound_Generic_BinkReactions_EMT					RETURN 1
		CASE g_iAA_PlaySound_Generic_BinkReactions_Grind				RETURN 1
		CASE g_iAA_PlaySound_Generic_BinkReactions_Guns					RETURN 1
		CASE g_iAA_PlaySound_Generic_BinkReactions_Vomit				RETURN 1
		CASE g_iAA_PlaySound_Generic_BinkReactions_TXT					RETURN 1
		CASE g_iAA_PlaySound_Generic_BinkReactions_Noise				RETURN 1
		CASE g_iAA_PlaySound_Generic_PlayerRemainingCount_15			RETURN 1
		CASE g_iAA_PlaySound_Generic_PlayerRemainingCount_14			RETURN 1
		CASE g_iAA_PlaySound_Generic_PlayerRemainingCount_13			RETURN 1
		CASE g_iAA_PlaySound_Generic_PlayerRemainingCount_12			RETURN 1
		CASE g_iAA_PlaySound_Generic_PlayerRemainingCount_11			RETURN 1
		CASE g_iAA_PlaySound_Generic_PlayerRemainingCount_10			RETURN 1
		CASE g_iAA_PlaySound_Generic_PlayerRemainingCount_9				RETURN 1
		CASE g_iAA_PlaySound_Generic_PlayerRemainingCount_8				RETURN 1
		CASE g_iAA_PlaySound_Generic_PlayerRemainingCount_7				RETURN 1
		CASE g_iAA_PlaySound_Generic_PlayerRemainingCount_6				RETURN 1
		CASE g_iAA_PlaySound_Generic_PlayerRemainingCount_5				RETURN 1
		CASE g_iAA_PlaySound_Generic_PlayerRemainingCount_4				RETURN 1
		CASE g_iAA_PlaySound_Generic_PlayerRemainingCount_3				RETURN 1
		CASE g_iAA_PlaySound_Generic_PlayerRemainingCount_2				RETURN 1
	ENDSWITCH	
	RETURN 3
ENDFUNC
FUNC INT GET_MODE_SPECIFIC_TAG_TEAM_SOUND_VARIATIONS(INT iSoundToPlay)
	SWITCH iSoundToPlay
		CASE g_iAA_PlaySound_TagTeam_Intro				RETURN 3
	ENDSWITCH	
	RETURN 2
ENDFUNC
FUNC INT GET_MODE_SPECIFIC_GAMES_MASTERS_SOUND_VARIATIONS(INT iSoundToPlay)
	SWITCH iSoundToPlay
		CASE g_iAA_PlaySound_GameMasters_Intro			RETURN 3
		CASE g_iAA_PlaySound_GameMasters_AlmostOver		RETURN 1
	ENDSWITCH	
	RETURN 2
ENDFUNC
FUNC INT GET_MODE_SPECIFIC_BOMB_FOOTBALL_SOUND_VARIATIONS(INT iSoundToPlay)
	SWITCH iSoundToPlay
		CASE g_iAA_PlaySound_Detonation_Intro				RETURN 3
		CASE g_iAA_PlaySound_Detonation_EvenScoreAfter30Sec RETURN 1
	ENDSWITCH	
	RETURN 2
ENDFUNC
FUNC INT GET_MODE_SPECIFIC_MONSTER_JAM_SOUND_VARIATIONS(INT iSoundToPlay)
	SWITCH iSoundToPlay
		CASE g_iAA_PlaySound_MonsterJam_Intro			RETURN 3
		CASE g_iAA_PlaySound_MonsterJam_Countdown		RETURN 3
	ENDSWITCH	
	RETURN 2
ENDFUNC
FUNC INT GET_MODE_SPECIFIC_FIRE_STARTER_SOUND_VARIATIONS(INT iSoundToPlay)
	SWITCH iSoundToPlay
		CASE g_iAA_PlaySound_Generic_CountdownFFA		RETURN 3
	ENDSWITCH	
	RETURN 3
ENDFUNC
FUNC INT GET_MODE_SPECIFIC_ARENA_CTF_SOUND_VARIATIONS(INT iSoundToPlay)
	SWITCH iSoundToPlay
		CASE g_iAA_PlaySound_CTF_Intro					RETURN 3
	ENDSWITCH	
	RETURN 2
ENDFUNC
FUNC INT GET_MODE_SPECIFIC_ARCADE_RACE_SOUND_VARIATIONS(INT iSoundToPlay)
	SWITCH iSoundToPlay
		CASE g_iAA_PlaySound_ArcadeRace_Intro			RETURN 3
	ENDSWITCH	
	RETURN 2
ENDFUNC
FUNC INT GET_MODE_SPECIFIC_DESTRUCTION_DERBY_SOUND_VARIATIONS(INT iSoundToPlay)
	SWITCH iSoundToPlay
		CASE g_iAA_PlaySound_DestructionDerby_Intro		RETURN 3
	ENDSWITCH
	RETURN 2
ENDFUNC
FUNC INT GET_MODE_SPECIFIC_CARNAGE_SOUND_VARIATIONS(INT iSoundToPlay)
	IF GlobalServerBD_DM.iTypeOfDeathmatch = DEATHMATCH_TYPE_TDM
		SWITCH iSoundToPlay
			CASE g_iAA_PlaySound_Carnage_LTS_Intro		RETURN 3
		ENDSWITCH
	ELSE
		SWITCH iSoundToPlay
			CASE g_iAA_PlaySound_Carnage_Intro			RETURN 3
		ENDSWITCH
	ENDIF
	RETURN 2
ENDFUNC
FUNC INT GET_MODE_SPECIFIC_PASS_THE_BOMB_SOUND_VARIATIONS(INT iSoundToPlay)
	IF GlobalServerBD_DM.iTypeOfDeathmatch = DEATHMATCH_TYPE_TDM
		SWITCH iSoundToPlay
			CASE g_iAA_PlaySound_PassTheBomb_LTS_Intro	RETURN 3
		ENDSWITCH	
	ELSE
		SWITCH iSoundToPlay
			CASE g_iAA_PlaySound_PassTheBomb_Intro		RETURN 3
		ENDSWITCH	
	ENDIF
	RETURN 2
ENDFUNC
FUNC BOOL IS_ANNOUNCER_VARIATION_VALID_FOR_MODE(INt iVariation)
	IF iVariation = 0 // not assigned yet.
		RETURN FALSE
	ENDIF
	
	IF iVariation = 1
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType)
		IF iVariation = 2
			RETURN TRUE
		ENDIF
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GAMES_MASTERS(g_FMMC_STRUCT.iAdversaryModeType)
		IF iVariation = 3
			RETURN TRUE
		ENDIF
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
		IF iVariation = 3
			RETURN TRUE
		ENDIF
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
		IF iVariation = 3
			RETURN TRUE
		ENDIF
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
		IF iVariation = 2
			RETURN TRUE
		ENDIF
	ELIF IS_THIS_ARCADE_RACE_TYPE()
		IF iVariation = 2
			RETURN TRUE
		ENDIF
	ELIF IS_THIS_DESTRUCTION_DERBY_RACE_TYPE() 
		IF iVariation = 2
			RETURN TRUE
		ENDIF
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)		
		IF GlobalServerBD_DM.iTypeOfDeathmatch = DEATHMATCH_TYPE_TDM
			IF iVariation = 3
				RETURN TRUE
			ENDIF
		ELSE
			IF iVariation = 2
				RETURN TRUE
			ENDIF
		ENDIF
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		IF GlobalServerBD_DM.iTypeOfDeathmatch = DEATHMATCH_TYPE_TDM
			IF iVariation = 2
				RETURN TRUE
			ENDIF
		ELSE
			IF iVariation = 3
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_PLAYING_VALID_MODE_SPECIFIC_FOR_ANNOUNCER()
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GAMES_MASTERS(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ELIF IS_THIS_ARCADE_RACE_TYPE()
		RETURN TRUE
	ELIF IS_THIS_DESTRUCTION_DERBY_RACE_TYPE() 
		RETURN TRUE
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC INT GET_MODE_SPECIFIC_VARIATIONS_FOR_THIS_SOUND(INT iSoundToPlay)	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN GET_MODE_SPECIFIC_TAG_TEAM_SOUND_VARIATIONS(iSoundToPlay)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GAMES_MASTERS(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN GET_MODE_SPECIFIC_GAMES_MASTERS_SOUND_VARIATIONS(iSoundToPlay)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN GET_MODE_SPECIFIC_BOMB_FOOTBALL_SOUND_VARIATIONS(iSoundToPlay)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN GET_MODE_SPECIFIC_MONSTER_JAM_SOUND_VARIATIONS(iSoundToPlay)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN GET_MODE_SPECIFIC_ARENA_CTF_SOUND_VARIATIONS(iSoundToPlay)
	ELIF IS_THIS_ARCADE_RACE_TYPE()
		RETURN GET_MODE_SPECIFIC_ARCADE_RACE_SOUND_VARIATIONS(iSoundToPlay)
	ELIF IS_THIS_DESTRUCTION_DERBY_RACE_TYPE() 
		RETURN GET_MODE_SPECIFIC_DESTRUCTION_DERBY_SOUND_VARIATIONS(iSoundToPlay)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN GET_MODE_SPECIFIC_CARNAGE_SOUND_VARIATIONS(iSoundToPlay)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN GET_MODE_SPECIFIC_PASS_THE_BOMB_SOUND_VARIATIONS(iSoundToPlay)
	ENDIF
	RETURN 0
ENDFUNC

FUNC TEXT_LABEL_15 GET_ARENA_ANNOUNCER_MODE_SPECIFIC_BLOCK_TO_LOAD()
	TEXT_LABEL_15 tl15 = ""
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType)
		tl15 = "ARASAU"
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GAMES_MASTERS(g_FMMC_STRUCT.iAdversaryModeType)
		tl15 = "ARASAU"
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
		tl15 = "ARASAU"
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
		tl15 = "ARASAU"
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
		tl15 = "ARASAU"
	ELIF IS_THIS_ARCADE_RACE_TYPE()
		tl15 = "ARASAU"
	ELIF IS_THIS_DESTRUCTION_DERBY_RACE_TYPE() 
		tl15 = "ARASAU"
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
		tl15 = "ARASAU"
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		tl15 = "ARASAU"
	ENDIF
	RETURN tl15
ENDFUNC

FUNC TEXT_LABEL_15 GET_ARENA_ANNOUNCER_MODE_SPECIFIC_ROOT_TO_LOAD()
	TEXT_LABEL_15 tl15 = ""
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType)
		tl15 = "ARAS_TT_"
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GAMES_MASTERS(g_FMMC_STRUCT.iAdversaryModeType)
		tl15 = "ARAS_GM_"
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
		tl15 = "ARAS_DT_"
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
		tl15 = "ARAS_MJ_"
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
		tl15 = "ARAS_CTF_"
	ELIF IS_THIS_ARCADE_RACE_TYPE()
		tl15 = "ARAS_AC_"
	ELIF IS_THIS_DESTRUCTION_DERBY_RACE_TYPE() 
		tl15 = "ARAS_DD_"
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
		IF GlobalServerBD_DM.iTypeOfDeathmatch = DEATHMATCH_TYPE_TDM
			tl15 = "ARAS_CAT_"
		ELSE
			tl15 = "ARAS_CAR_"
		ENDIF
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		IF GlobalServerBD_DM.iTypeOfDeathmatch = DEATHMATCH_TYPE_TDM		
			tl15 = "ARAS_PBT_"
		ELSE
			tl15 = "ARAS_PBM_"
		ENDIF
	ENDIF
	RETURN tl15
ENDFUNC

// To alleviate Spammy Lines - This allows us to set a value per sound for how many times we need to attempt to call it before it'll actually play.
FUNC INT GET_ARENA_ANNOUNCER_MODE_SPECIFIC_SOUND_TRACK_COUNT_MAX(INT iSoundToPlay)
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType)
		SWITCH iSoundToPlay
			CASE g_iAA_PlaySound_TagTeam_PlayerTaggedOut RETURN 0
		ENDSWITCH		
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GAMES_MASTERS(g_FMMC_STRUCT.iAdversaryModeType)
		SWITCH iSoundToPlay
			CASE g_iAA_PlaySound_GameMasters_CheckpointCollected RETURN 4
		ENDSWITCH	
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
		SWITCH iSoundToPlay
			CASE g_iAA_PlaySound_Detonation_Goal RETURN 0
		ENDSWITCH	
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
		SWITCH iSoundToPlay
			CASE g_iAA_PlaySound_MonsterJam_Countdown RETURN 0
		ENDSWITCH
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
		SWITCH iSoundToPlay
			CASE g_iAA_PlaySound_CTF_FlagStolenFromBase RETURN 0
		ENDSWITCH	
	ELIF IS_THIS_ARCADE_RACE_TYPE()
		SWITCH iSoundToPlay
			CASE g_iAA_PlaySound_ArcadeRace_FirstBlood RETURN 0
		ENDSWITCH	
	ELIF IS_THIS_DESTRUCTION_DERBY_RACE_TYPE() 
		SWITCH iSoundToPlay
			CASE g_iAA_PlaySound_DestructionDerby_Intro 					RETURN 0
			CASE g_iAA_PlaySound_DestructionDerby_LapCompletedByPlayer1st 	RETURN 3
			CASE g_iAA_PlaySound_DestructionDerby_MoreTraps 				RETURN 5
		ENDSWITCH	
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
		SWITCH iSoundToPlay
			CASE g_iAA_PlaySound_Carnage_FirstBlood RETURN 0
		ENDSWITCH	
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		SWITCH iSoundToPlay
			CASE g_iAA_PlaySound_PassTheBomb_PassedTheBomb RETURN 0
		ENDSWITCH		
	ENDIF
	
	RETURN 0
ENDFUNC

// To alleviate Spammy Lines
FUNC INT GET_ARENA_ANNOUNCER_GENERIC_SOUND_TRACK_COUNT_MAX(INT iSoundToPlay)
	SWITCH iSoundToPlay
		CASE g_iAA_PlaySound_Generic_CountdownFFA RETURN 0
		CASE g_iAA_PlaySound_Generic_BigAir RETURN 2
		CASE g_iAA_PlaySound_Generic_HeadOnCollision RETURN 2		
		CASE g_iAA_PlaySound_Generic_ArenaWheelSpin RETURN 3
		CASE g_iAA_PlaySound_Generic_DroneSpawn RETURN 3
		CASE g_iAA_PlaySound_Generic_RCCarSpawn RETURN 3
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC INT GET_ARENA_ANNOUNCER_SPECIFIC_CLEAR_LAST_PLAYED_TIME(INT iSoundToPlay)
	IF IS_THIS_DESTRUCTION_DERBY_RACE_TYPE() 
		SWITCH iSoundToPlay
			CASE g_iAA_PlaySound_DestructionDerby_PitstopHealedFully RETURN 5
			CASE g_iAA_PlaySound_DestructionDerby_PitstopBeingUsed	 RETURN 15
			CASE g_iAA_PlaySound_DestructionDerby_LapCompletedByPlayer1st RETURN 20
		ENDSWITCH
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC BOOL HAS_ENOUGH_TIME_PASSED_SINCE_LAST_PLAY(BOOL bSpecific, INT iSoundToPlay)
	IF bSpecific
		IF GET_ARENA_ANNOUNCER_SPECIFIC_CLEAR_LAST_PLAYED_TIME(iSoundToPlay) > 0
		AND (GET_CLOUD_TIME_AS_INT() - g_iAA_PlaySound_ModeSpec_Timestamp[iSoundToPlay]) >= GET_ARENA_ANNOUNCER_SPECIFIC_CLEAR_LAST_PLAYED_TIME(iSoundToPlay)
			PRINTLN("HAS_ENOUGH_TIME_PASSED_SINCE_LAST_PLAY - Returning TRUE for iSoundToPlay: ", iSoundToPlay)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_ARENA_ANNOUNCER_GENERIC_CLEAR_TIME(INT iSoundToPlay)
	UNUSED_PARAMETER(iSoundToPlay)
	
	RETURN 10
ENDFUNC

FUNC INT GET_ARENA_ANNOUNCER_SPECIFIC_CLEAR_TIME(INT iSoundToPlay)
	IF IS_THIS_DESTRUCTION_DERBY_RACE_TYPE() 
		SWITCH iSoundToPlay
			CASE g_iAA_PlaySound_DestructionDerby_PitstopHealedFully RETURN 3
		ENDSWITCH
	ENDIF
	
	RETURN 10
ENDFUNC

FUNC BOOL HAS_ENOUGH_TIME_PASSED_TO_CLEAR_SOUND(BOOL bSpecific, INT iSoundToPlay)
	IF NOT bSpecific
		IF GET_ARENA_ANNOUNCER_GENERIC_CLEAR_TIME(iSoundToPlay) > 0
		AND (GET_CLOUD_TIME_AS_INT() - g_iAA_PlaySound_Generic_Timestamp[iSoundToPlay]) >= GET_ARENA_ANNOUNCER_GENERIC_CLEAR_TIME(iSoundToPlay)
			PRINTLN("HAS_ENOUGH_TIME_PASSED_SINCE_TO_CLEAR_SOUND (Generic) - Returning TRUE for iSoundToPlay: ", iSoundToPlay)
			RETURN TRUE
		ENDIF
	ELSE
		IF GET_ARENA_ANNOUNCER_SPECIFIC_CLEAR_TIME(iSoundToPlay) > 0
		AND (GET_CLOUD_TIME_AS_INT() - g_iAA_PlaySound_ModeSpec_Timestamp[iSoundToPlay]) >= GET_ARENA_ANNOUNCER_SPECIFIC_CLEAR_TIME(iSoundToPlay)
			PRINTLN("HAS_ENOUGH_TIME_PASSED_SINCE_TO_CLEAR_SOUND (Mode Specific) - Returning TRUE for iSoundToPlay: ", iSoundToPlay)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// Are we playing arena content
FUNC BOOL CONTENT_IS_USING_ARENA_PRIVATE()
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, 12)//INTERIOR2_ARENA
ENDFUNC

FUNC BOOL SHOULD_ARENA_ANNOUNCER_GENERIC_LINE_BE_SINGULAR(INT iSoundToPlay)
	IF iSoundToPlay < 32
		RETURN IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic_MakeSingular, iSoundToPlay)
	ELSE
		RETURN IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic_2_MakeSingular, iSoundToPlay-32)
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC BOOL SHOULD_ARENA_ANNOUNCER_GENERIC_LINE_BE_PLURAL(INT iSoundToPlay)
	IF iSoundToPlay < 32
		RETURN IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic_MakePlural, iSoundToPlay)
	ELSE
		RETURN IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic_2_MakePlural, iSoundToPlay-32)
	ENDIF	
	RETURN FALSE
ENDFUNC
FUNC BOOL SHOULD_ARENA_ANNOUNCER_MODE_SPECIFIC_LINE_BE_SINGULAR(INT iSoundToPlay)
	IF iSoundToPlay < 32
		RETURN IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific_MakeSingular, iSoundToPlay)
	ELSE
		RETURN IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific_2_MakeSingular, iSoundToPlay-32)
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC BOOL SHOULD_ARENA_ANNOUNCER_MODE_SPECIFIC_LINE_BE_PLURAL(INT iSoundToPlay)
	IF iSoundToPlay < 32
		RETURN IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific_MakePlural, iSoundToPlay)
	ELSE
		RETURN IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific_2_MakePlural, iSoundToPlay-32)
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CURRENT_ANNOUNCER_GENERIC_PLAYING()
	IF g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = ci_Arena_Announcer_DataTypeForEvent_Generic
		RETURN g_iArenaAnnouncer_iSoundToPlayQueue[0]
	ENDIF
	RETURN -1
ENDFUNC

FUNC INT GET_CURRENT_ANNOUNCER_MODE_SPECIFIC_PLAYING()
	IF g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = ci_Arena_Announcer_DataTypeForEvent_ModeSpecific
		RETURN g_iArenaAnnouncer_iSoundToPlayQueue[0]
	ENDIF
	RETURN -1
ENDFUNC

PROC SET_ARENA_ANNOUNCER_PRIORITIZE_SOUND(INT iTypeSound, INT iSoundToPlay)
	IF iSoundToPlay < 32
		SET_BIT(g_iArenaAnnouncer_iSoundToPrioritize[iTypeSound], iSoundToPlay)
	ELSE
		SET_BIT(g_iArenaAnnouncer_iSoundToPrioritize_2[iTypeSound], iSoundToPlay-32)
	ENDIF
	PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_PRIORITIZE_SOUND] - Prioritize iSoundToPlay: ", iSoundToPlay, " iTypeSound: ", iTypeSound)
ENDPROC

FUNC BOOL IS_ARENA_ANNOUNCER_PRIORITIZE_SOUND_SET(INT iTypeSound, INT iSoundToPlay)
	IF iSoundToPlay < 32
		RETURN IS_BIT_SET(g_iArenaAnnouncer_iSoundToPrioritize[iTypeSound], iSoundToPlay)
	ELSE
		RETURN IS_BIT_SET(g_iArenaAnnouncer_iSoundToPrioritize_2[iTypeSound], iSoundToPlay-32)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_ARENA_ANNOUNCER_BS_CONTROL(INT iBit)
	IF NOT CONTENT_IS_USING_ARENA_PRIVATE()
		EXIT
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
	IF NOT IS_BIT_SET(g_iArenaAnnouncer_ControlBS, iBit)
		PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_CONTROL] - Setting iBit: ", iBit)
		SET_BIT(g_iArenaAnnouncer_ControlBS, iBit)
	ELSE
		PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_CONTROL] - Aleardy Set! - iBit: ", iBit)
	ENDIF
ENDPROC
PROC CLEAR_ARENA_ANNOUNCER_BS_CONTROL(INT iBit)
	DEBUG_PRINTCALLSTACK()	
	IF IS_BIT_SET(g_iArenaAnnouncer_ControlBS, iBit)
		PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_CONTROL] - Clearing iBit: ", iBit)
		CLEAR_BIT(g_iArenaAnnouncer_ControlBS, iBit)
	ELSE
		PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_CONTROL] - This is not set! - iBit: ", iBit)
	ENDIF
ENDPROC
FUNC BOOL IS_ARENA_ANNOUNCER_BS_GENERAL_LOCK_SET(INT iSoundToPlay)	
	IF iSoundToPlay < 32
		RETURN IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic_Play_Once_Lock, iSoundToPlay)
	ELSE
		RETURN IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic_2_Play_Once_Lock, iSoundToPlay-32)
	ENDIF
ENDFUNC
FUNC BOOL IS_ARENA_ANNOUNCER_BS_GENERAL_LOCKED(INT iSoundToPlay)	
	IF iSoundToPlay < 32
		RETURN (IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic_Play_Once_Lock, iSoundToPlay) AND IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic_Played, iSoundToPlay))
	ELSE
		RETURN (IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic_2_Play_Once_Lock, iSoundToPlay-32) AND IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic_2_Played, iSoundToPlay-32))
	ENDIF
ENDFUNC
FUNC BOOL IS_ARENA_ANNOUNCER_BS_GENERAL_SET(INT iSoundToPlay)	
	IF iSoundToPlay < 32
		RETURN IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic, iSoundToPlay)
	ELSE
		RETURN IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic_2, iSoundToPlay-32)
	ENDIF
ENDFUNC
PROC SET_ARENA_ANNOUNCER_BS_GENERAL_LOCKED(INT iSoundToPlay)
	IF NOT CONTENT_IS_USING_ARENA_PRIVATE()
		EXIT
	ENDIF
	DEBUG_PRINTCALLSTACK()
	IF iSoundToPlay < 32
		IF NOT IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic_Played, iSoundToPlay)
			PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_GENERAL_LOCKED] - (1) Setting iSoundToPlay: ", iSoundToPlay, " AS NOW PLAYED / LOCKED")
			SET_BIT(g_iArenaAnnouncer_PlaySound_Generic_Played, iSoundToPlay)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic_2_Played, iSoundToPlay-32)
			PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_GENERAL_LOCKED] - (2) Setting iSoundToPlay: ", iSoundToPlay, " AS NOW PLAYED / LOCKED")
			SET_BIT(g_iArenaAnnouncer_PlaySound_Generic_2_Played, iSoundToPlay-32)
		ENDIF
	ENDIF
ENDPROC
PROC SET_ARENA_ANNOUNCER_BS_GENERAL(INT iSoundToPlay, BOOL bPlayOnce = FALSE, BOOL bMakeSingular = FALSE, BOOL bMakePlural = FALSE, BOOL bInterrupt = FALSE)
	IF NOT CONTENT_IS_USING_ARENA_PRIVATE()
		EXIT
	ENDIF
	IF IS_ARENA_ANNOUNCER_BS_GENERAL_LOCKED(iSoundToPlay)
	OR IS_ARENA_ANNOUNCER_BS_GENERAL_LOCK_SET(iSoundToPlay)
		EXIT
	ENDIF
	IF g_iArenaAnnouncer_LastPlayed_Generic = iSoundToPlay
		PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_GENERAL] - (NOT) Setting iSoundToPlay: ", iSoundToPlay, " Because we've just played that one!")
		EXIT
	ENDIF

	DEBUG_PRINTCALLSTACK()
	IF iSoundToPlay < 32
		IF NOT IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic, iSoundToPlay)
			PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_GENERAL] - (1) Setting iSoundToPlay: ", iSoundToPlay, " bPlayOnce: ", bPlayOnce, " bMakeSingular: ", bMakeSingular, " bMakePlural: ", bMakePlural)
			SET_BIT(g_iArenaAnnouncer_PlaySound_Generic, iSoundToPlay)
			IF bPlayOnce
				SET_BIT(g_iArenaAnnouncer_PlaySound_Generic_Play_Once_Lock, iSoundToPlay)
			ENDIF			
			IF bMakeSingular
				SET_BIT(g_iArenaAnnouncer_PlaySound_Generic_MakeSingular, iSoundToPlay)
			ENDIF
			IF bMakePlural
				SET_BIT(g_iArenaAnnouncer_PlaySound_Generic_MakePlural, iSoundToPlay)
			ENDIF			
			IF bInterrupt
				SET_ARENA_ANNOUNCER_PRIORITIZE_SOUND(0, iSoundToPlay)
			ENDIF
			g_iAA_PlaySound_Generic_Timestamp[iSoundToPlay] = GET_CLOUD_TIME_AS_INT()
			PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_GENERAL] - Time stamp set as ", g_iAA_PlaySound_Generic_Timestamp[iSoundToPlay], " for sound ", iSoundToPlay)
		ELSE
			IF NOT bPlayOnce
				PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_GENERAL] - (1) Aleardy Set / Waiting to play! - iSoundToPlay: ", iSoundToPlay, " bMakeSingular: ", bMakeSingular, " bMakePlural: ", bMakePlural)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic_2, iSoundToPlay-32)
			PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_GENERAL] - (2) Setting iSoundToPlay: ", iSoundToPlay, " bPlayOnce: ", bPlayOnce, " bMakeSingular: ", bMakeSingular, " bMakePlural: ", bMakePlural)
			SET_BIT(g_iArenaAnnouncer_PlaySound_Generic_2, iSoundToPlay-32)
			IF bPlayOnce
				SET_BIT(g_iArenaAnnouncer_PlaySound_Generic_2_Play_Once_Lock, iSoundToPlay-32)	
			ENDIF
			IF bMakeSingular
				SET_BIT(g_iArenaAnnouncer_PlaySound_Generic_2_MakeSingular, iSoundToPlay-32)
			ENDIF
			IF bMakePlural
				SET_BIT(g_iArenaAnnouncer_PlaySound_Generic_2_MakePlural, iSoundToPlay-32)
			ENDIF
			IF bInterrupt
				SET_ARENA_ANNOUNCER_PRIORITIZE_SOUND(0, iSoundToPlay)
			ENDIF
			
			g_iAA_PlaySound_Generic_Timestamp[iSoundToPlay] = GET_CLOUD_TIME_AS_INT()
			PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_GENERAL] - Time stamp set as ", g_iAA_PlaySound_Generic_Timestamp[iSoundToPlay], " for sound ", iSoundToPlay)
		ELSE
			IF NOT bPlayOnce
				PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_GENERAL] - (2) Aleardy Set / Waiting to play! - iSoundToPlay: ", iSoundToPlay, " bMakeSingular: ", bMakeSingular, " bMakePlural: ", bMakePlural)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
PROC CLEAR_ARENA_ANNOUNCER_BS_GENERAL_LOCKED(INT iSoundToPlay)
	DEBUG_PRINTCALLSTACK()
	IF iSoundToPlay < 32
		IF IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic_Play_Once_Lock, iSoundToPlay)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_Generic_Play_Once_Lock, iSoundToPlay)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_Generic_Played, iSoundToPlay)
			PRINTLN("[LM][ArenaAnnouncer][CLEAR_ARENA_ANNOUNCER_BS_GENERAL_LOCKED] - (UNLOCKING) Clearing iSoundToPlay: ", iSoundToPlay)
		ENDIF
	ELSE
		IF IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic_2_Play_Once_Lock, iSoundToPlay-32)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_Generic_2_Play_Once_Lock, iSoundToPlay-32)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_Generic_2_Played, iSoundToPlay-32)
			PRINTLN("[LM][ArenaAnnouncer][CLEAR_ARENA_ANNOUNCER_BS_GENERAL_LOCKED] - (UNLOCKING) Clearing iSoundToPlay: ", iSoundToPlay)
		ENDIF
	ENDIF
ENDPROC
PROC CLEAR_ARENA_ANNOUNCER_BS_GENERAL(INT iSoundToPlay)
	DEBUG_PRINTCALLSTACK()
	IF iSoundToPlay < 32
		IF IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic, iSoundToPlay)
			PRINTLN("[LM][ArenaAnnouncer][CLEAR_ARENA_ANNOUNCER_BS_GENERAL] - Clearing iSoundToPlay: ", iSoundToPlay)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_Generic, iSoundToPlay)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_Generic_MakeSingular, iSoundToPlay)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_Generic_MakePlural, iSoundToPlay)
		ELSE
			PRINTLN("[LM][ArenaAnnouncer][CLEAR_ARENA_ANNOUNCER_BS_GENERAL] - This is not set! - iSoundToPlay: ", iSoundToPlay)
		ENDIF
	ELSE
		IF IS_BIT_SET(g_iArenaAnnouncer_PlaySound_Generic_2, iSoundToPlay-32)
			PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_GENERAL] - Clearing iSoundToPlay: ", iSoundToPlay)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_Generic_2, iSoundToPlay-32)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_Generic_2_MakeSingular, iSoundToPlay-32)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_Generic_2_MakePlural, iSoundToPlay-32)
		ELSE
			PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_GENERAL] - This is not set! - iSoundToPlay:  ", iSoundToPlay)
		ENDIF
	ENDIF
ENDPROC
FUNC BOOL IS_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCK_SET(INT iSoundToPlay)
	IF iSoundToPlay < 32
		RETURN IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific_Play_Once_Lock, iSoundToPlay)
	ELSE
		RETURN IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific_2_Play_Once_Lock, iSoundToPlay-32)
	ENDIF
ENDFUNC
FUNC BOOL IS_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED(INT iSoundToPlay)
	IF iSoundToPlay < 32
		RETURN (IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific_Play_Once_Lock, iSoundToPlay) AND IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific_Played, iSoundToPlay))
	ELSE
		RETURN (IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific_2_Play_Once_Lock, iSoundToPlay-32) AND IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific_2_Played, iSoundToPlay-32))
	ENDIF
ENDFUNC
FUNC BOOL IS_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_SET(INT iSoundToPlay)
	IF iSoundToPlay < 32
		RETURN IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific, iSoundToPlay)
	ELSE
		RETURN IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific_2, iSoundToPlay-32)
	ENDIF
ENDFUNC
PROC SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED(INT iSoundToPlay)
	IF NOT CONTENT_IS_USING_ARENA_PRIVATE()
		EXIT
	ENDIF
	DEBUG_PRINTCALLSTACK()
	IF iSoundToPlay < 32
		IF NOT IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific_Played, iSoundToPlay)
			PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED] - (1) Setting iSoundToPlay: ", iSoundToPlay, " AS NOW PLAYED / LOCKED")
			SET_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_Played, iSoundToPlay)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific_2_Played, iSoundToPlay-32)
			PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED] - (2) Setting iSoundToPlay: ", iSoundToPlay, " AS NOW PLAYED / LOCKED")
			SET_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_2_Played, iSoundToPlay-32)
		ENDIF
	ENDIF
ENDPROC
PROC SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(INT iSoundToPlay, BOOL bPlayOnce = FALSE, BOOL bMakeSingular = FALSE, BOOL bMakePlural = FALSE, BOOL bInterrupt = FALSE)
	IF NOT CONTENT_IS_USING_ARENA_PRIVATE()
		EXIT
	ENDIF
	IF IS_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED(iSoundToPlay)
	OR IS_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCK_SET(iSoundToPlay)
		EXIT
	ENDIF
	IF NOT IS_PLAYING_VALID_MODE_SPECIFIC_FOR_ANNOUNCER()
		EXIT
	ENDIF
	
	IF g_iArenaAnnouncer_LastPlayed_ModeSpecific = iSoundToPlay
		IF HAS_ENOUGH_TIME_PASSED_SINCE_LAST_PLAY(TRUE, iSoundToPlay)
			g_iArenaAnnouncer_LastPlayed_ModeSpecific = -1
			PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC] - Clearing g_iArenaAnnouncer_LastPlayed_ModeSpecific as enough time has passed")
		ELSE
			PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC] - (NOT) Setting iSoundToPlay: ", iSoundToPlay, " Because we've just played that one!")
			EXIT
		ENDIF
	ENDIF

	DEBUG_PRINTCALLSTACK()
	IF iSoundToPlay < 32
		IF NOT IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific, iSoundToPlay)
			PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC] - (1) Setting iSoundToPlay: ", iSoundToPlay, " bPlayOnce: ", bPlayOnce, " bMakeSingular: ", bMakeSingular, " bMakePlural: ", bMakePlural)
			SET_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific, iSoundToPlay)
			IF bPlayOnce
				SET_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_Play_Once_Lock, iSoundToPlay)
			ENDIF
			IF bMakeSingular
				SET_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_MakeSingular, iSoundToPlay)
			ENDIF
			IF bMakePlural
				SET_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_MakePlural, iSoundToPlay)
			ENDIF
			IF bInterrupt
				SET_ARENA_ANNOUNCER_PRIORITIZE_SOUND(1, iSoundToPlay)
			ENDIF
			g_iAA_PlaySound_ModeSpec_Timestamp[iSoundToPlay] = GET_CLOUD_TIME_AS_INT()
			PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC] - Time stamp set as ", g_iAA_PlaySound_ModeSpec_Timestamp[iSoundToPlay], " for sound ", iSoundToPlay)
		ELSE
			IF NOT bPlayOnce
				PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC] - (1) Aleardy Set / Waiting to play! - iSoundToPlay: ", iSoundToPlay, " bMakeSingular: ", bMakeSingular, " bMakePlural: ", bMakePlural)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific_2, iSoundToPlay-32)
			PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC] - (2) Setting iSoundToPlay: ", iSoundToPlay, " bPlayOnce: ", bPlayOnce, " bMakeSingular: ", bMakeSingular, " bMakePlural: ", bMakePlural)
			SET_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_2, iSoundToPlay-32)
			IF bPlayOnce
				SET_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_2_Play_Once_Lock, iSoundToPlay-32)
			ENDIF
			IF bMakeSingular
				SET_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_2_MakeSingular, iSoundToPlay-32)
			ENDIF
			IF bMakePlural
				SET_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_2_MakePlural, iSoundToPlay-32)
			ENDIF
			IF bInterrupt
				SET_ARENA_ANNOUNCER_PRIORITIZE_SOUND(1, iSoundToPlay)
			ENDIF
			g_iAA_PlaySound_ModeSpec_Timestamp[iSoundToPlay] = GET_CLOUD_TIME_AS_INT()
		ELSE
			IF NOT bPlayOnce
				PRINTLN("[LM][ArenaAnnouncer][SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC] - (2) Aleardy Set / Waiting to play! - iSoundToPlay: ", iSoundToPlay, " bMakeSingular: ", bMakeSingular, " bMakePlural: ", bMakePlural)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
PROC CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED(INT iSoundToPlay)
	DEBUG_PRINTCALLSTACK()
	IF iSoundToPlay < 32
		IF IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific_Play_Once_Lock, iSoundToPlay)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_Play_Once_Lock, iSoundToPlay)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_Played, iSoundToPlay)
			PRINTLN("[LM][ArenaAnnouncer][CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED] - (UNLOCKING) Clearing iSoundToPlay: ", iSoundToPlay)
		ENDIF
	ELSE
		IF IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific_2_Play_Once_Lock, iSoundToPlay-32)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_2_Play_Once_Lock, iSoundToPlay-32)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_2_Played, iSoundToPlay-32)
			PRINTLN("[LM][ArenaAnnouncer][CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED] - (UNLOCKING) Clearing iSoundToPlay: ", iSoundToPlay)
		ENDIF
	ENDIF
ENDPROC
PROC CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(INT iSoundToPlay)
	DEBUG_PRINTCALLSTACK()
	IF iSoundToPlay < 32
		IF IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific, iSoundToPlay)
			PRINTLN("[LM][ArenaAnnouncer][CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC] - Clearing iSoundToPlay: ", iSoundToPlay)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific, iSoundToPlay)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_MakeSingular, iSoundToPlay)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_MakePlural, iSoundToPlay)
		ELSE
			PRINTLN("[LM][ArenaAnnouncer][CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC] - This is not set! - iSoundToPlay: ", iSoundToPlay)
		ENDIF
	ELSE
		IF IS_BIT_SET(g_iArenaAnnouncer_PlaySound_ModeSpecific_2, iSoundToPlay-32)
			PRINTLN("[LM][ArenaAnnouncer][CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC] - Clearing iSoundToPlay: ", iSoundToPlay)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_2, iSoundToPlay-32)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_2_MakeSingular, iSoundToPlay-32)
			CLEAR_BIT(g_iArenaAnnouncer_PlaySound_ModeSpecific_2_MakePlural, iSoundToPlay-32)
		ELSE
			PRINTLN("[LM][ArenaAnnouncer][CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC] - This is not set! - iSoundToPlay:  ", iSoundToPlay)
		ENDIF
	ENDIF
ENDPROC

// This wrapper to broadcast the event is used so that we can instantly locally check if it's set and avoid event spamming.
PROC BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(INT iType, INT iSoundToPlay, BOOL bPlayOnce = FALSE, BOOL bMakeSingular = FALSE, BOOL bMakePlural = FALSE, BOOL bForceBroadcast = FALSE, BOOL bInterrupt = FALSE)
	BOOL bBroadcast
	
	IF bForceBroadcast
		PRINTLN("[LM][ArenaAnnouncer] -----------------------BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS | !! bForceBroadcast !! ---------------------")
	ENDIF
	
	// This is so that we can immediately locally check that we've set it as to not cause event spam.
	IF iType = ci_Arena_Announcer_DataTypeForEvent_Control		
		IF NOT IS_BIT_SET(g_iArenaAnnouncer_ControlBS, iSoundToPlay)
		OR bForceBroadcast
			PRINTLN("[LM][ArenaAnnouncer] -----------------------BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS | SET_ARENA_ANNOUNCER_BS_CONTROL | Setting Locally ---------------------")
			bBroadcast = TRUE
			SET_ARENA_ANNOUNCER_BS_CONTROL(iSoundToPlay)
		ENDIF
	ELIF iType = ci_Arena_Announcer_DataTypeForEvent_Generic		
		IF (NOT IS_ARENA_ANNOUNCER_BS_GENERAL_SET(iSoundToPlay)
		AND NOT IS_ARENA_ANNOUNCER_BS_GENERAL_LOCK_SET(iSoundToPlay)
		AND g_iArenaAnnouncer_LastPlayed_Generic != iSoundToPlay)
		OR bForceBroadcast
			PRINTLN("[LM][ArenaAnnouncer] -----------------------BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS | SET_ARENA_ANNOUNCER_BS_GENERAL | Setting Locally ---------------------")
			bBroadcast = TRUE
			SET_ARENA_ANNOUNCER_BS_GENERAL(iSoundToPlay, bPlayOnce, bMakeSingular, bMakePlural)		
			IF bInterrupt
				SET_ARENA_ANNOUNCER_PRIORITIZE_SOUND(0, iSoundToPlay)
			ENDIF
		ENDIF
	ELIF iType = ci_Arena_Announcer_DataTypeForEvent_ModeSpecific		
		IF (NOT IS_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_SET(iSoundToPlay)
		AND NOT IS_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCK_SET(iSoundToPlay)
		AND g_iArenaAnnouncer_LastPlayed_ModeSpecific != iSoundToPlay)
		OR bForceBroadcast
			IF IS_PLAYING_VALID_MODE_SPECIFIC_FOR_ANNOUNCER()
				PRINTLN("[LM][ArenaAnnouncer] -----------------------BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS | SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC | Setting Locally ---------------------")
				bBroadcast = TRUE
				SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(iSoundToPlay, bPlayOnce, bMakeSingular, bMakePlural)
				IF bInterrupt
					SET_ARENA_ANNOUNCER_PRIORITIZE_SOUND(1, iSoundToPlay)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bBroadcast
	OR bForceBroadcast
		BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS_EVENT(iType, iSoundToPlay, bPlayOnce, bMakeSingular, bMakePlural, bInterrupt)
	ENDIF
ENDPROC
FUNC BOOL LOAD_AND_INITIALIZE_ARENA_ANNOUNCER()
	IF NOT CONTENT_IS_USING_ARENA_PRIVATE()
		RETURN TRUE
	ENDIF
	
	PRINTLN("[LM][ArenaAnnouncer][LOAD_AND_INITIALIZE_ARENA_ANNOUNCER] - Loading...")
	
	// Init Variables.
	INT i = 0
	FOR i = 0 TO ci_ARENA_ANNOUNCER_QUEUE_MAX-1
		g_iArenaAnnouncer_iSoundToPlayQueue[i] = -1
	ENDFOR
	FOR i = 0 TO g_iAA_PlaySound_Generic_MAX-1
		g_iAA_PlaySound_Generic_Tracker[i] = -1
	ENDFOR
	FOR i = 0 TO g_iAA_PlaySound_ModeSpecific_MAX-1
		g_iAA_PlaySound_ModeSpec_Tracker[i] = -1
	ENDFOR	
	
	// Load Assets
	g_iLastAnnouncementTimeStamp = -1
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType)
		REQUEST_SCRIPT_AUDIO_BANK("DLC_AWXM2018/AJ_Interrupts")
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GAMES_MASTERS(g_FMMC_STRUCT.iAdversaryModeType)
		REQUEST_SCRIPT_AUDIO_BANK("DLC_AWXM2018/AV_Interrupts")
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
		REQUEST_SCRIPT_AUDIO_BANK("DLC_AWXM2018/AV_Interrupts")
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
		REQUEST_SCRIPT_AUDIO_BANK("DLC_AWXM2018/AV_Interrupts")
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
		REQUEST_SCRIPT_AUDIO_BANK("DLC_AWXM2018/AJ_Interrupts")
	ELIF IS_THIS_ARCADE_RACE_TYPE()
		REQUEST_SCRIPT_AUDIO_BANK("DLC_AWXM2018/AJ_Interrupts")
	ELIF IS_THIS_DESTRUCTION_DERBY_RACE_TYPE() 
		REQUEST_SCRIPT_AUDIO_BANK("DLC_AWXM2018/AJ_Interrupts")
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)	
		IF GlobalServerBD_DM.iTypeOfDeathmatch = DEATHMATCH_TYPE_TDM
			REQUEST_SCRIPT_AUDIO_BANK("DLC_AWXM2018/AV_Interrupts")
		ELSE
			REQUEST_SCRIPT_AUDIO_BANK("DLC_AWXM2018/AJ_Interrupts")
		ENDIF
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		IF GlobalServerBD_DM.iTypeOfDeathmatch = DEATHMATCH_TYPE_TDM
			REQUEST_SCRIPT_AUDIO_BANK("DLC_AWXM2018/AJ_Interrupts")
		ELSE
			REQUEST_SCRIPT_AUDIO_BANK("DLC_AWXM2018/AV_Interrupts")
		ENDIF
	ENDIF
	
	PRINTLN("[LM][ArenaAnnouncer][LOAD_AND_INITIALIZE_ARENA_ANNOUNCER] - Loaded - Returning TRUE!")
	RETURN TRUE
ENDFUNC

PROC CLEANUP_ARENA_ANNOUNCER_VARIABLES(BOOL bInit = FALSE)
	IF bInit
		PRINTLN("[LM][ArenaAnnouncer][CLEANUP_ARENA_ANNOUNCER_VARIABLES] - INITIALIZING ARENA ANNOUNCER VARIABLES!")
	ELSE
		PRINTLN("[LM][ArenaAnnouncer][CLEANUP_ARENA_ANNOUNCER_VARIABLES] - CLEANING UP ARENA ANNOUNCER!")
	ENDIF
	INT i = 0
	FOR i = 0 TO ci_ARENA_ANNOUNCER_QUEUE_MAX-1
		g_iArenaAnnouncer_iSoundToPlayQueue[i] = -1		
		g_iArenaAnnouncer_iSoundTypeToPlayQueue[i] = -1
	ENDFOR
	FOR i = 0 TO g_iAA_PlaySound_Generic_MAX-1
		g_iAA_PlaySound_Generic_Tracker[i] = 0
	ENDFOR
	FOR i = 0 TO g_iAA_PlaySound_ModeSpecific_MAX-1
		g_iAA_PlaySound_ModeSpec_Tracker[i] = 0
		g_iAA_PlaySound_ModeSpec_Timestamp[i] = -1
	ENDFOR		
	g_iArenaAnnouncer_ControlBS = 0
	g_iArenaAnnouncer_PlaySound_ModeSpecific = 0
	g_iArenaAnnouncer_PlaySound_Generic = 0
	g_iArenaAnnouncer_PlaySound_Generic_2 = 0
	g_iArenaAnnouncer_PlaySound_ModeSpecific_2 = 0
	g_iArenaAnnouncer_PlaySound_Generic_Play_Once_Lock = 0
	g_iArenaAnnouncer_PlaySound_Generic_2_Play_Once_Lock = 0
	g_iArenaAnnouncer_PlaySound_ModeSpecific_Play_Once_Lock = 0
	g_iArenaAnnouncer_PlaySound_ModeSpecific_2_Play_Once_Lock = 0
	g_iArenaAnnouncer_PlaySound_Generic_Played = 0
	g_iArenaAnnouncer_PlaySound_Generic_2_Played = 0
	g_iArenaAnnouncer_PlaySound_ModeSpecific_Played = 0
	g_iArenaAnnouncer_PlaySound_ModeSpecific_2_Played = 0
	g_iArenaAnnouncer_PlaySound_Generic_MakeSingular = 0
	g_iArenaAnnouncer_PlaySound_Generic_2_MakeSingular = 0
	g_iArenaAnnouncer_PlaySound_Generic_MakePlural = 0
	g_iArenaAnnouncer_PlaySound_Generic_2_MakePlural = 0
	g_iArenaAnnouncer_FailedToPlayCounter = 0
	g_iArenaAnnouncer_PlayAudioTimeStamp = 0
	g_bArenaAnnouncer_DialogueStarted = FALSE	
	g_iLastAnnouncementTimeStamp = -1
	g_iArenaAnnouncer_LastPlayed_Generic = -1
	g_iArenaAnnouncer_LastPlayed_ModeSpecific = -1
	RESET_NET_TIMER(g_tdArenaAnnouncerInterruptionTimer)
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_AWXM2018/AJ_Interrupts")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_AWXM2018/AV_Interrupts")
ENDPROC

// Tracking / Counter to make sure we are able to play this sound.
FUNC BOOL SHOULD_PLAY_ARENA_ANNOUNCER_GENERIC_SOUND(INT iSoundToPlay)
	PRINTLN("[LM][ArenaAnnouncer] - SHOULD_PLAY_ARENA_ANNOUNCER_GENERIC_SOUND - iSoundToPlay: ", iSoundToPlay, " Count: ", g_iAA_PlaySound_Generic_Tracker[iSoundToPlay], " CountMax: ", GET_ARENA_ANNOUNCER_GENERIC_SOUND_TRACK_COUNT_MAX(iSoundToPlay))
	
	IF IS_ARENA_ANNOUNCER_BS_GENERAL_LOCKED(iSoundToPlay)
		PRINTLN("[LM][ArenaAnnouncer] - SHOULD_PLAY_ARENA_ANNOUNCER_GENERIC_SOUND - Sound has been locked or has already been played - Returning False")
		RETURN FALSE
	ENDIF
	
	IF HAS_ENOUGH_TIME_PASSED_TO_CLEAR_SOUND(FALSE, iSoundToPlay)
		PRINTLN("[LM][ArenaAnnouncer] - SHOULD_PLAY_ARENA_ANNOUNCER_GENERIC_SOUND - Sounds wait time has run out - Returning False. iSoundToPlay: ", iSoundToPlay)
		RETURN FALSE
	ENDIF
	
	IF g_iAA_PlaySound_Generic_Tracker[iSoundToPlay] >= GET_ARENA_ANNOUNCER_GENERIC_SOUND_TRACK_COUNT_MAX(iSoundToPlay)
	OR g_iAA_PlaySound_Generic_Tracker[iSoundToPlay] = -1
		PRINTLN("[LM][ArenaAnnouncer] - SHOULD_PLAY_ARENA_ANNOUNCER_GENERIC_SOUND - Returning True")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[LM][ArenaAnnouncer] - SHOULD_PLAY_ARENA_ANNOUNCER_GENERIC_SOUND - Returning False - Incrementing...")
	g_iAA_PlaySound_Generic_Tracker[iSoundToPlay]++	
	RETURN FALSE
ENDFUNC

// General Sound Loading
FUNC BOOL CONSTRUCT_ARENA_ANNOUNCER_SOUND_GENERIC(INT iSoundToPlay)
	// Load ================================
	TEXT_LABEL_15 tl15_Load = "ARANNAU"
	REQUEST_ADDITIONAL_TEXT_FOR_DLC(tl15_Load, DLC_MISSION_DIALOGUE_TEXT_SLOT)
	PRINTLN("[LM][ArenaAnnouncer] - CONSTRUCT_ARENA_ANNOUNCER_SOUND_GENERIC - REQUEST_ADDITIONAL_TEXT_FOR_DLC(", tl15_Load, ", DLC_MISSION_DIALOGUE_TEXT_SLOT)")
	IF HAS_THIS_ADDITIONAL_TEXT_LOADED(tl15_Load, DLC_MISSION_DIALOGUE_TEXT_SLOT) 
		PRINTLN("[LM][ArenaAnnouncer] - CONSTRUCT_ARENA_ANNOUNCER_SOUND_GENERIC - ", tl15_Load, " Has loaded!")
	ELSE
		PRINTLN("[LM][ArenaAnnouncer] - CONSTRUCT_ARENA_ANNOUNCER_SOUND_GENERIC - ", tl15_Load, " Has NOT loaded yet. Return false.")
		RETURN FALSE
	ENDIF
	
	// Construct ===========================
	INT iRandomVariation = 0
	INT iVariations = 0	
	TEXT_LABEL_15 tl15_Line = "ARAN_GEN_"
	tl15_Line += (iSoundToPlay+1)
	iVariations = GET_GENERIC_SOUND_VARIATIONS(iSoundToPlay)	
	
	// Select a random Announcer Variation =======================
	IF iVariations > 0
	AND NOT SHOULD_ARENA_ANNOUNCER_GENERIC_LINE_BE_SINGULAR(iSoundToPlay)
	AND NOT SHOULD_ARENA_ANNOUNCER_GENERIC_LINE_BE_PLURAL(iSoundToPlay)
		IF iVariations > 1 AND iVariations <= 3
			WHILE NOT IS_ANNOUNCER_VARIATION_VALID_FOR_MODE(iRandomVariation)
				iRandomVariation = GET_RANDOM_INT_IN_RANGE(1, iVariations+1)
			ENDWHILE
		ELSE
			iRandomVariation = GET_RANDOM_INT_IN_RANGE(1, iVariations+1)
		ENDIF
		
		TEXT_LABEL_15 tl15_temp = GET_LETTER_FOR_ARENA_ANNOUNCER_TL(iRandomVariation)
		tl15_Line += tl15_temp
	ENDIF
	
	IF SHOULD_ARENA_ANNOUNCER_GENERIC_LINE_BE_SINGULAR(iSoundToPlay)
		tl15_Line += "S"
	ENDIF
	
	IF SHOULD_ARENA_ANNOUNCER_GENERIC_LINE_BE_PLURAL(iSoundToPlay)
		tl15_Line += "M"
	ENDIF
	
	PED_INDEX SpeakerPed = NULL
	PRINTLN("[LM][ArenaAnnouncer] - CONSTRUCT_ARENA_ANNOUNCER_SOUND_GENERIC - iSoundToPlay: ", iSoundToPlay, " iVariations: ", iVariations, " iRandomVariation: ", iRandomVariation, " Loaded Block: ", tl15_Load, " Constructed Label: ", 
				tl15_Line, " Singular: ", SHOULD_ARENA_ANNOUNCER_GENERIC_LINE_BE_SINGULAR(iSoundToPlay), " Plural: ", SHOULD_ARENA_ANNOUNCER_GENERIC_LINE_BE_PLURAL(iSoundToPlay))
	
	// Validate =======================
	
	
	// Assign =======================
	ADD_PED_FOR_DIALOGUE(g_ArenaAnnouncer_speechPedArenaStruct, 2, SpeakerPed, "ARENA_ANNOUNCE1")
	ADD_PED_FOR_DIALOGUE(g_ArenaAnnouncer_speechPedArenaStruct, 3, SpeakerPed, "ARENA_JOCK")
	ADD_PED_FOR_DIALOGUE(g_ArenaAnnouncer_speechPedArenaStruct, 4, SpeakerPed, "ARENA_ANNOUNCE3")	
	g_iArenaAnnouncer_iSoundToPlayQueue[0] = iSoundToPlay
	g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = ci_Arena_Announcer_DataTypeForEvent_Generic
	g_iArenaAnnouncer_sBlock[0] = tl15_Load
	g_iArenaAnnouncer_sRoot[0] = tl15_Line
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PLAY_ARENA_ANNOUNCER_MODE_SPECIFIC_SOUND(INT iSoundToPlay)
	PRINTLN("[LM][ArenaAnnouncer] - SHOULD_PLAY_ARENA_ANNOUNCER_MODE_SPECIFIC_SOUND - iSoundToPlay: ", iSoundToPlay, " Count: ", g_iAA_PlaySound_ModeSpec_Tracker[iSoundToPlay], " CountMax: ", GET_ARENA_ANNOUNCER_MODE_SPECIFIC_SOUND_TRACK_COUNT_MAX(iSoundToPlay))

	IF IS_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED(iSoundToPlay)
		PRINTLN("[LM][ArenaAnnouncer] - SHOULD_PLAY_ARENA_ANNOUNCER_MODE_SPECIFIC_SOUND - Sound has been locked or has already been played - Returning False")
		RETURN FALSE
	ENDIF
	
	IF HAS_ENOUGH_TIME_PASSED_TO_CLEAR_SOUND(TRUE, iSoundToPlay)
		PRINTLN("[LM][ArenaAnnouncer] - SHOULD_PLAY_ARENA_ANNOUNCER_MODE_SPECIFIC_SOUND - Sounds wait time has run out - Returning False. iSoundToPlay: ", iSoundToPlay)
		RETURN FALSE
	ENDIF
	
	IF g_iAA_PlaySound_ModeSpec_Tracker[iSoundToPlay] >= GET_ARENA_ANNOUNCER_MODE_SPECIFIC_SOUND_TRACK_COUNT_MAX(iSoundToPlay)
	OR g_iAA_PlaySound_ModeSpec_Tracker[iSoundToPlay] = -1
		PRINTLN("[LM][ArenaAnnouncer] - SHOULD_PLAY_ARENA_ANNOUNCER_MODE_SPECIFIC_SOUND - Returning True")
		RETURN TRUE
	ENDIF
	
	
	PRINTLN("[LM][ArenaAnnouncer] - SHOULD_PLAY_ARENA_ANNOUNCER_MODE_SPECIFIC_SOUND - Returning False - Incrementing...")
	g_iAA_PlaySound_ModeSpec_Tracker[iSoundToPlay]++	
	RETURN FALSE
ENDFUNC

// Mode Specific Sound Loading.
FUNC BOOL CONSTRUCT_ARENA_ANNOUNCER_MODE_SPECIFIC(INT iSoundToPlay)
	// Load ================================
	TEXT_LABEL_15 tl15_Load = GET_ARENA_ANNOUNCER_MODE_SPECIFIC_BLOCK_TO_LOAD()
	
	REQUEST_ADDITIONAL_TEXT_FOR_DLC(tl15_Load, DLC_MISSION_DIALOGUE_TEXT_SLOT)
	PRINTLN("[LM][ArenaAnnouncer] - CONSTRUCT_ARENA_ANNOUNCER_MODE_SPECIFIC - REQUEST_ADDITIONAL_TEXT_FOR_DLC(", tl15_Load, ", DLC_MISSION_DIALOGUE_TEXT_SLOT)")
	IF HAS_THIS_ADDITIONAL_TEXT_LOADED(tl15_Load, DLC_MISSION_DIALOGUE_TEXT_SLOT) 
		PRINTLN("[LM][ArenaAnnouncer] - CONSTRUCT_ARENA_ANNOUNCER_MODE_SPECIFIC - ", tl15_Load, " Has loaded!")
	ELSE
		PRINTLN("[LM][ArenaAnnouncer] - CONSTRUCT_ARENA_ANNOUNCER_MODE_SPECIFIC - ", tl15_Load, " Has NOT loaded yet. Return false.")
		RETURN FALSE
	ENDIF
	
	// Construct ===========================
	INT iRandomVariation = 0
	INT iVariations = 0	
	
	TEXT_LABEL_15 tl15_Line = GET_ARENA_ANNOUNCER_MODE_SPECIFIC_ROOT_TO_LOAD()
	tl15_Line += (iSoundToPlay+1)
	iVariations = GET_MODE_SPECIFIC_VARIATIONS_FOR_THIS_SOUND(iSoundToPlay)
	
	// Select a random variation =======================	
	IF iVariations > 0
	AND NOT SHOULD_ARENA_ANNOUNCER_GENERIC_LINE_BE_SINGULAR(iSoundToPlay)
	AND NOT SHOULD_ARENA_ANNOUNCER_GENERIC_LINE_BE_PLURAL(iSoundToPlay)
		iRandomVariation = GET_RANDOM_INT_IN_RANGE(1, iVariations+1)
		TEXT_LABEL_15 tl15_temp = GET_LETTER_FOR_ARENA_ANNOUNCER_TL(iRandomVariation)
		tl15_Line += tl15_temp
	ENDIF
	
	IF SHOULD_ARENA_ANNOUNCER_MODE_SPECIFIC_LINE_BE_SINGULAR(iSoundToPlay)
		tl15_Line += "S"
	ENDIF
	
	IF SHOULD_ARENA_ANNOUNCER_MODE_SPECIFIC_LINE_BE_PLURAL(iSoundToPlay)
		tl15_Line += "M"
	ENDIF

	PED_INDEX SpeakerPed = NULL
	PRINTLN("[LM][ArenaAnnouncer] - CONSTRUCT_ARENA_ANNOUNCER_MODE_SPECIFIC - iSoundToPlay: ", iSoundToPlay, " iVariations: ", iVariations, " iRandomVariation: ", iRandomVariation, " Loaded Block: ", tl15_Load, " Constructed Label: ",
				tl15_Line, " Singular: ", SHOULD_ARENA_ANNOUNCER_MODE_SPECIFIC_LINE_BE_SINGULAR(iSoundToPlay), " Plural: ", SHOULD_ARENA_ANNOUNCER_MODE_SPECIFIC_LINE_BE_PLURAL(iSoundToPlay))
	
	// Validate =======================
	
	
	// Cache Dialogue/Sound ==================		
	ADD_PED_FOR_DIALOGUE(g_ArenaAnnouncer_speechPedArenaStruct, 2, SpeakerPed, "ARENA_ANNOUNCE1")
	ADD_PED_FOR_DIALOGUE(g_ArenaAnnouncer_speechPedArenaStruct, 3, SpeakerPed, "ARENA_JOCK")
	ADD_PED_FOR_DIALOGUE(g_ArenaAnnouncer_speechPedArenaStruct, 4, SpeakerPed, "ARENA_ANNOUNCE3")	
	g_iArenaAnnouncer_iSoundToPlayQueue[0] = iSoundToPlay
	g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = ci_Arena_Announcer_DataTypeForEvent_ModeSpecific
	g_iArenaAnnouncer_sBlock[0] = tl15_Load
	g_iArenaAnnouncer_sRoot[0] = tl15_Line
	
	RETURN TRUE
ENDFUNC

// Interrupt a sound/dialogue so that it instantly finishes to play one of a higher priority.
PROC INTERRUPT_ARENA_ANNOUNCER_SOUND()
	
	
ENDPROC

FUNC BOOL SHOULD_ARENA_ANNOUNCER_SOUND_BE_INTERRUPTED()
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		SET_BIT(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Was_Ped_Dead)
		PRINTLN("[LM][g_bArenaAnnouncerBoolWasPedDead] - PLAY_ARENA_ANNOUNCER_SOUND - SHOULD_ARENA_ANNOUNCER_SOUND_BE_INTERRUPTED: Setting g_bArenaAnnouncerBoolPedDied to TRUE")
	ELSE
		IF IS_BIT_SET(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Was_Ped_Dead)
			PRINTLN("[LM][g_bArenaAnnouncerBoolWasPedDead] - PLAY_ARENA_ANNOUNCER_SOUND - SHOULD_ARENA_ANNOUNCER_SOUND_BE_INTERRUPTED: g_bArenaAnnouncerBoolPedDied and we are now alive.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Force_End_Current_Dialogue)
		PRINTLN("[LM][ArenaAnnouncer] - PLAY_ARENA_ANNOUNCER_SOUND - SHOULD_ARENA_ANNOUNCER_SOUND_BE_INTERRUPTED: ci_Arena_Announcer_Force_End_Current_Dialogue")
		RETURN TRUE
	ENDIF
	
	IF g_bCelebrationScreenIsActive
	AND GET_CURRENT_ANNOUNCER_MODE_SPECIFIC_PLAYING() != g_iAA_PlaySound_DestructionDerby_WinnerCrossedLine
		PRINTLN("[LM][ArenaAnnouncer] - PLAY_ARENA_ANNOUNCER_SOUND - SHOULD_ARENA_ANNOUNCER_SOUND_BE_INTERRUPTED: g_bCelebrationScreenIsActive")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC TOGGLE_ANNOUNCER_AUDIO_SCENE(BOOL bActivate)
	IF bActivate
		IF NOT IS_AUDIO_SCENE_ACTIVE("dlc_aw_arena_speech_ducking_scene")
			START_AUDIO_SCENE("dlc_aw_arena_speech_ducking_scene")
			PRINTLN("TOGGLE_ANNOUNCER_AUDIO_SCENE - Starting duck scene")
		ENDIF
	ELSE
		IF IS_AUDIO_SCENE_ACTIVE("dlc_aw_arena_speech_ducking_scene")
			STOP_AUDIO_SCENE("dlc_aw_arena_speech_ducking_scene")
			PRINTLN("TOGGLE_ANNOUNCER_AUDIO_SCENE - Stopping duck scene")
		ENDIF
	ENDIF
ENDPROC

PROC PLAY_ARENA_ANNOUNCER_GO_LINE()
	IF NOT CONTENT_IS_USING_ARENA_PRIVATE()
	OR IS_SCRIPTED_CONVERSATION_ONGOING()
		EXIT
	ENDIF
	INT iSoundToPlay = GET_RANDOM_INT_IN_RANGE(0, 9)
	STRING sSoundName = ""
	SWITCH iSoundToPlay
		CASE 0
			sSoundName = "ARANN_AAAC"
		BREAK
		CASE 1
			sSoundName = "ARANN_KOAC"
		BREAK
		CASE 2
			sSoundName = "ARANN_KPAC"
		BREAK
		CASE 3
			sSoundName = "ARANN_KQAC"
		BREAK
		CASE 4
			sSoundName = "ARANN_KRAC"
		BREAK
		CASE 5
			sSoundName = "ARANN_ABAB"
		BREAK
		CASE 6
			sSoundName = "ARANN_KSAB"
		BREAK
		CASE 7
			sSoundName = "ARANN_KVAB"
		BREAK
		CASE 8
			sSoundName = "ARANN_KXAB"
		BREAK
	ENDSWITCH
	
	PRINTLN("[LM][ArenaAnnouner][PLAY_ARENA_ANNOUNCER_GO_LINE] - Attempting to play: ", sSoundName, " at <<2800, -3800.2, 179.5>>")
	
	PLAY_AMBIENT_SPEECH_FROM_POSITION_NATIVE(sSoundName, "ARENA_ANNOUNCE1", <<2800, -3800.2, 179.5>>, "SPEECH_PARAMS_FORCE_FRONTEND")
ENDPROC

PROC PLAY_ANNOUNCER_INTERRUPTION_SOUND()
	STRING sSoundSet = ""
	STRING sSoundName = ""
	INT iSoundToPlay = GET_RANDOM_INT_IN_RANGE(0, 4)	
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ARCADE_RACE_TYPE()
	OR IS_THIS_DESTRUCTION_DERBY_RACE_TYPE() 
	OR (IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType) AND GlobalServerBD_DM.iTypeOfDeathmatch != DEATHMATCH_TYPE_TDM)
	OR (IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType) AND GlobalServerBD_DM.iTypeOfDeathmatch = DEATHMATCH_TYPE_TDM)
		sSoundSet = "DLC_AW_Arena_Announcer_Interrupts_Al_Jock"
		SWITCH iSoundToPlay
			CASE 0
				sSoundName = "Al_Interrupt"
			BREAK
			
			CASE 1
				sSoundName = "Al_Excited"
			BREAK
			
			CASE 2
				sSoundName = "Jock_Interrupt"
			BREAK
			
			CASE 3
				sSoundName = "Jock_Excited"
			BREAK
		ENDSWITCH
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GAMES_MASTERS(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
	OR (IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType) AND GlobalServerBD_DM.iTypeOfDeathmatch = DEATHMATCH_TYPE_TDM)
	OR (IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType) AND GlobalServerBD_DM.iTypeOfDeathmatch != DEATHMATCH_TYPE_TDM)
		sSoundSet = "DLC_AW_Arena_Announcer_Interrupts_Al_Violet"
		SWITCH iSoundToPlay
			CASE 0
				sSoundName = "Al_Interrupt"
			BREAK
			
			CASE 1
				sSoundName = "Al_Excited"
			BREAK
			
			CASE 2
				sSoundName = "Violet_Interrupt"
			BREAK
			
			CASE 3
				sSoundName = "Violet_Excited"
			BREAK
		ENDSWITCH
	ENDIF
	
	PRINTLN("[LM][ArenaAnnouncer] - PLAY_ARENA_ANNOUNCER_SOUND - PLAY_ANNOUNCER_INTERRUPTION_SOUND - iInterruptionSound: ", iSoundToPlay, " sSoundName: ", sSoundName, " sSoundSet: ", sSoundSet)
	PLAY_SOUND_FRONTEND(-1, sSoundName, sSoundSet)
	
	RESET_NET_TIMER(g_tdArenaAnnouncerInterruptionTimer)
	START_NET_TIMER(g_tdArenaAnnouncerInterruptionTimer)
			
ENDPROC

FUNC BOOL PLAY_ARENA_ANNOUNCER_SOUND()
	PRINTLN("[LM][PlayArenaAnnouncer] - PLAY_ARENA_ANNOUNCER_SOUND sBlock[0]: ", g_iArenaAnnouncer_sBlock[0], " Root[0]: ", g_iArenaAnnouncer_sRoot[0], " iSound: ", g_iArenaAnnouncer_iSoundToPlayQueue[0], " SoundType: ", g_iArenaAnnouncer_iSoundTypeToPlayQueue[0])	
	
	IF SHOULD_ARENA_ANNOUNCER_SOUND_BE_INTERRUPTED()
		PRINTLN("[LM][ArenaAnnouncer] - PLAY_ARENA_ANNOUNCER_SOUND Interrupting Dialogue.")
	
		IF NOT IS_BIT_SET(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Was_Ped_Dead)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ELSE
			KILL_FACE_TO_FACE_CONVERSATION()
		ENDIF
		
		IF IS_BIT_SET(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Force_End_Current_Dialogue)
			CLEAR_BIT(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Force_End_Current_Dialogue)
			IF NOT IS_BIT_SET(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Was_Ped_Dead)
				PLAY_ANNOUNCER_INTERRUPTION_SOUND()
			ENDIF
		ENDIF		
		RETURN TRUE
	ENDIF
	
	IF IS_CONVERSATION_STATUS_FREE()
	OR NOT IS_SCRIPTED_CONVERSATION_ONGOING()
		IF NOT g_bArenaAnnouncer_DialogueStarted
			IF CREATE_CONVERSATION(g_ArenaAnnouncer_speechPedArenaStruct, g_iArenaAnnouncer_sBlock[0], g_iArenaAnnouncer_sRoot[0], CONV_PRIORITY_VERY_HIGH)
				PRINTLN("[LM][ArenaAnnouncer] - PLAY_ARENA_ANNOUNCER_SOUND - Created Conversation Setting g_bArenaAnnouncer_DialogueStarted.")
				g_bArenaAnnouncer_DialogueStarted = TRUE
				TOGGLE_ANNOUNCER_AUDIO_SCENE(TRUE)
			ELSE
				PRINTLN("[LM][ArenaAnnouncer] - PLAY_ARENA_ANNOUNCER_SOUND - Could not Create Conversation.")
			ENDIF
		ELSE
			PRINTLN("[LM][ArenaAnnouncer] - PLAY_ARENA_ANNOUNCER_SOUND - Conversation over, Dialogue Finished, Setting g_bArenaAnnouncer_DialogueStarted back to false.")
			g_bArenaAnnouncer_DialogueStarted = FALSE
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("[LM][PlayArenaAnnouncer] - PLAY_ARENA_ANNOUNCER_SOUND - Waiting for conversation to finish.")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PURGE_ALL_ARENA_ANNOUNCEMENTS(BOOL bKillConversation = FALSE)
	INT i
	IF bKillConversation
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	ENDIF
	
	CLEAR_BIT(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Force_End_Current_Dialogue)
	FOR i = 0 TO g_iAA_PlaySound_Generic_MAX-1
		CLEAR_ARENA_ANNOUNCER_BS_GENERAL(i)
	ENDFOR
	FOR i = 0 TO g_iAA_PlaySound_ModeSpecific_MAX-1
		CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(i)
	ENDFOR
ENDPROC

FUNC BOOL IS_THIS_ARENA_ANNOUNCEMENT_MORE_IMPORTANT(INT iType, INT iSoundToPlay)
	SWITCH iType
		CASE ci_Arena_Announcer_DataTypeForEvent_Generic
			IF (g_iArenaAnnouncer_iSoundToPlayQueue[0] >=  g_iAA_PlaySound_Generic_BinkReactions_BeerSmash AND g_iArenaAnnouncer_iSoundToPlayQueue[0] <= g_iAA_PlaySound_Generic_BinkReactions_Noise)
			AND g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = ci_Arena_Announcer_DataTypeForEvent_Generic
				RETURN FALSE
			ENDIF
			IF iSoundToPlay < g_iAA_PlaySound_Generic_PlayerRemainingCount_15
			AND g_iArenaAnnouncer_iSoundToPlayQueue[0] >=  g_iAA_PlaySound_Generic_PlayerRemainingCount_15
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ci_Arena_Announcer_DataTypeForEvent_ModeSpecific
			IF (g_iArenaAnnouncer_iSoundToPlayQueue[0] >=  g_iAA_PlaySound_Generic_BinkReactions_BeerSmash AND g_iArenaAnnouncer_iSoundToPlayQueue[0] <= g_iAA_PlaySound_Generic_BinkReactions_Noise)
			AND g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = ci_Arena_Announcer_DataTypeForEvent_Generic
				RETURN FALSE
			ENDIF
			IF g_iArenaAnnouncer_iSoundToPlayQueue[0] >=  g_iAA_PlaySound_Generic_PlayerRemainingCount_15
			AND g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = ci_Arena_Announcer_DataTypeForEvent_Generic
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH	
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_ARENA_ANNOUNCER_ON_SCREEN_DEBUG()
	/*
	#IF IS_DEBUG_BUILD
		IF g_bToggleArenaAnnouncerDebug
			TEXT_LABEL_63 tl63 = "CURRENT: "
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(tl63, "", vBlankDbg)
			
			tl63 = "Sound To Play: "
			tl63 += g_iArenaAnnouncer_iSoundToPlayQueue[0]
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(tl63, "", vBlankDbg)
			
			IF g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = 1
				tl63 = "Generic"
			ELIF g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = 2
				tl63 = "Mode Specific"
			ELIF g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = -1
				tl63 = "None"
			ENDIF
			
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(tl63, "", vBlankDbg)
						
			tl63 = "Block: "
			tl63 += g_iArenaAnnouncer_sBlock[0]
			tl63 += "   Root:"
			tl63 += g_iArenaAnnouncer_sRoot[0]
			
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(tl63, "", vBlankDbg)
			
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(".............", "", vBlankDbg)
			
			tl63 = "GENERAL LINES SET: "
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(tl63, "", vBlankDbg)
			FOR i = 0 TO g_iAA_PlaySound_Generic_MAX-1				
				IF IS_ARENA_ANNOUNCER_BS_GENERAL_SET(i)
					tl63 = "Sound Queued "
					tl63 += i
					IF IS_ARENA_ANNOUNCER_PRIORITIZE_SOUND_SET(0, i)
						tl63 += "  PRIORITY"
					ENDIF
					PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(tl63, "", vBlankDbg)
				ENDIF
			ENDFOR
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(".............", "", vBlankDbg)
			
			tl63 = "MODE SPECIFIC LINES SET: "
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(tl63, "", vBlankDbg)
			FOR i = 0 TO g_iAA_PlaySound_ModeSpecific_MAX-1				
				IF IS_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_SET(i)
					tl63 = "Sound Queued "
					tl63 += i
					IF IS_ARENA_ANNOUNCER_PRIORITIZE_SOUND_SET(1, i)
						tl63 += "  PRIORITY"
					ENDIF
					PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(tl63, "", vBlankDbg)
				ENDIF
			ENDFOR
		ENDIF
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_0, KEYBOARD_MODIFIER_CTRL, "ArenaAnnouncerDebug")
			IF NOT g_bToggleArenaAnnouncerDebug
				g_bToggleArenaAnnouncerDebug = TRUE
			ELIF g_bToggleArenaAnnouncerDebug
				g_bToggleArenaAnnouncerDebug = FALSE
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
			ENDIF
		ENDIF
		/*
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_6, KEYBOARD_MODIFIER_CTRL, "ArenaAnnouncerDebug1")
			SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_RCCarSpawn)
		ENDIF
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_7, KEYBOARD_MODIFIER_CTRL, "ArenaAnnouncerDebug2")
			SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_DroneHitWithEMP)
		ENDIF
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_8, KEYBOARD_MODIFIER_CTRL, "ArenaAnnouncerDebug3")
			SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_RemoteGunTowerUseMG)
		ENDIF
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_9, KEYBOARD_MODIFIER_CTRL, "ArenaAnnouncerDebug4")
			SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_PlayerRemainingCount_13, DEFAULT, DEFAULT, DEFAULT, TRUE)
		ENDIF
	#ENDIF
	*/
	
	/*IF IS_DEBUG_KEY_JUST_PRESSED(KEY_6, KEYBOARD_MODIFIER_CTRL, "ArenaAnnouncerDebug1")
		SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_RCCarSpawn)
	ENDIF
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_7, KEYBOARD_MODIFIER_CTRL, "ArenaAnnouncerDebug2")
		SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_DroneHitWithEMP)
	ENDIF
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_8, KEYBOARD_MODIFIER_CTRL, "ArenaAnnouncerDebug3")
		SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_RemoteGunTowerUseMG)
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_9, KEYBOARD_MODIFIER_CTRL, "ArenaAnnouncerDebug4")
		SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_BigAir, DEFAULT, DEFAULT, DEFAULT, TRUE)
	ENDIF
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_0, KEYBOARD_MODIFIER_CTRL, "ArenaAnnouncerDebug5")
		SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_HeadOnCollision, DEFAULT, DEFAULT, DEFAULT, TRUE)
	ENDIF*/
	
ENDPROC

PROC PROCESS_ARENA_ANNOUNCER_EVERY_FRAME()
	IF NOT CONTENT_IS_USING_ARENA_PRIVATE()
		EXIT
	ENDIF
	
	BOOL bDelayPurge
	IF IS_THIS_DESTRUCTION_DERBY_RACE_TYPE()
	AND (g_iArenaAnnouncer_iSoundToPlayQueue[0] = g_iAA_PlaySound_DestructionDerby_WinnerCrossedLine AND g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = 2)
		bDelayPurge = TRUE
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_DM_TAGGED_EXPLOSION_TIMER)
	AND (g_iArenaAnnouncer_iSoundToPlayQueue[0] = g_iAA_PlaySound_PassTheBomb_Elimination AND g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = 2)
	AND NOT IS_TEAM_DEATHMATCH()
		bDelayPurge = TRUE
	ENDIF
	
	IF (g_bCelebrationScreenIsActive OR g_bMissionEnding)
	AND NOT bDelayPurge
		IF g_bCelebrationScreenIsActive
			IF NOT IS_BIT_SET(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Purge_At_Celeb_Screen)
				PURGE_ALL_ARENA_ANNOUNCEMENTS(TRUE)
				SET_BIT(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Purge_At_Celeb_Screen)
			ENDIF
		ENDIF		
		EXIT
	ENDIF
	
	INT i	
	BOOL bPriorityAssigned
	
	// General Sounds - Construct
	INT iSoundToPlayCacheGeneric = -1
	FOR i = 0 TO g_iAA_PlaySound_Generic_MAX-1
		IF IS_ARENA_ANNOUNCER_BS_GENERAL_SET(i)
			IF SHOULD_PLAY_ARENA_ANNOUNCER_GENERIC_SOUND(i)
				IF NOT bPriorityAssigned
					IF iSoundToPlayCacheGeneric = -1
						iSoundToPlayCacheGeneric = i
					ENDIF
					IF IS_ARENA_ANNOUNCER_PRIORITIZE_SOUND_SET(0, i)
					AND IS_THIS_ARENA_ANNOUNCEMENT_MORE_IMPORTANT(0, i)
						bPriorityAssigned = TRUE
						PRINTLN("[LM][ArenaAnnouncer] - (Generic) - PROCESS_ARENA_ANNOUNCER_EVERY_FRAME - iSoundToPlay: ", i, " has requested Priority over CurrentlyPlayingSound: ", g_iArenaAnnouncer_iSoundToPlayQueue[0])
						IF g_iArenaAnnouncer_iSoundToPlayQueue[0] != i
						AND g_iArenaAnnouncer_iSoundToPlayQueue[0] > -1
							PRINTLN("[LM][ArenaAnnouncer] - (Generic) - PROCESS_ARENA_ANNOUNCER_EVERY_FRAME - CurrentlyPlaying: ", g_iArenaAnnouncer_iSoundToPlayQueue[0], " Setting to FORCE END the current Announcement")
							SET_BIT(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Force_End_Current_Dialogue)
						ENDIF
						iSoundToPlayCacheGeneric = i
					ENDIF					
				ENDIF
			ELSE	
				CLEAR_ARENA_ANNOUNCER_BS_GENERAL(i)
			ENDIF
		ENDIF
	ENDFOR	
	IF iSoundToPlayCacheGeneric > -1
		IF g_iArenaAnnouncer_iSoundToPlayQueue[0] = -1
		AND g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = -1
			IF CONSTRUCT_ARENA_ANNOUNCER_SOUND_GENERIC(iSoundToPlayCacheGeneric)
				PRINTLN("[LM][ArenaAnnouncer] - PROCESS_ARENA_ANNOUNCER_EVERY_FRAME - CONSTRUCT_ARENA_ANNOUNCER_SOUND_GENERIC iSoundToPlayCacheGeneric: ", iSoundToPlayCacheGeneric, " Returned True.")
				CLEAR_BIT(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Force_End_Current_Dialogue)
				g_iArenaAnnouncer_FailedToPlayCounter = 0
				g_iArenaAnnouncer_PlayAudioTimeStamp = GET_CLOUD_TIME_AS_INT()
			ELSE
				g_iArenaAnnouncer_FailedToPlayCounter++
				IF g_iArenaAnnouncer_FailedToPlayCounter >= 100
					PRINTLN("[LM][ArenaAnnouncer] - PROCESS_ARENA_ANNOUNCER_EVERY_FRAME - CONSTRUCT_ARENA_ANNOUNCER_SOUND_GENERIC iSoundToPlayCacheGeneric: ", iSoundToPlayCacheGeneric, " FAILED ", g_iArenaAnnouncer_FailedToPlayCounter, " did not return true after 100 frames.")
					SCRIPT_ASSERT("[LM][ArenaAnnouncer] - (GENERIC) - Looks like the Arena Announcer got all choked up here. Something invalid must have been passed in, so we bailed, check the logs.")					
					g_iArenaAnnouncer_FailedToPlayCounter = 0
					CLEAR_ARENA_ANNOUNCER_BS_GENERAL(iSoundToPlayCacheGeneric)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Mode Specific - Construct
	IF IS_PLAYING_VALID_MODE_SPECIFIC_FOR_ANNOUNCER()
		INT iSoundToPlayCacheModeSpec = -1
		FOR i = 0 TO g_iAA_PlaySound_ModeSpecific_MAX-1
			IF IS_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_SET(i)
				IF SHOULD_PLAY_ARENA_ANNOUNCER_MODE_SPECIFIC_SOUND(i)
					IF NOT bPriorityAssigned
						IF iSoundToPlayCacheModeSpec = -1
							iSoundToPlayCacheModeSpec = i
						ENDIF
						IF IS_ARENA_ANNOUNCER_PRIORITIZE_SOUND_SET(1, i)
						AND IS_THIS_ARENA_ANNOUNCEMENT_MORE_IMPORTANT(1, i)
							bPriorityAssigned = TRUE
							PRINTLN("[LM][ArenaAnnouncer] - (Specific) - PROCESS_ARENA_ANNOUNCER_EVERY_FRAME - iSoundToPlay: ", i, " has requested Priority over CurrentlyPlayingSound: ", g_iArenaAnnouncer_iSoundToPlayQueue[0])
							IF g_iArenaAnnouncer_iSoundToPlayQueue[0] != i
							AND g_iArenaAnnouncer_iSoundToPlayQueue[0] > -1
								PRINTLN("[LM][ArenaAnnouncer] - (Specific) - PROCESS_ARENA_ANNOUNCER_EVERY_FRAME - CurrentlyPlaying: ", g_iArenaAnnouncer_iSoundToPlayQueue[0], " Setting to FORCE END the current Announcement")
								SET_BIT(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Force_End_Current_Dialogue)
							ENDIF							
							iSoundToPlayCacheModeSpec = i
						ENDIF					
					ENDIF
				ELSE
					CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(i)
				ENDIF
			ENDIF
		ENDFOR
		IF iSoundToPlayCacheModeSpec > -1
			IF g_iArenaAnnouncer_iSoundToPlayQueue[0] = -1
			AND g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = -1
				IF CONSTRUCT_ARENA_ANNOUNCER_MODE_SPECIFIC(iSoundToPlayCacheModeSpec)
					PRINTLN("[LM][ArenaAnnouncer] - PROCESS_ARENA_ANNOUNCER_EVERY_FRAME - CONSTRUCT_ARENA_ANNOUNCER_MODE_SPECIFIC iSoundToPlayCacheModeSpec: ", iSoundToPlayCacheModeSpec, " Returned True.")
					CLEAR_BIT(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Force_End_Current_Dialogue)
					g_iArenaAnnouncer_FailedToPlayCounter = 0
					g_iArenaAnnouncer_PlayAudioTimeStamp = GET_CLOUD_TIME_AS_INT()
				ELSE
					g_iArenaAnnouncer_FailedToPlayCounter++
					IF g_iArenaAnnouncer_FailedToPlayCounter >= 100
						PRINTLN("[LM][ArenaAnnouncer] - PROCESS_ARENA_ANNOUNCER_EVERY_FRAME - CONSTRUCT_ARENA_ANNOUNCER_MODE_SPECIFIC iSoundToPlayCacheModeSpec: ", iSoundToPlayCacheModeSpec, " FAILED for ", g_iArenaAnnouncer_FailedToPlayCounter, " frames. Giving Up, should investigate what happened.")
						SCRIPT_ASSERT("[LM][ArenaAnnouncer] - (MODE SPECIFIC) - Looks like the Arena Announcer got all choked up here. Something invalid must have been passed in, so we bailed, check the logs.")					
						g_iArenaAnnouncer_FailedToPlayCounter = 0
						CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(iSoundToPlayCacheModeSpec)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Play Cached Sound - (Note: keeping mode spec and general separate for now in case of special functionality. Might need different speakers etc.)		
	IF NOT HAS_NET_TIMER_STARTED(g_tdArenaAnnouncerInterruptionTimer)
	OR (HAS_NET_TIMER_STARTED(g_tdArenaAnnouncerInterruptionTimer) AND HAS_NET_TIMER_EXPIRED_READ_ONLY(g_tdArenaAnnouncerInterruptionTimer, 750))

		IF g_iArenaAnnouncer_iSoundToPlayQueue[0] > -1
		AND g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] > -1
			
			// 30 Second timeout if we haven't managed to play something, to free up the announcer.
			BOOL bTimedOut
			IF GET_FRAME_COUNT() % 10 = 0
				PRINTLN("[LM][PlayArenaAnnouncer] - PROCESS_ARENA_ANNOUNCER_EVERY_FRAME - PLAY_ARENA_ANNOUNCER_SOUND i: ", g_iArenaAnnouncer_iSoundToPlayQueue[0], " Attempted to play a line: ", GET_CLOUD_TIME_AS_INT() - g_iArenaAnnouncer_PlayAudioTimeStamp, " Seconds Ago.")
				IF GET_CLOUD_TIME_AS_INT() - g_iArenaAnnouncer_PlayAudioTimeStamp >= 30
					PRINTLN("[LM][PlayArenaAnnouncer] - PROCESS_ARENA_ANNOUNCER_EVERY_FRAME - PLAY_ARENA_ANNOUNCER_SOUND i: ", g_iArenaAnnouncer_iSoundToPlayQueue[0], " Timing out.")
					SCRIPT_ASSERT("[LM][PlayArenaAnnouncer] - (Play Sound) - Looks like the Arena Announcer got all choked up here. Something invalid must have been passed in, so we bailed, check the logs.")	
					bTimedOut = TRUE
				ENDIF
			ENDIF
			
			IF g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = ci_Arena_Announcer_DataTypeForEvent_Generic
				IF PLAY_ARENA_ANNOUNCER_SOUND()
				OR bTimedOut
					IF NOT bTimedOut
						PRINTLN("[LM][ArenaAnnouncer] - PROCESS_ARENA_ANNOUNCER_EVERY_FRAME - PLAY_ARENA_ANNOUNCER_SOUND (GENERIC) SOUND FINISHED")
					ELSE
						PRINTLN("[LM][ArenaAnnouncer] - PROCESS_ARENA_ANNOUNCER_EVERY_FRAME - PLAY_ARENA_ANNOUNCER_SOUND (GENERIC) TIMED OUT... Resetting Variables.")					
					ENDIF
					PRINTLN("[LM][ArenaAnnouncer] - PROCESS_ARENA_ANNOUNCER_EVERY_FRAME - PLAY_ARENA_ANNOUNCER_SOUND (GENERIC) iSoundToPlayQueue: ", g_iArenaAnnouncer_iSoundToPlayQueue[0] , " sBlock: ", g_iArenaAnnouncer_sBlock[0], " sRoot: ", g_iArenaAnnouncer_sRoot[0])
					
					g_iArenaAnnouncer_LastPlayed_Generic = g_iArenaAnnouncer_iSoundToPlayQueue[0]
					g_iAA_PlaySound_Generic_Tracker[g_iArenaAnnouncer_iSoundToPlayQueue[0]] = 0
					CLEAR_ARENA_ANNOUNCER_BS_GENERAL(g_iArenaAnnouncer_iSoundToPlayQueue[0])
					IF IS_ARENA_ANNOUNCER_BS_GENERAL_LOCK_SET(g_iArenaAnnouncer_iSoundToPlayQueue[0])
						SET_ARENA_ANNOUNCER_BS_GENERAL_LOCKED(g_iArenaAnnouncer_iSoundToPlayQueue[0])
					ENDIF
					TOGGLE_ANNOUNCER_AUDIO_SCENE(FALSE)
					g_iArenaAnnouncer_iSoundToPlayQueue[0] = -1
					g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = -1
					g_iArenaAnnouncer_FailedToPlayCounter = 0
					g_iArenaAnnouncer_PlayAudioTimeStamp = 0
					g_bArenaAnnouncer_DialogueStarted = FALSE
					g_iLastAnnouncementTimeStamp = GET_CLOUD_TIME_AS_INT()
					CLEAR_BIT(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Force_End_Current_Dialogue)
					CLEAR_BIT(g_iArenaBigScreenBinkControllerBS, ciArenaBigScreenBS_NoiseMetre)
					CLEAR_BIT(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Was_Ped_Dead)
				ENDIF
			ELIF g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = ci_Arena_Announcer_DataTypeForEvent_ModeSpecific
				IF PLAY_ARENA_ANNOUNCER_SOUND()
				OR bTimedOut
					IF NOT bTimedOut
						PRINTLN("[LM][ArenaAnnouncer] - PROCESS_ARENA_ANNOUNCER_EVERY_FRAME - PLAY_ARENA_ANNOUNCER_SOUND (MODE SPECIFIC) SOUND FINISHED")					
					ELSE
						PRINTLN("[LM][ArenaAnnouncer] - PROCESS_ARENA_ANNOUNCER_EVERY_FRAME - PLAY_ARENA_ANNOUNCER_SOUND (MODE SPECIFIC) TIMED OUT... Resetting Variables.")					
					ENDIF
					PRINTLN("[LM][ArenaAnnouncer] - PROCESS_ARENA_ANNOUNCER_EVERY_FRAME - PLAY_ARENA_ANNOUNCER_SOUND (MODE SPECIFIC) iSoundToPlayQueue: ", g_iArenaAnnouncer_iSoundToPlayQueue[0] , " sBlock: ", g_iArenaAnnouncer_sBlock[0], " sRoot: ", g_iArenaAnnouncer_sRoot[0])				
					g_iArenaAnnouncer_LastPlayed_ModeSpecific = g_iArenaAnnouncer_iSoundToPlayQueue[0]
					g_iAA_PlaySound_ModeSpec_Tracker[g_iArenaAnnouncer_iSoundToPlayQueue[0]] = 0
					CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(g_iArenaAnnouncer_iSoundToPlayQueue[0])
					IF IS_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCK_SET(g_iArenaAnnouncer_iSoundToPlayQueue[0])
						SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED(g_iArenaAnnouncer_iSoundToPlayQueue[0])
					ENDIF
					TOGGLE_ANNOUNCER_AUDIO_SCENE(FALSE)
					g_iArenaAnnouncer_iSoundToPlayQueue[0] = -1
					g_iArenaAnnouncer_iSoundTypeToPlayQueue[0] = -1
					g_iArenaAnnouncer_FailedToPlayCounter = 0
					g_iArenaAnnouncer_PlayAudioTimeStamp = 0
					g_bArenaAnnouncer_DialogueStarted = FALSE	
					g_iLastAnnouncementTimeStamp = GET_CLOUD_TIME_AS_INT()
					CLEAR_BIT(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Force_End_Current_Dialogue)				
					CLEAR_BIT(g_iArenaBigScreenBinkControllerBS, ciArenaBigScreenBS_NoiseMetre)
					CLEAR_BIT(g_iArenaAnnouncer_ControlBS, ci_Arena_Announcer_Was_Ped_Dead)
				ENDIF
			ENDIF
		ENDIF
	ELSE		
		IF (HAS_NET_TIMER_STARTED(g_tdArenaAnnouncerInterruptionTimer) AND HAS_NET_TIMER_EXPIRED_READ_ONLY(g_tdArenaAnnouncerInterruptionTimer, 750))
			RESET_NET_TIMER(g_tdArenaAnnouncerInterruptionTimer)
			PRINTLN("[LM][ArenaAnnouncer] - PLAY_ARENA_ANNOUNCER_SOUND Resetting Interruption Timer!")
		ELSE
			PRINTLN("[LM][ArenaAnnouncer] - PLAY_ARENA_ANNOUNCER_SOUND Currently waiting for Interruption sound to end!")
		ENDIF
	ENDIF
	
ENDPROC

// [ArenaAnnouncer] END =================================================================================
