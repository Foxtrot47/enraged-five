USING "globals.sch"

FUNC FLOAT GetDistanceToClosestActiveSpawnAreaInArray(VECTOR vPoint, SPAWN_AREA &SPArea[], INT &iIndex)
	INT i
	FLOAT fDist
	FLOAT fMinDist
	INT iClosest = -1
	
	fMinDist = 9999999.9

	REPEAT COUNT_OF(SPArea) i
			
		IF (SPArea[i].bIsActive)
			
			#IF IS_DEBUG_BUILD
			PRINTLN("GetDistanceToClosestActiveSpawnAreaInArray - SPArea[", i, "] at vCoords: ", SPArea[i].vCoords1, " is active.")
			#ENDIF
			
			fDist = 0.0			
						
			SWITCH SPArea[i].iShape
				CASE SPAWN_AREA_SHAPE_CIRCLE			
					fDist = VDIST(SPArea[i].vCoords1, vPoint)
					#IF IS_DEBUG_BUILD
					PRINTLN("GetDistanceToClosestActiveSpawnAreaInArray - Distance between SPArea[", i, "].vCoords (", SPArea[i].vCoords1, ")  and vPoint (", vPoint, ") = ", fDist)
					#ENDIF
					
					fDist -= (SPArea[i].fFloat + (g_SpawnData.iAttempt * SPArea[i].fIncreaseDist))
					#IF IS_DEBUG_BUILD
					PRINTLN("GetDistanceToClosestActiveSpawnAreaInArray - Upated fDist = ", fDist)
					#ENDIF
				BREAK
				CASE SPAWN_AREA_SHAPE_BOX
				CASE SPAWN_AREA_SHAPE_ANGLED
					IF (vPoint.x < SPArea[i].vCoords1.x)
						fDist += SPArea[i].vCoords1.x - vPoint.x
					ELIF (vPoint.x > SPArea[i].vCoords2.x)
						fDist += vPoint.x - SPArea[i].vCoords2.x
					ENDIF
					
					IF (vPoint.y < SPArea[i].vCoords1.y)
						fDist += SPArea[i].vCoords1.y - vPoint.y
					ELIF (vPoint.y > SPArea[i].vCoords2.y)
						fDist += vPoint.y - SPArea[i].vCoords2.y
					ENDIF
					
					IF (vPoint.z < SPArea[i].vCoords1.z)
						fDist += SPArea[i].vCoords1.z - vPoint.z
					ELIF (vPoint.z > SPArea[i].vCoords2.z)
						fDist += vPoint.z - SPArea[i].vCoords2.z
					ENDIF
				BREAK
			ENDSWITCH						
			
			IF (fDist < fMinDist)
				fMinDist = fDist
				iClosest = i
				
				#IF IS_DEBUG_BUILD
				PRINTLN("GetDistanceToClosestActiveSpawnAreaInArray - storing = ", iClosest, ", fMinDist = ", fMinDist)
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				PRINTLN("GetDistanceToClosestActiveSpawnAreaInArray - SPArea[", i, "] fMinDist is greater than fDist.")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTLN("GetDistanceToClosestActiveSpawnAreaInArray - SPArea[", i, "] is not active.")
			#ENDIF
		ENDIF
	ENDREPEAT
	
	IF (fMinDist < 0.0)
		fMinDist = 0.0
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTLN("GetDistanceToClosestActiveSpawnAreaInArray - returning iClosest = ", iClosest, ", fMinDist = ", fMinDist)
	#ENDIF
	
	iIndex = iClosest
	
	RETURN(fMinDist)
ENDFUNC


FUNC FLOAT GetDistanceToClosestActiveSpawnArea(VECTOR vPoint)
	INT iIndex	
	RETURN GetDistanceToClosestActiveSpawnAreaInArray(vPoint, g_SpawnData.MissionSpawnDetails.SpawnArea, iIndex)
ENDFUNC


PROC SetSpawnDataRawInfoFromSPArea(SPAWN_AREA &SPArea)
	g_SpawnData.bUseIncreasingDist = FALSE
	SWITCH SPArea.iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			g_SpawnData.PrivateParams.vSearchCoord = SPArea.vCoords1
			g_SpawnData.PrivateParams.fSearchRadius = SPArea.fFloat + (g_SpawnData.iAttempt * SPArea.fIncreaseDist)
			g_SpawnData.PublicParams.bConsiderOriginAsValidPoint = SPArea.bConsiderCentrePointAsValid
			IF (SPArea.fIncreaseDist > 0.0)
				g_SpawnData.bUseIncreasingDist = TRUE
			ENDIF
			IF (g_SpawnData.PublicParams.bConsiderOriginAsValidPoint)
				g_SpawnData.PrivateParams.fRawHeading = SPArea.fHeading
			ENDIF
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX
		
			g_SpawnData.PrivateParams.vSearchCoord = <<0.0, 0.0, 0.0>>
			g_SpawnData.PrivateParams.fRawHeading = 0.0
			g_SpawnData.PrivateParams.fSearchRadius = 0.0
			g_SpawnData.PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_ANGLED
			
			VECTOR vMin
			VECTOR vMax
			VECTOR vMid
			
			vMin = SPArea.vCoords1
			vMax = SPArea.vCoords2
			vMid = (SPArea.vCoords1 + SPArea.vCoords2)*0.5
			
			g_SpawnData.PrivateParams.vAngledAreaPoint1 = <<vMin.x, vMid.y, vMin.z>>
			g_SpawnData.PrivateParams.vAngledAreaPoint2 = <<vMax.x, vMid.y, vMax.z>>
			g_SpawnData.PrivateParams.fAngledAreaWidth = vMax.y - vMin.y
			
			g_SpawnData.PrivateParams.vSearchCoord = (g_SpawnData.PrivateParams.vAngledAreaPoint1 + g_SpawnData.PrivateParams.vAngledAreaPoint2) * 0.5

		BREAK
			
		CASE SPAWN_AREA_SHAPE_ANGLED
			g_SpawnData.PrivateParams.fRawHeading = 0.0
			g_SpawnData.PrivateParams.fSearchRadius = 0.0
			g_SpawnData.PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_ANGLED
			g_SpawnData.PrivateParams.vAngledAreaPoint1 = SPArea.vCoords1
			g_SpawnData.PrivateParams.vAngledAreaPoint2 = SPArea.vCoords2
			g_SpawnData.PrivateParams.fAngledAreaWidth = SPArea.fFloat	
			g_SpawnData.PrivateParams.vSearchCoord = (g_SpawnData.PrivateParams.vAngledAreaPoint1 + g_SpawnData.PrivateParams.vAngledAreaPoint2) * 0.5
		BREAK
		
	ENDSWITCH
ENDPROC


FUNC VECTOR GetRandomPointInCircle(VECTOR vCoords, FLOAT fMaxRadius, FLOAT fMinRadius=0.0)
	
	VECTOR vec
	
	vec = <<0.0, GET_RANDOM_FLOAT_IN_RANGE(fMinRadius, fMaxRadius), 0.0>>
	RotateVec(vec, <<0.0, 0.0, GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)>>)
	
	RETURN(vCoords + vec)
ENDFUNC

FUNC VECTOR GetRandomPointInBox(VECTOR vMin, VECTOR vMax)

	RETURN(<<GET_RANDOM_FLOAT_IN_RANGE(vMin.x, vMax.x), GET_RANDOM_FLOAT_IN_RANGE(vMin.y, vMax.y), GET_RANDOM_FLOAT_IN_RANGE(vMin.z, vMax.z)>>)

ENDFUNC

FUNC VECTOR GetRandomPointInAngledArea(VECTOR vCoord1, VECTOR vCoord2, FLOAT fWidth)
	VECTOR vec = vCoord2 - vCoord1
	VECTOR vCross = CROSS_PRODUCT(vec, <<vec.x, vec.y, 0.0>>)
	vec /= VMAG(vec)
	vCross /= VMAG(vCross)
	vCross *= GET_RANDOM_FLOAT_IN_RANGE(fWidth*-0.5, fWidth*0.5)
	vec *= GET_RANDOM_FLOAT_IN_RANGE(0.0, VDIST(vCoord1, vCoord2))
	RETURN(vCoord1 + vec + vCross)
ENDFUNC

PROC MovePointOutsideSphere(VECTOR &vPoint, VECTOR vCircleCentre, FLOAT fCircleRadius, FLOAT fAdditionalDistance=0.1, BOOL bChooseOppositeSide=FALSE, FLOAT fHeadingOffset=0.0)
	PRINTLN("MovePointOutsideSphere called with")
	VECTOR vec
	VECTOR vNewPoint
	vec = vPoint - vCircleCentre
	vec.z = 0.0 // to stop the point from being moved directly below.
	IF VMAG(vec) > 0.0
		vec /= VMAG(vec)
	ELSE
		vec = <<0.0, 1.0, 0.0>>
		IF (fHeadingOffset = 0.0)
			RotateVec(vec, <<0.0, 0.0, GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)>>)
		ELSE
			RotateVec(vec, <<0.0, 0.0, fHeadingOffset>>)
		ENDIF
	ENDIF
	vec *= (fCircleRadius + fAdditionalDistance)
	IF NOT (bChooseOppositeSide)
		vNewPoint = vCircleCentre + vec
	ELSE
		vNewPoint = vCircleCentre - vec
	ENDIF
	vPoint.x = vNewPoint.x
	vPoint.y = vNewPoint.y
	// z remains the same. 
ENDPROC

PROC MovePointOutsideAnyOtherSpawningPlayer(VECTOR &vPoint, FLOAT fMinRadius, PLAYER_INDEX LocalPlayerID, BOOL bCheckThisPlayer=FALSE)

	INT i
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS i
		IF NOT (NATIVE_TO_INT(LocalPlayerID) = i)
		OR (bCheckThisPlayer = TRUE)
			PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
			IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
			AND IS_NET_PLAYER_OK(LocalPlayerID, FALSE, TRUE)
				IF (GlobalServerBD.bWarpRequestTimeIntialised[i])
					IF (VDIST(GlobalServerBD.g_vWarpRequest[i], vPoint) < fMinRadius)
						NET_PRINT("MovePointOutsideAnyOtherSpawningPlayer - player ") NET_PRINT_INT(i) NET_PRINT(" is currently warping there") NET_NL()		
						MovePointOutsideSphere(vPoint, GlobalServerBD.g_vWarpRequest[i], fMinRadius)
					ENDIF
				ELSE
					// other player has perhaps arrived there and is still standing there
					IF (VDIST(GET_PLAYER_COORDS(PlayerID), vPoint) < fMinRadius)
						NET_PRINT("MovePointOutsideAnyOtherSpawningPlayer - player ") NET_PRINT_INT(i) NET_PRINT(" is currently warping there (arrived)") NET_NL()
						MovePointOutsideSphere(vPoint, GET_PLAYER_COORDS(PlayerID), fMinRadius)
					ENDIF
				ENDIF
			ELSE
				IF (GlobalServerBD.bWarpRequestTimeIntialised[i])
					IF (VDIST(GlobalServerBD.g_vWarpRequest[i], vPoint) < fMinRadius)
						NET_PRINT("MovePointOutsideAnyOtherSpawningPlayer - (PLAYERS NOT OK) player ") NET_PRINT_INT(i) NET_PRINT(" is currently warping there") NET_NL()
						MovePointOutsideSphere(vPoint, GlobalServerBD.g_vWarpRequest[i], fMinRadius)		
					ENDIF
				ELSE
					// other player has perhaps arrived there and is still standing there
					IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
						IF (VDIST(GET_PLAYER_COORDS(PlayerID), vPoint) < fMinRadius)
							NET_PRINT("MovePointOutsideAnyOtherSpawningPlayer - (PLAYERS NOT OK) player ") NET_PRINT_INT(i) NET_PRINT(" is currently warping there (arrived)") NET_NL()
							MovePointOutsideSphere(vPoint, GET_PLAYER_COORDS(PlayerID), fMinRadius)
						ENDIF
					ENDIF
				ENDIF				
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC


PROC MovePointOutsideAngledArea(VECTOR &vPoint, VECTOR vAngled1, VECTOR vAngled2, FLOAT fWidth, FLOAT fAdditionalDistance=0.1, BOOL bChooseOppositeSide=FALSE)

	VECTOR vec1, vec2
	VECTOR vCross
	
	
	// vec1 = from angled coord1 to coord1
	vec1 = vAngled2 - vAngled1
	vec1.z = 0.0
	
	// vec2 = from angled coord1 to point
	vec2 = vPoint - vAngled1
	vec2.z = 0.0
	
	
	vCross = CROSS_PRODUCT(<<0.0, 0.0, 1.0>>, vec1)
	vCross /= VMAG(vCross)
	
	//NET_PRINT("MovePointOutsideAngledArea - CROSS_PRODUCT= ") NET_PRINT_VECTOR(vCross) NET_NL()
	
	// dis point is from centre line
	FLOAT fDist = vMAG(vec2) * SIN(GET_ANGLE_BETWEEN_2D_VECTORS(vec1.x, vec1.y, vec2.x, vec2.y))
	
	//NET_PRINT("MovePointOutsideAngledArea - dist from centre line = ") NET_PRINT_FLOAT(fDist) NET_NL()
	
	VECTOR vSafePoint1
	VECTOR vSafePoint2
	
	IF (fDist < (fWidth*0.5))
	
//		// check z's are within range
//		FLOAT fMaxZ, fMinZ
//		IF (vAngled1.z > vAngled2.z
//			fMaxZ = vAngled1.z
//			fMinZ = vAngled2.z
//		ELSE
//			fMaxZ = vAngled2.z
//			fMinZ = vAngled1.z
//		ENDIF
//		IF (vPoint.z < fMaxZ) AND (vPoint.z > fMinZ)
	

			// point is inside angled area. what way is the nearest boundary?
		
			IF NOT (bChooseOppositeSide)
				IF DOT_PRODUCT(vCross, vec2) >= 0
					vCross *= (((fWidth*0.5) - fDist)+fAdditionalDistance)
				ELSE
					vCross *= ((((fWidth*0.5) - fDist)+fAdditionalDistance) * -1.0)
				ENDIF
			ELSE
				IF DOT_PRODUCT(vCross, vec2) >= 0
					vCross *= ((((fWidth*0.5) + fDist)+fAdditionalDistance) * -1.0)
				ELSE
					vCross *= ((((fWidth*0.5) + fDist)+fAdditionalDistance) )
				ENDIF			
			ENDIF

			//vPoint += vCross			
			vSafePoint1 = vPoint + vCross // this is the nearest point in the width direction
			
			
			// now find the nearest point in the height direction and chose whatever is nearer.
			
			FLOAT fHeight
			fHeight = VDIST(<<vAngled1.x, vAngled1.y, 0.0>>, <<vAngled2.x, vAngled2.y, 0.0>>)
			

			VECTOR vCentre
			vCentre = (vAngled1 + vAngled2) / 2.0
			vCentre.z = 0.0
			
			VECTOR vEdge1, vEdge2
			vCross = CROSS_PRODUCT(<<0.0, 0.0, 1.0>>, vec1)
			vCross /= VMAG(vCross)
			vCross *= (fWidth*0.5)
			vEdge1 = vCentre - vCross
			vEdge2 = vCentre + vCross
			
			VECTOR vec1B
			VECTOR vec2B
			
			// vec1 = from angled coord1 to coord1
			vec1B = vEdge2 - vEdge1
			vec1B.z = 0.0
			
			// vec2 = from angled coord1 to point
			vec2B = vPoint - vEdge1
			vec2B.z = 0.0			
			
			vCross = CROSS_PRODUCT(<<0.0, 0.0, 1.0>>, vec1B)
			vCross /= VMAG(vCross)			
			
		 	fDist = vMAG(vec2B) * SIN(GET_ANGLE_BETWEEN_2D_VECTORS(vec1B.x, vec1B.y, vec2B.x, vec2B.y))
			
			IF NOT (bChooseOppositeSide)
				IF DOT_PRODUCT(vCross, vec2B) >= 0
					vCross *= (((fHeight*0.5) - fDist)+fAdditionalDistance)
				ELSE
					vCross *= ((((fHeight*0.5) - fDist)+fAdditionalDistance) * -1.0)
				ENDIF
			ELSE
				IF DOT_PRODUCT(vCross, vec2B) >= 0
					vCross *= ((((fHeight*0.5) + fDist)+fAdditionalDistance) * -1.0)
				ELSE
					vCross *= ((((fHeight*0.5) + fDist)+fAdditionalDistance) )
				ENDIF			
			ENDIF

			//vPoint += vCross			
			vSafePoint2 = vPoint + vCross			
			
			IF VDIST(vSafePoint1, <<vPoint.x, vPoint.y, 0.0>>) < VDIST(vSafePoint2, <<vPoint.x, vPoint.y, 0.0>>)
				vPoint = vSafePoint1
			ELSE
				vPoint = vSafePoint2
			ENDIF
			
//		ENDIF
	ENDIF

ENDPROC


PROC MovePointInsideSphere(VECTOR &vPoint, VECTOR vCircleCentre, FLOAT fCircleRadius, FLOAT fAdditionalDist=0.01)
	PRINTLN("[spawning] MovePointInsideSphere - called with ",  vPoint, vCircleCentre, fCircleRadius, fAdditionalDist)
	VECTOR vec
	vec = vPoint - vCircleCentre
	vec.z = 0.0 // to stop the point from being moved directly below.
	IF VMAG(vec) > 0.0
		vec /= VMAG(vec)
	ELSE
		vec = <<0.0, 1.0, 0.0>>
	ENDIF
	vec *= (fCircleRadius - fAdditionalDist)
	vPoint = vCircleCentre + vec	
	PRINTLN("[spawning] MovePointInsideSphere - returning ",  vPoint)
ENDPROC

PROC MovePointOutsideBox(VECTOR &vPoint, VECTOR vMin, VECTOR vMax, FLOAT fAdditionalDistance=0.1, BOOL bChooseOppositeSide=FALSE)
	// find nearest edge
	//FLOAT fDist
	//fDist = 9999.9
	VECTOR vResult
	
	vResult = vPoint
	
	
	FLOAT fDistToMinX, fDistToMaxX, fMinXDist
	FLOAT fDistToMinY, fDistToMaxY, fMinYDist
	
	fDistToMinX = vPoint.x - vMin.x
	fDistToMaxX = vPoint.x - vMax.x
	IF (fDistToMinX < fDistToMaxX)
		fMinXDist = fDistToMinX
	ELSE
		fMinXDist = fDistToMaxX
	ENDIF
	
	fDistToMinY = vPoint.y - vMin.y
	fDistToMaxY = vPoint.y - vMax.y
	IF (fDistToMinY < fDistToMaxY)
		fMinYDist = fDistToMinY
	ELSE
		fMinYDist = fDistToMaxY
	ENDIF
	
	vResult = vPoint
	IF NOT (bChooseOppositeSide)
		IF (fMinXDist < fMinYDist)
			IF (fDistToMinX < fDistToMaxX)	
				vResult.x = vMin.x - fAdditionalDistance
			ELSE
				vResult.x = vMax.x + fAdditionalDistance
			ENDIF
		ELSE
			IF (fDistToMinY < fDistToMaxY)	
				vResult.y = vMin.y - fAdditionalDistance
			ELSE
				vResult.y = vMax.y + fAdditionalDistance
			ENDIF
		ENDIF
	ELSE
		IF (fMinXDist < fMinYDist)
			IF (fDistToMinX < fDistToMaxX)	
				vResult.x = vMax.x + fAdditionalDistance
			ELSE
				vResult.x = vMin.x - fAdditionalDistance
			ENDIF
		ELSE
			IF (fDistToMinY < fDistToMaxY)	
				vResult.y = vMax.y + fAdditionalDistance
			ELSE
				vResult.y = vMin.y - fAdditionalDistance
			ENDIF
		ENDIF		
	ENDIF
	
	vPoint = vResult
	
//	IF (vPoint.x - vMin.x < fDist)		
//		fDist = vPoint.x - vMin.x
//		vResult = vPoint
//		vResult.x = vMin.x - fAdditionalDistance
//	ENDIF
//	IF (vMax.x - vPoint.x < fDist)
//		fDist = vMax.x - vPoint.x	
//		vResult = vPoint
//		vResult.x = vMax.x + fAdditionalDistance		
//	ENDIF
//	IF (vPoint.y - vMin.y < fDist)
//		fDist = vPoint.y - vMin.y
//		vResult = vPoint
//		vResult.y = vMax.y - fAdditionalDistance		
//	ENDIF
//	IF (vMax.y - vPoint.y < fDist)
//		fDist = vMax.y - vPoint.y	
//		vResult = vPoint
//		vResult.y = vMax.y + fAdditionalDistance			
//	ENDIF
//
//	vPoint = vResult
	
ENDPROC

FUNC FLOAT GetSpawnAreaMiddleZ(SPAWN_AREA &SpArea)
	SWITCH SpArea.iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			RETURN SpArea.vCoords1.z
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX	
		CASE SPAWN_AREA_SHAPE_ANGLED
			RETURN ((SpArea.vCoords1.z + SpArea.vCoords2.z) * 0.5)
		BREAK
	ENDSWITCH	
	RETURN SpArea.vCoords1.z
ENDFUNC

PROC VerifyMaxMins(VECTOR &vMin, VECTOR &vMax)
	FLOAT fTemp
	IF (vMin.x > vMax.x)
		fTemp = vMax.x	
		vMax.x = vMin.x
		vMin.x = fTemp
	ENDIF
	IF (vMin.y > vMax.y)
		fTemp = vMax.y	
		vMax.y = vMin.y
		vMin.y = fTemp
	ENDIF
	IF (vMin.z > vMax.z)
		fTemp = vMax.z	
		vMax.z = vMin.z
		vMin.z = fTemp
	ENDIF
ENDPROC

PROC ConvertBoxToAngledArea(VECTOR vBoxMin, VECTOR vBoxMax, VECTOR &vAngledCoords1, VECTOR &vAngledCoords2, FLOAT &fAngledWidth)
	
	VerifyMaxMins(vBoxMin, vBoxMax)
	
	FLOAT fDiff
	
	fDiff = vBoxMax.x - vBoxMin.x	
	vAngledCoords1.x = vBoxMin.x + (fDiff*0.5)
	vAngledCoords1.y = vBoxMin.y
	vAngledCoords1.z = vBoxMin.z
	
	vAngledCoords2.x = vAngledCoords1.x
	vAngledCoords2.y = vBoxMax.y
	vAngledCoords2.z = vBoxMax.z
	
	fAngledWidth = fDiff*0.5
	
ENDPROC

FUNC BOOL IsPointInsideSphere(VECTOR vPoint, VECTOR vCircleCentre, FLOAT fCircleRadius, BOOL bIncludeAirspaceAbove, BOOL bIncludeAirspaceBelow)

	IF ((bIncludeAirspaceAbove) AND (bIncludeAirspaceBelow))
		vPoint.z = 0.0
		vCircleCentre.z = 0.0
		RETURN (VDIST(vPoint, vCircleCentre) < fCircleRadius + 0.01)
	ELIF (bIncludeAirspaceAbove)
		IF (vPoint.z > vCircleCentre.z)
			vPoint.z = vCircleCentre.z
		ENDIF
		RETURN (VDIST(vPoint, vCircleCentre) < fCircleRadius + 0.01)
	ELIF (bIncludeAirspaceBelow)
		IF (vPoint.z < vCircleCentre.z)
			vPoint.z = vCircleCentre.z
		ENDIF
		RETURN (VDIST(vPoint, vCircleCentre) < fCircleRadius + 0.01)
	ENDIF
	
	// don't check either airspace
	RETURN (VDIST(vPoint, vCircleCentre) < fCircleRadius + 0.01)
	
ENDFUNC



FUNC BOOL IsPointInsideBox(VECTOR vPoint, VECTOR vMin, VECTOR vMax, BOOL bIncludeAirspaceAbove, BOOL bIncludeAirspaceBelow)

	// check max and min are actually that
	VerifyMaxMins(vMin, vMax)

	// check xy - if not inside these return false
	IF NOT (vPoint.x >= vMin.x)
	OR NOT (vPoint.y >= vMin.y)
	OR NOT (vPoint.x <= vMax.x)
	OR NOT (vPoint.y <= vMax.y)
		RETURN(FALSE)
	ENDIF		

	// xy is inside - check z
	IF ((bIncludeAirspaceAbove) AND (bIncludeAirspaceBelow))
		RETURN(TRUE)
	ELIF (bIncludeAirspaceAbove)
		IF (vPoint.z >= vMin.z)
			RETURN(TRUE)
		ENDIF	
	ELIF (bIncludeAirspaceBelow)
		IF (vPoint.z <= vMax.z)
			RETURN(TRUE)
		ENDIF	
	ELSE
		IF (vPoint.z >= vMin.z)
		AND (vPoint.z <= vMax.z) 
			RETURN(TRUE)
		ENDIF	
	ENDIF
	
	// z not inside 
	RETURN(FALSE)
	
ENDFUNC

FUNC BOOL IsPointInsideSpawnArea(VECTOR vCoords, SPAWN_AREA &SpArea, FLOAT fErrorMargin=0.01, BOOL bIncludeAirspaceAbove=FALSE, BOOL bIncludeAirspaceBelow=FALSE)

	SWITCH SPArea.iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			RETURN(IsPointInsideSphere(vCoords, SpArea.vCoords1, SpArea.fFloat + fErrorMargin  + (g_SpawnData.iAttempt * SPArea.fIncreaseDist) , bIncludeAirspaceAbove, bIncludeAirspaceBelow))
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX
			RETURN(IsPointInsideBox(vCoords, SpArea.vCoords1 + <<fErrorMargin*-1.0, fErrorMargin*-1.0, fErrorMargin*-1.0>>, SpArea.vCoords2 + <<fErrorMargin, fErrorMargin, fErrorMargin>>, bIncludeAirspaceAbove, bIncludeAirspaceBelow))
		BREAK
		CASE SPAWN_AREA_SHAPE_ANGLED			
			IF ((bIncludeAirspaceAbove) AND (bIncludeAirspaceBelow))
				RETURN IS_POINT_IN_ANGLED_AREA ( vCoords, SpArea.vCoords1, SpArea.vCoords2, SpArea.fFloat, DEFAULT, FALSE )
			ELIF (bIncludeAirspaceAbove)
				IF IS_POINT_IN_ANGLED_AREA ( vCoords, SpArea.vCoords1, SpArea.vCoords2, SpArea.fFloat, DEFAULT, FALSE)
				AND NOT ((vCoords.z < SpArea.vCoords1.z) AND (vCoords.z < SpArea.vCoords2.z))
					RETURN(TRUE)
				ELSE
					RETURN(FALSE)
				ENDIF
			ELIF (bIncludeAirspaceBelow)
				IF IS_POINT_IN_ANGLED_AREA ( vCoords, SpArea.vCoords1, SpArea.vCoords2, SpArea.fFloat, DEFAULT, FALSE)
				AND NOT ((vCoords.z > SpArea.vCoords1.z) AND (vCoords.z > SpArea.vCoords2.z))
					RETURN(TRUE)
				ELSE
					RETURN(FALSE)
				ENDIF
			ELSE
				RETURN IS_POINT_IN_ANGLED_AREA ( vCoords, SpArea.vCoords1, SpArea.vCoords2, SpArea.fFloat)
			ENDIF
		BREAK
	ENDSWITCH

	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsPointInsideProblemArea(VECTOR vCoords, PROBLEM_AREA &PrbArea)
	RETURN IS_POINT_IN_ANGLED_AREA ( vCoords, PrbArea.vCoords1, PrbArea.vCoords2, PrbArea.fFloat)	
ENDFUNC



FUNC VECTOR GetNearestCarNodeNotInsideZone(VECTOR vStart, VECTOR vCoords1, VECTOR vCoords2, FLOAT fFloat, INT iShape, FLOAT fAdditionalDist=0.1, BOOL bChooseOppositeSide=FALSE)
	
	PRINTLN("GetNearestCarNodeNotInsideZone - called with ", vStart, vCoords1, vCoords2, fFloat, iShape) 
	// first move the start point outside the area.
	SWITCH iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			MovePointOutsideSphere(vStart, vCoords1, fFloat, fAdditionalDist, bChooseOppositeSide)
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX
			MovePointOutsideBox(vStart, vCoords1, vCoords2, fAdditionalDist, bChooseOppositeSide)
		BREAK
		CASE SPAWN_AREA_SHAPE_ANGLED
			MovePointOutsideAngledArea(vStart, vCoords1, vCoords2, fFloat, fAdditionalDist, bChooseOppositeSide)
		BREAK
	ENDSWITCH
	PRINTLN("GetNearestCarNodeNotInsideZone - vStart moved to ", vStart) 
	// now find the first nearest car node outside area (increment by 5 car nodes each time)
	INT i
	VECTOR vNodeCoords
	REPEAT 40 i
		GET_NTH_CLOSEST_VEHICLE_NODE(vStart, (i*5), vNodeCoords, DEFAULT, 0, 0)
		PRINTLN("GetNearestCarNodeNotInsideZone - checking i ", i, ", vNodeCoords = ", vNodeCoords) 
		SWITCH iShape
			CASE SPAWN_AREA_SHAPE_CIRCLE
				IF NOT IsPointInsideSphere(vNodeCoords, vCoords1, fFloat, FALSE, FALSE)
					PRINTLN("GetNearestCarNodeNotInsideZone - returning ", vNodeCoords) 
					RETURN vNodeCoords
				ENDIF
			BREAK
			CASE SPAWN_AREA_SHAPE_BOX
				IF NOT IsPointInsideBox(vNodeCoords, vCoords1, vCoords2, FALSE, FALSE)
					PRINTLN("GetNearestCarNodeNotInsideZone - returning ", vNodeCoords) 
					RETURN vNodeCoords
				ENDIF
			BREAK
			CASE SPAWN_AREA_SHAPE_ANGLED
				IF NOT IS_POINT_IN_ANGLED_AREA(vNodeCoords, vCoords1, vCoords2, fFloat)
					PRINTLN("GetNearestCarNodeNotInsideZone - returning ", vNodeCoords) 
					RETURN vNodeCoords
				ENDIF
			BREAK
		ENDSWITCH
	ENDREPEAT
	PRINTLN("GetNearestCarNodeNotInsideZone - couldn't find car node - returning ", vStart)
	RETURN vStart
ENDFUNC

PROC MovePointOutsideSpawnArea(VECTOR &vPoint, SPAWN_AREA &SpArea, FLOAT fAdditionalDist=0.1, BOOL bChooseOppositeSide=FALSE, BOOL bUseCarNodesForMove=FALSE)
	
	IF (bUseCarNodesForMove)

		SWITCH SpArea.iShape
			CASE SPAWN_AREA_SHAPE_CIRCLE
				vPoint = GetNearestCarNodeNotInsideZone(vPoint, SpArea.vCoords1, <<0.0, 0.0, 0.0>>, SpArea.fFloat + (g_SpawnData.iAttempt * SPArea.fIncreaseDist) , SPAWN_AREA_SHAPE_CIRCLE, fAdditionalDist, bChooseOppositeSide)
			BREAK
			CASE SPAWN_AREA_SHAPE_BOX	
				vPoint = GetNearestCarNodeNotInsideZone(vPoint, SpArea.vCoords1, SpArea.vCoords2, 0.0 , SPAWN_AREA_SHAPE_BOX, fAdditionalDist, bChooseOppositeSide)
			BREAK
			CASE SPAWN_AREA_SHAPE_ANGLED
				vPoint = GetNearestCarNodeNotInsideZone(vPoint, SpArea.vCoords1, SpArea.vCoords2, SpArea.fFloat , SPAWN_AREA_SHAPE_ANGLED, fAdditionalDist, bChooseOppositeSide)
			BREAK
		ENDSWITCH
		
	ELSE	
	
		SWITCH SpArea.iShape
			CASE SPAWN_AREA_SHAPE_CIRCLE
				MovePointOutsideSphere(vPoint, SpArea.vCoords1, SpArea.fFloat + (g_SpawnData.iAttempt * SPArea.fIncreaseDist) , fAdditionalDist, bChooseOppositeSide)
			BREAK
			CASE SPAWN_AREA_SHAPE_BOX	
				MovePointOutsideBox(vPoint, SpArea.vCoords1, SpArea.vCoords2, fAdditionalDist, bChooseOppositeSide)
			BREAK
			CASE SPAWN_AREA_SHAPE_ANGLED
				MovePointOutsideAngledArea(vPoint, SpArea.vCoords1, SpArea.vCoords2, SpArea.fFloat, fAdditionalDist, bChooseOppositeSide)
			BREAK
		ENDSWITCH
		
	ENDIF
ENDPROC

PROC MovePointInsideAngledArea(VECTOR &vPoint, VECTOR vAngled1, VECTOR vAngled2, FLOAT fWidth, FLOAT fAdditionalDistance=0.1, FLOAT fZDiffMultiplier=0.25)
	PRINTLN("[spawning] MovePointInsideAngledArea - called with ",  vPoint, vAngled1, vAngled2, fWidth, fAdditionalDistance)
	VECTOR vec1, vec2, vec3
	VECTOR vCross
	FLOAT fDistFromCentreLine
	FLOAT fDist
	
	// vec1 = from angled coord1 to coord2
	vec1 = vAngled2 - vAngled1
	vec1.z = 0.0
	
	// vec2 = from angled coord1 to point
	vec2 = vPoint - vAngled1
	vec2.z = 0.0
	
	// vec3 = from point to coord 2
	vec3 = vAngled2 - vPoint
	vec3.z = 0.0
	
	vCross = CROSS_PRODUCT(<<0.0, 0.0, 1.0>>, vec1)
	vCross /= VMAG(vCross)
	
	// dis point is from centre line
	fDistFromCentreLine = vMAG(vec2) * SIN(GET_ANGLE_BETWEEN_2D_VECTORS(vec1.x, vec1.y, vec2.x, vec2.y))
	
	VECTOR vSafePoint1
	VECTOR vSafePoint2
	
	// make vCross the same dist as new point is from the centre line of the angled area.
	vCross *= fDistFromCentreLine
	
	// make sure vCross is heading to towards point
	IF NOT (DOT_PRODUCT(vCross, vec2) >= 0 )
		vCross *= -1.0
	ENDIF
	
	vSafePoint1 = vAngled1 + vCross
	vSafePoint2 = vAngled2 + vCross
	
	VECTOR vecToSafe1
	VECTOR vecToSafe2
	
	vecToSafe1 = vSafePoint1 - vPoint
	vecToSafe2 = vSafePoint2 - vPoint
	
	IF (GET_ANGLE_BETWEEN_2D_VECTORS(vec1.x, vec1.y, vec2.x, vec2.y) > 90.0)
		// need to move towards vsafepoint 1
		fDist = VMAG(vecToSafe1)
		vecToSafe1 /= fDist
		vecToSafe1 *= (fDist + fAdditionalDistance)
		// shift point
		vPoint += vecToSafe1	
		PRINTLN("[spawning] MovePointInsideAngledArea - vPoint after 1st shift = ", vPoint)		
	ENDIF
	IF (GET_ANGLE_BETWEEN_2D_VECTORS(vec1.x, vec1.y, vec3.x, vec3.y) > 90.0)
		// need to move towards vsafepoint 2
		fDist = VMAG(vecToSafe2)
		vecToSafe2 /= fDist
		vecToSafe2 *= (fDist + fAdditionalDistance)
		// shift point
		vPoint += vecToSafe2
		PRINTLN("[spawning] MovePointInsideAngledArea - vPoint after 2nd shift = ", vPoint)		
	ENDIF

	// now we need to shift towards the centre line.
	IF ( fDistFromCentreLine > ((fWidth*0.5)-fAdditionalDistance))
		vCross /= vMAG(vCross)
		fDist = (fDistFromCentreLine - (fWidth*0.5))
		fDist +=  fAdditionalDistance
		
		vCross *= fDist
		vCross *= -1.0
		
		// shift point
		vPoint += vCross
		PRINTLN("[spawning] MovePointInsideAngledArea - vPoint after 3rd shift = ", vPoint)		
	ENDIF
	
	// now move inside height
	FLOAT fMinZ, fMaxZ
	FLOAT fZDiff
	IF (vAngled1.z > vAngled2.z)
		fMinZ = vAngled2.z
		fMaxZ = vAngled1.z
	ELSE
		fMinZ = vAngled1.z
		fMaxZ = vAngled2.z
	ENDIF
	fZDiff = fMaxZ - fMinZ
	
	IF (vPoint.z > (fMaxZ - (fZDiff*fZDiffMultiplier)))
		vPoint.z = (fMaxZ - (fZDiff*fZDiffMultiplier))
		PRINTLN("[spawning] MovePointInsideAngledArea - vPoint after z shift max = ", vPoint)
	ENDIF
	IF (vPoint.z < (fMinZ + (fZDiff*fZDiffMultiplier)))
		vPoint.z = (fMinZ + (fZDiff*fZDiffMultiplier))
		PRINTLN("[spawning] MovePointInsideAngledArea - vPoint after z shift min = ", vPoint)
	ENDIF
			
	PRINTLN("[spawning] MovePointInsideAngledArea - returning = ", vPoint)

ENDPROC


PROC MovePointInsideBox(VECTOR &vPoint, VECTOR vMin, VECTOR vMax, FLOAT fAdditionalDist=0.01)
	PRINTLN("[spawning] MovePointInsideBox - called with ",  vPoint, vMin, vMax, fAdditionalDist)
	IF (vPoint.x < vMin.x)
		vPoint.x = vMin.x + fAdditionalDist
	ENDIF
	IF (vPoint.x > vMax.x)
		vPoint.x = vMax.x - fAdditionalDist		
	ENDIF
	IF (vPoint.y < vMin.y)
		vPoint.y = vMin.y + fAdditionalDist
	ENDIF
	IF (vPoint.y > vMax.y)
		vPoint.y = vMax.y - fAdditionalDist		
	ENDIF
	
	// now move inside height
	FLOAT fMinZ, fMaxZ
	FLOAT fZDiff
	IF (vMin.z > vMax.z)
		fMinZ = vMax.z
		fMaxZ = vMin.z
	ELSE
		fMinZ = vMin.z
		fMaxZ = vMax.z
	ENDIF
	fZDiff = fMaxZ - fMinZ
	
	IF (vPoint.z > (fMaxZ - (fZDiff*0.25)))
		vPoint.z = (fMaxZ - (fZDiff*0.25))
		PRINTLN("[spawning] MovePointInsideBox - vPoint after z shift max = ", vPoint)
	ENDIF
	IF (vPoint.z < (fMinZ + (fZDiff*0.25)))
		vPoint.z = (fMinZ + (fZDiff*0.25))
		PRINTLN("[spawning] MovePointInsideBox - vPoint after z shift min = ", vPoint)
	ENDIF	
	PRINTLN("[spawning] MovePointInsideBox - returning ",  vPoint)
ENDPROC


PROC MovePointInsideSpawnArea(VECTOR &vPoint, SPAWN_AREA &SpArea, FLOAT fAdditionalDist=0.1)
	SWITCH SpArea.iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			MovePointInsideSphere(vPoint, SpArea.vCoords1, SpArea.fFloat, fAdditionalDist)
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX	
			MovePointInsideBox(vPoint, SpArea.vCoords1, SpArea.vCoords2, fAdditionalDist)
		BREAK
		CASE SPAWN_AREA_SHAPE_ANGLED
			MovePointInsideAngledArea(vPoint, SpArea.vCoords1, SpArea.vCoords2, SpArea.fFloat, fAdditionalDist)
		BREAK
	ENDSWITCH
ENDPROC

PROC MovePointToOppositeSideOfSpawnArea(VECTOR &vPoint, SPAWN_AREA &SpArea)
	VECTOR vCentre
	VECTOR vToCentre
	VECTOR vNewPoint
	SWITCH SpArea.iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			vCentre = SpArea.vCoords1
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX	
		CASE SPAWN_AREA_SHAPE_ANGLED
			vCentre = (SpArea.vCoords1 + SpArea.vCoords2) * 0.5
		BREAK		
	ENDSWITCH
	vToCentre = vCentre - vPoint
	vNewPoint = vPoint + (vToCentre * 2.0)
	// keep the original z
	vNewPoint.z = vPoint.z
	PRINTLN("[spawning] MovePointToOppositeSideOfSpawnArea - moving vPoint ", vPoint, " to ", vNewPoint)
	vPoint = vNewPoint
ENDPROC

PROC MovePointTowardsCentreOfSpawnArea(VECTOR &vPoint, SPAWN_AREA &SpArea, FLOAT fDistToMove)
	VECTOR vCentre
	VECTOR vToCentre
	VECTOR vNewPoint
	SWITCH SpArea.iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			vCentre = SpArea.vCoords1
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX	
		CASE SPAWN_AREA_SHAPE_ANGLED
			vCentre = (SpArea.vCoords1 + SpArea.vCoords2) * 0.5
		BREAK		
	ENDSWITCH
	vToCentre = vCentre - vPoint
	IF (VMAG(vToCentre) > fDistToMove)
		vToCentre /= VMAG(vToCentre)
		vNewPoint = vPoint + (vToCentre * fDistToMove)
		PRINTLN("[spawning] MovePointTowardsCentreOfSpawnArea - moving vPoint ", vPoint, " by ", fDistToMove, " to ", vNewPoint)
	ELSE
		vNewPoint = vCentre
		PRINTLN("[spawning] MovePointTowardsCentreOfSpawnArea - moving vPoint ", vPoint, " to centre point  ", vCentre)
	ENDIF
	
	vPoint = vNewPoint	
ENDPROC


FUNC INT GetNumActiveMissionSpawnAreas()
	INT i
	INT iCount
	REPEAT MAX_NUMBER_OF_MISSION_SPAWN_AREAS i
		IF (g_SpawnData.MissionSpawnDetails.SpawnArea[i].bIsActive)
			iCount += 1
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] GetNumActiveMissionSpawnAreas - iCount = ") NET_PRINT_INT(iCount) NET_NL()
	#ENDIF
	
	RETURN(iCount)
ENDFUNC

FUNC INT GetClosestActiveMissionSpawnArea(VECTOR vPoint)
	INT iIndex
	GetDistanceToClosestActiveSpawnAreaInArray(vPoint, g_SpawnData.MissionSpawnDetails.SpawnArea, iIndex)
	RETURN iIndex
ENDFUNC



FUNC INT GetGlobalExclusionAreaForPoint(VECTOR vCoord, BOOL bActiveExclusionAreasOnly=TRUE)
	INT i
	REPEAT MAX_NUMBER_OF_GLOBAL_EXCLUSION_AREAS i 
		IF (GlobalExclusionArea[i].ExclusionArea.bIsActive = TRUE)								
		OR (bActiveExclusionAreasOnly = FALSE)
			IF IsPointInsideSpawnArea(vCoord, GlobalExclusionArea[i].ExclusionArea, 0.1)
				RETURN i
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC


FUNC BOOL IsPointNearHangarLocalPlayerCanSpawnAt(VECTOR vPoint, INT &iReturnSimpleInteriorAsPropertyID)
	
	PRINTLN("[spawning] IsPointNearHangarLocalPlayerCanSpawnAt - called with ", vPoint)
	
	INT iHangarProperty 
	SIMPLE_INTERIORS eHangarSimpleInterior
	INT iExclusionAreaPoint
	iReturnSimpleInteriorAsPropertyID = -1
	
	iExclusionAreaPoint = GetGlobalExclusionAreaForPoint(vPoint, FALSE)
	IF NOT (iExclusionAreaPoint = -1)
		
		PRINTLN("[spawning] IsPointNearHangarLocalPlayerCanSpawnAt - iExclusionAreaPoint ", iExclusionAreaPoint)
		
		FOR iHangarProperty = ENUM_TO_INT(SIMPLE_INTERIOR_HANGAR_1) TO ENUM_TO_INT(SIMPLE_INTERIOR_HANGAR_5) STEP 1	
			PRINTLN("[spawning] IsPointNearHangarLocalPlayerCanSpawnAt - checking iHangarProperty ", iHangarProperty)
			eHangarSimpleInterior = INT_TO_ENUM(SIMPLE_INTERIORS, iHangarProperty)
			IF CanLocalPlayerSpawnAtHangar(eHangarSimpleInterior)					
				IF (GetGlobalExclusionAreaForPoint(g_SimpleInteriorData.vMidPoints[iHangarProperty], FALSE) = iExclusionAreaPoint)
					PRINTLN("[spawning] IsPointNearHangarLocalPlayerCanSpawnAt - true")
					iReturnSimpleInteriorAsPropertyID = iHangarProperty + 1000 // add 1000 as this is not really a property id, it is a simple interior
					RETURN TRUE				
				ENDIF
			ENDIF
		ENDFOR
		
	ENDIF
		
	RETURN FALSE

ENDFUNC













