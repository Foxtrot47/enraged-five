call setenv
echo Creating Model_Enums.sch ... 

if "%1"=="-cruisecontrol" goto :CRUISECONTROL
if "%1"=="-local" goto :LOCAL

ruby %RS_TOOLSLIB%\util\enum_maker.rb --scm x:\gta5\script\dev_ng\core\game\data\model_enums.sch x:\gta5\script\dev_ng\enum_maker.txt
echo Created Model Enums Created : Now check p4 for any changelist for you to submit if the modelenums has changed.
GOTO :FINISH

:LOCAL
ruby %RS_TOOLSLIB%\util\enum_maker.rb x:\gta5\script\dev_ng\core\game\data\model_enums.sch X:\gta5\script\dev_ng\enum_maker.txt
echo Created Model Enums Created
goto :FINISH

:CRUISECONTROL
ruby %RS_TOOLSLIB%\util\enum_maker.rb --scm --submit X:\gta5\script\dev_ng\core\game\data\model_enums.sch X:\gta5\script\dev_ng\enum_maker.txt
goto :END

:FINISH
if "%1"=="-nopause" goto :END
pause
:END


