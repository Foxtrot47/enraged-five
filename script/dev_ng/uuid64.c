#define STRICT
#include <windows.h>
#include <bcrypt.h>
#include <stdio.h>

#pragma comment(lib,"bcrypt.lib")
#pragma comment(lib,"user32.lib")


void main() {
	unsigned __int64 buffer = 0;
	char text[32];
	HANDLE newHandle;
	char *newS;

	// BCRYPT_ALG_HANDLE algo;
	// BCryptOpenAlgorithmProvider(&algo, MS_PRIMITIVE_PROVIDER, NULL, 0);
	// make sure upper 32 bits aren't clear
	while ((unsigned)buffer == buffer)
		BCryptGenRandom(NULL, (char*)&buffer, sizeof(buffer), BCRYPT_USE_SYSTEM_PREFERRED_RNG);
	
	sprintf(text,"0x%016I64x",buffer);

	OpenClipboard(NULL);
	EmptyClipboard();
	newHandle = GlobalAlloc(GMEM_MOVEABLE,strlen(text)+1);
	newS = (char*) GlobalLock(newHandle);
	strcpy(newS, text);
	GlobalUnlock(newHandle);
	SetClipboardData(CF_TEXT,newHandle);
	CloseClipboard();
	puts(text);
}
