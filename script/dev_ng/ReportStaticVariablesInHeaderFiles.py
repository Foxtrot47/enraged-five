import argparse
import sys
import glob
import re
import time

class CRecordForOneSchFile:
	def __init__(self):
		self.number_of_static_variables = 0
		self.sc_files_that_include_this_sch = []

	def update(self, number_of_statics, name_of_sc_file):
            self.number_of_static_variables = max(self.number_of_static_variables, number_of_statics)
            self.sc_files_that_include_this_sch.append(name_of_sc_file)


print ('About to run ReportStaticVariablesInHeaderFiles')

start_time = time.time()

parser = argparse.ArgumentParser(description='Read information about exclusive static variables from the scd files in the input folder. To create the required debug information in the scd files, you first need to add -printstaticsinheadersexclusive to Tools -> Settings -> Compiling -> SanScript -> Custom in the Script Editor. Then compile your scripts. You can either compile all scripts or only the individual scripts that you\'re interested in. The scd files will contain lines with the string \"exclusive statics in\"')

parser.add_argument('folder_containing_scd_files', help='The input folder that contains the scd files to be scanned')
parser.add_argument('out_text_file', help='The output text file that will contain the results')
parser.add_argument('--list_sc_files', action='store_true', help='For each sch file, list the sc files that include it [default: don\'t list the sc files]')

args = parser.parse_args()

dictionary_of_sch_files = {}

filenames = glob.glob(args.folder_containing_scd_files + '\\*.scd')

for filename in filenames:
  matchObj = re.match( r'.*[\\/](.*)\.scd\Z', filename)
  name_of_sc_file = matchObj.group(1) + '.sc'
  
  with open(filename) as text_file:
          for line in text_file:
                  if "exclusive statics in" in line:
                          matchObj = re.match( r'(\d+) exclusive statics in (.*\.sch)\..*', line)
                          if matchObj:
                                  num_of_statics = int(matchObj.group(1))
                                  path_to_sch = matchObj.group(2).lower().replace("\\", "/")
#                                 print "{0} excl statics in {1}".format(num_of_statics, path_to_sch)
                                  if path_to_sch in dictionary_of_sch_files:
                                    dictionary_of_sch_files[path_to_sch].update(num_of_statics, name_of_sc_file)
                                  else:
                                    new_record = CRecordForOneSchFile()
                                    new_record.update(num_of_statics, name_of_sc_file)
                                    dictionary_of_sch_files[path_to_sch] = new_record
                                  

array_of_data_for_each_sch_file = []

for (k, v) in dictionary_of_sch_files.iteritems():
  # print '{0} declares {1} statics and is included in {2} sc files'.format(k, v.number_of_static_variables, len(v.sc_files_that_include_this_sch))
  array_of_data_for_each_sch_file.append((k, v.number_of_static_variables, len(v.sc_files_that_include_this_sch)))


with open(args.out_text_file, 'w') as output_text_file:
  array_of_data_for_each_sch_file.sort(reverse=True, key=lambda item_tuple: item_tuple[1])
  output_text_file.write('Ordered by number of statics\n')
  for tup in array_of_data_for_each_sch_file:
    output_text_file.write('{0} contains {1} statics. (include in {2} .sc files)\n'.format(tup[0], tup[1], tup[2]))

  array_of_data_for_each_sch_file.sort(reverse=True, key=lambda item_tuple: item_tuple[2])
  output_text_file.write('\n\nOrdered by number of includes\n')
  for tup in array_of_data_for_each_sch_file:
    output_text_file.write('{0} is included in {2} .sc files ({1} statics)\n'.format(tup[0], tup[1], tup[2]))
    if args.list_sc_files:
      header_file_record = dictionary_of_sch_files[tup[0]]
      for sc_file in header_file_record.sc_files_that_include_this_sch:
        output_text_file.write('{0}\n'.format(sc_file))
      output_text_file.write('\n')

end_time = time.time()
print 'Completed in {:0.2f} seconds'.format(end_time - start_time)

sys.exit(0)
