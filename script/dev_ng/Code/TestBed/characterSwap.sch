USING "commands_pad.sch"
USING "commands_entity.sch"
USING "commands_streaming.sch"
USING "commands_player.sch"
USING "commands_hud.sch"

FUNC MODEL_NAMES GET_NEXT_PLAYER_MODEL()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SWITCH GET_ENTITY_MODEL(PLAYER_PED_ID())
			CASE PLAYER_ZERO
				PRINTLN("Swapping player to model PLAYER_ONE.")
				RETURN PLAYER_ONE
			BREAK
			CASE PLAYER_ONE
				PRINTLN("Swapping player to model PLAYER_TWO.")
				RETURN PLAYER_TWO
			BREAK
			CASE PLAYER_TWO
				PRINTLN("Swapping player to model S_M_Y_COP_01.")
				RETURN S_M_Y_COP_01
			BREAK
			CASE S_M_Y_COP_01
				PRINTLN("Swapping player to model S_F_Y_COP_01.")
				RETURN S_F_Y_COP_01
			BREAK
			CASE S_F_Y_COP_01
				PRINTLN("Swapping player to model PLAYER_ZERO.")
				RETURN PLAYER_ZERO
			BREAK
		ENDSWITCH
	ENDIF
	
	//Default to Michael if the player is injured.
	RETURN PLAYER_ZERO
ENDFUNC


INT iSwapHelpEndTime = -1

PROC UPDATE_C_PLAYER_CHAR_SWAP()

	IF iSwapHelpEndTime = -1
		iSwapHelpEndTime = GET_GAME_TIMER() + 25000
	ENDIF
	IF GET_GAME_TIMER() < iSwapHelpEndTime
		SET_TEXT_FONT(FONT_STANDARD)
		SET_TEXT_SCALE (0.4, 0.45)
		SET_TEXT_WRAP(0.0, 1.0)
		SET_TEXT_DROPSHADOW (0,0,0,0,255)
		SET_TEXT_COLOUR(0,200,0,255)
		SET_TEXT_EDGE (0,0,0,0,255)
		SET_TEXT_PROPORTIONAL (FALSE)
		SET_TEXT_JUSTIFICATION(FONT_CENTRE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.05, "STRING", "C to switch player models.")
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_C, KEYBOARD_MODIFIER_NONE, "Swap player char model")
		MODEL_NAMES eNextModel = GET_NEXT_PLAYER_MODEL()
		
		REQUEST_MODEL(eNextModel)
		WHILE NOT HAS_MODEL_LOADED(eNextModel)
			REQUEST_MODEL(eNextModel)
			WAIT(0)
		ENDWHILE
		
		SET_PLAYER_MODEL(PLAYER_ID(), eNextModel)
		SET_MODEL_AS_NO_LONGER_NEEDED(eNextModel)
	ENDIF
ENDPROC
