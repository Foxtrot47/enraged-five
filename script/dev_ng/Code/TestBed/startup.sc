USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_streaming.sch"
USING "commands_script.sch"
USING "commands_player.sch"
USING "commands_debug.sch"
USING "commands_ped.sch"
USING "commands_camera.sch"
USING "commands_hud.sch"
USING "commands_volume.sch"
USING "stack_sizes.sch"
USING "script_debug.sch"
USING "test_tools.sch"
USING "commands_entity.sch"
USING "commands_decorator.sch"
USING "characterSwap.sch"

// Mission Start Coords
VECTOR 	vPlayerStart
CONST_FLOAT	MAIN_START_HEADING		0.0

GLOBALS TRUE

CONST_INT	SCRIPT_BRAIN_GROUP_SINGLE_PLAYER		1

SCRIPT 
	PRINTLN( "LOADING STARTUP.SC FOR TESTBED")

	vPlayerStart = GET_PLAYER_START_POS()
	LOAD_SCENE(vPlayerStart)
	
	SET_GRAVITY_LEVEL(GRAV_EARTH)
	
	theplayerindex = GET_PLAYER_INDEX()	//	CREATE_PLAYER(0, PlayerStart)
	scplayer = GET_PLAYER_PED(theplayerindex)
	
	// Activate env_test train
	IF GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_GFX_TEST // Level enums are out of whack apparently
		DELETE_ALL_TRAINS()
		SWITCH_TRAIN_TRACK(0,TRUE)
		SET_TRAIN_TRACK_SPAWN_FREQUENCY(0, 15000)
		SET_RANDOM_TRAINS(TRUE)
	ENDIF
	
	// Allocates the Drive to Anim test script ("driveto_test.sc") to the oil pump object in cptestbed.
	REGISTER_OBJECT_SCRIPT_BRAIN("driveto_test", P_Oil_Pjack_03_S, 100, 100.0, -1, SCRIPT_BRAIN_GROUP_SINGLE_PLAYER)
	REGISTER_OBJECT_SCRIPT_BRAIN("driveto_test", P_Oil_Pjack_02_S, 100, 100.0, -1, SCRIPT_BRAIN_GROUP_SINGLE_PLAYER)
	//REGISTER_OBJECT_SCRIPT_BRAIN("driveto_test", p_pjack_fragtest_s, 100, 100.0, -1, SCRIPT_BRAIN_GROUP_SINGLE_PLAYER)
	
	// NOTE: The 'playercoords.txt' file in GTA_pc (used by the artists) will override these coords
	IF NOT IS_ENTITY_DEAD(scplayer)
		SET_PED_COORDS_KEEP_VEHICLE(scplayer, vPlayerStart)
		SET_ENTITY_HEADING(scplayer, MAIN_START_HEADING)
	ENDIF

	SHUTDOWN_LOADING_SCREEN()

	// Initial Setup
	SET_DEBUG_ACTIVE(FALSE)

	WAIT (0)
	WAIT (0)
	
	// load xml script
	REQUEST_SCRIPT("xml_menus")
	WHILE NOT HAS_SCRIPT_LOADED("xml_menus")
		WAIT(0)
	ENDWHILE
	START_NEW_SCRIPT("xml_menus", DEFAULT_STACK_SIZE)

/*	
	REQUEST_SCRIPT("object_widget")
	WHILE NOT HAS_SCRIPT_LOADED("object_widget")
		WAIT(0)
	ENDWHILE
	START_NEW_SCRIPT("object_widget", DEFAULT_STACK_SIZE)
*/	
	
	DO_SCREEN_FADE_IN(500)
	WHILE NOT IS_SCREEN_FADED_IN()
		WAIT(0)
	ENDWHILE

	WAIT (0)
	WAIT (0)

	PRINT_HELP ("tstdbg11")

	//VOLUME cylinder = CREATE_VOLUME_CYLINDER(<<-3.47, 75.30, 7.35>>,<<0.0,0.0,0.0>>,<<1.0,1.0,1.0>>)
//	VOLUME box = CREATE_VOLUME_BOX(<<-1.47, 78.30, 7.35>>,<<0.0,0.0,0.0>>,<<1.0,2.0,1.0>>)
	//VOLUME box2 = CREATE_VOLUME_BOX(<<-1.47, 78.30, 7.35>>,<<0.0,0.0,90.0>>,<<1.0,4.0,1.0>>)
	//VOLUME sphere = CREATE_VOLUME_SPHERE(<<-1.47, 73.30, 7.35>>,<<0.0,0.0,0.0>>,<<2.0,1.0,1.0>>)
	
	//VOLUME agg = CREATE_VOLUME_AGGREGATE()
	//ADD_TO_VOLUME_AGGREGATE(agg,box2)
	
	//VOLUME agg2
	//VOLUME ellipse
	
	DECOR_REGISTER("testdec1", DECOR_TYPE_BOOL)
	DECOR_REGISTER("testdec3", DECOR_TYPE_FLOAT)
	DECOR_REGISTER("testdec4", DECOR_TYPE_INT)
	// KGM 16/5/12: Locks decorator registration after all are registered - no more registration allowed after this point
	DECOR_REGISTER_LOCK()

	
	DECOR_SET_BOOL(scplayer,"testdec1",TRUE)
	DECOR_SET_FLOAT(scplayer,"testdec3",50.0)
	DECOR_SET_INT(scplayer,"testdec4",250)
	
//	DECOR_SET_BOOL(box,"testdec1",TRUE)
//	DECOR_SET_FLOAT(box,"testdec3",51.0)
//	DECOR_SET_INT(box,"testdec4",251)
//	DECOR_REMOVE(box, "testdec1")
	//MAIN LOOP
	WHILE TRUE
		WAIT (0)
		
		UPDATE_C_PLAYER_CHAR_SWAP()
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_K, KEYBOARD_MODIFIER_SHIFT, "Kill local player")
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_HEALTH(PLAYER_PED_ID(), 0)
			ENDIF
		ENDIF
				
		//IF IS_POINT_IN_VOLUME(box2,GET_ENTITY_COORDS(PLAYER_PED_ID()))
		//	PRINTSTRING( "Player is in BOX2")
		//	PRINTNL()
		//	DELETE_VOLUME(box2)
		//ENDIF
		
		//IF IS_POINT_IN_VOLUME(cylinder,GET_ENTITY_COORDS(PLAYER_PED_ID()))
		//	PRINTSTRING( "Player is in CYLINDER")
		//	PRINTNL()
		//	IF NOT IS_VOLUME_VALID(ellipse)
		//		ellipse = CREATE_VOLUME_ELLIPSE(<<1.47, 76.30, 7.35>>,<<0.0,0.0,0.0>>,<<1.0,1.0,2.0>>)
		//		ADD_TO_VOLUME_AGGREGATE(agg,ellipse)
		//	ENDIF
		//ENDIF
		
		//IF IS_POINT_IN_VOLUME(box,GET_ENTITY_COORDS(PLAYER_PED_ID()))
		//	PRINTSTRING( "Player is in BOX")
		//	PRINTNL()
		//	DELETE_VOLUME(box)
		//ENDIF
		
		//IF IS_POINT_IN_VOLUME(agg,GET_ENTITY_COORDS(PLAYER_PED_ID()))
		//	PRINTSTRING( "Player is in AGGREGATE")
		//	PRINTNL()
		//	DELETE_VOLUME(agg)
		//ENDIF
		
		//IF IS_POINT_IN_VOLUME(agg2,GET_ENTITY_COORDS(PLAYER_PED_ID()))
		//	PRINTSTRING( "Player is in AGGREGATE2")
		//	PRINTNL()
		//	DELETE_VOLUME(agg2)
		//ENDIF
		
		//IF IS_POINT_IN_VOLUME(sphere,GET_ENTITY_COORDS(PLAYER_PED_ID()))
		//	PRINTSTRING( "Player is in SPHERE")
		//	PRINTNL()
		//	DELETE_VOLUME(sphere)
		//	DELETE_VOLUME(agg2)
		//ENDIF
		
		//IF IS_POINT_IN_VOLUME(ellipse,GET_ENTITY_COORDS(PLAYER_PED_ID()))
		//	PRINTSTRING( "Player is in ELLIPSE")
		//	PRINTNL()
		//	DELETE_VOLUME(ellipse)
		//	IF NOT IS_VOLUME_VALID(agg2)
		//		agg2 = CREATE_VOLUME_AGGREGATE()
		//		ADD_TO_VOLUME_AGGREGATE(agg2,ellipse)
		//	ENDIF
		//ENDIF
	ENDWHILE

ENDSCRIPT

