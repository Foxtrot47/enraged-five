// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "script_xml.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "commands_streaming.sch"
USING "commands_graphics.sch"
USING "commands_debug.sch"
USING "test_tools.sch"
USING "commands_vehicle.sch"
USING "commands_camera.sch"
USING "commands_player.sch"
USING "script_DEBUG.sch"
USING "commands_script.sch"
USING "commands_entity.sch"

//Notes: This is script is to allow easy setup of test scenarios for the code department. It works in conjunction with test_tools.sch
//It  is a data driven system 

//Defaults values when the test is reset to factory the standard values are used

//Level 
	STRING XMLMenu = "Testbed/TestNavmenu"
	VECTOR gvMapOffset //offset all coords from the origin by the psoition of the mapin world coords
//Ped Vars
CONST_INT MAX_NUMBER_OF_PEDS 			10

peds_struct TestPeds [MAX_NUMBER_OF_PEDS]

//Vehicle Vars
CONST_INT MAX_NUMBER_OF_VEHICLES 	 10

vehicle_struct TestVehicles[MAX_NUMBER_OF_VEHICLES]

//Route Vars 
CONST_INT MAX_NUMBER_OF_NODES 				7

RouteStruct Route[MAX_NUMBER_OF_NODES]

//Camera Vars 
CONST_INT MAX_NUMBER_OF_CAMERAS			2
CONST_INT FixedCamera										0
CONST_INT TrackingCamera									1

StCameraData TestCams[MAX_NUMBER_OF_CAMERAS]

//Sequence 
//Task Vars
SEQUENCE_INDEX TEST_SEQ
bool Blooptest = FALSE


//Main Enums 

//ENUM Test_Scenario
//	InitialiseScenarioData,
//	CreateScenarioEntities,
//	SetScenarioEntities,
//	RunScenario,
//	CleanupScenario
//ENDENUM
//
//Test_Scenario TestScenarioAStatus = InitialiseScenarioData

//Functions: WIDGETS

FLOAT DYNAMIC_MOVE_BLEND_RATIO
FLOAT SlideSpeed = 1.0
FLOAT BlendSpeed = 8.0
BOOL bLoopAnim = FALSE, bExtractbackwardvel = TRUE, bExtractsidevel = TRUE, bholdlastframe = FALSE
Int seatindex = -1
int gtime = 1000


PROC CREATE_TEST_WIDGET ()
	START_WIDGET_GROUP("Navigation Tests")
		SETUP_WIDGET ()	
			START_WIDGET_GROUP("Navigation")
				ADD_WIDGET_BOOL ("Start scenario", gBeginCombatScenario)
				ADD_WIDGET_BOOL ("Restart scenario", gResetCombatScenario)
				ADD_WIDGET_BOOL ("Reset to default", gResetToDefault)
				ADD_WIDGET_BOOL ("Debug scenario", gRun_debuggig)
				ADD_WIDGET_BOOL ("Print all setup info", gPrintAllIinfo)
				ADD_WIDGET_BOOL ("Loop test", Blooptest)
				ADD_WIDGET_FLOAT_SLIDER ("Start_Coord.x",gPedcoords.x,  -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("Start_Coord.y",gPedcoords.y,  -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("Start_Coord.z",gPedcoords.z,  -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("Start_Heading",gPedheading,  -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("Target_Coord.x",DynamicTargetPos.x,  -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("Target_Coord.y",DynamicTargetPos.y,  -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("Target_Coord.z",DynamicTargetPos.z,  -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("Target_heading",DynamicTargetHeading,  -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("Slide speed",SlideSpeed,  -2000, 2000, 0.5)
				ADD_WIDGET_INT_SLIDER ("time", gtime ,-2, 5000, 1 )
				ADD_WIDGET_INT_SLIDER ("seat", seatindex ,-2, 2, 1 )
				ADD_WIDGET_FLOAT_SLIDER ("Move Blend Ratio", DYNAMIC_MOVE_BLEND_RATIO, PEDMOVEBLENDRATIO_STILL, PEDMOVEBLENDRATIO_SPRINT, 0.1 )
				START_WIDGET_GROUP ("Anim")
					ADD_WIDGET_BOOL ("loop anim", bLoopAnim )
					ADD_WIDGET_BOOL ("Extract y vel", bExtractbackwardvel )
					ADD_WIDGET_BOOL ("Extract x vel ", bExtractsidevel )
					ADD_WIDGET_BOOL ("Hold ladt frame ", bholdlastframe )
					ADD_WIDGET_FLOAT_SLIDER ("blend",BlendSpeed,  -2000, 2000, 0.5)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP ("Ped setup")
				ADD_WIDGET_INT_SLIDER ("Ped_index", gTestPedsIndex ,0, 9, 1 )
				ADD_WIDGET_FLOAT_SLIDER ("Ped_Coord.x",gPedcoords.x,  -2000, 2000, 0.25)
				ADD_WIDGET_FLOAT_SLIDER ("Ped_Coord.y",gPedcoords.y,  -2000, 2000, 0.25)
				ADD_WIDGET_FLOAT_SLIDER ("Ped_Coord.z",gPedcoords.z,  -2000, 2000, 0.25)
				ADD_WIDGET_FLOAT_SLIDER ("Ped_Heading",gPedheading,  -2000, 2000, 0.5)
					START_NEW_WIDGET_COMBO() 
					   ADD_TO_WIDGET_COMBO ("S_M_Y_Cop_01")
                        ADD_TO_WIDGET_COMBO ("G_M_Y_DESHENCH_01")
					STOP_WIDGET_COMBO("Ped", PedIdentifier)
						ADD_WIDGET_BOOL ("Create ped", gCreatePed)
						ADD_WIDGET_BOOL ("Delete ped", gdeletePed)
						ADD_WIDGET_INT_SLIDER ("Set Ped rel grp", gpedrelgrp  ,1, 4, 1 )
					START_NEW_WIDGET_COMBO() 
						ADD_TO_WIDGET_COMBO ("WEAPONTYPE_UNARMED")
						ADD_TO_WIDGET_COMBO ("WEAPONTYPE_PISTOL")
						ADD_TO_WIDGET_COMBO ("WEAPONTYPE_ASSAULTRIFLE")
						ADD_TO_WIDGET_COMBO ("WEAPONTYPE_M4")
					STOP_WIDGET_COMBO("weapon", WeaponIdentifier)
						ADD_WIDGET_BOOL ("Swap current peds weapon", gSwapPedWeapons) 
						ADD_WIDGET_BOOL ("print ped info", Bprint_ped_info)
				ADD_WIDGET_BOOL ("activate def area",gPedDefAActive)	
			STOP_WIDGET_GROUP ()
			START_WIDGET_GROUP ("Route Builder")
				ADD_WIDGET_FLOAT_SLIDER ("RoutePoint.x",gRoutePoint.x,  -2000, 2000, 0.25)
				ADD_WIDGET_FLOAT_SLIDER ("RoutePoint.y",gRoutePoint.y,  -2000, 2000, 0.25)
				ADD_WIDGET_FLOAT_SLIDER ("RoutePoint.z",gRoutePoint.z,  -2000, 2000, 0.25)
				ADD_WIDGET_INT_SLIDER ("Route Node", gRouteNode, 0, 7, 1 )
				ADD_WIDGET_BOOL ("Register Node", BSetNode)
				ADD_WIDGET_BOOL ("copy cam pos", BCopyCamCoords )
			STOP_WIDGET_GROUP ()	
		STOP_WIDGET_GROUP()
ENDPROC

// Functions end: WIDGET

//Mission flow House keeping

//PUPROSE: REmoves all the scenarios and resets all the relationships
	PROC Cleanup_Scenario_Entities ()
	int index = 0
		for index = 0 to MAX_NUMBER_OF_PEDS -1 	
			CLEAN_UP_PED_ENTITIES (TestPeds[index].ped)
		ENDFOR 
		
		for index = 0 to MAX_NUMBER_OF_VEHICLES -1
			CLEAN_UP_VEHICLE_ENTITIES (TestVehicles[index].Vehicle )
		ENDFOR

		Set_Gang_Relationships (FALSE)	
	ENDPROC


PROC Terminate_test_script ()
	if IS_KEYBOARD_KEY_JUST_PRESSED (KEY_S)
		CLEAR_PRINTS ()
		CLEAR_HELP ()
		Cleanup_Scenario_Entities ()
		Temp_cleanup_scenario_cams ()
		SET_PLAYER_COLISION(scplayer, true)
		IF NOT IS_PED_INJURED (scplayer)
				  SET_PED_COORDS_KEEP_VEHICLE(scplayer, GET_PLAYER_START_POS ())
		ENDIF
		
		TERMINATE_THIS_THREAD ()
		
	ENDIF
ENDPROC



NAVDATA test_data

PROC SETUP_NAVDATE (FLOAT heading )
	test_data.m_fSlideToCoordHeading = heading
ENDPROC

FUNC SEQUENCE_INDEX FOLLOW_NAVMESH_TO_COORD_SEQ ( VECTOR L_Target_Pos, FLOAT L_Target_heading)
		SETUP_NAVDATE (L_Target_heading)
		
		SEQUENCE_INDEX TEST_SEQUENCE
		OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
				TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED  (NULL, L_Target_Pos, DYNAMIC_MOVE_BLEND_RATIO, -1, 1, ENAV_ADV_SLIDE_TO_COORD_AND_ACHIEVE_HEADING_AT_END, test_data )
		CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)

		RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX SLIDE_TO_COORD_SEQ ( VECTOR L_Target_Pos, FLOAT L_Target_heading)
		SEQUENCE_INDEX TEST_SEQUENCE
		OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
				TASK_PED_SLIDE_TO_COORD   (NULL, L_Target_Pos,  L_Target_heading, SlideSpeed)
		CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX SLIDE_TO_COORD_AND_PLAY_ANIM_SEQ ( VECTOR L_Target_Pos, FLOAT L_Target_heading, string animName, string AnimDict)
		SEQUENCE_INDEX TEST_SEQUENCE
		OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
			TASK_PED_SLIDE_TO_COORD_AND_PLAY_ANIM   (NULL, L_Target_Pos,  L_Target_heading, SlideSpeed, animName,  AnimDict, BlendSpeed,  bLoopAnim, bExtractbackwardvel, bExtractsidevel, bholdlastframe, 0)
		CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX TEST_TASK_OPEN_VEHICLE_DOOR(VEHICLE_INDEX vehicle, int time = -1)
		SEQUENCE_INDEX TEST_SEQUENCE
		OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
				TASK_OPEN_VEHICLE_DOOR  (NULL, vehicle,  time,INT_TO_ENUM (VEHICLE_SEAT, seatindex ) )
		CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX TEST_TASK_PLAY_ANIM(string anim)
	
		SEQUENCE_INDEX TEST_SEQUENCE
		OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
				TASK_PLAY_ANIM  (NULL, "misstest_anim",anim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT) 
		CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		
		RETURN TEST_SEQUENCE
ENDFUNC					

FUNC SEQUENCE_INDEX TEST_TASK_FOLLOW_NAVMESH_ADVANCED(VECTOR target_coord, FLOAT heading)
		SETUP_NAVDATE (heading)
		
		SEQUENCE_INDEX TEST_SEQUENCE
		OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
				TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED  (NULL, target_coord, DYNAMIC_MOVE_BLEND_RATIO,  -1, 1, ENAV_ADV_SLIDE_TO_COORD_AND_ACHIEVE_HEADING_AT_END, test_data )
		CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		
		RETURN TEST_SEQUENCE
ENDFUNC		

FUNC SEQUENCE_INDEX TEST_TASK_STAND_STILL(int time, RouteStruct &TargetRoute[])
	
		SEQUENCE_INDEX TEST_SEQUENCE
		OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
				TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[0].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -1, 1)
				TASK_PAUSE (NULL, time)
				TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[1].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -1, 1)
		CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		
		RETURN TEST_SEQUENCE
ENDFUNC	

FUNC SEQUENCE_INDEX TEST_TASK_ENTER_VEHICLE(VEHICLE_INDEX Vehicle, int seat )
	
		SEQUENCE_INDEX TEST_SEQUENCE
		OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
				TASK_ENTER_VEHICLE  (NULL, Vehicle,   INFINITE_TASK_TIME,  INT_TO_ENUM(VEHICLE_SEAT, seat ))
		CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		
		RETURN TEST_SEQUENCE
ENDFUNC	

FUNC  SEQUENCE_INDEX TEST_TASK_GOTO_PED_OFFSET()

		SEQUENCE_INDEX TEST_SEQUENCE
		OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
			TASK_GOTO_ENTITY_OFFSET(NULL, scplayer, -1, 3.0, 30.0)		
			SET_SEQUENCE_TO_REPEAT(TEST_SEQUENCE, REPEAT_FOREVER)
		CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		
		RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX FOLLOW_ROUTE( RouteStruct &TargetRoute[])
		int index 
		BOOL BHaveValidNode = FALSE
		VECTOR TempNode
		
		//This allows us to pass a sequnce to a ped using 7 points for maximun testing. Iess than 7 nodes can be used, and the last valid node is copied into the remaining array elements.
		//Note all nodes must be in order there can be no gaps between them ie node 1 valid, 2 valid, 3 invalid is ok but not 1 valid, 2 invalid, 3  valid 
		//search our node array until we find a invalid array point
		for index = 0 to MAX_NUMBER_OF_NODES -1
			IF TargetRoute[index].RouteNodeState = FALSE
				IF index <> 0
					TempNode = TargetRoute[index - 1].RouteNode	//grab the previous point which is valid assuming that all nodes are sequential
					BHaveValidNode = TRUE
				ELSE
					SCRIPT_ASSERT ("Passed in sequnce with no valid nodes ")
				ENDIF
			ENDIF
			
			//copy this valid node into the remaing array elements
			IF (BHaveValidNode)
					TargetRoute[index].RouteNode = TempNode	
			ENDIF
				
		ENDFOR	
		
		SEQUENCE_INDEX TEST_SEQUENCE
		OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
			TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[0].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -1, 1)
			TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[1].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -1, 1)
			TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[2].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -1, 1)
			TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[3].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -1, 1)
			TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[4].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -1, 1)
			TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[5].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -1, 1)
			TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[6].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -1, 1)
		CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX TEST_TASK_FOLLOW_POINT_ROUTE_SINGLE( )
	
		SEQUENCE_INDEX TEST_SEQUENCE
		OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
			TASK_FLUSH_ROUTE()
			TASK_EXTEND_ROUTE(<<-8.18, 7.32, 1.19>> + gvMapOffset)
			TASK_EXTEND_ROUTE(<<5.11, 6.30, 1.19>>+ gvMapOffset)
			TASK_EXTEND_ROUTE(<<-2.72, 13.41, 1.19>>+ gvMapOffset)
			TASK_FOLLOW_POINT_ROUTE (NULL, PEDMOVE_WALK, TICKET_SINGLE)
		CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		
		RETURN TEST_SEQUENCE
ENDFUNC	

FUNC SEQUENCE_INDEX TEST_TASK_FOLLOW_POINT_ROUTE_SEASON()
	
		SEQUENCE_INDEX TEST_SEQUENCE
		OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
			TASK_FLUSH_ROUTE()
			TASK_EXTEND_ROUTE(<<-8.18, 7.32, 1.19>>+ gvMapOffset)
			TASK_EXTEND_ROUTE(<<5.11, 6.30, 1.19>>+ gvMapOffset)
			TASK_EXTEND_ROUTE(<<-2.72, 13.41, 1.19>>+ gvMapOffset)
			TASK_FOLLOW_POINT_ROUTE (NULL, PEDMOVE_WALK, TICKET_SEASON)
		CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		
		RETURN TEST_SEQUENCE
ENDFUNC	

//Test Specific Functions
FUNC VECTOR GetOffsetRoundCar(VEHICLE_INDEX vehicle)
	Vector temp = <<0.0, 0.0, 0.0>>, ModelMin, ModelMax
	MODEL_NAMES currentmodel
		
	IF IS_VEHICLE_DRIVEABLE (Vehicle)
		currentmodel = GET_ENTITY_MODEL(TestVehicles[0].Vehicle)
		GET_MODEL_DIMENSIONS (currentmodel,ModelMin, ModelMax )
	

		While ((temp.x < ModelMax.x+1.0) and (temp.x > ModelMin.x - 1.0) ) 
			temp.x = GET_RANDOM_FLOAT_IN_RANGE (-10.0, 10.0)	
		ENDWHILE
		
		WHILE ((temp.y < ModelMax.y+1.0 ) and (temp.y > ModelMin.y -1.0) ) 
			temp.y = GET_RANDOM_FLOAT_IN_RANGE (-15.0, 15.0)	
		ENDWHILE
	ENDIF
	
	return temp
ENDFUNC

//These enums must match with the testNavmenu.xml
ENUM Scenarios
	TestTaskSlideToCoord,
	TestTaskSlideToCoordPlayAnim,
	TestTaskFollowNavmesh,
	TestTaskOpenVehicleDoor,
	TestTaskPlayAnim,
	TestWalkToCarAnim,
	TestPlayRestAtCarAnim,
	TestFollowRoute,
	TestFollowNavemeshAdvacnedHeading,
	TestStandStill, 
	TestSEINOVE,
	TestWINKY,
	TestZ75,
	TestJZ125,
	TestAUTOGYRO,
	TestBCHOPPER, 
	TestHind, 
	TestSKIMOBILE, 
	TestRHINO,
	TestBOBSLEIGH,
	BugTicketSingle,
	BugTicketSeason,
	BugGotoPedOffset,
	TestEnableAssistedMovementRoute1,
	TestDisableAssistedMovementRoute1,
	TestEnableAssistedMovementRoute2,
	TestDisableAssistedMovementRoute2,	
	DefaultTest = 1000
ENDENUM



//  END_SCENARIO: SquadOfPedsMovingBetweenVehicles  
PROC SETUP_TEST_DATA ()
	SWITCH int_to_enum (scenarios, gcurrentselection)
		CASE TestSEINOVE
			TestCams[FixedCamera].cam_pos = <<-24, 14.57, 8.7>> + gvMapOffset
			TestCams[FixedCamera].cam_rot = <<-15.06, 0.0,-97.7>> 
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << -3.7756, 1.0, 1.1959>>  + gvMapOffset
			Testpeds[0].PedHeading = 359.6439
			Testpeds[0].Pedrelgrp = 1
			Testpeds[0].PedModel = S_M_Y_Cop_01
			Testpeds[0].PedsWeapon = WEAPONTYPE_UNARMED
			Testpeds[0].PedcombatMove = 2
			Testpeds[0].pedcombatrange = 2
			Testpeds[0].combatcover = CA_USE_COVER
			Testpeds[0].bcombatcover = FALSE
			
			TestVehicles [0].vehicleCoords = << -0.0012, 10.0009, 1.9393>>  + gvMapOffset
			TestVehicles [0].vehicleHeading = 266.0000
			TestVehicles [0].vehiclemodel = FBI
		BREAK
		
		CASE TestWINKY
			TestCams[FixedCamera].cam_pos = <<-24, 14.57, 8.7>> + gvMapOffset
			TestCams[FixedCamera].cam_rot = <<-15.06, 0.0,-97.7>> 
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << -3.7756, 1.0, 1.1959>>  + gvMapOffset
			Testpeds[0].PedHeading = 359.6439
			Testpeds[0].Pedrelgrp = 1
			Testpeds[0].PedModel = S_M_Y_Cop_01
			Testpeds[0].PedsWeapon = WEAPONTYPE_UNARMED
			Testpeds[0].PedcombatMove = 2
			Testpeds[0].pedcombatrange = 2
			Testpeds[0].combatcover = CA_USE_COVER
			Testpeds[0].bcombatcover = FALSE
			
			TestVehicles [0].vehicleCoords = << -0.0012, 10.0009, 1.9393>>   + gvMapOffset
			TestVehicles [0].vehicleHeading = 266.0000
			TestVehicles [0].vehiclemodel = FBI
		BREAK
		
		CASE TestZ75
			TestCams[FixedCamera].cam_pos = <<-24, 14.57, 8.7>> + gvMapOffset
			TestCams[FixedCamera].cam_rot = <<-15.06, 0.0,-97.7>> 
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << -3.7756, 1.0, 1.1959>>  + gvMapOffset
			Testpeds[0].PedHeading = 359.6439
			Testpeds[0].Pedrelgrp = 1
			Testpeds[0].PedModel = S_M_Y_Cop_01
			Testpeds[0].PedsWeapon = WEAPONTYPE_UNARMED
			Testpeds[0].PedcombatMove = 2
			Testpeds[0].pedcombatrange = 2
			Testpeds[0].combatcover = CA_USE_COVER
			Testpeds[0].bcombatcover = FALSE
			
			TestVehicles [0].vehicleCoords = << -0.0012, 10.0009, 1.9393>>  + gvMapOffset
			TestVehicles [0].vehicleHeading = 266.0000
			TestVehicles [0].vehiclemodel = SANCHEZ
		BREAK
		
		CASE TestJZ125
			TestCams[FixedCamera].cam_pos = <<-24, 14.57, 8.7>> + gvMapOffset
			TestCams[FixedCamera].cam_rot = <<-15.06, 0.0,-97.7>> 
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
					
			Testpeds[0].PedsCoords = << -3.7756, 1.0, 1.1959>>  + gvMapOffset
			Testpeds[0].PedHeading = 359.6439
			Testpeds[0].Pedrelgrp = 1
			Testpeds[0].PedModel = S_M_Y_Cop_01
			Testpeds[0].PedsWeapon = WEAPONTYPE_UNARMED
			Testpeds[0].PedcombatMove = 2
			Testpeds[0].pedcombatrange = 2
			Testpeds[0].combatcover = CA_USE_COVER
			Testpeds[0].bcombatcover = FALSE
			
			TestVehicles [0].vehicleCoords = << -0.0012, 10.0009, 1.9393>>  + gvMapOffset
			TestVehicles [0].vehicleHeading = 266.0000
			TestVehicles [0].vehiclemodel = SANCHEZ
		BREAK
		
		CASE TestAUTOGYRO
			TestCams[FixedCamera].cam_pos = <<-24, 14.57, 8.7>> + gvMapOffset
			TestCams[FixedCamera].cam_rot = <<-15.06, 0.0,-97.7>> 
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << -3.7756, 1.0, 1.1959>>  + gvMapOffset
			Testpeds[0].PedHeading = 359.6439
			Testpeds[0].Pedrelgrp = 1
			Testpeds[0].PedModel = S_M_Y_Cop_01
			Testpeds[0].PedsWeapon = WEAPONTYPE_UNARMED
			Testpeds[0].PedcombatMove = 2
			Testpeds[0].pedcombatrange = 2
			Testpeds[0].combatcover = CA_USE_COVER
			Testpeds[0].bcombatcover = FALSE
			
			TestVehicles [0].vehicleCoords = << -0.0012, 10.0009, 1.9393>>  + gvMapOffset
			TestVehicles [0].vehicleHeading = 266.0000
			TestVehicles [0].vehiclemodel = BUZZARD
		BREAK
		

		
		CASE TestTaskSlideToCoord
		CASE TestTaskSlideToCoordPlayAnim
		CASE TestTaskFollowNavmesh
		CASE TestTaskPlayAnim
		CASE TestFollowRoute
		CASE TestFollowNavemeshAdvacnedHeading
		CASE TestStandStill
		CASE BugTicketSingle
		CASE BugTicketSeason
			TestPeds[0].PedsCoords = << -2.4167, -1.4273, 1.1959>> + gvMapOffset
			TestPeds[0].PedHeading = 359.6439
			TestPeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = S_M_Y_Cop_01
			TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
			TestPeds[0].PedcombatMove = 2
			TestPeds[0].pedcombatrange = 2
			TestPeds[0].combatcover = CA_USE_COVER
			TestPeds[0].bcombatcover = FALSE
		
			//Target for the peds to move too
			DynamicTargetPos = << 0.0, 5.0, 1.0 >> + gvMapOffset
			DynamicTargetHeading = 90.0
			
			Route[0].RouteNode = <<9.3092, 3.2039, 1.3341>> + gvMapOffset
			Route[0].RouteNodeState = true
			
			Route[1].RouteNode = <<17.5666, 13.1102, 1.6668>> + gvMapOffset
			Route[1].RouteNodeState = true
			
			TestCams[FixedCamera].cam_pos = <<-24, 14.57, 8.7>> + gvMapOffset
			TestCams[FixedCamera].cam_rot = <<-15.06, 0.0,-97.7>> 
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
		BREAK
		
		CASE TestTaskOpenVehicleDoor
			Testpeds[0].PedsCoords = << -2.4167, -1.4273, 1.1959>>  + gvMapOffset
			Testpeds[0].PedHeading = 359.6439
			Testpeds[0].Pedrelgrp = 1
			Testpeds[0].PedModel = S_M_Y_Cop_01
			Testpeds[0].PedsWeapon = WEAPONTYPE_PISTOL
			Testpeds[0].PedcombatMove = 2
			Testpeds[0].pedcombatrange = 2
			Testpeds[0].combatcover = CA_USE_COVER
			Testpeds[0].bcombatcover = FALSE
		
			TestVehicles [0].vehicleCoords = << -0.0012, 10.0009, 1.9393>> + gvMapOffset
			TestVehicles [0].vehicleHeading = 90.0001
			TestVehicles [0].vehiclemodel = FBI
		
			TestCams[FixedCamera].cam_pos = <<-24, 14.57, 8.7>> + gvMapOffset
			TestCams[FixedCamera].cam_rot = <<-15.06, 0.0,-97.7>> 
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
		BREAK
		
		CASE TestWalkToCarAnim
			Testpeds[0].PedsCoords = << -3.7756, 1.0, 1.1959>>  + gvMapOffset
			Testpeds[0].PedHeading = 359.6439
			Testpeds[0].Pedrelgrp = 1
			Testpeds[0].PedModel = S_M_Y_Cop_01
			Testpeds[0].PedsWeapon = WEAPONTYPE_UNARMED
			Testpeds[0].PedcombatMove = 2
			Testpeds[0].pedcombatrange = 2
			Testpeds[0].combatcover = CA_USE_COVER
			Testpeds[0].bcombatcover = FALSE
			
			TestVehicles [0].vehicleCoords = << -3.4995, 8.5499, 1.9393>> + gvMapOffset
			TestVehicles [0].vehicleHeading = 266.0000
			TestVehicles [0].vehiclemodel = FBI

			DynamicTargetPos = << -3.7756, 3.4697, 1.1959>> + gvMapOffset
			DynamicTargetHeading = 0.0
			
			TestCams[FixedCamera].cam_pos = <<-24, 14.57, 8.7>> + gvMapOffset
			TestCams[FixedCamera].cam_rot = <<-15.06, 0.0,-97.7>> 
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
		BREAK
		
		CASE TestPlayRestAtCarAnim
			Testpeds[0].PedsCoords = << -3.7756, 1.0, 1.1959>> + gvMapOffset
			Testpeds[0].PedHeading = 359.6439
			Testpeds[0].Pedrelgrp = 1
			Testpeds[0].PedModel = S_M_Y_Cop_01
			Testpeds[0].PedsWeapon = WEAPONTYPE_UNARMED
			Testpeds[0].PedcombatMove = 2
			Testpeds[0].pedcombatrange = 2
			Testpeds[0].combatcover = CA_USE_COVER
			Testpeds[0].bcombatcover = FALSE
			
			TestVehicles [0].vehicleCoords = << -3.4995, 8.5499, 1.9393>>  + gvMapOffset
			TestVehicles [0].vehicleHeading = 266.0000
			TestVehicles [0].vehiclemodel = FBI

			DynamicTargetPos = << -3.776, 6.97, 1.1959>> + gvMapOffset
			DynamicTargetHeading = 0.0
			
			TestCams[FixedCamera].cam_pos = <<-24, 14.57, 8.7>> + gvMapOffset
			TestCams[FixedCamera].cam_rot = <<-15.06, 0.0,-97.7>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = true
		BREAK
		
		CASE BugGotoPedOffset
			
			Testpeds[0].PedsCoords = << -3.7756, 1.0, 1.1959>>  + gvMapOffset
			Testpeds[0].PedHeading = 0.0
			Testpeds[0].Pedrelgrp = 1
			Testpeds[0].PedModel = S_M_Y_Cop_01
			Testpeds[0].PedsWeapon = WEAPONTYPE_UNARMED
			
		BREAK		
		
	ENDSWITCH	
ENDPROC

//Test Specific Enums
	ENUM PedEnterVehicleFlags
		GIVE_TASK, 
		CHECK_IF_TASK_IS_COMPLETE, 
		SET_PED_A_NEW_POS_START_AGAIN
	ENDENUM
	
	PedEnterVehicleFlags PedEnterVehicleStatus = GIVE_TASK

	PROC INITALISE_TEST_STATE()
		SingleTaskStatus = startSingleTask
		PedEnterVehicleStatus = GIVE_TASK
ENDPROC

PROC RUN_TEST ()
	Vector vNewPedPos, vPedPos
	float heading	
	
		SWITCH int_to_enum (scenarios, gcurrentselection)		// the ENUM corresponds to the values from the XML file 
					//setup scenario
					CASE TestTaskSlideToCoord
							TEST_SEQ = SLIDE_TO_COORD_SEQ (DynamicTargetPos, DynamicTargetHeading)
							PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
							Set_Test_State_To_Default()
					BREAK
					
					CASE TestTaskSlideToCoordPlayAnim
						TEST_SEQ = SLIDE_TO_COORD_AND_PLAY_ANIM_SEQ (DynamicTargetPos, DynamicTargetHeading, "stash",  "misstest_anim"   )
						PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
						Set_Test_State_To_Default()
					BREAK
					
					CASE TestTaskFollowNavmesh
						TEST_SEQ = FOLLOW_NAVMESH_TO_COORD_SEQ (DynamicTargetPos, DynamicTargetHeading   )
						PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
						Set_Test_State_To_Default()
					BREAK
					
					CASE TestTaskOpenVehicleDoor
						IF IS_VEHICLE_DRIVEABLE (TestVehicles[gTestVehicleIndex].Vehicle )
							TEST_SEQ =  TEST_TASK_OPEN_VEHICLE_DOOR (TestVehicles[gTestVehicleIndex].Vehicle)
							PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
							Set_Test_State_To_Default()
						ENDIF
					BREAK
					
					CASE TestTaskPlayAnim
						TEST_SEQ =  TEST_TASK_PLAY_ANIM ("stash"  )
						PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
						Set_Test_State_To_Default()
					BREAK
					
					CASE  TestWalkToCarAnim
	
						TEST_SEQ = SLIDE_TO_COORD_AND_PLAY_ANIM_SEQ (DynamicTargetPos, DynamicTargetHeading, "walk_up_to_car",  "misstest_anim"   )
						PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
						Set_Test_State_To_Default()
					BREAK
					
					CASE TestPlayRestAtCarAnim
						TEST_SEQ = SLIDE_TO_COORD_AND_PLAY_ANIM_SEQ (DynamicTargetPos, DynamicTargetHeading, "rest_on_car",  "misstest_anim"   )
						PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
						Set_Test_State_To_Default()
					BREAK

					CASE TestFollowRoute
						TEST_SEQ = FOLLOW_ROUTE (route)
						PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
						Set_Test_State_To_Default()
					BREAK

					CASE TestFollowNavemeshAdvacnedHeading
						TEST_SEQ = TEST_TASK_FOLLOW_NAVMESH_ADVANCED(DynamicTargetPos, DynamicTargetHeading)
						PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
						Set_Test_State_To_Default()
					BREAK
					
					CASE TestStandStill
						TEST_SEQ = TEST_TASK_STAND_STILL (gtime, route)
						PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
						Set_Test_State_To_Default()
					BREAK
					
					CASE BugTicketSingle
						TEST_SEQ = TEST_TASK_FOLLOW_POINT_ROUTE_SINGLE ()
						PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
						Set_Test_State_To_Default()
					BREAK
					
					CASE BugTicketSeason
						TEST_SEQ = TEST_TASK_FOLLOW_POINT_ROUTE_SEASON ()
						PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
						Set_Test_State_To_Default()
					BREAK
					
					CASE BugGotoPedOffset
						TEST_SEQ = TEST_TASK_GOTO_PED_OFFSET()
						PedPerformsSequence(TestPeds[0].ped, TEST_SEQ)
						Set_Test_State_To_Default()
					BREAK
					
					CASE TestSEINOVE
					CASE TestWINKY
					CASE TestZ75
					CASE TestJZ125
					CASE TestAUTOGYRO
					CASE TestBCHOPPER 
					CASE TestHind
					CASE TestSKIMOBILE
					CASE TestRHINO
					CASE TestBOBSLEIGH
									
						SWITCH PedEnterVehicleStatus
							CASE GIVE_TASK
								PRINTSTRING ("GIVE_TASK")
								PRINTNL()
								
								IF  IS_VEHICLE_DRIVEABLE (TestVehicles[0].Vehicle)
									TEST_SEQ = TEST_TASK_ENTER_VEHICLE (TestVehicles[0].Vehicle, seatindex)
									PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
								ENDIF
								IF Blooptest 
									PedEnterVehicleStatus = CHECK_IF_TASK_IS_COMPLETE
								ELSE
									Set_Test_State_To_Default()
								ENDIF
							BREAK
							
							
							CASE CHECK_IF_TASK_IS_COMPLETE
								PRINTSTRING ("CHECK_IF_TASK_IS_COMPLETE")
								PRINTNL()
							
								IF NOT IS_PED_INJURED (TestPeds[0].ped )
									IF IS_PED_SITTING_IN_ANY_VEHICLE (TestPeds[0].ped )
										SETTIMERA(0)
										PedEnterVehicleStatus = SET_PED_A_NEW_POS_START_AGAIN
									ENDIF
								ENDIF
							BREAK
						
		
							CASE SET_PED_A_NEW_POS_START_AGAIN
								IF (TIMERA() > 1000)
									IF IS_VEHICLE_DRIVEABLE (TestVehicles[0].vehicle )
										vNewPedPos = GetOffsetRoundCar(TestVehicles[0].vehicle)			
										heading = GET_RANDOM_FLOAT_IN_RANGE (0, 360 )
										IF NOT IS_PED_INJURED (TestPeds[0].ped )
											vPedPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TestVehicles[0].vehicle, vNewPedPos)
											GET_GROUND_Z_FOR_3D_COORD (vPedPos, vPedPos.z )
											SET_ENTITY_COORDS(TestPeds[0].ped , vPedPos )
											SET_ENTITY_HEADING( TestPeds[0].ped,heading )
										ENDIF
										PedEnterVehicleStatus = GIVE_TASK
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					
					CASE TestEnableAssistedMovementRoute1
						ASSISTED_MOVEMENT_REQUEST_ROUTE("test_assist1")
					BREAK
					
					CASE TestDisableAssistedMovementRoute1
						ASSISTED_MOVEMENT_REMOVE_ROUTE("test_assist1")
					BREAK
					
					CASE TestEnableAssistedMovementRoute2
						ASSISTED_MOVEMENT_REQUEST_ROUTE("test_ASSist2_narrow")
					BREAK
					
					CASE TestDisableAssistedMovementRoute2
						ASSISTED_MOVEMENT_REMOVE_ROUTE("test_ASSist2_narrow")
					BREAK
					
					CASE DefaultTest
					
					BREAK
					
				ENDSWITCH	
			

ENDPROC


//Purpose: Runs the main scenarios
PROC Run_Test_Scenario ()
	int index = 0
	
	SWITCH TestScenarioAStatus
		//setps up all the data for the sceanrio, this is only called once or if selected in widget
		CASE InitialiseScenarioData
			PlayerStartPos = GET_PLAYER_START_POS()			//sets the players start coords at the default
			INITIALISE_PED_DATA (Testpeds)
			INITIALISE_VEHICLE_DATA (TestVehicles)
			INITIALISE_CAM_DATA(TestCams )
			SETUP_TEST_DATA ()
	
			HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
			TestScenarioAStatus = CreateScenarioEntities
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)		//sets the 
		
		BREAK
		
		// Creates all the scenario data
		CASE CreateScenarioEntities
			
			Set_Gang_Relationships (TRUE)	
		
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
		
			//peds
			FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
				CREATE_PED_ENTITY(Testpeds[index])
				GiveBlipsToPedsGrps (Testpeds[index])
				ALTER_COMBAT_STATS (Testpeds[index] )
				SWAP_PED_WEAPONS (Testpeds[index])
				ADD_RELATIONSHIP_GROUP_TO_STRUCT (Testpeds[index])
				Block_Peds_Temp_Events (Testpeds[index], TRUE)
				SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
			ENDFOR
			
			//TestVehicles
			FOR Index = 0 to MAX_NUMBER_OF_VEHICLES -1
				CREATE_VEHICLE_ENTITY (TestVehicles[index] )
			ENDFOR
		
			//TestCams
			for index = 0 to MAX_NUMBER_OF_CAMERAS -1
				CREATE_CAM_ENTITY (TestCams[index])
			ENDFOR
			
			TestScenarioAStatus = SetScenarioEntities
		BREAK
		
		CASE SetScenarioEntities
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
			
			Start_And_Reset_Test ()
			
			IF gBeginCombatScenario
				TestScenarioAStatus = RunScenario
				gBeginCombatScenario = FALSE
				gRun_debuggig = FALSE
				INITALISE_TEST_STATE()
				TEMP_ACTIVATE_CAMS (TestCams[FixedCamera].cam )
				
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam )
					HELP_TEXT_STATE =  HTF_SCENARIO_RUNNING_FIXED_CAM
				ELSE
					HELP_TEXT_STATE = HTF_STARTED_RUNNING_NO_CAMS
				ENDIF
			ENDIF
		BREAK
		
		//Runs the actual selected scenario
		
		CASE RunScenario
				
			Temp_Run_Scenario_Tracking_Cam (TestPeds, MAX_NUMBER_OF_PEDS, TestCams[TrackingCamera].cam) 
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
				
			Start_And_Reset_Test ()
				
		 	Check_For_Scenario_Reset ()
			
			IF DOES_CAM_EXIST (TestCams[TrackingCamera].cam)
				if IS_CAM_RENDERING (TestCams[TrackingCamera].cam)
					HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_TRACKING_CAM
				ELSE
					HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_FIXED_CAM
				ENDIF
			ENDIF
					
			RUN_TEST ()		//run the main tests 		
			
		BREAK 
				
		CASE CleanupScenario
			
			HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
			Cleanup_Scenario_Entities ()
			
			IF gResetToDefault 	
				Temp_cleanup_scenario_cams ()		//here we are changing scenarios so we need to reset cams
				TestScenarioAStatus = InitialiseScenarioData
				gResetToDefault = FALSE
			ENDIF
		
			IF gResetCombatScenario
				Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)
				gcurrentselection = gSelection 
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
					ACTIVATE_CAM (TestCams[FixedCamera].cam)
				ELSE
					Temp_cleanup_scenario_cams ()	
				ENDIF
				TestScenarioAStatus = CreateScenarioEntities
				gResetCombatScenario = FALSE
			ENDIF
		
		BREAK
		
	ENDSWITCH
ENDPROC


SCRIPT
		
	SET_DEBUG_ACTIVE (TRUE)
	
	gvMapOffset = GET_PLAYER_START_POS()
	
	SETUP_MISSION_XML_MENU (XMLMenu,  KEY_Q)
	
	SETUP_AREA_FOR_MISSION (<<0.0, 0.0, 0.0>>)
	
	//Gets a reference to the player
	Get_The_Player ()
	
	SET_PLAYER_COLISION(scplayer, true)

	//Sets the test widget for this script
	CREATE_TEST_WIDGET ()
	
	//request the test anim bank
//	REQUEST_TEST_ANIM_DICT ("misstest_anim")
	
WHILE TRUE
		// controls the help text hides if xml menu is active
		TEXT_CONTROLLER ()
	
		// Can set all scenario peds invincible from the widget
		Set_Scenario_Peds_Invincible (TestPeds, MAX_NUMBER_OF_PEDS, gsetpedsinvincible )
	
		//User can create a debug cam for setting sceanrios
		Temp_Create_Debug_Cam (TestCams[FixedCamera]) 
		
		//Runs the selected option from the XML menu 
		Run_Selection_From_XML_input ()
		
		//Checks that a valid selection has been input and runs the scenario
		IF (gcurrentselection <> InvalidSelection)
	
			Draw_Debug_Info (  )
			
			//Sets the test scenario into debug mode
			Set_To_Debug ()
			
			if (gRun_debuggig)
				Temp_Debug_Scenario (TestPeds, TestVehicles, Route, TestCams[FixedCamera].cam, S_M_Y_Cop_01)								//Allows the entities in the scneario to be adjusted
				Print_Scenario_Data (TestPeds, TestVehicles, Route, TestCams)
			ENDIF
			
			Run_Test_Scenario ()								
			
			SWITCH_BETWEEN_FIXED_AND_TRACKING (TestCams[FixedCamera].cam)
			
			PRINT_ACTIVE_TEST (Bscenario_running, gRun_debuggig )		
		ENDIF
		
		WAIT (0)
		
		Terminate_test_script ()
		
	
	ENDWHILE

ENDSCRIPT


#ENDIF	// IS_DEBUG_BUILD

