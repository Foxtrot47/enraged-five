// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD
USING "rage_builtins.sch"
//USING "globals.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
//USING "script_player.sch"
//USING "scumminess.sch"
//USING "streamed_scripts.sch"
//USING "respawn_location_data.sch"
USING "Mission_Flow_Initialiser.sch"

// All the registration headers required to specify which global variables need to be saved
//USING "flow_globals_reg.sch"

// Allow the globals to be created
GLOBALS TRUE                                                 


FLOAT fInitialPlayerHeading, groundZ
VECTOR vInitialPlayerCoord, current_coords, closest_safe_position, TESTvInitialPlayerCoord
INT index, random_closest_node
BOOL timeout

VEHICLE_INDEX test_veh[60]
PED_INDEX test_ped[90]
MODEL_NAMES testVehModel[10]
MODEL_NAMES testPedModel[4]
CONST_INT VehModelsNums 10
CONST_INT PedModelsNums 4
FLOAT sc_newcoord[4]

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE: Creates the player ped and places him in the world
/// NOTES:   The 'playercoords.txt' file (used by the artists) will override these player creation coords
PROC Create_The_Player()
	// Don't load the scene in Release mode -//depot/gta4_japanese/script/dev/Demo/coderTest.sc speeds up loading because Player is usually re-positioned
    //  by the first mission script anyway leading to two warps
	
	sc_newcoord[0] = 201.3993
	PRINTLN("Default start x = ", sc_newcoord[0])
	sc_newcoord[1] = -924.1564
	PRINTLN("Default start y = ", sc_newcoord[1])						
	sc_newcoord[2] = 820.0
	PRINTLN("Default start Z = ", sc_newcoord[2])						
	sc_newcoord[3] = 0.0
	PRINTLN("Default start Heading = ", sc_newcoord[3])						

	IF GET_COMMANDLINE_PARAM_EXISTS("sc_CreationStartup")
		sc_newcoord[0] = GET_COMMANDLINE_PARAM_FLOAT_BY_INDEX("sc_CreationStartup", 0)
		PRINTLN("sc_CreationStartupX = ", sc_newcoord[0])
		sc_newcoord[1] = GET_COMMANDLINE_PARAM_FLOAT_BY_INDEX("sc_CreationStartup", 1)
		PRINTLN("sc_CreationStartupY = ", sc_newcoord[1])
		sc_newcoord[2] = GET_COMMANDLINE_PARAM_FLOAT_BY_INDEX("sc_CreationStartup", 2)
		PRINTLN("sc_CreationStartupZ = ", sc_newcoord[2])
		sc_newcoord[3] = GET_COMMANDLINE_PARAM_FLOAT_BY_INDEX("sc_CreationStartup", 3)
		PRINTLN("sc_CreationStartupH = ", sc_newcoord[3])
	ELSE
		PRINTLN("sc_CreationStartup doesn't exist")						
	ENDIF	

	vInitialPlayerCoord = <<sc_newcoord[0], sc_newcoord[1], sc_newcoord[2]>>
	fInitialPlayerHeading = sc_newcoord[3]

	SET_CLOCK_TIME(17,0,0)
	SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
	//stop ambient creation of vehicles
    PRINTSTRING("Stop ambient creation of vehicles")PRINTNL()
	
	SET_NUMBER_OF_PARKED_VEHICLES(0)

	SET_VEHICLE_POPULATION_BUDGET(0)
	SET_GARBAGE_TRUCKS(FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-9999.9, -9999.9, -9999.9>>, <<9999.9, 9999.9, 9999.9>>, FALSE)
	CLEAR_AREA_OF_VEHICLES(vInitialPlayerCoord, 1000.0)

	//stop ambient creation of peds
    PRINTSTRING("Stop ambient creation of peds")PRINTNL()
	SET_PED_POPULATION_BUDGET(0)

	CLEAR_AREA_OF_PEDS(vInitialPlayerCoord, 1000.0)
	
	SET_ENTITY_COORDS(PLAYER_PED_ID(), vInitialPlayerCoord)
	SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
	SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), FALSE)

	LOAD_SCENE(vInitialPlayerCoord)
	SETTIMERA(0)

	WHILE NOT GET_GROUND_Z_FOR_3D_COORD(vInitialPlayerCoord, groundZ)
	AND NOT timeout
		WAIT(0)
		TESTvInitialPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
		PRINTLN("Finding Ground Z for 3d coord", " - players position = ", TESTvInitialPlayerCoord)	
		IF TIMERA() > 20000
			timeout = TRUE
			PRINTLN("No ground Z found")
		ENDIF
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
		SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0, 0)
	
	ENDWHILE
	
	SET_ENTITY_COORDS(PLAYER_PED_ID(), <<vInitialPlayerCoord.x, vInitialPlayerCoord.y, groundZ>>)
	SET_ENTITY_HEADING(PLAYER_PED_ID(), fInitialPlayerHeading)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()					
	vInitialPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
	PRINTLN("Players Start Position = ", vInitialPlayerCoord)	
		
	LOAD_SCENE(vInitialPlayerCoord)
	LOAD_ALL_OBJECTS_NOW()
	
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(0)
	ENDIF

ENDPROC

/// PURPOSE:
///     (stop ambient creation of vehicles, peds, create player in star junction,
///    create 20 vehicles, 20 peds).
///    see bug #
PROC Initialise_Test_Startup_Settings_With_Wait()
    INT i
	
    testVehModel[0] = double
	testVehModel[1] = banshee
    testVehModel[2] = tailgater
    testVehModel[3] = hexer
    testVehModel[4] = bison
    testVehModel[5] = taxi
	testVehModel[6] = packer
    testVehModel[7] = POLICE2
    testVehModel[8] = cavalcade
    testVehModel[9] = dominator
	
	testPedModel[0] = A_M_O_Tramp_01
	testPedModel[1] = A_F_Y_Business_02
	testPedModel[2] = A_M_M_Business_01
	testPedModel[3] = A_F_M_BevHills_01
	
	REQUEST_MODEL(double)
	REQUEST_MODEL(banshee)
	REQUEST_MODEL(tailgater)
	REQUEST_MODEL(hexer)
	REQUEST_MODEL(bison)
	REQUEST_MODEL(taxi)
	REQUEST_MODEL(packer)
	REQUEST_MODEL(POLICE2)
	REQUEST_MODEL(cavalcade)
	REQUEST_MODEL(dominator)
	
	REQUEST_MODEL(A_M_O_Tramp_01)
	REQUEST_MODEL(A_F_Y_Business_02)
	REQUEST_MODEL(A_M_M_Business_01)
	REQUEST_MODEL(A_F_M_BevHills_01)
	
	LOAD_ALL_OBJECTS_NOW()
//	WHILE NOT HAS_MODEL_LOADED(testVehModel)
//	OR NOT HAS_MODEL_LOADED(testPedModel)
//		WAIT(0)
//	ENDWHILE
	
	FLOAT fCurrentCount
	
	//create 60 vehicles
	REPEAT COUNT_OF(test_veh) i
		IF NOT DOES_ENTITY_EXIST(test_veh[i])
			fCurrentCount = (i / TO_FLOAT(COUNT_OF(test_veh))) * 360.0
			
			FLOAT fModCount = (fCurrentCount % 359.0) + fInitialPlayerHeading
			FLOAT fMult = 55.0+(ROUND(fCurrentCount/90) * 6.0) //fMult = 20.0
			
			VECTOR vTest_veh_coord = vInitialPlayerCoord+(<<SIN(fModCount), COS(fModCount), 0.0>>*fMult)
			FLOAT fTest_veh_head = GET_HEADING_FROM_VECTOR_2D(vInitialPlayerCoord.x-vTest_veh_coord.x, vInitialPlayerCoord.y-vTest_veh_coord.y)
			
//			index = GET_RANDOM_INT_IN_RANGE(0, VehModelsNums) //Probably remove. Needs to be more consistent
			index ++
			IF index >= VehModelsNums
				index = 0
			ENDIF
			test_veh[i] = CREATE_VEHICLE(testVehModel[index], <<vTest_veh_coord.x, vTest_veh_coord.y, INVALID_WORLD_Z>>, fTest_veh_head)
			SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(test_veh[i], FALSE)
			SET_ENTITY_HEALTH(test_veh[i], 9999)
		ENDIF
	ENDREPEAT
	
	
	//create 90 peds
	REPEAT COUNT_OF(test_ped) i
		IF NOT DOES_ENTITY_EXIST(test_ped[i])
//			fCurrentCount = (i / TO_FLOAT(COUNT_OF(test_ped))) * 360.0
//				
//			VECTOR vTest_ped_coord = vInitialPlayerCoord+(<<SIN(fCurrentCount), COS(fCurrentCount), 0.0>>*7.5)
//			FLOAT fTest_ped_head = GET_HEADING_FROM_VECTOR_2D(vInitialPlayerCoord.x-vTest_ped_coord.x, vInitialPlayerCoord.y-vTest_ped_coord.y)		

			fCurrentCount = (i / TO_FLOAT(COUNT_OF(test_ped))) * 360.0
			
			FLOAT fModCount = (fCurrentCount % 359.0) + fInitialPlayerHeading
			FLOAT fMult = 22.5+(ROUND(fCurrentCount/90) * 6.0) //fMult = 7.5
			
			VECTOR vTest_ped_coord = vInitialPlayerCoord+(<<SIN(fModCount), COS(fModCount), 0.0>>*fMult)
			FLOAT fTest_ped_head = GET_HEADING_FROM_VECTOR_2D(vInitialPlayerCoord.x-vTest_ped_coord.x, vInitialPlayerCoord.y-vTest_ped_coord.y)
			
//			index = GET_RANDOM_INT_IN_RANGE(0, PedModelsNums)
			index ++
			IF index >= PedModelsNums
				index = 0
			ENDIF
			test_ped[i] = CREATE_PED(PEDTYPE_MISSION, testPedModel[index], <<vTest_ped_coord.x, vTest_ped_coord.y, INVALID_WORLD_Z>>, fTest_ped_head)
			TASK_STAND_STILL(test_ped[i], -1)
			SET_PED_KEEP_TASK(test_ped[i], TRUE)
			SET_ENTITY_HEALTH(test_ped[i], 999)
		ENDIF
	ENDREPEAT
	
	//Give a handful of peds weapons
	IF DOES_ENTITY_EXIST(test_ped[1])
		GIVE_WEAPON_TO_PED(test_ped[1], WEAPONTYPE_CARBINERIFLE, INFINITE_AMMO, TRUE)
	ENDIF
	IF DOES_ENTITY_EXIST(test_ped[2])
		GIVE_WEAPON_TO_PED(test_ped[2], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
	ENDIF
	IF DOES_ENTITY_EXIST(test_ped[3])
		GIVE_WEAPON_TO_PED(test_ped[3], WEAPONTYPE_PUMPSHOTGUN, INFINITE_AMMO, TRUE)
	ENDIF
	
	REPEAT VehModelsNums i
		SET_MODEL_AS_NO_LONGER_NEEDED(testVehModel[i])
	ENDREPEAT
	
	REPEAT PedModelsNums i
		SET_MODEL_AS_NO_LONGER_NEEDED(testPedModel[i])
	ENDREPEAT
	
	// Fade the screen in
	IF NOT IS_SCREEN_FADED_IN()
	AND NOT IS_SCREEN_FADING_IN()
		DO_SCREEN_FADE_IN(0)
	ENDIF
	
//	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)

ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Requests the 'Main Persistent' script, waits for it to stream in, then launches it
PROC Request_And_Launch_Main_Persistent_Script_With_Wait()

    STRING theScriptName = "main_persistent"

    REQUEST_SCRIPT(theScriptName)
    WHILE NOT HAS_SCRIPT_LOADED(theScriptName)
        WAIT(0)
        REQUEST_SCRIPT(theScriptName)
    ENDWHILE
    
    START_NEW_SCRIPT(theScriptName, DEFAULT_STACK_SIZE)
    
ENDPROC



// ===========================================================================================================

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
		 TERMINATE_THIS_THREAD()
	ENDIF
    
    Create_The_Player()
    
    SET_DEBUG_ACTIVE(FALSE) 
    
    // IMPORTANT: THERE SHOULD BE NO 'WAIT' COMMANDS BEFORE HERE
    WAIT (0)
	
	Initialise_Test_Startup_Settings_With_Wait()

	WHILE TRUE
		WAIT(0)	
	
		IF random_closest_node < 200
		
		//Check if entity is colliding with anything and try to resapwn on a safe node
			REPEAT COUNT_OF(test_veh) index
				IF DOES_ENTITY_EXIST(test_veh[index])
					IF IS_VEHICLE_DRIVEABLE(test_veh[index])
						IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(test_veh[index])
						OR IS_ENTITY_IN_WATER(test_veh[index])
						OR IS_ENTITY_UPSIDEDOWN(test_veh[index])
//						OR IS_ENTITY_IN_AIR(test_veh[index])
							current_coords = GET_ENTITY_COORDS(test_veh[index])
							random_closest_node ++
							GET_NTH_CLOSEST_VEHICLE_NODE(current_coords, random_closest_node, closest_safe_position)
							SET_ENTITY_COORDS(test_veh[index], closest_safe_position)
							PRINTLN("Resetting Vehicle #", index)
							WAIT(0)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
	
			//Check if entity is colliding with anything and try to resapwn on a safe node
			REPEAT COUNT_OF(test_ped) index
				IF DOES_ENTITY_EXIST(test_ped[index])
					IF NOT IS_PED_INJURED(test_ped[index])
						IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(test_ped[index])
						OR IS_ENTITY_IN_WATER(test_ped[index])
//						OR IS_ENTITY_IN_AIR(test_ped[index])
							current_coords = GET_ENTITY_COORDS(test_ped[index])
							random_closest_node ++
							GET_NTH_CLOSEST_VEHICLE_NODE(current_coords, random_closest_node, closest_safe_position)
							SET_ENTITY_COORDS(test_ped[index], closest_safe_position)
							PRINTLN("Resetting Ped #", index)
							WAIT(0)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT

		ENDIF
		
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
		SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0, 0)
		
	ENDWHILE
	
	
	// Allow Startup to terminate
    TERMINATE_THIS_THREAD()
    
ENDSCRIPT


#ENDIF  // IS_DEBUG_BUILD

