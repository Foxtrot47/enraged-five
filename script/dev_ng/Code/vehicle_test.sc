// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "script_xml.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "commands_streaming.sch"
USING "commands_graphics.sch"
USING "commands_debug.sch"
USING "test_tools.sch"
USING "commands_vehicle.sch"
USING "commands_camera.sch"
USING "commands_player.sch"
USING "script_DEBUG.sch"
USING "commands_event.sch"
USING "commands_script.sch"
USING "commands_entity.sch"
USING "commands_itemsets.sch"
USING "commands_misc.sch"

//Notes: This is script is to allow easy setup of test scenarios for the code department. It works in conjunction with test_tools.sch
//It  is a data driven system 

//Defaults values when the test is reset to factory the standard values are used

STRING XMLMenu = "Testbed/TestVehicleMenu"

////////////////////////////////////////////////////////////////////////////////
   
// These enums must match with the /build/dev/common/data/script/xml/Testbed/TestVehicleMenu.xml
ENUM Scenarios
	TEST_SET_PED_IN_VEHICLE = 0,
	TEST_TASK_WARP_PED_INTO_VEHICLE = 1,
	TEST_MOVING_ALIGN = 2,
	BUG_LEAVE_ANY_VEHICLE = 3,
	TEST_ENTER_VEHICLE_NORMAL = 4,
	TEST_ENTER_VEHICLE_JACK = 5,
	TEST_ENTER_VEHICLE_FORCED = 6,
	TEST_BRING_VEHICLE_TO_HALT = 7,
	TEST_ENTER_VEHICLE_ON_SIDE = 8,
	TEST_PLANE_HOMING_MISSILES = 9,
	TEST_STREAM_VEHICLE_ANIMS = 10,
	TEST_SET_VELOCITY = 11,
	TEST_IN_VEHICLE_CONTEXT = 12,
	TEST_STOCKADE_WITH_STICKY_GRENADES = 13,
	TEST_DEATH_WHILST_EXITING = 14,
	TEST_COLLISION_WHILE_ENTERING = 15,
	TEST_COLLISION_WHILE_EXITING = 16,
	TEST_DEATH_WHILE_ENTERING = 17,
	TEST_DEATH_WHILE_EXITING = 18,
	TEST_THROUGH_WINDSCREEN = 19,
	TEST_JUMP_OUT = 20,
	TEST_COLLISION_WHILE_JACKING_PED = 21,
	TEST_DEATH_WHILE_JACKING_PED = 22,
	TEST_OVERRIDE_ENTRY_CLIPSET = 23,
	TEST_FLEE_EXIT = 24,
    DefaultTest = 1000
ENDENUM

ENUM VEHICLE_LAYOUTS
    VLAYOUT_BIKE_DIRT = 0,
    VLAYOUT_BIKE_SPORT,
    VLAYOUT_BIKE_CHOPPER,
    VLAYOUT_BIKE_FREEWAY,
	VLAYOUT_BIKE_SCOOTER,
	VLAYOUT_BICYCLE_BMX,
	VLAYOUT_BICYCLE_ROAD,
	VLAYOUT_BICYCLE_MOUNTAIN,
	VLAYOUT_BICYCLE_CRUISER,
	VLAYOUT_VAN,
	VLAYOUT_TRUCK,
	VLAYOUT_STD,
	VLAYOUT_LOW,
	VLAYOUT_CUBAN_800,
	VLAYOUT_DUSTER,
	VLAYOUT_HELI,
	VLAYOUT_HELI_FROGGER,
	VLAYOUT_BUS,
	VLAYOUT_QUAD_BIKE,
	VLAYOUT_TOURBUS
ENDENUM

ENUM WEAPON_ID
    PISTOL = 0,
    RIFLE_AK47,
    PUMPSHOTGUN,
	GRENADELAUNCHER
ENDENUM

ENUM WALL_HEIGHT
	ONE_METRE_HIGH_WALL = 0,
	TWO_METRE_HIGH_WALL,
	THREE_METRE_HIGH_WALL
ENDENUM

ENUM JUMP_HEIGHT
	GROUND_LEVEL = 0,
	SMALL_STEP,
	LOW_WALL,
	HIGH_WALL
ENDENUM

////////////////////////////////////////////////////////////////////////////////

CONST_INT MAX_NUMBER_OF_PEDS			10
CONST_INT MAX_NUMBER_OF_VEHICLES		10
CONST_INT MAX_NUMBER_OF_OBJECTS 		1
CONST_INT MAX_NUMBER_OF_NODES			7
CONST_INT MAX_NUMBER_OF_CAMERAS			2
CONST_INT FixedCamera					0
CONST_INT TrackingCamera				1

peds_struct 	TestPeds[MAX_NUMBER_OF_PEDS]
vehicle_struct	TestVehicles[MAX_NUMBER_OF_VEHICLES]
Object_struct	TestObjects[MAX_NUMBER_OF_OBJECTS]
RouteStruct		Route[MAX_NUMBER_OF_NODES]
StCameraData	TestCams[MAX_NUMBER_OF_CAMERAS]

////////////////////////////////////////////////////////////////////////////////

// Define Widget Variables

FLOAT DYNAMIC_MOVE_BLEND_RATIO = PEDMOVEBLENDRATIO_WALK
FLOAT SlideSpeed = 1.0
FLOAT BlendSpeed = 8.0
BOOL bLoopAnim = FALSE, bExtractbackwardvel = TRUE, bExtractsidevel = TRUE, bholdlastframe = FALSE
FLOAT Ground_z
BOOL REMOVE_VEHICLE_DOOR = FALSE
BOOL WARP_TO_ENTRY_POINT = FALSE
BOOL BIKE_LEFT_SIDE = TRUE
BOOL PREFER_LEFT_ENTRY = FALSE
BOOL PREFER_RIGHT_ENTRY = FALSE
INT TimeBeforeStop = 500
INT PEDS_SEAT_INDEX = ENUM_TO_INT(VS_DRIVER)
INT VEHICLE_TEST_INDEX = ENUM_TO_INT(VLAYOUT_STD)
INT WEAPON_TYPEID = 0
INT SWIPE_TYPE = 0
INT WAIT_BEFORE_ENTER_EXIT_TIME = 2000
INT WAIT_BEFORE_VEHICLE_RAM = 2000
INT WAIT_BEFORE_FIRE_TIME = 1000
INT DEATH_TIME = 1000
FLOAT ForwardSpeed = 30.0
BOOL ENTER_EXIT_TASK_GIVEN = FALSE
BOOL SHOOT_TASK_GIVEN = FALSE
BOOL VEHICLE_RAM_TASK_GIVEN = FALSE
INT WALL_HEIGHT_TYPE = ENUM_TO_INT(ONE_METRE_HIGH_WALL)
INT JUMP_HEIGHT_TYPE = ENUM_TO_INT(GROUND_LEVEL)
FLOAT VEHICLE_SPEED = 12.0
FLOAT VEHICLE_SIDE_OFFSET = 0.0
VECTOR camera_pos
FLOAT CAM_ZOOM = 6
FLOAT CAM_PAN = -128
FLOAT CAM_PITCH = 65
BOOL JACK_FROM_PASSENGER = FALSE
STRING clipsetname = "clipset@rcmpaparazzo1ig_1_ps"
FLOAT fVehicleHaltStoppingDist = 10.5

////////////////////////////////////////////////////////////////////////////////

 // Create Widgets 
  
PROC CREATE_TEST_WIDGET ()
    START_WIDGET_GROUP("Vehicle Test")
        SETUP_WIDGET ()
		
		 	START_WIDGET_GROUP("Tasks")
				START_NEW_WIDGET_COMBO() 
                	ADD_TO_WIDGET_COMBO("VS_ANY_PASSENGER")
               		ADD_TO_WIDGET_COMBO("VS_DRIVER")
                	ADD_TO_WIDGET_COMBO("VS_FRONT_RIGHT")
                 	ADD_TO_WIDGET_COMBO("VS_BACK_LEFT")
					ADD_TO_WIDGET_COMBO("VS_BACK_RIGHT")
					ADD_TO_WIDGET_COMBO("VS_EXTRA_LEFT_1")
					ADD_TO_WIDGET_COMBO("VS_EXTRA_RIGHT_1")
					ADD_TO_WIDGET_COMBO("VS_EXTRA_LEFT_2")
					ADD_TO_WIDGET_COMBO("VS_EXTRA_RIGHT_2")
					ADD_TO_WIDGET_COMBO("VS_EXTRA_LEFT_3")
					ADD_TO_WIDGET_COMBO("VS_EXTRA_RIGHT_3")
            	STOP_WIDGET_COMBO("PEDS_SEAT_INDEX", PEDS_SEAT_INDEX)
				START_NEW_WIDGET_COMBO() 
                	ADD_TO_WIDGET_COMBO("LAYOUT_BIKE_DIRT")
               		ADD_TO_WIDGET_COMBO("LAYOUT_BIKE_SPORT")
                	ADD_TO_WIDGET_COMBO("LAYOUT_BIKE_CHOPPER")
                 	ADD_TO_WIDGET_COMBO("LAYOUT_BIKE_FREEWAY")
					ADD_TO_WIDGET_COMBO("LAYOUT_BIKE_SCOOTER")
					ADD_TO_WIDGET_COMBO("LAYOUT_BICYCLE_BMX")
					ADD_TO_WIDGET_COMBO("LAYOUT_BICYCLE_ROAD")
					ADD_TO_WIDGET_COMBO("LAYOUT_BICYCLE_MOUNTAIN")
					ADD_TO_WIDGET_COMBO("LAYOUT_BICYCLE_CRUISER")
					ADD_TO_WIDGET_COMBO("LAYOUT_VAN")
					ADD_TO_WIDGET_COMBO("LAYOUT_TRUCK")
					ADD_TO_WIDGET_COMBO("LAYOUT_STD")
					ADD_TO_WIDGET_COMBO("LAYOUT_LOW")
					ADD_TO_WIDGET_COMBO("LAYOUT_CUBAN_800")
					ADD_TO_WIDGET_COMBO("LAYOUT_DUSTER")
					ADD_TO_WIDGET_COMBO("LAYOUT_HELI")
					ADD_TO_WIDGET_COMBO("LAYOUT_HELI_FROGGER")
					ADD_TO_WIDGET_COMBO("LAYOUT_BUS")
					ADD_TO_WIDGET_COMBO("LAYOUT_QUAD_BIKE")		
					ADD_TO_WIDGET_COMBO("LAYOUT_TOURBUS")		
            	STOP_WIDGET_COMBO("VEHICLE_TO_TEST", VEHICLE_TEST_INDEX)
				START_NEW_WIDGET_COMBO() 
                	ADD_TO_WIDGET_COMBO("ONE_METRE_HIGH_WALL")
               		ADD_TO_WIDGET_COMBO("TWO_METRE_HIGH_WALL")
                	ADD_TO_WIDGET_COMBO("THREE_METRE_HIGH_WALL")
                 	ADD_TO_WIDGET_COMBO("VS_BACK_LEFT")
            	STOP_WIDGET_COMBO("WALL_HEIGHT_TYPE", WALL_HEIGHT_TYPE)
				START_NEW_WIDGET_COMBO() 
                	ADD_TO_WIDGET_COMBO("GROUND_LEVEL")
               		ADD_TO_WIDGET_COMBO("SMALL_STEP")
                	ADD_TO_WIDGET_COMBO("LOW_WALL")
                 	ADD_TO_WIDGET_COMBO("HIGH_WALL")
            	STOP_WIDGET_COMBO("JUMP_HEIGHT_TYPE", JUMP_HEIGHT_TYPE)	
				ADD_WIDGET_FLOAT_SLIDER ("VEHICLE_SIDE_OFFSET", VEHICLE_SIDE_OFFSET,  -10.0, 10.0, 0.1)
				ADD_WIDGET_BOOL ("JACK_FROM_PASSENGER", JACK_FROM_PASSENGER )
				ADD_WIDGET_BOOL ("REMOVE_VEHICLE_DOOR", REMOVE_VEHICLE_DOOR )
				ADD_WIDGET_BOOL ("WARP_TO_ENTRY_POINT", WARP_TO_ENTRY_POINT )
				ADD_WIDGET_BOOL ("BIKE_LEFT_SIDE", BIKE_LEFT_SIDE )
				ADD_WIDGET_BOOL ("PREFER_LEFT_ENTRY", PREFER_LEFT_ENTRY)
				ADD_WIDGET_BOOL ("PREFER_RIGHT_ENTRY", PREFER_RIGHT_ENTRY)
				ADD_WIDGET_FLOAT_SLIDER ("Forward Speed", ForwardSpeed,  -100.0, 100.0, 0.1)
				ADD_WIDGET_INT_SLIDER ("Time between stop", TimeBeforeStop,  0, 3000, 1)
				ADD_WIDGET_INT_SLIDER ("Time to wait before vehicle ram", WAIT_BEFORE_VEHICLE_RAM,  0, 5000, 1)
				ADD_WIDGET_INT_SLIDER ("Time to wait before enter/exit", WAIT_BEFORE_ENTER_EXIT_TIME,  0, 5000, 1)
				ADD_WIDGET_INT_SLIDER ("Time to wait before firing", WAIT_BEFORE_FIRE_TIME,  0, 5000, 1)	
				ADD_WIDGET_INT_SLIDER ("Vehicle Swipe Type", SWIPE_TYPE,  0, 3, 1)
				ADD_WIDGET_FLOAT_SLIDER ("VEHICLE_SPEED", VEHICLE_SPEED, 0.0, 300.0, 1.0)		
				ADD_WIDGET_FLOAT_SLIDER ("CAM_ZOOM", CAM_ZOOM, -30.0, 30.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER ("CAM_PAN", CAM_PAN, -180.0, 180.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER ("CAM_PITCH", CAM_PITCH, -90.0, 90.0, 1.0)		
				
			STOP_WIDGET_GROUP()
			
            START_WIDGET_GROUP("Navigation")
                ADD_WIDGET_BOOL ("Start scenario", gBeginCombatScenario)
                ADD_WIDGET_BOOL ("Restart scenario", gResetCombatScenario)
                ADD_WIDGET_BOOL ("Reset to default", gResetToDefault)
                ADD_WIDGET_BOOL ("Debug scenario", gRun_debuggig)
                ADD_WIDGET_BOOL ("Print all setup info", gPrintAllIinfo)
                ADD_WIDGET_FLOAT_SLIDER ("Start_Coord.x",gPedcoords.x,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Start_Coord.y",gPedcoords.y,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Start_Coord.z",gPedcoords.z,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Start_Heading",gPedheading,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Target_Coord.x",DynamicTargetPos.x,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Target_Coord.y",DynamicTargetPos.y,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Target_Coord.z",DynamicTargetPos.z,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Target_heading",DynamicTargetHeading,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("GroiundZ",Ground_z,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Slide speed",SlideSpeed,  -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("Move Blend Ratio", DYNAMIC_MOVE_BLEND_RATIO, PEDMOVEBLENDRATIO_STILL, PEDMOVEBLENDRATIO_SPRINT, 0.1 )
                START_WIDGET_GROUP ("Anim")
                    ADD_WIDGET_BOOL ("loop anim", bLoopAnim )
                    ADD_WIDGET_BOOL ("Extract y vel", bExtractbackwardvel )
                    ADD_WIDGET_BOOL ("Extract x vel ", bExtractsidevel )
                    ADD_WIDGET_BOOL ("Hold ladt frame ", bholdlastframe )
                    ADD_WIDGET_FLOAT_SLIDER ("blend",BlendSpeed,  -2000, 2000, 0.5)
                STOP_WIDGET_GROUP()
            STOP_WIDGET_GROUP()
            START_WIDGET_GROUP ("Ped setup")
                ADD_WIDGET_INT_SLIDER ("Ped_index", gTestPedsIndex ,0, 9, 1 )
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Coord.x",gPedcoords.x,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Coord.y",gPedcoords.y,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Coord.z",gPedcoords.z,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Heading",gPedheading,  -2000, 2000, 0.5)
                    START_NEW_WIDGET_COMBO() 
                         ADD_TO_WIDGET_COMBO ("S_M_Y_COP_01")
                        ADD_TO_WIDGET_COMBO ("G_M_Y_DESHENCH_01")
                    STOP_WIDGET_COMBO("Ped", PedIdentifier)
                        ADD_WIDGET_BOOL ("Create ped", gCreatePed)
                        ADD_WIDGET_BOOL ("Delete ped", gdeletePed)
                        ADD_WIDGET_INT_SLIDER ("Set Ped rel grp", gpedrelgrp  ,1, 4, 1 )
                    START_NEW_WIDGET_COMBO() 
                        ADD_TO_WIDGET_COMBO ("WEAPONTYPE_PISTOL")
                        ADD_TO_WIDGET_COMBO ("WEAPONTYPE_RIFLE_AK47")
 						ADD_TO_WIDGET_COMBO ("WEAPONTYPE_PUMPSHOTGUN")
						ADD_TO_WIDGET_COMBO ("WEAPONTYPE_GRENADELAUNCHER")
                    STOP_WIDGET_COMBO("weapon", WEAPON_TYPEID)
                        ADD_WIDGET_BOOL ("Swap current peds weapon", gSwapPedWeapons) 
                        ADD_WIDGET_BOOL ("print ped info", Bprint_ped_info)
                ADD_WIDGET_BOOL ("activate def area",gPedDefAActive)    
            STOP_WIDGET_GROUP ()            
            START_WIDGET_GROUP ("Route Builder")
                ADD_WIDGET_FLOAT_SLIDER ("RoutePoint.x",gRoutePoint.x,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("RoutePoint.y",gRoutePoint.y,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("RoutePoint.z",gRoutePoint.z,  -2000, 2000, 0.25)
                ADD_WIDGET_INT_SLIDER ("Route Node", gRouteNode, 0, 7, 1 )
                ADD_WIDGET_BOOL ("Register Node", BSetNode)
                ADD_WIDGET_BOOL ("copy cam pos", BCopyCamCoords )
            STOP_WIDGET_GROUP ()
			
			START_WIDGET_GROUP("BRING_VEHICLE_TO_HALT")
				ADD_WIDGET_FLOAT_SLIDER("Stopping distance", fVehicleHaltStoppingDist, 0.0, 20.0, 0.1)
			STOP_WIDGET_GROUP()
        STOP_WIDGET_GROUP()
ENDPROC

////////////////////////////////////////////////////////////////////////////////

//Mission flow House keeping


//PUPROSE: REmoves all the scenarios and resets all the relationships
    PROC Cleanup_Scenario_Entities ()
    int index = 0
        for index = 0 to MAX_NUMBER_OF_PEDS -1  
            CLEAN_UP_PED_ENTITIES (TestPeds[index].ped)
        ENDFOR 
        
        for index = 0 to MAX_NUMBER_OF_VEHICLES -1
            CLEAN_UP_VEHICLE_ENTITIES (TestVehicles[index].Vehicle )
        ENDFOR
        
        for index = 0 to MAX_NUMBER_OF_OBJECTS -1
            CLEAN_UP_OBJECT_ENTITIES (TestObjects[index].Object)
        ENDFOR

        Set_Gang_Relationships (FALSE)  
    ENDPROC

PROC Terminate_test_script ()
    if IS_KEYBOARD_KEY_JUST_PRESSED (KEY_S)
        CLEAR_PRINTS ()
        CLEAR_HELP ()
        Cleanup_Scenario_Entities ()
        Temp_cleanup_scenario_cams ()
        SET_PLAYER_COLISION(scplayer, true)
        IF NOT IS_PED_INJURED (scplayer)
                  SET_PED_COORDS_KEEP_VEHICLE(scplayer, GET_PLAYER_START_POS())
        ENDIF
        
        TERMINATE_THIS_THREAD ()
        
    ENDIF
ENDPROC


//END: Mission flow House keeping

//Function: SCENARIO TASKS

//SEQUENCE_INDEX TEST_SEQ

FUNC SEQUENCE_INDEX FOLLOW_NAVMESH_TO_COORD_WALK_SEQ ( VECTOR L_Target_Pos)

        SEQUENCE_INDEX TEST_SEQUENCE
        OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, L_Target_Pos, PEDMOVE_WALK,  -2, 0.25)
        CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)

        RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX FOLLOW_NAVMESH_TO_COORD_RUN_SEQ ( VECTOR L_Target_Pos)

        SEQUENCE_INDEX TEST_SEQUENCE
        OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, L_Target_Pos, PEDMOVE_RUN,  -2, 0.25)
        CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
        
        RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX FOLLOW_NAVMESH_TO_COORD_SPRINT_SEQ ( VECTOR L_Target_Pos)
        SEQUENCE_INDEX TEST_SEQUENCE
        OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, L_Target_Pos, PEDMOVE_SPRINT,  -2, 0.25)
        CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)

        RETURN TEST_SEQUENCE
ENDFUNC

ENUM SCENARIO_STATE_ENUM
	STATE_INIT,
	STATE_1,
	STATE_2,
	STATE_3,
	STATE_4,
	STATE_5
ENDENUM

PROC SET_UP_RELATIONSHIP_GROUPS ()
	INT index = 0
	
	FOR index = 0  to MAX_NUMBER_OF_PEDS -1
		ADD_RELATIONSHIP_GROUP_TO_STRUCT(TestPeds[index])
    ENDFOR
ENDPROC

SCENARIO_STATE_ENUM SCENARIO_STATE = STATE_INIT

////////////////////////////////////////////////////////////////////////////////

PROC SETUP_TEST_DATA ()

	MODEL_NAMES testVehicleName
	
	SWITCH int_to_enum (VEHICLE_LAYOUTS, VEHICLE_TEST_INDEX)
	
		CASE VLAYOUT_BIKE_DIRT 			testVehicleName = SANCHEZ 		BREAK
   		CASE VLAYOUT_BIKE_SPORT			testVehicleName = VADER 		BREAK 
    	CASE VLAYOUT_BIKE_CHOPPER		testVehicleName = DAEMON 		BREAK 
    	CASE VLAYOUT_BIKE_FREEWAY		testVehicleName = POLICEB		BREAK
		CASE VLAYOUT_BIKE_SCOOTER		testVehicleName = FAGGIO2		BREAK
		CASE VLAYOUT_BICYCLE_BMX			testVehicleName = BMX 			BREAK		
		CASE VLAYOUT_BICYCLE_CRUISER		testVehicleName = CRUISER 		BREAK		
		CASE VLAYOUT_BICYCLE_ROAD		testVehicleName = TRIBIKE 		BREAK		
		CASE VLAYOUT_BICYCLE_MOUNTAIN	testVehicleName = SCORCHER 		BREAK		
		CASE VLAYOUT_VAN					testVehicleName = BURRITO 		BREAK		
		CASE VLAYOUT_TRUCK				testVehicleName = PHANTOM 		BREAK		
		CASE VLAYOUT_STD					testVehicleName = TAILGATER		BREAK		
		CASE VLAYOUT_LOW					testVehicleName = SENTINEL2		BREAK	
		CASE VLAYOUT_CUBAN_800			testVehicleName = CUBAN800		BREAK	
		CASE VLAYOUT_DUSTER				testVehicleName = DUSTER		BREAK
		CASE VLAYOUT_HELI				testVehicleName = POLMAV		BREAK	
		CASE VLAYOUT_HELI_FROGGER		testVehicleName = FROGGER		BREAK	
		CASE VLAYOUT_BUS					testVehicleName = BUS			BREAK	
		CASE VLAYOUT_QUAD_BIKE			testVehicleName = BLAZER		BREAK	
		CASE VLAYOUT_TOURBUS				testVehicleName = TOURBUS		BREAK
			
	ENDSWITCH
	
    SWITCH int_to_enum (scenarios, gcurrentselection)
	
		CASE TEST_SET_PED_IN_VEHICLE	
		CASE TEST_TASK_WARP_PED_INTO_VEHICLE
		
			TestPeds[0].PedsCoords = <<-4.03, 79.15, 6.35>>
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].PedModel = S_M_M_GAFFER_01
            
            TestVehicles[0].VehicleCoords = <<-0.95, 81.88, 6.35>> 
            TestVehicles[0].VehicleHeading = 90.0001
            TestVehicles[0].Vehiclemodel = testVehicleName       
            
            TestCams[FixedCamera].cam_pos = <<-1.228716,75.143669,10.048866>>
            TestCams[FixedCamera].cam_rot = <<-30.870243,-0.000000,0.979704>>
            TestCams[FixedCamera].bActivateCam = TRUE   
			
		BREAK
		
		CASE TEST_MOVING_ALIGN
				
			TestPeds[0].PedsCoords = <<-4.11, 72.40, 6.35>>
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].PedModel = S_M_M_GAFFER_01
			
		    TestVehicles[0].VehicleCoords = <<-9.39, 72.62, 6.35>> 
            TestVehicles[0].VehicleHeading = 90.0001
            TestVehicles[0].Vehiclemodel = testVehicleName       
		
		BREAK
		
		CASE BUG_LEAVE_ANY_VEHICLE
			TestPeds[0].PedsCoords = <<-4.11, 72.40, 6.35>>
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].PedModel = S_M_M_GAFFER_01
		
			TestPeds[1].PedsCoords = <<-7.11, 72.40, 6.35>>
            TestPeds[1].PedHeading = 359.6439
            TestPeds[1].PedModel = S_M_M_GAFFER_01
			
			TestPeds[2].PedsCoords = <<-11.11, 72.40, 6.35>>
            TestPeds[2].PedHeading = 359.6439
            TestPeds[2].PedModel = S_M_M_GAFFER_01
			
			TestPeds[3].PedsCoords = <<-1.11, 72.40, 6.35>>
            TestPeds[3].PedHeading = 359.6439
            TestPeds[3].PedModel = S_M_M_GAFFER_01
			
		    TestVehicles[0].VehicleCoords = <<-9.39, 72.62, 6.35>> 
            TestVehicles[0].VehicleHeading = 90.0001
            TestVehicles[0].Vehiclemodel = BUS       
		BREAK
		
		CASE TEST_ENTER_VEHICLE_NORMAL
		CASE TEST_ENTER_VEHICLE_JACK
		CASE TEST_ENTER_VEHICLE_FORCED
		CASE TEST_ENTER_VEHICLE_ON_SIDE
		CASE TEST_DEATH_WHILST_EXITING
		
			TestPeds[0].PedsCoords = <<-35.77, 90.64, 6.35>>
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].PedModel = PLAYER_ZERO
		
			TestPeds[1].PedsCoords = <<-32.89, 90.66, 6.35>>
            TestPeds[1].PedHeading = 359.6439
            TestPeds[1].PedModel = S_M_Y_ROBBER_01
			TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
		
			TestVehicles[0].VehicleCoords = <<-35.90, 94.50, 6.35>> 
            TestVehicles[0].VehicleHeading = 90.0001
            TestVehicles[0].Vehiclemodel = testVehicleName      
			
			TestCams[FixedCamera].cam_pos = <<-35.96, 90.58, 10.84>>
            TestCams[FixedCamera].cam_rot = <<-48.44, 0, -2.81>>
            TestCams[FixedCamera].bActivateCam = TRUE   
		BREAK
		
		CASE TEST_BRING_VEHICLE_TO_HALT
            TestVehicles[0].VehicleCoords = <<-0.95, 81.88, 6.35>> 
            TestVehicles[0].VehicleHeading = 90.0001
            TestVehicles[0].Vehiclemodel = TAILGATER
		BREAK
		
		CASE TEST_PLANE_HOMING_MISSILES
			
            TestVehicles[0].VehicleCoords = <<642.61, 38.08, 7.36>> 
            TestVehicles[0].VehicleHeading = 60.0001
            TestVehicles[0].Vehiclemodel = LAZER      
			
			TestVehicles[1].VehicleCoords = <<515.54, 35.07, 7.36>> 
            TestVehicles[1].VehicleHeading = 90.0001
            TestVehicles[1].Vehiclemodel = testVehicleName       
			
			TestVehicles[2].VehicleCoords = <<463.48, 31.66, 7.36>> 
            TestVehicles[2].VehicleHeading = 60.0001
            TestVehicles[2].Vehiclemodel = testVehicleName       
			
			TestVehicles[3].VehicleCoords = <<373.67, 19.61, 7.36>> 
            TestVehicles[3].VehicleHeading = 30.0001
            TestVehicles[3].Vehiclemodel = testVehicleName       
			
			TestVehicles[4].VehicleCoords = <<305.03, 38.95, 7.36>> 
            TestVehicles[4].VehicleHeading = 15.0001
            TestVehicles[4].Vehiclemodel = testVehicleName       
			
			TestVehicles[5].VehicleCoords = <<218.13, 31.83, 7.36>> 
            TestVehicles[5].VehicleHeading = 0.0001
            TestVehicles[5].Vehiclemodel = testVehicleName       
			
			TestVehicles[6].VehicleCoords = <<121.56, 26.35, 7.36>> 
            TestVehicles[6].VehicleHeading = 90.0001
            TestVehicles[6].Vehiclemodel = testVehicleName       
            
		BREAK
		
		CASE TEST_STREAM_VEHICLE_ANIMS
		BREAK
		
		CASE TEST_SET_VELOCITY
		
			TestPeds[0].PedsCoords = <<-35.77, 90.64, 6.35>>
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].PedModel = S_M_Y_ARMYMECH_01
		
			TestVehicles[0].VehicleCoords = <<-35.90, 94.50, 6.35>> 
            TestVehicles[0].VehicleHeading = 0.0001
            TestVehicles[0].Vehiclemodel = TAILGATER      
			
			TestCams[FixedCamera].cam_pos = <<-35.96, 90.58, 10.84>>
            TestCams[FixedCamera].cam_rot = <<-48.44, 0, -2.81>>
            TestCams[FixedCamera].bActivateCam = TRUE   
			
		BREAK
		
		CASE TEST_IN_VEHICLE_CONTEXT
		
			TestPeds[0].PedsCoords = <<-35.77, 90.64, 6.35>>
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].PedModel = S_M_Y_ARMYMECH_01
		
			TestVehicles[0].VehicleCoords = <<-35.90, 94.50, 6.35>> 
            TestVehicles[0].VehicleHeading = 0.0001
            TestVehicles[0].Vehiclemodel = BJXL      
			
			TestCams[FixedCamera].cam_pos = <<-35.96, 90.58, 10.84>>
            TestCams[FixedCamera].cam_rot = <<-48.44, 0, -2.81>>
            TestCams[FixedCamera].bActivateCam = TRUE  
			
		BREAK
		
		CASE TEST_STOCKADE_WITH_STICKY_GRENADES
			
			TestVehicles[0].VehicleCoords = <<-5.1, 81.8, 7.4>> 
            TestVehicles[0].VehicleHeading = 0.0001
            TestVehicles[0].Vehiclemodel = STOCKADE3
			
		BREAK
		
		CASE TEST_COLLISION_WHILE_ENTERING
		CASE TEST_COLLISION_WHILE_EXITING
		
			TestPeds[0].PedsCoords = <<-59.55467, 142.87741, 6.35158>>
            TestPeds[0].PedHeading = -1.58
            TestPeds[0].PedModel = PLAYER_ONE		
			
			TestPeds[1].PedsCoords = <<-46.60886, 143.81769, 6.35158>>
            TestPeds[1].PedHeading = 359.6439
            TestPeds[1].PedModel = PLAYER_TWO			
		
		 	TestVehicles[0].Vehiclemodel = TAILGATER      
			TestVehicles[0].VehicleCoords = <<-60.88103, 143.33746, 6.35158>> 
           	TestVehicles[0].VehicleHeading = 0.0

		 	TestVehicles[1].Vehiclemodel = testVehicleName      	
			TestVehicles[1].VehicleCoords = <<-49.16578, 144.24376, 6.35158>> 
           	TestVehicles[1].VehicleHeading = 90.0

			TestCams[FixedCamera].cam_pos = <<-59.807808,135.568634,11.596110>>
            TestCams[FixedCamera].cam_rot = <<-34.370911,0.000000,2.999246>>
            TestCams[FixedCamera].bActivateCam = TRUE  
			
		BREAK
		
		CASE TEST_DEATH_WHILE_ENTERING
		CASE TEST_DEATH_WHILE_EXITING
		
			TestPeds[0].PedsCoords = <<-58.72038, 142.80356, 6.35158>>
            TestPeds[0].PedHeading = -1.58
            TestPeds[0].PedModel = PLAYER_ONE		
			
			TestPeds[1].PedsCoords = <<-56.30446, 143.88820, 6.35158>>
            TestPeds[1].PedHeading = 92.18
            TestPeds[1].PedModel = S_M_Y_ARMYMECH_01	

			SWITCH int_to_enum (WEAPON_ID, WEAPON_TYPEID)
				CASE PISTOL 
					TestPeds[1].PedsWeapon = WEAPONTYPE_PISTOL
				BREAK
				CASE RIFLE_AK47 
					TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
				BREAK
				CASE PUMPSHOTGUN 
					TestPeds[1].PedsWeapon = WEAPONTYPE_PUMPSHOTGUN
				BREAK
				CASE GRENADELAUNCHER
					TestPeds[1].PedsWeapon = WEAPONTYPE_GRENADELAUNCHER
				BREAK
			ENDSWITCH
		
		 	TestVehicles[0].Vehiclemodel = testVehicleName      
			TestVehicles[0].VehicleCoords = <<-60.88103, 143.33746, 6.35158>> 
           	TestVehicles[0].VehicleHeading = 0.0

			TestCams[FixedCamera].cam_pos = <<-60.539310,147.936768,7.679381>>
            TestCams[FixedCamera].cam_rot = <<-7.064416,0.000000,-175.209595>>
            TestCams[FixedCamera].bActivateCam = TRUE  
		BREAK
		
		CASE TEST_THROUGH_WINDSCREEN
		
			TestPeds[0].PedsCoords = <<41.54198, 192.12083, 6.35078>>
            TestPeds[0].PedHeading = 90.0
            TestPeds[0].PedModel = PLAYER_TWO	
		
			TestVehicles[0].Vehiclemodel = testVehicleName      
			TestVehicles[0].VehicleCoords = <<39.05972, 188.74785, 6.35078>> 
           	TestVehicles[0].VehicleHeading = 0.0
		
			TestCams[FixedCamera].cam_pos = <<32.648914,201.675476,9.892513>>
            TestCams[FixedCamera].cam_rot = <<-24.138597,-0.000000,-98.526772>>
            TestCams[FixedCamera].bActivateCam = TRUE  
			
		BREAK
		
		CASE TEST_JUMP_OUT
		
			TestPeds[0].PedsCoords = <<-62.28864, 68.22392, 6.35084>>
            TestPeds[0].PedHeading = 90.0
            TestPeds[0].PedModel = PLAYER_TWO	
		
			TestVehicles[0].Vehiclemodel = testVehicleName      
			TestVehicles[0].VehicleCoords = <<-63.43328, 68.22320, 6.35084>> 
           	TestVehicles[0].VehicleHeading = 0.0
		
			TestCams[FixedCamera].cam_pos = <<-63.776264,73.140923,9.245447>>
            TestCams[FixedCamera].cam_rot = <<-11.765430,-0.000000,-6.115568>>
            TestCams[FixedCamera].bActivateCam = TRUE  
			
		BREAK
		
		CASE TEST_COLLISION_WHILE_JACKING_PED
			
			TestPeds[0].PedsCoords = <<-52.66739, 145.89999, 6.35158>>
            TestPeds[0].PedHeading = -1.58
            TestPeds[0].PedModel = PLAYER_ZERO
			
			TestPeds[1].PedsCoords = <<-59.55467, 142.87741, 6.35158>>
            TestPeds[1].PedHeading = -1.58
            TestPeds[1].PedModel = PLAYER_ONE		
			
			TestPeds[2].PedsCoords = <<-46.60886, 143.81769, 6.35158>>
            TestPeds[2].PedHeading = 359.6439
            TestPeds[2].PedModel = PLAYER_TWO			
		
		 	TestVehicles[0].Vehiclemodel = TAILGATER      
			TestVehicles[0].VehicleCoords = <<-60.88103, 143.33746, 6.35158>> 
           	TestVehicles[0].VehicleHeading = 180.0

		 	TestVehicles[1].Vehiclemodel = testVehicleName      	
			TestVehicles[1].VehicleCoords = <<-49.16578, 144.24376, 6.35158>> 
           	TestVehicles[1].VehicleHeading = 90.0

			TestCams[FixedCamera].cam_pos = <<-59.807808,135.568634,11.596110>>
            TestCams[FixedCamera].cam_rot = <<-34.370911,0.000000,2.999246>>
            TestCams[FixedCamera].bActivateCam = TRUE  
			
		BREAK
		
		CASE TEST_DEATH_WHILE_JACKING_PED
			
			TestPeds[0].PedsCoords = <<-52.66739, 145.89999, 6.35158>>
            TestPeds[0].PedHeading = -1.58
            TestPeds[0].PedModel = PLAYER_ZERO
			
			TestPeds[1].PedsCoords = <<-59.55467, 142.87741, 6.35158>>
            TestPeds[1].PedHeading = -1.58
            TestPeds[1].PedModel = PLAYER_ONE		
			
			TestPeds[2].PedsCoords = <<-56.87870, 143.20117, 6.35158>>
            TestPeds[2].PedHeading = 359.6439
            TestPeds[2].PedModel = S_M_Y_ARMYMECH_01			
		
			SWITCH int_to_enum (WEAPON_ID, WEAPON_TYPEID)
				CASE PISTOL 
					TestPeds[2].PedsWeapon = WEAPONTYPE_PISTOL
				BREAK
				CASE RIFLE_AK47 
					TestPeds[2].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
				BREAK
				CASE PUMPSHOTGUN 
					TestPeds[2].PedsWeapon = WEAPONTYPE_PUMPSHOTGUN
				BREAK
				CASE GRENADELAUNCHER
					TestPeds[2].PedsWeapon = WEAPONTYPE_GRENADELAUNCHER
				BREAK
			ENDSWITCH
			
		 	TestVehicles[0].Vehiclemodel = TAILGATER      
			TestVehicles[0].VehicleCoords = <<-60.88103, 143.33746, 6.35158>> 
           	TestVehicles[0].VehicleHeading = 180.0

		 	TestVehicles[1].Vehiclemodel = testVehicleName      	
			TestVehicles[1].VehicleCoords = <<-49.16578, 144.24376, 6.35158>> 
           	TestVehicles[1].VehicleHeading = 90.0

			TestCams[FixedCamera].cam_pos = <<-59.807808,135.568634,11.596110>>
            TestCams[FixedCamera].cam_rot = <<-34.370911,0.000000,2.999246>>
            TestCams[FixedCamera].bActivateCam = TRUE  
			
		BREAK
		
		CASE TEST_OVERRIDE_ENTRY_CLIPSET
		
			TestPeds[0].PedsCoords = <<-58.72038, 142.80356, 6.35158>>
            TestPeds[0].PedHeading = -1.58
            TestPeds[0].PedModel = IG_BEVERLY		
				
		 	TestVehicles[0].Vehiclemodel = VADER      
			TestVehicles[0].VehicleCoords = <<-60.88103, 143.33746, 6.35158>> 
           	TestVehicles[0].VehicleHeading = 0.0

			TestCams[FixedCamera].cam_pos = <<-60.539310,147.936768,7.679381>>
            TestCams[FixedCamera].cam_rot = <<-7.064416,0.000000,-175.209595>>
            TestCams[FixedCamera].bActivateCam = TRUE  
		BREAK

		CASE TEST_FLEE_EXIT
		
			TestPeds[0].PedsCoords = <<-1.82127, 80.65820, 6.35078>>
            TestPeds[0].PedHeading = -1.58
            TestPeds[0].PedModel = A_F_Y_BEACH_01
			
			TestPeds[1].PedsCoords = <<-2.36231, 77.52570, 6.35078>>
            TestPeds[1].PedHeading = 19.43
            TestPeds[1].PedModel = PLAYER_TWO
		
			TestVehicles[0].Vehiclemodel = testVehicleName      	
			TestVehicles[0].VehicleCoords = <<-6.25811, 83.23129, 6.35078>> 
           	TestVehicles[0].VehicleHeading = 180.0
			
			TestCams[FixedCamera].cam_pos = <<-6.418138,86.555687,10.362010>>
            TestCams[FixedCamera].cam_rot = <<-44.229584,-0.000001,-151.038116>>
            TestCams[FixedCamera].bActivateCam = TRUE  
		
		BREAK
                
    ENDSWITCH   
ENDPROC

////////////////////////////////////////////////////////////////////////////////
///    
PROC RUN_TEST()

	VECTOR vBlipPos
    //INT index = 0
		VECTOR vRotationEulers
		IF BIKE_LEFT_SIDE
			vRotationEulers = <<0.0, 90.0, 0.0>>
		ELSE
			vRotationEulers = <<0.0, -90.0, 0.0>>
		ENDIF
			
		ENTER_EXIT_VEHICLE_FLAGS ENTER_EXIT_FLAGS
		
		IF WARP_TO_ENTRY_POINT
			ENTER_EXIT_FLAGS |= ECF_WARP_ENTRY_POINT
		ENDIF
	   
        SWITCH int_to_enum (scenarios, gcurrentselection)       // the ENUM corresponds to the values from the XML file 

			CASE TEST_SET_PED_IN_VEHICLE
			
				IS_ENTRY_POINT_FOR_SEAT_CLEAR(TestPeds[0].Ped, TestVehicles[0].Vehicle, VS_DRIVER)
							
				SWITCH SCENARIO_STATE
				
					CASE STATE_INIT
						
						//INT SEAT
						//SEAT = PEDS_SEAT_INDEX - 2
						//IF SEAT >= ENUM_TO_INT(VS_ANY_PASSENGER) AND SEAT <= ENUM_TO_INT(VS_EXTRA_RIGHT_3)
						//	SET_PED_INTO_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, INT_TO_ENUM(VEHICLE_SEAT, SEAT))
						//ENDIF
                    	
						SET_PED_INTO_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, VS_ANY_PASSENGER)
						
						SCENARIO_STATE = STATE_1
					BREAK
					
				ENDSWITCH	
				
			BREAK
	
			CASE TEST_TASK_WARP_PED_INTO_VEHICLE
							
				SWITCH SCENARIO_STATE
				
					CASE STATE_INIT
						
						TASK_WARP_PED_INTO_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, VS_ANY_PASSENGER)
						
						SCENARIO_STATE = STATE_1
					BREAK
					
				ENDSWITCH	
				
			BREAK
			
			CASE TEST_MOVING_ALIGN
			
				SWITCH SCENARIO_STATE
				
					CASE STATE_INIT
						
						IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped)
							TASK_WARP_PED_INTO_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, VS_DRIVER)
							TASK_VEHICLE_MISSION_COORS_TARGET(TestPeds[0].Ped, TestVehicles[0].Vehicle, <<-4.67, 68.52, 6.35>>, MISSION_GOTO, 5.0, DRIVINGMODE_PLOUGHTHROUGH, 0.1, 5.0)
						ENDIF
						SCENARIO_STATE = STATE_1
					BREAK
					
				ENDSWITCH	
			BREAK
			
			CASE BUG_LEAVE_ANY_VEHICLE
			
					INT Index
					
					SWITCH SCENARIO_STATE
				
					CASE STATE_INIT
										
						IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
							FOR Index = 0  to 3
								IF NOT IS_ENTITY_DEAD(TestPeds[Index].Ped)
									INT SEAT
									SEAT = Index-1
					       			TASK_WARP_PED_INTO_VEHICLE(TestPeds[Index].Ped, TestVehicles[0].Vehicle, INT_TO_ENUM(VEHICLE_SEAT, SEAT))
								ENDIF
		  					ENDFOR	
						ENDIF
						
						SETTIMERA(0)
						SCENARIO_STATE = STATE_1
					BREAK
					
					CASE STATE_1
						IF TIMERA() > 1000
							
							FOR Index = 0  to 3
								IF NOT IS_ENTITY_DEAD(TestPeds[Index].Ped)
				       				TASK_LEAVE_ANY_VEHICLE(TestPeds[Index].Ped)
								ENDIF
		  					ENDFOR
							
							SCENARIO_STATE = STATE_2
						ENDIF
					BREAK
					
				ENDSWITCH	
			BREAK	
			
			CASE TEST_ENTER_VEHICLE_NORMAL
			
				SWITCH SCENARIO_STATE
				
					CASE STATE_INIT
					
						IF REMOVE_VEHICLE_DOOR AND NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
							SET_VEHICLE_DOOR_BROKEN(TestVehicles[0].Vehicle, SC_DOOR_FRONT_LEFT, TRUE)
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD( TestVehicles[0].Vehicle)
						
							TASK_ENTER_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, DYNAMIC_MOVE_BLEND_RATIO, ENTER_EXIT_FLAGS)
						ENDIF
						SCENARIO_STATE = STATE_1
					BREAK
					
				ENDSWITCH	
			BREAK
			
			CASE TEST_ENTER_VEHICLE_JACK
				SWITCH SCENARIO_STATE
				
					CASE STATE_INIT
					
						IF REMOVE_VEHICLE_DOOR AND NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
							SET_VEHICLE_DOOR_BROKEN(TestVehicles[0].Vehicle, SC_DOOR_FRONT_LEFT, TRUE)
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(TestPeds[1].Ped) AND NOT IS_ENTITY_DEAD( TestVehicles[0].Vehicle)
							TASK_ENTER_VEHICLE(TestPeds[1].Ped, TestVehicles[0].Vehicle, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_WALK, ECF_WARP_PED)
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD( TestVehicles[0].Vehicle)
							ENTER_EXIT_FLAGS |= ECF_JACK_ANYONE
							TASK_ENTER_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, DYNAMIC_MOVE_BLEND_RATIO, ENTER_EXIT_FLAGS)
						ENDIF
						SCENARIO_STATE = STATE_1
					BREAK
					
				ENDSWITCH	
			BREAK
			
			CASE TEST_ENTER_VEHICLE_FORCED
				SWITCH SCENARIO_STATE			
					CASE STATE_INIT
					
						IF REMOVE_VEHICLE_DOOR AND NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
							SET_VEHICLE_DOOR_BROKEN(TestVehicles[0].Vehicle, SC_DOOR_FRONT_LEFT, TRUE)
						ENDIF
					
						IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
							SET_VEHICLE_DOORS_LOCKED(TestVehicles[0].Vehicle, VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD( TestVehicles[0].Vehicle)
							TASK_ENTER_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, DYNAMIC_MOVE_BLEND_RATIO, ECF_JACK_ANYONE)
						ENDIF
						
						SCENARIO_STATE = STATE_1
					BREAK
				ENDSWITCH
			BREAK
			
			CASE TEST_BRING_VEHICLE_TO_HALT
				SWITCH SCENARIO_STATE
					CASE STATE_INIT
						vBlipPos = GET_PLAYER_START_POS()
						ADD_BLIP_FOR_COORD(vBlipPos)
						//DRAW_CHECKPOINT_WITH_ALPHA(vBlipPos, 2.0, 255, 0, 0, 80)
					
						SCENARIO_STATE = STATE_1
					BREAK
					
					CASE STATE_1
						vBlipPos = GET_PLAYER_START_POS()
						RENDER_FAKE_PICKUP_GLOW(vBlipPos, 2)
						//DRAW_CHECKPOINT_WITH_ALPHA(vBlipPos, 2.0, 255, 255, 0, 80)
						IF NOT IS_ENTITY_DEAD(scplayer)
							IF IS_PED_IN_ANY_VEHICLE(scplayer)
								TestVehicles[0].Vehicle = GET_VEHICLE_PED_IS_IN(scplayer)
								IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
									IF(GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(TestVehicles[0].Vehicle, FALSE), vBlipPos, FALSE) < 2.0)
										// Bring car to a complete stop in 3.0m and hold it stationary for 3s.
										BRING_VEHICLE_TO_HALT(TestVehicles[0].Vehicle, fVehicleHaltStoppingDist, 1)
									
										// Once we've been caught in the trap, allow the player to get his car free.
										SCENARIO_STATE = STATE_2
									ENDIF
								ENDIF
							
							ENDIF
						ENDIF
					BREAK
					
					CASE STATE_2
						vBlipPos = GET_PLAYER_START_POS()
						//DRAW_CHECKPOINT_WITH_ALPHA(vBlipPos, 2.0, 0, 255, 0, 80)
						// Once the player gets clear of the blip again, re-enable the trap!
						IF(GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(TestVehicles[0].Vehicle, FALSE), vBlipPos, FALSE) > 3.0)
							SCENARIO_STATE = STATE_1
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE TEST_ENTER_VEHICLE_ON_SIDE
				IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD( TestVehicles[0].Vehicle)
					SWITCH SCENARIO_STATE			
						CASE STATE_INIT
							SET_ENTITY_ROTATION(TestVehicles[0].Vehicle, vRotationEulers, EULER_XYZ)
							IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD( TestVehicles[0].Vehicle)
								TASK_ENTER_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, DYNAMIC_MOVE_BLEND_RATIO, ECF_JACK_ANYONE)
							ENDIF
							SCENARIO_STATE = STATE_1
						BREAK
					ENDSWITCH
				ENDIF
			BREAK
			
			CASE TEST_PLANE_HOMING_MISSILES
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD( TestVehicles[0].Vehicle)
					SWITCH SCENARIO_STATE			
						CASE STATE_INIT
				
							// Warp player into driver seat of first vehicle
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), TestVehicles[0].Vehicle, VS_DRIVER)					
							SCENARIO_STATE = STATE_1
						BREAK
					ENDSWITCH
				ENDIF
			BREAK
			
			CASE TEST_STREAM_VEHICLE_ANIMS
				SWITCH SCENARIO_STATE			
						CASE STATE_INIT
				
							REQUEST_VEHICLE_ASSET(PHANTOM)
							REQUEST_VEHICLE_ASSET(SANCHEZ)
							
							SCENARIO_STATE = STATE_1
							
						BREAK
						CASE STATE_1

							IF HAS_VEHICLE_ASSET_LOADED(PHANTOM)
							AND HAS_VEHICLE_ASSET_LOADED(SANCHEZ)
								SCENARIO_STATE = STATE_2
								SETTIMERA(0)
								PRINTLN("GOT_HERE")
							ENDIF
						BREAK
						CASE STATE_2
							IF TIMERA() > 2000
								REMOVE_VEHICLE_ASSET(PHANTOM)
								REMOVE_VEHICLE_ASSET(SANCHEZ)
								PRINTLN("GOT_HERE_2")
								
								SETTIMERA(0)
								REQUEST_VEHICLE_ASSET(BURRITO)
								REQUEST_VEHICLE_ASSET(RHINO)
								SCENARIO_STATE = STATE_3
							ENDIF					
						BREAK
						CASE STATE_3
							IF TIMERA() > 2000
								REMOVE_VEHICLE_ASSET(BURRITO)
								REMOVE_VEHICLE_ASSET(RHINO)
								PRINTLN("GOT_HERE_3")
								SCENARIO_STATE = STATE_4
							ENDIF					
						BREAK
				ENDSWITCH
			BREAK
			
			CASE TEST_SET_VELOCITY
				SWITCH SCENARIO_STATE			
						CASE STATE_INIT	
							IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
								SET_PED_CONFIG_FLAG(TestPeds[0].Ped, PCF_WillFlyThroughWindscreen, TRUE)
								SET_PED_INTO_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle)
								SCENARIO_STATE = STATE_1
								SETTIMERA(0)
								PRINTLN("GOT_HERE")
							ENDIF
						BREAK
						CASE STATE_1
							IF TIMERA() > 500	
								IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
									PRINTLN("GOT_HERE_2")
									SET_VEHICLE_FORWARD_SPEED(TestVehicles[0].Vehicle, ForwardSpeed)
									SETTIMERA(0)
								ENDIF
								SCENARIO_STATE = STATE_2
							ENDIF		
						BREAK
						CASE STATE_2
							IF TIMERA() > TimeBeforeStop	
								IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
									PRINTLN("GOT_HERE_3")
									SET_VEHICLE_FORWARD_SPEED(TestVehicles[0].Vehicle, 0.0)
								ENDIF	
								SETTIMERA(0)
								SCENARIO_STATE = STATE_3
							ENDIF	
						BREAK
				ENDSWITCH
			BREAK
			
			CASE TEST_IN_VEHICLE_CONTEXT
				SWITCH SCENARIO_STATE			
						CASE STATE_INIT	
							IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
								SET_PED_INTO_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle)
								SCENARIO_STATE = STATE_1
								PRINTLN("GOT_HERE")
							ENDIF
						BREAK
						CASE STATE_1
							IF NOT HAS_CLIP_SET_LOADED("clipset@missarmenian3@franklin_driving")
								REQUEST_CLIP_SET("clipset@missarmenian3@franklin_driving")
							ELSE
								PRINTLN("GOT_HERE_2")
								SCENARIO_STATE = STATE_2
							ENDIF		
						BREAK
						CASE STATE_2
							SET_PED_IN_VEHICLE_CONTEXT(TestPeds[0].Ped, GET_HASH_KEY("MISS_ARMENIAN3_FRANKLIN_TENSE"))
							SETTIMERA(0)
							SCENARIO_STATE = STATE_3
						BREAK
						CASE STATE_3
							IF TIMERA() > 15000	
								RESET_PED_IN_VEHICLE_CONTEXT(TestPeds[0].Ped)
								SCENARIO_STATE = STATE_4
							ENDIF		
						BREAK
						CASE STATE_4
							REMOVE_CLIP_SET("clipset@missarmenian3@franklin_driving")
							SCENARIO_STATE = STATE_5
						BREAK
				ENDSWITCH
			BREAK
			
			CASE TEST_STOCKADE_WITH_STICKY_GRENADES
				SWITCH SCENARIO_STATE			
						CASE STATE_INIT	
							IF NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
								GIVE_WEAPON_TO_PED( PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, 100, TRUE, TRUE )
								SCENARIO_STATE = STATE_1
							ENDIF
						BREAK
				ENDSWITCH
			BREAK
			
			CASE TEST_DEATH_WHILST_EXITING
							
					SWITCH SCENARIO_STATE
				
					CASE STATE_INIT
										
						IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
							IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped)
								INT SEAT
								SEAT = Index-1
				       			TASK_WARP_PED_INTO_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, INT_TO_ENUM(VEHICLE_SEAT, SEAT))
							ENDIF
						ENDIF
						
						SCENARIO_STATE = STATE_1
					BREAK
					
					CASE STATE_1
							
						IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped)
							ENTER_EXIT_VEHICLE_FLAGS flags
							IF PREFER_LEFT_ENTRY
								flags = ECF_USE_LEFT_ENTRY
							ELIF PREFER_RIGHT_ENTRY
								flags = ECF_USE_RIGHT_ENTRY
							ENDIF
								
				       		TASK_LEAVE_ANY_VEHICLE(TestPeds[0].Ped, 0, flags)
						ENDIF
						SETTIMERA(0)
						SCENARIO_STATE = STATE_2
					BREAK
					
					CASE STATE_2
					
						IF TIMERA() > DEATH_TIME
							//SET_PED_ACCURACY(TestPeds[1].Ped, 100)
							//SET_PED_COMBAT_ATTRIBUTES(TestPeds[1].Ped, CA_PERFECT_ACCURACY, TRUE)
							//TASK_SHOOT_AT_ENTITY(TestPeds[1].Ped, TestPeds[0].Ped, -1, FIRING_TYPE_CONTINUOUS)
							SCENARIO_STATE = STATE_3
						ENDIF
						
					BREAK
					
				ENDSWITCH	
			BREAK		
		
			CASE TEST_COLLISION_WHILE_ENTERING
				IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle) 
				AND NOT IS_ENTITY_DEAD(TestVehicles[1].Vehicle)
				AND NOT IS_ENTITY_DEAD(TestPeds[0].Ped)
				AND NOT IS_ENTITY_DEAD(TestPeds[1].Ped)
					
					SWITCH SCENARIO_STATE
					
						CASE STATE_INIT		
							IF SWIPE_TYPE = 0
								SET_ENTITY_COORDS(TestVehicles[1].Vehicle, <<-59.01208, 138.36926, 7.77247>> )
								SET_ENTITY_HEADING(TestVehicles[1].Vehicle, 0.0)
							ELIF SWIPE_TYPE = 1
								SET_ENTITY_COORDS(TestVehicles[1].Vehicle, <<-49.16578, 144.24376, 6.35158>> )
								SET_ENTITY_HEADING(TestVehicles[1].Vehicle, 90.0)
							ELIF SWIPE_TYPE = 2
								SET_ENTITY_COORDS(TestVehicles[1].Vehicle, <<-58.44479, 151.69591, 6.35158>>  )
								SET_ENTITY_HEADING(TestVehicles[1].Vehicle, 180.0)
							ENDIF   
							SET_PED_INTO_VEHICLE(TestPeds[1].Ped, TestVehicles[1].Vehicle)
							TASK_VEHICLE_TEMP_ACTION(TestPeds[1].Ped, TestVehicles[1].Vehicle, TEMPACT_GOFORWARD, 3000)
							SCENARIO_STATE = STATE_1
						BREAK
						CASE STATE_1
							SETTIMERA(0)
							SCENARIO_STATE = STATE_2
						BREAK
						CASE STATE_2
							IF TIMERA() > WAIT_BEFORE_ENTER_EXIT_TIME
								TASK_ENTER_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle)
								SCENARIO_STATE = STATE_3
							ENDIF
						BREAK
						
					ENDSWITCH
					
				ENDIF
			BREAK
			
			CASE TEST_COLLISION_WHILE_EXITING
			
				IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle) 
				AND NOT IS_ENTITY_DEAD(TestVehicles[1].Vehicle)
				AND NOT IS_ENTITY_DEAD(TestPeds[0].Ped)
				AND NOT IS_ENTITY_DEAD(scplayer)
				AND NOT IS_ENTITY_DEAD(TestPeds[1].Ped)
					
					SWITCH SCENARIO_STATE
					
						CASE STATE_INIT					
							IF SWIPE_TYPE = 0
								SET_ENTITY_COORDS(TestVehicles[1].Vehicle, <<-59.01208, 138.36926, 7.77247>> )
								SET_ENTITY_HEADING(TestVehicles[1].Vehicle, 0.0)
							ELIF SWIPE_TYPE = 1
								SET_ENTITY_COORDS(TestVehicles[1].Vehicle, <<-48.99829, 143.08881, 6.35158>> )
								SET_ENTITY_HEADING(TestVehicles[1].Vehicle, 90.0)
							ELIF SWIPE_TYPE = 2
								SET_ENTITY_COORDS(TestVehicles[1].Vehicle, <<-58.44479, 151.69591, 6.35158>>  )
								SET_ENTITY_HEADING(TestVehicles[1].Vehicle, 180.0)
							ENDIF  
					       	TASK_WARP_PED_INTO_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, VS_FRONT_RIGHT)
							SET_PED_INTO_VEHICLE(TestPeds[1].Ped, TestVehicles[1].Vehicle)
							TASK_VEHICLE_TEMP_ACTION(TestPeds[1].Ped, TestVehicles[1].Vehicle, TEMPACT_GOFORWARD, 3000)
							SCENARIO_STATE = STATE_1
						BREAK
						CASE STATE_1
							TASK_LEAVE_ANY_VEHICLE(TestPeds[0].Ped, WAIT_BEFORE_ENTER_EXIT_TIME)
							SCENARIO_STATE = STATE_2
						BREAK
						
				ENDSWITCH
				
				ENDIF
			BREAK
			
			CASE TEST_DEATH_WHILE_ENTERING
				IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle) 
				AND NOT IS_ENTITY_DEAD(TestPeds[0].Ped)
				AND NOT IS_ENTITY_DEAD(TestPeds[1].Ped)
					
					SWITCH SCENARIO_STATE
					
						CASE STATE_INIT	
							ENTER_EXIT_TASK_GIVEN = FALSE
							SHOOT_TASK_GIVEN = FALSE
							
							IF SWIPE_TYPE = 0
								SET_ENTITY_COORDS(TestVehicles[0].Vehicle, <<-60.88103, 143.33746, 6.35158>> )
								SET_ENTITY_HEADING(TestVehicles[0].Vehicle, 0.0)
								SET_CAM_PARAMS(TestCams[FixedCamera].cam,<<-60.539310,147.936768,7.679381>>,<<-7.064416,0.000000,-175.209595>>,65.000000)

							ELIF SWIPE_TYPE = 1	
								SET_ENTITY_COORDS(TestVehicles[0].Vehicle, <<-60.88103, 143.33746, 6.35158>>  )
								SET_ENTITY_HEADING(TestVehicles[0].Vehicle, 180.0)
								SET_CAM_PARAMS(TestCams[FixedCamera].cam,<<-58.738190,138.826096,8.312482>>,<<-22.549648,0.000000,12.793562>>,65.000000)
							ENDIF 

							SET_ENTITY_HEALTH(TestPeds[0].Ped, 101)
							SCENARIO_STATE = STATE_1
						BREAK
						CASE STATE_1
							SETTIMERA(0)
							SETTIMERB(0)
							SCENARIO_STATE = STATE_2
						BREAK
						CASE STATE_2
							IF NOT ENTER_EXIT_TASK_GIVEN AND TIMERA() > WAIT_BEFORE_ENTER_EXIT_TIME
								TASK_ENTER_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle)
								ENTER_EXIT_TASK_GIVEN = TRUE
							ENDIF
							
							IF NOT SHOOT_TASK_GIVEN AND TIMERB() > WAIT_BEFORE_FIRE_TIME
								SET_PED_ACCURACY(TestPeds[1].Ped, 100)
								SET_PED_COMBAT_ATTRIBUTES(TestPeds[1].Ped, CA_PERFECT_ACCURACY, TRUE)
								TASK_SHOOT_AT_ENTITY(TestPeds[1].Ped, TestPeds[0].Ped, -1, FIRING_TYPE_CONTINUOUS)
								SHOOT_TASK_GIVEN = TRUE
							ENDIF
						BREAK
						
					ENDSWITCH
					
				ENDIF
			BREAK
			
						
			CASE TEST_DEATH_WHILE_EXITING
				IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle) 
				AND NOT IS_ENTITY_DEAD(TestPeds[0].Ped)
				AND NOT IS_ENTITY_DEAD(TestPeds[1].Ped)
					
					SWITCH SCENARIO_STATE
					
						CASE STATE_INIT		
							ENTER_EXIT_TASK_GIVEN = FALSE
							SHOOT_TASK_GIVEN = FALSE
							
							IF SWIPE_TYPE = 0
								SET_ENTITY_COORDS(TestVehicles[0].Vehicle, <<-60.88103, 143.33746, 6.35158>> )
								SET_ENTITY_HEADING(TestVehicles[0].Vehicle, 0.0)
								SET_CAM_PARAMS(TestCams[FixedCamera].cam,<<-58.738190,138.826096,8.312482>>,<<-22.549648,0.000000,12.793562>>,65.000000)
							ELIF SWIPE_TYPE = 1
								SET_ENTITY_COORDS(TestVehicles[0].Vehicle, <<-60.88103, 143.33746, 6.35158>>  )
								SET_ENTITY_HEADING(TestVehicles[0].Vehicle, 180.0)
								SET_CAM_PARAMS(TestCams[FixedCamera].cam,<<-58.738190,138.826096,8.312482>>,<<-22.549648,0.000000,12.793562>>,65.000000)
							ENDIF  
							SET_PED_INTO_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle)
							SET_ENTITY_HEALTH(TestPeds[0].Ped, 101)
							SCENARIO_STATE = STATE_1
						BREAK
						CASE STATE_1
							SETTIMERA(0)
							SETTIMERB(0)
							SCENARIO_STATE = STATE_2
						BREAK
						CASE STATE_2						
							IF NOT ENTER_EXIT_TASK_GIVEN AND TIMERA() > WAIT_BEFORE_ENTER_EXIT_TIME
								TASK_LEAVE_ANY_VEHICLE(TestPeds[0].Ped, 0)
								ENTER_EXIT_TASK_GIVEN = TRUE
							ENDIF
							
							IF NOT SHOOT_TASK_GIVEN AND TIMERB() > WAIT_BEFORE_FIRE_TIME
								SET_PED_ACCURACY(TestPeds[1].Ped, 100)
								SET_PED_COMBAT_ATTRIBUTES(TestPeds[1].Ped, CA_PERFECT_ACCURACY, TRUE)
								TASK_SHOOT_AT_ENTITY(TestPeds[1].Ped, TestPeds[0].Ped, -1, FIRING_TYPE_CONTINUOUS)
								SHOOT_TASK_GIVEN = TRUE
							ENDIF
						BREAK
						
					ENDSWITCH
					
				ENDIF
			BREAK
			
			CASE TEST_THROUGH_WINDSCREEN
				IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle) 
				AND NOT IS_ENTITY_DEAD(TestPeds[0].Ped)
					
					SWITCH SCENARIO_STATE
					
						CASE STATE_INIT								
							IF WALL_HEIGHT_TYPE = ENUM_TO_INT(ONE_METRE_HIGH_WALL)
								SET_ENTITY_COORDS(TestVehicles[0].Vehicle, <<39.05972, 188.74785, 6.35078>> )
								SET_ENTITY_HEADING(TestVehicles[0].Vehicle, 0.0)
								SET_CAM_PARAMS(TestCams[FixedCamera].cam,<<35.180328,202.122940,7.617620>>,<<0.946441,-0.000000,-92.923889>>,65.000000)
							ELIF WALL_HEIGHT_TYPE = ENUM_TO_INT(TWO_METRE_HIGH_WALL)
								SET_ENTITY_COORDS(TestVehicles[0].Vehicle, <<44.47080, 192.04300, 6.35078>>  )
								SET_ENTITY_HEADING(TestVehicles[0].Vehicle, 0.0)
								SET_CAM_PARAMS(TestCams[FixedCamera].cam,<<40.557129,201.848328,7.710519>>,<<-3.626902,0.000000,-92.923889>>,65.000000)
							ELIF WALL_HEIGHT_TYPE = ENUM_TO_INT(THREE_METRE_HIGH_WALL)
								SET_ENTITY_COORDS(TestVehicles[0].Vehicle, <<51.56202, 191.70973, 6.35078>>  )
								SET_ENTITY_HEADING(TestVehicles[0].Vehicle, 0.0)
								SET_CAM_PARAMS(TestCams[FixedCamera].cam,<<46.430065,201.413742,8.109383>>,<<-2.345681,0.000000,-92.327446>>,65.000000)
							ENDIF  
							SET_PED_CONFIG_FLAG(TestPeds[0].Ped, PCF_WillFlyThroughWindscreen, TRUE)
							SET_PED_INTO_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle)
							SCENARIO_STATE = STATE_1
						BREAK
						CASE STATE_1
							SET_ENTITY_VELOCITY(TestVehicles[0].Vehicle, <<0.0,VEHICLE_SPEED, 0.0>>)
							TASK_VEHICLE_TEMP_ACTION(TestPeds[0].Ped, TestVehicles[0].Vehicle, TEMPACT_GOFORWARD, 3000)
							SCENARIO_STATE = STATE_2
						BREAK					
					ENDSWITCH
					
				ENDIF
			BREAK
			
			CASE TEST_JUMP_OUT
				IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle) 
				AND NOT IS_ENTITY_DEAD(TestPeds[0].Ped)
					
					SET_PED_RESET_FLAG(TestPeds[0].Ped, PRF_PreventGoingIntoStillInVehicleState, TRUE)
					
					IF DOES_CAM_EXIST(TestCams[TrackingCamera].cam)
					    POINT_CAM_AT_ENTITY(TestCams[TrackingCamera].cam, TestVehicles[0].Vehicle, <<0.0, 0.0, 0.0>>)
						camera_pos =  GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TestVehicles[0].Vehicle, SphericalCamCoords (CAM_ZOOM, CAM_PITCH, CAM_PAN))
                        SET_CAM_COORD (TestCams[TrackingCamera].cam, camera_pos)
					ENDIF
					
					SWITCH SCENARIO_STATE
							
						CASE STATE_INIT		
							IF DOES_CAM_EXIST(TestCams[TrackingCamera].cam)
                    			DESTROY_CAM(TestCams[TrackingCamera].cam)
							ENDIF
						
							TestCams[TrackingCamera].cam = CREATE_CAM ("DEFAULT_SCRIPTED_CAMERA")
							SET_CAM_ACTIVE(TestCams[TrackingCamera].cam, TRUE )    
			  				SET_VEHICLE_ENGINE_ON(TestVehicles[0].Vehicle, TRUE, TRUE)
							SET_ENTITY_COORDS(TestVehicles[0].Vehicle, <<-63.43328 + VEHICLE_SIDE_OFFSET, 68.22320, 6.35084>>)
							SET_PED_INTO_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle)
							SCENARIO_STATE = STATE_1
						BREAK
						CASE STATE_1
							SET_ENTITY_VELOCITY(TestVehicles[0].Vehicle, <<0.0,VEHICLE_SPEED, 0.0>>)
							TASK_VEHICLE_TEMP_ACTION(TestPeds[0].Ped, TestVehicles[0].Vehicle, TEMPACT_GOFORWARD, 3000)
							SETTIMERA(0)
							SCENARIO_STATE = STATE_2
						BREAK		
						CASE STATE_2						
							IF TIMERA() > WAIT_BEFORE_ENTER_EXIT_TIME
								TASK_LEAVE_ANY_VEHICLE(TestPeds[0].Ped, 0, ECF_JUMP_OUT)
								SCENARIO_STATE = STATE_3
							ENDIF
						BREAK
					ENDSWITCH
					
				ENDIF
			BREAK
			
			CASE TEST_COLLISION_WHILE_JACKING_PED
							
				IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle) 
				AND NOT IS_ENTITY_DEAD(TestVehicles[1].Vehicle) 
				AND NOT IS_ENTITY_DEAD(TestPeds[0].Ped)
				AND NOT IS_ENTITY_DEAD(TestPeds[1].Ped)
				AND NOT IS_ENTITY_DEAD(TestPeds[2].Ped)
					
					SWITCH SCENARIO_STATE
					
						CASE STATE_INIT		
							ENTER_EXIT_TASK_GIVEN = FALSE
							SHOOT_TASK_GIVEN = FALSE
							VEHICLE_RAM_TASK_GIVEN = FALSE
							
							IF SWIPE_TYPE = 0
								SET_ENTITY_COORDS(TestVehicles[1].Vehicle, <<-58.83489, 137.52158, 6.35158>> )
								SET_ENTITY_HEADING(TestVehicles[1].Vehicle, 0.0)
							ELIF SWIPE_TYPE = 1
								SET_ENTITY_COORDS(TestVehicles[1].Vehicle, <<-48.99829, 143.08881, 6.35158>> )
								SET_ENTITY_HEADING(TestVehicles[1].Vehicle, 90.0)
							ELIF SWIPE_TYPE = 2
								SET_ENTITY_COORDS(TestVehicles[1].Vehicle, <<-58.44479, 151.69591, 6.35158>>  )
								SET_ENTITY_HEADING(TestVehicles[1].Vehicle, 180.0)
							ENDIF  
							
							SET_CAM_PARAMS(TestCams[FixedCamera].cam,<<-58.738190,138.826096,8.312482>>,<<-22.549648,0.000000,12.793562>>,65.000000)
							
							VEHICLE_SEAT PLAYER_ZERO_SEAT
							PLAYER_ZERO_SEAT = VS_DRIVER
							IF JACK_FROM_PASSENGER
								PLAYER_ZERO_SEAT = VS_FRONT_RIGHT 
							ENDIF
							SET_PED_INTO_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, PLAYER_ZERO_SEAT)
							SET_ENTITY_HEALTH(TestPeds[0].Ped, 101)
							SET_PED_INTO_VEHICLE(TestPeds[2].Ped, TestVehicles[1].Vehicle, VS_DRIVER)
							SCENARIO_STATE = STATE_1
						BREAK
						CASE STATE_1
							SETTIMERA(0)
							SETTIMERB(0)
							SCENARIO_STATE = STATE_2
						BREAK
						CASE STATE_2						
							IF NOT ENTER_EXIT_TASK_GIVEN AND TIMERA() > WAIT_BEFORE_ENTER_EXIT_TIME
								TASK_ENTER_VEHICLE(TestPeds[1].Ped, TestVehicles[0].Vehicle, -1, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_JACK_ANYONE)
								ENTER_EXIT_TASK_GIVEN = TRUE
							ENDIF
							IF NOT VEHICLE_RAM_TASK_GIVEN AND TIMERB() > WAIT_BEFORE_VEHICLE_RAM
								TASK_VEHICLE_TEMP_ACTION(TestPeds[2].Ped, TestVehicles[1].Vehicle, TEMPACT_GOFORWARD, 3000)
								VEHICLE_RAM_TASK_GIVEN = TRUE
							ENDIF
						BREAK
						
					ENDSWITCH
					
				ENDIF
			BREAK
			
			CASE TEST_DEATH_WHILE_JACKING_PED
				IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle) 
				AND NOT IS_ENTITY_DEAD(TestVehicles[1].Vehicle) 
				AND NOT IS_ENTITY_DEAD(TestPeds[0].Ped)
				AND NOT IS_ENTITY_DEAD(TestPeds[1].Ped)
				AND NOT IS_ENTITY_DEAD(TestPeds[2].Ped)
					
					SWITCH SCENARIO_STATE
					
						CASE STATE_INIT		
							ENTER_EXIT_TASK_GIVEN = FALSE
							SHOOT_TASK_GIVEN = FALSE
							VEHICLE_RAM_TASK_GIVEN = FALSE
							
							IF SWIPE_TYPE = 0
								SET_ENTITY_COORDS(TestVehicles[1].Vehicle, <<-58.83489, 137.52158, 6.35158>> )
								SET_ENTITY_HEADING(TestVehicles[1].Vehicle, 0.0)
							ELIF SWIPE_TYPE = 1
								SET_ENTITY_COORDS(TestVehicles[1].Vehicle, <<-48.99829, 143.08881, 6.35158>> )
								SET_ENTITY_HEADING(TestVehicles[1].Vehicle, 90.0)
							ELIF SWIPE_TYPE = 2
								SET_ENTITY_COORDS(TestVehicles[1].Vehicle, <<-58.44479, 151.69591, 6.35158>>  )
								SET_ENTITY_HEADING(TestVehicles[1].Vehicle, 180.0)
							ENDIF  
							
							SET_CAM_PARAMS(TestCams[FixedCamera].cam,<<-58.738190,138.826096,8.312482>>,<<-22.549648,0.000000,12.793562>>,65.000000)
							
							VEHICLE_SEAT PLAYER_ZERO_SEAT
							PLAYER_ZERO_SEAT = VS_DRIVER
							IF JACK_FROM_PASSENGER
								PLAYER_ZERO_SEAT = VS_FRONT_RIGHT 
							ENDIF
							SET_PED_INTO_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, PLAYER_ZERO_SEAT)
							SET_ENTITY_HEALTH(TestPeds[0].Ped, 101)
							SCENARIO_STATE = STATE_1
						BREAK
						CASE STATE_1
							SETTIMERA(0)
							SETTIMERB(0)
							SCENARIO_STATE = STATE_2
						BREAK
						CASE STATE_2						
							IF NOT ENTER_EXIT_TASK_GIVEN AND TIMERA() > WAIT_BEFORE_ENTER_EXIT_TIME
								TASK_ENTER_VEHICLE(TestPeds[1].Ped, TestVehicles[0].Vehicle, -1, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_JACK_ANYONE)
								ENTER_EXIT_TASK_GIVEN = TRUE
							ENDIF
							IF NOT SHOOT_TASK_GIVEN AND TIMERB() > WAIT_BEFORE_FIRE_TIME
								SET_PED_ACCURACY(TestPeds[2].Ped, 100)
								SET_PED_COMBAT_ATTRIBUTES(TestPeds[2].Ped, CA_PERFECT_ACCURACY, TRUE)
								TASK_SHOOT_AT_ENTITY(TestPeds[2].Ped, TestPeds[1].Ped, -1, FIRING_TYPE_CONTINUOUS)
								SHOOT_TASK_GIVEN = TRUE
							ENDIF
						BREAK
						
					ENDSWITCH
					
				ENDIF
			BREAK
			
			CASE TEST_OVERRIDE_ENTRY_CLIPSET
				IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle) 
				AND NOT IS_ENTITY_DEAD(TestPeds[0].Ped)
							 
					SWITCH SCENARIO_STATE
						CASE STATE_INIT		
							REQUEST_CLIP_SET(clipsetname)
							IF HAS_CLIP_SET_LOADED(clipsetname)
								TASK_ENTER_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, -1, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK, ECF_JACK_ANYONE, clipsetname)
								SCENARIO_STATE = STATE_1
							ENDIF
						BREAK
					ENDSWITCH
					
				ENDIF
			BREAK
			
			CASE TEST_FLEE_EXIT
				IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle) 
				AND NOT IS_ENTITY_DEAD(TestPeds[0].Ped)
				AND NOT IS_ENTITY_DEAD(TestPeds[1].Ped)
					
					SWITCH SCENARIO_STATE
					
						CASE STATE_INIT		
							SET_PED_COMBAT_ATTRIBUTES(TestPeds[0].Ped, CA_USE_VEHICLE, FALSE)
							SET_PED_FLEE_ATTRIBUTES(TestPeds[0].Ped, FA_USE_VEHICLE, FALSE)
							SET_PED_INTO_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, VS_DRIVER)
							SCENARIO_STATE = STATE_1
						BREAK
						CASE STATE_1
							TASK_SMART_FLEE_PED(TestPeds[0].Ped, TestPeds[1].Ped, 50.0, -1)
							SCENARIO_STATE = STATE_2
						BREAK
						
					ENDSWITCH
					
				ENDIF
			BREAK
			
        ENDSWITCH   

ENDPROC

////////////////////////////////////////////////////////////////////////////////
 
//Purpose: Runs the main scenarios
PROC Run_Test_Scenario ()
    int index = 0
    
    SWITCH TestScenarioAStatus
        //setps up all the data for the sceanrio, this is only called once or if selected in widget
        CASE InitialiseScenarioData
            PlayerStartPos = GET_PLAYER_START_POS()         //sets the players start coords at the default

            Bscenario_running = FALSE
            INITIALISE_PED_DATA (Testpeds)
            INITIALISE_VEHICLE_DATA (TestVehicles)
            INITIALISE_CAM_DATA(TestCams )
            INITIALISE_OBJECT_DATA(TestObjects)
            SETUP_TEST_DATA ()
    
            HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
            TestScenarioAStatus = CreateScenarioEntities      
			
            Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)     //sets the 
        
        BREAK
        
        // Creates all the scenario data
        CASE CreateScenarioEntities
            Bscenario_running = FALSE
            
            Set_Gang_Relationships (TRUE)   
        
            Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
        
            //peds
            FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
                CREATE_PED_ENTITY(Testpeds[index])
                GiveBlipsToPedsGrps (Testpeds[index])
                ALTER_COMBAT_STATS (Testpeds[index] )
                SWAP_PED_WEAPONS (Testpeds[index])
               
                Block_Peds_Temp_Events (Testpeds[index], TRUE)
                SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
            ENDFOR
            
            //TestVehicles
            FOR Index = 0 to MAX_NUMBER_OF_VEHICLES -1
                CREATE_VEHICLE_ENTITY (TestVehicles[index] )
            ENDFOR
        
            //TestCams
            for index = 0 to MAX_NUMBER_OF_CAMERAS -1
                CREATE_CAM_ENTITY (TestCams[index])
            ENDFOR
            
            //TestObjects
            FOR Index = 0 to MAX_NUMBER_OF_OBJECTS -1
                CREATE_OBJECT_ENTITY (TestObjects[index])
            ENDFOR
            
            TestScenarioAStatus = SetScenarioEntities
        BREAK
        
        CASE SetScenarioEntities
            
            Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
			
            Start_And_Reset_Test ()
            
            IF gBeginCombatScenario
                TestScenarioAStatus = RunScenario
                gBeginCombatScenario = FALSE
                gRun_debuggig = FALSE
                Bscenario_running = TRUE
               // INITALISE_TEST_STATE()
                TEMP_ACTIVATE_CAMS (TestCams[FixedCamera].cam )
                
                IF DOES_CAM_EXIST (TestCams[FixedCamera].cam )
                    HELP_TEXT_STATE =  HTF_SCENARIO_RUNNING_FIXED_CAM
                ELSE
                    HELP_TEXT_STATE = HTF_STARTED_RUNNING_NO_CAMS
                ENDIF
            ENDIF
        BREAK
        
        //Runs the actual selected scenario
        
        CASE RunScenario
                
            Temp_Run_Scenario_Tracking_Cam (TestPeds, MAX_NUMBER_OF_PEDS, TestCams[TrackingCamera].cam) 
            
            Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
                
            Start_And_Reset_Test ()
                
            Check_For_Scenario_Reset ()
            
            IF DOES_CAM_EXIST (TestCams[TrackingCamera].cam)
                if IS_CAM_RENDERING (TestCams[TrackingCamera].cam)
                    HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_TRACKING_CAM
                ELSE
                    HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_FIXED_CAM
                ENDIF
            ENDIF
                    
            RUN_TEST ()     //run the main tests        
            
        BREAK 
                

        CASE CleanupScenario
            
            HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
            
			SCENARIO_STATE = STATE_INIT
			
			REMOVE_CLIP_SET(clipsetname)
			
            Cleanup_Scenario_Entities ()
			           
            IF gResetToDefault  
                Temp_cleanup_scenario_cams ()       //here we are changing scenarios so we need to reset cams
                TestScenarioAStatus = InitialiseScenarioData
                gResetToDefault = FALSE
            ENDIF
        
            IF gResetCombatScenario
                Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)
                gcurrentselection = gSelection 
                IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
                    ACTIVATE_CAM (TestCams[FixedCamera].cam)
                ELSE
                    Temp_cleanup_scenario_cams ()   
                ENDIF
                TestScenarioAStatus = CreateScenarioEntities
                gResetCombatScenario = FALSE
            ENDIF
        
        BREAK
        
    ENDSWITCH
ENDPROC

SCRIPT
    
    SET_DEBUG_ACTIVE (TRUE)
    
    SETUP_MISSION_XML_MENU (XMLMenu, KEY_Q )
    
    SETUP_AREA_FOR_MISSION (<<0.0, 0.0, 2.0>>)
    
    //Gets a reference to the player
    Get_The_Player ()
    
    CREATE_TEST_WIDGET ()
    
    //set the player ready for the script
    SET_PLAYER_COLISION(scplayer, true)
    
    WHILE TRUE
    
        // controls the help text hides if xml menu is active
        TEXT_CONTROLLER ()
    
        // Can set all scenario peds invincible from the widget
        Set_Scenario_Peds_Invincible (TestPeds, MAX_NUMBER_OF_PEDS, gsetpedsinvincible )
    
        //User can create a debug cam for setting sceanrios
        Temp_Create_Debug_Cam (TestCams[FixedCamera]) 
        
        //Runs the selected option from the XML menu 
        Run_Selection_From_XML_input ()
        
        //Checks that a valid selection has been input and runs the scenario
        IF (gcurrentselection <> InvalidSelection)
    
            Draw_Debug_Info (  )
            
            //Sets the test scenario into debug mode
            Set_To_Debug ()
            
            if (gRun_debuggig)
                Temp_Debug_Scenario (TestPeds, TestVehicles, Route, TestCams[FixedCamera].cam, S_M_Y_COP_01)                              //Allows the entities in the scneario to be adjusted
                Print_Scenario_Data (TestPeds, TestVehicles, Route, TestCams)
            ENDIF
            
            Run_Test_Scenario ()                                
            
            SWITCH_BETWEEN_FIXED_AND_TRACKING (TestCams[FixedCamera].cam)
            
            PRINT_ACTIVE_TEST (Bscenario_running,gRun_debuggig)     
        ENDIF
        
        Terminate_test_script ()
        
        WAIT (0)
    
    
    ENDWHILE

ENDSCRIPT


#ENDIF  // IS_DEBUG_BUILD

