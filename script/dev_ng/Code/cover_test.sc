// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

////////////////////////////////////////////////////////////////////////////////

// Notes: This is script is to allow easy setup of test scenarios for the code department.
// It works in conjunction with test_tools.sch
// It is a data driven system

////////////////////////////////////////////////////////////////////////////////

// Includes
USING "rage_builtins.sch"
USING "script_xml.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "commands_streaming.sch"
USING "commands_graphics.sch"
USING "commands_debug.sch"
USING "test_tools.sch"
USING "commands_vehicle.sch"
USING "commands_camera.sch"
USING "commands_player.sch"
USING "commands_physics.sch"
USING "script_DEBUG.sch"
USING "commands_event.sch"
USING "commands_script.sch"
USING "commands_entity.sch"
USING "commands_itemsets.sch"

////////////////////////////////////////////////////////////////////////////////

// Definitions

// These enums must match with those in /build/dev/common/data/script/xml/Testbed/TestCoverMenu.xml
ENUM TESTS
	TEST_TASK_EXIT_COVER = 0
ENDENUM

// Common states which can be reused across tests
ENUM GENERIC_TEST_STATE_ENUM
	STATE_INIT,
	STATE_1,
	STATE_2,
	STATE_3,
	STATE_4
ENDENUM

// Exit Cover Mode
ENUM EXIT_COVER_MODE
	IDLE_EXIT,
	AIMING_EXIT,
	CORNER_EXIT
ENDENUM

// Move Blend Values
ENUM MOVE_BLEND_VALUE
	STILL,
	WALK,
	RUN,
	SPRINT
ENDENUM

////////////////////////////////////////////////////////////////////////////////
     
// Constants
CONST_INT MAX_NUMBER_OF_PEDS         10
CONST_INT MAX_NUMBER_OF_VEHICLES     10
CONST_INT MAX_NUMBER_OF_NODES        7
CONST_INT MAX_NUMBER_OF_CAMERAS      2
CONST_INT MAX_NUMBER_OF_OBJECTS 	 1
CONST_INT MAX_NUMBER_OF_COVER_POINTS 10
CONST_INT FixedCamera                0
CONST_INT TrackingCamera             1

////////////////////////////////////////////////////////////////////////////////

// Defaults values when the test is reset to factory the standard values are used

STRING XMLMenu = "Testbed/TestCoverMenu"
VECTOR gvMapOffset  //offset all coords from the origin by the psoition of the mapin world coords

// Arrays of actors
peds_struct 		TestPeds[MAX_NUMBER_OF_PEDS]
vehicle_struct 		TestVehicles[MAX_NUMBER_OF_VEHICLES]
RouteStruct 		Route[MAX_NUMBER_OF_NODES]
StCameraData 		TestCams[MAX_NUMBER_OF_CAMERAS]
Object_struct 		TestObjects[MAX_NUMBER_OF_OBJECTS]
coverpoint_index 	CoverPoints[MAX_NUMBER_OF_COVER_POINTS]

// Stores the state of the test
GENERIC_TEST_STATE_ENUM TEST_STATE = STATE_INIT

INT EXIT_COVER_TYPE = ENUM_TO_INT(IDLE_EXIT)
INT MOVE_BLEND_TYPE = ENUM_TO_INT(WALK)
BOOL bUseGoToPointAimingTask = FALSE
BOOL bTestLeftCoverPoint = FALSE
VECTOR vGoToAndAimCoords

SEQUENCE_INDEX TEST_SEQUENCE

////////////////////////////////////////////////////////////////////////////////

 // Create Debug Rag Widgets 
PROC CREATE_TEST_WIDGET ()
    START_WIDGET_GROUP("Cover Test")
        SETUP_WIDGET ()
		 	START_WIDGET_GROUP("TEST_EXIT_COVER")
			 	START_NEW_WIDGET_COMBO() 
                    ADD_TO_WIDGET_COMBO("IDLE_EXIT")
                    ADD_TO_WIDGET_COMBO("AIMING_EXIT")
                    ADD_TO_WIDGET_COMBO("CORNER_EXIT")
				STOP_WIDGET_COMBO("EXIT_COVER_TYPE", EXIT_COVER_TYPE)
				START_NEW_WIDGET_COMBO() 
                    ADD_TO_WIDGET_COMBO("STILL")
                    ADD_TO_WIDGET_COMBO("WALK")
					ADD_TO_WIDGET_COMBO("RUN")
                    ADD_TO_WIDGET_COMBO("SPRINT")
				STOP_WIDGET_COMBO("MOVE_BLEND_TYPE", MOVE_BLEND_TYPE)
				ADD_WIDGET_BOOL ("USE GO TO POINT AIMING", bUseGoToPointAimingTask) 
				ADD_WIDGET_BOOL ("TEST LEFT COVER POINT", bTestLeftCoverPoint) 
			STOP_WIDGET_GROUP()     
    STOP_WIDGET_GROUP()
ENDPROC

////////////////////////////////////////////////////////////////////////////////

//Mission flow House keeping

//PUPROSE: Removes all the scenarios and resets all the relationships
PROC Cleanup_Scenario_Entities ()
    int index = 0
        for index = 0 to MAX_NUMBER_OF_PEDS -1  
            CLEAN_UP_PED_ENTITIES (TestPeds[index].ped)
        ENDFOR 
        
        for index = 0 to MAX_NUMBER_OF_VEHICLES -1
            CLEAN_UP_VEHICLE_ENTITIES (TestVehicles[index].Vehicle )
        ENDFOR
        
        for index = 0 to MAX_NUMBER_OF_OBJECTS -1
            CLEAN_UP_OBJECT_ENTITIES (TestObjects[index].Object)
        ENDFOR

    Set_Gang_Relationships (FALSE)  
ENDPROC

PROC Terminate_test_script ()
    if IS_KEYBOARD_KEY_JUST_PRESSED (KEY_S)
        CLEAR_PRINTS ()
        CLEAR_HELP ()
        Cleanup_Scenario_Entities ()
        Temp_cleanup_scenario_cams ()
        SET_PLAYER_COLISION(scplayer, true)
        IF NOT IS_PED_INJURED (scplayer)
            SET_PED_COORDS_KEEP_VEHICLE(scplayer, GET_PLAYER_START_POS () + gvMapOffset)
        ENDIF
        
        TERMINATE_THIS_THREAD ()
        
    ENDIF
ENDPROC

PROC INITALISE_TEST_STATE()
    //reset each test
    SingleTaskStatus = startSingleTask
	TEST_STATE = STATE_INIT
    //end reset
ENDPROC

//END: Mission flow House keeping

////////////////////////////////////////////////////////////////////////////////

// Individual test setup

FUNC COVER_EXIT_TYPE GET_COVER_EXIT_TYPE_FROM_WIDGETS()

	COVER_EXIT_TYPE RETURN_VALUE
	SWITCH INT_TO_ENUM(EXIT_COVER_MODE, EXIT_COVER_TYPE)
		CASE IDLE_EXIT 		
			RETURN_VALUE = IDLE_COVER_EXIT 		
		BREAK
		CASE AIMING_EXIT
			RETURN_VALUE = AIMING_COVER_EXIT 	
		BREAK
		CASE CORNER_EXIT 	
			RETURN_VALUE = CORNER_COVER_EXIT 	
		BREAK
	ENDSWITCH
	RETURN RETURN_VALUE
	
ENDFUNC

FUNC FLOAT GET_MOVE_BLEND_RATIO_FROM_WIDGETS()

	FLOAT RETURN_VALUE
	SWITCH INT_TO_ENUM(MOVE_BLEND_VALUE, MOVE_BLEND_TYPE)
		CASE STILL 		
			RETURN_VALUE = PEDMOVEBLENDRATIO_STILL 		
		BREAK
		CASE WALK
			RETURN_VALUE = PEDMOVEBLENDRATIO_WALK 	
		BREAK
		CASE RUN 	
			RETURN_VALUE = PEDMOVEBLENDRATIO_RUN 	
		BREAK
		CASE SPRINT 	
			RETURN_VALUE = PEDMOVEBLENDRATIO_SPRINT
		BREAK
	ENDSWITCH
	RETURN RETURN_VALUE
	
ENDFUNC

////////////////////////////////////////////////////////////////////////////////
  
PROC SETUP_EXIT_COVER_TEST()

    TestPeds[0].PedsCoords = <<16.39, 180.50, 6.35>>
    TestPeds[0].PedHeading = 0
    TestPeds[0].Pedrelgrp = 1
   	TestPeds[0].PedModel = S_M_M_MARINE_01
    TestPeds[0].PedsWeapon = WEAPONTYPE_SMG
    TestPeds[0].PedcombatMove = 0
    TestPeds[0].pedcombatrange = 2
    TestPeds[0].combatcover = CA_USE_COVER
    TestPeds[0].bcombatcover = TRUE
	
	TestCams[FixedCamera].cam_pos = <<11.491119,180.890244,9.094440>>
    TestCams[FixedCamera].cam_rot = <<-15.693443,-0.000000,-84.970634>>
    TestCams[FixedCamera].cam_fov = 65.0
    TestCams[FixedCamera].bActivateCam = TRUE   
	
ENDPROC

PROC INIT_EXIT_COVER_TEST()
	
	VECTOR vCoverCoords
	
	IF bTestLeftCoverPoint
		vCoverCoords = <<16.35316, 183.15704, 6.35078>>
		vGoToAndAimCoords = <<25.97518, 180.19562, 6.35078>>
		CoverPoints[0] = ADD_COVER_POINT(vCoverCoords, 270.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_180)
	ELSE
		vCoverCoords = <<16.39, 180.50, 6.35>>
		vGoToAndAimCoords = <<23.50147, 183.79396, 6.35078>>
		CoverPoints[0] = ADD_COVER_POINT(vCoverCoords, 270.0, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180)
	ENDIF
	
	SET_ENTITY_COORDS(TestPeds[0].Ped, vCoverCoords)
	
ENDPROC

PROC RUN_EXIT_COVER_TEST()
	
	PED_INDEX MainPedIndex = TestPeds[0].Ped
	COVERPOINT_INDEX CoverIndex = CoverPoints[0]
	
	IF NOT IS_ENTITY_DEAD(MainPedIndex)

		SWITCH TEST_STATE
			CASE STATE_INIT		
				TASK_PUT_PED_DIRECTLY_INTO_COVER(MainPedIndex, <<0.0,0.0,0.0>>, -1, FALSE, 0, FALSE, FALSE, CoverIndex)
				SETTIMERA(0)
				TEST_STATE = STATE_1
			BREAK
			CASE STATE_1
				IF TIMERA() > 1000
				
					FLOAT fMoveBlendRatio
					
					fMoveBlendRatio = GET_MOVE_BLEND_RATIO_FROM_WIDGETS()
					
					ENAV_SCRIPT_FLAGS iFlags
					
					IF fMoveBlendRatio > PEDMOVE_WALK	
						iFlags = ENAV_NO_STOPPING
					ENDIF
						
			        OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
						TASK_EXIT_COVER(NULL, GET_COVER_EXIT_TYPE_FROM_WIDGETS(), vGoToAndAimCoords)
						IF bUseGoToPointAimingTask
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, vGoToAndAimCoords, vGoToAndAimCoords, fMoveBlendRatio, FALSE, 0.25, 4.0, FALSE, iFlags)
						ELSE
			           		TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, vGoToAndAimCoords, fMoveBlendRatio, -1, 0.25, iFlags)
						ENDIF
			        CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
					
					TASK_PERFORM_SEQUENCE (MainPedIndex, TEST_SEQUENCE)
	        		CLEAR_SEQUENCE_TASK(TEST_SEQUENCE)
					
					TEST_STATE = STATE_2
				ENDIF
			BREAK
		ENDSWITCH
	
	ENDIF
	
ENDPROC

PROC CLEANUP_EXIT_COVER_TEST()
	
	INT index
	FOR index = 0 to MAX_NUMBER_OF_COVER_POINTS - 1
       	IF CoverPoints[index] != NULL
			REMOVE_COVER_POINT(CoverPoints[index])
			CoverPoints[index] = NULL
		ENDIF
    ENDFOR
	
ENDPROC

////////////////////////////////////////////////////////////////////////////////
  
// Main setup, run once when the test is started
PROC SETUP_TEST_DATA()
    SWITCH INT_TO_ENUM(TESTS, gcurrentselection)       // the ENUM corresponds to the values from the XML file 
       CASE TEST_TASK_EXIT_COVER
            SETUP_EXIT_COVER_TEST()
       BREAK
//	   CASE TEST_TASK_PUT_PED_DIRECTLY_INTO_COVER
//	   		SETUP_PUT_PED_DIRECTLY_INTO_COVER_TEST())
//	   BREAK
    ENDSWITCH   
ENDPROC

// Main init, run each time test is restarted
PROC INIT_TEST()
    SWITCH INT_TO_ENUM(TESTS, gcurrentselection)       // the ENUM corresponds to the values from the XML file 
       CASE TEST_TASK_EXIT_COVER
            INIT_EXIT_COVER_TEST()
       BREAK
//	   CASE TEST_TASK_PUT_PED_DIRECTLY_INTO_COVER
//	   		INIT_PUT_PED_DIRECTLY_INTO_COVER_TEST())
//	   BREAK
    ENDSWITCH   
ENDPROC

// Main run
PROC RUN_TEST()
    SWITCH INT_TO_ENUM (TESTS, gcurrentselection)       // the ENUM corresponds to the values from the XML file 
		CASE TEST_TASK_EXIT_COVER
			RUN_EXIT_COVER_TEST()
		BREAK
//	   CASE TEST_TASK_PUT_PED_DIRECTLY_INTO_COVER
//	   		RUN_PUT_PED_DIRECTLY_INTO_COVER_TEST())
//	   BREAK
	ENDSWITCH	
ENDPROC

// Main cleanup
PROC CLEANUP_TEST()
    SWITCH INT_TO_ENUM (TESTS, gcurrentselection)       // the ENUM corresponds to the values from the XML file 
		CASE TEST_TASK_EXIT_COVER
			CLEANUP_EXIT_COVER_TEST()
		BREAK
//	   CASE TEST_TASK_PUT_PED_DIRECTLY_INTO_COVER
//	   		CLEANUP_PUT_PED_DIRECTLY_INTO_COVER_TEST())
//	   BREAK
	ENDSWITCH	
ENDPROC

////////////////////////////////////////////////////////////////////////////////
  
//Purpose: Runs the main scenarios
PROC Run_Test_Scenario ()
    int index = 0
    
    SWITCH TestScenarioAStatus
        //setps up all the data for the sceanrio, this is only called once or if selected in widget
        CASE InitialiseScenarioData
            PlayerStartPos = GET_PLAYER_START_POS()         //sets the players start coords at the default

            Bscenario_running = FALSE
            INITIALISE_PED_DATA (Testpeds)
            INITIALISE_VEHICLE_DATA (TestVehicles)
            INITIALISE_CAM_DATA(TestCams )
            INITIALISE_OBJECT_DATA(TestObjects)
            SETUP_TEST_DATA()
    
            HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
            TestScenarioAStatus = CreateScenarioEntities      
			
            Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)     //sets the 
        
        BREAK
        
        // Creates all the scenario data
        CASE CreateScenarioEntities
            Bscenario_running = FALSE
            
            Set_Gang_Relationships (TRUE)   
        
            Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
        
            //peds
            FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
                CREATE_PED_ENTITY(Testpeds[index])
                GiveBlipsToPedsGrps (Testpeds[index])
                ALTER_COMBAT_STATS (Testpeds[index] )
                SWAP_PED_WEAPONS (Testpeds[index])
               
                Block_Peds_Temp_Events (Testpeds[index], TRUE)
                SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
            ENDFOR
            
            //TestVehicles
            FOR Index = 0 to MAX_NUMBER_OF_VEHICLES -1
                CREATE_VEHICLE_ENTITY (TestVehicles[index] )
            ENDFOR
        
            //TestCams
            for index = 0 to MAX_NUMBER_OF_CAMERAS -1
                CREATE_CAM_ENTITY (TestCams[index])
            ENDFOR
            
            //TestObjects
            FOR Index = 0 to MAX_NUMBER_OF_OBJECTS -1
                CREATE_OBJECT_ENTITY (TestObjects[index])
            ENDFOR
            
            TestScenarioAStatus = SetScenarioEntities
        BREAK
        
        CASE SetScenarioEntities
            
            Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
			
            Start_And_Reset_Test ()
            
            IF gBeginCombatScenario
				
                TestScenarioAStatus = RunScenario
                gBeginCombatScenario = FALSE
                gRun_debuggig = FALSE
                Bscenario_running = TRUE
                
				INITALISE_TEST_STATE()
				INIT_TEST()
				
                TEMP_ACTIVATE_CAMS (TestCams[FixedCamera].cam )
                
                IF DOES_CAM_EXIST (TestCams[FixedCamera].cam )
                    HELP_TEXT_STATE =  HTF_SCENARIO_RUNNING_FIXED_CAM
                ELSE
                    HELP_TEXT_STATE = HTF_STARTED_RUNNING_NO_CAMS
                ENDIF
            ENDIF
        BREAK
        
        //Runs the actual selected scenario
        
        CASE RunScenario
                
            Temp_Run_Scenario_Tracking_Cam (TestPeds, MAX_NUMBER_OF_PEDS, TestCams[TrackingCamera].cam) 
            
            Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
                
            Start_And_Reset_Test ()
                
            Check_For_Scenario_Reset ()
            
            IF DOES_CAM_EXIST (TestCams[TrackingCamera].cam)
                if IS_CAM_RENDERING (TestCams[TrackingCamera].cam)
                    HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_TRACKING_CAM
                ELSE
                    HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_FIXED_CAM
                ENDIF
            ENDIF
                    
            RUN_TEST()     //run the main tests        
            
        BREAK 
                

        CASE CleanupScenario
            
            HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
			
			CLEANUP_TEST()
            
            Cleanup_Scenario_Entities ()
            
            IF gResetToDefault  
                Temp_cleanup_scenario_cams ()       //here we are changing scenarios so we need to reset cams
                TestScenarioAStatus = InitialiseScenarioData
                gResetToDefault = FALSE
            ENDIF
        
            IF gResetCombatScenario
                Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)
                gcurrentselection = gSelection 
                IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
                    ACTIVATE_CAM (TestCams[FixedCamera].cam)
                ELSE
                    Temp_cleanup_scenario_cams ()   
                ENDIF
                TestScenarioAStatus = CreateScenarioEntities
                gResetCombatScenario = FALSE
            ENDIF	
        
        BREAK
        
    ENDSWITCH
ENDPROC

SCRIPT
    
	// This allows debug lines to be rendered around peds, vehicles and objects for locate commands.
    SET_DEBUG_ACTIVE (TRUE)
	
    gvMapOffset = GET_PLAYER_START_POS ()
    
    SETUP_MISSION_XML_MENU(XMLMenu, KEY_Q)
	
	// Create Debug Rag Widgets 
    CREATE_TEST_WIDGET()
	
	// Clear the area
    SETUP_AREA_FOR_MISSION(<<0.0, 0.0, 0.0>>)
    
    //Gets a reference to the player
    GET_THE_PLAYER()
    
    // Set the player ready for the script
    SET_PLAYER_COLISION(scplayer, true)
    
    WHILE TRUE
    
        // Controls the help text hides if xml menu is active
        TEXT_CONTROLLER()
    
        // Can set all scenario peds invincible from the widget
        Set_Scenario_Peds_Invincible (TestPeds, MAX_NUMBER_OF_PEDS, gsetpedsinvincible )
    
        // User can create a debug cam for setting sceanrios
        Temp_Create_Debug_Cam (TestCams[FixedCamera]) 
        
        //Runs the selected option from the XML menu 
        Run_Selection_From_XML_input ()
        
        //Checks that a valid selection has been input and runs the scenario
        IF (gcurrentselection <> InvalidSelection)
    
            Draw_Debug_Info ()
            
            //Sets the test scenario into debug mode
            Set_To_Debug ()
            
            if (gRun_debuggig)
                Temp_Debug_Scenario (TestPeds, TestVehicles, Route, TestCams[FixedCamera].cam, S_M_Y_COP_01)                              //Allows the entities in the scneario to be adjusted
                Print_Scenario_Data (TestPeds, TestVehicles, Route, TestCams)
            ENDIF
            
            Run_Test_Scenario ()                                
            
            SWITCH_BETWEEN_FIXED_AND_TRACKING (TestCams[FixedCamera].cam)
            
            PRINT_ACTIVE_TEST (Bscenario_running,gRun_debuggig)     
        ENDIF
        
        Terminate_test_script ()
        
        WAIT (0)
    
    
    ENDWHILE

ENDSCRIPT


#ENDIF  // IS_DEBUG_BUILD

