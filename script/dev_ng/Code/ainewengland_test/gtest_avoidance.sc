#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_path.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_physics.sch"
USING "commands_decorator.sch"
USING "model_enums.sch"

CONST_INT NUM_PEDS 10
PED_INDEX pindex[NUM_PEDS]

BOOL bSpawn = FALSE
bool bDelete = FALSE
BOOL bExecute = TRUE
BOOL bTask = FALSE

FLOAT fMoveBlendRatio1 = 1.0
FLOAT fMoveBlendRatio2 = 1.0
FLOAT fRadius = 10.0
FLOAT fSpacing = 1.5
FLOAT fDistance = 20.0
int iSelectedAvoidanceTest =0
int iGroup1PedCount = 0

BOOL bIgnoreGroup1Peds = FALSE


ENUM eAvoidanceTest

	eCircle,
	e1v1HeadOn,
	e2v2HeadOn,
	e3v3HeadOn,
	e1v1Perpendicular,
	e1v1Parallel,
	e2v2Parallel,
	e3v3Parallel,
	e1UseMobilePhone,
	e1CombatPlayer

ENDENUM


PROC DELETE_AVOIDANCE_PEDS()

	INT i
	FOR i = 0 TO NUM_PEDS-1
		IF DOES_ENTITY_EXIST(pindex[i])		
			DELETE_PED(pindex[i])
		ENDIF
	ENDFOR

ENDPROC


PROC CREATE_AVOIDANCE_PEDS_PARALLEL(INT iFrontPeds, int iBackPeds)

	float heading
	VECTOR pedForward
	VECTOR pedRight
	VECTOR vCenter
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
	
			pedForward = GET_ENTITY_FORWARD_VECTOR(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			pedRight = <<pedForward.y, -pedForward.x, 0.0>>
			VECTOR pedPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			
			heading = GET_ENTITY_HEADING(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
			heading = (heading + 180 ) % 360
						
			vCenter = pedPosition + ( pedForward * fRadius )
			
		ENDIF
	
	ENDIF
		
	INT i
	FOR i = 0 TO iFrontPeds -1
	
		FLOAT fHeading = (heading + 90) % 360
		VECTOR vOffset = <<0, fRadius, 0>>
		VECTOR vForwardOffset = pedForward
		vForwardOffset *= 1 + (i * fSpacing )
		
	
		vOffset = ROTATE_VECTOR_ABOUT_Z(vOffset, fHeading)
		pindex[i] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_DOCKWORK_01, vCenter + vOffset + vForwardOffset, ( fHeading + 180 ) % 360 )
		SET_PED_CONFIG_FLAG(pindex[i], PCF_Avoidance_Member_of_Group1, TRUE)		
		
	ENDFOR
	
	FOR i = 0 TO iBackPeds -1
	
		FLOAT fHeading = (heading +90) % 360
		VECTOR vOffset = <<0, fRadius, 0>>
		VECTOR vForwardOffset = pedForward
		VECTOR vSideOffset = pedRight
		
		vForwardOffset *= 1 + (i * fSpacing )		
	
		vOffset = ROTATE_VECTOR_ABOUT_Z(vOffset, fHeading)
		pindex[iBackPeds + i] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_DOCKWORK_01, vCenter + vOffset + vForwardOffset + vSideOffset, ( fHeading + 180 ) % 360 )
		SET_PED_CONFIG_FLAG(pindex[iBackPeds + i], PCF_Avoidance_Ignore_Group1, bIgnoreGroup1Peds)
	ENDFOR	

ENDPROC


PROC CREATE_AVOIDANCE_PEDS_HEAD_ON(INT iLeftPeds, int iRightPeds)

	float heading
	VECTOR pedForward
	VECTOR vCenter
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
	
			pedForward = GET_ENTITY_FORWARD_VECTOR(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			VECTOR pedPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			
			heading = GET_ENTITY_HEADING(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
			heading = (heading + 180 ) % 360
						
			vCenter = pedPosition + ( pedForward * fRadius )
			
		ENDIF
	
	ENDIF
		
	INT i
	FOR i = 0 TO iLeftPeds -1
	
		FLOAT fHeading = (heading + 90) % 360
		VECTOR vOffset = <<0, fRadius, 0>>
		VECTOR vForwardOffset = pedForward
		vForwardOffset *= 1 + (i * fSpacing )
		
	
		vOffset = ROTATE_VECTOR_ABOUT_Z(vOffset, fHeading)
		pindex[i] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_DOCKWORK_01, vCenter + vOffset + vForwardOffset, ( fHeading + 180 ) % 360 )
		
		SET_PED_CONFIG_FLAG(pindex[i], PCF_Avoidance_Member_of_Group1, TRUE)
		
	ENDFOR
	
	FOR i = 0 TO iRightPeds -1
	
		FLOAT fHeading = (heading -90) % 360
		VECTOR vOffset = <<0, fRadius, 0>>
		VECTOR vForwardOffset = pedForward
		vForwardOffset *= 1 + (i * fSpacing )		
	
		vOffset = ROTATE_VECTOR_ABOUT_Z(vOffset, fHeading)
		pindex[iLeftPeds + i] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_DOCKWORK_01, vCenter + vOffset + vForwardOffset, ( fHeading + 180 ) % 360 )
		SET_PED_CONFIG_FLAG(pindex[iLeftPeds + i], PCF_Avoidance_Ignore_Group1, bIgnoreGroup1Peds)
	ENDFOR	
	
	
ENDPROC

PROC CREATE_AVOIDANCE_PEDS_CIRCLE()

	VECTOR vCenter
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
	
			VECTOR pedForward = GET_ENTITY_FORWARD_VECTOR(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			VECTOR pedPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			
			FLOAT heading = GET_ENTITY_HEADING(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
			heading = (heading + 180 ) % 360
			
			pedForward *= fRadius
			vCenter = pedPosition + pedForward					
			
		ENDIF
	
	ENDIF
	
	
	INT i
	FOR i = 0 TO NUM_PEDS-1
	
		FLOAT fHeading = ( 360.0 / NUM_PEDS ) * i
		VECTOR vOffset = <<0, fRadius, 0>>
	
		vOffset = ROTATE_VECTOR_ABOUT_Z(vOffset, fHeading)
				
	
		pindex[i] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_DOCKWORK_01, vCenter + vOffset, ( fHeading + 180 ) % 360 )


			

	ENDFOR

ENDPROC

PROC CREATE_AVOIDANCE_PEDS()

	REQUEST_MODEL(S_M_Y_DOCKWORK_01) 
	WHILE not HAS_MODEL_LOADED(S_M_Y_DOCKWORK_01)
		WAIT(0)
	ENDWHILE

	SWITCH INT_TO_ENUM(eAvoidanceTest, iSelectedAvoidanceTest)
		CASE eCircle			
			CREATE_AVOIDANCE_PEDS_CIRCLE()
		BREAK
		
		CASE e1v1HeadOn
			iGroup1PedCount = 1
			CREATE_AVOIDANCE_PEDS_HEAD_ON(1, 1)
		BREAK
		
		CASE e2v2HeadOn
			iGroup1PedCount = 2
			CREATE_AVOIDANCE_PEDS_HEAD_ON(2, 2)
		BREAK
		
		CASE e3v3HeadOn
			iGroup1PedCount = 3
			CREATE_AVOIDANCE_PEDS_HEAD_ON(3, 3)
		BREAK
		
		CASE e2v2Parallel
		BREAK
		CASE e3v3Parallel
		BREAK

		CASE e1v1Parallel
		CASE e1UseMobilePhone
		CASE e1CombatPlayer
			iGroup1PedCount = 1
			CREATE_AVOIDANCE_PEDS_PARALLEL(1, 1)
		BREAK
		
	ENDSWITCH


ENDPROC


PROC TASK_PEDS_MOVE_FORWARD()
	INT i
	FOR i = 0 TO NUM_PEDS-1
		IF DOES_ENTITY_EXIST(pindex[i])	
			IF NOT IS_PED_INJURED(pindex[i])
				VECTOR pedForward = GET_ENTITY_FORWARD_VECTOR(pindex[i])
				VECTOR pedPosition = GET_ENTITY_COORDS(pindex[i])
				pedForward *= fDistance
				VECTOR vTarget = pedPosition + pedForward	
				
				IF i < iGroup1PedCount
					TASK_FOLLOW_NAV_MESH_TO_COORD(pindex[i], vTarget, fMoveBlendRatio1)
				ELSE
					TASK_FOLLOW_NAV_MESH_TO_COORD(pindex[i], vTarget, fMoveBlendRatio2)
				ENDIF
			ENDIF
		ENDIF		
	ENDFOR				

ENDPROC

PROC TASK_PEDS_USE_PHONE()
	INT i
	FOR i = 0 TO NUM_PEDS-1
		IF DOES_ENTITY_EXIST(pindex[i])	
			IF NOT IS_PED_INJURED(pindex[i])
				
				TASK_USE_MOBILE_PHONE(pindex[i], FALSE)

			ENDIF
		ENDIF		
	ENDFOR	
ENDPROC

PROC TASK_PEDS_COMBAT_PLAYER()
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))		
			INT i
			FOR i = 0 TO NUM_PEDS-1
				IF DOES_ENTITY_EXIST(pindex[i])	
					IF NOT IS_PED_INJURED(pindex[i])
						
						GIVE_WEAPON_TO_PED(pindex[i], WEAPONTYPE_ASSAULTRIFLE, INFINITE_AMMO, FALSE, true)
						SET_CURRENT_PED_WEAPON(pindex[i], WEAPONTYPE_ASSAULTRIFLE, true)

						SET_PED_RELATIONSHIP_GROUP_HASH(pindex[i], RELGROUPHASH_HATES_PLAYER)
						//TASK_COMBAT_PED(pindex[i], GET_PLAYER_PED(GET_PLAYER_INDEX())) 

					ENDIF
				ENDIF		
			ENDFOR
		ENDIF
	ENDIF			
			
ENDPROC

PROC TASK_AVOIDANCE_PEDS()
	
		SWITCH INT_TO_ENUM(eAvoidanceTest, iSelectedAvoidanceTest)
			CASE eCircle			
				TASK_PEDS_MOVE_FORWARD()
			BREAK
			CASE e1v1HeadOn
				TASK_PEDS_MOVE_FORWARD()
			BREAK
			CASE e2v2HeadOn
				TASK_PEDS_MOVE_FORWARD()
			BREAK
			CASE e3v3HeadOn
				TASK_PEDS_MOVE_FORWARD()
			BREAK
			CASE e1v1Perpendicular
				TASK_PEDS_MOVE_FORWARD()
			BREAK
			
			CASE e1v1Parallel
				TASK_PEDS_MOVE_FORWARD()
			BREAK		
			
			CASE e1UseMobilePhone
				TASK_PEDS_USE_PHONE()
			BREAK
			
			CASE e1CombatPlayer			
				TASK_PEDS_COMBAT_PLAYER()
			BREAK
		ENDSWITCH	

ENDPROC


PROC CREATE_WIDGETS()
 	START_WIDGET_GROUP ("gtest_avoidance")

		START_NEW_WIDGET_COMBO()	
			ADD_TO_WIDGET_COMBO ("eCircle")
			ADD_TO_WIDGET_COMBO ("e1v1HeadOn")
			ADD_TO_WIDGET_COMBO ("e2v2HeadOn")
			ADD_TO_WIDGET_COMBO ("e3v3HeadOn")
			ADD_TO_WIDGET_COMBO ("e1v1Perpendicular")
			ADD_TO_WIDGET_COMBO ("e1v1Parallel")
			ADD_TO_WIDGET_COMBO ("e2v2Parallel")
			ADD_TO_WIDGET_COMBO ("e3v3Parallel")
			
			ADD_TO_WIDGET_COMBO ("e1UseMobilePhone")
			ADD_TO_WIDGET_COMBO ("e1CombatPlayer")
			
        STOP_WIDGET_COMBO("AvoidanceTest", iSelectedAvoidanceTest)	
	
	
		ADD_WIDGET_BOOL ("Spawn", bSpawn)
		ADD_WIDGET_BOOL ("Delete", bDelete)
		ADD_WIDGET_BOOL ("Task", bTask)
		
		ADD_WIDGET_FLOAT_SLIDER("MoveBlendRatio1", fMoveBlendRatio1, 0.1, 10.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("MoveBlendRatio2", fMoveBlendRatio2, 0.1, 10.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Spacing", fSpacing, 0.1, 10.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Radius", fRadius, 1.0, 20.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("Distance", fDistance, 1.0, 200.0, 1.0)
		
		ADD_WIDGET_BOOL ("IgnoreGroup1Peds", bIgnoreGroup1Peds)
		
		
	STOP_WIDGET_GROUP()
ENDPROC

PROC PROCESS_WIDGETS()

	IF bSpawn
		bSpawn = FALSE
		DELETE_AVOIDANCE_PEDS()
		CREATE_AVOIDANCE_PEDS()
	ENDIF	
	
	IF bDelete
		bDelete = FALSE
		DELETE_AVOIDANCE_PEDS()
	ENDIF
	
	IF bTask
		bTask = FALSE
		TASK_AVOIDANCE_PEDS()

	ENDIF	

ENDPROC

PROC PROCESS_DECORATORS()
	bExecute = FALSE	
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
		
			IF DECOR_EXIST_ON(GET_PLAYER_PED(GET_PLAYER_INDEX()), "rsne_testscript_active")													
				bExecute = DECOR_GET_BOOL(GET_PLAYER_PED(GET_PLAYER_INDEX()),"rsne_testscript_active")				
			ENDIF				
				
		ENDIF		
	ENDIF		
ENDPROC


SCRIPT

	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	CREATE_WIDGETS()

	WHILE bExecute	
		PROCESS_DECORATORS()	
		PROCESS_WIDGETS()
		WAIT(0)
	ENDWHILE

	DELETE_AVOIDANCE_PEDS()

ENDSCRIPT

#ENDIF  // IS_DEBUG_BUILD