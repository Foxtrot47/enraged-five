#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "script_xml.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "commands_streaming.sch"
USING "commands_graphics.sch"
USING "commands_cutscene.sch"
USING "commands_debug.sch"
USING "test_tools.sch"
USING "commands_vehicle.sch"
USING "commands_camera.sch"
USING "commands_player.sch"
USING "script_DEBUG.sch"
USING "commands_script.sch"
USING "commands_entity.sch"
USING "commands_event.sch"
USING "commands_decorator.sch"

bool bSpawn = FALSE
bool bDelete = FALSE

INT iSelectedScript = 0

ENUM TestScriptEnum
	GTEST_AIRPLANE,
	GTEST_AVOIDANCE,
	GTEST_BOATS,
	GTEST_HELICOPTERS,	
	SUPPRESS_BREAKOUTS,
	NEAR_MISS_VIA_CAR,
	DIVING_AWAY_FROM_CAR,
	DIVING_AWAY_FROM_CAR_WHILE_FLEEING,
	NUM_TESTSCRIPTS
ENDENUM

PROC  SET_SCRIPT_CAN_START (string current_Script, int current_Script_Hash)
	
	int iCurrent_Script
		iCurrent_Script = GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(current_Script_Hash)
		
		IF (iCurrent_Script = 0)
			REQUEST_SCRIPT(current_Script)
			WHILE NOT HAS_SCRIPT_LOADED(current_Script)
					WAIT(0)
			ENDWHILE
			START_NEW_SCRIPT(current_Script, DEFAULT_STACK_SIZE)
								
			IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
				IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))			
					DECOR_SET_BOOL(GET_PLAYER_PED(GET_PLAYER_INDEX()),"rsne_testscript_active",TRUE)
				ENDIF
			ENDIF
						
			PRINTSTRING("Launching ")
			PRINTSTRING(current_Script)
			PRINTSTRING(" script")
			PRINTNL()			
			
		ENDIF
	//ENDIF
ENDPROC


PROC CREATE_TEST_SCRIPT ( TestScriptEnum eTestScript )

    SWITCH eTestScript
		CASE GTEST_AIRPLANE
			SET_SCRIPT_CAN_START ("gtest_airplane", HASH("gtest_airplane"))
		BREAK
		CASE GTEST_AVOIDANCE
			SET_SCRIPT_CAN_START ("gtest_avoidance", HASH("gtest_avoidance"))
		BREAK
		CASE SUPPRESS_BREAKOUTS
			SET_SCRIPT_CAN_START ("SuppressBreakout", HASH("SuppressBreakout"))
		BREAK
		CASE NEAR_MISS_VIA_CAR
			SET_SCRIPT_CAN_START("gtest_NearlyMissedByCar", HASH("gtest_NearlyMissedByCar"))
		BREAK
		CASE DIVING_AWAY_FROM_CAR
			SET_SCRIPT_CAN_START("gtest_DivingFromCar", HASH("gtest_DivingFromCar"))
		BREAK
		CASE GTEST_HELICOPTERS
			SET_SCRIPT_CAN_START("gtest_helicopter", HASH("gtest_helicopter"))
		BREAK
		CASE GTEST_BOATS
			SET_SCRIPT_CAN_START("gtest_boat", HASH("gtest_boat"))		
		BREAK
		CASE DIVING_AWAY_FROM_CAR_WHILE_FLEEING
			SET_SCRIPT_CAN_START("gtest_DivingFromCarWhileFleeing", HASH("gtest_DivingFromCarWhileFleeing"))
		BREAK
	ENDSWITCH

ENDPROC

PROC CLEANUP_TEST_SCRIPT ( )

	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
			DECOR_SET_BOOL(GET_PLAYER_PED(GET_PLAYER_INDEX()),"rsne_testscript_active",FALSE)					
		ENDIF
	ENDIF

ENDPROC


PROC CLEANUP()
 
 	CLEANUP_TEST_SCRIPT()
    CLEAR_PRINTS()
    CLEAR_HELP()		
    TERMINATE_THIS_THREAD ()  
	
ENDPROC

PROC CREATE_WIDGETS()
  
	START_WIDGET_GROUP ("RSNE_TEST_SCRIPT")
       
		START_NEW_WIDGET_COMBO() 
			ADD_TO_WIDGET_COMBO ("gtest_airplane")
			ADD_TO_WIDGET_COMBO ("gtest_avoidance")
			ADD_TO_WIDGET_COMBO ("gtest_boats")
			ADD_TO_WIDGET_COMBO ("gtest_helicopter")			
			ADD_TO_WIDGET_COMBO ("SuppressBreakout")
			ADD_TO_WIDGET_COMBO ("gtest_NearlyMissedByCar")
			ADD_TO_WIDGET_COMBO ("gtest_DivingFromCar")
			ADD_TO_WIDGET_COMBO ("gtest_DivingFromCarWhileFleeing")
        STOP_WIDGET_COMBO("Scripts", iSelectedScript)			
			
		ADD_WIDGET_BOOL ("Spawn", bSpawn)
		ADD_WIDGET_BOOL ("Delete", bDelete)	
			
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC PROCESS_WIDGETS()
	
	IF bSpawn

		CLEANUP_TEST_SCRIPT()
		CREATE_TEST_SCRIPT(INT_TO_ENUM(TestScriptEnum, iSelectedScript))
		
		bSpawn = FALSE
	ENDIF	
	
	IF bDelete
		CLEANUP_TEST_SCRIPT()
		bDelete = FALSE
	ENDIF

ENDPROC

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
		CLEANUP()
	ENDIF
	
	SET_DEBUG_ACTIVE (TRUE)
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	DECOR_REGISTER_UNLOCK()
	DECOR_REGISTER("rsne_testscript_active", DECOR_TYPE_BOOL)	
	DECOR_REGISTER_LOCK()
	
	CREATE_WIDGETS()

	WHILE TRUE
		PROCESS_WIDGETS()
	
		WAIT(0)
	ENDWHILE

	CLEANUP()

ENDSCRIPT

#ENDIF  // IS_DEBUG_BUILD
