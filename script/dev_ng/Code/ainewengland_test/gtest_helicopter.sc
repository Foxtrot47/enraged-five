//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	test_helicopter.sc											//
//		AUTHOR			:	Justin Wheelock												//
//		DESCRIPTION		:	This is a test script to test the functionality of a 	 	//
//							helicopter in persuit of a player.							//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_path.sch"
USING "commands_ped.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_physics.sch"
USING "commands_decorator.sch"
USING "model_enums.sch"
USING "rgeneral_include.sch"

// Data for the setup
//	-the heli to be used
//	-the driver to go into said car
STRUCT HeliVehicleData

	VEHICLE_INDEX vindex
	PED_INDEX pindex[5]
	
ENDSTRUCT


HeliVehicleData vehicleData[10]
int iSpawnedHelis = 0
int iSelectedHeli = 0
int iDriverIndex = 0
int iNumPassengers = 0
//WIDGET_GROUP_ID	WidgetID
VECTOR VecTestingAreaLocation			= <<1600, 3211, 40.5>>
//FLOAT zHeight 							= 20.0
FLOAT fHeliSpeed 						= 20.0
FLOAT fRadius							= 15.0
FLOAT fHeight							= 0.0
FLOAT fTurbulence						= 1.0
FLOAT fPrevioudTurbulence				= 1.0
BOOL bDriveBy							= FALSE
BOOL bGotoPlayer						= FALSE
BOOL bGotoPlayerAndLand					= FALSE
BOOL bCombatPlayer						= FALSE
BOOL bFleePlayer						= FALSE
BOOL bFollowPlayer						= FALSE
BOOL bCirclePlayer						= FALSE
BOOL bAttackPlayer						= FALSE
BOOL bEscortPlayer						= FALSE
BOOL bAttackCoord						= FALSE
BOOL bProtectPlayer						= FALSE
BOOL bTeleportAllToTestingArea			= FALSE
BOOL bToggleRockets						= FALSE
BOOL bUseRockets						= FALSE

BOOL bUseSearchLight					= FALSE
BOOL bUseSearchLightPreviousValue		= FALSE
BOOL bSearchLightAlwaysOn				= FALSE

BOOL bToggleHeightMapFollowing			= FALSE
BOOL bDisableHeightMap					= TRUE
BOOL bSpawn 							= FALSE
BOOL bDelete 							= FALSE
BOOL bExecute 							= TRUE
BOOL bQueryMissionType					= FALSE

MODEL_NAMES eDriverModel				= S_M_M_Pilot_02
MODEL_NAMES ePassengerModel				= S_F_Y_STRIPPER_01
int iSelectedHelicopter = 0


//
//
PROC SET_HELI_PED_COMBAT_AI(PED_INDEX pedId) 

	GIVE_DELAYED_WEAPON_TO_PED(pedId, WEAPONTYPE_SNIPERRIFLE, 300, TRUE) 
	SET_PED_COMBAT_ATTRIBUTES(pedId, CA_ALWAYS_FIGHT, TRUE) 
	SET_PED_COMBAT_ATTRIBUTES(pedId, CA_ALWAYS_FIGHT, FALSE) 

	SET_PED_COMBAT_ATTRIBUTES(pedId, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, true)


	SET_PED_COMBAT_MOVEMENT(pedId, CM_WILLADVANCE) 
	SET_PED_COMBAT_ABILITY(pedId, CAL_AVERAGE) 
	SET_PED_COMBAT_RANGE(pedId, CR_FAR) 
	SET_PED_TARGET_LOSS_RESPONSE(pedId, TLR_NEVER_LOSE_TARGET) 
	SET_PED_HIGHLY_PERCEPTIVE(pedId, TRUE) 
	SET_PED_CAN_BE_TARGETTED(pedId, TRUE) 
	SET_PED_SEEING_RANGE(pedId, 300+100.0) 

	// SET_PED_VISUAL_FIELD_MIN_ANGLE(pedId, -90.0) 
	// SET_PED_VISUAL_FIELD_MAX_ANGLE(pedId, 90.0) 
	// SET_PED_VISUAL_FIELD_MIN_ELEVATION_ANGLE(pedId, -90.0) 
	// SET_PED_VISUAL_FIELD_MAX_ELEVATION_ANGLE(pedId, 90.0) 
	SET_PED_VISUAL_FIELD_PERIPHERAL_RANGE(pedId, 400.0) 

	SET_COMBAT_FLOAT(pedId, CCF_HELI_SENSES_RANGE, 400.0) 
	
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedId, true)

	// Make peds a bit tougher. 
	SET_ENTITY_MAX_HEALTH(pedId, 250) 
	SET_ENTITY_HEALTH(pedId, 250) 
	SET_PED_ARMOUR(pedId, 250) 
	
	SET_PED_RELATIONSHIP_GROUP_HASH(pedId, RELGROUPHASH_PLAYER)	

ENDPROC



PROC DELETE_HELIS()
	
	INT i
	FOR i = 0 to iSpawnedHelis

		IF DOES_ENTITY_EXIST(vehicleData[i].vindex)			
			DELETE_VEHICLE(vehicleData[i].vindex)
		ENDIF
		
		int k
		FOR k = 0 TO 5 - 1
			IF DOES_ENTITY_EXIST(vehicleData[i].pindex[k])		
				DELETE_PED(vehicleData[i].pindex[k])
			ENDIF	
			
		ENDFOR		
	
	ENDFOR
	
	iSpawnedHelis = 0
ENDPROC

PROC GENERATE_HELI_LOCATION(VECTOR& heliPosition)

			// grab the player's position for offsetting the scenario
			VECTOR playerPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			VECTOR playerForward = GET_ENTITY_FORWARD_VECTOR(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			Vector g_up = <<0, 0, 1.0>>
			heliPosition = playerPosition + ( playerForward * fRadius ) + (g_up * fHeight)	
	
ENDPROC	// GENERATE_HELI_LOCATION

PROC CREATE_HELI()

	MODEL_NAMES eHeliModel = ANNIHILATOR
	IF iSelectedHelicopter = 1
		eHeliModel = BUZZARD
	ELIF iSelectedHelicopter = 2
		eHeliModel = BUZZARD
	elif iSelectedHelicopter = 3
		eHeliModel = CARGOBOB
	ENDIF 

	// grab the car model
	REQUEST_MODEL(eHeliModel)
	
	// grab the driver model
	REQUEST_MODEL(eDriverModel) 
	
	REQUEST_MODEL(ePassengerModel)
		
	// load them
	WHILE NOT (HAS_MODEL_LOADED(eHeliModel) AND HAS_MODEL_LOADED(eDriverModel) AND HAS_MODEL_LOADED(ePassengerModel))
	
		WAIT(0)
		
	ENDWHILE	// models loaded
		
	// better have a player at this point
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
		


			FLOAT playerHeading = GET_ENTITY_HEADING(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
			FLOAT heliHeading = (playerHeading + 180 ) % 360
			
			VECTOR heliPosition						
			GENERATE_HELI_LOCATION(heliPosition)
		
			// make the car and put in a driver
			vehicleData[iSpawnedHelis].vindex = CREATE_VEHICLE(
				eHeliModel,
				heliPosition, 
				heliHeading )
					
		
			IF DOES_ENTITY_EXIST(vehicleData[iSpawnedHelis].vindex)				
				
				SET_ENTITY_VISIBLE(vehicleData[iSpawnedHelis].vindex, TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehicleData[iSpawnedHelis].vindex, FALSE)

				vehicleData[iSpawnedHelis].pindex[iDriverIndex] = CREATE_PED_INSIDE_VEHICLE(
							vehicleData[iSpawnedHelis].vindex, 
							PEDTYPE_MISSION, 
							eDriverModel, 
							VS_DRIVER )
							
							
							

			int i = 0
			FOR i = 0 TO iNumPassengers - 1
				
				int passengerIndex = i + 1
				VEHICLE_SEAT seatIndex = INT_TO_ENUM(VEHICLE_SEAT, ENUM_TO_INT(VS_DRIVER)+ passengerIndex)
				IF IS_VEHICLE_SEAT_FREE(vehicleData[iSpawnedHelis].vindex, seatIndex  )				
			
						vehicleData[iSpawnedHelis].pindex[passengerIndex] = CREATE_PED_INSIDE_VEHICLE(
								vehicleData[iSpawnedHelis].vindex, 
								PEDTYPE_MISSION, 
								ePassengerModel, 
								seatIndex )		
								
								
						//SET_HELI_PED_COMBAT_AI(vehicleData[iSpawnedHelis].pindex[passengerIndex])									
								
				ENDIF								
			
			ENDFOR
				
							
							
							
				iSelectedHeli = iSpawnedHelis			
				iSpawnedHelis++							
							
			ENDIF	// car and driver set up

		ENDIF	// player uninjured
		
	ENDIF	// player exists
	
ENDPROC	// CREATE_HELI




PROC TASK_HELI_COMBAT_PLAYER()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedHeli].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedHeli].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedHeli].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedHeli].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedHeli].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedHeli].vindex, TRUE, TRUE)
					IF IS_PED_IN_FLYING_VEHICLE(vehicleData[iSelectedHeli].pindex[iDriverIndex])
	
						SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedHeli].vindex)	
						
					ENDIF	// vehicle is a plane/helicopter
					

/*
					int k		
					FOR k = 0 TO 5 - 1
						IF DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[k])		
							TASK_COMBAT_PED(vehicleData[iSelectedHeli].pindex[k], PLAYER_PED_ID())
						ENDIF	
						
					ENDFOR		
*/					
		
		
					VECTOR playerPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
					Vector g_up = <<0, 0, 1.0>>
					VECTOR targetPosition = playerPosition + (g_up * fHeight)				
		
				
					TASK_HELI_MISSION(vehicleData[iSelectedHeli].pindex[iDriverIndex], vehicleData[iSelectedHeli].vindex, NULL, NULL, targetPosition, 
						MISSION_GOTO, 5.0, 0.1, GET_HEADING_FROM_ENTITIES(vehicleData[iSelectedHeli].vindex, PLAYER_PED_ID()), 33, 3)
						
					//TASK_DRIVE_BY(vehicleData[iSelectedHeli].pindex[iDriverIndex], PLAYER_PED_ID(), null, <<0,0,0>>, 5000, 100, TRUE, FIRING_PATTERN_BURST_FIRE_HELI)
									
					
					
					//TASK_HELI_MISSION(sLocalData.niPed[0], sLocalData.niCutSceneChopper, NULL, NULL, <<356.0887, 1918.2512, 100.0761>>, MISSION_GOTO, 15.0, 100.0, -1, 100, 50)	

					//TASK_HELI_COMBAT_PLAYER()(vehicleData[iSelectedHeli].pindex[iDriverIndex], vehicleData[iSelectedHeli].vindex, NULL, PLAYER_PED_ID(), <<0.0,0.0,0.0>>, MISSION_GOTO, 20.0, 30.0, -1.0, 40, 40)					

					
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists

ENDPROC

PROC TASK_HELI_GOTO_PLAYER()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedHeli].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedHeli].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedHeli].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedHeli].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedHeli].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedHeli].vindex, TRUE, TRUE)
					IF IS_PED_IN_FLYING_VEHICLE(vehicleData[iSelectedHeli].pindex[iDriverIndex])
	
						SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedHeli].vindex)	
						
					ENDIF	// vehicle is a plane/helicopter
					
					VECTOR playerPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
					Vector g_up = <<0, 0, 1.0>>
					VECTOR targetPosition = playerPosition + (g_up * fHeight)						
					
					
				//TASK_HELI_MISSION(ped[ped_pilot].id, vehicle[vehChopper].id,null,null,<<-1315.0525, -238.1505, 59.0535>>,MISSION_GOTO,4,3,-54,20,20)					
				//TASK_HELI_MISSION(vehicleData[iSelectedHeli].pindex[iDriverIndex], vehicleData[iSelectedHeli].vindex,null,null,<<-1315.0525, -238.1505, 59.0535>>,MISSION_GOTO,4,3,-54,20,20)					
					

//					TASK_GO_TO_COORD_ANY_MEANS(vehicleData[iSelectedHeli].pindex[iDriverIndex], targetPosition, 20.0, vehicleData[iSelectedHeli].vindex)
					//TASK_GO_TO_COORD_ANY_MEANS(vehicleData[iSelectedHeli].pindex[iDriverIndex], targetPosition, 20.0, NULL)

					TASK_HELI_MISSION(vehicleData[iSelectedHeli].pindex[iDriverIndex], vehicleData[iSelectedHeli].vindex, NULL, NULL, targetPosition, MISSION_GOTO, 20.0, 30.0, -1.0, 0, 0)					

					
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists

ENDPROC

PROC TASK_HELI_GOTO_PLAYER_AND_LAND()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedHeli].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedHeli].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedHeli].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedHeli].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedHeli].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedHeli].vindex, TRUE, TRUE)
					IF IS_PED_IN_FLYING_VEHICLE(vehicleData[iSelectedHeli].pindex[iDriverIndex])
	
						SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedHeli].vindex)	
						
					ENDIF	// vehicle is a plane/helicopter
					
					VECTOR playerPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
					Vector g_up = <<0, 0, 1.0>>
					VECTOR targetPosition = playerPosition + (g_up * fHeight)						
					
					
					TASK_HELI_MISSION(vehicleData[iSelectedHeli].pindex[iDriverIndex], vehicleData[iSelectedHeli].vindex, NULL, NULL, targetPosition, MISSION_LAND, 30.0, 30.0, -1.0, 30, 30)					

					
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists

ENDPROC


PROC TASK_HELI_PROTECT_PLAYER()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedHeli].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedHeli].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedHeli].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedHeli].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedHeli].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedHeli].vindex, TRUE, TRUE)
					IF IS_PED_IN_FLYING_VEHICLE(vehicleData[iSelectedHeli].pindex[iDriverIndex])
	
						SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedHeli].vindex)	
						
					ENDIF	// vehicle is a plane/helicopter
					
					
					TASK_HELI_MISSION(vehicleData[iSelectedHeli].pindex[iDriverIndex], vehicleData[iSelectedHeli].vindex, NULL, PLAYER_PED_ID(), <<0.0,0.0,0.0>>, MISSION_PROTECT, 30.0, fRadius, -1.0, CEIL(fHeight), CEIL(fHeight))					
					//TASK_HELI_MISSION(pedPilot, vehChopper, NULL, PLAYER_PED_ID(), (<<0.0,0.0,0.0>>), MISSION_PROTECT, 30.0, 25, -1.0, CEIL(35), CEIL(35))											
					//TASK_VEHICLE_HELI_PROTECT(vehicleData[iSelectedHeli].pindex[iDriverIndex], vehicleData[iSelectedHeli].vindex, PLAYER_PED_ID(), fHeliSpeed, DF_SteerAroundObjects, fRadius, CEIL(fHeight))
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(vehicleData[iSelectedHeli].pindex[iDriverIndex], true)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehicleData[iSelectedHeli].vindex, true)			
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()), RELGROUPHASH_COP )
					int k
				
					FOR k = 1 TO 5 - 1
						IF (DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[k]) AND NOT IS_PED_INJURED(vehicleData[iSelectedHeli].pindex[k]))	
							IF NOT IS_PED_IN_COMBAT(vehicleData[iSelectedHeli].pindex[k]) 

								
								
								TASK_COMBAT_HATED_TARGETS_IN_AREA(vehicleData[iSelectedHeli].pindex[k], GET_ENTITY_COORDS(vehicleData[iSelectedHeli].pindex[k]), 299.0)
							ENDIF
						ENDIF	
			
					ENDFOR	
					
					
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists

ENDPROC


PROC TASK_HELI_ESCORT_PLAYER()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedHeli].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedHeli].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedHeli].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedHeli].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedHeli].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedHeli].vindex, TRUE, TRUE)
					IF IS_PED_IN_FLYING_VEHICLE(vehicleData[iSelectedHeli].pindex[iDriverIndex])
	
						SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedHeli].vindex)	
						
					ENDIF	// vehicle is a plane/helicopter
									
					
					TASK_HELI_MISSION(vehicleData[iSelectedHeli].pindex[iDriverIndex], vehicleData[iSelectedHeli].vindex, NULL, PLAYER_PED_ID(), <<0.0,0.0,0.0>>, MISSION_ESCORT_REAR, 20.0, fHeight, -1.0, CEIL(fHeight), 10)					

					
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists

ENDPROC
PROC TASK_HELI_FLEE_PLAYER()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedHeli].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedHeli].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedHeli].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedHeli].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedHeli].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedHeli].vindex, TRUE, TRUE)
					IF IS_PED_IN_FLYING_VEHICLE(vehicleData[iSelectedHeli].pindex[iDriverIndex])
	
						SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedHeli].vindex)	
						
					ENDIF	// vehicle is a plane/helicopter
					
					// run over the bystander!!!
					//SET_VEHICLE_FORWARD_SPEED(vehicleData[iSelectedHeli].vindex, fHeliSpeed)
					//VECTOR offset = <<0.0, 0.0, 0.0>>
					//TASK_HELI_CHASE(vehicleData[iSelectedHeli].pindex[iDriverIndex], GET_PLAYER_PED(GET_PLAYER_INDEX()), offset)
					
					TASK_HELI_MISSION(vehicleData[iSelectedHeli].pindex[iDriverIndex], vehicleData[iSelectedHeli].vindex, NULL, NULL, <<256.0887, 2918.2512, 42.0761>>, MISSION_FLEE, 25.0, 200.0, -1, 100, 50)					
					//TASK_HELI_MISSION(vehicleData[iSelectedHeli].pindex[iDriverIndex], vehicleData[iSelectedHeli].vindex, NULL, PLAYER_PED_ID(), <<0.0,fRadius,fHeight>>, MISSION_FOLLOW, 50.0, 30.0, -1.0, FLOOR(fHeight), FLOOR(fHeight))					

				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists

ENDPROC

PROC TASK_HELI_CHASE_PLAYER()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedHeli].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedHeli].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedHeli].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedHeli].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedHeli].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedHeli].vindex, TRUE, TRUE)
					IF IS_PED_IN_FLYING_VEHICLE(vehicleData[iSelectedHeli].pindex[iDriverIndex])
	
						SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedHeli].vindex)	
						
					ENDIF	// vehicle is a plane/helicopter
					
					// run over the bystander!!!
					//SET_VEHICLE_FORWARD_SPEED(vehicleData[iSelectedHeli].vindex, fHeliSpeed)
					//VECTOR offset = <<0.0, 0.0, 0.0>>
					//TASK_HELI_CHASE(vehicleData[iSelectedHeli].pindex[iDriverIndex], GET_PLAYER_PED(GET_PLAYER_INDEX()), offset)
					TASK_HELI_MISSION(vehicleData[iSelectedHeli].pindex[iDriverIndex], vehicleData[iSelectedHeli].vindex, NULL, PLAYER_PED_ID(), <<0.0,fRadius,fHeight>>, MISSION_FOLLOW, 50.0, 30.0, -1.0, FLOOR(fHeight), FLOOR(fHeight))					

				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists

ENDPROC	// TASK_chase player

PROC TASK_HELI_MISSION_CIRCLE()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedHeli].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedHeli].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedHeli].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedHeli].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedHeli].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedHeli].vindex, TRUE, TRUE)
					IF IS_PED_IN_FLYING_VEHICLE(vehicleData[iSelectedHeli].pindex[iDriverIndex])
	
						SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedHeli].vindex)	
						
					ENDIF	// vehicle is a plane/helicopter
					
					
					TASK_HELI_MISSION(vehicleData[iSelectedHeli].pindex[iDriverIndex], vehicleData[iSelectedHeli].vindex, NULL, GET_PLAYER_PED(GET_PLAYER_INDEX()), <<0,0,25>>, MISSION_CIRCLE, 20, 25, -1, 280, 10)

			
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists

ENDPROC	// TASK_HELI_MISSION_CIRCLE

PROC TOGGLE_HEIGHTMAP_FOLLOWING()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedHeli].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedHeli].vindex)

					SET_VEHICLE_DISABLE_HEIGHT_MAP_AVOIDANCE(vehicleData[iSelectedHeli].vindex, bDisableHeightMap)
					
					bDisableHeightMap = !bDisableHeightMap
				
					
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists
	
ENDPROC

PROC TOGGLE_HELI_SEARCHLIGHT()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedHeli].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedHeli].vindex)

					SET_VEHICLE_SEARCHLIGHT(vehicleData[iSelectedHeli].vindex, bUseSearchLight, bUseSearchLight)	
				
					
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists	

ENDPROC

PROC TOGGLE_HELI_ROCKETS()
	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedHeli].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedHeli].vindex)

					IF bUseRockets
						SET_CURRENT_PED_VEHICLE_WEAPON(vehicleData[iSelectedHeli].pindex[iDriverIndex], WEAPONTYPE_VEHICLE_PLAYER_BULLET)
					ELIF
						SET_CURRENT_PED_VEHICLE_WEAPON(vehicleData[iSelectedHeli].pindex[iDriverIndex], WEAPONTYPE_VEHICLE_SPACE_ROCKET)
					ENDIF
					
					bUseRockets = !bUseRockets
				
					
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists					

ENDPROC

PROC TASK_HELI_MISSION_ATTACK()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedHeli].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedHeli].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedHeli].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedHeli].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedHeli].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedHeli].vindex, TRUE, TRUE)
					IF IS_PED_IN_FLYING_VEHICLE(vehicleData[iSelectedHeli].pindex[iDriverIndex])
	
						SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedHeli].vindex)	
						
					ENDIF	// vehicle is a plane/helicopter

					SET_PED_ACCURACY(vehicleData[iSelectedHeli].pindex[iDriverIndex], 0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(vehicleData[iSelectedHeli].pindex[iDriverIndex], TRUE)								
					
					TASK_HELI_MISSION(vehicleData[iSelectedHeli].pindex[iDriverIndex], vehicleData[iSelectedHeli].vindex, GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(GET_PLAYER_INDEX())), GET_PLAYER_PED(GET_PLAYER_INDEX()), <<0,0,0>>, MISSION_ATTACK, 50, 50, -1, 10, 10)	
										
					//TASK_HELI_MISSION(driver[7].ped,mission_veh[23].veh,mission_veh[0].veh,NULL,<<0,0,0>>,MISSION_ATTACK,25,50,-1,70,60)

			
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists

ENDPROC	// TASK_HELI_MISSION_CIRCLE

PROC TASK_HELI_ATTACK_COORD

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedHeli].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedHeli].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedHeli].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedHeli].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedHeli].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedHeli].vindex, TRUE, TRUE)
					IF IS_PED_IN_FLYING_VEHICLE(vehicleData[iSelectedHeli].pindex[iDriverIndex])
	
						SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedHeli].vindex)	
						
					ENDIF	// vehicle is a plane/helicopter

					SET_PED_ACCURACY(vehicleData[iSelectedHeli].pindex[iDriverIndex], 0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(vehicleData[iSelectedHeli].pindex[iDriverIndex], TRUE)								
					
					//TASK_HELI_MISSION(vehicleData[iSelectedHeli].pindex[iDriverIndex], vehicleData[iSelectedHeli].vindex, GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(GET_PLAYER_INDEX())), GET_PLAYER_PED(GET_PLAYER_INDEX()), <<0,0,0>>, MISSION_ATTACK, 50, 50, -1, 10, 10)									
					//TASK_HELI_MISSION(driver[7].ped,mission_veh[23].veh,mission_veh[0].veh,NULL,<<0,0,0>>,MISSION_ATTACK,25,50,-1,70,60)
					//TASK_HELI_MISSION(vehicleData[iSelectedHeli].pindex[iDriverIndex], vehicleData[iSelectedHeli].vindex, NULL, MIKE_PED_ID(), (<<0,0,0>>), MISSION_ATTACK, 25,50,-1,70,60 )					

			
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists
ENDPROC


PROC TASK_HELI_DRIVEBY_PLAYER
	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedHeli].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedHeli].vindex)

/*
					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedHeli].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedHeli].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedHeli].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedHeli].vindex, TRUE, TRUE)
					IF IS_PED_IN_FLYING_VEHICLE(vehicleData[iSelectedHeli].pindex[iDriverIndex])
	
						SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedHeli].vindex)	
						
					ENDIF	// vehicle is a plane/helicopter
					
					// run over the bystander!!!
					SET_VEHICLE_FORWARD_SPEED(vehicleData[iSelectedHeli].vindex, fHeliSpeed)
					VECTOR offset = <<0.0, 0.0, 0.0>>
					TASK_HELI_CHASE(vehicleData[iSelectedHeli].pindex[iDriverIndex], GET_PLAYER_PED(GET_PLAYER_INDEX()), offset)
				*/	

					//IF NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(vehicleData[iSelectedHeli].pindex[iDriverIndex])		
										TASK_DRIVE_BY(vehicleData[iSelectedHeli].pindex[iDriverIndex], PLAYER_PED_ID(), null, <<0,0,0>>, 5000, 100, TRUE, FIRING_PATTERN_BURST_FIRE_HELI)
						//TASK_DRIVE_BY(vehicleData[iSelectedHeli].pindex[iDriverIndex], GET_PLAYER_PED(GET_PLAYER_INDEX()), NULL, <<0,0.0,0>>, 1000.0, 100, TRUE, FIRING_PATTERN_FULL_AUTO)
					//ENDIF					
										
					
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists
ENDPROC

PROC TELEPORT_TO_TESTING_AREA()
	
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
			SET_ENTITY_COORDS_NO_OFFSET(GET_PLAYER_PED(GET_PLAYER_INDEX()), VecTestingAreaLocation)
			
			VECTOR heliPosition
			
			GENERATE_HELI_LOCATION(heliPosition)
			
			IF DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].vindex)
			
				SET_ENTITY_COORDS_NO_OFFSET(vehicleData[iSelectedHeli].vindex, heliPosition)	
			
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists
	
ENDPROC	// TELEPORT_TO_TESTING_AREA

PROC CREATE_WIDGETS()

#IF IS_DEBUG_BUILD
	
	// create the widget
 	//WidgetID = 
	START_WIDGET_GROUP ("gtest_helicopter")
	
		START_NEW_WIDGET_COMBO()	
			ADD_TO_WIDGET_COMBO ("ANNIHILATOR")
			ADD_TO_WIDGET_COMBO ("BUZZARD")
			ADD_TO_WIDGET_COMBO ("HUNTER")
			ADD_TO_WIDGET_COMBO ("CARGOBOB")
			
        STOP_WIDGET_COMBO("Helicopter Type", iSelectedHelicopter)	
	
	
		ADD_WIDGET_BOOL ("Spawn Helicopter", bSpawn)
		ADD_WIDGET_BOOL ("Clean Up", bDelete)
		ADD_WIDGET_BOOL ("Teleport to Testing Area", bTeleportAllToTestingArea)
		ADD_WIDGET_BOOL ("Goto Player Location", bGotoPlayer)
		ADD_WIDGET_BOOL ("Goto Player And Land", bGotoPlayerAndLand)
		ADD_WIDGET_BOOL ("Combat Player", bCombatPlayer )
		ADD_WIDGET_BOOL ("Flee Player", bFleePlayer)
		ADD_WIDGET_BOOL ("Follow Player", bFollowPlayer)
		ADD_WIDGET_BOOL ("Circle Player", bCirclePlayer)
		ADD_WIDGET_BOOL ("Attack Player", bAttackPlayer)
		ADD_WIDGET_BOOL ("Attack Coord", bAttackCoord)
		ADD_WIDGET_BOOL ("Escort Player", bEscortPlayer)
		ADD_WIDGET_BOOL ("Protect Player", bProtectPlayer)
		ADD_WIDGET_BOOL ("Drive By", bDriveBy)
		
		ADD_WIDGET_FLOAT_SLIDER("Heli Speed", fHeliSpeed, 0.0, 200.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("Distance to Player", fRadius, 0.0, 200.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("Heli Height", fHeight, 0.0, 100.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("Heli Turbulence", fTurbulence, 0.0, 10.0, 0.1)
		ADD_WIDGET_INT_SLIDER("Num Passengers", iNumPassengers, 0, 4, 1)
				
		ADD_WIDGET_BOOL ("Toggle Rockets", bToggleRockets)
		ADD_WIDGET_BOOL ("Toggle HeightMap Following", bToggleHeightMapFollowing)
		ADD_WIDGET_BOOL ("Use Search Light", bUseSearchLight )
		ADD_WIDGET_BOOL ("Searchlight Always on", bSearchLightAlwaysOn)
		
		ADD_WIDGET_BOOL ("Check mission type", bQueryMissionType)
	
	STOP_WIDGET_GROUP()
	
#ENDIF
	
ENDPROC	//CREATE_WIDGETS

PROC PROCESS_WIDGETS()

	IF bSpawn
	
		bSpawn = FALSE
		CREATE_HELI()
		
	ENDIF	
	
	IF bDelete
	
		bDelete = FALSE
		DELETE_HELIS()
		
	ENDIF
		
	IF bTeleportAllToTestingArea
	
		bTeleportAllToTestingArea = FALSE
		TELEPORT_TO_TESTING_AREA()
		
	ENDIF	
	
	IF bGotoPlayer
	
		bGotoPlayer = FALSE
		TASK_HELI_GOTO_PLAYER()	
	ENDIF
	
	IF bGotoPlayerAndLand	
		bGotoPlayerAndLand = FALSE
		TASK_HELI_GOTO_PLAYER_AND_LAND()	
	ENDIF
	
	IF bCombatPlayer
	
		bCombatPlayer = FALSE
		TASK_HELI_COMBAT_PLAYER()	
	ENDIF	
	
	IF bFollowPlayer
	
		bFollowPlayer = FALSE
		TASK_HELI_CHASE_PLAYER()
		
	ENDIF
	
	IF  bFleePlayer
		bFleePlayer = FALSE
		
		TASK_HELI_FLEE_PLAYER()
		
	ENDIF
	
	IF bCirclePlayer
	
		bCirclePlayer = FALSE
		TASK_HELI_MISSION_CIRCLE()
		
	ENDIF	
	
	IF bDriveBy
		bDriveBy = FALSE
		TASK_HELI_DRIVEBY_PLAYER()
	
	ENDIF	
	
	IF bAttackPlayer
		bAttackPlayer = FALSE
		TASK_HELI_MISSION_ATTACK()	
	ENDIF	
	
	IF bAttackCoord
		bAttackCoord = FALSE
		TASK_HELI_ATTACK_COORD()
	ENDIF
	
	IF  bEscortPlayer
		bEscortPlayer = FALSE
		TASK_HELI_ESCORT_PLAYER()
	ENDIF
	
	IF bProtectPlayer
		bProtectPlayer = FALSE
		TASK_HELI_PROTECT_PLAYER()
	ENDIF 
	
	if bToggleHeightMapFollowing
		bToggleHeightMapFollowing = FALSE
		TOGGLE_HEIGHTMAP_FOLLOWING()
	ENDIF 
	
	IF bToggleRockets
		bToggleRockets = FALSE
		
		TOGGLE_HELI_ROCKETS()
	
	ENDIF 
	
	IF bUseSearchLight <> bUseSearchLightPreviousValue 
	
		bUseSearchLightPreviousValue = bUseSearchLight
		TOGGLE_HELI_SEARCHLIGHT()
	
	ENDIF	
	
	IF bQueryMissionType
		bQueryMissionType = FALSE
		GET_ACTIVE_VEHICLE_MISSION_TYPE(vehicleData[iSelectedHeli].vindex)		
	ENDIF
	
	IF fTurbulence<> fPrevioudTurbulence

		// do we have a vehicle?
		IF (DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedHeli].pindex[iDriverIndex]))
		
			// can it go fast-a?
			IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedHeli].vindex)	

				fPrevioudTurbulence = fTurbulence
				
				SET_HELI_TURBULENCE_SCALAR(vehicleData[iSelectedHeli].vindex, fTurbulence)

			ENDIF
		ENDIF
	
	ENDIF
	

ENDPROC	//PROCESS_WIDGETS

PROC PROCESS_DECORATORS()
	bExecute = FALSE	
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
		
			IF DECOR_EXIST_ON(GET_PLAYER_PED(GET_PLAYER_INDEX()), "rsne_testscript_active")													
				bExecute = DECOR_GET_BOOL(GET_PLAYER_PED(GET_PLAYER_INDEX()),"rsne_testscript_active")				
			ENDIF				
				
		ENDIF		
	ENDIF		
ENDPROC

SCRIPT

	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()

	CREATE_WIDGETS()

	WHILE bExecute

		PROCESS_DECORATORS()
		PROCESS_WIDGETS()

		// do we have a vehicle?
		IF (DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedHeli].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedHeli].pindex[iDriverIndex]))
		
			// can it go fast-a?
			IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedHeli].vindex)

				IS_HELI_LANDING_AREA_BLOCKED(vehicleData[iSelectedHeli].vindex)
				
			ENDIF
		ENDIF
		
		WAIT(0)
	
	ENDWHILE
	
	// Cleanup
	DELETE_HELIS()

ENDSCRIPT

#ENDIF  // IS_DEBUG_BUILD