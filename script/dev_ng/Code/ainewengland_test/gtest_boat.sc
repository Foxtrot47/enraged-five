//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	gtest_boat.sc											//
//		AUTHOR			:	Gareth												//
//		DESCRIPTION		:	This is a test script to test the functionality of a 	 	//
//							boatcopter in persuit of a player.							//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_path.sch"
USING "commands_ped.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_physics.sch"
USING "commands_decorator.sch"
USING "model_enums.sch"


// Data for the setup
//	-the boat to be used
//	-the driver to go into said car
STRUCT BoatVehicleData

	VEHICLE_INDEX vindex
	PED_INDEX pindex[5]
	
ENDSTRUCT


BoatVehicleData vehicleData[10]
int iSpawnedBoats = 0
int iSelectedBoat = 0
int iDriverIndex = 0
int iNumPassengers = 0

FLOAT fBoatSpeed 						= 20.0
FLOAT fRadius							= 15.0
FLOAT fHeight							= 0.0

BOOL bGotoPlayer						= FALSE
BOOL bGotoPlayerAnyMeans				= FALSE
BOOL bCombatPlayer						= FALSE
BOOL bFollowPlayer						= FALSE
BOOL bCirclePlayer						= FALSE
BOOL bWander							= FALSE
BOOL bFleePlayer						= FALSE
BOOL bBlockEvents						= true
bool bBlockEventsPreviousValue			= true
BOOL bDisableAvoidance					= FALSE
BOOL bDisableAvoidancePreviousValue		= FALSE


BOOL bSpawn 							= FALSE
BOOL bDelete 							= FALSE
BOOL bExecute 							= TRUE

MODEL_NAMES eDriverModel				= S_M_M_Pilot_02
MODEL_NAMES ePassengerModel				= S_F_Y_STRIPPER_01
//int iSelectedBoat = 0


//
//
PROC SET_BOAT_PED_COMBAT_AI(PED_INDEX pedId) 

	GIVE_DELAYED_WEAPON_TO_PED(pedId, WEAPONTYPE_SNIPERRIFLE, 300, TRUE) 
	SET_PED_COMBAT_ATTRIBUTES(pedId, CA_ALWAYS_FIGHT, TRUE) 
	SET_PED_COMBAT_ATTRIBUTES(pedId, CA_ALWAYS_FIGHT, FALSE) 

	SET_PED_COMBAT_ATTRIBUTES(pedId, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, true)


	SET_PED_COMBAT_MOVEMENT(pedId, CM_WILLADVANCE) 
	SET_PED_COMBAT_ABILITY(pedId, CAL_AVERAGE) 
	SET_PED_COMBAT_RANGE(pedId, CR_FAR) 
	SET_PED_TARGET_LOSS_RESPONSE(pedId, TLR_NEVER_LOSE_TARGET) 
	SET_PED_HIGHLY_PERCEPTIVE(pedId, TRUE) 
	SET_PED_CAN_BE_TARGETTED(pedId, TRUE) 
	SET_PED_SEEING_RANGE(pedId, 300+100.0) 

	// SET_PED_VISUAL_FIELD_MIN_ANGLE(pedId, -90.0) 
	// SET_PED_VISUAL_FIELD_MAX_ANGLE(pedId, 90.0) 
	// SET_PED_VISUAL_FIELD_MIN_ELEVATION_ANGLE(pedId, -90.0) 
	// SET_PED_VISUAL_FIELD_MAX_ELEVATION_ANGLE(pedId, 90.0) 
	SET_PED_VISUAL_FIELD_PERIPHERAL_RANGE(pedId, 400.0) 

	//SET_COMBAT_FLOAT(pedId, CCF_BOAT_SENSES_RANGE, 400.0) 
	
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedId, true)

	// Make peds a bit tougher. 
	SET_ENTITY_MAX_HEALTH(pedId, 250) 
	SET_ENTITY_HEALTH(pedId, 250) 
	SET_PED_ARMOUR(pedId, 250) 
	
	SET_PED_RELATIONSHIP_GROUP_HASH(pedId, RELGROUPHASH_PLAYER)	

ENDPROC



PROC DELETE_BOATS()
	
	INT i
	FOR i = 0 to iSpawnedBoats

		IF DOES_ENTITY_EXIST(vehicleData[i].vindex)			
			DELETE_VEHICLE(vehicleData[i].vindex)
		ENDIF
		
		int k
		FOR k = 0 TO 5 - 1
			IF DOES_ENTITY_EXIST(vehicleData[i].pindex[k])		
				DELETE_PED(vehicleData[i].pindex[k])
			ENDIF	
			
		ENDFOR		
	
	ENDFOR
	
	iSpawnedBoats = 0
ENDPROC

PROC GENERATE_BOAT_LOCATION(VECTOR& boatPosition)

			// grab the player's position for offsetting the scenario
			VECTOR playerPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			VECTOR playerForward = GET_ENTITY_FORWARD_VECTOR(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			Vector g_up = <<0, 0, 1.0>>
			boatPosition = playerPosition + ( playerForward * fRadius ) + (g_up * fHeight)	
	
ENDPROC	// GENERATE_BOAT_LOCATION

PROC CREATE_BOAT()

	MODEL_NAMES eBoatModel = DINGHY
	IF iSelectedBoat = 1
		eBoatModel = JETMAX
	ELIF iSelectedBoat = 2
		eBoatModel = SEASHARK

	ENDIF 

	// grab the car model
	REQUEST_MODEL(eBoatModel)
	
	// grab the driver model
	REQUEST_MODEL(eDriverModel) 
	
	REQUEST_MODEL(ePassengerModel)
		
	// load them
	WHILE NOT (HAS_MODEL_LOADED(eBoatModel) AND HAS_MODEL_LOADED(eDriverModel) AND HAS_MODEL_LOADED(ePassengerModel))
	
		WAIT(0)
		
	ENDWHILE	// models loaded
		
	// better have a player at this point
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
		


			FLOAT playerHeading = GET_ENTITY_HEADING(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
			FLOAT boatHeading = (playerHeading + 180 ) % 360
			
			VECTOR boatPosition						
			GENERATE_BOAT_LOCATION(boatPosition)
		
			// make the car and put in a driver
			vehicleData[iSpawnedBoats].vindex = CREATE_VEHICLE(
				eBoatModel,
				boatPosition, 
				boatHeading )
					
		
			IF DOES_ENTITY_EXIST(vehicleData[iSpawnedBoats].vindex)				
				
				SET_ENTITY_VISIBLE(vehicleData[iSpawnedBoats].vindex, TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehicleData[iSpawnedBoats].vindex, FALSE)

				vehicleData[iSpawnedBoats].pindex[iDriverIndex] = CREATE_PED_INSIDE_VEHICLE(
							vehicleData[iSpawnedBoats].vindex, 
							PEDTYPE_MISSION, 
							eDriverModel, 
							VS_DRIVER )
							
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(vehicleData[iSpawnedBoats].pindex[iDriverIndex], bBlockEvents)			
							

			int i = 0
			FOR i = 0 TO iNumPassengers - 1
				
				int passengerIndex = i + 1
				VEHICLE_SEAT seatIndex = INT_TO_ENUM(VEHICLE_SEAT, ENUM_TO_INT(VS_DRIVER)+ passengerIndex)
				IF IS_VEHICLE_SEAT_FREE(vehicleData[iSpawnedBoats].vindex, seatIndex  )				
			
						vehicleData[iSpawnedBoats].pindex[passengerIndex] = CREATE_PED_INSIDE_VEHICLE(
								vehicleData[iSpawnedBoats].vindex, 
								PEDTYPE_MISSION, 
								ePassengerModel, 
								seatIndex )		
								
								
						//SET_BOAT_PED_COMBAT_AI(vehicleData[iSpawnedBoats].pindex[passengerIndex])									
								
				ENDIF								
			
			ENDFOR
				
							
							
							
				iSelectedBoat = iSpawnedBoats			
				iSpawnedBoats++							
							
			ENDIF	// car and driver set up

		ENDIF	// player uninjured
		
	ENDIF	// player exists
	
ENDPROC	// CREATE_BOAT

PROC TASK_BOAT_WANDER()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedBoat].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedBoat].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedBoat].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedBoat].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedBoat].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedBoat].vindex, TRUE, TRUE)
								
					//TASK_VEHICLE_GOTO_NAVMESH(vehicleData[iSelectedBoat].pindex[iDriverIndex], vehicleData[iSelectedBoat].vindex, playerPosition, 20.0, DRIVINGMODE_PLOUGHTHROUGH, 5.0 )

					//TASK_GO_TO_COORD_ANY_MEANS(vehicleData[iSelectedboat].pindex[iDriverIndex], targetPosition, 20.0, vehicleData[iSelectedboat].vindex)
					TASK_VEHICLE_DRIVE_WANDER(vehicleData[iSelectedBoat].pindex[iDriverIndex], vehicleData[iSelectedBoat].vindex, 20.0, DRIVINGMODE_AVOIDCARS)
					//TASK_VEHICLE_MISSION_COORS_TARGET(vehicleData[iSelectedBoat].pindex[iDriverIndex], vehicleData[iSelectedBoat].vindex, playerPosition, MISSION_GOTO, 20.0, DRIVINGMODE_PLOUGHTHROUGH, 2.0, 0.0)					

					
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists

ENDPROC


PROC TASK_BOAT_COMBAT_PLAYER()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedBoat].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedBoat].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedBoat].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedBoat].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedBoat].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedBoat].vindex, TRUE, TRUE)
			

					int k
				
					FOR k = 0 TO 5 - 1
						IF DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].pindex[k])		
							TASK_COMBAT_PED(vehicleData[iSelectedBoat].pindex[k], PLAYER_PED_ID())
						ENDIF	
						
					ENDFOR					

					//TASK_BOAT_COMBAT_PLAYER()(vehicleData[iSelectedBoat].pindex[iDriverIndex], vehicleData[iSelectedBoat].vindex, NULL, PLAYER_PED_ID(), <<0.0,0.0,0.0>>, MISSION_GOTO, 20.0, 30.0, -1.0, 40, 40)					

					
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists

ENDPROC

PROC TASK_BOAT_FLEE_PLAYER()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedBoat].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedBoat].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedBoat].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedBoat].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedBoat].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedBoat].vindex, TRUE, TRUE)
			
					
					//TASK_VEHICLE_MISSION_PED_TARGET(vehicleData[iSelectedBoat].pindex[iDriverIndex], vehicleData[iSelectedBoat].vindex, GET_PLAYER_PED(GET_PLAYER_INDEX()), MISSION_FLEE, fBoatSpeed, DRIVINGMODE_PLOUGHTHROUGH, 2.0, 0.0)					

					//VECTOR targetPosition
					//VECTOR playerPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
					//VECTOR playerForward = GET_ENTITY_FORWARD_VECTOR(GET_PLAYER_PED(GET_PLAYER_INDEX()))
					//Vector g_up = <<0, 0, 1.0>>
					//targetPosition = playerPosition + ( playerForward * fRadius ) + (g_up * fHeight)	
					
					//TASK_VEHICLE_GOTO_NAVMESH(vehicleData[iSelectedBoat].pindex[iDriverIndex], vehicleData[iSelectedBoat].vindex, playerPosition, 20.0, DRIVINGMODE_PLOUGHTHROUGH, 5.0 )

					//TASK_GO_TO_COORD_ANY_MEANS(vehicleData[iSelectedboat].pindex[iDriverIndex], targetPosition, 20.0, vehicleData[iSelectedboat].vindex)
					
					//TASK_VEHICLE_MISSION_COORS_TARGET(vehicleData[iSelectedBoat].pindex[iDriverIndex], vehicleData[iSelectedBoat].vindex, targetPosition, MISSION_FLEE, 20.0, DRIVINGMODE_PLOUGHTHROUGH, 2.0, 0.0)					
					TASK_VEHICLE_MISSION_PED_TARGET(vehicleData[iSelectedBoat].pindex[iDriverIndex], vehicleData[iSelectedBoat].vindex, GET_PLAYER_PED(GET_PLAYER_INDEX()), MISSION_FLEE, 20.0, DRIVINGMODE_PLOUGHTHROUGH, 2.0, 0.0)					


					
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists

ENDPROC

PROC TASK_BOAT_GOTO_PLAYER_ANY_MEANS()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedBoat].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedBoat].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedBoat].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedBoat].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedBoat].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedBoat].vindex, TRUE, TRUE)
					
					VECTOR playerPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
					
					//TASK_VEHICLE_GOTO_NAVMESH(vehicleData[iSelectedBoat].pindex[iDriverIndex], vehicleData[iSelectedBoat].vindex, playerPosition, 20.0, DRIVINGMODE_PLOUGHTHROUGH, 5.0 )

					TASK_GO_TO_COORD_ANY_MEANS(vehicleData[iSelectedboat].pindex[iDriverIndex], playerPosition, 20.0, vehicleData[iSelectedboat].vindex)
					
					//TASK_VEHICLE_MISSION_COORS_TARGET(vehicleData[iSelectedBoat].pindex[iDriverIndex], vehicleData[iSelectedBoat].vindex, playerPosition, MISSION_GOTO, 20.0, DRIVINGMODE_PLOUGHTHROUGH, 2.0, 0.0)					

					
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists
ENDPROC

PROC TASK_BOAT_GOTO_PLAYER()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedBoat].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedBoat].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedBoat].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedBoat].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedBoat].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedBoat].vindex, TRUE, TRUE)
					
					VECTOR playerPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
					
					//TASK_VEHICLE_GOTO_NAVMESH(vehicleData[iSelectedBoat].pindex[iDriverIndex], vehicleData[iSelectedBoat].vindex, playerPosition, 20.0, DRIVINGMODE_PLOUGHTHROUGH, 5.0 )

					//TASK_GO_TO_COORD_ANY_MEANS(vehicleData[iSelectedboat].pindex[iDriverIndex], targetPosition, 20.0, vehicleData[iSelectedboat].vindex)
					
					
					//TASK_BOAT_MISSION(vehicleData[iSelectedBoat].pindex[iDriverIndex], vehicleData[iSelectedBoat].vindex, NULL, NULL, playerPosition, MISSION_GOTO
					//, 20.0, DRIVINGMODE_PLOUGHTHROUGH, 2.0, BCF_OPENOCEANSETTINGS  )					

					TASK_BOAT_MISSION(vehicleData[iSelectedBoat].pindex[iDriverIndex], vehicleData[iSelectedBoat].vindex, NULL, NULL, playerPosition, MISSION_GOTO
					, 20.0, DRIVINGMODE_PLOUGHTHROUGH, 2.0, BCF_DEFAULTSETTINGS  )										
					
					
					//TASK_VEHICLE_MISSION_COORS_TARGET(vehicleData[iSelectedBoat].pindex[iDriverIndex], vehicleData[iSelectedBoat].vindex, playerPosition, MISSION_GOTO, 20.0, DRIVINGMODE_PLOUGHTHROUGH, 2.0, 0.0)					

					
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists

ENDPROC

PROC ToggleBoatAvoidance()


	// do we have a vehicle?
	IF (DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedBoat].pindex[iDriverIndex]))
		IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedBoat].vindex)

			SET_BOAT_DISABLE_AVOIDANCE(vehicleData[iSelectedBoat].vindex, bDisableAvoidance)
			
		ENDIF	// vehicle is drivable
	ENDIF	// vehicle exists
			

ENDPROC


PROC TASK_BOAT_CHASE_PLAYER()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedBoat].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedBoat].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedBoat].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedBoat].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedBoat].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedBoat].vindex, TRUE, TRUE)

					TASK_VEHICLE_MISSION_PED_TARGET(vehicleData[iSelectedBoat].pindex[iDriverIndex], vehicleData[iSelectedBoat].vindex, PLAYER_PED_ID(), MISSION_FOLLOW, 20.0, DRIVINGMODE_PLOUGHTHROUGH, 50.0, 30.0)					

				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists

ENDPROC	// TASK_chase player

PROC TASK_BOAT_MISSION_CIRCLE()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedBoat].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedBoat].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedBoat].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedBoat].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedBoat].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedBoat].vindex, TRUE, TRUE)
				
					//TASK_VEHICLE_MISSION(vehicleData[iSelectedBoat].pindex[iDriverIndex], vehicleData[iSelectedBoat].vindex, NULL, GET_PLAYER_PED(GET_PLAYER_INDEX()), <<0,0,0>>, MISSION_CIRCLE, 20, 25, -1, 280, 10)

			
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists

ENDPROC	// TASK_BOAT_MISSION_CIRCLE

PROC TASK_BOAT_MISSION_ATTACK()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF (DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].vindex) AND DOES_ENTITY_EXIST(vehicleData[iSelectedBoat].pindex[iDriverIndex]) AND NOT IS_PED_INJURED(vehicleData[iSelectedBoat].pindex[iDriverIndex]))
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedBoat].vindex)

					//FREEZE_ENTITY_POSITION(vehicleData[iSelectedBoat].vindex, FALSE)
					SET_ENTITY_DYNAMIC(vehicleData[iSelectedBoat].vindex, TRUE)
					ACTIVATE_PHYSICS(vehicleData[iSelectedBoat].vindex)

					SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedBoat].vindex, TRUE, TRUE)

					SET_PED_ACCURACY(vehicleData[iSelectedBoat].pindex[iDriverIndex], 0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(vehicleData[iSelectedBoat].pindex[iDriverIndex], bBlockEvents)								
					
					TASK_VEHICLE_MISSION(vehicleData[iSelectedBoat].pindex[iDriverIndex], vehicleData[iSelectedBoat].vindex, GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(GET_PLAYER_INDEX())), GET_PLAYER_PED(GET_PLAYER_INDEX()), <<0,0,0>>, MISSION_ATTACK, 50, 50, -1, 10, 10)	
										
					//TASK_BOAT_MISSION(driver[7].ped,mission_veh[23].veh,mission_veh[0].veh,NULL,<<0,0,0>>,MISSION_ATTACK,25,50,-1,70,60)

			
				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists

ENDPROC	// TASK_BOAT_MISSION_CIRCLE


PROC CREATE_WIDGETS()

#IF IS_DEBUG_BUILD
	
	// create the widget
 	//WidgetID = 
	START_WIDGET_GROUP ("gtest_boat")
	

		START_NEW_WIDGET_COMBO()	
			ADD_TO_WIDGET_COMBO ("DINGHY")
			ADD_TO_WIDGET_COMBO ("JETMAX")
			ADD_TO_WIDGET_COMBO ("SEASHARK")
			
        STOP_WIDGET_COMBO("Boat Type", iSelectedBoat)	
	
	
		ADD_WIDGET_BOOL ("Spawn Boatcopter", bSpawn)
		ADD_WIDGET_BOOL ("Clean Up", bDelete)
		ADD_WIDGET_BOOL ("Goto Player", bGotoPlayer)
		ADD_WIDGET_BOOL ("Goto Player Any Means", bGotoPlayerAnyMeans)		
		ADD_WIDGET_BOOL ("Wander", bWander)
		ADD_WIDGET_BOOL ("Combat Player", bCombatPlayer)
		ADD_WIDGET_BOOL ("Follow Player", bFollowPlayer)
		ADD_WIDGET_BOOL ("Circle Player", bCirclePlayer)
		ADD_WIDGET_BOOL ("Flee Player", bFleePlayer)
		ADD_WIDGET_BOOL ("Block events", bBlockEvents)
		ADD_WIDGET_BOOL ("Disable Avoidance", bDisableAvoidance)
		
		ADD_WIDGET_FLOAT_SLIDER("Boat Speed", fBoatSpeed, 0.0, 200.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("Distance to Player", fRadius, 0.0, 200.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("Boat Height", fHeight, 0.0, 100.0, 1.0)
		ADD_WIDGET_INT_SLIDER("Num Passengers", iNumPassengers, 0, 4, 1)

	
	STOP_WIDGET_GROUP()
	
#ENDIF
	
ENDPROC	//CREATE_WIDGETS

PROC PROCESS_WIDGETS()

	IF bSpawn
	
		bSpawn = FALSE
		CREATE_BOAT()
		
	ENDIF	
	
	IF bDelete
	
		bDelete = FALSE
		DELETE_BOATS()
		
	ENDIF

	IF bWander
		bWander = false
		TASK_BOAT_WANDER()
	endif
		
	IF bGotoPlayer
	
		bGotoPlayer = FALSE
		TASK_BOAT_GOTO_PLAYER()	
	ENDIF
	
	IF bGotoPlayerAnyMeans
	
		bGotoPlayerAnyMeans = FALSE
		TASK_BOAT_GOTO_PLAYER_ANY_MEANS()	
	ENDIF	
	
	IF bCombatPlayer
	
		bCombatPlayer = FALSE
		TASK_BOAT_COMBAT_PLAYER()	
	ENDIF	
	
	IF bFollowPlayer
	
		bFollowPlayer = FALSE
		TASK_BOAT_CHASE_PLAYER()
		
	ENDIF
	
	IF  bFleePlayer 
		bFleePlayer = FALSE
		TASK_BOAT_FLEE_PLAYER()			

	ENDIF 
	
	
	IF bCirclePlayer
	
		bCirclePlayer = FALSE
		TASK_BOAT_MISSION_CIRCLE()
		
	ENDIF	
	
	if bBlockEvents <> bBlockEventsPreviousValue
	
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(vehicleData[iSelectedBoat].pindex[iDriverIndex], bBlockEvents)	
		bBlockEventsPreviousValue = bBlockEvents
	
	endif 
	
	if bDisableAvoidance <> bDisableAvoidancePreviousValue 
	
		bDisableAvoidancePreviousValue = bDisableAvoidance
		ToggleBoatAvoidance()

	
	ENDIF 
	

ENDPROC	//PROCESS_WIDGETS

PROC PROCESS_DECORATORS()
	bExecute = FALSE	
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
		
			IF DECOR_EXIST_ON(GET_PLAYER_PED(GET_PLAYER_INDEX()), "rsne_testscript_active")													
				bExecute = DECOR_GET_BOOL(GET_PLAYER_PED(GET_PLAYER_INDEX()),"rsne_testscript_active")				
			ENDIF				
				
		ENDIF		
	ENDIF		
ENDPROC

SCRIPT

	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()

	CREATE_WIDGETS()

	WHILE bExecute

		PROCESS_DECORATORS()
		PROCESS_WIDGETS()
		WAIT(0)
	
	ENDWHILE
	
	// Cleanup
	DELETE_BOATS()

ENDSCRIPT

#ENDIF  // IS_DEBUG_BUILD