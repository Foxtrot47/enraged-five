//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	test_NearlyMissedByCar.sc									//
//		AUTHOR			:	Justin Wheelock												//
//		DESCRIPTION		:	This is a test script to test the functionality of a ped 	//
//							diving away from an oncoming vehicle that won't actually 	//
//							hit.														//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_path.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_physics.sch"
USING "commands_decorator.sch"
USING "model_enums.sch"


// Data for the setup
//	-the car to be used
//	-the driver to go into said car
//	-the innocent bystander who will die a horrible horrible death
STRUCT HitAndRun

	VEHICLE_INDEX carIndex
	PED_INDEX driverIndex
	PED_INDEX innocentBystanderIndex
	
ENDSTRUCT

HitAndRun setupData
//WIDGET_GROUP_ID	WidgetID
VECTOR VecTestingAreaLocation			= <<1600, 3211, 40.5>>
FLOAT fDistanceFromCarToBystander 		= 5.0
FLOAT fCarSpeed 						= 20.0
FLOAT fBystanderDistanceFromCarEdge		= 0.2
FLOAT fCarRadius						= 1.25
FLOAT fPedHeading						= 180.0
FLOAT fVehicleHeading					= 0.0
FLOAT fDistanceBetweenBystanderAndCar	= 12.0
BOOL bStartTheKilling					= FALSE
BOOL bTeleportAllToTestingArea			= FALSE
BOOL bSpawn 							= FALSE
BOOL bDelete 							= FALSE
BOOL bExecute 							= TRUE
MODEL_NAMES eCarModel					= TAILGATER
MODEL_NAMES eDriverModel				= A_F_M_BEACH_01
MODEL_NAMES eBystanderModel				= A_F_M_BEACH_01
DRIVINGSTYLE dStyle						= DRIVINGSTYLE_NORMAL


PROC DELETE_NEAR_MISS_SCENARIO()

	// if the car exists, delete it
	IF DOES_ENTITY_EXIST(setupData.carIndex)	
	
		DELETE_VEHICLE(setupData.carIndex)
		
	ENDIF	// car no longer exists
	
	// if the driver of the car exists, delete it
	IF DOES_ENTITY_EXIST(setupData.driverIndex)		
	
		DELETE_PED(setupData.driverIndex)
		
	ENDIF	// driver no longer exists
	
	// if the bystander exists, delete it
	IF DOES_ENTITY_EXIST(setupData.innocentBystanderIndex)		
	
		DELETE_PED(setupData.innocentBystanderIndex)
		
	ENDIF	// bystander no longer exists

ENDPROC	// DELETE_HIT_AND_RUN_SCENARIO

PROC GENERATE_CAR_AND_BYSTANDER_LOCATIONS(VECTOR playerPosition, VECTOR& carPosition, VECTOR& bystanderPosition)

	// separate the vehicle and the bystander
	carPosition = playerPosition
	carPosition.x += fDistanceFromCarToBystander
	
	bystanderPosition = carPosition
	bystanderPosition.y += fDistanceBetweenBystanderAndCar
			
	// offset the bystander initially from car center
	bystanderPosition.x -= (fBystanderDistanceFromCarEdge + fCarRadius)

ENDPROC	// GENERATE_CAR_AND_BYSTANDER_LOCATIONS

PROC CREATE_NEAR_MISS_SCENARIO()

	// grab the car model
	REQUEST_MODEL(eCarModel)
	
	// grab the driver model
	REQUEST_MODEL(eDriverModel) 
	
	// grab the bystander model
	REQUEST_MODEL(eBystanderModel)
	
	// load them
	WHILE NOT (HAS_MODEL_LOADED(eCarModel) AND HAS_MODEL_LOADED(eDriverModel) AND HAS_MODEL_LOADED(eBystanderModel))
	
		WAIT(0)
		
	ENDWHILE	// models loaded
		
	// better have a player at this point
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
		
			// grab the player's position for offsetting the scenario
			VECTOR playerPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			VECTOR carPosition, bystanderPosition
			
			GENERATE_CAR_AND_BYSTANDER_LOCATIONS(playerPosition, carPosition, bystanderPosition)

			// make the car and put in a driver
			setupData.carIndex = CREATE_VEHICLE(
				eCarModel,
				carPosition, 
				fVehicleHeading )
					
		
			IF DOES_ENTITY_EXIST(setupData.carIndex)				
				
				SET_ENTITY_VISIBLE(setupData.carIndex, TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(setupData.carIndex, FALSE)

				setupData.driverIndex = CREATE_PED_INSIDE_VEHICLE(
							setupData.carIndex, 
							PEDTYPE_MISSION, 
							eDriverModel, 
							VS_DRIVER )
							
			ENDIF	// car and driver set up
			
			// make the bystander
			IF NOT DOES_ENTITY_EXIST(setupData.innocentBystanderIndex)
			
				setupData.innocentBystanderIndex = CREATE_PED(PEDTYPE_PROSTITUTE, eBystanderModel, bystanderPosition, fPedHeading)

			ENDIF	// bystander exists
			
		ENDIF	// player uninjured
		
	ENDIF	// player exists
	
ENDPROC	// CREATE_HIT_AND_RUN_SCENARIO

PROC TASK_DRIVER_MISS_BYSTANDER()

	// need a player, but could possibly be taken out?
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			// do we have a vehicle?
			IF DOES_ENTITY_EXIST(setupData.carIndex)
			
				// can it go fast-a?
				IF IS_VEHICLE_DRIVEABLE(setupData.carIndex)

					SET_ENTITY_DYNAMIC(setupData.carIndex, TRUE)
					ACTIVATE_PHYSICS(setupData.carIndex)
					
					// run over the bystander!!!
					SET_VEHICLE_FORWARD_SPEED(setupData.carIndex, fCarSpeed)

					SET_VEHICLE_ENGINE_ON(setupData.carIndex, TRUE, TRUE)

					IF DOES_ENTITY_EXIST(setupData.innocentBystanderIndex) AND DOES_ENTITY_EXIST(setupData.driverIndex)

						IF NOT (IS_PED_INJURED(setupData.innocentBystanderIndex) OR IS_PED_INJURED(setupData.driverIndex))
						
							// position of the bystander about to die!
							VECTOR driveToPosition = GET_ENTITY_COORDS(setupData.carIndex)
							driveToPosition.y += 2.0 * fDistanceBetweenBystanderAndCar

							TASK_VEHICLE_DRIVE_TO_COORD(
								setupData.driverIndex, 
								setupData.carIndex, 
								driveToPosition, 
								fCarSpeed, 
								dStyle, 
								GET_ENTITY_MODEL(setupData.carIndex), 
								DRIVINGMODE_PLOUGHTHROUGH,
								-1,
								-1 )

						ENDIF	// bystander is uninjured

					ENDIF	// we have a bystander

				ENDIF	// vehicle is drivable
				
			ENDIF	// vehicle exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists

ENDPROC	// TASK_DRIVER_SMASH_BYSTANDER

PROC TELEPORT_TO_TESTING_AREA()
	
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
			SET_ENTITY_COORDS_NO_OFFSET(GET_PLAYER_PED(GET_PLAYER_INDEX()), VecTestingAreaLocation)
			
			VECTOR carPosition, bystanderPosition
			
			GENERATE_CAR_AND_BYSTANDER_LOCATIONS(VecTestingAreaLocation, carPosition, bystanderPosition)
			
			IF DOES_ENTITY_EXIST(setupData.carIndex)
			
				SET_ENTITY_COORDS_NO_OFFSET(setupData.carIndex, carPosition)	
			
			ENDIF	// vehicle exists
			
			IF DOES_ENTITY_EXIST(setupData.innocentBystanderIndex)
			
				SET_ENTITY_COORDS_NO_OFFSET(setupData.innocentBystanderIndex, bystanderPosition)	
			
			ENDIF	// bystander exists
			
		ENDIF	// player is uninjured
		
	ENDIF	// player exists
	
ENDPROC	// TELEPORT_TO_TESTING_AREA

PROC CREATE_WIDGETS()

#IF IS_DEBUG_BUILD
	
	// create the widget
 	//WidgetID = 
	START_WIDGET_GROUP ("gtest_NearlyMissedByCar")
	
		ADD_WIDGET_BOOL ("Setup the Scenario", bSpawn)
		ADD_WIDGET_BOOL ("Clean Up", bDelete)
		ADD_WIDGET_BOOL ("Teleport to Testing Area", bTeleportAllToTestingArea)
		ADD_WIDGET_BOOL ("Start the Killing", bStartTheKilling)
		
		ADD_WIDGET_FLOAT_SLIDER("Car Speed", fCarSpeed, 0.0, 200.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("Distance to Bystander", fDistanceFromCarToBystander, 1.0, 200.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("Offset Bystander from Car Edge", fBystanderDistanceFromCarEdge, -10.0, 10.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Distance Between Bystander and Car", fDistanceBetweenBystanderAndCar, 1.0, 100.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("Bystander Heading", fPedHeading, -360.0, 360.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("Vehicle Heading", fVehicleHeading, -360.0, 360.0, 1.0)
	
	STOP_WIDGET_GROUP()
	
#ENDIF
	
ENDPROC	//CREATE_WIDGETS

PROC PROCESS_WIDGETS()

	IF bSpawn
	
		bSpawn = FALSE
		DELETE_NEAR_MISS_SCENARIO()
		CREATE_NEAR_MISS_SCENARIO()
		
	ENDIF	
	
	IF bDelete
	
		bDelete = FALSE
		DELETE_NEAR_MISS_SCENARIO()
		
	ENDIF
		
	IF bTeleportAllToTestingArea
	
		bTeleportAllToTestingArea = FALSE
		TELEPORT_TO_TESTING_AREA()
		
	ENDIF	
	
	IF bStartTheKilling
	
		bStartTheKilling = FALSE
		TASK_DRIVER_MISS_BYSTANDER()
		
	ENDIF	

ENDPROC	//PROCESS_WIDGETS

PROC PROCESS_DECORATORS()
	bExecute = FALSE	
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
		
			IF DECOR_EXIST_ON(GET_PLAYER_PED(GET_PLAYER_INDEX()), "rsne_testscript_active")													
				bExecute = DECOR_GET_BOOL(GET_PLAYER_PED(GET_PLAYER_INDEX()),"rsne_testscript_active")				
			ENDIF				
				
		ENDIF		
	ENDIF		
ENDPROC

SCRIPT

	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()

	CREATE_WIDGETS()

	WHILE bExecute

		PROCESS_DECORATORS()
		PROCESS_WIDGETS()
		WAIT(0)
	
	ENDWHILE
	
	// Cleanup
	DELETE_NEAR_MISS_SCENARIO()

ENDSCRIPT

#ENDIF  // IS_DEBUG_BUILD