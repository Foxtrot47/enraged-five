//////////////////////////////////////////////////////////////////////////////////////////
//											//
//		SCRIPT NAME		:	test_script.sc				//
//		AUTHOR			:	Test author				//
//		DESCRIPTION		:	Test desciption				//
//											//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_task.sch"

SCRIPT
	
	PRINTSTRING("\nTEST SCRIPT STARTING")PRINTNL()
	SUPPRESS_BREAKOUT_SCENARIO_EXITS_NEXT_FRAME()

ENDSCRIPT