#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF
 
#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_path.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_physics.sch"
USING "commands_decorator.sch"
USING "model_enums.sch"


// Data for the plane
STRUCT PlaneVehicleData
	VEHICLE_INDEX vindex
	PED_INDEX pindex
ENDSTRUCT

PlaneVehicleData vehicleData[10]
int iSpawnedPlanes = 0

FLOAT fPlaneHeight = 20.0
FLOAT fPlaneSpeed = 20.0
FLOAT fPlaneRadius = 20.0
FLOAT fOrientation = -1.0
BOOL bSpawn = FALSE
bool bDelete = FALSE
BOOL bTeleport = FALSE
bool bTaskChasePlayer = FALSE
bool bTaskChasePlane = FALSE
bool bTaskGotoPlayer = FALSE
BOOL bGotoPlayerAnyMeans = FALSE
bool bTaskPlaneLand = FALSE
bool bPlaneAttack = FALSE
bool bPlaneSequence = FALSE
bool bPlaneDriveOnGround = FALSE
bool bFleePlayer = FALSE
bool bLandingGear = true
bool bPreviousLandingGear = true


BOOL bExecute = TRUE
int iSpawnPlaneType = 0

int iPlaneMinHeightAboveTerrain = 20
int iPreviousPlaneMinHeightAboveTerrain = 20

int iSelectedPlane = 0
int iPreviousSelectedPlane = 20

int iChasePlane = 0
int iPreviousChasePlane = 20 
SEQUENCE_INDEX seqPlaneLeave

PROC GENERATE_PLANE_LOCATION(VECTOR& planePosition)

			// grab the player's position for offsetting the scenario
			VECTOR playerPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			VECTOR playerForward = GET_ENTITY_FORWARD_VECTOR(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			Vector g_up = <<0, 0, 1.0>>
			planePosition = playerPosition + ( playerForward * fPlaneRadius ) + (g_up * fPlaneHeight)	
	
ENDPROC	// GENERATE_HELI_LOCATION

PROC DELETE_PLANES()
	
	CLEAR_SEQUENCE_TASK(seqPlaneLeave)

	INT i
	FOR i = 0 to iSpawnedPlanes

		IF DOES_ENTITY_EXIST(vehicleData[i].vindex)			
			DELETE_VEHICLE(vehicleData[i].vindex)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehicleData[i].pindex)		
			DELETE_PED(vehicleData[i].pindex)
		ENDIF
	
	ENDFOR
	
	iSpawnedPlanes = 0
ENDPROC

PROC CREATE_PLANE()

	MODEL_NAMES model = CARGOPLANE 
	IF iSpawnPlaneType = 1
		model = CUBAN800
	ELIF iSpawnPlaneType = 2
		model = DUSTER
	ELIF iSpawnPlaneType = 3
		model = JET
	ELIF iSpawnPlaneType = 4
		model = LAZER	
	ELIF iSpawnPlaneType = 5
		model = LUXOR
	ELIF iSpawnPlaneType = 6
		model = MAMMATUS		
	ELIF iSpawnPlaneType = 7
		model = SHAMAL
	ELIF iSpawnPlaneType = 8
		model = STUNT			
	ELIF iSpawnPlaneType = 9
		model = TITAN
	ELIF iSpawnPlaneType = 10
		model = VELUM			

	ENDIF 

	MODEL_NAMES	pilot = S_M_Y_MARINE_01 //S_M_M_Pilot_02


	REQUEST_MODEL(model)
	REQUEST_MODEL(pilot) 
	while not (HAS_MODEL_LOADED(model) and HAS_MODEL_LOADED(pilot))
		WAIT(0)
	ENDWHILE
		
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
	
	
			FLOAT playerHeading = GET_ENTITY_HEADING(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
			FLOAT planeHeading = (playerHeading + 180 ) % 360
			
			VECTOR planePosition						
			GENERATE_PLANE_LOCATION(planePosition)
		
			// make the car and put in a driver
			vehicleData[iSpawnedplanes].vindex = CREATE_VEHICLE(
				model,
				planePosition, 
				planeHeading )	
	

			IF DOES_ENTITY_EXIST(vehicleData[iSpawnedPlanes].vindex)				
				
				SET_ENTITY_VISIBLE(vehicleData[iSpawnedPlanes].vindex, TRUE)
				FREEZE_ENTITY_POSITION(vehicleData[iSpawnedPlanes].vindex, TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehicleData[iSpawnedPlanes].vindex, FALSE)


				vehicleData[iSpawnedPlanes].pindex = CREATE_PED_INSIDE_VEHICLE(vehicleData[iSpawnedPlanes].vindex,PEDTYPE_MISSION,pilot,VS_DRIVER)

				iSelectedPlane = iSpawnedPlanes
				iSpawnedPlanes++
			ENDIF
			
		ENDIF
	
	ENDIF

ENDPROC

PROC TELEPORT_PLANE()
	
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
			VECTOR playerPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			playerPosition.z += fPlaneHeight	
			
			IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].vindex)
			
				SET_ENTITY_COORDS_NO_OFFSET(vehicleData[iSelectedPlane].vindex, playerPosition)	
			
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC
 
PROC TASK_PLANE_GOTO_PLAYER

	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
			VECTOR playerPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			playerPosition.z += fPlaneHeight	

			IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].vindex)
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedPlane].vindex)
				
					IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].pindex)	
						IF NOT IS_PED_INJURED(vehicleData[iSelectedPlane].pindex)	
					

							FREEZE_ENTITY_POSITION(vehicleData[iSelectedPlane].vindex, FALSE)
							SET_ENTITY_DYNAMIC(vehicleData[iSelectedPlane].vindex, TRUE)
							ACTIVATE_PHYSICS(vehicleData[iSelectedPlane].vindex)
							
							IF  fPlaneHeight > 2 
							
								SET_VEHICLE_FORWARD_SPEED(vehicleData[iSelectedPlane].vindex, fPlaneSpeed)
							ENDIF
							
							//TASK_GO_TO_COORD_ANY_MEANS(vehicleData[iSelectedPlane].pindex,playerPosition,PEDMOVEBLENDRATIO_RUN,GET_VEHICLE_PED_IS_IN(vehicleData[iSelectedPlane].pindex))

						
							SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedPlane].vindex, TRUE, TRUE)				
							SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedPlane].vindex)	
																	
							TASK_PLANE_MISSION(vehicleData[iSelectedPlane].pindex, vehicleData[iSelectedPlane].vindex
								, NULL, NULL, playerPosition
								, MISSION_GOTO, fPlaneSpeed, -1, fOrientation, iPlaneMinHeightAboveTerrain, iPreviousPlaneMinHeightAboveTerrain)									
							
						
							/*
							TASK_VEHICLE_DRIVE_TO_COORD(vehicleData[iSelectedPlane].pindex
								, vehicleData[iSelectedPlane].vindex
								, playerPosition
								, fPlaneSpeed
								, DRIVINGSTYLE_NORMAL
								, GET_ENTITY_MODEL(vehicleData[iSelectedPlane].vindex)
								, DRIVINGMODE_PLOUGHTHROUGH
								, -1
								, -1 )
							*/
							
						ENDIF
					ENDIF									
									
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC TASK_PLANE_GOTO_PLAYER_ANY_MEANS
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
			VECTOR playerPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			playerPosition.z += fPlaneHeight	

			IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].vindex)
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedPlane].vindex)
				
					IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].pindex)	
						IF NOT IS_PED_INJURED(vehicleData[iSelectedPlane].pindex)	
					

							FREEZE_ENTITY_POSITION(vehicleData[iSelectedPlane].vindex, FALSE)
							SET_ENTITY_DYNAMIC(vehicleData[iSelectedPlane].vindex, TRUE)
							ACTIVATE_PHYSICS(vehicleData[iSelectedPlane].vindex)
							
							IF  fPlaneHeight > 2 
							
								SET_VEHICLE_FORWARD_SPEED(vehicleData[iSelectedPlane].vindex, fPlaneSpeed)
							ENDIF
							
						
							SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedPlane].vindex, TRUE, TRUE)				
							SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedPlane].vindex)	
	
																	
							TASK_GO_TO_COORD_ANY_MEANS(vehicleData[iSelectedPlane].pindex,playerPosition,PEDMOVEBLENDRATIO_RUN,GET_VEHICLE_PED_IS_IN(vehicleData[iSelectedPlane].pindex))																					
								
						ENDIF
					ENDIF									
									
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


PROC TASK_PLANE_CHASE_PLAYER

	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
			IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].vindex)
				if IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedPlane].vindex)

					IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].pindex)	
						IF NOT IS_PED_INJURED(vehicleData[iSelectedPlane].pindex)	

							FREEZE_ENTITY_POSITION(vehicleData[iSelectedPlane].vindex, FALSE)
							SET_ENTITY_DYNAMIC(vehicleData[iSelectedPlane].vindex, TRUE)
							ACTIVATE_PHYSICS(vehicleData[iSelectedPlane].vindex)
							
							SET_VEHICLE_FORWARD_SPEED(vehicleData[iSelectedPlane].vindex, fPlaneSpeed)
							
							SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedPlane].vindex, TRUE, TRUE)				
							SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedPlane].vindex)						
							
							
							TASK_PLANE_CHASE(vehicleData[iSelectedPlane].pindex, GET_PLAYER_PED(GET_PLAYER_INDEX()), <<0,-20, 20>> )							
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


PROC TASK_PLANE_CHASE_PLANE

	IF DOES_ENTITY_EXIST(vehicleData[iChasePlane].pindex)	
		IF NOT IS_PED_INJURED(vehicleData[iChasePlane].pindex)	
	
			IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].vindex)
				if IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedPlane].vindex)

					IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].pindex)	
						IF NOT IS_PED_INJURED(vehicleData[iSelectedPlane].pindex)	

							FREEZE_ENTITY_POSITION(vehicleData[iSelectedPlane].vindex, FALSE)
							SET_ENTITY_DYNAMIC(vehicleData[iSelectedPlane].vindex, TRUE)
							ACTIVATE_PHYSICS(vehicleData[iSelectedPlane].vindex)
							
							SET_VEHICLE_FORWARD_SPEED(vehicleData[iSelectedPlane].vindex, fPlaneSpeed)
							
							SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedPlane].vindex, TRUE, TRUE)				
							SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedPlane].vindex)						
														
							TASK_PLANE_CHASE(vehicleData[iSelectedPlane].pindex, vehicleData[iChasePlane].pindex, <<0,-20, 20>> )							
														
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


PROC TEST_TASK_PLANE_ATTACK

	IF DOES_ENTITY_EXIST(vehicleData[iChasePlane].pindex)	
		IF NOT IS_PED_INJURED(vehicleData[iChasePlane].pindex)	
	
			IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].vindex)
				if IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedPlane].vindex)

					IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].pindex)	
						IF NOT IS_PED_INJURED(vehicleData[iSelectedPlane].pindex)	

							FREEZE_ENTITY_POSITION(vehicleData[iSelectedPlane].vindex, FALSE)
							SET_ENTITY_DYNAMIC(vehicleData[iSelectedPlane].vindex, TRUE)
							ACTIVATE_PHYSICS(vehicleData[iSelectedPlane].vindex)
							
							SET_VEHICLE_FORWARD_SPEED(vehicleData[iSelectedPlane].vindex, fPlaneSpeed)
							
							SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedPlane].vindex, TRUE, TRUE)				
							SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedPlane].vindex)						


							IF ( iChasePlane <> iSelectedPlane )
																										
								TASK_PLANE_MISSION(vehicleData[iSelectedPlane].pindex, vehicleData[iSelectedPlane].vindex
								, vehicleData[iChasePlane].vindex, vehicleData[iChasePlane].pindex, GET_ENTITY_COORDS(vehicleData[iChasePlane].pindex)
								, MISSION_ATTACK, fPlaneSpeed, -1, 30.0, 500, 50)		
							
							ELSE
							
								TASK_PLANE_MISSION(vehicleData[iSelectedPlane].pindex, vehicleData[iSelectedPlane].vindex
								, NULL, GET_PLAYER_PED(GET_PLAYER_INDEX()), GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
								, MISSION_ATTACK, fPlaneSpeed, -1, 30.0, 500, 50)		
							
							ENDIF
							
													
														
							//TASK_PLANE_CHASE(vehicleData[iSelectedPlane].pindex, vehicleData[iChasePlane].pindex, <<0,-20, 20>> )							
														
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC TEST_TASK_PLANE_FLEE_PLAYER()

	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	

			IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].vindex)
				if IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedPlane].vindex)

					IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].pindex)	
						IF NOT IS_PED_INJURED(vehicleData[iSelectedPlane].pindex)	

							FREEZE_ENTITY_POSITION(vehicleData[iSelectedPlane].vindex, FALSE)
							SET_ENTITY_DYNAMIC(vehicleData[iSelectedPlane].vindex, TRUE)
							ACTIVATE_PHYSICS(vehicleData[iSelectedPlane].vindex)
							
							SET_VEHICLE_FORWARD_SPEED(vehicleData[iSelectedPlane].vindex, fPlaneSpeed)
							
							SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedPlane].vindex, TRUE, TRUE)				
							SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedPlane].vindex)						

																								
							TASK_PLANE_MISSION(vehicleData[iSelectedPlane].pindex, vehicleData[iSelectedPlane].vindex
							, NULL, GET_PLAYER_PED(GET_PLAYER_INDEX()), GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
							, MISSION_FLEE, fPlaneSpeed, -1, 30.0, 500, 500)		
							
							
													
														
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC 

PROC TEST_TASK_PLANE_LAND()

	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
			IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].vindex)
				if IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedPlane].vindex)

					IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].pindex)	
						IF NOT IS_PED_INJURED(vehicleData[iSelectedPlane].pindex)	

							FREEZE_ENTITY_POSITION(vehicleData[iSelectedPlane].vindex, FALSE)
							SET_ENTITY_DYNAMIC(vehicleData[iSelectedPlane].vindex, TRUE)
							ACTIVATE_PHYSICS(vehicleData[iSelectedPlane].vindex)
							
							SET_VEHICLE_FORWARD_SPEED(vehicleData[iSelectedPlane].vindex, fPlaneSpeed)
							
							SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedPlane].vindex, TRUE, TRUE)				
							SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedPlane].vindex)						
									
							TASK_PLANE_LAND(vehicleData[iSelectedPlane].pindex, vehicleData[iSelectedPlane].vindex, <<1061.6,3071.7, 41.0>>, <<1703.5,3261.8,40.9>> )															
														
							//TASK_PLANE_MISSION(vehicleData.pindex,vehicleData.vindex,NULL,NULL,<<1600,3211,40.5>>,MISSION_LAND_AND_WAIT,30,5,294,40,0)							
																
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC TEST_TASK_PLANE_DRIVE_ON_GROUND()

	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
			VECTOR playerPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))	

			IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].vindex)
				IF IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedPlane].vindex)
				
					IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].pindex)	
						IF NOT IS_PED_INJURED(vehicleData[iSelectedPlane].pindex)	
					

							FREEZE_ENTITY_POSITION(vehicleData[iSelectedPlane].vindex, FALSE)
							SET_ENTITY_DYNAMIC(vehicleData[iSelectedPlane].vindex, TRUE)
							ACTIVATE_PHYSICS(vehicleData[iSelectedPlane].vindex)
											
							TASK_VEHICLE_DRIVE_TO_COORD(vehicleData[iSelectedPlane].pindex
								, vehicleData[iSelectedPlane].vindex
								, playerPosition
								, fPlaneSpeed
								, DRIVINGSTYLE_NORMAL
								, GET_ENTITY_MODEL(vehicleData[iSelectedPlane].vindex)
								, DF_PlaneTaxiMode | DF_PreferNavmeshRoute | DF_PreventBackgroundPathfinding
								, 2
								, 40 )
						
							SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedPlane].vindex, TRUE, TRUE)				
							SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedPlane].vindex)	
							
						ENDIF
					ENDIF									
									
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


PROC TEST_TASK_PLANE_SEQUENCE()

	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
	
			IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].vindex)
				if IS_VEHICLE_DRIVEABLE(vehicleData[iSelectedPlane].vindex)

					IF DOES_ENTITY_EXIST(vehicleData[iSelectedPlane].pindex)	
						IF NOT IS_PED_INJURED(vehicleData[iSelectedPlane].pindex)	

							FREEZE_ENTITY_POSITION(vehicleData[iSelectedPlane].vindex, FALSE)
							SET_ENTITY_DYNAMIC(vehicleData[iSelectedPlane].vindex, TRUE)
							ACTIVATE_PHYSICS(vehicleData[iSelectedPlane].vindex)
							
							SET_VEHICLE_FORWARD_SPEED(vehicleData[iSelectedPlane].vindex, fPlaneSpeed)
							
							SET_VEHICLE_ENGINE_ON(vehicleData[iSelectedPlane].vindex, TRUE, TRUE)				
							SET_HELI_BLADES_FULL_SPEED(vehicleData[iSelectedPlane].vindex)						

				
							VECTOR playerPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
							playerPosition.z += fPlaneHeight	
									
							OPEN_SEQUENCE_TASK(seqPlaneLeave)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehicleData[iSelectedPlane].vindex
								, playerPosition, fPlaneSpeed
								, DRIVINGSTYLE_NORMAL
								, GET_ENTITY_MODEL(vehicleData[iSelectedPlane].vindex)
								, DRIVINGMODE_PLOUGHTHROUGH
								, 17.5
								, -1) 
								
															
							TASK_VEHICLE_DRIVE_WANDER(NULL, vehicleData[iSelectedPlane].vindex, fPlaneSpeed, DRIVINGMODE_PLOUGHTHROUGH) //TASK_VEHICLE_TEMP_ACTION(NULL, NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), TEMPACT_GOFORWARD, 999999)
							CLOSE_SEQUENCE_TASK(seqPlaneLeave)		
							
							
							TASK_PERFORM_SEQUENCE(vehicleData[iSelectedPlane].pindex, seqPlaneLeave)							
																
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


PROC CREATE_WIDGETS()
 	START_WIDGET_GROUP ("gtest_airplane")
		ADD_WIDGET_INT_SLIDER("Selected Plane", iSelectedPlane, 0, 10, 1)
		START_NEW_WIDGET_COMBO()	
			ADD_TO_WIDGET_COMBO ("CARGOPLANE")
			ADD_TO_WIDGET_COMBO ("CUBAN800")
			ADD_TO_WIDGET_COMBO ("DUSTER")
			ADD_TO_WIDGET_COMBO ("JET")
			ADD_TO_WIDGET_COMBO ("LAZER")
			ADD_TO_WIDGET_COMBO ("LUXOR")		
			ADD_TO_WIDGET_COMBO ("MAMMATUS")
			ADD_TO_WIDGET_COMBO ("SHAMAL")
			ADD_TO_WIDGET_COMBO ("STUNT")			
			ADD_TO_WIDGET_COMBO ("TITAN")
			ADD_TO_WIDGET_COMBO ("VELUM")				
			
        STOP_WIDGET_COMBO("Plane Type", iSpawnPlaneType)
		ADD_WIDGET_BOOL ("Spawn", bSpawn)
		ADD_WIDGET_BOOL ("Delete", bDelete)
		ADD_WIDGET_BOOL ("Teleport", bTeleport)
		ADD_WIDGET_BOOL ("TaskGotoPlayer", bTaskGotoPlayer)
		ADD_WIDGET_BOOL ("TaskGotoPlayerAnyMeans", bGotoPlayerAnyMeans)
		ADD_WIDGET_BOOL ("TaskChasePlane", bTaskChasePlane)			
		ADD_WIDGET_INT_SLIDER("Selected Plane", iChasePlane, 0, 10, 1)		
		ADD_WIDGET_BOOL ("TaskChasePlayer", bTaskChasePlayer)
		ADD_WIDGET_BOOL ("TaskPlaneLand", bTaskPlaneLand)
		ADD_WIDGET_BOOL ("PlaneAttack", bPlaneAttack)
		ADD_WIDGET_BOOL ("PlaneSequence", bPlaneSequence)
		ADD_WIDGET_BOOL ("PlaneDriveOnGround", bPlaneDriveOnGround)
		ADD_WIDGET_BOOL ("FleePlayer", bFleePlayer)
		ADD_WIDGET_BOOL ("LandingGear", bLandingGear)
			

		ADD_WIDGET_FLOAT_SLIDER("PlaneHeight", fPlaneHeight, 0.0, 100.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("PlaneSpeed", fPlaneSpeed, 0.0, 200.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("fOrientation", fOrientation, -1.0, 360.0, 1.0)
		ADD_WIDGET_INT_SLIDER("MinHeightAboveTerrain", iPlaneMinHeightAboveTerrain, -100, 100, 1)
	STOP_WIDGET_GROUP()
ENDPROC

PROC PROCESS_WIDGETS()

	IF bSpawn
		bSpawn = FALSE
		CREATE_PLANE()
	ENDIF	
	
	IF bDelete
		bDelete = FALSE
		DELETE_PLANES()
	ENDIF
		
	IF bTeleport
		bTeleport = FALSE
		TELEPORT_PLANE()
	ENDIF	
	
	IF bTaskGotoPlayer
		bTaskGotoPlayer = FALSE
		TASK_PLANE_GOTO_PLAYER()
	ENDIF	
	
	IF bGotoPlayerAnyMeans
		bGotoPlayerAnyMeans = FALSE
		TASK_PLANE_GOTO_PLAYER_ANY_MEANS()
	ENDIF
	
	IF bTaskChasePlayer
		bTaskChasePlayer = FALSE
		TASK_PLANE_CHASE_PLAYER()
	ENDIF		
	
	IF bTaskChasePlane
		bTaskChasePlane = FALSE
		TASK_PLANE_CHASE_PLANE()
	ENDIF		
	
	IF bTaskPlaneLand
		bTaskPlaneLand = FALSE
		TEST_TASK_PLANE_LAND()
	
	ENDIF

	IF bPlaneAttack
		bPlaneAttack = FALSE
		TEST_TASK_PLANE_ATTACK()

	ENDIF	
	
	IF bPlaneSequence
		bPlaneSequence = FALSE
		TEST_TASK_PLANE_SEQUENCE()
	
	ENDIF 
	
	IF bPlaneDriveOnGround
		bPlaneDriveOnGround = FALSE
		TEST_TASK_PLANE_DRIVE_ON_GROUND()	
	ENDIF
	
	IF bFleePlayer
		bFleePlayer = FALSE
		TEST_TASK_PLANE_FLEE_PLAYER()
	ENDIF
	
	if bLandingGear <> bPreviousLandingGear
		bPreviousLandingGear = bLandingGear
		
		if bLandingGear
			CONTROL_LANDING_GEAR(vehicleData[iSelectedPlane].vindex, LGC_DEPLOY)
		else 
			CONTROL_LANDING_GEAR(vehicleData[iSelectedPlane].vindex, LGC_RETRACT)
		endif		
	
	Endif 
		
	if iSelectedPlane <> iPreviousSelectedPlane
		
		IF iSelectedPlane >= iSpawnedPlanes
			iSelectedPlane = iSpawnedPlanes-1	
		ENDIF

		IF iSelectedPlane < 0
			iSelectedPlane = 0
		ENDIF		
	
		iPreviousSelectedPlane = iSelectedPlane
	
	ENDIF
	
	if iChasePlane <> iPreviousChasePlane
		
		IF iChasePlane >= iSpawnedPlanes
			iChasePlane = iSpawnedPlanes-1	
		ENDIF

		IF iChasePlane = iSelectedPlane
			iChasePlane = iSelectedPlane-1	
		ENDIF

		IF iChasePlane < 0
			iChasePlane = 0
		ENDIF		
	
		iPreviousChasePlane = iChasePlane
	
	ENDIF	
	
	if iPlaneMinHeightAboveTerrain<> iPreviousPlaneMinHeightAboveTerrain
	
		iPreviousPlaneMinHeightAboveTerrain = iPlaneMinHeightAboveTerrain
		
		INT i
		FOR i = 0 to iSpawnedPlanes
		
		
			IF DOES_ENTITY_EXIST(vehicleData[i].vindex)
				IF IS_VEHICLE_DRIVEABLE(vehicleData[i].vindex)		
			
					SET_TASK_VEHICLE_GOTO_PLANE_MIN_HEIGHT_ABOVE_TERRAIN(vehicleData[i].vindex, iPlaneMinHeightAboveTerrain )
					
				ENDIF
			ENDIF		
		ENDFOR
	
	ENDIF
	

ENDPROC

PROC PROCESS_DECORATORS()
	bExecute = FALSE	
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(GET_PLAYER_INDEX()))	
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
		
			IF DECOR_EXIST_ON(GET_PLAYER_PED(GET_PLAYER_INDEX()), "rsne_testscript_active")													
				bExecute = DECOR_GET_BOOL(GET_PLAYER_PED(GET_PLAYER_INDEX()),"rsne_testscript_active")				
			ENDIF				
				
		ENDIF		
	ENDIF		
ENDPROC


SCRIPT

	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	CREATE_WIDGETS()

	WHILE bExecute	
		PROCESS_DECORATORS()	
		PROCESS_WIDGETS()
		WAIT(0)
	ENDWHILE

	DELETE_PLANES()

ENDSCRIPT

#ENDIF  // IS_DEBUG_BUILD