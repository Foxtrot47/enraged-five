// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

////////////////////////////////////////////////////////////////////////////////

// Notes: This is script is to allow easy setup of test scenarios for the code department.
// It works in conjunction with test_tools.sch
// It is a data driven system

////////////////////////////////////////////////////////////////////////////////

// Includes
USING "rage_builtins.sch"
USING "script_xml.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "commands_streaming.sch"
USING "commands_graphics.sch"
USING "commands_debug.sch"
USING "test_tools.sch"
USING "commands_vehicle.sch"
USING "commands_camera.sch"
USING "commands_player.sch"
USING "commands_physics.sch"
USING "script_DEBUG.sch"
USING "commands_event.sch"
USING "commands_script.sch"
USING "commands_entity.sch"
USING "commands_itemsets.sch"
USING "player_ped_public.sch"
USING "selector_public.sch"

////////////////////////////////////////////////////////////////////////////////

// Definitions

// These enums must match with those in /build/dev/common/data/script/xml/Testbed/TestSwitchMenu.xml
ENUM TESTS
	SWITCH_BETWEEN_IDLE_CHARACTERS 				= 0,
	SWITCH_BETWEEN_MOVING_CHARACTERS			= 1,
	SWITCH_BETWEEN_MOVING_AIMING_CHARACTERS		= 2,
	SWITCH_BETWEEN_AIMING_CHARACTERS			= 3,
	SWITCH_BETWEEN_COVER_CHARACTERS				= 4,
	SWITCH_BETWEEN_COMBAT_CHARACTERS			= 5,
	SWITCH_BETWEEN_DRIVING_CHARACTERS			= 6,
	SWITCH_BETWEEN_DRIVE_BY_CHARACTERS			= 7
ENDENUM

// Common states which can be reused across tests
ENUM GENERIC_TEST_STATE_ENUM
	STATE_INIT,
	STATE_1,
	STATE_2,
	STATE_3,
	STATE_4
ENDENUM

ENUM SWITCHING_STATE_ENUM
	STATE_SWITCH_INIT,
	STATE_SWITCH_PLAYING,
	STATE_SWITCH_SWITCHING
ENDENUM

// Move Blend Values
ENUM MOVE_BLEND_VALUE
	STILL,
	WALK,
	RUN,
	SPRINT
ENDENUM

ENUM WEAPON_FIRE_TYPES
	RANDOM_BURSTS,
	CONTINUOUS
ENDENUM

ENUM COMBAT_MOVEMENT_TYPES
	STATIONARY,
	DEFENSIVE,
	WILLADVANCE,
	WILLRETREAT
ENDENUM

////////////////////////////////////////////////////////////////////////////////
     
// Constants
CONST_INT MAX_NUMBER_OF_PLAYER_PEDS		3
CONST_INT MAX_NUMBER_OF_PEDS         	10
CONST_INT MAX_NUMBER_OF_VEHICLES     	10
CONST_INT MAX_NUMBER_OF_NODES        	7
CONST_INT MAX_NUMBER_OF_CAMERAS      	2
CONST_INT MAX_NUMBER_OF_OBJECTS 	 	1
CONST_INT MAX_NUMBER_OF_COVER_POINTS 	10
CONST_INT FixedCamera                	0
CONST_INT TrackingCamera             	1

////////////////////////////////////////////////////////////////////////////////

// Defaults values when the test is reset to factory the standard values are used

STRING XMLMenu = "Testbed/TestSwitchMenu"
VECTOR gvMapOffset  //offset all coords from the origin by the psoition of the mapin world coords

// Arrays of actors
peds_struct 		TestPeds[MAX_NUMBER_OF_PEDS]
vehicle_struct 		TestVehicles[MAX_NUMBER_OF_VEHICLES]
RouteStruct 		Route[MAX_NUMBER_OF_NODES]
StCameraData 		TestCams[MAX_NUMBER_OF_CAMERAS]
Object_struct 		TestObjects[MAX_NUMBER_OF_OBJECTS]
coverpoint_index 	CoverPoints[MAX_NUMBER_OF_COVER_POINTS]
INT					TestPedsTimers[MAX_NUMBER_OF_PEDS]
INT					TestPedsInVehicleTimes[MAX_NUMBER_OF_PEDS]
BOOL				TestPedsDriveVehicleFlags[MAX_NUMBER_OF_PEDS]
BOOL				TestPedsDriveByFlags[MAX_NUMBER_OF_PEDS]

// Stores the state of the test
GENERIC_TEST_STATE_ENUM TEST_STATE = STATE_INIT
SWITCHING_STATE_ENUM SWITCHING_STATE = STATE_SWITCH_INIT

INT MOVE_BLEND_TYPE 		= ENUM_TO_INT(WALK)
INT WEAPON_FIRE_TYPE 		= ENUM_TO_INT(RANDOM_BURSTS)
INT COMBAT_MOVEMENT_TYPE	= ENUM_TO_INT(STATIONARY)
SELECTOR_PED_STRUCT sSelectorPeds
SELECTOR_CAM_STRUCT sCamDetails
PED_INDEX playerpedindex
VECTOR playercoords

BOOL bAimShootToggle
BOOL bPeekAndAimToggle
BOOL bDriveVehicleToggle
BOOL bSwitchTaskGiven
BOOL bGiveSwitchTask

INT iTimeInCover = -1
INT iTimeInVehicle = -1
FLOAT fCruiseSpeed = 10.0

////////////////////////////////////////////////////////////////////////////////

 // Create Debug Rag Widgets 
PROC CREATE_TEST_WIDGET ()
    START_WIDGET_GROUP("Switch Test")
        SETUP_WIDGET ()
			START_NEW_WIDGET_COMBO() 
                ADD_TO_WIDGET_COMBO("STILL")
                ADD_TO_WIDGET_COMBO("WALK")
				ADD_TO_WIDGET_COMBO("RUN")
                ADD_TO_WIDGET_COMBO("SPRINT")
			STOP_WIDGET_COMBO("MOVE_BLEND_TYPE", MOVE_BLEND_TYPE)
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("RANDOM_BURSTS")
                ADD_TO_WIDGET_COMBO("CONTINUOUS")
			STOP_WIDGET_COMBO("WEAPON_FIRE_TYPE", WEAPON_FIRE_TYPE)
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("STATIONARY")
                ADD_TO_WIDGET_COMBO("DEFENSIVE")
				ADD_TO_WIDGET_COMBO("WILLADVANCE")
				ADD_TO_WIDGET_COMBO("WILLRETREAT")
			STOP_WIDGET_COMBO("COMBAT_MOVEMENT_TYPE", COMBAT_MOVEMENT_TYPE)
			ADD_WIDGET_BOOL("Aim/Shoot toggle", bAimShootToggle)
			ADD_WIDGET_BOOL("Peek & Aim toggle", bPeekAndAimToggle)
			ADD_WIDGET_BOOL("Drive Vehicle", bDriveVehicleToggle)
			ADD_WIDGET_INT_SLIDER("Time in cover", iTimeInCover, -1, 100000, 1000)
			ADD_WIDGET_INT_SLIDER("Time in vehicle", iTimeInVehicle, -1, 100000, 1000)
			ADD_WIDGET_FLOAT_SLIDER("Cruise speed", fCruiseSpeed, 0.0, 30.0, 1.0)
    STOP_WIDGET_GROUP()
ENDPROC

////////////////////////////////////////////////////////////////////////////////

//Mission flow House keeping

//PUPROSE: Removes all the scenarios and resets all the relationships
PROC Cleanup_Scenario_Entities ()
    int index = 0
        for index = 0 to MAX_NUMBER_OF_PEDS -1  
            CLEAN_UP_PED_ENTITIES (TestPeds[index].ped)
        ENDFOR 
        
        for index = 0 to MAX_NUMBER_OF_VEHICLES -1
            CLEAN_UP_VEHICLE_ENTITIES (TestVehicles[index].Vehicle )
        ENDFOR
        
        for index = 0 to MAX_NUMBER_OF_OBJECTS -1
            CLEAN_UP_OBJECT_ENTITIES (TestObjects[index].Object)
        ENDFOR

    Set_Gang_Relationships (FALSE)  
ENDPROC

PROC INITALISE_TEST_STATE()
    //reset each test
    SingleTaskStatus = startSingleTask
	TEST_STATE = STATE_INIT
    //end reset
ENDPROC

//END: Mission flow House keeping

////////////////////////////////////////////////////////////////////////////////

// Individual test setup

FUNC FLOAT GET_MOVE_BLEND_RATIO_FROM_WIDGETS()

	FLOAT RETURN_VALUE
	SWITCH INT_TO_ENUM(MOVE_BLEND_VALUE, MOVE_BLEND_TYPE)
		CASE STILL 		
			RETURN_VALUE = PEDMOVEBLENDRATIO_STILL 		
		BREAK
		CASE WALK
			RETURN_VALUE = PEDMOVEBLENDRATIO_WALK 	
		BREAK
		CASE RUN 	
			RETURN_VALUE = PEDMOVEBLENDRATIO_RUN 	
		BREAK
		CASE SPRINT 	
			RETURN_VALUE = PEDMOVEBLENDRATIO_SPRINT
		BREAK
	ENDSWITCH
	RETURN RETURN_VALUE
	
ENDFUNC

FUNC FIRING_TYPE GET_WEAPON_FIRE_TYPE_FROM_WIDGETS()

	SWITCH INT_TO_ENUM(WEAPON_FIRE_TYPES, WEAPON_FIRE_TYPE)
		CASE RANDOM_BURSTS 		
			RETURN FIRING_TYPE_RANDOM_BURSTS
		BREAK
		CASE CONTINUOUS
			RETURN FIRING_TYPE_CONTINUOUS	
		BREAK
	ENDSWITCH
	
	RETURN FIRING_TYPE_RANDOM_BURSTS
	
ENDFUNC

FUNC COMBAT_MOVEMENT GET_COMBAT_MOVEMENT_TYPE_FROM_WIDGETS()

	SWITCH INT_TO_ENUM(COMBAT_MOVEMENT_TYPES, COMBAT_MOVEMENT_TYPE)
		CASE STATIONARY
			RETURN CM_STATIONARY
		BREAK
		CASE DEFENSIVE
			RETURN CM_DEFENSIVE
		BREAK
		CASE WILLADVANCE
			RETURN CM_WILLADVANCE
		BREAK
		CASE WILLRETREAT
			RETURN CM_WILLRETREAT
		BREAK
	ENDSWITCH
	
	RETURN CM_STATIONARY

ENDFUNC

FUNC SELECTOR_SLOTS_ENUM GET_NEXT_PLAYER_SELECTOR()
	
	SWITCH ENUM_TO_INT(sSelectorPeds.eCurrentSelectorPed)
		CASE 0
			RETURN SELECTOR_PED_FRANKLIN
		BREAK
		CASE 1
			RETURN SELECTOR_PED_TREVOR
		BREAK
		CASE 2
			RETURN SELECTOR_PED_MICHAEL
		BREAK
	ENDSWITCH
	
	//Default to Michael if the player is injured.
	RETURN SELECTOR_PED_MICHAEL
ENDFUNC

FUNC INT GET_PREVIOUS_SELECTOR_PED_INT()

	SWITCH ENUM_TO_INT(sSelectorPeds.eCurrentSelectorPed)
		CASE 0
			RETURN 2
		BREAK
		CASE 1
			RETURN 0
		BREAK
		CASE 2
			RETURN 1
		BREAK
	ENDSWITCH
	
	RETURN 0

ENDFUNC

PROC PROCESS_BACKGROUND_SWITCHING()

	SWITCH SWITCHING_STATE
		
		CASE STATE_SWITCH_INIT		
			CLEANUP_SELECTOR_CAM(sCamDetails)
			sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = TestPeds[0].Ped
			sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = TestPeds[1].Ped
			sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = TestPeds[2].Ped	
			SWITCHING_STATE = STATE_SWITCH_PLAYING
		BREAK
		CASE STATE_SWITCH_PLAYING
			IF IS_BUTTON_PRESSED (pad1, DPADDOWN)		
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, GET_NEXT_PLAYER_SELECTOR())
				sCamDetails.pedTo = sSelectorPeds.pedID[GET_NEXT_PLAYER_SELECTOR()]
				bSwitchTaskGiven = FALSE
				SWITCHING_STATE = STATE_SWITCH_SWITCHING
			 ENDIF
		BREAK
		CASE STATE_SWITCH_SWITCHING
			
		 	IF NOT RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(sCamDetails, 0.0,0.0, SELECTOR_CAM_STRAIGHT_INTERP)
            	IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
					PRINTLN("Previous ped: ", GET_PREVIOUS_SELECTOR_PED_INT())
					bGiveSwitchTask = TRUE
					SWITCHING_STATE = STATE_SWITCH_INIT
            	ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// SWITCH BETWEEN IDLE CHARACTERS
PROC SETUP_SWITCH_BETWEEN_IDLE_CHARACTERS()

    TestPeds[0].PedsCoords = <<-3.59404, 75.58043, 6.35078>>
    TestPeds[0].PedHeading = 0
    TestPeds[0].Pedrelgrp = 2
   	TestPeds[0].PedModel = PLAYER_ZERO
    TestPeds[0].PedsWeapon = WEAPONTYPE_SMG
    TestPeds[0].PedcombatMove = 0
    TestPeds[0].pedcombatrange = 2
    TestPeds[0].combatcover = CA_USE_COVER
    TestPeds[0].bcombatcover = TRUE
	
	TestPeds[1].PedsCoords = <<-9.31440, 83.04971, 6.35078>>
    TestPeds[1].PedHeading = 0
    TestPeds[1].Pedrelgrp = 2
   	TestPeds[1].PedModel = PLAYER_ONE
    TestPeds[1].PedsWeapon = WEAPONTYPE_SMG
    TestPeds[1].PedcombatMove = 0
    TestPeds[1].pedcombatrange = 2
    TestPeds[1].combatcover = CA_USE_COVER
    TestPeds[1].bcombatcover = TRUE
		
	TestPeds[2].PedsCoords = <<-0.50841, 83.32625, 6.35078>>
    TestPeds[2].PedHeading = 0
    TestPeds[2].Pedrelgrp = 2
   	TestPeds[2].PedModel = PLAYER_TWO
    TestPeds[2].PedsWeapon = WEAPONTYPE_SMG
    TestPeds[2].PedcombatMove = 0
    TestPeds[2].pedcombatrange = 2
    TestPeds[2].combatcover = CA_USE_COVER
    TestPeds[2].bcombatcover = TRUE
	
	PlayerStartPos = <<-1.93147, 132.32083, 26.35070>>
	
ENDPROC

PROC INIT_SWITCH_BETWEEN_IDLE_CHARACTERS()

	SWITCHING_STATE = STATE_SWITCH_INIT
	g_savedGlobals.sFlow.isGameflowActive = FALSE
	PLAYER_INDEX playerindex = GET_PLAYER_INDEX()
    playerpedindex = GET_PLAYER_PED(playerindex)
	IF NOT IS_ENTITY_DEAD(playerpedindex)
		playercoords = GET_ENTITY_COORDS(playerpedindex)
		SET_ENTITY_COORDS(playerpedindex, <<-1.93147, 132.32083, 26.35070>>)
		SET_ENTITY_INVINCIBLE(playerpedindex, TRUE)
		CHANGE_PLAYER_PED(playerindex, TestPeds[0].Ped)		
	ENDIF
	
ENDPROC

PROC RUN_SWITCH_BETWEEN_IDLE_CHARACTERS()

	INT index
	
	PROCESS_BACKGROUND_SWITCHING()
	
	SWITCH TEST_STATE
		CASE STATE_INIT		
			
			FOR index = 0 to MAX_NUMBER_OF_PEDS -1  
				IF NOT IS_ENTITY_DEAD(TestPeds[index].ped)
	            	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[index].ped, TRUE)
				ENDIF
	        ENDFOR 
			
			TEST_STATE = STATE_1
		BREAK
		CASE STATE_1		
			TEST_STATE = STATE_2
		BREAK
		CASE STATE_2
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC CLEANUP_SWITCH_BETWEEN_IDLE_CHARACTERS()
	
	IF NOT IS_ENTITY_DEAD(playerpedindex)
		//SET_ENTITY_VISIBLE(scplayer, TRUE)
		SET_ENTITY_COORDS(playerpedindex, playercoords)
		SET_ENTITY_INVINCIBLE(playerpedindex, FALSE)
		PLAYER_INDEX playerindex = GET_PLAYER_INDEX()
		CHANGE_PLAYER_PED(playerindex, playerpedindex)		
	ENDIF
	
	INT index
	FOR index = 0 to MAX_NUMBER_OF_COVER_POINTS - 1
       	IF CoverPoints[index] != NULL
			REMOVE_COVER_POINT(CoverPoints[index])
			CoverPoints[index] = NULL
		ENDIF
    ENDFOR
	
ENDPROC
// END SWITCH BETWEEN IDLE CHARACTERS

// SWITCH BETWEEN AIMING CHARACTERS
PROC SETUP_SWITCH_BETWEEN_AIMING_CHARACTERS()

	TestPeds[0].PedsCoords = <<-54.73397, 140.24721, 6.37402>>
    TestPeds[0].PedHeading = 0
    TestPeds[0].Pedrelgrp = 2
   	TestPeds[0].PedModel = PLAYER_ZERO
    TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
    TestPeds[0].PedcombatMove = 0
    TestPeds[0].pedcombatrange = 2
    TestPeds[0].combatcover = CA_USE_COVER
    TestPeds[0].bcombatcover = TRUE
	
	TestPeds[1].PedsCoords = <<-55.91573, 143.67957, 6.38775>>
    TestPeds[1].PedHeading = 0
    TestPeds[1].Pedrelgrp = 2
   	TestPeds[1].PedModel = PLAYER_ONE
    TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
    TestPeds[1].PedcombatMove = 0
    TestPeds[1].pedcombatrange = 2
    TestPeds[1].combatcover = CA_USE_COVER
    TestPeds[1].bcombatcover = TRUE
		
	TestPeds[2].PedsCoords = <<-54.14495, 148.07845, 6.39171>>
    TestPeds[2].PedHeading = 0
    TestPeds[2].Pedrelgrp = 2
   	TestPeds[2].PedModel = PLAYER_TWO
    TestPeds[2].PedsWeapon = WEAPONTYPE_SAWNOFFSHOTGUN
    TestPeds[2].PedcombatMove = 0
    TestPeds[2].pedcombatrange = 2
    TestPeds[2].combatcover = CA_USE_COVER
    TestPeds[2].bcombatcover = TRUE
	
	PlayerStartPos = <<-1.93147, 132.32083, 26.35070>>

ENDPROC

PROC INIT_SWITCH_BETWEEN_AIMING_CHARACTERS()

	SWITCHING_STATE = STATE_SWITCH_INIT
	g_savedGlobals.sFlow.isGameflowActive = FALSE
	PLAYER_INDEX playerindex = GET_PLAYER_INDEX()
    playerpedindex = GET_PLAYER_PED(playerindex)
	IF NOT IS_ENTITY_DEAD(playerpedindex)
		playercoords = GET_ENTITY_COORDS(playerpedindex)
		SET_ENTITY_COORDS(playerpedindex, <<-1.93147, 132.32083, 26.35070>>)
		SET_ENTITY_INVINCIBLE(playerpedindex, TRUE)
		CHANGE_PLAYER_PED(playerindex, TestPeds[0].Ped)		
	ENDIF

ENDPROC

PROC RUN_SWITCH_BETWEEN_AIMING_CHARACTERS()

	INT index
	VECTOR vAimPositions[3]
	
	vAimPositions[0] = << -62.26797, 140.38333, 7.69567 >>
	vAimPositions[1] = << -62.26797, 144.29927, 7.80600 >>
	vAimPositions[2] = << -62.86958, 150.31885, 10.84060 >>
	
	PROCESS_BACKGROUND_SWITCHING()
	
	SWITCH TEST_STATE
		CASE STATE_INIT		
			
			FOR index = 0 to MAX_NUMBER_OF_PLAYER_PEDS -1  
				IF NOT IS_ENTITY_DEAD(TestPeds[index].ped)
	            	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[index].ped, TRUE)
					TASK_AIM_GUN_AT_COORD(TestPeds[index].Ped, vAimPositions[index], -1, TRUE)
				ENDIF
	        ENDFOR 
			
			TEST_STATE = STATE_1
		BREAK
		CASE STATE_1		
			
			IF ( SWITCHING_STATE = STATE_SWITCH_SWITCHING )
			
				IF ( bSwitchTaskGiven = FALSE )
				
						INT 		iPreviousPed
						WEAPON_TYPE eCurrentWeapon
						
						iPreviousPed = ENUM_TO_INT(sSelectorPeds.eCurrentSelectorPed)
						
						IF NOT GET_CURRENT_PED_WEAPON(TestPeds[iPreviousPed].ped, eCurrentWeapon)
							GIVE_WEAPON_TO_PED(TestPeds[iPreviousPed].ped, TestPeds[iPreviousPed].PedsWeapon, INFINITE_AMMO, TRUE, TRUE)
						ENDIF
					
						IF ( bAimShootToggle = TRUE )
							TASK_SHOOT_AT_COORD(TestPeds[iPreviousPed].ped, vAimPositions[iPreviousPed], -1, GET_WEAPON_FIRE_TYPE_FROM_WIDGETS())
						ELSE
							TASK_AIM_GUN_AT_COORD(TestPeds[iPreviousPed].ped, vAimPositions[iPreviousPed], -1, FALSE)
						ENDIF
					
					bSwitchTaskGiven = TRUE
					
				ENDIF
			
			ENDIF
			
		BREAK
		CASE STATE_2
		BREAK
		
	ENDSWITCH

ENDPROC

PROC CLEANUP_SWITCH_BETWEEN_AIMING_CHARACTERS()

	IF NOT IS_ENTITY_DEAD(playerpedindex)
		//SET_ENTITY_VISIBLE(scplayer, TRUE)
		SET_ENTITY_COORDS(playerpedindex, playercoords)
		SET_ENTITY_INVINCIBLE(playerpedindex, FALSE)
		PLAYER_INDEX playerindex = GET_PLAYER_INDEX()
		CHANGE_PLAYER_PED(playerindex, playerpedindex)		
	ENDIF
	
	INT index
	FOR index = 0 to MAX_NUMBER_OF_COVER_POINTS - 1
       	IF CoverPoints[index] != NULL
			REMOVE_COVER_POINT(CoverPoints[index])
			CoverPoints[index] = NULL
		ENDIF
    ENDFOR

ENDPROC
// END SWITCH BETWEEN AIMING CHARACTERS

//SWITCH BETWEEN MOVING CHARACTERS
PROC SETUP_SWITCH_BETWEEN_MOVING_CHARACTERS()

	TestPeds[0].PedsCoords = <<-57.01456, 87.40016, 6.35125>>
    TestPeds[0].PedHeading = -90.0
    TestPeds[0].Pedrelgrp = 2
   	TestPeds[0].PedModel = PLAYER_ZERO
    TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
    TestPeds[0].PedcombatMove = 0
    TestPeds[0].pedcombatrange = 2
    TestPeds[0].combatcover = CA_USE_COVER
    TestPeds[0].bcombatcover = TRUE
	
	TestPeds[1].PedsCoords = <<-58.92701, 82.48219, 6.35125>>
    TestPeds[1].PedHeading = -90.0
    TestPeds[1].Pedrelgrp = 2
   	TestPeds[1].PedModel = PLAYER_ONE
    TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
    TestPeds[1].PedcombatMove = 0
    TestPeds[1].pedcombatrange = 2
    TestPeds[1].combatcover = CA_USE_COVER
    TestPeds[1].bcombatcover = TRUE
		
	TestPeds[2].PedsCoords = <<-56.05682, 78.47922, 6.35125>>
    TestPeds[2].PedHeading = -90.0
    TestPeds[2].Pedrelgrp = 2
   	TestPeds[2].PedModel = PLAYER_TWO
    TestPeds[2].PedsWeapon = WEAPONTYPE_SAWNOFFSHOTGUN
    TestPeds[2].PedcombatMove = 0
    TestPeds[2].pedcombatrange = 2
    TestPeds[2].combatcover = CA_USE_COVER
    TestPeds[2].bcombatcover = TRUE
	
	PlayerStartPos = <<-1.93147, 132.32083, 26.35070>>

ENDPROC

PROC INIT_SWITCH_BETWEEN_MOVING_CHARACTERS()

	SWITCHING_STATE = STATE_SWITCH_INIT
	g_savedGlobals.sFlow.isGameflowActive = FALSE
	PLAYER_INDEX playerindex = GET_PLAYER_INDEX()
    playerpedindex = GET_PLAYER_PED(playerindex)
	IF NOT IS_ENTITY_DEAD(playerpedindex)
		playercoords = GET_ENTITY_COORDS(playerpedindex)
		SET_ENTITY_COORDS(playerpedindex, <<-1.93147, 132.32083, 26.35070>>)
		SET_ENTITY_INVINCIBLE(playerpedindex, TRUE)
		CHANGE_PLAYER_PED(playerindex, TestPeds[0].Ped)		
	ENDIF

ENDPROC

PROC RUN_SWITCH_BETWEEN_MOVING_CHARACTERS()

	INT index
	VECTOR vDestinations[3]
	
	vDestinations[0] = << -18.92424, 87.39049, 6.35078 >>
	vDestinations[1] = << -17.06908, 82.44601, 6.35077 >>
	vDestinations[2] = << -19.07478, 78.38323, 6.35078 >>
	
	PROCESS_BACKGROUND_SWITCHING()
	
	SWITCH TEST_STATE
		CASE STATE_INIT		
			
			FOR index = 0 to MAX_NUMBER_OF_PLAYER_PEDS - 1  
				IF NOT IS_ENTITY_DEAD(TestPeds[index].ped)
					IF ( TestPeds[index].ped <> PLAYER_PED_ID()  )
		            	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[index].ped, TRUE)
						
						SEQUENCE_INDEX SequenceIndex
						
						SequenceIndex = NULL
						
						OPEN_SEQUENCE_TASK(SequenceIndex)														  
							TASK_GO_TO_COORD_ANY_MEANS(NULL, vDestinations[index], GET_MOVE_BLEND_RATIO_FROM_WIDGETS(), NULL)
							TASK_GO_TO_COORD_ANY_MEANS(NULL, TestPeds[index].PedsCoords, GET_MOVE_BLEND_RATIO_FROM_WIDGETS(), NULL)
							SET_SEQUENCE_TO_REPEAT(SequenceIndex, REPEAT_FOREVER)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						TASK_PERFORM_SEQUENCE(TestPeds[index].ped, SequenceIndex)
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						
					ENDIF
				ENDIF
	        ENDFOR 
			
			TEST_STATE = STATE_1
		BREAK
		CASE STATE_1		
			
			IF ( SWITCHING_STATE = STATE_SWITCH_SWITCHING )
			
				IF ( bSwitchTaskGiven = FALSE )
				
					INT 		iPreviousPed
					WEAPON_TYPE eCurrentWeapon
					
					iPreviousPed = ENUM_TO_INT(sSelectorPeds.eCurrentSelectorPed)
					
					IF NOT GET_CURRENT_PED_WEAPON(TestPeds[iPreviousPed].ped, eCurrentWeapon)
						GIVE_WEAPON_TO_PED(TestPeds[iPreviousPed].ped, TestPeds[iPreviousPed].PedsWeapon, INFINITE_AMMO, TRUE, TRUE)
					ENDIF
				
					SEQUENCE_INDEX SequenceIndex
					OPEN_SEQUENCE_TASK(SequenceIndex)												  
						TASK_GO_TO_COORD_ANY_MEANS(NULL, vDestinations[iPreviousPed], GET_MOVE_BLEND_RATIO_FROM_WIDGETS(), NULL)
						TASK_GO_TO_COORD_ANY_MEANS(NULL, TestPeds[iPreviousPed].PedsCoords, GET_MOVE_BLEND_RATIO_FROM_WIDGETS(), NULL)
						SET_SEQUENCE_TO_REPEAT(SequenceIndex, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(SequenceIndex)
					TASK_PERFORM_SEQUENCE(TestPeds[iPreviousPed].ped, SequenceIndex)
					CLEAR_SEQUENCE_TASK(SequenceIndex)
			
					bSwitchTaskGiven = TRUE
			
				ENDIF
				
			ENDIF
			
		BREAK
		CASE STATE_2
		BREAK
		
	ENDSWITCH

ENDPROC

PROC CLEANUP_SWITCH_BETWEEN_MOVING_CHARACTERS()

	IF NOT IS_ENTITY_DEAD(playerpedindex)
		//SET_ENTITY_VISIBLE(scplayer, TRUE)
		SET_ENTITY_COORDS(playerpedindex, playercoords)
		SET_ENTITY_INVINCIBLE(playerpedindex, FALSE)
		PLAYER_INDEX playerindex = GET_PLAYER_INDEX()
		CHANGE_PLAYER_PED(playerindex, playerpedindex)		
	ENDIF
	
	INT index
	FOR index = 0 to MAX_NUMBER_OF_COVER_POINTS - 1
       	IF CoverPoints[index] != NULL
			REMOVE_COVER_POINT(CoverPoints[index])
			CoverPoints[index] = NULL
		ENDIF
    ENDFOR

ENDPROC
//END SWITCH BETWEEN MOVING CHARACTERS

//SWITCH BETWEEN MOVING AIMING CHARACTERS
PROC SETUP_SWITCH_BETWEEN_MOVING_AIMING_CHARACTERS()

	TestPeds[0].PedsCoords = <<-57.01456, 87.40016, 6.35125>>
    TestPeds[0].PedHeading = -90.0
    TestPeds[0].Pedrelgrp = 2
   	TestPeds[0].PedModel = PLAYER_ZERO
    TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
    TestPeds[0].PedcombatMove = 0
    TestPeds[0].pedcombatrange = 2
    TestPeds[0].combatcover = CA_USE_COVER
    TestPeds[0].bcombatcover = TRUE
	
	TestPeds[1].PedsCoords = <<-58.92701, 82.48219, 6.35125>>
    TestPeds[1].PedHeading = -90.0
    TestPeds[1].Pedrelgrp = 2
   	TestPeds[1].PedModel = PLAYER_ONE
    TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
    TestPeds[1].PedcombatMove = 0
    TestPeds[1].pedcombatrange = 2
    TestPeds[1].combatcover = CA_USE_COVER
    TestPeds[1].bcombatcover = TRUE
		
	TestPeds[2].PedsCoords = <<-56.05682, 78.47922, 6.35125>>
    TestPeds[2].PedHeading = -90.0
    TestPeds[2].Pedrelgrp = 2
   	TestPeds[2].PedModel = PLAYER_TWO
    TestPeds[2].PedsWeapon = WEAPONTYPE_SAWNOFFSHOTGUN
    TestPeds[2].PedcombatMove = 0
    TestPeds[2].pedcombatrange = 2
    TestPeds[2].combatcover = CA_USE_COVER
    TestPeds[2].bcombatcover = TRUE
	
	//ped to aim at
	TestPeds[3].PedsCoords = <<-10.88313, 74.56373, 6.35078>>
    TestPeds[3].PedHeading = 0
    TestPeds[3].Pedrelgrp = 1
   	TestPeds[3].PedModel = S_M_Y_CLOWN_01
    TestPeds[3].PedsWeapon = WEAPONTYPE_PISTOL
    TestPeds[3].PedcombatMove = 0
    TestPeds[3].pedcombatrange = 2
    TestPeds[3].combatcover = CA_USE_COVER
    TestPeds[3].bcombatcover = TRUE
	
	TestPeds[4].PedsCoords = <<-63.00739, 75.44943, 6.35126>>
    TestPeds[4].PedHeading = 0
    TestPeds[4].Pedrelgrp = 1
   	TestPeds[4].PedModel = S_M_Y_CLOWN_01
    TestPeds[4].PedsWeapon = WEAPONTYPE_PISTOL
    TestPeds[4].PedcombatMove = 0
    TestPeds[4].pedcombatrange = 2
    TestPeds[4].combatcover = CA_USE_COVER
    TestPeds[4].bcombatcover = TRUE
	
	PlayerStartPos = <<-1.93147, 132.32083, 26.35070>>

ENDPROC

PROC INIT_SWITCH_BETWEEN_MOVING_AIMING_CHARACTERS()

	SWITCHING_STATE = STATE_SWITCH_INIT
	g_savedGlobals.sFlow.isGameflowActive = FALSE
	PLAYER_INDEX playerindex = GET_PLAYER_INDEX()
    playerpedindex = GET_PLAYER_PED(playerindex)
	IF NOT IS_ENTITY_DEAD(playerpedindex)
		playercoords = GET_ENTITY_COORDS(playerpedindex)
		SET_ENTITY_COORDS(playerpedindex, <<-1.93147, 132.32083, 26.35070>>)
		SET_ENTITY_INVINCIBLE(playerpedindex, TRUE)
		CHANGE_PLAYER_PED(playerindex, TestPeds[0].Ped)		
	ENDIF
	
	INT i
	
	FOR i = 3 TO 4
	
		IF DOES_ENTITY_EXIST(TestPeds[i].Ped)
			IF NOT IS_ENTITY_DEAD(TestPeds[i].Ped)
				SET_ENTITY_HEALTH(TestPeds[i].Ped, 800)
				SET_ENTITY_INVINCIBLE(TestPeds[i].Ped, TRUE)
				SET_PED_CAN_RAGDOLL(TestPeds[i].Ped, FALSE)
				SET_ENTITY_PROOFS(TestPeds[i].Ped, TRUE, TRUE, TRUE, TRUE, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[i].Ped, TRUE)
			ENDIF
		ENDIF
		
	ENDFOR

ENDPROC

PROC RUN_SWITCH_BETWEEN_MOVING_AIMING_CHARACTERS()

	INT index
	VECTOR vDestinations[5]
	
	vDestinations[0] = << -18.92424, 87.39049, 6.35078 >>
	vDestinations[1] = << -17.06908, 82.44601, 6.35077 >>
	vDestinations[2] = << -19.07478, 78.38323, 6.35078 >>
	vDestinations[3] = << -11.04686, 94.40188, 6.35078 >>
	vDestinations[4] = << -63.03859, 94.58267, 6.35125 >>
	
	PROCESS_BACKGROUND_SWITCHING()
	
	SWITCH TEST_STATE
		CASE STATE_INIT		
			
			FOR index = 0 TO MAX_NUMBER_OF_PLAYER_PEDS - 1  
				IF NOT IS_ENTITY_DEAD(TestPeds[index].ped)
					IF ( TestPeds[index].ped <> PLAYER_PED_ID()  )
		            	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[index].ped, TRUE)
						
						SEQUENCE_INDEX SequenceIndex
						
						SequenceIndex = NULL
						
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, vDestinations[index], TestPeds[3].Ped,
																	GET_MOVE_BLEND_RATIO_FROM_WIDGETS(), bAimShootToggle)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, TestPeds[index].PedsCoords, TestPeds[4].Ped,
																	GET_MOVE_BLEND_RATIO_FROM_WIDGETS(), bAimShootToggle)
							SET_SEQUENCE_TO_REPEAT(SequenceIndex, REPEAT_FOREVER)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						TASK_PERFORM_SEQUENCE(TestPeds[index].ped, SequenceIndex)
						CLEAR_SEQUENCE_TASK(SequenceIndex)

					ENDIF
				ENDIF
	        ENDFOR
			
			FOR index = 3 TO 4
			
				//setup task for first aim target entity
				IF NOT IS_ENTITY_DEAD(TestPeds[index].Ped)
					SEQUENCE_INDEX SequenceIndex
							
					SequenceIndex = NULL
					
					OPEN_SEQUENCE_TASK(SequenceIndex)														  
						TASK_GO_TO_COORD_ANY_MEANS(NULL, vDestinations[index], PEDMOVE_RUN, NULL)
						TASK_GO_TO_COORD_ANY_MEANS(NULL, TestPeds[index].PedsCoords, PEDMOVE_RUN, NULL)
						SET_SEQUENCE_TO_REPEAT(SequenceIndex, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(SequenceIndex)
					TASK_PERFORM_SEQUENCE(TestPeds[index].Ped, SequenceIndex)
					CLEAR_SEQUENCE_TASK(SequenceIndex)
				ENDIF
				
			ENDFOR
			

			TEST_STATE = STATE_1
		BREAK
		CASE STATE_1		
			
			IF ( SWITCHING_STATE = STATE_SWITCH_SWITCHING )
			
				IF ( bSwitchTaskGiven = FALSE )
			
					INT 		iPreviousPed
					WEAPON_TYPE eCurrentWeapon
					
					iPreviousPed = ENUM_TO_INT(sSelectorPeds.eCurrentSelectorPed)
					
					IF NOT GET_CURRENT_PED_WEAPON(TestPeds[iPreviousPed].ped, eCurrentWeapon)
						GIVE_WEAPON_TO_PED(TestPeds[iPreviousPed].ped, TestPeds[iPreviousPed].PedsWeapon, INFINITE_AMMO, TRUE, TRUE)
					ENDIF
					
					SEQUENCE_INDEX SequenceIndex
					OPEN_SEQUENCE_TASK(SequenceIndex)
					
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, vDestinations[iPreviousPed], TestPeds[3].Ped,
																GET_MOVE_BLEND_RATIO_FROM_WIDGETS(), bAimShootToggle)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, TestPeds[iPreviousPed].PedsCoords, TestPeds[4].Ped,
																GET_MOVE_BLEND_RATIO_FROM_WIDGETS(), bAimShootToggle)
						SET_SEQUENCE_TO_REPEAT(SequenceIndex, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(SequenceIndex)
					TASK_PERFORM_SEQUENCE(TestPeds[iPreviousPed].ped, SequenceIndex)
					CLEAR_SEQUENCE_TASK(SequenceIndex)
			
					bSwitchTaskGiven = TRUE
			
				ENDIF
			
			ENDIF
			
		BREAK
		CASE STATE_2
		BREAK
		
	ENDSWITCH

ENDPROC

PROC CLEANUP_SWITCH_BETWEEN_MOVING_AIMING_CHARACTERS()

	IF NOT IS_ENTITY_DEAD(playerpedindex)
		//SET_ENTITY_VISIBLE(scplayer, TRUE)
		SET_ENTITY_COORDS(playerpedindex, playercoords)
		SET_ENTITY_INVINCIBLE(playerpedindex, FALSE)
		PLAYER_INDEX playerindex = GET_PLAYER_INDEX()
		CHANGE_PLAYER_PED(playerindex, playerpedindex)		
	ENDIF
	
	INT index
	FOR index = 0 to MAX_NUMBER_OF_COVER_POINTS - 1
       	IF CoverPoints[index] != NULL
			REMOVE_COVER_POINT(CoverPoints[index])
			CoverPoints[index] = NULL
		ENDIF
    ENDFOR

ENDPROC
//END SWITCH BETWEEN MOVING AIMING CHARACTERS

//SWITCH BETWEEN COVER CHARACTERS
PROC SETUP_SWITCH_BETWEEN_COVER_CHARACTERS()

	TestPeds[0].PedsCoords = <<33.51446, 192.52225, 6.35078>>
    TestPeds[0].PedHeading = 0.0
    TestPeds[0].Pedrelgrp = 2
   	TestPeds[0].PedModel = PLAYER_ZERO
    TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
    TestPeds[0].PedcombatMove = 0
    TestPeds[0].pedcombatrange = 2
    TestPeds[0].combatcover = CA_USE_COVER
    TestPeds[0].bcombatcover = TRUE
	
	TestPeds[1].PedsCoords = <<39.05818, 190.46242, 6.35078>>
    TestPeds[1].PedHeading = 0.0
    TestPeds[1].Pedrelgrp = 2
   	TestPeds[1].PedModel = PLAYER_ONE
    TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
    TestPeds[1].PedcombatMove = 0
    TestPeds[1].pedcombatrange = 2
    TestPeds[1].combatcover = CA_USE_COVER
    TestPeds[1].bcombatcover = TRUE
		
	TestPeds[2].PedsCoords = <<43.00849, 187.46425, 6.35078>>
    TestPeds[2].PedHeading = 0.0
    TestPeds[2].Pedrelgrp = 2
   	TestPeds[2].PedModel = PLAYER_TWO
    TestPeds[2].PedsWeapon = WEAPONTYPE_SAWNOFFSHOTGUN
    TestPeds[2].PedcombatMove = 0
    TestPeds[2].pedcombatrange = 2
    TestPeds[2].combatcover = CA_USE_COVER
    TestPeds[2].bcombatcover = TRUE
	
	PlayerStartPos = <<-1.93147, 132.32083, 26.35070>>
	
	//cover points
	CoverPoints[0] = ADD_COVER_POINT(<< 33.62334, 206.45764, 6.35078 >>, 0.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_180)
	CoverPoints[1] = ADD_COVER_POINT(<< 39.02701, 201.73619, 6.35078 >>, 0.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_180)
	CoverPoints[2] = ADD_COVER_POINT(<< 43.09028, 201.86171, 6.35078 >>, 0.0, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180)

ENDPROC

PROC INIT_SWITCH_BETWEEN_COVER_CHARACTERS()

	SWITCHING_STATE = STATE_SWITCH_INIT
	g_savedGlobals.sFlow.isGameflowActive = FALSE
	PLAYER_INDEX playerindex = GET_PLAYER_INDEX()
    playerpedindex = GET_PLAYER_PED(playerindex)
	IF NOT IS_ENTITY_DEAD(playerpedindex)
		playercoords = GET_ENTITY_COORDS(playerpedindex)
		SET_ENTITY_COORDS(playerpedindex, <<-1.93147, 132.32083, 26.35070>>)
		SET_ENTITY_INVINCIBLE(playerpedindex, TRUE)
		CHANGE_PLAYER_PED(playerindex, TestPeds[0].Ped)		
	ENDIF

ENDPROC

PROC RUN_SWITCH_BETWEEN_COVER_CHARACTERS()

	INT index
	
	PROCESS_BACKGROUND_SWITCHING()
	
	SWITCH TEST_STATE
		CASE STATE_INIT		
			
			FOR index = 0 to MAX_NUMBER_OF_PLAYER_PEDS - 1  
				IF NOT IS_ENTITY_DEAD(TestPeds[index].ped)
					IF ( TestPeds[index].ped <> PLAYER_PED_ID()  )
		            	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[index].ped, TRUE)
						
						SEQUENCE_INDEX SequenceIndex
						
						SequenceIndex = NULL
						
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_SEEK_COVER_TO_COVER_POINT(NULL, CoverPoints[index], << 37.81361, 211.82549, 7.19992 >>, iTimeInCover, bPeekAndAimToggle)
							TASK_EXIT_COVER(NULL, AIMING_COVER_EXIT, << 37.81361, 211.82549, 7.19992 >>)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, TestPeds[index].PedsCoords, << 37.81361, 211.82549, 7.19992 >>,
																   GET_MOVE_BLEND_RATIO_FROM_WIDGETS(), bAimShootToggle)
							SET_SEQUENCE_TO_REPEAT(SequenceIndex, REPEAT_FOREVER)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						TASK_PERFORM_SEQUENCE(TestPeds[index].ped, SequenceIndex)
						CLEAR_SEQUENCE_TASK(SequenceIndex)

					ENDIF
				ENDIF
	        ENDFOR 
			
			TEST_STATE = STATE_1
		BREAK
		CASE STATE_1		
			
			IF ( SWITCHING_STATE = STATE_SWITCH_SWITCHING )
			
				IF ( bSwitchTaskGiven = FALSE )
			
					INT 		iPreviousPed
					WEAPON_TYPE eCurrentWeapon
					
					iPreviousPed = ENUM_TO_INT(sSelectorPeds.eCurrentSelectorPed)
					
					IF NOT GET_CURRENT_PED_WEAPON(TestPeds[iPreviousPed].ped, eCurrentWeapon)
						GIVE_WEAPON_TO_PED(TestPeds[iPreviousPed].ped, TestPeds[iPreviousPed].PedsWeapon, INFINITE_AMMO, TRUE, TRUE)
					ENDIF

					SEQUENCE_INDEX SequenceIndex
					OPEN_SEQUENCE_TASK(SequenceIndex)
						TASK_SEEK_COVER_TO_COVER_POINT(NULL, CoverPoints[iPreviousPed], << 37.81361, 211.82549, 7.19992 >>, iTimeInCover, bPeekAndAimToggle)
						TASK_EXIT_COVER(NULL, AIMING_COVER_EXIT, << 37.81361, 211.82549, 7.19992 >>)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, TestPeds[iPreviousPed].PedsCoords, << 37.81361, 211.82549, 7.19992 >>,
															   GET_MOVE_BLEND_RATIO_FROM_WIDGETS(), bAimShootToggle)
						SET_SEQUENCE_TO_REPEAT(SequenceIndex, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(SequenceIndex)
					TASK_PERFORM_SEQUENCE(TestPeds[iPreviousPed].ped, SequenceIndex)
					CLEAR_SEQUENCE_TASK(SequenceIndex)

					bSwitchTaskGiven = TRUE
					
				ENDIF
			
			ENDIF
			
		BREAK
		CASE STATE_2
		BREAK
		
	ENDSWITCH

ENDPROC

PROC CLEANUP_SWITCH_BETWEEN_COVER_CHARACTERS()

	IF NOT IS_ENTITY_DEAD(playerpedindex)
		//SET_ENTITY_VISIBLE(scplayer, TRUE)
		SET_ENTITY_COORDS(playerpedindex, playercoords)
		SET_ENTITY_INVINCIBLE(playerpedindex, FALSE)
		PLAYER_INDEX playerindex = GET_PLAYER_INDEX()
		CHANGE_PLAYER_PED(playerindex, playerpedindex)		
	ENDIF
	
	INT index
	FOR index = 0 to MAX_NUMBER_OF_COVER_POINTS - 1
       	IF CoverPoints[index] != NULL
			REMOVE_COVER_POINT(CoverPoints[index])
			CoverPoints[index] = NULL
		ENDIF
    ENDFOR

ENDPROC
//END SWITCH BETWEEN COVER CHARACTERS

//SWITCH BETWEEN COMBAT CHARACTERS
PROC SETUP_SWITCH_BETWEEN_COMBAT_CHARACTERS()

	TestPeds[0].PedsCoords = <<33.51446, 192.52225, 6.35078>>
    TestPeds[0].PedHeading = 0.0
    TestPeds[0].Pedrelgrp = 2
   	TestPeds[0].PedModel = PLAYER_ZERO
    TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
    TestPeds[0].PedcombatMove = 0
    TestPeds[0].pedcombatrange = 2
    TestPeds[0].combatcover = CA_USE_COVER
    TestPeds[0].bcombatcover = TRUE
	
	TestPeds[1].PedsCoords = <<39.05818, 190.46242, 6.35078>>
    TestPeds[1].PedHeading = 0.0
    TestPeds[1].Pedrelgrp = 2
   	TestPeds[1].PedModel = PLAYER_ONE
    TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
    TestPeds[1].PedcombatMove = 0
    TestPeds[1].pedcombatrange = 2
    TestPeds[1].combatcover = CA_USE_COVER
    TestPeds[1].bcombatcover = TRUE
		
	TestPeds[2].PedsCoords = <<43.00849, 187.46425, 6.35078>>
    TestPeds[2].PedHeading = 0.0
    TestPeds[2].Pedrelgrp = 2
   	TestPeds[2].PedModel = PLAYER_TWO
    TestPeds[2].PedsWeapon = WEAPONTYPE_SAWNOFFSHOTGUN
    TestPeds[2].PedcombatMove = 0
    TestPeds[2].pedcombatrange = 2
    TestPeds[2].combatcover = CA_USE_COVER
    TestPeds[2].bcombatcover = TRUE
	
	PlayerStartPos = <<-1.93147, 132.32083, 26.35070>>
	
	//enemies
	TestPeds[3].PedsCoords = <<27.11506, 218.43642, 6.35078>>
    TestPeds[3].PedHeading = -180.0
    TestPeds[3].Pedrelgrp = 1
   	TestPeds[3].PedModel = S_M_Y_COP_01
    TestPeds[3].PedsWeapon = WEAPONTYPE_PISTOL
    TestPeds[3].PedcombatMove = 0
    TestPeds[3].pedcombatrange = 2
    TestPeds[3].combatcover = CA_USE_COVER
    TestPeds[3].bcombatcover = TRUE
	
	TestPeds[4].PedsCoords = <<40.91220, 216.43761, 6.35078>>
    TestPeds[4].PedHeading = -180.0
    TestPeds[4].Pedrelgrp = 1
   	TestPeds[4].PedModel = S_M_Y_COP_01
    TestPeds[4].PedsWeapon = WEAPONTYPE_PISTOL
    TestPeds[4].PedcombatMove = 0
    TestPeds[4].pedcombatrange = 2
    TestPeds[4].combatcover = CA_USE_COVER
    TestPeds[4].bcombatcover = TRUE

ENDPROC

PROC INIT_SWITCH_BETWEEN_COMBAT_CHARACTERS()

	SWITCHING_STATE = STATE_SWITCH_INIT
	g_savedGlobals.sFlow.isGameflowActive = FALSE
	PLAYER_INDEX playerindex = GET_PLAYER_INDEX()
    playerpedindex = GET_PLAYER_PED(playerindex)
	IF NOT IS_ENTITY_DEAD(playerpedindex)
		playercoords = GET_ENTITY_COORDS(playerpedindex)
		SET_ENTITY_COORDS(playerpedindex, <<-1.93147, 132.32083, 26.35070>>)
		SET_ENTITY_INVINCIBLE(playerpedindex, TRUE)
		CHANGE_PLAYER_PED(playerindex, TestPeds[0].Ped)		
	ENDIF
	
	INT i
	
	FOR i = 0 TO 4
	
		IF DOES_ENTITY_EXIST(TestPeds[i].Ped)
			IF NOT IS_ENTITY_DEAD(TestPeds[i].Ped)
				SET_ENTITY_HEALTH(TestPeds[i].Ped, 800)
				SET_ENTITY_INVINCIBLE(TestPeds[i].Ped, TRUE)
				SET_ENTITY_PROOFS(TestPeds[i].Ped, TRUE, TRUE, TRUE, TRUE, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[i].Ped, TRUE)
			ENDIF
		ENDIF
		
	ENDFOR
	
	//relationship groups settings
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, RELGROUPHASH_COP)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, RELGROUPHASH_COP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_COP, RELGROUPHASH_PLAYER)

ENDPROC

PROC RUN_SWITCH_BETWEEN_COMBAT_CHARACTERS()

	INT 	index
	VECTOR 	vDefensiveAreaPositions[5]
	FLOAT	fDefensiveAreaRadii[5]
	
	//buddies
	vDefensiveAreaPositions[0] 	= <<33.61008, 206.04863, 6.35078>>
	vDefensiveAreaPositions[1] 	= <<39.65670, 211.45782, 6.35078>>
	vDefensiveAreaPositions[2] 	= <<43.10796, 201.61380, 6.35078>>
	
	//enemies
	vDefensiveAreaPositions[3] 	= <<27.77337, 217.76309, 6.35078>>
	vDefensiveAreaPositions[4] 	= <<41.22980, 215.85138, 6.35078>>
	
	//buddies
	fDefensiveAreaRadii[0]		= 3.0
	fDefensiveAreaRadii[1]		= 3.0
	fDefensiveAreaRadii[2]		= 3.0
	
	//enemies
	fDefensiveAreaRadii[3]		= 15.0
	fDefensiveAreaRadii[4]		= 15.0
	
	PROCESS_BACKGROUND_SWITCHING()
	
	SWITCH TEST_STATE
		CASE STATE_INIT		
			
			FOR index = 0 to MAX_NUMBER_OF_PLAYER_PEDS - 1  
				IF NOT IS_ENTITY_DEAD(TestPeds[index].ped)
					IF ( TestPeds[index].ped <> PLAYER_PED_ID()  )
		            	SET_PED_SPHERE_DEFENSIVE_AREA(TestPeds[index].Ped, vDefensiveAreaPositions[index], fDefensiveAreaRadii[index])
						SET_PED_COMBAT_MOVEMENT(TestPeds[index].Ped, GET_COMBAT_MOVEMENT_TYPE_FROM_WIDGETS())
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(TestPeds[index].Ped, 100.0)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[index].ped, TRUE)
					ENDIF
				ENDIF
	        ENDFOR
			
			FOR index = 3 TO 4
				IF NOT IS_ENTITY_DEAD(TestPeds[index].ped)
	            	SET_PED_SPHERE_DEFENSIVE_AREA(TestPeds[index].Ped, vDefensiveAreaPositions[index], fDefensiveAreaRadii[index])
					SET_PED_COMBAT_MOVEMENT(TestPeds[index].Ped, CM_DEFENSIVE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(TestPeds[index].Ped, 100.0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[index].ped, FALSE)
				ENDIF
			ENDFOR
			
			TEST_STATE = STATE_1
		BREAK
		CASE STATE_1		
			
			IF ( SWITCHING_STATE = STATE_SWITCH_SWITCHING )
			
				IF ( bSwitchTaskGiven = FALSE )
			
					INT 		iPreviousPed
					WEAPON_TYPE eCurrentWeapon
					
					iPreviousPed = ENUM_TO_INT(sSelectorPeds.eCurrentSelectorPed)
					
					IF NOT GET_CURRENT_PED_WEAPON(TestPeds[iPreviousPed].ped, eCurrentWeapon)
						GIVE_WEAPON_TO_PED(TestPeds[iPreviousPed].ped, TestPeds[iPreviousPed].PedsWeapon, INFINITE_AMMO, TRUE, TRUE)
					ENDIF
					
					SET_ENTITY_HEALTH(TestPeds[iPreviousPed].ped, 800)
					SET_ENTITY_INVINCIBLE(TestPeds[iPreviousPed].ped, TRUE)
					SET_ENTITY_PROOFS(TestPeds[iPreviousPed].ped, TRUE, TRUE, TRUE, TRUE, TRUE)
					
					SET_PED_SPHERE_DEFENSIVE_AREA(TestPeds[iPreviousPed].Ped, vDefensiveAreaPositions[iPreviousPed], fDefensiveAreaRadii[iPreviousPed])
					SET_PED_COMBAT_MOVEMENT(TestPeds[iPreviousPed].Ped, GET_COMBAT_MOVEMENT_TYPE_FROM_WIDGETS())
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(TestPeds[iPreviousPed].Ped, 100.0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[iPreviousPed].ped, TRUE)

					bSwitchTaskGiven = TRUE
					
				ENDIF

			ENDIF
			
			IF ( bGiveSwitchTask = TRUE )
			
				INT 		iPreviousPed
				WEAPON_TYPE eCurrentWeapon
				
				iPreviousPed = GET_PREVIOUS_SELECTOR_PED_INT()
				
				IF NOT GET_CURRENT_PED_WEAPON(TestPeds[iPreviousPed].ped, eCurrentWeapon)
					GIVE_WEAPON_TO_PED(TestPeds[iPreviousPed].ped, TestPeds[iPreviousPed].PedsWeapon, INFINITE_AMMO, TRUE, TRUE)
				ENDIF
				
				SET_ENTITY_HEALTH(TestPeds[iPreviousPed].ped, 800)
				SET_ENTITY_INVINCIBLE(TestPeds[iPreviousPed].ped, TRUE)
				SET_ENTITY_PROOFS(TestPeds[iPreviousPed].ped, TRUE, TRUE, TRUE, TRUE, TRUE)
				
				SET_PED_SPHERE_DEFENSIVE_AREA(TestPeds[iPreviousPed].Ped, vDefensiveAreaPositions[iPreviousPed], fDefensiveAreaRadii[iPreviousPed])
				SET_PED_COMBAT_MOVEMENT(TestPeds[iPreviousPed].Ped, GET_COMBAT_MOVEMENT_TYPE_FROM_WIDGETS())
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(TestPeds[iPreviousPed].Ped, 100.0)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[iPreviousPed].ped, TRUE)
				
				bGiveSwitchTask = FALSE
			ENDIF
			
		BREAK
		CASE STATE_2
		BREAK
		
	ENDSWITCH

ENDPROC

PROC CLEANUP_SWITCH_BETWEEN_COMBAT_CHARACTERS()

	IF NOT IS_ENTITY_DEAD(playerpedindex)
		//SET_ENTITY_VISIBLE(scplayer, TRUE)
		SET_ENTITY_COORDS(playerpedindex, playercoords)
		SET_ENTITY_INVINCIBLE(playerpedindex, FALSE)
		PLAYER_INDEX playerindex = GET_PLAYER_INDEX()
		CHANGE_PLAYER_PED(playerindex, playerpedindex)		
	ENDIF
	
	INT index
	FOR index = 0 to MAX_NUMBER_OF_COVER_POINTS - 1
       	IF CoverPoints[index] != NULL
			REMOVE_COVER_POINT(CoverPoints[index])
			CoverPoints[index] = NULL
		ENDIF
    ENDFOR

ENDPROC
//END SWITCH BETWEEN COMBAT CHARACTERS

//SWITCH BETWEEN DRIVING CHARACTERS
PROC SETUP_SWITCH_BETWEEN_DRIVING_CHARACTERS()

	TestPeds[0].PedsCoords = <<134.94608, 61.64671, 6.66379>>
    TestPeds[0].PedHeading = -90.0
    TestPeds[0].Pedrelgrp = 2
   	TestPeds[0].PedModel = PLAYER_ZERO
    TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
    TestPeds[0].PedcombatMove = 0
    TestPeds[0].pedcombatrange = 2
    TestPeds[0].combatcover = CA_USE_COVER
    TestPeds[0].bcombatcover = TRUE
	
	TestPeds[1].PedsCoords = <<155.86455, 57.56470, 6.64452>>
    TestPeds[1].PedHeading = -90.0
    TestPeds[1].Pedrelgrp = 2
   	TestPeds[1].PedModel = PLAYER_ONE
    TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
    TestPeds[1].PedcombatMove = 0
    TestPeds[1].pedcombatrange = 2
    TestPeds[1].combatcover = CA_USE_COVER
    TestPeds[1].bcombatcover = TRUE
		
	TestPeds[2].PedsCoords = <<113.13109, 47.97093, 6.64452>>
    TestPeds[2].PedHeading = -90.0
    TestPeds[2].Pedrelgrp = 2
   	TestPeds[2].PedModel = PLAYER_TWO
    TestPeds[2].PedsWeapon = WEAPONTYPE_SAWNOFFSHOTGUN
    TestPeds[2].PedcombatMove = 0
    TestPeds[2].pedcombatrange = 2
    TestPeds[2].combatcover = CA_USE_COVER
    TestPeds[2].bcombatcover = TRUE
	
	PlayerStartPos = <<-1.93147, 132.32083, 26.35070>>

	TestVehicles[0].VehicleCoords = <<148.67931, 63.43314, 6.64452>>
	TestVehicles[0].VehicleHeading = -90.0
	TestVehicles[0].VehicleModel = TAILGATER
	
	TestVehicles[1].VehicleCoords = <<184.52771, 47.91029, 6.64466>>
	TestVehicles[1].VehicleHeading = -90.0
	TestVehicles[1].VehicleModel = BUFFALO
	
	TestVehicles[2].VehicleCoords = <<121.64184, 46.24977, 6.64452>>
	TestVehicles[2].VehicleHeading = -90.0
	TestVehicles[2].VehicleModel = SANCHEZ

ENDPROC

PROC INIT_SWITCH_BETWEEN_DRIVING_CHARACTERS()

	SWITCHING_STATE = STATE_SWITCH_INIT
	g_savedGlobals.sFlow.isGameflowActive = FALSE
	PLAYER_INDEX playerindex = GET_PLAYER_INDEX()
    playerpedindex = GET_PLAYER_PED(playerindex)
	IF NOT IS_ENTITY_DEAD(playerpedindex)
		playercoords = GET_ENTITY_COORDS(playerpedindex)
		SET_ENTITY_COORDS(playerpedindex, <<-1.93147, 132.32083, 26.35070>>)
		SET_ENTITY_INVINCIBLE(playerpedindex, TRUE)
		CHANGE_PLAYER_PED(playerindex, TestPeds[0].Ped)		
	ENDIF
	
	INT i
	
	FOR i = 0 TO MAX_NUMBER_OF_PEDS - 1
		IF DOES_ENTITY_EXIST(TestPeds[i].Ped)
			IF NOT IS_ENTITY_DEAD(TestPeds[i].Ped)
				SET_ENTITY_HEALTH(TestPeds[i].Ped, 800)
				SET_ENTITY_INVINCIBLE(TestPeds[i].Ped, TRUE)
				SET_ENTITY_PROOFS(TestPeds[i].Ped, TRUE, TRUE, TRUE, TRUE, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[i].Ped, TRUE)
			ENDIF
		ENDIF
		
		TestPedsTimers[i] = -1
		TestPedsInVehicleTimes[i] = -1
		TestPedsDriveVehicleFlags[i] = FALSE
	ENDFOR
	
	FOR i = 0 TO MAX_NUMBER_OF_VEHICLES - 1
		IF DOES_ENTITY_EXIST(TestVehicles[i].Vehicle)
			IF NOT IS_ENTITY_DEAD(TestVehicles[i].Vehicle)
				SET_ENTITY_INVINCIBLE(TestVehicles[i].Vehicle, TRUE)
				SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(TestVehicles[i].Vehicle, FALSE)
				SET_VEHICLE_CAN_LEAK_OIL(TestVehicles[i].Vehicle, FALSE)
				SET_VEHICLE_CAN_LEAK_PETROL(TestVehicles[i].Vehicle, FALSE)
			ENDIF
		ENDIF
	ENDFOR

ENDPROC

PROC RUN_SWITCH_BETWEEN_DRIVING_CHARACTERS()

	INT 	index
	
	PROCESS_BACKGROUND_SWITCHING()
	
	SWITCH TEST_STATE
		CASE STATE_INIT		
			
			FOR index = 0 to MAX_NUMBER_OF_PLAYER_PEDS - 1  
				IF NOT IS_ENTITY_DEAD(TestPeds[index].ped)
					IF ( TestPeds[index].ped <> PLAYER_PED_ID()  )
		            	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[index].ped, TRUE)
						IF IS_VEHICLE_DRIVEABLE(TestVehicles[index].Vehicle)
						
							SEQUENCE_INDEX SequenceIndex
							
							SequenceIndex = NULL
							
							OPEN_SEQUENCE_TASK(SequenceIndex)
								TASK_ENTER_VEHICLE(NULL, TestVehicles[index].Vehicle, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, GET_MOVE_BLEND_RATIO_FROM_WIDGETS())
								TASK_VEHICLE_DRIVE_WANDER(NULL, TestVehicles[index].Vehicle, fCruiseSpeed, DRIVINGMODE_AVOIDCARS)
							CLOSE_SEQUENCE_TASK(SequenceIndex)
							TASK_PERFORM_SEQUENCE(TestPeds[index].Ped, SequenceIndex)
							CLEAR_SEQUENCE_TASK(SequenceIndex)
						ENDIF
					ENDIF
				ENDIF
	        ENDFOR
			
			TEST_STATE = STATE_1
		BREAK
		CASE STATE_1		
			
			IF ( SWITCHING_STATE = STATE_SWITCH_SWITCHING )
			
				IF ( bSwitchTaskGiven = FALSE )
			
					INT 		iPreviousPed
					WEAPON_TYPE eCurrentWeapon
					
					iPreviousPed = ENUM_TO_INT(sSelectorPeds.eCurrentSelectorPed)
					
					TestPedsTimers[iPreviousPed] = -1
					TestPedsInVehicleTimes[iPreviousPed] = iTimeInVehicle
					TestPedsDriveVehicleFlags[iPreviousPed] = bDriveVehicleToggle
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[iPreviousPed].ped, TRUE)
					
					IF IS_VEHICLE_DRIVEABLE(TestVehicles[iPreviousPed].Vehicle)
					
						SEQUENCE_INDEX SequenceIndex

						OPEN_SEQUENCE_TASK(SequenceIndex)
							IF NOT IS_PED_IN_VEHICLE(TestPeds[iPreviousPed].Ped, TestVehicles[iPreviousPed].Vehicle)
								TASK_ENTER_VEHICLE(NULL, TestVehicles[iPreviousPed].Vehicle, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, GET_MOVE_BLEND_RATIO_FROM_WIDGETS())
							ENDIF
							IF ( TestPedsDriveVehicleFlags[iPreviousPed] = TRUE )
								TASK_VEHICLE_DRIVE_WANDER(NULL, TestVehicles[iPreviousPed].Vehicle, fCruiseSpeed, DRIVINGMODE_AVOIDCARS)
							ELSE
								TASK_PAUSE(NULL, TestPedsInVehicleTimes[iPreviousPed])
							ENDIF
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						TASK_PERFORM_SEQUENCE(TestPeds[iPreviousPed].Ped, SequenceIndex)
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						
					ENDIF
					
					
					bSwitchTaskGiven = TRUE
					
				ENDIF

			ENDIF
			

			IF ( SWITCHING_STATE = STATE_SWITCH_PLAYING )
			
				FOR index = 0 TO MAX_NUMBER_OF_PLAYER_PEDS - 1
			
					IF DOES_ENTITY_EXIST(TestPeds[index].ped)
					
						IF NOT IS_ENTITY_DEAD(TestPeds[index].ped)
						
							IF IS_VEHICLE_DRIVEABLE(TestVehicles[index].Vehicle)
						
								IF ( TestPeds[index].ped <> PLAYER_PED_ID() )
									
									IF ( TestPedsInVehicleTimes[index] <> -1 )
				
										IF ( TestPedsTimers[index]  = -1 )

											IF IS_PED_SITTING_IN_VEHICLE(TestPeds[index].Ped, TestVehicles[index].Vehicle)
												TestPedsTimers[index] = GET_GAME_TIMER()
											ENDIF
											
										ELSE
										
											IF ( GET_GAME_TIMER() - TestPedsTimers[index] > TestPedsInVehicleTimes[index] )
											
												IF IS_PED_SITTING_IN_VEHICLE(TestPeds[index].Ped, TestVehicles[index].Vehicle)
													TASK_LEAVE_VEHICLE(TestPeds[index].Ped, TestVehicles[index].Vehicle)
												ENDIF
											
											ENDIF
											
										ENDIF
								
									ENDIF
										
								ENDIF
								
							ENDIF
						
						ENDIF
					
					ENDIF
			
				ENDFOR	
			
			ENDIF
			
		BREAK
		CASE STATE_2
		BREAK
		
	ENDSWITCH

ENDPROC

PROC CLEANUP_SWITCH_BETWEEN_DRIVING_CHARACTERS()

	IF NOT IS_ENTITY_DEAD(playerpedindex)
		//SET_ENTITY_VISIBLE(scplayer, TRUE)
		SET_ENTITY_COORDS(playerpedindex, playercoords)
		SET_ENTITY_INVINCIBLE(playerpedindex, FALSE)
		PLAYER_INDEX playerindex = GET_PLAYER_INDEX()
		CHANGE_PLAYER_PED(playerindex, playerpedindex)		
	ENDIF
	
	INT index
	FOR index = 0 to MAX_NUMBER_OF_COVER_POINTS - 1
       	IF CoverPoints[index] != NULL
			REMOVE_COVER_POINT(CoverPoints[index])
			CoverPoints[index] = NULL
		ENDIF
    ENDFOR

ENDPROC
//END SWITCH BETWEEN DRIVING CHARACTERS

//SWITCH BETWEEN DRIVE BY CHARACTERS
PROC SETUP_SWITCH_BETWEEN_DRIVE_BY_CHARACTERS()

	TestPeds[0].PedsCoords = <<108.18954, 71.67590, 6.64452>>
    TestPeds[0].PedHeading = -90.0
    TestPeds[0].Pedrelgrp = 2
   	TestPeds[0].PedModel = PLAYER_ZERO
    TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
    TestPeds[0].PedcombatMove = 0
    TestPeds[0].pedcombatrange = 2
    TestPeds[0].combatcover = CA_USE_COVER
    TestPeds[0].bcombatcover = TRUE
	
	TestPeds[1].PedsCoords = <<115.83532, 62.12878, 6.64452>>
    TestPeds[1].PedHeading = -90.0
    TestPeds[1].Pedrelgrp = 2
   	TestPeds[1].PedModel = PLAYER_ONE
    TestPeds[1].PedsWeapon = WEAPONTYPE_MICROSMG
    TestPeds[1].PedcombatMove = 0
    TestPeds[1].pedcombatrange = 2
    TestPeds[1].combatcover = CA_USE_COVER
    TestPeds[1].bcombatcover = TRUE
		
	TestPeds[2].PedsCoords = <<118.74217, 54.26124, 6.64452>>
    TestPeds[2].PedHeading = -90.0
    TestPeds[2].Pedrelgrp = 2
   	TestPeds[2].PedModel = PLAYER_TWO
    TestPeds[2].PedsWeapon = WEAPONTYPE_MICROSMG
    TestPeds[2].PedcombatMove = 0
    TestPeds[2].pedcombatrange = 2
    TestPeds[2].combatcover = CA_USE_COVER
    TestPeds[2].bcombatcover = TRUE
	
	TestPeds[3].PedsCoords = <<124.11050, 63.00785, 6.64452>>
    TestPeds[3].PedHeading = -90.0
    TestPeds[3].Pedrelgrp = 1
   	TestPeds[3].PedModel = S_M_Y_CLOWN_01
    TestPeds[3].PedsWeapon = WEAPONTYPE_MICROSMG
    TestPeds[3].PedcombatMove = 0
    TestPeds[3].pedcombatrange = 2
    TestPeds[3].combatcover = CA_USE_COVER
    TestPeds[3].bcombatcover = TRUE
	
	PlayerStartPos = <<-1.93147, 132.32083, 26.35070>>

	TestVehicles[0].VehicleCoords = <<108.32034, 69.82668, 6.64452>>
	TestVehicles[0].VehicleHeading = -90.0
	TestVehicles[0].VehicleModel = TAILGATER
	
	TestVehicles[1].VehicleCoords = <<115.80811, 60.65564, 6.64452>>
	TestVehicles[1].VehicleHeading = -90.0
	TestVehicles[1].VehicleModel = BUFFALO
	
	TestVehicles[2].VehicleCoords = <<119.47410, 55.60754, 6.64452>>
	TestVehicles[2].VehicleHeading = -90.0
	TestVehicles[2].VehicleModel = SANCHEZ
	
	TestVehicles[3].VehicleCoords = <<124.27516, 61.92668, 6.64452>>
	TestVehicles[3].VehicleHeading = -90.0
	TestVehicles[3].VehicleModel = SANCHEZ

ENDPROC

PROC INIT_SWITCH_BETWEEN_DRIVE_BY_CHARACTERS()

	SWITCHING_STATE = STATE_SWITCH_INIT
	g_savedGlobals.sFlow.isGameflowActive = FALSE
	PLAYER_INDEX playerindex = GET_PLAYER_INDEX()
    playerpedindex = GET_PLAYER_PED(playerindex)
	IF NOT IS_ENTITY_DEAD(playerpedindex)
		playercoords = GET_ENTITY_COORDS(playerpedindex)
		SET_ENTITY_COORDS(playerpedindex, <<-1.93147, 132.32083, 26.35070>>)
		SET_ENTITY_INVINCIBLE(playerpedindex, TRUE)
		CHANGE_PLAYER_PED(playerindex, TestPeds[0].Ped)		
	ENDIF
	
	INT i
	
	FOR i = 0 TO MAX_NUMBER_OF_PEDS - 1
		IF DOES_ENTITY_EXIST(TestPeds[i].Ped)
			IF NOT IS_ENTITY_DEAD(TestPeds[i].Ped)
				SET_ENTITY_HEALTH(TestPeds[i].Ped, 800)
				SET_ENTITY_INVINCIBLE(TestPeds[i].Ped, TRUE)
				SET_ENTITY_PROOFS(TestPeds[i].Ped, TRUE, TRUE, TRUE, TRUE, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[i].Ped, TRUE)
			ENDIF
		ENDIF
		TestPedsDriveByFlags[i] = FALSE
	ENDFOR
	
	FOR i = 0 TO MAX_NUMBER_OF_VEHICLES - 1
		IF DOES_ENTITY_EXIST(TestVehicles[i].Vehicle)
			IF NOT IS_ENTITY_DEAD(TestVehicles[i].Vehicle)
				SET_ENTITY_INVINCIBLE(TestVehicles[i].Vehicle, TRUE)
				SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(TestVehicles[i].Vehicle, FALSE)
				SET_VEHICLE_CAN_LEAK_OIL(TestVehicles[i].Vehicle, FALSE)
				SET_VEHICLE_CAN_LEAK_PETROL(TestVehicles[i].Vehicle, FALSE)
			ENDIF
		ENDIF
	ENDFOR

ENDPROC

PROC RUN_SWITCH_BETWEEN_DRIVE_BY_CHARACTERS()

	INT 	index
	
	PROCESS_BACKGROUND_SWITCHING()
	
	SWITCH TEST_STATE
		CASE STATE_INIT		
			
			FOR index = 0 to MAX_NUMBER_OF_PLAYER_PEDS - 1  
				IF NOT IS_ENTITY_DEAD(TestPeds[index].ped)
					IF ( TestPeds[index].ped <> PLAYER_PED_ID()  )
		            	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[index].ped, TRUE)
						IF IS_VEHICLE_DRIVEABLE(TestVehicles[index].Vehicle)
						
							SEQUENCE_INDEX SequenceIndex
							
							SequenceIndex = NULL
							
							OPEN_SEQUENCE_TASK(SequenceIndex)
								TASK_ENTER_VEHICLE(NULL, TestVehicles[index].Vehicle, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, GET_MOVE_BLEND_RATIO_FROM_WIDGETS())
								TASK_VEHICLE_DRIVE_WANDER(NULL, TestVehicles[index].Vehicle, fCruiseSpeed, DRIVINGMODE_AVOIDCARS)
							CLOSE_SEQUENCE_TASK(SequenceIndex)
							TASK_PERFORM_SEQUENCE(TestPeds[index].Ped, SequenceIndex)
							CLEAR_SEQUENCE_TASK(SequenceIndex)

						ENDIF
					ENDIF
				ENDIF
	        ENDFOR
			
			IF NOT IS_ENTITY_DEAD(TestPeds[3].Ped)
				IF IS_VEHICLE_DRIVEABLE(TestVehicles[3].Vehicle)
					SET_PED_INTO_VEHICLE(TestPeds[3].Ped, TestVehicles[3].Vehicle, VS_DRIVER)
					TASK_VEHICLE_DRIVE_WANDER(TestPeds[3].Ped, TestVehicles[3].Vehicle, fCruiseSpeed, DRIVINGMODE_AVOIDCARS)
				ENDIF
			ENDIF
			
			TEST_STATE = STATE_1
		BREAK
		CASE STATE_1		
			
			IF ( SWITCHING_STATE = STATE_SWITCH_SWITCHING )
			
				IF ( bSwitchTaskGiven = FALSE )
			
					INT 		iPreviousPed
					WEAPON_TYPE eCurrentWeapon
					
					iPreviousPed = ENUM_TO_INT(sSelectorPeds.eCurrentSelectorPed)
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[iPreviousPed].ped, TRUE)
					
					IF NOT GET_CURRENT_PED_WEAPON(TestPeds[iPreviousPed].ped, eCurrentWeapon)
						GIVE_WEAPON_TO_PED(TestPeds[iPreviousPed].ped, TestPeds[iPreviousPed].PedsWeapon, INFINITE_AMMO, TRUE, TRUE)
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(TestVehicles[iPreviousPed].Vehicle)
					
						SEQUENCE_INDEX SequenceIndex
						
						OPEN_SEQUENCE_TASK(SequenceIndex)
							IF NOT IS_PED_IN_VEHICLE(TestPeds[iPreviousPed].Ped, TestVehicles[iPreviousPed].Vehicle)
								TASK_ENTER_VEHICLE(NULL, TestVehicles[iPreviousPed].Vehicle, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, GET_MOVE_BLEND_RATIO_FROM_WIDGETS())
							ENDIF
							TASK_VEHICLE_DRIVE_WANDER(NULL, TestVehicles[iPreviousPed].Vehicle, fCruiseSpeed, DRIVINGMODE_AVOIDCARS)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						TASK_PERFORM_SEQUENCE(TestPeds[iPreviousPed].Ped, SequenceIndex)
						CLEAR_SEQUENCE_TASK(SequenceIndex)

					ENDIF
					
					TestPedsDriveByFlags[iPreviousPed] = FALSE
					
					bSwitchTaskGiven = TRUE
					
				ENDIF

			ENDIF

			IF ( SWITCHING_STATE = STATE_SWITCH_PLAYING )
			
				FOR index = 0 TO MAX_NUMBER_OF_PLAYER_PEDS - 1
					IF DOES_ENTITY_EXIST(TestPeds[index].ped)
						IF NOT IS_ENTITY_DEAD(TestPeds[index].ped)
							IF IS_VEHICLE_DRIVEABLE(TestVehicles[index].Vehicle)
								IF ( TestPeds[index].ped <> PLAYER_PED_ID() )
									IF IS_PED_SITTING_IN_VEHICLE(TestPeds[index].Ped, TestVehicles[index].Vehicle)
										IF ( TestPedsDriveByFlags[index] = FALSE )
											IF 	GET_SCRIPT_TASK_STATUS(TestPeds[index].Ped, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
											AND NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(TestPeds[index].Ped)
												GIVE_WEAPON_TO_PED(TestPeds[index].ped, TestPeds[index].PedsWeapon, INFINITE_AMMO, TRUE, TRUE)
												TASK_DRIVE_BY(TestPeds[index].Ped, TestPeds[3].Ped, NULL, << 0.0, 0.0, 0.0 >>, 100.0, 80, TRUE)
												TestPedsDriveByFlags[index] = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF

		BREAK
		CASE STATE_2
		BREAK
		
	ENDSWITCH

ENDPROC

PROC CLEANUP_SWITCH_BETWEEN_DRIVE_BY_CHARACTERS()

	IF NOT IS_ENTITY_DEAD(playerpedindex)
		//SET_ENTITY_VISIBLE(scplayer, TRUE)
		SET_ENTITY_COORDS(playerpedindex, playercoords)
		SET_ENTITY_INVINCIBLE(playerpedindex, FALSE)
		PLAYER_INDEX playerindex = GET_PLAYER_INDEX()
		CHANGE_PLAYER_PED(playerindex, playerpedindex)		
	ENDIF
	
	INT index
	FOR index = 0 to MAX_NUMBER_OF_COVER_POINTS - 1
       	IF CoverPoints[index] != NULL
			REMOVE_COVER_POINT(CoverPoints[index])
			CoverPoints[index] = NULL
		ENDIF
    ENDFOR

ENDPROC
//END SWITCH BETWEEN DRIVE BY CHARACTERS

// Main setup, run once when the test is started
PROC SETUP_TEST_DATA()
    SWITCH INT_TO_ENUM(TESTS, gcurrentselection)       // the ENUM corresponds to the values from the XML file 
       	CASE SWITCH_BETWEEN_IDLE_CHARACTERS
        	SETUP_SWITCH_BETWEEN_IDLE_CHARACTERS()
      	BREAK
	  	CASE SWITCH_BETWEEN_AIMING_CHARACTERS
	   		SETUP_SWITCH_BETWEEN_AIMING_CHARACTERS()
	   	BREAK
	   	CASE SWITCH_BETWEEN_MOVING_CHARACTERS
	   		SETUP_SWITCH_BETWEEN_MOVING_CHARACTERS()
	   	BREAK
		CASE SWITCH_BETWEEN_MOVING_AIMING_CHARACTERS
	   		SETUP_SWITCH_BETWEEN_MOVING_AIMING_CHARACTERS()
	   	BREAK
		CASE SWITCH_BETWEEN_COVER_CHARACTERS
			SETUP_SWITCH_BETWEEN_COVER_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_COMBAT_CHARACTERS
			SETUP_SWITCH_BETWEEN_COMBAT_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_DRIVING_CHARACTERS
			SETUP_SWITCH_BETWEEN_DRIVING_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_DRIVE_BY_CHARACTERS
			SETUP_SWITCH_BETWEEN_DRIVE_BY_CHARACTERS()
		BREAK
    ENDSWITCH   
ENDPROC

// Main init, run each time test is restarted
PROC INIT_TEST()
    SWITCH INT_TO_ENUM(TESTS, gcurrentselection)       // the ENUM corresponds to the values from the XML file 
		CASE SWITCH_BETWEEN_IDLE_CHARACTERS
            INIT_SWITCH_BETWEEN_IDLE_CHARACTERS()
       	BREAK
	   	CASE SWITCH_BETWEEN_AIMING_CHARACTERS
	   		INIT_SWITCH_BETWEEN_AIMING_CHARACTERS()
	   	BREAK
	   	CASE SWITCH_BETWEEN_MOVING_CHARACTERS
	   		INIT_SWITCH_BETWEEN_MOVING_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_MOVING_AIMING_CHARACTERS
	   		INIT_SWITCH_BETWEEN_MOVING_AIMING_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_COVER_CHARACTERS
			INIT_SWITCH_BETWEEN_COVER_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_COMBAT_CHARACTERS
			INIT_SWITCH_BETWEEN_COMBAT_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_DRIVING_CHARACTERS
			INIT_SWITCH_BETWEEN_DRIVING_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_DRIVE_BY_CHARACTERS
			INIT_SWITCH_BETWEEN_DRIVE_BY_CHARACTERS()
		BREAK
    ENDSWITCH   
ENDPROC

// Main run
PROC RUN_TEST()
    SWITCH INT_TO_ENUM (TESTS, gcurrentselection)       // the ENUM corresponds to the values from the XML file 
		CASE SWITCH_BETWEEN_IDLE_CHARACTERS
			RUN_SWITCH_BETWEEN_IDLE_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_AIMING_CHARACTERS
			RUN_SWITCH_BETWEEN_AIMING_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_MOVING_CHARACTERS
			RUN_SWITCH_BETWEEN_MOVING_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_MOVING_AIMING_CHARACTERS
			RUN_SWITCH_BETWEEN_MOVING_AIMING_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_COVER_CHARACTERS
			RUN_SWITCH_BETWEEN_COVER_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_COMBAT_CHARACTERS
			RUN_SWITCH_BETWEEN_COMBAT_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_DRIVING_CHARACTERS
			RUN_SWITCH_BETWEEN_DRIVING_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_DRIVE_BY_CHARACTERS
			RUN_SWITCH_BETWEEN_DRIVE_BY_CHARACTERS()
		BREAK
	ENDSWITCH	
ENDPROC

// Main cleanup
PROC CLEANUP_TEST()
    SWITCH INT_TO_ENUM (TESTS, gcurrentselection)       // the ENUM corresponds to the values from the XML file 
		CASE SWITCH_BETWEEN_IDLE_CHARACTERS
			CLEANUP_SWITCH_BETWEEN_IDLE_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_AIMING_CHARACTERS
			CLEANUP_SWITCH_BETWEEN_AIMING_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_MOVING_CHARACTERS
			CLEANUP_SWITCH_BETWEEN_MOVING_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_MOVING_AIMING_CHARACTERS
			CLEANUP_SWITCH_BETWEEN_MOVING_AIMING_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_COVER_CHARACTERS
			CLEANUP_SWITCH_BETWEEN_COVER_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_COMBAT_CHARACTERS
			CLEANUP_SWITCH_BETWEEN_COMBAT_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_DRIVING_CHARACTERS
			CLEANUP_SWITCH_BETWEEN_DRIVING_CHARACTERS()
		BREAK
		CASE SWITCH_BETWEEN_DRIVE_BY_CHARACTERS
			CLEANUP_SWITCH_BETWEEN_DRIVE_BY_CHARACTERS()
		BREAK
	ENDSWITCH	
ENDPROC

////////////////////////////////////////////////////////////////////////////////
  
PROC Terminate_test_script ()
    if IS_KEYBOARD_KEY_JUST_PRESSED (KEY_S)
        CLEAR_PRINTS ()
        CLEAR_HELP ()
        Cleanup_Scenario_Entities ()
        Temp_cleanup_scenario_cams ()
        SET_PLAYER_COLISION(scplayer, true)
        IF NOT IS_PED_INJURED (scplayer)
            SET_PED_COORDS_KEEP_VEHICLE(scplayer, GET_PLAYER_START_POS () + gvMapOffset)
        ENDIF
        
        TERMINATE_THIS_THREAD ()
        
    ENDIF
ENDPROC

//Purpose: Runs the main scenarios
PROC Run_Test_Scenario ()
    int index = 0
    
    SWITCH TestScenarioAStatus
        //setps up all the data for the sceanrio, this is only called once or if selected in widget
        CASE InitialiseScenarioData
            PlayerStartPos = GET_PLAYER_START_POS()         //sets the players start coords at the default

            Bscenario_running = FALSE
            INITIALISE_PED_DATA (Testpeds)
            INITIALISE_VEHICLE_DATA (TestVehicles)
            INITIALISE_CAM_DATA(TestCams )
            INITIALISE_OBJECT_DATA(TestObjects)
            SETUP_TEST_DATA()
    
            HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
            TestScenarioAStatus = CreateScenarioEntities      
			
            Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)     //sets the 
        
        BREAK
        
        // Creates all the scenario data
        CASE CreateScenarioEntities
            Bscenario_running = FALSE
            
            Set_Gang_Relationships (TRUE)   
        
            Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
        
            //peds
            FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
                CREATE_PED_ENTITY(Testpeds[index])
                GiveBlipsToPedsGrps (Testpeds[index])
                ALTER_COMBAT_STATS (Testpeds[index] )
                SWAP_PED_WEAPONS (Testpeds[index])
               
                Block_Peds_Temp_Events (Testpeds[index], TRUE)
                SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
            ENDFOR
            
            //TestVehicles
            FOR Index = 0 to MAX_NUMBER_OF_VEHICLES -1
                CREATE_VEHICLE_ENTITY (TestVehicles[index] )
            ENDFOR
        
            //TestCams
            for index = 0 to MAX_NUMBER_OF_CAMERAS -1
                CREATE_CAM_ENTITY (TestCams[index])
            ENDFOR
            
            //TestObjects
            FOR Index = 0 to MAX_NUMBER_OF_OBJECTS -1
                CREATE_OBJECT_ENTITY (TestObjects[index])
            ENDFOR
            
            TestScenarioAStatus = SetScenarioEntities
        BREAK
        
        CASE SetScenarioEntities
            
            Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
			
            Start_And_Reset_Test ()
            
            IF gBeginCombatScenario
				
                TestScenarioAStatus = RunScenario
                gBeginCombatScenario = FALSE
                gRun_debuggig = FALSE
                Bscenario_running = TRUE
                
				INITALISE_TEST_STATE()
				INIT_TEST()
				
                TEMP_ACTIVATE_CAMS (TestCams[FixedCamera].cam )
                
                IF DOES_CAM_EXIST (TestCams[FixedCamera].cam )
                    HELP_TEXT_STATE =  HTF_SCENARIO_RUNNING_FIXED_CAM
                ELSE
                    HELP_TEXT_STATE = HTF_STARTED_RUNNING_NO_CAMS
                ENDIF
            ENDIF
        BREAK
        
        //Runs the actual selected scenario
        
        CASE RunScenario
                
            Temp_Run_Scenario_Tracking_Cam (TestPeds, MAX_NUMBER_OF_PEDS, TestCams[TrackingCamera].cam) 
            
            Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
                
            Start_And_Reset_Test ()
                
            Check_For_Scenario_Reset ()
            
            IF DOES_CAM_EXIST (TestCams[TrackingCamera].cam)
                if IS_CAM_RENDERING (TestCams[TrackingCamera].cam)
                    HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_TRACKING_CAM
                ELSE
                    HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_FIXED_CAM
                ENDIF
            ENDIF
                    
            RUN_TEST()     //run the main tests        
            
        BREAK 
                

        CASE CleanupScenario
            
            HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
			
			CLEANUP_TEST()
            
            Cleanup_Scenario_Entities ()
            
            IF gResetToDefault  
                Temp_cleanup_scenario_cams ()       //here we are changing scenarios so we need to reset cams
                TestScenarioAStatus = InitialiseScenarioData
                gResetToDefault = FALSE
            ENDIF
        
            IF gResetCombatScenario
                Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)
                gcurrentselection = gSelection 
                IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
                    ACTIVATE_CAM (TestCams[FixedCamera].cam)
                ELSE
                    Temp_cleanup_scenario_cams ()   
                ENDIF
                TestScenarioAStatus = CreateScenarioEntities
                gResetCombatScenario = FALSE
            ENDIF	
        
        BREAK
        
    ENDSWITCH
ENDPROC

SCRIPT
    
	// This allows debug lines to be rendered around peds, vehicles and objects for locate commands.
    SET_DEBUG_ACTIVE (TRUE)
	
    gvMapOffset = GET_PLAYER_START_POS ()
    
    SETUP_MISSION_XML_MENU(XMLMenu, KEY_Q)
	
	// Create Debug Rag Widgets 
    CREATE_TEST_WIDGET()
	
	// Clear the area
    SETUP_AREA_FOR_MISSION(<<0.0, 0.0, 0.0>>)
    
    //Gets a reference to the player
    GET_THE_PLAYER()
    
    // Set the player ready for the script
    SET_PLAYER_COLISION(scplayer, true)
    
    WHILE TRUE
    
        // Controls the help text hides if xml menu is active
        TEXT_CONTROLLER()
    
        // Can set all scenario peds invincible from the widget
        Set_Scenario_Peds_Invincible (TestPeds, MAX_NUMBER_OF_PEDS, gsetpedsinvincible )
    
        // User can create a debug cam for setting sceanrios
        Temp_Create_Debug_Cam (TestCams[FixedCamera]) 
        
        //Runs the selected option from the XML menu 
        Run_Selection_From_XML_input ()
        
        //Checks that a valid selection has been input and runs the scenario
        IF (gcurrentselection <> InvalidSelection)
    
            Draw_Debug_Info ()
            
            //Sets the test scenario into debug mode
            Set_To_Debug ()
            
            if (gRun_debuggig)
                Temp_Debug_Scenario (TestPeds, TestVehicles, Route, TestCams[FixedCamera].cam, S_M_Y_COP_01)                              //Allows the entities in the scneario to be adjusted
                Print_Scenario_Data (TestPeds, TestVehicles, Route, TestCams)
            ENDIF
            
            Run_Test_Scenario ()                                
            
            SWITCH_BETWEEN_FIXED_AND_TRACKING (TestCams[FixedCamera].cam)
            
            PRINT_ACTIVE_TEST (Bscenario_running,gRun_debuggig)     
        ENDIF
        
        Terminate_test_script ()
        
        WAIT (0)
    
    
    ENDWHILE

ENDSCRIPT


#ENDIF  // IS_DEBUG_BUILD

