// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "script_xml.sch"
USING "commands_entity.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "commands_streaming.sch"
USING "commands_graphics.sch"
USING "commands_cutscene.sch"
USING "commands_debug.sch"
USING "test_tools.sch"
USING "commands_vehicle.sch"
USING "commands_camera.sch"
USING "commands_player.sch"
USING "script_DEBUG.sch"
USING "commands_script.sch"
USING "commands_physics.sch"

//Notes: This is script is to allow easy setup of test scenarios for the code department. It works in conjunction with test_tools.sch
//It  is a data driven system 

//Defaults values when the test is reset to factory the standard values are used

//Level menu 
STRING XMLMenu = "Testbed/TestPhysicsMenu"

//Ped Vars
CONST_INT MAX_NUMBER_OF_PEDS 			10

peds_struct TestPeds [MAX_NUMBER_OF_PEDS]

//Vehicle Vars
CONST_INT MAX_NUMBER_OF_VEHICLES 	 10

vehicle_struct TestVehicles[MAX_NUMBER_OF_VEHICLES]

//Prop/object Vars
CONST_INT MAX_NUMBER_OF_OBJECTS 20
Object_struct TestObjects[MAX_NUMBER_OF_OBJECTS]

//Route Vars 
CONST_INT MAX_NUMBER_OF_NODES 				7

RouteStruct Route[MAX_NUMBER_OF_NODES]

//Camera Vars 
CONST_INT MAX_NUMBER_OF_CAMERAS			2
CONST_INT FixedCamera										0
CONST_INT TrackingCamera									1

StCameraData TestCams[MAX_NUMBER_OF_CAMERAS]

//Shape test Vars
CONST_INT MAX_NUMBER_OF_SHAPETESTS 		10 //Assumed to be >= 8
ShapeTestStruct TestShapeTests[MAX_NUMBER_OF_SHAPETESTS]

BOOL gAllowDebugging = TRUE

STRING kinematicAnimDict = "missprologueig_2"
SEQUENCE_INDEX seqMain

ROPE_INDEX ropeID


/////////// Variables for the various test scenarios ///////////

// Material physical properties test:
VECTOR vMatTest_NorthWestCorner = <<136.17, -105.55, 6.66>>
VECTOR vMatTest_SouthEastCorner = <<196.15, -165.52, 6.66>>
INT nMatTest_NumRows = 3
INT nMatTest_NumCols = 3
INT nMatTest_BallMaterialChoice = 0
INT nMatTest_ViewChoice = 0
FLOAT fMatTest_Width
FLOAT fMatTest_Height
FLOAT fMatTest_BallDropHeight = 3.0
INT nSwatch

MODEL_NAMES MatTest_BallMaterialTypes[8]
VECTOR vMatTest_CameraPositions
VECTOR vMatTest_CameraRotations

// Async ped targetting test:
INT nPedTargetTest_NumPeds = 10
INT nPedTargetTest_NumTestsPerFrame = 40
INT nPedTargetTest_NumTestsSubmitted = 0
REL_GROUP_HASH async_rel_group[10]
TEXT_LABEL async_group_label

// Shape test test:
INT NumFramesToSkipUpdate = 0

// Kinematic ped test:
FLOAT fKinTest_xPos = 23
FLOAT fKinTest_yPos = 181
FLOAT fKinTest_zPos = 6.35
FLOAT fKinTest_Rot = -120

// Dangle from rope test:
FLOAT fPhaseX = 0.0
FLOAT fPhaseY = 0.0
FLOAT fPhaseChangeRateX = 15.0
FLOAT fPhaseChangeRateY = 10.0

////////////////////////////////////////////////////////////////

//OBJECT_INDEX Object

bool testprint = FALSE
bool flushFile = FALSE
PROC CREATE_TEST_WIDGET ()
		START_WIDGET_GROUP("General Tests")
		SETUP_WIDGET()
			START_WIDGET_GROUP("Navigation")
				ADD_WIDGET_BOOL ("Start scenario", gBeginCombatScenario)
				ADD_WIDGET_BOOL ("Restart scenario", gResetCombatScenario)
				ADD_WIDGET_BOOL ("Reset to default", gResetToDefault)
				ADD_WIDGET_BOOL ("Debug scenario", gRun_debuggig)
				ADD_WIDGET_BOOL ("Print all setup info", gPrintAllIinfo)
				ADD_WIDGET_BOOL ("Test print", testprint)
				ADD_WIDGET_BOOL ("flushFilet", flushFile)
			STOP_WIDGET_GROUP() // "Navigation"
			
			START_WIDGET_GROUP("Physics materials tests")
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("Overview")
					ADD_TO_WIDGET_COMBO("Ice")
					ADD_TO_WIDGET_COMBO("Dirt-track")
					ADD_TO_WIDGET_COMBO("Concrete")
					ADD_TO_WIDGET_COMBO("Tarmac")
					ADD_TO_WIDGET_COMBO("Ceramic")
					ADD_TO_WIDGET_COMBO("Snow")
					ADD_TO_WIDGET_COMBO("Grass")
					ADD_TO_WIDGET_COMBO("Metal")
					ADD_TO_WIDGET_COMBO("Wood")
				STOP_WIDGET_COMBO("View selection", nMatTest_ViewChoice)

				ADD_WIDGET_FLOAT_SLIDER("Height to drop balls from", fMatTest_BallDropHeight, 0.0, 50.0, 1.0)

				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("Rubber")
					ADD_TO_WIDGET_COMBO("Stone")
					ADD_TO_WIDGET_COMBO("Metal")
					ADD_TO_WIDGET_COMBO("Wood")
					ADD_TO_WIDGET_COMBO("Ice")
					ADD_TO_WIDGET_COMBO("Glass")
					ADD_TO_WIDGET_COMBO("Polystyrene")
					ADD_TO_WIDGET_COMBO("Snow")
				STOP_WIDGET_COMBO("Material type for balls", nMatTest_BallMaterialChoice)
			STOP_WIDGET_GROUP() // "Physics materials tests"
			
			START_WIDGET_GROUP("Async ped targetting test")
				ADD_WIDGET_INT_SLIDER("Number of tests per frame", nPedTargetTest_NumTestsPerFrame, 0, 81, 1)
			STOP_WIDGET_GROUP() // ""
			
			START_WIDGET_GROUP ("Physics shape test test")
                ADD_WIDGET_INT_SLIDER ("Num frames to skip shapetest update", NumFramesToSkipUpdate ,0, 9, 1 )
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Kinematic peds test")
				ADD_WIDGET_FLOAT_SLIDER("x pos of ped", fKinTest_xPos, -300.0, 300.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("y pos of ped", fKinTest_yPos, -300.0, 300.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("z pos of ped", fKinTest_zPos, -10.0, 30.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("ped rotation", fKinTest_Rot, -180.0, 180.0, 1.0)
			STOP_WIDGET_GROUP() // "Kinematic peds test"
			
			START_WIDGET_GROUP("Dangling ped test")
				ADD_WIDGET_FLOAT_SLIDER("Movement rate X", fPhaseChangeRateX, 0.0, 100.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Movement rate Y", fPhaseChangeRateY, 0.0, 100.0, 0.1)
			STOP_WIDGET_GROUP() // "Kinematic peds test"
		STOP_WIDGET_GROUP()
ENDPROC


// Functions end: WIDGET

//Mission flow House keeping


//PUPROSE: REmoves all the scenarios and resets all the relationships
	PROC Cleanup_Scenario_Entities ()
	int index = 0
		FOR index = 0 TO MAX_NUMBER_OF_PEDS-1
			CLEAN_UP_PED_ENTITIES(TestPeds[index].ped)
		ENDFOR 
		
		FOR index = 0 TO MAX_NUMBER_OF_VEHICLES-1
			CLEAN_UP_VEHICLE_ENTITIES(TestVehicles[index].Vehicle )
		ENDFOR
		
		FOR index = 0 TO MAX_NUMBER_OF_OBJECTS-1
			CLEAN_UP_OBJECT_ENTITIES(TestObjects[index].Object)
		ENDFOR

		Set_Gang_Relationships (FALSE)	
	ENDPROC

PROC Terminate_test_script ()
	IF IS_KEYBOARD_KEY_JUST_PRESSED (KEY_S)
		CLEAR_PRINTS ()
		CLEAR_HELP ()
		Cleanup_Scenario_Entities ()
		Temp_cleanup_scenario_cams ()
		SET_PLAYER_COLISION(scplayer, true)
		IF NOT IS_PED_INJURED (scplayer)
			  SET_PED_COORDS_KEEP_VEHICLE(scplayer, GET_PLAYER_START_POS () )
		ENDIF

		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC
//END: House Keeping

//Test Specific Functions:

//EXAMPLE OF IMPLEMTING A SEQUENCE
FUNC SEQUENCE_INDEX TEST_TASK_OPEN_VEHICLE_DOOR(VEHICLE_INDEX vehicle, int time = -2)
		SEQUENCE_INDEX TEST_SEQUENCE
		OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
				TASK_OPEN_VEHICLE_DOOR  (NULL, vehicle,  time )
		CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		RETURN TEST_SEQUENCE
ENDFUNC					


PROC SET_CAMERA(VECTOR vPos, VECTOR vRot)
	IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
		SET_CAM_COORD (TestCams[FixedCamera].cam , vPos)
		SET_CAM_ROT (TestCams[FixedCamera].cam ,  vRot  )
		SET_CAM_ACTIVE (TestCams[FixedCamera].cam, TRUE )	
	ELSE
		TestCams[FixedCamera].cam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vPos, vRot, 60.0, TRUE )
		RENDER_SCRIPT_CAMS (TRUE, FALSE)	
		SET_CAM_ACTIVE (TestCams[FixedCamera].cam, TRUE )	
	ENDIF	
ENDPROC

//point route test
PROC DRIVE_TO_POINT_SEQUENCE (PED_INDEX ped, VEHICLE_INDEX veh)
	SEQUENCE_INDEX SEQ
	OPEN_SEQUENCE_TASK (SEQ)


		TASK_FLUSH_ROUTE()
		TASK_EXTEND_ROUTE(<< -0.0012, 40.0009, 7.9393>>)
		TASK_EXTEND_ROUTE(<<20.0012, 40.0009, 7.9393 >>)
		TASK_EXTEND_ROUTE(<<20.0012, 10.0009, 7.9393>>)
		TASK_EXTEND_ROUTE(<<-0.0012, 10.0009, 7.9393>>)
		TASK_DRIVE_POINT_ROUTE (NULL, veh, 5.0  )


		SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
		CLOSE_SEQUENCE_TASK(SEQ)
	
	IF NOT IS_PED_INJURED (ped)
		TASK_PERFORM_SEQUENCE (ped, seq)
		
		CLEAR_SEQUENCE_TASK (seq)
	ENDIF	
ENDPROC


//End Test Specific Functions

//These enums must match with the testNavmenu.xml



PROC INITALISE_TEST_STATE()
		SingleTaskStatus = startSingleTask
ENDPROC

ENUM Scenarios // Keep this up-to-date with common/data/script/xml/Testbed/TestPhysicsMenu.xml.
	AttachObjectToVehicleWithRope,
	MaterialPhysicsPropertiesTest,
	AsyncShapeTestTest,
	AsyncPedTargettingTest,
	AttachmentTestPedChain,
	AttachmentTestCarTree,
	ForkliftTest,
	HandlerTest,
	KinematicPedTest,
	DanglePedFromRope,
	DefaultTest = 1000
ENDENUM


//  END_SCENARIO: SquadOfPedsMovingBetweenVehicles  
PROC SETUP_TEST_DATA ()
	INT r
	INT c
	INT i
	SWITCH int_to_enum (scenarios, gcurrentselection)
		CASE AttachObjectToVehicleWithRope
			// Create the tow-truck.
			TestVehicles[0].vehicleCoords = <<5.00, 67.17, 6.98>>
			TestVehicles[0].vehicleHeading = 180.0
			TestVehicles[0].vehiclemodel = TOWTRUCK //TORNADO //TOURBUS //
			
			// Create the hook which will be attached to the tow-truck with rope.
			TestObjects[0].ObjectModel= PROP_VEHICLE_HOOK
			TestObjects[0].ObjectCoords = <<5.05, 70.93, 7.46>>
			TestObjects[0].ObjectRotation = <<0.0, 0.0, 0.0>>

			// Create the vehicle which will be pulled by the tow-truck.
			TestVehicles[1].vehicleCoords = <<4.69, 73.31, 6.98>>
			TestVehicles[1].vehicleHeading = 180.0
			TestVehicles[1].vehiclemodel = ANNIHILATOR
		BREAK
		
		CASE MaterialPhysicsPropertiesTest
			//looks like these models got removed. fixing compile:
			//@BSW
			MatTest_BallMaterialTypes[0] = DUMMY_MODEL_FOR_SCRIPT	//Test_Snow_bal
			MatTest_BallMaterialTypes[1] = DUMMY_MODEL_FOR_SCRIPT	//Test_stone_ball
			MatTest_BallMaterialTypes[2] = DUMMY_MODEL_FOR_SCRIPT	//Test_metal_ball
			MatTest_BallMaterialTypes[3] = DUMMY_MODEL_FOR_SCRIPT	//Test_Wood_ball
			MatTest_BallMaterialTypes[4] = DUMMY_MODEL_FOR_SCRIPT	//Test_ice_ball
			MatTest_BallMaterialTypes[5] = DUMMY_MODEL_FOR_SCRIPT	//Test_Glass_ball
			MatTest_BallMaterialTypes[6] = DUMMY_MODEL_FOR_SCRIPT	//Test_polystyrene_ball
			MatTest_BallMaterialTypes[7] = DUMMY_MODEL_FOR_SCRIPT	//Test_Snow_bal
			
			// Define the camera views.
			fMatTest_Width  = (vMatTest_NorthWestCorner.y - vMatTest_SouthEastCorner.y)/nMatTest_NumCols // Width is in the East-West direction.
			fMatTest_Height = (vMatTest_NorthWestCorner.x - vMatTest_SouthEastCorner.x)/nMatTest_NumRows // Height is in the North-South direction.

			// Overview camera.
			vMatTest_CameraPositions = <<110, -135, 30>>
			vMatTest_CameraRotations =  <<-25.0, 0.0, -90.0>>
			
			// Cameras zoomed in on each material swatch.
			IF nMatTest_ViewChoice > 0
				nSwatch = nMatTest_ViewChoice - 1
				r = nSwatch / nMatTest_NumCols
				c = nSwatch % nMatTest_NumRows

				vMatTest_CameraPositions = vMatTest_NorthWestCorner
				vMatTest_CameraPositions.x -= (0.2*fMatTest_Height) + fMatTest_Height*r
				vMatTest_CameraPositions.y -= (0.5*fMatTest_Width) + fMatTest_Width*c
				vMatTest_CameraPositions.z += 1.0
				vMatTest_CameraRotations = <<5.0, 0.0, -90.0>>
			ENDIF

			// Set up a camera to look at the material swatches.
			SET_CAMERA(vMatTest_CameraPositions, vMatTest_CameraRotations)
			
			// Create the required prop balls.
			FOR r = 0 to nMatTest_NumRows-1
				FOR c = 0 TO nMatTest_NumCols-1
					TestObjects[r*nMatTest_NumRows+c].ObjectModel = MatTest_BallMaterialTypes[nMatTest_BallMaterialChoice]
					TestObjects[r*nMatTest_NumRows+c].ObjectCoords = vMatTest_NorthWestCorner
					TestObjects[r*nMatTest_NumRows+c].ObjectCoords.x -= (0.5*fMatTest_Height) + fMatTest_Height*r
					TestObjects[r*nMatTest_NumRows+c].ObjectCoords.y -= (0.5*fMatTest_Width) + fMatTest_Width*c
					TestObjects[r*nMatTest_NumRows+c].ObjectCoords.z += fMatTest_BallDropHeight
					TestObjects[r*nMatTest_NumRows+c].ObjectRotation = <<0.0, 0.0, 0.0>>
				ENDFOR
			ENDFOR

		BREAK
		
		CASE AsyncPedTargettingTest
			FOR i = 0 TO nPedTargetTest_NumPeds-1
			    TestPeds[i].PedsCoords = <<1.0*i, 80.0+1.0*ABSI(i-nPedTargetTest_NumPeds/2), 6.35>>
    			TestPeds[i].PedHeading = 0
    			TestPeds[i].Pedrelgrp = 1
    			TestPeds[i].PedModel = S_M_Y_CLOWN_01
				async_group_label = "ASYNC_RG"
				async_group_label += i
				ADD_RELATIONSHIP_GROUP(async_group_label, async_rel_group[i])
			ENDFOR
			
			TestCams[FixedCamera].cam_pos = <<5.0,75.328735,13.197482>>
    		TestCams[FixedCamera].cam_rot = <<-40.0,0.0,0.0>>
    		TestCams[FixedCamera].cam_fov = 65.0
    		TestCams[FixedCamera].bActivateCam = TRUE
		BREAK
		
		CASE AsyncShapeTestTest
			// A couple of vehicles to move around to block LOS probes with.
			TestShapeTests[0].eShapeTestType = SHAPETEST_BOUND
			TestVehicles[0].vehicleCoords = <<5.00, 67.17, 6.98>>
			TestVehicles[0].vehicleHeading = 180.0
			TestVehicles[0].vehiclemodel = TOURBUS
			
			TestShapeTests[1].eShapeTestType = SHAPETEST_BOUNDINGBOX
			TestVehicles[1].vehicleCoords = <<5.00, 74.00, 6.98>>
			TestVehicles[1].vehicleHeading = 90.0
			TestVehicles[1].vehiclemodel = TOURBUS
			
			//An array of parallel line segments, spheres and capsules
			FOR i = 2 to 5
				TestShapeTests[i].eShapeTestType = SHAPETEST_LOSPROBE
				TestShapeTests[i].vStartPos = <<0.00, 60.00 + (0.5* TO_FLOAT(i)), 7>>
				TestShapeTests[i].vEndPos = <<10.00, 60.00 + (0.5* TO_FLOAT(i)), 7>>
				TestShapeTests[i].fRadius = 0.2
			ENDFOR
		
			FOR i = 6 TO 7
				TestShapeTests[i].eShapeTestType = SHAPETEST_LOSPROBE // SHAPETEST_SPHERE // Since nobody was using async sphere tests we disabled them
				TestShapeTests[i].vStartPos = <<0.00, 60.00 + (0.5* TO_FLOAT(i)), 7>>
				TestShapeTests[i].vEndPos = <<10.00, 60.00 + (0.5* TO_FLOAT(i)), 7>>
				TestShapeTests[i].fRadius = 0.2
			ENDFOR
			
			FOR i = 8 TO 8
				TestShapeTests[i].eShapeTestType = SHAPETEST_BOX
				TestShapeTests[i].vStartPos = <<5.00, 60.00 + (0.5* TO_FLOAT(i)), 7>>
				TestShapeTests[i].vDims = <<10.0, 0.2, 0.2>>
				TestShapeTests[i].vEulerAngles = <<45,0,0>>
			ENDFOR
			
			FOR i = 9 to MAX_NUMBER_OF_SHAPETESTS - 1
				TestShapeTests[i].eShapeTestType = SHAPETEST_CAPSULE
				TestShapeTests[i].vStartPos = <<0.00, 60.00 + (0.5* TO_FLOAT(i)), 7>>
				TestShapeTests[i].vEndPos = <<10.00, 60.00 + (0.5* TO_FLOAT(i)), 7>>
				TestShapeTests[i].fRadius = 0.2
			ENDFOR
		BREAK
		
		CASE AttachmentTestPedChain
			
		BREAK
		
		CASE AttachmentTestCarTree
			
		BREAK
		
		CASE ForkliftTest
			// Create some pallets and attach some objects to a few of them.
			TestObjects[0].ObjectModel= P_PALLET_02A_S
			TestObjects[0].ObjectCoords = <<-4.0, 85.0, 6.351031>>
			TestObjects[0].ObjectRotation = <<0.0, 0.0, 0.0>>

			TestObjects[1].ObjectModel= P_PALLET_02A_S
			TestObjects[1].ObjectCoords = <<-1.5, 85.0, 6.351031>>
			TestObjects[1].ObjectRotation = <<0.0, 0.0, 0.0>>

			TestObjects[2].ObjectModel= P_PALLET_02A_S
			TestObjects[2].ObjectCoords = <<1.0, 85.0, 6.351031>>
			TestObjects[2].ObjectRotation = <<0.0, 0.0, 0.0>>
			
			// A couple of props to attach to the pallets.
			TestObjects[3].ObjectModel= PROP_BOX_WOOD03A
			TestObjects[3].ObjectCoords = <<5.0, 85.0, 10.0>>
			TestObjects[3].ObjectRotation = <<0.0, 0.0, 0.0>>

			TestObjects[4].ObjectModel= PROP_BIOTECH_STORE
			TestObjects[4].ObjectCoords = <<10.0, 85.0, 10.0>>
			TestObjects[4].ObjectRotation = <<0.0, 0.0, 0.0>>
			
			TestVehicles[0].vehicleCoords = <<-4.00, 70.0, 6.98>>
			TestVehicles[0].vehicleHeading = 0.0
			TestVehicles[0].vehiclemodel = FORKLIFT
		BREAK
		
		CASE HandlerTest
			// Create a container to pick up.
			TestObjects[0].ObjectModel= Prop_Contr_03b_LD
			TestObjects[0].ObjectCoords = <<-4.0, 85.0, 6.351031>>
			TestObjects[0].ObjectRotation = <<0.0, 0.0, 0.0>>
			
			TestVehicles[0].vehicleCoords = <<-4.00, 70.0, 6.98>>
			TestVehicles[0].vehicleHeading = 0.0
			TestVehicles[0].vehiclemodel = HANDLER
		BREAK
		
		CASE KinematicPedTest
			TestPeds[0].PedsCoords = <<1.0, 80.0, 6.35>>
    		TestPeds[0].PedHeading = 0
    		TestPeds[0].Pedrelgrp = 1
    		TestPeds[0].PedModel = S_M_Y_Cop_01
		BREAK
		
		CASE DanglePedFromRope
			// Create the objects to which the rope and our ped will be attached.
			TestObjects[0].ObjectModel= Prop_LD_Test_01
			TestObjects[0].ObjectCoords = <<5.05, 70.93, 100.00>>
			TestObjects[0].ObjectRotation = <<0.0, 0.0, 0.0>>
			
			TestObjects[1].ObjectModel= Prop_LD_Test_01
			TestObjects[1].ObjectCoords = <<5.05, 70.93, 90.00>>
			TestObjects[1].ObjectRotation = <<0.0, 0.0, 0.0>>
		BREAK
		
	ENDSWITCH	
ENDPROC

// Further initialise objects now that they have been created. e.g. create constraints between them.
FUNC BOOL INIT_TEST_DATA()
	INT i
	VECTOR vecCarOffset
	VECTOR vecObjOffset
	VECTOR vecRotation
	
	VECTOR vRopePos
	VECTOR vRopeRot
	FLOAT fRopeLength
	BOOL bInitDone = FALSE
	SWITCH int_to_enum (scenarios, gcurrentselection)
		CASE AttachObjectToVehicleWithRope
			// Make sure the hook has been activated in the physics level.
			IF DOES_ENTITY_EXIST(TestObjects[0].Object)
				SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(TestObjects[0].Object, TRUE)
			ENDIF

			IF DOES_ENTITY_HAVE_PHYSICS(TestObjects[0].Object)
				// Connect the hook to the second car with a physical constraint.
				vecCarOffset.x = TestVehicles[1].VehicleCoords.x - TestObjects[0].ObjectCoords.x
				vecCarOffset.y = TestVehicles[1].VehicleCoords.y - TestObjects[0].ObjectCoords.y
				vecCarOffset.z = TestObjects[0].ObjectCoords.z - TestVehicles[1].VehicleCoords.z
				vecObjOffset.x = 0.0
				vecObjOffset.y = 0.0
				vecObjOffset.z = TestVehicles[1].VehicleCoords.z - TestObjects[0].ObjectCoords.z
				vecRotation = <<0.0, 0.0, 0.0>>
				IF NOT IS_ENTITY_DEAD(TestVehicles[1].Vehicle)
					ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(TestObjects[0].Object, TestVehicles[1].Vehicle, 0, 0, vecCarOffset, vecObjOffset, vecRotation, -1.0, FALSE, FALSE)
				ENDIF
		
				// Connect the tow-truck and hook with rope.
				vRopePos = <<4.67, 70.0, 7.45>>
				vRopeRot = <<0.0, 0.0, 0.0>>
				fRopeLength = 1.0
				ropeID = ADD_ROPE(vRopePos, vRopeRot, fRopeLength, PHYSICS_ROPE_DEFAULT)
				IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
					ATTACH_ENTITIES_TO_ROPE(ropeID, TestObjects[0].Object, TestVehicles[0].Vehicle, <<5.05, 70.93, 8.00>>, <<5.05, 70.93, 9.0>>, fRopeLength, 0, 0)
				ENDIF
			ENDIF
		BREAK
			
		CASE MaterialPhysicsPropertiesTest
			bInitDone = TRUE
		BREAK

		CASE AsyncPedTargettingTest
			FOR i = 0 TO nPedTargetTest_NumPeds-1
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[i].Ped, TRUE)
				SET_PED_SEEING_RANGE(TestPeds[i].Ped, 150.0)
				SET_PED_HIGHLY_PERCEPTIVE(TestPeds[i].Ped, TRUE)
		
				SET_PED_RELATIONSHIP_GROUP_HASH(TestPeds[0].Ped, async_rel_group[i])
			ENDFOR
			//SET_PED_RELATIONSHIP_GROUP_HASH(TestPeds[1].Ped, RELGROUPHASH_CIVMALE)
			//SET_PED_RELATIONSHIP_GROUP_HASH(TestPeds[2].Ped, RELGROUPHASH_CIVFEMALE)
			//SET_PED_RELATIONSHIP_GROUP_HASH(TestPeds[3].Ped, RELGROUPHASH_COP)
			//SET_PED_RELATIONSHIP_GROUP_HASH(TestPeds[4].Ped, RELGROUPHASH_HATES_PLAYER)
			//SET_PED_RELATIONSHIP_GROUP_HASH(TestPeds[5].Ped, RELGROUPHASH_AMBIENT_GANG_LOST)
			//SET_PED_RELATIONSHIP_GROUP_HASH(TestPeds[6].Ped, RELGROUPHASH_AMBIENT_GANG_MEXICAN)
			//SET_PED_RELATIONSHIP_GROUP_HASH(TestPeds[7].Ped, RELGROUPHASH_AMBIENT_GANG_FAMILY)
			//SET_PED_RELATIONSHIP_GROUP_HASH(TestPeds[8].Ped, RELGROUPHASH_AMBIENT_ARMY)
			//SET_PED_RELATIONSHIP_GROUP_HASH(TestPeds[9].Ped, RELGROUPHASH_SECURITY_GUARD)
			bInitDone = TRUE
		BREAK
		
		CASE AsyncShapeTestTest
			bInitDone = TRUE
		BREAK
		
		CASE ForkliftTest
			FOR i = 0 to 4
				IF DOES_ENTITY_EXIST(TestObjects[i].Object)
					SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(TestObjects[i].Object, TRUE)
				ENDIF
			ENDFOR

			// Attach some props to two of the pallets.
			i = 0
			IF DOES_ENTITY_HAVE_PHYSICS(TestObjects[0].Object)
				IF DOES_ENTITY_HAVE_PHYSICS(TestObjects[3].Object)
					ATTACH_ENTITY_TO_ENTITY(TestObjects[3].Object, TestObjects[0].Object, 0, <<0,0,0.25>>, <<0,0,0>>, FALSE, FALSE, TRUE)
					i += 1
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_HAVE_PHYSICS(TestObjects[1].Object)
				IF DOES_ENTITY_HAVE_PHYSICS(TestObjects[4].Object)
					ATTACH_ENTITY_TO_ENTITY(TestObjects[4].Object, TestObjects[1].Object, 0, <<0,0,0.85>>, <<0,0,0>>, FALSE, FALSE, TRUE)
					i += 1
				ENDIF
			ENDIF

			IF i >= 2
				bInitDone = TRUE
			ENDIF
		BREAK
		
		CASE HandlerTest
			bInitDone = TRUE
		BREAK
		
		CASE KinematicPedTest
			i = 0
			REQUEST_ANIM_DICT(kinematicAnimDict)
			IF HAS_ANIM_DICT_LOADED(kinematicAnimDict)
				i+=1
			ENDIF
		
			IF i >= 1
				bInitDone = TRUE
			ENDIF
		BREAK
		
		CASE DanglePedFromRope
			// Make sure the object has been activated in the physics level.
			IF DOES_ENTITY_EXIST(TestObjects[0].Object)
				SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(TestObjects[0].Object, TRUE)
			ENDIF
			IF DOES_ENTITY_EXIST(TestObjects[1].Object)
				SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(TestObjects[1].Object, TRUE)
			ENDIF

			IF DOES_ENTITY_EXIST(TestObjects[0].Object) AND DOES_ENTITY_HAVE_PHYSICS(TestObjects[0].Object) AND DOES_ENTITY_EXIST(TestObjects[1].Object) AND DOES_ENTITY_HAVE_PHYSICS(TestObjects[1].Object)
				// Connect the object and the player with rope.
				fRopeLength = 10.0
				ropeID = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TestObjects[0].Object,<<0, 0, 0>>), <<0,0,0>>, fRopeLength, PHYSICS_ROPE_THIN, -1, 10.0)
				ATTACH_ENTITIES_TO_ROPE(ropeID, TestObjects[0].Object, TestObjects[1].Object,
					GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TestObjects[0].Object,<<0, 0, 0>>),
					GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TestObjects[1].Object,<<0, 0.0, 0.0>>), fRopeLength, 0, 0)

				IF NOT IS_ENTITY_DEAD(scplayer) AND NOT IS_ENTITY_DEAD(TestObjects[1].Object)
					GIVE_WEAPON_TO_PED(scPlayer, WEAPONTYPE_ASSAULTRIFLE, 1000, TRUE, TRUE)
					ATTACH_ENTITY_TO_ENTITY(scplayer, TestObjects[1].Object, 0, <<0,0,0>>, <<0,0,0>>, TRUE)
				ENDIF
				   
				REQUEST_CLIP_SET(GET_CLIP_SET_FOR_SCRIPTED_GUN_TASK(SCRIPTED_GUN_TASK_ON_ROPE))
				WHILE NOT HAS_CLIP_SET_LOADED(GET_CLIP_SET_FOR_SCRIPTED_GUN_TASK(SCRIPTED_GUN_TASK_ON_ROPE))
					WAIT(0)
				ENDWHILE
				
				IF NOT IS_ENTITY_DEAD(scplayer)
				TASK_AIM_GUN_SCRIPTED(scplayer, SCRIPTED_GUN_TASK_ON_ROPE)
				ENDIF
				WHILE NOT (GET_SCRIPT_TASK_STATUS(scplayer, SCRIPT_TASK_AIM_GUN_SCRIPTED) = PERFORMING_TASK)
					WAIT(0)
					IS_ENTITY_DEAD(scplayer)
				ENDWHILE
				SET_ROPE_TRACK_ENTITY_FOR_SCRIPTED_GUN_TASK(scPlayer, TestObjects[0].Object)

				bInitDone = TRUE
			ENDIF
		BREAK

	ENDSWITCH
	
	RETURN bInitDone
ENDFUNC

FUNC ENTITY_INDEX CAST_TO_ENTITY_INDEX(ENTITY_INDEX A)
	RETURN A
ENDFUNC

PROC RUN_TEST ()
	INT i
	INT j
	
	SWITCH int_to_enum (scenarios, gcurrentselection)		// the ENUM corresponds to the values from the XML file 
		//setup scenario
		CASE AttachObjectToVehicleWithRope
			//Here we add some test cases
			IF NOT IS_PED_INJURED(TestPeds[0].ped)
				if IS_VEHICLE_DRIVEABLE(TestVehicles[0].Vehicle)
					Set_Test_State_To_Default()
				ENDIF
			ENDIF
		BREAK

		CASE MaterialPhysicsPropertiesTest
			//
			FOR i = 0 TO nMatTest_NumCols*nMatTest_NumRows-1
				SET_ENTITY_DYNAMIC(TestObjects[i].Object, TRUE)
			ENDFOR
		BREAK
		
		CASE AsyncPedTargettingTest
			nPedTargetTest_NumTestsSubmitted = 0
			FOR i = 0 TO nPedTargetTest_NumPeds-1
				FOR j = 0 TO nPedTargetTest_NumPeds-1
					IF nPedTargetTest_NumTestsSubmitted < nPedTargetTest_NumTestsPerFrame AND i <> j
						CAN_PED_SEE_HATED_PED(TestPeds[i].Ped, TestPeds[j].Ped)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,
														GET_PED_RELATIONSHIP_GROUP_HASH(TestPeds[i].Ped),
														GET_PED_RELATIONSHIP_GROUP_HASH(TestPeds[j].Ped))
						nPedTargetTest_NumTestsSubmitted += 1
					ENDIF
				ENDFOR
			ENDFOR
		BREAK
		
		CASE AsyncShapeTestTest
			IF NumFramesToSkipUpdate > 0
				NumFramesToSkipUpdate--
			ELSE
				TestShapeTests[0].EntityIndex = CAST_TO_ENTITY_INDEX(TestVehicles[0].Vehicle)
				TestShapeTests[1].EntityIndex = CAST_TO_ENTITY_INDEX(TestVehicles[1].Vehicle)

				FOR i = 0 TO MAX_NUMBER_OF_SHAPETESTS - 1
					//This will begin a new shape test request if possible
					if i = 4
						UPDATE_PERSISTENT_SHAPETEST(TestShapeTests[i], TestVehicles[0].Vehicle)
					ELSE
						UPDATE_PERSISTENT_SHAPETEST(TestShapeTests[i], TestVehicles[1].Vehicle)
					ENDIF
						
					//Rendering
					IF TestShapeTests[i].bIsHit
						IF TestShapeTests[i].bResultIsNew
							DRAW_DEBUG_SPHERE(TestShapeTests[i].vHitPos, 0.2, 50,205,50, 255) //Green
						ELSE //Render the old result
							DRAW_DEBUG_SPHERE(TestShapeTests[i].vHitPos, 0.2, 255,165,0, 255) //Orange
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		BREAK
		
		CASE AttachmentTestPedChain
			
		BREAK
		
		CASE AttachmentTestCarTree
				
		BREAK
		
		CASE ForkliftTest
			HELP_TEXT_STATE = HTF_DEBUG_FORKLIFT

			// Display a debug sphere to show whether this script command is working to find various objects.
			IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
				IF IS_ENTITY_ATTACHED_TO_FORKLIFT_FORKS(TestVehicles[0].Vehicle, TestObjects[3].Object)
					DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(TestObjects[3].Object), 2.0, 255, 255, 0, 100)
				ENDIF
				
				IF IS_ENTITY_ATTACHED_TO_FORKLIFT_FORKS(TestVehicles[0].Vehicle, TestObjects[4].Object)
					DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(TestObjects[4].Object), 2.0, 0, 255, 0, 100)
				ENDIF
			ENDIF
			
			// Test warping a pallet onto the forklift forks.
			IF IS_BUTTON_JUST_PRESSED(PAD1, DPADUP)
				IF NOT IS_ANY_ENTITY_ATTACHED_TO_FORKLIFT_FORKS(TestVehicles[0].Vehicle)
					ATTACH_PALLET_TO_FORKLIFT_FORKS(TestVehicles[0].Vehicle, TestObjects[0].Object)
				ENDIF
			ENDIF
			
			// Test detaching whatever is attached to the forklift forks.
			IF IS_BUTTON_JUST_PRESSED(PAD1, DPADDOWN)
				IF IS_ANY_ENTITY_ATTACHED_TO_FORKLIFT_FORKS(TestVehicles[0].Vehicle)
					DETACH_PALLET_FROM_FORKLIFT_FORKS(TestVehicles[0].Vehicle)
				ENDIF
			ENDIF
			
			// Test raising the forklift forks.
			IF IS_BUTTON_JUST_PRESSED(PAD1, DPADRIGHT)
				RAISE_FORKLIFT_FORKS(TestVehicles[0].Vehicle)
			ENDIF
			
			// Test lowering the forklift forks.
			IF IS_BUTTON_JUST_PRESSED(PAD1, DPADLEFT)
				LOWER_FORKLIFT_FORKS(TestVehicles[0].Vehicle)
			ENDIF
		BREAK
		
		CASE HandlerTest
			HELP_TEXT_STATE = HTF_DEBUG_FORKLIFT
			
			IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
			
			// Test attaching a container onto the handler's frame but check if it is in position first.
			IF IS_BUTTON_JUST_PRESSED(PAD1, DPADUP)
				IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(TestVehicles[0].Vehicle)
					IF IS_HANDLER_FRAME_LINED_UP_WITH_CONTAINER(TestVehicles[0].Vehicle, TestObjects[0].Object)
						ATTACH_CONTAINER_TO_HANDLER_FRAME_WHEN_LINED_UP(TestVehicles[0].Vehicle, TestObjects[0].Object)
					ENDIF
				ENDIF
			ENDIF
			
			// Test detaching whatever is attached to the handler's frame.
			IF IS_BUTTON_JUST_PRESSED(PAD1, DPADDOWN)
				IF IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(TestVehicles[0].Vehicle)
					DETACH_CONTAINER_FROM_HANDLER_FRAME(TestVehicles[0].Vehicle)
				ENDIF
			ENDIF
			
			// Test raising the handler's frame.
			IF IS_BUTTON_JUST_PRESSED(PAD1, DPADRIGHT)
				RAISE_HANDLER_FRAME(TestVehicles[0].Vehicle)
			ENDIF
			
			// Test lowering the handler's frame.
			IF IS_BUTTON_JUST_PRESSED(PAD1, DPADLEFT)
				LOWER_HANDLER_FRAME(TestVehicles[0].Vehicle)
			ENDIF
		BREAK
			
		CASE KinematicPedTest
			HELP_TEXT_STATE = HTF_DEBUG_KINEMATIC
			
			// Test lowering the forklift forks.
			IF IS_BUTTON_JUST_PRESSED(PAD1, DPADLEFT)
				SET_RAGDOLL_BLOCKING_FLAGS(TestPeds[0].Ped, RBF_PLAYER_IMPACT)
				OPEN_SEQUENCE_TASK(seqMain)
			 	TASK_PLAY_ANIM_ADVANCED(NULL, kinematicAnimDict, "main_brad", <<fKinTest_xPos, fKinTest_yPos, fKinTest_zPos>>,
					<<0,0,fKinTest_Rot>>, INSTANT_BLEND_IN, -1, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS,
					0.253 + 0.140)
				 CLOSE_SEQUENCE_TASK(seqMain)
				TASK_PERFORM_SEQUENCE(TestPeds[0].Ped, seqMain)
				CLEAR_SEQUENCE_TASK(seqMain)
			ENDIF
		BREAK
		
		CASE DanglePedFromRope
			SET_ENTITY_COORDS(TestObjects[0].Object, <<5.05+20.0*SIN(fPhaseX), 70.93+50.0*SIN(fPhaseY), 100.00>>)
			fPhaseX += fPhaseChangeRateX*GET_FRAME_TIME()
			fPhaseY += fPhaseChangeRateY*GET_FRAME_TIME()
		BREAK
		
	ENDSWITCH	

ENDPROC


//Purpose: Runs the main scenarios
PROC Run_Test_Scenario ()
	int index = 0
	
	SWITCH TestScenarioAStatus
		//setps up all the data for the sceanrio, this is only called once or if selected in widget
		CASE InitialiseScenarioData
			PlayerStartPos = GET_PLAYER_START_POS()				//sets the players start coords at the default
			Bscenario_running = FALSE
			INITIALISE_PED_DATA (Testpeds)
			INITIALISE_VEHICLE_DATA (TestVehicles)
			INITIALISE_CAM_DATA(TestCams )
			INITIALISE_SHAPETESTS(TestShapeTests)
			SETUP_TEST_DATA ()
	
			HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
			TestScenarioAStatus = CreateScenarioEntities
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)		//sets the 
		
		BREAK
		
		// Creates all the scenario data
		CASE CreateScenarioEntities
			Bscenario_running = FALSE
			
			Set_Gang_Relationships (TRUE)	
		
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
		
			//peds
			FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
				CREATE_PED_ENTITY(Testpeds[index])
				GiveBlipsToPedsGrps (Testpeds[index])
				ALTER_COMBAT_STATS (Testpeds[index] )
				SWAP_PED_WEAPONS (Testpeds[index])
				ADD_RELATIONSHIP_GROUP_TO_STRUCT (Testpeds[index])
				Block_Peds_Temp_Events (Testpeds[index], TRUE)
				SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
			ENDFOR
			
			//TestVehicles
			FOR Index = 0 to MAX_NUMBER_OF_VEHICLES -1
				CREATE_VEHICLE_ENTITY (TestVehicles[index] )
			ENDFOR

			//TestObjects
			FOR index = 0 TO MAX_NUMBER_OF_OBJECTS-1
				CREATE_OBJECT_ENTITY(TestObjects[index])
			ENDFOR

			//TestCams
			for index = 0 to MAX_NUMBER_OF_CAMERAS -1
				CREATE_CAM_ENTITY (TestCams[index])
			ENDFOR
			
			// Further initialise objects now that they have been created. e.g. create constraints between them.
			IF INIT_TEST_DATA()
				TestScenarioAStatus = SetScenarioEntities
			ENDIF
		BREAK
		
		CASE SetScenarioEntities
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
			
			Start_And_Reset_Test ()
			
			IF gBeginCombatScenario
				TestScenarioAStatus = RunScenario
				gBeginCombatScenario = FALSE
				gRun_debuggig = FALSE
				Bscenario_running = TRUE
				INITALISE_TEST_STATE()
				TEMP_ACTIVATE_CAMS (TestCams[FixedCamera].cam )
				
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam )
					HELP_TEXT_STATE =  HTF_SCENARIO_RUNNING_FIXED_CAM
				ELSE
					HELP_TEXT_STATE = HTF_STARTED_RUNNING_NO_CAMS
				ENDIF
			ENDIF
		BREAK
		
		//Runs the actual selected scenario
		
		CASE RunScenario
				
			Temp_Run_Scenario_Tracking_Cam (TestPeds, MAX_NUMBER_OF_PEDS, TestCams[TrackingCamera].cam) 
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
				
			Start_And_Reset_Test ()
				
		 	Check_For_Scenario_Reset ()
			
			IF DOES_CAM_EXIST (TestCams[TrackingCamera].cam)
				if IS_CAM_RENDERING (TestCams[TrackingCamera].cam)
					HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_TRACKING_CAM
				ELSE
					HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_FIXED_CAM
				ENDIF
			ENDIF
					
			RUN_TEST ()		//run the main tests 		
			
		BREAK 
				
		CASE CleanupScenario
			
			HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
		
			Cleanup_Scenario_Entities ()
			
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE (FALSE)
			
			IF gResetToDefault 	
				Temp_cleanup_scenario_cams ()		//here we are changing scenarios so we need to reset cams
				TestScenarioAStatus = InitialiseScenarioData
				gResetToDefault = FALSE
			ENDIF
		
			IF gResetCombatScenario
				Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)
				gcurrentselection = gSelection 
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
					ACTIVATE_CAM (TestCams[FixedCamera].cam)
				ELSE
					Temp_cleanup_scenario_cams ()	
				ENDIF
				TestScenarioAStatus = InitialiseScenarioData
				gResetCombatScenario = FALSE
			ENDIF
		
		BREAK
		
	ENDSWITCH
ENDPROC

SCRIPT
	
	SET_DEBUG_ACTIVE (TRUE)
	
	//gvMapOffset = GET_PLAYER_START_POS ()
	
	SETUP_MISSION_XML_MENU (XMLMenu, KEY_Q )	//defined at the top of the file
	
	SETUP_AREA_FOR_MISSION (<<0.0, 0.0, 0.0>>)
	
	//Gets a reference to the player
	Get_The_Player ()
	
	//set player collision
	SET_PLAYER_COLISION(scplayer, true)
	
	//Sets the test widget from the test tools
	CREATE_TEST_WIDGET ()

	WHILE TRUE
			
		// controls the help text hides if xml menu is active
		TEXT_CONTROLLER ()
	
		// Can set all scenario peds invincible from the widget
		Set_Scenario_Peds_Invincible (TestPeds, MAX_NUMBER_OF_PEDS, gsetpedsinvincible )
	
		//User can create a debug cam for setting sceanrios
		Temp_Create_Debug_Cam (TestCams[FixedCamera]) 
		
		//Runs the selected option from the XML menu 
		Run_Selection_From_XML_input ()
		
		//Checks that a valid selection has been input and runs the scenario
		IF (gcurrentselection <> InvalidSelection)
	
			Draw_Debug_Info (  )
			
			//Sets the test scenario into debug mode
			IF (gAllowDebugging)
				Set_To_Debug ()
			ENDIF
			
			if (gRun_debuggig)
				Temp_Debug_Scenario (TestPeds, TestVehicles, Route, TestCams[FixedCamera].cam, S_M_Y_COP_01)								//Allows the entities in the scneario to be adjusted
				Print_Scenario_Data (TestPeds, TestVehicles, Route, TestCams)
			ENDIF
			
			Run_Test_Scenario ()								
			
			SWITCH_BETWEEN_FIXED_AND_TRACKING (TestCams[FixedCamera].cam)
			
			PRINT_ACTIVE_TEST (Bscenario_running, gRun_debuggig)		
		ENDIF
		
		Terminate_test_script ()
		
		WAIT (0)
	
	
	ENDWHILE

ENDSCRIPT


#ENDIF	// IS_DEBUG_BUILD

