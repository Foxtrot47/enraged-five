// Chris McMahon

// Includes -----------------------------------------------//
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_pad.sch"
USING "commands_debug.sch"
USING "types.sch"
USING "commands_brains.sch"
USING "brains.sch"
USING "shared_debug_text.sch"



// Variables ----------------------------------------------//
ENUM ambStageFlag
	ambCanRun,
	ambRunning,
	ambEnd
ENDENUM
ambStageFlag ambStage = ambCanRun


// Clean Up
PROC missionCleanup()
	printDebugString("driveto_test Terminated >>>>>>>>>>>>>>>>>\n")
	TERMINATE_THIS_THREAD()
ENDPROC

// Mission Script -----------------------------------------//
SCRIPT(OBJECT_INDEX oOilPump)


IF DOES_ENTITY_EXIST(oOilPump)
	 
	FREEZE_ENTITY_POSITION(oOilPump, TRUE)
	
ENDIF

// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(0)
	IF DOES_ENTITY_EXIST(oOilPump)
		IF IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(oOilPump)
			SWITCH ambStage
				CASE ambCanRun
					IF DOES_ENTITY_HAVE_DRAWABLE(oOilPump)
						REQUEST_ANIM_DICT("map_objects")
						IF HAS_ANIM_DICT_LOADED("map_objects")
						
							//IF HAS_MODEL_LOADED(p_airdancer_01_s)
								PLAY_ENTITY_ANIM(oOilPump, "oilpump_run_03", "map_objects", 1.0, TRUE, FALSE, TRUE)
								ATTACH_ENTITY_BONE_TO_BONE_PHYSICALLY(oOilPump, GET_ENTITY_BONE_INDEX_BY_NAME(oOilPump, "Sec_lookat_Pjack_03"), GET_ENTITY_BONE_INDEX_BY_NAME(oOilPump, "Main_rot_Motor_Pjack_03"), <<0.0,5.594,0.0>>, <<0.0,-2.424,-0.594>>, 0.2)
								ambStage = ambRunning
							//ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE ambRunning
				
				BREAK
				CASE ambEnd
				
				BREAK
			ENDSWITCH 
		ELSE
			missionCleanup()
		ENDIF
	ELSE
		missionCleanup()
	ENDIF
ENDWHILE


ENDSCRIPT

