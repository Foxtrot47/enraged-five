// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "script_xml.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "commands_streaming.sch"
USING "commands_graphics.sch"
USING "commands_cutscene.sch"
USING "commands_debug.sch"
USING "test_tools.sch"
USING "commands_vehicle.sch"
USING "commands_camera.sch"
USING "commands_player.sch"
USING "script_DEBUG.sch"
USING "commands_script.sch"
USING "commands_entity.sch"

//Notes: This is script is to allow easy setup of test scenarios for the code department. It works in conjunction with test_tools.sch
//It  is a data driven system 

//Defaults values when the test is reset to factory the standard values are used

//vector gvMapOffset

//Level menu 
STRING XMLMenu = "Testbed/TestGeneralmenu"
//VECTOR gvMapOffset //offset all coords from the origin by the psoition of the mapin world coords

//Ped Vars
CONST_INT MAX_NUMBER_OF_PEDS            10

peds_struct TestPeds [MAX_NUMBER_OF_PEDS]

//Vehicle Vars
CONST_INT MAX_NUMBER_OF_VEHICLES     10

vehicle_struct TestVehicles[MAX_NUMBER_OF_VEHICLES]

//Route Vars 
CONST_INT MAX_NUMBER_OF_NODES               7

RouteStruct Route[MAX_NUMBER_OF_NODES]

//Camera Vars 
CONST_INT MAX_NUMBER_OF_CAMERAS         2
CONST_INT FixedCamera                                       0
CONST_INT TrackingCamera                                    1

StCameraData TestCams[MAX_NUMBER_OF_CAMERAS]


BOOL bStartedTasks = FALSE
BOOL bStartedAnim1 = FALSE
BOOL bStartedAnim2 = FALSE

BOOL gAllowDebugging = TRUE

//Test
//ENUM Test_Scenario
//  InitialiseScenarioData,
//  CreateScenarioEntities,
//  SetScenarioEntities,
//  RunScenario,
//  CleanupScenario
//ENDENUM
//
//Test_Scenario TestScenarioAStatus = InitialiseScenarioData

//Functions: WIDGETS

FLOAT DYNAMIC_MOVE_BLEND_RATIO
FLOAT SlideSpeed = 1.0
FLOAT BlendSpeed = 8.0
BOOL bLoopAnim = FALSE, bExtractbackwardvel = TRUE, bExtractsidevel = TRUE, bholdlastframe = FALSE
Int seatindex = -1

        
SEQUENCE_INDEX TEST_SEQ
int gtime = 1000

bool testprint = FALSE
bool flushFile = FALSE

//Vehicle damage
VECTOR vLocalDamage = <<0.0, 0.0, 0.0>>
FLOAT fDamage = 0
FLOAT fDeformation = 0
BOOL bLocalDamage = true
BOOL ApplyDamage = FALSE

FLOAT fTestDivingAwayFromVehiclePedHeading = 180.0
FLOAT fTestDivingAwayFromVehicleHorizontalOffset = 0.0
FLOAT fTestDivingAwayFromVehicleDistanceOffset = 40.0
BOOL bTestDivingAwayFromVehicleAlwaysSee = TRUE
BOOL bTestDivingAwayFromVehicleCanDiveAway = TRUE
BOOL bTestDivingAwayFromVehicleApplyChanges = FALSE

PROC CREATE_TEST_WIDGET ()
        START_WIDGET_GROUP("General Tests")
        SETUP_WIDGET () 
         	START_WIDGET_GROUP ("Vehicle Damage")
				ADD_WIDGET_BOOL ("Apply damage", ApplyDamage)
				ADD_WIDGET_FLOAT_SLIDER ("VehicleDamgePos.x",vLocalDamage.x,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("VehicleDamgePos.y",vLocalDamage.y,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("VehicleDamgePos.z",vLocalDamage.z,  -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("Damage",fDamage,  -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("fDeformation",fDeformation,  -2000, 2000, 0.5)
			 	ADD_WIDGET_BOOL ("LocalDamage", bLocalDamage)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Navigation")
                ADD_WIDGET_BOOL ("Start scenario", gBeginCombatScenario)
                ADD_WIDGET_BOOL ("Restart scenario", gResetCombatScenario)
                ADD_WIDGET_BOOL ("Reset to default", gResetToDefault)
                ADD_WIDGET_BOOL ("Debug scenario", gRun_debuggig)
                ADD_WIDGET_BOOL ("Print all setup info", gPrintAllIinfo)
                ADD_WIDGET_BOOL ("Test print", testprint)
                ADD_WIDGET_BOOL ("flushFilet", flushFile)
                ADD_WIDGET_FLOAT_SLIDER ("Start_Coord.x",gPedcoords.x,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Start_Coord.y",gPedcoords.y,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Start_Coord.z",gPedcoords.z,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Start_Heading",gPedheading,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Target_Coord.x",DynamicTargetPos.x,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Target_Coord.y",DynamicTargetPos.y,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Target_Coord.z",DynamicTargetPos.z,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Target_heading",DynamicTargetHeading,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Slide speed",SlideSpeed,  -2000, 2000, 0.5)
                ADD_WIDGET_INT_SLIDER ("time", gtime ,-2, 100000, 1 )
                ADD_WIDGET_INT_SLIDER ("seat", seatindex ,-2, 2, 1 )
				ADD_WIDGET_FLOAT_SLIDER ("Move Blend Ratio", DYNAMIC_MOVE_BLEND_RATIO, PEDMOVEBLENDRATIO_STILL, PEDMOVEBLENDRATIO_SPRINT, 0.1 )
                START_WIDGET_GROUP ("Anim")
                    ADD_WIDGET_BOOL ("loop anim", bLoopAnim )
                    ADD_WIDGET_BOOL ("Extract y vel", bExtractbackwardvel )
                    ADD_WIDGET_BOOL ("Extract x vel ", bExtractsidevel )
                    ADD_WIDGET_BOOL ("Hold ladt frame ", bholdlastframe )
                    ADD_WIDGET_FLOAT_SLIDER ("blend",BlendSpeed,  -2000, 2000, 0.5)
                STOP_WIDGET_GROUP()
            STOP_WIDGET_GROUP()
            START_WIDGET_GROUP ("Ped setup")
                ADD_WIDGET_INT_SLIDER ("Ped_index", gTestPedsIndex ,0, 9, 1 )
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Coord.x",gPedcoords.x,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Coord.y",gPedcoords.y,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Coord.z",gPedcoords.z,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Heading",gPedheading,  -2000, 2000, 0.5)
                    START_NEW_WIDGET_COMBO() 
                        ADD_TO_WIDGET_COMBO ("S_M_Y_COP_01")
                        ADD_TO_WIDGET_COMBO ("M_Y_HARLEM_01")
                    STOP_WIDGET_COMBO("Ped", PedIdentifier)
                        ADD_WIDGET_BOOL ("Create ped", gCreatePed)
                        ADD_WIDGET_BOOL ("Delete ped", gdeletePed)
                        ADD_WIDGET_INT_SLIDER ("Set Ped rel grp", gpedrelgrp  ,1, 4, 1 )
                    START_NEW_WIDGET_COMBO() 
                        ADD_TO_WIDGET_COMBO ("WEAPONTYPE_UNARMED")
                        ADD_TO_WIDGET_COMBO ("WEAPONTYPE_PISTOL")
                        ADD_TO_WIDGET_COMBO ("WEAPONTYPE_ASSAULTRIFLE")
                        ADD_TO_WIDGET_COMBO ("WEAPONTYPE_M4")
                    STOP_WIDGET_COMBO("weapon", WeaponIdentifier)
                        ADD_WIDGET_BOOL ("Swap current peds weapon", gSwapPedWeapons) 
                        ADD_WIDGET_BOOL ("print ped info", Bprint_ped_info)
                ADD_WIDGET_BOOL ("activate def area",gPedDefAActive)    
            STOP_WIDGET_GROUP ()
            START_WIDGET_GROUP ("Route Builder")
                ADD_WIDGET_FLOAT_SLIDER ("RoutePoint.x",gRoutePoint.x,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("RoutePoint.y",gRoutePoint.y,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("RoutePoint.z",gRoutePoint.z,  -2000, 2000, 0.25)
                ADD_WIDGET_INT_SLIDER ("Route Node", gRouteNode, 0, 7, 1 )
                ADD_WIDGET_BOOL ("Register Node", BSetNode)
                ADD_WIDGET_BOOL ("copy cam pos", BCopyCamCoords )
            STOP_WIDGET_GROUP ()    
			START_WIDGET_GROUP ("Test Diving Away From Vehicle")
				ADD_WIDGET_FLOAT_SLIDER ("Ped Heading", fTestDivingAwayFromVehiclePedHeading, 0.0, 360.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER ("Vehicle Horizontal Offset", fTestDivingAwayFromVehicleHorizontalOffset, -2.0, 2.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER ("Vehicle Distance Offset", fTestDivingAwayFromVehicleDistanceOffset, 0.0, 50.0, 1.0)
				ADD_WIDGET_BOOL ("Always See", bTestDivingAwayFromVehicleAlwaysSee)
				ADD_WIDGET_BOOL ("Can Dive Away", bTestDivingAwayFromVehicleCanDiveAway)
				ADD_WIDGET_BOOL ("Apply Changes", bTestDivingAwayFromVehicleApplyChanges)
			STOP_WIDGET_GROUP ()
        STOP_WIDGET_GROUP()
ENDPROC

// Functions end: WIDGET

//Mission flow House keeping


//PUPROSE: REmoves all the scenarios and resets all the relationships
    PROC Cleanup_Scenario_Entities ()
    int index = 0
        for index = 0 to MAX_NUMBER_OF_PEDS -1  
            CLEAN_UP_PED_ENTITIES (TestPeds[index].ped)
        ENDFOR 
        
        for index = 0 to MAX_NUMBER_OF_VEHICLES -1
            CLEAN_UP_VEHICLE_ENTITIES (TestVehicles[index].Vehicle )
        ENDFOR

        Set_Gang_Relationships (FALSE)  
    ENDPROC

PROC Terminate_test_script ()
    if IS_KEYBOARD_KEY_JUST_PRESSED (KEY_S)
        CLEAR_PRINTS ()
        CLEAR_HELP ()
        Cleanup_Scenario_Entities ()
        Temp_cleanup_scenario_cams ()
        SET_PLAYER_COLISION(scplayer, true)
        IF NOT IS_PED_INJURED (scplayer)
                  SET_PED_COORDS_KEEP_VEHICLE(scplayer, GET_PLAYER_START_POS () )
        ENDIF
        
        TERMINATE_THIS_THREAD ()
        
    ENDIF
ENDPROC


//END: House Keeping

//Test Specific Functions:

//FUNC SEQUENCE_INDEX FOLLOW_NAVMESH_TO_COORD_SEQ ( VECTOR L_Target_Pos, FLOAT L_Target_heading)
//      SEQUENCE_INDEX TEST_SEQUENCE
//      OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
//          TASK_FOLLOW_NAV_MESH_AND_SLIDE_TO_COORD (NULL, L_Target_Pos, INT_TO_ENUM (PEDMOVESTATE , (DYNAMIC_MOVE_TYPE +  2)    ),  -2, 0.25, L_Target_heading)
//      CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
//
//      RETURN TEST_SEQUENCE
//ENDFUNC

FUNC SEQUENCE_INDEX SLIDE_TO_COORD_SEQ ( VECTOR L_Target_Pos, FLOAT L_Target_heading)
        SEQUENCE_INDEX TEST_SEQUENCE
        OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
                TASK_PED_SLIDE_TO_COORD   (NULL, L_Target_Pos,  L_Target_heading, SlideSpeed)
        CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
        RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX SLIDE_TO_COORD_AND_PLAY_ANIM_SEQ ( VECTOR L_Target_Pos, FLOAT L_Target_heading, string animName, string AnimDict)
        SEQUENCE_INDEX TEST_SEQUENCE
        OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
            TASK_PED_SLIDE_TO_COORD_AND_PLAY_ANIM   (NULL, L_Target_Pos,  L_Target_heading, SlideSpeed, animName,  AnimDict, BlendSpeed,  bLoopAnim, bExtractbackwardvel, bExtractsidevel, bholdlastframe, 0)
        CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
        RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX TEST_TASK_OPEN_VEHICLE_DOOR(VEHICLE_INDEX vehicle, int time = -2)
        SEQUENCE_INDEX TEST_SEQUENCE
        OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
                TASK_OPEN_VEHICLE_DOOR  (NULL, vehicle,  time,INT_TO_ENUM (VEHICLE_SEAT, seatindex ) )
        CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
        RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX TEST_TASK_PLAY_ANIM(string anim)
    
        SEQUENCE_INDEX TEST_SEQUENCE
        OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
                TASK_PLAY_ANIM  (NULL, "misstest_anim",anim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1) 
        CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
        
        RETURN TEST_SEQUENCE
ENDFUNC                 


FUNC SEQUENCE_INDEX FOLLOW_ROUTE( RouteStruct &TargetRoute[])
        int index 
        BOOL BHaveValidNode = FALSE
        VECTOR TempNode
        
        //This allows us to pass a sequnce to a ped using 7 points for maximun testing. Iess than 7 nodes can be used, and the last valid node is copied into the remaining array elements.
        //Note all nodes must be in order there can be no gaps between them ie node 1 valid, 2 valid, 3 invalid is ok but not 1 valid, 2 invalid, 3  valid 
        //search our node array until we find a invalid array point
        for index = 0 to MAX_NUMBER_OF_NODES -1
            IF TargetRoute[index].RouteNodeState = FALSE
                IF index <> 0
                    TempNode = TargetRoute[index - 1].RouteNode //grab the previous point which is valid assuming that all nodes are sequential
                    BHaveValidNode = TRUE
                ELSE
                    SCRIPT_ASSERT ("Passed in sequnce with no valid nodes ")
                ENDIF
            ENDIF
            
            //copy this valid node into the remaing array elements
            IF (BHaveValidNode)
                    TargetRoute[index].RouteNode = TempNode 
            ENDIF
                
        ENDFOR  
        
        SEQUENCE_INDEX TEST_SEQUENCE
        OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[0].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -2, 1)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[1].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -2, 1)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[2].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -2, 1)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[3].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -2, 1)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[4].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -2, 1)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[5].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -2, 1)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[6].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -2, 1)
        CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
        RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX TEST_TASK_STAND_STILL(int time)
    
        SEQUENCE_INDEX TEST_SEQUENCE
    OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
            //  TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[0].routenode,   INT_TO_ENUM (PEDMOVESTATE , (DYNAMIC_MOVE_TYPE +  2)),  -1, 1)
            //  TASK_LOOK_AT_COORD (null,TargetRoute[1].routenode, time )
//              TASK_PAUSE (NULL, time)
//              TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[1].routenode,   INT_TO_ENUM (PEDMOVESTATE , (DYNAMIC_MOVE_TYPE +  2)),  -2, 1)
                TASK_GO_TO_ENTITY (null, TestVehicles[0].Vehicle, time, 0.25)
        CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
        RETURN TEST_SEQUENCE
ENDFUNC 

FUNC SEQUENCE_INDEX FOLLOW_PATROL_ROUTE ( STRING RouteName, PATROL_ALERT_STATE alert_state )
        SEQUENCE_INDEX TEST_SEQUENCE
        OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
            TASK_PATROL (null, RouteName, alert_state, true )
        CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)

        RETURN TEST_SEQUENCE
ENDFUNC

WEAPON_TYPE PedsWeapon
int NumberOfBullets = 0
vector vTempImpactPos

STRUCT SHOT_IMPACT
    Bool bImpact
    Vector vImpactPos
ENDSTRUCT

SHOT_IMPACT  WEAPON_IMPACTS[50]

PROC CLEAR_SHOTS_STRUCT(SHOT_IMPACT &shots[])
    int ArraySize = (COUNT_OF(shots) - 1) 
    int index 
    
    for index = 0 to ArraySize
        shots[index].bImpact = FALSE
        shots[index].vImpactPos = <<0.0, 0.0, 0.0>>
    ENDFOR
ENDPROC

    PROC DRAW_IMPACT_POINTS(SHOT_IMPACT &shots[])           
        int ArraySize = (COUNT_OF(shots) - 1) 
        int index 
            
        for  index = 0 to ArraySize
            IF shots[index].bImpact 
                DRAW_DEBUG_SPHERE (shots[index].vImpactPos, 0.04 )
            ENDIF
        ENDFOR
    ENDPROC

    //pistol - distance, 50m 


PROC SET_CAMERA(VECTOR vPos, VECTOR vRot)
    IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
        SET_CAM_COORD (TestCams[FixedCamera].cam , vPos)
        SET_CAM_ROT (TestCams[FixedCamera].cam ,  vRot  )
        SET_CAM_ACTIVE (TestCams[FixedCamera].cam, TRUE )   
    ELSE
        TestCams[FixedCamera].cam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vPos, vRot, 60.0, TRUE )
        RENDER_SCRIPT_CAMS (TRUE, FALSE)    
        SET_CAM_ACTIVE (TestCams[FixedCamera].cam, TRUE )   
    ENDIF   
ENDPROC

STRING PATHNAME = "X:/jimmy/"
STRING FileName = "Mytest.xml"
string mystring 

PROC ADD_STUFF_TO_debug ()
    OPEN_NAMED_DEBUG_FILE (PATHNAME , FileName )
        SAVE_INT_TO_NAMED_DEBUG_FILE  (10, PATHNAME, FileName)
        SAVE_NEWLINE_TO_NAMED_DEBUG_FILE (PATHNAME, FileName)
        SAVE_FLOAT_TO_NAMED_DEBUG_FILE (2820982309.555, PATHNAME, FileName)
        SAVE_NEWLINE_TO_NAMED_DEBUG_FILE (PATHNAME, FileName)
        SAVE_STRING_TO_NAMED_DEBUG_FILE (mystring, PATHNAME, FileName)
        SAVE_NEWLINE_TO_NAMED_DEBUG_FILE (PATHNAME, FileName)
    CLOSE_DEBUG_FILE ()
    
    OPEN_DEBUG_FILE ( )
        SAVE_INT_TO_DEBUG_FILE  (10)
        SAVE_NEWLINE_TO_DEBUG_FILE ()
        SAVE_FLOAT_TO_DEBUG_FILE (2820982309.555)
        SAVE_NEWLINE_TO_DEBUG_FILE ()
        SAVE_STRING_TO_DEBUG_FILE ("Test string")
        SAVE_NEWLINE_TO_DEBUG_FILE ()
    CLOSE_DEBUG_FILE ()
    
ENDPROC

//SET UP
PROC SETUP_PATROL_ROUTES ()

    OPEN_PATROL_ROUTE ("miss_guard_loop_1")
        ADD_PATROL_ROUTE_NODE ( 1, "StandGuard",  <<-32.04, 40.83, 6.35>> , <<0.0, 0.0, 0.0>>  )
        ADD_PATROL_ROUTE_NODE ( 2, "StandGuard",  <<-35.22, 8.20, 6.35>> , <<0.0, 0.0, 0.0>>  )
        ADD_PATROL_ROUTE_NODE ( 3, "StandGuard",  <<-18.09, 20.60, 6.35>> , <<0.0, 0.0, 0.0>>  )
    
        ADD_PATROL_ROUTE_LINK (1, 2)
        ADD_PATROL_ROUTE_LINK (2, 3)
        ADD_PATROL_ROUTE_LINK (3, 1)

    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
    
    OPEN_PATROL_ROUTE ("miss_guard_loop_2")
        ADD_PATROL_ROUTE_NODE ( 1, "StandGuard",  <<-41.18, 21.60, 6.35>> , <<0.0, 0.0, 0.0>>  )
        ADD_PATROL_ROUTE_NODE ( 2, "StandGuard",  <<-15.27, 27.30, 6.35>>, <<0.0, 0.0, 0.0>>  )
    
        ADD_PATROL_ROUTE_LINK (1, 2)
        ADD_PATROL_ROUTE_LINK (2, 1)

    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
    
ENDPROC 

//End Test Specific Functions

//These enums must match with the testNavmenu.xml

ENUM WeaponImpactFlags
    SetupWeaponImpacts,
    GetWeaponImpacts,
    DrawWeaponImpacts
ENDENUM
WeaponImpactFlags TestWeaponImpactPosStatus = SetupWeaponImpacts

ENUM SearchPedFlags
    SearchPedSetup, 
    SearchRunSearch
ENDENUM

SearchPedFlags SearchPedStatus = SearchPedSetup

ENUM TestReactionsStates
    TestReactionsSetup, 
    TestReactionsWait,
    TestReactionsShoot,
    TestReactionsRun
ENDENUM

INT iReactDirection = 1

TestReactionsStates TestReactionsStatus = TestReactionsSetup

PROC INITALISE_TEST_STATE()
        SingleTaskStatus = startSingleTask
        TestWeaponImpactPosStatus = SetupWeaponImpacts
        SearchPedStatus = SearchPedSetup
        TestReactionsStatus = TestReactionsSetup

ENDPROC

ENUM Scenarios
    TestSetup =0,
    CurrentBug = 1,
    ListeningGadgetTest = 2,
    CameraGadgetTest = 3,
    TestShotSpread =4,
    BugGetClosestped =5 ,
    TestReactions = 6,
    TestGuardChat = 7,
    TestReact1 = 8,
    TestInterruptableGetup = 9,
    TestGuardsSpotDeadPeds = 10,
    TestUnidentifiedPed = 11,
    SeekCoverOnlyTest =12 ,
    SurrenderTest =13,
    ClimbKillTest = 14,
    TaskChatTest= 15,
    Whistlingtest = 16,
    GuardFindingADeadPedTest = 17,
	MopAndBucketTest = 18,
	SyncMapObject = 19,
	TestDivingAwayFromVehicle = 20,
	TestLeaningForwardsScenario = 21,
	TestLeaningScenario = 22,
	TestBenchScenario = 23,
    DefaultTest = 1000
ENDENUM

OBJECT_INDEX mop = NULL
OBJECT_INDEX bucket = NULL
OBJECT_INDEX bench = NULL
PED_INDEX randomScenarioPed = NULL
ENTITY_INDEX randomScenarioEntity = NULL

//  END_SCENARIO: SquadOfPedsMovingBetweenVehicles  
PROC SETUP_TEST_DATA ()

    SWITCH int_to_enum (scenarios, gcurrentselection)
        CASE TestSetup
        CASE MopAndBucketTest
			REQUEST_ANIM_DICT("missfam5")
			
			TestPeds[0].PedsCoords = <<-4.62, 87.21, 6.35>>
            TestPeds[0].PedHeading = -84.10
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
			
		BREAK
		
        CASE CurrentBug
		case SyncMapObject
			 PlayerStartPos = <<-49.35, 24.07, 7.5>> 
		BREAK
		
		CASE TestDivingAwayFromVehicle
		
			gbPlayerShouldBeAtCamPos = FALSE
			PlayerStartPos = <<-2.9,67.0,8.8>>
		
			TestPeds[0].PedsCoords = <<-2.81, 93.63, 7.36>>
            TestPeds[0].PedHeading = fTestDivingAwayFromVehiclePedHeading
            TestPeds[0].Pedrelgrp = 0
            TestPeds[0].PedModel = S_F_M_MAID_01
			
			TestVehicles[0].VehicleCoords = TestPeds[0].PedsCoords + <<fTestDivingAwayFromVehicleHorizontalOffset, -fTestDivingAwayFromVehicleDistanceOffset, 0.0>>
            TestVehicles[0].VehicleHeading = 0.0
            TestVehicles[0].Vehiclemodel = TAILGATER
			
		BREAK
        
        CASE ListeningGadgetTest
            TestPeds[0].PedsCoords = << -2.4167, -1.4273, 1.1959>> 
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            
            IF NOT IS_PED_INJURED(scplayer)
                GIVE_WEAPON_TO_PED(scplayer, WEAPONTYPE_ASSAULTRIFLE, 0, TRUE)
            ENDIF
        BREAK

        CASE CameraGadgetTest
            TestPeds[0].PedsCoords = << -2.4167, -1.4273, 1.1959>> 
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            
            IF NOT IS_PED_INJURED(scplayer)
            ENDIF
        BREAK
        
        CASE TestShotSpread
            IF NOT IS_PED_INJURED (scplayer)
                  SET_PED_COORDS_KEEP_VEHICLE(scplayer, <<62, 0.0, 2.0>>)
            ENDIF
        BREAK
        
        CASE BugGetClosestped
            TestPeds[0].PedsCoords = << -2.4167, -1.4273, 1.1959>> 
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
        BREAK
        
        CASE TestReactions
            TestPeds[0].PedsCoords = <<8.33, 3.53, 6.35>> 
            TestPeds[0].PedHeading = -84.10
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            
            TestPeds[1].PedsCoords = <<8.38, -0.18, 6.35>> 
            TestPeds[1].PedHeading = 8.05
            TestPeds[1].Pedrelgrp = 2
            TestPeds[1].PedModel = S_M_Y_COP_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            
            IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
                SET_CAM_ACTIVE (TestCams[FixedCamera].cam, FALSE )  
            ENDIF
        BREAK
        
        CASE TestGuardChat
            TestPeds[0].PedsCoords = <<-32.04, 40.83, 6.35>> 
            TestPeds[0].PedHeading = -84.10
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            
            TestPeds[1].PedsCoords = <<-41.18, 21.60, 6.35>> 
            TestPeds[1].PedHeading = 8.05
            TestPeds[1].Pedrelgrp = 1
            TestPeds[1].PedModel = S_M_Y_COP_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE  
            
            IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
                SET_CAM_ACTIVE (TestCams[FixedCamera].cam, FALSE )  
            ENDIF
        BREAK
        
        CASE TestReact1
            TestPeds[0].PedsCoords = <<-32.05, -16.00, 6.35>> 
            TestPeds[0].PedHeading = -71.55
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            
            TestPeds[1].PedsCoords = <<-38.63, -2.44, 6.35>> 
            TestPeds[1].PedHeading = 86.51
            TestPeds[1].Pedrelgrp = 1
            TestPeds[1].PedModel = S_M_Y_COP_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE      
            
            TestPeds[2].PedsCoords = <<-57.18, -6.54, 6.35>> 
            TestPeds[2].PedHeading = 86.51
            TestPeds[2].Pedrelgrp = 2
            TestPeds[2].PedModel = S_M_Y_COP_01
            TestPeds[2].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE      
            
            IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
                SET_CAM_ACTIVE (TestCams[FixedCamera].cam, FALSE )  
            ENDIF
        BREAK
        
        CASE TestInterruptableGetup
            RENDER_SCRIPT_CAMS (FALSE, FALSE)
        BREAK
        
        CASE TestGuardsSpotDeadPeds
            // Dead ped
            TestPeds[0].PedsCoords = <<-31.33, -18.38, 6.35>> 
            TestPeds[0].PedHeading = -71.55
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            
            TestPeds[1].PedsCoords = <<-40.86, -7.63, 6.35>> 
            TestPeds[1].PedHeading = 86.51
            TestPeds[1].Pedrelgrp = 1
            TestPeds[1].PedModel = S_M_Y_COP_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE      
            
            IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
                SET_CAM_ACTIVE (TestCams[FixedCamera].cam, FALSE )  
            ENDIF
        BREAK
        
        CASE TestUnidentifiedPed
            TestPeds[0].PedsCoords = <<-37.31, -52.82, 6.35>> 
            TestPeds[0].PedHeading = 178.33
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            
            TestPeds[1].PedsCoords = <<-34.49, -24.42, 6.35>> 
            TestPeds[1].PedHeading = 86.51
            TestPeds[1].Pedrelgrp = 2
            TestPeds[1].PedModel = S_M_Y_COP_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE  
            
            IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
                SET_CAM_ACTIVE (TestCams[FixedCamera].cam, FALSE )  
            ENDIF
        BREAK
        
        CASE SeekCoverOnlyTest
            PlayerStartPos = <<22.35, 30.82, 6.35>> 
        
            TestPeds[0].PedsCoords = <<-31.54, -6.90, 6.35>> 
            TestPeds[0].PedHeading = -65
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            
            TestPeds[1].PedsCoords = <<-22.40, -3.79, 6.35>> 
            TestPeds[1].PedHeading = 105
            TestPeds[1].Pedrelgrp = 2
            TestPeds[1].PedModel = S_M_Y_COP_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            
            IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
                SET_CAM_ACTIVE (TestCams[FixedCamera].cam, FALSE )  
            ENDIF
        BREAK
        
        CASE SurrenderTest
        
            TestPeds[0].PedsCoords = <<19.23, 35.64, 6.35>> 
            TestPeds[0].PedHeading = -65
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            
            TestPeds[1].PedsCoords = <<26.92, 47.09, 6.35>> 
            TestPeds[1].PedHeading = -36.34
            TestPeds[1].Pedrelgrp = 1
            TestPeds[1].PedModel = S_M_Y_COP_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
        
            IF NOT IS_ENTITY_DEAD(scplayer)
           //     SET_PED_RELATIONSHIP_GROUP ( scplayer, RELGROUP_MISSION2 )
           //     SET_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION1, RELGROUP_MISSION2 )
            ENDIF
        BREAK
        
        CASE ClimbKillTest
        
            TestPeds[0].PedsCoords = <<-65.81, 159.72, 11.35>> 
            TestPeds[0].PedHeading = -178.07
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            
            TestPeds[1].PedsCoords = <<-64.87, 150.60, 11.35>> 
            TestPeds[1].PedHeading = -107.30
            TestPeds[1].Pedrelgrp = 1
            TestPeds[1].PedModel = S_M_Y_COP_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE  
        
            IF NOT IS_ENTITY_DEAD(scplayer)
            //    SET_PED_RELATIONSHIP_GROUP ( scplayer, RELGROUP_MISSION2 )
            //    SET_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION1, RELGROUP_MISSION2 )
            ENDIF
        BREAK
        
        CASE TaskChatTest
            TestPeds[0].PedsCoords = <<5.22, 46.52, 6.35>> 
            TestPeds[0].PedHeading = -125.97
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
        
            TestPeds[1].PedsCoords = <<20.66, 53.51, 6.35>> 
            TestPeds[1].PedHeading = -84.58
            TestPeds[1].Pedrelgrp = 1
            TestPeds[1].PedModel = S_M_Y_COP_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
        BREAK
        
        CASE Whistlingtest
            bStartedTasks = FALSE
            gAllowDebugging = FALSE // Don't trigger debug when going into stealth
            
            TestPeds[0].PedsCoords = <<-35.82, -12.80, 6.35>> 
            TestPeds[0].PedHeading = -176.44
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            
            IF NOT IS_ENTITY_DEAD(scplayer)
             //   SET_PED_RELATIONSHIP_GROUP ( scplayer, RELGROUP_MISSION2 )
            //    SET_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION1, RELGROUP_MISSION2 )
            ENDIF
        BREAK
        
        CASE GuardFindingADeadPedTest
        
            TestPeds[0].PedsCoords = <<-45.51, -29.78, 6.35>> 
            TestPeds[0].PedHeading = -176.44
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            
            TestPeds[1].PedsCoords = <<-52.81, -21.54, 6.35>> 
            TestPeds[1].PedHeading = -176.44
            TestPeds[1].Pedrelgrp = 1
            TestPeds[1].PedModel = S_M_Y_COP_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            
            TestPeds[2].PedsCoords = <<-49.50, -25.58, 6.35>> 
            TestPeds[2].PedHeading = -176.44
            TestPeds[2].Pedrelgrp = 1
            TestPeds[2].PedModel = S_M_Y_COP_01
            TestPeds[2].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
                
            IF NOT IS_ENTITY_DEAD(scplayer)
          //      SET_PED_RELATIONSHIP_GROUP ( scplayer, RELGROUP_MISSION2 )
              //  SET_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION1, RELGROUP_MISSION2 )
            ENDIF
        BREAK
		
		CASE TestLeaningForwardsScenario
			
			PlayerStartPos = <<9.645, 129.645, 8.396>>
		
			randomScenarioPed = CREATE_PED(PEDTYPE_CIVMALE, S_M_M_DOCKWORK_01, <<3.92, 129.763, 8.328>>, 270)
			randomScenarioEntity = GET_ENTITY_FROM_PED_OR_VEHICLE(randomScenarioPed)
			
		BREAK
		
		CASE TestLeaningScenario
		
			PlayerStartPos = <<9.645, 129.645, 8.396>>
			
			randomScenarioPed = CREATE_PED(PEDTYPE_CIVMALE, S_M_M_DOCKWORK_01, <<3.92, 129.763, 8.328>>, 270)
			randomScenarioEntity = GET_ENTITY_FROM_PED_OR_VEHICLE(randomScenarioPed)
			

		
		BREAK
		
		CASE TestBenchScenario
		
			PlayerStartPos = <<10.364, 128.887, 7.36>>
			
			randomScenarioPed = CREATE_PED(PEDTYPE_CIVMALE, S_M_M_DOCKWORK_01, <<0.364, 128.887, 7.36>>, 270)
			randomScenarioEntity = GET_ENTITY_FROM_PED_OR_VEHICLE(randomScenarioPed)
			
			REQUEST_MODEL(prop_bench_09)
       		WHILE NOT HAS_MODEL_LOADED(prop_bench_09)
				WAIT(0)
				REQUEST_MODEL(prop_bench_09)
        	ENDWHILE
			
			bench = CREATE_OBJECT(prop_bench_09, <<5.9, 132.313, 6.4>>)
			
			SET_ENTITY_ROTATION(bench, <<0, 0, 90>>)
		
		BREAK
        
    ENDSWITCH   
ENDPROC

PED_INDEX testped = NULL
int sceneId = -1

PROC PRE_RUN_TEST ()

	SWITCH int_to_enum (scenarios, gcurrentselection)       // the ENUM corresponds to the values from the XML file 
	
		CASE TestDivingAwayFromVehicle
		
			IF bTestDivingAwayFromVehicleApplyChanges
				
				//Set up the test data again.
				SETUP_TEST_DATA()
				
				//Reset the current scenario.
				gResetCombatScenario = TRUE
				
				//Clean up the current scenario.
				TestScenarioAStatus = CleanupScenario
				
				//Clear the flag.
				bTestDivingAwayFromVehicleApplyChanges = FALSE
				
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC RUN_TEST ()
    
        int index = 0
        VECTOR vCamPos 
        VECTOR vCamRot
        vector ScenePos = <<-51.05, 26.41, 7.53>>
      

       
        SWITCH int_to_enum (scenarios, gcurrentselection)       // the ENUM corresponds to the values from the XML file 
            //setup scenario
            CASE TestSetup
				if not IS_PED_INJURED(scplayer)
					GIVE_WEAPON_TO_PED(scplayer, GADGETTYPE_PARACHUTE, 1)
					SET_ENTITY_COORDS(scplayer, <<0, 0, 1500 >> )
					Set_Test_State_To_Default()
				ENDIF
			BREAK
        
            CASE CurrentBug
				PRINTSTRING("CurrentBug")
				PRINTNL()
	
				 
			//	SET_DEFAULT_VEHICLE_FOR_VEHICLE_FACTORY_WIDGET(POLICE)
				 
			
				 Set_Test_State_To_Default()
			BREAK
		
			CASE SyncMapObject
			 	REQUEST_ANIM_DICT("amb@newspaper")
				REQUEST_ANIM_DICT("map_objects")
			
				
				if (HAS_ANIM_DICT_LOADED("map_objects")  and HAS_ANIM_DICT_LOADED("amb@newspaper") )			
		
				
					if not IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
					
						sceneId = CREATE_SYNCHRONIZED_SCENE_AT_MAP_OBJECT ( ScenePos , 5.0 , Prop_news_disp_02a_S )
					
						PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM (ScenePos , 5.0 , Prop_news_disp_02a_S,  SceneId, "newspaperdispense_machine", "map_objects", INSTANT_BLEND_IN )
						if not IS_PED_INJURED(scplayer)
							TASK_SYNCHRONIZED_SCENE(scplayer, SceneId, "amb@newspaper" , "newspaperdispense_customer", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
						ENDIF
					ENDIF	
					
					if GET_SYNCHRONIZED_SCENE_PHASE(sceneId) = 1	
						STOP_SYNCHRONIZED_MAP_ENTITY_ANIM ( ScenePos,5.0, Prop_news_disp_02a_S, INSTANT_BLEND_OUT )
						if not IS_PED_INJURED(scplayer)
							STOP_SYNCHRONIZED_ENTITY_ANIM( scplayer, NORMAL_BLEND_OUT, true)
							CLEAR_PED_TASKS(scplayer)
						ENDIF
						Set_Test_State_To_Default()
					ENDIF	
				ENDIF
			BREAK
			
			CASE TestDivingAwayFromVehicle
			
				//Check if the player is not in a vehicle.
				GET_THE_PLAYER()
				IF NOT IS_PED_IN_ANY_VEHICLE(scplayer)
					
					//Put the player in the vehicle.
					SET_VEHICLE_ENGINE_ON(TestVehicles[0].Vehicle, TRUE, TRUE)
					SET_VEHICLE_UNDRIVEABLE(TestVehicles[0].Vehicle, FALSE)
					SET_PED_INTO_VEHICLE(scplayer, TestVehicles[0].Vehicle, VS_DRIVER)
					
					//We want the ped to react to events.
					Block_Peds_Temp_Events(TestPeds[0], FALSE)
					
					//Set the ped config flags.
					SET_PED_CONFIG_FLAG(TestPeds[0].Ped, PCF_AlwaysSeeApproachingVehicles, bTestDivingAwayFromVehicleAlwaysSee)
					SET_PED_CONFIG_FLAG(TestPeds[0].Ped, PCF_CanDiveAwayFromApproachingVehicles, bTestDivingAwayFromVehicleCanDiveAway)
					
				ENDIF
				
			BREAK
		
		
			CASE MopAndBucketTest
			
				if not IS_ENTITY_DEAD(scplayer)
					IF HAS_ANIM_SET_LOADED("move_ped_wpn_mop")
					
						if IS_BUTTON_JUST_PRESSED(PAD1, RIGHTSHOULDER1)
							FORCE_WEAPON_MOVEMENT_ANIM_GROUP(scplayer, "move_ped_wpn_mop")
							SET_PED_ANIM_GROUP(scplayer, "move_ped_mop")
							if not IS_ENTITY_ATTACHED(mop)
								ATTACH_ENTITY_TO_ENTITY(mop, scplayer, get_ped_bone_index(scplayer, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>) //bonetag_root
							endif 
							if IS_ENTITY_ATTACHED(bucket)
								DETACH_ENTITY(bucket)
							endif 
						ENDIF
						
						if IS_BUTTON_JUST_PRESSED(PAD1, LEFTSHOULDER1)
							FORCE_WEAPON_MOVEMENT_ANIM_GROUP(scplayer, "move_ped_wpn_bucket")
							SET_PED_ANIM_GROUP(scplayer, "move_ped_bucket")
							if not IS_ENTITY_ATTACHED(mop)
								ATTACH_ENTITY_TO_ENTITY(mop, scplayer, get_ped_bone_index(scplayer, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>) //bonetag_root
							endif 
							
							if not IS_ENTITY_ATTACHED(bucket)
								ATTACH_ENTITY_TO_ENTITY(bucket, scplayer, get_ped_bone_index(scplayer, BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
							endif 
						ENDIF
						
						if IS_BUTTON_JUST_PRESSED(PAD1, CIRCLE)
							RESET_WEAPON_MOVEMENT_ANIM_GROUP(scplayer)
							RESET_PED_ANIM_GROUP(scplayer)
							if IS_ENTITY_ATTACHED(mop)
								DETACH_ENTITY(mop)
							endif 
							if IS_ENTITY_ATTACHED(bucket)
								DETACH_ENTITY(bucket)
							endif 
						ENDIF

					ENDIF
				ENDIF
           				
            BREAK 
            
            CASE DefaultTest

            BREAK
            
            CASE ListeningGadgetTest

            BREAK

            CASE CameraGadgetTest

            BREAK
            
            CASE TestShotSpread
                SWITCH TestWeaponImpactPosStatus
                    
                    CASE SetupWeaponImpacts
                            
                            CLEAR_SHOTS_STRUCT(WEAPON_IMPACTS)
                            TestWeaponImpactPosStatus = GetWeaponImpacts
                    BREAK
                    
                    CASE GetWeaponImpacts
                        IF NOT IS_PED_INJURED (scplayer)
                            IF GET_CURRENT_PED_WEAPON(scplayer, PedsWeapon)
                                IF  GET_AMMO_IN_CLIP(scplayer, PedsWeapon, NumberOfBullets)
                                    IF GET_PED_LAST_WEAPON_IMPACT_COORD (scplayer, vTempImpactPos)
                                        IF NumberOfBullets > 0  
                                            FOR INDEX = 0 TO 49
                                                IF not WEAPON_IMPACTS[index].bImpact
                                                    WEAPON_IMPACTS[index].vImpactPos = vTempImpactPos
                                                    WEAPON_IMPACTS[index].bImpact = TRUE
                                                    INDEX = 50
                                                ENDIF
                                            ENDFOR
                                        
                                        ELSE
                                            SETTIMERA(0)
                                            TestWeaponImpactPosStatus = DrawWeaponImpacts
                                        ENDIF
                                    ENDIF
                                ENDIF
                            ENDIF
                        ENDIF   
                        
                        DRAW_IMPACT_POINTS(WEAPON_IMPACTS)  
                        
                    BREAK   
                        
                    CASE  DrawWeaponImpacts
                        IF NOT IS_PED_INJURED (scplayer)
                            IF TIMERA() > 500
                                IF IS_PED_SHOOTING (scplayer)
                                    TestWeaponImpactPosStatus = SetupWeaponImpacts
                                ENDIF
                            ENDIF
                        ENDIF
                    
                    DRAW_IMPACT_POINTS(WEAPON_IMPACTS)      
                        
                    BREAK

                ENDSWITCH
            BREAK
        
            CASE BugGetClosestped
                SWITCH SearchPedStatus
                    CASE SearchPedSetup
                        //  START_PED_SEARCH_CRITERIA ()
                            SearchPedStatus = SearchRunSearch
            
                    BREAK
                    
                    
                    CASE SearchRunSearch
                                
                        if  GET_CLOSEST_PED ( <<1.0, 1.0, 1.0>>, 50, FALSE, true, testped)
                            PRINTSTRING ("found a ped")
                            PRINTNL()
                        ENDIF
                
                        IF NOT IS_PED_INJURED (testped)
                              SET_PED_COORDS_KEEP_VEHICLE(testped, <<TestPeds[0].PedsCoords.x, TestPeds[0].PedsCoords.y, TestPeds[0].PedsCoords.z+ 5.0>>)
                        ENDIF
                    
                    BREAK
                
                
                
                
                ENDSWITCH
                    
        
            BREAK
            
            CASE TestReactions
            
                SWITCH TestReactionsStatus
                
                CASE TestReactionsSetup
                    vCamPos = <<-0.998314,3.204185,11.797215>>
                    vCamRot = <<-27.763252,-0.000040,-87.243950>>
                
                    SET_CAMERA(vCamPos,vCamRot)
                    
                    SWITCH (iReactDirection)
                        CASE 1 // Left
                              SET_PED_COORDS_KEEP_VEHICLE(TestPeds[1].ped,<<5.39, 9.09, 6.35>>)
                            SET_ENTITY_HEADING(TestPeds[1].ped,-1.58)
                            BREAK                   
                        CASE 2 // Top Left
                              SET_PED_COORDS_KEEP_VEHICLE(TestPeds[1].ped,<<17.18, 11.97, 6.35>>)
                            SET_ENTITY_HEADING(TestPeds[1].ped,-51.16)
                            BREAK
                        CASE 3 // Forward
                              SET_PED_COORDS_KEEP_VEHICLE(TestPeds[1].ped,<<19.14, 4.40, 6.35>>)
                            SET_ENTITY_HEADING(TestPeds[1].ped,-88.36)
                            BREAK
                        CASE 4 // Top right
                              SET_PED_COORDS_KEEP_VEHICLE(TestPeds[1].ped,<<15.79, -1.78, 6.35>>)
                            SET_ENTITY_HEADING(TestPeds[1].ped,-174.66)
                            BREAK   
                        CASE 5 // Right
                              SET_PED_COORDS_KEEP_VEHICLE(TestPeds[1].ped,<<6.13, -1.47, 6.35>>)
                            SET_ENTITY_HEADING(TestPeds[1].ped,-174.66)
                            BREAK
                        CASE 6 // Bottom right
                              SET_PED_COORDS_KEEP_VEHICLE(TestPeds[1].ped,<<2.99, -0.60, 6.35>>)
                            SET_ENTITY_HEADING(TestPeds[1].ped,-174.66)
                            BREAK   
                        CASE 7 // Back
                              SET_PED_COORDS_KEEP_VEHICLE(TestPeds[1].ped,<<4.49, 3.33, 6.35>>)
                            SET_ENTITY_HEADING(TestPeds[1].ped,90.06)
                            BREAK
                        DEFAULT // Bottom left
                              SET_PED_COORDS_KEEP_VEHICLE(TestPeds[1].ped,<<5.78, 7.58, 6.35>>)
                            SET_ENTITY_HEADING(TestPeds[1].ped,28.40)
                            BREAK           
                        
                    ENDSWITCH       
                
                    IF NOT IS_PED_INJURED (scplayer)
                          SET_PED_COORDS_KEEP_VEHICLE(scplayer, <<2.13, -8.26, 6.35>>)
                    ENDIF
                    
                    TASK_STAND_GUARD(TestPeds[0].Ped,TestPeds[0].PedsCoords,TestPeds[0].PedHeading,"Scenario_Stand_Guard")
                    
                    TestReactionsStatus = TestReactionsWait
                BREAK
                
                CASE TestReactionsWait
                    
                    // Let the guard stream in reaction anims
                    FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
                        IF NOT IS_PED_INJURED (Testpeds[index].Ped)
                            ADD_RELATIONSHIP_GROUP_TO_STRUCT (Testpeds[index])
                            Block_Peds_Temp_Events (Testpeds[index], FALSE)
                        ENDIF               
                    ENDFOR  
                        
                    WAIT(2000)
                    
                    TestReactionsStatus = TestReactionsShoot
                BREAK
                
                CASE TestReactionsShoot
                    FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
                        Block_Peds_Temp_Events (Testpeds[index], FALSE)
                    ENDFOR  
                    
                    IF NOT IS_PED_INJURED (Testpeds[1].Ped)
                        SWITCH (iReactDirection)
                        CASE 1 // Left
                            TASK_SHOOT_AT_COORD(Testpeds[1].Ped,<<9.70, 13.99, 6.350>>, 1000, FIRING_TYPE_1_BURST)
                            BREAK                   
                        CASE 2 // Top Left
                            TASK_SHOOT_AT_COORD(Testpeds[1].Ped,<<20.57, 16.26, 6.35>>, 1000, FIRING_TYPE_1_BURST)
                            BREAK
                        CASE 3 // Forward
                            TASK_SHOOT_AT_COORD(Testpeds[1].Ped,<<30.73, 4.68, 6.35>>, 1000, FIRING_TYPE_1_BURST)
                            BREAK
                        CASE 4 // Top right
                            TASK_SHOOT_AT_COORD(Testpeds[1].Ped,<<21.53, -6.71, 6.35>>, 1000, FIRING_TYPE_1_BURST)
                            BREAK   
                        CASE 5 // Right
                            TASK_SHOOT_AT_COORD(Testpeds[1].Ped,<<9.69, -6.10, 6.35>>, 1000, FIRING_TYPE_1_BURST)
                            BREAK
                        CASE 6 // Bottom right
                            TASK_SHOOT_AT_COORD(Testpeds[1].Ped,<<3.42, -1.19, 6.35>>, 1000, FIRING_TYPE_1_BURST)
                            BREAK   
                        CASE 7 // Back
                            TASK_SHOOT_AT_COORD(Testpeds[1].Ped,<<1.68, 3.14, 6.35>>, 1000, FIRING_TYPE_1_BURST)
                            BREAK
                        DEFAULT // Bottom left
                            TASK_SHOOT_AT_COORD(Testpeds[1].Ped,<<4.49, 10.33, 6.35>>, 1000, FIRING_TYPE_1_BURST)
                            BREAK               
                        ENDSWITCH           
                    ENDIF
                    
                    iReactDirection = iReactDirection + 1
                    
                    IF (iReactDirection > 8)
                        iReactDirection = 0
                    ENDIF
                            
                    TestReactionsStatus = TestReactionsRun
                BREAK
                
                CASE TestReactionsRun
                    FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
                        Block_Peds_Temp_Events (Testpeds[index], FALSE)
                    ENDFOR      
                BREAK
                
                ENDSWITCH
                
            BREAK
            
            CASE TestGuardChat
            
                vCamPos = <<-17.535797,18.522385,11.388047>>
                vCamRot = <<-13.469737,-0.000063,59.216499>>
                
                SET_CAMERA(vCamPos,vCamRot)
                
                FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
                    Block_Peds_Temp_Events(TestPeds[index], FALSE )
                ENDFOR
                
                IF NOT IS_PED_INJURED (scplayer)
                      SET_PED_COORDS_KEEP_VEHICLE(scplayer, <<2.13, -8.26, 6.35>>)
                ENDIF
                            
                SETUP_PATROL_ROUTES ()
                        
                TEST_SEQ = FOLLOW_PATROL_ROUTE ("miss_guard_loop_1", PAS_ALERT)
                PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
                
                TEST_SEQ = FOLLOW_PATROL_ROUTE ("miss_guard_loop_2", PAS_ALERT)
                PedPerformsSequence (TestPeds[1].ped, TEST_SEQ)
                //TASK_CHAT_TO_PED(TestPeds[0].ped, TestPeds[1].ped, TRUE, TRUE)
                //TASK_CHAT_TO_PED(TestPeds[1].ped, TestPeds[0].ped, FALSE, TRUE)       
                        
                Set_Test_State_To_Default()
            BREAK
            
            CASE TestReact1
                vCamPos = <<-22.147865,-5.238187,14.972472>>
                vCamRot = <<-27.736015,0.000001,96.991623>>
                
                SET_CAMERA(vCamPos,vCamRot)
                    
                FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
                    Block_Peds_Temp_Events(TestPeds[index], FALSE )
                ENDFOR
                
                IF NOT IS_PED_INJURED (scplayer)
                      SET_PED_COORDS_KEEP_VEHICLE(scplayer, <<2.13, -8.26, 6.35>>)
                ENDIF
                            
                TASK_GO_STRAIGHT_TO_COORD(TestPeds[2].ped, <<-57.35, -1.46, 6.35>>, PEDMOVE_WALK)
                        
                Set_Test_State_To_Default()
            BREAK
            
            CASE TestInterruptableGetup
                gbPlayerShouldBeAtCamPos = FALSE
                
                IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
                    SET_CAM_ACTIVE (TestCams[FixedCamera].cam, FALSE )  
                ENDIF
                
                IF DOES_CAM_EXIST (TestCams[TrackingCamera].cam)
                    SET_CAM_ACTIVE (TestCams[TrackingCamera].cam, TRUE )    
                ENDIF
                
                IF NOT IS_PED_INJURED (scplayer)
                      SET_PED_COORDS_KEEP_VEHICLE(scplayer, <<-20.33, -32.74, 20.81>>)
                ENDIF
                        
                Set_Test_State_To_Default()
            BREAK
            
            CASE TestGuardsSpotDeadPeds
                vCamPos = <<-22.321243,-3.941307,11.792035>>
                vCamRot = <<-11.021407,0.000016,118.724594>>
                
                SET_CAMERA(vCamPos,vCamRot)
                
                FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
                    Block_Peds_Temp_Events(TestPeds[index], FALSE )
                ENDFOR
                
                SET_PED_COMBAT_ATTRIBUTES(TestPeds[1].Ped,CA_IS_A_GUARD,TRUE)
                SET_ENTITY_HEALTH(TestPeds[0].Ped, 30)
                TASK_GO_TO_COORD_ANY_MEANS(TestPeds[1].Ped,TestPeds[0].PedsCoords,PEDMOVE_WALK,NULL)        
                
                Set_Test_State_To_Default()
            BREAK

            CASE TestUnidentifiedPed
                vCamPos = <<-35.433872,-21.467800,9.368533>>
                vCamRot = <<-9.972842,-0.000011,179.938751>>
                
                SET_CAMERA(vCamPos,vCamRot)
                
                FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
                    Block_Peds_Temp_Events(TestPeds[index], FALSE )
                ENDFOR
                
                SET_PED_COMBAT_ATTRIBUTES(TestPeds[1].Ped,CA_IS_A_GUARD,TRUE)
                SET_PED_STEALTH_ATTRIBUTES(TestPeds[0].Ped,SA_TREATED_AS_ACTING_SUSPCIOUSLY,TRUE)
                SET_PED_ID_RANGE(TestPeds[1].Ped,10.0)
                SET_PED_SEEING_RANGE(TestPeds[1].Ped,20.0)
                TASK_GO_TO_COORD_ANY_MEANS(TestPeds[1].Ped,TestPeds[0].PedsCoords,PEDMOVE_WALK,NULL)
                SET_PED_ID_RANGE(TestPeds[0].Ped,10.0)
                SET_PED_SEEING_RANGE(TestPeds[0].Ped,20.0)
                
                Set_Test_State_To_Default()
            BREAK
            
                    
            CASE SeekCoverOnlyTest
            
                vCamPos = <<-18.666416,-1.125346,9.091416>>
                vCamRot = <<-21.607365,-0.112191,113.084755>>
                
                SET_CAMERA(vCamPos,vCamRot)

                FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
                    Block_Peds_Temp_Events(TestPeds[index], FALSE )
                ENDFOR
                
                FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
                    IF NOT IS_PED_INJURED (Testpeds[index].Ped)
                        ADD_RELATIONSHIP_GROUP_TO_STRUCT (Testpeds[index])
                        Block_Peds_Temp_Events (Testpeds[index], FALSE)
                    ENDIF               
                ENDFOR  
                
                //IF NOT IS_PED_INJURED(scplayer)
                //    SET_PED_COORDS_KEEP_VEHICLE(scplayer,<<-20.40, 0.61, 6.35>>)
                //ENDIF
                
                SET_PED_COMBAT_ATTRIBUTES(TestPeds[0].Ped,CA_IS_A_GUARD,TRUE)
                SET_PED_COMBAT_ATTRIBUTES(TestPeds[0].Ped,CA_JUST_SEEK_COVER,TRUE)
                SET_PED_COMBAT_ATTRIBUTES(TestPeds[0].Ped,CA_BLIND_FIRE_IN_COVER,TRUE)
            
                SET_PED_COMBAT_ATTRIBUTES(TestPeds[1].Ped,CA_IS_A_GUARD,TRUE)
                
                Set_Test_State_To_Default()
            BREAK
            
            CASE SurrenderTest

                FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
                    IF NOT IS_PED_INJURED (Testpeds[index].Ped)
                        ADD_RELATIONSHIP_GROUP_TO_STRUCT (Testpeds[index])
                        SET_PED_COMBAT_ATTRIBUTES(TestPeds[index].Ped,CA_PLAY_REACTION_ANIMS,FALSE)
                        SET_PED_COMBAT_ATTRIBUTES(TestPeds[index].Ped,CA_IS_A_GUARD,TRUE)
                        Block_Peds_Temp_Events (Testpeds[index], FALSE)
                    ENDIF               
                ENDFOR  
                
                IF NOT IS_PED_INJURED(scplayer)
                      SET_PED_COORDS_KEEP_VEHICLE(scplayer,<<22.35, 30.82, 6.35>>)
                ENDIF
                            
                Set_Test_State_To_Default()
            BREAK
            
            CASE ClimbKillTest
                FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
                    IF NOT IS_PED_INJURED (Testpeds[index].Ped)
                        ADD_RELATIONSHIP_GROUP_TO_STRUCT (Testpeds[index])
                        SET_PED_COMBAT_ATTRIBUTES(TestPeds[index].Ped,CA_IS_A_GUARD,TRUE)
                        SET_PED_COMBAT_ATTRIBUTES(TestPeds[index].Ped,CA_CAN_INVESTIGATE,TRUE)
                        Block_Peds_Temp_Events (Testpeds[index], FALSE)
                    ENDIF               
                ENDFOR  
                
                IF NOT IS_PED_INJURED(scplayer)
                      SET_PED_COORDS_KEEP_VEHICLE(scplayer,<<-65.40, 161.27, 6.35>>)
                    SET_ENTITY_HEADING(scplayer, -142.85)
                ENDIF
                            
                Set_Test_State_To_Default()
            BREAK
            
            CASE TaskChatTest
            
                // Initialise test once
                IF NOT bStartedTasks
                    FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
                        IF NOT IS_PED_INJURED (Testpeds[index].Ped)
                            Block_Peds_Temp_Events (Testpeds[index], FALSE)
                        ENDIF               
                    ENDFOR  
                    
                    IF NOT IS_PED_INJURED(scplayer)
                          SET_PED_COORDS_KEEP_VEHICLE(scplayer,<<14.39, 62.89, 6.35>>)
                        SET_ENTITY_HEADING(scplayer, -167.25)
                    ENDIF       
                
                    TASK_CHAT_TO_PED( Testpeds[0].Ped, Testpeds[1].Ped, CF_GO_TO_SPECIFIC_POS + CF_USE_CUSTOM_HEADING , <<10.11, 54.15, 6.35>>, -60.26, 0.5) 
                    TASK_CHAT_TO_PED( Testpeds[1].Ped, Testpeds[0].Ped, CF_GO_TO_SPECIFIC_POS + CF_USE_CUSTOM_HEADING, <<14.18, 55.79, 6.35>>, -1.63, 0.5) 
                    
                    bStartedTasks = TRUE
                ENDIF   
                
                // Repeat the code below
                
                // This code makes each ped play anims in turn
                
                // If ped0 hasn't started an anim and is in position and ped1 isn't playing an anim, make him play it
                IF NOT bStartedAnim1
                    IF NOT IS_PED_INJURED(Testpeds[0].Ped)
                        AND IS_CHATTING_PED_IN_POSITION (Testpeds[0].Ped) 
                        AND NOT IS_CHATTING_PED_PLAYING_ANIM(Testpeds[0].Ped)
                        AND NOT IS_CHATTING_PED_PLAYING_ANIM(Testpeds[1].Ped)
                        MAKE_CHATTING_PED_PLAY_ANIM(Testpeds[0].Ped, "r_civi", "hands_up", NORMAL_BLEND_IN, 5000, AF_LOOPING)   
                        bStartedAnim1 = TRUE
                        bStartedAnim2 = FALSE
                    ENDIF
                ENDIF
                    
                // If ped0 has started the anim and has finished and ped1 isn't playing an anim, start our anim
                IF bStartedAnim1 
                AND NOT bStartedAnim2
                    IF NOT IS_PED_INJURED(Testpeds[1].Ped)
                        AND IS_CHATTING_PED_IN_POSITION (Testpeds[1].Ped) 
                        AND NOT IS_CHATTING_PED_PLAYING_ANIM(Testpeds[0].Ped)
                        AND NOT IS_CHATTING_PED_PLAYING_ANIM(Testpeds[1].Ped)
                        MAKE_CHATTING_PED_PLAY_ANIM(Testpeds[1].Ped, "r_civi", "cover_ears", NORMAL_BLEND_IN, 3000, AF_LOOPING) 
                        bStartedAnim1 = FALSE
                        bStartedAnim2 = TRUE
                    ENDIF
                ENDIF
                            
            BREAK
            
            CASE Whistlingtest  
            
                IF NOT bStartedTasks
                    FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
                        IF NOT IS_PED_INJURED (Testpeds[index].Ped)
                            ADD_RELATIONSHIP_GROUP_TO_STRUCT (Testpeds[index])
                            SET_PED_COMBAT_ATTRIBUTES(TestPeds[index].Ped,CA_IS_A_GUARD,TRUE)
                            SET_PED_COMBAT_ATTRIBUTES(TestPeds[index].Ped,CA_CAN_INVESTIGATE,TRUE)
                            Block_Peds_Temp_Events (Testpeds[index], FALSE)
                        ENDIF               
                    ENDFOR  
                    
                    IF NOT IS_PED_INJURED(scplayer)
                          SET_PED_COORDS_KEEP_VEHICLE(scplayer,<<-36.85, -3.55, 6.35>>)
                        SET_ENTITY_HEADING(scplayer, -83.89)
                    ENDIF
                    
                    bStartedTasks = TRUE
                ENDIF
                
                IF NOT bStartedAnim1
                    IF IS_PED_INVESTIGATING_WHISTLING_EVENT(TestPeds[0].Ped)
                        SET_INVESTIGATION_POSITION(TestPeds[0].Ped,<<-46.79, -3.39, 6.35>>) 
                        bStartedAnim1 = TRUE
                        Set_Test_State_To_Default()
                    ENDIF
                ENDIF       
                            
            BREAK
            
            CASE GuardFindingADeadPedTest
                FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
                    IF NOT IS_PED_INJURED (Testpeds[index].Ped)
                        SET_PED_COMBAT_ATTRIBUTES(TestPeds[index].Ped,CA_IS_A_GUARD,TRUE)
                        SET_PED_COMBAT_ATTRIBUTES(TestPeds[index].Ped,CA_CAN_INVESTIGATE,TRUE)
                        Block_Peds_Temp_Events (Testpeds[index], FALSE)
                    ENDIF               
                ENDFOR  
                
                SET_ENTITY_HEALTH(TestPeds[0].Ped,50)
                
                TASK_GO_TO_COORD_ANY_MEANS(TestPeds[1].Ped, <<-45.56, -27.07, 6.35>>, PEDMOVE_WALK, NULL)
                
                Set_Test_State_To_Default()
            BREAK
       
              
            CASE TestLeaningForwardsScenario
				IF NOT IS_PED_INJURED(randomScenarioPed)
					TASK_START_SCENARIO_IN_PLACE(randomScenarioPed, "WORLD_HUMAN_LEANING_FORWARDS", 0, TRUE)
					SET_ENTITY_AS_NO_LONGER_NEEDED(randomScenarioEntity)
				ENDIF
				
				Set_Test_State_To_Default()
			
			BREAK
			
			CASE TestLeaningScenario
				IF NOT IS_PED_INJURED(randomScenarioPed)
					TASK_START_SCENARIO_IN_PLACE(randomScenarioPed, "WORLD_HUMAN_LEANING", 0, TRUE)
					SET_ENTITY_AS_NO_LONGER_NEEDED(randomScenarioEntity)
				ENDIF
				
				Set_Test_State_To_Default()
			BREAK
			
			CASE TestBenchScenario
				IF NOT IS_PED_INJURED(randomScenarioPed)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(randomScenarioPed, FALSE)
					TASK_USE_NEAREST_SCENARIO_TO_COORD(randomScenarioPed, <<2.645, 129.645, 8.396>>, 10.0)
					SET_ENTITY_AS_NO_LONGER_NEEDED(randomScenarioEntity)
				ENDIF
				
				Set_Test_State_To_Default()
			BREAK
			

        
        
            
        ENDSWITCH   

ENDPROC


//Purpose: Runs the main scenarios
PROC Run_Test_Scenario ()
    int index = 0
    
    SWITCH TestScenarioAStatus
        //setps up all the data for the sceanrio, this is only called once or if selected in widget
        CASE InitialiseScenarioData
            PlayerStartPos = GET_PLAYER_START_POS()             //sets the players start coords at the default
            Bscenario_running = FALSE
            INITIALISE_PED_DATA (Testpeds)
            INITIALISE_VEHICLE_DATA (TestVehicles)
            INITIALISE_CAM_DATA(TestCams )
            SETUP_TEST_DATA ()
    
            HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
            TestScenarioAStatus = CreateScenarioEntities
            
            Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)     //sets the 
        
        BREAK
        
        // Creates all the scenario data
        CASE CreateScenarioEntities
            Bscenario_running = FALSE
            
            Set_Gang_Relationships (TRUE)   
        
            Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
        
            //peds
            FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
                CREATE_PED_ENTITY(Testpeds[index])
                GiveBlipsToPedsGrps (Testpeds[index])
                ALTER_COMBAT_STATS (Testpeds[index] )
                SWAP_PED_WEAPONS (Testpeds[index])
                ADD_RELATIONSHIP_GROUP_TO_STRUCT (Testpeds[index])
                Block_Peds_Temp_Events (Testpeds[index], TRUE)
                SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
            ENDFOR
            
            //TestVehicles
            FOR Index = 0 to MAX_NUMBER_OF_VEHICLES -1
                CREATE_VEHICLE_ENTITY (TestVehicles[index] )
            ENDFOR
        
            //TestCams
            for index = 0 to MAX_NUMBER_OF_CAMERAS -1
                CREATE_CAM_ENTITY (TestCams[index])
            ENDFOR
            
            TestScenarioAStatus = SetScenarioEntities
        BREAK
        
        CASE SetScenarioEntities
            
            Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
            
            Start_And_Reset_Test ()
			
			PRE_RUN_TEST ()
            
            IF gBeginCombatScenario
                TestScenarioAStatus = RunScenario
                gBeginCombatScenario = FALSE
                gRun_debuggig = FALSE
                Bscenario_running = TRUE
                INITALISE_TEST_STATE()
                TEMP_ACTIVATE_CAMS (TestCams[FixedCamera].cam )
                
                IF DOES_CAM_EXIST (TestCams[FixedCamera].cam )
                    HELP_TEXT_STATE =  HTF_SCENARIO_RUNNING_FIXED_CAM
                ELSE
                    HELP_TEXT_STATE = HTF_STARTED_RUNNING_NO_CAMS
                ENDIF
            ENDIF
        BREAK
        
        //Runs the actual selected scenario
        
        CASE RunScenario
                
            Temp_Run_Scenario_Tracking_Cam (TestPeds, MAX_NUMBER_OF_PEDS, TestCams[TrackingCamera].cam) 
            
            Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
                
            Start_And_Reset_Test ()
                
            Check_For_Scenario_Reset ()
            
            IF DOES_CAM_EXIST (TestCams[TrackingCamera].cam)
                if IS_CAM_RENDERING (TestCams[TrackingCamera].cam)
                    HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_TRACKING_CAM
                ELSE
                    HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_FIXED_CAM
                ENDIF
            ENDIF
                    
            RUN_TEST ()     //run the main tests        
            
        BREAK 
                
        CASE CleanupScenario
            
            HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
        
            Cleanup_Scenario_Entities ()
            
            IF gResetToDefault  
                Temp_cleanup_scenario_cams ()       //here we are changing scenarios so we need to reset cams
                TestScenarioAStatus = InitialiseScenarioData
                gResetToDefault = FALSE
            ENDIF
        
            IF gResetCombatScenario
                Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)
                gcurrentselection = gSelection 
                IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
                    ACTIVATE_CAM (TestCams[FixedCamera].cam)
                ELSE
                    Temp_cleanup_scenario_cams ()   
                ENDIF
                TestScenarioAStatus = CreateScenarioEntities
                gResetCombatScenario = FALSE
            ENDIF
        
        BREAK
        
    ENDSWITCH
ENDPROC

SCRIPT
    
    SET_DEBUG_ACTIVE (TRUE)
    
    //gvMapOffset = GET_PLAYER_START_POS ()
    
    SETUP_MISSION_XML_MENU (XMLMenu, KEY_Q )    //defined at the top of the file
    
    SETUP_AREA_FOR_MISSION (<<0.0, 0.0, 0.0>>)
    
    //Gets a reference to the player
    Get_The_Player ()
    
    //set player collios
    SET_PLAYER_COLISION(scplayer, true)
    
    //Sets the test widget from the test tools
    CREATE_TEST_WIDGET ()
    
    //request the test anim bank
    //REQUEST_TEST_ANIM_DICT ("misstest_anim")

    WHILE TRUE
            
        // controls the help text hides if xml menu is active
        TEXT_CONTROLLER ()
    
        // Can set all scenario peds invincible from the widget
        Set_Scenario_Peds_Invincible (TestPeds, MAX_NUMBER_OF_PEDS, gsetpedsinvincible )
    
        //User can create a debug cam for setting sceanrios
        Temp_Create_Debug_Cam (TestCams[FixedCamera]) 
        
        //Runs the selected option from the XML menu 
        Run_Selection_From_XML_input ()
        
        //Checks that a valid selection has been input and runs the scenario
        IF (gcurrentselection <> InvalidSelection)
    
            Draw_Debug_Info (  )
            
            //Sets the test scenario into debug mode
            IF (gAllowDebugging)
                Set_To_Debug ()
            ENDIF
            
            if (gRun_debuggig)
                Temp_Debug_Scenario (TestPeds, TestVehicles, Route, TestCams[FixedCamera].cam, S_M_Y_COP_01)                              //Allows the entities in the scneario to be adjusted
                Print_Scenario_Data (TestPeds, TestVehicles, Route, TestCams)
            ENDIF
            
            Run_Test_Scenario ()                                
            
            SWITCH_BETWEEN_FIXED_AND_TRACKING (TestCams[FixedCamera].cam)
            
            PRINT_ACTIVE_TEST (Bscenario_running, gRun_debuggig)        
		
			
        ENDIF
        
        Terminate_test_script ()
        
        WAIT (0)
    
    
    ENDWHILE

ENDSCRIPT


#ENDIF  // IS_DEBUG_BUILD
