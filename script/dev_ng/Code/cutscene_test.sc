// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "script_xml.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "commands_streaming.sch"
USING "commands_graphics.sch"
USING "commands_cutscene.sch"
USING "commands_debug.sch"
USING "test_tools.sch"
USING "commands_vehicle.sch"
USING "commands_camera.sch"
USING "commands_player.sch"
USING "script_DEBUG.sch"
USING "commands_script.sch"
USING "commands_entity.sch"
USING "commands_interiors.sch"

//Notes: This is script is to allow easy setup of test scenarios for the code department. It works in conjunction with test_tools.sch
//It  is a data driven system 

//Defaults values when the test is reset to factory the standard values are used

//vector gvMapOffset

//Level menu 
STRING XMLMenu = "Testbed/TestCutsceneMenu"
//VECTOR gvMapOffset //offset all coords from the origin by the psoition of the mapin world coords

//Ped Vars
CONST_INT MAX_NUMBER_OF_PEDS 			10

peds_struct TestPeds [MAX_NUMBER_OF_PEDS]

//Vehicle Vars
CONST_INT MAX_NUMBER_OF_VEHICLES 	 10

vehicle_struct TestVehicles[MAX_NUMBER_OF_VEHICLES]

//Route Vars 
CONST_INT MAX_NUMBER_OF_NODES 				7

RouteStruct Route[MAX_NUMBER_OF_NODES]

//Camera Vars 
CONST_INT MAX_NUMBER_OF_CAMERAS			2
CONST_INT FixedCamera										0
CONST_INT TrackingCamera									1

CONST_INT MAX_NUMBER_OF_OBJECTS 	 10

Object_struct TestObjects[MAX_NUMBER_OF_OBJECTS]


StCameraData TestCams[MAX_NUMBER_OF_CAMERAS]


BOOL gAllowDebugging = TRUE

//Test
//ENUM Test_Scenario
//	InitialiseScenarioData,
//	CreateScenarioEntities,
//	SetScenarioEntities,
//	RunScenario,
//	CleanupScenario
//ENDENUM
//
//Test_Scenario TestScenarioAStatus = InitialiseScenarioData

//Functions: WIDGETS


FLOAT TriggerOrient = 180
FLOAT triggerRadius = 11.0
FLOAT TriggerAngle = 90
VECTOR TriggerPos = <<0.0, 0.0, 0.0 >>
VECTOR vTestSceneOrigin 
int delayinload = 70


PROC CREATE_TEST_WIDGET ()
		
		START_WIDGET_GROUP("General Tests")
				ADD_WIDGET_INT_SLIDER ("TriggerOrient",delayinload,  0, 10000, 1)
				ADD_WIDGET_FLOAT_SLIDER ("TriggerOrient",TriggerOrient,  0, 360,1)
				ADD_WIDGET_FLOAT_SLIDER ("triggerRadius",triggerRadius,  0, 360,1)
				ADD_WIDGET_FLOAT_SLIDER ("TriggerAngle",TriggerAngle,  0, 360,1)
				ADD_WIDGET_FLOAT_SLIDER ("TriggerPos.x",TriggerPos.x,  -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("TriggerPos.y",TriggerPos.y,  -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("TriggerPos.z",TriggerPos.z,  -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("vTestSceneOrigin.x",vTestSceneOrigin.x,  -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("vTestSceneOrigin.y",vTestSceneOrigin.y,  -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("vTestSceneOrigin.z",vTestSceneOrigin.z,  -2000, 2000, 0.5)
		STOP_WIDGET_GROUP()
ENDPROC

// Functions end: WIDGET

//Mission flow House keeping


//PUPROSE: REmoves all the scenarios and resets all the relationships
	PROC Cleanup_Scenario_Entities ()
	int index = 0
		for index = 0 to MAX_NUMBER_OF_PEDS -1 	
			CLEAN_UP_PED_ENTITIES (TestPeds[index].ped)
		ENDFOR 
		
		for index = 0 to MAX_NUMBER_OF_VEHICLES -1
			CLEAN_UP_VEHICLE_ENTITIES (TestVehicles[index].Vehicle )
		ENDFOR

		FOR index = 0 TO MAX_NUMBER_OF_OBJECTS -1
			CLEAN_UP_OBJECT_ENTITIES(TestObjects[index].Object)
		ENDFOR	

		//Set_Gang_Relationships (FALSE)	
	ENDPROC

PROC Terminate_test_script ()
	if IS_KEYBOARD_KEY_JUST_PRESSED (KEY_S)
		CLEAR_PRINTS ()
		CLEAR_HELP ()
		Cleanup_Scenario_Entities ()
		Temp_cleanup_scenario_cams ()
		SET_PLAYER_COLISION(scplayer, true)
		IF NOT IS_PED_INJURED (scplayer)
				  SET_PED_COORDS_KEEP_VEHICLE(scplayer, GET_PLAYER_START_POS () )
		ENDIF
		
		TERMINATE_THIS_THREAD ()
		
	ENDIF
ENDPROC

//END: House Keeping
bool bPedSetForEnterState = FALSE
bool bPlayerSetForExitState = FALSE

ENUM CutSceneState
	CutSceneStart,
	CutSceneStreaming,
	CutScenePlaying, 
	CutSceneLoading, 
	CutScenePlayingAgain,
	CutscenePost
ENDENUM

CutSceneState CutSceneFlow = CutSceneStart

PROC INITALISE_TEST_STATE()
		SingleTaskStatus = startSingleTask
		CutSceneFlow = CutSceneStart
		
		bPedSetForEnterState = FALSE
		bPlayerSetForExitState = FALSE
		
ENDPROC

ENUM Scenarios
	CutSceneStandardTest = 0, 
	JanSeamlessTest = 1, 
	SCSPlayerTest1 = 2,
	SCSPlayerTest2 = 3,
	SCSPlayerTest3 = 4,
	SCSPlayerMeetsPed1 = 5,
	SCSPlayerMeetsPed2 = 6,
	SCSPlayerMeetsPed3 = 7,
	SCSPlayerMeetTriggerTest = 8, 
	SCSVehicleTest1 = 9,
	SCSVehicleTest2 = 10,
	SCSVehicleTest3 = 11,
	SCSPropTest1 = 12, 
	SCSPropTest2 = 13, 
	SCSPropTest3 = 14, 
	SCSSkinnedPropTest1 = 15, 
	SCSSkinnedPropTest2 = 16, 
	SCSSkinnedPropTest3 = 17, 
	JanSeamlessNonPed = 18, 
	SCSLowVehicleDetail = 19,
	PedExitWalkState = 20, 
	PedExitRunState = 21,
	PedExitIntoFreeFallState = 22, 
	PedExitIntoCoverState = 23, 
	VehicleDoorAndWheelTest = 24,
	SCSHeliboxTest = 25,
	WalkTest = 26,
	WalkTestSec = 27,
	VehicleTest2 = 28,
	ExpressionTest = 29, 
	PedExitWalkStateTask = 30,
	TestFailedToStreamAssetsInTime = 31,
	TestFailedToStreamCutFileInTime = 32,
	PedRegisteredDeadForScene = 33, 
	StopCutsceneViaScript = 34, 
	ExitSceneInVehicle = 35, 
	ExitWithWeapon = 36, 
	LoadUnloadTest = 37, 
	RayfireTest= 38, 
	OverlayTest = 39, 
	LoadingUnloading = 40, 
	offscreencrash = 41, 
	FamFourTest = 42, 
	BranchTestPart_2345 = 43, 
	BranchTestPart_1345 = 44, 
	DefaultTest = 1000
ENDENUM

string CutsceneName

			
//  END_SCENARIO: SquadOfPedsMovingBetweenVehicles  
PROC SETUP_TEST_DATA ()
	SWITCH int_to_enum (scenarios, gcurrentselection)
		CASE ExitWithWeapon
			vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
			PlayerStartPos = <<-3.83, 50.44, 6.35>>
			CutsceneName = "seamlessexit_wpn"
		BREAK
		
		CASE LoadUnloadTest
			vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
			PlayerStartPos = <<-3.83, 50.44, 6.35>>
		BREAK
		
		CASE RayfireTest
			PlayerStartPos = <<-115.87,6452.98,30.40 >> 
			CutsceneName = "cbh_helicrash"
		BREAK
		
		case FamFourTest
			CutsceneName = "family_4_int"
		
		BREAK
		
		CASE SCSHeliboxTest
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>> 
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 7.0
				TriggerOrient = 105.12
				TriggerAngle = 85
				
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				TestObjects[0].ObjectCoords = <<-3.83, 86.44, 6.35>>
			ENDIF
		
			CutsceneName = "biohiest_crate"
		
		BREAK
	
		CASE TestFailedToStreamAssetsInTime
		CASE CutSceneStandardTest
		CASE StopCutsceneViaScript
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 11.5
				TriggerOrient = 285.12
				TriggerAngle = 85
			ENDIF
			
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_GTA5)
				vTestSceneOrigin =  <<-1210.00, -1125.08, 6.67>>
				PlayerStartPos = <<-1234.96, -1135.65, 6.80>>
			ENDIF
			
			CutsceneName = "fbi_jan_sections"
			delayinload = 70
		BREAK
		
		CASE TestFailedToStreamCutFileInTime
				if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 11.5
				TriggerOrient = 285.12
				TriggerAngle = 85
			ENDIF
			
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_GTA5)
				vTestSceneOrigin =  <<-1210.00, -1125.08, 6.67>>
				PlayerStartPos = <<-1234.96, -1135.65, 6.80>>
			ENDIF
			
			CutsceneName = "fbi_jan_sections"
			delayinload = 0
		BREAK
		
		CASE WalkTest
	
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
				vTestSceneOrigin =  <<0.0, 0.0, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 15.0
				TriggerOrient = 0.0
				TriggerAngle = 0.0
			ENDIF
			
			CutsceneName = "walk_synctest"
		BREAK
		
		CASE WalkTestSec
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
				vTestSceneOrigin =  <<0.0, 0.0, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 15.0
				TriggerOrient = 0.0
				TriggerAngle = 0.0
			ENDIF
			
			CutsceneName = "walk_synctest"
		BREAK
		
		CASE SCSPlayerTest1
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 3.0
				TriggerOrient = 105.12
				TriggerAngle = 85
			ENDIF
			
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_GTA5)
				vTestSceneOrigin =  <<-1210.00, -1125.08, 6.67>>
				PlayerStartPos = <<-1234.96, -1135.65, 6.80>>
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 3.0
				TriggerOrient = 105.12
				TriggerAngle = 85
			ENDIF
			CutsceneName = "Seamlesstest1"
		BREAK
		CASE SCSPlayerTest2
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 3.0
				TriggerOrient = 285.12
				TriggerAngle = 152
			ENDIF
			
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_GTA5)
				vTestSceneOrigin =  <<-1210.00, -1125.08, 6.67>>
				PlayerStartPos = <<-1234.96, -1135.65, 6.80>>
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 3.0
				TriggerOrient = 285.12
				TriggerAngle = 0.0
			ENDIF
			CutsceneName = "Seamlesstest2"
		Break
		CASE SCSPlayerTest3
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 11.5
				TriggerOrient = 285.12
				TriggerAngle = 85
			ENDIF
			
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_GTA5)
				vTestSceneOrigin =  <<-1210.00, -1125.08, 6.67>>
				PlayerStartPos = <<-1234.96, -1135.65, 6.80>>
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 3.0
				TriggerOrient = 285.12
				TriggerAngle = 152
			ENDIF
			
			CutsceneName = "Seamlesstest3"
		
		BREAK
		
		CASE SCSPlayerMeetsPed1
	
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				TestPeds[0].PedsCoords = <<-3.83, 86.44, 6.35>>
				TestPeds[0].PedHeading = -176.44
				TestPeds[0].Pedrelgrp = 1
				TestPeds[0].PedModel = S_M_M_JANITOR
			
				PlayerStartPos = <<-3.83, 50.44, 6.35>>
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 11.5
				TriggerOrient = 180
				TriggerAngle = 85
			ENDIF
			
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_GTA5)
				vTestSceneOrigin =  <<-1210.00, -1125.08, 6.67>>
				TestPeds[0].PedsCoords = <<-1210.00, -1125.08, 6.67>>
				TestPeds[0].PedHeading = 112.30
				TestPeds[0].Pedrelgrp = 1
				TestPeds[0].PedModel = S_M_M_JANITOR
			
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 11.5
				TriggerOrient = 105.84
				TriggerAngle = 85
			
				PlayerStartPos = <<-1234.96, -1135.65, 6.80>>
			ENDIF
			
			CutsceneName = "Seamlesstest_2_a"
			
		BReak
		
		CASE PedRegisteredDeadForScene
				if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
			
				PlayerStartPos = <<-3.83, 50.44, 6.35>>
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 11.5
				TriggerOrient = 180
				TriggerAngle = 85
			ENDIF
		
			CutsceneName = "Seamlesstest_2_a"
		Break
			
		CASE SCSPlayerMeetsPed2
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 3.0
				TriggerOrient = 285.12
				TriggerAngle = 152
			ENDIF
			
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_GTA5)
				vTestSceneOrigin =  <<-1210.00, -1125.08, 6.67>>
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 3.0
				TriggerOrient = 110.12
				TriggerAngle = 129
			
				PlayerStartPos = <<-1234.96, -1135.65, 6.80>>
			ENDIF
		
			CutsceneName = "Seamlesstest_2_b"
			
		BREAK
		
		CASE SCSPlayerMeetsPed3
	
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>
				TestPeds[0].PedsCoords = <<-1210.00, -1125.08, 6.67>>
				TestPeds[0].PedHeading = 112.30
				TestPeds[0].Pedrelgrp = 1
				TestPeds[0].PedModel = S_M_M_JANITOR
			ENDIF
			
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_GTA5)
				vTestSceneOrigin =  <<-1210.00, -1125.08, 6.67>>
				TestPeds[0].PedsCoords = <<-1210.00, -1125.08, 6.67>>
				TestPeds[0].PedHeading = 112.30
				TestPeds[0].Pedrelgrp = 1
				TestPeds[0].PedModel = S_M_M_JANITOR
			
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 11.5
				TriggerOrient = 180
				TriggerAngle = 85
			
				PlayerStartPos = <<-1234.96, -1135.65, 6.80>>
			ENDIF
			
				CutsceneName = "Seamlesstest_2_c"
				
		BREAK
		
	
		CASE SCSPlayerMeetTriggerTest
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED) 
				vTestSceneOrigin =<<-36.82, -8.39, 6.35>>
				PlayerStartPos = <<-14.60, -6.91, 6.35>>	
			ENDIF
						
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_GTA5)
				vTestSceneOrigin =  <<-1210.00, -1125.08, 6.67>>
				PlayerStartPos = <<-1234.96, -1135.65, 6.80>>		
			ENDIF
		
		
			TestPeds[0].PedsCoords = vTestSceneOrigin
			TestPeds[0].PedHeading = -176.44
			TestPeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = S_M_M_JANITOR
			
		BREAK
		
		CASE JanSeamlessTest
		case JanSeamlessNonPed
		CutsceneName = "fbi_jan_sections"
			
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
				
			
				TestPeds[0].PedsCoords = <<-3.83, 86.44, 6.35>>
				TestPeds[0].PedHeading = -176.44
				TestPeds[0].Pedrelgrp = 1
				TestPeds[0].PedModel = S_M_M_JANITOR
				
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>
				TriggerPos = <<0.0, 0.0, 0.0>>
			 	TriggerRadius = 11.5
				TriggerOrient = 180
				TriggerAngle = 85
			ENDIF

			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_GTA5)
				vTestSceneOrigin = <<-104.11, -8.25, 65.35>>
		
				PlayerStartPos = << -75, -16, 68 >>
				
				bPedSetForEnterState = FALSE
				bPlayerSetForExitState = FALSE
				
				TestPeds[0].PedsCoords = << -106.76, -8.78, 69.52>> 
				TestPeds[0].PedHeading =  62.0425
				TestPeds[0].Pedrelgrp = 1
				TestPeds[0].PedModel = S_M_M_Janitor 
				
				TriggerRadius = 8.0
			
			ENDIF
		Break
		
		CASE ExpressionTest
			CutsceneName = "facial_combine"
			
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
				//	vTestSceneOrigin =  <<0.0, 0.0, 0.0>>
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				TestPeds[0].PedsCoords = <<-3.83, 86.44, 6.35>>
				TestPeds[0].PedHeading = -176.44
				TestPeds[0].Pedrelgrp = 1
				TestPeds[0].PedModel = S_M_M_JANITOR
			ENDIF	
		BREAK
		
		CASE SCSLowVehicleDetail
				if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED) 
				TriggerOrient = 180
 				triggerRadius = 11.0
 				TriggerAngle = 360
 				TriggerPos = <<0.0, 0.0, 0.0>>
				TestVehicles[0].VehicleCoords =<<-3.83, 86.44, 6.35>>
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>	
			ENDIF
			
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_GTA5)
				TestVehicles[0].VehicleCoords =<<-1210.00, -1125.08, 6.67>>
				vTestSceneOrigin =  <<-1210.00, -1125.08, 6.67>>
				PlayerStartPos = <<-1234.96, -1135.65, 6.80>>		
			ENDIF
			
			//TestVehicles[0].VehicleModel = SULTAN2_LO removed
			TestVehicles[0].VehicleModel = TAILGATER
			
			CutsceneName = "veh_test2"
		BREAK
		
		
		CASE VehicleTest2
		
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED) 
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>	
			ENDIF
			
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_GTA5)
				TestVehicles[0].VehicleCoords =<<-1210.00, -1125.08, 6.67>>
				vTestSceneOrigin =  <<-1210.00, -1125.08, 6.67>>
				PlayerStartPos = <<-1234.96, -1135.65, 6.80>>		
			ENDIF
			
			TestVehicles[0].VehicleModel = TAILGATER
			TestVehicles[0].VehicleCoords =<<-3.83, 86.44, 6.35>>
			CutsceneName = "veh_test2"
		BREAK
		
		
		CASE SCSVehicleTest1
		
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED) 
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>	
			ENDIF
			
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_GTA5)
				TestVehicles[0].VehicleCoords =<<-1210.00, -1125.08, 6.67>>
				vTestSceneOrigin =  <<-1210.00, -1125.08, 6.67>>
				PlayerStartPos = <<-1234.96, -1135.65, 6.80>>		
			ENDIF
			
			TestVehicles[0].VehicleModel = SULTAN
			TestVehicles[0].VehicleCoords =<<-3.83, 86.44, 6.35>>
			CutsceneName = "scsveh_test1"
		BREAK
		
		CASE SCSVehicleTest2
		
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED) 
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>	
			ENDIF
			
			TestVehicles[0].VehicleModel = SULTAN
			TestVehicles[0].VehicleCoords =<<-3.83, 86.44, 6.35>>
			CutsceneName = "scsveh_test2"
		BREAK
	
		CASE SCSVehicleTest3
		
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED) 
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>	
			ENDIF
			
			TestVehicles[0].VehicleModel = SULTAN
			TestVehicles[0].VehicleCoords =<<-3.83, 86.44, 6.35>>
			CutsceneName = "scsveh_test3"
		BREAK
		
		CASE VehicleDoorAndWheelTest
					if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED) 
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>	
			ENDIF
			
			TestVehicles[0].VehicleModel = TAILGATER
			TestVehicles[0].VehicleCoords =<<-3.83, 86.44, 6.35>>
			CutsceneName = "scsveh_test3"
		
		BREAK
	
	
		CASE SCSPropTest1
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED) 
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>	
			ENDIF
			
			TestObjects[0].ObjectModel= Prop_FBIBombPlant
			TestObjects[0].ObjectCoords =<<-3.83, 86.44, 6.35>>
			CutsceneName = "scs01_igprop"
	
		BREAK
	
		CASE SCSPropTest2
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED) 
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>	
			ENDIF
			
			TestObjects[0].ObjectModel= Prop_FBIBombPlant
			TestObjects[0].ObjectCoords =<<-3.83, 86.44, 6.35>>
			CutsceneName = "scs02_igprop"
		Break
	
		CASE SCSPropTest3
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED) 
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>	
			ENDIF
			CutsceneName = "scs03_igprop"
		Break
	
		
//		CASE SCSSkinnedPropTest1
//			if (GET_INDEX_OF_CURRENT_LEVEL() = testBed) 
//				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
//				PlayerStartPos = <<-3.83, 50.44, 6.35>>	
//			ENDIF
//			
//			TestObjects[0].ObjectModel= W_5_GLOCK38
//			TestObjects[0].ObjectCoords =<<-3.83, 86.44, 6.35>>
//			CutsceneName = "scs01_igsprop"
//	
//		BREAK
//	
//		CASE SCSSkinnedPropTest2
//			if (GET_INDEX_OF_CURRENT_LEVEL() = testBed) 
//				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
//				PlayerStartPos = <<-3.83, 50.44, 6.35>>	
//			ENDIF
//			
//			TestObjects[0].ObjectModel= W_5_GLOCK38
//			TestObjects[0].ObjectCoords =<<-3.83, 86.44, 6.35>>
//			CutsceneName = "scs02_igsprop"
//		Break
//	
//		CASE SCSSkinnedPropTest3
//			if (GET_INDEX_OF_CURRENT_LEVEL() = testBed) 
//				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
//				PlayerStartPos = <<-3.83, 50.44, 6.35>>	
//			ENDIF
//			
//			TestObjects[0].ObjectModel= W_5_GLOCK38
//			TestObjects[0].ObjectCoords =<<-3.83, 86.44, 6.35>>
//			CutsceneName = "scs03_igsprop"
//		Break
		
		CASE PedExitIntoFreeFallState
				if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
				vTestSceneOrigin =  <<21.3, -11.3, 10.35>>
				PlayerStartPos = <<-1.13, 69, 6.35>>
				TriggerPos = <<0.0, 0.0, 0.0>>
				TriggerRadius = 12.0
				TriggerOrient = 0
				TriggerAngle = 0
			ENDIF
			CutsceneName = "exit_jump"
	
		BREAK
		
		CASE PedExitRunState
				if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-1.13, 69, 6.35>>
				TriggerPos = <<0.0, 0.0, 0.0>>
				TriggerRadius = 12.0
				TriggerOrient = 0
				TriggerAngle = 0
			ENDIF
			CutsceneName = "exit_run"
	
		BREAK
		

		CASE PedExitWalkState
		CASE PedExitWalkStateTask
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-1.13, 69, 6.35>>
				TriggerPos = <<0.0, 0.0, 0.0>>
				TriggerRadius = 12.0
				TriggerOrient = 180
				TriggerAngle = 0
			ENDIF
			CutsceneName = "exit_walk"
	
		BREAK
		
		//test exiting from a cutscene into cover
		CASE PedExitIntoCoverState
			if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
			vTestSceneOrigin =  <<17.73, 178.80, 6.35>>
				PlayerStartPos = <<-1.13, 69, 6.35>>
				TriggerPos = <<0.0, 0.0, 0.0>>
				TriggerRadius = 1.5
				TriggerOrient = 180
				TriggerAngle = 0
			ENDIF
			CutsceneName = "exit_aim"
		BREAK
		
		CASE ExitSceneInVehicle
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>
				TriggerPos = <<0.0, 0.0, 0.0>>
				TriggerRadius = 1.5
				TriggerOrient = 180
				TriggerAngle = 0
		
			TestVehicles[0].VehicleModel = BJXL
			TestVehicles[0].VehicleCoords =<<-3.83, 86.44, 6.35>>
			
			TestPeds[0].PedsCoords = <<-3.83, 90.44, 6.35>>
			TestPeds[0].PedHeading = -176.44
			TestPeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PLAYER_ONE
			
			
			CutsceneName = "armenian_3_mcs_6"
		BREAK

		CASE OverlayTest
				vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
				PlayerStartPos = <<-3.83, 50.44, 6.35>>
				TestObjects[0].ObjectModel = PROP_TV_TEST
				TestObjects[0].ObjectCoords = <<-2.83, 86.44, 6.35>>
				TestObjects[0].ObjectRotation = <<0, 0, -180>>
				CutsceneName = "overlaytest"
		BREAK

		CASE LoadingUnloading
			vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
			PlayerStartPos = <<-3.83, 50.44, 6.35>>
						
			CutsceneName = "armenian_3_mcs_6"
		Break
	
		case offscreencrash
			vTestSceneOrigin =  <<-3.83, 86.44, 6.35>>
			PlayerStartPos = <<-3.83, 50.44, 6.35>>
			TestVehicles[0].VehicleModel = SEASHARK
			TestVehicles[0].VehicleCoords =<<-3.83, 86.44, 6.35>>
			CutsceneName = "family_2_mcs_3"
		BREAK
	
		CASE BranchTestPart_2345 
		CASE BranchTestPart_1345 
			CutsceneName = "FBI_3_INT"
			PlayerStartPos = <<-3.83, 50.44, 6.35>>
		BREAK
	ENDSWITCH	
ENDPROC

	BLIP_INDEX CutSceneBlip
	   NAVDATA navDatah
	   INTERIOR_INSTANCE_INDEX CutScenInstance
PROC RUN_TEST ()
		VECTOR vStreamArea = <<15.0 , 15.0 , 5.0>>
		

		SWITCH int_to_enum (scenarios, gcurrentselection)		// the ENUM corresponds to the values from the XML file 

			CASE SCSHeliboxTest
				SWITCH CutSceneFlow
					CASE CutSceneStart
						IF Not DOES_BLIP_EXIST(CutSceneBlip)
							CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
						ENDIF
						
						IF NOT IS_PED_INJURED(scplayer)
							if 	IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , vStreamArea)
								
								REQUEST_CUTSCENE(CutsceneName)
								CutSceneFlow = CutSceneStreaming
							ENDIF
						ENDIF
					BREAK
					
					CASE CutSceneStreaming
					//	SET_CUTSCENE_COORDS(vTestSceneOrigin)
						SET_CUTSCENE_TRIGGER_AREA(TriggerPos, triggerRadius, TriggerOrient, TriggerAngle)
						DISPLAY_CUTSCENE_TRIGGER_AREA()	
					
						IF NOT IS_PED_INJURED(scplayer)
							if HAS_CUTSCENE_LOADED()
								REGISTER_ENTITY_FOR_CUTSCENE(TestObjects[0].Object, "CS_Container_02", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								START_CUTSCENE()
								REMOVE_BLIP(CutSceneBlip)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						if not HAS_CUTSCENE_FINISHED()
							
						ELSE
							CutSceneFlow = CutscenePost
						ENDIF
					BREAK
				ENDSWITCH
			BREAK

			CASE CutSceneStandardTest
				SWITCH CutSceneFlow
					CASE CutSceneStart
						IF NOT IS_PED_INJURED(scplayer)
							REQUEST_AND_START_CUTSCENE(CutsceneName)
							CutSceneFlow = CutScenePlaying
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						if  HAS_CUTSCENE_FINISHED()
							Set_Test_State_To_Default()
						ENDIF
						
					BREAK
					
				ENDSWITCH	
			BREAK
				
			CASE TestFailedToStreamAssetsInTime
				SWITCH CutSceneFlow
					CASE CutSceneStart
							REQUEST_CUTSCENE(CutsceneName)
							wait(delayinload)
							START_CUTSCENE()
							CutSceneFlow = CutScenePlaying
					BREAK
					
					CASE CutScenePlaying
						if  HAS_CUTSCENE_FINISHED()
							Set_Test_State_To_Default()
						ENDIF
					BREAK
					
				ENDSWITCH	
			BREAK
			
			CASE FamFourTest
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF NOT IS_PED_INJURED(player_ped_id())
								set_entity_coords(player_ped_id(), <<-822.2418, 180.9213, 70.6951>>)
							ENDIF
						
					
							REQUEST_CUTSCENE(CutsceneName)
							
							CutScenInstance  =  GET_INTERIOR_AT_COORDS_WITH_TYPE( <<-800.31, 184.80, 72.72>>, "V_Michael" )

							PIN_INTERIOR_IN_MEMORY(CutScenInstance)	
							
							CutSceneFlow = CutSceneStreaming
					BREAK
					
					CASE CutSceneStreaming
						if HAS_CUTSCENE_LOADED()
							if  IS_INTERIOR_READY( CutScenInstance)
						
								START_CUTSCENE()
								
								CutSceneFlow = CutScenePlaying
							ENDIF	
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						if  HAS_CUTSCENE_FINISHED()
							UNPIN_INTERIOR (CutScenInstance )
							Set_Test_State_To_Default()
						ENDIF
					BREAK
					
				ENDSWITCH	
			BREAK
			
			CASE TestFailedToStreamCutFileInTime
				SWITCH CutSceneFlow
					CASE CutSceneStart
							REQUEST_CUTSCENE(CutsceneName)
							wait(delayinload)
							START_CUTSCENE()
							CutSceneFlow = CutScenePlaying
					BREAK
					
					CASE CutScenePlaying
						if  HAS_CUTSCENE_FINISHED()
							Set_Test_State_To_Default()
						ENDIF
					BREAK
					
				ENDSWITCH	
			BREAK
			
			CASE StopCutsceneViaScript
				SWITCH CutSceneFlow
					CASE CutSceneStart
							REQUEST_CUTSCENE(CutsceneName)
							wait(delayinload)
							START_CUTSCENE()
							STOP_CUTSCENE()
							CutSceneFlow = CutScenePlaying
					BREAK
					
					CASE CutScenePlaying
						if  HAS_CUTSCENE_FINISHED()
							Set_Test_State_To_Default()
						ENDIF
					BREAK
					
				ENDSWITCH	
			BREAK
			
			CASE SCSPlayerTest1
			CASE SCSPlayerTest2
			CASE SCSPlayerTest3
				SWITCH CutSceneFlow
					CASE CutSceneStart
						IF NOT IS_PED_INJURED(scplayer)
							REQUEST_CUTSCENE(CutsceneName)
							CutSceneFlow = CutSceneStreaming
						ENDIF
					BREAK
					
					CASE CutSceneStreaming
						//SET_CUTSCENE_COORDS(vTestSceneOrigin)
					
						IF NOT IS_PED_INJURED(scplayer)
							START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
							CutSceneFlow = CutScenePlaying
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						if  HAS_CUTSCENE_FINISHED()
							Set_Test_State_To_Default()
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE WalkTestSec
			CASE WalkTest
					SWITCH CutSceneFlow
						CASE CutSceneStart
							DRAW_DEBUG_LINE(<<0.0, 0.0, 6.38>>, <<0.0, 49.12, 6.38>>)
							REQUEST_CUTSCENE(CutsceneName)
							CutSceneFlow = CutSceneStreaming
						BREAK
						
						CASE CutSceneStreaming
							DRAW_DEBUG_LINE(<<0.0, 0.0, 6.38>>, <<0.0, 49.12, 6.38>>)
							if HAS_CUTSCENE_LOADED()
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
							ENDIF
							
						BREAK
						
						CASE CutScenePlaying
							DRAW_DEBUG_LINE(<<0.0, 0.0, 6.38>>, <<0.0, 49.12, 6.38>>)
							if not HAS_CUTSCENE_FINISHED()
								
							ELSE
								CutSceneFlow = CutscenePost
							ENDIF
						BREAK
				ENDSWITCH
			BREAK
			
			CASE ExpressionTest
					SWITCH CutSceneFlow
						CASE CutSceneStart
								REQUEST_CUTSCENE(CutsceneName)
								CutSceneFlow = CutSceneStreaming
						BREAK
						
						CASE CutSceneStreaming
							if HAS_CUTSCENE_LOADED()
								REGISTER_ENTITY_FOR_CUTSCENE(TestPeds[0].Ped, "z_z_MAXTEST", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
							ENDIF
						BREAK
						
						CASE CutScenePlaying
							if not HAS_CUTSCENE_FINISHED()
								
							ELSE
								CutSceneFlow = CutscenePost
							ENDIF
						BREAK
				ENDSWITCH
			BREAK
			
//Seamless test 			
			//player walks up to ped in world
			CASE PedRegisteredDeadForScene
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
							
							IF NOT IS_PED_INJURED(scplayer)
								if 	IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , vStreamArea)
									REQUEST_CUTSCENE(CutsceneName)
									CutSceneFlow = CutSceneStreaming
								ENDIF
							ENDIF
					BREAK
					
					CASE CutSceneStreaming
						IF NOT IS_PED_INJURED(scplayer)
					
							SET_CUTSCENE_TRIGGER_AREA(TriggerPos, triggerRadius, TriggerOrient, TriggerAngle)
							DISPLAY_CUTSCENE_TRIGGER_AREA()	
						
							if HAS_CUTSCENE_LOADED()
								REMOVE_BLIP(CutSceneBlip)
								REGISTER_ENTITY_FOR_CUTSCENE( null ,"s_m_m_janitor", CU_DONT_ANIMATE_ENTITY)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
				ENDSWITCH
			BREAK
			
			
			CASE SCSPlayerMeetsPed1
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
							
							IF NOT IS_PED_INJURED(scplayer)
								if 	IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , vStreamArea)
									REQUEST_CUTSCENE(CutsceneName)
									CutSceneFlow = CutSceneStreaming
								ENDIF
							ENDIF
					BREAK
					
					CASE CutSceneStreaming
						IF NOT IS_PED_INJURED(scplayer)
							SET_CUTSCENE_TRIGGER_AREA(TriggerPos, triggerRadius, TriggerOrient, TriggerAngle)
							DISPLAY_CUTSCENE_TRIGGER_AREA()	
						
							if HAS_CUTSCENE_LOADED()
								REMOVE_BLIP(CutSceneBlip)
								REGISTER_ENTITY_FOR_CUTSCENE(TestPeds[0].ped, "s_m_m_janitor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
				ENDSWITCH
			BREAK
			
			//player meets ped in world
			CASE SCSPlayerMeetsPed2
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
						
						IF NOT IS_PED_INJURED(scplayer)
							if 	IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , vStreamArea)
								REQUEST_CUTSCENE(CutsceneName)
								CutSceneFlow = CutSceneStreaming
							ENDIF
						ENDIF
					BREAK
					
					CASE CutSceneStreaming

						SET_CUTSCENE_TRIGGER_AREA(TriggerPos, triggerRadius, TriggerOrient, TriggerAngle)
						DISPLAY_CUTSCENE_TRIGGER_AREA()	
						
						IF NOT IS_PED_INJURED(scplayer)
							if HAS_CUTSCENE_LOADED()
								REMOVE_BLIP(CutSceneBlip)
								REGISTER_ENTITY_FOR_CUTSCENE(NULL, "s_m_m_janitor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						if not HAS_CUTSCENE_FINISHED()
							
							if not DOES_ENTITY_EXIST(testpeds[0].ped)
								testpeds[0].ped = GET_PED_INDEX_FROM_ENTITY_INDEX(   GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("s_m_m_janitor" ))
							ENDIF	
							
							DISPLAY_CUTSCENE_TRIGGER_AREA()
						ELSE
							if not IS_PED_INJURED(testpeds[0].ped)
								TASK_FOLLOW_NAV_MESH_TO_COORD(testpeds[0].ped, <<6.10, 95.46, 6.35>>, PEDMOVE_WALK)
								PRINTSTRING("told to walk")
								PRINTNL()
							ENDIF
							
							CutSceneFlow = CutscenePost
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			//player meets ped and then the ped is deleted
			CASE SCSPlayerMeetsPed3
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
						
						IF NOT IS_PED_INJURED(scplayer)
							if 	IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , vStreamArea)
								REQUEST_CUTSCENE(CutsceneName)
							
								CutSceneFlow = CutSceneStreaming
							ENDIF
						ENDIF
						PRINTNL()
					BREAK
					
					CASE CutSceneStreaming
						IF NOT IS_PED_INJURED(scplayer)
						
							DISPLAY_CUTSCENE_TRIGGER_AREA()
						
							SET_CUTSCENE_TRIGGER_AREA(TriggerPos, triggerRadius,TriggerOrient, TriggerAngle)
							
							if HAS_CUTSCENE_LOADED()
								REMOVE_BLIP(CutSceneBlip)
								REGISTER_ENTITY_FOR_CUTSCENE(TestPeds[0].ped, "s_m_m_janitor", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin )
								SET_PED_AS_NO_LONGER_NEEDED(TestPeds[0].ped)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						if not HAS_CUTSCENE_FINISHED()
							
						ELSE
							CutSceneFlow = CutscenePost
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			

	
			
			//Test the triggering of the cutscene
			CASE SCSPlayerMeetTriggerTest
				SWITCH CutSceneFlow
					CASE CutSceneStart
						IF Not DOES_BLIP_EXIST(CutSceneBlip)
							CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
						ENDIF
					
						IF NOT IS_PED_INJURED(scplayer)
							if 	IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , vStreamArea)
								REQUEST_CUTSCENE("Seamlesstest_2_a")
								
								CutSceneFlow = CutSceneStreaming
							ENDIF
						ENDIF
					BREAK
					
					CASE CutSceneStreaming
						
	
						SET_CUTSCENE_TRIGGER_AREA(TriggerPos, triggerRadius, TriggerOrient, TriggerAngle)
					
						
						DISPLAY_CUTSCENE_TRIGGER_AREA()	
						
						IF NOT IS_PED_INJURED(scplayer)
							if HAS_CUTSCENE_LOADED()
								REGISTER_ENTITY_FOR_CUTSCENE(TestPeds[0].ped, "s_m_m_janitor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								REMOVE_BLIP(CutSceneBlip)
								CutSceneFlow = CutScenePlaying
								PRINTSTRING("Can Trigger cutscene" )
								PRINTNL()
							ENDIF
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						if not HAS_CUTSCENE_FINISHED()
							DISPLAY_CUTSCENE_TRIGGER_AREA()
						ELSE
							CutSceneFlow = CutscenePost
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			
			//player walks up to ped in world
			
			
			CASE JanSeamlessNonPed
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
							
							IF NOT IS_PED_INJURED(scplayer)
								if 	IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , <<30.0, 30.0, 30.0>>)
									REQUEST_CUTSCENE(CutsceneName)
									
									CutSceneFlow = CutSceneStreaming
								ENDIF
							ENDIF
					BREAK
					
					CASE CutSceneStreaming
						IF NOT IS_PED_INJURED(scplayer)
							if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_GTA5)
								if IS_ENTITY_IN_ANGLED_AREA( scplayer, <<-100.9608 , -9.5618 , 67.6090 >> , <<-97.6742 , -10.5727 , 65.3198 >> , 1.750)
									REMOVE_BLIP(CutSceneBlip)
									START_CUTSCENE()
									CutSceneFlow = CutScenePlaying
								ENDIF
							ENDIF	
						ENDIF
						
						IF NOT IS_PED_INJURED(scplayer)
							if (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_TESTBED)
									if 	IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , <<15.0, 15.0, 30.0>>)
										REMOVE_BLIP(CutSceneBlip)
										PRINTSTRING("START_CUTSCENE_AT_COORDS(vTestSceneOrigin )")
										PRINTNL()
										SET_CUTSCENE_TRIGGER_AREA(<<0.0, 0.0, 0.0>>, 5.0, -70.0, 360.0)
										
										START_CUTSCENE_AT_COORDS(vTestSceneOrigin )
										
										CutSceneFlow = CutScenePlaying
								ENDIF
							ENDIF	
						ENDIF
					BREAK
					
					CASE CutScenePlaying
					if NOT HAS_CUTSCENE_FINISHED()
							IF Not IS_PED_INJURED(TestPeds[0].ped)
								IF NOT bPedSetForEnterState
									if CAN_SET_ENTER_STATE_FOR_REGISTERED_ENTITY("s_m_m_janitor")
									
										CLEAR_ALL_PED_PROPS( TestPeds[0].ped)
										bPedSetForEnterState = TRUE
									ENDIF
								ENDIF
							ENDIF
							
							if not bPlayerSetForExitState
								if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("CS_Player")
									IF Not IS_PED_INJURED(scplayer)
										SET_PED_COMPONENT_VARIATION(scplayer,  PED_COMP_SPECIAL2, 1, 0)
										bPlayerSetForExitState = true
										PRINTSTRING("made bag swap")
										PRINTNL()
									ENDIF
								ENDIF
							ENDIF	
						ENDIF
					BREAK
					
				ENDSWITCH
			BREAK
			
			
			CASE JanSeamlessTest
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
							
							IF NOT IS_PED_INJURED(scplayer)
								if 	IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , <<30.0, 30.0, 30.0>>)
									REQUEST_CUTSCENE(CutsceneName)
									CutSceneFlow = CutSceneStreaming
								ENDIF
							ENDIF
					BREAK
					
					CASE CutSceneStreaming
						IF NOT IS_PED_INJURED(scplayer)
								if HAS_CUTSCENE_LOADED()
									REMOVE_BLIP(CutSceneBlip)
									REGISTER_ENTITY_FOR_CUTSCENE(TestPeds[0].ped, "s_m_m_janitor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
									START_CUTSCENE()
									CutSceneFlow = CutScenePlaying
								ENDIF
						ENDIF
					
					BREAK
					
					CASE CutScenePlaying
					BREAK
					
				ENDSWITCH
			BREAK
			
		//-- VEHICLES --	
		
			CASE SCSLowVehicleDetail
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
						
							IF NOT IS_PED_INJURED(scplayer)
								if 	IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , vStreamArea)
								
									if IS_VEHICLE_DRIVEABLE(TestVehicles[0].Vehicle)
										REQUEST_VEHICLE_HIGH_DETAIL_MODEL(TestVehicles[0].Vehicle )
									ENDIF	
									
									REQUEST_CUTSCENE(CutsceneName)
									
									CutSceneFlow = CutSceneStreaming
								ENDIF
							ENDIF
					BREAK
					
					CASE CutSceneStreaming
						IF NOT HAS_CUTSCENE_LOADED()
							REGISTER_ENTITY_FOR_CUTSCENE(TestVehicles[0].Vehicle, "tailgater", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						
						ENDIF
					BREAK
					
				ENDSWITCH
			BREAK
			
			CASE VehicleTest2
						SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
							
							IF NOT IS_PED_INJURED(scplayer)
								if 	IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , vStreamArea)
									REQUEST_CUTSCENE(CutsceneName)
									
									CutSceneFlow = CutSceneStreaming
								ENDIF
							ENDIF
					BREAK
					
					CASE CutSceneStreaming
						if HAS_CUTSCENE_LOADED()
								REMOVE_BLIP(CutSceneBlip)
								REGISTER_ENTITY_FOR_CUTSCENE(TestVehicles[0].Vehicle, "tailgater", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
						ENDIF
					BREAK
					
				ENDSWITCH
			BREAK
			
			CASE SCSVehicleTest1
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
							
							IF NOT IS_PED_INJURED(scplayer)
								if 	IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , vStreamArea)
									REQUEST_CUTSCENE(CutsceneName)
									
									CutSceneFlow = CutSceneStreaming
								ENDIF
							ENDIF
					BREAK
					
					CASE CutSceneStreaming
						IF NOT IS_PED_INJURED(scplayer)
							//SET_CUTSCENE_COORDS(vTestSceneOrigin)
						
							if HAS_CUTSCENE_LOADED()
								REMOVE_BLIP(CutSceneBlip)
								REGISTER_ENTITY_FOR_CUTSCENE(TestVehicles[0].Vehicle, "sultan" , CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
				ENDSWITCH
			BREAK
			
			CASE SCSVehicleTest2
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
							IF NOT IS_PED_INJURED(scplayer)
								if 	IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , vStreamArea)
									REQUEST_CUTSCENE(CutsceneName)
									
									CutSceneFlow = CutSceneStreaming
								ENDIF
							ENDIF
					BREAK
					
					CASE CutSceneStreaming
						IF NOT IS_PED_INJURED(scplayer)
						
							if HAS_CUTSCENE_LOADED()
								REMOVE_BLIP(CutSceneBlip)
								REGISTER_ENTITY_FOR_CUTSCENE(TestVehicles[0].Vehicle, "sultan", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
				ENDSWITCH
			BREAK
			
			CASE SCSVehicleTest3
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
							
								IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , vStreamArea, FALSE)
							
							IF NOT IS_PED_INJURED(scplayer)
								if 	IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , vStreamArea)
									REQUEST_CUTSCENE(CutsceneName)
									
									CutSceneFlow = CutSceneStreaming
								ENDIF
							ENDIF
					BREAK
					
					CASE CutSceneStreaming
						IF NOT IS_PED_INJURED(scplayer)
						
							 if HAS_CUTSCENE_LOADED()
								REMOVE_BLIP(CutSceneBlip)
								REGISTER_ENTITY_FOR_CUTSCENE(TestVehicles[0].Vehicle, "sultan", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
				ENDSWITCH
			BREAK
			
			CASE SCSPropTest1
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
							
							IF NOT IS_PED_INJURED(scplayer)
								if 	IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , vStreamArea)
									REQUEST_CUTSCENE(CutsceneName)
									REGISTER_ENTITY_FOR_CUTSCENE(TestObjects[0].Object, "Prop_FBIBombPlant", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
									CutSceneFlow = CutSceneStreaming
								ENDIF
							ENDIF
					BREAK
					
					CASE CutSceneStreaming
						IF NOT IS_PED_INJURED(scplayer)
							//SET_CUTSCENE_COORDS(vTestSceneOrigin)
							SET_CUTSCENE_TRIGGER_AREA(TriggerPos, triggerRadius, TriggerOrient, TriggerAngle)
							DISPLAY_CUTSCENE_TRIGGER_AREA()	
						
							if HAS_CUTSCENE_LOADED()
								REMOVE_BLIP(CutSceneBlip)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
				ENDSWITCH
			BREAK
			
				CASE SCSPropTest2
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
							
							IF NOT IS_PED_INJURED(scplayer)
								if 	IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , vStreamArea)
									REQUEST_CUTSCENE(CutsceneName)
									REGISTER_ENTITY_FOR_CUTSCENE(TestObjects[0].Object, "Prop_FBIBombPlant",  CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
									CutSceneFlow = CutSceneStreaming
								ENDIF
							ENDIF
					BREAK
					
					CASE CutSceneStreaming
						IF NOT IS_PED_INJURED(scplayer)
						
							if HAS_CUTSCENE_LOADED()
								REMOVE_BLIP(CutSceneBlip)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
				ENDSWITCH
			BREAK
			
			CASE SCSPropTest3
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
							
							IF NOT IS_PED_INJURED(scplayer)
								if 	IS_ENTITY_AT_COORD( scplayer, vTestSceneOrigin , vStreamArea)
									REQUEST_CUTSCENE(CutsceneName)
									REGISTER_ENTITY_FOR_CUTSCENE(null, "Prop_FBIBombPlant", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
									CutSceneFlow = CutSceneStreaming
								ENDIF
							ENDIF
					BREAK
					
					CASE CutSceneStreaming
						IF NOT IS_PED_INJURED(scplayer)
							//SET_CUTSCENE_COORDS(vTestSceneOrigin)
							SET_CUTSCENE_TRIGGER_AREA(TriggerPos, triggerRadius, TriggerOrient, TriggerAngle)
							DISPLAY_CUTSCENE_TRIGGER_AREA()	
						
							if HAS_CUTSCENE_LOADED()
								REMOVE_BLIP(CutSceneBlip)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						IF NOT HAS_CUTSCENE_FINISHED()
							if not DOES_ENTITY_EXIST(TestObjects[0].Object)
								TestObjects[0].Object =GET_OBJECT_INDEX_FROM_ENTITY_INDEX( GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Prop_FBIBombPlant"))
							ENDIF
						ELSE
							if  DOES_ENTITY_EXIST(TestObjects[0].Object)
								
								SET_ENTITY_COORDS_NO_OFFSET( TestObjects[0].Object  , <<0.46, 88.97, 6.35>>)
								CutSceneFlow = CutscenePost
							ENDIF
						ENDIF
						
					BREAK
				ENDSWITCH
			BREAK
			
			
			//player meets ped in world
			CASE PedExitWalkStateTask
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
				
						REQUEST_CUTSCENE(CutsceneName)
						CutSceneFlow = CutSceneStreaming
					BREAK
					
					CASE CutSceneStreaming
						//SET_CUTSCENE_COORDS(vTestSceneOrigin)
						SET_CUTSCENE_TRIGGER_AREA(TriggerPos, triggerRadius, TriggerOrient, TriggerAngle)
						if HAS_CUTSCENE_LOADED()
							IF NOT IS_PED_INJURED(scplayer)

								REMOVE_BLIP(CutSceneBlip)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						if not IS_PED_INJURED(scplayer)
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Player_zero")
							 	
							    TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED(scplayer,<<-1.65, 74.35, 6.45>>, PEDMOVE_WALK, -1, 0.35, ENAV_NO_STOPPING, navDatah)
								FORCE_PED_MOTION_STATE(scplayer, MS_ON_FOOT_WALK, true, FAUS_CUTSCENE_EXIT )
								PRINTSTRING("TASK_FOLLOW_NAV_MESH_TO_COORD")
								PRINTNL()
								CutSceneFlow = CutscenePost
							ENDIF
						ENDIF
						
						if not HAS_CUTSCENE_FINISHED()
							
							if not DOES_ENTITY_EXIST(testpeds[0].ped)
								testpeds[0].ped = GET_PED_INDEX_FROM_ENTITY_INDEX( GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("p_m_zero" ))
							ENDIF	
							
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			
						//player meets ped in world
			CASE PedExitWalkState
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
				
						REQUEST_CUTSCENE(CutsceneName)
						CutSceneFlow = CutSceneStreaming
					BREAK
					
					CASE CutSceneStreaming
						//SET_CUTSCENE_COORDS(vTestSceneOrigin)
						SET_CUTSCENE_TRIGGER_AREA(TriggerPos, triggerRadius, TriggerOrient, TriggerAngle)
						if HAS_CUTSCENE_LOADED()
							IF NOT IS_PED_INJURED(scplayer)

								REMOVE_BLIP(CutSceneBlip)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						if not IS_PED_INJURED(scplayer)
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Player_zero")
								FORCE_PED_MOTION_STATE(scplayer, MS_ON_FOOT_WALK, true, FAUS_CUTSCENE_EXIT )
								CutSceneFlow = CutscenePost
							ENDIF
						ENDIF
						
						if not HAS_CUTSCENE_FINISHED()
							
							if not DOES_ENTITY_EXIST(testpeds[0].ped)
								testpeds[0].ped = GET_PED_INDEX_FROM_ENTITY_INDEX (GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("p_m_zero" ))
							ENDIF	
							
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			//player meets ped in world
			CASE PedExitRunState
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
				
						REQUEST_CUTSCENE(CutsceneName)
						CutSceneFlow = CutSceneStreaming
					BREAK
					
					CASE CutSceneStreaming
						//SET_CUTSCENE_COORDS(vTestSceneOrigin)
						SET_CUTSCENE_TRIGGER_AREA(TriggerPos, triggerRadius, TriggerOrient, TriggerAngle)
						if HAS_CUTSCENE_LOADED()
							IF NOT IS_PED_INJURED(scplayer)

								REMOVE_BLIP(CutSceneBlip)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						if not IS_PED_INJURED(scplayer)
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Player_zero")
							 	
							    TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED(scplayer,<<16.25, 79.55, 6.45>>, PEDMOVE_RUN, -1, 0.35, ENAV_NO_STOPPING, navDatah)
								FORCE_PED_MOTION_STATE(scplayer, MS_ON_FOOT_RUN, true, FAUS_CUTSCENE_EXIT )
								PRINTSTRING("TASK_FOLLOW_NAV_MESH_TO_COORD")
								PRINTNL()
								CutSceneFlow = CutscenePost
							ENDIF
						ENDIF
						
						if not HAS_CUTSCENE_FINISHED()
							
							if not DOES_ENTITY_EXIST(testpeds[0].ped)
								testpeds[0].ped = GET_PED_INDEX_FROM_ENTITY_INDEX (GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("p_m_zero" ))
							ENDIF	
							
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE PedExitIntoFreeFallState
				SWITCH CutSceneFlow
					CASE CutSceneStart
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
				
						REQUEST_CUTSCENE(CutsceneName)
						CutSceneFlow = CutSceneStreaming
					BREAK
					
					CASE CutSceneStreaming
						//SET_CUTSCENE_COORDS(vTestSceneOrigin)
						SET_CUTSCENE_TRIGGER_AREA(TriggerPos, triggerRadius, TriggerOrient, TriggerAngle)
						if HAS_CUTSCENE_LOADED()
							IF NOT IS_PED_INJURED(scplayer)

								REMOVE_BLIP(CutSceneBlip)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						if not IS_PED_INJURED(scplayer)
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Player_zero")
							 	 SET_PED_TO_RAGDOLL(scplayer, 1000, 3000,  TASK_RELAX)
								FORCE_PED_MOTION_STATE(scplayer, MS_DO_NOTHING, true, FAUS_CUTSCENE_EXIT )
								
								CutSceneFlow = CutscenePost
							ENDIF
						ENDIF
						
						if not HAS_CUTSCENE_FINISHED()
							
							if not DOES_ENTITY_EXIST(testpeds[0].ped)
								testpeds[0].ped = GET_PED_INDEX_FROM_ENTITY_INDEX (GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("p_m_zero" ))
							ENDIF	
							
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			
				//player meets ped in world
			CASE PedExitIntoCoverState
				SWITCH CutSceneFlow
					CASE CutSceneStart
							REQUEST_CUTSCENE(CutsceneName)
							CutSceneFlow = CutSceneStreaming
					BREAK
					
					CASE CutSceneStreaming
						
					//	SET_CUTSCENE_COORDS(vTestSceneOrigin)
						SET_CUTSCENE_TRIGGER_AREA(TriggerPos, triggerRadius, TriggerOrient, TriggerAngle)
			
						if HAS_CUTSCENE_LOADED()
							IF NOT IS_PED_INJURED(scplayer)
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						if not IS_PED_INJURED(scplayer)
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Player_zero")
							    TASK_PUT_PED_DIRECTLY_INTO_COVER( scplayer, vTestSceneOrigin, -1 ) 
								FORCE_PED_AI_AND_ANIMATION_UPDATE(scplayer)
								CutSceneFlow = CutscenePost
							ENDIF
						ENDIF
			
					BREAK
				ENDSWITCH
			BREAK
			
			CASE ExitSceneInVehicle
				SWITCH CutSceneFlow
					CASE CutSceneStart
						
							
							IF Not DOES_BLIP_EXIST(CutSceneBlip)
								CutSceneBlip = ADD_BLIP_FOR_COORD(vTestSceneOrigin)
							ENDIF
				
						REQUEST_CUTSCENE(CutsceneName)
						CutSceneFlow = CutSceneStreaming
					BREAK
					
					CASE CutSceneStreaming
						printstring ("CutSceneStreaming")
						PRINTNL()
						if HAS_CUTSCENE_LOADED()
							IF NOT IS_PED_INJURED(scplayer)
								REMOVE_BLIP(CutSceneBlip)
								if IS_VEHICLE_DRIVEABLE(TestVehicles[0].vehicle)
									REGISTER_ENTITY_FOR_CUTSCENE(TestVehicles[0].vehicle, "BJXL", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								ENDIF
								
								if not IS_PED_INJURED(TestPeds[0].ped)
									REGISTER_ENTITY_FOR_CUTSCENE(TestPeds[0].ped, "player_one" ,  CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								ENDIF
								
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						if not IS_PED_INJURED(scplayer)
						
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("player_one")
								if IS_VEHICLE_DRIVEABLE(TestVehicles[0].vehicle)
									if not IS_PED_INJURED(TestPeds[0].ped)
										SET_PED_INTO_VEHICLE(TestPeds[0].ped, TestVehicles[0].vehicle)
									ENDIF
								ENDIF
							ENDIF
							
							
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Player_zero")
					
							 	if IS_VEHICLE_DRIVEABLE(TestVehicles[0].vehicle)
									SET_PED_INTO_VEHICLE( scplayer, TestVehicles[0].vehicle, VS_BACK_RIGHT  )
								ENDIF
							
								if IS_VEHICLE_DRIVEABLE(TestVehicles[0].vehicle )	
										TASK_LEAVE_VEHICLE(scplayer, TestVehicles[0].vehicle )
								ENDIF
								
								wait(0)
								
								CutSceneFlow = CutscenePost
							ENDIF
						ENDIF
						
					BREAK
				ENDSWITCH
			BREAK
			
			CASE ExitWithWeapon
				SWITCH CutSceneFlow
					CASE CutSceneStart	
						if not IS_PED_INJURED(scplayer)
							GIVE_WEAPON_TO_PED( scplayer, WEAPONTYPE_ASSAULTRIFLE, 100, FALSE  )
						ENDIF
						
						wait(1000)
					
						REQUEST_CUTSCENE(CutsceneName)
				
						CutSceneFlow = CutSceneStreaming
					BREAK
					
					CASE CutSceneStreaming
						printstring ("CutSceneStreaming")
						PRINTNL()
						if HAS_CUTSCENE_LOADED()
							IF NOT IS_PED_INJURED(scplayer)
								REMOVE_BLIP(CutSceneBlip)
							
								if IS_VEHICLE_DRIVEABLE(TestVehicles[0].vehicle)
									REGISTER_ENTITY_FOR_CUTSCENE(TestVehicles[0].vehicle, "BJXL", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								ENDIF
										
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
							
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						if not IS_PED_INJURED(scplayer)
						if CAN_SET_ENTER_STATE_FOR_REGISTERED_ENTITY("Player_zero")
							SET_CURRENT_PED_WEAPON( scplayer, WEAPONTYPE_ASSAULTRIFLE, FALSE  )
						ENDIF
							
							//SET_WEAPON_VISIBILITY_FOR_CUTSCENE(scplayer, FALSE)
							
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Player_zero")
									GIVE_WEAPON_TO_PED( scplayer, WEAPONTYPE_ASSAULTRIFLE, 100, true  )
									FORCE_PED_AI_AND_ANIMATION_UPDATE(scplayer)
								CutSceneFlow = CutscenePost
							ENDIF
						ENDIF
						
					BREAK
				ENDSWITCH
			
			BREAK
			
			CASE LoadUnloadTest	
				SWITCH CutSceneFlow
					CASE CutSceneStart	
						PRINTSTRING("REQUEST_CUTSCENE(fbi_jan_sections)")
						PRINTNL()
						REQUEST_CUTSCENE("fbi_jan_sections")
						wait(100)
						CutSceneFlow = CutSceneStreaming
					BREAK
					
					CASE CutSceneStreaming
						PRINTSTRING("REMOVE_CUTSCENE()")
						PRINTNL()
						REMOVE_CUTSCENE()
						
						CutSceneFlow = CutSceneLoading
					BREAK
					
					CASE CutSceneLoading 
						PRINTSTRING("REQUEST_CUTSCENE(seamlessexit_wpn)")
						PRINTNL()
						if not IS_CUTSCENE_ACTIVE()
							REQUEST_CUTSCENE("seamlessexit_wpn")
							CutSceneFlow = CutScenePlayingAgain
						ENDIF
					BREAK
					
					CASE CutScenePlayingAgain
						if HAS_CUTSCENE_LOADED()
							START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
							CutSceneFlow = CutScenePlaying
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						if not IS_PED_INJURED(scplayer)
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Player_zero")
									GIVE_WEAPON_TO_PED( scplayer, WEAPONTYPE_ASSAULTRIFLE, 100, true  )
									FORCE_PED_AI_AND_ANIMATION_UPDATE(scplayer)
								CutSceneFlow = CutscenePost
							ENDIF
						ENDIF
						
					BREAK
				ENDSWITCH
			BREAK
			
			CASE RayfireTest
					SWITCH CutSceneFlow
					CASE CutSceneStart	
						REQUEST_CUTSCENE(CutsceneName)
						CutSceneFlow = CutSceneStreaming
					BREAK
					
					CASE CutSceneStreaming
						if HAS_CUTSCENE_LOADED()
							IF NOT IS_PED_INJURED(scplayer)
						
								START_CUTSCENE()
								SETTIMERA(0)
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						if (TIMERA() > 10)
							STOP_CUTSCENE()
							CutSceneFlow = CutscenePost
						ENDIF
						
						
						
						IF HAS_CUTSCENE_FINISHED()
							CutSceneFlow = CutscenePost
						ENDIF
					BREAK
				ENDSWITCH
			
			BREAK
			
			CASE OverlayTest
				SWITCH CutSceneFlow
					CASE CutSceneStart	
						REQUEST_CUTSCENE(CutsceneName)
						CutSceneFlow = CutSceneStreaming
					BREAK
					
					CASE CutSceneStreaming
						if HAS_CUTSCENE_LOADED()
							IF NOT IS_PED_INJURED(scplayer)
						
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
						
								if DOES_ENTITY_EXIST(TestObjects[0].Object)
									SET_ENTITY_COORDS(TestObjects[0].Object, <<-3.00, 88.56, 7.04>>)
									SET_ENTITY_ROTATION(TestObjects[0].Object, <<0, 0, 180>>)
									PRINTSTRING("rotating object")
									PRINTNL()
								ENDIF
						
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						
						IF HAS_CUTSCENE_FINISHED()
							CutSceneFlow = CutscenePost
						ENDIF
					BREAK
				ENDSWITCH
			
			BREAK
			
			CASE offscreencrash
				SWITCH CutSceneFlow
					CASE CutSceneStart	
						REQUEST_CUTSCENE(CutsceneName)
						CutSceneFlow = CutSceneStreaming
					BREAK
					
					CASE CutSceneStreaming
						if HAS_CUTSCENE_LOADED()
							IF NOT IS_PED_INJURED(scplayer)
								if IS_VEHICLE_DRIVEABLE(TestVehicles[0].Vehicle )
									REGISTER_ENTITY_FOR_CUTSCENE(TestVehicles[0].Vehicle, "SEASHARK", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								ENDIF	
								
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
									CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					break	
				ENDSWITCH	
			BREAK
			
			CASE BranchTestPart_1345
				SWITCH CutSceneFlow
					CASE CutSceneStart	
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(CutsceneName, CS_SECTION_1 | CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5 )
						CutSceneFlow = CutSceneStreaming
					BREAK
					
					CASE CutSceneStreaming
						if HAS_CUTSCENE_LOADED()
							IF NOT IS_PED_INJURED(scplayer)
									START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
									CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					break	
				ENDSWITCH	
			BREAK
			
			CASE BranchTestPart_2345
				SWITCH CutSceneFlow
					CASE CutSceneStart	
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(CutsceneName,  CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5 )
						CutSceneFlow = CutSceneStreaming
					BREAK
					
					CASE CutSceneStreaming
						if HAS_CUTSCENE_LOADED()
							IF NOT IS_PED_INJURED(scplayer)
									START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
									CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					break	
				ENDSWITCH	
			BREAK
			
			
			
			CASE LoadingUnloading
				SWITCH CutSceneFlow
					CASE CutSceneStart	
						      if get_distance_between_coords(vTestSceneOrigin, get_entity_coords(player_ped_id())) < 20
							            request_cutscene(CutsceneName)
											PRINTSTRING("request_cutscene")
												PRINTNL()
							    else 
						            if is_cutscene_active() or has_cutscene_loaded()
						                  if get_distance_between_coords(vTestSceneOrigin, get_entity_coords(player_ped_id())) > 25
						                        remove_cutscene()
												PRINTSTRING("remove_cutscene")
												PRINTNL()
						                  endif 
						            endif 
						            
						      endif 
						      
							      
							   
							      if has_cutscene_loaded()
							            printstring("loaded")
							            printnl()
							      else 
							            printstring("not loaded")
							            printnl()
							      endif 
											
					BREAK
					
					CASE CutSceneStreaming
						if HAS_CUTSCENE_LOADED()
							IF NOT IS_PED_INJURED(scplayer)
						
								START_CUTSCENE_AT_COORDS(vTestSceneOrigin)
						
								if DOES_ENTITY_EXIST(TestObjects[0].Object)
									SET_ENTITY_COORDS(TestObjects[0].Object, <<-3.00, 88.56, 7.04>>)
									SET_ENTITY_ROTATION(TestObjects[0].Object, <<0, 0, 180>>)
									PRINTSTRING("rotating object")
									PRINTNL()
								ENDIF
						
								CutSceneFlow = CutScenePlaying
							ENDIF
						ENDIF
					BREAK
					
					CASE CutScenePlaying
						
						IF HAS_CUTSCENE_FINISHED()
							CutSceneFlow = CutscenePost
						ENDIF
					BREAK
				ENDSWITCH
			
			BREAK
		ENDSWITCH	

ENDPROC


//Purpose: Runs the main scenarios
PROC Run_Test_Scenario ()
	int index = 0
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(true)
	
	SWITCH TestScenarioAStatus
		//setps up all the data for the sceanrio, this is only called once or if selected in widget
		CASE InitialiseScenarioData
			PlayerStartPos = GET_PLAYER_START_POS()				//sets the players start coords at the default
			Bscenario_running = FALSE
			INITIALISE_PED_DATA (Testpeds)
			INITIALISE_VEHICLE_DATA (TestVehicles)
			INITIALISE_CAM_DATA(TestCams )
		    INITIALISE_OBJECT_DATA(TestObjects)
			
			SETUP_TEST_DATA ()
	
			HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
			TestScenarioAStatus = CreateScenarioEntities
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)		//sets the 
		
		BREAK
		
		// Creates all the scenario data
		CASE CreateScenarioEntities
			Bscenario_running = FALSE
			
		//	Set_Gang_Relationships (TRUE)	
		
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
		
			//peds
			FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
				CREATE_PED_ENTITY(Testpeds[index])
				//GiveBlipsToPedsGrps (Testpeds[index])
				ALTER_COMBAT_STATS (Testpeds[index] )
				SWAP_PED_WEAPONS (Testpeds[index])
				ADD_RELATIONSHIP_GROUP_TO_STRUCT (Testpeds[index])
				Block_Peds_Temp_Events (Testpeds[index], TRUE)
				SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
			ENDFOR
			
			//TestVehicles
			FOR Index = 0 to MAX_NUMBER_OF_VEHICLES -1
				CREATE_VEHICLE_ENTITY (TestVehicles[index] )
			ENDFOR
		
			//TestCams
			for index = 0 to MAX_NUMBER_OF_CAMERAS -1
				CREATE_CAM_ENTITY (TestCams[index])
			ENDFOR
			
			//TestObject
			FOR index = 0 TO MAX_NUMBER_OF_OBJECTS -1
				CREATE_OBJECT_ENTITY(TestObjects[index])
			ENDFOR
			TestScenarioAStatus = SetScenarioEntities
		BREAK
		
		CASE SetScenarioEntities
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
			
			Start_And_Reset_Test ()
			
			IF gBeginCombatScenario
				TestScenarioAStatus = RunScenario
				gBeginCombatScenario = FALSE
				gRun_debuggig = FALSE
				Bscenario_running = TRUE
				INITALISE_TEST_STATE()
				TEMP_ACTIVATE_CAMS (TestCams[FixedCamera].cam )
				
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam )
					HELP_TEXT_STATE =  HTF_SCENARIO_RUNNING_FIXED_CAM
				ELSE
					HELP_TEXT_STATE = HTF_STARTED_RUNNING_NO_CAMS
				ENDIF
			ENDIF
		BREAK
		
		//Runs the actual selected scenario
		
		CASE RunScenario
				
			Temp_Run_Scenario_Tracking_Cam (TestPeds, MAX_NUMBER_OF_PEDS, TestCams[TrackingCamera].cam) 
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
				
			Start_And_Reset_Test ()
				
		 	Check_For_Scenario_Reset ()
			
			IF DOES_CAM_EXIST (TestCams[TrackingCamera].cam)
				if IS_CAM_RENDERING (TestCams[TrackingCamera].cam)
					HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_TRACKING_CAM
				ELSE
					HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_FIXED_CAM
				ENDIF
			ENDIF
					
			RUN_TEST ()		//run the main tests 		
			
		BREAK 
				
		CASE CleanupScenario
			
			HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
		
			Cleanup_Scenario_Entities ()
			
			IF gResetToDefault 	
				Temp_cleanup_scenario_cams ()		//here we are changing scenarios so we need to reset cams
				TestScenarioAStatus = InitialiseScenarioData
				gResetToDefault = FALSE
			ENDIF
		
			IF gResetCombatScenario
				Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)
				gcurrentselection = gSelection 
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
					ACTIVATE_CAM (TestCams[FixedCamera].cam)
				ELSE
					Temp_cleanup_scenario_cams ()	
				ENDIF
				TestScenarioAStatus = CreateScenarioEntities
				gResetCombatScenario = FALSE
			ENDIF
		
		BREAK
		
	ENDSWITCH
ENDPROC

SCRIPT
	
	SET_DEBUG_ACTIVE (TRUE)
	
	//gvMapOffset = GET_PLAYER_START_POS ()
	
	SETUP_MISSION_XML_MENU (XMLMenu, KEY_Q )	//defined at the top of the file
	
	SETUP_AREA_FOR_MISSION (<<0.0, 0.0, 0.0>>)
	
	//Gets a reference to the player
	Get_The_Player ()
	
	//set player collios
	SET_PLAYER_COLISION(scplayer, true)
	
	//Sets the test widget from the test tools
	CREATE_TEST_WIDGET ()
	
	//request the test anim bank
	//REQUEST_TEST_ANIM_DICT ("misstest_anim")

	WHILE TRUE
			
		// controls the help text hides if xml menu is active
		TEXT_CONTROLLER ()
	
		// Can set all scenario peds invincible from the widget
		Set_Scenario_Peds_Invincible (TestPeds, MAX_NUMBER_OF_PEDS, gsetpedsinvincible )
	
		//User can create a debug cam for setting sceanrios
		Temp_Create_Debug_Cam (TestCams[FixedCamera]) 
		
		//Runs the selected option from the XML menu 
		Run_Selection_From_XML_input ()
		
		//Checks that a valid selection has been input and runs the scenario
		IF (gcurrentselection <> InvalidSelection)
	
			Draw_Debug_Info (  )
			
			//Sets the test scenario into debug mode
			IF (gAllowDebugging)
				Set_To_Debug ()
			ENDIF
			
			if (gRun_debuggig)
				Temp_Debug_Scenario (TestPeds, TestVehicles, Route, TestCams[FixedCamera].cam, S_M_Y_COP_01)								//Allows the entities in the scneario to be adjusted
				Print_Scenario_Data (TestPeds, TestVehicles, Route, TestCams)
			ENDIF
			
			Run_Test_Scenario ()								
			
			SWITCH_BETWEEN_FIXED_AND_TRACKING (TestCams[FixedCamera].cam)
			
			PRINT_ACTIVE_TEST (Bscenario_running, gRun_debuggig)		
		ENDIF
		
		Terminate_test_script ()
		
		WAIT (0)
	
	
	ENDWHILE

ENDSCRIPT


#ENDIF	// IS_DEBUG_BUILD

