
// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

//***************************BLENDOUT EXAMPLE SCRIPT*********************************
//***********Written by Geoffrey Fermin (with massive help from Thomas French)********

//This example script will attempt to showcase various cutscene to ingame blend examples 
//covering a variety of situations. It is a barebones script with a menu and the bare minimum 
//scripting on entities to have them in the correct state for thier respective blends.


//to run script, please launch bank_release or beta
//hit 4 on debug keyboard, navigate to "cutscene samples"
//and pick your poisen.

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "test_tools.sch"
USING "Commands_cutscene.sch"
USING "Commands_task.sch"
USING "Commands_ped.sch"
USING "commands_object.sch"
USING "Selector_public.sch"
USING "weapons_public.sch"
USING "building_control_public.sch"
USING "commands_weapon.sch"

STRING XMLMenu = "Testbed/CutsceneSamples"
BOOL gAllowDebugging = TRUE

SELECTOR_PED_STRUCT pedSelector

//ped indexes for peds used in the cutscenes. 
PED_INDEX PedRetriever
PED_INDEX PedLamar
PED_INDEX PedBallasOG
PED_INDEX PedHoodRat
VEHICLE_INDEX VehBallasBike
VEHICLE_INDEX VehTrevor1 = NULL
VEHICLE_INDEX VehSubmarine = NULL
VEHICLE_INDEX VehNigelP
VEHICLE_INDEX VehAliN
VEHICLE_INDEX Veh3personBlend
OBJECT_INDEX WpnTrevorRifle
OBJECT_INDEX LesterCane
ENTITY_INDEX ObjMikesBag
PED_INDEX PedNigel
PED_INDEX PedThornhill
PED_INDEX PedLester
PED_INDEX PedTrevor
//this is used to help with USE_CUTSCENE_WHEEL_COMPRESSION command, you use it to return true when the car has hit its exit state,
//then set the use cutscene wheel compression command. this avoids pops.
BOOL hasSetCarExit
WEAPON_TYPE currentWeaponType

PROC Terminate_test_script ()
	if IS_KEYBOARD_KEY_JUST_PRESSED (KEY_S)
		CLEAR_PRINTS ()
		CLEAR_HELP ()
		//Cleanup_Scenario_Entities ()
		//Temp_cleanup_scenario_cams ()
		SET_PLAYER_COLISION(scplayer, true)
		IF NOT IS_PED_INJURED (scplayer)
			SET_PED_COORDS_KEEP_VEHICLE(scplayer, GET_PLAYER_START_POS () )
		ENDIF
		
		TERMINATE_THIS_THREAD ()
		
	ENDIF
ENDPROC

ENUM CutSceneState
	CutSceneStart,
	CutSceneStreaming,
	CutScenePlaying, 
	CutSceneLoading, 
	CutScenePlayingAgain,
	CutscenePost
ENDENUM

CutSceneState CutSceneFlow = CutSceneStart

PROC INITALISE_TEST_STATE()
	CutSceneFlow = CutSceneStart
ENDPROC

ENUM Example
	ExitIntoIdle = 1,
	ExitIntoWalk = 2,
	ExitIntoRun = 3,
	ExitIntoVehicle = 4,
	ExitIntoCover = 5,
	ExitCustomState = 6,
	ExitStaticProp = 7,
	ExitPedVehicle = 8,
	ExitVehiclePassengers = 9,
	ExitPedCustomAnim = 10,
	ExitBlendCam = 11,
	ExitVehTestBed = 12
ENDENUM

PROC SETUP_TEST_DATA ()
	SWITCH int_to_enum (Example, gcurrentselection)
		CASE ExitIntoIdle
		CASE ExitIntoWalk
		CASE ExitIntoRun
		CASE ExitIntoVehicle
		CASE ExitIntoCover
		CASE ExitCustomState
		CASE ExitStaticProp
		CASE ExitPedVehicle
		CASE ExitVehiclePassengers
		CASE ExitPedCustomAnim
		CASE ExitBlendCam
		CASE ExitVehTestBed
		BREAK
	ENDSWITCH
ENDPROC






//*************THINGS TO CONFIRM BEFORE BEGINNING WORK ON A BLEND************
// Ensure the animation assets have proper blend tags when needed, and if need be the feet are also tagged.
// Ensure all entities involved in the blend have correct handles. these need to be set by the animator.








//*************************EXAMPLE 1**********************
// FAMILY_5_MCS_5_P5 – Player blend to idle

//Since by default the active player always blends into an idle, and the camera is a catch up cam, this example
//requires nothing except ensuring the active player is set to Player_Zero. Blends with Idles with a catch-up cam is one
//of the easiest blends to script. The animator must ensure the mover is not intersecting the ground or a pop will occur.

PROC ProcExitIntoIdle()
	printstring ("FAMILY_5_MCS_5_P5 Idle Blend example") printnl()
	SWITCH CutSceneFlow
			
		CASE CutSceneStart
		
			SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_MICHAEL, FALSE)
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL) 
                WAIT(0) 
            ENDWHILE
			REQUEST_CUTSCENE ("FAMILY_5_MCS_5_P5")
			Cutsceneflow = CutsceneStreaming
		BREAK
					
		CASE CutsceneStreaming
			printstring ("Cutscene Streaming") printnl()
										
			IF HAS_CUTSCENE_LOADED ()
				START_CUTSCENE ()			
				Cutsceneflow = CutscenePlaying
			Endif
		BREAK
				
        CASE CutscenePlaying
			printstring ("Cutscene Playing") printnl() 
			IF
				CutSceneFlow = CutscenePost	
			ENDIF                                   
		BREAK
	Endswitch 
			
ENDPROC

//*************************EXAMPLE 2**********************
// ES_1_RCM_P1 – Player Walking blend, with a ped entity "blending" off camera

//This example has Player_one blending into a walk, and a dog ped blending over 1 frame off camera.
//In addition to demonstrating blending into a walk with the player,
//it is also a good example on how an entity can be handed off camera and safely warped for a blend. provided they are off screen.
//If the players walk is a bit wobbly, and or pops, ask the assigned animator for the cutscene
//to check the feet are tagged properly, and the mover is not in the ground.

PROC ProcExitIntoWalk()	
	SWITCH CutSceneFlow
	
		CASE CutSceneStart		
		// force active player to be franklin
			SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, TRUE)
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_FRANKLIN, FALSE)
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
				WAIT(0) 
            ENDWHILE			
			REQUEST_CUTSCENE ("ES_1_RCM_P1")
			Cutsceneflow = CutsceneStreaming
		BREAK
		
		CASE CutsceneStreaming
			printstring ("Cutscene Streaming") printnl()		
			//Set ped components and register entities
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", PLAYER_PED_ID())						
			ENDIF
			IF HAS_CUTSCENE_LOADED ()
				REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ONE)
				REGISTER_ENTITY_FOR_CUTSCENE(PedRetriever, "Hudson", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, A_C_RETRIEVER)
			START_CUTSCENE ()
			Cutsceneflow = CutscenePlaying
			ENDIF		
		BREAK
	
        CASE CutscenePlaying
			//check every frame for peds loaded in memory, then register them once they are.
			IF NOT DOES_ENTITY_EXIST(PedRetriever) 
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Hudson")) 
				PedRetriever = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Hudson")) 
				ENDIF 
			ENDIF
			//Setting task on Franklin to WALK for 3 seconds
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
                	SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(),(1),(3000), PEDMOVE_WALK)
                	FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
				ENDIF
            ENDIF
			
			//Warping A_C_Retriever to face correct direction, and walk.
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Hudson")
				IF NOT IS_PED_INJURED(PedRetriever)
					SET_ENTITY_COORDS(PedRetriever, << -192.7, 1298.6, 303.8 >>)
					SET_ENTITY_HEADING(PedRetriever, 220.1325)
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						TASK_SMART_FLEE_PED(PedRetriever, PLAYER_PED_ID(), 1000, -1)
					ENDIF
			    ENDIF			
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING() 
				CutSceneFlow = CutscenePost 
			ENDIF 
		BREAK
	Endswitch 
		
ENDPROC

//*************************EXAMPLE 3**********************
// FRA_0_MCS_1 – Player Running blend, 2 peds and a vehicle blend off camera over 1 frame, ped blends into a run in tandem with player.

//This example has the player blending into a run, along with a ped that is ahead of the player.
//This a good example of the diffrence in setting a state on the player, and a state on a ped. In addition to peds blending off camera.
//NOTE: when blending a player so close to a ped at the same time, it is important that the animator makes sure 
//there is ample room between entities while blending into a run, or else they may bump into each other 
//and literally knock each other to the ground.the cutscene has also been set up to equip the players last used 
//weapon when launching the cutscene, if using unarmed, you are equipped with the strongest weapon in the inventory.

PROC ProcExitIntoRun()
	SWITCH CutSceneFlow		
		CASE CutSceneStart
		//Force player to be set as franklin, and preload ig_lamardavis. We have to pre-load this model, because the cutscene has a
		//model swap with the cutscene version of lamar, CS_LamarDavis.
			
			REQUEST_MODEL(IG_LAMARDAVIS)
			WHILE  NOT HAS_MODEL_LOADED(IG_LAMARDAVIS)
				WAIT(0)
			ENDWHILE
			PedLamar = CREATE_PED(PEDTYPE_MISSION, IG_LAMARDAVIS, <<0,1,2>>)
			SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, TRUE)
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_FRANKLIN, FALSE)
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN) 
                WAIT(0) 
            ENDWHILE

			REQUEST_CUTSCENE ("FRA_0_MCS_1")
			Cutsceneflow = CutsceneStreaming
		BREAK
		
		CASE CutsceneStreaming

			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", PLAYER_PED_ID())
			ENDIF
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())	
				GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), currentWeaponType)
				REQUEST_WEAPON_ASSET(currentWeaponType)
			ENDIF		
			//Register entities
			IF HAS_CUTSCENE_LOADED ()				
				REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ONE)
				IF NOT IS_PED_INJURED(PedLamar)
					REGISTER_ENTITY_FOR_CUTSCENE(PedLamar, "Lamar", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, IG_LAMARDAVIS)
				ENDIF
				REGISTER_ENTITY_FOR_CUTSCENE(PedBallasOG, "Ballas_OG", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, IG_BALLASOG)
				REGISTER_ENTITY_FOR_CUTSCENE(PedHoodRat, "HoodRat_girl", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, A_F_Y_Genhot_01)
				REGISTER_ENTITY_FOR_CUTSCENE(VehBallasBike, "BallasOG_Bike", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, BATI)
				START_CUTSCENE ()												
				Cutsceneflow = CutscenePlaying
			ENDIF
			
		BREAK
	
        CASE CutscenePlaying
		//Wait for models to be loaded into memory
		
		IF NOT DOES_ENTITY_EXIST(PedBallasOG) 
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Ballas_OG")) 
				PedBallasOG = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Ballas_OG")) 
			ENDIF 
		ENDIF
		IF NOT DOES_ENTITY_EXIST(PedHoodRat) 
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("HoodRat_Girl")) 
				PedHoodRat = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("HoodRat_Girl")) 
			ENDIF 
		ENDIF
		IF NOT DOES_ENTITY_EXIST(VehBallasBike) 
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("BallasOG_Bike")) 
				VehBallasBike = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("BallasOG_Bike")) 
			ENDIF 
		ENDIF
			
		//Setting Functions to Player_One to simulate player input on exit sprinting for 4 seconds.
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())	
					IF currentWeaponType = WEAPONTYPE_UNARMED 
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_BEST_PED_WEAPON(PLAYER_PED_ID()), TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
					ELSE 
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), currentWeaponType, TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
					ENDIF
				ENDIF
			
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())					
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(),(3.0),(4000), PEDMOVE_SPRINT)
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_SPRINT, TRUE, FAUS_CUTSCENE_EXIT)
				ENDIF
			ENDIF
			
		//Lamar also set to run to a certain point and sprint there.
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lamar", IG_LAMARDAVIS)
				IF NOT IS_PED_INJURED(PedLamar)
					TASK_FOLLOW_NAV_MESH_TO_COORD(PedLamar, <<364.8270, 330.3931, 102.5908>>, PEDMOVE_SPRINT, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT)
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							TASK_SMART_FLEE_PED(PedLamar, PLAYER_PED_ID(), 1000, -1)
						ENDIF
					FORCE_PED_MOTION_STATE(PedLamar, MS_ON_FOOT_SPRINT, TRUE, FAUS_CUTSCENE_EXIT)
				ENDIF
			ENDIF

		//A_F_Y_Genhot_01 is off camera on exit, so keep her in the world and set to smart run from the player
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("HoodRat_Girl")
				IF NOT IS_PED_INJURED(PedHoodRat)
					SET_ENTITY_COORDS(PedHoodRat, <<387.3800, 359.1678, 101.4447>>)
					SET_ENTITY_HEADING(PedHoodRat, 98.7730)
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						TASK_SMART_FLEE_PED(PedHoodRat, PLAYER_PED_ID(), 1000, -1)
					ENDIF
			    ENDIF			
			ENDIF	
		
		//IG_BallasOG is off camera so will attach ballasOG to bati as driver, and give Bati to AI control and drive away from player.
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ballas_OG")
				IF NOT IS_PED_INJURED(PedBallasOG)
					IF IS_VEHICLE_DRIVEABLE(VehBallasBike)
						SET_PED_INTO_VEHICLE(PedBallasOG, VehBallasBike, VS_DRIVER)
					ENDIF
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						TASK_SMART_FLEE_PED(PedBallasOG, PLAYER_PED_ID(), 1000, -1)
					ENDIF
				ENDIF
			ENDIF
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("BallasOG_Bike")
				IF NOT IS_VEHICLE_DRIVEABLE(VehBallasBike)
					SET_ENTITY_COORDS(VehBallasBike, <<366.9993, 335.2962, 102.3977>>)
					SET_ENTITY_HEADING(VehBallasBike, 161.8660)
					SET_VEHICLE_ON_GROUND_PROPERLY(VehBallasBike)
				ENDIF
			ENDIF
				
		//Camera set to be behind the player on blendout, to avoid swinging around wildly.
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			ENDIF		
			
			IF NOT IS_CUTSCENE_PLAYING() 
				CutSceneFlow = CutscenePost 
			ENDIF

		BREAK
	Endswitch 
		
ENDPROC

//*************************EXAMPLE 4**********************
// FIN_C2_MCS_5 – Player blending into a vehicle, with the vehicle at rest.

//This example has the player blending into a vehicle that is at rest.
//There has to be an event set on the player to attach to the vehicle.
//in addition for this to work the animator must ensure the player hits his exit state before the vehicle, and the movers is aligned 
//1:1 with the respective seatnode of the vehicle, as well as ensure the vehicles positioning matches exactly 1:1 with the 
//vehciles position taken from a vehicle recording of the vehicle at rest. if not physics will cause a pop.
//it is important not to avoid using set on ground properly as this always causes a pop, when blending a vehicle out in an idle positon always use
//use_cutscene_wheel_compression on the exit state of the vehicle.


PROC ProcExitIntoVehicle()
	SWITCH CutSceneFlow	
		CASE CutSceneStart
			//Forcing player to be trevor
			SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_TREVOR, FALSE)
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR) 
                WAIT(0) 
            ENDWHILE

			REQUEST_CUTSCENE ("FIN_C2_MCS_5")
			Cutsceneflow = CutsceneStreaming
		BREAK
		
		CASE CutsceneStreaming
			//Registering entities
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED_ID())
				printstring ("check if player is dead") printnl()
			ENDIF
			
			IF HAS_CUTSCENE_LOADED ()
			printstring ("cutscene is loaded") printnl()
				REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)
				printstring ("Register Trevor") printnl()
				REGISTER_ENTITY_FOR_CUTSCENE(VehTrevor1, "tornado", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, TORNADO)
				printstring ("Register car") printnl()
				START_CUTSCENE()
				Cutsceneflow = CutscenePlaying
			ENDIF				

		BREAK
	
        CASE CutscenePlaying
			//Checking if entities are in memory, once they are setting up thier exit states.
			IF NOT DOES_ENTITY_EXIST(VehTrevor1) 
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("tornado"))
					VehTrevor1 = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("tornado"))
				ENDIF
			ENDIF
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("tornado", TORNADO)				
			ENDIF
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
			//now that both trevor and the car are regisitered, in memory, they are ready to receive states
			//for the vehicle blend to work, you have to attach a ped before setting up the vehicle exit state.
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(VehTrevor1)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), VehTrevor1)						
						IF IS_VEHICLE_DRIVEABLE(VehTrevor1)
							SET_VEHICLE_DOOR_SHUT(VehTrevor1, SC_DOOR_FRONT_RIGHT)
							SET_VEHICLE_DOOR_SHUT(VehTrevor1, SC_DOOR_FRONT_LEFT)
							SET_VEHICLE_DOOR_SHUT(VehTrevor1, SC_DOOR_REAR_LEFT)
							SET_VEHICLE_DOOR_SHUT(VehTrevor1, SC_DOOR_REAR_RIGHT)
							SET_VEHICLE_DOOR_SHUT(VehTrevor1, SC_DOOR_BOOT)
							//for a blend, not only should you use SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION in place of
							//SET_VEHICLE_ON_GROUND_PROPERLY, but it also needs to be the last state set, otherwise it disturbs the
							//physics and causes a pop. (IE setting cutscene wheel compression, then attaching a ped after will cause a pop)
							SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(VehTrevor1)
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			ENDIF				
		
			IF NOT IS_CUTSCENE_PLAYING() 
				CutSceneFlow = CutscenePost 
			ENDIF	                        
		BREAK
	Endswitch 
		
ENDPROC

//*************************EXAMPLE 5**********************
// CHI_1_MCS_1 – Player blending into Cover with a 2 handed weapon

//In this example, the player blends into cover with a carbine rifle. You must force the exit state of the player to be in cover state
//additionally you must also ensure the movers attach to cover point matches the cutscene mover position. if this is not possible, ask
//the animator to position the mover where this will be possible.
//you must also have the weapon attach itself to the player properly. for this to work however, the animator must ensure
//that the ph_bone of the player is aligned 1:1 with the gungrip bone of the weapon.
//**NOTE** THIS BLEND OUT TYPE WILL NOT WORK IF THE CAMERA IS A CATCH UP CAMERA.
//if it is, then ask the animator to change the camera to a blendout cam instead.

PROC ProcExitIntoCover()
	SWITCH CutSceneFlow
	
		CASE CutSceneStart
			SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_TREVOR, FALSE)
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR) 
                WAIT(0) 
            ENDWHILE
			REQUEST_CUTSCENE ("CHI_1_MCS_1")
			Cutsceneflow = CutsceneStreaming
		BREAK
		
		CASE CutsceneStreaming
		//register player entity
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED_ID())						
			ENDIF
			//Here we give the weapon to trevor, if not assert will happen as the script will try to
			//give trevor a weapon he does not have in his inventory. for the blend to work the entity must have the
			//weapon they are blending with before the exit state is hit.
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_CARBINERIFLE, 500)
				ENDIF
			IF HAS_CUTSCENE_LOADED ()
				REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)				
				WpnTrevorRifle = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(PLAYER_PED_ID(), WEAPONTYPE_CARBINERIFLE, TRUE) 
				REGISTER_ENTITY_FOR_CUTSCENE(WpnTrevorRifle, "Trevors_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				
				START_CUTSCENE()
				Cutsceneflow = CutscenePlaying
			Endif
		BREAK
	
        CASE CutscenePlaying
				
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			//Here we give a task to the player ped to go into cover near the coords that the player hits his exit state at.
				TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), <<1389.7571, 3599.7686, 37.9419>>, -1, FALSE, 0.5, TRUE, FALSE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), FALSE)
			ENDIF
		ENDIF
		// Here we give trevor his weapon, that was created, and given to at the start of the proc.
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_weapon")
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				GIVE_WEAPON_OBJECT_TO_PED(WpnTrevorRifle, PLAYER_PED_ID())
			ENDIF
		ENDIF
		//setting camera heading and pitch to look at the ground outside the window
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(112.5)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-13.2)
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING() 
				CutSceneFlow = CutscenePost 
			ENDIF
                        
		BREAK
	Endswitch 
		
ENDPROC

//*************************EXAMPLE 6**********************
// LSDH_2B_MCS_1 – Player blending into a custom animation state (drunk)

//In this example, the player blends into a custom animation state of being moderately drunk. 
//Which has to be forced on via script. However, For this to work the animator has to 
//match the exact pose of the first frame of the custom animation state.

PROC ProcExitCustomState()
	SWITCH CutSceneFlow
		
		CASE CutSceneStart
			SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_TREVOR, FALSE)
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR) 
                WAIT(0) 
            ENDWHILE
			REQUEST_CUTSCENE ("LSDH_2B_MCS_1")
			Cutsceneflow = CutsceneStreaming
		BREAK
		
		CASE CutsceneStreaming
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED_ID())						
			ENDIF
			IF HAS_CUTSCENE_LOADED ()
				REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)
				REGISTER_ENTITY_FOR_CUTSCENE(VehSubmarine, "Submarine", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, SUBMERSIBLE)
				START_CUTSCENE()
				Cutsceneflow = CutscenePlaying
			Endif
		BREAK
	
        CASE CutscenePlaying
		//here we set the ped clipset to the custom drunk moveclip, and then set the player to be drunk for 3 seconds after.
			IF IS_CUTSCENE_PLAYING()
				REQUEST_ANIM_SET("move_m@drunk@verydrunk")
				IF HAS_ANIM_SET_LOADED("move_m@drunk@verydrunk")				
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID(), Get_Drunk_Level_Moveclip(DL_verydrunk), 0.0)
					ENDIF
				ENDIF
			ENDIF
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					MAKE_PED_DRUNK(PLAYER_PED_ID(), 30000)
				ENDIF
			ENDIF
			
		 	IF NOT DOES_ENTITY_EXIST(VehSubmarine) 
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Submarine")) 
					VehSubmarine = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Submarine")) 
				ENDIF 
			ENDIF
				
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Submarine")
					IF NOT IS_VEHICLE_DRIVEABLE(Vehsubmarine)
						SET_VEHICLE_ON_GROUND_PROPERLY(Vehsubmarine)
					ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			ENDIF					
			
			IF NOT IS_CUTSCENE_PLAYING() 
				CutSceneFlow = CutscenePost 
			ENDIF
                        
		BREAK
	Endswitch 
		
ENDPROC

//*************************EXAMPLE 7**********************
// LES_1A_MCS_4 – Player blending into walk, in addition to having a static prop remain in world in the same position the cutscene left it in.

//In this example, the player blends into a walk, and his backpack that he has removed remains on the desk behind him and in the world
//even after the cutscene ends. demonstrating leaving props in the world after a cutscene is over.
//Most of the time this pops as cutscenes have more animation channels open that ingame objects normally do, so its best to 
//hand them off camera, or during a camera cut. in this example it is done off camera.

PROC ProcExitStaticProp()
	SWITCH CutSceneFlow
	
		CASE CutSceneStart
			SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_MICHAEL, FALSE)
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL) 
                WAIT(0) 
            ENDWHILE
			REQUEST_CUTSCENE ("LES_1A_MCS_4")
			Cutsceneflow = CutsceneStreaming
		BREAK
		
		CASE CutsceneStreaming
			SET_BUILDING_STATE(BUILDINGNAME_IPL_INVADER_OFFICE_INTERIOR, BUILDINGSTATE_DESTROYED )
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())						
			ENDIF
			IF HAS_CUTSCENE_LOADED ()
				REGISTER_ENTITY_FOR_CUTSCENE(ObjMikesBag, "Michaels_Bag",CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, P_MICHAEL_BACKPACK_S)
				START_CUTSCENE()
				Cutsceneflow = CutscenePlaying
			Endif
		BREAK
	
        CASE CutscenePlaying 
		
		IF NOT DOES_ENTITY_EXIST(ObjMikesBag)
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michaels_Bag")) 
				ObjMikesBag = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michaels_Bag"))
			ENDIF
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			If not IS_PED_INJURED(PLAYER_PED_ID())
            	SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(),(1),(3000), PEDMOVE_WALK)
            	FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
			ENDIF
		ENDIF
		//here we warp the prop to stay in its position and stay in the world.
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_Bag")
			SET_ENTITY_COORDS(ObjMikesBag, <<-1053.15, -230.89, 43.92>>, FALSE)
		ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING()
				SET_BUILDING_STATE(BUILDINGNAME_IPL_INVADER_OFFICE_INTERIOR, BUILDINGSTATE_CLEANUP, TRUE )
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(v_ilev_door_orangesolid, <<-1055.9583, -236.4251, 44.1710>>, false, 0)
				CutSceneFlow = CutscenePost 
			ENDIF
                        
		BREAK
	Endswitch 
		
ENDPROC

//*************************EXAMPLE 8**********************
// NMT_2_MCS_2 – Player blending into idle, while 2 other peds blend into a vehicle that is in motion.

//In this example, the player blends into an idle, while watching 2 peds blending on camera in a vehicle that is driving away.
//For this to work the animator must ensure mover's of the peds have to be aligned 1:1 with the seat nodes of the vehicle, in addition to that
//the vehicle needs to be playing an unaltered recording at the moment the blend occurs on the vehicle to match with the speed, 
//velocity, and suspension of the vehicle when it is handed to the game, otherwise a pop will occur.

PROC ProcExitPedVehicle()
	SWITCH CutSceneFlow
	
		CASE CutSceneStart

		SET_PED_POPULATION_BUDGET(0) 
		SET_VEHICLE_POPULATION_BUDGET(0)

			REQUEST_MODEL(IG_NIGEL)
			WHILE  NOT HAS_MODEL_LOADED(IG_NIGEL)
				WAIT(0)
			ENDWHILE
			PedNigel = CREATE_PED(PEDTYPE_MISSION, IG_NIGEL, <<0,1,2>>)
			
			REQUEST_MODEL(IG_MRS_THORNHILL)
			WHILE  NOT HAS_MODEL_LOADED(IG_MRS_THORNHILL)
				WAIT(0)
			ENDWHILE
			PedThornhill = CREATE_PED(PEDTYPE_MISSION, IG_MRS_THORNHILL, <<0,1,2>>)
		
			SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_TREVOR, FALSE)
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR) 
                WAIT(0) 
            ENDWHILE
			REQUEST_CUTSCENE ("NMT_2_MCS_2")
			Cutsceneflow = CutsceneStreaming
		BREAK
		
		CASE CutsceneStreaming
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED_ID())						
			ENDIF
			IF HAS_CUTSCENE_LOADED ()			
				IF NOT IS_PED_INJURED(PedNigel)
					REGISTER_ENTITY_FOR_CUTSCENE(PedNigel, "NIGEL", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, IG_NIGEL)
				ENDIF
				IF NOT IS_PED_INJURED(PedThornhill)
					REGISTER_ENTITY_FOR_CUTSCENE(PedThornhill, "MRS_Thornhill", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, IG_MRS_THORNHILL)
				ENDIF
				REGISTER_ENTITY_FOR_CUTSCENE(VehNigelP, "Showroom_Car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PREMIER)
				REGISTER_ENTITY_FOR_CUTSCENE(VehAliN, "EXL_2_abandoned_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, DUBSTA)
				START_CUTSCENE()
				Cutsceneflow = CutscenePlaying
			Endif
		BREAK
	
        CASE CutscenePlaying
		//setting ped and vehicle budget to 0 just to avoid peds and vehicles getting in the way, and clearing area.
		IF IS_CUTSCENE_PLAYING()
			CLEAR_AREA(<<389.9,-615.1,28.8>>, 3000, TRUE)
			SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0) 
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		ENDIF

		
		IF NOT DOES_ENTITY_EXIST(PedNigel)
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("NIGEL")) 
				PedNigel = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("NIGEL"))
			ENDIF	
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("NIGEL")
			IF NOT IS_PED_INJURED(PedNigel)
				IF IS_VEHICLE_DRIVEABLE(VehNigelP)
					SET_PED_INTO_VEHICLE(PedNigel, VehNigelP, VS_DRIVER)
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						TASK_SMART_FLEE_PED(PedNigel, PLAYER_PED_ID(), 1000, -1)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PedNigel)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(PedThornhill)
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MRS_Thornhill")) 
				PedThornhill = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MRS_Thornhill"))
			ENDIF	
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MRS_Thornhill")
			IF NOT IS_PED_INJURED(PedThornhill)
				IF IS_VEHICLE_DRIVEABLE(VehNigelP)
					SET_PED_INTO_VEHICLE(PedThornhill, VehNigelP, VS_FRONT_RIGHT)
				ENDIF
			ENDIF
		ENDIF
		
	 	IF NOT DOES_ENTITY_EXIST(VehNigelP) 
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Showroom_Car")) 
				VehNigelP = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Showroom_Car")) 
			ENDIF 
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Showroom_Car")
			IF IS_VEHICLE_DRIVEABLE(VehNigelP)
				SET_VEHICLE_DOOR_SHUT(VehNigelP, SC_DOOR_FRONT_RIGHT)
				SET_VEHICLE_DOOR_SHUT(VehNigelP, SC_DOOR_FRONT_LEFT)
				SET_VEHICLE_DOOR_SHUT(VehNigelP, SC_DOOR_REAR_LEFT)
				SET_VEHICLE_DOOR_SHUT(VehNigelP, SC_DOOR_REAR_RIGHT)
				SET_VEHICLE_DOOR_SHUT(VehNigelP, SC_DOOR_BOOT)
				SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(VehNigelP)
				SET_ENTITY_VELOCITY(VehNigelP, <<10,10,0>>)
			ENDIF
		ENDIF
	
	 	IF NOT DOES_ENTITY_EXIST(VehAliN) 
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("EXL_2_abandoned_car")) 
				VehAliN = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("EXL_2_abandoned_car")) 
			ENDIF 
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("EXL_2_abandoned_car")
			IF IS_VEHICLE_DRIVEABLE(VehAliN)
				SET_VEHICLE_DOOR_SHUT(VehAliN, SC_DOOR_FRONT_RIGHT)
				SET_VEHICLE_DOOR_SHUT(VehAliN, SC_DOOR_FRONT_LEFT)
				SET_VEHICLE_DOOR_SHUT(VehAliN, SC_DOOR_REAR_LEFT)
				SET_VEHICLE_DOOR_SHUT(VehAliN, SC_DOOR_REAR_RIGHT)
				SET_VEHICLE_DOOR_SHUT(VehAliN, SC_DOOR_BOOT)
				SET_VEHICLE_ON_GROUND_PROPERLY(VehAliN)
			ENDIF
		ENDIF
		
			IF NOT IS_CUTSCENE_PLAYING() 
				CutSceneFlow = CutscenePost 
			ENDIF
			//setting ped and vehicle budget back to defaults. if not, cars and peds will never load.
			SET_PED_POPULATION_BUDGET(3) 
			SET_VEHICLE_POPULATION_BUDGET(3) 
		BREAK
	Endswitch 
		
ENDPROC

//*************************EXAMPLE 9**********************
// RBHS_MCS_1 – Player blending into vehicle, while 2 other peds blend into a vehicle as well, vehicle is idle resting

//player blends into vehicle while it is in its idle resting state, as well as 2 other peds blending into the same vehicle.
//animator must ensure all movers are aligned 1:1 with the respective seatnodes of the vehicles, as well as ensure the vehicles positioning
//matches exactly 1:1 with the vehciles position taken from a vehicle recording of the vehicle at rest. if not physics will cause a pop.
//it is important not to use set on ground properly as this always causes a pop, when blending a vehicle out in an idle positon always use
//"use_cutscene_wheel_compression

PROC ProcExitVehiclePassengers()
	SWITCH CutSceneFlow
	
		CASE CutSceneStart
		SET_PED_POPULATION_BUDGET(0) 
		SET_VEHICLE_POPULATION_BUDGET(0)		

			REQUEST_MODEL(IG_LESTERCREST)
			WHILE  NOT HAS_MODEL_LOADED(IG_LESTERCREST)
				WAIT(0)
			ENDWHILE
			PedLester = CREATE_PED(PEDTYPE_MISSION, IG_LESTERCREST, <<0,1,2>>)
			
			SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_MICHAEL, FALSE)
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL) 
                WAIT(0) 
            ENDWHILE
			REQUEST_CUTSCENE ("RBHS_MCS_1")
			Cutsceneflow = CutsceneStreaming
		BREAK
		
		CASE CutsceneStreaming
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())						
			ENDIF		
			IF HAS_CUTSCENE_LOADED()
				IF NOT IS_PED_INJURED(PedLester)
					REGISTER_ENTITY_FOR_CUTSCENE(PedLester, "Lester", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, IG_LESTERCREST)
				ENDIF
				REGISTER_ENTITY_FOR_CUTSCENE(PedTrevor, "Trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PLAYER_TWO)
				REGISTER_ENTITY_FOR_CUTSCENE(Veh3personBlend, "michaels_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PREMIER)
				START_CUTSCENE()
				Cutsceneflow = CutscenePlaying
			Endif
		BREAK
	
        CASE CutscenePlaying
		
		IF IS_CUTSCENE_PLAYING()
			CLEAR_AREA(<<-339.9,6183.1,31.2>>, 3000, TRUE)
			SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0) 
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		ENDIF
			
			IF NOT DOES_ENTITY_EXIST(PedLester)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester")) 
					PedLester = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester"))
				ENDIF	
			ENDIF
			
			If CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lester")
				If not IS_PED_INJURED(PedLester)
					IF IS_VEHICLE_DRIVEABLE(Veh3personBlend)
						SET_PED_INTO_VEHICLE(PedLester,Veh3personBlend, VS_BACK_RIGHT)
					ENDIF
				ENDIF
			ENDIF
			IF NOT DOES_ENTITY_EXIST(PedTrevor)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor")) 
					PedTrevor = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor"))
				ENDIF	
			ENDIF
			
				If CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
					If not IS_PED_INJURED(PedTrevor)
						IF IS_VEHICLE_DRIVEABLE(Veh3personBlend)
							SET_PED_INTO_VEHICLE(PedTrevor,Veh3personBlend, VS_FRONT_RIGHT)
						ENDIF
					ENDIF
				ENDIF
				
				If CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					If not IS_PED_INJURED(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(Veh3personBlend)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),Veh3personBlend, VS_DRIVER)
						ENDIF
					ENDIF
				ENDIF
				
			IF NOT DOES_ENTITY_EXIST(Veh3personBlend) 
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("michaels_car")) 
					Veh3personBlend = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("michaels_car")) 
				ENDIF 
			ENDIF
			//Sett up a bool to return true after the exit state is hit so the SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION no longer
			//is needed, avoiding a pop.
			

			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michaels_car")
				IF IS_VEHICLE_DRIVEABLE(Veh3personBlend)
					hasSetCarExit = TRUE
					SET_VEHICLE_DOORS_SHUT(Veh3personBlend)
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(Veh3personBlend)
				IF NOT hasSetCarExit
					SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(Veh3personBlend, TRUE, TRUE, TRUE)
				ENDIF
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING() 
				CutSceneFlow = CutscenePost 
			ENDIF
			SET_PED_POPULATION_BUDGET(3) 
			SET_VEHICLE_POPULATION_BUDGET(3)	
		BREAK
	Endswitch 
		
ENDPROC

//*************************EXAMPLE 10**********************
// ARMENIAN_1_INT – Player blending into walk, while ped blends into custom ingame animation

//Probably one of the harder blend outs, Player blends into a walk, while ped blends into a custom ingame animation.
//for this to work you have to not only force the ped entity into the custom ingame animation,
//but the animators (cutscene and ingame) must ensure the last frame of the cutscene and first frame of the ingame animation
//both match exactly, both skeleton and mover, for the blend to work correctly.

PROC ProcExitPedCustomAnim()
	SWITCH CutSceneFlow
	
		CASE CutSceneStart
			REQUEST_MODEL(IG_LAMARDAVIS)
			WHILE  NOT HAS_MODEL_LOADED(IG_LAMARDAVIS)
				WAIT(0)
			ENDWHILE
			PedLamar = CREATE_PED(PEDTYPE_MISSION, IG_LAMARDAVIS, <<0,1,2>>)
			
			SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, TRUE)
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_FRANKLIN, FALSE)
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN) 
                WAIT(0) 
            ENDWHILE
			REQUEST_CUTSCENE ("ARMENIAN_1_INT")
			Cutsceneflow = CutsceneStreaming
		BREAK
		
		CASE CutsceneStreaming
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", PLAYER_PED_ID())						
			ENDIF	
			IF HAS_CUTSCENE_LOADED ()
				IF NOT IS_PED_INJURED(PedLamar)
					REGISTER_ENTITY_FOR_CUTSCENE(PedLamar, "Lamar", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, IG_LAMARDAVIS)
				ENDIF
				START_CUTSCENE()
				Cutsceneflow = CutscenePlaying
			Endif
		BREAK
	
        CASE CutscenePlaying
			IF IS_CUTSCENE_PLAYING()
				REQUEST_ANIM_DICT("missarmenian1leadinout")
				IF HAS_ANIM_DICT_LOADED("missarmenian1leadinout")
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lamar")
						IF NOT IS_PED_INJURED(PedLamar)
						And NOT IS_ENTITY_PLAYING_ANIM(PedLamar, "missarmenian1leadinout", "arm1_int_leadout_gatewalk_lam")
							TASK_PLAY_ANIM_ADVANCED(PedLamar, "missarmenian1leadinout", "arm1_int_leadout_gatewalk_lam", <<-1900.330, -595.638, 12.233>>, <<0.000, 0.000, 48.0>>,
												INSTANT_BLEND_IN, WALK_BLEND_OUT, -1, 
												AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.526)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PedLamar)
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(),(1),(3000), PEDMOVE_WALK)
                	FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
				ENDIF
			ENDIF
			
		IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			IF not IS_PED_INJURED(PLAYER_PED_ID())
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(-41.0 - GET_ENTITY_HEADING(PLAYER_PED_ID()))
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			ENDIF
		ENDIF	
			
			IF NOT IS_CUTSCENE_PLAYING() 
				CutSceneFlow = CutscenePost 
			ENDIF
		BREAK
	Endswitch 
		
ENDPROC

//*************************EXAMPLE 11**********************
// ASS_INT_2_ALT1 – Player blending into idle, while ped blends into custom animation state, camera is a blendout and needs heading set.

//Player blends into an idle, while lester blends during a camera cut into a custom animation. he pops, but this
//is hidden since it happens over a camera cut.
//For this to work the animator must ensure that the ped lesterhits his exit state on the camera cut.
//the camera is also not a catchup but a blendout, and you must set the heading manually or the camera will try to blend
//to a random spot, and will likely look broken.


PROC ProcExitBlendCam()
	SWITCH CutSceneFlow
	
		CASE CutSceneStart
		
			REQUEST_MODEL(IG_LESTERCREST)
			WHILE  NOT HAS_MODEL_LOADED(IG_LESTERCREST)
				WAIT(0)
			ENDWHILE
			PedLester = CREATE_PED(PEDTYPE_MISSION, IG_LESTERCREST, <<0,1,2>>)
			
			SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, TRUE)
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_FRANKLIN, FALSE)
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN) 
                WAIT(0) 
            ENDWHILE
			REQUEST_CUTSCENE ("ASS_INT_2_ALT1")
			Cutsceneflow = CutsceneStreaming
		BREAK
		
		CASE CutsceneStreaming
			printstring ("Cutscene Streaming") printnl()		
			IF HAS_CUTSCENE_LOADED ()
			
				IF NOT IS_PED_INJURED(PedLester)
					REGISTER_ENTITY_FOR_CUTSCENE(PedLester, "Lester", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, IG_LESTERCREST)
				ENDIF
				REGISTER_ENTITY_FOR_CUTSCENE(LesterCane, "WalkingStick_Lester", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PROP_CS_WALKING_STICK)
				START_CUTSCENE()
				Cutsceneflow = CutscenePlaying
			Endif
		BREAK
	
        CASE CutscenePlaying
		
			IF NOT DOES_ENTITY_EXIST(PedLester)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester")) 
					PedLester = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester"))
				ENDIF	
			ENDIF
			
			IF IS_CUTSCENE_PLAYING()
				REQUEST_ANIM_DICT("oddjobs@assassinate@hotel@leadin")
				IF HAS_ANIM_DICT_LOADED("oddjobs@assassinate@hotel@leadin")			
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lester") //, IG_LESTERCREST)
						IF DOES_ENTITY_EXIST(pedLester) AND NOT IS_PED_INJURED(pedLester)
							SET_ENTITY_COORDS(pedLester, << -1509.458, -948.195, 7.750 >>)
							SET_ENTITY_HEADING(pedLester, -11.000)
							TASK_PLAY_ANIM(pedLester, "oddjobs@assassinate@hotel@leadin", "lester_leadin", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0, FALSE, AIK_DISABLE_LEG_IK)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedLester)
							PRINTLN("TASKING LESTER TO PLAY IDLE ANIMATION")
						ELSE
							PRINTLN("LESTER DOES NOT EXIST")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		IF NOT DOES_ENTITY_EXIST(LesterCane)
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("WalkingStick_Lester")) 
				LesterCane = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("WalkingStick_Lester"))
			ENDIF
		ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("WalkingStick_Lester")
					IF DOES_ENTITY_EXIST(LesterCane)
						IF DOES_ENTITY_EXIST(pedLester)
						AND NOT IS_PED_INJURED(pedLester)
							ATTACH_ENTITY_TO_ENTITY(LesterCane, pedLester, GET_PED_BONE_INDEX(pedLester, BONETAG_PH_R_HAND), << 0,0,0 >>, << 0,0,0 >>)
						ENDIF
					ENDIF
				ENDIF
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING() 
				CutSceneFlow = CutscenePost 
			ENDIF   
		BREAK
	Endswitch 
		
ENDPROC

PROC ProcExitVehTestBed()

	//This is a test blend set up to be run in the testbed for physics code to investigate popping issues with wheels and vehicles coming out of
	//cutscenes.
	SWITCH CutSceneFlow
	
		CASE CutSceneStart
			REQUEST_CUTSCENE ("TestBed_Veh_Blend")		
			Cutsceneflow = CutsceneStreaming
		BREAK
		
		CASE CutsceneStreaming
			IF HAS_CUTSCENE_LOADED ()
				START_CUTSCENE()
				Cutsceneflow = CutscenePlaying
			ENDIF			
		BREAK
	
        CASE CutscenePlaying
			IF NOT IS_CUTSCENE_PLAYING() 
				CutSceneFlow = CutscenePost 
			ENDIF			
		BREAK
		
	Endswitch 
		
ENDPROC

PROC RUN_TEST ()
	SWITCH int_to_enum (Example, gcurrentselection)		// the ENUM corresponds to the values from the XML file 
		
		CASE ExitIntoIdle
			ProcExitIntoIdle()
		BREAK
		
		CASE ExitIntoWalk
			ProcExitIntoWalk()
		BREAK
		
		CASE ExitIntoRun
		 	ProcExitIntoRun()
		BREAK
		
		CASE ExitIntoVehicle
			ProcExitIntoVehicle()
		BREAK
		
		CASE ExitIntoCover
			ProcExitIntoCover()
		BREAK
		
		CASE ExitCustomState
			ProcExitCustomState()
		BREAK
		
		CASE ExitStaticProp
			ProcExitStaticProp()
		BREAK
		
		CASE ExitPedVehicle
			ProcExitPedVehicle()
		BREAK
		
		CASE ExitVehiclePassengers
			ProcExitVehiclePassengers()
		BREAK
		
		CASE ExitPedCustomAnim
			ProcExitPedCustomAnim()
		BREAK
		
		CASE ExitBlendCam
			ProcExitBlendCam()
		BREAK
		
		CASE ExitVehTestBed
			ProcExitVehTestBed()
		BREAK		
	ENDSWITCH	
ENDPROC

//Purpose: Runs the main scenarios
PROC Run_Test_Scenario ()
	//int index = 0
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(true)
	
	SWITCH TestScenarioAStatus
		//setps up all the data for the sceanrio, this is only called once or if selected in widget
		CASE InitialiseScenarioData
//			PlayerStartPos = GET_PLAYER_START_POS()				//sets the players start coords at the default
//			Bscenario_running = FALSE
//			INITIALISE_PED_DATA (Testpeds)
//			INITIALISE_VEHICLE_DATA (TestVehicles)
//			INITIALISE_CAM_DATA(TestCams )
//		    INITIALISE_OBJECT_DATA(TestObjects)
//			
			SETUP_TEST_DATA ()
	
			//HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
			TestScenarioAStatus = CreateScenarioEntities
			
//			Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)		//sets the 
		
		BREAK
		
		// Creates all the scenario data
		CASE CreateScenarioEntities
			Bscenario_running = FALSE
			
			TestScenarioAStatus = SetScenarioEntities
		BREAK
//		
		CASE SetScenarioEntities
		
			Start_And_Reset_Test ()
			
			IF gBeginCombatScenario
				TestScenarioAStatus = RunScenario
				CutSceneFlow = CutSceneStart
				gBeginCombatScenario = FALSE
				gRun_debuggig = FALSE
				Bscenario_running = TRUE
				INITALISE_TEST_STATE()
				HELP_TEXT_STATE = HTF_STARTED_RUNNING_NO_CAMS
			ENDIF
		BREAK
		
		//Runs the actual selected scenario
		
		CASE RunScenario
				
			//Temp_Run_Scenario_Tracking_Cam (TestPeds, MAX_NUMBER_OF_PEDS, TestCams[TrackingCamera].cam) 
			
			//Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
				
			Start_And_Reset_Test ()
				
		 	Check_For_Scenario_Reset ()
					
			RUN_TEST ()		//run the main tests 		
			
		BREAK 
				
		CASE CleanupScenario
			
			HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
			
			CutSceneFlow = CutSceneStart
			
			IF gResetToDefault 	
				Temp_cleanup_scenario_cams ()		//here we are changing scenarios so we need to reset cams
				TestScenarioAStatus = InitialiseScenarioData
				gResetToDefault = FALSE
			ENDIF
		
			IF gResetCombatScenario
				gcurrentselection = gSelection 
				TestScenarioAStatus = CreateScenarioEntities
				gResetCombatScenario = FALSE
			ENDIF
		
		BREAK
		
	ENDSWITCH
ENDPROC

SCRIPT
	
	SET_DEBUG_ACTIVE (TRUE)
	
	//gvMapOffset = GET_PLAYER_START_POS ()
	
	SETUP_MISSION_XML_MENU (XMLMenu, KEY_Q )	//defined at the top of the file
	
	//SETUP_AREA_FOR_MISSION (<<0.0, 0.0, 0.0>>)
	
	//Gets a reference to the player
	Get_The_Player ()
	
	//set player collios
	SET_PLAYER_COLISION(scplayer, true)
	
	//Sets the test widget from the test tools
	//CREATE_TEST_WIDGET ()
	
	//request the test anim bank
	//REQUEST_TEST_ANIM_DICT ("misstest_anim")

	WHILE TRUE
//		PRINTSTRING("Word to your mother")
//		PRINTNL()
			
		// controls the help text hides if xml menu is active
		TEXT_CONTROLLER ()
		
		//Runs the selected option from the XML menu 
		Run_Selection_From_XML_input ()
		
		//Checks that a valid selection has been input and runs the scenario
		IF (gcurrentselection <> InvalidSelection)
	
			Draw_Debug_Info (  )
			
			//Sets the test scenario into debug mode
			IF (gAllowDebugging)
				Set_To_Debug ()
			ENDIF
			
//			if (gRun_debuggig)
//				Temp_Debug_Scenario (TestPeds, TestVehicles, Route, TestCams[FixedCamera].cam, S_M_Y_COP_01)
//				Allows the entities in the scneario to be adjusted
//				Print_Scenario_Data (TestPeds, TestVehicles, Route, TestCams)
//			ENDIF
			
			Run_Test_Scenario ()								
			
			//SWITCH_BETWEEN_FIXED_AND_TRACKING (TestCams[FixedCamera].cam)
			
			PRINT_ACTIVE_TEST (Bscenario_running, gRun_debuggig)		
		ENDIF
		
		Terminate_test_script ()
		
		WAIT (0)
	
	
	ENDWHILE
	
ENDSCRIPT

#ENDIF // IS_DEBUG_BUILD