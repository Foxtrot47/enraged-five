///    PC Benchmark script

USING "rage_builtins.sch"
USING "globals.sch"
USING "script_player.sch"
USING "script_DEBUG.sch"
USING "commands_script.sch"
USING "commands_entity.sch"
USING "commands_ped.sch"
USING "commands_misc.sch"
USING "RC_Helper_Functions.sch"
USING "commands_event.sch"
USING "savegame_private.sch"
USING "flow_reset_game.sch"
USING "script_misc.sch"

//USING "flow_public_game.sch"

ENUM BENCHMARK_STATE
	BENCHMARK_STATE_INIT,
	BENCHMARK_STATE_WARP_TO_AREA_FADE_OUT,
	BENCHMARK_STATE_WARP_TO_AREA_FADE_IN,
	BENCHMARK_STATE_WARP_TO_AREA_TIMELAPSE,
	BENCHMARK_STATE_JET_FLYBY,
	BENCHMARK_STATE_INTERP_JET_TO_PED,
	BENCHMARK_STATE_WANTED_LEVEL,
	BENCHMARK_STATE_FINISHED,
	BENCHMARK_STATE_MAX
ENDENUM
BENCHMARK_STATE m_eCurrentBenchmarkState = BENCHMARK_STATE_INIT

//ENUM BENCHMARK_BLOCK_REASON
//	BMBR_NONE = 0,
//	
//	BMBR_FORCE_CLEANUP_OCCURRED,
//	BMBR_ALREADY_RUNNING,
//	BMBR_PLAYER_SWITCH_IN_PROGRESS,
//	BMBR_TIMETABLE_SCENE_IN_PROGRESS,
//	BMBR_IN_TAXI,
//	BMBR_LOCKED_INTO_TRIGGER,
//	BMBR_ON_RAMPAGE,
//	BMBR_IN_GARAGE,
//	BMBR_IN_SHOP,
//	BMBR_RESULT_SCREEN_DISPLAYING,
//	BMBR_AUTOSAVE_IN_PROGRESS,
//	BMBR_TRANSITION_ACTIVE,
//	BMBR_PLAYER_IS_WANTED,
//	BMBR_PLAYER_BEING_ARRESTED,
//	BMBR_REPEAT_PLAY_ACTIVE,
//	BMBR_FINALE_CHOICE_RUNNING,
//	BMBR_ON_MISSION,
//	BMBR_NOT_SAFE_TO_SAVE,
//	BMBR_CUTSCENE_RUNNING
//ENDENUM

FEATURE_BLOCK_REASON m_eFeatureBlockReason

BOOL m_bPausedGameForAlertScreen

BOOL m_bSaveBenchmarkResults = FALSE
BOOL m_bCurrentlyBenchmarking = FALSE
FLOAT m_fFramesPerSecondToDisplay
INT m_iCurrentFramesPerSecondArraySlot = 0
CONST_INT FPS_ARRAY_SIZE 10
FLOAT m_fFramesPerSecondArray[FPS_ARRAY_SIZE]
INT m_iBenchmarkIterationsToDo
INT m_iBenchmarkIterationsDone
INT m_iBenchmarkPassToDo

//FLOAT m_fRectCentreX = 0.131
//FLOAT m_fRectCentreY = 0.14
//FLOAT m_fRectWidth = 0.162
//FLOAT m_fRectHeight = 0.07
//INT m_iRectAlpha = 180
FLOAT m_fFPSStringX = 0.5
FLOAT m_fFPSStringY = 0.9

// Player pos and heading when starting the benchmarking
VECTOR vPlayerStartPosition
FLOAT fPlayerStartHeading

CONST_INT NUM_WARP_TO_AREA_POSITIONS 5
INT m_iCurrentWarpToAreaPosition
VECTOR m_vWarpToAreaPosition[NUM_WARP_TO_AREA_POSITIONS]
FLOAT m_fWarpToAreaHeading[NUM_WARP_TO_AREA_POSITIONS]
VECTOR m_vCamPosStart[NUM_WARP_TO_AREA_POSITIONS]
VECTOR m_vCamRotStart[NUM_WARP_TO_AREA_POSITIONS]
VECTOR m_vCamPosDest[NUM_WARP_TO_AREA_POSITIONS]
VECTOR m_vCamRotDest[NUM_WARP_TO_AREA_POSITIONS]
INT m_iTimeToWaitBeforeWarp
BOOL m_bDoneWarpToAreaFadeIn
BOOL m_bDoneWarpToAreaFadeOut

VEHICLE_INDEX m_vehJet
MODEL_NAMES m_modelJet = LAZER
INT m_iJetCarRecording = 000
STRING m_sJetCarRecording = "pc_bench"
FLOAT m_fJetStartAtCarrecTime = 4500
INT m_iJetSetupTimer
INT m_iInterpToJetTime = 10000
INT m_iInterpFromJetAtCarrecTime = 104000
INT m_iTimeInterpFromJetToPed = 10000
INT m_iInterpFromJetToPedTimer
FLOAT m_fJetRecordingPlaybackSpeed = 2.0
BOOL m_bStartedCinematicCamOnJet = FALSE
BOOL m_bStoppedCinematicCamOnJet = FALSE
INT m_iStartCinematicCamOnJetAtCarrecTime = 32000
INT m_iStopCinematicCamOnJetAtCarrecTime = 100000

VEHICLE_INDEX m_vehCar
MODEL_NAMES m_modelCar = PATRIOT
VECTOR m_vCarPos = <<295.5545, 175.0924, 103.0558>>
FLOAT m_fCarHeading = 66.6414
BOOL m_bDoneRepopulate

CONST_INT NUM_PED_GANG 3
PED_INDEX m_pedGang[NUM_PED_GANG]
MODEL_NAMES m_modelGang[NUM_PED_GANG]
VECTOR m_vPedGang0 = <<299.0901, 184.7974, 103.1263>>
FLOAT m_fPedGang0 = 142.0870
REL_GROUP_HASH relGroupPeds
BOOL m_bPedEnteredCar
INT m_iShockingEvent
INT m_iGangCarRecording = 001
STRING m_sGangCarRecording = "pc_bench"

CONST_INT NUM_TRAFFIC_JAM 2
VEHICLE_INDEX m_vehTrafficJam[NUM_TRAFFIC_JAM]
MODEL_NAMES m_modelVehTrafficJam[NUM_TRAFFIC_JAM]
PED_INDEX m_pedTrafficJam[NUM_TRAFFIC_JAM]
MODEL_NAMES m_modelPedTrafficJam[NUM_TRAFFIC_JAM]
VECTOR m_vTrafficJam[NUM_TRAFFIC_JAM]
FLOAT m_fTrafficJam[NUM_TRAFFIC_JAM]
VEHICLE_INDEX m_vehTanker
MODEL_NAMES m_modelTanker = TANKER
CAMERA_INDEX m_camExplosion
BOOL m_bCreatedTrafficJam
BOOL m_bCarExploded

INT m_iWantedLevelTimer
INT m_iTimeUntilEndWanted = 70000

INT m_iExplodeCarTimer
INT m_iExplodeAmbientCarsTimer

INT m_iFadeTime = 500

CAMERA_INDEX m_cameraInterp
CAMERA_INDEX m_cameraInterp2

structTimelapse m_sTimelapse
TIMEOFDAY m_todReference
INT m_iSkipToHour

FLOAT m_fAmbientPedDensity = 0.0
FLOAT m_fAmbientVehicleDensity = 0.0
FLOAT m_fAmbientScenarioDensity = 0.0

BOOL m_bUseCinematicCamera = FALSE

VEHICLE_INDEX m_vehLastPlayerVehicle = NULL

#IF IS_DEBUG_BUILD

WIDGET_GROUP_ID widgetBenchmark
BOOL m_bOutputJetRecordingPos = FALSE
BOOL m_bOutputGangCarPlaybackPosition = FALSE

PROC BENCHMARK_CREATE_WIDGET()

	IF NOT DOES_WIDGET_GROUP_EXIST(widgetBenchmark)
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: creating benchmark widget")
		widgetBenchmark = START_WIDGET_GROUP("Benchmark widgets")
		ADD_WIDGET_BOOL("Use cinematic camera during vehicle sections", m_bUseCinematicCamera)
		ADD_WIDGET_FLOAT_SLIDER("Ambient ped density", m_fAmbientPedDensity, 0.0, 2.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Ambient vehicle density", m_fAmbientVehicleDensity, 0.0, 2.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Ambient scenario density", m_fAmbientScenarioDensity, 0.0, 2.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Jet start at carrec time", m_fJetStartAtCarrecTime, 0.0, 30000.0, 500.0)
		ADD_WIDGET_FLOAT_SLIDER("Jet recording playback speed", m_fJetRecordingPlaybackSpeed, 1.0, 10.0, 1.0)
		ADD_WIDGET_BOOL("m_bOutputJetRecordingPos", m_bOutputJetRecordingPos)
		ADD_WIDGET_INT_SLIDER("Camera interp to jet time", m_iInterpToJetTime, 0, 30000, 1000)
		ADD_WIDGET_INT_SLIDER("Camera interp from jet at carrec time", m_iInterpFromJetAtCarrecTime, 0, 150000, 1000)
		ADD_WIDGET_INT_SLIDER("Camera interp to ped time", m_iTimeInterpFromJetToPed, 0, 15000, 1000)
		ADD_WIDGET_BOOL("Output gang car playback position", m_bOutputGangCarPlaybackPosition)
		//ADD_WIDGET_FLOAT_SLIDER("m_fRectCentreX", m_fRectCentreX, 0.0, 1.0, 0.001)
		//ADD_WIDGET_FLOAT_SLIDER("m_fRectCentreY", m_fRectCentreY, 0.0, 1.0, 0.001)
		//ADD_WIDGET_FLOAT_SLIDER("m_fRectWidth", m_fRectWidth, 0.0, 1.0, 0.001)
		//ADD_WIDGET_FLOAT_SLIDER("m_fRectHeight", m_fRectHeight, 0.0, 1.0, 0.001)
		//ADD_WIDGET_INT_SLIDER("m_iRectAlpha", m_iRectAlpha, 0, 255, 1)
		ADD_WIDGET_FLOAT_SLIDER("m_fFPSStringX", m_fFPSStringX, 0.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("m_fFPSStringY", m_fFPSStringY, 0.0, 1.0, 0.001)
		STOP_WIDGET_GROUP()
	ENDIF

ENDPROC

PROC BENCHMARK_CLEANUP_WIDGET()

	IF DOES_WIDGET_GROUP_EXIST(widgetBenchmark)
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: removing benchmark widget")
		DELETE_WIDGET_GROUP(widgetBenchmark)
	ENDIF
	
ENDPROC

#ENDIF

PROC BENCHMARK_COMMANDS_TO_RUN_EVERY_FRAME()

	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME() // Suppress replay recording because it'll likely affect benchmark results
	DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
	DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(m_fAmbientScenarioDensity, m_fAmbientScenarioDensity)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(m_fAmbientVehicleDensity)
	SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(m_fAmbientVehicleDensity)
	SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5) // Fewer parked vehicles to take up vehicle spots
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(m_fAmbientPedDensity)
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_SOCIAL_CLUB)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_SOCIAL_CLUB_SECONDARY)

ENDPROC

PROC BENCHMARK_SAFE_FADE_SCREEN_OUT_TO_BLACK(INT iTime = 500, BOOL bWaitForFade = TRUE)
	IF IS_SCREEN_FADED_IN()
		IF NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(iTime)
			IF bWaitForFade
				WHILE NOT IS_SCREEN_FADED_OUT()
					BENCHMARK_COMMANDS_TO_RUN_EVERY_FRAME()
					WAIT(0)
				ENDWHILE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC BENCHMARK_SAFE_FADE_SCREEN_IN_FROM_BLACK(INT iTime = 500, BOOL bWaitForFade = TRUE)
	IF IS_SCREEN_FADED_OUT()
	OR IS_SCREEN_FADING_OUT()
		IF NOT IS_SCREEN_FADING_IN()
			DO_SCREEN_FADE_IN(iTime)
			SHUTDOWN_LOADING_SCREEN()
		ENDIF
	ENDIF
	IF bWaitForFade
		WHILE NOT IS_SCREEN_FADED_IN()
			BENCHMARK_COMMANDS_TO_RUN_EVERY_FRAME()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC BENCHMARK_CLEANUP_WARP_TO_AREA()

	CPRINTLN(DEBUG_MISSION, "BENCHMARK: cleaning up BENCHMARK_STATE_WARP_TO_AREA")
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	DESTROY_ALL_CAMS()
	
ENDPROC

PROC BENCHMARK_CLEANUP_JET_FLYBY()

	CPRINTLN(DEBUG_MISSION, "BENCHMARK: cleaning up BENCHMARK_STATE_JET_FLYBY")
	IF IS_ENTITY_ALIVE(m_vehJet)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(m_vehJet)
			STOP_PLAYBACK_RECORDED_VEHICLE(m_vehJet)
		ENDIF
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), m_vehJet)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
		ENDIF
	ENDIF
	SAFE_DELETE_VEHICLE(m_vehJet)
	REMOVE_VEHICLE_RECORDING(m_iJetCarRecording, m_sJetCarRecording)
	SET_MODEL_AS_NO_LONGER_NEEDED(m_modelJet)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	DESTROY_ALL_CAMS()
	
ENDPROC

PROC BENCHMARK_CLEANUP_GANG_CAR_MODELS()

	SET_MODEL_AS_NO_LONGER_NEEDED(m_modelCar)
	INT i = 0
	REPEAT NUM_PED_GANG i
		SET_MODEL_AS_NO_LONGER_NEEDED(m_modelGang[i])
	ENDREPEAT

ENDPROC

PROC BENCHMARK_CLEANUP_TRAFFIC_JAM_MODELS()

	INT i = 0
	REPEAT NUM_TRAFFIC_JAM i
		SET_MODEL_AS_NO_LONGER_NEEDED(m_modelPedTrafficJam[i])
		SET_MODEL_AS_NO_LONGER_NEEDED(m_modelVehTrafficJam[i])
	ENDREPEAT
	SET_MODEL_AS_NO_LONGER_NEEDED(m_modelTanker)

ENDPROC

PROC BENCHMARK_CLEANUP_WANTED_LEVEL()

	CPRINTLN(DEBUG_MISSION, "BENCHMARK: cleaning up BENCHMARK_STATE_WANTED_LEVEL")
	SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), FALSE)
	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	SET_MAX_WANTED_LEVEL(5)
	IF IS_ENTITY_ALIVE(m_vehCar)
		SET_ENTITY_INVINCIBLE(m_vehCar, FALSE)
		SET_VEHICLE_DOORS_LOCKED(m_vehCar, VEHICLELOCK_UNLOCKED)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), m_vehCar)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
		ENDIF
	ENDIF
	INT i = 0
	REPEAT NUM_PED_GANG i
		SAFE_RELEASE_PED(m_pedGang[i])
	ENDREPEAT
	IF IS_ENTITY_ALIVE(m_vehTrafficJam[0])
		SET_ENTITY_LOAD_COLLISION_FLAG(m_vehTrafficJam[0], FALSE)
	ENDIF
	i = 0
	REPEAT NUM_TRAFFIC_JAM i
		SAFE_RELEASE_PED(m_pedTrafficJam[i])
		SAFE_RELEASE_VEHICLE(m_vehTrafficJam[i])
	ENDREPEAT
	IF IS_ENTITY_ALIVE(m_vehTanker)
		FREEZE_ENTITY_POSITION(m_vehTanker, FALSE)
	ENDIF
	SAFE_RELEASE_VEHICLE(m_vehTanker)
	REMOVE_RELATIONSHIP_GROUP(relGroupPeds)	
	SAFE_DELETE_VEHICLE(m_vehCar)
	SET_MODEL_AS_NO_LONGER_NEEDED(m_modelCar)
	REMOVE_VEHICLE_RECORDING(m_iGangCarRecording, m_sGangCarRecording)
	REMOVE_ALL_SHOCKING_EVENTS(FALSE)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	DESTROY_ALL_CAMS()
	
ENDPROC

/// PURPOSE:
///    Gets called if the benchmark was started via the command line / landing screen and we don't need to do any cleanup, only fade to black and quit the game
PROC BENCHMARK_QUIT_GAME()

	CPRINTLN(DEBUG_MISSION, "BENCHMARK: fade out to black, quit game and terminate benchmark script")
	BENCHMARK_SAFE_FADE_SCREEN_OUT_TO_BLACK()

	// If launched from landing menu, restart game (close and re-open)
	IF LANDING_SCREEN_STARTED_END_USER_BENCHMARK()
		RESTART_GAME()
	ELSE
		QUIT_GAME()
	ENDIF
	TERMINATE_THIS_THREAD()

ENDPROC

PROC BENCHMARK_CLEANUP(BOOL b_DoFullCleanup = TRUE)
	
	CPRINTLN(DEBUG_MISSION, "BENCHMARK: starting basic cleanup")
	CLEAR_PRINTS()
	CLEAR_HELP()
	SAFE_RELEASE_VEHICLE(m_vehLastPlayerVehicle)
	BENCHMARK_CLEANUP_GANG_CAR_MODELS()
	BENCHMARK_CLEANUP_TRAFFIC_JAM_MODELS()
	BENCHMARK_CLEANUP_WARP_TO_AREA()
	BENCHMARK_CLEANUP_JET_FLYBY()
	BENCHMARK_CLEANUP_WANTED_LEVEL()
	CPRINTLN(DEBUG_MISSION, "BENCHMARK: finished basic cleanup")
	
	IF b_DoFullCleanup = TRUE
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: starting full cleanup")
		CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
		#IF IS_DEBUG_BUILD
			BENCHMARK_CLEANUP_WIDGET()
		#ENDIF
	
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,FALSE)
		SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), FALSE)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
		ENDIF
		SET_CINEMATIC_MODE_ACTIVE(FALSE)
		SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL, -1)
		CLEAR_WEATHER_TYPE_PERSIST()
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")
		
		//BENCHMARK_SAFE_FADE_SCREEN_IN_FROM_BLACK(m_iFadeTime, FALSE) // The subsequent QUEUE_MISSION_REPEAT_LOAD handles fading in now
		
		//Reload pre-benchmark save
		QUEUE_MISSION_REPEAT_LOAD()
		
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: finished full cleanup so terminating benchmark script")
		TERMINATE_THIS_THREAD()
	ENDIF
	
ENDPROC

PROC ADVANCE_BENCHMARK_STATE()

	m_eCurrentBenchmarkState = INT_TO_ENUM(BENCHMARK_STATE, ENUM_TO_INT(m_eCurrentBenchmarkState) + 1)
	
ENDPROC

PROC START_BENCHMARKING()

	IF m_bCurrentlyBenchmarking = FALSE
		m_bCurrentlyBenchmarking = TRUE
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: start benchmarking")
		START_END_USER_BENCHMARK()
	ENDIF
	
ENDPROC

PROC STOP_BENCHMARKING()

	CLEAR_HELP() // Clear the quit benchmarking help text
	IF m_bCurrentlyBenchmarking = TRUE
		m_bCurrentlyBenchmarking = FALSE
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: stop benchmarking")
		STOP_END_USER_BENCHMARK()
		m_bSaveBenchmarkResults = TRUE // At least one benchmark has completed so we need to output the results when exiting this benchmarking script
	ENDIF

ENDPROC

PROC BENCHMARK_REPOPULATE_WORLD()

	INSTANTLY_FILL_PED_POPULATION()
	INSTANTLY_FILL_VEHICLE_POPULATION() 
	POPULATE_NOW()

ENDPROC

PROC RETURN_PLAYER_TO_START_BENCHMARK_POSITION()

	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND NOT IS_VECTOR_ZERO(vPlayerStartPosition)
		IF IS_ENTITY_ALIVE(m_vehLastPlayerVehicle)
			CPRINTLN(DEBUG_MISSION, "BENCHMARK: warping player to his starting vehicle")
			SET_ENTITY_VISIBLE(m_vehLastPlayerVehicle, TRUE)
			SET_VEHICLE_DOORS_LOCKED(m_vehLastPlayerVehicle, VEHICLELOCK_UNLOCKED)
			FREEZE_ENTITY_POSITION(m_vehLastPlayerVehicle, FALSE)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), m_vehLastPlayerVehicle, VS_DRIVER)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			SET_VEHICLE_ON_GROUND_PROPERLY(m_vehLastPlayerVehicle)
		ELSE
			CPRINTLN(DEBUG_MISSION, "BENCHMARK: warping player to his starting location")
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerStartPosition)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerStartHeading)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
		ENDIF
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		WAIT_FOR_WORLD_TO_LOAD(vPlayerStartPosition)
		BENCHMARK_REPOPULATE_WORLD()
	ENDIF

ENDPROC

PROC HANDLE_EXITING_BENCHMARKING()

	IF m_eCurrentBenchmarkState != BENCHMARK_STATE_INIT
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BMARK_EXIT")
			IF IS_SCREEN_FADED_IN()
				//CPRINTLN(DEBUG_MISSION, "BENCHMARK: displaying help text BMARK_EXIT")
				PRINT_HELP_FOREVER("BMARK_EXIT")
			ENDIF
		ELSE
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				CPRINTLN(DEBUG_MISSION, "BENCHMARK: Player quit benchmarking.")
				STOP_BENCHMARKING()
				
				// TODO 2320901 - Ensure benchmark results are saved out when quitting early.
				IF m_bSaveBenchmarkResults
					CPRINTLN(DEBUG_MISSION, "BENCHMARK: Saving benchmark results as we quit early.")
					SAVE_END_USER_BENCHMARK()
				ENDIF
				
				IF UI_STARTED_END_USER_BENCHMARK()
					BENCHMARK_SAFE_FADE_SCREEN_OUT_TO_BLACK(m_iFadeTime, TRUE)
					//RETURN_PLAYER_TO_START_BENCHMARK_POSITION() // The QUEUE_MISSION_REPEAT_LOAD in BENCHMARK_CLEANUP() handles repositioning the player now
					//BENCHMARK_SAFE_FADE_SCREEN_IN_FROM_BLACK(m_iFadeTime, FALSE) // The QUEUE_MISSION_REPEAT_LOAD in BENCHMARK_CLEANUP() handles repositioning the player now
					BENCHMARK_CLEANUP()
				ELSE
					BENCHMARK_QUIT_GAME()
				ENDIF
				RESET_END_USER_BENCHMARK()
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC DISPLAY_FPS()

	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BMARK_EXIT") // Looks cleaner to have this appear when the help text does
		m_fFramesPerSecondArray[m_iCurrentFramesPerSecondArraySlot] = 1 / GET_SYSTEM_TIME_STEP()
		//CPRINTLN(DEBUG_MISSION, "BENCHMARK: array slot ", m_iCurrentFramesPerSecondArraySlot," = ", m_fFramesPerSecondArray[m_iCurrentFramesPerSecondArraySlot])
		m_iCurrentFramesPerSecondArraySlot++
		IF m_iCurrentFramesPerSecondArraySlot = FPS_ARRAY_SIZE // We've filled the array so work out the average and reset it
			m_iCurrentFramesPerSecondArraySlot = 0 // Reset the current slot
			FLOAT fNewFramesPerSecondToDisplay = 0.0
			INT i = 0
			REPEAT FPS_ARRAY_SIZE i
				fNewFramesPerSecondToDisplay += m_fFramesPerSecondArray[i]
			ENDREPEAT
			m_fFramesPerSecondToDisplay = fNewFramesPerSecondToDisplay / FPS_ARRAY_SIZE
			//CPRINTLN(DEBUG_MISSION, "BENCHMARK: m_fFramesPerSecondToDisplay updated to ", m_fFramesPerSecondToDisplay)
		ENDIF
		IF m_fFramesPerSecondToDisplay > 0.0
			/*FLOAT fRectCentreX = m_fRectCentreX
			FLOAT fRectCentreY = m_fRectCentreY
			FLOAT fRectWidth = m_fRectWidth
			FLOAT fFPSStringX = m_fFPSStringX
			FLOAT fFPSStringY = m_fFPSStringY
			FLOAT fScreenAspectRatio = GET_SCREEN_ASPECT_RATIO()
			
			// Set the position of the "fps" text and rectangle depending on the screen aspect ratio
			IF (fScreenAspectRatio > 1.24 AND fScreenAspectRatio < 1.26) // 1.250000 1280x1024
				fRectCentreX = 0.168
				fRectCentreY = m_fRectCentreY
				fRectWidth = 0.231
				fFPSStringX = 0.057
				fFPSStringY = m_fFPSStringY
			ELIF (fScreenAspectRatio > 1.33 AND fScreenAspectRatio < 1.34) // 1.333333 // 800x600 1024x768 1152x864 1280x960
				fRectCentreX = 0.161
				fRectCentreY = m_fRectCentreY
				fRectWidth = 0.216
				fFPSStringX = 0.056
				fFPSStringY = m_fFPSStringY
			ELIF (fScreenAspectRatio > 1.35 AND fScreenAspectRatio < 1.36) // 1.353638 1600x1200
				fRectCentreX = 0.153
				fRectCentreY = m_fRectCentreY
				fRectWidth = 0.207
				fFPSStringX = m_fFPSStringX
				fFPSStringY = m_fFPSStringY
			ELIF (fScreenAspectRatio > 1.55 AND fScreenAspectRatio < 1.57) // 1.562500 1600x1024
				fRectCentreX = 0.140
				fRectCentreY = 0.198
				fRectWidth = 0.181
				fFPSStringX = m_fFPSStringX
				fFPSStringY = 0.159
			ELIF (fScreenAspectRatio > 1.59 AND fScreenAspectRatio < 1.61) // 1.600000 1280x800 1680x1050
				fRectCentreX = 0.138
				fRectCentreY = 0.189
				fRectWidth = 0.176
				fFPSStringX = m_fFPSStringX
				fFPSStringY = 0.149
			ELIF (fScreenAspectRatio > 1.62 AND fScreenAspectRatio < 1.63) // 1.624365 1920x1200
				fRectCentreX = 0.137
				fRectCentreY = 0.186
				fRectWidth = 0.173
				fFPSStringX = m_fFPSStringX
				fFPSStringY = 0.146
			ELIF (fScreenAspectRatio > 1.66 AND fScreenAspectRatio < 1.67) // 1.666667 1280x768
				fRectCentreX = 0.136
				fRectCentreY = 0.172
				fRectWidth = 0.172
				fFPSStringX = m_fFPSStringX
				fFPSStringY = 0.132
			ELSE // 1.77778 1280x720 1600x900 1920x1080 // 1.770833 1360x768 // 1.778646 1366x768
				fRectCentreX = m_fRectCentreX
				fRectCentreY = m_fRectCentreY
				fRectWidth = m_fRectWidth
				fFPSStringX = m_fFPSStringX
				fFPSStringY = m_fFPSStringY
			ENDIF
			
			DRAW_RECT(fRectCentreX+0.5, fRectCentreY, fRectWidth, m_fRectHeight, 0, 0, 0, m_iRectAlpha)*/
			SET_TEXT_JUSTIFICATION(FONT_CENTRE)
			SET_TEXT_OUTLINE()
			DISPLAY_TEXT_WITH_FLOAT(m_fFPSStringX, m_fFPSStringY, "BMARK_FPS", m_fFramesPerSecondToDisplay, 1)
		ENDIF
	ENDIF
	
ENDPROC

PROC BENCHMARK_SET_WEATHER(INT iWeather)

	SWITCH iWeather
		CASE 0
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
		BREAK
		CASE 1
			SET_WEATHER_TYPE_NOW_PERSIST("SMOG")
		BREAK
		CASE 2
			SET_WEATHER_TYPE_NOW_PERSIST("RAIN")
		BREAK
		CASE 3
			SET_WEATHER_TYPE_NOW_PERSIST("CLEAR")
		BREAK
		CASE 4
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
		BREAK
	ENDSWITCH

ENDPROC

PROC BENCHMARK_LOAD_JET_FLYBY_ASSETS()

	REQUEST_MODEL(m_modelJet)
	REQUEST_VEHICLE_RECORDING(m_iJetCarRecording, m_sJetCarRecording)
	WHILE NOT HAS_MODEL_LOADED(m_modelJet)
	AND NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(m_iJetCarRecording, m_sJetCarRecording)
		WAIT(0)
	ENDWHILE
	CPRINTLN(DEBUG_MISSION, "BENCHMARK: loaded jet flyby assets")

ENDPROC

PROC BENCHMARK_SETUP_JET_FLYBY()

	IF m_iCurrentWarpToAreaPosition = 4
	AND m_iTimeToWaitBeforeWarp > -1
	AND (GET_GAME_TIMER() - m_iJetSetupTimer) > (m_iTimeToWaitBeforeWarp - 5000)
		IF NOT IS_ENTITY_ALIVE(m_vehJet)
			VECTOR v_pos, v_rot
			v_pos = GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(m_iJetCarRecording, m_sJetCarRecording), m_fJetStartAtCarrecTime)
			v_rot = GET_ROTATION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(m_iJetCarRecording, m_sJetCarRecording), m_fJetStartAtCarrecTime)
			m_vehJet = CREATE_VEHICLE(m_modelJet, v_pos, v_rot.z)
			SET_ENTITY_INVINCIBLE(m_vehJet, TRUE)
			SET_VEHICLE_ENGINE_ON(m_vehJet, TRUE, TRUE)
			SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(m_vehJet, FALSE)
			SET_VEHICLE_CAN_BREAK(m_vehJet, FALSE)
			SET_ENTITY_PROOFS(m_vehJet, TRUE, TRUE, TRUE, TRUE, TRUE)
			CONTROL_LANDING_GEAR(m_vehJet, LGC_RETRACT_INSTANT)
			SET_ENTITY_LOD_DIST(m_vehJet, 5000)
			START_PLAYBACK_RECORDED_VEHICLE(m_vehJet, m_iJetCarRecording, m_sJetCarRecording)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(m_vehJet)
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(m_vehJet, m_fJetStartAtCarrecTime)
			ENDIF
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), m_vehJet)
			m_iTimeToWaitBeforeWarp = -1
			m_fAmbientPedDensity = 2.0
			m_fAmbientVehicleDensity = 2.0
			m_fAmbientScenarioDensity = 1.0
		ENDIF
	ENDIF
	
ENDPROC

PROC REQUEST_CAR_AND_PED_MODELS()

	REQUEST_MODEL(m_modelCar)
	INT i = 0
	REPEAT NUM_PED_GANG i
		REQUEST_MODEL(m_modelGang[i])
	ENDREPEAT
	REQUEST_VEHICLE_RECORDING(m_iGangCarRecording, m_sGangCarRecording)

ENDPROC

PROC BENCHMARK_CREATE_CAR_AND_PEDS()

	IF NOT IS_ENTITY_ALIVE(m_vehCar)
	
		REQUEST_CAR_AND_PED_MODELS()
		IF NOT HAS_MODEL_LOADED(m_modelCar)
			EXIT
		ENDIF
		INT i = 0
		REPEAT NUM_PED_GANG i
			IF NOT HAS_MODEL_LOADED(m_modelGang[i])
				EXIT
			ENDIF
		ENDREPEAT
		IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(m_iGangCarRecording, m_sGangCarRecording)
			EXIT
		ENDIF
		
		VECTOR v_pos, v_rot
		v_pos = GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(m_iGangCarRecording, m_sGangCarRecording), 0)
		v_rot = GET_ROTATION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(m_iGangCarRecording, m_sGangCarRecording), 0)

		CLEAR_AREA(v_pos, 5.0, TRUE)
		m_vehCar = CREATE_VEHICLE(m_modelCar, v_pos, v_rot.z)
		SET_VEHICLE_TYRES_CAN_BURST(m_vehCar, FALSE)
		SET_VEHICLE_WHEELS_CAN_BREAK(m_vehCar, FALSE)
		SET_VEHICLE_ALARM(m_vehCar, FALSE)
		SET_DISABLE_PRETEND_OCCUPANTS(m_vehCar, TRUE)
		SET_ENTITY_LOD_DIST(m_vehCar, 200)
		SET_VEHICLE_STRONG(m_vehCar, TRUE)
		SET_VEHICLE_HAS_STRONG_AXLES(m_vehCar, TRUE)
		SET_ENTITY_INVINCIBLE(m_vehCar, TRUE)
		SET_VEHICLE_MOD_KIT(m_vehCar, 0) 
		SET_VEHICLE_MOD(m_vehCar, MOD_ARMOUR, 2)
		SET_VEHICLE_MOD(m_vehCar, MOD_ENGINE, 3)
		SET_VEHICLE_MOD(m_vehCar, MOD_BRAKES, 2)
		SET_VEHICLE_MOD(m_vehCar, MOD_GEARBOX, 2)
		SET_VEHICLE_MOD(m_vehCar, MOD_WHEELS, 10)
		SET_VEHICLE_MOD(m_vehCar, MOD_REAR_WHEELS, 10)
		TOGGLE_VEHICLE_MOD(m_vehCar, MOD_TOGGLE_XENON_LIGHTS, TRUE)
		TOGGLE_VEHICLE_MOD(m_vehCar, MOD_TOGGLE_NITROUS, TRUE)
		TOGGLE_VEHICLE_MOD(m_vehCar, MOD_TOGGLE_HYDRAULICS, TRUE)
		TOGGLE_VEHICLE_MOD(m_vehCar, MOD_TOGGLE_TYRE_SMOKE, TRUE)
		TOGGLE_VEHICLE_MOD(m_vehCar, MOD_TOGGLE_SUBWOOFER, TRUE)
		SET_VEHICLE_COLOURS(m_vehCar, 0, 0)
		SET_VEHICLE_EXTRA_COLOURS(m_vehCar, 0, 0)
		SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(m_vehCar, 1)
		
		ADD_RELATIONSHIP_GROUP("rel_group_peds", relGroupPeds)	
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, relGroupPeds)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupPeds, RELGROUPHASH_PLAYER)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupPeds, RELGROUPHASH_COP)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_COP, relGroupPeds)
		m_pedGang[0] = CREATE_PED(PEDTYPE_MISSION, m_modelGang[0], m_vPedGang0, m_fPedGang0)
		m_pedGang[1] = CREATE_PED_INSIDE_VEHICLE(m_vehCar, PEDTYPE_MISSION, m_modelGang[1], VS_BACK_LEFT)
		m_pedGang[2] = CREATE_PED_INSIDE_VEHICLE(m_vehCar, PEDTYPE_MISSION, m_modelGang[2], VS_BACK_RIGHT)
		i = 0
		REPEAT NUM_PED_GANG i
			IF IS_ENTITY_ALIVE(m_pedGang[i])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_pedGang[i], TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(m_pedGang[i], relGroupPeds)
				SET_PED_SUFFERS_CRITICAL_HITS(m_pedGang[i], FALSE)
				SET_PED_CAN_BE_TARGETTED(m_pedGang[i], FALSE)
				SET_PED_CONFIG_FLAG(m_pedGang[i], PCF_DontInfluenceWantedLevel, TRUE)
				SET_PED_CONFIG_FLAG(m_pedGang[i], PCF_GetOutBurningVehicle, FALSE)
				SET_PED_CONFIG_FLAG(m_pedGang[i], PCF_DontAllowToBeDraggedOutOfVehicle, TRUE)
				SET_PED_CONFIG_FLAG(m_pedGang[i], PCF_AllowMedicsToAttend, FALSE)
				SET_PED_CONFIG_FLAG(m_pedGang[i], PCF_WillFlyThroughWindscreen, FALSE)
				SET_ENTITY_INVINCIBLE(m_pedGang[i], TRUE)
			ENDIF
		ENDREPEAT
		
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: created gang car and peds")
		BENCHMARK_CLEANUP_GANG_CAR_MODELS()
	ENDIF

ENDPROC

PROC REQUEST_TRAFFIC_JAM_MODELS()

	INT i = 0
	REPEAT NUM_TRAFFIC_JAM i
		REQUEST_MODEL(m_modelVehTrafficJam[i])
		REQUEST_MODEL(m_modelPedTrafficJam[i])
	ENDREPEAT
	REQUEST_MODEL(m_modelTanker)

ENDPROC

FUNC BOOL ARE_TRAFFIC_JAM_MODELS_LOADED()

	REQUEST_TRAFFIC_JAM_MODELS()
	INT i = 0
	REPEAT NUM_TRAFFIC_JAM i
		IF NOT HAS_MODEL_LOADED(m_modelVehTrafficJam[i])
			RETURN FALSE
		ENDIF
		IF NOT HAS_MODEL_LOADED(m_modelPedTrafficJam[i])
			RETURN FALSE
		ENDIF
	ENDREPEAT
	IF NOT HAS_MODEL_LOADED(m_modelTanker)
		RETURN FALSE
	ENDIF

	RETURN TRUE

ENDFUNC

FUNC BOOL IS_TRAFFIC_JAM_CREATED()

	IF m_bCreatedTrafficJam = TRUE
		RETURN TRUE
	ENDIF

	IF ARE_TRAFFIC_JAM_MODELS_LOADED()
		CLEAR_AREA(m_vTrafficJam[0], 10.0, TRUE)
		INT i = 0
		REPEAT NUM_TRAFFIC_JAM i
			m_vehTrafficJam[i] = CREATE_VEHICLE(m_modelVehTrafficJam[i], m_vTrafficJam[i], m_fTrafficJam[i])
			SET_VEHICLE_ON_GROUND_PROPERLY(m_vehTrafficJam[i])
			m_pedTrafficJam[i] = CREATE_PED_INSIDE_VEHICLE(m_vehTrafficJam[i], PEDTYPE_MISSION, m_modelPedTrafficJam[i], VS_DRIVER)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_pedTrafficJam[i], TRUE)
		ENDREPEAT
		SET_ENTITY_LOAD_COLLISION_FLAG(m_vehTrafficJam[0], TRUE)
		m_vehTanker = CREATE_VEHICLE(m_modelTanker, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(m_vehTrafficJam[0], <<0, -50, 10>>), m_fTrafficJam[0])
		ATTACH_VEHICLE_TO_TRAILER(m_vehTrafficJam[0], m_vehTanker)
		SET_TRAILER_LEGS_RAISED(m_vehTanker)
		FREEZE_ENTITY_POSITION(m_vehTanker, TRUE)
		m_bCreatedTrafficJam = TRUE
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: created traffic jam cars and peds")
		BENCHMARK_CLEANUP_TRAFFIC_JAM_MODELS()
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

PROC BENCHMARK_PED_ENTER_CAR()

	IF m_iInterpFromJetToPedTimer > -1
	//AND (GET_GAME_TIMER() - m_iInterpFromJetToPedTimer) > 5000 // Make him walk immediately, rather than waiting for 5 seconds then running
	AND IS_ENTITY_ALIVE(m_vehCar)
	AND IS_ENTITY_ALIVE(m_pedGang[0])
		IF IS_PED_IN_VEHICLE(m_pedGang[0], m_vehCar)
			IF m_bPedEnteredCar = FALSE
				CPRINTLN(DEBUG_MISSION, "BENCHMARK: ped entered car")
				m_bPedEnteredCar = TRUE
				INT i = 0
				REPEAT NUM_PED_GANG i
					IF IS_ENTITY_ALIVE(m_pedGang[i])
						GIVE_WEAPON_TO_PED(m_pedGang[i], WEAPONTYPE_MICROSMG, -1, TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_pedGang[i], FALSE)
						SET_ENTITY_INVINCIBLE(m_pedGang[i], FALSE)
					ENDIF
				ENDREPEAT
				IF m_bUseCinematicCamera = TRUE
					SET_CINEMATIC_MODE_ACTIVE(TRUE)
				ENDIF
				IF m_iShockingEvent = 0
					m_iShockingEvent = ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_SEEN_WEAPON_THREAT, PLAYER_PED_ID())
				ENDIF
				m_fAmbientVehicleDensity = 0.5 // Reduce traffic for the police chase
				BENCHMARK_CLEANUP_JET_FLYBY()
			ENDIF
		ELSE
			REQUEST_TRAFFIC_JAM_MODELS()
			IF NOT IsPedPerformingTask(m_pedGang[0], SCRIPT_TASK_ENTER_VEHICLE)
				TASK_ENTER_VEHICLE(m_pedGang[0], m_vehCar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK)
				CPRINTLN(DEBUG_MISSION, "BENCHMARK: tasked ped to enter car")
			ENDIF
			IF m_bDoneRepopulate = FALSE
				BENCHMARK_REPOPULATE_WORLD()
				m_bDoneRepopulate = TRUE
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC BENCHMARK_CAR_RAM_TANKER()

	IF m_bPedEnteredCar = TRUE
	AND IS_ENTITY_ALIVE(m_vehCar)
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(m_vehCar)
			START_PLAYBACK_RECORDED_VEHICLE(m_vehCar, m_iGangCarRecording, m_sGangCarRecording)
			CPRINTLN(DEBUG_MISSION, "BENCHMARK: starting playback on gang car")
		ELSE
			SET_PLAYBACK_SPEED(m_vehCar, 0.7)
			SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
			SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
			UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
			#IF IS_DEBUG_BUILD
				IF m_bOutputGangCarPlaybackPosition = TRUE
					CPRINTLN(DEBUG_MISSION, "BENCHMARK: playback on gang car position ", GET_POSITION_IN_RECORDING(m_vehCar))
				ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC BENCHMARK_HANDLE_EXPLOSION_CAMERA()

	IF NOT DOES_CAM_EXIST(m_camExplosion)
	AND IS_ENTITY_ALIVE(m_vehCar)
	AND IS_ENTITY_ALIVE(m_vehTanker)
	AND GET_DISTANCE_BETWEEN_ENTITIES(m_vehCar, m_vehTanker) < 30
		m_camExplosion = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-503.5369, 261.8961, 88.6503>>, <<-0.1443, -0.0000, 103.3085>>, 50.0, TRUE)
		SHAKE_CAM(m_camExplosion, "HAND_SHAKE", 0.5)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		SET_CINEMATIC_MODE_ACTIVE(FALSE)
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: changed to final cam")
		FREEZE_ENTITY_POSITION(m_vehTanker, FALSE)
	ENDIF

ENDPROC

FUNC BOOL IS_THIS_MODEL_A_COP(MODEL_NAMES modelPed)

	IF modelPed = S_M_Y_Cop_01
	OR modelPed = S_F_Y_Cop_01
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

PROC BENCHMARK_MOVE_VEHICLES_AHEAD_OF_CAR()
	IF IS_ENTITY_ALIVE(m_vehCar)
		VEHICLE_INDEX veh_random = GET_RANDOM_VEHICLE_IN_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(m_vehCar,<<0,20,0>>), 5.0, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
		IF IS_VEHICLE_OK(veh_random)
		AND veh_random != m_vehCar
			PED_INDEX ped_random = GET_PED_IN_VEHICLE_SEAT(veh_random)
			IF IS_ENTITY_ALIVE(ped_random)
			AND NOT IsPedPerformingTask(ped_random, SCRIPT_TASK_SMART_FLEE_PED)
			AND NOT IS_THIS_MODEL_A_COP(GET_ENTITY_MODEL(ped_random))
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_random, TRUE)
				SET_PED_FLEE_ATTRIBUTES(ped_random, FA_USE_VEHICLE, TRUE)
				SET_PED_FLEE_ATTRIBUTES(ped_random, FA_DISABLE_EXIT_VEHICLE, TRUE)
				TASK_SMART_FLEE_PED(ped_random, PLAYER_PED_ID(), 300, -1)
				SET_PED_KEEP_TASK(ped_random, TRUE)
				CPRINTLN(DEBUG_MISSION, "BENCHMARK: set an ambient car to flee")
			ENDIF
			SAFE_RELEASE_PED(ped_random)
		ENDIF
		SAFE_RELEASE_VEHICLE(veh_random)
	ENDIF
ENDPROC

PROC BENCHMARK_BLOW_UP_NEARBY_VEHICLES()

	IF m_bCarExploded = TRUE
	AND (GET_GAME_TIMER() - m_iExplodeAmbientCarsTimer) > 250
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
		m_iExplodeAmbientCarsTimer = GET_GAME_TIMER()
		VEHICLE_INDEX carArray[20]
		INT iNumCars = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), carArray)
		INT i = 0
		REPEAT iNumCars i
			IF IS_ENTITY_ALIVE(carArray[i])
			AND carArray[i] != m_vehCar
			//AND carArray[i] != m_vehTanker
			//AND carArray[i] != m_vehTrafficJam[0]
			//AND carArray[i] != m_vehTrafficJam[1]
			AND IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(carArray[i])) // Don't explode helicopters
			AND GET_DISTANCE_BETWEEN_ENTITIES(carArray[i], PLAYER_PED_ID()) < 20
				EXPLODE_VEHICLE(carArray[i])
				CPRINTLN(DEBUG_MISSION, "BENCHMARK: exploded car array ", i)
				EXIT
			ENDIF
		ENDREPEAT
	ENDIF

ENDPROC

FUNC BOOL SHOULD_TRAFFIC_JAM_EXPLODE()

	IF m_bCarExploded = TRUE
		RETURN FALSE // Don't explode the traffic jam if it's already been exploded
	ENDIF

	IF (GET_GAME_TIMER() - m_iWantedLevelTimer) > m_iTimeUntilEndWanted // Safety timer in case none of the below occurs
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: timer elapsed so SHOULD_TRAFFIC_JAM_EXPLODE returning true")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_ALIVE(m_vehTanker)
	AND IS_ENTITY_TOUCHING_ENTITY(m_vehCar, m_vehTanker) // Gang car touches tanker
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: gang car is touching tanker so SHOULD_TRAFFIC_JAM_EXPLODE returning true")
		EXPLODE_VEHICLE(m_vehTanker)
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_ALIVE(m_vehTrafficJam[0])
	AND IS_ENTITY_TOUCHING_ENTITY(m_vehCar, m_vehTrafficJam[0]) // Gang car touches truck pulling the tanker
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: gang car is touching truck so SHOULD_TRAFFIC_JAM_EXPLODE returning true")
		EXPLODE_VEHICLE(m_vehTrafficJam[0])
		RETURN TRUE
	ENDIF
	
	IF DOES_CAM_EXIST(m_camExplosion)
	AND GET_ENTITY_SPEED(m_vehCar) < 1
	AND GET_DISTANCE_BETWEEN_ENTITIES(m_vehCar, m_vehTanker) < 15 // Gang car is stopped near tanker, probably there's another vehicle blocking the gang car ramming the tanker
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: gang car is stopped near tanker so SHOULD_TRAFFIC_JAM_EXPLODE returning true")
		RETURN TRUE
	ENDIF

	IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(m_vehCar)
	AND GET_POSITION_IN_RECORDING(m_vehCar) > 849.0
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: gang car playback position is greater than 849.0")
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

PROC SET_STARTING_PASS_TO_DO()

	IF m_iBenchmarkPassToDo = -1
		m_iCurrentWarpToAreaPosition = 0
	ELSE
		m_iCurrentWarpToAreaPosition = m_iBenchmarkPassToDo
	ENDIF
	CPRINTLN(DEBUG_MISSION, "BENCHMARK: m_iCurrentWarpToAreaPosition initially set to = ", m_iCurrentWarpToAreaPosition)

ENDPROC

PROC RESET_BENCHMARK_VARS()

	m_iInterpFromJetToPedTimer = -1
	m_bDoneRepopulate = FALSE
	m_bPedEnteredCar = FALSE
	m_iShockingEvent = 0
	m_bCreatedTrafficJam = FALSE
	m_bCarExploded = FALSE
	m_iExplodeCarTimer = -1
	m_fAmbientPedDensity = 0.0
	m_fAmbientVehicleDensity = 0.0
	m_fAmbientScenarioDensity = 0.0

ENDPROC

//==================
// BENCHMARK STATES
//==================

PROC BENCHMARK_INIT()	

	IF g_savedGlobals.sFlow.isGameflowActive	//	B* 2106274

		CPRINTLN(DEBUG_MISSION, " - benchmark.sc - BENCHMARK_INIT - Creating a restore point with autosave as g_savedGlobals.sFlow.isGameflowActive = TRUE.")
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: Performing pre-init save and script cleanup")
		Store_RepeatPlay_Snapshot()
		PERFORM_PRE_SAVEGAME_ROUTINE(FALSE,TRUE)
		QUEUE_MISSION_REPEAT_SAVE_FOR_BENCHMARK_TEST()
		WHILE GET_STATUS_OF_MISSION_REPEAT_SAVE() = SAVEGAME_OPERATION_IN_PROGRESS
			WAIT(0)
			CPRINTLN(debug_mission,"BENCHMARK: Waiting for repeat play save to finish")
		ENDWHILE
		//Quit if the savegame failed
		IF GET_STATUS_OF_MISSION_REPEAT_SAVE() = SAVEGAME_OPERATION_FAILED
			CPRINTLN(DEBUG_MISSION,"BENCHMARK: Repeat play save failed. exiting.")
			TERMINATE_THIS_THREAD()
			EXIT
		ELSE
			CPRINTLN(DEBUG_MISSION,"BENCHMARK: Repeat play save was successful.")
		ENDIF
		//Run general script cleanup if this is done
		RUN_GENERAL_SCRIPT_CLEANUP()
	
	ELSE
		CPRINTLN(DEBUG_MISSION, " - benchmark.sc - BENCHMARK_INIT - Skipped the restore save point as g_savedGlobals.sFlow.isGameflowActive = FALSE.")
	ENDIF
	
	CPRINTLN(DEBUG_MISSION, "BENCHMARK: doing init")
	CPRINTLN(DEBUG_MISSION, "BENCHMARK: fScreenAspectRatio ", GET_SCREEN_ASPECT_RATIO())
	CLEAR_PRINTS()
	CLEAR_HELP()
	FORCE_CLOSE_TEXT_INPUT_BOX()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
		SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_NEVER)
	ENDIF
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)
	SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), TRUE)
	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL, 1) // See B*1820422
	#IF IS_DEBUG_BUILD
		BENCHMARK_CREATE_WIDGET()
	#ENDIF
	WAIT(100)
	BENCHMARK_SAFE_FADE_SCREEN_OUT_TO_BLACK(m_iFadeTime, TRUE)
	
	ANIMPOSTFX_STOP_ALL() // B*2204861

	REQUEST_ADDITIONAL_TEXT("BMARK", MISSION_TEXT_SLOT)
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		WAIT(0)
	ENDWHILE
	
	CPRINTLN(DEBUG_MISSION, "BENCHMARK: Starting to wait for building swaps to complete...")
	WHILE NOT g_bInitialBuildingStatesSet
		WAIT(0)
	ENDWHILE
	CPRINTLN(DEBUG_MISSION, "BENCHMARK: ...Finished waiting for building swaps to complete.")
	
	RESET_BENCHMARK_VARS()
	m_iBenchmarkIterationsToDo = GET_BENCHMARK_ITERATIONS()
	IF m_iBenchmarkIterationsToDo < 1
		m_iBenchmarkIterationsToDo = 1
	ENDIF
	CPRINTLN(DEBUG_MISSION, "BENCHMARK: m_iBenchmarkIterationsToDo = ", m_iBenchmarkIterationsToDo)
	m_iBenchmarkPassToDo = GET_BENCHMARK_PASS()
	IF m_iBenchmarkPassToDo < -1
	OR m_iBenchmarkPassToDo >= NUM_WARP_TO_AREA_POSITIONS
		m_iBenchmarkPassToDo = -1
	ENDIF
	CPRINTLN(DEBUG_MISSION, "BENCHMARK: m_iBenchmarkPassToDo = ", m_iBenchmarkPassToDo)
	SET_STARTING_PASS_TO_DO()
	m_vWarpToAreaPosition[0] = <<-9.8369, -1474.0880, 29.5453>> // Franklin house 1
	m_vWarpToAreaPosition[1] = <<806.0358, 1101.2482, 306.0945>> // Vinewood sign
	m_vWarpToAreaPosition[2] =  <<2005.9663, 3803.6094, 31.1808>> // Trevor's trailer
	m_vWarpToAreaPosition[3] =  <<-1713.2614, -1077.4807, 12.0172>> // Pleasure pier
	m_vWarpToAreaPosition[4] = <<-663.8303, 4484.1050, 69.2669>> // Waterfall
	m_fWarpToAreaHeading[0] = 5.4894
	m_fWarpToAreaHeading[1] = 348.7347
	m_fWarpToAreaHeading[2] = 60.1616
	m_fWarpToAreaHeading[3] = 18.8230
	m_fWarpToAreaHeading[4] = 238.4128
	m_vCamPosStart[0] = <<-3.0097, -1484.3165, 41.1898>>
	m_vCamPosStart[1] = <<804.6505, 1184.0697, 347.4004>>
	m_vCamPosStart[2] = <<1987.1251, 3810.7661, 33.2256>>
	m_vCamPosStart[3] = <<-1705.3199, -1074.6646, 21.6983>>
	m_vCamPosStart[4] = <<-608.3302, 4463.3027, 23.4348>>
	m_vCamRotStart[0] = <<0.5292, -0.0000, 24.6248>>
	m_vCamRotStart[1] = <<-7.4378, -0.0000, 110.8038>>
	m_vCamRotStart[2] = <<-2.1421, -0.0000, 37.1211>>
	m_vCamRotStart[3] = <<1.5777, -0.0000, -163.0528>>
	m_vCamRotStart[4] = <<-1.9612, 0.0000, -127.5001>>
	m_vCamPosDest[0] = <<-13.8420, -1454.4639, 32.9113>>
	m_vCamPosDest[1] = <<771.2347, 1091.1151, 337.3588>>
	m_vCamPosDest[2] = <<1967.3821, 3840.4832, 54.8269>>
	m_vCamPosDest[3] = <<-1731.1056, -1098.6263, 24.3947>>
	m_vCamPosDest[4] = <<-615.5822, 4508.3086, 110.3908>>
	m_vCamRotDest[0] = <<-2.4231, 0.0000, 1.8101>>
	m_vCamRotDest[1] = <<-4.9158, -0.0000, 22.8592>>
	m_vCamRotDest[2] = <<-1.2952, -0.0000, 62.8825>>
	m_vCamRotDest[3] = <<-3.3930, -0.0000, -109.8041>>
	m_vCamRotDest[4] = <<-22.8700, 0.0000, -133.0594>>
	m_modelGang[0] = G_M_M_ArmBoss_01
	m_modelGang[1] = G_M_M_ArmGoon_01
	m_modelGang[2] = G_M_M_ArmLieut_01
	m_modelVehTrafficJam[0] = PHANTOM
	m_modelVehTrafficJam[1] = BOXVILLE
	m_modelPedTrafficJam[0] = S_M_Y_Construct_01
	m_modelPedTrafficJam[1] = S_M_M_Trucker_01
	m_vTrafficJam[0] = <<-542.9353, 245.1501, 82.0225>>
	m_vTrafficJam[1] = <<-543.4668, 235.8478, 81.8742>>
	m_fTrafficJam[0] = 173.5275
	m_fTrafficJam[1] = 191.9280
	ADVANCE_BENCHMARK_STATE()
	
ENDPROC

PROC BENCHMARK_WARP_TO_AREA_FADE_OUT()

	CPRINTLN(DEBUG_MISSION, "BENCHMARK: starting warp to area ", m_iCurrentWarpToAreaPosition)
	BENCHMARK_SAFE_FADE_SCREEN_OUT_TO_BLACK(m_iFadeTime, TRUE)
	BENCHMARK_CLEANUP_WARP_TO_AREA()
	IF m_iCurrentWarpToAreaPosition = 0
	AND IS_VECTOR_ZERO(vPlayerStartPosition)
	AND m_vehLastPlayerVehicle = NULL
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
		vPlayerStartPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
		fPlayerStartHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			m_vehLastPlayerVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		ENDIF
		IF IS_ENTITY_ALIVE(m_vehLastPlayerVehicle)
			CPRINTLN(DEBUG_MISSION, "BENCHMARK: stored last player vehicle")
			SET_ENTITY_COORDS(m_vehLastPlayerVehicle, vPlayerStartPosition)
			SET_ENTITY_HEADING(m_vehLastPlayerVehicle, fPlayerStartHeading)
			FREEZE_ENTITY_POSITION(m_vehLastPlayerVehicle, TRUE)
		ENDIF
	ENDIF
	SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), m_vWarpToAreaPosition[m_iCurrentWarpToAreaPosition], m_fWarpToAreaHeading[m_iCurrentWarpToAreaPosition])
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
	ENDIF
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
	CLEAR_AREA(m_vWarpToAreaPosition[m_iCurrentWarpToAreaPosition], 100.0, TRUE)
	IF IS_ENTITY_ALIVE(m_vehLastPlayerVehicle)
		SET_VEHICLE_DOORS_LOCKED(m_vehLastPlayerVehicle, VEHICLELOCK_LOCKED) // So no ambient peds will get in and drive off while the player is elsewhere during the benchmark
		SET_ENTITY_VISIBLE(m_vehLastPlayerVehicle, FALSE)
	ENDIF
	WAIT_FOR_WORLD_TO_LOAD(m_vWarpToAreaPosition[m_iCurrentWarpToAreaPosition], 100, FLAG_MAPDATA | FLAG_COLLISIONS_MOVER, 15000) // Try to ensure streaming requests are completed before we fade in and start the benchmark timing
	BENCHMARK_REPOPULATE_WORLD()
	BENCHMARK_SET_WEATHER(m_iCurrentWarpToAreaPosition)
	IF m_iCurrentWarpToAreaPosition = 4
		BENCHMARK_LOAD_JET_FLYBY_ASSETS()
	ENDIF
	SET_CLOCK_TIME(12, 0, 0)
	m_bDoneWarpToAreaFadeIn = FALSE
	m_bDoneWarpToAreaFadeOut = FALSE

	ADVANCE_BENCHMARK_STATE()
	
ENDPROC

PROC BENCHMARK_WARP_TO_AREA_FADE_IN()

	IF REQUEST_AMBIENT_AUDIO_BANK("TIME_LAPSE")
		m_iTimeToWaitBeforeWarp = 15000
		m_sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
		ADD_CAM_SPLINE_NODE(m_sTimelapse.splineCamera, m_vCamPosStart[m_iCurrentWarpToAreaPosition], m_vCamRotStart[m_iCurrentWarpToAreaPosition], m_iTimeToWaitBeforeWarp)
		ADD_CAM_SPLINE_NODE(m_sTimelapse.splineCamera, m_vCamPosDest[m_iCurrentWarpToAreaPosition], m_vCamRotDest[m_iCurrentWarpToAreaPosition], m_iTimeToWaitBeforeWarp)
		SET_CAM_SPLINE_PHASE(m_sTimelapse.splineCamera, 0.0)
		SET_CAM_FOV(m_sTimelapse.splineCamera, 50)
		SET_CAM_SPLINE_SMOOTHING_STYLE(m_sTimelapse.splineCamera, CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
		SET_CAM_ACTIVE(m_sTimelapse.splineCamera, TRUE)
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: setup spline camera for area ", m_iCurrentWarpToAreaPosition, ", from ", m_vCamPosStart[m_iCurrentWarpToAreaPosition], " to ", m_vCamPosDest[m_iCurrentWarpToAreaPosition])
		m_sTimelapse.iTimeSkipStage = 0
		SET_TODS_CUTSCENE_RUNNING(m_sTimelapse, TRUE, FALSE, 0)
		m_todReference = GET_CURRENT_TIMEOFDAY()
		ADD_TIME_TO_TIMEOFDAY(m_todReference, 0, 0, 23, 0)
		m_iSkipToHour = GET_TIMEOFDAY_HOUR(m_todReference)
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: set timelapse running for area ", m_iCurrentWarpToAreaPosition, ", m_iSkipToHour = ", m_iSkipToHour)
		IF m_iCurrentWarpToAreaPosition = 4
			m_iJetSetupTimer = GET_GAME_TIMER()
		ENDIF
		ADVANCE_BENCHMARK_STATE()
	ENDIF

ENDPROC

PROC BENCHMARK_WARP_TO_AREA_TIMELAPSE()

	IF m_bDoneWarpToAreaFadeIn = FALSE
	AND DOES_CAM_EXIST(m_sTimelapse.splineCamera)
	AND GET_CAM_SPLINE_PHASE(m_sTimelapse.splineCamera) > 0.05
		BENCHMARK_SAFE_FADE_SCREEN_IN_FROM_BLACK(m_iFadeTime, FALSE)
		START_BENCHMARKING()
		m_bDoneWarpToAreaFadeIn = TRUE
	ENDIF
	
	// Start fading out just before the timelapse has finished
	IF m_bDoneWarpToAreaFadeOut = FALSE
	AND m_iCurrentWarpToAreaPosition < NUM_WARP_TO_AREA_POSITIONS-1
	AND DOES_CAM_EXIST(m_sTimelapse.splineCamera)
	AND GET_CAM_SPLINE_PHASE(m_sTimelapse.splineCamera) > 0.88
		STOP_BENCHMARKING()
		BENCHMARK_SAFE_FADE_SCREEN_OUT_TO_BLACK(m_iFadeTime, FALSE)
		m_bDoneWarpToAreaFadeOut = TRUE
	ENDIF
	
	BENCHMARK_SETUP_JET_FLYBY() // Setup the jet flyby for the next benchmark
	
	// Wait for the timelapse to finish
	IF SKIP_TO_TIME_DURING_SPLINE_CAMERA(m_iSkipToHour, 0, "EXTRASUNNY", "cirrocumulus", m_sTimelapse, 0.0)
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: done timelapse for area ", m_iCurrentWarpToAreaPosition)
		
		// Manually cleanup assets instead of calling SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE) because that will do its own fade which we don't want to do here
		IF IS_AUDIO_SCENE_ACTIVE("TOD_SHIFT_SCENE") 
			STOP_SOUND(m_sTimelapse.iSplineStageSound)
			STOP_AUDIO_SCENE("TOD_SHIFT_SCENE")
		ENDIF
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.6)
		CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(FALSE)
		CASCADE_SHADOWS_ENABLE_FREEZER(FALSE)	
        IF g_savedGlobals.sVehicleGenData.eMissionVehTimeStamp != INVALID_TIMEOFDAY
              g_savedGlobals.sVehicleGenData.eMissionVehTimeStamp = GET_CURRENT_TIMEOFDAY()
        ENDIF
		
		m_iCurrentWarpToAreaPosition++
		IF m_iCurrentWarpToAreaPosition < NUM_WARP_TO_AREA_POSITIONS
			IF m_iBenchmarkPassToDo = -1 // GET_BENCHMARK_PASS wasn't set so do all passes
				m_eCurrentBenchmarkState = BENCHMARK_STATE_WARP_TO_AREA_FADE_OUT
			ELSE // GET_BENCHMARK_PASS was set to something so the user only wanted to do one particular pass, it's been done so finish this iteration
				BENCHMARK_SAFE_FADE_SCREEN_OUT_TO_BLACK(m_iFadeTime, TRUE)
				m_eCurrentBenchmarkState = BENCHMARK_STATE_FINISHED
			ENDIF
		ELSE
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			RENDER_SCRIPT_CAMS(FALSE, TRUE, m_iInterpToJetTime, FALSE)
			ADVANCE_BENCHMARK_STATE()
		ENDIF
	ENDIF

ENDPROC

PROC BENCHMARK_JET_FLYBY()

	IF IS_ENTITY_ALIVE(m_vehJet)
	AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(m_vehJet)
		SET_PLAYBACK_SPEED(m_vehJet, m_fJetRecordingPlaybackSpeed)
		INT iCurrentCarrecTime = ROUND(GET_TIME_POSITION_IN_RECORDING(m_vehJet))
		#IF IS_DEBUG_BUILD IF m_bOutputJetRecordingPos = TRUE CPRINTLN(DEBUG_MISSION, "BENCHMARK: get_time_position_in_recording = ", iCurrentCarrecTime) ENDIF #ENDIF
		IF iCurrentCarrecTime > m_iInterpFromJetAtCarrecTime
			CPRINTLN(DEBUG_MISSION, "BENCHMARK: done jet flyby")
			ADVANCE_BENCHMARK_STATE()
		ELIF m_bStartedCinematicCamOnJet = FALSE
			IF iCurrentCarrecTime > m_iStartCinematicCamOnJetAtCarrecTime
				IF m_bUseCinematicCamera = TRUE
					CPRINTLN(DEBUG_MISSION, "BENCHMARK: started cinematic cam on jet at carrec time ", iCurrentCarrecTime)
					SET_CINEMATIC_MODE_ACTIVE(TRUE)
				ENDIF
				m_bStartedCinematicCamOnJet = TRUE
				REQUEST_CAR_AND_PED_MODELS() // Start requesting the car and ped models for the next section
			ENDIF
		ELIF m_bStoppedCinematicCamOnJet = FALSE
			IF iCurrentCarrecTime > m_iStopCinematicCamOnJetAtCarrecTime
				IF m_bUseCinematicCamera = TRUE
					CPRINTLN(DEBUG_MISSION, "BENCHMARK: stopped cinematic cam on jet at carrec time ", iCurrentCarrecTime)
					SET_CINEMATIC_MODE_ACTIVE(FALSE)
				ENDIF
				m_bStoppedCinematicCamOnJet = TRUE
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC
	
PROC BENCHMARK_INTERP_JET_TO_PED()

	IF NOT DOES_CAM_EXIST(m_cameraInterp)
		m_cameraInterp = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), GET_FINAL_RENDERED_CAM_FOV())
		m_cameraInterp2 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<295.1843, 168.9212, 111.3010>>, <<-43.0954, 0.0000, 1.3863>>, GET_FINAL_RENDERED_CAM_FOV())
		SET_CAM_ACTIVE_WITH_INTERP(m_cameraInterp2, m_cameraInterp, m_iTimeInterpFromJetToPed)
		RENDER_SCRIPT_CAMS(TRUE, FALSE, DEFAULT_INTERP_TO_FROM_GAME, FALSE)
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: started camera interp from jet to ground")
		m_iInterpFromJetToPedTimer = GET_GAME_TIMER()
	ENDIF
	BENCHMARK_CREATE_CAR_AND_PEDS()
	BENCHMARK_PED_ENTER_CAR()
	
	// 250 ms after the interp starts, warp the player into the patriot if it has been created by that point, if not warp the player to where the car will be created so he doesn't continue zooming off in the jet and cause world drop out
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND (GET_GAME_TIMER() - m_iInterpFromJetToPedTimer) > 250
		IF IS_ENTITY_ALIVE(m_vehCar)
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), m_vehCar)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), m_vehCar)
				CPRINTLN(DEBUG_MISSION, "BENCHMARK: teleported player into car")
			ENDIF
		ELSE
			IF IS_ENTITY_ALIVE(m_vehJet)
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), m_vehJet)
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), m_vCarPos, m_fCarHeading)
				CPRINTLN(DEBUG_MISSION, "BENCHMARK: teleported player out of jet")
			ENDIF
		ENDIF
	ENDIF
	
	IF (GET_GAME_TIMER() - m_iInterpFromJetToPedTimer) > m_iTimeInterpFromJetToPed
	AND IS_ENTITY_ALIVE(m_vehCar)
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), m_vehCar)
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: done interp from jet to ped")
		m_iWantedLevelTimer = GET_GAME_TIMER()
		ADVANCE_BENCHMARK_STATE()
	ENDIF
			
ENDPROC

PROC BENCHMARK_WANTED_LEVEL()

	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
		SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), FALSE)
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		SET_MAX_WANTED_LEVEL(2)
		SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
	ENDIF
	BENCHMARK_PED_ENTER_CAR()
	IF IS_TRAFFIC_JAM_CREATED()
		BENCHMARK_CAR_RAM_TANKER()
		BENCHMARK_HANDLE_EXPLOSION_CAMERA()
		BENCHMARK_MOVE_VEHICLES_AHEAD_OF_CAR()
		BENCHMARK_BLOW_UP_NEARBY_VEHICLES()
	ENDIF
	IF NOT IS_ENTITY_ALIVE(m_vehCar)
		IF m_iExplodeCarTimer = -1
			m_iExplodeCarTimer = GET_GAME_TIMER()
		ELIF (GET_GAME_TIMER() - m_iExplodeCarTimer) > 5000
			CPRINTLN(DEBUG_MISSION, "BENCHMARK: done wanted level")
			STOP_BENCHMARKING()
			BENCHMARK_SAFE_FADE_SCREEN_OUT_TO_BLACK(m_iFadeTime, TRUE)
			BENCHMARK_CLEANUP_WANTED_LEVEL()
			ADVANCE_BENCHMARK_STATE()
		ENDIF
	ELIF SHOULD_TRAFFIC_JAM_EXPLODE()
		IF GET_ENTITY_HEALTH(m_vehCar) > 0.0
			SET_ENTITY_INVINCIBLE(m_vehCar, FALSE)
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE) // B*2047835, player is in m_vehCar so ensure we set the player back to invincible
			SET_VEHICLE_TYRES_CAN_BURST(m_vehCar, TRUE)
			SET_VEHICLE_WHEELS_CAN_BREAK(m_vehCar, TRUE)
			SET_VEHICLE_STRONG(m_vehCar, FALSE)
			EXPLODE_VEHICLE(m_vehCar)
			m_bCarExploded = TRUE
			m_iExplodeAmbientCarsTimer = GET_GAME_TIMER()
			CPRINTLN(DEBUG_MISSION, "BENCHMARK: set car on fire")
		ENDIF
	ENDIF
	
ENDPROC

PROC BENCHMARK_FINISHED()

	CPRINTLN(DEBUG_MISSION, "BENCHMARK: done all benchmarks")
	IF m_bSaveBenchmarkResults = TRUE
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: saving benchmark results")
		SAVE_END_USER_BENCHMARK()
	ENDIF
	m_iBenchmarkIterationsDone++
	CPRINTLN(DEBUG_MISSION, "BENCHMARK: m_iBenchmarkIterationsDone = ", m_iBenchmarkIterationsDone, " m_iBenchmarkIterationsToDo = ", m_iBenchmarkIterationsToDo)
	IF m_iBenchmarkIterationsDone >= m_iBenchmarkIterationsToDo
		IF UI_STARTED_END_USER_BENCHMARK()
			RESET_END_USER_BENCHMARK()
			//RETURN_PLAYER_TO_START_BENCHMARK_POSITION() // The QUEUE_MISSION_REPEAT_LOAD in BENCHMARK_CLEANUP() handles repositioning the player now
			BENCHMARK_CLEANUP()
		ELSE
			BENCHMARK_QUIT_GAME()
			RESET_END_USER_BENCHMARK()
		ENDIF
	ELSE
		BENCHMARK_CLEANUP(FALSE)
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: finished cleanup for iteration ", m_iBenchmarkIterationsDone)
		RESET_BENCHMARK_VARS()
		SET_STARTING_PASS_TO_DO()
		m_eCurrentBenchmarkState = BENCHMARK_STATE_WARP_TO_AREA_FADE_OUT
	ENDIF

ENDPROC

/// PURPOSE: Returns whether running the benchmark is currently blocked and why
//FUNC BENCHMARK_BLOCK_REASON GET_BENCHMARK_BLOCKED_REASON()
//
//	IF HAS_FORCE_CLEANUP_OCCURRED()
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: HAS_FORCE_CLEANUP_OCCURRED returned true")
//		RETURN BMBR_FORCE_CLEANUP_OCCURRED
//	ELIF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_OF_THIS_SCRIPT_NAME()) > 1
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: benchmark is already running")
//		RETURN BMBR_ALREADY_RUNNING
//	ELIF IS_PLAYER_SWITCH_IN_PROGRESS()
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: player switch is in progress")
//		RETURN BMBR_PLAYER_SWITCH_IN_PROGRESS
//	ELIF Is_Player_Timetable_Scene_In_Progress()
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: player timetable scene is in progress")
//		RETURN BMBR_TIMETABLE_SCENE_IN_PROGRESS
//	ELIF g_bPlayerIsInTaxi
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: player is in a taxi")
//		RETURN BMBR_IN_TAXI
//	ELIF g_bPlayerLockedInToTrigger = TRUE
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: player is locked in to trigger a mission")
//		RETURN BMBR_LOCKED_INTO_TRIGGER
//	ELIF g_bIsOnRampage = TRUE
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: player is on a rampage")
//		RETURN BMBR_ON_RAMPAGE
//	ELIF g_sVehicleGenNSData.bInGarage = TRUE
//	OR GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = g_intPlayerGarage
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: player is in a garage")
//		RETURN BMBR_IN_GARAGE
//	ELIF (IS_PLAYER_IN_ANY_SHOP() OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP() OR IS_ANY_SHOP_INTRO_RUNNING() OR IS_PLAYER_IN_BARBER_CHAIR_OR_DOING_BARBER_INTRO())
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: player is in a shop")
//		RETURN BMBR_IN_SHOP
//	ELIF IS_RESULT_SCREEN_DISPLAYING()
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: result screen is displaying")
//		RETURN BMBR_RESULT_SCREEN_DISPLAYING
//	ELIF IS_AUTOSAVE_REQUEST_IN_PROGRESS()
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: autosave request is in progress")
//		RETURN BMBR_AUTOSAVE_IN_PROGRESS
//	ELIF IS_TRANSITION_ACTIVE()
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: transition is active")
//		RETURN BMBR_TRANSITION_ACTIVE
//	ELIF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: player is wanted by the cops")
//		RETURN BMBR_PLAYER_IS_WANTED
//	ELIF IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: player is being arrested")
//		RETURN BMBR_PLAYER_BEING_ARRESTED
//	ELIF IS_REPEAT_PLAY_ACTIVE(TRUE)
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: already playing a repeat play")
//		RETURN BMBR_REPEAT_PLAY_ACTIVE
//	ELIF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Finale_Choice")) > 0 // B*2203812
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: Finale_Choice is currently running")
//		RETURN BMBR_FINALE_CHOICE_RUNNING
//	ELIF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_OFF_MISSION)  	
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: player isn't off-mission")
//		RETURN BMBR_ON_MISSION
//	ELIF NOT SAFE_TO_SAVE_GAME()
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: not safe to save game")
//		RETURN BMBR_NOT_SAFE_TO_SAVE
//	ELIF g_bScriptsSetSafeForCutscene = TRUE
//		CPRINTLN(DEBUG_MISSION, "BENCHMARK: a cutscene is in progress")
//		RETURN BMBR_CUTSCENE_RUNNING
//	ENDIF
//	
//	CPRINTLN(DEBUG_MISSION, "BENCHMARK: ok to launch benchmark")
//	RETURN BMBR_NONE
//
//ENDFUNC

/// PURPOSE:
///    Returns whether the benchmark should be blocked and why- includes benchmark spefic block prior to featue blocks.
/// PARAMS:
///    bSilenceDebug - 		Turn of debug logging
///    bBlockDupeFeature - 	Check if script is already running
/// RETURNS:
///    
FUNC FEATURE_BLOCK_REASON GET_BENCHMARK_BLOCKED_REASON(BOOL bSilenceDebug = FALSE)

	RETURN GET_FEATURE_BLOCKED_REASON(bSilenceDebug, TRUE)

ENDFUNC

SCRIPT

	CPRINTLN(DEBUG_MISSION, "BENCHMARK: launching benchmark script")
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: The benchmark has been force cleaned up.")
		BENCHMARK_CLEANUP(TRUE)
	ENDIF
	
	IF UI_STARTED_END_USER_BENCHMARK()
		WHILE IS_PAUSE_MENU_ACTIVE()
			WAIT(0)
			CPRINTLN(DEBUG_MISSION, "BENCHMARK:waiting for pause menu to close")
		ENDWHILE
	ENDIF
	
	IF IS_COMMANDLINE_END_USER_BENCHMARK() OR LANDING_SCREEN_STARTED_END_USER_BENCHMARK()
		CPRINTLN(DEBUG_MISSION, "BENCHMARK: Skipping checking for benchmark block reasons due to running from command line.")
		m_eFeatureBlockReason = FBR_NONE
	ELSE
		m_eFeatureBlockReason = GET_BENCHMARK_BLOCKED_REASON()
	ENDIF

	WHILE (TRUE)
	
		IF m_eFeatureBlockReason = FBR_NONE
			
			BENCHMARK_COMMANDS_TO_RUN_EVERY_FRAME()
		
			#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
					CPRINTLN(DEBUG_MISSION, "BENCHMARK: user pressed a debug key to quit")
					BENCHMARK_CLEANUP()
				ENDIF
			#ENDIF
								
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
							
				SWITCH m_eCurrentBenchmarkState
					CASE BENCHMARK_STATE_INIT
						BENCHMARK_INIT()
					BREAK
					CASE BENCHMARK_STATE_WARP_TO_AREA_FADE_OUT
						BENCHMARK_WARP_TO_AREA_FADE_OUT()
					BREAK
					CASE BENCHMARK_STATE_WARP_TO_AREA_FADE_IN
						BENCHMARK_WARP_TO_AREA_FADE_IN()
					BREAK
					CASE BENCHMARK_STATE_WARP_TO_AREA_TIMELAPSE
						BENCHMARK_WARP_TO_AREA_TIMELAPSE()
					BREAK
					CASE BENCHMARK_STATE_JET_FLYBY
						BENCHMARK_JET_FLYBY()
					BREAK
					CASE BENCHMARK_STATE_INTERP_JET_TO_PED
						BENCHMARK_INTERP_JET_TO_PED()
					BREAK
					CASE BENCHMARK_STATE_WANTED_LEVEL
						BENCHMARK_WANTED_LEVEL()
					BREAK
					CASE BENCHMARK_STATE_FINISHED
						BENCHMARK_FINISHED()
					BREAK
					DEFAULT
						SCRIPT_ASSERT("m_eCurrentBenchmarkState is unknown")
					BREAK
				ENDSWITCH
				
				HANDLE_EXITING_BENCHMARKING()
				DISPLAY_FPS()
				
			ENDIF
		
		ELSE // The benchmark cannot launch so display the alert screen and wait for user input
		
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			IF NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				IF m_bPausedGameForAlertScreen = FALSE
					CPRINTLN(DEBUG_MISSION, "BENCHMARK: pausing game and displaying alert screen")
					SET_GAME_PAUSED(TRUE)
					m_bPausedGameForAlertScreen = TRUE
				ENDIF
				HIDE_LOADING_ON_FADE_THIS_FRAME()
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
				
				STRING strFBLabel = GET_FEATURE_BLOCK_MESSAGE_TEXT(m_eFeatureBlockReason)
				SET_WARNING_MESSAGE("FBR_GENERIC", FE_WARNING_CONTINUE, DEFAULT, DEFAULT, DEFAULT, "FBR_BENCHMARK", strFBLabel)
			ELSE
				SET_GAME_PAUSED(FALSE)
				CPRINTLN(DEBUG_MISSION, "BENCHMARK: unpausing game and terminating thread")
				TERMINATE_THIS_THREAD()
			ENDIF
			
		ENDIF
		
		WAIT(0)
		
	ENDWHILE
	
ENDSCRIPT
