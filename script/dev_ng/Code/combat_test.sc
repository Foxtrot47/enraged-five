// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "script_xml.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "commands_streaming.sch"
USING "commands_graphics.sch"
USING "commands_debug.sch"
USING "test_tools.sch"
USING "commands_vehicle.sch"
USING "commands_camera.sch"
USING "commands_player.sch"
USING "commands_physics.sch"
USING "script_DEBUG.sch"
USING "commands_event.sch"
USING "commands_script.sch"
USING "commands_entity.sch"
USING "commands_itemsets.sch"

//Notes: This is script is to allow easy setup of test scenarios for the code department. It works in conjunction with test_tools.sch
//It  is a data driven system 

//Defaults values when the test is reset to factory the standard values are used

STRING XMLMenu = "Testbed/TestCombatmenu"
VECTOR gvMapOffset  //offset all coords from the origin by the psoition of the mapin world coords

////////////////////////////////////////////////////////////////////////////////
///    
//These enums must match with the /build/dev/common/data/script/xml/Testbed/TestCombatMenu.xml
ENUM Scenarios
	TEST_TASK_COMBAT_PED = 0,
	TEST_TASK_COMBAT_HATED_TARGETS_AROUND_PED,
	TEST_TASK_COMBAT_HATED_TARGETS_AROUND_PED_TIMED,
	TEST_TASK_COMBAT_TIMED,
	TEST_TASK_COMBAT_HATED_PED_IN_AREA, 
	TEST_TASK_AIM_GUN_AT_COORD,
	TEST_TASK_AIM_GUN_AT_ENTITY,
	TEST_TASK_SHOOT_AT_COORD,
	TEST_TASK_GOTO_X_WHILE_AIMING_AT_X,
	TEST_TASK_SEEK_COVER_FROM_POS,
	TEST_TASK_SEEK_COVER_FROM_PED,
	TEST_TASK_SEEK_COVER_TO_COVER_POINT,
	TEST_TASK_SEEK_COVER_TO_COVER_COORD,
	TEST_TASK_PUT_PED_DIRECTLY_INTO_COVER,
	TEST_TASK_GUARD_ASSIGNED_DEFENSIVE_AREA,
	TEST_TASK_GUARD_ANGLED_DEFENSIVE_AREA,
	TEST_TASK_GUARD_SPHERE_DEFENSIVE_AREA,
	TEST_TASK_GUARD_CURRENT_POS,
	TEST_TASK_STAND_GUARD,
	SET_PED_PREFERRED_COVER,
	TEST_TASK_PATROL,
	TEST_IS_PED_RESPONDING_TO_EVENT,
	TEST_HAS_PED_RECEIVED_EVENT,
    SCENARIO_PED_MOVE_TO_COVER_PED_STAYS_STILL,
    SCENARIO_BOTH_PEDS_MOVE_TO_COVER,
    SCENARIO_TWO_PEDS_NO_COVER,
    ARifle_guy_in_cover_breaking,
    SquadOfPedsMovingBetweenVehicles,
    SquadStaysDefensiveBehindVehicle,
    SquadDefendingPos,
    PedDoesWeirdThingsSeekingCover,
    ScenarioCombatRanges,
    ScenarioPedShootsCoord, 
    BugPedsNotShootingThroughFreindlyPed, 
    TestPedsUsingRandomPaths,
    BugAiCantDriveTank, 
    ScenarioTaskStayInCover,
    TargetDummy,
  	SWAT_VAN_DRIVE_BY,
	TASK_RAPPEL_DOWN_WALL_PlayerTest,
	TASK_RAPPEL_DOWN_WALL_AITest,
	TEST_TASK_MELEE_COMBAT_ONE,
	TEST_TASK_MELEE_COMBAT_MANY,
	TEST_TASK_DRAG_INJURED_PED,
	TEST_TASK_SHOOT_AT_PLAYER,
	TEST_TASK_SHOOT_AT_VEHICLE,
	TEST_TASK_AI_STEALTH_KILL,
	TEST_TASK_ANIMAL_MELEE,
	TEST_TASK_ARMED_MELEE_IN_COVER,
	
    DefaultTest = 1000
ENDENUM

////////////////////////////////////////////////////////////////////////////////

//Ped Vars
CONST_INT MAX_NUMBER_OF_PEDS            10

peds_struct TestPeds [MAX_NUMBER_OF_PEDS]

//Vehicle Vars
CONST_INT MAX_NUMBER_OF_VEHICLES     10

vehicle_struct TestVehicles[MAX_NUMBER_OF_VEHICLES]

//Route Vars 
CONST_INT MAX_NUMBER_OF_NODES               7

RouteStruct Route[MAX_NUMBER_OF_NODES]

//Camera Vars 
CONST_INT MAX_NUMBER_OF_CAMERAS         2
CONST_INT FixedCamera                                       0
CONST_INT TrackingCamera                                    1

StCameraData TestCams[MAX_NUMBER_OF_CAMERAS]

//Defensive are
INT MAX_NUMBER_DEFENSIVE_AREA  = 0

//Object Vars
CONST_INT MAX_NUMBER_OF_OBJECTS 1
Object_struct TestObjects[MAX_NUMBER_OF_OBJECTS]

////////////////////////////////////////////////////////////////////////////////

// Define Widget Variables

FLOAT DYNAMIC_MOVE_BLEND_RATIO = PEDMOVEBLENDRATIO_WALK
FLOAT SlideSpeed = 1.0
FLOAT BlendSpeed = 8.0
BOOL bLoopAnim = FALSE, bExtractbackwardvel = TRUE, bExtractsidevel = TRUE, bholdlastframe = FALSE
Int seatindex = -1
FLOAT Ground_z
COVERPOINT_INDEX SCRIPTED_COVER_INDEX
BOOL bUseLowCoverPoint = TRUE
BOOL bUseOneHandedWeapon = FALSE
BOOL bUseSwatModel = TRUE
BOOL bTestStanding = TRUE

FLOAT gfGoToPointAndAimAtPointMoveState = 0
INT COMBAT_TARGETS_AROUND_AREA_TIME = 5000

ENUM GOTO_POINT_AND_AIM_MODE
	GOTO_COORD_AND_AIM_AT_COORD,
	GOTO_COORD_AND_AIM_AT_PED,
	GOTO_COORD_AND_AIM_AT_VEHICLE,
	GOTO_COORD_AND_AIM_AT_OBJECT,
	GOTO_PED_AND_AIM_AT_COORD,
	GOTO_PED_AND_AIM_AT_PED,
	GOTO_PED_AND_AIM_AT_VEHICLE,
	GOTO_PED_AND_AIM_AT_OBJECT,
	GOTO_PED_AIMING
ENDENUM

INT 	GOTO_X_AND_AIM_AT_X_MODE = ENUM_TO_INT(GOTO_COORD_AND_AIM_AT_COORD)
VECTOR 	GOTO_COORD = <<-36.82, 247.95, 6.35>>
VECTOR 	AIM_AT_COORD = <<-42.19, 246.29, 7.72>>
INT 	AIM_AT_TIME = 5000
BOOL 	MOVE_AND_SHOOT = FALSE

BOOL 	DO_DRIVEBY = FALSE

////////////////////////////////////////////////////////////////////////////////

 // Create Widgets 
  
PROC CREATE_TEST_WIDGET ()
    START_WIDGET_GROUP("Combat Test")
        SETUP_WIDGET ()
		 	START_WIDGET_GROUP("Tasks")
				ADD_WIDGET_INT_SLIDER ("COMBAT_TARGETS_AROUND_AREA_TIME",COMBAT_TARGETS_AROUND_AREA_TIME,  0, 20000, 1000)
				ADD_WIDGET_INT_SLIDER ("AIM_AT_TIME",AIM_AT_TIME,  -1, 20000, 1000)
				ADD_WIDGET_BOOL ("MOVE_AND_SHOOT", MOVE_AND_SHOOT)
				ADD_WIDGET_BOOL ("SWAT_VAN_DO_DRIVEBY", DO_DRIVEBY)
				ADD_WIDGET_BOOL ("USE_LOW_COVER", bUseLowCoverPoint)
				ADD_WIDGET_BOOL ("USE_ONE_HANDED_WEAPON", bUseOneHandedWeapon)		
				ADD_WIDGET_BOOL ("USE_SWAT_MODEL", bUseSwatModel)	
				ADD_WIDGET_BOOL ("TEST_STAND_ENTERS", bTestStanding)
			STOP_WIDGET_GROUP()
            START_WIDGET_GROUP("Navigation")
                ADD_WIDGET_BOOL ("Start scenario", gBeginCombatScenario)
                ADD_WIDGET_BOOL ("Restart scenario", gResetCombatScenario)
                ADD_WIDGET_BOOL ("Reset to default", gResetToDefault)
                ADD_WIDGET_BOOL ("Debug scenario", gRun_debuggig)
                ADD_WIDGET_BOOL ("Print all setup info", gPrintAllIinfo)
                ADD_WIDGET_FLOAT_SLIDER ("Start_Coord.x",gPedcoords.x,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Start_Coord.y",gPedcoords.y,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Start_Coord.z",gPedcoords.z,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Start_Heading",gPedheading,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Target_Coord.x",DynamicTargetPos.x,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Target_Coord.y",DynamicTargetPos.y,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Target_Coord.z",DynamicTargetPos.z,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Target_heading",DynamicTargetHeading,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("GroiundZ",Ground_z,  -2000, 2000, 0.5)
                ADD_WIDGET_FLOAT_SLIDER ("Slide speed",SlideSpeed,  -2000, 2000, 0.5)
                ADD_WIDGET_INT_SLIDER ("seat", seatindex ,-2, 2, 1 )
				ADD_WIDGET_FLOAT_SLIDER ("Move Blend Ratio", DYNAMIC_MOVE_BLEND_RATIO, PEDMOVEBLENDRATIO_STILL, PEDMOVEBLENDRATIO_SPRINT, 0.1 )
                START_WIDGET_GROUP ("Anim")
                    ADD_WIDGET_BOOL ("loop anim", bLoopAnim )
                    ADD_WIDGET_BOOL ("Extract y vel", bExtractbackwardvel )
                    ADD_WIDGET_BOOL ("Extract x vel ", bExtractsidevel )
                    ADD_WIDGET_BOOL ("Hold ladt frame ", bholdlastframe )
                    ADD_WIDGET_FLOAT_SLIDER ("blend",BlendSpeed,  -2000, 2000, 0.5)
                STOP_WIDGET_GROUP()
            STOP_WIDGET_GROUP()
            START_WIDGET_GROUP ("Ped setup")
                ADD_WIDGET_INT_SLIDER ("Ped_index", gTestPedsIndex ,0, 9, 1 )
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Coord.x",gPedcoords.x,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Coord.y",gPedcoords.y,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Coord.z",gPedcoords.z,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Heading",gPedheading,  -2000, 2000, 0.5)
                    START_NEW_WIDGET_COMBO() 
                         ADD_TO_WIDGET_COMBO ("S_M_Y_COP_01")
                        ADD_TO_WIDGET_COMBO ("G_M_Y_DESHENCH_01")
                    STOP_WIDGET_COMBO("Ped", PedIdentifier)
                        ADD_WIDGET_BOOL ("Create ped", gCreatePed)
                        ADD_WIDGET_BOOL ("Delete ped", gdeletePed)
                        ADD_WIDGET_INT_SLIDER ("Set Ped rel grp", gpedrelgrp  ,1, 4, 1 )
                    START_NEW_WIDGET_COMBO() 
                        ADD_TO_WIDGET_COMBO ("WEAPONTYPE_UNARMED")
                        ADD_TO_WIDGET_COMBO ("WEAPONTYPE_PISTOL")
                        ADD_TO_WIDGET_COMBO ("WEAPONTYPE_ASSAULTRIFLE")
                        ADD_TO_WIDGET_COMBO ("WEAPONTYPE_M4")
                    STOP_WIDGET_COMBO("weapon", WeaponIdentifier)
                        ADD_WIDGET_BOOL ("Swap current peds weapon", gSwapPedWeapons) 
                        ADD_WIDGET_BOOL ("print ped info", Bprint_ped_info)
                ADD_WIDGET_BOOL ("activate def area",gPedDefAActive)    
            STOP_WIDGET_GROUP ()            
            START_WIDGET_GROUP ("Route Builder")
                ADD_WIDGET_FLOAT_SLIDER ("RoutePoint.x",gRoutePoint.x,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("RoutePoint.y",gRoutePoint.y,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("RoutePoint.z",gRoutePoint.z,  -2000, 2000, 0.25)
                ADD_WIDGET_INT_SLIDER ("Route Node", gRouteNode, 0, 7, 1 )
                ADD_WIDGET_BOOL ("Register Node", BSetNode)
                ADD_WIDGET_BOOL ("copy cam pos", BCopyCamCoords )
            STOP_WIDGET_GROUP ()
            START_WIDGET_GROUP("GOTO_X_AND_AIM_AT_X")
                START_NEW_WIDGET_COMBO() 
                    ADD_TO_WIDGET_COMBO("GOTO_COORD_AND_AIM_AT_COORD")
                    ADD_TO_WIDGET_COMBO("GOTO_COORD_AND_AIM_AT_PED")
                    ADD_TO_WIDGET_COMBO("GOTO_COORD_AND_AIM_AT_VEHICLE")
                    ADD_TO_WIDGET_COMBO("GOTO_COORD_AND_AIM_AT_OBJECT")
                    ADD_TO_WIDGET_COMBO("GOTO_PED_AND_AIM_AT_COORD")
                    ADD_TO_WIDGET_COMBO("GOTO_PED_AND_AIM_AT_PED")
                    ADD_TO_WIDGET_COMBO("GOTO_PED_AND_AIM_AT_VEHICLE")
                    ADD_TO_WIDGET_COMBO("GOTO_PED_AND_AIM_AT_OBJECT")
					ADD_TO_WIDGET_COMBO("GOTO_PED_AIMING")
				STOP_WIDGET_COMBO("GOTO_X_AND_AIM_AT_X_MODE", GOTO_X_AND_AIM_AT_X_MODE)
                ADD_WIDGET_FLOAT_SLIDER("GOTO_COORD.x", GOTO_COORD.x, -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER("GOTO_COORD.y", GOTO_COORD.y, -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER("GOTO_COORD.z", GOTO_COORD.z, -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER("AIM_AT_COORD.x", AIM_AT_COORD.x, -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER("AIM_AT_COORD.y", AIM_AT_COORD.y, -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER("AIM_AT_COORD.z", AIM_AT_COORD.z, -2000, 2000, 0.25)
				ADD_WIDGET_FLOAT_SLIDER ("Move Blend Ratio", gfGoToPointAndAimAtPointMoveState, PEDMOVEBLENDRATIO_STILL, PEDMOVEBLENDRATIO_SPRINT, 0.1 )
                ADD_WIDGET_BOOL("MOVE_AND_SHOOT", MOVE_AND_SHOOT)
            STOP_WIDGET_GROUP()
        STOP_WIDGET_GROUP()
ENDPROC

////////////////////////////////////////////////////////////////////////////////

//Mission flow House keeping


//PUPROSE: REmoves all the scenarios and resets all the relationships
    PROC Cleanup_Scenario_Entities ()
    int index = 0
        for index = 0 to MAX_NUMBER_OF_PEDS -1  
            CLEAN_UP_PED_ENTITIES (TestPeds[index].ped)
        ENDFOR 
        
        for index = 0 to MAX_NUMBER_OF_VEHICLES -1
            CLEAN_UP_VEHICLE_ENTITIES (TestVehicles[index].Vehicle )
        ENDFOR
        
        for index = 0 to MAX_NUMBER_OF_OBJECTS -1
            CLEAN_UP_OBJECT_ENTITIES (TestObjects[index].Object)
        ENDFOR

        Set_Gang_Relationships (FALSE)  
    ENDPROC

PROC Terminate_test_script ()
    if IS_KEYBOARD_KEY_JUST_PRESSED (KEY_S)
        CLEAR_PRINTS ()
        CLEAR_HELP ()
        Cleanup_Scenario_Entities ()
        Temp_cleanup_scenario_cams ()
        SET_PLAYER_COLISION(scplayer, true)
        IF NOT IS_PED_INJURED (scplayer)
                  SET_PED_COORDS_KEEP_VEHICLE(scplayer, GET_PLAYER_START_POS () + gvMapOffset)
        ENDIF
        
        TERMINATE_THIS_THREAD ()
        
    ENDIF
ENDPROC


//END: Mission flow House keeping

//Function: SCENARIO TASKS

SEQUENCE_INDEX TEST_SEQ

FUNC SEQUENCE_INDEX TASK_SHOOT_AT_COORD_SEQ ( VECTOR L_Target_Pos)

        SEQUENCE_INDEX TEST_SEQUENCE
        OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
            TASK_SHOOT_AT_COORD  (NULL, L_Target_Pos, -1, FIRING_TYPE_1_BURST)
        CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)

        RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX FOLLOW_NAVMESH_TO_COORD_WALK_SEQ ( VECTOR L_Target_Pos)

        SEQUENCE_INDEX TEST_SEQUENCE
        OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, L_Target_Pos, PEDMOVE_WALK,  -2, 0.25)
        CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)

        RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX FOLLOW_NAVMESH_TO_COORD_RUN_SEQ ( VECTOR L_Target_Pos)

        SEQUENCE_INDEX TEST_SEQUENCE
        OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, L_Target_Pos, PEDMOVE_RUN,  -2, 0.25)
        CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
        
        RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX FOLLOW_NAVMESH_TO_COORD_SPRINT_SEQ ( VECTOR L_Target_Pos)
        SEQUENCE_INDEX TEST_SEQUENCE
        OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, L_Target_Pos, PEDMOVE_SPRINT,  -2, 0.25)
        CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)

        RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX FOLLOW_ROUTE( RouteStruct &TargetRoute[])
        int index 
        BOOL BHaveValidNode = FALSE
        VECTOR TempNode
        
        //This allows us to pass a sequnce to a ped using 7 points for maximun testing. Iess than 7 nodes can be used, and the last valid node is copied into the remaining array elements.
        //Note all nodes must be in order there can be no gaps between them ie node 1 valid, 2 valid, 3 invalid is ok but not 1 valid, 2 invalid, 3  valid 
        //search our node array until we find a invalid array point
        for index = 0 to MAX_NUMBER_OF_NODES -1
            IF TargetRoute[index].RouteNodeState = FALSE
                IF index <> 0
                    TempNode = TargetRoute[index - 1].RouteNode //grab the previous point which is valid assuming that all nodes are sequential
                    BHaveValidNode = TRUE
                ELSE
                    SCRIPT_ASSERT ("Passed in sequnce with no valid nodes ")
                ENDIF
            ENDIF
            
            //copy this valid node into the remaing array elements
            IF (BHaveValidNode)
                    TargetRoute[index].RouteNode = TempNode 
            ENDIF
                
        ENDFOR  
        
        SEQUENCE_INDEX TEST_SEQUENCE
        OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[0].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -2, 1)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[1].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -2, 1)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[2].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -2, 1)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[3].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -2, 1)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[4].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -2, 1)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[5].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -2, 1)
            TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[6].routenode, DYNAMIC_MOVE_BLEND_RATIO,  -2, 1)
        CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
        RETURN TEST_SEQUENCE
ENDFUNC


    
//SET UP
PROC SETUP_PATROL_ROUTES ()

    GET_GROUND_Z_FOR_3D_COORD (<<1.0, 1.0, 25>>, Ground_z)

    OPEN_PATROL_ROUTE ("miss_loop")
        ADD_PATROL_ROUTE_NODE ( 1, "StandGuard",  <<1.0, 1.0, Ground_z>>  , <<-4.94, 11.30, 7.0>>, 4000 )
        ADD_PATROL_ROUTE_NODE ( 2, "StandGuard",  <<1.0, 10.0, Ground_z>> , <<0.0, 0.0, 0.0>> , 2000 )
        ADD_PATROL_ROUTE_NODE ( 3, "StandGuard",  <<10.0, 1.0, Ground_z>> , <<0.0, 0.0, 0.0>>, 4000 )
    
        ADD_PATROL_ROUTE_LINK (1, 2)
        ADD_PATROL_ROUTE_LINK (2, 3)
        ADD_PATROL_ROUTE_LINK (3, 1)
    CLOSE_PATROL_ROUTE ()
    
    CREATE_PATROL_ROUTE ()
    
    OPEN_PATROL_ROUTE ("miss_branched_loop")
        ADD_PATROL_ROUTE_NODE ( 1, "StandGuard",  <<-17.77, 1.77, Ground_z>>, <<0.0, 0.0, 0.0>>  )
        ADD_PATROL_ROUTE_NODE ( 2, "StandGuard",  <<-17.38, 9.63,  Ground_z>>, <<0.0, 0.0, 0.0>>  )
        ADD_PATROL_ROUTE_NODE ( 3, "StandGuard",  <<-26.82, 17.10, Ground_z>>, <<0.0, 0.0, 0.0>> )
        ADD_PATROL_ROUTE_NODE ( 4, "StandGuard",  <<-25.76, -1.33, Ground_z>>, <<0.0, 0.0, 0.0>> )
        ADD_PATROL_ROUTE_NODE ( 5, "StandGuard",  <<-10.45, 11.47,  Ground_z>>, <<0.0, 0.0, 0.0>> )
        ADD_PATROL_ROUTE_NODE ( 6, "StandGuard",  <<-10.61, -3.34, Ground_z>>, <<0.0, 0.0, 0.0>>  )
    
        ADD_PATROL_ROUTE_LINK (1, 2)
        ADD_PATROL_ROUTE_LINK (2, 3)
        ADD_PATROL_ROUTE_LINK (3, 4)
        ADD_PATROL_ROUTE_LINK (4, 1)
        ADD_PATROL_ROUTE_LINK (2, 5)
        ADD_PATROL_ROUTE_LINK (5, 6)
        ADD_PATROL_ROUTE_LINK (6, 1)
        
        
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
    
    OPEN_PATROL_ROUTE ("miss_branched")
        ADD_PATROL_ROUTE_NODE ( 1, "StandGuard",  <<21.94, -1.26, Ground_z>> , <<0.0, 0.0, 0.0>>  )
        ADD_PATROL_ROUTE_NODE ( 2, "StandGuard",  <<30.09, 0.43,  Ground_z>> , <<0.0, 0.0, 0.0>>  )
        ADD_PATROL_ROUTE_NODE ( 3, "StandGuard",  <<41.27, -3.28, Ground_z>> , <<0.0, 0.0, 0.0>>  )
        ADD_PATROL_ROUTE_NODE ( 4, "StandGuard",  <<32.41, 12.16, Ground_z>> , <<0.0, 0.0, 0.0>>  )
        ADD_PATROL_ROUTE_NODE ( 5, "StandGuard",  <<18.41, 20.38,  Ground_z>> , <<0.0, 0.0, 0.0>>  )
        ADD_PATROL_ROUTE_NODE ( 6, "StandGuard",  <<45.12, 7.63, Ground_z>> , <<0.0, 0.0, 0.0>>  )
    
        ADD_PATROL_ROUTE_LINK (1, 2)
        ADD_PATROL_ROUTE_LINK (2, 3)
        ADD_PATROL_ROUTE_LINK (2, 4)
        ADD_PATROL_ROUTE_LINK (4, 5)
        ADD_PATROL_ROUTE_LINK (4, 6)
    
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
    
    
        OPEN_PATROL_ROUTE ("miss_dead_end")
        ADD_PATROL_ROUTE_NODE ( 1, "StandGuard",  <<10.63, -17.70, Ground_z>> , <<0.0, 0.0, 0.0>>  )
        ADD_PATROL_ROUTE_NODE ( 2, "StandGuard",  <<1.46, -16.37,  Ground_z>> , <<0.0, 0.0, 0.0>>  )
        ADD_PATROL_ROUTE_NODE ( 3, "StandGuard",  <<1.57, -7.94, Ground_z>> , <<0.0, 0.0, 0.0>>  )
        ADD_PATROL_ROUTE_NODE ( 4, "StandGuard",  <<15.67, -7.83, Ground_z>> , <<0.0, 0.0, 0.0>>  )

        ADD_PATROL_ROUTE_LINK (1, 2)
        ADD_PATROL_ROUTE_LINK (2, 3)
        ADD_PATROL_ROUTE_LINK (3, 4)
    
    
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
    
    
ENDPROC 

FUNC SEQUENCE_INDEX FOLLOW_PATROL_ROUTE ( STRING RouteName, PATROL_ALERT_STATE alert_state )
        SEQUENCE_INDEX TEST_SEQUENCE
        OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
            TASK_PATROL (null, RouteName, alert_state, true )
        CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)

        RETURN TEST_SEQUENCE
ENDFUNC

PROC DRIVE_TO_POINT_SEQUENCE (PED_INDEX ped, VEHICLE_INDEX veh)
    SEQUENCE_INDEX SEQ
    OPEN_SEQUENCE_TASK (SEQ)
//      IF NOT IS_PED_INJURED(TestPeds[1].Ped)
//          TASK_VEHICLE_SHOOT_AT_PED(NULL, TestPeds[1].Ped )
//      ENDIF
        TASK_FLUSH_ROUTE()
        TASK_EXTEND_ROUTE(<< -17.30, -6.60, 1.19>>+ gvMapOffset )
        TASK_EXTEND_ROUTE(<<-16.69, 12.23, 1.19 >>+ gvMapOffset ) 
        TASK_EXTEND_ROUTE(<<12.18, 12.76, 1.19 >> + gvMapOffset)
        TASK_EXTEND_ROUTE(<<13.37, -14.18, 1.19 >>+ gvMapOffset )
        TASK_DRIVE_POINT_ROUTE (NULL, veh, 1.0  )
//      IF bShootAtPed1 = TRUE
//          ADD_VEHICLE_SUBTASK_ATTACK_PED(NULL, TestPeds[1].Ped )
//      ENDIF
        SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
    CLOSE_SEQUENCE_TASK(SEQ)
    
    IF NOT IS_PED_INJURED (ped)
        TASK_PERFORM_SEQUENCE (ped, seq)
        
        CLEAR_SEQUENCE_TASK (seq)
    ENDIF   
ENDPROC

//Function End: SCENARIO TASKS

//Functions: DEFENSIVE AREA
STRUCT DefSphereSt
    VECTOR DefensiveAreaPos
    FLOAT   DefensiveAreaRadius
    VECTOR DefensiveAreaDirection
ENDSTRUCT

DefSphereSt DefensiveSphereAreas [4]
int iCurrentDefensiveArea = 0



ENUM SCENARIO_STATE_ENUM
	STATE_INIT,
	STATE_1,
	STATE_2,
	STATE_3,
	STATE_4
ENDENUM

SCENARIO_STATE_ENUM SCENARIO_STATE = STATE_INIT

ENUM PREFERRED_COVER_FLAGS
	PREFERRED_COVER_INIT,
	PREFERRED_COVER_RUN,
	PREFERRED_COVER_FINISH
ENDENUM

PREFERRED_COVER_FLAGS PREFERRED_COVER_STATE = PREFERRED_COVER_INIT

// SCENARIO: SquadOfPedsMovingBetweenVehicles  
   
ENUM Scenario_SquadOfPedsMovingBetweenVehicles_Flags
    Scenario_SquadOfPedsMovingBetweenVehicles_init,
    Scenario_SquadOfPedsMovingBetweenVehicles_SwapDefAreas
ENDENUM


    Scenario_SquadOfPedsMovingBetweenVehicles_Flags Scenario_SquadOfPedsMovingBetweenVehicles = Scenario_SquadOfPedsMovingBetweenVehicles_init

ENUM Scenario_TaskStayInCover_Flags
    Scenario_TaskStayInCover_init,
    Scenario_TaskStayInCover_SwapDefAreas
ENDENUM
    
    Scenario_TaskStayInCover_Flags Scenario_TaskStayInCover = Scenario_TaskStayInCover_init

//  Peds moving behind a moving vehicle
ENUM Scenario_SquadStaysDefensiveBehindVehicle_Flags
    Scenario_SquadStaysDefensiveBehindVehicle_init,
    Scenario_SquadStaysDefensiveBehindVehicle_Run
ENDENUM

Scenario_SquadStaysDefensiveBehindVehicle_Flags Scenario_SquadStaysDefensiveBehindVehicle = Scenario_SquadStaysDefensiveBehindVehicle_init

// Scenario_SquadDefendingPos
ENUM Scenario_SquadDefendingPos_Flags
 Scenario_SquadDefendingPos_init,
 Scenario_SquadDefendingPos_grp2,
  Scenario_SquadDefendingPos_grp3
ENDENUM

Scenario_SquadDefendingPos_Flags    Scenario_SquadDefendingPos = Scenario_SquadDefendingPos_init

//Scenario_PedDoesWeirdThingsSeekingCover
ENUM Scenario_PedDoesWeirdThingsSeekingCover_Flags
    Scenario_PedDoesWeirdThingsSeekingCover_init,
    Scenario_PedDoesWeirdThingsSeekingCover_run
ENDENUM

Scenario_PedDoesWeirdThingsSeekingCover_Flags Scenario_PedDoesWeirdThingsSeekingCover_Status = Scenario_PedDoesWeirdThingsSeekingCover_init

ENUM Scenario_CombatRanges_Flags
    Scenario_CombatRanges_init, 
    Scenario_CombatRanges_run
ENDENUM
Scenario_CombatRanges_Flags Scenario_CombatRanges_Status =  Scenario_CombatRanges_init

PROC INITALISE_TEST_STATE()
    //reset each scenario
    SingleTaskStatus = startSingleTask
    //reset each scenario
    Scenario_SquadOfPedsMovingBetweenVehicles = Scenario_SquadOfPedsMovingBetweenVehicles_init
    Scenario_SquadStaysDefensiveBehindVehicle = Scenario_SquadStaysDefensiveBehindVehicle_Init
    Scenario_SquadDefendingPos = Scenario_SquadDefendingPos_init
    Scenario_PedDoesWeirdThingsSeekingCover_Status = Scenario_PedDoesWeirdThingsSeekingCover_init
    Scenario_TaskStayInCover = Scenario_TaskStayInCover_init
	SCENARIO_STATE = STATE_INIT
    //end reset
ENDPROC

ITEMSET_INDEX PreferredCoverItemSet = NULL
BOOL ADDED_PREFERRED_COVER_POINTS = FALSE

VECTOR COV_POS1 = <<4.21, 184.97, 6.35>>
VECTOR COV_POS2 = <<9.84, 184.98, 6.35>>
VECTOR COV_POS3 = <<14.36, 185.07, 6.35>>
VECTOR COV_POS4 = <<21.17, 178.58, 6.35>>

PROC SETUP_COMBAT_TARGETS_DATA ()

    TestPeds[0].PedsCoords = <<10.76, 238.84, 6.35>>
    TestPeds[0].PedHeading = 0
    TestPeds[0].Pedrelgrp = 1
   	TestPeds[0].PedModel = S_M_Y_Cop_01
    TestPeds[0].PedsWeapon = WEAPONTYPE_SMG
    TestPeds[0].PedcombatMove = 0
    TestPeds[0].pedcombatrange = 2
    TestPeds[0].combatcover = CA_USE_COVER
    TestPeds[0].bcombatcover = TRUE
	
	TestPeds[1].PedsCoords = <<8.91, 244.60, 6.35>>
    TestPeds[1].PedHeading = 0
    TestPeds[1].Pedrelgrp = 2
    TestPeds[1].PedModel = S_M_Y_MARINE_01
    TestPeds[1].PedsWeapon = WEAPONTYPE_CARBINERIFLE
    TestPeds[1].PedcombatMove = 0
    TestPeds[1].pedcombatrange = 2
    TestPeds[1].combatcover = CA_USE_COVER
    TestPeds[1].bcombatcover = TRUE	
	
	TestPeds[2].PedsCoords = <<11.04, 244.83, 6.35>>
    TestPeds[2].PedHeading = 0
    TestPeds[2].Pedrelgrp = 2
    TestPeds[2].PedModel = S_M_Y_MARINE_02
    TestPeds[2].PedsWeapon = WEAPONTYPE_CARBINERIFLE
    TestPeds[2].PedcombatMove = 0
    TestPeds[2].pedcombatrange = 2
    TestPeds[2].combatcover = CA_USE_COVER
    TestPeds[2].bcombatcover = TRUE	
	
	TestCams[FixedCamera].cam_pos = <<12.399977,229.328735,13.197482>>
    TestCams[FixedCamera].cam_rot = <<-31.897488,-0.000000,-9.631414>>
    TestCams[FixedCamera].cam_fov = 65.0
    TestCams[FixedCamera].bActivateCam = TRUE   
	
ENDPROC

PROC SETUP_AIM_GUN_DATA()

    TestPeds[0].PedsCoords = <<-41.63, 243.31, 6.35>>
    TestPeds[0].PedHeading = 0
    TestPeds[0].Pedrelgrp = 1
    TestPeds[0].PedModel = S_M_Y_Cop_01
    TestPeds[0].PedsWeapon = WEAPONTYPE_MINIGUN
    TestPeds[0].PedcombatMove = 0
    TestPeds[0].pedcombatrange = 2
    TestPeds[0].combatcover = CA_USE_COVER
    TestPeds[0].bcombatcover = TRUE
	
	TestPeds[1].PedsCoords = <<-34.78, 243.96, 6.35>>
    TestPeds[1].PedHeading = 0
    TestPeds[1].Pedrelgrp = 2
    TestPeds[1].PedModel = S_M_Y_MARINE_01
    TestPeds[1].PedsWeapon = WEAPONTYPE_CARBINERIFLE
    TestPeds[1].PedcombatMove = 0
    TestPeds[1].pedcombatrange = 2
    TestPeds[1].combatcover = CA_USE_COVER
    TestPeds[1].bcombatcover = TRUE	
	
	TestPeds[2].PedsCoords = <<-34.78, 240.96, 6.35>>
    TestPeds[2].PedHeading = 0
    TestPeds[2].Pedrelgrp = 2
    TestPeds[2].PedModel = S_M_Y_MARINE_01
    TestPeds[2].PedsWeapon = WEAPONTYPE_CARBINERIFLE
    TestPeds[2].PedcombatMove = 0
    TestPeds[2].pedcombatrange = 2
    TestPeds[2].combatcover = CA_USE_COVER
    TestPeds[2].bcombatcover = TRUE	
	
	TestVehicles [0].vehicleCoords = <<-33.68, 249.10, 6.35>>
    TestVehicles [0].vehicleHeading = 90
    TestVehicles [0].vehiclemodel = TAILGATER
	
	TestObjects[0].ObjectCoords = <<-43.51, 242.88, 6.35>>
	TestObjects[0].ObjectModel = PROP_LETTERBOX_01
	
	TestCams[FixedCamera].cam_pos = <<-42.041958,239.649292,10.411165>>
    TestCams[FixedCamera].cam_rot = <<-36.519039,-0.000000,-13.888417>>
    TestCams[FixedCamera].cam_fov = 65.0
    TestCams[FixedCamera].bActivateCam = TRUE   
	
ENDPROC

PROC SETUP_COVER_DATA()

	IF bUseLowCoverPoint
		TestPeds[0].PedsCoords = <<-64.11, 92.81, 6.35>>
	ELSE
   		TestPeds[0].PedsCoords = <<-63.36, 98.82, 6.35>>
	ENDIF
	 
    TestPeds[0].PedHeading = 98.00
    TestPeds[0].Pedrelgrp = 1
    IF bUseSwatModel
		TestPeds[0].PedModel = S_M_Y_SWAT_01
	ELSE
    	TestPeds[0].PedModel = S_M_Y_Cop_01
	ENDIF
	
	IF bUseOneHandedWeapon
		 TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
	ELSE
		 TestPeds[0].PedsWeapon = WEAPONTYPE_CARBINERIFLE
	ENDIF
   
    TestPeds[0].PedcombatMove = 0
    TestPeds[0].pedcombatrange = 2
    TestPeds[0].combatcover = CA_USE_COVER
    TestPeds[0].bcombatcover = TRUE
		
    TestPeds[1].PedsCoords = <<-68.53, 101.53, 6.35>>
    TestPeds[1].PedHeading = 0
    TestPeds[1].Pedrelgrp = 2
    TestPeds[1].PedModel = S_F_M_MAID_01
    TestPeds[1].PedsWeapon = WEAPONTYPE_MINIGUN
    TestPeds[1].PedcombatMove = 0
    TestPeds[1].pedcombatrange = 2
    TestPeds[1].combatcover = CA_USE_COVER
    TestPeds[1].bcombatcover = TRUE

	TestCams[FixedCamera].cam_pos = <<-55.604721,99.186378,11.271409>>
    TestCams[FixedCamera].cam_rot = <<-34.457100,-0.000000,106.999374>>
    TestCams[FixedCamera].cam_fov = 65.0
    TestCams[FixedCamera].bActivateCam = TRUE   
	
ENDPROC

PROC SETUP_GUARD_DATA()

    TestPeds[0].PedsCoords = <<13.77, 182.27, 6.35>>
    TestPeds[0].PedHeading = 0
    TestPeds[0].Pedrelgrp = 1
    TestPeds[0].PedModel = S_M_Y_Cop_01
    TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
    TestPeds[0].PedcombatMove = 0
    TestPeds[0].pedcombatrange = 2
    TestPeds[0].combatcover = CA_USE_COVER
    TestPeds[0].bcombatcover = TRUE

	TestCams[FixedCamera].cam_pos = <<6.797447,175.978683,15.128465>>
    TestCams[FixedCamera].cam_rot = <<-38.216122,0.225905,-52.729095>>
    TestCams[FixedCamera].cam_fov = 65.0
    TestCams[FixedCamera].bActivateCam = TRUE   
	
ENDPROC

PROC SETUP_COMBAT_PED_DATA ()

	gbPlayerShouldBeAtCamPos = TRUE
		
    TestPeds[0].PedsCoords = <<21.75, -56.00, 6.35>>
    TestPeds[0].PedHeading = 0
    TestPeds[0].Pedrelgrp = 1
    TestPeds[0].PedModel = S_M_Y_Cop_01
    TestPeds[0].PedsWeapon = WEAPONTYPE_SMG
    TestPeds[0].PedcombatMove = 0
    TestPeds[0].pedcombatrange = 2
    TestPeds[0].combatcover = CA_USE_COVER
    TestPeds[0].bcombatcover = TRUE
	
	TestPeds[1].PedsCoords = <<24.13, -46.20, 6.35>>
    TestPeds[1].PedHeading = 180
    TestPeds[1].Pedrelgrp = 2
    TestPeds[1].PedModel = S_M_Y_MARINE_01
    TestPeds[1].PedsWeapon = WEAPONTYPE_CARBINERIFLE
    TestPeds[1].PedcombatMove = 0
    TestPeds[1].pedcombatrange = 2
    TestPeds[1].combatcover = CA_USE_COVER
    TestPeds[1].bcombatcover = TRUE	
	
	TestCams[FixedCamera].cam_pos = <<13.917382,-50.876545,7.909274>>
    TestCams[FixedCamera].cam_rot = <<-3.043049,0.000000,-102.989365>>
    TestCams[FixedCamera].cam_fov = 65.0
    TestCams[FixedCamera].bActivateCam = TRUE   
	
ENDPROC

PROC SETUP_PREFERRED_COVER_DATA ()
ENDPROC

PROC SETUP_TEST_DATA ()

    SWITCH int_to_enum (scenarios, gcurrentselection)
		CASE TEST_TASK_COMBAT_PED		
			SETUP_COMBAT_PED_DATA ()
		BREAK
		CASE TEST_TASK_COMBAT_HATED_TARGETS_AROUND_PED
		CASE TEST_TASK_COMBAT_HATED_TARGETS_AROUND_PED_TIMED
		CASE TEST_TASK_COMBAT_TIMED
		CASE TEST_TASK_COMBAT_HATED_PED_IN_AREA
		
			SETUP_COMBAT_TARGETS_DATA ()
			
		BREAK
		
		CASE TEST_TASK_AIM_GUN_AT_COORD
		CASE TEST_TASK_AIM_GUN_AT_ENTITY
		CASE TEST_TASK_SHOOT_AT_COORD
		CASE TEST_TASK_GOTO_X_WHILE_AIMING_AT_X
		
			SETUP_AIM_GUN_DATA()
			
		BREAK
		
		CASE TEST_TASK_SEEK_COVER_FROM_POS
		CASE TEST_TASK_SEEK_COVER_FROM_PED
		CASE TEST_TASK_SEEK_COVER_TO_COVER_POINT
		CASE TEST_TASK_SEEK_COVER_TO_COVER_COORD
		CASE TEST_TASK_PUT_PED_DIRECTLY_INTO_COVER
		
			gbPlayerShouldBeAtCamPos = FALSE
			PlayerStartPos = <<-62.38, 88.74, 6.35>>
			
			SETUP_COVER_DATA()
			
		BREAK
		
		CASE TEST_TASK_GUARD_ASSIGNED_DEFENSIVE_AREA
		CASE TEST_TASK_GUARD_ANGLED_DEFENSIVE_AREA
		CASE TEST_TASK_GUARD_SPHERE_DEFENSIVE_AREA
		CASE TEST_TASK_GUARD_CURRENT_POS
		CASE TEST_TASK_STAND_GUARD
			
			gbPlayerShouldBeAtCamPos = TRUE
			SETUP_GUARD_DATA()
			
		BREAK
		
		CASE SET_PED_PREFERRED_COVER	
	
			gbPlayerShouldBeAtCamPos = FALSE
			PlayerStartPos = <<13.96, 193.80, 6.35>>
				
		    TestPeds[0].PedsCoords = <<11.90, 172.52, 6.35>>
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_Cop_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            TestPeds[0].PedcombatMove = 0
            TestPeds[0].pedcombatrange = 2
            TestPeds[0].combatcover = CA_USE_COVER
            TestPeds[0].bcombatcover = TRUE
			
			IF NOT ADDED_PREFERRED_COVER_POINTS	
				COVERPOINT_INDEX coverIndex	
				PreferredCoverItemSet = CREATE_ITEMSET(true)
				coverIndex = ADD_COVER_POINT(COV_POS1, 0.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
				ADD_TO_ITEMSET(coverIndex, PreferredCoverItemSet)
				coverIndex = ADD_COVER_POINT(COV_POS2, 0.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
				ADD_TO_ITEMSET(coverIndex, PreferredCoverItemSet)
				coverIndex = ADD_COVER_POINT(COV_POS3, 0.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
				ADD_TO_ITEMSET(coverIndex, PreferredCoverItemSet)
				coverIndex = ADD_COVER_POINT(COV_POS4, 0.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
				ADD_TO_ITEMSET(coverIndex, PreferredCoverItemSet)	
				ADDED_PREFERRED_COVER_POINTS = TRUE
			ENDIF
			
			PREFERRED_COVER_STATE = PREFERRED_COVER_INIT
		BREAK

		CASE TEST_TASK_PATROL
		
		 	TestPeds[0].PedsCoords = << 0.95, -5.32, 7.36>>
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_Cop_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_UNARMED
            TestPeds[0].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[0].PedcombatMove = 0
            TestPeds[0].pedcombatrange = 2
            TestPeds[0].combatcover = CA_USE_COVER
            TestPeds[0].bcombatcover = FALSE
            
            TestPeds[1].PedsCoords = <<4.59, -5.45, 7.36>>
            TestPeds[1].PedHeading = 359.6439
            TestPeds[1].Pedrelgrp = 1
            TestPeds[1].PedModel = S_M_Y_Cop_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_UNARMED
            TestPeds[1].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[1].PedcombatMove = 0
            TestPeds[1].pedcombatrange = 2
            TestPeds[1].combatcover = CA_USE_COVER
            TestPeds[1].bcombatcover = FALSE
            
            TestPeds[2].PedsCoords = << 8.09, -5.49, 7.36>> 
            TestPeds[2].PedHeading = 359.6439
            TestPeds[2].Pedrelgrp = 1
            TestPeds[2].PedModel = S_M_Y_Cop_01
            TestPeds[2].PedsWeapon = WEAPONTYPE_UNARMED
            TestPeds[2].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[2].PedcombatMove = 0
            TestPeds[2].pedcombatrange = 2
            TestPeds[2].combatcover = CA_USE_COVER
            TestPeds[2].bcombatcover = FALSE
            
            TestPeds[3].PedsCoords = <<13.40, -5.49, 7.36>>
            TestPeds[3].PedHeading = 359.6439
            TestPeds[3].Pedrelgrp = 1
            TestPeds[3].PedModel = S_M_Y_Cop_01
            TestPeds[3].PedsWeapon = WEAPONTYPE_UNARMED
            TestPeds[3].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[3].PedcombatMove = 0
            TestPeds[3].pedcombatrange = 2
            TestPeds[3].combatcover = CA_USE_COVER
            TestPeds[3].bcombatcover = FALSE

            TestCams[FixedCamera].cam_pos = <<-5.19, 16.72, 14>>
            TestCams[FixedCamera].cam_rot = <<-16.42, 0, -154.76>>
            TestCams[FixedCamera].bActivateCam = TRUE   
			
		BREAK
		
		CASE TEST_IS_PED_RESPONDING_TO_EVENT
			SETUP_COMBAT_PED_DATA ()
		BREAK
	
		CASE TEST_HAS_PED_RECEIVED_EVENT
		
			gbPlayerShouldBeAtCamPos = FALSE
			PlayerStartPos = <<13.96, 193.80, 6.35>>
				
		    TestPeds[0].PedsCoords = <<11.90, 172.52, 6.35>>
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_Cop_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            TestPeds[0].PedcombatMove = 0
            TestPeds[0].pedcombatrange = 2
            TestPeds[0].combatcover = CA_USE_COVER
            TestPeds[0].bcombatcover = TRUE
			
			TestVehicles [0].vehicleCoords = <<-33.68, 249.10, 6.35>>
    		TestVehicles [0].vehicleHeading = 90
  			TestVehicles [0].vehiclemodel = TAILGATER
			
		BREAK
		
		CASE SCENARIO_PED_MOVE_TO_COVER_PED_STAYS_STILL
           
		   	gbPlayerShouldBeAtCamPos = TRUE
			
			TestPeds[0].PedsCoords = <<-1.48, 93.43, 6.35>>
            TestPeds[0].PedHeading = 180
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_Cop_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[0].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[0].PedcombatMove = 0
            TestPeds[0].pedcombatrange = 2
            TestPeds[0].combatcover = CA_USE_COVER
            TestPeds[0].bcombatcover = FALSE
            
            TestPeds[1].PedsCoords = <<-1.91, 80.54, 6.35>>
            TestPeds[1].PedHeading = -180.0000
            TestPeds[1].Pedrelgrp = 2
            TestPeds[1].PedModel = S_M_M_ARMOURED_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[1].pedsstringweapon = "WEAPONTYPE_PISTOL"
            
            TestVehicles[0].vehicleCoords = <<-1.20, 84.55, 6.35>>
            TestVehicles[0].vehicleHeading = 90.0001
            TestVehicles[0].vehiclemodel = TAILGATER
			
			TestCams[FixedCamera].cam_pos = <<-18.172731,81.990250,11.204312>>
            TestCams[FixedCamera].cam_rot = <<-15.434171,0.000000,-85.945595>>
            TestCams[FixedCamera].bActivateCam = TRUE   

        BREAK
        
        CASE BugPedsNotShootingThroughFreindlyPed
            TestPeds[0].PedsCoords = << -1.4167, 0.0727, 7.36>>
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_Cop_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            TestPeds[0].PedcombatMove = 0
            TestPeds[0].pedcombatrange = 2
            TestPeds[0].combatcover = CA_USE_COVER
            TestPeds[0].bcombatcover = TRUE
            
            TestPeds[1].PedsCoords = << -1.2937, 0.9857, 7.36>>
            TestPeds[1].PedHeading = 5.0422
            TestPeds[1].Pedrelgrp = 1
            TestPeds[1].PedModel = S_M_Y_Cop_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            
            TestPeds[2].PedsCoords = << -1.0437, 8.4857, 7.36>>
            TestPeds[2].PedHeading = 181.0422
            TestPeds[2].Pedrelgrp = 2
            TestPeds[2].PedModel = S_M_Y_Cop_01
            TestPeds[2].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[2].PedcombatMove = 0
            TestPeds[2].combatcover = CA_USE_COVER
            TestPeds[2].bcombatcover = FALSE
            
            TestCams[FixedCamera].cam_pos = <<-10.2,6.3,4.3>>
            TestCams[FixedCamera].cam_rot = <<-11.27,  0.0, -98.75>>
            TestCams[FixedCamera].cam_fov = 65.0
            TestCams[FixedCamera].bActivateCam = TRUE   
        BREAK
        
        CASE SCENARIO_BOTH_PEDS_MOVE_TO_COVER
            TestPeds[0].PedsCoords = << -1.4167, 0.0727, 1.1959>> + gvMapOffset
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_Cop_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[0].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[0].PedcombatMove = 2
            TestPeds[0].pedcombatrange = 2
            TestPeds[0].combatcover = CA_USE_COVER
            TestPeds[0].bcombatcover = TRUE
            
            TestPeds[2].PedsCoords = << 0.0000, 20.2500, 1.1959>> + gvMapOffset
            TestPeds[2].PedHeading = 180.0000
            TestPeds[2].Pedrelgrp = 2
            TestPeds[2].PedModel = S_M_Y_Cop_01
            TestPeds[2].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[1].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[2].combatcover = CA_USE_COVER
            TestPeds[2].bcombatcover = TRUE
            
            TestVehicles [0 ].vehicleCoords = << -0.2515, 9.7511, 1.9393>> + gvMapOffset
            TestVehicles [0 ].vehicleHeading = 89.9995
            TestVehicles [0 ].vehiclemodel = FBI
            
            TestCams[FixedCamera].cam_pos = <<-19.069626,7.701386,6.704120>>+ gvMapOffset
            TestCams[FixedCamera].cam_rot = <<-19.276978,0.000002,-93.249466>>
            TestCams[FixedCamera].bActivateCam = TRUE   
        
        BREAK
        
        CASE SCENARIO_TWO_PEDS_NO_COVER
        
            TestPeds[0].PedsCoords = << -0.9167, -5.9273, 1.1959>> + gvMapOffset
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_Cop_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[0].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[0].PedcombatMove = 2       //will advance
            TestPeds[0].pedcombatrange = 0  //combat_range
            TestPeds[0].combatcover = CA_USE_COVER
            TestPeds[0].bcombatcover = FALSE
            
            TestPeds[2].PedsCoords = << -3.8539, 22.2401, 1.1959>> + gvMapOffset
            TestPeds[2].PedHeading = 178.7689
            TestPeds[2].Pedrelgrp = 2
            TestPeds[2].PedModel = S_M_Y_Cop_01
            TestPeds[2].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[2].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[2].PedcombatMove = 2
            TestPeds[2].pedcombatrange = 0
            TestPeds[2].combatcover = CA_USE_COVER
            TestPeds[2].bcombatcover = FALSE
            
            TestCams[FixedCamera].cam_pos = <<-19.069626,7.701386,6.704120>>+ gvMapOffset
            TestCams[FixedCamera].cam_rot = <<-19.276978,0.000002,-93.249466>>
            TestCams[FixedCamera].bActivateCam = TRUE   
            
        BREAK
        
        CASE ARifle_guy_in_cover_breaking
            TestPeds[0].PedsCoords = << -2.1667, -1.4273, 1.1959>> + gvMapOffset
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_Cop_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_UNARMED
            TestPeds[0].pedsstringweapon = "WEAPONTYPE_UNARMED"
            TestPeds[0].PedcombatMove = 2
            TestPeds[0].pedcombatrange = 2
            TestPeds[0].combatcover = CA_USE_COVER
            TestPeds[0].bcombatcover = FALSE
        
            TestPeds[1].PedsCoords = << -4.6667, -1.4273, 1.1959>> + gvMapOffset
            TestPeds[1].PedHeading = -0.0000
            TestPeds[1].Pedrelgrp = 1
            TestPeds[1].PedModel = S_M_Y_Cop_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[1].pedsstringweapon = "WEAPONTYPE_PISTOL"
            
            TestPeds[2].PedsCoords = << -2.5000, 19.5000, 1.1959>> + gvMapOffset
            TestPeds[2].PedHeading = 180.0000
            TestPeds[2].Pedrelgrp = 2
            TestPeds[2].PedModel = S_M_Y_Cop_01
            TestPeds[2].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            TestPeds[2].pedsstringweapon = "WEAPONTYPE_ASSAULTRIFLE"
            
            TestVehicles [0 ].vehicleCoords = << -3.2515, 17.2517, 1.9393>> + gvMapOffset
            TestVehicles [0 ].vehicleHeading = 90.0001
            TestVehicles [0 ].vehiclemodel = FBI
            
            TestCams[0].cam_pos = <<-27.408384,8.174817,9.370035>>+ gvMapOffset
            TestCams[0].cam_rot = <<-19.276978,0.000002,-93.249466>>
            TestCams[FixedCamera].bActivateCam = TRUE   
    
        BREAK
        
        CASE SquadOfPedsMovingBetweenVehicles
        CASE ScenarioCombatRanges   
            TestPeds[0].PedsCoords = << -2.1667, -8.4273, 1.1959>> + gvMapOffset
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_Cop_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[0].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[0].PedcombatMove = 1
            TestPeds[0].combatcover = CA_USE_COVER
            TestPeds[0].bcombatcover = TRUE
            
            TestPeds[1].PedsCoords = << -8.6667, -8.6773, 1.1959>> + gvMapOffset
            TestPeds[1].PedHeading = -0.0000
            TestPeds[1].Pedrelgrp = 1
            TestPeds[1].PedModel = S_M_Y_Cop_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[1].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[1].PedcombatMove = 1
            TestPeds[1].combatcover = CA_USE_COVER
            TestPeds[1].bcombatcover = TRUE
            
            
            TestPeds[2].PedsCoords = << -5.5000, 22.7500, 1.1959>> + gvMapOffset
            TestPeds[2].PedHeading = 180.0000
            TestPeds[2].Pedrelgrp = 2
            TestPeds[2].PedModel = S_M_Y_Cop_01
            TestPeds[2].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[2].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[2].PedActiveDefArea = TRUE
            TestPeds[2].PedDefCoord = <<-3.0, 21.25, 2.25>> 
            TestPeds[2].PedDefArea = 6.25
            TestPeds[2].PedcombatMove = 1
            TestPeds[2].combatcover = CA_USE_COVER
            TestPeds[2].bcombatcover = TRUE
            
            TestPeds[3].PedsCoords = << -3.7500, 22.5000, 1.1959>> + gvMapOffset
            TestPeds[3].PedHeading = 188.0000
            TestPeds[3].Pedrelgrp = 2
            TestPeds[3].PedModel = S_M_Y_Cop_01
            TestPeds[3].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[3].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[3].PedActiveDefArea = TRUE
            TestPeds[3].PedDefCoord = <<-3.0, 21.25, 2.25>> 
            TestPeds[3].PedDefArea = 6.25
            TestPeds[3].PedcombatMove = 1
            TestPeds[3].combatcover = CA_USE_COVER
            TestPeds[3].bcombatcover = TRUE
            
            TestPeds[4].PedsCoords = << -2.0000, 22.5000, 1.1959>> + gvMapOffset
            TestPeds[4].PedHeading = 178.0000
            TestPeds[4].Pedrelgrp = 2
            TestPeds[4].PedModel = S_M_Y_Cop_01
            TestPeds[4].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[4].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[4].PedActiveDefArea = TRUE
            TestPeds[4].PedDefCoord = <<-3.0, 21.25, 2.25>> 
            TestPeds[4].PedDefArea = 6.25
            TestPeds[4].PedcombatMove = 1
            TestPeds[4].combatcover = CA_USE_COVER
            TestPeds[4].bcombatcover = TRUE
            
            TestPeds[5].PedsCoords = << -0.7500, -6.2500, 1.1959>> + gvMapOffset
            TestPeds[5].PedHeading = -0.0000
            TestPeds[5].Pedrelgrp = 1
            TestPeds[5].PedModel = S_M_Y_Cop_01
            TestPeds[5].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[5].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[5].PedcombatMove = 1
            TestPeds[5].combatcover = CA_USE_COVER
            TestPeds[5].bcombatcover = TRUE
            
            TestVehicles [0 ].vehicleCoords = << -0.7516, 20.0020, 1.9393>> + gvMapOffset
            TestVehicles [0 ].vehicleHeading = 90.0001
            TestVehicles [0 ].vehiclemodel = FBI
            
            TestVehicles [1 ].vehicleCoords = << -5.5018, 20.2520, 1.9343>> + gvMapOffset
            TestVehicles [1 ].vehicleHeading = 268.0000
            TestVehicles [1 ].vehiclemodel = FBI
            
            TestVehicles [2 ].vehicleCoords = << 2.7483, -4.4986, 1.9392>> + gvMapOffset
            TestVehicles [2 ].vehicleHeading = 114.0000
            TestVehicles [2 ].vehiclemodel = FBI
            
            TestVehicles [3 ].vehicleCoords = << -8.7512, 4.5025, 1.9393>> + gvMapOffset
            TestVehicles [3 ].vehicleHeading = 246.0001
            TestVehicles [3 ].vehiclemodel = FBI
            
            TestVehicles [4 ].vehicleCoords = << -13.2511, -2.7475, 1.9393>> + gvMapOffset
            TestVehicles [4 ].vehicleHeading = 258.0002
            TestVehicles [4 ].vehiclemodel = FBI
            
            TestVehicles [5 ].vehicleCoords = << 0.4979, 8.7521, 1.9393>> + gvMapOffset
            TestVehicles [5 ].vehicleHeading = 284.0001
            TestVehicles [5 ].vehiclemodel = FBI
            
            TestCams[0].cam_pos = <<9.449883,-7.967893,12.322974>> + gvMapOffset
            TestCams[0].cam_rot = <<-38.123238,-0.000001,50.392761>>
            TestCams[FixedCamera].bActivateCam = TRUE   
            
            DefensiveSphereAreas[0].DefensiveAreaPos = <<-14.0, -1.25, 1.25 >> + gvMapOffset
            DefensiveSphereAreas[0].DefensiveAreaRadius = 4.25
            DefensiveSphereAreas[0].DefensiveAreaDirection = <<3.25, -3.25, 1.25 >> + gvMapOffset
            DefensiveSphereAreas[1].DefensiveAreaPos = <<3.25, -3.25, 1.25 >>
            DefensiveSphereAreas[1].DefensiveAreaRadius = 4.25
            DefensiveSphereAreas[1].DefensiveAreaDirection = <<-14.0, -1.25, 1.25 >> + gvMapOffset
            DefensiveSphereAreas[2].DefensiveAreaPos = <<-8.75, 4.0, 1.25>>
            DefensiveSphereAreas[2].DefensiveAreaRadius = 4.25
            DefensiveSphereAreas[2].DefensiveAreaDirection = <<0.75, 8.5, 1.25 >> + gvMapOffset
            DefensiveSphereAreas[3].DefensiveAreaPos = <<0.75, 8.5, 1.25 >>
            DefensiveSphereAreas[3].DefensiveAreaRadius = 4.25
            DefensiveSphereAreas[3].DefensiveAreaDirection = <<-8.75, 4.0, 1.25>> + gvMapOffset
        
            MAX_NUMBER_DEFENSIVE_AREA = 4
        BREAK
                
        CASE ScenarioTaskStayInCover
            TestPeds[0].PedsCoords = << -2.1667, -8.4273, 1.1959>>  + gvMapOffset
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[0].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[0].PedcombatMove = 1
            TestPeds[0].combatcover = CA_USE_COVER
            TestPeds[0].bcombatcover = TRUE
            
            TestPeds[1].PedsCoords = << -8.6667, -8.6773, 1.1959>> + gvMapOffset
            TestPeds[1].PedHeading = -0.0000
            TestPeds[1].Pedrelgrp = 1
            TestPeds[1].PedModel = S_M_Y_COP_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[1].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[1].PedcombatMove = 1
            TestPeds[1].combatcover = CA_USE_COVER
            TestPeds[1].bcombatcover = TRUE
            
            
            TestPeds[2].PedsCoords = << -5.5000, 22.7500, 1.1959>> + gvMapOffset
            TestPeds[2].PedHeading = 180.0000
            TestPeds[2].Pedrelgrp = 2
            TestPeds[2].PedModel = S_M_Y_COP_01
            TestPeds[2].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[2].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[2].PedActiveDefArea = TRUE
            TestPeds[2].PedDefCoord = <<-3.0, 21.25, 2.25>> 
            TestPeds[2].PedDefArea = 6.25
            TestPeds[2].PedcombatMove = 1
            TestPeds[2].combatcover = CA_USE_COVER
            TestPeds[2].bcombatcover = TRUE
            
            TestPeds[3].PedsCoords = << -3.7500, 22.5000, 1.1959>> + gvMapOffset
            TestPeds[3].PedHeading = 188.0000
            TestPeds[3].Pedrelgrp = 2
            TestPeds[3].PedModel = S_M_Y_COP_01
            TestPeds[3].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[3].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[3].PedActiveDefArea = TRUE
            TestPeds[3].PedDefCoord = <<-3.0, 21.25, 2.25>>   + gvMapOffset
            TestPeds[3].PedDefArea = 6.25
            TestPeds[3].PedcombatMove = 1
            TestPeds[3].combatcover = CA_USE_COVER
            TestPeds[3].bcombatcover = TRUE
            
            TestPeds[4].PedsCoords = << -2.0000, 22.5000, 1.1959>> + gvMapOffset
            TestPeds[4].PedHeading = 178.0000
            TestPeds[4].Pedrelgrp = 2
            TestPeds[4].PedModel = S_M_Y_COP_01
            TestPeds[4].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[4].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[4].PedActiveDefArea = TRUE
            TestPeds[4].PedDefCoord = <<-3.0, 21.25, 2.25>>  + gvMapOffset
            TestPeds[4].PedDefArea = 6.25
            TestPeds[4].PedcombatMove = 1
            TestPeds[4].combatcover = CA_USE_COVER
            TestPeds[4].bcombatcover = TRUE
            
            TestPeds[5].PedsCoords = << -0.7500, -6.2500, 1.1959>> + gvMapOffset
            TestPeds[5].PedHeading = -0.0000
            TestPeds[5].Pedrelgrp = 1
            TestPeds[5].PedModel = S_M_Y_COP_01
            TestPeds[5].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[5].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[5].PedcombatMove = 1
            TestPeds[5].combatcover = CA_USE_COVER
            TestPeds[5].bcombatcover = TRUE
            
            TestVehicles [0 ].vehicleCoords = << -0.7516, 20.0020, 1.9393>> + gvMapOffset
            TestVehicles [0 ].vehicleHeading = 90.0001
            TestVehicles [0 ].vehiclemodel = FBI
            
            TestVehicles [1 ].vehicleCoords = << -5.5018, 20.2520, 1.9343>> + gvMapOffset
            TestVehicles [1 ].vehicleHeading = 268.0000
            TestVehicles [1 ].vehiclemodel = FBI
            
            TestVehicles [2 ].vehicleCoords = << 2.7483, -4.4986, 1.9392>> + gvMapOffset
            TestVehicles [2 ].vehicleHeading = 114.0000
            TestVehicles [2 ].vehiclemodel = FBI
            
            TestVehicles [3 ].vehicleCoords = << -8.7512, 4.5025, 1.9393>> + gvMapOffset
            TestVehicles [3 ].vehicleHeading = 246.0001
            TestVehicles [3 ].vehiclemodel = FBI
            
            TestVehicles [4 ].vehicleCoords = << -13.2511, -2.7475, 1.9393>> + gvMapOffset
            TestVehicles [4 ].vehicleHeading = 258.0002
            TestVehicles [4 ].vehiclemodel = FBI
            
            TestVehicles [5 ].vehicleCoords = << 0.4979, 8.7521, 1.9393>> + gvMapOffset
            TestVehicles [5 ].vehicleHeading = 284.0001
            TestVehicles [5 ].vehiclemodel = FBI
            
            TestCams[0].cam_pos = <<9.449883,-7.967893,12.322974>>+ gvMapOffset
            TestCams[0].cam_rot = <<-38.123238,-0.000001,50.392761>>
            TestCams[FixedCamera].bActivateCam = TRUE   
            
            DefensiveSphereAreas[0].DefensiveAreaPos = <<-14.0, -1.25, 1.25 >>+ gvMapOffset
            DefensiveSphereAreas[0].DefensiveAreaRadius = 4.25
            DefensiveSphereAreas[0].DefensiveAreaDirection = <<3.25, -3.25, 1.25 >>+ gvMapOffset
            DefensiveSphereAreas[1].DefensiveAreaPos = <<3.25, -3.25, 1.25 >>
            DefensiveSphereAreas[1].DefensiveAreaRadius = 4.25
            DefensiveSphereAreas[1].DefensiveAreaDirection = <<-14.0, -1.25, 1.25 >>+ gvMapOffset
            DefensiveSphereAreas[2].DefensiveAreaPos = <<-8.75, 4.0, 1.25>>
            DefensiveSphereAreas[2].DefensiveAreaRadius = 4.25
            DefensiveSphereAreas[2].DefensiveAreaDirection = <<0.75, 8.5, 1.25 >>+ gvMapOffset
            DefensiveSphereAreas[3].DefensiveAreaPos = <<0.75, 8.5, 1.25 >>
            DefensiveSphereAreas[3].DefensiveAreaRadius = 4.25
            DefensiveSphereAreas[3].DefensiveAreaDirection = <<-8.75, 4.0, 1.25>>+ gvMapOffset
        
            MAX_NUMBER_DEFENSIVE_AREA = 4
        BREAK
        
        CASE BugAiCantDriveTank
            TestPeds[0].PedsCoords = << 0.9833, -15.0394, 1.1959>> + gvMapOffset
            TestPeds[0].PedHeading = -0.0000
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_UNARMED
            TestPeds[0].pedsstringweapon = "WEAPONTYPE_UNARMED"
            TestPeds[0].PedDefCoord = << -4.4804, -10.3374, 2.0797>>  + gvMapOffset
            TestPeds[0].PedDefArea = 5.0000
            TestPeds[0].PedActiveDefArea = TRUE

//          TestPeds[1].PedsCoords = << 20.9833, -15.0394, 1.1959>> 
//          TestPeds[1].PedHeading = -0.0000
//          TestPeds[1].Pedrelgrp = 2
//          TestPeds[1].PedModel = S_M_Y_COP_01
//          TestPeds[1].PedsWeapon = WEAPONTYPE_UNARMED
//          TestPeds[1].pedsstringweapon = "WEAPONTYPE_UNARMED"
//          TestPeds[1].PedDefCoord = << -4.4804, -10.3374, 2.0797>> 
//          TestPeds[1].PedDefArea = 5.0000
//          TestPeds[1].PedActiveDefArea = TRUE

            TestVehicles [5].vehicleCoords = << -5.0020, -9.9980, 1.9393>> + gvMapOffset
            TestVehicles [5].vehicleHeading = 90.0
            TestVehicles [5].vehiclemodel = FIRETRUK
        
            TestCams[0].cam_pos = <<-27.408384,8.174817,9.370035>>+ gvMapOffset
            TestCams[0].cam_rot = <<-19.276978,0.000002,-93.249466>>
            TestCams[FixedCamera].bActivateCam = TRUE   
        
        BREAK
            
        CASE SquadStaysDefensiveBehindVehicle
            TestPeds[0].PedsCoords = << 0.9833, -15.0394, 1.1959>> + gvMapOffset
            TestPeds[0].PedHeading = -0.0000
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_UNARMED
            TestPeds[0].pedsstringweapon = "WEAPONTYPE_UNARMED"
            TestPeds[0].PedDefCoord = << -4.4804, -10.3374, 2.0797>> + gvMapOffset
            TestPeds[0].PedDefArea = 5.0000
            TestPeds[0].PedActiveDefArea = TRUE
            
            
            TestPeds[1].PedsCoords = << -7.9167, -15.6773, 1.1959>> + gvMapOffset
            TestPeds[1].PedHeading = -0.0000
            TestPeds[1].Pedrelgrp = 1
            TestPeds[1].PedModel = S_M_Y_COP_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[1].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[1].PedDefCoord = << 3.2500, -3.2500, 1.2500>> + gvMapOffset
            TestPeds[1].PedDefArea = 4.2500
            TestPeds[1].PedActiveDefArea = TRUE
            TestPeds[1].PedcombatMove = 1
            TestPeds[1].combatcover = CA_USE_COVER
            TestPeds[1].bcombatcover = TRUE
            
            TestPeds[2].PedsCoords = << -5.5000, 22.7500, 1.1959>> + gvMapOffset
            TestPeds[2].PedHeading = 180.0000
            TestPeds[2].Pedrelgrp = 2
            TestPeds[2].PedModel = S_M_Y_COP_01
            TestPeds[2].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[2].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[2].PedDefCoord = << -3.0000, 21.2500, 2.2500>> + gvMapOffset
            TestPeds[2].PedDefArea = 6.2500
            TestPeds[2].PedActiveDefArea = TRUE
            TestPeds[2].PedcombatMove = 1
            TestPeds[2].combatcover = CA_USE_COVER
            TestPeds[2].bcombatcover = TRUE
            
            TestPeds[3].PedsCoords = << -3.7500, 22.5000, 1.1959>> + gvMapOffset
            TestPeds[3].PedHeading = 188.0000
            TestPeds[3].Pedrelgrp = 2
            TestPeds[3].PedModel = S_M_Y_COP_01
            TestPeds[3].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[3].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[3].PedDefCoord = << -3.0000, 21.2500, 2.2500>> + gvMapOffset
            TestPeds[3].PedDefArea = 6.2500
            TestPeds[3].PedActiveDefArea = TRUE
            TestPeds[3].PedcombatMove = 1
            TestPeds[3].combatcover = CA_USE_COVER
            TestPeds[3].bcombatcover = TRUE
            
            TestPeds[4].PedsCoords = << -2.0000, 22.5000, 1.1959>> + gvMapOffset
            TestPeds[4].PedHeading = 178.0000
            TestPeds[4].Pedrelgrp = 2
            TestPeds[4].PedModel = S_M_Y_COP_01
            TestPeds[4].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[4].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[4].PedDefCoord = << -3.0000, 21.2500, 2.2500>> + gvMapOffset
            TestPeds[4].PedDefArea = 6.2500
            TestPeds[4].PedActiveDefArea = TRUE
            TestPeds[4].PedcombatMove = 1
            TestPeds[4].combatcover = CA_USE_COVER
            TestPeds[4].bcombatcover = TRUE
            
            TestPeds[5].PedsCoords = << -2.2500, -15.7500, 1.1959>> + gvMapOffset
            TestPeds[5].PedHeading = -0.0000
            TestPeds[5].Pedrelgrp = 1
            TestPeds[5].PedModel = S_M_Y_COP_01
            TestPeds[5].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[5].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[5].PedDefCoord = << 3.2500, -3.2500, 1.2500>> + gvMapOffset
            TestPeds[5].PedDefArea = 4.2500
            TestPeds[5].PedActiveDefArea = TRUE
            TestPeds[5].PedcombatMove = 1
            TestPeds[5].combatcover = CA_USE_COVER
            TestPeds[5].bcombatcover = TRUE
            
            TestVehicles [0 ].vehicleCoords = << -0.5532, 19.9923, 1.9328>> + gvMapOffset
            TestVehicles [0 ].vehicleHeading = 90.1937
            TestVehicles [0 ].vehiclemodel = FBI
            
            TestVehicles [1 ].vehicleCoords = << -5.7134, 20.2718, 1.9398>> + gvMapOffset
            TestVehicles [1 ].vehicleHeading = 268.5231
            TestVehicles [1 ].vehiclemodel = FBI
            
            TestVehicles [5].vehicleCoords = << -5.0020, -9.9980, 1.9393>> + gvMapOffset
            TestVehicles [5].vehicleHeading = 360.0001
            TestVehicles [5].vehiclemodel = FBI
    
            TestCams[0].cam_pos = <<-27.408384,8.174817,9.370035>>+ gvMapOffset
            TestCams[0].cam_rot = <<-19.276978,0.000002,-93.249466>>
            TestCams[FixedCamera].bActivateCam = TRUE   
            
        BREAK
        
        CASE SquadDefendingPos
    
            
            TestPeds[1].PedsCoords = << -12.0000, 7.7500, 1.1959>> + gvMapOffset
            TestPeds[1].PedHeading = 352.0000
            TestPeds[1].Pedrelgrp = 1
            TestPeds[1].PedModel = S_M_Y_COP_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            TestPeds[1].pedsstringweapon = "WEAPONTYPE_ASSAULTRIFLE"
            TestPeds[1].PedDefCoord = << -7.0000, 8.2500, 1.2500>> + gvMapOffset
            TestPeds[1].PedDefArea = 11.7500
            TestPeds[1].PedActiveDefArea = TRUE
            TestPeds[1].PedcombatMove = 1
            TestPeds[1].combatcover = CA_USE_COVER
            TestPeds[1].bcombatcover = TRUE

            TestPeds[2].PedsCoords = << -8.7500, 7.7500, 1.1959>> + gvMapOffset
            TestPeds[2].PedHeading = -0.0000
            TestPeds[2].Pedrelgrp = 1
            TestPeds[2].PedModel = S_M_Y_COP_01
            TestPeds[2].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            TestPeds[2].pedsstringweapon = "WEAPONTYPE_ASSAULTRIFLE"
            TestPeds[2].PedDefCoord = << -7.0000, 8.2500, 1.2500>> + gvMapOffset
            TestPeds[2].PedDefArea = 11.7500
            TestPeds[2].PedActiveDefArea = TRUE
            TestPeds[2].PedcombatMove = 1
            TestPeds[2].combatcover = CA_USE_COVER
            TestPeds[2].bcombatcover = TRUE
            
            TestPeds[3].PedsCoords = << -6.0000, 7.7500, 1.1959>> + gvMapOffset
            TestPeds[3].PedHeading = -0.0000
            TestPeds[3].Pedrelgrp = 1
            TestPeds[3].PedModel = S_M_Y_COP_01
            TestPeds[3].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            TestPeds[3].pedsstringweapon = "WEAPONTYPE_ASSAULTRIFLE"
            TestPeds[3].PedDefCoord = << -7.0000, 8.2500, 1.2500>> + gvMapOffset
            TestPeds[3].PedDefArea = 11.7500
            TestPeds[3].PedActiveDefArea = TRUE
            TestPeds[3].PedcombatMove = 1
            TestPeds[3].combatcover = CA_USE_COVER
            TestPeds[3].bcombatcover = TRUE
            
            TestPeds[4].PedsCoords = << -2.5000, 7.7500, 1.1959>> + gvMapOffset
            TestPeds[4].PedHeading = -0.0000
            TestPeds[4].Pedrelgrp = 1
            TestPeds[4].PedModel = S_M_Y_COP_01
            TestPeds[4].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            TestPeds[4].pedsstringweapon = "WEAPONTYPE_ASSAULTRIFLE"
            TestPeds[4].PedDefCoord = << -7.0000, 8.2500, 1.2500>> + gvMapOffset
            TestPeds[4].PedDefArea = 11.7500
            TestPeds[4].PedActiveDefArea = TRUE
            TestPeds[4].PedcombatMove = 1
            TestPeds[4].combatcover = CA_USE_COVER
            TestPeds[4].bcombatcover = TRUE
            
            TestPeds[5].PedsCoords = << -2.7500, 31.0000, 1.1959>> + gvMapOffset
            TestPeds[5].PedHeading = 176.0000
            TestPeds[5].Pedrelgrp = 2
            TestPeds[5].PedModel = S_M_Y_COP_01
            TestPeds[5].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            TestPeds[5].pedsstringweapon = "WEAPONTYPE_ASSAULTRIFLE"
            
            TestPeds[6].PedsCoords = << -6.0000, 31.0000, 1.1959>> + gvMapOffset
            TestPeds[6].PedHeading = 192.0000
            TestPeds[6].Pedrelgrp = 2
            TestPeds[6].PedModel = S_M_Y_COP_01
            TestPeds[6].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            TestPeds[6].pedsstringweapon = "WEAPONTYPE_ASSAULTRIFLE"
            
            TestVehicles [0].vehicleCoords = << -4.7522, 2.7254, 1.9343>> + gvMapOffset
            TestVehicles [0 ].vehicleHeading = -92.0000
            TestVehicles [0 ].vehiclemodel = FBI
            
            TestVehicles [1].vehicleCoords = << -11.0024, 2.9754, 1.9343>> + gvMapOffset
            TestVehicles [1 ].vehicleHeading = 86.0000
            TestVehicles [1 ].vehiclemodel = FBI
            
            TestVehicles [2].vehicleCoords = << -10.7526, 15.7254, 1.9343>> + gvMapOffset
            TestVehicles [2 ].vehicleHeading = -92.0000
            TestVehicles [2 ].vehiclemodel = FBI
            
            TestVehicles [3].vehicleCoords = << -3.7528, 15.7254, 1.9343>> + gvMapOffset
            TestVehicles [3 ].vehicleHeading = 86.0000
            TestVehicles [3 ].vehiclemodel = FBI
            
            TestVehicles [4].vehicleCoords = << -0.2531, 28.4757, 1.9343>> + gvMapOffset
            TestVehicles [4 ].vehicleHeading = 68.0000
            TestVehicles [4 ].vehiclemodel = FBI
            
            TestVehicles [5].vehicleCoords = << -8.7532, 27.9757, 1.9343>> + gvMapOffset
            TestVehicles [5 ].vehicleHeading = -78.0000
            TestVehicles [5 ].vehiclemodel = FBI
            
            TestVehicles [6].vehicleCoords = << -14.7534, -11.0243, 1.9343>> + gvMapOffset
            TestVehicles [6 ].vehicleHeading = -104.0000
            TestVehicles [6 ].vehiclemodel = FBI
            
            TestVehicles [7].vehicleCoords = << 0.9964, -11.5243, 1.9343>> + gvMapOffset
            TestVehicles [7].vehicleHeading = -76.0000
            TestVehicles [7].vehiclemodel = FBI
            
            TestCams[0].cam_pos = <<-34.791626,9.048415,15.266695>>+ gvMapOffset
            TestCams[0].cam_rot = <<-26.172144,0.000001,-91.914223>>
            TestCams[FixedCamera].bActivateCam = TRUE   
            
        BREAK
        
        CASE PedDoesWeirdThingsSeekingCover
            TestPeds[0].PedsCoords = << -2.4167, -1.4273, 1.1959>> + gvMapOffset
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].Pedrelgrp = 1
            TestPeds[0].PedModel = S_M_Y_COP_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[0].pedsstringweapon = "WEAPONTYPE_PISTOL"
            TestPeds[0].PedcombatMove = 0               //CM_STATIONARY
            TestPeds[0].pedcombatrange = 2          //CR_FAR
            TestPeds[0].combatcover = CA_USE_COVER                
            TestPeds[0].bcombatcover = FALSE        //WONT USE COVER
            
            
            TestPeds[2].PedsCoords = << 0.0000, 20.2500, 1.1959>> + gvMapOffset
            TestPeds[2].PedHeading = 180.0000
            TestPeds[2].Pedrelgrp = 2
            TestPeds[2].PedModel = S_M_Y_COP_01
            TestPeds[2].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[2].pedsstringweapon = "WEAPONTYPE_PISTOL"
            
            TestVehicles [0 ].vehicleCoords = << -0.0012, 10.0009, 1.9393>> + gvMapOffset
            TestVehicles [0 ].vehicleHeading = 90.0001
            TestVehicles [0 ].vehiclemodel = FBI
            
            TestCams[0].cam_pos = <<-27.408384,8.174817,9.370035>>+ gvMapOffset
            TestCams[0].cam_rot = <<-19.276978,0.000002,-93.249466>>    
            TestCams[FixedCamera].bActivateCam = TRUE   
            
        BREAK
        
        CASE TestPedsUsingRandomPaths
            TestPeds[1].PedsCoords = << -12.2500, -7.7500, 1.1959>> + gvMapOffset
            TestPeds[1].PedHeading = 352.0000
            TestPeds[1].Pedrelgrp = 1
            TestPeds[1].PedModel = S_M_Y_COP_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            TestPeds[1].PedDefCoord = << -11.0000, -4.5000, 1.5000>> + gvMapOffset
            TestPeds[1].PedDefArea = -5.2500
            TestPeds[1].PedActiveDefArea = TRUE
            TestPeds[1].PedcombatMove = 1
            TestPeds[1].combatcover = CA_USE_COVER
            TestPeds[1].bcombatcover = TRUE
            
            TestPeds[2].PedsCoords = << -8.7500, -9.0000, 1.1959>> + gvMapOffset
            TestPeds[2].PedHeading = -0.0000
            TestPeds[2].Pedrelgrp = 1
            TestPeds[2].PedModel = S_M_Y_COP_01
            TestPeds[2].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            TestPeds[2].PedDefCoord = << -6.2500, 23.7500, 1.2500>> + gvMapOffset
            TestPeds[2].PedDefArea = 7.7500
            TestPeds[2].PedActiveDefArea = TRUE
            TestPeds[2].PedcombatMove = 1
            TestPeds[2].combatcover = CA_USE_COVER
            TestPeds[2].bcombatcover = TRUE
            
            TestPeds[3].PedsCoords = << -6.0000, -9.0000, 1.1959>> + gvMapOffset
            TestPeds[3].PedHeading = -0.0000
            TestPeds[3].Pedrelgrp = 1
            TestPeds[3].PedModel = S_M_Y_COP_01
            TestPeds[3].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            TestPeds[3].PedDefCoord = << -7.0000, 8.2500, 1.2500>> + gvMapOffset
            TestPeds[3].PedDefArea = 11.7500
            TestPeds[3].PedActiveDefArea = TRUE
            TestPeds[3].PedcombatMove = 1
            TestPeds[3].combatcover = CA_USE_COVER
            TestPeds[3].bcombatcover = TRUE
            
            TestPeds[4].PedsCoords = << -2.5000, -9.7500, 1.1959>> + gvMapOffset
            TestPeds[4].PedHeading = -0.0000
            TestPeds[4].Pedrelgrp = 1
            TestPeds[4].PedModel = S_M_Y_COP_01
            TestPeds[4].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            TestPeds[4].PedDefCoord = << -7.0000, 8.2500, 1.2500>> + gvMapOffset
            TestPeds[4].PedDefArea = 11.7500
            TestPeds[4].PedActiveDefArea = TRUE
            TestPeds[4].PedcombatMove = 1
            TestPeds[4].combatcover = CA_USE_COVER
            TestPeds[4].bcombatcover = TRUE
            
            TestPeds[5].PedsCoords = << -9.0108, 10.7228, 1.1959>> + gvMapOffset
            TestPeds[5].PedHeading = 176.0000
            TestPeds[5].Pedrelgrp = 2
            TestPeds[5].PedModel = S_M_Y_COP_01
            TestPeds[5].PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            
            TestVehicles [0].vehicleCoords = << -7.6633, 10.1550, 1.9750>> + gvMapOffset
            TestVehicles [0].vehicleHeading = 182.5181
            TestVehicles [0].vehiclemodel = FBI
            
            TestVehicles [1].vehicleCoords = << -11.5364, 8.4755, 1.6070>> + gvMapOffset
            TestVehicles [1].vehicleHeading = 86.5097
            TestVehicles [1].vehiclemodel = FBI
            
            TestVehicles [2].vehicleCoords = << -12.5460, 11.9288, 1.6016>> + gvMapOffset
            TestVehicles [2].vehicleHeading = 172.5550
            TestVehicles [2].vehiclemodel = FBI
            
            TestVehicles [3].vehicleCoords = << -8.9682, 13.2184, 1.6011>> + gvMapOffset
            TestVehicles [3].vehicleHeading = 86.2281
            TestVehicles [3].vehiclemodel = FBI
            
            TestVehicles [4].vehicleCoords = << -4.2551, 23.7287, 1.6070>> + gvMapOffset
            TestVehicles [4].vehicleHeading = 92.0432
            TestVehicles [4].vehiclemodel = FBI
            
            TestVehicles [5].vehicleCoords = << -9.7513, 23.2251, 1.6070>> + gvMapOffset
            TestVehicles [5].vehicleHeading = 272.0267
            TestVehicles [5].vehiclemodel = FBI
            
            TestVehicles [6].vehicleCoords = << -12.8695, -3.6767, 1.6081>> + gvMapOffset
            TestVehicles [6].vehicleHeading = 250.7110
            TestVehicles [6].vehiclemodel = FBI
            
            TestVehicles [7].vehicleCoords = << -8.0016, -5.7747, 1.6020>> + gvMapOffset
            TestVehicles [7].vehicleHeading = 242.0266
            TestVehicles [7].vehiclemodel = FBI
            
            TestVehicles [8].vehicleCoords = << -16.7516, 12.2253, 1.6070>> + gvMapOffset
            TestVehicles [8].vehicleHeading = 252.0001
            TestVehicles [8].vehiclemodel = FBI
            
            TestVehicles [9].vehicleCoords = << -2.7516, 10.7253, 1.6020>> + gvMapOffset
            TestVehicles [9].vehicleHeading = -92.0000
            TestVehicles [9].vehiclemodel = FBI
            
            TestCams[0].Cam_Pos = <<16.756636,5.160227,10.248656>>+ gvMapOffset
            TestCams[0].Cam_Rot =<<-17.394821,0.000026,81.399506>> 
            TestCams[0].Cam_Fov = 60.0000
            TestCams[0].bActivateCam = TRUE
            
            DefensiveSphereAreas[0].DefensiveAreaPos = <<-10.25, -4.0, 2.0 >>+ gvMapOffset
            DefensiveSphereAreas[0].DefensiveAreaRadius = 7.5
            DefensiveSphereAreas[1].DefensiveAreaPos = <<-6.5, 25.25, 1.75 >>+ gvMapOffset
            DefensiveSphereAreas[1].DefensiveAreaRadius = 7.75      
            mAX_NUMBER_DEFENSIVE_AREA = 2
        BREAK
        
        CASE TargetDummy
            TestPeds[0].PedsCoords = << -2.4167, -1.4273, 1.1959>> + gvMapOffset
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].Pedrelgrp = 0
            TestPeds[0].PedModel = S_M_Y_COP_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
            TestPeds[0].pedsstringweapon = "WEAPONTYPE_PISTOL"
        BREAK
		
		CASE SWAT_VAN_DRIVE_BY
			
			gbPlayerShouldBeAtCamPos = FALSE
			PlayerStartPos = <<-25.11, 79.15, 6.35>>
			
			TestPeds[0].PedsCoords = <<-24.79, 74.05, 6.35>>
            TestPeds[0].PedHeading = 359.6439
            TestPeds[0].Pedrelgrp = 0
            TestPeds[0].PedModel = S_M_Y_SWAT_01
            TestPeds[0].PedsWeapon = WEAPONTYPE_CARBINERIFLE
            TestPeds[0].pedsstringweapon = "WEAPONTYPE_CARBINERIFLE"
			
	        TestPeds[1].PedsCoords = <<-24.63, 74.11, 6.35>>
            TestPeds[1].PedHeading = 359.6439
            TestPeds[1].Pedrelgrp = 0
            TestPeds[1].PedModel = S_M_Y_SWAT_01
            TestPeds[1].PedsWeapon = WEAPONTYPE_CARBINERIFLE
            TestPeds[1].pedsstringweapon = "WEAPONTYPE_CARBINERIFLE"
			
			TestPeds[2].PedsCoords = <<-27.65, 83.66, 6.35>>
            TestPeds[2].PedHeading = 359.6439
            TestPeds[2].Pedrelgrp = 0
            TestPeds[2].PedModel = S_M_Y_SWAT_01
            TestPeds[2].PedsWeapon = WEAPONTYPE_CARBINERIFLE
            TestPeds[2].pedsstringweapon = "WEAPONTYPE_CARBINERIFLE"
			
			TestPeds[3].PedsCoords = <<-32.36, 85.47, 6.35>>
            TestPeds[3].PedHeading = 359.6439
            TestPeds[3].Pedrelgrp = 0
            TestPeds[3].PedModel = S_M_Y_SWAT_01
            TestPeds[3].PedsWeapon = WEAPONTYPE_CARBINERIFLE
            TestPeds[3].pedsstringweapon = "WEAPONTYPE_CARBINERIFLE"
			
            TestVehicles[0].vehicleCoords = <<-30.10, 78.62, 6.35>>
            TestVehicles[0].vehicleHeading = 182.5181
            TestVehicles[0].vehiclemodel = RIOT
		BREAK
		
		CASE TASK_RAPPEL_DOWN_WALL_PlayerTest
			
			gbPlayerShouldBeAtCamPos = FALSE
			PlayerStartPos = <<-1.9, 132.77, 27.36>>
			
		BREAK
		
		CASE TASK_RAPPEL_DOWN_WALL_AITest
			
			gbPlayerShouldBeAtCamPos = FALSE
			PlayerStartPos = <<-1.9, 132.77, 27.36>>
			
			TestPeds[0].PedsCoords = <<-1.9, 126.23, 27.36>>
            TestPeds[0].PedHeading = 270
            TestPeds[0].Pedrelgrp = 0
            TestPeds[0].PedModel = S_M_Y_SWAT_01
			
			TestCams[FixedCamera].cam_pos = <<8.91, 116.65, 30.32>>
		    TestCams[FixedCamera].cam_rot = <<-36.26, -0.00, 25.74>>
		    TestCams[FixedCamera].cam_fov = 65.0
		    TestCams[FixedCamera].bActivateCam = TRUE  
			
		BREAK
		
		CASE TEST_TASK_MELEE_COMBAT_ONE 
			
			TestPeds[0].PedsCoords = <<-4.11, 72.40, 6.35>>
            TestPeds[0].PedHeading = 270
            TestPeds[0].Pedrelgrp = 0
            TestPeds[0].PedModel = S_M_M_GAFFER_01
			
		BREAK
		
		CASE TEST_TASK_MELEE_COMBAT_MANY 
			
			TestPeds[0].PedsCoords = <<-4.11, 72.40, 6.35>>
            TestPeds[0].PedHeading = 270
            TestPeds[0].Pedrelgrp = 0
            TestPeds[0].PedModel = S_M_M_GAFFER_01
			
			TestPeds[1].PedsCoords = <<-4.11, 75.40, 6.35>>
            TestPeds[1].PedHeading = 270
            TestPeds[1].Pedrelgrp = 0
            TestPeds[1].PedModel = S_M_M_BOUNCER_01
			
			TestPeds[2].PedsCoords = <<-7.77, 73.23, 6.35>>
            TestPeds[2].PedHeading = 270
            TestPeds[2].Pedrelgrp = 0
            TestPeds[2].PedModel = S_M_M_DOCKWORK_01
			
			TestPeds[3].PedsCoords = <<-7.77, 78.23, 6.35>>
            TestPeds[3].PedHeading = 270
            TestPeds[3].Pedrelgrp = 0
            TestPeds[3].PedModel = S_M_M_GAFFER_01
			
			TestPeds[4].PedsCoords = <<-10.77, 73.23, 6.35>>
            TestPeds[4].PedHeading = 270
            TestPeds[4].Pedrelgrp = 0
            TestPeds[4].PedModel = S_M_M_BOUNCER_01
			
			TestPeds[5].PedsCoords = <<-10.77, 77.23, 6.35>>
            TestPeds[5].PedHeading = 270
            TestPeds[5].Pedrelgrp = 0
            TestPeds[5].PedModel = S_M_M_DOCKWORK_01
			
		BREAK
		
		CASE TEST_TASK_DRAG_INJURED_PED
		
			gbPlayerShouldBeAtCamPos = FALSE
			PlayerStartPos = <<-3.43, 74.00, 7.36>>
		
			TestPeds[0].PedsCoords = <<-3.26, 77.88, 7.36>>
            TestPeds[0].PedHeading = 180
            TestPeds[0].Pedrelgrp = 0
            TestPeds[0].PedModel = S_M_Y_Cop_01
			
			TestPeds[1].PedsCoords = <<-2.81, 93.63, 7.36>>
            TestPeds[1].PedHeading = 180
            TestPeds[1].Pedrelgrp = 0
            TestPeds[1].PedModel = S_M_Y_Cop_01
		
		BREAK
		
		CASE TEST_TASK_SHOOT_AT_PLAYER
        	
			TestPeds[0].PedsCoords = <<-4.11, 72.40, 6.35>>
            TestPeds[0].PedHeading = 270
            TestPeds[0].Pedrelgrp = 0
            TestPeds[0].PedModel = S_M_M_GAFFER_01
			
		BREAK
		
		CASE TEST_TASK_SHOOT_AT_VEHICLE
			
			TestPeds[0].PedsCoords = <<-4.11, 72.40, 6.35>>
            TestPeds[0].PedHeading = 270
            TestPeds[0].Pedrelgrp = 0
            TestPeds[0].PedModel = S_M_M_GAFFER_01
			
			TestVehicles[0].vehicleCoords = <<-3.7,78.9,6.35>>
            TestVehicles[0].vehicleHeading = 180.0
            TestVehicles[0].vehiclemodel = TAILGATER
            
		BREAK
		
		CASE TEST_TASK_AI_STEALTH_KILL
			
			TestPeds[0].PedsCoords = <<-4.11, 72.40, 6.35>>
            TestPeds[0].PedHeading = 270
            TestPeds[0].Pedrelgrp = 0
            TestPeds[0].PedModel = S_M_M_GAFFER_01
			
			TestPeds[1].PedsCoords = <<2.1, 72.8, 7.4>>
            TestPeds[1].PedHeading = 270
            TestPeds[1].Pedrelgrp = 0
            TestPeds[1].PedModel = S_M_M_DOCKWORK_01
			
		BREAK
		
		CASE TEST_TASK_ANIMAL_MELEE
			
			TestPeds[0].PedsCoords = <<-4.11, 72.40, 6.35>>
            TestPeds[0].PedHeading = 270
            TestPeds[0].Pedrelgrp = 0
            TestPeds[0].PedModel = A_C_ROTTWEILER
			
		BREAK
		
		CASE TEST_TASK_ARMED_MELEE_IN_COVER
		
			PlayerStartPos = <<6.5, 126.5, 7.4>>
			
			TestPeds[0].PedsCoords = <<1.6, 131.4, 7.4>>
            TestPeds[0].PedHeading = 90
            TestPeds[0].Pedrelgrp = 0
            TestPeds[0].PedModel = S_M_M_GAFFER_01
            
		BREAK
		
    ENDSWITCH   
ENDPROC

ENUM PREFERRED_COVER_MISSION_STATE
	SET_FIRST_DEFENSIVE_AREA,
	WAIT_1,
	SET_SECOND_DEFENSIVE_AREA,
	WAIT_2,
	SET_THIRD_DEFENSIVE_AREA,
	WAIT_3,
	SET_FOURTH_DEFENSIVE_AREA,
	WAIT_4
ENDENUM

PROC SET_UP_RELATIONSHIP_GROUPS ()
	INT index = 0
	
	FOR index = 0  to MAX_NUMBER_OF_PEDS -1
		ADD_RELATIONSHIP_GROUP_TO_STRUCT(TestPeds[index])
    ENDFOR
ENDPROC

PREFERRED_COVER_MISSION_STATE PC_STATE
INT TIME_BETWEEN_DEFENSIVE_MOVEMENT = 15000

PROC RUN_TEST()
    int index= 0, dead_attacking_peds = 0, j
	
    VECTOR offsetvec
   // VECTOR vPatrolPoint1 = <<-11.96, -9.73, -0.07>> + gvMapOffset
    //VECTOR vPatrolPoint2 = <<-13.69, 14.58, -0.07>> + gvMapOffset
	   
        SWITCH int_to_enum (scenarios, gcurrentselection)       // the ENUM corresponds to the values from the XML file 
                    //setup scenario
					CASE TEST_TASK_COMBAT_PED
									
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								SET_UP_RELATIONSHIP_GROUPS ()
										
								FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
				       				Block_Peds_Temp_Events(TestPeds[Index], FALSE )
		  						ENDFOR
		                    
								SCENARIO_STATE = STATE_1
							BREAK
						ENDSWITCH
					BREAK
					
					CASE TEST_TASK_COMBAT_HATED_TARGETS_AROUND_PED
						
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								SET_UP_RELATIONSHIP_GROUPS ()
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(TestPeds[0].Ped, 10.0)
								SCENARIO_STATE = STATE_1
							BREAK
						ENDSWITCH
						
					BREAK
					
					CASE TEST_TASK_COMBAT_HATED_TARGETS_AROUND_PED_TIMED
						
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								SET_UP_RELATIONSHIP_GROUPS ()
								TASK_COMBAT_HATED_TARGETS_AROUND_PED_TIMED(TestPeds[0].Ped, 10.0, COMBAT_TARGETS_AROUND_AREA_TIME)
								SCENARIO_STATE = STATE_1
							BREAK
						ENDSWITCH
						
					BREAK
					
					CASE TEST_TASK_COMBAT_TIMED
					
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								SET_UP_RELATIONSHIP_GROUPS ()
								TASK_COMBAT_PED_TIMED(TestPeds[0].Ped, TestPeds[1].Ped, COMBAT_TARGETS_AROUND_AREA_TIME)
								SCENARIO_STATE = STATE_1
						ENDSWITCH
						
					BREAK
					
					CASE TEST_TASK_COMBAT_HATED_PED_IN_AREA
					
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								SET_UP_RELATIONSHIP_GROUPS ()
								TASK_COMBAT_HATED_TARGETS_IN_AREA(TestPeds[0].Ped, TestPeds[1].PedsCoords, 10.0)
								SCENARIO_STATE = STATE_1
							BREAK
						ENDSWITCH
						
					BREAK
					
					CASE TEST_TASK_AIM_GUN_AT_COORD
					
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								TASK_AIM_GUN_AT_COORD(TestPeds[0].Ped, <<-42.33, 246.17, 8.00>>, AIM_AT_TIME)
								SCENARIO_STATE = STATE_1
							BREAK
						ENDSWITCH
						
					BREAK
					
					CASE TEST_TASK_AIM_GUN_AT_ENTITY
					
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								TASK_AIM_GUN_AT_ENTITY(TestPeds[0].Ped, TestPeds[1].Ped, AIM_AT_TIME)
								SCENARIO_STATE = STATE_1
							BREAK
						ENDSWITCH
					BREAK
					
					CASE TEST_TASK_SHOOT_AT_COORD
					
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								TASK_SHOOT_AT_COORD(TestPeds[0].Ped,  <<-42.33, 246.17, 8.00>>, AIM_AT_TIME, FIRING_TYPE_CONTINUOUS)	// firing type should be determined from firing pattern
								SCENARIO_STATE = STATE_1
							BREAK
						ENDSWITCH
					BREAK
					
					CASE TEST_TASK_GOTO_X_WHILE_AIMING_AT_X
						
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								SWITCH INT_TO_ENUM(GOTO_POINT_AND_AIM_MODE, GOTO_X_AND_AIM_AT_X_MODE)
								
									CASE GOTO_COORD_AND_AIM_AT_COORD
										TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(TestPeds[0].Ped, GOTO_COORD, AIM_AT_COORD, DYNAMIC_MOVE_BLEND_RATIO, MOVE_AND_SHOOT, 2.0, 4.0, FALSE)	// firing type should be determined from firing pattern
									BREAK
									
									CASE GOTO_COORD_AND_AIM_AT_OBJECT
										TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(TestPeds[0].Ped, GOTO_COORD, TestObjects[0].Object, DYNAMIC_MOVE_BLEND_RATIO, MOVE_AND_SHOOT, 2.0, 4.0, FALSE)	// firing type should be determined from firing pattern
									BREAK	
									
									CASE GOTO_COORD_AND_AIM_AT_PED
										IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD(TestPeds[1].Ped)
											TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(TestPeds[0].Ped, GOTO_COORD, TestPeds[1].Ped, DYNAMIC_MOVE_BLEND_RATIO, MOVE_AND_SHOOT, 2.0, 4.0, FALSE)	// firing type should be determined from firing pattern
										ENDIF
									BREAK
									
									CASE GOTO_COORD_AND_AIM_AT_VEHICLE									
										IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
											IF NOT IS_ENTITY_DEAD(TestPeds[2].Ped)
												SET_PED_INTO_VEHICLE(TestPeds[2].Ped, TestVehicles[0].Vehicle)
											ENDIF
											
											TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(TestPeds[0].Ped, GOTO_COORD, TestVehicles[0].Vehicle, DYNAMIC_MOVE_BLEND_RATIO, MOVE_AND_SHOOT, 2.0, 4.0, FALSE)	// firing type should be determined from firing pattern
										ENDIF
									BREAK
									
									CASE GOTO_PED_AND_AIM_AT_COORD
										IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD(TestPeds[1].Ped)
											TASK_GO_TO_ENTITY_WHILE_AIMING_AT_COORD(TestPeds[0].Ped, TestPeds[1].Ped, AIM_AT_COORD, DYNAMIC_MOVE_BLEND_RATIO, MOVE_AND_SHOOT, 2.0, 4.0, FALSE)	// firing type should be determined from firing pattern
										ENDIF
									BREAK
									
									CASE GOTO_PED_AND_AIM_AT_OBJECT
										IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD(TestPeds[1].Ped)
											TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(TestPeds[0].Ped, TestPeds[1].Ped, TestObjects[0].Object, DYNAMIC_MOVE_BLEND_RATIO, MOVE_AND_SHOOT, 2.0, 4.0, FALSE)	// firing type should be determined from firing pattern
										ENDIF
									BREAK
									
									CASE GOTO_PED_AND_AIM_AT_PED
										IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD(TestPeds[1].Ped)
											TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(TestPeds[0].Ped, TestPeds[1].Ped, TestPeds[1].Ped, DYNAMIC_MOVE_BLEND_RATIO, MOVE_AND_SHOOT, 2.0, 4.0, FALSE)	// firing type should be determined from firing pattern
										ENDIF
									BREAK
									
									CASE GOTO_PED_AND_AIM_AT_VEHICLE
										IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD(TestPeds[1].Ped)
											IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
												TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(TestPeds[0].Ped, TestPeds[1].Ped, TestVehicles[0].Vehicle, DYNAMIC_MOVE_BLEND_RATIO, MOVE_AND_SHOOT, 2.0, 4.0, FALSE)	// firing type should be determined from firing pattern
											ENDIF
										ENDIF
									BREAK
									
									CASE GOTO_PED_AIMING
										IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD(TestPeds[1].Ped)
											TASK_GOTO_ENTITY_AIMING(TestPeds[0].Ped, TestPeds[1].Ped, 2.0, 5.0)	// firing type should be determined from firing pattern
										ENDIF
									BREAK
									
								ENDSWITCH
								
								SCENARIO_STATE = STATE_1
							BREAK
						ENDSWITCH
						
					BREAK
				
					CASE TEST_TASK_PUT_PED_DIRECTLY_INTO_COVER
					CASE TEST_TASK_SEEK_COVER_FROM_POS
					CASE TEST_TASK_SEEK_COVER_FROM_PED
					CASE TEST_TASK_SEEK_COVER_TO_COVER_COORD
					CASE TEST_TASK_SEEK_COVER_TO_COVER_POINT
					
						SWITCH SCENARIO_STATE
									
							CASE STATE_INIT
							
								IF NOT gbPlayerShouldBeAtCamPos
									SET_ENTITY_COORDS(scplayer, PlayerStartPos, FALSE, true )
		           				ENDIF
								
								SET_UP_RELATIONSHIP_GROUPS()
								
								SETTIMERA(0)
								
								IF SCRIPTED_COVER_INDEX <> NULL
									REMOVE_COVER_POINT(SCRIPTED_COVER_INDEX)
								ENDIF
								
								IF bUseLowCoverPoint = TRUE
									SCRIPTED_COVER_INDEX = ADD_COVER_POINT(<<-64.11, 92.81, 6.35>>, 90.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_180)
								ELSE
									SCRIPTED_COVER_INDEX = ADD_COVER_POINT(<<-64.11, 103.75, 6.35>>, 90.0, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180)
								ENDIF
								
								SCENARIO_STATE = STATE_1
							BREAK
							CASE STATE_1
								IF bTestStanding
									// Aim forwards
									VECTOR vAimAtPos
									vAimAtPos = TestPeds[0].PedsCoords + 5.0 *GET_ENTITY_FORWARD_VECTOR(TestPeds[0].Ped) + <<0.0, 0.0, 0.5>>
									TASK_AIM_GUN_AT_COORD(TestPeds[0].Ped, vAimAtPos, -1)
								ELSE
									TASK_AIM_GUN_AT_ENTITY(TestPeds[0].Ped, TestPeds[1].Ped, -1)
								ENDIF
								SCENARIO_STATE = STATE_2
							BREAK
							CASE STATE_2
								IF TIMERA() > 3000
											
									IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD(TestPeds[1].Ped)
										
										SEQUENCE_INDEX SEQ
		    							OPEN_SEQUENCE_TASK (SEQ)
										SWITCH int_to_enum (scenarios, gcurrentselection)
											CASE TEST_TASK_PUT_PED_DIRECTLY_INTO_COVER
												IF SCRIPTED_COVER_INDEX <> NULL
													TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, <<-64.05, 98.23, 6.35>>, 5000, FALSE, NORMAL_BLEND_DURATION, FALSE, FALSE, SCRIPTED_COVER_INDEX)
												ENDIF
											BREAK
											CASE TEST_TASK_SEEK_COVER_FROM_POS
												TASK_SEEK_COVER_FROM_POS(NULL, TestPeds[1].PedsCoords, 5000)
											BREAK
											CASE TEST_TASK_SEEK_COVER_FROM_PED
												TASK_SEEK_COVER_FROM_PED(NULL, TestPeds[1].Ped, 5000)
											BREAK
											CASE TEST_TASK_SEEK_COVER_TO_COVER_POINT
												IF SCRIPTED_COVER_INDEX <> NULL
													TASK_SEEK_COVER_TO_COVER_POINT(NULL, SCRIPTED_COVER_INDEX, <<-69.58, 104.78, 6.35>>, 5000)
												ENDIF
											BREAK
											CASE TEST_TASK_SEEK_COVER_TO_COVER_COORD
												TASK_SEEK_COVER_TO_COORDS(NULL, <<-64.15, 94.94, 6.35>>, TestPeds[1].PedsCoords, 10000)
											BREAK
										ENDSWITCH
										
										TASK_COMBAT_PED_TIMED(NULL, TestPeds[1].Ped, 30000)
										CLOSE_SEQUENCE_TASK(SEQ)
								
										// Wait till coverpoints are streamed in
										SETTIMERA(0)
										TASK_PERFORM_SEQUENCE (TestPeds[0].Ped, SEQ)
		        						CLEAR_SEQUENCE_TASK(seq)
								
									ENDIF
				
									SCENARIO_STATE = STATE_3
								ENDIF
							BREAK
						ENDSWITCH	
						
					BREAK
					
					CASE TEST_TASK_GUARD_ASSIGNED_DEFENSIVE_AREA
					CASE TEST_TASK_GUARD_ANGLED_DEFENSIVE_AREA
					CASE TEST_TASK_GUARD_SPHERE_DEFENSIVE_AREA
					CASE TEST_TASK_GUARD_CURRENT_POS
					CASE TEST_TASK_STAND_GUARD
					
					SWITCH SCENARIO_STATE
						CASE STATE_INIT
							
							SET_UP_RELATIONSHIP_GROUPS()
																	
							SWITCH int_to_enum (scenarios, gcurrentselection)
								CASE TEST_TASK_GUARD_ASSIGNED_DEFENSIVE_AREA
									TASK_GUARD_ASSIGNED_DEFENSIVE_AREA(TestPeds[0].Ped, <<13.77, 182.27, 6.35>>, 0.0, 5.0, 30000)
								BREAK
								CASE TEST_TASK_GUARD_ANGLED_DEFENSIVE_AREA
									TASK_GUARD_ANGLED_DEFENSIVE_AREA(TestPeds[0].Ped, <<13.77, 182.27, 6.35>>, 0.0, 10.0, 30000, <<7.825742,181.893753,6.350637>>, <<21.033215,181.771240,16.350637>>, 11.0)
								BREAK
								CASE TEST_TASK_GUARD_SPHERE_DEFENSIVE_AREA
									TASK_GUARD_SPHERE_DEFENSIVE_AREA(TestPeds[0].Ped, <<13.77, 182.27, 6.35>>, 0.0, 10.0, 30000, <<13.77, 182.27, 6.35>>,  5.0)
								BREAK
								CASE TEST_TASK_GUARD_CURRENT_POS
									TASK_GUARD_CURRENT_POSITION(TestPeds[0].Ped, 10.0, 5.0, TRUE)
								BREAK
								CASE TEST_TASK_STAND_GUARD
									TASK_STAND_GUARD(TestPeds[0].Ped, <<13.77, 182.27, 6.35>>, 0.0, "DEFEND")
								BREAK
							ENDSWITCH		
							
							SCENARIO_STATE = STATE_1
								
						BREAK
					ENDSWITCH
					
					BREAK
 
                    CASE SET_PED_PREFERRED_COVER
											
						SWITCH PREFERRED_COVER_STATE
							CASE PREFERRED_COVER_INIT		
							
								SET_UP_RELATIONSHIP_GROUPS()
								
								FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
				       				Block_Peds_Temp_Events(TestPeds[0], FALSE )
		  						ENDFOR
								
								// Warp player to scenario location
								IF NOT gbPlayerShouldBeAtCamPos
									SET_ENTITY_COORDS(scplayer, PlayerStartPos, FALSE, true )
		           				ENDIF
								
								SET_PED_PREFERRED_COVER_SET(TestPeds[0].Ped, PreferredCoverItemSet)					         

								PC_STATE = SET_FIRST_DEFENSIVE_AREA
							   
								PREFERRED_COVER_STATE = PREFERRED_COVER_RUN
							BREAK
							CASE PREFERRED_COVER_RUN
								SWITCH PC_STATE
									CASE SET_FIRST_DEFENSIVE_AREA
										SET_PED_SPHERE_DEFENSIVE_AREA(TestPeds[0].Ped, COV_POS2, 8.0)
										SETTIMERA(0)
										PC_STATE = WAIT_1
									BREAK
									CASE WAIT_1
										IF (TIMERA() > TIME_BETWEEN_DEFENSIVE_MOVEMENT)
											PC_STATE = SET_SECOND_DEFENSIVE_AREA
										ENDIF
									BREAK
									CASE SET_SECOND_DEFENSIVE_AREA
										//SET_PED_SPHERE_DEFENSIVE_AREA(TestPeds[0].Ped, COV_POS2, 1.0)
										SETTIMERA(0)
										PC_STATE = WAIT_2
									BREAK
									CASE WAIT_2
										IF (TIMERA() > TIME_BETWEEN_DEFENSIVE_MOVEMENT)
										PC_STATE = SET_THIRD_DEFENSIVE_AREA
										ENDIF
									BREAK
									CASE SET_THIRD_DEFENSIVE_AREA
										//SET_PED_SPHERE_DEFENSIVE_AREA(TestPeds[0].Ped, COV_POS3, 1.0)
										SETTIMERA(0)
										PC_STATE = WAIT_3
									BREAK
									CASE WAIT_3
										IF (TIMERA() > TIME_BETWEEN_DEFENSIVE_MOVEMENT)
										PC_STATE = SET_FOURTH_DEFENSIVE_AREA
										ENDIF
									BREAK
									CASE SET_FOURTH_DEFENSIVE_AREA
										SET_PED_SPHERE_DEFENSIVE_AREA(TestPeds[0].Ped, COV_POS4, 1.0)
										SETTIMERA(0)
										PC_STATE = WAIT_4
									BREAK
									CASE WAIT_4
										IF (TIMERA() > TIME_BETWEEN_DEFENSIVE_MOVEMENT)
										PC_STATE = SET_FIRST_DEFENSIVE_AREA
										ENDIF
									BREAK
								ENDSWITCH
							BREAK
						ENDSWITCH				
						//Set_Test_State_To_Default()
					BREAK
					
					CASE TEST_TASK_PATROL
						
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								
							FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
		                        Block_Peds_Temp_Events(TestPeds[index], FALSE )
		                    ENDFOR
	                            
		                    SETUP_PATROL_ROUTES ()
		                
		                    TEST_SEQ = FOLLOW_PATROL_ROUTE ("miss_loop", PAS_ALERT)
		                    PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
		        
		                    TEST_SEQ = FOLLOW_PATROL_ROUTE ("miss_branched_loop", PAS_ALERT)
		                    PedPerformsSequence (TestPeds[1].ped, TEST_SEQ)
		                    
		                    TEST_SEQ = FOLLOW_PATROL_ROUTE ("miss_branched", PAS_ALERT)
		                    PedPerformsSequence (TestPeds[2].ped, TEST_SEQ) 
		                    
		                    TEST_SEQ = FOLLOW_PATROL_ROUTE ("miss_dead_end", PAS_ALERT)
		                    PedPerformsSequence (TestPeds[3].ped, TEST_SEQ) 
		                
		                   SCENARIO_STATE = STATE_1
						   BREAK
					 ENDSWITCH
					BREAK
					
					CASE TEST_IS_PED_RESPONDING_TO_EVENT
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								SET_UP_RELATIONSHIP_GROUPS ()
										
			       				Block_Peds_Temp_Events(TestPeds[0], FALSE )
								Block_Peds_Temp_Events(TestPeds[1], TRUE )
                    
								SCENARIO_STATE = STATE_1
							BREAK
							CASE STATE_1
								IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND IS_PED_RESPONDING_TO_EVENT(TestPeds[0].Ped, EVENT_ACQUAINTANCE_PED_HATE)
									TASK_AIM_GUN_AT_COORD(TestPeds[0].Ped, <<-42.33, 246.17, 8.00>>, AIM_AT_TIME)
									Block_Peds_Temp_Events(TestPeds[0], TRUE)
									SCENARIO_STATE = STATE_2
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					
					CASE TEST_HAS_PED_RECEIVED_EVENT
						
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								SET_UP_RELATIONSHIP_GROUPS()
								
								// Warp player to scenario location
								IF NOT gbPlayerShouldBeAtCamPos
									SET_ENTITY_COORDS(scplayer, PlayerStartPos, FALSE, true )
				           		ENDIF
								
								Block_Peds_Temp_Events(TestPeds[0], TRUE)   
								
								SCENARIO_STATE = STATE_1
							BREAK
						ENDSWITCH
								
						IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped)  
							IF GET_SCRIPT_TASK_STATUS(TestPeds[0].Ped, SCRIPT_TASK_HANDS_UP) = FINISHED_TASK
								IF HAS_PED_RECEIVED_EVENT(TestPeds[0].Ped, EVENT_GUN_AIMED_AT)
									TASK_HANDS_UP(TestPeds[0].Ped, 5000)
								ENDIF
							ENDIF
						ENDIF
								
					BREAK
					
					CASE SCENARIO_PED_MOVE_TO_COVER_PED_STAYS_STILL
					
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								
								SET_UP_RELATIONSHIP_GROUPS()
																	
		                        FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
		                            Block_Peds_Temp_Events(TestPeds[index], FALSE )
		                        ENDFOR
								
								SCENARIO_STATE = STATE_1
							BREAK
						ENDSWITCH
					BREAK
					
                    CASE BugPedsNotShootingThroughFreindlyPed
                        FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
                            Block_Peds_Temp_Events(TestPeds[index], FALSE )
                            IF NOT IS_PED_INJURED(TestPeds[index].ped)
                                SET_PED_CAN_RAGDOLL (TestPeds[index].ped, FALSE )
                                SET_ENTITY_INVINCIBLE(TestPeds[index].ped, TRUE)
                            ENDIF   
                        ENDFOR
                    BREAK   
                    
					
                    CASE SCENARIO_BOTH_PEDS_MOVE_TO_COVER   
                    CASE ARifle_guy_in_cover_breaking 
                    CASE SCENARIO_TWO_PEDS_NO_COVER

                        FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
                            Block_Peds_Temp_Events(TestPeds[index], FALSE )
                        ENDFOR
                    
                            Set_Test_State_To_Default()
                    BREAK
                    
                
                    //scenario 
                    CASE ScenarioTaskStayInCover
                        SWITCH Scenario_TaskStayInCover
                            CASE Scenario_TaskStayInCover_init
                                FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
                                        IF NOT IS_PED_INJURED(TestPeds[index].ped)
                                            SET_PED_CAN_RAGDOLL (TestPeds[index].ped, FALSE )
                                            SET_ENTITY_PROOFS(TestPeds[index].ped, TRUE, TRUE, TRUE, TRUE, TRUE)
                                            TASK_STAY_IN_COVER(TestPeds[index].ped)
                                        ENDIF
                                    Block_Peds_Temp_Events(TestPeds[index], TRUE )
                                    
                                ENDFOR
                                
                                FOR Index = 0  to MAX_NUMBER_OF_VEHICLES -1
                                    IF IS_VEHICLE_DRIVEABLE (TestVehicles[index].vehicle)
                                        SET_ENTITY_PROOFS(TestVehicles[index].vehicle, true, FALSE, FALSE, FALSE, FALSE)
                                    ENDIF
                                ENDFOR
                                
                                
                                Scenario_TaskStayInCover = Scenario_TaskStayInCover_SwapDefAreas
                            
                            
                            BREAK
                            
                            CASE Scenario_TaskStayInCover_SwapDefAreas
                            
                                IF IS_BUTTON_JUST_PRESSED (PAD1, TRIANGLE)
                                    iCurrentDefensiveArea += 1
                                        
                                        IF iCurrentDefensiveArea > MAX_NUMBER_DEFENSIVE_AREA -1
                                            iCurrentDefensiveArea = 0
                                        ENDIF
                                        
                                    for index = 0 to MAX_NUMBER_OF_PEDS -1
                                        IF NOT IS_PED_INJURED(TestPeds[index].ped)
                                            IF TestPeds[index].Pedrelgrp = 1
                                                TestPeds[index].PedDefCoord =  DefensiveSphereAreas[iCurrentDefensiveArea].DefensiveAreaPos
                                                TestPeds[index].PedDefArea =  DefensiveSphereAreas[iCurrentDefensiveArea].DefensiveAreaRadius
                                                TestPeds[index].PedDefDirection =  DefensiveSphereAreas[iCurrentDefensiveArea].DefensiveAreaDirection
                                                PRINTSTRING("PedDefDirection: <<")
                                                PRINTFLOAT(TestPeds[index].PedDefDirection.x)
                                                PRINTSTRING(", ")
                                                PRINTFLOAT(TestPeds[index].PedDefDirection.y)
                                                PRINTSTRING(", ")
                                                PRINTFLOAT(TestPeds[index].PedDefDirection.z)
                                                PRINTSTRING(">>")
                                                PRINTNL()
                                                TestPeds[index].PedActiveDefArea = TRUE
                                                SET_PED_DEFENSIVE_AREAS(TestPeds[index] )
                                            ENDIF
                                        ENDIF   
                                    ENDFOR
                                ENDIF
                            BREAK
                        
                        ENDSWITCH   
                            
                    BREAK

                    CASE SquadOfPedsMovingBetweenVehicles
                    CASE TestPedsUsingRandomPaths
                        SWITCH Scenario_SquadOfPedsMovingBetweenVehicles
                            CASE Scenario_SquadOfPedsMovingBetweenVehicles_init
                                FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
                                        IF NOT IS_PED_INJURED(TestPeds[index].ped)
                                            SET_PED_CAN_RAGDOLL (TestPeds[index].ped, FALSE )
                                            SET_ENTITY_PROOFS(TestPeds[index].ped, TRUE, TRUE, TRUE, TRUE, TRUE)
                                        ENDIF
                                    Block_Peds_Temp_Events(TestPeds[index], FALSE )
                                    
                                ENDFOR
                                
                                FOR Index = 0  to MAX_NUMBER_OF_VEHICLES -1
                                    IF IS_VEHICLE_DRIVEABLE (TestVehicles[index].vehicle)
                                        SET_ENTITY_PROOFS(TestVehicles[index].vehicle, true, FALSE, FALSE, FALSE, FALSE)
                                    ENDIF
                                ENDFOR
                                
                                
                                Scenario_SquadOfPedsMovingBetweenVehicles = Scenario_SquadOfPedsMovingBetweenVehicles_SwapDefAreas
                            
                            
                            BREAK
                            
                            CASE Scenario_SquadOfPedsMovingBetweenVehicles_SwapDefAreas
                            
                                IF IS_BUTTON_JUST_PRESSED (PAD1, TRIANGLE)
                                    iCurrentDefensiveArea += 1
                                        
                                        IF iCurrentDefensiveArea > MAX_NUMBER_DEFENSIVE_AREA -1
                                            iCurrentDefensiveArea = 0
                                        ENDIF
                                        
                                    for index = 0 to MAX_NUMBER_OF_PEDS -1
                                        IF NOT IS_PED_INJURED(TestPeds[index].ped)
                                            IF TestPeds[index].Pedrelgrp = 1
                                                TestPeds[index].PedDefCoord =  DefensiveSphereAreas[iCurrentDefensiveArea].DefensiveAreaPos
                                                TestPeds[index].PedDefArea =  DefensiveSphereAreas[iCurrentDefensiveArea].DefensiveAreaRadius
                                                TestPeds[index].PedActiveDefArea = TRUE
                                                SET_PED_DEFENSIVE_AREAS(TestPeds[index] )
                                            ENDIF
                                        ENDIF   
                                    ENDFOR
                                ENDIF
                            BREAK
                        
                        ENDSWITCH   
                            
                    BREAK
                    
                    CASE BugAiCantDriveTank
                        IF NOT IS_PED_INJURED (TestPeds[0].ped)
                            IF IS_VEHICLE_DRIVEABLE (TestVehicles [5].Vehicle)
                                Block_Peds_Temp_Events (TestPeds[0], TRUE)
                                SET_PED_INTO_VEHICLE (TestPeds[0].ped, TestVehicles [5].Vehicle )
                                DRIVE_TO_POINT_SEQUENCE (TestPeds[0].ped, TestVehicles [5].Vehicle)
                            ENDIF
                        ENDIF
                        Set_Test_State_To_Default()
                    BREAK
                    
                    

                    CASE SquadStaysDefensiveBehindVehicle
                        SWITCH Scenario_SquadStaysDefensiveBehindVehicle
                            CASE Scenario_SquadStaysDefensiveBehindVehicle_Init
        
                                FOR INDEX = 0 to MAX_NUMBER_OF_PEDS-1
//                                    SET_PED_SENSE_RANGE (TestPeds[index].ped, 100)
                                ENDFOR
                                
                                IF NOT IS_PED_INJURED (TestPeds[0].ped)
                                    IF IS_VEHICLE_DRIVEABLE (TestVehicles [5].Vehicle)
                                        Block_Peds_Temp_Events (TestPeds[0], TRUE)
                                        SET_PED_INTO_VEHICLE (TestPeds[0].ped, TestVehicles [5].Vehicle )
                                        DRIVE_TO_POINT_SEQUENCE (TestPeds[0].ped, TestVehicles [5].Vehicle)
                                    ENDIF
                                ENDIF
                                
                                FOR Index = 1  to MAX_NUMBER_OF_PEDS -1
                                    SetPedsInvincible (TestPeds[index].ped, TRUE)
                                    Block_Peds_Temp_Events(TestPeds[index], FALSE )
                                ENDFOR
                                Scenario_SquadStaysDefensiveBehindVehicle = Scenario_SquadStaysDefensiveBehindVehicle_Run
                            break
                            CASE Scenario_SquadStaysDefensiveBehindVehicle_Run
                                IF IS_VEHICLE_DRIVEABLE (TestVehicles [5].Vehicle)
                                //  offsetvec = GET_ENTITY_COORDS( TestVehicles [5].Vehicle)
                                    offsetvec = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS (TestVehicles [5].Vehicle, <<0.0, -3.0, 0.0>>)
                                    
                                    for index = 1 to MAX_NUMBER_OF_PEDS -1
                                        IF TestPeds[index].Pedrelgrp = 1
                                            TestPeds[index].PedDefCoord = offsetvec
                                            TestPeds[index].PedDefArea =  6.0
                                            TestPeds[index].PedActiveDefArea = TRUE
                                            SET_PED_DEFENSIVE_AREAS(TestPeds[index] )
                                        ENDIF
                                    ENDFOR
                                ENDIF
                            BREAK
                    
                        ENDSWITCH
                    BREAK

                    CASE SquadDefendingPos
                            
                        SWITCH Scenario_SquadDefendingPos
                            CASE Scenario_SquadDefendingPos_init

                                FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
                                    Block_Peds_Temp_Events(TestPeds[index], FALSE )
                                ENDFOR
                                
                                TestPeds[7].PedsCoords = << -4.7500, -14.5000, 1.1959>> + gvMapOffset
                                TestPeds[7].PedHeading = -0.0000
                                TestPeds[7].Pedrelgrp = 3
                                TestPeds[7].PedModel = S_M_Y_COP_01
                                TestPeds[7].PedsWeapon = WEAPONTYPE_PISTOL
                                
                                TestPeds[8].PedsCoords = << -7.5000, -14.5000, 1.1959>> + gvMapOffset
                                TestPeds[8].PedHeading = -0.0000
                                TestPeds[8].Pedrelgrp = 3
                                TestPeds[8].PedModel = S_M_Y_COP_01
                                TestPeds[8].PedsWeapon = WEAPONTYPE_PISTOL
            
                                Scenario_SquadDefendingPos =  Scenario_SquadDefendingPos_grp2
                            BREAK   
                                
                            CASE Scenario_SquadDefendingPos_grp2
                                dead_attacking_peds = 0
                                for index = 0 to MAX_NUMBER_OF_PEDS-1
                                    IF TestPeds[index].Pedrelgrp = 2                            //check rel grp 2
                                    
                                        IF IS_PED_INJURED (TestPeds[index].ped)
                                            dead_attacking_peds += 1
                                        ENDIF   
                                            
                                            IF dead_attacking_peds = 2                              //all rel 2 peds are dead create a new group
                                                for j  = 0 to MAX_NUMBER_OF_PEDS-1
                                                    IF TestPeds[j].Pedrelgrp = 2
                                                        CLEAN_UP_PED_ENTITIES (TestPeds[j].ped)
                                                    ENDIF   
                                                    IF TestPeds[j].Pedrelgrp = 3
                                                        CREATE_PED_ENTITY(TestPeds[j])
                                                        GIVEBLIPSTOPEDSGRPS (TestPeds[j])
                                                        ALTER_COMBAT_STATS (TestPeds[j] )
                                                        SWAP_PED_WEAPONS (TestPeds[j])
                                                        Scenario_SquadDefendingPos = Scenario_SquadDefendingPos_grp3
                                                    ENDIF
                                                    
                                                ENDFOR
                                                dead_attacking_peds = 0 
                                                index =  MAX_NUMBER_OF_PEDS-1
                                            ENDIF
                                    ENDIF
                                ENDFOR
                            BREAK   
                    
                            CASE Scenario_SquadDefendingPos_grp3
                                for index = 0 to MAX_NUMBER_OF_PEDS-1
                                    IF TestPeds[index].Pedrelgrp = 3                            //check rel grp 2
                                        IF IS_PED_INJURED (TestPeds[index].ped)
                                            dead_attacking_peds += 1
                                        ENDIF   
                                        
                                        IF dead_attacking_peds = 2                          //all rel 2 peds are dead create a new group
                                            for j = 0 to MAX_NUMBER_OF_PEDS-1
                                                IF TestPeds[j].Pedrelgrp = 3
                                                    CLEAN_UP_PED_ENTITIES (TestPeds[j].ped)
                                                ENDIF   
                                                IF TestPeds[j].Pedrelgrp = 2
                                                    CREATE_PED_ENTITY(TestPeds[j])
                                                    GIVEBLIPSTOPEDSGRPS (TestPeds[j])
                                                    ALTER_COMBAT_STATS (TestPeds[j] )
                                                    SWAP_PED_WEAPONS (TestPeds[j])
                                       
                                                    Scenario_SquadDefendingPos = Scenario_SquadDefendingPos_grp2
                                                ENDIF
                                            ENDFOR
                                            dead_attacking_peds = 0 
                                            index =  MAX_NUMBER_OF_PEDS-1
                                        ENDIF
                                    ENDIF
                                ENDFOR
                            BREAK
                        
                        ENDSWITCH
                    BREAK
                                    
                    CASE PedDoesWeirdThingsSeekingCover
                        SWITCH Scenario_PedDoesWeirdThingsSeekingCover_Status
                            CASE Scenario_PedDoesWeirdThingsSeekingCover_init

                                    SetPedsInvincible (TestPeds[2].ped, TRUE)
                                    IF NOT IS_PED_INJURED (TestPeds[2].ped)
                                        TASK_SEEK_COVER_FROM_POS ( TestPeds[2].ped, <<0.0, 10.0, 1.5 >>+ gvMapOffset, -1 )
                                    ENDIF
                                    Scenario_PedDoesWeirdThingsSeekingCover_Status =  Scenario_PedDoesWeirdThingsSeekingCover_run
                            BREAK
                            
                            CASE Scenario_PedDoesWeirdThingsSeekingCover_run
                            
                            
                            BREAK
                        ENDSWITCH
                    BREAK   

                    CASE ScenarioCombatRanges 
                            SWITCH Scenario_CombatRanges_Status
                                CASE Scenario_CombatRanges_init
                    
                                    FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
                                        SetPedsInvincible (TestPeds[index].ped, TRUE)
                                        Block_Peds_Temp_Events(TestPeds[index], FALSE )
                                    ENDFOR
                                BREAK
                                
                                CASE Scenario_CombatRanges_run
                                    IF IS_BUTTON_JUST_PRESSED (PAD1, TRIANGLE)
                                        iCurrentDefensiveArea += 1
                                            
                                            IF iCurrentDefensiveArea > 2
                                                iCurrentDefensiveArea = 0
                                            ENDIF
                                            
                                        for index = 0 to MAX_NUMBER_OF_PEDS -1
                                            IF NOT IS_PED_INJURED(TestPeds[index].ped)
                                                IF TestPeds[index].Pedrelgrp = 1
                                                    SET_PED_COMBAT_RANGE (TestPeds[index].ped, INT_TO_ENUM(COMBAT_RANGE, TestPeds[index].pedcombatrange  ))
                                                ENDIF
                                            ENDIF   
                                        ENDFOR
                                    ENDIF 
                                
                                BREAK
                                
                                
                            ENDSWITCH
                    BREAK

                    
                    CASE TargetDummy
                        FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
                            Block_Peds_Temp_Events(TestPeds[index], FALSE )
                            IF NOT IS_PED_INJURED(TestPeds[index].ped)
                                SET_PED_CAN_RAGDOLL(TestPeds[index].ped, FALSE)
                                SET_ENTITY_INVINCIBLE(TestPeds[index].ped, TRUE)
                                SET_DECISION_MAKER(TestPeds[index].ped, DECISION_MAKER_EMPTY)
                            ENDIF
                        ENDFOR
                    BREAK
                        
					CASE SWAT_VAN_DRIVE_BY
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								
								SET_UP_RELATIONSHIP_GROUPS()
								
								IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
									IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped)
										SET_PED_INTO_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, VS_EXTRA_LEFT_1)
									ENDIF
									
									IF NOT IS_ENTITY_DEAD(TestPeds[1].Ped)
										SET_PED_INTO_VEHICLE(TestPeds[1].Ped, TestVehicles[0].Vehicle, VS_EXTRA_LEFT_2)
									ENDIF
									
									IF NOT IS_ENTITY_DEAD(TestPeds[2].Ped)
										SET_PED_INTO_VEHICLE(TestPeds[2].Ped, TestVehicles[0].Vehicle, VS_EXTRA_RIGHT_1)
									ENDIF
									
									IF NOT IS_ENTITY_DEAD(TestPeds[3].Ped)
										SET_PED_INTO_VEHICLE(TestPeds[3].Ped, TestVehicles[0].Vehicle, VS_EXTRA_RIGHT_2)
									ENDIF
								ENDIF
								
								// Warp player to scenario location
								IF NOT gbPlayerShouldBeAtCamPos
									SET_ENTITY_COORDS(scplayer, PlayerStartPos, FALSE, true )
				           		ENDIF
																	
								SCENARIO_STATE = STATE_1
								
							BREAK
							
							CASE STATE_1
								IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle) AND DO_DRIVEBY
									IF NOT IS_PED_IN_VEHICLE(scplayer, TestVehicles[0].Vehicle) AND NOT IS_PED_GETTING_INTO_A_VEHICLE(scplayer)
										FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
											IF NOT IS_ENTITY_DEAD(TestPeds[index].Ped)
												SET_PED_AMMO(TestPeds[index].Ped, WEAPONTYPE_CARBINERIFLE, -1)
				                           		Block_Peds_Temp_Events(TestPeds[index], TRUE )
												TASK_DRIVE_BY(TestPeds[index].Ped, scplayer, NULL, <<0.0,0.0,0.0>>, 50.0, 100)
											ENDIF
				                        ENDFOR
										SCENARIO_STATE = STATE_2
									ENDIF 	
								ENDIF
							BREAK
							
							CASE STATE_2
							
								IF NOT IS_ENTITY_DEAD(TestVehicles[0].Vehicle)
									IF IS_PED_IN_VEHICLE(scplayer, TestVehicles[0].Vehicle)
										FOR Index = 0  to MAX_NUMBER_OF_PEDS -1
											IF NOT IS_ENTITY_DEAD(TestPeds[index].Ped)
												CLEAR_PED_TASKS(TestPeds[index].Ped)
											ENDIF
			                       		ENDFOR
										SCENARIO_STATE = STATE_1
									ENDIF
								ENDIF
							BREAK
								
								
						ENDSWITCH
					BREAK
					
					CASE TASK_RAPPEL_DOWN_WALL_PlayerTest					
					CASE TASK_RAPPEL_DOWN_WALL_AITest
						SWITCH SCENARIO_STATE
						
							CASE STATE_INIT
								
								IF NOT IS_ENTITY_DEAD(scplayer)
									// Warp player to scenario location
									IF NOT gbPlayerShouldBeAtCamPos
										SET_ENTITY_COORDS(scplayer, PlayerStartPos, FALSE, true )					
										SET_ENTITY_HEADING(scplayer, 270)
					           		ENDIF
								ENDIF
																						
								SCENARIO_STATE = STATE_1								
							BREAK
							
							CASE STATE_1
								
								PED_INDEX pedIndex
								SWITCH int_to_enum (scenarios, gcurrentselection)
									CASE TASK_RAPPEL_DOWN_WALL_PlayerTest
										pedIndex = scplayer
									BREAK
									CASE TASK_RAPPEL_DOWN_WALL_AITest
										pedIndex = TestPeds[0].Ped
									BREAK									
								ENDSWITCH
								
								IF NOT IS_ENTITY_DEAD(pedIndex)
									ROPE_INDEX serverLocalRope
									VECTOR vRopeCoords
									VECTOR vCurrentCoords
									vCurrentCoords = GET_ENTITY_COORDS(pedIndex)
									vRopeCoords = vCurrentCoords
									vRopeCoords.x = 0.0
									serverLocalRope = ADD_ROPE(vRopeCoords, <<0.0, 90.0, 0.0>>, 30.0, PHYSICS_ROPE_DEFAULT_32, -1, 0.5, 0.5, TRUE)
									TASK_RAPPEL_DOWN_WALL(pedIndex, vCurrentCoords, vRopeCoords, vCurrentCoords.z - 18.0, serverLocalRope)
								ENDIF
								
								SCENARIO_STATE = STATE_2
								
							BREAK
							
						ENDSWITCH
					BREAK
					
					CASE TEST_TASK_MELEE_COMBAT_ONE
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								IF NOT IS_ENTITY_DEAD( TestPeds[0].Ped ) AND NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
									SET_PED_RELATIONSHIP_GROUP_HASH( TestPeds[0].Ped, RELGROUPHASH_AMBIENT_GANG_LOST )
									TASK_COMBAT_PED( TestPeds[0].Ped, PLAYER_PED_ID() )
								ENDIF	
								SCENARIO_STATE = STATE_1								
							BREAK
						ENDSWITCH
					BREAK
					
					CASE TEST_TASK_MELEE_COMBAT_MANY
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								IF NOT IS_ENTITY_DEAD( TestPeds[0].Ped ) AND NOT IS_ENTITY_DEAD( TestPeds[1].Ped ) AND NOT IS_ENTITY_DEAD( TestPeds[2].Ped ) AND NOT IS_ENTITY_DEAD( TestPeds[4].Ped ) AND NOT IS_ENTITY_DEAD( TestPeds[5].Ped ) AND NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
									SET_PED_RELATIONSHIP_GROUP_HASH( TestPeds[0].Ped, RELGROUPHASH_AMBIENT_GANG_LOST )
									SET_PED_RELATIONSHIP_GROUP_HASH( TestPeds[1].Ped, RELGROUPHASH_AMBIENT_GANG_LOST )
									SET_PED_RELATIONSHIP_GROUP_HASH( TestPeds[2].Ped, RELGROUPHASH_AMBIENT_GANG_LOST )
									SET_PED_RELATIONSHIP_GROUP_HASH( TestPeds[3].Ped, RELGROUPHASH_AMBIENT_GANG_LOST )
									SET_PED_RELATIONSHIP_GROUP_HASH( TestPeds[4].Ped, RELGROUPHASH_AMBIENT_GANG_LOST )
									SET_PED_RELATIONSHIP_GROUP_HASH( TestPeds[5].Ped, RELGROUPHASH_AMBIENT_GANG_LOST )
									TASK_COMBAT_PED( TestPeds[0].Ped, PLAYER_PED_ID() )
									TASK_COMBAT_PED( TestPeds[1].Ped, PLAYER_PED_ID() )
									TASK_COMBAT_PED( TestPeds[2].Ped, PLAYER_PED_ID() )
									TASK_COMBAT_PED( TestPeds[3].Ped, PLAYER_PED_ID() )
									TASK_COMBAT_PED( TestPeds[4].Ped, PLAYER_PED_ID() )
									TASK_COMBAT_PED( TestPeds[5].Ped, PLAYER_PED_ID() )
								ENDIF	
								SCENARIO_STATE = STATE_1								
							BREAK
						ENDSWITCH
					BREAK
					
					CASE TEST_TASK_DRAG_INJURED_PED
						SWITCH SCENARIO_STATE
						
							CASE STATE_INIT
								APPLY_DAMAGE_TO_PED(TestPeds[0].ped, 1000, FALSE)
								SCENARIO_STATE = STATE_1
							BREAK
							
							CASE STATE_1
								IF IS_ENTITY_DEAD(TestPeds[0].ped)
									TASK_DRAG_PED_TO_COORD(TestPeds[1].ped, TestPeds[0].ped, <<-2.92, 84.14, 7.36>>)
									SCENARIO_STATE = STATE_2
								ENDIF
							BREAK
							
						ENDSWITCH
					BREAK
					
					CASE TEST_TASK_SHOOT_AT_PLAYER
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								
								IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(TestPeds[0].Ped)
									GIVE_WEAPON_TO_PED( TestPeds[0].Ped, WEAPONTYPE_PISTOL, 1000, TRUE )
									//TASK_SHOOT_AT_ENTITY( TestPeds[0].Ped, PLAYER_PED_ID(), -1, FIRING_TYPE_1_BURST )
									TASK_COMBAT_PED( TestPeds[0].Ped, PLAYER_PED_ID() )
								ENDIF
																						
								SCENARIO_STATE = STATE_1								
							BREAK
						ENDSWITCH
					BREAK
					
					CASE TEST_TASK_SHOOT_AT_VEHICLE
						
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								IF NOT IS_ENTITY_DEAD( TestPeds[0].Ped ) AND NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
									SET_PED_RELATIONSHIP_GROUP_HASH( TestPeds[0].Ped, RELGROUPHASH_AMBIENT_GANG_LOST )
									SET_ENTITY_ONLY_DAMAGED_BY_PLAYER( PLAYER_PED_ID(), TRUE )
									GIVE_WEAPON_TO_PED( TestPeds[0].Ped, WEAPONTYPE_MINIGUN, 2000, TRUE, TRUE )
									WAIT(100)
									TASK_SHOOT_AT_COORD( TestPeds[0].Ped, <<-3.6,76.4,7.0>>, -1, FIRING_TYPE_CONTINUOUS )
									//TASK_COMBAT_PED( TestPeds[0].Ped, PLAYER_PED_ID() )
								ENDIF	
								SCENARIO_STATE = STATE_1								
							BREAK
						ENDSWITCH
						
					BREAK
					
					CASE TEST_TASK_AI_STEALTH_KILL
						
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								IF NOT IS_ENTITY_DEAD( TestPeds[0].Ped ) AND NOT IS_ENTITY_DEAD( TestPeds[1].Ped )
									GIVE_WEAPON_TO_PED( TestPeds[0].Ped, WEAPONTYPE_UNARMED, 1, TRUE, TRUE )
									GIVE_WEAPON_TO_PED( TestPeds[1].Ped, WEAPONTYPE_UNARMED, 1, TRUE, TRUE )
									SET_PED_RELATIONSHIP_GROUP_HASH( TestPeds[0].Ped, RELGROUPHASH_AMBIENT_GANG_LOST )
									TASK_STEALTH_KILL( TestPeds[0].Ped, TestPeds[1].Ped, GET_HASH_KEY( "AR_stealth_kill_a" ) )
								ENDIF	
								SCENARIO_STATE = STATE_1								
							BREAK
						ENDSWITCH
						
					BREAK
					
					CASE TEST_TASK_ANIMAL_MELEE
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								IF NOT IS_ENTITY_DEAD( TestPeds[0].Ped ) AND NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
									SET_PED_RELATIONSHIP_GROUP_HASH( TestPeds[0].Ped, RELGROUPHASH_AMBIENT_GANG_LOST )
									GIVE_WEAPON_TO_PED( TestPeds[0].Ped, WEAPONTYPE_ANIMAL, 1, TRUE, TRUE )
									TASK_COMBAT_PED( TestPeds[0].Ped, PLAYER_PED_ID() )
								ENDIF	
								SCENARIO_STATE = STATE_1								
							BREAK
						ENDSWITCH
					BREAK
					
					CASE TEST_TASK_ARMED_MELEE_IN_COVER
						SWITCH SCENARIO_STATE
							CASE STATE_INIT
								IF NOT IS_ENTITY_DEAD( TestPeds[0].Ped ) AND NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
									SET_PED_RELATIONSHIP_GROUP_HASH( TestPeds[0].Ped, RELGROUPHASH_SECURITY_GUARD )
									GIVE_WEAPON_TO_PED( PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 2000, TRUE, TRUE )
								ENDIF
								SCENARIO_STATE = STATE_1
							BREAK	
						ENDSWITCH
					BREAK
					
                ENDSWITCH   


ENDPROC


//Purpose: Runs the main scenarios
PROC Run_Test_Scenario ()
    int index = 0
    
    SWITCH TestScenarioAStatus
        //setps up all the data for the sceanrio, this is only called once or if selected in widget
        CASE InitialiseScenarioData
            PlayerStartPos = GET_PLAYER_START_POS()         //sets the players start coords at the default

            Bscenario_running = FALSE
            INITIALISE_PED_DATA (Testpeds)
            INITIALISE_VEHICLE_DATA (TestVehicles)
            INITIALISE_CAM_DATA(TestCams )
            INITIALISE_OBJECT_DATA(TestObjects)
            SETUP_TEST_DATA ()
    
            HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
            TestScenarioAStatus = CreateScenarioEntities      
			
            Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)     //sets the 
        
        BREAK
        
        // Creates all the scenario data
        CASE CreateScenarioEntities
            Bscenario_running = FALSE
            
            Set_Gang_Relationships (TRUE)   
        
            Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
        
            //peds
            FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
                CREATE_PED_ENTITY(Testpeds[index])
                GiveBlipsToPedsGrps (Testpeds[index])
                ALTER_COMBAT_STATS (Testpeds[index] )
                SWAP_PED_WEAPONS (Testpeds[index])
               
                Block_Peds_Temp_Events (Testpeds[index], TRUE)
                SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
            ENDFOR
            
            //TestVehicles
            FOR Index = 0 to MAX_NUMBER_OF_VEHICLES -1
                CREATE_VEHICLE_ENTITY (TestVehicles[index] )
            ENDFOR
        
            //TestCams
            for index = 0 to MAX_NUMBER_OF_CAMERAS -1
                CREATE_CAM_ENTITY (TestCams[index])
            ENDFOR
            
            //TestObjects
            FOR Index = 0 to MAX_NUMBER_OF_OBJECTS -1
                CREATE_OBJECT_ENTITY (TestObjects[index])
            ENDFOR
            
            TestScenarioAStatus = SetScenarioEntities
        BREAK
        
        CASE SetScenarioEntities
            
            Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
			
            Start_And_Reset_Test ()
            
            IF gBeginCombatScenario
                TestScenarioAStatus = RunScenario
                gBeginCombatScenario = FALSE
                gRun_debuggig = FALSE
                Bscenario_running = TRUE
                INITALISE_TEST_STATE()
                TEMP_ACTIVATE_CAMS (TestCams[FixedCamera].cam )
                
                IF DOES_CAM_EXIST (TestCams[FixedCamera].cam )
                    HELP_TEXT_STATE =  HTF_SCENARIO_RUNNING_FIXED_CAM
                ELSE
                    HELP_TEXT_STATE = HTF_STARTED_RUNNING_NO_CAMS
                ENDIF
            ENDIF
        BREAK
        
        //Runs the actual selected scenario
        
        CASE RunScenario
                
            Temp_Run_Scenario_Tracking_Cam (TestPeds, MAX_NUMBER_OF_PEDS, TestCams[TrackingCamera].cam) 
            
            Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
                
            Start_And_Reset_Test ()
                
            Check_For_Scenario_Reset ()
            
            IF DOES_CAM_EXIST (TestCams[TrackingCamera].cam)
                if IS_CAM_RENDERING (TestCams[TrackingCamera].cam)
                    HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_TRACKING_CAM
                ELSE
                    HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_FIXED_CAM
                ENDIF
            ENDIF
                    
            RUN_TEST ()     //run the main tests        
            
        BREAK 
                

        CASE CleanupScenario
            
            HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
            
            Cleanup_Scenario_Entities ()
			
			PREFERRED_COVER_STATE = PREFERRED_COVER_INIT
            
            IF gResetToDefault  
                Temp_cleanup_scenario_cams ()       //here we are changing scenarios so we need to reset cams
                TestScenarioAStatus = InitialiseScenarioData
                gResetToDefault = FALSE
            ENDIF
        
            IF gResetCombatScenario
                Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)
                gcurrentselection = gSelection 
                IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
                    ACTIVATE_CAM (TestCams[FixedCamera].cam)
                ELSE
                    Temp_cleanup_scenario_cams ()   
                ENDIF
                TestScenarioAStatus = CreateScenarioEntities
                gResetCombatScenario = FALSE
            ENDIF
        
        BREAK
        
    ENDSWITCH
ENDPROC

SCRIPT
    
    gvMapOffset = GET_PLAYER_START_POS ()
    
    SET_DEBUG_ACTIVE (TRUE)
    
    SETUP_MISSION_XML_MENU (XMLMenu, KEY_Q )
    
    SETUP_AREA_FOR_MISSION (<<0.0, 0.0, 0.0>>)
    
    //Gets a reference to the player
    Get_The_Player ()
    
    CREATE_TEST_WIDGET ()
    
    //set the player ready for the script
    SET_PLAYER_COLISION(scplayer, true)
    
    WHILE TRUE
    
        // controls the help text hides if xml menu is active
        TEXT_CONTROLLER ()
    
        // Can set all scenario peds invincible from the widget
        Set_Scenario_Peds_Invincible (TestPeds, MAX_NUMBER_OF_PEDS, gsetpedsinvincible )
    
        //User can create a debug cam for setting sceanrios
        Temp_Create_Debug_Cam (TestCams[FixedCamera]) 
        
        //Runs the selected option from the XML menu 
        Run_Selection_From_XML_input ()
        
        //Checks that a valid selection has been input and runs the scenario
        IF (gcurrentselection <> InvalidSelection)
    
            Draw_Debug_Info (  )
            
            //Sets the test scenario into debug mode
            Set_To_Debug ()
            
            if (gRun_debuggig)
                Temp_Debug_Scenario (TestPeds, TestVehicles, Route, TestCams[FixedCamera].cam, S_M_Y_COP_01)                              //Allows the entities in the scneario to be adjusted
                Print_Scenario_Data (TestPeds, TestVehicles, Route, TestCams)
            ENDIF
            
            Run_Test_Scenario ()                                
            
            SWITCH_BETWEEN_FIXED_AND_TRACKING (TestCams[FixedCamera].cam)
            
            PRINT_ACTIVE_TEST (Bscenario_running,gRun_debuggig)     
        ENDIF
        
        Terminate_test_script ()
        
        WAIT (0)
    
    
    ENDWHILE

ENDSCRIPT


#ENDIF  // IS_DEBUG_BUILD

