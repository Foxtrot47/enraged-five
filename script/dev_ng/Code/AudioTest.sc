// Do nothing in release mode

#IF IS_FINAL_BUILD
	SCRIPT
	ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD



//???????????????????????????????????????????????????????????????????????????????
//?				Author:  GEORGE				Date: 05/02/10						?
//???????????????????????????????????????????????????????????????????????????????
//?																				?
//?						     Various audio tests!								?
//?																				?
//?																				?
//???????????????????????????????????????????????????????????????????????????????

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "commands_weapon.sch"
USING "commands_fire.sch"
USING "flow_public_game.sch"

//USING "flow_public_core_override.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "selector_public.sch"
USING "player_ped_public.sch"
USING "flow_public_game.sch"

//Variables etc

OBJECT_INDEX obj_cursor
MODEL_NAMES model_cursor = PROP_POOL_BALL_01
MODEL_NAMES model_shooter = A_M_Y_MUSCLBEAC_01
MODEL_NAMES model_shooter_last = A_M_Y_MUSCLBEAC_01
WEAPON_TYPE wt_shooterGun

WIDGET_GROUP_ID widget_debug
WIDGET_GROUP_ID widget_settings
WIDGET_GROUP_ID widgets_9 //for conversation


TEXT_WIDGET_ID widget_soundSet_name
TEXT_WIDGET_ID widget_sound_name
TEXT_WIDGET_ID widget_circle_sound_name
TEXT_WIDGET_ID widget_square_sound_name
TEXT_WIDGET_ID widget_triangle_sound_name
TEXT_WIDGET_ID widget_cross_sound_name
TEXT_WIDGET_ID widget_bank_name
TEXT_WIDGET_ID widget_bank_name_1
TEXT_WIDGET_ID widget_bank_name_2
TEXT_WIDGET_ID widget_bank_name_3
TEXT_WIDGET_ID widget_bank_name_4
TEXT_WIDGET_ID widget_bank_name_5
TEXT_WIDGET_ID widget_variable_name
TEXT_WIDGET_ID widget_scanner_scripted_report_name
TEXT_WIDGET_ID widget_scanner_scripted_report_name_2
TEXT_WIDGET_ID widget_scanner_audio_scene_name
TEXT_WIDGET_ID widget_dynamic_mixer
TEXT_WIDGET_ID widget_dynamic_mixer_variable_name
TEXT_WIDGET_ID widget_dynamic_mixer_group_name
TEXT_WIDGET_ID widget_script_stream_name
TEXT_WIDGET_ID widget_zonelist_name


PED_INDEX pi_shooter_01
BLIP_INDEX bi_shooter_01
BLIP_INDEX bi_sound_source_blip

//conversation test widgets
TEXT_WIDGET_ID widget_conversation_name
TEXT_WIDGET_ID widget_conversation_text_block_name
TEXT_WIDGET_ID widget_voice0_name
TEXT_WIDGET_ID widget_voice1_name
TEXT_WIDGET_ID widget_voice2_name
TEXT_WIDGET_ID widget_voice3_name
TEXT_WIDGET_ID widget_voice4_name
TEXT_WIDGET_ID widget_voice5_name
TEXT_WIDGET_ID widget_voice6_name
TEXT_WIDGET_ID widget_voice7_name
TEXT_WIDGET_ID widget_voice8_name
BOOL b_start_conversation = FALSE
BOOL b_stop_conversation = FALSE
BOOL b_speakers_made = FALSE

//Additional widgets for testing anim linked convos
INT i_ped_number
BOOL b_LoadAnims = TRUE
TEXT_WIDGET_ID widget_anim_name
TEXT_WIDGET_ID widget_anim_name2
TEXT_WIDGET_ID widget_anim_dictionary_name
TEXT_WIDGET_ID widget_anim_dictionary_name_2
TEXT_LABEL_63 s_anim_name
TEXT_LABEL_63 s_anim_dict_name
BOOL b_anim_HaveAllAnimsLoaded = FALSE
INT i_animCurrentChunk = 0
INT i_anim_load_stage = 0
INT i_numberOfConversationChunks = 5


//Conversation Testing
CONST_INT i_numberOfSpeakers 9
PED_INDEX pi_Speakers[i_numberOfSpeakers]
structPedsForConversation conversationPedStruct
FLOAT f_speaker_min_dist = 2.0
FLOAT f_speaker_max_dist = 5.0

BOOL b_doCustomFunction = FALSE
float f_CustomFunction = 0.0


//ZONELIST VARS
STRING s_zoneListName = "null"
INT i_zoneListFunction = 1
BOOL b_zoneListApply = FALSE
BOOL b_zoneForceChange = FALSE


BOOL b_playSound = TRUE
BOOL b_lockControls = TRUE
BOOL b_reloadBank = FALSE
BOOL b_releaseBank = FALSE
BOOL b_retriggerSound = FALSE
BOOL b_freezeSound = FALSE
BOOL b_yInheritX = FALSE
BOOL b_shooter_fire = TRUE
BOOL b_shooter_respot = FALSE
BOOL b_shooter_ready = FALSE
BOOL b_shooter_respawning = FALSE
BOOL b_playSoundFrontend = FALSE
BOOL b_weaponsAllowed[24]
BOOL b_bank_test_update_debug_var = FALSE
BOOL b_bank_test_up_dbg_var_on_start = FALSE
BOOL b_bank_test_controls_locked = FALSE

STRING s_sound_name

//BANKTEST STRINGS
BOOL b_setScriptCleanupTimeout = FALSE
INT i_scriptCleanupTimeout = 0
BOOL b_use_SoundSet
BOOL b_hint_banks
int i_bank_to_release = -1
STRING s_soundSet_name
STRING s_circle_sound_name
STRING s_square_sound_name
STRING s_triangle_sound_name
STRING s_cross_sound_name
STRING s_variable_name

TEXT_LABEL_63 s_bank_name
TEXT_LABEL_63 s_bank_name_2
TEXT_LABEL_63 s_bank_name_3
TEXT_LABEL_63 s_bank_name_4
TEXT_LABEL_63 s_bank_name_5

int i_loadTries = 0

VECTOR v_player_pos 
VECTOR v_sound_pos

INT LSX
INT LSY
INT RSX
INT RSY

INT i_testSoundId0 = GET_SOUND_ID()
INT i_testSoundId1 = GET_SOUND_ID()
INT i_testSoundId2 = GET_SOUND_ID()
INT i_testSoundId3 = GET_SOUND_ID()
INT i_debug_sound_type
INT i_test_mode	=	5
INT i_test_mode_last_frame = i_test_mode
PTFX_ID i_sparklyEffect 
INT i_freezeTime
INT i_shooter_gun_type	=	0
INT i_GunManRespawnTime = 5
INT i_GunManTimeToNewTarget = 10
INT i
INT i_model_number = 0
INT i_bankTestPlayFrom = 0
INT i_shoot_mode = 0
INT i_shoot_rate = 100

FLOAT f_shooter_burst_pause	= 1.0
FLOAT f_shooter_burst_length	= 3.0
FLOAT f_shooter_min_dist	=	5.0
FLOAT f_shooter_max_dist	=	15.0
FLOAT f_shooter_aim_offset	=	1.0
FLOAT f_widget_product = -1
FLOAT f_debug_sound_variable				= 1.0
FLOAT f_debug_sound_variable_last			= 0.0
FLOAT f_debug_sound_variable_min				= 0.0
FLOAT f_debug_sound_variable_max				= 1.0

FLOAT f_debug_sound_cycle_time				= 10.0
FLOAT f_debug_sound_retrigger_time				= 5.0
FLOAT f_debug_sound_X			= 10.0
FLOAT f_debug_sound_Y			= 10.0
FLOAT f_debug_sound_Z			= 0.0
FLOAT f_debug_sound_audio_scene_wet			= 0.0
FLOAT f_debug_sound_audio_scene_wet_old = 0.0


CONST_INT i_numberOfGunMen 10

SEQUENCE_INDEX si_shooter_tasks
PED_INDEX pi_GunMen[i_numberOfGunMen]
BLIP_INDEX bi_GunMen[i_numberOfGunMen]
BOOL b_GunMen_armed[i_numberOfGunMen]

INT i_currentGunMan

INT i_crimeNumber = 0 //Crime number for passing to vigilante report triggering
INT i_crimeValue = 0 //Crime Value (?) for passing to vigilante report triggering 

//ALARM TESTING
BOOL b_alarmsOn = FALSE
BOOL b_alarmsArePlaying = FALSE //For testing function
BOOL b_alarmsPlaying = FALSE
BOOL b_alarmsSkipToSustain = FALSE
BOOL b_alarmsForceState = FALSE
STRING s_alarmName
TEXT_WIDGET_ID widget_alarm_name

//SCANNER UNIT BITS
BOOL b_updateScannerUnit = FALSE //Bool to control report triggering	

BOOL b_toggleDisableScanner = FALSE 
BOOL b_disableScanner = FALSE
BOOL b_toggleScannerScriptedOnly = FALSE 
BOOL b_scannerScriptedOnly = FALSE

// INT i_scannerUnitDivision = 0  //Division removed.
INT i_scannerUnitType = 0
INT i_scannerUnitBeat = 0

//Stuff for testing glas
BOOL b_glassToggle = FALSE
BOOL b_glassIsOn = FALSE
FLOAT f_glassRadius = 10

//Script-set position for scanner lines
BOOL b_updateScannerPos = FALSE	
FLOAT f_scannerPosX = 0.0
FLOAT f_scannerPosY = 0.0
FLOAT f_scannerPosZ = 0.0
BOOL b_updateScannerCrime = FALSE

BOOL b_emitterOn = FALSE
BOOL b_emitterStatus = FALSE
BOOL b_emitterLinkToProp = FALSE
BOOL b_emitterSpawnProp = FALSE
BOOL b_emitterToggleScore = FALSE
bool b_emitterRetune = FALSE
TEXT_WIDGET_ID widget_emitter_name
TEXT_WIDGET_ID widget_station_name
TEXT_WIDGET_ID widget_music_name
OBJECT_INDEX obj_boombox


STRING s_scannerAudioScene //For crime name widget
BOOL b_setScannerAudioScene
BOOL b_unsetScannerAudioScene
FLOAT f_scannerApplyValue = 0.0

BOOL b_playScannerScriptedLine = FALSE //Bool to control report triggering	
BOOL b_scannerAutoInfo = FALSE //Bool to decide to send all info
BOOL b_reportCrime = FALSE //Bool to control report triggering	
BOOL b_playVigilanteCrime = FALSE //Bool to control report triggering	
//STRING s_scannerScriptedCrime //For crime name widget
TEXT_LABEL_63 s_scannerScriptedCrime
FLOAT f_crimeX = -415.0	//Crime Coords
FLOAT f_crimeY = -535.0	//Crime Coords
FLOAT f_crimeZ = 40.0	//Crime Coords

BOOL b_loadYankton = FALSE 
BOOL b_yanktonLoaded = FALSE

BOOL b_dynamicMixerStart = FALSE
BOOL b_dynamicMixerTestGroups = FALSE
BOOL b_dynamicMixerAddVehicleToMixGroup = FALSE 
BOOL b_dynamicMixerRemoveVehicleFromMixGroup = FALSE
BOOL b_dynamicMixerStop = FALSE 
BOOL b_dynamicMixerUseVariable = FALSE //Don't apply initially
BOOL b_dynamicMixerApplyWithPad = FALSE //Don't apply initially
STRING s_dynamicMixerName = "DefaultStringValue"
STRING s_lastDynamicMixerName  = "DefaultStringValue2"
STRING s_dynamicMixerVariableName = "DefaultVaribleStringValue"
FLOAT f_dynamicMixerVariableValue = 0

STRING s_scriptedStreamName
BOOL b_overrideUnderwaterStream = FALSE
BOOL b_overrideUnderwaterStreamApplied = FALSE
BOOL b_streamIsPlaying = FALSE
BOOL b_loadStream = FALSE
BOOL b_playStream = FALSE
BOOL b_stopStream = FALSE
INT i_streamplaybackMode = 0
INT i_streamLoadTryCount = 0
INT i_streamLoadTryCountLimit = 100

//Walla Debug
FLOAT f_walla_density
FLOAT f_walla_factor
BOOL b_override_walla = FALSE

		
//WIDGETS
PROC CREATE_MAIN_WIDGET_GROUP()
	widget_debug = START_WIDGET_GROUP("Audio Test")
		START_WIDGET_GROUP("GLOBAL BITS")
			ADD_WIDGET_BOOL("Do Custom Function",b_doCustomFunction)
			ADD_WIDGET_FLOAT_SLIDER("Custom Float", f_CustomFunction, 0.0, 1.0, 0.01)
			ADD_WIDGET_BOOL("Load Yankton",b_loadYankton)
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("Sound Panner")
				ADD_TO_WIDGET_COMBO("Single Shooter")			
				ADD_TO_WIDGET_COMBO("Multiple Shooters")			
				ADD_TO_WIDGET_COMBO("Banked Sound Tester")			
				ADD_TO_WIDGET_COMBO("Police Scanner Tester")			
				ADD_TO_WIDGET_COMBO("Audio Scene tester")			
				ADD_TO_WIDGET_COMBO("Scripted Stream Tester")
				ADD_TO_WIDGET_COMBO("Walla Testing")
				ADD_TO_WIDGET_COMBO("AmbientZoneList Toggling")
				ADD_TO_WIDGET_COMBO("Conversation Tester")
				ADD_TO_WIDGET_COMBO("Anim linked conversation Tester")
				ADD_TO_WIDGET_COMBO("Glass Feet")
				ADD_TO_WIDGET_COMBO("Alarms")
				ADD_TO_WIDGET_COMBO("Static Emitters")
			STOP_WIDGET_COMBO("Test Mode", i_test_mode)
		STOP_WIDGET_GROUP()
	
		//Dynamic widgets
		widget_settings = START_WIDGET_GROUP("Settings")
		STOP_WIDGET_GROUP()		
	
	STOP_WIDGET_GROUP()		
ENDPROC
		
PROC CREATE_WIDGETS()
	SET_CURRENT_WIDGET_GROUP(widget_settings)
	SWITCH i_test_mode
		CASE 0

				widgets_9 = START_WIDGET_GROUP("Sound Panner")
					ADD_WIDGET_FLOAT_SLIDER("Sound Emitter Cycle Time", f_debug_sound_cycle_time, 0.0, 50.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Sound Emitter Retrigger Time", f_debug_sound_retrigger_time, 0.0, 50.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Sound Emitter X Distance", f_debug_sound_X, 0.0, 500.0, 1)
					ADD_WIDGET_FLOAT_SLIDER("Sound Emitter Y Distance", f_debug_sound_Y, 0.0, 500.0, 1)
					ADD_WIDGET_FLOAT_SLIDER("Sound Emitter Z Distance", f_debug_sound_Z, 0.0, 500.0, 1)
					ADD_WIDGET_FLOAT_SLIDER("Rolloff Scaling", f_debug_sound_audio_scene_wet, 0.0, 1.0, 0.01)
					ADD_WIDGET_BOOL("Y Inherit from X",b_yInheritX)
					ADD_WIDGET_BOOL("Retrigger Sound",b_retriggerSound)
					ADD_WIDGET_BOOL("Sound On",b_playSound)
					ADD_WIDGET_BOOL("Freeze Sound Position",b_freezeSound)
					widget_sound_name = ADD_TEXT_WIDGET("SOUND_NAME")
					SET_CONTENTS_OF_TEXT_WIDGET(widget_sound_name,"PANNING_TEST")
				
					START_NEW_WIDGET_COMBO()
						ADD_TO_WIDGET_COMBO("TYPE_CIRCLE")
						ADD_TO_WIDGET_COMBO("TYPE_FLYBY")			
						ADD_TO_WIDGET_COMBO("TYPE_MANUAL_POS")
						ADD_TO_WIDGET_COMBO("TYPE_RANDOM_POS")
					STOP_WIDGET_COMBO("Sound Model Type", i_debug_sound_type)
				STOP_WIDGET_GROUP()

		BREAK
		CASE 1

			widgets_9 = START_WIDGET_GROUP("Shooters")
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("A_M_Y_MUSCLBEAC_01")
					ADD_TO_WIDGET_COMBO("A_M_M_Business_01")
					ADD_TO_WIDGET_COMBO("G_M_Y_MexGoon_02")
					ADD_TO_WIDGET_COMBO("A_F_M_BUSINESS01")
					ADD_TO_WIDGET_COMBO("A_F_Y_BEACH_01")
					ADD_TO_WIDGET_COMBO("A_F_M_BevHills_02")
					ADD_TO_WIDGET_COMBO("A_M_Y_CYCLIST_01")
					ADD_TO_WIDGET_COMBO("S_M_Y_COP_01")
					ADD_TO_WIDGET_COMBO("S_F_Y_HOOKER_01")
					ADD_TO_WIDGET_COMBO("S_M_Y_MARINE_01")
					ADD_TO_WIDGET_COMBO("S_M_Y_SWAT_01")
					ADD_TO_WIDGET_COMBO("S_M_Y_SWAT_01")
				STOP_WIDGET_COMBO("Enemy Ped", i_model_number)
				ADD_WIDGET_FLOAT_SLIDER("Min Distance", f_shooter_min_dist, 0.0, 200.0, 1)
				ADD_WIDGET_FLOAT_SLIDER("Max Distance", f_shooter_max_dist, 0.0, 200.0, 1)

				START_WIDGET_GROUP("Single Shooter")
					ADD_WIDGET_BOOL("Fire!",b_shooter_fire)
					ADD_WIDGET_BOOL("Respot",b_shooter_respot)
					ADD_WIDGET_INT_SLIDER("Shoot Rate",i_shoot_rate ,0,100,1)
					ADD_WIDGET_FLOAT_SLIDER("Player aim offset", f_shooter_aim_offset, 0.0, 200.0, 1)
					ADD_WIDGET_FLOAT_SLIDER("Shooter Burst Length", f_shooter_burst_length, 0.0, 20.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Shooter Burst Pause", f_shooter_burst_pause, 0.0, 10.0, 0.1)
					//FIRING TYPE
					START_NEW_WIDGET_COMBO()
						ADD_TO_WIDGET_COMBO("FIRING_TYPE_1_BURST")
						ADD_TO_WIDGET_COMBO("FIRING_TYPE_1_THEN_AIM")
						ADD_TO_WIDGET_COMBO("FIRING_TYPE_CLIP")
						ADD_TO_WIDGET_COMBO("FIRING_TYPE_CONTINUOUS")
						ADD_TO_WIDGET_COMBO("FIRING_TYPE_RANDOM_BURSTS")
						ADD_TO_WIDGET_COMBO("AT PLAYER CLIP")
						ADD_TO_WIDGET_COMBO("AT PLAYER CONTINUOUS")
					STOP_WIDGET_COMBO("Firing Type", i_shoot_mode)

					START_NEW_WIDGET_COMBO()
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_PISTOL")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_COMBATPISTOL")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_APPISTOL")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_MICROSMG")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_SMG")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_ASSAULTRIFLE")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_CARBINERIFLE")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_ADVANCEDRIFLE")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_MG")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_COMBATMG")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_PUMPSHOTGUN")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_SAWNOFFSHOTGUN")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_ASSAULTSHOTGUN")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_SNIPERRIFLE")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_HEAVYSNIPER")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_REMOTESNIPER")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_GRENADELAUNCHER")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_RPG")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_MINIGUN")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_GRENADE")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_SMOKEGRENADE")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_STICKYBOMB")
						ADD_TO_WIDGET_COMBO("WEAPONTYPE_STUNGUN")
					STOP_WIDGET_COMBO("Gun Type", i_shooter_gun_type)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()

		BREAK
		CASE 2

			widgets_9 = START_WIDGET_GROUP("Shooters")
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("A_M_Y_MUSCLBEAC_01")
					ADD_TO_WIDGET_COMBO("A_M_M_Business_01")
					ADD_TO_WIDGET_COMBO("G_M_Y_MexGoon_02")
					ADD_TO_WIDGET_COMBO("A_F_M_BUSINESS01")
					ADD_TO_WIDGET_COMBO("A_F_Y_BEACH_01")
					ADD_TO_WIDGET_COMBO("A_F_M_BevHills_02")
					ADD_TO_WIDGET_COMBO("A_M_Y_CYCLIST_01")
					ADD_TO_WIDGET_COMBO("S_M_Y_COP_01")
					ADD_TO_WIDGET_COMBO("S_F_Y_HOOKER_01")
					ADD_TO_WIDGET_COMBO("S_M_Y_MARINE_01")
					ADD_TO_WIDGET_COMBO("S_M_Y_SWAT_01")
					ADD_TO_WIDGET_COMBO("S_M_Y_SWAT_01")
				STOP_WIDGET_COMBO("Enemy Ped", i_model_number)
				ADD_WIDGET_FLOAT_SLIDER("Min Distance", f_shooter_min_dist, 0.0, 200.0, 1)
				ADD_WIDGET_FLOAT_SLIDER("Max Distance", f_shooter_max_dist, 0.0, 200.0, 1)
				START_WIDGET_GROUP("Multiple Shooters")
					ADD_WIDGET_BOOL("Timed Respawns", b_shooter_respawning)
					ADD_WIDGET_INT_SLIDER("Respawn Time", i_GunManRespawnTime, 1, 20, 1)
					ADD_WIDGET_INT_SLIDER("Time before target change", i_GunManTimeToNewTarget, 1, 20, 1)
						ADD_WIDGET_BOOL("WEAPONTYPE_PISTOL",b_weaponsAllowed[0])
						ADD_WIDGET_BOOL("WEAPONTYPE_COMBATPISTOL",b_weaponsAllowed[1])
						ADD_WIDGET_BOOL("WEAPONTYPE_APPISTOL",b_weaponsAllowed[2])
						ADD_WIDGET_BOOL("WEAPONTYPE_MICROSMG",b_weaponsAllowed[3])
						ADD_WIDGET_BOOL("WEAPONTYPE_SMG",b_weaponsAllowed[4])
						ADD_WIDGET_BOOL("WEAPONTYPE_ASSAULTRIFLE",b_weaponsAllowed[5])
						ADD_WIDGET_BOOL("WEAPONTYPE_CARBINERIFLE",b_weaponsAllowed[6])
						ADD_WIDGET_BOOL("WEAPONTYPE_ADVANCEDRIFLE",b_weaponsAllowed[7])
						ADD_WIDGET_BOOL("WEAPONTYPE_MG",b_weaponsAllowed[8])
						ADD_WIDGET_BOOL("WEAPONTYPE_COMBATMG",b_weaponsAllowed[9])
						ADD_WIDGET_BOOL("WEAPONTYPE_PUMPSHOTGUN",b_weaponsAllowed[10])
						ADD_WIDGET_BOOL("WEAPONTYPE_SAWNOFFSHOTGUN",b_weaponsAllowed[11])
						ADD_WIDGET_BOOL("WEAPONTYPE_ASSAULTSHOTGUN",b_weaponsAllowed[12])
						ADD_WIDGET_BOOL("WEAPONTYPE_SNIPERRIFLE",b_weaponsAllowed[13])
						ADD_WIDGET_BOOL("WEAPONTYPE_HEAVYSNIPER",b_weaponsAllowed[14])
						ADD_WIDGET_BOOL("WEAPONTYPE_REMOTESNIPER",b_weaponsAllowed[15])
						ADD_WIDGET_BOOL("WEAPONTYPE_GRENADELAUNCHER",b_weaponsAllowed[16])
						ADD_WIDGET_BOOL("WEAPONTYPE_RPG",b_weaponsAllowed[17])
						ADD_WIDGET_BOOL("WEAPONTYPE_MINIGUN",b_weaponsAllowed[18])
						ADD_WIDGET_BOOL("WEAPONTYPE_GRENADE",b_weaponsAllowed[19])
						ADD_WIDGET_BOOL("WEAPONTYPE_SMOKEGRENADE",b_weaponsAllowed[20])
						ADD_WIDGET_BOOL("WEAPONTYPE_STICKYBOMB",b_weaponsAllowed[21])
						ADD_WIDGET_BOOL("WEAPONTYPE_STUNGUN",b_weaponsAllowed[22])
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()

		BREAK
		CASE 3

			widgets_9 = START_WIDGET_GROUP("Bank Sound Test")
				
				ADD_WIDGET_BOOL("Use SoundSets",b_use_SoundSet)
				widget_soundSet_name = ADD_TEXT_WIDGET("SoundSet Name")
				ADD_WIDGET_BOOL("Apply Script Cleanup Timeout",b_setScriptCleanupTimeout)
				ADD_WIDGET_INT_SLIDER("Script Cleanup Timeout",i_scriptCleanupTimeout,0,10000,1)

				
				
				ADD_WIDGET_STRING("Audio bank names - note that the first too boxes are combined for long names")
				widget_bank_name = ADD_TEXT_WIDGET("BANK NAME 0")
				widget_bank_name_1 = ADD_TEXT_WIDGET("BANK NAME 0 pt 2")
				widget_bank_name_2 = ADD_TEXT_WIDGET("BANK NAME 1")
				widget_bank_name_3 = ADD_TEXT_WIDGET("BANK NAME 2")
				widget_bank_name_4 = ADD_TEXT_WIDGET("BANK NAME 3")
				widget_bank_name_5 = ADD_TEXT_WIDGET("BANK NAME 4")
				ADD_WIDGET_INT_SLIDER("Bank To Release (-1 = all)",i_bank_to_release,-1,4,1)
				
				ADD_WIDGET_BOOL("Lock controls",b_lockControls)
				ADD_WIDGET_BOOL("Reload Banks",b_reloadBank)
				ADD_WIDGET_BOOL("Hint Banks",b_hint_banks)
				ADD_WIDGET_BOOL("Release Banks",b_releaseBank)
				widget_variable_name = ADD_TEXT_WIDGET("Variable Name")
				ADD_WIDGET_STRING("Control variable value using y-axis of left analogue stick on pad 1 - interpolates between:")
				ADD_WIDGET_FLOAT_SLIDER("Variable Value Min", f_debug_sound_variable_min, -100.0, 100.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Variable Value Max", f_debug_sound_variable_max, -100.0, 100.0, 0.01)
				ADD_WIDGET_BOOL("Update Variable Value every frame",b_bank_test_update_debug_var)
				ADD_WIDGET_BOOL("Update Variable Value on start",b_bank_test_up_dbg_var_on_start)
				widget_circle_sound_name = ADD_TEXT_WIDGET("CIRCLE SOUND")
				widget_square_sound_name= ADD_TEXT_WIDGET("SQUARE SOUND")
				widget_cross_sound_name = ADD_TEXT_WIDGET("CROSS SOUND")
				widget_triangle_sound_name = ADD_TEXT_WIDGET("TRIANGLE SOUND")
				ADD_WIDGET_BOOL("Frontend",b_playSoundFrontend)
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("Player")
					ADD_TO_WIDGET_COMBO("Player Car")
					ADD_TO_WIDGET_COMBO("Frontend")
					ADD_TO_WIDGET_COMBO("Cam Coords")
				STOP_WIDGET_COMBO("Play from:", i_bankTestPlayFrom)
			STOP_WIDGET_GROUP()

		BREAK
		CASE 4

			widgets_9 = START_WIDGET_GROUP("Police Scanner Test")
				widget_scanner_scripted_report_name = ADD_TEXT_WIDGET("Scripted Report Name")
				widget_scanner_scripted_report_name_2 = ADD_TEXT_WIDGET("Scripted Report Name (overflow)")
				
				ADD_WIDGET_BOOL("Disable scanner",b_toggleDisableScanner)
				ADD_WIDGET_BOOL("Scripted Scanner only",b_toggleScannerScriptedOnly)
						

				
				ADD_WIDGET_BOOL("Play Scripted Scanner Line",b_playScannerScriptedLine)
				ADD_WIDGET_BOOL("Auto-set info",b_scannerAutoInfo)
				ADD_WIDGET_BOOL("Update Unit Name",b_updateScannerUnit)
				//ADD_WIDGET_INT_SLIDER("Division",i_scannerUnitDivision,0,9,1)
				ADD_WIDGET_INT_SLIDER("Type",i_scannerUnitType,0,25,1)
				ADD_WIDGET_INT_SLIDER("Beat",i_scannerUnitBeat,0,9,1)
				ADD_WIDGET_BOOL("Update Position",b_updateScannerPos)
				ADD_WIDGET_FLOAT_SLIDER("x",f_scannerPosX,-4000.0,4000.0,1.0)
				ADD_WIDGET_FLOAT_SLIDER("y",f_scannerPosY,-4000.0,4000.0,1.0)
				ADD_WIDGET_FLOAT_SLIDER("z",f_scannerPosZ,-4000.0,4000.0,1.0)
				ADD_WIDGET_BOOL("Update Scanner Crime",b_updateScannerCrime)
				ADD_WIDGET_INT_SLIDER("Crime number (from enum)", i_crimeNumber, 0, 35, 1)
				widget_scanner_audio_scene_name = ADD_TEXT_WIDGET("Scanner Audio Scene")
				ADD_WIDGET_BOOL("Set Audio Scene",b_setScannerAudioScene)
				ADD_WIDGET_BOOL("Remove Audio Scene",b_unsetScannerAudioScene)
				ADD_WIDGET_FLOAT_SLIDER("Scene Apply",f_scannerApplyValue,0.0,1.0,0.01)
				START_WIDGET_GROUP("CRIMES AND VIGILANTE")
					ADD_WIDGET_BOOL("Play Vigilante Crime",b_playVigilanteCrime)
					ADD_WIDGET_BOOL("Report Crime",b_reportCrime)
					ADD_WIDGET_FLOAT_SLIDER("XCoord",f_crimeX,-5000,5000,10)
					ADD_WIDGET_FLOAT_SLIDER("YCoord",f_crimeY,-5000,5000,10)
					ADD_WIDGET_FLOAT_SLIDER("ZCoord",f_crimeZ,-100,1000,10)
					ADD_WIDGET_INT_SLIDER("Crime Value???", i_crimeValue, 0, 128, 1)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()

		BREAK
		CASE 5

			widgets_9 = START_WIDGET_GROUP("Audio Scene Tester")
				widget_dynamic_mixer = ADD_TEXT_WIDGET("Scene Name")
				ADD_WIDGET_BOOL("Start Scene",b_dynamicMixerStart)
				ADD_WIDGET_BOOL("Stop Scene",b_dynamicMixerStop)
				ADD_WIDGET_BOOL("Apply Variable",b_dynamicMixerUseVariable)
				ADD_WIDGET_BOOL("Pad Control",b_dynamicMixerApplyWithPad)
				ADD_WIDGET_BOOL("Test Mix Groups",b_dynamicMixerTestGroups)
				widget_dynamic_mixer_group_name = ADD_TEXT_WIDGET("Mix Group Name")
				ADD_WIDGET_BOOL("Add Vehicle to Group",b_dynamicMixerAddVehicleToMixGroup)
				ADD_WIDGET_BOOL("Remove Vehicle to Group",b_dynamicMixerRemoveVehicleFromMixGroup)
				widget_dynamic_mixer_variable_name = ADD_TEXT_WIDGET("Variable Name")
				ADD_WIDGET_FLOAT_SLIDER("Variable Value",f_dynamicMixerVariableValue,0,1,0.01)
			STOP_WIDGET_GROUP()
	
		BREAK
		CASE 6			

			widgets_9 = START_WIDGET_GROUP("Scripted Stream Tester")
				widget_script_stream_name = ADD_TEXT_WIDGET("Stream Name")
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("Player")
					ADD_TO_WIDGET_COMBO("Player Pos")
					ADD_TO_WIDGET_COMBO("Nearest Vehicle")		
					ADD_TO_WIDGET_COMBO("Frontend")			
				STOP_WIDGET_COMBO("Play from:", i_streamplaybackMode)
				ADD_WIDGET_BOOL("Use SoundSets",b_use_SoundSet)
				widget_soundSet_name = ADD_TEXT_WIDGET("SoundSet Name")
				ADD_WIDGET_BOOL("Load Stream",b_loadStream)
				ADD_WIDGET_BOOL("Play Stream",b_playStream)
				ADD_WIDGET_BOOL("Stop Stream",b_stopStream)
				ADD_WIDGET_STRING("READ ONLY - SHOULD BE TRUE WHEN SCRIPTED STREAM IS PLAYING!")
				ADD_WIDGET_BOOL("StreamIsPlaying",b_streamIsPlaying)
								
				//VARIABLE CONTROL BITS
				widget_variable_name = ADD_TEXT_WIDGET("Variable Name")
				ADD_WIDGET_STRING("Control variable value using y-axis of left analogue stick on pad 1 - interpolates between:")
				ADD_WIDGET_FLOAT_SLIDER("Variable Value Min", f_debug_sound_variable_min, -100.0, 100.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Variable Value Max", f_debug_sound_variable_max, -100.0, 100.0, 0.01)
				ADD_WIDGET_BOOL("Update Variable Value every frame",b_bank_test_update_debug_var)
				ADD_WIDGET_BOOL("Update Variable Value on start",b_bank_test_up_dbg_var_on_start)
				
				//Override underwater
				ADD_WIDGET_BOOL("Override Underwater Stream",b_overrideUnderwaterStream)
				
			STOP_WIDGET_GROUP()
			
		BREAK	
		CASE 7
			widgets_9 = START_WIDGET_GROUP("Walla")
				ADD_WIDGET_FLOAT_SLIDER("Ped Density", f_walla_density, 0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Ped Density Override Factor", f_walla_factor, 0.0, 1.0, 0.01)
				ADD_WIDGET_BOOL("Set Walla level",b_override_walla)
			STOP_WIDGET_GROUP()
			
		BREAK
		CASE 8
			widgets_9 = START_WIDGET_GROUP("Ambient Zone Control Testing")
				widget_zonelist_name = ADD_TEXT_WIDGET("Zone or Zonelist Name")
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("Zone On Temp")
					ADD_TO_WIDGET_COMBO("Zone Off Temp")
					ADD_TO_WIDGET_COMBO("Zone Reset Temp")
					ADD_TO_WIDGET_COMBO("Zone On Persistent")		
					ADD_TO_WIDGET_COMBO("Zone Off Persistent")		
					ADD_TO_WIDGET_COMBO("ZoneList On Temp")
					ADD_TO_WIDGET_COMBO("ZoneList Off Temp")
					ADD_TO_WIDGET_COMBO("ZoneList Reset Temp")
					ADD_TO_WIDGET_COMBO("ZoneList On Persistent")		
					ADD_TO_WIDGET_COMBO("ZoneList Off Persistent")							
				STOP_WIDGET_COMBO("Set to:", i_zoneListFunction)
				ADD_WIDGET_BOOL("Force Instant",b_zoneForceChange)
				ADD_WIDGET_BOOL("Apply",b_zoneListApply)
			STOP_WIDGET_GROUP()
		BREAK	
		CASE 9
			widgets_9 = START_WIDGET_GROUP("Conversation Testing")
				widget_conversation_text_block_name = ADD_TEXT_WIDGET("Conversation Text Block")
				widget_conversation_name = ADD_TEXT_WIDGET("Conversation Name")
				widget_voice0_name = ADD_TEXT_WIDGET("Voice0") 
				widget_voice1_name = ADD_TEXT_WIDGET("Voice1") 
				widget_voice2_name = ADD_TEXT_WIDGET("Voice2") 
				widget_voice3_name = ADD_TEXT_WIDGET("Voice3") 
				widget_voice4_name = ADD_TEXT_WIDGET("Voice4") 
				widget_voice5_name = ADD_TEXT_WIDGET("Voice5") 
				widget_voice6_name = ADD_TEXT_WIDGET("Voice6") 
				widget_voice7_name = ADD_TEXT_WIDGET("Voice7") 
				widget_voice8_name = ADD_TEXT_WIDGET("Voice8") 
				ADD_WIDGET_BOOL("Start Conversation",b_start_conversation)
				ADD_WIDGET_BOOL("Stop Conversation",b_stop_conversation)
			STOP_WIDGET_GROUP()
		BREAK
		CASE 10
			widgets_9 = START_WIDGET_GROUP("Anim Linked Coversations")
				widget_conversation_text_block_name = ADD_TEXT_WIDGET("Conversation Text Block")
				widget_conversation_name = ADD_TEXT_WIDGET("Conversation Name")
				widget_voice0_name = ADD_TEXT_WIDGET("Voice")
				ADD_WIDGET_INT_SLIDER("Voice Number",i_ped_number,0,16,1)
				ADD_WIDGET_BOOL("Load Anims",b_LoadAnims)
				ADD_WIDGET_INT_SLIDER("Number of Chunks",i_numberOfConversationChunks,0,16,1)
				widget_anim_dictionary_name = ADD_TEXT_WIDGET("Anim Dictionary Name")
				widget_anim_dictionary_name_2 = ADD_TEXT_WIDGET("Anim Dictionary Name")
				widget_anim_name = ADD_TEXT_WIDGET("Anim Name")
				widget_anim_name2 = ADD_TEXT_WIDGET("Anim Name 2")
				ADD_WIDGET_BOOL("Start Conversation",b_start_conversation)
				ADD_WIDGET_BOOL("Stop Conversation",b_stop_conversation)
			STOP_WIDGET_GROUP()
		BREAK
		CASE 11
			widgets_9 = START_WIDGET_GROUP("Glass feet - centred on camera")
				ADD_WIDGET_FLOAT_SLIDER("Glass Radius",f_glassRadius,0,100,1)
				ADD_WIDGET_BOOL("Toggle Glass",b_glassToggle)
			STOP_WIDGET_GROUP()
		BREAK
		
		CASE 12
			widgets_9 = START_WIDGET_GROUP("Alarms")
				ADD_WIDGET_BOOL("Toggle Alarms",b_alarmsOn)
				widget_alarm_name = ADD_TEXT_WIDGET("Alarm Name")
				ADD_WIDGET_BOOL("Skip Decay",b_alarmsSkipToSustain)
				ADD_WIDGET_BOOL("Force Alarms off",b_alarmsForceState)
				ADD_WIDGET_BOOL("Alarms are playing?",b_alarmsArePlaying)
			STOP_WIDGET_GROUP()
		BREAK

		CASE 13
			widgets_9 = START_WIDGET_GROUP("Static Emitters")
				widget_emitter_name = ADD_TEXT_WIDGET("Emitter Name")
				SET_CONTENTS_OF_TEXT_WIDGET(widget_emitter_name,"BEACH_PARTY_BOOMBOX_03")
				ADD_WIDGET_BOOL("Toggle Emitter",b_emitterOn)
				widget_station_name = ADD_TEXT_WIDGET("Station Name")
				SET_CONTENTS_OF_TEXT_WIDGET(widget_station_name,"RADIO_02_POP")
				ADD_WIDGET_BOOL("Retune Emitter",b_emitterRetune)
				ADD_WIDGET_BOOL("Link To Prop",b_emitterLinkToProp)
				ADD_WIDGET_BOOL("Spawn Prop",b_emitterSpawnProp)
				widget_music_name = ADD_TEXT_WIDGET("Music Event Name")
				ADD_TEXT_WIDGET("TRY   CHN2_MISSION_START    CHN2_STOP_TRACK  ")
				SET_CONTENTS_OF_TEXT_WIDGET(widget_station_name,"CHN2_MISSION_START")
				ADD_WIDGET_BOOL("Toggle Score",b_emitterToggleScore)
			STOP_WIDGET_GROUP()
		BREAK
		
	ENDSWITCH
	
	CLEAR_CURRENT_WIDGET_GROUP(widget_settings)
ENDPROC

PROC DESTROY_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(widget_debug)
		DELETE_WIDGET_GROUP(widget_debug)
	ENDIF
ENDPROC

FUNC BOOL IS_SHOOTING(PED_INDEX shooter)
    IF NOT IS_PED_INJURED(shooter)
        SCRIPTTASKSTATUS taskStatus = GET_SCRIPT_TASK_STATUS(shooter, SCRIPT_TASK_PERFORM_SEQUENCE)
        IF taskStatus = FINISHED_TASK
            RETURN FALSE
        ENDIF
    ENDIF

    RETURN TRUE
ENDFUNC

PROC setShooterTask()
	v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	CLEAR_SEQUENCE_TASK(si_shooter_tasks)
	OPEN_SEQUENCE_TASK(si_shooter_tasks)

		SWITCH i_shoot_mode
			case 0
				TASK_SHOOT_AT_COORD(NULL,v_player_pos + <<0,0,f_shooter_aim_offset>>, floor(f_shooter_burst_length*1000), FIRING_TYPE_1_BURST)
			BREAK
			case 1
				TASK_SHOOT_AT_COORD(NULL,v_player_pos + <<0,0,f_shooter_aim_offset>>, floor(f_shooter_burst_length*1000), FIRING_TYPE_1_THEN_AIM)
			BREAK
			case 2
				TASK_SHOOT_AT_COORD(NULL,v_player_pos + <<0,0,f_shooter_aim_offset>>, floor(f_shooter_burst_length*1000), FIRING_TYPE_CLIP)
			BREAK
			case 3
				TASK_SHOOT_AT_COORD(NULL,v_player_pos + <<0,0,f_shooter_aim_offset>>, floor(f_shooter_burst_length*1000), FIRING_TYPE_CONTINUOUS)
			BREAK
			case 4
				TASK_SHOOT_AT_COORD(NULL,v_player_pos + <<0,0,f_shooter_aim_offset>>, floor(f_shooter_burst_length*1000), FIRING_TYPE_RANDOM_BURSTS)
			BREAK
			case 5
				TASK_SHOOT_AT_ENTITY(NULL,PLAYER_PED_ID(),floor(f_shooter_burst_length*1000), FIRING_TYPE_CONTINUOUS)
			BREAK
			case 6
				TASK_SHOOT_AT_ENTITY(NULL,PLAYER_PED_ID(),floor(f_shooter_burst_length*1000), FIRING_TYPE_CLIP)
			BREAK
		ENDSWITCH
		TASK_PAUSE(NULL,floor(f_shooter_burst_pause * 1000))
	CLOSE_SEQUENCE_TASK(si_shooter_tasks)
ENDPROC

FUNC FLOAT getWidgetValues()
	FLOAT widget_value = f_shooter_min_dist + f_shooter_max_dist + f_shooter_aim_offset + TO_FLOAT(i_shooter_gun_type)
	RETURN widget_value
ENDFUNC

PROC Setup_Shooter()
	//SETUP INITIAL WEAPONS FOR GUNMEN
		//WEAPONTYPE_PISTOL
		b_weaponsAllowed[0] = TRUE
		//WEAPONTYPE_COMBATPISTOL
		b_weaponsAllowed[1] = FALSE
		//WEAPONTYPE_APPISTOL
		b_weaponsAllowed[2] = FALSE
		//WEAPONTYPE_MICROSMG
		b_weaponsAllowed[3] = FALSE
		//WEAPONTYPE_SMG
		b_weaponsAllowed[4] = TRUE
		//WEAPONTYPE_ASSAULTRIFLE
		b_weaponsAllowed[5] = FALSE
		//WEAPONTYPE_CARBINERIFLE
		b_weaponsAllowed[6] = FALSE
		//WEAPONTYPE_ADVANCEDRIFLE
		b_weaponsAllowed[7] = FALSE
		//WEAPONTYPE_MG
		b_weaponsAllowed[8] = TRUE
		//WEAPONTYPE_COMBATMG
		b_weaponsAllowed[9] = FALSE
		//WEAPONTYPE_PUMPSHOTGUN
		b_weaponsAllowed[10] = FALSE
		//WEAPONTYPE_SAWNOFFSHOTGUN
		b_weaponsAllowed[11] = FALSE
		//WEAPONTYPE_ASSAULTSHOTGUN
		b_weaponsAllowed[12] = FALSE
		//WEAPONTYPE_SNIPERRIFLE
		b_weaponsAllowed[13] = FALSE
		//WEAPONTYPE_HEAVYSNIPER
		b_weaponsAllowed[14] = FALSE
		//WEAPONTYPE_REMOTESNIPER
		b_weaponsAllowed[15] = FALSE
		//WEAPONTYPE_GRENADELAUNCHER
		b_weaponsAllowed[16] = FALSE
		//WEAPONTYPE_RPG
		b_weaponsAllowed[17] = FALSE
		//WEAPONTYPE_MINIGUN
		b_weaponsAllowed[18] = FALSE
		//WEAPONTYPE_GRENADE
		b_weaponsAllowed[19] = FALSE
		//WEAPONTYPE_SMOKEGRENADE
		b_weaponsAllowed[20] = FALSE
		//WEAPONTYPE_STICKYBOMB
		b_weaponsAllowed[21] = FALSE
		//WEAPONTYPE_STUNGUN
		b_weaponsAllowed[22] = FALSE
	
	REQUEST_MODEL(model_shooter)
	WHILE NOT HAS_MODEL_LOADED(model_shooter)
		REQUEST_MODEL(model_shooter)
		PRINTSTRING("Loading Models (init)")
		PRINTNL()
		WAIT(0)
	ENDWHILE
	//Find state of shooter widgets
	f_widget_product = getWidgetValues()

ENDPROC

PROC Setup_GunMen()
	REQUEST_MODEL(model_shooter)
	WHILE NOT HAS_MODEL_LOADED(model_shooter)
		REQUEST_MODEL(model_shooter)
		PRINTSTRING("Loading Models (init)")
		PRINTNL()
		WAIT(0)
	ENDWHILE
ENDPROC

PROC Setup_Panner()
	START_AUDIO_SCENE("AMBISONIC_PANNING_TEST_SCENE")
	SET_AUDIO_SCENE_VARIABLE("AMBISONIC_PANNING_TEST_SCENE","apply",0)
	WHILE NOT (HAS_MODEL_LOADED(model_cursor)
		AND HAS_PTFX_ASSET_LOADED())
		REQUEST_PTFX_ASSET()
		REQUEST_MODEL(model_cursor)
		PRINTSTRING("Loading Models")
		PRINTNL()
		WAIT(0)
	ENDWHILE
	obj_cursor = CREATE_OBJECT(model_cursor, <<-1234,-1135,13>>)
	IF NOT DOES_BLIP_EXIST(bi_sound_source_blip)
		bi_sound_source_blip = ADD_BLIP_FOR_ENTITY(obj_cursor)
	ENDIF
ENDPROC

PROC Setup_PoScanTest()
	PRINTSTRING("STARTING POLICE SCANNER TESTER")
	PRINTNL()
		
	SET_CONTENTS_OF_TEXT_WIDGET(widget_scanner_scripted_report_name,"CNC_OFFICER_DOWN")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_scanner_scripted_report_name_2,"")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_scanner_audio_scene_name,"POLICE_SCANNER_IMPORTANT_SCENE")
ENDPROC

PROC Setup_DynamicMixTest()
	PRINTSTRING("STARTING AUDIO SCENE TESTER")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_dynamic_mixer,"SOL_1_CHASE_JET_SCENE")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_dynamic_mixer_group_name,"SOL_1_CHASE_JET_SHAMAL")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_dynamic_mixer_variable_name,"apply")
	PRINTNL()
ENDPROC

PROC Setup_ScriptedStreamTester()
	PRINTSTRING("STARTING SCRIPTED STREAM TESTER")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_script_stream_name,"TEST_SCRIPTED_STREAM_NAME")
	set_CONTENTS_OF_TEXT_WIDGET(widget_soundSet_name,"DOCKS_HEIST_FINALE_2A_SOUNDS")
	PRINTNL()
ENDPROC

PROC Setup_WallaTest()
	PRINTSTRING("STARTING WALLA TESTING WIDGETS")
	PRINTNL()
ENDPROC

PROC Setup_ZoneListTest()
	PRINTSTRING("STARTING ZONE TESTING WIDGETS")
	PRINTNL()
	SET_CONTENTS_OF_TEXT_WIDGET(widget_zonelist_name,"ZONE_LIST_YANKTON")
ENDPROC

PROC Setup_ConversationTest()
	PRINTSTRING("STARTING Conversation TESTING WIDGETS")
	PRINTNL()
	SET_CONTENTS_OF_TEXT_WIDGET(widget_conversation_name,"")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_conversation_text_block_name,"")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_voice0_name,"default")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_voice1_name,"default")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_voice2_name,"default")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_voice3_name,"default")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_voice4_name,"default")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_voice5_name,"default")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_voice6_name,"default")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_voice7_name,"default")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_voice8_name,"default")
	b_speakers_made = FALSE
ENDPROC

PROC Setup_ConversationAnimTest()
	PRINTSTRING("STARTING Conversation TESTING WIDGETS")
	PRINTNL()
	SET_CONTENTS_OF_TEXT_WIDGET(widget_conversation_text_block_name,"PBJSAUD")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_conversation_name,"PBJS_RAND_1")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_voice0_name,"JESSE")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_anim_dictionary_name,"special_ped@jessie")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_anim_name,"jessie_ig_1_")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_anim_name2,"p1_heydudes")
	i_ped_number = 3
ENDPROC

FUNC BOOL LoadABank(STRING bankName)
	//LOAD - BAIL AFTER 1000ms
	if REQUEST_SCRIPT_AUDIO_BANK(bankName)
		PRINTSTRING(bankName)
		PRINTSTRING(" IS LOADED IN ")
		PRINTNL()		
		return TRUE
	ELSE
		PRINTSTRING(bankName)
		PRINTSTRING(" Requested")
		PRINTNL()	
		return FALSE
	endif
ENDFUNC

PROC getBankNames()
	//GET NAMES
//	s_bank_name = "SCRIPT\\"
//	s_bank_name_2 = "SCRIPT\\"
//	s_bank_name_3 = "SCRIPT\\"
//	s_bank_name_4 = "SCRIPT\\"
//	s_bank_name_5 = "SCRIPT\\"	
	
	s_bank_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name)
	s_bank_name += GET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name_1)
	s_bank_name_2 = GET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name_2)
	s_bank_name_3 = GET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name_3)
	s_bank_name_4 = GET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name_4)
	s_bank_name_5 = GET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name_5)
ENDPROC

FUNC BOOL hintAllBanks()
	getBankNames()
	//LOAD
	HINT_SCRIPT_AUDIO_BANK(s_bank_name)
	HINT_SCRIPT_AUDIO_BANK(s_bank_name_2)
	HINT_SCRIPT_AUDIO_BANK(s_bank_name_3)
	HINT_SCRIPT_AUDIO_BANK(s_bank_name_4)
	HINT_SCRIPT_AUDIO_BANK(s_bank_name_5)
	RETURN TRUE
ENDFUNC

FUNC BOOL LoadAllBanks()
	getBankNames()	
	
	i_loadTries += 1
	
	//LOAD
	if (LoadABank(s_bank_name) AND LoadABank(s_bank_name_2) AND LoadABank(s_bank_name_3) AND LoadABank(s_bank_name_4) AND LoadABank(s_bank_name_5)) OR i_loadTries > 100
		i_loadTries = 0
		return TRUE
	ELSE
		return FALSE
	ENDIF
ENDFUNC


PROC Setup_BankSoundTest()
	//Set initial sound names and bank
	SET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name,"Underwater")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name_1,"")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name_2,"FBI_05_Chemical_Factory_01")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name_3,"LIFTS")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name_4,"ALARM_BELL_01")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name_5,"ALARM_BELL_02")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_circle_sound_name,"UW_Ambience")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_square_sound_name,"FBI_05_RAID_GASMASK")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_triangle_sound_name,"FBI_05_RAID_TORCH")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_cross_sound_name,"RIFLE_FIRE")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_variable_name,"AbseilTime")
	SET_CONTENTS_OF_TEXT_WIDGET(widget_soundSet_name,"Phone_SoundSet_Default")
	
	LoadAllBanks()	
		
	s_circle_sound_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_circle_sound_name)
	s_square_sound_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_square_sound_name)
	s_triangle_sound_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_triangle_sound_name)
	s_cross_sound_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_cross_sound_name)
	
ENDPROC

proc setup_glass()
	PRINTNL()
ENDPROC

proc Setup_Alarms()
	SET_CONTENTS_OF_TEXT_WIDGET(widget_alarm_name,"PORT_OF_LS_HEIST_FORT_ZANCUDO_ALARMS")
ENDPROC

proc Setup_Emitters()
	//NOWT
ENDPROC




PROC SetupTestMode()
	SWITCH i_test_mode 
		CASE 0
			Setup_Panner()
		BREAK
		CASE 1
			Setup_Shooter()
		BREAK
		CASE 2
			Setup_GunMen()
		BREAK
		CASE 3
			Setup_BankSoundTest()
		BREAK
		CASE 4
			Setup_PoScanTest()
		BREAK
		CASE 5
			Setup_DynamicMixTest()
		BREAK
		CASE 6
			Setup_ScriptedStreamTester()
		BREAK
		CASE 7
			Setup_WallaTest()
		BREAK
		CASE 8
			Setup_ZoneListTest()
		BREAK
		CASE 9
			Setup_ConversationTest()
		BREAK
		CASE 10
			Setup_ConversationAnimTest()
		BREAK
		CASE 11
			Setup_Glass()
		BREAK
		CASE 12
			Setup_Alarms()
		BREAK
		CASE 13
			Setup_Emitters()
		BREAK
	ENDSWITCH
ENDPROC

PROC MISSION_SETUP()	
	
	REGISTER_SCRIPT_WITH_AUDIO()
	
	//Create Main widgets top level group
	CREATE_MAIN_WIDGET_GROUP()
	
	//Create Widgets for the current test mode
	CREATE_WIDGETS()

	//Setup the current test mode
	SetupTestMode()

	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	CLEAR_HELP()
	CLEAR_PRINTS()
	
	SETTIMERA(0)
	SETTIMERB(0)
	
ENDPROC


PROC SoundPanningTest()
	
	//Get Widget Values and player coords	
	s_sound_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_sound_name)
	v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	if b_yInheritX
		f_debug_sound_Y = f_debug_sound_X
	ENDIF

	if b_playSound 
		if not b_retriggerSound
			if HAS_SOUND_FINISHED(i_testSoundId0)
				PLAY_SOUND_FROM_ENTITY(i_testSoundId0,s_sound_name,obj_cursor)
			ENDIF
		ENDIF
		IF i_sparklyEffect = NULL
			i_sparklyEffect = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_bio_grille_cutting", obj_cursor,<<0,0,0>>,<<0,0,0>>,1)
		//	PRINTINT(i_sparklyEffect)
			PRINTNL()
		ENDIF
	ELSE
		if not HAS_SOUND_FINISHED(i_testSoundId0)
			STOP_SOUND(i_testSoundId0)
			IF i_sparklyEffect != NULL
				STOP_PARTICLE_FX_LOOPED(i_sparklyEffect)
				i_sparklyEffect = NULL
			ENDIF
		ENDIF
	ENDIF
	if b_playSound and not b_freezeSound
		if i_freezeTime > 0
			SETTIMERA(i_freezeTime)
		ENDIF
		
		SWITCH i_debug_sound_type
			case 0
				//Circling Sound
				if TIMERA() >= f_debug_sound_cycle_time*1000
					SETTIMERA(0)
				endif
				
				v_sound_pos.x = SIN((TO_FLOAT(TIMERA()) / (f_debug_sound_cycle_time*1000))*360) * f_debug_sound_X + v_player_pos.x
				v_sound_pos.y = COS((TO_FLOAT(TIMERA()) / (f_debug_sound_cycle_time*1000))*360) * f_debug_sound_Y + v_player_pos.y
				v_sound_pos.z = f_debug_sound_Z + v_player_pos.z
			BREAK
			
			CASE 1
				//Passing Sound
				if TIMERA() >= f_debug_sound_cycle_time*1000
					SETTIMERA(0)
				endif
				
				v_sound_pos.x = ((TO_FLOAT(TIMERA()) / (f_debug_sound_cycle_time * 1000))*2 - 1) *  f_debug_sound_X + v_player_pos.x
				v_sound_pos.y = f_debug_sound_Y + v_player_pos.y
				v_sound_pos.z = f_debug_sound_Z + v_player_pos.z			
			BREAK
		
			CASE 2
				//Manually Positioned Sound
				v_sound_pos.x = v_player_pos.x + f_debug_sound_X
				v_sound_pos.y = v_player_pos.y + f_debug_sound_Y
				v_sound_pos.z = v_player_pos.z + f_debug_sound_Z
			BREAK

			CASE 3
				//Randomised position
				if TIMERB() >= f_debug_sound_retrigger_time * 1000
					v_sound_pos = GET_RANDOM_POINT_IN_DISC(v_player_pos,f_debug_sound_X,f_debug_sound_Z)
				ENDIF
			BREAK
							
		ENDSWITCH
		i_freezeTime = -1
	ELSE
		if i_freezeTime < 0
			i_freezeTime = TIMERA()
		ENDIF
	ENDIF

	IF b_retriggerSound and b_playSound
		IF TIMERB() > f_debug_sound_retrigger_time * 1000
			PLAY_SOUND_FROM_ENTITY(i_testSoundId0,s_sound_name,obj_cursor)	
		ENDIF
	ENDIF


	if TIMERB() > f_debug_sound_retrigger_time * 1000
		SETTIMERB(0)
	ENDIF

	SET_ENTITY_COORDS_NO_OFFSET(obj_cursor,v_sound_pos)

	if not IS_AUDIO_SCENE_ACTIVE("AMBISONIC_PANNING_TEST_SCENE")
		START_AUDIO_SCENE("AMBISONIC_PANNING_TEST_SCENE")
	ELSE
		if f_debug_sound_audio_scene_wet != f_debug_sound_audio_scene_wet_old
			SET_AUDIO_SCENE_VARIABLE("AMBISONIC_PANNING_TEST_SCENE","apply",f_debug_sound_audio_scene_wet)
		ENDIF
		f_debug_sound_audio_scene_wet_old = f_debug_sound_audio_scene_wet
	ENDIF	
ENDPROC

FUNC VECTOR SHOOTER_GET_NEW_COORDS()
		FLOAT f_random_distance = GET_RANDOM_FLOAT_IN_RANGE(f_shooter_min_dist,f_shooter_max_dist)
		FLOAT f_random_angle = GET_RANDOM_FLOAT_IN_RANGE(0,360)
		VECTOR new_pos = v_player_pos + <<f_random_distance*SIN(f_random_angle),f_random_distance*COS(f_random_angle),0>>
		GET_GROUND_Z_FOR_3D_COORD(new_pos,new_pos.z)
		RETURN new_pos
ENDFUNC

FUNC FLOAT GetHeadingBetweenVectors(VECTOR A, VECTOR B)
	RETURN GET_HEADING_FROM_VECTOR_2D(B.x-A.x,B.y-A.y)
ENDFUNC

PROC updateTheModel()
	model_shooter_last = model_shooter
	SWITCH i_model_number
		CASE 0
			model_shooter = A_M_Y_MUSCLBEAC_01
		BREAK
		CASE 1
			model_shooter = A_M_M_Business_01
		BREAK
		CASE 2
			model_shooter = G_M_Y_MexGoon_02
		BREAK
		CASE 3
			model_shooter = S_M_M_HighSec_01//A_F_M_BUSINESS01
		BREAK
		CASE 4
			model_shooter = A_F_Y_BEACH_01
		BREAK
		CASE 5
			model_shooter = A_F_M_BevHills_02
		BREAK
		CASE 6
			model_shooter = A_M_Y_CYCLIST_01
		BREAK
		CASE 7
			model_shooter = S_M_Y_COP_01
		BREAK
		CASE 8
			model_shooter = S_F_Y_HOOKER_01
		BREAK
		CASE 9
			model_shooter = S_M_Y_MARINE_01
		BREAK
		CASE 10
			model_shooter = S_M_Y_SWAT_01
		BREAK
		CASE 11
			model_shooter = S_M_Y_SWAT_01
		BREAK
	ENDSWITCH
	
	IF model_shooter != model_shooter_last
		SET_MODEL_AS_NO_LONGER_NEEDED(model_shooter_last)
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(model_shooter)
		REQUEST_MODEL(model_shooter)
		PRINTSTRING("LOADING SHOOTER - UPDATE")
		PRINTNL()
	ENDIF
ENDPROC


PROC Shooter_Creator()
	if HAS_MODEL_LOADED(model_shooter)
		IF DOES_ENTITY_EXIST(pi_shooter_01)
			IF NOT IS_ENTITY_DEAD(pi_shooter_01)
				IF NOT DOES_BLIP_EXIST(bi_shooter_01)
					bi_shooter_01 = ADD_BLIP_FOR_ENTITY(pi_shooter_01)
				ENDIF
				SET_PED_ACCURACY(pi_shooter_01,100)
				GIVE_WEAPON_TO_PED(pi_shooter_01, wt_shooterGun, 100)
				SET_PED_INFINITE_AMMO(pi_shooter_01,TRUE)
				SET_ENTITY_INVINCIBLE(pi_shooter_01,TRUE)
				SET_PED_SHOOT_RATE(pi_shooter_01,100)
				SET_PED_CURRENT_WEAPON_VISIBLE(pi_shooter_01,TRUE)
				setShooterTask()
				b_shooter_ready = TRUE
			ENDIF
		ELSE
			VECTOR v_new_shooter_pos = SHOOTER_GET_NEW_COORDS()
			pi_shooter_01 = CREATE_PED( PEDTYPE_MISSION, model_shooter, v_new_shooter_pos,GetHeadingBetweenVectors(v_new_shooter_pos, v_player_pos))
		ENDIF
	ELSE
		REQUEST_MODEL(model_shooter)
		PRINTSTRING("LOADING SHOOTER (Creator)")
		PRINTNL()
	ENDIF
ENDPROC

FUNC WEAPON_TYPE getGunFromInt(INT k)
	WEAPON_TYPE chosenGun
	SWITCH k
		CASE 0
			chosenGun = WEAPONTYPE_PISTOL
		BREAK
		CASE 1
			chosenGun = WEAPONTYPE_COMBATPISTOL
		BREAK
		CASE 2
			chosenGun = WEAPONTYPE_APPISTOL
		BREAK
		CASE 3
			chosenGun = WEAPONTYPE_MICROSMG
		BREAK
		CASE 4
			chosenGun = WEAPONTYPE_SMG
		BREAK
		CASE 5
			chosenGun = WEAPONTYPE_ASSAULTRIFLE
		BREAK
		CASE 6
			chosenGun = WEAPONTYPE_CARBINERIFLE
		BREAK
		CASE 7
			chosenGun = WEAPONTYPE_ADVANCEDRIFLE
		BREAK
		CASE 8
			chosenGun = WEAPONTYPE_MG
		BREAK
		CASE 9
			chosenGun = WEAPONTYPE_COMBATMG
		BREAK
		CASE 10
			chosenGun = WEAPONTYPE_PUMPSHOTGUN
		BREAK
		CASE 11
			chosenGun = WEAPONTYPE_SAWNOFFSHOTGUN
		BREAK
		CASE 12
			chosenGun = WEAPONTYPE_ASSAULTSHOTGUN
		BREAK
		CASE 13
			chosenGun = WEAPONTYPE_SNIPERRIFLE
		BREAK
		CASE 14
			chosenGun = WEAPONTYPE_HEAVYSNIPER
		BREAK
		CASE 15
			chosenGun = WEAPONTYPE_REMOTESNIPER
		BREAK
		CASE 16
			chosenGun = WEAPONTYPE_GRENADELAUNCHER
		BREAK
		CASE 17
			chosenGun = WEAPONTYPE_RPG
		BREAK
		CASE 18
			chosenGun = WEAPONTYPE_MINIGUN
		BREAK
		CASE 19
			chosenGun = WEAPONTYPE_GRENADE
		BREAK
		CASE 20
			chosenGun = WEAPONTYPE_SMOKEGRENADE
		BREAK
		CASE 21
			chosenGun = WEAPONTYPE_STICKYBOMB
		BREAK
		CASE 22
			chosenGun = WEAPONTYPE_STUNGUN
		BREAK
	ENDSWITCH

	RETURN chosenGun

ENDFUNC

PROC ShooterTest()
	
	updateTheModel()
	
	v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	wt_shooterGun = getGunFromInt(i_shooter_gun_type)	
	
	
	IF HAS_MODEL_LOADED(model_shooter)
		IF f_widget_product = getWidgetValues()
			if not b_shooter_ready
				Shooter_Creator()
			ELSE
				IF b_shooter_respot
					IF NOT IS_ENTITY_DEAD(pi_shooter_01)
						VECTOR v_new_shooter_pos = SHOOTER_GET_NEW_COORDS()
						/*SET_ENTITY_COORDS*/ SET_PED_COORDS_KEEP_VEHICLE(pi_shooter_01, v_new_shooter_pos)
						SET_ENTITY_HEADING(pi_shooter_01,GetHeadingBetweenVectors(v_new_shooter_pos,v_player_pos))
						b_shooter_respot = FALSE
					ENDIF
				ENDIF
				IF b_shooter_fire
					IF NOT IS_ENTITY_DEAD(pi_shooter_01)
						IF NOT IS_SHOOTING(pi_shooter_01)
							//Reset task
							SET_PED_SHOOT_RATE(pi_shooter_01, i_shoot_rate)
							setShooterTask()
							TASK_PERFORM_SEQUENCE(pi_shooter_01, si_shooter_tasks)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_ENTITY_DEAD(pi_shooter_01)
						IF IS_SHOOTING(pi_shooter_01)
							CLEAR_PED_TASKS(pi_shooter_01)
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_SHOOTING(pi_shooter_01)
				CLEAR_PED_TASKS(pi_shooter_01)
			ELSE
				IF NOT IS_ENTITY_DEAD(pi_shooter_01)
					Shooter_Creator()
					TASK_PERFORM_SEQUENCE(pi_shooter_01, si_shooter_tasks)
					f_widget_product = getWidgetValues()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC WEAPON_TYPE GetAGunForGunMan()
	BOOL NotAGoodGun = TRUE
	INT TriesToGetAGoodGun = 0
	INT i_temp
	WHILE NotAGoodGun
		i_temp = GET_RANDOM_INT_IN_RANGE(0,24)
		IF b_weaponsAllowed[i_temp]
			NotAGoodGun = FALSE
		ELSE
			IF TriesToGetAGoodGun > 10
				//FALL BACK TO PISTOL
				i_temp = 0
				NotAGoodGun = FALSE
			ENDIF
			i_temp++
		ENDIF
	ENDWHILE
	RETURN getGunFromInt(i_temp)
ENDFUNC

PROC makeMeAGunMan(INT k)
	v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	VECTOR v_new_shooter_pos = SHOOTER_GET_NEW_COORDS()
	pi_GunMen[k] = CREATE_PED(PEDTYPE_CRIMINAL, model_shooter, v_new_shooter_pos,GetHeadingBetweenVectors(v_new_shooter_pos, v_player_pos))
	b_GunMen_armed[k] = FALSE
ENDPROC

PROC MakeAGunMan()
	IF DOES_ENTITY_EXIST(pi_GunMen[i_currentGunMan])
		IF NOT IS_ENTITY_DEAD(pi_GunMen[i_currentGunMan])
			bi_GunMen[i_currentGunMan] = ADD_BLIP_FOR_ENTITY(pi_GunMen[i_currentGunMan])
			SET_PED_ACCURACY(pi_GunMen[i_currentGunMan],GET_RANDOM_INT_IN_RANGE(0,100))
			GIVE_WEAPON_TO_PED(pi_GunMen[i_currentGunMan], wt_shooterGun, 100)
			SET_PED_INFINITE_AMMO(pi_GunMen[i_currentGunMan],TRUE)
			SET_PED_SHOOT_RATE(pi_GunMen[i_currentGunMan],100)
			SET_PED_CURRENT_WEAPON_VISIBLE(pi_GunMen[i_currentGunMan],TRUE)
			SET_PED_COMBAT_ABILITY(pi_GunMen[i_currentGunMan], CAL_PROFESSIONAL)
			TASK_COMBAT_PED(pi_GunMen[i_currentGunMan],PLAYER_PED_ID())				
			i_currentGunMan = (i_currentGunMan + 1) % i_numberOfGunMen
			
			IF DOES_ENTITY_EXIST(pi_GunMen[i_currentGunMan])
				IF NOT IS_ENTITY_DEAD(pi_GunMen[i_currentGunMan])
					ADD_EXPLOSION(GET_ENTITY_COORDS(pi_GunMen[i_currentGunMan]),EXP_TAG_GRENADE)
				ELSE
					ADD_EXPLOSION(/*WITH DEADCHECK=FALSE*/ GET_ENTITY_COORDS(pi_GunMen[i_currentGunMan]),EXP_TAG_GRENADE)
				ENDIF
				
				SET_PED_AS_NO_LONGER_NEEDED(pi_GunMen[i_currentGunMan])
			ENDIF

			IF DOES_BLIP_EXIST(bi_GunMen[i_currentGunMan])
				REMOVE_BLIP(bi_GunMen[i_currentGunMan])
			ENDIF
			
			SETTIMERA(0)
		ENDIF
	ELSE
		v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		wt_shooterGun = GetAGunForGunMan()
		VECTOR v_new_shooter_pos = SHOOTER_GET_NEW_COORDS()
		pi_GunMen[i_currentGunMan] = CREATE_PED(PEDTYPE_CRIMINAL, model_shooter, v_new_shooter_pos,GetHeadingBetweenVectors(v_new_shooter_pos, v_player_pos))
	ENDIF
ENDPROC

PROC removeDeadGunMenBlips()
	FOR i = 0 to i_numberOfGunMen - 1
		IF DOES_BLIP_EXIST(bi_GunMen[i])
			IF IS_ENTITY_DEAD(pi_GunMen[i])
				PRINTSTRING("PED ")
				PRINTINT(i)
				PRINTSTRING(" IS DEAD - REMOVING BLIP")
				PRINTNL()
				REMOVE_BLIP(bi_GunMen[i])
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC PED_INDEX getSafeCombatTarget(INT j)
	INT i_fightTarget
	INT i_guesses = 0
	WHILE i_guesses < 10
		i_fightTarget = GET_RANDOM_INT_IN_RANGE(0,COUNT_OF(pi_GunMen))
		IF DOES_ENTITY_EXIST(pi_GunMen[i_fightTarget])
			IF NOT(i_fightTarget = COUNT_OF(pi_GunMen) OR i_fightTarget = j OR IS_ENTITY_DEAD(pi_GunMen[i_fightTarget]))
				RETURN pi_GunMen[i_fightTarget]
			ENDIF
		ENDIF
	ENDWHILE
	RETURN PLAYER_PED_ID()
ENDFUNC

PROC UpdateGunMen()

	FOR i = 0 to i_numberOfGunMen - 1
		IF DOES_ENTITY_EXIST(pi_GunMen[i])
			IF IS_ENTITY_DEAD(pi_GunMen[i])
				SET_PED_AS_NO_LONGER_NEEDED(pi_GunMen[i])
				IF DOES_BLIP_EXIST(bi_GunMen[i])
					REMOVE_BLIP(bi_GunMen[i])
				ENDIF
				makeMeAGunMan(i)
			ELSE
				IF NOT b_GunMen_armed[i]
					wt_shooterGun = GetAGunForGunMan()
					bi_GunMen[i] = ADD_BLIP_FOR_ENTITY(pi_GunMen[i])
					SET_PED_ACCURACY(pi_GunMen[i],GET_RANDOM_INT_IN_RANGE(0,100))
					GIVE_WEAPON_TO_PED(pi_GunMen[i], wt_shooterGun, 100)
					SET_PED_INFINITE_AMMO(pi_GunMen[i],TRUE)
					SET_PED_SHOOT_RATE(pi_GunMen[i],100)
					SET_PED_CURRENT_WEAPON_VISIBLE(pi_GunMen[i],TRUE)
					SET_PED_COMBAT_ABILITY(pi_GunMen[i], CAL_PROFESSIONAL)
					TASK_COMBAT_PED_TIMED(pi_GunMen[i],getSafeCombatTarget(i),i_GunManTimeToNewTarget*1000)
					b_GunMen_armed[i] = TRUE
				ELSE
					IF GET_SCRIPT_TASK_STATUS(pi_GunMen[i],SCRIPT_TASK_COMBAT)= FINISHED_TASK
						TASK_COMBAT_PED_TIMED(pi_GunMen[i],getSafeCombatTarget(i),i_GunManTimeToNewTarget*1000)
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			makeMeAGunMan(i)
		ENDIF
	ENDFOR
ENDPROC

PROC GunMenTest()

	updateTheModel()

	IF HAS_MODEL_LOADED(model_shooter)
		IF b_shooter_respawning
			IF TIMERA()>i_GunManRespawnTime*1000	
				MakeAGunMan()
			ENDIF
			removeDeadGunMenBlips()
		ELSE
			UpdateGunMen()
		ENDIF
	ENDIF
	
ENDPROC



PROC PoliceScannerTest()
	
	
	
	IF b_toggleDisableScanner
		IF b_disableScanner
			//SET_POLICE_SCANNER_ACTIVE(FALSE)
			b_disableScanner = FALSE
			PRINTSTRING("SCANNER DISABLED! - SET FALSE")
			PRINTNL()
		ELSE
			//SET_POLICE_SCANNER_ACTIVE(TRUE)
			b_disableScanner = TRUE
			PRINTSTRING("SCANNER ENABLED! - SET TRUE")
			PRINTNL()
		ENDIF
		b_toggleDisableScanner = FALSE
	ENDIF

	IF b_toggleScannerScriptedOnly
		IF b_scannerScriptedOnly
			//SET_ONLY_ALLOW_SCRIPT_TRIGGERED_POLICE_SCANNER_ACTIVE(FALSE)
			b_scannerScriptedOnly = FALSE
			PRINTSTRING("ALL REPORTS ALLOWED - SET FALSE")
			PRINTNL()
		ELSE
			//SET_ONLY_ALLOW_SCRIPT_TRIGGERED_POLICE_SCANNER_ACTIVE(TRUE)
			b_scannerScriptedOnly = TRUE
			PRINTSTRING("ONLY SCRIPTED REPORTS ALLOWED - SET TRUE")
			PRINTNL()
		ENDIF
		b_toggleScannerScriptedOnly = FALSE
	ENDIF


			
	
	IF b_reportCrime
		REPORT_CRIME(PLAYER_ID(),INT_TO_ENUM(CRIME_TYPE,i_crimeNumber),i_crimeValue)
		b_reportCrime = FALSE
	ENDIF
	if b_playVigilanteCrime
		PLAY_VIGILANTE_CRIME(i_crimeNumber, <<f_crimeX, f_crimeY, f_crimeZ>>)
		b_playVigilanteCrime = FALSE
	ENDIF
	IF b_playScannerScriptedLine
		s_scannerScriptedCrime = GET_CONTENTS_OF_TEXT_WIDGET(widget_scanner_scripted_report_name)
		s_scannerScriptedCrime += GET_CONTENTS_OF_TEXT_WIDGET(widget_scanner_scripted_report_name_2)
		
		
		IF b_scannerAutoInfo
			//Set location
			IF f_scannerPosX + f_scannerPosY + f_scannerPosZ = 0
				//Use player coords if sliders are all unset
				SET_POLICE_SCANNER_POSITION_INFO(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ELSE
				SET_POLICE_SCANNER_POSITION_INFO(<<f_scannerPosX, f_scannerPosY, f_scannerPosZ>>)
			ENDIF
			//Set unit
			SET_POLICE_SCANNER_CAR_CODE_INFO(INT_TO_ENUM(POLICE_SCANNER_CAR_CODE,i_scannerUnitType),i_scannerUnitBeat)
			//Set Crime
			SET_POLICE_SCANNER_CRIME_INFO(i_crimeNumber)

		ENDIF
				
		PLAY_POLICE_REPORT(s_scannerScriptedCrime, f_scannerApplyValue)
		b_playScannerScriptedLine = FALSE
	ENDIF
	IF b_updateScannerUnit
		LOAD_STREAM("ARM_WRESTLING_CROWD_MASTER")
		SET_POLICE_SCANNER_CAR_CODE_INFO(INT_TO_ENUM(POLICE_SCANNER_CAR_CODE,i_scannerUnitType),i_scannerUnitBeat)
		b_updateScannerUnit = FALSE
	ENDIF
	IF b_updateScannerPos
		SET_POLICE_SCANNER_POSITION_INFO(<<f_scannerPosX, f_scannerPosY, f_scannerPosZ>>)
		b_updateScannerPos = FALSE
	ENDIF
	IF b_updateScannerCrime
		SET_POLICE_SCANNER_CRIME_INFO(i_crimeNumber)
		b_updateScannerCrime = FALSE
	ENDIF
	IF b_setScannerAudioScene
		s_scannerAudioScene = GET_CONTENTS_OF_TEXT_WIDGET(widget_scanner_audio_scene_name)
		if not IS_AUDIO_SCENE_ACTIVE("s_scannerAudioScene")
			SET_POLICE_SCANNER_AUDIO_SCENE(s_scannerAudioScene)
		ENDIF
		
		
				
		b_setScannerAudioScene = FALSE
	ENDIF
	IF b_unsetScannerAudioScene
		STOP_AND_REMOVE_POLICE_SCANNER_AUDIO_SCENE()
		b_unsetScannerAudioScene = FALSE
	ENDIF
ENDPROC

PROC setVarOnStream()
	GET_POSITION_OF_ANALOGUE_STICKS(PAD1,LSX,LSY,RSX,RSY)
	f_debug_sound_variable = ((TO_FLOAT(LSY+128)/255) * (f_debug_sound_variable_max - f_debug_sound_variable_min)) + f_debug_sound_variable_min
	s_variable_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_variable_name)
	IF f_debug_sound_variable_last != f_debug_sound_variable
		PRINTSTRING("Setting ")
		PRINTSTRING(s_variable_name)
		PRINTSTRING(" to ")
		PRINTFLOAT(f_debug_sound_variable)
		if b_streamIsPlaying //WE ARE IN REGULAR MODE
			PRINTSTRING(" NORMAL MODE")
			SET_VARIABLE_ON_STREAM(s_variable_name,f_debug_sound_variable)
		ELSE //WE ARE IN UW MODE
			PRINTSTRING(" UNDERWATER MODE")
			SET_VARIABLE_ON_UNDER_WATER_STREAM(s_variable_name, f_debug_sound_variable)
		ENDIF
		PRINTNL()
		f_debug_sound_variable_last = f_debug_sound_variable
	ENDIF
ENDPROC

PROC ScriptedStreamTest()
	IF b_loadStream
		//Load a stream
		s_scriptedStreamName = GET_CONTENTS_OF_TEXT_WIDGET(widget_script_stream_name)

		IF b_use_SoundSet
			s_soundSet_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_soundSet_name)
		ELSE 
			s_soundSet_name = NULL
		ENDIF

		IF (NOT LOAD_STREAM(s_scriptedStreamName,s_soundSet_name) AND (i_streamLoadTryCount < i_streamLoadTryCountLimit))
			PRINTSTRING("LOADING ")
			PRINTSTRING(s_scriptedStreamName)
			PRINTINT(i_streamLoadTryCount)
			PRINTNL()
			++i_streamLoadTryCount
		ELSE
			IF i_streamLoadTryCount = i_streamLoadTryCountLimit
				PRINTSTRING("Load of ")
				PRINTSTRING(s_scriptedStreamName)
				PRINTSTRING(" failed! Is name bad?")
				PRINTNL()
			ELSE
				PRINTSTRING("Load of ")
				PRINTSTRING(s_scriptedStreamName)
				PRINTSTRING(" suceeded!")
				PRINTNL()
			ENDIF
			b_loadStream = FALSE
			i_streamLoadTryCount = 0
		ENDIF
	ENDIF
	
	IF b_playStream
		IF load_stream(s_scriptedStreamName,s_soundSet_name)
			IF i_streamplaybackMode = 0
				PLAY_STREAM_FROM_PED(PLAYER_PED_ID())
			ELIF i_streamplaybackMode = 1
				PLAY_STREAM_FROM_POSITION(GET_ENTITY_COORDS(GET_ENTITY_FROM_PED_OR_VEHICLE(PLAYER_PED_ID())))
			ELIF i_streamplaybackMode = 1
				PLAY_STREAM_FROM_VEHICLE( GET_CLOSEST_VEHICLE( GET_ENTITY_COORDS(PLAYER_PED_ID()),20,TAILGATER,0))
			ELSE
				PLAY_STREAM_FRONTEND()
			ENDIF
			IF b_bank_test_up_dbg_var_on_start
				setVarOnStream()
			ENDIF
			b_streamIsPlaying = true
		ELSE
			PRINTSTRING("Need to load it first!")
			PRINTNL()
		ENDIF
		b_playStream = FALSE
	ENDIF

	IF b_bank_test_update_debug_var AND b_streamIsPlaying
		setVarOnStream()
	ENDIF
	
	IF b_stopStream 
		IF b_streamIsPlaying
			STOP_STREAM()
		ELSE
			PRINTSTRING("Need to play it first!")
			PRINTNL()
		ENDIF
		b_streamIsPlaying = FALSE
		b_stopStream = FALSE
	ENDIF

	IF b_overrideUnderwaterStream
		s_scriptedStreamName = GET_CONTENTS_OF_TEXT_WIDGET(widget_script_stream_name)
		if not b_overrideUnderwaterStreamApplied
			OVERRIDE_UNDERWATER_STREAM(s_scriptedStreamName, TRUE)
			b_overrideUnderwaterStreamApplied = TRUE
			PRINTSTRING("Overriding underwater stream with: ")
			PRINTSTRING(s_scriptedStreamName)
			PRINTNL()
		endif
		IF b_bank_test_update_debug_var
			setVarOnStream()
		ENDIF
	ELSE
		if b_overrideUnderwaterStreamApplied
			OVERRIDE_UNDERWATER_STREAM(s_scriptedStreamName, FALSE)
			b_overrideUnderwaterStreamApplied = FALSE
			PRINTSTRING("Cancelling override of underwater stream with: ")
			PRINTSTRING(s_scriptedStreamName)
			PRINTNL()
		ENDIF
	endIF
	

			
ENDPROC


PROC DynamicMixerTest()
	IF b_dynamicMixerStart
		s_dynamicMixerName = GET_CONTENTS_OF_TEXT_WIDGET(widget_dynamic_mixer)
		IF IS_AUDIO_SCENE_ACTIVE(s_dynamicMixerName)
			PRINTSTRING(s_dynamicMixerName)
			PRINTSTRING(" is alread active!")
			PRINTNL()
		ELSE
			START_AUDIO_SCENE(s_dynamicMixerName)
		ENDIF
		b_dynamicMixerStart = FALSE
	ENDIF

	IF b_dynamicMixerStop
		s_dynamicMixerName = GET_CONTENTS_OF_TEXT_WIDGET(widget_dynamic_mixer)
		IF NOT IS_AUDIO_SCENE_ACTIVE(s_dynamicMixerName)
			PRINTSTRING(s_dynamicMixerName)
			PRINTSTRING(" can't stop - it isn't playing")
			PRINTNL()
		ELSE
			STOP_AUDIO_SCENE(s_dynamicMixerName)
		ENDIF
		b_dynamicMixerStop = FALSE
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE(s_dynamicMixerName)
		IF b_dynamicMixerUseVariable
			s_dynamicMixerVariableName = GET_CONTENTS_OF_TEXT_WIDGET(widget_dynamic_mixer_variable_name)
			IF b_dynamicMixerApplyWithPad
				GET_POSITION_OF_ANALOGUE_STICKS(PAD1,LSX,LSY,RSX,RSY)
				f_dynamicMixerVariableValue = TO_FLOAT(LSY+128)/255
			ENDIF
			SET_AUDIO_SCENE_VARIABLE(s_dynamicMixerName,s_dynamicMixerVariableName,f_dynamicMixerVariableValue)
		ENDIF
	ENDIF

	if b_dynamicMixerTestGroups
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			if b_dynamicMixerAddVehicleToMixGroup
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(GET_ENTITY_FROM_PED_OR_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())), GET_CONTENTS_OF_TEXT_WIDGET(widget_dynamic_mixer_group_name))
				PRINTSTRING("PLAYER VEHICLE ADDED TO MIX GROUP")
				PRINTNL()
				b_dynamicMixerAddVehicleToMixGroup = FALSE
			ENDIF
			IF b_dynamicMixerRemoveVehicleFromMixGroup 
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(GET_ENTITY_FROM_PED_OR_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
				PRINTSTRING("PLAYER VEHICLE REMOVED FROM MIX GROUP")
				PRINTNL()
				b_dynamicMixerRemoveVehicleFromMixGroup = FALSE
			ENDIF
		ELSE 
			PRINTSTRING("PLAYER NOT IN A VEHICLE - This Mixgroup debug functionality needs you in a vehicle!")
			PRINTNL()
			b_dynamicMixerTestGroups = FALSE
		ENDIF
	ENDIF


ENDPROC

PROC do_custom_fn()
	IF 	b_doCustomFunction
		if not IS_AUDIO_SCENE_ACTIVE("EXILE_TEST")
			START_AUDIO_SCENE("EXILE_TEST")
		ENDIF
		
		v_player_pos = GET_PLAYER_COORDS(GET_PLAYER_INDEX())
		
		f_crimeZ = (v_player_pos.z-60)/(70-60)
		
		if f_crimeZ > 1
			f_crimeZ = 1
		ENDIF
		if f_crimeZ < 0
			f_crimeZ = 0
		ENDIF
	
		if IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))		
				SET_AUDIO_SCENE_VARIABLE("EXILE_TEST","apply",f_crimeZ)
				PRINTSTRING("In the air! ")
				PRINTFLOAT(f_crimeZ)
				PRINTNL()
			ELSE	
				PRINTSTRING("Not in air. ")
				PRINTFLOAT(f_crimeZ)
				PRINTNL()
				SET_AUDIO_SCENE_VARIABLE("EXILE_TEST","apply",0)
			ENDIF
		ENDIF
	ELIF IS_AUDIO_SCENE_ACTIVE("EXILE_TEST")
		STOP_AUDIO_SCENE("EXILE_TEST")
	ENDIF

/*	
		IF REQUEST_SCRIPT_AUDIO_BANK("FBI_03_Panic")
			PLAY_SOUND_FROM_ENTITY(-1,"Party_Panic",PLAYER_PED_ID(),"FBI_03_Torture_Sounds")
			SET_PED_WALLA_DENSITY(1,1)
			FORCE_PED_PANIC_WALLA()
		ENDIF

*/

/*  SHOOTYSHOOTY

		//VECTOR v_interpPos = GET_ENTITY_COORDS(PLAYER_PED_ID())*f_CustomFunction + GET_CAM_COORD(GET_DEBUG_CAM()) * (1 - f_CustomFunction)
		
		
		PLAY_SOUND_FROM_COORD(-1, "SNIPER_HIT_SPLAT", GET_CAM_COORD(GET_DEBUG_CAM()),"DOCKS_HEIST_FINALE_2A_SOUNDS")
		PLAY_SOUND_FROM_COORD(-1, "SNIPER_HIT_ZIP", GET_ENTITY_COORDS(PLAYER_PED_ID())* 0.05 + GET_CAM_COORD(GET_DEBUG_CAM()) * 0.95,"DOCKS_HEIST_FINALE_2A_SOUNDS")
		PLAY_SOUND_FROM_COORD(-1, "SNIPER_HIT_SPRAY", GET_ENTITY_COORDS(PLAYER_PED_ID())* -0.05 + GET_CAM_COORD(GET_DEBUG_CAM()) * 1.05,"DOCKS_HEIST_FINALE_2A_SOUNDS")
*/		


/*	PAD DEBUG
	
INT i_customFunctionCount1 = 0
INT i_customFunctionCount2 = 0
INT i_customFunctionCount3 = 0
INT i_customFunctionCount4 = 0

	

		//LEFT
		if GET_BUTTON_VALUE(PAD1, DPADLEFT) > i_customFunctionCount1
			PRINTSTRING("LEFT: ")
			PRINTINT(GET_BUTTON_VALUE(PAD1, DPADLEFT))		
			PRINTNL()
			i_customFunctionCount1 = GET_BUTTON_VALUE(PAD1, DPADLEFT)
		ENDIF
		
		//DOWN
		if GET_BUTTON_VALUE(PAD1, DPADDOWN) > i_customFunctionCount2
			PRINTSTRING("DOWN: ")
			PRINTINT(GET_BUTTON_VALUE(PAD1, DPADDOWN))
			PRINTNL()
			i_customFunctionCount2 = GET_BUTTON_VALUE(PAD1, DPADDOWN)
		ENDIF
		
		//RIGHT
		if GET_BUTTON_VALUE(PAD1, DPADRIGHT) > i_customFunctionCount3
			PRINTSTRING("RIGHT: ")
			PRINTINT(GET_BUTTON_VALUE(PAD1, DPADRIGHT))		
			PRINTNL()
			i_customFunctionCount3 = GET_BUTTON_VALUE(PAD1, DPADRIGHT)
		ENDIF
		
		//UP
		if GET_BUTTON_VALUE(PAD1, DPADUP) > i_customFunctionCount4
			PRINTSTRING("UP: ")
			PRINTINT(GET_BUTTON_VALUE(PAD1, DPADUP))
			PRINTNL()
			i_customFunctionCount4 = GET_BUTTON_VALUE(PAD1, DPADUP)
		ENDIF		
*/

/*		PRINTSTRING("Warping up 1000m, giving chute")
		PRINTNL()
		REQUEST_WEAPON_ASSET(GADGETTYPE_PARACHUTE)
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),GADGETTYPE_PARACHUTE,1,TRUE)
		VECTOR skydiveStart = GET_GAMEPLAY_CAM_COORD()
		skydiveStart.z += 1000.0
		SET_ENTITY_COORDS(PLAYER_PED_ID(),skydiveStart)
		b_doCustomFunction = FALSE
*/
	
ENDPROC

PROC setupYankton()
	IF b_loadYankton and not b_yanktonLoaded 
		b_yanktonLoaded = TRUE
		VECTOR vPlayerStart = <<5311,-5212,86>>
		
		//Interior
		interior_Instance_Index intDepot = GET_INTERIOR_AT_COORDS_WITH_TYPE(vPlayerStart, "V_CashDepot")
		
		PIN_INTERIOR_IN_MEMORY(intDepot)
		WHILE NOT IS_INTERIOR_READY(intDepot)
			WAIT(0)
		ENDWHILE

		//IPL Groups
		REQUEST_IPL("prologue01")
		REQUEST_IPL("prologue02")
		REQUEST_IPL("prologue03")
		REQUEST_IPL("prologue04")
		REQUEST_IPL("prologue05")
		REQUEST_IPL("prologue06")
		REQUEST_IPL("prologuerd")
		
		//Clear Area
		CLEAR_AREA(vPlayerStart, 1000.0, TRUE)
		
		//Minimap
		SET_MINIMAP_IN_PROLOGUE(TRUE)

		//Cellphone
		g_Use_Prologue_Cellphone = TRUE
			
		//WARP
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<5311,-5212,86>>)
		ENDIF
		
	ELIF not b_loadYankton and b_yanktonLoaded
		//Minimap
		SET_MINIMAP_IN_PROLOGUE(FALSE)
	
		//Phone
		g_Use_Prologue_Cellphone = FALSE
	
		//IPL Groups
		REMOVE_IPL("prologue01")
		REMOVE_IPL("prologue02")
		REMOVE_IPL("prologue03")
		REMOVE_IPL("prologue04")
		REMOVE_IPL("prologue05")
		REMOVE_IPL("prologue06")
		REMOVE_IPL("prologuerd")
	ENDIF
ENDPROC

PROC setVarOnSound(INT i_soundID)
	GET_POSITION_OF_ANALOGUE_STICKS(PAD1,LSX,LSY,RSX,RSY)
	f_debug_sound_variable = ((TO_FLOAT(LSY+128)/255) * (f_debug_sound_variable_max - f_debug_sound_variable_min)) + f_debug_sound_variable_min
	
	IF f_debug_sound_variable_last != f_debug_sound_variable
		s_variable_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_variable_name)
		PRINTSTRING("Setting ")
		PRINTSTRING(s_variable_name)
		PRINTSTRING(" to ")
		PRINTFLOAT(f_debug_sound_variable)
		PRINTNL()
		SET_VARIABLE_ON_SOUND(i_soundID,s_variable_name,f_debug_sound_variable)
		f_debug_sound_variable_last = f_debug_sound_variable
	ENDIF
ENDPROC

PROC playTestSound(INT i_soundID, STRING soundName)
	IF b_use_SoundSet
		s_soundset_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_soundSet_name)
	ELSE
		s_soundSet_name = NULL
	ENDIF
	
	IF HAS_SOUND_FINISHED(i_soundID)
		SWITCH i_bankTestPlayFrom
			CASE 0
				PLAY_SOUND_FROM_ENTITY(i_soundID,soundName,PLAYER_PED_ID(),s_soundSet_name)
			BREAK
			CASE 1
				PLAY_SOUND_FROM_ENTITY(i_soundID,soundName,GET_ENTITY_FROM_PED_OR_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())),s_soundSet_name)
			BREAK
			CASE 2
				PLAY_SOUND_FRONTEND(i_soundID,soundName,s_soundSet_name)
			BREAK
			CASE 3
				PLAY_SOUND_FROM_COORD(i_soundID,soundName,GET_CAM_COORD(GET_DEBUG_CAM()),s_soundSet_name)
			BREAK
		ENDSWITCH
		IF b_bank_test_up_dbg_var_on_start
				f_debug_sound_variable_last = f_debug_sound_variable + 1
				setVarOnSound(i_soundID)
		ENDIF
	ELSE
		STOP_SOUND(i_soundID)
	ENDIF
ENDPROC

PROC BankSoundTest()

	IF b_setScriptCleanupTimeout
		SET_AUDIO_SCRIPT_CLEANUP_TIME(i_scriptCleanupTimeout)
		b_setScriptCleanupTimeout = FALSE
		PRINTSTRING("Script Cleanup Timeout set to ")
		PRINTINT(i_scriptCleanupTimeout)
		PRINTNL()
	ENDIF

	IF IS_PLAYER_PLAYING(PLAYER_ID()) 
		IF b_lockControls
			IF NOT b_bank_test_controls_locked
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				b_bank_test_controls_locked = TRUE
			ENDIF
		ELSE
			IF b_bank_test_controls_locked
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				b_bank_test_controls_locked = FALSE
			ENDIF
		ENDIF
		
	ENDIF

	IF b_releaseBank
		if i_bank_to_release = -1
			RELEASE_SCRIPT_AUDIO_BANK()
		ELSE
			SWITCH i_bank_to_release
				CASE 0			
					s_bank_name = "SCRIPT\\"
					s_bank_name += GET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name)
					s_bank_name += GET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name_1)
					RELEASE_NAMED_SCRIPT_AUDIO_BANK(s_bank_name)
				BREAK
				CASE 1
					s_bank_name_2 = "SCRIPT\\"
					s_bank_name_2 += GET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name_2)
					RELEASE_NAMED_SCRIPT_AUDIO_BANK(s_bank_name_2)
				BREAK
				CASE 2
					s_bank_name_3 = "SCRIPT\\"
					s_bank_name_3 += GET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name_3)
					RELEASE_NAMED_SCRIPT_AUDIO_BANK(s_bank_name_3)
				BREAK
				CASE 3
					s_bank_name_4 = "SCRIPT\\"
					s_bank_name_4 += GET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name_4)
					RELEASE_NAMED_SCRIPT_AUDIO_BANK(s_bank_name_4)
				BREAK
				CASE 4
					s_bank_name_5 = "SCRIPT\\"
					s_bank_name_5 += GET_CONTENTS_OF_TEXT_WIDGET(widget_bank_name_5)
					RELEASE_NAMED_SCRIPT_AUDIO_BANK(s_bank_name_5)
				BREAK
			ENDSWITCH
		ENDIF
		b_releaseBank = FALSE
	ENDIF


	IF b_reloadBank
		IF LoadAllBanks()
			b_reloadBank = FALSE	
		ENDIF
	ENDIF
	
	IF b_hint_banks
		IF hintAllBanks()
			b_hint_banks = FALSE	
		ENDIF
	ENDIF
	
	IF b_bank_test_update_debug_var 
		IF NOT HAS_SOUND_FINISHED(i_testSoundId0)
			setVarOnSound(i_testSoundId0)
		ENDIF
		IF NOT HAS_SOUND_FINISHED(i_testSoundId1)
			setVarOnSound(i_testSoundId1)
		ENDIF
		IF NOT HAS_SOUND_FINISHED(i_testSoundId2)
			setVarOnSound(i_testSoundId2)
		ENDIF
		IF NOT HAS_SOUND_FINISHED(i_testSoundId3)
			setVarOnSound(i_testSoundId3)
		ENDIF
	ENDIF
		
	s_circle_sound_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_circle_sound_name)
	s_square_sound_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_square_sound_name)
	s_triangle_sound_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_triangle_sound_name)
	s_cross_sound_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_cross_sound_name)	
	if b_bank_test_controls_locked
		IF IS_BUTTON_JUST_PRESSED(PAD1,SQUARE)
			playTestSound(i_testSoundId0, s_square_sound_name)
		ENDIF
		IF IS_BUTTON_JUST_PRESSED(PAD1,CIRCLE)
			playTestSound(i_testSoundId1, s_circle_sound_name)
		ENDIF
		IF IS_BUTTON_JUST_PRESSED(PAD1,TRIANGLE)
			playTestSound(i_testSoundId2, s_triangle_sound_name)
		ENDIF
		IF IS_BUTTON_JUST_PRESSED(PAD1,CROSS)
			playTestSound(i_testSoundId3, s_cross_sound_name)
		ENDIF	
	ENDIF
ENDPROC

PROC wallaTest()
	IF b_override_walla
		SET_PED_WALLA_DENSITY(f_walla_density, f_walla_factor)
		PRINTSTRING("Walla Ped Density set to: ")
		PRINTFLOAT(f_walla_density)
		PRINTSTRING(" Apply value: ")
		PRINTFLOAT(f_walla_density)
		PRINTNL()
		b_override_walla = FALSE
	ENDIF
ENDPROC

PROC zoneListTest()
	//Zones / Zonelists have three states - on, off, default - when set by script temporarily
	//Zones / Zonelists have two states (on / off) when set by script permanently
	if b_zoneListApply
		//Grab zone / list name
		s_zoneListName = GET_CONTENTS_OF_TEXT_WIDGET(widget_zonelist_name)
		PRINTSTRING(s_zoneListName)
		SWITCH i_zoneListFunction
			CASE 0
				// Zone On Temp
				PRINTSTRING(" - Zone On Temp")
				PRINTNL()
				SET_AMBIENT_ZONE_STATE(s_zoneListName, TRUE, b_zoneForceChange)
			BREAK
			CASE 1
				// Zone Off Temp
				PRINTSTRING(" - Zone Off Temp")
				PRINTNL()
				SET_AMBIENT_ZONE_STATE(s_zoneListName, FALSE, b_zoneForceChange)
			BREAK
			CASE 2
				// Zone Reset Temp
				PRINTSTRING(" - Zone Reset Temp")
				PRINTNL()
				CLEAR_AMBIENT_ZONE_STATE(s_zoneListName, b_zoneForceChange)
			BREAK
			CASE 3
				// Zone On Persistent		
				PRINTSTRING(" - Zone On Persistent")
				PRINTNL()
				SET_AMBIENT_ZONE_STATE_PERSISTENT(s_zoneListName,TRUE, b_zoneForceChange)
			BREAK
			CASE 4
				// Zone Off Persistent		
				PRINTSTRING(" - Zone Off Persistent")
				PRINTNL()
				SET_AMBIENT_ZONE_STATE_PERSISTENT(s_zoneListName,FALSE, b_zoneForceChange)
			BREAK
			CASE 5
				// ZoneList On Temp
				PRINTSTRING(" - ZoneList On Temp")
				PRINTNL()
				SET_AMBIENT_ZONE_LIST_STATE(s_zoneListName, TRUE, b_zoneForceChange)
			BREAK
			CASE 6
				// ZoneList Off Temp
				PRINTSTRING(" - ZoneList Off Temp")
				PRINTNL()
				SET_AMBIENT_ZONE_LIST_STATE(s_zoneListName, FALSE, b_zoneForceChange)
			BREAK
			CASE 7
				// ZoneList Reset Temp
				PRINTSTRING(" - ZoneList Reset Temp")
				PRINTNL()
				CLEAR_AMBIENT_ZONE_LIST_STATE(s_zoneListName, b_zoneForceChange)
			BREAK
			CASE 8
				// ZoneList On Persistent	
				PRINTSTRING(" - ZoneList On Persistent")
				PRINTNL()
				SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT(s_zoneListName, TRUE, b_zoneForceChange)
			BREAK
			CASE 9
				// ZoneList Off Persistent
				PRINTSTRING(" - ZoneList Off Persistent")
				PRINTNL()
				SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT(s_zoneListName, FALSE, b_zoneForceChange)
			BREAK
		ENDSWITCH
		b_zoneListApply = FALSE
	ENDIF

ENDPROC

FUNC VECTOR SPEAKER_GET_NEW_COORDS()
		FLOAT f_random_distance = GET_RANDOM_FLOAT_IN_RANGE(f_speaker_min_dist,f_speaker_max_dist)
		FLOAT f_random_angle = GET_RANDOM_FLOAT_IN_RANGE(0,360)
		VECTOR new_pos = v_player_pos + <<f_random_distance*SIN(f_random_angle),f_random_distance*COS(f_random_angle),0>>
		GET_GROUND_Z_FOR_3D_COORD(new_pos,new_pos.z)
		RETURN new_pos
ENDFUNC

PROC MakeSpeakers()
	FOR i = 0 to i_numberOfSpeakers - 1
		IF DOES_ENTITY_EXIST(pi_Speakers[i])
			DELETE_PED(pi_Speakers[i])
		ENDIF
	ENDFOR
	
	REQUEST_MODEL(model_shooter)
	WHILE NOT HAS_MODEL_LOADED(model_shooter)
		REQUEST_MODEL(model_shooter)
		PRINTSTRING("Loading Models (init)")
		PRINTNL()
		WAIT(0)
	ENDWHILE

	FOR i = 0 to i_numberOfSpeakers - 1
		PRINTSTRING("Creating ped") PRINTINT(i)	PRINTNL()
		IF NOT DOES_ENTITY_EXIST(pi_Speakers[i])
			v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			VECTOR v_new_speaker_pos = SHOOTER_GET_NEW_COORDS()
			pi_Speakers[i] = CREATE_PED(PEDTYPE_CIVMALE, model_shooter, v_new_speaker_pos,GetHeadingBetweenVectors(v_new_speaker_pos, v_player_pos))
			PRINTSTRING("Created ped") PRINTINT(i)	PRINTNL()
		ENDIF
	ENDFOR
ENDPROC

PROC CleanupSpeakers()
	STOP_SCRIPTED_CONVERSATION(FALSE)

	FOR i = 0 to i_numberOfSpeakers - 1
		IF DOES_ENTITY_EXIST(pi_Speakers[i])
			DELETE_PED(pi_Speakers[i])
		ENDIF
	ENDFOR
	SET_MODEL_AS_NO_LONGER_NEEDED(model_shooter)
ENDPROC

PROC conversationTest()
	IF NOT b_speakers_made
		MakeSpeakers()
		b_speakers_made = TRUE
// KEITH 5/3/13: Removing CnC Globals - I can't find these referenced anywhere else so I've just commented them out. Hope that's ok.
//		GlobalServerBD_CNC.CodeNames[NATIVE_TO_INT(GET_PLAYER_INDEX())].CodeName.iCodeUnit = 15
//		GlobalServerBD_CNC.CodeNames[NATIVE_TO_INT(GET_PLAYER_INDEX())].CodeName.iFirstNumber = 4
		//SET_PLAYER_FOR_CONVERSATION_CODE_NAME(GET_PLAYER_INDEX())
	ENDIF
	
	IF b_start_conversation
		b_start_conversation = FALSE
		ADD_PED_FOR_DIALOGUE(conversationPedStruct, 0, pi_Speakers[0], GET_CONTENTS_OF_TEXT_WIDGET(widget_voice0_name))
		ADD_PED_FOR_DIALOGUE(conversationPedStruct, 1, pi_Speakers[1], GET_CONTENTS_OF_TEXT_WIDGET(widget_voice1_name))
		ADD_PED_FOR_DIALOGUE(conversationPedStruct, 2, pi_Speakers[2], GET_CONTENTS_OF_TEXT_WIDGET(widget_voice2_name))
		ADD_PED_FOR_DIALOGUE(conversationPedStruct, 3, pi_Speakers[3], GET_CONTENTS_OF_TEXT_WIDGET(widget_voice3_name))
		ADD_PED_FOR_DIALOGUE(conversationPedStruct, 4, pi_Speakers[4], GET_CONTENTS_OF_TEXT_WIDGET(widget_voice4_name))
		ADD_PED_FOR_DIALOGUE(conversationPedStruct, 5, pi_Speakers[5], GET_CONTENTS_OF_TEXT_WIDGET(widget_voice5_name))
		ADD_PED_FOR_DIALOGUE(conversationPedStruct, 6, pi_Speakers[6], GET_CONTENTS_OF_TEXT_WIDGET(widget_voice6_name))
		ADD_PED_FOR_DIALOGUE(conversationPedStruct, 7, pi_Speakers[7], GET_CONTENTS_OF_TEXT_WIDGET(widget_voice7_name))
		ADD_PED_FOR_DIALOGUE(conversationPedStruct, 8, pi_Speakers[8], GET_CONTENTS_OF_TEXT_WIDGET(widget_voice8_name))
		
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			CREATE_CONVERSATION(conversationPedStruct, GET_CONTENTS_OF_TEXT_WIDGET(widget_conversation_text_block_name), 
				GET_CONTENTS_OF_TEXT_WIDGET(widget_conversation_name), CONV_PRIORITY_HIGH)
		ENDIF
	ENDIF
	IF b_stop_conversation
		STOP_SCRIPTED_CONVERSATION(FALSE)
	ENDIF
	

ENDPROC



FUNC STRING IntToLetter(INT number)
	SWITCH number
		CASE 0
			return "A"
		CASE 1
			return "B"
		CASE 2
			return "C"
		CASE 3
			return "D"
		CASE 4
			return "E"
		CASE 5
			return "F"
		CASE 6
			return "G"
		CASE 7
			return "H"
		CASE 8
			return "I"
		CASE 9
			return "J"
		CASE 10
			return "K"
		CASE 11
			return "L"
		CASE 12
			return "M"
		CASE 13
			return "N"
		CASE 14
			return "O"
		CASE 15
			return "P"
		CASE 16
			return "Q"
	ENDSWITCH
	//SHOULDNT GET HERE - RETURN Z IN CASE
	RETURN "Z"
ENDFUNC

PROC REMOVE_ALL_ANIM_DICTS()
	FOR i = 0 to i_numberOfConversationChunks
		//Request all animations
		s_anim_dict_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_anim_dictionary_name)
		s_anim_dict_name += GET_CONTENTS_OF_TEXT_WIDGET(widget_anim_dictionary_name_2)
		s_anim_dict_name += IntToLetter(i)
		REMOVE_ANIM_DICT(s_anim_dict_name)
	ENDFOR
ENDPROC

PROC KillAnimConv
		STOP_SCRIPTED_CONVERSATION(FALSE)
		IF b_LoadAnims
			STOP_ANIM_PLAYBACK(PLAYER_PED_ID())
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			REMOVE_ALL_ANIM_DICTS()
			REMOVE_ANIM_DICT(GET_CONTENTS_OF_TEXT_WIDGET(widget_anim_dictionary_name))
		ENDIF
ENDPROC

FUNC BOOL haveAllAnimsLoaded()
	b_anim_HaveAllAnimsLoaded = TRUE
	FOR i = 0 to i_numberOfConversationChunks
		//Request all animations
		s_anim_dict_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_anim_dictionary_name)
		s_anim_dict_name += GET_CONTENTS_OF_TEXT_WIDGET(widget_anim_dictionary_name_2)
		s_anim_dict_name += IntToLetter(i)
		IF NOT HAS_ANIM_DICT_LOADED(s_anim_dict_name)
			b_anim_HaveAllAnimsLoaded = FALSE
		ENDIF
	ENDFOR
	RETURN b_anim_HaveAllAnimsLoaded
ENDFUNC

PROC setCurrentAnimNameAndDict()
	s_anim_dict_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_anim_dictionary_name)
	s_anim_dict_name += GET_CONTENTS_OF_TEXT_WIDGET(widget_anim_dictionary_name_2)
	s_anim_dict_name += IntToLetter(i_animCurrentChunk)
	s_anim_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_anim_name)
	s_anim_name += GET_CONTENTS_OF_TEXT_WIDGET(widget_anim_name2)
	s_anim_name += "_"
	s_anim_name += i_animCurrentChunk
ENDPROC

						
FUNC BOOL IS_ANIM_PLAYING_ON_PED()
	IF ((GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PLAY_ANIM) <> PERFORMING_TASK)
	AND (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PLAY_ANIM) <> WAITING_TO_START_TASK))
		return FALSE
	ENDIF
	return TRUE
ENDFUNC


PROC animLinkedConvTest()

	if b_start_conversation
		Switch i_anim_load_stage
			CASE 0
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF b_LoadAnims
						//Load dem anims:
						i_animCurrentChunk = 0
						FOR i = 0 to i_numberOfConversationChunks
							//Request all animations
							s_anim_dict_name = GET_CONTENTS_OF_TEXT_WIDGET(widget_anim_dictionary_name)
							s_anim_dict_name += GET_CONTENTS_OF_TEXT_WIDGET(widget_anim_dictionary_name_2)
							s_anim_dict_name += IntToLetter(i)
							REQUEST_ANIM_DICT(s_anim_dict_name)
						ENDFOR
					ENDIF
					START_AUDIO_SCENE("MAGDEMO_JESSE_SCENE")
					IF b_LoadAnims
						IF haveAllAnimsLoaded()
							SETTIMERA(0)
							PRINTSTRING("Anims Loaded")
							PRINTNL()
							SET_CONVERSATION_AUDIO_CONTROLLED_BY_ANIM(TRUE)
							ADD_PED_FOR_DIALOGUE(conversationPedStruct,i_ped_number,PLAYER_PED_ID(),GET_CONTENTS_OF_TEXT_WIDGET(widget_voice0_name),FALSE)
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(PLAYER_PED_ID(),"MAGDEMO_JESSE_MIX_GROUP")
							CREATE_CONVERSATION(conversationPedStruct, GET_CONTENTS_OF_TEXT_WIDGET(widget_conversation_text_block_name), GET_CONTENTS_OF_TEXT_WIDGET(widget_conversation_name), CONV_PRIORITY_HIGH)
							PRINTSTRING("Conversation preparing! ")
							PRINTINT(i_anim_load_stage)						
							PRINTNL()
							i_anim_load_stage++
						ELSE
							PRINTSTRING("Waiting for Anim Dicts! ")
							PRINTINT(i_anim_load_stage)						
							PRINTNL()
						ENDIF
					ELSE
						SETTIMERA(0)
						PRINTSTRING("Skipping Anim loading")
						PRINTNL()
						SET_CONVERSATION_AUDIO_CONTROLLED_BY_ANIM(TRUE)
						ADD_PED_FOR_DIALOGUE(conversationPedStruct,i_ped_number,PLAYER_PED_ID(),GET_CONTENTS_OF_TEXT_WIDGET(widget_voice0_name),FALSE)
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(PLAYER_PED_ID(),"MAGDEMO_JESSE_MIX_GROUP")
						CREATE_CONVERSATION(conversationPedStruct, GET_CONTENTS_OF_TEXT_WIDGET(widget_conversation_text_block_name), GET_CONTENTS_OF_TEXT_WIDGET(widget_conversation_name), CONV_PRIORITY_HIGH)
						PRINTSTRING("Conversation preparing! ")
						PRINTINT(i_anim_load_stage)						
						PRINTNL()
						i_anim_load_stage++
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF TIMERA()>1000
					IF b_LoadAnims
						PRINTSTRING("TRIGGERING CONVERSATION! ")
						PRINTINT(i_anim_load_stage)						
						PRINTNL()
						//Get names for anim and dict for the current chunk
						setCurrentAnimNameAndDict()
						TASK_PLAY_ANIM(PLAYER_PED_ID(),s_anim_dict_name,s_anim_name)
					ELSE
						PRINTSTRING("Conversation preloaded - trigger first anim ")
						PRINTINT(i_anim_load_stage)			
						PRINTNL()						
					ENDIF
					i_anim_load_stage++
				ENDIF
			BREAK
			CASE 2
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PRINTSTRING("Finished convo - turning off Anim Synched Dialogue ")
					PRINTINT(i_anim_load_stage)
					PRINTNL()
					SET_CONVERSATION_AUDIO_CONTROLLED_BY_ANIM(FALSE)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(PLAYER_PED_ID())
					STOP_AUDIO_SCENE("MAGDEMO_JESSE_SCENE")
					b_start_conversation = FALSE
					i_anim_load_stage = 0
				ELSE
					if i_animCurrentChunk < i_numberOfConversationChunks
						if not IS_ANIM_PLAYING_ON_PED()
							i_animCurrentChunk++
							setCurrentAnimNameAndDict()
							TASK_PLAY_ANIM(PLAYER_PED_ID(),s_anim_dict_name,s_anim_name)
							PRINTSTRING("Anim finished! Starting ")
							PRINTINT(i_animCurrentChunk)			
							PRINTNL()						
						ENDIF
					ELSE	
							PRINTSTRING("Anims all finished!")
							PRINTNL()						
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF

	IF b_stop_conversation
		b_stop_conversation = FALSE
		KillAnimConv()
	ENDIF	
	
ENDPROC

PROC floorGlassTest()
	if b_glassToggle
		if b_glassIsOn
			//TURN OFF GLASS
			PRINTSTRING("Clearing up!")
			PRINTNL()
			CLEAR_ALL_BROKEN_GLASS()
			b_glassIsOn = FALSE
		ELSE
			RECORD_BROKEN_GLASS(GET_GAMEPLAY_CAM_COORD(),f_glassRadius)
			b_glassIsOn = TRUE
		ENDIF
		b_glassToggle = FALSE
	endif
ENDPROC

PROC alarmTest()
	s_alarmName = GET_CONTENTS_OF_TEXT_WIDGET(widget_alarm_name)
	b_alarmsArePlaying = IS_ALARM_PLAYING(s_alarmName)
	IF b_alarmsOn
		if not b_alarmsPlaying
			IF PREPARE_ALARM(s_alarmName)
				START_ALARM(s_alarmName,b_alarmsSkipToSustain)
				b_alarmsPlaying = TRUE
				PRINTSTRING("Alarm Started")
				PRINTNL()
			ELSE
				PRINTSTRING("Alarm Preparing")
				PRINTNL()
			ENDIF
		ENDIF
	ELSE
		if b_alarmsPlaying
			STOP_ALARM(s_alarmName,b_alarmsForceState)
			PRINTSTRING("Alarm Stopped")
			PRINTNL()
			b_alarmsPlaying = FALSE
		endif
	ENDIF
	
ENDPROC


PROC emitterTest()
	//TEST EMITTERS

	IF b_emitterOn and not(b_emitterStatus)
		SET_STATIC_EMITTER_ENABLED(GET_CONTENTS_OF_TEXT_WIDGET(widget_emitter_name),TRUE)
		b_emitterStatus = TRUE
		b_emitterOn = FALSE
	ELIF b_emitterOn and b_emitterStatus
		SET_STATIC_EMITTER_ENABLED(GET_CONTENTS_OF_TEXT_WIDGET(widget_emitter_name),FALSE)
		b_emitterStatus = FALSE
		b_emitterOn = FALSE		
	ENDIF

	IF b_emitterRetune
		SET_EMITTER_RADIO_STATION(GET_CONTENTS_OF_TEXT_WIDGET(widget_emitter_name),GET_CONTENTS_OF_TEXT_WIDGET(widget_station_name))
		b_emitterRetune = FALSE
	ENDIF
	
	IF b_emitterSpawnProp
		if DOES_ENTITY_EXIST(obj_boombox)
			DELETE_OBJECT(obj_boombox)
		ENDIF
		obj_boombox = CREATE_OBJECT(PROP_BOOMBOX_01, GET_PLAYER_COORDS(GET_PLAYER_INDEX()) + <<1,1,0>>)
		b_emitterSpawnProp = FALSE
	ENDIF
	
	IF b_emitterLinkToProp
		LINK_STATIC_EMITTER_TO_ENTITY(GET_CONTENTS_OF_TEXT_WIDGET(widget_emitter_name), obj_boombox)
		b_emitterLinkToProp = FALSE
	ENDIF
	
	IF b_emitterToggleScore
		IF PREPARE_MUSIC_EVENT(get_contents_of_Text_widget(widget_music_name))
			PRINTSTRING("Triggering Music Event")
			PRINTNL()
			TRIGGER_MUSIC_EVENT(get_contents_of_Text_widget(widget_music_name))
			b_emitterToggleScore = FALSE
			i_loadTries = 0
		ENDIF
		if i_loadTries > 10
			b_emitterToggleScore = FALSE
			i_loadTries = 0
		ELSE
			i_loadTries += 1
		ENDIF
	ENDIF
	
ENDPROC


// ===========================================================================================================
//		Termination
// ===========================================================================================================


// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC CleanupPanner()
	IF i_sparklyEffect != NULL
		STOP_PARTICLE_FX_LOOPED(i_sparklyEffect)
		i_sparklyEffect = NULL
	ENDIF
	IF DOES_ENTITY_EXIST(obj_cursor)
		DELETE_OBJECT(obj_cursor)
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("AMBISONIC_PANNING_TEST_SCENE")
		STOP_AUDIO_SCENE("AMBISONIC_PANNING_TEST_SCENE")
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(model_cursor)
	SET_OBJECT_AS_NO_LONGER_NEEDED(obj_cursor)
	REMOVE_BLIP(bi_sound_source_blip)
ENDPROC

PROC CleanupShooter()
	IF DOES_ENTITY_EXIST(pi_shooter_01)
		DELETE_PED(pi_shooter_01)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(model_shooter)
	SET_PED_AS_NO_LONGER_NEEDED(pi_shooter_01)
	REMOVE_BLIP(bi_shooter_01)
	b_shooter_ready = FALSE
ENDPROC
PROC CleanupGunMen()
	FOR i = 0 to i_numberOfGunMen - 1
		IF DOES_BLIP_EXIST(bi_GunMen[i])
			REMOVE_BLIP(bi_GunMen[i])
		ENDIF
		IF DOES_ENTITY_EXIST(pi_GunMen[i])
			DELETE_PED(pi_GunMen[i])
		ENDIF
	ENDFOR
	SET_MODEL_AS_NO_LONGER_NEEDED(model_shooter)
ENDPROC

PROC CleanupPoliceScannerTest()
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	b_bank_test_controls_locked = FALSE
ENDPROC

PROC CleanupBankSoundTest()
	RELEASE_AMBIENT_AUDIO_BANK()
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	b_bank_test_controls_locked = FALSE
ENDPROC

PROC CleanupDynamicMixerTest()
	IF IS_AUDIO_SCENE_ACTIVE(s_dynamicMixerName)
		PRINTSTRING("WARNING ")
		PRINTSTRING(s_dynamicMixerName)
		PRINTSTRING(" is still active")
		PRINTNL()
		//STOP_AUDIO_SCENE(s_dynamicMixerName)
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE(s_lastDynamicMixerName)
		PRINTSTRING("WARNING ")
		PRINTSTRING(s_lastDynamicMixerName)
		PRINTSTRING(" is still active")
		PRINTNL()
		//STOP_AUDIO_SCENE(s_lastDynamicMixerName)
	ENDIF
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	b_bank_test_controls_locked = FALSE
ENDPROC

PROC CleanupScriptedStreamTest()
	STOP_STREAM()
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	b_bank_test_controls_locked = FALSE
ENDPROC

PROC CleanupWallaTest()
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	b_bank_test_controls_locked = FALSE
ENDPROC

PROC CleanupZoneListTest()
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	b_bank_test_controls_locked = FALSE
ENDPROC

PROC CleanupConversationTest()
	CleanupSpeakers()
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	b_bank_test_controls_locked = FALSE
ENDPROC

PROC CleanupAnimConversationTest()
	REMOVE_ANIM_DICT(GET_CONTENTS_OF_TEXT_WIDGET(widget_anim_dictionary_name))
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	b_bank_test_controls_locked = FALSE
ENDPROC
PROC CleanupGlass()
	CLEAR_ALL_BROKEN_GLASS()
ENDPROC
PROC CleanupAlarms()
	if b_alarmsPlaying
		STOP_ALARM(s_alarmName,TRUE)
		b_alarmsPlaying = FALSE
	endif
ENDPROC

PROC CleanupEmitters()
	//CLEANUP - NOTHING HERE
ENDPROC


PROC cleanupLastTestModeWidgets()
	DELETE_WIDGET_GROUP(widgets_9)
ENDPROC

PROC cleanupAMode(int ModeToClean)
	SWITCH ModeToClean
		CASE 0
			CleanupPanner()
		BREAK
		CASE 1
			CleanupShooter()
		BREAK
		CASE 2
			CleanupGunMen()
		BREAK
		CASE 3
			CleanupBankSoundTest()
		BREAK
		CASE 4
			CleanupPoliceScannerTest()
		BREAK
		CASE 5
			CleanupDynamicMixerTest()
		BREAK	
		CASE 6
			CleanupScriptedStreamTest()
		BREAK		
		CASE 7
			CleanupWallaTest()
		BREAK
		CASE 8
			CleanupZoneListTest()
		BREAK
		CASE 9
			CleanupConversationTest()
		BREAK
		CASE 10
			CleanupAnimConversationTest()
		BREAK
		CASE 11
			CleanupGlass()
		BREAK
		CASE 12
			CleanupAlarms()
		BREAK
		CASE 13
			CleanupEmitters()
		BREAK
	ENDSWITCH
ENDPROC


PROC cleanupLastTestMode()
	//Cleanup bits from last test mode
	cleanupAMode(i_test_mode_last_frame)

	//Remove last mode's widgets
	cleanupLastTestModeWidgets()
	
	//Create widgets for new mode
	CREATE_WIDGETS()
		
ENDPROC

PROC Mission_Cleanup()
	cleanupAMode(i_test_mode)
	RELEASE_SOUND_ID(i_testSoundId0)
	RELEASE_SOUND_ID(i_testSoundId1)
	RELEASE_SOUND_ID(i_testSoundId2)
	RELEASE_SOUND_ID(i_testSoundId3)
	PRINTSTRING("...Placeholder Mission Cleanup")
	PRINTNL()
	UNREGISTER_SCRIPT_WITH_AUDIO()
	DESTROY_WIDGETS()
	TERMINATE_THIS_THREAD()
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Passed()

	PRINTSTRING("...Placeholder Mission Passed")
	PRINTNL()
	
	//Mission_Flow_Mission_Passed()
	Mission_Cleanup()
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Failed()

	PRINTSTRING("...Placeholder Mission Failed")
	PRINTNL()
	
	//Mission_Flow_Mission_Failed()
	Mission_Cleanup()
	
ENDPROC


// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT

	PRINTSTRING("...Placeholder Mission Launched")
	PRINTNL()
	
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(500)
	ENDIF

	MISSION_SETUP()	

	IF HAS_FORCE_CLEANUP_OCCURRED()
		PRINTSTRING("...Placeholder Mission Force Cleanup")
		PRINTNL()
		Mission_Flow_Mission_Force_Cleanup()
		Mission_Cleanup()
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	WHILE (TRUE)
	
		setupYankton()
		do_custom_fn()
		
		IF i_test_mode != i_test_mode_last_frame
			PRINTLN("Setting new test mod", i_test_mode)
			CleanupLastTestMode()
			SetupTestMode()
			i_test_mode_last_frame = i_test_mode
		ENDIF
	
		SWITCH i_test_mode
			CASE 0
				SoundPanningTest()
			BREAK
			CASE 1
				ShooterTest()
			BREAK
			CASE 2
				GunMenTest()
			BREAK
			CASE 3
				BankSoundTest()
			BREAK
			CASE 4
				PoliceScannerTest()
			BREAK
			CASE 5
				DynamicMixerTest()
			BREAK
			CASE 6
				ScriptedStreamTest()
			BREAK
			CASE 7
				wallaTest()
			BREAK
			CASE 8
				zoneListTest()
			BREAK
			CASE 9
				conversationTest()
			BREAK
			CASE 10
				animLinkedConvTest()
			BREAK
			CASE 11
				floorGlassTest()
			BREAK
			CASE 12
				alarmTest()
			BREAK
			CASE 13
				emitterTest()
			BREAK
		
		ENDSWITCH
		
		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			Mission_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			Mission_Failed()
		ENDIF

	
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

#ENDIF  // IS_DEBUG_BUILD
