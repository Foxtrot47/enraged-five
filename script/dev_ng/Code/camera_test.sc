// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "test_tools.sch"
USING "commands_task.sch"
USING "script_player.sch"
USING "script_DEBUG.sch"
USING "commands_script.sch"
USING "commands_entity.sch"
USING "commands_ped.sch"

//Level 
STRING XMLMenu = "Testbed/TestCamMenu"
VECTOR gvMapOffset //offset all coords from the origin by the psoition of the mapin world coords

//Ped Vars
CONST_INT MAX_NUMBER_OF_PEDS 			10

peds_struct TestPeds [MAX_NUMBER_OF_PEDS]

//Vehicle Vars
CONST_INT MAX_NUMBER_OF_VEHICLES 	 10

vehicle_struct TestVehicles[MAX_NUMBER_OF_VEHICLES]

//Route Vars 
CONST_INT MAX_NUMBER_OF_NODES 				7

RouteStruct Route[MAX_NUMBER_OF_NODES]

//Cam Vars
//Camera Vars 
CONST_INT FixedCamera										0
CONST_INT TrackingCamera									1


CONST_INT MAX_NUMBER_OF_CAMS      10
CONST_INT MAX_NUMBER_OF_CAMeraS      2


CAMERA_INDEX NewTestCams[MAX_NUMBER_OF_CAMS]

StCameraData TestCams[MAX_NUMBER_OF_CAMS]

//Sequence 


//global cam vars
	int gTargetCam = 0
	bool gbCamState = true
	bool gSetCamState = false
	bool gGetCamState = false
	bool gCamRendering = FALSE
	
	vector gCamPos
	Vector gCamRot
	float gCamFov = 1.0
	bool gCamAffectsAiming = TRUE

	int gZoomLevel = 0
	int gViewModeContext = 0

	//Attach
	bool gAttach_ped = FALSE
	bool gAttach_vehicle = FALSE
	bool gAttach_object = FALSE
	bool gDettach = FALSE
	bool gRelative = TRUE
	int gAttach_PedBoneTag = -1
	Vector gAttachOffset = <<0.0, - 1.0,  1.0  >>
	
	//point 
	bool gPoint_ped = FALSE
	bool gPoint_vehicle = FALSE
	bool gPoint_object = FALSE
	bool gStopPoint = FALSE
	bool gStopPointNow = FALSE
	bool gPointRelative = TRUE
	bool gsetCamPos = FALSE
	int gPoint_PedBoneTag = -1
	Vector gPointOffset = <<0.0, 0.0,  0.0  >>
	vector test_Cam_pos 
	int gInterpTime = 0
	
	//Shake 
	float gAmplitude =1.0
	BOOL gbStockShake = FALSE
	BOOL gShouldShakeScriptGlobal = FALSE
	
	//Shake 
	float gAnimationStartPhase = 0.0
	BOOL gbHasStartedAnimatedCamera = FALSE
	BOOL gbHasFinishedAnimatedCamera = FALSE
	
	//fade
	bool bDoSCreenFadeOut = FALSE
	bool bDoSCreenFadeIn = FALSE
	bool isScreenFadedin, isScreenFadedOut, IsScreenFadingIn, IsScreenFadingOut
	int gDuration
	
	//Gameplay
	Bool  gCamVisiblePoint, gpointcoord, gbFollowNearestPed = FALSE, gShouldOverrideFollowPedCamera = FALSE, gShouldOverrideFollowVehicleCamera = FALSE, gShouldIgnoreAttachParentMovementForFollowCamera = FALSE, gShouldApplyZoomFactor = FALSE, gShouldQueryZoomFactor = FALSE, g_ShouldDisableAimCameras = FALSE, gShouldOverrideFirstPersonAimCameraZoomLimits = FALSE, gShouldOverrideFirstPersonAimCameraOrientationLimits = FALSE
	BOOL gShouldOverrideFollowVehicleCameraHighAngleMode = FALSE, gOverriddenFollowVehicleCameraHighAngleMode = FALSE
	float gGameplayHeading
	float gGameplayPitch, gGameplayPitchSmoothRate = 1.0
	float gMinRelativeHeading = -180.0, gMaxRelativeHeading = 180.0, gMinRelativePitch = -90.0, gMaxRelativePitch = 90.0
	float gMinZoomFactor = 1.0, gMaxZoomFactor = 20.0
	int gNumFramesAiming = 0
	TEXT_WIDGET_ID gOverriddenFollowPedCameraTextWidgetId, gOverriddenFollowVehicleCameraTextWidgetId
	STRING gOverriddenFollowPedCameraName = "DEFAULT_FOLLOW_PED_CAMERA", gOverriddenFollowVehicleCameraName = "DEFAULT_FOLLOW_VEHICLE_CAMERA"
	int gOverriddenFollowPedCameraInterpDuration = DEFAULT_PED_CAM_INTERP_TIME, gOverriddenFollowVehicleCameraInterpDuration = DEFAULT_VEHICLE_CAM_INTERP_TIME
	float gZoomFactor = 1.0
	
	//ThirdPerson Limits
	BOOL g_b3rdHeadingLimit  = FALSE
	BOOL g_b3rdPitchLimit    = FALSE
	BOOL g_b3rdDistanceLimit = FALSE
	FLOAT g_f3rdMinHeading   = -30.0
	FLOAT g_f3rdMaxHeading   =  30.0
	FLOAT g_f3rdMinPitch     = -10.0
	FLOAT g_f3rdMaxPitch     =  10.0
	FLOAT g_f3rdMinDistance  =  4.0
	FLOAT g_f3rdMaxDistance  =  5.0
	
	//Debug
	BOOL gbDebugCamActiveState = FALSE
	BOOL gbShouldIgnoreDebugPadCameraToggle = FALSE
	
	//spline 
	bool gbSetSplinePhase = FALSE
	float gfSplinePhase = 0.0
	
	//dof 
	float NearDof = 15.0
	float FarDof = 15.75
	float DofStrength = 1.0
	BOOL gShouldUseShallowDof = TRUE
	BOOL gShouldSetAllDofPlanes = FALSE
	
	//adaptive DOF
	FLOAT g_AdaptiveDofOverriddenFocusDistance = 16.5
	FLOAT g_AdaptiveDofOverriddenFocusDistanceBlendLevel = 1.0
	FLOAT g_AdaptiveDofFocusDistanceGridScalingX = 0.667
	FLOAT g_AdaptiveDofFocusDistanceGridScalingY = 0.667
	FLOAT g_AdaptiveDofSubjectMagnificationPowerFactorNear = 1.0
	FLOAT g_AdaptiveDofSubjectMagnificationPowerFactorFar = 0.866
	FLOAT g_AdaptiveDofMaxPixelDepth = 90.0
	FLOAT g_AdaptiveDofPixelDepthPowerFactor = 1.0
	FLOAT g_AdaptiveDofFNumberOfLens = 8.0
	FLOAT g_AdaptiveDofFocalLengthMultiplier = 1.0
	FLOAT g_AdaptiveDofFocusDistanceBias = 0.0
	FLOAT g_AdaptiveDofMaxNearInFocusDistance = 0.5
	FLOAT g_AdaptiveDofMaxNearInFocusDistanceBlendLevel = 1.0
	FLOAT g_AdaptiveDofMaxBlurRadiusAtNearInFocusLimit = 1.0
	FLOAT g_AdaptiveDofFocusDistanceIncreaseSpringConstant = 100.0
	FLOAT g_AdaptiveDofFocusDistanceDecreaseSpringConstant = 300.0
	BOOL g_AdaptiveDofShouldFocusOnLookAtTarget = TRUE
	BOOL g_AdaptiveDofShouldFocusOnAttachParent = FALSE
	BOOL g_AdaptiveDofShouldKeepLookAtTargetInFocus = FALSE
	BOOL g_AdaptiveDofShouldKeepAttachParentInFocus = FALSE
	BOOL g_AdaptiveDofShouldMeasurePostAlphaPixelDepth = FALSE
	
	//Misc
	PED_INDEX gFocusPedIndex = NULL
	FLOAT gFocusPedMaxDistanceFromCamera = 30.0
	INT gFocusPedScreenPositionTestBoneTag = ENUM_TO_INT(BONETAG_HEAD)
	FLOAT gFocusPedMaxScreenWidthRatioAroundCentreForTestBone = 0.8
	FLOAT gFocusPedMaxScreenHeightRatioAroundCentreForTestBone = 0.7
	FLOAT gFocusPedMinRelativeHeadingScore = 0.0
	FLOAT gFocusPedMaxScreenCentreScoreBoost = 8.0
	FLOAT gFocusPedMaxScreenRatioAroundCentreForScoreBoost = 0.333
	INT gFocusPedLosTestBoneTag1 = ENUM_TO_INT(BONETAG_HEAD)
	INT gFocusPedLosTestBoneTag2 = ENUM_TO_INT(BONETAG_SPINE3)
	BOOL gbShouldDisableCinematicBonnetCamera = FALSE

	//cinematic camera
	float pedoneOffset = 1.0
	float pedTwoOff = 1.0
	float pedThreeOff = 1.0
	float pedFourOff = 1.0
	
	bool pedoneTask = TRUE
	bool pedTwoTask = TRUE
	bool pedThreeTask = FALSE
	bool pedFourTask = FALSE
	
	int BikerIndex = 0
	
Proc CREATE_TEST_WIDGET ()
	START_WIDGET_GROUP ("Camera Test")
		SETUP_WIDGET ()
		ADD_WIDGET_BOOL ("Set Cam State",gSetCamState)
		ADD_WIDGET_BOOL ("CamState", gbCamState)
		ADD_WIDGET_INT_SLIDER ("Camera index",gTargetCam, 0, 9, 1 )
		ADD_WIDGET_BOOL ("is cam rendering ",gCamRendering)
		ADD_WIDGET_FLOAT_SLIDER ("CamPos.x" , gCamPos.x, -7000, 7000, 0.5)
		ADD_WIDGET_FLOAT_SLIDER ("CamPos.y" , gCamPos.y, -7000, 7000, 0.5)
		ADD_WIDGET_FLOAT_SLIDER ("CamPos.z" , gCamPos.z, -7000, 7000, 0.5)
		ADD_WIDGET_FLOAT_SLIDER ("CamRot.x" , gCamRot.x, -2000, 2000, 0.5)
		ADD_WIDGET_FLOAT_SLIDER ("CamRot.y" , gCamRot.y, -2000, 2000, 0.5)
		ADD_WIDGET_FLOAT_SLIDER ("CamRot.z" , gCamRot.z, -2000, 2000, 0.5)
		ADD_WIDGET_FLOAT_SLIDER ("Cam FOV" , gCamFov, 1.0, 100.0, 0.5)
		ADD_WIDGET_BOOL ("Should Cam Affect Aiming", gCamAffectsAiming)
			START_WIDGET_GROUP("Attach")
				ADD_WIDGET_BOOL ("Attach_ped ",gAttach_ped)
				ADD_WIDGET_BOOL ("Attach_veh ",gAttach_vehicle) 
				ADD_WIDGET_BOOL ("Attach_obj ",gAttach_object) 
				ADD_WIDGET_BOOL ("Dettach ",gDettach)
				ADD_WIDGET_BOOL ("Relative ",gRelative)
				ADD_WIDGET_FLOAT_SLIDER ("CamPos.x" , gAttachOffset.x, -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("CamPos.y" , gAttachOffset.y, -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("CamPos.z" , gAttachOffset.z, -2000, 2000, 0.5)
				ADD_WIDGET_INT_SLIDER ("Attach ped bone tag",gAttach_PedBoneTag, -1, 65535, 1 )
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Point")
				ADD_WIDGET_BOOL ("point ",gPoint_ped) 
				ADD_WIDGET_BOOL ("Point_veh ",gPoint_vehicle) 
				ADD_WIDGET_BOOL ("Point_obj ",gPoint_object) 
				ADD_WIDGET_BOOL ("Stop point ",gStopPoint)
				ADD_WIDGET_BOOL ("Relative point",gPointRelative)
				ADD_WIDGET_BOOL ("Set cam to new pos ",gsetCamPos) 
			
				ADD_WIDGET_INT_SLIDER ("interp time to new cam",gInterpTime, 0, 5000, 1 )
				ADD_WIDGET_FLOAT_SLIDER ("CamPoint.x" , gPointOffset.x, -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("CamPoint.y" , gPointOffset.y, -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("CamPoint.z" , gPointOffset.z, -2000, 2000, 0.5)
				ADD_WIDGET_INT_SLIDER ("Point ped bone tag",gPoint_PedBoneTag, -1, 65535, 1 )
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Shake")
				ADD_WIDGET_BOOL ("Shake globally", gShouldShakeScriptGlobal)
				ADD_WIDGET_BOOL ("Do stock", gbStockShake)
				ADD_WIDGET_FLOAT_SLIDER ("Amplitude" , gAmplitude, -2000, 2000, 0.5)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Animation")
				ADD_WIDGET_FLOAT_SLIDER ("Start phase" , gAnimationStartPhase, 0.0, 1.0, 0.1)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Fade")
				ADD_WIDGET_BOOL ("Fade in",bDoSCreenFadeIn)
				ADD_WIDGET_BOOL ("Fade out",bDoSCreenFadeOut) 
				ADD_WIDGET_INT_SLIDER ("Fade duration",gDuration, 0, 10000, 1 )
				ADD_WIDGET_BOOL ("Is screen fading out",IsScreenFadingOut)
				ADD_WIDGET_BOOL ("Is screen fading in",IsScreenFadingIn) 
				ADD_WIDGET_BOOL ("Is screen faded in",isScreenFadedin)
				ADD_WIDGET_BOOL ("Is screen faded out",isScreenFadedOut) 
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Gameplay")
				ADD_WIDGET_BOOL ("Get/display heading and pitch", gGetCamState) 
				ADD_WIDGET_BOOL ("Set heading and pitch", gSetCamState) 
				ADD_WIDGET_FLOAT_SLIDER ("Heading" , gGameplayHeading, -180.0, 180.0, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("Pitch" , gGameplayPitch, -90.0, 90.0, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("Pitch smooth rate" , gGameplayPitchSmoothRate, 0.0, 1.0, 0.1)
				ADD_WIDGET_BOOL ("Visible ",gCamVisiblePoint)
				ADD_WIDGET_FLOAT_SLIDER ("CamPos.x" , gCamPos.x, -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("CamPos.y" , gCamPos.y, -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("CamPos.z" , gCamPos.z, -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("CamRot.x" , gCamRot.x, -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("CamRot.y" , gCamRot.y, -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("CamRot.z" , gCamRot.z, -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("Cam FOV" , gCamFov, 1.0, 100.0, 0.5)
				ADD_WIDGET_BOOL ("Do stock ",gbStockShake)
				ADD_WIDGET_FLOAT_SLIDER ("Amplitude" , gAmplitude, -2000, 2000, 0.5)
				ADD_WIDGET_BOOL ("Follow nearest ped to gameplay camera", gbFollowNearestPed)
				ADD_WIDGET_BOOL ("Ignore attach parent movement for follow camera", gShouldIgnoreAttachParentMovementForFollowCamera)
				ADD_WIDGET_BOOL ("Override follow-ped camera", gShouldOverrideFollowPedCamera)
				gOverriddenFollowPedCameraTextWidgetId = ADD_TEXT_WIDGET("Overridden follow-ped camera")
				SET_CONTENTS_OF_TEXT_WIDGET(gOverriddenFollowPedCameraTextWidgetId, gOverriddenFollowPedCameraName)
				ADD_WIDGET_INT_SLIDER("Follow-ped interp duration", gOverriddenFollowPedCameraInterpDuration, 0, 10000, 100)
				ADD_WIDGET_BOOL ("Override follow-vehicle camera", gShouldOverrideFollowVehicleCamera)
				gOverriddenFollowVehicleCameraTextWidgetId = ADD_TEXT_WIDGET("Overridden follow-vehicle camera")
				SET_CONTENTS_OF_TEXT_WIDGET(gOverriddenFollowVehicleCameraTextWidgetId, gOverriddenFollowVehicleCameraName)
				ADD_WIDGET_INT_SLIDER("Follow-vehicle interp duration", gOverriddenFollowVehicleCameraInterpDuration, 0, 10000, 100)
				ADD_WIDGET_BOOL ("Override follow-vehicle camera high-angle mode", gShouldOverrideFollowVehicleCameraHighAngleMode)
				ADD_WIDGET_BOOL ("Overridden follow-vehicle camera high-angle mode", gOverriddenFollowVehicleCameraHighAngleMode)
				ADD_WIDGET_FLOAT_SLIDER ("Zoom factor" , gZoomFactor, 1.0, 20.0, 0.1)
				ADD_WIDGET_BOOL ("Apply zoom factor", gShouldApplyZoomFactor)
				ADD_WIDGET_BOOL ("Query zoom factor", gShouldQueryZoomFactor)
				ADD_WIDGET_BOOL ("Override first-person aim camera zoom limits", gShouldOverrideFirstPersonAimCameraZoomLimits)
				ADD_WIDGET_FLOAT_SLIDER ("Min zoom factor" , gMinZoomFactor, 0.0, 20.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER ("Max zoom factor" , gMaxZoomFactor, 0.0, 20.0, 0.1)
				ADD_WIDGET_BOOL ("Disable aim cameras", g_ShouldDisableAimCameras)
				ADD_WIDGET_BOOL ("Override first-person aim camera orientation limits", gShouldOverrideFirstPersonAimCameraOrientationLimits)
				ADD_WIDGET_FLOAT_SLIDER ("Min relative heading" , gMinRelativeHeading, -180.0, 180.0, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("Max relative heading" , gMaxRelativeHeading, -180.0, 180.0, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("Min relative pitch" , gMinRelativePitch, -90.0, 90.0, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("Max relative pitch" , gMaxRelativePitch, -90.0, 90.0, 0.5)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Debug camera")
				ADD_WIDGET_BOOL("Active state", gbDebugCamActiveState)
				ADD_WIDGET_BOOL("Should ignore debug pad camera toggle", gbShouldIgnoreDebugPadCameraToggle)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Spline camera")
				ADD_WIDGET_BOOL("set spline", gbSetSplinePhase)
				ADD_WIDGET_FLOAT_SLIDER("New Spline Phase", gfSplinePhase, 0.0, 1.0, 0.1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("vehicle view mode")
				ADD_WIDGET_BOOL ("Set view mode", gbStockShake)
				ADD_WIDGET_INT_SLIDER ("context", gViewModeContext, 0, ENUM_TO_INT(NUM_CAM_VIEW_MODE_CONTEXTS) - 1, 1 ) 
				ADD_WIDGET_INT_SLIDER ("view mode", gZoomLevel, 0, ENUM_TO_INT(NUM_CAM_VIEW_MODES) - 1, 1 ) 
				ADD_WIDGET_BOOL ("Disable bonnet camera", gbShouldDisableCinematicBonnetCamera)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("HINT")
				ADD_WIDGET_BOOL ("Hint ped ",gPoint_ped)
				ADD_WIDGET_BOOL ("Hint vehicle ",gPoint_vehicle) 
				ADD_WIDGET_BOOL ("hint object ",gPoint_object)  
				ADD_WIDGET_BOOL ("hint coord ", gpointcoord)
				ADD_WIDGET_BOOL ("Stop hint (release)",gStopPoint)
				ADD_WIDGET_BOOL ("Stop hint immediately",gStopPointNow)
				ADD_WIDGET_BOOL ("hint relative",gPointRelative)
				ADD_WIDGET_BOOL ("Stop hint being cancelled", gbStockShake)
				ADD_WIDGET_FLOAT_SLIDER ("CamPoint.x" , gPointOffset.x, -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("CamPoint.y" , gPointOffset.y, -2000, 2000, 0.5)
				ADD_WIDGET_FLOAT_SLIDER ("CamPoint.z" , gPointOffset.z, -2000, 2000, 0.5)
			STOP_WIDGET_GROUP()

			START_WIDGET_GROUP("THIRD PERSON LIMITS")
				ADD_WIDGET_BOOL ("Enable heading limits ",	g_b3rdHeadingLimit)
				ADD_WIDGET_FLOAT_SLIDER ("Min Heading Limit" , g_f3rdMinHeading, -360, 360, 1.0)
				ADD_WIDGET_FLOAT_SLIDER ("Max Heading Limit" , g_f3rdMaxHeading, -360, 360, 1.0)
				ADD_WIDGET_BOOL ("Enable pitch limits ",	g_b3rdPitchLimit)
				ADD_WIDGET_FLOAT_SLIDER ("Min Pitch Limit" , g_f3rdMinPitch, -360, 360, 1.0)
				ADD_WIDGET_FLOAT_SLIDER ("Max Pitch Limit" , g_f3rdMaxPitch, -360, 360, 1.0)
				ADD_WIDGET_BOOL ("Enable distance limits ",	g_b3rdDistanceLimit)
				ADD_WIDGET_FLOAT_SLIDER ("Min Distance Limit" , g_f3rdMinDistance, 0.1, 299, 1.0)
				ADD_WIDGET_FLOAT_SLIDER ("Max Distance Limit" , g_f3rdMaxDistance, 0.2, 300, 1.0)
			STOP_WIDGET_GROUP()

			START_WIDGET_GROUP("DOF")
				ADD_WIDGET_FLOAT_SLIDER ("Near DOF", NearDof, 0.0, 100.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER ("Far DOF" , FarDof, 0.0, 100.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER ("DOF Strength" , DofStrength, 0.0, 1.0, 0.01)
				ADD_WIDGET_BOOL("Use Shallow DOF", gShouldUseShallowDof)
				ADD_WIDGET_BOOL("Set all DOF planes", gShouldSetAllDofPlanes)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Adaptive DOF")
				ADD_WIDGET_FLOAT_SLIDER ("OverriddenFocusDistance", g_AdaptiveDofOverriddenFocusDistance, 0.0, 99999.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER ("OverriddenFocusDistanceBlendLevel", g_AdaptiveDofOverriddenFocusDistanceBlendLevel, 0.0, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER ("FocusDistanceGridScalingX", g_AdaptiveDofFocusDistanceGridScalingX, 0.0, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER ("FocusDistanceGridScalingY" , g_AdaptiveDofFocusDistanceGridScalingY, 0.0, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER ("SubjectMagnificationPowerFactorNear", g_AdaptiveDofSubjectMagnificationPowerFactorNear, 0.0, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER ("SubjectMagnificationPowerFactorFar" , g_AdaptiveDofSubjectMagnificationPowerFactorFar, 0.0, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER ("MaxPixelDepth" , g_AdaptiveDofMaxPixelDepth, 0.0, 99999.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER ("PixelDepthPowerFactor" , g_AdaptiveDofPixelDepthPowerFactor, 0.0, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER ("FNumberOfLens" , g_AdaptiveDofFNumberOfLens, 0.5, 256.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER ("FocalLengthMultiplier" , g_AdaptiveDofFocalLengthMultiplier, 0.1, 10.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER ("FocusDistanceBias" , g_AdaptiveDofFocusDistanceBias, -100.0, 100.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER ("MaxNearInFocusDistance" , g_AdaptiveDofMaxNearInFocusDistance, 0.0, 99999.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER ("MaxNearInFocusDistanceBlendLevel", g_AdaptiveDofMaxNearInFocusDistanceBlendLevel, 0.0, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER ("MaxBlurRadiusAtNearInFocusLimit", g_AdaptiveDofMaxBlurRadiusAtNearInFocusLimit, 0.0, 30.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER ("FocusDistanceIncreaseSpringConstant", g_AdaptiveDofFocusDistanceIncreaseSpringConstant, 0.0, 1000.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER ("FocusDistanceDecreaseSpringConstant", g_AdaptiveDofFocusDistanceDecreaseSpringConstant, 0.0, 1000.0, 0.01)
				ADD_WIDGET_BOOL("ShouldFocusOnLookAtTarget", g_AdaptiveDofShouldFocusOnLookAtTarget)
				ADD_WIDGET_BOOL("ShouldFocusOnAttachParent", g_AdaptiveDofShouldFocusOnAttachParent)
				ADD_WIDGET_BOOL("ShouldKeepLookAtTargetInFocus", g_AdaptiveDofShouldKeepLookAtTargetInFocus)
				ADD_WIDGET_BOOL("ShouldKeepAttachParentInFocus", g_AdaptiveDofShouldKeepAttachParentInFocus)
				ADD_WIDGET_BOOL("ShouldMeasurePostAlphaPixelDepth", g_AdaptiveDofShouldMeasurePostAlphaPixelDepth)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Focus ped")
				ADD_WIDGET_FLOAT_SLIDER ("MaxDistanceFromCamera", gFocusPedMaxDistanceFromCamera, 0.0, 100.0, 1.0)
				ADD_WIDGET_INT_SLIDER ("ScreenPositionTestBoneTag", gFocusPedScreenPositionTestBoneTag, -1, 64729, 1)
				ADD_WIDGET_FLOAT_SLIDER ("MaxScreenWidthRatioAroundCentreForHead", gFocusPedMaxScreenWidthRatioAroundCentreForTestBone, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER ("MaxScreenHeightRatioAroundCentreForHead", gFocusPedMaxScreenHeightRatioAroundCentreForTestBone, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER ("MinRelativeHeadingScore" , gFocusPedMinRelativeHeadingScore, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER ("MaxScreenCentreScoreBoost" , gFocusPedMaxScreenCentreScoreBoost, 0.0, 100.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER ("MaxScreenRatioAroundCentreForScoreBoost" , gFocusPedMaxScreenRatioAroundCentreForScoreBoost, 0.0, 1.0, 0.01)
				ADD_WIDGET_INT_SLIDER ("LosTestBoneTag1", gFocusPedLosTestBoneTag1, -1, 64729, 1)
				ADD_WIDGET_INT_SLIDER ("LosTestBoneTag2", gFocusPedLosTestBoneTag2, -1, 64729, 1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Biker peds")
				ADD_WIDGET_FLOAT_SLIDER ("Ped 1 offset" , pedoneOffset, 1.0, 10.0, 1.0)
				ADD_WIDGET_BOOL("Give Ped 1 Task", pedoneTask)
				ADD_WIDGET_FLOAT_SLIDER ("Ped 2 offset" , pedTwoOff, 1.0, 10.0, 1.0)
				ADD_WIDGET_BOOL("Give Ped 2 Task", pedTwoTask)
				ADD_WIDGET_FLOAT_SLIDER ("Ped 3 offset" , pedThreeOff, 1.0, 10.0, 1.0)
				ADD_WIDGET_BOOL("Give Ped 3 Task", pedThreeTask)
				ADD_WIDGET_FLOAT_SLIDER ("Ped 4 offset" , pedFourOff, 1.0, 10.0, 1.0)
				ADD_WIDGET_BOOL("Give Ped 4 Task", pedFourTask)
			STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP ()
ENDPROC

PROC Cleanup_Scenario_Entities ()
	int index = 0
		for index = 0 to MAX_NUMBER_OF_PEDS -1 	
			CLEAN_UP_PED_ENTITIES (TestPeds[index].ped)
		ENDFOR 
		
		for index = 0 to MAX_NUMBER_OF_VEHICLES -1
			CLEAN_UP_VEHICLE_ENTITIES (TestVehicles[index].Vehicle )
		ENDFOR

		RENDER_SCRIPT_CAMS (FALSE, FALSE)

		for index = 0 to MAX_NUMBER_OF_CAMS-1
			if DOES_CAM_EXIST (NewTestCams[index])
				DESTROY_CAM (NewTestCams[index])
				DESTROY_CAM(TestCams[index].cam)
			ENDIF
		ENDFOR

		Set_Gang_Relationships (FALSE)	
ENDPROC


PROC GET_CAM_PARAMS (CAMERA_INDEX cam)
		gCamPos = GET_CAM_COORD(cam )
		gCamRot = GET_CAM_ROT  (cam)
		gCamFov = GET_CAM_FOV  (cam)

ENDPROC


ENUM  ScenarioActivateDeactivateCamera_Flags
	ScenarioActivateDeactivateCamera_Init,
	ScenarioActivateDeactivateCamera_SwitchBetweenCams
ENDENUM
ScenarioActivateDeactivateCamera_Flags ScenarioActivateDeactivateCamera_Status = ScenarioActivateDeactivateCamera_Init

ENUM ScenarioCreateFixedCam_Flags
	ScenarioCreateFixedCam_Init
ENDENUM

ScenarioCreateFixedCam_Flags ScenarioCreateFixedCam_Status = ScenarioCreateFixedCam_Init

ENUM  ScenarioAttachCameras_Flags
	ScenarioAttachCameras_Init, 
	ScenarioAttachCameras_Run
ENDENUM

ScenarioAttachCameras_Flags ScenarioAttachCameras_Status = ScenarioAttachCameras_Init

ENUM  ScenarioPointCameras_Flags
	ScenarioPointCamera_Init, 
	ScenarioPointCamera_Run
ENDENUM

ScenarioPointCameras_Flags ScenarioPointCamera_Status = ScenarioPointCamera_Init

ENUM ScenarioShakeCam_Flags
	ScenarioShakeCam_Init,
	ScenarioShakeCam_Run
ENDENUM
	ScenarioShakeCam_Flags ScenarioShakeCam_Status = ScenarioShakeCam_Init

	ENUM ScenarioSimpleInterp_Flags
		ScenarioSimpleInterp_Init,
		ScenarioCreateFixedCam_Run
	ENDENUM
					
	ScenarioSimpleInterp_Flags ScenarioSimpleInterp_Status = ScenarioSimpleInterp_Init

ENUM ScenarioCinematicDriveby_Flags
	ScenarioCinematicDriveby_Init,
	ScenarioCinematicDriveby_Run
ENDENUM
					
ScenarioCinematicDriveby_Flags ScenarioCinematicDriveby_Status = ScenarioCinematicDriveby_Init

ENUM ScenarioSceneTransition_Flags
	ScenarioSceneTransition_Init, 
	ScenarioSceneTransition_WaitToFinish
ENDENUM

ScenarioSceneTransition_Flags ScenarioSceneTransition_Status = ScenarioSceneTransition_Init

ENUM ScenarioCameraFocusPed_Flags
	ScenarioCameraFocusPed_Init, 
	ScenarioCameraFocusPed_WaitToFinish
ENDENUM

ScenarioCameraFocusPed_Flags ScenarioCameraFocusPed_Status = ScenarioCameraFocusPed_Init

ENUM ScenarioSplineCams_Flags
	ScenarioSplineCams_Init, 
	ScenarioSplineCams_Run

ENDENUM

ScenarioSplineCams_Flags ScenarioSplineCams_Status = ScenarioSplineCams_Init

ENUM ScenarioDofTest_Flags
	ScenarioDofTest_Init,
	ScenarioDofTest_Run
ENDENUM

ScenarioDofTest_Flags ScenarioDofTest_Status = ScenarioDofTest_Init

ENUM ScenarioAdaptiveDofTest_Flags
	ScenarioAdaptiveDofTest_Init,
	ScenarioAdaptiveDofTest_Run
ENDENUM

ScenarioAdaptiveDofTest_Flags ScenarioAdaptiveDofTest_Status = ScenarioAdaptiveDofTest_Init

ENUM ScenarioInterpToAndFromGameCam_Flags
	ScenarioInterpToAndFromGameCam_Init,
	ScenarioInterpToAndFromGameCam_InterpToScript,
	ScenarioInterpToAndFromGameCam_InterpFromScript
ENDENUM

ScenarioInterpToAndFromGameCam_Flags ScenarioInterpToAndFromGameCam_Status = ScenarioInterpToAndFromGameCam_Init

ENUM ScenarioInterpToAndFromGameCamCatchUp_Flags
	ScenarioInterpToAndFromGameCamCatchUp_Init,
	ScenarioInterpToAndFromGameCamCatchUp_InterpToScript
ENDENUM
ScenarioInterpToAndFromGameCamCatchUp_Flags ScenarioInterpToAndFromGameCamCatchUp_Status = ScenarioInterpToAndFromGameCamCatchUp_Init

ENUM ScenarioThirdPersonScriptLimits_Flags
	ScenarioThirdPersonScriptLimits_Run
ENDENUM
ScenarioThirdPersonScriptLimits_Flags ScenarioThirdPersonScriptLimits_Status = ScenarioThirdPersonScriptLimits_Run

ENUM ScenarioScriptedFlyCamera_Flags
	ScenarioScriptedFlyCamera_Init, 
	ScenarioScriptedFlyCamera_WaitToFinish
ENDENUM

ScenarioScriptedFlyCamera_Flags ScenarioScriptedFlyCamera_Status = ScenarioScriptedFlyCamera_Init


PROC INITALISE_TEST_STATE()
	SingleTaskStatus = startSingleTask
	ScenarioActivateDeactivateCamera_Status = ScenarioActivateDeactivateCamera_Init
	ScenarioCreateFixedCam_Status = ScenarioCreateFixedCam_Init
	ScenarioAttachCameras_Status = ScenarioAttachCameras_Init
	ScenarioPointCamera_Status = ScenarioPointCamera_Init
	ScenarioShakeCam_Status = ScenarioShakeCam_Init
	ScenarioSimpleInterp_Status = ScenarioSimpleInterp_Init
	ScenarioCinematicDriveby_Status = ScenarioCinematicDriveby_Init	
	ScenarioSceneTransition_Status = ScenarioSceneTransition_Init
	ScenarioCameraFocusPed_Status = ScenarioCameraFocusPed_Init
	ScenarioSplineCams_Status = ScenarioSplineCams_Init
	ScenarioDofTest_Status = ScenarioDofTest_Init
	ScenarioAdaptiveDofTest_Status = ScenarioAdaptiveDofTest_Init
	ScenarioInterpToAndFromGameCam_Status = ScenarioInterpToAndFromGameCam_Init
	ScenarioScriptedFlyCamera_Status = ScenarioScriptedFlyCamera_Init
	ScenarioInterpToAndFromGameCamCatchUp_Status = ScenarioInterpToAndFromGameCamCatchUp_Init
					
	gbHasStartedAnimatedCamera = FALSE
	gbHasFinishedAnimatedCamera = FALSE
ENDPROC	
	
ENUM Scenarios
	ScenarioActivateDeactivateCamera,
	ScenarioCreateFixedCam, 
	ScenarioAttachCam,
	ScenarioPointCam,
	ScenarioSplineCams,
	ScenarioInterpCams,
	ScenarioShakeCam,
	ScenarioAnimateCam,
	ScenarioFadeCam,
	ScenarioGameplayCamera,
	ScenarioDebugCamera,
	ScenarioPedZoom,
	ScenarioVehicleZoom,
	ScenarioHintCam,
	ScenarioSimpleInterp,
	ScenarioCinematicDriveby = 15,
	ScenarioSceneTransition = 16,
	ScenarioDofTest = 17,
	ScenarioInterpToAndFromGameCam = 18,
	TimedSplineTest = 19,
	RoundedSplineTest = 20,
	SmoothedSplineTest = 21, 
	SplineLookAtSpline = 22,
	ScenarioCameraFocusPed = 23,
	ScenarioScriptedFlyCamera = 24,
	ScenarioVehicleFormationCamera = 25,
	ScenarioHeliFallout = 26,
	ScenarioInterpToAndFromGameCamCatchUp = 27,
	ScenarioThirdPersonScriptLimits = 28,
	ScenarioAdaptiveDofTest = 29,
	DefaultTest = 1000
ENDENUM

Proc SETUP_TEST_DATA ()
		SWITCH int_to_enum (scenarios, gcurrentselection)
			CASE ScenarioActivateDeactivateCamera
			CASE ScenarioCreateFixedCam
				TestPeds[0].PedsCoords = << -2.4167, -1.4273, 1.1959>> + gvMapOffset
				TestPeds[0].PedHeading = 359.6439
				TestPeds[0].Pedrelgrp = 1
				TestPeds[0].PedModel = S_M_Y_Cop_01
				TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
				TestPeds[0].PedcombatMove = 2
				TestPeds[0].pedcombatrange = 2
				TestPeds[0].combatcover = CA_USE_COVER
				TestPeds[0].bcombatcover = FALSE
			BREAK
			
			CASE  ScenarioAttachCam
			CASE ScenarioPointCam
			CASE ScenarioHintCam
			
				TestPeds[0].PedsCoords = << -2.4167, -1.4273, 1.1959>> + gvMapOffset
				TestPeds[0].PedHeading = 359.6439
				TestPeds[0].Pedrelgrp = 1
				TestPeds[0].PedModel = S_M_Y_Cop_01
				TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
				TestPeds[0].PedcombatMove = 2
				TestPeds[0].pedcombatrange = 2
				TestPeds[0].combatcover = CA_USE_COVER
				TestPeds[0].bcombatcover = FALSE
			
				TestVehicles [0 ].vehicleCoords = << -0.0012, 10.0009, 1.9393>> + gvMapOffset
				TestVehicles [0 ].vehicleHeading = 90.0001
				TestVehicles [0 ].vehiclemodel = FBI
		
			BREAK
			
			
			
		
			CASE ScenarioGameplayCamera
			CASE ScenarioDebugCamera
			CASE ScenarioVehicleZoom
				TestVehicles [0 ].vehicleCoords = << -0.0012, 10.0009, 1.9393>> + gvMapOffset
				TestVehicles [0 ].vehicleHeading = 90.0001
				TestVehicles [0 ].vehiclemodel = FBI
			
			BREAK
			
			CASE ScenarioCinematicDriveby
				TestVehicles [0 ].vehicleCoords = << -0.0012, 10.0009, 1.9393>> + gvMapOffset
				TestVehicles [0 ].vehicleHeading = 90.0001
				TestVehicles [0 ].vehiclemodel = FBI
			BREAK
			
			CASE ScenarioSceneTransition
			
			BREAK
			
			CASE ScenarioCameraFocusPed

			BREAK
			
			CASE ScenarioScriptedFlyCamera
			
			BREAK
			
			CASE ScenarioDofTest
				TestPeds[0].PedsCoords = << -2.58706617, -1.31549120, 7.35537958>>
				TestPeds[0].PedHeading = 180.6439
				TestPeds[0].Pedrelgrp = 1
				TestPeds[0].PedModel = S_M_Y_Cop_01
				TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
				TestPeds[0].PedcombatMove = 2
				TestPeds[0].pedcombatrange = 2
				TestPeds[0].combatcover = CA_USE_COVER
				TestPeds[0].bcombatcover = FALSE
				TestPeds[1].PedsCoords = << -2.24279618, -2.53976870, 7.35537958>>
				TestPeds[1].PedHeading = 359.6439
				TestPeds[1].Pedrelgrp = 1
				TestPeds[1].PedModel = S_F_Y_Cop_01
				TestPeds[1].PedsWeapon = WEAPONTYPE_PISTOL
				TestPeds[1].PedcombatMove = 2
				TestPeds[1].pedcombatrange = 2
				TestPeds[1].combatcover = CA_USE_COVER
				TestPeds[1].bcombatcover = FALSE
			BREAK
			
			CASE ScenarioAdaptiveDofTest
				TestPeds[0].PedsCoords = << -2.58706617, -1.31549120, 7.35537958>>
				TestPeds[0].PedHeading = 180.6439
				TestPeds[0].Pedrelgrp = 1
				TestPeds[0].PedModel = S_M_Y_Cop_01
				TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
				TestPeds[0].PedcombatMove = 2
				TestPeds[0].pedcombatrange = 2
				TestPeds[0].combatcover = CA_USE_COVER
				TestPeds[0].bcombatcover = FALSE
				TestPeds[1].PedsCoords = << -2.24279618, -2.53976870, 7.35537958>>
				TestPeds[1].PedHeading = 359.6439
				TestPeds[1].Pedrelgrp = 1
				TestPeds[1].PedModel = S_F_Y_Cop_01
				TestPeds[1].PedsWeapon = WEAPONTYPE_PISTOL
				TestPeds[1].PedcombatMove = 2
				TestPeds[1].pedcombatrange = 2
				TestPeds[1].combatcover = CA_USE_COVER
				TestPeds[1].bcombatcover = FALSE
			BREAK
			
			case ScenarioHeliFallout
				TestPeds[0].PedsCoords = << 6.5, 96, 8>>
				TestPeds[0].PedHeading = 180.6439
				TestPeds[0].Pedrelgrp = 1
				TestPeds[0].PedModel = S_M_M_BOUNCER_01
				
				
				TestVehicles [0 ].vehicleCoords = << 0, 95, 9>>
				TestVehicles [0 ].vehicleHeading = 90.0001
				TestVehicles [0 ].vehiclemodel = FROGGER
			break
			
			
			case ScenarioVehicleFormationCamera
				TestPeds[0].PedsCoords = << 357.74, -1064.44, 28.38>>
				TestPeds[0].PedHeading = 180.6439
				TestPeds[0].Pedrelgrp = 1
				TestPeds[0].PedModel = S_M_M_BOUNCER_01
				
				
				TestPeds[1].PedsCoords = << 352.70, -1064.45, 28.42>>
				TestPeds[1].PedHeading = 180.6439
				TestPeds[1].Pedrelgrp = 1
				TestPeds[1].PedModel = S_M_M_BOUNCER_01
				
				TestPeds[2].PedsCoords = << 352.72, -1065.59, 28.43>>
				TestPeds[2].PedHeading = 180.6439
				TestPeds[2].Pedrelgrp = 1
				TestPeds[2].PedModel = S_M_M_BOUNCER_01
				
				TestPeds[3].PedsCoords = << 351.11, -1065.56, 28.44>>
				TestPeds[3].PedHeading = 180.6439
				TestPeds[3].Pedrelgrp = 1
				TestPeds[3].PedModel = S_M_M_BOUNCER_01
				
				
				TestVehicles [0 ].vehicleCoords = << 349.17, -1063.38, 28.41>>
				TestVehicles [0 ].vehicleHeading = 90.0001
				TestVehicles [0 ].vehiclemodel = DAEMON
				
				TestVehicles [1].vehicleCoords = << 349.09, -1065.31, 28.42>>
				TestVehicles [1].vehicleHeading = 90.0001
				TestVehicles [1].vehiclemodel = DAEMON
				
					TestVehicles [2].vehicleCoords = << 346.91, -1063.46, 28.39>>
				TestVehicles [2].vehicleHeading = 90.0001
				TestVehicles [2].vehiclemodel = DAEMON
				
					TestVehicles [3].vehicleCoords = << 346.82, -1065.57, 28.41>>
				TestVehicles [3].vehicleHeading = 90.0001
				TestVehicles [3].vehiclemodel = DAEMON
				
					TestVehicles [4].vehicleCoords = <<346.83, -1067.12, 28.57>>
				TestVehicles [4].vehicleHeading = 90.0001
				TestVehicles [4].vehiclemodel = DAEMON
			BREAK
			
			
		ENDSWITCH
ENDPROC
VECTOR vStart

PROC RUN_TEST ()
			SWITCH int_to_enum (scenarios, gcurrentselection)		// the ENUM corresponds to the values from the XML file 
					CASE ScenarioActivateDeactivateCamera
						SWITCH ScenarioActivateDeactivateCamera_Status
							
							CASE  ScenarioActivateDeactivateCamera_Init
								NewTestCams[0] = CREATE_CAM_WITH_PARAMS ("DEFAULT_SCRIPTED_CAMERA", << 1.20, -5.93, 5.26>>+ gvMapOffset, <<-19, 0, 0>> )
								NewTestCams[1] = CREATE_CAM_WITH_PARAMS ("DEFAULT_SCRIPTED_CAMERA", << -7.20, 6.93, 6.9>>+ gvMapOffset, <<-28, 0, -116.0>> )
								NewTestCams[2] = CREATE_CAM_WITH_PARAMS ("DEFAULT_SCRIPTED_CAMERA",  << 9.20, 9.93, 6.15>>+ gvMapOffset, <<-28, 0, 129.0>> )
							
					
								PRINTSTRING ("ScenarioActivateDeactivateCamera")
							
								SET_CAM_ACTIVE (NewTestCams[0], TRUE )
								
								RENDER_SCRIPT_CAMS (TRUE, FALSE)
								
								wait (0)
								
								PRINTSTRING ("ScenarioActivateDeactivateCamera")
								
								IF DOES_CAM_EXIST (NewTestCams[5])
									SET_CAM_ACTIVE (NewTestCams[5], TRUE )
								ENDIF
								ScenarioActivateDeactivateCamera_Status = ScenarioActivateDeactivateCamera_SwitchBetweenCams
							BREAK
							
							CASE  ScenarioActivateDeactivateCamera_SwitchBetweenCams



								if (gSetCamState)	
									IF DOES_CAM_EXIST (NewTestCams[gTargetCam])				
										SET_CAM_ACTIVE (NewTestCams[gTargetCam], gbCamState)
									ENDIF
									gSetCamState = FALSE
								ENDIF
								
								IF DOES_CAM_EXIST (NewTestCams[gTargetCam])	
									gCamRendering =	IS_CAM_RENDERING (NewTestCams[gTargetCam])
							
								ENDIF
							BREAK
					
						ENDSWITCH
					BREAK
					
					CASE ScenarioCreateFixedCam
						SWITCH ScenarioCreateFixedCam_Status
							CASE  ScenarioCreateFixedCam_Init
								NewTestCams[0] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
								SET_CAM_COORD (NewTestCams[0] ,  << 1.20, -5.93, 5.26>>+ gvMapOffset  )
								SET_CAM_ROT (NewTestCams[0] ,  <<-19, 0, 0>>  )
								SET_CAM_ACTIVE (NewTestCams[0], TRUE )
								SET_CAM_AFFECTS_AIMING(NewTestCams[0], gCamAffectsAiming)
								RENDER_SCRIPT_CAMS (TRUE, FALSE)	
								
								IF DOES_CAM_EXIST (NewTestCams[5])
									SET_CAM_ACTIVE (NewTestCams[5], TRUE )
									SET_CAM_AFFECTS_AIMING(NewTestCams[5], gCamAffectsAiming)
								ENDIF
								
								Set_Test_State_To_Default()
							BREAK
						ENDSWITCH
					BREAK

					CASE ScenarioDofTest
						SWITCH ScenarioDofTest_Status
							CASE  ScenarioDofTest_Init
								NewTestCams[0] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
								SET_CAM_PARAMS(NewTestCams[0],<<8.504449,-13.629935,8.343534>>,<<-1.497766,-0.000000,42.899403>>,2.599557)
								SET_CAM_ACTIVE (NewTestCams[0], TRUE )
								SET_CAM_USE_SHALLOW_DOF_MODE(NewTestCams[0], gShouldUseShallowDof)
								IF(gShouldSetAllDofPlanes)
									SET_CAM_DOF_PLANES(NewTestCams[0], 15.0, 16.0, 17.0, 18.0)
								ELSE
									SET_CAM_NEAR_DOF( NewTestCams[0], 16.0 )
									SET_CAM_FAR_DOF( NewTestCams[0], 17.0 )
									SET_CAM_DOF_STRENGTH( NewTestCams[0], DofStrength )
								ENDIF
								
								NewTestCams[1] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
								SET_CAM_PARAMS(NewTestCams[1],<<8.504449,-13.629935,8.343534>>,<<-1.497766,-0.000000,42.899403>>,2.599557)
								SET_CAM_ACTIVE (NewTestCams[1], TRUE )
								SET_CAM_USE_SHALLOW_DOF_MODE(NewTestCams[1], gShouldUseShallowDof)
								IF(gShouldSetAllDofPlanes)
									SET_CAM_DOF_PLANES(NewTestCams[1], 14.0, 15.0, 15.75, 16.75)
								ELSE
									SET_CAM_NEAR_DOF( NewTestCams[1], 15.0 )
									SET_CAM_FAR_DOF( NewTestCams[1], 15.75 )
									SET_CAM_DOF_STRENGTH( NewTestCams[1], DofStrength )
								ENDIF
								
								IF DOES_CAM_EXIST (NewTestCams[0])
									SET_CAM_ACTIVE (NewTestCams[0], TRUE )
								ENDIF
								
								IF DOES_CAM_EXIST (NewTestCams[1])
									SET_CAM_ACTIVE_WITH_INTERP(NewTestCams[1], NewTestCams[0], 4000 )
								ENDIF
								
								RENDER_SCRIPT_CAMS (TRUE, FALSE)	
								
								ScenarioDofTest_Status = ScenarioDofTest_Run
							
							BREAK
							
							CASE ScenarioDofTest_Run
								IF(gShouldSetAllDofPlanes)
									SET_CAM_DOF_PLANES(NewTestCams[1], NearDof - 1.0, NearDof, FarDof, FarDof + 1.0)
								ELSE
									SET_CAM_NEAR_DOF( NewTestCams[1], NearDof )
									SET_CAM_FAR_DOF( NewTestCams[1], FarDof )
									SET_CAM_DOF_STRENGTH(NewTestCams[1], DofStrength)
								ENDIF
								
								SET_CAM_USE_SHALLOW_DOF_MODE(NewTestCams[1], gShouldUseShallowDof)
								SET_USE_HI_DOF()
							
							BREAK
							
						ENDSWITCH
					BREAK
					
					case ScenarioInterpToAndFromGameCam
						SWITCH ScenarioInterpToAndFromGameCam_Status
							CASE ScenarioInterpToAndFromGameCam_Init
								NewTestCams[0] = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << 1.20, -5.93, 5.26>>+ gvMapOffset, <<-19, 0, 0>>, 60.0, TRUE )
								RENDER_SCRIPT_CAMS (TRUE, TRUE)	
								ScenarioInterpToAndFromGameCam_Status = ScenarioInterpToAndFromGameCam_InterpToScript
							BREAK
						
							CASE ScenarioInterpToAndFromGameCam_InterpToScript
								if IS_INTERPOLATING_TO_SCRIPT_CAMS()
									PRINTSTRING("IS_INTERPOLATING_TO_SCRIPT_CAM ")
									PRINTNL()
								ELSE
									RENDER_SCRIPT_CAMS (FALSE, TRUE)
									ScenarioInterpToAndFromGameCam_Status = ScenarioInterpToAndFromGameCam_InterpFromScript
								ENDIF
									
							BREAK			
							
							CASE ScenarioInterpToAndFromGameCam_InterpFromScript
								if IS_INTERPOLATING_FROM_SCRIPT_CAMS()
									PRINTSTRING("IS_INTERPOLATING_FROM_SCRIPT_CAM ")
									PRINTNL()
								ELSE
									Set_Test_State_To_Default()
								ENDIF
							
							BREAK
							
						ENDSWITCH
					
					BREAK

					case ScenarioInterpToAndFromGameCamCatchUp
						SWITCH ScenarioInterpToAndFromGameCamCatchUp_Status
							CASE ScenarioInterpToAndFromGameCamCatchUp_Init
								NewTestCams[0] = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << 1.20, -5.93, 5.26>>+ gvMapOffset, <<-19, 0, 0>>, 60.0, TRUE )
								RENDER_SCRIPT_CAMS (TRUE, TRUE)	
								ScenarioInterpToAndFromGameCamCatchUp_Status = ScenarioInterpToAndFromGameCamCatchUp_InterpToScript
							BREAK
						
							CASE ScenarioInterpToAndFromGameCamCatchUp_InterpToScript
								IF IS_INTERPOLATING_TO_SCRIPT_CAMS()
									PRINTSTRING("IS_INTERPOLATING_TO_SCRIPT_CAM ")
									PRINTNL()
								ELSE
									STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(FALSE)
									Set_Test_State_To_Default()
								ENDIF
							BREAK			
							
						ENDSWITCH
					
					BREAK

					case ScenarioThirdPersonScriptLimits
						SWITCH ScenarioThirdPersonScriptLimits_Status
							CASE ScenarioThirdPersonScriptLimits_Run
								IF (g_b3rdHeadingLimit)
									SET_THIRD_PERSON_CAM_RELATIVE_HEADING_LIMITS_THIS_UPDATE(g_f3rdMinHeading, g_f3rdMaxHeading)
								ENDIF
								IF (g_b3rdPitchLimit)
									SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(g_f3rdMinPitch, g_f3rdMaxPitch)
								ENDIF
								IF (g_b3rdDistanceLimit)
									SET_THIRD_PERSON_CAM_ORBIT_DISTANCE_LIMITS_THIS_UPDATE(g_f3rdMinDistance, g_f3rdMaxDistance)
								ENDIF
							BREAK

						ENDSWITCH
					
					BREAK
					
					CASE ScenarioSimpleInterp
						SWITCH ScenarioSimpleInterp_Status
							CASE  ScenarioSimpleInterp_Init
								NewTestCams[0] = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << 1.20, -5.93, 5.26>>+ gvMapOffset, <<-19, 0, 0>>, 60.0, TRUE )
								RENDER_SCRIPT_CAMS (TRUE, FALSE)	
								SET_CAM_PARAMS (NewTestCams[0], << -7.20, 6.93, 6.9>>, <<-28, 0, -116.0>>, 70.0, 5000 )
								ScenarioSimpleInterp_Status = ScenarioCreateFixedCam_Run
							BREAK
							
							CASE  ScenarioCreateFixedCam_Run
								
								IF NOT IS_CAM_INTERPOLATING (NewTestCams[0])
									RENDER_SCRIPT_CAMS (FALSE, TRUE)
									DESTROY_CAM (NewTestCams[0] )
										Set_Test_State_To_Default()
								ENDIF	
							BREAK
						ENDSWITCH
					BREAK

					CASE ScenarioAttachCam
						SWITCH ScenarioAttachCameras_Status
							
							CASE  ScenarioAttachCameras_Init
								NewTestCams[0] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
								SET_CAM_COORD (NewTestCams[0] ,  << 1.20, -5.93, 5.26>>+ gvMapOffset  )
								SET_CAM_ROT (NewTestCams[0] ,  <<-19, 0, 0>>  )
							
								SET_CAM_ACTIVE (NewTestCams[0], TRUE )
								
								RENDER_SCRIPT_CAMS (TRUE, FALSE)

								ScenarioAttachCameras_Status = ScenarioAttachCameras_Run
							BREAK
							
							CASE  ScenarioAttachCameras_Run

								if (gAttach_ped)	
									IF  (gAttach_vehicle = FALSE) and (gAttach_object = FALSE)
										IF DOES_CAM_EXIST (NewTestCams[gTargetCam])				
											ATTACH_CAM_TO_PED_BONE(NewTestCams[gTargetCam], TestPeds[0].ped, INT_TO_ENUM(PED_BONETAG, gAttach_PedBoneTag), gAttachOffset, gRelative   )
										ENDIF
									ENDIF
								ENDIF
								
								if (gAttach_vehicle)	
									IF  (gAttach_object = FALSE) and (gAttach_ped = FALSE)
										IF DOES_CAM_EXIST (NewTestCams[gTargetCam])				
											IF  IS_VEHICLE_DRIVEABLE (TestVehicles[0].Vehicle )
												ATTACH_CAM_TO_ENTITY(NewTestCams[gTargetCam], TestVehicles[0].vehicle, gAttachOffset, gRelative   )
											ENDIF
										ENDIF
									ENDIF
								ENDIF

								if (gDettach)
									IF DOES_CAM_EXIST (NewTestCams[gTargetCam])		
										DETACH_CAM (NewTestCams[gTargetCam])	
									ENDIF
									gAttach_ped = FALSE
									gAttach_vehicle = FALSE
									
									gDettach = FALSE
									
								ENDIF
								
							BREAK
					
						ENDSWITCH
					BREAK
				
					CASE ScenarioPointCam
						SWITCH ScenarioPointCamera_Status
							
							CASE  ScenarioPointCamera_Init
								NewTestCams[0] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
								SET_CAM_COORD (NewTestCams[0] ,  << 1.20, -5.93, 5.26>> + gvMapOffset )
								SET_CAM_ROT (NewTestCams[0] ,  <<-19, 0, 0>>  )
							
								SET_CAM_ACTIVE (NewTestCams[0], TRUE )
								
								RENDER_SCRIPT_CAMS (TRUE, FALSE)
								
								
								ScenarioPointCamera_Status = ScenarioPointCamera_Run
							BREAK
							
							CASE ScenarioPointCamera_Run

							if (gsetCamPos)
								IF DOES_CAM_EXIST (NewTestCams[gTargetCam])		
									test_Cam_pos =   GET_CAM_COORD (NewTestCams[gTargetCam] )
									SET_CAM_PARAMS ( NewTestCams[gTargetCam], <<test_Cam_pos.x, test_Cam_pos.y, test_Cam_pos.z + 5.0>>+ gvMapOffset, <<0.0, 0.0, 0.0>>,  65.0, gInterpTime )
									
								ENDIF
								gsetCamPos =FALSE
							ENDIF

								if (gPoint_ped)	
									IF  (gPoint_vehicle = FALSE) and (gPoint_object = FALSE)
										IF DOES_CAM_EXIST (NewTestCams[gTargetCam])				
											IF NOT IS_PED_INJURED (scplayer)
												POINT_CAM_AT_PED_BONE(NewTestCams[gTargetCam], scplayer, INT_TO_ENUM(PED_BONETAG, gPoint_PedBoneTag), gPointOffset, gPointRelative   )
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								if (gPoint_vehicle)	
									IF  (gPoint_object = FALSE) and (gPoint_ped = FALSE)
										IF DOES_CAM_EXIST (NewTestCams[gTargetCam])				
											IF  IS_VEHICLE_DRIVEABLE (TestVehicles[0].Vehicle )
												POINT_CAM_AT_ENTITY(NewTestCams[gTargetCam], TestVehicles[0].vehicle, gPointOffset, gPointRelative   )
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								
								if (gStopPoint)
									IF DOES_CAM_EXIST (NewTestCams[gTargetCam])		
										STOP_CAM_POINTING (NewTestCams[gTargetCam])	
									ENDIF
									gPoint_ped = FALSE
									gPoint_vehicle = FALSE
									
									gStopPoint = FALSE
									
								ENDIF
								
							BREAK
					
						ENDSWITCH
					BREAK
				
					CASE ScenarioSplineCams
						
						SWITCH ScenarioSplineCams_Status
							CASE  ScenarioSplineCams_Init
						
								NewTestCams[0] = CREATE_CAMERA (CAMTYPE_SPLINE_SMOOTHED)
								SET_CAM_FOV(NewTestCams[0], 20)
								SET_CAM_SPLINE_SMOOTHING_STYLE(NewTestCams[0] , CAM_SPLINE_NO_SMOOTH)
							
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[0] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<< 1.20, -5.93, 15>>, <<-19, 0, 0>>, 60, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[0] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -7.20, 6.93, 11>>, <<-28, 0, -116.0>>, 50, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[0] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<< 9.20, 9.93, 8>>, <<-28, 0, 129.0>>, 40, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								
								SET_CAM_ACTIVE (NewTestCams[0], TRUE )
								RENDER_SCRIPT_CAMS (TRUE, FALSE)
								ScenarioSplineCams_Status = ScenarioSplineCams_Run
						
							BREAK
							
							CASE ScenarioSplineCams_Run
								PRINTFLOAT	(GET_CAM_SPLINE_PHASE(NewTestCams[0]))
								PRINTNL()
								
								if(gbSetSplinePhase)
									SET_CAM_SPLINE_PHASE(NewTestCams[0] ,gfSplinePhase)
									gbSetSplinePhase = FALSE
								ENDIF	
								
								if(GET_CAM_SPLINE_PHASE(NewTestCams[0]) = 1.0)
									Set_Test_State_To_Default()
								ENDIF
								
							BREAK
							ENDSWITCH
					BREAK
				
					CASE TimedSplineTest
						
						SWITCH ScenarioSplineCams_Status
							CASE  ScenarioSplineCams_Init
						
								NewTestCams[0] = CREATE_CAMERA (CAMTYPE_SPLINE_TIMED)
								SET_CAM_FOV(NewTestCams[0], 20)
								SET_CAM_SPLINE_SMOOTHING_STYLE(NewTestCams[0] , CAM_SPLINE_NO_SMOOTH)
							
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[0] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<< 1.20, -5.93, 15>>, <<-19, 0, 0>>, 60, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[0] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -7.20, 6.93, 11>>, <<-28, 0, -116.0>>, 50, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[0] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<< 9.20, 9.93, 8>>, <<-28, 0, 129.0>>, 40, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								
								SET_CAM_ACTIVE (NewTestCams[0], TRUE )
								RENDER_SCRIPT_CAMS (TRUE, FALSE)
								ScenarioSplineCams_Status = ScenarioSplineCams_Run
						
							BREAK
							
							CASE ScenarioSplineCams_Run
								PRINTFLOAT	(GET_CAM_SPLINE_PHASE(NewTestCams[0]))
								PRINTNL()
								
								if(gbSetSplinePhase)
									SET_CAM_SPLINE_PHASE(NewTestCams[0] ,gfSplinePhase)
									gbSetSplinePhase = FALSE
								ENDIF	
								
								if(GET_CAM_SPLINE_PHASE(NewTestCams[0]) = 1.0)
									Set_Test_State_To_Default()
								ENDIF
								
							BREAK
							ENDSWITCH
					BREAK
				
					CASE RoundedSplineTest
						
						SWITCH ScenarioSplineCams_Status
							CASE  ScenarioSplineCams_Init
						
								NewTestCams[0] = CREATE_CAMERA (CAMTYPE_SPLINE_ROUNDED)
								SET_CAM_SPLINE_DURATION(NewTestCams[0], 9000 )
	
								SET_CAM_FOV(NewTestCams[0], 20)
								SET_CAM_SPLINE_SMOOTHING_STYLE(NewTestCams[0] , CAM_SPLINE_NO_SMOOTH)
							
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[0] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<< 1.20, -5.93, 15>>, <<-19, 0, 0>>, 60, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[0] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -7.20, 6.93, 11>>, <<-28, 0, -116.0>>, 50, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[0] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<< 9.20, 9.93, 8>>, <<-28, 0, 129.0>>, 40, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								
								SET_CAM_ACTIVE (NewTestCams[0], TRUE )
								RENDER_SCRIPT_CAMS (TRUE, FALSE)
								ScenarioSplineCams_Status = ScenarioSplineCams_Run
						
							BREAK
							
							CASE ScenarioSplineCams_Run
								PRINTFLOAT	(GET_CAM_SPLINE_PHASE(NewTestCams[0]))
								PRINTNL()
								
								if(gbSetSplinePhase)
									SET_CAM_SPLINE_PHASE(NewTestCams[0] ,gfSplinePhase)
									gbSetSplinePhase = FALSE
								ENDIF	
								
								if(GET_CAM_SPLINE_PHASE(NewTestCams[0]) = 1.0)
									Set_Test_State_To_Default()
								ENDIF
								
							BREAK
							ENDSWITCH
					BREAK
				
				
					CASE SmoothedSplineTest
						
						SWITCH ScenarioSplineCams_Status
							CASE  ScenarioSplineCams_Init
						
								NewTestCams[0] = CREATE_CAMERA (CAMTYPE_SPLINE_SMOOTHED)
								SET_CAM_SPLINE_DURATION(NewTestCams[0], 10000 )
								SET_CAM_FOV(NewTestCams[0], 20)
								SET_CAM_SPLINE_SMOOTHING_STYLE(NewTestCams[0] , CAM_SPLINE_NO_SMOOTH)
							
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[0] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<< 1.20, -5.93, 15>>, <<-19, 0, 0>>, 60, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[0] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -7.20, 6.93, 11>>, <<-28, 0, -116.0>>, 50, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[0] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<< 9.20, 9.93, 8>>, <<-28, 0, 129.0>>, 40, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								
								SET_CAM_ACTIVE (NewTestCams[0], TRUE )
								RENDER_SCRIPT_CAMS (TRUE, FALSE)
								ScenarioSplineCams_Status = ScenarioSplineCams_Run
						
							BREAK
							
							CASE ScenarioSplineCams_Run
								PRINTFLOAT	(GET_CAM_SPLINE_PHASE(NewTestCams[0]))
								PRINTNL()
								
								if(gbSetSplinePhase)
									SET_CAM_SPLINE_PHASE(NewTestCams[0] ,gfSplinePhase)
									gbSetSplinePhase = FALSE
								ENDIF	
								
								if(GET_CAM_SPLINE_PHASE(NewTestCams[0]) = 1.0)
									Set_Test_State_To_Default()
								ENDIF
								
							BREAK
							ENDSWITCH
					BREAK
				
					CASE SplineLookAtSpline
						
						SWITCH ScenarioSplineCams_Status
							CASE  ScenarioSplineCams_Init
						
								NewTestCams[0] = CREATE_CAMERA (CAMTYPE_SPLINE_ROUNDED)
								NewTestCams[1] = CREATE_CAMERA (CAMTYPE_SPLINE_SMOOTHED)
								
								SET_CAM_SPLINE_DURATION(NewTestCams[0], 10000 )
								SET_CAM_FOV(NewTestCams[0], 20)
								SET_CAM_SPLINE_SMOOTHING_STYLE(NewTestCams[0] , CAM_SPLINE_NO_SMOOTH)
							
								//looking spline
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[0] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<< 1.20, -5.93, 15>>, <<-19, 0, 0>>, 60, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[0] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << -7.20, 6.93, 11>>, <<-28, 0, -116.0>>, 50, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
									ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[0] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<3.88, -2.62, 13.92>>, <<-28, 0, 129.0>>, 40, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[0] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<2.54, 11.62, 13.92>>, <<-28, 0, 129.0>>, 40, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								//lookedatspline
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[1] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<< -0.30, -2.79, 6.35>>, <<-19, 0, 0>>, 60, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[1] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-1.63, 4.05, 6.35>>, <<-28, 0, -116.0>>, 50, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								ADD_CAM_SPLINE_NODE_USING_CAMERA(NewTestCams[1] , CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<11.35, 0.89, 6.35>>, <<-28, 0, -116.0>>, 50, TRUE), 5000, CAM_SPLINE_NODE_NO_FLAGS)
								
								
								SET_CAM_SPLINE_DURATION(NewTestCams[1], 10000 )
								SET_CAM_SPLINE_SMOOTHING_STYLE(NewTestCams[1] , CAM_SPLINE_NO_SMOOTH)
								
								POINT_CAM_AT_CAM(NewTestCams[0], NewTestCams[1])
								SET_CAM_ACTIVE (NewTestCams[1], TRUE )
								SET_CAM_ACTIVE (NewTestCams[0], TRUE )
								RENDER_SCRIPT_CAMS (TRUE, FALSE)
								ScenarioSplineCams_Status = ScenarioSplineCams_Run
						
							BREAK
							
							CASE ScenarioSplineCams_Run
								PRINTFLOAT	(GET_CAM_SPLINE_PHASE(NewTestCams[0]))
								PRINTNL()
								
								if(gbSetSplinePhase)
									SET_CAM_SPLINE_PHASE(NewTestCams[0] ,gfSplinePhase)
									gbSetSplinePhase = FALSE
								ENDIF	
								
								if(GET_CAM_SPLINE_PHASE(NewTestCams[0]) = 1.0)
									Set_Test_State_To_Default()
								ENDIF
								
							BREAK
							ENDSWITCH
					BREAK
				
				
				
					CASE ScenarioInterpCams
				
						NewTestCams[0] = CREATE_CAM_WITH_PARAMS ("DEFAULT_SCRIPTED_CAMERA", << 1.20, -5.93, 5.26>>+ gvMapOffset, <<-19, 0, 0>> )
						NewTestCams[1] = CREATE_CAM_WITH_PARAMS ("DEFAULT_SCRIPTED_CAMERA", << -7.20, 6.93, 6.9>>+ gvMapOffset, <<-28, 0, -116.0>> )
						
						SET_CAM_ACTIVE_WITH_INTERP (NewTestCams[1], NewTestCams[0],  5000 )
						RENDER_SCRIPT_CAMS (TRUE, FALSE)
						Set_Test_State_To_Default()
					BREAK

					CASE ScenarioShakeCam
						SWITCH ScenarioShakeCam_Status
							CASE ScenarioShakeCam_Init
								NewTestCams[0] = CREATE_CAM_WITH_PARAMS ("DEFAULT_SCRIPTED_CAMERA", << 1.20, -5.93, 5.26>>+ gvMapOffset, <<-19, 0, 0>> )
								
								SET_CAM_ACTIVE (NewTestCams[0], true )
								RENDER_SCRIPT_CAMS (TRUE, TRUE)
								ScenarioShakeCam_Status = ScenarioShakeCam_Run
							BREAK

							CASE ScenarioShakeCam_Run
								IF(gShouldShakeScriptGlobal)
									IF (gbStockShake)
										SHAKE_SCRIPT_GLOBAL("MEDIUM_EXPLOSION_SHAKE", gAmplitude)
										gbStockShake = FALSE
									ENDIF
									
									IF(IS_SCRIPT_GLOBAL_SHAKING())
										SET_SCRIPT_GLOBAL_SHAKE_AMPLITUDE(gAmplitude)
									ENDIF
								ELSE
									IF (gbStockShake)
										SHAKE_CAM(NewTestCams[0], "MEDIUM_EXPLOSION_SHAKE", gAmplitude)
										gbStockShake = FALSE
									ENDIF
								
									IF(IS_CAM_SHAKING(NewTestCams[0]))
										SET_CAM_SHAKE_AMPLITUDE(NewTestCams[0], gAmplitude)
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					
					CASE ScenarioAnimateCam
						IF NOT gbHasFinishedAnimatedCamera
							IF NOT gbHasStartedAnimatedCamera
								//Load the animation dictionary.
								REQUEST_ANIM_DICT("missintro_bank")
								WHILE NOT HAS_ANIM_DICT_LOADED("missintro_bank")
									WAIT(0)
								ENDWHILE
								
								IF NOT IS_PED_INJURED(scplayer)
									NewTestCams[0] = CREATE_CAM ("DEFAULT_ANIMATED_CAMERA")
									
									VECTOR playerGroundPosition
									playerGroundPosition = GET_ENTITY_COORDS(scplayer)
									GET_GROUND_Z_FOR_3D_COORD(playerGroundPosition, playerGroundPosition.z)
									
									FLOAT playerHeading
									playerHeading = GET_ENTITY_HEADING(scplayer)
									playerHeading = playerHeading + 180.0
									IF (playerHeading >= 360.0)
										playerHeading = playerHeading - 360.0
									ENDIF
									
									PLAY_CAM_ANIM(NewTestCams[0], "cam_tie_up_guard", "missintro_bank", playerGroundPosition, <<0.0, 0.0, playerHeading>>, CAF_LOOPING)
									SET_CAM_ANIM_CURRENT_PHASE(NewTestCams[0], gAnimationStartPhase)
									SET_CAM_ACTIVE (NewTestCams[0], true )
									RENDER_SCRIPT_CAMS (TRUE, FALSE)
									
									gbHasStartedAnimatedCamera = TRUE
								ENDIF
							ELIF IS_CAM_PLAYING_ANIM(NewTestCams[0], "cam_tie_up_guard", "missintro_bank")
								//Display the current animation phase.
								FLOAT phase
								phase = GET_CAM_ANIM_CURRENT_PHASE(NewTestCams[0])
								PRINTSTRING("Animation phase = ")
								PRINTFLOAT(phase)
								PRINTNL()
								
								IF (phase >= 1.0)
									//Clean up the camera now it has finished animating.
									//DESTROY_CAM(NewTestCams[0])
									//RENDER_SCRIPT_CAMS (FALSE, FALSE)
									
								//	REMOVE_ANIM_DICT("missintro_bank")
									
								//	gbHasFinishedAnimatedCamera = TRUE
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE ScenarioFadeCam
						
							IF bDoSCreenFadeOut
								DO_SCREEN_FADE_OUT (gDuration)
								bDoSCreenFadeOut = FALSE
							ENDIF
							
							IF bDoSCreenFadeIn
								DO_SCREEN_FADE_IN (gDuration)
								bDoSCreenFadeIn = FALSE
							ENDIF
							
							isScreenFadedin =  IS_SCREEN_FADED_IN ()
							isScreenFadedOut =  IS_SCREEN_FADED_OUT ()
							IsScreenFadingIn	=	IS_SCREEN_FADING_IN()
							IsScreenFadingOut	=	IS_SCREEN_FADING_OUT()
					BREAK
					
			
					CASE ScenarioGameplayCamera
						IF (gSetCamState)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING ( gGameplayHeading )
							SET_GAMEPLAY_CAM_RELATIVE_PITCH ( gGameplayPitch, gGameplayPitchSmoothRate )
							gSetCamState = FALSE
						ELIF (gGetCamState)
							gGameplayHeading = GET_GAMEPLAY_CAM_RELATIVE_HEADING()
							gGameplayPitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
						ENDIF
						
						DRAW_DEBUG_SPHERE (<<1.0, 1.0, 3.0>>, 1.0)
						
						gCamVisiblePoint = IS_SPHERE_VISIBLE(<<1.0, 1.0, 3.0>>+ gvMapOffset, 1.0)
						gCamPos = GET_GAMEPLAY_CAM_COORD()
						gCamRot = GET_GAMEPLAY_CAM_ROT ()
						gCamFov = GET_GAMEPLAY_CAM_FOV()
						
						IF (gbStockShake)
							SHAKE_GAMEPLAY_CAM("MEDIUM_EXPLOSION_SHAKE")
							gbStockShake = FALSE
						ENDIF
						
						IF(IS_GAMEPLAY_CAM_SHAKING())
							SET_GAMEPLAY_CAM_SHAKE_AMPLITUDE(gAmplitude)
						ENDIF
						
						IF (gbFollowNearestPed)
							PED_INDEX closestPed
							IF (GET_CLOSEST_PED(GET_GAMEPLAY_CAM_COORD(), 10.0, TRUE, TRUE, closestPed))
								SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(closestPed)
							ENDIF
						ENDIF
						
						IF (gShouldIgnoreAttachParentMovementForFollowCamera)
							SET_FOLLOW_CAM_IGNORE_ATTACH_PARENT_MOVEMENT_THIS_UPDATE()
						ENDIF
						
						IF (gShouldOverrideFollowPedCamera)
							gOverriddenFollowPedCameraName = GET_CONTENTS_OF_TEXT_WIDGET(gOverriddenFollowPedCameraTextWidgetId)
							SET_FOLLOW_PED_CAM_THIS_UPDATE(gOverriddenFollowPedCameraName, gOverriddenFollowPedCameraInterpDuration)
						ENDIF
						
						IF (gShouldOverrideFollowVehicleCamera)
							gOverriddenFollowVehicleCameraName = GET_CONTENTS_OF_TEXT_WIDGET(gOverriddenFollowVehicleCameraTextWidgetId)
							SET_FOLLOW_VEHICLE_CAM_THIS_UPDATE(gOverriddenFollowVehicleCameraName, gOverriddenFollowVehicleCameraInterpDuration)
						ENDIF
						
						IF (gShouldOverrideFollowVehicleCameraHighAngleMode)
							SET_FOLLOW_VEHICLE_CAM_HIGH_ANGLE_MODE_THIS_UPDATE(gOverriddenFollowVehicleCameraHighAngleMode)
						ENDIF
						
						IF (gShouldApplyZoomFactor)
							SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(gZoomFactor)
							gShouldApplyZoomFactor = FALSE
						ENDIF
						
						IF (gShouldQueryZoomFactor)
							gZoomFactor = GET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR()
							gShouldQueryZoomFactor = FALSE
						ENDIF
						
						IF (gShouldOverrideFirstPersonAimCameraZoomLimits)
							SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR_LIMITS_THIS_UPDATE(gMinZoomFactor, gMaxZoomFactor)
						ENDIF
						
						IF(g_ShouldDisableAimCameras)
							DISABLE_AIM_CAM_THIS_UPDATE()
						ENDIF
						
						IF (gShouldOverrideFirstPersonAimCameraOrientationLimits)
							//Freeze the player position to prevent the player from rotating and allow relative heading limits to be applied.
							FREEZE_ENTITY_POSITION(scplayer, TRUE)
							SET_FIRST_PERSON_AIM_CAM_RELATIVE_HEADING_LIMITS_THIS_UPDATE(gMinRelativeHeading, gMaxRelativeHeading)
							SET_FIRST_PERSON_AIM_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(gMinRelativePitch, gMaxRelativePitch)
						ELSE
							FREEZE_ENTITY_POSITION(scplayer, FALSE)
						ENDIF
					BREAK
					
					CASE ScenarioDebugCamera
						SET_DEBUG_CAM_ACTIVE(gbDebugCamActiveState, gbShouldIgnoreDebugPadCameraToggle)
					BREAK
					
					CASE ScenarioPedZoom
							IF (gbStockShake)
								SET_FOLLOW_PED_CAM_VIEW_MODE (INT_TO_ENUM ( CAM_VIEW_MODE, gZoomLevel))
								gbStockShake = FALSE
							ENDIF
				
							switch GET_FOLLOW_PED_CAM_ZOOM_LEVEL ()
								CASE PED_ZOOM_LEVEL_NEAR
									PRINTSTRING ("PED_ZOOM_LEVEL_NEAR")
									PRINTNL()
								BREAK
								CASE PED_ZOOM_LEVEL_MEDIUM
									PRINTSTRING ("PED_ZOOM_LEVEL_MEDIUM")
									PRINTNL()
								BREAK
								CASE PED_ZOOM_LEVEL_FAR
									PRINTSTRING ("PED_ZOOM_LEVEL_FAR")
									PRINTNL()
								BREAK
							ENDSWITCH
					BREAK
					
					CASE ScenarioVehicleZoom
							IF (gbStockShake)
								SET_CAM_VIEW_MODE_FOR_CONTEXT(INT_TO_ENUM ( CAM_VIEW_MODE_CONTEXT, gViewModeContext), INT_TO_ENUM ( CAM_VIEW_MODE, gZoomLevel))
								gbStockShake = FALSE
							ENDIF
							
							switch GET_CAM_VIEW_MODE_FOR_CONTEXT(INT_TO_ENUM ( CAM_VIEW_MODE_CONTEXT, gViewModeContext))
								CASE CAM_VIEW_MODE_THIRD_PERSON_NEAR
									PRINTSTRING ("CAM_VIEW_MODE_THIRD_PERSON_NEAR")
									PRINTNL()
								BREAK
								CASE CAM_VIEW_MODE_THIRD_PERSON_MEDIUM
									PRINTSTRING ("CAM_VIEW_MODE_THIRD_PERSON_MEDIUM")
									PRINTNL()
								BREAK
								CASE CAM_VIEW_MODE_THIRD_PERSON_FAR
									PRINTSTRING ("CAM_VIEW_MODE_THIRD_PERSON_FAR")
									PRINTNL()
								BREAK
								CASE CAM_VIEW_MODE_CINEMATIC
									PRINTSTRING ("CAM_VIEW_MODE_CINEMATIC")
									PRINTNL()
								BREAK
								CASE CAM_VIEW_MODE_FIRST_PERSON
									PRINTSTRING ("CAM_VIEW_MODE_FIRST_PERSON")
									PRINTNL()
								BREAK
							ENDSWITCH
							
							IF (gbShouldDisableCinematicBonnetCamera)
								DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
							ENDIF
					BREAK
					
					CASE ScenarioHeliFallout
						
						if(DOES_ENTITY_EXIST(TestVehicles[0].Vehicle))	
							SET_PED_INTO_VEHICLE(GET_PLAYER_PED(GET_PLAYER_INDEX()), TestVehicles[0].Vehicle, VS_BACK_RIGHT )
							SET_PED_CAN_RAGDOLL(GET_PLAYER_PED(GET_PLAYER_INDEX()), FALSE)
						ENDIF
						
						if DOES_ENTITY_EXIST(TestPeds[0].ped) AND DOES_ENTITY_EXIST(TestVehicles[0].Vehicle)
							if not IS_PED_INJURED(TestPeds[0].ped)
								//SET_PED_AS_GROUP_MEMBER(TestPeds[0].ped, GET_PLAYER_GROUP(GET_PLAYER_INDEX()))
								if (IS_VEHICLE_DRIVEABLE(TestVehicles[0].Vehicle))
									SET_PED_INTO_VEHICLE(TestPeds[0].ped, TestVehicles[0].Vehicle)
									TASK_HELI_MISSION(TestPeds[0].ped, TestVehicles[0].Vehicle, NULL, NULL, <<588, 22, 8>>, mISSION_GOTO, 10.0, 5.0, 0.0,50, 30 )
									//TASK_VEHICLE_ESCORT(TestPeds[1].ped, TestVehicles[1].Vehicle, GET_PLAYER_PED(GET_PLAYER_INDEX()), VEHICLE_ESCORT_LEFT, 50.0, DRIVINGMODE_AVOIDCARS)
								ENDIF
							ENDIF
						ENDIF
						
						
						
						
						Set_Test_State_To_Default()	
					BREAK
					
					
					CASE ScenarioVehicleFormationCamera
						if(DOES_ENTITY_EXIST(TestVehicles[4].Vehicle))	
							SET_PED_INTO_VEHICLE(GET_PLAYER_PED(GET_PLAYER_INDEX()), TestVehicles[4].Vehicle )
						ENDIF
						
						
						FOR bikerIndex = 0 to 3
							if DOES_ENTITY_EXIST(TestPeds[bikerIndex].ped) AND DOES_ENTITY_EXIST(TestVehicles[bikerIndex].Vehicle)
								if not IS_PED_INJURED(TestPeds[bikerIndex].ped)
									//SET_PED_AS_GROUP_MEMBER(TestPeds[0].ped, GET_PLAYER_GROUP(GET_PLAYER_INDEX()))
									if (IS_VEHICLE_DRIVEABLE(TestVehicles[bikerIndex].Vehicle))
										SET_PED_INTO_VEHICLE(TestPeds[bikerIndex].ped, TestVehicles[bikerIndex].Vehicle)
										//TASK_VEHICLE_ESCORT(TestPeds[1].ped, TestVehicles[1].Vehicle, GET_PLAYER_PED(GET_PLAYER_INDEX()), VEHICLE_ESCORT_LEFT, 50.0, DRIVINGMODE_AVOIDCARS)
									ENDIF
								ENDIF
							ENDIF
						
						ENDFOR
						
						if(pedoneTask)
							bikerIndex = 0
							if DOES_ENTITY_EXIST(TestPeds[bikerIndex].ped) AND DOES_ENTITY_EXIST(TestVehicles[bikerIndex].Vehicle)
								if not IS_PED_INJURED(TestPeds[bikerIndex].ped)
									SET_PED_AS_GROUP_MEMBER(TestPeds[bikerIndex].ped, GET_PLAYER_GROUP(GET_PLAYER_INDEX()))
									if (IS_VEHICLE_DRIVEABLE(TestVehicles[bikerIndex].Vehicle))
										SET_PED_INTO_VEHICLE(TestPeds[bikerIndex].ped, TestVehicles[bikerIndex].Vehicle)
										TASK_VEHICLE_ESCORT(TestPeds[bikerIndex].ped, TestVehicles[bikerIndex].Vehicle, GET_PLAYER_PED(GET_PLAYER_INDEX()), VEHICLE_ESCORT_RIGHT, 50.0, DRIVINGMODE_AVOIDCARS, pedoneOffset)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						if(pedTwoTask)
							bikerIndex = 1
							if DOES_ENTITY_EXIST(TestPeds[bikerIndex].ped) AND DOES_ENTITY_EXIST(TestVehicles[bikerIndex].Vehicle)
								if not IS_PED_INJURED(TestPeds[bikerIndex].ped)
									SET_PED_AS_GROUP_MEMBER(TestPeds[bikerIndex].ped, GET_PLAYER_GROUP(GET_PLAYER_INDEX()))
									if (IS_VEHICLE_DRIVEABLE(TestVehicles[bikerIndex].Vehicle))
										SET_PED_INTO_VEHICLE(TestPeds[bikerIndex].ped, TestVehicles[bikerIndex].Vehicle)
										TASK_VEHICLE_ESCORT(TestPeds[bikerIndex].ped, TestVehicles[bikerIndex].Vehicle, GET_PLAYER_PED(GET_PLAYER_INDEX()), VEHICLE_ESCORT_LEFT, 50.0, DRIVINGMODE_AVOIDCARS, pedTwoOff)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						if(pedThreeTask)
							bikerIndex = 2
							if DOES_ENTITY_EXIST(TestPeds[bikerIndex].ped) AND DOES_ENTITY_EXIST(TestVehicles[bikerIndex].Vehicle)
								if not IS_PED_INJURED(TestPeds[bikerIndex].ped)
									SET_PED_AS_GROUP_MEMBER(TestPeds[bikerIndex].ped, GET_PLAYER_GROUP(GET_PLAYER_INDEX()))
									if (IS_VEHICLE_DRIVEABLE(TestVehicles[bikerIndex].Vehicle))
										SET_PED_INTO_VEHICLE(TestPeds[bikerIndex].ped, TestVehicles[bikerIndex].Vehicle)
										TASK_VEHICLE_ESCORT(TestPeds[bikerIndex].ped, TestVehicles[bikerIndex].Vehicle, GET_PLAYER_PED(GET_PLAYER_INDEX()), VEHICLE_ESCORT_FRONT, 50.0, DRIVINGMODE_AVOIDCARS, pedThreeOff)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						if(pedFourTask)
							bikerIndex = 3
							if DOES_ENTITY_EXIST(TestPeds[bikerIndex].ped) AND DOES_ENTITY_EXIST(TestVehicles[bikerIndex].Vehicle)
								if not IS_PED_INJURED(TestPeds[bikerIndex].ped)
									SET_PED_AS_GROUP_MEMBER(TestPeds[bikerIndex].ped, GET_PLAYER_GROUP(GET_PLAYER_INDEX()))
									if (IS_VEHICLE_DRIVEABLE(TestVehicles[bikerIndex].Vehicle))
										SET_PED_INTO_VEHICLE(TestPeds[bikerIndex].ped, TestVehicles[bikerIndex].Vehicle)
										TASK_VEHICLE_ESCORT(TestPeds[bikerIndex].ped, TestVehicles[bikerIndex].Vehicle, GET_PLAYER_PED(GET_PLAYER_INDEX()), VEHICLE_ESCORT_REAR, 50.0, DRIVINGMODE_AVOIDCARS, pedFourOff)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						
						
						
						Set_Test_State_To_Default()	
							
							
					BREAK
				
					CASE ScenarioHintCam 
								
								IF(gbStockShake)
									STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(true)
								ENDIF
								
								if (gPoint_ped)	
									IF  (gPoint_vehicle = FALSE) and (gPoint_object = FALSE)		
										IF NOT IS_PED_INJURED (TestPeds[0].ped)
											 SET_GAMEPLAY_ENTITY_HINT (TestPeds[0].ped, gPointOffset, gPointRelative, -1, DEFAULT_DWELL_TIME, DEFAULT_DWELL_TIME, HINTTYPE_NO_FOV )
											gPoint_ped = FALSE
										ENDIF
									ENDIF
								ENDIF
								
								if (gPoint_vehicle)	
									IF  (gPoint_object = FALSE) and (gPoint_ped = FALSE)
											IF  IS_VEHICLE_DRIVEABLE (TestVehicles[0].Vehicle )
												SET_GAMEPLAY_VEHICLE_HINT (TestVehicles[0].vehicle, gPointOffset, gPointRelative   )
												gPoint_vehicle = FALSE
											ENDIF
									ENDIF
								ENDIF
								
								if  (gStopPoint)
									if IS_GAMEPLAY_HINT_ACTIVE()
										STOP_GAMEPLAY_HINT()
									ENDIF 
								ENDIF
								
								IF (gStopPointNow) AND IS_GAMEPLAY_HINT_ACTIVE()
									STOP_GAMEPLAY_HINT(TRUE)
									gStopPointNow = FALSE
								ENDIF
								
								
//								SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
//								
//								if not IS_PED_INJURED(scplayer)
//									if IS_PED_IN_ANY_VEHICLE(scplayer)
//											if(IS_BUTTON_JUST_PRESSED(PAD1, CIRCLE ))
//													SET_GAMEPLAY_COORD_HINT(GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS(), -1, DEFAULT_INTERP_IN_TIME, DEFAULT_INTERP_IN_TIME )
//												ENDIF
//											
//												if NOT ( IS_BUTTON_PRESSED(PAD1, CIRCLE) )
//													if IS_GAMEPLAY_HINT_ACTIVE()
//														STOP_GAMEPLAY_HINT()
//													ENDIF 
//												ENDIF
//									
//									ELSE
//									
//										if(IS_BUTTON_JUST_PRESSED(PAD1, RIGHTSHOULDER1 ))
//											SET_GAMEPLAY_COORD_HINT(GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS(), -1, DEFAULT_INTERP_IN_TIME, DEFAULT_INTERP_IN_TIME )
//										ENDIF
//									
//										if NOT ( IS_BUTTON_PRESSED(PAD1, RIGHTSHOULDER1) )
//											if IS_GAMEPLAY_HINT_ACTIVE()
//												STOP_GAMEPLAY_HINT()
//											ENDIF 
//										ENDIF
//									ENDIF	
//								ENDIF
//								if (gStopPoint)
//									STOP_GAMEPLAY_HINT ()	
//									gPoint_ped = FALSE
//									gPoint_vehicle = FALSE
//									
//									gStopPoint = FALSE
//									
//								ENDIF
				
					BREAK
					
					CASE ScenarioCinematicDriveby
						SWITCH ScenarioCinematicDriveby_Status
							CASE  ScenarioCinematicDriveby_Init
								IF(IS_FOLLOW_VEHICLE_CAM_ACTIVE())
									SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(VEHICLE_ZOOM_LEVEL_CINEMATIC)
									gGameplayHeading = -1000.0
									ScenarioCinematicDriveby_Status = ScenarioCinematicDriveby_Run
								ENDIF
							BREAK
							
							CASE  ScenarioCinematicDriveby_Run
								IF(IS_PLAYER_PLAYING(PLAYER_ID())) AND (IS_PED_DOING_DRIVEBY(PLAYER_PED_ID()))
									IF(gNumFramesAiming = 0) AND (gGameplayHeading != -1000.0)
										//Apply the previous driveby camera heading (relative to the vehicle.)
										SET_GAMEPLAY_CAM_RELATIVE_HEADING(gGameplayHeading)
									ENDIF

									//Grab the heading relative to the vehicle.
									gGameplayHeading = GET_GAMEPLAY_CAM_RELATIVE_HEADING()
									
									gNumFramesAiming = gNumFramesAiming + 1
								ELSE
									gNumFramesAiming = 0
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
				
					CASE ScenarioSceneTransition
						SWITCH ScenarioSceneTransition_Status
							CASE ScenarioSceneTransition_Init
								PRINTSTRING("creating camera")
								PRINTNL()
								NewTestCams[0] = CREATE_CAM ("DEFAULT_SCENE_TRANSITION_CAMERA")
								SET_CAM_ACTIVE(NewTestCams[0], TRUE)
								
							if not IS_PED_INJURED(	GET_PLAYER_PED(GET_PLAYER_INDEX()))
									PRINTSTRING("START_SCENE_TRANSITION")
									PRINTNL()
									 vStart = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))						
					
									START_CAM_TRANSITION(NewTestCams[0], vStart, gCamPos ) 
									RENDER_SCRIPT_CAMS (TRUE, TRUE)
								ENDIF	
								
								ScenarioSceneTransition_Status = ScenarioSceneTransition_WaitToFinish
							BREAK
						
							CASE ScenarioSceneTransition_WaitToFinish
								if DOES_CAM_EXIST(NewTestCams[0])
								
									if not	IS_CAM_TRANSITIONING(NewTestCams[0])
										RENDER_SCRIPT_CAMS (FALSE, FALSE)
										SET_CAM_ACTIVE(NewTestCams[0], FALSE)
										DESTROY_CAM(NewTestCams[0])				
									ELSE
									PRINTFLOAT(GET_CAM_TRANSITION_PHASE(NewTestCams[0]))
									PRINTNL()
									ENDIF
								ENDIF
							BREAK
						
						ENDSWITCH
					BREAK 
					
					CASE ScenarioCameraFocusPed
						SWITCH ScenarioCameraFocusPed_Status
							CASE ScenarioCameraFocusPed_Init
								ScenarioCameraFocusPed_Status = ScenarioCameraFocusPed_WaitToFinish
							BREAK
						
							CASE ScenarioCameraFocusPed_WaitToFinish
								gFocusPedIndex = GET_FOCUS_PED_ON_SCREEN(gFocusPedMaxDistanceFromCamera, INT_TO_ENUM(PED_BONETAG, gFocusPedScreenPositionTestBoneTag), gFocusPedMaxScreenWidthRatioAroundCentreForTestBone, gFocusPedMaxScreenHeightRatioAroundCentreForTestBone, gFocusPedMinRelativeHeadingScore, gFocusPedMaxScreenCentreScoreBoost, gFocusPedMaxScreenRatioAroundCentreForScoreBoost, INT_TO_ENUM(PED_BONETAG, gFocusPedLosTestBoneTag1), INT_TO_ENUM(PED_BONETAG, gFocusPedLosTestBoneTag2))
								IF (gFocusPedIndex != NULL)
									IF NOT IS_PED_INJURED(gFocusPedIndex)
										vStart = GET_PED_BONE_COORDS(gFocusPedIndex, BONETAG_HEAD, <<0.0, 0.0, 0.0>>)
										DRAW_DEBUG_SPHERE(vStart, 0.2, 255, 0, 0, 128)
									ENDIF
								ENDIF
							BREAK
						
						ENDSWITCH
					BREAK
					
					CASE ScenarioScriptedFlyCamera
						SWITCH ScenarioScriptedFlyCamera_Status
							CASE ScenarioScriptedFlyCamera_Init
								NewTestCams[0] = CREATE_CAMERA(CAMTYPE_SCRIPTED_FLY, TRUE)
								IF DOES_CAM_EXIST(NewTestCams[0])
									IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
										vStart = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
										SET_CAM_COORD(NewTestCams[0], vStart)
									ENDIF
									
									RENDER_SCRIPT_CAMS(TRUE, FALSE)
								ENDIF
								
								ScenarioScriptedFlyCamera_Status = ScenarioScriptedFlyCamera_WaitToFinish
							BREAK
						
							CASE ScenarioScriptedFlyCamera_WaitToFinish
								
							BREAK
						
						ENDSWITCH
					BREAK
					
					CASE ScenarioAdaptiveDofTest
						SWITCH ScenarioAdaptiveDofTest_Status
							CASE  ScenarioAdaptiveDofTest_Init
								NewTestCams[0] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
								SET_CAM_PARAMS(NewTestCams[0],<<8.504449,-13.629935,8.343534>>,<<-1.497766,-0.000000,42.899403>>,2.599557)
								SET_CAM_ACTIVE (NewTestCams[0], TRUE )

								RENDER_SCRIPT_CAMS (TRUE, FALSE)	
								
								ScenarioAdaptiveDofTest_Status = ScenarioAdaptiveDofTest_Run
							
							BREAK
							
							CASE ScenarioAdaptiveDofTest_Run
								IF DOES_CAM_EXIST(NewTestCams[0])
									SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE(NewTestCams[0], g_AdaptiveDofOverriddenFocusDistance)
									SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE_BLEND_LEVEL(NewTestCams[0], g_AdaptiveDofOverriddenFocusDistanceBlendLevel)
									
									SET_CAM_DOF_FOCUS_DISTANCE_GRID_SCALING(NewTestCams[0], g_AdaptiveDofFocusDistanceGridScalingX, g_AdaptiveDofFocusDistanceGridScalingY)
									SET_CAM_DOF_SUBJECT_MAGNIFICATION_POWER_FACTOR_NEAR_FAR(NewTestCams[0], g_AdaptiveDofSubjectMagnificationPowerFactorNear, g_AdaptiveDofSubjectMagnificationPowerFactorFar)
									SET_CAM_DOF_MAX_PIXEL_DEPTH(NewTestCams[0], g_AdaptiveDofMaxPixelDepth)
									SET_CAM_DOF_PIXEL_DEPTH_POWER_FACTOR(NewTestCams[0], g_AdaptiveDofPixelDepthPowerFactor)
									SET_CAM_DOF_FNUMBER_OF_LENS(NewTestCams[0], g_AdaptiveDofFNumberOfLens)
									SET_CAM_DOF_FOCAL_LENGTH_MULTIPLIER(NewTestCams[0], g_AdaptiveDofFocalLengthMultiplier)
									SET_CAM_DOF_FOCUS_DISTANCE_BIAS(NewTestCams[0], g_AdaptiveDofFocusDistanceBias)
									SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE(NewTestCams[0], g_AdaptiveDofMaxNearInFocusDistance)
									SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(NewTestCams[0], g_AdaptiveDofMaxNearInFocusDistanceBlendLevel)
									SET_CAM_DOF_MAX_BLUR_RADIUS_AT_NEAR_IN_FOCUS_LIMIT(NewTestCams[0], g_AdaptiveDofMaxBlurRadiusAtNearInFocusLimit)
									SET_CAM_DOF_FOCUS_DISTANCE_INCREASE_SPRING_CONSTANT(NewTestCams[0], g_AdaptiveDofFocusDistanceIncreaseSpringConstant)
									SET_CAM_DOF_FOCUS_DISTANCE_DECREASE_SPRING_CONSTANT(NewTestCams[0], g_AdaptiveDofFocusDistanceDecreaseSpringConstant)
									SET_CAM_DOF_SHOULD_FOCUS_ON_LOOK_AT_TARGET(NewTestCams[0], g_AdaptiveDofShouldFocusOnLookAtTarget)
									SET_CAM_DOF_SHOULD_FOCUS_ON_ATTACH_PARENT(NewTestCams[0], g_AdaptiveDofShouldFocusOnAttachParent)
									SET_CAM_DOF_SHOULD_KEEP_LOOK_AT_TARGET_IN_FOCUS(NewTestCams[0], g_AdaptiveDofShouldKeepLookAtTargetInFocus)
									SET_CAM_DOF_SHOULD_KEEP_ATTACH_PARENT_IN_FOCUS(NewTestCams[0], g_AdaptiveDofShouldKeepAttachParentInFocus)
									SET_CAM_DOF_SHOULD_MEASURE_POST_ALPHA_PIXEL_DEPTH(NewTestCams[0], g_AdaptiveDofShouldMeasurePostAlphaPixelDepth)
								ENDIF
							BREAK
							
						ENDSWITCH
					BREAK
				
				CASE DefaultTest
				
				BREAK
				
				ENDSWITCH	
	
	ENDPROC


//Purpose: Runs the main scenarios
PROC Run_Test_Scenario ()
	int index = 0
	
	SWITCH TestScenarioAStatus
		//setps up all the data for the sceanrio, this is only called once or if selected in widget
		CASE InitialiseScenarioData
			PlayerStartPos = GET_PLAYER_START_POS()			//sets the players start coords at the default
			Bscenario_running = FALSE
			INITIALISE_PED_DATA (Testpeds)
			INITIALISE_VEHICLE_DATA (TestVehicles)
			INITIALISE_CAM_DATA(TestCams )
			SETUP_TEST_DATA ()
	
			HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
			TestScenarioAStatus = CreateScenarioEntities
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)		//sets the 
		
		BREAK
		
		// Creates all the scenario data
		CASE CreateScenarioEntities
			Bscenario_running = FALSE
			
			Set_Gang_Relationships (TRUE)	
		
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
		
			//peds
			FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
				CREATE_PED_ENTITY(Testpeds[index])
				GiveBlipsToPedsGrps (Testpeds[index])
				ALTER_COMBAT_STATS (Testpeds[index] )
				SWAP_PED_WEAPONS (Testpeds[index])
				ADD_RELATIONSHIP_GROUP_TO_STRUCT (Testpeds[index])
				Block_Peds_Temp_Events (Testpeds[index], TRUE)
				SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
			ENDFOR
			
			//TestVehicles
			FOR Index = 0 to MAX_NUMBER_OF_VEHICLES -1
				CREATE_VEHICLE_ENTITY (TestVehicles[index] )
			ENDFOR
		
			//TestCams
			for index = 0 to MAX_NUMBER_OF_CAMERAS -1
				CREATE_CAM_ENTITY (TestCams[index])
			ENDFOR
			
			TestScenarioAStatus = SetScenarioEntities
		BREAK
		
		CASE SetScenarioEntities
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
			
			Start_And_Reset_Test ()
			
			IF gBeginCombatScenario
				TestScenarioAStatus = RunScenario
				gBeginCombatScenario = FALSE
				gRun_debuggig = FALSE
				Bscenario_running = TRUE
				INITALISE_TEST_STATE()
				TEMP_ACTIVATE_CAMS (TestCams[FixedCamera].cam )
				
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam )
					HELP_TEXT_STATE =  HTF_SCENARIO_RUNNING_FIXED_CAM
				ELSE
					HELP_TEXT_STATE = HTF_STARTED_RUNNING_NO_CAMS
				ENDIF
			ENDIF
		BREAK
		
		//Runs the actual selected scenario
		
		CASE RunScenario
				
			Temp_Run_Scenario_Tracking_Cam (TestPeds, MAX_NUMBER_OF_PEDS, TestCams[TrackingCamera].cam) 
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
				
			Start_And_Reset_Test ()
				
		 	Check_For_Scenario_Reset ()
			
			IF DOES_CAM_EXIST (TestCams[TrackingCamera].cam)
				if IS_CAM_RENDERING (TestCams[TrackingCamera].cam)
					HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_TRACKING_CAM
				ELSE
					HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_FIXED_CAM
				ENDIF
			ENDIF
					
			RUN_TEST ()		//run the main tests 		
			
		BREAK 
				
		CASE CleanupScenario
			
			HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED

			Cleanup_Scenario_Entities ()
			
			IF gResetToDefault 	
				Temp_cleanup_scenario_cams ()		//here we are changing scenarios so we need to reset cams
				TestScenarioAStatus = InitialiseScenarioData
				gResetToDefault = FALSE
			ENDIF
		
			IF gResetCombatScenario
				Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)
				gcurrentselection = gSelection 
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
					ACTIVATE_CAM (TestCams[FixedCamera].cam)
				ELSE
					Temp_cleanup_scenario_cams ()	
				ENDIF
				TestScenarioAStatus = CreateScenarioEntities
				gResetCombatScenario = FALSE
			ENDIF
		
		BREAK
		
	ENDSWITCH
ENDPROC


PROC EditScenario ()
	IF gRun_debuggig
		SETUP_PEDS_POS (TestPeds[gTestPedsIndex])		
		IF gCreatePed
			PedModelSelection (TestPeds[gTestPedsIndex], S_M_Y_Cop_01)
		
			CREATE_PED_ENTITY (TestPeds[gTestPedsIndex])
			ADD_RELATIONSHIP_GROUP_TO_STRUCT (TestPeds[gTestPedsIndex])
			gCreatePed = FALSE
		ENDIF
		
		IF gdeletePed 
			delete_scenario_ped (TestPeds[gTestPedsIndex] )
			gdeletePed = FALSE
		ENDIF
	ENDIF	
ENDPROC

PROC Terminate_test_script ()
	if IS_KEYBOARD_KEY_JUST_PRESSED (KEY_S)
		CLEAR_PRINTS ()
		CLEAR_HELP ()
		Cleanup_Scenario_Entities ()
		Temp_cleanup_scenario_cams ()
		SET_PLAYER_COLISION(scplayer, true)
		IF NOT IS_PED_INJURED (scplayer)
				  SET_PED_COORDS_KEEP_VEHICLE(scplayer, GET_PLAYER_START_POS () + gvMapOffset )
		ENDIF
		
		TERMINATE_THIS_THREAD ()
		
	ENDIF
ENDPROC



SCRIPT
	
	gvMapOffset =  GET_PLAYER_START_POS () //<<0.0, 0.0, 6.35>>
	
	SET_DEBUG_ACTIVE (TRUE)
	
	SETUP_MISSION_XML_MENU ( XMLMenu, KEY_Q)
	
	SETUP_AREA_FOR_MISSION (<<0.0, 0.0, 0.0>>)
	
	//Gets a reference to the player
	Get_The_Player ()
	
	SET_PLAYER_COLISION(scplayer, true)

	//Sets the test widget for this script
	CREATE_TEST_WIDGET ()
	
	//request the test anim bank
	//REQUEST_TEST_ANIM_DICT ("misstest_anim")
	
WHILE TRUE
		// controls the help text hides if xml menu is active
		TEXT_CONTROLLER ()
	
		// Can set all scenario peds invincible from the widget
		Set_Scenario_Peds_Invincible (TestPeds, MAX_NUMBER_OF_PEDS, gsetpedsinvincible )
	
		//User can create a debug cam for setting sceanrios
		Temp_Create_Debug_Cam (TestCams[FixedCamera]) 
		
		//Runs the selected option from the XML menu 
		Run_Selection_From_XML_input ()
		
		//Checks that a valid selection has been input and runs the scenario
		IF (gcurrentselection <> InvalidSelection)
			
			Draw_Debug_Info (  )
			
			//Sets the test scenario into debug mode
			Set_To_Debug ()
			
			if (gRun_debuggig)
				Temp_Debug_Scenario (TestPeds, TestVehicles, Route, TestCams[FixedCamera].cam, S_M_Y_Cop_01 )								//Allows the entities in the scneario to be adjusted
				Print_Scenario_Data (TestPeds, TestVehicles, Route, TestCams)
			ENDIF
			
			Run_Test_Scenario ()								
			
			SWITCH_BETWEEN_FIXED_AND_TRACKING (TestCams[FixedCamera].cam)
			
			PRINT_ACTIVE_TEST (Bscenario_running, gRun_debuggig )		
		ENDIF
		
		WAIT (0)
		
		Terminate_test_script ()
		
	
	ENDWHILE


ENDSCRIPT


#ENDIF	// IS_DEBUG_BUILD

