#IF IS_FINAL_BUILD
	SCRIPT 
	ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Fraser Morgan				Date: 11/05/2012			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│		SCRIPT NAME: PROP DROP													│
//│		DESCRIPTION: Create any prop and either drop it from set height,		│
//│					 or fire it into another custom prop.						│
//│					 For testing Prop collision audio.							│
//╘═════════════════════════════════════════════════════════════════════════════╛

// HEADERS  
USING "rage_builtins.sch"
USING "globals.sch" 
USING "brains.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_camera.sch"
USING "commands_ped.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "commands_physics.sch"
USING "commands_misc.sch"
USING "flow_public_core_override.sch"
USING "weapon_enums.sch"


OBJECT_INDEX PROP_A
OBJECT_INDEX PROP_B
MODEL_NAMES prop_model_A
MODEL_NAMES prop_model_B



// VECTOR vOffset
//VECTOR vPlayerPos
VECTOR vPropALocation, vADistance, vTransAPos
VECTOR vPropBLocation, vBDistance, vTransBPos
VECTOR vFront, vSide, vUp, vPos
VECTOR force_vec_ba, force_vec_ab
VECTOR vPropACurrent, vPropBCurrent
VECTOR vRotationA, vRotationB


FLOAT fPropAHeight 		= 0
FLOAT fPropBHeight		= 0
FLOAT fADistance		= 1
FLOAT fBDistance		= 1
FLOAT force_multiplier	= 0
FLOAT fADelta			= 1
FLOAT fBDelta			= 1
FLOAT fX, fY, fZ

INT iSeconds			= 1000

BOOL bPropACreated   		= FALSE
BOOL bPropBCreated 	 		= FALSE
BOOL bThrowPropsTogether	= FALSE
BOOL bCleanedLastProp		= FALSE
BOOL bClearProps			= FALSE
BOOL bRandomRot				= FALSE
BOOL bAutoReset				= FALSE
BOOL bFireAatB				= FALSE
BOOL bFireBatA				= FALSE
BOOL bBothProps				= TRUE
BOOL bAmbience


WIDGET_GROUP_ID createPropDropWidgets
TEXT_WIDGET_ID twid_PropASelection
TEXT_WIDGET_ID twid_PropBSelection

PROC CLEANUP_SCRIPT()
		IF DOES_WIDGET_GROUP_EXIST(createPropDropWidgets)
			DELETE_WIDGET_GROUP(createPropDropWidgets)
		ENDIF
		IF DOES_ENTITY_EXIST(PROP_A)
			DELETE_OBJECT(PROP_A)
		ENDIF
		IF DOES_ENTITY_EXIST(PROP_B)
			DELETE_OBJECT(PROP_B)
		ENDIF 
	TERMINATE_THIS_THREAD()
ENDPROC
PROC CREATE_PROP_A()

	prop_model_A = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(twid_PropASelection)))
	
	IF NOT bCleanedLastProp
		IF DOES_ENTITY_EXIST(PROP_A)
			DELETE_OBJECT(PROP_A)
		ENDIF
		bCleanedLastProp = TRUE
	ENDIF

	
		
	IF NOT DOES_ENTITY_EXIST(PROP_A)
		PROP_A = CREATE_OBJECT(prop_model_A, vPropALocation)
			IF bRandomRot
				fX = GET_RANDOM_FLOAT_IN_RANGE(0.0, 180.0)
				fY = GET_RANDOM_FLOAT_IN_RANGE(0.0, 180.0)
				fZ = GET_RANDOM_FLOAT_IN_RANGE(0.0, 180.0)
				vRotationA = << fX, fY, fZ >>
				SET_ENTITY_ROTATION(PROP_A, vRotationA, EULER_XYZ)
			ENDIF
		bCleanedLastProp = FALSE
	ENDIF
ENDPROC
PROC CREATE_PROP_B()

	prop_model_B = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(twid_PropBSelection)))


	IF NOT bCleanedLastProp
		IF DOES_ENTITY_EXIST(PROP_B)
			DELETE_OBJECT(PROP_B)
		ENDIF
		bCleanedLastProp = TRUE
	ENDIF
	
		
	IF NOT DOES_ENTITY_EXIST(PROP_B)
		PROP_B = CREATE_OBJECT(prop_model_B, vPropBLocation)
			IF bRandomRot
				fX = GET_RANDOM_FLOAT_IN_RANGE(0.0, 180.0)
				fY = GET_RANDOM_FLOAT_IN_RANGE(0.0, 180.0)
				fZ = GET_RANDOM_FLOAT_IN_RANGE(0.0, 180.0)
				vRotationB = << fX, fY, fZ >>
				SET_ENTITY_ROTATION(PROP_B, vRotationB, EULER_XYZ)
			ENDIF
		bCleanedLastProp = FALSE
	ENDIF
ENDPROC

PROC CREATE_PROP_DROP_WIDGETS()
//	#IF IS_DEBUG_BUILD
	createPropDropWidgets = START_WIDGET_GROUP("Prop Drop")
	
		ADD_WIDGET_BOOL("Mute Ambience", bAmbience)
			
		START_WIDGET_GROUP("Prop Creation")
			ADD_WIDGET_STRING("Prop A Creation")
			twid_PropASelection = ADD_TEXT_WIDGET("Prop A")
			SET_CONTENTS_OF_TEXT_WIDGET(twid_PropASelection, "Enter prop model name...")
			ADD_WIDGET_FLOAT_SLIDER("Prop A Height:", fPropAHeight, -1, 70, 1)
			ADD_WIDGET_FLOAT_SLIDER("Prop A Left Dist:", fADelta, 0, 10, 1)
			ADD_WIDGET_FLOAT_SLIDER("Prop A Forward Dist:", fADistance, 0, 20, 1)
			ADD_WIDGET_BOOL("Create Prop A", bPropACreated)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_STRING("Prop B Creation")
			twid_PropBSelection = ADD_TEXT_WIDGET("Prop B:")
			SET_CONTENTS_OF_TEXT_WIDGET(twid_PropBSelection, "Enter prop model name...")
			ADD_WIDGET_FLOAT_SLIDER("Prop B Height:", fPropBHeight, -1, 70, 1)
			ADD_WIDGET_FLOAT_SLIDER("Prop B Right Dist:", fBDelta, 0, 10, 1)
			ADD_WIDGET_FLOAT_SLIDER("Prop B Forward Dist:", fBDistance, 0, 20, 1)
			ADD_WIDGET_BOOL("Create Prop B", bPropBCreated)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_STRING("Automatic Creation Options")
			ADD_WIDGET_BOOL("Apply Random Rotation When Spawning", bRandomRot)
			ADD_WIDGET_BOOL("Auto-Reset Props After Firing", bAutoReset)
			ADD_WIDGET_INT_SLIDER("Time to Auto-Reset Props (ms)", iSeconds, 100, 10000, 500)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Clear All Props", bClearProps)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Firing Options")
			ADD_WIDGET_FLOAT_SLIDER("Amount of Force:", force_multiplier, 0, 20, 1)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_STRING("Firing Method")
			ADD_WIDGET_BOOL("Fire both props together", bBothProps)
			ADD_WIDGET_BOOL("Fire A at B", bFireAatB)
			ADD_WIDGET_BOOL("Fire B at A", bFireBatA)
			ADD_WIDGET_STRING("Triangle/Y on Pad 1 will also fire props")
			ADD_WIDGET_BOOL("Fire The Props", bThrowPropsTogether)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
// #ENDIF
ENDPROC
PROC FORCE_APPLIED()
			// If Only one prop exists, apply the force to fire it at the ground.
			IF DOES_ENTITY_EXIST(PROP_A)
			AND DOES_ENTITY_EXIST(PROP_B)
				
				IF bBothProps = TRUE
					bFireAatB = FALSE
					bFireBatA = FALSE
					vPropACurrent = GET_ENTITY_COORDS(PROP_A)
					vPropBCurrent = GET_ENTITY_COORDS(PROP_B)
					
				
					force_vec_ba = vPropBCurrent - vPropACurrent
					force_vec_ab = vPropACurrent - vPropBCurrent
					
					normalise_vector(force_vec_ba)
					normalise_vector(force_vec_ab)
				
					force_vec_ba.x *= force_multiplier
					force_vec_ba.y *= force_multiplier
					force_vec_ba.z *= force_multiplier
				
					force_vec_ab.x *= force_multiplier
					force_vec_ab.y *= force_multiplier
					force_vec_ab.z *= force_multiplier
					
					APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(PROP_B, APPLY_TYPE_IMPULSE, force_vec_ab, 0, FALSE, TRUE)	
					APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(PROP_A, APPLY_TYPE_IMPULSE, force_vec_ba, 0, FALSE, TRUE)
				ENDIF
				IF bFireAatB = TRUE
					bBothProps = FALSE
					bFireBatA = FALSE
					IF DOES_ENTITY_EXIST(PROP_A)
					AND DOES_ENTITY_EXIST(PROP_B)
						vPropACurrent = GET_ENTITY_COORDS(PROP_A)
						vPropBCurrent = GET_ENTITY_COORDS(PROP_B)
			
						force_vec_ab = vPropBCurrent - vPropACurrent

						normalise_vector(force_vec_ab)
			
						force_vec_ab.x *= force_multiplier
						force_vec_ab.y *= force_multiplier
						force_vec_ab.z *= force_multiplier
						
						FREEZE_ENTITY_POSITION(PROP_B, TRUE)
						APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(PROP_A, APPLY_TYPE_IMPULSE, force_vec_ab, 0, FALSE, TRUE)	
					ELSE
						bFireAatB = FALSE
					ENDIF
				ENDIF
				IF bFireBatA = TRUE
					bFireAatB = FALSE
					bBothProps = FALSE
					IF DOES_ENTITY_EXIST(PROP_B)
					AND DOES_ENTITY_EXIST(PROP_A)
							vPropACurrent = GET_ENTITY_COORDS(PROP_A)
							vPropBCurrent = GET_ENTITY_COORDS(PROP_B)
			
							force_vec_ba = vPropACurrent - vPropBCurrent

							normalise_vector(force_vec_ba)
			
							force_vec_ba.x *= force_multiplier
							force_vec_ba.y *= force_multiplier
							force_vec_ba.z *= force_multiplier
						
							FREEZE_ENTITY_POSITION(PROP_A, TRUE)
							APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(PROP_B, APPLY_TYPE_IMPULSE, force_vec_ba, 0, FALSE, TRUE)	
					ELSE
						bFireBatA = FALSE
					ENDIF
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(PROP_A)
				AND NOT DOES_ENTITY_EXIST(PROP_B)
					vPropACurrent = GET_ENTITY_COORDS(PROP_A)
					vADistance = GET_ENTITY_COORDS(PROP_A) - <<0, 0, (GET_ENTITY_HEIGHT_ABOVE_GROUND(PROP_A))>>
					force_vec_ab = vADistance - vPropACurrent
					normalise_vector(force_vec_ab)
					force_vec_ab.x *= force_multiplier
					force_vec_ab.y *= force_multiplier
					force_vec_ab.z *= force_multiplier
					APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(PROP_A, APPLY_TYPE_IMPULSE, force_vec_ab, 0, FALSE, TRUE)
				ENDIF
				IF DOES_ENTITY_EXIST(PROP_B)
				AND NOT DOES_ENTITY_EXIST(PROP_A)
					vPropBCurrent = GET_ENTITY_COORDS(PROP_B)
					vBDistance = GET_ENTITY_COORDS(PROP_B) - <<0, 0, (GET_ENTITY_HEIGHT_ABOVE_GROUND(PROP_B))>>
					force_vec_ba = vBDistance - vPropBCurrent
					normalise_vector(force_vec_ba)
					force_vec_ba.x *= force_multiplier
					force_vec_ba.y *= force_multiplier
					force_vec_ba.z *= force_multiplier
					APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(PROP_B, APPLY_TYPE_IMPULSE, force_vec_ba, 0, FALSE, TRUE)
				ENDIf	
			ENDIF
ENDPROC
SCRIPT 

	CREATE_PROP_DROP_WIDGETS()
	
	REGISTER_SCRIPT_WITH_AUDIO()
	
	WHILE TRUE
		WAIT(0)
		INVALIDATE_IDLE_CAM()
		
		IF bAmbience = TRUE
			IF NOT IS_AUDIO_SCENE_ACTIVE("MUTES_AMBIENCE_SCENE")
				START_AUDIO_SCENE("MUTES_AMBIENCE_SCENE")
			ENDIF
		ELSE STOP_AUDIO_SCENE("MUTES_AMBIENCE_SCENE")
		ENDIF

		// Calcultating prop spawn distances.
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		
			GET_ENTITY_MATRIX(PLAYER_PED_ID(), vFront, vSide, vUp, vPos)
		
//				vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), TRUE)
			
					vTransAPos = vPos - fADelta * vSide
					vTransBPos = vPos + fBDelta * vSide
		
						vADistance = vTransAPos + vFront * fADistance
						vBDistance = vTransBPos + vFront * fBDistance
				
							vPropALocation = vADistance + vUp * fPropAHeight
							vPropBLocation = vBDistance + vUp * fPropBHeight
		ENDIF				
		
		IF bPropACreated = TRUE
			CREATE_PROP_A()
			bPropACreated = FALSE
		ENDIF
		
		IF bPropBCreated = TRUE
			CREATE_PROP_B()
			bPropBCreated = FALSE
		ENDIF
		
		IF bClearProps = TRUE
			IF DOES_ENTITY_EXIST(PROP_A)
				DELETE_OBJECT(PROP_A)
			ENDIF
			IF DOES_ENTITY_EXIST(PROP_B)
				DELETE_OBJECT(PROP_B)
			ENDIF
			bClearProps = FALSE
		ENDIF
		
		IF bAutoReset = TRUE
			IF bThrowPropsTogether = TRUE
				FORCE_APPLIED()
				WAIT(iSeconds)
				IF DOES_ENTITY_EXIST(PROP_A)
					bPropACreated = TRUE
				ENDIF
				IF DOES_ENTITY_EXIST(PROP_B)
					bPropBCreated = TRUE
				ENDIF
				bThrowPropsTogether = FALSE
			ENDIF
		ENDIF
		
		IF bThrowPropsTogether = TRUE
			FORCE_APPLIED()
			bThrowPropsTogether = FALSE
		ENDIF
		
		IF IS_BUTTON_PRESSED(PAD1, TRIANGLE)
			bThrowPropsTogether = TRUE
		ENDIF
		
		SET_CLOCK_TIME(12, 00, 00)

		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			CLEANUP_SCRIPT()
		ENDIF
	ENDWHILE
ENDSCRIPT

#ENDIF	//	 IS_DEBUG_BUILD
