// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "script_xml.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "commands_streaming.sch"
USING "commands_graphics.sch"
USING "commands_cutscene.sch"
USING "commands_debug.sch"
USING "test_tools.sch"
USING "commands_vehicle.sch"
USING "commands_camera.sch"
USING "commands_player.sch"
USING "script_DEBUG.sch"
USING "commands_script.sch"

//Notes: This is script is to allow easy setup of test scenarios for the code department. It works in conjunction with test_tools.sch
//It  is a data driven system 

//Defaults values when the test is reset to factory the standard values are used

//Level menu 
STRING XMLMenu = "Testbed/TestHorseAiMenu"

//Ped Vars
CONST_INT MAX_NUMBER_OF_PEDS 			10

peds_struct TestPeds [MAX_NUMBER_OF_PEDS]

//Vehicle Vars
CONST_INT MAX_NUMBER_OF_VEHICLES 	 10

vehicle_struct TestVehicles[MAX_NUMBER_OF_VEHICLES]

//Camera Vars 
CONST_INT MAX_NUMBER_OF_CAMERAS			2
CONST_INT FixedCamera										0
CONST_INT TrackingCamera									1
StCameraData TestCams[MAX_NUMBER_OF_CAMERAS]

PROC CREATE_TEST_WIDGET ()
		START_WIDGET_GROUP("General Tests")
		SETUP_WIDGET ()	
			START_WIDGET_GROUP("Horse")
			STOP_WIDGET_GROUP ()	
		STOP_WIDGET_GROUP()
ENDPROC

// Functions end: WIDGET

//Mission flow House keeping


//PUPROSE: REmoves all the scenarios and resets all the relationships
	PROC Cleanup_Scenario_Entities ()
	int index = 0
		for index = 0 to MAX_NUMBER_OF_PEDS -1 	
			CLEAN_UP_PED_ENTITIES (TestPeds[index].ped)
		ENDFOR 
		
		for index = 0 to MAX_NUMBER_OF_VEHICLES -1
			CLEAN_UP_VEHICLE_ENTITIES (TestVehicles[index].Vehicle )
		ENDFOR

		Set_Gang_Relationships (FALSE)	
	ENDPROC

PROC Terminate_test_script ()
	if IS_KEYBOARD_KEY_JUST_PRESSED (KEY_S)
		CLEAR_PRINTS ()
		CLEAR_HELP ()
		Cleanup_Scenario_Entities ()
		Temp_cleanup_scenario_cams ()
		SET_PLAYER_COLISION(scplayer, true)
		IF NOT IS_PED_INJURED (scplayer)
				  SET_PED_COORDS_KEEP_VEHICLE(scplayer, GET_PLAYER_START_POS () )
		ENDIF
		
		TERMINATE_THIS_THREAD ()
		
	ENDIF
ENDPROC
//END: House Keeping

//Test Specific Functions:
	

//End Test Specific Functions

//These enums must match with the testNavmenu.xml



PROC INITALISE_TEST_STATE()
		SingleTaskStatus = startSingleTask
ENDPROC

ENUM Scenarios
	HorseTest,
	DefaultTest = 1000
ENDENUM


//  END_SCENARIO: SquadOfPedsMovingBetweenVehicles  
PROC SETUP_TEST_DATA ()
	SWITCH int_to_enum (scenarios, gcurrentselection)
		CASE HorseTest
		
			//fCarBootAngleRatio = 1.0
			
			//Create a vehicle 
			//TestVehicles [0].vehicleCoords = << 80.0012, 66.0009, 1.9393>> 
			//TestVehicles [0].vehicleHeading = 90.0001
			//TestVehicles [0].vehiclemodel = SANCHEZ			
			
			//Create a ped
			TestPeds[0].PedsCoords = << 3.0000 , 70.0000, 10.1959>> 
			TestPeds[0].PedHeading = 359.6439
			TestPeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = S_M_Y_COP_01
			TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
			
			//Create horse
			TestPeds[1].PedsCoords = << 0.0000 , 70.0000, 10.1959>> 
			TestPeds[1].PedHeading = 359.6439
			TestPeds[1].Pedrelgrp = 1
			//TestPeds[1].PedModel = A_C_HORSE
			TestPeds[1].PedsWeapon = WEAPONTYPE_UNARMED
			
			TestPeds[2].PedsCoords = << 0.0000 , 73.0000, 10.1959>> 
			TestPeds[2].PedHeading = 359.6439
			TestPeds[2].Pedrelgrp = 1
			//TestPeds[2].PedModel = A_C_HORSE
			TestPeds[2].PedsWeapon = WEAPONTYPE_UNARMED
		Break
	ENDSWITCH	
ENDPROC

PROC RUN_TEST ()
		BOOL testRunning = TRUE
		SWITCH int_to_enum (scenarios, gcurrentselection)		// the ENUM corresponds to the values from the XML file 
			CASE HorseTest
			
				//WHILE TRUE
				//	SET_PED_RESET_FLAG(TestPeds[0].Ped, PRF_InfiniteStamina, TRUE)
				//	WAIT(0)
				//ENDWHILE
				
				//SET_PED_ONTO_MOUNT(TestPeds[0].Ped, TestPeds[1].Ped)
				

				IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped)
					TASK_MOUNT_ANIMAL(TestPeds[0].Ped, TestPeds[1].Ped)
				ENDIF
				WHILE testRunning
					IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped)
						IF IS_PED_ON_MOUNT(TestPeds[0].Ped)
							PRINTLN("Mounted!")
						ELSE
							PRINTLN("Unmounted!")
						ENDIF
						IF IS_BUTTON_PRESSED(DEBUG_PAD, TRIANGLE) 
							testRunning=FALSE
							TASK_DISMOUNT_ANIMAL(TestPeds[0].Ped)
							//REMOVE_PED_FROM_MOUNT(TestPeds[0].Ped)
						ENDIF
					ENDIF
					WAIT(0)
				ENDWHILE
				
				Set_Test_State_To_Default()
					
			BREAK			
		ENDSWITCH	

ENDPROC


//Purpose: Runs the main scenarios
PROC Run_Test_Scenario ()
	int index = 0
	
	SWITCH TestScenarioAStatus
		//setps up all the data for the sceanrio, this is only called once or if selected in widget
		CASE InitialiseScenarioData
			PlayerStartPos = GET_PLAYER_START_POS()				//sets the players start coords at the default
			Bscenario_running = FALSE
			INITIALISE_PED_DATA (Testpeds)
			INITIALISE_VEHICLE_DATA (TestVehicles)
			INITIALISE_CAM_DATA(TestCams )
			SETUP_TEST_DATA ()
	
			HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
			TestScenarioAStatus = CreateScenarioEntities
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)		//sets the 
		
		BREAK
		
		// Creates all the scenario data
		CASE CreateScenarioEntities
			Bscenario_running = FALSE
			
			Set_Gang_Relationships (TRUE)	
		
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
		
			//peds
			FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
				CREATE_PED_ENTITY(Testpeds[index])
				GiveBlipsToPedsGrps (Testpeds[index])
				ALTER_COMBAT_STATS (Testpeds[index] )
				SWAP_PED_WEAPONS (Testpeds[index])
				ADD_RELATIONSHIP_GROUP_TO_STRUCT (Testpeds[index])
				Block_Peds_Temp_Events (Testpeds[index], TRUE)
				SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
			ENDFOR
			
			//TestVehicles
			FOR Index = 0 to MAX_NUMBER_OF_VEHICLES -1
				CREATE_VEHICLE_ENTITY (TestVehicles[index] )
			ENDFOR
		
			//TestCams
			for index = 0 to MAX_NUMBER_OF_CAMERAS -1
				CREATE_CAM_ENTITY (TestCams[index])
			ENDFOR
			
			TestScenarioAStatus = SetScenarioEntities
		BREAK
		
		CASE SetScenarioEntities
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
			
			Start_And_Reset_Test ()
			
			IF gBeginCombatScenario
				TestScenarioAStatus = RunScenario
				gBeginCombatScenario = FALSE
				gRun_debuggig = FALSE
				Bscenario_running = TRUE
				INITALISE_TEST_STATE()
				TEMP_ACTIVATE_CAMS (TestCams[FixedCamera].cam )
				
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam )
					HELP_TEXT_STATE =  HTF_SCENARIO_RUNNING_FIXED_CAM
				ELSE
					HELP_TEXT_STATE = HTF_STARTED_RUNNING_NO_CAMS
				ENDIF
			ENDIF
		BREAK
		
		//Runs the actual selected scenario
		
		CASE RunScenario
				
			Temp_Run_Scenario_Tracking_Cam (TestPeds, MAX_NUMBER_OF_PEDS, TestCams[TrackingCamera].cam) 
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
				
			Start_And_Reset_Test ()
				
		 	Check_For_Scenario_Reset ()
			
			IF DOES_CAM_EXIST (TestCams[TrackingCamera].cam)
				if IS_CAM_RENDERING (TestCams[TrackingCamera].cam)
					HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_TRACKING_CAM
				ELSE
					HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_FIXED_CAM
				ENDIF
			ENDIF
					
			RUN_TEST ()		//run the main tests 		
			
		BREAK 
				
		CASE CleanupScenario
			
			HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
		
			Cleanup_Scenario_Entities ()
			
			IF gResetToDefault 	
				Temp_cleanup_scenario_cams ()		//here we are changing scenarios so we need to reset cams
				TestScenarioAStatus = InitialiseScenarioData
				gResetToDefault = FALSE
			ENDIF
		
			IF gResetCombatScenario
				Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)
				gcurrentselection = gSelection 
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
					ACTIVATE_CAM (TestCams[FixedCamera].cam)
				ELSE
					Temp_cleanup_scenario_cams ()	
				ENDIF
				TestScenarioAStatus = CreateScenarioEntities
				gResetCombatScenario = FALSE
			ENDIF
		
		BREAK
		
	ENDSWITCH
ENDPROC

SCRIPT
	
	SET_DEBUG_ACTIVE (TRUE)
	
	//gvMapOffset = GET_PLAYER_START_POS ()
	
	SETUP_MISSION_XML_MENU (XMLMenu, KEY_Q )	//defined at the top of the file
	
	SETUP_AREA_FOR_MISSION (<<0.0, 0.0, 0.0>>)
	
	//Gets a reference to the player
	Get_The_Player ()
	
	//set player collios
	SET_PLAYER_COLISION(scplayer, true)
	
	//Sets the test widget from the test tools
	CREATE_TEST_WIDGET ()
	
	//request the test anim bank
	//REQUEST_TEST_ANIM_DICT ("FELTZER2010")

	WHILE TRUE
			
		// controls the help text hides if xml menu is active
		TEXT_CONTROLLER ()
	
		// Can set all scenario peds invincible from the widget
		Set_Scenario_Peds_Invincible (TestPeds, MAX_NUMBER_OF_PEDS, gsetpedsinvincible )
	
		//User can create a debug cam for setting sceanrios
		Temp_Create_Debug_Cam (TestCams[FixedCamera]) 
		
		//Runs the selected option from the XML menu 
		Run_Selection_From_XML_input ()
		
		//Checks that a valid selection has been input and runs the scenario
		IF (gcurrentselection <> InvalidSelection)
	
			Draw_Debug_Info (  )
			
			//Sets the test scenario into debug mode
			//IF (gAllowDebugging)
			//	Set_To_Debug ()
			//ENDIF
			
			//if (gRun_debuggig)
			//	Temp_Debug_Scenario (TestPeds, TestVehicles, Route, TestCams[FixedCamera].cam, S_M_Y_COP_01)								//Allows the entities in the scneario to be adjusted
			//	Print_Scenario_Data (TestPeds, TestVehicles, Route, TestCams)
			//ENDIF
			
			Run_Test_Scenario ()								
			
			SWITCH_BETWEEN_FIXED_AND_TRACKING (TestCams[FixedCamera].cam)
			
			PRINT_ACTIVE_TEST (Bscenario_running, gRun_debuggig)		
		ENDIF
		
		Terminate_test_script ()
		
		WAIT (0)
	
	
	ENDWHILE

ENDSCRIPT


#ENDIF	// IS_DEBUG_BUILD

