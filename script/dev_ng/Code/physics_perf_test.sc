// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_fire.sch"
USING "commands_object.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "script_player.sch"
USING "commands_xml.sch"
USING "commands_physics.sch"

VECTOR vStartPos = << 180, -10, 9.65>>
VECTOR vWidgetStartPos = vStartPos
FLOAT fInitialPlayerHeading = 180
FLOAT fWidgetInitialHeading = fInitialPlayerHeading
VECTOR vInitialPlayerCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<-7.5, -10, 0>>)
BOOL bSetNewStartPos
BOOL bForceCleanup

FLOAT SimUpdateTime, PreCollideTime, CollideTime, CollideJobsTime, PostCollideTime
INT frameNumber = 1000, numberOfTestFrames = 1000
BOOL bAutoTestRun, bToggleAutoTest

CONST_INT arraySize 32

OBJECT_INDEX prop[arraySize][arraySize]


VEHICLE_INDEX veh[arraySize][arraySize]

VEHICLE_INDEX vehRam
//VECTOR vehicleVelocity = <<0,-25,0>>
FLOAT offsetVehicleDist = 25
FLOAT vehicleSpeed = 25

PED_INDEX ped[arraySize][arraySize]

//PED_INDEX pedDriver

INT iTestSize = 5
INT iTestSizeMax = 16
INT iStackSize = 3
INT iStackSizeMax = 16

FLOAT tempZ = 9.7

TEXT_WIDGET_ID pedModelWidget, vehModelWidget, propModelWidget

MODEL_NAMES pedModel
MODEL_NAMES vehModel
MODEL_NAMES propModel

BOOL bClearCustomOptions

REL_GROUP_HASH relGroup[arraySize]
TEXT_LABEL sRelGroup[arraySize]


ENUM PHYSICS_TEST_TYPE
	PPT_NONE=0,
	
	PPT_BOXES_NOT_TOUCHING, 		// 1. A bunch of boxes all laying out in a field on the ground, not touching each other
	PPT_BOXES_RANDOM_PILE,			// 2. A bunch of boxes in a disorganized pile
	PPT_BOXES_STACKED,				// 3. A bunch of boxes in a stack (round towers are nice and stable, or a pyramid shape)
	
	PPT_NM_GROUP_NOT_TOUCHING,		// 4. A field of ragdolls falling on the ground but not touching each other. Create a bunch of peds and make them collapse through NM
	PPT_NM_GROUP_RANDOM_PILE, 		// 5. A dog-pile of ragdolls, mostly all touching each other. Create a bunch of peds above the ground enough such that they go into NM.
	
	PPT_VEH_NOT_TOUCHING,			// 6. A field of vehicles all touching the ground but not each other
	PPT_VEH_RANDOM_PILE,			// 7. A field of vehicles in a pile.
	
	PPT_PED_GROUP_STANDING,			// 8. A bunch of characters standing around (potentially with character cloth)
	PPT_PED_GROUP_SHOOTING,			// 9. A bunch of invulnerable characters shooting at each other
	
	PPT_PROPS_NOT_TOUCHING,			// 10. A bunch of props not touching each other 
//	PPT_PROPS_CLOTH_NOT_TOUCHING,	// 11. A bunch of props with cloth not touching each other
	
	PPT_FRAG_PROPS_W_VEH_CRASH, 	// 12. A field of fragment props with a car crashing through it
	PPT_FRAG_PROPS_W_EXP,			// 13. A field of fragment props with an explosion going off in the middle
	
	PPT_VEH_W_VEH_CRASH,			// 14. A field of cars with a car crashing through it
	PPT_VEH_W_EXP,					// 15. A field of cars with an explosion going off in the middle
	
	PPT_PED_GROUP_W_VEH_CRASH,		// 16. A field of peds with a car crashing through them
	PPT_PED_GROUP_W_EXP,			// 17. A field of peds with an explosion going off in the middle
	
	PPT_MIXED_GROUP,
	
	PPT_PED_TABLE_GROUP_W_VEH_CRASH,
	
	PPT_END
ENDENUM

INT iSelectedTest = (ENUM_TO_INT(PPT_NONE))
BOOL bRunSelectedTest = FALSE

// -----------------------------------------------------------------------------------------------------------

PROC Create_The_Player()

	//IF GET_COMMANDLINE_PARAM_EXISTS("sc_physics_perf_test")
	IF NOT ARE_VECTORS_ALMOST_EQUAL(vStartPos, <<0,0,0>>)
		tempZ = vStartPos.z
		vWidgetStartPos = vStartPos
		fWidgetInitialHeading = fInitialPlayerHeading
		vInitialPlayerCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<-7.5, -10, 0>>)
	ELSE
		vStartPos = << 180, -25, 9.65>>//<< 180, -10, 9.65>>
		tempZ = vStartPos.z
		vWidgetStartPos = vStartPos
		fInitialPlayerHeading = 180
		fWidgetInitialHeading = fInitialPlayerHeading
		vInitialPlayerCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<-7.5, -10, 0>>)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_ENTITY_HEADING(PLAYER_PED_ID(), fInitialPlayerHeading)
    	SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vInitialPlayerCoord)
	ENDIF
    LOAD_SCENE(vInitialPlayerCoord)
	SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")

	SET_NUMBER_OF_PARKED_VEHICLES(0)

	SET_VEHICLE_POPULATION_BUDGET(0)
	SET_GARBAGE_TRUCKS(FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-9999.9, -9999.9, -9999.9>>, <<9999.9, 9999.9, 9999.9>>, FALSE)
	
	CLEAR_AREA_OF_VEHICLES(vInitialPlayerCoord, 1000.0)
	CLEAR_AREA_OF_PEDS(vInitialPlayerCoord, 1000.0)
	
	SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
    
    PHYSICS_DEBUG_SET_SLEEP_ENABLED(false)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	DO_SCREEN_FADE_IN(0)
    
ENDPROC

PROC Cleanup_Previous_Performance_Test()
	INT i
	INT ii
	REPEAT arraySize i
		REPEAT arraySize ii
			IF DOES_ENTITY_EXIST(prop[i][ii])
				DELETE_OBJECT(prop[i][ii])
				SET_MODEL_AS_NO_LONGER_NEEDED(propModel)
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	REPEAT arraySize i
		REPEAT arraySize ii
			IF DOES_ENTITY_EXIST(ped[i][ii])
				DELETE_PED(ped[i][ii])
				SET_MODEL_AS_NO_LONGER_NEEDED(propModel)
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	REPEAT arraySize i
		REPEAT arraySize ii
			IF DOES_ENTITY_EXIST(veh[i][ii])
				DELETE_VEHICLE(veh[i][ii])
				SET_MODEL_AS_NO_LONGER_NEEDED(propModel)
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	
	REPEAT arraySize i	
		REMOVE_RELATIONSHIP_GROUP(relGroup[i])
	ENDREPEAT
	
	
//	IF DOES_ENTITY_EXIST(pedDriver)
//		DELETE_PED(pedDriver)
//		SET_MODEL_AS_NO_LONGER_NEEDED(pedModel)
//	ENDIF
	
	IF DOES_ENTITY_EXIST(vehRam)
		DELETE_VEHICLE(vehRam)
		SET_MODEL_AS_NO_LONGER_NEEDED(vehModel)
	ENDIF
	REMOVE_DECALS_IN_RANGE(vStartPos, 150)
	CLEAR_AREA(vStartPos, 150, TRUE)
ENDPROC

PROC Set_Creation_Defaults()
	pedModel = S_M_Y_CLOWN_01
	vehModel = SENTINEL
	propModel = PROP_BOX_WOOD01A
	
	iTestSize = 5
	
	SET_CONTENTS_OF_TEXT_WIDGET(pedModelWidget, "New text widget")
	SET_CONTENTS_OF_TEXT_WIDGET(vehModelWidget, "New text widget")
	SET_CONTENTS_OF_TEXT_WIDGET(propModelWidget, "New text widget")
ENDPROC

PROC Run_Physics_Performance_Test()
	INT i, ii, iBoxes, evenNumberTest
	VECTOR vMin, vMax
	VECTOR vTemp, vHeading
	FLOAT fRadius, fBoxes, createAngle, createAngleHalved, fHeading, fTemp
	FLOAT fMiddle = TO_FLOAT(iTestSize)/2
	MODEL_NAMES chairModel = PROP_TABLE_02_CHR
	MODEL_NAMES tableModel = PROP_TABLE_02
	
	frameNumber = 0

	START_PHYSICS_PROFILE_CAPTURE()
	
	SWITCH INT_TO_ENUM(PHYSICS_TEST_TYPE, iSelectedTest)
		CASE PPT_NONE
			SCRIPT_ASSERT("PHYSICS_TEST_TYPE = PPT_NONE. You should select a test type.")
		BREAK
		
		CASE PPT_BOXES_NOT_TOUCHING 
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_BOXES_NOT_TOUCHING", 10000, 1)
			
			REQUEST_MODEL(propModel)
			WHILE NOT HAS_MODEL_LOADED(propModel)
			WAIT(0)
			ENDWHILE
			
			GET_MODEL_DIMENSIONS(propModel, vMin, vMax)
			GET_GROUND_Z_FOR_3D_COORD(vStartPos, fTemp)
			REPEAT iTestSize i
				REPEAT iTestSize ii
					prop[i][ii] = CREATE_OBJECT_NO_OFFSET(propModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<vStartPos.x, vStartPos.y, fTemp>>, fInitialPlayerHeading, <<(3*vMax.x)*i, (3*vMax.y)*ii, 0>>))
					SET_ENTITY_DYNAMIC(prop[i][ii], TRUE)
				ENDREPEAT
			ENDREPEAT
			
		BREAK
		
		CASE PPT_BOXES_RANDOM_PILE	
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_BOXES_RANDOM_PILE", 10000, 1)
			
			REQUEST_MODEL(propModel)
			WHILE NOT HAS_MODEL_LOADED(propModel)
			WAIT(0)
			ENDWHILE
			
			GET_MODEL_DIMENSIONS(propModel, vMin, vMax)
			GET_GROUND_Z_FOR_3D_COORD(vStartPos, fTemp)
			REPEAT iTestSize i
				REPEAT iTestSize ii
					prop[i][ii] = CREATE_OBJECT_NO_OFFSET(propModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<vStartPos.x, vStartPos.y, fTemp>>, GET_RANDOM_FLOAT_IN_RANGE(0,360), <<0, vMax.y*i, (vMax.z*ii)>>))
					SET_ENTITY_DYNAMIC(prop[i][ii], TRUE)
				ENDREPEAT
			ENDREPEAT
			
		BREAK
		CASE PPT_BOXES_STACKED	
			
			
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_BOXES_STACKED", 10000, 1)
			
			REQUEST_MODEL(propModel)
			WHILE NOT HAS_MODEL_LOADED(propModel)
			WAIT(0)
			ENDWHILE
			
			IF iTestSize < 4
				iTestSize = 4
			ENDIF
			
			iBoxes = iTestSize
			fBoxes = TO_FLOAT(iBoxes)
			
			createAngle = 360/fBoxes
			createAngleHalved = 180/fBoxes	
			
			GET_MODEL_DIMENSIONS(propModel, vMin, vMax)
			fRadius = COS(createAngleHalved)*(vMax.x/SIN(createAngleHalved))
			fRadius += (vMax.y)
			
			PRINTSTRING("Adjacent = ")
			PRINTFLOAT(fRadius)
			PRINTNL()
			
			PRINTSTRING("Hypotenuse = ")
			PRINTFLOAT((vMax.y)*SIN(createAngleHalved))
			PRINTNL()
			
			PRINTSTRING("vMin.x = ")
			PRINTFLOAT(vMin.x)
			PRINTNL()
			
			PRINTSTRING("vMax.y = ")
			PRINTFLOAT(vMax.x)
			PRINTNL()
			
			PRINTSTRING("vMin.y = ")
			PRINTFLOAT(vMin.y)
			PRINTNL()
			
			PRINTSTRING("vMax.y = ")
			PRINTFLOAT(vMax.y)
			PRINTNL()
			
			PRINTSTRING("fRadius = ")
			PRINTFLOAT(fRadius)
			PRINTNL()
			GET_GROUND_Z_FOR_3D_COORD(vStartPos, fTemp)
			REPEAT iStackSize ii
				REPEAT iTestSize i
					evenNumberTest = ii / 2
					IF evenNumberTest * 2 = ii
						vTemp.x = vStartPos.x+(fRadius*SIN(TO_FLOAT(i)*createAngle))
						vTemp.y = vStartPos.y+(fRadius*COS(TO_FLOAT(i)*createAngle))
					ELSE
						vTemp.x = vStartPos.x+(fRadius*SIN((TO_FLOAT(i)*createAngle)-createAngleHalved))
						vTemp.y = vStartPos.y+(fRadius*COS((TO_FLOAT(i)*createAngle)-createAngleHalved))
					ENDIF
					vTemp.z = (fTemp+(vMax.z*ii))
					prop[i][ii] = CREATE_OBJECT_NO_OFFSET(propModel, vTemp)
					vTemp -= vStartPos
					SET_ENTITY_HEADING(prop[i][ii], GET_HEADING_FROM_VECTOR_2D(vTemp.x, vTemp.y))
					SET_ENTITY_DYNAMIC(prop[i][ii], TRUE)
					PRINTSTRING("Created prop number ")
					PRINTINT(i)
					PRINTSTRING(" - ")
					PRINTINT(ii)
					PRINTNL()
				ENDREPEAT
			ENDREPEAT
		BREAK 
		CASE PPT_NM_GROUP_NOT_TOUCHING		
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_NM_GROUP_NOT_TOUCHING", 10000, 1)
			
			REQUEST_MODEL(pedModel)
			WHILE NOT HAS_MODEL_LOADED(pedModel)
			WAIT(0)
			ENDWHILE
			
			GET_MODEL_DIMENSIONS(pedModel, vMin, vMax)
			REPEAT iTestSize i
				REPEAT iTestSize ii
					ped[i][ii] = CREATE_PED(PEDTYPE_CIVMALE, pedModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(10*vMax.x)*i, (10*vMax.y)*ii, 4>>))
					SET_PED_TO_RAGDOLL(ped[i][ii], 0, 10000, TASK_RELAX, FALSE, FALSE)
					SET_ENTITY_HEALTH(ped[i][ii], 0)
				ENDREPEAT
			ENDREPEAT
		BREAK
		CASE PPT_NM_GROUP_RANDOM_PILE 	
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_NM_GROUP_RANDOM_PILE", 10000, 1)
			
			REQUEST_MODEL(pedModel)
			WHILE NOT HAS_MODEL_LOADED(pedModel)
			WAIT(0)
			ENDWHILE
			
			GET_MODEL_DIMENSIONS(pedModel, vMin, vMax)
			REPEAT iTestSize i
				REPEAT iTestSize ii
					ped[i][ii] = CREATE_PED(PEDTYPE_CIVMALE, pedModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<0, (vMax.y/i), ((vMax.z*ii)*2)>>))
					SET_ENTITY_HEALTH(ped[i][ii], 0)
				ENDREPEAT
			ENDREPEAT
		BREAK
		
		CASE PPT_VEH_NOT_TOUCHING	
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_VEH_NOT_TOUCHING", 10000, 1)
			
			REQUEST_MODEL(vehModel)
			WHILE NOT HAS_MODEL_LOADED(vehModel)
			WAIT(0)
			ENDWHILE
			GET_MODEL_DIMENSIONS(vehModel, vMin, vMax)
			REPEAT iTestSize i
			REPEAT iTestSize ii
				veh[i][ii] = CREATE_VEHICLE(vehModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(3*vMax.x)*i, (3*vMax.y)*ii, 0>>), fInitialPlayerHeading)
			ENDREPEAT
			ENDREPEAT
		BREAK
		
		CASE PPT_VEH_RANDOM_PILE	
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_VEH_RANDOM_PILE", 10000, 1)
			
			REQUEST_MODEL(vehModel)
			WHILE NOT HAS_MODEL_LOADED(vehModel)
			WAIT(0)
			ENDWHILE
			GET_MODEL_DIMENSIONS(vehModel, vMin, vMax)
			REPEAT iTestSize i
				REPEAT iTestSize ii
					veh[i][ii] = CREATE_VEHICLE(vehModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<0, (3*vMax.y)*i, ((3*vMax.z)*(ii+1))>>), fInitialPlayerHeading)
					//SET_ENTITY_COORDS(veh[i][ii], GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, GET_RANDOM_FLOAT_IN_RANGE(0,360), <<0, vMax.y/i, ((3*vMax.z)*ii)>>))
					SET_ENTITY_DYNAMIC(veh[i][ii], TRUE)
					SET_ENTITY_PROOFS(veh[i][ii], TRUE, TRUE, TRUE, TRUE, TRUE)
				ENDREPEAT
			ENDREPEAT
		BREAK
		
		CASE PPT_PED_GROUP_STANDING	
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_PED_GROUP_STANDING", 10000, 1)
			
			REQUEST_MODEL(pedModel)
			WHILE NOT HAS_MODEL_LOADED(pedModel)
			WAIT(0)
			ENDWHILE
			
			GET_MODEL_DIMENSIONS(pedModel, vMin, vMax)
			REPEAT iTestSize i
				REPEAT iTestSize ii
					ped[i][ii] = CREATE_PED(PEDTYPE_CIVMALE, pedModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(3*vMax.x)*i, (3*vMax.y)*ii, 0>>))
				ENDREPEAT
			ENDREPEAT
		BREAK
		
		CASE PPT_PED_GROUP_SHOOTING		
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_PED_GROUP_SHOOTING", 10000, 1)
			
			REQUEST_MODEL(pedModel)
			WHILE NOT HAS_MODEL_LOADED(pedModel)
			WAIT(0)
			ENDWHILE
			
			REPEAT iTestSize i
				sRelGroup[i] = "relgroup["
				sRelGroup[i] += i
				ADD_RELATIONSHIP_GROUP(sRelGroup[i], relGroup[i])
			ENDREPEAT
			
			REPEAT iTestSize i
				REPEAT iTestSize ii
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroup[ii], relGroup[i])
				ENDREPEAT
			ENDREPEAT
			
			iBoxes = iTestSize
			fBoxes = TO_FLOAT(iBoxes)
			
			createAngle = 360/fBoxes
			createAngleHalved = 180/fBoxes	
			
			GET_MODEL_DIMENSIONS(pedModel, vMin, vMax)
			fRadius = COS(createAngleHalved)*(vMax.x/SIN(createAngleHalved))
			fRadius += (vMax.y+2)
			
			REPEAT iTestSize i
				vTemp.x = vStartPos.x+(fRadius*SIN(TO_FLOAT(i)*createAngle))
				vTemp.y = vStartPos.y+(fRadius*COS(TO_FLOAT(i)*createAngle))
				vTemp.z = tempZ
				ped[0][i] = CREATE_PED(PEDTYPE_CIVMALE, pedModel, vTemp)
				vTemp -= vStartPos
				SET_ENTITY_HEADING(ped[0][i], GET_HEADING_FROM_VECTOR_2D(vTemp.x, vTemp.y))
				SET_PED_RELATIONSHIP_GROUP_HASH(ped[0][i], relGroup[i])
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(ped[0][i], 50)
				SET_ENTITY_INVINCIBLE(ped[0][i], TRUE)
				GIVE_WEAPON_TO_PED(ped[0][i], WEAPONTYPE_PISTOL, 999, TRUE)
				vTemp = GET_ENTITY_COORDS(ped[0][i])
				GET_GROUND_Z_FOR_3D_COORD(vTemp, vTemp.z)
				SET_ENTITY_COORDS(ped[0][i], vTemp)
			ENDREPEAT
		BREAK
		
		CASE PPT_PROPS_NOT_TOUCHING	
			propModel = PROP_FORSALEJR1
			
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_PROPS_NOT_TOUCHING", 10000, 1)
			
			REQUEST_MODEL(propModel)

			WHILE NOT HAS_MODEL_LOADED(propModel)
			WAIT(0)
			ENDWHILE
			GET_GROUND_Z_FOR_3D_COORD(vStartPos, fTemp)
			GET_MODEL_DIMENSIONS(propModel, vMin, vMax)
			REPEAT iTestSize i
				REPEAT iTestSize ii
					prop[i][ii] = CREATE_OBJECT_NO_OFFSET(propModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<vStartPos.x, vStartPos.y, fTemp>>, fInitialPlayerHeading, <<(2*vMax.x)*i, (2*vMax.y)*ii, 0>>))
				ENDREPEAT
			ENDREPEAT
		BREAK
		
//		CASE PPT_PROPS_CLOTH_NOT_TOUCHING	

		CASE PPT_FRAG_PROPS_W_VEH_CRASH 
			propModel = PROP_FORSALEJR1
			
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_FRAG_PROPS_W_VEH_CRASH", 10000, 1)
			
			REQUEST_MODEL(propModel)
			REQUEST_MODEL(pedModel)
			REQUEST_MODEL(vehModel)
			WHILE NOT HAS_MODEL_LOADED(propModel)
			OR NOT HAS_MODEL_LOADED(pedModel)
			OR NOT HAS_MODEL_LOADED(vehModel)
			WAIT(0)
			ENDWHILE
			GET_GROUND_Z_FOR_3D_COORD(vStartPos, fTemp)
			GET_MODEL_DIMENSIONS(propModel, vMin, vMax)
			REPEAT iTestSize i
				REPEAT iTestSize ii
					prop[i][ii] = CREATE_OBJECT_NO_OFFSET(propModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<vStartPos.x, vStartPos.y, fTemp>>, fInitialPlayerHeading, <<(2*vMax.x)*i, (2*vMax.y)*ii, 0>>))
				ENDREPEAT
			ENDREPEAT
			
			vehRam = CREATE_VEHICLE(vehModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(2*vMax.x)*(fMiddle), -offsetVehicleDist, 0>>), fInitialPlayerHeading)
//			pedDriver = CREATE_PED_INSIDE_VEHICLE(vehRam, PEDTYPE_CIVMALE, pedModel)
//			TASK_VEHICLE_DRIVE_TO_COORD(pedDriver, vehRam, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(2*vMax.x)*(fMiddle), (2*vMax.y)*(fMiddle), 2>>), 60, DRIVINGSTYLE_STRAIGHTLINE, vehModel, DRIVINGMODE_PLOUGHTHROUGH, 0.1, 100)
			//SET_ENTITY_VELOCITY(vehRam, vehicleVelocity)
			SET_VEHICLE_FORWARD_SPEED(vehRam, vehicleSpeed)
		BREAK
		CASE PPT_FRAG_PROPS_W_EXP	
			propModel = PROP_FORSALEJR1
			
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_FRAG_PROPS_W_EXP", 10000, 1)
			
			REQUEST_MODEL(propModel)

			WHILE NOT HAS_MODEL_LOADED(propModel)
			WAIT(0)
			ENDWHILE
			GET_GROUND_Z_FOR_3D_COORD(vStartPos, fTemp)
			GET_MODEL_DIMENSIONS(propModel, vMin, vMax)
			REPEAT iTestSize i
				REPEAT iTestSize ii
					prop[i][ii] = CREATE_OBJECT_NO_OFFSET(propModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<vStartPos.x, vStartPos.y, fTemp>>, fInitialPlayerHeading, <<(2*vMax.x)*i, (2*vMax.y)*ii, 0>>))
					SET_ENTITY_DYNAMIC(prop[i][ii], TRUE)
				ENDREPEAT
			ENDREPEAT
			WAIT(0)
			ADD_EXPLOSION(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(2*vMax.x)*(fMiddle), (2*vMax.y)*(fMiddle), 2>>),  EXP_TAG_PETROL_PUMP, 1)
		BREAK
		
		CASE PPT_VEH_W_VEH_CRASH
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_VEH_W_VEH_CRASH", 10000, 1)
			
			REQUEST_MODEL(pedModel)
			REQUEST_MODEL(vehModel)
			WHILE NOT HAS_MODEL_LOADED(pedModel)
			OR NOT HAS_MODEL_LOADED(vehModel)
			WAIT(0)
			ENDWHILE
			
			GET_MODEL_DIMENSIONS(vehModel, vMin, vMax)
			REPEAT iTestSize i
				REPEAT iTestSize ii
					veh[i][ii] = CREATE_VEHICLE(vehModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(3*vMax.x)*i, (3*vMax.y)*ii, 0>>), fInitialPlayerHeading)
				ENDREPEAT
			ENDREPEAT
			
			vehRam = CREATE_VEHICLE(vehModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(2*vMax.x)*(fMiddle), -offsetVehicleDist, 0>>), fInitialPlayerHeading)
//			pedDriver = CREATE_PED_INSIDE_VEHICLE(vehRam, PEDTYPE_CIVMALE, pedModel)
//			TASK_VEHICLE_DRIVE_TO_COORD(pedDriver, vehRam, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(3*vMax.x)*fMiddle, (3*vMax.y)*fMiddle, 0>>), 60, DRIVINGSTYLE_STRAIGHTLINE, vehModel, DRIVINGMODE_PLOUGHTHROUGH, 0.1, 100)
			//SET_ENTITY_VELOCITY(vehRam, vehicleVelocity)
			SET_VEHICLE_FORWARD_SPEED(vehRam, vehicleSpeed)
		BREAK

		CASE PPT_VEH_W_EXP	
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_VEH_W_EXP", 10000, 1)
			
			REQUEST_MODEL(vehModel)
			WHILE NOT HAS_MODEL_LOADED(vehModel)
			WAIT(0)
			ENDWHILE
			
			GET_MODEL_DIMENSIONS(vehModel, vMin, vMax)
			REPEAT iTestSize i
				REPEAT iTestSize ii
					veh[i][ii] = CREATE_VEHICLE(vehModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(3*vMax.x)*i, (3*vMax.y)*ii, 0>>), fInitialPlayerHeading)
				ENDREPEAT
			ENDREPEAT
			ADD_EXPLOSION(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(2*vMax.x)*(fMiddle), (3*vMax.y)*(fMiddle), 1>>),  EXP_TAG_PETROL_PUMP, 1)//((1/TO_FLOAT(iTestSizeMax))*TO_FLOAT(iTestSize)))
		BREAK
		
		CASE PPT_PED_GROUP_W_VEH_CRASH	
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_PED_GROUP_W_VEH_CRASH", 10000, 1)
			
			REQUEST_MODEL(pedModel)
			REQUEST_MODEL(vehModel)
			WHILE NOT HAS_MODEL_LOADED(pedModel)
			OR NOT HAS_MODEL_LOADED(vehModel)
			WAIT(0)
			ENDWHILE
			
			GET_MODEL_DIMENSIONS(pedModel, vMin, vMax)
			REPEAT iTestSize i
				REPEAT iTestSize ii
					ped[i][ii] = CREATE_PED(PEDTYPE_CIVMALE, pedModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(3*vMax.x)*i, (3*vMax.y)*ii, 0>>))
				ENDREPEAT
			ENDREPEAT
			
			vehRam = CREATE_VEHICLE(vehModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(2*vMax.x)*(fMiddle), -offsetVehicleDist, 0>>), fInitialPlayerHeading)
//			pedDriver = CREATE_PED_INSIDE_VEHICLE(vehRam, PEDTYPE_CIVMALE, pedModel)
			//TASK_VEHICLE_DRIVE_TO_COORD(pedDriver, vehRam, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(3*vMax.x)*(fMiddle), (3*vMax.y)*(fMiddle), 1>>), 60, DRIVINGSTYLE_STRAIGHTLINE, vehModel, DRIVINGMODE_PLOUGHTHROUGH, 0.1, 100)
			//SET_ENTITY_VELOCITY(vehRam, vehicleVelocity)
			SET_VEHICLE_FORWARD_SPEED(vehRam, vehicleSpeed)
		BREAK
		
		CASE PPT_PED_GROUP_W_EXP
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_PED_GROUP_W_EXP", 10000, 1)
			
			REQUEST_MODEL(pedModel)
			WHILE NOT HAS_MODEL_LOADED(pedModel)
			WAIT(0)
			ENDWHILE
			
			GET_MODEL_DIMENSIONS(pedModel, vMin, vMax)
			REPEAT iTestSize i
				REPEAT iTestSize ii
					ped[i][ii] = CREATE_PED(PEDTYPE_CIVMALE, pedModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(3*vMax.y)*i, (3*vMax.y)*ii, 0>>))
				ENDREPEAT
			ENDREPEAT
			WAIT(0)
			ADD_EXPLOSION(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(3*vMax.x)*(fMiddle), (3*vMax.y)*(fMiddle), 1>>),  EXP_TAG_PETROL_PUMP, 1)//((1/TO_FLOAT(iTestSizeMax))*TO_FLOAT(iTestSize)))
		BREAK
		
		CASE PPT_MIXED_GROUP
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_MIXED_GROUP", 10000, 1)
			
			REQUEST_MODEL(pedModel)
			REQUEST_MODEL(vehModel)
			REQUEST_MODEL(propModel)
			WHILE NOT HAS_MODEL_LOADED(pedModel)
			OR NOT HAS_MODEL_LOADED(vehModel)
			OR NOT HAS_MODEL_LOADED(propModel)
			WAIT(0)
			ENDWHILE
			
			IF iTestSize < 4
				iTestSize = 4
			ENDIF
			
			iBoxes = iTestSize
			fBoxes = TO_FLOAT(iBoxes)
			
			createAngle = 360/fBoxes
			createAngleHalved = 180/fBoxes	
			
			GET_MODEL_DIMENSIONS(propModel, vMin, vMax)
			fRadius = COS(createAngleHalved)*(vMax.x/SIN(createAngleHalved))
			fRadius += (vMax.y)
			
			PRINTSTRING("Adjacent = ")
			PRINTFLOAT(fRadius)
			PRINTNL()
			
			PRINTSTRING("Hypotenuse = ")
			PRINTFLOAT((vMax.y)*SIN(createAngleHalved))
			PRINTNL()
			
			PRINTSTRING("vMin.x = ")
			PRINTFLOAT(vMin.x)
			PRINTNL()
			
			PRINTSTRING("vMax.y = ")
			PRINTFLOAT(vMax.x)
			PRINTNL()
			
			PRINTSTRING("vMin.y = ")
			PRINTFLOAT(vMin.y)
			PRINTNL()
			
			PRINTSTRING("vMax.y = ")
			PRINTFLOAT(vMax.y)
			PRINTNL()
			
			PRINTSTRING("fRadius = ")
			PRINTFLOAT(fRadius)
			PRINTNL()
			
			REPEAT iStackSize ii
				REPEAT iTestSize i
					evenNumberTest = ii / 2
					IF evenNumberTest * 2 = ii
						vTemp.x = vStartPos.x+(fRadius*SIN(TO_FLOAT(i)*createAngle))
						vTemp.y = vStartPos.y+(fRadius*COS(TO_FLOAT(i)*createAngle))
					ELSE
						vTemp.x = vStartPos.x+(fRadius*SIN((TO_FLOAT(i)*createAngle)-createAngleHalved))
						vTemp.y = vStartPos.y+(fRadius*COS((TO_FLOAT(i)*createAngle)-createAngleHalved))
					ENDIF
					vTemp.z = (tempZ+(vMax.z*ii))
					prop[i][ii] = CREATE_OBJECT_NO_OFFSET(propModel, vTemp)
					vTemp -= vStartPos
					SET_ENTITY_HEADING(prop[i][ii], GET_HEADING_FROM_VECTOR_2D(vTemp.x, vTemp.y))
					SET_ENTITY_DYNAMIC(prop[i][ii], TRUE)
					PRINTSTRING("Created prop number ")
					PRINTINT(i)
					PRINTSTRING(" - ")
					PRINTINT(ii)
					PRINTNL()
				ENDREPEAT
			ENDREPEAT
			
			GET_MODEL_DIMENSIONS(pedModel, vMin, vMax)
			REPEAT iTestSize i
				REPEAT iTestSize ii
					ped[i][ii] = CREATE_PED(PEDTYPE_CIVMALE, pedModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(10*vMax.x)*i, (10*vMax.y)*ii, 4>>))
					SET_PED_TO_RAGDOLL(ped[i][ii], 0, 10000, TASK_RELAX, FALSE, FALSE)
					SET_ENTITY_HEALTH(ped[i][ii], 0)
				ENDREPEAT
			ENDREPEAT
			
			GET_MODEL_DIMENSIONS(vehModel, vMin, vMax)
			vHeading = vStartPos - GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(2*vMax.x)*(fMiddle), -offsetVehicleDist, 0>>)
			fHeading = GET_HEADING_FROM_VECTOR_2D(vHeading.x, vHeading.y)
			veh[0][0] = CREATE_VEHICLE(vehModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(2*vMax.x)*(fMiddle), -offsetVehicleDist, 0>>), fHeading)
			SET_VEHICLE_FORWARD_SPEED(veh[0][0], vehicleSpeed)
			
			vHeading = vStartPos - GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(2*vMax.x)*(fMiddle), offsetVehicleDist, 0>>)
			fHeading = GET_HEADING_FROM_VECTOR_2D(vHeading.x, vHeading.y)
			veh[0][1] = CREATE_VEHICLE(vehModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<(2*vMax.x)*(fMiddle), offsetVehicleDist, 0>>), fHeading)
			SET_VEHICLE_FORWARD_SPEED(veh[0][1], vehicleSpeed)
			
			vHeading = vStartPos - GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<-offsetVehicleDist,(2*vMax.x)*(fMiddle),  0>>)
			fHeading = GET_HEADING_FROM_VECTOR_2D(vHeading.x, vHeading.y)
			veh[1][0] = CREATE_VEHICLE(vehModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<-offsetVehicleDist,(2*vMax.x)*(fMiddle),  0>>), fHeading)
			SET_VEHICLE_FORWARD_SPEED(veh[1][0], vehicleSpeed)
			
			vHeading = vStartPos - GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<offsetVehicleDist,(2*vMax.x)*(fMiddle), 0>>)
			fHeading = GET_HEADING_FROM_VECTOR_2D(vHeading.x, vHeading.y)
			veh[1][1] = CREATE_VEHICLE(vehModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<offsetVehicleDist,(2*vMax.x)*(fMiddle), 0>>), fHeading)
			SET_VEHICLE_FORWARD_SPEED(veh[1][1], vehicleSpeed)
		BREAK
		
		CASE PPT_PED_TABLE_GROUP_W_VEH_CRASH
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PPT_PED_TABLE_GROUP_W_VEH_CRASH", 10000, 1)
			
			REQUEST_MODEL(pedModel)
			REQUEST_MODEL(vehModel)
			REQUEST_MODEL(chairModel)
			REQUEST_MODEL(tableModel)
			WHILE NOT HAS_MODEL_LOADED(pedModel)
			OR NOT HAS_MODEL_LOADED(vehModel)
			OR NOT HAS_MODEL_LOADED(chairModel)
			OR NOT HAS_MODEL_LOADED(tableModel)
			WAIT(0)
			ENDWHILE
			
			//
			iBoxes = iTestSize
			fBoxes = TO_FLOAT(iBoxes)
			
			createAngle = 360/fBoxes
			createAngleHalved = 180/fBoxes	
			
			GET_MODEL_DIMENSIONS(tableModel, vMin, vMax)
			//fRadius = COS(createAngleHalved)*(vMax.x/SIN(createAngleHalved))
			fRadius = vMax.x
			GET_MODEL_DIMENSIONS(chairModel, vMin, vMax)
			fRadius += (vMax.y)
			//vStartPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<0, 6, 0>>)
			GET_GROUND_Z_FOR_3D_COORD(vStartPos, fTemp)
			prop[10][10] = CREATE_OBJECT_NO_OFFSET(tableModel, <<vStartPos.x, vStartPos.y, fTemp>>)
			REPEAT iTestSize i
				vTemp.x = vStartPos.x+(fRadius*SIN(TO_FLOAT(i)*createAngle))
				vTemp.y = vStartPos.y+(fRadius*COS(TO_FLOAT(i)*createAngle))
				vTemp.z = fTemp
				ped[0][i] = CREATE_PED(PEDTYPE_CIVMALE, pedModel, vTemp)
				vTemp += vStartPos
				SET_ENTITY_HEADING(ped[0][i], GET_HEADING_FROM_VECTOR_2D(vTemp.x, vTemp.y))
				
				vTemp.x = vStartPos.x+(fRadius*SIN(TO_FLOAT(i)*createAngle))
				vTemp.y = vStartPos.y+(fRadius*COS(TO_FLOAT(i)*createAngle))
				vTemp.z = fTemp
				prop[0][i] = CREATE_OBJECT_NO_OFFSET(chairModel, vTemp)
				TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(ped[0][i], vTemp, 1.5)
				vTemp -= vStartPos 
				SET_ENTITY_HEADING(prop[0][i], GET_HEADING_FROM_VECTOR_2D(vTemp.x, vTemp.y))
				
			ENDREPEAT
			
			vehRam = CREATE_VEHICLE(vehModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<0, -offsetVehicleDist, 0>>), fInitialPlayerHeading)
			//SET_ENTITY_VELOCITY(vehRam, vehicleVelocity)
			SET_VEHICLE_FORWARD_SPEED(vehRam, vehicleSpeed)
			
		BREAK
		
		CASE PPT_END
			SCRIPT_ASSERT("PHYSICS_TEST_TYPE = PPT_END. Most likely my fault.")
		BREAK
	ENDSWITCH
	
	
ENDPROC

PROC Output_Performance_Results()
	STRING testName
	IF iSelectedTest = ENUM_TO_INT(PPT_BOXES_NOT_TOUCHING)
		testName = "PPT_BOXES_NOT_TOUCHING"
	ELIF iSelectedTest = ENUM_TO_INT(PPT_BOXES_RANDOM_PILE)
		testName = "PPT_BOXES_RANDOM_PILE"
	ELIF iSelectedTest = ENUM_TO_INT(PPT_BOXES_STACKED)
		testName = "PPT_BOXES_STACKED"
	ELIF iSelectedTest = ENUM_TO_INT(PPT_NM_GROUP_NOT_TOUCHING)
		testName = "PPT_NM_GROUP_NOT_TOUCHING"
	ELIF iSelectedTest = ENUM_TO_INT(PPT_NM_GROUP_RANDOM_PILE)
		testName = "PPT_NM_GROUP_RANDOM_PILE"
	ELIF iSelectedTest = ENUM_TO_INT(PPT_VEH_NOT_TOUCHING)
		testName = "PPT_VEH_NOT_TOUCHING"
	ELIF iSelectedTest = ENUM_TO_INT(PPT_VEH_RANDOM_PILE)
		testName = "PPT_VEH_RANDOM_PILE"
	ELIF iSelectedTest = ENUM_TO_INT(PPT_PED_GROUP_STANDING)
		testName = "PPT_PED_GROUP_STANDING"
	ELIF iSelectedTest = ENUM_TO_INT(PPT_PED_GROUP_SHOOTING)
		testName = "PPT_PED_GROUP_SHOOTING"
	ELIF iSelectedTest = ENUM_TO_INT(PPT_PROPS_NOT_TOUCHING)
		testName = "PPT_PROPS_NOT_TOUCHING"
	ELIF iSelectedTest = ENUM_TO_INT(PPT_FRAG_PROPS_W_VEH_CRASH)
		testName = "PPT_FRAG_PROPS_W_VEH_CRASH"
	ELIF iSelectedTest = ENUM_TO_INT(PPT_FRAG_PROPS_W_EXP)
		testName = "PPT_FRAG_PROPS_W_EXP"
	ELIF iSelectedTest = ENUM_TO_INT(PPT_VEH_W_VEH_CRASH)
		testName = "PPT_VEH_W_VEH_CRASH"
	ELIF iSelectedTest = ENUM_TO_INT(PPT_VEH_W_EXP)
		testName = "PPT_VEH_W_EXP"
	ELIF iSelectedTest = ENUM_TO_INT(PPT_PED_GROUP_W_VEH_CRASH)
		testName = "PPT_PED_GROUP_W_VEH_CRASH"
	ELIF iSelectedTest = ENUM_TO_INT(PPT_PED_GROUP_W_EXP)
	 	testName = "PPT_PED_GROUP_W_EXP"	
	ELIF iSelectedTest = ENUM_TO_INT(PPT_MIXED_GROUP)
	 	testName = "PPT_MIXED_GROUP"
	ELIF iSelectedTest = ENUM_TO_INT(PPT_PED_TABLE_GROUP_W_VEH_CRASH)
	 	testName = "PPT_PED_TABLE_GROUP_W_VEH_CRASH"
	ENDIF
	
	OPEN_NAMED_DEBUG_FILE("X:/gta5/script/dev/Code/", "perftest.txt")
	
		SAVE_STRING_TO_NAMED_DEBUG_FILE(testName, "X:/gta5/script/dev/Code/", "perftest.txt")
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE("X:/gta5/script/dev/Code/", "perftest.txt")
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("SimUpdateTime over 1000 frames = ", "X:/gta5/script/dev/Code/", "perftest.txt")
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(SimUpdateTime, "X:/gta5/script/dev/Code/", "perftest.txt")
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE("X:/gta5/script/dev/Code/", "perftest.txt")
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("PreCollideTime over 1000 frames = ", "X:/gta5/script/dev/Code/", "perftest.txt")
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(PreCollideTime, "X:/gta5/script/dev/Code/", "perftest.txt")
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE("X:/gta5/script/dev/Code/", "perftest.txt")
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("CollideTime over 1000 frames = ", "X:/gta5/script/dev/Code/", "perftest.txt")
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(CollideTime, "X:/gta5/script/dev/Code/", "perftest.txt")
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE("X:/gta5/script/dev/Code/", "perftest.txt")
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("CollideJobsTime over 1000 frames = ", "X:/gta5/script/dev/Code/", "perftest.txt")
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(CollideJobsTime, "X:/gta5/script/dev/Code/", "perftest.txt")
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE("X:/gta5/script/dev/Code/", "perftest.txt")
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("PostCollideTime over 1000 frames = ", "X:/gta5/script/dev/Code/", "perftest.txt")
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(PostCollideTime, "X:/gta5/script/dev/Code/", "perftest.txt")
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE("X:/gta5/script/dev/Code/", "perftest.txt")
		
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE("X:/gta5/script/dev/Code/", "perftest.txt")
	CLOSE_DEBUG_FILE()
	
	SimUpdateTime = 0
	PreCollideTime = 0
	CollideTime = 0
	CollideJobsTime = 0
	PostCollideTime = 0
	
ENDPROC

PROC UPDATE_PROFILE_STATS()
	FLOAT frameTime
	frameTime = GET_PROFILE_STATS_FRAME_TIME("ph Simulator", "Simulator", "SimUpdate")
	SimUpdateTime += frameTime
	frameTime = GET_PROFILE_STATS_FRAME_TIME("ph Simulator", "Simulator", "PreCollide")
	PreCollideTime += frameTime
	frameTime = GET_PROFILE_STATS_FRAME_TIME("ph Simulator", "Simulator", "Collide")
	CollideTime += frameTime
	frameTime = GET_PROFILE_STATS_FRAME_TIME("ph Simulator", "Simulator", "CollideJobs")
	CollideJobsTime += frameTime
	frameTime = GET_PROFILE_STATS_FRAME_TIME("ph Simulator", "Simulator", "PostCollide")
	PostCollideTime += frameTime
	frameNumber++
ENDPROC

PROC Wait_for_Physics_Performance_Test_Selection()

	// Use KEY_S to (S)tart auto test
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
	OR bToggleAutoTest
		IF bAutoTestRun
			bAutoTestRun = FALSE
		ELSE
			Set_Creation_Defaults()
			iSelectedTest = (ENUM_TO_INT(PPT_NONE))
			frameNumber = numberOfTestFrames
			bAutoTestRun = TRUE
		ENDIF
		bToggleAutoTest = FALSE
	ENDIF
	
	IF frameNumber > numberOfTestFrames-1
		IF iSelectedTest <> ENUM_TO_INT(PPT_NONE)
			IF SimUpdateTime > 0
				Output_Performance_Results()
			ENDIF
		ENDIF
		
		IF bAutoTestRun
			iSelectedTest++
			IF iSelectedTest = ENUM_TO_INT(PPT_END)
				bAutoTestRun = FALSE
			ELSE
				bRunSelectedTest = TRUE
			ENDIF
		ENDIF
	ELSE
		UPDATE_PROFILE_STATS()
	ENDIF
	

	// Use KEY_DOWN to cycle through default tests
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
		Set_Creation_Defaults()
		iSelectedTest++
		
		IF iSelectedTest = ENUM_TO_INT(PPT_END)
			iSelectedTest = (ENUM_TO_INT(PPT_NONE)+1)
		ENDIF
		
		bRunSelectedTest = TRUE
	ENDIF
	// Use KEY_DOWN to cycle back through default tests
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
		Set_Creation_Defaults()
		iSelectedTest--
		
		IF iSelectedTest <= ENUM_TO_INT(PPT_NONE)
			iSelectedTest = (ENUM_TO_INT(PPT_END)-1)
		ENDIF
		
		bRunSelectedTest = TRUE
	ENDIF
	// Use KEY_RIGHT to replay current
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)
		//Set_Creation_Defaults()
		
		bRunSelectedTest = TRUE
	ENDIF
	
	
	IF bRunSelectedTest
		// Remove all previous crap
		Cleanup_Previous_Performance_Test()
		
		// Ensure that custom options are valid, then set them.
		IF NOT ARE_STRINGS_EQUAL(GET_CONTENTS_OF_TEXT_WIDGET(pedModelWidget), "New text widget")
			pedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(pedModelWidget)))
			PRINTSTRING(GET_CONTENTS_OF_TEXT_WIDGET(pedModelWidget))
		ENDIF
		IF NOT ARE_STRINGS_EQUAL(GET_CONTENTS_OF_TEXT_WIDGET(vehModelWidget), "New text widget")
			vehModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(vehModelWidget)))
			PRINTSTRING(GET_CONTENTS_OF_TEXT_WIDGET(vehModelWidget))
		ENDIF
		IF NOT ARE_STRINGS_EQUAL(GET_CONTENTS_OF_TEXT_WIDGET(propModelWidget), "New text widget")
			propModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(propModelWidget)))
			PRINTSTRING(GET_CONTENTS_OF_TEXT_WIDGET(propModelWidget))
		ENDIF
		
		// Ensures the script doesn't terminate due to using an invalid model.
		IF NOT IS_MODEL_IN_CDIMAGE(pedModel)
			PRINTSTRING("Attempted to set an ped vehicle model. Reset to S_M_Y_CLOWN_01. \n")
			pedModel = S_M_Y_CLOWN_01
		ENDIF
		IF NOT IS_MODEL_IN_CDIMAGE(vehModel)
			PRINTSTRING("Attempted to set an invalid vehicle model. Reset to SENTINEL. \n")
			vehModel = SENTINEL
		ENDIF
		IF NOT IS_MODEL_IN_CDIMAGE(propModel)
			PRINTSTRING("Attempted to set an invalid prop model. Reset to PROP_BOX_WOOD01A. \n")
			propModel = PROP_BOX_WOOD01A
		ENDIF
		
		Run_Physics_Performance_Test()
		
		bRunSelectedTest = FALSE
	ENDIF
ENDPROC

PROC Debug_Widget_Checks()
	
	IF IS_MOUSE_BUTTON_JUST_RELEASED(MB_LEFT_BTN)
		vWidgetStartPos = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
	ENDIF
	
	IF bSetNewStartPos
		vStartPos = vWidgetStartPos
		tempZ = vStartPos.z
		fInitialPlayerHeading = fWidgetInitialHeading
		
		vInitialPlayerCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fInitialPlayerHeading, <<-7.5, -10, 0>>)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fInitialPlayerHeading)
			SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vInitialPlayerCoord)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		ENDIF
		bSetNewStartPos = FALSE
	ENDIF
	
	IF bForceCleanup
		Cleanup_Previous_Performance_Test()
		bForceCleanup = FALSE
	ENDIF
	IF bClearCustomOptions
		Set_Creation_Defaults()
		bClearCustomOptions = FALSE
	ENDIF
ENDPROC



PROC Create_Performance_Test_Widgets()
	START_WIDGET_GROUP("Physics Performance Tests")		
		
		ADD_WIDGET_FLOAT_SLIDER("vStartPos.x", vWidgetStartPos.x, -1000, 1000, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("vStartPos.y", vWidgetStartPos.y, -1000, 1000, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("vStartPos.z", vWidgetStartPos.z, -1000, 1000, 0.01)
		
		ADD_WIDGET_FLOAT_SLIDER("Heading", fWidgetInitialHeading, 0, 359, 0.1)
		
		ADD_WIDGET_BOOL("Set new test start position and heading", bSetNewStartPos)
		
		ADD_WIDGET_BOOL("Force Cleanup", bForceCleanup)
		
		ADD_WIDGET_BOOL("Set to defaults", bClearCustomOptions)

		START_WIDGET_GROUP("Test type")
			
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("PPT_NONE")
				
				ADD_TO_WIDGET_COMBO("PPT_BOXES_NOT_TOUCHING")		
				ADD_TO_WIDGET_COMBO("PPT_BOXES_RANDOM_PILE")			
				ADD_TO_WIDGET_COMBO("PPT_BOXES_STACKED")				
				ADD_TO_WIDGET_COMBO("PPT_NM_GROUP_NOT_TOUCHING")		
				ADD_TO_WIDGET_COMBO("PPT_NM_GROUP_RANDOM_PILE") 		
				ADD_TO_WIDGET_COMBO("PPT_VEH_NOT_TOUCHING")			
				ADD_TO_WIDGET_COMBO("PPT_VEH_RANDOM_PILE")			
				ADD_TO_WIDGET_COMBO("PPT_PED_GROUP_STANDING")			
				ADD_TO_WIDGET_COMBO("PPT_PED_GROUP_SHOOTING")			
				ADD_TO_WIDGET_COMBO("PPT_PROPS_NOT_TOUCHING")			
//				ADD_TO_WIDGET_COMBO("PPT_PROPS_CLOTH_NOT_TOUCHING")	
				ADD_TO_WIDGET_COMBO("PPT_FRAG_PROPS_W_VEH_CRASH") 	
				ADD_TO_WIDGET_COMBO("PPT_FRAG_PROPS_W_EXP")			
				ADD_TO_WIDGET_COMBO("PPT_VEH_W_VEH_CRASH")			
				ADD_TO_WIDGET_COMBO("PPT_VEH_W_EXP")					
				ADD_TO_WIDGET_COMBO("PPT_PED_GROUP_W_VEH_CRASH")		
				ADD_TO_WIDGET_COMBO("PPT_PED_GROUP_W_EXP")	
				ADD_TO_WIDGET_COMBO("PPT_MIXED_GROUP")
				ADD_TO_WIDGET_COMBO("PPT_PED_TABLE_GROUP_W_VEH_CRASH")

			STOP_WIDGET_COMBO("PHYSICS_TEST_TYPE", iSelectedTest)
				
			ADD_WIDGET_BOOL("Run selected test", bRunSelectedTest)
			
		STOP_WIDGET_GROUP()
		
//		carJackStageWidget = 
		START_WIDGET_GROUP("Test options")
			
			// Ped model
			pedModelWidget = ADD_TEXT_WIDGET("Ped")
			// Prop model
			propModelWidget = ADD_TEXT_WIDGET("Prop")
			// Veh model
			vehModelWidget = ADD_TEXT_WIDGET("Vehicle")
			//ADD_WIDGET_BOOL("Use selected model", bUseCustomOptions)
			// Test size
			ADD_WIDGET_INT_SLIDER("Test size", iTestSize, 1, iTestSizeMax, 1)
			ADD_WIDGET_INT_SLIDER("Stack size (PPT_BOXES_STACKED)", iStackSize, 1, iStackSizeMax, 1)
			
			ADD_WIDGET_BOOL("Run selected test", bRunSelectedTest)
			// Use selected options
			
			// Clear options after use
			//ADD_WIDGET_BOOL("Clear/Reset custom options after run", bClearCustomOptions)
			
			ADD_WIDGET_INT_SLIDER("Number of frames to collect profile stats", numberOfTestFrames, 50, 2000, 50)
			ADD_WIDGET_BOOL("Toggle Auto Test ON/OFF", bToggleAutoTest)
			
			
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
ENDPROC


// ===========================================================================================================

SCRIPT(PHYSICS_PERF_STRUCT commandlineParams)
    vStartPos = commandlineParams.vInput
	fInitialPlayerHeading = commandlineParams.fInput
	
    Create_The_Player()
	WAIT(0)
    Create_Performance_Test_Widgets()

WHILE TRUE

	WAIT(0)
	
	Wait_for_Physics_Performance_Test_Selection()	

	Debug_Widget_Checks()
	
ENDWHILE
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Allow Startup to terminate
    TERMINATE_THIS_THREAD()
    
ENDSCRIPT


#ENDIF  // IS_DEBUG_BUILD

