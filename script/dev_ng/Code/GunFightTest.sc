// Do nothing in release mode
#IF IS_FINAL_BUILD
	SCRIPT
	ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Jorge O'Valle				Date: 25/03/11				│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						     Testing the V ambisonics							│
//│																				│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛


USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "commands_weapon.sch"
USING "commands_fire.sch"

USING "flow_public_core_override.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "selector_public.sch"
USING "player_ped_public.sch"


//Variables etc
MODEL_NAMES modelShooter = G_M_M_ChiBoss_01//A_M_Y_GENASIAN01
MODEL_NAMES lastModelShooter = G_M_M_ChiBoss_01//A_M_Y_GENASIAN01
WEAPON_TYPE shooterGun
WIDGET_GROUP_ID widgetDebug

//BOOL b_shooter_respawning = FALSE
BOOL weaponsAllowed[8]
//BOOL finish = FALSE
VECTOR playerPos 
INT testSoundId = GET_SOUND_ID()
//INT i_GunManRespawnTime = 5
INT shooterTimeToNewTarget = 10
INT i
INT modelNumber = 0
//INT currentShooter
CONST_INT numberOfShooters 2
FLOAT f_shooter_min_dist	=	5.0
FLOAT f_shooter_max_dist	=	15.0
FLOAT shooterBurstLength	= 3.0
FLOAT shooterAimOffset	=	1.0
PED_INDEX ShootersPedIndex[numberOfShooters]
BLIP_INDEX ShootersBlipIndex[numberOfShooters]
BOOL ShootersArmed[numberOfShooters]
// -----------------------------------------------------------------------------------------------------------
//		Init Camera
// -----------------------------------------------------------------------------------------------------------
PROC InitCamera()
	SET_DEBUG_CAM_ACTIVE(TRUE)
	SET_CAM_COORD(GET_DEBUG_CAM (),GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0,0.0,288.0>>) 
	SET_CAM_ROT(GET_DEBUG_CAM (), <<270.0,0.0,0.0>>)
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Aux
// -----------------------------------------------------------------------------------------------------------
PROC CreateWidgets()

	//SETUP INITIAL WEAPONS FOR GUNMEN
	//"WEAPONTYPE_PISTOL"
	weaponsAllowed[0] = TRUE
	//"WEAPONTYPE_SMG"
	weaponsAllowed[1] = TRUE
	//"WEAPONTYPE_ASSAULTRIFLE"
	weaponsAllowed[2] = TRUE
	//"WEAPONTYPE_PUMPSHOTGUN"
	weaponsAllowed[3] = TRUE
	//"WEAPONTYPE_HEAVYSNIPER"
	weaponsAllowed[4] = TRUE
	//"WEAPONTYPE_GRENADELAUNCHER"
	weaponsAllowed[5] = FALSE
	//"WEAPONTYPE_RPG"
	weaponsAllowed[6] = FALSE
	//"WEAPONTYPE_STUNGUN"
	weaponsAllowed[7] = FALSE

	widgetDebug = START_WIDGET_GROUP("Audio Test")
		START_WIDGET_GROUP("Shooters")
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("A_M_Y_MUSCLBEAC_01")
				ADD_TO_WIDGET_COMBO("S_M_M_HighSec_01")
				ADD_TO_WIDGET_COMBO("S_M_M_UPS_01")
				ADD_TO_WIDGET_COMBO("S_M_M_HighSec_01")
				ADD_TO_WIDGET_COMBO("A_F_Y_BEACH_01")
				ADD_TO_WIDGET_COMBO("A_F_M_BevHills_02")
				ADD_TO_WIDGET_COMBO("A_M_Y_CYCLIST_01")
				ADD_TO_WIDGET_COMBO("S_M_Y_COP_01")
				ADD_TO_WIDGET_COMBO("S_F_Y_HOOKER_01")
				ADD_TO_WIDGET_COMBO("S_M_Y_MARINE_01")
				ADD_TO_WIDGET_COMBO("S_M_Y_SWAT_01")
				ADD_TO_WIDGET_COMBO("S_M_Y_SWAT_01")
			STOP_WIDGET_COMBO("Enemy Ped", modelNumber)
			ADD_WIDGET_FLOAT_SLIDER("Min Distance", f_shooter_min_dist, 0.0, 200.0, 1)
			ADD_WIDGET_FLOAT_SLIDER("Max Distance", f_shooter_max_dist, 0.0, 200.0, 1)
			
			START_WIDGET_GROUP("Multiple Shooters")
//				ADD_WIDGET_BOOL("Timed Respawns", b_shooter_respawning)
//				ADD_WIDGET_INT_SLIDER("Respawn Time", i_GunManRespawnTime, 1, 20, 1)
				ADD_WIDGET_INT_SLIDER("Time before target change", shooterTimeToNewTarget, 1, 20, 1)
				ADD_WIDGET_BOOL("WEAPONTYPE_PISTOL",weaponsAllowed[0])
				ADD_WIDGET_BOOL("WEAPONTYPE_SMG",weaponsAllowed[1])
				ADD_WIDGET_BOOL("WEAPONTYPE_ASSAULTRIFLE",weaponsAllowed[2])
				ADD_WIDGET_BOOL("WEAPONTYPE_PUMPSHOTGUN",weaponsAllowed[3])
				ADD_WIDGET_BOOL("WEAPONTYPE_HEAVYSNIPER",weaponsAllowed[4])
				ADD_WIDGET_BOOL("WEAPONTYPE_GRENADELAUNCHER",weaponsAllowed[5])
				ADD_WIDGET_BOOL("WEAPONTYPE_RPG",weaponsAllowed[6])
				ADD_WIDGET_BOOL("WEAPONTYPE_STUNGUN",weaponsAllowed[7])
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC
PROC DestroyWidgets()
	IF DOES_WIDGET_GROUP_EXIST(widgetDebug)
		DELETE_WIDGET_GROUP(widgetDebug)
	ENDIF
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Weapons
// -----------------------------------------------------------------------------------------------------------
FUNC WEAPON_TYPE GetWeaponType(INT k)
	WEAPON_TYPE chosenGun
	SWITCH k
		CASE 0
			chosenGun = WEAPONTYPE_PISTOL
		BREAK
		CASE 1
			chosenGun = WEAPONTYPE_SMG
		BREAK
		CASE 2
			chosenGun = WEAPONTYPE_ASSAULTRIFLE
		BREAK
		CASE 3
			chosenGun = WEAPONTYPE_PUMPSHOTGUN
		BREAK
		CASE 4
			chosenGun = WEAPONTYPE_HEAVYSNIPER
		BREAK
		CASE 5
			chosenGun = WEAPONTYPE_GRENADELAUNCHER
		BREAK
		CASE 6
			chosenGun = WEAPONTYPE_RPG
		BREAK
		CASE 7
			chosenGun = WEAPONTYPE_STUNGUN
		BREAK
	ENDSWITCH

	RETURN chosenGun

ENDFUNC
FUNC WEAPON_TYPE GetAGunForShooter()
	BOOL NotAGoodGun = TRUE
	INT TriesToGetAGoodGun = 0
	INT temp
	WHILE NotAGoodGun
		temp = GET_RANDOM_INT_IN_RANGE(0,7)
		IF weaponsAllowed[temp]
			NotAGoodGun = FALSE
		ELSE
			IF TriesToGetAGoodGun > 10
				//FALL BACK TO PISTOL
				temp = 0
				NotAGoodGun = FALSE
			ENDIF
			temp++
		ENDIF
	ENDWHILE
	RETURN GetWeaponType(temp)
ENDFUNC
// -----------------------------------------------------------------------------------------------------------
//		Shooters
// -----------------------------------------------------------------------------------------------------------
PROC InitShooters()
	WHILE NOT HAS_MODEL_LOADED(modelShooter)
		REQUEST_MODEL(modelShooter)
		WAIT(0)
	ENDWHILE
ENDPROC
PROC CleanupShooters()
	FOR i = 0 to numberOfShooters - 1
		IF DOES_BLIP_EXIST(ShootersBlipIndex[i])
			REMOVE_BLIP(ShootersBlipIndex[i])
		ENDIF
		IF DOES_ENTITY_EXIST(ShootersPedIndex[i])
			DELETE_PED(ShootersPedIndex[i])
		ENDIF
	ENDFOR
	SET_MODEL_AS_NO_LONGER_NEEDED(modelShooter)
ENDPROC
FUNC BOOL IsShooting(PED_INDEX shooter)
    IF NOT IS_PED_INJURED(shooter)
        SCRIPTTASKSTATUS taskStatus = GET_SCRIPT_TASK_STATUS(shooter, SCRIPT_TASK_PERFORM_SEQUENCE)
        IF taskStatus = FINISHED_TASK
            RETURN FALSE
        ENDIF
    ENDIF

    RETURN TRUE
ENDFUNC
FUNC FLOAT GetHeadingBetweenVectors(VECTOR A, VECTOR B)
	RETURN GET_HEADING_FROM_VECTOR_2D(B.x-A.x,B.y-A.y)
ENDFUNC
PROC updateTheModel()
	lastModelShooter = modelShooter
	SWITCH modelNumber
		CASE 0
			modelShooter = A_M_Y_MUSCLBEAC_01
		BREAK
		CASE 1
			modelShooter = S_M_M_HighSec_01//A_M_M_BUSINESS01
		BREAK
		CASE 2
			modelShooter = S_M_M_UPS_01//A_M_O_GENLATINO01
		BREAK
		CASE 3
			modelShooter = S_M_M_HighSec_01
		BREAK
		CASE 4
			modelShooter = A_F_Y_BEACH_01
		BREAK
		CASE 5
			modelShooter = A_F_M_BevHills_02
		BREAK
		CASE 6
			modelShooter = A_M_Y_CYCLIST_01
		BREAK
		CASE 7
			modelShooter = S_M_Y_COP_01
		BREAK
		CASE 8
			modelShooter = S_F_Y_HOOKER_01
		BREAK
		CASE 9
			modelShooter = S_M_Y_MARINE_01
		BREAK
		CASE 10
			modelShooter = S_M_Y_SWAT_01
		BREAK
		CASE 11
			modelShooter = S_M_Y_SWAT_01
		BREAK
	ENDSWITCH
	
	IF modelShooter != lastModelShooter
		SET_MODEL_AS_NO_LONGER_NEEDED(lastModelShooter)
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(modelShooter)
		REQUEST_MODEL(modelShooter)
	ENDIF
ENDPROC
PROC makeMeAShooter(INT k)
	playerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	VECTOR newShooterPos = SHOOTER_POS_IN_SECTOR(2,7)//(GET_RANDOM_INT_IN_RANGE(1,8),GET_RANDOM_INT_IN_RANGE(1,8))
	ShootersPedIndex[k] = CREATE_PED(PEDTYPE_CRIMINAL, modelShooter, newShooterPos,GetHeadingBetweenVectors(newShooterPos, playerPos))
	ShootersArmed[k] = FALSE
ENDPROC
PROC removeDeadGunMenBlips()
	FOR i = 0 to numberOfShooters - 1
		IF DOES_BLIP_EXIST(ShootersBlipIndex[i])
			IF IS_ENTITY_DEAD(ShootersPedIndex[i])
				PRINTSTRING("PED ")
				PRINTINT(i)
				PRINTSTRING(" IS DEAD - REMOVING BLIP")
				PRINTNL()
				REMOVE_BLIP(ShootersBlipIndex[i])
			ENDIF
		ENDIF
	ENDFOR
ENDPROC
FUNC PED_INDEX getSafeCombatTarget(INT j)
	INT fightTarget
	INT guesses = 0
	WHILE guesses < 10
		fightTarget = GET_RANDOM_INT_IN_RANGE(0,COUNT_OF(ShootersPedIndex))
		IF DOES_ENTITY_EXIST(ShootersPedIndex[fightTarget])
			IF NOT(fightTarget = COUNT_OF(ShootersPedIndex) OR fightTarget = j OR IS_ENTITY_DEAD(ShootersPedIndex[fightTarget]))
				RETURN ShootersPedIndex[fightTarget]
			ENDIF
		ENDIF
	ENDWHILE
	RETURN PLAYER_PED_ID()
ENDFUNC
// -----------------------------------------------------------------------------------------------------------
//		Finish script
// -----------------------------------------------------------------------------------------------------------
PROC CleanUp()
	CleanupShooters()
	RELEASE_SOUND_ID(testSoundId)
	UNREGISTER_SCRIPT_WITH_AUDIO()
	DestroyWidgets()
	TERMINATE_THIS_THREAD()
ENDPROC
PROC FinishScript()
	Mission_Flow_Mission_Failed()
	CleanUp()
ENDPROC

PROC UpdateGunFight()

	updateTheModel()

	IF HAS_MODEL_LOADED(modelShooter)
//		IF shooterRespawning
//			IF TIMERA()>i_GunManRespawnTime*1000	
//				MakeAGunMan()
//			ENDIF
//			removeDeadGunMenBlips()
//		ELSE
		FOR i = 0 to numberOfShooters -1
			IF DOES_ENTITY_EXIST(ShootersPedIndex[i])
				IF IS_ENTITY_DEAD(ShootersPedIndex[i])
					SET_PED_AS_NO_LONGER_NEEDED(ShootersPedIndex[i])
					IF DOES_BLIP_EXIST(ShootersBlipIndex[i])
						REMOVE_BLIP(ShootersBlipIndex[i])
					ENDIF
					makeMeAShooter(i)
				ELSE
					IF NOT ShootersArmed[i]
						//shooterGun = GetAGunForShooter()
						shooterGun = GetWeaponType(0)
						ShootersBlipIndex[i] = ADD_BLIP_FOR_ENTITY(ShootersPedIndex[i])
						SET_PED_ACCURACY(ShootersPedIndex[i],GET_RANDOM_INT_IN_RANGE(0,100))
						GIVE_WEAPON_TO_PED(ShootersPedIndex[i], shooterGun, 100, true)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ShootersPedIndex[i], true)
						SET_CURRENT_PED_WEAPON(ShootersPedIndex[i], shooterGun, true)
						SET_PED_CURRENT_WEAPON_VISIBLE(ShootersPedIndex[i],TRUE)
						SET_PED_SHOOT_RATE(ShootersPedIndex[i],100)
						SET_PED_INFINITE_AMMO(ShootersPedIndex[i],TRUE)
						SET_PED_COMBAT_ABILITY(ShootersPedIndex[i], CAL_PROFESSIONAL)
//						SET_ENTITY_INVINCIBLE(ShootersPedIndex[i],TRUE)
						//TASK_COMBAT_PED_TIMED(ShootersPedIndex[i],getSafeCombatTarget(i),shooterTimeToNewTarget*1000)
						//WAIT( 5000)
						TASK_SHOOT_AT_COORD(ShootersPedIndex[i],playerPos + <<0,0,shooterAimOffset>>, floor(shooterBurstLength*1000), FIRING_TYPE_1_THEN_AIM)
						ShootersArmed[i] = TRUE
					ELSE
					//IF GET_SCRIPT_TASK_STATUS(ShootersPedIndex[i],SCRIPT_TASK_COMBAT)= FINISHED_TASK
					//			TASK_COMBAT_PED_TIMED(ShootersPedIndex[i],getSafeCombatTarget(i),shooterTimeToNewTarget*1000)
					IF GET_SCRIPT_TASK_STATUS(ShootersPedIndex[i],SCRIPT_TASK_SHOOT_AT_COORD)= FINISHED_TASK
						TASK_SHOOT_AT_COORD(ShootersPedIndex[i],playerPos + <<0,0,shooterAimOffset>>, floor(shooterBurstLength*1000), FIRING_TYPE_1_BURST)
						//EXPLODE_PED_HEAD(ShootersPedIndex[i])
//						finish = TRUE
					ENDIF
					ENDIF
				ENDIF
				
			ELSE
				makeMeAShooter(i)
			ENDIF
		ENDFOR
		ENDIF
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		Init
// -----------------------------------------------------------------------------------------------------------
PROC Init()	
	REGISTER_SCRIPT_WITH_AUDIO()
	CreateWidgets()
	InitCamera()
	InitShooters()
ENDPROC


// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(500)
	ENDIF
	Init()	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		PRINTSTRING("...Placeholder Mission Force Cleanup")
		PRINTNL()
		Mission_Flow_Mission_Force_Cleanup()
		CleanUp()
	ENDIF
	WHILE (TRUE)
		DRAW_GUN_FIGHT_GRID()
//	IF NOT finish
		UpdateGunFight()
//	ENDIF
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			FinishScript()
		ENDIF
		WAIT(0)
	ENDWHILE
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

#ENDIF  // IS_DEBUG_BUILD


