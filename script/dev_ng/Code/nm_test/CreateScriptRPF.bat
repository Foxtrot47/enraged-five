@echo off
::
:: When creating a new branch, make sure the following variables are updated:
::	* BUILD_BRANCH - branch as specified in $(toolsconfig)\project.xml
::	* EXPORT_FILE - path to the exported zip file.
::	* RPF_OUTPUT_FILE - path to the resulting rpf file.
::
call setenv_gen9_sga.bat

set BRANCH_EXTENSION=gen9_sga

:: Set some global variables
set BUILD_BRANCH=titleupdate_%BRANCH_EXTENSION%
set BUILD_CONFIGURATION=%~1
set BUILD_PLATFORM=%~2
set BUILD_FOLDER=%~3

set RPF_CREATE=%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.RpfCreate.exe
set RPF_CREATE_FILELIST=%~dp0\rpf_create_filelist.txt

:: Delete all .sco files in the preview folder
set PREVIEW_FOLDER="%BUILD_FOLDER%\preview"
if exist %PREVIEW_FOLDER%\*.sco del %PREVIEW_FOLDER%\*.sco

:: Compile the script.zip in the assets\export folder
set INPUT=%~dp0\%BUILD_CONFIGURATION%
set INPUT_FILTER=*.sco
set EXPORT_FILE=%RS_PROJROOT%\assets_%BRANCH_EXTENSION%\export\levels\nm_test\script\script.zip

:: Validate the input arguments
if "%BUILD_CONFIGURATION%"=="" (
	echo ERROR! Build configuration wasn't supplied to the post-compile batch script.
	exit 1
	goto :eof
)
if "%BUILD_PLATFORM%"=="" (
	echo ERROR! Build platform wasn't supplied to the post-compile batch script.
	exit 1
	goto :eof
)
if "%BUILD_FOLDER%"=="" (
	echo ERROR! Build folder wasn't supplied to the post-compile batch script.
	exit 1
	goto :eof
)

pushd %INPUT%
if EXIST %EXPORT_FILE% (
	attrib -r %EXPORT_FILE%
)

echo making export file:
echo %RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --filter=%INPUT_FILTER% --output=%EXPORT_FILE% %INPUT%
%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --filter=%INPUT_FILTER% --output=%EXPORT_FILE% %INPUT%

:: Build the script rpf for the requested platform.
if "%BUILD_PLATFORM%"=="XboxOne" (
	set PLATFORM_PREFIX=d
	set PLATFORM_PATH=xboxone
) else if "%BUILD_PLATFORM%"=="PS4" (
	set PLATFORM_PREFIX=o
	set PLATFORM_PATH=ps4
) else if "%BUILD_PLATFORM%"=="Win64" (
	set PLATFORM_PREFIX=y
	set PLATFORM_PATH=x64
) else if "%BUILD_PLATFORM%"=="Xbox360" (
	set PLATFORM_PREFIX=x
	set PLATFORM_PATH=xbox360
) else if "%BUILD_PLATFORM%"=="PS3" (
	set PLATFORM_PREFIX=c
	set PLATFORM_PATH=ps3
) else (
	echo Unsupported build platform '%BUILD_PLATFORM%'.  No RPF file will be generated.
	goto :eof
)

:: Clear out any compiled script files from the preview folder.
if exist %PREVIEW_FOLDER%\*.%PLATFORM_PREFIX%sc del %PREVIEW_FOLDER%\*.%PLATFORM_PREFIX%sc

:: Create the file list to use in the RpfCreate process.
set SOURCE_FOLDER=%INPUT%\%BUILD_PLATFORM%
set SOURCE_FILES=%SOURCE_FOLDER%\*.%PLATFORM_PREFIX%sc

if exist %RPF_CREATE_FILELIST% del %RPF_CREATE_FILELIST%
for /F "tokens=*" %%i in ('dir /B %SOURCE_FILES%') do (
	echo %SOURCE_FOLDER%\%%i %%i >> %RPF_CREATE_FILELIST%
)

set RPF_OUTPUT_FILE=%BUILD_FOLDER%\%PLATFORM_PATH%\levels\nm_test\script\script.rpf
attrib -r %RPF_OUTPUT_FILE%
%RPF_CREATE% --branch %BUILD_BRANCH% --platform %BUILD_PLATFORM% --filelist %RPF_CREATE_FILELIST% --output %RPF_OUTPUT_FILE%
if errorlevel 0 (
	echo %RPF_OUTPUT_FILE% successfully converted.
) else (
	echo Conversion failed!  Check for previous errors to determine what went wrong.
)

popd