USING "rage_builtins.sch"
USING "script_xml.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "commands_streaming.sch"
USING "commands_graphics.sch"
USING "commands_debug.sch"
USING "test_tools.sch"
USING "commands_vehicle.sch"
USING "commands_camera.sch"
USING "commands_player.sch"
USING "commands_fire.sch"
USING "commands_clock.sch"

USING "buildtype.sch"

//PURPOSE: Returns the offset to the origin of a specific map
VECTOR GET_MAP_OFFSET=<<0.0, 0.0, 6.127>>

//Notes: This is script is to allow easy setup of test scenarios for the code department. It works in conjunction with test_tools.sch
//It  is a data driven system 

//Defaults values when the test is reset to factory the standard values are used
STRING XMLMenu = "Testbed/TestNmmenu"

WEAPON_TYPE HELD_WEAPON = WEAPONTYPE_PISTOL

//Ped Vars
CONST_INT MAX_NUMBER_OF_PEDS 			10

peds_struct TestPeds [MAX_NUMBER_OF_PEDS]

//Vehicle Vars
CONST_INT MAX_NUMBER_OF_VEHICLES 	 10

vehicle_struct TestVehicles[MAX_NUMBER_OF_VEHICLES]

//objects
CONST_INT MAX_NUMBER_OF_OBJECTS 1

OBJECT_INDEX oHook
VECTOR vHookPos
VECTOR vHookVelocity = <<0.0, 0.0, 0.0>>

VECTOR vCarriedPos = <<0.176,0.108,-0.124>>
VECTOR vCarriedRot = <<4.320,174.240,23.040>>

VECTOR vAgainstWallPedsCoords = << 17.5, -6.61, 0.0>> 
float fPedWallDist = 0.4

Object_struct TestObject[MAX_NUMBER_OF_OBJECTS]

// Shot_2 variables
FLOAT gShot2Distance = 4.0
FLOAT gShot2OrientationOffset = 0.0
FLOAT gShot2LastDistance = 4.0
FLOAT gShot2LastOrientationOffset = 0.0
FLOAT SHOT_2_DYNAMIC_MOVE_TYPE = 1.0
FLOAT gShot2MovementDistance = 10.0
INT gFramesToCleanup
INT deadPedTimer
INT gShot2PedType = 0
bool shot2PedInitialized = FALSE
bool gShot2bForward = FALSE
bool gShot2bLeft = FALSE
bool gShot2bRight = FALSE
bool gShot2bBackward = FALSE
bool gShot2bWasForward = FALSE
bool gShot2bWasLeft = FALSE
bool gShot2bWasRight = FALSE
bool gShot2bWasBackward = FALSE
bool gShot2Crouch = FALSE
bool gShot2WasCrouch = FALSE
bool gPositionOnCliff = FALSE
bool gPositionOnCliffLast = FALSE
VECTOR vFallFromCliffPlayerStartPos = << 44.0, 38.6, 21.1 >> 
VECTOR vFallFromCliffFixedCamPos = << 38.6, 34.6, 22.4 >> 
float vFallFromCliffPlayerStartHeading = 90.0

bool gGrabVehicleDoorLockDoor = TRUE

//Route Vars 
CONST_INT MAX_NUMBER_OF_NODES 				7

RouteStruct Route[MAX_NUMBER_OF_NODES]

//Camera Vars 
CONST_INT MAX_NUMBER_OF_CAMERAS			2
CONST_INT FixedCamera										0
CONST_INT TrackingCamera									1

StCameraData TestCams[MAX_NUMBER_OF_CAMERAS]

bool	bAutoSwitchToTrackCam = FALSE

float gMinZoom = 0.5
float gMaxZoom = 15
float gZoomspeed = 0.15
float gCamzoom =4.5
float gCamPan = 270
float gCamPitch = -72

float gCamZoomMIn = 0.5 
float gCamZoomMax = 20.0

INT PedComp

//TASK
SEQUENCE_INDEX TEST_SEQ

//Functions: WIDGETS

FLOAT BUMP_PLAYER_DYNAMIC_MOVE_TYPE = 2.0
FLOAT RUN_SHOT_DYNAMIC_MOVE_TYPE = 2.0
FLOAT WALK_DOWN_HILL_DYNAMIC_MOVE_TYPE = 1.0


int PedComponent  = 0

bool gbshootPed = FALSE
bool gStartedDangle = FALSE

float gVehicleAttachBone = 49.0
VECTOR gVehicleAttachPositionOffset = <<0.0,- 0.4, 0.0>>
VECTOR gVehicleAttachRotationOffset = <<0.0, 0.0, 0.0>>

VECTOR CurrentPedPos
float gVehicleSpeed = 1.0

float gVehicleMoveDistance = 10.0
VECTOR gVehBumpPlayerPlayerStartPos = << 0.9, 1.0, 7.1 >> 
VECTOR gVehBumpPlayerCarStartOffset = << -2.0, 0.0, 0.0 >> 
float gVehBumpPlayerPlayerStartHeading = 90.0
float gVehBumpPlayerCarStartHeading = 180.0
bool gVehBumpPlayerInitialized = FALSE
bool gVehBumpPlayerReversing = FALSE
bool gVehBumpPlayerForceFallOff = FALSE
bool gStopVehicleIfPlayerRagdolls = FALSE

// fall
VECTOR gInitialFallVelocity = <<0.0, 0.0, 0.0>>

float gtestPan = 270, gtestPitch = -72 , gtestZoom = 3.25
//float  gvehicleZoom = 3.25,  gVehiclePitch = -90, gVehiclepan = 270

//Punch
//float gTestPedheading = 0.0

//NM Explosion
int  ExplosionTag
float ExplosionScale= 1.0
FLOAT fExplosionDistance 
bool bNoExplosionEffects  = FALSE
bool bTestVehicleExplosion = FALSE
bool bWasTestVehicleExplosion = TRUE
int giPedHealth = 200
//NM relax 
Float NM_Relaxation = 50.0
Bool bHoldPose = FALSE

//NM Teeter
VECTOR vImpulseTeeter
VECTOR vOffsetTeeter = <<0.0, 0.0, 0.0>>
FLOAT fPedEdgeDist = 0.25
//PED_RAGDOLL_COMPONENTS PedComp

//NM flinch		
bool bNM_FLINCH_RIGHT_HANDED			 = FALSE
bool bNM_FLINCH_LEFT_HANDED = FALSE
float fNM_FLINCH_HAND_DIST_LEFTRIGHT = 0.1
float fNM_FLINCH_HAND_DIST_FRONTBACK = 0.1
float fNM_FLINCH_HAND_DIST_VERTICAL = 0.1
float fNM_FLINCH_BODY_STIFFNESS = 12.0
float fNM_FLINCH_BODY_DAMPING = 1.0
float fNM_FLINCH_BACK_BEND = -0.55
bool bNM_FLINCH_USE_LEFT_ARM = TRUE
bool bNM_FLINCH_USE_RIGHT_ARM = TRUE
float fNM_FLINCH_NOISE_SCALE = 0.1
bool bNM_FLINCH_NEW_HIT = TRUE 
INT iNM_FLINCH_TURN_TOWARDS = 1 
				
INT MIN_TASK_TIME = 1000, MAX_TASK_TIME = 2000

//NM Grab
BOOL  bNM_GRAB_A_LINE = FALSE
BOOL bNM_GRAB_BRACE_ONLY = FALSE
BOOL bNM_GRAB_SURFACE = FALSE
BOOL bNM_GRAB_USE_LEFT = TRUE
BOOL bNM_GRAB_USE_RIGHT = true
BOOL bNM_GRAB_DONT_LET_GO = FALSE

int iNM_GRAB_INSTANCE_INDEX  = -1
//int iNM_GRAB_INSTANCE_PART_INDEX = 0

//VECTOR vPos1
//VECTOR vPos2
//VECTOR vPos3
//VECTOR vPos4

VECTOR vImpulse = <<0.0, 0.0, -20>>
VECTOR vOffset, vOffsetVec1 = <<-1.75, -0.1, 0.95>>, vOffsetVec2 = <<1.5, -0.1, 0.95>>,  vOffsetVec3 =  <<1.5, -0.1, 0.85>>, vOffsetVec4 =   <<-1.75, -0.1, 0.85>>
int gGrabType = 0

FLOAT fNM_GRAB_STRENGTH = -1.0
FLOAT fNM_GRAB_BODY_STIFFNESS = 11.0
FLOAT fNM_GRAB_REACH_ANGLE = 2.8
FLOAT fNM_GRAB_ONE_SIDE_REACH_ANGLE = 1.4
FLOAT fNM_GRAB_DISTANCE = 0.45
FLOAT fNM_GRAB_PULLUP_TIMER = 1.0
FLOAT fNM_GRAB_HOLD_MAX_TIMER = 1.00
FLOAT fNM_GRAB_PULLUP_STRENGTH_LEFT = 0.0
FLOAT fNM_GRAB_PULLUP_STRENGTH_RIGHT = 0.0

float gdistance_to_grab = 0.7
//bool ENABLE_MOVE_OFFSET = false
bool gresetoffsets = FALSE
	//Vector VehiclePos[4],vPos3,vPos4

//VECTOR vCarOffset 

int nVehicleIdentifier
ENUM eSelectedVehicleType
	SELECTED_VEHICLE_TYPE_CAR,
	SELECTED_VEHICLE_TYPE_LORRY,
	SELECTED_VEHICLE_TYPE_HELICOPTER,
	SELECTED_VEHICLE_TYPE_AEROPLANE
ENDENUM

// NM bind pose.
BOOL bDO_BLEND_FROM_NM = FALSE
BOOL bNM_BINDPOSE_FIRST_TIME = TRUE

// NM Shooting while attached:
int iNM_SHOOTINGWHILEATTACHED_INSTANCE_PART_INDEX = 15
//bool bNM_RAGDOLL_COLLIDES_WITH_VAN = FALSE
bool bNM_ANIMPOSE_DEBUGANIMMATCH = FALSE

// NM Skiing down slope.
VECTOR vTopOfSkiSlope = <<-141.32, -592.01, 157.18>>
VECTOR vBottomOfSkiSlope = << -149.54, -168.81, 7.49 >>
FLOAT fLateralMin = -150.0 // Min lateral extent of ski route control points.
FLOAT fLateralMax = -125.0 // Max    "      "    "   "    "      "      "

// NM truck bed
BOOL bFlatbedTruck = FALSE

// NM cover
INT nElectrocuteWaitTime = 2000
INT nElectrocuteTime = 65534
BOOL bRagdollOnCollision = FALSE
BOOL bCheckShouldSeekCover = FALSE

FLOAT fOldBottomOfSkiRoute = vBottomOfSkiSlope.y
VECTOR vSkiRoute[7]
BOOL bResetSkiRouteStraight = FALSE
BOOL bResetSkiRouteSlalom = TRUE

//weapon type
int gWeapon = 4 
int gLastWeapon = 4 
BOOL bAimWeapon = FALSE
BOOL bInvulnerableTestPed = FALSE
BOOL bGetInCarMode = FALSE
BOOL bWasAimingWeapon = FALSE
BOOL bBlockTempAIEvents = FALSE
BOOL bLastBlockTempAIEvents = FALSE


/// PURPOSE: Creates a valid vehicle of the specified type
///    
/// RETURNS:
///    
FUNC MODEL_NAMES NMVehicleModelSelection()
    MODEL_NAMES current_model

        SWITCH INT_TO_ENUM(eSelectedVehicleType, nVehicleIdentifier)
            CASE SELECTED_VEHICLE_TYPE_CAR
             current_model = INFERNUS
            BREAK

			CASE SELECTED_VEHICLE_TYPE_LORRY
                current_model = BENSON
            BREAK
            
            CASE SELECTED_VEHICLE_TYPE_HELICOPTER
                current_model= BUZZARD 
            BREAK
            
            CASE SELECTED_VEHICLE_TYPE_AEROPLANE
                current_model= CUBAN800
            BREAK
        ENDSWITCH
 
    RETURN current_model
    
ENDFUNC

PROC CYCLE_WEAPON()
	SWITCH HELD_WEAPON
    	CASE WEAPONTYPE_PISTOL 
    		HELD_WEAPON = WEAPONTYPE_SMG
   		BREAK
    	CASE WEAPONTYPE_SMG
    		HELD_WEAPON = WEAPONTYPE_ASSAULTRIFLE
   		BREAK    
   	 	CASE WEAPONTYPE_ASSAULTRIFLE 
    		HELD_WEAPON = WEAPONTYPE_PUMPSHOTGUN 
   		BREAK
		CASE WEAPONTYPE_PUMPSHOTGUN
    		HELD_WEAPON = WEAPONTYPE_SNIPERRIFLE 
   		BREAK    	
    	CASE WEAPONTYPE_SNIPERRIFLE
    		HELD_WEAPON = WEAPONTYPE_GRENADELAUNCHER 
   		BREAK    
    	CASE WEAPONTYPE_GRENADELAUNCHER 
    		HELD_WEAPON = WEAPONTYPE_UNARMED 
   		BREAK	
		DEFAULT 
    		HELD_WEAPON = WEAPONTYPE_PISTOL 
   		BREAK
    ENDSWITCH
ENDPROC

//////////////////////////////////////////////////////////////////////////
// Purpose: Reset the ski route control points.
PROC RESET_NM_SKI_TEST_ROUTE()
	FLOAT fTopOfRoute = vTopOfSkiSlope.y
	FLOAT fBottomOfRoute = vBottomOfSkiSlope.y
	FLOAT fCentreLine = fLateralMin + ((fLateralMax - fLateralMin) / 2.0)
	// Compute increments between control points in y-direction.
	FLOAT fDeltaY = ABSF(fBottomOfRoute-fTopOfRoute) / 7.0
	FLOAT fGroundZ, fYCoord

	IF bResetSkiRouteStraight = TRUE
		INT i
		FOR i = 0 TO 6
			fGroundZ = 160.0
			fYCoord = fTopOfRoute + i*fDeltaY
			GET_GROUND_Z_FOR_3D_COORD(<<fCentreLine, fYCoord, fGroundZ>>, fGroundZ)
			vSkiRoute[i] = <<fCentreLine, fYCoord, fGroundZ>>
		ENDFOR
		bResetSkiRouteStraight = FALSE
	ENDIF
	IF bResetSkiRouteSlalom = TRUE
		vSkiRoute[0] = vTopOfSkiSlope
		INT i
		FOR i = 1 TO 5 STEP 2
			fGroundZ = 160.0
			fYCoord = fTopOfRoute + i*fDeltaY
			GET_GROUND_Z_FOR_3D_COORD(<<fLateralMin, fYCoord, fGroundZ>>, fGroundZ)
			vSkiRoute[i] = <<fLateralMin, fYCoord, fGroundZ>>
			fGroundZ = 160.0
			fYCoord = fTopOfRoute + (i+1)*fDeltaY
			GET_GROUND_Z_FOR_3D_COORD(<<fLateralMax, fYCoord, fGroundZ>>, fGroundZ)
			vSkiRoute[i+1] = <<fLateralMax, fYCoord, fGroundZ>>
		ENDFOR
		vSkiRoute[6] = vBottomOfSkiSlope
		bResetSkiRouteSlalom = FALSE
	ENDIF
	
	// If the length has been altered by a widget, we want to reflect that interactively too.
	IF(fOldBottomOfSkiRoute != fBottomOfRoute)
		INT i
		FOR i = 0 TO 6
			fYCoord = fTopOfRoute + i*fDeltaY
			vSkiRoute[i].y = fYCoord
			fGroundZ = 160.0
			GET_GROUND_Z_FOR_3D_COORD(<<vSkiRoute[i].x, fYCoord, fGroundZ>>, fGroundZ)
			vSkiRoute[i].z = fGroundZ
		ENDFOR
		fOldBottomOfSkiRoute = fBottomOfRoute
	ENDIF
	
	
	// If we are using the Route structure function/
	INT i
	FOR i = 0 TO 6
		Route[i].RouteNode = vSkiRoute[i]
		Route[i].RouteNodeState = TRUE
	ENDFOR
ENDPROC
PROC DRAW_DEBUG_SKI_ROUTE()
	INT i
	FOR i = 0 to 6
		DRAW_DEBUG_SPHERE(<<vSkiRoute[i].x, vSkiRoute[i].y, vSkiRoute[i].z+3.0>>, 0.1, 0, 0, 255, 255)
	ENDFOR
	
	FOR i = 0 to 5
		DRAW_DEBUG_LINE(<<vSkiRoute[i].x, vSkiRoute[i].y, vSkiRoute[i].z+3.0>>, <<vSkiRoute[i+1].x, vSkiRoute[i+1].y, vSkiRoute[i+1].z+3.0>>, 0, 0, 255, 255)
	ENDFOR
ENDPROC
PROC CREATE_TEST_WIDGET ()
		START_WIDGET_GROUP("Tests")
		SETUP_WIDGET ()	
			START_WIDGET_GROUP("NM")
				START_WIDGET_GROUP ("NM Ped Selection")		
					START_NEW_WIDGET_COMBO() 
					    ADD_TO_WIDGET_COMBO ("Male")
                        ADD_TO_WIDGET_COMBO ("Female")
					STOP_WIDGET_COMBO("Ped", PedIdentifier)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP ("NM Vehicle selection")	
					START_NEW_WIDGET_COMBO()
						// Must match order from ENUM eSelectedVehicleType defined above!
						ADD_TO_WIDGET_COMBO("Car")
						ADD_TO_WIDGET_COMBO("Lorry")
						ADD_TO_WIDGET_COMBO("Helicopter")
						ADD_TO_WIDGET_COMBO("Aeroplane")
					STOP_WIDGET_COMBO("Vehicle", nVehicleIdentifier)
				STOP_WIDGET_GROUP()  // "NM Vehicle selection"

				START_WIDGET_GROUP ("NM Shot")
					ADD_WIDGET_BOOL ("Position on cliff", gPositionOnCliff)				
					START_NEW_WIDGET_COMBO ( )
						ADD_TO_WIDGET_COMBO ("Man")
						ADD_TO_WIDGET_COMBO ("Woman")
						ADD_TO_WIDGET_COMBO ("Large Man")
						ADD_TO_WIDGET_COMBO ("Cop")
						ADD_TO_WIDGET_COMBO ("Swat")
						ADD_TO_WIDGET_COMBO ("Animal")
					STOP_WIDGET_COMBO("Ped Type", gShot2PedType)
					ADD_WIDGET_INT_SLIDER("Frames To Cleanup", gFramesToCleanup, 0, 500, 60)
					ADD_WIDGET_FLOAT_SLIDER("Distance", gShot2Distance, -20.0, 100.0, 0.5)
					ADD_WIDGET_FLOAT_SLIDER("Rotation Offset", gShot2OrientationOffset, 0.0, 360.0, 0.01)
					START_NEW_WIDGET_COMBO ( )
						ADD_TO_WIDGET_COMBO ("Pistol")
						ADD_TO_WIDGET_COMBO ("AK47")
						ADD_TO_WIDGET_COMBO ("Shotgun")
						ADD_TO_WIDGET_COMBO ("RubberGun")
						ADD_TO_WIDGET_COMBO ("Unarmed")
					STOP_WIDGET_COMBO("Weapon Type", gWeapon)
					ADD_WIDGET_BOOL ("Aim Weapon", bAimWeapon)
					ADD_WIDGET_BOOL ("Don't deal damage", bInvulnerableTestPed)
					ADD_WIDGET_BOOL ("Get in car mode (will stop aiming after getting shot if Block Temp AI Events is true)", bGetInCarMode)
					ADD_WIDGET_BOOL ("Block Temp AI Events", bBlockTempAIEvents)
					ADD_WIDGET_FLOAT_SLIDER("Movement Speed", SHOT_2_DYNAMIC_MOVE_TYPE, 1.0, 3.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Movement Distance", gShot2MovementDistance, 1.0, 100.0, 0.5)
					ADD_WIDGET_BOOL ("Move Forward", gShot2bForward)
					ADD_WIDGET_BOOL ("Move Left", gShot2bLeft)
					ADD_WIDGET_BOOL ("Move Right", gShot2bRight)
					ADD_WIDGET_BOOL ("Move Backward", gShot2bBackward)
					ADD_WIDGET_BOOL ("Crouch", gShot2Crouch)
				STOP_WIDGET_GROUP() // "NM Shot"
				
				START_WIDGET_GROUP ("NM Shot Unit Test")
					START_NEW_WIDGET_COMBO ( )
						ADD_TO_WIDGET_COMBO ("L foot")
						ADD_TO_WIDGET_COMBO ("R foot")
						ADD_TO_WIDGET_COMBO ("Left knee")
						ADD_TO_WIDGET_COMBO ("Right knee")
						ADD_TO_WIDGET_COMBO ("L Hand")
						ADD_TO_WIDGET_COMBO ("R Hand")
						ADD_TO_WIDGET_COMBO ("Pelvis")
						ADD_TO_WIDGET_COMBO ("chest")
						ADD_TO_WIDGET_COMBO ("Left shoulder")
						ADD_TO_WIDGET_COMBO ("right shoulder")
						ADD_TO_WIDGET_COMBO ("head")
					STOP_WIDGET_COMBO("ped component", pedComponent)
					START_NEW_WIDGET_COMBO ( )
						ADD_TO_WIDGET_COMBO ("Pistol")
						ADD_TO_WIDGET_COMBO ("AK47")
						ADD_TO_WIDGET_COMBO ("Shotgun")
						ADD_TO_WIDGET_COMBO ("RubberGun")						
					STOP_WIDGET_COMBO("weapon type", gWeapon)
				STOP_WIDGET_GROUP() // "NM Shot"				
				
				START_WIDGET_GROUP ("NM Run Shot")
					ADD_WIDGET_FLOAT_SLIDER("movement speed", RUN_SHOT_DYNAMIC_MOVE_TYPE, 1.0, 3.0, 0.01)
					START_NEW_WIDGET_COMBO ( )
						ADD_TO_WIDGET_COMBO ("Pistol")
						ADD_TO_WIDGET_COMBO ("AK47")
						ADD_TO_WIDGET_COMBO ("Shotgun")
						ADD_TO_WIDGET_COMBO ("RubberGun")								
					STOP_WIDGET_COMBO("weapon type", gWeapon)
				STOP_WIDGET_GROUP() // "NM Run Shot"
						
				START_WIDGET_GROUP ("NM Bump player")
					ADD_WIDGET_FLOAT_SLIDER("movement speed", BUMP_PLAYER_DYNAMIC_MOVE_TYPE, 1.0, 3.0, 0.01)
				STOP_WIDGET_GROUP()	// "NM Bump player"
				
				START_WIDGET_GROUP ("NM player bumped by vehicle")
					ADD_WIDGET_BOOL ("Stop vehicle when the player ragdolls", gStopVehicleIfPlayerRagdolls)
					ADD_WIDGET_FLOAT_SLIDER ("Vehicle speed", gVehicleSpeed,  0, 20, 1.0 )
					ADD_WIDGET_FLOAT_SLIDER ("Vehicle movement distance", gVehicleMoveDistance,  0, 50, 1.0 )
					ADD_WIDGET_BOOL ("Force enemy ped fall off", gVehBumpPlayerForceFallOff)
				STOP_WIDGET_GROUP()	// "NM player bumped by vehicle"				
			
				START_WIDGET_GROUP ("NM Fall")
					ADD_WIDGET_FLOAT_SLIDER ("ped height",CurrentPedPos.z,  -2000, 2000, 0.25)
					ADD_WIDGET_FLOAT_SLIDER	("initial velocity x", gInitialFallVelocity.x, -50, 50, 0.1)
					ADD_WIDGET_FLOAT_SLIDER	("initial velocity y", gInitialFallVelocity.y, -50, 50, 0.1)
					ADD_WIDGET_FLOAT_SLIDER	("initial velocity z", gInitialFallVelocity.z, -50, 50, 0.1)
				STOP_WIDGET_GROUP() // "NM Fall"
		
				START_WIDGET_GROUP ("NM grab vehicle door")
					ADD_WIDGET_BOOL ("Lock vehicle door", gGrabVehicleDoorLockDoor)
				STOP_WIDGET_GROUP() // "NM grab vehicle door"			
		
				START_WIDGET_GROUP ("NM Vehicle Balance")
					ADD_WIDGET_FLOAT_SLIDER ("vehicle speed", gVehicleSpeed,  0, 2000, 1.0 )
				STOP_WIDGET_GROUP() // "NM Vehicle Balance"
			
				START_WIDGET_GROUP("NM Jump from vehicle")
						ADD_WIDGET_FLOAT_SLIDER ("vehicle speed",gVehicleSpeed, 0, 100, 1.0 )
				STOP_WIDGET_GROUP()	// "NM Jump from vehicle"
		
				START_WIDGET_GROUP("NM shooting while attached")
					ADD_WIDGET_FLOAT_SLIDER ("vehicle attach bone",gVehicleAttachBone, 0, 100, 1.0 )
					START_WIDGET_GROUP("Attach offset position")
						ADD_WIDGET_FLOAT_SLIDER ("x",gVehicleAttachPositionOffset.x,  -10, 10, 0.001)
						ADD_WIDGET_FLOAT_SLIDER ("y",gVehicleAttachPositionOffset.y,  -10, 10, 0.001)
						ADD_WIDGET_FLOAT_SLIDER ("z",gVehicleAttachPositionOffset.z,  -10, 10, 0.001)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Attach offset rotation")
						ADD_WIDGET_FLOAT_SLIDER ("x",gVehicleAttachRotationOffset.x,  -10, 10, 0.001)
						ADD_WIDGET_FLOAT_SLIDER ("y",gVehicleAttachRotationOffset.y,  -10, 10, 0.001)
						ADD_WIDGET_FLOAT_SLIDER ("z",gVehicleAttachRotationOffset.z,  -10, 10, 0.001)
					STOP_WIDGET_GROUP()
				STOP_WIDGET_GROUP()	// "NM Jump from vehicle"
		
				START_WIDGET_GROUP("NM Explosion")
					START_NEW_WIDGET_COMBO() 
						ADD_TO_WIDGET_COMBO ("ROCKET")
						ADD_TO_WIDGET_COMBO ("GRENADE")
						ADD_TO_WIDGET_COMBO ("MOLOTOV")
					STOP_WIDGET_COMBO("Explosion", ExplosionTag)
					ADD_WIDGET_FLOAT_SLIDER ("Explosion Scale",ExplosionScale,  0, 1.0, 0.01 )
					ADD_WIDGET_BOOL ("No Explosion effects",bNoExplosionEffects)
					ADD_WIDGET_BOOL ("Test vehicles instead of humans",bTestVehicleExplosion)
					ADD_WIDGET_FLOAT_READ_ONLY ("Explosion Distance", fExplosionDistance )
					ADD_WIDGET_INT_SLIDER ("Ped Health", giPedHealth, 100, 1000, 1)
				STOP_WIDGET_GROUP() // "NM Explosion"

				START_WIDGET_GROUP("NM_Body_Relax")
					ADD_WIDGET_FLOAT_SLIDER ("NM_Relaxation", NM_Relaxation,  0, 100.0, 1.0 )
					ADD_WIDGET_BOOL ("Hold pose",bHoldPose)
				STOP_WIDGET_GROUP() // "NM_Body_Relax"
				
				START_WIDGET_GROUP("NM_Flinch")
					ADD_WIDGET_BOOL ("NM_FLINCH_RIGHT_HANDED", bNM_FLINCH_RIGHT_HANDED)
					ADD_WIDGET_BOOL ("NM_FLINCH_LEFT_HANDED", bNM_FLINCH_LEFT_HANDED)
					ADD_WIDGET_FLOAT_SLIDER ("NM_FLINCH_HAND_DIST_LEFTRIGHT", fNM_FLINCH_HAND_DIST_LEFTRIGHT,  0, 1.0, 0.01 )
					ADD_WIDGET_FLOAT_SLIDER ("NM_FLINCH_HAND_DIST_FRONTBACK", fNM_FLINCH_HAND_DIST_FRONTBACK,  0, 1.0, 0.01 )
					ADD_WIDGET_FLOAT_SLIDER ("NM_FLINCH_HAND_DIST_VERTICAL", fNM_FLINCH_HAND_DIST_VERTICAL,  0, 1.0, 0.1 )
					ADD_WIDGET_FLOAT_SLIDER ("NM_FLINCH_BODY_STIFFNESS", fNM_FLINCH_BODY_STIFFNESS,  6.0, 16.0, 0.1 )
					ADD_WIDGET_FLOAT_SLIDER ("NM_FLINCH_BODY_DAMPING", fNM_FLINCH_BODY_DAMPING, 0, 1.5, 0.1 )
					ADD_WIDGET_FLOAT_SLIDER ("NM_FLINCH_BACK_BEND", fNM_FLINCH_BACK_BEND, -1.0, 1.0, 0.1 )
					ADD_WIDGET_FLOAT_SLIDER ("NM_FLINCH_NOISE_SCALE", fNM_FLINCH_NOISE_SCALE, 0.0, 1.0, 0.1 )
					ADD_WIDGET_BOOL ("NM_FLINCH_USE_LEFT_ARM", bNM_FLINCH_USE_LEFT_ARM)
					ADD_WIDGET_BOOL ("NM_FLINCH_USE_RIGHT_ARM", bNM_FLINCH_USE_RIGHT_ARM)
					ADD_WIDGET_BOOL ("NM_FLINCH_NEW_HIT", bNM_FLINCH_NEW_HIT)
					ADD_WIDGET_INT_SLIDER ("NM_FLINCH_TURN_TOWARDS", iNM_FLINCH_TURN_TOWARDS, 0, 10, 1 )
					ADD_WIDGET_INT_SLIDER ("Min ragdoll time", MIN_TASK_TIME, 1, 10000, 100 )
					ADD_WIDGET_INT_SLIDER ("Max ragdoll time", MAX_TASK_TIME, 1, 10000, 100 )
				STOP_WIDGET_GROUP() // "NM_Flinch"
				
				START_WIDGET_GROUP("NM Grab")
					START_NEW_WIDGET_COMBO ( )
						ADD_TO_WIDGET_COMBO ("Line grab")
						ADD_TO_WIDGET_COMBO ("Surface grab")
						ADD_TO_WIDGET_COMBO ("setup")
					STOP_WIDGET_COMBO("Grab type", gGrabType)
					ADD_WIDGET_BOOL("Reset to default", gresetoffsets)
						START_WIDGET_GROUP("Slope catch forces applied to ped")
							ADD_WIDGET_FLOAT_SLIDER ("Force.x",vImpulse.x,  -2000, 2000, 0.25)
							ADD_WIDGET_FLOAT_SLIDER ("Force.y",vImpulse.y,  -2000, 2000, 0.25)
							ADD_WIDGET_FLOAT_SLIDER ("Force.z",vImpulse.z,  -2000, 2000, 0.25)
							ADD_WIDGET_FLOAT_SLIDER ("Force vOffset.x",vOffset.x,  -2000, 2000, 0.25)
							ADD_WIDGET_FLOAT_SLIDER ("Force vOffset.y",vOffset.y,  -2000, 2000, 0.25)
							ADD_WIDGET_FLOAT_SLIDER (" Force vOffset.z",vOffset.z,  -2000, 2000, 0.25)
						STOP_WIDGET_GROUP()	
					ADD_WIDGET_FLOAT_SLIDER ("vOffset1.x (Red) ",vOffsetVec1.x,  -2000, 2000, 0.25)
					ADD_WIDGET_FLOAT_SLIDER ("vOffset1.y (Red)",vOffsetVec1.y,  -2000, 2000, 0.25)
					ADD_WIDGET_FLOAT_SLIDER ("vOffset1.z (Red)",vOffsetVec1.z,  -2000, 2000, 0.25)
					ADD_WIDGET_FLOAT_SLIDER ("vOffset2.x (Green)",vOffsetVec2.x,  -2000, 2000, 0.25)
					ADD_WIDGET_FLOAT_SLIDER ("vOffset2.y (Green)",vOffsetVec2.y,  -2000, 2000, 0.25)
					ADD_WIDGET_FLOAT_SLIDER ("vOffset2.z (Green)",vOffsetVec2.z,  -2000, 2000, 0.25)
					ADD_WIDGET_FLOAT_SLIDER ("vOffset3.x (blue) surface",vOffsetVec3.x,  -2000, 2000, 0.25)
					ADD_WIDGET_FLOAT_SLIDER ("vOffset3.y (blue) surface",vOffsetVec3.y,  -2000, 2000, 0.25)
					ADD_WIDGET_FLOAT_SLIDER ("vOffset3.z (blue) surface",vOffsetVec3.z,  -2000, 2000, 0.25)
					ADD_WIDGET_FLOAT_SLIDER ("vOffset4.x (white) surface",vOffsetVec4.x,  -2000, 2000, 0.25)
					ADD_WIDGET_FLOAT_SLIDER ("vOffset4.y (white) surface",vOffsetVec4.y,  -2000, 2000, 0.25)
					ADD_WIDGET_FLOAT_SLIDER ("vOffset4.z (white) surface",vOffsetVec4.z,  -2000, 2000, 0.25)		
					ADD_WIDGET_BOOL ("bNM_GRAB_A_LINE", bNM_GRAB_A_LINE)
					ADD_WIDGET_BOOL ("bNM_GRAB_BRACE_ONLY", bNM_GRAB_BRACE_ONLY)
					ADD_WIDGET_BOOL ("bNM_GRAB_SURFACE", bNM_GRAB_SURFACE)
					ADD_WIDGET_BOOL ("bNM_GRAB_USE_LEFT", bNM_GRAB_USE_LEFT)
					ADD_WIDGET_BOOL ("bNM_GRAB_USE_RIGHT", bNM_GRAB_USE_RIGHT)
					ADD_WIDGET_BOOL ("bNM_GRAB_DONT_LET_GO", bNM_GRAB_DONT_LET_GO)
					
					ADD_WIDGET_FLOAT_SLIDER ("fNM_GRAB_STRENGTH", fNM_GRAB_STRENGTH,  -1.0, 100.0, 0.01 )
					ADD_WIDGET_FLOAT_SLIDER ("fNM_GRAB_BODY_STIFFNESS", fNM_GRAB_BODY_STIFFNESS,  6, 16.0, 0.01 )
					ADD_WIDGET_FLOAT_SLIDER ("fNM_GRAB_REACH_ANGLE", fNM_GRAB_REACH_ANGLE,  0, 10.0, 0.1 )
					ADD_WIDGET_FLOAT_SLIDER ("fNM_GRAB_ONE_SIDE_REACH_ANGLE", fNM_GRAB_ONE_SIDE_REACH_ANGLE,  0.0, 10.0, 0.1 )
					ADD_WIDGET_FLOAT_SLIDER ("fNM_GRAB_DISTANCE", fNM_GRAB_DISTANCE, 0, 1, 0.1 )
					ADD_WIDGET_FLOAT_SLIDER ("fNM_GRAB_PULLUP_TIMER", fNM_GRAB_PULLUP_TIMER, 0, 1.0, 0.1 )
					ADD_WIDGET_FLOAT_SLIDER ("fNM_GRAB_HOLD_MAX_TIMER", fNM_GRAB_HOLD_MAX_TIMER, 0.0, 1.0, 0.1 )
					ADD_WIDGET_FLOAT_SLIDER ("fNM_GRAB_PULLUP_STRENGTH_LEFT", fNM_GRAB_PULLUP_STRENGTH_LEFT, 0.0, 10.0, 0.1 )
					ADD_WIDGET_FLOAT_SLIDER ("fNM_GRAB_PULLUP_STRENGTH_RIGHT", fNM_GRAB_PULLUP_STRENGTH_RIGHT, 0.0, 10.0, 0.1 )
					
					ADD_WIDGET_INT_SLIDER ("iNM_GRAB_INSTANCE_INDEX", iNM_GRAB_INSTANCE_INDEX, -1, 10, 1 )
					ADD_WIDGET_INT_SLIDER ("iNM_GRAB_INSTANCE_INDEX", iNM_GRAB_INSTANCE_INDEX, -1, 10, 1 )
					
					ADD_WIDGET_FLOAT_SLIDER ("distance b4 grab", gdistance_to_grab,  -720.0, 720.0, 0.1 )  
					//ADD_WIDGET_BOOL ("ENABLE_MOVE_OFFSET", ENABLE_MOVE_OFFSET)
					
					//ADD_WIDGET_FLOAT_SLIDER ("gtestZoom", gtestZoom,  0.0, 100.0, 0.1 )
					//ADD_WIDGET_FLOAT_SLIDER ("gtestPitch", gtestPitch,  -360, 360.0, 0.1 )
				//	ADD_WIDGET_FLOAT_SLIDER ("gtestPan", gtestPan,  -720.0, 720.0, 0.1 )
				//	ADD_WIDGET_FLOAT_SLIDER ("gVehicleZoom", gVehicleZoom,  0.0, 100.0, 0.1 )
				//	ADD_WIDGET_FLOAT_SLIDER ("gVehiclePitch", gVehiclePitch, -360, 360.0, 0.1 )
				//	ADD_WIDGET_FLOAT_SLIDER ("gVehiclePan", gVehiclePan,  -720.0, 720.0, 0.1 ) 
					
					ADD_WIDGET_INT_SLIDER ("Min ragdoll time", MIN_TASK_TIME, 1, 10000, 100 )
					ADD_WIDGET_INT_SLIDER ("Max ragdoll time", MAX_TASK_TIME, 1, 10000, 100 )
				
				STOP_WIDGET_GROUP() // "NM_Grab"
					
				START_WIDGET_GROUP("NM_Balance")
					ADD_WIDGET_INT_SLIDER ("Min ragdoll time", MIN_TASK_TIME, 1, 10000, 100 )
					ADD_WIDGET_INT_SLIDER ("Max ragdoll time", MAX_TASK_TIME, 1, 10000, 100 )
				STOP_WIDGET_GROUP() // "NM_Balance"
				
				START_WIDGET_GROUP("NM Bind pose transition")
					ADD_WIDGET_BOOL("Blend back to animation", bDO_BLEND_FROM_NM)
				STOP_WIDGET_GROUP() // "NM Bind pose transition"
				
				START_WIDGET_GROUP("NM Shooting while attached")
					ADD_WIDGET_INT_SLIDER("iNM_SHOOTINGWHILEATTACHED_INSTANCE_PART_INDEX", iNM_SHOOTINGWHILEATTACHED_INSTANCE_PART_INDEX, 0, 50, 1 )
					//ADD_WIDGET_BOOL("Ragdoll collides with van?", bNM_RAGDOLL_COLLIDES_WITH_VAN)
					ADD_WIDGET_BOOL("animPose: show anim accuracy", bNM_ANIMPOSE_DEBUGANIMMATCH)
				STOP_WIDGET_GROUP() // "NM Shooting while attached"
				START_WIDGET_GROUP("NM Walking down hill")
					ADD_WIDGET_FLOAT_SLIDER("movement speed", WALK_DOWN_HILL_DYNAMIC_MOVE_TYPE, 1.0, 3.0, 0.01)
				STOP_WIDGET_GROUP() // "NM Walking down hill"
				
				START_WIDGET_GROUP("NM Teeter")
					ADD_WIDGET_FLOAT_SLIDER("Ped's distance to edge", fPedEdgeDist, 0.0, 3.0, 0.1)
				STOP_WIDGET_GROUP() // "NM Teeter"
				
				START_WIDGET_GROUP("NM Slide")
					ADD_WIDGET_FLOAT_SLIDER("Ped heading", gShot2OrientationOffset, 0.0, 360.0, 0.01)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("NM Dangle")
					ADD_WIDGET_FLOAT_SLIDER("Hook Velocity x", vHookVelocity.x, -1.0, 1.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Hook Velocity y", vHookVelocity.y, -1.0, 1.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Hook Velocity z", vHookVelocity.z, -1.0, 1.0, 0.1)
				STOP_WIDGET_GROUP()
				
				
				START_WIDGET_GROUP("Shot Against Wall")
					ADD_WIDGET_FLOAT_SLIDER("Ped's distance from wall", fPedWallDist, 0.0, 2.0, 0.01)
				STOP_WIDGET_GROUP() // "Shot Against Wall"
				
				START_WIDGET_GROUP("Slung Over Shoulder")
					ADD_WIDGET_FLOAT_SLIDER("vCarriedPos.x", vCarriedPos.x, -1.0, 1.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("vCarriedPos.y", vCarriedPos.y, -1.0, 1.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("vCarriedPos.z", vCarriedPos.z, -1.0, 1.0, 0.01)
					
					ADD_WIDGET_FLOAT_SLIDER("vCarriedRot.x", vCarriedRot.x, -360.0, 360.0, 1.0)
					ADD_WIDGET_FLOAT_SLIDER("vCarriedRot.y", vCarriedRot.y, -360.0, 360.0, 1.0)
					ADD_WIDGET_FLOAT_SLIDER("vCarriedRot.z", vCarriedRot.z, -360.0, 360.0, 1.0)
				STOP_WIDGET_GROUP() // "Slung Over Shoulder"
				
				START_WIDGET_GROUP("NM Truck Bed")
					ADD_WIDGET_FLOAT_SLIDER("Vehicle speed", gVehicleSpeed, -50.0, 50.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Ped offset X", vOffset.x, -50.0, 50.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Ped offset Y", vOffset.y, -50.0, 50.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Ped offset Z", vOffset.z, -50.0, 50.0, 0.01)
					ADD_WIDGET_BOOL("Use flatbed truck", bFlatbedTruck)
				STOP_WIDGET_GROUP() // "NM Truck Bed"
				
				START_WIDGET_GROUP("NM Cover")
					ADD_WIDGET_FLOAT_SLIDER("Ped offset X", vOffset.x, -50.0, 50.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Ped offset Y", vOffset.y, -50.0, 50.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Ped offset Z", vOffset.z, -50.0, 50.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Ped ragdoll offset X", vOffsetVec1.x, -50.0, 50.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Ped ragdoll offset Y", vOffsetVec1.y, -50.0, 50.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Ped ragdoll offset Z", vOffsetVec1.z, -50.0, 50.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Ped target offset X", vOffsetVec2.x, -50.0, 50.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Ped target offset Y", vOffsetVec2.y, -50.0, 50.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Ped target offset Z", vOffsetVec2.z, -50.0, 50.0, 0.01)
					ADD_WIDGET_INT_SLIDER("Electrocute time (milliseconds)", nElectrocuteTime, 0, 65534, 10)
					ADD_WIDGET_INT_SLIDER("Wait time (milliseconds) between electrocute and cover", nElectrocuteWaitTime, 0, 100000, 10)
					ADD_WIDGET_BOOL("Ragdoll on collision", bRagdollOnCollision)
				STOP_WIDGET_GROUP() // "NM Cover"
				
				//create widget in here	
				
				
			STOP_WIDGET_GROUP() // "NM"
		STOP_WIDGET_GROUP() // "Tests"
ENDPROC

FUNC Model_Names PED_MODEL_SELECTOR()
	MODEL_NAMES gPedModel
	
	SWITCH(pedidentifier)
		CASE 0
			gPedModel = S_M_M_DOCKWORK_01
		BREAK
		
		CASE 1
			gPedModel = S_F_M_SWEATSHOP_01
		BREAK
	ENDSWITCH
	
	return gPedModel
ENDFUNC

FUNC Model_Names PED_MODEL_SELECTOR_SHOT_2(INT pedType)
	MODEL_NAMES gPedModel
	
	SWITCH(pedType)
		CASE 0
			gPedModel = S_M_M_DOCKWORK_01
		BREAK
		
		CASE 1
			gPedModel = S_F_M_SWEATSHOP_01
		BREAK
		
		CASE 2
			gPedModel = S_M_M_CHEMSEC_01
		BREAK
		
		CASE 3
			gPedModel = S_M_Y_COP_01
		BREAK
		
		CASE 4
			gPedModel = S_M_Y_SWAT_01
		BREAK

		CASE 5
			gPedModel = A_C_ROTTWEILER
		BREAK
	ENDSWITCH
	
	return gPedModel
ENDFUNC



FUNC WEAPON_TYPE SELECT_WEAPON_TYPE ()
	weapon_type current_weapon = WEAPONTYPE_PISTOL
	SWITCH(gWeapon)
 		CASE 0
			current_weapon = WEAPONTYPE_PISTOL
		BREAK
		
		CASE 1
			current_weapon = WEAPONTYPE_ASSAULTRIFLE
		BREAK
			
		CASE 2
			current_weapon = WEAPONTYPE_PUMPSHOTGUN
		BREAK
		
		CASE 3
			current_weapon = WEAPONTYPE_STUNGUN
		BREAK
		
		CASE 4 
			current_weapon = WEAPONTYPE_UNARMED
		BREAK	
		
		
	ENDSWITCH
	
	return current_weapon
	
ENDFUNC

PROC NM_TempControlSkiCam(peds_struct &peds_l[], CAMERA_INDEX cam)

    vector camera_pos
    
        if DOES_CAM_EXIST(cam)
            IF IS_CAM_ACTIVE (cam)
                    int stick_left_x, stick_right_x, stick_left_y, stick_right_y
					
                    GET_POSITION_OF_ANALOGUE_STICKS (PAD1, stick_left_x, stick_left_y, stick_right_x, stick_right_y )
                            IF NOT IS_PED_INJURED(peds_l[camped].ped  )
								IF NOT IS_PED_RAGDOLL (peds_l[camped].ped  )
									if not 	IS_PED_GETTING_UP(peds_l[camped].ped )
										POINT_CAM_AT_ENTITY(cam, peds_l[camped].ped, <<0.0, 0.0, 0.0>> )
										
										IF (stick_right_x >= -128) AND (stick_right_x < 0)	
												gCamPan     -= 2
										ENDIF
										
										IF (stick_right_x <= 128) AND (stick_right_x > 0)
											gCamPan += 2
										ENDIF
										
										IF (stick_right_y >= -128) AND (stick_right_y < 0)
											IF gCamPitch < -3
												gCamPitch += 2
											ENDIF
										
										ENDIF
										
										IF (stick_right_y <= 128) AND (stick_right_y > 0)
											IF gCamPitch > -178
												gCamPitch  -= 2
											ENDIF
										ENDIf

										IF IS_BUTTON_PRESSED (PAD1, LEFTSHOULDER2)
											if gCamzoom > gCamZoomMIn
												gCamzoom -= 0.15
											ENDIF
										ENDIF
										
										IF IS_BUTTON_PRESSED (PAD1, RIGHTSHOULDER2)
											IF gCamzoom < gCamZoomMax
												gCamzoom += 0.15
											ENDIF
										ENDIf

										IF NOT IS_PED_INJURED(peds_l[camped].ped  )
											IF NOT IS_PED_RAGDOLL (peds_l[camped].ped  )
												POINT_CAM_AT_ENTITY(cam, peds_l[camped].ped, <<0.0, 0.0, 0.0>> )
												camera_pos =  GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS (peds_l[camped].ped, SphericalCamCoords (gCamzoom, gCamPitch, gCamPan))
												SET_CAM_COORD (cam, camera_pos)
											ENDIF	
										ENDIF
		
									ENDIF	
                            	ENDIF
							ENDIF
                        
            ENDIF
        ENDIF
ENDPROC



ENUM CAM_FLAGS
	SELECT_PED_TO_LOOK_AT,
	CREATE_TRACKING_CAM,
	POINT_TRACKING_CAM_AT_PED,
	ACTIVATE_FIXED_CAM,
	DEACTIVATE_PED_CAM,
	WAIT_FOR_CAM_INPUT
ENDENUM

CAM_FLAGS CAM_STATUS = WAIT_FOR_CAM_INPUT

PROC CAM_MANAGER (CAMERA_INDEX &pedcam, CAMERA_INDEX &fixedcam,int number_of_peds, peds_struct &peds_l[] )

	bool   SearchingForPeds = TRUE, found_ped = FALSE
	int FallOutOfLoop = 0
		
		IF bAutoSwitchToTrackCam 
			CAM_STATUS = SELECT_PED_TO_LOOK_AT
		ENDIF	
		
		IF (IS_BUTTON_JUST_PRESSED (Pad1, DPADUP) )
			//swap between the fised and ped cam
			IF DOES_CAM_EXIST(fixedcam)
				IF IS_CAM_RENDERING (fixedcam)
						CAM_STATUS = SELECT_PED_TO_LOOK_AT
				ELSE
					CAM_STATUS =  ACTIVATE_FIXED_CAM
				ENDIF
			ELSE
				IF DOES_CAM_EXIST(pedCam )
					IF IS_CAM_RENDERING (pedCam)
						CAM_STATUS = DEACTIVATE_PED_CAM
					ELSE
						CAM_STATUS = SELECT_PED_TO_LOOK_AT
					ENDIF
				ENDIF
			ENDIF	
		ENDIF

	
		SWITCH CAM_STATUS
			CASE SELECT_PED_TO_LOOK_AT
				bAutoSwitchToTrackCam = FALSE

				WHILE SearchingForPeds  
					FallOutOfLoop+= 1
					
					//this terminates the while loop if there are no valid peds.
					IF FallOutOfLoop = number_of_peds
						SearchingForPeds = FALSE
					ENDIF
						
					IF  camped = number_of_peds -1 
						camped = 0
					ENDIF
                	
					IF NOT IS_PED_INJURED(peds_l[camped].ped  )
						found_ped = true
						SearchingForPeds = FALSE
					ELSE
						camped += 1 
					ENDIF
			
				ENDWHILE
				
				//no valid ped was found bail out here
				IF NOT found_ped
					CAM_STATUS = WAIT_FOR_CAM_INPUT
				ELSE
					CAM_STATUS = CREATE_TRACKING_CAM
				ENDIF
			BREAK
		
		
			CASE CREATE_TRACKING_CAM 
					IF NOT DOES_CAM_EXIST (pedcam )
                        pedcam = CREATE_CAM ("DEFAULT_SCRIPTED_CAMERA")
							IF NOT IS_PED_INJURED(peds_l[camped].ped  )
								//IF NOT IS_PED_RAGDOLL (peds_l[camped].ped  )
									POINT_CAM_AT_ENTITY(pedcam, peds_l[camped].ped, <<0.0, 0.0, 0.0>> )
									SET_CAM_COORD (pedcam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS (peds_l[camped].ped, SphericalCamCoords (gCamzoom, gCamPitch, gCamPan)))
								//ENDIF	
							ENDIF
                    ENDIF
			
					SET_CAM_ACTIVE (pedcam, true)
			
					IF camscope = 0 
						RENDER_SCRIPT_CAMS     (TRUE, FALSE)
						camscope += 1
					ENDIF
						
					IF DOES_CAM_EXIST (Fixedcam)
						SET_CAM_ACTIVE (Fixedcam, FALSE)
					ENDIF
						
						CAM_STATUS =  POINT_TRACKING_CAM_AT_PED
			BREAK
		
			CASE POINT_TRACKING_CAM_AT_PED
				IF Bscenario_running
					NM_TempControlSkiCam( peds_l, pedcam)
				ELSE
					CAM_STATUS =  WAIT_FOR_CAM_INPUT
				ENDIF
			BREAK
			
			CASE ACTIVATE_FIXED_CAM
				SET_CAM_ACTIVE(fixedcam, true)
					IF camscope = 0 
						RENDER_SCRIPT_CAMS     (TRUE, FALSE)
						camscope += 1
					ENDIF		
					
					IF DOES_CAM_EXIST (pedcam)
						SET_CAM_ACTIVE (pedcam, FALSE)
					ENDIF
					
					CAM_STATUS =  WAIT_FOR_CAM_INPUT
			BREAK
			
			
			CASE DEACTIVATE_PED_CAM
					IF camscope = 1 
						RENDER_SCRIPT_CAMS     (FALSE, FALSE)
						camscope -= 1
					ENDIF
					
					SET_CAM_ACTIVE(pedcam, FALSE)
					
					CAM_STATUS =  WAIT_FOR_CAM_INPUT
			BREAK
			
			CASE WAIT_FOR_CAM_INPUT
			
			BREAK
			
		ENDSWITCH

ENDPROC


FUNC  EXPLOSION_TAG SELECT_EXPLOSION_TYPE()
	EXPLOSION_TAG Current_Explosions
	
	SWITCH ExplosionTag
		CASE 0
			Current_Explosions = EXP_TAG_ROCKET
		BREAK

		CASE 1
			Current_Explosions = EXP_TAG_GRENADE
		BREAK
		
		CASE 2
			Current_Explosions = EXP_TAG_MOLOTOV
		BREAK

/*		
		CASE 3
			Current_Explosions = EXP_TAG_HI_OCTANE
		BREAK

		CASE 4
			Current_Explosions = EXP_TAG_VEHICLE
		BREAK
		
		CASE 5
			Current_Explosions = EXP_TAG_PLANE
		BREAK
		
		CASE 6
			Current_Explosions = EXP_TAG_PETROL_PUMP
		BREAK
*/		
	ENDSWITCH
	
	RETURN Current_Explosions
	
ENDFUNC

// Functions end: WIDGET

//Mission flow House keeping

//PUPROSE: REmoves all the scenarios and resets all the relationships
	PROC Cleanup_Scenario_Entities ()
	
	STOP_FIRE_IN_RANGE( PlayerStartPos, 100)
	
	if not IS_PED_INJURED(scplayer)
		IF IS_PED_SITTING_IN_ANY_VEHICLE (scplayer)
			SET_ENTITY_COORDS (scplayer, PlayerStartPos, FALSE)
		ENDIF
	ENDIF
	
	int index = 0
		for index = 0 to MAX_NUMBER_OF_PEDS -1 	
			REMOVE_BLIP(TestPeds[index].PedBlip)
			
			CLEAN_UP_PED_ENTITIES (TestPeds[index].ped)
		ENDFOR 
		
		for index = 0 to MAX_NUMBER_OF_VEHICLES -1
			CLEAN_UP_VEHICLE_ENTITIES (TestVehicles[index].Vehicle )
		ENDFOR

		for Index = 0 to MAX_NUMBER_OF_OBJECTS -1
			CLEAN_UP_OBJECT_ENTITIES(TestObject[index].Object)
		ENDFOR	
		//Set_Gang_Relationships (FALSE)	
	ENDPROC

PROC Terminate_test_script ()
	if IS_KEYBOARD_KEY_JUST_PRESSED (KEY_S)
		CLEAR_PRINTS ()
		CLEAR_HELP ()
		Cleanup_Scenario_Entities ()
		Temp_cleanup_scenario_cams ()
		SET_PLAYER_COLISION(scplayer, true)
		IF NOT IS_PED_INJURED (scplayer)
				SET_ENTITY_COORDS (scplayer, GET_PLAYER_START_POS () )
		ENDIF
		
		//TERMINATE_THIS_THREAD ()
		
	ENDIF
ENDPROC

//copies the current selection from the xml menu into a new var. gCurrent selction is then set to 1000 so that the it forces the test state into the default state 

//END: House Keeping

CONST_INT LEFT_FOOT				0
CONST_INT RIGHT_FOOT			1

CONST_INT LEFT_KNEE				2
CONST_INT RIGHT_KNEE 			3

CONST_INT LEFT_HAND				4
CONST_INT RIGHT_HAND			5

CONST_INT STOMACH 				  6
CONST_INT CHEST 					7

CONST_INT LEFT_SHOULDER 	8
CONST_INT RIGHT_SHOULDER 	9
CONST_INT HEAD 						  10

//Test Specific Functions:
FUNC VECTOR  SELECT_PED_COMPONENT (PED_INDEX ped)
	VECTOR ped_offset = <<0.0, 0.0, 0.0>>
	
	IF IS_BUTTON_JUST_PRESSED (pad1, CIRCLE )
		PedComponent++
		IF PedComponent > 10
			PedComponent = 0
		ENDIF
	ENDIF
	
	IF IS_BUTTON_JUST_PRESSED (pad1, TRIANGLE )
		PedComponent--
		IF PedComponent < 0
			PedComponent = 10
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED (ped)
		SWITCH PedComponent
			CASE LEFT_KNEE
				ped_offset =  GET_PED_BONE_COORDS (ped, BONETAG_L_CALF, <<0.0, 0.0, 0.0>>)
			BREAK
			
			CASE RIGHT_KNEE
				ped_offset = GET_PED_BONE_COORDS (ped, BONETAG_R_CALF, <<0.0, 0.0, 0.0>>)
			BREAK
			
			CASE LEFT_SHOULDER
				ped_offset = GET_PED_BONE_COORDS (ped, BONETAG_L_UPPERARM, <<0.0, 0.0, 0.0>>)
			BREAK
			
			CASE RIGHT_SHOULDER
					ped_offset = GET_PED_BONE_COORDS (ped, BONETAG_R_UPPERARM, <<0.0, 0.0, 0.0>>)
			BREAK
			
			CASE HEAD
				ped_offset = GET_PED_BONE_COORDS (ped, BONETAG_HEAD, <<0.0, 0.0, 0.0>>)
			BREAK
			
			CASE STOMACH
				ped_offset = GET_PED_BONE_COORDS (ped, BONETAG_PELVIS, <<0.0, 0.0, 0.0>>)
			BREAK
			
			CASE CHEST
				ped_offset = GET_PED_BONE_COORDS (ped, BONETAG_SPINE2, <<0.0, 0.0, 0.0>>)
			BREAK
			
			CASE RIGHT_FOOT
				ped_offset = GET_PED_BONE_COORDS (ped, BONETAG_R_FOOT, <<0.0, 0.0, 0.0>>)
			BREAK
			
			CASE LEFT_FOOT
				ped_offset = GET_PED_BONE_COORDS (ped, BONETAG_L_FOOT, <<0.0, 0.0, 0.0>>)
			BREAK
			
			CASE RIGHT_HAND
				ped_offset = GET_PED_BONE_COORDS (ped, BONETAG_R_HAND, <<0.0, 0.0, 0.0>>)
			BREAK
			
			CASE LEFT_HAND
				ped_offset = GET_PED_BONE_COORDS (ped, BONETAG_L_HAND, <<0.0, 0.0, 0.0>>)
			BREAK
			
			
		ENDSWITCH
	ENDIF
	
	RETURN ped_offset
	
ENDFUNC
//SHOT RUN
FUNC SEQUENCE_INDEX FOLLOW_ROUTE( RouteStruct &TargetRoute[], int NumberofPointsInRoute, int DYNAMIC_MOVE_TYPE)
		int index 
		BOOL BHaveValidNode = FALSE
		VECTOR TempNode
		
		//This allows us to pass a sequnce to a ped using 7 points for maximun testing. Iess than 7 nodes can be used, and the last valid node is copied into the remaining array elements.
		//Note all nodes must be in order there can be no gaps between them ie node 1 valid, 2 valid, 3 invalid is ok but not 1 valid, 2 invalid, 3  valid 
		//search our node array until we find a invalid array point
		for index = 0 to MAX_NUMBER_OF_NODES -1
			IF TargetRoute[index].RouteNodeState = FALSE
				IF index <> 0
					TempNode = TargetRoute[index - 1].RouteNode	//grab the previous point which is valid assuming that all nodes are sequential
					BHaveValidNode = TRUE
				ELSE
					SCRIPT_ASSERT ("Passed in sequnce with no valid nodes ")
				ENDIF
			ENDIF
			
			//copy this valid node into the remaing array elements
			IF (BHaveValidNode)
					TargetRoute[index].RouteNode = TempNode	
			ENDIF
				
		ENDFOR	
		SEQUENCE_INDEX TEST_SEQUENCE
		
		if ((NumberofPointsInRoute <= 7) and (NumberofPointsInRoute > 0) )
			
			OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
				for index = 0 to NumberofPointsInRoute-1
					TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[index].routenode,   DYNAMIC_MOVE_TYPE,  -1, 1)
				ENDFOR
			CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		ELSE
			SCRIPT_ASSERT ("max number of routes is 7 or has none making a route with one point to the origin")
			TargetRoute[0].routenode = <<1.0, 1.0, 1.0>>
			TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, TargetRoute[0].routenode,   DYNAMIC_MOVE_TYPE,  -1, 1)
		ENDIF
		
		RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX TEST_ROUTE_SHOT_2 (float MOVE_BLEND_RATIO, bool bForward, bool bLeft, bool bRight, bool bBackward, float moveDistance )

//		// strafing and backpedaling require aiming an equipped weapon
//		IF bLeft OR bRight OR bBackward
//			IF gWeapon >= 4
//				gWeapon = 1
//				gLastWeapon = gWeapon
//				GIVE_WEAPON_TO_PED(TestPeds[0].ped, SELECT_WEAPON_TYPE(), 300, TRUE)
//			ENDIF		
//			IF NOT bAimWeapon
//				bAimWeapon = TRUE
//				bWasAimingWeapon = TRUE
//			ENDIF
//		ENDIF
		
		VECTOR front
		VECTOR side
		VECTOR up
		VECTOR pos
		IF NOT IS_PED_INJURED (TestPeds[0].ped)
			GET_ENTITY_MATRIX(TestPeds[0].ped, front, side, up, pos)
		ENDIF
		IF bForward
			pos = pos + front * moveDistance
		ENDIF
		IF bLeft
			pos = pos + side * moveDistance		
		ENDIF		
		IF bRight
			pos = pos - side * moveDistance		
		ENDIF		
		IF bBackward
			pos = pos - front * moveDistance		
		ENDIF

		SEQUENCE_INDEX TEST_SEQUENCE
			OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
				IF bAimWeapon AND gWeapon < 4
					VECTOR aimCoords
					aimCoords = pos + front * 100.0
					TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, pos, aimCoords, MOVE_BLEND_RATIO, TRUE)				
				ELSE
					TASK_GO_STRAIGHT_TO_COORD(NULL, pos, MOVE_BLEND_RATIO, -1)
				ENDIF	
			CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		return 	TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX TEST_ROUTE_ACTIVE_OPPONENT (float MOVE_BLEND_RATIO)
		VECTOR MOVE_TARGET_1=<<-4.75, -30.63, 0.0>>
		VECTOR MOVE_TARGET_2=<<-4.75, -22.63, 0.0>>
		VECTOR MOVE_TARGET_3=<<-12.75, -22.63, 0.0>>
		VECTOR MOVE_TARGET_4=<<-12.75, -30.63, 0.0>>
		VECTOR SHOOT_TARGET=<<-4.75, 0.63, 0.0>>
		FLOAT TARGET_DISTANCE = 0.0
		FLOAT SLOW_DISTANCE = 0.0
		INT NUM_LOOPS = 3
		INT index = 0
		SEQUENCE_INDEX TEST_SEQUENCE
			OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
				FOR index = 0 to NUM_LOOPS - 1
					TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, MOVE_TARGET_1, SHOOT_TARGET, MOVE_BLEND_RATIO, TRUE, TARGET_DISTANCE, SLOW_DISTANCE)
					TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, MOVE_TARGET_2, SHOOT_TARGET, MOVE_BLEND_RATIO, TRUE, TARGET_DISTANCE, SLOW_DISTANCE)
					TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, MOVE_TARGET_3, SHOOT_TARGET, MOVE_BLEND_RATIO, TRUE, TARGET_DISTANCE, SLOW_DISTANCE)
					TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, MOVE_TARGET_4, SHOOT_TARGET, MOVE_BLEND_RATIO , TRUE, TARGET_DISTANCE, SLOW_DISTANCE)
				ENDFOR
			CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		return 	TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX TEST_ROUTE (float MOVE_BLEND_RATIO)
		SEQUENCE_INDEX TEST_SEQUENCE
			OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
					//TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, <<-4.75, -30.63, 6.13>>, INT_TO_ENUM (PEDMOVESTATE , (DYNAMIC_MOVE_TYPE +  2)),  -1, 1 )
					//TASK_FOLLOW_NAV_MESH_TO_COORD (NULL, <<-2.69, 4.20, 6.13>> ,   INT_TO_ENUM (PEDMOVESTATE , (DYNAMIC_MOVE_TYPE +  2)),  -1)
					TASK_GO_STRAIGHT_TO_COORD  (NULL, Route[1].RouteNode, MOVE_BLEND_RATIO, -1)
			CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		return 	TEST_SEQUENCE
ENDFUNC
// Like TEST_ROUTE(), but for skiing. Allows slalom routes to be set up via RAG widgets.
FUNC SEQUENCE_INDEX SKI_TEST_ROUTE(VECTOR& vRoute[], float MOVE_BLEND_RATIO)
		SEQUENCE_INDEX TEST_SEQUENCE
			OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
					TASK_GO_STRAIGHT_TO_COORD(NULL, vRoute[1], MOVE_BLEND_RATIO, -1)
					TASK_GO_STRAIGHT_TO_COORD(NULL, vRoute[2], MOVE_BLEND_RATIO, -1)
					TASK_GO_STRAIGHT_TO_COORD(NULL, vRoute[3], MOVE_BLEND_RATIO, -1)
					TASK_GO_STRAIGHT_TO_COORD(NULL, vRoute[4], MOVE_BLEND_RATIO, -1)
					TASK_GO_STRAIGHT_TO_COORD(NULL, vRoute[5], MOVE_BLEND_RATIO, -1)
					TASK_GO_STRAIGHT_TO_COORD(NULL, vRoute[6], MOVE_BLEND_RATIO, -1)
			CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		return 	TEST_SEQUENCE
ENDFUNC

//Player bump
FUNC SEQUENCE_INDEX FOLLOW_NAVMESH_TO_COORD_SEQ ( VECTOR L_Target_Pos, float MOVE_BLEND_RATIO)
		
		SEQUENCE_INDEX TEST_SEQUENCE
		OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
				TASK_GO_STRAIGHT_TO_COORD  (NULL, L_Target_Pos, MOVE_BLEND_RATIO  )
		CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)

		RETURN TEST_SEQUENCE
ENDFUNC

FUNC SEQUENCE_INDEX DRIVE_TO_POINT_SEQUENCE  ( VEHICLE_INDEX veh, vector target, float speed)
	SEQUENCE_INDEX TEST_SEQUENCE
	
	OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
		TASK_FLUSH_ROUTE()
		TASK_EXTEND_ROUTE(target )
		TASK_DRIVE_POINT_ROUTE_ADVANCED(NULL, veh, speed, DRIVINGSTYLE_STRAIGHTLINE, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_PLOUGHTHROUGH )
		TASK_DRIVE_POINT_ROUTE (NULL, veh, speed )
	CLOSE_SEQUENCE_TASK(TEST_SEQUENCE)
	
	RETURN TEST_SEQUENCE
ENDFUNC

PROC createDebugVehicle(MODEL_NAMES model, vehicle_index &veh )

    VECTOR vehiclePosition
    FLOAT vehicleHeading
    BOOL playerWasInCar = FALSE
    
    IF IS_MODEL_IN_CDIMAGE(model)
        REQUEST_MODEL(model)
        WHILE NOT HAS_MODEL_LOADED(model)
            WAIT(0)
            REQUEST_MODEL(model)
        ENDWHILE
        
        IF IS_PLAYER_PLAYING(theplayerindex)   
        
            IF DOES_ENTITY_EXIST(veh)
            
                IF NOT IS_ENTITY_DEAD(veh)
                    IF IS_PED_IN_VEHICLE(scplayer, veh)
                        vehiclePosition = GET_ENTITY_COORDS(veh)
                        vehicleHeading  = GET_ENTITY_HEADING(veh)
                        SET_ENTITY_COORDS(scplayer, <<vehiclePosition.x, vehiclePosition.y, vehiclePosition.z+4.0>>, FALSE)
                        playerWasInCar = TRUE
                    ENDIF
                ENDIF
                    
                DELETE_VEHICLE(veh)
            ENDIF
            
            IF NOT playerWasInCar
                vehiclePosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scplayer, << 0.0, 3.5, 0.0 >>)
                vehicleHeading  = (GET_ENTITY_HEADING(scplayer) + 90.0)
            ENDIF
        
            veh = CREATE_VEHICLE(model, vehiclePosition, vehicleHeading)
            SET_VEHICLE_NEEDS_TO_BE_HOTWIRED(veh, FALSE)
            SET_VEHICLE_ENGINE_ON (veh, true, true)
            IF playerWasInCar
                SET_PED_INTO_VEHICLE(scplayer,  veh, VS_DRIVER) 
            ENDIF
            
            SET_MODEL_AS_NO_LONGER_NEEDED(model)
        
        ENDIF   
    ENDIF

ENDPROC

PROC UPDATE_VECTOR_FROM_INPUT ( float &zoom, float &pitch , float &pan,  bool update_pan = true , bool update_pitch = true,bool update_zoom = true )
	 int stick_left_x, stick_right_x, stick_left_y, stick_right_y

  	
  	GET_POSITION_OF_ANALOGUE_STICKS (PAD1, stick_left_x, stick_left_y, stick_right_x, stick_right_y )
    
	if update_pan
		IF (stick_left_x >= -128) AND (stick_left_x < 0)
			pan    -= 2
		ENDIF
    
		IF (stick_left_x <= 128) AND (stick_left_x > 0)
			pan += 2
		ENDIF
    ENDIF
	
	IF update_pitch 
		IF (stick_left_y >= -128) AND (stick_left_y < 0) 
			IF pitch< -3
				pitch += 2
			ENDIF
		ENDIF
		
		IF (stick_left_y <= 128) AND (stick_left_y > 0)
			IF pitch > -178
				pitch -= 2
			ENDIF
		ENDIf
	ENDIF
	
	IF update_zoom
		IF IS_BUTTON_PRESSED (PAD1, LEFTSHOULDER1)
			if zoom> gMinZoom
				zoom -= gZoomspeed
			ENDIF
		ENDIF
		
		IF IS_BUTTON_PRESSED (PAD1, RIGHTSHOULDER1)
			IF zoom < gMaxZoom
				zoom += gZoomspeed
			ENDIF
		ENDIf
	ENDIF
	
ENDPROC 

FUNC VECTOR GET_POINT_AROUND_PED_IN_SPHERICAL_COORDS (PED_INDEX ped, float &zoom, float &pitch, float &pan, bool bUseinput = true,  bool update_pan = true , bool update_pitch = true,bool update_zoom = true )
	
  vector world_pos = <<0.0, 0.0, 0.0>>
  	
	if bUseinput
		UPDATE_VECTOR_FROM_INPUT ( zoom, pitch, pan, update_pan  , update_pitch , update_zoom)
	ENDIF
	
    if DOES_ENTITY_EXIST (ped)
        IF NOT IS_PED_INJURED(ped)
            world_pos =  GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS (ped, SphericalCamCoords  (zoom, pitch, pan))
        ENDIF
    ENDIF

	return world_pos
ENDFUNC 

FUNC VECTOR GET_POINT_AROUND_VEHICLE_IN_SPHERICAL_COORDS (VEHICLE_INDEX veh, float &zoom, float &pitch, float &pan, bool bUseinput = true, bool update_pan = true , bool update_pitch = true,bool update_zoom = true )

	 vector world_pos = <<0.0, 0.0, 0.0>>
		if bUseinput
			UPDATE_VECTOR_FROM_INPUT ( zoom, pitch, pan, update_pan  , update_pitch , update_zoom)
		ENDIF
		
    if DOES_ENTITY_EXIST (veh)
        IF IS_VEHICLE_DRIVEABLE (veh)
            world_pos =  GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS (veh, SphericalCamCoords  (zoom, pitch, pan))
        ENDIF
    ENDIF

	return world_pos
ENDFUNC


//These enums must match with the TestNmMenu.xml

ENUM Scenarios
	NM_SHOT_2 = 0, 
	NM_RUN_SHOT = 1,
	NM_SHOT_UNDERWATER = 3,
	NM_BUMPED_BY_PLAYER = 4, 
	NM_BUMPED_BY_VEHICLE = 5,
	NM_FALLING_FROM_HEIGHT = 6,
	NM_STANDING_ON_MOVING_VEHICLE = 7,
	NM_JUMPING_FROM_VEHICLE = 8,
	NM_GRAB_VEHICLE_DOOR = 9,
	NM_EXPLOSION = 10,
	NM_ON_FIRE = 11,
	NM_SHOCK_EVENT_REACTION,
	NM_WATER_CANON,
	NM_RELAX_TEST,
	NM_DYING,
	NM_FLINCH,
	NM_SHOT_BUILDING_LEDGE_RAIL_5M,
	NM_SHOT_BUILDING_LEDGE_RAIL_10M,
	NM_SHOT_BUILDING_LEDGE_RAIL_15M,
	NM_SHOT_BUILDING_LEDGE_RAIL_20M,
	NM_SHOT_BUILDING_LEDGE_2M,
	NM_SHOT_BUILDING_LEDGE_5M,
	NM_SHOT_BUILDING_LEDGE_10M,
	NM_SHOT_BUILDING_LEDGE_15M,
	NM_SHOT_BUILDING_LEDGE_20M,
	NM_SLOPE, 
	NM_SLOPE_CATCH  = 27,
	NM_BALANCE_TEST = 28,
	NM_BINDPOSE_TEST = 29,
	NM_WALKING_DOWN_HILL = 36,
	NM_TEETER_20M = 38,
	NM_ATTACH_IDLE = 39, 
	NM_ATTACH_UNDERNEATH= 40,
	NM_PUNCHED_LOW_HEALTH = 41,
	NM_SHOT_BUILDING_LEDGE_RAIL_2M = 42,
	NM_DANGLE_FROM_MEATHOOK = 43,
	NM_SLUNG_OVER_SHOULDER = 44,
	NM_SHOT_AGAINST_WALL = 45,
	NM_SLIDE_TEST = 47,	
	NM_WARP_TO_INTERIOR = 48,
	NM_PLAYER_BUMPED_BY_VEHICLE = 49,
	NM_TRUCK_BED = 50,
	NM_COVER = 51,
	NM_QUIT = 52,
	DefaultTest = 1000			//do not add below here 
ENDENUM

	bool bIsTestRunning = FALSE 

	PROC RUN_LOCAL_HELP_TEXT ()
		SWITCH int_to_enum (scenarios, gcurrentselection)
			CASE NM_SHOT_UNDERWATER
			CASE NM_RUN_SHOT
			CASE NM_DANGLE_FROM_MEATHOOK
			CASE NM_SLUNG_OVER_SHOULDER
			case NM_SHOT_AGAINST_WALL
			CASE NM_SHOT_BUILDING_LEDGE_RAIL_2M
			CASE NM_SHOT_BUILDING_LEDGE_RAIL_5M
			CASE NM_SHOT_BUILDING_LEDGE_RAIL_10M
			CASE NM_SHOT_BUILDING_LEDGE_RAIL_15M
			CASE NM_SHOT_BUILDING_LEDGE_RAIL_20M
			CASE NM_SHOT_BUILDING_LEDGE_2M
			CASE NM_SHOT_BUILDING_LEDGE_5M
			CASE NM_SHOT_BUILDING_LEDGE_10M
			CASE NM_SHOT_BUILDING_LEDGE_15M
			CASE NM_SHOT_BUILDING_LEDGE_20M
			CASE NM_TEETER_20M
			CASE NM_SLOPE
			CASE NM_DYING
			CASE NM_WALKING_DOWN_HILL
			CASE NM_FALLING_FROM_HEIGHT
				FORMAT_HELP_TEXT ("tstdbg17")	
				gbPrintGlobalText = FALSE
			BREAK
			 
			CASE NM_STANDING_ON_MOVING_VEHICLE
				FORMAT_HELP_TEXT ("tstdbg18")	
				gbPrintGlobalText = FALSE
			BREAK
			
			CASE NM_EXPLOSION
				FORMAT_HELP_TEXT ("tstdbg19")	
				gbPrintGlobalText = FALSE
			BREAK
			
			CASE NM_ON_FIRE
			CASE NM_RELAX_TEST
				FORMAT_HELP_TEXT ("tstdbg20")	
				gbPrintGlobalText = FALSE
			BREAK
			
			CASE NM_PLAYER_BUMPED_BY_VEHICLE
			CASE NM_BUMPED_BY_PLAYER
			CASE NM_BUMPED_BY_VEHICLE			
			CASE NM_SHOT_2
				//FORMAT_HELP_TEXT ("tstdbg18")	
				//gbPrintGlobalText = FALSE
			BREAK			
			
			CASE NM_FLINCH
				FORMAT_HELP_TEXT ("tstdbg21")	
				gbPrintGlobalText = FALSE
			BREAK
			
			CASE NM_PUNCHED_LOW_HEALTH
				FORMAT_HELP_TEXT ("tstdbg22")	
				gbPrintGlobalText = FALSE
			BREAK
			
			CASE NM_JUMPING_FROM_VEHICLE
				FORMAT_HELP_TEXT ("tstdbg23")	
				gbPrintGlobalText = FALSE
			BREAK
			
			CASE NM_GRAB_VEHICLE_DOOR
			CASE NM_SLOPE_CATCH
			CASE NM_ATTACH_IDLE
			CASE NM_ATTACH_UNDERNEATH
			CASE NM_TRUCK_BED
			CASE NM_COVER
				FORMAT_HELP_TEXT ("tstdbg24")	
				gbPrintGlobalText = FALSE
			BREAK
			
		ENDSWITCH
	ENDPROC

//  END_SCENARIO: SquadOfPedsMovingBetweenVehicles  

	
PROC SETUP_TEST_DATA ()



//Camera zoom around target
gCamzoom =4.5
gCamPan = 270
gCamPitch = -90
gCamZoomMIn = 0.5 
gCamZoomMax = 20.0


gMinZoom = 0.5
gMaxZoom = 15
gZoomspeed = 0.1

	RUN_LOCAL_HELP_TEXT ()

	SWITCH int_to_enum (scenarios, gcurrentselection)
		CASE NM_DYING
		CASE NM_BALANCE_TEST
		CASE NM_BINDPOSE_TEST
		
		CASE NM_DANGLE_FROM_MEATHOOK
			TestCams[FixedCamera].cam_pos =<<-19.655760,23.159075,3.672047>>
			TestCams[FixedCamera].cam_rot = <<-15.097453,-0.000049,179.572372>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << -19.8500, 13.9800, 1.00>> 
			Testpeds[0].PedHeading = 0.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = S_M_M_DOCKWORK_01

			vHookPos = TestPeds[0].PedsCoords + <<0.0, 0.0, 3.0>>
			oHook = CREATE_OBJECT_NO_OFFSET(Prop_LD_hook, vHookPos)	
			
			REQUEST_ANIM_DICT("misswiphanging")
			LOAD_ALL_OBJECTS_NOW()
			wait(40)
			
			gtestPan = 270
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			bAutoSwitchToTrackCam = true
			gStartedDangle = FALSE
			
		BREAK
		
		CASE NM_SLUNG_OVER_SHOULDER
		
			gbPlayerShouldBeAtCamPos = FALSE
		
			//TestCams[FixedCamera].cam_pos =<<-19.655760,23.159075,3.672047>>
			//TestCams[FixedCamera].cam_rot = <<-15.097453,-0.000049,179.572372>>
			//TestCams[FixedCamera].cam_fov = 45.00	
			//TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << -19.8500, 13.9800, 0.0041>> 
			Testpeds[0].PedHeading = 0.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = S_M_M_DOCKWORK_01
	
			gtestPan = 270
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			
			REQUEST_ANIM_DICT("NM")
			
			WHILE NOT HAS_ANIM_DICT_LOADED("NM")
				WAIT (0)
			ENDWHILE	
			
		BREAK
		
		
		CASE NM_SHOT_UNDERWATER
		
			IF gWeapon = 4
				gWeapon = 0
			ENDIF
			TestCams[FixedCamera].cam_pos =<< -201.0, 46.0, -26.0 >>
			TestCams[FixedCamera].cam_rot = <<-15.097453,-0.000049,179.572372>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << -201.0, 36.0, -30.0 >> 
			Testpeds[0].PedHeading = 0.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = S_M_M_DOCKWORK_01
	
			gtestPan = 270
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			bAutoSwitchToTrackCam = true
			
		BREAK
		
		CASE NM_SHOT_BUILDING_LEDGE_RAIL_2M
		
			IF gWeapon = 4
				gWeapon = 0
			ENDIF
			TestCams[FixedCamera].cam_pos = <<24.028452,-17.157396,6.391581>>
			TestCams[FixedCamera].cam_rot = <<-28.410831,-0.000000,88.959137>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << 16.6000, -16.8800, 2.0044>> 
			Testpeds[0].PedHeading = 270.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
	
			gtestPan = 90
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			bAutoSwitchToTrackCam = true
		
		BREAK

		CASE NM_BUMPED_BY_VEHICLE
			
			gbPlayerShouldBeAtCamPos = FALSE
			deadPedTimer = 0
			shot2PedInitialized = FALSE
			
			gFramesToCleanup = 60
			gShot2Distance = 4.0
			gShot2OrientationOffset = 0.0
			gShot2LastDistance = gShot2Distance
			gShot2LastOrientationOffset = gShot2OrientationOffset
	
			gtestPan = 270
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1

			TestVehicles [0 ].vehicleCoords = <<-7.05, 5.01, 0.0>>
			TestVehicles [0 ].vehicleHeading = 90.0001
			TestVehicles [0 ].vehiclemodel = NMVehicleModelSelection ( )
			
		BREAK
		
		CASE NM_BUMPED_BY_PLAYER
		CASE NM_SHOT_2
		
			gbPlayerShouldBeAtCamPos = FALSE
			deadPedTimer = 0
			shot2PedInitialized = FALSE
			
			gFramesToCleanup = 60
			gShot2Distance = 4.0
			gShot2OrientationOffset = 0.0
			gShot2LastDistance = gShot2Distance
			gShot2LastOrientationOffset = gShot2OrientationOffset
	
			gtestPan = 270
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
		
		BREAK		
		
		CASE NM_SHOT_AGAINST_WALL
		
			IF gWeapon = 4
				gWeapon = 0
			ENDIF
		
			TestCams[FixedCamera].cam_pos = <<20.30,-6.57,2.5>>
			TestCams[FixedCamera].cam_rot = <<-12.00,0.000000,91.48>>
			TestCams[FixedCamera].cam_fov = 50.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
		
			gtestPan = 90
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			bAutoSwitchToTrackCam = true
		
			Testpeds[0].PedsCoords = vAgainstWallPedsCoords 
			Testpeds[0].PedsCoords.x = Testpeds[0].PedsCoords.x + fPedWallDist
			Testpeds[0].PedHeading = -90
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
		
		BREAK
		
		CASE NM_SHOT_BUILDING_LEDGE_RAIL_5M
		
			IF gWeapon = 4
				gWeapon = 0
			ENDIF
			TestCams[FixedCamera].cam_pos = <<27.152411,-6.538266,8.959295>>
			TestCams[FixedCamera].cam_rot = <<-26.662188,0.000006,89.960609>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << 15.8500, -6.8800, 5.0044>> 
			Testpeds[0].PedHeading = 270.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
	
			gtestPan = 90
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			bAutoSwitchToTrackCam = true
		
		BREAK
		
		CASE NM_SHOT_BUILDING_LEDGE_RAIL_10M
		
			IF gWeapon = 4
				gWeapon = 0
			ENDIF
			TestCams[FixedCamera].cam_pos = <<32.760582,6.777083,13.965983>>
			TestCams[FixedCamera].cam_rot = <<-23.199821,0.000014,87.236572>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << 17.1000, 7.8700, 10.2544>> 
			Testpeds[0].PedHeading = 270.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
	
			gtestPan = 90
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			bAutoSwitchToTrackCam = true
		
		BREAK
		
		CASE NM_SHOT_BUILDING_LEDGE_RAIL_15M
		
			IF gWeapon = 4
				gWeapon = 0
			ENDIF
			TestCams[FixedCamera].cam_pos = <<37.031815,23.234030,20.426088>>
			TestCams[FixedCamera].cam_rot = <<-27.883694,0.000024,91.675102>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << 16.6000, 22.3700, 15.0044>> 
			Testpeds[0].PedHeading = 270.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
	
			gtestPan = 90
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			bAutoSwitchToTrackCam = true
		
		BREAK
			
		CASE NM_SHOT_BUILDING_LEDGE_RAIL_20M
		
			IF gWeapon = 4
				gWeapon = 0
			ENDIF
			TestCams[FixedCamera].cam_pos = <<2.328152,39.680660,24.140524>>
			TestCams[FixedCamera].cam_rot = <<-21.252611,0.000018,-90.651260>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << 10.3500, 39.6200, 20.0044>> 
			Testpeds[0].PedHeading = 90.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
	
			gtestPan = 90
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			bAutoSwitchToTrackCam = true	
		BREAK
		
		CASE NM_TEETER_20M
			TestCams[FixedCamera].cam_pos = <<37.241039,39.461372,23.894213>>
			TestCams[FixedCamera].cam_rot = <<-22.673187,0.000042,91.172180>>
			TestCams[FixedCamera].cam_fov = 45.00
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = <<25.77, 42.00-fPedEdgeDist, 20.0044>> 
			Testpeds[0].PedHeading = 0.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
	
			gtestPan = 90
			gtestPitch = -50
			gtestZoom = 1.0
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			bAutoSwitchToTrackCam = true
		BREAK
		
		CASE NM_SHOT_BUILDING_LEDGE_20M
		
			IF gWeapon = 4
				gWeapon = 0
			ENDIF
			TestCams[FixedCamera].cam_pos = <<37.241039,39.461372,23.894213>>
			TestCams[FixedCamera].cam_rot = <<-22.673187,0.000042,91.172180>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << 28.8500, 39.8700, 20.0044>> 
			Testpeds[0].PedHeading = 270.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
	
			gtestPan = 90
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			bAutoSwitchToTrackCam = true
		
		BREAK
		
		
		
		CASE NM_SHOT_BUILDING_LEDGE_15M
		
			IF gWeapon = 4
				gWeapon = 0
			ENDIF
			TestCams[FixedCamera].cam_pos = <<29.099384,40.207008,18.317476>>
			TestCams[FixedCamera].cam_rot = <<-15.551497,0.000040,-87.900055>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << 40.6000, 39.6200, 15.0044>> 
			Testpeds[0].PedHeading = 90.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
	
			gtestPan = 90
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			bAutoSwitchToTrackCam = true
		
		BREAK
		
		CASE NM_SHOT_BUILDING_LEDGE_10M
		
			IF gWeapon = 4
				gWeapon = 0
			ENDIF
			TestCams[FixedCamera].cam_pos =<<31.737461,25.261326,12.880817>>
			TestCams[FixedCamera].cam_rot = <<-14.158609,0.000039,-89.433922>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << 40.3500, 24.8700, 10.0044>> 
			Testpeds[0].PedHeading = 90.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
	
			gtestPan = 90
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			bAutoSwitchToTrackCam = true
		BREAK

		CASE NM_SHOT_BUILDING_LEDGE_5M
		
			IF gWeapon = 4
				gWeapon = 0
			ENDIF
		
			TestCams[FixedCamera].cam_pos = <<31.226727,10.951522,7.784438>>
			TestCams[FixedCamera].cam_rot = <<-11.589794,0.000036,-91.120361>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << 40.8500, 10.8700, 5.0044>>
			Testpeds[0].PedHeading = 90.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
	
			gtestPan = 90
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			bAutoSwitchToTrackCam = true
		
		BREAK
		
		CASE NM_SHOT_BUILDING_LEDGE_2M
		
			IF gWeapon = 4
				gWeapon = 0
			ENDIF
		
			TestCams[FixedCamera].cam_pos = <<42.099998,-3.159755,4.395028>>
			TestCams[FixedCamera].cam_rot = <<-17.999569,0.000000,0.000000>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << 42.1000, 1.1200, 2.0044>> 
			Testpeds[0].PedHeading = 180.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
	
			gtestPan = 90
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			bAutoSwitchToTrackCam = true
		
		BREAK
		
		CASE NM_SLOPE
			TestCams[FixedCamera].cam_pos = <<23.337534,-36.423306,12.903880>>
			TestCams[FixedCamera].cam_rot = <<-22.232384,-0.000015,-88.650375>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << 37.1000, -36.1300, 10.0045>> 
			Testpeds[0].PedHeading = 90.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
	
			gtestPan = 90
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			bAutoSwitchToTrackCam = true
		
		BREAK
		
		CASE NM_SLOPE_CATCH
		
			vOffsetVec1 = <<-1.75, -0.1, 0.95>>
			vOffsetVec2 = <<1.5, -0.1, 0.95>>
			vOffsetVec3 =  <<1.5, -0.1, 0.85>>
			vOffsetVec4 =   <<-1.75, -0.1, 0.85>>
		
			TestCams[FixedCamera].cam_pos = <<27.053932,27.167685,14.977894>>
			TestCams[FixedCamera].cam_rot =<<4.736102,0.000004,-0.675041>>
			TestCams[FixedCamera].cam_fov = 70.000000	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = <<26.93, 37.59, 20.00>>
			Testpeds[0].PedHeading = 180.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
	
			TestObject[0].ObjectModel = PROP_BOMB_01
			TestObject[0].ObjectCoords = <<27.32, 32.20, 14.08>>
	
			gtestPan = 90
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			bAutoSwitchToTrackCam = true
		
		BREAK
		CASE NM_RUN_SHOT
			TestCams[FixedCamera].cam_pos =<<-2.945364,10.007973,3.775786>>
			TestCams[FixedCamera].cam_rot = <<-12.014372,-0.000005,178.104584>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE
			
			IF gWeapon = 4
				gWeapon = 0
			ENDIF
			
			Testpeds[0].PedsCoords = <<-4.75, -30.63, 0.0>>
			Testpeds[0].PedHeading = 0.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
	
			gtestPan = 270
			gtestPitch = -90
			gtestZoom = 3.25
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			
			Route[0].RouteNode = <<-4.75, -30.63, 6.13>>
			Route[0].RouteNodeState = true
			
			Route[1].RouteNode = <<-2.69, 4.20, 6.13>> 
			Route[1].RouteNodeState = true
			
			bAutoSwitchToTrackCam = true
			
			
		BREAK
		
		
		CASE NM_PLAYER_BUMPED_BY_VEHICLE
		
			gVehBumpPlayerInitialized = FALSE
			gbPlayerShouldBeAtCamPos = FALSE
			gVehBumpPlayerReversing = FALSE
			gVehBumpPlayerForceFallOff = FALSE
	
			gtestPan = 270
			gtestPitch = -72 
			gtestZoom = 3.25
			
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			
			TestVehicles [0].vehicleCoords = gVehBumpPlayerPlayerStartPos + gVehBumpPlayerCarStartOffset
			TestVehicles [0].vehicleHeading = gVehBumpPlayerCarStartHeading	
			TestVehicles [0].vehiclemodel = NMVehicleModelSelection ()			
			
		BREAK
		
		CASE NM_FALLING_FROM_HEIGHT 
			
			TestCams[FixedCamera].cam_pos =<<-19.655760,23.159075,3.672047>>
			TestCams[FixedCamera].cam_rot = <<-15.097453,-0.000049,179.572372>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << -19.8500, 13.9800, 0.0041>> 
			Testpeds[0].PedHeading = 0.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
			
			CurrentPedPos = Testpeds[0].PedsCoords
		
		BREAK
		
		CASE NM_STANDING_ON_MOVING_VEHICLE
			gbPlayerShouldBeAtCamPos = FALSE
			
			TestCams[FixedCamera].cam_pos =<<-19.655760,23.159075,3.672047>>
			TestCams[FixedCamera].cam_rot = <<-15.097453,-0.000049,179.572372>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << -19.8500, 13.9800, 0.0041>> 
			Testpeds[0].PedHeading = 0.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()

			TestVehicles [0].vehicleCoords = << -10.8500, 13.9800, 0.0041>> 
			TestVehicles [0].vehicleHeading = 89.7811
			TestVehicles [0].vehiclemodel = NMVehicleModelSelection ( )
		
			gVehicleSpeed = 3
		
		BREAK
		
		CASE NM_JUMPING_FROM_VEHICLE
				gVehicleSpeed = 15.0
			
		BREAK

		
		CASE NM_GRAB_VEHICLE_DOOR
			gbPlayerShouldBeAtCamPos = FALSE
			
//			TestCams[FixedCamera].cam_pos =<<-2.784771,14.860714,5.900194>>
//			TestCams[FixedCamera].cam_rot = <<-19.199753,-0.000005,178.104584>>
//			TestCams[FixedCamera].cam_fov = 45.00	
//			TestCams[FixedCamera].bActivateCam = TRUE	
		
//			gvehicleZoom = 1.0
//			gVehiclePitch = -62
//			gVehiclepan = 180
			
			gtestPan = 360
			gtestPitch = -90
			gtestZoom = 0.95
			gZoomspeed = 0.1
				
			TestPeds[0].PedsCoords = << -4.5052, 1.7486, 0.1483>> 
			TestPeds[0].PedHeading = 182.0000
			TestPeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
			TestPeds[0].PedsWeapon = WEAPONTYPE_UNARMED
			
			TestVehicles [0].vehicleCoords = << -4.0256, 0.4923, 1.6070>> 
			TestVehicles [0].vehicleHeading = 89.8333
			TestVehicles [0].vehiclemodel = POLICE
			
			vOffsetVec1 = <<0.9, -1.05, 0.9>>
			vOffsetVec2 = <<0.9, -1.05, 0.4>>
			vOffsetVec3 = <<0.9, 0.5, 0.4>>
			vOffsetVec4 = <<0.9, 0.5, 0.9>>
		
		BREAK
		
		CASE NM_ATTACH_UNDERNEATH
			gbPlayerShouldBeAtCamPos = FALSE
			
//			TestCams[FixedCamera].cam_pos =<<-2.784771,14.860714,5.900194>>
//			TestCams[FixedCamera].cam_rot = <<-19.199753,-0.000005,178.104584>>
//			TestCams[FixedCamera].cam_fov = 45.00	
//			TestCams[FixedCamera].bActivateCam = TRUE	
		
//			gvehicleZoom = 1.0
//			gVehiclePitch = -62
//			gVehiclepan = 180
			
			gtestPan = 360
			gtestPitch = -90
			gtestZoom = 0.95
			gZoomspeed = 0.1
				
			TestPeds[0].PedsCoords = <<-8.11, -4.50, 0.0>>
			TestPeds[0].PedHeading = 182.0000
			TestPeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
			TestPeds[0].PedsWeapon = WEAPONTYPE_UNARMED //WEAPONTYPE_SMG_SKORPION
			
			TestVehicles [0].vehicleCoords = <<-7.91, 3.32, 0.0>> 
			TestVehicles [0].vehicleHeading = 89.8333
			TestVehicles [0].vehiclemodel = BARRACKS //NMVehicleModelSelection ( )
			
			vOffsetVec1 = <<0.9, -1.05, 0.9>>
			vOffsetVec2 = <<0.9, -1.05, 0.4>>
			vOffsetVec3 = <<0.9, 0.5, 0.4>>
			vOffsetVec4 = <<0.9, 0.5, 0.9>>
		
			bIsTestRunning = FALSE
		
			PRINTSTRING(" requesting NM_ATTACH_IDLE Anim dict loaded ")
			PRINTNL()
		
			REQUEST_ANIM_DICT("NM")
			
			WHILE NOT HAS_ANIM_DICT_LOADED("NM")
				WAIT (0)
			ENDWHILE	
			
			PRINTSTRING("NM_ATTACH_IDLE Anim dict loaded ")
			PRINTNL()
		BREAK
		
		CASE NM_ATTACH_IDLE
			gbPlayerShouldBeAtCamPos = FALSE
			
//			TestCams[FixedCamera].cam_pos =<<-2.784771,14.860714,5.900194>>
//			TestCams[FixedCamera].cam_rot = <<-19.199753,-0.000005,178.104584>>
//			TestCams[FixedCamera].cam_fov = 45.00	
//			TestCams[FixedCamera].bActivateCam = TRUE	
		
//			gvehicleZoom = 1.0
//			gVehiclePitch = -62
//			gVehiclepan = 180
			
			gtestPan = 360
			gtestPitch = -90
			gtestZoom = 0.95
			gZoomspeed = 0.1
				
			TestPeds[0].PedsCoords = <<-8.11, -4.50, 0.0>>
			TestPeds[0].PedHeading = 182.0000
			TestPeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
			TestPeds[0].PedsWeapon = WEAPONTYPE_UNARMED //WEAPONTYPE_SMG_SKORPION
			
			TestVehicles [0].vehicleCoords = <<-7.91, 3.32, 0.0>> 
			TestVehicles [0].vehicleHeading = 89.8333
			TestVehicles [0].vehiclemodel = FIRETRUK //VehicleModelSelection ( )
			
			vOffsetVec1 = <<0.9, -1.05, 0.9>>
			vOffsetVec2 = <<0.9, -1.05, 0.4>>
			vOffsetVec3 = <<0.9, 0.5, 0.4>>
			vOffsetVec4 = <<0.9, 0.5, 0.9>>
		
			bIsTestRunning = FALSE
		
			PRINTSTRING(" requesting NM_ATTACH_IDLE Anim dict loaded ")
			PRINTNL()
		
			REQUEST_ANIM_DICT("NM")
			
			WHILE NOT HAS_ANIM_DICT_LOADED("NM")
				WAIT (0)
			ENDWHILE	
			
			PRINTSTRING("NM_ATTACH_IDLE Anim dict loaded ")
			PRINTNL()
		BREAK
		
		CASE NM_RELAX_TEST
		CASE NM_FLINCH
			bAutoSwitchToTrackCam = true
			
			TestCams[FixedCamera].cam_pos =<<-19.655760,23.159075,3.672047>>
			TestCams[FixedCamera].cam_rot = <<-15.097453,-0.000049,179.572372>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << -19.8500, 13.9800, 0.0041>> 
			Testpeds[0].PedHeading = 0.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
		
		BREAK
		
		CASE NM_EXPLOSION
		CASE NM_ON_FIRE
		
			TestCams[FixedCamera].cam_pos =<<-19.655760,23.159075,3.672047>>
			TestCams[FixedCamera].cam_rot = <<-15.097453,-0.000049,179.572372>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << -19.8500, 13.9800, 0.0041>> 
			Testpeds[0].PedHeading = 0.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
					
			bAutoSwitchToTrackCam = true

		BREAK
	
		CASE NM_PUNCHED_LOW_HEALTH
			gbPlayerShouldBeAtCamPos = FALSE
			
			gtestPan = 270
			gtestPitch = -90
			gtestZoom = 1.5
			
			gCamzoom = 2.6
			
			gCamZoomMIn = 2.5
			
			gMinZoom = 0.65
			gMaxZoom = 2.0
			gZoomspeed = 0.01
			
			TestCams[FixedCamera].cam_pos =<<-19.655760,23.159075,3.672047>>
			TestCams[FixedCamera].cam_rot = <<-15.097453,-0.000049,179.572372>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = << -19.8500, 13.9800, 0.0041>> 
			Testpeds[0].PedHeading = 0.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
			
		BREAK
		
		CASE NM_WALKING_DOWN_HILL
			TestCams[FixedCamera].cam_pos = <<-145.0, -587.00, 160.00>>
			TestCams[FixedCamera].cam_rot = <<-12.014372,-0.000005,178.104584>>
			TestCams[FixedCamera].cam_fov = 45.00	
			TestCams[FixedCamera].bActivateCam = TRUE	
			
			Testpeds[0].PedsCoords = <<-141.32, -592.01, 157.18>>
			Testpeds[0].PedHeading = 0.0
			Testpeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
			Testpeds[0].PedsCoords -= GET_MAP_OFFSET // Because this will be added on later for *all* objects.
				
			gtestPan = 90
			gtestPitch = -90
			gtestZoom = 3.25
			gMinZoom = 0.5
			gMaxZoom = 15
			gZoomspeed = 0.1
			
			Route[0].RouteNode = <<-141.32, -592.01, 157.18>>
			Route[0].RouteNodeState = true
			
			Route[1].RouteNode = << -149.54, -168.81, 7.49 >>
			Route[1].RouteNodeState = true
			
			bAutoSwitchToTrackCam = true
		BREAK
		
		CASE NM_TRUCK_BED
		
			gbPlayerShouldBeAtCamPos = FALSE
			
			gtestPan = 360
			gtestPitch = -90
			gtestZoom = 0.95
			gZoomspeed = 0.1
			gVehicleSpeed = -10.0
				
			TestPeds[0].PedsCoords = << -4.0052, -19.2514, 2.1483>> 
			TestPeds[0].PedHeading = 182.0000
			TestPeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
			TestPeds[0].PedsWeapon = WEAPONTYPE_UNARMED
			
			TestVehicles[0].VehicleCoords = <<-4.0256, -20.4923, 1.6070>> 
			TestVehicles[0].VehicleHeading = 180
			TestVehicles[0].VehicleModel = BODHI2
			
			TestVehicles[1].VehicleCoords = <<-4.0256, -10.4923, 1.6070>>
			TestVehicles[1].VehicleHeading = 180
			TestVehicles[1].VehicleModel = TRFLAT
		BREAK
		
		CASE NM_COVER
			gbPlayerShouldBeAtCamPos = FALSE
			
			gtestPan = 360
			gtestPitch = -90
			gtestZoom = 0.95
			gZoomspeed = 0.1
			gVehicleSpeed = -10.0
			
			vOffset = <<0.0, 0.0, -6.1>>
			vOffsetVec1 = <<-1.0, -1.0, 0.0>>
			vOffsetVec2 = <<0.0, 0.0, 0.0>>

			TestPeds[0].PedsCoords = <<-36.8, -15.1, 7.1>>
			TestPeds[0].PedHeading = 180.0000
			TestPeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = PED_MODEL_SELECTOR()
			TestPeds[0].PedsWeapon = WEAPONTYPE_UNARMED

			TestPeds[1].PedsCoords = <<-36.8, -17.4, 7.1>>
			TestPeds[1].PedHeading = 0.0
			TestPeds[1].Pedrelgrp = 1
			TestPeds[1].PedModel = S_M_Y_COP_01
			TestPeds[1].PedsWeapon = WEAPONTYPE_PISTOL
			
		BREAK
				
		CASE NM_SLIDE_TEST
			gbPlayerShouldBeAtCamPos = FALSE
			
			IF NOT IS_ENTITY_DEAD(scplayer)
				SET_ENTITY_COORDS(scplayer, <<18.86, -272.20, 21.68>>)
				SET_ENTITY_HEADING(scplayer, gShot2OrientationOffset)
			ENDIF
			
		BREAK
		
		CASE NM_WARP_TO_INTERIOR
			gbPlayerShouldBeAtCamPos = FALSE
			
			IF NOT IS_ENTITY_DEAD(scplayer)
				SET_ENTITY_COORDS(scplayer, <<152.644, -132.157, 29.47>>)
			ENDIF
			
		BREAK
	
	ENDSWITCH	
	
	// Offset all fixed coordinates defined above to account for changing map offsets.
	
	Testpeds[0].PedsCoords += GET_MAP_OFFSET
	TestCams[FixedCamera].cam_pos += GET_MAP_OFFSET
	TestObject[0].ObjectCoords += GET_MAP_OFFSET
	TestVehicles[0].vehicleCoords += GET_MAP_OFFSET

ENDPROC

//ENUMS Run Shot
ENUM NRS_FLAGS
	NRS_GivePedTask,
	NRS_ShootPed
ENDENUM

NRS_FLAGS NRS_Status = NRS_GivePedTask

ENUM NFFH_Flags
	NFFH_SetPedPos,
	NFFH_SetDropPed
ENDENUM

NFFH_Flags NFFH_STATUS = NFFH_SetPedPos

ENUM NJFV_FLAGS
	NJFV_Setup,
	NJFV_Run,
	NJFV_Wait
ENDENUM

NJFV_FLAGS NJFV_Status = NJFV_Setup
		
PROC INITALISE_TEST_STATE()
	SingleTaskStatus = startSingleTask
	NRS_Status = NRS_GivePedTask
	NFFH_STATUS = NFFH_SetPedPos
	NJFV_Status = NJFV_Setup
ENDPROC


//bool bDrawSecondPoint = FALSE
//VECTOR gTargetPos
	VECTOR vBehaviourTextScreenPos = <<0.45, 0.055, 0.0>>
PROC RUN_TEST ()
	vector GunPos, TargetPos, secondtargetPos
	int index
	bool distanceChanged = FALSE
	bool orientationChanged = FALSE
	VECTOR aimCoords
	VECTOR cFallenPedCoords
	VECTOR vehBumpBackPoint = gVehBumpPlayerPlayerStartPos + gVehBumpPlayerCarStartOffset + <<0.0, gVehicleMoveDistance / 2.0, 0.0>>
	VECTOR vehBumpFrontPoint = gVehBumpPlayerPlayerStartPos + gVehBumpPlayerCarStartOffset - <<0.0, gVehicleMoveDistance / 2.0, 0.0>>
	VECTOR vehPos

	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE (true)
	
		SWITCH int_to_enum (scenarios, gcurrentselection)		// the ENUM corresponds to the values from the XML file 
			//setup scenario
			CASE NM_SHOT_UNDERWATER
			CASE NM_SHOT_BUILDING_LEDGE_RAIL_2M
			CASE NM_SHOT_BUILDING_LEDGE_RAIL_5M
			CASE NM_SHOT_BUILDING_LEDGE_RAIL_10M
			CASE NM_SHOT_BUILDING_LEDGE_RAIL_15M
			CASE NM_SHOT_BUILDING_LEDGE_RAIL_20M
			CASE NM_SHOT_BUILDING_LEDGE_2M
			CASE NM_SHOT_BUILDING_LEDGE_5M
			CASE NM_SHOT_BUILDING_LEDGE_10M
			CASE NM_SHOT_BUILDING_LEDGE_15M
			CASE NM_SHOT_BUILDING_LEDGE_20M
			CASE NM_SLOPE
				DRAW_DEBUG_TEXT_2D ("Shot Behaviour", vBehaviourTextScreenPos)
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
					Cleanup_Scenario_Entities ()
					
					FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
						TestPeds[0].PedModel = PED_MODEL_SELECTOR()
						CREATE_PED_ENTITY(Testpeds[index])
						GiveBlipsToPedsGrps (Testpeds[index])
						ALTER_COMBAT_STATS (Testpeds[index] )
						SWAP_PED_WEAPONS (Testpeds[index])
						//ADD_RELATIONSHIP_GROUP (Testpeds[index])
						Block_Peds_Temp_Events (Testpeds[index], TRUE)
						SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
					ENDFOR
					
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						SET_ENTITY_COORDS (TestPeds[0].ped, TestPeds[0].PedsCoords)
						SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)
					ENDIF	
				ENDIF
					
				IF NOT IS_PED_INJURED (TestPeds[0].ped)
				
					REQUEST_RAGDOLL_BOUNDS_UPDATE(TestPeds[0].ped)
					
						GunPos = GET_POINT_AROUND_PED_IN_SPHERICAL_COORDS (TestPeds[0].ped, gtestZoom, gtestPitch, gtestPan)
						TargetPos =  SELECT_PED_COMPONENT (TestPeds[0].ped)   // GET_OFFSET_FROM_PED_IN_WORLD_COORDS (TestPeds[0].ped, SELECT_PED_COMPONENT (TestPeds[0].ped) )
			
						secondtargetPos = (GunPos + 2.0*( TargetPos - GunPos ))
					
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						IF NOT IS_PED_RAGDOLL (TestPeds[0].ped)
							DRAW_DEBUG_SPHERE (GunPos, 0.05, 0, 0, 255,255 )
							DRAW_DEBUG_SPHERE (TargetPos, 0.05, 255, 0,0,255 )
							DRAW_DEBUG_LINE_WITH_TWO_COLOURS (GunPos, secondtargetPos, 0,0,255, 255, 255, 0, 0, 255 ) 
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
					gbshootPed = true
				
					IF (gbshootPed)
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS (GunPos, secondtargetPos, 0, true, SELECT_WEAPON_TYPE() )
						gbshootPed = FALSE
					ENDIF
			
				ENDIF
			
			BREAK
			
			CASE NM_SHOT_AGAINST_WALL
				DRAW_DEBUG_TEXT_2D ("Shot Behaviour", vBehaviourTextScreenPos)
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
					Cleanup_Scenario_Entities ()
					
					FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
						TestPeds[0].PedModel = PED_MODEL_SELECTOR()
						CREATE_PED_ENTITY(Testpeds[index])
						GiveBlipsToPedsGrps (Testpeds[index])
						ALTER_COMBAT_STATS (Testpeds[index] )
						SWAP_PED_WEAPONS (Testpeds[index])
						//ADD_RELATIONSHIP_GROUP (Testpeds[index])
						Block_Peds_Temp_Events (Testpeds[index], TRUE)
						SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
					ENDFOR
					
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
					
						TestPeds[0].PedsCoords.x = vAgainstWallPedsCoords.x
						TestPeds[0].PedsCoords.x = TestPeds[0].PedsCoords.x + fPedWallDist
						
						SET_ENTITY_HEALTH(TestPeds[0].ped, GET_PED_MAX_HEALTH(TestPeds[0].ped))
					
						SET_ENTITY_COORDS (TestPeds[0].ped, TestPeds[0].PedsCoords)
						SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)
						
					ENDIF	
				ENDIF	
					
				IF NOT IS_PED_INJURED (TestPeds[0].ped)
				
					REQUEST_RAGDOLL_BOUNDS_UPDATE(TestPeds[0].ped)
					
						GunPos = GET_POINT_AROUND_PED_IN_SPHERICAL_COORDS (TestPeds[0].ped, gtestZoom, gtestPitch, gtestPan)
						TargetPos =  SELECT_PED_COMPONENT (TestPeds[0].ped)   // GET_OFFSET_FROM_PED_IN_WORLD_COORDS (TestPeds[0].ped, SELECT_PED_COMPONENT (TestPeds[0].ped) )
			
						secondtargetPos = (GunPos + 2.0*( TargetPos - GunPos ))
					
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						IF NOT IS_PED_RAGDOLL (TestPeds[0].ped)
							DRAW_DEBUG_SPHERE (GunPos, 0.05, 0, 0, 255,255 )
							DRAW_DEBUG_SPHERE (TargetPos, 0.05, 255, 0,0,255 )
							DRAW_DEBUG_LINE_WITH_TWO_COLOURS (GunPos, secondtargetPos, 0,0,255, 255, 255, 0, 0, 255 ) 
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
					gbshootPed = true
				
					IF (gbshootPed)
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS (GunPos, secondtargetPos, 300, true, SELECT_WEAPON_TYPE() )
						gbshootPed = FALSE
					ENDIF
			
				ENDIF
			
			BREAK
			
			CASE NM_DANGLE_FROM_MEATHOOK
				DRAW_DEBUG_TEXT_2D ("Dangle from Meathook", vBehaviourTextScreenPos)
				
				IF NOT IS_PED_INJURED (TestPeds[0].ped)
					REQUEST_RAGDOLL_BOUNDS_UPDATE(TestPeds[0].ped)
				ENDIF
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE) OR !gStartedDangle
					Cleanup_Scenario_Entities ()
					
					wait(40)
						
					// Reset the hook
					vHookPos = TestPeds[0].PedsCoords + <<0.0, 0.0, 3.0>>
					DELETE_OBJECT(oHook)
					oHook = CREATE_OBJECT_NO_OFFSET(Prop_LD_hook, vHookPos)			
					LOAD_ALL_OBJECTS_NOW()
					wait(40)
						
					SET_OBJECT_PHYSICS_PARAMS(oHook, 400.0, -1.0,<<-1.0,-1.0, -1.0>>, <<-1.0,-1.0, -1.0>>)	
					SET_ENTITY_DYNAMIC(oHook, TRUE)
	
					SET_ENTITY_COORDS_NO_OFFSET(oHook, vHookPos)
					SET_ENTITY_ROTATION(oHook, <<0.0, 0.0, 180.0>>)
					
					FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
						TestPeds[0].PedModel = S_M_M_DOCKWORK_01
						CREATE_PED_ENTITY(Testpeds[index])
						GiveBlipsToPedsGrps (Testpeds[index])
						ALTER_COMBAT_STATS (Testpeds[index] )
						SWAP_PED_WEAPONS (Testpeds[index])
						//ADD_RELATIONSHIP_GROUP (Testpeds[index])
						Block_Peds_Temp_Events (Testpeds[index], TRUE)
						SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
					ENDFOR
					
					wait(40)

					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						//Attach Michael to hook
						ATTACH_ENTITY_TO_ENTITY(TestPeds[0].Ped, oHook, 0, <<-0.025, -0.05, -2.1>>, <<0.0, 0.0, 180.0>>)
						WAIT(0)
						
						// Check for injury here (again) because the wait() call appears to be resetting the bCheckedForDead flag
						IF IS_PED_HURT (TestPeds[0].ped)
						ENDIF
						
						TASK_PLAY_ANIM(TestPeds[0].Ped, "misswiphanging", "idle", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_FORCE_START)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(TestPeds[0].Ped)
						WAIT(0)
						
						// Check for injury here (again) because the wait() call appears to be resetting the bCheckedForDead flag
						IF IS_PED_HURT (TestPeds[0].ped)
						ENDIF
						
						// Make the ped dangle
						FREEZE_ENTITY_POSITION(oHook, FALSE)
						DETACH_ENTITY(TestPeds[0].ped)
						DANGLE_FROM_MEATHOOK(TRUE, TestPeds[0].ped, oHook, vHookPos, FALSE, 1.0, 1000, 60000, TRUE, -0.3, -0.3, -0.3, 0.3, 0.3, 0.3)
						
						// Add ankle cuffs
						SET_ENABLE_BOUND_ANKLES(TestPeds[0].ped, TRUE)
						
						// Pin the ankle to the bottom of the hook
						ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(TestPeds[0].ped, oHook, GET_PED_BONE_INDEX(TestPeds[0].ped, BONETAG_L_FOOT), -1, <<-0.075, -0.1, -0.85>>, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, -1.0, TRUE, FALSE, FALSE, FALSE)
						ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(TestPeds[0].ped, oHook, GET_PED_BONE_INDEX(TestPeds[0].ped, BONETAG_R_FOOT), -1, <<0.075, -0.1, -0.85>>, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, -1.0, TRUE, FALSE, FALSE, FALSE)
						
						gStartedDangle = TRUE
					ENDIF	
				ENDIF
				
				IF gStartedDangle
					vHookPos = vHookPos + ((vHookVelocity)*GET_FRAME_TIME())
					DANGLE_FROM_MEATHOOK(FALSE, TestPeds[0].ped, oHook, vHookPos, FALSE, 1.0, 100000, 100000, TRUE, -0.3, -0.3, -0.3, 0.3, 0.3, 0.3)
				ENDIF
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
					
					DETACH_ENTITY(TestPeds[0].ped)
					SET_ENABLE_BOUND_ANKLES(TestPeds[0].ped, FALSE)
			
				ENDIF
			
			BREAK
			
			CASE NM_BUMPED_BY_PLAYER
			CASE NM_BUMPED_BY_VEHICLE			
			CASE NM_SHOT_2
				DRAW_DEBUG_TEXT_2D ("Shot 2", vBehaviourTextScreenPos)
				
				IF NOT IS_PED_INJURED (scplayer)
				
					IF NOT shot2PedInitialized OR IS_PED_INJURED (TestPeds[0].ped) OR IS_BUTTON_JUST_PRESSED(PAD1, DPADDOWN)
					
						IF shot2PedInitialized
							deadPedTimer = deadPedTimer + 1
						ENDIF
						
						IF deadPedTimer > gFramesToCleanup OR NOT shot2PedInitialized OR IS_BUTTON_JUST_PRESSED(PAD1, DPADDOWN)
						
							FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
								DELETE_SCENARIO_PED(TestPeds[index])
								TestPeds[0].PedModel = PED_MODEL_SELECTOR_SHOT_2(gShot2PedType)
								CREATE_PED_ENTITY(Testpeds[index])
								GiveBlipsToPedsGrps (Testpeds[index])
								ALTER_COMBAT_STATS (Testpeds[index] )
								SWAP_PED_WEAPONS (Testpeds[index])
								//ADD_RELATIONSHIP_GROUP (Testpeds[index])
								Block_Peds_Temp_Events (Testpeds[index], bBlockTempAIEvents)
								SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
							ENDFOR
							
							IF IS_PED_INJURED (TestPeds[0].ped)
							ENDIF
							IF IS_PED_INJURED (scplayer)
							ENDIF
							IF IS_VEHICLE_DRIVEABLE(TestVehicles[0].vehicle)
							ENDIF
							
							IF int_to_enum(scenarios, gcurrentselection) = NM_BUMPED_BY_VEHICLE
								IF NOT IS_PED_SITTING_IN_VEHICLE (scplayer, TestVehicles[0].vehicle)
									FOR Index = 0 to MAX_NUMBER_OF_VEHICLES -1
										TestVehicles[0].VehicleModel = NMVehicleModelSelection ( )
										CREATE_VEHICLE_ENTITY (TestVehicles[index] )
									ENDFOR
									
									IF NOT IS_PED_INJURED (scplayer)
										TestVehicles [0].vehicleCoords = GET_ENTITY_COORDS(scplayer)
										TestVehicles [0].vehicleHeading = GET_ENTITY_HEADING(scplayer)
										
										CLEAR_PED_TASKS (scplayer)	
										IF IS_VEHICLE_DRIVEABLE(TestVehicles[0].vehicle)
											SET_ENTITY_COORDS (TestVehicles[0].vehicle, TestVehicles [0].vehicleCoords)
											SET_ENTITY_HEADING (TestVehicles[0].vehicle, TestVehicles [0].vehicleHeading)	
											SET_PED_INTO_VEHICLE (scplayer, TestVehicles[0].vehicle )
										ENDIF
										wait(0)
									ENDIF
								ENDIF
							ENDIF
							RENDER_SCRIPT_CAMS     (FALSE, FALSE)
							
							IF IS_PED_INJURED (TestPeds[0].ped)
							ENDIF
							IF IS_PED_INJURED (scplayer)
							ENDIF
							
							IF gPositionOnCliff
								SET_ENTITY_COORDS (scplayer, vFallFromCliffPlayerStartPos)
								SET_ENTITY_HEADING (scplayer, vFallFromCliffPlayerStartHeading)	
								SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
							ENDIF
					
							VECTOR front
							VECTOR side
							VECTOR up
							VECTOR pos
							GET_ENTITY_MATRIX(scplayer, front, side, up, pos)
							FLOAT playerHeading
							playerHeading = GET_ENTITY_HEADING(scplayer) + 180.0
							IF playerHeading > 360.0
								playerHeading = playerHeading - 360.0
							ENDIF
							playerHeading = playerHeading + gShot2OrientationOffset
							
							if IS_PED_IN_ANY_VEHICLE(scplayer) 
								pos = GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(scplayer))
							ENDIF
							
							Testpeds[0].PedsCoords = pos + front * gShot2Distance
							FLOAT adjust
							adjust = GET_ENTITY_HEIGHT_ABOVE_GROUND(Testpeds[0].Ped)
							Testpeds[0].PedsCoords.z = Testpeds[0].PedsCoords.z - adjust
							Testpeds[0].PedHeading = playerHeading
							Testpeds[0].Pedrelgrp = 1
							TestPeds[0].PedModel = PED_MODEL_SELECTOR()
						
							GIVE_WEAPON_TO_PED(TestPeds[0].ped, SELECT_WEAPON_TYPE(), 300, TRUE)
						
							SET_ENTITY_COORDS (TestPeds[0].ped, TestPeds[0].PedsCoords)
							SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)
							SET_PED_DUCKING(TestPeds[0].ped, gShot2Crouch)
							
							IF bAimWeapon
								GET_ENTITY_MATRIX(TestPeds[0].ped, front, side, up, pos)
								aimCoords = pos + front * 100.0
								TASK_AIM_GUN_AT_COORD( TestPeds[0].ped, aimCoords, -1 )
							ENDIF
							
							IF gShot2bForward OR gShot2bLeft OR gShot2bRight OR gShot2bBackward
								TEST_SEQ = TEST_ROUTE_SHOT_2 (SHOT_2_DYNAMIC_MOVE_TYPE, gShot2bForward, gShot2bLeft, gShot2bRight, gShot2bBackward, gShot2MovementDistance)
								PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
							ENDIF
							
							deadPedTimer = 0
							IF NOT shot2PedInitialized
								shot2PedInitialized = TRUE
							ENDIF		
							
						ENDIF

					ENDIF	
					
					IF shot2PedInitialized AND NOT IS_PED_INJURED (TestPeds[0].ped)
					
						IF IS_PED_INJURED (TestPeds[0].ped)
						ENDIF
						IF IS_PED_INJURED (scplayer)
						ENDIF
						
						IF bBlockTempAIEvents != bLastBlockTempAIEvents
							Block_Peds_Temp_Events (Testpeds[0], bBlockTempAIEvents)
							bLastBlockTempAIEvents = bBlockTempAIEvents
						ENDIF
						
						IF gPositionOnCliff != gPositionOnCliffLast
							IF gPositionOnCliff
								// Force a restart
								gShot2Distance = 4.0
								gShot2LastDistance = 4.0
								shot2PedInitialized = FALSE
							ELSE
								RENDER_SCRIPT_CAMS     (FALSE, FALSE)
							ENDIF
							gPositionOnCliffLast = gPositionOnCliff
						ENDIF
					
						IF gWeapon != gLastWeapon
							GIVE_WEAPON_TO_PED(TestPeds[0].ped, SELECT_WEAPON_TYPE(), 300, TRUE)
							IF gLastWeapon >= 4
								bWasAimingWeapon = FALSE
							ENDIF
							gLastWeapon = gWeapon
						ENDIF
						
						BOOL resetAimAndMovement
						IF bGetInCarMode
							resetAimAndMovement = bAimWeapon AND NOT GET_PED_CONFIG_FLAG(TestPeds[0].ped, PCF_IsAimingGun) AND IS_PED_IN_ANY_VEHICLE(TestPeds[0].ped)
						ELSE
							resetAimAndMovement = bAimWeapon AND NOT GET_PED_CONFIG_FLAG(TestPeds[0].ped, PCF_IsAimingGun)
						ENDIF
						resetAimAndMovement = resetAimAndMovement OR bAimWeapon != bWasAimingWeapon
						
						IF resetAimAndMovement
							VECTOR playerCoords
							playerCoords = GET_ENTITY_COORDS(scplayer)
							IF bAimWeapon 
								VECTOR front
								VECTOR side
								VECTOR up
								VECTOR pos
								GET_ENTITY_MATRIX(TestPeds[0].ped, front, side, up, pos)
								aimCoords = pos + front * 100.0
								TASK_AIM_GUN_AT_COORD( TestPeds[0].ped, aimCoords, -1 )
							ELSE
								TASK_AIM_GUN_AT_COORD( TestPeds[0].ped, playerCoords, 0 )					
							ENDIF
							IF gShot2bForward OR gShot2bLeft OR gShot2bRight OR gShot2bBackward
								TEST_SEQ = TEST_ROUTE_SHOT_2 (SHOT_2_DYNAMIC_MOVE_TYPE, gShot2bForward, gShot2bLeft, gShot2bRight, gShot2bBackward, gShot2MovementDistance)
								PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
							ENDIF
							bWasAimingWeapon = bAimWeapon
						ENDIF					
					
						IF gShot2Distance != gShot2LastDistance
							gShot2LastDistance = gShot2Distance
							distanceChanged = TRUE
						ENDIF
						IF gShot2OrientationOffset != gShot2LastOrientationOffset
							gShot2LastOrientationOffset = gShot2OrientationOffset
							orientationChanged = TRUE
						ENDIF
						
						IF distanceChanged OR orientationChanged
						
							VECTOR front
							VECTOR side
							VECTOR up
							VECTOR pos
							GET_ENTITY_MATRIX(scplayer, front, side, up, pos)
							FLOAT playerHeading
							playerHeading = GET_ENTITY_HEADING(scplayer) + 180.0
							IF playerHeading > 360.0
								playerHeading = playerHeading - 360.0
							ENDIF
							playerHeading = playerHeading + gShot2OrientationOffset
							
							Testpeds[0].PedsCoords = pos + front * gShot2Distance
							FLOAT adjust
							adjust = GET_ENTITY_HEIGHT_ABOVE_GROUND(Testpeds[0].Ped)
							Testpeds[0].PedsCoords.z = Testpeds[0].PedsCoords.z - adjust
							Testpeds[0].PedHeading = playerHeading
						
							IF distanceChanged
								SET_ENTITY_COORDS (TestPeds[0].ped, TestPeds[0].PedsCoords)
							ENDIF
							IF orientationChanged
								SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)
								IF bAimWeapon 
									GET_ENTITY_MATRIX(TestPeds[0].ped, front, side, up, pos)
									aimCoords = pos + front * 100.0
									TASK_AIM_GUN_AT_COORD( TestPeds[0].ped, aimCoords, -1 )
								ENDIF
							ENDIF
							
						ENDIF
						
						IF gShot2bForward != gShot2bWasForward OR gShot2bLeft != gShot2bWasLeft OR gShot2bRight != gShot2bWasRight OR gShot2bBackward != gShot2bWasBackward
							TEST_SEQ = TEST_ROUTE_SHOT_2 (SHOT_2_DYNAMIC_MOVE_TYPE, gShot2bForward, gShot2bLeft, gShot2bRight, gShot2bBackward, gShot2MovementDistance)
							PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)	
							gShot2bWasForward = gShot2bForward
							gShot2bWasLeft = gShot2bLeft
							gShot2bWasRight = gShot2bRight
							gShot2bWasBackward = gShot2bBackward
						ENDIF
																		
						IF gShot2Crouch != gShot2WasCrouch
							SET_PED_DUCKING(TestPeds[0].ped, gShot2Crouch)
							gShot2WasCrouch = gShot2Crouch
						ENDIF
						
					ENDIF
					
					// Switch to fall cam if the victim falls
					IF shot2PedInitialized AND gPositionOnCliff
					
						cFallenPedCoords = GET_ENTITY_COORDS(TestPeds[0].ped, FALSE)
						IF vFallFromCliffPlayerStartPos.z - cFallenPedCoords.z > 0.1 OR cFallenPedCoords.x < 39.4
						
							IF NOT IS_PED_RAGDOLL(TestPeds[0].ped)
								shot2PedInitialized = FALSE
							ENDIF
						
							IF NOT DOES_CAM_EXIST (TestCams[FixedCamera].cam)
								TestCams[FixedCamera].cam = CREATE_CAM_WITH_PARAMS ("DEFAULT_SCRIPTED_CAMERA", vFallFromCliffFixedCamPos, <<0.0,0.0,0.0>>, 50.00, true) 
							ENDIF
								
							RENDER_SCRIPT_CAMS     (TRUE, TRUE, 600)
							POINT_CAM_AT_ENTITY(TestCams[FixedCamera].cam, TestPeds[0].ped, <<0.0, 0.0, 0.0>>)
								
						ENDIF
					ENDIF
					
					IF bInvulnerableTestPed AND NOT IS_PED_FATALLY_INJURED (TestPeds[0].ped)
						SET_ENTITY_HEALTH(TestPeds[0].ped, GET_PED_MAX_HEALTH(TestPeds[0].ped))
					ENDIF
					
				ENDIF
				
			BREAK
			
			CASE NM_SLUNG_OVER_SHOULDER
				DRAW_DEBUG_TEXT_2D ("Slung Over Shoulder", vBehaviourTextScreenPos)
				
				IF NOT IS_PED_INJURED (TestPeds[0].ped)
					REQUEST_RAGDOLL_BOUNDS_UPDATE(TestPeds[0].ped)
				ENDIF				
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE) OR IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
					Cleanup_Scenario_Entities ()
					
					TestPeds[0].PedModel = PED_MODEL_SELECTOR()
					
					FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
						CREATE_PED_ENTITY(Testpeds[index])
						GiveBlipsToPedsGrps (Testpeds[index])
						ALTER_COMBAT_STATS (Testpeds[index] )
						SWAP_PED_WEAPONS (Testpeds[index])
						//ADD_RELATIONSHIP_GROUP (Testpeds[index])
						Block_Peds_Temp_Events (Testpeds[index], TRUE)
						SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
					ENDFOR
					
					IF NOT IS_PED_INJURED (TestPeds[0].Ped)
					      CLEAR_PED_TASKS(scplayer)
					      CLEAR_PED_TASKS_IMMEDIATELY(TestPeds[0].Ped)                                               
					      ATTACH_ENTITY_TO_ENTITY(TestPeds[0].Ped, scplayer, GET_PED_BONE_INDEX(scplayer, BONETAG_L_CLAVICLE), vCarriedPos, vCarriedRot, FALSE, FALSE, FALSE, TRUE)
					      TASK_PLAY_ANIM_ADVANCED(TestPeds[0].Ped, "nm", "firemans_carry", <<0,0,0>>, <<0,0,0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_HOLD_LAST_FRAME|AF_NOT_INTERRUPTABLE, 1.0)    
					      FORCE_PED_AI_AND_ANIMATION_UPDATE(TestPeds[0].Ped)
					                                                
                               
					ENDIF
												
				ENDIF
					
				IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
					WAIT(0) //REQUIRED: need to wait at least a frame to give animations time to pose the ped.    
					// Make the ped dangle
					IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD(scplayer)
						SLUNG_OVER_SHOULDER(TestPeds[0].ped, scplayer, 100000, 100000)
					ENDIF
				ENDIF
			BREAK
			
			CASE NM_TEETER_20M
				DRAW_DEBUG_TEXT_2D ("Teeter Behaviour", vBehaviourTextScreenPos)
				PedComponent = STOMACH
				
				IF NOT IS_PED_INJURED (TestPeds[0].ped)
					REQUEST_RAGDOLL_BOUNDS_UPDATE(TestPeds[0].ped)
				ENDIF				
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
					Cleanup_Scenario_Entities ()
					
					FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
						TestPeds[0].PedModel = PED_MODEL_SELECTOR()
						CREATE_PED_ENTITY(Testpeds[index])
						GiveBlipsToPedsGrps (Testpeds[index])
						ALTER_COMBAT_STATS (Testpeds[index] )
						SWAP_PED_WEAPONS (Testpeds[index])
						//ADD_RELATIONSHIP_GROUP (Testpeds[index])
						Block_Peds_Temp_Events (Testpeds[index], TRUE)
						SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
					ENDFOR
					
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						TestPeds[0].PedsCoords.y = 42.00-fPedEdgeDist
						SET_ENTITY_COORDS (TestPeds[0].ped, TestPeds[0].PedsCoords)
						SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)
					ENDIF	
				ENDIF

				IF NOT IS_PED_INJURED (TestPeds[0].ped)
					
						GunPos = GET_POINT_AROUND_PED_IN_SPHERICAL_COORDS (TestPeds[0].ped, gtestZoom, gtestPitch, gtestPan)
						TargetPos =  SELECT_PED_COMPONENT (TestPeds[0].ped)   // GET_OFFSET_FROM_PED_IN_WORLD_COORDS (TestPeds[0].ped, SELECT_PED_COMPONENT (TestPeds[0].ped) )
			
						secondtargetPos = (GunPos + 2.0*( TargetPos - GunPos ))
					
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						IF NOT IS_PED_RAGDOLL (TestPeds[0].ped)
							DRAW_DEBUG_SPHERE (GunPos, 0.05, 0, 0, 255,255 )
							DRAW_DEBUG_SPHERE (TargetPos, 0.05, 255, 0,0,255 )
							DRAW_DEBUG_LINE_WITH_TWO_COLOURS (GunPos, secondtargetPos, 0,0,255, 255, 255, 0, 0, 255 ) 
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
					gbshootPed = true
										
					IF (gbshootPed)
						// Apply a force to the ped's root to push them towards the edge. Scale the force depending on
						// the length of the line showing the direction.
						IF NOT IS_PED_INJURED(TestPeds[0].ped)
							vImpulseTeeter.x = (TargetPos.x - GunPos.x)*0.4
							vImpulseTeeter.y = (TargetPos.y - GunPos.y)*0.4
							vImpulseTeeter.z = (TargetPos.z - GunPos.z)*0.4
							
							IF NOT IS_PED_INJURED (TestPeds[0].ped)						
								SET_PED_TO_RAGDOLL(TestPeds[0].ped, 2000, 10000, TASK_NM_BALANCE)
							ENDIF
							
							IF NOT IS_PED_INJURED (TestPeds[0].ped)
								PedComp = ENUM_TO_INT(RAGDOLL_PELVIS)
								APPLY_FORCE_TO_ENTITY(TestPeds[0].ped, APPLY_TYPE_IMPULSE, vImpulseTeeter, vOffsetTeeter, PedComp, FALSE, FALSE, true )
							ENDIF
						ENDIF	
						
						gbshootPed = FALSE
					ENDIF
			
				ENDIF
			BREAK
			
			CASE NM_SLOPE_CATCH
				DRAW_DEBUG_TEXT_2D ("Grab Behaviour", vBehaviourTextScreenPos)
			
				IF NOT IS_PED_INJURED (TestPeds[0].ped)
					REQUEST_RAGDOLL_BOUNDS_UPDATE(TestPeds[0].ped)
				ENDIF
			
					IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
					Cleanup_Scenario_Entities ()
					
					FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
						TestPeds[0].PedModel = PED_MODEL_SELECTOR()
						
						CREATE_PED_ENTITY(Testpeds[index])
						GiveBlipsToPedsGrps (Testpeds[index])
						ALTER_COMBAT_STATS (Testpeds[index] )
						SWAP_PED_WEAPONS (Testpeds[index])
						//ADD_RELATIONSHIP_GROUP (Testpeds[index])
						Block_Peds_Temp_Events (Testpeds[index], TRUE)
						SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
					ENDFOR
					
					FOR index = 0 to MAX_NUMBER_OF_OBJECTS -1
						CREATE_OBJECT_ENTITY(testobject[index])
					ENDFOR	
					
					
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						SET_ENTITY_COORDS (TestPeds[0].ped, TestPeds[0].PedsCoords)
						SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)
					ENDIF	
				ENDIF
					
				//draw the relevant debug info	
				IF DOES_ENTITY_EXIST (TestObject[0].Object)
				
					SWITCH (gGrabType)
						CASE 0
							DRAW_DEBUG_SPHERE (GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec1  ), 0.1, 255, 0, 0)
							DRAW_DEBUG_SPHERE (GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec2  ), 0.1, 0, 255, 0)
							DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec1)  , GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec2 ), 255, 0, 0)
						BREAK
					
						CASE 1
							DRAW_DEBUG_SPHERE (GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec1  ), 0.1, 255, 0, 0)
							DRAW_DEBUG_SPHERE (GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec2  ), 0.1, 0, 255, 0)
							DRAW_DEBUG_SPHERE (GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec3  ), 0.1, 0, 0, 255)
							DRAW_DEBUG_SPHERE (GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec4  ), 0.1, 255, 255, 255)
							DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec1)  , GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec2 ), 255, 0, 0)
							DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec2)  , GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec3 ), 0, 255, 0)
							DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec3)  , GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec4 ), 0, 0, 255)
							DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec4)  , GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec1 ), 255, 255, 255)
						BREAK
						
						CASE 2
							DRAW_DEBUG_SPHERE (GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec1  ), 0.1, 255, 0, 0)
							DRAW_DEBUG_SPHERE (GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec2  ), 0.1, 0, 255, 0)
							DRAW_DEBUG_SPHERE (GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec3  ), 0.1, 0, 0, 255)
							DRAW_DEBUG_SPHERE (GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec4  ), 0.1, 255, 255, 255)
							DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec1)  , GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec2 ), 255, 0, 0)
							DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec2)  , GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec3 ), 0, 255, 0)
							DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec3)  , GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec4 ), 0, 0, 255)
							DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec4)  , GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS ( TestObject[0].Object, vOffsetVec1 ), 255, 255, 255)
						
						BREAK
						
						
					ENDSWITCH
		
					FREEZE_ENTITY_POSITION(TestObject[0].Object, true)
				
					SET_ENTITY_VISIBLE (TestObject[0].object, FALSE)
				ENDIF	
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
					gbshootPed = true
					
					SWITCH (gGrabType)
						CASE 0
								bNM_GRAB_SURFACE = FALSE
								bNM_GRAB_A_LINE = true
								bNM_GRAB_DONT_LET_GO = true
								if gresetoffsets
									vOffsetVec1 = <<-1.0, -0.1, 0.95>>
									vOffsetVec2 = <<0.0, -0.1, 0.95>>
									gresetoffsets = FALSE
								ENDIF
						BREAK
					
						CASE 1
							bNM_GRAB_A_LINE = FALSE
							bNM_GRAB_SURFACE = TRUE
							bNM_GRAB_DONT_LET_GO = true
							if gresetoffsets
								vOffsetVec1 = <<-1.75, -0.1, 0.95>>
								vOffsetVec2 = <<1.5, -0.1, 0.95>>
								vOffsetVec3 =  <<1.5, -0.1, 0.85>>
								vOffsetVec4 =   <<-1.75, -0.1, 0.85>>
								gresetoffsets = FALSE
							ENDIF
						BREAK		
					ENDSWITCH
					
					
					IF NOT IS_PED_INJURED (TestPeds[0].ped)						
							SET_PED_TO_RAGDOLL (TestPeds[0].ped, 0, 0, TASK_NM_SCRIPT)
							CREATE_NM_MESSAGE (NM_START_START, NM_BALANCE_MSG)
							GIVE_PED_NM_MESSAGE (TestPeds[0].ped)
					
								
							CREATE_NM_MESSAGE (NM_START_START, NM_GRAB_MSG)
							SET_NM_MESSAGE_INSTANCE_INDEX(NM_GRAB_INSTANCE_INDEX, NULL, null, TestObject[0].object)
							SET_NM_MESSAGE_VEC3(NM_GRAB_POS2_VEC3, vOffsetVec1)
							SET_NM_MESSAGE_VEC3(NM_GRAB_POS1_VEC3, vOffsetVec2)
						
							SET_NM_MESSAGE_VEC3(NM_GRAB_POS3_VEC3, vOffsetVec3)
							SET_NM_MESSAGE_VEC3(NM_GRAB_POS4_VEC3, vOffsetVec4)
							
						SET_NM_MESSAGE_BOOL (NM_GRAB_A_LINE, bNM_GRAB_A_LINE)	
							SET_NM_MESSAGE_BOOL (NM_GRAB_BRACE_ONLY , bNM_GRAB_BRACE_ONLY)	
							SET_NM_MESSAGE_BOOL (NM_GRAB_SURFACE ,bNM_GRAB_SURFACE)	
							SET_NM_MESSAGE_BOOL (NM_GRAB_USE_LEFT , bNM_GRAB_USE_LEFT)	
							SET_NM_MESSAGE_BOOL (NM_GRAB_USE_RIGHT , bNM_GRAB_USE_RIGHT)	
							SET_NM_MESSAGE_BOOL (NM_GRAB_DONT_LET_GO , bNM_GRAB_DONT_LET_GO)	
							
						//	SET_NM_MESSAGE_INT(NM_GRAB_INSTANCE_INDEX, iNM_GRAB_INSTANCE_INDEX)
						//	SET_NM_MESSAGE_INT(NM_GRAB_INSTANCE_PART_INDEX, iNM_GRAB_INSTANCE_PART_INDEX)
							
					
						//	SET_NM_MESSAGE_VEC3(NM_GRAB_NORM_L_VEC3, vNormalR )
						//	SET_NM_MESSAGE_VEC3(NM_GRAB_NORM_R_VEC3, vNormalL )
							
							SET_NM_MESSAGE_FLOAT (NM_GRAB_STRENGTH , fNM_GRAB_STRENGTH)	
							SET_NM_MESSAGE_FLOAT (NM_GRAB_BODY_STIFFNESS , fNM_GRAB_BODY_STIFFNESS)	
							SET_NM_MESSAGE_FLOAT (NM_GRAB_REACH_ANGLE , fNM_GRAB_REACH_ANGLE)	
							SET_NM_MESSAGE_FLOAT (NM_GRAB_DISTANCE , fNM_GRAB_DISTANCE)	
							SET_NM_MESSAGE_FLOAT (NM_GRAB_ONE_SIDE_REACH_ANGLE , fNM_GRAB_ONE_SIDE_REACH_ANGLE)	
						
							SET_NM_MESSAGE_FLOAT (NM_GRAB_PULLUP_TIMER , fNM_GRAB_PULLUP_TIMER)	
							SET_NM_MESSAGE_FLOAT (NM_GRAB_HOLD_MAX_TIMER , fNM_GRAB_HOLD_MAX_TIMER)	
							
							SET_NM_MESSAGE_FLOAT (NM_GRAB_PULLUP_STRENGTH_LEFT , fNM_GRAB_PULLUP_STRENGTH_LEFT)	
							SET_NM_MESSAGE_FLOAT (NM_GRAB_PULLUP_STRENGTH_RIGHT , fNM_GRAB_PULLUP_STRENGTH_RIGHT)	
						GIVE_PED_NM_MESSAGE(TestPeds[0].ped)
							
					ENDIF
				
					IF (gbshootPed)
							IF NOT IS_PED_INJURED (TestPeds[0].ped)	
								PedComp = ENUM_TO_INT(RAGDOLL_PELVIS)
								APPLY_FORCE_TO_ENTITY(TestPeds[0].ped, APPLY_TYPE_IMPULSE, vImpulse, vOffset, PedComp, true, FALSE, true )
							ENDIF	
						gbshootPed = FALSE
					ENDIF
				ENDIF	
			BREAK
			
			
			CASE NM_DYING
					DRAW_DEBUG_TEXT_2D ("Death Behaviour", vBehaviourTextScreenPos)
					
				IF NOT IS_PED_INJURED (TestPeds[0].ped)
					REQUEST_RAGDOLL_BOUNDS_UPDATE(TestPeds[0].ped)
				ENDIF					
			
						IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
					Cleanup_Scenario_Entities ()
					
					FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
						TestPeds[0].PedModel = PED_MODEL_SELECTOR()
						CREATE_PED_ENTITY(Testpeds[index])
						GiveBlipsToPedsGrps (Testpeds[index])
						ALTER_COMBAT_STATS (Testpeds[index] )
						SWAP_PED_WEAPONS (Testpeds[index])
						//ADD_RELATIONSHIP_GROUP (Testpeds[index])
						Block_Peds_Temp_Events (Testpeds[index], TRUE)
						SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
					ENDFOR
					
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						SET_ENTITY_COORDS (TestPeds[0].ped, TestPeds[0].PedsCoords)
						SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)
						SET_ENTITY_HEALTH (TestPeds[0].ped, 101)
					ENDIF	
				ENDIF
					
				IF NOT IS_PED_INJURED (TestPeds[0].ped)
					
						GunPos = GET_POINT_AROUND_PED_IN_SPHERICAL_COORDS (TestPeds[0].ped, gtestZoom, gtestPitch, gtestPan)
						TargetPos =  SELECT_PED_COMPONENT (TestPeds[0].ped)   // GET_OFFSET_FROM_PED_IN_WORLD_COORDS (TestPeds[0].ped, SELECT_PED_COMPONENT (TestPeds[0].ped) )
			
						secondtargetPos = (GunPos + 2.0*( TargetPos - GunPos ))
					
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						IF NOT IS_PED_RAGDOLL (TestPeds[0].ped)
							DRAW_DEBUG_SPHERE (GunPos, 0.05, 0, 0, 255,255 )
							DRAW_DEBUG_SPHERE (TargetPos, 0.05, 255, 0,0,255 )
							DRAW_DEBUG_LINE_WITH_TWO_COLOURS (GunPos, secondtargetPos, 0,0,255, 255, 255, 0, 0, 255 ) 
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
					gbshootPed = true
				
					IF (gbshootPed)
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS (GunPos, TargetPos, 10, true )
						gbshootPed = FALSE
					ENDIF
			
				ENDIF
		

			BREAK
			
			CASE NM_RUN_SHOT
				DRAW_DEBUG_TEXT_2D ("Run Shot Behaviour", vBehaviourTextScreenPos)
				
				IF NOT IS_PED_INJURED (TestPeds[0].ped)
					REQUEST_RAGDOLL_BOUNDS_UPDATE(TestPeds[0].ped)
				ENDIF
				
				SWITCH NRS_Status
					CASE NRS_GivePedTask
						IF NOT IS_PED_INJURED (TestPeds[0].ped)
							TEST_SEQ = TEST_ROUTE (RUN_SHOT_DYNAMIC_MOVE_TYPE) 
							PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)	
						ENDIF
						NRS_Status = NRS_ShootPed
					BREAK
					
					CASE NRS_ShootPed
						IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
							Cleanup_Scenario_Entities ()
						
							FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
								TestPeds[0].PedModel = PED_MODEL_SELECTOR()
								
								CREATE_PED_ENTITY(Testpeds[index])
								GiveBlipsToPedsGrps (Testpeds[index])
								ALTER_COMBAT_STATS (Testpeds[index] )
								SWAP_PED_WEAPONS (Testpeds[index])
								//ADD_RELATIONSHIP_GROUP (Testpeds[index])
								Block_Peds_Temp_Events (Testpeds[index], TRUE)
								SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
							ENDFOR
							
							IF NOT IS_PED_INJURED (TestPeds[0].ped)	
								CLEAR_PED_TASKS(TestPeds[0].ped)
								SET_ENTITY_COORDS (TestPeds[0].ped, TestPeds[0].PedsCoords)
								SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)
								NRS_Status = NRS_GivePedTask						
							ENDIF	
						ENDIF
						
						IF NOT IS_PED_INJURED (TestPeds[0].ped)		
							
							GunPos = GET_POINT_AROUND_PED_IN_SPHERICAL_COORDS (TestPeds[0].ped, gtestZoom, gtestPitch, gtestPan)
							TargetPos =  SELECT_PED_COMPONENT (TestPeds[0].ped)   // GET_OFFSET_FROM_PED_IN_WORLD_COORDS (TestPeds[0].ped, SELECT_PED_COMPONENT (TestPeds[0].ped) )
				
							secondtargetPos = (GunPos + 2.0*( TargetPos - GunPos ))
													
							IF NOT IS_PED_RAGDOLL (TestPeds[0].ped)
								DRAW_DEBUG_SPHERE (GunPos, 0.05, 0, 0, 255,255 )
								DRAW_DEBUG_SPHERE (TargetPos, 0.05, 255, 0,0,255 )
								DRAW_DEBUG_LINE_WITH_TWO_COLOURS (GunPos, secondtargetPos, 0,0,255, 255, 255, 0, 0, 255 ) 
							ENDIF
						ENDIF
						
						IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
							gbshootPed = true
						
							IF (gbshootPed)
								SHOOT_SINGLE_BULLET_BETWEEN_COORDS (GunPos, secondtargetPos, 0, true, SELECT_WEAPON_TYPE()  )
								gbshootPed = FALSE
							ENDIF
					
						ENDIF
					
					BREAK
							
				ENDSWITCH
			BREAK
			
			
			CASE NM_PLAYER_BUMPED_BY_VEHICLE
				DRAW_DEBUG_TEXT_2D ("Player bumped by vehicle", vBehaviourTextScreenPos)
				
				IF NOT IS_PED_INJURED (scplayer)
										
					IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE) OR NOT gVehBumpPlayerInitialized
				
						Cleanup_Scenario_Entities ()
						
						TestVehicles[0].Vehicle = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(scplayer), 100.0, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
						
						IF NOT DOES_ENTITY_EXIST (TestVehicles[0].Vehicle)
							TestVehicles[0].VehicleModel = NMVehicleModelSelection ( )
							CREATE_VEHICLE_ENTITY (TestVehicles[0] )
						ENDIF
						
						SET_ENTITY_COORDS (TestVehicles[0].vehicle, gVehBumpPlayerPlayerStartPos + gVehBumpPlayerCarStartOffset + <<0.0, gVehicleMoveDistance / 2.0, 0.0>>)
						SET_ENTITY_HEADING (TestVehicles[0].vehicle, gVehBumpPlayerCarStartHeading)
						
						IF IS_PED_INJURED (scplayer)
						ENDIF
						
						SET_ENTITY_COORDS (scplayer, gVehBumpPlayerPlayerStartPos)
						SET_ENTITY_HEADING (scplayer, gVehBumpPlayerPlayerStartHeading)		
						
						gVehBumpPlayerInitialized = TRUE

					ENDIF	
				ENDIF	
				
				IF gStopVehicleIfPlayerRagdolls AND IS_PED_RAGDOLL(scplayer) 
					gVehicleSpeed = gVehicleSpeed * 0.5
					IF gVehicleSpeed < 0.01
						gVehicleSpeed = 0.0
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(TestVehicles[0].Vehicle) AND IS_VEHICLE_DRIVEABLE (TestVehicles[0].Vehicle)
					vehPos = GET_ENTITY_COORDS(TestVehicles[0].Vehicle)
					
					IF vehPos.y < vehBumpFrontPoint.y
						gVehBumpPlayerReversing = TRUE
					ELIF vehPos.y > vehBumpBackPoint.y
						gVehBumpPlayerReversing = FALSE
					ENDIF
					
					IF gVehBumpPlayerReversing 
						SET_VEHICLE_FORWARD_SPEED (TestVehicles[0].Vehicle, -gVehicleSpeed)
					ELSE
						SET_VEHICLE_FORWARD_SPEED (TestVehicles[0].Vehicle, gVehicleSpeed)					
					ENDIF
					
					SET_ENEMY_PEDS_FORCE_FALL_OFF_THIS_VEHICLE (TestVehicles[0].Vehicle, gVehBumpPlayerForceFallOff)
				ENDIF
				
			BREAK
		
			CASE NM_FALLING_FROM_HEIGHT
				DRAW_DEBUG_TEXT_2D ("High Fall Behaviour", vBehaviourTextScreenPos)
				Vector tempoffset
				tempoffset = GET_MAP_OFFSET
				int stick_left_x, stick_right_x, stick_left_y, stick_right_y
				 	
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
					IF IS_CAM_ACTIVE(TestCams[FixedCamera].cam)
						POINT_CAM_AT_ENTITY(TestCams[FixedCamera].cam, TestPeds[0].ped, <<0.0, 0.0, 0.0>>)
					ENDIF
				ENDIF

				SWITCH NFFH_STATUS
					CASE NFFH_SetPedPos
						IF not IS_PED_INJURED (TestPeds[0].ped)
							IF NOT IS_PED_RAGDOLL (TestPeds[0].ped)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[0].Ped, TRUE)
								SET_PED_GRAVITY(TestPeds[0].Ped, FALSE)
								GET_POSITION_OF_ANALOGUE_STICKS (PAD1, stick_left_x, stick_left_y, stick_right_x, stick_right_y )
								
								IF (stick_left_y >= -128) AND (stick_left_y < 0)
									CurrentPedPos.z += 0.25
								ENDIF
								
								IF (stick_left_y <= 128) AND (stick_left_y > 0)
									CurrentPedPos.z -= 0.25
								ENDIf
								
								
								SET_ENTITY_COORDS (TestPeds[0].ped, <<CurrentPedPos.x, CurrentPedPos.y, (CurrentPedPos.z + tempoffset.z)>>)
								
									IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
										NFFH_STATUS = NFFH_SetDropPed
										SET_ENTITY_VELOCITY(TestPeds[0].Ped, gInitialFallVelocity)
									ENDIF
							ELSE
							 NFFH_STATUS = NFFH_SetDropPed
							ENDIF
						ENDIF
					BREAK
					CASE NFFH_SetDropPed
						IF !IS_PED_INJURED(TestPeds[0].ped)
							SET_PED_GRAVITY(TestPeds[0].Ped, TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TestPeds[0].Ped, FALSE)
						ENDIF
						
						IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
								Cleanup_Scenario_Entities ()
								FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
									TestPeds[0].PedModel = PED_MODEL_SELECTOR()
									CREATE_PED_ENTITY(Testpeds[index])
									GiveBlipsToPedsGrps (Testpeds[index])
									ALTER_COMBAT_STATS (Testpeds[index] )
									SWAP_PED_WEAPONS (Testpeds[index])
									//ADD_RELATIONSHIP_GROUP (Testpeds[index])
									Block_Peds_Temp_Events (Testpeds[index], TRUE)
									SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
								ENDFOr
											
							NFFH_STATUS = NFFH_SetPedPos
						ENDIF
						
					BREAK
					
				ENDSWITCH
			BREAK
			
			CASE NM_STANDING_ON_MOVING_VEHICLE
				DRAW_DEBUG_TEXT_2D ("Balance Behaviour", vBehaviourTextScreenPos)
				
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
					IF IS_CAM_ACTIVE(TestCams[FixedCamera].cam)
						POINT_CAM_AT_ENTITY(TestCams[FixedCamera].cam, TestPeds[0].ped, <<0.0, 0.0, 0.0>>)
					ENDIF
				ENDIF
					
					MODEL_NAMES CurrentVehicleModel
					vector vModelMin, vModelMax, vVehicleCoords
					
					IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
							Cleanup_Scenario_Entities ()
							TestPeds[0].PedModel = PED_MODEL_SELECTOR()
							
							FOR index = 0 to MAX_NUMBER_OF_PEDS - 1	
								CREATE_PED_ENTITY(Testpeds[index])
								GiveBlipsToPedsGrps (Testpeds[index])
								ALTER_COMBAT_STATS (Testpeds[index] )
								SWAP_PED_WEAPONS (Testpeds[index])
								//ADD_RELATIONSHIP_GROUP (Testpeds[index])
								Block_Peds_Temp_Events (Testpeds[index], TRUE)
								SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
							ENDFOR
							
							FOR Index = 0 to MAX_NUMBER_OF_VEHICLES -1
								TestVehicles[0].VehicleModel = NMVehicleModelSelection ( )
								CREATE_VEHICLE_ENTITY (TestVehicles[index] )
							ENDFOR
						
						
						IF NOT IS_PED_INJURED (scplayer)
								if IS_VEHICLE_DRIVEABLE(TestVehicles[0].vehicle)
									IF NOT IS_PED_SITTING_IN_VEHICLE (scplayer, TestVehicles[0].vehicle)
										SET_PED_INTO_VEHICLE (scplayer, TestVehicles[0].vehicle )
									ENDIF	
								ENDIF		
								wait(0)
						ENDIF
					
						IF NOT IS_PED_INJURED (TestPeds[0].ped)
							IF IS_VEHICLE_DRIVEABLE (TestVehicles[0].Vehicle)
								CurrentVehicleModel = GET_ENTITY_MODEL (TestVehicles[0].Vehicle)
								GET_MODEL_DIMENSIONS (CurrentVehicleModel, vModelMin, vModelMax)
								vVehicleCoords = GET_ENTITY_COORDS (TestVehicles[0].Vehicle )
								
								vVehicleCoords.z += vModelMax.z			//set the ped on the roof of the vehicle assuming the roof is above the root bone
								
								SET_ENTITY_COORDS (TestPeds[0].ped, vVehicleCoords)
								SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)
							ENDIF
						ENDIF
						
						
					ENDIF
			
					IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
						IF NOT IS_PED_INJURED (scplayer)		
							
							IF NOT IS_PED_INJURED (TestPeds[0].ped)
								IF IS_VEHICLE_DRIVEABLE (TestVehicles[0].Vehicle)
									CurrentVehicleModel = GET_ENTITY_MODEL (TestVehicles[0].Vehicle)
									GET_MODEL_DIMENSIONS (CurrentVehicleModel, vModelMin, vModelMax)
									vVehicleCoords = GET_ENTITY_COORDS (TestVehicles[0].Vehicle )
									
									vVehicleCoords.z += vModelMax.z			//set the ped on the roof of the vehicle assuming the roof is above the root bone
									
									SET_ENTITY_COORDS (TestPeds[0].ped, vVehicleCoords)
									SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)
								ENDIF
							ENDIF

							if IS_VEHICLE_DRIVEABLE(TestVehicles[0].vehicle)
								IF NOT IS_PED_SITTING_IN_VEHICLE (scplayer, TestVehicles[0].vehicle)
									SET_PED_INTO_VEHICLE (scplayer, TestVehicles[0].vehicle )
								ENDIF	

								CLEAR_PED_TASKS (scplayer)	
								
								TEST_SEQ = DRIVE_TO_POINT_SEQUENCE (TestVehicles[0].vehicle, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS (TestVehicles[0].vehicle, <<0.0, 20.0, 0.0>> ), gVehicleSpeed)
								
							ENDIF		
	
							PedPerformsSequence (scplayer, TEST_SEQ)	
						ENDIF
					
					ENDIF
					
			BREAK
				
			CASE NM_PUNCHED_LOW_HEALTH
				DRAW_DEBUG_TEXT_2D ("Balance / FLinch Behaviour", vBehaviourTextScreenPos)
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
					IF IS_CAM_ACTIVE(TestCams[FixedCamera].cam)
						POINT_CAM_AT_ENTITY(TestCams[FixedCamera].cam, TestPeds[0].ped, <<0.0, 0.0, 0.0>>)
					ENDIF
				ENDIF
			
				IF NOT IS_PED_INJURED (TestPeds[0].ped)
					
					IF NOT IS_BUTTON_PRESSED (PAD1, LEFTSHOULDER2) 
						
						GunPos = GET_POINT_AROUND_PED_IN_SPHERICAL_COORDS (TestPeds[0].ped, gtestZoom, gtestPitch, gtestPan, TRUE,  TRUE, FALSE)
						
						gtestPan += 180
						
						TargetPos = GET_POINT_AROUND_PED_IN_SPHERICAL_COORDS (TestPeds[0].ped, gtestZoom, gtestPitch, gtestPan,  TRUE, TRUE, FALSE)
						
						gtestPan -= 180
						
						IF NOT IS_PED_RAGDOLL (TestPeds[0].ped)
							DRAW_DEBUG_SPHERE (GunPos, 0.05 )
							DRAW_LINE (GunPos, TargetPos) 
					
							//gTestPedheading = (GET_HEADING_FROM_VECTOR_2D (GunPos.x, TargetPos.y) )
					
							IF NOT IS_PED_INJURED (scplayer)						
								SET_ENTITY_COORDS(scplayer, <<GunPos.x, GunPos.y, GunPos.z-1>> )
								SET_ENTITY_HEADING (scplayer, gtestPan -90  )
							ENDIF
						
						ENDIF
					ENDIF	
				ENDIF		
					
				IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE) 
					IF NOT IS_BUTTON_PRESSED (PAD1, LEFTSHOULDER2)
						Cleanup_Scenario_Entities ()
						TestPeds[0].PedModel = PED_MODEL_SELECTOR()
						FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
							CREATE_PED_ENTITY(Testpeds[index])
							GiveBlipsToPedsGrps (Testpeds[index])
							ALTER_COMBAT_STATS (Testpeds[index] )
							SWAP_PED_WEAPONS (Testpeds[index])
							//ADD_RELATIONSHIP_GROUP (Testpeds[index])
							BLOCK_PEDS_TEMP_EVENTS (Testpeds[index], TRUE)
							SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
						ENDFOR
				
			
						IF NOT IS_PED_INJURED(TestPeds[0].ped)
							SET_ENTITY_HEALTH(TestPeds[0].ped, 200)
						ENDIF
					ENDIF	
				ENDIF
			BREAK
				
			CASE NM_JUMPING_FROM_VEHICLE
				DRAW_DEBUG_TEXT_2D ("Jump roll from road vehicle", vBehaviourTextScreenPos)
			
				MODEL_NAMES current_model
				
				SWITCH NJFV_Status
					CASE NJFV_Setup						
						 current_model = NMVehicleModelSelection ()
						
						createDebugVehicle(current_model, TestVehicles[0].Vehicle )
						NJFV_Status = NJFV_Run
						
						IF IS_VEHICLE_DRIVEABLE (TestVehicles[0].Vehicle)
							IF NOT IS_PED_INJURED (scplayer)	
								IF NOT  IS_PED_SITTING_IN_VEHICLE (scplayer , TestVehicles[0].Vehicle)
									SET_PED_INTO_VEHICLE (scplayer , TestVehicles[0].Vehicle)
								ENDIF
								SET_ENTITY_COORDS (TestVehicles[0].Vehicle, <<-19, 43.78,  6.13>> )
								SET_ENTITY_HEADING (TestVehicles[0].Vehicle, 180)
	
							ENDIF
						ENDIF
						
					BREAK
				
					CASE NJFV_Run
						IF IS_VEHICLE_DRIVEABLE (TestVehicles[0].Vehicle)
							IF NOT IS_PED_INJURED (scplayer)	
								SET_VEHICLE_FORWARD_SPEED (TestVehicles[0].Vehicle, gVehicleSpeed)
							ENDIF
						
							if IS_PED_RAGDOLL (scplayer)
								NJFV_Status = NJFV_WAIT	
							ENDIF						
						ENDIF

						
						IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE) 
							NJFV_Status = NJFV_Setup	
						ENDIF
						
					BREAK
					
				CASE NJFV_WAIT
						IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE) 
							NJFV_Status = NJFV_Setup	
						ENDIF
				BREAK
					
					
				ENDSWITCH
			BREAK

			CASE NM_GRAB_VEHICLE_DOOR
				DRAW_DEBUG_TEXT_2D ("Grab behaviour", vBehaviourTextScreenPos)
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						SET_ENTITY_COORDS (TestPeds[0].ped, TestPeds[0].PedsCoords)
						SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)
					ELSE
						Cleanup_Scenario_Entities ()
						TestPeds[0].PedModel = PED_MODEL_SELECTOR()
						FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
							CREATE_PED_ENTITY(Testpeds[index])
							GiveBlipsToPedsGrps (Testpeds[index])
							ALTER_COMBAT_STATS (Testpeds[index] )
							SWAP_PED_WEAPONS (Testpeds[index])
							//ADD_RELATIONSHIP_GROUP (Testpeds[index])
							Block_Peds_Temp_Events (Testpeds[index], TRUE)
							SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
						ENDFOR
					ENDIF	
							
					FOR Index = 0 to MAX_NUMBER_OF_VEHICLES -1
						CREATE_VEHICLE_ENTITY (TestVehicles[index] )
					ENDFOR
					
					IF NOT IS_PED_INJURED (scplayer)
						if IS_VEHICLE_DRIVEABLE(TestVehicles[0].vehicle)
							IF NOT IS_PED_SITTING_IN_VEHICLE (scplayer, TestVehicles[0].vehicle)
								SET_PED_INTO_VEHICLE (scplayer, TestVehicles[0].vehicle )
							ENDIF

							SET_ENTITY_COORDS (TestVehicles[0].vehicle, TestVehicles[0].VehicleCoords )
							SET_ENTITY_HEADING (TestVehicles[0].vehicle, TestVehicles[0].VehicleHeading )
						ENDIF
					ENDIF		
				ENDIF
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)		
			
					IF NOT IS_PED_INJURED (scplayer) AND NOT IS_PED_INJURED(TestPeds[0].ped)
						if IS_VEHICLE_DRIVEABLE(TestVehicles[0].vehicle)
							IF NOT IS_PED_SITTING_IN_VEHICLE (scplayer, TestVehicles[0].vehicle)
								SET_PED_INTO_VEHICLE (scplayer, TestVehicles[0].vehicle )
							ENDIF

							SET_ENTITY_COORDS (TestVehicles[0].vehicle, TestVehicles[0].VehicleCoords )
							SET_ENTITY_HEADING (TestVehicles[0].vehicle, TestVehicles[0].VehicleHeading )
													
							TASK_ENTER_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_JACK_ANYONE )
						ENDIF
					ENDIF
				ENDIF
				
				IF gGrabVehicleDoorLockDoor
					SET_VEHICLE_DOORS_LOCKED(TestVehicles[0].Vehicle, VEHICLELOCK_LOCKED)
				ELSE
					SET_VEHICLE_DOORS_LOCKED(TestVehicles[0].Vehicle, VEHICLELOCK_NONE)
				ENDIF		
			BREAK
				
			CASE NM_EXPLOSION
					
				DRAW_DEBUG_TEXT_2D ("Roll up, Writhe, Windmill and Flinch behaviour", vBehaviourTextScreenPos)
				vector vExplosionPos, vPedPos
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE) OR (bTestVehicleExplosion != bWasTestVehicleExplosion)
					bWasTestVehicleExplosion = bTestVehicleExplosion
					Cleanup_Scenario_Entities ()
					
					TestPeds[0].PedModel = PED_MODEL_SELECTOR()
					FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
						CREATE_PED_ENTITY(Testpeds[index])
						GiveBlipsToPedsGrps (Testpeds[index])
						ALTER_COMBAT_STATS (Testpeds[index] )
						SWAP_PED_WEAPONS (Testpeds[index])
						//ADD_RELATIONSHIP_GROUP (Testpeds[index])
						Block_Peds_Temp_Events (Testpeds[index], TRUE)
						SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
					ENDFOR
					
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						SET_ENTITY_COORDS (TestPeds[0].ped, TestPeds[0].PedsCoords)
						SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)
						SET_ENTITY_HEALTH (TestPeds[0].ped , giPedHealth )
					ENDIF						
					
					IF (bTestVehicleExplosion)
						TestVehicles[0].VehicleModel = NMVehicleModelSelection ( )
						CREATE_VEHICLE_ENTITY (TestVehicles[0] )
						IF NOT IS_PED_INJURED (TestPeds[0].ped)
							IF IS_VEHICLE_DRIVEABLE(TestVehicles[0].vehicle)
								IF NOT IS_PED_SITTING_IN_VEHICLE (TestPeds[0].ped, TestVehicles[0].vehicle)
									SET_PED_INTO_VEHICLE (TestPeds[0].ped, TestVehicles[0].vehicle )
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//track ped with cam
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
					IF IS_CAM_ACTIVE(TestCams[FixedCamera].cam)
						POINT_CAM_AT_ENTITY(TestCams[FixedCamera].cam, TestPeds[0].ped, <<0.0, 0.0, 0.0>>)
					ENDIF
				ENDIF
				
//				IF (bTestVehicleExplosion)
//					IF IS_VEHICLE_DRIVEABLE(TestVehicles[0].Vehicle)
//						IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_ANY_OBJECT(TestVehicles[0].Vehicle)
//							vExplosionPos = GET_POINT_AROUND_VEHICLE_IN_SPHERICAL_COORDS (TestVehicles[0].Vehicle,gtestZoom, gtestPitch, gtestPan )
//							vPedPos = GET_ENTITY_COORDS (TestVehicles[0].Vehicle )	
//						ENDIF
//					ENDIF
//				ELSE
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						IF NOT IS_PED_RAGDOLL (TestPeds[0].ped)
							vExplosionPos = GET_POINT_AROUND_PED_IN_SPHERICAL_COORDS (TestPeds[0].ped,gtestZoom, gtestPitch, gtestPan )
							vPedPos = GET_ENTITY_COORDS (TestPeds[0].ped )
							SET_PED_CAN_EVASIVE_DIVE (TestPeds[0].ped, FALSE)
						ENDIF
					ENDIF
				//ENDIF
				
				DRAW_DEBUG_SPHERE (vExplosionPos, 0.05 )
				DRAW_LINE (vExplosionPos, vPedPos) 
					
				fExplosionDistance = VDIST2 (vPedPos, vExplosionPos)
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
					ADD_EXPLOSION (vExplosionPos,   SELECT_EXPLOSION_TYPE(), ExplosionScale, true, bNoExplosionEffects   )
				ENDIF
			
			BREAK
				
			CASE NM_ON_FIRE
				DRAW_DEBUG_TEXT_2D ("Writhe behaviour", vBehaviourTextScreenPos)
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
					IF IS_CAM_ACTIVE(TestCams[FixedCamera].cam)
						POINT_CAM_AT_ENTITY(TestCams[FixedCamera].cam, TestPeds[0].ped, <<0.0, 0.0, 0.0>>)
					ENDIF
				ENDIF
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						START_ENTITY_FIRE (TestPeds[0].ped)
					ENDIF
				ENDIF
				
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
//					IF NOT IS_PED_INJURED (TestPeds[0].ped)
//						SET_ENTITY_COORDS (TestPeds[0].ped, TestPeds[0].PedsCoords)
//						SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)
//					ELSE
						Cleanup_Scenario_Entities ()
						TestPeds[0].PedModel = PED_MODEL_SELECTOR()
						
						FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
							CREATE_PED_ENTITY(Testpeds[index])
							GiveBlipsToPedsGrps (Testpeds[index])
							ALTER_COMBAT_STATS (Testpeds[index] )
							SWAP_PED_WEAPONS (Testpeds[index])
							//ADD_RELATIONSHIP_GROUP (Testpeds[index])
							Block_Peds_Temp_Events (Testpeds[index], TRUE)
							SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
						ENDFOR
//					ENDIF	
				ENDIF
			BREAK	
				
			CASE NM_RELAX_TEST	
				DRAW_DEBUG_TEXT_2D ("Relax behaviour", vBehaviourTextScreenPos)
				IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
					CREATE_NM_MESSAGE (NM_START_START, NM_RELAX_MSG)
					SET_NM_MESSAGE_FLOAT (NM_RELAX_RELAXATION, NM_Relaxation)
					SET_NM_MESSAGE_BOOL (NM_RELAX_HOLD_POSE,bHoldPose )
					
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						SET_PED_TO_RAGDOLL (TestPeds[0].ped, 4000, 10000, TASK_NM_SCRIPT)
						GIVE_PED_NM_MESSAGE (TestPeds[0].ped)
					ENDIF
				ENDIF
			
			
				IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
						Cleanup_Scenario_Entities ()
						TestPeds[0].PedModel = PED_MODEL_SELECTOR()
						FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
							CREATE_PED_ENTITY(Testpeds[index])
							GiveBlipsToPedsGrps (Testpeds[index])
							ALTER_COMBAT_STATS (Testpeds[index] )
							SWAP_PED_WEAPONS (Testpeds[index])
							//ADD_RELATIONSHIP_GROUP (Testpeds[index])
							Block_Peds_Temp_Events (Testpeds[index], TRUE)
							SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
						ENDFOR
				ENDIF
			
			BREAK
	
			CASE NM_FLINCH
				VECTOR vImpactPos
					DRAW_DEBUG_TEXT_2D ("Flinch behaviour", vBehaviourTextScreenPos)
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						 vImpactPos = GET_POINT_AROUND_PED_IN_SPHERICAL_COORDS (TestPeds[0].ped , gtestZoom, gtestPitch, gtestPan)
						 vPedPos = GET_ENTITY_COORDS (TestPeds[0].ped )
				
						IF NOT IS_PED_RAGDOLL (TestPeds[0].ped  )
							DRAW_DEBUG_SPHERE (vImpactPos, 0.05 )
							DRAW_LINE (vImpactPos, vPedPos) 	
						ENDIF
					ENDIF

				IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
					CREATE_NM_MESSAGE (NM_START_START, NM_FLINCH_MSG)
					SET_NM_MESSAGE_BOOL (NM_FLINCH_RIGHT_HANDED, bNM_FLINCH_RIGHT_HANDED)	
					SET_NM_MESSAGE_BOOL (NM_FLINCH_LEFT_HANDED, bNM_FLINCH_LEFT_HANDED)	
					SET_NM_MESSAGE_FLOAT(NM_FLINCH_HAND_DIST_LEFTRIGHT, fNM_FLINCH_HAND_DIST_LEFTRIGHT)
					SET_NM_MESSAGE_FLOAT(NM_FLINCH_HAND_DIST_FRONTBACK, fNM_FLINCH_HAND_DIST_FRONTBACK )
					SET_NM_MESSAGE_FLOAT(NM_FLINCH_HAND_DIST_VERTICAL, fNM_FLINCH_HAND_DIST_VERTICAL )
					SET_NM_MESSAGE_FLOAT(NM_FLINCH_BODY_STIFFNESS, fNM_FLINCH_BODY_STIFFNESS )
					SET_NM_MESSAGE_VEC3(NM_FLINCH_POS_VEC3, vImpactPos )
					SET_NM_MESSAGE_FLOAT(NM_FLINCH_BODY_DAMPING, fNM_FLINCH_BODY_DAMPING )
					SET_NM_MESSAGE_FLOAT(NM_FLINCH_BACK_BEND, fNM_FLINCH_BACK_BEND )
					SET_NM_MESSAGE_BOOL (NM_FLINCH_USE_LEFT_ARM, bNM_FLINCH_USE_LEFT_ARM)	
					SET_NM_MESSAGE_BOOL (NM_FLINCH_USE_RIGHT_ARM, bNM_FLINCH_USE_RIGHT_ARM)	
					SET_NM_MESSAGE_FLOAT(NM_FLINCH_NOISE_SCALE, fNM_FLINCH_NOISE_SCALE )
					SET_NM_MESSAGE_BOOL (NM_FLINCH_NEW_HIT, bNM_FLINCH_NEW_HIT)	
					SET_NM_MESSAGE_INT (NM_FLINCH_TURN_TOWARDS, iNM_FLINCH_TURN_TOWARDS)	
					
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						SET_PED_TO_RAGDOLL (TestPeds[0].ped, MIN_TASK_TIME, MAX_TASK_TIME, TASK_NM_SCRIPT)
						GIVE_PED_NM_MESSAGE (TestPeds[0].ped)
					
						CREATE_NM_MESSAGE (NM_START_START, NM_BALANCE_MSG)
						GIVE_PED_NM_MESSAGE (TestPeds[0].ped)
					ENDIF
				ENDIF
			
			
				IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
						Cleanup_Scenario_Entities ()
						TestPeds[0].PedModel = PED_MODEL_SELECTOR()
						FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
							CREATE_PED_ENTITY(Testpeds[index])
							GiveBlipsToPedsGrps (Testpeds[index])
							ALTER_COMBAT_STATS (Testpeds[index] )
							SWAP_PED_WEAPONS (Testpeds[index])
							//ADD_RELATIONSHIP_GROUP (Testpeds[index])
							Block_Peds_Temp_Events (Testpeds[index], TRUE)
							SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
						ENDFOR
				ENDIF
			BREAK
				
			CASE NM_BALANCE_TEST	
				DRAW_DEBUG_TEXT_2D ("Balance behaviour", vBehaviourTextScreenPos)
				IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
					CREATE_NM_MESSAGE (NM_START_START, NM_BALANCE_MSG)
						
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						SET_PED_TO_RAGDOLL (TestPeds[0].ped, MIN_TASK_TIME, MAX_TASK_TIME, TASK_NM_SCRIPT)
						GIVE_PED_NM_MESSAGE (TestPeds[0].ped)
					ENDIF
				ENDIF
			
			
				IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
					Cleanup_Scenario_Entities ()
					TestPeds[0].PedModel = PED_MODEL_SELECTOR()
					FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
						CREATE_PED_ENTITY(Testpeds[index])
						GiveBlipsToPedsGrps (Testpeds[index])
						ALTER_COMBAT_STATS (Testpeds[index] )
						SWAP_PED_WEAPONS (Testpeds[index])
						//ADD_RELATIONSHIP_GROUP (Testpeds[index])
						Block_Peds_Temp_Events (Testpeds[index], TRUE)
						SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
					ENDFOR
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						SET_ENTITY_COORDS (TestPeds[0].ped, TestPeds[0].PedsCoords)
						SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)	
					ENDIF
				
				ENDIF
				
			BREAK
			
			CASE NM_BINDPOSE_TEST	
				DRAW_DEBUG_TEXT_2D ("BindPose transition behaviour", vBehaviourTextScreenPos)
				
				IF(bNM_BINDPOSE_FIRST_TIME)
					TASK_BIND_POSE(TestPeds[0].Ped, bDO_BLEND_FROM_NM)
					bNM_BINDPOSE_FIRST_TIME = FALSE
				ENDIF
								
				IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						TOGGLE_NM_BINDPOSE_TASK(TestPeds[0].ped, bDO_BLEND_FROM_NM)
					ENDIF
				ENDIF
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
					bNM_BINDPOSE_FIRST_TIME = TRUE
					Cleanup_Scenario_Entities ()
					TestPeds[0].PedModel = PED_MODEL_SELECTOR()
					FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
						CREATE_PED_ENTITY(Testpeds[index])
						GiveBlipsToPedsGrps (Testpeds[index])
						ALTER_COMBAT_STATS (Testpeds[index] )
						SWAP_PED_WEAPONS (Testpeds[index])
						//ADD_RELATIONSHIP_GROUP (Testpeds[index])
						Block_Peds_Temp_Events (Testpeds[index], TRUE)
						SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
					ENDFOR
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						SET_ENTITY_COORDS (TestPeds[0].ped, TestPeds[0].PedsCoords)
						SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)	
					ENDIF
				
				ENDIF
				
			BREAK
			
			CASE NM_ATTACH_UNDERNEATH
				DRAW_DEBUG_TEXT_2D("Attach underneath test", vBehaviourTextScreenPos)

				IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
					IF NOT bIsTestRunning
						IF NOT IS_PED_INJURED (scplayer)
							IF IS_VEHICLE_DRIVEABLE(TestVehicles[0].vehicle)
								IF NOT IS_PED_SITTING_IN_VEHICLE (scplayer, TestVehicles[0].vehicle)
									SET_PED_INTO_VEHICLE (scplayer, TestVehicles[0].vehicle )
								ENDIF

								SET_ENTITY_COORDS (TestVehicles[0].vehicle, TestVehicles[0].VehicleCoords )
								SET_ENTITY_HEADING (TestVehicles[0].vehicle, TestVehicles[0].VehicleHeading )
							ENDIF
						ENDIF
						
						
						IF NOT IS_PED_INJURED (TestPeds[0].ped)
							if IS_VEHICLE_DRIVEABLE(TestVehicles[0].Vehicle)
							SET_ENTITY_INVINCIBLE(TestPeds[0].ped, true)
							TASK_NM_ATTACH_PED_TO_ENTITY(TestPeds[0].ped, TestVehicles[0].Vehicle, 0, <<0.0, -0.5, 0.0>>,  <<0.0, 0.0, 0.0>>,  NAC_LEFT_FOOT | NAC_RIGHT_FOOT| NAC_LEFT_HAND| NAC_RIGHT_HAND
								| NAC_LEFT_FOREARM | NAC_RIGHT_FOREARM | NAC_LEFT_SHIN | NAC_RIGHT_SHIN
								| NAC_LEFT_THIGH | NAC_RIGHT_THIGH,   "nm", "hang_truck_geton", "hang_truck_idle" )
							SET_ENTITY_COLLISION(TestPeds[0].ped, FALSE)
							bIsTestRunning = true
							
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
					
					
				IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
					IF  bIsTestRunning
						IF NOT IS_PED_INJURED (TestPeds[0].ped)
							SET_ENTITY_COLLISION(TestPeds[0].ped, TRUE)
							CLEAR_PED_TASKS(TestPeds[0].ped)
							SET_ENTITY_COORDS (TestPeds[0].ped, TestPeds[0].PedsCoords)
							SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)
						ENDIF
						
						Cleanup_Scenario_Entities ()
						TestPeds[0].PedModel = PED_MODEL_SELECTOR()
						FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
							CREATE_PED_ENTITY(Testpeds[index])
							CREATE_VEHICLE_ENTITY (TestVehicles[index] )
							GiveBlipsToPedsGrps (Testpeds[index])
							ALTER_COMBAT_STATS (Testpeds[index] )
							SWAP_PED_WEAPONS (Testpeds[index])
							//ADD_RELATIONSHIP_GROUP (Testpeds[index])
							Block_Peds_Temp_Events (Testpeds[index], TRUE)
							SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
						ENDFOR
						bIsTestRunning = FALSE
					ENDIF
				ENDIF	
			BREAK
			
			CASE NM_ATTACH_IDLE
				DRAW_DEBUG_TEXT_2D("Attach idle test", vBehaviourTextScreenPos)
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
					IF NOT bIsTestRunning
						IF NOT IS_PED_INJURED (scplayer)
							IF IS_VEHICLE_DRIVEABLE(TestVehicles[0].vehicle)
								IF NOT IS_PED_SITTING_IN_VEHICLE (scplayer, TestVehicles[0].vehicle)
									SET_PED_INTO_VEHICLE (scplayer, TestVehicles[0].vehicle )
								ENDIF

								SET_ENTITY_COORDS (TestVehicles[0].vehicle, TestVehicles[0].VehicleCoords )
								SET_ENTITY_HEADING (TestVehicles[0].vehicle, TestVehicles[0].VehicleHeading )
							ENDIF
						ENDIF
						
						
						IF NOT IS_PED_INJURED (TestPeds[0].ped)
							if IS_VEHICLE_DRIVEABLE(TestVehicles[0].Vehicle)
								
							SET_ENTITY_INVINCIBLE(TestPeds[0].ped, true)
							SET_ENTITY_COLLISION(TestPeds[0].ped, FALSE)
							//TASK_NM_ATTACH_PED_TO_ENTITY(TestPeds[0].ped, TestVehicles[0].Vehicle, 1, /*<<0.5, 2.0, 2.5>>*/ <<0.0,- 0.35, 0.0>>,  <<0.0, 0.0, 0.0>>,  NAC_LEFT_FOOT | NAC_RIGHT_FOOT| NAC_LEFT_HAND| NAC_RIGHT_HAND,   "nm", "stand_truck_getin", "stand_truck_loop" )
							
							TASK_ENTER_VEHICLE(TestPeds[0].ped, TestVehicles[0].Vehicle, 1, VS_EXTRA_LEFT_1, 0.0, ECF_WARP_PED )
							
							bIsTestRunning = true
							
							ENDIF	
						ENDIF
					ENDIF
				ENDIF	
					
					
				IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
					IF  bIsTestRunning
						IF NOT IS_PED_INJURED (TestPeds[0].ped)
							SET_ENTITY_COLLISION(TestPeds[0].ped, TRUE)
							CLEAR_PED_TASKS(TestPeds[0].ped)
							SET_ENTITY_COORDS (TestPeds[0].ped, TestPeds[0].PedsCoords)
							SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)
						ENDIF
						
						Cleanup_Scenario_Entities ()
						TestPeds[0].PedModel = PED_MODEL_SELECTOR()
						FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
							CREATE_PED_ENTITY(Testpeds[index])
							CREATE_VEHICLE_ENTITY (TestVehicles[index] )
							GiveBlipsToPedsGrps (Testpeds[index])
							ALTER_COMBAT_STATS (Testpeds[index] )
							SWAP_PED_WEAPONS (Testpeds[index])
							//ADD_RELATIONSHIP_GROUP (Testpeds[index])
							Block_Peds_Temp_Events (Testpeds[index], TRUE)
							SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
						ENDFOR
						bIsTestRunning = FALSE
					ENDIF
				ENDIF
			BREAK
			
			CASE NM_WALKING_DOWN_HILL
				DRAW_DEBUG_TEXT_2D ("Walking down ski route", vBehaviourTextScreenPos)
				
				IF NOT IS_PED_INJURED (TestPeds[0].ped)
					REQUEST_RAGDOLL_BOUNDS_UPDATE(TestPeds[0].ped)
				ENDIF
				
				SWITCH NRS_Status
					CASE NRS_GivePedTask
						IF NOT IS_PED_INJURED(TestPeds[0].ped)
							TEST_SEQ = TEST_ROUTE(WALK_DOWN_HILL_DYNAMIC_MOVE_TYPE) 
							PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)	
						ENDIF
						NRS_Status = NRS_ShootPed
					BREAK
					
					CASE NRS_ShootPed
						IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
							Cleanup_Scenario_Entities ()
						
							FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
								TestPeds[0].PedModel = PED_MODEL_SELECTOR()
								
								CREATE_PED_ENTITY(Testpeds[index])
								GiveBlipsToPedsGrps (Testpeds[index])
								ALTER_COMBAT_STATS (Testpeds[index] )
								SWAP_PED_WEAPONS (Testpeds[index])
								//ADD_RELATIONSHIP_GROUP (Testpeds[index])
								Block_Peds_Temp_Events (Testpeds[index], TRUE)
								SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
							ENDFOR
							
							IF NOT IS_PED_INJURED (TestPeds[0].ped)	
								CLEAR_PED_TASKS(TestPeds[0].ped)
								SET_ENTITY_COORDS (TestPeds[0].ped, Route[0].RouteNode)
								SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)
								NRS_Status = NRS_GivePedTask						
							ENDIF	
						ENDIF
						
						IF NOT IS_PED_INJURED (TestPeds[0].ped)		
							
							GunPos = GET_POINT_AROUND_PED_IN_SPHERICAL_COORDS (TestPeds[0].ped, gtestZoom, gtestPitch, gtestPan)
							TargetPos =  SELECT_PED_COMPONENT (TestPeds[0].ped)   // GET_OFFSET_FROM_PED_IN_WORLD_COORDS (TestPeds[0].ped, SELECT_PED_COMPONENT (TestPeds[0].ped) )
				
							secondtargetPos = (GunPos + 2.0*( TargetPos - GunPos ))
													
							IF NOT IS_PED_RAGDOLL (TestPeds[0].ped)
								DRAW_DEBUG_SPHERE (GunPos, 0.05, 0, 0, 255,255 )
								DRAW_DEBUG_SPHERE (TargetPos, 0.05, 255, 0,0,255 )
								DRAW_DEBUG_LINE_WITH_TWO_COLOURS (GunPos, secondtargetPos, 0,0,255, 255, 255, 0, 0, 255 ) 
							ENDIF
						ENDIF
						
						IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
							gbshootPed = true
						
							IF (gbshootPed)
								SHOOT_SINGLE_BULLET_BETWEEN_COORDS (GunPos, secondtargetPos, 0, true )
								gbshootPed = FALSE
							ENDIF
					
						ENDIF
					
					BREAK
							
				ENDSWITCH
			BREAK
			
			CASE NM_TRUCK_BED
				DRAW_DEBUG_TEXT_2D ("Truck Bed", vBehaviourTextScreenPos)
								
				SWITCH NJFV_Status
				
					CASE NJFV_Setup
						Cleanup_Scenario_Entities ()
						TestPeds[0].PedModel = PED_MODEL_SELECTOR()
						FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
							CREATE_PED_ENTITY(Testpeds[index])
							GiveBlipsToPedsGrps (Testpeds[index])
							ALTER_COMBAT_STATS (Testpeds[index] )
							SWAP_PED_WEAPONS (Testpeds[index])
							Block_Peds_Temp_Events (Testpeds[index], TRUE)
							SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
						ENDFOR
					
						SET_ENTITY_COORDS (TestPeds[0].ped, TestPeds[0].PedsCoords + vOffset)
						SET_ENTITY_HEADING (TestPeds[0].ped, TestPeds[0].PedHeading)
						
						IF bFlatbedTruck = TRUE
							TestVehicles[0].VehicleModel = PHANTOM
							TestVehicles[1].VehicleModel = TRFLAT
						ELSE
							TestVehicles[0].VehicleModel = BODHI2
							TestVehicles[1].VehicleModel = DUMMY_MODEL_FOR_SCRIPT
						ENDIF

						FOR index = 0 to MAX_NUMBER_OF_VEHICLES - 1
							CREATE_VEHICLE_ENTITY (TestVehicles[index])
						ENDFOR
					
						CLEAR_PED_TASKS (scplayer)	
						IF IS_VEHICLE_DRIVEABLE(TestVehicles[0].vehicle)
							SET_ENTITY_COORDS (TestVehicles[0].vehicle, TestVehicles[0].VehicleCoords)
							SET_ENTITY_HEADING (TestVehicles[0].vehicle, TestVehicles[0].VehicleHeading)	
							SET_PED_INTO_VEHICLE (scplayer, TestVehicles[0].vehicle)
				            SET_VEHICLE_NEEDS_TO_BE_HOTWIRED (TestVehicles[0].vehicle, FALSE)
				            SET_VEHICLE_ENGINE_ON (TestVehicles[0].vehicle, TRUE, TRUE)
												
							IF DOES_ENTITY_EXIST(TestVehicles[1].Vehicle) AND NOT IS_VEHICLE_ATTACHED_TO_TRAILER(TestVehicles[0].Vehicle)
								ATTACH_VEHICLE_TO_TRAILER(TestVehicles[0].Vehicle, TestVehicles[1].Vehicle)
							ENDIF
						ENDIF
						WAIT(0)
						
						NJFV_Status = NJFV_Run
					BREAK
					
					CASE NJFV_Run
						IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
							NJFV_Status = NJFV_Wait
						ELIF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
							NJFV_Status = NJFV_Setup
						ENDIF
					BREAK
					
					CASE NJFV_Wait
						IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
							NJFV_Status = NJFV_Setup
						ENDIF
					
						SET_VEHICLE_FORWARD_SPEED (TestVehicles[0].Vehicle, gVehicleSpeed)
					BREAK
				
				ENDSWITCH
			BREAK
			
			CASE NM_COVER
				DRAW_DEBUG_TEXT_2D ("Cover", vBehaviourTextScreenPos)
				IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS)
					IF NOT IS_PED_INJURED (TestPeds[1].Ped)
						ELECTROCUTE_PED (TestPeds[1].Ped, nElectrocuteTime, nElectrocuteTime)
					ENDIF

					SET_PED_RAGDOLL_ON_COLLISION (TestPeds[0].Ped, bRagdollOnCollision)
					
					SETTIMERA(0)
					bCheckShouldSeekCover = TRUE
				ENDIF

				IF TIMERA() > nElectrocuteWaitTime AND bCheckShouldSeekCover = TRUE
					IF NOT IS_PED_INJURED (TestPeds[0].Ped)
						TASK_SEEK_COVER_FROM_PED (TestPeds[0].Ped, scplayer, -1)
					ENDIF

					bCheckShouldSeekCover = FALSE
				ENDIF
				
				IF IS_BUTTON_JUST_PRESSED (PAD1, DPADDOWN)
					IF NOT IS_PED_INJURED (TestPeds[0].Ped)
						SET_PED_TO_RAGDOLL (TestPeds[0].Ped, nElectrocuteTime, nElectrocuteTime, TASK_RELAX)
					ENDIF
				ENDIF
			
				IF IS_BUTTON_JUST_PRESSED (PAD1, SQUARE)
					Cleanup_Scenario_Entities ()
					TestPeds[0].PedModel = PED_MODEL_SELECTOR()
					FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
						CREATE_PED_ENTITY(Testpeds[index])
						GiveBlipsToPedsGrps (Testpeds[index])
						ALTER_COMBAT_STATS (Testpeds[index])
						SWAP_PED_WEAPONS (Testpeds[index])
						Block_Peds_Temp_Events (Testpeds[index], TRUE)
						SET_PED_DEFENSIVE_AREAS (Testpeds[index])
					ENDFOR
					IF NOT IS_PED_INJURED (TestPeds[0].Ped)
						SET_ENTITY_COORDS (TestPeds[0].Ped, TestPeds[0].PedsCoords + vOffset)
						SET_ENTITY_HEADING (TestPeds[0].Ped, TestPeds[0].PedHeading)
						SET_PED_RAGDOLL_ON_COLLISION (TestPeds[0].Ped, FALSE)
					ENDIF
					IF NOT IS_PED_INJURED (TestPeds[1].Ped)
						SET_ENTITY_COORDS (TestPeds[1].Ped, TestPeds[1].PedsCoords + vOffsetVec1)
						SET_ENTITY_HEADING (TestPeds[1].Ped, TestPeds[1].PedHeading)
						SET_PED_CONFIG_FLAG (TestPeds[1].Ped, PCF_AllowNearbyCoverUsage, TRUE)
					ENDIF

					CLEAR_PED_TASKS (scplayer)
					SET_ENTITY_COORDS (scplayer, <<-36.8, -26.1, 7.1>> + vOffsetVec2)
					SET_ENTITY_HEADING (scplayer, 0.0)
					
					bCheckShouldSeekCover = FALSE
				ENDIF
				
			BREAK
											
			CASE NM_SLIDE_TEST
			
				IF NOT IS_ENTITY_DEAD(scplayer) AND IS_BUTTON_JUST_PRESSED(PAD1, CROSS)
					SET_ENTITY_COORDS(scplayer, <<18.86, -272.20, 21.68>>)
					SET_ENTITY_HEADING(scplayer, gShot2OrientationOffset)
				ENDIF
				
			BREAK
			
			CASE NM_WARP_TO_INTERIOR
			
				IF NOT IS_ENTITY_DEAD(scplayer)
					SET_ENTITY_COORDS(scplayer, <<152.644, -132.157, 29.47>>)
				ENDIF
				
				Set_Test_State_To_Default()
				
			BREAK
			
		ENDSWITCH	
ENDPROC
//Purpose: Runs the main scenarios
PROC Run_Test_Scenario ()
	int index = 0

	SWITCH TestScenarioAStatus
		//setps up all the data for the sceanrio, this is only called once or if selected in widget
		CASE InitialiseScenarioData
			PlayerStartPos = GET_PLAYER_START_POS()				//sets the players start coords at the default
			Bscenario_running = FALSE
			gbPlayerShouldBeAtCamPos = TRUE
			gbPrintGlobalText = TRUE
			bAutoSwitchToTrackCam = FALSE
			
			INITIALISE_PED_DATA (Testpeds)
			INITIALISE_VEHICLE_DATA (TestVehicles)
			INITIALISE_CAM_DATA(TestCams )
			INITIALISE_OBJECT_DATA (TestObject)
			
			SETUP_TEST_DATA ()
	
			HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
			TestScenarioAStatus = CreateScenarioEntities
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)		//sets the 
		
		BREAK
		
		// Creates all the scenario data
		CASE CreateScenarioEntities
			Bscenario_running = FALSE
			
			//Set_Gang_Relationships (TRUE)	
		
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
		
			//peds
			FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
				CREATE_PED_ENTITY(Testpeds[index])
				GiveBlipsToPedsGrps (Testpeds[index])
				ALTER_COMBAT_STATS (Testpeds[index] )
				SWAP_PED_WEAPONS (Testpeds[index])
				//ADD_RELATIONSHIP_GROUP (Testpeds[index])
				Block_Peds_Temp_Events (Testpeds[index], TRUE)
				SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
			ENDFOR
			
			//TestVehicles
			FOR Index = 0 to MAX_NUMBER_OF_VEHICLES -1
				CREATE_VEHICLE_ENTITY (TestVehicles[index] )
			ENDFOR
		
			//TestCams
			for index = 0 to MAX_NUMBER_OF_CAMERAS -1
				CREATE_CAM_ENTITY (TestCams[index])
			ENDFOR
			
			for index = 0 to MAX_NUMBER_OF_OBJECTS -1
				CREATE_OBJECT_ENTITY(TestObject[index])
			ENDFOR
			
			TestScenarioAStatus = SetScenarioEntities
		BREAK
		
		CASE SetScenarioEntities			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
			
			Start_And_Reset_Test ()
			
		//	IF gBeginCombatScenario
				TestScenarioAStatus = RunScenario
				gBeginCombatScenario = FALSE
				gRun_debuggig = FALSE
				Bscenario_running = TRUE
				INITALISE_TEST_STATE()
				TEMP_ACTIVATE_CAMS (TestCams[FixedCamera].cam )
				
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam )
					HELP_TEXT_STATE =  HTF_SCENARIO_RUNNING_FIXED_CAM
				ELSE
					HELP_TEXT_STATE = HTF_STARTED_RUNNING_NO_CAMS
				ENDIF
		//	ENDIF
		BREAK
		
		//Runs the actual selected scenario
		
		CASE RunScenario
			CAM_MANAGER (TestCams[TrackingCamera].cam, TestCams[FixedCamera].cam, MAX_NUMBER_OF_PEDS, TestPeds)
				
			//NM_Temp_Run_Scenario_Tracking_Cam (TestPeds, MAX_NUMBER_OF_PEDS, TestCams[TrackingCamera].cam, bAutoSwitchToTrackCam) 
			
			//bAutoSwitchToTrackCam = FALSE
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
				
			Start_And_Reset_Test ()
				
		 	Check_For_Scenario_Reset ()
			
			IF DOES_CAM_EXIST (TestCams[TrackingCamera].cam)
				if IS_CAM_RENDERING (TestCams[TrackingCamera].cam)
					HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_TRACKING_CAM
				ELSE
					HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_FIXED_CAM
				ENDIF
			ENDIF
					
			RUN_TEST ()		//run the main tests 		
			
		BREAK 
				

		CASE CleanupScenario
			
			HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
	
			Cleanup_Scenario_Entities ()
			
			IF gResetToDefault 	
				Temp_cleanup_scenario_cams ()		//here we are changing scenarios so we need to reset cams
				TestScenarioAStatus = InitialiseScenarioData
				gResetToDefault = FALSE
			ENDIF
		
			IF gResetCombatScenario
				Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)
				gcurrentselection = gSelection 
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
					ACTIVATE_CAM (TestCams[FixedCamera].cam)
				ELSE
					Temp_cleanup_scenario_cams ()	
				ENDIF
				TestScenarioAStatus = CreateScenarioEntities
				gResetCombatScenario = FALSE
			ENDIF
		
		BREAK
		
	ENDSWITCH
ENDPROC


SCRIPT

	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
    ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
    ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
    ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
    ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	
	SET_DEBUG_ACTIVE (TRUE)
	
	SETUP_MISSION_XML_MENU (XMLMenu,  KEY_Q)

	SETUP_AREA_FOR_MISSION (<<0.0, 0.0, 0.0>>)
	
	//Gets a reference to the player
	Get_The_Player ()
	
	SET_PLAYER_COLISION(scplayer, true)
	
	SET_ENTITY_INVINCIBLE(scplayer, TRUE)

	// Make sure the ski route control points contain valid data before creating the RAG widgets.
	RESET_NM_SKI_TEST_ROUTE()
	//Sets the test widget for this script
	CREATE_TEST_WIDGET ()
	
	SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
	WHILE TRUE
	
		IF NOT DOES_ENTITY_EXIST(scplayer)
			//Gets a reference to the player
			Get_The_Player ()
	
			SET_PLAYER_COLISION(scplayer, true)
		ENDIF

		// Don't want it getting dark in debug level.
		SET_CLOCK_TIME(12, 00, 00)

		// controls the help text hides if xml menu is active
		TEXT_CONTROLLER ()
	
		// Can set all scenario peds invincible from the widget
		Set_Scenario_Peds_Invincible (TestPeds, MAX_NUMBER_OF_PEDS, gsetpedsinvincible )
	
		//User can create a debug cam for setting sceanrios
		Temp_Create_Debug_Cam (TestCams[FixedCamera]) 
		
		//Runs the selected option from the XML menu 
		Run_Selection_From_XML_input ()
		//Checks that a valid selection has been input and runs the scenario
		IF (gcurrentselection <> InvalidSelection)
	
			Draw_Debug_Info (  )
			
			//Sets the test scenario into debug mode
			Set_To_Debug ()
			
			if (gRun_debuggig)
				Temp_Debug_Scenario (TestPeds, TestVehicles, Route, TestCams[FixedCamera].cam, TestVehicles[0].VehicleModel) //Allows the entities in the scenario to be adjusted
				Print_Scenario_Data (TestPeds, TestVehicles, Route, TestCams)
			ENDIF
			
			Run_Test_Scenario ()								
			
			NM_PRINT_ACTIVE_TEST (Bscenario_running, gRun_debuggig, TestCams[TrackingCamera].cam,TestCams[FixedCamera].cam )		
		ENDIF
		
		WAIT (0)
		
		Terminate_test_script ()
		
	
	ENDWHILE

ENDSCRIPT
