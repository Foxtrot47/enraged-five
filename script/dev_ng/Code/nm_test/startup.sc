USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_streaming.sch"
USING "commands_script.sch"
USING "commands_player.sch"
USING "commands_debug.sch"
USING "commands_ped.sch"
USING "commands_camera.sch"
USING "commands_hud.sch"
USING "stack_sizes.sch"
USING "script_DEBUG.sch"

//PURPOSE: Returns the offset to the origin of a specific map
VECTOR GET_MAP_OFFSET=<<0.0, 0.0, 6.127>>

// Mission Start Coords
CONST_FLOAT	START_X		17.0
CONST_FLOAT	START_Y		-29.0
CONST_FLOAT	START_Z		0.0
CONST_FLOAT	MAIN_START_HEADING		0.0

PLAYER_INDEX player1
PED_INDEX scplayer


GLOBALS TRUE


SCRIPT 
	
	LOAD_SCENE(<<START_X, START_Y, START_Z>>)
	player1 = GET_PLAYER_INDEX()	//	CREATE_PLAYER(0, <<START_X, START_Y, START_Z + 1.0>> + GET_MAP_OFFSET)
	scplayer = GET_PLAYER_PED(player1)
	
	// NOTE: The 'playercoords.txt' file in GTA_pc (used by the artists) will override these coords
	IF NOT IS_ENTITY_DEAD(scplayer)
		SET_ENTITY_COORDS(scplayer,<<START_X, START_Y, START_Z + 1.0>> + GET_MAP_OFFSET)
		SET_ENTITY_HEADING(scplayer, MAIN_START_HEADING)
	ENDIF

	SHUTDOWN_LOADING_SCREEN()

	// Initial Setup
	SET_DEBUG_ACTIVE(FALSE)

	WAIT (0)
	WAIT (0)
	
	// load xml script
	REQUEST_SCRIPT("xml_menus")
	WHILE NOT HAS_SCRIPT_LOADED("xml_menus")
		WAIT(0)
	ENDWHILE
	START_NEW_SCRIPT("xml_menus", DEFAULT_STACK_SIZE)
	
	
	REQUEST_SCRIPT("nm_test")
	WHILE NOT HAS_SCRIPT_LOADED("nm_test")
		WAIT(0)
	ENDWHILE
	START_NEW_SCRIPT("nm_test", DEFAULT_STACK_SIZE)
	
	DO_SCREEN_FADE_IN(500)
	WHILE NOT IS_SCREEN_FADED_IN()
		WAIT(0)
	ENDWHILE

	WAIT (0)
	WAIT (0)

	//MAIN LOOP
	WHILE TRUE
		WAIT (0)
	ENDWHILE

ENDSCRIPT

