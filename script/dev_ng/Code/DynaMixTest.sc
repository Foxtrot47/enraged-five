// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF



// Debug mode test contents
#IF IS_DEBUG_BUILD
USING "commands_audio.sch"
USING "commands_debug.sch"
USING "script_xml.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "commands_streaming.sch"
USING "commands_graphics.sch"
USING "commands_cutscene.sch"
USING "commands_debug.sch"
USING "test_tools.sch"
USING "commands_vehicle.sch"
USING "commands_camera.sch"
USING "commands_player.sch"
USING "script_DEBUG.sch"
USING "commands_script.sch"

//Ped Vars
CONST_INT MAX_NUMBER_OF_PEDS 			10

//peds_struct TestPeds [MAX_NUMBER_OF_PEDS]


SCRIPT

   SET_DEBUG_ACTIVE (TRUE)
   NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME ()
   

//	BOOL run = true
/*
SET_AUDIO_SCRIPT_CLEANUP_TIME(30000)
INT mySound = GET_SOUND_ID()

PLAY_SOUND_FRONTEND(mySound, "TestRing", "Frontend")
WHILE (!LOAD_STREAM("LOADING_TUNE_01"))
WAIT(10)
ENDWHILE

PLAY_STREAM_FRONTEND()

WAIT(3000)
*/



vehicle_struct veh
veh.VehicleCoords = << 32, 26, 71>>
veh.VehicleHeading = 360
veh.VehicleModel = TAILGATER
CREATE_VEHICLE_ENTITY(veh)

ADD_ENTITY_TO_AUDIO_MIX_GROUP(veh.Vehicle, "COL_TEST_MIXGROUP_1")

//WHILE NOT IS_KEYBOARD_KEY_JUST_PRESSED(KEY_PERIOD) //(run = TRUE)
//SET_AUDIO_SCRIPT_VARIABLE("script", 0.5)
WHILE NOT START_AUDIO_SCENE("COL_TEST_SCENE_2")
WAIT(0)
ENDWHILE
//SET_AUDIO_SCENE_VARIABLE("COL_TEST_SCENE", "apply", 0.5)


WAIT(5000)



WAIT(5000)



WAIT(10000)
//STOP_AUDIO_SCENE("COL_TEST_SCENE_2")
WAIT(5000)


//IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_PERIOD)
//run = FALSE
//ENDIF

//WAIT(15000)

WAIT(5000)

//STOP_AUDIO_SCENE("COL_TEST_SCENE_2")

WAIT(0)

//REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(veh.Vehicle)
CLEAN_UP_VEHICLE_ENTITIES(veh.Vehicle)

WAIT(0)



ENDSCRIPT

#ENDIF
