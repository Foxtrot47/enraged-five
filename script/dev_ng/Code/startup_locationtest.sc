
//╒═════════════════════════════════════════════════════════════════════════════╕
//│								Date: 21/03/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						Location Smoketest Startup Script						│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_entity.sch"
USING "commands_ped.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "script_player.sch"


//In front of Michael's house.
VECTOR 	vStartingPosition = << -829.8442, 169.6196, 69.0172 >>
FLOAT	fStartingHeading = 140.6081


GLOBALS TRUE


PROC WAIT_FOR_TIME(INT period)
	PRINTSTRING("// WAIT_FOR_TIME")PRINTNL()
	INT currentTime = GET_GAME_TIMER()
	WHILE GET_GAME_TIMER() - currentTime < period 
		PRINTSTRING("// STILL WAITING")PRINTNL()
		WAIT(0)
	ENDWHILE
ENDPROC

SCRIPT 

	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS)
	ENDIF
	
	PRINTSTRING("----- GAME INITIALISED WITH THE LOCATION STARTUP SCRIPT -----\n")

	//Create player.
//	PLAYER_INDEX player = CREATE_PLAYER(0, vStartingPosition)
	PED_INDEX pedPlayer = PLAYER_PED_ID()	//	GET_PLAYER_PED(player)
	
	//Position player.
	IF NOT IS_ENTITY_DEAD(pedPlayer)
		SET_PED_COORDS_KEEP_VEHICLE(pedPlayer, vStartingPosition)
		SET_ENTITY_HEADING(pedPlayer, fStartingHeading)
	ENDIF
	
	//Load environment.
	LOAD_SCENE(vStartingPosition)
	
	SHUTDOWN_LOADING_SCREEN()
	DO_SCREEN_FADE_IN(500)
	WHILE NOT IS_SCREEN_FADED_IN()
		WAIT(0)
	ENDWHILE
	
	// START TESTS
	WHILE NOT SMOKETEST_STARTED()
		WAIT(0)
		PRINTSTRING("//Waiting for smoketest to start()")PRINTNL()
	ENDWHILE
	
	PRINTSTRING("//SMOKETEST STARTED")PRINTNL()
	
	INT i
	FOR i = 0 to GET_DEBUG_LOCATION_COUNT() -1
		SET_CURRENT_DEBUG_LOCATION(i)
		WAIT_FOR_TIME(5000)
		STRING name = GET_CURRENT_DEBUG_LOCATION_NAME()
		
		IF IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_IN(0)
		ENDIF
		PRINTSTRING("//SMOKETEST ")PRINTSTRING(name)PRINTNL()
		SMOKETEST_START_CAPTURE_SECTION(name)
		WAIT_FOR_TIME(5000)
		SMOKETEST_STOP_CAPTURE_SECTION()
		
		WAIT(0)
		SAVE_SCREENSHOT(name)
		WAIT(500)
	ENDFOR
	
	SMOKETEST_END()
	PRINTSTRING("//SMOKETEST ENDED")PRINTNL()
	
	TERMINATE_THIS_THREAD()
	
ENDSCRIPT

#ENDIF