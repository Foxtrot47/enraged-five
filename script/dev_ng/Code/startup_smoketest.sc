
//╒═════════════════════════════════════════════════════════════════════════════╕
//│								Date: 07/07/11									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│							Smoketest Startup Script							│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_entity.sch"
USING "commands_ped.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "script_player.sch"
USING "commands_pad.sch"
USING "flow_public_core_override.sch" 



VECTOR 	vStartingPosition = << -829.8442, 169.6196, 69.0172 >>
FLOAT	fStartingHeading = 140.6081

//Streaming Test vars
VECTOR v_car_pos 			= 	<< -190.45, -699.04, 33.31 >>
VECTOR v_car_destination	= 	<< 542.70, -853.72, 40.36 >>
FLOAT car_heading			=	-120
BOOL stream_test_finished, physics_test_finished

VEHICLE_INDEX veh_car
MODEL_NAMES model_car = NINEF2

GLOBALS TRUE

ENUM eSCRIPT_STATE	
    SCRIPT_STATE_START = 0,
    SCRIPT_STATE_CREATE_CAR,
    SCRIPT_STATE_DRIVE_CAR,
    SCRIPT_STATE_DRIVE_TO_POINT
ENDENUM

eSCRIPT_STATE script_state = SCRIPT_STATE_START

//Physics Test vars
CONST_INT numlocation 5
INT index
VECTOR GangStartPos[numlocation], CopStartPos[numlocation], player_start_pos
FLOAT GangStartHead[numlocation], CopStartHead[numlocation]
PED_INDEX pedCop[numlocation], pedCriminal[numlocation]

REL_GROUP_HASH relGroupCop, relGroupCrim

ENUM FPS_TEST_STAGE
	SETUP,
	WAIT_FOR_CLEANUP
ENDENUM
FPS_TEST_STAGE GangStartPosStage = SETUP


PROC WAIT_FOR_TIME(INT period)
	PRINTSTRING("// WAIT_FOR_TIME")PRINTNL()
	INT currentTime = GET_GAME_TIMER()
	WHILE GET_GAME_TIMER() - currentTime < period 
		PRINTSTRING("// STILL WAITING")PRINTNL()
		WAIT(0)
	ENDWHILE
ENDPROC

SCRIPT 

	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS)
	ENDIF

	PRINTSTRING("----- GAME INITIALISED WITH THE SMOKETEST STARTUP SCRIPT -----\n")

	//Create player.
//	PLAYER_INDEX player = CREATE_PLAYER(0, vStartingPosition)
	PED_INDEX pedPlayer = PLAYER_PED_ID()	//	GET_PLAYER_PED(player)
	
	//Position player.
	IF NOT IS_ENTITY_DEAD(pedPlayer)
		SET_PED_COORDS_KEEP_VEHICLE(pedPlayer, vStartingPosition)
		SET_ENTITY_HEADING(pedPlayer, fStartingHeading)
		SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	ENDIF
	
	//Load environment.
	LOAD_SCENE(vStartingPosition)
	
	SHUTDOWN_LOADING_SCREEN()
	DO_SCREEN_FADE_IN(500)
	WHILE NOT IS_SCREEN_FADED_IN()
		WAIT(0)
	ENDWHILE
	
	// START TESTS
	WHILE NOT SMOKETEST_STARTED()
		WAIT(0)
		PRINTSTRING("//Waiting for smoketest to start()")PRINTNL()
	ENDWHILE
	
	PRINTSTRING("//SMOKETEST STARTED")PRINTNL()
	
	INT i
	FOR i = 0 to 2
		SET_CURRENT_DEBUG_LOCATION(i)
		WAIT_FOR_TIME(5000)
		STRING name = GET_CURRENT_DEBUG_LOCATION_NAME()
		
		PRINTSTRING("//SMOKETEST ")PRINTSTRING(name)PRINTNL()
		SMOKETEST_START_CAPTURE_SECTION(name)
		WAIT_FOR_TIME(5000)
		SMOKETEST_STOP_CAPTURE_SECTION()
		
		WAIT(0)
		SAVE_SCREENSHOT(name)
		WAIT(0)
	ENDFOR

		
	WHILE NOT stream_test_finished
		WAIT(0)

		IF IS_PLAYER_PLAYING(PLAYER_ID())

			SWITCH script_state

				CASE SCRIPT_STATE_START
					PRINTSTRING("...Going for a drive")
					PRINTNL()
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					SET_CLOCK_TIME(12,0,0)
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
//					SET_VEHICLE_POPULATION_BUDGET(0)

					SET_ROADS_IN_AREA(<<-2000, -2000, -200>>, <<2000, 2000, 200>>, FALSE)
					CLEAR_AREA(v_car_pos, 5000.0, TRUE, TRUE, TRUE, TRUE)
					script_state = SCRIPT_STATE_CREATE_CAR
				BREAK
			
				CASE SCRIPT_STATE_CREATE_CAR
					REQUEST_MODEL(model_car)
					LOAD_ALL_OBJECTS_NOW()
					veh_car = CREATE_VEHICLE(model_car, v_car_pos, car_heading)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), veh_car)
					SET_DEBUG_CAM_ACTIVE(FALSE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					LOAD_SCENE(v_car_pos)
					script_state = SCRIPT_STATE_DRIVE_CAR
				BREAK
			
				CASE SCRIPT_STATE_DRIVE_CAR
					IF IS_VEHICLE_DRIVEABLE(veh_car)
						SMOKETEST_START_CAPTURE_SECTION("Streaming test")
						TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(),veh_car, v_car_destination,30.0,DRIVINGSTYLE_RACING,model_car,DRIVINGMODE_PLOUGHTHROUGH,5.0,1.0)					
						script_state = SCRIPT_STATE_DRIVE_TO_POINT
					ENDIF
				BREAK
				
				CASE SCRIPT_STATE_DRIVE_TO_POINT
					IF IS_VEHICLE_DRIVEABLE(veh_car) AND FINISHED_TASK = GET_SCRIPT_TASK_STATUS( PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD )
						SMOKETEST_STOP_CAPTURE_SECTION()
						WAIT(0)
						SAVE_SCREENSHOT("Streaming test")
						WAIT(0)
						SET_MODEL_AS_NO_LONGER_NEEDED(model_car)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(veh_car)
						stream_test_finished = TRUE
					ENDIF
				BREAK
					
			ENDSWITCH
		
			SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
			SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
			SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
			SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0, 0)
				
		ENDIF
		
	ENDWHILE
	
	
	WHILE NOT physics_test_finished
		
		WAIT(0)

			IF IS_PLAYER_PLAYING(PLAYER_ID())
			
				SWITCH GangStartPosStage
				
					CASE SETUP
						REQUEST_MODEL(S_M_Y_COP_01)
						REQUEST_MODEL(A_M_Y_MEXTHUG_01)
						LOAD_ALL_OBJECTS_NOW()
						
						player_start_pos = << -592.5580, -1652.5548, 23.4580 >>
						CLEAR_AREA(player_start_pos, 300, TRUE)
						SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), player_start_pos)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 195)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_CLOCK_TIME(17, 0, 0)	
						LOAD_SCENE(player_start_pos)

						ADD_RELATIONSHIP_GROUP("RE_ARREST_COP", relGroupCop)
						ADD_RELATIONSHIP_GROUP("RE_ARREST_CRIM", relGroupCrim)

						CopStartPos[0] = << -579.2419, -1656.8274, 18.7912 >>
						CopStartHead[0] = 118
						CopStartPos[1] = << -572.1108, -1662.7108, 18.2502 >>
						CopStartHead[1] = 94
						CopStartPos[2] = << -581.9711, -1671.4646, 18.2560 >>	
						CopStartHead[2] = 94
						CopStartPos[3] = << -566.4862, -1671.4352, 18.2296 >>	
						CopStartHead[3] = 87
						CopStartPos[4] = << -571.9099, -1675.8474, 18.7016 >>
						CopStartHead[4] = 48
						
						REPEAT numlocation index
							pedCop[index] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01, CopStartPos[index])
							SET_PED_HEADING_AND_PITCH(pedCop[index], CopStartHead[index], 0)
							SET_PED_COMBAT_MOVEMENT(pedCop[index], CM_DEFENSIVE )
							SET_PED_FLEE_ATTRIBUTES(pedCop[index], FA_USE_COVER, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(pedCop[index], CA_CAN_CAPTURE_ENEMY_PEDS, FALSE)
							SET_ENTITY_PROOFS(pedCop[index], TRUE, TRUE, TRUE, TRUE, TRUE)
							SET_PED_RELATIONSHIP_GROUP_HASH(pedCop[index], relGroupCop)
							GIVE_WEAPON_TO_PED(pedCop[index], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
							SET_CURRENT_PED_WEAPON(pedCop[index], WEAPONTYPE_PISTOL, TRUE)
						ENDREPEAT

						GangStartPos[0] = << -601.7824, -1674.6423, 18.5608 >> 	
						GangStartHead[0] = 285
						GangStartPos[1] = << -600.7036, -1669.3536, 18.5807 >>	
						GangStartHead[1] = 285
						GangStartPos[2] = << -606.2323, -1669.1875, 18.8904 >>	
						GangStartHead[2] = 274
						GangStartPos[3] = << -605.7510, -1678.3025, 18.6469 >>	
						GangStartHead[3] = 289
						GangStartPos[4] = << -596.6554, -1674.1642, 18.4122 >>
						GangStartHead[4] = 286
						
						REPEAT numlocation index
							pedCriminal[index] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_MEXTHUG_01, GangStartPos[index])
							SET_PED_HEADING_AND_PITCH(pedCriminal[index], GangStartHead[index], 0)
							SET_PED_COMBAT_MOVEMENT(pedCriminal[index], CM_DEFENSIVE )
							SET_PED_FLEE_ATTRIBUTES(pedCriminal[index], FA_USE_COVER, TRUE)
							SET_ENTITY_PROOFS(pedCriminal[index], TRUE, TRUE, TRUE, TRUE, TRUE)
							SET_PED_RELATIONSHIP_GROUP_HASH(pedCriminal[index], relGroupCrim)
							GIVE_WEAPON_TO_PED(pedCriminal[index], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
							SET_CURRENT_PED_WEAPON(pedCriminal[index], WEAPONTYPE_PISTOL, TRUE)
						ENDREPEAT
						
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupCrim, relGroupCop)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupCop, relGroupCrim)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupCop, RELGROUPHASH_PLAYER)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupCrim, RELGROUPHASH_PLAYER)
	
						REPEAT numlocation index
							REGISTER_HATED_TARGETS_IN_AREA(pedCop[index], CopStartPos[index], 200)
							TASK_TURN_PED_TO_FACE_ENTITY(pedCop[index], pedCriminal[index])
						ENDREPEAT
						REPEAT numlocation index
							REGISTER_HATED_TARGETS_IN_AREA(pedCriminal[index], GangStartPos[index], 200)
							TASK_TURN_PED_TO_FACE_ENTITY(pedCriminal[index], pedCop[index])
						ENDREPEAT						

						GangStartPosStage = WAIT_FOR_CLEANUP		
					BREAK
				
					CASE WAIT_FOR_CLEANUP
						SMOKETEST_START_CAPTURE_SECTION("Physics test")
						WAIT_FOR_TIME(20000)
						SMOKETEST_STOP_CAPTURE_SECTION()
						WAIT(0)
						SAVE_SCREENSHOT("Physics test")
						WAIT(0)
						physics_test_finished = TRUE		
					BREAK
					
				ENDSWITCH

			ENDIF
		
	ENDWHILE
	
	SMOKETEST_END()
	PRINTSTRING("//SMOKETEST ENDED")PRINTNL()

	SET_VEHICLE_POPULATION_BUDGET(1)
	SET_PED_POPULATION_BUDGET(1)
	SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	SET_ROADS_BACK_TO_ORIGINAL(<<-2000, -2000, -200>>, <<2000, 2000, 200>>)
	
	TERMINATE_THIS_THREAD()
	
ENDSCRIPT

#ENDIF