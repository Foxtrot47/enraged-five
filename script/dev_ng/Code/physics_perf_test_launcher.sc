USING "commands_debug.sch"
USING "commands_script.sch"
USING "stack_sizes.sch"
USING "globals.sch"

SCRIPT
	
	#IF IS_FINAL_BUILD
		TERMINATE_THIS_THREAD()
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		
		PHYSICS_PERF_STRUCT commandlineParams
		
		REQUEST_SCRIPT("physics_perf_test")
		WHILE NOT HAS_SCRIPT_LOADED("physics_perf_test")
			WAIT(0)
		ENDWHILE

		NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()

		commandlineParams.vInput.x = GET_COMMANDLINE_PARAM_FLOAT_BY_INDEX("sc_physics_perf_test", 0)
		commandlineParams.vInput.y = GET_COMMANDLINE_PARAM_FLOAT_BY_INDEX("sc_physics_perf_test", 1)
		commandlineParams.vInput.z = GET_COMMANDLINE_PARAM_FLOAT_BY_INDEX("sc_physics_perf_test", 2)
		commandlineParams.fInput = GET_COMMANDLINE_PARAM_FLOAT_BY_INDEX("sc_physics_perf_test", 3)

		START_NEW_SCRIPT_WITH_ARGS("physics_perf_test", commandlineParams, SIZE_OF(commandlineParams), DEBUG_SCRIPT_STACK_SIZE)
	
		WHILE TRUE
			WAIT(0)
		ENDWHILE
	#ENDIF
ENDSCRIPT
