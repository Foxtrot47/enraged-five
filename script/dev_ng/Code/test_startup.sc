// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "script_player.sch"
USING "streamed_scripts.sch"
USING "respawn_location_data.sch"
USING "Mission_Flow_Initialiser.sch"
USING "net_betting_client.sch"

// All the registration headers required to specify which global variables need to be saved
USING "flow_globals_reg.sch"

// Allow the globals to be created
GLOBALS TRUE                                                 


FLOAT fInitialPlayerHeading = 159.6765
VECTOR vInitialPlayerCoord = << 201.3993, -924.1564, 29.6919 >>, current_coords, closest_safe_position		//Pershing square? ()
INT index, random_closest_node

VEHICLE_INDEX test_veh[60]
VECTOR test_veh_vect[60]
FLOAT test_veh_float[60]

PED_INDEX test_ped[90]
MODEL_NAMES testVehModel[10]
MODEL_NAMES testPedModel[4]
CONST_INT VehModelsNums 10
CONST_INT PedModelsNums 4
WIDGET_GROUP_ID wgGroup

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE: Creates the player ped and places him in the world
/// NOTES:   The 'playercoords.txt' file (used by the artists) will override these player creation coords
PROC Create_The_Player()
	// Don't load the scene in Release mode -//depot/gta4_japanese/script/dev/Demo/coderTest.sc speeds up loading because Player is usually re-positioned
    //  by the first mission script anyway leading to two warps
    #IF IS_DEBUG_BUILD
        LOAD_SCENE(vInitialPlayerCoord)
    #ENDIF
    
    // Looks like the player aleady exists at this point, is this still needed? 
//    CREATE_PLAYER(0, vInitialPlayerCoord)
    
    // Explicitly set the player ped coords, as if the player already existed, calling CREATE_PLAYER
    // does not set the player coords.
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	    /*SET_ENTITY_COORDS*/ SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vInitialPlayerCoord)
	    SET_ENTITY_HEADING(PLAYER_PED_ID(), fInitialPlayerHeading)
	ENDIF
    
ENDPROC

/// PURPOSE:
///     (stop ambient creation of vehicles, peds, create player in star junction,
///    create 20 vehicles, 20 peds).
///    see bug #
PROC Initialise_Test_Startup_Settings_With_Wait()
    INT i
	
	SET_CLOCK_TIME(17,0,0)
	SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
	//stop ambient creation of vehicles
    PRINTSTRING("//stop ambient creation of vehicles")PRINTNL()
	CLEAR_AREA_OF_VEHICLES(vInitialPlayerCoord, 1000.0)
	SET_NUMBER_OF_PARKED_VEHICLES(0)
	SET_VEHICLE_POPULATION_BUDGET(0)
	SET_GARBAGE_TRUCKS(FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-9999.9, -9999.9, -9999.9>>, <<9999.9, 9999.9, 9999.9>>, FALSE)

	//stop ambient creation of peds
    PRINTSTRING("//stop ambient creation of peds")PRINTNL()
	CLEAR_AREA_OF_PEDS(vInitialPlayerCoord, 1000.0)
	SET_PED_POPULATION_BUDGET(0)
	
    testVehModel[0] = double
	testVehModel[1] = banshee
    testVehModel[2] = tailgater
    testVehModel[3] = hexer
    testVehModel[4] = bison
    testVehModel[5] = taxi
	testVehModel[6] = packer
    testVehModel[7] = POLICE2
    testVehModel[8] = cavalcade
    testVehModel[9] = dominator
	
	testPedModel[0] = A_M_O_Tramp_01
	testPedModel[1] = A_F_Y_Business_02
	testPedModel[2] = A_M_M_Business_01
	testPedModel[3] = A_F_M_BevHills_01
	
	REQUEST_MODEL(double)
	REQUEST_MODEL(banshee)
	REQUEST_MODEL(tailgater)
	REQUEST_MODEL(hexer)
	REQUEST_MODEL(bison)
	REQUEST_MODEL(taxi)
	REQUEST_MODEL(packer)
	REQUEST_MODEL(POLICE2)
	REQUEST_MODEL(cavalcade)
	REQUEST_MODEL(dominator)
	
	REQUEST_MODEL(A_M_O_Tramp_01)
	REQUEST_MODEL(A_F_Y_Business_02)
	REQUEST_MODEL(A_M_M_Business_01)
	REQUEST_MODEL(A_F_M_BevHills_01)
	
	LOAD_ALL_OBJECTS_NOW()
//	WHILE NOT HAS_MODEL_LOADED(testVehModel)
//	OR NOT HAS_MODEL_LOADED(testPedModel)
//		WAIT(0)
//	ENDWHILE
	
	FLOAT fCurrentCount

//testVehModel_vector[0] = <<186.1, -822.9, 30.8>>
//testVehModel_heading[0] = 180



test_veh_vect[0] = <<206.6987, -1039.8453, 28.3721>>
test_veh_float[0] = 343.1739
test_veh_vect[1] = <<198.5071, -1038.0665, 28.3708>>
test_veh_float[1] = 342.4447
test_veh_vect[2] = <<192.6146, -1037.4288, 28.3650>>
test_veh_float[2] = 342.4521
test_veh_vect[3] = <<187.2161, -1035.2522, 28.3591>>
test_veh_float[3] = 342.4442
test_veh_vect[4] = <<180.8838, -1032.7042, 28.3611>>
test_veh_float[4] = 342.4445
test_veh_vect[5] = <<175.0022, -1030.3782, 28.3678>>
test_veh_float[5] = 342.4575
test_veh_vect[6] = <<165.8161, -1027.3179, 28.3790>>
test_veh_float[6] = 342.4449
test_veh_vect[7] = <<157.2598, -1025.1091, 28.3890>>
test_veh_float[7] = 342.4417
test_veh_vect[8] = <<148.2094, -1022.5166, 28.3988>>
test_veh_float[8] = 342.4715
test_veh_vect[9] = <<139.0062, -1017.7036, 28.4033>>
test_veh_float[9] = 342.4443

test_veh_vect[10] = <<108.3263, -997.2086, 28.3959>>
test_veh_float[10] = 252.8044
test_veh_vect[11] = <<107.7715, -991.8757, 28.4134>>
test_veh_float[11] = 252.7936
test_veh_vect[12] = <<110.1615, -987.0870, 28.4076>>
test_veh_float[12] = 252.7939
test_veh_vect[13] = <<111.3613, -981.6348, 28.4073>>
test_veh_float[13] = 252.7914
test_veh_vect[14] = <<111.4430, -977.2441, 28.4073>>
test_veh_float[14] = 252.7958
test_veh_vect[15] = <<113.0738, -972.4412, 28.4073>>
test_veh_float[15] = 252.7943
test_veh_vect[16] = <<114.6622, -965.9135, 28.4820>>
test_veh_float[16] = 252.7924
test_veh_vect[17] = <<117.1381, -960.5906, 28.5582>>
test_veh_float[17] = 252.7941
test_veh_vect[18] = <<118.3351, -955.2291, 28.6179>>
test_veh_float[18] = 252.7935
test_veh_vect[19] = <<121.1946, -939.9125, 28.8015>>
test_veh_float[19] = 246.9031
test_veh_vect[20] = <<122.9722, -933.8780, 28.8765>>
test_veh_float[20] = 246.9050
test_veh_vect[21] = <<125.1615, -929.2699, 28.9372>>
test_veh_float[21] = 246.9046
test_veh_vect[22] = <<125.5500, -923.0488, 29.0070>>
test_veh_float[22] = 246.8923
test_veh_vect[23] = <<128.8248, -916.8493, 29.0915>>
test_veh_float[23] = 246.8983
test_veh_vect[24] = <<131.7781, -910.4286, 29.1757>>
test_veh_float[24] = 246.9023
test_veh_vect[25] = <<134.1747, -900.7925, 29.2937>>
test_veh_float[25] = 246.8915
test_veh_vect[26] = <<135.6476, -892.2643, 29.3517>>
test_veh_float[26] = 246.9026
test_veh_vect[27] = <<140.6770, -880.8380, 29.5127>>
test_veh_float[27] = 243.3477
test_veh_vect[28] = <<143.0298, -870.3422, 29.6332>>
test_veh_float[28] = 243.3357
test_veh_vect[29] = <<148.5701, -862.0368, 29.7875>>
test_veh_float[29] = 243.3460

test_veh_vect[30] = <<184.9702, -826.0327, 30.2067>>
test_veh_float[30] = 166.9933
test_veh_vect[31] = <<196.0320, -824.7734, 30.1044>>
test_veh_float[31] = 166.9362
test_veh_vect[32] = <<206.7497, -828.4476, 29.8026>>
test_veh_float[32] = 167.0350
test_veh_vect[33] = <<216.2865, -832.4612, 29.5592>>
test_veh_float[33] = 166.9939
test_veh_vect[34] = <<234.7173, -832.8547, 29.1451>>
test_veh_float[34] = 167.0806
test_veh_vect[35] = <<243.1136, -838.8093, 28.9121>>
test_veh_float[35] = 167.0252
test_veh_vect[36] = <<253.7434, -840.5871, 28.6628>>
test_veh_float[36] = 167.0373
test_veh_vect[37] = <<261.1298, -843.3223, 28.4743>>
test_veh_float[37] = 167.0024
test_veh_vect[38] = <<269.2050, -846.6691, 28.3342>>
test_veh_float[38] = 166.9980
test_veh_vect[39] = <<277.4965, -851.8069, 28.1873>>
test_veh_float[39] = 166.9400

test_veh_vect[40] = <<287.7495, -863.8610, 28.3275>>
test_veh_float[40] = 71.6018
test_veh_vect[41] = <<287.8554, -870.4238, 28.3247>>
test_veh_float[41] = 71.6071
test_veh_vect[42] = <<287.6473, -878.3958, 28.2818>>
test_veh_float[42] = 71.6062
test_veh_vect[43] = <<280.3173, -889.0336, 28.0923>>
test_veh_float[43] = 71.6112
test_veh_vect[44] = <<279.4331, -896.9427, 27.9906>>
test_veh_float[44] = 71.5986
test_veh_vect[45] = <<277.3947, -904.2783, 28.0030>>
test_veh_float[45] = 71.6103
test_veh_vect[46] = <<274.0884, -912.1621, 28.0314>>
test_veh_float[46] = 71.6059
test_veh_vect[47] = <<271.5664, -922.1691, 28.0870>>
test_veh_float[47] = 71.6132
test_veh_vect[48] = <<268.2768, -933.7896, 28.2786>>
test_veh_float[48] = 71.6205
test_veh_vect[49] = <<263.6005, -946.7740, 28.3866>>
test_veh_float[49] = 71.6111
test_veh_vect[50] = <<262.0273, -955.2792, 28.2963>>
test_veh_float[50] = 71.6559
test_veh_vect[51] = <<255.4025, -970.4787, 28.2753>>
test_veh_float[51] = 71.6818
test_veh_vect[52] = <<258.2577, -962.3406, 28.2863>>
test_veh_float[52] = 71.6406
test_veh_vect[53] = <<247.6898, -983.4844, 28.2910>>
test_veh_float[53] = 71.4643
test_veh_vect[54] = <<251.0686, -976.3849, 28.2735>>
test_veh_float[54] = 71.6190
test_veh_vect[55] = <<243.9829, -989.8423, 28.2281>>
test_veh_float[55] = 71.6201
test_veh_vect[56] = <<245.7980, -997.5056, 28.1626>>
test_veh_float[56] = 71.5639
test_veh_vect[57] = <<241.5478, -1006.8408, 28.2672>>
test_veh_float[57] = 71.6043
test_veh_vect[58] = <<240.4712, -1015.4442, 28.1695>>
test_veh_float[58] = 71.6234
test_veh_vect[59] = <<239.7478, -1024.1592, 28.2432>>
test_veh_float[59] = 71.4723

	
	//create 60 vehicles
	REPEAT COUNT_OF(test_veh) i
		IF NOT DOES_ENTITY_EXIST(test_veh[i])
//			fCurrentCount = (i / TO_FLOAT(COUNT_OF(test_veh))) * 360.0
//			
//			FLOAT fModCount = (fCurrentCount % 359.0) + fInitialPlayerHeading
//			FLOAT fMult = 55.0+(ROUND(fCurrentCount/90) * 6.0) //fMult = 20.0
//			
//			VECTOR vTest_veh_coord = vInitialPlayerCoord+(<<SIN(fModCount), COS(fModCount), 0.0>>*fMult)
//			FLOAT fTest_veh_head = GET_HEADING_FROM_VECTOR_2D(vInitialPlayerCoord.x-vTest_veh_coord.x, vInitialPlayerCoord.y-vTest_veh_coord.y)
			
			index ++
			IF index >= VehModelsNums
				index = 0
			ENDIF
			test_veh[i] = CREATE_VEHICLE(testVehModel[index], test_veh_vect[i], test_veh_float[i])
			SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(test_veh[i], FALSE)
		ENDIF
	ENDREPEAT
	
	
	//create 90 peds
	REPEAT COUNT_OF(test_ped) i
		IF NOT DOES_ENTITY_EXIST(test_ped[i])
//			fCurrentCount = (i / TO_FLOAT(COUNT_OF(test_ped))) * 360.0
//				
//			VECTOR vTest_ped_coord = vInitialPlayerCoord+(<<SIN(fCurrentCount), COS(fCurrentCount), 0.0>>*7.5)
//			FLOAT fTest_ped_head = GET_HEADING_FROM_VECTOR_2D(vInitialPlayerCoord.x-vTest_ped_coord.x, vInitialPlayerCoord.y-vTest_ped_coord.y)
//			

			fCurrentCount = (i / TO_FLOAT(COUNT_OF(test_ped))) * 360.0
			
			FLOAT fModCount = (fCurrentCount % 359.0) + fInitialPlayerHeading
			FLOAT fMult = 22.5+(ROUND(fCurrentCount/90) * 6.0) //fMult = 7.5
			
			VECTOR vTest_ped_coord = vInitialPlayerCoord+(<<SIN(fModCount), COS(fModCount), 0.0>>*fMult)
			FLOAT fTest_ped_head = GET_HEADING_FROM_VECTOR_2D(vInitialPlayerCoord.x-vTest_ped_coord.x, vInitialPlayerCoord.y-vTest_ped_coord.y)
			
//			index = GET_RANDOM_INT_IN_RANGE(0, PedModelsNums)
			index ++
			IF index >= PedModelsNums
				index = 0
			ENDIF
			test_ped[i] = CREATE_PED(PEDTYPE_MISSION, testPedModel[index], vTest_ped_coord, fTest_ped_head)
			TASK_STAND_STILL(test_ped[i], -1)
			SET_PED_KEEP_TASK(test_ped[i], TRUE)
		ENDIF
	ENDREPEAT
	
	//Give a handful of peds weapons
	IF DOES_ENTITY_EXIST(test_ped[1])
		GIVE_WEAPON_TO_PED(test_ped[1], WEAPONTYPE_CARBINERIFLE, INFINITE_AMMO, TRUE)
	ENDIF
	IF DOES_ENTITY_EXIST(test_ped[2])
		GIVE_WEAPON_TO_PED(test_ped[2], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
	ENDIF
	IF DOES_ENTITY_EXIST(test_ped[3])
		GIVE_WEAPON_TO_PED(test_ped[3], WEAPONTYPE_PUMPSHOTGUN, INFINITE_AMMO, TRUE)
	ENDIF
	
	REPEAT VehModelsNums i
		SET_MODEL_AS_NO_LONGER_NEEDED(testVehModel[i])
	ENDREPEAT
	
	REPEAT PedModelsNums i
		SET_MODEL_AS_NO_LONGER_NEEDED(testPedModel[i])
	ENDREPEAT
	
	SHUTDOWN_LOADING_SCREEN()
	
	// Fade the screen in
	IF NOT IS_SCREEN_FADED_IN()
	AND NOT IS_SCREEN_FADING_IN()
		DO_SCREEN_FADE_IN(500)
	ENDIF
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Requests the 'Main Persistent' script, waits for it to stream in, then launches it
PROC Request_And_Launch_Main_Persistent_Script_With_Wait()

    STRING theScriptName = "main_persistent"

    REQUEST_SCRIPT(theScriptName)
    WHILE NOT HAS_SCRIPT_LOADED(theScriptName)
        WAIT(0)
        REQUEST_SCRIPT(theScriptName)
    ENDWHILE
    
    START_NEW_SCRIPT(theScriptName, DEFAULT_STACK_SIZE)
    
ENDPROC



// ===========================================================================================================

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
		 TERMINATE_THIS_THREAD()
	ENDIF
    
    Create_The_Player()
    
    SET_DEBUG_ACTIVE(FALSE) 
    
    // IMPORTANT: THERE SHOULD BE NO 'WAIT' COMMANDS BEFORE HERE
    WAIT (0)
	
	Initialise_Test_Startup_Settings_With_Wait()

	IF GET_COMMANDLINE_PARAM_EXISTS("create_betting_widget_group")	
		CREATE_BETTING_WIDGETS(wgGroup)
	ENDIF
	
	WHILE TRUE
		WAIT(0)	
	
		random_closest_node ++

		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)

//		//Check if entity is colliding with anything and try to resapwn on a safe node
//		REPEAT COUNT_OF(test_veh) index
//			IF DOES_ENTITY_EXIST(test_veh[index])
//				IF IS_VEHICLE_DRIVEABLE(test_veh[index])
//					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(test_veh[index])
//						current_coords = GET_ENTITY_COORDS(test_veh[index])
//						GET_NTH_CLOSEST_VEHICLE_NODE(current_coords, random_closest_node, closest_safe_position)
//						SET_ENTITY_COORDS(test_veh[index], closest_safe_position)
//						WAIT(0)
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDREPEAT
	
//		Check if entity is colliding with anything and try to resapwn on a safe node
		REPEAT COUNT_OF(test_ped) index
			IF DOES_ENTITY_EXIST(test_ped[index])
				IF NOT IS_PED_INJURED(test_ped[index])
					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(test_ped[index])
						current_coords = GET_ENTITY_COORDS(test_ped[index])
						GET_NTH_CLOSEST_VEHICLE_NODE(current_coords, random_closest_node, closest_safe_position)
						SET_ENTITY_COORDS(test_ped[index], closest_safe_position)
						WAIT(0)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT

		IF random_closest_node >= 30
			random_closest_node = 0
		ENDIF
		
	ENDWHILE
	
	
	// Allow Startup to terminate
    TERMINATE_THIS_THREAD()
    
ENDSCRIPT


#ENDIF  // IS_DEBUG_BUILD

