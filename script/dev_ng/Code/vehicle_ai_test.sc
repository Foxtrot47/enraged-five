// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "script_xml.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "commands_streaming.sch"
USING "commands_graphics.sch"
USING "commands_cutscene.sch"
USING "commands_debug.sch"
USING "test_tools.sch"
USING "commands_vehicle.sch"
USING "commands_camera.sch"
USING "commands_player.sch"
USING "script_DEBUG.sch"
USING "commands_script.sch"

//Notes: This is script is to allow easy setup of test scenarios for the code department. It works in conjunction with test_tools.sch
//It  is a data driven system 

//Defaults values when the test is reset to factory the standard values are used

//vector gvMapOffset

//Level menu 
STRING XMLMenu = "Testbed/TestVehicleAiMenu"

//Ped Vars
CONST_INT MAX_NUMBER_OF_PEDS 			10

peds_struct TestPeds [MAX_NUMBER_OF_PEDS]

//Vehicle Vars
CONST_INT MAX_NUMBER_OF_VEHICLES 	 10

vehicle_struct TestVehicles[MAX_NUMBER_OF_VEHICLES]

//Route Vars 
CONST_INT MAX_NUMBER_OF_NODES 				7

RouteStruct Route[MAX_NUMBER_OF_NODES]

//Camera Vars 
CONST_INT MAX_NUMBER_OF_CAMERAS			2
CONST_INT FixedCamera										0
CONST_INT TrackingCamera									1

StCameraData TestCams[MAX_NUMBER_OF_CAMERAS]

//FLOAT fCarBootAngleRatio

VECTOR PlaneTargetCoord = <<  150.0000 , 40.0000 , 10.9393  >>
VECTOR PlaneTargetCoord1 = << 600.3486, 40.0000 , 60.9393  >>
VECTOR PlaneTargetCoord2 = << 10000.3486, 40.0000 , 60.9393  >>

VECTOR HeliTargetCoord = << 50.3486, 100.0366, 30.8365 >>
VECTOR HeliTargetCoord1 = << 50.3486, 180.0366,30.8365 >>
VECTOR HeliTargetCoord2 = << -44.3486, 241.0366, 30.8365 >>
VECTOR HeliTargetCoord3 = << -100.3486, 156.0366, 30.8365 >>
VECTOR HeliTargetCoord4 = << 8.3486, 35.0366, 10.8365 >>
BOOL gAnimPlayed = FALSE

BOOL gAllowDebugging = TRUE

SEQUENCE_INDEX TEST_SEQ

//OBJECT_INDEX Object

//bool gBootOpen = TRUE

bool testprint = FALSE
bool flushFile = FALSE
PROC CREATE_TEST_WIDGET ()
		START_WIDGET_GROUP("General Tests")
		SETUP_WIDGET ()	
			START_WIDGET_GROUP("Navigation")
				ADD_WIDGET_BOOL ("Start scenario", gBeginCombatScenario)
				ADD_WIDGET_BOOL ("Restart scenario", gResetCombatScenario)
				ADD_WIDGET_BOOL ("Reset to default", gResetToDefault)
				ADD_WIDGET_BOOL ("Debug scenario", gRun_debuggig)
				ADD_WIDGET_BOOL ("Print all setup info", gPrintAllIinfo)
				ADD_WIDGET_BOOL ("Test print", testprint)
				ADD_WIDGET_BOOL ("flushFilet", flushFile)
			STOP_WIDGET_GROUP ()	
		STOP_WIDGET_GROUP()
ENDPROC

// Functions end: WIDGET

//Mission flow House keeping


//PUPROSE: REmoves all the scenarios and resets all the relationships
	PROC Cleanup_Scenario_Entities ()
	int index = 0
		for index = 0 to MAX_NUMBER_OF_PEDS -1 	
			CLEAN_UP_PED_ENTITIES (TestPeds[index].ped)
		ENDFOR 
		
		for index = 0 to MAX_NUMBER_OF_VEHICLES -1
			CLEAN_UP_VEHICLE_ENTITIES (TestVehicles[index].Vehicle )
		ENDFOR

		Set_Gang_Relationships (FALSE)	
	ENDPROC

PROC Terminate_test_script ()
	if IS_KEYBOARD_KEY_JUST_PRESSED (KEY_S)
		CLEAR_PRINTS ()
		CLEAR_HELP ()
		Cleanup_Scenario_Entities ()
		Temp_cleanup_scenario_cams ()
		SET_PLAYER_COLISION(scplayer, true)
		IF NOT IS_PED_INJURED (scplayer)
				  SET_PED_COORDS_KEEP_VEHICLE(scplayer, GET_PLAYER_START_POS () )
		ENDIF
		
		TERMINATE_THIS_THREAD ()
		
	ENDIF
ENDPROC
//END: House Keeping

//Test Specific Functions:

//EXAMPLE OF IMPLEMTING A SEQUENCE
FUNC SEQUENCE_INDEX TEST_TASK_OPEN_VEHICLE_DOOR(VEHICLE_INDEX vehicle, int time = -2)
		SEQUENCE_INDEX TEST_SEQUENCE
		OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
				TASK_OPEN_VEHICLE_DOOR  (NULL, vehicle,  time )
		CLOSE_SEQUENCE_TASK (TEST_SEQUENCE)
		RETURN TEST_SEQUENCE
ENDFUNC					


PROC SET_CAMERA(VECTOR vPos, VECTOR vRot)
	IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
		SET_CAM_COORD (TestCams[FixedCamera].cam , vPos)
		SET_CAM_ROT (TestCams[FixedCamera].cam ,  vRot  )
		SET_CAM_ACTIVE (TestCams[FixedCamera].cam, TRUE )	
	ELSE
		TestCams[FixedCamera].cam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vPos, vRot, 60.0, TRUE )
		RENDER_SCRIPT_CAMS (TRUE, FALSE)	
		SET_CAM_ACTIVE (TestCams[FixedCamera].cam, TRUE )	
	ENDIF	
ENDPROC

//point route test
PROC DRIVE_TO_POINT_SEQUENCE (PED_INDEX ped, VEHICLE_INDEX veh)
	SEQUENCE_INDEX SEQ
	OPEN_SEQUENCE_TASK (SEQ)


		TASK_FLUSH_ROUTE()
		TASK_EXTEND_ROUTE(<< -0.0012, 40.0009, 7.9393>>)
		TASK_EXTEND_ROUTE(<<20.0012, 40.0009, 7.9393 >>)
		TASK_EXTEND_ROUTE(<<20.0012, 10.0009, 7.9393>>)
		TASK_EXTEND_ROUTE(<<-0.0012, 10.0009, 7.9393>>)
		TASK_DRIVE_POINT_ROUTE (NULL, veh, 5.0  )


		SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
	CLOSE_SEQUENCE_TASK(SEQ)
	
	IF NOT IS_PED_INJURED (ped)
		TASK_PERFORM_SEQUENCE (ped, seq)
		
		CLEAR_SEQUENCE_TASK (seq)
	ENDIF	
ENDPROC


//End Test Specific Functions

//These enums must match with the testNavmenu.xml



PROC INITALISE_TEST_STATE()
		SingleTaskStatus = startSingleTask
ENDPROC

ENUM Scenarios
	TestExample, 
	PointRouteTest,
	HeliStrafe,
	CarSetTempAction,
	CarDrive,
	CarDriveWander,
	PlaneTest,
	TrainTest,
	PlaneDriveBy,
	HeliDriveBy,
	DefaultTest = 1000
ENDENUM


//  END_SCENARIO: SquadOfPedsMovingBetweenVehicles  
PROC SETUP_TEST_DATA ()
	SWITCH int_to_enum (scenarios, gcurrentselection)
		CASE TestExample

		CASE CarSetTempAction
		CASE CarDrive
		
			//fCarBootAngleRatio = 1.0
			
			//Create a vehicle 
			TestVehicles [0].vehicleCoords = << 80.0012, 66.0009, 1.9393>> 
			TestVehicles [0].vehicleHeading = 90.0001
			TestVehicles [0].vehiclemodel = SANCHEZ
			gAnimPlayed = FALSE
			

						//Create a ped
			TestPeds[0].PedsCoords = << 80.0000 , 57.0000, 10.1959>> 
			TestPeds[0].PedHeading = 359.6439
			TestPeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = S_M_Y_COP_01
			TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
		Break
		
		CASE CarDriveWander
			//Create a vehicle 
			TestVehicles [0].vehicleCoords = << 34.0012, 66.0009, 1.9393>> 
			TestVehicles [0].vehicleHeading = 90.0001
			TestVehicles [0].vehiclemodel = TAILGATER
			
			TestVehicles [1].vehicleCoords = << 34.0000 , 66.0000 , 1.9393>> 
			TestVehicles [1].vehicleHeading = 90.0001
			TestVehicles [1].vehiclemodel = TOWTRUCK
			
			//Create a ped
			TestPeds[0].PedsCoords = << 30.4167, 66.4273, 8.1959>> 
			TestPeds[0].PedHeading = 359.6439
			TestPeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = S_M_Y_COP_01
			TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
		Break
		
		CASE PointRouteTest
			//Create a vehicle 
			TestVehicles [0].vehicleCoords = << 0.0000 , 61.0000 , 8 >> 
			TestVehicles [0].vehicleHeading = 90
			TestVehicles [0].vehiclemodel = PACKER
		
			TestVehicles [1].vehicleCoords = << 14.0000 , 60.0000 , 8 >> 
			TestVehicles [1].vehicleHeading = 90
			TestVehicles [1].vehiclemodel = TRAILERS
			
			//Create a ped
			TestPeds[0].PedsCoords = << -2.4167, -1.4273, 1.1959>> 
			TestPeds[0].PedHeading = 359.6439
			TestPeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = S_M_Y_COP_01
			TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
		Break
			
		CASE HeliStrafe
			//create heli for strafing
			TestVehicles [0].vehicleCoords = << 0.0000 , 30.0000 , 1.9393 >>
			TestVehicles [0].vehicleHeading = 90.0
			TestVehicles [0].vehiclemodel = BUZZARD
			
			//Create a ped
			TestPeds[0].PedsCoords = << -2.4167, -1.4273, 1.1959>> 
			TestPeds[0].PedHeading = 359.6439
			TestPeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = S_M_Y_COP_01
			TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
		Break
		
		CASE PlaneTest
			
			//create plane for strafing
			TestVehicles [0].vehicleCoords = << 100.0000 , 40.0000 , 12.9393 >>
			TestVehicles [0].vehicleHeading = -93.0
			TestVehicles [0].vehiclemodel = TITAN
			
			SET_VEHICLE_DOOR_OPEN(TestVehicles [0].Vehicle, SC_DOOR_BOOT)
			
			TestVehicles [1].vehicleCoords = << 80.0000 , 40.0000 , 9.9393 >>
			TestVehicles [1].vehicleHeading = -93.0
			TestVehicles [1].vehiclemodel = FELTZER2
			
			//REQUEST_VEHICLE_RECORDING(1, "titan")
			
			//Create a ped
			TestPeds[0].PedsCoords = << 80.0000 , 57.0000, 10.1959>> 
			TestPeds[0].PedHeading = 359.6439
			TestPeds[0].Pedrelgrp = 1
			TestPeds[0].PedModel = S_M_Y_COP_01
			TestPeds[0].PedsWeapon = WEAPONTYPE_PISTOL
		Break
		
		CASE TrainTest
			TestVehicles [0].vehicleCoords = << 20.0000 , 30.0000 , 10.9393 >>
			TestVehicles [0].vehicleHeading = 0.0
			TestVehicles [0].vehiclemodel = FREIGHT
		BREAK
		
		CASE PlaneDriveBy
			PlayerStartPos = <<664.56,48.84,7.36>>
			
			TestPeds[0].PedsCoords = <<642.49, 49.29, 7.36>> 
            TestPeds[0].PedHeading = 60.0001
            TestPeds[0].PedModel = S_M_M_GAFFER_01
			
            TestVehicles[0].VehicleCoords = <<642.61, 38.08, 7.36>> 
            TestVehicles[0].VehicleHeading = 0.0
            TestVehicles[0].Vehiclemodel = LAZER // VULKAN // This test will probably no longer work correctly as the LAZER is not a VTOL vehicle
			
			TestVehicles[1].VehicleCoords = <<373.67, 19.61, 7.36>> 
            TestVehicles[1].VehicleHeading = 30.0001
            TestVehicles[1].Vehiclemodel = TAILGATER     
			
			TestVehicles[2].VehicleCoords = <<373.67, 29.61, 7.36>> 
            TestVehicles[2].VehicleHeading = 30.0001
            TestVehicles[2].Vehiclemodel = TAILGATER     
		BREAK
		
		CASE HeliDriveBy
			PlayerStartPos = <<664.56,48.84,7.36>>
			
			TestPeds[0].PedsCoords = <<642.49, 49.29, 7.36>> 
            TestPeds[0].PedHeading = 60.0001
            TestPeds[0].PedModel = S_M_M_GAFFER_01
			
            TestVehicles[0].VehicleCoords = <<642.61, 38.08, 7.36>> 
            TestVehicles[0].VehicleHeading = 0.0
            TestVehicles[0].Vehiclemodel = BUZZARD         
			
			TestVehicles[1].VehicleCoords = <<373.67, 19.61, 7.36>> 
            TestVehicles[1].VehicleHeading = 30.0001
            TestVehicles[1].Vehiclemodel = TAILGATER
		BREAK
		
	ENDSWITCH	
ENDPROC

PROC RUN_TEST ()
		SWITCH int_to_enum (scenarios, gcurrentselection)		// the ENUM corresponds to the values from the XML file 
			//setup scenario
			CASE TestExample
				//Here we add some test cases
				IF NOT IS_PED_INJURED(TestPeds[0].ped)
					if IS_VEHICLE_DRIVEABLE(TestVehicles[0].Vehicle)
						TEST_SEQ = TEST_TASK_OPEN_VEHICLE_DOOR (TestVehicles[0].vehicle, 0)
						PedPerformsSequence (TestPeds[0].ped, TEST_SEQ)
						Set_Test_State_To_Default()
					ENDIF
				ENDIF
			BREAK
			
			CASE PointRouteTest
				IF NOT IS_PED_INJURED (TestPeds[0].ped)
					//IF IS_VEHICLE_DRIVEABLE (TestVehicles [0].Vehicle)
						//IF IS_VEHICLE_DRIVEABLE (TestVehicles [1].Vehicle)
							//SET_PED_INTO_VEHICLE (TestPeds[0].ped, TestVehicles [0].Vehicle )
							//DRIVE_TO_POINT_SEQUENCE (TestPeds[0].ped, TestVehicles [0].Vehicle)
							
							
							
							// Load the models
							//REQUEST_MODEL(PACKER)
							//REQUEST_MODEL(TRAILERS)
							//REQUEST_PTFX_ASSET("script")
							
							//LOAD_ALL_OBJECTS_NOW()
							
							//WHILE NOT HAS_MODEL_LOADED(PACKER)
							//OR NOT HAS_MODEL_LOADED(TR3)
							//ENDWHILE

							//DISPLAY_RADAR(TRUE)
							//DISPLAY_HUD(TRUE)   

						//	IF IS_PLAYER_PLAYING(PLAYER_ID())
							//		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						//            SET_PED_COORDS_KEEP_VEHICLE(player_ped_id(), <<717.3974, -979.5804, 23.0563>>)
						//          SET_ENTITY_HEADING(player_ped_id(), 257.9481)
						//	ENDIF
							
							//TestVehicles [0].Vehicle = CREATE_VEHICLE(PACKER, << 0.0000 , 61.0000 , 8 >> , 15)
							
							//TestVehicles [1].Vehicle = CREATE_VEHICLE(TRAILERS, << 14.0000 , 60.0000 , 8 >> , 30)

						
							ATTACH_VEHICLE_TO_TRAILER(TestVehicles [0].Vehicle, TestVehicles [1].Vehicle)
							SET_VEHICLE_ON_GROUND_PROPERLY(TestVehicles [0].Vehicle)
						//ENDIF
					//ENDIF
				ENDIF
				Set_Test_State_To_Default()
			BREAK
			
			CASE CarSetTempAction
				IF NOT IS_PED_INJURED (TestPeds[0].ped)
					IF IS_VEHICLE_DRIVEABLE (TestVehicles [0].Vehicle)
						//Block_Peds_Temp_Events (TestPeds[0], TRUE)
						SET_PED_INTO_VEHICLE (TestPeds[0].ped, TestVehicles [0].Vehicle )
						//TASK_VEHICLE_TEMP_ACTION (TestPeds[0].ped, TestVehicles [0].Vehicle, TEMPACT_REVERSE,2000)
					ENDIF
				ENDIF
				Set_Test_State_To_Default()
			BREAK
			
			CASE CarDrive
			
				//SET_VEHICLE_DOOR_CONTROL(TestVehicles [0].Vehicle, SC_DOOR_BOOT, DT_DOOR_INTACT, 1.25)
				
				//fAbilityGunCarBootAngle = GET_VEHICLE_DOOR_ANGLE_RATIO(AbilityGunCarID, SC_DOOR_BOOT)
			/*	
				SET_VEHICLE_DOOR_OPEN(TestVehicles [0].Vehicle, SC_DOOR_FRONT_LEFT)
				
				SET_VEHICLE_TYRE_FIXED(TestVehicles [0].Vehicle, SC_WHEEL_CAR_FRONT_LEFT)
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED (KEY_S)
					gBootOpen = !gBootOpen
				ENDIF
				
				IF gBootOpen
					SET_VEHICLE_DOOR_OPEN(TestVehicles [0].Vehicle, SC_DOOR_BOOT)
				ELSE
					SET_VEHICLE_DOOR_SHUT(TestVehicles [0].Vehicle, SC_DOOR_BOOT)
				ENDIF
				*/
				//DrawLiteralStringFloat("fCarBootAngleRatio ", fCarBootAngleRatio, 2, HUD_COLOUR_BLUE)
				//DrawLiteralStringFloat("fAbilityGunCarBootAngle ", fAbilityGunCarBootAngle, 3, HUD_COLOUR_BLUE)
				//TASK_VEHICLE_DRIVE_TO_COORD(TestPeds[0].ped,TestVehicles [0].Vehicle,HeliTargetCoord,30.0,DRIVINGSTYLE_NORMAL,SQUALO,DRIVINGMODE_PLOUGHTHROUGH,10.0,10)					

				//SET_VEHICLE_DOOR_CONTROL(TestVehicles [0].Vehicle, SC_DOOR_BOOT, DT_DOOR_SWINGING_FREE, 0.05)
				
				IF GET_CONVERTIBLE_ROOF_STATE( TestVehicles[0].Vehicle ) = CRS_LOWERED
					RAISE_CONVERTIBLE_ROOF( TestVehicles[0].Vehicle)
				ENDIF
				
				IF NOT gAnimPlayed
					LOWER_CONVERTIBLE_ROOF( TestVehicles[0].Vehicle, TRUE  )
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF( TestVehicles [0].Vehicle, SC_DOOR_BONNET, FALSE )
			
					gAnimPlayed = TRUE
					/*IF HAS_ANIM_DICT_LOADED("FELTZER2")
						
						TASK_VEHICLE_PLAY_ANIM( TestVehicles[0].Vehicle, "FELTZER2", "FELTZER2_ROOF" )
						
					ENDIF*/
					//SET_VEHICLE_DIGGER_ARM_POSITION(TestVehicles[0].Vehicle, 1.0)
				ENDIF
				
			BREAK
			
			CASE CarDriveWander
				IF NOT IS_PED_INJURED (TestPeds[0].ped)
					IF IS_VEHICLE_DRIVEABLE (TestVehicles [1].Vehicle)
						//Block_Peds_Temp_Events (TestPeds[0], TRUE)
						//SET_PED_INTO_VEHICLE (TestPeds[0].ped, TestVehicles [0].Vehicle )
						//TASK_VEHICLE_TEMP_ACTION (TestPeds[0].ped, TestVehicles [0].Vehicle, TEMPACT_REVERSE,2000)
						//TASK_VEHICLE_DRIVE_WANDER(TestPeds[0].ped, TestVehicles [0].Vehicle, 5, DRIVINGMODE_STOPFORCARS )
						
						IF IS_BUTTON_JUST_PRESSED(PAD1, RIGHTSHOULDER1)
							 SET_VEHICLE_TOW_TRUCK_ARM_POSITION(TestVehicles [1].Vehicle, 1.0)
						ENDIF
						IF IS_BUTTON_JUST_PRESSED(PAD1, LEFTSHOULDER1)
							SET_VEHICLE_TOW_TRUCK_ARM_POSITION(TestVehicles [1].Vehicle, 0.5)
						ENDIF
						IF IS_BUTTON_JUST_PRESSED(PAD1, RIGHTSHOULDER2)
							SET_VEHICLE_TOW_TRUCK_ARM_POSITION(TestVehicles [1].Vehicle, 0.0)
						ENDIF
						SET_VEHICLE_TOW_TRUCK_ARM_POSITION(TestVehicles [1].Vehicle, 0.0)
						//IF IS_KEYBOARD_KEY_JUST_PRESSED (KEY_3)
							//ATTACH_VEHICLE_TO_TOW_TRUCK(TestVehicles [1].Vehicle, TestVehicles [0].Vehicle, GET_ENTITY_BONE_INDEX_BY_NAME(TestVehicles [0].Vehicle,"overheat"), <<0.5,0,0>> )
						//ENDIF
						
						//IF IS_KEYBOARD_KEY_JUST_PRESSED (KEY_4)
						//	DETACH_VEHICLE_FROM_TOW_TRUCK(TestVehicles [1].Vehicle, TestVehicles [0].Vehicle)
						//ENDIF
						
						
						
						//ATTACH_VEHICLE_TO_TOW_TRUCK(vehTowtruck, vehTowed, GET_ENTITY_BONE_INDEX_BY_NAME(vehTowed,"overheat"), <<0.5,0,0>>)

					ENDIF
				ENDIF
				Set_Test_State_To_Default()
			BREAK
			
			
			
			CASE HeliStrafe
				IF NOT IS_PED_INJURED (TestPeds[0].ped)
					IF IS_VEHICLE_DRIVEABLE (TestVehicles [0].Vehicle)
						Block_Peds_Temp_Events (TestPeds[0], TRUE)
						SET_PED_INTO_VEHICLE (TestPeds[0].ped, TestVehicles [0].Vehicle )
						
						//Gets a reference to the player
						Get_The_Player ()
						
						SEQUENCE_INDEX TEST_SEQUENCE
							OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
								TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TestVehicles [0].Vehicle, HeliTargetCoord, MISSION_GOTO, 120.0, DRIVINGMODE_PLOUGHTHROUGH, 50, -1)
								TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TestVehicles [0].Vehicle, HeliTargetCoord1, MISSION_GOTO, 120.0, DRIVINGMODE_PLOUGHTHROUGH, 50, -1)
								TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TestVehicles [0].Vehicle, HeliTargetCoord2, MISSION_GOTO, 120.0, DRIVINGMODE_PLOUGHTHROUGH, 50, -1)
								TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TestVehicles [0].Vehicle, HeliTargetCoord3, MISSION_GOTO, 120.0, DRIVINGMODE_PLOUGHTHROUGH, 50, -1)
								TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TestVehicles [0].Vehicle, HeliTargetCoord4, MISSION_GOTO, 120.0, DRIVINGMODE_PLOUGHTHROUGH, 50, -1)

							CLOSE_SEQUENCE_TASK(TEST_SEQUENCE)
							
							TASK_PERFORM_SEQUENCE(TestPeds[0].ped, TEST_SEQUENCE)
							CLEAR_SEQUENCE_TASK(TEST_SEQUENCE)
						//	TASK_VEHICLE_DRIVE_TO_COORD(TestPeds[0].ped,TestVehicles [0].Vehicle,HeliTargetCoord,30.0,DRIVINGSTYLE_NORMAL,BUZZARD,DRIVINGMODE_PLOUGHTHROUGH,10.0,10)					
						//	SET_PED_KEEP_TASK(TestPeds[0].ped,TRUE)
					ENDIF
				ENDIF
				Set_Test_State_To_Default()
			BREAK
			
			CASE PlaneTest
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						IF IS_VEHICLE_DRIVEABLE (TestVehicles [0].Vehicle)
							SET_VEHICLE_DOOR_OPEN(TestVehicles [0].Vehicle, SC_DOOR_BOOT)
							Block_Peds_Temp_Events (TestPeds[0], TRUE)
							SET_PED_INTO_VEHICLE (TestPeds[0].ped, TestVehicles [0].Vehicle )
							//Gets a reference to the player
							Get_The_Player ()
							

							  SET_PED_COORDS_KEEP_VEHICLE(scplayer , << 92.0000 , 40.0000, 9.1959>>)
							
							//SET_PED_INTO_VEHICLE (scplayer, TestVehicles [1].Vehicle )
							//SET_VEHICLE_FORWARD_SPEED(TestVehicles [0].Vehicle, 0.0)
					
							
							//POP_OUT_VEHICLE_WINDSCREEN(TestVehicles [1].Vehicle)
							
							//Object = CREATE_OBJECT (Prop_BoxPile_03A , << 95.0000 , 40.0000 , 7.9393 >> )
							
							//SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(Object, TRUE)
							//FREEZE_ENTITY_POSITION( Object, FALSE)
							//APPLY_FORCE_TO_ENTITY(Object, APPLY_TYPE_FORCE, <<0.0, -0.1, 0.0>>, <<0.0, 0.0, 0.0>>, 0, FALSE, TRUE, TRUE)
							

	
							SEQUENCE_INDEX TEST_SEQUENCE
							OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
								TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TestVehicles [0].Vehicle, PlaneTargetCoord, MISSION_GOTO, 1.0, DRIVINGMODE_PLOUGHTHROUGH, 20, -1)
								TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TestVehicles [0].Vehicle, PlaneTargetCoord1, MISSION_GOTO, 60.0, DRIVINGMODE_PLOUGHTHROUGH, 100, -1)
								TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TestVehicles [0].Vehicle, PlaneTargetCoord2, MISSION_GOTO, 60.0, DRIVINGMODE_PLOUGHTHROUGH, 50, -1)
							CLOSE_SEQUENCE_TASK(TEST_SEQUENCE)
							
							TASK_PERFORM_SEQUENCE(TestPeds[0].ped, TEST_SEQUENCE)
							CLEAR_SEQUENCE_TASK(TEST_SEQUENCE)
					
					
						ENDIF
					ENDIF
				Set_Test_State_To_Default()
			BREAK
			
			CASE TrainTest
					IF NOT IS_PED_INJURED (TestPeds[0].ped)
						IF IS_VEHICLE_DRIVEABLE (TestVehicles [0].Vehicle)
							Block_Peds_Temp_Events (TestPeds[0], TRUE)
							SET_PED_INTO_VEHICLE (TestPeds[0].ped, TestVehicles [0].Vehicle )
							//Gets a reference to the player
							Get_The_Player ()
							
							SET_VEHICLE_FORWARD_SPEED(TestVehicles [0].Vehicle, 10.0)

	
							SEQUENCE_INDEX TEST_SEQUENCE
							OPEN_SEQUENCE_TASK (TEST_SEQUENCE)
								TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TestVehicles [0].Vehicle, PlaneTargetCoord, MISSION_GOTO, 60.0, DRIVINGMODE_PLOUGHTHROUGH, 50, -1)
								TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TestVehicles [0].Vehicle, PlaneTargetCoord1, MISSION_GOTO, 60.0, DRIVINGMODE_PLOUGHTHROUGH, 50, -1)
								TASK_VEHICLE_MISSION_COORS_TARGET(NULL, TestVehicles [0].Vehicle, PlaneTargetCoord2, MISSION_GOTO, 60.0, DRIVINGMODE_PLOUGHTHROUGH, 50, -1)
							CLOSE_SEQUENCE_TASK(TEST_SEQUENCE)
							
							TASK_PERFORM_SEQUENCE(TestPeds[0].ped, TEST_SEQUENCE)
							CLEAR_SEQUENCE_TASK(TEST_SEQUENCE)
							
					
					
						ENDIF
					ENDIF
				Set_Test_State_To_Default()
			BREAK	
			
			CASE PlaneDriveBy
					
				// Warp ped into driver seat of first vehicle
				IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD( TestVehicles[0].Vehicle)
					SET_PED_INTO_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, VS_DRIVER)					
					WAIT(100)
				ENDIF
					
				// Fly plane to designated coord
				IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD( TestVehicles[0].Vehicle)
					PlaneTargetCoord = <<444.45,24.65,17.05>>
					TASK_PLANE_GOTO_PRECISE_VTOL(TestPeds[0].Ped, TestVehicles[0].Vehicle, PlaneTargetCoord )	
					WAIT(100)
				ENDIF
							
				// Task with drive by 
				IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD( TestVehicles[0].Vehicle) AND NOT IS_ENTITY_DEAD( TestVehicles[1].Vehicle)
					PlaneTargetCoord1 = <<0.0,0.0,0.0>>
					TASK_DRIVE_BY( TestPeds[0].Ped, NULL, TestVehicles[1].Vehicle, PlaneTargetCoord1, 10000, 20, TRUE )	
					WAIT(10000)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD( TestVehicles[2].Vehicle) 
					PlaneTargetCoord1 = <<0.0,0.0,0.0>>
					SET_DRIVEBY_TASK_TARGET( TestPeds[0].Ped, NULL, TestVehicles[2].Vehicle, PlaneTargetCoord1 )
					WAIT(5000)
				ENDIF
					
				Set_Test_State_To_Default()
			BREAK
			
			CASE HeliDriveBy
					
				// Warp ped into driver seat of first vehicle
				IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD( TestVehicles[0].Vehicle)
					SET_PED_INTO_VEHICLE(TestPeds[0].Ped, TestVehicles[0].Vehicle, VS_DRIVER)
					//SET_PED_FIRING_PATTERN(TestPeds[0].Ped, FIRING_PATTERN_FULL_AUTO )
					WAIT(100)
				ENDIF
				
				// Task heli to attack
				IF NOT IS_ENTITY_DEAD(TestPeds[0].Ped) AND NOT IS_ENTITY_DEAD( TestVehicles[0].Vehicle) AND NOT IS_ENTITY_DEAD( TestVehicles[1].Vehicle)
					TASK_HELI_MISSION(TestPeds[0].Ped, TestVehicles[0].Vehicle, TestVehicles[1].Vehicle, NULL, <<0,0,0>>, MISSION_ATTACK, 10.0, 6.0, -1, 2, 8)
					WAIT(100)
				ENDIF
								
				Set_Test_State_To_Default()
			BREAK
			
		ENDSWITCH	

ENDPROC


//Purpose: Runs the main scenarios
PROC Run_Test_Scenario ()
	int index = 0
	
	SWITCH TestScenarioAStatus
		//setps up all the data for the sceanrio, this is only called once or if selected in widget
		CASE InitialiseScenarioData
			PlayerStartPos = GET_PLAYER_START_POS()				//sets the players start coords at the default
			Bscenario_running = FALSE
			INITIALISE_PED_DATA (Testpeds)
			INITIALISE_VEHICLE_DATA (TestVehicles)
			INITIALISE_CAM_DATA(TestCams )
			SETUP_TEST_DATA ()
	
			HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
			TestScenarioAStatus = CreateScenarioEntities
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)		//sets the 
		
		BREAK
		
		// Creates all the scenario data
		CASE CreateScenarioEntities
			Bscenario_running = FALSE
			
			Set_Gang_Relationships (TRUE)	
		
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
		
			//peds
			FOR index = 0 to MAX_NUMBER_OF_PEDS - 1
				CREATE_PED_ENTITY(Testpeds[index])
				GiveBlipsToPedsGrps (Testpeds[index])
				ALTER_COMBAT_STATS (Testpeds[index] )
				SWAP_PED_WEAPONS (Testpeds[index])
				ADD_RELATIONSHIP_GROUP_TO_STRUCT (Testpeds[index])
				Block_Peds_Temp_Events (Testpeds[index], TRUE)
				SET_PED_DEFENSIVE_AREAS(Testpeds[index] )
			ENDFOR
			
			//TestVehicles
			FOR Index = 0 to MAX_NUMBER_OF_VEHICLES -1
				CREATE_VEHICLE_ENTITY (TestVehicles[index] )
			ENDFOR
		
			//TestCams
			for index = 0 to MAX_NUMBER_OF_CAMERAS -1
				CREATE_CAM_ENTITY (TestCams[index])
			ENDFOR
			
			TestScenarioAStatus = SetScenarioEntities
		BREAK
		
		CASE SetScenarioEntities
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
			
			Start_And_Reset_Test ()
			
			IF gBeginCombatScenario
				TestScenarioAStatus = RunScenario
				gBeginCombatScenario = FALSE
				gRun_debuggig = FALSE
				Bscenario_running = TRUE
				INITALISE_TEST_STATE()
				TEMP_ACTIVATE_CAMS (TestCams[FixedCamera].cam )
				
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam )
					HELP_TEXT_STATE =  HTF_SCENARIO_RUNNING_FIXED_CAM
				ELSE
					HELP_TEXT_STATE = HTF_STARTED_RUNNING_NO_CAMS
				ENDIF
			ENDIF
		BREAK
		
		//Runs the actual selected scenario
		
		CASE RunScenario
				
			Temp_Run_Scenario_Tracking_Cam (TestPeds, MAX_NUMBER_OF_PEDS, TestCams[TrackingCamera].cam) 
			
			Temp_Update_Player_With_Scenario(TestCams, scplayer, true, PlayerStartPos)
				
			Start_And_Reset_Test ()
				
		 	Check_For_Scenario_Reset ()
			
			IF DOES_CAM_EXIST (TestCams[TrackingCamera].cam)
				if IS_CAM_RENDERING (TestCams[TrackingCamera].cam)
					HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_TRACKING_CAM
				ELSE
					HELP_TEXT_STATE = HTF_SCENARIO_RUNNING_FIXED_CAM
				ENDIF
			ENDIF
					
			RUN_TEST ()		//run the main tests 		
			
		BREAK 
				
		CASE CleanupScenario
			
			HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
		
			Cleanup_Scenario_Entities ()
			
			IF gResetToDefault 	
				Temp_cleanup_scenario_cams ()		//here we are changing scenarios so we need to reset cams
				TestScenarioAStatus = InitialiseScenarioData
				gResetToDefault = FALSE
			ENDIF
		
			IF gResetCombatScenario
				Temp_Update_Player_With_Scenario(TestCams, scplayer, FALSE, PlayerStartPos)
				gcurrentselection = gSelection 
				IF DOES_CAM_EXIST (TestCams[FixedCamera].cam)
					ACTIVATE_CAM (TestCams[FixedCamera].cam)
				ELSE
					Temp_cleanup_scenario_cams ()	
				ENDIF
				TestScenarioAStatus = CreateScenarioEntities
				gResetCombatScenario = FALSE
			ENDIF
		
		BREAK
		
	ENDSWITCH
ENDPROC

SCRIPT
	
	SET_DEBUG_ACTIVE (TRUE)
	
	//gvMapOffset = GET_PLAYER_START_POS ()
	
	SETUP_MISSION_XML_MENU (XMLMenu, KEY_Q )	//defined at the top of the file
	
	SETUP_AREA_FOR_MISSION (<<0.0, 0.0, 0.0>>)
	
	//Gets a reference to the player
	Get_The_Player ()
	
	//set player collios
	SET_PLAYER_COLISION(scplayer, true)
	
	//Sets the test widget from the test tools
	CREATE_TEST_WIDGET ()
	
	//request the test anim bank
	//REQUEST_TEST_ANIM_DICT ("FELTZER2")

	WHILE TRUE
			
		// controls the help text hides if xml menu is active
		TEXT_CONTROLLER ()
	
		// Can set all scenario peds invincible from the widget
		Set_Scenario_Peds_Invincible (TestPeds, MAX_NUMBER_OF_PEDS, gsetpedsinvincible )
	
		//User can create a debug cam for setting sceanrios
		Temp_Create_Debug_Cam (TestCams[FixedCamera]) 
		
		//Runs the selected option from the XML menu 
		Run_Selection_From_XML_input ()
		
		//Checks that a valid selection has been input and runs the scenario
		IF (gcurrentselection <> InvalidSelection)
	
			Draw_Debug_Info (  )
			
			//Sets the test scenario into debug mode
			IF (gAllowDebugging)
				Set_To_Debug ()
			ENDIF
			
			if (gRun_debuggig)
				Temp_Debug_Scenario (TestPeds, TestVehicles, Route, TestCams[FixedCamera].cam, S_M_Y_COP_01)								//Allows the entities in the scneario to be adjusted
				Print_Scenario_Data (TestPeds, TestVehicles, Route, TestCams)
			ENDIF
			
			Run_Test_Scenario ()								
			
			SWITCH_BETWEEN_FIXED_AND_TRACKING (TestCams[FixedCamera].cam)
			
			PRINT_ACTIVE_TEST (Bscenario_running, gRun_debuggig)		
		ENDIF
		
		Terminate_test_script ()
		
		WAIT (0)
	
	
	ENDWHILE

ENDSCRIPT


#ENDIF	// IS_DEBUG_BUILD

