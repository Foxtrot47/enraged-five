
USING "commands_ped.sch"
USING "commands_vehicle.sch"
USING "commands_debug.sch"
USING "commands_object.sch"
USING "commands_streaming.sch"
USING "globals.sch"
USING "commands_hud.sch"
USING "commands_debug.sch"
USING "commands_graphics.sch"
USING "commands_pad.sch"
USING "commands_misc.sch"
USING "commands_camera.sch"
USING "commands_player.sch"
USING "commands_task.sch"
USING "script_xml.sch"
USING "commands_entity.sch"
USING "commands_shapetest.sch"

// This is to be used for test scenario functionality. It is a data driven system using the structs below. When a scenario is setup  the data is read in the scenario is intiated. Each member of the struct can be accessed
//allowing for quick setup of scenarios.

PED_INDEX DefaultPedIndex = NULL
VEHICLE_INDEX DefaultVehicleIndex = NULL
//BLIP_INDEX test_blip

//peds 
INT gTestPedsIndex = 0
VECTOR gPedcoords
FLOAT gPedheading
BOOL Bprint_ped_info = FALSE

STRUCT peds_struct
    PED_INDEX Ped
    BLIP_INDEX PedBlip
    VECTOR PedsCoords
    FLOAT PedHeading
    WEAPON_TYPE PedsWeapon
    STRING pedsstringweapon
    MODEL_NAMES PedModel
    MODEL_NAMES PedWeaponModel
    VECTOR PedDefCoord                      //defensive area pos
    FLOAT PedDefArea                            //defensive area radius
    BOOL PedActiveDefArea                   //ped has an assigned active area
    VECTOR PedDefDirection
    INT Pedrelgrp
    int pedCombatMove 
    int pedcombatrange
    int pedcombatability
    COMBAT_ATTRIBUTE combatcover 
    BOOL bcombatcover
    COMBAT_ATTRIBUTE combatUseVehicle
    BOOL bcombatUseVehicle
    COMBAT_ATTRIBUTE Combatdodrivebys
    BOOL bCombatdodrivebys
    COMBAT_ATTRIBUTE combatleavevehicle
    BOOL bcombatleavevehicle
    COMBAT_ATTRIBUTE combativestevents
    BOOL bcombativestevents
    COMBAT_ATTRIBUTE combativestpeds
    BOOL bcombativestpeds
ENDSTRUCT

int pedidentifier            //selects which ped model will be loaded
int gpedrelgrp = 1       //Sets up the ped relationship grp

//peds combat
BOOL gsetCombatMove = FALSE
BOOL gsetCombatability = FALSE
BOOL gsetCombatRange = FALSE
BOOL gsetCombatAttribute = FALSE

int gCombatMove = 0
int gCombatRange = 0
int gCombatAbility = 0
int gCombatAttributes = 0
BOOL gActivateCombatAttributes = FALSE

CONST_INT MAX_NUMBER_OF_COMBAT_ATTRIBUTES       6           

// vehicle structs
STRUCT vehicle_struct
    VEHICLE_INDEX Vehicle
    MODEL_NAMES VehicleModel
    VECTOR VehicleCoords
    FLOAT VehicleHeading
ENDSTRUCT

STRUCT Object_struct
    OBJECT_INDEX Object
    MODEL_NAMES ObjectModel
    VECTOR ObjectCoords
    VECTOR ObjectRotation
ENDSTRUCT

int objectidentifier

//vehicles
INT gTestVehicleIndex
VECTOR gVehiclecoords
FLOAT gVehicleheading
BLIP_INDEX VehicleBlip

BOOL Bprint_vehicle_info


int Vehicledentifier

//entity setup

int WeaponIdentifier


bool  gPrintAllIinfo = FALSE
BOOL gBeginCombatScenario = FALSE
BOOL gResetCombatScenario = FALSE
BOOL gRun_debuggig = FALSE
BOOL gResetToDefault = FALSE
BOOL gSwapPedWeapons = FALSE

BOOL gCreateVehicle = FALSE, gCreatePed = FALSE
BOOL gdeletePed = FALSE, gDeleteVehicle = FALSE, gsetpedsinvincible = FALSE

// Relationship groups
BOOL gCreatedRelationshipGroups = FALSE
REL_GROUP_HASH RELGROUP_MISSION1
REL_GROUP_HASH RELGROUP_MISSION2
//REL_GROUP_HASH RELGROUP_MISSION3
//REL_GROUP_HASH RELGROUP_MISSION4


// defensive area
VECTOR gPedDefACoords
FLOAT gPedDefARadius
BOOL gPedDefAActive = FALSE

//Routes
VECTOR gRoutePoint

INT gRouteNode = 0
INT gcurrentNode = -1

BOOL BSetNode = FALSE
BOOL BCopyCamCoords = FALSE

STRUCT RouteStruct
    VECTOR RouteNode
    BOOL RouteNodeState
ENDSTRUCT

//Shape tests
STRUCT ShapeTestStruct
	SHAPETEST_INDEX ShapeTest
	SHAPETEST_TYPE	eShapeTestType
	//Input
	VECTOR			vStartPos
	VECTOR			vEndPos
	VECTOR			vDims 			//For box
	VECTOR			vEulerAngles 	//For box
	EULER_ROT_ORDER eEulerOrder		//For box
	FLOAT			fRadius
	BOOL			bRequested
	ENTITY_INDEX	EntityIndex //For bounding box test
	//Results
	BOOL			bResultIsNew
	VECTOR			vHitPos
	VECTOR			vHitNormal
	BOOL			bIsHit
ENDSTRUCT

//Camera Vars
CAMERA_INDEX TestCam
CAMERA_INDEX SkiCam

INT camscope = 0
int camped = 0          //the ped the camera is attached to
int CamPointPed = 0   //the ped the camera is pointing at

float gZoom, gPan, gPitch


STRUCT StCameraData
    CAMERA_INDEX cam
    VECTOR cam_pos
    VECTOR cam_rot
    FLOAT cam_fov
    bool bActivateCam 
ENDSTRUCT

StCameraData TestCamInfo

//Scenario Control Vars
CONST_INT InvalidSelection  -1                      



//DEBUG
ENUM Debug_mode
    DebugPed,
    DebugVehicle,
    DebugDefensiveArea,
    DebugRoute,
    DebugTargetDestination,
    Debug_default
ENDENUM

Debug_mode  DebugStatus = DebugPed

VECTOR DynamicTargetPos 
FLOAT DynamicTargetHeading 

BOOL Bscenario_running = FALSE 


//Widgets
PROC SETUP_WIDGET ()

        START_WIDGET_GROUP ("Setup Widget")
            ADD_WIDGET_BOOL ("Start scenario", gBeginCombatScenario)
            ADD_WIDGET_BOOL ("Restart scenario", gResetCombatScenario)
            ADD_WIDGET_BOOL ("Reset to default", gResetToDefault)
            ADD_WIDGET_BOOL ("Debug scenario", gRun_debuggig)
            ADD_WIDGET_BOOL ("Print all setup info", gPrintAllIinfo)
            ADD_WIDGET_BOOL ("Set peds invincible", gsetpedsinvincible)
            START_WIDGET_GROUP ("Ped setup")
                ADD_WIDGET_INT_SLIDER ("Ped_index", gTestPedsIndex ,0, 9, 1 )
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Coord.x",gPedcoords.x,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Coord.y",gPedcoords.y,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Coord.z",gPedcoords.z,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Heading",gPedheading,  -2000, 2000, 0.5)
                    START_NEW_WIDGET_COMBO() 
                        ADD_TO_WIDGET_COMBO ("G_M_M_ALPHENCH_01")
                        ADD_TO_WIDGET_COMBO ("G_M_Y_DESHENCH_01")
                    STOP_WIDGET_COMBO("Ped", PedIdentifier)
                        ADD_WIDGET_BOOL ("Create ped", gCreatePed)
                        ADD_WIDGET_BOOL ("Delete ped", gdeletePed)
                        ADD_WIDGET_INT_SLIDER ("Set Ped rel grp", gpedrelgrp  ,1, 4, 1 )
                    START_NEW_WIDGET_COMBO() 
                        ADD_TO_WIDGET_COMBO ("WEAPONTYPE_UNARMED")
                        ADD_TO_WIDGET_COMBO ("WEAPONTYPE_PISTOL")
                        ADD_TO_WIDGET_COMBO ("WEAPONTYPE_ASSAULTRIFLE")
                        ADD_TO_WIDGET_COMBO ("WEAPONTYPE_M4")
                    STOP_WIDGET_COMBO("weapon", WeaponIdentifier)
                        ADD_WIDGET_BOOL ("Swap current peds weapon", gSwapPedWeapons) 
                        ADD_WIDGET_BOOL ("print ped info", Bprint_ped_info)
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Def_A_Coord.x",gPedDefACoords.x,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Def_A_Coord.y",gPedDefACoords.y,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Def_A_Coord.z",gPedDefACoords.z,  -2000, 2000, 0.25)  
                ADD_WIDGET_FLOAT_SLIDER ("Ped_Def_A_Radius.z",gPedDefARadius,  -2000, 2000, 0.25)   
                ADD_WIDGET_BOOL ("activate def area",gPedDefAActive)    
            STOP_WIDGET_GROUP ()            
                START_WIDGET_GROUP ("combat")
                    ADD_WIDGET_INT_SLIDER ("Ped_index", gTestPedsIndex ,0, 9, 1 )   
                        START_NEW_WIDGET_COMBO() 
                            ADD_TO_WIDGET_COMBO ("CM_STATIONARY")
                            ADD_TO_WIDGET_COMBO ("CM_DEFENSIVE")
                            ADD_TO_WIDGET_COMBO ("CM_WILLADVANCE")
                        STOP_WIDGET_COMBO("Combat Movement", gCombatMove)
                        ADD_WIDGET_BOOL ("Set ped combat move", gsetCombatMove  ) 
                        START_NEW_WIDGET_COMBO() 
                            ADD_TO_WIDGET_COMBO ("CR_NEAR")
                            ADD_TO_WIDGET_COMBO ("CR_MEDIUM")
                            ADD_TO_WIDGET_COMBO ("CR_FAR")
                        STOP_WIDGET_COMBO("Combat Range", gCombatRange)
                            ADD_WIDGET_BOOL ("Set ped combat ability", gsetCombatRange  ) 
                        START_NEW_WIDGET_COMBO() 
                            ADD_TO_WIDGET_COMBO ("CAL_POOR")
                            ADD_TO_WIDGET_COMBO ("CAL_AVERAGE")
                            ADD_TO_WIDGET_COMBO ("CAL_PROFESSIONAL")
                        STOP_WIDGET_COMBO("Combat Ability", gCombatAbility)
                        ADD_WIDGET_BOOL ("Set ped combat ability", gsetCombatability  ) 
                        START_NEW_WIDGET_COMBO() 
                            ADD_TO_WIDGET_COMBO ("CA_USE_COVER")
                            ADD_TO_WIDGET_COMBO ("CA_USE_VEHICLE")
                            ADD_TO_WIDGET_COMBO ("CA_DO_DRIVEBYS")
                            ADD_TO_WIDGET_COMBO ("CA_LEAVE_VEHICLES")
                        STOP_WIDGET_COMBO("Combat Attributes", gCombatAttributes)        
                            ADD_WIDGET_BOOL ("Activate attributes", gActivateCombatAttributes) 
                            ADD_WIDGET_BOOL ("set ped attribute", gsetCombatAttribute) 
                STOP_WIDGET_GROUP()
            START_WIDGET_GROUP ("Vehicle setup")
                ADD_WIDGET_INT_SLIDER ("Vehicle", gTestVehicleIndex ,0, 9, 1 )
                ADD_WIDGET_FLOAT_SLIDER ("Vehicle_Coord.x",gVehiclecoords.x,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("Vehicle_Coord.y",gVehiclecoords.y,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("Vehicle_Coord.z",gVehiclecoords.z,  -2000, 2000, 0.25)
                ADD_WIDGET_FLOAT_SLIDER ("Vehicle_Heading",gVehicleheading,  -2000, 2000, 0.5)
                ADD_WIDGET_BOOL ("Create vehicle", gCreateVehicle)
                ADD_WIDGET_BOOL ("Delete vehicle", gDeleteVehicle)
                ADD_WIDGET_BOOL ("print vehicle info", Bprint_vehicle_info)
            STOP_WIDGET_GROUP ()
    
            START_WIDGET_GROUP ("objects")  
                ADD_WIDGET_INT_SLIDER ("Vehicle", gTestVehicleIndex ,0, 9, 1 )
                    START_NEW_WIDGET_COMBO() 
                        ADD_TO_WIDGET_COMBO ("P_ALP_BARREL01")
                        ADD_TO_WIDGET_COMBO ("P_ALP_BARREL02")
                    STOP_WIDGET_COMBO("Object", objectidentifier)
                STOP_WIDGET_GROUP()
        STOP_WIDGET_GROUP() 
ENDPROC

//~>                HELP TEXT 
ENUM HELP_TEXT_FLAGS
    HTF_NO_SCENARIO_SELECTED,
    HTF_SCENARIO_SELECTED_NOT_STARTED,
    HTF_STARTED_RUNNING_NO_CAMS,
    HTF_SCENARIO_RUNNING_FIXED_CAM,
    HTF_SCENARIO_RUNNING_TRACKING_CAM,
    HTF_DEBUG_PEDS,
    HTF_DEBUG_VEHICLES,
    HTF_DEBUG_DEFENSIVE_AREAS,
    HTF_DEBUG_ROUTES,
    HTF_DEBUG_DA_TARGET,
	HTF_DEBUG_FORKLIFT,
	HTF_DEBUG_KINEMATIC,
    HTF_WAITING_FOR_NEW_TEXT_STATE
ENDENUM

HELP_TEXT_FLAGS HELP_TEXT_STATE = HTF_NO_SCENARIO_SELECTED

PROC FORMAT_HELP_TEXT (STRING text)
    IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED (text)
        CLEAR_HELP ()
        PRINT_HELP_FOREVER (text)
    ENDIF   
ENDPROC

bool gbPrintGlobalText = true

PROC TEXT_CONTROLLER ()
    
    IF IS_XML_MENU_ON_SCREEN ()
            HIDE_HELP_TEXT_THIS_FRAME ()
    ENDIF       
    
    IF gbPrintGlobalText    
        SWITCH HELP_TEXT_STATE
            CASE HTF_NO_SCENARIO_SELECTED
                    FORMAT_HELP_TEXT ("tstdbg10")   //Scenario: Press Q for test menu.                  
                    HELP_TEXT_STATE = HTF_WAITING_FOR_NEW_TEXT_STATE
            BREAK
        
            CASE HTF_SCENARIO_SELECTED_NOT_STARTED
                FORMAT_HELP_TEXT ("tstdbg04")   //Test: LB+A:Start Test~n~ LB+B: Reset Test ~n~ LS: Press to Debug~n~  Q: new scenario 
                HELP_TEXT_STATE = HTF_WAITING_FOR_NEW_TEXT_STATE
            BREAK
            
            CASE HTF_SCENARIO_RUNNING_FIXED_CAM
                FORMAT_HELP_TEXT ("tstdbg08")       //  Test:LB+B: Reset Test ~n~ RB: Set Follow Cam ~n~  Q: new scenario 
                HELP_TEXT_STATE = HTF_WAITING_FOR_NEW_TEXT_STATE
            BREAK
        
            CASE HTF_SCENARIO_RUNNING_TRACKING_CAM
                FORMAT_HELP_TEXT ("tstdbg09")   //Test: LS: Cam X + Y~n~ RS: Cam Z ~n~ RB: Selct Ped ~n~ RT: Point Ped ~n~ LB+RB: Fixed Cam ~n~ LB+B: Reset Test ~n~  Q: new scenario 
                HELP_TEXT_STATE = HTF_WAITING_FOR_NEW_TEXT_STATE
            BREAK
        
            CASE HTF_DEBUG_PEDS
                FORMAT_HELP_TEXT ("tstdbg01")       //Dbg peds: (LS: Ped pos)~n~ (A/B: Slct ped up/dwn) ~n~ (Y:new ped) (LB+Y:Del ped) ~n~ (LS: Press to Exit)~n~ (RS:Next Dbg Mode)
                HELP_TEXT_STATE = HTF_WAITING_FOR_NEW_TEXT_STATE
            BREAK
            
            CASE HTF_DEBUG_VEHICLES
                FORMAT_HELP_TEXT ("tstdbg02")       //Dbg vehicles: (LS: Car pos)~n~ (A/B Slct car up/dwn) ~n~ (Y:new car) (LB+Y:Del car) ~n~ (LS: Press to Exit) ~n~ (RS:Next Dbg Mode)
                HELP_TEXT_STATE = HTF_WAITING_FOR_NEW_TEXT_STATE
            BREAK
            
            CASE HTF_DEBUG_DEFENSIVE_AREAS
                FORMAT_HELP_TEXT ("tstdbg03")       //Dbg Dfnsve A: (LS: DA Pos)~n~(RS U/D: DA z) (RS L/R: DA radius) ~n~ (A/B: Slct ped up/dwn) ~n~ (Y/LB+Y : new/del DA) ~n~ (LS/RS:Exit/Next Dbg Mode)
                HELP_TEXT_STATE = HTF_WAITING_FOR_NEW_TEXT_STATE
            BREAK
            
            CASE HTF_DEBUG_ROUTES
                FORMAT_HELP_TEXT ("tstdbg05")       //Route: = (LS: Node XY) (RS: Node Z) (A/B: Slct route up/dwn) ~n~ (Y:add/del route)
                HELP_TEXT_STATE = HTF_WAITING_FOR_NEW_TEXT_STATE
            BREAK
            
            CASE HTF_DEBUG_DA_TARGET
                FORMAT_HELP_TEXT ("tstdbg06")       //Destination: = (LS: Target XY) (RS: Target Z)  (A/B: Slct route up/dwn) ~n~ (Y:add/del route)
                HELP_TEXT_STATE = HTF_WAITING_FOR_NEW_TEXT_STATE
            BREAK
            
            CASE HTF_STARTED_RUNNING_NO_CAMS
                FORMAT_HELP_TEXT ("tstdbg12")       //Destination: = (LS: Target XY) (RS: Target Z)  (A/B: Slct route up/dwn) ~n~ (Y:add/del route)
                HELP_TEXT_STATE = HTF_WAITING_FOR_NEW_TEXT_STATE
            BREAK
			
			CASE HTF_DEBUG_FORKLIFT
				FORMAT_HELP_TEXT("tstdbg27")
			BREAK
			
			CASE HTF_DEBUG_KINEMATIC
				FORMAT_HELP_TEXT("tstdbg28")
			BREAK
            
            CASE HTF_WAITING_FOR_NEW_TEXT_STATE
            
            BREAK
    
        ENDSWITCH
    ENDIF
ENDPROC


//Functions: ANIM 
PROC REQUEST_TEST_ANIM_DICT(string animdict)
    REQUEST_ANIM_DICT (animdict)

    WHILE NOT HAS_ANIM_DICT_LOADED (animdict)
        wait (0)
    ENDWHILE

ENDPROC

// Functions End: ANIM

// Functions: PLAYER

//player 
PLAYER_INDEX theplayerindex
PED_INDEX scplayer
bool gbPlayerShouldBeAtCamPos = true


//PURPOSE: Gets a reference to the player.
PROC GET_THE_PLAYER ()
    theplayerindex =  GET_PLAYER_INDEX()
    scplayer = GET_PLAYER_PED(theplayerindex)
ENDPROC
 

//Player vars
VECTOR PlayerStartPos

FUNC VECTOR GET_PLAYER_START_POS ()
    VECTOR vPlayerStart = <<0.0, 0.0, 0.0>> 
    
	IF PlayerStartPos.X > 0
	ENDIF
	
    SWITCH GET_INDEX_OF_CURRENT_LEVEL()
        CASE LEVEL_GTA5
			PRINTLN("Level is GTA5.")
            vPlayerStart = <<-1234.70, -1135.56, 6.81 >>
        BREAK

        CASE LEVEL_VFX_TEST
			PRINTLN("Level is VFX_TEST.")
            vPlayerStart = <<0.0, 0.0, 6.0>>
        BREAK
        
        CASE LEVEL_ANIM_TEST
			PRINTLN("Level is ANIM_TEST.")
			vPlayerStart = << 14.7000, 0.5000, 1.0>>
		BREAK
		
        CASE LEVEL_TESTBED
			PRINTLN("Level is TESTBED.")
            vPlayerStart = << -3.47, 73.44, 7.36 >>
        BREAK
        
		CASE LEVEL_CP_TESTBED
			PRINTLN("Level is CP_TESTBED.")
			vPlayerStart = <<0.0705357, 65.6115, 11.7485>>
		BREAK
		
		CASE LEVEL_WATERBED
			PRINTLN("Level is WATERBED.")
			vPlayerStart = <<6.81083, -10.2265, 4.8533>>
		BREAK
		
        CASE LEVEL_NM_TEST
			PRINTLN("Level is NM_TEST.")
            vPlayerStart = <<1.0, 1.0,  6.13>>
        BREAK
		
		CASE LEVEL_TOOLS_TEST
			PRINTLN("Level is TOOLS_TEST.")
			vPlayerStart = <<1.0, 1.0,  -2.17>>
		BREAK
		
		CASE LEVEL_GFX_TEST
			PRINTLN("Level is GFX_TEST.")
			vPlayerStart = <<-234, 838, 37>>
		BREAK
		
        DEFAULT 
            SCRIPT_ASSERT ("GET_PLAYER_START_POS: Current level does not have a script ENUM assigned. Bug Graeme W.")
        BREAK
		
//      CASE LEVEL_CODETEST	
//			PRINTLN("Level is CODETEST.")
//			vPlayerStart  = <<6.75, 240.0, 26>>
//      BREAK
    
    ENDSWITCH
	
	PRINTLN("Player starting position: ", vPlayerStart, ".")
    return vPlayerStart

ENDFUNC



bool player_collision_Set = true

PROC SET_PLAYER_COLISION(PED_INDEX ped, BOOL bReady)
    IF NOT IS_PED_INJURED (ped)    
        IF NOT IS_PED_IN_ANY_VEHICLE (ped)
            IF NOT IS_PED_SITTING_IN_ANY_VEHICLE (ped)    
                if (bReady)
                    //if not player_collision_Set
                        SET_ENTITY_VISIBLE(ped, TRUE)
                        SET_ENTITY_COLLISION(ped, TRUE)
                        //SET_ENTITY_INVINCIBLE(ped, true)
                        player_collision_Set = true
                //  ENDIF 
                ELSE     
                    SET_ENTITY_VISIBLE(ped, FALSE)
                    SET_ENTITY_COLLISION(ped, FALSE)
                    //SET_ENTITY_INVINCIBLE(ped, TRUE)
                    player_collision_Set = FALSE
                        
                    ENDIF   
            ENDIF  
            
        ENDIF
    ENDIF   
ENDPROC


PROC TEMP_UPDATE_PLAYER_WITH_SCENARIO (StCameraData &lcams[],  PED_INDEX &ped, bool update, Vector vec)
  
    if Bscenario_running
    
    ENDIF
    
    int ArraySize = COUNT_OF (lcams ) 
    int index 
    vector cam_pos, player_pos
    bool bcam_is_rendering = FALSE
    CAMERA_INDEX DebugCam
    float ground_z

    DebugCam = GET_DEBUG_CAM ()
    
    if gbPlayerShouldBeAtCamPos         //some tests you want the player in them but with a fixed cam
        if update
            //attach to our cam
            FOR index = 0 to ArraySize -1
                IF DOES_CAM_EXIST (lcams[index].cam)
                    IF IS_CAM_RENDERING (lcams[index].cam)
                        
                        IF NOT IS_PED_INJURED (ped) 
                            IF IS_PED_IN_ANY_VEHICLE (ped)
                                SET_ENTITY_COORDS(PED, vec )
                            ENDIF
                        ENDIF   
                        
                        cam_pos = GET_CAM_COORD (lcams[index].cam )
                        SET_PLAYER_COLISION(ped, FALSE)
                        bcam_is_rendering = true
                        IF not IS_PED_INJURED (ped)
                              SET_PED_COORDS_KEEP_VEHICLE(ped,cam_pos)
                        ENDIF
                    ENDIF
                ENDIF
            ENDFOR  
                
            // neither cameras are active but the players collison has been set to false at some point  
            IF not bcam_is_rendering
                IF not player_collision_Set
                    //update the player if the debug cam is active
                    if IS_CAM_RENDERING (DebugCam)
                        cam_pos = GET_CAM_COORD (DebugCam )
                        IF not IS_PED_INJURED (ped)
                              SET_PED_COORDS_KEEP_VEHICLE(ped,cam_pos)
                        ENDIF
                    ELSE
                    //
                        IF NOT IS_PED_INJURED (ped)
                            player_pos = GET_ENTITY_COORDS(ped )
                            
                            GET_GROUND_Z_FOR_3D_COORD ( player_pos, ground_z)
                            SET_PLAYER_COLISION(ped, true)
                            
                             SET_PED_COORDS_KEEP_VEHICLE(ped, <<player_pos.x, player_pos.y, ground_z>> )
                        
                        ENDIF
                    ENDIF   
                ENDIF
            ENDIF
    
            
        ELSE
            
            IF NOT IS_PED_INJURED (scplayer)
                IF IS_PED_IN_ANY_VEHICLE (scplayer)
                    SET_ENTITY_COORDS(scplayer, vec )
                ENDIF
            ENDIF
            
            SET_PLAYER_COLISION(ped, true)
            
            if IS_CAM_RENDERING (DebugCam)
                    cam_pos = GET_CAM_COORD (DebugCam )
                    IF not IS_PED_INJURED (ped)
                        if  GET_GROUND_Z_FOR_3D_COORD ( cam_pos, ground_z)
                              SET_PED_COORDS_KEEP_VEHICLE(ped,<<cam_pos.x, cam_pos.y, ground_z>> )
                        ELSE
                            IF NOT IS_PED_INJURED (PED)
                                  SET_PED_COORDS_KEEP_VEHICLE(ped, vec )
                            ENDIF
                        ENDIF
                    
                    ENDIF
            ELSE
                IF NOT IS_PED_INJURED (PED)
                          SET_PED_COORDS_KEEP_VEHICLE(ped, vec )
                ENDIF
            ENDIF   
        ENDIF
    ELSE
        IF not player_collision_Set
            SET_PLAYER_COLISION(ped, true)
        ENDIF
    ENDIF
ENDPROC


//Functions end: PLAYER

// Functions: ROUTE
//Purpose: Allows a route to be added using the pos of the debug cam 
PROC SET_ROUTE_NODES(RouteStruct &routes)
    CAMERA_INDEX DCam   
    VECTOR camPos   
    ///Check the selected node is new and grab its coords
    IF gRouteNode <> gcurrentNode
        gRoutePoint = routes.RouteNode
        gcurrentNode = gRouteNode
        IF (routes.RouteNodeState )
            BSetNode = TRUE
        ELSE
            BSetNode = FALSE
        ENDIF
    ELSE
        //else update the node to coords passed in by the widget
        routes.RouteNode = gRoutePoint
    ENDIF       
    
    //use the debug cam to set node positions
    DCam = GET_DEBUG_CAM()
    camPos = GET_CAM_COORD  (DCam )
    IF (BCopyCamCoords)
        gRoutePoint =  camPos
        BCopyCamCoords = FALSE
    ENDIF

    // use this to register a node as valid the falg should be set if the route is valid.
    
    IF (routes.RouteNodeState = FALSE)
        IF BSetNode 
            routes.RouteNodeState = TRUE
        ENDIF
    ELSE
        IF (BSetNode = FALSE) 
            routes.RouteNodeState = FALSE
        ENDIF
    ENDIF
    
ENDPROC

//Purpose: Draws any registed route Nodes 
PROC  DRAW_ROUTE_POINTS (RouteStruct &routes)
    IF routes.RouteNodeState = TRUE
        DRAW_DEBUG_LINE(<<routes.RouteNode.x, routes.RouteNode.y, routes.RouteNode.z + 1.0>>, <<routes.RouteNode.x, routes.RouteNode.y, routes.RouteNode.z - 1.0>>)
    ENDIF
ENDPROC

//PURPOSE: Draws aline abobve the currently selected node
PROC  DRAW_NODE_HIGHLIGHTER (RouteStruct &routes)
    IF routes.RouteNodeState = TRUE
        DRAW_DEBUG_LINE(<<routes.RouteNode.x, routes.RouteNode.y, routes.RouteNode.z + 1.25>>, <<routes.RouteNode.x, routes.RouteNode.y, routes.RouteNode.z + 1.75>>)
    ENDIF
ENDPROC

// Functions end: ROUTE 

//DEBUG
//Functions: DEBUG
//PURPOSE: Selects which debug mode to run 
FUNC Debug_mode SELECT_DEBUG_MODE (Debug_mode enum_state)
        int icurrent_state 

        Debug_mode CurrentState = enum_state

        IF IS_BUTTON_JUST_PRESSED (PAD1, RIGHTSHOULDER1 )
                icurrent_state = ENUM_TO_INT (enum_state)
                icurrent_state += 1
                CurrentState = INT_TO_ENUM (Debug_mode, icurrent_state )
            
            IF (CurrentState =  Debug_default)      //The debug default is the last enum 
                CurrentState = INT_TO_ENUM (Debug_mode, 0 ) 
            ENDIF
    
        ENDIF
            
    RETURN CurrentState
ENDFUNC

//PURPOSE: Starts and resets the sceanario on the players input
PROC Start_And_Reset_Test ()
    IF NOT gRun_debuggig
        if IS_BUTTON_PRESSED (PAD1, LEFTSHOULDER1)
            IF IS_BUTTON_JUST_PRESSED (PAD1, CROSS )
                gBeginCombatScenario = TRUE
                HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
            ENDIF
        ENDIF
        
        IF IS_BUTTON_PRESSED (PAD1, LEFTSHOULDER1) 
            if IS_BUTTON_JUST_PRESSED (PAD1, CIRCLE)
                gResetCombatScenario = TRUE
            ENDIF
        ENDIF
    ENDIF
ENDPROC

//PURPOSE: Draws the lines for object placement
PROC  DRAW_DEBUG_LINE_FOR_ENTITY_PLACEMENT (peds_struct lpeds, vehicle_struct lvehicles  )
    //draw ped line
    VECTOR TempCoords 
    VECTOR TempVehicleCoords
        IF not DOES_ENTITY_EXIST(lpeds.ped)
            DRAW_DEBUG_LINE( gPedcoords, <<gPedcoords.x, gPedcoords.y, gPedcoords.z+1>>)
        ELSE
            IF NOT IS_PED_INJURED (lpeds.ped)
                TempCoords = GET_ENTITY_COORDS(lpeds.ped)
                DRAW_DEBUG_LINE(<<TempCoords.x, TempCoords.y, TempCoords.z+2>>, <<TempCoords.x, TempCoords.y, TempCoords.z+2.25>> )
            ENDIF
        ENDIF
        
        IF not DOES_ENTITY_EXIST(lvehicles.vehicle)
            DRAW_DEBUG_LINE( gvehiclecoords, <<gvehiclecoords.x, gvehiclecoords.y, gvehiclecoords.z+1>>)
        ELSE
            IF IS_VEHICLE_DRIVEABLE (lvehicles.vehicle)
                TempVehicleCoords = GET_ENTITY_COORDS(lvehicles.Vehicle)
                DRAW_DEBUG_LINE(<<TempVehicleCoords.x, TempVehicleCoords.y, TempVehicleCoords.z+2>>, <<TempVehicleCoords.x, TempVehicleCoords.y, TempVehicleCoords.z+2.25>> )
            ENDIF
        ENDIF
ENDPROC

//PURPOSE: Draws the peds coords and target destination.
PROC DRAW_DEBUG_INFO (  )
    
        float temp_x
        float temp_y 
        
        vector target_vec = DynamicTargetPos
        vector start_vec = gPedcoords
    
        // destinations
        DRAW_DEBUG_LINE(<<start_vec.x, start_vec.y,  (start_vec.z + 1)>> ,  <<start_vec.x, start_vec.y,  (start_vec.z - 1)>>  )
        DRAW_DEBUG_LINE(<<target_vec.x, target_vec.y,  (target_vec.z + 1)>> ,  <<target_vec.x, target_vec.y,  (target_vec.z - 1)>>  )
            
        //angle 
        temp_x = (1 * sin  (DynamicTargetHeading ))
         temp_y  = (1 * cos (DynamicTargetHeading ))
        DRAW_DEBUG_LINE(<<target_vec.x, target_vec.y,  (target_vec.z + 1)>>,   <<(target_vec.x - temp_x), (target_vec.y + temp_y),  (target_vec.z + 1)>>  )
        
        temp_x = (1 * sin  (gPedheading ))
        temp_y   = (1 * cos (gPedheading ))
        DRAW_DEBUG_LINE(<<start_vec.x, start_vec.y,  (start_vec.z + 1)>>,   <<(start_vec.x - temp_x), (start_vec.y + temp_y),  (start_vec.z + 1)>>  )
        
ENDPROC

//PURPOSE: Clears an area around the 
PROC SETUP_AREA_FOR_MISSION (VECTOR Coords)
    CLEAR_AREA (COORDS, 500, TRUE )
ENDPROC

//PURPOSE: Sets the scenario mode into debug
PROC SET_TO_DEBUG ()
        IF IS_BUTTON_JUST_PRESSED (PAD1, LEFTSHOCK )
            IF (gRun_debuggig)
                IF NOT IS_XML_MENU_ON_SCREEN ()
                    HELP_TEXT_STATE = HTF_SCENARIO_SELECTED_NOT_STARTED
                ENDIF   
                    gRun_debuggig = FALSE
                ELSE
                    gRun_debuggig = TRUE
                ENDIF
        ENDIF       

ENDPROC

//PUPROSE: Allows the entities in a sceanrio to be adjusted when in debug mode
PROC USE_JOYSTICK_TO_POSITION_PEDS_AND_VEHICLES  (int NumberOfPeds, int  NumberOfVehicles, Int NumberOfRoutes )
    int iLeft_x, iright_x, ileft_y, iright_y
        
        SWITCH  DebugStatus
        
            CASE DebugRoute
                
                    IF NOT IS_XML_MENU_ON_SCREEN ()
                        HELP_TEXT_STATE = HTF_DEBUG_ROUTES
                    ENDIF
                    GET_POSITION_OF_ANALOGUE_STICKS (PAD1, ileft_x, ileft_y, iright_x, iright_y )
                        
                    IF (ileft_x >= -128) AND (ileft_x < 0)
                        gRoutePoint.x -= 0.25
                    ENDIF
                    
                    IF (ileft_x <= 128) AND (ileft_x > 0)
                        gRoutePoint.x += 0.25
                    ENDIF
                    
                    IF (ileft_y >= -128) AND (ileft_y < 0)
                        gRoutePoint.y += 0.25
                    ENDIF
                    
                    IF (ileft_y <= 128) AND (ileft_y > 0)
                        gRoutePoint.y -= 0.25
                    ENDIf
            
                    IF (iright_y >= -128) AND (iright_y < 0)
                        gRoutePoint.z += 0.25
                    ENDIF
                    
                    IF (iright_y <= 128) AND (iright_y > 0)
                        gRoutePoint.z -= 0.25
                    ENDIf
                    
                    IF IS_BUTTON_just_PRESSED (PAD1, CROSS)
                        IF NOT (gRouteNode = NumberOfRoutes -1 )
                            gRouteNode += 1
                        endif
                    ENDIF
            
                    IF IS_BUTTON_just_PRESSED (PAD1, CIRCLE)
                        IF NOT (gRouteNode = 0 )
                            gRouteNode -= 1
                        endif
                    ENDIF
            
                    IF IS_BUTTON_just_PRESSED (PAD1, TRIANGLE)
                        BSetNode = TRUE
                    ENDIF
            BREAK
        
            CASE  DebugPed
                IF NOT IS_XML_MENU_ON_SCREEN ()
                    HELP_TEXT_STATE = HTF_DEBUG_PEDS
                ENDIF
                
                    GET_POSITION_OF_ANALOGUE_STICKS (PAD1, ileft_x, ileft_y, iright_x, iright_y )
                        IF (ileft_x >= -128) AND (ileft_x < 0)
                            gPedcoords.x -= 0.25
                        ENDIF
                        
                        IF (ileft_x <= 128) AND (ileft_x > 0)
                            gPedcoords.x += 0.25
                        ENDIF
                        
                        IF (ileft_y >= -128) AND (ileft_y < 0)
                            gPedcoords.y += 0.25
                        ENDIF
                        
                        IF (ileft_y <= 128) AND (ileft_y > 0)
                            gPedcoords.y -= 0.25
                        ENDIf
            
                        IF (iright_y >= -128) AND (iright_y < 0)
                            gPedcoords.z += 0.25
                        ENDIF
                        
                        IF (iright_y <= 128) AND (iright_y > 0)
                            gPedcoords.z -= 0.25
                        ENDIf
            
                        IF (iright_x >= -128) AND (iright_x < 0)
                            gPedheading += 2
                        ENDIF
                    
                        IF (iright_x <= 128) AND (iright_x > 0)
                            gPedheading -= 2
                        ENDIF
            
                        IF IS_BUTTON_just_PRESSED (PAD1, CROSS)
                            IF NOT (gTestPedsIndex = NumberOfPeds -1 )
                                gTestPedsIndex += 1
                            endif
                        ENDIF
                
                        IF IS_BUTTON_just_PRESSED (PAD1, CIRCLE)
                            IF NOT (gTestPedsIndex = 0 )
                                gTestPedsIndex -= 1
                            endif
                        ENDIF
                    
                    IF NOT IS_BUTTON_PRESSED (PAD1, LEFTSHOULDER1)
                        IF IS_BUTTON_just_PRESSED (PAD1, TRIANGLE)
                                gCreatePed = TRUE
                        ENDIF
                    ELSE
                        IF IS_BUTTON_just_PRESSED (PAD1, TRIANGLE)
                            gdeletePed = TRUE
                        ENDIF
                    ENDIF 
                
            BREAK               
            
            CASE    DebugVehicle        
                HELP_TEXT_STATE = HTF_DEBUG_VEHICLES
                GET_POSITION_OF_ANALOGUE_STICKS (PAD1, ileft_x, ileft_y, iright_x, iright_y )
                    IF (ileft_x >= -128) AND (ileft_x < 0)
                        gVehiclecoords.x -= 0.25
                    ENDIF
                    
                    IF (ileft_x <= 128) AND (ileft_x > 0)
                        gVehiclecoords.x += 0.25
                    ENDIF
                    
                    IF (ileft_y >= -128) AND (ileft_y < 0)
                        gVehiclecoords.y += 0.25
                    ENDIF
                    
                    IF (ileft_y <= 128) AND (ileft_y > 0)
                        gVehiclecoords.y -= 0.25
                    ENDIf
                    
                    IF (iright_y >= -128) AND (iright_y < 0)
                        gVehiclecoords.z += 0.25
                    ENDIF
                    
                    IF (iright_y <= 128) AND (iright_y > 0)
                        gVehiclecoords.z -= 0.25
                    ENDIf
            
                    IF (iright_x >= -128) AND (iright_x < 0)
                        gVehicleheading -= 2
                    ENDIF
                    
                    IF (iright_x <= 128) AND (iright_x > 0)
                        gVehicleheading += 2
                    ENDIF
        
            
                    IF IS_BUTTON_just_PRESSED (PAD1, CROSS)
                        IF NOT (gTestvehicleIndex = NumberOfVehicles - 1 )
                            gTestvehicleIndex += 1
                        endif
                    ENDIF
            
                    IF IS_BUTTON_just_PRESSED (PAD1, CIRCLE)
                        IF NOT (gTestvehicleIndex = 0 )
                            gTestvehicleIndex -= 1
                        endif
                    ENDIF
            
                
                IF NOT IS_BUTTON_PRESSED (PAD1, LEFTSHOULDER1)
                    IF IS_BUTTON_just_PRESSED (PAD1, TRIANGLE)
                            gCreateVehicle= TRUE
                    ENDIF
                ELSE
                    IF IS_BUTTON_just_PRESSED (PAD1, TRIANGLE)
                        gDeleteVehicle = TRUE
                    ENDIF
                ENDIF 
        BREAK
        
        CASE DebugDefensiveArea
            IF NOT IS_XML_MENU_ON_SCREEN ()
                HELP_TEXT_STATE = HTF_DEBUG_DEFENSIVE_AREAS
            ENDIF
            GET_POSITION_OF_ANALOGUE_STICKS (PAD1, ileft_x, ileft_y, iright_x, iright_y )
                IF (ileft_x >= -128) AND (ileft_x < 0)
                        gPedDefACoords.x -= 0.25
                    ENDIF
                    
                    IF (ileft_x <= 128) AND (ileft_x > 0)
                        gPedDefACoords.x += 0.25
                    ENDIF
                    
                    IF (ileft_y >= -128) AND (ileft_y < 0)
                        gPedDefACoords.y += 0.25
                    ENDIF
                    
                    IF (ileft_y <= 128) AND (ileft_y > 0)
                        gPedDefACoords.y -= 0.25
                    ENDIf
            
                    IF (iright_x >= -128) AND (iright_x < 0)
                        gPedDefARadius -= 0.25
                    ENDIF
                    
                    IF (iright_x <= 128) AND (iright_x > 0)
                        gPedDefARadius += 0.25
                    ENDIF
            
                    IF (iright_y >= -128) AND (iright_y < 0)
                        gPedDefACoords.z += 0.25
                    ENDIF
                    
                    IF (iright_y <= 128) AND (iright_y > 0)
                        gPedDefACoords.z -= 0.25
                    ENDIf
                    
                    IF IS_BUTTON_just_PRESSED (PAD1, CROSS)
                        IF NOT (gTestPedsIndex = NumberOfPeds -1 )
                            gTestPedsIndex += 1
                        endif
                    ENDIF
            
                    IF IS_BUTTON_just_PRESSED (PAD1, CIRCLE)
                        IF NOT (gTestPedsIndex = 0 )
                            gTestPedsIndex -= 1
                        endif
                    ENDIF
                    
                IF NOT IS_BUTTON_PRESSED (PAD1, LEFTSHOULDER1)
                    IF IS_BUTTON_just_PRESSED (PAD1, TRIANGLE)
                            gPedDefAActive = TRUE
                    ENDIF
                ELSE
                    IF IS_BUTTON_just_PRESSED (PAD1, TRIANGLE)
                        gPedDefAActive = FALSE
                    ENDIF
                ENDIF 
        BREAK
        
        
        CASE DebugTarGetDestination
            IF NOT IS_XML_MENU_ON_SCREEN ()
                HELP_TEXT_STATE = HTF_DEBUG_DA_TARGET
            ENDIF
        
            GET_POSITION_OF_ANALOGUE_STICKS (PAD1, ileft_x, ileft_y, iright_x, iright_y )
                IF (ileft_x >= -128) AND (ileft_x < 0)
                        DynamicTargetPos.x -= 0.25
                    ENDIF
                    
                    IF (ileft_x <= 128) AND (ileft_x > 0)
                        DynamicTargetPos.x += 0.25
                    ENDIF
                    
                    IF (ileft_y >= -128) AND (ileft_y < 0)
                        DynamicTargetPos.y += 0.25
                    ENDIF
                    
                    IF (ileft_y <= 128) AND (ileft_y > 0)
                        DynamicTargetPos.y -= 0.25
                    ENDIf
            
                    IF (iright_x >= -128) AND (iright_x < 0)
                        DynamicTargetHeading -= 1
                    ENDIF
                    
                    IF (iright_x <= 128) AND (iright_x > 0)
                        DynamicTargetHeading += 1
                    ENDIF
            
                    IF (iright_y >= -128) AND (iright_y < 0)
                        DynamicTargetPos.z += 0.25
                    ENDIF
                    
                    IF (iright_y <= 128) AND (iright_y > 0)
                        DynamicTargetPos.z -= 0.25
                    ENDIf
                    
                    IF IS_BUTTON_just_PRESSED (PAD1, CROSS)
                        IF NOT (gTestPedsIndex = NumberOfPeds -1 )
                            gTestPedsIndex += 1
                        endif
                    ENDIF
            
                    IF IS_BUTTON_just_PRESSED (PAD1, CIRCLE)
                        IF NOT (gTestPedsIndex = 0 )
                            gTestPedsIndex -= 1
                        endif
                    ENDIF
        BREAK
        
    ENDSWITCH   
        
ENDPROC



//Peds

//Peds Combat skills
//PURPOSE: Sets the ped combat movement ability. 
PROC ChangePedCombatMovement (peds_struct &lpeds )
    IF NOT IS_PED_INJURED (lpeds.Ped)
        IF (lpeds.pedCombatMove <> -1)
            SET_PED_COMBAT_MOVEMENT (lpeds.Ped, INT_TO_ENUM(COMBAT_MOVEMENT, lpeds.pedCombatMove ) )
        ENDIF
    ENDIF
ENDPROC

//PURPOSE: Sets the ped combat range ability. 
PROC ChangePedCombatRange  (peds_struct &lpeds )
    IF NOT IS_PED_INJURED (lpeds.Ped)
        IF (lpeds.pedcombatrange <> -1)
            SET_PED_COMBAT_RANGE (lpeds.Ped, INT_TO_ENUM(COMBAT_RANGE, lpeds.pedcombatrange  ))
        ENDIF
    ENDIF
ENDPROC

//PURPOSE: Sets the ped combat ability. 
PROC ChangePedCombatAbility (peds_struct &lpeds ) 
    IF NOT IS_PED_INJURED (lpeds.Ped)
        IF (lpeds.pedcombatability <> -1)
            SET_PED_COMBAT_ABILITY (lpeds.Ped,INT_TO_ENUM(COMBAT_ABILITY_LEVEL, lpeds.pedcombatability ) )
        ENDIF
    ENDIF
ENDPROC

PROC SET_COMBAT_ABILITY (peds_struct &lpeds)
    IF NOT IS_PED_INJURED (lpeds.Ped)
        lpeds.pedcombatability = gCombatAbility
    ENDIF   
ENDPROC

PROC SET_COMBAT_Range (peds_struct &lpeds)
    IF NOT IS_PED_INJURED (lpeds.Ped)
        lpeds.pedcombatrange = gCombatRange
    ENDIF
ENDPROC

PROC SET_COMBAT_movement (peds_struct &lpeds)
    IF NOT IS_PED_INJURED (lpeds.Ped)
        lpeds.pedCombatMove = gCombatMove
    ENDIF
ENDPROC

PROC set_combat_attributes  (peds_struct &lpeds)
    IF NOT IS_PED_INJURED (lpeds.Ped)
        SWITCH gCombatAttributes
            CASE 0
                lpeds.combatcover = CA_USE_COVER 
                lpeds.bcombatcover  = gActivateCombatAttributes

            BREAK
            
            CASE 1
                lpeds.combatUseVehicle = CA_USE_VEHICLE
                lpeds.bcombatUseVehicle  = gActivateCombatAttributes
            BREAK
            
            CASE 2
                lpeds.Combatdodrivebys = CA_DO_DRIVEBYS
                lpeds.bCombatdodrivebys  = gActivateCombatAttributes
            BREAK
            
            CASE 3
                lpeds.combatleavevehicle = CA_LEAVE_VEHICLES
                lpeds.bcombatleavevehicle  = gActivateCombatAttributes
            BREAK
        ENDSWITCH
    ENDIF
ENDPROC

PROC ChangePedCombatAttribute (peds_struct &lpeds)
    int index
    IF NOT IS_PED_INJURED  (lpeds.Ped)
        for index = 0 to  MAX_NUMBER_OF_COMBAT_ATTRIBUTES -1
            Switch index
                CASE 0
                    IF (lpeds.combatcover <> CA_INVALID)
                        
                        SET_PED_COMBAT_ATTRIBUTES (lpeds.Ped , lpeds.combatcover  ,  lpeds.bcombatcover  )
                    ENDIF
                BREAK
                CASE 1
                    IF (lpeds.combatUseVehicle  <> CA_INVALID)
                        SET_PED_COMBAT_ATTRIBUTES (lpeds.Ped , lpeds.combatUseVehicle , lpeds.bcombatUseVehicle )
                    ENDIF
                BREAK
                CASE 2
                    IF (lpeds.Combatdodrivebys  <> CA_INVALID)
                        SET_PED_COMBAT_ATTRIBUTES (lpeds.Ped , lpeds.Combatdodrivebys , lpeds.bCombatdodrivebys )
                    ENDIF
                BREAK
                CASE 3
                    IF (lpeds.combatleavevehicle  <> CA_INVALID)
                        SET_PED_COMBAT_ATTRIBUTES (lpeds.Ped , lpeds.combatleavevehicle , lpeds.bcombatleavevehicle )
                    ENDIF
                BREAK
                CASE 4
                    IF (lpeds.combativestevents  <> CA_INVALID)
                        SET_PED_COMBAT_ATTRIBUTES (lpeds.Ped , lpeds.combativestevents , lpeds.bcombativestevents )
                    ENDIF
                BREAK
                CASE 5
                    IF (lpeds.combativestpeds  <> CA_INVALID)
                        SET_PED_COMBAT_ATTRIBUTES (lpeds.Ped , lpeds.combativestpeds , lpeds.bcombativestpeds )
                    ENDIF
                BREAK
            ENDSWITCH
        ENDFOR
    ENDIF
ENDPROC

PROC ALTER_COMBAT_STATS (peds_struct &lpeds )
    ChangePedCombatMovement (lpeds )
    ChangePedCombatRange  (lpeds )
    ChangePedCombatAbility (lpeds ) 
    ChangePedCombatAttribute (lpeds ) 
ENDPROC

PROC SET_PED_DEFENSIVE_AREAS(peds_struct &lpeds )
    IF NOT IS_PED_INJURED (lpeds.ped)
        IF lpeds.PedActiveDefArea
            SET_PED_SPHERE_DEFENSIVE_AREA (lpeds.Ped, lpeds.PedDefCoord, lpeds.PedDefArea)
        ENDIF   
    ENDIF   
ENDPROC

PROC RESET_PED_STRUCT (peds_struct &lpeds)
        lpeds.ped = NULL
        lpeds.PedsCoords = <<0.0, 0.0, 0.0>>
        lpeds.PedHeading = 0.0
        lpeds.PedsWeapon = WEAPONTYPE_UNARMED
        lpeds.PedModel = DUMMY_MODEL_FOR_SCRIPT
        lpeds.pedsstringweapon = "WEAPONTYPE_UNARMED"
        lpeds.PedWeaponModel = GET_WEAPONTYPE_MODEL(WEAPONTYPE_PISTOL)
        lpeds.Pedrelgrp = -1
        lpeds.PedDefCoord = <<0.0, 0.0, 0.0>>
        lpeds.PedDefArea = 0.0
        lpeds.PedActiveDefArea = FALSE
        lpeds.pedCombatMove = -1
        lpeds.pedcombatrange = -1
        lpeds.pedcombatability =-1
        lpeds.combatcover = CA_INVALID
        lpeds.bcombatcover = FALSE
        lpeds.combatUseVehicle = CA_INVALID
        lpeds.bcombatUseVehicle = FALSE
        lpeds.Combatdodrivebys = CA_INVALID
        lpeds.bCombatdodrivebys = FALSE
        lpeds.combatleavevehicle = CA_INVALID
        lpeds.bcombatleavevehicle = FALSE
        lpeds.combativestevents = CA_INVALID
        lpeds.bcombativestevents = FALSE
        lpeds.combativestpeds = CA_INVALID
        lpeds.bcombativestpeds = FALSE
ENDPROC

PROC RESET_CAM_STRUCT (StCameraData &Camst)
    Camst.cam_pos = <<1.0, 1.0, 1.0>>
    Camst.cam_rot = <<0.0, 0.0, 0.0>>
    Camst.cam_fov = 65.0
    Camst.bActivateCam = FALSE
ENDPROC

PROC RESET_OBJECT_STRUCT (object_struct &Object)
    Object.Object = NULL
    Object.ObjectModel = DUMMY_MODEL_FOR_SCRIPT
    Object.ObjectCoords = <<0.0, 0.0, 0.0>>
    Object.ObjectRotation = <<0.0, 0.0, 0.0>>
ENDPROC

PROC RESET_SHAPETEST_STRUCT(ShapeTestStruct& lShapeTest)
	lShapeTest.ShapeTest = NULL
	lShapeTest.eShapeTestType = SHAPETEST_INVALID
	lShapeTest.bRequested = FALSE
	lShapeTest.bResultIsNew = FALSE
	lShapeTest.vStartPos = <<0.0, 0.0, 0.0>>
	lShapeTest.vEndPos = <<0.0, 0.0, 0.0>>
	lShapeTest.vDims = <<0.0, 0.0, 0.0>>
	lShapeTest.vEulerAngles = <<0.0, 0.0, 0.0>>
	lShapeTest.eEulerOrder = EULER_XYZ
	lShapeTest.fRadius = 0.0
	lShapeTest.vHitPos = <<0.0, 0.0, 0.0>>
	lShapeTest.vHitNormal = <<0.0, 0.0, 0.0>>
	lShapeTest.bIsHit = FALSE
ENDPROC

PROC PedModelSelection (peds_struct &lpeds, MODEL_NAMES Model)
    IF NOT DOES_ENTITY_EXIST(lpeds.ped)
        SWITCH PedIdentifier
            CASE 0
                lpeds.PedModel = Model
            BREAK
            CASE 1 
                lpeds.PedModel = Model
            BREAK
        ENDSWITCH
        lpeds.PedsCoords = gPedcoords
    ENDIF
ENDPROC

PROC INITIALISE_PED_DATA (peds_struct &lpeds[])
    int index = 0
    int array = COUNT_OF (lpeds) 
    
    FOR index = 0 to array -1
        RESET_PED_STRUCT (lpeds[index])
    ENDFOR
ENDPROC

PROC INITIALISE_CAM_DATA (StCameraData &Camst[])
    int index = 0
    int array = COUNT_OF (Camst) 
    
    FOR index = 0 to array -1
        RESET_CAM_STRUCT (Camst[index])
    ENDFOR
ENDPROC

PROC INITIALISE_OBJECT_DATA (object_struct &lObjects[])
    int index = 0
    int array = COUNT_OF (lObjects) 
    
    FOR index = 0 to array -1
        RESET_OBJECT_STRUCT (lObjects[index])
    ENDFOR
ENDPROC

PROC INITIALISE_SHAPETESTS (ShapeTestStruct &lShapeTests[])
    int index = 0
    int array = COUNT_OF (lShapeTests) 
    
    FOR index = 0 to array -1
        RESET_SHAPETEST_STRUCT (lShapeTests[index])
    ENDFOR
ENDPROC

//PURPOSE: Checks if a given task is complete
FUNC BOOL IS_TASK_COMPLETE (PED_INDEX TestPed, SCRIPT_TASK_NAME current_task)
    SCRIPTTASKSTATUS CurrentTaskStatus
        IF NOT IS_PED_INJURED ( TestPed)
            CurrentTaskStatus = GET_SCRIPT_TASK_STATUS (TestPed, current_task )
        ENDIF
        
        IF CurrentTaskStatus = FINISHED_TASK
            RETURN TRUE
        ENDIF
        
        RETURN FALSE
ENDFUNC

//PURPOSE: Checks if a given task is complete
PROC PedPerformsSequence (PED_INDEX ped, SEQUENCE_INDEX seq)
    IF NOT IS_PED_INJURED (ped)
        TASK_PERFORM_SEQUENCE (ped, seq)
        CLEAR_SEQUENCE_TASK (seq)
    ENDIF
ENDPROC

FUNC BOOL IS_TASK_BEING_PERFORMEND (PED_INDEX TestPed, SCRIPT_TASK_NAME current_task)
    SCRIPTTASKSTATUS CurrentTaskStatus
        IF NOT IS_PED_INJURED ( TestPed)
            CurrentTaskStatus = GET_SCRIPT_TASK_STATUS (TestPed, current_task )
        ENDIF
        
        IF CurrentTaskStatus = PERFORMING_TASK
            RETURN TRUE
        ENDIF
        
        RETURN FALSE
ENDFUNC

ENUM SingleTaskFlags
    StartSingleTask,
    PerformSingleTask, 
    CompleteSimpleTask
ENDENUM
SingleTaskFlags SingleTaskStatus = StartSingleTask

proc Is_Test_Sequence_Task_Complete (PED_INDEX ped )
        
        SWITCH SingleTaskStatus
            CASE StartSingleTask

                SingleTaskStatus = PerformSingleTask
        
            BREAK
                
            CASE PerformSingleTask
            
                IF IS_TASK_BEING_PERFORMEND (ped, SCRIPT_TASK_PERFORM_SEQUENCE )
                    SingleTaskStatus = CompleteSimpleTask
                ENDIF
            BREAK
            
            CASE CompleteSimpleTask
                IF IS_TASK_COMPLETE (ped, SCRIPT_TASK_PERFORM_SEQUENCE )
                
                ENDIF
            BREAK
        ENDSWITCH
        
ENDPROC

PROC RESET_VEHICLE_STRUCT (vehicle_struct &lvehicle)
    lvehicle.vehicle = NULL
    lvehicle.VehicleModel = DUMMY_MODEL_FOR_SCRIPT
    lvehicle.VehicleCoords = <<0.0,0.0, 0.0>>
    lvehicle.VehicleHeading = 0.0
ENDPROC


PROC INITIALISE_VEHICLE_DATA (vehicle_struct &lvehicle[])
    int index = 0
    int array_size = COUNT_OF(lvehicle)
    FOR index = 0 to array_size-1
        RESET_vehicle_STRUCT (lvehicle[index])
    ENDFOR
ENDPROC

// Functions End: VEHICLE

FUNC MODEL_NAMES VehicleModelSelection ( )
    MODEL_NAMES current_model

        SWITCH Vehicledentifier
            CASE 0
             current_model =  Burrito  
            BREAK
            CASE 1 
                current_model= Buzzard
            BREAK
            
            CASE 2
                current_model= Emperor 
            BREAK
            
            CASE 3
                current_model= FBI  
            BREAK
            
            CASE 4 
                current_model= MULE
            BREAK
            
            CASE 5
                current_model= FIRETRUK
            BREAK
            
            CASE 6
                current_model= FORKLIFT
            BREAK
            
            CASE 7
                current_model= INFERNUS
            BREAK
            
            CASE 8
                current_model= POLICE
            BREAK
            
            CASE 9
                current_model= SANCHEZ
            BREAK
            
            CASE 10
                current_model= SQUALO
            BREAK
            
            CASE 11
                current_model= SULTAN
            BREAK
            
        ENDSWITCH
 
    RETURN current_model
    
ENDFUNC

PROC CREATE_VEHICLE_ENTITY (vehicle_Struct &lVehicles)
    IF NOT DOES_ENTITY_EXIST(lVehicles.vehicle)
        IF (lVehicles.VehicleModel <> DUMMY_MODEL_FOR_SCRIPT)       
            REQUEST_MODEL(lVehicles.vehiclemodel )
            WHILE NOT HAS_MODEL_LOADED(lVehicles.vehiclemodel)
                    WAIT (0)
            ENDWHILE
            lVehicles.vehicle = CREATE_VEHICLE  (lvehicles.Vehiclemodel, lvehicles.VehicleCoords, lvehicles.VehicleHeading  )
            SET_MODEL_AS_NO_LONGER_NEEDED (lVehicles.vehiclemodel)
        ENDIF
    ENDIF
ENDPROC

PROC DELETE_SCENARIO_VEHICLE (vehicle_struct &lVehicle )
    IF DOES_ENTITY_EXIST(lvehicle.Vehicle)
        DELETE_VEHICLE(lvehicle.Vehicle)
        RESET_VEHICLE_STRUCT (lvehicle)
    ENDIF
ENDPROC

FUNC WEAPON_TYPE WeaponModelSelection (peds_Struct &lpeds) 
    WEAPON_TYPE WeaponTypes
    
    SWITCH WeaponIdentifier
        CASE 0 
            lpeds.PedWeaponModel = GET_WEAPONTYPE_MODEL(WEAPONTYPE_PISTOL)
            lpeds.PedsWeapon = WEAPONTYPE_UNARMED
            lpeds.pedsstringweapon = "WEAPONTYPE_UNARMED"
        BREAK
        
        CASE 1
            lpeds.PedsWeapon =  WEAPONTYPE_PISTOL
            lpeds.PedWeaponModel  = GET_WEAPONTYPE_MODEL(lpeds.PedsWeapon)
            lpeds.pedsstringweapon = "WEAPONTYPE_PISTOL"
        BREAK
        CASE 2
            lpeds.PedWeaponModel  =  GET_WEAPONTYPE_MODEL(WEAPONTYPE_ASSAULTRIFLE)
            lpeds.PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            lpeds.pedsstringweapon = "WEAPONTYPE_ASSAULTRIFLE"
        BREAK
        CASE 3 
            lpeds.PedWeaponModel  = GET_WEAPONTYPE_MODEL(WEAPONTYPE_ASSAULTRIFLE)  
            lpeds.PedsWeapon = WEAPONTYPE_ASSAULTRIFLE
            lpeds.pedsstringweapon =  "WEAPONTYPE_ASSAULTRIFLE"
        BREAK
        
    ENDSWITCH
    
    RETURN WeaponTypes
    
ENDFUNC

PROC CREATE_OBJECT_ENTITY (object_struct &lObjects)
    IF NOT DOES_ENTITY_EXIST( lObjects.Object )
        IF (lObjects.ObjectModel <> DUMMY_MODEL_FOR_SCRIPT)
            REQUEST_MODEL(lObjects.ObjectModel)
            WHILE NOT HAS_MODEL_LOADED(lObjects.ObjectModel)
                WAIT (0)
            ENDWHILE
            lObjects.Object = CREATE_OBJECT (lObjects.ObjectModel, lObjects.ObjectCoords)
            SET_MODEL_AS_NO_LONGER_NEEDED (lObjects.ObjectModel)
        ENDIF
    ENDIF
ENDPROC

PROC START_SHAPETEST (ShapeTestStruct& lShapeTest, ENTITY_INDEX ExcludeEntityIndex)

	SWITCH lShapeTest.eShapeTestType

		CASE SHAPETEST_INVALID

		BREAK
		
		CASE SHAPETEST_LOSPROBE
			lShapeTest.ShapeTest = START_SHAPE_TEST_LOS_PROBE(lShapeTest.vStartPos, lShapeTest.vEndPos, SCRIPT_INCLUDE_ALL, ExcludeEntityIndex)
		BREAK
	
		// Since nobody was using async sphere tests we disabled them
		//CASE SHAPETEST_SPHERE
			//lShapeTest.ShapeTest = START_SHAPE_TEST_SPHERE(lShapeTest.vStartPos, lShapeTest.fRadius, SCRIPT_INCLUDE_ALL)
		//BREAK
		
		CASE SHAPETEST_BOUND
			lShapeTest.ShapeTest = START_SHAPE_TEST_BOUND(lShapeTest.EntityIndex, SCRIPT_INCLUDE_ALL)
		BREAK
		
		CASE SHAPETEST_BOUNDINGBOX
			lShapeTest.ShapeTest = START_SHAPE_TEST_BOUNDING_BOX(lShapeTest.EntityIndex, SCRIPT_INCLUDE_ALL)
		BREAK

		CASE SHAPETEST_BOX
			lShapeTest.ShapeTest = START_SHAPE_TEST_BOX(lShapeTest.vStartPos, lShapeTest.vDims, lShapeTest.vEulerAngles, lShapeTest.eEulerOrder, SCRIPT_INCLUDE_ALL)
		BREAK
		
		CASE SHAPETEST_CAPSULE
			lShapeTest.ShapeTest = START_SHAPE_TEST_CAPSULE(lShapeTest.vStartPos, lShapeTest.vEndPos, lShapeTest.fRadius, SCRIPT_INCLUDE_ALL)
		BREAK
	
	ENDSWITCH
	
	lShapeTest.bRequested = (lShapeTest.ShapeTest <> NULL)
	
ENDPROC

//Keep renewing the shape test (call this every frame)
PROC UPDATE_PERSISTENT_SHAPETEST (ShapeTestStruct& lShapeTest, ENTITY_INDEX ExcludeEntityIndex)
	
	ENTITY_INDEX hitEntity
	INT 		 iHit 		// int as bool
	
	SHAPETEST_STATUS Status = GET_SHAPE_TEST_RESULT(lShapeTest.ShapeTest, iHit, lShapeTest.vHitPos, lShapeTest.vHitNormal, hitEntity)
	
	lShapeTest.bResultIsNew = (SHAPETEST_STATUS_RESULTS_READY = Status)
	
	//Convert the int as bool to a bool
	IF lShapeTest.bResultIsNew
		IF 0 = iHit
			lShapeTest.bIsHit = FALSE
		ELSE
			lShapeTest.bIsHit = TRUE
		ENDIF
	ENDIF
	
	//Try to keep all shape tests requested at all times
	IF Status <> SHAPETEST_STATUS_RESULTS_NOTREADY //Result was just returned or request was nonexistent
		START_SHAPETEST (lShapeTest, ExcludeEntityIndex)
	ENDIF

ENDPROC

PROC SWAP_PED_WEAPONS (peds_struct &lpeds)
        
            IF NOT IS_PED_INJURED (lPeds.ped)
                REMOVE_ALL_PED_WEAPONS (lpeds.ped ) 
                
                IF NOT (lpeds.PedsWeapon = WEAPONTYPE_UNARMED)  //the peds current weapon type is not unarmed
                    REQUEST_MODEL(lpeds.PedWeaponModel)
                    WHILE NOT HAS_MODEL_LOADED(lpeds.PedWeaponModel)
                        WAIT (0)
                    ENDWHILE    
            
                    IF NOT IS_PED_INJURED (lPeds.ped)
                        GIVE_WEAPON_TO_PED (lPeds.ped,  lpeds.PedsWeapon, 9999, TRUE)
                    ENDIF
                    
                    SET_MODEL_AS_NO_LONGER_NEEDED (lpeds.PedWeaponModel)
                ENDIF
            ENDIF

ENDPROC

PROC ADD_RELATIONSHIP_GROUP_TO_STRUCT (peds_struct  &lpeds)
            IF DOES_ENTITY_EXIST(lpeds.ped)
                IF NOT IS_PED_INJURED (lpeds.Ped )
                    IF lpeds.Pedrelgrp = -1
                        lpeds.Pedrelgrp = gpedrelgrp
                    ENDIF
					
					IF NOT gCreatedRelationshipGroups
						ADD_RELATIONSHIP_GROUP("RELGROUP_MISSION1", RELGROUP_MISSION1)
						ADD_RELATIONSHIP_GROUP("RELGROUP_MISSION2", RELGROUP_MISSION2)
//						ADD_RELATIONSHIP_GROUP("RELGROUP_MISSION3", RELGROUP_MISSION3)
//						ADD_RELATIONSHIP_GROUP("RELGROUP_MISSION4", RELGROUP_MISSION4)
						
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUP_MISSION1, RELGROUP_MISSION2)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUP_MISSION2, RELGROUP_MISSION1)

						gCreatedRelationshipGroups = true
					ENDIF
                   
                    IF (lpeds.Pedrelgrp = 1)
                        SET_PED_RELATIONSHIP_GROUP_HASH ( lpeds.ped, RELGROUP_MISSION1 )
                    ENDIF
                    
                    IF (lpeds.Pedrelgrp = 2)
                        SET_PED_RELATIONSHIP_GROUP_HASH ( lpeds.ped, RELGROUP_MISSION2 )
                    ENDIF
                    
//                    IF (lpeds.Pedrelgrp = 3)
//                        SET_PED_RELATIONSHIP_GROUP_HASH ( lpeds.ped, RELGROUP_MISSION3 )
//                    ENDIF
//                    
//                    IF (lpeds.Pedrelgrp = 4)
//                        SET_PED_RELATIONSHIP_GROUP_HASH ( lpeds.ped, RELGROUP_MISSION4 )
//                    ENDIF
                
                
                ENDIF
            ENDIF
ENDPROC

PROC CREATE_PED_ENTITY(peds_struct  &lpeds )
        
        
        IF NOT DOES_ENTITY_EXIST(lPeds.ped)
            
            // the ped is being created by a widget
            IF (lpeds.PedModel <> DUMMY_MODEL_FOR_SCRIPT)       
                REQUEST_MODEL(lpeds.PedModel )
                WHILE NOT HAS_MODEL_LOADED(lpeds.PedModel)
                    WAIT (0)
                ENDWHILE                        
                lpeds.ped = CREATE_PED  (PEDTYPE_MISSION,  lpeds.PedModel, lpeds.PedsCoords, lpeds.PedHeading )
                SET_MODEL_AS_NO_LONGER_NEEDED (lpeds.PedModel)
            ENDIF   
        ENDIF

ENDPROC

PROC DELETE_SCENARIO_PED (peds_struct &lpeds)
    IF DOES_ENTITY_EXIST(lpeds.Ped)
        DELETE_PED(lpeds.Ped)
        reset_ped_struct (lpeds)
    ENDIF   
ENDPROC

//PURPOSE: Positions a defwnsive area around a ped
PROC PositionDefensiveArea (peds_struct  &lpeds)
    
    IF (lpeds.ped <> DefaultPedIndex)
        IF NOT IS_PED_INJURED (lpeds.ped)
            gPedcoords = GET_ENTITY_COORDS(lpeds.ped)
            gPedheading= GET_ENTITY_HEADING(lpeds.ped)
            gPedcoords.z = gPedcoords.z - 1.0
            DefaultPedIndex = lpeds.ped 
        ENDIF   
        
        IF  lpeds.PedActiveDefArea
                gPedDefACoords = lpeds.PedDefCoord 
                gPedDefARadius = lpeds.PedDefArea
                gPedDefAActive = TRUE
        ELSE
            IF gPedDefAActive 
                gPedDefAActive = FALSE
            ENDIF
        ENDIF
        
    ENDIF
    
    IF gPedDefAActive
        lpeds.PedActiveDefArea = TRUE
        lpeds.PedDefCoord = gPedDefACoords
        lpeds.PedDefArea = gPedDefARadius
    ELSE
        IF lpeds.PedActiveDefArea
             lpeds.PedActiveDefArea = FALSE
        ENDIF
    ENDIF

        IF lpeds.PedActiveDefArea
            IF NOT IS_PED_INJURED (lpeds.Ped)
                SET_PED_SPHERE_DEFENSIVE_AREA (lpeds.Ped, lpeds.PedDefCoord, lpeds.PedDefArea)
            ENDIF   
        ELSE
            IF NOT IS_PED_INJURED (lpeds.Ped)
                REMOVE_PED_DEFENSIVE_AREA ( (lpeds.Ped))
            ENDIF   
        ENDIF   
ENDPROC


PROC PostionVehicles(vehicle_struct &lvehicle)
    IF IS_VEHICLE_DRIVEABLE (lvehicle.vehicle)
        lvehicle.VehicleCoords = gVehiclecoords
        lvehicle.VehicleHeading = gVehicleheading
        SET_ENTITY_COORDS(lvehicle.vehicle, lvehicle.VehicleCoords )
        SET_ENTITY_HEADING(lvehicle.vehicle, lvehicle.VehicleHeading)
    ENDIF
ENDPROC

PROC PositionPeds (peds_struct  &lpeds )
    IF NOT IS_PED_INJURED (lpeds.ped)
        lpeds.PedsCoords = gPedcoords
        lpeds.PedHeading = gPedheading
          SET_PED_COORDS_KEEP_VEHICLE(lpeds.ped , lpeds.PedsCoords)
        SET_ENTITY_HEADING(lpeds.ped, lpeds.PedHeading)
    ENDIF
ENDPROC

PROC SetPedsInvincible (PED_INDEX PED, bool invincible)
    IF NOT IS_PED_INJURED (PED)
        IF (invincible)
            SET_ENTITY_INVINCIBLE(PED, invincible)
        ELSE
            SET_ENTITY_INVINCIBLE(PED, invincible)
        ENDIF
    ENDIF
ENDPROC

PROC SET_PED_TO_SKI (PED_INDEX ped, BOOL ski)
    IF NOT IS_PED_INJURED (ped)
        SET_PED_SKIING (ped, ski)
    ENDIF
ENDPROC

PROC SET_PED_DROWNS (PED_INDEX ped, BOOL Drown)
    IF NOT IS_PED_INJURED (ped)
        SET_PED_DIES_IN_WATER( ped,  Drown)
    ENDIF
ENDPROC

PROC SET_SCENARIO_PEDS_INVINCIBLE (peds_struct &pedsl[], int array_size, BOOL invincible )
    INT Index = 0
        FOR index = 0 to array_size -1
            SetPedsInvincible (pedsl[index].ped, invincible)
        ENDFOR
ENDPROC

PROC Block_Peds_Temp_Events (peds_struct lpeds, BOOL block)
        IF NOT IS_PED_INJURED (lpeds.Ped)
            SET_BLOCKING_OF_NON_TEMPORARY_EVENTS (lpeds.Ped, block)
        ENDIF   
ENDPROC

PROC Set_Gang_Relationships (BOOL set_relationsips )
    IF (set_relationsips)
        
    
//      SET_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION1, RELGROUP_MISSION2 )
//        SET_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION2, RELGROUP_MISSION1 )
//      SET_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION1, RELGROUP_MISSION3 )
//       SET_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION3, RELGROUP_MISSION1 )
//     SET_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION1, RELGROUP_MISSION4 )
//        SET_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION4, RELGROUP_MISSION1 )
//        SET_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION2, RELGROUP_MISSION3 )
//        SET_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION3, RELGROUP_MISSION2 )
//        SET_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION2, RELGROUP_MISSION4 )
//        SET_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION4, RELGROUP_MISSION2 )
//        SET_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION3, RELGROUP_MISSION4 )
//        SET_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION4, RELGROUP_MISSION3 )
    ELSE
//        CLEAR_RELATIONSHIP_BETWEEN_REL_GROUPS  ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION1, RELGROUP_MISSION2 )
//        CLEAR_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION2, RELGROUP_MISSION1 )
//        CLEAR_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION1, RELGROUP_MISSION3 )
//        CLEAR_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION3, RELGROUP_MISSION1 )
//        CLEAR_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION1, RELGROUP_MISSION4 )
//        CLEAR_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION4, RELGROUP_MISSION1 )
//        CLEAR_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION2, RELGROUP_MISSION3 )
//        CLEAR_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION3, RELGROUP_MISSION2 )
//        CLEAR_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION2, RELGROUP_MISSION4 )
//        CLEAR_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION4, RELGROUP_MISSION2 )
//        CLEAR_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION3, RELGROUP_MISSION4 )
//        CLEAR_RELATIONSHIP_BETWEEN_REL_GROUPS ( ACQUAINTANCE_TYPE_PED_HATE,  RELGROUP_MISSION4, RELGROUP_MISSION3 )     
    ENDIF
ENDPROC


PROC GiveBlipsToPedsGrps (peds_struct &lpeds )
    int rel_grp
    IF NOT IS_PED_INJURED (lpeds.ped)
        rel_grp = lpeds.Pedrelgrp
        
        IF rel_grp = 1
            IF NOT DOES_BLIP_EXIST (lpeds.PedBlip)
                lpeds.PedBlip = ADD_BLIP_FOR_ENTITY(lpeds.ped)
                SET_BLIP_COLOUR (lpeds.PedBlip, BLIP_COLOUR_RED)
            ENDIF
        ENDIF
        
        IF rel_grp = 2
            IF NOT DOES_BLIP_EXIST (lpeds.PedBlip)
                lpeds.PedBlip = ADD_BLIP_FOR_ENTITY(lpeds.ped)
                SET_BLIP_COLOUR (lpeds.PedBlip, BLIP_COLOUR_BLUE)
            ENDIF
        ENDIF
        
        IF rel_grp = 3
            IF NOT DOES_BLIP_EXIST (lpeds.PedBlip)
                lpeds.PedBlip = ADD_BLIP_FOR_ENTITY(lpeds.ped)
                SET_BLIP_COLOUR (lpeds.PedBlip, BLIP_COLOUR_YELLOW)
            ENDIF
        ENDIF
        
        IF rel_grp = 4
            IF NOT DOES_BLIP_EXIST (lpeds.PedBlip)
                lpeds.PedBlip = ADD_BLIP_FOR_ENTITY(lpeds.ped)
                SET_BLIP_COLOUR (lpeds.PedBlip, BLIP_COLOUR_GREEN)
            ENDIF
        ENDIF
    ENDIF
ENDPROC

PROC SETUP_PEDS_POS (peds_struct &lpeds )
    IF NOT DOES_ENTITY_EXIST(lpeds.ped)
        DefaultPedIndex = NULL
    ENDIF
    
    IF (lpeds.ped <> DefaultPedIndex)
        IF NOT IS_PED_INJURED (lpeds.ped)
            gPedcoords = GET_ENTITY_COORDS(lpeds.ped)
            gPedheading= GET_ENTITY_HEADING(lpeds.ped)
            gPedcoords.z = gPedcoords.z - 1.0
            DefaultPedIndex = lpeds.ped
        ENDIF   
    ENDIF

    IF NOT IS_PED_INJURED (lpeds.ped)
            positionPeds (lpeds )
    ENDIF
ENDPROC

PROC SETUP_VEHICLE_POS (vehicle_struct &lvehicle )
    IF NOT DOES_ENTITY_EXIST(lvehicle.vehicle)
        DefaultVehicleIndex = NULL
    ENDIF
    IF (lvehicle.vehicle <> DefaultVehicleIndex)
        IF IS_VEHICLE_DRIVEABLE (lvehicle.vehicle)
            gVehiclecoords = GET_ENTITY_COORDS(lvehicle.vehicle)
            gVehicleheading = GET_ENTITY_HEADING(lvehicle.vehicle)
            DefaultVehicleIndex = lvehicle.vehicle
                IF DOES_BLIP_EXIST (VehicleBlip )
                    REMOVE_BLIP (VehicleBlip)
                ENDIF
                
            VehicleBlip = ADD_BLIP_FOR_ENTITY(lvehicle.vehicle )
            
            SET_BLIP_COLOUR (VehicleBlip, BLIP_COLOUR_BLUE) 
        
        ENDIF
    ENDIF

        IF  IS_VEHICLE_DRIVEABLE (lvehicle.vehicle)
            PostionVehicles (lvehicle )
        ELSE
            IF DOES_BLIP_EXIST (VehicleBlip )
                REMOVE_BLIP (VehicleBlip)
            ENDIF   
        ENDIF
ENDPROC

//All Entities
PROC CLEAN_UP_PED_ENTITIES  (PED_INDEX ped)
    IF DOES_ENTITY_EXIST(ped)
        DELETE_PED(ped)
    ENDIF   
ENDPROC 

PROC CLEAN_UP_VEHICLE_ENTITIES  (VEHICLE_INDEX vehicle)
    IF DOES_ENTITY_EXIST(vehicle)
        DELETE_VEHICLE(vehicle)
    ENDIF
ENDPROC 
    
PROC CLEAN_UP_OBJECT_ENTITIES  (OBJECT_INDEX object)
    IF DOES_ENTITY_EXIST(object)
        DELETE_OBJECT(object)
    ENDIF
ENDPROC     
    
    PROC PRINT_CAR_INFO_TO_OUTPUTFILE (vehicle_struct lVehicle, int iVEH_INDEX)
        STRING models
            IF DOES_ENTITY_EXIST(lVehicle.Vehicle)
                IF IS_VEHICLE_DRIVEABLE (lVehicle.Vehicle)
                    models = GET_MODEL_NAME_FOR_DEBUG (lVehicle.VehicleModel)       
                    OPEN_DEBUG_FILE ()
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                        SAVE_STRING_TO_DEBUG_FILE("VEHICLES [")
                        SAVE_INT_TO_DEBUG_FILE(iVEH_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].vehicleCoords = << ")
                        SAVE_FLOAT_TO_DEBUG_FILE(lvehicle.VehicleCoords.x)
                        SAVE_STRING_TO_DEBUG_FILE(", ")
                        SAVE_FLOAT_TO_DEBUG_FILE(lvehicle.VehicleCoords.y)
                        SAVE_STRING_TO_DEBUG_FILE(", ")
                        SAVE_FLOAT_TO_DEBUG_FILE(lvehicle.VehicleCoords.z)
                        SAVE_STRING_TO_DEBUG_FILE(">> ")
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                        SAVE_STRING_TO_DEBUG_FILE("VEHICLES [")
                        SAVE_INT_TO_DEBUG_FILE(iVEH_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].vehicleHeading = ")
                        SAVE_FLOAT_TO_DEBUG_FILE(lvehicle.vehicleheading)
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                        SAVE_STRING_TO_DEBUG_FILE("VEHICLES [")
                        SAVE_INT_TO_DEBUG_FILE(iVEH_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].vehiclemodel = ")
                        SAVE_STRING_TO_DEBUG_FILE(models)
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                    CLOSE_DEBUG_FILE()  
                ENDIF   
            ENDIF
        
    ENDPROC
        
FUNC STRING RETURN_command_types (bool test_bool )
    STRING RETURN_string
    IF test_bool 
        RETURN_string = "TRUE"  
    ELSE
        RETURN_string = "FALSE"
    ENDIF   

    RETURN RETURN_string

ENDFUNC

//~> Functions: CAMERA
//Functiions: CAMERA

PROC SWAP_BETWEEN_CAMS (CAMERA_INDEX cam1, CAMERA_INDEX cam2)
    IF DOES_CAM_EXIST (cam2)
        SET_CAM_ACTIVE (cam2, FALSE)
    ENDIF
    
    IF DOES_CAM_EXIST (cam1)
        SET_CAM_ACTIVE (cam1, TRUE)
    ENDIF
ENDPROC

PROC SET_UP_TEST_CAM()
    
    IF NOT DOES_CAM_EXIST (TestCam )
        testCam = CREATE_CAM ("DEFAULT_SCRIPTED_CAMERA")
    ENDIF
    
    SET_CAM_COORD (TestCam, TestCamInfo.cam_pos )
    SET_CAM_ROT (TestCam, TestCamInfo.cam_rot )
    SET_CAM_FOV (TestCam, TestCamInfo.cam_fov )
        
    SET_CAM_ACTIVE (TestCam, TRUE)
    
    IF camscope = 0
        RENDER_SCRIPT_CAMS (TRUE, FALSE)
        camscope += 1
    ENDIF
    
ENDPROC

PROC TEMP_ACTIVATE_CAMS (CAMERA_INDEX cam )
    if DOES_CAM_EXIST (cam)
        
        if not IS_CAM_ACTIVE (cam)
            SET_CAM_ACTIVE (cam, true)
        ENDIF
        
        IF camscope = 0
            RENDER_SCRIPT_CAMS (TRUE, FALSE)
            camscope += 1
        ENDIF
    ENDIF   
ENDPROC

PROC ACTIVATE_CAM (CAMERA_INDEX cam)
    IF DOES_CAM_EXIST (CAM)
        SET_CAM_ACTIVE (cam, true)
    ENDIF
ENDPROC

PROC SWITCH_BETWEEN_FIXED_AND_TRACKING (CAMERA_INDEX CAM)
    IF IS_BUTTON_PRESSED (pad1, LEFTSHOULDER1)
        IF IS_BUTTON_JUST_PRESSED (PAD1,RIGHTSHOULDER1)

                IF DOES_CAM_EXIST (CAM)
                    SET_CAM_ACTIVE (CAM, true)
                ENDIF
        
        ENDIF
    ENDIF
ENDPROC



PROC CREATE_A_SCRIPTED_CAM (CAMERA_INDEX &cam, VECTOR cam_pos, VECTOR cam_rot, FLOAT Cam_fov)
        IF NOT DOES_CAM_EXIST (cam )
            cam = CREATE_CAM_WITH_PARAMS ("DEFAULT_SCRIPTED_CAMERA",cam_pos, cam_rot, Cam_fov)
        ENDIF
ENDPROC



PROC CREATE_CAM_ENTITY (StCameraData &lcam)
    IF NOT DOES_CAM_EXIST (lcam.cam)
        if lcam.bActivateCam 
            lcam.cam = CREATE_CAM_WITH_PARAMS ("DEFAULT_SCRIPTED_CAMERA", lcam.cam_pos, lcam.cam_rot, lcam.cam_fov )
        ENDIF
    ENDIF
ENDPROC

//PURPOSE: Create a scripted camera from the pos of the debug cam, for setting up scenarios
PROC Temp_Create_Debug_Cam (StCameraData &lcam)
    
        CAMERA_INDEX DebugCam
        //VECTOR ground_draw_pos
    //  FLOAT ground_z
    

        DebugCam = GET_DEBUG_CAM ()
            IF IS_CAM_RENDERING (DebugCam)
            
                IF (IS_BUTTON_PRESSED (PAD1, LEFTSHOULDER1 ) and IS_BUTTON_JUST_PRESSED (PAD1,LEFTSHOULDER2) )
                    or (IS_BUTTON_PRESSED (DEBUG_PAD, LEFTSHOULDER1 ) and IS_BUTTON_JUST_PRESSED (DEBUG_PAD,LEFTSHOULDER2) )
                
                        lcam.cam_pos = GET_CAM_COORD(DebugCam)
                        lcam.cam_rot  = GET_CAM_ROT (DebugCam)
                        lcam.cam_fov =  GET_CAM_FOV (DebugCam)  
                    
                            IF NOT DOES_CAM_EXIST (lcam.cam)
                                lcam.cam =  CREATE_CAM_WITH_PARAMS ("DEFAULT_SCRIPTED_CAMERA", lcam.cam_pos, lcam.cam_rot ,lcam.cam_fov,  true) 
                            ENDIF   

                    
                  
                        SET_CAM_PARAMS (lcam.cam, lcam.cam_pos, lcam.cam_rot,  lcam.cam_fov  )  
    
                    IF camscope = 0
                        RENDER_SCRIPT_CAMS   (TRUE, FALSE)
                        camscope += 1
                    ENDIF   
                    
                ENDIF
            ENDIF   
ENDPROC

//PURPOSE: Create a scripted camera from the pos of the debug cam, for setting up scenarios
PROC Create_Debug_Cam ()
        VECTOR CamPos, CamRot
        CAMERA_INDEX DebugCam
        VECTOR ground_draw_pos
        FLOAT ground_z
    
        IF  DOES_CAM_EXIST (DebugCam)
            DebugCam = GET_DEBUG_CAM ()
            IF IS_CAM_ACTIVE (DebugCam)
                CamPos = GET_CAM_COORD(DebugCam)
                CamRot = GET_CAM_ROT (DebugCam)
                IF IS_KEYBOARD_KEY_JUST_PRESSED (key_y)
                    IF NOT DOES_CAM_EXIST (TestCam)
                        TestCam =   CREATE_CAM ("DEFAULT_SCRIPTED_CAMERA")
                        
                        IF NOT IS_PED_INJURED (scplayer)
                            GET_GROUND_Z_FOR_3D_COORD (CamPos, ground_z  )
                            //set the player pso on the ground below our debug camera
                              SET_PED_COORDS_KEEP_VEHICLE(scplayer, <<CamPos.x, CamPos.y, ground_z>> )
                            SET_ENTITY_HEADING(scplayer, camrot.z )
                            
                            //get the ground z from an offset from the player
                            ground_draw_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS (scplayer, <<0.0, 20, 50.0>> )
                            GET_GROUND_Z_FOR_3D_COORD (ground_draw_pos, ground_z  )
                            
                            //** FIX THIS NEED A WAY OF GETTING A POINT IN FRONT OF THE CAM **
                            
                            // find an empty ped slot to fill our new ground pos and set the ped index to this new value so when we turn on the debug we will get a nice marker infront of our cam
//                              for index = 0 to number_of_peds -1
//                                  IF NOT DOES_ENTITY_EXIST(TestPeds[index].ped )
//                                      gPedcoords =  <<ground_draw_pos.x, ground_draw_pos.y,  ground_z>>
//                                      gTestPedsIndex =  index
//                                      index = MAX_NUMBER_OF_PEDS
//                                  ENDIF
//                              ENDFOR
                        ENDIF   
                    ENDIF   
                            
                    SET_CAM_COORD     (TestCam, CamPos)
                    SET_CAM_ROT(TestCam, CamRot)
                    
                    SET_CAM_FOV(TestCam, 60)
                    SWAP_BETWEEN_CAMS (TestCam , SkiCam)
                    
                    IF camscope = 0
                        RENDER_SCRIPT_CAMS   (TRUE, FALSE)
                        camscope += 1
                    ENDIF   
                    
                ENDIF
            ENDIF   
        ENDIF

            IF IS_KEYBOARD_KEY_JUST_PRESSED ( key_e) 
                IF  DOES_CAM_EXIST (TestCam)
                    DESTROY_CAM (TestCam)
                    IF camscope = 1
                        RENDER_SCRIPT_CAMS   (FALSE, FALSE)
                        camscope -= 1
                    ENDIF   
                ENDIF
            ENDIF

ENDPROC



FUNC VECTOR SphericalCamCoords (float zoom, float pan, float pitch )

    Vector CartesianCoords
    
    CartesianCoords.x = zoom * SIN (pan) * COS (pitch)
    
    CartesianCoords.y = zoom * SIN (pan) * SIN (pitch)
    
    CartesianCoords.z = zoom * COS (pan)
    
    return  CartesianCoords

ENDFUNC


PROC TempControlSkiCam(peds_struct &peds_l[], int number_of_peds, CAMERA_INDEX cam)
    BOOL SearchingForPeds = TRUE
    int FallOutOfLoop = 0
    vector camera_pos
    
        if DOES_CAM_EXIST(cam)
            IF IS_CAM_ACTIVE (cam)
                    int ileft_x, iright_x, ileft_y, iright_y
                    GET_POSITION_OF_ANALOGUE_STICKS (PAD1, ileft_x, ileft_y, iright_x, iright_y )
                            IF NOT IS_PED_INJURED(peds_l[camped].ped  )
                                IF NOT IS_PED_RAGDOLL (peds_l[camped].ped  )
                                    if not  IS_PED_GETTING_UP(peds_l[camped].ped )
                                        POINT_CAM_AT_ENTITY(cam, peds_l[camped].ped, <<0.0, 0.0, 0.0>> )
                                        
                                        IF (iright_x >= -128) AND (iright_x < 0)  
                                                gPan     -= 2
                                        ENDIF
                                        
                                        IF (iright_x <= 128) AND (iright_x > 0)
                                            gPan += 2
                                        ENDIF
                                        
                                        IF (iright_y >= -128) AND (iright_y < 0)
                                            IF gPitch < -3
                                                gPitch += 2
                                            ENDIF
                                        
                                        ENDIF
                                        
                                        IF (iright_y <= 128) AND (iright_y > 0)
                                            IF gPitch > -178
                                                gPitch  -= 2
                                            ENDIF
                                        ENDIf
                            
                                        IF IS_BUTTON_PRESSED (PAD1, LEFTSHOULDER2)
                                            if gZoom > 0.5
                                                gZoom -= 0.15
                                            ENDIF
                                        ENDIF
                                        
                                        IF IS_BUTTON_PRESSED (PAD1, RIGHTSHOULDER2)
                                            IF gZoom < 17.5
                                                gZoom += 0.15
                                            ENDIF
                                        ENDIf

                                        IF NOT IS_PED_INJURED(peds_l[camped].ped  )
                                            IF NOT IS_PED_RAGDOLL (peds_l[camped].ped  )
                                                POINT_CAM_AT_ENTITY(cam, peds_l[camped].ped, <<0.0, 0.0, 0.0>> )
                                                camera_pos =  GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(peds_l[camped].ped, SphericalCamCoords (gzoom, gPitch, gPan))
                                                SET_CAM_COORD (cam, camera_pos)
                                            ENDIF   
                                        ENDIF
        
                                    ENDIF   
                                ENDIF
                            ENDIF
                        
                IF IS_BUTTON_JUST_PRESSED (Pad1, RIGHTSHOULDER2) 
                        
                    WHILE SearchingForPeds  
                        FallOutOfLoop+= 1
                        
                        //this terminates the while loop if there are no valid peds.
                        IF FallOutOfLoop = number_of_peds
                            SearchingForPeds = FALSE
                        ENDIF
                        
                        CamPointPed += 1    
                        
                        IF  CamPointPed = number_of_peds -1 
                            CamPointPed = 0
                        ENDIF
                        
                        if DOES_ENTITY_EXIST(peds_l[camped].ped )
                            IF NOT IS_PED_INJURED(peds_l[CamPointPed].ped  )
                              //  POINT_CAM_AT_ENTITY(cam, peds_l[CamPointPed].ped, <<0.0, 0.0,0.0>> )
                                SearchingForPeds = FALSE            
                            ENDIF   
                        ENDIF   
                    ENDWHILE
                    
                ENDIF
            ENDIF
        ENDIF
ENDPROC

PROC Temp_Run_Scenario_Tracking_Cam (peds_struct &peds_l[], int number_of_peds, CAMERA_INDEX &cam, bool forcecam = FALSE)
    
    BOOL SearchingForPeds = TRUE
    int FallOutOfLoop = 0
    vector camera_pos
    
        IF (IS_BUTTON_PRESSED (Pad1, LEFTSHOULDER1) AND IS_BUTTON_JUST_PRESSED (PAD1, RIGHTSHOULDER2 ) )or forcecam
                
            WHILE SearchingForPeds  
                FallOutOfLoop+= 1
                
                //this terminates the while loop if there are no valid peds.
                IF FallOutOfLoop = number_of_peds
                    SearchingForPeds = FALSE
                ENDIF
                
                camped += 1 
                
                IF  camped = number_of_peds -1 
                    camped = 0
                ENDIF
                
                IF NOT IS_PED_INJURED(peds_l[camped].ped  )
                        //switches off the test cam
                        
                        IF NOT DOES_CAM_EXIST (cam )
                            cam = CREATE_CAM ("DEFAULT_SCRIPTED_CAMERA")
                                gZoom =3.25
                                gPan = 90
                                gPitch = -72
                        
                        ENDIF
                        
                        SET_CAM_ACTIVE (cam, true)
                       
                        
                        IF camscope = 0 
                            RENDER_SCRIPT_CAMS     (TRUE, FALSE)
                            camscope += 1
                        ENDIF
                    
                    IF NOT DOES_CAM_EXIST (cam )
                            IF NOT IS_PED_INJURED(peds_l[camped].ped  )
                                IF NOT IS_PED_RAGDOLL (peds_l[camped].ped  )
                                    POINT_CAM_AT_ENTITY(cam, peds_l[camped].ped, <<0.0, 0.0, 0.0>> )
                                    camera_pos =  GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(peds_l[camped].ped, SphericalCamCoords (gzoom, gPitch, gPan))
                                    SET_CAM_COORD (cam, camera_pos)
                                ENDIF   
                            ENDIF
                        ENDIF   
                    SearchingForPeds = FALSE            
                ENDIF   
            ENDWHILE
        ENDIF
    
        TempControlSkiCam (peds_l,  number_of_peds, cam)
        
ENDPROC





Proc SET_TEST_CAM_POS ()
    VECTOR Pos, Rot
    IF DOES_CAM_EXIST (TestCam)
        Pos = GET_CAM_COORD (TestCam)
        Rot = GET_CAM_ROT (TestCam)
        
        TestCamInfo.cam_pos = Pos
        TestCamInfo.cam_rot = Rot
        
    ENDIF
    
ENDPROC

Proc TEMP_SET_TEST_CAM_POS ()
    VECTOR Pos, Rot
    IF DOES_CAM_EXIST (TestCam)
        Pos = GET_CAM_COORD (TestCam)
        Rot = GET_CAM_ROT (TestCam)
        
        TestCamInfo.cam_pos = Pos
        TestCamInfo.cam_rot = Rot
        
    ENDIF
    
ENDPROC


//PURPOSE: Cleans up cameras after each scenario has been run

PROC Temp_cleanup_scenario_cams ( )
    IF camscope = 1
        RENDER_SCRIPT_CAMS   (FALSE, FALSE)
        camscope -= 1
    ENDIF
    
    DESTROY_ALL_CAMS ()

ENDPROC

//PROC Cleanup_Scenario_Cams ()
//    DESTROY_ALL_CAMS ()
//    IF DOES_CAM_EXIST (SkiCam)
//        DESTROY_CAM (SkiCam)
//    ENDIF   
//    
//    IF NOT DOES_CAM_EXIST (TestCam)
//        IF camscope = 1
//            RENDER_SCRIPT_CAMS   (FALSE, FALSE)
//            camscope -= 1
//        ENDIF
//    ENDIF
//ENDPROC

//PROC STOP_SCRIPTED_CAMERA ()
//      RENDER_SCRIPT_CAMS   (FALSE, FALSE)
//  DESTROY_ALL_CAMS ()
//ENDPROC


PROC TEMP_DEBUG_SCENARIO (peds_struct &pedsl[], vehicle_struct &vehiclesl[], RouteStruct& routel[], CAMERA_INDEX cam, MODEL_NAMES model )
    int index
    int PedArray, VehicleArray, RouteArray  
    bool    CanUseCamCoords = FALSE
    vector cam_coords, cam_rot, ground_draw_pos
    float Ground_Z
    
    PedArray = COUNT_OF (pedsl) 
    VehicleArray = COUNT_OF (vehiclesl)
    RouteArray = COUNT_OF (routel)
    
    CAMERA_INDEX debug_cam
    
    debug_cam = GET_DEBUG_CAM ()
    
        //TEMP_SET_TEST_CAM_POS ()
        
        DebugStatus = SELECT_DEBUG_MODE (DebugStatus)
        
        //debug 
        DRAW_DEBUG_LINE_FOR_ENTITY_PLACEMENT (pedsl[gTestPedsIndex], vehiclesl[gTestVehicleIndex]  )

        if DOES_CAM_EXIST (cam)
            IF IS_CAM_RENDERING(cam) or IS_CAM_RENDERING(debug_cam)
                Use_Joystick_To_Position_Peds_and_Vehicles  (PedArray, VehicleArray, RouteArray)
            ENDIF
        ELSE
            IF IS_CAM_RENDERING (debug_cam)
                Use_Joystick_To_Position_Peds_and_Vehicles  (PedArray, VehicleArray, RouteArray)
            ENDIF
        ENDIF
        
        //Each aspect of debugging has state to allow the controller to be used 
        SWITCH  DebugStatus
        
    
        //Routes 
            CASE DebugRoute
            
                SET_ROUTE_NODES(routel[gRouteNode])
                
                FOR index = 0 to RouteArray -1 
                    DRAW_ROUTE_POINTS (routel[index])
                ENDFOR
                
                DRAW_NODE_HIGHLIGHTER (routel[gRouteNode])
                
            BREAK
        
        //defensive area
            CASE DebugDefensiveArea
                //weapons
                IF gSwapPedWeapons
                    IF NOT IS_PED_INJURED (pedsl[gTestPedsIndex].Ped)               
                        WeaponModelSelection (pedsl[gTestPedsIndex])
                        SWAP_PED_WEAPONS (pedsl[gTestPedsIndex])
                    ENDIF
                    gSwapPedWeapons = FALSE
                ENDIF
            
                if gPedDefAActive
                    PositionDefensiveArea (pedsl[gTestPedsIndex])
                ENDIF
            BREAK
            
            CASE DebugPed
                //peds
                SETUP_PEDS_POS (pedsl[gTestPedsIndex])      
                
            
      
					IF gCreatePed
						PedModelSelection (pedsl[gTestPedsIndex], model)
					
						CREATE_PED_ENTITY (pedsl[gTestPedsIndex])
					
						IF DOES_ENTITY_EXIST(pedsl[gTestPedsIndex].PED)
							if DOES_CAM_EXIST (cam)
								IF IS_CAM_RENDERING(cam)             
									cam_coords = GET_CAM_COORD (cam)
									cam_rot = GET_CAM_ROT(cam )
									CanUseCamCoords = true
								ELSE 
									if IS_CAM_RENDERING(debug_cam)
										cam_coords = GET_CAM_COORD (debug_cam)
										cam_rot = GET_CAM_ROT(debug_cam )
										CanUseCamCoords = true
									ENDIF
								ENDIF   
							ELSE
								IF IS_CAM_RENDERING (debug_cam)
										cam_coords = GET_CAM_COORD (debug_cam)
										cam_rot = GET_CAM_ROT(debug_cam )
										CanUseCamCoords = true
									
								ENDIF
							ENDIF
							
							if CanUseCamCoords  
								IF not IS_PED_INJURED (pedsl[gTestPedsIndex].PED)
									  SET_PED_COORDS_KEEP_VEHICLE(pedsl[gTestPedsIndex].PED, cam_coords)
									SET_ENTITY_HEADING(pedsl[gTestPedsIndex].PED, cam_rot.z )
									
									ground_draw_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( pedsl[gTestPedsIndex].ped, <<0.0, 20, 50.0>> )
									GET_GROUND_Z_FOR_3D_COORD (ground_draw_pos, ground_z  )
								
									pedsl[gTestPedsIndex].PedsCoords = <<ground_draw_pos.x, ground_draw_pos.y, ground_z>>
								
									  SET_PED_COORDS_KEEP_VEHICLE(pedsl[gTestPedsIndex].PED, pedsl[gTestPedsIndex].PedsCoords)
								
								ENDIF
							ENDIF
						
						ENDIF   
					
						Block_Peds_Temp_Events (pedsl[gTestPedsIndex], TRUE)
						SWAP_PED_WEAPONS (pedsl[gTestPedsIndex])
						ADD_RELATIONSHIP_GROUP_TO_STRUCT (pedsl[gTestPedsIndex])
							GiveBlipsToPedsGrps (pedsl[gTestPedsIndex])
						gCreatePed = FALSE
                ENDIF
    
                IF gdeletePed 
                    delete_scenario_ped (pedsl[gTestPedsIndex] )
                    gdeletePed = FALSE
                ENDIF
                
                    //ped combat
                IF gsetCombatability
                    SET_COMBAT_ABILITY (pedsl[gTestPedsIndex] )
                    ChangePedCombatAbility (pedsl[gTestPedsIndex] ) 
                    gsetCombatability = FALSE
                ENDIF
                
                IF gsetCombatMove
                    SET_COMBAT_MOVEMENT (pedsl[gTestPedsIndex] )
                    ChangePedCombatMovement (pedsl[gTestPedsIndex])
                    gsetCombatMove = FALSE
                ENDIF
                IF gsetCombatRange
                    SET_COMBAT_RANGE (pedsl[gTestPedsIndex] )
                    ChangePedCombatRange  (pedsl[gTestPedsIndex]  )
                    gsetCombatRange = FALSE
                ENDIF
                
                IF gsetCombatAttribute
                    set_combat_attributes  (pedsl[gTestPedsIndex] )
                    ChangePedCombatAttribute (pedsl[gTestPedsIndex] )
                    gsetCombatAttribute = FALSE
                ENDIF
                
                //weapons
                IF gSwapPedWeapons
                    IF NOT IS_PED_INJURED (pedsl[gTestPedsIndex].Ped)               
                        WeaponModelSelection (pedsl[gTestPedsIndex])
                        SWAP_PED_WEAPONS (pedsl[gTestPedsIndex])
                    ENDIF
                    gSwapPedWeapons = FALSE
                ENDIF
        BREAK
        
        CASE DebugVehicle
        
            //vehicles
            SETUP_VEHICLE_POS (vehiclesl[gTestVehicleIndex])
            
            IF gCreateVehicle
                vehiclesl[gTestVehicleIndex].VehicleModel =  VehicleModelSelection ()
                vehiclesl[gTestVehicleIndex].VehicleCoords = gVehiclecoords
                CREATE_VEHICLE_ENTITY (vehiclesl[gTestVehicleIndex])    
                gCreateVehicle = false
            ENDIF
            
            IF gDeleteVehicle
            
                DELETE_SCENARIO_VEHICLE (vehiclesl[gTestVehicleIndex])
                gDeleteVehicle = FALSE
            ENDIF
            
    
        BREAK
    ENDSWITCH
ENDPROC




PROC DEBUG_SCENARIO (peds_struct &pedsl[], vehicle_struct &vehiclesl[], RouteStruct& routel[], MODEL_NAMES model  )
    int index
    int PedArray, VehicleArray, RouteArray  
        
    PedArray = COUNT_OF (pedsl) 
    VehicleArray = COUNT_OF (vehiclesl)
    RouteArray = COUNT_OF (routel)
    
        SET_TEST_CAM_POS ()
        
        DebugStatus = SELECT_DEBUG_MODE (DebugStatus)
        
        //debug 
        DRAW_DEBUG_LINE_FOR_ENTITY_PLACEMENT (pedsl[gTestPedsIndex], vehiclesl[gTestVehicleIndex]  )
        
        
        
        IF DOES_CAM_EXIST (TestCam)
            IF IS_CAM_ACTIVE (TestCam)
                Use_Joystick_To_Position_Peds_and_Vehicles  (PedArray, VehicleArray, RouteArray)
            ENDIF
        ENDIF
        
        //Each aspect of debugging has state to allow the controller to be used 
        
        SWITCH  DebugStatus
    
        //Routes 
            CASE DebugRoute
            
                SET_ROUTE_NODES(routel[gRouteNode])
                
                FOR index = 0 to RouteArray -1 
                    DRAW_ROUTE_POINTS (routel[index])
                ENDFOR
                
                DRAW_NODE_HIGHLIGHTER (routel[gRouteNode])
                
            BREAK
        
        //defensive area
            CASE DebugDefensiveArea
                //weapons
                IF gSwapPedWeapons
                    IF NOT IS_PED_INJURED (pedsl[gTestPedsIndex].Ped)               
                        WeaponModelSelection (pedsl[gTestPedsIndex])
                        SWAP_PED_WEAPONS (pedsl[gTestPedsIndex])
                    ENDIF
                    gSwapPedWeapons = FALSE
                ENDIF
            
            BREAK
            
            CASE DebugPed
                //peds
                SETUP_PEDS_POS (pedsl[gTestPedsIndex])      
                
                IF gCreatePed
                    PedModelSelection (pedsl[gTestPedsIndex], model)
                
                    CREATE_PED_ENTITY (pedsl[gTestPedsIndex])
                
                    Block_Peds_Temp_Events (pedsl[gTestPedsIndex], TRUE)
                    SWAP_PED_WEAPONS (pedsl[gTestPedsIndex])
                    ADD_RELATIONSHIP_GROUP_TO_STRUCT (pedsl[gTestPedsIndex])
                        GiveBlipsToPedsGrps (pedsl[gTestPedsIndex])
                    gCreatePed = FALSE
                ENDIF
    
                IF gdeletePed 
                    delete_scenario_ped (pedsl[gTestPedsIndex] )
                    gdeletePed = FALSE
                ENDIF
                
                    //ped combat
                IF gsetCombatability
                    SET_COMBAT_ABILITY (pedsl[gTestPedsIndex] )
                    ChangePedCombatAbility (pedsl[gTestPedsIndex] ) 
                    gsetCombatability = FALSE
                ENDIF
                
                IF gsetCombatMove
                    SET_COMBAT_MOVEMENT (pedsl[gTestPedsIndex] )
                    ChangePedCombatMovement (pedsl[gTestPedsIndex])
                    gsetCombatMove = FALSE
                ENDIF
                IF gsetCombatRange
                    SET_COMBAT_RANGE (pedsl[gTestPedsIndex] )
                    ChangePedCombatRange  (pedsl[gTestPedsIndex]  )
                    gsetCombatRange = FALSE
                ENDIF
                
                IF gsetCombatAttribute
                    set_combat_attributes  (pedsl[gTestPedsIndex] )
                    ChangePedCombatAttribute (pedsl[gTestPedsIndex] )
                    gsetCombatAttribute = FALSE
                ENDIF
                
                //weapons
                IF gSwapPedWeapons
                    IF NOT IS_PED_INJURED (pedsl[gTestPedsIndex].Ped)               
                        WeaponModelSelection (pedsl[gTestPedsIndex])
                        SWAP_PED_WEAPONS (pedsl[gTestPedsIndex])
                    ENDIF
                    gSwapPedWeapons = FALSE
                ENDIF
        BREAK
        
        CASE DebugVehicle
        
            //vehicles
            SETUP_VEHICLE_POS (vehiclesl[gTestVehicleIndex])
            
            IF gCreateVehicle
                vehiclesl[gTestVehicleIndex].VehicleModel =  VehicleModelSelection (vehiclesl[gTestVehicleIndex].Vehicle)
               vehiclesl[gTestVehicleIndex].VehicleCoords = gVehiclecoords
                CREATE_VEHICLE_ENTITY (vehiclesl[gTestVehicleIndex])    
                gCreateVehicle = false
            ENDIF
            
            IF gDeleteVehicle
            
                DELETE_SCENARIO_VEHICLE (vehiclesl[gTestVehicleIndex])
                gDeleteVehicle = FALSE
            ENDIF
            
    
        BREAK
    ENDSWITCH
ENDPROC

ENUM TestTextFlag
    TTS_TestRunning,
    TTS_DebugMode,
    TTS_ClearPrints,
    TTS_DebugAndTestActive,
    TTS_TestRunningFixedCam,
    TTS_TestRunningPedCam,
    TTS_WaitForState
ENDENUM

TestTextFlag TestTextState = TTS_WaitForState


PROC NM_PRINT_ACTIVE_TEST(bool scenario, bool debug, CAMERA_INDEX ped_CAm, CAMERA_INDEX fixed_Cam)

    if scenario 
           TestTextState = TTS_TestRunning
        
        IF DOES_CAM_EXIST (ped_CAm)
            IF IS_CAM_RENDERING (ped_CAM)
                  TestTextState = TTS_TestRunningPedCam
            ENDIF
        ENDIF
        
        IF DOES_CAM_EXIST (fixed_Cam)
            IF IS_CAM_RENDERING (fixed_Cam)
                  TestTextState = TTS_TestRunningFixedCam
            ENDIF
        ENDIF
  
        if debug
            TestTextState = TTS_DebugAndTestActive
        ENDIF
    ELSE
        if debug
            TestTextState = TTS_DebugMode
        ELSE
            TestTextState = TTS_ClearPrints
        ENDIF
    ENDIF
    
    SWITCH  TestTextState
    
        CASE TTS_TestRunning
            PRINT_NOW("tstdbg07", 1000000 ,3 )
            TestTextState = TTS_WaitForState
        BREAK
        
        CASE TTS_TestRunningFixedCam
            PRINT_NOW("tstdbg7a", 1000000 ,3 )
            TestTextState = TTS_WaitForState
        BREAK
        
        CASE TTS_TestRunningPedCam
            PRINT_NOW("tstdbg7b", 1000000 ,3 )
            TestTextState = TTS_WaitForState
        BREAK
        
        CASE TTS_DebugMode
            PRINT_NOW("tstdbg13", 1000000 ,3 )
            TestTextState = TTS_WaitForState
        BREAK
    
        CASE TTS_DebugAndTestActive
            PRINT_NOW("tstdbg14", 1000000 ,3 )
            TestTextState = TTS_WaitForState
        BREAK
    
        CASE TTS_ClearPrints
            CLEAR_PRINTS ()
            TestTextState = TTS_WaitForState
        BREAK
        
        CASE TTS_WaitForState
        
        BREAK
        
        
    ENDSWITCH
    
ENDPROC

PROC PRINT_ACTIVE_TEST(bool scenario, bool debug)

    if scenario 
        TestTextState = TTS_TestRunning
        
        if debug
            TestTextState = TTS_DebugAndTestActive
        ENDIF
    ELSE
        if debug
            TestTextState = TTS_DebugMode
        ELSE
            TestTextState = TTS_ClearPrints
        ENDIF
    ENDIF
    
    SWITCH  TestTextState
    
        CASE TTS_TestRunning
            PRINT_NOW("tstdbg07", 1000000 ,3 )
            TestTextState = TTS_WaitForState
        BREAK
        
        CASE TTS_DebugMode
            PRINT_NOW("tstdbg13", 1000000 ,3 )
            TestTextState = TTS_WaitForState
        BREAK
    
        CASE TTS_DebugAndTestActive
            PRINT_NOW("tstdbg14", 1000000 ,3 )
            TestTextState = TTS_WaitForState
        BREAK
    
        CASE TTS_ClearPrints
            CLEAR_PRINTS ()
            TestTextState = TTS_WaitForState
        BREAK
        
        CASE TTS_WaitForState
        
        BREAK
        
        
    ENDSWITCH
    
ENDPROC


PROC PRINT_PED_INFO_TO_OUTPUTFILE ( peds_struct lpeds, int iped_index)
    STRING models
        IF DOES_ENTITY_EXIST(lpeds.ped)
            IF not IS_PED_INJURED (lpeds.ped)
                models = GET_MODEL_NAME_FOR_DEBUG (lpeds.PedModel)                  
                OPEN_DEBUG_FILE ()
                    SAVE_NEWLINE_TO_DEBUG_FILE()
                    SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                    SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                    SAVE_STRING_TO_DEBUG_FILE("].PedsCoords = << ")
                    SAVE_FLOAT_TO_DEBUG_FILE(lpeds.PedsCoords.x)
                    SAVE_STRING_TO_DEBUG_FILE(", ")
                    SAVE_FLOAT_TO_DEBUG_FILE(lpeds.PedsCoords.y)
                    SAVE_STRING_TO_DEBUG_FILE(", ")
                    SAVE_FLOAT_TO_DEBUG_FILE(lpeds.PedsCoords.z)
                    SAVE_STRING_TO_DEBUG_FILE(">> ")
                    SAVE_NEWLINE_TO_DEBUG_FILE()
                    SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                    SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                    SAVE_STRING_TO_DEBUG_FILE("].PedHeading = ")
                    SAVE_FLOAT_TO_DEBUG_FILE(lpeds.PedHeading)
                    SAVE_NEWLINE_TO_DEBUG_FILE()
                    SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                    SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                    SAVE_STRING_TO_DEBUG_FILE("].Pedrelgrp = ")
                    SAVE_INT_TO_DEBUG_FILE(lpeds.Pedrelgrp)
                    SAVE_NEWLINE_TO_DEBUG_FILE()
                    SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                    SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                    SAVE_STRING_TO_DEBUG_FILE("].PedModel = ")
                    SAVE_STRING_TO_DEBUG_FILE(models)
                    SAVE_NEWLINE_TO_DEBUG_FILE()
                    SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                    SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                    SAVE_STRING_TO_DEBUG_FILE("].PedsWeapon = ")
                    SAVE_STRING_TO_DEBUG_FILE(lpeds.pedsstringweapon)
                    SAVE_NEWLINE_TO_DEBUG_FILE()
                CLOSE_DEBUG_FILE()  
        
                IF lpeds.PedActiveDefArea 
                    OPEN_DEBUG_FILE()
                    SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                    SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                    SAVE_STRING_TO_DEBUG_FILE("].PedDefCoord = << ")
                    SAVE_FLOAT_TO_DEBUG_FILE(lpeds.PedDefCoord.x)
                    SAVE_STRING_TO_DEBUG_FILE(", ")
                    SAVE_FLOAT_TO_DEBUG_FILE(lpeds.PedDefCoord.y)
                    SAVE_STRING_TO_DEBUG_FILE(", ")
                    SAVE_FLOAT_TO_DEBUG_FILE(lpeds.PedDefCoord.z)
                    SAVE_STRING_TO_DEBUG_FILE(">> ")
                    SAVE_NEWLINE_TO_DEBUG_FILE()
                    SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                    SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                    SAVE_STRING_TO_DEBUG_FILE("].PedDefArea = ")
                    SAVE_FLOAT_TO_DEBUG_FILE(lpeds.PedDefArea)
                    SAVE_NEWLINE_TO_DEBUG_FILE()
                    SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                    SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                    SAVE_STRING_TO_DEBUG_FILE("].PedActiveDefArea = ")
                    SAVE_STRING_TO_DEBUG_FILE(RETURN_command_types (lpeds.PedActiveDefArea))
                    SAVE_NEWLINE_TO_DEBUG_FILE()
                    CLOSE_DEBUG_FILE()
                ENDIF
                //only print if data is needed
                IF (lpeds.pedCombatMove <> - 1)
                    OPEN_DEBUG_FILE ()
                        SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                        SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].PedcombatMove = ")
                        SAVE_INT_TO_DEBUG_FILE(lpeds.pedCombatMove)
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                    CLOSE_DEBUG_FILE ()
                ENDIF
                
                IF (lpeds.pedcombatrange <> - 1)
                    OPEN_DEBUG_FILE ()
                        SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                        SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].pedcombatrange = ")
                        SAVE_INT_TO_DEBUG_FILE(lpeds.pedcombatrange)
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                    CLOSE_DEBUG_FILE ()
                ENDIF
                
                IF (lpeds.pedcombatability <> - 1)
                    OPEN_DEBUG_FILE ()
                        SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                        SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].pedcombatability = ")
                        SAVE_INT_TO_DEBUG_FILE(lpeds.pedcombatability)
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                    CLOSE_DEBUG_FILE () 
                ENDIF
                
                IF (lpeds.combatcover <> CA_INVALID)
                    OPEN_DEBUG_FILE ()
                        SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                        SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].combatcover = ")
                        //SAVE_INT_TO_DEBUG_FILE(lpeds.combatcover)
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                        SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                        SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].bcombatcover = ")
                        SAVE_STRING_TO_DEBUG_FILE(RETURN_command_types (lpeds.bcombatcover))
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                    CLOSE_DEBUG_FILE ()
                ENDIF
                
                IF (lpeds.combatUseVehicle <> CA_INVALID)
                    OPEN_DEBUG_FILE ()
                        SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                        SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].combatUseVehicle = ")
                        //SAVE_INT_TO_DEBUG_FILE(lpeds.combatUseVehicle)
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                        SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                        SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].bcombatUseVehicle = ")
                        SAVE_STRING_TO_DEBUG_FILE(RETURN_command_types (lpeds.bcombatUseVehicle))
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                    CLOSE_DEBUG_FILE ()
                ENDIF
                
                IF (lpeds.Combatdodrivebys <> CA_INVALID)
                    OPEN_DEBUG_FILE()
                        SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                        SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].Combatdodrivebys = ")
                        //SAVE_INT_TO_DEBUG_FILE(lpeds.Combatdodrivebys)
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                        SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                        SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].bCombatdodrivebys = ")
                        SAVE_STRING_TO_DEBUG_FILE(RETURN_command_types (lpeds.bCombatdodrivebys))
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                    CLOSE_DEBUG_FILE ()
                ENDIF
                
                IF (lpeds.combatleavevehicle <> CA_INVALID)
                    OPEN_DEBUG_FILE ()
                        SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                        SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].combatleavevehicle = ")
                        //SAVE_INT_TO_DEBUG_FILE(lpeds.combatleavevehicle)
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                        SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                        SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].bcombatleavevehicle = ")
                        SAVE_STRING_TO_DEBUG_FILE(RETURN_command_types (lpeds.bcombatleavevehicle))
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                    CLOSE_DEBUG_FILE ()
                ENDIF
                
                IF (lpeds.combativestevents <> CA_INVALID)
                    OPEN_DEBUG_FILE ()
                        SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                        SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].combativestevents = ")
                        //SAVE_INT_TO_DEBUG_FILE(lpeds.combativestevents)
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                        SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                        SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].bcombativestevents = ")
                        SAVE_STRING_TO_DEBUG_FILE(RETURN_command_types (lpeds.bcombativestevents))
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                    CLOSE_DEBUG_FILE()
                ENDIF
                
                IF (lpeds.combativestpeds <> CA_INVALID)
                    OPEN_DEBUG_FILE ()              
                        SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                        SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].combativestpeds = ")
                        //SAVE_INT_TO_DEBUG_FILE(lpeds.combativestpeds)
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                        SAVE_STRING_TO_DEBUG_FILE("TestPeds[")
                        SAVE_INT_TO_DEBUG_FILE(iped_INDEX)
                        SAVE_STRING_TO_DEBUG_FILE("].bcombativestpeds = ")
                        SAVE_STRING_TO_DEBUG_FILE(RETURN_command_types (lpeds.bcombativestpeds))
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                    CLOSE_DEBUG_FILE ()
                ENDIF
            ENDIF   
        ENDIF   

ENDPROC

PROC PRINT_CAMERA_INFO (VECTOR Pos, VECTOR Rot, float fov)
    OPEN_DEBUG_FILE ()
        SAVE_NEWLINE_TO_DEBUG_FILE()
        SAVE_STRING_TO_DEBUG_FILE("TestCamInfo.Cam_Pos = << ")
        SAVE_FLOAT_TO_DEBUG_FILE(Pos.x)
        SAVE_STRING_TO_DEBUG_FILE(", ")
        SAVE_FLOAT_TO_DEBUG_FILE(Pos.y)
        SAVE_STRING_TO_DEBUG_FILE(", ")
        SAVE_FLOAT_TO_DEBUG_FILE(Pos.z)
        SAVE_STRING_TO_DEBUG_FILE(">> ")
        SAVE_NEWLINE_TO_DEBUG_FILE()            
        SAVE_STRING_TO_DEBUG_FILE("TestCamInfo.Cam_Rot = << ")
        SAVE_FLOAT_TO_DEBUG_FILE(Rot.x)
        SAVE_STRING_TO_DEBUG_FILE(", ")
        SAVE_FLOAT_TO_DEBUG_FILE(Rot.y)
        SAVE_STRING_TO_DEBUG_FILE(", ")
        SAVE_FLOAT_TO_DEBUG_FILE(Rot.z)
        SAVE_STRING_TO_DEBUG_FILE(">> ")
        SAVE_NEWLINE_TO_DEBUG_FILE()
        SAVE_STRING_TO_DEBUG_FILE("TestCamInfo.Cam_Fov = ")
        SAVE_FLOAT_TO_DEBUG_FILE(fov)
        SAVE_NEWLINE_TO_DEBUG_FILE()
    CLOSE_DEBUG_FILE () 
ENDPROC

PROC PRINT_ROUTE_POS (VECTOR Pos, int index)
    OPEN_DEBUG_FILE ()
        SAVE_NEWLINE_TO_DEBUG_FILE()
        SAVE_STRING_TO_DEBUG_FILE("Route[")
        SAVE_INT_TO_DEBUG_FILE(index)
        SAVE_STRING_TO_DEBUG_FILE("].RouteNode = <<")
        SAVE_FLOAT_TO_DEBUG_FILE(Pos.x)
        SAVE_STRING_TO_DEBUG_FILE(", ")
        SAVE_FLOAT_TO_DEBUG_FILE(Pos.y)
        SAVE_STRING_TO_DEBUG_FILE(", ")
        SAVE_FLOAT_TO_DEBUG_FILE(Pos.z)
        SAVE_STRING_TO_DEBUG_FILE(">> ")
        SAVE_NEWLINE_TO_DEBUG_FILE()    
        SAVE_STRING_TO_DEBUG_FILE("Route[")
        SAVE_INT_TO_DEBUG_FILE(index)
        SAVE_STRING_TO_DEBUG_FILE("].RouteNodeState = true")
        SAVE_NEWLINE_TO_DEBUG_FILE()    
    CLOSE_DEBUG_FILE () 
ENDPROC

PROC PRINT_TAREGET_POS (VECTOR Pos, FLOAT heading)
    OPEN_DEBUG_FILE ()
        SAVE_NEWLINE_TO_DEBUG_FILE()
        SAVE_STRING_TO_DEBUG_FILE("DynamicTargetPos = <<")
        SAVE_FLOAT_TO_DEBUG_FILE(Pos.x)
        SAVE_STRING_TO_DEBUG_FILE(", ")
        SAVE_FLOAT_TO_DEBUG_FILE(Pos.y)
        SAVE_STRING_TO_DEBUG_FILE(", ")
        SAVE_FLOAT_TO_DEBUG_FILE(Pos.z)
        SAVE_STRING_TO_DEBUG_FILE(">> ")
        SAVE_NEWLINE_TO_DEBUG_FILE()    
        SAVE_STRING_TO_DEBUG_FILE("DynamicTargetHeading = ")
        SAVE_FLOAT_TO_DEBUG_FILE(heading)
        SAVE_NEWLINE_TO_DEBUG_FILE()    
    CLOSE_DEBUG_FILE () 
ENDPROC

//PURPOSE: Prints the data for the scenario
PROC Print_Scenario_Data (peds_struct &pedsl[], vehicle_struct &vehiclesl[], RouteStruct &routesl[], StCameraData &cams[])
    int index
    int ArraySize
    
    IF gPrintAllIinfo
        ArraySize = COUNT_OF(pedsl)
        
        FOR index = 0 to ArraySize -1
            IF DOES_ENTITY_EXIST(pedsl[index].ped)
                PRINT_PED_INFO_TO_OUTPUTFILE (pedsl[index], index)
            ENDIF
        ENDFOR
        
        ArraySize = COUNT_OF(vehiclesl)
        FOR index = 0 to ArraySize - 1
            IF DOES_ENTITY_EXIST(vehiclesl[index].vehicle)
                PRINT_CAR_INFO_TO_OUTPUTFILE (vehiclesl[index], index)
            ENDIF
        ENDFOR
        
        ArraySize = COUNT_OF(routesl)
        FOR index = 0 to ArraySize - 1
            IF routesl[index].RouteNodeState
                PRINT_ROUTE_POS( routesl[index].RouteNode, index)
            ENDIF
        ENDFOR
        
        ArraySize = COUNT_OF(cams)
            FOR index = 0 to ArraySize - 1
                IF DOES_CAM_EXIST (cams[index].cam)
                    PRINT_CAMERA_INFO (cams[index].cam_pos, cams[index].cam_rot, cams[index].cam_fov)
                ENDIF
            ENDFOR
            
        PRINT_TAREGET_POS (DynamicTargetPos , DynamicTargetHeading)
        
        gPrintAllIinfo = FALSE
    ENDIF
    
    IF Bprint_ped_info
        PRINT_PED_INFO_TO_OUTPUTFILE (pedsl[gTestPedsIndex], gTestPedsIndex)
        Bprint_ped_info = FALSE
    ENDIF
    
    IF Bprint_vehicle_info
        PRINT_CAR_INFO_TO_OUTPUTFILE (vehiclesl[gTestVehicleIndex], gTestVehicleIndex) 
        Bprint_vehicle_info = FALSE
    ENDIF
ENDPROC 

//Test
ENUM Test_Scenario
    InitialiseScenarioData,
    CreateScenarioEntities,
    SetScenarioEntities,
    RunScenario,
    CleanupScenario
ENDENUM

Test_Scenario TestScenarioAStatus = InitialiseScenarioData

INT gcurrentselection = -1      
int gSelection

CONST_INT OneFrameTest              1000

    PROC  Run_Selection_From_XML_input ()
        //unrefnced nonsense
        IF TestScenarioAStatus = InitialiseScenarioData
        ENDIF
        
        IF gSelection > 0
        ENDIF
        
        
        int CurrentScenarioSelection = -1 
    
        CurrentScenarioSelection =  GET_MISSION_XML_VALUE ()
        
        IF  (CurrentScenarioSelection  <> -1) 
            TestScenarioAStatus = CleanupScenario
            gResetToDefault = true
            gCurrentSelection = CurrentScenarioSelection
            gSelection = CurrentScenarioSelection
        ENDIF
    
ENDPROC

PROC Check_For_Scenario_Reset ()
    IF gResetToDefault or gResetCombatScenario
        TestScenarioAStatus = CleanupScenario
    ENDIF
ENDPROC

PROC Set_Test_State_To_Default()
    gSelection = gcurrentselection
    gcurrentselection = OneFrameTest
ENDPROC
