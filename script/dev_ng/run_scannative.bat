cd /d %~dp0

@echo Make sure your RELEASE scripts were cleaned out and then rebuilt, and all C++ code is submitted.
p4 edit %~dp0gta5_files.txt

REM add more special cases here as necessary:
echo %RS_CODEBRANCH%\game\debug\DebugNodes.cpp > %~dp0gta5_files.txt
echo %RS_CODEBRANCH%\rage\script\src\script\thread.cpp >> %~dp0gta5_files.txt
findstr /m SCR_REGISTER_ %RS_CODEBRANCH%\game\script\*.cpp >> %~dp0gta5_files.txt
scannative %~dp0singleplayer\sco\RELEASE\PS4 %~dp0gta5_files.txt
