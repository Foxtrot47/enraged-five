﻿namespace EmailStarPrototype
{
    partial class AddResponse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addingnote = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.threadkiller = new System.Windows.Forms.CheckBox();
            this.firethread = new System.Windows.Forms.CheckBox();
            this.pickthread = new System.Windows.Forms.ComboBox();
            this.selecttag = new System.Windows.Forms.ComboBox();
            this.selectresponse = new System.Windows.Forms.ComboBox();
            this.cancel = new System.Windows.Forms.Button();
            this.add = new System.Windows.Forms.Button();
            this.AutoResponseCheck = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // addingnote
            // 
            this.addingnote.AutoSize = true;
            this.addingnote.Location = new System.Drawing.Point(13, 13);
            this.addingnote.Name = "addingnote";
            this.addingnote.Size = new System.Drawing.Size(82, 13);
            this.addingnote.TabIndex = 0;
            this.addingnote.Text = "Adding to email:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Selection tag (string)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Response Email";
            // 
            // threadkiller
            // 
            this.threadkiller.AutoSize = true;
            this.threadkiller.Location = new System.Drawing.Point(12, 99);
            this.threadkiller.Name = "threadkiller";
            this.threadkiller.Size = new System.Drawing.Size(124, 17);
            this.threadkiller.TabIndex = 3;
            this.threadkiller.Text = "Ends Current Thread";
            this.threadkiller.UseVisualStyleBackColor = true;
            // 
            // firethread
            // 
            this.firethread.AutoSize = true;
            this.firethread.Location = new System.Drawing.Point(12, 122);
            this.firethread.Name = "firethread";
            this.firethread.Size = new System.Drawing.Size(110, 17);
            this.firethread.TabIndex = 4;
            this.firethread.Text = "Fires New Thread";
            this.firethread.UseVisualStyleBackColor = true;
            this.firethread.CheckedChanged += new System.EventHandler(this.firethread_CheckedChanged);
            // 
            // pickthread
            // 
            this.pickthread.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pickthread.Enabled = false;
            this.pickthread.FormattingEnabled = true;
            this.pickthread.Location = new System.Drawing.Point(12, 145);
            this.pickthread.Name = "pickthread";
            this.pickthread.Size = new System.Drawing.Size(121, 21);
            this.pickthread.TabIndex = 5;
            // 
            // selecttag
            // 
            this.selecttag.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.selecttag.FormattingEnabled = true;
            this.selecttag.Location = new System.Drawing.Point(121, 45);
            this.selecttag.Name = "selecttag";
            this.selecttag.Size = new System.Drawing.Size(176, 21);
            this.selecttag.TabIndex = 6;
            // 
            // selectresponse
            // 
            this.selectresponse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.selectresponse.FormattingEnabled = true;
            this.selectresponse.Location = new System.Drawing.Point(121, 72);
            this.selectresponse.Name = "selectresponse";
            this.selectresponse.Size = new System.Drawing.Size(503, 21);
            this.selectresponse.TabIndex = 7;
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(549, 145);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(75, 23);
            this.cancel.TabIndex = 8;
            this.cancel.Text = "Cancel";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // add
            // 
            this.add.Location = new System.Drawing.Point(549, 118);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(75, 23);
            this.add.TabIndex = 9;
            this.add.Text = "Add";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // AutoResponseCheck
            // 
            this.AutoResponseCheck.AutoSize = true;
            this.AutoResponseCheck.Location = new System.Drawing.Point(304, 45);
            this.AutoResponseCheck.Name = "AutoResponseCheck";
            this.AutoResponseCheck.Size = new System.Drawing.Size(99, 17);
            this.AutoResponseCheck.TabIndex = 10;
            this.AutoResponseCheck.Text = "Auto Response";
            this.AutoResponseCheck.UseVisualStyleBackColor = true;
            this.AutoResponseCheck.CheckedChanged += new System.EventHandler(this.AutoResponseCheck_CheckedChanged);
            // 
            // AddResponse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 173);
            this.ControlBox = false;
            this.Controls.Add(this.AutoResponseCheck);
            this.Controls.Add(this.add);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.selectresponse);
            this.Controls.Add(this.selecttag);
            this.Controls.Add(this.pickthread);
            this.Controls.Add(this.firethread);
            this.Controls.Add(this.threadkiller);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.addingnote);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AddResponse";
            this.Text = "AddResponse";
            this.Enter += new System.EventHandler(this.AddResponse_focus);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label addingnote;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox threadkiller;
        private System.Windows.Forms.CheckBox firethread;
        private System.Windows.Forms.ComboBox pickthread;
        private System.Windows.Forms.ComboBox selecttag;
        private System.Windows.Forms.ComboBox selectresponse;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.CheckBox AutoResponseCheck;
    }
}