﻿using System.Windows.Forms;
namespace EmailStarPrototype
{
    partial class DataView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabs = new System.Windows.Forms.TabControl();
            this.stringtab = new System.Windows.Forms.TabPage();
            this.stringtabs = new System.Windows.Forms.TabControl();
            this.nametab = new System.Windows.Forms.TabPage();
            this.namelist = new System.Windows.Forms.ListView();
            this.addresstab = new System.Windows.Forms.TabPage();
            this.addresslist = new System.Windows.Forms.ListView();
            this.signofftab = new System.Windows.Forms.TabPage();
            this.signslist = new System.Windows.Forms.ListView();
            this.emailcontenttab = new System.Windows.Forms.TabPage();
            this.mailcontentlist = new System.Windows.Forms.ListView();
            this.emailtitletab = new System.Windows.Forms.TabPage();
            this.mailtitlelist = new System.Windows.Forms.ListView();
            this.responsetab = new System.Windows.Forms.TabPage();
            this.responselist = new System.Windows.Forms.ListView();
            this.mailertab = new System.Windows.Forms.TabPage();
            this.deletemailer = new System.Windows.Forms.Button();
            this.edmailer = new System.Windows.Forms.Button();
            this.mailerlist = new System.Windows.Forms.ListView();
            this.emailtab = new System.Windows.Forms.TabPage();
            this.deleteemail = new System.Windows.Forms.Button();
            this.edmails = new System.Windows.Forms.Button();
            this.emailslist = new System.Windows.Forms.ListView();
            this.threadtab = new System.Windows.Forms.TabPage();
            this.prevthread = new System.Windows.Forms.Button();
            this.deletethreads = new System.Windows.Forms.Button();
            this.edthread = new System.Windows.Forms.Button();
            this.threadlist = new System.Windows.Forms.ListView();
            this.edthreditor = new System.Windows.Forms.Button();
            this.tabs.SuspendLayout();
            this.stringtab.SuspendLayout();
            this.stringtabs.SuspendLayout();
            this.nametab.SuspendLayout();
            this.addresstab.SuspendLayout();
            this.signofftab.SuspendLayout();
            this.emailcontenttab.SuspendLayout();
            this.emailtitletab.SuspendLayout();
            this.responsetab.SuspendLayout();
            this.mailertab.SuspendLayout();
            this.emailtab.SuspendLayout();
            this.threadtab.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.stringtab);
            this.tabs.Controls.Add(this.mailertab);
            this.tabs.Controls.Add(this.emailtab);
            this.tabs.Controls.Add(this.threadtab);
            this.tabs.Location = new System.Drawing.Point(2, 2);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(711, 416);
            this.tabs.TabIndex = 0;
            // 
            // stringtab
            // 
            this.stringtab.Controls.Add(this.stringtabs);
            this.stringtab.Location = new System.Drawing.Point(4, 22);
            this.stringtab.Name = "stringtab";
            this.stringtab.Padding = new System.Windows.Forms.Padding(3);
            this.stringtab.Size = new System.Drawing.Size(703, 390);
            this.stringtab.TabIndex = 0;
            this.stringtab.Text = "Strings";
            this.stringtab.UseVisualStyleBackColor = true;
            // 
            // stringtabs
            // 
            this.stringtabs.Controls.Add(this.nametab);
            this.stringtabs.Controls.Add(this.addresstab);
            this.stringtabs.Controls.Add(this.signofftab);
            this.stringtabs.Controls.Add(this.emailcontenttab);
            this.stringtabs.Controls.Add(this.emailtitletab);
            this.stringtabs.Controls.Add(this.responsetab);
            this.stringtabs.Location = new System.Drawing.Point(0, 0);
            this.stringtabs.Name = "stringtabs";
            this.stringtabs.SelectedIndex = 0;
            this.stringtabs.Size = new System.Drawing.Size(703, 390);
            this.stringtabs.TabIndex = 0;
            // 
            // nametab
            // 
            this.nametab.Controls.Add(this.namelist);
            this.nametab.Location = new System.Drawing.Point(4, 22);
            this.nametab.Name = "nametab";
            this.nametab.Padding = new System.Windows.Forms.Padding(3);
            this.nametab.Size = new System.Drawing.Size(695, 364);
            this.nametab.TabIndex = 0;
            this.nametab.Text = "Names";
            this.nametab.UseVisualStyleBackColor = true;
            // 
            // namelist
            // 
            this.namelist.Location = new System.Drawing.Point(4, 3);
            this.namelist.Name = "namelist";
            this.namelist.Size = new System.Drawing.Size(685, 354);
            this.namelist.TabIndex = 0;
            this.namelist.UseCompatibleStateImageBehavior = false;
            // 
            // addresstab
            // 
            this.addresstab.Controls.Add(this.addresslist);
            this.addresstab.Location = new System.Drawing.Point(4, 22);
            this.addresstab.Name = "addresstab";
            this.addresstab.Padding = new System.Windows.Forms.Padding(3);
            this.addresstab.Size = new System.Drawing.Size(695, 364);
            this.addresstab.TabIndex = 1;
            this.addresstab.Text = "Adresses";
            this.addresstab.UseVisualStyleBackColor = true;
            // 
            // addresslist
            // 
            this.addresslist.Location = new System.Drawing.Point(5, 4);
            this.addresslist.Name = "addresslist";
            this.addresslist.Size = new System.Drawing.Size(690, 354);
            this.addresslist.TabIndex = 0;
            this.addresslist.UseCompatibleStateImageBehavior = false;
            // 
            // signofftab
            // 
            this.signofftab.Controls.Add(this.signslist);
            this.signofftab.Location = new System.Drawing.Point(4, 22);
            this.signofftab.Name = "signofftab";
            this.signofftab.Size = new System.Drawing.Size(695, 364);
            this.signofftab.TabIndex = 2;
            this.signofftab.Text = "Signoffs";
            this.signofftab.UseVisualStyleBackColor = true;
            // 
            // signslist
            // 
            this.signslist.Location = new System.Drawing.Point(4, 4);
            this.signslist.Name = "signslist";
            this.signslist.Size = new System.Drawing.Size(688, 357);
            this.signslist.TabIndex = 0;
            this.signslist.UseCompatibleStateImageBehavior = false;
            // 
            // emailcontenttab
            // 
            this.emailcontenttab.Controls.Add(this.mailcontentlist);
            this.emailcontenttab.Location = new System.Drawing.Point(4, 22);
            this.emailcontenttab.Name = "emailcontenttab";
            this.emailcontenttab.Size = new System.Drawing.Size(695, 364);
            this.emailcontenttab.TabIndex = 3;
            this.emailcontenttab.Text = "Mail Contents";
            this.emailcontenttab.UseVisualStyleBackColor = true;
            // 
            // mailcontentlist
            // 
            this.mailcontentlist.Location = new System.Drawing.Point(4, 4);
            this.mailcontentlist.Name = "mailcontentlist";
            this.mailcontentlist.Size = new System.Drawing.Size(688, 357);
            this.mailcontentlist.TabIndex = 0;
            this.mailcontentlist.UseCompatibleStateImageBehavior = false;
            // 
            // emailtitletab
            // 
            this.emailtitletab.Controls.Add(this.mailtitlelist);
            this.emailtitletab.Location = new System.Drawing.Point(4, 22);
            this.emailtitletab.Name = "emailtitletab";
            this.emailtitletab.Size = new System.Drawing.Size(695, 364);
            this.emailtitletab.TabIndex = 4;
            this.emailtitletab.Text = "Mail Titles";
            this.emailtitletab.UseVisualStyleBackColor = true;
            // 
            // mailtitlelist
            // 
            this.mailtitlelist.Location = new System.Drawing.Point(4, 4);
            this.mailtitlelist.Name = "mailtitlelist";
            this.mailtitlelist.Size = new System.Drawing.Size(688, 358);
            this.mailtitlelist.TabIndex = 0;
            this.mailtitlelist.UseCompatibleStateImageBehavior = false;
            // 
            // responsetab
            // 
            this.responsetab.Controls.Add(this.responselist);
            this.responsetab.Location = new System.Drawing.Point(4, 22);
            this.responsetab.Name = "responsetab";
            this.responsetab.Size = new System.Drawing.Size(695, 364);
            this.responsetab.TabIndex = 5;
            this.responsetab.Text = "Responses";
            this.responsetab.UseVisualStyleBackColor = true;
            // 
            // responselist
            // 
            this.responselist.Location = new System.Drawing.Point(4, 4);
            this.responselist.Name = "responselist";
            this.responselist.Size = new System.Drawing.Size(688, 357);
            this.responselist.TabIndex = 0;
            this.responselist.UseCompatibleStateImageBehavior = false;
            // 
            // mailertab
            // 
            this.mailertab.Controls.Add(this.deletemailer);
            this.mailertab.Controls.Add(this.edmailer);
            this.mailertab.Controls.Add(this.mailerlist);
            this.mailertab.Location = new System.Drawing.Point(4, 22);
            this.mailertab.Name = "mailertab";
            this.mailertab.Padding = new System.Windows.Forms.Padding(3);
            this.mailertab.Size = new System.Drawing.Size(703, 390);
            this.mailertab.TabIndex = 1;
            this.mailertab.Text = "Mailers";
            this.mailertab.UseVisualStyleBackColor = true;
            // 
            // deletemailer
            // 
            this.deletemailer.Location = new System.Drawing.Point(618, 360);
            this.deletemailer.Name = "deletemailer";
            this.deletemailer.Size = new System.Drawing.Size(78, 23);
            this.deletemailer.TabIndex = 2;
            this.deletemailer.Text = "Delete Mailer";
            this.deletemailer.UseVisualStyleBackColor = true;
            this.deletemailer.Click += new System.EventHandler(this.deletemailer_Click);
            // 
            // edmailer
            // 
            this.edmailer.Location = new System.Drawing.Point(7, 364);
            this.edmailer.Name = "edmailer";
            this.edmailer.Size = new System.Drawing.Size(75, 23);
            this.edmailer.TabIndex = 1;
            this.edmailer.Text = "Edit Mailer";
            this.edmailer.UseVisualStyleBackColor = true;
            this.edmailer.Click += new System.EventHandler(this.edmailer_Click);
            // 
            // mailerlist
            // 
            this.mailerlist.Location = new System.Drawing.Point(4, 3);
            this.mailerlist.Name = "mailerlist";
            this.mailerlist.Size = new System.Drawing.Size(692, 354);
            this.mailerlist.TabIndex = 0;
            this.mailerlist.UseCompatibleStateImageBehavior = false;
            // 
            // emailtab
            // 
            this.emailtab.Controls.Add(this.deleteemail);
            this.emailtab.Controls.Add(this.edmails);
            this.emailtab.Controls.Add(this.emailslist);
            this.emailtab.Location = new System.Drawing.Point(4, 22);
            this.emailtab.Name = "emailtab";
            this.emailtab.Size = new System.Drawing.Size(703, 390);
            this.emailtab.TabIndex = 2;
            this.emailtab.Text = "Emails";
            this.emailtab.UseVisualStyleBackColor = true;
            // 
            // deleteemail
            // 
            this.deleteemail.Location = new System.Drawing.Point(614, 360);
            this.deleteemail.Name = "deleteemail";
            this.deleteemail.Size = new System.Drawing.Size(82, 23);
            this.deleteemail.TabIndex = 2;
            this.deleteemail.Text = "Delete Email";
            this.deleteemail.UseVisualStyleBackColor = true;
            this.deleteemail.Click += new System.EventHandler(this.deleteemail_Click);
            // 
            // edmails
            // 
            this.edmails.Location = new System.Drawing.Point(7, 360);
            this.edmails.Name = "edmails";
            this.edmails.Size = new System.Drawing.Size(75, 23);
            this.edmails.TabIndex = 1;
            this.edmails.Text = "Edit Email";
            this.edmails.UseVisualStyleBackColor = true;
            this.edmails.Click += new System.EventHandler(this.edmails_Click);
            // 
            // emailslist
            // 
            this.emailslist.Location = new System.Drawing.Point(3, 4);
            this.emailslist.Name = "emailslist";
            this.emailslist.Size = new System.Drawing.Size(697, 350);
            this.emailslist.TabIndex = 0;
            this.emailslist.UseCompatibleStateImageBehavior = false;
            // 
            // threadtab
            // 
            this.threadtab.Controls.Add(this.edthreditor);
            this.threadtab.Controls.Add(this.prevthread);
            this.threadtab.Controls.Add(this.deletethreads);
            this.threadtab.Controls.Add(this.edthread);
            this.threadtab.Controls.Add(this.threadlist);
            this.threadtab.Location = new System.Drawing.Point(4, 22);
            this.threadtab.Name = "threadtab";
            this.threadtab.Size = new System.Drawing.Size(703, 390);
            this.threadtab.TabIndex = 3;
            this.threadtab.Text = "Threads";
            this.threadtab.UseVisualStyleBackColor = true;
            // 
            // prevthread
            // 
            this.prevthread.Location = new System.Drawing.Point(287, 360);
            this.prevthread.Name = "prevthread";
            this.prevthread.Size = new System.Drawing.Size(127, 23);
            this.prevthread.TabIndex = 3;
            this.prevthread.Text = "Preview thread!";
            this.prevthread.UseVisualStyleBackColor = true;
            this.prevthread.Click += new System.EventHandler(this.prevthread_Click);
            // 
            // deletethreads
            // 
            this.deletethreads.Location = new System.Drawing.Point(603, 360);
            this.deletethreads.Name = "deletethreads";
            this.deletethreads.Size = new System.Drawing.Size(93, 23);
            this.deletethreads.TabIndex = 2;
            this.deletethreads.Text = "Delete Threads";
            this.deletethreads.UseVisualStyleBackColor = true;
            this.deletethreads.Click += new System.EventHandler(this.deletethreads_Click);
            // 
            // edthread
            // 
            this.edthread.Location = new System.Drawing.Point(7, 360);
            this.edthread.Name = "edthread";
            this.edthread.Size = new System.Drawing.Size(75, 23);
            this.edthread.TabIndex = 1;
            this.edthread.Text = "Edit Thread";
            this.edthread.UseVisualStyleBackColor = true;
            this.edthread.Click += new System.EventHandler(this.edthread_Click);
            // 
            // threadlist
            // 
            this.threadlist.Location = new System.Drawing.Point(3, 4);
            this.threadlist.Name = "threadlist";
            this.threadlist.Size = new System.Drawing.Size(697, 350);
            this.threadlist.TabIndex = 0;
            this.threadlist.UseCompatibleStateImageBehavior = false;
            // 
            // edthreditor
            // 
            this.edthreditor.Location = new System.Drawing.Point(88, 360);
            this.edthreditor.Name = "edthreditor";
            this.edthreditor.Size = new System.Drawing.Size(157, 23);
            this.edthreditor.TabIndex = 4;
            this.edthreditor.Text = "Edit Thread (with Threditor!)";
            this.edthreditor.UseVisualStyleBackColor = true;
            this.edthreditor.Click += new System.EventHandler(this.edthreditor_Click);
            // 
            // DataView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 419);
            this.Controls.Add(this.tabs);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DataView";
            this.Text = "DataView";
            this.Enter += new System.EventHandler(this.DataView_onenter);
            this.tabs.ResumeLayout(false);
            this.stringtab.ResumeLayout(false);
            this.stringtabs.ResumeLayout(false);
            this.nametab.ResumeLayout(false);
            this.addresstab.ResumeLayout(false);
            this.signofftab.ResumeLayout(false);
            this.emailcontenttab.ResumeLayout(false);
            this.emailtitletab.ResumeLayout(false);
            this.responsetab.ResumeLayout(false);
            this.mailertab.ResumeLayout(false);
            this.emailtab.ResumeLayout(false);
            this.threadtab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage stringtab;
        private System.Windows.Forms.TabPage mailertab;
        private System.Windows.Forms.TabPage emailtab;
        private System.Windows.Forms.TabPage threadtab;
        private System.Windows.Forms.TabControl stringtabs;
        private System.Windows.Forms.TabPage nametab;
        private System.Windows.Forms.TabPage addresstab;
        private System.Windows.Forms.TabPage signofftab;
        private System.Windows.Forms.TabPage emailcontenttab;
        private System.Windows.Forms.TabPage emailtitletab;
        private System.Windows.Forms.TabPage responsetab;
        private System.Windows.Forms.ListView mailerlist;
        private System.Windows.Forms.ListView emailslist;
        private System.Windows.Forms.ListView threadlist;
        private System.Windows.Forms.ListView addresslist;
        private System.Windows.Forms.ListView signslist;
        private System.Windows.Forms.ListView mailcontentlist;
        private System.Windows.Forms.ListView mailtitlelist;
        private System.Windows.Forms.ListView responselist;
        private System.Windows.Forms.ListView namelist;
        private Button edmailer;
        private Button edmails;
        private Button edthread;
        private Button deletemailer;
        private Button deleteemail;
        private Button deletethreads;
        private Button prevthread;
        private Button edthreditor;
    }
}