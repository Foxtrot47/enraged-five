﻿namespace EmailStarPrototype
{
    partial class AddString
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addbutton = new System.Windows.Forms.Button();
            this.cancelbutton = new System.Windows.Forms.Button();
            this.stringcontents = new System.Windows.Forms.TextBox();
            this.pickcategory = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.keepOpen = new System.Windows.Forms.CheckBox();
            this.AutoRe = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // addbutton
            // 
            this.addbutton.Location = new System.Drawing.Point(346, 38);
            this.addbutton.Name = "addbutton";
            this.addbutton.Size = new System.Drawing.Size(75, 23);
            this.addbutton.TabIndex = 0;
            this.addbutton.Text = "Add";
            this.addbutton.UseVisualStyleBackColor = true;
            this.addbutton.Click += new System.EventHandler(this.addbutton_Click);
            // 
            // cancelbutton
            // 
            this.cancelbutton.Location = new System.Drawing.Point(427, 38);
            this.cancelbutton.Name = "cancelbutton";
            this.cancelbutton.Size = new System.Drawing.Size(75, 23);
            this.cancelbutton.TabIndex = 1;
            this.cancelbutton.Text = "Cancel";
            this.cancelbutton.UseVisualStyleBackColor = true;
            this.cancelbutton.Click += new System.EventHandler(this.cancelbutton_Click);
            // 
            // stringcontents
            // 
            this.stringcontents.Location = new System.Drawing.Point(2, 12);
            this.stringcontents.Name = "stringcontents";
            this.stringcontents.Size = new System.Drawing.Size(500, 20);
            this.stringcontents.TabIndex = 2;
            //
            this.stringcontents.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.returnbind);
            // 
            // pickcategory
            // 
            this.pickcategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pickcategory.FormattingEnabled = true;
            this.pickcategory.Items.AddRange(new object[] {
            "Emailer Name",
            "Emailer Address",
            "Emailer Signoff",
            "Email Content",
            "Email Title",
            "Response Title"});
            this.pickcategory.Location = new System.Drawing.Point(12, 58);
            this.pickcategory.Name = "pickcategory";
            this.pickcategory.Size = new System.Drawing.Size(154, 21);
            this.pickcategory.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Pick Category";
            // 
            // keepOpen
            // 
            this.keepOpen.AutoSize = true;
            this.keepOpen.Location = new System.Drawing.Point(374, 67);
            this.keepOpen.Name = "keepOpen";
            this.keepOpen.Size = new System.Drawing.Size(128, 17);
            this.keepOpen.TabIndex = 5;
            this.keepOpen.Text = "Keep open after entry";
            this.keepOpen.UseVisualStyleBackColor = true;
            // 
            // AutoRe
            // 
            this.AutoRe.AutoSize = true;
            this.AutoRe.Location = new System.Drawing.Point(183, 67);
            this.AutoRe.Name = "AutoRe";
            this.AutoRe.Size = new System.Drawing.Size(146, 17);
            this.AutoRe.TabIndex = 6;
            this.AutoRe.Text = "Auto add \"RE:\" duplicate";
            this.AutoRe.UseVisualStyleBackColor = true;
            // 
            // AddString
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 91);
            this.ControlBox = false;
            this.Controls.Add(this.AutoRe);
            this.Controls.Add(this.keepOpen);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pickcategory);
            this.Controls.Add(this.stringcontents);
            this.Controls.Add(this.cancelbutton);
            this.Controls.Add(this.addbutton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AddString";
            this.Text = "AddString";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addbutton;
        private System.Windows.Forms.Button cancelbutton;
        private System.Windows.Forms.TextBox stringcontents;
        private System.Windows.Forms.ComboBox pickcategory;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox keepOpen;
        private System.Windows.Forms.CheckBox AutoRe;
    }
}