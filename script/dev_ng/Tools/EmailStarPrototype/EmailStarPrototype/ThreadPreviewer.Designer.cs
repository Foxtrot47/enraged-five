﻿namespace EmailStarPrototype
{
    partial class ThreadPreviewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.responsebox = new System.Windows.Forms.ComboBox();
            this.responsepick = new System.Windows.Forms.Button();
            this.emailcontent = new System.Windows.Forms.TextBox();
            this.selectemailertoview = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.recmails = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.resetpreview = new System.Windows.Forms.Button();
            this.forceduration = new System.Windows.Forms.Button();
            this.blockedornot = new System.Windows.Forms.Label();
            this.stagestate = new System.Windows.Forms.Label();
            this.threadstatus = new System.Windows.Forms.Label();
            this.actionlog = new System.Windows.Forms.ListBox();
            this.delayamount = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.responsebox);
            this.groupBox1.Controls.Add(this.responsepick);
            this.groupBox1.Controls.Add(this.emailcontent);
            this.groupBox1.Controls.Add(this.selectemailertoview);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.recmails);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(564, 335);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Preview Inbox";
            // 
            // responsebox
            // 
            this.responsebox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.responsebox.FormattingEnabled = true;
            this.responsebox.Location = new System.Drawing.Point(327, 308);
            this.responsebox.Name = "responsebox";
            this.responsebox.Size = new System.Drawing.Size(137, 21);
            this.responsebox.TabIndex = 5;
            // 
            // responsepick
            // 
            this.responsepick.Location = new System.Drawing.Point(470, 306);
            this.responsepick.Name = "responsepick";
            this.responsepick.Size = new System.Drawing.Size(88, 23);
            this.responsepick.TabIndex = 3;
            this.responsepick.Text = "Pick Response";
            this.responsepick.UseVisualStyleBackColor = true;
            this.responsepick.Click += new System.EventHandler(this.responsepick_Click);
            // 
            // emailcontent
            // 
            this.emailcontent.Location = new System.Drawing.Point(132, 12);
            this.emailcontent.Multiline = true;
            this.emailcontent.Name = "emailcontent";
            this.emailcontent.ReadOnly = true;
            this.emailcontent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.emailcontent.Size = new System.Drawing.Size(426, 288);
            this.emailcontent.TabIndex = 2;
            // 
            // selectemailertoview
            // 
            this.selectemailertoview.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.selectemailertoview.FormattingEnabled = true;
            this.selectemailertoview.Location = new System.Drawing.Point(10, 19);
            this.selectemailertoview.Name = "selectemailertoview";
            this.selectemailertoview.Size = new System.Drawing.Size(116, 21);
            this.selectemailertoview.TabIndex = 3;
            this.selectemailertoview.SelectedIndexChanged += new System.EventHandler(this.selectemailertoview_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Recieved Mails";
            // 
            // recmails
            // 
            this.recmails.FormattingEnabled = true;
            this.recmails.Location = new System.Drawing.Point(6, 64);
            this.recmails.Name = "recmails";
            this.recmails.Size = new System.Drawing.Size(120, 264);
            this.recmails.TabIndex = 0;
            this.recmails.SelectedIndexChanged += new System.EventHandler(this.recmails_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.delayamount);
            this.groupBox2.Controls.Add(this.resetpreview);
            this.groupBox2.Controls.Add(this.forceduration);
            this.groupBox2.Controls.Add(this.blockedornot);
            this.groupBox2.Controls.Add(this.stagestate);
            this.groupBox2.Location = new System.Drawing.Point(583, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(199, 336);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thread Control";
            // 
            // resetpreview
            // 
            this.resetpreview.Location = new System.Drawing.Point(10, 36);
            this.resetpreview.Name = "resetpreview";
            this.resetpreview.Size = new System.Drawing.Size(62, 36);
            this.resetpreview.TabIndex = 6;
            this.resetpreview.Text = "Reset Preview";
            this.resetpreview.UseVisualStyleBackColor = true;
            this.resetpreview.Click += new System.EventHandler(this.resetpreview_Click);
            // 
            // forceduration
            // 
            this.forceduration.Location = new System.Drawing.Point(6, 100);
            this.forceduration.Name = "forceduration";
            this.forceduration.Size = new System.Drawing.Size(79, 36);
            this.forceduration.TabIndex = 2;
            this.forceduration.Text = "Step Past Duration";
            this.forceduration.UseVisualStyleBackColor = true;
            this.forceduration.Click += new System.EventHandler(this.forceduration_Click);
            // 
            // blockedornot
            // 
            this.blockedornot.AutoSize = true;
            this.blockedornot.Location = new System.Drawing.Point(7, 84);
            this.blockedornot.Name = "blockedornot";
            this.blockedornot.Size = new System.Drawing.Size(55, 13);
            this.blockedornot.TabIndex = 1;
            this.blockedornot.Text = "Blocked?!";
            // 
            // stagestate
            // 
            this.stagestate.AutoSize = true;
            this.stagestate.Location = new System.Drawing.Point(7, 16);
            this.stagestate.Name = "stagestate";
            this.stagestate.Size = new System.Drawing.Size(80, 13);
            this.stagestate.TabIndex = 0;
            this.stagestate.Text = "At Stage X of X";
            // 
            // threadstatus
            // 
            this.threadstatus.AutoSize = true;
            this.threadstatus.Location = new System.Drawing.Point(16, 353);
            this.threadstatus.Name = "threadstatus";
            this.threadstatus.Size = new System.Drawing.Size(216, 13);
            this.threadstatus.TabIndex = 5;
            this.threadstatus.Text = "Thread at stage X, awaiting response from X";
            // 
            // actionlog
            // 
            this.actionlog.FormattingEnabled = true;
            this.actionlog.Location = new System.Drawing.Point(13, 369);
            this.actionlog.Name = "actionlog";
            this.actionlog.Size = new System.Drawing.Size(769, 199);
            this.actionlog.TabIndex = 6;
            // 
            // delayamount
            // 
            this.delayamount.AutoSize = true;
            this.delayamount.Location = new System.Drawing.Point(10, 155);
            this.delayamount.Name = "delayamount";
            this.delayamount.Size = new System.Drawing.Size(34, 13);
            this.delayamount.TabIndex = 7;
            this.delayamount.Text = "Delay";
            // 
            // ThreadPreviewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 575);
            this.Controls.Add(this.actionlog);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.threadstatus);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ThreadPreviewer";
            this.Text = "ThreadPreviewer";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox recmails;
        private System.Windows.Forms.TextBox emailcontent;
        private System.Windows.Forms.Button forceduration;
        private System.Windows.Forms.Label blockedornot;
        private System.Windows.Forms.Label stagestate;
        private System.Windows.Forms.ComboBox selectemailertoview;
        private System.Windows.Forms.Button responsepick;
        private System.Windows.Forms.ComboBox responsebox;
        private System.Windows.Forms.Label threadstatus;
        private System.Windows.Forms.Button resetpreview;
        private System.Windows.Forms.ListBox actionlog;
        private System.Windows.Forms.Label delayamount;
    }
}