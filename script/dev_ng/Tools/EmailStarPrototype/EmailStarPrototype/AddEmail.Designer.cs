﻿namespace EmailStarPrototype
{
    partial class AddEmail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.blocks = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.fromlist = new System.Windows.Forms.ComboBox();
            this.tolist = new System.Windows.Forms.ComboBox();
            this.titlelist = new System.Windows.Forms.ComboBox();
            this.contentlist = new System.Windows.Forms.ComboBox();
            this.responsesadded = new System.Windows.Forms.ListBox();
            this.add = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.respadd = new System.Windows.Forms.Button();
            this.delaybox = new System.Windows.Forms.TextBox();
            this.removeresp = new System.Windows.Forms.Button();
            this.editresp = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Title (string)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Content (string)";
            // 
            // blocks
            // 
            this.blocks.AutoSize = true;
            this.blocks.Location = new System.Drawing.Point(10, 247);
            this.blocks.Name = "blocks";
            this.blocks.Size = new System.Drawing.Size(90, 17);
            this.blocks.TabIndex = 2;
            this.blocks.Text = "blocks thread";
            this.blocks.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(178, 251);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Thread Delay Amount(ms)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Responses";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "From (emailer)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "To (emailer)";
            // 
            // fromlist
            // 
            this.fromlist.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fromlist.FormattingEnabled = true;
            this.fromlist.Location = new System.Drawing.Point(92, 13);
            this.fromlist.Name = "fromlist";
            this.fromlist.Size = new System.Drawing.Size(361, 21);
            this.fromlist.TabIndex = 7;
            // 
            // tolist
            // 
            this.tolist.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tolist.FormattingEnabled = true;
            this.tolist.Location = new System.Drawing.Point(92, 40);
            this.tolist.Name = "tolist";
            this.tolist.Size = new System.Drawing.Size(361, 21);
            this.tolist.TabIndex = 8;
            // 
            // titlelist
            // 
            this.titlelist.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.titlelist.FormattingEnabled = true;
            this.titlelist.Location = new System.Drawing.Point(92, 71);
            this.titlelist.Name = "titlelist";
            this.titlelist.Size = new System.Drawing.Size(361, 21);
            this.titlelist.TabIndex = 9;
            // 
            // contentlist
            // 
            this.contentlist.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.contentlist.FormattingEnabled = true;
            this.contentlist.Location = new System.Drawing.Point(92, 98);
            this.contentlist.Name = "contentlist";
            this.contentlist.Size = new System.Drawing.Size(361, 21);
            this.contentlist.TabIndex = 10;
            // 
            // responsesadded
            // 
            this.responsesadded.FormattingEnabled = true;
            this.responsesadded.Location = new System.Drawing.Point(18, 138);
            this.responsesadded.Name = "responsesadded";
            this.responsesadded.Size = new System.Drawing.Size(435, 69);
            this.responsesadded.TabIndex = 11;
            // 
            // add
            // 
            this.add.Location = new System.Drawing.Point(378, 213);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(75, 23);
            this.add.TabIndex = 12;
            this.add.Text = "Add";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(378, 243);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(75, 23);
            this.cancel.TabIndex = 13;
            this.cancel.Text = "Cancel";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // respadd
            // 
            this.respadd.Location = new System.Drawing.Point(18, 213);
            this.respadd.Name = "respadd";
            this.respadd.Size = new System.Drawing.Size(88, 23);
            this.respadd.TabIndex = 14;
            this.respadd.Text = "Add Response";
            this.respadd.UseVisualStyleBackColor = true;
            this.respadd.Click += new System.EventHandler(this.respadd_Click);
            // 
            // delaybox
            // 
            this.delaybox.Location = new System.Drawing.Point(125, 244);
            this.delaybox.Name = "delaybox";
            this.delaybox.Size = new System.Drawing.Size(47, 20);
            this.delaybox.TabIndex = 17;
            // 
            // removeresp
            // 
            this.removeresp.Location = new System.Drawing.Point(206, 213);
            this.removeresp.Name = "removeresp";
            this.removeresp.Size = new System.Drawing.Size(106, 23);
            this.removeresp.TabIndex = 18;
            this.removeresp.Text = "Remove Response";
            this.removeresp.UseVisualStyleBackColor = true;
            this.removeresp.Click += new System.EventHandler(this.removeresp_Click);
            // 
            // editresp
            // 
            this.editresp.Location = new System.Drawing.Point(112, 213);
            this.editresp.Name = "editresp";
            this.editresp.Size = new System.Drawing.Size(88, 23);
            this.editresp.TabIndex = 19;
            this.editresp.Text = "Edit Response";
            this.editresp.UseVisualStyleBackColor = true;
            this.editresp.Click += new System.EventHandler(this.editresp_Click);
            // 
            // AddEmail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 273);
            this.ControlBox = false;
            this.Controls.Add(this.editresp);
            this.Controls.Add(this.removeresp);
            this.Controls.Add(this.delaybox);
            this.Controls.Add(this.respadd);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.add);
            this.Controls.Add(this.responsesadded);
            this.Controls.Add(this.contentlist);
            this.Controls.Add(this.titlelist);
            this.Controls.Add(this.tolist);
            this.Controls.Add(this.fromlist);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.blocks);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AddEmail";
            this.Text = "AddEmail";
            this.Enter += new System.EventHandler(this.AddEmail_focus);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox blocks;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox fromlist;
        private System.Windows.Forms.ComboBox tolist;
        private System.Windows.Forms.ComboBox titlelist;
        private System.Windows.Forms.ComboBox contentlist;
        private System.Windows.Forms.ListBox responsesadded;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Button respadd;
        private System.Windows.Forms.TextBox delaybox;
        private System.Windows.Forms.Button removeresp;
        private System.Windows.Forms.Button editresp;

    }
}