﻿using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;


namespace EmailStarPrototype
{
    /// <summary>
    /// Very hacky prototype for the email editor
    /// 
    /// Saving data to csv for reload goes here
    /// </summary>
    public sealed partial class EmailStar
    {

        public static void LoadSystem(string filepath)
        {
            myInstance.mLoadSystem(filepath);
        }

        void mLoadSystem(string filepath)
        {
            //MessageBox.Show(filepath);

            StreamReader r = new StreamReader(filepath);
            string massdump = r.ReadToEnd();
            r.Close();

            string [] nlsplit = massdump.Split('\n');
            int ReadIndex = 0;//nlsplit.Length;
            int StringsToRead = 0;

            //List<EmailSystemString> SystemStringsEmailerName;
            //int total -> csvs
            StringsToRead = int.Parse(nlsplit[ReadIndex]);
            ++ReadIndex;
            SystemStringsEmailerName.Clear();
            for (int i = 0; i < StringsToRead; ++i)
            {
                EmailSystemString ess = new EmailSystemString(nlsplit[ReadIndex]);
                SystemStringsEmailerName.Add(ess);
                ++ReadIndex;
            }


            //List<EmailSystemString> SystemStringsEmailerAddress;
            //int total -> csvs
            StringsToRead = int.Parse(nlsplit[ReadIndex]);
            ++ReadIndex;
            SystemStringsEmailerAddress.Clear();
            for (int i = 0; i < StringsToRead; ++i)
            {
                EmailSystemString ess = new EmailSystemString(nlsplit[ReadIndex]);
                SystemStringsEmailerAddress.Add(ess);
                ++ReadIndex;
            }

            //List<EmailSystemString> SystemStringsEmailerSignoff;
            //int total -> csvs
            StringsToRead = int.Parse(nlsplit[ReadIndex]);
            ++ReadIndex;
            SystemStringsEmailerSignoff.Clear();
            for (int i = 0; i < StringsToRead; ++i)
            {
                EmailSystemString ess = new EmailSystemString(nlsplit[ReadIndex]);
                SystemStringsEmailerSignoff.Add(ess);
                ++ReadIndex;
            }
            //List<EmailSystemString> SystemStringsEmailContent;
            //int total -> csvs
            StringsToRead = int.Parse(nlsplit[ReadIndex]);
            ++ReadIndex;
            SystemStringsEmailContent.Clear();
            for (int i = 0; i < StringsToRead; ++i)
            {
                EmailSystemString ess = new EmailSystemString(nlsplit[ReadIndex]);
                SystemStringsEmailContent.Add(ess);
                ++ReadIndex;
            }
            //List<EmailSystemString> SystemStringsEmailTitle;
            //int total -> csvs
            StringsToRead = int.Parse(nlsplit[ReadIndex]);
            ++ReadIndex;
            SystemStringsEmailTitle.Clear();
            for (int i = 0; i < StringsToRead; ++i)
            {
                EmailSystemString ess = new EmailSystemString(nlsplit[ReadIndex]);
                SystemStringsEmailTitle.Add(ess);
                ++ReadIndex;
            }
            //List<EmailSystemString> SystemStringsResponseTitle;
            //int total -> csvs
            StringsToRead = int.Parse(nlsplit[ReadIndex]);
            ++ReadIndex;
            SystemStringsResponseTitle.Clear();
            for (int i = 0; i < StringsToRead; ++i)
            {
                EmailSystemString ess = new EmailSystemString(nlsplit[ReadIndex]);
                SystemStringsResponseTitle.Add(ess);
                ++ReadIndex;
            }
            
            //read mailer indices and unpack them into strings
            //public EmailSystemString sMailerName;
            //public EmailSystemString sMailerAddress;
            //public EmailSystemString sMailerSignoff;

            int MailersToRead = int.Parse(nlsplit[ReadIndex]);
            ++ReadIndex;
            SystemMailers.Clear();
            for (int i = 0; i < MailersToRead; ++i)
            {
                Emailer em = new Emailer();

                em.sMailerName = SystemStringsEmailerName[int.Parse(nlsplit[ReadIndex])];
                ++ReadIndex;
                em.sMailerAddress = SystemStringsEmailerAddress[int.Parse(nlsplit[ReadIndex])];
                ++ReadIndex;
                if (nlsplit[ReadIndex] != "")
                {
                    int signoffindex = int.Parse(nlsplit[ReadIndex]);
                    if (signoffindex != -1)
                    {
                        em.sMailerSignoff = SystemStringsEmailerSignoff[signoffindex];
                    }
                }
                ++ReadIndex;

                SystemMailers.Add(em);
            }

            //now load first bits of thread data: the tags and the participants
            //mail thread ordering data is loaded after the emails themselves are loaded
            SystemThreads.Clear();
            int ThreadsToRead = int.Parse(nlsplit[ReadIndex]);
            ++ReadIndex;
            for (int i = 0; i < ThreadsToRead; ++i)
            {
                Thread threadtosave = new Thread();
                //read the tag, number of participants then participant list
                threadtosave.sThreadInternalTag = nlsplit[ReadIndex];
                ++ReadIndex;

                int numberofparticipants = int.Parse(nlsplit[ReadIndex]);
                ++ReadIndex;
                //threadtosave.SetThreadData(   

                List<Emailer> partlist = new List<Emailer>();

                for (int j = 0; j < numberofparticipants; ++j)
                {
                    partlist.Add(SystemMailers[int.Parse(nlsplit[ReadIndex])]);
                         ++ReadIndex;
                }

                threadtosave.SetThreadData(partlist);
                SystemThreads.Add(threadtosave);
            }

            //now load all the emails and responses
            int EmailsToRead = int.Parse(nlsplit[ReadIndex]);
            ++ReadIndex;
            SystemEmails.Clear();
            for (int i = 0; i < EmailsToRead; ++i)
            {
                Email emailtoadd = new Email();
                //public Emailer From;//int
                emailtoadd.From = SystemMailers[int.Parse(nlsplit[ReadIndex])];
                ++ReadIndex;
                //public Emailer To;//int
                emailtoadd.To = SystemMailers[int.Parse(nlsplit[ReadIndex])];
                ++ReadIndex;

                // public EmailSystemString sTitle;//int
                emailtoadd.sTitle = SystemStringsEmailTitle[int.Parse(nlsplit[ReadIndex])];
                ++ReadIndex;
                //public EmailSystemString sContent;//int
                emailtoadd.sContent = SystemStringsEmailContent[int.Parse(nlsplit[ReadIndex])];
                ++ReadIndex;

                //public bool bEmailBlocksThread;//true//false
                if (nlsplit[ReadIndex] == "true")
                {
                    emailtoadd.bEmailBlocksThread = true;
                }
                ++ReadIndex;

                //public int iDelay;//int
                emailtoadd.iDelay = int.Parse(nlsplit[ReadIndex]);
                ++ReadIndex;


                //now load some of the response data
                int ResponsesToRead = int.Parse(nlsplit[ReadIndex]);
                ++ReadIndex;

                for (int j = 0; j < ResponsesToRead; ++j)
                {
                    Response loadresp = new Response();

                    int respindex = int.Parse(nlsplit[ReadIndex]);
                    //read SystemStringsResponseTitle index

                    if (respindex != -1)//in this case its an actual response
                    {
                        loadresp.sResponseTag = SystemStringsResponseTitle[respindex];
                        loadresp.bResponseAutoFires = false;
                    }
                    else//otherwise its an autoresponse, set the index and bool
                    {
                        loadresp.bResponseAutoFires = true;
                        loadresp.sResponseTag = SystemStringsResponseTitle[0];
                    }



                    ++ReadIndex;
                    //bResponseEndsThread true/false
                    if (nlsplit[ReadIndex] == "true")
                    {
                        loadresp.bResponseEndsThread = true;
                    }
                    ++ReadIndex;
                    //read ResponseFiresThread index
                    int respthreadindex = int.Parse(nlsplit[ReadIndex]);
                    if (respthreadindex != -1)
                    {
                        loadresp.ResponseFiresThread = SystemThreads[respthreadindex];
                    }
                    else
                    {
                        loadresp.ResponseFiresThread = null;
                    }
                    ++ReadIndex;

                    emailtoadd.Responses.Add(loadresp);
                }



                    SystemEmails.Add(emailtoadd);
            }



            for (int i = 0; i < EmailsToRead; ++i)
            {
                //now emails are complete they will be gone through again to pick up the response email data

                //public Email ResponseMail;
                //read number of responses for i email
                int ResponsesToRead = int.Parse(nlsplit[ReadIndex]);
                ++ReadIndex;

                for (int j = 0; j < ResponsesToRead; ++j)
                {
                    SystemEmails[i].Responses[j].ResponseMail = SystemEmails[int.Parse(nlsplit[ReadIndex])];
                    ++ReadIndex;
                }

            }

            //finally go back to the threads and save the email indices for the progression
            //ThreadsToRead = int.Parse(nlsplit[ReadIndex]);
            //++ReadIndex;

            //each thread
            for (int i = 0; i < ThreadsToRead; ++i)
            {
                //number of mails // int
                int MailCount = int.Parse(nlsplit[ReadIndex]);
                ++ReadIndex;

                List<Email> elist = new List<Email>();
                for (int j = 0; j < MailCount; ++j)
                {
                    
                    //read mail index and add to list 
                    elist.Add(SystemEmails[int.Parse(nlsplit[ReadIndex])]);
                    ++ReadIndex;
                }
                SystemThreads[i].SetThreadPathData(elist);
            }
            
        }
    }
}