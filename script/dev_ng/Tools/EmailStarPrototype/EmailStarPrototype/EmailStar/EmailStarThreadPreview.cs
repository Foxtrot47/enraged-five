﻿namespace EmailStarPrototype
{
    /// <summary>
    /// Thread preview parsing here
    /// </summary>
    public sealed partial class EmailStar
    {

        //public List<Email> DebugThreadLogMails;
        //public int iDebugCurrentlyAt;
        //public bool bDebugBlockedUntilResponse;   

        //get status
        public static string GetPreviewMailContentForThread(Thread tin, int participant, int threadlogindex)
        {
            return myInstance.mGetPreviewMailContentForThread(tin, participant, threadlogindex);
        }
        string mGetPreviewMailContentForThread(Thread tin,int participant,int threadlogindex)
        { 
            string emailcontent = "";

            Emailer em = tin.participants[participant];
            //build the email content from DebugThreadLogMails
            //start at latest and go backwards until finished
            //latest being threadlogindex

            int ViewingFrom = threadlogindex;

            for (int i = ViewingFrom; i > -1; --i)
            {
                emailcontent += mRenderEmail(tin.DebugThreadLogMails[i],tin);
                if (i != 0)
                {
                    //if not last add a divider
                    emailcontent += "\r\n\r\n------------------------------\r\n\r\n";
                }
            }



                return emailcontent;
        }

        string mRenderEmail(Email torender,Thread threadin)
        {
            string mail = "";

            //render header

           
                //from
            mail += "FROM: " + torender.From.sMailerName.s + "(" + torender.From.sMailerAddress.s + ")\r\n";
                //senttime?
                //to
            mail += "TO: " + torender.To.sMailerName.s + "(" + torender.To.sMailerAddress.s + ")\r\n";
                // cc's
            mail += "CC: ";
            foreach (Emailer em in threadin.participants)
            {
                if ((em != torender.From) && (em != torender.To))
                {
                    mail += "(" + em.sMailerName.s + "(" + em.sMailerAddress.s + "))";
                    
                }
            }
            mail += "\r\n";
                //title
            mail += "SUBJECT: " + torender.sTitle.s + "\r\n";
            //render content
            mail += "\r\n" + torender.sContent.s + "\r\n\r\n";
            //end with signoff if there is one

            if (torender.From.sMailerSignoff != null)
            {
                mail += "\r\n" + torender.From.sMailerSignoff.s + "\r\n\r\n";
            }

            //mail += torender.From.sMailerName;
                
            return mail;
        }

        //move onward
        public static bool ProgressThreadPreview(Thread tin)
        {
            //if thread is over OR there are no mails in the stack and no possible responses
            
            if ( (tin.mails.Count == tin.iDebugCurrentlyAt && tin.DebugThreadLogMails[tin.DebugThreadLogMails.Count - 1].Responses.Count == 0) || tin.bDebugThreadEnded)
            {
                tin.DebugPreviewActionLog.Add("Thread progress is finished, THREAD OVER"); 
                //prevent progression on an ended thread
                tin.bDebugThreadEnded = true;
                return false;//thread over
            }

            //if the current thread is blocked this means this action is being forced, note it
            if (tin.bDebugBlockedUntilResponse)
            {
                tin.DebugPreviewActionLog.Add("Thread progression forced, response choice from " + tin.DebugThreadLogMails[tin.DebugThreadLogMails.Count-1].To.sMailerAddress.s + " skipped!");
            }
            //otherwise add a new email to the thread
            Email addmail = tin.mails[tin.iDebugCurrentlyAt];

            //add the delay to the thread
            tin.iDebugDelayingFor = addmail.iDelay;

            tin.DebugThreadLogMails.Add(addmail);
            //check for blocking
            //tin.mails[tin.iDebugCurrentlyAt].bEmailBlocksThread

            tin.bDebugBlockedUntilResponse = addmail.bEmailBlocksThread;
            
            //check for delay
            //tin.mails[tin.iDebugCurrentlyAt].iDelay
            tin.iDebugDelayingFor = addmail.iDelay;

            tin.DebugPreviewActionLog.Add("Mail with title \""+addmail.sTitle.s+ "\" sent from " +addmail.From.sMailerAddress.s+ " to " +addmail.To.sMailerAddress.s+ "");
            if (addmail.bEmailBlocksThread)
            {
                tin.DebugPreviewActionLog.Add("Mail blocked thread");
            }
            if (addmail.iDelay > 0)
            {
                tin.DebugPreviewActionLog.Add("Mail applied delay of " + addmail.iDelay + "ms to thread");
            }

            //if that was the last mail and it has no responses
            if (((tin.mails.Count) == tin.iDebugCurrentlyAt+1) && (addmail.Responses.Count == 0))
            {
                tin.DebugPreviewActionLog.Add("Thread progression ENDED due to last email and no possible responses.");
                tin.bDebugThreadEnded = true;
            }
            tin.iDebugCurrentlyAt++;
            // OR there is only one mail in the thread AND no responses in that mail
            if ((tin.mails.Count == 1) && (tin.mails[0].Responses.Count > 0))
            {
                tin.DebugPreviewActionLog.Add("This thread has only one email that contains no responses, THREAD OVER");
                //prevent progression on an ended thread
                tin.bDebugThreadEnded = true;
                return false;//thread over
            }

  
            //check responses in addmail for auto respond
            if(addmail.Responses.Count > 0)
            {
                int rcounter = 0;
                foreach (Response r in addmail.Responses)
                {
                    if (r.bResponseAutoFires)
                    {
                        //fire that shit
                        tin.DebugPreviewActionLog.Add("Added email has an autoresponse, firing first found.");
                        FirePreviewResponse(tin, rcounter);
                    }

                    rcounter++;
                }
            }


            return true;
        }

        //fire response
        public static bool FirePreviewResponse(Thread tin,int resindex)
        {
            //check response is in the list
            if (tin.DebugThreadLogMails[(tin.DebugThreadLogMails.Count - 1)].Responses.Count < resindex)
            {
                //not enough responses to do this
                return false;
            }
            //add the mail to the progress list
            Email respmail = tin.DebugThreadLogMails[(tin.DebugThreadLogMails.Count - 1)].Responses[resindex].ResponseMail;
            tin.DebugPreviewActionLog.Add("Response fired to email " + tin.DebugThreadLogMails[(tin.DebugThreadLogMails.Count - 1)].sTitle.s + " by emailer " + tin.DebugThreadLogMails[(tin.DebugThreadLogMails.Count - 1)].To.sMailerAddress.s + " ");
            //TODO note a thread fired if nessessary
            //tin.DebugThreadLogMails[(tin.DebugThreadLogMails.Count - 1)].Responses[resindex].ResponseFiresThread
            if (tin.DebugThreadLogMails[(tin.DebugThreadLogMails.Count - 1)].Responses[resindex].ResponseFiresThread != null)
            {
                tin.DebugPreviewActionLog.Add("Response attempts to fire thread: " + tin.DebugThreadLogMails[(tin.DebugThreadLogMails.Count - 1)].Responses[resindex].ResponseFiresThread.sThreadInternalTag);

            }

            //TODO does the response end the thread?
            if(tin.DebugThreadLogMails[(tin.DebugThreadLogMails.Count - 1)].Responses[resindex].bResponseEndsThread)
            {
                tin.DebugPreviewActionLog.Add("Thread progression ENDED by response.");
                tin.bDebugThreadEnded = true;
            }

            // check for respmail being a blocker and block thread if needed
            if (respmail.bEmailBlocksThread)
            {
                tin.DebugPreviewActionLog.Add("Thread progression BLOCKED by response mail.");
                tin.bDebugBlockedUntilResponse = true;
                //if this has no responses
                if (respmail.Responses.Count == 0)
                {
                    //if there are no mails left in the progression stack
                    if (tin.iDebugCurrentlyAt == (tin.mails.Count))
                    {
                        //thread must be over now
                        tin.DebugPreviewActionLog.Add("This response has no responses and there are no mails left in the progression stack, THREAD OVER!");
                        tin.bDebugThreadEnded = true;
                    }
                }
            }

            bool previousblockedthread = tin.DebugThreadLogMails[(tin.DebugThreadLogMails.Count - 1)].bEmailBlocksThread;
           
            //before adding the mail apply any delay to the thread
            tin.iDebugDelayingFor = respmail.iDelay;


            tin.DebugThreadLogMails.Add(respmail);

            


            //if the thread is not over
            if (!tin.bDebugThreadEnded)
            {
                //if the email sent by this response is not a blocker
                if (!respmail.bEmailBlocksThread)
                {
                    //finally, if the email this response is in previously blocked the thread
                    if (previousblockedthread)
                    {
                        //then unblock the thread and move it on
                        tin.bDebugBlockedUntilResponse = false;
                        tin.DebugPreviewActionLog.Add("Thread unblocked by response.");
                        ProgressThreadPreview(tin);
                    }
                }
            }

            //if it has responses, check for autorespond and fire WARNING, POTENTIAL INFINITE LOOP :P
            if (respmail.Responses.Count > 0)
            {
                int respcount = 0;
                foreach (Response r in respmail.Responses)
                {
                    if (r.bResponseAutoFires)
                    {
                        tin.DebugPreviewActionLog.Add("Auto response fired by response");
                        FirePreviewResponse(tin, respcount);
                    }

                    respcount++;
                }
            }
            return true;
        }

        public static void ResetThreadPreview(Thread ThreadIn)
        {
            ThreadIn.bDebugBlockedUntilResponse = false;
            ThreadIn.DebugThreadLogMails.Clear();
            ThreadIn.iDebugCurrentlyAt = 0;
            ThreadIn.iDebugDelayingFor = 0;
            ThreadIn.bDebugThreadEnded = false;
            ThreadIn.DebugThreadsFiredInPreview.Clear();
            ThreadIn.DebugPreviewActionLog.Clear();
            ThreadIn.DebugPreviewActionLog.Add("!Preview reset!");
            //prime the pump with the first mail
            //ThreadIn.DebugThreadLogMails.Add(ThreadIn.mails[0]);
            ProgressThreadPreview(ThreadIn);
        }
        

    }
}