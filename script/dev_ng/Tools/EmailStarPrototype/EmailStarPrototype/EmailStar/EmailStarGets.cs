﻿using System;
using System.Collections.Generic;
namespace EmailStarPrototype
{
    /// <summary>
    /// Very hacky prototype for the email editor
    /// 
    /// Functionality for grabbing form data here
    /// </summary>
    public sealed partial class EmailStar
    {


        public static List<EmailSystemString> GetStringList(StringCategory sc)
        {
            if (myInstance != null)
            {
                return myInstance.GetListByCategory(sc);
            }
            return null;
        }

        List<EmailSystemString> GetListByCategory(StringCategory sc)
        {

            switch (sc)
            {
                //Emailer Name // 0
                case EmailStar.StringCategory.EmailerName:

                       return SystemStringsEmailerName;

                //Emailer Address //1
                case EmailStar.StringCategory.EmailerAddress:

                    return SystemStringsEmailerAddress;
                    
  
                //Emailer Signoff //2
                case EmailStar.StringCategory.EmailerSignoff:

                    return SystemStringsEmailerSignoff;
                    

                //Email Content //3
                case EmailStar.StringCategory.EmailContent:

                    return SystemStringsEmailContent;
                    

                //Email Title //4 
                case EmailStar.StringCategory.EmailTitle:

                    return SystemStringsEmailTitle;
                    

                //Response Title //5
                case EmailStar.StringCategory.ResponseTitle:

                    return SystemStringsResponseTitle;
                    

                default:
                    throw new NotImplementedException();
                    
            }

            //fail
            //return null;
        }

        public static List<Emailer> GetEmailers()
        {
            if (myInstance != null)
            {
                return myInstance.SystemMailers;
            }
            return null;
        }
        public static List<Email> GetEmails()
        {
            if (myInstance != null)
            {
                return myInstance.SystemEmails;
            }
            return null;
        }
        public static List<Thread> GetThreads()
        {
            if (myInstance != null)
            {
                return myInstance.SystemThreads;
            }
            return null;
        }
    }
}