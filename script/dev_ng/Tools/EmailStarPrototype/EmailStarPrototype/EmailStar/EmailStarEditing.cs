﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
namespace EmailStarPrototype
{
    /// <summary>
    /// Very hacky prototype for the email editor
    /// 
    /// editing methods here
    /// </summary>
    public sealed partial class EmailStar
    {
        //used by editing controls to check for changes
        int iEditActions = 0;
        public static int getEditActionTotal()
        {
            if (myInstance != null)
            {
                return myInstance.iEditActions;
            }
            return -1;
        }

        //add thread
        public static void AddThread()
        {
            if (myInstance != null)
            {
                myInstance.mAddThread();
            }
        }
        void mAddThread()
        {
            iEditActions++;
        }

        //add string
        public static void AddString(EmailSystemString s)
        {
            if (myInstance != null)
            {
                myInstance.mAddString(s);
            }
        }

        void mAddString(EmailSystemString s)
        {
            if (!GetListByCategory(s.myCategory).Contains(s))
            {
                GetListByCategory(s.myCategory).Add(s);
                iEditActions++;
            }
        }


        public static void AddEmailer(int name, int address, int signoff)
        {
            if (myInstance != null)
            {
                //TODO, error check
                if (signoff == -1)
                {
                    myInstance.AddEmailer(myInstance.SystemStringsEmailerName[name], myInstance.SystemStringsEmailerAddress[address], null);
                }
                else
                {
                    myInstance.AddEmailer(myInstance.SystemStringsEmailerName[name], myInstance.SystemStringsEmailerAddress[address], myInstance.SystemStringsEmailerSignoff[signoff]);
                }
            }
        }
        public static void AddEmailer(Emailer existing, int name, int address, int signoff)
        {
            if (myInstance != null)
            {
                //TODO, error check
                if (signoff == -1)
                {
                    myInstance.AddEmailer(existing, myInstance.SystemStringsEmailerName[name], myInstance.SystemStringsEmailerAddress[address], null);
                }
                else
                {
                    myInstance.AddEmailer(existing,myInstance.SystemStringsEmailerName[name], myInstance.SystemStringsEmailerAddress[address], myInstance.SystemStringsEmailerSignoff[signoff]);
                }
            }
        }

        void AddEmailer(EmailSystemString name, EmailSystemString address, EmailSystemString signoff)
        {
            //SystemStringsEmailerName[]
            Emailer em = new Emailer();

            em.sMailerName = name;
            em.sMailerAddress = address;
            em.sMailerSignoff = signoff;

            SystemMailers.Add(em);
            iEditActions++;
        }
        void AddEmailer(Emailer existing, EmailSystemString name, EmailSystemString address, EmailSystemString signoff)
        {
            //SystemStringsEmailerName[]
            Emailer em = existing;

            em.sMailerName = name;
            em.sMailerAddress = address;
            em.sMailerSignoff = signoff;

            //SystemMailers.Add(em);
            iEditActions++;
        }

        internal static void CommitEmail(Email EditingMail, int from, int to, int title, int content, int parseddelay, bool blocks)
        {
            if (myInstance == null)
            {
                MessageBox.Show("System not properly intialised, CommitEmail called before instance created", "ARRRGH WTF");
                return;
            }

            if (!myInstance.SystemEmails.Contains(EditingMail))
            {
                myInstance.SystemEmails.Add(EditingMail);
            }

            EditingMail.From = myInstance.SystemMailers[from];
            EditingMail.To = myInstance.SystemMailers[to];

            EditingMail.sTitle = myInstance.SystemStringsEmailTitle[title];
            EditingMail.sContent = myInstance.SystemStringsEmailContent[content];

            EditingMail.bEmailBlocksThread = blocks;
            EditingMail.iDelay = parseddelay;

            myInstance.iEditActions++;
                
            return;
        }

        internal static void CommitThread(Thread exthread, string tagname,List<Email> threadpath, List<Emailer> participants)
        {
            myInstance.mCommitThread(exthread, tagname, threadpath, participants);
        }


        internal void mCommitThread(Thread exthread, string tagname, List<Email> threadpath, List<Emailer> participants)
        {
            Thread threadin = exthread;
            if (exthread == null)
            {
                //create a new thread
                threadin = new Thread();
                SystemThreads.Add(threadin);
            }
            else
            {
                //check that this thread is already there
                //SystemThreads
                if (!SystemThreads.Contains(exthread))
                {
                    throw new NotImplementedException("Something has gone horribly wrong in EmailStar.mCommitThread");
                    
                }
            }
            threadin.sThreadInternalTag = tagname;

            threadin.SetThreadData(threadpath, participants);

            iEditActions++;
        }

    }
}