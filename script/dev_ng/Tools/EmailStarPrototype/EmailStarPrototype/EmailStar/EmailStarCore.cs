﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmailStarPrototype
{
    /// <summary>
    /// Very hacky prototype for the email editor
    /// 
    /// Core data types are here
    /// </summary>
    public sealed partial class EmailStar
    {
        /// <summary>
        /// A container for email system string, mainly so we can track what is where
        /// also for potentially adding other tagging data that needs to go with the string
        /// 
        /// eventually there should be getters and setters for most of these, 
        /// made public for now
        /// </summary>
        public class EmailSystemString
        {
            public StringCategory myCategory;
            public string s;

            public EmailSystemString()
            {

            }
            public EmailSystemString(string sin)
            {
                s = sin;
            }
        }
        //List<EmailSystemString> SystemStrings;//the strings for output to the gxtgen txt
        public enum StringCategory
        {
            EmailerName,
            EmailerAddress,
            EmailerSignoff,
            EmailContent,
            EmailTitle,
            ResponseTitle
        }
        List<EmailSystemString> SystemStringsEmailerName;
        List<EmailSystemString> SystemStringsEmailerAddress;
        List<EmailSystemString> SystemStringsEmailerSignoff;
        List<EmailSystemString> SystemStringsEmailContent;
        List<EmailSystemString> SystemStringsEmailTitle;
        List<EmailSystemString> SystemStringsResponseTitle;

        public class Emailer
        {
            public EmailSystemString sMailerName;
            public EmailSystemString sMailerAddress;
            public EmailSystemString sMailerSignoff;

            public bool bEditing;
        }
        List<Emailer> SystemMailers;//the individuals that are part of the system

        public class Response
        {
            public EmailSystemString sResponseTag;
            public Email ResponseMail;
            public bool bResponseEndsThread;
            public Thread ResponseFiresThread;
            public bool bResponseAutoFires;
        }

        public class Email
        {
            public Emailer From;
            public Emailer To;

            public EmailSystemString sTitle;
            public EmailSystemString sContent;

            public List<Response> Responses;

            public bool bEmailBlocksThread;
            public int iDelay;

            public bool bEditing;

            public Email()
            {
                Responses = new List<Response>();
            }
            
        }
        List<Email> SystemEmails;


        public class Thread
        {
            public string sThreadInternalTag;//used for labeling in menus

            protected List<Emailer> ThreadParticipants;
            protected List<Email> ThreadCoreMails;

            public List<Emailer> participants
            {
                get
                {
                    return ThreadParticipants;
                }
            }
            public List<Email> mails
            {
                get
                {
                    return ThreadCoreMails;
                }
            }

            

            //these will be used for email preview mode - not exported
            public List<Email> DebugThreadLogMails;
            public int iDebugCurrentlyAt;
            public bool bDebugBlockedUntilResponse;
            public int iDebugDelayingFor;
            public bool bDebugThreadEnded;
            public List<Thread> DebugThreadsFiredInPreview;
            public List<string> DebugPreviewActionLog;

            public bool bEditing = false;

            public Thread()
            {
                ThreadParticipants = new List<Emailer>();
                ThreadCoreMails = new List<Email>();

                DebugThreadLogMails = new List<Email>();
                DebugThreadsFiredInPreview = new List<Thread>();

                DebugPreviewActionLog = new List<string>();
            }

            public void SetThreadData(List<Email> threadpath, List<Emailer> participants)
            {
                SetThreadData(participants);

                SetThreadPathData(threadpath);

            }
            public void SetThreadData(List<Emailer> participants)
            {
                ThreadParticipants.Clear();
                foreach (Emailer em in participants)
                {
                    ThreadParticipants.Add(em);
                }
            }
            public void SetThreadPathData(List<Email> threadpath)
            {
                ThreadCoreMails.Clear();
                foreach (Email e in threadpath)
                {
                    ThreadCoreMails.Add(e);
                }
            }

        }
        List<Thread> SystemThreads;


        //housekeeping data
        static EmailStar myInstance;//singleton

        static public EmailStar Init()
        {
            if (myInstance == null)
            {
                myInstance = new EmailStar();
            }

            return myInstance;
        }

        //intialisation
        EmailStar()
        {
            //myForm = form_in;
            SystemStringsEmailerName = new List<EmailSystemString>();
            SystemStringsEmailerAddress = new List<EmailSystemString>();
            SystemStringsEmailerSignoff = new List<EmailSystemString>();
            SystemStringsEmailContent = new List<EmailSystemString>();
            SystemStringsEmailTitle = new List<EmailSystemString>();
            SystemStringsResponseTitle = new List<EmailSystemString>();

            SystemMailers = new List<Emailer>();
            //SystemResponses = new List<Response>();
            SystemEmails = new List<Email>();
            SystemThreads = new List<Thread>();

        }

        internal static void FillOutResponse(Response editResponse, int tag, int email, bool endsthread, bool fireathread, int threadtofire)
        {
            if(tag == -1) // auto fire, will be converted to auto indice on export
            {
                editResponse.sResponseTag = myInstance.SystemStringsResponseTitle[0];
                editResponse.bResponseAutoFires = true;
            }else{
                editResponse.bResponseAutoFires = false;
                editResponse.sResponseTag = myInstance.SystemStringsResponseTitle[tag];
            }
            
            editResponse.ResponseMail = myInstance.SystemEmails[email];

            editResponse.bResponseEndsThread = endsthread;
            if (fireathread)
            {
                if (threadtofire > -1)
                {
                    editResponse.ResponseFiresThread = myInstance.SystemThreads[threadtofire];
                }
            }
            else
            {
                editResponse.ResponseFiresThread = null;
            }
        }
    }
}
