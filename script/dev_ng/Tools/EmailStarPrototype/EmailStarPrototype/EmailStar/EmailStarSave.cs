﻿using System.IO;
using System.Windows.Forms;


namespace EmailStarPrototype
{
    /// <summary>
    /// Very hacky prototype for the email editor
    /// 
    /// Saving data to csv for reload goes here
    /// </summary>
    public sealed partial class EmailStar
    {

        public static void SaveSystem(string filepath)
        {
            myInstance.mSaveSystem(filepath);
        }

        void mSaveSystem(string filepath)
        {
            //MessageBox.Show(filepath);

            StreamWriter w = new StreamWriter(filepath);
            //writer.WriteLine("Testing this shit!");
            
            //format for est files
            //List<EmailSystemString> SystemStringsEmailerName;
                //int total -> csvs
            w.Write(SystemStringsEmailerName.Count + "\n");
            foreach (EmailSystemString ess in SystemStringsEmailerName)
            {
                w.Write(ess.s + "\n");
            }

            //List<EmailSystemString> SystemStringsEmailerAddress;
                //int total -> csvs
            w.Write(SystemStringsEmailerAddress.Count + "\n");
            foreach (EmailSystemString ess in SystemStringsEmailerAddress)
            {
                w.Write(ess.s + "\n");
            }

            //List<EmailSystemString> SystemStringsEmailerSignoff;
                //int total -> csvs
            w.Write(SystemStringsEmailerSignoff.Count + "\n");
            foreach (EmailSystemString ess in SystemStringsEmailerSignoff)
            {
                w.Write(ess.s + "\n");
            }
            //List<EmailSystemString> SystemStringsEmailContent;
                //int total -> csvs
            w.Write(SystemStringsEmailContent.Count + "\n");
            foreach (EmailSystemString ess in SystemStringsEmailContent)
            {
                w.Write(ess.s + "\n");
            }
            //List<EmailSystemString> SystemStringsEmailTitle;
                //int total -> csvs
            w.Write(SystemStringsEmailTitle.Count + "\n");
            foreach (EmailSystemString ess in SystemStringsEmailTitle)
            {
                w.Write(ess.s + "\n");
            }
            //List<EmailSystemString> SystemStringsResponseTitle;
                //int total -> csvs
            w.Write(SystemStringsResponseTitle.Count + "\n");
            foreach (EmailSystemString ess in SystemStringsResponseTitle)
            {
                w.Write(ess.s + "\n");
            }

            //write the data and string indices for the other system elements

            //List<Emailer> SystemMailers;//the individuals that are part of the system
            //List<Email> SystemEmails;
            //List<Thread> SystemThreads;


            //save mailers
                    //public EmailSystemString sMailerName;
                    //public EmailSystemString sMailerAddress;
                    //public EmailSystemString sMailerSignoff;
            w.Write(SystemMailers.Count + "\n");
            foreach (Emailer em in SystemMailers)
            {
                w.Write(SystemStringsEmailerName.IndexOf(em.sMailerName) + "\n");
                w.Write(SystemStringsEmailerAddress.IndexOf(em.sMailerAddress) + "\n");
                w.Write(SystemStringsEmailerSignoff.IndexOf(em.sMailerSignoff) + "\n");
            }

            //now save first bits of thread data: the tags and the participants
            //mail thread ordering data is loaded after the emails themselves are loaded

            w.Write(SystemThreads.Count + "\n");

            foreach (Thread t in SystemThreads)
            {
                //save the tag, number of participants then participant list
                w.Write(t.sThreadInternalTag + "\n");
                w.Write(t.participants.Count + "\n");
                foreach (Emailer em in t.participants)
                {
                    w.Write(SystemMailers.IndexOf(em) + "\n");
                }
            }
            
            //now save the email data including thread data
            w.Write(SystemEmails.Count + "\n");

            foreach(Email e in SystemEmails)
            {
                //public Emailer From;
                w.Write(SystemMailers.IndexOf(e.From) + "\n");
                //public Emailer To;
                w.Write(SystemMailers.IndexOf(e.To) + "\n");

               // public EmailSystemString sTitle;
                w.Write(SystemStringsEmailTitle.IndexOf(e.sTitle) + "\n");
                
                //public EmailSystemString sContent;
                w.Write(SystemStringsEmailContent.IndexOf(e.sContent) + "\n");
                
               
                //public bool bEmailBlocksThread;

                if (e.bEmailBlocksThread)
                {
                    w.Write("true\n");
                }
                else
                {
                    w.Write("false\n");
                }


                //public int iDelay;
                w.Write(e.iDelay + "\n");

                //public List<Response> Responses;
                w.Write(e.Responses.Count+"\n");
                
                foreach (Response r in e.Responses)
                {
                    //public EmailSystemString sResponseTag;
                    if (r.bResponseAutoFires)
                    {
                        w.Write("-1\n");
                    }
                    else
                    {
                        w.Write(SystemStringsResponseTitle.IndexOf(r.sResponseTag) + "\n");
                    }
                            //public Email ResponseMail;
                            //w.Write(SystemEmails.IndexOf() + "\n"); //this will need filling out in another pass
                    //public bool bResponseEndsThread;
                    if (r.bResponseEndsThread)
                    {
                        w.Write("true\n");
                    }
                    else
                    {
                        w.Write("false\n");
                    }
                    //public Thread ResponseFiresThread;
                    w.Write(SystemThreads.IndexOf(r.ResponseFiresThread) +"\n");
                }
            }

            //now emails are complete they will be gone through again to pick up the response data
            foreach (Email e in SystemEmails)
            {
                //public List<Response> Responses;
                w.Write(e.Responses.Count + "\n");
                foreach (Response r in e.Responses)
                {
                    //public Email ResponseMail;
                    w.Write(SystemEmails.IndexOf(r.ResponseMail) + "\n");

                    if (SystemEmails.IndexOf(r.ResponseMail) == -1)
                    {
                        MessageBox.Show("Attempt to write -1 as email response index in email titled : " + e.sTitle);
                    }
                }
            }

            //finally go back to the threads and save the email indices for the progression
            
            //w.Write(SystemThreads.Count + "\n");

            foreach (Thread t in SystemThreads)
            {
                w.Write(t.mails.Count + "\n");
                foreach (Email e in t.mails)
                {
                    w.Write(SystemEmails.IndexOf(e)+ "\n");
                }
            }

            w.Close();
        }
    }
}