﻿


//notes

//export global sizes
//remember to set highest number of response choices too!
//Also max participants


using System.IO;
using System.Collections.Generic;
using System;
using System.Windows.Forms;
namespace EmailStarPrototype
{
    /// <summary>
    /// Very hacky prototype for the email editor
    /// 
    /// exporting functionality is here
    /// </summary>
    public sealed partial class EmailStar
    {
        //export to three files
        //Email_generated_settings.sch
        //Email_generated_globals.sch
        //Strings to text tag gxt

        public static void ExportRAGEFiles(string directorytarget)
        {
            myInstance.mExportRAGEFiles(directorytarget);


            
        }

        void mExportRAGEFiles(string dir)
        {
            //create the three files at the directory picked
            //TODO, detect for "script" and ask user if they want export auto added to the right places

            bool pickeddir = true;

            if (dir == "toscript")
            {
                pickeddir = false;
            }
            //if direct export then try to use
            //X:\gta5\script\dev\singleplayer\include\globals\email_generated_globals.sch
            //X:\gta5\script\dev\singleplayer\include\private\email_generated_functions.sch
            //X:\gta5\build\dev\common\text\americanEmail.txt
            
            //data to export

            //strings
            //organise strings into the gxt-txt
            //build string retrival functions into Email_generated_functions.sch
            //note list lengths in Email_generated_globals.sch

            //emails

            //threads

            List<EmailSystemString> SystemStringIndexTracker = new List<EmailSystemString>();
            StreamWriter wAmericanEmail;
            if (pickeddir)
            {
                wAmericanEmail = new StreamWriter(dir + "\\americanEmail.txt");
            }
            else
            {
                //X:\gta5\build\dev\common\text\americanEmail.txt
                try
                {
                    wAmericanEmail = new StreamWriter("X:\\gta5\\build\\dev\\common\\text\\americanEmail.txt");
                }
                catch
                {
                    MessageBox.Show("Could not save to X:\\gta5\\build\\dev\\common\\text\\americanEmail.txt! Is it checked out?","Export failed");
                    return;
                }

            }
            wAmericanEmail.Write("start\n\nEMAIL\n\nend\n\n\n\n{---------Auto generated email strings--------}\n\n\n");

            //////////////////////////American_email.txt//////////////////////////////
            // 
            //  generate string tag names, format EMSTR_(INT)
            //  

            //debug blahs
            wAmericanEmail.Write("[EM_PH_EMH]\nSelect_an_Emailer:\n\n");//to be removed
            wAmericanEmail.Write("[EM_PH_MAI]\nSelect_an_Email:\n\n");//to be removed
            wAmericanEmail.Write("[EM_PH_RES]\nSelect_a_response:\n\n"); //to be removed

            wAmericanEmail.Write("[EM_INBOX:EMAIL]\nInbox\n\n");
            wAmericanEmail.Write("[EM_RESPONSE:EMAIL]\nPick a response\n\n");

            int stringcount = 0;
            //List<EmailSystemString> SystemStringsEmailerName;
            foreach (EmailSystemString s in SystemStringsEmailerName)
            {
                wAmericanEmail.Write("[EMSTR_" + stringcount + ":EMAIL]\n");
                wAmericanEmail.Write(s.s.Replace(' ', '_') + "\n\n");

                SystemStringIndexTracker.Add(s);
                ++stringcount;
            }
            //List<EmailSystemString> SystemStringsEmailerAddress;
            foreach (EmailSystemString s in SystemStringsEmailerAddress)
            {
                wAmericanEmail.Write("[EMSTR_" + stringcount + ":EMAIL]\n");
                wAmericanEmail.Write(s.s.Replace(' ','_') + "\n\n");

                SystemStringIndexTracker.Add(s);
                ++stringcount;
            }
            //List<EmailSystemString> SystemStringsEmailerSignoff;
            foreach (EmailSystemString s in SystemStringsEmailerSignoff)
            {
                wAmericanEmail.Write("[EMSTR_" + stringcount + ":EMAIL]\n");
                wAmericanEmail.Write(s.s.Replace(' ', '_') + "\n\n");

                SystemStringIndexTracker.Add(s);
                ++stringcount;
            }
            //List<EmailSystemString> SystemStringsEmailContent;
            foreach (EmailSystemString s in SystemStringsEmailContent)
            {
                wAmericanEmail.Write("[EMSTR_" + stringcount + ":EMAIL]\n");
                wAmericanEmail.Write(s.s.Replace(' ', '_') + "\n\n");

                SystemStringIndexTracker.Add(s);
                ++stringcount;
            }
            //List<EmailSystemString> SystemStringsEmailTitle;
            foreach (EmailSystemString s in SystemStringsEmailTitle)
            {
                wAmericanEmail.Write("[EMSTR_" + stringcount + ":EMAIL]\n");
                wAmericanEmail.Write(s.s.Replace(' ', '_') + "\n\n");

                SystemStringIndexTracker.Add(s);
                ++stringcount;
            }
            //List<EmailSystemString> SystemStringsResponseTitle;
            foreach (EmailSystemString s in SystemStringsResponseTitle)
            {
                wAmericanEmail.Write("[EMSTR_" + stringcount + ":EMAIL]\n");
                wAmericanEmail.Write(s.s.Replace(' ', '_') + "\n\n");

                SystemStringIndexTracker.Add(s);
                ++stringcount;
            }
            wAmericanEmail.Close();
            //////////////strings over
            StreamWriter wGlobals;
            if (pickeddir)
            {
                wGlobals = new StreamWriter(dir + "\\email_generated_globals.sch");
            }
            else
            {
                try
                {
                    wGlobals = new StreamWriter("X:\\gta5\\script\\dev\\singleplayer\\include\\globals\\email_generated_globals.sch");
                }
                catch
                {
                    MessageBox.Show("Could not write to X:\\gta5\\script\\dev\\singleplayer\\include\\globals\\email_generated_globals.sch! is it checked out?", "Export failed");
                    return;
                }
            }
            ///////////////////////Email_generated_globals.sch//////////////////////////////////////
            //generate exported header values
            int highresponses = 0;
            foreach(Email e in SystemEmails)
            {
                if (e.Responses.Count > highresponses)
                {
                    highresponses = e.Responses.Count;
                }
            }
            //CONST_INT MAX_RESPONSES_IN_EMAIL 
            wGlobals.Write("CONST_INT MAX_RESPONSES_IN_EMAIL " + highresponses + "\n");
            //
            //CONST_INT TOTAL_EMAILS_IN_GAME //count
            wGlobals.Write("CONST_INT TOTAL_EMAILS_IN_GAME " + SystemEmails.Count + "\n");
            //CONST_INT TOTAL_EMAIL_THREADS_IN_GAME //count
            wGlobals.Write("CONST_INT TOTAL_EMAIL_THREADS_IN_GAME " + SystemThreads.Count + "\n");
            //
            wGlobals.Write("CONST_INT EMAIL_AUTO_RESPONSE_ID_TAG_INDEX -1\n");



            int highparts = 0;
            int highlength = 0;
            foreach(Thread t in SystemThreads)
            {
                if (t.participants.Count > highparts)
                {
                    highparts = t.participants.Count;
                }
                if (t.mails.Count > highlength)
                {
                    highlength = t.mails.Count;
                }
            }
            //CONST_INT MAX_EMAIL_THREAD_PARTICIPANTS //iterate threads for most participants
            wGlobals.Write("CONST_INT MAX_EMAIL_THREAD_PARTICIPANTS " + highparts + "\n");
            //CONST_INT MAX_THREAD_LENGTH //iterate threads for longest
            wGlobals.Write("CONST_INT MAX_THREAD_LENGTH 16\n");//+ highlength + "\n");
            //
            //CONST_INT TOTAL_INBOXES  // number from max mailers // TODO maybe tag certain mailers and bind them to specific inboxes
            wGlobals.Write("CONST_INT TOTAL_INBOXES " + SystemMailers.Count + "\n");
            //
            //ENUM EMAILER_ENUMS // create from caps'd names of all the emailers
            wGlobals.Write("\nENUM EMAILER_ENUMS\n");
            List<string> MailerUpperEnumNames = new List<string>();//generated enums stored here for saving the thread participants
            for(int i = 0;i < SystemMailers.Count;++i)
            {
                string eupper = SystemMailers[i].sMailerName.s.ToUpper();
                eupper = eupper.Replace(' ','_');//todo, regex for invalid chars
                MailerUpperEnumNames.Add("EMAILER_" + eupper);
                //save the enum
                wGlobals.Write("\tEMAILER_" + eupper);
                //if not last then comma
                if(i != (SystemMailers.Count-1))
                {
                    wGlobals.Write(",");
                }
                wGlobals.Write("\n");
            }
            wGlobals.Write("ENDENUM\n\n\n\n\n\n");


            wGlobals.Close();
            /////////globals over
            StreamWriter wFunctions;
            if (pickeddir)
            {
                wFunctions = new StreamWriter(dir + "\\email_generated_functions.sch");
            }
            else
            {
                try
                {

                    wFunctions = new StreamWriter("X:\\gta5\\script\\dev\\singleplayer\\include\\private\\email_generated_functions.sch");
                }
                catch
                {
                    MessageBox.Show("Could not write to X:\\gta5\\script\\dev\\singleplayer\\include\\private\\email_generated_functions.sch! Is it checked out?", "Export failed");
                    return;
                }
            }
            ////////////////Email_generated_functions.sch////////////////////////////////////////////////////////
            //
            // USING "email_globals.sch" // that in turn should be USING "Email_generated_globals.sch"
            wFunctions.Write("USING \"email_globals.sch\"\n\n\n");

            wFunctions.Write(" /////THIS FILE IS AUTO GENERATED BY EMAILSTAR PROTOTYPE\n\n\n");
            //
            // FUNC STRING RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(INT indice)
            wFunctions.Write("FUNC STRING RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(INT indice)\n\n");
            //      //retrieval order and tags same as American_email.txt
            //
            //EMSTR_
            wFunctions.Write("\tSWITCH indice\n");
            stringcount = 0;
            //List<EmailSystemString> SystemStringsEmailerName;
            foreach (EmailSystemString s in SystemStringsEmailerName)
            {
                wFunctions.Write("\t\tCASE " + stringcount + "\n");
                wFunctions.Write("\t\t\tRETURN \"EMSTR_" + stringcount + "\"\n");
                //wFunctions.Write("\t\tBREAK\n");
                ++stringcount;
            }
            //List<EmailSystemString> SystemStringsEmailerAddress;
            foreach (EmailSystemString s in SystemStringsEmailerAddress)
            {
                wFunctions.Write("\t\tCASE " + stringcount + "\n");
                wFunctions.Write("\t\t\tRETURN \"EMSTR_" + stringcount + "\"\n");
                //wFunctions.Write("\t\tBREAK\n");
                ++stringcount;
            }
            //List<EmailSystemString> SystemStringsEmailerSignoff;
            foreach (EmailSystemString s in SystemStringsEmailerSignoff)
            {   
                wFunctions.Write("\t\tCASE " + stringcount + "\n");
                wFunctions.Write("\t\t\tRETURN \"EMSTR_" + stringcount + "\"\n");
                //wFunctions.Write("\t\tBREAK\n");
                ++stringcount;
            }
            //List<EmailSystemString> SystemStringsEmailContent;
            foreach (EmailSystemString s in SystemStringsEmailContent)
            {
                wFunctions.Write("\t\tCASE " + stringcount + "\n");
                wFunctions.Write("\t\t\tRETURN \"EMSTR_" + stringcount + "\"\n");
                //wFunctions.Write("\t\tBREAK\n");
                ++stringcount;
            }
            //List<EmailSystemString> SystemStringsEmailTitle;
            foreach (EmailSystemString s in SystemStringsEmailTitle)
            {
                wFunctions.Write("\t\tCASE " + stringcount + "\n");
                wFunctions.Write("\t\t\tRETURN \"EMSTR_" + stringcount + "\"\n");
                //wFunctions.Write("\t\tBREAK\n");
                ++stringcount;
            }
            //List<EmailSystemString> SystemStringsResponseTitle;
            foreach (EmailSystemString s in SystemStringsResponseTitle)
            {
                wFunctions.Write("\t\tCASE " + stringcount + "\n");
                wFunctions.Write("\t\t\tRETURN \"EMSTR_" + stringcount + "\"\n");
                //wFunctions.Write("\t\tBREAK\n");
                ++stringcount;
            }
            //
            // 
            wFunctions.Write("\tENDSWITCH\n");
            wFunctions.Write("\tRETURN \"EMAIL_TAG_MISSING\"\n");
            wFunctions.Write("ENDFUNC\n\n");
            //
            //
            //

            //PROC INTIALISE_EMAILS() // fill out all the thread data and indices to the following structures
            wFunctions.Write("PROC INTIALISE_EMAIL_SYSTEM_CONTENT()\n\n");
            //use SystemStringIndexTracker for ease of grabbing string indices
            //use MailerUpperEnumNames for grabbing mailer names
            //g_AllEmails 
                //iTitleTag
                //iContentTag
                //eFrom
                //eTo
                //iResponses
                    // and .Responses
             wFunctions.Write("\n\n\t/////Emails\n\n");
            int currmail = 0;
            foreach (Email e in SystemEmails)
            {
                wFunctions.Write("\t//////// " + e.sTitle.s + " \n\t// TO: " + e.To.sMailerAddress.s + " \n\t// FROM: " + e.From.sMailerAddress.s + "\n\t// " + e.sContent.s.Split('\n')[0] + "\n\n");
                wFunctions.Write("\tg_AllEmails[" + currmail + "].iTitleTag = " + SystemStringIndexTracker.IndexOf(e.sTitle) + "\n");
                wFunctions.Write("\tg_AllEmails[" + currmail + "].iContentTag = " + SystemStringIndexTracker.IndexOf(e.sContent) + "\n\n");

                //efrom and to
                wFunctions.Write("\tg_AllEmails[" + currmail + "].eFrom = " + MailerUpperEnumNames[SystemMailers.IndexOf(e.From)] + "\n");
                wFunctions.Write("\tg_AllEmails[" + currmail + "].eTo = " + MailerUpperEnumNames[SystemMailers.IndexOf(e.To)] + "\n");
                wFunctions.Write("\tg_AllEmails[" + currmail + "].bEmailBlocksThread = " + e.bEmailBlocksThread + "\n");
                wFunctions.Write("\tg_AllEmails[" + currmail + "].iDelay = " + e.iDelay + "\n\n");


                wFunctions.Write("\tg_AllEmails[" + currmail + "].iResponses = " + e.Responses.Count + "\n\n");


                int currresponse = 0;
                foreach (Response r in e.Responses)
                {
                    SystemEmails.IndexOf(r.ResponseMail);
                    //wFunctions.Write("\tg_AllEmails[" + currmail + "].Responses[currresponse]. = "

                    //INT iResponseNameTag//the short version tag for picking this response
                    if (!r.bResponseAutoFires)
                    {
                        //not auto firing, add true index
                        wFunctions.Write("\tg_AllEmails[" + currmail + "].Responses[" + currresponse + "].iResponseNameTag = " + SystemStringIndexTracker.IndexOf(r.sResponseTag) + "\n");
                    }
                    else
                    {
                        //autofiring, add enum
                        wFunctions.Write("\tg_AllEmails[" + currmail + "].Responses[" + currresponse + "].iResponseNameTag = EMAIL_AUTO_RESPONSE_ID_TAG_INDEX\n");
                    }
                    //INT iResponseMail//the index of the email response sent when this is picked
                    wFunctions.Write("\tg_AllEmails[" + currmail + "].Responses[" + currresponse + "].iResponseMail = " + SystemEmails.IndexOf(r.ResponseMail) + "\n");
                    //BOOL bResponseEndsThread//does this response being picked cause the thread to finish?
                    wFunctions.Write("\tg_AllEmails[" + currmail + "].Responses[" + currresponse + "].bResponseEndsThread = " + r.bResponseEndsThread + "\n");
                    //INT iResponseFiresThread//-1 means triggers nothing, otherwise attempt to start EMAIL_THREAD in threadlist
                    if (r.ResponseFiresThread == null)
                    {
                        wFunctions.Write("\tg_AllEmails[" + currmail + "].Responses[" + currresponse + "].iResponseFiresThread = -1\n\n");
                    }
                    else
                    {
                        wFunctions.Write("\tg_AllEmails[" + currmail + "].Responses[" + currresponse + "].iResponseFiresThread = " + SystemThreads.IndexOf(r.ResponseFiresThread) + "\n\n");
                    }

                    ++currresponse;
                }
                ++currmail;
            }





            wFunctions.Write("\n\n\t/////Threads\n");

            //g_AllEmailThreads
                	//INT iParticipants;
	                //EMAILER Participants[MAX_EMAIL_THREAD_PARTICIPANTS] //the people being mailed as part of this thread, if the current message is not TO or FROM them (within the email) they are marked as CC

	                //INT ThreadMails[MAX_THREAD_LENGTH]//indices of emails that make up the core this thread, including replies - thus this may be greater than thread path
	                //INT ThreadLog[MAX_THREAD_LOG_LENGTH]//The main path of emails through the thread, not including responses
	                //INT iCurrentlyAt//how far through the thread path the mail is
	                //INT iMailsInLog//how much of the thread log has been used
                    //INT iTotalThreadMails//how many mails are in the log
	                //INT iDelay
	                //BOOL bBLockedUntilResponse//this thread is stopped, waiting for a player response to iCurrentlyAt
	                //INT iAutoFireThread

            //
            //
            //int thread 
            ////TODO CHECK THIS WORKS///
            int currthread = 0;
            foreach (Thread t in SystemThreads)
            {
                wFunctions.Write("\t//////////// " + t.sThreadInternalTag +"\n");
                //resets
                wFunctions.Write("\tg_AllEmailThreads[" + currthread + "].bBLockedUntilResponse = false\n");
                wFunctions.Write("\tg_AllEmailThreads[" + currthread + "].iCurrentlyAt = 0\n");
                wFunctions.Write("\tg_AllEmailThreads[" + currthread + "].iMailsInLog = 0\n");
                wFunctions.Write("\tg_AllEmailThreads[" + currthread + "].iDelay = 0\n");

                wFunctions.Write("\tg_AllEmailThreads[" + currthread + "].iParticipants = "+t.participants.Count+"\n");
                int currpart = 0;
                foreach(Emailer e in t.participants)
                {

                    wFunctions.Write("\t\tg_AllEmailThreads[" + currthread + "].Participants[" + currpart + "] = " + MailerUpperEnumNames[SystemMailers.IndexOf(e)] + "\n");
                    ++currpart;
                }

                currmail = 0;
                wFunctions.Write("\tg_AllEmailThreads[" + currthread + "].iTotalThreadMails = " + t.mails.Count + "\n");
                foreach (Email e in t.mails)
                {
                    wFunctions.Write("\t\tg_AllEmailThreads[" + currthread + "].ThreadMails[" + currmail + "] = " + SystemEmails.IndexOf(e) + "\n");
                    ++currmail;
                }


                ++currthread;
                wFunctions.Write("\n\n");
            }

            //ENDPROC
            //
            //
            wFunctions.Write("ENDPROC\n\n\n");


            //Add
            //MailerUpperEnumNames
            //SystemStringIndexTracker
            //FUNC STRING RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(INT indice)
            

            //FUNC STRING GET_EMAILER_NAME_TAG(EMAILER_ENUMS mailer)
            wFunctions.Write("FUNC STRING GET_EMAILER_NAME_TAG(EMAILER_ENUMS mailer)\n");
            wFunctions.Write("\tSWITCH mailer\n");
            foreach(Emailer em in SystemMailers)
            {
                wFunctions.Write("\t\tCASE " + MailerUpperEnumNames[SystemMailers.IndexOf(em)] + "\n");
                wFunctions.Write("\t\t\tRETURN RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(" + SystemStringIndexTracker.IndexOf(em.sMailerName) + ")\n");
            }
            wFunctions.Write("\tENDSWITCH\n");
            wFunctions.Write("\tRETURN \"NULL\"\n");
            wFunctions.Write("ENDFUNC\n");


            //FUNC STRING GET_EMAILER_ADDRESS_TAG(EMAILER_ENUMS mailer)
            wFunctions.Write("FUNC STRING GET_EMAILER_ADDRESS_TAG(EMAILER_ENUMS mailer)\n");
            wFunctions.Write("\tSWITCH mailer\n");
            foreach (Emailer em in SystemMailers)
            {
                wFunctions.Write("\t\tCASE " + MailerUpperEnumNames[SystemMailers.IndexOf(em)] + "\n");
                wFunctions.Write("\t\t\tRETURN RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(" + SystemStringIndexTracker.IndexOf(em.sMailerAddress) + ")\n");
            }
            wFunctions.Write("\tENDSWITCH\n");
            wFunctions.Write("\tRETURN \"NULL\"\n");
            wFunctions.Write("ENDFUNC\n");

            //FUNC STRING GET_EMAILER_SIGNOFF_TAG(EMAILER_ENUMS mailer)
            wFunctions.Write("FUNC STRING GET_EMAILER_SIGNOFF_TAG(EMAILER_ENUMS mailer)\n");
            wFunctions.Write("\tSWITCH mailer\n");
            foreach (Emailer em in SystemMailers)
            {
                wFunctions.Write("\t\tCASE " + MailerUpperEnumNames[SystemMailers.IndexOf(em)] + "\n");
                wFunctions.Write("\t\t\tRETURN RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(" + SystemStringIndexTracker.IndexOf(em.sMailerSignoff) + ")\n");
            }
            wFunctions.Write("\tENDSWITCH\n");
            wFunctions.Write("\tRETURN \"NULL\"\n");
            wFunctions.Write("ENDFUNC\n");


            
            wFunctions.Close();

            
            if (!pickeddir)
            {
                MessageBox.Show("Export complete!","Success!");
            }
        }
    }
}