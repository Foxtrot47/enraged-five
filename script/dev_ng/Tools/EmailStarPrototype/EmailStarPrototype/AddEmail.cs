﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmailStarPrototype
{
    public partial class AddEmail : Form
    {
        int iLastCheckedEditorActions = 0;

        EmailStar.Email EditingMail = null;

        Form parentlauncher;

        public AddEmail(Form parentform)
        {
            parentlauncher = parentform;
            InitializeComponent();
            RefreshLists();
        }
        public AddEmail(EmailStar.Email editing,Form parentform)
        {
            if (editing.bEditing == true)
            {
                this.Close();
            }
            else
            {
                editing.bEditing = true;

                parentlauncher = parentform;
                EditingMail = editing;
                InitializeComponent();
                RefreshLists();
            }
        }

 
        void RefreshLists()
        {
            //update the listings

            List<EmailStar.Emailer> mailersreturned;
            List<EmailStar.EmailSystemString> stringsreturned;
            //fromlist - list of emailers
            //tolist - list of emailers
            mailersreturned = EmailStar.GetEmailers();
            fromlist.Items.Clear();
            tolist.Items.Clear();

            foreach (EmailStar.Emailer e in mailersreturned)
            {
                fromlist.Items.Add(e.sMailerName.s + " (" + e.sMailerAddress.s+ ")");
                tolist.Items.Add(e.sMailerName.s + " (" + e.sMailerAddress.s + ")");
            }
            if (EditingMail != null)
            {
                //set selection indices
                fromlist.SelectedIndex = mailersreturned.IndexOf(EditingMail.From);
                tolist.SelectedIndex = mailersreturned.IndexOf(EditingMail.To);
            }

            //titlelist - list of title strings
            stringsreturned = EmailStar.GetStringList(EmailStar.StringCategory.EmailTitle);
            titlelist.Items.Clear();

            foreach (EmailStar.EmailSystemString e in stringsreturned)
            {
                titlelist.Items.Add(e.s);
            }
            if (EditingMail != null)
            {
                //set selection indices
                titlelist.SelectedIndex = stringsreturned.IndexOf(EditingMail.sTitle);
            }

            //contentlist - list of content strings
            stringsreturned = EmailStar.GetStringList(EmailStar.StringCategory.EmailContent);
            contentlist.Items.Clear();

            foreach (EmailStar.EmailSystemString e in stringsreturned)
            {
                contentlist.Items.Add(e.s);
            }
            if (EditingMail != null)
            {
                //set selection indices
                contentlist.SelectedIndex = stringsreturned.IndexOf(EditingMail.sContent);
            }

            if (EditingMail != null)
            {
                //responsesadded
                responsesadded.Items.Clear();
                foreach (EmailStar.Response r in EditingMail.Responses)
                {
                    if (!r.bResponseAutoFires)
                    {
                        responsesadded.Items.Add(r.sResponseTag.s + " : (" + r.ResponseMail.sTitle.s + ") " + r.ResponseMail.sContent.s + "");
                    }
                    else
                    {
                        responsesadded.Items.Add("!AUTO!REPONSE! : (" + r.ResponseMail.sTitle.s + ") " + r.ResponseMail.sContent.s + "");
                    }
                }

                //finally update the settings

                //set other settings
                //delaybox
                delaybox.Text = EditingMail.iDelay + "";
 
                //blocks
                blocks.Checked = EditingMail.bEmailBlocksThread;
            }

            iLastCheckedEditorActions = EmailStar.getEditActionTotal();
        }

        private void AddEmail_focus(object sender, EventArgs e)
        {

            if (iLastCheckedEditorActions != EmailStar.getEditActionTotal())
            {
                RefreshLists();

                //if (EditingMail != null)
                //{
                //    SetIndices();
                //}
            }
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            if (EditingMail != null)
            {
                EditingMail.bEditing = false;

            }
            this.Close();
        }

        private void add_Click(object sender, EventArgs e)
        {
            CommitEmail();
            if (EditingMail != null)
            {
                EditingMail.bEditing = false;

            }
            this.Close();
        }

        void CommitEmail()
        {
            //check for errors
            if (fromlist.SelectedIndex == -1 ||
                tolist.SelectedIndex == -1 || 
                titlelist.SelectedIndex == -1 ||
                contentlist.SelectedIndex == -1)
            {
                MessageBox.Show("From, To, Title and Content must be picked before saving an email.", "Input error");
                return;
            }
            int parseddelay = 0;

            if (delaybox.Text != "")
            {
                try
                {
                    parseddelay = int.Parse(delaybox.Text);
                }
                catch
                {
                    MessageBox.Show("Delay int not set to a valid integer value.","Input error");
                    return;
                }
            }

            if (EditingMail == null)
            {
                EditingMail = new EmailStar.Email();

            }
                //save current settings to editmail
                //from, to, title, content, delay, blocks
                //ignore responses, they're handled elsewhere
            EmailStar.CommitEmail(EditingMail,
                                   fromlist.SelectedIndex,
                                   tolist.SelectedIndex,
                                   titlelist.SelectedIndex,
                                   contentlist.SelectedIndex,
                                   parseddelay,
                                   blocks.Checked);

        }

        private void respadd_Click(object sender, EventArgs e)
        {
            if (EditingMail == null)
            {
                DialogResult res = MessageBox.Show("To add responses this email must be saved, continue?", "Adding response", MessageBoxButtons.YesNo);

                if (res == DialogResult.No)
                {
                    return;
                }
                CommitEmail();
            }

            //othewise open up a response dlg

            Form f = new AddResponse(EditingMail, -1, this);
            f.MdiParent = parentlauncher;
            f.Show();

        }

        private void editresp_Click(object sender, EventArgs e)
        {
            //edit response
            if (responsesadded.Items.Count == 0)
            {
                //no responses to edit
                MessageBox.Show("You must add responses before you can edit them", "Editing response");
                return;
            }
            if (responsesadded.SelectedIndex == -1)
            {
                MessageBox.Show("You must select a response before you can edit it", "Editing response");
                return;
            }

            //attempt to edit the response - add error checking for out of bounds
            Form f = new AddResponse(EditingMail, responsesadded.SelectedIndex, this);
            f.MdiParent = parentlauncher;
            f.Show();

        }

        private void removeresp_Click(object sender, EventArgs e)
        {
            //remove response
            if (responsesadded.Items.Count == 0)
            {
                MessageBox.Show("You must add responses before you can delete them", "Editing response");
            }
            if (responsesadded.SelectedIndex == -1)
            {
                MessageBox.Show("You must select a response before you can edit it", "Editing response");
                return;
            }

            //attempt to delete the response
            EditingMail.Responses.RemoveAt(responsesadded.SelectedIndex);
            //fire a refresh
            RefreshLists();
        }


        internal void RefreshResponseLists()
        {
            RefreshLists();
        }

    }
}
