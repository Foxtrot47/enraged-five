﻿namespace EmailStarPrototype
{
    partial class Threditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ThreadTree = new System.Windows.Forms.TreeView();
            this.To = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ToSelect = new System.Windows.Forms.ComboBox();
            this.FromSelect = new System.Windows.Forms.ComboBox();
            this.TitleSelect = new System.Windows.Forms.ComboBox();
            this.ContentsSelect = new System.Windows.Forms.ComboBox();
            this.TitleEntry = new System.Windows.Forms.TextBox();
            this.ContentsEntry = new System.Windows.Forms.TextBox();
            this.EmailGroup = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.delaycheck = new System.Windows.Forms.CheckBox();
            this.delayamount = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.removeResponse = new System.Windows.Forms.Button();
            this.addResponse = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ResponseTitle = new System.Windows.Forms.TextBox();
            this.responsepickthread = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.selectresponsemail = new System.Windows.Forms.ComboBox();
            this.responseautofire = new System.Windows.Forms.CheckBox();
            this.responsefirethread = new System.Windows.Forms.CheckBox();
            this.responseendsthread = new System.Windows.Forms.CheckBox();
            this.ResponseList = new System.Windows.Forms.ListBox();
            this.EmailStatusLine = new System.Windows.Forms.Label();
            this.CommitContents = new System.Windows.Forms.Button();
            this.CommitTitle = new System.Windows.Forms.Button();
            this.ThreadGroup = new System.Windows.Forms.GroupBox();
            this.ThreadStatusLine = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ThreadName = new System.Windows.Forms.TextBox();
            this.DeleteParticipant = new System.Windows.Forms.Button();
            this.AddParticipant = new System.Windows.Forms.Button();
            this.SelectParticipant = new System.Windows.Forms.ComboBox();
            this.Participants = new System.Windows.Forms.ListBox();
            this.CommitButton = new System.Windows.Forms.Button();
            this.AddEmailToThread = new System.Windows.Forms.Button();
            this.RemoveEmailFromThread = new System.Windows.Forms.Button();
            this.MoveUp = new System.Windows.Forms.Button();
            this.MoveDown = new System.Windows.Forms.Button();
            this.PreviewThread = new System.Windows.Forms.Button();
            this.EmailGroup.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.ThreadGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // ThreadTree
            // 
            this.ThreadTree.Location = new System.Drawing.Point(12, 13);
            this.ThreadTree.Name = "ThreadTree";
            this.ThreadTree.Size = new System.Drawing.Size(282, 635);
            this.ThreadTree.TabIndex = 0;
            this.ThreadTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.ThreadTree_AfterSelect);
            // 
            // To
            // 
            this.To.AutoSize = true;
            this.To.Location = new System.Drawing.Point(24, 51);
            this.To.Name = "To";
            this.To.Size = new System.Drawing.Size(20, 13);
            this.To.TabIndex = 1;
            this.To.Text = "To";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "From";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Title";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Contents";
            // 
            // ToSelect
            // 
            this.ToSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ToSelect.FormattingEnabled = true;
            this.ToSelect.Location = new System.Drawing.Point(78, 51);
            this.ToSelect.Name = "ToSelect";
            this.ToSelect.Size = new System.Drawing.Size(121, 21);
            this.ToSelect.TabIndex = 6;
            this.ToSelect.SelectedIndexChanged += new System.EventHandler(this.ToSelect_SelectedIndexChanged);
            // 
            // FromSelect
            // 
            this.FromSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FromSelect.FormattingEnabled = true;
            this.FromSelect.Location = new System.Drawing.Point(78, 79);
            this.FromSelect.Name = "FromSelect";
            this.FromSelect.Size = new System.Drawing.Size(121, 21);
            this.FromSelect.TabIndex = 7;
            this.FromSelect.SelectedIndexChanged += new System.EventHandler(this.FromSelect_SelectedIndexChanged);
            // 
            // TitleSelect
            // 
            this.TitleSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TitleSelect.FormattingEnabled = true;
            this.TitleSelect.Location = new System.Drawing.Point(78, 111);
            this.TitleSelect.Name = "TitleSelect";
            this.TitleSelect.Size = new System.Drawing.Size(121, 21);
            this.TitleSelect.TabIndex = 8;
            this.TitleSelect.SelectedIndexChanged += new System.EventHandler(this.TitleSelect_SelectedIndexChanged);
            // 
            // ContentsSelect
            // 
            this.ContentsSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ContentsSelect.FormattingEnabled = true;
            this.ContentsSelect.Location = new System.Drawing.Point(78, 147);
            this.ContentsSelect.Name = "ContentsSelect";
            this.ContentsSelect.Size = new System.Drawing.Size(345, 21);
            this.ContentsSelect.TabIndex = 9;
            this.ContentsSelect.SelectedIndexChanged += new System.EventHandler(this.ContentsSelect_SelectedIndexChanged);
            // 
            // TitleEntry
            // 
            this.TitleEntry.Location = new System.Drawing.Point(219, 111);
            this.TitleEntry.Name = "TitleEntry";
            this.TitleEntry.Size = new System.Drawing.Size(204, 20);
            this.TitleEntry.TabIndex = 13;
            this.TitleEntry.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TitleEntry_TextChanged);
            // 
            // ContentsEntry
            // 
            this.ContentsEntry.Location = new System.Drawing.Point(27, 183);
            this.ContentsEntry.Multiline = true;
            this.ContentsEntry.Name = "ContentsEntry";
            this.ContentsEntry.Size = new System.Drawing.Size(428, 112);
            this.ContentsEntry.TabIndex = 14;
            this.ContentsEntry.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ContentsEntry_TextChanged);
            // 
            // EmailGroup
            // 
            this.EmailGroup.Controls.Add(this.groupBox2);
            this.EmailGroup.Controls.Add(this.groupBox1);
            this.EmailGroup.Controls.Add(this.EmailStatusLine);
            this.EmailGroup.Controls.Add(this.CommitContents);
            this.EmailGroup.Controls.Add(this.CommitTitle);
            this.EmailGroup.Controls.Add(this.ContentsEntry);
            this.EmailGroup.Controls.Add(this.To);
            this.EmailGroup.Controls.Add(this.label2);
            this.EmailGroup.Controls.Add(this.TitleEntry);
            this.EmailGroup.Controls.Add(this.label3);
            this.EmailGroup.Controls.Add(this.label4);
            this.EmailGroup.Controls.Add(this.ToSelect);
            this.EmailGroup.Controls.Add(this.ContentsSelect);
            this.EmailGroup.Controls.Add(this.FromSelect);
            this.EmailGroup.Controls.Add(this.TitleSelect);
            this.EmailGroup.Location = new System.Drawing.Point(300, 205);
            this.EmailGroup.Name = "EmailGroup";
            this.EmailGroup.Size = new System.Drawing.Size(470, 472);
            this.EmailGroup.TabIndex = 16;
            this.EmailGroup.TabStop = false;
            this.EmailGroup.Text = "Email";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.delaycheck);
            this.groupBox2.Controls.Add(this.delayamount);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(219, 11);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(236, 70);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Settings";
            // 
            // delaycheck
            // 
            this.delaycheck.AutoSize = true;
            this.delaycheck.Location = new System.Drawing.Point(9, 19);
            this.delaycheck.Name = "delaycheck";
            this.delaycheck.Size = new System.Drawing.Size(95, 17);
            this.delaycheck.TabIndex = 27;
            this.delaycheck.Text = "Delays Thread";
            this.delaycheck.UseVisualStyleBackColor = true;
            this.delaycheck.CheckedChanged += new System.EventHandler(this.delaycheck_CheckedChanged);
            // 
            // delayamount
            // 
            this.delayamount.Location = new System.Drawing.Point(153, 45);
            this.delayamount.Name = "delayamount";
            this.delayamount.Size = new System.Drawing.Size(77, 20);
            this.delayamount.TabIndex = 25;
            this.delayamount.TextChanged += new System.EventHandler(this.delayamount_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Delay degree (seconds)";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.removeResponse);
            this.groupBox1.Controls.Add(this.addResponse);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.ResponseList);
            this.groupBox1.Location = new System.Drawing.Point(9, 301);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(455, 164);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Responses";
            // 
            // removeResponse
            // 
            this.removeResponse.Location = new System.Drawing.Point(168, 54);
            this.removeResponse.Name = "removeResponse";
            this.removeResponse.Size = new System.Drawing.Size(36, 29);
            this.removeResponse.TabIndex = 35;
            this.removeResponse.Text = "-";
            this.removeResponse.UseVisualStyleBackColor = true;
            this.removeResponse.Click += new System.EventHandler(this.removeResponse_Click);
            // 
            // addResponse
            // 
            this.addResponse.Location = new System.Drawing.Point(168, 19);
            this.addResponse.Name = "addResponse";
            this.addResponse.Size = new System.Drawing.Size(36, 29);
            this.addResponse.TabIndex = 34;
            this.addResponse.Text = "+";
            this.addResponse.UseVisualStyleBackColor = true;
            this.addResponse.Click += new System.EventHandler(this.addResponse_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ResponseTitle);
            this.groupBox3.Controls.Add(this.responsepickthread);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.selectresponsemail);
            this.groupBox3.Controls.Add(this.responseautofire);
            this.groupBox3.Controls.Add(this.responsefirethread);
            this.groupBox3.Controls.Add(this.responseendsthread);
            this.groupBox3.Location = new System.Drawing.Point(211, 10);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(238, 146);
            this.groupBox3.TabIndex = 33;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Response Settings";
            // 
            // ResponseTitle
            // 
            this.ResponseTitle.Location = new System.Drawing.Point(9, 19);
            this.ResponseTitle.Name = "ResponseTitle";
            this.ResponseTitle.Size = new System.Drawing.Size(218, 20);
            this.ResponseTitle.TabIndex = 33;
            this.ResponseTitle.TextChanged += new System.EventHandler(this.ResponseTitle_TextChanged);
            // 
            // responsepickthread
            // 
            this.responsepickthread.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.responsepickthread.Enabled = false;
            this.responsepickthread.FormattingEnabled = true;
            this.responsepickthread.Location = new System.Drawing.Point(107, 69);
            this.responsepickthread.Name = "responsepickthread";
            this.responsepickthread.Size = new System.Drawing.Size(121, 21);
            this.responsepickthread.TabIndex = 32;
            this.responsepickthread.SelectedIndexChanged += new System.EventHandler(this.responsepickthread_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 100);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "Response Mail";
            // 
            // selectresponsemail
            // 
            this.selectresponsemail.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.selectresponsemail.FormattingEnabled = true;
            this.selectresponsemail.Location = new System.Drawing.Point(9, 116);
            this.selectresponsemail.Name = "selectresponsemail";
            this.selectresponsemail.Size = new System.Drawing.Size(218, 21);
            this.selectresponsemail.TabIndex = 24;
            // 
            // responseautofire
            // 
            this.responseautofire.AutoSize = true;
            this.responseautofire.Location = new System.Drawing.Point(9, 46);
            this.responseautofire.Name = "responseautofire";
            this.responseautofire.Size = new System.Drawing.Size(73, 17);
            this.responseautofire.TabIndex = 28;
            this.responseautofire.Text = "Auto Fires";
            this.responseautofire.UseVisualStyleBackColor = true;
            this.responseautofire.CheckedChanged += new System.EventHandler(this.responseautofire_CheckedChanged);
            // 
            // responsefirethread
            // 
            this.responsefirethread.AutoSize = true;
            this.responsefirethread.Location = new System.Drawing.Point(117, 46);
            this.responsefirethread.Name = "responsefirethread";
            this.responsefirethread.Size = new System.Drawing.Size(110, 17);
            this.responsefirethread.TabIndex = 31;
            this.responsefirethread.Text = "Fires New Thread";
            this.responsefirethread.UseVisualStyleBackColor = true;
            this.responsefirethread.CheckedChanged += new System.EventHandler(this.responsefirethread_CheckedChanged);
            // 
            // responseendsthread
            // 
            this.responseendsthread.AutoSize = true;
            this.responseendsthread.Location = new System.Drawing.Point(9, 69);
            this.responseendsthread.Name = "responseendsthread";
            this.responseendsthread.Size = new System.Drawing.Size(87, 17);
            this.responseendsthread.TabIndex = 29;
            this.responseendsthread.Text = "Ends Thread";
            this.responseendsthread.UseVisualStyleBackColor = true;
            this.responseendsthread.CheckedChanged += new System.EventHandler(this.responseendsthread_CheckedChanged);
            // 
            // ResponseList
            // 
            this.ResponseList.FormattingEnabled = true;
            this.ResponseList.Location = new System.Drawing.Point(10, 19);
            this.ResponseList.Name = "ResponseList";
            this.ResponseList.Size = new System.Drawing.Size(152, 134);
            this.ResponseList.TabIndex = 24;
            this.ResponseList.SelectedIndexChanged += new System.EventHandler(this.ResponseList_SelectedIndexChanged);
            // 
            // EmailStatusLine
            // 
            this.EmailStatusLine.AutoSize = true;
            this.EmailStatusLine.Location = new System.Drawing.Point(24, 25);
            this.EmailStatusLine.Name = "EmailStatusLine";
            this.EmailStatusLine.Size = new System.Drawing.Size(88, 13);
            this.EmailStatusLine.TabIndex = 21;
            this.EmailStatusLine.Text = "Email Status Line";
            // 
            // CommitContents
            // 
            this.CommitContents.Location = new System.Drawing.Point(429, 147);
            this.CommitContents.Name = "CommitContents";
            this.CommitContents.Size = new System.Drawing.Size(26, 23);
            this.CommitContents.TabIndex = 19;
            this.CommitContents.Text = "+";
            this.CommitContents.UseVisualStyleBackColor = true;
            this.CommitContents.Click += new System.EventHandler(this.CommitContents_Click);
            // 
            // CommitTitle
            // 
            this.CommitTitle.Location = new System.Drawing.Point(429, 109);
            this.CommitTitle.Name = "CommitTitle";
            this.CommitTitle.Size = new System.Drawing.Size(26, 23);
            this.CommitTitle.TabIndex = 18;
            this.CommitTitle.Text = "+";
            this.CommitTitle.UseVisualStyleBackColor = true;
            this.CommitTitle.Click += new System.EventHandler(this.CommitTitle_Click);
            // 
            // ThreadGroup
            // 
            this.ThreadGroup.Controls.Add(this.ThreadStatusLine);
            this.ThreadGroup.Controls.Add(this.label1);
            this.ThreadGroup.Controls.Add(this.ThreadName);
            this.ThreadGroup.Controls.Add(this.DeleteParticipant);
            this.ThreadGroup.Controls.Add(this.AddParticipant);
            this.ThreadGroup.Controls.Add(this.SelectParticipant);
            this.ThreadGroup.Controls.Add(this.Participants);
            this.ThreadGroup.Location = new System.Drawing.Point(300, 13);
            this.ThreadGroup.Name = "ThreadGroup";
            this.ThreadGroup.Size = new System.Drawing.Size(470, 186);
            this.ThreadGroup.TabIndex = 17;
            this.ThreadGroup.TabStop = false;
            this.ThreadGroup.Text = "Thread";
            // 
            // ThreadStatusLine
            // 
            this.ThreadStatusLine.AutoSize = true;
            this.ThreadStatusLine.Location = new System.Drawing.Point(15, 16);
            this.ThreadStatusLine.Name = "ThreadStatusLine";
            this.ThreadStatusLine.Size = new System.Drawing.Size(97, 13);
            this.ThreadStatusLine.TabIndex = 23;
            this.ThreadStatusLine.Text = "Thread Status Line";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Thread Internal Name";
            // 
            // ThreadName
            // 
            this.ThreadName.Location = new System.Drawing.Point(129, 45);
            this.ThreadName.Name = "ThreadName";
            this.ThreadName.Size = new System.Drawing.Size(326, 20);
            this.ThreadName.TabIndex = 4;
            // 
            // DeleteParticipant
            // 
            this.DeleteParticipant.Location = new System.Drawing.Point(219, 100);
            this.DeleteParticipant.Name = "DeleteParticipant";
            this.DeleteParticipant.Size = new System.Drawing.Size(41, 23);
            this.DeleteParticipant.TabIndex = 3;
            this.DeleteParticipant.Text = "-";
            this.DeleteParticipant.UseVisualStyleBackColor = true;
            this.DeleteParticipant.Click += new System.EventHandler(this.DeleteParticipant_Click);
            // 
            // AddParticipant
            // 
            this.AddParticipant.Location = new System.Drawing.Point(219, 71);
            this.AddParticipant.Name = "AddParticipant";
            this.AddParticipant.Size = new System.Drawing.Size(41, 23);
            this.AddParticipant.TabIndex = 2;
            this.AddParticipant.Text = "<-";
            this.AddParticipant.UseVisualStyleBackColor = true;
            this.AddParticipant.Click += new System.EventHandler(this.AddParticipant_Click);
            // 
            // SelectParticipant
            // 
            this.SelectParticipant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SelectParticipant.FormattingEnabled = true;
            this.SelectParticipant.Location = new System.Drawing.Point(266, 71);
            this.SelectParticipant.Name = "SelectParticipant";
            this.SelectParticipant.Size = new System.Drawing.Size(189, 21);
            this.SelectParticipant.TabIndex = 1;
            // 
            // Participants
            // 
            this.Participants.FormattingEnabled = true;
            this.Participants.Location = new System.Drawing.Point(16, 71);
            this.Participants.Name = "Participants";
            this.Participants.Size = new System.Drawing.Size(192, 95);
            this.Participants.TabIndex = 0;
            // 
            // CommitButton
            // 
            this.CommitButton.Location = new System.Drawing.Point(668, 683);
            this.CommitButton.Name = "CommitButton";
            this.CommitButton.Size = new System.Drawing.Size(102, 23);
            this.CommitButton.TabIndex = 18;
            this.CommitButton.Text = "Commit Thread";
            this.CommitButton.UseVisualStyleBackColor = true;
            this.CommitButton.Click += new System.EventHandler(this.CommitButton_Click);
            // 
            // AddEmailToThread
            // 
            this.AddEmailToThread.Location = new System.Drawing.Point(171, 654);
            this.AddEmailToThread.Name = "AddEmailToThread";
            this.AddEmailToThread.Size = new System.Drawing.Size(123, 23);
            this.AddEmailToThread.TabIndex = 19;
            this.AddEmailToThread.Text = "Add Email To Thread";
            this.AddEmailToThread.UseVisualStyleBackColor = true;
            this.AddEmailToThread.Click += new System.EventHandler(this.AddEmailToThread_Click);
            // 
            // RemoveEmailFromThread
            // 
            this.RemoveEmailFromThread.Location = new System.Drawing.Point(171, 683);
            this.RemoveEmailFromThread.Name = "RemoveEmailFromThread";
            this.RemoveEmailFromThread.Size = new System.Drawing.Size(153, 23);
            this.RemoveEmailFromThread.TabIndex = 20;
            this.RemoveEmailFromThread.Text = "Remove Email From Thread";
            this.RemoveEmailFromThread.UseVisualStyleBackColor = true;
            this.RemoveEmailFromThread.Click += new System.EventHandler(this.RemoveEmailFromThread_Click);
            // 
            // MoveUp
            // 
            this.MoveUp.Location = new System.Drawing.Point(12, 654);
            this.MoveUp.Name = "MoveUp";
            this.MoveUp.Size = new System.Drawing.Size(82, 23);
            this.MoveUp.TabIndex = 21;
            this.MoveUp.Text = "Move Up";
            this.MoveUp.UseVisualStyleBackColor = true;
            this.MoveUp.Click += new System.EventHandler(this.MoveUp_Click);
            // 
            // MoveDown
            // 
            this.MoveDown.Location = new System.Drawing.Point(12, 683);
            this.MoveDown.Name = "MoveDown";
            this.MoveDown.Size = new System.Drawing.Size(82, 23);
            this.MoveDown.TabIndex = 22;
            this.MoveDown.Text = "Move Down";
            this.MoveDown.UseVisualStyleBackColor = true;
            this.MoveDown.Click += new System.EventHandler(this.MoveDown_Click);
            // 
            // PreviewThread
            // 
            this.PreviewThread.Location = new System.Drawing.Point(396, 683);
            this.PreviewThread.Name = "PreviewThread";
            this.PreviewThread.Size = new System.Drawing.Size(153, 23);
            this.PreviewThread.TabIndex = 23;
            this.PreviewThread.Text = "Preview this thread";
            this.PreviewThread.UseVisualStyleBackColor = true;
            this.PreviewThread.Click += new System.EventHandler(this.PreviewThread_Click);
            // 
            // Threditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 711);
            this.Controls.Add(this.PreviewThread);
            this.Controls.Add(this.MoveDown);
            this.Controls.Add(this.MoveUp);
            this.Controls.Add(this.RemoveEmailFromThread);
            this.Controls.Add(this.AddEmailToThread);
            this.Controls.Add(this.CommitButton);
            this.Controls.Add(this.ThreadGroup);
            this.Controls.Add(this.EmailGroup);
            this.Controls.Add(this.ThreadTree);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Threditor";
            this.ShowIcon = false;
            this.Text = "Threditor";
            this.EmailGroup.ResumeLayout(false);
            this.EmailGroup.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ThreadGroup.ResumeLayout(false);
            this.ThreadGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView ThreadTree;
        private System.Windows.Forms.Label To;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox ToSelect;
        private System.Windows.Forms.ComboBox TitleSelect;
        private System.Windows.Forms.ComboBox ContentsSelect;
        private System.Windows.Forms.TextBox TitleEntry;
        private System.Windows.Forms.TextBox ContentsEntry;
        private System.Windows.Forms.GroupBox EmailGroup;
        private System.Windows.Forms.GroupBox ThreadGroup;
        private System.Windows.Forms.Button CommitContents;
        private System.Windows.Forms.Button CommitTitle;
        private System.Windows.Forms.ComboBox SelectParticipant;
        private System.Windows.Forms.ListBox Participants;
        private System.Windows.Forms.Button DeleteParticipant;
        private System.Windows.Forms.Button AddParticipant;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ThreadName;
        private System.Windows.Forms.Button CommitButton;
        private System.Windows.Forms.Label EmailStatusLine;
        private System.Windows.Forms.ComboBox FromSelect;
        private System.Windows.Forms.Label ThreadStatusLine;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox ResponseList;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox delaycheck;
        private System.Windows.Forms.TextBox delayamount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox selectresponsemail;
        private System.Windows.Forms.CheckBox responseendsthread;
        private System.Windows.Forms.CheckBox responseautofire;
        private System.Windows.Forms.ComboBox responsepickthread;
        private System.Windows.Forms.CheckBox responsefirethread;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox ResponseTitle;
        private System.Windows.Forms.Button removeResponse;
        private System.Windows.Forms.Button addResponse;
        private System.Windows.Forms.Button AddEmailToThread;
        private System.Windows.Forms.Button RemoveEmailFromThread;
        private System.Windows.Forms.Button MoveUp;
        private System.Windows.Forms.Button MoveDown;
        private System.Windows.Forms.Button PreviewThread;
    }
}