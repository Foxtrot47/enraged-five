﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmailStarPrototype
{
    public partial class Mainform : Form
    {
   

        public Mainform()
        {
            InitializeComponent();

            
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //throw new NotImplementedException();

        }
        /*
        private void threadadd_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = new ListViewItem("Testing this");
            //EmailStar.AddThread();
            //ThreadList.View = View.List;
            //ThreadList.Items.Add(lvi);
        }*/

        public void IssueRefresh()
        {

        }

        private void loadThreadSetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //call load from project
            //MessageBox.Show("Loading unimplemented", "Unimplemented function");
            openFileDLG.Filter = "Email Star database (*.est)|*.est";
            DialogResult res = openFileDLG.ShowDialog();
            if (res == DialogResult.OK || res == DialogResult.Yes)
            {
                //close all open forms
                foreach (Form f in this.MdiChildren)
                {
                    f.Close();
                }

                EmailStar.LoadSystem(openFileDLG.FileName);

                Form fi = new DataView((Form)this);
                if (!fi.IsDisposed)
                {
                    fi.MdiParent = this;
                    fi.Show();
                }
            }

        }

        private void saveThreadSetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //call save from project
            //MessageBox.Show("Saving unimplemented", "Unimplemented function");
            saveFileDLG.Filter = "Email Star database (*.est)|*.est";
            DialogResult res = saveFileDLG.ShowDialog();
            if (res == DialogResult.OK || res == DialogResult.Yes)
            {
                //throw new NotImplementedException();
                EmailStar.SaveSystem(saveFileDLG.FileName);
            }
        }

        private void exportForGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //call game file export
            //MessageBox.Show("Export unimplemented", "Unimplemented function");
            DialogResult res = folderBrowserDialog.ShowDialog();
            if (res == DialogResult.OK || res == DialogResult.Yes)
            {
                //throw new NotImplementedException();
                EmailStar.ExportRAGEFiles(folderBrowserDialog.SelectedPath);
            }
        }

        private void addstring_Click(object sender, EventArgs e)
        {
            //open the string adding dlg
            //Application.Run(new AddString());
            //this.ShowDialog();
            Form f = new AddString();
            if (!f.IsDisposed)
            {
                f.MdiParent = this;
                f.Show();
            }
        }

        private void addemailer_Click(object sender, EventArgs e)
        {
            Form f = new AddEmailer();
            if (!f.IsDisposed)
            {
                f.MdiParent = this;
                f.Show();
            }
        }

        private void AddEmail_Click(object sender, EventArgs e)
        {
            Form f = new AddEmail((Form)this);
            if (!f.IsDisposed)
            {
                f.MdiParent = this;
                f.Show();
            }
        }

        private void addthread_Click(object sender, EventArgs e)
        {
            Form f = new AddThread();
            if (!f.IsDisposed)
            {
                f.MdiParent = this;
                f.Show();
            }
        }

        private void dataview_Click(object sender, EventArgs e)
        {
            Form f = new DataView((Form)this);
            if (!f.IsDisposed)
            {
                f.MdiParent = this;
                f.Show();
            }
        }

        private void addStringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new AddString();
            if (!f.IsDisposed)
            {
                f.MdiParent = this;
                f.Show();
            }
        }

        private void addEmailerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new AddEmailer();
            if (!f.IsDisposed)
            {
                f.MdiParent = this;
                f.Show();
            }
        }

        private void addEmailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new AddEmail((Form)this);
            if (!f.IsDisposed)
            {
                f.MdiParent = this;
                f.Show();
            }
        }

        private void addThreadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new AddThread();
            if (!f.IsDisposed)
            {
                f.MdiParent = this;
                f.Show();
            }
        }

        private void dataViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new DataView((Form)this);
            if (!f.IsDisposed)
            {
                f.MdiParent = this;
                f.Show();
            }
        }

        private void exportToXgta5ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmailStar.ExportRAGEFiles("toscript");
        }

        private void openThreditorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new Threditor(this);
            if (!f.IsDisposed)
            {
                f.MdiParent = this;
                f.Show();
            }
        }


    }
}
