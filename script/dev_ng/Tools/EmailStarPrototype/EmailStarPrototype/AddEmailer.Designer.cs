﻿namespace EmailStarPrototype
{
    partial class AddEmailer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.namelist = new System.Windows.Forms.ComboBox();
            this.emaillist = new System.Windows.Forms.ComboBox();
            this.signofflist = new System.Windows.Forms.ComboBox();
            this.addm = new System.Windows.Forms.Button();
            this.cancelm = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Email Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "SignOff (optional)";
            // 
            // namelist
            // 
            this.namelist.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.namelist.FormattingEnabled = true;
            this.namelist.Location = new System.Drawing.Point(125, 6);
            this.namelist.Name = "namelist";
            this.namelist.Size = new System.Drawing.Size(202, 21);
            this.namelist.TabIndex = 4;
            // 
            // emaillist
            // 
            this.emaillist.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.emaillist.FormattingEnabled = true;
            this.emaillist.Location = new System.Drawing.Point(125, 33);
            this.emaillist.Name = "emaillist";
            this.emaillist.Size = new System.Drawing.Size(202, 21);
            this.emaillist.TabIndex = 5;
            // 
            // signofflist
            // 
            this.signofflist.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.signofflist.FormattingEnabled = true;
            this.signofflist.Location = new System.Drawing.Point(125, 61);
            this.signofflist.Name = "signofflist";
            this.signofflist.Size = new System.Drawing.Size(202, 21);
            this.signofflist.TabIndex = 6;
            // 
            // addm
            // 
            this.addm.Location = new System.Drawing.Point(171, 104);
            this.addm.Name = "addm";
            this.addm.Size = new System.Drawing.Size(75, 23);
            this.addm.TabIndex = 7;
            this.addm.Text = "Add";
            this.addm.UseVisualStyleBackColor = true;
            this.addm.Click += new System.EventHandler(this.addm_Click);
            // 
            // cancelm
            // 
            this.cancelm.Location = new System.Drawing.Point(252, 104);
            this.cancelm.Name = "cancelm";
            this.cancelm.Size = new System.Drawing.Size(75, 23);
            this.cancelm.TabIndex = 8;
            this.cancelm.Text = "Cancel";
            this.cancelm.UseVisualStyleBackColor = true;
            this.cancelm.Click += new System.EventHandler(this.cancelm_Click);
            // 
            // AddEmailer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 130);
            this.ControlBox = false;
            this.Controls.Add(this.cancelm);
            this.Controls.Add(this.addm);
            this.Controls.Add(this.signofflist);
            this.Controls.Add(this.emaillist);
            this.Controls.Add(this.namelist);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AddEmailer";
            this.Text = "AddEmailer";
            this.Enter += new System.EventHandler(this.AddEmailer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox namelist;
        private System.Windows.Forms.ComboBox emaillist;
        private System.Windows.Forms.ComboBox signofflist;
        private System.Windows.Forms.Button addm;
        private System.Windows.Forms.Button cancelm;

    }
}