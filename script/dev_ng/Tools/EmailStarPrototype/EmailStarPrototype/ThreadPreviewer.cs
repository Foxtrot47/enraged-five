﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace EmailStarPrototype
{
    //Used to simulate a thread based on it's data
    public partial class ThreadPreviewer : Form
    {
        EmailStar.Thread ThreadIn;
        int SelectedInbox = 0;

        public ThreadPreviewer(EmailStar.Thread threadin)
        {
            if (threadin.participants.Count < 2)
            {
                MessageBox.Show("Cannot preview a thread with less than 2 participant");
                this.Close();
            }
            if (threadin.mails.Count < 1)
            {
                MessageBox.Show("Cannot preview a thread with less than 1 mail");
                this.Close();
            }
            if (threadin.bEditing)
            {
                MessageBox.Show("Cannot preview a thread that is being edited!");
                this.Close();
            }
            this.Load += new System.EventHandler(this.ThreadPreviewer_onclose);
            threadin.bEditing = true;

            ThreadIn = threadin;
            InitializeComponent();

            //fill out the selectemailertoview from the ThreadIn participants
                //select index 0
            foreach (EmailStar.Emailer e in ThreadIn.participants)
            {
                selectemailertoview.Items.Add(e.sMailerAddress.s + "");
            }
            selectemailertoview.SelectedIndex = 0;

            EmailStar.ResetThreadPreview(ThreadIn);
            responsebox.Enabled = false;
            responsepick.Enabled = false;
            RefreshMenusFromThread(false);
            
        }


        void RefreshMenusFromThread(bool mailfocusupdate)
        {
            //TODO, this method is a bundle of crap, refactor
            responsepick.Enabled = false;
            responsebox.Enabled = false;
            //fill out the menus based on ThreadIn and SelectedInbox
            //recmails
            //preserve selected if there are enough items

            //only refresh the email list if it doesn't have focus
            if (!mailfocusupdate)
            {
                // int preserveselection = recmails.SelectedIndex;

                recmails.Items.Clear();

                int totalmails = ThreadIn.DebugThreadLogMails.Count;
                //backwards so most recent is at the top
                for (int i = totalmails - 1; i > -1; --i)
                {

                    bool doaddmail = false;
                    //filter to exclude mails sent by the current inbox owner, unless they were sent to self
                    if (ThreadIn.DebugThreadLogMails[i].From != ThreadIn.participants[SelectedInbox])
                    {
                        //not from inbox owner, show

                        doaddmail = true;
                    }
                    else
                    {
                        //is from inbox owner but is also adressed directly TO inbox owner, show anyway
                        if (ThreadIn.DebugThreadLogMails[i].To == ThreadIn.participants[SelectedInbox])
                        {
                            doaddmail = true;
                        }
                    }
                    if (doaddmail)
                    {
                        recmails.Items.Add(ThreadIn.DebugThreadLogMails[i].sTitle.s);
                    }
                }
                //re-enstate the selection
                /*
                if (preserveselection != -1)
                {
                    if (!(preserveselection > (totalmails - 1)))
                    {
                        recmails.SelectedIndex = preserveselection;
                    }
                }*/
            }



                //
            if (recmails.SelectedIndex != -1)
            {
                //emailcontent //from selected index of recmails

                //figure out the actual index of the selected email in the debug log
                //emailcontent.Text = EmailStar.GetPreviewMailContentForThread(ThreadIn, SelectedInbox);
                //recmails.SelectedIndex
                int totalmails = ThreadIn.DebugThreadLogMails.Count;
                int atinlist = 0;
                bool foundindex = false;
                for (int i = totalmails - 1; (i > -1)&& !foundindex; --i)
                {
                    //for every mail that would be added use atinlist to track the menu index
                    bool docheckmail = false;
                    if (ThreadIn.DebugThreadLogMails[i].From != ThreadIn.participants[SelectedInbox])
                    {
                        //not from self
                        docheckmail = true;
                    }
                    else
                    {
                        //check if TO self
                        if (ThreadIn.DebugThreadLogMails[i].To == ThreadIn.participants[SelectedInbox])
                        {
                            docheckmail = true;
                        }
                    }
                    if (docheckmail)
                    {
                        if (recmails.SelectedIndex == atinlist)
                        {
                            //found our index, find the string
                            emailcontent.Text = EmailStar.GetPreviewMailContentForThread(ThreadIn, SelectedInbox,i);
                            foundindex = true;
                        }
                        else
                        {
                            //carry on looking
                            ++atinlist;
                        }
                    }
                }


                //if the selected email has responses to pick, then fill out responsebox  //do not preserve selection
                responsebox.Items.Clear();
                bool ResponsesFilledOut = false;
                //is the selected email the last one?
                if (recmails.SelectedIndex == 0)
                {
                    //does this email have responses
                    //find the mail in question
                    totalmails = ThreadIn.DebugThreadLogMails.Count;
                    //backwards so most recent is at the top
                    for (int i = totalmails - 1; i > -1; --i)
                    {
                        //filter to exclude mails sent by the current inbox owner, unless they were sent to self
                        bool docheckmail = false;
                        if (ThreadIn.DebugThreadLogMails[i].From != ThreadIn.participants[SelectedInbox])
                        {
                            //not from self
                            docheckmail = true;
                        }
                        else
                        {
                            //check if TO self
                            if (ThreadIn.DebugThreadLogMails[i].To == ThreadIn.participants[SelectedInbox])
                            {
                                docheckmail = true;
                            }
                        }
                        if (docheckmail)
                        {
                            if (i == (totalmails - 1))
                            {
                                //if here then the first email displayed is also the first in the list
                                //so eligible for replies
                                if (ThreadIn.DebugThreadLogMails[i].Responses.Count > 0)
                                {
                                    //there are responses!
                                    //fill out responsebox!
                                    responsebox.Enabled = true;
                                    responsepick.Enabled = true;
                                    ResponsesFilledOut = true;
                                    foreach (EmailStar.Response r in ThreadIn.DebugThreadLogMails[i].Responses)
                                    {
                                        responsebox.Items.Add(r.sResponseTag.s + " (" + r.ResponseMail.sTitle.s + ")");
                                    }
                                }
                            }
                        }
                    }
                }
                //otherwise disable the responses

                responsebox.Enabled = ResponsesFilledOut;
                responsepick.Enabled = ResponsesFilledOut;
            }

            //fill out status strings
                //threadstatus
                    //Thread at stage X, awaiting response from X   
            if (ThreadIn.bDebugBlockedUntilResponse)
            {
                threadstatus.Text = "Thread at stage " + (ThreadIn.iDebugCurrentlyAt) + " awaiting response from " + ThreadIn.DebugThreadLogMails[ThreadIn.DebugThreadLogMails.Count - 1].To.sMailerAddress.s;
            }
            else
            {
                if (!ThreadIn.bDebugThreadEnded)
                {
                    threadstatus.Text = "Thread at stage " + (ThreadIn.iDebugCurrentlyAt) + " pending delay progression.";
                }
                else
                {
                    threadstatus.Text = "Thread ended at stage " + (ThreadIn.iDebugCurrentlyAt) + ".";
                }
            }
                    
                //blockedornot  
                    //Blocked/not blocked
            if (!ThreadIn.bDebugThreadEnded)
            {
                if (ThreadIn.bDebugBlockedUntilResponse)
                {
                    blockedornot.Text = "Blocked pending response.";

                    if (ThreadIn.iDebugCurrentlyAt == ThreadIn.mails.Count)
                    {
                        forceduration.Text = "No mails left";
                        forceduration.Enabled = false;
                    }else{
                        forceduration.Text = "Force past block";
                        forceduration.Enabled = true;
                    }
                }
                else
                {
                    blockedornot.Text = "Not blocked";
                    forceduration.Text = "Step past duration";
                    forceduration.Enabled = true;
                }
            }
            else
            {
                //thread over
                blockedornot.Text = "Thread finished yo.";
                forceduration.Text = "Thread over = very yes";
                forceduration.Enabled = false;
            }
                //stagestate
                    //Stage X of X
            stagestate.Text = "Stage " + (ThreadIn.iDebugCurrentlyAt) + " of " + ThreadIn.mails.Count + "";


            if (ThreadIn.iDebugDelayingFor == 0)
            {
                delayamount.Text = "No delay added from last mail";
            }
            else
            {
                delayamount.Text = "Delay of " + ThreadIn.iDebugDelayingFor + " from last mail";
            }


            //update the action log

            actionlog.Items.Clear();
            foreach (string s in ThreadIn.DebugPreviewActionLog)
            {
                actionlog.Items.Add(s);
            }
            //set focus to last action
            actionlog.SelectedIndex = (actionlog.Items.Count - 1);
        }


        private void selectemailertoview_SelectedIndexChanged(object sender, EventArgs e)
        {
            //change the selected inbox - SelectedInbox
            SelectedInbox = selectemailertoview.SelectedIndex;

            //refresh
            RefreshMenusFromThread(false);
        }

        private void ThreadPreviewer_onclose(object sender, EventArgs e)
        {
            if (ThreadIn != null)
            {
                ThreadIn.bEditing = false;
                EmailStar.ResetThreadPreview(ThreadIn);

            }
        }

        private void recmails_SelectedIndexChanged(object sender, EventArgs e)
        {
            //change the selected email 
            RefreshMenusFromThread(true);
        }

        private void responsepick_Click(object sender, EventArgs e)
        {
            //a response picked! fire it
            if (responsebox.SelectedIndex != -1)
            {
                EmailStar.FirePreviewResponse(ThreadIn, responsebox.SelectedIndex);

                RefreshMenusFromThread(false);
            }
        }

        private void resetpreview_Click(object sender, EventArgs e)
        {
            //reset the thread data
            EmailStar.ResetThreadPreview(ThreadIn);

            //refresh the menus
            RefreshMenusFromThread(false);
        }

        private void forceduration_Click(object sender, EventArgs e)
        {
            //step to next part of email
            EmailStar.ProgressThreadPreview(ThreadIn);
            //update fields
            RefreshMenusFromThread(false);
        }


    }
}
