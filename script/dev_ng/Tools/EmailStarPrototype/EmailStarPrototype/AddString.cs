﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmailStarPrototype
{
    public partial class AddString : Form
    {
        EmailStar.EmailSystemString Editable;

        public AddString()
        {
            Editable = new EmailStar.EmailSystemString();
            InitializeComponent();
        }
        public AddString(EmailStar.EmailSystemString toedit)
        {
            //todo, error checking
            Editable = toedit;
            InitializeComponent();
        }

        private void cancelbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void addbutton_Click(object sender, EventArgs e)
        {
            //check for a category being picked
            EmailStar.StringCategory sc;

            if (pickcategory.SelectedIndex == -1)
            {
                MessageBox.Show("A category must be picked.", "Input error");
                return;
            }
            else
            {
                //set the category to pass in 
                switch(pickcategory.SelectedIndex)
                {
                    //Emailer Name // 0
                    case 0:
                        sc = EmailStar.StringCategory.EmailerName;
                        break;
                    //Emailer Address //1
                    case 1:
                        sc = EmailStar.StringCategory.EmailerAddress;
                        break;
                    //Emailer Signoff //2
                    case 2:
                        sc = EmailStar.StringCategory.EmailerSignoff;
                        break;
                    //Email Content //3
                    case 3:
                        sc = EmailStar.StringCategory.EmailContent;
                        break;
                    //Email Title //4 
                    case 4:
                        sc = EmailStar.StringCategory.EmailTitle;
                        break;
                    //Response Title //5
                    case 5:
                        sc = EmailStar.StringCategory.ResponseTitle;
                        break;
                    default:
                        MessageBox.Show("Category type is unhandled! (" + pickcategory.Text + " " + pickcategory.SelectedIndex + ") please contact whoever is maintaining this application.", "Input error");
                        return;
                }
            }

            if (stringcontents.Text != "")
            {
                //yes this is bad practise but sod it
                Editable.s = stringcontents.Text;
                Editable.myCategory = sc;

                //attempt to add the string to EmailStar
                EmailStar.AddString(Editable);

                if (AutoRe.Checked)
                {
                    EmailStar.EmailSystemString restring = new EmailStar.EmailSystemString("Re: " + stringcontents.Text);
                    restring.myCategory = sc;
                    EmailStar.AddString(restring);
                }
                

                if(keepOpen.Checked)
                {
                    stringcontents.Clear();
                    stringcontents.Focus();
                    Editable = new EmailStar.EmailSystemString();
                }else{
                    this.Close();
                }
            }
            else
            {
                //do a popup error or something
                MessageBox.Show("Cannot add a null string!", "Input error");

            }
        }

        private void returnbind(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r') //this is crap, change it later
            {
                //add this!
                addbutton_Click(sender, null);//TODO: calling this is a bit cheeky, functionality should be moved

            }
        }
    }
}
