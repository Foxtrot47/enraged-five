﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmailStarPrototype
{
    public partial class DataView : Form
    {
        static DataView instance = null;

        Form parentform;

        public DataView(Form parent)
        {
            parentform = parent;

            InitializeComponent();

            //namelist
            namelist.View = View.List;
            //addresslist
                //addresslist.View
            addresslist.View = View.List;
            //signslist
                //signslist.View
            signslist.View = View.List;
            //mailcontentlist
                //mailcontentlist.View
            mailcontentlist.View = View.List;
            //mailtitlelist
                //mailtitlelist.View
            mailtitlelist.View = View.List;
            //responselist
                //responselist.View
            responselist.View = View.List;
            //mailerlist
                //mailerlist.View
            mailerlist.View = View.List;
            //emailslist 
                //emailslist.View
            emailslist.View = View.List;
            //threadlist
                //threadlist.View
            threadlist.View = View.List;
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                if (!instance.IsDisposed)
                {
                    this.Close();
                }
                else
                {
                    instance = this;
                }
            }
        }

        private void DataView_onenter(object sender, EventArgs e)
        {
            //start refresh stack
            /*
            switch(tabs.SelectedIndex)
            {
                //strings
                case 0:
                    break;
                //mailers
                case 1:
                    break;
                //emails
                case 2:
                    break;
                //threads
                case 3:
                    break;
            }*/
            RefreshStrings();
            RefreshMailers();
            RefreshEmails();
            RefreshThreads();

        }

        void RefreshStrings()
        {
            List<EmailStar.EmailSystemString> ess;

            //namelist
            namelist.Clear();
            ess = EmailStar.GetStringList(EmailStar.StringCategory.EmailerName);
            foreach(EmailStar.EmailSystemString e in ess)
            {
                namelist.Items.Add("\"" + e.s + "\"");
            }
            //addresslist
            addresslist.Clear();
            ess = EmailStar.GetStringList(EmailStar.StringCategory.EmailerAddress);
            foreach (EmailStar.EmailSystemString e in ess)
            {
                addresslist.Items.Add("\"" + e.s + "\"");
            }
            //signslist
            signslist.Clear();
            ess = EmailStar.GetStringList(EmailStar.StringCategory.EmailerSignoff);
            foreach (EmailStar.EmailSystemString e in ess)
            {
                signslist.Items.Add("\"" + e.s + "\"");
            }
            //mailcontentlist
            mailcontentlist.Clear();
            ess = EmailStar.GetStringList(EmailStar.StringCategory.EmailContent);
            foreach (EmailStar.EmailSystemString e in ess)
            {
                mailcontentlist.Items.Add("\"" + e.s + "\"");
            }
            //mailtitlelist
            mailtitlelist.Clear();
            ess = EmailStar.GetStringList(EmailStar.StringCategory.EmailTitle);
            foreach (EmailStar.EmailSystemString e in ess)
            {
                mailtitlelist.Items.Add("\"" + e.s + "\"");
            }
            //responselist
            responselist.Clear();
            ess = EmailStar.GetStringList(EmailStar.StringCategory.ResponseTitle);
            foreach (EmailStar.EmailSystemString e in ess)
            {
                responselist.Items.Add("\"" + e.s + "\"");
            }


        }
        void RefreshMailers()
        {
            //mailerlist - fill
            List<EmailStar.Emailer> mlrs= EmailStar.GetEmailers();
            mailerlist.Clear();

            foreach (EmailStar.Emailer e in mlrs)
            {
                mailerlist.Items.Add(e.sMailerName.s + " : " + e.sMailerAddress.s);
            }

            
        }
        void RefreshEmails()
        {
            //emailslist - fill
            List<EmailStar.Email> mails = EmailStar.GetEmails();
            emailslist.Clear();

            foreach (EmailStar.Email e in mails)
            {
                emailslist.Items.Add(e.sTitle.s + " (TO: " + e.To.sMailerAddress.s + ") (FROM: " + e.From.sMailerAddress.s + ")" + " (Content : " +e.sContent.s+ ")");
            }
        }
        void RefreshThreads()
        {
            //threadlist
            List<EmailStar.Thread> threads = EmailStar.GetThreads();
            threadlist.Items.Clear();

            foreach(EmailStar.Thread t in threads)
            {
                threadlist.Items.Add(t.sThreadInternalTag + "");
            }

        }


        private void edmailer_Click(object sender, EventArgs e)
        {
            if (mailerlist.SelectedIndices.Count < 1)
            {
                MessageBox.Show("Please select an emailer");
                return;
            }

            foreach (int i in mailerlist.SelectedIndices)
            {
                Form f = new AddEmailer(i);
                if (!f.IsDisposed)
                {
                    f.MdiParent = parentform;
                    f.Show();
                }
            }
        }

        private void edmails_Click(object sender, EventArgs e)
        {
            if (emailslist.SelectedIndices.Count < 1)
            {
                MessageBox.Show("Please select an email");
                return;
            }

            foreach (int i in emailslist.SelectedIndices)
            {
                Form f = new AddEmail(EmailStar.GetEmails()[i], parentform);
                if (!f.IsDisposed)
                {
                    f.MdiParent = parentform;
                    f.Show();
                }
            }
        }

        private void edthread_Click(object sender, EventArgs e)
        {

            if (threadlist.SelectedIndices.Count < 1)
            {
                MessageBox.Show("Please select a thread");
                return;
            }

            foreach (int i in threadlist.SelectedIndices)
            {
                Form f = new AddThread(EmailStar.GetThreads()[i]);
                if (!f.IsDisposed)
                {
                    f.MdiParent = parentform;
                    f.Show();
                }
            }
        }

        private void edthreditor_Click(object sender, EventArgs e)
        {
            if (threadlist.SelectedIndices.Count < 1)
            {
                MessageBox.Show("Please select a thread");
                return;
            }

            foreach (int i in threadlist.SelectedIndices)
            {
                Form f = new Threditor(EmailStar.GetThreads()[i], parentform);
                if (!f.IsDisposed)
                {
                    f.MdiParent = parentform;
                    f.Show();
                }
            }
        }

        private void deletemailer_Click(object sender, EventArgs e)
        {


            if (mailerlist.SelectedIndices.Count < 1)
            {
                MessageBox.Show("Please select a mailer");
                return;
            }
            if (MessageBox.Show("Currently this can fuck up your data if you don't clear out everything that this element is linked to. \n\nProceed anyway?", "WARNING", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }
            //build a deletion list, ignoring any that are open
            List<EmailStar.Emailer> mailers = EmailStar.GetEmailers();
            List<EmailStar.Emailer> deletionlist = new List<EmailStar.Emailer>();

            foreach (int i in mailerlist.SelectedIndices)
            {
                if (!mailers[i].bEditing)
                {
                    deletionlist.Add(mailers[i]);
                }
            }
            //carry out all valid deletions

            foreach (EmailStar.Emailer em in deletionlist)
            {
                mailers.Remove(em);
            }
            RefreshMailers();
        }

        private void deleteemail_Click(object sender, EventArgs e)
        {


            if (emailslist.SelectedIndices.Count < 1)
            {
                MessageBox.Show("Please select an email");
                return;
            }
            if (MessageBox.Show("Currently this can fuck up your data if you don't clear out everything that this element is linked to. \n\nProceed anyway?", "WARNING", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }
            //build a deletion list, ignoring any that are open
            List<EmailStar.Email> mail = EmailStar.GetEmails();
            List<EmailStar.Email> deletionlist = new List<EmailStar.Email>();

            foreach (int i in emailslist.SelectedIndices)
            {
                if (!mail[i].bEditing)
                {
                    deletionlist.Add(mail[i]);
                }
            }
            //carry out all valid deletions

            foreach (EmailStar.Email em in deletionlist)
            {
                mail.Remove(em);
            }
            RefreshEmails();
        }

        private void deletethreads_Click(object sender, EventArgs e)
        {

            if (threadlist.SelectedIndices.Count < 1)
            {
                MessageBox.Show("Please select a thread");
                return;
            }
            if (MessageBox.Show("Currently this can fuck up your data if you don't clear out everything that this element is linked to. \n\nProceed anyway?", "WARNING", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }
            //build a deletion list, ignoring any that are open
            List<EmailStar.Thread> thread = EmailStar.GetThreads();
            List<EmailStar.Thread> deletionlist = new List<EmailStar.Thread>();

            foreach (int i in threadlist.SelectedIndices)
            {
                if (!thread[i].bEditing)
                {
                    deletionlist.Add(thread[i]);
                }
            }
            //carry out all valid deletions

            foreach (EmailStar.Thread em in deletionlist)
            {
                thread.Remove(em);
            }
            RefreshThreads();
        }

        private void prevthread_Click(object sender, EventArgs e)
        {
            if (threadlist.SelectedIndices.Count < 1)
            {
                MessageBox.Show("Please select a thread");
                return;
            }

            foreach (int i in threadlist.SelectedIndices)
            {
                Form f = new ThreadPreviewer(EmailStar.GetThreads()[i]);
                if (!f.IsDisposed)
                {
                    f.MdiParent = parentform;
                    f.Show();
                }
            }
        }



    }
}
