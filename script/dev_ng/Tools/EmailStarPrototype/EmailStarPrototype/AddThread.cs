﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmailStarPrototype
{
    public partial class AddThread : Form
    {

        EmailStar.Thread editThread = null;

        List<EmailStar.Email> MailsSelected;
        List<EmailStar.Emailer> ParticipantsSelected;

        public AddThread()
        {
            //MailsSelected = new List<int>();
            //ParticipantsSelected = new List<int>();

            MailsSelected = new List<EmailStar.Email>();
            ParticipantsSelected = new List<EmailStar.Emailer>();
            InitializeComponent();
           
        }

        public AddThread(EmailStar.Thread toedit)
        {
            //MailsSelected = new List<int>();
            if (!toedit.bEditing)
            {
                toedit.bEditing = true;
                //ParticipantsSelected = new List<int>();
                editThread = toedit;
                MailsSelected = new List<EmailStar.Email>();
                ParticipantsSelected = new List<EmailStar.Emailer>();
                InitializeComponent();
                internalname.Text = toedit.sThreadInternalTag;
                //intialise MailsSelected/emails and ParticipantsSelected/participants
                //with details from editThread
                List<EmailStar.Email> mailsin = editThread.mails;

                foreach (EmailStar.Email e in mailsin)
                {
                    MailsSelected.Add(e);
                    emails.Items.Add(e.sTitle.s + "(to:" + e.To.sMailerAddress.s + ") " + e.sContent.s +"");
                }

                List<EmailStar.Emailer> mailersin = editThread.participants;
                foreach (EmailStar.Emailer em in mailersin)
                {
                    ParticipantsSelected.Add(em);
                    participants.Items.Add(em.sMailerAddress.s + "");
                }
            }
            else
            {
                this.Close();
            }
        }

        private void AddThread_onenter(object sender, EventArgs e)
        {
            RefreshLists();
            //update stored lists for this thread
            //editThread.sThreadInternalTag

            //if this has 

        }

        public void RefreshLists()
        {
            if (editThread == null)
            {
                indexstatuslable.Text = "Thread unsaved, no index set";
            }
            else
            {
                indexstatuslable.Text = "Thread saved at index " + 
                                        EmailStar.GetThreads().IndexOf(editThread) + 
                                        " use this to refer to this thread in script."; 
            }
            //fill out picking lists
            //maillist
            maillist.Items.Clear();
            //EmailStar.GetEmails
            List<EmailStar.Email> mails = EmailStar.GetEmails();

            foreach (EmailStar.Email m in mails)
            {
                maillist.Items.Add(m.sTitle.s + " (TO:" + m.To.sMailerAddress.s + ") " + m.sContent.s + "");
            }
            //pickparticipant
            pickparticipant.Items.Clear();
            //EmailStar.GetEmailers
            List<EmailStar.Emailer> mailers = EmailStar.GetEmailers();

            foreach (EmailStar.Emailer mlr in mailers)
            {
                pickparticipant.Items.Add(mlr.sMailerAddress.s + "");
            }
            ////////////////////////////////////////////
            //
        }

        private void cancelb_Click(object sender, EventArgs e)
        {
            if (editThread != null)
            {
                editThread.bEditing = false;
            }
            this.Close();
        }

        private void addb_Click(object sender, EventArgs e)
        {
            //parse the picked lists and create or edit the thread
            //create the thread and add MailsSelected, and ParticipantsSelected

            //MailsSelected and ParticipantsSelected
            if (internalname.Text == "")
            {
                MessageBox.Show("Please choose an internal name for this thread");
            }

            EmailStar.CommitThread(editThread, internalname.Text, MailsSelected, ParticipantsSelected);
            if (editThread != null)
            {
                editThread.bEditing = false;
            }
           //then close
            this.Close();
        }

        private void addemailb_Click(object sender, EventArgs e)
        {
            //add to emails // and MailsSelected
            //from maillist
            if (maillist.SelectedIndex == -1)
            {
                MessageBox.Show("Please select an email");
                return;
            }

            //otherwise lookup the mail reference by index
            EmailStar.Email mailadd = EmailStar.GetEmails()[maillist.SelectedIndex];

            emails.Items.Add(mailadd.sTitle.s + " (TO: " + mailadd.To.sMailerAddress.s + ")");

            MailsSelected.Add(mailadd);

            //then reset maillist
            maillist.SelectedIndex = -1;
        }

        private void addpartbh_Click(object sender, EventArgs e)
        {
            // add to participants // and ParticipantsSelected 
            //from pickparticipant
            if (pickparticipant.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a participant");
                return;
            }
            EmailStar.Emailer mailertoadd = EmailStar.GetEmailers()[pickparticipant.SelectedIndex];

            if (ParticipantsSelected.Contains(mailertoadd))
            {
                MessageBox.Show("This participant has already been added");
                return;
            }


            participants.Items.Add(mailertoadd.sMailerAddress.s + "");

            ParticipantsSelected.Add(mailertoadd);

            //then reset pickparticipant
            pickparticipant.SelectedIndex = -1;
        }

        private void deletemailb_Click(object sender, EventArgs e)
        {
            //is a mail selected
            if (emails.SelectedIndex == -1)
            {
                MessageBox.Show("Please select an email");
                return;
            }
            //List<EmailStar.Email> MailsSelected;
            MailsSelected.RemoveAt(emails.SelectedIndex);


            RefreshLists();
        }

        private void delpartb_Click(object sender, EventArgs e)
        {
            //is a participant
            if (pickparticipant.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a participant");
                return;
            }
            //List<EmailStar.Emailer> ParticipantsSelected;
            ParticipantsSelected.RemoveAt(pickparticipant.SelectedIndex);

            RefreshLists();
        }
    }
}
