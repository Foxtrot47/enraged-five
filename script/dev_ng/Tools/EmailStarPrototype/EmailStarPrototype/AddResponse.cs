﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmailStarPrototype
{
    public partial class AddResponse : Form
    {
        int iLastCheckedEditorActions = 0;
        EmailStar.Email editMail;
        EmailStar.Response editResponse = null;
        AddEmail createdBy = null;

        public AddResponse(EmailStar.Email addResponseTo,int editIndex, AddEmail creator)
        {
            createdBy = creator;
            InitializeComponent();
            editMail = addResponseTo;
            addingnote.Text = "Adding to email: " + editMail.sTitle.s + " (To: " + editMail.To.sMailerName.s + ")";

            if (editIndex != -1)
            {
                //attempt to find the response reference we need from the email
                 editResponse = editMail.Responses[editIndex];

            }//otherwise just save a new one when done

            RefreshLists();
        }


        void RefreshLists()
        {
            //selecttag - string
            List<EmailStar.EmailSystemString> returnedstrings;
            returnedstrings = EmailStar.GetStringList(EmailStar.StringCategory.ResponseTitle);
            selecttag.Items.Clear();

            foreach (EmailStar.EmailSystemString s in returnedstrings)
            {
                selecttag.Items.Add(s.s);
            }

            //selectresponse - emails
            List<EmailStar.Email> returnedmails;
            returnedmails = EmailStar.GetEmails();
            selectresponse.Items.Clear();

            foreach (EmailStar.Email e in returnedmails)
            {
                selectresponse.Items.Add(e.sTitle.s + " (To: " + e.To.sMailerName.s + ") " + e.sContent.s + "");
            }

            if (editResponse != null)
            {
                //set indices
                //selecttag - string
                

                //selectresponse - emails

                if (editResponse.bResponseAutoFires)
                {
                    selecttag.SelectedIndex = -1;
                    selecttag.Enabled = false;
                }
                else
                {
                    selecttag.Enabled = true;
                    selecttag.SelectedIndex = returnedstrings.IndexOf(editResponse.sResponseTag);
                }

                selectresponse.SelectedIndex = returnedmails.IndexOf(editResponse.ResponseMail);

                //threadkiller - bool
                threadkiller.Checked = editResponse.bResponseEndsThread;
                //firethread - bool
                if (editResponse.ResponseFiresThread == null)
                {
                    firethread.Checked = false;
                    pickthread.Enabled = false;
                }
                else
                {
                    //if fire thread is true, set pickthread
                    firethread.Checked = true;
                    
                    RefreshPickthread();
                }
            }


            

            iLastCheckedEditorActions = EmailStar.getEditActionTotal();
        }

        private void AddResponse_focus(object sender, EventArgs e)
        {
            if (iLastCheckedEditorActions != EmailStar.getEditActionTotal())
            {
                RefreshLists();
            }
        }


        void RefreshPickthread()
        {
            //attempt to rebuild the contents of pickthread 
            // grab a list of all the threads and place their lables in pickthread
            List<EmailStar.Thread> allthreads = EmailStar.GetThreads();
            pickthread.Items.Clear();
            foreach (EmailStar.Thread t in allthreads)
            {
                pickthread.Items.Add(t.sThreadInternalTag);
            }

            if (editResponse != null)
            {
                pickthread.SelectedIndex = EmailStar.GetThreads().IndexOf(editResponse.ResponseFiresThread);
            }
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void firethread_CheckedChanged(object sender, EventArgs e)
        {
            if (firethread.Checked == true)
            {
                pickthread.Enabled = true;
                RefreshPickthread();
            }
            else
            {
                pickthread.Items.Clear();
                pickthread.Enabled = false;
            }
        }

        private void add_Click(object sender, EventArgs e)
        {
            //check for everything nessesary for response
            //selecttag
            if (selecttag.SelectedIndex == -1 && !AutoResponseCheck.Checked)
            {
                MessageBox.Show("No tag picked","Input error");
            }
            //selectresponse
            if (selectresponse.SelectedIndex == -1)
            {
                MessageBox.Show("No response picked", "Input error");
            }
            //threadkiller no check needed

            //firethread->pickthread
            if (firethread.Checked)
            {
                if (pickthread.SelectedIndex == -1)
                {
                    MessageBox.Show("No thread picked", "Input error");
                }
            }

            
            if (editResponse == null)
            {
                editResponse = new EmailStar.Response();
            }
            
            //set everything and add the response if its not being edited
            //selecttag
            //selectresponse
            //threadkiller
            //firethread->pickthread
            
            EmailStar.FillOutResponse(editResponse, 
                                        selecttag.SelectedIndex,
                                        selectresponse.SelectedIndex,
                                        threadkiller.Checked,
                                        firethread.Checked,
                                        pickthread.SelectedIndex);

            //EmailStar.AddResponseToEmail(editMail,editResponse);
            if (!editMail.Responses.Contains(editResponse))
            {
                editMail.Responses.Add(editResponse);
            }
            this.Close();

            if (createdBy != null)
            {
                createdBy.RefreshResponseLists();
            }

        }

        private void AutoResponseCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (AutoResponseCheck.Checked)
            {
                selecttag.SelectedIndex = -1;
                selecttag.Enabled = false;
            }
            else
            {
                selecttag.SelectedIndex = -1;
                selecttag.Enabled = true;
            }
        }


    }
}
