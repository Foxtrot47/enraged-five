﻿namespace EmailStarPrototype
{
    partial class AddThread
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.participants = new System.Windows.Forms.ListBox();
            this.emails = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.internalname = new System.Windows.Forms.TextBox();
            this.addpartbh = new System.Windows.Forms.Button();
            this.delpartb = new System.Windows.Forms.Button();
            this.addemailb = new System.Windows.Forms.Button();
            this.deletemailb = new System.Windows.Forms.Button();
            this.addb = new System.Windows.Forms.Button();
            this.cancelb = new System.Windows.Forms.Button();
            this.pickparticipant = new System.Windows.Forms.ComboBox();
            this.maillist = new System.Windows.Forms.ComboBox();
            this.indexstatuslable = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // participants
            // 
            this.participants.FormattingEnabled = true;
            this.participants.Location = new System.Drawing.Point(471, 98);
            this.participants.Name = "participants";
            this.participants.Size = new System.Drawing.Size(120, 108);
            this.participants.TabIndex = 0;
            // 
            // emails
            // 
            this.emails.FormattingEnabled = true;
            this.emails.Location = new System.Drawing.Point(12, 98);
            this.emails.Name = "emails";
            this.emails.Size = new System.Drawing.Size(416, 303);
            this.emails.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Internal name:";
            // 
            // internalname
            // 
            this.internalname.Location = new System.Drawing.Point(12, 25);
            this.internalname.Name = "internalname";
            this.internalname.Size = new System.Drawing.Size(416, 20);
            this.internalname.TabIndex = 3;
            // 
            // addpartbh
            // 
            this.addpartbh.Location = new System.Drawing.Point(471, 212);
            this.addpartbh.Name = "addpartbh";
            this.addpartbh.Size = new System.Drawing.Size(120, 23);
            this.addpartbh.TabIndex = 4;
            this.addpartbh.Text = "Add Participant";
            this.addpartbh.UseVisualStyleBackColor = true;
            this.addpartbh.Click += new System.EventHandler(this.addpartbh_Click);
            // 
            // delpartb
            // 
            this.delpartb.Location = new System.Drawing.Point(471, 241);
            this.delpartb.Name = "delpartb";
            this.delpartb.Size = new System.Drawing.Size(120, 23);
            this.delpartb.TabIndex = 5;
            this.delpartb.Text = "Delete Participant";
            this.delpartb.UseVisualStyleBackColor = true;
            this.delpartb.Click += new System.EventHandler(this.delpartb_Click);
            // 
            // addemailb
            // 
            this.addemailb.Location = new System.Drawing.Point(434, 290);
            this.addemailb.Name = "addemailb";
            this.addemailb.Size = new System.Drawing.Size(114, 23);
            this.addemailb.TabIndex = 6;
            this.addemailb.Text = "Add Mail";
            this.addemailb.UseVisualStyleBackColor = true;
            this.addemailb.Click += new System.EventHandler(this.addemailb_Click);
            // 
            // deletemailb
            // 
            this.deletemailb.Location = new System.Drawing.Point(435, 319);
            this.deletemailb.Name = "deletemailb";
            this.deletemailb.Size = new System.Drawing.Size(113, 23);
            this.deletemailb.TabIndex = 7;
            this.deletemailb.Text = "Delete Mail";
            this.deletemailb.UseVisualStyleBackColor = true;
            this.deletemailb.Click += new System.EventHandler(this.deletemailb_Click);
            // 
            // addb
            // 
            this.addb.Location = new System.Drawing.Point(527, 359);
            this.addb.Name = "addb";
            this.addb.Size = new System.Drawing.Size(75, 23);
            this.addb.TabIndex = 8;
            this.addb.Text = "Add Thread";
            this.addb.UseVisualStyleBackColor = true;
            this.addb.Click += new System.EventHandler(this.addb_Click);
            // 
            // cancelb
            // 
            this.cancelb.Location = new System.Drawing.Point(527, 388);
            this.cancelb.Name = "cancelb";
            this.cancelb.Size = new System.Drawing.Size(75, 23);
            this.cancelb.TabIndex = 9;
            this.cancelb.Text = "Cancel";
            this.cancelb.UseVisualStyleBackColor = true;
            this.cancelb.Click += new System.EventHandler(this.cancelb_Click);
            // 
            // pickparticipant
            // 
            this.pickparticipant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pickparticipant.FormattingEnabled = true;
            this.pickparticipant.Location = new System.Drawing.Point(471, 71);
            this.pickparticipant.Name = "pickparticipant";
            this.pickparticipant.Size = new System.Drawing.Size(120, 21);
            this.pickparticipant.TabIndex = 10;
            // 
            // maillist
            // 
            this.maillist.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.maillist.FormattingEnabled = true;
            this.maillist.Location = new System.Drawing.Point(12, 71);
            this.maillist.Name = "maillist";
            this.maillist.Size = new System.Drawing.Size(416, 21);
            this.maillist.TabIndex = 11;
            // 
            // indexstatuslable
            // 
            this.indexstatuslable.AutoSize = true;
            this.indexstatuslable.Location = new System.Drawing.Point(25, 53);
            this.indexstatuslable.Name = "indexstatuslable";
            this.indexstatuslable.Size = new System.Drawing.Size(65, 13);
            this.indexstatuslable.TabIndex = 12;
            this.indexstatuslable.Text = "Testing blah";
            // 
            // AddThread
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 414);
            this.ControlBox = false;
            this.Controls.Add(this.indexstatuslable);
            this.Controls.Add(this.maillist);
            this.Controls.Add(this.pickparticipant);
            this.Controls.Add(this.cancelb);
            this.Controls.Add(this.addb);
            this.Controls.Add(this.deletemailb);
            this.Controls.Add(this.addemailb);
            this.Controls.Add(this.delpartb);
            this.Controls.Add(this.addpartbh);
            this.Controls.Add(this.internalname);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.emails);
            this.Controls.Add(this.participants);
            this.Name = "AddThread";
            this.Text = "AddThread";
            this.Enter += new System.EventHandler(this.AddThread_onenter);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox participants;
        private System.Windows.Forms.ListBox emails;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox internalname;
        private System.Windows.Forms.Button addpartbh;
        private System.Windows.Forms.Button delpartb;
        private System.Windows.Forms.Button addemailb;
        private System.Windows.Forms.Button deletemailb;
        private System.Windows.Forms.Button addb;
        private System.Windows.Forms.Button cancelb;
        private System.Windows.Forms.ComboBox pickparticipant;
        private System.Windows.Forms.ComboBox maillist;
        private System.Windows.Forms.Label indexstatuslable;
    }
}