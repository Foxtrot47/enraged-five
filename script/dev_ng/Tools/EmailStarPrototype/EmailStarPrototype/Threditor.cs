﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmailStarPrototype
{
    public partial class Threditor : Form
    {
        EmailStar.Thread toEdit = null;

        List<EmailStar.Email> MailsSelected;
        List<EmailStar.Emailer> ParticipantsSelected;

        Dictionary<TreeNode, EmailStar.Email> TreeLookup;
        //Dictionary<EmailStar.Email, TreeNode> EmailLookup;

        EmailStar.Email editingMail = null;

        EmailStar.Response editingResponse = null;

        Form parentlauncher;

        public Threditor(Form parent)
        {
            InitializeComponent();

            MailsSelected = new List<EmailStar.Email>();
            ParticipantsSelected = new List<EmailStar.Emailer>();
            TreeLookup = new Dictionary<TreeNode, EmailStar.Email>();
            //EmailLookup = new Dictionary<EmailStar.Email, TreeNode>();

            ThreadName.Text = "NEW THREAD - UNNAMED";

            Refresh_Lists();
            Refresh_Tree();

            parentlauncher = parent;          
        }

        public Threditor(EmailStar.Thread toedit, Form parent)
        {
            InitializeComponent();

            toEdit = toedit;

            MailsSelected = new List<EmailStar.Email>();
            ParticipantsSelected = new List<EmailStar.Emailer>();
            TreeLookup = new Dictionary<TreeNode, EmailStar.Email>();
            //EmailLookup = new Dictionary<EmailStar.Email, TreeNode>();

            //fill out mails and participants from passed in thread

            foreach (EmailStar.Email e in toEdit.mails)
            {
                //add it to the selected mails
                MailsSelected.Add(e);
            }
            foreach (EmailStar.Emailer p in toEdit.participants)
            {
                //add it to the selected participants list
                ParticipantsSelected.Add(p);
            }
            //set the thread name
            ThreadName.Text = toEdit.sThreadInternalTag;
            //finally rebuild the trees
            Refresh_Lists();
            Refresh_Tree();

            parentlauncher = parent;  
        }

        private void CommitButton_Click(object sender, EventArgs e)
        {
            //check the thread data is valid
                //is there participants
            if (ParticipantsSelected.Count == 0)
            {
                //not enough participants to commit thread!
                return;
            }
                //is there emails?
            if (MailsSelected.Count == 0)
            {
                //not enough emails to commit thread

                return;
            }

            //is there thread data outstanding to commit?

            //is there email data outstanding to commit?


            //if we've got past all that gubbins the thread can finally be commited
            EmailStar.CommitThread(toEdit, ThreadName.Text, MailsSelected, ParticipantsSelected);
            
        }

        private void Refresh_Lists()
        {


            //SelectParticipant //all possible participants
                //selection not important, do not preserve
                //build from EmailStar.GetEmailers
                
            List<EmailStar.Emailer> ems = EmailStar.GetEmailers();
            SelectParticipant.Items.Clear();
            foreach(EmailStar.Emailer e in ems)
            {
                SelectParticipant.Items.Add(e.sMailerName.s + " (" + e.sMailerAddress.s+")");
            }


            //Participants //ParticipantsSelected
                //selection is not important, do not preserve
                //build from ParticipantsSelected

            Participants.Items.Clear();

            foreach (EmailStar.Emailer e in ParticipantsSelected)
            {
                Participants.Items.Add(e.sMailerName.s +" ("+e.sMailerAddress.s+")");
            }



            if (editingMail != null)
            {
                delaycheck.Enabled = true;
                delaycheck.Checked = editingMail.bEmailBlocksThread;
                //editingMail.iDelay


                delayamount.Enabled = true;
                delayamount.Text = editingMail.iDelay + "";


                //enable and fill out the email lists
                ToSelect.Enabled = true;
                FromSelect.Enabled = true;
                TitleSelect.Enabled = true;
                ContentsSelect.Enabled = true;
                

                ToSelect.Items.Clear();
                FromSelect.Items.Clear();
                TitleSelect.Items.Clear();
                ContentsSelect.Items.Clear();


                TitleEntry.Enabled = true;
                ContentsEntry.Enabled = true;

                List <EmailStar.Emailer> es = EmailStar.GetEmailers();


                //ToSelect //from emailers
                //set selection from editingMail if selected

                //SelectionIndex = es.IndexOf(editingMail.To);
                
                foreach(EmailStar.Emailer e in es)
                {
                    ToSelect.Items.Add(e.sMailerName.s + " (" + e.sMailerAddress.s + ")");
                }
                ToSelect.SelectedIndex = es.IndexOf(editingMail.To);


                //FromSelect //from emailers
                //set selection from editingMail if selected    
                foreach (EmailStar.Emailer e in es)
                {
                    FromSelect.Items.Add(e.sMailerName.s + " (" + e.sMailerAddress.s + ")");
                }
                FromSelect.SelectedIndex = es.IndexOf(editingMail.From);


                //TitleSelect //from all titles
                //set selection from editingMail if selected
                List<EmailStar.EmailSystemString> strs =  EmailStar.GetStringList(EmailStar.StringCategory.EmailTitle);

                foreach (EmailStar.EmailSystemString s in strs)
                {
                    TitleSelect.Items.Add(s.s);
                }
                TitleSelect.SelectedIndex = strs.IndexOf(editingMail.sTitle);
                TitleEntry.Text = editingMail.sTitle.s;

                //ContentsSelect //from all contents
                //set selection from editingMail if selected
                strs = EmailStar.GetStringList(EmailStar.StringCategory.EmailContent);
                foreach (EmailStar.EmailSystemString s in strs)
                {
                    ContentsSelect.Items.Add(s.s);
                }
                ContentsSelect.SelectedIndex = strs.IndexOf(editingMail.sContent);
                ContentsEntry.Text = editingMail.sContent.s;


                //does this email have responses

                if (editingMail.Responses.Count != 0)
                {
                    //fill out response list
                    ResponseList.Enabled = true;
                    ResponseList.Items.Clear();
                    //ResponseList

                    foreach (EmailStar.Response r in editingMail.Responses)
                    {
                        ResponseList.Items.Add(r.sResponseTag.s + " (" + r.ResponseMail.sTitle.s + ")");
                    }

                }else{
                    //no responses, deactivate response menu
                    ResponseList.Enabled = false;
                    ResponseList.Items.Clear();
                }



            }
            else
            {

                delaycheck.Checked = false;
                delaycheck.Enabled = false;

                delayamount.Clear();
                delayamount.Enabled = false;

                TitleEntry.Clear();
                ContentsEntry.Clear();

                //clear and disable these field
                ToSelect.Items.Clear();
                FromSelect.Items.Clear();
                TitleSelect.Items.Clear();
                ContentsSelect.Items.Clear();
               // SignOffSelect.Items.Clear();

                ToSelect.Enabled = false;
                FromSelect.Enabled = false;
                TitleSelect.Enabled = false;
                ContentsSelect.Enabled = false;
                //SignOffSelect.Enabled = false;

                //disable buttons

                CommitTitle.Enabled = false;
                CommitContents.Enabled = false;
               // CommitSignoff.Enabled = false;

                //disable entry

                TitleEntry.Enabled = false;
                ContentsEntry.Enabled = false;
                //SignOffEntry.Enabled = false;

                //disable the response stuff
                ResponseList.Enabled = false;
            }

        }

        private void Refresh_Response_List()
        {
            //fill out the response details if there is a selected response
            if (editingResponse != null)
            {
                ResponseTitle.Text = editingResponse.sResponseTag.s;
                //
                //responseautofire
                responseautofire.Checked = editingResponse.bResponseAutoFires;
                //responseendsthread
                responseendsthread.Checked = editingResponse.bResponseEndsThread;

                //selectresponsemail // fill out from mails
                    
                selectresponsemail.Items.Clear();

                //

                List<EmailStar.Email> ems = EmailStar.GetEmails();

                foreach (EmailStar.Email e in ems)
                {
                    selectresponsemail.Items.Add(e.sTitle.s + " (From" + e.From.sMailerName.s + ")");
                }
                //set selection
                selectresponsemail.SelectedIndex = ems.IndexOf(editingResponse.ResponseMail);

                //responsefirethread
                //responsepickthread
                //editingResponse.ResponseFiresThread
                //fill out threads if needed
                    //set selection


                if (editingResponse.ResponseFiresThread != null)
                {
                    responsefirethread.Checked = true;
                    responsepickthread.Items.Clear();
                    responsepickthread.Enabled = true;

                    List<EmailStar.Thread> threads = EmailStar.GetThreads();


                    foreach (EmailStar.Thread t in threads)
                    {
                        responsepickthread.Items.Add(t.sThreadInternalTag);
                    }

                    //set selection
                    responsepickthread.SelectedIndex = threads.IndexOf(editingResponse.ResponseFiresThread);

                }
                else
                {
                    //does not fire thread, clean up
                    responsefirethread.Checked = false;

                    responsepickthread.Items.Clear();
                    responsepickthread.Enabled = false;

                }



            }
        }

        private void Refresh_Tree()
        {
            //rebuild the thread tree
            ThreadTree.Nodes.Clear();
            //EmailLookup.Clear();
            TreeLookup.Clear();

            //create thread name node, if set
            TreeNode root = new TreeNode(ThreadName.Text);
            ThreadTree.Nodes.Add(root);

            //remember to add an entry in TreeLookup for every email
            
            //for each email in main thread path
            //fill out the main thread list
                //follow the response chains to fill out the response tree
                    //TODO test for infinite recursion

            foreach (EmailStar.Email e in MailsSelected)
            {
                TreeNode mailnode = new TreeNode(e.sTitle.s);
                TreeLookup.Add(mailnode, e);
                //EmailLookup.Add(e, mailnode);
                ThreadTree.Nodes.Add(mailnode);
                //do recursive response add
                Recursive_Mail_Tree_Build(e, mailnode);
            }

        }


        private void Recursive_Mail_Tree_Build(EmailStar.Email email,TreeNode myroot)
        {
            //for each response in this email, add a node and call this function for it
            foreach (EmailStar.Response r in email.Responses)
            {
                TreeNode thisnode = new TreeNode("R: " + r.sResponseTag.s + " (" + r.ResponseMail.sTitle.s+")");
                TreeLookup.Add(thisnode, r.ResponseMail);
                //EmailLookup.Add(r.ResponseMail, thisnode);
                myroot.Nodes.Add(thisnode);

                Recursive_Mail_Tree_Build(r.ResponseMail, thisnode);
            }

        }


        private void ThreadTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //a node in the tree has been selected, fill out the email feilds if needed

            

            //if the node is in the dictionary
            if (!TreeLookup.ContainsKey(e.Node))
            {
                //node not in dictionary, must be root or not have an email
                editingMail = null;
                editingResponse = null;
                Refresh_Lists();


                return;
            }
            
            //fill out the email details and set the editing focus
            if (editingMail != TreeLookup[e.Node])
            {
                responsepickthread.Items.Clear();
                selectresponsemail.Items.Clear();

                editingMail = TreeLookup[e.Node];
                editingResponse = null;
                //clear the response menus
                ResponseList.Items.Clear();
                selectresponsemail.Items.Clear();
                responsepickthread.Items.Clear();

                responsefirethread.Checked = false;
                responseautofire.Checked = false;
                responseendsthread.Checked = false;

                selectresponsemail.Enabled = false;
                responsepickthread.Enabled = false;

                responsefirethread.Enabled = false;
                responseautofire.Enabled = false;
                responseendsthread.Enabled = false;
                ResponseTitle.Enabled = false;
                ResponseTitle.Clear();

                if (editingMail.Responses.Count > 0)
                {
                    ResponseList.Enabled = true;
                }
                else
                {
                    ResponseList.Enabled = false;
                }

                Refresh_Response_List();
                Refresh_Lists();
                return;
            }
        }

        private void ResponseList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //TODO all
            //an email response has been picked
            if(editingMail != null)
            {
                if (ResponseList.SelectedIndex > -1)
                {
                    //set editing response from editingMail list and ui elements
                    editingResponse = editingMail.Responses[ResponseList.SelectedIndex];
                    //then refresh the list
                    selectresponsemail.Enabled = true;
                    responsepickthread.Enabled = true;

                    responsefirethread.Enabled = true;
                    responseautofire.Enabled = true;
                    responseendsthread.Enabled = true;
                    ResponseTitle.Enabled = true;

                    Refresh_Response_List();

                    
                }
            }

        }

        private void CommitTitle_Click(object sender, EventArgs e)
        {
            //check string for difference
            int compdegree = TitleEntry.Text.CompareTo(editingMail.sTitle.s);
            if (compdegree == 0) //no difference
            {
                MessageBox.Show("No difference in string! Cannot commit!");
            }
            else
            {
                //there is a difference, see if they want to edit or replace
                DialogResult dlgr = MessageBox.Show("Overwrite?", "commit title", MessageBoxButtons.YesNoCancel);

                if (dlgr == System.Windows.Forms.DialogResult.Yes)
                {
                    //overwrite it
                    editingMail.sTitle.s = TitleEntry.Text;
                    Refresh_Lists();
                }
                else
                {
                    if (dlgr == System.Windows.Forms.DialogResult.Cancel)
                    {
                        //do nothing yo
                        return;
                    }
                    else
                    {
                        dlgr = MessageBox.Show("Create new?", "commit title", MessageBoxButtons.YesNo);

                        if (dlgr == System.Windows.Forms.DialogResult.Yes)
                        {
                            //make a new title entry

                            EmailStar.EmailSystemString emtoadd = new EmailStar.EmailSystemString(TitleEntry.Text);
                            emtoadd.myCategory = EmailStar.StringCategory.EmailTitle;
                            EmailStar.AddString(emtoadd);
                            editingMail.sTitle = emtoadd;
                            Refresh_Lists();

                        }
                    }
                }

            }
        }

        private void CommitContents_Click(object sender, EventArgs e)
        {
            //check string for difference
            int compdegree = ContentsEntry.Text.CompareTo(editingMail.sContent.s);
            if (compdegree == 0) //no difference
            {
                MessageBox.Show("No difference in string! Cannot commit!");
            }
            else
            {
                //there is a difference, see if they want to edit or replace
                DialogResult dlgr = MessageBox.Show("Overwrite?", "commit content", MessageBoxButtons.YesNoCancel);

                if (dlgr == System.Windows.Forms.DialogResult.Yes)
                {
                    //overwrite it
                    editingMail.sContent.s = ContentsEntry.Text;
                    Refresh_Lists();
                }
                else
                {
                    if (dlgr == System.Windows.Forms.DialogResult.Cancel)
                    {
                        //do nothing yo
                        return;
                    }
                    else
                    {
                        dlgr = MessageBox.Show("Create new?", "commit content", MessageBoxButtons.YesNo);

                        if (dlgr == System.Windows.Forms.DialogResult.Yes)
                        {
                            //make a new title entry

                            EmailStar.EmailSystemString emtoadd = new EmailStar.EmailSystemString(ContentsEntry.Text);
                            emtoadd.myCategory = EmailStar.StringCategory.EmailContent;
                            EmailStar.AddString(emtoadd);
                            editingMail.sContent = emtoadd;
                            Refresh_Lists();

                        }
                    }
                }

            }
        }

        private void TitleEntry_TextChanged(object sender, KeyEventArgs e)
        {
            if (TitleEntry.Focus())
            {
                CommitTitle.Enabled = true;
            }
        }

        private void ContentsEntry_TextChanged(object sender, KeyEventArgs e)
        {
            if (ContentsEntry.Focus())
            {
                CommitContents.Enabled = true;
            }
        }

        private void ToSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (editingMail != null)
            {
                //if the index is different than the currently selected on in the mail
                List<EmailStar.Emailer> ems = EmailStar.GetEmailers();
                if (ToSelect.SelectedIndex > -1)
                {
                    if (ems.IndexOf(editingMail.To) != ToSelect.SelectedIndex)
                    {
                        //either the email has changed or the index has changed

                        //TODO, check for change of email by something else 

                        editingMail.To = ems[ToSelect.SelectedIndex];

                        Refresh_Lists();  
                    }
                }
            }

        }

        private void FromSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (editingMail != null)
            {
                //if the index is different than the currently selected on in the mail
                List<EmailStar.Emailer> ems = EmailStar.GetEmailers();
                if (FromSelect.SelectedIndex > -1)
                {
                    if (ems.IndexOf(editingMail.From) != FromSelect.SelectedIndex)
                    {
                        //either the email has changed or the index has changed

                        //TODO, check for change of email by something else 

                        editingMail.From = ems[FromSelect.SelectedIndex];

                        Refresh_Lists();
                    }
                }
            }

        }

        private void TitleSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (editingMail != null)
            {
                List<EmailStar.EmailSystemString> emstrs = EmailStar.GetStringList(EmailStar.StringCategory.EmailTitle);

                if (TitleSelect.SelectedIndex > -1)
                {
                    if (emstrs.IndexOf(editingMail.sTitle) != TitleSelect.SelectedIndex)
                    {
                        editingMail.sTitle = emstrs[TitleSelect.SelectedIndex];
                        Refresh_Lists();
                    }
                }
            }
        }

        private void ContentsSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (editingMail != null)
            {
                List<EmailStar.EmailSystemString> emstrs = EmailStar.GetStringList(EmailStar.StringCategory.EmailContent);

                if (ContentsSelect.SelectedIndex > -1)
                {
                    if (emstrs.IndexOf(editingMail.sContent) != ContentsSelect.SelectedIndex)
                    {
                        editingMail.sContent = emstrs[ContentsSelect.SelectedIndex];
                        Refresh_Lists();
                    }
                }
            }
        }

        private void delaycheck_CheckedChanged(object sender, EventArgs e)
        {
            if (editingMail != null)
            {
                //change the bool and enable delay amount if its true
                if (delaycheck.Checked)
                {
                        editingMail.bEmailBlocksThread = true;
                }
                else
                {
                        editingMail.bEmailBlocksThread = false;
                }
            }
        }

        private void delayamount_TextChanged(object sender, EventArgs e)
        {
            if (editingMail != null)
            {
                //attempt to convert the string to an int
                bool bfailed = false;
                int convint = 0;
                try
                {
                    convint = int.Parse(delayamount.Text);
                }
                catch
                {
                    bfailed = true;
                }

                if (!bfailed)
                {
                    editingMail.iDelay = convint;
                    delayamount.BackColor = Color.White;
                }
                else
                {
                    //if it fails turn the background of the delay box red
                    delayamount.BackColor = Color.Red;
                }
            }
        }

        private void responsepickthread_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (editingResponse != null && responsepickthread.Enabled)
            {
                if (responsepickthread.SelectedIndex == -1)
                {
                    //clear the thread index for editresponse
                    editingResponse.ResponseFiresThread = null;
                }
                else
                {
                    //an indice has been picked, set the thread in the response
                    editingResponse.ResponseFiresThread = EmailStar.GetThreads()[responsepickthread.SelectedIndex];
                }
            }

            
        }

        private void responsefirethread_CheckedChanged(object sender, EventArgs e)
        {
            if (editingResponse != null)
            {
                if (responsefirethread.Checked)
                {

                    responsepickthread.Enabled = true;
                    //fill out the thread list
                    responsepickthread.Items.Clear();
                    //fill out the thread list
                    List<EmailStar.Thread> threads = EmailStar.GetThreads();


                    foreach(EmailStar.Thread t in threads)
                    {
                        responsepickthread.Items.Add(t.sThreadInternalTag);
                    }
                }
                else
                {
                    //disable the menu
                    responsepickthread.Enabled = false;
                    responsepickthread.Items.Clear();
                    //clear the thread
                    editingResponse.ResponseFiresThread = null;
                }
            }
        }

        private void responseautofire_CheckedChanged(object sender, EventArgs e)
        {
            if (editingResponse != null)
            {
                editingResponse.bResponseAutoFires = responseautofire.Checked;
            }
        }

        private void responseendsthread_CheckedChanged(object sender, EventArgs e)
        {
            if (editingResponse != null)
            {
                editingResponse.bResponseEndsThread = responseendsthread.Checked;
            }
        }

        private void AddParticipant_Click(object sender, EventArgs e)
        {
            //add a participant
            if (toEdit != null)
            {
                if (SelectParticipant.SelectedIndex != -1 && SelectParticipant.Items.Count > 0)
                {
                    //toEdit.participants
                    ParticipantsSelected.Add(EmailStar.GetEmailers()[SelectParticipant.SelectedIndex]);

                    Refresh_Lists();
                }
            }

        }

        private void DeleteParticipant_Click(object sender, EventArgs e)
        {
            //remove a participant
            if (toEdit != null)
            {
                //is a particpant in the list selected?
                if (Participants.SelectedIndex != -1)
                {
                    //remove it and refresh the list
                    ParticipantsSelected.RemoveAt(Participants.SelectedIndex);
                    Refresh_Lists();
                }
            }
        }

        private void ResponseTitle_TextChanged(object sender, EventArgs e)
        {
            if (editingResponse != null && editingMail != null && ResponseTitle.Enabled)
            {   //check the string is different to the response name
                if (ResponseTitle.Text.CompareTo(editingResponse.sResponseTag.s) != 0)
                {
                    //check for the string being blank or made of all invalid chars
                    //TODO, check for whitespace only
                    if (ResponseTitle.Text.Length > 0)
                    {
                        editingResponse.sResponseTag.s = ResponseTitle.Text;
                       
                        ResponseList.Items[editingMail.Responses.IndexOf(editingResponse)] = ResponseTitle.Text;
                    }
                }
                
            }
        }

        private void addResponse_Click(object sender, EventArgs e)
        {
            if (editingMail != null)
            {

                Form f = new ThreditorResponse(this,editingMail);
                f.MdiParent = parentlauncher;
                f.Show();

                //Refresh_Response_List();
            }
        }

        private void removeResponse_Click(object sender, EventArgs e)
        {
            if (editingMail != null && editingResponse != null)
            {
                if (MessageBox.Show("Deleting this response will take effect immediatly", "Warning", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
                {
                    //canceled, do nothing
                    return;
                }

                //remove the node
                
                //TreeNode key = EmailLookup[editingResponse.ResponseMail];
                //EmailLookup.Remove(editingResponse.ResponseMail);
                //TreeLookup.Remove(key);

               // key.Parent.Nodes.Remove(key);

                //remove the given response
                editingMail.Responses.Remove(editingResponse);
                Refresh_Lists();
                Refresh_Response_List();
                Refresh_Tree();
            }
        }
/*
        private void Threditor_Focus(object sender, EventArgs e)
        {
            if (responsewindow != null)
            {
                if (!responsewindow.Visible)
                {
                    responsewindow = null;
                    Refresh_Tree();
                    Refresh_Lists();
                    Refresh_Response_List();
                }
            }
        }
*/






        internal void ResponseHasBeenAdded()
        {
            Refresh_Lists();
            Refresh_Response_List();
            Refresh_Tree();
        }

        private void AddEmailToThread_Click(object sender, EventArgs e)
        {
            if (toEdit == null)
            {
                return;
            }
            //add a blank email to the end of the thread
            //toEdit.mails.
            //make a blank email with default settings
            //commit it to thread

            bool AddAtPlace = true;


            EmailStar.Email NewMail = new EmailStar.Email();

            EmailStar.CommitEmail(NewMail,
                                   0,//use last receiver in thread - or 0 if none
                                   0,//use last sender in thread - or 0 if none
                                   0,//use last title in thread - or 0 if none
                                   0,//use last content in thread - or 0 if none
                                   0, //default to 0
                                   false);//default to none blocking

            //

            TreeNode tn = ThreadTree.SelectedNode;

            if (tn == null)
            {
                AddAtPlace = false;
            }
            else
            {
                if (!TreeLookup.ContainsKey(tn))
                {
                    AddAtPlace = false;
                }
                else
                {
                    //Dictionary<TreeNode, EmailStar.Email> TreeLookup;
                    EmailStar.Email mailtofind = TreeLookup[tn];
                    //find the right mail in MailsSelected
                    if (!MailsSelected.Contains(mailtofind))
                    {
                        AddAtPlace = false;
                    }
                    else
                    {
                        //is it on the base level of the nodes?
                        if (!ThreadTree.Nodes.Contains(tn))
                        {
                            AddAtPlace = false;
                        }
                    }
                }
            }

            

            //now add the mail to the thread
            //toEdit.mails.Add(NewMail); //not this because no direct edits until commit

            if (!AddAtPlace)
            {
                //add it at the end of list
                MailsSelected.Add(NewMail);
            }
            else
            {
                MailsSelected.Insert(ThreadTree.Nodes.IndexOf(tn), NewMail);
            }


            //and refresh the lists
            Refresh_Lists();
            Refresh_Response_List();
            Refresh_Tree();
        }

        private void RemoveEmailFromThread_Click(object sender, EventArgs e)
        {
            //remove selected email from the tree and the thread 
            if (toEdit == null)
            {
                return;
            }
            TreeNode tn = ThreadTree.SelectedNode;

            if(tn == null)
            {
                return;
            }

            if (!TreeLookup.ContainsKey(tn))
            {
                //its the header node or one thats not in the list, so no interested
                return;
            }


            //Dictionary<TreeNode, EmailStar.Email> TreeLookup;
            EmailStar.Email mailtofind = TreeLookup[tn];

            //find the right mail in MailsSelected, if its in there then baleet it
            if (!MailsSelected.Contains(mailtofind))
            {
                return;
            }
            //is it on the base level of the nodes?
            if (!ThreadTree.Nodes.Contains(tn))
            {
                //selected node is a response node!

                MessageBox.Show("Node isn't a base level email, deletion of responses is not implemented yet via this button.");
                //TODO handle this in another way
                return;
            }

            MailsSelected.RemoveAt(ThreadTree.Nodes.IndexOf(tn)-1);

            editingMail = null;
            //and rebuild the tree
            Refresh_Lists();
            Refresh_Response_List();
            Refresh_Tree();
        }

        private void MoveUp_Click(object sender, EventArgs e)
        {
            //Shift the email in the order 

            if (toEdit == null)
            {
                return;
            }
            TreeNode tn = ThreadTree.SelectedNode;

            if (tn == null)
            {
                return;
            }

            if (!TreeLookup.ContainsKey(tn))
            {
                //its the header node or one thats not in the list, so no interested
                return;
            }


            //Dictionary<TreeNode, EmailStar.Email> TreeLookup;
            EmailStar.Email mailtofind = TreeLookup[tn];

            //find the right mail in MailsSelected, if its in there then baleet it
            if (!MailsSelected.Contains(mailtofind))
            {
                return;
            }
            //is it on the base level of the nodes?
            if (!ThreadTree.Nodes.Contains(tn))
            {
                //selected node is a response node!

                MessageBox.Show("Node isn't a base level email, reordering of responses should be done by editing the email they are contained in.");
                //TODO handle this in another way
                return;
            }

            //MailsSelected.RemoveAt(ThreadTree.Nodes.IndexOf(tn) - 1);

            //move the index

            int mailindex = ThreadTree.Nodes.IndexOf(tn) -1;
            EmailStar.Email mailtomove = MailsSelected[mailindex];
            int newindex = mailindex -1;
            if (newindex < 0)
            {
                return;//can't move up anymore
            }

            //move it
            MailsSelected.RemoveAt(mailindex);
            MailsSelected.Insert(newindex, mailtomove);
            //rebuild the tree
            Refresh_Lists();
            Refresh_Response_List();
            Refresh_Tree();
            //Reselect it

            //ThreadTree.Nodes.IndexOf(
            
            ThreadTree.SelectedNode = ThreadTree.Nodes[(MailsSelected.IndexOf(mailtomove) + 1)];
            ThreadTree.Select();
        }

        private void MoveDown_Click(object sender, EventArgs e)
        {
            //Shift the email in the order 

            if (toEdit == null)
            {
                return;
            }
            TreeNode tn = ThreadTree.SelectedNode;

            if (tn == null)
            {
                return;
            }

            if (!TreeLookup.ContainsKey(tn))
            {
                //its the header node or one thats not in the list, so no interested
                return;
            }


            //Dictionary<TreeNode, EmailStar.Email> TreeLookup;
            EmailStar.Email mailtofind = TreeLookup[tn];

            //find the right mail in MailsSelected, if its in there then baleet it
            if (!MailsSelected.Contains(mailtofind))
            {
                return;
            }
            //is it on the base level of the nodes?
            if (!ThreadTree.Nodes.Contains(tn))
            {
                //selected node is a response node!

                MessageBox.Show("Node isn't a base level email, reordering of responses should be done by editing the email they are contained in.");
                //TODO handle this in another way
                return;
            }

            //MailsSelected.RemoveAt(ThreadTree.Nodes.IndexOf(tn) - 1);

            //move the index

            int mailindex = ThreadTree.Nodes.IndexOf(tn) - 1;
            EmailStar.Email mailtomove = MailsSelected[mailindex];
            int newindex = mailindex + 1;
            if (newindex >= (ThreadTree.Nodes.Count -1))
            {
                return;//can't move down anymore
            }

            //move it
            MailsSelected.RemoveAt(mailindex);
            MailsSelected.Insert(newindex, mailtomove);
            //rebuild the tree
            Refresh_Lists();
            Refresh_Response_List();
            Refresh_Tree();
            //Reselect it

            //ThreadTree.Nodes.IndexOf(

            ThreadTree.SelectedNode = ThreadTree.Nodes[(MailsSelected.IndexOf(mailtomove) + 1)];
            ThreadTree.Select();
        }

        private void PreviewThread_Click(object sender, EventArgs e)
        {
            //launch thread previewer on this thread
            if (toEdit != null)
            {

                Form f = new ThreadPreviewer(toEdit);
                if (!f.IsDisposed)
                {
                    f.MdiParent = parentlauncher;
                    f.Show();
                }
            }
        }
    }
}
