﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmailStarPrototype
{
    public partial class AddEmailer : Form
    {
        int iLastEditActions = 0;

        int edindex = -1;
        EmailStar.Emailer editing = null;

        public AddEmailer()
        {
            //intialise the list contents

            InitializeComponent();

            RefreshLists();
        }
        public AddEmailer(int editindex)
        {
            editing = EmailStar.GetEmailers()[editindex];

            if (!editing.bEditing)
            {
                editing.bEditing = true;

                edindex = editindex;

                InitializeComponent();
                RefreshLists();
            }
            else
            {
                this.Close();
            }
        }
        //TODO editing startup with auto selection

        void RefreshLists()
        {
            List<EmailStar.EmailSystemString> stringsout;

            //namelist;
            namelist.SelectedIndex = -1;
            namelist.Items.Clear();
            stringsout = EmailStar.GetStringList(EmailStar.StringCategory.EmailerName);
            foreach (EmailStar.EmailSystemString es in stringsout)
            {
                namelist.Items.Add(es.s);
            }

            if (editing != null)
            {
                //set selected value
                if (editing.sMailerName != null)
                {
                    namelist.SelectedIndex = stringsout.IndexOf(editing.sMailerName);
                }
            }


            //emaillist
            emaillist.SelectedIndex = -1;
            emaillist.Items.Clear();
            stringsout = EmailStar.GetStringList(EmailStar.StringCategory.EmailerAddress);
            foreach (EmailStar.EmailSystemString es in stringsout)
            {
                emaillist.Items.Add(es.s);
            }

            if (editing != null)
            {
                //set selected value
                if (editing.sMailerAddress != null)
                {
                    emaillist.SelectedIndex = stringsout.IndexOf(editing.sMailerAddress);
                }
            }

            //signofflist
            signofflist.SelectedIndex = -1;
            signofflist.Items.Clear();
            stringsout = EmailStar.GetStringList(EmailStar.StringCategory.EmailerSignoff);
            foreach (EmailStar.EmailSystemString es in stringsout)
            {
                signofflist.Items.Add(es.s);
            }
            if (editing != null)
            {
                //set selected value
                if (editing.sMailerSignoff != null)
                {
                    signofflist.SelectedIndex = stringsout.IndexOf(editing.sMailerSignoff);
                }
            }


            iLastEditActions = EmailStar.getEditActionTotal();
        }

        private void addm_Click(object sender, EventArgs e)
        {
            //check selections are valid
            if (namelist.SelectedIndex == -1 || emaillist.SelectedIndex == -1)
            {
                MessageBox.Show("Both name and adress must be set, only signoff is optional","Invalid input!");
            }

            //add the emailer to emailstar
            if (editing == null)
            {
                EmailStar.AddEmailer(namelist.SelectedIndex, emaillist.SelectedIndex, signofflist.SelectedIndex);
            }
            else
            {
                EmailStar.AddEmailer(editing, namelist.SelectedIndex, emaillist.SelectedIndex, signofflist.SelectedIndex);
            }

            if (editing != null)
            {
                editing.bEditing = false;
            }
            this.Close();
        }

        private void cancelm_Click(object sender, EventArgs e)
        {
            if (editing != null)
            {
                editing.bEditing = false;
            }
            this.Close();
        }

        private void AddEmailer_Load(object sender, EventArgs e)
        {
            if (this.iLastEditActions != EmailStar.getEditActionTotal())
            {
                RefreshLists();
            }
        }

    }
}
