﻿namespace EmailStarPrototype
{
    partial class Mainform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadThreadSetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveThreadSetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportForGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToXgta5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addStringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addEmailerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addEmailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addThreadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openThreditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDLG = new System.Windows.Forms.SaveFileDialog();
            this.openFileDLG = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.addToolStripMenuItem,
            this.viewToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1016, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadThreadSetToolStripMenuItem,
            this.saveThreadSetToolStripMenuItem,
            this.exportForGameToolStripMenuItem,
            this.exportToXgta5ToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadThreadSetToolStripMenuItem
            // 
            this.loadThreadSetToolStripMenuItem.Name = "loadThreadSetToolStripMenuItem";
            this.loadThreadSetToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.loadThreadSetToolStripMenuItem.Text = "Load thread set";
            this.loadThreadSetToolStripMenuItem.Click += new System.EventHandler(this.loadThreadSetToolStripMenuItem_Click);
            // 
            // saveThreadSetToolStripMenuItem
            // 
            this.saveThreadSetToolStripMenuItem.Name = "saveThreadSetToolStripMenuItem";
            this.saveThreadSetToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.saveThreadSetToolStripMenuItem.Text = "Save thread set";
            this.saveThreadSetToolStripMenuItem.Click += new System.EventHandler(this.saveThreadSetToolStripMenuItem_Click);
            // 
            // exportForGameToolStripMenuItem
            // 
            this.exportForGameToolStripMenuItem.Name = "exportForGameToolStripMenuItem";
            this.exportForGameToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.exportForGameToolStripMenuItem.Text = "Export for game";
            this.exportForGameToolStripMenuItem.Click += new System.EventHandler(this.exportForGameToolStripMenuItem_Click);
            // 
            // exportToXgta5ToolStripMenuItem
            // 
            this.exportToXgta5ToolStripMenuItem.Name = "exportToXgta5ToolStripMenuItem";
            this.exportToXgta5ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.exportToXgta5ToolStripMenuItem.Text = "Export to X:\\gta5";
            this.exportToXgta5ToolStripMenuItem.Click += new System.EventHandler(this.exportToXgta5ToolStripMenuItem_Click);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addStringToolStripMenuItem,
            this.addEmailerToolStripMenuItem,
            this.addEmailToolStripMenuItem,
            this.addThreadToolStripMenuItem,
            this.openThreditorToolStripMenuItem});
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.addToolStripMenuItem.Text = "Add";
            // 
            // addStringToolStripMenuItem
            // 
            this.addStringToolStripMenuItem.Name = "addStringToolStripMenuItem";
            this.addStringToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.addStringToolStripMenuItem.Text = "Add String";
            this.addStringToolStripMenuItem.Click += new System.EventHandler(this.addStringToolStripMenuItem_Click);
            // 
            // addEmailerToolStripMenuItem
            // 
            this.addEmailerToolStripMenuItem.Name = "addEmailerToolStripMenuItem";
            this.addEmailerToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.addEmailerToolStripMenuItem.Text = "Add Emailer";
            this.addEmailerToolStripMenuItem.Click += new System.EventHandler(this.addEmailerToolStripMenuItem_Click);
            // 
            // addEmailToolStripMenuItem
            // 
            this.addEmailToolStripMenuItem.Name = "addEmailToolStripMenuItem";
            this.addEmailToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.addEmailToolStripMenuItem.Text = "Add Email";
            this.addEmailToolStripMenuItem.Click += new System.EventHandler(this.addEmailToolStripMenuItem_Click);
            // 
            // addThreadToolStripMenuItem
            // 
            this.addThreadToolStripMenuItem.Name = "addThreadToolStripMenuItem";
            this.addThreadToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.addThreadToolStripMenuItem.Text = "Add Thread";
            this.addThreadToolStripMenuItem.Click += new System.EventHandler(this.addThreadToolStripMenuItem_Click);
            // 
            // openThreditorToolStripMenuItem
            // 
            this.openThreditorToolStripMenuItem.Name = "openThreditorToolStripMenuItem";
            this.openThreditorToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.openThreditorToolStripMenuItem.Text = "Open Threditor";
            this.openThreditorToolStripMenuItem.Click += new System.EventHandler(this.openThreditorToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataViewToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // dataViewToolStripMenuItem
            // 
            this.dataViewToolStripMenuItem.Name = "dataViewToolStripMenuItem";
            this.dataViewToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.dataViewToolStripMenuItem.Text = "Data View";
            this.dataViewToolStripMenuItem.Click += new System.EventHandler(this.dataViewToolStripMenuItem_Click);
            // 
            // openFileDLG
            // 
            this.openFileDLG.FileName = "pick a database to load";
            // 
            // Mainform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 893);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Mainform";
            this.Text = "EmailStar - Prototype";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadThreadSetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveThreadSetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportForGameToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDLG;
        private System.Windows.Forms.OpenFileDialog openFileDLG;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addStringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addEmailerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addEmailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addThreadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataViewToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.ToolStripMenuItem exportToXgta5ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openThreditorToolStripMenuItem;
    }
}

