﻿using MissionStatsStar;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace MissionStatsStar
{
    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "CategoryPermutation")]
    public partial class CategoryPermutation : ModelBase
    {
        /// <remarks/>
        [XmlTextAttribute()]
        public string Value
        {
            get
            {
                return m_value;
            }
            set
            {
                m_value = value;
                NotifyPropertyChanged("Value");
            }
        }

        private string m_value;
    }
    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "Leaderboard")]
    public partial class Leaderboard : ModelBase
    {
        public override string ToString()
        {
            return Name + " (" + Id + ")";
        }

        /// <remarks/>
        [XmlArray(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName = "CategoryPermutations")]
        [XmlArrayItem("CategoryPermutation", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ObservableCollection<CategoryPermutation> CategoryPermutations
        {
            get
            {
                return m_categoryPermutations;
            }
            set
            {
                m_categoryPermutations = value;
                NotifyPropertyChanged("CategoryPermutations");
            }
        }

        /// <remarks/>
        [XmlArray(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName = "Columns")]
        [XmlArrayItem("Column", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public ObservableCollection<Column> Columns
        {
            get
            {
                return m_columns;
            }
            set
            {
                m_columns = value;
                NotifyPropertyChanged("Columns");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "id")]
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
                NotifyPropertyChanged("Id");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "name")]
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
                NotifyPropertyChanged("Name");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "entryExpiration")]
        public string EntryExpiration
        {
            get
            {
                return m_entryExpiration;
            }
            set
            {
                m_entryExpiration = value;
                NotifyPropertyChanged("EntryExpiration");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "resetType")]
        public string ResetType
        {
            get
            {
                return m_resetType;
            }
            set
            {
                m_resetType = value;
                NotifyPropertyChanged("ResetType");
            }
        }

        private ObservableCollection<CategoryPermutation> m_categoryPermutations;
        private ObservableCollection<Column> m_columns;
        private string m_id;
        private string m_name;
        private string m_entryExpiration;
        private string m_resetType;
    }

    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "GameInput")]
    public partial class GameInput : ModelBase
    {
        /// <remarks/>
        [XmlAttribute(AttributeName = "inputId")]
        public string InputId
        {
            get
            {
                return m_inputId;
            }
            set
            {
                m_inputId = value;
                NotifyPropertyChanged("InputId");
            }
        }

        private string m_inputId;
    }
    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "ColumnInput")]
    public partial class ColumnInput : ModelBase
    {
        /// <remarks/>
        [XmlAttribute(AttributeName = "ordinal")]
        public string Ordinal
        {
            get
            {
                return m_ordinal;
            }
            set
            {
                m_ordinal = value;
                NotifyPropertyChanged("Ordinal");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "columnId")]
        public string ColumnId
        {
            get
            {
                return m_columnId;
            }
            set
            {
                m_columnId = value;
                NotifyPropertyChanged("ColumnId");
            }
        }

        private string m_ordinal;
        private string m_columnId;
    }
    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "Aggregation")]
    public partial class Aggregation : ModelBase
    {
        /// <remarks/>
        [XmlElement("ColumnInput", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ObservableCollection<ColumnInput> ColumnInput
        {
            get
            {
                return m_columnInput;
            }
            set
            {
                m_columnInput = value;
                NotifyPropertyChanged("ColumnInput");
            }
        }

        /// <remarks/>
        [XmlElement("GameInput", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public GameInput GameInput
        {
            get
            {
                return m_gameInput;
            }
            set
            {
                m_gameInput = value;
                NotifyPropertyChanged("GameInput");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "type")]
        public string Type
        {
            get
            {
                return m_type;
            }
            set
            {
                m_type = value;
                NotifyPropertyChanged("Type");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "conditionalOnColumnId")]
        public string ConditionalOnColumnId
        {
            get
            {
                return m_conditionalOnColumnId;
            }
            set
            {
                m_conditionalOnColumnId = value;
                NotifyPropertyChanged("ConditionalOnColumnId");
            }
        }

        private ObservableCollection<ColumnInput> m_columnInput;
        private GameInput m_gameInput;
        private string m_type;
        private string m_conditionalOnColumnId;
    }
    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "Column")]
    public partial class Column : ModelBase
    {
        public override string ToString()
        {
            return Name + " (" + Id + ")";
        }


        /// <remarks/>
        [XmlElement(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName = "Aggregation")]
        public Aggregation Aggregation
        {
            get
            {
                return m_aggregation;
            }
            set
            {
                m_aggregation = value;
                NotifyPropertyChanged("Aggregation");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "id")]
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
                NotifyPropertyChanged("Id");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "name")]
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
                NotifyPropertyChanged("Name");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "ordinal")]
        public string Ordinal
        {
            get
            {
                return m_ordinal;
            }
            set
            {
                m_ordinal = value;
                NotifyPropertyChanged("Ordinal");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "isRankingColumn")]
        public string IsRankingColumn
        {
            get
            {
                return m_isRankingColumn;
            }
            set
            {
                m_isRankingColumn = value;
                NotifyPropertyChanged("IsRankingColumn");
            }
        }

        private Aggregation m_aggregation;
        private string m_id;
        private string m_name;
        private string m_ordinal;
        private string m_isRankingColumn;
        public MishStatStar.StatCategory statCategoryLastBoundTo;
    }

    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "Category")]
    public partial class Category : ModelBase
    {
        /// <remarks/>
        [XmlAttribute(AttributeName = "name")]
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
                NotifyPropertyChanged("Name");
            }
        }

        private string m_name;
    }
    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "Input")]
    public partial class Input : ModelBase
    {
        /// <remarks/>
        [XmlAttribute(AttributeName = "id")]
        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
                NotifyPropertyChanged("Id");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "name")]
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
                NotifyPropertyChanged("Name");
            }
        }

        /// <remarks/>
        [XmlAttribute(AttributeName = "dataType")]
        public string DataType
        {
            get
            {
                return m_dataType;
            }
            set
            {
                m_dataType = value;
                NotifyPropertyChanged("DataType");
            }
        }

        private string m_id;
        private string m_name;
        private string m_dataType;
    }
    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "LeaderboardsConfiguration")]
    public partial class LeaderboardsConfiguration : ModelBase
    {
        /// <remarks/>
        [XmlArray(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName = "Inputs")]
        [XmlArrayItem("Input", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public ObservableCollection<Input> Inputs
        {
            get
            {
                return m_inputs;
            }
            set
            {
                m_inputs = value;
                NotifyPropertyChanged("Inputs");
            }
        }

        /// <remarks/>
        [XmlArray(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName = "Categories")]
        [XmlArrayItem("Category", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public ObservableCollection<Category> Categories
        {
            get
            {
                return m_categories;
            }
            set
            {
                m_categories = value;
                NotifyPropertyChanged("Categories");
            }
        }

        /// <remarks/>
        [XmlArray(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName = "Leaderboards")]
        [XmlArrayItem("Leaderboard", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public ObservableCollection<Leaderboard> Leaderboards
        {
            get
            {
                return m_leaderboards;
            }
            set
            {
                m_leaderboards = value;
                NotifyPropertyChanged("Leaderboards");
            }
        }

        private ObservableCollection<Input> m_inputs;
        private ObservableCollection<Category> m_categories;
        private ObservableCollection<Leaderboard> m_leaderboards;
    }
   /// <summary>
    /// The base class of objects in the Model namespace. The Model namespace CONTAINS NO LOGIC.
    /// Marked as Serializable for Copy/Paste to/from the system clipboard. AllAttributes
    /// and AllElements are used to preserve unrecognized XML nodes.
    /// </summary>
    [Serializable()]
    public class ModelBase : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        public XmlAttribute[] AllAttributes
        {
            get
            {
                return m_allAttributes;
            }
            set
            {
                m_allAttributes = value;
                NotifyPropertyChanged("AllAttributes");
            }
        }

        public XmlElement[] AllElements
        {
            get
            {
                return m_allElements;
            }
            set
            {
                m_allElements = value;
                NotifyPropertyChanged("AllElements");
            }
        }

        private XmlAttribute[] m_allAttributes;
        private XmlElement[] m_allElements;
    }


    /// <remarks/>
    [Serializable()]
    [XmlType(AnonymousType = true, TypeName = "RockstarScsGameConfiguration")]
    [XmlRootAttribute(Namespace = "", ElementName = "RockstarScsGameConfiguration", IsNullable = false)]
    public partial class ScsGameConfiguration : ModelBase
    {
        /// <remarks/>
        [XmlElement(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName = "LeaderboardsConfiguration")]
        public LeaderboardsConfiguration LeaderboardsConfiguration
        {
            get
            {
                return m_leaderboardsConfiguration;
            }
            set
            {
                m_leaderboardsConfiguration = value;
                NotifyPropertyChanged("LeaderboardsConfiguration");
            }
        }

        /// <remarks/>
        [XmlAttribute("gameTitle")]
        public string GameTitle
        {
            get
            {
                return m_gameTitle;
            }
            set
            {
                m_gameTitle = value;
                NotifyPropertyChanged("GameTitle");
            }
        }

        /// <remarks/>
        [XmlAttribute("configVersion")]
        public string ConfigVersion
        {
            get
            {
                return m_configVersion;
            }
            set
            {
                m_configVersion = value;
                NotifyPropertyChanged("ConfigVersion");
            }
        }

        /// <remarks/>
        [XmlAttribute("fileVersion")]
        public int FileVersion
        {
            get
            {
                return m_fileVersion;
            }
            set
            {
                m_fileVersion = value;
                NotifyPropertyChanged("FileVersion");
            }
        }

        private LeaderboardsConfiguration m_leaderboardsConfiguration;
        private string m_gameTitle;
        private string m_configVersion;
        private int m_fileVersion;
    }

    ///////////////////////
    ///////////////////////
    ///////////////////////
    ///////////////////////
    ///////////////////////
    ///////////////////////
    class ScsGameConfigObject
    {
        public readonly static ScsGameConfigObject Instance = new ScsGameConfigObject();

        /// <summary>
        ///  This is used to add the [XmlAnyAttribute()] and [XmlAnyElement()] attributes to the
        ///  AllAttributes and AllElements properties on ModelBase, respectively. If these attributes
        ///  are added in code (compile time) then XmlUnknownAttribute and XmlUnknownElement exceptions
        ///  are never thrown during load, making it impossible to output debug messages for those
        ///  errors.
        /// </summary>
        private static XmlAttributeOverrides GetSerializationOverrides()
        {
            XmlAttributes anyAttAttributes = new XmlAttributes();
            XmlAnyAttributeAttribute anyAtt = new XmlAnyAttributeAttribute();
            anyAttAttributes.XmlAnyAttribute = anyAtt;
            XmlAttributes anyElemAttributes = new XmlAttributes();
            XmlAnyElementAttribute anyElem = new XmlAnyElementAttribute();
            anyElemAttributes.XmlAnyElements.Add(anyElem);
            XmlAttributeOverrides overrides = new XmlAttributeOverrides();
            overrides.Add(typeof(ModelBase), "AllAttributes", anyAttAttributes);
            overrides.Add(typeof(ModelBase), "AllElements", anyElemAttributes);
            return overrides;
        }

        public void LoadConfigFromFile(string fileName)
        {
            using (StreamReader reader = new StreamReader(fileName))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ScsGameConfiguration), GetSerializationOverrides());
                m_config = serializer.Deserialize(reader) as ScsGameConfiguration;

                //DebugOutput.WriteLine("Successfully loading config from " + fileName);
                ///DebugOutput.WriteLine("Inputs: " + m_config.LeaderboardsConfiguration.Inputs.Count);
                //DebugOutput.WriteLine("Leaderboards: " + m_config.LeaderboardsConfiguration.Leaderboards.Count);
                //DebugOutput.WriteLine("Categories: " + m_config.LeaderboardsConfiguration.Categories.Count);
            }
        }

        public ScsGameConfiguration GetConfig()
        {
            return m_config;
        }

        public Input GetInputById(int inputId)
        {
            foreach (Input inpt in m_config.LeaderboardsConfiguration.Inputs)
            {
                int id = Convert.ToInt32(inpt.Id);
                if (id == inputId)
                {
                    return inpt;
                }
            }

            return null;
        }

        public Column GetColumnByIdOnLeaderboard(Leaderboard lb, int id)
        {
            foreach (Column col in lb.Columns)
            {
                int colId = Convert.ToInt32(col.Id);
                if (colId == id)
                {
                    return col;
                }
            }

            return null;
        }

        public Input[] GetInputsOnLeaderboard(Leaderboard lb)
        {
            List<Input> inputList = new List<Input>();
            foreach (Column col in lb.Columns)
            {
                if (col.Aggregation.GameInput != null)
                {
                    int inputID = Convert.ToInt32(col.Aggregation.GameInput.InputId);
                    if (inputID > 0)
                    {
                        //Find the input with this ID.
                        Input lbInput = GetInputById(inputID);
                        if (lbInput != null)
                        {
                            inputList.Add(lbInput);
                        }
                    }
                }
            }

            return inputList.ToArray();
        }

        public ScsGameConfiguration m_config;
    }






}
