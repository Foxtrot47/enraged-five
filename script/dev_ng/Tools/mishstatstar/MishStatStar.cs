﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;


namespace MissionStatsStar
{


    public partial class MishStatStar : Form
    {

        //data is smooshed in with the UI for now 
        LeaderboardCollection leaderboards = null;

        private void TryToLoadAndParseFuzzyXML()
        {

            XmlDocument doc = new XmlDocument();

            //WHY DOES THIS FILE KEEP GETTING RENAMED?
            //string uri = "file:///X:/gta5/xlast/PlayerFuzzy.gameconfig";//"file://" ;
            //string uri = "file:///X:/gta5/xlast/Fuzzy.gameconfig";
            // string uri = "file:///X:/gta5/xlast/NewLeaderboards.ros.xml";
            //string uri = "file:///X:/gta5/xlast/NewLeaderboards.ros.xml";// this new config doesn't fucking load

            //string uri = "X:/gta5/xlast/NewLeaderboards.ros.xml";
            //depot/gta5/xlast/NewLeaderboards.ros.xml 
            /*
            try
            {
                doc.Load(uri);//eLeaderboardtypes.LEADERBOARD_TYPE_CLAN;//
            }
            catch
            {
                MessageBox.Show("gameconfig xml load failed, please make sure you have this file : " + uri + " this will mean exporting leaderboard update procedure cannot complete");
                return;
            }

            eLeaderboardtypes leaderboardtype = eLeaderboardtypes.LEADERBOARD_TYPE_PLAYER;
            PropertyCollection properties = new PropertyCollection(leaderboardtype);
            leaderboards = new LeaderboardCollection(leaderboardtype);
            properties.Reset(doc);
            leaderboards.Reset(doc, properties);

            lbdump.Items.Clear();

            foreach (Leaderboard l in leaderboards.GetLeaderboards())
            {
                lbdump.Items.Add(l);
            }*/



            //scgameconfig2c_2010.ScsGameConfigObject.Instance.  //.Instance.LoadConfigFromFile("X:/gta5/xlast/NewLeaderboards.ros.xml");


            // BASE GAME
            //ScsGameConfigObject.Instance.LoadConfigFromFile("X:/gta5/xlast/NewLeaderboards.ros.xml");

            // DLC
            ScsGameConfigObject.Instance.LoadConfigFromFile("X:/gta5/xlast/NewLeaderboards.tu.ros.xml");

            leaderboards = new LeaderboardCollection(ScsGameConfigObject.Instance.m_config.LeaderboardsConfiguration.Leaderboards);
            // leaderboards.Populate();

            lbdump.Items.Clear();

            foreach (Leaderboard lb in leaderboards.GetLeaderboards())
            {
                lbdump.Items.Add(lb);
            }

        }

        /// <summary>
        /// These have to match up with the stat categories in script
        /// </summary>
        public enum StatCategory
        {
            Unset = -1,

            MISSION_STAT_TYPE_TOTALTIME = 0,
            MISSION_STAT_TYPE_ACTION_CAM_USE,
            MISSION_STAT_TYPE_UNIQUE_BOOL,
            MISSION_STAT_TYPE_WINDOWED_TIMER,
            MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD,
            MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD,
            MISSION_STAT_TYPE_HEADSHOTS,
            MISSION_STAT_TYPE_FRACTION,
            MISSION_STAT_TYPE_ACCURACY,
            MISSION_STAT_TYPE_PURE_COUNT,
            MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE,
            MISSION_STAT_TYPE_FINANCE_DIRECT,
            MISSION_STAT_TYPE_FINANCE_TABLE,

            //new type
            MISSION_STAT_TYPE_PURE_COUNT_DISTANCE, //In pending new upload type
            MISSION_STAT_TYPE_BULLETS_FIRED,//accuracy but shots only
            MISSION_STAT_TYPE_INNOCENTS_KILLED,
            MISSION_STAT_TYPE_SPECIAL_ABILITY_USE,


            MAX_MISSION_STAT_TYPES
        }


        public static string getCatNoteString(StatCategory s)
        {
            switch (s)
            {
                case StatCategory.MISSION_STAT_TYPE_TOTALTIME:
                    return "Total time taken in mission (MS)";
                case StatCategory.MISSION_STAT_TYPE_ACTION_CAM_USE:
                    return "Time using action cam in mission (MS)";
                case StatCategory.MISSION_STAT_TYPE_UNIQUE_BOOL:
                    return "A boolean stat, pass or fail";
                case StatCategory.MISSION_STAT_TYPE_WINDOWED_TIMER:
                    return "A mission sub section timer";
                case StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD:
                    return "Top speed for a single entity";
                case StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD:
                    return "Damage done to a single entity";
                case StatCategory.MISSION_STAT_TYPE_HEADSHOTS:
                    return "Number of headshots on watch list";
                case StatCategory.MISSION_STAT_TYPE_FRACTION:
                    return "A generic X/Y stat";
                case StatCategory.MISSION_STAT_TYPE_ACCURACY:
                    return "Overall mission shot accuracy";
                case StatCategory.MISSION_STAT_TYPE_PURE_COUNT:
                    return "A raw count of X";
                case StatCategory.MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE:
                    return "Same as pure count but shown as a %";
                case StatCategory.MISSION_STAT_TYPE_FINANCE_DIRECT:
                    return "A dollar value set by mission";
                case StatCategory.MISSION_STAT_TYPE_FINANCE_TABLE:
                    return "Dollar values based on models destroyed";

                case StatCategory.MISSION_STAT_TYPE_PURE_COUNT_DISTANCE:
                    return "A pure count type that displays as distance in meters on end screen";
                case StatCategory.MISSION_STAT_TYPE_BULLETS_FIRED:
                    return "Bullets fired over the course of the mission";
                case StatCategory.MISSION_STAT_TYPE_INNOCENTS_KILLED:
                    return "Innocents killed over the course of the mission";
                case StatCategory.MISSION_STAT_TYPE_SPECIAL_ABILITY_USE:
                    return "Time spent using special ability during mission in MS";



            }
            return "No category picked";
        }

        public static string getCatEnum(StatCategory s)
        {

            switch (s)
            {
                case StatCategory.MISSION_STAT_TYPE_TOTALTIME:
                    return "MISSION_STAT_TYPE_TOTALTIME";
                case StatCategory.MISSION_STAT_TYPE_ACTION_CAM_USE:
                    return "MISSION_STAT_TYPE_ACTION_CAM_USE";
                case StatCategory.MISSION_STAT_TYPE_UNIQUE_BOOL:
                    return "MISSION_STAT_TYPE_UNIQUE_BOOL";
                case StatCategory.MISSION_STAT_TYPE_WINDOWED_TIMER:
                    return "MISSION_STAT_TYPE_WINDOWED_TIMER";
                case StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD:
                    return "MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD";
                case StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD:
                    return "MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD";
                case StatCategory.MISSION_STAT_TYPE_HEADSHOTS:
                    return "MISSION_STAT_TYPE_HEADSHOTS";
                case StatCategory.MISSION_STAT_TYPE_FRACTION:
                    return "MISSION_STAT_TYPE_FRACTION";
                case StatCategory.MISSION_STAT_TYPE_ACCURACY:
                    return "MISSION_STAT_TYPE_ACCURACY";
                case StatCategory.MISSION_STAT_TYPE_PURE_COUNT:
                    return "MISSION_STAT_TYPE_PURE_COUNT";
                case StatCategory.MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE:
                    return "MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE";
                case StatCategory.MISSION_STAT_TYPE_FINANCE_DIRECT:
                    return "MISSION_STAT_TYPE_FINANCE_DIRECT";
                case StatCategory.MISSION_STAT_TYPE_FINANCE_TABLE:
                    return "MISSION_STAT_TYPE_FINANCE_TABLE";


                case StatCategory.MISSION_STAT_TYPE_PURE_COUNT_DISTANCE:
                    return "MISSION_STAT_TYPE_PURE_COUNT_DISTANCE";
                case StatCategory.MISSION_STAT_TYPE_BULLETS_FIRED:
                    return "MISSION_STAT_TYPE_BULLETS_FIRED";
                case StatCategory.MISSION_STAT_TYPE_INNOCENTS_KILLED:
                    return "MISSION_STAT_TYPE_INNOCENTS_KILLED";
                case StatCategory.MISSION_STAT_TYPE_SPECIAL_ABILITY_USE:
                    return "MISSION_STAT_TYPE_SPECIAL_ABILITY_USE";

            }

            return "STAT_CATEGORY_UNSET";
        }

        public static string getCatString(StatCategory s)///MISHSTA
        {

            switch (s)
            {
                case StatCategory.MISSION_STAT_TYPE_TOTALTIME:
                    return "Time";
                case StatCategory.MISSION_STAT_TYPE_ACTION_CAM_USE:
                    return "Action Cam Use";
                /*
            case StatCategory.MISSION_STAT_TYPE_UNIQUE_BOOL:
                    return "UNNAMED BOOL STAT!";
            case StatCategory.MISSION_STAT_TYPE_WINDOWED_TIMER:
                    return "";
                 */
                case StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD:
                    return "Max Speed";
                case StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD:
                    return "Damage";
                case StatCategory.MISSION_STAT_TYPE_HEADSHOTS:
                    return "Headshots";
                /*
            case StatCategory.MISSION_STAT_TYPE_FRACTION:
                    return "";
                 */
                case StatCategory.MISSION_STAT_TYPE_ACCURACY:
                    return "Accuracy";
                /*
            case StatCategory.MISSION_STAT_TYPE_PURE_COUNT:
                    return "";
                     
            case StatCategory.MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE:
                    return "";
                 */
                case StatCategory.MISSION_STAT_TYPE_FINANCE_DIRECT:
                case StatCategory.MISSION_STAT_TYPE_FINANCE_TABLE:
                    return "Damage Caused";

                //case StatCategory.MISSION_STAT_TYPE_PURE_COUNT_DISTANCE:

                case StatCategory.MISSION_STAT_TYPE_BULLETS_FIRED:
                    return "Bullets Fired";
                case StatCategory.MISSION_STAT_TYPE_INNOCENTS_KILLED:
                    return "Innocents Killed";
                case StatCategory.MISSION_STAT_TYPE_SPECIAL_ABILITY_USE:
                    return "Special Ability Use";

            }

            return "MISHSSNOTAG";
        }


        public class MissionStat : IComparable
        {
            private static int count = 0;

            public string enumname = "UNNAMED_MISSION_STAT";
            public StatCategory Category = StatCategory.Unset;
            public int baseValue = 0;
            public int targetValue = 1;
            public bool lessThanTarget = false;
            public bool Hidden = false;
            //score ranging
            public int ScoreMin = 0;
            public int ScoreMax = 1;
            //short desc
            public string shortdesc = "";
            //group of settings relating to the
            public string overrideString = "";//does this stat have another display name


            // diff: [REQUIRED] Is this stat used as a differentiator //(do we add on the score from this stat to allow for differentiation between 2 people who both met the objective)? For example if 2 people got > -10 car damage, we can add on the actual damage they took which may place one player above another. (bool)
            public bool lb_differentiator = false;
            // weighting: [REQUIRED] The amount of points this value is worth. For example each 1% of car damage is worth 1 point, but we don't want each millisecond of action cam use to be worth 1 point as it isn't fairly weighted. Therefore each 1 millisecond of action cam use is worth 0.001 points - this is variable and TBD per mission (float)
            public float lb_weight_PPC = 1.0f;
            // min: [REQUIRED] The lowest/worst score the player can legally get, or the cut off point where we don't care anymore if they're lower - may be negated if required (int)
            public int lb_min_legal = 0;
            // max: [REQUIRED] The highest/best score the player can legally get, or the cut off point where we don't care anymore if they#re higher - may be negated if required. Cut off point is particularly useful for unbounded values like time, bullets fired, damage etc. We can record scores up to 30 minutes, and if they go above this, we only add 30 mins worth of bonus score to their time etc. (int)
            public int lb_max_legal = 1;
            // precedence: [REQUIRED for mission objectives] The order which the objectives take precedence. 1 is worth the most. Objectives can have the same precedence level if there is no way to rank one higher than another. (int)
            public int lb_precedence = 1;

            //Saved things used to look up the fuzzy binding
            public uint lb_columnID = 0;
            public string lb_enumerant = "";
            public string igDesc = "";


            int IComparable.CompareTo(object obj)
            {
                return this.ToString().CompareTo(obj.ToString());
            }

            public MissionStat(string namein)
            {

                enumname = namein;//"UNNAMED_MISSION_STAT_" + MissionStat.count;
                ++MissionStat.count;
            }
            public MissionStat(string namein, string descin)
            {
                this.shortdesc = descin;
                enumname = namein;//"UNNAMED_MISSION_STAT_" + MissionStat.count;
                ++MissionStat.count;
            }
            public MissionStat(BinaryReader br)
            {
                enumname = br.ReadString();
                Category = (StatCategory)br.ReadInt32();
                if (Category == StatCategory.Unset)
                {
                    Category = StatCategory.MISSION_STAT_TYPE_UNIQUE_BOOL;
                }
                baseValue = br.ReadInt32();
                targetValue = br.ReadInt32();

                lessThanTarget = br.ReadBoolean();
                Hidden = br.ReadBoolean();
                //score ranging
                ScoreMin = br.ReadInt32();
                ScoreMax = br.ReadInt32();
                //short desc
                shortdesc = br.ReadString();
                overrideString = br.ReadString();

                //group of settings relating to the score value

                lb_differentiator = br.ReadBoolean();
                lb_weight_PPC = (float)br.ReadDouble();
                lb_min_legal = br.ReadInt32();
                lb_max_legal = br.ReadInt32();
                lb_precedence = br.ReadInt32();

                //new 
                lb_columnID = br.ReadUInt32();
                lb_enumerant = br.ReadString();
                igDesc = br.ReadString();


            }

            internal void Write(BinaryWriter bw)
            {
                bw.Write(enumname);
                bw.Write((Int32)Category);
                bw.Write(baseValue);
                bw.Write(targetValue);

                bw.Write(lessThanTarget);
                bw.Write(Hidden);

                bw.Write(ScoreMin);
                bw.Write(ScoreMax);

                bw.Write(shortdesc);
                bw.Write(overrideString);

                bw.Write(lb_differentiator);
                bw.Write((double)lb_weight_PPC);
                bw.Write(lb_min_legal);
                bw.Write(lb_max_legal);
                bw.Write(lb_precedence);

                //new 
                bw.Write(lb_columnID);
                bw.Write(lb_enumerant);
                bw.Write(igDesc);


            }

            public override string ToString()
            {
                return enumname;
            }

            //generate profile stat name if this is a not a hidden stat

            internal MissionStat DuplicateMe()
            {
                MissionStat ne = new MissionStat(enumname + "_DUPE");
                ne.Category = Category;
                ne.baseValue = baseValue;
                ne.targetValue = targetValue;
                ne.lessThanTarget = lessThanTarget;
                ne.Hidden = Hidden;
                ne.ScoreMin = ScoreMin;
                ne.ScoreMax = ScoreMax;
                ne.shortdesc = shortdesc;
                ne.overrideString = overrideString;
                ne.lb_weight_PPC = lb_weight_PPC;
                ne.lb_min_legal = lb_min_legal;
                ne.lb_max_legal = lb_max_legal;
                ne.lb_precedence = lb_precedence;
                ne.igDesc = igDesc;
                ne.lb_differentiator = lb_differentiator;
                return ne;
            }

            internal void SetColBind(uint id, string enumerant)
            {
                lb_columnID = id;
                lb_enumerant = enumerant;
            }

            internal string getXmlTypeBoundaries()
            {
                switch (this.Category)
                {
                    case StatCategory.MISSION_STAT_TYPE_ACCURACY:
                    case StatCategory.MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE:
                        return "Min=\"0\" Max=\"127\"";//
                    case StatCategory.MISSION_STAT_TYPE_UNIQUE_BOOL:
                        return "Min=\"0\" Max=\"3\"";//unset/false/true
                    case StatCategory.MISSION_STAT_TYPE_TOTALTIME:
                           return "Min=\"0\" Max=\"8388607\"";
                    case StatCategory.MISSION_STAT_TYPE_SPECIAL_ABILITY_USE:
                    case StatCategory.MISSION_STAT_TYPE_ACTION_CAM_USE:
                    case StatCategory.MISSION_STAT_TYPE_WINDOWED_TIMER:
                        return "Min=\"0\" Max=\"4194303\"";
                    case StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD:
                        return "Min=\"0\" Max=\"127\"";//
                    case StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD:
                        return "Min=\"0\" Max=\"2047\"";//
                    case StatCategory.MISSION_STAT_TYPE_HEADSHOTS:
                        return "Min=\"0\" Max=\"63\"";//
                    case StatCategory.MISSION_STAT_TYPE_FRACTION:
                        return "Min=\"0\" Max=\"255\"";//
                    case StatCategory.MISSION_STAT_TYPE_PURE_COUNT:
                        return "Min=\"0\" Max=\"255\"";//
                    case StatCategory.MISSION_STAT_TYPE_FINANCE_DIRECT:
                    case StatCategory.MISSION_STAT_TYPE_FINANCE_TABLE:
                        return "Min=\"0\" Max=\"1073741823\"";//
                    case StatCategory.MISSION_STAT_TYPE_PURE_COUNT_DISTANCE:
                    case StatCategory.MISSION_STAT_TYPE_BULLETS_FIRED:
                           return "Min=\"0\" Max=\"131071\"";//
                    case StatCategory.MISSION_STAT_TYPE_INNOCENTS_KILLED:
                        return "Min=\"0\" Max=\"1023\"";//
                }

                return "Min=\"0\" Max=\"2147483646\"";
            }
        }

        public class MissionEntry
        {
            //specified
            //Two leaderboard names for this mission. Last try and best try.
            public string LBLast = "";//last is same thing as indiv
            public uint LBLastID = 0;
            public string LBBest = "";
            public uint LBBestID = 0;

            //The mission’s flow enum
            public string FlowEnum = "UNSET_FLOW_MISSION_ENUM";
            //A mission name string tag
            public string MishString = "";
            public bool bRCMission = false;

            //A list of associated mission stats
            public List<MissionStat> MyStats = null;

            //


            public override string ToString()
            {
                return FlowEnum + " (" + MyStats.Count + ")";
            }

            //generate A best percentage obtained profile stat

            public MissionEntry(string enumname, bool isRC)
            {
                MyStats = new List<MissionStat>();
                FlowEnum = enumname;
                bRCMission = isRC;
            }

            public MissionEntry(BinaryReader br, List<MissionStat> Stats)
            {
                MyStats = new List<MissionStat>();

                LBLast = br.ReadString();
                LBLastID = br.ReadUInt32();

                LBBest = br.ReadString();
                LBBestID = br.ReadUInt32();

                FlowEnum = br.ReadString();
                MishString = br.ReadString();
                bRCMission = br.ReadBoolean();

                int tstats = br.ReadInt32();

                //A list of associated mission stats
                //MyStats
                for (int i = 0; i < tstats; ++i)
                {
                    int ind = br.ReadInt32();

                    if (ind > -1)
                    {
                        MyStats.Add(Stats[ind]);
                    }
                }

            }

            internal void Write(BinaryWriter bw, List<MissionStat> Stats)
            {
                bw.Write(LBLast);

                bw.Write(LBLastID);//new

                bw.Write(LBBest);

                bw.Write(LBBestID);//new

                bw.Write(FlowEnum);
                bw.Write(MishString);
                bw.Write(bRCMission);
                bw.Write(MyStats.Count());

                foreach (MissionStat ms in MyStats)
                {
                    bw.Write(Stats.IndexOf(ms));
                }

            }

            internal void SetLeaderboardBestBind(uint bid, string enumerant)
            {
                LBBestID = bid;

                LBBest = enumerant;
            }

            internal void SetLeaderboardIndivBind(uint bid, string enumerant)
            {
                LBLastID = bid;

                LBLast = enumerant;

            }

            internal void Absorb(MissionEntry dupe)
            {
                foreach (MissionStat ms in dupe.MyStats)
                {
                    MyStats.Add(ms);
                }
            }
        }


        List<MissionStat> Stats;
        List<MissionEntry> Missions;

        MPLeaderboardBinder mpSettings;


        public MishStatStar()
        {
            InitializeComponent();

            Stats = new List<MissionStat>();
            Missions = new List<MissionEntry>();
            mpSettings = new MPLeaderboardBinder();

            categorylist.Items.Clear();

            //getCatEnum(StatCategory s)
            for (int i = 0; i < (int)StatCategory.MAX_MISSION_STAT_TYPES; ++i)
            {
                categorylist.Items.Add(getCatEnum((StatCategory)i).Replace("MISSION_STAT_", "*"));
            }



            //drag drop start bind
            this.statlist.MouseDown += new MouseEventHandler(statlist_MouseDown);
            //this.statlist.SelectedIndexChanged += new System.EventHandler(this.statlist_SelectedIndexChanged);

            this.statbindlist.DragOver += new DragEventHandler(statbindlist_DragOver);
            this.statbindlist.DragDrop += new DragEventHandler(statbindlist_DragDrop);

            this.statlist.KeyUp += new KeyEventHandler(statlist_KeyDown);





            //

            TryToLoadAndParseFuzzyXML();




        }





        private void saveSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {


            SaveFileDialog sfd = new SaveFileDialog();
            sfd.DefaultExt = "mista";
            sfd.Filter = "Mission stat settings file (* .mista)|*.mista";
            DialogResult dr = sfd.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
            {

                Stream s = sfd.OpenFile();
                BinaryWriter bw = new BinaryWriter(s);

                SaveStats(bw);
                SaveMissions(bw);
                //mpSettings.SaveMPBinds(bw);

                s.Close();
            }
        }

        private void SaveMissions(BinaryWriter bw)
        {

            bw.Write(Missions.Count);
            foreach (MissionEntry m in Missions)
            {
                m.Write(bw, Stats);
            }
        }

        private void SaveStats(BinaryWriter bw)
        {
            bw.Write(Stats.Count);
            foreach (MissionStat m in Stats)
            {
                m.Write(bw);
            }
        }

        private void loadSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = "mista";
            ofd.Filter = "Mission stat settings file (* .mista)|*.mista";
            DialogResult dr = ofd.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                Stream s = ofd.OpenFile();
                BinaryReader br = new BinaryReader(s);

                LoadStats(br);
                LoadMissions(br);


                //mpSettings.LoadMPBinds(br,leaderboards);

                //mpSettings.RefreshMyLists(MPBoardExports,MPColIgnore);

                s.Close();
            }
        }
        /// <summary>
        /// memo, this has to happen after the stat load as the binds are index based
        /// </summary>
        /// <param name="br"></param>
        private void LoadMissions(BinaryReader br)
        {
            Missions.Clear();
            int mtoload = br.ReadInt32();

            for (int i = 0; i < mtoload; ++i)
            {
                MissionEntry m = new MissionEntry(br, Stats);
                Missions.Add(m);
            }
            PopulateMissionList();
        }

        private void LoadStats(BinaryReader br)
        {
            Stats.Clear();
            int stoload = br.ReadInt32();
            for (int i = 0; i < stoload; ++i)
            {
                MissionStat m = new MissionStat(br);
                Stats.Add(m);
            }
            PopulateStatList();
        }

        private void PopulateMissionList(MissionEntry mish = null)
        {
            object se = missionlist.SelectedItem;



            missionlist.Items.Clear();
            foreach (MissionEntry m in Missions)
            {
                missionlist.Items.Add(m);
            }

            if (mish == null)
            {
                if (se != null)
                {
                    if (missionlist.Items.Contains(se))
                    {
                        missionlist.SelectedItem = se;
                    }
                }
            }
            else
            {
                if (missionlist.Items.Contains(mish))
                {
                    missionlist.SelectedItem = mish;
                }
            }

        }
        private void PopulateStatList()
        {
            object se = statlist.SelectedItem;

            statlist.Items.Clear();
            foreach (MissionStat m in Stats)
            {
                statlist.Items.Add(m);
            }


            //do optional ordering pass
            statlist.Sorted = true;  //.SortDescriptions.Clear();
            //statlist.Items//.SortDescriptions.Add(new SortDescription(null, ListSortDirection.Ascending));




            if (se != null)
            {
                if (statlist.Items.Contains(se))
                {
                    statlist.SelectedItem = se;
                }
            }
        }

        MissionStat GetSelectedMissionStat()
        {
            MissionStat ms = (MissionStat)statlist.SelectedItem;
            return ms;
        }
        MissionEntry GetSelectedMission()
        {
            MissionEntry me = (MissionEntry)missionlist.SelectedItem;
            return me;
        }

        private void missionlist_SelectedIndexChanged(object sender, EventArgs e)
        {
            //populate the data for the selected mission
            //statlist.ClearSelected();

            if (missionlist.SelectedItem == null)
            {
                return;
            }

            MissionEntry me = GetSelectedMission();

            mishflowenum.Text = me.FlowEnum;


            missionleadboardbest.Text = "'" + me.LBBest + "' : " + me.LBBestID;
            missionflowlblast.Text = "'" + me.LBLast + "' : " + +me.LBLastID;
            // missionstringtag.Text = me.MishString;


            //statbindlist
            statbindlist.Items.Clear();


            foreach (MissionStat ms in me.MyStats)
            {
                statbindlist.Items.Add(ms);
            }


            isrcmission.Checked = me.bRCMission;
        }

        private void statlist_SelectedIndexChanged()//object sender, EventArgs e)
        {
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();
            //bw.Write(enumname);statenumname
            statenumname.Text = ms.enumname;
            //bw.Write((Int32)Category);categorylist


            categorylist.SelectedIndex = (int)ms.Category;



            //bw.Write(baseValue);basevalue

            basevalue.Text = ms.baseValue.ToString();

            //bw.Write(targetValue);targetvalue
            targetvalue.Text = ms.targetValue.ToString();

            //bw.Write(lessThanTarget); notlessthan / islessthan

            if (ms.lessThanTarget)
            {
                islessthan.Checked = true;
            }
            else
            {
                notlessthan.Checked = true;
            }



            //bw.Write(Hidden);//hiddenonpass
            hiddenonpass.Checked = ms.Hidden;


            //bw.Write(ScoreMin); minscore
            minscore.Text = ms.ScoreMin.ToString();


            //bw.Write(ScoreMax);maxscore
            maxscore.Text = ms.ScoreMax.ToString();

            //bw.Write(overrideString); overridecatstring 
            overridecatstring.Text = ms.overrideString;

            //bw.Write(shortdesc); / shortdescription
            shortdescription.Text = ms.shortdesc;


            ingdesc.Text = ms.igDesc;

            lbisdiff.Checked = ms.lb_differentiator;
            //bw.Write((double)lb_weight_PPC);lbweight
            lbweight.Text = ms.lb_weight_PPC.ToString();
            //bw.Write((double)lb_min_legal);minlegal
            minlegal.Text = ms.lb_min_legal.ToString();
            //bw.Write((double)lb_max_legal);maxlegal
            maxlegal.Text = ms.lb_max_legal.ToString();
            //bw.Write((double)lb_precedence);precedence
            precedence.Text = ms.lb_precedence.ToString();

            LBCol.Text = "'" + ms.lb_enumerant + "' : " + ms.lb_columnID;







        }

        private void categorylist_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (categorylist.SelectedIndex == -1)
            {
                return;
            }

            catnotes.Text = getCatNoteString((StatCategory)categorylist.SelectedIndex);

            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();

            ms.Category = (StatCategory)categorylist.SelectedIndex;



        }

        private void statlist_MouseDown(object sender, MouseEventArgs e)
        {
            Capture = false;
            base.OnMouseDown(e);
            //attempt to start a drag on selected mission stat


            statlist_SelectedIndexChanged();


            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();


            DragDropEffects dde = DoDragDrop(ms, DragDropEffects.All);


            if (dde == DragDropEffects.All)
            {
                //drop completed?
            }



        }

        private void statlist_KeyDown(object sender, KeyEventArgs e)
        {
            statlist_SelectedIndexChanged();
            if (statlist.SelectedItem == null)
            {
                return;
            }

        }



        private void statbindlist_DragOver(object sender, System.Windows.Forms.DragEventArgs e)
        {
            //dragging stats into selected missions
            if (missionlist.SelectedItem == null)
            {
                e.Effect = DragDropEffects.None;
                return;
            }

            // MissionEntry me = GetSelectedMission();

            e.Effect = DragDropEffects.All;//ready to accept the drop apparently

        }

        private void statbindlist_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
        {

            if (missionlist.SelectedItem == null)
            {
                e.Effect = DragDropEffects.None;
                return;
            }
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();
            MissionEntry me = GetSelectedMission();


            if (me.MyStats.Contains(ms))
            {
                //cannot bind a stat more than once, and not again to its own mission
                return;
            }


            //do check for it being in any other mission

            foreach (MissionEntry mishen in Missions)
            {
                if (me != mishen)//don't check against self, this was done first
                {
                    if (mishen.MyStats.Contains(ms))
                    {
                        //ask the user if they want to overwrite this, bail if no

                        string message = "The stat '" +
                            ms.enumname +
                            "' is already used in mission " +
                            mishen.FlowEnum +
                            ". Do you want to bind it to " +
                            me.FlowEnum + " instead?";


                        DialogResult dr = MessageBox.Show(message, "stat conflict", MessageBoxButtons.YesNo);

                        if (dr == System.Windows.Forms.DialogResult.Yes)
                        {
                            //purge from this one
                            mishen.MyStats.Remove(ms);
                        }
                        else
                        {
                            //bail
                            return;
                        }


                    }

                }
            }



            me.MyStats.Add(ms);
            statbindlist.Items.Add(ms);

            PopulateMissionList();

        }

        private void addMission_Click(object sender, EventArgs e)
        {
            /*
            if (missionlist.SelectedItem == null)
            {
                return;
            }
            MissionEntry me = GetSelectedMission();
            */
            
            MissionEntry mish = new MissionEntry("UNNAMED_MISSION_ENTRY", false);
            if (missionlist.SelectedIndex >= 0 && missionlist.SelectedIndex < Missions.Count)
                Missions.Insert(missionlist.SelectedIndex, mish);
            else
                Missions.Add(mish);


            PopulateMissionList(mish);
        }



        private void removeMission_Click(object sender, EventArgs e)
        {
            if (missionlist.SelectedItems.Count < 1)
            {
                return;
            }
            foreach (object me in missionlist.SelectedItems)
            {
                Missions.Remove((MissionEntry)me);
            }




            PopulateMissionList();
        }

        private void unbindstat_Click(object sender, EventArgs e)
        {
            if (missionlist.SelectedItem == null)
            {
                return;
            }
            if (statbindlist.SelectedItems.Count < 1)
            {
                return;
            }

            MissionEntry me = GetSelectedMission();
            foreach (object m in statbindlist.SelectedItems)
            {
                me.MyStats.Remove((MissionStat)m);
            }



            PopulateMissionList(me);

        }

        private void mishflowenum_TextChanged(object sender, EventArgs e)
        {
            if (missionlist.SelectedItem == null)
            {
                return;
            }
            MissionEntry me = GetSelectedMission();
            bool doReferesh = false;

            if (me.FlowEnum.CompareTo(mishflowenum.Text) != 0)
            {
                doReferesh = true;
            }

            me.FlowEnum = mishflowenum.Text;

            if (doReferesh)
            {
                PopulateMissionList();
            }

        }

        private void missionleadboardbest_TextChanged(object sender, EventArgs e)
        {
            /*
            if (missionlist.SelectedItem == null)
            {
                return;
            }
            MissionEntry me = GetSelectedMission();

            me.LBBest = missionleadboardbest.Text;
             * */
        }

        private void missionflowlblast_TextChanged(object sender, EventArgs e)
        {
            /*
            if (missionlist.SelectedItem == null)
            {
                return;
            }
            MissionEntry me = GetSelectedMission();

            me.LBLast = missionflowlblast.Text;
             */
        }

        private void missionstringtag_TextChanged(object sender, EventArgs e)
        {
            if (missionlist.SelectedItem == null)
            {
                return;
            }
            MissionEntry me = GetSelectedMission();

            //me.MishString = missionstringtag.Text;
        }

        private void isrcmission_CheckedChanged(object sender, EventArgs e)
        {
            if (missionlist.SelectedItem == null)
            {
                return;
            }
            MissionEntry me = GetSelectedMission();

            me.bRCMission = isrcmission.Checked;
        }

        private void addstat_Click(object sender, EventArgs e)
        {
            Stats.Add(new MissionStat("UNNAMED_MISSION_STAT"));

            PopulateStatList();
        }

        private void dupstat_Click(object sender, EventArgs e)
        {
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();

            Stats.Add(ms.DuplicateMe());

            PopulateStatList();
        }

        private void delstat_Click(object sender, EventArgs e)
        {
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();


            foreach (MissionEntry me in Missions)
            {
                if (me.MyStats.Contains(ms))
                {
                    string message = "This stat is used by mission: '" +
                    me.FlowEnum + "' do you still want to delete it?";

                    DialogResult dr = MessageBox.Show(message, "Stat Warning", MessageBoxButtons.YesNo);

                    if (dr == System.Windows.Forms.DialogResult.No)
                    {
                        return;
                    }
                    me.MyStats.Remove(ms);

                }
            }

            Stats.Remove(ms);
            PopulateMissionList();
            PopulateStatList();
        }









        private void statenumname_TextChanged(object sender, EventArgs e)
        {


            //public string enumname = "UNNAMED_MISSION_STAT";
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();




            ms.enumname = statenumname.Text;

            PopulateStatList();
        }

        private void basevalue_TextChanged(object sender, EventArgs e)
        {
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();
            //public int baseValue = 0;
            int ival = 0;
            try
            {
                ival = int.Parse(basevalue.Text);
                ms.baseValue = ival;
                basevalue.BackColor = Color.White;
            }
            catch
            {
                basevalue.BackColor = Color.Red;
            }

        }

        private void targetvalue_TextChanged(object sender, EventArgs e)
        {
            //public int targetValue = 1;
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();
            //public int baseValue = 0;
            int ival = 0;
            try
            {
                ival = int.Parse(targetvalue.Text);
                ms.targetValue = ival;
                targetvalue.BackColor = Color.White;
            }
            catch
            {
                targetvalue.BackColor = Color.Red;
            }
        }

        private void notlessthan_CheckedChanged(object sender, EventArgs e)
        {
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();
            //public bool lessThanTarget = false;
            if (notlessthan.Checked)
            {
                ms.lessThanTarget = false;
            }

        }

        private void islessthan_CheckedChanged(object sender, EventArgs e)
        {
            //public bool lessThanTarget = true;
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();
            if (islessthan.Checked)
            {
                ms.lessThanTarget = true;
            }

        }

        private void hiddenonpass_CheckedChanged(object sender, EventArgs e)
        {
            //public bool Hidden = false;
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();

            ms.Hidden = hiddenonpass.Checked;
        }

        private void shortdescription_TextChanged(object sender, EventArgs e)
        {

            //public string shortdesc = "";
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();

            ms.shortdesc = shortdescription.Text;


        }

        private void ingdesc_TextChanged(object sender, EventArgs e)
        {
            //public string shortdesc = "";
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();

            ms.igDesc = ingdesc.Text;
        }


        private void overridecatstring_TextChanged(object sender, EventArgs e)
        {
            //public string overrideString = "";
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();

            ms.overrideString = overridecatstring.Text;

        }

        private void minscore_TextChanged(object sender, EventArgs e)
        {

            //public int ScoreMin = 0;
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();
            int ival = 0;
            try
            {
                ival = int.Parse(minscore.Text);
                ms.ScoreMin = ival;
                minscore.BackColor = Color.White;
            }
            catch
            {
                minscore.BackColor = Color.Red;
            }
        }

        private void maxscore_TextChanged(object sender, EventArgs e)
        {
            //public int ScoreMax = 1;
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();
            int ival = 0;
            try
            {
                ival = int.Parse(maxscore.Text);
                ms.ScoreMax = ival;
                maxscore.BackColor = Color.White;
            }
            catch
            {
                maxscore.BackColor = Color.Red;
            }
        }

        private void lbweight_TextChanged(object sender, EventArgs e)
        {
            //public float lb_weight_PPC = 1.0f;
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();

            float fval = 0.0f;

            try
            {
                fval = float.Parse(lbweight.Text);
                ms.lb_weight_PPC = fval;
                lbweight.BackColor = Color.White;
            }
            catch
            {
                lbweight.BackColor = Color.Red;
            }

        }

        private void minlegal_TextChanged(object sender, EventArgs e)
        {
            //public int lb_min_legal = 0;
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();
            int ival = 0;
            try
            {
                ival = int.Parse(minlegal.Text);
                ms.lb_min_legal = ival;
                minlegal.BackColor = Color.White;
            }
            catch
            {
                minlegal.BackColor = Color.Red;
            }
        }

        private void maxlegal_TextChanged(object sender, EventArgs e)
        {
            //public int lb_max_legal = 1;
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();
            int ival = 0;
            try
            {
                ival = int.Parse(maxlegal.Text);
                ms.lb_max_legal = ival;
                maxlegal.BackColor = Color.White;
            }
            catch
            {
                maxlegal.BackColor = Color.Red;
            }


        }

        private void precedence_TextChanged(object sender, EventArgs e)
        {
            //public int lb_precedence = 1;
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();
            int ival = 0;
            try
            {
                ival = int.Parse(precedence.Text);
                ms.lb_precedence = ival;
                precedence.BackColor = Color.White;
            }
            catch
            {
                precedence.BackColor = Color.Red;
            }
        }

        private void exportAllToDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            DialogResult dr = fbd.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                ExportGenerator expgen = new ExportGenerator(Missions, Stats, leaderboards);

                expgen.exportStringEntriesToFile(fbd.SelectedPath + "\\ToGoIntoStrings.txt");
                expgen.exportHeaderFile(fbd.SelectedPath + "\\mission_stat_generated_globals.sch");
                expgen.exportFunctionFile(fbd.SelectedPath + "\\mission_stat_generated_private.sch");
                expgen.exportStatFile(fbd.SelectedPath + "\\ToGoIntospStatsSetup.xml");
                expgen.exportImplementationNotes(fbd.SelectedPath + "\\ImplementationNotesForScripters.txt");

                //expgen.exportLeaderboardConsistancyWarnings(fbd.SelectedPath + "\\LeaderboardBindWarnings.txt");
                expgen.exportLeaderboardHeader(fbd.SelectedPath + "\\mission_stat_generated_lb_binds.sch");
                //expgen.exportMishNoVis(fbd.SelectedPath + "\\missions_with_no_visible.txt");
                // expgen.exportLBWebBindXML(fbd.SelectedPath + "\\LBWebBindXML.xml");

                expgen.exportSCDevEntriesToFile(fbd.SelectedPath + "\\SocialClubDevNotes.txt");
                expgen.exportSCDevLBEntriesToFile(fbd.SelectedPath + "\\SocialClubDevLBs.txt");
                expgen.validateStats(fbd.SelectedPath + "\\LeaderboardBindValidation.txt");

            }
        }

        private void exportToXgta5ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //note paths



        }

        private void exportToAGTHeadersToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            string fileName, fileName2, fileName3, fileName4, fileName5;
            if (NGmodeBox.Checked) {
                fileName = "x:\\gta5\\script\\dev_ng\\singleplayer\\include\\globals\\mission_stat_generated_globals.sch";
                fileName2 = "X:\\gta5\\script\\dev_ng\\dlcscripts\\dlcAgentTrevor\\headers\\private\\mission_stat_generated_private_AGT.sch";
                fileName3 = "X:\\gta5\\script\\dev_ng\\dlcscripts\\dlcAgentTrevor\\headers\\private\\mission_stat_generated_lb_binds_AGT.sch";
                fileName4 = "X:\\gta5_dlc\\spPacks\\dlc_agentTrevor\\build\\dev_ng\\common\\data\\spStatsSetup.xml";
                fileName5 = "X:\\gta5_dlc\\spPacks\\dlc_agentTrevor\\assets_ng\\GameText\\American\\americanMission.txt";
            }
            else  {
                fileName = "x:\\gta5\\script\\dev_network\\singleplayer\\include\\globals\\mission_stat_generated_globals.sch";
                fileName2 = "X:\\gta5\\script\\dev_network\\dlcscripts\\dlcAgentTrevor\\headers\\private\\mission_stat_generated_private_AGT.sch";
                fileName3 = "X:\\gta5\\script\\dev_network\\dlcscripts\\dlcAgentTrevor\\headers\\private\\mission_stat_generated_lb_binds_AGT.sch";
                fileName4 = "X:\\gta5_dlc\\spPacks\\dlc_agentTrevor\\build\\dev\\common\\data\\spStatsSetup.xml";
                fileName5 = "X:\\gta5_dlc\\spPacks\\dlc_agentTrevor\\assets\\GameText\\American\\americanMission.txt";

            }

            ExportGenerator expgen = new ExportGenerator(Missions, Stats, leaderboards);

            expgen.exportStringEntriesToFile("X:\\StatsExport\\ToGoIntoStrings.txt");
            //expgen.exportHeaderFile("X:\\gta5\\script\\dev_network\\singleplayer\\include\\globals\\dlc\\dlcmission_stat_generated_globals_AGT.sch");
            expgen.replaceEnumsInHeaderFile(fileName);
            expgen.exportFunctionFile(fileName2);
            expgen.exportStatFile("X:\\StatsExport\\ToGoIntospStatsSetup.xml");
            expgen.exportImplementationNotes("X:\\StatsExport\\ImplementationNotesForScripters.txt");

            //expgen.exportLeaderboardConsistancyWarnings(fbd.SelectedPath + "\\LeaderboardBindWarnings.txt");
            expgen.exportLeaderboardHeader(fileName3);
            //expgen.exportMishNoVis(fbd.SelectedPath + "\\missions_with_no_visible.txt");
            // expgen.exportLBWebBindXML(fbd.SelectedPath + "\\LBWebBindXML.xml");

            expgen.exportSCDevEntriesToFile("X:\\StatsExport\\SocialClubDevNotes.txt");
            expgen.exportSCDevLBEntriesToFile("X:\\StatsExport\\SocialClubDevLBs.txt");
            expgen.validateStats("X:\\StatsExport\\LeaderboardBindValidation.txt");

            //Start File copy windows
            MessageBox.Show("Please copy the new stats from the generated XML to the game XML");
            System.Diagnostics.Process.Start("X:\\StatsExport\\ToGoIntospStatsSetup.xml");
            System.Diagnostics.Process.Start(fileName4);
            MessageBox.Show("When done copying, close the XML files and press OK.\nThere is another set of lines to be copied");
            System.Diagnostics.Process.Start("X:\\StatsExport\\ToGoIntoStrings.txt");
            System.Diagnostics.Process.Start(fileName5);
            MessageBox.Show("AGT stats export finished");
                
        }

        //Select all text on click in the mission name textbox
        private void mishflowenum_Click(object sender, System.EventArgs e)
        {
            mishflowenum.SelectAll();
        }

        //Handle pressing Enter by adding a new entry
        private void mishflowenum_Enter(object sender, System.EventArgs e)
        {
            KeyPressEventArgs ea = e as KeyPressEventArgs;
            if (ea.KeyChar == (char)13)
                addMission_Click(sender, e);
        }



        private void tempstatsadd_Click(object sender, EventArgs e)
        {


            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Kill Chain – Player kills X number of aliens in under X amount of seconds (more likely with Minigun). Barry 1 "));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Unmarked – Player takes less than X% damage during the shoot-out.Barry 1 "));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Clown Car – Player destroys X clown vans quickly before they have a chance to spawn clowns. Barry 2 "));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Unmarked – Player takes less than X% damage during the shoot-out. Barry 2 "));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Mission Time – Player delivers vehicle under a set amount of time (more generous due to losing cops). Barry 3A"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Crush 'em – Player drives over X number of vehicles with the Monster Truck. Barry 3A"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Mission Time – Player delivers vehicle within a set amount of time.  Barry 3C "));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Hooked – Stash car never unhooks from tow truck. Barry 3C "));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Sea legs – Player exits the club without falling over. Dale 2 "));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Road Kill – Player runs down and kills Dale with a vehicle. Dale 2 "));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Cut! – Player kills Dreyfuss before he reaches his car. Dreyfuss 1 "));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Use the Force – Player completes the mission without using the artefact detector. Epsilon 4"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Saviour – Player saves the epsilonist by killing the soldiers that turn up. Epsilon 6 "));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Touchdown – Player lands the epsilon plane perfectly (no damage). Epsilon 6"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Judas/Show me the Money – Player steals the epsilon money and escapes. Epsilon 8"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Extremist Exit/Cult Intervention – Player wipes out all epsilon security. Epsilon 8"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Downhill King – Player wins the bike race against Dom. Extreme 1"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Big Air – Player gets 2.5 seconds of air time during the bike race (can be done). Extreme 1"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Free Faller – Player falls for 7 seconds before opening shoot. Extreme 1"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Sky Blazer – Player performs X number of spins on the quad bike. Extreme 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Dive Bomber – Player jumps off ATV and free falls into water (doesn’t use parachute).  Extreme 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Bullseye – Player lands perfectly on the back of the truck. Extreme 3"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Ladies First – Player opens their parachute after Dom. Extreme 3"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Leap of Faith – Player jumps and survives after following Dom; chute auto opens. Extreme 4"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Contender – Win without using shortcuts.  Fanatic 1"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Athlete – Win without using shortcuts. Fanatic 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Champion – Win without using shortcuts. Fanatic 3"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Bad Signal – Player hits all three satellite dishes without missing. Hunting 1"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Pop! Pop! – Player has X% shooting accuracy popping all three car tires. Hunting 1"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "2 for 1 – Player kills 2 coyotes with one shot.  Hunting 1"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Head Hunter – Player kills all three elk with headshots. Hunting 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Downwind – Player does not get detected by any elk. Hunting 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Quick Catch – Player stops Avery’s car within X seconds of him leaving his house. Josh 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Pulverizer – Player uses a melee weapon to beat up Avery. Josh 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Pyromaniac – Player pours fuel trail in one go. Josh 3"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Out of the Frying Pan/No Time for Bacon – Player escapes without alerting the cops. Josh 3"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Hot Pursuit – Player escapes in the parked cop car. Josh 4"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Dirty Rat – Player kills Josh before escaping. Josh 4"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Stop the Music – Player stops the Mariachi within X time of the leaving bar.  Minute 1"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Mariachi My Ride – Player takes the Mariachi vehicle after stunning both band members   Minute 1"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Shock and Awe – Player/AI uses stun gun to stop all immigrants (not ramming the off road) Minute 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Downed – Capture the first immigrant within X seconds. Minute 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Double Downed – Capture the second group of immigrants within X time. Minute 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "No Migration – Player kills Joe and Josef before they can leave the farm. Minute 3"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "What goes around… – Player uses the stun gun on both Joe and Josef before killing them. Minute 3"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Shattered – Player smashes all windows on the car. Mrs A vs. B 1"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Dry as a Bone – Player dumps the car in the sea without getting wet. Mrs A vs. B 1"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Good for the Skin – Player takes a dip in the piss filled pool. Mrs A vs. B 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Garden Hopper – Player loses the wanted rating on foot. Mrs A vs. B 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Attention! – Player waits for all neighbours respond to Mrs A. Mrs A vs. B 3"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Not a Scratch – Player takes less than X% vehicle damage during drive.  Mrs A vs. B 3"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Hot Shot – Player captures the perfect photograph on first attempt. Mrs A vs. B 4"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Eagle Eye – Player finds Bethany and Marvin within X seconds at hotel. Mrs A vs. B 4"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Not a Scratch – Player takes less than X% vehicle damage during drive.  Mrs A vs. B 4"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Lady killer – Player kills Mrs A and Mrs B with a headshot each. Mrs A vs. B 5"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Pugilist of Peace – Player takes out the body guards without killing them. Nigel 1A"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Salt in the Wound – Player escapes in Robbie Nails car. Nigel 1A"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "G.O.A.T – Player takes less than X damage during the fight with Robbie Nails.  Nigel 1A"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Sneak Thief – Player steals the clothes without detection. Nigel 1B"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Weed Killer – Player take outs the gardener. Nigel 1B"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Hot on the Paws – Player keeps close to Mr Muffy Cakes throughout chase. Nigel 1C"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "FOUR! – Kill Glen Stanky and his 3 security guards. Nigel 1D"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Hole in One – Headshot Glen Stanky. Nigel 1D"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Under Par – Player steals the golf club off Glen Stanky in under X seconds after Starky is alerted or killed. Nigel 1D"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Stalker – Player keeps close to Al Di Napoli during the chase. Nigel 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Accident & Emergency – Player doesn’t harm anyone whilst driving through the hospital. Nigel 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Not a Scratch – Player takes less than X% vehicle damage during chase. Nigel 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Skin of your Teeth – Player exits the car at the last possible moment on train tracks. Nigel 3"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Tickets Please/Locomotivation – Player reverses down train tracks to kill Al Di Napoli. Nigel 3"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Smack Down – Player ensures Beverly takes out the rival on his first swing. Paparazzo 1"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Picture Perfect – Player helps Beverly snap X number of pictures. Paparazzo 1"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Sucker Punch – Player takes out the security guard whilst fleeing to the car. Paparazzo 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Quick Dip – Player jumps into the swimming pool whilst following Beverly. Paparazzo 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Money shot – Player gets face recognition software up to X% during chase. Paparazzo 2"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Thick of it – Player stays close to Poppy throughout the chase. Paparazzo 3A"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "DUI Diva – Player takes the photo of Poppy once she’s been cuffed by the cop. Paparazzo 3A"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Silent Snapper – Player takes the photograph of the Princess without being spotted. Paparazzo 3B"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Royal Drag – Player takes the photo of the Princess as she’s taking a drag. Paparazzo 3B"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Action! – Player kills the entire crew with one shot (explosives). Paparazzo 4"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Wounded – Player wounds the Sasquatch X times before catching him. Sasquatch"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Hunter – Player kills X ambient animals whilst chasing the Sasquatch. Sasquatch"));
            Stats.Add(new MissionStat("UNNAMEDRCMSTAT", "Mr Green – Player heads to the scat site on foot (doesn’t use Dune buggy). Sasquatch"));






            PopulateStatList();
        }

        private void whatIsThisToolStripMenuItem_Click(object sender, EventArgs e)
        {

            System.Diagnostics.Process.Start("http://rsgediwiki1/wiki/index.php/MissionStatStar");


        }

        private void lbisdiff_CheckedChanged(object sender, EventArgs e)
        {
            //public float lb_weight_PPC = 1.0f;
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();




            ms.lb_differentiator = lbisdiff.Checked;


        }


        private void lbdump_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (lbdump.SelectedItem == null)
            {
                return;
            }
            Leaderboard lb = (Leaderboard)lbdump.SelectedItem;
            debugcols.Items.Clear();
            foreach (Column c in lb.Columns)
            {
                debugcols.Items.Add(c);
            }





        }

        private void mishbestbind_Click(object sender, EventArgs e)
        {

            if (lbdump.SelectedItem == null)
            {
                return;
            }
            Leaderboard lb = (Leaderboard)lbdump.SelectedItem;

            //is a mission selected?
            if (missionlist.SelectedItem == null)
            {
                return;
            }
            MissionEntry me = GetSelectedMission();


            //TODO error check the leaderboard for the constant stats


            //attempt to bind lb to me best
            me.SetLeaderboardBestBind(uint.Parse(lb.Id), lb.Name);
            PopulateMissionList();


        }

        private void mishindivbind_Click(object sender, EventArgs e)
        {

            if (lbdump.SelectedItem == null)
            {
                return;
            }
            Leaderboard lb = (Leaderboard)lbdump.SelectedItem;

            //is a mission selected?
            if (missionlist.SelectedItem == null)
            {
                return;
            }
            MissionEntry me = GetSelectedMission();

            //TODO error check the leaderboard for the constant stats

            //attempt to bind lb to me best
            me.SetLeaderboardIndivBind(uint.Parse(lb.Id), lb.Name);
            PopulateMissionList();


        }



        private void lbbindcolstat_Click(object sender, EventArgs e)
        {
            if (debugcols.SelectedItem == null)
            {
                return;
            }
            Column cb = (Column)debugcols.SelectedItem;

            //is a mission selected?
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = GetSelectedMissionStat();

            ms.SetColBind(uint.Parse(cb.Id), cb.Name);

            statlist_SelectedIndexChanged();
        }

        private void findstatboundto_Click(object sender, EventArgs e)
        {
            if (statlist.SelectedItem == null)
            {
                return;
            }
            MissionStat ms = (MissionStat)statlist.SelectedItem;
            //missionlist.Items

            //PopulateMissionList

            foreach (MissionEntry me in Missions)
            {
                if (me.MyStats.Contains(ms))
                {
                    missionlist.SelectedItem = me;
                    PopulateMissionList(me);
                    return;
                }
            }


            MessageBox.Show(ms.enumname + " does not appear to be bound to any mission!");
        }

        private void changeStatsEndingInINNOCENTSKILLEDToThatCategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (MissionStat ms in Stats)
            {
                //_INNOCENTS_KILLED
                if (ms.enumname.Contains("_INNOCENTS_KILLED"))
                {
                    ms.Category = StatCategory.MISSION_STAT_TYPE_INNOCENTS_KILLED;
                }
            }
        }

        private void changeStatsEndingInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (MissionStat ms in Stats)
            {
                //_SPECIAL_ABILITY_TIME
                if (ms.enumname.Contains("_SPECIAL_ABILITY_TIME"))
                {
                    ms.Category = StatCategory.MISSION_STAT_TYPE_SPECIAL_ABILITY_USE;
                }
            }
        }

        private void changeStatsContainingBULLETSFIREDToThatCategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (MissionStat ms in Stats)
            {
                //_SPECIAL_ABILITY_TIME
                if (ms.enumname.Contains("_BULLETS_FIRED"))
                {
                    ms.Category = StatCategory.MISSION_STAT_TYPE_BULLETS_FIRED;
                }
            }

        }

        private void bullshitswitch_Click(object sender, EventArgs e)
        {
            //"AGGREGATE" -> "INDIV_RECORDS"

            foreach (MissionEntry me in Missions)
            {
                me.LBLast = me.LBLast.Replace("AGGREGATE", "INDIV_RECORDS");
            }
        }

        private void listOfCompaniesWithStatsButNoneVisibleToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        /*
        private void addMPexportboard_Click(object sender, EventArgs e)
        {

            if (lbdump.SelectedItem == null)
            {
                return;
            }
            Leaderboard lb = (Leaderboard)lbdump.SelectedItem;

            MPBoardExports.Items.Add(mpSettings.AddLB(lb));

            //MPBoardExports
            
        }*/
        /*
        private void addMPignorecol_Click(object sender, EventArgs e)
        {
            if (debugcols.SelectedItem == null)
            {
                return;
            }
            Leaderboard.Column cb = (Leaderboard.Column)debugcols.SelectedItem;

            MPColIgnore.Items.Add(mpSettings.AddCOL(cb));

            //MPColIgnore
        }
        */
        /*
        private void removeMPboard_Click(object sender, EventArgs e)
        {
            //MPBoardExports
            if (MPBoardExports.SelectedItem == null)
            {
                return;
            }
            mpSettings.RemoveLB(MPBoardExports.SelectedItem);
            MPBoardExports.Items.Remove(MPBoardExports.SelectedItem);

        }
        */
        /*
        private void removeMPcolignore_Click(object sender, EventArgs e)
        {
            //MPColIgnore
            if (MPColIgnore.SelectedItem == null)
            {
                return;
            }
            mpSettings.RemoveCOL(MPColIgnore.SelectedItem);
            MPColIgnore.Items.Remove(MPColIgnore.SelectedItem);



        }
        */

        private void exportMPHeaderToolStripMenuItem_Click(object sender, EventArgs e)
        {/*
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            DialogResult dr = fbd.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                ExportGenerator expgen = new ExportGenerator(Missions, Stats, leaderboards);

                mpSettings.ExportScriptHeader(fbd.SelectedPath + "\\LeaderboardDirectCalls.sch");

            }*/
        }

        private void debugcols_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void rebindLeadboardIDsAndColIDsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (MissionStatsStar.MishStatStar.MissionEntry me in Missions)
            {
                if (me.LBBest != null && me.LBLast != null && me.LBBestID != 0 && me.LBLastID != 0)
                {
                    uint r = getLbIndexFromName(me.LBBest);
                    if (r > 0)
                    {
                        me.LBBestID = r;
                    }
                    else
                    {
                        me.LBBestID = 0;
                        me.LBBest = "";
                    }
                    r = getLbIndexFromName(me.LBLast);
                    if (r > 0)
                    {
                        me.LBLastID = r;
                    }
                    else
                    {
                        me.LBLastID = 0;
                        me.LBLast = "";
                    }



                    if (me.MyStats.Count > 0 && me.LBBest != null && me.LBLast != null && me.LBBestID != 0 && me.LBLastID != 0)
                    {

                        foreach (MishStatStar.MissionStat ms in me.MyStats)
                        {
                            if (ms.lb_enumerant != null && ms.lb_columnID > 0)
                            {
                                r = getColIDFromNameAndBoards(me.LBBest, me.LBLast, ms.lb_enumerant);

                                if (r > 0)
                                {
                                    ms.lb_columnID = r;
                                }
                                else
                                {
                                    ms.lb_enumerant = "";
                                    ms.lb_columnID = 0;
                                }
                            }
                        }

                    }
                }
            }
        }

        private uint getColIDFromNameAndBoards(string board1, string board2, string columnname)
        {

            return leaderboards.findColumnIDInBoardsByName(board1, board2, columnname);
        }

        private uint getLbIndexFromName(string p)
        {


            return leaderboards.findBoardIndexByName(p);
        }

        private void exportTestDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            DialogResult dr = fbd.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                ExportGenerator expgen = new ExportGenerator(Missions, Stats, leaderboards);

                expgen.exportTestEntriesToFile(fbd.SelectedPath + "\\StatTestDoc.txt");

            }
        }

        private void mergeDuplicateSPMissionEntriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
         MishStatStar.MissionEntry prime = null;
         MishStatStar.MissionEntry dupe = null;
         bool bDone = false;

         while (!bDone)
         {

             foreach (MishStatStar.MissionEntry me in Missions)
             {
                 foreach (MishStatStar.MissionEntry md in Missions)
                 {
                     if (me != md)
                     {
                         if (me.FlowEnum.CompareTo(md.FlowEnum) == 0)
                         {
                             prime = me;
                             dupe = md;
                             break;
                         }
                     }
                 }

                 if (prime != null && dupe != null)
                 {
                     break;
                 }
             }

             if (prime != null && dupe != null)
             {

                 //copy dupe's entries into prime

                 prime.Absorb(dupe);
                 Missions.Remove(dupe);

                 prime = null;
                 dupe = null;
             }
             else
             {
                 bDone = true;
             }

             

         }
         PopulateMissionList();
        


        }



        public class LeaderboardCollection
        {

            private List<Leaderboard> boards;

            public LeaderboardCollection(System.Collections.ObjectModel.ObservableCollection<Leaderboard> observableCollection)
            {
                boards = new List<Leaderboard>();
                Populate(observableCollection);
            }


            internal List<Leaderboard> GetLeaderboards()
            {
                return boards;
            }

            internal void Populate(System.Collections.ObjectModel.ObservableCollection<Leaderboard> observableCollection)
            {
                boards.Clear();
                foreach (Leaderboard l in observableCollection)
                {
                    boards.Add(l);
                }
            }

            internal uint findBoardIndexByName(string p)
            {
                foreach (Leaderboard b in boards)
                {
                    if (p.CompareTo(b.Name) == 0)
                    {
                        return uint.Parse(b.Id);
                    }
                }
                return 0;
            }

            internal uint findColumnIDInBoardsByName(string board1, string board2, string columnname)
            {
                uint board1result = findBoardIndexByName(board1);
                uint board2result = findBoardIndexByName(board2);
                if (board1result == 0 || board2result == 0)
                {
                    return 0;
                }
                //otherwise look up
                uint colid1 = getColumnFromBoardByNumber(board1result, columnname);
                uint colid2 = getColumnFromBoardByNumber(board2result, columnname);
                if (colid1 != colid2)
                {
                    return 0;
                }
                return colid1;
            }

            private uint getColumnFromBoardByNumber(uint board1result, string columnname)
            {
                foreach (Leaderboard b in boards)
                {
                    if (uint.Parse(b.Id) == board1result)
                    {
                        //return uint.Parse(b.Id);
                        foreach (Column c in b.Columns)
                        {
                            if (c.Name.CompareTo(columnname) == 0)
                            {
                                return uint.Parse(c.Id);
                            }
                        }
                    }
                }
                return 0;
            }
        }

        private void statbindlist_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Select the stat in the statlist when we click on one. Saves a bit of time.
            if (statlist.SelectedItem != statbindlist.SelectedItem)
            {
                statlist.SelectedItem = statbindlist.SelectedItem;
                statlist_SelectedIndexChanged();

            }
        }

        private void differentiatorScoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://rsgediwiki1/wiki/index.php/Mission_Leaderboards_And_Scoring");
        }

        private void statlist_SelectedIndexChanged(object sender, EventArgs e)
        {
            statlist_SelectedIndexChanged();
        }

        private void exportSCDevDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            DialogResult dr = fbd.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                ExportGenerator expgen = new ExportGenerator(Missions, Stats, leaderboards);

                expgen.exportSCDevEntriesToFile(fbd.SelectedPath + "\\SocialClubDevNotes.txt");

            }
        }

        private void exportSCLBDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            DialogResult dr = fbd.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                ExportGenerator expgen = new ExportGenerator(Missions, Stats, leaderboards);

                expgen.exportSCDevLBEntriesToFile(fbd.SelectedPath + "\\SocialClubDevLBs.txt");

            }
        }

        private void exportBindingValidationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            DialogResult dr = fbd.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                ExportGenerator expgen = new ExportGenerator(Missions, Stats, leaderboards);

                expgen.validateStats(fbd.SelectedPath + "\\LeaderboardBindValidation.txt");

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ExportGenerator expgen = new ExportGenerator(Missions, Stats, leaderboards);
            string fileName;
            if (NGmodeBox.Checked)
                fileName = "x:\\gta5\\script\\dev_ng\\singleplayer\\include\\globals\\mission_stat_generated_globals.sch";
            else
                fileName = "x:\\gta5\\script\\dev_network\\singleplayer\\include\\globals\\mission_stat_generated_globals.sch";
            
            expgen.replaceEnumsInHeaderFile(fileName);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ExportGenerator expgen = new ExportGenerator(Missions, Stats, leaderboards);

            expgen.exportStringEntriesToFile("X:\\StatsExport\\ToGoIntoStrings.txt");
            //expgen.exportHeaderFile("X:\\gta5\\script\\dev_network\\singleplayer\\include\\globals\\dlc\\dlcmission_stat_generated_globals_AGT.sch");
            expgen.exportStatFile("X:\\StatsExport\\ToGoIntospStatsSetup.xml");
            expgen.exportImplementationNotes("X:\\StatsExport\\ImplementationNotesForScripters.txt");

            string fileName, fileName2;
            if (NGmodeBox.Checked)
            {
                fileName = "X:\\gta5_dlc\\spPacks\\dlc_agentTrevor\\build\\dev_ng\\common\\data\\spStatsSetup.xml";
                fileName2 = "X:\\gta5_dlc\\spPacks\\dlc_agentTrevor\\assets_ng\\GameText\\American\\americanMission.txt";
            }
            else
            {
                fileName = "X:\\gta5_dlc\\spPacks\\dlc_agentTrevor\\build\\dev\\common\\data\\spStatsSetup.xml";
                fileName2 = "X:\\gta5_dlc\\spPacks\\dlc_agentTrevor\\assets\\GameText\\American\\americanMission.txt";
            }
            //Start File copy windows
            MessageBox.Show("Please copy the new stats from the generated XML to the game XML");
            System.Diagnostics.Process.Start("X:\\StatsExport\\ToGoIntospStatsSetup.xml");
            System.Diagnostics.Process.Start(fileName);
            MessageBox.Show("When done copying, close the XML files and press OK.\nThere is another set of lines to be copied");
            System.Diagnostics.Process.Start("X:\\StatsExport\\ToGoIntoStrings.txt");
            System.Diagnostics.Process.Start(fileName2);
        }

        private void missionUp_Click(object sender, EventArgs e)
        {
            int index = missionlist.SelectedIndex;
            if (index == 0) return;
            MissionEntry temp = Missions[index-1];
            Missions[index - 1] = Missions[index];
            Missions[index] = temp;

            PopulateMissionList();
        }

        private void missionDown_Click(object sender, EventArgs e)
        {
            int index = missionlist.SelectedIndex;
            if (index >= Missions.Count-1) return;
            MissionEntry temp = Missions[index + 1];
            Missions[index + 1] = Missions[index];
            Missions[index] = temp;
         
            PopulateMissionList();
        }

    }

}
