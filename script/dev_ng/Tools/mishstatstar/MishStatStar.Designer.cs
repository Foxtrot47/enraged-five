﻿namespace MissionStatsStar
{
    partial class MishStatStar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.missionDown = new System.Windows.Forms.Button();
            this.missionUp = new System.Windows.Forms.Button();
            this.removeMission = new System.Windows.Forms.Button();
            this.addMission = new System.Windows.Forms.Button();
            this.missionlist = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.isrcmission = new System.Windows.Forms.CheckBox();
            this.unbindstat = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.statbindlist = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.missionflowlblast = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.missionleadboardbest = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.mishflowenum = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportAllToDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToXgta5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportTestDocumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportSCDevDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportSCLBDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportBindingValidationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToAGTHeadersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statsListByToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unnusedStatsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usedInSelectedMissionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearFiltersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wizardsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeStatsEndingInINNOCENTSKILLEDToThatCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeStatsEndingInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeStatsContainingBULLETSFIREDToThatCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bullshitswitch = new System.Windows.Forms.ToolStripMenuItem();
            this.rebindLeadboardIDsAndColIDsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mergeDuplicateSPMissionEntriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.whatIsThisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.differentiatorScoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.findstatboundto = new System.Windows.Forms.Button();
            this.dupstat = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.ingdesc = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.overridecatstring = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.maxscore = new System.Windows.Forms.TextBox();
            this.minscore = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.catnotes = new System.Windows.Forms.Label();
            this.targetvalue = new System.Windows.Forms.TextBox();
            this.basevalue = new System.Windows.Forms.TextBox();
            this.islessthan = new System.Windows.Forms.RadioButton();
            this.notlessthan = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.shortdescription = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.categorylist = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.LBCol = new System.Windows.Forms.TextBox();
            this.lbisdiff = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.maxlegal = new System.Windows.Forms.TextBox();
            this.precedence = new System.Windows.Forms.TextBox();
            this.lbweight = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.minlegal = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.statenumname = new System.Windows.Forms.TextBox();
            this.hiddenonpass = new System.Windows.Forms.CheckBox();
            this.delstat = new System.Windows.Forms.Button();
            this.addstat = new System.Windows.Forms.Button();
            this.statlist = new MissionStatsStar.PreSelectableListBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lbbindcolstat = new System.Windows.Forms.Button();
            this.mishindivbind = new System.Windows.Forms.Button();
            this.mishbestbind = new System.Windows.Forms.Button();
            this.debugcols = new System.Windows.Forms.ListBox();
            this.lbdump = new System.Windows.Forms.ListBox();
            this.exportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.xmlGenButton = new System.Windows.Forms.Button();
            this.NGmodeBox = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.missionDown);
            this.groupBox1.Controls.Add(this.missionUp);
            this.groupBox1.Controls.Add(this.removeMission);
            this.groupBox1.Controls.Add(this.addMission);
            this.groupBox1.Controls.Add(this.missionlist);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(8, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(300, 654);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Missions";
            // 
            // missionDown
            // 
            this.missionDown.Location = new System.Drawing.Point(226, 346);
            this.missionDown.Name = "missionDown";
            this.missionDown.Size = new System.Drawing.Size(68, 23);
            this.missionDown.TabIndex = 5;
            this.missionDown.Text = "Down";
            this.missionDown.UseVisualStyleBackColor = true;
            this.missionDown.Click += new System.EventHandler(this.missionDown_Click);
            // 
            // missionUp
            // 
            this.missionUp.Location = new System.Drawing.Point(156, 346);
            this.missionUp.Name = "missionUp";
            this.missionUp.Size = new System.Drawing.Size(68, 23);
            this.missionUp.TabIndex = 4;
            this.missionUp.Text = "Up";
            this.missionUp.UseVisualStyleBackColor = true;
            this.missionUp.Click += new System.EventHandler(this.missionUp_Click);
            // 
            // removeMission
            // 
            this.removeMission.Location = new System.Drawing.Point(82, 346);
            this.removeMission.Name = "removeMission";
            this.removeMission.Size = new System.Drawing.Size(68, 23);
            this.removeMission.TabIndex = 3;
            this.removeMission.Text = "-";
            this.removeMission.UseVisualStyleBackColor = true;
            this.removeMission.Click += new System.EventHandler(this.removeMission_Click);
            // 
            // addMission
            // 
            this.addMission.Location = new System.Drawing.Point(12, 346);
            this.addMission.Name = "addMission";
            this.addMission.Size = new System.Drawing.Size(68, 23);
            this.addMission.TabIndex = 2;
            this.addMission.Text = "+";
            this.addMission.UseVisualStyleBackColor = true;
            this.addMission.Click += new System.EventHandler(this.addMission_Click);
            // 
            // missionlist
            // 
            this.missionlist.FormattingEnabled = true;
            this.missionlist.Location = new System.Drawing.Point(12, 14);
            this.missionlist.Name = "missionlist";
            this.missionlist.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.missionlist.Size = new System.Drawing.Size(282, 329);
            this.missionlist.TabIndex = 1;
            this.missionlist.SelectedIndexChanged += new System.EventHandler(this.missionlist_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.isrcmission);
            this.groupBox2.Controls.Add(this.unbindstat);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.statbindlist);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.missionflowlblast);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.missionleadboardbest);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.mishflowenum);
            this.groupBox2.Location = new System.Drawing.Point(12, 373);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(276, 275);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Mission Settings";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 41);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(187, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "Leaderboard enums to place scores in";
            // 
            // isrcmission
            // 
            this.isrcmission.AutoSize = true;
            this.isrcmission.Location = new System.Drawing.Point(9, 246);
            this.isrcmission.Name = "isrcmission";
            this.isrcmission.Size = new System.Drawing.Size(129, 17);
            this.isrcmission.TabIndex = 16;
            this.isrcmission.Text = "Random Char Mission";
            this.isrcmission.UseVisualStyleBackColor = true;
            this.isrcmission.CheckedChanged += new System.EventHandler(this.isrcmission_CheckedChanged);
            // 
            // unbindstat
            // 
            this.unbindstat.Location = new System.Drawing.Point(144, 245);
            this.unbindstat.Name = "unbindstat";
            this.unbindstat.Size = new System.Drawing.Size(20, 19);
            this.unbindstat.TabIndex = 13;
            this.unbindstat.Text = "-";
            this.unbindstat.UseVisualStyleBackColor = true;
            this.unbindstat.Click += new System.EventHandler(this.unbindstat_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(209, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Associated Stats - Drag to this list to assign";
            // 
            // statbindlist
            // 
            this.statbindlist.AllowDrop = true;
            this.statbindlist.FormattingEnabled = true;
            this.statbindlist.Location = new System.Drawing.Point(6, 124);
            this.statbindlist.Name = "statbindlist";
            this.statbindlist.Size = new System.Drawing.Size(264, 108);
            this.statbindlist.TabIndex = 6;
            this.statbindlist.SelectedIndexChanged += new System.EventHandler(this.statbindlist_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Agg";
            // 
            // missionflowlblast
            // 
            this.missionflowlblast.Enabled = false;
            this.missionflowlblast.Location = new System.Drawing.Point(42, 87);
            this.missionflowlblast.Name = "missionflowlblast";
            this.missionflowlblast.Size = new System.Drawing.Size(228, 20);
            this.missionflowlblast.TabIndex = 4;
            this.missionflowlblast.TextChanged += new System.EventHandler(this.missionflowlblast_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Best";
            // 
            // missionleadboardbest
            // 
            this.missionleadboardbest.Enabled = false;
            this.missionleadboardbest.Location = new System.Drawing.Point(41, 61);
            this.missionleadboardbest.Name = "missionleadboardbest";
            this.missionleadboardbest.Size = new System.Drawing.Size(230, 20);
            this.missionleadboardbest.TabIndex = 2;
            this.missionleadboardbest.TextChanged += new System.EventHandler(this.missionleadboardbest_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mission Flow Enum";
            // 
            // mishflowenum
            // 
            this.mishflowenum.Location = new System.Drawing.Point(101, 13);
            this.mishflowenum.Name = "mishflowenum";
            this.mishflowenum.Size = new System.Drawing.Size(169, 20);
            this.mishflowenum.TabIndex = 0;
            this.mishflowenum.Click += new System.EventHandler(this.mishflowenum_Click);
            this.mishflowenum.TextChanged += new System.EventHandler(this.mishflowenum_TextChanged);
            this.mishflowenum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mishflowenum_Enter);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.exportToolStripMenuItem,
            this.filterToolStripMenuItem,
            this.wizardsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1268, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadSettingsToolStripMenuItem,
            this.saveSettingsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadSettingsToolStripMenuItem
            // 
            this.loadSettingsToolStripMenuItem.Name = "loadSettingsToolStripMenuItem";
            this.loadSettingsToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.loadSettingsToolStripMenuItem.Text = "Load Settings";
            this.loadSettingsToolStripMenuItem.Click += new System.EventHandler(this.loadSettingsToolStripMenuItem_Click);
            // 
            // saveSettingsToolStripMenuItem
            // 
            this.saveSettingsToolStripMenuItem.Name = "saveSettingsToolStripMenuItem";
            this.saveSettingsToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.saveSettingsToolStripMenuItem.Text = "Save Settings";
            this.saveSettingsToolStripMenuItem.Click += new System.EventHandler(this.saveSettingsToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportAllToDirectoryToolStripMenuItem,
            this.exportToXgta5ToolStripMenuItem,
            this.exportTestDocumentToolStripMenuItem,
            this.exportSCDevDataToolStripMenuItem,
            this.exportSCLBDataToolStripMenuItem,
            this.exportBindingValidationToolStripMenuItem,
            this.exportToAGTHeadersToolStripMenuItem});
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.exportToolStripMenuItem.Text = "Export";
            // 
            // exportAllToDirectoryToolStripMenuItem
            // 
            this.exportAllToDirectoryToolStripMenuItem.Name = "exportAllToDirectoryToolStripMenuItem";
            this.exportAllToDirectoryToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.exportAllToDirectoryToolStripMenuItem.Text = "Export to Folder";
            this.exportAllToDirectoryToolStripMenuItem.Click += new System.EventHandler(this.exportAllToDirectoryToolStripMenuItem_Click);
            // 
            // exportToXgta5ToolStripMenuItem
            // 
            this.exportToXgta5ToolStripMenuItem.Enabled = false;
            this.exportToXgta5ToolStripMenuItem.Name = "exportToXgta5ToolStripMenuItem";
            this.exportToXgta5ToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.exportToXgta5ToolStripMenuItem.Text = "Export to X:\\gta5";
            this.exportToXgta5ToolStripMenuItem.Click += new System.EventHandler(this.exportToXgta5ToolStripMenuItem_Click);
            // 
            // exportTestDocumentToolStripMenuItem
            // 
            this.exportTestDocumentToolStripMenuItem.Name = "exportTestDocumentToolStripMenuItem";
            this.exportTestDocumentToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.exportTestDocumentToolStripMenuItem.Text = "Export test document";
            this.exportTestDocumentToolStripMenuItem.Click += new System.EventHandler(this.exportTestDocumentToolStripMenuItem_Click);
            // 
            // exportSCDevDataToolStripMenuItem
            // 
            this.exportSCDevDataToolStripMenuItem.Name = "exportSCDevDataToolStripMenuItem";
            this.exportSCDevDataToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.exportSCDevDataToolStripMenuItem.Text = "Export SC Mission data";
            this.exportSCDevDataToolStripMenuItem.Click += new System.EventHandler(this.exportSCDevDataToolStripMenuItem_Click);
            // 
            // exportSCLBDataToolStripMenuItem
            // 
            this.exportSCLBDataToolStripMenuItem.Name = "exportSCLBDataToolStripMenuItem";
            this.exportSCLBDataToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.exportSCLBDataToolStripMenuItem.Text = "Export SC Leaderboard data";
            this.exportSCLBDataToolStripMenuItem.Click += new System.EventHandler(this.exportSCLBDataToolStripMenuItem_Click);
            // 
            // exportBindingValidationToolStripMenuItem
            // 
            this.exportBindingValidationToolStripMenuItem.Name = "exportBindingValidationToolStripMenuItem";
            this.exportBindingValidationToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.exportBindingValidationToolStripMenuItem.Text = "Export Binding Validation";
            this.exportBindingValidationToolStripMenuItem.Click += new System.EventHandler(this.exportBindingValidationToolStripMenuItem_Click);
            // 
            // exportToAGTHeadersToolStripMenuItem
            // 
            this.exportToAGTHeadersToolStripMenuItem.Name = "exportToAGTHeadersToolStripMenuItem";
            this.exportToAGTHeadersToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.exportToAGTHeadersToolStripMenuItem.Text = "Export to AGT Headers";
            this.exportToAGTHeadersToolStripMenuItem.Click += new System.EventHandler(this.exportToAGTHeadersToolStripMenuItem_Click);
            // 
            // filterToolStripMenuItem
            // 
            this.filterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statsListByToolStripMenuItem,
            this.clearFiltersToolStripMenuItem});
            this.filterToolStripMenuItem.Enabled = false;
            this.filterToolStripMenuItem.Name = "filterToolStripMenuItem";
            this.filterToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.filterToolStripMenuItem.Text = "Filter";
            // 
            // statsListByToolStripMenuItem
            // 
            this.statsListByToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unnusedStatsToolStripMenuItem,
            this.usedInSelectedMissionToolStripMenuItem});
            this.statsListByToolStripMenuItem.Name = "statsListByToolStripMenuItem";
            this.statsListByToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.statsListByToolStripMenuItem.Text = "...Stats List By...";
            // 
            // unnusedStatsToolStripMenuItem
            // 
            this.unnusedStatsToolStripMenuItem.Name = "unnusedStatsToolStripMenuItem";
            this.unnusedStatsToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.unnusedStatsToolStripMenuItem.Text = "Unnused stats";
            // 
            // usedInSelectedMissionToolStripMenuItem
            // 
            this.usedInSelectedMissionToolStripMenuItem.Name = "usedInSelectedMissionToolStripMenuItem";
            this.usedInSelectedMissionToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.usedInSelectedMissionToolStripMenuItem.Text = "Used in selected mission";
            // 
            // clearFiltersToolStripMenuItem
            // 
            this.clearFiltersToolStripMenuItem.Name = "clearFiltersToolStripMenuItem";
            this.clearFiltersToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.clearFiltersToolStripMenuItem.Text = "Clear Filters";
            // 
            // wizardsToolStripMenuItem
            // 
            this.wizardsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeStatsEndingInINNOCENTSKILLEDToThatCategoryToolStripMenuItem,
            this.changeStatsEndingInToolStripMenuItem,
            this.changeStatsContainingBULLETSFIREDToThatCategoryToolStripMenuItem,
            this.bullshitswitch,
            this.rebindLeadboardIDsAndColIDsToolStripMenuItem,
            this.mergeDuplicateSPMissionEntriesToolStripMenuItem});
            this.wizardsToolStripMenuItem.Name = "wizardsToolStripMenuItem";
            this.wizardsToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.wizardsToolStripMenuItem.Text = "Wizards";
            // 
            // changeStatsEndingInINNOCENTSKILLEDToThatCategoryToolStripMenuItem
            // 
            this.changeStatsEndingInINNOCENTSKILLEDToThatCategoryToolStripMenuItem.Name = "changeStatsEndingInINNOCENTSKILLEDToThatCategoryToolStripMenuItem";
            this.changeStatsEndingInINNOCENTSKILLEDToThatCategoryToolStripMenuItem.Size = new System.Drawing.Size(432, 22);
            this.changeStatsEndingInINNOCENTSKILLEDToThatCategoryToolStripMenuItem.Text = "Change stats containing \"_INNOCENTS_KILLED\" to that category";
            this.changeStatsEndingInINNOCENTSKILLEDToThatCategoryToolStripMenuItem.Click += new System.EventHandler(this.changeStatsEndingInINNOCENTSKILLEDToThatCategoryToolStripMenuItem_Click);
            // 
            // changeStatsEndingInToolStripMenuItem
            // 
            this.changeStatsEndingInToolStripMenuItem.Name = "changeStatsEndingInToolStripMenuItem";
            this.changeStatsEndingInToolStripMenuItem.Size = new System.Drawing.Size(432, 22);
            this.changeStatsEndingInToolStripMenuItem.Text = "Change stats containing \"_SPECIAL_ABILITY_TIME\" to that category.";
            this.changeStatsEndingInToolStripMenuItem.Click += new System.EventHandler(this.changeStatsEndingInToolStripMenuItem_Click);
            // 
            // changeStatsContainingBULLETSFIREDToThatCategoryToolStripMenuItem
            // 
            this.changeStatsContainingBULLETSFIREDToThatCategoryToolStripMenuItem.Name = "changeStatsContainingBULLETSFIREDToThatCategoryToolStripMenuItem";
            this.changeStatsContainingBULLETSFIREDToThatCategoryToolStripMenuItem.Size = new System.Drawing.Size(432, 22);
            this.changeStatsContainingBULLETSFIREDToThatCategoryToolStripMenuItem.Text = "Change stats containing \"_BULLETS_FIRED\" to that category";
            this.changeStatsContainingBULLETSFIREDToThatCategoryToolStripMenuItem.Click += new System.EventHandler(this.changeStatsContainingBULLETSFIREDToThatCategoryToolStripMenuItem_Click);
            // 
            // bullshitswitch
            // 
            this.bullshitswitch.Name = "bullshitswitch";
            this.bullshitswitch.Size = new System.Drawing.Size(432, 22);
            this.bullshitswitch.Text = "Change mission indivs from \"_AGGREGATE\" to \"_INDIV_RECORDS\"";
            this.bullshitswitch.Click += new System.EventHandler(this.bullshitswitch_Click);
            // 
            // rebindLeadboardIDsAndColIDsToolStripMenuItem
            // 
            this.rebindLeadboardIDsAndColIDsToolStripMenuItem.Name = "rebindLeadboardIDsAndColIDsToolStripMenuItem";
            this.rebindLeadboardIDsAndColIDsToolStripMenuItem.Size = new System.Drawing.Size(432, 22);
            this.rebindLeadboardIDsAndColIDsToolStripMenuItem.Text = "Rebind leaderboard IDs and col IDs";
            this.rebindLeadboardIDsAndColIDsToolStripMenuItem.Click += new System.EventHandler(this.rebindLeadboardIDsAndColIDsToolStripMenuItem_Click);
            // 
            // mergeDuplicateSPMissionEntriesToolStripMenuItem
            // 
            this.mergeDuplicateSPMissionEntriesToolStripMenuItem.Name = "mergeDuplicateSPMissionEntriesToolStripMenuItem";
            this.mergeDuplicateSPMissionEntriesToolStripMenuItem.Size = new System.Drawing.Size(432, 22);
            this.mergeDuplicateSPMissionEntriesToolStripMenuItem.Text = "Merge duplicate SP mission entries";
            this.mergeDuplicateSPMissionEntriesToolStripMenuItem.Click += new System.EventHandler(this.mergeDuplicateSPMissionEntriesToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.whatIsThisToolStripMenuItem,
            this.differentiatorScoresToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // whatIsThisToolStripMenuItem
            // 
            this.whatIsThisToolStripMenuItem.Name = "whatIsThisToolStripMenuItem";
            this.whatIsThisToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.whatIsThisToolStripMenuItem.Text = "What is this?";
            this.whatIsThisToolStripMenuItem.Click += new System.EventHandler(this.whatIsThisToolStripMenuItem_Click);
            // 
            // differentiatorScoresToolStripMenuItem
            // 
            this.differentiatorScoresToolStripMenuItem.Name = "differentiatorScoresToolStripMenuItem";
            this.differentiatorScoresToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.differentiatorScoresToolStripMenuItem.Text = "Mission Score Details";
            this.differentiatorScoresToolStripMenuItem.Click += new System.EventHandler(this.differentiatorScoresToolStripMenuItem_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.findstatboundto);
            this.groupBox3.Controls.Add(this.dupstat);
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.delstat);
            this.groupBox3.Controls.Add(this.addstat);
            this.groupBox3.Controls.Add(this.statlist);
            this.groupBox3.Location = new System.Drawing.Point(314, 30);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(573, 654);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Script Mission Stats";
            // 
            // findstatboundto
            // 
            this.findstatboundto.Location = new System.Drawing.Point(6, 625);
            this.findstatboundto.Name = "findstatboundto";
            this.findstatboundto.Size = new System.Drawing.Size(122, 23);
            this.findstatboundto.TabIndex = 19;
            this.findstatboundto.Text = "<- Select bound to ";
            this.findstatboundto.UseVisualStyleBackColor = true;
            this.findstatboundto.Click += new System.EventHandler(this.findstatboundto_Click);
            // 
            // dupstat
            // 
            this.dupstat.Location = new System.Drawing.Point(169, 625);
            this.dupstat.Name = "dupstat";
            this.dupstat.Size = new System.Drawing.Size(40, 23);
            this.dupstat.TabIndex = 12;
            this.dupstat.Text = "dupe";
            this.dupstat.UseVisualStyleBackColor = true;
            this.dupstat.Click += new System.EventHandler(this.dupstat_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.ingdesc);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.overridecatstring);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Controls.Add(this.catnotes);
            this.groupBox4.Controls.Add(this.targetvalue);
            this.groupBox4.Controls.Add(this.basevalue);
            this.groupBox4.Controls.Add(this.islessthan);
            this.groupBox4.Controls.Add(this.notlessthan);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.shortdescription);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.categorylist);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.statenumname);
            this.groupBox4.Controls.Add(this.hiddenonpass);
            this.groupBox4.Location = new System.Drawing.Point(244, 14);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(320, 634);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Stat Settings";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(16, 277);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(96, 13);
            this.label22.TabIndex = 25;
            this.label22.Text = "Ingame description";
            // 
            // ingdesc
            // 
            this.ingdesc.Location = new System.Drawing.Point(14, 293);
            this.ingdesc.Multiline = true;
            this.ingdesc.Name = "ingdesc";
            this.ingdesc.Size = new System.Drawing.Size(292, 48);
            this.ingdesc.TabIndex = 24;
            this.ingdesc.TextChanged += new System.EventHandler(this.ingdesc_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(19, 373);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(104, 13);
            this.label16.TabIndex = 23;
            this.label16.Text = "Override name string";
            // 
            // overridecatstring
            // 
            this.overridecatstring.Location = new System.Drawing.Point(142, 373);
            this.overridecatstring.Name = "overridecatstring";
            this.overridecatstring.Size = new System.Drawing.Size(163, 20);
            this.overridecatstring.TabIndex = 22;
            this.overridecatstring.TextChanged += new System.EventHandler(this.overridecatstring_TextChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.maxscore);
            this.groupBox6.Controls.Add(this.minscore);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Location = new System.Drawing.Point(6, 399);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(308, 82);
            this.groupBox6.TabIndex = 21;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Percentage Score Ranging Values";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(213, 37);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(27, 13);
            this.label14.TabIndex = 4;
            this.label14.Text = "Max";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(44, 37);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(24, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "Min";
            // 
            // maxscore
            // 
            this.maxscore.Location = new System.Drawing.Point(181, 53);
            this.maxscore.Name = "maxscore";
            this.maxscore.Size = new System.Drawing.Size(100, 20);
            this.maxscore.TabIndex = 2;
            this.maxscore.TextChanged += new System.EventHandler(this.maxscore_TextChanged);
            // 
            // minscore
            // 
            this.minscore.Location = new System.Drawing.Point(17, 53);
            this.minscore.Name = "minscore";
            this.minscore.Size = new System.Drawing.Size(100, 20);
            this.minscore.TabIndex = 1;
            this.minscore.TextChanged += new System.EventHandler(this.minscore_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(14, 21);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(254, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Set max and min integers to generate % contrib caps";
            // 
            // catnotes
            // 
            this.catnotes.AutoSize = true;
            this.catnotes.Location = new System.Drawing.Point(10, 67);
            this.catnotes.Name = "catnotes";
            this.catnotes.Size = new System.Drawing.Size(162, 13);
            this.catnotes.TabIndex = 20;
            this.catnotes.Text = "Pick category to see value notes";
            // 
            // targetvalue
            // 
            this.targetvalue.Location = new System.Drawing.Point(237, 88);
            this.targetvalue.Name = "targetvalue";
            this.targetvalue.Size = new System.Drawing.Size(74, 20);
            this.targetvalue.TabIndex = 19;
            this.targetvalue.TextChanged += new System.EventHandler(this.targetvalue_TextChanged);
            // 
            // basevalue
            // 
            this.basevalue.Location = new System.Drawing.Point(80, 88);
            this.basevalue.Name = "basevalue";
            this.basevalue.Size = new System.Drawing.Size(74, 20);
            this.basevalue.TabIndex = 18;
            this.basevalue.TextChanged += new System.EventHandler(this.basevalue_TextChanged);
            // 
            // islessthan
            // 
            this.islessthan.AutoSize = true;
            this.islessthan.Location = new System.Drawing.Point(13, 131);
            this.islessthan.Name = "islessthan";
            this.islessthan.Size = new System.Drawing.Size(161, 17);
            this.islessthan.TabIndex = 17;
            this.islessthan.TabStop = true;
            this.islessthan.Text = "pass is when base is < target";
            this.islessthan.UseVisualStyleBackColor = true;
            this.islessthan.CheckedChanged += new System.EventHandler(this.islessthan_CheckedChanged);
            // 
            // notlessthan
            // 
            this.notlessthan.AutoSize = true;
            this.notlessthan.Location = new System.Drawing.Point(13, 108);
            this.notlessthan.Name = "notlessthan";
            this.notlessthan.Size = new System.Drawing.Size(167, 17);
            this.notlessthan.TabIndex = 16;
            this.notlessthan.TabStop = true;
            this.notlessthan.Text = "pass is when base is >= target";
            this.notlessthan.UseVisualStyleBackColor = true;
            this.notlessthan.CheckedChanged += new System.EventHandler(this.notlessthan_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(160, 91);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Target Integer";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 91);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Base Integer";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 195);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(266, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Short Internal Description - used for implemention notes";
            // 
            // shortdescription
            // 
            this.shortdescription.Location = new System.Drawing.Point(13, 211);
            this.shortdescription.Multiline = true;
            this.shortdescription.Name = "shortdescription";
            this.shortdescription.Size = new System.Drawing.Size(292, 48);
            this.shortdescription.TabIndex = 12;
            this.shortdescription.TextChanged += new System.EventHandler(this.shortdescription_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Category";
            // 
            // categorylist
            // 
            this.categorylist.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.categorylist.FormattingEnabled = true;
            this.categorylist.Location = new System.Drawing.Point(142, 43);
            this.categorylist.Name = "categorylist";
            this.categorylist.Size = new System.Drawing.Size(169, 21);
            this.categorylist.TabIndex = 9;
            this.categorylist.SelectedIndexChanged += new System.EventHandler(this.categorylist_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Internal Enum Name";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.LBCol);
            this.groupBox5.Controls.Add(this.lbisdiff);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.maxlegal);
            this.groupBox5.Controls.Add(this.precedence);
            this.groupBox5.Controls.Add(this.lbweight);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.minlegal);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Location = new System.Drawing.Point(6, 487);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(308, 141);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Leaderboard Score Generation Settings for this Stat";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 119);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 13);
            this.label15.TabIndex = 26;
            this.label15.Text = "LB Column";
            // 
            // LBCol
            // 
            this.LBCol.Enabled = false;
            this.LBCol.Location = new System.Drawing.Point(99, 116);
            this.LBCol.Name = "LBCol";
            this.LBCol.Size = new System.Drawing.Size(169, 20);
            this.LBCol.TabIndex = 25;
            // 
            // lbisdiff
            // 
            this.lbisdiff.AutoSize = true;
            this.lbisdiff.Location = new System.Drawing.Point(13, 12);
            this.lbisdiff.Name = "lbisdiff";
            this.lbisdiff.Size = new System.Drawing.Size(95, 17);
            this.lbisdiff.TabIndex = 24;
            this.lbisdiff.Text = "Is differentiator";
            this.lbisdiff.UseVisualStyleBackColor = true;
            this.lbisdiff.CheckedChanged += new System.EventHandler(this.lbisdiff_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Precedence";
            // 
            // maxlegal
            // 
            this.maxlegal.Location = new System.Drawing.Point(101, 71);
            this.maxlegal.Name = "maxlegal";
            this.maxlegal.Size = new System.Drawing.Size(169, 20);
            this.maxlegal.TabIndex = 21;
            this.maxlegal.TextChanged += new System.EventHandler(this.maxlegal_TextChanged);
            // 
            // precedence
            // 
            this.precedence.Location = new System.Drawing.Point(100, 92);
            this.precedence.Name = "precedence";
            this.precedence.Size = new System.Drawing.Size(169, 20);
            this.precedence.TabIndex = 23;
            this.precedence.TextChanged += new System.EventHandler(this.precedence_TextChanged);
            // 
            // lbweight
            // 
            this.lbweight.Location = new System.Drawing.Point(101, 29);
            this.lbweight.Name = "lbweight";
            this.lbweight.Size = new System.Drawing.Size(169, 20);
            this.lbweight.TabIndex = 17;
            this.lbweight.TextChanged += new System.EventHandler(this.lbweight_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 74);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 13);
            this.label17.TabIndex = 22;
            this.label17.Text = "Max legal";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 32);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 13);
            this.label19.TabIndex = 18;
            this.label19.Text = "Weight";
            // 
            // minlegal
            // 
            this.minlegal.Location = new System.Drawing.Point(101, 50);
            this.minlegal.Name = "minlegal";
            this.minlegal.Size = new System.Drawing.Size(169, 20);
            this.minlegal.TabIndex = 19;
            this.minlegal.TextChanged += new System.EventHandler(this.minlegal_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 53);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 13);
            this.label18.TabIndex = 20;
            this.label18.Text = "Min legal";
            // 
            // statenumname
            // 
            this.statenumname.Location = new System.Drawing.Point(142, 17);
            this.statenumname.Name = "statenumname";
            this.statenumname.Size = new System.Drawing.Size(169, 20);
            this.statenumname.TabIndex = 9;
            this.statenumname.TextChanged += new System.EventHandler(this.statenumname_TextChanged);
            // 
            // hiddenonpass
            // 
            this.hiddenonpass.AutoSize = true;
            this.hiddenonpass.Location = new System.Drawing.Point(13, 154);
            this.hiddenonpass.Name = "hiddenonpass";
            this.hiddenonpass.Size = new System.Drawing.Size(135, 17);
            this.hiddenonpass.TabIndex = 0;
            this.hiddenonpass.Text = "Hidden on pass screen";
            this.hiddenonpass.UseVisualStyleBackColor = true;
            this.hiddenonpass.CheckedChanged += new System.EventHandler(this.hiddenonpass_CheckedChanged);
            // 
            // delstat
            // 
            this.delstat.Location = new System.Drawing.Point(215, 625);
            this.delstat.Name = "delstat";
            this.delstat.Size = new System.Drawing.Size(23, 23);
            this.delstat.TabIndex = 10;
            this.delstat.Text = "-";
            this.delstat.UseVisualStyleBackColor = true;
            this.delstat.Click += new System.EventHandler(this.delstat_Click);
            // 
            // addstat
            // 
            this.addstat.Location = new System.Drawing.Point(139, 625);
            this.addstat.Name = "addstat";
            this.addstat.Size = new System.Drawing.Size(24, 23);
            this.addstat.TabIndex = 9;
            this.addstat.Text = "+";
            this.addstat.UseVisualStyleBackColor = true;
            this.addstat.Click += new System.EventHandler(this.addstat_Click);
            // 
            // statlist
            // 
            this.statlist.FormattingEnabled = true;
            this.statlist.Location = new System.Drawing.Point(6, 19);
            this.statlist.Name = "statlist";
            this.statlist.Size = new System.Drawing.Size(232, 602);
            this.statlist.TabIndex = 8;
            this.statlist.SelectedIndexChanged += new System.EventHandler(this.statlist_SelectedIndexChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.label20);
            this.groupBox7.Controls.Add(this.lbbindcolstat);
            this.groupBox7.Controls.Add(this.mishindivbind);
            this.groupBox7.Controls.Add(this.mishbestbind);
            this.groupBox7.Controls.Add(this.debugcols);
            this.groupBox7.Controls.Add(this.lbdump);
            this.groupBox7.Location = new System.Drawing.Point(893, 30);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(367, 654);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Leaderboard Bindings";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(201, 14);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 13);
            this.label21.TabIndex = 6;
            this.label21.Text = "Columns";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 13);
            this.label20.TabIndex = 5;
            this.label20.Text = "Leaderboards";
            // 
            // lbbindcolstat
            // 
            this.lbbindcolstat.Location = new System.Drawing.Point(204, 625);
            this.lbbindcolstat.Name = "lbbindcolstat";
            this.lbbindcolstat.Size = new System.Drawing.Size(157, 23);
            this.lbbindcolstat.TabIndex = 4;
            this.lbbindcolstat.Text = "<- Bind to selected stat";
            this.lbbindcolstat.UseVisualStyleBackColor = true;
            this.lbbindcolstat.Click += new System.EventHandler(this.lbbindcolstat_Click);
            // 
            // mishindivbind
            // 
            this.mishindivbind.Location = new System.Drawing.Point(6, 625);
            this.mishindivbind.Name = "mishindivbind";
            this.mishindivbind.Size = new System.Drawing.Size(192, 23);
            this.mishindivbind.TabIndex = 3;
            this.mishindivbind.Text = "<- Bind to selected Mission Agg";
            this.mishindivbind.UseVisualStyleBackColor = true;
            this.mishindivbind.Click += new System.EventHandler(this.mishindivbind_Click);
            // 
            // mishbestbind
            // 
            this.mishbestbind.Location = new System.Drawing.Point(6, 598);
            this.mishbestbind.Name = "mishbestbind";
            this.mishbestbind.Size = new System.Drawing.Size(192, 23);
            this.mishbestbind.TabIndex = 2;
            this.mishbestbind.Text = "<- Bind to selected Mission Best";
            this.mishbestbind.UseVisualStyleBackColor = true;
            this.mishbestbind.Click += new System.EventHandler(this.mishbestbind_Click);
            // 
            // debugcols
            // 
            this.debugcols.FormattingEnabled = true;
            this.debugcols.HorizontalScrollbar = true;
            this.debugcols.Location = new System.Drawing.Point(204, 32);
            this.debugcols.Name = "debugcols";
            this.debugcols.Size = new System.Drawing.Size(157, 589);
            this.debugcols.TabIndex = 1;
            this.debugcols.SelectedIndexChanged += new System.EventHandler(this.debugcols_SelectedIndexChanged);
            // 
            // lbdump
            // 
            this.lbdump.FormattingEnabled = true;
            this.lbdump.HorizontalScrollbar = true;
            this.lbdump.Location = new System.Drawing.Point(6, 32);
            this.lbdump.Name = "lbdump";
            this.lbdump.Size = new System.Drawing.Size(192, 563);
            this.lbdump.TabIndex = 0;
            this.lbdump.SelectedIndexChanged += new System.EventHandler(this.lbdump_SelectedIndexChanged);
            // 
            // exportToolStripMenuItem1
            // 
            this.exportToolStripMenuItem1.Name = "exportToolStripMenuItem1";
            this.exportToolStripMenuItem1.Size = new System.Drawing.Size(32, 19);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(249, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(91, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Replace Enums";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // xmlGenButton
            // 
            this.xmlGenButton.Location = new System.Drawing.Point(347, 0);
            this.xmlGenButton.Name = "xmlGenButton";
            this.xmlGenButton.Size = new System.Drawing.Size(95, 23);
            this.xmlGenButton.TabIndex = 5;
            this.xmlGenButton.Text = "Generate XML";
            this.xmlGenButton.UseVisualStyleBackColor = true;
            this.xmlGenButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // NGmodeBox
            // 
            this.NGmodeBox.AutoSize = true;
            this.NGmodeBox.Location = new System.Drawing.Point(448, 4);
            this.NGmodeBox.Name = "NGmodeBox";
            this.NGmodeBox.Size = new System.Drawing.Size(42, 17);
            this.NGmodeBox.TabIndex = 6;
            this.NGmodeBox.Text = "NG";
            this.NGmodeBox.UseVisualStyleBackColor = true;
            // 
            // MishStatStar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1268, 687);
            this.Controls.Add(this.NGmodeBox);
            this.Controls.Add(this.xmlGenButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MishStatStar";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Mission Stat Star";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }



        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox missionflowlblast;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox missionleadboardbest;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox mishflowenum;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportAllToDirectoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToXgta5ToolStripMenuItem;
        private System.Windows.Forms.Button removeMission;
        private System.Windows.Forms.Button addMission;
        private System.Windows.Forms.ListBox missionlist;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox statbindlist;
        private System.Windows.Forms.GroupBox groupBox3;
        private PreSelectableListBox statlist;
        private System.Windows.Forms.Button delstat;
        private System.Windows.Forms.Button addstat;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button dupstat;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox hiddenonpass;
        private System.Windows.Forms.ToolStripMenuItem filterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statsListByToolStripMenuItem;
        private System.Windows.Forms.TextBox targetvalue;
        private System.Windows.Forms.TextBox basevalue;
        private System.Windows.Forms.RadioButton islessthan;
        private System.Windows.Forms.RadioButton notlessthan;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox shortdescription;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox categorylist;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox statenumname;
        private System.Windows.Forms.ToolStripMenuItem unnusedStatsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usedInSelectedMissionToolStripMenuItem;
        private System.Windows.Forms.Label catnotes;
        private System.Windows.Forms.Button unbindstat;
        private System.Windows.Forms.ToolStripMenuItem clearFiltersToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox maxscore;
        private System.Windows.Forms.TextBox minscore;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox overridecatstring;
        private System.Windows.Forms.CheckBox isrcmission;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox maxlegal;
        private System.Windows.Forms.TextBox precedence;
        private System.Windows.Forms.TextBox lbweight;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox minlegal;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem whatIsThisToolStripMenuItem;
        private System.Windows.Forms.CheckBox lbisdiff;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ListBox lbdump;
        private System.Windows.Forms.ListBox debugcols;
        private System.Windows.Forms.Button mishindivbind;
        private System.Windows.Forms.Button mishbestbind;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox LBCol;
        private System.Windows.Forms.Button lbbindcolstat;
        private System.Windows.Forms.Button findstatboundto;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ToolStripMenuItem wizardsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeStatsEndingInINNOCENTSKILLEDToThatCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeStatsEndingInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeStatsContainingBULLETSFIREDToThatCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bullshitswitch;
        private System.Windows.Forms.ToolStripMenuItem rebindLeadboardIDsAndColIDsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportTestDocumentToolStripMenuItem;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox ingdesc;
        private System.Windows.Forms.ToolStripMenuItem mergeDuplicateSPMissionEntriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem differentiatorScoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportSCDevDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportSCLBDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportBindingValidationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToAGTHeadersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button xmlGenButton;
        private System.Windows.Forms.Button missionDown;
        private System.Windows.Forms.Button missionUp;
        private System.Windows.Forms.CheckBox NGmodeBox;
    }
}

