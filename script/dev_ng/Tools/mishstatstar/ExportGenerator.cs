﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MissionStatsStar
{
    class ExportGenerator
    {
        //data in
        private List<MishStatStar.MissionEntry> Missions;
        private List<MishStatStar.MissionStat> Stats;
        private MissionStatsStar.MishStatStar.LeaderboardCollection Leaderboards;

        //generated data
        private List<string> ExportStrings;
        private List<int> ExportStringIndices;
        private Dictionary<MishStatStar.MissionStat, String> DescLookups;
        private Dictionary<String, String> DescNames;
        

        private List<bool> MissionBindsConsistant;
        int MissionBindsConsistantCount = 0;
        int MissionBindsConsistantCountRC = 0;


        public ExportGenerator(List<MishStatStar.MissionEntry> Missions, List<MishStatStar.MissionStat> Stats, MissionStatsStar.MishStatStar.LeaderboardCollection currentLC)
        {
            this.Missions = Missions;
            this.Stats = Stats;
            this.Leaderboards = currentLC;

            ExportStrings = new List<string>();
            ExportStringIndices = new List<int>();
            MissionBindsConsistant = new List<bool>();
            //prepare the data prerequisites
            //find duplicate exportable strings and build a binding list that matches the stat list
                //for each string that is to go into game compare it against the contents of the list and add
                //to the export list
                    //first add the category strings
                    //for each stat check if the override name string is != "" 
                        //try to set indice for this stat
                        //if not try to add it

            /*
            ExportStrings.Add("Time");
            ExportStrings.Add("Action Cam Use");
            ExportStrings.Add("Max Speed");
            ExportStrings.Add("Damage");
            ExportStrings.Add("Headshots");
            ExportStrings.Add("Accuracy");
            ExportStrings.Add("Damage Caused");
            */
            List<String> Descriptions = new List<string>();
            DescLookups = new Dictionary<MishStatStar.MissionStat,String>();
            DescNames = new Dictionary<String,String>();
            foreach(MishStatStar.MissionStat ms in Stats)
            {
                String tostore = ms.igDesc + "";
                bool found = false;
                foreach(String s in Descriptions){
                    if(tostore.CompareTo(s) == 0)
                    {
                        tostore = s;
                        found = true;
                        break;
                    }
                }
                if(!found){
                    Descriptions.Add(tostore);
                }
                if (tostore.Length > 3)
                {
                    DescLookups.Add(ms, tostore);
                }
                
            }

            int indexer = 0;
            foreach (String s in DescLookups.Values)
            {
                //DescLookups[ms] = "[MISHSTD" + indexer + "]" + "\n\n" + DescLookups[ms] + "\n\n";
                String str = "MISHSTD" + indexer + "";
                if (!DescNames.Keys.Contains(s))
                {
                    DescNames.Add(s, str);
                }
                
                ++indexer;
            }

            foreach (MishStatStar.MissionStat ms in Stats)
            {

                bool isUsed = false;
                foreach (MishStatStar.MissionEntry me in Missions)
                {
                    if (me.MyStats.Count > 0)
                    {
                        isUsed = me.MyStats.Contains(ms);
                        if (isUsed)
                        {
                            break;
                        }
                    }
                }


                if (!ms.Hidden && isUsed)
                {
                    string stringtoplace = "STAT WITH UNSET STRING AND CATEGORY IN THE EXPORT LIST: " + ms.enumname + "!";




                    if (ms.overrideString.CompareTo("") == 0)
                    {
                        //its using a category entry
                        switch (ms.Category)
                        {
                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_TOTALTIME:
                                stringtoplace = "Time";
                                break;
                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_ACTION_CAM_USE:
                                stringtoplace = "Action Cam Use";
                                break;
                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD:
                                stringtoplace = "Max Speed";
                                break;
                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD:
                                stringtoplace = "Damage";
                                break;
                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_HEADSHOTS:
                                stringtoplace = "Headshots";
                                break;
                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_ACCURACY:
                                stringtoplace = "Accuracy";
                                break;
                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_FINANCE_DIRECT:
                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_FINANCE_TABLE:
                                stringtoplace = "Damage Caused";
                                break;
                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_BULLETS_FIRED:
                                stringtoplace = "Bullets Fired";
                                break;
                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_INNOCENTS_KILLED:
                                stringtoplace = "Innocents Killed";
                                break;
                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_SPECIAL_ABILITY_USE:
                                stringtoplace = "Special Ability Use";
                                break;
                        }
                    }
                    else
                    {
                        //try to add a new entry
                        stringtoplace = ms.overrideString;
                    }


                    int placeindex = -1;
                    bool found = false;


                    //search for the string in ExportStrings
                    foreach (string s in ExportStrings)
                    {
                        if ((s.CompareTo(stringtoplace) == 0) && (s.Length == stringtoplace.Length))
                        {
                            //match 
                            found = true;

                            placeindex = ExportStrings.IndexOf(s);

                            break;
                        }
                    }


                    if (!found)
                    {
                        placeindex = ExportStrings.Count;
                        ExportStrings.Add(stringtoplace);
                    }

                    //if not found make a new entry and put that indice on the ExportStringIndices pile
                    ExportStringIndices.Add(placeindex);

                }//hidden skip
            }//end string sorting loop

        }


        //private List<string> ExportStrings;
        //private List<int> ExportStringIndices;

        public void exportStringEntriesToFile(string target)
        {
            //private List<string> ExportStrings;
            //private List<int> ExportStringIndices;

            //tag format "[MISHSTA_*:MISHSTA]"
            string buffer = "";


 

            List<int> exported = new List<int>();
            List<String> exportedDescs = new List<String>();

            int indcount = 0;
           // foreach (MishStatStar.MissionEntry me in Missions)
            //{
                //if (me.MyStats.Count > 0)
                //{
                    foreach (MishStatStar.MissionStat ms in Stats)
                    {

                        bool isUsed = false;
                        foreach (MishStatStar.MissionEntry me in Missions)
                        {
                            if (me.MyStats.Count > 0)
                            {
                                isUsed = me.MyStats.Contains(ms);
                                if (isUsed)
                                {
                                    break;
                                }
                            }
                        }


                        if (!ms.Hidden && isUsed)
                        {

                            int expindex = ExportStringIndices[indcount];//ExportStringIndices[Stats.IndexOf(ms)];

                            if (!exported.Contains(expindex))
                            {
                                exported.Add(expindex);
                                string towrite = "[MISHSTA_" + expindex + ":MISHSTA]\r\n"
                                        + ExportStrings[expindex] + "\r\n\r\n";


                                buffer += towrite;
                                
                            }


                            if (DescLookups.Count > 0)
                            {
                                if (!exportedDescs.Contains(DescLookups[ms]))
                                {
                                    buffer += "[" + DescNames[DescLookups[ms]] + ":MISHSTA]\r\n";
                                    buffer += DescLookups[ms] + "\r\n";
                                    exportedDescs.Add(DescLookups[ms]);
                                }
                            }


                            ++indcount;
                        }

                    }
                //}

            //}

            FileStream f = new FileStream(target,FileMode.Create);
            StreamWriter w =  new StreamWriter(f);

            w.Write(buffer);

            w.Close();

        }

        public void replaceEnumsInHeaderFile(string target)
        {
            string buffer = "//BEGIN AGT STATS\r\n";

            int maxsta = 0;

            buffer += "\tUNSET_MISSION_STAT_ENUM_AGT = -1,\r\n";

            foreach (MishStatStar.MissionEntry me in Missions)
            { if (me.MyStats.Count > maxsta) { maxsta = me.MyStats.Count; } }
                        
            //add each of the stat enums from the list, by mission
            foreach (MishStatStar.MissionEntry me in Missions)
            {

                //
                if (me.MyStats.Count > 0)
                {
                    buffer += "\t// stats for mission " + me.FlowEnum + "\r\n";


                    foreach (MishStatStar.MissionStat ms in me.MyStats)
                    {
                        buffer += "\t" + ms.enumname + ", // " + ms.shortdesc.Replace("\r\n", " ").Replace("\n", " ") + "\r\n";
                    }
                }

            }



            buffer += "\tMAX_MISSION_STATS_AGENT,\r\n";
            buffer += "\tEMPTY_MISSION_STAT_AGENT,\r\n";

            FileStream f = new FileStream(target, FileMode.Open);
            StreamReader read = new StreamReader(f);
            string old = read.ReadToEnd();
            read.Close();

            int insAt = old.IndexOf("//BEGIN AGT STATS");
            int insTil = old.IndexOf("//END AGT STATS");
            old = old.Remove(insAt, insTil - insAt);
            old = old.Insert(insAt, buffer);
            //MessageBox.Show("Replacing from " + insAt + " to " + insTil+" with ",buffer);

            //*
            f = new FileStream(target, FileMode.Create);
            StreamWriter w = new StreamWriter(f);
            w.Write(old);
            w.Close();
            //*/
        }

        public void exportHeaderFile(string target)
        {
            //the .sch with the enums //pretty much it
            /*
            CONST_INT MAX_TRACKED_MISSION_STATS 7
             * 
            ENUM ENUM_MISSION_STATS
	            UNSET_MISSION_STAT_ENUM,
	
	
	
	
	
	            MAX_MISSION_STATS,
	            EMPTY_MISSION_STAT
            ENDENUM
            */
            string buffer = "";

            int maxsta = 0;


            foreach (MishStatStar.MissionEntry me in Missions)
            {if (me.MyStats.Count > maxsta) {maxsta = me.MyStats.Count;}}
            

            buffer += "\r\n\r\nCONST_INT MAX_TRACKED_MISSION_STATS " + maxsta + "\r\n";
  
            buffer += "ENUM ENUM_MISSION_STATS\r\n";
	        buffer += "\tUNSET_MISSION_STAT_ENUM = -1,\r\n";
	
	
	            //add each of the stat enums from the list, by mission
            /*
            foreach (MishStatStar.MissionStat ms in Stats)
            {
                buffer += "\tUNSET_MISSION_STAT_ENUM,\r\n";
            }
             */
            foreach (MishStatStar.MissionEntry me in Missions)
            {
                
                //
                if (me.MyStats.Count > 0)
                {
                    buffer += "\t// stats for mission " + me.FlowEnum + "\r\n";


                    foreach (MishStatStar.MissionStat ms in me.MyStats)
                    {
                        buffer += "\t" + ms.enumname + ", // " + ms.shortdesc.Replace("\r\n"," ").Replace("\n", " ") + "\r\n";
                    }
                }

            }

	
	           
            buffer += "\tMAX_MISSION_STATS,\r\n";
            buffer += "\tEMPTY_MISSION_STAT\r\n";
            
            buffer += "ENDENUM\r\n";

            FileStream f = new FileStream(target, FileMode.Create);
            StreamWriter w = new StreamWriter(f);
            w.Write(buffer);
            w.Close();
        }




        internal void exportFunctionFile(string target)
        {
            /*
            USING "globals.sch"\r\n
             * 


            PROC FLOW_PRIME_STATS_FOR_MISSION(SP_MISSIONS m)
            ENDPROC

            PROC FLOW_PRIME_STATS_FOR_RC_MISSION(g_eRC_MissionIDs m)
            //
		        RESET_MISSION_STATS_SYSTEM()
		
		
		        MISSION_STAT_ADD_WATCH(PAPARAZZO_4_ENTIRE_CREW_IN_ONE_SHOT)

  
            ENDPROC
 
            */
            string buffer = "//Auto generated private lookups for stat system\r\n";
            buffer += "USING \"globals.sch\"\r\n\r\n\r\n";

            //functions
            buffer += "PROC MISSION_STAT_RESET_STAT_REGISTER(INT index)\r\n";
	        buffer += "\tg_MissionStatTrackingArray[index].ivalue = 0\r\n";
	            buffer += "\tg_MissionStatTrackingArray[index].fvalue = 0.0\r\n";

                buffer += "\tg_MissionStatTrackingArray[index].invalidationReason = MSSIR_VALID\r\n";
	            buffer += "\tg_MissionStatTrackingArray[index].successshown = FALSE\r\n";
            buffer += "ENDPROC\r\n";
            buffer += "PROC MISSION_STAT_ADD_WATCH(ENUM_MISSION_STATS prototype)\r\n";
	            buffer += "\tg_bMissionStatSystemPrimed = TRUE\r\n";
	            buffer += "\tg_bMissionStatSystemMissionStarted = TRUE\r\n";
	                buffer += "\r\n";
	           buffer += "\t IF g_iMissionStatsBeingTracked > (MAX_TRACKED_MISSION_STATS-1)\r\n";
               buffer += "\t\tSCRIPT_ASSERT(\"MISSION_STAT_ADD_WATCH: Attempted to register new watch stats when max was already reached\")\r\n";
               buffer += "\t\tEXIT\r\n";
	            buffer += "\tENDIF\r\n";
	            buffer += "\tMISSION_STAT_RESET_STAT_REGISTER(g_iMissionStatsBeingTracked)\r\n";
	            buffer += "\tg_MissionStatTrackingArray[g_iMissionStatsBeingTracked].target = prototype\r\n";
                buffer += "\t++g_iMissionStatsBeingTracked\r\n";



                buffer += "\tIF g_MissionStatTrackingPrototypes[prototype].type = MISSION_STAT_TYPE_INNOCENTS_KILLED\r\n";
		        buffer += "\tg_bTrackingInnocentsLogged = TRUE\r\n";
	            buffer += "\tENDIF\r\n";


           buffer +=  "ENDPROC\r\n";
             


            /*
             * 
                PROC INTERNAL_GENERATED_ASSOCIATE_STATS_WITH_FLOW()

	                g_sMissionStaticData[paramMissionID].statCount = 0
	
	                g_sMissionStaticData[paramMissionID].stats[g_sMissionStaticData[paramMissionID].statCount] = paramStatEnum
	                g_sMissionStaticData[paramMissionID].statCount++


                ENDPROC
             * 
             */


           buffer += "\r\n\r\nPROC INTERNAL_GENERATED_ASSOCIATE_STATS_WITH_FLOW()\r\n";


           
           foreach (MishStatStar.MissionEntry me in Missions)
           {
               if (!me.bRCMission)
               {
                   buffer += "\tg_sMissionStaticData[" + me.FlowEnum + "].statCount = " + me.MyStats.Count + "\r\n";

                   foreach (MishStatStar.MissionStat ms in me.MyStats)
                   {
                       buffer += "\tg_sMissionStaticData[" + me.FlowEnum + "].stats[" + me.MyStats.IndexOf(ms)+ "] = " + ms.enumname + " \r\n";

                   }
               }
           }


           buffer += "\r\n\r\n\r\n";

           

           buffer += "ENDPROC\r\n";


            

            buffer += "\r\n\r\n\r\nFUNC STRING GET_MISSION_STAT_NAME(ENUM_MISSION_STATS e)\r\n";

            buffer += "\tSWITCH(e)\r\n";

            int switchlimit = 0;
            int indcount = 0;


                    //buffer += "\t// stats for mission " + me.FlowEnum + "\r\n";
                    

                    foreach (MishStatStar.MissionStat ms in Stats)
                    {
                        bool isUsed = false;
                        foreach (MishStatStar.MissionEntry me in Missions)
                        {
                            //
                            if (me.MyStats.Count > 0)
                            {

                                isUsed = me.MyStats.Contains(ms);

                                if (isUsed)
                                {
                                    break;
                                }
                            }
                        }

                        if (!ms.Hidden && isUsed)
                        {
                            ++switchlimit;
                            if (switchlimit > 30)
                            {
                                buffer += "\tENDSWITCH\r\n";
                                buffer += "\tSWITCH(e)\r\n";
                                switchlimit = 0;
                            }

                            buffer += "\t\tCASE " + ms.enumname + " \r\n";
                            buffer += "\t\t\tRETURN \"MISHSTA_" + ExportStringIndices[indcount] + "\" \r\n";
                            //ExportStringIndices[Stats.IndexOf(ms)]
                            ++indcount;
                        }
                    }
                
            
            buffer += "\tENDSWITCH\r\n";

            buffer += "\tSCRIPT_ASSERT(\"GET_MISSION_STAT_NAME : Mission stat with no string found!\")\r\n";
            buffer += "RETURN \"MISSING_MISSION_STAT_STRING\"\r\n";

            buffer += "ENDFUNC\r\n";
            buffer += "\r\n";





            


            buffer += "\r\n\r\n\r\nFUNC STRING GET_MISSION_STAT_DESCRIPTION(ENUM_MISSION_STATS e)\r\n";
            buffer += "\tSWITCH(e)\r\n";


            int dsccount = 0;
            if (DescLookups.Count > 0)
            {
                foreach (MishStatStar.MissionStat ms in Stats)
                {
                    //private Dictionary<MishStatStar.MissionStat, String> DescLookups;
                    //private Dictionary<String, String> DescNames;
                    //DescNames[DescLookups[ms]]
                    //DescLookups[ms]
                    if (DescLookups.ContainsKey(ms))
                    {
                        ++dsccount;
                        buffer += "\tCASE " + ms.enumname + "\r\n";
                        buffer += "\t\tRETURN \"" + DescNames[DescLookups[ms]] + "\"\r\n";
                    }
                    if (dsccount % 20 == 0)
                    {
                        buffer += "\tENDSWITCH\r\n";
                        buffer += "\tSWITCH(e)\r\n";
                        ++dsccount;
                    }
                }
            }

            if (dsccount == 0)
            {
                buffer += "\tDEFAULT\r\n";
                buffer += "\t\tRETURN \"NO_MISSION_STATS\"\r\n";
            }


            buffer += "\tENDSWITCH\r\n";
            buffer += "\tSCRIPT_ASSERT(\"GET_MISSION_STAT_NAME : Mission stat with no description found!\")\r\n";
            buffer += "RETURN \"MISSING_MISSION_STAT_STRING\"\r\n";
            buffer += "ENDFUNC\r\n";








            string standardFuncBuffer = "\r\n";
            string rcFuncBuffer = "\r\n";
            /*
            PROC FLOW_PRIME_STATS_FOR_MISSION(SP_MISSIONS m)
             * 
             * GET_STORY_MISSION_NAME_LABEL

            ENDPROC

            PROC FLOW_PRIME_STATS_FOR_RC_MISSION(g_eRC_MissionIDs m)
		        
                MISSION_STAT_ADD_WATCH(PAPARAZZO_4_ENTIRE_CREW_IN_ONE_SHOT)
             * 
             * GET_RC_MISSION_NAME_LABEL
             */

            rcFuncBuffer += "PROC INTERNAL_FLOW_PRIME_STATS_FOR_RC_MISSION(g_eRC_MissionIDs m)\r\n";
            standardFuncBuffer += "PROC INTERNAL_FLOW_PRIME_STATS_FOR_MISSION(SP_MISSIONS m)\r\n";
            int rcswicount = 0;
            int mcswicount = 0;

            //Count number of RC missions, skip SWITCH if it's 0
            foreach (MishStatStar.MissionEntry me in Missions)
                if (me.bRCMission)
                    rcswicount += 1;

           standardFuncBuffer += "SWITCH(m)\r\n";
            if (rcswicount >0)
                rcFuncBuffer += "SWITCH(m)\r\n";

            rcswicount = 0;

            foreach (MishStatStar.MissionEntry me in Missions)
            {
                if (!me.bRCMission)
                {


                    if (me.MyStats.Count > 0)
                    {

                        //regular mission
                        ++mcswicount;
                        if (mcswicount > 30)
                        {
                            //reclose and open switch again
                            standardFuncBuffer += "ENDSWITCH\r\n";
                            standardFuncBuffer += "SWITCH(m)\r\n";
                            mcswicount = 0;
                        }


                        standardFuncBuffer += "\tCASE " + me.FlowEnum + "  \r\n";

                        foreach (MishStatStar.MissionStat ms in me.MyStats)
                        {
                            //MISSION_STAT_ADD_WATCH()

                            standardFuncBuffer += "\t\tMISSION_STAT_ADD_WATCH(" + ms.enumname + ")  \r\n";
                        }

                        /*
                        if((me.LBBest.CompareTo("") != 0 ) && (me.LBLast.CompareTo("") != 0))
                        {
                            standardFuncBuffer += "\tg_eTargetMissionScoreLeaderboardBestRun = " + me.LBBest + "\n";
		                    standardFuncBuffer += "\tg_eTargetMissionScoreLeaderboardIndivRec = " + me.LBLast + "\n";
                        }*/

                        standardFuncBuffer += "\t\tEXIT\r\n";
                    }


                }
                else
                {
                    //rc mission

                    if (me.MyStats.Count > 0)
                    {
                        ++rcswicount;
                        if (rcswicount > 30)
                        {
                            //reclose and open switch again
                            rcFuncBuffer += "ENDSWITCH\r\n";
                            rcFuncBuffer += "SWITCH(m)\r\n";
                            rcswicount = 0;
                        }

                        rcFuncBuffer += "\tCASE " + me.FlowEnum + "  \r\n";

                        foreach (MishStatStar.MissionStat ms in me.MyStats)
                        {
                            rcFuncBuffer += "\t\tMISSION_STAT_ADD_WATCH(" + ms.enumname + ")  \r\n";
                        }
                        rcFuncBuffer += "\t\tEXIT\r\n";
                    }


                }
            }
            standardFuncBuffer += "ENDSWITCH\r\n";

            if (rcswicount>0)
                rcFuncBuffer += "ENDSWITCH\r\n";

            //IF RC count is 0, return successfully
            if (rcswicount == 0)
                rcFuncBuffer += "IF m=m \r\nENDIF \r\nEXIT\r\n";

            standardFuncBuffer += "\tSCRIPT_ASSERT(\"INTERNAL_FLOW_PRIME_STATS_FOR_MISSION : Mission has no stats registered, if this is intentional please inform Andrew Knight so he can add this mission to the stat exclusion list. Thanks!\")\r\n";
            rcFuncBuffer += "\tSCRIPT_ASSERT(\"INTERNAL_FLOW_PRIME_STATS_FOR_RC_MISSION : RC Mission has no stats registered, if this is intentional please inform Andrew Knight so he can add this mission to the stat exclusion list. Thanks!\")\r\n";


            standardFuncBuffer += "ENDPROC\r\n";
            rcFuncBuffer += "ENDPROC\r\n";



            buffer += standardFuncBuffer + rcFuncBuffer;



           // buffer += "\r\n\r\n\r\n//Init for stat values and targets, search and replace '//**////**////' with a tab to enable stats when in\r\n";

            buffer += "PROC INTERNAL_GENERATED_MISSION_STAT_CONFIGURE_TYPES_AND_VALUES_FOR_INDEX(ENUM_MISSION_STATS index, MissionStatInfo &tar)\r\n";

            int initcounts = 0;
            buffer += "\tSWITCH(index)\r\n";


            //for each of the stats do their init
            foreach (MishStatStar.MissionStat ms in Stats)
            {
                bool usethisstat = false;
                MishStatStar.MissionEntry meis = null;

                foreach (MishStatStar.MissionEntry me in Missions)
                {
                    if (me.MyStats.Contains(ms))
                    {
                        usethisstat = true;
                        meis = me;
                    }
                }

                if (usethisstat)
                {

                    ++initcounts;

                    if (initcounts > 30)
                    {
                        buffer += "\tENDSWITCH\r\n";
                        buffer += "\tSWITCH(index)\r\n";
                        initcounts = 0;

                    }

                    buffer += "\tCASE " + ms.enumname + "\r\n";

                    //set all the parameters
                    //ENUM_MISSION_STAT_TYPES type
                    buffer += "\t\ttar.type = " + MishStatStar.getCatEnum(ms.Category) + "\r\n";
                    //INT currentvalue
                    buffer += "\t\ttar.currentvalue = " + ms.baseValue + "\r\n";
                    //INT success_threshold
                    buffer += "\t\ttar.success_threshold = " + ms.targetValue + "\r\n";
                    //BOOL less_than_threshold
                    buffer += "\t\ttar.less_than_threshold = " + ms.lessThanTarget + "\r\n";

                    //STATSENUM statname
                    if ((!ms.Hidden))
                    {//**////**////

                        string stemp = ms.enumname;

                        if (stemp.Length > 28)
                        {
                            stemp = stemp.Remove(28);
                        }

                        buffer += "\t\ttar.statname = MS_" + stemp + "\r\n";
                    }

                    //INT MinRange
                    buffer += "\t\ttar.MinRange = " + ms.ScoreMin + "\r\n";
                    //INT MaxRange
                    buffer += "\t\ttar.MaxRange = " + ms.ScoreMax + "\r\n";

                    //BOOL bVisible
                    buffer += "\t\ttar.bHidden = " + ms.Hidden + "\r\n";

                    //INT iLastGeneratedLBScore //not premade
                    //buffer += "\t\ttar.iLastGeneratedLBScore = 0\r\n";


                    buffer += "\t\ttar.lb_differentiator = " + ms.lb_differentiator + "\r\n";
                    
                    //FLOAT lb_weight_PPC
                    buffer += "\t\ttar.lb_weight_PPC = " + ms.lb_weight_PPC + "\r\n";
                    //INT lb_min_legal
                    buffer += "\t\ttar.lb_min_legal = " + ms.lb_min_legal + "\r\n";
                    //INT lb_max_legal
                    buffer += "\t\ttar.lb_max_legal = " + ms.lb_max_legal + "\r\n";
                    //INT lb_precedence
                    buffer += "\t\ttar.lb_precedence = " + ms.lb_precedence + "\r\n";



                    buffer += "\tEXIT\r\n";
                }


            }



            buffer += "\tENDSWITCH\r\n";

            buffer += "ENDPROC\r\n\r\n";





            //

            buffer += "FUNC INT GET_NUMBER_OF_STATS_FOR_RC_MISSION(g_eRC_MissionIDs m)\r\n";

            //Count number of RC missions, skip SWITCH if it's 0
            rcswicount = 0;
            foreach (MishStatStar.MissionEntry me in Missions)
                if (me.bRCMission)
                    rcswicount += 1;

            buffer += "\t\r\n";
            if (rcswicount>0)
                buffer += "\tSWITCH m\r\n";

            int resetcount = 0;
            foreach (MishStatStar.MissionEntry me in Missions)
            {
                if (me.MyStats.Count > 0)
                {
                    if (me.bRCMission)
                    {
                        if (resetcount > 20)
                        {
                            resetcount = 0;
                            buffer += "\tENDSWITCH\r\n";
                            buffer += "\tSWITCH m\r\n";
                        }
                        //needs an entry, otherwise zero


                        buffer += "\t\tCASE " + me.FlowEnum + "\r\n";
                        buffer += "\t\t\tRETURN " + me.MyStats.Count + "\r\n";

                        ++resetcount;

                    }
                }
            }
            if (rcswicount > 0)
                buffer += "\tENDSWITCH\r\n";
            else
                buffer += "IF m=m \r\nENDIF \r\n";
            buffer += "\tRETURN 0\r\n";

            buffer += "ENDFUNC\r\n\r\n";



            buffer += "FUNC ENUM_MISSION_STATS GET_NTH_STAT_FOR_RC_MISSION(g_eRC_MissionIDs m, INT i )\r\n";

            buffer += "\t\r\n";
            if (rcswicount>0)
                buffer += "\tSWITCH m\r\n";

            resetcount = 0;
            foreach (MishStatStar.MissionEntry me in Missions)
            {
                if (me.MyStats.Count > 0)
                {
                    if (me.bRCMission)
                    {
                        if (resetcount > 20)
                        {
                            resetcount = 0;
                            buffer += "\tENDSWITCH\r\n";
                            buffer += "\tSWITCH m\r\n";
                        }
                        //needs an entry, otherwise zero


                        buffer += "\t\tCASE " + me.FlowEnum + "\r\n";
                        buffer += "\t\t\tSWITCH i\r\n";
                        int nth = 0;
                        foreach (MishStatStar.MissionStat ms in me.MyStats)
                        {
                            buffer += "\t\t\t\tCASE " + nth + "\r\n";
                            buffer += "\t\t\t\tRETURN " + ms.enumname + "\r\n";
                            ++nth;
                        }
                        buffer += "\t\t\tENDSWITCH\r\n";
                        buffer += "\t\t\tRETURN UNSET_MISSION_STAT_ENUM\r\n";
                        ++resetcount;

                    }
                }
            }
            if (rcswicount > 0)
                buffer += "\tENDSWITCH\r\n";
            else
                buffer += "IF m=m \r\nENDIF \r\nIF i=i \r\nENDIF \r\n";

            buffer += "\tRETURN UNSET_MISSION_STAT_ENUM\r\n";

            buffer += "ENDFUNC\r\n\r\n";






            FileStream f = new FileStream(target, FileMode.Create);
            StreamWriter w = new StreamWriter(f);
            w.Write(buffer);
            w.Close();




            








        }

        internal void exportStatFile(string target)
        {

            //sp stats entries for each of the mission stats, lets do by index
            //but make the comment human readable with the text label and the enum name
            string buffer = "";

            //"<stat Name=\"" +  + "\"Type=\"int\" Group=\"8\" SaveCategory=\"0\"  Default=\"0\" online=\"false\"    profile=\"true\" FlushPriority=\"0\" ServerAuthoritative=\"false\"   community=\"false\" OmitTimestamp=\"false\"  UserData=\"STAT_FLAG_NEVER_SHOW\"  Owner=\"script\" Comment=\"Mission stat : " +  + "\" />"

            //"MS_MISHPROF_*"

            //<!-- MISSION STATS -->

            foreach (MishStatStar.MissionEntry me in Missions)
            {

                //
                if (me.MyStats.Count > 0)
                {

                    if (!me.bRCMission)
                    {
                        buffer += "\r\n<!-- MISSION PROFILE STATS FOR : " + me.FlowEnum + " -->\r\n";
                    }

                    foreach (MishStatStar.MissionStat ms in me.MyStats)
                    {


                        if (!ms.Hidden)
                        {
                            //byte[] utf8String = Encoding.UTF8.GetBytes(ms.shortdesc.Replace("\n", " ").Replace("\"", " ").Replace("\r", " ").Replace("\\", " ").Replace("&", "and"));
                            string com = ms.shortdesc.Replace("\n", " ").Replace("\"", " ").Replace("\r", " ").Replace("\\", " ").Replace("&", "and");

                            com = Regex.Replace(com, @"[^\u0000-\u007F]", string.Empty);


                            string stemp = ms.enumname;
                            if (stemp.Length > 28)
                            {
                                stemp = stemp.Remove(28);
                            }
                             

                            buffer += "<stat Name=\"MS_" + stemp
                            + "\" Type=\"int\" " 
                            + ms.getXmlTypeBoundaries()
                            + " Group=\"8\" SaveCategory=\"0\"  Default=\"0\" online=\"false\"    profile=\"true\" FlushPriority=\"0\" ServerAuthoritative=\"false\"   community=\"false\" OmitTimestamp=\"false\"  UserData=\"STAT_FLAG_NEVER_SHOW\"  Owner=\"script\" Comment=\"Mission stat : '" +
                            ms.enumname + " : " +
                           // Encoding.UTF8.GetString(utf8String) +
                           com +
                            "' Target value is: '" + ms.targetValue + "' lessThan status is : '" + ms.lessThanTarget +
                            "'\" />\r\n";
                        }

                    }

                }


            }



            FileStream f = new FileStream(target, FileMode.Create);
            StreamWriter w = new StreamWriter(f);
            w.Write(buffer);
            w.Close();
        }

        internal void exportImplementationNotes(string target)
        {
            string buffer = "These notes relate to [[Mission Stats System and MissionStatStar]]\r\n\r\n=Mission Stat implementation notes=\r\n";
            //notes for all the stats by mission.
                //implementation notes by category enum
                //this is the true purpose for the comment
                
                //anchored in same page
               //<a href="#tips">Visit the Useful Tips Section</a> 
               //<a id="tips">Useful Tips Section</a>
            
            //indexing entries by mission
            /*
            foreach (MishStatStar.MissionEntry me in Missions)
            {

                //
                if (me.MyStats.Count > 0)
                {

                    buffer += "<a href=\"#" +me.FlowEnum+ "\">" + me.FlowEnum  + "</a></br>";



                }
                
            }
            buffer += "</br></br></br>";
            */

            //for each mission for each stat in that mission
            foreach (MishStatStar.MissionEntry me in Missions)
            {

                //
                if (me.MyStats.Count > 0)
                {

                    buffer += "==" + me.FlowEnum + "==\r\n";

                    foreach (MishStatStar.MissionStat ms in me.MyStats)
                    {
                        buffer += "===Stat details for : " + ms.enumname + "===\r\n\r\n";


                        if (ms.shortdesc.CompareTo("") != 0)
                        {
                            buffer += "Description : " + ms.shortdesc + " \r\n\r\n";
                        }
                        else
                        {
                            buffer += "Description : NO DESCRIPTION SET FOR THIS STAT!\r\n";
                        }




                        buffer += "Enumerated type : " + 
                            MishStatStar.getCatEnum(ms.Category) + 
                            " // " +
                            MishStatStar.getCatNoteString(ms.Category) + " \r\n\r\n";




                        switch (ms.Category)
                        {
                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_TOTALTIME:
					//Actions required by scripter: None, automatic.
					//Optional actions: You can inform the stat system of when you take control from the player or of points you do not wish the counter to run by using INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_START() and INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
									  //Mission stat system can be told of timeskips that would invalidate this via MISSION_STAT_SYSTEM_ALERT_TIME_WARP


                                buffer += "Actions required by scripter: None, this stat is tracked automatically.\r\n";
                                buffer += "Optional actions: You can inform the stat system of when you take control from the player or of points you do not wish the counter to run by using INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_START() and INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()\r\n";
                                buffer += "Mission stat system can be told of timeskips that would invalidate this via MISSION_STAT_SYSTEM_ALERT_TIME_WARP\r\n";

                                break;


                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_ACTION_CAM_USE:
					//Actions required by scripter: None, this is automatic
                                buffer += "Actions required by scripter: Use INFORM_MISSION_STATS_SYSTEM_OF_ACTION_CAM_START() and INFORM_MISSION_STATS_SYSTEM_OF_ACTION_CAM_END().\r\n";
                                break;

                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_UNIQUE_BOOL:
					//Actions required by scripter: When the conditions in the comment are met call INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(ENUM_MISSION_STATS e)
                                buffer += "Actions required by scripter: When the conditions in the comment are met call INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(" + ms.enumname + ")\r\n";
                                break;



                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_WINDOWED_TIMER:
					//Actions required by scripter: Open and close the time window in the conditions noted in the comment using INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(ENUM_MISSION_STATS windowstat) and INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
                                buffer += "Actions required by scripter: Open and close the time window in the conditions noted in the comment using INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(" + ms.enumname + ") and INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()\r\n";
                                break;

                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD:
					//Actions required by scripter: Tell the mission stats system which entity to watch the speed of using INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(ENTITY_INDEX in) pass this null if you want to clear the watch

                                buffer += "Actions required by scripter: Tell the mission stats system which entity to watch the speed of using INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(ENTITY_INDEX in) pass this null if you want to clear the watch\r\n";
                                break;

                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD:
					//Actions required by scripter: Tell the mission stat system which entity to watch the damage of using INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(ENTITY_INDEX in) pass this null to clear
                                buffer += "Actions required by scripter: Tell the mission stat system which entity to watch the damage of using INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(ENTITY_INDEX in) pass this null to clear\r\n";
                                break;

                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_HEADSHOTS:
					//Actions required by scripter: Tell the mission stat system which entities to watch for headshots using INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(ENTITY_INDEX in), the buffer is currently 64 entities long and can be cleared if needed using RESET_MISSION_STATS_ENTITY_WATCH()
                                buffer += "Actions required by scripter: Tell the mission stat system which entities to watch for headshots using INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(ENTITY_INDEX in), the buffer is currently 64 entities long and can be cleared if needed using RESET_MISSION_STATS_ENTITY_WATCH()\r\n";
                                break;

                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_FRACTION:
					//Actions required by scripter: Call INFORM_MISSION_STATS_OF_INCREMENT(ENUM_MISSION_STATS e) to increment this stat when the events detailed in the comment happen.
                                buffer += "Actions required by scripter: Call INFORM_MISSION_STATS_OF_INCREMENT(" + ms.enumname + ") to increment this stat when the events detailed in the comment happen.\r\n";
                                break;

                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_ACCURACY:
					//Actions required by scripter: None, this is automatic
                                buffer += "Actions required by scripter: None, this stat is tracked automatically.";
                                break;

                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_PURE_COUNT:
					//Actions required by scripter: Call INFORM_MISSION_STATS_OF_INCREMENT(ENUM_MISSION_STATS e) to increment this stat when the events detailed in the comment happen.
                                buffer += "Actions required by scripter: Call INFORM_MISSION_STATS_OF_INCREMENT(" + ms.enumname + ") to increment this stat when the events detailed in the comment happen.\r\n";
                                break;

                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE:
					//Actions required by scripter: Call INFORM_MISSION_STATS_OF_INCREMENT(ENUM_MISSION_STATS e) to increment this stat when the events detailed in the comment happen. May want to call it once to set the percentage for this metric. Use the optional integer argument to set the percentage in one call.
                                buffer += "Actions required by scripter: Call INFORM_MISSION_STATS_OF_INCREMENT(" + ms.enumname + ") to increment this stat when the events detailed in the comment happen. May want to call it once to set the percentage for this metric. Use the optional integer argument to set the percentage in one call.\r\n";
                                break;

                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_FINANCE_DIRECT:
					//Actions required by scripter: Call INFORM_MISSION_STATS_OF_FINANCIAL_DAMAGE(INT dollars) to increment property damage related to this stat.
                                buffer += "Actions required by scripter: Call INFORM_MISSION_STATS_OF_FINANCIAL_DAMAGE(INT dollars) to increment property damage related to this stat.</br>";
                                break;

                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_FINANCE_TABLE:
					//"Actions required by scripter: To set the dollar score values of certain models call INFORM_MISSION_STATS_OF_FINANCE_MODEL(MODEL_NAMES name, INT value), you can reset this list using INFORM_MISSION_STATS_OF_FINANCE_WATCH_RESET()"
                                buffer += "Actions required by scripter: To set the dollar score values of certain models call INFORM_MISSION_STATS_OF_FINANCE_MODEL(MODEL_NAMES name, INT value), you can reset this list using INFORM_MISSION_STATS_OF_FINANCE_WATCH_RESET()\r\n";
                                break;

                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_PURE_COUNT_DISTANCE: //In pending new upload type
                                buffer += "Set the distance value using \r\n";
                                break;
                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_BULLETS_FIRED://accuracy but shots only
                                buffer += "Actions required by scripter: None, this stat is tracked automatically.";
                                break;
                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_INNOCENTS_KILLED:
                                buffer += "Actions required by scripter: None, this stat is tracked automatically.";
                                break;
                            case MishStatStar.StatCategory.MISSION_STAT_TYPE_SPECIAL_ABILITY_USE:
                                buffer += "Actions required by scripter: None, this stat is tracked automatically.";
                                break;

                        }










                        buffer += "\r\n";
                    }

                }


            }






            buffer += "\r\n";
            FileStream f = new FileStream(target, FileMode.Create);
            StreamWriter w = new StreamWriter(f);
            w.Write(buffer);
            w.Close();
        }

        internal void exportLeaderboardConsistancyWarnings(string target)
        {
            /*
            string buffer = "";
            MissionBindsConsistant.Clear();
            MissionBindsConsistantCount = 0;
            MissionBindsConsistantCountRC = 0;
            foreach (MishStatStar.MissionEntry me in Missions)
            {
                bool valid = false;
                //does it have stats?
                if (me.MyStats.Count == 0)
                {
                    buffer += me.FlowEnum + " has no stats set. Is this intentional?\r\n";
                }
                else
                {
                    //it has stats, check if it has leaderboard binds
                    if (me.LBBestID != 0 && me.LBLastID != 0 && me.LBBest.CompareTo("") != 0 && me.LBLast.CompareTo("") != 0)
                    {

                        Leaderboard best = null;
                        Leaderboard last = null;

                        bool foundBest = false;
                        //it has a leaderboard bind, does it match with the indices?
                        //check best
                        foreach (Leaderboard lb in Leaderboards.GetLeaderboards())
                        {
                            if (lb..m_LeaderboardId == me.LBBestID)
                            {
                                //id matches
                                if (lb.m_Name.CompareTo(me.LBBest) == 0)
                                {
                                    foundBest = true;
                                    best = lb;
                                }
                            }
                        }

                        if (!foundBest)
                        {
                            buffer += me.FlowEnum + " does not have a valid 'Best' leaderboard set! IDs do not match.\r\n";
                        }


                        bool foundLast = false;
                        //check last
                        foreach (Leaderboard lb in Leaderboards.GetLeaderboards())
                        {
                            //lb.m_Name 
                            //me.LBBest
                            if (lb.m_LeaderboardId == me.LBLastID)
                            {
                                //id matches
                                if (lb.m_Name.CompareTo(me.LBLast) == 0)
                                {
                                    foundLast = true;
                                    last = lb;
                                }
                            }
                        }

                        if (!foundLast)
                        {
                            buffer += me.FlowEnum + " does not have a valid 'Last' leaderboard set! IDs do not match.\r\n";
                        }

                        if (best != null)
                        {
                            best.passedLastValidation = false;
                        }
                        if (last != null)
                        {
                            best.passedLastValidation = false;
                        }

                        if (foundLast && foundBest)
                        {
                            bool keepChecking = true;
                            //now validate the stat layout.
                            if (last == null || best == null) {keepChecking = false;}//redundant

                            //do both leaderboards have exactly matching entries, compare last to best
                            if (keepChecking)
                            {
                                if (last.getColumns().Count != best.getColumns().Count)
                                {
                                    keepChecking = false;
                                    buffer += me.FlowEnum + " has a leaderboard structure mismatch between Best and Indiv : counts do not match up.\r\n";

                                }
                                else
                                {
                                    int t = last.getColumns().Count;

                                    for (int i = 0; i < t; ++i)
                                    {
                                        if (last.getColumns()[i].m_Id != best.getColumns()[i].m_Id)
                                        {
                                            keepChecking = false;

                                            buffer += me.FlowEnum + " has a leaderboard structure mismatch between Indiv and Best : indices within don't match:\r\n";
                                            buffer += last.getColumns()[i].m_Name + " (" + last.getColumns()[i].m_Id + ") !=  " + best.getColumns()[i].m_Name + " (" + best.getColumns()[i].m_Id + ")  \r\n";

                                        }
                                        else
                                        {
                                            if (last.getColumns()[i].m_Name.CompareTo(best.getColumns()[i].m_Name) != 0)
                                            {
                                                keepChecking = false;
                                                buffer += me.FlowEnum + " has a leaderboard structure mismatch between Indiv and Best : names at same indice with same ID don't match:\r\n";
                                                buffer += last.getColumns()[i].m_Name + " (" + last.getColumns()[i].m_Id + ") !=  " + best.getColumns()[i].m_Name + " (" + best.getColumns()[i].m_Id + ")  \r\n";
                                            }

                                        }
                                    }

                                }
                            }


                            int validationCount = 0;
                            //do the leaderboards have the right default mission entries?
                            if (keepChecking)
                            {
                                //look for 
                                //SCORE
                                bool foundscore = false;
                                //BEST_TIME
                                bool foundtime = false;
                                //REPLAYS
                                bool foundreplays = false;
                                //FAILS
                                bool foundfails = false;
                                //PASSES
                                bool foundpasses = false;
                                //all of these should be present
                                
                                //only check last since if we've got this far they match completely

                                foreach (Leaderboard.Column lbc in last.getColumns())
                                {
                                    if (!foundscore) { if (lbc.m_Name.CompareTo("SCORE_COLUMN") == 0) { foundscore = true; } }
                                    if (!foundtime) { if (lbc.m_Name.CompareTo("BEST_TIME") == 0) { foundtime = true; } }
                                    if (!foundreplays) { if (lbc.m_Name.CompareTo("REPLAYS") == 0) { foundreplays = true; } }
                                    if (!foundfails) { if (lbc.m_Name.CompareTo("FAILS") == 0) { foundfails = true; } }
                                    if (!foundpasses) { if (lbc.m_Name.CompareTo("PASSES") == 0) { foundpasses = true; } }
                                }

                                if (!foundscore || !foundtime || !foundreplays || !foundfails || !foundpasses)
                                {
                                    if (!foundscore) { buffer += me.FlowEnum + " is missing an entry for SCORE_COLUMN on it's leaderboard pair\r\n"; }
                                    if (!foundtime) { buffer += me.FlowEnum + " is missing an entry for BEST_TIME on it's leaderboard pair\r\n"; }
                                    if (!foundreplays) { buffer += me.FlowEnum + " is missing an entry for REPLAYS on it's leaderboard pair\r\n"; }
                                    if (!foundfails) { buffer += me.FlowEnum + " is missing an entry for FAILS on it's leaderboard pair\r\n"; }
                                    if (!foundpasses) { buffer += me.FlowEnum + " is missing an entry for PASSES on it's leaderboard pair\r\n"; }
                                }
                                else
                                {
                                    validationCount = 5; //if we got this far then set note these 5 entries
                                }

                            }
                            if (validationCount == 0)
                            {
                                keepChecking = false;//bail out if the count is invalid
                            }

                            //do the stats bound to mission exist in the leaderboard?
                            //those that have leaderboard bindings at least
                            if (keepChecking)
                            {
                                foreach (MishStatStar.MissionStat ms in me.MyStats)
                                {
                                    if(ms.lb_columnID != 0) //ignore unset
                                    {
                                        foreach (Leaderboard.Column lbc in last.getColumns())
                                        {
                                            if (lbc.m_Name.CompareTo(ms.lb_enumerant) == 0)
                                            {
                                                //found a match, check the IDs
                                                if (lbc.m_Id != ms.lb_columnID)
                                                {
                                                    ms.lb_columnID = lbc.m_Id;
                                                    buffer += me.FlowEnum + " : WARNING column id mismatch for " + lbc.m_Name + " id in stat was " + ms.lb_columnID + " but ID in leaderboard was " + lbc.m_Id + " \r\n";
                                                    buffer += me.FlowEnum + " changed column ID for this stat to match. This should work providing the indices in the leaderboard config are correct\r\n";
                                                }


                                                //make sure its not a rebinding of an existing stat

                                                if (!(ms.lb_enumerant.CompareTo("SCORE_COLUMN") == 0)
                                                && !(ms.lb_enumerant.CompareTo("BEST_TIME") == 0)
                                                && !(ms.lb_enumerant.CompareTo("REPLAYS") == 0)
                                                && !(ms.lb_enumerant.CompareTo("FAILS") == 0)
                                                && !(ms.lb_enumerant.CompareTo("PASSES") == 0))
                                                {
                                                    ++validationCount;
                                                }
                                               // else
                                                //{
                                                //    ++overlapCount;
                                               // }
                                               
                                            }
                                        }
                                    }
                                }
                            }

                            //are there any remaining unbound stats in the leaderboard?
                            if (keepChecking)
                            {
                                //check validation count against the total
                                if (validationCount != last.getColumns().Count)
                                {
                                    buffer += me.FlowEnum + "'s validation count does not match up, there are " + validationCount + " leaderboard binds for it when there should be " + last.getColumns().Count + " !!\r\n";
                                    keepChecking = false;

                                    if (last.getColumns().Count > validationCount)//more in lb than bound
                                    {
                                        buffer += "Looking for potential unbound entries: \r\n";

                                        foreach (Leaderboard.Column lc in last.getColumns())
                                        {
                                            if (!(lc.m_Name.CompareTo("SCORE_COLUMN") == 0)
                                                && !(lc.m_Name.CompareTo("BEST_TIME") == 0)
                                                && !(lc.m_Name.CompareTo("REPLAYS") == 0)
                                                && !(lc.m_Name.CompareTo("FAILS") == 0)
                                                && !(lc.m_Name.CompareTo("PASSES") == 0))
                                                {
                                                    bool Bindfound = false;
                                                    //its an auto bound, skip
                                                    foreach (MishStatStar.MissionStat ms in me.MyStats)
                                                    {
                                                        if (!(ms.lb_enumerant.CompareTo("SCORE_COLUMN") == 0)
                                                        && !(ms.lb_enumerant.CompareTo("BEST_TIME") == 0)
                                                        && !(ms.lb_enumerant.CompareTo("REPLAYS") == 0)
                                                        && !(ms.lb_enumerant.CompareTo("FAILS") == 0)
                                                        && !(ms.lb_enumerant.CompareTo("PASSES") == 0))
                                                        {
                                                            //its not a rebind, do check

                                                            if (lc.m_Name.CompareTo(ms.lb_enumerant) == 0)
                                                            {
                                                                Bindfound = true; 
                                                            }

                                                        }
                                                    }
                                                    if (!Bindfound)
                                                    {
                                                        buffer += "No bind found for " + lc.m_Name + " \r\n";   
                                                    }
                                                }
                                        }
                                    }

                                }
                            }

                            //if we made it this far then the binds are valid for this mission
                            if (keepChecking)
                            {
                                buffer +="SUCCESS: " +  me.FlowEnum + "'s leaderboard binds passed validation! Hooray!\r\n";
                                valid = true;

                                best.passedLastValidation = true;
                                last.passedLastValidation = true;

                            }
                        }


                    }
                    else
                    {
                        buffer += me.FlowEnum + " does not have a pair of leaderboard binds, Is this correct?\r\n";
                    }


                }
                if (valid)
                {
                    if (me.bRCMission)
                    {
                        ++MissionBindsConsistantCountRC;
                    }
                    else
                    {
                        ++MissionBindsConsistantCount;
                    }
                }
                MissionBindsConsistant.Add(valid);
                buffer += "\r\n";
            }



            

            buffer += "\r\n";
            FileStream f = new FileStream(target, FileMode.Create);
            StreamWriter w = new StreamWriter(f);
            w.Write(buffer);
            w.Close();
            */
        }

        internal void exportLeaderboardHeader(string target)
        {

            string buffer = "USING \"globals.sch\"\r\n//Leaderboard mission stat write header\r\n";
            buffer += "USING \"mission_stat_public.sch\"\r\n\r\n";




            string bufferMC = "";
            string bufferRC = "";

            int mcC = 0;
            int rcC = 0;

            int iNewSwitch = 0;


            bufferMC += "\tLeaderboardUpdateData writeData\n";
            bufferRC += "\tLeaderboardUpdateData writeData\n";

            bufferMC += "\tSWITCH target\n";
            bufferRC += "\tSWITCH target\n";
            foreach(MissionStatsStar.MishStatStar.MissionEntry me in Missions)
            {
               string bufferME = "";


               if (me.LBBest != null && me.LBLast != null && me.LBBestID != 0 && me.LBLastID != 0)
               {
                   if(me.MyStats.Count > 0){
                       //bufferME += "// stats for " + me.MishString + " - " + me.MyStats.Count + " entries \n";
                       //bufferME += "// lbbest : " + me.LBBest + " - lblast : " + me.LBLast + " \n";

                       if (  me.FlowEnum.CompareTo("SP_MISSION_EXILE_2") == 0)
                       {
                           int fsg = 131;
                       }

                       

                       string melast = "\t\twriteData.m_LeaderboardId = " + me.LBLastID + "\n";
                       melast += "\t\tLEADERBOARDS2_WRITE_DATA(writeData)\n";
                       string mebest = "\t\twriteData.m_LeaderboardId = " + me.LBBestID + "\n";
                       mebest += "\t\tLEADERBOARDS2_WRITE_DATA(writeData)\n";





                       string staticbinds = getMissionStaticLBBinds(me);
                       mebest += staticbinds;
                       melast += staticbinds;

                       bool failed = false;

                       foreach (MishStatStar.MissionStat ms in me.MyStats)
                       {

                           //if (  ms.lb_enumerant.CompareTo("SPECIAL_ABILITY_TIME") != 0)
                           //{
                               if (ms.lb_enumerant != null && ms.lb_columnID > 0)
                               {
                                   //bufferME += "\t" + ms.lb_enumerant + " - " + ms.lb_columnID + "\n";
                                   string bes = getInputForBoardColumn(me.LBBestID, ms.lb_enumerant,ref failed);
                                   string las = getInputForBoardColumn(me.LBLastID, ms.lb_enumerant,ref failed);
                                   //ms.lb_enumerant
                                   mebest += "\t\tLEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_" + bes + ", " + getColumnValueRead(ms) + ", 0.0)\n";
                                   melast += "\t\tLEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_" + las + ", " + getColumnValueRead(ms) + ", 0.0)\n";

                                   if (me.bRCMission) { ++rcC; } else { ++mcC; }
                               }
                          // }
                       }


                       if (!failed)
                       {
                           bufferME += "\tCASE " + me.FlowEnum + "// " + me.LBBest + " and " + me.LBLast + "\n";
                           bufferME += melast;
                           bufferME += mebest;
                           bufferME += "\tEXIT\n";
                       }



                       ++iNewSwitch;
                       if (iNewSwitch > 24)
                       {
                           bufferME += "\tENDSWITCH\n\tSWITCH target\n";
                           iNewSwitch = 0;
                       }

                   }
                   
               }

               if(me.bRCMission){bufferRC += bufferME;}else{bufferMC+=bufferME;}
            }
            bufferMC += "\tENDSWITCH\n";
            bufferRC += "\tENDSWITCH\n";

            buffer += "PROC GENERATED_TRY_WRITE_CURRENT_STAT_REGISTERS_TO_LEADERBOARD(SP_MISSIONS target)\n";
            if (mcC > 0)
            {
                buffer += bufferMC;
            }
            else
            {
                //placeholder call
                
                buffer += "\tSP_MISSIONS t = target //placholder with no binds\n";
                buffer += "\tt = t //placholder with no binds\n";
                
            }
            buffer += "ENDPROC\n";

            buffer += "PROC GENERATED_TRY_WRITE_CURRENT_STAT_REGISTERS_TO_LEADERBOARDRC(g_eRC_MissionIDs target)\n";
            if (rcC > 0)
            {
                buffer += bufferRC;
                
            }
            else
            {
                //placeholder call
                buffer += "\tg_eRC_MissionIDs t = target //placholder with no binds\n";
                buffer += "\tt = t //placholder with no binds\n";

            }
            buffer += "ENDPROC\n";

            FileStream f = new FileStream(target, FileMode.Create);
            StreamWriter w = new StreamWriter(f);
            w.Write(buffer);
            w.Close();


/*
            //check the consistancy bind results match the number of missions

            if (MissionBindsConsistant.Count != Missions.Count)
            {
                MessageBox.Show("Cannot generate leaderboards header! Mission binds count doesn match mission count! Please contact Andrew Knight with the steps or file that duplicates this!");
            }

            
            string buffer = "USING \"globals.sch\"\r\n//Leaderboard mission stat write header\r\n";
           // buffer += "USING \"mission_stat_public.sch\"\r\n\r\n";



            buffer += "FUNC INT GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(ENUM_MISSION_STATS m )\r\n";
                    buffer += "\tINT i = 0\r\n";
                    buffer += "\tREPEAT g_iMissionStatsBeingTracked i\r\n";
                            buffer += "\t\tIF g_MissionStatTrackingArray[i].target  = m\r\n";
         			            buffer += "\t\t\tRETURN g_MissionStatTrackingArray[i].ivalue\r\n";
                            buffer += "\t\tENDIF\r\n";
                    buffer += "\tENDREPEAT\r\n";
		            buffer += "\tSCRIPT_ASSERT(\"GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS: target not in the registers. Pass this bug to Andrew Knight with a log and description of what you were doing.\")\r\n";
		            buffer += "\tRETURN 0\r\n";
                    buffer += "ENDFUNC\r\n\r\n";

            

            if (MissionBindsConsistantCountRC == 0 && MissionBindsConsistantCount == 0)
            { //do placeholder
                buffer += "PROC GENERATED_TRY_WRITE_CURRENT_STAT_REGISTERS_TO_LEADERBOARD(SP_MISSIONS target)\r\n";
                buffer += "SP_MISSIONS t = target //placholder with no binds\r\n";
                buffer += "t = t //placholder with no binds\r\n";
                buffer += "ENDPROC\r\n";

                buffer += "PROC GENERATED_TRY_WRITE_CURRENT_STAT_REGISTERS_TO_LEADERBOARDRC(g_eRC_MissionIDs target)\r\n";
                buffer += "g_eRC_MissionIDs t = target //placholder with no binds\r\n";
                buffer += "t = t //placholder with no binds\r\n";
                buffer += "ENDPROC\r\n";
                FileStream f2 = new FileStream(target, FileMode.Create);
                StreamWriter w2 = new StreamWriter(f2);
                w2.Write(buffer);
                w2.Close();
                return;
            }



            //for every mission that has passed consistancy validation
            //populate the write struct
            //write to both leaderboard IDs

            //use the order of the leaderboard cols to populate the array in the struct



            //LeaderboardData stcrp
            //stcrp.m_Type = LEADERBOARD_TYPE_PLAYER
	        //skipping m_GroupId and m_Group because don't care
	        //stcrp.m_NumValues = 1
	        //stcrp.m_ColValues[0] = score
            int total = Missions.Count;

            //SP_MISSIONS m //add split
            //g_eRC_MissionIDs m


            buffer += "PROC GENERATED_TRY_WRITE_CURRENT_STAT_REGISTERS_TO_LEADERBOARD(SP_MISSIONS target)\r\n";
            string bufferRC = "PROC GENERATED_TRY_WRITE_CURRENT_STAT_REGISTERS_TO_LEADERBOARDRC(g_eRC_MissionIDs target)\r\n";
            buffer += "\r\n";
            bufferRC += "\r\n"; 

            int swcount = 0;
            int swcountrc = 0;

            buffer += "LeaderboardUpdateData  lud\r\n";
            bufferRC += "LeaderboardUpdateData  lud\r\n";



            string netproc = "IF NOT NETWORK_IS_SIGNED_ONLINE()\r\n";
		    netproc += "PRINTLN(\"GENERATED_TRY_WRITE_CURRENT_STAT_REGISTERS_TO_LEADERBOARD: not online, cannot write\")\r\n";
            netproc += "EXIT\r\n";
	        netproc += "ENDIF\r\n\r\n";
                
            netproc += "GAMER_HANDLE gamerHandle\r\nNETWORK_CLAN_DESC PlayerClan\r\n";
           // netproc += "IF NETWORK_IS_SIGNED_ONLINE()\r\n";
            netproc += "IF NETWORK_CLAN_SERVICE_IS_VALID()\r\n";
            netproc += "gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())\r\n";
            netproc += " IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)\r\n";
            netproc += "  IF NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)\r\n";
            netproc += "lud.m_ClanId = PlayerClan.Id\r\n";
            netproc += "ENDIF\r\n";
            //netproc += "ENDIF\r\n";
            netproc += "ENDIF\r\n";
            netproc += "ENDIF\r\n";



            buffer += netproc;
            bufferRC += netproc;

            buffer += "\tSWITCH(target)\r\n";
            bufferRC += "\tSWITCH(target)\r\n";

            for (int i = 0; i < total; ++i)
            {
                string swibuffer = "";


                if (MissionBindsConsistant[i])
                {

                    if (Missions[i].bRCMission){
                        ++swcountrc;
                    }else{
                        ++swcount;
                    }

                    //set lud.m_LeaderboardId to the value of the leaderboard

                    //lud.m_LeaderboardId = 
                    //LEADERBOARDS2_WRITE_DATA(lud)

                    //LEADERBOARDS_WRITE_SET_INT(INT columnIndex, INT value)

                    //LEADERBOARDS_WRITE_FLUSH()



                    //find both of the leaderboards so we can use the ordering

                    Leaderboard fa = null;
                    Leaderboard fa2 = null;
                    foreach (Leaderboard l in Leaderboards.GetLeaderboards())
                    {
                        if (fa == null)
                        {
                            if (l.m_LeaderboardId == Missions[i].LBLastID)
                            {
                                fa = l;
                            }
                        }else{
                            break;
                        }
                    }
                    foreach (Leaderboard l in Leaderboards.GetLeaderboards())
                    {
                        if (fa2 == null)
                        {
                            if (l.m_LeaderboardId == Missions[i].LBBestID)
                            {
                                fa2 = l;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    //f will be a valid lb now as this mission passed validation step previously
                    //done by column order
                    //stcrp.m_NumValues
                    //stcrp.m_ColValues[0]
                    int iCol = 0;

                    //swibuffer += "\t\t\tstcrp.m_NumValues = " + fa.getColumns().Count + "\r\n";
                    swibuffer += "\r\n\r\n";
                    swibuffer += "\t\tCASE " + Missions[i].FlowEnum + "\r\n\r\n";

                    //lud.m_LeaderboardId = 
                    //(lud)
                    swibuffer += "\t\t\tlud.m_LeaderboardId = " + ((int)Missions[i].LBLastID) + "\r\n";
                    swibuffer += "\t\t\tLEADERBOARDS2_WRITE_DATA(lud)\r\n\r\n";

                    

                    foreach (Leaderboard.Column c in fa.getColumns())
                    {
                        bool set = false;
                        //c.m_Name

                        //is it a default? "SCORE_COLUMN" "BEST_TIME" "REPLAYS" "FAILS" "PASSES"
                        //if so bind to globals
                        if (!set)
                        {
                            if (c.m_Name.CompareTo("SCORE_COLUMN") == 0)
                            {
                                //swibuffer += "\t\t\tstcrp.m_ColValues[" + iCol + "] = TO_FLOAT(g_LeaderboardLastMissionScore)\r\n";



                                swibuffer += "\t\t\tLEADERBOARDS_WRITE_SET_INT(" + iCol + ", g_LeaderboardLastMissionScore)\r\n";


                                set = true;

                                c.statCategoryLastBoundTo = MishStatStar.StatCategory.Unset;
                            }
                        }
                        if (!set)
                        {
                            if (c.m_Name.CompareTo("BEST_TIME") == 0)
                            {
                                //swibuffer += "\t\t\tstcrp.m_ColValues[" + iCol + "] = TO_FLOAT(g_LeaderboardLastMissionBestTime )\r\n";


                                swibuffer += "\t\t\tLEADERBOARDS_WRITE_SET_INT(" + iCol + ", g_LeaderboardLastMissionBestTime)\r\n";

                                set = true;

                                c.statCategoryLastBoundTo = MishStatStar.StatCategory.MISSION_STAT_TYPE_TOTALTIME;
                            }
                        }
                        if (!set)
                        {
                            if (c.m_Name.CompareTo("REPLAYS") == 0)
                            {
                                //swibuffer += "\t\t\tstcrp.m_ColValues[" + iCol + "] = TO_FLOAT(g_LeaderboardLastMissionPasses )\r\n";


                                swibuffer += "\t\t\tLEADERBOARDS_WRITE_SET_INT(" + iCol + ", g_LeaderboardLastMissionPasses)\r\n";

                                set = true;
                            }
                        }
                        if (!set)
                        {
                            if (c.m_Name.CompareTo("FAILS") == 0)
                            {
                                //swibuffer += "\t\t\tstcrp.m_ColValues[" + iCol + "] = TO_FLOAT(g_LeaderboardLastMissionFails)\r\n";

                                swibuffer += "\t\t\tLEADERBOARDS_WRITE_SET_INT(" + iCol + ", g_LeaderboardLastMissionFails)\r\n";


                                set = true;
                            }
                        }
                        if (!set)
                        {
                            if (c.m_Name.CompareTo("PASSES") == 0)
                            {
                                //swibuffer += "\t\t\tstcrp.m_ColValues[" + iCol + "] = TO_FLOAT(g_LeaderboardLastMissionReplays)\r\n";

                                swibuffer += "\t\t\tLEADERBOARDS_WRITE_SET_INT(" + iCol + ", g_LeaderboardLastMissionReplays)\r\n";


                                set = true;
                            }
                        }
                        //if not find the stat enum that matches it
                        //then find the value from the registers using that enum
                        //set it, done
                        if (!set)
                        {
                            
                            foreach(MishStatStar.MissionStat ms in Missions[i].MyStats)                            
                            {
                                if (c.m_Name.CompareTo(ms.lb_enumerant) == 0)
                                {
                                    //ms.enumname


                                    //GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(ms.enumname)

                                   // swibuffer += "\t\t\tstcrp.m_ColValues[" + iCol + "] = TO_FLOAT(GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(" + ms.enumname + "))\r\n";

                                    swibuffer += "\t\t\tLEADERBOARDS_WRITE_SET_INT(" + iCol + ", GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(" + ms.enumname + "))\r\n";


                                    set = true;


                                    c.statCategoryLastBoundTo = ms.Category;
                                    break;
                                }
                            }
                        }

                        ++iCol;
                    }



                    ////////////////////////////////////fa2

                    //only need to flush after both writes
                    //swibuffer += "\r\n\t\t\tLEADERBOARDS_WRITE_FLUSH()\r\n\r\n";
                    iCol = 0;
                    swibuffer += "\t\t\tlud.m_LeaderboardId = " + ((int)Missions[i].LBBestID) + "\r\n";
                    swibuffer += "\t\t\tLEADERBOARDS2_WRITE_DATA(lud)\r\n";
                    

                    foreach (Leaderboard.Column c in fa2.getColumns())
                    {
                        bool set = false;
                        //c.m_Name

                        //is it a default? "SCORE_COLUMN" "BEST_TIME" "REPLAYS" "FAILS" "PASSES"
                        //if so bind to globals
                        if (!set)
                        {
                            if (c.m_Name.CompareTo("SCORE_COLUMN") == 0)
                            {
                                //swibuffer += "\t\t\tstcrp.m_ColValues[" + iCol + "] = TO_FLOAT(g_LeaderboardLastMissionScore)\r\n";



                                swibuffer += "\t\t\tLEADERBOARDS_WRITE_SET_INT(" + iCol + ", g_LeaderboardLastMissionScore)\r\n";


                                set = true;

                                c.statCategoryLastBoundTo = MishStatStar.StatCategory.Unset;
                            }
                        }
                        if (!set)
                        {
                            if (c.m_Name.CompareTo("BEST_TIME") == 0)
                            {
                                //swibuffer += "\t\t\tstcrp.m_ColValues[" + iCol + "] = TO_FLOAT(g_LeaderboardLastMissionBestTime )\r\n";


                                swibuffer += "\t\t\tLEADERBOARDS_WRITE_SET_INT(" + iCol + ", g_LeaderboardLastMissionBestTime)\r\n";

                                set = true;

                                c.statCategoryLastBoundTo = MishStatStar.StatCategory.MISSION_STAT_TYPE_TOTALTIME;
                            }
                        }
                        if (!set)
                        {
                            if (c.m_Name.CompareTo("REPLAYS") == 0)
                            {
                                //swibuffer += "\t\t\tstcrp.m_ColValues[" + iCol + "] = TO_FLOAT(g_LeaderboardLastMissionPasses )\r\n";


                                swibuffer += "\t\t\tLEADERBOARDS_WRITE_SET_INT(" + iCol + ", g_LeaderboardLastMissionPasses)\r\n";

                                set = true;
                            }
                        }
                        if (!set)
                        {
                            if (c.m_Name.CompareTo("FAILS") == 0)
                            {
                                //swibuffer += "\t\t\tstcrp.m_ColValues[" + iCol + "] = TO_FLOAT(g_LeaderboardLastMissionFails)\r\n";

                                swibuffer += "\t\t\tLEADERBOARDS_WRITE_SET_INT(" + iCol + ", g_LeaderboardLastMissionFails)\r\n";


                                set = true;
                            }
                        }
                        if (!set)
                        {
                            if (c.m_Name.CompareTo("PASSES") == 0)
                            {
                                //swibuffer += "\t\t\tstcrp.m_ColValues[" + iCol + "] = TO_FLOAT(g_LeaderboardLastMissionReplays)\r\n";

                                swibuffer += "\t\t\tLEADERBOARDS_WRITE_SET_INT(" + iCol + ", g_LeaderboardLastMissionReplays)\r\n";


                                set = true;
                            }
                        }
                        //if not find the stat enum that matches it
                        //then find the value from the registers using that enum
                        //set it, done
                        if (!set)
                        {
                            
                            foreach(MishStatStar.MissionStat ms in Missions[i].MyStats)                            
                            {
                                if (c.m_Name.CompareTo(ms.lb_enumerant) == 0)
                                {
                                    //ms.enumname


                                    //GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(ms.enumname)

                                   // swibuffer += "\t\t\tstcrp.m_ColValues[" + iCol + "] = TO_FLOAT(GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(" + ms.enumname + "))\r\n";

                                    swibuffer += "\t\t\tLEADERBOARDS_WRITE_SET_INT(" + iCol + ", GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(" + ms.enumname + "))\r\n";


                                    set = true;


                                    c.statCategoryLastBoundTo = ms.Category;
                                    break;
                                }
                            }
                        }

                        ++iCol;
                    }


                   // swibuffer += "\r\n\t\t\tLEADERBOARDS_WRITE_FLUSH()\r\n";

                    swibuffer += "\t\tBREAK\r\n\r\n";


                }



                if (Missions[i].bRCMission)
                {
                 
                    if (swcountrc > 10 && !(i > (total - 4)))
                    {
                        swcountrc = 0;
                        //reset the switch
                        swibuffer += "\tENDSWITCH\r\n";
                        swibuffer += "\tSWITCH(target)\r\n";

                    }

                    bufferRC += swibuffer;
                }else{

                    
                    if (swcount > 10 && !(i > (total - 4)))
                    {
                        swcount = 0;
                        //reset the switch
                        swibuffer += "\tENDSWITCH\r\n";
                        swibuffer += "\tSWITCH(target)\r\n";

                    }

                    buffer += swibuffer;
                }
                

            }
            buffer += "\tENDSWITCH\r\n";
            bufferRC += "\tENDSWITCH\r\n";

            //buffer += "\tINT bestID = 0 \r\n";
            //buffer += "\tINT lastID = 0 \r\n";

 
            buffer += "ENDPROC//GENERATED_TRY_WRITE_CURRENT_STAT_REGISTERS_TO_LEADERBOARD\r\n";

            bufferRC += "ENDPROC//GENERATED_TRY_WRITE_CURRENT_STAT_REGISTERS_TO_LEADERBOARDRC\r\n";
            


            buffer += "\r\n";

            if (MissionBindsConsistantCountRC != 0)
            {
                buffer += bufferRC;
            }
            else
            {
                //add placeholder for RC
                buffer += "PROC GENERATED_TRY_WRITE_CURRENT_STAT_REGISTERS_TO_LEADERBOARDRC(g_eRC_MissionIDs target)\r\n";
                buffer += "g_eRC_MissionIDs t = target //placholder with no binds\r\n";
                buffer += "t = t //placholder with no binds\r\n";
                buffer += "ENDPROC\r\n";
            }

           

            FileStream f = new FileStream(target, FileMode.Create);
            StreamWriter w = new StreamWriter(f);
            w.Write(buffer);
            w.Close();*/
        
        }

        private string getInputForBoardColumn(uint leaderboard, string column, ref bool failed)
        {
            foreach(Leaderboard l in ScsGameConfigObject.Instance.m_config.LeaderboardsConfiguration.Leaderboards)
            {
                if (uint.Parse(l.Id) == leaderboard)
                {
                   // int cid = -1;
                    Column clm = null;
                    Leaderboard lb = l;

                    string col = column;

                    if (col.CompareTo("SPECIAL_ABILITY_USE") == 0)
                    {
                        col = "SPECIAL_ABILITY_TIME";
                    }
                    if (col.CompareTo("LESTER_ACCURACY") == 0)
                    {
                        col = "ACCURACY";
                    }
                    foreach (Column c in lb.Columns)
                    {
                        if (c.Name.CompareTo(col) == 0)
                        {
                            clm = c;
                        }
                    }


                    if (clm == null)
                    {
                        failed = true;
                        return "FAIL!";
                    }
                    
                    
                    List<Input> inp = new List<Input>(ScsGameConfigObject.Instance.GetInputsOnLeaderboard(lb));
                    if (inp != null)
                    {
                        int iv = int.Parse(clm.Ordinal);

                        return inp[iv].Name;
                    }
                    
                    /*
                    if (clm != null && inp != null)
                    {
                        uint inpid = uint.Parse(clm.Aggregation.GameInput.InputId);

                        foreach (Input i in inp)
                        {
                            if (uint.Parse(i.Id) == inpid)
                            {
                                return i.Name;//LB_INPUT_
                            }
                        }
                    }
                    */
                    failed = true;
                    return "!!ERROR!!INPUTS NOT FOUND!!";
                }
            }
            failed = true;
            return "!!ERROR!!LEADERBOARD NOT FOUND!!";
        }

        private string getMissionStaticLBBinds(MishStatStar.MissionEntry me)
        {

           // return "\tLEADERBOARDS_WRITE_ADD_COLUMN(SCORE_COLUMN, g_LeaderboardLastMissionScore,0.0)\n";
            //"\tLEADERBOARDS_WRITE_SET_INT(2, g_LeaderboardLastMissionBestTime)\n";
            //"\tLEADERBOARDS_WRITE_SET_INT(3, g_LeaderboardLastMissionPasses)\n" +
            //"\tLEADERBOARDS_WRITE_SET_INT(4, g_LeaderboardLastMissionFails)\n" +
            //"\tLEADERBOARDS_WRITE_SET_INT(5, g_LeaderboardLastMissionReplays)\n";


            //LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE, g_LeaderboardLastMissionScore,0.0)
	        //LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_MISSION_SCORE, g_LeaderboardLastMissionScore,0.0)



            return "\t\tLEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE, g_LeaderboardLastMissionScore,0.0)\n" + "\tLEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_MISSION_SCORE, g_LeaderboardLastMissionScore_Derivs,0.0)\n" +
                    "\t\tLEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_PASSES, 1,0.0)\r\n" +
                    "\t\tLEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_OBJECTIVES_PASSED, g_LeaderboardLastObjectivesPassed,0.0)\r\n";
                
        }

        private string getColumnValueRead(MishStatStar.MissionStat ms)
        {
            return "GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS(" + ms.enumname + ")";
        }

        internal void exportMishNoVis(string target)
        {

            string buffer = "\r\n";







            FileStream f = new FileStream(target, FileMode.Create);
            StreamWriter w = new StreamWriter(f);
            w.Write(buffer);
            w.Close();
        }
        
        private Leaderboard getLeaderboardByName(string p)
        {
            foreach (Leaderboard lb in Leaderboards.GetLeaderboards())
            {
                if (lb.Name.CompareTo(p) == 0)
                {
                    return lb;
                }
            }
            return null;
        }
        private string makeXMLWebEntryForLeaderboard(Leaderboard lb, MishStatStar.MissionEntry me)
        {
            string buffer = "\r\n";
             
           
            



            buffer += "<Leaderboard Name=\"" + lb.Name + "\" Id=\"" + lb.Id + "\" GroupName=\"SPMissions\" Visible=\"true\" IsRanked=\"true\"> \r\n";
            buffer += "\t\t<ColumnGroups>\r\n";
            buffer += "\t\t\t<ColumnGroup>\r\n";
            buffer += "\t\t\t\t<Columns>\r\n";


            /*
            if (!lb.passedLastValidation)
            {
                buffer += "<!-- This leaderboard didn't bind to stats correctly. Types will be all default.  -->\r\n";
                buffer += "<!-- See LeaderboardBindWarnings.txt for details -->\r\n";
            }*/

            int ordering = 1;
            foreach(Column c in lb.Columns)
            {
               buffer += "\t\t\t\t\t";
               buffer += "<Column Name=\"" + c.Name + "\" Ordering=\"" + ordering + "\" ";


               /*
                   Speed - "Meters per sec to MPH"
                    Time - "TIME (millisec)
                    Accuracy - "PERCENTAGE"
                */
               if (c.Name.CompareTo("SCORE_COLUMN") != 0)
               {

                   buffer += "ValueID=\"" + c.Id + "\" ";

               }

                switch(c.statCategoryLastBoundTo)
                {
                    case MishStatStar.StatCategory.MISSION_STAT_TYPE_TOTALTIME:
                    case MishStatStar.StatCategory.MISSION_STAT_TYPE_WINDOWED_TIMER:
                    case MishStatStar.StatCategory.MISSION_STAT_TYPE_SPECIAL_ABILITY_USE:
                    case MishStatStar.StatCategory.MISSION_STAT_TYPE_ACTION_CAM_USE:
                        buffer += "DataType=\"TIME (millisec)\" ";
                        break;
                    case MishStatStar.StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD:
                         buffer += "DataType=\"Meters per sec to MPH\" ";    
                        break;
                    case MishStatStar.StatCategory.MISSION_STAT_TYPE_ACCURACY:
                    case MishStatStar.StatCategory.MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE:
                        buffer += "DataType=\"PERCENTAGE\" ";    
                        break;

                    case MishStatStar.StatCategory.MISSION_STAT_TYPE_FINANCE_DIRECT:
                    case MishStatStar.StatCategory.MISSION_STAT_TYPE_FINANCE_TABLE:
                        buffer += "DataType=\"MONEY\" ";
                        break;
                    default:
                        buffer += "DataType=\"INT\" ";
                        break;
                }


               

               if (c.Name.CompareTo("SCORE_COLUMN") == 0)
               {

                   buffer += "RankedCol=\"true\""; 

               }
               buffer += "/> \r\n";


                ++ordering;
            }


            buffer += "\t\t\t\t</Columns>\r\n";
            buffer += "\t\t\t</ColumnGroup>\r\n";
            buffer += "\t\t</ColumnGroups>\r\n";
            buffer += "</Leaderboard> \r\n";





           // lb.getColumns()



            

            return buffer;
        }

        internal void exportLBWebBindXML(string target)
        {
            string buffer = "\r\n";

            buffer += "<?xml version=\"1.0\" encoding=\"utf-16\" ?>\r\n";
            buffer += "<Leaderboards>\r\n";


            //a list of leaderboard index bindings for the web for simon
            //foreach (Leaderboard lb in Leaderboards.GetLeaderboards())


            foreach(MishStatStar.MissionEntry me in Missions)
            {
                if (me.LBBest != null)
                {
                    Leaderboard lb = getLeaderboardByName(me.LBBest);
                    if(lb != null)
                    {
                        //try to make an xml entry for it
                        buffer += makeXMLWebEntryForLeaderboard(lb, me);
                    }

                }
                if (me.LBLast != null)
                {
                    Leaderboard lb = getLeaderboardByName(me.LBLast);
                    if (lb != null)
                    {
                        //try to make an xml entry for it
                        buffer += makeXMLWebEntryForLeaderboard(lb, me);
                    }
                }

            }

            buffer += "</Leaderboards>";







            FileStream f = new FileStream(target, FileMode.Create);
            StreamWriter w = new StreamWriter(f);
            w.Write(buffer);
            w.Close();
        }






        internal void exportTestEntriesToFile(string target)
        {

            string buffer = "\r\n";



            foreach (MishStatStar.MissionEntry me in Missions)
            {
                bool bMishshown = false;
                foreach (MishStatStar.MissionStat ms in me.MyStats)
                {
                    if (!ms.Hidden)
                    {
                        if (!bMishshown)
                        {
                            bMishshown = true;
                            
                            buffer += me.FlowEnum +"\r\n";
                        }

                        buffer += "\t" +  ms.enumname + " : \"" + ms.overrideString + "\"\r\n";
                        buffer += "\t\tTarget : " + ms.targetValue + " - ";
                            buffer += "Base " + ms.baseValue + " - ";

                            if (ms.lessThanTarget)
                            {buffer += "Passes when less than target";} else{buffer += "Passes when equal or greater than target";}
                        buffer += "\r\n";

                        buffer += "\t\tDescription: " + ms.shortdesc;
                        buffer += "\r\n";

                    }
                }
            }




            FileStream f = new FileStream(target, FileMode.Create);
            StreamWriter w = new StreamWriter(f);
            w.Write(buffer);
            w.Close();
        }

        public string CategoryToScoreType(MishStatStar.StatCategory category)
        {
            switch (category)
            {
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_ACCURACY:
                    return "Accuracy";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_FINANCE_DIRECT:
                    return "Money";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_FINANCE_TABLE:
                    return "Money";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_FRACTION:
                    return "Fraction";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_HEADSHOTS:
                    return "Fraction";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_PURE_COUNT:
                    return "Number";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE:
                    return "Percentage";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD:
                    return "Bool";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD:
                    return "Percentage";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_TOTALTIME:
                    return "Time";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_UNIQUE_BOOL:
                    return "Bool";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_WINDOWED_TIMER:
                    return "Time";
                default:
                    return "Number";
            }
        }

        public string CategoryToScoreTypeForLB(MishStatStar.StatCategory category)
        {
            switch (category)
            {
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_ACCURACY:
                    return "PERCENTAGE";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_FINANCE_DIRECT:
                    return "MONEY";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_FINANCE_TABLE:
                    return "MONEY";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_FRACTION:
                    return "INT";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_HEADSHOTS:
                    return "INT";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_PURE_COUNT:
                    return "INT";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE:
                    return "PERCENTAGE";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD:
                    return "HEALTH|VEHICLE DAMAGE PERCENT";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD:
                    return "Meters per sec to MPH";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_TOTALTIME:
                    return "TIME (millisec)";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_UNIQUE_BOOL:
                    return "ENUM BOOL";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_WINDOWED_TIMER:
                    return "TIME (millisec)";
                default:
                    return "INT";
            }
        }

        public string CategoryToIconType(MishStatStar.StatCategory category)
        {
            switch (category)
            {
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_ACCURACY:
                    return "gtavicon-accuracy";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_FINANCE_DIRECT:
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_FINANCE_TABLE:
                    return "gtavicon-icon_dollar";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_FRACTION:
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_PURE_COUNT:
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_UNIQUE_BOOL:
                    return "";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_HEADSHOTS:
                    return "gtavicon-headshots";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE:
                    return "PERCENTAGE";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD:
                    return "gtavicon-car_crashes|gtavicon-health_damage_taken";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD:
                    return "gtavicon-maxspeed";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_TOTALTIME:
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_WINDOWED_TIMER:
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_SPECIAL_ABILITY_USE:
                    return "gtavicon-timetaken";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_INNOCENTS_KILLED:
                    return "gtavicon-innocents_killed";
                case MishStatStar.StatCategory.MISSION_STAT_TYPE_BULLETS_FIRED:
                    return "gtavicon-bullets_fired";
                default:
                    return "";
            }
        }

        // This exports all of the mission stat data and Personal Best stats to a text file to make it easy to include in Social Club
        internal void exportSCDevEntriesToFile(string target)
        {
            string buffer = "\r\n";
            foreach (MishStatStar.MissionEntry me in Missions)
            {
                bool bMishshown = false;

                // Write mission objective data
                foreach (MishStatStar.MissionStat ms in me.MyStats.Where(x => !x.Hidden))
                {
                    if (!bMishshown)
                    {
                        bMishshown = true;

                        buffer += me.FlowEnum + "\r\n\r\n";
                        buffer += "MISSION OBJECTIVES\r\n";
                    }

                    var lessThan = ms.lessThanTarget ? "True" : "False";

                    buffer += "<StatItem Name=\"MS_" + ms.enumname.Trim() + "\" Comment=\"" + ms.overrideString.Trim() + "\" Objective=\"" + ms.igDesc.Trim() + "\" Target=\"" + ms.targetValue + "\" LessThan=\"" + lessThan + "\" ScoreType=\"" + CategoryToScoreType(ms.Category) + "\" />";
                    buffer += "\r\n";
                }

                buffer += "\r\n";

                // Write "Personal Bests" LB stat data
                buffer += "PERSONAL BESTS\r\n";
                foreach (MishStatStar.MissionStat ms in me.MyStats)
                {
                    if (!bMishshown)
                    {
                        bMishshown = true;

                        buffer += me.FlowEnum + "\r\n";
                    }

                    var name = ms.overrideString.Trim() != "" ? ms.overrideString.Trim() : ms.lb_enumerant;

                    buffer += "<StatItem Name=\"" + name + "\" ColumnName=\"" + ms.lb_enumerant + "\" Text=\"" + ms.lb_enumerant + "\" Icon=\"" + CategoryToIconType(ms.Category) + "\" />";
                    buffer += "\r\n";
                }

                buffer += "\r\n";
            }

            FileStream f = new FileStream(target, FileMode.Create);
            StreamWriter w = new StreamWriter(f);
            w.Write(buffer);
            w.Close();
        }

        /// <summary>
        /// This exports all of the binded LB data to a file as XML so it can be easily imported into Social Club.
        /// </summary>
        /// <param name="target"></param>
        internal void exportSCDevLBEntriesToFile(string target)
        {
            string buffer = "\r\n";
            foreach (MishStatStar.MissionEntry me in Missions)
            {
                // EXPORT BEST_RUN LB DATA

                buffer += string.Format("\t<Leaderboard Name=\"{0}\" Id=\"{1}\" GroupName=\"SPMissions\" Visible=\"true\" IsRanked=\"true\">\r\n",me.LBBest,me.LBBestID);
                buffer += "\t\t<ColumnGroups>\r\n";
                buffer += "\t\t\t<ColumnGroup>\r\n";
                buffer += "\t\t\t\t<Columns>\r\n";
                buffer += "\t\t\t\t\t<Column Name=\"SCORE_COLUMN\" Hidden=\"true\" DataType=\"INT\" RankedCol=\"true\"/>\r\n";

                // Write mission objective data
                buffer += "\t\t\t\t\t<!-- Mission Objectives -->\r\n";
                foreach (MishStatStar.MissionStat ms in me.MyStats.Where(x => !x.Hidden))
                {
                    var diff = ms.lb_differentiator ? ms.lb_weight_PPC.ToString() : "0";
                    var error = ms.lb_enumerant == "" ? "MISSING BIND" : "";
                    buffer += string.Format("{5}\t\t\t\t\t<Column Name=\"{0}\" ValueID=\"{1}\" DataType=\"{2}\" StatName=\"{3}\" Objective=\"True\" Differentiator=\"{4}\" />\r\n", ms.lb_enumerant, ms.lb_columnID, CategoryToScoreTypeForLB(ms.Category), ms.enumname, diff, error);
                }

                // Write differentiator stat data
                buffer += "\t\t\t\t\t<!-- Differentiator Stats -->\r\n";
                foreach (MishStatStar.MissionStat ms in me.MyStats.Where(x => x.Hidden && x.lb_differentiator))
                {
                    var error = ms.lb_enumerant == "" ? "MISSING BIND" : "";
                    buffer += string.Format("{4}\t\t\t\t\t<Column Name=\"{0}\" Hidden=\"true\" ValueID=\"{1}\" DataType=\"{2}\" Differentiator=\"{3}\" />\r\n", ms.lb_enumerant, ms.lb_columnID, CategoryToScoreTypeForLB(ms.Category), ms.lb_weight_PPC, error);
                }

                // Add the 3 special stats
                buffer += "\t\t\t\t\t<!-- Misc Stats -->\r\n";
                var lbs = Leaderboards.GetLeaderboards();
                var thisMissionLb = lbs.FirstOrDefault(x => x.Name == me.LBBest);
                if (thisMissionLb != null)
                {
                    var col = thisMissionLb.Columns.FirstOrDefault(x => x.Name == "PASSES");

                    if (col != null)
                    {
                        buffer += string.Format("\t\t\t\t\t<Column Name=\"PASSES\" Hidden=\"true\" ValueID=\"{0}\" DataType=\"INT\" Differentiator=\"0\" />\r\n", col.Id);
                    }

                    col = thisMissionLb.Columns.FirstOrDefault(x => x.Name == "OBJECTIVES");

                    if (col != null)
                    {
                        buffer += string.Format("\t\t\t\t\t<Column Name=\"OBJECTIVES\" Hidden=\"true\" ValueID=\"{0}\" DataType=\"INT\" Differentiator=\"0\" />\r\n", col.Id);
                    }

                    col = thisMissionLb.Columns.FirstOrDefault(x => x.Name == "MISSION_SCORE");

                    if (col != null)
                    {
                        buffer += string.Format("\t\t\t\t\t<Column Name=\"MISSION_SCORE\" Hidden=\"true\" ValueID=\"{0}\" DataType=\"INT\" Differentiator=\"0\" />\r\n", col.Id);
                    }
                }

                buffer += "\t\t\t\t</Columns>\r\n";
                buffer += "\t\t\t</ColumnGroup>\r\n";
                buffer += "\t\t</ColumnGroups>\r\n";
                buffer += "\t</Leaderboard>\r\n";
                
                buffer += "\r\n";

                // Export AGGREGATE LB Data
                
                buffer += string.Format("\t<Leaderboard Name=\"{0}\" Id=\"{1}\" GroupName=\"SPMissions\" Visible=\"true\" IsRanked=\"true\">\r\n", me.LBLast, me.LBLastID);
                buffer += "\t\t<ColumnGroups>\r\n";
                buffer += "\t\t\t<ColumnGroup>\r\n";
                buffer += "\t\t\t\t<Columns>\r\n";
                buffer += "\t\t\t\t\t<Column Name=\"SCORE_COLUMN\" Hidden=\"true\" DataType=\"INT\" RankedCol=\"true\"/>\r\n";

                // Write mission objective data
                foreach (MishStatStar.MissionStat ms in me.MyStats)
                {
                    buffer += string.Format("\t\t\t\t\t<Column Name=\"{0}\" ValueID=\"{1}\" DataType=\"{2}\" />\r\n", ms.lb_enumerant, ms.lb_columnID, CategoryToScoreTypeForLB(ms.Category));
                }

                buffer += "\t\t\t\t</Columns>\r\n";
                buffer += "\t\t\t</ColumnGroup>\r\n";
                buffer += "\t\t</ColumnGroups>\r\n";
                buffer += "\t</Leaderboard>\r\n";

                buffer += "\r\n";
            }

            FileStream f = new FileStream(target, FileMode.Create);
            StreamWriter w = new StreamWriter(f);
            w.Write(buffer);
            w.Close();
        }

        /// <summary>
        /// This exports all of the binded LB data to a file as XML so it can be easily imported into Social Club.
        /// </summary>
        /// <param name="target"></param>
        internal void validateStats(string target)
        {
            string buffer = "";
            var totalErrors = 0;
            // For each mission
            foreach (MishStatStar.MissionEntry me in Missions)
            {
                int errors = 0;
                var totalStats = me.MyStats.Count();
                var objectiveStats = me.MyStats.Count(x => !x.Hidden);
                var differentiatorStats = me.MyStats.Count(x => x.Hidden && x.lb_differentiator);

                buffer += string.Format("{0} has {1} stats ({2} Objectives and {3} Differentiators).", me.FlowEnum, totalStats, objectiveStats, differentiatorStats) + "\r\n";

                var bestrunBinded = me.LBBest != "";
                var bestrun = bestrunBinded ? me.LBBest + " (" + me.LBBestID + ")" : "NOT BINDED!!!";
                buffer += string.Format("BEST_RUN LB: {0}", bestrun) + "\r\n";
                var aggregateBinded = me.LBLast != "";
                var aggregate = aggregateBinded ? me.LBLast + " (" + me.LBLastID + ")" : "NOT BINDED!!!";
                buffer += string.Format("AGGREGATE LB: {0}", aggregate) + "\r\n\r\n";

                if (!bestrunBinded || !aggregateBinded)
                {
                    errors++;
                    totalErrors++;
                }

                // Check each stat
                foreach (MishStatStar.MissionStat ms in me.MyStats.Where(x => !x.Hidden))
                {
                    var statBinded = ms.lb_enumerant != "";
                    var binded =  statBinded ? "binded to " + ms.lb_enumerant + " (" + ms.lb_columnID + ")" : "NOT BINDED!!!";
                    buffer += string.Format("OBJ STAT: \"{0}\" {1}", ms.enumname, binded) + "\r\n";
                    if (!statBinded)
                    {
                        errors++;
                        totalErrors++;
                    }
                }

                // And check each differentiator stat
                foreach (MishStatStar.MissionStat ms in me.MyStats.Where(x => x.Hidden && x.lb_differentiator))
                {
                    var statBinded = ms.lb_enumerant != "";
                    var binded = statBinded ? "binded to " + ms.lb_enumerant + " (" + ms.lb_columnID + ")" : "NOT BINDED!!!";
                    buffer += string.Format("DIF STAT: \"{0}\" {1}", ms.enumname, binded) + "\r\n";
                    if (!statBinded)
                    {
                        errors++;
                        totalErrors++;
                    }
                }

                buffer += "\r\n";
                var errorChecking = errors > 0 ? "BINDING ERRORS. SEE ABOVE." : "SUCCESS: All stats for this mission are binded!";
                buffer += errorChecking + "\r\n\r\n";
            }

            var totalerrorChecking = totalErrors > 0 ? "BINDING ERRORS. Breakdown below." : "SUCCESS: All stats are binded! Breakdown below.";

            buffer = "############################################################\r\n# " + totalerrorChecking + "\r\n############################################################\r\n\r\n" + buffer;

            FileStream f = new FileStream(target, FileMode.Create);
            StreamWriter w = new StreamWriter(f);
            w.Write(buffer);
            w.Close();
        }

    }
}
