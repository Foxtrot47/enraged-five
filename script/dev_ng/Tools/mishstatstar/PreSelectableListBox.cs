﻿
using System;
using System.Windows.Forms;
namespace MissionStatsStar
{


    class PreSelectableListBox : ListBox
    {
        private const int WM_LBUTTONDOWN = 0x201;
        private const int WM_LBUTTONUP = 0x0202;

        public event EventHandler PreSelect;
        bool downlastframe = false;
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_LBUTTONDOWN:
                    OnPreSelect();
                    break;
                case WM_LBUTTONUP:
                    downlastframe = false;
                    break;
            }

            base.WndProc(ref m);
        }

        protected void OnPreSelect()
        {
            if(downlastframe)
            {

                return;
            }


            if (null != PreSelect)
                PreSelect(this, new EventArgs());

            downlastframe = true;
        }

    }

}