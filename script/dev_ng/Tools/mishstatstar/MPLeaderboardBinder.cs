﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MissionStatsStar
{
    class MPLeaderboardBinder // 
    {
        /*
        private List<Leaderboard> lbs;
        private List<Leaderboard.Column> cols;

        public MPLeaderboardBinder()
        {
            lbs = new List<Leaderboard>();
            cols = new List<Leaderboard.Column>();
        }

        internal object AddLB(Leaderboard lb)
        {
            lbs.Add(lb);
            return lb;//whatever you return gets put in the listbox
        }

        internal object AddCOL(Leaderboard.Column cb)
        {
            cols.Add(cb);
            return cb;//whatever you return gets put in the listbox
        }

        internal void RemoveLB(object p)
        {
            if (lbs.Contains(p))
            {
                lbs.Remove((Leaderboard)p);
            }
        }

        internal void RemoveCOL(object p)
        {
            if (cols.Contains(p))
            {
                cols.Remove((Leaderboard.Column)p);
            }
        }





        public struct scriptArg
        {
            public string type;
            public string name;
            public uint colID;
        };

        private List<scriptArg> makeArgsForBoard(Leaderboard l)
        {
            List<scriptArg> args = new List<scriptArg>();

            bool bvalid = false;
            foreach(Leaderboard.Column c in l.getColumns())
            {


                scriptArg arg = new scriptArg();
                switch (c.m_LbProp.m_Type)
                {
                    case DataTypes.XUSER_DATA_TYPE_INT64:
                    case DataTypes.XUSER_DATA_TYPE_INT32:
                        bvalid = true;
                        arg.type = "INT";
                        break;
                    case DataTypes.XUSER_DATA_TYPE_FLOAT:
                    case DataTypes.XUSER_DATA_TYPE_DOUBLE:
                        bvalid = true;
                        arg.type = "FLOAT";
                        break;

                }

                arg.colID = c.m_Id;
                //make sure the arg isn't on the invalidation list
                foreach (Leaderboard.Column co in cols)
                {
                    if (co.m_Name.CompareTo(c.m_Name) == 0)
                    {
                        bvalid = false;
                    }
                }

                if (bvalid)
                {
                    arg.name = c.m_Name;
                    args.Add(arg);
                }

            }





            return args;
        }

        internal void ExportScriptHeader(string target)
        {
            string buffer = "//MSS MP Leaderboard write auto generated script header\r\n\r\n";


            foreach (Leaderboard l in lbs)
            {

                List<scriptArg> callArgs = makeArgsForBoard(l);

                buffer += "//Writer function for "+ l.m_Name +" \r\n";
                buffer += "PROC LEADERBOARD_WRITE_TO_" + l.m_Name + "(";


                int nth = callArgs.Count();
                foreach (scriptArg a in callArgs)
                {
                    buffer += a.type + " " + a.name;

                    if (nth > 1)
                    {
                        buffer += ",";
                    }
                    --nth;
                }

                buffer += ")\r\n\r\n";


                buffer += "\tLeaderboardUpdateData lbrd\r\n";
                buffer += "\tlbrd.m_LeaderboardId = " + l.m_LeaderboardId + "\r\n";
                buffer += "\tLEADERBOARDS2_WRITE_DATA(lbrd)\r\n";



                foreach (scriptArg a in callArgs)
                {
                    if (a.type.CompareTo("FLOAT") == 0)
                    {
                        //LEADERBOARDS_WRITE_SET_FLOAT
                        buffer += "\tLEADERBOARDS_WRITE_SET_FLOAT(";
                    }
                    else if (a.type.CompareTo("INT") == 0)
                    {
                        //LEADERBOARDS_WRITE_SET_INT
                        buffer += "\tLEADERBOARDS_WRITE_SET_INT(";
                    }
                    buffer += a.colID + "," + a.name;
                    buffer += ")\r\n";
                }
                buffer += "\r\n\r\nENDPROC\r\n\r\n";



            }








            FileStream f = new FileStream(target, FileMode.Create);
            StreamWriter w = new StreamWriter(f);
            w.Write(buffer);
            w.Close();
        }



        internal void SaveMPBinds(BinaryWriter bw)
        {
            bw.Write(lbs.Count());
            bw.Write(cols.Count());

            foreach (Leaderboard l in lbs)
            {
                bw.Write(l.m_Name);
                bw.Write(l.m_LeaderboardId);
            }
            foreach (Leaderboard.Column c in cols)
            {
                bw.Write(c.m_Name);
                bw.Write(c.m_Id);
            }



        }

        internal void LoadMPBinds(BinaryReader br, LeaderboardCollection leaderboards)
        {
            lbs.Clear();
            cols.Clear();

            //read the totals
            int lnum = br.ReadInt32();
            int cnum = br.ReadInt32();

            //load the data
            for(int i = 0;i < lnum;++i)
            {
                //brute force lookup because we don't care if this is slow
                string name = br.ReadString();
                uint id = br.ReadUInt32();

                foreach (Leaderboard l in leaderboards.GetLeaderboards())
                {
                    if (l.m_LeaderboardId == id && name.CompareTo(l.m_Name) == 0)
                    {
                        lbs.Add(l);
                        break;
                    }
                }

            }
            for(int i = 0;i < cnum;++i)
            {
                string name = br.ReadString();
                uint id = br.ReadUInt32();

                bool bfound = false;
                foreach (Leaderboard l in leaderboards.GetLeaderboards())
                {
                    foreach (Leaderboard.Column c in l.getColumns())
                    {
                        if (c.m_Id == id && name.CompareTo(c.m_Name) == 0)//
                        {
                            cols.Add(c);
                            bfound = true;
                            break;
                        }
   
                    }
                    if (bfound)
                    {
                        break;
                    }
                }


            }



            


        }

        internal void RefreshMyLists(System.Windows.Forms.ListBox Boards,
                                     System.Windows.Forms.ListBox Cols)
        {
            Boards.Items.Clear();
            Cols.Items.Clear();

            foreach (Leaderboard l in lbs)
            {
                Boards.Items.Add(l);
            }
            foreach (Leaderboard.Column c in cols)
            {
                Cols.Items.Add(c);
            }

        }*/
    }
}
