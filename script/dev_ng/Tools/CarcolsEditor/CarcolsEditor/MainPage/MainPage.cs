﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using CarcolsEditor.Data;
using CarcolsEditor.Logs;
using CarcolsEditor.Managers;
using System.Xml.Serialization;
using CarcolsEditor.HelperLibs;

namespace CarcolsEditor
{
    public partial class MainPage : Form
    {
        private bool _editingMode = false;
        private OpenFileDialog ofd;

        public MainPage()
        {
            InitializeComponent();
            InitMainPanels();
            VehicleModManager.Instantiate();
            VehiclePanelManager.Instantiate(vehicleMainPanel, modMainPanel, resources);
            CarcolsManager.Instantiate();
            Program.MainTick.Tick += MainUpdate;
        }

        #region Updates
        private void MainUpdate(object sender, EventArgs e)
        {
            VehicleModManager.Instance?.Update();
            VehiclePanelManager.Instance?.Update();

            TriggerEditMode();
            UpdateEditModBtAndLabelName();
            UpdatePreviewTextBox();
        }

        private void UpdatePreviewTextBox()
        {
            if(VehicleModManager.Instance.UpdatePreviewPanel)
            {
                UpdatePreviewPanel();
                VehicleModManager.Instance.UpdatePreviewPanel = false;
            }
        }

        /// <summary>
        /// Update Bt name and labels whe Edit mode is active
        /// </summary>
        private void UpdateEditModBtAndLabelName()
        {
            if (_editingMode)
            {
                if (VehicleModManager.Instance.EditingVehiclePanel)
                {
                    if (createNewCarBt != null)
                    {
                        if (createNewCarBt.Text != "Save Mod Changes")
                        {
                            createNewCarBt.Text = "Save Mod Changes";
                        }
                    }

                    if (ModMainLabel != null)
                    {
                        if (CarcolsManager.Instance.Kits.Count > 0)
                        {
                            string EditingVehicleName = "Edit " + CarcolsManager.Instance.GetCarcolsVehicleName(VehicleModManager.Instance.EditingVehicleIndex) + " Mods";
                            if (ModMainLabel.Text != EditingVehicleName)
                            {
                                ModMainLabel.Text = EditingVehicleName;
                                ControlPositioning.CenterHorizontally(ModMainLabel);
                            }
                        }
                    }

                    if(AddToModLibBT != null)
                    {
                        AddToModLibBT.Visible = true;
                    }
                }
                else
                {
                    if (createNewCarBt.Text != "Create New Vehicle")
                    {
                        createNewCarBt.Text = "Create New Vehicle";
                    }
                    if (ModMainLabel.Text != "Mod Library")
                    {
                        ModMainLabel.Text = "Mod Library";
                        ControlPositioning.CenterHorizontally(ModMainLabel);
                    }
                }
                if (saveModLibraryBt != null)
                {
                    saveModLibraryBt.Visible = false;
                }
                if (addToExistingVehicle != null)
                {
                    addToExistingVehicle.Visible = false;
                }
            }
            else
            {
                if (createNewCarBt.Text != "Create New Vehicle")
                {
                    createNewCarBt.Text = "Create New Vehicle";
                }
                if (ModMainLabel.Text != "Mod Library")
                {
                    ModMainLabel.Text = "Mod Library";
                    ControlPositioning.CenterHorizontally(ModMainLabel);
                }
                if (saveModLibraryBt != null)
                {
                    saveModLibraryBt.Visible = true;
                }
                if (addToExistingVehicle != null)
                {
                    addToExistingVehicle.Visible = true;
                }
                if (AddToModLibBT != null)
                {
                    AddToModLibBT.Visible = false;
                }
            }
        }

        #endregion

        #region Methods
        private void TriggerEditMode()
        {
            if (VehicleModManager.Instance.EditingMode)
            {
                if (VehicleModManager.Instance.EditingVehiclePanel && !VehicleModManager.Instance.EditingModPanel)
                {
                    _editingMode = true;
                    VehicleModManager.Instance.CreateVehicleData();
                    VehicleModManager.Instance.CreateVehicleModData();

                    // Lets make a copy of our vehicle mod list
                    List<ModData> ListOfMods = new List<ModData>(CarcolsManager.Instance.Kits[VehicleModManager.Instance.EditingVehicleIndex].GetVehicleModTypeList());
                    VehicleModManager.Instance.SetVehicleManagerVehicleList(ListOfMods);
                    VehiclePanelManager.Instance.RebuildAllModPanels();
                    VehicleModManager.Instance.EditingMode = false;
                    SetThisMainPanelToVisible(modMainPanel);
                    PrintLine.WriteLine("[MainPage] AddVehicleToCarcolsAndCleanup - called start edit vehicle panel");
                }
                else
                {
                    _editingMode = true;
                    VehicleModManager.Instance.SetupEditModData(VehicleModManager.Instance.GetVehicleModList()[VehicleModManager.Instance.EditingModIndex]);
                    modItemShopLabelTextBox.Text = VehicleModManager.Instance.GetModData().ModShopLabel;
                    modModelTextBox.Text = VehicleModManager.Instance.GetModData().ModModelName;
                    boneTextBox.Text = VehicleModManager.Instance.GetModData().Bone;
                    ModTypeDropDown.Text = VehicleModManager.Instance.GetModData().ModType;
                    cameraPositionTextBox.Text = VehicleModManager.Instance.GetModData().CameraPos;
                    CollisionBoneTextBox.Text = VehicleModManager.Instance.GetModData().CollisionBone;
                    audioApplyTextBox.Text = VehicleModManager.Instance.GetModData().AudioApply.Value;
                    weightTextBox.Text = VehicleModManager.Instance.GetModData().Weight.Value;
                    turnOffExtraComboBox.Text = VehicleModManager.Instance.GetModData().TurnOffExtra.Value.Equals(true) ? "true" : "false";
                    disableBonnetCameraComboBox.Text = VehicleModManager.Instance.GetModData().DisableBonnetCamera.Value.Equals(true) ? "true" : "false";
                    allowBonnetSlideComboBox.Text = VehicleModManager.Instance.GetModData().AllowBonnetSlide.Value.Equals(true) ? "true" : "false";
                    LinkedModelListView.Items.Clear();
                    TurnOffBoneListView.Items.Clear();
                    SetLinkedModelsFromListView(VehicleModManager.Instance.EditingModIndex);
                    SetTurnOffBonesFromListView(VehicleModManager.Instance.EditingModIndex);
                    VehicleModManager.Instance.EditingMode = false;
                    SetThisMainPanelToVisible(createNewModPanel);
                    PrintLine.WriteLine("[MainPage] AddVehicleToCarcolsAndCleanup - called start edit mod panel");
                }
            }
        }

        private void InitMainPanels()
        {
            // Bring dashboard panel to front
            SetThisMainPanelToVisible(dashboardMainPanel);

            // Show dashboard menu panel
            ShowThisMenuPanel(dashboardMenuPanel);
        }

        private void ShowThisMenuPanel(Panel thisPanel)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new MethodInvoker(() => ShowThisMenuPanel(thisPanel)));
            }
            else
            {
                dashboardMenuPanel.Hide();
                previewMenuPanel.Hide();
                vehicleMenuPanel.Hide();
                modsMenuPanel.Hide();
                thisPanel.Show();
            }
        }

        private void SetThisMainPanelToVisible(Panel thisPanel)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new MethodInvoker(() => SetThisMainPanelToVisible(thisPanel)));
            }
            else
            {
                dashboardMainPanel.Visible = false;
                previewMainPanel.Visible = false;
                vehicleMainPanel.Visible = false;
                createNewVehPanel.Visible = false;
                createNewModPanel.Visible = false;
                VehiclePanelQuickAddVeh.Visible = false;
                modMainPanel.Visible = false;
                ModPanelQuickAddMod.Visible = false;
                previewQuickPanel.Visible = false;

                if (thisPanel == vehicleMainPanel)
                {
                    VehiclePanelQuickAddVeh.Visible = true;
                }
                else if (thisPanel == modMainPanel)
                {
                    ModPanelQuickAddMod.Visible = true;
                }
                else if (thisPanel == previewMainPanel)
                {
                    previewQuickPanel.Visible = true;
                }

                thisPanel.Visible = true;
            }
        }

        private void AddVehicleToCarcolsAndCleanup()
        {
            PrintLine.WriteLine("[MainPage] AddVehicleToCarcolsAndCleanup - called _editingMode: " + _editingMode);

            VehicleData vehData = new VehicleData();
            vehData.ModKitName = modKitNametextBox.Text;
            vehData.ModKitType = ModKitTypeComboBox.Text;
            if (IdTextBox.Text == "")
            {
                vehData.Id.Value = 0;
            }
            else
            {
                vehData.Id.Value = Convert.ToInt32(IdTextBox.Text);
            }


            VehiclePanelManager.Instance.AddSelectedModsToModData(ref vehData.visibleMods);

            if (!_editingMode)
            {
                CarcolsManager.Instance.AddVehiclesToCarcols(vehData);
                ClearCurrentModDetailInModPanel();
                ClearCurrentModKitDetailInModPanel();
                VehiclePanelManager.Instance.RebuildAllVehiclePanels();
            }
            else
            {
                CarcolsManager.Instance.EditVehicleInCarcols(VehicleModManager.Instance.EditingVehicleIndex, vehData);
                ClearCurrentModDetailInModPanel();
                ClearCurrentModKitDetailInModPanel();
                VehiclePanelManager.Instance.RebuildAllVehiclePanels();
                CleanupEditingMode();
            }

            // Update preview panel 
            VehicleModManager.Instance.UpdatePreviewPanel = true;

            SetThisMainPanelToVisible(vehicleMainPanel);
            ShowThisMenuPanel(vehicleMenuPanel);
        }

        private void AddModtoModPanelAndCleanup()
        {
            PrintLine.WriteLine("[MainPage] AddModtoModPanelAndCleanup - called _editingMode: " + _editingMode);

            if (_editingMode && !VehicleModManager.Instance.AddNewMod)
            {
                VehicleModManager.Instance.EditModInManagerModList(VehicleModManager.Instance.EditingModIndex);
                ClearCurrentModDetailInModPanel();
                ClearCurrentModKitDetailInModPanel();
                VehiclePanelManager.Instance.RebuildAllModPanels();

                if (VehicleModManager.Instance.EditingModPanel && !VehicleModManager.Instance.EditingVehiclePanel)
                {
                    CleanupEditingMode();
                }
            }
            else
            {
                VehicleModManager.Instance.AddNewModToManagerModList();

                ClearCurrentModDetailInModPanel();

                int ListCount = VehicleModManager.Instance.GetVehicleModList().Count;
                if (ListCount > 0)
                {
                    VehiclePanelManager.Instance.CreateAndAddVehiclePanelToList(ListCount - 1,
                        VehicleModManager.Instance.GetVehicleModList()[ListCount - 1].ModType
                        , VehPanel.PanelType.TypeModLibrary, ListCount.ToString());
                }
                else
                {
                    PrintLine.WriteLine("[MainPage] AddModtoModPanelAndCleanup - VehicleModManager.Instance.GetVehicleList().Count: " + VehicleModManager.Instance.GetVehicleModList().Count);
                }

                VehicleModManager.Instance.AddNewMod = false;
            }

            VehiclePanelManager.Instance.RebuildAllModPanels();
            VehicleModManager.Instance.CleanupModData();

            // Update preview panel 
            VehicleModManager.Instance.UpdatePreviewPanel = true;

            SetThisMainPanelToVisible(modMainPanel);
            ShowThisMenuPanel(modsMenuPanel);
        }

        public static string RemoveAllXmlNamespace(string xmlData)
        {
            string xmlnsPattern = "\\s+xmlns\\s*(:\\w)?\\s*=\\s*\\\"(?<url>[^\\\"]*)\\\"";
            MatchCollection matchCol = Regex.Matches(xmlData, xmlnsPattern);

            foreach (Match m in matchCol)
            {
                xmlData = xmlData.Replace(m.ToString(), "");
            }
            return xmlData;
        }

        private static FileAttributes RemoveAttribute(FileAttributes attributes, FileAttributes attributesToRemove)
        {
            return attributes & ~attributesToRemove;
        }

        private void UpdatePreviewPanel()
        {
            string filename = @"tempCarcol.meta";
            string tempPath = @"C:\\Temp";
            tempPath = Path.Combine(tempPath, filename);

            PrintLine.WriteLine("[MainPage] UpdatePreviewPanel path: " + Path.GetFullPath(tempPath));
            FileStream fs = new FileStream(tempPath, FileMode.Create, FileAccess.ReadWrite);

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            XmlSerializer sr = new XmlSerializer(CarcolsManager.Instance.GetType());
            sr.Serialize(fs, CarcolsManager.Instance, ns);

            fs.Close();

            previewTextBox.Text = "";
            previewTextBox.Text = File.ReadAllText(RemoveAllXmlNamespace(tempPath));
        }

        public void ExportData(object obj, string FileName)
        {
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            XmlSerializer sr = new XmlSerializer(obj.GetType());

            using (StreamWriter streamWrite = new StreamWriter(FileName))
            {
                TextWriter writer = streamWrite;
                sr.Serialize(writer, obj, ns);

                writer.Dispose();
                streamWrite.Close();

                string removeNameSpaces = File.ReadAllText(FileName);
                previewTextBox.Text = RemoveAllXmlNamespace(removeNameSpaces);
                File.WriteAllText(FileName, RemoveAllXmlNamespace(removeNameSpaces));
            }
        }

        private void weightTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!Char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void audioApplyTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (ch == 46 && audioApplyTextBox.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }

            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void ModTypeDropDown_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void turnOffExtraComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void disableBonnetCameraComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void allowBonnetSlideComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
        }
        #endregion

        #region Cleanups
        private void ClearCurrentModDetailInModPanel()
        {
            modItemShopLabelTextBox.Text = "";
            modModelTextBox.Text = "";
            boneTextBox.Text = "";
            ModTypeDropDown.Text = "";
            cameraPositionTextBox.Text = "";
            boneTextBox.Text = "";
            CollisionBoneTextBox.Text = "";
            audioApplyTextBox.Text = "";
            weightTextBox.Text = "";
            turnOffExtraComboBox.Text = "";
            disableBonnetCameraComboBox.Text = "";
            allowBonnetSlideComboBox.Text = "";
            LinkedModelListView.Items.Clear();
            TurnOffBoneListView.Items.Clear();
        }

        private void ClearCurrentModKitDetailInModPanel()
        {
            modKitNametextBox.Text = "";
            ModKitTypeComboBox.Text = "";
            IdTextBox.Text = "";
        }

        private bool CleanupEditingMode(bool DisplayMessageBox = false)
        {
            if (_editingMode)
            {
                if (DisplayMessageBox)
                {
                    if (DialogResult.Yes == MessageBox.Show("Are you sure you want to leave this page without saving", "LEAVE", MessageBoxButtons.YesNo))
                    {
                        _editingMode = false;
                        VehicleModManager.Instance.EditingVehiclePanel = false;
                        VehicleModManager.Instance.EditingModPanel = false;
                        PrintLine.WriteLine("[MainPage] CleanupEditingMode - called _editingMode: " + _editingMode);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    _editingMode = false;
                    VehicleModManager.Instance.EditingVehiclePanel = false;
                    VehicleModManager.Instance.EditingModPanel = false;
                    UpdateEditModBtAndLabelName();
                    PrintLine.WriteLine("[MainPage] CleanupEditingMode - called _editingMode: " + _editingMode);
                }
            }

            return true;
        }

        private bool CleanupAddModsToExisitingVehicle(bool DisplayMessageBox = true)
        {
            if (VehicleModManager.Instance.AddToExistingVehicle)
            {
                if (DisplayMessageBox)
                {
                    if (DialogResult.Yes == MessageBox.Show("Are you sure you want to leave this page without saving", "LEAVE", MessageBoxButtons.YesNo))
                    {
                        VehicleModManager.Instance.AddToExistingVehicle = false;
                        saveToExistingVehicleBt.Visible = false;
                        exportCarcolsDataBt.Visible = true;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    VehicleModManager.Instance.AddToExistingVehicle = false;
                    saveToExistingVehicleBt.Visible = false;
                    exportCarcolsDataBt.Visible = true;
                }
            }

            return true;
        }
        #endregion

        #region Button Click Events

        private void ExitBt_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Are you sure you want to exit Carcols Editor?", "EXIT", MessageBoxButtons.YesNo))
            {
                this.Close();
            }
        }

        private void DashboardBt_Click(object sender, EventArgs e)
        {
            if (!CleanupEditingMode(true))
            {
                return;
            }

            if (!CleanupAddModsToExisitingVehicle())
            {
                return;
            }
            VehicleModManager.Instance.AddNewMod = false;
            UpdateEditModBtAndLabelName();
            ShowThisMenuPanel(dashboardMenuPanel);
            SetThisMainPanelToVisible(dashboardMainPanel);
        }

        private void PreviewBt_Click(object sender, EventArgs e)
        {
            if (!CleanupEditingMode(true))
            {
                return;
            }

            if (!CleanupAddModsToExisitingVehicle())
            {
                return;
            }

            VehicleModManager.Instance.AddNewMod = false;
            UpdateEditModBtAndLabelName();
            ShowThisMenuPanel(previewMenuPanel);
            SetThisMainPanelToVisible(previewMainPanel);
        }

        private void VehicleMainBt_Click(object sender, EventArgs e)
        {
            if (!CleanupEditingMode(true))
            {
                return;
            }

            if (!CleanupAddModsToExisitingVehicle())
            {
                return;
            }

            VehicleModManager.Instance.AddNewMod = false;
            UpdateEditModBtAndLabelName();
            ShowThisMenuPanel(vehicleMenuPanel);
            SetThisMainPanelToVisible(vehicleMainPanel);
        }

        private void loadExistingCarcols_Click(object sender, EventArgs e)
        {
            using (ofd = new OpenFileDialog() { Filter = "Text File|*.meta", Multiselect = false })
            {

                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        using (StreamReader sr = new StreamReader(ofd.FileName))
                        {
                            CarcolsManager.Instance.Kits.Clear();

                            XmlSerializer serialzer = new XmlSerializer(typeof(CarcolsManager));
                            CarcolsManager carcolsManager = CarcolsManager.Instance;

                            carcolsManager = (CarcolsManager)serialzer.Deserialize(sr);
                            CarcolsManager.Instance.SetCarcolsInstance(carcolsManager);

                            previewTextBox.Text = File.ReadAllText(ofd.FileName);
                            sr.Close();

                            VehiclePanelManager.Instance.RebuildAllVehiclePanels();
                            SetThisMainPanelToVisible(vehicleMainPanel);
                            ShowThisMenuPanel(vehicleMenuPanel);

                            // Not going to format since it is taking long for big files
                            //previewTextBox.InitXml(previewTextBox.Text);
                            PrintLine.WriteLine("[MainPage] loadExistingCarcols_Click - Complete");
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Invalid Carcols file.",
                           "Error",
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error);

                    }
                }
            }
        }

        private void savePreviewBt_Click(object sender, EventArgs e)
        {
            if (savePreviewFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    using (StringReader stringReader = new StringReader(previewTextBox.Text))
                    {
                        XmlSerializer serialzer = new XmlSerializer(typeof(CarcolsManager));
                        CarcolsManager carcolsManager = CarcolsManager.Instance;

                        carcolsManager = (CarcolsManager)serialzer.Deserialize(stringReader);
                        CarcolsManager.Instance.SetCarcolsInstance(carcolsManager);
                        File.WriteAllText(savePreviewFile.FileName, previewTextBox.Text);
                        VehiclePanelManager.Instance.RebuildAllVehiclePanels();
                    }
                }
                catch(Exception)
                {
                    MessageBox.Show("Invalid Carcols format. Please double check your changes!",
                           "Error",
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error);
                }
                
            }
        }

        private void createNewVehicleBt_Click(object sender, EventArgs e)
        {
            if (VehiclePanelManager.Instance.GetModPanelList() != null && VehiclePanelManager.Instance.GetModPanelList().Count > 0)
            {
                if (VehiclePanelManager.Instance.IsAnyModChecked() || VehicleModManager.Instance.EditingVehiclePanel)
                {
                    if (_editingMode)
                    {
                        if (VehicleModManager.Instance.EditingVehiclePanel)
                        {
                            modKitNametextBox.Text = CarcolsManager.Instance.Kits[VehicleModManager.Instance.EditingVehicleIndex].ModKitName;
                            IdTextBox.Text = Convert.ToString(CarcolsManager.Instance.Kits[VehicleModManager.Instance.EditingVehicleIndex].Id.Value);
                            ModKitTypeComboBox.Text = CarcolsManager.Instance.Kits[VehicleModManager.Instance.EditingVehicleIndex].ModKitType;
                            SetThisMainPanelToVisible(createNewVehPanel);
                            return;
                        }
                    }
                    VehicleModManager.Instance.CreateVehicleData();
                    SetThisMainPanelToVisible(createNewVehPanel);
                }
                else
                {
                    MessageBox.Show("Please select at least one mod!");
                }
            }
            else
            {
                MessageBox.Show("Please add some mods first!");
            }

        }

        private List<ItemElement> GetLinkedModelsFromListView()
        {
            List<ItemElement> LinkedModelsList = new List<ItemElement>();
            int ListCount = LinkedModelListView.Items.Count;
            if (ListCount > 0)
            {
                for (int Index = 0; Index < ListCount; Index++)
                {
                    ItemElement ie = new ItemElement();
                    ie.Value = LinkedModelListView.Items[Index].Text;
                    LinkedModelsList.Add(ie);
                }
            }

            return LinkedModelsList;
        }

        private void SetLinkedModelsFromListView(int VehModListIndex)
        {
            if (VehicleModManager.Instance.GetVehicleModList().Count > 0)
            {
                int Count = VehicleModManager.Instance.GetVehicleModList()[VehModListIndex].LinkedModels.Count;
                if (Count > 0)
                {
                    for (int Index = 0; Index < Count; Index++)
                    {
                        LinkedModelListView.Items.Add(VehicleModManager.Instance.GetVehicleModList()[VehModListIndex]
                            .LinkedModels[Index].Value);
                    }
                }
            }
        }

        private List<ItemElement> GetTurnOffBonesFromListView()
        {
            List<ItemElement> turnOffBonesList = new List<ItemElement>();
            int ListCount = TurnOffBoneListView.Items.Count;
            if (ListCount > 0)
            {
                for (int Index = 0; Index < ListCount; Index++)
                {
                    ItemElement ie = new ItemElement();
                    ie.Value = TurnOffBoneListView.Items[Index].Text;
                    turnOffBonesList.Add(ie);
                }
            }

            return turnOffBonesList;
        }

        private void SetTurnOffBonesFromListView(int VehModListIndex)
        {
            if (VehicleModManager.Instance.GetVehicleModList().Count > 0)
            {
                int Count = VehicleModManager.Instance.GetVehicleModList()[VehModListIndex].TurnOffBones.Count;
                if (Count > 0)
                {
                    for (int Index = 0; Index < Count; Index++)
                    {
                        TurnOffBoneListView.Items.Add(VehicleModManager.Instance.GetVehicleModList()[VehModListIndex].TurnOffBones[Index].Value);
                    }
                }
            }
        }

        private void saveModBt_Click(object sender, EventArgs e)
        {
            if (ModTypeDropDown.Text == "")
            {
                MessageBox.Show("You must enter a Mod Type!");
                return;
            }

            if (VehicleModManager.Instance.GetModData() == null || VehicleModManager.Instance.AddNewMod)
            {
                VehicleModManager.Instance.CreateVehicleModData();

                VehicleModManager.Instance.CreateModData(ModTypeDropDown.Text, modModelTextBox.Text, modItemShopLabelTextBox.Text, boneTextBox.Text, CollisionBoneTextBox.Text,
                cameraPositionTextBox.Text, audioApplyTextBox.Text, weightTextBox.Text, turnOffExtraComboBox.Text, disableBonnetCameraComboBox.Text,
                allowBonnetSlideComboBox.Text, GetLinkedModelsFromListView(), GetTurnOffBonesFromListView());
            }
            else
            {
                VehicleModManager.Instance.GetModData().ModType = ModTypeDropDown.Text;
                VehicleModManager.Instance.GetModData().ModModelName = modModelTextBox.Text;
                VehicleModManager.Instance.GetModData().ModShopLabel = modItemShopLabelTextBox.Text;
                VehicleModManager.Instance.GetModData().Bone = boneTextBox.Text;
                VehicleModManager.Instance.GetModData().CollisionBone = CollisionBoneTextBox.Text;
                VehicleModManager.Instance.GetModData().CameraPos = cameraPositionTextBox.Text;
                VehicleModManager.Instance.GetModData().AudioApply.Value = audioApplyTextBox.Text;
                VehicleModManager.Instance.GetModData().Weight.Value = weightTextBox.Text;
                VehicleModManager.Instance.GetModData().TurnOffExtra.Value = turnOffExtraComboBox.Text.Equals("true") ? true : false;
                VehicleModManager.Instance.GetModData().DisableBonnetCamera.Value = disableBonnetCameraComboBox.Text.Equals("true") ? true : false;
                VehicleModManager.Instance.GetModData().AllowBonnetSlide.Value = allowBonnetSlideComboBox.Text.Equals("true") ? true : false;
                VehicleModManager.Instance.GetModData().LinkedModels = GetLinkedModelsFromListView();
                VehicleModManager.Instance.GetModData().TurnOffBones = GetTurnOffBonesFromListView();
            }

            AddModtoModPanelAndCleanup();
        }

        private void exportCarcolsDataBt_Click(object sender, EventArgs e)
        {
            if (CarcolsManager.Instance.Kits.Count > 0)
            {
                if (savePreviewFile.ShowDialog() == DialogResult.OK)
                {
                    if (!savePreviewFile.FileName.Contains(".meta"))
                    {
                        ExportData(CarcolsManager.Instance, savePreviewFile.FileName + ".meta");
                    }
                    else
                    {
                        ExportData(CarcolsManager.Instance, savePreviewFile.FileName);
                    }
                    MessageBox.Show("Export Complete!");
                }
            }
            else
            {
                MessageBox.Show("Please enter at least one vehicle!");
            }
        }

        private void modsMenuBt_Click(object sender, EventArgs e)
        {
            if (!CleanupEditingMode(true))
            {
                return;
            }

            if (!CleanupAddModsToExisitingVehicle())
            {
                return;
            }

            if (!_editingMode)
            {
                VehiclePanelManager.Instance.RebuildAllModPanels();
            }

            VehicleModManager.Instance.AddNewMod = false;
            UpdateEditModBtAndLabelName();
            ShowThisMenuPanel(modsMenuPanel);
            SetThisMainPanelToVisible(modMainPanel);
        }

        private void createNewModBt_Click(object sender, EventArgs e)
        {
            ClearCurrentModDetailInModPanel();
            VehicleModManager.Instance.EditingModPanel = false;
            VehicleModManager.Instance.AddNewMod = true;
            VehicleModManager.Instance.CreateVehicleModData();
            ShowThisMenuPanel(createNewModPanel);
            SetThisMainPanelToVisible(createNewModPanel);
        }

        private void saveVehBt_Click(object sender, EventArgs e)
        {
            if (modKitNametextBox.Text == "")
            {
                MessageBox.Show("Please enter Mod Kit name!");
                return;
            }
            else
            {
                int ListCount = CarcolsManager.Instance.Kits.Count;
                if (ListCount > 0)
                {
                    for (int Index = 0; Index < ListCount; Index++)
                    {
                        if (!_editingMode)
                        {
                            if (CarcolsManager.Instance.DoesThisVehicleNameExistInCarcols(modKitNametextBox.Text))
                            {
                                MessageBox.Show("This Mod Kit already exist. Please add a valid Mod Kit Name!");
                                return;
                            }
                        }
                        else
                        {
                            if (CarcolsManager.Instance.DoesThisVehicleNameExistInCarcols(modKitNametextBox.Text)
                                && CarcolsManager.Instance.GetCarcolsVehicleName(VehicleModManager.Instance.EditingVehicleIndex) != modKitNametextBox.Text)
                            {
                                MessageBox.Show("This Mod Kit already exist. Please add a valid Mod Kit Name!");
                                return;
                            }
                        }
                    }
                }

                AddVehicleToCarcolsAndCleanup();
                PrintLine.WriteLine("[MainPage] saveVehicleBt_Click - user trying to save the mods, adding the current and only mod for this vehicle");
            }
        }

        private void saveModLibraryBt_Click(object sender, EventArgs e)
        {
            if (VehicleModManager.Instance.GetVehicleModList().Count > 0)
            {
                if (VehiclePanelManager.Instance.IsAnyModChecked())
                {
                    if (savePreviewFile.ShowDialog() == DialogResult.OK)
                    {
                        if (!savePreviewFile.FileName.Contains(".meta"))
                        {
                            ExportData(VehicleModManager.Instance.GetVehicleModList(), savePreviewFile.FileName + ".meta");
                        }
                        else
                        {
                            ExportData(VehicleModManager.Instance.GetVehicleModList(), savePreviewFile.FileName);
                        }
                        MessageBox.Show("Saved!");
                    }
                }
                else
                {
                    MessageBox.Show("Please select at least one mod!");
                }
            }
            else
            {
                MessageBox.Show("Please enter at least one Mod!");
            }
        }

        private void loadModLibrary_Click(object sender, EventArgs e)
        {
            try
            {
                using (ofd = new OpenFileDialog() { Filter = "Text File|*.meta", Multiselect = false })
                {
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        using (StreamReader sr = new StreamReader(ofd.FileName))
                        {
                            VehicleModManager.Instance.GetVehicleModList().Clear();

                            XmlSerializer serialzer = new XmlSerializer(typeof(List<ModData>));
                            List<ModData> vehicleManager = VehicleModManager.Instance.GetVehicleModList();

                            vehicleManager = (List<ModData>)serialzer.Deserialize(sr);
                            VehicleModManager.Instance.SetVehicleManagerVehicleList(vehicleManager);

                            sr.Close();

                            VehiclePanelManager.Instance.RebuildAllModPanels();
                            ShowThisMenuPanel(modsMenuPanel);
                            SetThisMainPanelToVisible(modMainPanel);
                            PrintLine.WriteLine("[MainPage] loadModLibrary_Click - Complete");
                        }
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Mod Library file.",
                   "Error",
                   MessageBoxButtons.OK,
                   MessageBoxIcon.Error);
            }
        }

        private void addToExistingVehicle_Click(object sender, EventArgs e)
        {
            if (CarcolsManager.Instance.Kits.Count > 0)
            {
                if (VehicleModManager.Instance.GetVehicleModList().Count > 0)
                {
                    if (VehiclePanelManager.Instance.IsAnyModChecked())
                    {
                        VehicleModManager.Instance.AddToExistingVehicle = true;
                        VehiclePanelManager.Instance.RebuildAllVehiclePanels();
                        saveToExistingVehicleBt.Visible = true;
                        exportCarcolsDataBt.Visible = false;
                        ShowThisMenuPanel(vehicleMenuPanel);
                        SetThisMainPanelToVisible(vehicleMainPanel);
                    }
                    else
                    {
                        MessageBox.Show("Please select at least one Mod!");
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Please enter at least one Mod!");
                    return;
                }
            }
            else
            {
                MessageBox.Show("Please add at least one Vehicle to Carcols!");
                return;
            }
        }

        private void saveToExistingVehicle_Click(object sender, EventArgs e)
        {
            if (VehiclePanelManager.Instance.IsAnyVehicleChecked())
            {
                CleanupAddModsToExisitingVehicle(false);
                VehiclePanelManager.Instance.AddSelectedModsToExistingVehicles();
                VehiclePanelManager.Instance.RebuildAllVehiclePanels();
            }
            else
            {
                MessageBox.Show("Please select at least one Vehicle!");
                return;
            }
        }

        private void LinkedModelAddBT_Click(object sender, EventArgs e)
        {
            if (LinkedModelTextBox.Text != "")
            {
                ListViewItem item = new ListViewItem(LinkedModelTextBox.Text);
                LinkedModelListView.Items.Add(item);
                LinkedModelTextBox.Clear();
                LinkedModelTextBox.Focus();
            }
        }

        private void LinkedModelRemoveBT_Click(object sender, EventArgs e)
        {
            if (LinkedModelListView.Items.Count > 0)
            {
                if (LinkedModelListView.SelectedItems.Count > 0)
                {
                    LinkedModelListView.Items.Remove(LinkedModelListView.SelectedItems[0]);
                }
            }
        }

        private void TunrOffBoneAddBt_Click(object sender, EventArgs e)
        {
            if (TurnOffBonesTextBox.Text != "")
            {
                ListViewItem item = new ListViewItem(TurnOffBonesTextBox.Text);
                TurnOffBoneListView.Items.Add(item);
                TurnOffBonesTextBox.Clear();
                TurnOffBonesTextBox.Focus();
            }
        }

        private void TurnOffBoneRemove_Click(object sender, EventArgs e)
        {
            if (TurnOffBoneListView.Items.Count > 0)
            {
                if (TurnOffBoneListView.SelectedItems.Count > 0)
                {
                    PrintLine.WriteLine("[MainPage] TurnOffBoneRemove_Click - index" + TurnOffBoneListView.SelectedItems[0].Index);
                    TurnOffBoneListView.Items.Remove(TurnOffBoneListView.SelectedItems[0]);
                }
            }
        }

        private void IdTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!Char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void clearCarcolsBT_Click(object sender, EventArgs e)
        {
            if (CarcolsManager.Instance.Kits.Count > 0)
            {
                CarcolsManager.Instance.Kits.Clear();
                VehiclePanelManager.Instance.RebuildAllVehiclePanels();
                VehicleModManager.Instance.UpdatePreviewPanel = true;
                MessageBox.Show("Carcols Cleared!");
            }
            else
            {
                MessageBox.Show("Current Carcols is Empty!");
            }
        }

        private void MainPage_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult di = MessageBox.Show("Are you sure you want to exit Carcols Editor?", "EXIT", MessageBoxButtons.YesNo);
            if (di == DialogResult.Yes)
            {
                Environment.Exit(0);
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void AddToModLibBT_Click(object sender, EventArgs e)
        {
            if (VehiclePanelManager.Instance.IsAnyModChecked())
            {
                VehicleModManager.Instance.CopySelectedModsToLibrary();
            }
            else
            {
                MessageBox.Show("Please select at least one Mod!");
            }
        }
        #endregion
    }
}
