﻿using CustomXmlViewer;
using System.Windows.Forms;

namespace CarcolsEditor
{
    public partial class MainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Fields
        private System.ComponentModel.ComponentResourceManager resources;
        private Panel leftPanel;
        private Button DashboardBt;
        private Button VehiclesBt;
        private Button PreviewBt;
        private Button ExitBt;
        private Panel dashboardMenuPanel;
        private Panel vehicleMenuPanel;
        private Panel previewMenuPanel;
        private RoundButton loadExistingFile;
        private Panel dashboardMainPanel;
        private Panel previewMainPanel;
        private ucXmlRichTextBox previewTextBox;
        private RoundButton savePreviewBt;
        private SaveFileDialog savePreviewFile;
        private Panel createNewVehPanel;
        private Label modKitTypeLabel;
        private TextBox modKitNametextBox;
        private Label modKitNameLabel;
        private Panel vehicleMainPanel;
        private Panel VehiclePanelQuickAddVeh;
        private RoundButton exportCarcolsDataBt;
        private Panel modsMenuPanel;
        private Button modsMenuBt;
        private Panel modMainPanel;
        private Panel ModPanelQuickAddMod;
        private RoundButton QCreateNewModBt;
        private Panel createNewModPanel;
        private Label modModellabel;
        private Label modTypeLabel;
        private Label modShopMenuDescLabel;
        private TextBox modModelTextBox;
        private TextBox boneTextBox;
        private RoundButton createNewModBt;
        private RoundButton createNewCarBt;
        private RoundButton saveVehBt;
        private Panel previewQuickPanel;
        private Label ModMainLabel;
        private Label vehicleMainLabel;
        private RoundButton saveModLibraryBt;
        private RoundButton loadModLibrary;
        private RoundButton addToExistingVehicle;
        private RoundButton saveToExistingVehicleBt;
        private ComboBox ModTypeDropDown;
        private TextBox cameraPositionTextBox;
        private Label modItemShopLabel;
        private Label cameraPositionlabel;
        private TextBox modItemShopLabelTextBox;
        private RoundButton saveModBt;
        private Label label2;
        private TextBox weightTextBox;
        private Label weightLB;
        private TextBox audioApplyTextBox;
        private Label audioApplyLB;
        private TextBox CollisionBoneTextBox;
        private Label label1;
        private ComboBox allowBonnetSlideComboBox;
        private Label allowBonnetSlideLabel;
        private ComboBox disableBonnetCameraComboBox;
        private Label label3;
        private ComboBox turnOffExtraComboBox;
        private Button TurnOffBoneRemove;
        private Button TunrOffBoneAddBt;
        private Button LinkedModelRemoveBT;
        private Button LinkedModelAddBT;
        private ListView TurnOffBoneListView;
        private TextBox TurnOffBonesTextBox;
        private Label label4;
        private TextBox LinkedModelTextBox;
        private Label linkedModelLB;
        private ListView LinkedModelListView;
        private ColumnHeader LinkedModels;
        private ColumnHeader TurnOffBones;
        private TextBox IdTextBox;
        private Label IDlabel;
        private ComboBox ModKitTypeComboBox;
        private RoundButton clearCarcolsBT;
        private RoundButton AddToModLibBT;
        #endregion

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainPage));
            this.leftPanel = new System.Windows.Forms.Panel();
            this.dashboardMenuPanel = new System.Windows.Forms.Panel();
            this.modsMenuPanel = new System.Windows.Forms.Panel();
            this.vehicleMenuPanel = new System.Windows.Forms.Panel();
            this.previewMenuPanel = new System.Windows.Forms.Panel();
            this.DashboardBt = new System.Windows.Forms.Button();
            this.VehiclesBt = new System.Windows.Forms.Button();
            this.modsMenuBt = new System.Windows.Forms.Button();
            this.PreviewBt = new System.Windows.Forms.Button();
            this.ExitBt = new System.Windows.Forms.Button();
            this.dashboardMainPanel = new System.Windows.Forms.Panel();
            this.createNewVehPanel = new System.Windows.Forms.Panel();
            this.ModKitTypeComboBox = new System.Windows.Forms.ComboBox();
            this.IdTextBox = new System.Windows.Forms.TextBox();
            this.IDlabel = new System.Windows.Forms.Label();
            this.modKitTypeLabel = new System.Windows.Forms.Label();
            this.modKitNametextBox = new System.Windows.Forms.TextBox();
            this.modKitNameLabel = new System.Windows.Forms.Label();
            this.previewMainPanel = new System.Windows.Forms.Panel();
            this.savePreviewFile = new System.Windows.Forms.SaveFileDialog();
            this.vehicleMainPanel = new System.Windows.Forms.Panel();
            this.vehicleMainLabel = new System.Windows.Forms.Label();
            this.VehiclePanelQuickAddVeh = new System.Windows.Forms.Panel();
            this.modMainPanel = new System.Windows.Forms.Panel();
            this.ModMainLabel = new System.Windows.Forms.Label();
            this.ModPanelQuickAddMod = new System.Windows.Forms.Panel();
            this.createNewModPanel = new System.Windows.Forms.Panel();
            this.LinkedModelListView = new System.Windows.Forms.ListView();
            this.LinkedModels = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TurnOffBoneRemove = new System.Windows.Forms.Button();
            this.TunrOffBoneAddBt = new System.Windows.Forms.Button();
            this.LinkedModelRemoveBT = new System.Windows.Forms.Button();
            this.LinkedModelAddBT = new System.Windows.Forms.Button();
            this.TurnOffBoneListView = new System.Windows.Forms.ListView();
            this.TurnOffBones = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TurnOffBonesTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.LinkedModelTextBox = new System.Windows.Forms.TextBox();
            this.linkedModelLB = new System.Windows.Forms.Label();
            this.allowBonnetSlideComboBox = new System.Windows.Forms.ComboBox();
            this.allowBonnetSlideLabel = new System.Windows.Forms.Label();
            this.disableBonnetCameraComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.turnOffExtraComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.weightTextBox = new System.Windows.Forms.TextBox();
            this.weightLB = new System.Windows.Forms.Label();
            this.audioApplyTextBox = new System.Windows.Forms.TextBox();
            this.audioApplyLB = new System.Windows.Forms.Label();
            this.CollisionBoneTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cameraPositionTextBox = new System.Windows.Forms.TextBox();
            this.modItemShopLabel = new System.Windows.Forms.Label();
            this.cameraPositionlabel = new System.Windows.Forms.Label();
            this.modItemShopLabelTextBox = new System.Windows.Forms.TextBox();
            this.ModTypeDropDown = new System.Windows.Forms.ComboBox();
            this.modModellabel = new System.Windows.Forms.Label();
            this.modTypeLabel = new System.Windows.Forms.Label();
            this.modShopMenuDescLabel = new System.Windows.Forms.Label();
            this.modModelTextBox = new System.Windows.Forms.TextBox();
            this.boneTextBox = new System.Windows.Forms.TextBox();
            this.previewQuickPanel = new System.Windows.Forms.Panel();
            this.savePreviewBt = new CarcolsEditor.RoundButton();
            this.previewTextBox = new CustomXmlViewer.ucXmlRichTextBox();
            this.saveModBt = new CarcolsEditor.RoundButton();
            this.AddToModLibBT = new CarcolsEditor.RoundButton();
            this.addToExistingVehicle = new CarcolsEditor.RoundButton();
            this.saveModLibraryBt = new CarcolsEditor.RoundButton();
            this.createNewCarBt = new CarcolsEditor.RoundButton();
            this.QCreateNewModBt = new CarcolsEditor.RoundButton();
            this.clearCarcolsBT = new CarcolsEditor.RoundButton();
            this.loadModLibrary = new CarcolsEditor.RoundButton();
            this.createNewModBt = new CarcolsEditor.RoundButton();
            this.loadExistingFile = new CarcolsEditor.RoundButton();
            this.saveVehBt = new CarcolsEditor.RoundButton();
            this.saveToExistingVehicleBt = new CarcolsEditor.RoundButton();
            this.exportCarcolsDataBt = new CarcolsEditor.RoundButton();
            this.leftPanel.SuspendLayout();
            this.dashboardMainPanel.SuspendLayout();
            this.createNewVehPanel.SuspendLayout();
            this.previewMainPanel.SuspendLayout();
            this.vehicleMainPanel.SuspendLayout();
            this.VehiclePanelQuickAddVeh.SuspendLayout();
            this.modMainPanel.SuspendLayout();
            this.ModPanelQuickAddMod.SuspendLayout();
            this.createNewModPanel.SuspendLayout();
            this.previewQuickPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // leftPanel
            // 
            this.leftPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.leftPanel.Controls.Add(this.dashboardMenuPanel);
            this.leftPanel.Controls.Add(this.modsMenuPanel);
            this.leftPanel.Controls.Add(this.vehicleMenuPanel);
            this.leftPanel.Controls.Add(this.previewMenuPanel);
            this.leftPanel.Controls.Add(this.DashboardBt);
            this.leftPanel.Controls.Add(this.VehiclesBt);
            this.leftPanel.Controls.Add(this.modsMenuBt);
            this.leftPanel.Controls.Add(this.PreviewBt);
            this.leftPanel.Controls.Add(this.ExitBt);
            this.leftPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.leftPanel.Location = new System.Drawing.Point(0, 0);
            this.leftPanel.Name = "leftPanel";
            this.leftPanel.Size = new System.Drawing.Size(171, 806);
            this.leftPanel.TabIndex = 2;
            // 
            // dashboardMenuPanel
            // 
            this.dashboardMenuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(166)))), ((int)(((byte)(0)))));
            this.dashboardMenuPanel.Location = new System.Drawing.Point(3, 4);
            this.dashboardMenuPanel.Name = "dashboardMenuPanel";
            this.dashboardMenuPanel.Size = new System.Drawing.Size(16, 123);
            this.dashboardMenuPanel.TabIndex = 3;
            // 
            // modsMenuPanel
            // 
            this.modsMenuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(166)))), ((int)(((byte)(0)))));
            this.modsMenuPanel.Location = new System.Drawing.Point(3, 129);
            this.modsMenuPanel.Name = "modsMenuPanel";
            this.modsMenuPanel.Size = new System.Drawing.Size(16, 123);
            this.modsMenuPanel.TabIndex = 9;
            // 
            // vehicleMenuPanel
            // 
            this.vehicleMenuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(166)))), ((int)(((byte)(0)))));
            this.vehicleMenuPanel.Location = new System.Drawing.Point(3, 254);
            this.vehicleMenuPanel.Name = "vehicleMenuPanel";
            this.vehicleMenuPanel.Size = new System.Drawing.Size(16, 123);
            this.vehicleMenuPanel.TabIndex = 5;
            // 
            // previewMenuPanel
            // 
            this.previewMenuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(166)))), ((int)(((byte)(0)))));
            this.previewMenuPanel.Location = new System.Drawing.Point(3, 379);
            this.previewMenuPanel.Name = "previewMenuPanel";
            this.previewMenuPanel.Size = new System.Drawing.Size(16, 123);
            this.previewMenuPanel.TabIndex = 4;
            // 
            // DashboardBt
            // 
            this.DashboardBt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(45)))));
            this.DashboardBt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.DashboardBt.FlatAppearance.BorderSize = 0;
            this.DashboardBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DashboardBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DashboardBt.ForeColor = System.Drawing.Color.White;
            this.DashboardBt.Image = ((System.Drawing.Image)(resources.GetObject("DashboardBt.Image")));
            this.DashboardBt.Location = new System.Drawing.Point(1, 4);
            this.DashboardBt.Name = "DashboardBt";
            this.DashboardBt.Size = new System.Drawing.Size(167, 123);
            this.DashboardBt.TabIndex = 5;
            this.DashboardBt.TabStop = false;
            this.DashboardBt.Text = "Dashboard";
            this.DashboardBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.DashboardBt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.DashboardBt.UseVisualStyleBackColor = false;
            this.DashboardBt.Click += new System.EventHandler(this.DashboardBt_Click);
            // 
            // VehiclesBt
            // 
            this.VehiclesBt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.VehiclesBt.FlatAppearance.BorderSize = 0;
            this.VehiclesBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.VehiclesBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VehiclesBt.ForeColor = System.Drawing.Color.White;
            this.VehiclesBt.Image = ((System.Drawing.Image)(resources.GetObject("VehiclesBt.Image")));
            this.VehiclesBt.Location = new System.Drawing.Point(1, 254);
            this.VehiclesBt.Name = "VehiclesBt";
            this.VehiclesBt.Size = new System.Drawing.Size(167, 123);
            this.VehiclesBt.TabIndex = 7;
            this.VehiclesBt.TabStop = false;
            this.VehiclesBt.Text = "Kits";
            this.VehiclesBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.VehiclesBt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.VehiclesBt.UseVisualStyleBackColor = true;
            this.VehiclesBt.Click += new System.EventHandler(this.VehicleMainBt_Click);
            // 
            // modsMenuBt
            // 
            this.modsMenuBt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.modsMenuBt.FlatAppearance.BorderSize = 0;
            this.modsMenuBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.modsMenuBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modsMenuBt.ForeColor = System.Drawing.Color.White;
            this.modsMenuBt.Image = ((System.Drawing.Image)(resources.GetObject("modsMenuBt.Image")));
            this.modsMenuBt.Location = new System.Drawing.Point(1, 129);
            this.modsMenuBt.Name = "modsMenuBt";
            this.modsMenuBt.Size = new System.Drawing.Size(167, 123);
            this.modsMenuBt.TabIndex = 10;
            this.modsMenuBt.TabStop = false;
            this.modsMenuBt.Text = "Mods";
            this.modsMenuBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.modsMenuBt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.modsMenuBt.UseVisualStyleBackColor = true;
            this.modsMenuBt.Click += new System.EventHandler(this.modsMenuBt_Click);
            // 
            // PreviewBt
            // 
            this.PreviewBt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.PreviewBt.FlatAppearance.BorderSize = 0;
            this.PreviewBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PreviewBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreviewBt.ForeColor = System.Drawing.Color.White;
            this.PreviewBt.Image = ((System.Drawing.Image)(resources.GetObject("PreviewBt.Image")));
            this.PreviewBt.Location = new System.Drawing.Point(1, 379);
            this.PreviewBt.Name = "PreviewBt";
            this.PreviewBt.Size = new System.Drawing.Size(167, 123);
            this.PreviewBt.TabIndex = 6;
            this.PreviewBt.TabStop = false;
            this.PreviewBt.Text = "Preview";
            this.PreviewBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.PreviewBt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.PreviewBt.UseVisualStyleBackColor = true;
            this.PreviewBt.Click += new System.EventHandler(this.PreviewBt_Click);
            // 
            // ExitBt
            // 
            this.ExitBt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ExitBt.FlatAppearance.BorderSize = 0;
            this.ExitBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitBt.ForeColor = System.Drawing.Color.White;
            this.ExitBt.Image = ((System.Drawing.Image)(resources.GetObject("ExitBt.Image")));
            this.ExitBt.Location = new System.Drawing.Point(1, 504);
            this.ExitBt.Name = "ExitBt";
            this.ExitBt.Size = new System.Drawing.Size(167, 123);
            this.ExitBt.TabIndex = 8;
            this.ExitBt.TabStop = false;
            this.ExitBt.Text = "Exit";
            this.ExitBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ExitBt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ExitBt.UseVisualStyleBackColor = true;
            this.ExitBt.Click += new System.EventHandler(this.ExitBt_Click);
            // 
            // dashboardMainPanel
            // 
            this.dashboardMainPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(50)))));
            this.dashboardMainPanel.Controls.Add(this.clearCarcolsBT);
            this.dashboardMainPanel.Controls.Add(this.loadModLibrary);
            this.dashboardMainPanel.Controls.Add(this.createNewModBt);
            this.dashboardMainPanel.Controls.Add(this.loadExistingFile);
            this.dashboardMainPanel.Location = new System.Drawing.Point(172, 1);
            this.dashboardMainPanel.Name = "dashboardMainPanel";
            this.dashboardMainPanel.Size = new System.Drawing.Size(1265, 806);
            this.dashboardMainPanel.TabIndex = 3;
            // 
            // createNewVehPanel
            // 
            this.createNewVehPanel.Controls.Add(this.ModKitTypeComboBox);
            this.createNewVehPanel.Controls.Add(this.IdTextBox);
            this.createNewVehPanel.Controls.Add(this.IDlabel);
            this.createNewVehPanel.Controls.Add(this.saveVehBt);
            this.createNewVehPanel.Controls.Add(this.modKitTypeLabel);
            this.createNewVehPanel.Controls.Add(this.modKitNametextBox);
            this.createNewVehPanel.Controls.Add(this.modKitNameLabel);
            this.createNewVehPanel.Location = new System.Drawing.Point(172, 1);
            this.createNewVehPanel.Name = "createNewVehPanel";
            this.createNewVehPanel.Size = new System.Drawing.Size(1266, 806);
            this.createNewVehPanel.TabIndex = 12;
            // 
            // ModKitTypeComboBox
            // 
            this.ModKitTypeComboBox.FormattingEnabled = true;
            this.ModKitTypeComboBox.Items.AddRange(new object[] {
            "MKT_STANDARD",
            "MKT_SPORT",
            "MKT_SUV",
            "MKT_SPECIAL"});
            this.ModKitTypeComboBox.Location = new System.Drawing.Point(526, 326);
            this.ModKitTypeComboBox.Name = "ModKitTypeComboBox";
            this.ModKitTypeComboBox.Size = new System.Drawing.Size(362, 28);
            this.ModKitTypeComboBox.TabIndex = 53;
            // 
            // IdTextBox
            // 
            this.IdTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(234)))), ((int)(((byte)(234)))));
            this.IdTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.IdTextBox.Location = new System.Drawing.Point(526, 245);
            this.IdTextBox.MaxLength = 10;
            this.IdTextBox.Multiline = true;
            this.IdTextBox.Name = "IdTextBox";
            this.IdTextBox.Size = new System.Drawing.Size(362, 28);
            this.IdTextBox.TabIndex = 2;
            this.IdTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IdTextBox_KeyPress);
            // 
            // IDlabel
            // 
            this.IDlabel.AutoSize = true;
            this.IDlabel.ForeColor = System.Drawing.Color.LightBlue;
            this.IDlabel.Location = new System.Drawing.Point(378, 249);
            this.IDlabel.Name = "IDlabel";
            this.IDlabel.Size = new System.Drawing.Size(26, 20);
            this.IDlabel.TabIndex = 52;
            this.IDlabel.Text = "ID";
            // 
            // modKitTypeLabel
            // 
            this.modKitTypeLabel.AutoSize = true;
            this.modKitTypeLabel.ForeColor = System.Drawing.Color.LightBlue;
            this.modKitTypeLabel.Location = new System.Drawing.Point(378, 330);
            this.modKitTypeLabel.Name = "modKitTypeLabel";
            this.modKitTypeLabel.Size = new System.Drawing.Size(100, 20);
            this.modKitTypeLabel.TabIndex = 4;
            this.modKitTypeLabel.Text = "Mod Kit Type";
            // 
            // modKitNametextBox
            // 
            this.modKitNametextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(234)))), ((int)(((byte)(234)))));
            this.modKitNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.modKitNametextBox.Location = new System.Drawing.Point(526, 164);
            this.modKitNametextBox.Multiline = true;
            this.modKitNametextBox.Name = "modKitNametextBox";
            this.modKitNametextBox.Size = new System.Drawing.Size(362, 28);
            this.modKitNametextBox.TabIndex = 1;
            // 
            // modKitNameLabel
            // 
            this.modKitNameLabel.AutoSize = true;
            this.modKitNameLabel.ForeColor = System.Drawing.Color.LightBlue;
            this.modKitNameLabel.Location = new System.Drawing.Point(378, 168);
            this.modKitNameLabel.Name = "modKitNameLabel";
            this.modKitNameLabel.Size = new System.Drawing.Size(108, 20);
            this.modKitNameLabel.TabIndex = 0;
            this.modKitNameLabel.Text = "Mod Kit Name";
            // 
            // previewMainPanel
            // 
            this.previewMainPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(50)))));
            this.previewMainPanel.Controls.Add(this.previewTextBox);
            this.previewMainPanel.Location = new System.Drawing.Point(173, 1);
            this.previewMainPanel.Name = "previewMainPanel";
            this.previewMainPanel.Size = new System.Drawing.Size(1265, 806);
            this.previewMainPanel.TabIndex = 13;
            // 
            // vehicleMainPanel
            // 
            this.vehicleMainPanel.AutoScroll = true;
            this.vehicleMainPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(50)))));
            this.vehicleMainPanel.Controls.Add(this.vehicleMainLabel);
            this.vehicleMainPanel.Location = new System.Drawing.Point(173, 1);
            this.vehicleMainPanel.Name = "vehicleMainPanel";
            this.vehicleMainPanel.Size = new System.Drawing.Size(1265, 686);
            this.vehicleMainPanel.TabIndex = 12;
            // 
            // vehicleMainLabel
            // 
            this.vehicleMainLabel.AutoSize = true;
            this.vehicleMainLabel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 15.75F, System.Drawing.FontStyle.Bold);
            this.vehicleMainLabel.ForeColor = System.Drawing.Color.CadetBlue;
            this.vehicleMainLabel.Location = new System.Drawing.Point(571, 25);
            this.vehicleMainLabel.Name = "vehicleMainLabel";
            this.vehicleMainLabel.Size = new System.Drawing.Size(126, 26);
            this.vehicleMainLabel.TabIndex = 1;
            this.vehicleMainLabel.Text = "Vehicle List";
            // 
            // VehiclePanelQuickAddVeh
            // 
            this.VehiclePanelQuickAddVeh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.VehiclePanelQuickAddVeh.Controls.Add(this.saveToExistingVehicleBt);
            this.VehiclePanelQuickAddVeh.Controls.Add(this.exportCarcolsDataBt);
            this.VehiclePanelQuickAddVeh.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.VehiclePanelQuickAddVeh.Location = new System.Drawing.Point(171, 687);
            this.VehiclePanelQuickAddVeh.Name = "VehiclePanelQuickAddVeh";
            this.VehiclePanelQuickAddVeh.Size = new System.Drawing.Size(1266, 119);
            this.VehiclePanelQuickAddVeh.TabIndex = 0;
            // 
            // modMainPanel
            // 
            this.modMainPanel.AutoScroll = true;
            this.modMainPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(50)))));
            this.modMainPanel.Controls.Add(this.ModMainLabel);
            this.modMainPanel.Location = new System.Drawing.Point(173, 1);
            this.modMainPanel.Name = "modMainPanel";
            this.modMainPanel.Size = new System.Drawing.Size(1265, 686);
            this.modMainPanel.TabIndex = 13;
            // 
            // ModMainLabel
            // 
            this.ModMainLabel.AutoSize = true;
            this.ModMainLabel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ModMainLabel.ForeColor = System.Drawing.Color.CadetBlue;
            this.ModMainLabel.Location = new System.Drawing.Point(571, 25);
            this.ModMainLabel.Name = "ModMainLabel";
            this.ModMainLabel.Size = new System.Drawing.Size(136, 26);
            this.ModMainLabel.TabIndex = 0;
            this.ModMainLabel.Text = "Mod Library";
            // 
            // ModPanelQuickAddMod
            // 
            this.ModPanelQuickAddMod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ModPanelQuickAddMod.Controls.Add(this.AddToModLibBT);
            this.ModPanelQuickAddMod.Controls.Add(this.addToExistingVehicle);
            this.ModPanelQuickAddMod.Controls.Add(this.saveModLibraryBt);
            this.ModPanelQuickAddMod.Controls.Add(this.createNewCarBt);
            this.ModPanelQuickAddMod.Controls.Add(this.QCreateNewModBt);
            this.ModPanelQuickAddMod.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ModPanelQuickAddMod.Location = new System.Drawing.Point(171, 568);
            this.ModPanelQuickAddMod.Name = "ModPanelQuickAddMod";
            this.ModPanelQuickAddMod.Size = new System.Drawing.Size(1266, 119);
            this.ModPanelQuickAddMod.TabIndex = 15;
            // 
            // createNewModPanel
            // 
            this.createNewModPanel.Controls.Add(this.LinkedModelListView);
            this.createNewModPanel.Controls.Add(this.TurnOffBoneRemove);
            this.createNewModPanel.Controls.Add(this.TunrOffBoneAddBt);
            this.createNewModPanel.Controls.Add(this.LinkedModelRemoveBT);
            this.createNewModPanel.Controls.Add(this.LinkedModelAddBT);
            this.createNewModPanel.Controls.Add(this.TurnOffBoneListView);
            this.createNewModPanel.Controls.Add(this.TurnOffBonesTextBox);
            this.createNewModPanel.Controls.Add(this.label4);
            this.createNewModPanel.Controls.Add(this.LinkedModelTextBox);
            this.createNewModPanel.Controls.Add(this.linkedModelLB);
            this.createNewModPanel.Controls.Add(this.allowBonnetSlideComboBox);
            this.createNewModPanel.Controls.Add(this.allowBonnetSlideLabel);
            this.createNewModPanel.Controls.Add(this.disableBonnetCameraComboBox);
            this.createNewModPanel.Controls.Add(this.label3);
            this.createNewModPanel.Controls.Add(this.turnOffExtraComboBox);
            this.createNewModPanel.Controls.Add(this.label2);
            this.createNewModPanel.Controls.Add(this.weightTextBox);
            this.createNewModPanel.Controls.Add(this.weightLB);
            this.createNewModPanel.Controls.Add(this.audioApplyTextBox);
            this.createNewModPanel.Controls.Add(this.audioApplyLB);
            this.createNewModPanel.Controls.Add(this.CollisionBoneTextBox);
            this.createNewModPanel.Controls.Add(this.label1);
            this.createNewModPanel.Controls.Add(this.saveModBt);
            this.createNewModPanel.Controls.Add(this.cameraPositionTextBox);
            this.createNewModPanel.Controls.Add(this.modItemShopLabel);
            this.createNewModPanel.Controls.Add(this.cameraPositionlabel);
            this.createNewModPanel.Controls.Add(this.modItemShopLabelTextBox);
            this.createNewModPanel.Controls.Add(this.ModTypeDropDown);
            this.createNewModPanel.Controls.Add(this.modModellabel);
            this.createNewModPanel.Controls.Add(this.modTypeLabel);
            this.createNewModPanel.Controls.Add(this.modShopMenuDescLabel);
            this.createNewModPanel.Controls.Add(this.modModelTextBox);
            this.createNewModPanel.Controls.Add(this.boneTextBox);
            this.createNewModPanel.Location = new System.Drawing.Point(173, 1);
            this.createNewModPanel.Name = "createNewModPanel";
            this.createNewModPanel.Size = new System.Drawing.Size(1266, 806);
            this.createNewModPanel.TabIndex = 31;
            // 
            // LinkedModelListView
            // 
            this.LinkedModelListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.LinkedModels});
            this.LinkedModelListView.GridLines = true;
            this.LinkedModelListView.Location = new System.Drawing.Point(215, 485);
            this.LinkedModelListView.MultiSelect = false;
            this.LinkedModelListView.Name = "LinkedModelListView";
            this.LinkedModelListView.Size = new System.Drawing.Size(362, 179);
            this.LinkedModelListView.TabIndex = 77;
            this.LinkedModelListView.UseCompatibleStateImageBehavior = false;
            this.LinkedModelListView.View = System.Windows.Forms.View.List;
            // 
            // LinkedModels
            // 
            this.LinkedModels.Width = 485;
            // 
            // TurnOffBoneRemove
            // 
            this.TurnOffBoneRemove.Location = new System.Drawing.Point(1063, 443);
            this.TurnOffBoneRemove.Name = "TurnOffBoneRemove";
            this.TurnOffBoneRemove.Size = new System.Drawing.Size(92, 31);
            this.TurnOffBoneRemove.TabIndex = 76;
            this.TurnOffBoneRemove.Text = "Remove";
            this.TurnOffBoneRemove.UseVisualStyleBackColor = true;
            this.TurnOffBoneRemove.Click += new System.EventHandler(this.TurnOffBoneRemove_Click);
            // 
            // TunrOffBoneAddBt
            // 
            this.TunrOffBoneAddBt.Location = new System.Drawing.Point(932, 443);
            this.TunrOffBoneAddBt.Name = "TunrOffBoneAddBt";
            this.TunrOffBoneAddBt.Size = new System.Drawing.Size(92, 31);
            this.TunrOffBoneAddBt.TabIndex = 75;
            this.TunrOffBoneAddBt.Text = "Add";
            this.TunrOffBoneAddBt.UseVisualStyleBackColor = true;
            this.TunrOffBoneAddBt.Click += new System.EventHandler(this.TunrOffBoneAddBt_Click);
            // 
            // LinkedModelRemoveBT
            // 
            this.LinkedModelRemoveBT.Location = new System.Drawing.Point(415, 443);
            this.LinkedModelRemoveBT.Name = "LinkedModelRemoveBT";
            this.LinkedModelRemoveBT.Size = new System.Drawing.Size(92, 31);
            this.LinkedModelRemoveBT.TabIndex = 74;
            this.LinkedModelRemoveBT.Text = "Remove";
            this.LinkedModelRemoveBT.UseVisualStyleBackColor = true;
            this.LinkedModelRemoveBT.Click += new System.EventHandler(this.LinkedModelRemoveBT_Click);
            // 
            // LinkedModelAddBT
            // 
            this.LinkedModelAddBT.Location = new System.Drawing.Point(285, 443);
            this.LinkedModelAddBT.Name = "LinkedModelAddBT";
            this.LinkedModelAddBT.Size = new System.Drawing.Size(92, 31);
            this.LinkedModelAddBT.TabIndex = 73;
            this.LinkedModelAddBT.Text = "Add";
            this.LinkedModelAddBT.UseVisualStyleBackColor = true;
            this.LinkedModelAddBT.Click += new System.EventHandler(this.LinkedModelAddBT_Click);
            // 
            // TurnOffBoneListView
            // 
            this.TurnOffBoneListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.TurnOffBones});
            this.TurnOffBoneListView.GridLines = true;
            this.TurnOffBoneListView.Location = new System.Drawing.Point(854, 485);
            this.TurnOffBoneListView.MultiSelect = false;
            this.TurnOffBoneListView.Name = "TurnOffBoneListView";
            this.TurnOffBoneListView.Size = new System.Drawing.Size(362, 179);
            this.TurnOffBoneListView.TabIndex = 72;
            this.TurnOffBoneListView.UseCompatibleStateImageBehavior = false;
            this.TurnOffBoneListView.View = System.Windows.Forms.View.List;
            // 
            // TurnOffBones
            // 
            this.TurnOffBones.Width = 485;
            // 
            // TurnOffBonesTextBox
            // 
            this.TurnOffBonesTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(234)))), ((int)(((byte)(234)))));
            this.TurnOffBonesTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TurnOffBonesTextBox.Location = new System.Drawing.Point(854, 399);
            this.TurnOffBonesTextBox.Multiline = true;
            this.TurnOffBonesTextBox.Name = "TurnOffBonesTextBox";
            this.TurnOffBonesTextBox.Size = new System.Drawing.Size(362, 28);
            this.TurnOffBonesTextBox.TabIndex = 70;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.LightBlue;
            this.label4.Location = new System.Drawing.Point(664, 403);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 20);
            this.label4.TabIndex = 69;
            this.label4.Text = "Turn Off Bones";
            // 
            // LinkedModelTextBox
            // 
            this.LinkedModelTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(234)))), ((int)(((byte)(234)))));
            this.LinkedModelTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LinkedModelTextBox.Location = new System.Drawing.Point(215, 399);
            this.LinkedModelTextBox.Multiline = true;
            this.LinkedModelTextBox.Name = "LinkedModelTextBox";
            this.LinkedModelTextBox.Size = new System.Drawing.Size(362, 28);
            this.LinkedModelTextBox.TabIndex = 68;
            // 
            // linkedModelLB
            // 
            this.linkedModelLB.AutoSize = true;
            this.linkedModelLB.ForeColor = System.Drawing.Color.LightBlue;
            this.linkedModelLB.Location = new System.Drawing.Point(35, 403);
            this.linkedModelLB.Name = "linkedModelLB";
            this.linkedModelLB.Size = new System.Drawing.Size(111, 20);
            this.linkedModelLB.TabIndex = 67;
            this.linkedModelLB.Text = "Linked Models";
            // 
            // allowBonnetSlideComboBox
            // 
            this.allowBonnetSlideComboBox.FormattingEnabled = true;
            this.allowBonnetSlideComboBox.Items.AddRange(new object[] {
            "true",
            "false"});
            this.allowBonnetSlideComboBox.Location = new System.Drawing.Point(854, 306);
            this.allowBonnetSlideComboBox.Name = "allowBonnetSlideComboBox";
            this.allowBonnetSlideComboBox.Size = new System.Drawing.Size(362, 28);
            this.allowBonnetSlideComboBox.TabIndex = 66;
            this.allowBonnetSlideComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.allowBonnetSlideComboBox_KeyDown);
            // 
            // allowBonnetSlideLabel
            // 
            this.allowBonnetSlideLabel.AutoSize = true;
            this.allowBonnetSlideLabel.ForeColor = System.Drawing.Color.LightBlue;
            this.allowBonnetSlideLabel.Location = new System.Drawing.Point(664, 310);
            this.allowBonnetSlideLabel.Name = "allowBonnetSlideLabel";
            this.allowBonnetSlideLabel.Size = new System.Drawing.Size(141, 20);
            this.allowBonnetSlideLabel.TabIndex = 65;
            this.allowBonnetSlideLabel.Text = "Allow Bonnet Slide";
            // 
            // disableBonnetCameraComboBox
            // 
            this.disableBonnetCameraComboBox.FormattingEnabled = true;
            this.disableBonnetCameraComboBox.Items.AddRange(new object[] {
            "true",
            "false"});
            this.disableBonnetCameraComboBox.Location = new System.Drawing.Point(854, 255);
            this.disableBonnetCameraComboBox.Name = "disableBonnetCameraComboBox";
            this.disableBonnetCameraComboBox.Size = new System.Drawing.Size(362, 28);
            this.disableBonnetCameraComboBox.TabIndex = 64;
            this.disableBonnetCameraComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.disableBonnetCameraComboBox_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.LightBlue;
            this.label3.Location = new System.Drawing.Point(664, 259);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(178, 20);
            this.label3.TabIndex = 63;
            this.label3.Text = "Disable Bonnet Camera";
            // 
            // turnOffExtraComboBox
            // 
            this.turnOffExtraComboBox.FormattingEnabled = true;
            this.turnOffExtraComboBox.Items.AddRange(new object[] {
            "true",
            "false"});
            this.turnOffExtraComboBox.Location = new System.Drawing.Point(854, 204);
            this.turnOffExtraComboBox.Name = "turnOffExtraComboBox";
            this.turnOffExtraComboBox.Size = new System.Drawing.Size(362, 28);
            this.turnOffExtraComboBox.TabIndex = 62;
            this.turnOffExtraComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.turnOffExtraComboBox_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.LightBlue;
            this.label2.Location = new System.Drawing.Point(664, 208);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 20);
            this.label2.TabIndex = 61;
            this.label2.Text = "Turn Off Extra";
            // 
            // weightTextBox
            // 
            this.weightTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(234)))), ((int)(((byte)(234)))));
            this.weightTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.weightTextBox.Location = new System.Drawing.Point(854, 153);
            this.weightTextBox.Multiline = true;
            this.weightTextBox.Name = "weightTextBox";
            this.weightTextBox.Size = new System.Drawing.Size(362, 28);
            this.weightTextBox.TabIndex = 58;
            this.weightTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.weightTextBox_KeyPress);
            // 
            // weightLB
            // 
            this.weightLB.AutoSize = true;
            this.weightLB.ForeColor = System.Drawing.Color.LightBlue;
            this.weightLB.Location = new System.Drawing.Point(664, 157);
            this.weightLB.Name = "weightLB";
            this.weightLB.Size = new System.Drawing.Size(59, 20);
            this.weightLB.TabIndex = 59;
            this.weightLB.Text = "Weight";
            // 
            // audioApplyTextBox
            // 
            this.audioApplyTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(234)))), ((int)(((byte)(234)))));
            this.audioApplyTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.audioApplyTextBox.Location = new System.Drawing.Point(854, 102);
            this.audioApplyTextBox.Multiline = true;
            this.audioApplyTextBox.Name = "audioApplyTextBox";
            this.audioApplyTextBox.Size = new System.Drawing.Size(362, 28);
            this.audioApplyTextBox.TabIndex = 56;
            this.audioApplyTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.audioApplyTextBox_KeyPress);
            // 
            // audioApplyLB
            // 
            this.audioApplyLB.AutoSize = true;
            this.audioApplyLB.ForeColor = System.Drawing.Color.LightBlue;
            this.audioApplyLB.Location = new System.Drawing.Point(664, 106);
            this.audioApplyLB.Name = "audioApplyLB";
            this.audioApplyLB.Size = new System.Drawing.Size(93, 20);
            this.audioApplyLB.TabIndex = 57;
            this.audioApplyLB.Text = "Audio Apply";
            // 
            // CollisionBoneTextBox
            // 
            this.CollisionBoneTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(234)))), ((int)(((byte)(234)))));
            this.CollisionBoneTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CollisionBoneTextBox.Location = new System.Drawing.Point(215, 255);
            this.CollisionBoneTextBox.Multiline = true;
            this.CollisionBoneTextBox.Name = "CollisionBoneTextBox";
            this.CollisionBoneTextBox.Size = new System.Drawing.Size(362, 28);
            this.CollisionBoneTextBox.TabIndex = 47;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.LightBlue;
            this.label1.Location = new System.Drawing.Point(35, 259);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 20);
            this.label1.TabIndex = 55;
            this.label1.Text = "Collision Bone";
            // 
            // cameraPositionTextBox
            // 
            this.cameraPositionTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(234)))), ((int)(((byte)(234)))));
            this.cameraPositionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cameraPositionTextBox.Location = new System.Drawing.Point(215, 306);
            this.cameraPositionTextBox.Multiline = true;
            this.cameraPositionTextBox.Name = "cameraPositionTextBox";
            this.cameraPositionTextBox.Size = new System.Drawing.Size(362, 28);
            this.cameraPositionTextBox.TabIndex = 48;
            // 
            // modItemShopLabel
            // 
            this.modItemShopLabel.AutoSize = true;
            this.modItemShopLabel.ForeColor = System.Drawing.Color.LightBlue;
            this.modItemShopLabel.Location = new System.Drawing.Point(35, 106);
            this.modItemShopLabel.Name = "modItemShopLabel";
            this.modItemShopLabel.Size = new System.Drawing.Size(161, 20);
            this.modItemShopLabel.TabIndex = 51;
            this.modItemShopLabel.Text = "Mod Item Shop Label";
            // 
            // cameraPositionlabel
            // 
            this.cameraPositionlabel.AutoSize = true;
            this.cameraPositionlabel.ForeColor = System.Drawing.Color.LightBlue;
            this.cameraPositionlabel.Location = new System.Drawing.Point(35, 310);
            this.cameraPositionlabel.Name = "cameraPositionlabel";
            this.cameraPositionlabel.Size = new System.Drawing.Size(125, 20);
            this.cameraPositionlabel.TabIndex = 52;
            this.cameraPositionlabel.Text = "Camera Position";
            // 
            // modItemShopLabelTextBox
            // 
            this.modItemShopLabelTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(234)))), ((int)(((byte)(234)))));
            this.modItemShopLabelTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.modItemShopLabelTextBox.Location = new System.Drawing.Point(215, 102);
            this.modItemShopLabelTextBox.Multiline = true;
            this.modItemShopLabelTextBox.Name = "modItemShopLabelTextBox";
            this.modItemShopLabelTextBox.Size = new System.Drawing.Size(362, 28);
            this.modItemShopLabelTextBox.TabIndex = 44;
            // 
            // ModTypeDropDown
            // 
            this.ModTypeDropDown.FormattingEnabled = true;
            this.ModTypeDropDown.Items.AddRange(new object[] {
            "VMT_SPOILER",
            "VMT_BUMPER_F",
            "VMT_BUMPER_R",
            "VMT_SKIRT",
            "VMT_EXHAUST",
            "VMT_CHASSIS",
            "VMT_CHASSIS2",
            "VMT_CHASSIS3",
            "VMT_CHASSIS4",
            "VMT_CHASSIS5",
            "VMT_GRILL",
            "VMT_BONNET",
            "VMT_WING_L",
            "VMT_WING_R",
            "VMT_ROOF",
            "VMT_ENGINE",
            "VMT_BRAKES",
            "VMT_GEARBOX",
            "VMT_HORN",
            "VMT_SUSPENSION",
            "VMT_ARMOUR",
            "VMT_PLTVANITY",
            "VMT_PLTHOLDER",
            "VMT_INTERIOR1",
            "VMT_INTERIOR2",
            "VMT_INTERIOR3",
            "VMT_INTERIOR4",
            "VMT_INTERIOR5",
            "VMT_SEATS",
            "VMT_STEERING",
            "VMT_KNB",
            "VMT_PLAQUE",
            "VMT_ICE",
            "VMT_TRUNK",
            "VMT_HYDRO",
            "VMT_ENGINEBAY1",
            "VMT_ENGINEBAY2",
            "VMT_ENGINEBAY3",
            "VMT_DOOR_L",
            "VMT_DOOR_R",
            "VMT_LIVERY_MOD"});
            this.ModTypeDropDown.Location = new System.Drawing.Point(215, 153);
            this.ModTypeDropDown.Name = "ModTypeDropDown";
            this.ModTypeDropDown.Size = new System.Drawing.Size(362, 28);
            this.ModTypeDropDown.TabIndex = 45;
            this.ModTypeDropDown.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ModTypeDropDown_KeyPress);
            // 
            // modModellabel
            // 
            this.modModellabel.AutoSize = true;
            this.modModellabel.ForeColor = System.Drawing.Color.LightBlue;
            this.modModellabel.Location = new System.Drawing.Point(383, 44);
            this.modModellabel.Name = "modModellabel";
            this.modModellabel.Size = new System.Drawing.Size(98, 20);
            this.modModellabel.TabIndex = 47;
            this.modModellabel.Text = "Model Name";
            // 
            // modTypeLabel
            // 
            this.modTypeLabel.AutoSize = true;
            this.modTypeLabel.ForeColor = System.Drawing.Color.LightBlue;
            this.modTypeLabel.Location = new System.Drawing.Point(35, 157);
            this.modTypeLabel.Name = "modTypeLabel";
            this.modTypeLabel.Size = new System.Drawing.Size(78, 20);
            this.modTypeLabel.TabIndex = 45;
            this.modTypeLabel.Text = "Mod Type";
            // 
            // modShopMenuDescLabel
            // 
            this.modShopMenuDescLabel.AutoSize = true;
            this.modShopMenuDescLabel.ForeColor = System.Drawing.Color.LightBlue;
            this.modShopMenuDescLabel.Location = new System.Drawing.Point(35, 208);
            this.modShopMenuDescLabel.Name = "modShopMenuDescLabel";
            this.modShopMenuDescLabel.Size = new System.Drawing.Size(47, 20);
            this.modShopMenuDescLabel.TabIndex = 46;
            this.modShopMenuDescLabel.Text = "Bone";
            // 
            // modModelTextBox
            // 
            this.modModelTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(234)))), ((int)(((byte)(234)))));
            this.modModelTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.modModelTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modModelTextBox.Location = new System.Drawing.Point(522, 40);
            this.modModelTextBox.Multiline = true;
            this.modModelTextBox.Name = "modModelTextBox";
            this.modModelTextBox.Size = new System.Drawing.Size(362, 28);
            this.modModelTextBox.TabIndex = 43;
            // 
            // boneTextBox
            // 
            this.boneTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(234)))), ((int)(((byte)(234)))));
            this.boneTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.boneTextBox.Location = new System.Drawing.Point(215, 204);
            this.boneTextBox.Multiline = true;
            this.boneTextBox.Name = "boneTextBox";
            this.boneTextBox.Size = new System.Drawing.Size(362, 28);
            this.boneTextBox.TabIndex = 46;
            // 
            // previewQuickPanel
            // 
            this.previewQuickPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.previewQuickPanel.Controls.Add(this.savePreviewBt);
            this.previewQuickPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.previewQuickPanel.Location = new System.Drawing.Point(171, 449);
            this.previewQuickPanel.Name = "previewQuickPanel";
            this.previewQuickPanel.Size = new System.Drawing.Size(1266, 119);
            this.previewQuickPanel.TabIndex = 45;
            // 
            // savePreviewBt
            // 
            this.savePreviewBt.BackColor = System.Drawing.Color.Teal;
            this.savePreviewBt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.savePreviewBt.FlatAppearance.BorderSize = 0;
            this.savePreviewBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.savePreviewBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savePreviewBt.ForeColor = System.Drawing.Color.White;
            this.savePreviewBt.Image = ((System.Drawing.Image)(resources.GetObject("savePreviewBt.Image")));
            this.savePreviewBt.Location = new System.Drawing.Point(582, 6);
            this.savePreviewBt.Name = "savePreviewBt";
            this.savePreviewBt.Size = new System.Drawing.Size(106, 106);
            this.savePreviewBt.TabIndex = 14;
            this.savePreviewBt.TabStop = false;
            this.savePreviewBt.Text = "Save";
            this.savePreviewBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.savePreviewBt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.savePreviewBt.UseVisualStyleBackColor = true;
            this.savePreviewBt.Click += new System.EventHandler(this.savePreviewBt_Click);
            // 
            // previewTextBox
            // 
            this.previewTextBox.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.previewTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.previewTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.previewTextBox.Font = new System.Drawing.Font("Consolas", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.previewTextBox.Location = new System.Drawing.Point(0, 0);
            this.previewTextBox.Name = "previewTextBox";
            this.previewTextBox.Size = new System.Drawing.Size(1265, 683);
            this.previewTextBox.TabIndex = 12;
            this.previewTextBox.Text = "";
            this.previewTextBox.Xml = "";
            // 
            // saveModBt
            // 
            this.saveModBt.BackColor = System.Drawing.Color.Teal;
            this.saveModBt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.saveModBt.FlatAppearance.BorderSize = 0;
            this.saveModBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveModBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveModBt.ForeColor = System.Drawing.Color.White;
            this.saveModBt.Image = ((System.Drawing.Image)(resources.GetObject("saveModBt.Image")));
            this.saveModBt.Location = new System.Drawing.Point(580, 687);
            this.saveModBt.Name = "saveModBt";
            this.saveModBt.Size = new System.Drawing.Size(106, 106);
            this.saveModBt.TabIndex = 53;
            this.saveModBt.TabStop = false;
            this.saveModBt.Text = "Save Mod";
            this.saveModBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.saveModBt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.saveModBt.UseVisualStyleBackColor = true;
            this.saveModBt.Click += new System.EventHandler(this.saveModBt_Click);
            // 
            // AddToModLibBT
            // 
            this.AddToModLibBT.BackColor = System.Drawing.Color.Teal;
            this.AddToModLibBT.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.AddToModLibBT.FlatAppearance.BorderSize = 0;
            this.AddToModLibBT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddToModLibBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddToModLibBT.ForeColor = System.Drawing.Color.White;
            this.AddToModLibBT.Image = ((System.Drawing.Image)(resources.GetObject("AddToModLibBT.Image")));
            this.AddToModLibBT.Location = new System.Drawing.Point(852, 5);
            this.AddToModLibBT.Name = "AddToModLibBT";
            this.AddToModLibBT.Size = new System.Drawing.Size(106, 106);
            this.AddToModLibBT.TabIndex = 15;
            this.AddToModLibBT.TabStop = false;
            this.AddToModLibBT.Text = "Add To Mod Library";
            this.AddToModLibBT.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AddToModLibBT.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.AddToModLibBT.UseVisualStyleBackColor = false;
            this.AddToModLibBT.Click += new System.EventHandler(this.AddToModLibBT_Click);
            // 
            // addToExistingVehicle
            // 
            this.addToExistingVehicle.BackColor = System.Drawing.Color.Teal;
            this.addToExistingVehicle.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.addToExistingVehicle.FlatAppearance.BorderSize = 0;
            this.addToExistingVehicle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addToExistingVehicle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addToExistingVehicle.ForeColor = System.Drawing.Color.White;
            this.addToExistingVehicle.Image = ((System.Drawing.Image)(resources.GetObject("addToExistingVehicle.Image")));
            this.addToExistingVehicle.Location = new System.Drawing.Point(578, 5);
            this.addToExistingVehicle.Name = "addToExistingVehicle";
            this.addToExistingVehicle.Size = new System.Drawing.Size(106, 106);
            this.addToExistingVehicle.TabIndex = 14;
            this.addToExistingVehicle.TabStop = false;
            this.addToExistingVehicle.Text = "Add To Existing Vehicle";
            this.addToExistingVehicle.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.addToExistingVehicle.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.addToExistingVehicle.UseVisualStyleBackColor = false;
            this.addToExistingVehicle.Click += new System.EventHandler(this.addToExistingVehicle_Click);
            // 
            // saveModLibraryBt
            // 
            this.saveModLibraryBt.BackColor = System.Drawing.Color.Teal;
            this.saveModLibraryBt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.saveModLibraryBt.FlatAppearance.BorderSize = 0;
            this.saveModLibraryBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveModLibraryBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveModLibraryBt.ForeColor = System.Drawing.Color.White;
            this.saveModLibraryBt.Image = ((System.Drawing.Image)(resources.GetObject("saveModLibraryBt.Image")));
            this.saveModLibraryBt.Location = new System.Drawing.Point(1126, 5);
            this.saveModLibraryBt.Name = "saveModLibraryBt";
            this.saveModLibraryBt.Size = new System.Drawing.Size(106, 106);
            this.saveModLibraryBt.TabIndex = 13;
            this.saveModLibraryBt.TabStop = false;
            this.saveModLibraryBt.Text = "Save Mod Library";
            this.saveModLibraryBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.saveModLibraryBt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.saveModLibraryBt.UseVisualStyleBackColor = false;
            this.saveModLibraryBt.Click += new System.EventHandler(this.saveModLibraryBt_Click);
            // 
            // createNewCarBt
            // 
            this.createNewCarBt.BackColor = System.Drawing.Color.Teal;
            this.createNewCarBt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.createNewCarBt.FlatAppearance.BorderSize = 0;
            this.createNewCarBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createNewCarBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createNewCarBt.ForeColor = System.Drawing.Color.White;
            this.createNewCarBt.Image = ((System.Drawing.Image)(resources.GetObject("createNewCarBt.Image")));
            this.createNewCarBt.Location = new System.Drawing.Point(304, 3);
            this.createNewCarBt.Name = "createNewCarBt";
            this.createNewCarBt.Size = new System.Drawing.Size(106, 106);
            this.createNewCarBt.TabIndex = 12;
            this.createNewCarBt.TabStop = false;
            this.createNewCarBt.Text = "Create New Vehicle";
            this.createNewCarBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.createNewCarBt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.createNewCarBt.UseVisualStyleBackColor = false;
            this.createNewCarBt.Click += new System.EventHandler(this.createNewVehicleBt_Click);
            // 
            // QCreateNewModBt
            // 
            this.QCreateNewModBt.BackColor = System.Drawing.Color.Teal;
            this.QCreateNewModBt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.QCreateNewModBt.FlatAppearance.BorderSize = 0;
            this.QCreateNewModBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.QCreateNewModBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QCreateNewModBt.ForeColor = System.Drawing.Color.White;
            this.QCreateNewModBt.Image = ((System.Drawing.Image)(resources.GetObject("QCreateNewModBt.Image")));
            this.QCreateNewModBt.Location = new System.Drawing.Point(30, 4);
            this.QCreateNewModBt.Name = "QCreateNewModBt";
            this.QCreateNewModBt.Size = new System.Drawing.Size(106, 106);
            this.QCreateNewModBt.TabIndex = 11;
            this.QCreateNewModBt.TabStop = false;
            this.QCreateNewModBt.Text = "Create New Mod";
            this.QCreateNewModBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.QCreateNewModBt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.QCreateNewModBt.UseVisualStyleBackColor = false;
            this.QCreateNewModBt.Click += new System.EventHandler(this.createNewModBt_Click);
            // 
            // clearCarcolsBT
            // 
            this.clearCarcolsBT.BackColor = System.Drawing.Color.Teal;
            this.clearCarcolsBT.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.clearCarcolsBT.FlatAppearance.BorderSize = 0;
            this.clearCarcolsBT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clearCarcolsBT.ForeColor = System.Drawing.Color.White;
            this.clearCarcolsBT.Image = ((System.Drawing.Image)(resources.GetObject("clearCarcolsBT.Image")));
            this.clearCarcolsBT.Location = new System.Drawing.Point(552, 412);
            this.clearCarcolsBT.Name = "clearCarcolsBT";
            this.clearCarcolsBT.Size = new System.Drawing.Size(160, 160);
            this.clearCarcolsBT.TabIndex = 14;
            this.clearCarcolsBT.TabStop = false;
            this.clearCarcolsBT.Text = "Clear Current Carcols";
            this.clearCarcolsBT.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.clearCarcolsBT.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.clearCarcolsBT.UseVisualStyleBackColor = false;
            this.clearCarcolsBT.Click += new System.EventHandler(this.clearCarcolsBT_Click);
            // 
            // loadModLibrary
            // 
            this.loadModLibrary.BackColor = System.Drawing.Color.Teal;
            this.loadModLibrary.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.loadModLibrary.FlatAppearance.BorderSize = 0;
            this.loadModLibrary.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loadModLibrary.ForeColor = System.Drawing.Color.White;
            this.loadModLibrary.Image = ((System.Drawing.Image)(resources.GetObject("loadModLibrary.Image")));
            this.loadModLibrary.Location = new System.Drawing.Point(794, 221);
            this.loadModLibrary.Name = "loadModLibrary";
            this.loadModLibrary.Size = new System.Drawing.Size(160, 160);
            this.loadModLibrary.TabIndex = 13;
            this.loadModLibrary.TabStop = false;
            this.loadModLibrary.Text = "Load Mod Library";
            this.loadModLibrary.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.loadModLibrary.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.loadModLibrary.UseVisualStyleBackColor = false;
            this.loadModLibrary.Click += new System.EventHandler(this.loadModLibrary_Click);
            // 
            // createNewModBt
            // 
            this.createNewModBt.BackColor = System.Drawing.Color.Teal;
            this.createNewModBt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.createNewModBt.FlatAppearance.BorderSize = 0;
            this.createNewModBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createNewModBt.ForeColor = System.Drawing.Color.White;
            this.createNewModBt.Image = ((System.Drawing.Image)(resources.GetObject("createNewModBt.Image")));
            this.createNewModBt.Location = new System.Drawing.Point(310, 221);
            this.createNewModBt.Name = "createNewModBt";
            this.createNewModBt.Size = new System.Drawing.Size(160, 160);
            this.createNewModBt.TabIndex = 12;
            this.createNewModBt.TabStop = false;
            this.createNewModBt.Text = "Create New Mod";
            this.createNewModBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.createNewModBt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.createNewModBt.UseVisualStyleBackColor = false;
            this.createNewModBt.Click += new System.EventHandler(this.createNewModBt_Click);
            // 
            // loadExistingFile
            // 
            this.loadExistingFile.BackColor = System.Drawing.Color.Teal;
            this.loadExistingFile.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.loadExistingFile.FlatAppearance.BorderSize = 0;
            this.loadExistingFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loadExistingFile.ForeColor = System.Drawing.Color.White;
            this.loadExistingFile.Image = ((System.Drawing.Image)(resources.GetObject("loadExistingFile.Image")));
            this.loadExistingFile.Location = new System.Drawing.Point(552, 221);
            this.loadExistingFile.Name = "loadExistingFile";
            this.loadExistingFile.Size = new System.Drawing.Size(160, 160);
            this.loadExistingFile.TabIndex = 11;
            this.loadExistingFile.TabStop = false;
            this.loadExistingFile.Text = "Load Carcols File";
            this.loadExistingFile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.loadExistingFile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.loadExistingFile.UseVisualStyleBackColor = false;
            this.loadExistingFile.Click += new System.EventHandler(this.loadExistingCarcols_Click);
            // 
            // saveVehBt
            // 
            this.saveVehBt.BackColor = System.Drawing.Color.Teal;
            this.saveVehBt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.saveVehBt.FlatAppearance.BorderSize = 0;
            this.saveVehBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveVehBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveVehBt.ForeColor = System.Drawing.Color.White;
            this.saveVehBt.Image = ((System.Drawing.Image)(resources.GetObject("saveVehBt.Image")));
            this.saveVehBt.Location = new System.Drawing.Point(580, 552);
            this.saveVehBt.Name = "saveVehBt";
            this.saveVehBt.Size = new System.Drawing.Size(106, 106);
            this.saveVehBt.TabIndex = 51;
            this.saveVehBt.TabStop = false;
            this.saveVehBt.Text = "Save Vehicle";
            this.saveVehBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.saveVehBt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.saveVehBt.UseVisualStyleBackColor = true;
            this.saveVehBt.Click += new System.EventHandler(this.saveVehBt_Click);
            // 
            // saveToExistingVehicleBt
            // 
            this.saveToExistingVehicleBt.BackColor = System.Drawing.Color.Teal;
            this.saveToExistingVehicleBt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.saveToExistingVehicleBt.FlatAppearance.BorderSize = 0;
            this.saveToExistingVehicleBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveToExistingVehicleBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveToExistingVehicleBt.ForeColor = System.Drawing.Color.White;
            this.saveToExistingVehicleBt.Image = ((System.Drawing.Image)(resources.GetObject("saveToExistingVehicleBt.Image")));
            this.saveToExistingVehicleBt.Location = new System.Drawing.Point(579, 5);
            this.saveToExistingVehicleBt.Name = "saveToExistingVehicleBt";
            this.saveToExistingVehicleBt.Size = new System.Drawing.Size(106, 107);
            this.saveToExistingVehicleBt.TabIndex = 13;
            this.saveToExistingVehicleBt.TabStop = false;
            this.saveToExistingVehicleBt.Text = "Save";
            this.saveToExistingVehicleBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.saveToExistingVehicleBt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.saveToExistingVehicleBt.UseVisualStyleBackColor = false;
            this.saveToExistingVehicleBt.Visible = false;
            this.saveToExistingVehicleBt.Click += new System.EventHandler(this.saveToExistingVehicle_Click);
            // 
            // exportCarcolsDataBt
            // 
            this.exportCarcolsDataBt.BackColor = System.Drawing.Color.Teal;
            this.exportCarcolsDataBt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.exportCarcolsDataBt.FlatAppearance.BorderSize = 0;
            this.exportCarcolsDataBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exportCarcolsDataBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exportCarcolsDataBt.ForeColor = System.Drawing.Color.White;
            this.exportCarcolsDataBt.Image = ((System.Drawing.Image)(resources.GetObject("exportCarcolsDataBt.Image")));
            this.exportCarcolsDataBt.Location = new System.Drawing.Point(579, 5);
            this.exportCarcolsDataBt.Name = "exportCarcolsDataBt";
            this.exportCarcolsDataBt.Size = new System.Drawing.Size(106, 107);
            this.exportCarcolsDataBt.TabIndex = 12;
            this.exportCarcolsDataBt.TabStop = false;
            this.exportCarcolsDataBt.Text = "Export Carcols Data";
            this.exportCarcolsDataBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.exportCarcolsDataBt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.exportCarcolsDataBt.UseVisualStyleBackColor = false;
            this.exportCarcolsDataBt.Click += new System.EventHandler(this.exportCarcolsDataBt_Click);
            // 
            // MainPage
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(45)))));
            this.ClientSize = new System.Drawing.Size(1437, 806);
            this.Controls.Add(this.dashboardMainPanel);
            this.Controls.Add(this.previewQuickPanel);
            this.Controls.Add(this.previewMainPanel);
            this.Controls.Add(this.createNewModPanel);
            this.Controls.Add(this.ModPanelQuickAddMod);
            this.Controls.Add(this.createNewVehPanel);
            this.Controls.Add(this.modMainPanel);
            this.Controls.Add(this.vehicleMainPanel);
            this.Controls.Add(this.VehiclePanelQuickAddVeh);
            this.Controls.Add(this.leftPanel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainPage";
            this.Text = "R* Carcols Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainPage_FormClosing);
            this.leftPanel.ResumeLayout(false);
            this.dashboardMainPanel.ResumeLayout(false);
            this.createNewVehPanel.ResumeLayout(false);
            this.createNewVehPanel.PerformLayout();
            this.previewMainPanel.ResumeLayout(false);
            this.vehicleMainPanel.ResumeLayout(false);
            this.vehicleMainPanel.PerformLayout();
            this.VehiclePanelQuickAddVeh.ResumeLayout(false);
            this.modMainPanel.ResumeLayout(false);
            this.modMainPanel.PerformLayout();
            this.ModPanelQuickAddMod.ResumeLayout(false);
            this.createNewModPanel.ResumeLayout(false);
            this.createNewModPanel.PerformLayout();
            this.previewQuickPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

