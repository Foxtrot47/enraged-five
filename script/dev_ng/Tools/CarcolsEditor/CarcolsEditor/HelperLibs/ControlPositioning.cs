﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarcolsEditor.HelperLibs
{
    public static class ControlPositioning
    {
        public static void CenterVertically(Control control)
        {
            Rectangle parentRect = control.Parent.ClientRectangle;
            control.Top = (parentRect.Height - control.Height) / 2;
        }

        public static void CenterHorizontally(Control control)
        {
            Rectangle parentRect = control.Parent.ClientRectangle;
            control.Left = (parentRect.Width - control.Width) / 2;
        }

        public static void Center(Control control)
        {
            CenterHorizontally(control);
            CenterVertically(control);
        }
    }
}
