﻿using System.Reflection;
using System.Runtime.InteropServices;

namespace CarcolsEditor
{
    public class ProgramInfo
    {
        static public string AssemblyGuid
        {
            get
            {
                var attribute = (GuidAttribute)Assembly.GetEntryAssembly().GetCustomAttributes(typeof(GuidAttribute), true)[0];
                return attribute.Value;
            }
        }
    }
}
