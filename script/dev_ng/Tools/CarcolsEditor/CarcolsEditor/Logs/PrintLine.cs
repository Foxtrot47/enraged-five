﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarcolsEditor.Logs
{
    public static class PrintLine
    {
        private static bool _started;
        private static FileStream _logFileStream;
        private static StreamWriter _logFileWriter;

        public static void Initialise()
        {
            string logName = "Carcols_Editor_Log.log";

            string fileName = @"X:\americas\build\dev";
            _logFileStream = new FileStream(fileName + "\\" + logName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read);
            _logFileWriter = new StreamWriter(_logFileStream, Encoding.Unicode) { AutoFlush = true };

            _started = true;

            WriteLine(" ***** Started Carcols Editor ****** ");
        }

        public static void WriteLine(string message, bool assert = false)
        {
            if (!_started) return;

            string line = ConstructLine(message);

            Debug.Print(line);
            _logFileWriter.WriteLine(line);

            if (assert)
            {
                MessageBox.Show(message, $"Assert - ", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private static string ConstructLine(string message)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            sb.Append(DateTime.UtcNow.ToString("H:mm:ss - dd/MM/yy"));
            sb.Append("]");
            sb.Append(" :: ");
            sb.Append(message);
            return sb.ToString();
        }

    }
}
