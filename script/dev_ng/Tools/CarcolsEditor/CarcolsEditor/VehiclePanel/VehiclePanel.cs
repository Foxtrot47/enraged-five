﻿using System;
using System.Windows.Forms;
using CarcolsEditor.HelperLibs;
using CarcolsEditor.Logs;
using CarcolsEditor.Managers;

namespace CarcolsEditor.VehPanel
{
    public class VehiclePanel
    {
        public Panel _BackgrounPanel { get; set; }
        private Button _EditBt;
        private Button _DeleteBt;
        private Label _PanelTitle;
        private Label _numberOfItems;
        public CheckBox SelectCheckB { get; set; }
        private PanelType _PanelType;

        const int VEHICLE_PANEL_OFFSET = 113;
        const int VEHICLE_PANEL_START_X = 20;
        const int VEHICLE_PANEL_START_Y = 80;

        public VehiclePanel(int vehiclePanelIndex, PanelType type)
        {
            this._BackgrounPanel = new Panel();
            this._EditBt = new Button();
            this._DeleteBt = new Button();
            this._PanelTitle = new Label();
            this._numberOfItems = new Label();
            this.SelectCheckB = new CheckBox();

            this._PanelType = type;

            this._BackgrounPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(54)))), ((int)(((byte)(66)))));
            this._BackgrounPanel.Location = new System.Drawing.Point(VEHICLE_PANEL_START_X, (vehiclePanelIndex * VEHICLE_PANEL_OFFSET) + VEHICLE_PANEL_START_Y);

            if (type == PanelType.TypeVehicles)
            {
                this._BackgrounPanel.Name = "vehiclePanel_" + vehiclePanelIndex;
            }
            else
            {
                this._BackgrounPanel.Name = "modPanel_" + vehiclePanelIndex;
            }
            this._BackgrounPanel.Size = new System.Drawing.Size(1220, 100);
            this._BackgrounPanel.TabIndex = vehiclePanelIndex;
        }

        private void AddVehiclePanelFieldsToMainPanel(Control MainPanel)
        {

            this._BackgrounPanel.Controls.Add(_EditBt);
            this._BackgrounPanel.Controls.Add(_DeleteBt);
            this._BackgrounPanel.Controls.Add(_PanelTitle);
            this._BackgrounPanel.Controls.Add(_numberOfItems);
            this._BackgrounPanel.Controls.Add(SelectCheckB);

            if (MainPanel.InvokeRequired)
            {
                MainPanel.BeginInvoke(new MethodInvoker(() => MainPanel.Controls.Add(this._BackgrounPanel)));
            }
            else
            {
                MainPanel.Controls.Add(this._BackgrounPanel);
            }

            PrintLine.WriteLine("[VehiclePanel] AddVehiclePanelFieldsToMainPanel");
        }

        private void SetupVehiclePanelButtons(int vehiclePanelIndex, System.ComponentModel.ComponentResourceManager resources)
        {
            // Edit button 
            this._EditBt.BackColor = System.Drawing.Color.Teal;
            this._EditBt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this._EditBt.FlatAppearance.BorderSize = 0;
            this._EditBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._EditBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._EditBt.ForeColor = System.Drawing.Color.White;
            this._EditBt.Image = Properties.Resources.edit;
            this._EditBt.Location = new System.Drawing.Point(1022, 12);
            this._EditBt.Name = "vehicleEditBt_" + vehiclePanelIndex;
            this._EditBt.Size = new System.Drawing.Size(72, 73);
            this._EditBt.TabStop = false;
            this._EditBt.Text = "Edit";
            this._EditBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this._EditBt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._EditBt.UseVisualStyleBackColor = true;
            this._EditBt.Click += new System.EventHandler(this.EditPanelBt_Click);
            ControlPositioning.CenterVertically(_EditBt);

            // Delete button 
            this._DeleteBt.BackColor = System.Drawing.Color.Teal;
            this._DeleteBt.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this._DeleteBt.FlatAppearance.BorderSize = 0;
            this._DeleteBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._DeleteBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DeleteBt.ForeColor = System.Drawing.Color.White;
            this._DeleteBt.Image = Properties.Resources.delete;
            this._DeleteBt.Location = new System.Drawing.Point(1135, 12);
            this._DeleteBt.Name = "vehicleDeleteBt_" + vehiclePanelIndex;
            this._DeleteBt.Size = new System.Drawing.Size(72, 73);
            this._DeleteBt.TabStop = false;
            this._DeleteBt.Text = "Delete";
            this._DeleteBt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this._DeleteBt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._DeleteBt.UseVisualStyleBackColor = true;
            this._DeleteBt.Click += new System.EventHandler(this.DeletePanelBt_Click);
            ControlPositioning.CenterVertically(_DeleteBt);

            // Select Check Box 
            if (_PanelType == PanelType.TypeModLibrary || VehicleModManager.Instance.AddToExistingVehicle)
            {
                this.SelectCheckB.AutoSize = true;
                this.SelectCheckB.ForeColor = System.Drawing.Color.White;
                this.SelectCheckB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.0F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.SelectCheckB.Location = new System.Drawing.Point(900, 38);

                if (_PanelType == PanelType.TypeModLibrary)
                {
                    this.SelectCheckB.Name = "modSelectCheckBox" + vehiclePanelIndex;
                    this.SelectCheckB.Text = "Select Mod";
                    if (VehicleModManager.Instance.EditingVehiclePanel)
                    {
                        this.SelectCheckB.Text = "Add To Mod Library";
                        this.SelectCheckB.Location = new System.Drawing.Point(830, 38);
                    }
                }
                else
                {
                    this.SelectCheckB.Name = "VehicleSelectCheckBox" + vehiclePanelIndex;
                    this.SelectCheckB.Text = "Select Vehicle";
                }

                this.SelectCheckB.Size = new System.Drawing.Size(88, 19);
                this.SelectCheckB.UseVisualStyleBackColor = true;
                ControlPositioning.CenterVertically(SelectCheckB);
            }
            else
            {
                this.SelectCheckB.Visible = false;
            }

            PrintLine.WriteLine("[VehiclePanel] SetupVehiclePanelButtons vehiclePanelIndex: " + vehiclePanelIndex);
        }

        private void EditPanelBt_Click(object sender, EventArgs e)
        {
            var editBt = sender as Button;
            if (editBt != null)
            {
                if (GetPanelIndex(editBt.Name) == -1)
                {
                    PrintLine.WriteLine("[VehiclePanel] EditPanelBt_Click panel index is -1", true);
                    return;
                }

                VehicleModManager.Instance.EditingMode = true;

                switch (_PanelType)
                {
                    case PanelType.TypeModLibrary:
                        {
                            VehicleModManager.Instance.EditingModPanel = true;
                            VehicleModManager.Instance.EditingModIndex = GetPanelIndex(editBt.Name);
                        }
                        break;
                    case PanelType.TypeVehicles:
                        {
                            VehicleModManager.Instance.EditingVehiclePanel = true;
                            VehicleModManager.Instance.EditingVehicleIndex = GetPanelIndex(editBt.Name);
                        }
                        break;
                }

                PrintLine.WriteLine("[VehiclePanel] EditPanelBt_Click EditingMode = true _PanelType: " + _PanelType);
                PrintLine.WriteLine("[VehiclePanel] EditPanelBt_Click EditingMode = true EditingVehicleIndex: " + VehicleModManager.Instance.EditingModIndex);
                PrintLine.WriteLine("[VehiclePanel] EditPanelBt_Click EditingMode = true EditingVehicleIndex: " + VehicleModManager.Instance.EditingVehicleIndex);
            }
            else
            {
                PrintLine.WriteLine("[VehiclePanel] EditPanelBt_Click editBt is NULL!", true);
            }
        }

        public int GetPanelIndex(string ItemName)
        {
            if (ItemName == string.Empty)
            {
                return -1;
            }

            string btNameDigit = string.Empty;
            int btId;

            for (int i = 0; i < ItemName.Length; i++)
            {
                if (char.IsDigit(ItemName[i]))
                {
                    btNameDigit += ItemName[i];
                }
            }

            if (ItemName.Length > 0)
            {
                btId = int.Parse(btNameDigit);
                PrintLine.WriteLine("[VehiclePanel] GetPanelIndex btId: " + btId);
                return btId;
            }
            return -1;
        }

        private void DeletePanelBt_Click(object sender, EventArgs e)
        {
            string MessageText = "";
            if (_PanelType == PanelType.TypeVehicles)
            {
                MessageText = "Are you sure you want to Delete this Vehicle?";
            }
            else
            {
                MessageText = "Are you sure you want to Delete this Mod?";
            }

            if (DialogResult.Yes == MessageBox.Show(MessageText, "DELETE", MessageBoxButtons.YesNo))
            {
                var deleteBt = sender as Button;
                if (deleteBt != null)
                {
                    int DeletePanelIndex = GetPanelIndex(deleteBt.Name);
                    if (DeletePanelIndex == -1)
                    {
                        PrintLine.WriteLine("[VehiclePanel] DeletePanelBt_Click panel index is -1", true);
                        return;
                    }

                    if (_PanelType == PanelType.TypeVehicles)
                    {
                        CarcolsManager.Instance.RemoveVehicleFromCarcols(DeletePanelIndex);
                        VehiclePanelManager.Instance.RebuildAllVehiclePanels();
                        VehicleModManager.Instance.UpdatePreviewPanel = true;
                    }
                    else
                    {
                        VehicleModManager.Instance.RemoveModInManagerModList(DeletePanelIndex);
                        VehiclePanelManager.Instance.RebuildAllModPanels();
                        VehicleModManager.Instance.UpdatePreviewPanel = true;
                    }
                }
                else
                {
                    PrintLine.WriteLine("[VehiclePanel] DeletePanelBt_Click deleteBt is NULL!", true);
                }
            }
            else
            {
                return;
            }
        }

        private void SetupVehiclePanelTextLabel(int vehiclePanelIndex, string panelTitle, string NumOfItems)
        {
            // Setup Panel Title label
            this._PanelTitle.AutoSize = true;
            this._PanelTitle.Location = new System.Drawing.Point(76, 38);

            if (_PanelType == PanelType.TypeVehicles)
            {
                this._PanelTitle.Name = "vehiclePanelTitleLb_" + vehiclePanelIndex;
            }
            else
            {
                this._PanelTitle.Name = "modPanelTitleLb_" + vehiclePanelIndex;
            }

            this._PanelTitle.Size = new System.Drawing.Size(51, 20);
            this._PanelTitle.Text = panelTitle;
            this._PanelTitle.ForeColor = System.Drawing.Color.Teal;
            ControlPositioning.CenterVertically(_PanelTitle);

            // Setup Number of Items label
            this._numberOfItems.AutoSize = true;
            this._numberOfItems.Location = new System.Drawing.Point(450, 38);

            if (_PanelType == PanelType.TypeVehicles)
            {
                this._numberOfItems.Name = "vehiclePanelNumOfItemsLb_" + vehiclePanelIndex;
                this._numberOfItems.Text = "Mods: " + NumOfItems;
            }
            else
            {
                this._numberOfItems.Name = "modPanelNumOfItemsLb_" + vehiclePanelIndex;
                this._numberOfItems.Text = "Mod Type: " + NumOfItems;
            }

            ControlPositioning.Center(_numberOfItems);
            this._numberOfItems.ForeColor = System.Drawing.Color.LightSteelBlue;
            this._numberOfItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.0F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))); ;
            this._numberOfItems.Size = new System.Drawing.Size(51, 20);
            PrintLine.WriteLine("[VehiclePanel] SetupVehiclePanelTextLabel vehiclePanelIndex: " + vehiclePanelIndex);
        }

        public void SetupVehiclePanel(Control MainPanel, int vehiclePanelIndex, System.ComponentModel.ComponentResourceManager resources, string panelTitle, string NumOfItems)
        {
            AddVehiclePanelFieldsToMainPanel(MainPanel);
            SetupVehiclePanelButtons(vehiclePanelIndex, resources);
            SetupVehiclePanelTextLabel(vehiclePanelIndex, panelTitle, NumOfItems);
        }
    }
    public enum PanelType
    {
        TypeInvalid = -1,
        TypeVehicles,
        TypeModLibrary
    }
}
