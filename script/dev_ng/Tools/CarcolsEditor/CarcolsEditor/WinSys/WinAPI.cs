﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace CarcolsEditor.WinSys
{
    public static class WinApi
    {
        public const int HWND_BROADCAST = 0xffff;
        public static readonly int WM_KEY_PRESS = 0x0312;

        [StructLayout(LayoutKind.Sequential)]
        public struct CustomRect
        {
            public int Left;        // x position of upper-left corner  
            public int Top;         // y position of upper-left corner  
            public int Right;       // x position of lower-right corner  
            public int Bottom;      // y position of lower-right corner  
        }

        public static int RegisterWindowMessage(string format, params object[] args)
        {
            string message = string.Format(format, args);
            return RegisterWindowMessage(message);
        }

        #region Assembly Imports

        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool MoveWindow(IntPtr hWnd, int x, int y, int nWidth, int nHeight, bool bRepaint);

        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(IntPtr hWnd, int bindId, int keyModifiers, int keyCode);

        [DllImport("user32.dll")]
        public static extern bool GetWindowRect(IntPtr hWnd, ref CustomRect rect);

        // Not used yet
        [DllImport("user32.dll")]
        public static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        [DllImport("user32.dll")]
        public static extern int RegisterWindowMessage(string message);

        [DllImport("user32.dll")]
        public static extern bool PostMessage(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam);

        [DllImport("user32.dll")]
        public static extern bool SendMessage(IntPtr hwnd, uint msg, int wparam, int lparam);

        #endregion
    }
}
