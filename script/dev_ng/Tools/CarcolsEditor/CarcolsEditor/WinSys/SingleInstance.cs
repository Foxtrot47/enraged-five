﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

/* SingleInstance logic based on the very helpful CodeProject tutorial here:
 * http://www.codeproject.com/Articles/32908/C-Single-Instance-App-With-the-Ability-To-Restore
 * */

namespace CarcolsEditor.WinSys
{
    public static class SingleInstance
    {
        public static readonly int WM_SHOWFIRSTINSTANCE = WinApi.RegisterWindowMessage("WM_SHOWFIRSTINSTANCE|{0}", ProgramInfo.AssemblyGuid);
        private static Mutex _mutexLock;

        public static bool Start()
        {
            // Local\\ = Logged in user session
            // Global\\ = All user sessions on the the system
            bool onlyInstance;
            string mutexName = string.Format("Local\\{0}", ProgramInfo.AssemblyGuid);
            _mutexLock = new Mutex(true, mutexName, out onlyInstance);
            return onlyInstance;
        }

        public static void Stop()
        {
            _mutexLock.ReleaseMutex();
        }

        public static void ShowFirstInstance()
        {
            WinApi.PostMessage(
                (IntPtr)WinApi.HWND_BROADCAST,
                WM_SHOWFIRSTINSTANCE,
                IntPtr.Zero,
                IntPtr.Zero);
        }

    }
}
