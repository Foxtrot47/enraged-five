﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using CarcolsEditor.Logs;

namespace CarcolsEditor.Data
{
    [Serializable()]
    [XmlType("Item", Namespace = "VehicleData")]
    public class VehicleData
    {
        [XmlElement(ElementName = "kitName", Order = 1, Namespace = "")]
        public string ModKitName { get; set; }

        [XmlElement(ElementName = "id", Order = 2, Namespace = "")]
        public IntValue Id = new IntValue();

        [XmlElement(ElementName = "kitType", Order = 3, Namespace = "")]
        public string ModKitType { get; set; }

        [XmlArray(ElementName = "visibleMods", Order = 4, Namespace = "")]
        public List<ModData> visibleMods = new List<ModData>();

        [XmlArray(ElementName = "linkMods", Order = 5, Namespace = "")]
        public List<LinkMod> LinkMods { get; set; }

        [XmlArray(ElementName = "statMods", Order = 6, Namespace = "")]
        public List<StatMod> StatMods { get; set; }

        [XmlArray(ElementName = "slotNames", Order = 7, Namespace = "")]
        public List<SlotName> SlotNames { get; set; }

        [XmlArray(ElementName = "liveryNames", Order = 8, Namespace = "")]
        public List<LiveryName> LiveryNames { get; set; }

        [XmlNamespaceDeclarations]
        private XmlSerializerNamespaces xmlns;

        /// <summary>
        /// We can't add parameter due to de-serialization!
        /// During an object's de-serialization, the class responsible for de-serializing an object creates an instance of the serialized class
        /// and then proceeds to populate the serialized fields and properties only after acquiring an instance to populate.
        /// </summary>
        public VehicleData()
        {
            this.xmlns = new XmlSerializerNamespaces();
            xmlns.Add("", "");
        }

        public List<ModData> GetVehicleModTypeList()
        {
            return visibleMods;
        }
    }

    [Serializable()]
    [XmlType("Item", Namespace = "ModData")]
    public class ModData
    {
        [XmlElement("modelName", Order = 1, Namespace = "")]
        public string ModModelName { get; set; }

        [XmlElement("modShopLabel", Order = 2, Namespace = "")]
        public string ModShopLabel { get; set; }

        [XmlArray(ElementName = "linkedModels", Order = 3, Namespace = "")]
        public List<ItemElement> LinkedModels = new List<ItemElement>();

        [XmlArray(ElementName = "turnOffBones", Order = 4, Namespace = "")]
        public List<ItemElement> TurnOffBones = new List<ItemElement>();

        [XmlElement("type", Order = 5, Namespace = "")]
        public string ModType { get; set; }

        [XmlElement("bone", Order = 6, Namespace = "")]
        public string Bone { get; set; }

        [XmlElement("collisionBone", Order = 7, Namespace = "")]
        public string CollisionBone { get; set; }

        [XmlElement("cameraPos", Order = 8, Namespace = "")]
        public string CameraPos { get; set; }

        [XmlElement("audioApply", Order = 9, Namespace = "")]
        // Float
        public StringValue AudioApply = new StringValue();

        [XmlElement("weight", Order = 10, Namespace = "")]
        // Int
        public StringValue Weight = new StringValue();

        [XmlElement("turnOffExtra", Order = 11, Namespace = "")]
        public BoolValue TurnOffExtra = new BoolValue();

        [XmlElement("disableBonnetCamera", Order = 12, Namespace = "")]
        public BoolValue DisableBonnetCamera = new BoolValue();

        [XmlElement("allowBonnetSlide", Order = 13, Namespace = "")]
        public BoolValue AllowBonnetSlide = new BoolValue();

        [XmlNamespaceDeclarations]
        private XmlSerializerNamespaces xmlns;

        public ModData()
        {
            this.xmlns = new XmlSerializerNamespaces();
            xmlns.Add("", "");
        }

        public void InitializeModTypeData(string ModType, string ModModelName, string ModShopLabel, string Bone, string CollisionBone, string CameraPos, string Weight,
            string AudioApply, string TurnOffExtra, string DisableBonnetCamera, string AllowBonnetSlide, List<ItemElement> LinkedModels, List<ItemElement> turnOffBones)
        {
            this.ModType = ModType;
            this.ModModelName = ModModelName;
            this.ModShopLabel = ModShopLabel;
            this.Bone = Bone;
            this.CollisionBone = CollisionBone;
            this.CameraPos = CameraPos;
            this.Weight.Value = Weight;
            this.AudioApply.Value = AudioApply;
            this.TurnOffExtra.Value = TurnOffExtra.Equals("true") ? true : false;
            this.DisableBonnetCamera.Value = DisableBonnetCamera.Equals("true") ? true : false;
            this.AllowBonnetSlide.Value = AllowBonnetSlide.Equals("true") ? true : false;

            if (this.Weight.Value == "")
            {
                this.Weight.Value = "0";
            }
            if (this.AudioApply.Value == "")
            {
                this.AudioApply.Value = "0.0";
            }

            this.LinkedModels = LinkedModels;
            this.TurnOffBones = turnOffBones;
        }
    }

    [Serializable()]
    [XmlType("Item", Namespace = "LinkMod")]
    public class LinkMod
    {
        [XmlElement("modelName", Order = 1, Namespace = "")]
        public string ModelName { get; set; }

        [XmlElement("bone", Order = 2, Namespace = "bone")]
        public string Bone { get; set; }

        [XmlElement("turnOffExtra", Order = 3, Namespace = "")]
        public BoolValue TurnOffExtra = new BoolValue();

        [XmlNamespaceDeclarations]
        private XmlSerializerNamespaces xmlns;

        public LinkMod()
        {
            this.xmlns = new XmlSerializerNamespaces();
            xmlns.Add("", "");
        }
    }

    [Serializable()]
    [XmlType("Item", Namespace = "StatMod")]
    public class StatMod
    {
        [XmlElement("identifier", Order = 1, Namespace = "")]
        public string Identifier { get; set; }

        [XmlElement("modifier", Order = 2, Namespace = "")]
        public StringValue Modifier = new StringValue();

        [XmlElement("audioApply", Order = 3, Namespace = "")]
        public StringValue AudioApply = new StringValue();

        [XmlElement("weight", Order = 4, Namespace = "")]
        public StringValue Weight = new StringValue();

        [XmlElement("type", Order = 5, Namespace = "")]
        public string Type { get; set; }

        [XmlNamespaceDeclarations]
        private XmlSerializerNamespaces Xmlns;

        public StatMod()
        {
            this.Xmlns = new XmlSerializerNamespaces();
            Xmlns.Add("", "");
        }
    }

    [Serializable()]
    [XmlType("Item", Namespace = "SlotName")]
    public class SlotName
    {
        [XmlElement("slot", Order = 1, Namespace = "")]
        public string Slot { get; set; }

        [XmlElement("name", Order = 2, Namespace = "")]
        public string Name { get; set; }

        [XmlNamespaceDeclarations]
        private XmlSerializerNamespaces Xmlns;

        public SlotName()
        {
            this.Xmlns = new XmlSerializerNamespaces();
            Xmlns.Add("", "");
        }
    }

    [Serializable()]
    [XmlType("Item", Namespace = "LiveryName")]
    public class LiveryName
    {
        [XmlNamespaceDeclarations]
        private XmlSerializerNamespaces Xmlns;

        public LiveryName()
        {
            this.Xmlns = new XmlSerializerNamespaces();
            Xmlns.Add("", "");
        }
    }

    [Serializable()]
    [XmlType("Item", Namespace = "ItemElement")]
    public class ItemElement
    {
        [XmlText]
        public string Value { get; set; }

        [XmlNamespaceDeclarations]
        private XmlSerializerNamespaces Xmlns;
        public ItemElement()
        {
            this.Xmlns = new XmlSerializerNamespaces();
            Xmlns.Add("", "");
        }
    }

    [Serializable()]
    [XmlType("Item", Namespace = "Lights")]
    public class Lights
    {
        [XmlElement(ElementName = "id", Order = 1, Namespace = "")]
        public IntValue Id = new IntValue();

        [XmlElement(ElementName = "indicator", Order = 2, Namespace = "")]
        public Light Indicator { get; set; }

        [XmlElement(ElementName = "rearIndicatorCorona", Order = 3, Namespace = "")]
        public Corona RearIndicatorCorona { get; set; }

        [XmlElement(ElementName = "frontIndicatorCorona", Order = 4, Namespace = "")]
        public Corona FrontIndicatorCorona { get; set; }

        [XmlElement(ElementName = "tailLight", Order = 5, Namespace = "")]
        public Light TailLight { get; set; }

        [XmlElement(ElementName = "tailLightCorona", Order = 6, Namespace = "")]
        public Corona TailLightCorona { get; set; }

        [XmlElement(ElementName = "tailLightMiddleCorona", Order = 7, Namespace = "")]
        public Corona TailLightMiddleCorona { get; set; }

        [XmlElement(ElementName = "headLight", Order = 8, Namespace = "")]
        public Light HeadLight { get; set; }

        [XmlElement(ElementName = "headLightCorona", Order = 9, Namespace = "")]
        public Corona HeadLightCorona { get; set; }

        [XmlElement(ElementName = "reversingLight", Order = 10, Namespace = "")]
        public Light ReversingLight { get; set; }

        [XmlElement(ElementName = "reversingLightCorona", Order = 11, Namespace = "")]
        public Corona ReversingLightCorona { get; set; }

        [XmlElement(ElementName = "name", Order = 12, Namespace = "")]
        public string Name { get; set; }

        [XmlNamespaceDeclarations]
        private XmlSerializerNamespaces xmlns;

        public Lights()
        {
            this.xmlns = new XmlSerializerNamespaces();
            xmlns.Add("", "");
        }
    }

    [Serializable()]
    [XmlType("Item", Namespace = "Indicator")]
    public class Light
    {
        [XmlElement("intensity", Order = 1, Namespace = "")]
        public StringValue Intensity = new StringValue();

        [XmlElement("falloffMax", Order = 2, Namespace = "")]
        public StringValue FalloffMax = new StringValue();

        [XmlElement("falloffExponent", Order = 3, Namespace = "")]
        public StringValue FalloffExponent = new StringValue();

        [XmlElement("innerConeAngle", Order = 4, Namespace = "")]
        public StringValue InnerConeAngle = new StringValue();

        [XmlElement("outerConeAngle", Order = 5, Namespace = "")]
        public StringValue OuterConeAngle = new StringValue();

        [XmlElement("emmissiveBoost", Order = 6, Namespace = "")]
        public StringValue EmmissiveBoost = new StringValue();

        [XmlElement("color", Order = 7, Namespace = "")]
        public StringValue Color = new StringValue();


        [XmlNamespaceDeclarations]
        private XmlSerializerNamespaces xmlns;

        public Light()
        {
            this.xmlns = new XmlSerializerNamespaces();
            xmlns.Add("", "");
        }
    }

    [Serializable()]
    [XmlType("Item", Namespace = "rearIndicatorCorona")]
    public class Corona
    {
        [XmlElement("size", Order = 1, Namespace = "")]
        public StringValue Size = new StringValue();

        [XmlElement("size_far", Order = 2, Namespace = "")]
        public StringValue SizeFar = new StringValue();

        [XmlElement("intensity", Order = 3, Namespace = "")]
        public StringValue Intensity = new StringValue();

        [XmlElement("intensity_far", Order = 4, Namespace = "")]
        public StringValue IntensityFar = new StringValue();

        [XmlElement("color", Order = 5, Namespace = "")]
        public StringValue Color = new StringValue();

        [XmlElement("numCoronas", Order = 6, Namespace = "")]
        public StringValue NumCoronas = new StringValue();

        [XmlElement("distBetweenCoronas", Order = 7, Namespace = "")]
        public StringValue DistBetweenCoronas = new StringValue();

        [XmlElement("distBetweenCoronas_far", Order = 8, Namespace = "")]
        public StringValue DistBetweenCoronasFar = new StringValue();

        [XmlElement("xRotation", Order = 9, Namespace = "")]
        public StringValue XRotation = new StringValue();

        [XmlElement("yRotation", Order = 10, Namespace = "")]
        public StringValue YRotation = new StringValue();

        [XmlElement("zRotation", Order = 11, Namespace = "")]
        public StringValue ZRotation = new StringValue();

        [XmlElement("zBias", Order = 12, Namespace = "")]
        public StringValue ZBias = new StringValue();

        [XmlElement("pullCoronaIn", Order = 13, Namespace = "")]
        public StringValue PullCoronaIn = new StringValue();

        [XmlNamespaceDeclarations]
        private XmlSerializerNamespaces xmlns;

        public Corona()
        {
            this.xmlns = new XmlSerializerNamespaces();
            xmlns.Add("", "");
        }
    }

    public class IntValue
    {
        [XmlAttribute("value")]
        public int Value { get; set; }
        public IntValue()
        {
        }
    }

    public class StringValue
    {
        [XmlAttribute("value")]
        public string Value { get; set; }
        public StringValue()
        {
        }
    }

    public class BoolValue
    {
        [XmlAttribute("value")]
        public bool Value { get; set; }
        public BoolValue()
        {
        }
    }
}
