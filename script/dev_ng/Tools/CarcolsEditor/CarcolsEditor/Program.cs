﻿using System;
using CarcolsEditor.WinSys;
using System.Windows.Forms;
using CarcolsEditor.Logs;
using System.ComponentModel;
using System.IO;

namespace CarcolsEditor
{
    static class Program
    {
        private const int MAIN_UPDATE_TICK_MS = 1000;

        public static Timer MainTick;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (!SingleInstance.Start())
            {
                SingleInstance.ShowFirstInstance();
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            MainTick = new Timer();
            MainTick.Interval = MAIN_UPDATE_TICK_MS;
            MainTick.Start();

            PrintLine.Initialise();

            AsyncOperationManager.SynchronizationContext = new WindowsFormsSynchronizationContext();

            try
            {
                Application.Run(new MainPage());
            }
            catch (Exception ex)
            {
                string filename = @"CrashLogs.txt";
                string tempPath = @"C:\\Temp";
                tempPath = Path.Combine(tempPath, filename);
                string stackTrace = ex.StackTrace;
                File.WriteAllText(tempPath, stackTrace);
            }

            SingleInstance.Stop();
        }
    }
}
