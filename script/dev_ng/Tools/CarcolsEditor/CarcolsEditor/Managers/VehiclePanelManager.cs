﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarcolsEditor.Logs;
using CarcolsEditor.VehPanel;
using System.Windows.Forms;
using CarcolsEditor.Data;

namespace CarcolsEditor.Managers
{
    public class VehiclePanelManager
    {
        private List<VehiclePanel> _vehiclePanel;
        private List<VehiclePanel> _modPanel;
        public Control MainVehiclePanel;
        public Control MainModPanel;
        public System.ComponentModel.ComponentResourceManager Resources;

        #region Singleton
        public static VehiclePanelManager Instance => _instance;
        private static VehiclePanelManager _instance;

        public static void Instantiate(Control MainVehiclePanel, Control MainModPanel, System.ComponentModel.ComponentResourceManager Resources)
        {
            if (_instance != null)
            {
                PrintLine.WriteLine("[VehiclePanelManager] Instantiate - Trying to create a VehiclePanelManager when one already exists!");
                return;
            }

            PrintLine.WriteLine("[VehiclePanelManager] Instantiate - Instantiated a new VehiclePanelManager singleton");

            _instance = new VehiclePanelManager(MainVehiclePanel, MainModPanel, Resources);
        }

        #endregion

        public List<VehiclePanel> GetModPanelList()
        {
            return _modPanel;
        }

        public List<VehiclePanel> GetVehiclePanelList()
        {
            return _vehiclePanel;
        }

        public bool IsAnyModChecked()
        {
            int ListCount = _modPanel.Count;

            if (_modPanel != null && ListCount > 0)
            {
                for (int Index = 0; Index < ListCount; Index++)
                {
                    if (_modPanel[Index].SelectCheckB.Checked)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool IsAnyVehicleChecked()
        {
            int ListCount = _vehiclePanel.Count;

            if (_vehiclePanel != null && ListCount > 0)
            {
                for (int Index = 0; Index < ListCount; Index++)
                {
                    if (_vehiclePanel[Index].SelectCheckB.Checked)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public void AddSelectedModsToExistingVehicles()
        {
            if (IsAnyVehicleChecked())
            {
                int ListCount = _vehiclePanel.Count;

                if (_vehiclePanel != null && ListCount > 0)
                {
                    for (int Index = 0; Index < ListCount; Index++)
                    {
                        if (_vehiclePanel[Index].SelectCheckB.Checked)
                        {
                            List<ModData> existingVehicle = new List<ModData>();
                            existingVehicle = CarcolsManager.Instance.GetCarcolsVehicleModData(Index);
                            AddSelectedModsToModData(ref existingVehicle);
                        }
                    }

                }

            }
        }

        public void AddSelectedModsToModData(ref List<ModData> modData)
        {
            int ListCount = _modPanel.Count;

            if (_modPanel != null && ListCount > 0)
            {
                for (int Index = 0; Index < ListCount; Index++)
                {
                    if (_modPanel[Index].SelectCheckB.Checked || VehicleModManager.Instance.EditingVehiclePanel)
                    {
                        int SelectedPanelIndex = _modPanel[Index].GetPanelIndex(_modPanel[Index].SelectCheckB.Name);
                        if (SelectedPanelIndex != -1 && SelectedPanelIndex < VehicleModManager.Instance.GetVehicleModList().Count)
                        {
                            if (modData == null)
                            {
                                modData = new List<ModData>();
                            }

                            modData.Add(VehicleModManager.Instance.GetVehicleModList()[SelectedPanelIndex]);
                            PrintLine.WriteLine("[VehiclePanelManager] AddSelectedModsToModData - Added item to list SelectedPanelIndex: " + SelectedPanelIndex);
                        }
                        else
                        {
                            PrintLine.WriteLine("[VehiclePanelManager] AddSelectedModsToModData - invalid check box index SelectedPanelIndex: " + SelectedPanelIndex);
                        }
                    }
                }
            }
        }

        public VehiclePanelManager(Control MainVehiclePanel, Control MainModPanel, System.ComponentModel.ComponentResourceManager Resources)
        {
            _vehiclePanel = new List<VehiclePanel>();
            _modPanel = new List<VehiclePanel>();
            this.MainVehiclePanel = MainVehiclePanel;
            this.MainModPanel = MainModPanel;
            this.Resources = Resources;
            PrintLine.WriteLine("[VehiclePanelManager] VehiclePanelManager - create VehiclePanelManager");
        }

        public void CreateAndAddVehiclePanelToList(int vehiclePanelIndex, string panelTitle, PanelType type, string NumOfItems)
        {
            VehiclePanel vehiclePanel = new VehiclePanel(vehiclePanelIndex, type);

            if (type == PanelType.TypeVehicles)
            {
                vehiclePanel.SetupVehiclePanel(MainVehiclePanel, vehiclePanelIndex, Resources, panelTitle, NumOfItems);
                _vehiclePanel.Add(vehiclePanel);
                PrintLine.WriteLine("[VehiclePanelManager] CreateAndAddVehiclePanelToList - _vehiclePanel size: " + _vehiclePanel.Count);
            }
            else
            {
                vehiclePanel.SetupVehiclePanel(MainModPanel, vehiclePanelIndex, Resources, panelTitle, NumOfItems);
                _modPanel.Add(vehiclePanel);
                PrintLine.WriteLine("[VehiclePanelManager] CreateAndAddVehiclePanelToList - _modPanel size: " + _modPanel.Count);
            }

            PrintLine.WriteLine("[VehiclePanelManager] CreateAndAddVehiclePanelToList - create vehiclePanelIndex: " + vehiclePanelIndex);
        }

        public void RebuildAllVehiclePanels()
        {
            int ListCount = _vehiclePanel.Count;

            PrintLine.WriteLine("[VehiclePanelManager] RebuildAllVehiclePanels - _vehiclePanel size: " + ListCount);

            for (int PanelIndex = 0; PanelIndex < ListCount; PanelIndex++)
            {
                RemovePanelFromList(PanelIndex, PanelType.TypeVehicles);
            }

            _vehiclePanel.Clear();

            ListCount = CarcolsManager.Instance.Kits.Count;

            PrintLine.WriteLine("[VehiclePanelManager] RebuildAllVehiclePanels - vehicles size: " + ListCount);

            for (int VehicleIndex = 0; VehicleIndex < ListCount; VehicleIndex++)
            {
                CreateAndAddVehiclePanelToList(VehicleIndex, CarcolsManager.Instance.GetCarcolsVehicleName(VehicleIndex), PanelType.TypeVehicles, CarcolsManager.Instance.GetCarcolsVehicleModData(VehicleIndex).Count.ToString());
            }
        }

        public void RebuildAllModPanels()
        {
            int ListCount = _modPanel.Count;

            PrintLine.WriteLine("[VehiclePanelManager] RebuildAllModPanels - _vehiclePanel size: " + ListCount);

            for (int PanelIndex = 0; PanelIndex < ListCount; PanelIndex++)
            {
                RemovePanelFromList(PanelIndex, PanelType.TypeModLibrary);
            }

            _modPanel.Clear();


            ListCount = VehicleModManager.Instance.GetVehicleModList().Count;

            PrintLine.WriteLine("[VehiclePanelManager] RebuildAllModPanels - vehicles size: " + ListCount);

            for (int VehicleIndex = 0; VehicleIndex < ListCount; VehicleIndex++)
            {
                CreateAndAddVehiclePanelToList(VehicleIndex, VehicleModManager.Instance.GetVehicleModList()[VehicleIndex].ModModelName, PanelType.TypeModLibrary,
                    VehicleModManager.Instance.GetVehicleModList()[VehicleIndex].ModType);
            }
        }

        public void RemovePanelFromList(int Index, PanelType Type)
        {
            VehiclePanel vehiclePanel;

            if (Type == PanelType.TypeVehicles)
            {
                vehiclePanel = _vehiclePanel[Index];

                if (MainVehiclePanel.InvokeRequired)
                {
                    MainVehiclePanel.BeginInvoke(new MethodInvoker(() => MainVehiclePanel.Controls.Remove(vehiclePanel._BackgrounPanel)));
                }
                else
                {
                    MainVehiclePanel.Controls.Remove(vehiclePanel._BackgrounPanel);
                }

                PrintLine.WriteLine("[VehiclePanelManager] RemovePanelFromList - _vehiclePanel size: " + _vehiclePanel.Count);
            }
            else
            {
                vehiclePanel = _modPanel[Index];

                if (MainModPanel.InvokeRequired)
                {
                    MainModPanel.BeginInvoke(new MethodInvoker(() => MainModPanel.Controls.Remove(vehiclePanel._BackgrounPanel)));
                }
                else
                {
                    MainModPanel.Controls.Remove(vehiclePanel._BackgrounPanel);
                }
                PrintLine.WriteLine("[VehiclePanelManager] RemovePanelFromList - _vehiclePanel size: " + _modPanel.Count);
            }

            PrintLine.WriteLine("[VehiclePanelManager] RemovePanelFromList - Index: " + Index);
        }

        public void Update()
        {
        }
    }
}
