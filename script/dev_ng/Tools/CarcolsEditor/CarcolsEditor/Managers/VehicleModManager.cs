﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarcolsEditor.Logs;
using CarcolsEditor;
using System.Windows.Forms;
using CarcolsEditor.Data;

namespace CarcolsEditor.Managers
{
    public class VehicleModManager
    {

        public bool EditingMode;
        public bool EditingVehiclePanel;
        public bool EditingModPanel;
        public bool AddToExistingVehicle;
        public bool AddNewMod;
        public bool UpdatePreviewPanel;
        public int EditingVehicleIndex;
        public int EditingModIndex;

        private VehicleData _vehicleData;
        private VehicleData _EditVehicleData;
        private List<ModData> _VehicleModData;
        private List<ModData> _EditVehicleModData;
        private ModData _ModData;
        private ModData _EditModData;

        #region Singleton
        public static VehicleModManager Instance => _instance;
        private static VehicleModManager _instance;

        public static void Instantiate()
        {
            if (_instance != null)
            {
                PrintLine.WriteLine("[VehicleModManager] Instantiate - Trying to create a VehicleModManager when one already exists!");
                return;
            }

            PrintLine.WriteLine("[VehicleModManager] Instantiate - Instantiated a new VehicleModManager singleton");

            _instance = new VehicleModManager();
        }

        #endregion

        private VehicleModManager()
        {
            _VehicleModData = new List<ModData>();
            _EditVehicleModData = new List<ModData>();
            EditingMode = false;
        }

        public void SetVehicleManagerVehicleList(List<ModData> List)
        {
            if (EditingVehiclePanel)
            {
                _EditVehicleModData = List;
            }
            else
            {
                _VehicleModData = List;
            }
        }

        public List<ModData> GetVehicleModList()
        {
            if (EditingVehiclePanel)
            {
                return _EditVehicleModData;
            }

            return _VehicleModData;
        }

        public void CreateVehicleData()
        {
            if (EditingVehiclePanel)
            {

                _EditVehicleData = new VehicleData();
                _EditVehicleData.visibleMods = CarcolsManager.Instance.Kits[EditingVehicleIndex].visibleMods;
                _EditVehicleData.LinkMods = CarcolsManager.Instance.Kits[EditingVehicleIndex].LinkMods;
                _EditVehicleData.StatMods = CarcolsManager.Instance.Kits[EditingVehicleIndex].StatMods;
                _EditVehicleData.SlotNames = CarcolsManager.Instance.Kits[EditingVehicleIndex].SlotNames;
                _EditVehicleData.LiveryNames = CarcolsManager.Instance.Kits[EditingVehicleIndex].LiveryNames;
            }
            else
            {
                _vehicleData = new VehicleData();
            }
        }

        public void CreateVehicleModData()
        {
            if (EditingVehiclePanel)
            {
                _EditModData = new ModData();
            }
            else
            {
                _ModData = new ModData();
            }
        }

        public void Update()
        {
        }

        public void CreateModData(string ModType, string ModModelName, string ModShopLabel, string Bone, string CollisionBone, string CameraPos, string Weight,
            string AudioApply, string TurnOffExtra, string DisableBonnetCamera, string AllowBonnetSlide, List<ItemElement> LinkedModels, List<ItemElement> turnOffBones)
        {
            if (EditingVehiclePanel)
            {
                if (_EditModData == null)
                {
                    _EditModData = new ModData();
                    PrintLine.WriteLine("[VehicleModManager] CreateModData (EditingVehiclePanel) - _ModData == null create new one");
                }

                _EditModData.InitializeModTypeData(ModType, ModModelName, ModShopLabel, Bone, CollisionBone, CameraPos, Weight, AudioApply,
                    TurnOffExtra, DisableBonnetCamera, AllowBonnetSlide, LinkedModels, turnOffBones);
            }
            else
            {
                if (_ModData == null)
                {
                    _ModData = new ModData();
                    PrintLine.WriteLine("[VehicleModManager] CreateModData - _ModData == null create new one");
                }

                _ModData.InitializeModTypeData(ModType, ModModelName, ModShopLabel, Bone, CollisionBone, CameraPos, Weight, AudioApply,
                    TurnOffExtra, DisableBonnetCamera, AllowBonnetSlide, LinkedModels, turnOffBones);
            }
        }

        public ModData GetModData()
        {
            if (EditingVehiclePanel)
            {
                return _EditModData;
            }

            return _ModData;
        }

        public void SetupEditModData(ModData modData)
        {
            CleanupModData();
            CreateVehicleModData();

            if (EditingVehiclePanel)
            {
                _EditModData.ModType = modData.ModType;
                _EditModData.ModModelName = modData.ModModelName;
                _EditModData.ModShopLabel = modData.ModShopLabel;
                _EditModData.Bone = modData.Bone;
                _EditModData.CollisionBone = modData.CollisionBone;
                _EditModData.CameraPos = modData.CameraPos;
                _EditModData.AudioApply = modData.AudioApply;
                _EditModData.Weight = modData.Weight;
                _EditModData.TurnOffExtra = modData.TurnOffExtra;
                _EditModData.DisableBonnetCamera = modData.DisableBonnetCamera;
                _EditModData.AllowBonnetSlide = modData.AllowBonnetSlide;
            }
            else
            {
                _ModData.ModType = modData.ModType;
                _ModData.ModModelName = modData.ModModelName;
                _ModData.ModShopLabel = modData.ModShopLabel;
                _ModData.Bone = modData.Bone;
                _ModData.CollisionBone = modData.CollisionBone;
                _ModData.CameraPos = modData.CameraPos;
                _ModData.AudioApply = modData.AudioApply;
                _ModData.Weight = modData.Weight;
                _ModData.TurnOffExtra = modData.TurnOffExtra;
                _ModData.DisableBonnetCamera = modData.DisableBonnetCamera;
                _ModData.AllowBonnetSlide = modData.AllowBonnetSlide;
            }
        }

        public void CopySelectedModsToLibrary()
        {
            if (_EditVehicleData.visibleMods.Count > 0)
            {
                int ListCount = VehiclePanelManager.Instance.GetModPanelList().Count;
                if (ListCount > 0)
                {
                    for (int Index = 0; Index < ListCount; Index++)
                    {
                        if (VehiclePanelManager.Instance.GetModPanelList()[Index].SelectCheckB.Checked)
                        {
                            if (_ModData == null)
                            {
                                _ModData = new ModData();
                            }
                            else
                            {
                                _ModData = _EditVehicleData.visibleMods[Index];
                                _VehicleModData.Add(_ModData);
                                PrintLine.WriteLine("[VehicleModManager] CopySelectedModsToLibrary");
                            }
                        }
                    }
                }
            }
        }

        public void AddNewModToManagerModList()
        {
            if (!EditingVehiclePanel)
            {
                if (_VehicleModData == null)
                {
                    _VehicleModData = new List<ModData>();
                    PrintLine.WriteLine("[VehicleModManager] AddNewModToManagerModList - _VehicleModData == null create new one");
                }

                PrintLine.WriteLine("[VehicleModManager] AddNewModToManagerModList - added new Mod : " + _ModData.ModType);

                _VehicleModData.Add(_ModData);

                PrintLine.WriteLine("[VehicleModManager] AddNewModToManagerModList - added new Mod Data to list, count: " + _VehicleModData.Count);
            }
            else
            {
                if (_EditVehicleModData == null)
                {
                    _EditVehicleModData = new List<ModData>();
                    PrintLine.WriteLine("[VehicleModManager] AddNewModToManagerModList (EditingVehiclePanel) - _EditingVehicleModData == null create new one");
                }

                PrintLine.WriteLine("[VehicleModManager] AddNewModToManagerModList (EditingVehiclePanel) - added new Mod : " + _EditModData.ModType);

                _EditVehicleModData.Add(_EditModData);

                PrintLine.WriteLine("[VehicleModManager] AddNewModToManagerModList (EditingVehiclePanel) - added new Mod Data to list, count: " + _EditVehicleModData.Count);
            }
        }

        public void CleanupModData()
        {
            if (!EditingVehiclePanel)
            {
                _ModData = null;
            }
            else
            {
                _EditModData = null;
            }
        }

        public void EditModInManagerModList(int Index)
        {
            if (EditingVehiclePanel)
            {
                _EditVehicleModData[Index] = _EditModData;
            }
            else
            {
                _VehicleModData[Index] = _ModData;
            }
        }

        public void RemoveModInManagerModList(int Index)
        {
            if (EditingVehiclePanel)
            {
                _EditVehicleModData.RemoveAt(Index);
            }
            else
            {
                _VehicleModData.RemoveAt(Index);
            }
        }
    }
}
