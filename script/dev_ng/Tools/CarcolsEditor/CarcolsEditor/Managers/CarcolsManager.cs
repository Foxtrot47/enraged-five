﻿using CarcolsEditor.Data;
using CarcolsEditor.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CarcolsEditor.Managers
{
    [Serializable()]
    [XmlType("CVehicleModelInfoVarGlobal")]
    public class CarcolsManager
    {
        [XmlArray(ElementName = "Kits")]
        public List<VehicleData> Kits { get; set; }

        [XmlArray(ElementName = "Lights")]
        public List<Lights> Lights { get; set; }

        #region Singleton
        public static CarcolsManager Instance => _instance;
        private static CarcolsManager _instance;

        public static void Instantiate()
        {
            if (_instance != null)
            {
                PrintLine.WriteLine("[CarcolsManager] Instantiate - Trying to create a CarcolsManager when one already exists!");
                return;
            }

            PrintLine.WriteLine("[CarcolsManager] Instantiate - Instantiated a new CarcolsManager singleton");

            _instance = new CarcolsManager();
        }
        #endregion

        public void SetCarcolsInstance(CarcolsManager Instance)
        {
            _instance = Instance;
        }

        public CarcolsManager()
        {
            this.Kits = new List<VehicleData>();
        }
        public void AddVehiclesToCarcols(VehicleData newVehicles)
        {
            this.Kits.Add(newVehicles);
        }

        public void EditVehicleInCarcols(int Index, VehicleData newVehicles)
        {
            this.Kits[Index] = newVehicles;
        }

        public void RemoveVehicleFromCarcols(int Index)
        {
            this.Kits.RemoveAt(Index);
        }

        public bool DoesThisVehicleNameExistInCarcols(string NameToCheck)
        {
            for (int Index = 0; Index < Kits.Count; Index++)
            {
                if (GetCarcolsVehicleName(Index) == NameToCheck)
                {
                    return true;
                }
            }

            return false;
        }

        public string GetCarcolsVehicleName(int Index)
        {
            return this.Kits[Index].ModKitName;
        }

        public string GetCarcolsVehicleModKitType(int Index)
        {
            return this.Kits[Index].ModKitType;
        }

        public List<ModData> GetCarcolsVehicleModData(int Index)
        {
            return this.Kits[Index].GetVehicleModTypeList();
        }
    }
}
