﻿namespace EmailStar
{
    partial class EmailStar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toemail = new System.Windows.Forms.ComboBox();
            this.fromemail = new System.Windows.Forms.ComboBox();
            this.emailtitle = new System.Windows.Forms.TextBox();
            this.emailcontent = new System.Windows.Forms.TextBox();
            this.emaildelay = new System.Windows.Forms.TextBox();
            this.emailblocks = new System.Windows.Forms.CheckBox();
            this.removeresponse = new System.Windows.Forms.Button();
            this.addresponse = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.emailresponses = new System.Windows.Forms.ListBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.autofirecheck = new System.Windows.Forms.CheckBox();
            this.addemail = new System.Windows.Forms.ComboBox();
            this.responseenumname = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.responsestring = new System.Windows.Forms.TextBox();
            this.responsefirestthread = new System.Windows.Forms.CheckBox();
            this.threadtofire = new System.Windows.Forms.ComboBox();
            this.responseendsthread = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.scriptemailenumoverride = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.selectedemailgroup = new System.Windows.Forms.GroupBox();
            this.weblink = new System.Windows.Forms.TextBox();
            this.haslink = new System.Windows.Forms.CheckBox();
            this.textureDictname = new System.Windows.Forms.TextBox();
            this.hasTextureDict = new System.Windows.Forms.CheckBox();
            this.ExportEmailEnum = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.quickAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadDatasetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportHeadersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toXgta5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mergeDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pickparticipant = new System.Windows.Forms.ComboBox();
            this.removeparticipant = new System.Windows.Forms.Button();
            this.addparticipant = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.participantlist = new System.Windows.Forms.ListBox();
            this.removeemailfromthread = new System.Windows.Forms.Button();
            this.addemailtothread = new System.Windows.Forms.Button();
            this.removethread = new System.Windows.Forms.Button();
            this.addthread = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.emailsinthread = new System.Windows.Forms.ListBox();
            this.afadf = new System.Windows.Forms.Label();
            this.threadlist = new System.Windows.Forms.ListBox();
            this.wtfIsThisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.threadname = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enumificationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.concatenateMatchingStringsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purgeUnnusedStringsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDLG = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDLG = new System.Windows.Forms.SaveFileDialog();
            this.exportFolderDlg = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox1.SuspendLayout();
            this.selectedemailgroup.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toemail
            // 
            this.toemail.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toemail.FormattingEnabled = true;
            this.toemail.Location = new System.Drawing.Point(61, 70);
            this.toemail.Name = "toemail";
            this.toemail.Size = new System.Drawing.Size(252, 21);
            this.toemail.TabIndex = 20;
            this.toemail.SelectedIndexChanged += new System.EventHandler(this.toemail_SelectedIndexChanged);
            // 
            // fromemail
            // 
            this.fromemail.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fromemail.FormattingEnabled = true;
            this.fromemail.Location = new System.Drawing.Point(61, 41);
            this.fromemail.Name = "fromemail";
            this.fromemail.Size = new System.Drawing.Size(252, 21);
            this.fromemail.TabIndex = 19;
            this.fromemail.SelectedIndexChanged += new System.EventHandler(this.fromemail_SelectedIndexChanged);
            // 
            // emailtitle
            // 
            this.emailtitle.Location = new System.Drawing.Point(61, 130);
            this.emailtitle.Name = "emailtitle";
            this.emailtitle.Size = new System.Drawing.Size(252, 20);
            this.emailtitle.TabIndex = 16;
            this.emailtitle.TextChanged += new System.EventHandler(this.emailtitle_TextChanged);
            // 
            // emailcontent
            // 
            this.emailcontent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.emailcontent.Location = new System.Drawing.Point(61, 154);
            this.emailcontent.Multiline = true;
            this.emailcontent.Name = "emailcontent";
            this.emailcontent.Size = new System.Drawing.Size(252, 145);
            this.emailcontent.TabIndex = 15;
            this.emailcontent.TextChanged += new System.EventHandler(this.emailcontent_TextChanged);
            // 
            // emaildelay
            // 
            this.emaildelay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.emaildelay.Location = new System.Drawing.Point(325, 318);
            this.emaildelay.Name = "emaildelay";
            this.emaildelay.Size = new System.Drawing.Size(56, 20);
            this.emaildelay.TabIndex = 14;
            this.emaildelay.TextChanged += new System.EventHandler(this.emaildelay_TextChanged);
            // 
            // emailblocks
            // 
            this.emailblocks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.emailblocks.AutoSize = true;
            this.emailblocks.Location = new System.Drawing.Point(325, 282);
            this.emailblocks.Name = "emailblocks";
            this.emailblocks.Size = new System.Drawing.Size(95, 17);
            this.emailblocks.TabIndex = 13;
            this.emailblocks.Text = "Blocks Thread";
            this.emailblocks.UseVisualStyleBackColor = true;
            this.emailblocks.CheckedChanged += new System.EventHandler(this.emailblocks_CheckedChanged);
            // 
            // removeresponse
            // 
            this.removeresponse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.removeresponse.Location = new System.Drawing.Point(125, 480);
            this.removeresponse.Name = "removeresponse";
            this.removeresponse.Size = new System.Drawing.Size(20, 23);
            this.removeresponse.TabIndex = 12;
            this.removeresponse.Text = "-";
            this.removeresponse.UseVisualStyleBackColor = true;
            this.removeresponse.Click += new System.EventHandler(this.removeresponse_Click);
            // 
            // addresponse
            // 
            this.addresponse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.addresponse.Location = new System.Drawing.Point(6, 480);
            this.addresponse.Name = "addresponse";
            this.addresponse.Size = new System.Drawing.Size(64, 23);
            this.addresponse.TabIndex = 11;
            this.addresponse.Text = "+";
            this.addresponse.UseVisualStyleBackColor = true;
            this.addresponse.Click += new System.EventHandler(this.addresponse_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 79);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "Email to add";
            // 
            // emailresponses
            // 
            this.emailresponses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.emailresponses.FormattingEnabled = true;
            this.emailresponses.Location = new System.Drawing.Point(6, 357);
            this.emailresponses.Name = "emailresponses";
            this.emailresponses.Size = new System.Drawing.Size(138, 121);
            this.emailresponses.TabIndex = 10;
            this.emailresponses.SelectedIndexChanged += new System.EventHandler(this.emailresponses_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 341);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Responses";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.autofirecheck);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.addemail);
            this.groupBox1.Controls.Add(this.responseenumname);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.responsestring);
            this.groupBox1.Controls.Add(this.responsefirestthread);
            this.groupBox1.Controls.Add(this.threadtofire);
            this.groupBox1.Controls.Add(this.responseendsthread);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Location = new System.Drawing.Point(151, 341);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(280, 162);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Response Settings";
            // 
            // autofirecheck
            // 
            this.autofirecheck.AutoSize = true;
            this.autofirecheck.Location = new System.Drawing.Point(201, 114);
            this.autofirecheck.Name = "autofirecheck";
            this.autofirecheck.Size = new System.Drawing.Size(73, 17);
            this.autofirecheck.TabIndex = 10;
            this.autofirecheck.Text = "Auto Fires";
            this.autofirecheck.UseVisualStyleBackColor = true;
            this.autofirecheck.CheckedChanged += new System.EventHandler(this.autofirecheck_CheckedChanged);
            // 
            // addemail
            // 
            this.addemail.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addemail.FormattingEnabled = true;
            this.addemail.Location = new System.Drawing.Point(77, 76);
            this.addemail.Name = "addemail";
            this.addemail.Size = new System.Drawing.Size(197, 21);
            this.addemail.TabIndex = 8;
            this.addemail.SelectedIndexChanged += new System.EventHandler(this.addemail_SelectedIndexChanged);
            // 
            // responseenumname
            // 
            this.responseenumname.Location = new System.Drawing.Point(100, 45);
            this.responseenumname.Name = "responseenumname";
            this.responseenumname.Size = new System.Drawing.Size(174, 20);
            this.responseenumname.TabIndex = 7;
            this.responseenumname.TextChanged += new System.EventHandler(this.responseenumname_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 48);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "Script Enum Name";
            // 
            // responsestring
            // 
            this.responsestring.Location = new System.Drawing.Point(93, 16);
            this.responsestring.Name = "responsestring";
            this.responsestring.Size = new System.Drawing.Size(181, 20);
            this.responsestring.TabIndex = 5;
            this.responsestring.TextChanged += new System.EventHandler(this.responsestring_TextChanged);
            // 
            // responsefirestthread
            // 
            this.responsefirestthread.AutoSize = true;
            this.responsefirestthread.Location = new System.Drawing.Point(9, 137);
            this.responsefirestthread.Name = "responsefirestthread";
            this.responsefirestthread.Size = new System.Drawing.Size(85, 17);
            this.responsefirestthread.TabIndex = 4;
            this.responsefirestthread.Text = "Fires Thread";
            this.responsefirestthread.UseVisualStyleBackColor = true;
            this.responsefirestthread.CheckedChanged += new System.EventHandler(this.responsefirestthread_CheckedChanged);
            // 
            // threadtofire
            // 
            this.threadtofire.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.threadtofire.FormattingEnabled = true;
            this.threadtofire.Location = new System.Drawing.Point(100, 135);
            this.threadtofire.Name = "threadtofire";
            this.threadtofire.Size = new System.Drawing.Size(174, 21);
            this.threadtofire.TabIndex = 3;
            this.threadtofire.SelectedIndexChanged += new System.EventHandler(this.threadtofire_SelectedIndexChanged);
            // 
            // responseendsthread
            // 
            this.responseendsthread.AutoSize = true;
            this.responseendsthread.Location = new System.Drawing.Point(9, 114);
            this.responseendsthread.Name = "responseendsthread";
            this.responseendsthread.Size = new System.Drawing.Size(110, 17);
            this.responseendsthread.TabIndex = 2;
            this.responseendsthread.Text = "Ends This Thread";
            this.responseendsthread.UseVisualStyleBackColor = true;
            this.responseendsthread.CheckedChanged += new System.EventHandler(this.responseendsthread_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Selection String";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "To";
            // 
            // scriptemailenumoverride
            // 
            this.scriptemailenumoverride.Location = new System.Drawing.Point(108, 16);
            this.scriptemailenumoverride.Name = "scriptemailenumoverride";
            this.scriptemailenumoverride.Size = new System.Drawing.Size(188, 20);
            this.scriptemailenumoverride.TabIndex = 7;
            this.scriptemailenumoverride.TextChanged += new System.EventHandler(this.scriptemailenumoverride_TextChanged);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(322, 302);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Delays thread:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Title";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Content";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Script Enum Name";
            // 
            // selectedemailgroup
            // 
            this.selectedemailgroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.selectedemailgroup.Controls.Add(this.weblink);
            this.selectedemailgroup.Controls.Add(this.haslink);
            this.selectedemailgroup.Controls.Add(this.textureDictname);
            this.selectedemailgroup.Controls.Add(this.hasTextureDict);
            this.selectedemailgroup.Controls.Add(this.ExportEmailEnum);
            this.selectedemailgroup.Controls.Add(this.label9);
            this.selectedemailgroup.Controls.Add(this.toemail);
            this.selectedemailgroup.Controls.Add(this.fromemail);
            this.selectedemailgroup.Controls.Add(this.emailtitle);
            this.selectedemailgroup.Controls.Add(this.emailcontent);
            this.selectedemailgroup.Controls.Add(this.emaildelay);
            this.selectedemailgroup.Controls.Add(this.emailblocks);
            this.selectedemailgroup.Controls.Add(this.removeresponse);
            this.selectedemailgroup.Controls.Add(this.addresponse);
            this.selectedemailgroup.Controls.Add(this.emailresponses);
            this.selectedemailgroup.Controls.Add(this.label10);
            this.selectedemailgroup.Controls.Add(this.groupBox1);
            this.selectedemailgroup.Controls.Add(this.scriptemailenumoverride);
            this.selectedemailgroup.Controls.Add(this.label8);
            this.selectedemailgroup.Controls.Add(this.label7);
            this.selectedemailgroup.Controls.Add(this.label6);
            this.selectedemailgroup.Controls.Add(this.label5);
            this.selectedemailgroup.Controls.Add(this.label4);
            this.selectedemailgroup.Controls.Add(this.label3);
            this.selectedemailgroup.Location = new System.Drawing.Point(455, 42);
            this.selectedemailgroup.Name = "selectedemailgroup";
            this.selectedemailgroup.Size = new System.Drawing.Size(437, 509);
            this.selectedemailgroup.TabIndex = 34;
            this.selectedemailgroup.TabStop = false;
            this.selectedemailgroup.Text = "Selected Email";
            // 
            // weblink
            // 
            this.weblink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.weblink.Location = new System.Drawing.Point(80, 315);
            this.weblink.Name = "weblink";
            this.weblink.Size = new System.Drawing.Size(233, 20);
            this.weblink.TabIndex = 26;
            this.weblink.TextChanged += new System.EventHandler(this.weblink_TextChanged);
            // 
            // haslink
            // 
            this.haslink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.haslink.AutoSize = true;
            this.haslink.Location = new System.Drawing.Point(10, 317);
            this.haslink.Name = "haslink";
            this.haslink.Size = new System.Drawing.Size(64, 17);
            this.haslink.TabIndex = 25;
            this.haslink.Text = "Has link";
            this.haslink.UseVisualStyleBackColor = true;
            this.haslink.CheckedChanged += new System.EventHandler(this.haslink_CheckedChanged);
            // 
            // textureDictname
            // 
            this.textureDictname.Location = new System.Drawing.Point(151, 104);
            this.textureDictname.Name = "textureDictname";
            this.textureDictname.Size = new System.Drawing.Size(263, 20);
            this.textureDictname.TabIndex = 24;
            this.textureDictname.TextChanged += new System.EventHandler(this.textureDictname_TextChanged);
            // 
            // hasTextureDict
            // 
            this.hasTextureDict.AutoSize = true;
            this.hasTextureDict.Location = new System.Drawing.Point(8, 104);
            this.hasTextureDict.Name = "hasTextureDict";
            this.hasTextureDict.Size = new System.Drawing.Size(137, 17);
            this.hasTextureDict.TabIndex = 23;
            this.hasTextureDict.Text = "Has Texture Dictionary:";
            this.hasTextureDict.UseVisualStyleBackColor = true;
            this.hasTextureDict.CheckedChanged += new System.EventHandler(this.hasTextureDict_CheckedChanged);
            // 
            // ExportEmailEnum
            // 
            this.ExportEmailEnum.AutoSize = true;
            this.ExportEmailEnum.Location = new System.Drawing.Point(303, 16);
            this.ExportEmailEnum.Name = "ExportEmailEnum";
            this.ExportEmailEnum.Size = new System.Drawing.Size(103, 17);
            this.ExportEmailEnum.TabIndex = 22;
            this.ExportEmailEnum.Text = "Export My Enum";
            this.ExportEmailEnum.UseVisualStyleBackColor = true;
            this.ExportEmailEnum.CheckedChanged += new System.EventHandler(this.ExportEmailEnum_CheckedChanged);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(382, 321);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Seconds";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "From";
            // 
            // quickAddToolStripMenuItem
            // 
            this.quickAddToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.emailerToolStripMenuItem});
            this.quickAddToolStripMenuItem.Name = "quickAddToolStripMenuItem";
            this.quickAddToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.quickAddToolStripMenuItem.Text = "Add";
            // 
            // emailerToolStripMenuItem
            // 
            this.emailerToolStripMenuItem.Name = "emailerToolStripMenuItem";
            this.emailerToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.emailerToolStripMenuItem.Text = "Emailer";
            this.emailerToolStripMenuItem.Click += new System.EventHandler(this.emailerToolStripMenuItem_Click);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadDatasetToolStripMenuItem,
            this.saveDataToolStripMenuItem,
            this.exportHeadersToolStripMenuItem,
            this.mergeDataToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadDatasetToolStripMenuItem
            // 
            this.loadDatasetToolStripMenuItem.Name = "loadDatasetToolStripMenuItem";
            this.loadDatasetToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.loadDatasetToolStripMenuItem.Text = "Load Data";
            this.loadDatasetToolStripMenuItem.Click += new System.EventHandler(this.loadDatasetToolStripMenuItem_Click);
            // 
            // saveDataToolStripMenuItem
            // 
            this.saveDataToolStripMenuItem.Name = "saveDataToolStripMenuItem";
            this.saveDataToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.saveDataToolStripMenuItem.Text = "Save Data";
            this.saveDataToolStripMenuItem.Click += new System.EventHandler(this.saveDataToolStripMenuItem_Click);
            // 
            // exportHeadersToolStripMenuItem
            // 
            this.exportHeadersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toFolderToolStripMenuItem,
            this.toXgta5ToolStripMenuItem});
            this.exportHeadersToolStripMenuItem.Name = "exportHeadersToolStripMenuItem";
            this.exportHeadersToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.exportHeadersToolStripMenuItem.Text = "Export headers";
            this.exportHeadersToolStripMenuItem.Click += new System.EventHandler(this.exportHeadersToolStripMenuItem_Click);
            // 
            // toFolderToolStripMenuItem
            // 
            this.toFolderToolStripMenuItem.Name = "toFolderToolStripMenuItem";
            this.toFolderToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.toFolderToolStripMenuItem.Text = "To folder";
            this.toFolderToolStripMenuItem.Click += new System.EventHandler(this.toFolderToolStripMenuItem_Click);
            // 
            // toXgta5ToolStripMenuItem
            // 
            this.toXgta5ToolStripMenuItem.Name = "toXgta5ToolStripMenuItem";
            this.toXgta5ToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.toXgta5ToolStripMenuItem.Text = "To X:\\gta5";
            this.toXgta5ToolStripMenuItem.Click += new System.EventHandler(this.toXgta5ToolStripMenuItem_Click);
            // 
            // mergeDataToolStripMenuItem
            // 
            this.mergeDataToolStripMenuItem.Name = "mergeDataToolStripMenuItem";
            this.mergeDataToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.mergeDataToolStripMenuItem.Text = "Merge Data";
            this.mergeDataToolStripMenuItem.Click += new System.EventHandler(this.mergeDataToolStripMenuItem_Click);
            // 
            // pickparticipant
            // 
            this.pickparticipant.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pickparticipant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pickparticipant.FormattingEnabled = true;
            this.pickparticipant.Location = new System.Drawing.Point(231, 58);
            this.pickparticipant.Name = "pickparticipant";
            this.pickparticipant.Size = new System.Drawing.Size(218, 21);
            this.pickparticipant.TabIndex = 33;
            // 
            // removeparticipant
            // 
            this.removeparticipant.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.removeparticipant.Location = new System.Drawing.Point(432, 173);
            this.removeparticipant.Name = "removeparticipant";
            this.removeparticipant.Size = new System.Drawing.Size(17, 23);
            this.removeparticipant.TabIndex = 32;
            this.removeparticipant.Text = "-";
            this.removeparticipant.UseVisualStyleBackColor = true;
            this.removeparticipant.Click += new System.EventHandler(this.removeparticipant_Click);
            // 
            // addparticipant
            // 
            this.addparticipant.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.addparticipant.Location = new System.Drawing.Point(350, 173);
            this.addparticipant.Name = "addparticipant";
            this.addparticipant.Size = new System.Drawing.Size(49, 23);
            this.addparticipant.TabIndex = 31;
            this.addparticipant.Text = "+";
            this.addparticipant.UseVisualStyleBackColor = true;
            this.addparticipant.Click += new System.EventHandler(this.addparticipant_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(237, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "Thread Participants";
            // 
            // participantlist
            // 
            this.participantlist.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.participantlist.FormattingEnabled = true;
            this.participantlist.Location = new System.Drawing.Point(231, 85);
            this.participantlist.Name = "participantlist";
            this.participantlist.Size = new System.Drawing.Size(218, 82);
            this.participantlist.TabIndex = 29;
            // 
            // removeemailfromthread
            // 
            this.removeemailfromthread.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.removeemailfromthread.Location = new System.Drawing.Point(432, 528);
            this.removeemailfromthread.Name = "removeemailfromthread";
            this.removeemailfromthread.Size = new System.Drawing.Size(17, 23);
            this.removeemailfromthread.TabIndex = 28;
            this.removeemailfromthread.Text = "-";
            this.removeemailfromthread.UseVisualStyleBackColor = true;
            this.removeemailfromthread.Click += new System.EventHandler(this.removeemailfromthread_Click);
            // 
            // addemailtothread
            // 
            this.addemailtothread.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.addemailtothread.Location = new System.Drawing.Point(350, 528);
            this.addemailtothread.Name = "addemailtothread";
            this.addemailtothread.Size = new System.Drawing.Size(49, 23);
            this.addemailtothread.TabIndex = 27;
            this.addemailtothread.Text = "+";
            this.addemailtothread.UseVisualStyleBackColor = true;
            this.addemailtothread.Click += new System.EventHandler(this.addemailtothread_Click);
            // 
            // removethread
            // 
            this.removethread.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.removethread.Location = new System.Drawing.Point(207, 528);
            this.removethread.Name = "removethread";
            this.removethread.Size = new System.Drawing.Size(18, 23);
            this.removethread.TabIndex = 26;
            this.removethread.Text = "-";
            this.removethread.UseVisualStyleBackColor = true;
            this.removethread.Click += new System.EventHandler(this.removethread_Click);
            // 
            // addthread
            // 
            this.addthread.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.addthread.Location = new System.Drawing.Point(126, 528);
            this.addthread.Name = "addthread";
            this.addthread.Size = new System.Drawing.Size(49, 23);
            this.addthread.TabIndex = 25;
            this.addthread.Text = "+";
            this.addthread.UseVisualStyleBackColor = true;
            this.addthread.Click += new System.EventHandler(this.addthread_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(231, 186);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Emails in thread";
            // 
            // emailsinthread
            // 
            this.emailsinthread.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.emailsinthread.FormattingEnabled = true;
            this.emailsinthread.Location = new System.Drawing.Point(231, 203);
            this.emailsinthread.Name = "emailsinthread";
            this.emailsinthread.Size = new System.Drawing.Size(218, 316);
            this.emailsinthread.TabIndex = 23;
            this.emailsinthread.SelectedIndexChanged += new System.EventHandler(this.emailsinthread_SelectedIndexChanged);
            // 
            // afadf
            // 
            this.afadf.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.afadf.AutoSize = true;
            this.afadf.Location = new System.Drawing.Point(36, 42);
            this.afadf.Name = "afadf";
            this.afadf.Size = new System.Drawing.Size(46, 13);
            this.afadf.TabIndex = 22;
            this.afadf.Text = "Threads";
            // 
            // threadlist
            // 
            this.threadlist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.threadlist.FormattingEnabled = true;
            this.threadlist.Location = new System.Drawing.Point(12, 81);
            this.threadlist.Name = "threadlist";
            this.threadlist.Size = new System.Drawing.Size(213, 420);
            this.threadlist.TabIndex = 21;
            this.threadlist.SelectedIndexChanged += new System.EventHandler(this.threadlist_SelectedIndexChanged);
            // 
            // wtfIsThisToolStripMenuItem
            // 
            this.wtfIsThisToolStripMenuItem.Name = "wtfIsThisToolStripMenuItem";
            this.wtfIsThisToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.wtfIsThisToolStripMenuItem.Text = "Wtf is this?";
            this.wtfIsThisToolStripMenuItem.Click += new System.EventHandler(this.wtfIsThisToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wtfIsThisToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // threadname
            // 
            this.threadname.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.threadname.Location = new System.Drawing.Point(12, 58);
            this.threadname.Name = "threadname";
            this.threadname.Size = new System.Drawing.Size(213, 20);
            this.threadname.TabIndex = 20;
            this.threadname.TextChanged += new System.EventHandler(this.threadname_TextChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.quickAddToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(904, 24);
            this.menuStrip1.TabIndex = 19;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enumificationToolStripMenuItem,
            this.concatenateMatchingStringsToolStripMenuItem,
            this.purgeUnnusedStringsToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.toolsToolStripMenuItem.Text = "Actions";
            // 
            // enumificationToolStripMenuItem
            // 
            this.enumificationToolStripMenuItem.Name = "enumificationToolStripMenuItem";
            this.enumificationToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.enumificationToolStripMenuItem.Text = "Enumify Names";
            this.enumificationToolStripMenuItem.Click += new System.EventHandler(this.enumificationToolStripMenuItem_Click);
            // 
            // concatenateMatchingStringsToolStripMenuItem
            // 
            this.concatenateMatchingStringsToolStripMenuItem.Name = "concatenateMatchingStringsToolStripMenuItem";
            this.concatenateMatchingStringsToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.concatenateMatchingStringsToolStripMenuItem.Text = "Concatenate Matching Strings";
            this.concatenateMatchingStringsToolStripMenuItem.Click += new System.EventHandler(this.concatenateMatchingStringsToolStripMenuItem_Click);
            // 
            // purgeUnnusedStringsToolStripMenuItem
            // 
            this.purgeUnnusedStringsToolStripMenuItem.Name = "purgeUnnusedStringsToolStripMenuItem";
            this.purgeUnnusedStringsToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.purgeUnnusedStringsToolStripMenuItem.Text = "Purge unnused strings";
            this.purgeUnnusedStringsToolStripMenuItem.Click += new System.EventHandler(this.purgeUnnusedStringsToolStripMenuItem_Click);
            // 
            // openFileDLG
            // 
            this.openFileDLG.FileName = "openFileDialog1";
            // 
            // EmailStar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(904, 565);
            this.Controls.Add(this.selectedemailgroup);
            this.Controls.Add(this.pickparticipant);
            this.Controls.Add(this.removeparticipant);
            this.Controls.Add(this.addparticipant);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.participantlist);
            this.Controls.Add(this.removeemailfromthread);
            this.Controls.Add(this.addemailtothread);
            this.Controls.Add(this.removethread);
            this.Controls.Add(this.addthread);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.emailsinthread);
            this.Controls.Add(this.afadf);
            this.Controls.Add(this.threadlist);
            this.Controls.Add(this.threadname);
            this.Controls.Add(this.menuStrip1);
            this.MinimumSize = new System.Drawing.Size(920, 540);
            this.Name = "EmailStar";
            this.Text = "EmailStar, Thread View";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.selectedemailgroup.ResumeLayout(false);
            this.selectedemailgroup.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox toemail;
        private System.Windows.Forms.ComboBox fromemail;
        private System.Windows.Forms.TextBox emailtitle;
        private System.Windows.Forms.TextBox emailcontent;
        private System.Windows.Forms.TextBox emaildelay;
        private System.Windows.Forms.CheckBox emailblocks;
        private System.Windows.Forms.Button removeresponse;
        private System.Windows.Forms.Button addresponse;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ListBox emailresponses;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox addemail;
        private System.Windows.Forms.TextBox responseenumname;
        private System.Windows.Forms.TextBox responsestring;
        private System.Windows.Forms.CheckBox responsefirestthread;
        private System.Windows.Forms.ComboBox threadtofire;
        private System.Windows.Forms.CheckBox responseendsthread;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox scriptemailenumoverride;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox selectedemailgroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem quickAddToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emailerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadDatasetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveDataToolStripMenuItem;
        private System.Windows.Forms.ComboBox pickparticipant;
        private System.Windows.Forms.Button removeparticipant;
        private System.Windows.Forms.Button addparticipant;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox participantlist;
        private System.Windows.Forms.Button removeemailfromthread;
        private System.Windows.Forms.Button addemailtothread;
        private System.Windows.Forms.Button removethread;
        private System.Windows.Forms.Button addthread;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox emailsinthread;
        private System.Windows.Forms.Label afadf;
        private System.Windows.Forms.ListBox threadlist;
        private System.Windows.Forms.ToolStripMenuItem wtfIsThisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.TextBox threadname;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem exportHeadersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toXgta5ToolStripMenuItem;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.OpenFileDialog openFileDLG;
        private System.Windows.Forms.SaveFileDialog saveFileDLG;
        private System.Windows.Forms.CheckBox autofirecheck;
        private System.Windows.Forms.FolderBrowserDialog exportFolderDlg;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enumificationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem concatenateMatchingStringsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mergeDataToolStripMenuItem;
        private System.Windows.Forms.CheckBox ExportEmailEnum;
        private System.Windows.Forms.ToolStripMenuItem purgeUnnusedStringsToolStripMenuItem;
        private System.Windows.Forms.TextBox textureDictname;
        private System.Windows.Forms.CheckBox hasTextureDict;
        private System.Windows.Forms.CheckBox haslink;
        private System.Windows.Forms.TextBox weblink;

    }
}

