﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmailStar
{
    public partial class EmailersEditDialog : Form
    {
        private EmailStarDataSet ds;

        public EmailersEditDialog(EmailStarDataSet DataSet)
        {
            InitializeComponent();
            // TODO: Complete member initialization
            this.ds = DataSet;




            EmailerList.Items.Clear();

            List<EmailStarDataSet.EmailStarEmailer> mlrs = ds.getMailers();


            foreach (EmailStarDataSet.EmailStarEmailer m in mlrs)
            {
                EmailerList.Items.Add(m);
            }

        }

        private void donebutton_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void AddEmailer_Click(object sender, EventArgs e)
        {
            ds.AddNewMailer();

            EmailerList.Items.Clear();

            List<EmailStarDataSet.EmailStarEmailer> mlrs = ds.getMailers();


            foreach (EmailStarDataSet.EmailStarEmailer m in mlrs)
            {
                EmailerList.Items.Add(m);
            }



        }

        private void RemoveEmailer_Click(object sender, EventArgs e)
        {
            if (EmailerList.SelectedItem == null)
            {
                return;
            }

            EmailStarDataSet.EmailStarEmailer emlr = (EmailStarDataSet.EmailStarEmailer)EmailerList.SelectedItem;

            ds.RemoveMailer(emlr);


            EmailerList.Items.Clear();

            List<EmailStarDataSet.EmailStarEmailer> mlrs = ds.getMailers();


            foreach (EmailStarDataSet.EmailStarEmailer m in mlrs)
            {
                EmailerList.Items.Add(m);
            }
        }

        private void EmailerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //emailerenumname
            //nametext
            //addressstring
            //signoffstring

            if (EmailerList.SelectedItem == null)
            {
                return;
            }

            EmailStarDataSet.EmailStarEmailer emlr = (EmailStarDataSet.EmailStarEmailer) EmailerList.SelectedItem;

            emailerenumname.Text = emlr.GetEnumName();
            nametext.Text        = emlr.GetNameString().ToString();
            addressstring.Text = emlr.GetAddressString().ToString();
            signoffstring.Text = emlr.GetSignOffString().ToString();

            bHasInbox.Checked = emlr.GetHasInbox();


        }

        private void emailerenumname_TextChanged(object sender, EventArgs e)
        {
            if (EmailerList.SelectedItem == null)
            {
                return;
            }

            EmailStarDataSet.EmailStarEmailer emlr = (EmailStarDataSet.EmailStarEmailer)EmailerList.SelectedItem;


            emlr.EditEnum(emailerenumname.Text);


            //force listbox to check the string again
            object o = EmailerList.SelectedItem;
            EmailerList.Items[EmailerList.SelectedIndex] = o;
            EmailerList.SelectedItem = o;
        }

        private void nametext_TextChanged(object sender, EventArgs e)
        {
            if (EmailerList.SelectedItem == null)
            {
                return;
            }

            EmailStarDataSet.EmailStarEmailer emlr = (EmailStarDataSet.EmailStarEmailer)EmailerList.SelectedItem;

            emlr.EditName(nametext.Text);
        }

        private void addressstring_TextChanged(object sender, EventArgs e)
        {
            if (EmailerList.SelectedItem == null)
            {
                return;
            }

            EmailStarDataSet.EmailStarEmailer emlr = (EmailStarDataSet.EmailStarEmailer)EmailerList.SelectedItem;

            emlr.EditAddress(addressstring.Text);
        }

        private void signoffstring_TextChanged(object sender, EventArgs e)
        {
            if (EmailerList.SelectedItem == null)
            {
                return;
            }

            EmailStarDataSet.EmailStarEmailer emlr = (EmailStarDataSet.EmailStarEmailer)EmailerList.SelectedItem;

            emlr.EditSignoff(signoffstring.Text);
        }

        private void bHasInbox_CheckedChanged(object sender, EventArgs e)
        {
            if (EmailerList.SelectedItem == null)
            {
                return;
            }

            EmailStarDataSet.EmailStarEmailer emlr = (EmailStarDataSet.EmailStarEmailer)EmailerList.SelectedItem;

            emlr.EditHasInbox(bHasInbox.Checked);
        }



    }
}
