﻿




using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
namespace EmailStar
{
    public partial class EmailStarDataSet
    {

        ///Data to export
        //List<EmailStarString> Strings;
        //List<EmailStarEmailer> Emailers;
            //uses strings
        //List<EmailStarEmail> Emails;
            //uses emailers, emails and threads
            //contains//List<EmailStarEmailResponse> Responses = null;
                //uses strings and threads
        //List<EmailStarThread> Threads;
            //uses emails

        internal void SaveDataFile(string p)
        {

            FileStream w = File.Create(p);
            BinaryWriter b = new BinaryWriter(w);
            //try
            //{
                //note, convert references to indices with -1 as null


                bool purgingStrings = true;
                EmailStarString topurge;
                while(purgingStrings)
                {
                    purgingStrings = false;
                    topurge = null;
                    foreach (EmailStarString s in Strings)
                    {
                        if (s.MarkedForDeletion)
                        {
                            purgingStrings = true;
                            topurge = s;
                            break;
                        }
                    }

                    if (topurge != null)
                    {
                        Strings.Remove(topurge);
                    }

                }

                //number of strings
                //Strings.Count
                b.Write(Strings.Count);
                foreach (EmailStarString s in Strings)
                {
                    s.Write(b);
                }



                //number of emailers
                b.Write(Emailers.Count);            
                foreach(EmailStarEmailer emlr in Emailers)  
                {
                    emlr.Write(b, Strings);
                }


                //write emails

                b.Write(Emails.Count);
                foreach (EmailStarEmail e in Emails)
                {
                    e.Write(b, Strings,Emailers,Threads,Emails);
                }


                //number of threads

                b.Write(Threads.Count);
                foreach (EmailStarThread t in Threads)
                {
                    t.Write(b,Emails,Emailers);
                }









            //}
            //catch
            //{
            //    MessageBox.Show("Misc fail during file save");
            //}

            b.Close();
            w.Close();




        }
        internal void LoadDataFile(string p)
        {
            FileStream r = File.OpenRead(p);
            BinaryReader b = new BinaryReader(r);

            Strings.Clear();
            Emailers.Clear();
            Emails.Clear();
            Threads.Clear();

            //strings
            int stringtotal = b.ReadInt32();

            for (int i = 0; i < stringtotal; ++i)
            {
                Strings.Add(new EmailStarString(b));
            }

            //emailers
            int emailertotal = b.ReadInt32();

            for (int i = 0; i < emailertotal; ++i)
            {
                Emailers.Add(new EmailStarEmailer(b));
            }

            //emails
            int emailtotal = b.ReadInt32();

            for (int i = 0; i < emailtotal; ++i)
            {
                Emails.Add(new EmailStarEmail(this,b));
            }

            //threads
            int threadtotal = b.ReadInt32();

            for (int i = 0; i < threadtotal; ++i)
            {
                Threads.Add(new EmailStarThread(b));
            }


            b.Close();
            r.Close();


            foreach (EmailStarEmailer emlr in Emailers)
            {
                emlr.Bind(Strings);
            }

            foreach (EmailStarEmail e in Emails)
            {
                e.Bind(Strings, Emailers, Threads, Emails);

            }

            foreach (EmailStarThread t in Threads)
            {
                t.Bind(Emails, Emailers);
            }

            
        }



        internal void ExportTo(string path)
        {
            bool bDirectPath = true;

            if (path != null)
            {
                bDirectPath = false;
            }






            //X:\gta5\script\dev\singleplayer\include\globals\email_generated_globals.sch
            //X:\gta5\script\dev\singleplayer\include\private\email_generated_functions.sch
            //X:\gta5\assets\GameText\American\americanEmail.txt
            string globalsfile = "\\email_generated_globals.sch";
            string funfile = "\\email_generated_functions.sch";
            string textfile = "\\americanEmail.txt";
            if (bDirectPath)
            {
                globalsfile = "X:\\gta5\\script\\dev\\singleplayer\\include\\globals" + globalsfile;
                funfile = "X:\\gta5\\script\\dev\\singleplayer\\include\\private" + funfile;
                textfile = "X:\\gta5\\assets\\GameText\\American" + textfile;
            }
            else
            {
                globalsfile = path + globalsfile;
                funfile = path + funfile;
                textfile = path + textfile;
            }
            //TODO, amalgamate matching strings

            //enumify and check for duplicate enums



            //////////////////////////americanEmail.txt // textfile
            StreamWriter wAmericanEmail;


            try
            {
                wAmericanEmail = new StreamWriter(textfile);

            }
            catch{
                MessageBox.Show("Could not open " + textfile + " for write, is it checked out or is the target directory read only?", "Export failed");
                return;
            }
            wAmericanEmail.Write("start\n\nEMAIL\n\nend\n\n\n\n{---------Auto generated email strings--------}\n\n\n");





            wAmericanEmail.Write("[EM_INBOX:EMAIL]\nInbox\n\n");
            wAmericanEmail.Write("[EM_RESPONSE:EMAIL]\nPick a response\n\n");


            foreach (EmailStarString s in Strings)
            {
                if (!s.MarkedForDeletion)
                {

                    bool bIsEmailerName = false;
                    /*
                    foreach(EmailStarEmailer emlr in Emailers)
                    {
                        if (emlr.GetNameString() == s)
                        {
                            bIsEmailerName = true;
                        }
                    }*/
                    //is now content strings that are excluded
                    foreach (EmailStarEmail e in Emails)
                    {
                        if (e.GetContent() == s)
                        {
                            bIsEmailerName = true;
                        }
                    }


                    bool bVeto = false;
                    if(!bIsEmailerName)
                    {
                        string str = s.ToString();
                        if (str.CompareTo("Dynamic E-Mail") == 0)
                        {
                               bVeto = true;
                        }else if(str.CompareTo("") == 0)
                        {
                            bVeto = true;
                        }
                    }

                    if (!bVeto)
                    {
                       // if (!bIsEmailerName)
                       // {
                      //      wAmericanEmail.Write("[EMSTR_" + Strings.IndexOf(s) + ":EMAIL]\n");
                      //  }
                     //   else
                      //  {
                            wAmericanEmail.Write("[EMSTR_" + Strings.IndexOf(s) + "]\n");
                      //  }
                            string sin = s.ToString();
                            sin = sin.Replace("\n\n\n", "\n");
                            sin = sin.Replace("\n\n", "\n");
                            sin = sin.Replace("\r\r\r", "\r");
                            sin = sin.Replace("\r\r", "\r");
                            wAmericanEmail.Write(sin + "\n\n");
                    }
                }
            }






            wAmericanEmail.Close();
            //////////////////////////email_generated_globals.sch // globalsfile
            StreamWriter wGlobals;


            try
            {
                wGlobals = new StreamWriter(globalsfile);

            }
            catch
            {
                MessageBox.Show("Could not open " + globalsfile + " for write, is it checked out or is the target directory read only?", "Export failed");
                return;
            }



            int highresponses = 0;
            foreach (EmailStarEmail e in Emails)
            {
                if (e.TotalResponses() > highresponses)
                {
                    highresponses = e.TotalResponses();
                }
            }
            if (highresponses == 0)
            {
                highresponses = 1;
            }
            //CONST_INT MAX_RESPONSES_IN_EMAIL 
            wGlobals.Write("CONST_INT MAX_RESPONSES_IN_EMAIL " + highresponses + "\n");
            //
            //CONST_INT TOTAL_EMAILS_IN_GAME //count
            wGlobals.Write("CONST_INT TOTAL_EMAILS_IN_GAME " + Emails.Count + "\n");
            //CONST_INT TOTAL_EMAIL_THREADS_IN_GAME //count
            wGlobals.Write("CONST_INT TOTAL_EMAIL_THREADS_IN_GAME " + Threads.Count + "\n");
            //
            wGlobals.Write("CONST_INT EMAIL_AUTO_RESPONSE_ID_TAG_INDEX -1\n");


            int highparts = 0;
            int highlength = 0;
            foreach (EmailStarThread t in Threads)
            {
                if (t.TotalParticipants() > highparts)
                {
                    highparts = t.TotalParticipants();
                }
                if (t.TotalMails() > highlength)
                {
                    highlength = t.TotalMails();
                }
            }

            //CONST_INT MAX_EMAIL_THREAD_PARTICIPANTS //iterate threads for most participants
            wGlobals.Write("CONST_INT MAX_EMAIL_THREAD_PARTICIPANTS " + highparts + "\n");
            //CONST_INT MAX_THREAD_LENGTH //iterate threads for longest

          


            wGlobals.Write("CONST_INT MAX_THREAD_LENGTH " + highlength + "\n");
            //
            //CONST_INT TOTAL_INBOXES  // number from max mailers // TODO maybe tag certain mailers and bind them to specific inboxes
            wGlobals.Write("CONST_INT TOTAL_EMAILERS " + Emailers.Count + "\n");



             


            int inboxcount = 0;

            foreach (EmailStarEmailer emlr in Emailers)
            {
                if (emlr.GetHasInbox())
                {
                    ++inboxcount;
                }
            }

            wGlobals.Write("CONST_INT TOTAL_INBOXES " + inboxcount + "\n");



            int ninboxcount = inboxcount;
            inboxcount = 0;
            
            wGlobals.Write("\nENUM EMAILER_ENUMS\n");
            for (int i = 0; i < Emailers.Count; ++i)
            {

                //save the enum
                wGlobals.Write("\t" + Emailers[i].GetEnumName());

                //if this emailer has an inbox
                if (Emailers[i].GetHasInbox())
                {
                    wGlobals.Write(" = " + inboxcount);

                    ++inboxcount;
                }
                else
                {

                    wGlobals.Write(" = " + ninboxcount);
                    ++ninboxcount;
                }

                //if not last then comma
                if (i != (Emailers.Count - 1))
                {
                    wGlobals.Write(",");
                }
                wGlobals.Write("\n");
            }
            wGlobals.Write("ENDENUM\n\n");



            wGlobals.Write("\nENUM EMAIL_THREAD_ENUMS\n");
            for (int i = 0; i < Threads.Count; ++i)
            {
                //save the enum
                wGlobals.Write("\t" + Threads[i].GetEnumName());
                //if not last then comma
               // if (i != (Threads.Count - 1))
               // {
                    wGlobals.Write(",");
               // }
                wGlobals.Write("\n");
            }
            wGlobals.Write("\tWEAPON_STOCK_DUMMY\n");
            
            wGlobals.Write("ENDENUM\n");



            bool doEmailNames = false;
            int finalfound = -1;

            foreach (EmailStarEmail e in Emails)
            {
                if (e.ExportMyEnum() == true)
                {
                    doEmailNames = true;
                    finalfound = Emails.IndexOf(e);

                }
            }
            
            if (doEmailNames)
            {
                wGlobals.Write("\nENUM EMAIL_MESSAGE_ENUMS\n");
                for (int i = 0; i < Emails.Count; ++i)
                {
                    if (Emails[i].ExportMyEnum() == true)
                    {
                        //save the enum
                        wGlobals.Write("\t" + Emails[i].GetEnum() + " = " + i);

                        //if not last then comma
                        if (i != finalfound)
                        {
                            wGlobals.Write(",");
                        }
                        wGlobals.Write("\n");
                    }
                }
                wGlobals.Write("ENDENUM\n");

            }


            wGlobals.Write("\n\n\n\n\n");


            wGlobals.Close();
            //////////////////////////email_generated_functions.sch // funfile
            StreamWriter wFunctions;
            try
            {
                wFunctions = new StreamWriter(funfile);
            }
            catch
            {
                MessageBox.Show("Could not open " + funfile + " for write, is it checked out or is the target directory read only?", "Export failed");
                return;
            }

            wFunctions.Write("USING \"globals.sch\"\n\n\n");

            wFunctions.Write(" /////THIS FILE IS AUTO GENERATED BY EMAILSTAR\n\n\n");

            //old lookup function snipped thanks to new script functionality


            //"EMSTR_"


            //PROC INITIALISE_EMAILS() // fill out all the thread data and indices to the following structures
            wFunctions.Write("PROC INITIALISE_EMAIL_SYSTEM_CONTENT()\n\n");


            wFunctions.Write("\n\n\t/////Emails\n\n");
            int currmail = 0;
            foreach (EmailStarEmail e in Emails)
            {
                string cstringtmp = e.GetContent().ToString().Split('\r')[0];
                cstringtmp = cstringtmp.Split('\n')[0];


                wFunctions.Write("\t//////// " + e.GetTitle() 
                    + " \n\t// TO: " + e.GetTo().GetAddressString() 
                    + " \n\t// FROM: " + e.GetFrom().GetAddressString()
                    + "\n\t// " + cstringtmp + "\n\n");
                wFunctions.Write("\tg_AllEmails[" + currmail + "].iTitleTag = " + Strings.IndexOf(e.GetTitle()) + "\n");
                wFunctions.Write("\tg_AllEmails[" + currmail + "].iContentTag = " + Strings.IndexOf(e.GetContent()) + "\n\n");

                //efrom and to
                wFunctions.Write("\tg_AllEmails[" + currmail + "].eFrom = " + e.GetFrom().GetEnumName() + "\n");
                wFunctions.Write("\tg_AllEmails[" + currmail + "].eTo = " + e.GetTo().GetEnumName() + "\n");
                wFunctions.Write("\tg_AllEmails[" + currmail + "].bEmailBlocksThread = " + e.GetBlocks() + "\n");
                wFunctions.Write("\tg_AllEmails[" + currmail + "].iDelay = " + e.GetDelay() + "000\n\n");


                wFunctions.Write("\tg_AllEmails[" + currmail + "].iResponses = " + e.TotalResponses() + "\n\n");


                int currresponse = 0;
                foreach (EmailStarEmail.EmailStarEmailResponse r in e.GetResponses())
                {
                    //currresponse = Emails.IndexOf(r.GetEmail());
                    //wFunctions.Write("\tg_AllEmails[" + currmail + "].Responses[currresponse]. = "

                    //INT iResponseNameTag//the short version tag for picking this response
                    if (!r.GetAutoFires())
                    {
                        //not auto firing, add true index
                        wFunctions.Write("\tg_AllEmails[" + currmail + "].Responses[" + currresponse + "].iResponseNameTag = " + Strings.IndexOf(r.GetSelectionString()) + "\n");
                    }
                    else
                    {
                        //autofiring, add enum
                        wFunctions.Write("\tg_AllEmails[" + currmail + "].Responses[" + currresponse + "].iResponseNameTag = EMAIL_AUTO_RESPONSE_ID_TAG_INDEX\n");
                    }
                    //INT iResponseMail//the index of the email response sent when this is picked
                    wFunctions.Write("\tg_AllEmails[" + currmail + "].Responses[" + currresponse + "].iResponseMail = " + Emails.IndexOf(r.GetEmail()) + "\n");
                    //BOOL bResponseEndsThread//does this response being picked cause the thread to finish?
                    wFunctions.Write("\tg_AllEmails[" + currmail + "].Responses[" + currresponse + "].bResponseEndsThread = " + r.GetEndsThread() + "\n");
                    //INT iResponseFiresThread//-1 means triggers nothing, otherwise attempt to start EMAIL_THREAD in threadlist
                    if (r.GetFiresThread() == null)
                    {
                        wFunctions.Write("\tg_AllEmails[" + currmail + "].Responses[" + currresponse + "].iResponseFiresThread = -1\n\n");
                    }
                    else
                    {
                        wFunctions.Write("\tg_AllEmails[" + currmail + "].Responses[" + currresponse + "].iResponseFiresThread = " + Threads.IndexOf(r.GetFiresThread()) + "\n\n");
                    }

                    ++currresponse;
                }
                ++currmail;
            }





            wFunctions.Write("\n\n\t/////Threads\n");

            //g_AllEmailThreads
            //INT iParticipants;
            //EMAILER Participants[MAX_EMAIL_THREAD_PARTICIPANTS] //the people being mailed as part of this thread, if the current message is not TO or FROM them (within the email) they are marked as CC

            //INT ThreadMails[MAX_THREAD_LENGTH]//indices of emails that make up the core this thread, including replies - thus this may be greater than thread path
            //INT ThreadLog[MAX_THREAD_LOG_LENGTH]//The main path of emails through the thread, not including responses
            //INT iCurrentlyAt//how far through the thread path the mail is
            //INT iMailsInLog//how much of the thread log has been used
            //INT iTotalThreadMails//how many mails are in the log
            //INT iDelay
            //BOOL bBLockedUntilResponse//this thread is stopped, waiting for a player response to iCurrentlyAt
            //INT iAutoFireThread

            //
            //
            //int thread 
            ////TODO CHECK THIS WORKS///
            int currthread = 0;
            foreach (EmailStarThread t in Threads)
            {
                wFunctions.Write("\t//////////// " + t.GetEnumName() + "\n");
                //resets
                wFunctions.Write("\tg_AllEmailThreads[" + currthread + "].bBLockedUntilResponse = false\n");
                wFunctions.Write("\tg_AllEmailThreads[" + currthread + "].iCurrentlyAt = 0\n");
                wFunctions.Write("\tg_AllEmailThreads[" + currthread + "].iMailsInLog = 0\n");
                wFunctions.Write("\tg_AllEmailThreads[" + currthread + "].iDelay = 0\n");

                wFunctions.Write("\tg_AllEmailThreads[" + currthread + "].iParticipants = " + t.TotalParticipants() + "\n");
                int currpart = 0;
                foreach (EmailStarEmailer e in t.GetParticipants())
                {

                    wFunctions.Write("\t\tg_AllEmailThreads[" + currthread + "].Participants[" + currpart + "] = " + e.GetEnumName() + "\n");
                    ++currpart;
                }

                currmail = 0;
                wFunctions.Write("\tg_AllEmailThreads[" + currthread + "].iTotalThreadMails = " + t.TotalMails() + "\n");
                foreach (EmailStarEmail e in t.GetMails())
                {
                    wFunctions.Write("\t\tg_AllEmailThreads[" + currthread + "].ThreadMails[" + currmail + "] = " + Emails.IndexOf(e) + "\n");
                    ++currmail;
                }


                ++currthread;
                wFunctions.Write("\n\n");
            }

            //ENDPROC
            //
            //
            wFunctions.Write("ENDPROC\n\n\n");


            //Add
            //MailerUpperEnumNames
            //SystemStringIndexTracker
            //FUNC STRING RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(INT indice)


            //FUNC STRING GET_EMAILER_NAME_TAG(EMAILER_ENUMS mailer)
            wFunctions.Write("FUNC STRING GET_EMAILER_NAME_TAG(EMAILER_ENUMS mailer)\n");
            wFunctions.Write("\tSWITCH mailer\n");
            foreach (EmailStarEmailer em in Emailers)
            {
                wFunctions.Write("\t\tCASE " + em.GetEnumName() + "\n");
                wFunctions.Write("\t\t\tRETURN \"EMSTR_" + Strings.IndexOf(em.GetNameString()) + "\"\n");
            }
            wFunctions.Write("\tENDSWITCH\n");
            wFunctions.Write("\tRETURN \"NULL\"\n");
            wFunctions.Write("ENDFUNC\n");


            //FUNC STRING GET_EMAILER_ADDRESS_TAG(EMAILER_ENUMS mailer)
            wFunctions.Write("FUNC STRING GET_EMAILER_ADDRESS_TAG(EMAILER_ENUMS mailer)\n");
            wFunctions.Write("\tSWITCH mailer\n");
            foreach (EmailStarEmailer em in Emailers)
            {
                wFunctions.Write("\t\tCASE " + em.GetEnumName() + "\n");
                wFunctions.Write("\t\t\tRETURN \"EMSTR_" + Strings.IndexOf(em.GetAddressString())+ "\"\n");
            }
            wFunctions.Write("\tENDSWITCH\n");
            wFunctions.Write("\tRETURN \"NULL\"\n");
            wFunctions.Write("ENDFUNC\n");

            //FUNC STRING GET_EMAILER_SIGNOFF_TAG(EMAILER_ENUMS mailer)
            wFunctions.Write("FUNC STRING GET_EMAILER_SIGNOFF_TAG(EMAILER_ENUMS mailer)\n");
            wFunctions.Write("\tSWITCH mailer\n");
            foreach (EmailStarEmailer em in Emailers)
            {
                wFunctions.Write("\t\tCASE " + em.GetEnumName() + "\n");
                wFunctions.Write("\t\t\tRETURN \"EMSTR_" + Strings.IndexOf(em.GetSignOffString()) + "\"\n");
            }
            wFunctions.Write("\tENDSWITCH\n");
            wFunctions.Write("\tRETURN \"NULL\"\n");
            wFunctions.Write("ENDFUNC\n");



            //FUNC BOOL GET_DOES_EMAIL_HAVE_ATTACHMENT_TEXTURE(INT emailIndex)

            wFunctions.Write("FUNC BOOL GET_DOES_EMAIL_HAVE_ATTACHMENT_TEXTURE(INT emailIndex)\n");
            wFunctions.Write("\tSWITCH emailIndex\n");
            foreach(EmailStarEmail e in Emails)
            {

                if(e.HasTextureDictionary())
                {
                    int ind = Emails.IndexOf(e);

                    wFunctions.Write("\t\tCASE " + ind + "\n");
                }

                
            }
            wFunctions.Write("\t\t\tRETURN TRUE\n");


            wFunctions.Write("\tENDSWITCH\n");
            wFunctions.Write("\tRETURN FALSE\n");
            wFunctions.Write("ENDFUNC\n");



            //FUNC STRING GET_EMAIL_ATTACHMENT_TEXTURE_DICTIONARY_NAME(INT emailIndex)

            wFunctions.Write("FUNC STRING GET_EMAIL_ATTACHMENT_TEXTURE_DICTIONARY_NAME(INT emailIndex)\n");
            wFunctions.Write("\tSWITCH emailIndex\n");
            foreach (EmailStarEmail e in Emails)
            {

                if (e.HasTextureDictionary())
                {
                    int ind = Emails.IndexOf(e);

                    wFunctions.Write("\t\tCASE " + ind + "\n");
                    wFunctions.Write("\t\t\tRETURN \"" + e.GetTextureDictionaryString() + "\"\n");
                }

                
            }
            wFunctions.Write("\tENDSWITCH\n");
            wFunctions.Write("\tRETURN \"NULL\"\n");
            wFunctions.Write("ENDFUNC\n\n");


            wFunctions.Write("FUNC BOOL GET_DOES_EMAIL_HAVE_URL(INT emailIndex)\n");
            wFunctions.Write("\tSWITCH emailIndex\n");
            foreach (EmailStarEmail e in Emails)
            {

                if (e.HasLink())
                {
                    int ind = Emails.IndexOf(e);

                    wFunctions.Write("\t\tCASE " + ind + "\n");
                }


            }
            wFunctions.Write("\t\t\tRETURN TRUE\n");


            wFunctions.Write("\tENDSWITCH\n");
            wFunctions.Write("\tRETURN FALSE\n");
            wFunctions.Write("ENDFUNC\n");



            //FUNC STRING GET_EMAIL_ATTACHMENT_TEXTURE_DICTIONARY_NAME(INT emailIndex)

            wFunctions.Write("FUNC STRING GET_EMAIL_URL_STRING(INT emailIndex)\n");
            wFunctions.Write("\tSWITCH emailIndex\n");
            foreach (EmailStarEmail e in Emails)
            {

                if (e.HasLink())
                {
                    int ind = Emails.IndexOf(e);

                    wFunctions.Write("\t\tCASE " + ind + "\n");
                    wFunctions.Write("\t\t\tRETURN \"" + e.GetUrl() + "\"\n");
                }


            }
            wFunctions.Write("\tENDSWITCH\n");
            wFunctions.Write("\tRETURN \"NULL\"\n");
            wFunctions.Write("ENDFUNC\n\n");



            wFunctions.Close();









             MessageBox.Show("Export complete! Remember to run X:\\gta5\\build\\dev\\common\\text\\american.bat to ensure your text is synched!", "Success!");


        }

        internal void CorrectEnums()
        {
            //put all the items into an enum helper

            EnumStringHelper esh = new EnumStringHelper();

            foreach(EmailStarEmailer e in Emailers)
            {
                esh.RegisterEnumerable(e);
            }
            foreach(EmailStarEmail e in Emails)
            {
                 esh.RegisterEnumerable(e);
            }
            foreach (EmailStarThread e in Threads)
            {
                esh.RegisterEnumerable(e);
            }
            esh.ProcessEnumStrings();

        }

        internal void ConcatenateMatchingStringReferences()
        {
            //Strings

            //check each string against each other string
                //when matches have found amalgamate all occurences of that string to the 
                //first occurence of that string
                    //do for emailers
                    //do for emails
                    //do for responses

            List<EmailStarString> todelete = new List<EmailStarString>();

            for (int i = 0; i < Strings.Count-1; ++i)
            {
                EmailStarString s1 = Strings[i];

                //
                for (int j = i; j < Strings.Count; ++j)
                {
                    EmailStarString s2 = Strings[j];

                    if (s1 != s2)//not the same reference
                    {
                        if (string.Compare(s1.ToString(), s2.ToString()) == 0)
                        {
                            //make all references of s2 point to s1
                            foreach (EmailStarEmail e in Emails)
                            {
                                e.ReplaceString(s2, s1);
                            }
                            foreach (EmailStarEmailer e in Emailers)
                            {
                                e.ReplaceString(s2, s1);
                            }

                            //remove s2

                            todelete.Add(s2);

                            
                        }
                    }
                }
            }

            //now remove any strings that are marked for deletion
            foreach (EmailStarString s in todelete)
            {
                if (Strings.Contains(s))
                {
                    Strings.Remove(s);
                }
            }
        }

        internal void Merge(EmailStarDataSet m)
        {
            //remove any duplicate enums before merge

            CorrectEnums();


            //smoosh the lists togeter
            //List<EmailStarString> Strings;
            //List<EmailStarEmailer> Emailers;
            //List<EmailStarEmail> Emails;
            //List<EmailStarThread> Threads;

            foreach(EmailStarString s in m.Strings)
            {
                Strings.Add(s);

            }
            foreach (EmailStarEmailer s in m.Emailers)
            {
                Emailers.Add(s);
            }
            foreach (EmailStarEmail s in m.Emails)
            {
                Emails.Add(s);
                s.OverrideParent(this);
            }
            foreach (EmailStarThread s in m.Threads)
            {
                Threads.Add(s);
            }

            //concatenate entries with matching enum names




            this.MergeItemsWithMatchingEnums();



            

        }

        private void MergeItemsWithMatchingEnums()
        {

            ConcatenateMatchingStringReferences();

            List<EmailStarEmailer> MarkedForDeletion = new List<EmailStarEmailer>();

            foreach (EmailStarEmailer em1 in Emailers)
            {
                foreach (EmailStarEmailer em2 in Emailers)
                {
                    if (em1 != em2 )
                    {
                        if (!MarkedForDeletion.Contains(em1) && !MarkedForDeletion.Contains(em2))
                        {
                            if (string.Compare(em1.GetEnumName(), em2.GetEnumName()) == 0)
                            {
                                //replace all instaces of references to em2 with em1
                                MarkedForDeletion.Add(em2);

                                //purge from 
                                foreach(EmailStarEmail e in Emails)
                                {
                                    e.ReplaceEmailer(em1,em2);
                                }
                                foreach (EmailStarThread t in Threads)
                                {
                                    t.ReplaceEmailer(em1, em2);
                                }

                            }
                        }
                    }
                }
            }
            //remove MarkedForDeletion from the 
            foreach (EmailStarEmailer marked in MarkedForDeletion)
            {
                Emailers.Remove(marked);
            }






            List<EmailStarEmail> MarkedEmailsToDelete = new List<EmailStarEmail>();
            foreach (EmailStarEmail e1 in Emails)
            {
                foreach (EmailStarEmail e2 in Emails)
                {
                    if (e1 != e2)
                    {
                        if (!MarkedEmailsToDelete.Contains(e1) && !MarkedEmailsToDelete.Contains(e2))
                        {

                            if (string.Compare(e1.GetEnum(), e2.GetEnum()) == 0)
                            {
                                MarkedEmailsToDelete.Add(e2);

                                //replace emails in

                                //emails
                                foreach(EmailStarEmail e3 in Emails)
                                {
                                    if (e3 != e2)
                                    {
                                        e3.ReplaceEmails(e1, e2);
                                    }
                                }


                                //threads
                                foreach (EmailStarThread t in Threads)
                                {
                                    t.ReplaceEmails(e1,e2);
                                }

                            }
                        }
                    }
                }
            }
            foreach (EmailStarEmail s in MarkedEmailsToDelete)
            {
                Emails.Remove(s);
            }


            List<EmailStarThread> MarkedThreadsToDelete = new List<EmailStarThread>();

            foreach (EmailStarThread t1 in Threads)
            {
                foreach (EmailStarThread t2 in Threads)
                {
                    //continue from here

                    if (t1 != t2)
                    {
                        if (!MarkedThreadsToDelete.Contains(t1) && !MarkedThreadsToDelete.Contains(t2))
                        {

                            if (string.Compare(t1.GetEnumName(), t2.GetEnumName()) == 0)
                            {
                                //if so replace all references to t2 with t1
                                MarkedThreadsToDelete.Add(t2);
                                //Threads are only referenced by responses directly

                                foreach(EmailStarEmail e in Emails)
                                {
                                    e.ReplaceThreads(t1,t2);
                                }
                            }
                        }
                    }

                }
            }

            foreach (EmailStarThread td in MarkedThreadsToDelete)
            {
                Threads.Remove(td);
            }
        }

        internal void PurgeUnusedMails()
        {
            foreach (EmailStarEmail e in Emails)
            {
                e.ClearUsedTag();
            }

            foreach (EmailStarThread t in Threads)
            {
                foreach (EmailStarEmail e in t.GetMails())
                {
                    e.SetUsedTag();
                }
            }

            for (int i = 0; i < Emails.Count; ++i)
            {
                if (!Emails[i].GetUsedTag())
                {
                    Emails.RemoveAt(i);

                    --i;
                }
            }
        }

        internal void RemoveUnnusedStrings()
        {


            foreach (EmailStarString s in Strings)
            {
                if (!isStringUsed(s))
                {
                    s.MarkedForDeletion = true;
                }
            }

            //remove all marked for delete
            
        }

        private bool isStringUsed(EmailStarString s)
        {
            foreach (EmailStarEmail e in Emails)
            {
                if (e.UsesString(s))
                {
                    return true;
                }
            }

            foreach (EmailStarEmailer e in Emailers)
            {
                if (e.UsesString(s))
                {
                    return true;
                }
            }
            return false;
        }
    }
}















