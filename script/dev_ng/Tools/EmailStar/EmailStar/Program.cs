﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EmailStar
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            EmailStarDataSet e = new EmailStarDataSet();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new EmailStar(e));
        }
    }
}
