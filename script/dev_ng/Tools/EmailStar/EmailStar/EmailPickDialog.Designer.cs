﻿namespace EmailStar
{
    partial class EmailPickDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.blankmail = new System.Windows.Forms.Button();
            this.ortext = new System.Windows.Forms.Label();
            this.pickemail = new System.Windows.Forms.ComboBox();
            this.addexsisting = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // blankmail
            // 
            this.blankmail.Location = new System.Drawing.Point(12, 12);
            this.blankmail.Name = "blankmail";
            this.blankmail.Size = new System.Drawing.Size(238, 23);
            this.blankmail.TabIndex = 0;
            this.blankmail.Text = "Create New Blank Email";
            this.blankmail.UseVisualStyleBackColor = true;
            this.blankmail.Click += new System.EventHandler(this.blankmail_Click);
            // 
            // ortext
            // 
            this.ortext.AutoSize = true;
            this.ortext.Location = new System.Drawing.Point(116, 48);
            this.ortext.Name = "ortext";
            this.ortext.Size = new System.Drawing.Size(23, 13);
            this.ortext.TabIndex = 1;
            this.ortext.Text = "OR";
            // 
            // pickemail
            // 
            this.pickemail.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pickemail.FormattingEnabled = true;
            this.pickemail.Location = new System.Drawing.Point(12, 101);
            this.pickemail.Name = "pickemail";
            this.pickemail.Size = new System.Drawing.Size(238, 21);
            this.pickemail.TabIndex = 2;
            // 
            // addexsisting
            // 
            this.addexsisting.Location = new System.Drawing.Point(12, 72);
            this.addexsisting.Name = "addexsisting";
            this.addexsisting.Size = new System.Drawing.Size(238, 23);
            this.addexsisting.TabIndex = 3;
            this.addexsisting.Text = "Add This Exsisting Email";
            this.addexsisting.UseVisualStyleBackColor = true;
            this.addexsisting.Click += new System.EventHandler(this.addexsisting_Click);
            // 
            // EmailPickDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(262, 134);
            this.Controls.Add(this.addexsisting);
            this.Controls.Add(this.pickemail);
            this.Controls.Add(this.ortext);
            this.Controls.Add(this.blankmail);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EmailPickDialog";
            this.Text = "Add Email";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button blankmail;
        private System.Windows.Forms.Label ortext;
        private System.Windows.Forms.ComboBox pickemail;
        private System.Windows.Forms.Button addexsisting;
    }
}