﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmailStar
{
    public partial class EmailStarDataSet
    {
        public EmailStarDataSet()
        {
            Strings = new List<EmailStarString>();
            Emailers = new List<EmailStarEmailer>();
            Emails = new List<EmailStarEmail>();
            Threads = new List<EmailStarThread>();
        }


        //data/////////
        List<EmailStarString> Strings;
        List<EmailStarEmailer> Emailers;
        List<EmailStarEmail> Emails;
        List<EmailStarThread> Threads;


        public enum EmailStarStringType
        {
            Unset,
            Name,
            Address,
            Signoff,
            Title,
            Content,
            Response
        }

        //strings
        public class EmailStarString
        {
            EmailStarStringType type;
            string s = "UNSET STRING";


            public bool MarkedForDeletion = false;


            public EmailStarString(string p, EmailStarStringType typeIn)
            {
                this.s = p;
                type = typeIn;
            }



            public override string ToString()
            {
                return s;// +"(" + type + ")";
            }

            internal void SetString(string p)
            {
                s = p.Replace("\n","");
            }

            internal void Write(System.IO.BinaryWriter b)
            {
                //EmailStarStringType type;
                //string s = "UNSET STRING";

                b.Write((int)type);

                b.Write(s);

            }

            public EmailStarString(System.IO.BinaryReader b)
            {
                int ty = b.ReadInt32();
                type = (EmailStarStringType)ty;

                s = b.ReadString();
            }


        }

        //emailer
        public class EmailStarEmailer : EnumStringHelper.EnumStringListable
        {
            string EnumName = "UNSET EMAILER ENUM";

            public override string ToString()
            {
                return EnumName;
            }

            EmailStarString name = null;
            EmailStarString address = null;
            EmailStarString signoff = null;

            bool bHasInbox = false;


            public EmailStarEmailer(EmailStarString n, EmailStarString a, EmailStarString s)
            {
                this.name = n;
                this.address = a;
                this.signoff = s;
            }



            internal string GetEnumName()
            {
                return EnumName;
            }

            internal EmailStarString GetNameString()
            {
                return name;
            }

            internal EmailStarString GetAddressString()
            {
                return address;
            }

            internal EmailStarString GetSignOffString()
            {
                return signoff;
            }

            internal void EditEnum(string p)
            {
                EnumName = p;
            }

            internal void EditName(string p)
            {
                name.SetString(p);
            }

            internal void EditAddress(string p)
            {
                address.SetString(p);
            }

            internal void EditSignoff(string p)
            {
                signoff.SetString(p);
            }

            internal void Write(System.IO.BinaryWriter b, List<EmailStarString> Strings)
            {
                //string EnumName = "UNSET EMAILER ENUM"
                //EmailStarString name = null;
                //EmailStarString address = null;
                //EmailStarString signoff = null;

                b.Write(EnumName);

                if(name != null)
                {
                    b.Write(Strings.IndexOf(name));
                }else{
                    b.Write(-1);
                }

                if (address != null)
                {
                    b.Write(Strings.IndexOf(address));
                }
                else
                {
                    b.Write(-1);
                }

                if (signoff != null)
                {
                    b.Write(Strings.IndexOf(signoff));
                }
                else
                {
                    b.Write(-1);
                }


                b.Write(bHasInbox);

            }

            int nt, at, st;

            public EmailStarEmailer(System.IO.BinaryReader b)
            {
                //string EnumName = "UNSET EMAILER ENUM"
                //EmailStarString name = null;
                //EmailStarString address = null;
                //EmailStarString signoff = null;

                EnumName = b.ReadString();

                nt = b.ReadInt32();
                at = b.ReadInt32();
                st = b.ReadInt32();

                bHasInbox = b.ReadBoolean();

            }

            internal void Bind(List<EmailStarString> Strings)
            {
                this.name = Strings[nt];
                this.address = Strings[at];
                this.signoff = Strings[st];
            }

            public string eslGetString()
            {
                return this.EnumName;
            }

            public void eslSetString(string s)
            {
                EnumName = s;
            }

            internal void ReplaceString(EmailStarString replace, EmailStarString with)
            {
                if (this.address == replace)
                {
                    address = with;
                }
                if (this.name == replace)
                {
                    name = with;
                }
                if (this.signoff == replace)
                {
                    signoff = with;
                }
            }



            internal void EditHasInbox(bool p)
            {
                bHasInbox = p;
            }

            internal bool GetHasInbox()
            {
                return this.bHasInbox;
            }

            internal bool UsesString(EmailStarString s)
            {
                if (this.name == s)
                {
                    return true;
                }
                if (this.signoff == s)
                {
                    return true;
                }
                if (this.address == s)
                {
                    return true;
                }
                return false;
            }
        }

        //emails
            //responses
        public class EmailStarEmail : EnumStringHelper.EnumStringListable
        {
            string EnumName = "UNSET EMAIL ENUM";

            EmailStarDataSet parent;

            public override string ToString()
            {
                return EnumName;
            }

            public EmailStarEmail(EmailStarDataSet p)
            {
                parent = p;
                Responses = new List<EmailStarEmailResponse>();
            }



            EmailStarString title = null;
            EmailStarString content = null;

            EmailStarEmailer from = null;
            EmailStarEmailer to = null;

            bool blocks = false;
            int delay = 0;

            bool bExportMyEnum = false;

            List<EmailStarEmailResponse> Responses = null;


            private bool bHasTextureDict;
            private string sTexturedictString = "";

            private bool bHasInGameURL;
            private string sInGameURL = "";


            public class EmailStarEmailResponse
            {
                EmailStarDataSet parent;
                public EmailStarEmailResponse(EmailStarDataSet dataset)
                {
                    parent = dataset;

                    SelectionString = new EmailStarString("Unset name",EmailStarStringType.Name);
                    parent.RegisterSystemString(SelectionString);
                }

                public override string ToString()
                {

                    return EnumString;
                }

                string EnumString = "UNSET_ENUM";
                EmailStarString SelectionString = null;

                EmailStarEmail ResponseEmail = null;
                bool EndThread = false;
                bool AutoFire = false;
                EmailStarThread firethread = null;

                internal EmailStarString GetSelectionString()
                {
                    if (SelectionString == null)
                    {
                        SelectionString = new EmailStarString("Unset selection string", EmailStarStringType.Response);
                        parent.RegisterSystemString(SelectionString);
                    }

                    return SelectionString;
                }

                internal string GetEnumString()
                {
                    return EnumString;
                }

                internal EmailStarThread GetFiresThread()
                {
                    return firethread;
                }

                internal bool GetEndsThread()
                {
                    return EndThread;
                }

                internal void SetEnum(string p)
                {
                    this.EnumString = p;
                }

                internal void SetEmail(EmailStarEmail selemail)
                {
                    this.ResponseEmail = selemail;
                }

                internal void SetResponseString(string p)
                {
                    if (SelectionString == null)
                    {
                        SelectionString = new EmailStarString(p, EmailStarStringType.Response);
                        parent.RegisterSystemString(SelectionString);

                    }
                    else
                    {
                        this.SelectionString.SetString(p);
                    }
                }

                internal void SetResponseEndsThread(bool p)
                {
                    this.EndThread = p;
                }

                internal void ClearThreadToFire()
                {
                    firethread = null;
                }

                internal void SetFiresThread(EmailStarThread fires)
                {
                    firethread = fires;
                }

                internal EmailStarEmail GetEmail()
                {
                    return ResponseEmail;
                }

                internal void Write(System.IO.BinaryWriter b, List<EmailStarString> Strings, List<EmailStarEmail> Emails, List<EmailStarThread> Threads)
                {
                    
                    //string EnumString = "Unset Enum";

                    b.Write(EnumString);

                    //EmailStarString SelectionString;
                    if (SelectionString != null)
                    {
                        b.Write(Strings.IndexOf(SelectionString));
                    }
                    else
                    {
                        b.Write(-1);
                    }

                    //EmailStarEmail ResponseEmail = null;
                    if (ResponseEmail != null)
                    {
                        b.Write(Emails.IndexOf(ResponseEmail));
                    }
                    else
                    {
                        b.Write(-1);
                    }

                    //bool EndThread = false;
                    b.Write(EndThread);
                    //bool AutoFire
                    b.Write(AutoFire);
                    //EmailStarThread firethread = null;
                    if (firethread != null)
                    {
                        b.Write(Threads.IndexOf(firethread));
                    }
                    else
                    {
                        b.Write(-1);
                    }
                }

                internal void SetAutoFires(bool p)
                {
                    AutoFire = p;
                }

                internal bool GetAutoFires()
                {
                    return AutoFire;
                }

                int ss, resm, fthread;
                public EmailStarEmailResponse(EmailStarDataSet dataset,System.IO.BinaryReader b)
                {
                    parent = dataset;
                    //string EnumString = "Unset Enum";
                    EnumString = b.ReadString();
                    //EmailStarString SelectionString;
                    ss = b.ReadInt32();
                    //EmailStarEmail ResponseEmail = null;
                    resm = b.ReadInt32();
                    //bool EndThread = false;
                    EndThread = b.ReadBoolean();
                    //bool AutoFire
                    AutoFire = b.ReadBoolean();
                    //EmailStarThread firethread = null;
                    fthread = b.ReadInt32();
                }

                internal void Bind(List<EmailStarString> Strings, List<EmailStarEmail> Emails, List<EmailStarThread> Threads)
                {

                    //EmailStarString SelectionString;
                    //ss = b.ReadInt32();
                    if (ss > -1)
                    {
                        SelectionString = Strings[ss];
                    }
                 
                    //EmailStarEmail ResponseEmail = null;
                    //resm = b.ReadInt32();
                    if (resm > -1)
                    {
                        ResponseEmail = Emails[resm];
                    }
                    //EmailStarThread firethread = null;
                    //fthread = b.ReadInt32();
                    if (fthread > -1)
                    {
                        firethread = Threads[fthread];
                    }
                }

                internal void ReplaceString(EmailStarString replace, EmailStarString with)
                {
                    if (this.SelectionString == replace)
                    {
                        SelectionString = with;
                    }
                    
                }

                internal bool UsesString(EmailStarString s)
                {
                    if (this.SelectionString == s)
                    {
                        return true;
                    }

                    if(ResponseEmail != null)
                    {
                    
                        ResponseEmail.UsesString(s);
                    }

                    return false;
                }
            }


            internal string GetEnum()
            {
                return EnumName;
            }

            internal EmailStarString GetTitle()
            {
                if (title == null)
                {
                    title = new EmailStarString("TITLE NOT SET", EmailStarStringType.Title);
                    parent.RegisterSystemString(title);
                }
                return title;
            }

            internal EmailStarString GetContent()
            {
                if (content == null)
                {
                    content = new EmailStarString("CONTENT NOT SET", EmailStarStringType.Content);
                    parent.RegisterSystemString(content);
                }
                return content;
            }

            internal string GetDelay()
            {
                return delay + "";
            }

            internal EmailStarEmailer GetFrom()
            {
                return from;
            }

            internal EmailStarEmailer GetTo()
            {
                return to;
            }

            internal List<EmailStarEmailResponse> GetResponses()
            {
                return Responses;
            }

            internal bool GetBlocks()
            {
                return blocks;
            }

            internal void AddNewResponse()
            {
                Responses.Add(new EmailStarEmail.EmailStarEmailResponse(parent));
            }

            internal void SetFrom(EmailStarEmailer f)
            {
                this.from = f;
            }

            internal void SetTo(EmailStarEmailer t)
            {
                this.to = t;
            }

            internal void EditTitleString(string p)
            {
                if (p == null)
                    return;

                if (title == null)
                {
                    title = new EmailStarString(p, EmailStarStringType.Title);
                    this.parent.RegisterSystemString(title);
                    return;
                }

                title.SetString(p);
            }

            internal void EditContentString(string p)
            {
                if (p == null)
                    return;

                if (content == null)
                {
                    content = new EmailStarString(p, EmailStarStringType.Content);
                    this.parent.RegisterSystemString(content);
                    return;
                }

                content.SetString(p);
            }

            internal void SetBlocks(bool p)
            {
                blocks = p;
            }

            internal void SetDelays(int i)
            {
                delay = i;
            }

            internal void SetEnum(string p)
            {
                this.EnumName = p;
            }

            internal void RemoveResponse(EmailStarEmailResponse selected)
            {
                Responses.Remove(selected);
            }

            internal void Write(System.IO.BinaryWriter b, List<EmailStarString> Strings, List<EmailStarEmailer> Emailers, List<EmailStarThread> Threads, List<EmailStarEmail> Emails)
            {
                //string EnumName = "UNSET EMAIL ENUM";
                b.Write(EnumName);

                //EmailStarString title = null;
                b.Write(Strings.IndexOf(title));

                //EmailStarString content = null;
                b.Write(Strings.IndexOf(content));

                //EmailStarEmailer from = null;
                b.Write(Emailers.IndexOf(from));

                //EmailStarEmailer to = null;
                b.Write(Emailers.IndexOf(to));

                //bool blocks = false;
                b.Write(blocks);

                //int delay = 0;
                b.Write(delay);

                //bool bExportMyEnum
                b.Write(bExportMyEnum);

                //responses
                b.Write(Responses.Count);

                foreach (EmailStarEmailResponse r in Responses)
                {
                    r.Write(b, Strings, Emails, Threads);
                }

                //private bool bHasTextureDict;
                b.Write(bHasTextureDict);


                //private string sTexturedictString;
                b.Write(sTexturedictString);

                b.Write(bHasInGameURL);
                b.Write(sInGameURL);
            }

            int ti, co, fr, tomail;
            public EmailStarEmail(EmailStarDataSet dataset,System.IO.BinaryReader b)
            {
                parent = dataset;
                Responses = new List<EmailStarEmailResponse>();

                //string EnumName = "UNSET EMAIL ENUM";
                EnumName = b.ReadString();
                //EmailStarString title = null;
                ti = b.ReadInt32();
                //EmailStarString content = null;
                co = b.ReadInt32();
                //EmailStarEmailer from = null;
                fr = b.ReadInt32();
                //EmailStarEmailer to = null;
                tomail = b.ReadInt32();
                //bool blocks = false;
                blocks = b.ReadBoolean();
                //int delay = 0;
                delay = b.ReadInt32();
                //bool bExportMyEnum
                bExportMyEnum = b.ReadBoolean();

                //responses
                int nresps = b.ReadInt32();

                for (int i = 0; i < nresps; ++i)
                {
                    Responses.Add(new EmailStarEmailResponse(this.parent,b));
                }



                //private bool bHasTextureDict;
                bHasTextureDict = b.ReadBoolean();// b.Write(bHasTextureDict);

                //private string sTexturedictString;
                sTexturedictString = b.ReadString();

                bHasInGameURL = b.ReadBoolean();
                sInGameURL = b.ReadString();

            }

            internal void Bind(List<EmailStarString> Strings, List<EmailStarEmailer> Emailers, List<EmailStarThread> Threads, List<EmailStarEmail> Emails)
            {
                //EmailStarString title = null;
                //ti = b.ReadInt32();
                if (ti > -1)
                {

                    title = Strings[ti];
                }
                else
                {
                    title = null;
                }
                //EmailStarString content = null;
                //co = b.ReadInt32();



                if (co > -1)
                {

                    content = Strings[co];
                }
                else
                {
                    content = null;
                }
                //EmailStarEmailer from = null;
                //fr = b.ReadInt32();

                if (fr > -1)
                {
                    from = Emailers[fr];
                }
                else
                {
                    from = null;
                }
                //EmailStarEmailer to = null;
                //tomail = b.ReadInt32();
                
                if (tomail > -1)
                {
                    to = Emailers[tomail];
                }
                else
                {
                    to = null;
                }

                foreach(EmailStarEmailResponse r in Responses)
                {
                    r.Bind(Strings, Emails, Threads);
                }
            }

            internal int TotalResponses()
            {
                return this.Responses.Count;
            }

            public string eslGetString()
            {
                return this.EnumName;
            }

            public void eslSetString(string s)
            {
                EnumName = s;
            }

            internal void ReplaceString(EmailStarString replace, EmailStarString with)
            {
                if(this.title == replace)
                {
                    title = with;
                }

                if(this.content == replace)
                {
                    content = with;
                }


                foreach (EmailStarEmail.EmailStarEmailResponse r in Responses)
                {
                    r.ReplaceString(replace, with);
                }
            }

            /// <summary>
            /// Replaces seconds with first
            /// </summary>
            /// <param name="with"></param>
            /// <param name="replace"></param>
            internal void ReplaceEmailer(EmailStarEmailer with, EmailStarEmailer replace)
            {
               if(this.from == replace)
               {
                   from = with;
               }
               if (this.to == replace)
               {
                   to = with;
               }
            }

            internal void OverrideParent(EmailStarDataSet emailStarDataSet)
            {
                this.parent = emailStarDataSet;
            }

            internal void ReplaceEmails(EmailStarEmail with, EmailStarEmail replace)
            {
                foreach (EmailStarEmail.EmailStarEmailResponse r in this.Responses)
                {
                    if (r.GetEmail() == replace)
                    {
                        r.SetEmail(with);
                    }
                }
            }

            internal void ReplaceThreads(EmailStarThread with, EmailStarThread replace)
            {
                foreach (EmailStarEmail.EmailStarEmailResponse r in this.Responses)
                {
                    if (r.GetFiresThread() == replace)
                    {
                        r.SetFiresThread(with);
                    }
                }
            }

            internal bool ExportMyEnum()
            {
                return bExportMyEnum;
            }

            internal void SetExports(bool p)
            {
                bExportMyEnum = p;
            }

            bool bUsed = false;


            internal void ClearUsedTag()
            {
                bUsed = false;
            }

            internal void SetUsedTag()
            {
                bUsed = true;
            }

            internal bool GetUsedTag()
            {
                return bUsed;
            }

            internal bool UsesString(EmailStarString s)
            {
                if(this.title == s)
                {
                    return true;
                }

                if (this.content == s)
                {
                    return true;
                }


                //check responses
                foreach (EmailStarEmailResponse r in this.Responses)
                {
                    if (r.UsesString(s))
                    {
                        return true;
                    }
                }
                return false;
            }

            internal bool HasTextureDictionary()
            {
                return bHasTextureDict;
            }

            internal string GetTextureDictionaryString()
            {
                return sTexturedictString;
            }

            internal void SetHasTexture(bool p)
            {
                bHasTextureDict = p;
            }

            internal void SetTextureDictName(string p)
            {
                sTexturedictString = p;
            }

            internal void SetHasLink(bool p)
            {
                bHasInGameURL = p;
            }

            internal void SetURLString(string p)
            {
                sInGameURL = p;
            }

            internal bool HasLink()
            {
                return bHasInGameURL;
            }

            internal string GetUrl()
            {
                return sInGameURL;
            }
        }

        //threads

        public class EmailStarThread : EnumStringHelper.EnumStringListable
        {
            string EnumName = "UNSET_THREAD_NAME";

            public EmailStarThread()
            {
                participants = new List<EmailStarEmailer>();
                mails = new List<EmailStarEmail>();
            }



            public override string ToString()
            {
                return EnumName;
            }

            List<EmailStarEmailer> participants = null;
            List<EmailStarEmail> mails = null;


            internal List<EmailStarEmailer> GetParticipants()
            {
                return participants;
            }

            internal List<EmailStarEmail> GetMails()
            {
                return mails;
            }

            internal void AddEmail(EmailStarEmail e)
            {
                mails.Add(e);
            }

            internal void RemoveParticipant(EmailStarEmailer emlr)
            {
                participants.Remove(emlr);
            }

            internal void RemoveEmailAtPosition(int IndexPosition)
            {
                
                //has to be index due to allowance of repetition

                mails.RemoveAt(IndexPosition);

            }

            internal void AddParticipant(EmailStarEmailer emlr)
            {
                if (participants.Contains(emlr))
                {
                    return;
                }

                participants.Add(emlr);
            }

            internal string GetEnumName()
            {
                return this.EnumName;
            }

            internal void ChangeEnumName(string p)
            {
                EnumName = p;
            }

            internal void Write(System.IO.BinaryWriter b, List<EmailStarEmail> Emails, List<EmailStarEmailer> Emailers)
            {
                //string EnumName = "UNSET_THREAD_NAME";
                b.Write(EnumName);

                //List<EmailStarEmailer> participants = null;
                b.Write(participants.Count);
                foreach (EmailStarEmailer p in participants)
                {
                    b.Write(Emailers.IndexOf(p));
                }

                //List<EmailStarEmail> mails = null;
                b.Write(mails.Count);
                foreach (EmailStarEmail m in mails)
                {
                    b.Write(Emails.IndexOf(m));
                }

            }

            List<int>pints;
            List<int>mints;

            public EmailStarThread(System.IO.BinaryReader b)
            {
                participants = new List<EmailStarEmailer>();
                mails = new List<EmailStarEmail>();

                pints = new List<int>();
                mints = new List<int>();

                //string EnumName = "UNSET_THREAD_NAME";
                EnumName = b.ReadString();

                //List<EmailStarEmailer> participants = null;
                int nparts = b.ReadInt32();

                for(int i = 0;i < nparts;++i)
                {
                    pints.Add(b.ReadInt32());

                }


                //List<EmailStarEmail> mails = null;
                int nmails = b.ReadInt32();

                for (int i = 0; i < nmails; ++i)
                {
                    mints.Add(b.ReadInt32());
                }
            }

            internal void Bind(List<EmailStarEmail> Emails, List<EmailStarEmailer> Emailers)
            {
                foreach (int p in pints)
                {
                    participants.Add(Emailers[p]);
                }
                foreach (int m in mints)
                {
                    mails.Add(Emails[m]);
                }
            }

            internal int TotalParticipants()
            {
                return participants.Count;
            }

            internal int TotalMails()
            {
                return mails.Count;
            }

            public string eslGetString()
            {
                return this.EnumName;
            }

            public void eslSetString(string s)
            {
                EnumName = s;
            }

            internal void ReplaceEmailer(EmailStarEmailer with, EmailStarEmailer replace)
            {
                if (participants.Contains(replace))
                {
                    //participants.Remove(replace);
                    //participants.Add(with);

                    participants[participants.IndexOf(replace)] = with;
                }
            }

            internal void ReplaceEmails(EmailStarEmail with, EmailStarEmail replace)
            {
                if (this.mails.Contains(replace))
                {
                    mails[mails.IndexOf(replace)] = with;
                }
            }
        }


        //////////////////////////////////////////////////////Functions
        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////

        internal List<EmailStarThread> getThreads()
        {
            return this.Threads;
        }

        internal List<EmailStarEmailer> getMailers()
        {
            return this.Emailers;
        }

        internal List<EmailStarEmail> getEmails()
        {
            return Emails;
        }

        internal void AddBlankEmailToThread(EmailStarThread selected)
        {
            if (selected == null)
            {
                return;
            }

            EmailStarEmail e = new EmailStarEmail(this);

            Emails.Add(e);
            selected.AddEmail(e);


        }

        internal void AddNewThread()
        {
            Threads.Add(new EmailStarThread());
        }

        internal void AddBlankToThread(EmailStarThread selected, EmailStarEmail etoadd)
        {
            if (selected == null || etoadd == null)
            {
                return;
            }

            Emails.Add(etoadd);
            selected.AddEmail(etoadd);
        }

        internal void AddNewMailer()
        {
            EmailStarString n = new EmailStarString("UNSET NAME", EmailStarStringType.Name);
            EmailStarString a = new EmailStarString("UNSET EMAIL ADDRESS", EmailStarStringType.Address);
            EmailStarString s = new EmailStarString("UNSET SIGNOFF", EmailStarStringType.Signoff);


            Strings.Add(n);
            Strings.Add(a);
            Strings.Add(s);

            EmailStarEmailer mlr = new EmailStarEmailer(n,a,s);


            Emailers.Add(mlr);
        }

        internal void RemoveMailer(EmailStarEmailer emlr)
        {
            Emailers.Remove(emlr);
        }

        internal void AddParticipantToThread(EmailStarEmailer emlr, EmailStarThread thrd)
        {
            
            thrd.AddParticipant(emlr);
        }

        internal void RemoveThread(EmailStarThread t)
        {
            Threads.Remove(t);
        }

        internal void RegisterSystemString(EmailStarString s)
        {
            this.Strings.Add(s);
        }
    }
}
