﻿namespace EmailStar
{
    partial class EmailersEditDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EmailerList = new System.Windows.Forms.ListBox();
            this.AddEmailer = new System.Windows.Forms.Button();
            this.RemoveEmailer = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nametext = new System.Windows.Forms.TextBox();
            this.addressstring = new System.Windows.Forms.TextBox();
            this.signoffstring = new System.Windows.Forms.TextBox();
            this.donebutton = new System.Windows.Forms.Button();
            this.emailerenumname = new System.Windows.Forms.TextBox();
            this.bHasInbox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // EmailerList
            // 
            this.EmailerList.FormattingEnabled = true;
            this.EmailerList.Location = new System.Drawing.Point(3, 28);
            this.EmailerList.Name = "EmailerList";
            this.EmailerList.Size = new System.Drawing.Size(171, 355);
            this.EmailerList.TabIndex = 0;
            this.EmailerList.SelectedIndexChanged += new System.EventHandler(this.EmailerList_SelectedIndexChanged);
            // 
            // AddEmailer
            // 
            this.AddEmailer.Location = new System.Drawing.Point(3, 386);
            this.AddEmailer.Name = "AddEmailer";
            this.AddEmailer.Size = new System.Drawing.Size(75, 23);
            this.AddEmailer.TabIndex = 1;
            this.AddEmailer.Text = "+";
            this.AddEmailer.UseVisualStyleBackColor = true;
            this.AddEmailer.Click += new System.EventHandler(this.AddEmailer_Click);
            // 
            // RemoveEmailer
            // 
            this.RemoveEmailer.Location = new System.Drawing.Point(148, 386);
            this.RemoveEmailer.Name = "RemoveEmailer";
            this.RemoveEmailer.Size = new System.Drawing.Size(26, 23);
            this.RemoveEmailer.TabIndex = 2;
            this.RemoveEmailer.Text = "-";
            this.RemoveEmailer.UseVisualStyleBackColor = true;
            this.RemoveEmailer.Click += new System.EventHandler(this.RemoveEmailer_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(180, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(177, 144);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(180, 250);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Signoff";
            // 
            // nametext
            // 
            this.nametext.Location = new System.Drawing.Point(183, 69);
            this.nametext.Name = "nametext";
            this.nametext.Size = new System.Drawing.Size(182, 20);
            this.nametext.TabIndex = 7;
            this.nametext.TextChanged += new System.EventHandler(this.nametext_TextChanged);
            // 
            // addressstring
            // 
            this.addressstring.Location = new System.Drawing.Point(180, 160);
            this.addressstring.Name = "addressstring";
            this.addressstring.Size = new System.Drawing.Size(185, 20);
            this.addressstring.TabIndex = 9;
            this.addressstring.TextChanged += new System.EventHandler(this.addressstring_TextChanged);
            // 
            // signoffstring
            // 
            this.signoffstring.Location = new System.Drawing.Point(183, 266);
            this.signoffstring.Name = "signoffstring";
            this.signoffstring.Size = new System.Drawing.Size(182, 20);
            this.signoffstring.TabIndex = 11;
            this.signoffstring.TextChanged += new System.EventHandler(this.signoffstring_TextChanged);
            // 
            // donebutton
            // 
            this.donebutton.Location = new System.Drawing.Point(330, 386);
            this.donebutton.Name = "donebutton";
            this.donebutton.Size = new System.Drawing.Size(75, 23);
            this.donebutton.TabIndex = 15;
            this.donebutton.Text = "Done";
            this.donebutton.UseVisualStyleBackColor = true;
            this.donebutton.Click += new System.EventHandler(this.donebutton_Click);
            // 
            // emailerenumname
            // 
            this.emailerenumname.Location = new System.Drawing.Point(3, 2);
            this.emailerenumname.Name = "emailerenumname";
            this.emailerenumname.Size = new System.Drawing.Size(171, 20);
            this.emailerenumname.TabIndex = 16;
            this.emailerenumname.TextChanged += new System.EventHandler(this.emailerenumname_TextChanged);
            // 
            // bHasInbox
            // 
            this.bHasInbox.AutoSize = true;
            this.bHasInbox.Location = new System.Drawing.Point(183, 4);
            this.bHasInbox.Name = "bHasInbox";
            this.bHasInbox.Size = new System.Drawing.Size(74, 17);
            this.bHasInbox.TabIndex = 17;
            this.bHasInbox.Text = "Has Inbox";
            this.bHasInbox.UseVisualStyleBackColor = true;
            this.bHasInbox.CheckedChanged += new System.EventHandler(this.bHasInbox_CheckedChanged);
            // 
            // EmailersEditDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 412);
            this.Controls.Add(this.bHasInbox);
            this.Controls.Add(this.emailerenumname);
            this.Controls.Add(this.donebutton);
            this.Controls.Add(this.signoffstring);
            this.Controls.Add(this.addressstring);
            this.Controls.Add(this.nametext);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RemoveEmailer);
            this.Controls.Add(this.AddEmailer);
            this.Controls.Add(this.EmailerList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EmailersEditDialog";
            this.Text = "Editing Emailers";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox EmailerList;
        private System.Windows.Forms.Button AddEmailer;
        private System.Windows.Forms.Button RemoveEmailer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox nametext;
        private System.Windows.Forms.TextBox addressstring;
        private System.Windows.Forms.TextBox signoffstring;
        private System.Windows.Forms.Button donebutton;
        private System.Windows.Forms.TextBox emailerenumname;
        private System.Windows.Forms.CheckBox bHasInbox;
    }
}