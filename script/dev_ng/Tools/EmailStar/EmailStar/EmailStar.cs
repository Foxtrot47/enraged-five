﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmailStar
{
    public partial class EmailStar : Form
    {
        private EmailStarDataSet ds;

        public EmailStar(EmailStarDataSet dataset)
        {
            InitializeComponent();
            this.ds = dataset;

            RefreshAllListings();
        }

        private void RefreshAllListings()
        {
            //selectedemailgroup.Controls
            //foreach()
            FormHelper.ResetFields(this);


            //fill out the thread list

            List<EmailStarDataSet.EmailStarThread> tlist = ds.getThreads();

            //threadlist.Items
            foreach (EmailStarDataSet.EmailStarThread t in tlist)
            {
                threadlist.Items.Add(t);
                threadtofire.Items.Add(t);
            }



            

            

        }





        private void addemailtothread_Click(object sender, EventArgs e)
        {

            if (threadlist.SelectedItem == null)
            {
                return;
            }


            EmailStarDataSet.EmailStarThread selected = (EmailStarDataSet.EmailStarThread)threadlist.SelectedItem;
            EmailPickDialog epd = new EmailPickDialog(ds, selected);

           

            DialogResult dr = epd.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.Cancel)
            {
                return;
            }
      

            //refresh the email listings
            emailsinthread.Items.Clear();

            object ob = emailsinthread.SelectedItem;

            List<EmailStarDataSet.EmailStarEmail> emstrl = selected.GetMails();

            
            foreach (EmailStarDataSet.EmailStarEmail eml in emstrl)
            {
                emailsinthread.Items.Add(eml);
            }

            emailsinthread.SelectedItem = ob;

        }

        private void emailerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmailersEditDialog eed = new EmailersEditDialog(ds);



            DialogResult dr = eed.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.Cancel)
            {
                return;
            }

            //refresh the emailer listings

            List<EmailStarDataSet.EmailStarEmailer> mlrs = ds.getMailers();

            //pickparticipant
            pickparticipant.Items.Clear();
            foreach (EmailStarDataSet.EmailStarEmailer mailer in mlrs)
            {
                pickparticipant.Items.Add(mailer);
            }

            

        }

        private void addthread_Click(object sender, EventArgs e)
        {
            //if
            ds.AddNewThread();
            RefreshAllListings();
        }

        private void threadlist_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (threadlist.SelectedItem == null)
            {
                return;
            }


            EmailStarDataSet.EmailStarThread selected = (EmailStarDataSet.EmailStarThread)threadlist.SelectedItem;


            threadname.Text = selected.GetEnumName();

            //thread has been picked, fill out emails list and participantlist
            emailsinthread.SelectedItem = null;
            emailsinthread.Items.Clear();

            participantlist.SelectedItem = null;
            participantlist.Items.Clear();

            //grab the lists

            List<EmailStarDataSet.EmailStarEmailer> parts = selected.GetParticipants();
            List<EmailStarDataSet.EmailStarEmail> mails = selected.GetMails();
            

            //fill them out

            foreach(EmailStarDataSet.EmailStarEmailer p in parts)
            {
                participantlist.Items.Add(p);
            }

            foreach(EmailStarDataSet.EmailStarEmail m in mails)
            {
                emailsinthread.Items.Add(m);
            }



            
            List<EmailStarDataSet.EmailStarEmailer> allmailers = ds.getMailers();
            pickparticipant.Items.Clear();

            foreach(EmailStarDataSet.EmailStarEmailer mlr in allmailers)
            {
                pickparticipant.Items.Add(mlr);
            }

        }

        private void emailsinthread_SelectedIndexChanged(object sender, EventArgs e)
        {
            //email is selected, fill out email fields

            if (emailsinthread.SelectedItem == null)
            {
                return;
            }

            EmailStarDataSet.EmailStarEmail selected = (EmailStarDataSet.EmailStarEmail)emailsinthread.SelectedItem;

            ////texts
            //scriptemailenumoverride
            //emailtitle
            //emailcontent
            //emaildelay

            scriptemailenumoverride.Text = selected.GetEnum();
            emailtitle.Text = selected.GetTitle().ToString();
            emailcontent.Text = selected.GetContent().ToString();
            emaildelay.Text = selected.GetDelay();
            ExportEmailEnum.Checked = selected.ExportMyEnum();

            hasTextureDict.Checked = selected.HasTextureDictionary();
            textureDictname.Text = selected.GetTextureDictionaryString();


            haslink.Checked = selected.HasLink();
            weblink.Text = selected.GetUrl();
            ////lists
            //fromemail
            fromemail.Items.Clear();

            List<EmailStarDataSet.EmailStarEmailer> mlrs = ds.getMailers();

            foreach (EmailStarDataSet.EmailStarEmailer m in mlrs)
            {
                fromemail.Items.Add(m);
            }

            fromemail.SelectedItem = (EmailStarDataSet.EmailStarEmailer)selected.GetFrom();




            //toemail
            toemail.Items.Clear();


            foreach (EmailStarDataSet.EmailStarEmailer m in mlrs)
            {
                toemail.Items.Add(m);
            }


            toemail.SelectedItem = (EmailStarDataSet.EmailStarEmailer)selected.GetTo();




            //emailresponses
            emailresponses.Items.Clear();



            List<EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse> ersp = selected.GetResponses();

            foreach (EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse esr in ersp)
            {
                emailresponses.Items.Add(esr);
            }


            ////bools
            //emailblocks
            emailblocks.Checked = selected.GetBlocks();




        }

        private void emailresponses_SelectedIndexChanged(object sender, EventArgs e)
        {
            //response selected, fill out the response settings

            EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse selected = (EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse)emailresponses.SelectedItem;

            if(selected == null)
            {
                return;
            }

            ///text
            //responsestring
            //responseenumname
            responsestring.Text = selected.GetSelectionString().ToString();
            responseenumname.Text = selected.GetEnumString();

            ///list 
            //threadtofire
            //addemail

            threadtofire.Items.Clear();
            addemail.Items.Clear();

            List<EmailStarDataSet.EmailStarThread> threads = ds.getThreads();

            foreach (EmailStarDataSet.EmailStarThread t in threads)
            {
                threadtofire.Items.Add(t);
            }


            List<EmailStarDataSet.EmailStarEmail> emails = ds.getEmails();

            foreach(EmailStarDataSet.EmailStarEmail em in emails)
            {
                addemail.Items.Add(em);
            }


            addemail.SelectedItem = selected.GetEmail();

            EmailStarDataSet.EmailStarThread tofire = selected.GetFiresThread();
            
            //responsefirestthread(check) -> grey threadtofire(list) if set
            if (tofire == null)
            {
                responsefirestthread.Checked = false;
                threadtofire.Enabled = false;
            }
            else
            {
                threadtofire.Enabled = true;
                responsefirestthread.Checked = true;

                threadtofire.SelectedItem = tofire;
            }

            


            ///check 
            //

            responseendsthread.Checked = selected.GetEndsThread();
            autofirecheck.Checked = selected.GetAutoFires();
            



        }

        private void addparticipant_Click(object sender, EventArgs e)
        {
            if (pickparticipant.SelectedItem == null || threadlist.SelectedItem == null) 
            {
                return;
            }

            EmailStarDataSet.EmailStarEmailer emlr = (EmailStarDataSet.EmailStarEmailer)pickparticipant.SelectedItem;
            EmailStarDataSet.EmailStarThread thrd = (EmailStarDataSet.EmailStarThread)threadlist.SelectedItem;

            ds.AddParticipantToThread(emlr, thrd);


            participantlist.SelectedItem = null;
            participantlist.Items.Clear();

            //grab the lists

            List<EmailStarDataSet.EmailStarEmailer> parts = thrd.GetParticipants();

            //fill them out

            foreach (EmailStarDataSet.EmailStarEmailer p in parts)
            {
                participantlist.Items.Add(p);
            }

        }

        private void removethread_Click(object sender, EventArgs e)
        {
            if (threadlist.SelectedItem == null)
            {
                return;
            }

            EmailStarDataSet.EmailStarThread t = (EmailStarDataSet.EmailStarThread)threadlist.SelectedItem;

            ds.RemoveThread(t);
            RefreshAllListings();
        }

        private void removeparticipant_Click(object sender, EventArgs e)
        {
            if (participantlist.SelectedItem == null || threadlist.SelectedItem == null)
            {
                return;
            }

            EmailStarDataSet.EmailStarEmailer emlr = (EmailStarDataSet.EmailStarEmailer)participantlist.SelectedItem;
            EmailStarDataSet.EmailStarThread thrd = (EmailStarDataSet.EmailStarThread)threadlist.SelectedItem;

            thrd.RemoveParticipant(emlr);


            //grab the lists

            List<EmailStarDataSet.EmailStarEmailer> parts = thrd.GetParticipants();

            //fill them out
            participantlist.Items.Clear();
            foreach (EmailStarDataSet.EmailStarEmailer p in parts)
            {
                participantlist.Items.Add(p);
            }
        }

        private void removeemailfromthread_Click(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null)
            {
                return;
            }
            //EmailStarDataSet.EmailStarEmail em = (EmailStarDataSet.EmailStarEmail)emailsinthread.SelectedItem;
            int IndexPosition = emailsinthread.SelectedIndex;
            EmailStarDataSet.EmailStarThread thrd = (EmailStarDataSet.EmailStarThread)threadlist.SelectedItem;



            thrd.RemoveEmailAtPosition(IndexPosition);

            emailsinthread.Items.Clear();
            List<EmailStarDataSet.EmailStarEmail> emstrl = thrd.GetMails();


            foreach (EmailStarDataSet.EmailStarEmail eml in emstrl)
            {
                emailsinthread.Items.Add(eml);
            }

        }

        private void addresponse_Click(object sender, EventArgs e)
        {

            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null)
            {
                return;
            }


            EmailStarDataSet.EmailStarEmail selected = (EmailStarDataSet.EmailStarEmail)emailsinthread.SelectedItem;


            selected.AddNewResponse();
            //emailresponses
            emailresponses.Items.Clear();



            List<EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse> ersp = selected.GetResponses();

            foreach (EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse esr in ersp)
            {
                emailresponses.Items.Add(esr);
            }
        }

        private void wtfIsThisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://rsgediwiki1/wiki/index.php/Emailstar");
        }

        private void threadname_TextChanged(object sender, EventArgs e)
        {
            if (threadlist.SelectedItem == null)
            {
                return;
            }

            EmailStarDataSet.EmailStarThread thrd = (EmailStarDataSet.EmailStarThread)threadlist.SelectedItem;

            thrd.ChangeEnumName(threadname.Text);

            //force listbox to check the string again
            object o = threadlist.SelectedItem;
            threadlist.Items[threadlist.SelectedIndex] = o;
            threadlist.SelectedItem = o;
        }

        private void scriptemailenumoverride_TextChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null)
            {
                return;
            }
            EmailStarDataSet.EmailStarEmail selected = (EmailStarDataSet.EmailStarEmail)emailsinthread.SelectedItem;

            selected.SetEnum(scriptemailenumoverride.Text);

            //force listbox to update the string
            object o = emailsinthread.SelectedItem;
            emailsinthread.Items[emailsinthread.SelectedIndex] = o;
            emailsinthread.SelectedItem = o;
        }

        private void fromemail_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null || fromemail.SelectedItem == null)
            {
                return;
            }
            EmailStarDataSet.EmailStarEmail selected = (EmailStarDataSet.EmailStarEmail)emailsinthread.SelectedItem;
            EmailStarDataSet.EmailStarEmailer from = (EmailStarDataSet.EmailStarEmailer)fromemail.SelectedItem;


            selected.SetFrom(from);


        }

        private void toemail_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null || toemail.SelectedItem == null)
            {
                return;
            }
            EmailStarDataSet.EmailStarEmail selected = (EmailStarDataSet.EmailStarEmail)emailsinthread.SelectedItem;
            EmailStarDataSet.EmailStarEmailer to = (EmailStarDataSet.EmailStarEmailer)toemail.SelectedItem;


            selected.SetTo(to);
        }

        private void emailtitle_TextChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null)
            {
                return;
            }
            EmailStarDataSet.EmailStarEmail selected = (EmailStarDataSet.EmailStarEmail)emailsinthread.SelectedItem;


            selected.EditTitleString(emailtitle.Text);
        }

        private void emailcontent_TextChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null)
            {
                return;
            }
            EmailStarDataSet.EmailStarEmail selected = (EmailStarDataSet.EmailStarEmail)emailsinthread.SelectedItem;

            selected.EditContentString(emailcontent.Text);
        }

        private void emailblocks_CheckedChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null)
            {
                return;
            }
            EmailStarDataSet.EmailStarEmail selected = (EmailStarDataSet.EmailStarEmail)emailsinthread.SelectedItem;

            selected.SetBlocks(emailblocks.Checked);
        }
        private void ExportEmailEnum_CheckedChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null)
            {
                return;
            }
            EmailStarDataSet.EmailStarEmail selected = (EmailStarDataSet.EmailStarEmail)emailsinthread.SelectedItem;

            selected.SetExports(ExportEmailEnum.Checked);
        }

        private void emaildelay_TextChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null)
            {
                return;
            }
            EmailStarDataSet.EmailStarEmail selected = (EmailStarDataSet.EmailStarEmail)emailsinthread.SelectedItem;


            //check for valid int
            try
            {
                int i = int.Parse(emaildelay.Text);
                selected.SetDelays(i);
                emaildelay.BackColor = Color.White;
                return;
            }
            catch
            {
                emaildelay.BackColor = Color.Red;
                return;
            }



            
        }

        private void removeresponse_Click(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null || emailresponses.SelectedItem == null)
            {
                return;
            }


            EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse selected = (EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse)emailresponses.SelectedItem;
            EmailStarDataSet.EmailStarEmail email = (EmailStarDataSet.EmailStarEmail)emailsinthread.SelectedItem;

            email.RemoveResponse(selected);


            emailresponses.Items.Clear();
            List<EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse> l = email.GetResponses();

            foreach(EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse r in l)
            {
                emailresponses.Items.Add(r);
            }


        }

        private void responseenumname_TextChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null || emailresponses.SelectedItem == null)
            {
                return;
            }


            EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse selected = (EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse)emailresponses.SelectedItem;
            //EmailStarDataSet.EmailStarEmail email = (EmailStarDataSet.EmailStarEmail)emailsinthread.SelectedItem;

            selected.SetEnum(responseenumname.Text);



            //force listbox to check the string again
            object o = emailresponses.SelectedItem;
            emailresponses.Items[emailresponses.SelectedIndex] = o;
            emailresponses.SelectedItem = o;
        }

        private void responsestring_TextChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null || emailresponses.SelectedItem == null)
            {
                return;
            }


            EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse selected = (EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse)emailresponses.SelectedItem;

            selected.SetResponseString(responsestring.Text);
        }



        private void addemail_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null || emailresponses.SelectedItem == null)
            {
                return;
            }


            EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse selected = (EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse)emailresponses.SelectedItem;
            EmailStarDataSet.EmailStarEmail selemail = (EmailStarDataSet.EmailStarEmail)addemail.SelectedItem;

            selected.SetEmail(selemail);

        }

        private void responseendsthread_CheckedChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null || emailresponses.SelectedItem == null)
            {
                return;
            }


            EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse selected = (EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse)emailresponses.SelectedItem;

            selected.SetResponseEndsThread(responseendsthread.Checked);
        }

        private void responsefirestthread_CheckedChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null || emailresponses.SelectedItem == null)
            {
                return;
            }
            EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse selected = (EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse)emailresponses.SelectedItem;

            if (responsefirestthread.Checked)
            {
                threadtofire.Enabled = true;
            }
            else
            {
                threadtofire.Enabled = false;
                selected.ClearThreadToFire();
            }

        }

        private void threadtofire_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null || emailresponses.SelectedItem == null || threadtofire.SelectedItem == null)
            {
                return;
            }
            EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse selected = (EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse)emailresponses.SelectedItem;
            EmailStarDataSet.EmailStarThread fires = (EmailStarDataSet.EmailStarThread)threadtofire.SelectedItem;

            selected.SetFiresThread(fires);
        }


        private void saveDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDLG.Filter = "Email Star database (*.est)|*.est";
            DialogResult res = saveFileDLG.ShowDialog();
            if (res == DialogResult.OK || res == DialogResult.Yes)
            {
                ds.PurgeUnusedMails();
                //throw new NotImplementedException();
                    //saveFileDLG.FileName
                ds.SaveDataFile(saveFileDLG.FileName);
            }

            
        }

        private void loadDatasetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDLG.Filter = "Email Star database (*.est)|*.est";
            DialogResult res = openFileDLG.ShowDialog();
            if (res == DialogResult.OK || res == DialogResult.Yes)
            {
                //close all open forms
                foreach (Form f in this.MdiChildren)
                {
                    f.Close();
                }

                    //openFileDLG.FileName
                ds.LoadDataFile(openFileDLG.FileName);
                RefreshAllListings();
            }
        }

        private void mergeDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDLG.Filter = "Email Star database (*.est)|*.est";
            DialogResult res = openFileDLG.ShowDialog();
            if (res == DialogResult.OK || res == DialogResult.Yes)
            {
                //close all open forms
                foreach (Form f in this.MdiChildren)
                {
                    f.Close();
                }

                //openFileDLG.FileName
                EmailStarDataSet merge = new EmailStarDataSet();
                merge.LoadDataFile(openFileDLG.FileName);
                ds.Merge(merge);
                RefreshAllListings();
            }
        }

        private void toFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult res = exportFolderDlg.ShowDialog();
            //ds

            if (res == DialogResult.OK || res == DialogResult.Yes)
            {
                ds.PurgeUnusedMails();
                ds.CorrectEnums();
                ds.RemoveUnnusedStrings();
                ds.ExportTo(exportFolderDlg.SelectedPath);
            }
        }

        private void toXgta5ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            //ds
            ds.PurgeUnusedMails();
            ds.CorrectEnums();
            ds.ExportTo(null);
        }


        private void autofirecheck_CheckedChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null || emailresponses.SelectedItem == null)
            {
                return;
            }


            EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse selected = (EmailStarDataSet.EmailStarEmail.EmailStarEmailResponse)emailresponses.SelectedItem;

            selected.SetAutoFires(autofirecheck.Checked);
        }

        private void enumificationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ds.CorrectEnums();

            RefreshAllListings();
        }

        private void concatenateMatchingStringsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ds.ConcatenateMatchingStringReferences();
        }

        private void exportHeadersToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void purgeUnnusedStringsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ds.RemoveUnnusedStrings();
        }

        private void hasTextureDict_CheckedChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null)
            {
                return;
            }
            EmailStarDataSet.EmailStarEmail selected = (EmailStarDataSet.EmailStarEmail)emailsinthread.SelectedItem;


            selected.SetHasTexture(hasTextureDict.Checked);
        }

        private void textureDictname_TextChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null)
            {
                return;
            }
            EmailStarDataSet.EmailStarEmail selected = (EmailStarDataSet.EmailStarEmail)emailsinthread.SelectedItem;

            selected.SetTextureDictName(textureDictname.Text);
        }

        private void haslink_CheckedChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null)
            {
                return;
            }
            EmailStarDataSet.EmailStarEmail selected = (EmailStarDataSet.EmailStarEmail)emailsinthread.SelectedItem;


            selected.SetHasLink(haslink.Checked);
        }

        private void weblink_TextChanged(object sender, EventArgs e)
        {
            if (emailsinthread.SelectedItem == null || threadlist.SelectedItem == null)
            {
                return;
            }
            EmailStarDataSet.EmailStarEmail selected = (EmailStarDataSet.EmailStarEmail)emailsinthread.SelectedItem;

            selected.SetURLString(weblink.Text);
        }









    }
}
