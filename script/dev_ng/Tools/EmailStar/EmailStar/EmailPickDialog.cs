﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmailStar
{
    public partial class EmailPickDialog : Form
    {
        private EmailStarDataSet ds;

        private EmailStarDataSet.EmailStarThread selected;

        
        public EmailPickDialog(EmailStarDataSet dsin, EmailStarDataSet.EmailStarThread selected)
        {

            InitializeComponent();
            this.ds = dsin;

            this.selected = selected;

            pickemail.Items.Clear();


            List<EmailStarDataSet.EmailStarEmail> gmails = ds.getEmails();

            foreach (EmailStarDataSet.EmailStarEmail em in gmails)
            {
                pickemail.Items.Add(em);
            }


        }

        private void blankmail_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;


            ds.AddBlankEmailToThread(selected);
            
        }

        private void addexsisting_Click(object sender, EventArgs e)
        {
            

            if (pickemail.SelectedItem == null)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                this.Close();
            }

            EmailStarDataSet.EmailStarEmail etoadd = (EmailStarDataSet.EmailStarEmail) pickemail.SelectedItem;

            ds.AddBlankToThread(selected, etoadd);


            //add the email reference

            this.DialogResult = System.Windows.Forms.DialogResult.Yes;
        }


    }
}
