﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmailStar
{
    class EnumStringHelper
    {
        public interface EnumStringListable
        {
            string eslGetString();
            void eslSetString(string s);
        }

        List<EnumStringListable> sl;
        

        public EnumStringHelper()
        {
            sl = new List<EnumStringListable>();
        }

        public void RegisterEnumerable(EnumStringListable l)
        {
            sl.Add(l);
        }

        public void ProcessEnumStrings()
        {
            if (sl.Count < 1)
            {
                return;
            }

            int [] duplicates = new int[sl.Count];

            //remove illigal chars

            //capsify


            foreach (EnumStringListable l in sl)
            {
                string s = l.eslGetString();

                s = s.ToUpper();
                s= s.Replace("]", "");
                s = s.Replace("[", "");
                s = s.Replace("'", "");
                s = s.Replace("@", "");
                s = s.Replace(">", "");
                s = s.Replace("<", "");
                s = s.Replace("?", "");
                s = s.Replace("¦", "");
                s = s.Replace("¬", "");
                s = s.Replace("`", "");
                s = s.Replace("$", "");
                s = s.Replace("%", "");
                s = s.Replace("^", "");
                s = s.Replace("&", "");
                s = s.Replace("*", "");
                s = s.Replace("\"", "");
                s = s.Replace("!", "");
                s = s.Replace("|", "");
                s = s.Replace("\\", "");
                s = s.Replace("/", "");
                s = s.Replace("=", "");
                s = s.Replace("+", "");
                s = s.Replace(")", "");
                s = s.Replace("(", "");
                s = s.Replace("}", "");
                s = s.Replace("{", "");
                s = s.Replace("#", "");
                s = s.Replace("~", "");
                s = s.Replace(":", "");
                s = s.Replace(";", "");
                s = s.Replace("-", "_");
                s = s.Replace(".", "");
                s = s.Replace(",", "");
                s = s.Replace(" ", "_");

                l.eslSetString(s);
            }

            //look for duplicates

            for (int i = 0; i < sl.Count-1; ++i)
            {
                
                for (int j = i+1; j < sl.Count; ++j)
                {
                    if (i != j)
                    {
                        if (string.Compare(sl[i].eslGetString(), sl[j].eslGetString()) == 0)
                        {
                            ++duplicates[j];
                        }
                    }
                }
            }

            for (int i = 0; i < sl.Count; ++i)
            {
                if (duplicates[i] > 0)
                {
                    sl[i].eslSetString("" + sl[i].eslGetString() + duplicates[i]);
                }
            }



        }
    }
}
