﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;


    namespace EmailStar
    {
        public class FormHelper
        {
            public static void ResetFields(Control form)
		{
			foreach (Control ctrl in form.Controls)
			{
				if (ctrl.Controls.Count > 0)
					ResetFields(ctrl);
				Reset(ctrl);
			}
		}
            public static void Reset(Control ctrl)
            {
                if (ctrl is TextBox)
                {
                    TextBox tb = (TextBox)ctrl;
                    if (tb != null)
                    {
                        tb.Clear();
                    }
                }
                else if (ctrl is ComboBox)
                {
                    ComboBox dd = (ComboBox)ctrl;
                    if (dd != null)
                    {
                        dd.Items.Clear();
                        dd.SelectedIndex = -1;
                    }
                }else if(ctrl is ListBox)
                {
                    ListBox lb = (ListBox)ctrl;

                    if (lb != null)
                    {
                        lb.SelectedItem = null;
                        lb.Items.Clear();
                    }
                }
            }
        }
    }
