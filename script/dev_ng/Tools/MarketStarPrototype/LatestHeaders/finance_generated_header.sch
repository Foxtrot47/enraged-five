///Automatically generated header.

USING "stats_enums.sch"

/// List of company enums.

	ENUM BAWSAQ_COMPANIES
		BS_CO_BRU,// Brute// Online
		BS_CO_VAP,// Vapid// Online
		BS_CO_MAI,// Maibatsu// Online
		BS_CO_PFI,// Pfister// Online
		BS_CO_HVY,// HVY// Online
		BS_CO_MTL,// MTL// Online
		BS_CO_GUN1,// ArmsManufacturerA// Online
		BS_CO_GUN2,// ArmsManufacturerB// Online
		BS_CO_GUN3,// ArmsManufacturerC// Online
		BS_CO_SPK,// Sprunk// Online
		BS_CO_CHI,// ChihuahuaHotdogs// Online
		BS_CO_WTR,// Water// Online
		BS_CO_PIS,// Piswasser// Online
		BS_CO_DAI,// DailyGlobe// Online
		BS_CO_MET,// METV// Online
		BS_CO_WZL,// Weazel// Online
		BS_CO_CNT,// CNT// Online
		BS_CO_LTA,// LibertyTransportAuthority// Online
		BS_CO_VIG,// VigInsurance// Online
		BS_CO_CRE,// Crevis// Online
		BS_CO_BDG,// Badger// Online
		BS_CO_WIZ,// Wiz// Online
		BS_CO_TNK,// Tinkel// Online
		BS_CO_FRT,// iFruit// Online
		BS_CO_BTR,// Bittersweet// Online
		BS_CO_RON,// RonOil// Online
		BS_CO_LBL,// Libel// Online
		BS_CO_BLE,// Bleeter// Online
		BS_CO_LOM,// BankofLiberty_LOM// Online
		BS_CO_BNC,// BankC// Online
		BS_CO_TXI,// Taxi// Online
		BS_CO_GLS,// GoldbergLignerShyster// Online
		BS_CO_LYB,// LawyerB// Online
		BS_CO_LYC,// LawyerC// Online
		BS_CO_PRT,// Pirate// Online
		BS_CO_CHA,// ClothingCompanyA// Online
		BS_CO_CHB,// ClothingCompanyB// Online
		BS_CO_RLE,// RealEstateCompany// Online
		BS_CO_PET,// Petfoodcompany// Online
		BS_CO_ECL,// eCola// Offline
		BS_CO_CDY,// Candybox// Offline
		BS_CO_BGR,// BurgerShot// Offline
		BS_CO_CLK,// CluckingBell// Offline
		BS_CO_GLY,// GloryHole// Offline
		BS_CO_BTE,// Bite// Offline
		BS_CO_BEN,// BeanMachine// Offline
		BS_CO_BEO,// BEOFinance// Offline
		BS_CO_FLC,// Fleeca// Offline
		BS_CO_PRO,// Prolaps// Offline
		BS_CO_XER,// Xero// Offline
		BS_CO_UNI,// VanillaUnicorn// Offline
		BS_CO_KRP,// Krapea// Offline
		BS_CO_LFB,// Lifeblurb// Offline
		BS_CO_GBL,// Gambling// Offline
		BS_CO_FUS,// FlyUS// Offline
		BS_CO_ADI,// Adios// Offline
		BS_CO_PED,// Pensioncompany// Offline
		BS_CO_REC,// Recordlabel// Offline
		BS_CO_CHC,// ClothingCompanyC// Offline
		BS_CO_CHD,// ClothingCompanyD// Offline
		BS_CO_CHE,// ClothingCompanyE// Offline
		BS_CO_CHF,// ClothingCompanyF// Offline
		BS_CO_FLM,// FilmCompany// Offline
		BS_CO_PHM,// PharmaceuticalCompany// Offline
		BS_CO_GOP,// GoPostal// Offline
		BS_CO_BAN,// Banner// Offline
		BS_CO_VON,// VonCrastenburg// Offline
		BS_CO_WTA,// WatchManufactuerA// Offline
		BS_CO_WTB,// WatchManufactuerB// Offline
		BS_CO_FGA,// CigarettesA// Offline
		BS_CO_FGB,// CigarettesB// Offline
		BS_CO_MAX,// MaxRena// Offline
		BS_CO_VEN,// Veneer// Offline
		BS_CO_RLC,// RealEstateC// Offline
		BS_CO_RLB,// RealEstateB// Offline
		BS_CO_GAS,// GastroBans// Offline
		BS_CO_GRU,// GruppeSechs// Offline
		BS_CO_PMP,// Pump&Run// Offline
		BS_CO_MHO,// Mhollis// Offline
		BS_CO_FCD,// Facade// Online
		BS_CO_CTM,// CustomAutos// Online
		BS_CO_IOU,// MazeBank-IOUBank// Online
		BS_CO_TOTAL_LISTINGS
	ENDENUM

///List of modifier enums.

	ENUM BSMF_TYPES
		BSMF_SITEVISIT_FOR_BRU,// WebSiteVisits : used by Brute
		BSMF_SITEVISIT_FOR_VAP,// WebSiteVisits : used by Vapid
		BSMF_SITEVISIT_FOR_MAI,// WebSiteVisits : used by Maibatsu
		BSMF_SITEVISIT_FOR_PFI,// WebSiteVisits : used by Pfister
		BSMF_SITEVISIT_FOR_HVY,// WebSiteVisits : used by HVY
		BSMF_SITEVISIT_FOR_MTL,// WebSiteVisits : used by MTL
		BSMF_SITEVISIT_FOR_GUN1,// WebSiteVisits : used by ArmsManufacturerA
		BSMF_SITEVISIT_FOR_GUN2,// WebSiteVisits : used by ArmsManufacturerB
		BSMF_SITEVISIT_FOR_GUN3,// WebSiteVisits : used by ArmsManufacturerC
		BSMF_SITEVISIT_FOR_SPK,// WebSiteVisits : used by Sprunk
		BSMF_SITEVISIT_FOR_CHI,// WebSiteVisits : used by ChihuahuaHotdogs
		BSMF_SITEVISIT_FOR_WTR,// WebSiteVisits : used by Water
		BSMF_SITEVISIT_FOR_PIS,// WebSiteVisits : used by Piswasser
		BSMF_SITEVISIT_FOR_DAI,// WebSiteVisits : used by DailyGlobe
		BSMF_SITEVISIT_FOR_MET,// WebSiteVisits : used by METV
		BSMF_SITEVISIT_FOR_WZL,// WebSiteVisits : used by Weazel
		BSMF_SITEVISIT_FOR_CNT,// WebSiteVisits : used by CNT
		BSMF_SITEVISIT_FOR_LTA,// WebSiteVisits : used by LibertyTransportAuthority
		BSMF_SITEVISIT_FOR_VIG,// WebSiteVisits : used by VigInsurance
		BSMF_SITEVISIT_FOR_CRE,// WebSiteVisits : used by Crevis
		BSMF_SITEVISIT_FOR_BDG,// WebSiteVisits : used by Badger
		BSMF_SITEVISIT_FOR_WIZ,// WebSiteVisits : used by Wiz
		BSMF_SITEVISIT_FOR_TNK,// WebSiteVisits : used by Tinkel
		BSMF_SITEVISIT_FOR_FRT,// WebSiteVisits : used by iFruit
		BSMF_SITEVISIT_FOR_BTR,// WebSiteVisits : used by Bittersweet
		BSMF_SITEVISIT_FOR_RON,// WebSiteVisits : used by RonOil
		BSMF_SITEVISIT_FOR_LBL,// WebSiteVisits : used by Libel
		BSMF_SITEVISIT_FOR_BLE,// WebSiteVisits : used by Bleeter
		BSMF_SITEVISIT_FOR_LOM,// WebSiteVisits : used by BankofLiberty_LOM
		BSMF_SITEVISIT_FOR_BNC,// WebSiteVisits : used by BankC
		BSMF_SITEVISIT_FOR_TXI,// WebSiteVisits : used by Taxi
		BSMF_SITEVISIT_FOR_GLS,// WebSiteVisits : used by GoldbergLignerShyster
		BSMF_SITEVISIT_FOR_LYB,// WebSiteVisits : used by LawyerB
		BSMF_SITEVISIT_FOR_LYC,// WebSiteVisits : used by LawyerC
		BSMF_SITEVISIT_FOR_PRT,// WebSiteVisits : used by Pirate
		BSMF_SITEVISIT_FOR_CHA,// WebSiteVisits : used by ClothingCompanyA
		BSMF_SITEVISIT_FOR_CHB,// WebSiteVisits : used by ClothingCompanyB
		BSMF_SITEVISIT_FOR_RLE,// WebSiteVisits : used by RealEstateCompany
		BSMF_SITEVISIT_FOR_PET,// WebSiteVisits : used by Petfoodcompany
		BSMF_SITEVISIT_FOR_ECL,// WebSiteVisits : used by eCola
		BSMF_SITEVISIT_FOR_CDY,// WebSiteVisits : used by Candybox
		BSMF_SITEVISIT_FOR_BGR,// WebSiteVisits : used by BurgerShot
		BSMF_SITEVISIT_FOR_CLK,// WebSiteVisits : used by CluckingBell
		BSMF_SITEVISIT_FOR_GLY,// WebSiteVisits : used by GloryHole
		BSMF_SITEVISIT_FOR_BTE,// WebSiteVisits : used by Bite
		BSMF_SITEVISIT_FOR_BEN,// WebSiteVisits : used by BeanMachine
		BSMF_SITEVISIT_FOR_BEO,// WebSiteVisits : used by BEOFinance
		BSMF_SITEVISIT_FOR_FLC,// WebSiteVisits : used by Fleeca
		BSMF_SITEVISIT_FOR_PRO,// WebSiteVisits : used by Prolaps
		BSMF_SITEVISIT_FOR_XER,// WebSiteVisits : used by Xero
		BSMF_SITEVISIT_FOR_UNI,// WebSiteVisits : used by VanillaUnicorn
		BSMF_SITEVISIT_FOR_KRP,// WebSiteVisits : used by Krapea
		BSMF_SITEVISIT_FOR_LFB,// WebSiteVisits : used by Lifeblurb
		BSMF_SITEVISIT_FOR_GBL,// WebSiteVisits : used by Gambling
		BSMF_SITEVISIT_FOR_FUS,// WebSiteVisits : used by FlyUS
		BSMF_SITEVISIT_FOR_ADI,// WebSiteVisits : used by Adios
		BSMF_SITEVISIT_FOR_PED,// WebSiteVisits : used by Pensioncompany
		BSMF_SITEVISIT_FOR_REC,// WebSiteVisits : used by Recordlabel
		BSMF_SITEVISIT_FOR_CHC,// WebSiteVisits : used by ClothingCompanyC
		BSMF_SITEVISIT_FOR_CHD,// WebSiteVisits : used by ClothingCompanyD
		BSMF_SITEVISIT_FOR_CHE,// WebSiteVisits : used by ClothingCompanyE
		BSMF_SITEVISIT_FOR_CHF,// WebSiteVisits : used by ClothingCompanyF
		BSMF_SITEVISIT_FOR_FLM,// WebSiteVisits : used by FilmCompany
		BSMF_SITEVISIT_FOR_PHM,// WebSiteVisits : used by PharmaceuticalCompany
		BSMF_SITEVISIT_FOR_GOP,// WebSiteVisits : used by GoPostal
		BSMF_SITEVISIT_FOR_BAN,// WebSiteVisits : used by Banner
		BSMF_SITEVISIT_FOR_VON,// WebSiteVisits : used by VonCrastenburg
		BSMF_SITEVISIT_FOR_WTA,// WebSiteVisits : used by WatchManufactuerA
		BSMF_SITEVISIT_FOR_WTB,// WebSiteVisits : used by WatchManufactuerB
		BSMF_SITEVISIT_FOR_FGA,// WebSiteVisits : used by CigarettesA
		BSMF_SITEVISIT_FOR_FGB,// WebSiteVisits : used by CigarettesB
		BSMF_SITEVISIT_FOR_MAX,// WebSiteVisits : used by MaxRena
		BSMF_SITEVISIT_FOR_VEN,// WebSiteVisits : used by Veneer
		BSMF_SITEVISIT_FOR_RLC,// WebSiteVisits : used by RealEstateC
		BSMF_SITEVISIT_FOR_RLB,// WebSiteVisits : used by RealEstateB
		BSMF_SITEVISIT_FOR_GAS,// WebSiteVisits : used by GastroBans
		BSMF_SITEVISIT_FOR_GRU,// WebSiteVisits : used by GruppeSechs
		BSMF_SITEVISIT_FOR_PMP,// WebSiteVisits : used by Pump&Run
		BSMF_SITEVISIT_FOR_MHO,// WebSiteVisits : used by Mhollis
		BSMF_SITEVISIT_FOR_FCD,// WebSiteVisits : used by Facade
		BSMF_SITEVISIT_FOR_CTM,// WebSiteVisits : used by CustomAutos
		BSMF_SITEVISIT_FOR_IOU,// WebSiteVisits : used by MazeBank-IOUBank
		BSMF_BRAVECDEST_FOR_BRU,// BrandedVehiclesDestroyed : used by Brute
		BSMF_BRAVECDEST_FOR_VAP,// BrandedVehiclesDestroyed : used by Vapid
		BSMF_BRAVECDEST_FOR_MAI,// BrandedVehiclesDestroyed : used by Maibatsu
		BSMF_BRAVECDEST_FOR_PFI,// BrandedVehiclesDestroyed : used by Pfister
		BSMF_BRAVECDEST_FOR_HVY,// BrandedVehiclesDestroyed : used by HVY
		BSMF_BRAVECDEST_FOR_MTL,// BrandedVehiclesDestroyed : used by MTL
		BSMF_BRAVECDEST_FOR_GUN1,// BrandedVehiclesDestroyed : used by ArmsManufacturerA
		BSMF_BRAVECDEST_FOR_GUN2,// BrandedVehiclesDestroyed : used by ArmsManufacturerB
		BSMF_BRAVECDEST_FOR_GUN3,// BrandedVehiclesDestroyed : used by ArmsManufacturerC
		BSMF_BRAVECDEST_FOR_SPK,// BrandedVehiclesDestroyed : used by Sprunk
		BSMF_BRAVECDEST_FOR_CHI,// BrandedVehiclesDestroyed : used by ChihuahuaHotdogs
		BSMF_BRAVECDEST_FOR_WTR,// BrandedVehiclesDestroyed : used by Water
		BSMF_BRAVECDEST_FOR_PIS,// BrandedVehiclesDestroyed : used by Piswasser
		BSMF_BRAVECDEST_FOR_DAI,// BrandedVehiclesDestroyed : used by DailyGlobe
		BSMF_BRAVECDEST_FOR_MET,// BrandedVehiclesDestroyed : used by METV
		BSMF_BRAVECDEST_FOR_WZL,// BrandedVehiclesDestroyed : used by Weazel
		BSMF_BRAVECDEST_FOR_CNT,// BrandedVehiclesDestroyed : used by CNT
		BSMF_BRAVECDEST_FOR_LTA,// BrandedVehiclesDestroyed : used by LibertyTransportAuthority
		BSMF_BRAVECDEST_FOR_VIG,// BrandedVehiclesDestroyed : used by VigInsurance
		BSMF_BRAVECDEST_FOR_CRE,// BrandedVehiclesDestroyed : used by Crevis
		BSMF_BRAVECDEST_FOR_BDG,// BrandedVehiclesDestroyed : used by Badger
		BSMF_BRAVECDEST_FOR_WIZ,// BrandedVehiclesDestroyed : used by Wiz
		BSMF_BRAVECDEST_FOR_TNK,// BrandedVehiclesDestroyed : used by Tinkel
		BSMF_BRAVECDEST_FOR_FRT,// BrandedVehiclesDestroyed : used by iFruit
		BSMF_BRAVECDEST_FOR_BTR,// BrandedVehiclesDestroyed : used by Bittersweet
		BSMF_BRAVECDEST_FOR_RON,// BrandedVehiclesDestroyed : used by RonOil
		BSMF_BRAVECDEST_FOR_LBL,// BrandedVehiclesDestroyed : used by Libel
		BSMF_BRAVECDEST_FOR_BLE,// BrandedVehiclesDestroyed : used by Bleeter
		BSMF_BRAVECDEST_FOR_LOM,// BrandedVehiclesDestroyed : used by BankofLiberty_LOM
		BSMF_BRAVECDEST_FOR_BNC,// BrandedVehiclesDestroyed : used by BankC
		BSMF_BRAVECDEST_FOR_TXI,// BrandedVehiclesDestroyed : used by Taxi
		BSMF_BRAVECDEST_FOR_GLS,// BrandedVehiclesDestroyed : used by GoldbergLignerShyster
		BSMF_BRAVECDEST_FOR_LYB,// BrandedVehiclesDestroyed : used by LawyerB
		BSMF_BRAVECDEST_FOR_LYC,// BrandedVehiclesDestroyed : used by LawyerC
		BSMF_BRAVECDEST_FOR_PRT,// BrandedVehiclesDestroyed : used by Pirate
		BSMF_BRAVECDEST_FOR_CHA,// BrandedVehiclesDestroyed : used by ClothingCompanyA
		BSMF_BRAVECDEST_FOR_CHB,// BrandedVehiclesDestroyed : used by ClothingCompanyB
		BSMF_BRAVECDEST_FOR_RLE,// BrandedVehiclesDestroyed : used by RealEstateCompany
		BSMF_BRAVECDEST_FOR_PET,// BrandedVehiclesDestroyed : used by Petfoodcompany
		BSMF_BRAVECDEST_FOR_ECL,// BrandedVehiclesDestroyed : used by eCola
		BSMF_BRAVECDEST_FOR_CDY,// BrandedVehiclesDestroyed : used by Candybox
		BSMF_BRAVECDEST_FOR_CLK,// BrandedVehiclesDestroyed : used by CluckingBell
		BSMF_BRAVECDEST_FOR_GLY,// BrandedVehiclesDestroyed : used by GloryHole
		BSMF_BRAVECDEST_FOR_BTE,// BrandedVehiclesDestroyed : used by Bite
		BSMF_BRAVECDEST_FOR_BEN,// BrandedVehiclesDestroyed : used by BeanMachine
		BSMF_BRAVECDEST_FOR_BEO,// BrandedVehiclesDestroyed : used by BEOFinance
		BSMF_BRAVECDEST_FOR_FLC,// BrandedVehiclesDestroyed : used by Fleeca
		BSMF_BRAVECDEST_FOR_PRO,// BrandedVehiclesDestroyed : used by Prolaps
		BSMF_BRAVECDEST_FOR_XER,// BrandedVehiclesDestroyed : used by Xero
		BSMF_BRAVECDEST_FOR_UNI,// BrandedVehiclesDestroyed : used by VanillaUnicorn
		BSMF_BRAVECDEST_FOR_KRP,// BrandedVehiclesDestroyed : used by Krapea
		BSMF_BRAVECDEST_FOR_LFB,// BrandedVehiclesDestroyed : used by Lifeblurb
		BSMF_BRAVECDEST_FOR_GBL,// BrandedVehiclesDestroyed : used by Gambling
		BSMF_BRAVECDEST_FOR_FUS,// BrandedVehiclesDestroyed : used by FlyUS
		BSMF_BRAVECDEST_FOR_ADI,// BrandedVehiclesDestroyed : used by Adios
		BSMF_BRAVECDEST_FOR_PED,// BrandedVehiclesDestroyed : used by Pensioncompany
		BSMF_BRAVECDEST_FOR_REC,// BrandedVehiclesDestroyed : used by Recordlabel
		BSMF_BRAVECDEST_FOR_CHC,// BrandedVehiclesDestroyed : used by ClothingCompanyC
		BSMF_BRAVECDEST_FOR_CHD,// BrandedVehiclesDestroyed : used by ClothingCompanyD
		BSMF_BRAVECDEST_FOR_CHE,// BrandedVehiclesDestroyed : used by ClothingCompanyE
		BSMF_BRAVECDEST_FOR_CHF,// BrandedVehiclesDestroyed : used by ClothingCompanyF
		BSMF_BRAVECDEST_FOR_FLM,// BrandedVehiclesDestroyed : used by FilmCompany
		BSMF_BRAVECDEST_FOR_PHM,// BrandedVehiclesDestroyed : used by PharmaceuticalCompany
		BSMF_BRAVECDEST_FOR_GOP,// BrandedVehiclesDestroyed : used by GoPostal
		BSMF_BRAVECDEST_FOR_BAN,// BrandedVehiclesDestroyed : used by Banner
		BSMF_BRAVECDEST_FOR_VON,// BrandedVehiclesDestroyed : used by VonCrastenburg
		BSMF_BRAVECDEST_FOR_WTA,// BrandedVehiclesDestroyed : used by WatchManufactuerA
		BSMF_BRAVECDEST_FOR_WTB,// BrandedVehiclesDestroyed : used by WatchManufactuerB
		BSMF_BRAVECDEST_FOR_FGA,// BrandedVehiclesDestroyed : used by CigarettesA
		BSMF_BRAVECDEST_FOR_FGB,// BrandedVehiclesDestroyed : used by CigarettesB
		BSMF_BRAVECDEST_FOR_MAX,// BrandedVehiclesDestroyed : used by MaxRena
		BSMF_BRAVECDEST_FOR_VEN,// BrandedVehiclesDestroyed : used by Veneer
		BSMF_BRAVECDEST_FOR_RLC,// BrandedVehiclesDestroyed : used by RealEstateC
		BSMF_BRAVECDEST_FOR_RLB,// BrandedVehiclesDestroyed : used by RealEstateB
		BSMF_BRAVECDEST_FOR_GAS,// BrandedVehiclesDestroyed : used by GastroBans
		BSMF_BRAVECDEST_FOR_PMP,// BrandedVehiclesDestroyed : used by Pump&Run
		BSMF_BRAVECDEST_FOR_MHO,// BrandedVehiclesDestroyed : used by Mhollis
		BSMF_BRAVECDEST_FOR_FCD,// BrandedVehiclesDestroyed : used by Facade
		BSMF_BRAVECDEST_FOR_CTM,// BrandedVehiclesDestroyed : used by CustomAutos
		BSMF_BRAVECDEST_FOR_IOU,// BrandedVehiclesDestroyed : used by MazeBank-IOUBank
		BSMF_BOARDDEST_FOR_BRU,// BillboardsDestroyed : used by Brute
		BSMF_BOARDDEST_FOR_VAP,// BillboardsDestroyed : used by Vapid
		BSMF_BOARDDEST_FOR_MAI,// BillboardsDestroyed : used by Maibatsu
		BSMF_BOARDDEST_FOR_PFI,// BillboardsDestroyed : used by Pfister
		BSMF_BOARDDEST_FOR_HVY,// BillboardsDestroyed : used by HVY
		BSMF_BOARDDEST_FOR_MTL,// BillboardsDestroyed : used by MTL
		BSMF_BOARDDEST_FOR_GUN1,// BillboardsDestroyed : used by ArmsManufacturerA
		BSMF_BOARDDEST_FOR_GUN2,// BillboardsDestroyed : used by ArmsManufacturerB
		BSMF_BOARDDEST_FOR_GUN3,// BillboardsDestroyed : used by ArmsManufacturerC
		BSMF_BOARDDEST_FOR_SPK,// BillboardsDestroyed : used by Sprunk
		BSMF_BOARDDEST_FOR_CHI,// BillboardsDestroyed : used by ChihuahuaHotdogs
		BSMF_BOARDDEST_FOR_WTR,// BillboardsDestroyed : used by Water
		BSMF_BOARDDEST_FOR_PIS,// BillboardsDestroyed : used by Piswasser
		BSMF_BOARDDEST_FOR_DAI,// BillboardsDestroyed : used by DailyGlobe
		BSMF_BOARDDEST_FOR_MET,// BillboardsDestroyed : used by METV
		BSMF_BOARDDEST_FOR_WZL,// BillboardsDestroyed : used by Weazel
		BSMF_BOARDDEST_FOR_CNT,// BillboardsDestroyed : used by CNT
		BSMF_BOARDDEST_FOR_LTA,// BillboardsDestroyed : used by LibertyTransportAuthority
		BSMF_BOARDDEST_FOR_VIG,// BillboardsDestroyed : used by VigInsurance
		BSMF_BOARDDEST_FOR_CRE,// BillboardsDestroyed : used by Crevis
		BSMF_BOARDDEST_FOR_BDG,// BillboardsDestroyed : used by Badger
		BSMF_BOARDDEST_FOR_WIZ,// BillboardsDestroyed : used by Wiz
		BSMF_BOARDDEST_FOR_TNK,// BillboardsDestroyed : used by Tinkel
		BSMF_BOARDDEST_FOR_FRT,// BillboardsDestroyed : used by iFruit
		BSMF_BOARDDEST_FOR_BTR,// BillboardsDestroyed : used by Bittersweet
		BSMF_BOARDDEST_FOR_RON,// BillboardsDestroyed : used by RonOil
		BSMF_BOARDDEST_FOR_LBL,// BillboardsDestroyed : used by Libel
		BSMF_BOARDDEST_FOR_BLE,// BillboardsDestroyed : used by Bleeter
		BSMF_BOARDDEST_FOR_LOM,// BillboardsDestroyed : used by BankofLiberty_LOM
		BSMF_BOARDDEST_FOR_BNC,// BillboardsDestroyed : used by BankC
		BSMF_BOARDDEST_FOR_TXI,// BillboardsDestroyed : used by Taxi
		BSMF_BOARDDEST_FOR_GLS,// BillboardsDestroyed : used by GoldbergLignerShyster
		BSMF_BOARDDEST_FOR_LYB,// BillboardsDestroyed : used by LawyerB
		BSMF_BOARDDEST_FOR_LYC,// BillboardsDestroyed : used by LawyerC
		BSMF_BOARDDEST_FOR_PRT,// BillboardsDestroyed : used by Pirate
		BSMF_BOARDDEST_FOR_CHA,// BillboardsDestroyed : used by ClothingCompanyA
		BSMF_BOARDDEST_FOR_CHB,// BillboardsDestroyed : used by ClothingCompanyB
		BSMF_BOARDDEST_FOR_RLE,// BillboardsDestroyed : used by RealEstateCompany
		BSMF_BOARDDEST_FOR_PET,// BillboardsDestroyed : used by Petfoodcompany
		BSMF_BOARDDEST_FOR_ECL,// BillboardsDestroyed : used by eCola
		BSMF_BOARDDEST_FOR_CDY,// BillboardsDestroyed : used by Candybox
		BSMF_BOARDDEST_FOR_BGR,// BillboardsDestroyed : used by BurgerShot
		BSMF_BOARDDEST_FOR_CLK,// BillboardsDestroyed : used by CluckingBell
		BSMF_BOARDDEST_FOR_GLY,// BillboardsDestroyed : used by GloryHole
		BSMF_BOARDDEST_FOR_BTE,// BillboardsDestroyed : used by Bite
		BSMF_BOARDDEST_FOR_BEN,// BillboardsDestroyed : used by BeanMachine
		BSMF_BOARDDEST_FOR_BEO,// BillboardsDestroyed : used by BEOFinance
		BSMF_BOARDDEST_FOR_FLC,// BillboardsDestroyed : used by Fleeca
		BSMF_BOARDDEST_FOR_PRO,// BillboardsDestroyed : used by Prolaps
		BSMF_BOARDDEST_FOR_XER,// BillboardsDestroyed : used by Xero
		BSMF_BOARDDEST_FOR_UNI,// BillboardsDestroyed : used by VanillaUnicorn
		BSMF_BOARDDEST_FOR_KRP,// BillboardsDestroyed : used by Krapea
		BSMF_BOARDDEST_FOR_LFB,// BillboardsDestroyed : used by Lifeblurb
		BSMF_BOARDDEST_FOR_GBL,// BillboardsDestroyed : used by Gambling
		BSMF_BOARDDEST_FOR_FUS,// BillboardsDestroyed : used by FlyUS
		BSMF_BOARDDEST_FOR_ADI,// BillboardsDestroyed : used by Adios
		BSMF_BOARDDEST_FOR_PED,// BillboardsDestroyed : used by Pensioncompany
		BSMF_BOARDDEST_FOR_REC,// BillboardsDestroyed : used by Recordlabel
		BSMF_BOARDDEST_FOR_CHC,// BillboardsDestroyed : used by ClothingCompanyC
		BSMF_BOARDDEST_FOR_CHD,// BillboardsDestroyed : used by ClothingCompanyD
		BSMF_BOARDDEST_FOR_CHE,// BillboardsDestroyed : used by ClothingCompanyE
		BSMF_BOARDDEST_FOR_CHF,// BillboardsDestroyed : used by ClothingCompanyF
		BSMF_BOARDDEST_FOR_FLM,// BillboardsDestroyed : used by FilmCompany
		BSMF_BOARDDEST_FOR_PHM,// BillboardsDestroyed : used by PharmaceuticalCompany
		BSMF_BOARDDEST_FOR_GOP,// BillboardsDestroyed : used by GoPostal
		BSMF_BOARDDEST_FOR_BAN,// BillboardsDestroyed : used by Banner
		BSMF_BOARDDEST_FOR_VON,// BillboardsDestroyed : used by VonCrastenburg
		BSMF_BOARDDEST_FOR_WTA,// BillboardsDestroyed : used by WatchManufactuerA
		BSMF_BOARDDEST_FOR_WTB,// BillboardsDestroyed : used by WatchManufactuerB
		BSMF_BOARDDEST_FOR_FGA,// BillboardsDestroyed : used by CigarettesA
		BSMF_BOARDDEST_FOR_FGB,// BillboardsDestroyed : used by CigarettesB
		BSMF_BOARDDEST_FOR_MAX,// BillboardsDestroyed : used by MaxRena
		BSMF_BOARDDEST_FOR_VEN,// BillboardsDestroyed : used by Veneer
		BSMF_BOARDDEST_FOR_RLC,// BillboardsDestroyed : used by RealEstateC
		BSMF_BOARDDEST_FOR_RLB,// BillboardsDestroyed : used by RealEstateB
		BSMF_BOARDDEST_FOR_GAS,// BillboardsDestroyed : used by GastroBans
		BSMF_BOARDDEST_FOR_GRU,// BillboardsDestroyed : used by GruppeSechs
		BSMF_BOARDDEST_FOR_PMP,// BillboardsDestroyed : used by Pump&Run
		BSMF_BOARDDEST_FOR_MHO,// BillboardsDestroyed : used by Mhollis
		BSMF_BOARDDEST_FOR_FCD,// BillboardsDestroyed : used by Facade
		BSMF_BOARDDEST_FOR_CTM,// BillboardsDestroyed : used by CustomAutos
		BSMF_BOARDDEST_FOR_IOU,// BillboardsDestroyed : used by MazeBank-IOUBank
		BSMF_VECBUY_FOR_BRU,// VehiclesOfTypeBought : used by Brute
		BSMF_VECBUY_FOR_VAP,// VehiclesOfTypeBought : used by Vapid
		BSMF_VECBUY_FOR_MAI,// VehiclesOfTypeBought : used by Maibatsu
		BSMF_VECBUY_FOR_PFI,// VehiclesOfTypeBought : used by Pfister
		BSMF_VECBUY_FOR_HVY,// VehiclesOfTypeBought : used by HVY
		BSMF_VECBUY_FOR_MTL,// VehiclesOfTypeBought : used by MTL
		BSMF_DISTDRIV_FOR_BRU,// DistanceDrivenInType : used by Brute
		BSMF_DISTDRIV_FOR_VAP,// DistanceDrivenInType : used by Vapid
		BSMF_DISTDRIV_FOR_MAI,// DistanceDrivenInType : used by Maibatsu
		BSMF_DISTDRIV_FOR_PFI,// DistanceDrivenInType : used by Pfister
		BSMF_DISTDRIV_FOR_HVY,// DistanceDrivenInType : used by HVY
		BSMF_DISTDRIV_FOR_MTL,// DistanceDrivenInType : used by MTL
		BSMF_VECMOD_FOR_BRU,// VehicleModification : used by Brute
		BSMF_VECMOD_FOR_VAP,// VehicleModification : used by Vapid
		BSMF_VECMOD_FOR_MAI,// VehicleModification : used by Maibatsu
		BSMF_VECMOD_FOR_PFI,// VehicleModification : used by Pfister
		BSMF_VECMOD_FOR_HVY,// VehicleModification : used by HVY
		BSMF_VECMOD_FOR_MTL,// VehicleModification : used by MTL
		BSMF_STUNTJUMP_FOR_BRU,// StuntJumpsCompleatedInVehilceType : used by Brute
		BSMF_STUNTJUMP_FOR_MAI,// StuntJumpsCompleatedInVehilceType : used by Maibatsu
		BSMF_STUNTJUMP_FOR_PFI,// StuntJumpsCompleatedInVehilceType : used by Pfister
		BSMF_STUNTJUMP_FOR_HVY,// StuntJumpsCompleatedInVehilceType : used by HVY
		BSMF_STUNTJUMP_FOR_MTL,// StuntJumpsCompleatedInVehilceType : used by MTL
		BSMF_VECTHEFT_FOR_BRU,// VehicleOfTypeStolen : used by Brute
		BSMF_VECTHEFT_FOR_VAP,// VehicleOfTypeStolen : used by Vapid
		BSMF_VECTHEFT_FOR_MAI,// VehicleOfTypeStolen : used by Maibatsu
		BSMF_VECTHEFT_FOR_PFI,// VehicleOfTypeStolen : used by Pfister
		BSMF_VECTHEFT_FOR_HVY,// VehicleOfTypeStolen : used by HVY
		BSMF_VECTHEFT_FOR_MTL,// VehicleOfTypeStolen : used by MTL
		BSMF_VECDAMAGE_FOR_BRU,// DamageDoneToVehicleType : used by Brute
		BSMF_VECDAMAGE_FOR_VAP,// DamageDoneToVehicleType : used by Vapid
		BSMF_VECDAMAGE_FOR_MAI,// DamageDoneToVehicleType : used by Maibatsu
		BSMF_VECDAMAGE_FOR_PFI,// DamageDoneToVehicleType : used by Pfister
		BSMF_VECDAMAGE_FOR_HVY,// DamageDoneToVehicleType : used by HVY
		BSMF_VECDAMAGE_FOR_MTL,// DamageDoneToVehicleType : used by MTL
		BSMF_VECKILL_FOR_BRU,// PeopleKilledWithVehicle : used by Brute
		BSMF_VECKILL_FOR_VAP,// PeopleKilledWithVehicle : used by Vapid
		BSMF_VECKILL_FOR_MAI,// PeopleKilledWithVehicle : used by Maibatsu
		BSMF_VECKILL_FOR_PFI,// PeopleKilledWithVehicle : used by Pfister
		BSMF_VECKILL_FOR_HVY,// PeopleKilledWithVehicle : used by HVY
		BSMF_VECKILL_FOR_MTL,// PeopleKilledWithVehicle : used by MTL
		BSMF_TAXIUSE, //TaxisAndUseOfPublicTransport
		BSMF_WEPUSED_FOR_GUN1,// WeaponTypeUsed : used by ArmsManufacturerA
		BSMF_WEPUSED_FOR_GUN2,// WeaponTypeUsed : used by ArmsManufacturerB
		BSMF_WEPUSED_FOR_GUN3,// WeaponTypeUsed : used by ArmsManufacturerC
		BSMF_WEPTAKEN_FOR_GUN1,// WeaponTypeTaken : used by ArmsManufacturerA
		BSMF_WEPTAKEN_FOR_GUN2,// WeaponTypeTaken : used by ArmsManufacturerB
		BSMF_WEPTAKEN_FOR_GUN3,// WeaponTypeTaken : used by ArmsManufacturerC
		BSMF_WEPCOPKILL_FOR_GUN1,// CopsKilledWithWeaponType : used by ArmsManufacturerA
		BSMF_WEPCOPKILL_FOR_GUN2,// CopsKilledWithWeaponType : used by ArmsManufacturerB
		BSMF_WEPCOPKILL_FOR_GUN3,// CopsKilledWithWeaponType : used by ArmsManufacturerC
		BSMF_WEPCRIMKILL_FOR_GUN1,// CriminalsKilledWithWeaponType : used by ArmsManufacturerA
		BSMF_WEPCRIMKILL_FOR_GUN2,// CriminalsKilledWithWeaponType : used by ArmsManufacturerB
		BSMF_WEPCRIMKILL_FOR_GUN3,// CriminalsKilledWithWeaponType : used by ArmsManufacturerC
		BSMF_WEPCIVKILL_FOR_GUN1,// CiviliansKilledWithWeaponType : used by ArmsManufacturerA
		BSMF_WEPCIVKILL_FOR_GUN2,// CiviliansKilledWithWeaponType : used by ArmsManufacturerB
		BSMF_WEPCIVKILL_FOR_GUN3,// CiviliansKilledWithWeaponType : used by ArmsManufacturerC
		BSMF_WEPCRIMCARKILL_FOR_GUN1,// CriminalVehiclesKilledWithWeapon : used by ArmsManufacturerA
		BSMF_WEPCRIMCARKILL_FOR_GUN2,// CriminalVehiclesKilledWithWeapon : used by ArmsManufacturerB
		BSMF_WEPCRIMCARKILL_FOR_GUN3,// CriminalVehiclesKilledWithWeapon : used by ArmsManufacturerC
		BSMF_WEPPLAKILL_FOR_GUN1,// TimesPlayerIsKilledWithWeaponType : used by ArmsManufacturerA
		BSMF_WEPPLAKILL_FOR_GUN2,// TimesPlayerIsKilledWithWeaponType : used by ArmsManufacturerB
		BSMF_WEPPLAKILL_FOR_GUN3,// TimesPlayerIsKilledWithWeaponType : used by ArmsManufacturerC
		BSMF_VENDUSED_FOR_SPK,// VendingMachieneOftypeUsed : used by Sprunk
		BSMF_VENDUSED_FOR_CHI,// VendingMachieneOftypeUsed : used by ChihuahuaHotdogs
		BSMF_VENDUSED_FOR_WTR,// VendingMachieneOftypeUsed : used by Water
		BSMF_VENDUSED_FOR_ECL,// VendingMachieneOftypeUsed : used by eCola
		BSMF_VENDUSED_FOR_CDY,// VendingMachieneOftypeUsed : used by Candybox
		BSMF_VENDUSED_FOR_BGR,// VendingMachieneOftypeUsed : used by BurgerShot
		BSMF_VENDUSED_FOR_CLK,// VendingMachieneOftypeUsed : used by CluckingBell
		BSMF_VENDUSED_FOR_GLY,// VendingMachieneOftypeUsed : used by GloryHole
		BSMF_VENDUSED_FOR_BTE,// VendingMachieneOftypeUsed : used by Bite
		BSMF_VENDUSED_FOR_BEN,// VendingMachieneOftypeUsed : used by BeanMachine
		BSMF_FITDOWN, //Reducedfitness
		BSMF_FITUP, //Increasedfitness
		BSMF_NEWSSTALLDAM, //DamageToNewsagentStalls
		BSMF_HPEDKILL, //HealthyPeopleKilled
		BSMF_PUBCLUBVISIT, //PubandClubvisits
		BSMF_TIMESDRUNK, //TimesDrunk
		BSMF_FRNDTOPUB, //FriendsTakenToPubsclubs
		BSMF_DRNKCRIME, //DrunkenCrimes
		BSMF_PLYRAMPAGE, //RampagesComplete
		BSMF_HWANTEDTIME, //GainedFourstars
		BSMF_NOWNTIME, //TimeWithNoWantedRating
		BSMF_LISTENTIME_FOR_MET,// RadioStationsListenedTo : used by METV
		BSMF_LISTENTIME_FOR_WZL,// RadioStationsListenedTo : used by Weazel
		BSMF_LISTENTIME_FOR_CNT,// RadioStationsListenedTo : used by CNT
		BSMF_ZITID_FOR_MET,// SongsIDedWithZIT : used by METV
		BSMF_ZITID_FOR_WZL,// SongsIDedWithZIT : used by Weazel
		BSMF_ZITID_FOR_CNT,// SongsIDedWithZIT : used by CNT
		BSMF_ZITID_FOR_PRT,// SongsIDedWithZIT : used by Pirate
		BSMF_RADSWITCH_FOR_MET,// TimesChangedAwayFromRadiostation : used by METV
		BSMF_RADSWITCH_FOR_WZL,// TimesChangedAwayFromRadiostation : used by Weazel
		BSMF_RADSWITCH_FOR_CNT,// TimesChangedAwayFromRadiostation : used by CNT
		BSMF_VEC, //PublicVehiclesDamagedOrDestroyed
		BSMF_PARAJUMP, //ParachuteJumpsBaseJumps
		BSMF_WHEELIES, //Wheelies
		BSMF_STOPPIES, //Stoppies
		BSMF_ONFIRETIME, //BeingOnFireTime
		BSMF_VECBAIL, //BailsFromVehicle
		BSMF_FIREVECBAIL, //BailsFromVehicleWhenOnFire
		BSMF_PARADEATH, //DeathWhenParachuting
		BSMF_CRASHDEATH, //DeathsFromCrashingVehicles
		BSMF_PHONEOPENTIME_FOR_BDG,// PhonePopularity : used by Badger
		BSMF_PHONEOPENTIME_FOR_WIZ,// PhonePopularity : used by Wiz
		BSMF_PHONEOPENTIME_FOR_TNK,// PhonePopularity : used by Tinkel
		BSMF_CALLTIME_FOR_BDG,// PhoneUseCalls : used by Badger
		BSMF_CALLTIME_FOR_WIZ,// PhoneUseCalls : used by Wiz
		BSMF_CALLTIME_FOR_TNK,// PhoneUseCalls : used by Tinkel
		BSMF_TEXTSUSED_FOR_BDG,// PhoneUseTexts : used by Badger
		BSMF_TEXTSUSED_FOR_WIZ,// PhoneUseTexts : used by Wiz
		BSMF_TEXTSUSED_FOR_TNK,// PhoneUseTexts : used by Tinkel
		BSMF_CALLREFUSED_FOR_BDG,// PhoneCallsIgnored : used by Badger
		BSMF_CALLREFUSED_FOR_WIZ,// PhoneCallsIgnored : used by Wiz
		BSMF_CALLREFUSED_FOR_TNK,// PhoneCallsIgnored : used by Tinkel
		BSMF_OTPLTIME_FOR_BDG,// TimeUsingOtherPlayerCharacters : used by Badger
		BSMF_OTPLTIME_FOR_WIZ,// TimeUsingOtherPlayerCharacters : used by Wiz
		BSMF_OTPLTIME_FOR_TNK,// TimeUsingOtherPlayerCharacters : used by Tinkel
		BSMF_LAPTOPTIME, //TimeSpentOnLaptops
		BSMF_PHONEPOP_FOR_FRT,// PhonePopularity : used by iFruit
		BSMF_PHONEPOP_FOR_BTR,// PhonePopularity : used by Bittersweet
		BSMF_PHONEPOP_FOR_FCD,// PhonePopularity : used by Facade
		BSMF_APPSDL_FOR_FRT,// PhoneAppsDownloaded : used by iFruit
		BSMF_APPSDL_FOR_BTR,// PhoneAppsDownloaded : used by Bittersweet
		BSMF_INTERNETTIME_FOR_FRT,// PhoneInternetUse : used by iFruit
		BSMF_INTERNETTIME_FOR_BTR,// PhoneInternetUse : used by Bittersweet
		BSMF_INTERNETTIME_FOR_FCD,// PhoneInternetUse : used by Facade
		BSMF_APPTIME, //GTA5iphoneappUsage
		BSMF_BROKENPHONE_FOR_FRT,// TimesKilledNeedNewPhone : used by iFruit
		BSMF_BROKENPHONE_FOR_BTR,// TimesKilledNeedNewPhone : used by Bittersweet
		BSMF_BROKENPHONE_FOR_FCD,// TimesKilledNeedNewPhone : used by Facade
		BSMF_GAMEPLAYEDTIME, //TimeInGameConsolesPlayed
		BSMF_COMPTIME, //TimeSpentOnComputerDesktop
		BSMF_JTKILLS, //PeopleKilledAfterPlayingIngameVideoGamse
		BSMF_COTANKDEST_FOR_RON,// CompetitorTakersDestroyed : used by RonOil
		BSMF_COTANKDEST_FOR_XER,// CompetitorTakersDestroyed : used by Xero
		BSMF_TANKERDEST_FOR_RON,// TankersDestroyed : used by RonOil
		BSMF_TANKERDEST_FOR_XER,// TankersDestroyed : used by Xero
		BSMF_GARAGEDEST_FOR_RON,// GaragesDestroyed : used by RonOil
		BSMF_GARAGEDEST_FOR_XER,// GaragesDestroyed : used by Xero
		BSMF_SMINTUSED, //TimesTwitterFacebookIntergrationUsed
		BSMF_GARAGEREPAIR, //GarageRepairsMade
		BSMF_GARAGEMONEY, //MoneySpentAtGarage
		BSMF_MODBUYFAIL, //CarsModifiedThenNotBought
		BSMF_MONEYGAINED_FOR_LOM,// MoneyInPlayersAccount : used by BankofLiberty_LOM
		BSMF_MONEYGAINED_FOR_BNC,// MoneyInPlayersAccount : used by BankC
		BSMF_MONEYGAINED_FOR_IOU,// MoneyInPlayersAccount : used by MazeBank-IOUBank
		BSMF_MONEYSPENT_FOR_LOM,// AmountOfMoneySPent : used by BankofLiberty_LOM
		BSMF_MONEYSPENT_FOR_BNC,// AmountOfMoneySPent : used by BankC
		BSMF_MONEYSPENT_FOR_IOU,// AmountOfMoneySPent : used by MazeBank-IOUBank
		BSMF_TAXIUSE0, //TaxiRides
		BSMF_TAXI_DESTROYED, //TaxisDestroyed
		BSMF_BUSTED_FOR_GLS,// TimesBusted : used by GoldbergLignerShyster
		BSMF_BUSTED_FOR_LYB,// TimesBusted : used by LawyerB
		BSMF_BUSTED_FOR_LYC,// TimesBusted : used by LawyerC
		BSMF_PROPBUY_FOR_GLS,// PropertyBought : used by GoldbergLignerShyster
		BSMF_PROPBUY_FOR_LYB,// PropertyBought : used by LawyerB
		BSMF_PROPBUY_FOR_LYC,// PropertyBought : used by LawyerC
		BSMF_KILLS_FOR_GLS,// PeopleKilled : used by GoldbergLignerShyster
		BSMF_KILLS_FOR_LYB,// PeopleKilled : used by LawyerB
		BSMF_KILLS_FOR_LYC,// PeopleKilled : used by LawyerC
		BSMF_EVADEDSTARS_FOR_GLS,// WantedRatingEvaded : used by GoldbergLignerShyster
		BSMF_EVADEDSTARS_FOR_LYB,// WantedRatingEvaded : used by LawyerB
		BSMF_EVADEDSTARS_FOR_LYC,// WantedRatingEvaded : used by LawyerC
		BSMF_CRIMES_FOR_GLS,// CrimesComited : used by GoldbergLignerShyster
		BSMF_CRIMES_FOR_LYB,// CrimesComited : used by LawyerB
		BSMF_CRIMES_FOR_LYC,// CrimesComited : used by LawyerC
		BSMF_CLOBOUGHT_FOR_CHA,// ClothesBought : used by ClothingCompanyA
		BSMF_CLOBOUGHT_FOR_CHB,// ClothesBought : used by ClothingCompanyB
		BSMF_CLOBOUGHT_FOR_CHC,// ClothesBought : used by ClothingCompanyC
		BSMF_CLOBOUGHT_FOR_CHD,// ClothesBought : used by ClothingCompanyD
		BSMF_CLOBOUGHT_FOR_CHE,// ClothesBought : used by ClothingCompanyE
		BSMF_CLOBOUGHT_FOR_CHF,// ClothesBought : used by ClothingCompanyF
		BSMF_COMPETITORSALES_FOR_CHA,// SalesToCompetitors : used by ClothingCompanyA
		BSMF_COMPETITORSALES_FOR_CHB,// SalesToCompetitors : used by ClothingCompanyB
		BSMF_COMPETITORSALES_FOR_CHC,// SalesToCompetitors : used by ClothingCompanyC
		BSMF_COMPETITORSALES_FOR_CHD,// SalesToCompetitors : used by ClothingCompanyD
		BSMF_COMPETITORSALES_FOR_CHE,// SalesToCompetitors : used by ClothingCompanyE
		BSMF_COMPETITORSALES_FOR_CHF,// SalesToCompetitors : used by ClothingCompanyF
		BSMF_DOGAPPUSE, //DogApplicationUse
		BSMF_HAPPYDOGTIME, //HappyDogTime
		BSMF_DOGFED, //DogFed
		BSMF_UNHAPPYDOGTIME, //UnhappyDogTime
		BSMF_PRISONBAILS, //TimesBailedFromPrison
		BSMF_MINUSMONEYTIME, //TimeInTheRed
		BSMF_GOLFPLAYED, //TimesPlayedGolf
		BSMF_YOGATIMES, //TimesCompleatedYoga
		BSMF_TRICOMPLETED, //TimesCompleatedTriathlon
		BSMF_GYMTIME, //TimeSpentAtGym
		BSMF_STRIPCLUBVISIT, //StripClubVisits
		BSMF_STRIPTIME, //TimeAtStripClub
		BSMF_UGHOOKERSKILL, //UglyHookersKilled
		BSMF_STRIPCLUBTROUBLE, //TroubleCauseAtStripClub
		BSMF_SAFEHOUSEUPGRD, //SafeHousesUpgraded
		BSMF_BDLGDESTROYED, //BuldingsDestroyed
		BSMF_OWNDSAFEHOUSE, //SafeHousesOwned
		BSMF_VECDEST, //VehiclesDestroyed
		BSMF_BETSPLACED, //InGameBetsPlaced
		BSMF_PILOTTIMES, //PilotSchoolUse
		BSMF_TIMEPLAYED, //TimePlayed
		BSMF_TOTALKILLS, //TotalPeopleKilled
		BSMF_HOSPITALVIS, //HospitalVsits
		BSMF_PEOPLEINJURED, //PeopleInjured
		BSMF_DRUGBUY, //DrugsBought
		BSMF_HEALTHPACKSUSED, //HealtPacksUsed
		BSMF_DDKILLED, //DrugDealersKilled
		BSMF_HANGOVERS, //NumberOfHangOvers
		BSMF_KILLSPREES, //KillingSpres
		BSMF_MAILBOXDEST, //MailBoxesDestroyed
		BSMF_EMAILSENT, //EmailsSent
		BSMF_TEXTMESSENT, //TextMessagesSent
		BSMF_PEDFIREKILL, //PedsKilledByFire
		BSMF_PEDAFLAMETIME, //PedsOnFireTimes
		BSMF_TOTAL_INDICES
	ENDENUM

/// Intialisation and modifier update functionality.

CONST_INT MAX_STOCK_PRICE_LOG_ENTRIES 14 //two logs a day

STRUCT BAWSAQ_LISTING_STRUCT
		TEXT_LABEL sLongName
		TEXT_LABEL sShortName
		TEXT_LABEL sCoDesc

		BOOL bOnlineStock

		FLOAT fCurrentPrice
		FLOAT fLastPriceChange
		FLOAT fStockPriceChangeSensitivity
		FLOAT fUniversalPriceModifier
		FLOAT fUPMAppliedLastUpdate
		INT iVolumeTraded
		INT iLogIndexCaret

		FLOAT fHighValue
		FLOAT fLowValue

		FLOAT fLastPriceChangeInPercent

		FLOAT LoggedPrices[MAX_STOCK_PRICE_LOG_ENTRIES]
		INT LoggedVolumes[MAX_STOCK_PRICE_LOG_ENTRIES]
		STATSENUM OnlinePriceStat
ENDSTRUCT

BAWSAQ_LISTING_STRUCT g_BS_Listings[BS_CO_TOTAL_LISTINGS]


STRUCT BAWSAQ_MODIFIER_STRUCT
	FLOAT fModValue	INT iChangeThisUpdate
	INT iChangeLastUpdate
	bool bOnline
	STATSENUM uploadTo
ENDSTRUCT

BAWSAQ_MODIFIER_STRUCT g_BS_Modifiers[BSMF_TOTAL_INDICES]

	

PROC BAWSAQ_INTIALISE_LISTS()

	INT i = 0
	REPEAT BS_CO_TOTAL_LISTINGS i
		g_BS_Listings[i].fLastPriceChange = 0.0
		g_BS_Listings[i].iLogIndexCaret = 0
		g_BS_Listings[i].iVolumeTraded = 0
		g_BS_Listings[i].fHighValue = 0
		g_BS_Listings[i].fLowValue = 3.402823E+38
		int j = 0
		REPEAT MAX_STOCK_PRICE_LOG_ENTRIES j
			g_BS_Listings[i].LoggedPrices[j] = 0.0
			g_BS_Listings[i].LoggedVolumes[j] = 0
		ENDREPEAT
	ENDREPEAT

	i = 0
	REPEAT BSMF_TOTAL_INDICES i
		g_BS_Modifiers[i].iChangeThisUpdate = 0
		g_BS_Modifiers[i].iChangeLastUpdate = 0
		g_BS_Modifiers[i].bOnline = FALSE
	ENDREPEAT

	g_BS_Listings[BS_CO_BRU].sLongName =  "BSS_0"
	g_BS_Listings[BS_CO_BRU].sShortName =  "BSS_1"
	g_BS_Listings[BS_CO_BRU].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_BRU].OnlinePriceStat =  SM_BRU_PRICE
	g_BS_Listings[BS_CO_BRU].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_BRU].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_VAP].sLongName =  "BSS_2"
	g_BS_Listings[BS_CO_VAP].sShortName =  "BSS_3"
	g_BS_Listings[BS_CO_VAP].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_VAP].OnlinePriceStat =  SM_VAP_PRICE
	g_BS_Listings[BS_CO_VAP].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_VAP].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_MAI].sLongName =  "BSS_4"
	g_BS_Listings[BS_CO_MAI].sShortName =  "BSS_5"
	g_BS_Listings[BS_CO_MAI].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_MAI].OnlinePriceStat =  SM_MAI_PRICE
	g_BS_Listings[BS_CO_MAI].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_MAI].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_PFI].sLongName =  "BSS_6"
	g_BS_Listings[BS_CO_PFI].sShortName =  "BSS_7"
	g_BS_Listings[BS_CO_PFI].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_PFI].OnlinePriceStat =  SM_PFI_PRICE
	g_BS_Listings[BS_CO_PFI].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_PFI].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_HVY].sLongName =  "BSS_8"
	g_BS_Listings[BS_CO_HVY].sShortName =  "BSS_8"
	g_BS_Listings[BS_CO_HVY].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_HVY].OnlinePriceStat =  SM_HVY_PRICE
	g_BS_Listings[BS_CO_HVY].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_HVY].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_MTL].sLongName =  "BSS_9"
	g_BS_Listings[BS_CO_MTL].sShortName =  "BSS_9"
	g_BS_Listings[BS_CO_MTL].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_MTL].OnlinePriceStat =  SM_MTL_PRICE
	g_BS_Listings[BS_CO_MTL].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_MTL].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_GUN1].sLongName =  "BSS_10"
	g_BS_Listings[BS_CO_GUN1].sShortName =  "BSS_11"
	g_BS_Listings[BS_CO_GUN1].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_GUN1].OnlinePriceStat =  SM_GUN1_PRICE
	g_BS_Listings[BS_CO_GUN1].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_GUN1].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_GUN2].sLongName =  "BSS_12"
	g_BS_Listings[BS_CO_GUN2].sShortName =  "BSS_13"
	g_BS_Listings[BS_CO_GUN2].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_GUN2].OnlinePriceStat =  SM_GUN2_PRICE
	g_BS_Listings[BS_CO_GUN2].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_GUN2].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_GUN3].sLongName =  "BSS_14"
	g_BS_Listings[BS_CO_GUN3].sShortName =  "BSS_15"
	g_BS_Listings[BS_CO_GUN3].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_GUN3].OnlinePriceStat =  SM_GUN3_PRICE
	g_BS_Listings[BS_CO_GUN3].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_GUN3].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_SPK].sLongName =  "BSS_16"
	g_BS_Listings[BS_CO_SPK].sShortName =  "BSS_17"
	g_BS_Listings[BS_CO_SPK].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_SPK].OnlinePriceStat =  SM_SPK_PRICE
	g_BS_Listings[BS_CO_SPK].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_SPK].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_CHI].sLongName =  "BSS_18"
	g_BS_Listings[BS_CO_CHI].sShortName =  "BSS_19"
	g_BS_Listings[BS_CO_CHI].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_CHI].OnlinePriceStat =  SM_CHI_PRICE
	g_BS_Listings[BS_CO_CHI].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_CHI].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_WTR].sLongName =  "BSS_20"
	g_BS_Listings[BS_CO_WTR].sShortName =  "BSS_21"
	g_BS_Listings[BS_CO_WTR].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_WTR].OnlinePriceStat =  SM_WTR_PRICE
	g_BS_Listings[BS_CO_WTR].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_WTR].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_PIS].sLongName =  "BSS_22"
	g_BS_Listings[BS_CO_PIS].sShortName =  "BSS_23"
	g_BS_Listings[BS_CO_PIS].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_PIS].OnlinePriceStat =  SM_PIS_PRICE
	g_BS_Listings[BS_CO_PIS].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_PIS].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_DAI].sLongName =  "BSS_24"
	g_BS_Listings[BS_CO_DAI].sShortName =  "BSS_25"
	g_BS_Listings[BS_CO_DAI].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_DAI].OnlinePriceStat =  SM_DAI_PRICE
	g_BS_Listings[BS_CO_DAI].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_DAI].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_MET].sLongName =  "BSS_26"
	g_BS_Listings[BS_CO_MET].sShortName =  "BSS_27"
	g_BS_Listings[BS_CO_MET].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_MET].OnlinePriceStat =  SM_MET_PRICE
	g_BS_Listings[BS_CO_MET].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_MET].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_WZL].sLongName =  "BSS_28"
	g_BS_Listings[BS_CO_WZL].sShortName =  "BSS_29"
	g_BS_Listings[BS_CO_WZL].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_WZL].OnlinePriceStat =  SM_WZL_PRICE
	g_BS_Listings[BS_CO_WZL].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_WZL].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_CNT].sLongName =  "BSS_30"
	g_BS_Listings[BS_CO_CNT].sShortName =  "BSS_30"
	g_BS_Listings[BS_CO_CNT].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_CNT].OnlinePriceStat =  SM_CNT_PRICE
	g_BS_Listings[BS_CO_CNT].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_CNT].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_LTA].sLongName =  "BSS_31"
	g_BS_Listings[BS_CO_LTA].sShortName =  "BSS_32"
	g_BS_Listings[BS_CO_LTA].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_LTA].OnlinePriceStat =  SM_LTA_PRICE
	g_BS_Listings[BS_CO_LTA].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_LTA].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_VIG].sLongName =  "BSS_33"
	g_BS_Listings[BS_CO_VIG].sShortName =  "BSS_34"
	g_BS_Listings[BS_CO_VIG].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_VIG].OnlinePriceStat =  SM_VIG_PRICE
	g_BS_Listings[BS_CO_VIG].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_VIG].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_CRE].sLongName =  "BSS_35"
	g_BS_Listings[BS_CO_CRE].sShortName =  "BSS_36"
	g_BS_Listings[BS_CO_CRE].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_CRE].OnlinePriceStat =  SM_CRE_PRICE
	g_BS_Listings[BS_CO_CRE].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_CRE].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_BDG].sLongName =  "BSS_37"
	g_BS_Listings[BS_CO_BDG].sShortName =  "BSS_38"
	g_BS_Listings[BS_CO_BDG].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_BDG].OnlinePriceStat =  SM_BDG_PRICE
	g_BS_Listings[BS_CO_BDG].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_BDG].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_WIZ].sLongName =  "BSS_39"
	g_BS_Listings[BS_CO_WIZ].sShortName =  "BSS_40"
	g_BS_Listings[BS_CO_WIZ].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_WIZ].OnlinePriceStat =  SM_WIZ_PRICE
	g_BS_Listings[BS_CO_WIZ].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_WIZ].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_TNK].sLongName =  "BSS_41"
	g_BS_Listings[BS_CO_TNK].sShortName =  "BSS_42"
	g_BS_Listings[BS_CO_TNK].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_TNK].OnlinePriceStat =  SM_TNK_PRICE
	g_BS_Listings[BS_CO_TNK].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_TNK].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_FRT].sLongName =  "BSS_43"
	g_BS_Listings[BS_CO_FRT].sShortName =  "BSS_44"
	g_BS_Listings[BS_CO_FRT].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_FRT].OnlinePriceStat =  SM_FRT_PRICE
	g_BS_Listings[BS_CO_FRT].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_FRT].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_BTR].sLongName =  "BSS_45"
	g_BS_Listings[BS_CO_BTR].sShortName =  "BSS_46"
	g_BS_Listings[BS_CO_BTR].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_BTR].OnlinePriceStat =  SM_BTR_PRICE
	g_BS_Listings[BS_CO_BTR].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_BTR].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_RON].sLongName =  "BSS_47"
	g_BS_Listings[BS_CO_RON].sShortName =  "BSS_48"
	g_BS_Listings[BS_CO_RON].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_RON].OnlinePriceStat =  SM_RON_PRICE
	g_BS_Listings[BS_CO_RON].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_RON].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_LBL].sLongName =  "BSS_49"
	g_BS_Listings[BS_CO_LBL].sShortName =  "BSS_50"
	g_BS_Listings[BS_CO_LBL].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_LBL].OnlinePriceStat =  SM_LBL_PRICE
	g_BS_Listings[BS_CO_LBL].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_LBL].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_BLE].sLongName =  "BSS_51"
	g_BS_Listings[BS_CO_BLE].sShortName =  "BSS_52"
	g_BS_Listings[BS_CO_BLE].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_BLE].OnlinePriceStat =  SM_BLE_PRICE
	g_BS_Listings[BS_CO_BLE].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_BLE].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_LOM].sLongName =  "BSS_53"
	g_BS_Listings[BS_CO_LOM].sShortName =  "BSS_54"
	g_BS_Listings[BS_CO_LOM].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_LOM].OnlinePriceStat =  SM_LOM_PRICE
	g_BS_Listings[BS_CO_LOM].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_LOM].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_BNC].sLongName =  "BSS_55"
	g_BS_Listings[BS_CO_BNC].sShortName =  "BSS_56"
	g_BS_Listings[BS_CO_BNC].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_BNC].OnlinePriceStat =  SM_BNC_PRICE
	g_BS_Listings[BS_CO_BNC].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_BNC].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_TXI].sLongName =  "BSS_57"
	g_BS_Listings[BS_CO_TXI].sShortName =  "BSS_58"
	g_BS_Listings[BS_CO_TXI].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_TXI].OnlinePriceStat =  SM_TXI_PRICE
	g_BS_Listings[BS_CO_TXI].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_TXI].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_GLS].sLongName =  "BSS_59"
	g_BS_Listings[BS_CO_GLS].sShortName =  "BSS_60"
	g_BS_Listings[BS_CO_GLS].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_GLS].OnlinePriceStat =  SM_GLS_PRICE
	g_BS_Listings[BS_CO_GLS].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_GLS].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_LYB].sLongName =  "BSS_61"
	g_BS_Listings[BS_CO_LYB].sShortName =  "BSS_62"
	g_BS_Listings[BS_CO_LYB].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_LYB].OnlinePriceStat =  SM_LYB_PRICE
	g_BS_Listings[BS_CO_LYB].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_LYB].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_LYC].sLongName =  "BSS_63"
	g_BS_Listings[BS_CO_LYC].sShortName =  "BSS_64"
	g_BS_Listings[BS_CO_LYC].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_LYC].OnlinePriceStat =  SM_LYC_PRICE
	g_BS_Listings[BS_CO_LYC].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_LYC].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_PRT].sLongName =  "BSS_65"
	g_BS_Listings[BS_CO_PRT].sShortName =  "BSS_66"
	g_BS_Listings[BS_CO_PRT].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_PRT].OnlinePriceStat =  SM_PRT_PRICE
	g_BS_Listings[BS_CO_PRT].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_PRT].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_CHA].sLongName =  "BSS_67"
	g_BS_Listings[BS_CO_CHA].sShortName =  "BSS_68"
	g_BS_Listings[BS_CO_CHA].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_CHA].OnlinePriceStat =  SM_CHA_PRICE
	g_BS_Listings[BS_CO_CHA].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_CHA].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_CHB].sLongName =  "BSS_69"
	g_BS_Listings[BS_CO_CHB].sShortName =  "BSS_70"
	g_BS_Listings[BS_CO_CHB].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_CHB].OnlinePriceStat =  SM_CHB_PRICE
	g_BS_Listings[BS_CO_CHB].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_CHB].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_RLE].sLongName =  "BSS_71"
	g_BS_Listings[BS_CO_RLE].sShortName =  "BSS_72"
	g_BS_Listings[BS_CO_RLE].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_RLE].OnlinePriceStat =  SM_RLE_PRICE
	g_BS_Listings[BS_CO_RLE].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_RLE].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_PET].sLongName =  "BSS_73"
	g_BS_Listings[BS_CO_PET].sShortName =  "BSS_74"
	g_BS_Listings[BS_CO_PET].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_PET].OnlinePriceStat =  SM_PET_PRICE
	g_BS_Listings[BS_CO_PET].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_PET].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_ECL].sLongName =  "BSS_75"
	g_BS_Listings[BS_CO_ECL].sShortName =  "BSS_76"
	g_BS_Listings[BS_CO_ECL].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_ECL].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_ECL].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_CDY].sLongName =  "BSS_77"
	g_BS_Listings[BS_CO_CDY].sShortName =  "BSS_78"
	g_BS_Listings[BS_CO_CDY].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_CDY].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_CDY].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_BGR].sLongName =  "BSS_79"
	g_BS_Listings[BS_CO_BGR].sShortName =  "BSS_80"
	g_BS_Listings[BS_CO_BGR].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_BGR].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_BGR].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_CLK].sLongName =  "BSS_81"
	g_BS_Listings[BS_CO_CLK].sShortName =  "BSS_82"
	g_BS_Listings[BS_CO_CLK].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_CLK].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_CLK].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_GLY].sLongName =  "BSS_83"
	g_BS_Listings[BS_CO_GLY].sShortName =  "BSS_84"
	g_BS_Listings[BS_CO_GLY].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_GLY].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_GLY].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_BTE].sLongName =  "BSS_85"
	g_BS_Listings[BS_CO_BTE].sShortName =  "BSS_86"
	g_BS_Listings[BS_CO_BTE].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_BTE].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_BTE].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_BEN].sLongName =  "BSS_87"
	g_BS_Listings[BS_CO_BEN].sShortName =  "BSS_88"
	g_BS_Listings[BS_CO_BEN].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_BEN].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_BEN].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_BEO].sLongName =  "BSS_89"
	g_BS_Listings[BS_CO_BEO].sShortName =  "BSS_90"
	g_BS_Listings[BS_CO_BEO].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_BEO].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_BEO].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_FLC].sLongName =  "BSS_91"
	g_BS_Listings[BS_CO_FLC].sShortName =  "BSS_92"
	g_BS_Listings[BS_CO_FLC].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_FLC].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_FLC].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_PRO].sLongName =  "BSS_93"
	g_BS_Listings[BS_CO_PRO].sShortName =  "BSS_94"
	g_BS_Listings[BS_CO_PRO].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_PRO].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_PRO].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_XER].sLongName =  "BSS_95"
	g_BS_Listings[BS_CO_XER].sShortName =  "BSS_96"
	g_BS_Listings[BS_CO_XER].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_XER].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_XER].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_UNI].sLongName =  "BSS_97"
	g_BS_Listings[BS_CO_UNI].sShortName =  "BSS_98"
	g_BS_Listings[BS_CO_UNI].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_UNI].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_UNI].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_KRP].sLongName =  "BSS_99"
	g_BS_Listings[BS_CO_KRP].sShortName =  "BSS_100"
	g_BS_Listings[BS_CO_KRP].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_KRP].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_KRP].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_LFB].sLongName =  "BSS_101"
	g_BS_Listings[BS_CO_LFB].sShortName =  "BSS_102"
	g_BS_Listings[BS_CO_LFB].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_LFB].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_LFB].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_GBL].sLongName =  "BSS_103"
	g_BS_Listings[BS_CO_GBL].sShortName =  "BSS_104"
	g_BS_Listings[BS_CO_GBL].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_GBL].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_GBL].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_FUS].sLongName =  "BSS_105"
	g_BS_Listings[BS_CO_FUS].sShortName =  "BSS_106"
	g_BS_Listings[BS_CO_FUS].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_FUS].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_FUS].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_ADI].sLongName =  "BSS_107"
	g_BS_Listings[BS_CO_ADI].sShortName =  "BSS_108"
	g_BS_Listings[BS_CO_ADI].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_ADI].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_ADI].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_PED].sLongName =  "BSS_109"
	g_BS_Listings[BS_CO_PED].sShortName =  "BSS_110"
	g_BS_Listings[BS_CO_PED].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_PED].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_PED].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_REC].sLongName =  "BSS_111"
	g_BS_Listings[BS_CO_REC].sShortName =  "BSS_112"
	g_BS_Listings[BS_CO_REC].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_REC].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_REC].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_CHC].sLongName =  "BSS_113"
	g_BS_Listings[BS_CO_CHC].sShortName =  "BSS_114"
	g_BS_Listings[BS_CO_CHC].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_CHC].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_CHC].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_CHD].sLongName =  "BSS_115"
	g_BS_Listings[BS_CO_CHD].sShortName =  "BSS_116"
	g_BS_Listings[BS_CO_CHD].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_CHD].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_CHD].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_CHE].sLongName =  "BSS_117"
	g_BS_Listings[BS_CO_CHE].sShortName =  "BSS_118"
	g_BS_Listings[BS_CO_CHE].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_CHE].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_CHE].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_CHF].sLongName =  "BSS_119"
	g_BS_Listings[BS_CO_CHF].sShortName =  "BSS_120"
	g_BS_Listings[BS_CO_CHF].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_CHF].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_CHF].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_FLM].sLongName =  "BSS_121"
	g_BS_Listings[BS_CO_FLM].sShortName =  "BSS_122"
	g_BS_Listings[BS_CO_FLM].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_FLM].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_FLM].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_PHM].sLongName =  "BSS_123"
	g_BS_Listings[BS_CO_PHM].sShortName =  "BSS_124"
	g_BS_Listings[BS_CO_PHM].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_PHM].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_PHM].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_GOP].sLongName =  "BSS_125"
	g_BS_Listings[BS_CO_GOP].sShortName =  "BSS_126"
	g_BS_Listings[BS_CO_GOP].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_GOP].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_GOP].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_BAN].sLongName =  "BSS_127"
	g_BS_Listings[BS_CO_BAN].sShortName =  "BSS_128"
	g_BS_Listings[BS_CO_BAN].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_BAN].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_BAN].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_VON].sLongName =  "BSS_129"
	g_BS_Listings[BS_CO_VON].sShortName =  "BSS_130"
	g_BS_Listings[BS_CO_VON].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_VON].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_VON].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_WTA].sLongName =  "BSS_131"
	g_BS_Listings[BS_CO_WTA].sShortName =  "BSS_132"
	g_BS_Listings[BS_CO_WTA].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_WTA].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_WTA].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_WTB].sLongName =  "BSS_133"
	g_BS_Listings[BS_CO_WTB].sShortName =  "BSS_134"
	g_BS_Listings[BS_CO_WTB].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_WTB].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_WTB].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_FGA].sLongName =  "BSS_135"
	g_BS_Listings[BS_CO_FGA].sShortName =  "BSS_136"
	g_BS_Listings[BS_CO_FGA].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_FGA].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_FGA].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_FGB].sLongName =  "BSS_137"
	g_BS_Listings[BS_CO_FGB].sShortName =  "BSS_138"
	g_BS_Listings[BS_CO_FGB].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_FGB].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_FGB].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_MAX].sLongName =  "BSS_139"
	g_BS_Listings[BS_CO_MAX].sShortName =  "BSS_140"
	g_BS_Listings[BS_CO_MAX].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_MAX].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_MAX].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_VEN].sLongName =  "BSS_141"
	g_BS_Listings[BS_CO_VEN].sShortName =  "BSS_142"
	g_BS_Listings[BS_CO_VEN].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_VEN].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_VEN].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_RLC].sLongName =  "BSS_143"
	g_BS_Listings[BS_CO_RLC].sShortName =  "BSS_144"
	g_BS_Listings[BS_CO_RLC].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_RLC].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_RLC].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_RLB].sLongName =  "BSS_145"
	g_BS_Listings[BS_CO_RLB].sShortName =  "BSS_146"
	g_BS_Listings[BS_CO_RLB].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_RLB].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_RLB].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_GAS].sLongName =  "BSS_147"
	g_BS_Listings[BS_CO_GAS].sShortName =  "BSS_148"
	g_BS_Listings[BS_CO_GAS].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_GAS].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_GAS].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_GRU].sLongName =  "BSS_149"
	g_BS_Listings[BS_CO_GRU].sShortName =  "BSS_150"
	g_BS_Listings[BS_CO_GRU].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_GRU].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_GRU].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_PMP].sLongName =  "BSS_151"
	g_BS_Listings[BS_CO_PMP].sShortName =  "BSS_152"
	g_BS_Listings[BS_CO_PMP].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_PMP].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_PMP].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_MHO].sLongName =  "BSS_153"
	g_BS_Listings[BS_CO_MHO].sShortName =  "BSS_154"
	g_BS_Listings[BS_CO_MHO].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_MHO].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_MHO].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_FCD].sLongName =  "BSS_155"
	g_BS_Listings[BS_CO_FCD].sShortName =  "BSS_156"
	g_BS_Listings[BS_CO_FCD].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_FCD].OnlinePriceStat =  SM_FCD_PRICE
	g_BS_Listings[BS_CO_FCD].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_FCD].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_CTM].sLongName =  "BSS_157"
	g_BS_Listings[BS_CO_CTM].sShortName =  "BSS_158"
	g_BS_Listings[BS_CO_CTM].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_CTM].OnlinePriceStat =  SM_CTM_PRICE
	g_BS_Listings[BS_CO_CTM].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_CTM].fStockPriceChangeSensitivity = 0.02 

	g_BS_Listings[BS_CO_IOU].sLongName =  "BSS_159"
	g_BS_Listings[BS_CO_IOU].sShortName =  "BSS_160"
	g_BS_Listings[BS_CO_IOU].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_IOU].OnlinePriceStat =  SM_IOU_PRICE
	g_BS_Listings[BS_CO_IOU].fCurrentPrice = 1 
	g_BS_Listings[BS_CO_IOU].fStockPriceChangeSensitivity = 0.02 

/* Uncomment the following when the stats are in place
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_BRU].uploadTo 	= 	SM_SITEVISITBRU //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_VAP].uploadTo 	= 	SM_SITEVISITVAP //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_MAI].uploadTo 	= 	SM_SITEVISITMAI //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_PFI].uploadTo 	= 	SM_SITEVISITPFI //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_HVY].uploadTo 	= 	SM_SITEVISITHVY //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_MTL].uploadTo 	= 	SM_SITEVISITMTL //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_GUN1].uploadTo 	= 	SM_SITEVISITGUN1 //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_GUN2].uploadTo 	= 	SM_SITEVISITGUN2 //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_GUN3].uploadTo 	= 	SM_SITEVISITGUN3 //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_SPK].uploadTo 	= 	SM_SITEVISITSPK //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHI].uploadTo 	= 	SM_SITEVISITCHI //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_WTR].uploadTo 	= 	SM_SITEVISITWTR //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_PIS].uploadTo 	= 	SM_SITEVISITPIS //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_DAI].uploadTo 	= 	SM_SITEVISITDAI //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_MET].uploadTo 	= 	SM_SITEVISITMET //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_WZL].uploadTo 	= 	SM_SITEVISITWZL //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_CNT].uploadTo 	= 	SM_SITEVISITCNT //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_LTA].uploadTo 	= 	SM_SITEVISITLTA //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_VIG].uploadTo 	= 	SM_SITEVISITVIG //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_CRE].uploadTo 	= 	SM_SITEVISITCRE //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_BDG].uploadTo 	= 	SM_SITEVISITBDG //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_WIZ].uploadTo 	= 	SM_SITEVISITWIZ //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_TNK].uploadTo 	= 	SM_SITEVISITTNK //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_FRT].uploadTo 	= 	SM_SITEVISITFRT //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_BTR].uploadTo 	= 	SM_SITEVISITBTR //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_RON].uploadTo 	= 	SM_SITEVISITRON //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_LBL].uploadTo 	= 	SM_SITEVISITLBL //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_BLE].uploadTo 	= 	SM_SITEVISITBLE //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_LOM].uploadTo 	= 	SM_SITEVISITLOM //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_BNC].uploadTo 	= 	SM_SITEVISITBNC //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_TXI].uploadTo 	= 	SM_SITEVISITTXI //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_GLS].uploadTo 	= 	SM_SITEVISITGLS //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_LYB].uploadTo 	= 	SM_SITEVISITLYB //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_LYC].uploadTo 	= 	SM_SITEVISITLYC //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_PRT].uploadTo 	= 	SM_SITEVISITPRT //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHA].uploadTo 	= 	SM_SITEVISITCHA //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHB].uploadTo 	= 	SM_SITEVISITCHB //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_RLE].uploadTo 	= 	SM_SITEVISITRLE //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_PET].uploadTo 	= 	SM_SITEVISITPET //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_FCD].uploadTo 	= 	SM_SITEVISITFCD //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_CTM].uploadTo 	= 	SM_SITEVISITCTM //WebSiteVisits
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_IOU].uploadTo 	= 	SM_SITEVISITIOU //WebSiteVisits
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BRU].uploadTo 	= 	SM_BRAVECDESTBRU //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_VAP].uploadTo 	= 	SM_BRAVECDESTVAP //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_MAI].uploadTo 	= 	SM_BRAVECDESTMAI //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PFI].uploadTo 	= 	SM_BRAVECDESTPFI //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_HVY].uploadTo 	= 	SM_BRAVECDESTHVY //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_MTL].uploadTo 	= 	SM_BRAVECDESTMTL //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GUN1].uploadTo 	= 	SM_BRAVECDESTGUN1 //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GUN2].uploadTo 	= 	SM_BRAVECDESTGUN2 //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GUN3].uploadTo 	= 	SM_BRAVECDESTGUN3 //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_SPK].uploadTo 	= 	SM_BRAVECDESTSPK //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHI].uploadTo 	= 	SM_BRAVECDESTCHI //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_WTR].uploadTo 	= 	SM_BRAVECDESTWTR //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PIS].uploadTo 	= 	SM_BRAVECDESTPIS //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_DAI].uploadTo 	= 	SM_BRAVECDESTDAI //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_MET].uploadTo 	= 	SM_BRAVECDESTMET //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_WZL].uploadTo 	= 	SM_BRAVECDESTWZL //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CNT].uploadTo 	= 	SM_BRAVECDESTCNT //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LTA].uploadTo 	= 	SM_BRAVECDESTLTA //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_VIG].uploadTo 	= 	SM_BRAVECDESTVIG //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CRE].uploadTo 	= 	SM_BRAVECDESTCRE //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BDG].uploadTo 	= 	SM_BRAVECDESTBDG //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_WIZ].uploadTo 	= 	SM_BRAVECDESTWIZ //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_TNK].uploadTo 	= 	SM_BRAVECDESTTNK //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FRT].uploadTo 	= 	SM_BRAVECDESTFRT //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BTR].uploadTo 	= 	SM_BRAVECDESTBTR //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_RON].uploadTo 	= 	SM_BRAVECDESTRON //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LBL].uploadTo 	= 	SM_BRAVECDESTLBL //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BLE].uploadTo 	= 	SM_BRAVECDESTBLE //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LOM].uploadTo 	= 	SM_BRAVECDESTLOM //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BNC].uploadTo 	= 	SM_BRAVECDESTBNC //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_TXI].uploadTo 	= 	SM_BRAVECDESTTXI //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GLS].uploadTo 	= 	SM_BRAVECDESTGLS //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LYB].uploadTo 	= 	SM_BRAVECDESTLYB //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LYC].uploadTo 	= 	SM_BRAVECDESTLYC //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PRT].uploadTo 	= 	SM_BRAVECDESTPRT //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHA].uploadTo 	= 	SM_BRAVECDESTCHA //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHB].uploadTo 	= 	SM_BRAVECDESTCHB //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_RLE].uploadTo 	= 	SM_BRAVECDESTRLE //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PET].uploadTo 	= 	SM_BRAVECDESTPET //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FCD].uploadTo 	= 	SM_BRAVECDESTFCD //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CTM].uploadTo 	= 	SM_BRAVECDESTCTM //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_IOU].uploadTo 	= 	SM_BRAVECDESTIOU //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_BRU].uploadTo 	= 	SM_BOARDDESTBRU //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_VAP].uploadTo 	= 	SM_BOARDDESTVAP //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_MAI].uploadTo 	= 	SM_BOARDDESTMAI //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_PFI].uploadTo 	= 	SM_BOARDDESTPFI //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_HVY].uploadTo 	= 	SM_BOARDDESTHVY //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_MTL].uploadTo 	= 	SM_BOARDDESTMTL //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_GUN1].uploadTo 	= 	SM_BOARDDESTGUN1 //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_GUN2].uploadTo 	= 	SM_BOARDDESTGUN2 //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_GUN3].uploadTo 	= 	SM_BOARDDESTGUN3 //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_SPK].uploadTo 	= 	SM_BOARDDESTSPK //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHI].uploadTo 	= 	SM_BOARDDESTCHI //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_WTR].uploadTo 	= 	SM_BOARDDESTWTR //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_PIS].uploadTo 	= 	SM_BOARDDESTPIS //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_DAI].uploadTo 	= 	SM_BOARDDESTDAI //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_MET].uploadTo 	= 	SM_BOARDDESTMET //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_WZL].uploadTo 	= 	SM_BOARDDESTWZL //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_CNT].uploadTo 	= 	SM_BOARDDESTCNT //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_LTA].uploadTo 	= 	SM_BOARDDESTLTA //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_VIG].uploadTo 	= 	SM_BOARDDESTVIG //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_CRE].uploadTo 	= 	SM_BOARDDESTCRE //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_BDG].uploadTo 	= 	SM_BOARDDESTBDG //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_WIZ].uploadTo 	= 	SM_BOARDDESTWIZ //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_TNK].uploadTo 	= 	SM_BOARDDESTTNK //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_FRT].uploadTo 	= 	SM_BOARDDESTFRT //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_BTR].uploadTo 	= 	SM_BOARDDESTBTR //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_RON].uploadTo 	= 	SM_BOARDDESTRON //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_LBL].uploadTo 	= 	SM_BOARDDESTLBL //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_BLE].uploadTo 	= 	SM_BOARDDESTBLE //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_LOM].uploadTo 	= 	SM_BOARDDESTLOM //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_BNC].uploadTo 	= 	SM_BOARDDESTBNC //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_TXI].uploadTo 	= 	SM_BOARDDESTTXI //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_GLS].uploadTo 	= 	SM_BOARDDESTGLS //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_LYB].uploadTo 	= 	SM_BOARDDESTLYB //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_LYC].uploadTo 	= 	SM_BOARDDESTLYC //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_PRT].uploadTo 	= 	SM_BOARDDESTPRT //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHA].uploadTo 	= 	SM_BOARDDESTCHA //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHB].uploadTo 	= 	SM_BOARDDESTCHB //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_RLE].uploadTo 	= 	SM_BOARDDESTRLE //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_PET].uploadTo 	= 	SM_BOARDDESTPET //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_FCD].uploadTo 	= 	SM_BOARDDESTFCD //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_CTM].uploadTo 	= 	SM_BOARDDESTCTM //BillboardsDestroyed
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_IOU].uploadTo 	= 	SM_BOARDDESTIOU //BillboardsDestroyed
	g_BS_Modifiers[BSMF_VECBUY_FOR_BRU].uploadTo 	= 	SM_VECBUYBRU //VehiclesOfTypeBought
	g_BS_Modifiers[BSMF_VECBUY_FOR_VAP].uploadTo 	= 	SM_VECBUYVAP //VehiclesOfTypeBought
	g_BS_Modifiers[BSMF_VECBUY_FOR_MAI].uploadTo 	= 	SM_VECBUYMAI //VehiclesOfTypeBought
	g_BS_Modifiers[BSMF_VECBUY_FOR_PFI].uploadTo 	= 	SM_VECBUYPFI //VehiclesOfTypeBought
	g_BS_Modifiers[BSMF_VECBUY_FOR_HVY].uploadTo 	= 	SM_VECBUYHVY //VehiclesOfTypeBought
	g_BS_Modifiers[BSMF_VECBUY_FOR_MTL].uploadTo 	= 	SM_VECBUYMTL //VehiclesOfTypeBought
	g_BS_Modifiers[BSMF_DISTDRIV_FOR_BRU].uploadTo 	= 	SM_DISTDRIVBRU //DistanceDrivenInType
	g_BS_Modifiers[BSMF_DISTDRIV_FOR_VAP].uploadTo 	= 	SM_DISTDRIVVAP //DistanceDrivenInType
	g_BS_Modifiers[BSMF_DISTDRIV_FOR_MAI].uploadTo 	= 	SM_DISTDRIVMAI //DistanceDrivenInType
	g_BS_Modifiers[BSMF_DISTDRIV_FOR_PFI].uploadTo 	= 	SM_DISTDRIVPFI //DistanceDrivenInType
	g_BS_Modifiers[BSMF_DISTDRIV_FOR_HVY].uploadTo 	= 	SM_DISTDRIVHVY //DistanceDrivenInType
	g_BS_Modifiers[BSMF_DISTDRIV_FOR_MTL].uploadTo 	= 	SM_DISTDRIVMTL //DistanceDrivenInType
	g_BS_Modifiers[BSMF_VECMOD_FOR_BRU].uploadTo 	= 	SM_VECMODBRU //VehicleModification
	g_BS_Modifiers[BSMF_VECMOD_FOR_VAP].uploadTo 	= 	SM_VECMODVAP //VehicleModification
	g_BS_Modifiers[BSMF_VECMOD_FOR_MAI].uploadTo 	= 	SM_VECMODMAI //VehicleModification
	g_BS_Modifiers[BSMF_VECMOD_FOR_PFI].uploadTo 	= 	SM_VECMODPFI //VehicleModification
	g_BS_Modifiers[BSMF_VECMOD_FOR_HVY].uploadTo 	= 	SM_VECMODHVY //VehicleModification
	g_BS_Modifiers[BSMF_VECMOD_FOR_MTL].uploadTo 	= 	SM_VECMODMTL //VehicleModification
	g_BS_Modifiers[BSMF_STUNTJUMP_FOR_BRU].uploadTo 	= 	SM_STUNTJUMPBRU //StuntJumpsCompleatedInVehilceType
	g_BS_Modifiers[BSMF_STUNTJUMP_FOR_MAI].uploadTo 	= 	SM_STUNTJUMPMAI //StuntJumpsCompleatedInVehilceType
	g_BS_Modifiers[BSMF_STUNTJUMP_FOR_PFI].uploadTo 	= 	SM_STUNTJUMPPFI //StuntJumpsCompleatedInVehilceType
	g_BS_Modifiers[BSMF_STUNTJUMP_FOR_HVY].uploadTo 	= 	SM_STUNTJUMPHVY //StuntJumpsCompleatedInVehilceType
	g_BS_Modifiers[BSMF_STUNTJUMP_FOR_MTL].uploadTo 	= 	SM_STUNTJUMPMTL //StuntJumpsCompleatedInVehilceType
	g_BS_Modifiers[BSMF_VECTHEFT_FOR_BRU].uploadTo 	= 	SM_VECTHEFTBRU //VehicleOfTypeStolen
	g_BS_Modifiers[BSMF_VECTHEFT_FOR_VAP].uploadTo 	= 	SM_VECTHEFTVAP //VehicleOfTypeStolen
	g_BS_Modifiers[BSMF_VECTHEFT_FOR_MAI].uploadTo 	= 	SM_VECTHEFTMAI //VehicleOfTypeStolen
	g_BS_Modifiers[BSMF_VECTHEFT_FOR_PFI].uploadTo 	= 	SM_VECTHEFTPFI //VehicleOfTypeStolen
	g_BS_Modifiers[BSMF_VECTHEFT_FOR_HVY].uploadTo 	= 	SM_VECTHEFTHVY //VehicleOfTypeStolen
	g_BS_Modifiers[BSMF_VECTHEFT_FOR_MTL].uploadTo 	= 	SM_VECTHEFTMTL //VehicleOfTypeStolen
	g_BS_Modifiers[BSMF_VECDAMAGE_FOR_BRU].uploadTo 	= 	SM_VECDAMAGEBRU //DamageDoneToVehicleType
	g_BS_Modifiers[BSMF_VECDAMAGE_FOR_VAP].uploadTo 	= 	SM_VECDAMAGEVAP //DamageDoneToVehicleType
	g_BS_Modifiers[BSMF_VECDAMAGE_FOR_MAI].uploadTo 	= 	SM_VECDAMAGEMAI //DamageDoneToVehicleType
	g_BS_Modifiers[BSMF_VECDAMAGE_FOR_PFI].uploadTo 	= 	SM_VECDAMAGEPFI //DamageDoneToVehicleType
	g_BS_Modifiers[BSMF_VECDAMAGE_FOR_HVY].uploadTo 	= 	SM_VECDAMAGEHVY //DamageDoneToVehicleType
	g_BS_Modifiers[BSMF_VECDAMAGE_FOR_MTL].uploadTo 	= 	SM_VECDAMAGEMTL //DamageDoneToVehicleType
	g_BS_Modifiers[BSMF_VECKILL_FOR_BRU].uploadTo 	= 	SM_VECKILLBRU //PeopleKilledWithVehicle
	g_BS_Modifiers[BSMF_VECKILL_FOR_VAP].uploadTo 	= 	SM_VECKILLVAP //PeopleKilledWithVehicle
	g_BS_Modifiers[BSMF_VECKILL_FOR_MAI].uploadTo 	= 	SM_VECKILLMAI //PeopleKilledWithVehicle
	g_BS_Modifiers[BSMF_VECKILL_FOR_PFI].uploadTo 	= 	SM_VECKILLPFI //PeopleKilledWithVehicle
	g_BS_Modifiers[BSMF_VECKILL_FOR_HVY].uploadTo 	= 	SM_VECKILLHVY //PeopleKilledWithVehicle
	g_BS_Modifiers[BSMF_VECKILL_FOR_MTL].uploadTo 	= 	SM_VECKILLMTL //PeopleKilledWithVehicle
	g_BS_Modifiers[BSMF_TAXIUSE].uploadTo 	= 	SM_TAXIUSE //TaxisAndUseOfPublicTransport
	g_BS_Modifiers[BSMF_WEPUSED_FOR_GUN1].uploadTo 	= 	SM_WEPUSEDGUN1 //WeaponTypeUsed
	g_BS_Modifiers[BSMF_WEPUSED_FOR_GUN2].uploadTo 	= 	SM_WEPUSEDGUN2 //WeaponTypeUsed
	g_BS_Modifiers[BSMF_WEPUSED_FOR_GUN3].uploadTo 	= 	SM_WEPUSEDGUN3 //WeaponTypeUsed
	g_BS_Modifiers[BSMF_WEPTAKEN_FOR_GUN1].uploadTo 	= 	SM_WEPTAKENGUN1 //WeaponTypeTaken
	g_BS_Modifiers[BSMF_WEPTAKEN_FOR_GUN2].uploadTo 	= 	SM_WEPTAKENGUN2 //WeaponTypeTaken
	g_BS_Modifiers[BSMF_WEPTAKEN_FOR_GUN3].uploadTo 	= 	SM_WEPTAKENGUN3 //WeaponTypeTaken
	g_BS_Modifiers[BSMF_WEPCOPKILL_FOR_GUN1].uploadTo 	= 	SM_WEPCOPKILLGUN1 //CopsKilledWithWeaponType
	g_BS_Modifiers[BSMF_WEPCOPKILL_FOR_GUN2].uploadTo 	= 	SM_WEPCOPKILLGUN2 //CopsKilledWithWeaponType
	g_BS_Modifiers[BSMF_WEPCOPKILL_FOR_GUN3].uploadTo 	= 	SM_WEPCOPKILLGUN3 //CopsKilledWithWeaponType
	g_BS_Modifiers[BSMF_WEPCRIMKILL_FOR_GUN1].uploadTo 	= 	SM_WEPCRIMKILLGUN1 //CriminalsKilledWithWeaponType
	g_BS_Modifiers[BSMF_WEPCRIMKILL_FOR_GUN2].uploadTo 	= 	SM_WEPCRIMKILLGUN2 //CriminalsKilledWithWeaponType
	g_BS_Modifiers[BSMF_WEPCRIMKILL_FOR_GUN3].uploadTo 	= 	SM_WEPCRIMKILLGUN3 //CriminalsKilledWithWeaponType
	g_BS_Modifiers[BSMF_WEPCIVKILL_FOR_GUN1].uploadTo 	= 	SM_WEPCIVKILLGUN1 //CiviliansKilledWithWeaponType
	g_BS_Modifiers[BSMF_WEPCIVKILL_FOR_GUN2].uploadTo 	= 	SM_WEPCIVKILLGUN2 //CiviliansKilledWithWeaponType
	g_BS_Modifiers[BSMF_WEPCIVKILL_FOR_GUN3].uploadTo 	= 	SM_WEPCIVKILLGUN3 //CiviliansKilledWithWeaponType
	g_BS_Modifiers[BSMF_WEPCRIMCARKILL_FOR_GUN1].uploadTo 	= 	SM_WPCRMCRKLLGN1 //CriminalVehiclesKilledWithWeapon
	g_BS_Modifiers[BSMF_WEPCRIMCARKILL_FOR_GUN2].uploadTo 	= 	SM_WPCRMCRKLLGN2 //CriminalVehiclesKilledWithWeapon
	g_BS_Modifiers[BSMF_WEPCRIMCARKILL_FOR_GUN3].uploadTo 	= 	SM_WPCRMCRKLLGN3 //CriminalVehiclesKilledWithWeapon
	g_BS_Modifiers[BSMF_WEPPLAKILL_FOR_GUN1].uploadTo 	= 	SM_WEPPLAKILLGUN1 //TimesPlayerIsKilledWithWeaponType
	g_BS_Modifiers[BSMF_WEPPLAKILL_FOR_GUN2].uploadTo 	= 	SM_WEPPLAKILLGUN2 //TimesPlayerIsKilledWithWeaponType
	g_BS_Modifiers[BSMF_WEPPLAKILL_FOR_GUN3].uploadTo 	= 	SM_WEPPLAKILLGUN3 //TimesPlayerIsKilledWithWeaponType
	g_BS_Modifiers[BSMF_VENDUSED_FOR_SPK].uploadTo 	= 	SM_VENDUSEDSPK //VendingMachieneOftypeUsed
	g_BS_Modifiers[BSMF_VENDUSED_FOR_CHI].uploadTo 	= 	SM_VENDUSEDCHI //VendingMachieneOftypeUsed
	g_BS_Modifiers[BSMF_VENDUSED_FOR_WTR].uploadTo 	= 	SM_VENDUSEDWTR //VendingMachieneOftypeUsed
	g_BS_Modifiers[BSMF_FITDOWN].uploadTo 	= 	SM_FITDOWN //Reducedfitness
	g_BS_Modifiers[BSMF_FITUP].uploadTo 	= 	SM_FITUP //Increasedfitness
	g_BS_Modifiers[BSMF_NEWSSTALLDAM].uploadTo 	= 	SM_NEWSSTALLDAM //DamageToNewsagentStalls
	g_BS_Modifiers[BSMF_HPEDKILL].uploadTo 	= 	SM_HPEDKILL //HealthyPeopleKilled
	g_BS_Modifiers[BSMF_PUBCLUBVISIT].uploadTo 	= 	SM_PUBCLUBVISIT //PubandClubvisits
	g_BS_Modifiers[BSMF_TIMESDRUNK].uploadTo 	= 	SM_TIMESDRUNK //TimesDrunk
	g_BS_Modifiers[BSMF_FRNDTOPUB].uploadTo 	= 	SM_FRNDTOPUB //FriendsTakenToPubsclubs
	g_BS_Modifiers[BSMF_DRNKCRIME].uploadTo 	= 	SM_DRNKCRIME //DrunkenCrimes
	g_BS_Modifiers[BSMF_PLYRAMPAGE].uploadTo 	= 	SM_PLYRAMPAGE //RampagesComplete
	g_BS_Modifiers[BSMF_HWANTEDTIME].uploadTo 	= 	SM_HWANTEDTIME //GainedFourstars
	g_BS_Modifiers[BSMF_NOWNTIME].uploadTo 	= 	SM_NOWNTIME //TimeWithNoWantedRating
	g_BS_Modifiers[BSMF_LISTENTIME_FOR_MET].uploadTo 	= 	SM_LISTENTIMEMET //RadioStationsListenedTo
	g_BS_Modifiers[BSMF_LISTENTIME_FOR_WZL].uploadTo 	= 	SM_LISTENTIMEWZL //RadioStationsListenedTo
	g_BS_Modifiers[BSMF_LISTENTIME_FOR_CNT].uploadTo 	= 	SM_LISTENTIMECNT //RadioStationsListenedTo
	g_BS_Modifiers[BSMF_ZITID_FOR_MET].uploadTo 	= 	SM_ZITIDMET //SongsIDedWithZIT
	g_BS_Modifiers[BSMF_ZITID_FOR_WZL].uploadTo 	= 	SM_ZITIDWZL //SongsIDedWithZIT
	g_BS_Modifiers[BSMF_ZITID_FOR_CNT].uploadTo 	= 	SM_ZITIDCNT //SongsIDedWithZIT
	g_BS_Modifiers[BSMF_ZITID_FOR_PRT].uploadTo 	= 	SM_ZITIDPRT //SongsIDedWithZIT
	g_BS_Modifiers[BSMF_RADSWITCH_FOR_MET].uploadTo 	= 	SM_RADSWITCHMET //TimesChangedAwayFromRadiostation
	g_BS_Modifiers[BSMF_RADSWITCH_FOR_WZL].uploadTo 	= 	SM_RADSWITCHWZL //TimesChangedAwayFromRadiostation
	g_BS_Modifiers[BSMF_RADSWITCH_FOR_CNT].uploadTo 	= 	SM_RADSWITCHCNT //TimesChangedAwayFromRadiostation
	g_BS_Modifiers[BSMF_VEC].uploadTo 	= 	SM_VEC //PublicVehiclesDamagedOrDestroyed
	g_BS_Modifiers[BSMF_PARAJUMP].uploadTo 	= 	SM_PARAJUMP //ParachuteJumpsBaseJumps
	g_BS_Modifiers[BSMF_WHEELIES].uploadTo 	= 	SM_WHEELIES //Wheelies
	g_BS_Modifiers[BSMF_STOPPIES].uploadTo 	= 	SM_STOPPIES //Stoppies
	g_BS_Modifiers[BSMF_ONFIRETIME].uploadTo 	= 	SM_ONFIRETIME //BeingOnFireTime
	g_BS_Modifiers[BSMF_VECBAIL].uploadTo 	= 	SM_VECBAIL //BailsFromVehicle
	g_BS_Modifiers[BSMF_FIREVECBAIL].uploadTo 	= 	SM_FIREVECBAIL //BailsFromVehicleWhenOnFire
	g_BS_Modifiers[BSMF_PARADEATH].uploadTo 	= 	SM_PARADEATH //DeathWhenParachuting
	g_BS_Modifiers[BSMF_CRASHDEATH].uploadTo 	= 	SM_CRASHDEATH //DeathsFromCrashingVehicles
	g_BS_Modifiers[BSMF_PHONEOPENTIME_FOR_BDG].uploadTo 	= 	SM_PHONEOPENTIMEBDG //PhonePopularity
	g_BS_Modifiers[BSMF_PHONEOPENTIME_FOR_WIZ].uploadTo 	= 	SM_PHONEOPENTIMEWIZ //PhonePopularity
	g_BS_Modifiers[BSMF_PHONEOPENTIME_FOR_TNK].uploadTo 	= 	SM_PHONEOPENTIMETNK //PhonePopularity
	g_BS_Modifiers[BSMF_CALLTIME_FOR_BDG].uploadTo 	= 	SM_CALLTIMEBDG //PhoneUseCalls
	g_BS_Modifiers[BSMF_CALLTIME_FOR_WIZ].uploadTo 	= 	SM_CALLTIMEWIZ //PhoneUseCalls
	g_BS_Modifiers[BSMF_CALLTIME_FOR_TNK].uploadTo 	= 	SM_CALLTIMETNK //PhoneUseCalls
	g_BS_Modifiers[BSMF_TEXTSUSED_FOR_BDG].uploadTo 	= 	SM_TEXTSUSEDBDG //PhoneUseTexts
	g_BS_Modifiers[BSMF_TEXTSUSED_FOR_WIZ].uploadTo 	= 	SM_TEXTSUSEDWIZ //PhoneUseTexts
	g_BS_Modifiers[BSMF_TEXTSUSED_FOR_TNK].uploadTo 	= 	SM_TEXTSUSEDTNK //PhoneUseTexts
	g_BS_Modifiers[BSMF_CALLREFUSED_FOR_BDG].uploadTo 	= 	SM_CALLREFUSEDBDG //PhoneCallsIgnored
	g_BS_Modifiers[BSMF_CALLREFUSED_FOR_WIZ].uploadTo 	= 	SM_CALLREFUSEDWIZ //PhoneCallsIgnored
	g_BS_Modifiers[BSMF_CALLREFUSED_FOR_TNK].uploadTo 	= 	SM_CALLREFUSEDTNK //PhoneCallsIgnored
	g_BS_Modifiers[BSMF_OTPLTIME_FOR_BDG].uploadTo 	= 	SM_OTPLTIMEBDG //TimeUsingOtherPlayerCharacters
	g_BS_Modifiers[BSMF_OTPLTIME_FOR_WIZ].uploadTo 	= 	SM_OTPLTIMEWIZ //TimeUsingOtherPlayerCharacters
	g_BS_Modifiers[BSMF_OTPLTIME_FOR_TNK].uploadTo 	= 	SM_OTPLTIMETNK //TimeUsingOtherPlayerCharacters
	g_BS_Modifiers[BSMF_LAPTOPTIME].uploadTo 	= 	SM_LAPTOPTIME //TimeSpentOnLaptops
	g_BS_Modifiers[BSMF_PHONEPOP_FOR_FRT].uploadTo 	= 	SM_PHONEPOPFRT //PhonePopularity
	g_BS_Modifiers[BSMF_PHONEPOP_FOR_BTR].uploadTo 	= 	SM_PHONEPOPBTR //PhonePopularity
	g_BS_Modifiers[BSMF_PHONEPOP_FOR_FCD].uploadTo 	= 	SM_PHONEPOPFCD //PhonePopularity
	g_BS_Modifiers[BSMF_APPSDL_FOR_FRT].uploadTo 	= 	SM_APPSDLFRT //PhoneAppsDownloaded
	g_BS_Modifiers[BSMF_APPSDL_FOR_BTR].uploadTo 	= 	SM_APPSDLBTR //PhoneAppsDownloaded
	g_BS_Modifiers[BSMF_INTERNETTIME_FOR_FRT].uploadTo 	= 	SM_INTERNETTIMEFRT //PhoneInternetUse
	g_BS_Modifiers[BSMF_INTERNETTIME_FOR_BTR].uploadTo 	= 	SM_INTERNETTIMEBTR //PhoneInternetUse
	g_BS_Modifiers[BSMF_INTERNETTIME_FOR_FCD].uploadTo 	= 	SM_INTERNETTIMEFCD //PhoneInternetUse
	g_BS_Modifiers[BSMF_APPTIME].uploadTo 	= 	SM_APPTIME //GTA5iphoneappUsage
	g_BS_Modifiers[BSMF_BROKENPHONE_FOR_FRT].uploadTo 	= 	SM_BROKENPHONEFRT //TimesKilledNeedNewPhone
	g_BS_Modifiers[BSMF_BROKENPHONE_FOR_BTR].uploadTo 	= 	SM_BROKENPHONEBTR //TimesKilledNeedNewPhone
	g_BS_Modifiers[BSMF_BROKENPHONE_FOR_FCD].uploadTo 	= 	SM_BROKENPHONEFCD //TimesKilledNeedNewPhone
	g_BS_Modifiers[BSMF_GAMEPLAYEDTIME].uploadTo 	= 	SM_GAMEPLAYEDTIME //TimeInGameConsolesPlayed
	g_BS_Modifiers[BSMF_COMPTIME].uploadTo 	= 	SM_COMPTIME //TimeSpentOnComputerDesktop
	g_BS_Modifiers[BSMF_JTKILLS].uploadTo 	= 	SM_JTKILLS //PeopleKilledAfterPlayingIngameVideoGamse
	g_BS_Modifiers[BSMF_COTANKDEST_FOR_RON].uploadTo 	= 	SM_COTANKDESTRON //CompetitorTakersDestroyed
	g_BS_Modifiers[BSMF_TANKERDEST_FOR_RON].uploadTo 	= 	SM_TANKERDESTRON //TankersDestroyed
	g_BS_Modifiers[BSMF_GARAGEDEST_FOR_RON].uploadTo 	= 	SM_GARAGEDESTRON //GaragesDestroyed
	g_BS_Modifiers[BSMF_SMINTUSED].uploadTo 	= 	SM_SMINTUSED //TimesTwitterFacebookIntergrationUsed
	g_BS_Modifiers[BSMF_GARAGEREPAIR].uploadTo 	= 	SM_GARAGEREPAIR //GarageRepairsMade
	g_BS_Modifiers[BSMF_GARAGEMONEY].uploadTo 	= 	SM_GARAGEMONEY //MoneySpentAtGarage
	g_BS_Modifiers[BSMF_MODBUYFAIL].uploadTo 	= 	SM_MODBUYFAIL //CarsModifiedThenNotBought
	g_BS_Modifiers[BSMF_MONEYGAINED_FOR_LOM].uploadTo 	= 	SM_MONEYGAINEDLOM //MoneyInPlayersAccount
	g_BS_Modifiers[BSMF_MONEYGAINED_FOR_BNC].uploadTo 	= 	SM_MONEYGAINEDBNC //MoneyInPlayersAccount
	g_BS_Modifiers[BSMF_MONEYGAINED_FOR_IOU].uploadTo 	= 	SM_MONEYGAINEDIOU //MoneyInPlayersAccount
	g_BS_Modifiers[BSMF_MONEYSPENT_FOR_LOM].uploadTo 	= 	SM_MONEYSPENTLOM //AmountOfMoneySPent
	g_BS_Modifiers[BSMF_MONEYSPENT_FOR_BNC].uploadTo 	= 	SM_MONEYSPENTBNC //AmountOfMoneySPent
	g_BS_Modifiers[BSMF_MONEYSPENT_FOR_IOU].uploadTo 	= 	SM_MONEYSPENTIOU //AmountOfMoneySPent
	g_BS_Modifiers[BSMF_TAXIUSE0].uploadTo 	= 	SM_TAXIUSE0 //TaxiRides
	g_BS_Modifiers[BSMF_TAXI_DESTROYED].uploadTo 	= 	SM_TAXI_DESTROYED //TaxisDestroyed
	g_BS_Modifiers[BSMF_BUSTED_FOR_GLS].uploadTo 	= 	SM_BUSTEDGLS //TimesBusted
	g_BS_Modifiers[BSMF_BUSTED_FOR_LYB].uploadTo 	= 	SM_BUSTEDLYB //TimesBusted
	g_BS_Modifiers[BSMF_BUSTED_FOR_LYC].uploadTo 	= 	SM_BUSTEDLYC //TimesBusted
	g_BS_Modifiers[BSMF_PROPBUY_FOR_GLS].uploadTo 	= 	SM_PROPBUYGLS //PropertyBought
	g_BS_Modifiers[BSMF_PROPBUY_FOR_LYB].uploadTo 	= 	SM_PROPBUYLYB //PropertyBought
	g_BS_Modifiers[BSMF_PROPBUY_FOR_LYC].uploadTo 	= 	SM_PROPBUYLYC //PropertyBought
	g_BS_Modifiers[BSMF_KILLS_FOR_GLS].uploadTo 	= 	SM_KILLSGLS //PeopleKilled
	g_BS_Modifiers[BSMF_KILLS_FOR_LYB].uploadTo 	= 	SM_KILLSLYB //PeopleKilled
	g_BS_Modifiers[BSMF_KILLS_FOR_LYC].uploadTo 	= 	SM_KILLSLYC //PeopleKilled
	g_BS_Modifiers[BSMF_EVADEDSTARS_FOR_GLS].uploadTo 	= 	SM_EVADEDSTARSGLS //WantedRatingEvaded
	g_BS_Modifiers[BSMF_EVADEDSTARS_FOR_LYB].uploadTo 	= 	SM_EVADEDSTARSLYB //WantedRatingEvaded
	g_BS_Modifiers[BSMF_EVADEDSTARS_FOR_LYC].uploadTo 	= 	SM_EVADEDSTARSLYC //WantedRatingEvaded
	g_BS_Modifiers[BSMF_CRIMES_FOR_GLS].uploadTo 	= 	SM_CRIMESGLS //CrimesComited
	g_BS_Modifiers[BSMF_CRIMES_FOR_LYB].uploadTo 	= 	SM_CRIMESLYB //CrimesComited
	g_BS_Modifiers[BSMF_CRIMES_FOR_LYC].uploadTo 	= 	SM_CRIMESLYC //CrimesComited
	g_BS_Modifiers[BSMF_CLOBOUGHT_FOR_CHA].uploadTo 	= 	SM_CLOBOUGHTCHA //ClothesBought
	g_BS_Modifiers[BSMF_CLOBOUGHT_FOR_CHB].uploadTo 	= 	SM_CLOBOUGHTCHB //ClothesBought
	g_BS_Modifiers[BSMF_COMPETITORSALES_FOR_CHA].uploadTo 	= 	SM_CMPTTRSLSCH //SalesToCompetitors
	g_BS_Modifiers[BSMF_COMPETITORSALES_FOR_CHB].uploadTo 	= 	SM_CMPTTRSLSCHB //SalesToCompetitors
	g_BS_Modifiers[BSMF_DOGAPPUSE].uploadTo 	= 	SM_DOGAPPUSE //DogApplicationUse
	g_BS_Modifiers[BSMF_HAPPYDOGTIME].uploadTo 	= 	SM_HAPPYDOGTIME //HappyDogTime
	g_BS_Modifiers[BSMF_DOGFED].uploadTo 	= 	SM_DOGFED //DogFed
	g_BS_Modifiers[BSMF_UNHAPPYDOGTIME].uploadTo 	= 	SM_UNHAPPYDOGTIME //UnhappyDogTime
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_BRU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_VAP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_MAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_PFI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_HVY].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_MTL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_GUN1].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_GUN2].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_GUN3].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_SPK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_WTR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_PIS].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_DAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_MET].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_WZL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_CNT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_LTA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_VIG].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_CRE].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_BDG].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_WIZ].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_TNK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_FRT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_BTR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_RON].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_LBL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_BLE].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_LOM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_BNC].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_TXI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_GLS].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_LYB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_LYC].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_PRT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_RLE].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_PET].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_FCD].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_CTM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SITEVISIT_FOR_IOU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BRU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_VAP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_MAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PFI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_HVY].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_MTL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GUN1].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GUN2].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GUN3].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_SPK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_WTR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PIS].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_DAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_MET].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_WZL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CNT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LTA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_VIG].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CRE].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BDG].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_WIZ].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_TNK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FRT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BTR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_RON].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LBL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BLE].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LOM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BNC].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_TXI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GLS].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LYB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LYC].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PRT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_RLE].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PET].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FCD].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CTM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BRAVECDEST_FOR_IOU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_BRU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_VAP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_MAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_PFI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_HVY].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_MTL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_GUN1].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_GUN2].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_GUN3].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_SPK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_WTR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_PIS].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_DAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_MET].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_WZL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_CNT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_LTA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_VIG].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_CRE].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_BDG].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_WIZ].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_TNK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_FRT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_BTR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_RON].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_LBL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_BLE].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_LOM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_BNC].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_TXI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_GLS].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_LYB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_LYC].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_PRT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_RLE].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_PET].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_FCD].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_CTM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BOARDDEST_FOR_IOU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECBUY_FOR_BRU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECBUY_FOR_VAP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECBUY_FOR_MAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECBUY_FOR_PFI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECBUY_FOR_HVY].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECBUY_FOR_MTL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_DISTDRIV_FOR_BRU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_DISTDRIV_FOR_VAP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_DISTDRIV_FOR_MAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_DISTDRIV_FOR_PFI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_DISTDRIV_FOR_HVY].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_DISTDRIV_FOR_MTL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECMOD_FOR_BRU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECMOD_FOR_VAP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECMOD_FOR_MAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECMOD_FOR_PFI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECMOD_FOR_HVY].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECMOD_FOR_MTL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_STUNTJUMP_FOR_BRU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_STUNTJUMP_FOR_MAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_STUNTJUMP_FOR_PFI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_STUNTJUMP_FOR_HVY].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_STUNTJUMP_FOR_MTL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECTHEFT_FOR_BRU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECTHEFT_FOR_VAP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECTHEFT_FOR_MAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECTHEFT_FOR_PFI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECTHEFT_FOR_HVY].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECTHEFT_FOR_MTL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECDAMAGE_FOR_BRU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECDAMAGE_FOR_VAP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECDAMAGE_FOR_MAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECDAMAGE_FOR_PFI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECDAMAGE_FOR_HVY].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECDAMAGE_FOR_MTL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECKILL_FOR_BRU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECKILL_FOR_VAP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECKILL_FOR_MAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECKILL_FOR_PFI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECKILL_FOR_HVY].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECKILL_FOR_MTL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_TAXIUSE].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPUSED_FOR_GUN1].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPUSED_FOR_GUN2].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPUSED_FOR_GUN3].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPTAKEN_FOR_GUN1].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPTAKEN_FOR_GUN2].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPTAKEN_FOR_GUN3].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPCOPKILL_FOR_GUN1].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPCOPKILL_FOR_GUN2].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPCOPKILL_FOR_GUN3].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPCRIMKILL_FOR_GUN1].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPCRIMKILL_FOR_GUN2].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPCRIMKILL_FOR_GUN3].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPCIVKILL_FOR_GUN1].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPCIVKILL_FOR_GUN2].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPCIVKILL_FOR_GUN3].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPCRIMCARKILL_FOR_GUN1].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPCRIMCARKILL_FOR_GUN2].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPCRIMCARKILL_FOR_GUN3].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPPLAKILL_FOR_GUN1].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPPLAKILL_FOR_GUN2].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WEPPLAKILL_FOR_GUN3].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VENDUSED_FOR_SPK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VENDUSED_FOR_CHI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VENDUSED_FOR_WTR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_FITDOWN].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_FITUP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_NEWSSTALLDAM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_HPEDKILL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_PUBCLUBVISIT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_TIMESDRUNK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_FRNDTOPUB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_DRNKCRIME].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_PLYRAMPAGE].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_HWANTEDTIME].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_NOWNTIME].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_LISTENTIME_FOR_MET].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_LISTENTIME_FOR_WZL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_LISTENTIME_FOR_CNT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_ZITID_FOR_MET].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_ZITID_FOR_WZL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_ZITID_FOR_CNT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_ZITID_FOR_PRT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_RADSWITCH_FOR_MET].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_RADSWITCH_FOR_WZL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_RADSWITCH_FOR_CNT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VEC].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_PARAJUMP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_WHEELIES].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_STOPPIES].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_ONFIRETIME].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_VECBAIL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_FIREVECBAIL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_PARADEATH].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_CRASHDEATH].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_PHONEOPENTIME_FOR_BDG].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_PHONEOPENTIME_FOR_WIZ].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_PHONEOPENTIME_FOR_TNK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_CALLTIME_FOR_BDG].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_CALLTIME_FOR_WIZ].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_CALLTIME_FOR_TNK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_TEXTSUSED_FOR_BDG].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_TEXTSUSED_FOR_WIZ].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_TEXTSUSED_FOR_TNK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_CALLREFUSED_FOR_BDG].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_CALLREFUSED_FOR_WIZ].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_CALLREFUSED_FOR_TNK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_OTPLTIME_FOR_BDG].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_OTPLTIME_FOR_WIZ].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_OTPLTIME_FOR_TNK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_LAPTOPTIME].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_PHONEPOP_FOR_FRT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_PHONEPOP_FOR_BTR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_PHONEPOP_FOR_FCD].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_APPSDL_FOR_FRT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_APPSDL_FOR_BTR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_INTERNETTIME_FOR_FRT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_INTERNETTIME_FOR_BTR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_INTERNETTIME_FOR_FCD].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_APPTIME].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BROKENPHONE_FOR_FRT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BROKENPHONE_FOR_BTR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BROKENPHONE_FOR_FCD].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_GAMEPLAYEDTIME].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_COMPTIME].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_JTKILLS].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_COTANKDEST_FOR_RON].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_TANKERDEST_FOR_RON].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_GARAGEDEST_FOR_RON].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SMINTUSED].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_GARAGEREPAIR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_GARAGEMONEY].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_MODBUYFAIL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_MONEYGAINED_FOR_LOM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_MONEYGAINED_FOR_BNC].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_MONEYGAINED_FOR_IOU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_MONEYSPENT_FOR_LOM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_MONEYSPENT_FOR_BNC].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_MONEYSPENT_FOR_IOU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_TAXIUSE0].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_TAXI_DESTROYED].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BUSTED_FOR_GLS].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BUSTED_FOR_LYB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_BUSTED_FOR_LYC].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_PROPBUY_FOR_GLS].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_PROPBUY_FOR_LYB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_PROPBUY_FOR_LYC].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_KILLS_FOR_GLS].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_KILLS_FOR_LYB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_KILLS_FOR_LYC].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_EVADEDSTARS_FOR_GLS].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_EVADEDSTARS_FOR_LYB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_EVADEDSTARS_FOR_LYC].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_CRIMES_FOR_GLS].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_CRIMES_FOR_LYB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_CRIMES_FOR_LYC].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_CLOBOUGHT_FOR_CHA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_CLOBOUGHT_FOR_CHB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_COMPETITORSALES_FOR_CHA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_COMPETITORSALES_FOR_CHB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_DOGAPPUSE].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_HAPPYDOGTIME].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_DOGFED].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_UNHAPPYDOGTIME].bOnline 	= TRUE	
*/ //Uncomment the previous when stats are in place
ENDPROC

/// Auto generated calculation of new UPM for each company

PROC BAWSAQ_GENERATED_APPLY_MODIFIERS_TO_PRICES()
	FLOAT fTotalEffects
	FLOAT fModifierSum
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_BRU].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_BRU].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BRU].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BRU].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_BRU].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_BRU].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECBUY_FOR_BRU].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECBUY_FOR_BRU].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_DISTDRIV_FOR_BRU].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_DISTDRIV_FOR_BRU].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECMOD_FOR_BRU].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECMOD_FOR_BRU].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_STUNTJUMP_FOR_BRU].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_STUNTJUMP_FOR_BRU].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECTHEFT_FOR_BRU].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECTHEFT_FOR_BRU].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECDAMAGE_FOR_BRU].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECDAMAGE_FOR_BRU].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECKILL_FOR_BRU].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECKILL_FOR_BRU].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_TAXIUSE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TAXIUSE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_VAP].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_VAP].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_MAI].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_MAI].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_PFI].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_PFI].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_HVY].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_HVY].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_MTL].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_MTL].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_BRU].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_VAP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_VAP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_VAP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_VAP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_VAP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_VAP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECBUY_FOR_VAP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECBUY_FOR_VAP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_DISTDRIV_FOR_VAP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_DISTDRIV_FOR_VAP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECMOD_FOR_VAP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECMOD_FOR_VAP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECTHEFT_FOR_VAP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECTHEFT_FOR_VAP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECDAMAGE_FOR_VAP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECDAMAGE_FOR_VAP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECKILL_FOR_VAP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECKILL_FOR_VAP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_TAXIUSE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TAXIUSE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BRU].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BRU].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_MAI].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_MAI].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_PFI].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_PFI].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_HVY].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_HVY].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_MTL].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_MTL].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_VAP].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_TAXIUSE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TAXIUSE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECKILL_FOR_MAI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECKILL_FOR_MAI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECDAMAGE_FOR_MAI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECDAMAGE_FOR_MAI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECTHEFT_FOR_MAI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECTHEFT_FOR_MAI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_STUNTJUMP_FOR_MAI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_STUNTJUMP_FOR_MAI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECMOD_FOR_MAI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECMOD_FOR_MAI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_DISTDRIV_FOR_MAI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_DISTDRIV_FOR_MAI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECBUY_FOR_MAI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECBUY_FOR_MAI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_MAI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_MAI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_MAI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_MAI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_MAI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_MAI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BRU].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BRU].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_VAP].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_VAP].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_PFI].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_PFI].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_HVY].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_HVY].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_MTL].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_MTL].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_MAI].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_TAXIUSE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TAXIUSE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECKILL_FOR_PFI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECKILL_FOR_PFI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECDAMAGE_FOR_PFI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECDAMAGE_FOR_PFI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECTHEFT_FOR_PFI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECTHEFT_FOR_PFI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_STUNTJUMP_FOR_PFI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_STUNTJUMP_FOR_PFI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECMOD_FOR_PFI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECMOD_FOR_PFI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_DISTDRIV_FOR_PFI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_DISTDRIV_FOR_PFI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECBUY_FOR_PFI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECBUY_FOR_PFI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_PFI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_PFI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PFI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PFI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_PFI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_PFI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BRU].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BRU].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_MAI].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_MAI].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_VAP].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_VAP].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_HVY].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_HVY].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_MTL].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_MTL].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_PFI].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_TAXIUSE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TAXIUSE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECKILL_FOR_HVY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECKILL_FOR_HVY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECDAMAGE_FOR_HVY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECDAMAGE_FOR_HVY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECTHEFT_FOR_HVY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECTHEFT_FOR_HVY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_STUNTJUMP_FOR_HVY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_STUNTJUMP_FOR_HVY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECMOD_FOR_HVY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECMOD_FOR_HVY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_DISTDRIV_FOR_HVY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_DISTDRIV_FOR_HVY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECBUY_FOR_HVY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECBUY_FOR_HVY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_HVY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_HVY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_HVY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_HVY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_HVY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_HVY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BRU].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BRU].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_MAI].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_MAI].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_VAP].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_VAP].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_PFI].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_PFI].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_MTL].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_MTL].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_HVY].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_TAXIUSE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TAXIUSE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECKILL_FOR_MTL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECKILL_FOR_MTL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECDAMAGE_FOR_MTL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECDAMAGE_FOR_MTL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECTHEFT_FOR_MTL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECTHEFT_FOR_MTL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_STUNTJUMP_FOR_MTL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_STUNTJUMP_FOR_MTL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECMOD_FOR_MTL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECMOD_FOR_MTL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_DISTDRIV_FOR_MTL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_DISTDRIV_FOR_MTL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECBUY_FOR_MTL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECBUY_FOR_MTL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_MTL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_MTL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_MTL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_MTL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_MTL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_MTL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BRU].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BRU].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_MAI].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_MAI].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_VAP].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_VAP].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_PFI].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_PFI].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_HVY].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_HVY].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_MTL].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_GUN1].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_GUN1].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GUN1].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GUN1].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_GUN1].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_GUN1].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPUSED_FOR_GUN1].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPUSED_FOR_GUN1].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPTAKEN_FOR_GUN1].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPTAKEN_FOR_GUN1].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPCOPKILL_FOR_GUN1].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPCOPKILL_FOR_GUN1].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPCRIMKILL_FOR_GUN1].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPCRIMKILL_FOR_GUN1].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPCIVKILL_FOR_GUN1].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPCIVKILL_FOR_GUN1].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPCRIMCARKILL_FOR_GUN1].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPCRIMCARKILL_FOR_GUN1].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPPLAKILL_FOR_GUN1].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPPLAKILL_FOR_GUN1].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_GUN2].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_GUN2].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_GUN3].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_GUN3].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_GUN1].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_GUN2].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_GUN2].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GUN2].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GUN2].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_GUN2].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_GUN2].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPUSED_FOR_GUN2].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPUSED_FOR_GUN2].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPTAKEN_FOR_GUN2].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPTAKEN_FOR_GUN2].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPCOPKILL_FOR_GUN2].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPCOPKILL_FOR_GUN2].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPCRIMKILL_FOR_GUN2].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPCRIMKILL_FOR_GUN2].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPCIVKILL_FOR_GUN2].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPCIVKILL_FOR_GUN2].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPCRIMCARKILL_FOR_GUN2].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPCRIMCARKILL_FOR_GUN2].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPPLAKILL_FOR_GUN2].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPPLAKILL_FOR_GUN2].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_GUN3].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_GUN3].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_GUN1].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_GUN1].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_GUN2].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_GUN3].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_GUN3].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GUN3].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GUN3].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_GUN3].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_GUN3].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPUSED_FOR_GUN3].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPUSED_FOR_GUN3].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPTAKEN_FOR_GUN3].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPTAKEN_FOR_GUN3].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPCOPKILL_FOR_GUN3].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPCOPKILL_FOR_GUN3].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPCRIMKILL_FOR_GUN3].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPCRIMKILL_FOR_GUN3].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPCIVKILL_FOR_GUN3].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPCIVKILL_FOR_GUN3].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPCRIMCARKILL_FOR_GUN3].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPCRIMCARKILL_FOR_GUN3].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WEPPLAKILL_FOR_GUN3].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WEPPLAKILL_FOR_GUN3].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_GUN2].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_GUN2].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_GUN1].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_GUN1].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_GUN3].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_SPK].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_SPK].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_SPK].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_SPK].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_SPK].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_SPK].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VENDUSED_FOR_SPK].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VENDUSED_FOR_SPK].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITDOWN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITDOWN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITUP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITUP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_HPEDKILL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_HPEDKILL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CHI].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CHI].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_WTR].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_WTR].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_SPK].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VENDUSED_FOR_CHI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VENDUSED_FOR_CHI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITDOWN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITDOWN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITUP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITUP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_HPEDKILL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_HPEDKILL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_WTR].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_WTR].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_SPK].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_SPK].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_CHI].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_WTR].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_WTR].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_WTR].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_WTR].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_WTR].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_WTR].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VENDUSED_FOR_WTR].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VENDUSED_FOR_WTR].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITDOWN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITDOWN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITUP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITUP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_HPEDKILL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_HPEDKILL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CHI].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CHI].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_SPK].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_SPK].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_WTR].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_PIS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_PIS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PIS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PIS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_PIS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_PIS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PUBCLUBVISIT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PUBCLUBVISIT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_TIMESDRUNK].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TIMESDRUNK].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FRNDTOPUB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FRNDTOPUB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_DRNKCRIME].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_DRNKCRIME].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_PIS].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_DAI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_DAI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_DAI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_DAI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_DAI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_DAI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PLYRAMPAGE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PLYRAMPAGE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_HWANTEDTIME].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_HWANTEDTIME].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_NOWNTIME].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_NOWNTIME].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_DAI].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_MET].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_MET].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_MET].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_MET].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_MET].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_MET].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_LISTENTIME_FOR_MET].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_LISTENTIME_FOR_MET].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_ZITID_FOR_MET].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_ZITID_FOR_MET].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_RADSWITCH_FOR_MET].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_RADSWITCH_FOR_MET].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_WZL].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_WZL].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CNT].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CNT].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_MET].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_WZL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_WZL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_WZL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_WZL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_WZL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_WZL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_LISTENTIME_FOR_WZL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_LISTENTIME_FOR_WZL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_ZITID_FOR_WZL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_ZITID_FOR_WZL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_RADSWITCH_FOR_WZL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_RADSWITCH_FOR_WZL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_MET].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_MET].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CNT].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CNT].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_WZL].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_CNT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_CNT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CNT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CNT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_CNT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_CNT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_LISTENTIME_FOR_CNT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_LISTENTIME_FOR_CNT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_ZITID_FOR_CNT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_ZITID_FOR_CNT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_RADSWITCH_FOR_CNT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_RADSWITCH_FOR_CNT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_WZL].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_WZL].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_MET].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_MET].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_CNT].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_LTA].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_LTA].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LTA].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LTA].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_LTA].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_LTA].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_TAXIUSE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TAXIUSE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VEC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VEC].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_LTA].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_VIG].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_VIG].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_VIG].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_VIG].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_VIG].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_VIG].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_VIG].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_CRE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_CRE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CRE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CRE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_CRE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_CRE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PARAJUMP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PARAJUMP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_WHEELIES].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_WHEELIES].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_STOPPIES].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_STOPPIES].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_ONFIRETIME].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_ONFIRETIME].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECBAIL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECBAIL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FIREVECBAIL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FIREVECBAIL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PARADEATH].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PARADEATH].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_CRASHDEATH].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_CRASHDEATH].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_CRE].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_BDG].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_BDG].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BDG].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BDG].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_BDG].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_BDG].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PHONEOPENTIME_FOR_BDG].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PHONEOPENTIME_FOR_BDG].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_CALLTIME_FOR_BDG].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_CALLTIME_FOR_BDG].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_TEXTSUSED_FOR_BDG].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TEXTSUSED_FOR_BDG].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_CALLREFUSED_FOR_BDG].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_CALLREFUSED_FOR_BDG].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_OTPLTIME_FOR_BDG].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_OTPLTIME_FOR_BDG].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_WIZ].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_WIZ].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_TNK].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_TNK].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_BDG].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_WIZ].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_WIZ].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_WIZ].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_WIZ].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_WIZ].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_WIZ].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PHONEOPENTIME_FOR_WIZ].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PHONEOPENTIME_FOR_WIZ].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_CALLTIME_FOR_WIZ].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_CALLTIME_FOR_WIZ].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_TEXTSUSED_FOR_WIZ].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TEXTSUSED_FOR_WIZ].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_CALLREFUSED_FOR_WIZ].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_CALLREFUSED_FOR_WIZ].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_OTPLTIME_FOR_WIZ].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_OTPLTIME_FOR_WIZ].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_TNK].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_TNK].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BDG].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BDG].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_WIZ].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_TNK].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_TNK].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_TNK].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_TNK].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_TNK].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_TNK].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PHONEOPENTIME_FOR_TNK].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PHONEOPENTIME_FOR_TNK].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_CALLTIME_FOR_TNK].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_CALLTIME_FOR_TNK].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_TEXTSUSED_FOR_TNK].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TEXTSUSED_FOR_TNK].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_CALLREFUSED_FOR_TNK].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_CALLREFUSED_FOR_TNK].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_OTPLTIME_FOR_TNK].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_OTPLTIME_FOR_TNK].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_WIZ].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_WIZ].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BDG].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BDG].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_TNK].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_FRT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_FRT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FRT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FRT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_FRT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_FRT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PHONEPOP_FOR_FRT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PHONEPOP_FOR_FRT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_APPSDL_FOR_FRT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_APPSDL_FOR_FRT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_INTERNETTIME_FOR_FRT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_INTERNETTIME_FOR_FRT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BROKENPHONE_FOR_FRT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BROKENPHONE_FOR_FRT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_LAPTOPTIME].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_LAPTOPTIME].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_APPTIME].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_APPTIME].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_FCD].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_FCD].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BTR].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BTR].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_FRT].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_BTR].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_BTR].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BTR].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BTR].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_BTR].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_BTR].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BROKENPHONE_FOR_BTR].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BROKENPHONE_FOR_BTR].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_INTERNETTIME_FOR_BTR].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_INTERNETTIME_FOR_BTR].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_APPSDL_FOR_BTR].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_APPSDL_FOR_BTR].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PHONEPOP_FOR_BTR].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PHONEPOP_FOR_BTR].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_FRT].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_FRT].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_FCD].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_FCD].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_BTR].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_RON].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_RON].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_RON].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_RON].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_RON].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_RON].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_GARAGEDEST_FOR_RON].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_GARAGEDEST_FOR_RON].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_TANKERDEST_FOR_RON].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TANKERDEST_FOR_RON].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_COTANKDEST_FOR_RON].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_COTANKDEST_FOR_RON].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_RON].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_LBL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_LBL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LBL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LBL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_LBL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_LBL].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_LBL].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_BLE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_BLE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BLE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BLE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_BLE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_BLE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_SMINTUSED].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SMINTUSED].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_BLE].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_LOM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_LOM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LOM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LOM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_LOM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_LOM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_MONEYGAINED_FOR_LOM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_MONEYGAINED_FOR_LOM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_MONEYSPENT_FOR_LOM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_MONEYSPENT_FOR_LOM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BNC].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BNC].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_IOU].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_IOU].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_LOM].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_BNC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_BNC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BNC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BNC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_BNC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_BNC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_MONEYGAINED_FOR_BNC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_MONEYGAINED_FOR_BNC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_MONEYSPENT_FOR_BNC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_MONEYSPENT_FOR_BNC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_IOU].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_IOU].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_LOM].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_LOM].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_BNC].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_TXI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_TXI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_TXI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_TXI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_TXI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_TXI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_TAXIUSE0].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TAXIUSE0].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_TAXI_DESTROYED].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TAXI_DESTROYED].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_TXI].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_GLS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_GLS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GLS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GLS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_GLS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_GLS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BUSTED_FOR_GLS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BUSTED_FOR_GLS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PROPBUY_FOR_GLS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PROPBUY_FOR_GLS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_KILLS_FOR_GLS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_KILLS_FOR_GLS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_EVADEDSTARS_FOR_GLS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_EVADEDSTARS_FOR_GLS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_CRIMES_FOR_GLS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_CRIMES_FOR_GLS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_LYB].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_LYB].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_LYC].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_LYC].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_GLS].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_LYB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_LYB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LYB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LYB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_LYB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_LYB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BUSTED_FOR_LYB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BUSTED_FOR_LYB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PROPBUY_FOR_LYB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PROPBUY_FOR_LYB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_KILLS_FOR_LYB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_KILLS_FOR_LYB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_EVADEDSTARS_FOR_LYB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_EVADEDSTARS_FOR_LYB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_CRIMES_FOR_LYB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_CRIMES_FOR_LYB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_LYC].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_LYC].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_GLS].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_GLS].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_LYB].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_LYC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_LYC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LYC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LYC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_LYC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_LYC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BUSTED_FOR_LYC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BUSTED_FOR_LYC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PROPBUY_FOR_LYC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PROPBUY_FOR_LYC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_KILLS_FOR_LYC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_KILLS_FOR_LYC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_EVADEDSTARS_FOR_LYC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_EVADEDSTARS_FOR_LYC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_CRIMES_FOR_LYC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_CRIMES_FOR_LYC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_GLS].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_GLS].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_LYB].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_LYB].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_LYC].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_PRT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_PRT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PRT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PRT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_PRT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_PRT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_ZITID_FOR_PRT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_ZITID_FOR_PRT].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_PRT].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHA].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHA].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHA].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHA].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHA].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHA].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_CLOBOUGHT_FOR_CHA].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_CLOBOUGHT_FOR_CHA].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_COMPETITORSALES_FOR_CHA].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_COMPETITORSALES_FOR_CHA].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CHB].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CHB].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_CHA].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_CLOBOUGHT_FOR_CHB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_CLOBOUGHT_FOR_CHB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_COMPETITORSALES_FOR_CHB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_COMPETITORSALES_FOR_CHB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CHA].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CHA].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_CHB].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_RLE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_RLE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_RLE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_RLE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_RLE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_RLE].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_RLE].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_PET].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_PET].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PET].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PET].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_PET].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_PET].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_UNHAPPYDOGTIME].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_UNHAPPYDOGTIME].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_DOGFED].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_DOGFED].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_HAPPYDOGTIME].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_HAPPYDOGTIME].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_DOGAPPUSE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_DOGAPPUSE].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_PET].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_ECL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_ECL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_ECL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_ECL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_ECL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_ECL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VENDUSED_FOR_ECL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VENDUSED_FOR_ECL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITDOWN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITDOWN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITUP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITUP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CDY].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CDY].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BGR].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BGR].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CLK].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CLK].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_GLY].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_GLY].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BTE].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BTE].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BEN].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BEN].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_ECL].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_CDY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_CDY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CDY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CDY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_CDY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_CDY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VENDUSED_FOR_CDY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VENDUSED_FOR_CDY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITDOWN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITDOWN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITUP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITUP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BEN].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BEN].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_ECL].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_ECL].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BGR].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BGR].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CLK].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CLK].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_GLY].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_GLY].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BTE].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BTE].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_CDY].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_BGR].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_BGR].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_BGR].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_BGR].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VENDUSED_FOR_BGR].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VENDUSED_FOR_BGR].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITDOWN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITDOWN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITUP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITUP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_ECL].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_ECL].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CDY].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CDY].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CLK].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CLK].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_GLY].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_GLY].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BTE].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BTE].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BEN].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BEN].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_BGR].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_CLK].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_CLK].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CLK].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CLK].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_CLK].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_CLK].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VENDUSED_FOR_CLK].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VENDUSED_FOR_CLK].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITDOWN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITDOWN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITUP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITUP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_ECL].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_ECL].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CDY].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CDY].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BGR].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BGR].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_GLY].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_GLY].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BTE].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BTE].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BEN].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BEN].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_CLK].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_GLY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_GLY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GLY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GLY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_GLY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_GLY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VENDUSED_FOR_GLY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VENDUSED_FOR_GLY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITDOWN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITDOWN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITUP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITUP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_ECL].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_ECL].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CDY].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CDY].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BGR].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BGR].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CLK].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CLK].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BTE].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BTE].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BEN].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BEN].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_GLY].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_BTE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_BTE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BTE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BTE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_BTE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_BTE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VENDUSED_FOR_BTE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VENDUSED_FOR_BTE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITDOWN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITDOWN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITUP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITUP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_ECL].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_ECL].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CDY].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CDY].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BGR].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BGR].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CLK].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CLK].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_GLY].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_GLY].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BEN].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BEN].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_BTE].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_BEN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_BEN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BEN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BEN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_BEN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_BEN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VENDUSED_FOR_BEN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VENDUSED_FOR_BEN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITDOWN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITDOWN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITUP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITUP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_NEWSSTALLDAM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_ECL].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_ECL].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CDY].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CDY].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BGR].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BGR].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CLK].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CLK].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_GLY].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_GLY].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BTE].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BTE].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_BEN].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_BEO].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_BEO].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BEO].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BEO].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_BEO].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_BEO].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_FLC].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_FLC].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_BEO].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_FLC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_FLC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FLC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FLC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_FLC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_FLC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_MINUSMONEYTIME].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_MINUSMONEYTIME].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PRISONBAILS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PRISONBAILS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BEO].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BEO].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_FLC].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_PRO].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_PRO].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PRO].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PRO].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_PRO].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_PRO].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_GOLFPLAYED].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_GOLFPLAYED].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_YOGATIMES].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_YOGATIMES].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_TRICOMPLETED].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TRICOMPLETED].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_GYMTIME].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_GYMTIME].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_PRO].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_XER].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_XER].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_XER].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_XER].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_XER].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_XER].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_COTANKDEST_FOR_XER].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_COTANKDEST_FOR_XER].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_TANKERDEST_FOR_XER].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TANKERDEST_FOR_XER].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_GARAGEDEST_FOR_XER].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_GARAGEDEST_FOR_XER].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_XER].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_UNI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_UNI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_UNI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_UNI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_UNI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_UNI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_STRIPCLUBVISIT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_STRIPCLUBVISIT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_STRIPTIME].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_STRIPTIME].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_UGHOOKERSKILL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_UGHOOKERSKILL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_STRIPCLUBTROUBLE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_STRIPCLUBTROUBLE].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_UNI].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_KRP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_KRP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_KRP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_KRP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_KRP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_KRP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_SAFEHOUSEUPGRD].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SAFEHOUSEUPGRD].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BDLGDESTROYED].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BDLGDESTROYED].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_OWNDSAFEHOUSE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_OWNDSAFEHOUSE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_VECDEST].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_VECDEST].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_KRP].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_LFB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_LFB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LFB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_LFB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_LFB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_LFB].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_LFB].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_GBL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_GBL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GBL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GBL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_GBL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_GBL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BETSPLACED].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BETSPLACED].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_GBL].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_FUS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_FUS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FUS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FUS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_FUS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_FUS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PILOTTIMES].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PILOTTIMES].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_ADI].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_ADI].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_FUS].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_ADI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_ADI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_ADI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_ADI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_ADI].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_ADI].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PILOTTIMES].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PILOTTIMES].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_FUS].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_FUS].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_ADI].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_PED].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_PED].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PED].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PED].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_PED].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_PED].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_TIMEPLAYED].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TIMEPLAYED].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_PED].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_REC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_REC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_REC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_REC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_REC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_REC].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_REC].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_CLOBOUGHT_FOR_CHC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_CLOBOUGHT_FOR_CHC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_COMPETITORSALES_FOR_CHC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_COMPETITORSALES_FOR_CHC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CHD].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CHD].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CHE].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CHE].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CHF].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CHF].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_CHC].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHD].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHD].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHD].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHD].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHD].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHD].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_CLOBOUGHT_FOR_CHD].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_CLOBOUGHT_FOR_CHD].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_COMPETITORSALES_FOR_CHD].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_COMPETITORSALES_FOR_CHD].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CHC].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CHC].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CHE].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CHE].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CHF].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CHF].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_CHD].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_CLOBOUGHT_FOR_CHE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_CLOBOUGHT_FOR_CHE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_COMPETITORSALES_FOR_CHE].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_COMPETITORSALES_FOR_CHE].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CHD].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CHD].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CHC].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CHC].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CHF].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CHF].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_CHE].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHF].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_CHF].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHF].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CHF].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHF].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_CHF].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_CLOBOUGHT_FOR_CHF].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_CLOBOUGHT_FOR_CHF].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_COMPETITORSALES_FOR_CHF].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_COMPETITORSALES_FOR_CHF].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CHD].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CHD].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CHC].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CHC].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_CHE].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_CHE].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_CHF].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_FLM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_FLM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FLM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FLM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_FLM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_FLM].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_FLM].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_PHM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_PHM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PHM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PHM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_PHM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_PHM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_HOSPITALVIS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_HOSPITALVIS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PEOPLEINJURED].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PEOPLEINJURED].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_DRUGBUY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_DRUGBUY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_HEALTHPACKSUSED].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_HEALTHPACKSUSED].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_DDKILLED].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_DDKILLED].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_HANGOVERS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_HANGOVERS].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_PHM].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_GOP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_GOP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GOP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GOP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_GOP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_GOP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_KILLSPREES].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_KILLSPREES].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_MAILBOXDEST].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_MAILBOXDEST].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_EMAILSENT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_EMAILSENT].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_TEXTMESSENT].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_TEXTMESSENT].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_GOP].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_BAN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_BAN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BAN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_BAN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_BAN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_BAN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_VON].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_VON].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_BAN].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_VON].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_VON].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_VON].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_VON].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_VON].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_VON].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BAN].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BAN].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_VON].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_WTA].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_WTA].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_WTA].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_WTA].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_WTA].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_WTA].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_WTB].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_WTB].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_WTA].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_WTB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_WTB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_WTB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_WTB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_WTB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_WTB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_WTA].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_WTA].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_WTB].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_FGA].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_FGA].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FGA].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FGA].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_FGA].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_FGA].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_FGB].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_FGB].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_FGA].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_FGB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_FGB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FGB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FGB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_FGB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_FGB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_FGA].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_FGA].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_FGB].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_MAX].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_MAX].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_MAX].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_MAX].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_MAX].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_MAX].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PEDFIREKILL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PEDFIREKILL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PEDAFLAMETIME].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PEDAFLAMETIME].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_VEN].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_VEN].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_MAX].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_VEN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_VEN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_VEN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_VEN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_VEN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_VEN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PEDFIREKILL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PEDFIREKILL].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PEDAFLAMETIME].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PEDAFLAMETIME].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_MAX].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_MAX].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_VEN].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_RLC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_RLC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_RLC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_RLC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_RLC].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_RLC].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_RLB].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_RLB].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_RLC].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_RLB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_RLB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_RLB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_RLB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_RLB].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_RLB].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_RLC].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_RLC].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_RLB].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_GAS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_GAS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GAS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_GAS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_GAS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_GAS].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_GAS].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_GRU].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_GRU].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_GRU].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_GRU].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_GRU].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_PMP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_PMP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PMP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_PMP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_PMP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_PMP].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_GYMTIME].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_GYMTIME].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITDOWN].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITDOWN].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_FITUP].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_FITUP].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_PMP].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_MHO].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_MHO].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_MHO].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_MHO].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_MHO].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_MHO].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_MHO].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_FCD].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_FCD].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FCD].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_FCD].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_FCD].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_FCD].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_PHONEPOP_FOR_FCD].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_PHONEPOP_FOR_FCD].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_INTERNETTIME_FOR_FCD].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_INTERNETTIME_FOR_FCD].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BROKENPHONE_FOR_FCD].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BROKENPHONE_FOR_FCD].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_GAMEPLAYEDTIME].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_GAMEPLAYEDTIME].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_COMPTIME].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_COMPTIME].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_JTKILLS].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_JTKILLS].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_FRT].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_FRT].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BTR].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BTR].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_FCD].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_CTM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_CTM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CTM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_CTM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_CTM].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_CTM].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_GARAGEREPAIR].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_GARAGEREPAIR].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_GARAGEMONEY].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_GARAGEMONEY].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_MODBUYFAIL].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_MODBUYFAIL].fModValue
		ENDIF
		fTotalEffects+= 1
		g_BS_Listings[BS_CO_CTM].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
		fTotalEffects = 0.0
		fModifierSum = 0.0
		IF NOT ( g_BS_Modifiers[BSMF_SITEVISIT_FOR_IOU].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_SITEVISIT_FOR_IOU].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BRAVECDEST_FOR_IOU].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BRAVECDEST_FOR_IOU].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_BOARDDEST_FOR_IOU].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_BOARDDEST_FOR_IOU].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_MONEYSPENT_FOR_IOU].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_MONEYSPENT_FOR_IOU].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT ( g_BS_Modifiers[BSMF_MONEYGAINED_FOR_IOU].fModValue = 0.0)
			fModifierSum += g_BS_Modifiers[BSMF_MONEYGAINED_FOR_IOU].fModValue
		ENDIF
		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_LOM].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_LOM].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		IF NOT (g_BS_Listings[BS_CO_BNC].fUPMAppliedLastUpdate = 0.0)
			fModifierSum += g_BS_Listings[BS_CO_BNC].fUPMAppliedLastUpdate
		ENDIF

		fTotalEffects+= 1
		g_BS_Listings[BS_CO_IOU].fUniversalPriceModifier =  fModifierSum/fTotalEffects 
ENDPROC
