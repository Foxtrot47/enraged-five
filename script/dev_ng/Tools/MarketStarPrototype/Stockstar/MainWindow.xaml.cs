﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;
using System.ComponentModel;

namespace Stockstar
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Simulator> simulations;

        public MainWindow()
        {
            InitializeComponent();
            MarketData.Start();

            simulations = new List<Simulator>();
            
        }

        internal void RefreshListings(bool IgnoreFocus = false)
        {
            object cob = colist.SelectedItem;
            object mob = molist.SelectedItem;

            pickmod.SelectedIndex = -1;

            colist.SelectedIndex = -1;
            molist.SelectedIndex = -1;
            colist.Items.Clear();
            molist.Items.Clear();
            pickmod.Items.Clear();
            compick.Items.Clear();

            foreach (MarketData.CompanyListing cl in MarketData.getCompanyList)
            {
                colist.Items.Add(cl);
                compick.Items.Add(cl);
            }
            foreach (MarketData.ModifierListing ml in MarketData.getModifierList)
            {
                molist.Items.Add(ml);
                pickmod.Items.Add(ml);
            }
            pickmod.Items.SortDescriptions.Clear();
            pickmod.Items.SortDescriptions.Add(new SortDescription(null, ListSortDirection.Ascending));
           
            if (MarketData.IsCompanyValid((MarketData.CompanyListing)cob))
            {
                //set selection
                colist.SelectedItem = cob;
            }

            if (MarketData.IsModifierValid((MarketData.ModifierListing)mob))
            {
                //set selection
                molist.SelectedItem = mob;
            }

        }

        private void fload_Click(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Company_List_Latest"; // Default file name
            dlg.DefaultExt = ".mst"; // Default file extension
            dlg.Filter = "Marketstar Documents (.mst)|*.mst"; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                // Open document
                 //dlg.FileName;
                MarketData.LoadData(dlg.FileName);
                RefreshListings();
            }
        }

        private void fsave_Click(object sender, RoutedEventArgs e)
        {
            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Company_List_Latest"; // Default file name
            dlg.DefaultExt = ".mst"; // Default file extension
            dlg.Filter = "Marketstar Documents (.mst)|*.mst"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                //string filename = dlg.FileName;
                MarketData.SaveData(dlg.FileName);
            }
        }

        private void coaddbutton_Click(object sender, RoutedEventArgs e)
        {
            //create a new company and select it in colist
            int companyindex = MarketData.AddCompany();

            RefreshListings();

            //set focus of colist
        }

        private void colist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            defbut.IsEnabled = false;
            negbut.IsEnabled = false;
            posbut.IsEnabled = false;

            if (!(e.AddedItems.Count > 0))
            {
                return;
            }
            //((MarketData.CompanyListing)e.AddedItems[0])
            
            coname.Text = ((MarketData.CompanyListing)e.AddedItems[0]).Name;
            cosname.Text = ((MarketData.CompanyListing)e.AddedItems[0]).ShortTag;
            coenum.Text = ((MarketData.CompanyListing)e.AddedItems[0]).EnumString;

           // cogencheck.IsChecked = ((MarketData.CompanyListing)e.AddedItems[0]).bAutoGenEnum;
            coonline.IsChecked = ((MarketData.CompanyListing)e.AddedItems[0]).bOnline;

            //costartprice.Text = ((MarketData.CompanyListing)e.AddedItems[0]).fStartingPrice.ToString();
           // costartsens.Text = ((MarketData.CompanyListing)e.AddedItems[0]).fStartingSensitivity.ToString();
            //fill out pickedmods
            pickedmods.Items.Clear();


            /*
            foreach (MarketData.ModifierListing ml in ((MarketData.CompanyListing)e.AddedItems[0]).AttachedModifiers)
            {
                pickedmods.Items.Add(ml);

                //ml.bInterpretation

                    ((MarketData.CompanyListing)e.AddedItems[0]).AttachedModifierInterpretations
            }*/

            int posto = 0;
            int negto = 0;
            for (int i = 0; i < ((MarketData.CompanyListing)e.AddedItems[0]).AttachedModifiers.Count; ++i)
            {
                pickedmods.Items.Add(((MarketData.CompanyListing)e.AddedItems[0]).AttachedModifiers[i]);
                int atterp = ((MarketData.CompanyListing)e.AddedItems[0]).AttachedModifierInterpretations[i];
                bool baseterp = ((MarketData.CompanyListing)e.AddedItems[0]).AttachedModifiers[i].bInterpretation;
                if (atterp == 0){if(baseterp){++posto; }else{++negto;}}else if (atterp == 1) {++posto;}else{++negto;}
            }
            posnegnote.Content = "p:n = " + posto + ":" + negto + "";



            //temp procedure to auto set the attributes
            //Random r = new Random();
            //only none default if having mods
            /*
            if (posto != 0 && negto != 0)
            {
                MarketData.CompanyListing cl = ((MarketData.CompanyListing)e.AddedItems[0]);
               
                cl.floor = 2.5f + (float)(r.NextDouble());
                cl.noisiness = (((float)(r.NextDouble()))*0.75f);
                if (cl.noisiness < 0.075f)
                {
                    cl.noisiness = 0.0f;
                }
                //leave bearishness and bullishness for now, will set when we see how stuff moves
                //cl.bullishness
                //cl.bearishness

                //cl.ceiling = 4000.0f;
                //cl.stepcap

                float f = (r.Next(9)+1) * 0.5f;
                cl.stepcap = f;
                cl.floor = f;
            }
            */

            MarketData.GenerateCompanyEnum((MarketData.CompanyListing)e.AddedItems[0]);

            compicked.Items.Clear();
            foreach (MarketData.CompanyListing cl in ((MarketData.CompanyListing)e.AddedItems[0]).Competitors)
            {
                compicked.Items.Add(cl);
            }

            combox.Text = ((MarketData.CompanyListing)e.AddedItems[0]).comment;
            combox.Text = combox.Text.Replace('-', ' ');

            bullval.Text = ((MarketData.CompanyListing)e.AddedItems[0]).bullishness.ToString();
            bearval.Text = ((MarketData.CompanyListing)e.AddedItems[0]).bearishness.ToString();
            ceilval.Text = ((MarketData.CompanyListing)e.AddedItems[0]).ceiling.ToString();
            floval.Text = ((MarketData.CompanyListing)e.AddedItems[0]).floor.ToString();
            stepval.Text = ((MarketData.CompanyListing)e.AddedItems[0]).stepcap.ToString();
            noival.Text = ((MarketData.CompanyListing)e.AddedItems[0]).noisiness.ToString();

            var item = (MarketData.CompanyListing) e.AddedItems[0];
            if (!string.IsNullOrWhiteSpace(item.IDV.ToString()))
            {
                idvval.Text = item.IDV.ToString();
                iphvval.Text = item.IPHV.ToString();
                isavval.Text = item.ISAV.ToString();
                iabsval.Text = item.IABS.ToString();
                ifbbval.Text = item.IFBB.ToString();
                icbbval.Text = item.ICBB.ToString();

                SetHighlightState(item.IDV, MarketData.InclinationConstants.IDV, idvval);
                SetHighlightState(item.IPHV, MarketData.InclinationConstants.IPHV, iphvval);
                SetHighlightState(item.ISAV, MarketData.InclinationConstants.ISAV, isavval);
                SetHighlightState(item.IABS, MarketData.InclinationConstants.IABS, iabsval);
                SetHighlightState(item.IFBB, MarketData.InclinationConstants.IFBB, ifbbval);
                SetHighlightState(item.ICBB, MarketData.InclinationConstants.ICBB, icbbval);
            }
        }

        private void SetHighlightState(float value, float constant, TextBox txtBox)
        {
            if (!value.Equals(constant))
            {
                Highlight(txtBox);
            }
            else
            {
                Reset(txtBox);
            }
        }

        private static void Highlight(TextBox txtBox)
        {
            txtBox.Background = new SolidColorBrush(Color.FromScRgb(50, 255, 0, 0));
        }

        private static void Reset(TextBox txtBox)
        {
            txtBox.Background = new SolidColorBrush(Color.FromScRgb(50, 255, 255, 255));
        }

        private void corembutton_Click(object sender, RoutedEventArgs e)
        {
            //remove index from company list if one is selected
            //int selected = colist.SelectedIndex;
            //Console.WriteLine(colist.SelectedIndex + "");

            MarketData.RemoveCompany(colist.SelectedIndex);

            RefreshListings();
        }

        private void moadd_Click(object sender, RoutedEventArgs e)
        {
            //add a default modifier
            int modifierindex = MarketData.AddModifier();

            RefreshListings();
        }

        private void molist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(e.AddedItems.Count > 0))
            {
                return;
            }

            moname.Text = ((MarketData.ModifierListing)e.AddedItems[0]).Name;
            moenum.Text = ((MarketData.ModifierListing)e.AddedItems[0]).GeneratedEnum;
            moautogen.IsChecked = ((MarketData.ModifierListing)e.AddedItems[0]).bAutoGenEnum;
            moscaling.Text = ((MarketData.ModifierListing)e.AddedItems[0]).fApplicationScale.ToString();

            moshared.IsChecked = ((MarketData.ModifierListing)e.AddedItems[0]).bShared;

            if (((MarketData.ModifierListing)e.AddedItems[0]).bInterpretation)
            {
                modpos.IsChecked = true;
            }
            else
            {
                modneg.IsChecked = true;
            }


            usesstatenum.IsChecked = ((MarketData.ModifierListing)e.AddedItems[0]).bExtEnum;
            spstatsenumname.Text = ((MarketData.ModifierListing)e.AddedItems[0]).SpStatsEnumOverride;

            //modpos
            //modneg


            statisfloatprof.IsChecked = ((MarketData.ModifierListing)e.AddedItems[0]).bStatIsFloatProfType;

        }

        private void moremove_Click(object sender, RoutedEventArgs e)
        {
            MarketData.RemoveModifier((MarketData.ModifierListing)molist.SelectedItem);
           
            RefreshListings();
        }

        private void coname_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(!(colist.SelectedIndex > -1))
            {
                return;
            }

            //get focused company
            MarketData.CompanyListing cl = (MarketData.CompanyListing)colist.SelectedItem;

            cl.Name = coname.Text;

        }

        private void coname_LostFocus(object sender, RoutedEventArgs e)
        {
           
            RefreshListings();

        }

        private void cosname_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!(colist.SelectedIndex > -1))
            {
                return;
            }

            //get focused company
            MarketData.CompanyListing cl = (MarketData.CompanyListing)colist.SelectedItem;

            cl.ShortTag = cosname.Text;
        }

        private void coenum_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!(colist.SelectedIndex > -1))
            {
                return;
            }

            //TODO check for auto gen
            /*
            if (cogencheck.IsChecked == true)
            {
                //auto gen override!

                MarketData.GenerateCompanyEnum((MarketData.CompanyListing)colist.SelectedItem);
                coenum.Text = ((MarketData.CompanyListing)colist.SelectedItem).EnumString;
                return;
            }
            */
            //get focused company
            MarketData.CompanyListing cl = (MarketData.CompanyListing)colist.SelectedItem;

            cl.EnumString = coenum.Text;
        }

        private void cosname_LostFocus(object sender, RoutedEventArgs e)
        {
            RefreshListings();
        }

        private void coenum_LostFocus(object sender, RoutedEventArgs e)
        {
            RefreshListings();
        }

        private void cogencheck_Checked(object sender, RoutedEventArgs e)
        {
            if (!(colist.SelectedIndex > -1))
            {
                return;
            }

            //get focused company
            MarketData.CompanyListing cl = (MarketData.CompanyListing)colist.SelectedItem;

            //cl.bAutoGenEnum = (bool)cogencheck.IsChecked;
        }

        private void coonline_Checked(object sender, RoutedEventArgs e)
        {
            if (!(colist.SelectedIndex > -1))
            {
                return;
            }

            //get focused company
            MarketData.CompanyListing cl = (MarketData.CompanyListing)colist.SelectedItem;

            cl.bOnline = (bool)coonline.IsChecked;
        }

        private void moname_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!(molist.SelectedIndex > -1))
            {
                return;
            }

            //get focused company
            MarketData.ModifierListing ml = (MarketData.ModifierListing)molist.SelectedItem;

            ml.Name = moname.Text;
        }

        private void moname_LostFocus(object sender, RoutedEventArgs e)
        {
            RefreshListings();
        }

        private void moenum_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!(molist.SelectedIndex > -1))
            {
                return;
            }

            //TODO, check for auto gen
            if (moautogen.IsChecked == true)
            {
                MarketData.GenerateModifierEnum((MarketData.ModifierListing)molist.SelectedItem);
                moenum.Text = ((MarketData.ModifierListing)molist.SelectedItem).GeneratedEnum;
                return;
            }
            //get focused company
            MarketData.ModifierListing ml = (MarketData.ModifierListing)molist.SelectedItem;

            ml.GeneratedEnum = moenum.Text;
        }

        private void moenum_LostFocus(object sender, RoutedEventArgs e)
        {
            RefreshListings();
        }

        private void moautogen_Checked(object sender, RoutedEventArgs e)
        {
            if (!(molist.SelectedIndex > -1))
            {
                return;
            }

            //get focused company
            MarketData.ModifierListing ml = (MarketData.ModifierListing)molist.SelectedItem;

            ml.bAutoGenEnum = (bool)moautogen.IsChecked;

            if ((bool)moautogen.IsChecked)
            {
                MarketData.GenerateModifierEnum((MarketData.ModifierListing)molist.SelectedItem);
                moenum.Text = ((MarketData.ModifierListing)molist.SelectedItem).GeneratedEnum;
            }
        }
        private void moshared_Checked(object sender, RoutedEventArgs e)
        {
            if (!(molist.SelectedIndex > -1))
            {
                return;
            }

            //get focused company
            MarketData.ModifierListing ml = (MarketData.ModifierListing)molist.SelectedItem;

            ml.bShared = (bool)moshared.IsChecked;

            if (!ml.bShared)
            {
                ml.bExtEnum = false;//cannot be split and use a spStatsEnum
                usesstatenum.IsChecked = false;
            }


        }
        private void usesstatenum_Checked(object sender, RoutedEventArgs e)
        {
            if (!(molist.SelectedIndex > -1))
            {
                return;
            }
            if (usesstatenum.IsChecked == false)
            {
                moshared.IsChecked = true;//stat enums must be shared

            }
            MarketData.ModifierListing ml = (MarketData.ModifierListing)molist.SelectedItem;

            ml.bExtEnum = (bool)usesstatenum.IsChecked;

            if(ml.bExtEnum)
            {
                ml.bShared = true;
                moshared.IsChecked = true;
            }

        }
        private void spstatsenumname_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!(molist.SelectedIndex > -1))
            {
                return;
            }
            MarketData.ModifierListing ml = (MarketData.ModifierListing)molist.SelectedItem;


            ml.SpStatsEnumOverride = spstatsenumname.Text;
        }
        private void addmod_Click(object sender, RoutedEventArgs e)
        {
            if (pickmod.SelectedIndex == -1 || colist.SelectedIndex == -1) 
            {
                return;
            }

            if (((MarketData.CompanyListing)colist.SelectedItem).AttachedModifiers.Contains((MarketData.ModifierListing)pickmod.SelectedItem))
            {
                return;
            }

            ((MarketData.CompanyListing)colist.SelectedItem).AttachedModifiers.Add((MarketData.ModifierListing)pickmod.SelectedItem);
            ((MarketData.CompanyListing)colist.SelectedItem).AttachedModifierInterpretations.Add(0);//default
            pickedmods.Items.Add(pickmod.SelectedItem);




        }

        private void delmod_Click(object sender, RoutedEventArgs e)
        {
            //delete selected modifier from company
            if (pickedmods.SelectedIndex == -1 || colist.SelectedIndex == -1)
            {
                return;
            }
            //cancel if it doesn't contain it
            if (!((MarketData.CompanyListing)colist.SelectedItem).AttachedModifiers.Contains((MarketData.ModifierListing)pickedmods.SelectedItem))
            {
                return;
            }

            //remove if it does
            int indof = ((MarketData.CompanyListing)colist.SelectedItem).AttachedModifiers.IndexOf((MarketData.ModifierListing)pickedmods.SelectedItem);
            ((MarketData.CompanyListing)colist.SelectedItem).AttachedModifiers.Remove((MarketData.ModifierListing)pickedmods.SelectedItem);
            ((MarketData.CompanyListing)colist.SelectedItem).AttachedModifierInterpretations.RemoveAt(indof);

            pickedmods.Items.Remove(pickedmods.SelectedItem);
        }

        private void jumpmod_Click(object sender, RoutedEventArgs e)
        {
            if (pickedmods.SelectedIndex == -1 || colist.SelectedIndex == -1)
            {
                return;
            }
            molist.SelectedItem = pickedmods.SelectedItem;
            tabpanel.SelectedIndex = 1;
        }

        private void fsim_Click(object sender, RoutedEventArgs e)
        {
            //launch a new sim window

            Simulator s = new Simulator(this);
            
            simulations.Add(s);
        }

        private void costartprice_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (colist.SelectedIndex == -1)
            {
                return;
            }

            float floaded;
            /*
            if (float.TryParse(costartprice.Text, out floaded))
            {

                ((MarketData.CompanyListing)colist.SelectedItem).fStartingPrice = floaded;
                costartprice.Background = Brushes.White;
            }
            else
            {

                //incorrect value
                costartprice.Background = Brushes.Red;

            }*/
        }

        private void costartsens_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (colist.SelectedIndex == -1)
            {
                return;
            }

            float floaded;
            /*
            if (float.TryParse(costartsens.Text, out floaded))
            {
                ((MarketData.CompanyListing)colist.SelectedItem).fStartingSensitivity = floaded;
                costartsens.Background = Brushes.White;
            }
            else
            {
                //incorrect value
                costartsens.Background = Brushes.Red;
            }
            */

        }

        private void coofflinebutton_Click(object sender, RoutedEventArgs e)
        {
            RefreshListings();
            //remove onlines
            foreach (MarketData.CompanyListing o in MarketData.getCompanyList)
            {
                if (o.bOnline)
                {
                    colist.Items.Remove(o);
                }
            }
        }

        private void coonlinebutton_Click(object sender, RoutedEventArgs e)
        {
            RefreshListings();
            //remove offlines            
            foreach (MarketData.CompanyListing o in MarketData.getCompanyList)
            {
                if (!o.bOnline)
                {
                    colist.Items.Remove(o);
                }
            }
        }

        private void cobothbutton_Click(object sender, RoutedEventArgs e)
        {
            RefreshListings();
        }


        private void genbutton_Click(object sender, RoutedEventArgs e)
        {
            //enum list into 
            //enums

            //companies

            //
            /*
            enums.Text = "";

            foreach (string s in MarketData.getGenerateXMLStatNames(true))
            {
                enums.Text += s + "\n";
            }
            */

           // enums.Text += MarketData.genScriptCompanyList();
           // enums.Text +=  "\n";

            
        }

        private void posbut_Checked(object sender, RoutedEventArgs e)
        {

            if (pickedmods.SelectedIndex == -1 || colist.SelectedIndex == -1)
            {
                return;
            }
            //if there is a pickedmods selected then find it's indice and set it's value
            ((MarketData.CompanyListing)colist.SelectedItem).AttachedModifierInterpretations[pickedmods.SelectedIndex] = 1;

                
        }

        private void negbut_Checked(object sender, RoutedEventArgs e)
        {
            if (pickedmods.SelectedIndex == -1 || colist.SelectedIndex == -1)
            {
                return;
            }
            //if there is a pickedmods selected then find it's indice and set it's value
            ((MarketData.CompanyListing)colist.SelectedItem).AttachedModifierInterpretations[pickedmods.SelectedIndex] = 2;
        }

        private void defbut_Checked(object sender, RoutedEventArgs e)
        {
            if (pickedmods.SelectedIndex == -1 || colist.SelectedIndex == -1)
            {
                return;
            }
            //if there is a pickedmods selected then find it's indice and set it's value
            ((MarketData.CompanyListing)colist.SelectedItem).AttachedModifierInterpretations[pickedmods.SelectedIndex] = 0;
        }

        private void pickedmods_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //fill out the bool selector for posbut/negbut
            defbut.IsEnabled = true;
            negbut.IsEnabled = true;
            posbut.IsEnabled = true;
            if (pickedmods.SelectedIndex == -1 || colist.SelectedIndex == -1)
            {
                return;
            }

            switch (((MarketData.CompanyListing)colist.SelectedItem).AttachedModifierInterpretations[pickedmods.SelectedIndex])
            {

                case 0: //default
                    defbut.IsChecked = true;
                    break;
                case 1: //true
                    posbut.IsChecked = true;
                    break;
                case 2: //false
                    negbut.IsChecked = true;
                    break;
            }
            

        }

        private void modpos_Checked(object sender, RoutedEventArgs e)
        {
            if (!(molist.SelectedIndex > -1))
            {
                return;
            }

            //get focused company
            MarketData.ModifierListing ml = (MarketData.ModifierListing)molist.SelectedItem;

            ml.bInterpretation = true;
        }

        private void modneg_Checked(object sender, RoutedEventArgs e)
        {
            if (!(molist.SelectedIndex > -1))
            {
                return;
            }

            //get focused company
            MarketData.ModifierListing ml = (MarketData.ModifierListing)molist.SelectedItem;

            ml.bInterpretation = false;
        }

        private void comadd_Click(object sender, RoutedEventArgs e)
        {
            if (!(colist.SelectedIndex > -1) || !(compick.SelectedIndex > -1))
            {
                return;
            }

            if (colist.SelectedItem == compick.SelectedItem)
            {
                return;
            }

            compicked.Items.Add(compick.SelectedItem);
            MarketData.CompanyListing cl = ((MarketData.CompanyListing)colist.SelectedItem);
            cl.Competitors.Add((MarketData.CompanyListing)compick.SelectedItem);
        }

        private void comremove_Click(object sender, RoutedEventArgs e)
        {
            if (!(colist.SelectedIndex > -1) || !(compicked.SelectedIndex > -1))
            {
                return;
            }
            
            MarketData.CompanyListing cl = ((MarketData.CompanyListing)colist.SelectedItem);
            cl.Competitors.Remove((MarketData.CompanyListing)compicked.SelectedItem);

            compicked.Items.RemoveAt(compicked.SelectedIndex);
        }



        
        private void fsavetype_Click(object sender, RoutedEventArgs e)
        {
            //

            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "finance_generated_header.sch"; // Default file name
            dlg.DefaultExt = ".sch"; // Default file extension
            dlg.Filter = "Marketstar Generated RAGE header (.sch)|*.sch"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                //string filename = dlg.FileName;
                //MarketData.SaveData(dlg.FileName);
                MarketData.OutputHeaders(dlg.FileName);
            }
        }

        private void fsavestring_Click(object sender, RoutedEventArgs e)
        {
            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "finance_system_strings.txt"; // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Finance system strings (.txt)|*.txt"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                //string filename = dlg.FileName;
                //MarketData.SaveData(dlg.FileName);
                MarketData.OutputStrings(dlg.FileName);
            }
        }

        private void fsavexml_Click(object sender, RoutedEventArgs e)
        {
            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "finance_system_online_entries.xml"; // Default file name
            dlg.DefaultExt = ".xml"; // Default file extension
            dlg.Filter = "Finance system strings (.xml)|*.xml"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                //string filename = dlg.FileName;
                //MarketData.SaveData(dlg.FileName);
                MarketData.OutputOnlineModifiers(dlg.FileName);

                
            }
        }

        private void fsaveoverview_Click(object sender, RoutedEventArgs e)
        {
            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "test.txt"; // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Finance system online overview (.txt)|*.txt"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                //string filename = dlg.FileName;
                //MarketData.SaveData(dlg.FileName);
                MarketData.OutPutOverviewDocument(dlg.FileName);
            }
        }

        private void modpos_Checked_1(object sender, RoutedEventArgs e)
        {

        }

        private void defbut_Checked_1(object sender, RoutedEventArgs e)
        {

        }

        private void fimpoverview_Click(object sender, RoutedEventArgs e)
        {
            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "OnlineOverview.txt"; // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Finance system strings (.txt)|*.txt"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                //string filename = dlg.FileName;
                //MarketData.SaveData(dlg.FileName);
                //MarketData.OutPutMethodDocument(dlg.FileName);
                MarketData.OutPutNewMethodDocument(dlg.FileName);
            }
        }

        private void sortyo_Click(object sender, RoutedEventArgs e)
        {
            //molist.
            //MarketData.ModifierListing ml = (MarketData.ModifierListing)molist.Items[];
            /*
            if (molist.Items.Count < 2)
            {
                return;
            }

            if (molist.Items.CanSort)
            {
                molist.Items.
            }*/

            molist.Items.SortDescriptions.Clear();
            molist.Items.SortDescriptions.Add(new SortDescription(null, ListSortDirection.Ascending));
            molist.ScrollIntoView(molist.SelectedItem);



        }

        private void fimpovernotes_click(object sender, RoutedEventArgs e)
        {
            //output warning document

            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "warningdoc.txt"; // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Finance system strings (.txt)|*.txt"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                //string filename = dlg.FileName;
                //MarketData.SaveData(dlg.FileName);
                MarketData.OutPutWarningDocument(dlg.FileName);
            }

        }

        private void creatcsv_click(object sender, RoutedEventArgs e)
        {

            //output warning document

            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "warningdoc.txt"; // Default file name
            dlg.DefaultExt = ".csv"; // Default file extension
            dlg.Filter = "Finance system strings (.csv)|*.csv"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                //string filename = dlg.FileName;
                //MarketData.SaveData(dlg.FileName);
                MarketData.OutputOverviewCsv(dlg.FileName);
            }


        }

        private void createCsvForAnalytics_click(object sender, RoutedEventArgs e)
        {
            // Configure save file dialog box
            var dlg = new Microsoft.Win32.SaveFileDialog
            {
                FileName = "modifiers.txt",
                DefaultExt = ".txt",
                Filter = "Stock modifiers for analytics config (.txt)|*.txt",
            };

            // Show save file dialog box
            var result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                MarketData.OutputCsvForAnalytics_Online(dlg.FileName);
            }
        }
        

        private void tabpanel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void combox_TextChanged(object sender, TextChangedEventArgs e)
        {

            //combox.Text

            if (colist.SelectedItems.Count < 1)
            {
                return;
            }

            
            ((MarketData.CompanyListing)colist.SelectedItems[0]).SetComment(combox.Text);

        }

        private void bullval_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (colist.SelectedItems.Count < 1){return;}


            MarketData.CompanyListing cl = (MarketData.CompanyListing)colist.SelectedItem;
            float val = 0.0f;
            if (float.TryParse(bullval.Text, out val))
            {
                cl.bullishness = val;
            }


        }

        private void bearval_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (colist.SelectedItems.Count < 1) { return; }


            MarketData.CompanyListing cl = (MarketData.CompanyListing)colist.SelectedItem;
            float val = 0.0f;
            if (float.TryParse(bearval.Text, out val))
            {
                cl.bearishness = val;
            }

        }

        private void ceilval_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (colist.SelectedItems.Count < 1) { return; }


            MarketData.CompanyListing cl = (MarketData.CompanyListing)colist.SelectedItem;
            float val = 0.0f;
            if (float.TryParse(ceilval.Text, out val))
            {
                cl.ceiling = val;
            }

        }

        private void floval_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (colist.SelectedItems.Count < 1) { return; }


            MarketData.CompanyListing cl = (MarketData.CompanyListing)colist.SelectedItem;
            float val = 0.0f;
            if (float.TryParse(floval.Text, out val))
            {
                cl.floor = val;
            }

        }

        private void stepval_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (colist.SelectedItems.Count < 1) { return; }


            MarketData.CompanyListing cl = (MarketData.CompanyListing)colist.SelectedItem;
            float val = 0.0f;
            if (float.TryParse(stepval.Text, out val))
            {
                cl.stepcap = val;
            }

        }

        private void noival_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (colist.SelectedItems.Count < 1) { return; }


            MarketData.CompanyListing cl = (MarketData.CompanyListing)colist.SelectedItem;
            float val = 0.0f;
            if (float.TryParse(noival.Text, out val))
            {
                cl.noisiness = val;
            }

        }

        private void statisfloatprof_Checked(object sender, RoutedEventArgs e)
        {
            if (!(molist.SelectedIndex > -1))
            {
                return;
            }

            MarketData.ModifierListing ml = (MarketData.ModifierListing)molist.SelectedItem;

            ml.bStatIsFloatProfType = (bool)statisfloatprof.IsChecked;


        }

        private void idvval_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (colist.SelectedItems.Count < 1) { return; }

            MarketData.CompanyListing cl = (MarketData.CompanyListing)colist.SelectedItem;
            float val;
            if (float.TryParse(idvval.Text, out val))
            {
                cl.IDV = val;
            }
        }

        private void iphvval_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (colist.SelectedItems.Count < 1) { return; }

            MarketData.CompanyListing cl = (MarketData.CompanyListing)colist.SelectedItem;
            float val;
            if (float.TryParse(iphvval.Text, out val))
            {
                cl.IPHV = val;
            }
        }

        private void isavval_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (colist.SelectedItems.Count < 1) { return; }

            MarketData.CompanyListing cl = (MarketData.CompanyListing)colist.SelectedItem;
            float val;
            if (float.TryParse(isavval.Text, out val))
            {
                cl.ISAV = val;
            }
        }

        private void iabsval_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (colist.SelectedItems.Count < 1) { return; }

            MarketData.CompanyListing cl = (MarketData.CompanyListing)colist.SelectedItem;
            float val;
            if (float.TryParse(iabsval.Text, out val))
            {
                cl.IABS = val;
            }
        }

        private void ifbbval_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (colist.SelectedItems.Count < 1) { return; }

            MarketData.CompanyListing cl = (MarketData.CompanyListing)colist.SelectedItem;
            float val;
            if (float.TryParse(ifbbval.Text, out val))
            {
                cl.IFBB = val;
            }
        }

        private void icbbval_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (colist.SelectedItems.Count < 1) { return; }

            MarketData.CompanyListing cl = (MarketData.CompanyListing)colist.SelectedItem;
            float val;
            if (float.TryParse(icbbval.Text, out val))
            {
                cl.ICBB = val;
            }
        }
    }
}
