﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Stockstar
{
    public partial class MarketData
    {
        private static MarketData myInstance = null;

        public static List<CompanyListing> getCompanyList
        {
            get
            {
                if(myInstance == null)
                {
                    return null;
                }

                return myInstance.Companies;
            }
        }
        public static List<ModifierListing> getModifierList
        {
            get
            {
                if (myInstance == null)
                {
                    return null;
                }

                return myInstance.Modifiers;
            }
        }


        public static void Start()
        {
            if (myInstance != null)
            {
                return;
            }
            myInstance = new MarketData();
        }

        internal static void LoadData(string p)
        {
            myInstance.Load(p);
        }



        internal static void SaveData(string p)
        {
            myInstance.Save(p);
        }

        private void Save(string p)
        {
            //throw new NotImplementedException();
            StreamWriter w = new StreamWriter(p);

            //write everything
            //w.Write();

            //companies int
            w.WriteLine(Companies.Count + ",");
            //modifiers int
            w.WriteLine(Modifiers.Count + ",");


            if ((Companies.Count == 0) && (Modifiers.Count == 0))
            {
                //nothing to write
                w.Close();
                return;
            }


            //save names
            foreach (CompanyListing c in Companies)
            {
                //public String Name;
                c.Name.Replace(",", "");
                w.WriteLine(c.nameraw + ",");
                //public String ShortTag;
                c.ShortTag.Replace(",", "");
                w.WriteLine(c.ShortTag + ",");
                //public String EnumString;
                c.EnumString.Replace(",", "");
                w.WriteLine(c.EnumString + ",");
            }
            foreach (ModifierListing m in Modifiers)
            {
                //String Name; // long enum name
                m.Name.Replace(",", "");
                w.WriteLine(m.Name + ",");
                //String GeneratedEnum;
                m.GeneratedEnum.Replace(",", "");
                w.WriteLine(m.GeneratedEnum + ",");
            }

            //save bools
            //Companies.Count number sets of 2 bools
            //public bool bAutoGenEnum;
            //public bool bOnline;
            foreach (CompanyListing c in Companies)
            {

                w.WriteLine(c.bAutoGenEnum + ",");
                w.WriteLine(c.bOnline + ",");
            }

            //Modifiers.Count sets of 1 bool
            //public bool bAutoGenEnum;
            foreach (ModifierListing m in Modifiers)
            {
                w.WriteLine(m.bAutoGenEnum + ",");
            }

            //save company modifier list
            foreach (CompanyListing c in Companies)
            {
                w.WriteLine(c.AttachedModifiers.Count + ",");
                foreach (ModifierListing m in c.AttachedModifiers)
                {
                    w.WriteLine(Modifiers.IndexOf(m) + ",");
                }
            }

            //start price and sensitivity
            foreach (CompanyListing c in Companies)
            {
                w.WriteLine(c.fStartingPrice + ",");
                w.WriteLine(c.fStartingSensitivity + ",");
            }
            //modifier scale
            foreach (ModifierListing m in Modifiers)
            {
                w.WriteLine(m.fApplicationScale + ",");
            }

            //save modifier shared mode
            foreach (ModifierListing m in Modifiers)
            {
                w.WriteLine(m.bShared + ",");
            }

            //Save modifier interpretations
            foreach (CompanyListing c in Companies)
            {
                foreach (int i in c.AttachedModifierInterpretations)
                {
                    w.WriteLine(i + ",");
                }
            }

            foreach (ModifierListing m in Modifiers)
            {
                w.WriteLine(m.bInterpretation + ",");
            }

            foreach (CompanyListing c in Companies)
            {
                //write number of competitors followed by competitor indices

                w.WriteLine(c.Competitors.Count + ",");


                foreach (CompanyListing cp in c.Competitors)
                {
                    w.WriteLine(Companies.IndexOf(cp) +  ",");
                }
            }

            foreach (ModifierListing m in Modifiers)
            {
                w.WriteLine(m.bExtEnum + ",");
                w.WriteLine(m.SpStatsEnumOverride + ",");
            }


            foreach (CompanyListing c in Companies)
            {
                if (c.comment != null)
                {
                    w.WriteLine(Companies.IndexOf(c) + ":");
                    w.WriteLine(c.comment + ",");
                }
            }


            w.WriteLine("!SYS!STATSEC!BEGIN!,");//dump all the stat floats in sets of 6

            foreach (CompanyListing c in Companies)
            {
                w.Write(c.bearishness);
                w.Write(":");
                w.Write(c.bullishness);
                w.Write(":");
                w.Write(c.ceiling);
                w.Write(":");
                w.Write(c.floor);
                w.Write(":");
                w.Write(c.stepcap);
                w.Write(":");
                w.Write(c.noisiness);
                w.Write(":");

                // Add support for optional constants
                w.Write(c.IDV); w.Write(":");
                w.Write(c.IPHV); w.Write(":");
                w.Write(c.ISAV); w.Write(":");
                w.Write(c.IABS); w.Write(":");
                w.Write(c.IFBB); w.Write(":");
                w.Write(c.ICBB); w.Write(":\n");
            }


            //now for each stat dump the bStatIsFloatProfType bool if tag is present
            w.WriteLine("!SYS!PROFFSPEC!BEGIN!,");

            foreach (ModifierListing m in Modifiers)
            {
                w.WriteLine(m.bStatIsFloatProfType + ",");
            }

            w.Close();
        }

        private void Load(string p)
        {
            //format 1

            //values comma separated
            //newlines ignored

            //List<CompanyListing> Companies = null;
            //List<ModifierListing> Modifiers = null;

            //length of companies
            //length of modifiers


            //read data

            //public bool bAutoGenEnum;
            //public bool bOnline;

            //lastly store any additional datatypes at the end
            //if not found then set to defaults as file is an erlier version

            StreamReader r = new StreamReader(p);
            string massdump = r.ReadToEnd();
            r.Close();
            massdump = massdump.Replace('\n', ' ');
            massdump = massdump.Replace('\t', ' ');
            massdump = massdump.Replace('\r', ' ');
            massdump = massdump.Replace(",,", ",");
            massdump = massdump.Replace(" ", "");
            string[] split = massdump.Split(',');

            int iCaret = 0;
            //read 2 ints
            if (split.Length < 3)
            {
                //read failed, file too short, show error
                return;
            }
            int total_companies = int.Parse(split[iCaret]); ++iCaret;
            int total_modifiers = int.Parse(split[iCaret]); ++iCaret;

            //clear the lists
            Companies.Clear();
            Modifiers.Clear();

            //read the names
            for (int i = 0; i < total_companies; ++i)
            {
                CompanyListing cl = new CompanyListing();

                //public String Name;
                cl.Name = split[iCaret];
                ++iCaret;
                //public String ShortTag;
                cl.ShortTag = split[iCaret];
                ++iCaret;
                //public String EnumString;
                cl.EnumString = split[iCaret];
                ++iCaret;

                Companies.Add(cl);
            }
            for (int i = 0; i < total_modifiers; ++i)
            {
                ModifierListing ml = new ModifierListing();

                //String Name; // long enum name
                ml.Name = split[iCaret];
                ++iCaret;
                //String GeneratedEnum;
                ml.GeneratedEnum = split[iCaret];
                ++iCaret;

                Modifiers.Add(ml);
            }


            
            //load bools
            //total_companies number sets of 2 bools
            //public bool bAutoGenEnum;
            //public bool bOnline;
            for (int i = 0; i < total_companies; ++i)
            {
                Companies[i].bAutoGenEnum = bool.Parse(split[iCaret]);
                ++iCaret;

                Companies[i].bOnline = bool.Parse(split[iCaret]);
                ++iCaret;
            }


            //total_modifiers sets of 1 bool
            //public bool bAutoGenEnum;
            for (int i = 0; i < total_modifiers; ++i)
            {
                Modifiers[i].bAutoGenEnum = bool.Parse(split[iCaret]);
                ++iCaret;
            }

            //load company modifier lists
            //total_companies

            for (int i = 0; i < total_companies; ++i)
            {
                //read number
                int modstoadd = int.Parse(split[iCaret]);
                ++iCaret;

                for (int j = 0; j < modstoadd; ++j)
                {
                    //read modifier indices and build lists
                    int indice = int.Parse(split[iCaret]);
                    ++iCaret;

                    Companies[i].AttachedModifiers.Add(Modifiers[indice]);
                    Companies[i].AttachedModifierInterpretations.Add(0);
                }
            }
            
            //start price and sensitivity
            for (int i = 0; i < total_companies; ++i)
            {
                Companies[i].fStartingPrice = float.Parse(split[iCaret]);
                    ++iCaret;
                Companies[i].fStartingSensitivity = float.Parse(split[iCaret]);
                    ++iCaret;
            }
            //modifier scale
            for (int i = 0; i < total_modifiers; ++i)
            {
                Modifiers[i].fApplicationScale = float.Parse(split[iCaret]);
                ++iCaret;
            }
            //modifier switch
            for (int i = 0; i < total_modifiers; ++i)
            {
                Modifiers[i].bShared = bool.Parse(split[iCaret]);
                ++iCaret;
            }
            
            //load modifier interpretations
            for (int i = 0; i < total_companies; ++i)
            {
                //for each modifier in this company
                //Companies[i].AttachedModifiers.Add(Modifiers[indice]);
                foreach (ModifierListing m in Companies[i].AttachedModifiers)
                {
                    int indice = Companies[i].AttachedModifiers.IndexOf(m);
                    int value = int.Parse(split[iCaret]);
                    ++iCaret;

                    Companies[i].AttachedModifierInterpretations[indice] = value;
                }
            }

            //modifier interpretation
            for (int i = 0; i < total_modifiers; ++i)
            {
                Modifiers[i].bInterpretation = bool.Parse(split[iCaret]);
                ++iCaret;
            }
            
            for (int i = 0; i < total_companies; ++i)
            {
                int icompanies = int.Parse(split[iCaret]);
                ++iCaret;

                for (int j = 0; j < icompanies; ++j)
                {
                    int indexin = int.Parse(split[iCaret]);
                    Companies[i].Competitors.Add(Companies[indexin]);
                    ++iCaret;
                }
                    
            }



            //optional section with override enums added to format spec on 14/08/2012

            if ((split.Length - iCaret) > Modifiers.Count*2)
            {
                //attempt to load enum overrides
                for (int i = 0; i < total_modifiers; ++i)
                {
                    Modifiers[i].bExtEnum = bool.Parse(split[iCaret]);
                    ++iCaret;
                    Modifiers[i].SpStatsEnumOverride = split[iCaret];
                    ++iCaret;
                }
            }


            //extra section with comments for companies added to the spec on the 23/08/2012

            //until iCaret == split.Length
            bool bailcomments = false;
            while (iCaret != split.Length && !bailcomments)
           {
               if ((split[iCaret].Contains("!SYS!STATSEC!BEGIN!")))
               {
                   bailcomments = true;
               }
               else
               {
                   string[] f = split[iCaret].Split(':');

                   int cindex = -1;

                   if (int.TryParse(f[0], out cindex))
                   {
                       Companies[cindex].SetComment(f[1]);
                   }
                   ++iCaret;
               }
               
           }

            if (!(iCaret < split.Length))
            {
                return;//done
            }
            //otherwise check for newsec
            if ((split[iCaret].Contains("!SYS!STATSEC!BEGIN!")))
            {
                //try new load of stats for all companies

                ++iCaret;
                string[] statvals = split[iCaret].Split(':');
                
                for (int i = 0; i < total_companies; ++i)
                {
                    var areConstantsAvailable = (statvals.Length > (6 * total_companies) + 1);

                    int at = areConstantsAvailable ? (i * 12) : (i * 6);
                    Companies[i].bullishness = float.Parse(statvals[at]);
                    Companies[i].bearishness = float.Parse(statvals[at+1]);
                    Companies[i].ceiling = float.Parse(statvals[at+2]);
                    Companies[i].floor = float.Parse(statvals[at+3]);
                    Companies[i].stepcap = float.Parse(statvals[at+4]);
                    Companies[i].noisiness = float.Parse(statvals[at + 5]);

                    // Add support for loading optional constants (if available)
                    if (areConstantsAvailable)
                    {
                        Companies[i].IDV = float.Parse(statvals[at + 6]);
                        Companies[i].IPHV = float.Parse(statvals[at + 7]);
                        Companies[i].ISAV = float.Parse(statvals[at + 8]);
                        Companies[i].IABS = float.Parse(statvals[at + 9]);
                        Companies[i].IFBB = float.Parse(statvals[at + 10]);
                        Companies[i].ICBB = float.Parse(statvals[at + 11]);
                    }
                }

            }


            if (split[iCaret].Contains("!SYS!PROFFSPEC!BEGIN!"))
            {
                ++iCaret;

                for (int i = 0; i < total_modifiers; ++i)
                {
                    Modifiers[i].bStatIsFloatProfType = bool.Parse(split[iCaret]);
                    ++iCaret;
                }

            }


        }


        internal static void OutputHeaders(string file)
        {

            StreamWriter w = new StreamWriter(file);

            w.Write("///Automatically generated header.\n\n");
            w.Write("USING \"stats_enums.sch\"\n\n");


            w.Write("/// List of company enums.\n\n");
            w.Write(genScriptCompanyList());

            w.Write("\n");

            w.Write("///List of modifier enums.\n\n");
            w.Write(genScriptModfierList());

            w.Write("\n");

            w.Write("/// Initialisation and modifier update functionality.\n\n");
            w.Write(genStructsAndInit());

            w.Close();
        }


        internal static void OutputStrings(string file)
        {
            StreamWriter w = new StreamWriter(file);

            

            w.Write(genStrings());
            w.Close();
        }

        internal static void OutputOnlineModifiers(string p)
        {
            if (myInstance == null)
            {
                return;
            }

            StreamWriter w = new StreamWriter(p);
            w.Write(myInstance.genOnlineXMLEntries());
            w.Close();
        }


        internal static void OutPutOverviewDocument(string p)
        {
            if (myInstance == null)
            {
                return;
            }

            StreamWriter w = new StreamWriter(p);
            w.Write(myInstance.genSystemDocument());
            w.Close();
        }


        internal static void OutPutMethodDocument(string p)
        {
            if (myInstance == null)
            {
                return;
            }

            StreamWriter w = new StreamWriter(p);
            w.Write(myInstance.genModifierInfo());
            w.Close();
        }
        internal static void OutPutNewMethodDocument(string p)
        {
            if (myInstance == null)
            {
                return;
            }

            StreamWriter w = new StreamWriter(p);
            w.Write(myInstance.genOnlineStatAssociations());
            w.Close();
        }


        internal static void OutPutWarningDocument(string p)
        {
            if (myInstance == null)
            {
                return;
            }

            StreamWriter w = new StreamWriter(p);
            w.Write(myInstance.genWarningInfo());
            w.Close();
        }
        internal static void OutputOverviewCsv(string p)
        {
            if (myInstance == null)
            {
                return;
            }

            StreamWriter w = new StreamWriter(p);
            w.Write(myInstance.genOverviewCsv());
            w.Close();
        }

        internal static void OutputCsvForAnalytics_Online(string p)
        {
            if (myInstance == null)
            {
                return;
            }

            using(StreamWriter w = new StreamWriter(p))
            {
                w.Write(myInstance.generateCsvForAnalytics(true));
            }
        }

        internal static void OutputCsvForAnalytics_Offline(string p)
        {
            if (myInstance == null)
            {
                return;
            }

            using(StreamWriter w = new StreamWriter(p))
            {
                w.Write(myInstance.generateCsvForAnalytics(false));
            }
        }

        internal static void OutputCsvForAnalytics_Both(string p)
        {
            if (myInstance == null)
            {
                return;
            }

            using(StreamWriter w = new StreamWriter(p))
            {
                w.Write(myInstance.generateCsvForAnalytics(null));
            }
        }


    }
}
