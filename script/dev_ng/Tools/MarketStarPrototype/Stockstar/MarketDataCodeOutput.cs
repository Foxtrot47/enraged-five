﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Stockstar
{
    public partial class MarketData
    {



        //enum list for companies 
        //ENUM BAWSAQ_COMPANIES
        //BS_CO_

        public static string genScriptCompanyList()
        {
            return myInstance.ScriptCompanyList();
        }

        public string ScriptCompanyList()
        {
            string s = "";

            s += "\tENUM BAWSAQ_COMPANIES\n";

            foreach (CompanyListing c in Companies)
            {
                s += "\t\tBS_CO_" + c.EnumString + ",// " + c.Name;

                if (c.bOnline)
                {
                    s += "// Online";
                }
                else
                {
                    s += "// Offline";
                }

                s += "\n";
            }
            s += "\t\tBS_CO_TOTAL_LISTINGS\n";
            s += "\tENDENUM\n";

            return s;
        }

        //enum list for modifiers
        //ENUM BSMF_TYPES
        //BSMF_

            //remember to build competitor modifiers
        public static string genScriptModfierList()
        {
            return myInstance.ScriptModfierList();
        }


        public void RemoveModifierDuplicates()
        {
            //TODO
            int totalmatches = 0;
            for (int i = 0; i < Modifiers.Count; ++i)
            {
                int matchcounter = 0;
                for (int j = 0; j < Modifiers.Count; ++j)
                {
                    if (i != j)
                    {
                        if (Modifiers[i].GeneratedEnum.CompareTo(Modifiers[j].GeneratedEnum) == 0)
                        {
                            Modifiers[j].GeneratedEnum += matchcounter;
                            ++matchcounter;
                            ++totalmatches;
                        }

                    }
                }
            }

            if (totalmatches != 0)
            {
                MessageBox.Show("Warning: " + totalmatches + " duplicate modifier enum names detected and changed.");
            }

        }

        public string ScriptModfierList()
        {
            //
            RemoveModifierDuplicates();

            string s = "";

            s += "\tENUM BSMF_TYPES\n";

            foreach (ModifierListing m in Modifiers)
            {

                
                bool doAdd = false;

                if (!m.bExtEnum)//pure modifiers are all included.
                {
                    doAdd = true;
                }


                if (!doAdd)//Filter modifiers that are online only 
                {
                    foreach (CompanyListing c in Companies)
                    {
                        if (!c.bOnline)
                        {
                            if (c.AttachedModifiers.Contains(m))
                            {
                                doAdd = true;
                            }
                        }
                    }
                }


     
                if (doAdd)
                {
                    if (!m.bExtEnum)//doesn't have an already existing modifier enum
                    {
                        //s += "\t\t" + c.EnumString + ",\n";
                        if (m.bShared == true)
                        {
                            s += "\t\tBSMF_" + m.GeneratedEnum + ", //" + m.Name + "\n";

                        }
                        else
                        {
                            //if split then create new entries for each company that uses
                            foreach (CompanyListing c in Companies)
                            {
                                if (c.AttachedModifiers.Contains(m))
                                {
                                    //company specific version
                                    s += "\t\tBSMF_" + m.GeneratedEnum + "_FOR_" + c.EnumString + ",// " + m.Name + " : used by " + c.Name + "\n";
                                }
                            }
                        }
                    }
                    else
                    {
                        //its a used ext enum
                        s += "\t\tBSMF_" + m.GeneratedEnum + ", //" + m.Name + " //this is an external enum used by offline\n";
                    }
                }
            }
            s += "\t\tBSMF_TOTAL_INDICES\n";
            s += "\tENDENUM\n";

            return s;
        }


        //update
        /*
        public static string genModifierUploadCall()
        {
            return myInstance.ModifierUploadCall();
        }

        public string ModifierUploadCall()
        {
            string s = "PROC INFORM_BAWSAQ_OF_MODIFIER(BSMF_TYPES modifier, INT degree)\n";


            s += "\t\n";


            s += "\t\n";


            s += "\tSWITCH modifier\n";

            s += "\t\tCASE\n";
            s += "\t\tBREAK\n";


            //for each modifier 


                
            s += "\tENDSWITCH\n";

            s += "ENDPROC\n";
            return s;
        }
        */





        //intialisation function
        //PROC BAWSAQ_INTERNAL_INTIALISE_COMPANY_LISTS()

        //each company
            //public String Name;
            //public String ShortTag;
            //public String EnumString;

            //public bool bOnline;

            //public float fStartingPrice = 1.0f;
            //public float fStartingSensitivity = 0.02f;

        //each modifier
            //public String Name; // long enum name
            //public String GeneratedEnum;
            //public bool bAutoGenEnum;

            //public bool bInterpretation = true;//positive or negative
            //public float fApplicationScale = 1.0f;
 
            public static string genStructsAndInit()
            {
                return myInstance.StructsAndInit();
            }

            public string StructsAndInit()
            {
                string s = "CONST_INT MAX_STOCK_PRICE_LOG_ENTRIES 14 //two logs a day\n\n";

                List<string> refs = myInstance.PurgeDuplicateStrings();

                //public String Name;
                //public String ShortTag;
                //public String EnumString;

                //public bool bOnline;

                //public float fStartingPrice = 1.0f;
                //public float fStartingSensitivity = 0.02f;

                s += "STRUCT BAWSAQ_LISTING_STRUCT\n";
                s += "\t\tTEXT_LABEL sLongName\n";
                s += "\t\tTEXT_LABEL sShortName\n";
                //s += "\t\tTEXT_LABEL sCoDesc\n\n";
                s += "\t\tBOOL bOnlineStock\n\n";
                s += "\t\tFLOAT fCurrentPrice\n";
                s += "\t\tFLOAT fLastPriceChange\n";

                s += "\t\tFLOAT fUniversalPriceModifier\n";
                s += "\t\tFLOAT fUPMAppliedLastUpdate\n";
                s += "\t\tINT iVolumeTraded\n";
                s += "\t\tINT iLogIndexCaret\n\n";

                s += "\t\tFLOAT fHighValue\n";
		        s += "\t\tFLOAT fLowValue\n\n";


                s += "\t\tFLOAT fLastPriceChangeInPercent\n\n";

                s += "\t\tFLOAT LoggedPrices[MAX_STOCK_PRICE_LOG_ENTRIES]\n";
                //+= "\t\tINT LoggedVolumes[MAX_STOCK_PRICE_LOG_ENTRIES]\n";

                //online lookup index int
                s += "\t\tINT StatIndex//for online index is in g_BS_OIndexData, for offline g_BS_OfAttribData\n";

                s += "ENDSTRUCT\n\n";



                s += "BAWSAQ_LISTING_STRUCT g_BS_Listings[BS_CO_TOTAL_LISTINGS]\n\n\n";


                s += "//Online stat data\n";
                s += "STRUCT BAWSAQ_ONLINE_STAT_STUCT\n";


                s += "\t\tSTATSENUM OnlinePriceStat\n";
                s += "\t\tSTATSENUM OnlineVolumeUploadStat\n";
                s += "\t\tSTATSENUM OnlineVolumeDownloadStat\n";
                s += "\t\tSTATSENUM OnlineCharOwned[3]//mike//frank//trev\n";

               // s += "\t\t#IF IS_DEBUG_BUILD\n";
               // s += "\t\tSTATSENUM OnlinePriceStatDebug\n";
                //s += "\t\tSTATSENUM OnlineVolumeUploadStatDebug\n";
                //s += "\t\tSTATSENUM OnlineVolumeDownloadStatDebug\n";
                //s += "\t\tSTATSENUM OnlineCharOwnedDebug[3]//mike//frank//trev\n";
               // s += "\t\t#ENDIF\n";

                s += "ENDSTRUCT\n\n";




                List<CompanyListing> olist = new List<CompanyListing>();
                List<CompanyListing> oflist = new List<CompanyListing>();
                foreach (CompanyListing cl in Companies)
                {
                    if (cl.bOnline)
                    {
                        olist.Add(cl);
                    }
                    else
                    {
                        oflist.Add(cl);
                    }
                }


                s += "BAWSAQ_ONLINE_STAT_STUCT g_BS_OIndexData[" + olist.Count + "]\n\n\n";


                s += "STRUCT BAWSAQ_OFFLINE_ATTRIB_STUCT // contains the calculation settings needed for new price generation // only offline companies have this\n";
                s += "\t\tFLOAT fCurrentInclination//This is the only thing that would need to be saved from this list\n\n";
                s += "\t\tFLOAT fBearish\n";
                s += "\t\tFLOAT fBullish\n";
                s += "\t\tFLOAT fCeiling\n";
                s += "\t\tFLOAT fFloor\n";
                s += "\t\tFLOAT fStepCap\n";
                s += "\t\tFLOAT fNoise\n";
                s += "ENDSTRUCT\n\n";



                s += "BAWSAQ_OFFLINE_ATTRIB_STUCT g_BS_OfAttribData[" + oflist.Count + "]\n\n\n";


                s += "STRUCT BAWSAQ_MODIFIER_STRUCT\n";
                s += "\tbool bOnline\n";
                s += "\tSTATSENUM uploadTo\n";

               // s += "\t#IF IS_DEBUG_BUILD\n";
               // s += "\tSTATSENUM uploadToDebug\n";
               // s += "\t#ENDIF\n";


                s += "\tbool bReadFromProfile//does this modifier read it's value from a profile stat? \n";
                s += "\tSTATSENUM ProfStatToRead\n";
                s += "\tBOOL bProfIsFloatType\n";
                s += "\tINT iPrev\n";
                s += "\tINT iDelta\n";

                s += "ENDSTRUCT\n\n";


                s += "BAWSAQ_MODIFIER_STRUCT g_BS_Modifiers[BSMF_TOTAL_INDICES]\n\n";
                s += "\t\n\n";
    
                
                //TODO, start init function here

                s += "PROC BAWSAQ_INTIALISE_LISTS()\n\n";



                s += "\tINT i = 0\n";
                s += "\tREPEAT BS_CO_TOTAL_LISTINGS i\n";
                s += "\t\tg_BS_Listings[i].fLastPriceChange = 0.0\n";
                s += "\t\tg_BS_Listings[i].iLogIndexCaret = 0\n";
                s += "\t\tg_BS_Listings[i].iVolumeTraded = 0\n";
                s += "\t\tg_BS_Listings[i].fHighValue = 0\n";
                s += "\t\tg_BS_Listings[i].fLowValue = " + float.MaxValue + "\n";
                s += "\t\tint j = 0\n";
                s += "\t\tREPEAT MAX_STOCK_PRICE_LOG_ENTRIES j\n";
                s += "\t\t\tg_BS_Listings[i].LoggedPrices[j] = 0.0\n";
                //s += "\t\t\tg_BS_Listings[i].LoggedVolumes[j] = 0\n";
                s += "\t\tENDREPEAT\n";
                s += "\tENDREPEAT\n\n";

                //FLOAT fHighValue
                //FLOAT fLowValue

                s += "\ti = 0\n";
                s += "\tREPEAT BSMF_TOTAL_INDICES i\n";
                //s += "\t\tg_BS_Modifiers[i].iChangeThisUpdate = 0\n";
                //s += "\t\tg_BS_Modifiers[i].iChangeLastUpdate = 0\n";
                //s += "\t\tg_BS_Modifiers[i].bOnline = FALSE\n";

                s += "\t\tg_BS_Modifiers[i].bReadFromProfile = FALSE\n";
                s += "\t\t//g_BS_Modifiers[i].ProfStatToRead = FALSE //unset\n";
                s += "\tENDREPEAT\n\n";

                //generate strings for each company using "refs" indexat and "BSS_"

                foreach (CompanyListing cl in Companies)
                {
                    s += "\tg_BS_Listings[" + "BS_CO_" + cl.EnumString + "].sLongName =  \"BSS_BSTR_" + refs.IndexOf(cl.Name) + "\"\n";
                    s += "\tg_BS_Listings[" + "BS_CO_" + cl.EnumString + "].sShortName =  \"BSS_BSTR_" + refs.IndexOf(cl.ShortTag) + "\"\n";

                    //cl.bOnline////BOOL bOnlineStock

                    if (cl.bOnline)
                    {
                        //its an online stock, set the index
                        s += "\tg_BS_Listings[" + "BS_CO_" + cl.EnumString + "].bOnlineStock = TRUE \n";
                        s += "\tg_BS_Listings[" + "BS_CO_" + cl.EnumString + "].StatIndex = " + olist.IndexOf(cl) + " ";
                    }
                    else
                    {
                        //its an offline stock, set the index to point at the right stats field
                        s += "\tg_BS_Listings["+ "BS_CO_" + cl.EnumString +"].bOnlineStock = FALSE \n";
                        s += "\tg_BS_Listings[" + "BS_CO_" + cl.EnumString + "].StatIndex = " + oflist.IndexOf(cl) + " ";
                    }




                    s += "\n";
                }

                //attributes
                for (int i = 0; i < oflist.Count; ++i)
                {
                    s += "\t//Offline price calculation attribs for " + oflist[i].Name + "\n";
                    s += "\tg_BS_OfAttribData[" + i + "].fBullish \t=  " + oflist[i].bullishness +"\t\n";
                    s += "\tg_BS_OfAttribData[" + i + "].fBearish \t=  " + oflist[i].bearishness + "\t\n";
                    s += "\tg_BS_OfAttribData[" + i + "].fCeiling \t=  " + oflist[i].ceiling + "\t\n";
                    s += "\t\tg_BS_OfAttribData[" + i + "].fFloor = " + oflist[i].floor + "  \n";
                    s += "\t\tg_BS_OfAttribData[" + i + "].fStepCap = " + oflist[i].stepcap + "\n";
                    s += "\t\tg_BS_OfAttribData[" + i + "].fNoise = " + oflist[i].noisiness + " \n";
                }
                s += "\n";





                List<string> statnames = GenerateXMLStatNames(false);
                //s += "/* Uncomment the following when the stats are in place\n";
                for (int i = 0; i < ModifierXMLMatchingList.Count; ++i)
                {
                    bool skip = true;
                    //set modifier g_BS_Modifiers[ml.uploadTo = statnames
                    if (ModXMLList[i].bExtEnum)
                    {
                        skip = true;
                    }
                    else
                    {
                        foreach (CompanyListing cl in Companies)
                        {
                            if (cl.bOnline)
                            {
                                if (cl.AttachedModifiers.Contains(ModXMLList[i]))
                                {
                                    skip = false;
                                    break;
                                }
                            }
                        }
                    }


                    if (!skip)
                    {
                        s += "\tg_BS_Modifiers[BSMF_" + ModifierXMLMatchingList[i] + "].uploadTo \t= \t" + statnames[i] + " //" + ModXMLList[i].Name + "\n";
                        s += "\tg_BS_Modifiers[BSMF_" + ModifierXMLMatchingList[i] + "].bOnline \t= TRUE\t\n";


                           // s += "\t#IF IS_DEBUG_BUILD\t\n";
                           // s += "\tg_BS_Modifiers[BSMF_" + ModifierXMLMatchingList[i] + "].uploadToDebug \t= \t" + statnames[i] + "_DEBUG //" + ModXMLList[i].Name + "_DEBUG\n";
                           // s += "\t#ENDIF\t\n";
                      
                    }
                   
                }

              /*
                for (int i = 0; i < ModifierXMLMatchingList.Count; ++i)
                {
                    if (!ModXMLList[i].bExportEnumWasExcluded)
                    {
                        s += "\tg_BS_Modifiers[BSMF_" + ModifierXMLMatchingList[i] + "].bOnline \t= TRUE\t\n";
                        }
                }
                */
                for (int i = 0; i < Modifiers.Count; ++i)
                {
                    if (Modifiers[i].bExtEnum)
                    {


                        //s += "\tbool bReadFromProfile//does this modifier read it's value from a profile stat? \n";
                        //s += "\tSTATSENUM ProfStatToRead\n";

                        bool doUse = false;

                        foreach (CompanyListing c in oflist)
                        {
                            if (c.AttachedModifiers.Contains(Modifiers[i]))
                            {
                                doUse = true;
                            }
                        }

                        if(doUse)
                        {
                            s += "\tg_BS_Modifiers[BSMF_" + Modifiers[i].GeneratedEnum + "].bReadFromProfile \t= TRUE\t\n";

                            s += "\tg_BS_Modifiers[BSMF_" + Modifiers[i].GeneratedEnum + "].ProfStatToRead \t= " + Modifiers[i].SpStatsEnumOverride + "\t\n";

                            s += "\tg_BS_Modifiers[BSMF_" + Modifiers[i].GeneratedEnum + "].bProfIsFloatType \t= " + Modifiers[i].bStatIsFloatProfType + "\t\n";
                            /*
                            if ((!Modifiers[i].bStatIsFloatProfType) && !Modifiers[i].bStatIsFloatProfType)
                            {
                                s += "\t#IF IS_DEBUG_BUILD\t\n";
                                s += "\tg_BS_Modifiers[" + i + "].ProfStatToReadDEBUG \t= " + Modifiers[i].SpStatsEnumOverride + "_DEBUG\t\n";
                                s += "\t#ENDIF\t\n";
                            }*/
                           
                        }



                    }
                }




                    s += "\n";

                //s += "BAWSAQ_ONLINE_STAT_STUCT g_BS_OIndexData[" + olist.Count + "]\n\n\n";

                //for each of these online companies set it's 
                for (int i = 0; i < olist.Count; ++i)
                {
                    /*
                    s += "//Online stat data\n";
                    s += "STRUCT BAWSAQ_ONLINE_STAT_STUCT\n";
                    s += "\t\tSTATSENUM OnlinePriceStat\n";
                    s += "\t\tSTATSENUM OnlineVolumeUploadStat\n";
                    s += "\t\tSTATSENUM OnlineVolumeDownloadStat\n";
                    s += "\t\tSTATSENUM OnlineCharOwned[3]//mike//frank//trev\n";
                    s += "ENDSTRUCT\n\n";
                    */
                    s += "\tg_BS_OIndexData[" + i + "].OnlinePriceStat \t=  SM_PRICE_" + olist[i].EnumString + "\t\n";
                    //s += "\tg_BS_OIndexData[" + i + "].OnlineVolumeUploadStat \t=  " + olist[i].EnumString + "_VOLUP\t\n";
                    //s += "\tg_BS_OIndexData[" + i + "].OnlineVolumeDownloadStat \t=  " + olist[i].EnumString + "_VOLDN\t\n";
                    s += "\t\tg_BS_OIndexData[" + i + "].OnlineCharOwned[0] \t= " + olist[i].EnumString + "_OW0  //mike\n";
                    s += "\t\tg_BS_OIndexData[" + i + "].OnlineCharOwned[1] \t= " + olist[i].EnumString + "_OW1 //frank\n";
                    s += "\t\tg_BS_OIndexData[" + i + "].OnlineCharOwned[2] \t= " + olist[i].EnumString + "_OW2  //trev\n";



                    //s += "\t#IF IS_DEBUG_BUILD\t\n";
                   // s += "\tg_BS_OIndexData[" + i + "].OnlinePriceStatDebug \t=  SM_PRICE_" + olist[i].EnumString + "_DEBUG\t\n";
                    //s += "\tg_BS_OIndexData[" + i + "].OnlineVolumeUploadStatDebug \t=  " + olist[i].EnumString + "_VOLUP_DEBUG\t\n";
                   // s += "\tg_BS_OIndexData[" + i + "].OnlineVolumeDownloadStatDebug \t=  " + olist[i].EnumString + "_VOLDN_DEBUG\t\n";
                   // s += "\t\tg_BS_OIndexData[" + i + "].OnlineCharOwnedDebug[0] \t= " + olist[i].EnumString + "_OW0_DEBUG  //mike\n";
                   // s += "\t\tg_BS_OIndexData[" + i + "].OnlineCharOwnedDebug[1] \t= " + olist[i].EnumString + "_OW1_DEBUG //frank\n";
                   // s += "\t\tg_BS_OIndexData[" + i + "].OnlineCharOwnedDebug[2] \t= " + olist[i].EnumString + "_OW2_DEBUG  //trev\n";
                   // s += "\t#ENDIF\t\n";
                }
                s += "\n";





                //s += "*/ //Uncomment the previous when stats are in place\n";
                s += "ENDPROC\n\n";



                /*
                    PROC BAWSAQ_UPDATE_MODIFIER_PRICE_ASSOCIATIONS()
	                    //build a UPM for each company listing and store it in the data struct
                    ENDPROC
                 */

                //link up stats with modifiers
                //ModifierXMLMatchingList
                s += "/// Auto generated calculation of new UPM for each company\n\n";
                s += "PROC BAWSAQ_GENERATED_APPLY_MODIFIERS_TO_PRICES()\n";

                //TODO, make the new modifier aggregation for the offline stocks

                /*
                s += "\tFLOAT fTotalEffects\n";
                s += "\tFLOAT fModifierSum\n";
                //for each price
                    //apply modifiers using
                    //apply competitors using their fLastPriceChange
                    foreach (CompanyListing cl in Companies)
                    {
                        if (!cl.bOnline)
                        {
                            s += "\t\tfTotalEffects = 0.0\n";
                            s += "\t\tfModifierSum = 0.0\n";
                            //cl.AttachedModifiers
                            //cl.AttachedModifierInterpretations

                            for (int i = 0; i < cl.AttachedModifiers.Count; ++i)
                            {
                                //cl.AttachedModifiers[i].GeneratedEnum

                                if (cl.AttachedModifiers[i].bShared)
                                {
                                    s += "\t\tIF NOT ( g_BS_Modifiers[BSMF_" + cl.AttachedModifiers[i].GeneratedEnum + "].fModValue = 0.0)\n";
                                    s += "\t\t\tfModifierSum += g_BS_Modifiers[BSMF_" + cl.AttachedModifiers[i].GeneratedEnum + "].fModValue\n";
                                }
                                else
                                {
                                    //otherwise 
                                    //
                                    s += "\t\tIF NOT ( g_BS_Modifiers[BSMF_" + cl.AttachedModifiers[i].GeneratedEnum + "_FOR_" + cl.EnumString + "].fModValue = 0.0)\n";
                                    s += "\t\t\tfModifierSum += g_BS_Modifiers[BSMF_" + cl.AttachedModifiers[i].GeneratedEnum + "_FOR_" + cl.EnumString + "].fModValue\n";
                                }


                                s += "\t\tENDIF\n";

                                s += "\t\tfTotalEffects+= 1\n";
                            }

                            //modifiers totalled, now total up for each competitor
                            //using previous upm if != 0
                            foreach (CompanyListing cc in cl.Competitors)
                            {
                                //fUPMAppliedLastUpdate
                                s += "\t\tIF NOT (g_BS_Listings[BS_CO_" + cc.EnumString + "].fUPMAppliedLastUpdate = 0.0)\n";
                                s += "\t\t\tfModifierSum += g_BS_Listings[BS_CO_" + cc.EnumString + "].fUPMAppliedLastUpdate\n";
                                s += "\t\tENDIF\n\n";
                                s += "\t\tfTotalEffects+= 1\n";
                            }

                            s += "\t\tg_BS_Listings[BS_CO_" + cl.EnumString + "].fUniversalPriceModifier =  fModifierSum/fTotalEffects \n";
                            //now divide the modifier sum by the total effects 
                            //for this company and compare to last update's modifier

                            //cl.Competitors //fUPMAppliedLastUpdate
                        }
                    }
                    
                */
                s += "ENDPROC\n";



                return s;
            }


            private static string genStrings()
            {
                return myInstance.Strings();
            }

        /// <summary>
        /// output the strings for the .txt used to make the .gxt
        /// </summary>
        /// <returns></returns>
            public string Strings()
            {
                string s = "\n";


                List<string> refs = myInstance.PurgeDuplicateStrings();

                int icount = 0;
                foreach (string cl in refs)
                {
                    s += "[BSS_BSTR_" + (icount++) + "]\n";
                    string decom = cl.Replace("|",",");
                    s += cl + "\n\n";
                }
                return s;
            }
        /// <summary>
        /// returns a comparison list
        /// </summary>
        /// <returns></returns>
            private List<string> PurgeDuplicateStrings()
            {
               //push all the strings to parse into one list

                List<string> strings = new List<string>();

                //for each company

                foreach (CompanyListing cl in Companies)
                {
                    strings.Add(cl.Name);
                    strings.Add(cl.ShortTag);
                }




                for (int i = 0; i < strings.Count; ++i)
                {
                    
                    for(int j = 0;j < strings.Count;++j)
                    {
                        if (strings[i].CompareTo(strings[j]) == 0)
                        {
                            if (i != j)
                            {
                                //check reference
                                //if (strings[i] != strings[j])
                                //{
                                //refrence doesn't match for matching strings, is duplicate
                                //strings[j] = strings[i];
                                if (i >= j)
                                {
                                    --i;
                                }
                                strings.RemoveAt(j);
                                --j;
                                //}
                            }
                        }
                    }
                }
                foreach (CompanyListing cl in Companies)
                {
                    if (!strings.Contains(cl.Name))
                    {
                        //find and overwrite name
                        cl.Name = strings[FindFirstNameStringMatchAndReturnIndex(strings, cl)];
                    }
                    if (!strings.Contains(cl.ShortTag))
                    {
                        //find and overwrite name
                        cl.ShortTag = strings[FindFirstShortTagStringMatchAndReturnIndex(strings, cl)];
                    }
                }
                return strings;
            }
            
        private int FindFirstNameStringMatchAndReturnIndex(List<string> strings, CompanyListing cl)
        {
            foreach (string s in strings)
            {
                if (cl.Name.CompareTo(s) == 0)
                {
                    return strings.IndexOf(s);
                }
            }
            return -1;
        }
        private int FindFirstShortTagStringMatchAndReturnIndex(List<string> strings, CompanyListing cl)
        {
            foreach (string s in strings)
            {
                if (cl.ShortTag.CompareTo(s) == 0)
                {
                    return strings.IndexOf(s);
                }
            }
            return -1;
        }


        private string genOnlineXMLEntries()
        {
            string s = "";

            //<!-- ONLINE STOCK MARKET MODIFIER STATS -->


                //for each modifier that belongs to an online company
            List<string> EntryNames = GenerateXMLStatNames(false);


            //foreach (string str in EntryNames)
            //{






            s += "\n<!-- ONLINE STOCK MARKET STATS - modifiers  -->\n\n";
            for(int i = 0; i < EntryNames.Count;++i)
            {
                if (!ModXMLIsOverrideList[i])//if its not a stat that already exists in the profile stats
                {
                   // s += "<stat Name=\" SM_" + EntryNames[i] + "\"  Type=\"int\"    Default=\"0\" ses=\"sp_ps\" OmitTimestamp=\"false\"  UserData=\"STAT_FLAG_NEVER_SHOW\"  Owner=\"script\" Comment=\"Stock Modifier\" />\n";

                    s += "<stat Name=\"" + EntryNames[i] + "\"          Type=\"int\"      Group=\"7\"  SaveCategory=\"0\" Default=\"0\" online=\"false\"   profile=\"true\" FlushPriority=\"0\" ServerAuthoritative=\"false\" telemetry=\"false\" community=\"false\" OmitTimestamp=\"false\"  UserData=\"STAT_FLAG_NEVER_SHOW\"  Owner=\"script\" Comment=\"Stock Modifier\" />\r\n";



                }
            }
           // }
            s += "\n<!-- ONLINE STOCK MARKET STATS - prices  -->\n\n";
            //now ouput price entries for each online company
            foreach(CompanyListing cl in Companies)
            {

                if (cl.bOnline)
                {
                    //		<stat Name="SM_RON_PRICE"               Type="float"    Group="7"  SaveCategory="0" Default="0" online="false"   profile="false" FlushPriority="0" ServerAuthoritative="false" telemetry="false" community="true" UserData="STAT_FLAG_NEVER_SHOW"  Owner="script" Comment="Comunity stat" />
                    s += "<stat Name=\"SM_PRICE_" + cl.EnumString + "\"               Type=\"float\"    Group=\"7\"  SaveCategory=\"0\" Default=\"0\" online=\"false\"   profile=\"false\" FlushPriority=\"0\" ServerAuthoritative=\"false\" telemetry=\"false\" community=\"true\" UserData=\"STAT_FLAG_NEVER_SHOW\"  Owner=\"script\" Comment=\"Comunity stat\" />\r\n";
                }
            }


            s += "\n<!-- ONLINE STOCK MARKET STATS - volumes  -->\n\n";
            //output volup and voldown for each online
            //foreach (CompanyListing cl in Companies)
           // {

                //if (cl.bOnline)
               // {
               //     s += "<stat Name=\"" + cl.EnumString + "_VOLUP\"               Type=\"int\"      Group=\"7\"  SaveCategory=\"0\" Default=\"0\" online=\"false\"   profile=\"true\" FlushPriority=\"0\" ServerAuthoritative=\"false\" telemetry=\"false\" community=\"false\" UserData=\"STAT_FLAG_NEVER_SHOW\"  Owner=\"script\" Comment=\"Stock market volume data\" />\r\n";
               // }
          //  }
           // foreach (CompanyListing cl in Companies)
           // {
                //if (cl.bOnline)
               //{
                    //s += "<stat Name=\"" + cl.EnumString + "_VOLDN\"               Type=\"int\"      Group=\"7\"  SaveCategory=\"0\" Default=\"0\" online=\"false\"   profile=\"false\" FlushPriority=\"0\" ServerAuthoritative=\"false\" telemetry=\"false\" community=\"true\" UserData=\"STAT_FLAG_NEVER_SHOW\"  Owner=\"script\" Comment=\"Stock market volume data\" />\r\n";
               // }
           // }
            s += "\n<!-- ONLINE STOCK MARKET STATS - owned amounts  -->\n\n";
            //output new set of 3 stats for owned per each online //717860
            foreach (CompanyListing cl in Companies)
            {
                if (cl.bOnline)
                {
                    s += "<stat Name=\"" + cl.EnumString + "_OW0\"               Type=\"int\"      Group=\"7\"  SaveCategory=\"0\" Default=\"0\" online=\"false\"   profile=\"true\" FlushPriority=\"0\" ServerAuthoritative=\"false\" telemetry=\"false\" community=\"false\" UserData=\"STAT_FLAG_NEVER_SHOW\"  Owner=\"script\" Comment=\"Stock market volume data\" />\r\n";
                    s += "<stat Name=\"" + cl.EnumString + "_OW1\"               Type=\"int\"      Group=\"7\"  SaveCategory=\"0\" Default=\"0\" online=\"false\"   profile=\"true\" FlushPriority=\"0\" ServerAuthoritative=\"false\" telemetry=\"false\" community=\"false\" UserData=\"STAT_FLAG_NEVER_SHOW\"  Owner=\"script\" Comment=\"Stock market volume data\" />\r\n";
                    s += "<stat Name=\"" + cl.EnumString + "_OW2\"               Type=\"int\"      Group=\"7\"  SaveCategory=\"0\" Default=\"0\" online=\"false\"   profile=\"true\" FlushPriority=\"0\" ServerAuthoritative=\"false\" telemetry=\"false\" community=\"false\" UserData=\"STAT_FLAG_NEVER_SHOW\"  Owner=\"script\" Comment=\"Stock market volume data\" />\r\n";
                }
            }


            return s;
        }
        public static List<string> getGenerateXMLStatNames(bool withcomments)
        {
            return myInstance.GenerateXMLStatNames(withcomments);
        }


        public List<string> ModifierXMLMatchingList;
        public List<ModifierListing> ModXMLList;
        public List<bool> ModXMLIsOverrideList;

        public List<string> GenerateXMLStatNames( bool withcomments)
        {
            List<string> EntryNames = new List<string>();
            List<string> Comments = new List<string>();
            List<ModifierListing> MListings = new List<ModifierListing>();
            ModifierXMLMatchingList = new List<string>();
            ModXMLList = new List<ModifierListing>();
            ModXMLIsOverrideList = new List<bool>();

            foreach (ModifierListing m in Modifiers)
            {
                    //check if any online companies use it if so then make one entry
                    foreach (CompanyListing cl in Companies)
                    {
                 
                        if (cl.AttachedModifiers.Contains(m) && cl.bOnline)
                        {

                                //online stock and uses this modifier
                                if (m.bShared)
                                {
                                    if (!MListings.Contains(m))//make sure its not been added already
                                    {
                                        //it is shared, make one entry
                                        ModXMLIsOverrideList.Add(m.bExtEnum);
                                        if (m.bExtEnum)
                                        {
                                            //uses external enum
                                            EntryNames.Add(m.SpStatsEnumOverride);
                                            ModifierXMLMatchingList.Add(m.SpStatsEnumOverride);
                                            ModXMLList.Add(m);

                                        }
                                        else
                                        {
                                            if (!withcomments)
                                            {
                                                EntryNames.Add(m.GeneratedEnum);
                                                ModifierXMLMatchingList.Add(m.GeneratedEnum);
                                                ModXMLList.Add(m);
                                            }
                                            else
                                            {
                                                EntryNames.Add(m.GeneratedEnum);
                                                Comments.Add(" // online mod " + m.Name + " (" + m.GeneratedEnum + ")");
                                                ModifierXMLMatchingList.Add(m.GeneratedEnum);
                                                ModXMLList.Add(m);
                                            }
                                        }
                                    
                                        //put it in the logged list so an entry won't be made again
                                        MListings.Add(m);
                                    }
                                }
                                else
                                {
                                    //its not shared, create an entry with this modifier for this company
                                    ModXMLIsOverrideList.Add(m.bExtEnum);
                                    if (!withcomments)
                                    {
                                        EntryNames.Add(m.GeneratedEnum + cl.EnumString);
                                        ModifierXMLMatchingList.Add(m.GeneratedEnum + "_FOR_" +  cl.EnumString);
                                        ModXMLList.Add(m);
                                    }else{
                                        EntryNames.Add(m.GeneratedEnum + cl.EnumString);
                                        Comments.Add( " // online mod for " + cl.Name + " : " + m.Name + " (" + m.GeneratedEnum + ")");
                                        ModifierXMLMatchingList.Add(m.GeneratedEnum + "_FOR_" + cl.EnumString);
                                        ModXMLList.Add(m);
                                    }

                                
                                }

                        
                            
                    }
                }


            }

            //parse EntryNames and make sure the strings are valid and unique and shorter than 16 chars
            
            //s1.CompareTo
            for (int i = 0; i < EntryNames.Count; ++i)
            {
                if(!ModXMLList[i].bExtEnum)
                {

                    EntryNames[i] = EntryNames[i].Replace("BSS_BSTR_", "");
                    EntryNames[i] = EntryNames[i].Replace("BSM_", "");
                    if (EntryNames[i].Length > 16)
                    {
                        EntryNames[i] = EntryNames[i].Replace("A", "");
                        EntryNames[i] = EntryNames[i].Replace("E", "");
                        EntryNames[i] = EntryNames[i].Replace("I", "");
                        EntryNames[i] = EntryNames[i].Replace("O", "");
                        EntryNames[i] = EntryNames[i].Replace("U", "");

                        if (EntryNames[i].Length > 16)
                        {
                            //still over 16 chars even after vowel removal
                            //remove multiples of the same letter in a row
                            char[] carray = EntryNames[i].ToCharArray();
                            for (int j = 1; j < EntryNames[i].Length; ++j)
                            {
                                if (EntryNames[i][j] == EntryNames[i][j - 1])
                                {
                                    carray[j-1] = ' ';
                                }
                            }
                            EntryNames[i] = (new String(carray)).ToString();
                            EntryNames[i] = EntryNames[i].Replace(" ", "");

                            if (EntryNames[i].Length > 16)
                            {
                                //still greater than 16, truncate
                                EntryNames[i] = EntryNames[i].Remove(16);
                            }


                        }
                    }
                }
            }

            int matchincrementer = 0;

            for (int i = 0; i < EntryNames.Count; ++i)
            {
                matchincrementer = 0;
                for (int j = 0; j < EntryNames.Count; ++j)
                {
                    if (i != j)
                    {
                        if (EntryNames[i].CompareTo(EntryNames[j]) == 0)
                        {
                            EntryNames[j] = EntryNames[j] + (++matchincrementer);
                        }
                    }
                }
            }
            /*
            if (withcomments)
            {
                for (int i = 0; i < EntryNames.Count; ++i)
                {
                    EntryNames[i] += Comments[i];
                }
            }
            */
            return EntryNames;
        }





        private string genCoModInfo(CompanyListing cl, ModifierListing ml,int modindex)
        {
            string s = "";

            s += "\tModifier name: " + ml.Name + "\n";
            if (ml.bShared)
            {
                s += "\t\tShared state: SHARED // it is not split for each company that uses it.\n";
            }
            else
            {
                s += "\t\tShared state: NOT SHARED // it is cloned between each company that uses it.\n";
            }
            if (ml.bShared)
            {
                s += "\t\tEnum: " + ml.GeneratedEnum + "\n";
            }
            else
            {
                s += "\t\tEnum: " + ml.GeneratedEnum + "_FOR_" + cl.EnumString + "\n";
            }

            switch (cl.AttachedModifierInterpretations[modindex])
            {
                case 0://
                    //use mod interp
                    if (ml.bInterpretation)
                    {
                        //positive
                        s += "\t\tInterpretation of this modifier by " + cl.Name +  " is POSITIVE, not overridden. \n";
                    }
                    else
                    {
                        //negative
                        s += "\t\tInterpretation of this modifier by " + cl.Name + " is NEGATIVE, not overridden.\n";
                    }
                    break;
                case 1: //positive override
                    s += "\t\tInterpretation of this modifier  is overridden to POSITIVE by " + cl.Name + "\n";

                    break;
                case 2://negative override
                    s += "\t\tInterpretation of this modifier is overridden to NEGATIVE by " + cl.Name + "\n";

                    break;
            }


            s += "\t\tScaling value : " + ml.fApplicationScale + ";\n ";


            s += "\n";

            return s;

        }



        private string genSystemDocument()
        {

            genOnlineXMLEntries();

            string s = "Company list showing which modifiers affect each company,\r\nand which competitors each company has.\r\n\r\n";

            //online companies
            foreach (CompanyListing cl in this.Companies)
            {
               // if (cl.bOnline)//for each company
                {
                   
                    if (cl.bOnline)
                    {
                        s += "Online (BAWSAQ) Company: " + cl.Name + "\t(" + cl.EnumString + ") :  \r\n";
                    }
                    else
                    {
                        s += "Offline (LCN) Company: " + cl.Name + "\t(" + cl.EnumString + ") :  \r\n";
                    }


                    s += "\tModifiers:\r\n";
                    for (int i = 0; i < cl.AttachedModifiers.Count; ++i)
                    {
                        ModifierListing ml = cl.AttachedModifiers[i];
                        //s += genCoModInfo(cl, ml, i);
                        s += "\t\t" ;
 

                        switch(cl.AttachedModifierInterpretations[i])
                        {
                            case 0://use default
                                if(ml.bInterpretation)
                                {
                                    //positive
                                    s += " (+)";// +  " (" + ml.fApplicationScale + ")";
                                }else{
                                    //negative
                                    s += " (-)";// + " (" + ml.fApplicationScale + ")";
                                }
                                break;
                            case 1://positive
                                s += " (+)";// + " (" + ml.fApplicationScale + ")";
                                break;
                            case 2: //negative
                                s += " (-)";// + " (" + ml.fApplicationScale + ")";
                                break;
                        }

                        if (ml.bShared)
                        {

                            s += ml.Name + " (" + ml.GeneratedEnum + ")  \r\n";
                        }
                        else
                        {
                            s += ml.Name + " (" + ml.GeneratedEnum + "_FOR_" + cl.EnumString + ")  \r\n";
                        }
                    }

                    s += "\tCompetitors:\r\n";

                    for (int i = 0; i < cl.Competitors.Count; ++i)
                    {
                        s += "\t\t" + cl.Competitors[i].Name + " \t\t(" + cl.Competitors[i].EnumString + ")  \r\n";
                    }

                    s += "\r\n";
                }
            }

            s += "\r\nThe following list shows the xml stat modifier names and how they relate to the system enums.\r\n\r\n";

            foreach (string str in MarketData.getGenerateXMLStatNames(true))
            {
                s += "" + str + "\r\n";
            }


            return s;
            /*
            //generate an overview 
            string s = "Online company list:\n";
            //online companies
            foreach (CompanyListing cl in this.Companies)
            {
                if (cl.bOnline)//for each company
                {
                    s += cl.Name + "\n";
                    //for each modifier owned
                    s += "\tStarting price: " + cl.fStartingPrice + "\n";
                    s += "\tStarting sensitivity: " + cl.fStartingSensitivity + "\n";
                    s += "\tShort tag: " + cl.ShortTag + "\n";
                    s += "\tEnum: " + cl.EnumString + "\n";
                   /// s += "Online enum: " + cl.ShortTag + "\n"; not generated yet
                    s += "\n\tDirect modifiers: " + cl.AttachedModifiers.Count + "\n\n";
                    
                    for(int i = 0;i < cl.AttachedModifiers.Count;++i)
                    {
                        ModifierListing ml = cl.AttachedModifiers[i];
                        s += genCoModInfo(cl, ml, i);
                    }
                    s += "\tCompetitor modifiers for  : '" + cl.Name + "' Total: " + cl.Competitors.Count + " \n\n ";
                    for (int i = 0; i < cl.Competitors.Count; ++i)
                    {
                        s += "\tModifiers from : '" + cl.Competitors[i].Name + "' \n";
                        //for each competitor modifier
                        for(int j = 0;j < cl.Competitors[i].AttachedModifiers.Count;++j)
                        {
                            s += genCoModInfo(cl.Competitors[i], cl.Competitors[i].AttachedModifiers[j], j);
                        }
                    }
                }
            }
            //offline companies
            return s;
             */
        }


        private string genModifierInfo()
        {
            string s = "\r\n";

            //for each modifier

            foreach (ModifierListing ml in this.Modifiers)
            {



                if (ml.bShared)
                {


                    int count = 0;
                    int online = 0;


                    foreach (CompanyListing cl in Companies)
                    {
            
                        if (cl.AttachedModifiers.Contains(ml))
                        {

                       
                            ++count;

                            if (cl.bOnline)
                            {
                                online++;
                            }
                        }
                    }
                    //no split, no company search
                    if (online > 0)
                    {
                        if (online == count)
                        {
                            s += ml.Name + " : SHARED stock, online only, " + count + " uses \r\n"; // (" + ml.GeneratedEnum + ") \r\n";
                        }
                        else
                        {
                            s += ml.Name + " : SHARED stock, both offline and online, " + count + " uses \r\n"; // (" + ml.GeneratedEnum + ") \r\n";
                        }
                    }
                    else
                    {
                        s += ml.Name + " : SHARED stock, offline only, " + count + " uses \r\n"; // (" + ml.GeneratedEnum + ") \r\n";
                    }
                    s += "\t\r\n";

                    
                }
                else
                {
                    //find each company that its in and display the enum
                    //s += ml.Name + "  \r\n";



                    //no split, no company search
                    


                    int count = 0;
                    int online = 0;


                    foreach (CompanyListing cl in Companies)
                    {
                       // for (int i = 0; i < cl.AttachedModifiers.Count; ++i)
                       // {
                            if (cl.AttachedModifiers.Contains(ml))
                            {

                                //s += "\t\r" + ml.Name + " : (" + ml.GeneratedEnum + "_FOR_" + cl.EnumString + ") \r\n";
                                ++count;

                                if (cl.bOnline)
                                {
                                    online++;
                                }
                            }
                       // }
                    }
                    s += ml.Name + " : SPLIT " + count + " times, " + online + " in online stocks\r\n";
                    s += "\t\r\n";

                }
            }

            return s;
        }



        private string genOnlineStatAssociations()
        {
            string s = "\nModifiers belonging to each price listing by listing\nInterpretation of each modifier is denoted in the brackets\n\n\n";
            //for every online company // list it's name price and volume tags
                //list its base attributes
                //list the stats it draws on


            foreach (CompanyListing c in Companies)
            {
                if (c.bOnline && c.AttachedModifiers.Count > 0)
                {
                    s += c.EnumString + "_PRICE\n";
                    

                    foreach(ModifierListing m in c.AttachedModifiers)
                    {
                        if(Modifiers.Contains(m))
                        {
                            s += "\t";


                            int i = c.AttachedModifiers.IndexOf(m);
                            bool binterp = false;
                            if (c.AttachedModifierInterpretations[i] > 0)
                            {
                                if (c.AttachedModifierInterpretations[i] == 1)
                                {
                                    binterp = true;
                                }
                            }
                            else
                            {
                                //use mod sign
                                if (m.bInterpretation)
                                {
                                    binterp = true;
                                }
                            }
                            if (binterp)
                            {
                                s += " (+) ";
                            }
                            else
                            {
                                s += " (-) ";
                            }


                            if (m.bExtEnum)
                            {
                                s += m.SpStatsEnumOverride;

                            }
                            else
                            {
                                if(m.bShared)
                                {
                                    s += m.GeneratedEnum;
                                }else{
                                    s += m.GeneratedEnum + c.ShortTag;
                                }
                            }





                            s += "\n";
                        }
                    }
                    

                    //s+="Company"
                    s += "\n";
                }


                



            }


            s += "\n\n\nThe following are the attributes for each of the price listings in csv form\n\n\n\n";

            s += "PriceListing,Bullishness,Bearishness,Ceiling,Floor,Stepcap,Noisiness\n";
            foreach (CompanyListing c in Companies)
            {
                if (c.bOnline)
                {
                    

/*
            //new attribute settings
            public float bullishness = 1.0f;
            public float bearishness = 1.0f;
            public float ceiling = 1000.0f;
            public float floor = 3.5f;
            public float stepcap = 3.5f;
            public float noisiness = 0.10f;
*/
                    s += c.ShortTag + "_PRICE, " + c.bullishness + ", " + c.bearishness + ", " + c.ceiling + ", " + c.floor + ", " + c.stepcap + ", " + c.noisiness + "\n";
                    

                }
            }

            s += "\r\n\r\n\r\n\r\n";
            foreach (CompanyListing c in Companies)
            {
                s += c.nameraw + " : " + c.ShortTag  + "\n";
            }


            return s;
        }

        private string generateCsvForAnalytics(bool? isOnline)
        {
            StringBuilder sb = new StringBuilder();
            var companies = Companies.Where(c => !isOnline.HasValue || c.bOnline == isOnline);
            sb.AppendFormat("Company	SM_ +	SM_ -	Misc +	Misc -	Bearishness	Bullishness	Ceiling	Floor	StepCap	Noisiness   IDV\tIPHV\tISAV\tIABS\tIFBB\tICBB\r\n");
            foreach (var c in companies)
            {
                sb.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\r\n", 
                                    c.EnumString, 
                                    generateModiferList(c, false, true), 
                                    generateModiferList(c, false, false),
                                    generateModiferList(c, true, true), 
                                    generateModiferList(c, true, false),
                                    c.bearishness, c.bullishness, c.ceiling, c.floor, c.stepcap, c.noisiness,
                                    c.IDV, c.IPHV, c.ISAV, c.IABS, c.IFBB, c.ICBB);
            }
            return sb.ToString();
        }

        private static string generateModiferList(CompanyListing company, bool isExisting, bool isPositive)
        {
            var modifiers = company.AttachedModifiers;
            if(modifiers.Any())
            {
                var filteredList = modifiers.Where(m => m.bExtEnum == isExisting && m.bInterpretation == isPositive);
                if(!filteredList.Any()) return "";
                var sbList = filteredList.Aggregate(new StringBuilder("\""),
                    (sb, nextModifier) => sb.AppendFormat("{0},", extractModifierStatName(nextModifier, company.ShortTag)));
                var strList = sbList.ToString().TrimEnd(',') + "\"";
                return strList;
            }
            return "";
        }

        private static string extractModifierStatName(ModifierListing modifier, string companyTag)
        {
            // Use the SpStatsEnumOverride if available otherwise use the GeneratedEnum with the SM_ prefix removed
            // Non-shared sm_ modifiers get the company name appended as they're company specific stats
            return string.IsNullOrWhiteSpace(modifier.SpStatsEnumOverride) 
                            ? modifier.bShared
                                ? modifier.GeneratedEnum.Substring("SM_".Length)
                                : modifier.GeneratedEnum.Substring("SM_".Length) + companyTag
                            : modifier.SpStatsEnumOverride;
        }

        private string genWarningInfo()
        {
            string s = "\r\n";

            s += "Companies with few modifiers (<3): \r\n";
            s += "\r\n";



            foreach (CompanyListing cl in Companies)
            {
                if (cl.AttachedModifiers.Count < 3)
                {
                    s += cl.Name + " : " + cl.AttachedModifiers.Count;
                    s += "\r\n";
                }

            }

            s += "\r\n";
            s += "\r\n";
            s += "\r\n";


            List<ModifierListing> offlineMods = new List<ModifierListing>();
            List<ModifierListing> onlineMods = new List<ModifierListing>();
            List<ModifierListing> spstatsMods = new List<ModifierListing>();

            foreach (CompanyListing cl in Companies)
            {
                foreach (ModifierListing m in cl.AttachedModifiers)
                {

                    if(m.bExtEnum)
                    {
                        //its based on an existing stat entry - these are all shared and doesn't matter if they're online or offline
                        if(!spstatsMods.Contains(m))
                        {
                            spstatsMods.Add(m);
                        }

                    }else{

                        if (m.bShared)
                        {
                            if (cl.bOnline)
                            {

                                if (!onlineMods.Contains(m))
                                {
                                    if (offlineMods.Contains(m))
                                    {
                                        offlineMods.Remove(m);
                                    }
                                    onlineMods.Add(m);
                                }
                            }
                            else
                            {
                                if (!onlineMods.Contains(m))
                                {
                                    if (!offlineMods.Contains(m))
                                    {
                                        offlineMods.Add(m);
                                    }
                                }
                            }
                        }
                        else //its split so easier to count
                        {
                            //the modifier is split so add anyway
                            if (cl.bOnline)
                            {
                                onlineMods.Add(m);
                            }
                            else
                            {
                                offlineMods.Add(m);
                            }
                        }
                    }
                }
            }



            s += "Modifier ratio, online vs offline vs existing stat binds (excluding unbound) : \r\n";
            s += "\r\n";

            s += onlineMods.Count + " vs " + offlineMods.Count + " vs " + spstatsMods.Count;
            s += "\r\n";
            s += "\r\n";
            s += "\r\n";


            Dictionary<ModifierListing,int> mostsplit = new Dictionary<ModifierListing,int>();


            foreach (ModifierListing m in Modifiers)
            {
                if (!m.bShared && !m.bExtEnum)
                {
                    int isplits = 0;
                    foreach (ModifierListing mc in onlineMods)
                    {
                        if (m == mc)
                        {
                            ++isplits;
                        }
                    }
                    foreach (ModifierListing mc in offlineMods)
                    {
                        if (m == mc)
                        {
                            ++isplits;
                        }
                    }
                    mostsplit.Add(m,isplits);
                }
            }





            List<ModifierListing> ordered = mostsplit.Keys.OrderByDescending(k => mostsplit[k]).ToList();
            s += "Modifiers by most split : \r\n";
            s += "\r\n";
            foreach (ModifierListing m in ordered)
            {
                s += m.Name + " : " + mostsplit[m] + " times ";

                if (onlineMods.Contains(m))
                {
                    s += "(Online)";
                }

                s += "\r\n";


                foreach (CompanyListing cl in Companies)
                {
                    if (cl.AttachedModifiers.Contains(m))
                    {
                        if (cl.bOnline)
                        {
                            s += "\t- In online listing : " + cl.Name +"\r\n";
                        }
                        else
                        {
                            s += "\t- In offline listing : " + cl.Name + "\r\n";
                        }
                    }
                }



            }
            s += "\r\n";
            s += "\r\n";
            s += "\r\n";


            s += "Unnused modifiers : \r\n";
            s += "\r\n";

            foreach (ModifierListing m in Modifiers)
            {
                if (!onlineMods.Contains(m) && !offlineMods.Contains(m) && !spstatsMods.Contains(m))
                {
                    s += m.Name + "\r\n";
                }
            }



            s += "Modifiers that must be script incremented : \r\n";
            s += "\r\n";


            foreach (ModifierListing m in Modifiers)
            {
                if (onlineMods.Contains(m) || offlineMods.Contains(m) && !spstatsMods.Contains(m))
                {

                    s += m.Name + " : a -> " + m.GeneratedEnum + " : ";
                    if (m.bShared)
                    {
                        s += "(SHARED)";
                    }


                    s += "\r\n";
                }
            }

            return s;
        }


        private string genOverviewCsv()
        {
            string s = "Modifier name,Enum Base,Used by companies,split,\r\n";//"\r\n";

            foreach (ModifierListing m in Modifiers)
            {
                s += m.Name + ",";

                s += m.GeneratedEnum + ",";
             

                int usedByCompanies = 0;

                foreach (CompanyListing cl in Companies)
                {
                    if (cl.AttachedModifiers.Contains(m))
                    {
                        ++usedByCompanies;
                    }
                }
                s += usedByCompanies + ",";

                if (m.bShared)
                {
                    s += "false,";//not split
                }
                else
                {
                    s += "true,";//split
                }



                s += "\r\n";


            }


            return s;
        }



    }
}
