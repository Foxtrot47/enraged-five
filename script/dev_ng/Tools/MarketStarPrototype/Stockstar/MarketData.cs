﻿using System;
using System.Collections.Generic;

namespace Stockstar
{
    public partial class MarketData
    {
        private MarketData()
        {
            Companies = new List<CompanyListing>();
            Modifiers = new List<ModifierListing>();
        }

        //datasets
        public class CompanyListing
        {
            public string nameraw;
            public String Name
            {
                get{
                    string cl = nameraw.Replace("|", ",");
                    return cl;
                }
                set
                {
                    nameraw = value.Replace(",", "|");
                }
            }

            public String ShortTag;
            public String EnumString;

            public bool bAutoGenEnum;
            public bool bOnline;

            public List<ModifierListing> AttachedModifiers;
            public List<int> AttachedModifierInterpretations;
            public List<CompanyListing> Competitors;
            public CompanyListing()
            {
                AttachedModifiers = new List<ModifierListing>();
                AttachedModifierInterpretations = new List<int>();
                Competitors = new List<CompanyListing>();
            }

            public override string ToString() 
            {
                return Name;
            }

            //default settings
            public float fStartingPrice = 1.0f;
            public float fStartingSensitivity = 0.02f;

            public string comment = null;

            internal void SetComment(string p)
            {
                string s = p.Replace(' ', '-');
                s = p.Replace(',', ' ');
                s = s.Replace(':', ' ');
                comment = s;
            }

            //new attribute settings
            public float bullishness = 1.0f;
            public float bearishness = 1.0f;
            public float ceiling = 1000.0f;
            public float floor = 3.5f;
            public float stepcap = 3.5f;
            public float noisiness = 0.10f;

            // (editable) constants
            public float IDV = InclinationConstants.IDV;
            public float IPHV = InclinationConstants.IPHV;
            public float ISAV = InclinationConstants.ISAV;
            public float IABS = InclinationConstants.IABS;
            public float IFBB = InclinationConstants.IFBB;
            public float ICBB = InclinationConstants.ICBB;
        }

        public static class InclinationConstants
        {
            public static float IDV = 0.995f;
            public static float IPHV = 0.66f;
            public static float ISAV = 0.33f;
            public static float IABS = 0.02f;
            public static float IFBB = 0.01f;
            public static float ICBB = 0.01f;
        }

        public class ModifierListing : IComparable
        {
            public String Name; // long enum name
            public String GeneratedEnum;
            public bool bAutoGenEnum;

            public bool bShared = true;//default to on

            public bool bInterpretation = true;

            public override string ToString()
            {
                return Name;
            }
               
            //default settings
            public float fApplicationScale = 1.0f;//application scaling value
            public bool bExtEnum = false;
            public string SpStatsEnumOverride;
            public bool bStatIsFloatProfType;


            int IComparable.CompareTo(object obj)
            {
                return this.ToString().CompareTo(obj.ToString());
            }
        }

        //local//datasets
        public List<CompanyListing> Companies = null;
        public List<ModifierListing> Modifiers = null;

        internal static int AddCompany()
        {
            return myInstance.mAddCompany();
        }

        private int mAddCompany()
        {
            if (Companies == null)
            {
                return -1;
            }
            CompanyListing cl = new CompanyListing();

            cl.Name = "Default";
            cl.ShortTag = "DEF";
            cl.EnumString = "BSS_DEF";
            cl.bOnline = false;
            cl.bAutoGenEnum = false;

            Companies.Add(cl);

            return Companies.IndexOf(cl);
        }

        internal static int AddModifier()
        {
            return myInstance.mAddModifier();
        }

        private int mAddModifier()
        {
            if (Modifiers == null)
            {
                return -1;
            }
            ModifierListing ml = new ModifierListing();

            ml.Name = "Default";
            ml.GeneratedEnum = "BSM_DEF";
            ml.bAutoGenEnum = false;

            Modifiers.Add(ml);
            return Modifiers.IndexOf(ml);
            
        }

        internal static void RemoveCompany(int p)
        {
            if (myInstance == null)
            {
                return;
            }

            myInstance.mRemoveCompany(p);
        }

        internal void mRemoveCompany(int p)
        {
            for (int i = 0; i < Companies.Count; ++i)
            {
                if (i != p)
                {
                    Companies[i].Competitors.Contains(Companies[p]);
                    Companies[i].Competitors.Remove(Companies[p]);
                }
            }

            if (Companies == null)
            {
                return;
            }

            if ((p >= Companies.Count) || (p < 0))
            {
                return;
            }

            Companies.RemoveAt(p);

        }

        internal static void RemoveModifier(ModifierListing p)
        {
            if (myInstance == null)
            {
                return;
            }

            myInstance.mRemoveModifier(myInstance.Modifiers.IndexOf(p));
        }


        internal void mRemoveModifier(int p)
        {
            foreach (CompanyListing cl in Companies)
            {
                for (int i = 0; i < cl.AttachedModifiers.Count; ++i)
                {
                    if (cl.AttachedModifiers[i] == Modifiers[p])
                    {
                        cl.AttachedModifiers.RemoveAt(i);

                        cl.AttachedModifierInterpretations.RemoveAt(i);

                        --i;
                    }
                }
            }

            if (Modifiers == null)
            {
                return;
            }

            if ((p >= Modifiers.Count) || (p < 0))
            {
                return;
            }

            Modifiers.RemoveAt(p);
        }

        internal static bool IsCompanyValid(CompanyListing companyListing)
        {
            if(myInstance == null)
            {
                return false;
            }
            return myInstance.mIsCompanyValid(companyListing);
        }

        private bool mIsCompanyValid(CompanyListing companyListing)
        {
            return Companies.Contains(companyListing);
        }

        internal static bool IsModifierValid(ModifierListing modifierListing)
        {
            if (myInstance == null)
            {
                return false;
            }
            return myInstance.mIsModifierValid(modifierListing);
        }

        private bool mIsModifierValid(ModifierListing modifierListing)
        {
            return Modifiers.Contains(modifierListing);
        }

        internal static void GenerateCompanyEnum(CompanyListing companyListing)
        {
            if (myInstance == null)
            {
                return;
            }

            myInstance.mGenerateCompanyEnum(companyListing);
        }

        private void mGenerateCompanyEnum(CompanyListing companyListing)
        {
            if(!Companies.Contains(companyListing))
            {
                return;
            }
            if (!companyListing.bAutoGenEnum)
            {
                return;
            }

            //generate enum from name

            string potentialname = companyListing.Name;

            potentialname = potentialname.ToUpper();

            //take out the vowels

            potentialname = potentialname.Replace("A", "");
            potentialname = potentialname.Replace("E", "");
            potentialname = potentialname.Replace("I", "");
            potentialname = potentialname.Replace("O", "");
            potentialname = potentialname.Replace("U", "");


            //add the prefix
            potentialname = "BSU_" + potentialname;



            // TODO check for matching ones
            // TODO check for length


            //finally set it
            companyListing.EnumString = potentialname;
            
        }

        internal static void GenerateModifierEnum(ModifierListing modifierListing)
        {
            if (myInstance == null)
            {
                return;
            }

            myInstance.mGenerateModifierEnum(modifierListing);

        }

        private void mGenerateModifierEnum(ModifierListing modifierListing)
        {
            if (!Modifiers.Contains(modifierListing))
            {
                return;
            }
            if (!modifierListing.bAutoGenEnum)
            {
                return;
            }

            string potentialname = modifierListing.Name;

            potentialname = potentialname.ToUpper();

            //take out the vowels

            potentialname = potentialname.Replace("A", "");
            potentialname = potentialname.Replace("E", "");
            potentialname = potentialname.Replace("I", "");
            potentialname = potentialname.Replace("O", "");
            potentialname = potentialname.Replace("U", "");


            //add the prefix
            potentialname = "BSM_" + potentialname;


            // TODO check for matching ones
            // TODO check for length

            //finally set it
            modifierListing.GeneratedEnum = potentialname;

        }


        internal static void OuputPriceVolumeAndOwnedStats(string p)
        {
            throw new NotImplementedException();
        }


    }
}
