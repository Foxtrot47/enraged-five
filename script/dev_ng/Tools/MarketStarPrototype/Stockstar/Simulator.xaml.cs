﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Stockstar
{
    /// <summary>
    /// Interaction logic for Simulator.xaml
    /// </summary>
    public partial class Simulator : Window
    {
        const int LogSteps = (24*7);

        private MainWindow parent;

        byte[] ColorData;
        WriteableBitmap wb;


        Int32Rect lRect;
        int pstride;
        int lstride;
        int alen;

        DispatcherTimer dtp;


        //simulation settings
        bool bPlayingSimulation = false;


        class PriceSnap
        {
            
            public float fPriceLastUpdate = 0.0f;
            public float fCurrentPrice = 100.0f;

            public int UpdatesPerformed = 0;
            public List<float> PriceLog;

            public List<ModifierSnap> BoundModifiers;
            public List<int> BoundModifiersInterps;

            public string Name;
            public string Enum;

            static Random r = null;

            //public float fStartingSensitivity; //removing this from the calculation


            /*
             * Problem: stocks do not have equal amounts of positive and negative mods
             * also even if they did the rates wouldn't nessesarily balance each other
             * this is where the stock personality state comes in
             * 
             * possible base states
             * stock has only pos or neg mods
             * 
             * stock has a mix of both
             * 
             * 
             * base stats - fixed for each company, can be changed to change behaviour
             * bullishness, inclination of the stat to rise, effects dimimishing returns on constant rises
             * bearishness, inclination of the stat to fall
             * floor price, a threshold below which rules apply to bring the stat back into range
             * ceiling price, a threshold above which rules apply to bring the stat back into range
             * noisyness, noise applied to the price after all calculations, used to make a stock more "spikey"
             * stepcap - an absolute limit on the amount a stock can change in one step
             * 
             * transitory stats - set by rules in the update process
             * 
             * Inclination - used to apply dimishing returns either way 
             * 
             * 
             * 
             * 
             */

            //new price listing personality settings modifiers

            //base stats
            public float Bullishness = 1.0f;//10% default
            public float Bearishness = 1.0f;//10% default
            public float FloorPrice = 3.5f;
            public float StartingPrice = 100.0f;
            public float CeilingPrice = 1000;
            public float Noisyness = 0.50f; // +/- x on each price
            public float StepCap = 3.5f;
           // public float MinChange = 0.0f;//if a change happens it has to be at least +/- this
            public float PullToTargetValue = 0.0f;
            public float PullToTargetStrength = 0.10f;

            //transitory stats
            float Inclination = 0.0f;//inclination should tend towards variance*(Bullishness - Bearishness) perhaps?






            public Color GraphColour;


            //pricegen data

            List<float> UPMs;


            public PriceSnap()
            {
                BoundModifiers = new List<ModifierSnap>();
                BoundModifiersInterps = new List<int>();
                PriceLog = new List<float>();
                UPMs = new List<float>();

                if (r == null)
                {
                    r = new Random();
                }

                GraphColour = new Color();
                byte [] bs = new byte[3];
                r.NextBytes(bs);
                GraphColour.R = bs[0];
                GraphColour.G = bs[1];
                GraphColour.B = bs[2];

                fCurrentPrice = StartingPrice;
                fPriceLastUpdate = StartingPrice;
            }

            public override string ToString()
            {
                return Name;
            }


            internal void UpdatePriceFromModifiers()
            {

                float upmpos = 0.0f;
                float upmneg = 0.0f;
                int posmods = 0;
                int negmods = 0;
                if (BoundModifiers.Count > 0)
                {
                    for (int i = 0; i < this.BoundModifiers.Count; ++i)
                    {
                        if (BoundModifiers[i].LastAvtDoc != 0.0f)
                        {
                            switch (BoundModifiersInterps[i])
                            {
                                case 1: //overriden to positive
                                    upmpos += BoundModifiers[i].LastAvtDoc;
                                    ++posmods;
                                    break;
                                case 2://overriden to negative
                                    upmneg -= BoundModifiers[i].LastAvtDoc;
                                    ++negmods;
                                    break;

                                default://use the modifier's base
                                    if (BoundModifiers[i].bInterpretation)
                                    {
                                        upmpos += BoundModifiers[i].LastAvtDoc;
                                        ++posmods;
                                    }
                                    else
                                    {
                                        upmneg -= BoundModifiers[i].LastAvtDoc;
                                        ++negmods;
                                    }

                                    break;
                            }
                        }
                    }

                    if (posmods > 0)
                    {
                        upmpos = upmpos / posmods;
                    }

                    if (negmods > 0)
                    {
                        upmneg = upmneg / negmods;
                    }

                }




                Inclination *= 0.995f;//inclination wears off over time, decay value

                if ((upmpos + upmneg) != 0.0f)
                {
                        float pendingPriceChange = 0.0f;


                        pendingPriceChange = ((upmpos * Bullishness) + (upmneg * Bearishness)) * fCurrentPrice;




                    
                        bool PPCisneg = false;
                        //flip the sign for this check if needed for reading simplicity
                        if (pendingPriceChange < 0.0f) {pendingPriceChange *= -1.0f; PPCisneg = true;}
                        if (pendingPriceChange > StepCap)
                        {
                            //for now hard lock
                            pendingPriceChange = StepCap;
                        }
                        if (PPCisneg) { pendingPriceChange *= -1.0f; }//flip back if needed


                    /*
                        if ((MinChange > pendingPriceChange) && (pendingPriceChange > -MinChange))
                        {
                            pendingPriceChange = MinChange;
                            if (PPCisneg)
                            {
                                pendingPriceChange *= -1;
                            }
                        }
                     */

                    //apply the pending price change
                        fPriceLastUpdate = fCurrentPrice;

                      

                        fCurrentPrice += pendingPriceChange + Inclination;

                        Inclination -= ((pendingPriceChange*0.66f) + (Inclination*0.33f)) * 0.02f;

                }

                //simple merge back toward floor/ceil with inclination reset 
                if (FloorPrice > fCurrentPrice)
                {
                    if (Inclination < 0.0f) { Inclination = 0.0f; }
                    Inclination += 0.01f*Bullishness;//how hard does it bounce back?
                    fCurrentPrice = (FloorPrice + fCurrentPrice)*0.5f ;
                }

                if (CeilingPrice < fCurrentPrice)
                {
                    if (Inclination > 0.0f) { Inclination = 0.0f; }
                    Inclination -= 0.01f*Bearishness;//how hard does it crash?
                    fCurrentPrice = (CeilingPrice + fCurrentPrice) * 0.5f;
                }

                //obfuscate the value if needed
                if (Noisyness != 0.0f)
                {
                    fCurrentPrice += ((((float)r.NextDouble()) * Noisyness) - (0.5f * Noisyness));
                }


                if (PullToTargetValue != 0.0f)
                {
                    //pull the price toward this value if this is set
                    float push = (fCurrentPrice - PullToTargetValue) * PullToTargetStrength;
                    fCurrentPrice -= push;

                }

                //PriceLog.Add(50.0f);

                PriceLog.Add(fCurrentPrice);
                while (PriceLog.Count > LogSteps)
                {
                    PriceLog.RemoveAt(0);
                }

            }
        }

        class ModifierSnap
        {
            public float ApplicationScale = 1.0f;

            public string Name;
            public string Enum;
            
            //Faked online data - mirrors steps in StockExample
            int minChange = 0;
            int maxChange = 400;

            int LastN = 10;//the length of tail that is sampled

            long runningTotal = 0;

            int runningThisTick = 0;

            List<int> Deltas;
            List<float> DoC;
            List<float> DoCAvt;

            public float LastAvtDoc = 0.0f;//the value used to make the ump in price


            static Random r = null;
            public bool bInterpretation;


            public ModifierSnap()
            {
                if (r == null)
                {
                    r = new Random();
                }


                Deltas = new List<int>();
                DoC = new List<float>();
                DoCAvt = new List<float>();

                //prime the pump with zeros
                for (int i = 0; i < LastN; ++i)
                {
                    DoC.Add(0.0f);
                    DoCAvt.Add(0.0f);
                }

            }


            public override string ToString()
            {
                return Name;
            }


            internal void GenerateNewValue(bool ranrange, int f, int t)
            {

                //new change value
                //int change = r.Next(maxChange - minChange) + minChange;
                //runningTotal += change;

                if (ranrange)
                {
                    runningThisTick += r.Next(t - f) + f;
                }


                Deltas.Add(runningThisTick);
                runningThisTick = 0;

                if(Deltas.Count < 2)
                {
                    return;
                }


                float currentdelta = Deltas[Deltas.Count-1];
                

                float degreeOfChange = 0.0f;

                if (currentdelta != 0.0f)
                {
                    /*float lastNoneZeroDelta = 0.0f;

                    
                    for (int i = Deltas.Count - 2; i > 0; --i)
                    {
                        if(Deltas[i] != 0.0f)
                        {
                            lastNoneZeroDelta = Deltas[i];
                            i = 0;
                        }
                    }


                    if (lastNoneZeroDelta != 0)
                    {
                        degreeOfChange = (currentdelta - lastNoneZeroDelta) / lastNoneZeroDelta;
                    }
                    */
                    float lastDelta = Deltas[Deltas.Count - 2];
                    if (lastDelta != 0.0f)
                    {
                        degreeOfChange = (currentdelta - lastDelta) / lastDelta;
                    }
                }



                 

                

                DoC.Add(degreeOfChange);

                float avtdoc = 0.0f;

                //sum

                for (int i = 1; i != LastN; ++i)
                {
                    avtdoc += DoC[DoC.Count - i];
                }
                if (avtdoc != 0.0f)
                {
                    avtdoc = avtdoc / LastN;
                }


                //done
                
                
                if (float.IsInfinity(avtdoc))
                {
                    avtdoc = 0.0f;//oh snap, should have bailed out before here
                }

                LastAvtDoc = avtdoc;

                DoCAvt.Add(avtdoc);


                //strip overflow from the end of the log
                while (Deltas.Count > LogSteps) { Deltas.RemoveAt(0); }
                while (DoC.Count > LogSteps) { DoC.RemoveAt(0); }
                while (DoCAvt.Count > LogSteps) { DoCAvt.RemoveAt(0); }

            }

            internal void FireEventsNow(int num)
            {
                runningThisTick += num;
            }
        }

        List<PriceSnap> Prices;
        List<ModifierSnap> Modifiers;

        public Simulator(MainWindow parentin)
        {
            this.parent = parentin;
            
            InitializeComponent();

            //Prep buffers
            System.Windows.Media.PixelFormat pf = System.Windows.Media.PixelFormats.Bgra32;
            int width = (int)simgraph.Width;
            int height = (int)simgraph.Height;
            int stride = (width * pf.BitsPerPixel)/8;

            byte[] pixels = new byte[height * stride];

            //init dumpbuffer
            lRect = new Int32Rect(0,0,width,height);
            ColorData = pixels;//new byte[height * stride];


            bool alternator = false;
            int c = 0;
            for (int i = 0; i < (height * width); ++i)
            {
                    c = i * (pf.BitsPerPixel/8);

                    if (alternator)
                    {
                        pixels[c] = 0xFF;
                        pixels[c + 1] = 0xFF;
                        pixels[c + 2] = 0xFF;

                        pixels[c + 3] = 0xFF;
                    }
                    else
                    {
                        pixels[c] = 0x00;
                        pixels[c + 1] = 0x00;
                        pixels[c + 2] = 0x00;

                        pixels[c + 3] = 0xFF;
                    }
 
                alternator = (!alternator);
            }

            //simgraph.Width
            //simgraph.Height
            wb = new WriteableBitmap(BitmapSource.Create(
                                                    width,
                                                    height,
                                                    96,
                                                    96,
                                                    pf,
                                                    BitmapPalettes.WebPalette,
                                                    pixels,
                                                    stride));


            pstride = (wb.Format.BitsPerPixel / 8);
            lstride = (wb.Format.BitsPerPixel * lRect.Width) / 8;
            alen = (lRect.Width * lRect.Height) * pstride;

            simgraph.Source = wb;

            Redraw();


            //prep graph redraw dispatcher
            dtp = new DispatcherTimer();

            dtp.Tick += new EventHandler(GraphRedrawTick);
            dtp.Interval = new TimeSpan(0,0,0,0,25);
            dtp.Start();



            Prices = new List<PriceSnap>();
            Modifiers = new List<ModifierSnap>();
            SnapShotBaseValues();
            RefreshSelectorList();

            this.Show();
            this.Focus();
                        
        }

        void SnapShotBaseValues()
        {
            Prices.Clear();
            Modifiers.Clear();

            foreach (MarketData.ModifierListing m in MarketData.getModifierList)
            {
                ModifierSnap ms = new ModifierSnap();

                ms.ApplicationScale = m.fApplicationScale;
                ms.Name = m.Name;

                ms.bInterpretation = m.bInterpretation;

                Modifiers.Add(ms);
            }

            foreach (MarketData.CompanyListing c in MarketData.getCompanyList)
            {
                PriceSnap ps = new PriceSnap();

                ps.StartingPrice = c.fStartingPrice;
                ps.fPriceLastUpdate = c.fStartingPrice;
                ps.fCurrentPrice = c.fStartingPrice;

                ps.Enum = (string)c.EnumString.Clone();

                ps.Name = (string)c.Name.Clone();

                //ps.fStartingSensitivity = c.fStartingSensitivity;

                foreach(MarketData.ModifierListing m in c.AttachedModifiers)
                {
                    ps.BoundModifiers.Add(Modifiers[MarketData.getModifierList.IndexOf(m)]);
                    
                }

                foreach (int i in c.AttachedModifierInterpretations)
                {
                    ps.BoundModifiersInterps.Add(i);
                }

                


                Prices.Add(ps);
            }

            RefreshSelectorList();
        }

        private void RefreshSelectorList()
        {

                //fill out plist
                plist.Items.Clear();

                foreach (PriceSnap p in Prices)
                {
                    plist.Items.Add(p);
                }


                //fill out mlist
                mlist.Items.Clear();

                foreach (ModifierSnap m in Modifiers)
                {
                    mlist.Items.Add(m);
                }
            
        }

        void GraphRedrawTick(object sender, EventArgs e)
        {
            /*
            for (int i = 0; i < tcnt; i += 1)
            {
                //for (int j = 0; j < tcnt; j += 2)
                //{
                    //DrawLine(i * 0.01f, j * 0.01f, (100 + (i * 3)) * 0.10f, (100 + (j * 3)) * 0.10f);
                DrawLine(i * 0.01f, 0.0f, i * 0.01f, 0.99f);
                //}
            }
            tcnt++;
             */


            //draw graph back

            //


            if (framePushesLeft > 0)
            {
                if (frameAmounts > 0)
                {
                    scroll.Text += "Events fired this tick : " + frameAmounts + "\n";
                    foreach (ModifierSnap m in mlist.SelectedItems)
                    {

                        m.FireEventsNow(frameAmounts);
                    }
                }

                --framePushesLeft;
            }
            else
            {
                if (frameAmounts > 0)
                {
                    scroll.Text += "Event firing on selected modifiers complete\n";
                }
                frameAmounts = 0;
            }

                

            UpdateGraph();

            //DrawLine(0, 185, 26, 169);

            //DrawLine(10, 10, 200, 200);

            Redraw();
        }

        void UpdateGraph()
        {
            if (plist.SelectedItems.Count < 1)
            {
                return;
            }
            //generate modifier values or read from data
            foreach (ModifierSnap m in Modifiers)
            {
                //do fake online style update
                    int f = 0;
                    int t = 5;
                ;

                if ((bool)ranrangecheck.IsChecked &&
                    int.TryParse(minranra.Text,out f)&&
                    int.TryParse(maxranra.Text, out t))
                {
                    if (t < f)
                    {
                        int a = f;
                        f = t;
                        t = a;
                    }

                    m.GenerateNewValue(true, f, t);
                }
                else
                {
                    m.GenerateNewValue(false, 0, 400);
                }

                    
            }




            //apply modifier data to prices
            foreach (PriceSnap ps in plist.SelectedItems)//Prices)
            {
                ps.UpdatePriceFromModifiers();
            }


            float max = float.MinValue;
            float min = float.MaxValue;
            float div = 1.0f / ((float) LogSteps);

            //find the max and min
            foreach (object o in plist.SelectedItems)
            {
                PriceSnap ps = (PriceSnap)o;

                for (int i = 0; i < ps.PriceLog.Count; ++i)
                {
                    if (ps.PriceLog[i] > max)
                    {
                        max = ps.PriceLog[i];
                    }
                    if (ps.PriceLog[i] < min)
                    {
                        min = ps.PriceLog[i];
                    }
                }
            }

            //set the max min texts

            maxl.Content = "Max:" + max;
            DrawLine(0.0f, 0.0f, 1.0f, 0.0f, Colors.Gray);
            minl.Content = "Min:" + min;
            DrawLine(0.0f, 1.0f, 1.0f, 1.0f, Colors.Gray);
            float range = max - min;

            //draw the scales
            float interval = 10.0f;
            
            //min/max scaled to interval
            float intevmin = min / interval;
            float intevmax = max / interval;

            int lowband = (int)Math.Ceiling(intevmin);
            int highband = (int)Math.Floor(intevmax);

            //for each int between lowband and highband that is within range after mult by interval
            float ra = 0.0f; 
            int dif = highband - lowband;

            if (dif < 50)//todo, move up order of magnitude
            {
                highband++;
                while (lowband < highband)
                {
                    ra = (lowband * interval);
                    if ((ra > min) && (ra < max))
                    {
                        //render line scaled to range
                        float ty = 1.0f - ((ra - min) / range);
                        DrawLine(0.0f, ty, 1.0f, ty, Colors.LightGray);
                    }
                    ++lowband;
                }
            }
            

            //draw the graphs
            int count = 0;
            
            foreach (object o in plist.SelectedItems)
            {
                PriceSnap ps = (PriceSnap)o;

                for (int i = 1; i < ps.PriceLog.Count; ++i)
                {
                    float fy = 1.0f-((ps.PriceLog[i - 1] - min) / range);
                    float ty = 1.0f - ((ps.PriceLog[i] - min) / range);
                    float fx = div*(i - 1);
                    float tx = div*i;


                     DrawLine(fx, fy, tx, ty, ps.GraphColour);
   
                    
                }

                count++;

            }
            
        }

        void DrawLine(float x0, float y0, float x1, float y1, Color c = new Color())
        {
            if (x0 > 1.0f)
            {
                x0 = 1.0f;
            }
            if (y0 > 1.0f)
            {
                y0 = 1.0f;
            }
            if (x1 > 1.0f)
            {
                x1 = 1.0f;
            }
            if (y1 > 1.0f)
            {
                y1 = 1.0f;
            }

            if (x0 < 0.0f)
            {
                x0 = 0.0f;
            }
            if (y0 < 0.0f)
            {
                y0 = 0.0f;
            }
            if (x1 < 0.0f)
            {
                x1 = 0.0f;
            }
            if (y1 < 0.0f)
            {
                y1 = 0.0f;
            }

            int xa = (int)(x0 * lRect.Width);
            int ya = (int)(y0 * lRect.Height);
            int xb = (int)(x1 * lRect.Width);
            int yb = (int)(y1 * lRect.Height);

            if (!(xa < lRect.Width))
            {
                xa = lRect.Width - 1;
            }

            if (!(ya < lRect.Height))
            {
                ya = lRect.Height - 1;
            }

            if (!(xb < lRect.Width))
            {
                xb = lRect.Width - 1;
            }

            if (!(yb < lRect.Height))
            {
                yb = lRect.Height - 1;
            }
            if (yb < 0)
            {
                yb = 0;
            }
            if (ya < 0)
            {
                ya = 0;
            }
            DrawLine(xa,
                     ya,
                     xb,
                     yb,
                     c);
        }
        
        void Redraw()
        {
            wb.WritePixels(lRect, ColorData, lstride, 0);
            simgraph.Source = wb;
            Array.Clear(ColorData, 0, ColorData.Length);
        }

        void DrawLine(int xin0, int yin0, int xin1, int yin1, Color c = new Color())
        {
            int x0 = xin0;
            int y0 = yin0;
            int x1 = xin1;
            int y1 = yin1;
            int dx = x1-x0;
            int dy = y1-y0;
            int sx = 1;
            int sy = 1;

            if (dx < 0)
            {
                dx *= -1;
            }
            if (dy < 0)
            {
                dy *= -1;
            } 

            int err = dx - dy;
            int e2;
                       


            if (!(x0 < x1))
            {
                sx = -1;
            }

            if (!(y0 < y1))
            {
                sy = -1;
            }


            //create the buffer
            //wb.Format.BitsPerPixel
           // int pstride = (wb.Format.BitsPerPixel / 8);
           // int lstride = (wb.Format.BitsPerPixel * lRect.Width) / 8;
           // int alen = (lRect.Width * lRect.Height) * pstride;
            //byte[] ColorData = new byte[alen];
            //Array.Clear(ColorData, 0, ColorData.Length);


            while (true)
            {
                //write pixel at x0,y0

                ColorData[(y0 * lstride) + (x0 * pstride)] = c.B;//c.A;//0x00;
                ColorData[((y0 * lstride) + (x0 * pstride)) + 1] = c.G;//c.G;//0xFF;
                ColorData[((y0 * lstride) + (x0 * pstride)) + 2] = c.R;//c.R; //0x00;
                ColorData[((y0 * lstride) + (x0 * pstride)) + 3] = 0xFF;//alpha always max for now


                if ((x0 == x1) && (y0 == y1))
                {
                    return;
                }
                e2 = 2 * err;

                if (e2 > -dy) 
                {
                    err = err - dy;
                    x0 = x0 + sx;
                }
                if (e2 < dx)
                {
                    err = err + dx;
                    y0 = y0 + sy;
                }

            }


        }

        private void plist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //for each selected price up to a limit set the colour to a new colour
            if (plist.SelectedItems.Count > 3)//TODO change the magic numbers here to the max registered graph colours
            {
                while (plist.SelectedItems.Count > 3)
                {
                    plist.SelectedItems.RemoveAt((plist.SelectedItems.Count - 1));
                }
            }

            


        }

        private void mlist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (mlist.SelectedItems.Count > 3)
            {
                while (mlist.SelectedItems.Count > 3)
                {
                    mlist.SelectedItems.RemoveAt((mlist.SelectedItems.Count - 1));
                }
            }
            


        }

        private void runbutton_Click(object sender, RoutedEventArgs e)
        {

        }



        int framePushesLeft = 0;
        int frameAmounts = 0;
       
        private void firevent_Click(object sender, RoutedEventArgs e)
        {
            
            int num = 0;
            int over = 0;
            int.TryParse(modventnum.Text, out num);

            int.TryParse(framessel.Text, out over);


            if (num > 0 && over > 0)
            {
                int per = num / over;
                framePushesLeft += over;
                frameAmounts += per;
            }
        }

        private void scroll_TextChanged(object sender, TextChangedEventArgs e)
        {
            //select end of text
            if (!scroll.IsFocused)
            {
                scroll.ScrollToEnd();
            }
        }



    }
}
