Company list showing which modifiers affect each company,
and which competitors each company has.

Online (BAWSAQ) Company: Ammunation	(AMU) :  
	Modifiers:
		 (+)KillingSprees (SM_KILLSPR)  
		 (+)BASE_SP_SPREE_PED_FRANK (SM_SP_SPREE_F)  
		 (+)BASE_SP_SPREE_PED_MIKE (SM_SP_SPREE_M)  
		 (+)BASE_SP_SPREE_PED_TREV (SM_SP_SPREE_T)  
	Competitors:

Online (BAWSAQ) Company: Badger	(BDG) :  
	Modifiers:
		 (+)TimesUsedCarApp (SM_CARAPP)  
		 (-)SPLIT_CALLS_CANCELED (SM_CALCAN_FOR_BDG)  
		 (+)SPLIT_PHONE_CALLS (SM_PHONCAL_FOR_BDG)  
		 (+)SPLIT_PHONE_TEXTS (SM_PHONTXT_FOR_BDG)  
		 (+)SPLIT_TICKS_ON_CHAR (SM_CHTICK_FOR_BDG)  
		 (+)BASE_SP_DEATH_MIKE (SM_SP_DEAD_M)  
	Competitors:
		Whiz 		(WIZ)  
		Tinkle 		(TNK)  

Offline (LCN) Company: BankOfLiberty	(BOL) :  
	Modifiers:
		 (+)MoneyGainedTotal (SM_MONUP_FOR_BOL)  
		 (-)AmountOfMoneySpent (SM_MONSP_FOR_BOL)  
	Competitors:
		MazeBank 		(MAZ)  
		Fleeca 		(FLC)  

Online (BAWSAQ) Company: BF	(BFA) :  
	Modifiers:
		 (+)VehiclesOfTypeBought (SM_VECBUY_FOR_BFA)  
		 (+)DistanceDrivenInType (SM_DISDRIV_FOR_BFA)  
		 (+)TaxisDestroyed (SM_TAXDEST)  
		 (-)BrandedVehiclesDestroyed (SM_BRVECDES_FOR_BFA)  
		 (-)VehicleOfTypeStolen (SM_VECSTOL_FOR_BFA)  
		 (-)DamageDoneToVehicleType (SM_VECDMG_FOR_BFA)  
		 (-)PeopleKilledWith (SM_KILW_FOR_BFA)  
		 (-)BASE_SP_MONEY_TAXI_FRANK (SM_MONTAX_F)  
		 (-)BASE_SP_MONEY_TAXI_MIKE (SM_MONTAX_M)  
		 (-)BASE_SP_MONEY_TAXI_TREV (SM_MONTAX_T)  
		 (-)BASE_SP_ROADDEATH_TREV (SM_ROADE_T)  
		 (-)BASE_SP_ROADDEATH_FRANK (SM_ROADE_F)  
		 (+)BASE_SP_ROADDEATH_MIKE (SM_ROADE_M)  
	Competitors:
		Hijak 		(HJK)  
		Ubermacht 		(UMA)  
		Vapid 		(VAP)  
		Brute 		(BRU)  
		Maibatsu 		(MAI)  
		Schyster 		(SHT)  
		HVYIndustries 		(HVY)  

Online (BAWSAQ) Company: Binco	(BIN) :  
	Modifiers:
		 (+)ClothesBoughtFrom (SM_CLOBOF_FOR_BIN)  
		 (-)BeingOnFireTicks (SM_TKFIRE)  
		 (-)PedsKilledByFire (SM_PEDFIREKILL)  
		 (+)BASE_SP_MONEY_CLOTHES_FRANK (SM_MONCLO_F)  
		 (+)BASE_SP_MONEY_CLOTHES_MIKE (SM_MONCLO_M)  
		 (+)BASE_SP_MONEY_CLOTHES_TREV (SM_MONCLO_T)  
	Competitors:
		Peckerwood 		(PKW)  
		Ponsonbys 		(PON)  

Online (BAWSAQ) Company: BitterSweet	(BTR) :  
	Modifiers:
		 (+)TimesUsedCarApp (SM_CARAPP)  
	Competitors:
		Fruit 		(FRT)  
		Facade 		(FAC)  

Online (BAWSAQ) Company: Bleeter	(BLE) :  
	Modifiers:
	Competitors:

Online (BAWSAQ) Company: Brute	(BRU) :  
	Modifiers:
		 (+)VehiclesOfTypeBought (SM_VECBUY_FOR_BRU)  
		 (+)DistanceDrivenInType (SM_DISDRIV_FOR_BRU)  
		 (+)TaxisDestroyed (SM_TAXDEST)  
		 (-)BrandedVehiclesDestroyed (SM_BRVECDES_FOR_BRU)  
		 (-)VehicleOfTypeStolen (SM_VECSTOL_FOR_BRU)  
		 (-)DamageDoneToVehicleType (SM_VECDMG_FOR_BRU)  
		 (-)PeopleKilledWith (SM_KILW_FOR_BRU)  
		 (-)BailsFromVehicleWhenOnFire (SM_FIBAI)  
		 (-)PilotSchoolUse (SM_PISCO)  
		 (-)BASE_SP_MONEY_TAXI_FRANK (SM_MONTAX_F)  
		 (-)BASE_SP_MONEY_TAXI_MIKE (SM_MONTAX_M)  
		 (-)BASE_SP_MONEY_TAXI_TREV (SM_MONTAX_T)  
	Competitors:
		Hijak 		(HJK)  
		Maibatsu 		(MAI)  
		Ubermacht 		(UMA)  
		Vapid 		(VAP)  
		HVYIndustries 		(HVY)  
		BF 		(BFA)  
		Schyster 		(SHT)  

Online (BAWSAQ) Company: CNT	(CNT) :  
	Modifiers:
		 (+)RadioStationsListenedTo (SM_RAD_FOR_CNT)  
		 (+)SongsIDedWithZIT (SM_ZITIT_FOR_CNT)  
		 (-)TimesChangedAwayFromRadiostation (SM_RADCHA_FOR_CNT)  
	Competitors:
		Weazel 		(WZL)  

Online (BAWSAQ) Company: Crevis	(CRE) :  
	Modifiers:
		 (+)ParaJumps (SM_PARA)  
		 (-)HealthyPeopleKilled (SM_HPKIL)  
		 (-)PubandClubvisits (SM_PUBCLUB)  
		 (-)TimesDrunk (SM_TDRNK)  
		 (-)FriendsTakenToPubsclubs (SM_FRNPUB)  
		 (-)TicksAtStripClub (SM_STRIP)  
		 (+)PedsOnFireTimes (SM_PEDFIRETICK)  
		 (+)BASE_SP_WINDSCREEN_FRANK (SM_WINSCR_F)  
		 (+)BASE_SP_WINDSCREEN_MIKE (SM_WINSCR_M)  
		 (+)BASE_SP_WINDSCREEN_TREV (SM_WINSCR_T)  
		 (+)TaxisDestroyed (SM_TAXDEST)  
		 (-)BASE_SP_MONEY_TAXI_FRANK (SM_MONTAX_F)  
		 (-)BASE_SP_MONEY_TAXI_MIKE (SM_MONTAX_M)  
		 (-)BASE_SP_MONEY_TAXI_TREV (SM_MONTAX_T)  
		 (+)BeingOnFireTicks (SM_TKFIRE)  
		 (+)BailsFromVehicleWhenOnFire (SM_FIBAI)  
		 (+)BASE_SP_CARBAILS_MIKE (SM_SP_CARBAILS_M)  
		 (+)BASE_SP_CARBAILS_TREV (SM_SP_CARBAILS_T)  
		 (+)BASE_SP_COPKILL_FRANK (SM_COPKIL_F)  
	Competitors:

Online (BAWSAQ) Company: DailyGlobe	(DGP) :  
	Modifiers:
		 (-)DamageToNewsagentStalls (SM_NEWDAM)  
		 (+)BASE_SP_LEGIT_KILLS_FRANK (SM_SP_LKILLS_F)  
		 (+)BASE_SP_LEGIT_KILLS_MIKE (SM_SP_LKILLS_M)  
		 (+)BASE_SP_LEGIT_KILLS_TREV (SM_SP_LKILLS_T)  
	Competitors:

Online (BAWSAQ) Company: Dept.WaterandPower	(WAP) :  
	Modifiers:
		 (+)TVTypeUsage (SM_TVTICK_FOR_WAP)  
	Competitors:

Online (BAWSAQ) Company: Facade	(FAC) :  
	Modifiers:
		 (+)TimesUsedCarApp (SM_CARAPP)  
	Competitors:
		Fruit 		(FRT)  
		BitterSweet 		(BTR)  

Offline (LCN) Company: Fleeca	(FLC) :  
	Modifiers:
		 (+)MoneyGainedTotal (SM_MONUP_FOR_FLC)  
	Competitors:
		BankOfLiberty 		(BOL)  
		MazeBank 		(MAZ)  

Offline (LCN) Company: Hijak	(HJK) :  
	Modifiers:
		 (+)VehiclesOfTypeBought (SM_VECBUY_FOR_HJK)  
		 (+)DistanceDrivenInType (SM_DISDRIV_FOR_HJK)  
		 (+)TaxisDestroyed (SM_TAXDEST)  
		 (-)BrandedVehiclesDestroyed (SM_BRVECDES_FOR_HJK)  
		 (-)VehicleOfTypeStolen (SM_VECSTOL_FOR_HJK)  
		 (-)DamageDoneToVehicleType (SM_VECDMG_FOR_HJK)  
		 (-)PeopleKilledWith (SM_KILW_FOR_HJK)  
		 (-)BailsFromVehicleWhenOnFire (SM_FIBAI)  
		 (-)PilotSchoolUse (SM_PISCO)  
		 (-)BASE_SP_MONEY_TAXI_FRANK (SM_MONTAX_F)  
		 (-)BASE_SP_MONEY_TAXI_MIKE (SM_MONTAX_M)  
		 (-)BASE_SP_MONEY_TAXI_TREV (SM_MONTAX_T)  
		 (+)BASE_SP_CARBAILS_FRANK (SM_SP_CARBAILS_F)  
		 (+)BASE_SP_CARBAILS_MIKE (SM_SP_CARBAILS_M)  
		 (+)BASE_SP_CARBAILS_TREV (SM_SP_CARBAILS_T)  
	Competitors:
		Maibatsu 		(MAI)  
		BF 		(BFA)  
		Schyster 		(SHT)  
		Ubermacht 		(UMA)  
		Vapid 		(VAP)  
		HVYIndustries 		(HVY)  
		Brute 		(BRU)  

Online (BAWSAQ) Company: Fruit	(FRT) :  
	Modifiers:
		 (+)TimesUsedCarApp (SM_CARAPP)  
	Competitors:
		BitterSweet 		(BTR)  
		Facade 		(FAC)  

Offline (LCN) Company: AuguryInsurance	(AUG) :  
	Modifiers:
		 (+)VehicleModification (SM_VECMOD_FOR_AUG)  
		 (+)TicksWithNoWantedRating (SM_TKNWA)  
		 (+)BailsFromVehicleWhenOnFire (SM_FIBAI)  
		 (+)BeingOnFireTicks (SM_TKFIRE)  
		 (+)CrimesCommited (SM_CRIMCOM)  
		 (+)TroubleCauseAtStripClub (SM_STRTRO)  
		 (+)ParaJumps (SM_PARA)  
		 (+)PilotSchoolUse (SM_PISCO)  
		 (-)BASE_SP_FALLDEATH_FRANK (SM_FALDE_F)  
		 (-)BASE_SP_FALLDEATH_MIKE (SM_FALDE_M)  
		 (-)BASE_SP_FALLDEATH_TREV (SM_FALDE_T)  
		 (-)BASE_SP_LARGE_CRASH_FRANK (SM_SP_LCRASH_F)  
		 (-)BASE_SP_LARGE_CRASH_MIKE (SM_SP_LCRASH_M)  
		 (-)BASE_SP_LARGE_CRASH_TREV (SM_SP_LCRASH_T)  
	Competitors:

Offline (LCN) Company: BULLHEAD	(BUL) :  
	Modifiers:
		 (+)DrunkenCrimes (SM_DRNKCRM)  
		 (+)CrimesCommited (SM_CRIMCOM)  
		 (+)TroubleCauseAtStripClub (SM_STRTRO)  
		 (+)KillingSprees (SM_KILLSPR)  
		 (-)TicksWithNoWantedRating (SM_TKNWA)  
		 (-)PeopleInjured (SM_TOTINJ)  
		 (+)BASE_SP_MONEY_BRIBES_MIKE (SM_MONBRI_M)  
		 (+)BASE_SP_STARS_ATTAINED_MIKE (SM_STATAI_M)  
		 (+)BASE_SP_STARS_LOST_MIKE (SM_STALOS_M)  
	Competitors:
		Hammerstein&Faust 		(HAF)  
		Slaughter,Slaughter&Slaughter 		(SSS)  

Offline (LCN) Company: Hammerstein&Faust	(HAF) :  
	Modifiers:
		 (+)DrunkenCrimes (SM_DRNKCRM)  
		 (+)CrimesCommited (SM_CRIMCOM)  
		 (+)TroubleCauseAtStripClub (SM_STRTRO)  
		 (+)KillingSprees (SM_KILLSPR)  
		 (-)TicksWithNoWantedRating (SM_TKNWA)  
		 (-)PeopleInjured (SM_TOTINJ)  
		 (+)BASE_SP_MONEY_BRIBES_FRANK (SM_MONBRI_F)  
		 (+)BASE_SP_STARS_ATTAINED_FRANK (SM_STATAI_F)  
		 (+)BASE_SP_STARS_LOST_FRANK (SM_STALOS_F)  
	Competitors:
		Slaughter,Slaughter&Slaughter 		(SSS)  
		BULLHEAD 		(BUL)  

Offline (LCN) Company: Slaughter,Slaughter&Slaughter	(SSS) :  
	Modifiers:
		 (+)DrunkenCrimes (SM_DRNKCRM)  
		 (+)CrimesCommited (SM_CRIMCOM)  
		 (+)TroubleCauseAtStripClub (SM_STRTRO)  
		 (+)KillingSprees (SM_KILLSPR)  
		 (-)TicksWithNoWantedRating (SM_TKNWA)  
		 (-)PeopleInjured (SM_TOTINJ)  
		 (+)BASE_SP_MONEY_BRIBES_TREV (SM_MONBRI_T)  
		 (+)BASE_SP_STARS_ATTAINED_TREV (SM_STATAI_T)  
		 (+)BASE_SP_STARS_LOST_TREV (SM_STALOS_T)  
	Competitors:
		BULLHEAD 		(BUL)  
		Hammerstein&Faust 		(HAF)  

Online (BAWSAQ) Company: LosSantosCustoms	(LSC) :  
	Modifiers:
		 (+)VehicleModification (SM_VECMOD_FOR_LSC)  
		 (+)GarageRepairsMade (SM_GAREP)  
		 (+)MoneySpentAtGarage (SM_GAMONSP)  
		 (+)TimesUsedCarApp (SM_CARAPP)  
		 (-)BrandedVehiclesDestroyed (SM_BRVECDES_FOR_LSC)  
		 (-)TaxisDestroyed (SM_TAXDEST)  
		 (-)CarsModifiedThenNotBought (SM_MONB)  
		 (+)BASE_SP_MONEY_TAXI_FRANK (SM_MONTAX_F)  
		 (+)BASE_SP_MONEY_TAXI_MIKE (SM_MONTAX_M)  
		 (+)BASE_SP_MONEY_TAXI_TREV (SM_MONTAX_T)  
	Competitors:

Online (BAWSAQ) Company: LSTransport	(LST) :  
	Modifiers:
		 (+)VehicleOfTypeStolen (SM_VECSTOL_FOR_LST)  
		 (+)PeopleKilledWithVehicle (SM_VECPEDKIL)  
		 (+)TaxisDestroyed (SM_TAXDEST)  
		 (-)DistanceDrivenInType (SM_DISDRIV_FOR_LST)  
		 (-)PilotSchoolUse (SM_PISCO)  
		 (-)BrandedVehiclesDestroyed (SM_BRVECDES_FOR_LST)  
		 (+)BASE_SP_MONEY_TAXI_TREV (SM_MONTAX_T)  
		 (+)BASE_SP_MONEY_TAXI_MIKE (SM_MONTAX_M)  
		 (+)BASE_SP_MONEY_TAXI_FRANK (SM_MONTAX_F)  
	Competitors:

Online (BAWSAQ) Company: LTDOil	(LTD) :  
	Modifiers:
		 (+)BrandedVehiclesDestroyed (SM_BRVECDES_FOR_LTD)  
		 (+)TaxisDestroyed (SM_TAXDEST)  
		 (+)PilotSchoolUse (SM_PISCO)  
		 (-)TankersDestroyed (SM_TANDES)  
		 (-)PedsKilledByFire (SM_PEDFIREKILL)  
		 (-)StoreRobberiesProfits (SM_STOROB)  
		 (+)BASE_SP_MONEY_TAXI_FRANK (SM_MONTAX_F)  
		 (+)BASE_SP_MONEY_TAXI_MIKE (SM_MONTAX_M)  
		 (+)BASE_SP_MONEY_TAXI_TREV (SM_MONTAX_T)  
	Competitors:
		RonOil 		(RON)  

Online (BAWSAQ) Company: Maibatsu	(MAI) :  
	Modifiers:
		 (+)VehiclesOfTypeBought (SM_VECBUY_FOR_MAI)  
		 (+)DistanceDrivenInType (SM_DISDRIV_FOR_MAI)  
		 (+)TaxisDestroyed (SM_TAXDEST)  
		 (-)BrandedVehiclesDestroyed (SM_BRVECDES_FOR_MAI)  
		 (-)VehicleOfTypeStolen (SM_VECSTOL_FOR_MAI)  
		 (-)DamageDoneToVehicleType (SM_VECDMG_FOR_MAI)  
		 (-)PeopleKilledWith (SM_KILW_FOR_MAI)  
		 (-)CiviliansKilledWithWeaponType (SM_KILCIV_FOR_MAI)  
		 (-)BailsFromVehicleWhenOnFire (SM_FIBAI)  
		 (-)PilotSchoolUse (SM_PISCO)  
		 (-)BASE_SP_WINDSCREEN_FRANK (SM_WINSCR_F)  
		 (-)BASE_SP_WINDSCREEN_MIKE (SM_WINSCR_M)  
		 (-)BASE_SP_WINDSCREEN_TREV (SM_WINSCR_T)  
		 (-)BASE_SP_ROADDEATH_FRANK (SM_ROADE_F)  
		 (-)BASE_SP_ROADDEATH_MIKE (SM_ROADE_M)  
		 (-)BASE_SP_ROADDEATH_TREV (SM_ROADE_T)  
		 (-)BASE_SP_MONEY_TAXI_TREV (SM_MONTAX_T)  
		 (-)BASE_SP_MONEY_TAXI_MIKE (SM_MONTAX_M)  
		 (-)BASE_SP_MONEY_TAXI_FRANK (SM_MONTAX_F)  
	Competitors:
		Schyster 		(SHT)  
		Ubermacht 		(UMA)  
		Vapid 		(VAP)  
		HVYIndustries 		(HVY)  
		BF 		(BFA)  
		Brute 		(BRU)  
		Hijak 		(HJK)  

Offline (LCN) Company: MazeBank	(MAZ) :  
	Modifiers:
		 (+)MoneyGainedTotal (SM_MONUP_FOR_MAZ)  
	Competitors:
		Fleeca 		(FLC)  
		BankOfLiberty 		(BOL)  

Online (BAWSAQ) Company: Peckerwood	(PKW) :  
	Modifiers:
		 (+)ClothesBoughtFrom (SM_CLOBOF_FOR_PKW)  
		 (-)BeingOnFireTicks (SM_TKFIRE)  
		 (-)PedsKilledByFire (SM_PEDFIREKILL)  
	Competitors:
		Binco 		(BIN)  
		Ponsonbys 		(PON)  

Offline (LCN) Company: AnimalArt	(ARK) :  
	Modifiers:
		 (+)HappyDogTime (SM_HDOG)  
		 (-)UnhappyDogTime (SM_UHDOG)  
	Competitors:

Online (BAWSAQ) Company: Pisswasser	(PIS) :  
	Modifiers:
		 (+)PubandClubvisits (SM_PUBCLUB)  
		 (+)TimesDrunk (SM_TDRNK)  
		 (+)FriendsTakenToPubsclubs (SM_FRNPUB)  
		 (+)TicksAtStripClub (SM_STRIP)  
		 (-)DrunkenCrimes (SM_DRNKCRM)  
		 (-)TroubleCauseAtStripClub (SM_STRTRO)  
		 (-)NumberOfHangOvers (SM_HANGOVR)  
	Competitors:
		Logger 		(LOG)  

Online (BAWSAQ) Company: Ponsonbys	(PON) :  
	Modifiers:
		 (+)ClothesBoughtFrom (SM_CLOBOF_FOR_PON)  
		 (-)PedsKilledByFire (SM_PEDFIREKILL)  
		 (-)BeingOnFireTicks (SM_TKFIRE)  
	Competitors:
		Binco 		(BIN)  
		Peckerwood 		(PKW)  

Offline (LCN) Company: Raine	(RAI) :  
	Modifiers:
		 (+)VendingMachineOftypeUsed (SM_VENUSE_FOR_RAI)  
		 (+)TimesCompletedTriathlon (SM_TRI)  
		 (+)TimesCompletedYoga (SM_YOGA)  
		 (+)TimeSpentAtGym (SM_GYM)  
		 (+)NumberOfHangOvers (SM_HANGOVR)  
		 (-)HealthyPeopleKilled (SM_HPKIL)  
	Competitors:
		Sprunk 		(SPU)  

Online (BAWSAQ) Company: RonOil	(RON) :  
	Modifiers:
		 (+)BrandedVehiclesDestroyed (SM_BRVECDES_FOR_RON)  
		 (+)TaxisDestroyed (SM_TAXDEST)  
		 (+)PilotSchoolUse (SM_PISCO)  
		 (-)TankersDestroyed (SM_TANDES)  
		 (-)PedsKilledByFire (SM_PEDFIREKILL)  
		 (-)StoreRobberiesProfits (SM_STOROB)  
		 (+)BASE_SP_MONEY_TAXI_FRANK (SM_MONTAX_F)  
		 (+)BASE_SP_MONEY_TAXI_MIKE (SM_MONTAX_M)  
		 (+)BASE_SP_MONEY_TAXI_TREV (SM_MONTAX_T)  
	Competitors:
		LTDOil 		(LTD)  

Online (BAWSAQ) Company: Schyster	(SHT) :  
	Modifiers:
		 (+)VehiclesOfTypeBought (SM_VECBUY_FOR_SHT)  
		 (+)DistanceDrivenInType (SM_DISDRIV_FOR_SHT)  
		 (+)TaxisDestroyed (SM_TAXDEST)  
		 (-)BrandedVehiclesDestroyed (SM_BRVECDES_FOR_SHT)  
		 (-)VehicleOfTypeStolen (SM_VECSTOL_FOR_SHT)  
		 (-)DamageDoneToVehicleType (SM_VECDMG_FOR_SHT)  
		 (-)PeopleKilledWith (SM_KILW_FOR_SHT)  
		 (-)CiviliansKilledWithWeaponType (SM_KILCIV_FOR_SHT)  
		 (-)BailsFromVehicleWhenOnFire (SM_FIBAI)  
		 (-)PilotSchoolUse (SM_PISCO)  
		 (-)BASE_SP_MONEY_TAXI_TREV (SM_MONTAX_T)  
		 (-)BASE_SP_MONEY_TAXI_MIKE (SM_MONTAX_M)  
		 (-)BASE_SP_MONEY_TAXI_FRANK (SM_MONTAX_F)  
	Competitors:
		BF 		(BFA)  
		Brute 		(BRU)  
		Hijak 		(HJK)  
		Maibatsu 		(MAI)  
		Ubermacht 		(UMA)  
		Vapid 		(VAP)  
		HVYIndustries 		(HVY)  

Online (BAWSAQ) Company: Sprunk	(SPU) :  
	Modifiers:
		 (+)VendingMachineOftypeUsed (SM_VENUSE_FOR_SPU)  
		 (+)HealthyPeopleKilled (SM_HPKIL)  
		 (+)NumberOfHangOvers (SM_HANGOVR)  
		 (-)TimesCompletedTriathlon (SM_TRI)  
		 (-)TimesCompletedYoga (SM_YOGA)  
		 (-)TimeSpentAtGym (SM_GYM)  
	Competitors:
		Raine 		(RAI)  

Online (BAWSAQ) Company: Tinkle	(TNK) :  
	Modifiers:
		 (+)TimesUsedCarApp (SM_CARAPP)  
		 (-)SPLIT_CALLS_CANCELED (SM_CALCAN_FOR_TNK)  
		 (+)SPLIT_PHONE_CALLS (SM_PHONCAL_FOR_TNK)  
		 (+)SPLIT_PHONE_TEXTS (SM_PHONTXT_FOR_TNK)  
		 (+)SPLIT_TICKS_ON_CHAR (SM_CHTICK_FOR_TNK)  
		 (+)BASE_SP_DEATH_TREV (SM_SP_DEAD_T)  
	Competitors:
		Badger 		(BDG)  
		Whiz 		(WIZ)  

Online (BAWSAQ) Company: WIWANG	(WIW) :  
	Modifiers:
		 (+)TVTypeUsage (SM_TVTICK_FOR_WIW)  
	Competitors:

Online (BAWSAQ) Company: Ubermacht	(UMA) :  
	Modifiers:
		 (+)VehiclesOfTypeBought (SM_VECBUY_FOR_UMA)  
		 (+)DistanceDrivenInType (SM_DISDRIV_FOR_UMA)  
		 (+)TaxisDestroyed (SM_TAXDEST)  
		 (-)BrandedVehiclesDestroyed (SM_BRVECDES_FOR_UMA)  
		 (-)VehicleOfTypeStolen (SM_VECSTOL_FOR_UMA)  
		 (-)DamageDoneToVehicleType (SM_VECDMG_FOR_UMA)  
		 (-)PeopleKilledWith (SM_KILW_FOR_UMA)  
		 (-)CiviliansKilledWithWeaponType (SM_KILCIV_FOR_UMA)  
		 (-)BailsFromVehicleWhenOnFire (SM_FIBAI)  
		 (-)PilotSchoolUse (SM_PISCO)  
		 (-)BASE_SP_MONEY_TAXI_FRANK (SM_MONTAX_F)  
		 (-)BASE_SP_MONEY_TAXI_MIKE (SM_MONTAX_M)  
		 (-)BASE_SP_MONEY_TAXI_TREV (SM_MONTAX_T)  
	Competitors:
		BF 		(BFA)  
		Brute 		(BRU)  
		Hijak 		(HJK)  
		Maibatsu 		(MAI)  
		Schyster 		(SHT)  
		Vapid 		(VAP)  
		HVYIndustries 		(HVY)  

Online (BAWSAQ) Company: Vapid	(VAP) :  
	Modifiers:
		 (+)VehiclesOfTypeBought (SM_VECBUY_FOR_VAP)  
		 (+)DistanceDrivenInType (SM_DISDRIV_FOR_VAP)  
		 (+)TaxisDestroyed (SM_TAXDEST)  
		 (-)BrandedVehiclesDestroyed (SM_BRVECDES_FOR_VAP)  
		 (-)VehicleOfTypeStolen (SM_VECSTOL_FOR_VAP)  
		 (-)DamageDoneToVehicleType (SM_VECDMG_FOR_VAP)  
		 (-)PeopleKilledWith (SM_KILW_FOR_VAP)  
		 (-)BailsFromVehicleWhenOnFire (SM_FIBAI)  
		 (-)PilotSchoolUse (SM_PISCO)  
		 (-)BASE_SP_MONEY_TAXI_MIKE (SM_MONTAX_M)  
		 (-)BASE_SP_MONEY_TAXI_TREV (SM_MONTAX_T)  
		 (-)BASE_SP_MONEY_TAXI_FRANK (SM_MONTAX_F)  
	Competitors:
		HVYIndustries 		(HVY)  
		Ubermacht 		(UMA)  
		BF 		(BFA)  
		Brute 		(BRU)  
		Maibatsu 		(MAI)  
		Schyster 		(SHT)  
		Hijak 		(HJK)  

Online (BAWSAQ) Company: VomFeuer	(VOM) :  
	Modifiers:
		 (+)RampagesComplete (SM_RAMCOM)  
		 (+)CiviliansKilledWithWeaponType (SM_KILCIV_FOR_VOM)  
		 (+)PeopleKilledWith (SM_KILW_FOR_VOM)  
		 (-)WeaponTypeTaken (SM_WEPTAKE_FOR_VOM)  
		 (-)CopsKilledWithWeaponType (SM_KILCOP_FOR_VOM)  
		 (+)CriminalsKilledWithWeaponType (SM_KILCRIM_FOR_VOM)  
	Competitors:

Online (BAWSAQ) Company: Weazel	(WZL) :  
	Modifiers:
		 (+)RadioStationsListenedTo (SM_RAD_FOR_WZL)  
		 (+)SongsIDedWithZIT (SM_ZITIT_FOR_WZL)  
		 (-)TimesChangedAwayFromRadiostation (SM_RADCHA_FOR_WZL)  
	Competitors:
		CNT 		(CNT)  

Online (BAWSAQ) Company: Whiz	(WIZ) :  
	Modifiers:
		 (+)TimesUsedCarApp (SM_CARAPP)  
		 (+)SPLIT_TICKS_ON_CHAR (SM_CHTICK_FOR_WIZ)  
		 (+)SPLIT_PHONE_TEXTS (SM_PHONTXT_FOR_WIZ)  
		 (+)SPLIT_PHONE_CALLS (SM_PHONCAL_FOR_WIZ)  
		 (-)SPLIT_CALLS_CANCELED (SM_CALCAN_FOR_WIZ)  
		 (-)BASE_SP_DEATH_FRANK (SM_SP_DEAD_F)  
	Competitors:
		Badger 		(BDG)  
		Tinkle 		(TNK)  

Offline (LCN) Company: Debonair	(DEB) :  
	Modifiers:
		 (+)TimesSmoked (BSM_SMOKED_FOR_DEB)  
	Competitors:

Online (BAWSAQ) Company: Zit	(ZIT) :  
	Modifiers:
		 (+)RadioStationsListenedTo (SM_RAD_FOR_ZIT)  
		 (+)SongsIDedWithZIT (SM_ZITIT_FOR_ZIT)  
		 (+)ZitPopularitySongGroup (SM_ZITPOP_FOR_ZIT)  
	Competitors:

Online (BAWSAQ) Company: Shark	(SHK) :  
	Modifiers:
		 (-)MoneyGainedTotal (SM_MONUP_FOR_SHK)  
		 (+)BASE_SP_MONEY_HEALTHCARE_FRANK (SM_HECAR_F)  
		 (+)BASE_SP_MONEY_HEALTHCARE_MIKE (SM_HECAR_M)  
		 (+)BASE_SP_MONEY_HEALTHCARE_TREV (SM_HECAR_T)  
	Competitors:

Online (BAWSAQ) Company: BettaPharmaceuticals	(MOL) :  
	Modifiers:
		 (+)TimesDrunk (SM_TDRNK)  
		 (+)UglyHookersKilled (SM_UGHOK)  
		 (+)DrugDealersKilled (SM_DRUGKIL)  
	Competitors:

Online (BAWSAQ) Company: PumpnRun	(PMP) :  
	Modifiers:
		 (+)TimeSpentAtGym (SM_GYM)  
		 (+)PeopleInjured (SM_TOTINJ)  
		 (-)FriendsTakenToPubsclubs (SM_FRNPUB)  
	Competitors:

Online (BAWSAQ) Company: GrainOfTruth	(GOT) :  
	Modifiers:
		 (-)BASE_SP_MONEY_HEALTHCARE_FRANK (SM_HECAR_F)  
		 (-)BASE_SP_MONEY_HEALTHCARE_MIKE (SM_HECAR_M)  
		 (-)BASE_SP_MONEY_HEALTHCARE_TREV (SM_HECAR_T)  
		 (+)BASE_SP_RUNDIST_FRANK (SM_SP_RUNDIST_F)  
		 (+)BASE_SP_RUNDIST_MIKE (SM_SP_RUNDIST_M)  
		 (+)BASE_SP_RUNDIST_TREV (SM_SP_RUNDIST_T)  
		 (+)BASE_SP_BIKED_DIST_FRANK (SM_SP_BIKEDIST_F)  
		 (+)BASE_SP_BIKED_DIST_MIKE (SM_SP_BIKEDIST_M)  
		 (+)BASE_SP_BIKED_DIST_TREV (SM_SP_BIKEDIST_T)  
	Competitors:

Online (BAWSAQ) Company: Eyefind	(EYE) :  
	Modifiers:
		 (+)TimesUsedCarApp (SM_CARAPP)  
	Competitors:

Offline (LCN) Company: AirEmu	(EMU) :  
	Modifiers:
		 (+)PilotSchoolUse (SM_PISCO)  
	Competitors:
		FlyUS 		(FUS)  

Offline (LCN) Company: BeanMachine	(BEN) :  
	Modifiers:
		 (+)HealthyPeopleKilled (SM_HPKIL)  
		 (+)BASE_SP_MONEY_TAXI_MIKE (SM_MONTAX_M)  
	Competitors:
		BurgerShot 		(BGR)  
		CluckinBell 		(CLK)  
		CoolBeans 		(BAN)  
		eCola 		(ECL)  
		TacoBomb 		(TBO)  
		UpandAtom 		(UPA)  

Offline (LCN) Company: GoldCoast	(GOL) :  
	Modifiers:
		 (+)BASE_SP_SAVED_FRANK (SM_SAVE_F)  
		 (+)BASE_SP_SAVED_MIKE (SM_SAVE_M)  
		 (-)BASE_SP_SAVED_TREV (SM_SAVE_T)  
	Competitors:

Offline (LCN) Company: BobMulet	(BOM) :  
	Modifiers:
		 (+)TimesHairCut (SM_HAIRCUTS)  
		 (-)BeingOnFireTicks (SM_TKFIRE)  
	Competitors:

Offline (LCN) Company: BurgerShot	(BGR) :  
	Modifiers:
		 (+)HealthyPeopleKilled (SM_HPKIL)  
		 (-)BASE_SP_RUNDIST_FRANK (SM_SP_RUNDIST_F)  
	Competitors:
		BeanMachine 		(BEN)  
		CluckinBell 		(CLK)  
		CoolBeans 		(BAN)  
		eCola 		(ECL)  
		TacoBomb 		(TBO)  
		UpandAtom 		(UPA)  

Offline (LCN) Company: CluckinBell	(CLK) :  
	Modifiers:
		 (+)HealthyPeopleKilled (SM_HPKIL)  
		 (-)BASE_SP_RUNDIST_TREV (SM_SP_RUNDIST_T)  
	Competitors:
		UpandAtom 		(UPA)  
		TacoBomb 		(TBO)  
		eCola 		(ECL)  
		CoolBeans 		(BAN)  
		BurgerShot 		(BGR)  
		BeanMachine 		(BEN)  

Offline (LCN) Company: CoolBeans	(BAN) :  
	Modifiers:
		 (+)HealthyPeopleKilled (SM_HPKIL)  
		 (-)BASE_SP_RUNDIST_MIKE (SM_SP_RUNDIST_M)  
	Competitors:
		BeanMachine 		(BEN)  
		BurgerShot 		(BGR)  
		CluckinBell 		(CLK)  
		eCola 		(ECL)  
		TacoBomb 		(TBO)  
		UpandAtom 		(UPA)  

Offline (LCN) Company: DollarPills	(DOP) :  
	Modifiers:
		 (+)NumberOfHangOvers (SM_HANGOVR)  
		 (+)DrugDealersKilled (SM_DRUGKIL)  
		 (+)BASE_SP_MONEY_HEALTHCARE_FRANK (SM_HECAR_F)  
		 (+)BASE_SP_MONEY_HEALTHCARE_MIKE (SM_HECAR_M)  
		 (+)BASE_SP_MONEY_HEALTHCARE_TREV (SM_HECAR_T)  
	Competitors:

Offline (LCN) Company: eCola	(ECL) :  
	Modifiers:
		 (+)HealthyPeopleKilled (SM_HPKIL)  
	Competitors:
		UpandAtom 		(UPA)  
		TacoBomb 		(TBO)  
		CoolBeans 		(BAN)  
		BeanMachine 		(BEN)  
		BurgerShot 		(BGR)  
		CluckinBell 		(CLK)  

Offline (LCN) Company: FlyUS	(FUS) :  
	Modifiers:
		 (+)PilotSchoolUse (SM_PISCO)  
	Competitors:
		AirEmu 		(EMU)  

Offline (LCN) Company: GastroBand	(GAS) :  
	Modifiers:
		 (+)PeopleInjured (SM_TOTINJ)  
		 (-)TimeSpentAtGym (SM_GYM)  
		 (+)BASE_SP_BIKED_DIST_FRANK (SM_SP_BIKEDIST_F)  
		 (+)BASE_SP_RUNDIST_FRANK (SM_SP_RUNDIST_F)  
		 (+)BASE_SP_BIKED_DIST_MIKE (SM_SP_BIKEDIST_M)  
		 (+)BASE_SP_RUNDIST_MIKE (SM_SP_RUNDIST_M)  
		 (+)BASE_SP_BIKED_DIST_TREV (SM_SP_BIKEDIST_T)  
		 (+)BASE_SP_RUNDIST_TREV (SM_SP_RUNDIST_T)  
		 (+)BASE_SP_MONEY_HEALTHCARE_FRANK (SM_HECAR_F)  
		 (+)BASE_SP_MONEY_HEALTHCARE_MIKE (SM_HECAR_M)  
		 (+)BASE_SP_MONEY_HEALTHCARE_TREV (SM_HECAR_T)  
	Competitors:

Offline (LCN) Company: GoPostal	(GOP) :  
	Modifiers:
		 (-)MailBoxesDestroyed (SM_MAILDEST)  
		 (+)RampagesComplete (SM_RAMCOM)  
		 (+)BASE_SP_FIRESTART_FRANK (SM_SP_FIRESTART_F)  
		 (+)BASE_SP_FIRESTART_MIKE (SM_SP_FIRESTART_M)  
		 (+)BASE_SP_FIRESTART_TREV (SM_SP_FIRESTART_T)  
	Competitors:

Offline (LCN) Company: GruppeSechs	(GRU) :  
	Modifiers:
		 (+)BASE_SP_SWATKILL_FRANK (SM_SWAKIL_F)  
		 (+)BASE_SP_SWATKILL_MIKE (SM_SWAKIL_M)  
		 (+)BASE_SP_SWATKILL_TREV (SM_SWAKIL_T)  
		 (+)BASE_SP_COPKILL_FRANK (SM_COPKIL_F)  
		 (+)BASE_SP_COPKILL_MIKE (SM_COPKIL_M)  
		 (+)BASE_SP_COPKILL_TREV (SM_COPKIL_T)  
		 (-)TicksWithNoWantedRating (SM_TKNWA)  
		 (-)BASE_SP_BIKED_DIST_FRANK (SM_SP_BIKEDIST_F)  
		 (-)BASE_SP_BIKED_DIST_MIKE (SM_SP_BIKEDIST_M)  
		 (-)BASE_SP_BIKED_DIST_TREV (SM_SP_BIKEDIST_T)  
		 (-)BASE_SP_BUSTED_FRANK (SM_BUST_F)  
		 (-)BASE_SP_BUSTED_MIKE (SM_BUST_M)  
		 (-)BASE_SP_BUSTED_TREV (SM_BUST_T)  
	Competitors:

Offline (LCN) Company: Krapea	(KRP) :  
	Modifiers:
		 (+)BASE_SP_COVERTIME_FRANK (SP_COTI_F)  
		 (+)BASE_SP_COVERTIME_MIKE (SP_COTI_M)  
		 (+)BASE_SP_COVERTIME_TREV (SP_COTI_T)  
		 (-)FurnitureDestroyed (SM_FURDEST)  
	Competitors:

Offline (LCN) Company: LifeInvader	(LFI) :  
	Modifiers:
	Competitors:

Offline (LCN) Company: MaxRenda	(MAX) :  
	Modifiers:
		 (+)PeopleInjured (SM_TOTINJ)  
		 (+)BASE_SP_MONEY_HEALTHCARE_FRANK (SM_HECAR_F)  
		 (+)BASE_SP_MONEY_HEALTHCARE_MIKE (SM_HECAR_M)  
		 (+)BASE_SP_MONEY_HEALTHCARE_TREV (SM_HECAR_T)  
	Competitors:

Offline (LCN) Company: PostOP	(POP) :  
	Modifiers:
		 (-)MailBoxesDestroyed (SM_MAILDEST)  
	Competitors:
		GoPostal 		(GOP)  

Offline (LCN) Company: ProLaps	(PRO) :  
	Modifiers:
		 (+)ParaJumps (SM_PARA)  
		 (+)TimesCompletedTriathlon (SM_TRI)  
		 (-)HealthyPeopleKilled (SM_HPKIL)  
		 (-)PubandClubvisits (SM_PUBCLUB)  
		 (-)TicksAtStripClub (SM_STRIP)  
		 (-)TimesDrunk (SM_TDRNK)  
		 (-)FriendsTakenToPubsclubs (SM_FRNPUB)  
		 (+)BASE_SP_BIKED_DIST_FRANK (SM_SP_BIKEDIST_F)  
		 (+)BASE_SP_RUNDIST_FRANK (SM_SP_RUNDIST_F)  
		 (+)BASE_SP_BIKED_DIST_MIKE (SM_SP_BIKEDIST_M)  
		 (+)BASE_SP_RUNDIST_MIKE (SM_SP_RUNDIST_M)  
		 (+)BASE_SP_RUNDIST_TREV (SM_SP_RUNDIST_T)  
		 (+)BASE_SP_BIKED_DIST_TREV (SM_SP_BIKEDIST_T)  
	Competitors:

Offline (LCN) Company: Redwood	(RWC) :  
	Modifiers:
		 (+)TimesSmoked (BSM_SMOKED_FOR_RWC)  
	Competitors:

Offline (LCN) Company: RichardsMajestic	(RIM) :  
	Modifiers:
		 (+)TVTypeUsage (SM_TVTICK_FOR_RIM)  
	Competitors:

Offline (LCN) Company: TacoBomb	(TBO) :  
	Modifiers:
		 (+)HealthyPeopleKilled (SM_HPKIL)  
	Competitors:
		BeanMachine 		(BEN)  
		BurgerShot 		(BGR)  
		CluckinBell 		(CLK)  
		CoolBeans 		(BAN)  
		eCola 		(ECL)  
		UpandAtom 		(UPA)  

Offline (LCN) Company: UpandAtom	(UPA) :  
	Modifiers:
		 (+)HealthyPeopleKilled (SM_HPKIL)  
	Competitors:
		BeanMachine 		(BEN)  
		BurgerShot 		(BGR)  
		CluckinBell 		(CLK)  
		CoolBeans 		(BAN)  
		eCola 		(ECL)  
		TacoBomb 		(TBO)  

Offline (LCN) Company: Vangelico	(VAG) :  
	Modifiers:
	Competitors:

Offline (LCN) Company: VanillaUnicorn	(UNI) :  
	Modifiers:
		 (+)TicksAtStripClub (SM_STRIP)  
		 (-)TroubleCauseAtStripClub (SM_STRTRO)  
	Competitors:

Online (BAWSAQ) Company: HVYIndustries	(HVY) :  
	Modifiers:
		 (+)VehiclesOfTypeBought (SM_VECBUY_FOR_HVY)  
		 (+)DistanceDrivenInType (SM_DISDRIV_FOR_HVY)  
		 (+)TaxisDestroyed (SM_TAXDEST)  
		 (-)BrandedVehiclesDestroyed (SM_BRVECDES_FOR_HVY)  
		 (-)VehicleOfTypeStolen (SM_VECSTOL_FOR_HVY)  
		 (-)DamageDoneToVehicleType (SM_VECDMG_FOR_HVY)  
		 (-)PeopleKilledWith (SM_KILW_FOR_HVY)  
		 (-)BailsFromVehicleWhenOnFire (SM_FIBAI)  
		 (-)PilotSchoolUse (SM_PISCO)  
		 (+)BASE_SP_MONEY_TAXI_FRANK (SM_MONTAX_F)  
		 (+)BASE_SP_MONEY_TAXI_MIKE (SM_MONTAX_M)  
		 (+)BASE_SP_MONEY_TAXI_TREV (SM_MONTAX_T)  
	Competitors:

Offline (LCN) Company: Logger	(LOG) :  
	Modifiers:
		 (+)PubandClubvisits (SM_PUBCLUB)  
		 (+)TimesDrunk (SM_TDRNK)  
		 (+)FriendsTakenToPubsclubs (SM_FRNPUB)  
		 (+)TicksAtStripClub (SM_STRIP)  
		 (-)DrunkenCrimes (SM_DRNKCRM)  
		 (-)TroubleCauseAtStripClub (SM_STRTRO)  
		 (-)NumberOfHangOvers (SM_HANGOVR)  
	Competitors:
		Pisswasser 		(PIS)  

Offline (LCN) Company: Merryweather	(MER) :  
	Modifiers:
		 (+)CrimesCommited (SM_CRIMCOM)  
		 (+)DrunkenCrimes (SM_DRNKCRM)  
		 (+)KillingSprees (SM_KILLSPR)  
		 (+)PeopleInjured (SM_TOTINJ)  
		 (+)PeopleKilledWith (SM_KILW_FOR_MER)  
		 (+)PeopleKilledWithVehicle (SM_VECPEDKIL)  
		 (+)RampagesComplete (SM_RAMCOM)  
		 (+)StoreRobberiesProfits (SM_STOROB)  
		 (+)BASE_SP_LEGIT_KILLS_TREV (SM_SP_LKILLS_T)  
		 (+)BASE_SP_LEGIT_KILLS_MIKE (SM_SP_LKILLS_M)  
		 (+)BASE_SP_LEGIT_KILLS_FRANK (SM_SP_LKILLS_F)  
	Competitors:

Offline (LCN) Company: WorldwideFM	(RA1) :  
	Modifiers:
		 (+)RadioStationsListenedTo (SM_RAD_FOR_RA1)  
		 (-)TimesChangedAwayFromRadiostation (SM_RADCHA_FOR_RA1)  
	Competitors:

Offline (LCN) Company: RadioLosSantos	(RA2) :  
	Modifiers:
		 (+)RadioStationsListenedTo (SM_RAD_FOR_RA2)  
		 (-)TimesChangedAwayFromRadiostation (SM_RADCHA_FOR_RA2)  
	Competitors:

Online (BAWSAQ) Company: Shewsbury	(SHR) :  
	Modifiers:
		 (+)WeaponTypeBought (SM_WEPBUY_FOR_SHR)  
		 (+)RampagesComplete (SM_RAMCOM)  
		 (-)CiviliansKilledWithWeaponType (SM_KILCIV_FOR_SHR)  
		 (-)CopsKilledWithWeaponType (SM_KILCOP_FOR_SHR)  
		 (-)WeaponTypeTaken (SM_WEPTAKE_FOR_SHR)  
		 (+)CriminalsKilledWithWeaponType (SM_KILCRIM_FOR_SHR)  
	Competitors:
		VomFeuer 		(VOM)  
		HawkAndLittle 		(HAL)  

Online (BAWSAQ) Company: HawkAndLittle	(HAL) :  
	Modifiers:
		 (+)WeaponTypeBought (SM_WEPBUY_FOR_HAL)  
		 (+)RampagesComplete (SM_RAMCOM)  
		 (-)WeaponTypeTaken (SM_WEPTAKE_FOR_HAL)  
		 (-)CiviliansKilledWithWeaponType (SM_KILCIV_FOR_HAL)  
		 (-)CopsKilledWithWeaponType (SM_KILCOP_FOR_HAL)  
		 (+)CriminalsKilledWithWeaponType (SM_KILCRIM_FOR_HAL)  
	Competitors:
		VomFeuer 		(VOM)  
		Shewsbury 		(SHR)  

Offline (LCN) Company: MorsMutualInsurance	(MOR) :  
	Modifiers:
		 (+)TimeSpentAtGym (SM_GYM)  
		 (-)HealthyPeopleKilled (SM_HPKIL)  
		 (+)ParaJumps (SM_PARA)  
		 (+)BASE_SP_MONEY_HEALTHCARE_FRANK (SM_HECAR_F)  
		 (+)BASE_SP_MONEY_HEALTHCARE_MIKE (SM_HECAR_M)  
		 (+)BASE_SP_MONEY_HEALTHCARE_TREV (SM_HECAR_T)  
		 (+)BASE_SP_FALLDEATH_FRANK (SM_FALDE_F)  
		 (+)BASE_SP_FALLDEATH_MIKE (SM_FALDE_M)  
		 (+)BASE_SP_FIRESTART_FRANK (SM_SP_FIRESTART_F)  
	Competitors:

Offline (LCN) Company: Bilkinton	(BSS_PRI) :  
	Modifiers:
	Competitors:


The following list shows the xml stat modifier names and how they relate to the system enums.

SM_BRVECDESBFA
SM_BRVECDESBRU
SM_BRVECDESLSC
SM_BRVECDESLST
SM_BRVECDESLTD
SM_BRVECDESMAI
SM_BRVECDESRON
SM_BRVECDESSHT
SM_BRVECDESUMA
SM_BRVECDESVAP
SM_BRVECDESHVY
SM_VECBUYBFA
SM_VECBUYBRU
SM_VECBUYMAI
SM_VECBUYSHT
SM_VECBUYUMA
SM_VECBUYVAP
SM_VECBUYHVY
SM_DISDRIVBFA
SM_DISDRIVBRU
SM_DISDRIVLST
SM_DISDRIVMAI
SM_DISDRIVSHT
SM_DISDRIVUMA
SM_DISDRIVVAP
SM_DISDRIVHVY
SM_VECMODLSC
SM_VECSTOLBFA
SM_VECSTOLBRU
SM_VECSTOLLST
SM_VECSTOLMAI
SM_VECSTOLSHT
SM_VECSTOLUMA
SM_VECSTOLVAP
SM_VECSTOLHVY
SM_VECDMGBFA
SM_VECDMGBRU
SM_VECDMGMAI
SM_VECDMGSHT
SM_VECDMGUMA
SM_VECDMGVAP
SM_VECDMGHVY
SM_VECPEDKIL
SM_WEPBUYSHR
SM_WEPBUYHAL
SM_WEPTAKEVOM
SM_WEPTAKESHR
SM_WEPTAKEHAL
SM_KILCOPVOM
SM_KILCOPSHR
SM_KILCOPHAL
SM_KILCRIMVOM
SM_KILCRIMSHR
SM_KILCRIMHAL
SM_KILCIVMAI
SM_KILCIVSHT
SM_KILCIVUMA
SM_KILCIVVOM
SM_KILCIVSHR
SM_KILCIVHAL
SM_VENUSESPU
SM_NEWDAM
SM_HPKIL
SM_PUBCLUB
SM_TDRNK
SM_FRNPUB
SM_DRNKCRM
SM_RAMCOM
SM_RADCNT
SM_RADWZL
SM_RADZIT
SM_ZITITCNT
SM_ZITITWZL
SM_ZITITZIT
SM_RADCHACNT
SM_RADCHAWZL
SM_PARA
SM_TKFIRE
SM_FIBAI
SM_TANDES
SM_GAREP
SM_GAMONSP
SM_MONB
SM_MONUPSHK
SM_TAXDEST
SM_KILWBFA
SM_KILWBRU
SM_KILWMAI
SM_KILWSHT
SM_KILWUMA
SM_KILWVAP
SM_KILWVOM
SM_KILWHVY
SM_CLOBOFBIN
SM_CLOBOFPKW
SM_CLOBOFPON
SM_YOGA
SM_TRI
SM_GYM
SM_STRIP
SM_UGHOK
SM_STRTRO
SM_PISCO
SM_TOTINJ
SM_DRUGKIL
SM_HANGOVR
SM_KILLSPR
SM_PEDFIREKILL
SM_PEDFIRETICK
SM_TVTICKWAP
SM_TVTICKWIW
SM_ZITPOPZIT
SM_CARAPP
SM_STOROB
SP0_DEATHS
SP1_DEATHS
SP2_DEATHS
SP0_DIST_WALKING
SP1_DIST_WALKING
SP2_DIST_WALKING
SP0_DIST_DRIVING_BICYCLE
SP1_DIST_DRIVING_BICYCLE
SP2_DIST_DRIVING_BICYCLE
SP0_BAILED_FROM_VEHICLE
SP2_BAILED_FROM_VEHICLE
SP0_PEDS_KILLS_ON_SPREE
SP1_PEDS_KILLS_ON_SPREE
SP2_PEDS_KILLS_ON_SPREE
SP0_TOTAL_LEGITIMATE_KILLS
SP1_TOTAL_LEGITIMATE_KILLS
SP2_TOTAL_LEGITIMATE_KILLS
SP0_MONEY_SPENT_IN_CLOTHES
SP1_MONEY_SPENT_IN_CLOTHES
SP2_MONEY_SPENT_IN_CLOTHES
SP0_MONEY_SPENT_ON_TAXIS
SP1_MONEY_SPENT_ON_TAXIS
SP2_MONEY_SPENT_ON_TAXIS
SP0_MONEY_SPENT_ON_HEALTHCARE
SP1_MONEY_SPENT_ON_HEALTHCARE
SP2_MONEY_SPENT_ON_HEALTHCARE
SP0_THROWNTHROUGH_WINDSCREEN
SP1_THROWNTHROUGH_WINDSCREEN
SP2_THROWNTHROUGH_WINDSCREEN
SM_PHONCALBDG
SM_PHONCALTNK
SM_PHONCALWIZ
SM_PHONTXTBDG
SM_PHONTXTTNK
SM_PHONTXTWIZ
SM_CHTICKBDG
SM_CHTICKTNK
SM_CHTICKWIZ
SM_CALCANBDG
SM_CALCANTNK
SM_CALCANWIZ
SP0_DIED_IN_ROAD
SP1_DIED_IN_ROAD
SP2_DIED_IN_ROAD
SP1_KILLS_COP
