from trac.core import *
from trac.wiki.macros import WikiMacroBase
from StringIO import StringIO

""" added """
from trac.wiki import wiki_to_html
import os
import urllib

"""__all__ = ['sanScriptMacro']"""


"""
{{{
Macro for adding drop down sanScript commands to the wiki
Simon Lashley 23/04/2009
Example usage, # used as seperator
[[sanScript(FUNC BOOL#IS_PED_DEAD#PED_INDEX ped#This command will return TRUE is the ped passed is dead)]]
}}}
"""
   
class sanScriptMacro(WikiMacroBase):

    def render_macro(self, formatter, name, args):

        buf = StringIO()

        if args:
            args = args.split('#')

            if len(args) == 1:
                if args[0] == 'JAVACODE':
                    buf.write('<script type=\'text/javascript\'>function showHideMenu(id){ var domObj = document.getElementById(id); if ( domObj.style.display == \'none\' ) { domObj.style.display =\'inline\'; } else { domObj.style.display = \'none\'; } } </script>')
                else:
                    buf.write("sanScript has wrong number of arguments, should have 4.")

            elif len(args) == 4:
                """
                 buf.write('<span style="font-family: arial, monospace;"><span onClick="javascript: showHideMenu(\''+ args[1]+'\');" style="color:#0000C0;font-size:14pt;">'+args[1]+'</span><br/><span id="'+args[1]+'" style="display:none;font-size:12pt;"><span style="color:#0000C0">'+args[0]+'</span> <span style="color:#C00000">'+args[1]+'</span><span style="color:#000000">(</span><span style="color:#009090">'+args[2]+'</span><span style="color:#000000">)</span><br/><span style="color:#00A000; font-style:italic;">'+args[3]+'</span></span></span>')
                """
                buf.write('<span style="font-family: arial, monospace;"><span onClick="javascript: showHideMenu(\''+ args[1]+'\');" style="color:#0000C0;font-size:12pt;">'+args[1]+'</span><br/><span id="'+args[1]+'" style="display:none;font-size:10pt;"><span style="color:#00A000; font-style:italic;">'+args[3]+'</span><br/><span style="color:#0000C0">'+args[0]+'</span><span style="color:#C00000">'+args[1]+'</span><span style="color:#000000">(</span><span style="color:#009090">'+args[2]+'</span><span style="color:#000000">)</span></span></span>')



            else:
                 buf.write("sanScript has wrong number of arguments, should have 4.")
        else:
            buf.write("sanScript has not been passed any arguments")

        return buf.getvalue()
