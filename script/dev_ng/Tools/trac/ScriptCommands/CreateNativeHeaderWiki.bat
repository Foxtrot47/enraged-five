
@echo off
CALL setenv.bat

SET INPUT_XML=%RS_SCRIPTBRANCH%\Tools\trac\ScriptCommands\ScriptHeadersFilesForTrac.xml
SET OUTPUT_FILENAME=%RS_SCRIPTBRANCH%\Tools\trac\ScriptCommands\NativeHeaderTracFormat.txt

x:
PUSHD %RS_TOOLSROOT%\script\util\script
ruby script_parse_sch_to_python_and_xml.rb --input=%INPUT_XML% --output=%OUTPUT_FILENAME% --native --python
POPD

pause