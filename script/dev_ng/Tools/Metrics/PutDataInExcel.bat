@echo off

echo -----------------------------------------------------------------------------
echo 				Copy Data To Excel
echo -----------------------------------------------------------------------------
echo Launched at %TIME%

echo Grabbing all metrics data...


echo Importing all metrics data into excel doc...
CALL X:\gta5\script\dev\Tools\Metrics\bin\CopyMetricDataToExcel X:\gta5\script\dev\Tools\Metrics\bin\script_metric_data.log


echo successfull copied data to excel

echo finished
pause

REM print error if something cocked up
IF %ERRORLEVEL% EQU 1 (
	echo .
	echo WARNING: Something cocked up.
	pause
)