@echo off

echo -----------------------------------------------------------------------------
echo 				METRICS GRABBER
echo -----------------------------------------------------------------------------
echo Launched at %TIME%

echo Grabbing all metrics data...

del X:\gta5\script\dev\Tools\Metrics\bin\script_metric_data.log

FOR /R "X:\gta5\script\dev\Tools\Metrics\LogFiles\"  %%X in (*.log) DO (

echo extracting from %%X 
type "%%X" | findstr /R ^.*\.*$ >> X:\gta5\script\dev\Tools\Metrics\bin\script_metric_data.log

)

echo END OF LOG >> X:\gta5\script\dev\Tools\Metrics\bin\script_metric_data.log





echo Successfully got all metrics data!




echo finished
pause

REM print error if something cocked up
IF %ERRORLEVEL% EQU 1 (
	echo .
	echo WARNING: Something cocked up.
	pause
)