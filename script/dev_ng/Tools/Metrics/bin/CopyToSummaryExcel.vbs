
'============== DEBUG FILE =========================
dim bUseDebugFile
dim debugFile
bUseDebugFile = TRUE

'create the debug file
Set filesys = CreateObject("Scripting.FileSystemObject")
if bUseDebugFile then
	Set debugFile = filesys.OpenTextFile("X:\gta5\script\dev\Tools\Metrics\bin\debug.txt", 2, True)
end if

function debug_write(strDebugString)
	if bUseDebugFile then
		debugFile.Write(CStr(strDebugString))
		debugFile.WriteLine()
	end if
end function


dim draggedFile

dim objExcel
dim objWorkbook, objWorksheet
dim objWorkbookSummary, objWorksheetSummary

'Launch Excel
Set objExcel = CreateObject("Excel.Application")

'Set Excel to be visible
objExcel.Application.Visible = false 'True

'open the summary workbook
Set objWorkbookSummary = objExcel.Workbooks.Open("X:\gta5\script\dev\Tools\Metrics\script_times")
Set objWorksheetSummary = objWorkbookSummary.Worksheets(1)

if WScript.Arguments.Count > 0 then
	for each draggedFile in WScript.Arguments
	
		Set objWorkbook = objExcel.Workbooks.Open(draggedFile)
	

		dim iVersionNo
		dim exeVersion
		dim scriptVersion
		dim iNumPlayers
		dim iNumBots
		dim script_name
		dim host_time
		dim client_time
		dim peak_time
		dim iActiveRow
		dim iDate
		dim iTime
		
	
		'for each worksheet in the workbook
		dim iNumWorksheets
		dim iWorkSheet
		iNumWorksheets = objWorkbook.Worksheets.Count
	
		debug_write(" iNumWorksheets = " & iNumWorksheets )
	
		for iWorkSheet=1 to iNumWorksheets
			
			set objWorksheet = objWorkbook.Worksheets(iWorkSheet)
			
						
			iVersionNo = objWorksheet.Cells(2, 2).Value
			exeVersion = objWorksheet.Cells(3, 2).Value
			scriptVersion = objWorksheet.Cells(4, 2).Value
			iNumPlayers = objWorksheet.Cells(1, 5).Value
			iNumBots = objWorksheet.Cells(2, 5).Value
			iDate = objWorksheet.Cells(5, 2).Value
			iTime = objWorksheet.Cells(6, 2).Value
			

			'find active column in excel
			dim iActiveCol
			dim bIsNewActiveCol
			dim i
			iActiveCol = 0
			i = 1
			bIsNewActiveCol = 0
			while iActiveCol = 0
				if IsEmpty(objWorksheetSummary.Cells(1, i).Value)  then
					iActiveCol = i
					bIsNewActiveCol = 1
				elseif ((objWorksheetSummary.Cells(1, i).Value = iVersionNo) and (objWorksheetSummary.Cells(2, i).Value = iNumPlayers) and (objWorksheetSummary.Cells(3, i).Value = iNumBots) and (StrComp(exeVersion, objWorksheetSummary.Cells(6, i).Value, 1) = 0 ) and (StrComp(scriptVersion, objWorksheetSummary.Cells(7, i).Value, 1) = 0 ) ) then
					iActiveCol = i
					bIsNewActiveCol = 0
				end if
				i = i + 1
			wend

			'write the version number and number of players to the sheet
			if (bIsNewActiveCol = 1) then 
				objWorksheetSummary.Cells(1, iActiveCol).Value = iVersionNo
				objWorksheetSummary.Cells(2, iActiveCol).Value = iNumPlayers
				objWorksheetSummary.Cells(3, iActiveCol).Value = iNumBots
				objWorksheetSummary.Cells(4, iActiveCol).Value = iDate
				objWorksheetSummary.Cells(5, iActiveCol).Value = iTime
				objWorksheetSummary.Cells(6, iActiveCol).Value = exeVersion
				objWorksheetSummary.Cells(7, iActiveCol).Value = scriptVersion
				
				objWorksheetSummary.Cells(1, iActiveCol+1).Value = "-"
				objWorksheetSummary.Cells(1, iActiveCol+2).Value = "-"
				objWorksheetSummary.Cells(8, iActiveCol).Value = "Host"
				objWorksheetSummary.Cells(8, iActiveCol+1).Value = "Client"
				objWorksheetSummary.Cells(8, iActiveCol+2).Value = "Peak"					
			end if
			
			
			script_name = objWorksheet.Cells(1, 2).Value
			host_time = objWorksheet.Cells(9, 2).Value
			client_time = objWorksheet.Cells(14, 2).Value
			peak_time = objWorksheet.Cells(20, 2).Value
			
			if isNumeric(host_time) or isNumeric(client_time) then 
			
				debug_write("worksheet for script " & script_name & ", host_time = " & host_time & ", av time = " & client_time & ", peak time = " & peak_time )
			
				'first find the row for this script
				iActiveRow = 0
				i = 1
				while iActiveRow = 0
					if (StrComp(script_name, objWorksheetSummary.Cells(i, 1).value, 1) = 0 ) or IsEmpty(objWorksheetSummary.Cells(i, 1).Value) then
						iActiveRow = i
					end if
					i = i + 1
				wend
		
				debug_write("iActiveRow = " & iActiveRow & ", iActiveCol = " & iActiveCol )
			
				'write the details of this script
				if IsEmpty(objWorksheetSummary.Cells(iActiveRow, 1).Value) then
					objWorksheetSummary.Cells(iActiveRow, 1).Value = script_name
				end if
				if IsEmpty(objWorksheetSummary.Cells(iActiveRow, iActiveCol).Value) then
					objWorksheetSummary.Cells(iActiveRow, iActiveCol).Value = host_time
					if IsNumeric(client_time) and IsNumeric(host_time) then
						if host_time > (client_time + 0.4) then
							objWorksheetSummary.Cells(iActiveRow, iActiveCol).Interior.Color = RGB(255,0,0)
						end if
					end if
				end if
				if IsEmpty(objWorksheetSummary.Cells(iActiveRow, iActiveCol+1).Value) then
					objWorksheetSummary.Cells(iActiveRow, iActiveCol+1).Value = client_time
				end if
				if IsEmpty(objWorksheetSummary.Cells(iActiveRow, iActiveCol+2).Value) then
					objWorksheetSummary.Cells(iActiveRow, iActiveCol+2).Value = peak_time
				end if
			
				set objWorksheet = nothing
				
				objWorkbookSummary.Save
			
			end if
			
		next 
		
		objWorkbookSummary.Save
	
	next
	
	objWorkbookSummary.Save
end if
	
WScript.Echo "Finished adding to summary excel"
	
objExcel.Quit

