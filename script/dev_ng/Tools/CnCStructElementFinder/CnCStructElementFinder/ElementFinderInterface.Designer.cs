﻿namespace CnCStructElementFinder
{
    partial class ElementFinderInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.structselect = new System.Windows.Forms.ComboBox();
            this.setpathtoscd = new System.Windows.Forms.Button();
            this.results = new System.Windows.Forms.ListBox();
            this.setindex = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dofind = new System.Windows.Forms.Button();
            this.openFileDLG = new System.Windows.Forms.OpenFileDialog();
            this.filetargets = new System.Windows.Forms.ComboBox();
            this.clipdump = new System.Windows.Forms.Button();
            this.filter = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // structselect
            // 
            this.structselect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.structselect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.structselect.FormattingEnabled = true;
            this.structselect.Location = new System.Drawing.Point(199, 33);
            this.structselect.Name = "structselect";
            this.structselect.Size = new System.Drawing.Size(257, 21);
            this.structselect.TabIndex = 0;
            // 
            // setpathtoscd
            // 
            this.setpathtoscd.Location = new System.Drawing.Point(13, 4);
            this.setpathtoscd.Name = "setpathtoscd";
            this.setpathtoscd.Size = new System.Drawing.Size(75, 23);
            this.setpathtoscd.TabIndex = 1;
            this.setpathtoscd.Text = "Add Path";
            this.setpathtoscd.UseVisualStyleBackColor = true;
            this.setpathtoscd.Click += new System.EventHandler(this.setpathtoscd_Click);
            // 
            // results
            // 
            this.results.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.results.FormattingEnabled = true;
            this.results.Location = new System.Drawing.Point(13, 87);
            this.results.MinimumSize = new System.Drawing.Size(524, 95);
            this.results.Name = "results";
            this.results.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.results.Size = new System.Drawing.Size(524, 121);
            this.results.TabIndex = 3;
            // 
            // setindex
            // 
            this.setindex.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.setindex.Location = new System.Drawing.Point(392, 61);
            this.setindex.Name = "setindex";
            this.setindex.Size = new System.Drawing.Size(64, 20);
            this.setindex.TabIndex = 4;
            this.setindex.TextChanged += new System.EventHandler(this.setindex_TextChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(155, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Struct:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(319, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Element index:";
            // 
            // dofind
            // 
            this.dofind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dofind.Location = new System.Drawing.Point(462, 7);
            this.dofind.Name = "dofind";
            this.dofind.Size = new System.Drawing.Size(75, 74);
            this.dofind.TabIndex = 7;
            this.dofind.Text = "Find it!";
            this.dofind.UseVisualStyleBackColor = true;
            this.dofind.Click += new System.EventHandler(this.dofind_Click);
            // 
            // openFileDLG
            // 
            this.openFileDLG.FileName = "Please select an scd file!";
            // 
            // filetargets
            // 
            this.filetargets.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.filetargets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.filetargets.FormattingEnabled = true;
            this.filetargets.Location = new System.Drawing.Point(95, 7);
            this.filetargets.Name = "filetargets";
            this.filetargets.Size = new System.Drawing.Size(361, 21);
            this.filetargets.TabIndex = 9;
            this.filetargets.SelectedIndexChanged += new System.EventHandler(this.filetargets_SelectedIndexChanged);
            // 
            // clipdump
            // 
            this.clipdump.Location = new System.Drawing.Point(13, 59);
            this.clipdump.Name = "clipdump";
            this.clipdump.Size = new System.Drawing.Size(159, 23);
            this.clipdump.TabIndex = 10;
            this.clipdump.Text = "Dump Selected To Clipboard";
            this.clipdump.UseVisualStyleBackColor = true;
            this.clipdump.Click += new System.EventHandler(this.dumpSelected);
            // 
            // filter
            // 
            this.filter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.filter.Location = new System.Drawing.Point(13, 33);
            this.filter.Name = "filter";
            this.filter.Size = new System.Drawing.Size(136, 20);
            this.filter.TabIndex = 11;
            this.filter.TextChanged += new System.EventHandler(this.filter_TextChanged);
            // 
            // ElementFinderInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 224);
            this.Controls.Add(this.filter);
            this.Controls.Add(this.clipdump);
            this.Controls.Add(this.filetargets);
            this.Controls.Add(this.dofind);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.setindex);
            this.Controls.Add(this.results);
            this.Controls.Add(this.setpathtoscd);
            this.Controls.Add(this.structselect);
            this.MinimumSize = new System.Drawing.Size(565, 262);
            this.Name = "ElementFinderInterface";
            this.Text = "CnC Element Finder -(Findstar?)";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox structselect;
        private System.Windows.Forms.Button setpathtoscd;
        private System.Windows.Forms.ListBox results;
        private System.Windows.Forms.TextBox setindex;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button dofind;
        private System.Windows.Forms.OpenFileDialog openFileDLG;
        private System.Windows.Forms.ComboBox filetargets;
        private System.Windows.Forms.Button clipdump;
        private System.Windows.Forms.TextBox filter;
    }
}

