﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace CnCStructElementFinder
{
    public partial class ElementFinderInterface : Form
    {
        //data 
        //string filepath = null;
        private SCDStructParser parsedfile = null;
        int targetindex = 0;

        //List<string> filepaths;
        



        public ElementFinderInterface()
        {
           // filepaths = new List<string>();
            InitializeComponent();

            setindex.Text = targetindex + "";

            //attempt to load filepath from module directory
            this.LoadSettingsDat();


        }



        private void InitiateRefresh(string filepath)
        {


            

            //now attempt to load the file and set targetscdpath

            parsedfile = new SCDStructParser(filepath);


            if (!parsedfile.isValid())
            {
                //targetscdpath.Text = "READING FAILED";

                MessageBox.Show("Reading failed");
            }


            //now populate structselect
            structselect.Items.Clear();


            List<SCDStructParser.ScriptStruct> sslist = parsedfile.GetStructs();

            bool dofilter = false;

            if(filter.Text.Length > 0)
            {
                dofilter = true;
            }

            foreach (SCDStructParser.ScriptStruct ss in sslist)
            {
                if(!dofilter)
                {
                    structselect.Items.Add(ss);
                }else{
                    if (ss.GetName().Contains(filter.Text))
                    {
                        structselect.Items.Add(ss);
                    }
                }
            }


            structselect.SelectedItem = null;
            results.Items.Clear();//TODO, potentially add note into log
            //targetscdpath.Text = filepath;

        }


        private void setpathtoscd_Click(object sender, EventArgs e)
        {
            openFileDLG.CheckFileExists = true;
            openFileDLG.CheckPathExists = true;
            openFileDLG.Filter = "Script build file (*.scd)|*.scd";
            DialogResult res = openFileDLG.ShowDialog();
            if (res == DialogResult.OK || res == DialogResult.Yes)
            {
                //add the path to 
                filetargets.Items.Add(openFileDLG.FileName);

                this.SaveSettingsDat();

                this.InitiateRefresh(openFileDLG.FileName);
            }
        }

        private void setindex_TextChanged(object sender, EventArgs e)
        {
            //parse for a valid integer
            //set targetindex 

            int i = 0;

            try
            {
                i = int.Parse(setindex.Text);
                setindex.BackColor = Color.White;

                if (i > -1)
                {
                    targetindex = i;
                }
                else
                {
                    setindex.BackColor = Color.Red;
                }
            }
            catch
            {
                setindex.BackColor = Color.Red;
            }



        }

        private void dofind_Click(object sender, EventArgs e)
        {
            if (structselect.SelectedItem == null)
            {
                MessageBox.Show("Please select a struct from the dropdown list before searching.");
                return;
            }

            SCDStructParser.ScriptStruct ss = (SCDStructParser.ScriptStruct)structselect.SelectedItem;



            string s = ss.DoFind(targetindex);



            results.Items.Add(s);
            results.SelectedIndex = results.Items.Count-1;
        }


        private void SaveSettingsDat()
        {
            //try to save the filepath
            
            FileStream w = null;
            try
            {
                string cwd = System.IO.Directory.GetCurrentDirectory();
                w = File.Create(cwd + "\\settings.dat");
                BinaryWriter b = new BinaryWriter(w);

                //b.Write(filepath);
                b.Write(filetargets.Items.Count);

                foreach (string s in filetargets.Items)
                {
                    b.Write(s);
                }
                
                b.Close();
                w.Close();
            }
            catch
            {
                if (w != null)
                {
                    w.Close();
                    w = null;
                }
            }


        }
        private void LoadSettingsDat()
        {
            FileStream r = null;
            try
            {
                string cwd = System.IO.Directory.GetCurrentDirectory();
                r = File.OpenRead(cwd +"\\settings.dat");
                BinaryReader b = new BinaryReader(r);

                //filepath = b.ReadString();


                filetargets.Items.Clear();

                int strings = b.ReadInt32();

                for (int i = 0; i < strings; ++i)
                {
                    filetargets.Items.Add(b.ReadString());
                }

                b.Close();
                r.Close();

            }
            catch
            {
                if (r != null)
                {
                    r.Close();
                    r = null;
                }
                //load path failed
                MessageBox.Show("Settings load failed, please select an scd file.");

                filetargets.Items.Clear();
            }
          
        }

        private void filetargets_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (filetargets.SelectedItem != null)
            {
                InitiateRefresh((string)filetargets.SelectedItem);
            }
        }

        private void dumpSelectedToClipboard()
        {
            if (results.SelectedItem == null)
            {
                return;
            }

            string sret = "";



            foreach(object o in results.SelectedItems)
            {
                sret += o.ToString() + "\r\n";
            }

            System.Windows.Forms.Clipboard.SetText(sret);
        }

        private void dumpSelected(object sender, EventArgs e)
        {
            dumpSelectedToClipboard();
        }

        private void filter_TextChanged(object sender, EventArgs e)
        {
            if (filter.Text.Length == 0 || parsedfile == null)
            {
                return;
            }

            //otherwise filter
            structselect.SelectedItem = null;
            structselect.Items.Clear();
            

            List<SCDStructParser.ScriptStruct> sslist = parsedfile.GetStructs();

            foreach (SCDStructParser.ScriptStruct ss in sslist)
            {
                if (ss.GetName().Contains(filter.Text))
                {
                    structselect.Items.Add(ss);
                }
            }
        }




    }
}
