﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CnCStructElementFinder
{
    class SCDStructParser
    {
        //a scd file contains a list of elements
        //we are interested in parsing and storing the structs

        private bool bValid = false;

       

        //struct data

        public class ScriptStruct
        {
            string data;
            string name;

            //these really should be in their own container class
            Dictionary<int, string> entries;
            Dictionary<int, ScriptStruct> recursion;
            Dictionary<int, string> elemnames;
            Dictionary<int, int> arrays;
            Dictionary<int, int> arrays2d;//todo, support 3d+ arrays and make the depth of arrays scale nicely

            int elements = 0;

            public ScriptStruct(string raw)
            {
                data = raw;

                name = raw.Split(',')[1];

                

                entries = new Dictionary<int, string>();
                recursion = new Dictionary<int, ScriptStruct>();
                elemnames = new Dictionary<int, string>();
                arrays = new Dictionary<int, int>();
                arrays2d = new Dictionary<int, int>();
            }

            internal void AddElement(int lineno, string s)
            {
                entries.Add(lineno,s);
                ++elements;


                //parse the name
                string[] spl = s.Split(',');


                string name = spl[2];
                int arraylen = 1;
                //check for array length
                if (s.Contains('['))
                {
                    string[] aspl = spl[1].Split(']');

                    //name = aspl[0];
   
                    if (aspl.Length > 2)
                    {
                        //name = aspl[2];
                        arraylen = int.Parse(aspl[0].Replace('[', ' '));
                        int val2d = int.Parse(aspl[1].Replace('[', ' '));

                        arrays2d.Add(lineno, val2d);
                    }
                    else if (aspl.Length > 1)
                    {
                        //name = aspl[1];
                        arraylen = int.Parse(aspl[0].Replace('[', ' '));
                    }
                }
                elemnames.Add(lineno, name);
                arrays.Add(lineno,arraylen);
                //parse for array data

            }
            internal void AddRecursion(int lineno, ScriptStruct re)
            {
                recursion.Add(lineno, re);
            }
            public override string ToString()
            {
                return name;
            }

            internal string DoFind(int targetindex)
            {

                int greatestSmallest = 0;
                
                foreach (int i in entries.Keys)
                {
                    if (i > greatestSmallest)
                    {
                        if (!(i > targetindex))
                        {
                            greatestSmallest = i;
                        }
                    }
                }

                int newind = targetindex - greatestSmallest;

                string arrayData = "";
                int alen = arrays[greatestSmallest];


                if (alen > 1)
                {
                    arrayData += "[" + alen + "]";
                

                    if (arrays2d.ContainsKey(greatestSmallest))
                    {
                        arrayData += "[" + arrays2d[greatestSmallest] + "]";
                    }
                }



                if (recursion.ContainsKey(greatestSmallest))
                {
                    
                    return name + "->" + elemnames[greatestSmallest] + arrayData + " >>>> " + recursion[greatestSmallest].DoFind(newind);
                }
                else
                {
                    return name + "->" + elemnames[greatestSmallest] + arrayData + "";
                }
            }

            private int GetSize()
            {
                return elements;
            }

            internal string[] GetFieldStrings()
            {
                List<string> sns = new List<string>();
                foreach (string s in entries.Values)
                {
                    sns.Add(s);
                }
                return sns.ToArray();
            }

            internal string GetData()
            {
                return data;
            }

            internal string GetName()
            {
                return name;
            }
        }
        private List<ScriptStruct> structs;



        //struct parser
        ScriptStruct instruct = null;



        public SCDStructParser( string path )
        {
            structs = new List<ScriptStruct>();
            StreamReader r = null;
            string massdump = null;
            try
            {
                r = new StreamReader(path);
                massdump = r.ReadToEnd();
                r.Close();r = null;
            }
            catch
            {
                if (r != null)
                {
                    r.Close();
                    r = null;
                }
                return;
            }

            //now attempt to parse the contents of massdump
            string[] tok = new string[1]; tok[0] = "\r\n";
            string[] spl = massdump.Split(tok, StringSplitOptions.RemoveEmptyEntries);


            foreach (string s in spl)
            {
                string[] lnslp = s.Split(',');


                if (instruct != null)
                {
                    //check for this line being a field
                        //if it is then get index and save into the current struct
                    if (lnslp[0].CompareTo("  FIELD") == 0)
                    {
                        int lineno = int.Parse(lnslp[lnslp.Length-1]);//final index should contain element count

                        instruct.AddElement(lineno,s);
                    }
                    else
                    {
                        //otherwise bail out of this, and continue seeking structs
                        instruct = null;
                    }

                }

                if (instruct == null)
                {
                    //check for this line being a struct
                    if (lnslp[0].CompareTo("STRUCT") == 0)
                    {
                        //hooray! add a new element to the list
                        instruct = new ScriptStruct(s);
                        structs.Add(instruct);

                    }
                }               
            }

            //now do a pass to check for recursion

            foreach (ScriptStruct st in structs)
            {
               // string[] lnslp = st.ToString().Split(',');

                string[] flds = st.GetFieldStrings();


                foreach (string si in flds)
                {
                    
                    string[] endat = si.Split(',');
                    string[] stype = endat[1].Split(' ');
                    string[] arraysplit = stype[0].Split(']');//horrible hacky way of seperating off the array length
                    string sname = arraysplit[0];

                    if (arraysplit.Length > 2)
                    {
                        sname = arraysplit[2];
                    }
                    else if (arraysplit.Length > 1)
                    {
                        sname = arraysplit[1];
                    }

                    if (sname.CompareTo("STRUCT") == 0)
                    {
                        //its a struct, find the recursion and bind
                        string nameToFind = stype[1];
                        int index = int.Parse(endat[endat.Length-1]);
                        ScriptStruct re = null;

                        foreach (ScriptStruct sr in structs)
                        {
                            if (sr != st)
                            {
                                if (nameToFind.CompareTo(sr.GetName()) == 0)
                                {
                                    re = sr;
                                    break;
                                }
                            }
                        }



                        if (re != null)
                        {
                            st.AddRecursion(index,re);
                        }


                    }
                }




                //int id = flds[flds.Length - 1];



            }


            bValid = true;
        }






        internal bool isValid()
        {
            return bValid;
        }

        internal List<ScriptStruct> GetStructs()
        {
            return structs;
        }
    }
}
