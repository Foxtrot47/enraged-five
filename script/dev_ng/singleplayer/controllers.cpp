#include "csupport.h" 
#include "./scripts/Ambient/family/family_controller.sc.cpp" 
#include "./scripts/Ambient/Misc/bootycallhandler.sc.cpp" 
#include "./scripts/Ambient/Misc/emergencycalllauncher.sc.cpp" 
#include "./scripts/Ambient/Misc/trainlinecontroller.sc.cpp" 
#include "./scripts/Ambient/Races/dragracelauncher.sc.cpp" 
#include "./scripts/blipcontrol/blip_controller.sc.cpp" 
#include "./scripts/blipcontrol/blip_discoverer.sc.cpp" 
#include "./scripts/buildingcontrol/building_controller.sc.cpp" 
#include "./scripts/cellphone/cellphone_controller.sc.cpp" 
#include "./scripts/cellphone/dialogue_handler.sc.cpp" 
#include "./scripts/codecontrol/code_controller.sc.cpp" 
#include "./scripts/commscontrol/comms_controller.sc.cpp" 
#include "./scripts/contextcontrol/context_controller.sc.cpp" 
#include "./scripts/deadped/deadpedman.sc.cpp" 
#include "./scripts/Drunk/drunk_controller.sc.cpp" 
#include "./scripts/email/email_controller.sc.cpp" 
#include "./scripts/eventcontrol/event_controller.sc.cpp" 
#include "./scripts/finance/bank_controller.sc.cpp" 
#include "./scripts/finance/stock_controller.sc.cpp" 
#include "./scripts/floatinghelp/floating_help_controller.sc.cpp" 
#include "./scripts/flowHelp/flow_help_controller.sc.cpp" 
#include "./scripts/friends/friends_controller.sc.cpp" 
#include "./scripts/heists/controllers/heist_ctrl_docks.sc.cpp" 
#include "./scripts/heists/controllers/heist_ctrl_fbi.sc.cpp" 
#include "./scripts/heists/controllers/heist_ctrl_finale.sc.cpp" 
#include "./scripts/heists/controllers/heist_ctrl_jewel.sc.cpp" 
#include "./scripts/heists/controllers/heist_ctrl_nice.sc.cpp" 
#include "./scripts/heists/controllers/heist_ctrl_rural.sc.cpp" 
#include "./scripts/helpcontroller/help_controller.sc.cpp" 
#include "./scripts/MissionFlow/flow_controller.sc.cpp" 
#include "./scripts/MissionFlow/shitskip_messenger.sc.cpp" 
#include "./scripts/missionlauncher/candidate_controller.sc.cpp" 
#include "./scripts/missionlauncher/mission_triggerer.sc.cpp" 
#include "./scripts/missionstats/mission_stat_alerter.sc.cpp" 
#include "./scripts/missionstats/mission_stat_watcher.sc.cpp" 
// #include "./scripts/playercontrol/player_controller.sc.cpp" 
#include "./scripts/RandomChar/randomchar_controller.sc.cpp" 
#include "./scripts/Replay/replay_controller.sc.cpp" 
#include "./scripts/respawncontrol/respawn_controller.sc.cpp" 
#include "./scripts/savegame/autosave_controller.sc.cpp" 
#include "./scripts/selectorcontrol/selector.sc.cpp" 
#include "./scripts/shops/shop_controller.sc.cpp" 
#include "./scripts/stats/stats_controller.sc.cpp" 
#include "./scripts/vehiclecontrol/vehicle_gen_controller.sc.cpp" 
script_def *defs[] = { 
&family_controller_script_def, 
&bootycallhandler_script_def, 
&emergencycalllauncher_script_def, 
&trainlinecontroller_script_def, 
&dragracelauncher_script_def, 
&blip_controller_script_def, 
&blip_discoverer_script_def, 
&building_controller_script_def, 
&cellphone_controller_script_def, 
&dialogue_handler_script_def, 
&code_controller_script_def, 
&comms_controller_script_def, 
&context_controller_script_def, 
&deadpedman_script_def, 
&drunk_controller_script_def, 
&email_controller_script_def, 
&event_controller_script_def, 
&bank_controller_script_def, 
&stock_controller_script_def, 
&floating_help_controller_script_def, 
&flow_help_controller_script_def, 
&friends_controller_script_def, 
&heist_ctrl_docks_script_def, 
&heist_ctrl_fbi_script_def, 
&heist_ctrl_finale_script_def, 
&heist_ctrl_jewel_script_def, 
&heist_ctrl_nice_script_def, 
&heist_ctrl_rural_script_def, 
&help_controller_script_def, 
&flow_controller_script_def, 
&shitskip_messenger_script_def, 
&candidate_controller_script_def, 
&mission_triggerer_script_def, 
&mission_stat_alerter_script_def, 
&mission_stat_watcher_script_def, 
// &player_controller_script_def, 
&randomchar_controller_script_def, 
&replay_controller_script_def, 
&respawn_controller_script_def, 
&autosave_controller_script_def, 
&selector_script_def, 
&shop_controller_script_def, 
&stats_controller_script_def, 
&vehicle_gen_controller_script_def, 
}; 
#define MODULE_NAME "SCRIPT_controllers" 
#include "csupportentry.h" 
