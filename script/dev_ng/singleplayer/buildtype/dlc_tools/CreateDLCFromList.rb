require 'pipeline/config/projects'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_rpf'
include Pipeline
SCO_FILES_PATH = "X:/gta5/script/dev_network/singleplayer/sco/"
PLATFORM_NAME=["xbox360","ps3"]
PLATFORM_RBS_NAME=["xenon","ps3"]
PLATFORM_EXT=["xsc","csc"]
XBOX360 = 0
PS3 = 1


def CreateRPF(buildtype,platform,input,outputDir, rpfName )
	begin 
	output = File.open("temp.rbs","w+")
	rescue
		puts "Can't open temp"
		exit
	end
	output << "set_platform(\"#{PLATFORM_RBS_NAME[platform]}\")\n"
	output << "start_pack()\n"
	SetFilesToPack(buildtype,input,platform,output)
	outDir = outputDir.dup
	outDir.gsub!(/\\/,"/")
	saveDir = "#{outDir}/#{PLATFORM_NAME[platform]}/data/"
	FileUtils.mkdir_p saveDir
	targetFile = "#{saveDir}#{rpfName}.rpf"
	system("p4 edit -k #{targetFile}")
	
	output << "save_pack(\"#{targetFile}\")\n"
	output << "close_pack()\n"
	output.close()
	system("ragebuilder_0378.exe temp.rbs")
	File.delete("temp.rbs")
	
end
	

def SetFilesToPack(buildtype,fileList,platform,output)
	fileList.gsub!(/\r\n?/, "\n")
	filesToPack = Array.new
	fileList.each_line 	do |a| 
		output<<"add_to_pack(\"#{SCO_FILES_PATH}#{buildtype}/#{PLATFORM_NAME[platform]}/#{a.gsub(/\n/,"")}.#{PLATFORM_EXT[platform]}\" , \"#{a.gsub(/\n/,"")}.#{PLATFORM_EXT[platform]}\", true ) \n"
	end
end

def main()
	begin
		if ARGV.length<4
			puts "not enough args"
			exit
		end
		buildtype = ARGV[0]
		filelist = ARGV[1]
		outputDir = ARGV[2]
		outputFile = ARGV[3]
		text = File.open("#{filelist}.txt", "rb").read	
		CreateRPF(buildtype,PS3,text,outputDir, outputFile)
		CreateRPF(buildtype,XBOX360,text,outputDir,outputFile)
	rescue
	 puts "File list read error, make sure file list provided exists"
	 exit
	end
end

main()