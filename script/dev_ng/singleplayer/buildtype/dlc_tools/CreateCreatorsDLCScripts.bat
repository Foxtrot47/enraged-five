@ECHO OFF
IF "%1"=="" goto :noParam

%RS_TOOLSRUBY% CreateDLCFromList.rb %1 FileList_creatorFiles X:\gta5_dlc\scriptPacks\dlc_Creator_controllers\build\dev creatorDLC
%RS_TOOLSRUBY% CreateDLCFromList.rb %1 FileList_creatorFilesPaid X:\gta5_dlc\scriptPacks\dlc_creators\build\dev creatorDLCPaid
goto :eof
:noParam
ECHO No Parameters supplied, run debug/release versions instead
