// RELEASE JAPANESE
// buildtype.sch
// used for building different versions based on the dropdown box in the script editor.

CONST_INT DEFINE_NM_BUILD_MODE	0 //a build type for NM so we can comment out game specific references where possible.
CONST_INT IS_JAPANESE_BUILD		1
CONST_INT USE_SP_DLC			0
CONST_INT USE_CLF_DLC			0
CONST_INT USE_NRM_DLC			0

CONST_INT IS_NEXTGEN_BUILD		1
CONST_INT IS_DEBUG_OR_PROFILE_BUILD	0

// Defaulting all scripts to compile in title update specific script.
// Individual scripts can override this to omit TU changes.
#IF NOT DEFINED(USE_TU_CHANGES)
	CONST_INT 	USE_TU_CHANGES	1
#ENDIF
