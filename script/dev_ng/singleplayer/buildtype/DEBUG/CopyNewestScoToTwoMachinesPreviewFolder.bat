@echo off

rem setLocal EnableDelayedExpansion
rem setlocal

set SecondMachine=ediw-aduthie

ECHO Y | DEL \\%SecondMachine%\d$\gta5\build\dev_ng\preview\*.*
echo Deleted \\%SecondMachine%\d$\gta5\build\dev_ng\preview\

ECHO Y | DEL \\%SecondMachine%\d$\gta5\build\release\preview\*.*
echo Deleted \\%SecondMachine%\d$\gta5\build\dev_ng\preview\

ECHO Y | DEL \\%SecondMachine%\d$\gta5\build\dev_ng\preview\*.*
echo Deleted \\%SecondMachine%\d$\gta5\build\dev_ng\preview\

set BaseSrcDir=X:\gta5\script\dev_ng\singleplayer\sco\DEBUG\

set srcDir=%BaseSrcDir%
set extensionToMatch=sco
CALL :CopyLastModified

::set srcDir=%BaseSrcDir%\ps3
::set extensionToMatch=csc
::CALL :CopyLastModified

::set srcDir=%BaseSrcDir%\win32
::set extensionToMatch=wsc
::CALL :CopyLastModified

set srcDir=%BaseSrcDir%\win64
set extensionToMatch=ysc
CALL :CopyLastModified

::set srcDir=%BaseSrcDir%\xbox360
::set extensionToMatch=xsc
::CALL :CopyLastModified

PAUSE

GOTO :eof


:CopyLastModified
set lastmod=

pushd %srcDir%

for /f "tokens=*" %%a in ('dir *.%extensionToMatch% /b /od') do set lastmod=%%a

if "%lastmod%"=="" echo Could not find any files in source directory.&goto :eof


SET Branch=dev_ng
CALL :CopyOneFile

SET Branch=release
CALL :CopyOneFile

SET Branch=dev_ng
CALL :CopyOneFile

GOTO :eof


:CopyOneFile
set destDir=X:\gta5\build\%Branch%\preview\
echo Copying %lastmod% to %destDir%

IF NOT EXIST %destDir% MKDIR %destDir%
copy "%lastmod%" %destDir%

PING %SecondMachine% -n 1 | FIND "TTL=" > NUL
IF ERRORLEVEL 1 GOTO eof

set destDir=\\%SecondMachine%\d$\gta5\build\%Branch%\preview\
echo Copying %lastmod% to %destDir%

IF NOT EXIST %destDir% MKDIR %destDir%
copy "%lastmod%" %destDir%

GOTO :eof

