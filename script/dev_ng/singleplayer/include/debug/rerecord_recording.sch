using "rage_builtins.sch"
using "globals.sch"

#IF IS_DEBUG_BUILD
using "commands_pad.sch" 
using "commands_player.sch"
using "commands_streaming.sch"
using "commands_vehicle.sch" 
using "script_debug.sch"
USING "script_maths.sch"


CONST_INT STATE_INITIALISE 	0
CONST_INT STATE_PROCESS		1
CONST_INT STATE_RESET		2

INT iState
INT iReRecNo 
INT iNewRecNo

BOOL bResetToStart
BOOL bShowPath
BOOL bShowGhost
BOOL bStartOnButtonPress
BOOL bStopOnButtonPress
BOOL bNewCarUsesAI
BOOL bNewCarUsesRecording
BOOL bIsRecording
BOOL bRecordingHasStarted
BOOL bHasSwitchedToAI
BOOL bFreeCar	   
BOOL bReRecShowOriginalRecording 
BOOL bStopRecordingWhenOriginalStops
BOOL bSetReady
BOOL bReRecInitialized

FLOAT fInitialSpeed
FLOAT fCarYOffset = 5.0 
FLOAT fForceMultiplierPosition = 0.0 
FLOAT fForceMultiplierHeading = 0.0 
FLOAT fCruiseSpeedMultiplier = 2.0
FLOAT fPlaybackTime = 0.0



MODEL_NAMES mReRecCar

PED_INDEX GhostDriverID

VEHICLE_INDEX GhostCarID
VEHICLE_INDEX NewCarID

TEXT_WIDGET_ID TextInput
text_widget_id car_rec_input

SCENARIO_BLOCKING_INDEX sbiRerecordBlockingArea


FUNC FLOAT GET_DISTANCE_FROM_LEAD_VEHICLE(VEHICLE_INDEX LeadCar, VEHICLE_INDEX FollowCar)

	// Car A is assumed to be the car in front

	VECTOR vForward
	VECTOR vAB

	VECTOR vPosA
	VECTOR vPosB

	FLOAT fAngle

	IF IS_VEHICLE_DRIVEABLE(LeadCar)
		vForward.x = GET_ENTITY_FORWARD_X(LeadCar)
		vForward.y = GET_ENTITY_FORWARD_Y(LeadCar)
		vPosA = GET_ENTITY_COORDS(LeadCar)
	ENDIF

	IF IS_VEHICLE_DRIVEABLE(FollowCar)
		vPosB = GET_ENTITY_COORDS(FollowCar)
	ENDIF

	vAB = vPosB - vPosA

	fAngle = GET_ANGLE_BETWEEN_2D_VECTORS(vAB.x, vAB.y, vForward.x, vForward.y)

	fAngle += -90.0
	IF (fAngle < 0.0)
		fAngle *= -1.0
	ENDIF
	
	vAB.z = 0.0
	RETURN(VMAG(vAB) * SIN(fAngle))

ENDFUNC

FUNC BOOL IS_VEHICLE_IN_FRONT_OF_VEHICLE(VEHICLE_INDEX inCar1, VEHICLE_INDEX inCar2)

	VECTOR posA		// car1 position
	VECTOR posB 	// car2 position
	VECTOR posC
	VECTOR vecAB 	// vector from car1 to car2
	VECTOR vecCar2	// unit vector pointing in the direction the car2 is facing
	FLOAT fTemp
	
	// get positions
	IF IS_VEHICLE_DRIVEABLE(inCar1)
		posA = GET_ENTITY_COORDS(inCar1)
	ENDIF
	IF IS_VEHICLE_DRIVEABLE(inCar2)
		posB = GET_ENTITY_COORDS(inCar2)
	ENDIF

	// get vec from car1 to car2
	vecAB = posB - posA
	
	
	// get unit vector pointing forwards from car2
	IF IS_VEHICLE_DRIVEABLE(inCar2)
		posC = get_offset_from_entity_in_world_coords(inCar2, <<0.0, 5.0, 0.0>>)
		vecCar2 = posC - posB
	ENDIF

	// take the z out
	vecAB.z = 0.0
	vecCar2.z = 0.0

	// calculate dot product of vecAB and vecCar2
	fTemp = DOT_PRODUCT(vecAB,vecCar2)
	IF (fTemp < 0.0)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)

ENDFUNC

FUNC BOOL IS_CAR_PAUSED_AT_START_OF_RECORDING(VEHICLE_INDEX inCar, INT iRec)

	FLOAT fTemp

	IF IS_VEHicle_DRIVEABLE(inCar)
		IF NOT IS_PLAYBACK_GOING_ON_FOR_vehicle(inCar)
			START_PLAYBACK_RECORDED_vehicle(inCar, iRec, GET_CONTENTS_OF_TEXT_WIDGET(car_rec_input))	
		ELSE
			
			RECORDING_ID rID = GET_CURRENT_PLAYBACK_FOR_VEHICLE(inCar)
			STRING sVehRecName = GET_VEHICLE_RECORDING_NAME(rID) //this is full name + rec number
			      
			//To get the rec number
			INT iVehicleRecordingNumber
			STRING_TO_INT(GET_STRING_FROM_STRING(sVehRecName,GET_LENGTH_OF_LITERAL_STRING(sVehRecName)-3,GET_LENGTH_OF_LITERAL_STRING(sVehRecName)),iVehicleRecordingNumber)

			//to get the recording name
			//sVehRecName=GET_STRING_FROM_STRING(sVehRecName,0,GET_LENGTH_OF_LITERAL_STRING(sVehRecName)-3)
			
			IF (iVehicleRecordingNumber = iRec)
				fTemp = get_TIME_POSITION_IN_RECORDING(inCar)
				SKIP_TIME_IN_PLAYBACK_RECORDED_vehicle(inCar, (0.0 - fTemp))
				PAUSE_PLAYBACK_RECORDED_vehicle(inCar)
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF

	RETURN(FALSE)

ENDFUNC


PROC DO_RE_RECORDING_WIDGET(WIDGET_GROUP_ID widgetGroupReRecord = NULL)

	VECTOR pos
	FLOAT fTemp, fDist, fSpeed
	
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	
	SWITCH iState
		CASE STATE_INITIALISE

			
			if is_player_playing(player_id())
				clear_player_wanted_level(player_id())
			endif 
			//OVERRIDE_NUMBER_OF_PARKED_CARS(0)

			// default settings
			iReRecNo = 1
			iNewRecNo = 0
			bResetToStart = FALSE
			bShowPath = TRUE
			bShowGhost = FALSE // TRUE
			bStartOnButtonPress = TRUE
			bNewCarUsesAI = FALSE
			bNewCarUsesRecording = FALSE
			bStopOnButtonPress = TRUE
			bIsRecording = FALSE
			bRecordingHasStarted = FALSE
			bReRecInitialized = FALSE
			fPlaybackTime = 0.0
			
			IF widgetGroupReRecord <> NULL
				SET_CURRENT_WIDGET_GROUP(widgetGroupReRecord)
			ENDIF
			
				start_widget_group("Recording Re-Recorder")
					ADD_WIDGET_FLOAT_READ_ONLY("Playback Time", fPlaybackTime)
					car_rec_input = ADD_TEXT_WIDGET("car recording string")
					ADD_WIDGET_int_SLIDER("Old Recording Number", iReRecNo, 1, 3000, 1)
					ADD_WIDGET_int_SLIDER("New Recording Number", iNewRecNo, 0, 3000, 1)
					TextInput = ADD_TEXT_WIDGET("Car Model")
					add_widget_bool("Load the above and goto start", bResetToStart)
					add_widget_bool("Free car from start position", bFreeCar)
					add_widget_bool("Show Recording Path", bShowPath)
					add_widget_bool("Show Ghost Car", bShowGhost)
					add_widget_bool("Start Recording when accelerator is pressed?", bStartOnButtonPress)
					add_widget_bool("New Car drives with AI", bNewCarUsesAI)
					add_widget_bool("New Car drives with original recording", bNewCarUsesRecording) 
					add_widget_bool("Stop Recording when PS SQUARE OR XBOX X is pressed", bStopOnButtonPress) 
					add_widget_bool("Record New Car", bIsRecording)
					add_widget_bool("Show Original Recording", bReRecShowOriginalRecording)
					add_widget_bool("bStopRecordingWhenOriginalStops", bStopRecordingWhenOriginalStops) 
				stop_WIDGET_GROUP()
			IF widgetGroupReRecord <> NULL
				CLEAR_CURRENT_WIDGET_GROUP(widgetGroupReRecord)
			ENDIF
			
			// default car model 
			SET_CONTENTS_OF_TEXT_WIDGET(TextInput, "biff")
			SET_CONTENTS_OF_TEXT_WIDGET(car_rec_input, "larrytruck")

			iState = STATE_PROCESS

		BREAK

		CASE STATE_PROCESS
			IF sbiRerecordBlockingArea = NULL
				sbiRerecordBlockingArea = ADD_SCENARIO_BLOCKING_AREA(<<-9999.9, -9999.9, -9999.9>>, <<9999.9, 9999.9, 9999.9>>)
			ENDIF


			IF (bReRecShowOriginalRecording)
				IF HAS_vehicle_RECORDING_BEEN_LOADED(iReRecNo, GET_CONTENTS_OF_TEXT_WIDGET(car_rec_input))
					bShowGhost = FALSE
					IF DOES_ENTITY_EXIST(NewCarID)
						IF NOT IS_ENTITY_DEAD(NewCarID)
							IF IS_PLAYBACK_GOING_ON_FOR_vehicle(NewCarID)
								STOP_PLAYBACK_RECORDED_vehicle(NewCarID)
							ENDIF
							START_PLAYBACK_RECORDED_vehicle(NewCarID, iReRecNo, GET_CONTENTS_OF_TEXT_WIDGET(car_rec_input))
						ENDIF
					ENDIF
					bReRecShowOriginalRecording = FALSE
				ELSE
					bResetToStart = TRUE
				ENDIF
			ENDIF


			// reset
			IF (bResetToStart)
				iState = STATE_RESET
				BREAK
			ENDIF


			// show path
			IF DOES_ENTITY_EXIST(GhostCarID)
				IF NOT IS_ENTITY_DEAD(GhostCarID)
					IF IS_PLAYBACK_GOING_ON_FOR_vehicle(GhostCarID)
						IF (bShowPath)
							DISPLAY_PLAYBACK_RECORDED_vehicle(GhostCarID, RDM_WHOLELINE)
						ELSE
							DISPLAY_PLAYBACK_RECORDED_vehicle(GhostCarID, RDM_NONE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			// show ghost & driver
			IF DOES_ENTITY_EXIST(GhostCarID)
				IF NOT IS_ENTITY_DEAD(GhostCarID)
					SET_ENTITY_VISIBLE(GhostCarID, bShowGhost)
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(GhostDriverID)
				IF NOT IS_ENTITY_DEAD(GhostDriverID)
					SET_ENTITY_VISIBLE(GhostDriverID, bShowGhost)
				ENDIF
			ENDIF


			// start recording
			IF (bStartOnButtonPress)
				IF bReRecInitialized
					IF NOT (bNewCarUsesAI)
					AND NOT (bNewCarUsesRecording) 
						IF IS_BUTTON_PRESSED(PAD1, RIGHTSHOULDER2)
							bIsRecording = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			// free car
			IF (bFreeCar)
				IF DOES_ENTITY_EXIST(NewCarID)
					IF NOT IS_ENTITY_DEAD(NewCarID)
						IF IS_PLAYBACK_GOING_ON_FOR_vehicle(NewCarID)
					 		fInitialSpeed = GET_ENTITY_SPEED(NewCarID)
							PRINTSTRING("car speed = ")
							PRINTFLOAT(fInitialSpeed)
							PRINTSTRING("\n")
					 		STOP_PLAYBACK_RECORDED_vehicle(NewCarID)
							FREEZE_ENTITY_POSITION(NewCarID, TRUE)
							SET_vehicle_FORWARD_SPEED(NewCarID, 0.0)
							FREEZE_ENTITY_POSITION(NewCarID, FALSE)
							APPLY_FORCE_TO_ENTITY(NewCarID, APPLY_TYPE_FORCE, <<0.0, 0.0, -10.0>>, <<0.0, 0.0, 0.0>>, 0, FALSE, TRUE, TRUE)
							bFreeCar = FALSE
					 	ENDIF
					ENDIF
				ENDIF
			ENDIF

			// start recording
			IF (bIsRecording) 
			AND NOT (bRecordingHasStarted)
				
				IF HAS_vehicle_RECORDING_BEEN_LOADED(iReRecNo, GET_CONTENTS_OF_TEXT_WIDGET(car_rec_input))
				
					// unpause ghost car
					IF DOES_ENTITY_EXIST(GhostCarID)
						IF NOT IS_ENTITY_DEAD(GhostCarID)
							IF IS_PLAYBACK_GOING_ON_FOR_vehicle(GhostCarID)
								UNPAUSE_PLAYBACK_RECORDED_vehicle(GhostCarID)	
							ENDIF
						ENDIF
					ENDIF

					// stop the players car recording
					IF DOES_ENTITY_EXIST(NewCarID)
						IF NOT IS_ENTITY_DEAD(NewCarID)
							IF NOT (bNewCarUsesAI)
							AND NOT (bNewCarUsesRecording)
								IF IS_PLAYBACK_GOING_ON_FOR_vehicle(NewCarID)
									STOP_PLAYBACK_RECORDED_vehicle(NewCarID)
								ENDIF
							ELSE
								IF IS_PLAYBACK_GOING_ON_FOR_vehicle(NewCarID)
									UNPAUSE_PLAYBACK_RECORDED_vehicle(NewCarID)
									IF (bNewCarUsesAI)
										SET_PLAYBACK_TO_USE_AI(NewCarID)
									ENDIF
								ELSE
									START_PLAYBACK_RECORDED_VEHICLE_USING_AI(NewCarID, iReRecNo, GET_CONTENTS_OF_TEXT_WIDGET(car_rec_input))
								ENDIF	
							ENDIF
						ENDIF
					ENDIF

					// set the intial speed the same
					IF DOES_ENTITY_EXIST(NewCarID)
						IF NOT IS_ENTITY_DEAD(NewCarID)
							SET_vehicle_FORWARD_SPEED(NewCarID, fInitialSpeed)
						ENDIF
					ENDIF

					// start recording car
					IF DOES_ENTITY_EXIST(NewCarID)
						IF NOT IS_ENTITY_DEAD(NewCarID)
							START_RECORDING_vehicle(NewCarID, iNewRecNo, GET_CONTENTS_OF_TEXT_WIDGET(car_rec_input), true)

							PRINT_NOW("CRECSTART5", 3000, 1)
						ENDIF
					ENDIF

					bRecordingHasStarted = TRUE

				ELSE
					bResetToStart = TRUE
				ENDIF

			ENDIF

			// if recording is happening with ai, speed up or slow down if behind original
			IF (bIsRecording)
			AND (bRecordingHasStarted)
				IF (bNewCarUsesAI)
					IF IS_PLAYER_PLAYING(PLAYER_ID())
						IF DOES_ENTITY_EXIST(GhostCarID)
						AND DOES_ENTITY_EXIST(NewCarID)
							IF NOT IS_ENTITY_DEAD(GhostCarID)
							AND NOT IS_ENTITY_DEAD(NewCarID)
								
								IF IS_PLAYBACK_GOING_ON_FOR_vehicle(GhostCarID)
									
									VECTOR posA, posB
									VECTOR vec
									
									posA = get_offset_from_entity_in_world_coords(NewCarID, <<0.0, 0.0, 0.0>>)
									posb = get_offset_from_entity_in_world_coords(GhostCarID, <<0.0, 0.0, 0.0>>)
									
									vec = posB - posA
									vec *= fForceMultiplierPosition
									vec.z = 0.0

									APPLY_FORCE_TO_ENTITY(NewCarID, APPLY_TYPE_FORCE, <<vec.x, vec.y, vec.z>>, <<0.0, 0.0, 0.0>>, 0, FALSE, TRUE, TRUE)



									FLOAT fHeadingA, fHeadingB
									FLOAT fHeading

									// make sure car is pointing towards other car
									fHeadingB = GET_ENTITY_HEADING(GhostCarID)
									fHeadingA = GET_ENTITY_HEADING(NewCarID)

									fHeading = fHeadingB - fHeadingA
									IF (fHeading > 180.0)
										fHeading += -360.0
									ENDIF
									IF (fHeading < -180.0)
										fHeading += 360.0
									ENDIF

									// max degree shift is 10
									IF (fHeading > 10.0)
										fHeading = 10.0
									ENDIF
									IF (fHeading < -10.0)
										fHeading = -10.0
									ENDIF
									
//										PRINTSTRING("ReRecording fHeading = ")
//										PRINTFLOAT(fHeading)
//										PRINTSTRING("\n")

									fTemp = fHeading / 10.0
									fTemp *= fForceMultiplierHeading

									APPLY_FORCE_TO_ENTITY(NewCarID, APPLY_TYPE_FORCE, <<fTemp, 0.0, 0.0>>, <<0.0, fCarYOffset, 0.0>>, 0, TRUE, TRUE, TRUE)
									
									
									
									fDist = GET_DISTANCE_FROM_LEAD_VEHICLE(GhostCarID, NewCarID)
									fSpeed = GET_ENTITY_SPEED(GhostCarID)
									IF NOT IS_VEHICLE_IN_FRONT_OF_VEHICLE(GhostCarID, NewCarID)
										fDist *= -1.0
									ENDIF


									// max dist 2.0
									IF (fDist > 3.0)
										fDist = 3.0
									ENDIF
									IF (fDist < -3.0)
										fDist = -3.0
									ENDIF
//										PRINTSTRING("ReRecording fDist = ")
//										PRINTFLOAT(fDist)
//										PRINTSTRING("\n")

									// max speed difference is 2.0
									fTemp = fDist / 3.0
									fTemp *= fCruiseSpeedMultiplier
									//fTemp += 1.0
									
									

									SET_DRIVE_TASK_CRUISE_SPEED(PLAYER_ped_ID(), (fTemp * fSpeed))
									//APPLY_FORCE_TO_CAR(NewCarID, APPLY_TYPE_FORCE, <<0.0, fTemp, 0.0>>, <<0.0, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
									//SET_CAR_FORWARD_SPEED(NewCarID, fTemp * fSpeed)

								ELSE
									bIsRecording = FALSE
								ENDIF

							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			// stop recording
			IF (bIsRecording)
			AND (bRecordingHasStarted)
				IF (bStopRecordingWhenOriginalStops)
					IF NOT IS_ENTITY_DEAD(GhostCarID)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_vehicle(GhostCarID)
							bIsRecording = FALSE		
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			// hijack recording
			IF (bIsRecording)
			AND (bRecordingHasStarted)
				IF (bNewCarUsesAI)
				OR (bNewCarUsesRecording)
					IF (bStartOnButtonPress)	
						IF DOES_ENTITY_EXIST(NewCarID)
							IF NOT IS_ENTITY_DEAD(NewCarID)
								 IF IS_PLAYBACK_GOING_ON_FOR_vehicle(NewCarID)
									IF IS_BUTTON_PRESSED(PAD1, RIGHTSHOULDER2)
										STOP_PLAYBACK_RECORDED_vehicle(NewCarID)
										PRINT_NOW("REC_HIJACKED", 3000, 1)
									ENDIF
								 ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			// hijack recording - to switch to ai
			IF NOT (bHasSwitchedToAI)
				IF (bIsRecording)
				AND (bRecordingHasStarted)
					IF (bNewCarUsesRecording)
						IF (bNewCarUsesAI)	
							IF DOES_ENTITY_EXIST(NewCarID)
								IF NOT IS_ENTITY_DEAD(NewCarID)
									 IF IS_PLAYBACK_GOING_ON_FOR_vehicle(NewCarID)
										SET_PLAYBACK_TO_USE_AI(NewCarID)
										bHasSwitchedToAI = TRUE
									 ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(NewCarID)
					IF NOT IS_ENTITY_DEAD(NewCarID)
					 	IF NOT IS_PLAYBACK_GOING_ON_FOR_vehicle(NewCarID)
							bHasSwitchedToAI = FALSE	
						ENDIF
					ELSE
						bHasSwitchedToAI = FALSE
					ENDIF
				ELSE
					bHasSwitchedToAI = FALSE
				ENDIF
			ENDIF

			// end recording
			IF DOES_ENTITY_EXIST(NewCarID)
				IF NOT IS_ENTITY_DEAD(NewCarID)
					IF IS_RECORDING_GOING_ON_FOR_vehicle(NewCarID)
						IF (bStopOnButtonPress)
							if is_control_pressed(player_control, input_jump) 
								bIsRecording = FALSE
							ENDIF
						ENDIF

						IF NOT (bIsRecording)
							STOP_RECORDING_vehicle(NewCarID)
							PRINT_NOW("CRECSTOP5", 3000, 1)
							bRecordingHasStarted = FALSE
						ENDIF

					ENDIF
				ENDIF
			ENDIF


		BREAK

		CASE STATE_RESET

			IF IS_PLAYER_PLAYING(PLAYER_ID())
				CLEAR_ped_TASKS_IMMEDIATELY(PLAYER_ped_ID())
			ENDIF
			
			// stop any existing recordings
			STOP_RECORDING_ALL_VEHICLES()

			// stop any existing playbacks and delete cars
			IF DOES_ENTITY_EXIST(GhostCarID)
				IF NOT IS_ENTITY_DEAD(GhostCarID)
					IF IS_PLAYBACK_GOING_ON_FOR_vehicle(GhostCarID)
						STOP_PLAYBACK_RECORDED_vehicle(GhostCarID)
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(GhostDriverID)
					DELETE_PED(GhostDriverID)
				ENDIF
				DELETE_VEHICLE(GhostCarID)
			ENDIF
			IF DOES_ENTITY_EXIST(NewCarID)
				IF NOT IS_ENTITY_DEAD(NewCarID)
					IF IS_PLAYBACK_GOING_ON_FOR_vehicle(NewCarID)
						STOP_PLAYBACK_RECORDED_vehicle(NewCarID)
					ENDIF
				ENDIF
				DELETE_VEHICLE(NewCarID)
			ENDIF

			// 1. check model name
			mReRecCar = INT_TO_ENUM(model_names, GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(TextInput)))
			IF NOT IS_MODEL_IN_CDIMAGE(mReRecCar)
				SCRIPT_ASSERT("Invalid Model Name")
				bResetToStart = FALSE
				iState = STATE_PROCESS
				BREAK
			ENDIF

			// 2. check the recording number
			IF NOT (iNewRecNo > 0)
			AND NOT (iNewRecNo <= 3000)
				SCRIPT_ASSERT("Invalid New Car Recording Number")
				bResetToStart = FALSE
				iState = STATE_PROCESS
				BREAK
			ENDIF 

			// 3. check existing recording number
			IF NOT (iReRecNo > 0)
			AND NOT (iReRecNo < 3000)
				SCRIPT_ASSERT("Invalid Old Car Recording Number")
				bResetToStart = FALSE
				iState = STATE_PROCESS
				BREAK
			ENDIF 
						
			// load car recording and model
			REQUEST_vehicle_RECORDING(iReRecNo, GET_CONTENTS_OF_TEXT_WIDGET(car_rec_input))
			REQUEST_MODEL(mReRecCar)
			REQUEST_MODEL(A_M_M_Business_01)
			WHILE NOT HAS_vehicle_RECORDING_BEEN_LOADED(iReRecNo, GET_CONTENTS_OF_TEXT_WIDGET(car_rec_input))
			   OR NOT HAS_MODEL_LOADED(mReRecCar)
			   OR NOT HAS_MODEL_LOADED(A_M_M_Business_01)
				WAIT(0)
			ENDWHILE

			// create car & ghost car and driver
			pos = GET_POSITION_OF_vehicle_RECORDING_AT_TIME(iReRecNo, 0.0, GET_CONTENTS_OF_TEXT_WIDGET(car_rec_input))
			GhostCarID = CREATE_vehicle(mReRecCar, pos)
		//	SET_VEHICLE_ALPHA(GhostCarID, 64)
			SET_ENTITY_COLLISION(GhostCarID, FALSE)
			GhostDriverID = CREATE_ped_INSIDE_vehicle(GhostCarID, PEDTYPE_CIVMALE, A_M_M_Business_01)
			SET_ENTITY_COLLISION(GhostDriverID, FALSE)
			//FREEZE_CAR_POSITION(GhostCarID, TRUE)
		//	SET_PED_ALPHA(GhostDriverID, 64)
			SET_ENTITY_VISIBLE(GhostCarID, FALSE)
			SET_ENTITY_VISIBLE(GhostDriverID, FALSE)
			bShowGhost = FALSE

			NewCarID = CREATE_vehicle(mReRecCar, pos)
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				set_ped_INTO_vehicle(PLAYER_ped_ID(), NewCarID)
			ENDIF

			// start car at recording
			WHILE NOT IS_car_PAUSED_AT_START_OF_RECORDING(GhostCarID, iReRecNo)
			   OR NOT IS_car_PAUSED_AT_START_OF_RECORDING(NewCarID, iReRecNo)
				WAIT(0)
			ENDWHILE


			// done
			bHasSwitchedToAI = FALSE
			bIsRecording = FALSE
			bRecordingHasStarted = FALSE
			bResetToStart = FALSE
			bReRecInitialized = TRUE
			iState = STATE_PROCESS

			IF (bSetReady)
				bShowGhost = TRUE
				bFreeCar = TRUE
				bShowPath = TRUE
				bStartOnButtonPress = TRUE
				bStopOnButtonPress = TRUE
				bNewCarUsesAI = FALSE
				bStopRecordingWhenOriginalStops = TRUE
				bSetReady = FALSE
			ENDIF

		BREAK

	ENDSWITCH	

	//Whenever the main car is playing a recording, store the playback time in debug
	IF NOT IS_ENTITY_DEAD(NewCarID)
		IF IS_PLAYBACK_GOING_ON_FOR_vehicle(NewCarID)
			fPlaybackTime = GET_TIME_POSITION_IN_RECORDING(NewCarID)
		ENDIF
	ENDIF
ENDPROC
 
#ENDIF
