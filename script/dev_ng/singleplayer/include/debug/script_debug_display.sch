//////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      				//
//      SCRIPT NAME     :   script_debug.sch                                   							//
//      AUTHOR          :   Michael Wadelin                                                				//
//      DESCRIPTION     :   Common functions for displaying debug information from the script			//
//                                                                                      				//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_team_info.sch"
USING "commands_misc.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"
USING "script_maths.sch"
USING "script_conversion.sch"


CONST_INT				I_DEBUG_DISPLAY_MAX_PAGES				10
CONST_INT				I_DEBUG_DISPLAY_MAX_ITEMS_TOTAL			50
CONST_INT				I_DEBUG_DISPLAY_MAX_ITEMS_SHOWN			30

CONST_FLOAT				F_DEBUG_DISPLY_X_INDENT					0.05
CONST_FLOAT				F_DEBUG_DISPLAY_Y_INDEX					0.05
CONST_FLOAT				F_DEBUG_DISPLAY_Y_SPACING				0.015


STRUCT DEBUG_DISPLAY
	BOOL				bShowDebug
	INT					iPageDisplayed
	INT					iItemScrollStart
	TEXT_LABEL_23		tlPageName[I_DEBUG_DISPLAY_MAX_PAGES]
	TEXT_LABEL_63		tlDisplayString[I_DEBUG_DISPLAY_MAX_PAGES][I_DEBUG_DISPLAY_MAX_ITEMS_TOTAL]
ENDSTRUCT


FUNC BOOL IS_SCRIPT_DEBUG_DISPLAY_ENABLED(DEBUG_DISPLAY &sDebugStruct)
	RETURN sDebugStruct.bShowDebug
ENDFUNC


FUNC INT DEBUG_DISPLAY_GET_PAGE_INDEX(DEBUG_DISPLAY &sDebugStruct, STRING strPage = NULL)

	// Page name is empty then use the first general page
	IF IS_STRING_NULL_OR_EMPTY( strPage )
	OR ARE_STRINGS_EQUAL( strPage, "General Debug" )
		RETURN 0
	ENDIF

	// Found an existing page to use
	INT i
	REPEAT I_DEBUG_DISPLAY_MAX_PAGES i
		IF NOT IS_STRING_NULL_OR_EMPTY( strPage )
		AND i != 0
			IF ARE_STRINGS_EQUAL( strPage, sDebugStruct.tlPageName[i] )
				RETURN i
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Found blank page
	REPEAT I_DEBUG_DISPLAY_MAX_PAGES i
		IF IS_STRING_NULL_OR_EMPTY( sDebugStruct.tlPageName[i] )
		AND i != 0
			RETURN i
		ENDIF
	ENDREPEAT

	RETURN 0
ENDFUNC


FUNC INT DEBUG_DISPLAY_GET_ITEM_INDEX(DEBUG_DISPLAY &sDebugStruct, INT iPage)

	INT i
	REPEAT I_DEBUG_DISPLAY_MAX_ITEMS_TOTAL i
		// Found empty 
		IF IS_STRING_NULL_OR_EMPTY( sDebugStruct.tlDisplayString[iPage][i] )
			RETURN i
		ENDIF
	ENDREPEAT

	RETURN -1
ENDFUNC


FUNC BOOL DEBUG_DISPLAY_ADD_ITEM_TO_LIST(DEBUG_DISPLAY &sDebugStruct, STRING strDebugString, STRING strPage = NULL)
	
	INT iPage = DEBUG_DISPLAY_GET_PAGE_INDEX( sDebugStruct, strPage )
	INT iItem = DEBUG_DISPLAY_GET_ITEM_INDEX( sDebugStruct, iPage )
	
	// Out of range or failed to find a spare slot
	IF iPage = -1 OR iPage >= I_DEBUG_DISPLAY_MAX_PAGES
	OR iItem = -1 OR iItem >= I_DEBUG_DISPLAY_MAX_ITEMS_TOTAL
		RETURN FALSE
	ENDIF
	
	// Page name not stored, store now
	IF IS_STRING_NULL_OR_EMPTY( sDebugStruct.tlPageName[iPage] )
		sDebugStruct.tlPageName[iPage] = strPage
	ENDIF
	
	// Store the item in the list
	IF IS_STRING_NULL_OR_EMPTY( sDebugStruct.tlDisplayString[iPage][iItem] )
		sDebugStruct.tlDisplayString[iPage][iItem] = strDebugString
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


FUNC BOOL DEBUG_DISPLAY_INT_THIS_FRAME(DEBUG_DISPLAY &sDebugStruct, STRING strValueName, INT iValue, STRING strPage = NULL)

	TEXT_LABEL_63 tlDebugString 	= strValueName
	tlDebugString					+= ": "
	tlDebugString					+= iValue
	
	RETURN DEBUG_DISPLAY_ADD_ITEM_TO_LIST( sDebugStruct, tlDebugString , strPage)
ENDFUNC


FUNC BOOL DEBUG_DISPLAY_TIMER_THIS_FRAME(DEBUG_DISPLAY &sDebugStruct, STRING strValueName, INT iValue, STRING strPage = NULL)

	TEXT_LABEL_63 tlDebugString 	= strValueName
	tlDebugString					+= ": "
	IF iValue = -1
		tlDebugString				+= iValue
	ELSE
		tlDebugString				+= GET_GAME_TIMER() - iValue
	ENDIF
	
	RETURN DEBUG_DISPLAY_ADD_ITEM_TO_LIST( sDebugStruct, tlDebugString , strPage)
ENDFUNC


FUNC BOOL DEBUG_DISPLAY_STRING_THIS_FRAME(DEBUG_DISPLAY &sDebugStruct, STRING strValue, STRING strPage = NULL)
	RETURN DEBUG_DISPLAY_ADD_ITEM_TO_LIST( sDebugStruct, strValue, strPage)
ENDFUNC


FUNC BOOL DEBUG_DISPLAY_STRING_VAR_THIS_FRAME(DEBUG_DISPLAY &sDebugStruct, STRING strValueName, STRING strValue, STRING strPage = NULL)

	TEXT_LABEL_63 tlDebugString 	= strValueName
	tlDebugString					+= ": "
	tlDebugString					+= strValue

	RETURN DEBUG_DISPLAY_ADD_ITEM_TO_LIST( sDebugStruct, tlDebugString, strPage)
ENDFUNC


FUNC BOOL DEBUG_DISPLAY_BOOL_THIS_FRAME(DEBUG_DISPLAY &sDebugStruct, STRING strValueName, BOOL bValue, STRING strPage = NULL)

	TEXT_LABEL_63 tlDebugString 	= strValueName
	tlDebugString					+= ": "
	IF bValue
		tlDebugString				+= "TRUE"
	ELSE
		tlDebugString				+= "FALSE"
	ENDIF
	
	RETURN DEBUG_DISPLAY_ADD_ITEM_TO_LIST( sDebugStruct, tlDebugString, strPage )
ENDFUNC


FUNC BOOL DEBUG_DISPLAY_SPACE_THIS_FRAME(DEBUG_DISPLAY &sDebugStruct, STRING strPage = NULL)
	
	IF DEBUG_DISPLAY_ADD_ITEM_TO_LIST( sDebugStruct, "*SPACE*", strPage)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


FUNC BOOL DEBUG_DISPLAY_FLOAT_THIS_FRAME(DEBUG_DISPLAY &sDebugStruct, STRING strValueName, FLOAT fValue, STRING strPage = NULL, INT iPrecision = DEFAULT_FLOAT_TO_STRING_PRECISION, FORMAT_FLOAT_TO_STRING eFormat = FORMATFLOATTOSTRING_NORMAL )

	TEXT_LABEL_63 tlDebugString 	= strValueName
	tlDebugString					+= ": "
	tlDebugString					+= FLOAT_TO_STRING(fValue, iPrecision, eFormat)

	RETURN DEBUG_DISPLAY_ADD_ITEM_TO_LIST( sDebugStruct, tlDebugString , strPage)
ENDFUNC


FUNC BOOL DEBUG_DISPLAY_VECTOR_THIS_FRAME(DEBUG_DISPLAY &sDebugStruct, STRING strValueName, VECTOR vValue, STRING strPage = NULL, INT iPrecision = DEFAULT_FLOAT_TO_STRING_PRECISION, FORMAT_VECTOR_TO_STRING eFormat = FORMATVECTORTOSTRING_AS_SEEN_IN_SCRIPT )

	TEXT_LABEL_63 tlDebugString 	= strValueName
	tlDebugString					+= ": "
	tlDebugString					+= VECTOR_TO_STRING(vValue, iPrecision, eFormat)

	RETURN DEBUG_DISPLAY_ADD_ITEM_TO_LIST( sDebugStruct, tlDebugString , strPage)
ENDFUNC


PROC DEBUG_DISPLAY_UPDATE(DEBUG_DISPLAY &sDebugStruct)

	CONST_INT		I_TEXT_COLOUR_R		255
	CONST_INT		I_TEXT_COLOUR_G		255
	CONST_INT		I_TEXT_COLOUR_B		0
	CONST_INT		I_TEXT_COLOUR_A		255

	// Toggle debug display on/off
	IF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_HOME )
		sDebugStruct.bShowDebug = !sDebugStruct.bShowDebug
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE( sDebugStruct.bShowDebug )
	ENDIF
	
	IF sDebugStruct.bShowDebug
	
		// Count number of pages
		INT i, iNumOfPages
		REPEAT I_DEBUG_DISPLAY_MAX_PAGES i
			IF NOT IS_STRING_NULL_OR_EMPTY( sDebugStruct.tlPageName[i] )
			OR i = 0
				iNumOfPages++
			ENDIF
		ENDREPEAT
		
		// next page
		IF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_END )
			
			sDebugStruct.iPageDisplayed++
			IF sDebugStruct.iPageDisplayed >= iNumOfPages
				sDebugStruct.iPageDisplayed = 0
			ENDIF
			
			sDebugStruct.iItemScrollStart = 0
			
		// previous page
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_DELETE )
			
			sDebugStruct.iPageDisplayed--
			IF sDebugStruct.iPageDisplayed < 0
				sDebugStruct.iPageDisplayed = iNumOfPages-1
			ENDIF
			
			sDebugStruct.iItemScrollStart = 0
		
		ENDIF
		
		// Count number of items on current page
		INT iNumOfItemsOnPage
		REPEAT I_DEBUG_DISPLAY_MAX_ITEMS_TOTAL i
			IF NOT IS_STRING_NULL_OR_EMPTY( sDebugStruct.tlDisplayString[sDebugStruct.iPageDisplayed][i] )
				iNumOfItemsOnPage++
			ENDIF
		ENDREPEAT
		
		// Scroll down
		IF iNumOfItemsOnPage > I_DEBUG_DISPLAY_MAX_ITEMS_SHOWN
			IF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_DOWN )
			
				IF sDebugStruct.iItemScrollStart < iNumOfItemsOnPage
					sDebugStruct.iItemScrollStart++
				ENDIF
				
			// Scroll up
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_UP )
			
				IF sDebugStruct.iItemScrollStart > 0
					sDebugStruct.iItemScrollStart--
				ENDIF
			
			ENDIF
		ENDIF

	
		// Rendering
			
		INT iDrawnCount
		
		i = sDebugStruct.iItemScrollStart
		
		WHILE i < I_DEBUG_DISPLAY_MAX_ITEMS_TOTAL
		AND iDrawnCount < I_DEBUG_DISPLAY_MAX_ITEMS_SHOWN

			IF NOT IS_STRING_NULL_OR_EMPTY( sDebugStruct.tlDisplayString[sDebugStruct.iPageDisplayed][i] )
			
				TEXT_LABEL_63 tlStringToDisplay
				
				IF ARE_STRINGS_EQUAL( "*DIVIDER*", sDebugStruct.tlDisplayString[sDebugStruct.iPageDisplayed][i] )
				
					tlStringToDisplay = "-------------------------------------------------------------------------"

				ELIF ARE_STRINGS_EQUAL( "*SPACE*", sDebugStruct.tlDisplayString[sDebugStruct.iPageDisplayed][i] )
				
					tlStringToDisplay = ""
				
				ELSE
				
					tlStringToDisplay = sDebugStruct.tlDisplayString[sDebugStruct.iPageDisplayed][i]
					
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(tlStringToDisplay)
				
					DRAW_DEBUG_TEXT_2D( tlStringToDisplay, 
							<< F_DEBUG_DISPLY_X_INDENT, F_DEBUG_DISPLAY_Y_INDEX + ( F_DEBUG_DISPLAY_Y_SPACING * ( iDrawnCount+4 ) ), 0.0>>,
							I_TEXT_COLOUR_R,I_TEXT_COLOUR_G,I_TEXT_COLOUR_B,I_TEXT_COLOUR_A )
						
				ENDIF
				
				iDrawnCount++
			ENDIF
			
			i++
			
		ENDWHILE
		
		TEXT_LABEL_31 tlPageTitle
		IF sDebugStruct.iPageDisplayed = 0
			
			tlPageTitle = "General Debug"
		
		ELIF NOT IS_STRING_NULL_OR_EMPTY( sDebugStruct.tlPageName[sDebugStruct.iPageDisplayed] )
		
			tlPageTitle = sDebugStruct.tlPageName[sDebugStruct.iPageDisplayed]

		ENDIF
		
		tlPageTitle += " ("
		tlPageTitle += sDebugStruct.iPageDisplayed
		tlPageTitle += ")"
		
		DRAW_DEBUG_TEXT_2D( "-------------------------------------------------------------------------", 
				<< F_DEBUG_DISPLY_X_INDENT, F_DEBUG_DISPLAY_Y_INDEX, 0.0>>,
				I_TEXT_COLOUR_R,I_TEXT_COLOUR_G,I_TEXT_COLOUR_B,I_TEXT_COLOUR_A )
		
		DRAW_DEBUG_TEXT_2D( tlPageTitle, 
				<< F_DEBUG_DISPLY_X_INDENT, F_DEBUG_DISPLAY_Y_INDEX + F_DEBUG_DISPLAY_Y_SPACING, 0.0>>,
				I_TEXT_COLOUR_R,I_TEXT_COLOUR_G,I_TEXT_COLOUR_B,I_TEXT_COLOUR_A )
				
		DRAW_DEBUG_TEXT_2D( "-------------------------------------------------------------------------", 
				<< F_DEBUG_DISPLY_X_INDENT, F_DEBUG_DISPLAY_Y_INDEX + (F_DEBUG_DISPLAY_Y_SPACING * 2), 0.0>>,
				I_TEXT_COLOUR_R,I_TEXT_COLOUR_G,I_TEXT_COLOUR_B,I_TEXT_COLOUR_A )
		
		
		
		// Reset for next frame
		
		INT j
		REPEAT I_DEBUG_DISPLAY_MAX_PAGES i
			sDebugStruct.tlPageName[i] = ""
			REPEAT I_DEBUG_DISPLAY_MAX_ITEMS_TOTAL j
				sDebugStruct.tlDisplayString[i][j] = ""
			ENDREPEAT
		ENDREPEAT

	ENDIF

ENDPROC

