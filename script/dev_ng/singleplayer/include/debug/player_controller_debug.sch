#IF IS_DEBUG_BUILD

USING "player_scene_anim.sch"

//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	player_ped_debug.sch										//
//		AUTHOR			:	Alwyn Roberts												//
//		DESCRIPTION		:	Handles all the player character related debug procs.		//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

FUNC BOOL Test_Player_Ped_Scene(PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene,
		MODEL_NAMES vehModel, WIDGET_GROUP_ID sceneWidget, VECTOR vVehCoordsOffset, FLOAT fVehHeadOffset, 
		VECTOR vSceneVeh_driveOffset,
		VECTOR vecCamEndPos, VECTOR vecCamEndRot,
		OBJECT_INDEX &playerProp, OBJECT_INDEX &playerExtraProp,
		OBJECT_INDEX &player_door_l, OBJECT_INDEX &player_door_r,
		VEHICLE_INDEX &scene_veh)
	
	g_bDrawLiteralSceneString = TRUE
	g_eRecentlySelectedScene = sPassedScene.sScene.eScene
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	Force_All_Drunk_Peds_To_Become_Sober_And_Quit_Camera()
	
	BOOL bRender_scene_cam = TRUE
	CAMERA_INDEX scene_cam
	scene_cam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",
			vecCamEndPos, vecCamEndRot,
			GET_GAMEPLAY_CAM_FOV()) 
	SET_CAM_ACTIVE(scene_cam, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
	MODEL_NAMES ePropModel = DUMMY_MODEL_FOR_SCRIPT, eExtraPropModel = DUMMY_MODEL_FOR_SCRIPT
	VECTOR propOffset = <<0,0,0>>, propRotation = <<0,0,0>>
	VECTOR extraPropOffset = <<0,0,0>>, extraPropRotation = <<0,0,0>>
	PED_BONETAG epropBonetag = BONETAG_NULL, eExtraPropBonetag = BONETAG_NULL
	FLOAT fDetachAnimPhase = -1.0
	BOOL bAttachedToBuddy = FALSE
	enumPlayerSceneObjectAction thisSceneObjectAction = PSOA_NULL
	GET_OBJECTS_FOR_SCENE(sPassedScene.sScene.eScene, ePropModel, propOffset, propRotation, epropBonetag, fDetachAnimPhase, thisSceneObjectAction)
	GET_EXTRA_OBJECTS_FOR_SCENE(sPassedScene.sScene.eScene, eExtraPropModel, extraPropOffset, extraPropRotation, eExtraPropBonetag, fDetachAnimPhase, thisSceneObjectAction, bAttachedToBuddy)
	
	MODEL_NAMES eDoorModel_l = DUMMY_MODEL_FOR_SCRIPT, eDoorModel_R = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vDoorOffset_L = <<0,0,0>>, vDoorOffset_R= <<0,0,0>>
	DOOR_HASH_ENUM eDoorHash_L, eDoorHash_R
	BOOL bReplaceDoor
	FLOAT fHideDoorRadius
	GET_DOORS_FOR_SCENE(sPassedScene.sScene.eScene, eDoorModel_l, eDoorModel_R, vDoorOffset_L, vDoorOffset_R, eDoorHash_L, eDoorHash_R, bReplaceDoor, fHideDoorRadius)
	
	BOOL bVehOffsetFromPlayerGivenWorldCoords, bVehOffsetFromPlayerInWorldCoords
	BOOL bVehRotationProperly, bDrawVehRotationProperly
	
	TEXT_LABEL_63 tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut
	TEXT_LABEL_63 tSceneBuddyAnimDict, tSceneBuddyAnimLoop, tSceneBuddyAnimOut
	
	ANIMATION_FLAGS playerLoopFlag, playerOutFlag
	
	BOOL bWaitForBuddyAssets = FALSE
	
	VECTOR vCreateCoords = sPassedScene.sScene.vCreateCoords, vStoredCoords
	FLOAT fCreateHead = sPassedScene.sScene.fCreateHead, fStoredHead
	
	VECTOR vCreateRot
	GET_PLAYER_PED_ROTATION_FOR_SCENE(sPassedScene.sScene.eScene, fCreateHead, vCreateRot)
	
	VECTOR vCamEndOffset = vecCamEndPos - vCreateCoords
	
	FLOAT gpCamPitch
	FLOAT gpCamHead
	
	TEXT_LABEL tRecording_name
	INT iRecording_num
	FLOAT fRecording_start, fRecording_skip, fRecording_stop, fSpeed_switch, fSpeed_exit
	
	BOOL /*bDisableLegIk,*/ bOverridePhysics, bTurnOffCollision, bExtractInitialOffset, bNotInterruptable, bLooping
	BOOL bExitSynchScene
//	BOOL bGetPlaceholderCoordPostMission
	WHILE NOT bWaitForBuddyAssets
		bWaitForBuddyAssets = TRUE
		
		IF (sPassedScene.eLoopTask = script_task_play_anim)
		OR (sPassedScene.eOutTask = script_task_play_anim)
		
		OR (sPassedScene.eLoopTask = script_task_synchronized_scene)
		OR (sPassedScene.eOutTask = script_task_synchronized_scene)
			
			//enumPlayerSceneAnimProgress ePlayerSceneAnimProgress = MAX_PLAYER_SCENE_ANIM_PROGRESS
			IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sPassedScene.sScene.eScene,
					tPlayerAnimDict, tPlayerAnimLoop, tPlayerAnimOut,
					playerLoopFlag, playerOutFlag)	//, ePlayerSceneAnimProgress)
				
//				bDisableLegIk = IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag,			AF_DISABLE_LEG_IK)
				bOverridePhysics = IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag,		AF_OVERRIDE_PHYSICS)
				bTurnOffCollision = IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag,		AF_TURN_OFF_COLLISION)
				bExtractInitialOffset = IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag,	AF_EXTRACT_INITIAL_OFFSET)
				bNotInterruptable = IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag, 	AF_NOT_INTERRUPTABLE)
				bLooping = IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag,				AF_LOOPING)
				
				IF NOT (IS_STRING_NULL(tPlayerAnimDict) OR ARE_STRINGS_EQUAL(tPlayerAnimDict, ""))
					REQUEST_ANIM_DICT(tPlayerAnimDict)
					IF NOT HAS_ANIM_DICT_LOADED(tPlayerAnimDict)
						bWaitForBuddyAssets = FALSE
						REQUEST_ANIM_DICT(tPlayerAnimDict)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF sPassedScene.eSceneBuddy <> NO_CHARACTER
			
			IF (sPassedScene.eBuddyLoopTask = script_task_play_anim)
			OR (sPassedScene.eBuddyOutTask = script_task_play_anim)
			
			OR (sPassedScene.eBuddyLoopTask = script_task_synchronized_scene)
			OR (sPassedScene.eBuddyOutTask = script_task_synchronized_scene)
				
				ANIMATION_FLAGS buddyLoopFlag = AF_DEFAULT, buddyOutFlag = AF_DEFAULT
				IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(sPassedScene.sScene.eScene,
						tSceneBuddyAnimDict, tSceneBuddyAnimLoop, tSceneBuddyAnimOut,
						buddyLoopFlag, buddyOutFlag)
					IF NOT (IS_STRING_NULL(tSceneBuddyAnimDict) OR ARE_STRINGS_EQUAL(tSceneBuddyAnimDict, ""))
						REQUEST_ANIM_DICT(tSceneBuddyAnimDict)
						IF NOT HAS_ANIM_DICT_LOADED(tSceneBuddyAnimDict)
							bWaitForBuddyAssets = FALSE
							REQUEST_ANIM_DICT(tSceneBuddyAnimDict)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(g_pScene_buddy)
				
				IF (sPassedScene.eSceneBuddy <> CHAR_BLANK_ENTRY)
					IF NOT CREATE_NPC_PED_ON_FOOT(g_pScene_buddy, sPassedScene.eSceneBuddy,
							sPassedScene.sScene.vCreateCoords+sPassedScene.vSceneBuddyCoordOffset,
							sPassedScene.sScene.fCreateHead+sPassedScene.fSceneBuddyHeadOffset)
						bWaitForBuddyAssets = FALSE
					ENDIF
				ELSE
					
					PED_TYPE PedType = PEDTYPE_MISSION
					MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
					SETUP_BLANK_BUDDY_FOR_SCENE(sPassedScene.sScene.eScene, PedType, model)
					
					bWaitForBuddyAssets = FALSE
					
					IF (model = GET_CHOP_MODEL())
						IF CREATE_CHOP(g_pScene_buddy,
								sPassedScene.sScene.vCreateCoords+sPassedScene.vSceneBuddyCoordOffset,
								sPassedScene.sScene.fCreateHead+sPassedScene.fSceneBuddyHeadOffset)
						ENDIF
						
					ELSE
						REQUEST_MODEL(model)
				        IF HAS_MODEL_LOADED(model)
				            g_pScene_buddy = CREATE_PED(PedType, model,
									sPassedScene.sScene.vCreateCoords+sPassedScene.vSceneBuddyCoordOffset,
									sPassedScene.sScene.fCreateHead+sPassedScene.fSceneBuddyHeadOffset,
									FALSE, FALSE)
				            
							SET_MODEL_AS_NO_LONGER_NEEDED(model)
				        ENDIF
					ENDIF
					
				ENDIF
			ELSE
				IF NOT IS_ENTITY_VISIBLE(g_pScene_buddy)
					SET_ENTITY_VISIBLE(g_pScene_buddy, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF sPassedScene.eVehState <> PTVS_0_noVehicle
			VECTOR vVehCoords = sPassedScene.sScene.vCreateCoords+vVehCoordsOffset
			FLOAT fVehHead = sPassedScene.sScene.fCreateHead+fVehHeadOffset
			
			enumCharacterList paramPlayerCharID = GET_CURRENT_PLAYER_PED_ENUM()
			IF DOES_ENTITY_EXIST(g_vPlayerVeh[paramPlayerCharID])
				IF NOT DOES_ENTITY_EXIST(scene_veh)
					IF GET_ENTITY_MODEL(g_vPlayerVeh[paramPlayerCharID]) = vehModel
						scene_veh = g_vPlayerVeh[paramPlayerCharID]
						
						IF NOT IS_ENTITY_DEAD(scene_veh)
							SET_ENTITY_COORDS(scene_veh, vVehCoords)
							SET_ENTITY_HEADING(scene_veh, fVehHead)
							
							SET_VEHICLE_ON_GROUND_PROPERLY(scene_veh)
							FREEZE_ENTITY_POSITION(scene_veh, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(scene_veh)
				bWaitForBuddyAssets = FALSE
				
				REQUEST_MODEL(vehModel)
				IF HAS_MODEL_LOADED(vehModel)
					scene_veh = CREATE_VEHICLE(vehModel,
							vVehCoords, fVehHead,
							FALSE, FALSE)
					SET_VEHICLE_ON_GROUND_PROPERLY(scene_veh)
					SET_MODEL_AS_NO_LONGER_NEEDED(vehModel)
					FREEZE_ENTITY_POSITION(scene_veh, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF ePropModel <> DUMMY_MODEL_FOR_SCRIPT
			IF NOT DOES_ENTITY_EXIST(playerProp)
				bWaitForBuddyAssets = FALSE
				
				REQUEST_MODEL(ePropModel)
				IF HAS_MODEL_LOADED(ePropModel)
					playerProp = CREATE_OBJECT(ePropModel,
							vCreateCoords+propOffset)
							
					IF (epropBonetag <> BONETAG_NULL)
						SET_ENTITY_RECORDS_COLLISIONS(playerProp, TRUE)
						FREEZE_ENTITY_POSITION(playerProp, FALSE)
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							ATTACH_ENTITY_TO_ENTITY(playerProp, PLAYER_PED_ID(),
									GET_PED_BONE_INDEX(PLAYER_PED_ID(), epropBonetag),
									propOffset, propRotation)
						ENDIF
						
					ELSE
						VECTOR vObjectCoords = sPassedScene.sScene.vCreateCoords
						FLOAT fObjectHead = sPassedScene.sScene.fCreateHead
						
						SET_ENTITY_COORDS(playerProp, vObjectCoords+propOffset)
						SET_ENTITY_ROTATION(playerProp, <<0,0,fObjectHead>>+propRotation)
						
						FREEZE_ENTITY_POSITION(playerProp, TRUE)
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		
		IF eExtraPropModel <> DUMMY_MODEL_FOR_SCRIPT
			IF NOT DOES_ENTITY_EXIST(playerExtraProp)
				bWaitForBuddyAssets = FALSE
				
				REQUEST_MODEL(eExtraPropModel)
				IF HAS_MODEL_LOADED(eExtraPropModel)
					
					IF bAttachedToBuddy
						IF DOES_ENTITY_EXIST(g_pScene_buddy)
							playerExtraProp = CREATE_OBJECT(eExtraPropModel,
									vCreateCoords+ExtraPropOffset)
						ENDIF
					ELSE
						playerExtraProp = CREATE_OBJECT(eExtraPropModel,
								vCreateCoords+ExtraPropOffset)
					ENDIF
					
					IF DOES_ENTITY_EXIST(playerExtraProp)
						IF (eExtraPropBonetag <> BONETAG_NULL)
							SET_ENTITY_RECORDS_COLLISIONS(playerExtraProp, TRUE)
							FREEZE_ENTITY_POSITION(playerExtraProp, FALSE)
							
							IF NOT bAttachedToBuddy
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									ATTACH_ENTITY_TO_ENTITY(playerExtraProp, PLAYER_PED_ID(),
											GET_PED_BONE_INDEX(PLAYER_PED_ID(), eExtraPropBonetag),
											ExtraPropOffset, ExtraPropRotation)
								ENDIF
							ELSE
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									ATTACH_ENTITY_TO_ENTITY(playerExtraProp, g_pScene_buddy,
											GET_PED_BONE_INDEX(g_pScene_buddy, eExtraPropBonetag),
											ExtraPropOffset, ExtraPropRotation)
								ENDIF
							ENDIF
						ELSE
							VECTOR vObjectCoords = sPassedScene.sScene.vCreateCoords
							FLOAT fObjectHead = sPassedScene.sScene.fCreateHead
							
							SET_ENTITY_COORDS(playerExtraProp, vObjectCoords+ExtraPropOffset)
							SET_ENTITY_ROTATION(playerExtraProp, <<0,0,fObjectHead>>+ExtraPropRotation)
							
							FREEZE_ENTITY_POSITION(playerExtraProp, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF eDoorModel_l <> DUMMY_MODEL_FOR_SCRIPT
			IF NOT DOES_ENTITY_EXIST(player_door_l)
				bWaitForBuddyAssets = FALSE
				REQUEST_MODEL(eDoorModel_l)
				IF NOT HAS_MODEL_LOADED(eDoorModel_l)
					REQUEST_MODEL(eDoorModel_l)
				ELSE
					player_door_l = CREATE_OBJECT(eDoorModel_l, sPassedScene.sScene.vCreateCoords)
					SET_MODEL_AS_NO_LONGER_NEEDED(eDoorModel_l)
				ENDIF
			ENDIF
		ENDIF
		IF eDoorModel_r <> DUMMY_MODEL_FOR_SCRIPT
			IF NOT DOES_ENTITY_EXIST(player_door_r)
				bWaitForBuddyAssets = FALSE
				REQUEST_MODEL(eDoorModel_r)
				IF NOT HAS_MODEL_LOADED(eDoorModel_r)
					REQUEST_MODEL(eDoorModel_r)
				ELSE
					player_door_r = CREATE_OBJECT(eDoorModel_r, sPassedScene.sScene.vCreateCoords)
					SET_MODEL_AS_NO_LONGER_NEEDED(eDoorModel_r)
				ENDIF
			ENDIF
		ENDIF
		
		
		TEXT_LABEL_31 sPlayerTimetableAdditional_script = ""
		IF SETUP_TIMETABLE_SCRIPT_FOR_SCENE(sPassedScene.sScene.eScene, sPlayerTimetableAdditional_script)
			
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(sPlayerTimetableAdditional_script)) <= 0
				
				bWaitForBuddyAssets = FALSE
				
				CONST_INT iSCENE_STACK_SIZE FRIEND_STACK_SIZE	//bug 381398
				REQUEST_SCRIPT(sPlayerTimetableAdditional_script)
				IF NOT HAS_SCRIPT_LOADED(sPlayerTimetableAdditional_script)
					//
				ELSE
					START_NEW_SCRIPT(sPlayerTimetableAdditional_script,
							iSCENE_STACK_SIZE)
					SET_SCRIPT_AS_NO_LONGER_NEEDED(sPlayerTimetableAdditional_script)
				ENDIF
			ENDIF
			
		ENDIF
		
		
		PRINTSTRING("bWaitForBuddyAssets...")PRINTNL()
		WAIT(0)
	ENDWHILE
	
	
	
	TEXT_LABEL_63 str = "Test_Player_Ped_Scene - "
	str += Get_String_From_Ped_Request_Scene_Enum(sPassedScene.sScene.eScene)
	SET_CURRENT_WIDGET_GROUP(sceneWidget)
	WIDGET_GROUP_ID testPlayerPedScene_widget = START_WIDGET_GROUP(str)
		FLOAT fXY_slider = 0.01
		IF (sPassedScene.sScene.eScene = PR_SCENE_F0_TANISHAFIGHT)
			fXY_slider = 0.0001
		ENDIF
		ADD_WIDGET_FLOAT_SLIDER("vCreateCoords x", vCreateCoords.x, vCreateCoords.x-100.0, vCreateCoords.x+100.0, fXY_slider)
		ADD_WIDGET_FLOAT_SLIDER("vCreateCoords y", vCreateCoords.y, vCreateCoords.y-100.0, vCreateCoords.y+100.0, fXY_slider)
		ADD_WIDGET_FLOAT_SLIDER("vCreateCoords z", vCreateCoords.z, vCreateCoords.z-10.0, vCreateCoords.z+10.0, 0.0001)
		
		IF NOT GET_PLAYER_PED_ROTATION_FOR_SCENE(sPassedScene.sScene.eScene, fCreateHead, vCreateRot)
			
			vCreateRot.z = fCreateHead
			
			IF vCreateRot.z > 180.0
				vCreateRot.z -= 360.0
			ENDIF
			IF vCreateRot.z < -180.0
				vCreateRot.z += 360.0
			ENDIF
			ADD_WIDGET_FLOAT_SLIDER("fCreateHead", vCreateRot.z, -180.0, 180.0, 1.0)
		ELSE
			
			IF vCreateRot.z > 180.0
				vCreateRot.z -= 360.0
			ENDIF
			IF vCreateRot.z < -180.0
				vCreateRot.z += 360.0
			ENDIF
			ADD_WIDGET_STRING("GET_PLAYER_PED_ROTATION_FOR_SCENE()")
			ADD_WIDGET_VECTOR_SLIDER("vCreateRot", vCreateRot, -180.0, 180.0, 0.01)
		ENDIF
		
//		IF NOT ARE_VECTORS_ALMOST_EQUAL(sPassedScene.vPlayerCoordOffset, <<0,0,0>>)
//		OR NOT (sPassedScene.fPlayerHeadOffset = 0)
////			ADD_WIDGET_STRING("scene_player_offset")
////			ADD_WIDGET_VECTOR_SLIDER("vPlayerCoordOffset", sPassedScene.vPlayerCoordOffset, -40.0, 40.0, 0.01)
////			ADD_WIDGET_FLOAT_SLIDER("fPlayerHeadOffset", sPassedScene.fPlayerHeadOffset, -180.0, 181.0, 1.0)
//			
//			
//			vCreateCoords += sPassedScene.vPlayerCoordOffset
//			vCamEndOffset -= sPassedScene.vPlayerCoordOffset
//			vVehCoordsOffset -= sPassedScene.vPlayerCoordOffset
//			sPassedScene.vPlayerCoordOffset = <<0,0,0>>
//			
//			fCreateHead += sPassedScene.fPlayerHeadOffset
//			fVehHeadOffset -= sPassedScene.fPlayerHeadOffset
//			sPassedScene.fPlayerHeadOffset = 0
//			
//		ENDIF
		
		

		IF (sPassedScene.eLoopTask = script_task_play_anim)
		OR (sPassedScene.eOutTask = script_task_play_anim)
		
		OR (sPassedScene.eLoopTask = script_task_synchronized_scene)
		OR (sPassedScene.eOutTask = script_task_synchronized_scene)
			
			START_WIDGET_GROUP("loop anim flags")
			
//				ADD_WIDGET_BOOL("bDisableLegIk", bDisableLegIk)
				ADD_WIDGET_BOOL("bOverridePhysics", bOverridePhysics)
				ADD_WIDGET_BOOL("bTurnOffCollision", bTurnOffCollision)
				ADD_WIDGET_BOOL("bExtractInitialOffset", bExtractInitialOffset)
				ADD_WIDGET_BOOL("bNotInterruptable", bNotInterruptable)
				ADD_WIDGET_BOOL("bLooping", bLooping)

			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_BOOL("bExitSynchScene", bExitSynchScene)
		ENDIF
		
//		VECTOR vCoordFrom, vCoordTo, vLastKnownCoords
//		FLOAT fPercentDist, fLastKnownHead
//		IF GetPlaceholderCoordPostMission(sPassedScene.sScene.eScene, vCoordFrom, vCoordTo, fPercentDist,
//				vLastKnownCoords, fLastKnownHead)
//			ADD_WIDGET_BOOL("bGetPlaceholderCoordPostMission", bGetPlaceholderCoordPostMission)
//		ENDIF
		
		IF DOES_ENTITY_EXIST(g_pScene_buddy)
			ADD_WIDGET_STRING("g_pScene_buddy")
			FLOAT fSceneBuddyCoordOffsetMag
			fSceneBuddyCoordOffsetMag = VMAG(sPassedScene.vSceneBuddyCoordOffset)
			
			IF (fSceneBuddyCoordOffsetMag < ABSF(sPassedScene.vSceneBuddyCoordOffset.x))
				fSceneBuddyCoordOffsetMag = ABSF(sPassedScene.vSceneBuddyCoordOffset.x)
			ENDIF
			IF (fSceneBuddyCoordOffsetMag < ABSF(sPassedScene.vSceneBuddyCoordOffset.y))
				fSceneBuddyCoordOffsetMag = ABSF(sPassedScene.vSceneBuddyCoordOffset.y)
			ENDIF
			IF (fSceneBuddyCoordOffsetMag < ABSF(sPassedScene.vSceneBuddyCoordOffset.z))
				fSceneBuddyCoordOffsetMag = ABSF(sPassedScene.vSceneBuddyCoordOffset.z)
			ENDIF
			
			fSceneBuddyCoordOffsetMag *= 2
			IF (fSceneBuddyCoordOffsetMag < 20.0)
				fSceneBuddyCoordOffsetMag = 20.0
			ENDIF
			
			ADD_WIDGET_VECTOR_SLIDER("vSceneBuddyCoordOffset", sPassedScene.vSceneBuddyCoordOffset,
				-fSceneBuddyCoordOffsetMag, fSceneBuddyCoordOffsetMag, fSceneBuddyCoordOffsetMag * 0.0005)
			ADD_WIDGET_FLOAT_SLIDER("fSceneBuddyHeadOffset", sPassedScene.fSceneBuddyHeadOffset,
				-180.0, 180.0, 1.0)
		ENDIF
		IF DOES_ENTITY_EXIST(scene_veh)
			ADD_WIDGET_STRING("scene_veh")
			
			IF (sPassedScene.eVehState = PTVS_1_playerWithVehicle)
				ADD_WIDGET_VECTOR_SLIDER("vVehCoordsOffset", vVehCoordsOffset, -100.0, 100.0, 0.01)
				IF fVehHeadOffset < -180.0
					fVehHeadOffset += 360.0
				ELIF fVehHeadOffset > 180.0
					fVehHeadOffset -= 360.0
				ENDIF
				ADD_WIDGET_FLOAT_SLIDER("fVehHeadOffset", fVehHeadOffset, -180.0, 180.0, 1.0)
				
				ADD_WIDGET_BOOL("bVehOffsetFromPlayerGivenWorldCoords", bVehOffsetFromPlayerGivenWorldCoords)
				ADD_WIDGET_BOOL("bVehOffsetFromPlayerInWorldCoords", bVehOffsetFromPlayerInWorldCoords)
				
			ELIF (sPassedScene.eVehState = PTVS_2_playerInVehicle)
				ADD_WIDGET_VECTOR_SLIDER("vSceneVeh_driveOffset", vSceneVeh_driveOffset, -50.0, 50.0, 0.5)
			ELSE
				ADD_WIDGET_STRING("UNKNOWN sPassedScene.eVehState")
			ENDIF
			
			IF GET_PLAYER_PED_ROTATION_FOR_SCENE(sPassedScene.sScene.eScene, fCreateHead, vCreateRot)
				ADD_WIDGET_BOOL("bVehRotationProperly", bVehRotationProperly)
				ADD_WIDGET_BOOL("bDrawVehRotationProperly", bDrawVehRotationProperly)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(playerProp)
			TEXT_LABEL_63 tplayerProp = "playerProp - "
			tplayerProp += GET_MODEL_NAME_FOR_DEBUG(ePropModel)
			ADD_WIDGET_STRING(tplayerProp)
			ADD_WIDGET_VECTOR_SLIDER("propOffset", propOffset, -100.0, 100.0, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("propRotation", propRotation, -180.0, 180.0, 1.0)
		ENDIF
		IF DOES_ENTITY_EXIST(playerExtraProp)
			TEXT_LABEL_63 tPlayerExtraProp = "playerExtraProp - "
			tPlayerExtraProp += GET_MODEL_NAME_FOR_DEBUG(eExtraPropModel)
			ADD_WIDGET_STRING(tPlayerExtraProp)
			ADD_WIDGET_VECTOR_SLIDER("ExtraPropOffset", ExtraPropOffset, -100.0, 100.0, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("ExtraPropRotation", ExtraPropRotation, -180.0, 180.0, 1.0)
		ENDIF
		
		IF DOES_ENTITY_EXIST(player_door_l)
			TEXT_LABEL_63 tplayer_door_l = "player_door_l - "
			tplayer_door_l += GET_MODEL_NAME_FOR_DEBUG(eDoorModel_l)
			ADD_WIDGET_STRING(tplayer_door_l)
			ADD_WIDGET_VECTOR_SLIDER("vDoorOffset_L", vDoorOffset_L, -100.0, 100.0, 0.01)
		ENDIF
		IF DOES_ENTITY_EXIST(player_door_r)
			TEXT_LABEL_63 tplayer_door_r = "player_door_r - "
			tplayer_door_r += GET_MODEL_NAME_FOR_DEBUG(eDoorModel_R)
			ADD_WIDGET_STRING(tplayer_door_r)
			ADD_WIDGET_VECTOR_SLIDER("vDoorOffset_R", vDoorOffset_R, -100.0, 100.0, 0.01)
		ENDIF
		
		ADD_WIDGET_STRING("scene_cam")
		ADD_WIDGET_BOOL("bRender_scene_cam", bRender_scene_cam)
		ADD_WIDGET_VECTOR_SLIDER("vCamEndOffset", vCamEndOffset, -100.0, 100.0, 0.01)
		
		IF vecCamEndRot.z < -180.0
			vecCamEndRot.z += 360.0
		ENDIF
		IF vecCamEndRot.z > 180.0
			vecCamEndRot.z -= 360.0
		ENDIF
		
		ADD_WIDGET_VECTOR_SLIDER("vecCamEndRot", vecCamEndRot, -180.0, 180.0, 1.0)
		
		FLOAT gameplayCamSmoothRate
		IF NOT SETUP_RELATIVE_GAMEPLAY_CAM_PITCH_SCENE(sPassedScene.sScene.eScene,
				gpCamPitch, gameplayCamSmoothRate,
				gpCamHead)
			FLOAT finalSplineCameraHeading = vecCamEndRot.z
			FLOAT playerHeading = fCreateHead
			gpCamHead = finalSplineCameraHeading - playerHeading
		ENDIF
		
		IF (gpCamPitch <> 999.000 ) OR (gpCamHead <> 999.000 )
			ADD_WIDGET_STRING("gameplayCam")
			
			IF (gpCamPitch = 999.000 )
				gpCamPitch = 0
			ENDIF
			IF (gpCamHead = 999.000 )
				gpCamHead = 0
			ENDIF
			
			ADD_WIDGET_FLOAT_SLIDER("gpCamPitch", gpCamPitch, -90.0, 45.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("gpCamHead", gpCamHead, -360.0, 360.0, 1.0)
		ENDIF
		
		IF GET_PLAYER_VEH_RECORDING_FOR_SCENE(sPassedScene.sScene.eScene,
				tRecording_name, iRecording_num,
				fRecording_start, fRecording_skip, fRecording_stop,
				fSpeed_switch, fSpeed_exit)
			ADD_WIDGET_STRING("vehicleRec")
			ADD_WIDGET_STRING(tRecording_name)
			ADD_WIDGET_INT_SLIDER("iRecording_num",		iRecording_num,		0, 100, 0)
			ADD_WIDGET_FLOAT_SLIDER("fRecording_start",	fRecording_start,	0.0, 100000.0, 500.0)
			ADD_WIDGET_FLOAT_SLIDER("fRecording_skip",	fRecording_skip,	0.0, 100000.0, 500.0)
			ADD_WIDGET_FLOAT_SLIDER("fRecording_stop",	fRecording_stop,	0.0, 100000.0, 500.0)
			ADD_WIDGET_FLOAT_SLIDER("fSpeed_switch",	fSpeed_switch,		0.0, 2.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("fSpeed_exit",		fSpeed_exit,		0.0, 2.0, 0.1)
			
		ENDIF
		
		
	STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(sceneWidget)
	
	WHILE NOT IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
		
		IF DOES_CAM_EXIST(scene_cam)
			
			IF bRender_scene_cam
				SET_CAM_ACTIVE(scene_cam, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				SET_CAM_COORD(scene_cam, vCamEndOffset+vCreateCoords)
				SET_CAM_ROT(scene_cam, vecCamEndRot)
			ELSE
				SET_CAM_ACTIVE(scene_cam, FALSE)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				
				PRINTSTRING("gpCamHead = ")
//				PRINTFLOAT(finalSplineCameraHeading)
//				PRINTSTRING(" – ")
//				PRINTFLOAT(playerHeading)
//				PRINTSTRING(" = ")
				PRINTFLOAT(gpCamHead)
				PRINTNL()
				
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(gpCamPitch)
				
				IF (sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL)
				OR (sPassedScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
				OR (sPassedScene.eLoopTask = SCRIPT_TASK_AIM_GUN_AT_ENTITY)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(gpCamHead - 180)
				ELSE
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(gpCamHead)
				
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
			DRAW_DEBUG_SPHERE(vCreateCoords, 0.25, 255, 0, 0, 255)
			
			TEXT_LABEL_63 str1 = "", str2 = "", str3 = "", str4 = ""
			
			TEXT_LABEL_63 tPlayerAnimTest
			SCRIPT_TASK_NAME eTestedTask
			IF NOT bExitSynchScene
				tPlayerAnimTest = tPlayerAnimLoop
				eTestedTask = sPassedScene.eLoopTask
			ELSE
				tPlayerAnimTest = tPlayerAnimOut
				eTestedTask = sPassedScene.eOutTask
			ENDIF
			
			SWITCH eTestedTask
				CASE SCRIPT_TASK_PLAY_ANIM
					str1 += ("SCRIPT_TASK_PLAY_ANIM")
					
//					ANIMATION_FLAGS playerLoopFlag, playerOutFlag
//					playerLoopFlag = AF_DEFAULT
//					playerOutFlag = AF_DEFAULT
//					IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sPassedScene.sScene.eScene,
//							tPlayerAnimDict, tPlayerAnimTest, tPlayerAnimOut,
//							playerLoopFlag, playerOutFlag)
						
						str2 += ("\"")
						str2 += GET_STRING_FROM_STRING(tPlayerAnimDict,
								GET_LENGTH_OF_LITERAL_STRING("SWITCH@"),
								GET_LENGTH_OF_LITERAL_STRING(tPlayerAnimDict))
						str2 += ("\"")
						
						str3 += ("\"")
						str3 += (tPlayerAnimTest)
						str3 += ("\"")
						
//						str4 += "eloop: "
//						str4 += ENUM_TO_INT(playerLoopFlag)
//						str4 += ", eOut: "
//						str4 += ENUM_TO_INT(playerOutFlag)

						FLOAT fPlayerAnimCoord_groundZ, fAnimPos_heightDiff
						VECTOR vPlayerAnimCoord
						fPlayerAnimCoord_groundZ = 0.0
						vPlayerAnimCoord = vCreateCoords
						IF GET_GROUND_Z_FOR_3D_COORD(vPlayerAnimCoord+<<0,0,1>>, fPlayerAnimCoord_groundZ)
							fAnimPos_heightDiff = (vPlayerAnimCoord.z) - fPlayerAnimCoord_groundZ
							
							TEXT_LABEL_63 strAnim
							strAnim  = "_z:"
							strAnim += GET_STRING_FROM_FLOAT(fAnimPos_heightDiff)
							
							IF fAnimPos_heightDiff > 0
								DrawDebugSceneTextWithOffset(strAnim, vCreateCoords, 0, HUD_COLOUR_RED)
							ELIF fAnimPos_heightDiff < 0
								DrawDebugSceneTextWithOffset(strAnim, vCreateCoords, 0, HUD_COLOUR_GREEN)
							ELSE
								DrawDebugSceneTextWithOffset(strAnim, vCreateCoords, 0, HUD_COLOUR_BLUE)
							ENDIF
							
						ENDIF
						
//						IF bDisableLegIk
//							IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag, AF_DISABLE_LEG_IK)
//								SET_BITMASK_ENUM_AS_ENUM(playerLoopFlag, AF_DISABLE_LEG_IK)
//								CLEAR_PED_TASKS(PLAYER_PED_ID())
//							ENDIF
//						ELSE
//							IF IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag, AF_DISABLE_LEG_IK)
//								CLEAR_BITMASK_ENUM_AS_ENUM(playerLoopFlag, AF_DISABLE_LEG_IK)
//								CLEAR_PED_TASKS(PLAYER_PED_ID())
//							ENDIF
//						ENDIF
						
						IF bOverridePhysics
							IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag, AF_OVERRIDE_PHYSICS)
								SET_BITMASK_ENUM_AS_ENUM(playerLoopFlag, AF_OVERRIDE_PHYSICS)
								CLEAR_PED_TASKS(PLAYER_PED_ID())
							ENDIF
						ELSE
							IF IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag, AF_OVERRIDE_PHYSICS)
								CLEAR_BITMASK_ENUM_AS_ENUM(playerLoopFlag, AF_OVERRIDE_PHYSICS)
								CLEAR_PED_TASKS(PLAYER_PED_ID())
							ENDIF
						ENDIF
						
						IF bTurnOffCollision
							IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag, AF_TURN_OFF_COLLISION)
								SET_BITMASK_ENUM_AS_ENUM(playerLoopFlag, AF_TURN_OFF_COLLISION)
								CLEAR_PED_TASKS(PLAYER_PED_ID())
							ENDIF
						ELSE
							IF IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag, AF_TURN_OFF_COLLISION)
								CLEAR_BITMASK_ENUM_AS_ENUM(playerLoopFlag, AF_TURN_OFF_COLLISION)
								CLEAR_PED_TASKS(PLAYER_PED_ID())
							ENDIF
						ENDIF
						
						IF bExtractInitialOffset
							IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag, AF_EXTRACT_INITIAL_OFFSET)
								SET_BITMASK_ENUM_AS_ENUM(playerLoopFlag, AF_EXTRACT_INITIAL_OFFSET)
								CLEAR_PED_TASKS(PLAYER_PED_ID())
							ENDIF
						ELSE
							IF IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag, AF_EXTRACT_INITIAL_OFFSET)
								CLEAR_BITMASK_ENUM_AS_ENUM(playerLoopFlag, AF_EXTRACT_INITIAL_OFFSET)
								CLEAR_PED_TASKS(PLAYER_PED_ID())
							ENDIF
						ENDIF
						
						IF bNotInterruptable
							IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag, AF_NOT_INTERRUPTABLE)
								SET_BITMASK_ENUM_AS_ENUM(playerLoopFlag, AF_NOT_INTERRUPTABLE)
								CLEAR_PED_TASKS(PLAYER_PED_ID())
							ENDIF
						ELSE
							IF IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag, AF_NOT_INTERRUPTABLE)
								CLEAR_BITMASK_ENUM_AS_ENUM(playerLoopFlag, AF_NOT_INTERRUPTABLE)
								CLEAR_PED_TASKS(PLAYER_PED_ID())
							ENDIF
						ENDIF
						
						IF bLooping
							IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag, AF_LOOPING)
								SET_BITMASK_ENUM_AS_ENUM(playerLoopFlag, AF_LOOPING)
								CLEAR_PED_TASKS(PLAYER_PED_ID())
							ENDIF
						ELSE
							IF IS_BITMASK_ENUM_AS_ENUM_SET(playerLoopFlag, AF_LOOPING)
								CLEAR_BITMASK_ENUM_AS_ENUM(playerLoopFlag, AF_LOOPING)
								CLEAR_PED_TASKS(PLAYER_PED_ID())
							ENDIF
						ENDIF
						
						fCreateHead = vCreateRot.z
						
						IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerAnimDict, tPlayerAnimTest)
							
							vStoredCoords = vCreateCoords
							fStoredHead = fCreateHead
							
							SET_ENTITY_COORDS(PLAYER_PED_ID(), vCreateCoords)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), fCreateHead)
							
							TASK_PLAY_ANIM(PLAYER_PED_ID(), tPlayerAnimDict, tPlayerAnimTest,
									INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
									playerLoopFlag)
						ELSE
							
							IF NOT ARE_VECTORS_EQUAL(vStoredCoords, vCreateCoords)
							OR (fStoredHead <> (fCreateHead))
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								CLEAR_AREA(vCreateCoords, 4.0, TRUE)
							ENDIF
							
						ENDIF
						
//					ELSE
//						str2 += ("none")
//						
//					ENDIF
					
				BREAK
				CASE SCRIPT_TASK_SYNCHRONIZED_SCENE
					str1 += ("SCRIPT_TASK_SYNCHRONIZED_SCENE")
					
					
					IF NOT bExitSynchScene
						str3 += ("Loop_SynchSceneID")
						IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
							DETACH_SYNCHRONIZED_SCENE(g_iPlayer_Timetable_Exit_SynchSceneID)
							g_iPlayer_Timetable_Exit_SynchSceneID = -1
						ENDIF
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)

							TEXT_LABEL_63 tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut
							tPlayerSceneSyncAnimDict = ""
							tPlayerSceneSyncAnimLoop = "" 
							tPlayerSceneSyncAnimOut = "" 
							ANIMATION_FLAGS playerSyncLoopFlag, playerSyncOutFlag
							//enumPlayerSceneAnimProgress ePlayerSceneAnimProgress
							
							playerSyncLoopFlag = AF_DEFAULT
							playerSyncOutFlag = AF_DEFAULT
//							ePlayerSceneAnimProgress = MAX_PLAYER_SCENE_ANIM_PROGRESS
							
							IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sPassedScene.sScene.eScene,
									tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut,
									playerSyncLoopFlag, playerSyncOutFlag)	//, ePlayerSceneAnimProgress)
								
							//	SetPedFromVehicleIntoSynchedScene(PED_INDEX PedIndex, VEHICLE_INDEX VehicleIndex)
								IF (sPassedScene.eVehState = PTVS_2_playerInVehicle)
									
									IF NOT IS_ENTITY_DEAD(scene_veh)
										IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), scene_veh)
											SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(scene_veh))
											SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_ENTITY_HEADING(scene_veh))
											
//											RETURN TRUE
										ENDIF
									ENDIF
								ENDIF
								
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
								ELSE
									CLEAR_PED_TASKS(PLAYER_PED_ID())
								ENDIF
								
								g_iPlayer_Timetable_Loop_SynchSceneID = CREATE_SYNCHRONIZED_SCENE(vCreateCoords, vCreateRot)
								
								SET_SYNCHRONIZED_SCENE_LOOPED(g_iPlayer_Timetable_Loop_SynchSceneID, TRUE)
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), g_iPlayer_Timetable_Loop_SynchSceneID,
										tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop,
										NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
								
								IF NOT IS_ENTITY_DEAD(scene_veh)
									TEXT_LABEL_63 tPlayerSceneSyncAnimVehLoop
									tPlayerSceneSyncAnimVehLoop = ""
									IF GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(sPassedScene.sScene.eScene, tPlayerSceneSyncAnimVehLoop)
										PLAY_SYNCHRONIZED_ENTITY_ANIM(scene_veh,
												g_iPlayer_Timetable_Loop_SynchSceneID,
												tPlayerSceneSyncAnimVehLoop, tPlayerSceneSyncAnimDict,
												NORMAL_BLEND_IN)
										
										vVehCoordsOffset = GET_ENTITY_COORDS(scene_veh) - vCreateCoords
										fVehHeadOffset = GET_ENTITY_HEADING(scene_veh) - fCreateHead
										
										IF fVehHeadOffset > 180.0
											fVehHeadOffset -= 360.0
										ENDIF
										IF fVehHeadOffset < -180.0
											fVehHeadOffset += 360.0
										ENDIF
									ENDIF
								ENDIF
								
								TEXT_LABEL_63 tPlayerSceneSyncLoopCam
								tPlayerSceneSyncLoopCam = ""
								IF GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_LOOP_SCENE(sPassedScene.sScene.eScene, tPlayerSceneSyncLoopCam)
									IF DOES_CAM_EXIST(scene_cam)
										DESTROY_CAM(scene_cam)
									ENDIF
									scene_cam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
									PLAY_SYNCHRONIZED_CAM_ANIM(scene_cam,
											g_iPlayer_Timetable_Loop_SynchSceneID,
											tPlayerSceneSyncLoopCam, tPlayerSceneSyncAnimDict)

								ELSE
									IF DOES_CAM_EXIST(scene_cam)
										DESTROY_CAM(scene_cam)
									ENDIF
									scene_cam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",
											vecCamEndPos, vecCamEndRot,
											GET_GAMEPLAY_CAM_FOV())
								ENDIF
							ENDIF
							
							
							
						ELSE
							SET_SYNCHRONIZED_SCENE_ORIGIN(g_iPlayer_Timetable_Loop_SynchSceneID, vCreateCoords, vCreateRot)
						ENDIF
					ELSE
						str3 += ("Exit_SynchSceneID ")
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
							DETACH_SYNCHRONIZED_SCENE(g_iPlayer_Timetable_Loop_SynchSceneID)
							g_iPlayer_Timetable_Loop_SynchSceneID = -1
						ENDIF
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
							TEXT_LABEL_63 tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut
							tPlayerSceneSyncAnimDict = ""
							tPlayerSceneSyncAnimLoop = "" 
							tPlayerSceneSyncAnimOut = "" 
							ANIMATION_FLAGS playerSyncexitFlag, playerSyncOutFlag
							//enumPlayerSceneAnimProgress ePlayerSceneAnimProgress
							
							playerSyncexitFlag = AF_DEFAULT
							playerSyncOutFlag = AF_DEFAULT
							//ePlayerSceneAnimProgress = MAX_PLAYER_SCENE_ANIM_PROGRESS
							
							IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sPassedScene.sScene.eScene,
									tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut,
									playerSyncexitFlag, playerSyncOutFlag)	//, ePlayerSceneAnimProgress)
								
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
								ELSE
									CLEAR_PED_TASKS(PLAYER_PED_ID())
								ENDIF
								
								g_iPlayer_Timetable_Exit_SynchSceneID = CREATE_SYNCHRONIZED_SCENE(vCreateCoords, vCreateRot)
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), g_iPlayer_Timetable_Exit_SynchSceneID,
										tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimOut,
										NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
								SET_SYNCHRONIZED_SCENE_LOOPED(g_iPlayer_Timetable_Exit_SynchSceneID, FALSE)
								SET_SYNCHRONIZED_SCENE_RATE(g_iPlayer_Timetable_Exit_SynchSceneID, 1.0)
								
								IF NOT IS_ENTITY_DEAD(scene_veh)
									TEXT_LABEL_63 tPlayerSceneSyncAnimVehExit
									tPlayerSceneSyncAnimVehExit = ""
									IF GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_EXIT_SCENE(sPassedScene.sScene.eScene, tPlayerSceneSyncAnimVehExit)
										PLAY_SYNCHRONIZED_ENTITY_ANIM(scene_veh,
												g_iPlayer_Timetable_Exit_SynchSceneID,
												tPlayerSceneSyncAnimVehExit, tPlayerSceneSyncAnimDict,
												NORMAL_BLEND_IN)
										
										vVehCoordsOffset = GET_ENTITY_COORDS(scene_veh) - vCreateCoords
										fVehHeadOffset = GET_ENTITY_HEADING(scene_veh) - fCreateHead
										
										IF fVehHeadOffset > 180.0
											fVehHeadOffset -= 360.0
										ENDIF
										IF fVehHeadOffset < -180.0
											fVehHeadOffset += 360.0
										ENDIF
									ENDIF
								ENDIF
								
								TEXT_LABEL_63 tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix
								tPlayerSceneSyncExitCam = ""
								tPlayerSceneEstabShotSuffix = ""
								IF GET_SYNCHRONIZED_CAM_FOR_TIMETABLE_EXIT_SCENE(sPassedScene.sScene.eScene, tPlayerSceneSyncExitCam, tPlayerSceneEstabShotSuffix)
									IF DOES_CAM_EXIST(scene_cam)
										DESTROY_CAM(scene_cam)
									ENDIF
									scene_cam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
									PLAY_SYNCHRONIZED_CAM_ANIM(scene_cam,
											g_iPlayer_Timetable_Exit_SynchSceneID,
											tPlayerSceneSyncExitCam, tPlayerSceneSyncAnimDict)

								ELSE
									IF DOES_CAM_EXIST(scene_cam)
										DESTROY_CAM(scene_cam)
									ENDIF
									scene_cam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",
											vecCamEndPos, vecCamEndRot,
											GET_GAMEPLAY_CAM_FOV())
								ENDIF
							ENDIF
							
							TEXT_LABEL_63 tPlayerDoorSyncAnimL, tPlayerDoorSyncAnimR
							tPlayerDoorSyncAnimL = ""
							tPlayerDoorSyncAnimR = ""
							IF GET_SYNCHRONIZED_DOOR_FOR_TIMETABLE_EXIT_SCENE(sPassedScene.sScene.eScene, tPlayerDoorSyncAnimL, tPlayerDoorSyncAnimR)

								IF NOT IS_STRING_NULL_OR_EMPTY(tPlayerDoorSyncAnimL)
									IF DOES_ENTITY_EXIST(player_door_l)
										PLAY_SYNCHRONIZED_ENTITY_ANIM(player_door_l,
												g_iPlayer_Timetable_Exit_SynchSceneID,
												tPlayerDoorSyncAnimL, tPlayerSceneSyncAnimDict,
												NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
									ENDIF
								ENDIF
								IF NOT IS_STRING_NULL_OR_EMPTY(tPlayerDoorSyncAnimR)
									IF DOES_ENTITY_EXIST(player_door_r)
										PLAY_SYNCHRONIZED_ENTITY_ANIM(player_door_r,
												g_iPlayer_Timetable_Exit_SynchSceneID,
												tPlayerDoorSyncAnimR, tPlayerSceneSyncAnimDict,
												NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
									ENDIF
								ENDIF
							ENDIF
							
						ELSE
							FLOAT fExitSynchScene_phase
							fExitSynchScene_phase = GET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID)
							
							IF fExitSynchScene_phase >= 1.0
								IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
									TEXT_LABEL_63 tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut
									tPlayerSceneSyncAnimDict = ""
									tPlayerSceneSyncAnimLoop = "" 
									tPlayerSceneSyncAnimOut = "" 
									ANIMATION_FLAGS playerSyncexitFlag, playerSyncOutFlag
									
									playerSyncexitFlag = AF_DEFAULT
									playerSyncOutFlag = AF_DEFAULT
									
									IF GET_PLAYER_ANIM_FOR_TIMETABLE_SCENE(sPassedScene.sScene.eScene,
											tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimLoop, tPlayerSceneSyncAnimOut,
											playerSyncexitFlag, playerSyncOutFlag)	//, ePlayerSceneAnimProgress)
										IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimOut, ANIM_SYNCED_SCENE)
											CLEAR_PED_TASKS(PLAYER_PED_ID())
											
											PRINTSTRING("testA ")
										ELSE
											TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), g_iPlayer_Timetable_Exit_SynchSceneID,
													tPlayerSceneSyncAnimDict, tPlayerSceneSyncAnimOut,
													NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
											
											PRINTSTRING("testB ")
										ENDIF
										
										VECTOR vPedBoneCoord
										FLOAT fPedBoneCoord_groundZ, fSyncPos_heightDiff
										
										fPedBoneCoord_groundZ = 0.0
										vPedBoneCoord = GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_L_toe, <<0,0,0>>)
										IF GET_GROUND_Z_FOR_3D_COORD(vPedBoneCoord, fPedBoneCoord_groundZ)
											fSyncPos_heightDiff = (vPedBoneCoord.z) - fPedBoneCoord_groundZ
											
											PRINTSTRING(", L_toe_z: ")
											PRINTFLOAT(fSyncPos_heightDiff)
											
										ENDIF
										fPedBoneCoord_groundZ = 0.0
										vPedBoneCoord = GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_R_toe, <<0,0,0>>)
										IF GET_GROUND_Z_FOR_3D_COORD(vPedBoneCoord, fPedBoneCoord_groundZ)
											fSyncPos_heightDiff = (vPedBoneCoord.z) - fPedBoneCoord_groundZ
											
											PRINTSTRING(", R_toe_z: ")
											PRINTFLOAT(fSyncPos_heightDiff)
											
										ENDIF
										PRINTNL()
										
										
										
									ENDIF
								ENDIF
							ENDIF
								
							FLOAT fPlayerCoord_groundZ, fSyncPos_heightDiff
							VECTOR vPlayerCoord
							fPlayerCoord_groundZ = 0.0
							vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
							IF GET_GROUND_Z_FOR_3D_COORD(vPlayerCoord, fPlayerCoord_groundZ)
								fSyncPos_heightDiff = (vPlayerCoord.z - 1) - fPlayerCoord_groundZ
								
								str1 += "_z:"
								str1 += GET_STRING_FROM_FLOAT(fSyncPos_heightDiff)
							ENDIF
							
							str3 += GET_STRING_FROM_FLOAT(fExitSynchScene_phase)
							
							SET_SYNCHRONIZED_SCENE_ORIGIN(g_iPlayer_Timetable_Exit_SynchSceneID, vCreateCoords, vCreateRot)
							
							str4 += "L/R: change phase, UP: toggle rate"
							IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
							
								IF GET_SYNCHRONIZED_SCENE_RATE(g_iPlayer_Timetable_Exit_SynchSceneID) <= 0
									SET_SYNCHRONIZED_SCENE_RATE(g_iPlayer_Timetable_Exit_SynchSceneID, 1.0)
								ELSE
									SET_SYNCHRONIZED_SCENE_RATE(g_iPlayer_Timetable_Exit_SynchSceneID, 0.0)
								ENDIF
							ENDIF
							IF IS_KEYBOARD_KEY_PRESSED(KEY_LEFT)
								fExitSynchScene_phase -= 0.01
								IF fExitSynchScene_phase <= 0
									fExitSynchScene_phase = 0
								ENDIF
								SET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID, fExitSynchScene_phase)
							ELIF IS_KEYBOARD_KEY_PRESSED(KEY_RIGHT)
								fExitSynchScene_phase += 0.01
								IF fExitSynchScene_phase >= 1
									fExitSynchScene_phase = 1
								ENDIF
								SET_SYNCHRONIZED_SCENE_PHASE(g_iPlayer_Timetable_Exit_SynchSceneID, fExitSynchScene_phase)
							ENDIF
							
							IF (ePropModel = PROP_CS_BEER_BOT_01)
								REQUEST_PTFX_ASSET()
								IF HAS_PTFX_ASSET_LOADED()
									IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
										START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_pts_glass_bottle",
												PLAYER_PED_ID(), <<0,0,0>>, <<0,0,0>>, BONETAG_PH_L_HAND)
										PRINTSTRING("START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE(\"scr_pts_glass_bottle\", PLAYER_PED_ID(), <<0,0,0>>, <<0,0,0>>, BONETAG_PH_L_HAND)")
										PRINTNL()
									ENDIF
								ENDIF
								
							ENDIF
							
						ENDIF
					ENDIF
				BREAK
				CASE SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING
					str1 += ("SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING")
					
					IF NOT IS_ENTITY_DEAD(scene_veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(scene_veh)
							REQUEST_VEHICLE_RECORDING(iRecording_num, tRecording_name)
							IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecording_num, tRecording_name)
								REQUEST_VEHICLE_RECORDING(iRecording_num, tRecording_name)
							ELSE
								FREEZE_ENTITY_POSITION(scene_veh, FALSE)
								
								START_PLAYBACK_RECORDED_VEHICLE(scene_veh, iRecording_num, tRecording_name)
								DISPLAY_PLAYBACK_RECORDED_VEHICLE(scene_veh, RDM_WHOLELINE)
							ENDIF
						ELSE
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(scene_veh, fRecording_skip - GET_TIME_POSITION_IN_RECORDING(scene_veh))
							
							VECTOR vRecording_start, vRecording_skip
							vRecording_start = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecording_num, fRecording_start, tRecording_name)
							vRecording_skip = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecording_num, fRecording_skip, tRecording_name)
							
							DrawDebugSceneSphere(vRecording_start, 1.0, HUD_COLOUR_RED, 0.5)
							DrawDebugSceneSphere(vRecording_skip, 1.0, HUD_COLOUR_GREEN, 0.5)
						ENDIF
					ENDIF
					
					str2 += ("tRecording_name: ")str2 += (tRecording_name)
					IF iRecording_num < 100	str2 += ("0") ENDIF
					IF iRecording_num < 10	str2 += ("0") ENDIF
					str2 += (iRecording_num)
					
					str3 += ("fRecording_start: ")	str3 += GET_STRING_FROM_FLOAT(fRecording_start)
					str4 += ("fRecording_skip: ")	str4 += GET_STRING_FROM_FLOAT(fRecording_skip)
//					str5 += ("fSpeed_switch: ")		str5 += GET_STRING_FROM_FLOAT(fSpeed_switch)
//					str6 += ("fSpeed_exit: ")		str6 += GET_STRING_FROM_FLOAT(fSpeed_exit)
					
				BREAK
				
				DEFAULT
					
					fCreateHead = vCreateRot.z
					
					FLOAT fHeadingOffset
					fHeadingOffset = 0.0
					
					IF eTestedTask = SCRIPT_TASK_STAND_STILL
						str1 += ("SCRIPT_TASK_STAND_STILL")
						fHeadingOffset = 180
					ELIF eTestedTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
						
						str1 += ("SCRIPT_TASK_GO_STRAIGHT_TO_COORD")
						fHeadingOffset = 180
						
						VECTOR vWalkToCoord
						vWalkToCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,-5,0>>)
						
						SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
						DRAW_DEBUG_LINE(vWalkToCoord, GET_ENTITY_COORDS(PLAYER_PED_ID()))
						DRAW_DEBUG_SPHERE(vWalkToCoord, 0.5)
							
					ELIF eTestedTask = SCRIPT_TASK_AIM_GUN_AT_ENTITY
						str1 += ("SCRIPT_TASK_AIM_GUN_AT_ENTITY")
						fHeadingOffset = 180
					ELIF eTestedTask = SCRIPT_TASK_ENTER_VEHICLE
						str1 += ("SCRIPT_TASK_ENTER_VEHICLE")
						fHeadingOffset = 180
					ELIF eTestedTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
						IF IS_VEHICLE_DRIVEABLE(scene_veh)
							str1 += ("SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD")
							
							VECTOR vDriveToCoord
							vDriveToCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_veh, vSceneVeh_driveOffset)
							
							SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
							DRAW_DEBUG_LINE(vDriveToCoord, GET_ENTITY_COORDS(scene_veh))
							DRAW_DEBUG_SPHERE(vDriveToCoord, 1.0)
						ELSE
							str1 += ("SCRIPT_TASK_DEAD_VEHICLE_DRIVE_TO_COORD")
						ENDIF
					ELSE
						str1 += (Get_String_From_Script_Task_Name(eTestedTask))
						str1 += ("_LOOP")
					ENDIF
					
					
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vCreateCoords)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), fCreateHead + fHeadingOffset)
					

					IF sPassedScene.eVehState = PTVS_2_playerInVehicle
						IF IS_VEHICLE_DRIVEABLE(scene_veh)
							IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), scene_veh)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), scene_veh)
							ELSE
								VECTOR vSceneVeh_driveCoord
								vSceneVeh_driveCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_veh, vSceneVeh_driveOffset)
								
								SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
								DRAW_DEBUG_SPHERE(vSceneVeh_driveCoord, 1.0)
								
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
			ENDSWITCH
			
			DrawLiteralSceneString(Get_String_From_Ped_Request_Scene_Enum(sPassedScene.sScene.eScene), 0)
			DrawLiteralSceneString(str1, 1)
			DrawLiteralSceneString(str2, 2)
			DrawLiteralSceneString(str3, 3)
			DrawLiteralSceneString(str4, 4)
			
		ENDIF
		
		IF NOT IS_PED_INJURED(g_pScene_buddy)
			IF sPassedScene.eVehState <> PTVS_2_playerInVehicle
			
				SET_ENTITY_COORDS(g_pScene_buddy, vCreateCoords+sPassedScene.vSceneBuddyCoordOffset)
				SET_ENTITY_HEADING(g_pScene_buddy, fCreateHead+sPassedScene.fSceneBuddyHeadOffset)
				
				IF (sPassedScene.eBuddyLoopTask = script_task_play_anim)
				OR (sPassedScene.eBuddyOutTask = script_task_play_anim)
					ANIMATION_FLAGS buddyAnimLoopFlag, buddyAnimOutFlag
					buddyAnimLoopFlag = AF_DEFAULT
					buddyAnimOutFlag = AF_DEFAULT
					IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(sPassedScene.sScene.eScene,
							tSceneBuddyAnimDict, tSceneBuddyAnimLoop, tSceneBuddyAnimOut,
							buddyAnimLoopFlag, buddyAnimOutFlag)
				
						IF NOT IS_ENTITY_PLAYING_ANIM(g_pScene_buddy, tSceneBuddyAnimDict, tSceneBuddyAnimLoop)
							TASK_PLAY_ANIM_ADVANCED(g_pScene_buddy, tSceneBuddyAnimDict, tSceneBuddyAnimLoop,
									vCreateCoords+sPassedScene.vSceneBuddyCoordOffset, <<0,0,fCreateHead+sPassedScene.fSceneBuddyHeadOffset>>,
									INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
									buddyAnimLoopFlag)
										
							sPassedScene.vSceneBuddyCoordOffset = GET_ENTITY_COORDS(g_pScene_buddy) - vCreateCoords
							sPassedScene.fSceneBuddyHeadOffset = GET_ENTITY_HEADING(g_pScene_buddy) - fCreateHead
							
							IF sPassedScene.fSceneBuddyHeadOffset > 180.0
								sPassedScene.fSceneBuddyHeadOffset -= 360.0
							ENDIF
							IF sPassedScene.fSceneBuddyHeadOffset < -180.0
								sPassedScene.fSceneBuddyHeadOffset += 360.0
							ENDIF
						ENDIF
					ENDIF
				ELIF (sPassedScene.eBuddyLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE)
				OR (sPassedScene.eBuddyOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE)
					
					IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
						TEXT_LABEL_63 tBuddySceneSyncAnimDict, tBuddySceneSyncAnimLoop, tBuddySceneSyncAnimOut
						tBuddySceneSyncAnimDict = ""
						tBuddySceneSyncAnimLoop = "" 
						tBuddySceneSyncAnimOut = "" 
						ANIMATION_FLAGS buddySyncLoopFlag, buddySyncOutFlag
						buddySyncLoopFlag = AF_DEFAULT
						buddySyncOutFlag = AF_DEFAULT
						
						IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(sPassedScene.sScene.eScene,
								tBuddySceneSyncAnimDict, tBuddySceneSyncAnimLoop, tBuddySceneSyncAnimOut,
								buddySyncLoopFlag, buddySyncOutFlag)
							IF NOT IS_ENTITY_PLAYING_ANIM(g_pScene_buddy, tBuddySceneSyncAnimDict, tBuddySceneSyncAnimLoop, ANIM_SYNCED_SCENE)
								TASK_SYNCHRONIZED_SCENE(g_pScene_buddy, g_iPlayer_Timetable_Loop_SynchSceneID,
										tBuddySceneSyncAnimDict, tBuddySceneSyncAnimLoop,
										NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
										
								sPassedScene.vSceneBuddyCoordOffset = GET_ENTITY_COORDS(g_pScene_buddy) - vCreateCoords
								sPassedScene.fSceneBuddyHeadOffset = GET_ENTITY_HEADING(g_pScene_buddy) - fCreateHead
								
								IF sPassedScene.fSceneBuddyHeadOffset > 180.0
									sPassedScene.fSceneBuddyHeadOffset -= 360.0
								ENDIF
								IF sPassedScene.fSceneBuddyHeadOffset < -180.0
									sPassedScene.fSceneBuddyHeadOffset += 360.0
								ENDIF
							ENDIF
						ENDIF
					ELIF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
						TEXT_LABEL_63 tBuddySceneSyncAnimDict, tBuddySceneSyncAnimLoop, tBuddySceneSyncAnimOut
						tBuddySceneSyncAnimDict = ""
						tBuddySceneSyncAnimLoop = "" 
						tBuddySceneSyncAnimOut = "" 
						ANIMATION_FLAGS buddySyncexitFlag, buddySyncOutFlag
						buddySyncexitFlag = AF_DEFAULT
						buddySyncOutFlag = AF_DEFAULT
						
						IF GET_BUDDY_ANIM_FOR_TIMETABLE_SCENE(sPassedScene.sScene.eScene,
								tBuddySceneSyncAnimDict, tBuddySceneSyncAnimLoop, tBuddySceneSyncAnimOut,
								buddySyncexitFlag, buddySyncOutFlag)
							IF NOT IS_ENTITY_PLAYING_ANIM(g_pScene_buddy, tBuddySceneSyncAnimDict, tBuddySceneSyncAnimOut, ANIM_SYNCED_SCENE)
								TASK_SYNCHRONIZED_SCENE(g_pScene_buddy, g_iPlayer_Timetable_Exit_SynchSceneID,
										tBuddySceneSyncAnimDict, tBuddySceneSyncAnimOut,
										NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
								
								sPassedScene.vSceneBuddyCoordOffset = GET_ENTITY_COORDS(g_pScene_buddy) - vCreateCoords
								sPassedScene.fSceneBuddyHeadOffset = GET_ENTITY_HEADING(g_pScene_buddy) - fCreateHead
								
								IF sPassedScene.fSceneBuddyHeadOffset > 180.0
									sPassedScene.fSceneBuddyHeadOffset -= 360.0
								ENDIF
								IF sPassedScene.fSceneBuddyHeadOffset < -180.0
									sPassedScene.fSceneBuddyHeadOffset += 360.0
								ENDIF
							ELSE
								
								FLOAT fBuddyCoord_groundZ, fAnimPos_heightDiff
								VECTOR vBuddyCoord
								fBuddyCoord_groundZ = 0.0
								vBuddyCoord = GET_ENTITY_COORDS(g_pScene_buddy, FALSE)
								IF GET_GROUND_Z_FOR_3D_COORD(vBuddyCoord, fBuddyCoord_groundZ)
									fAnimPos_heightDiff = (vBuddyCoord.z - 1) - fBuddyCoord_groundZ
									
									TEXT_LABEL_63 strBuddy
									strBuddy  = "_z:"
									strBuddy += GET_STRING_FROM_FLOAT(fAnimPos_heightDiff)
									
									DrawDebugSceneTextWithOffset(strBuddy, vBuddyCoord, 0, HUD_COLOUR_RED)
								ENDIF
								
							ENDIF
						
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(scene_veh)
			
			fCreateHead = vCreateRot.z
			
			TEXT_LABEL_63 tPlayerSceneSyncAnimVehTemp
			tPlayerSceneSyncAnimVehTemp = ""
			IF NOT (IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID) AND GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_LOOP_SCENE(sPassedScene.sScene.eScene, tPlayerSceneSyncAnimVehTemp))
			AND NOT (IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_exit_SynchSceneID) AND GET_SYNCHRONIZED_VEH_FOR_TIMETABLE_exit_SCENE(sPassedScene.sScene.eScene, tPlayerSceneSyncAnimVehTemp))
				SET_ENTITY_COORDS(scene_veh, vCreateCoords+vVehCoordsOffset)
				SET_ENTITY_HEADING(scene_veh, fCreateHead+fVehHeadOffset)
			ENDIF
			
			IF bVehOffsetFromPlayerGivenWorldCoords
				VECTOR vVehOffsetFromPlayerGivenWorldCoords
				vVehOffsetFromPlayerGivenWorldCoords = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PLAYER_PED_ID(), vCreateCoords+vVehCoordsOffset)
				
				PRINTSTRING("vVehOffsetFromPlayerGivenWorldCoords: ")
				PRINTVECTOR(vVehOffsetFromPlayerGivenWorldCoords)
				PRINTNL()
			ENDIF
			

			IF bVehRotationProperly
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Loop_SynchSceneID)
					DETACH_SYNCHRONIZED_SCENE(g_iPlayer_Timetable_Loop_SynchSceneID)
					g_iPlayer_Timetable_Loop_SynchSceneID = -1
				ELIF IS_SYNCHRONIZED_SCENE_RUNNING(g_iPlayer_Timetable_Exit_SynchSceneID)
					DETACH_SYNCHRONIZED_SCENE(g_iPlayer_Timetable_Exit_SynchSceneID)
					g_iPlayer_Timetable_Exit_SynchSceneID = -1
				ENDIF
				
				SET_VEHICLE_ON_GROUND_PROPERLY(scene_veh)
				
				vCreateRot = GET_ENTITY_ROTATION(scene_veh) - <<0,0,fVehHeadOffset>>
				vCreateCoords = GET_ENTITY_COORDS(scene_veh) - vVehCoordsOffset
				
				VECTOR vCreateDimMin = <<0,0,0>>, vCreateDimMax = <<0,0,0>>
				GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(scene_veh), vCreateDimMin, vCreateDimMax)
				
				vCreateCoords -= vCreateDimMin.z
				
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				SAVE_STRING_TO_DEBUG_FILE("eScene: ")
				SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(sPassedScene.sScene.eScene))
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				SAVE_STRING_TO_DEBUG_FILE("vCreateCoords: ")
				SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				SAVE_STRING_TO_DEBUG_FILE("vCreateRot: ")
				SAVE_VECTOR_TO_DEBUG_FILE(vCreateRot)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				SAVE_STRING_TO_DEBUG_FILE("vCreateDim: ")
				SAVE_VECTOR_TO_DEBUG_FILE(vCreateDimMin)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_VECTOR_TO_DEBUG_FILE(vCreateDimMax)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				bVehRotationProperly = FALSE
			ENDIF
			
			IF bDrawVehRotationProperly
				
				VECTOR returnMin = <<0,0,0>>, returnMax = <<0,0,0>>
				GET_MODEL_DIMENSIONS(vehModel, returnMin, returnMax)
				
				VECTOR offsetA = <<returnMin.x, returnMin.y, returnMin.z>>
				VECTOR offsetB = <<returnMin.x, returnMax.y, returnMin.z>>
				VECTOR offsetC = <<returnMax.x, returnMax.y, returnMin.z>>
				VECTOR offsetD = <<returnMax.x, returnMin.y, returnMin.z>>
				
				VECTOR coordA = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_veh, offsetA)
				VECTOR coordB = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_veh, offsetB)
				VECTOR coordC = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_veh, offsetC)
				VECTOR coordD = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(scene_veh, offsetD)
				
				FLOAT coordGroundZ = 0.0
				
				DrawDebugSceneSphere(coordA, 0.1, HUD_COLOUR_RED)
				DrawDebugSceneTextWithOffset("coordA", coordA, 0, HUD_COLOUR_RED)
				IF GET_GROUND_Z_FOR_3D_COORD(coordA, coordGroundZ)
					DrawDebugSceneTextWithOffset(GET_STRING_FROM_FLOAT(coordA.z-coordGroundZ), coordA, 1, HUD_COLOUR_RED)
				ELSE
					DrawDebugSceneTextWithOffset("no ground", coordA, 1, HUD_COLOUR_BLUELIGHT)
				ENDIF
				
				DrawDebugSceneSphere(coordB, 0.1, HUD_COLOUR_RED)
				DrawDebugSceneTextWithOffset("coordB", coordB, 0, HUD_COLOUR_RED)
				IF GET_GROUND_Z_FOR_3D_COORD(coordB, coordGroundZ)
					DrawDebugSceneTextWithOffset(GET_STRING_FROM_FLOAT(coordB.z-coordGroundZ), coordB, 1, HUD_COLOUR_RED)
				ELSE
					DrawDebugSceneTextWithOffset("no ground", coordB, 1, HUD_COLOUR_BLUELIGHT)
				ENDIF
				
				DrawDebugSceneSphere(coordC, 0.1, HUD_COLOUR_RED)
				DrawDebugSceneTextWithOffset("coordC", coordC, 0, HUD_COLOUR_RED)
				IF GET_GROUND_Z_FOR_3D_COORD(coordC, coordGroundZ)
					DrawDebugSceneTextWithOffset(GET_STRING_FROM_FLOAT(coordC.z-coordGroundZ), coordC, 1, HUD_COLOUR_RED)
				ELSE
					DrawDebugSceneTextWithOffset("no ground", coordC, 1, HUD_COLOUR_BLUELIGHT)
				ENDIF
				
				DrawDebugSceneSphere(coordD, 0.1, HUD_COLOUR_RED)
				DrawDebugSceneTextWithOffset("coordD", coordD, 0, HUD_COLOUR_RED)
				IF GET_GROUND_Z_FOR_3D_COORD(coordD, coordGroundZ)
					DrawDebugSceneTextWithOffset(GET_STRING_FROM_FLOAT(coordD.z-coordGroundZ), coordD, 1, HUD_COLOUR_RED)
				ELSE
					DrawDebugSceneTextWithOffset("no ground", coordD, 1, HUD_COLOUR_BLUELIGHT)
				ENDIF
				
				
			ENDIF
			
			IF bVehOffsetFromPlayerInWorldCoords
				
				VECTOR VecNewOffset = << -0.276546, -3.53309, -1.36032 >>
				
				VECTOR vVehOffsetFromPlayerInWorldCoords = <<0,0,0>>
				VECTOR vVehOffsetFromPlayerAsSceneOffset = <<0,0,0>>
				
				
				/*
				SET_CURRENT_WIDGET_GROUP(testPlayerPedScene_widget)
				WIDGET_GROUP_ID vehWorldOffset_widget = START_WIDGET_GROUP("vVehOffsetFromPlayerInWorldCoords")
					ADD_WIDGET_BOOL("bVehOffsetFromPlayerInWorldCoords", bVehOffsetFromPlayerInWorldCoords)
					ADD_WIDGET_VECTOR_SLIDER("VecNewOffset", VecNewOffset, -40000, 40000, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("vCreateCoords", vCreateCoords, -40000, 40000, 0.0)
					ADD_WIDGET_VECTOR_SLIDER("vVehCoordsOffset", vVehCoordsOffset, -40000, 40000, 0.0)
					ADD_WIDGET_VECTOR_SLIDER("vVehOffsetFromPlayerInWorldCoords", vVehOffsetFromPlayerInWorldCoords, -40000, 40000, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("vVehOffsetFromPlayerAsSceneOffset", vVehOffsetFromPlayerAsSceneOffset, -40000, 40000, 0.1)
				STOP_WIDGET_GROUP()
				CLEAR_CURRENT_WIDGET_GROUP(testPlayerPedScene_widget)
				
				//
				
				WHILE bVehOffsetFromPlayerInWorldCoords
				*/
					vVehOffsetFromPlayerInWorldCoords =
							GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),
							VecNewOffset)
					vVehOffsetFromPlayerAsSceneOffset = vVehOffsetFromPlayerInWorldCoords - vCreateCoords
					vVehCoordsOffset = vVehOffsetFromPlayerAsSceneOffset
					
				/*
					IF IS_VEHICLE_DRIVEABLE(scene_veh)
						SET_ENTITY_COORDS(scene_veh, vCreateCoords+vVehCoordsOffset)
						SET_ENTITY_HEADING(scene_veh, fCreateHead+fVehHeadOffset)
					ENDIF
					
					WAIT(0)
				ENDWHILE
				
				IF DOES_WIDGET_GROUP_EXIST(vehWorldOffset_widget)
					DELETE_WIDGET_GROUP(vehWorldOffset_widget)
				ENDIF
				vVehCoordsOffset = vVehOffsetFromPlayerAsSceneOffset
				*/
				
				
				//
				
			ENDIF
			
		ENDIF
		
		IF DOES_ENTITY_EXIST(playerProp)
			IF (ePropBonetag <> BONETAG_NULL)
				SET_ENTITY_RECORDS_COLLISIONS(playerProp, TRUE)
				FREEZE_ENTITY_POSITION(playerProp, FALSE)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					ATTACH_ENTITY_TO_ENTITY(playerProp, PLAYER_PED_ID(),
							GET_PED_BONE_INDEX(PLAYER_PED_ID(), ePropBonetag),
							PropOffset, PropRotation)
				ENDIF
				
			ELSE
				fCreateHead = vCreateRot.z
				
				SET_ENTITY_COORDS(playerProp, vCreateCoords+PropOffset)
				SET_ENTITY_ROTATION(playerProp, <<0,0,fCreateHead>>+PropRotation)
			ENDIF
			
		ENDIF
		
		IF DOES_ENTITY_EXIST(playerExtraProp)
			IF (eExtraPropBonetag <> BONETAG_NULL)
				SET_ENTITY_RECORDS_COLLISIONS(playerExtraProp, TRUE)
				FREEZE_ENTITY_POSITION(playerExtraProp, FALSE)
				
				IF NOT bAttachedToBuddy
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						ATTACH_ENTITY_TO_ENTITY(playerExtraProp, PLAYER_PED_ID(),
								GET_PED_BONE_INDEX(PLAYER_PED_ID(), eExtraPropBonetag),
								ExtraPropOffset, ExtraPropRotation)
					ENDIF
				ELSE
					IF NOT IS_PED_INJURED(g_pScene_buddy)
						ATTACH_ENTITY_TO_ENTITY(playerExtraProp, g_pScene_buddy,
								GET_PED_BONE_INDEX(g_pScene_buddy, eExtraPropBonetag),
								ExtraPropOffset, ExtraPropRotation)
					ENDIF
				ENDIF
				
			ELSE
				fCreateHead = vCreateRot.z
				
				SET_ENTITY_COORDS(playerExtraProp, vCreateCoords+ExtraPropOffset)
				SET_ENTITY_ROTATION(playerExtraProp, <<0,0,fCreateHead>>+ExtraPropRotation)
			ENDIF
			
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_H)
			IF DOES_ENTITY_EXIST(player_door_l)
				REMOVE_MODEL_HIDE(sPassedScene.sScene.vCreateCoords, 5.0, eDoorModel_l)
			ENDIF
			IF DOES_ENTITY_EXIST(player_door_r)
				REMOVE_MODEL_HIDE(sPassedScene.sScene.vCreateCoords, 5.0, eDoorModel_r)
			ENDIF
		ENDIF
		
//		IF bGetPlaceholderCoordPostMission
//			
//			vCoordFrom = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,1000,0>>)
//			vCoordTo = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,-1000,0>>)
//			fPercentDist = 0.5
//			
//			SET_CURRENT_WIDGET_GROUP(sceneWidget)
//			WIDGET_GROUP_ID postMissionCoord_widget = START_WIDGET_GROUP("postMissionCoord_widget")
//				ADD_WIDGET_BOOL("bGetPlaceholderCoordPostMission", bGetPlaceholderCoordPostMission)
//				
//				ADD_WIDGET_VECTOR_SLIDER("vCoordFrom", vCoordFrom, -4000.0, 4000.0, 0.1)
//				ADD_WIDGET_VECTOR_SLIDER("vCoordTo", vCoordTo, -4000.0, 4000.0, 0.1)
//				ADD_WIDGET_FLOAT_SLIDER("fPercentDist", fPercentDist, 0.0, 1.0, 0.05)
//			STOP_WIDGET_GROUP()
//			CLEAR_CURRENT_WIDGET_GROUP(sceneWidget)
//			
//			WHILE bGetPlaceholderCoordPostMission
//				IF GetPlaceholderCoordPostMission(sPassedScene.sScene.eScene, vCoordFrom, vCoordTo, fPercentDist,
//						vCreateCoords, fCreateHead)
//					
//					DrawDebugSceneSphere(vCoordFrom, 1.0, HUD_COLOUR_RED)
//					DrawDebugSceneLineBetweenCoords(vCoordFrom, vCreateCoords, HUD_COLOUR_RED)
//					DrawDebugSceneLineBetweenCoords(vCreateCoords, vCoordTo, HUD_COLOUR_BLUE)
//					DrawDebugSceneSphere(vCoordTo, 1.0, HUD_COLOUR_BLUE)
//					
//					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//						SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vCreateCoords)
//						SET_ENTITY_HEADING(PLAYER_PED_ID(), fCreateHead)
//					ENDIF
//				ELSE
//					bGetPlaceholderCoordPostMission = FALSE
//				ENDIF
//				
//				WAIT(0)
//			ENDWHILE
//			
//			#IF IS_DEBUG_BUILD
//			OPEN_DEBUG_FILE()
//			SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_STRING_TO_DEBUG_FILE("		CASE ")SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(sPassedScene.sScene.eScene))SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_STRING_TO_DEBUG_FILE("			vLastKnownCoords = ")SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords)SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_STRING_TO_DEBUG_FILE("			fLastKnownHead = ")SAVE_FLOAT_TO_DEBUG_FILE(fCreateHead)SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_STRING_TO_DEBUG_FILE("			RETURN TRUE	BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_STRING_TO_DEBUG_FILE("		BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_NEWLINE_TO_DEBUG_FILE()
//			CLOSE_DEBUG_FILE()
//			#ENDIF
//			
//			IF DOES_WIDGET_GROUP_EXIST(postMissionCoord_widget)
//				DELETE_WIDGET_GROUP(postMissionCoord_widget)
//			ENDIF
//			
//		ENDIF
		
		
		WAIT(0)
	ENDWHILE
	
	IF DOES_CAM_EXIST(scene_cam)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_CAM_ACTIVE(scene_cam, FALSE)
		DESTROY_CAM(scene_cam)
	ENDIF

	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS(PLAYER_PED_ID())
	ENDIF
	IF NOT IS_PED_INJURED(g_pScene_buddy)
		CLEAR_PED_TASKS(g_pScene_buddy)
	ENDIF
	
	IF DOES_WIDGET_GROUP_EXIST(testPlayerPedScene_widget)
		DELETE_WIDGET_GROUP(testPlayerPedScene_widget)
	ENDIF
	
	OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("		//\"")SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(sPassedScene.sScene.eScene))SAVE_STRING_TO_DEBUG_FILE("\"")SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		//
		SAVE_STRING_TO_DEBUG_FILE("	//GET_PLAYER_PED_POSITION_FOR_SCENE()")SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("CASE ")SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(sPassedScene.sScene.eScene))
		SAVE_STRING_TO_DEBUG_FILE("		vCreateCoords = ")SAVE_VECTOR_TO_DEBUG_FILE(vCreateCoords)
		SAVE_STRING_TO_DEBUG_FILE("			fCreateHead = ")SAVE_FLOAT_TO_DEBUG_FILE(vCreateRot.z)
		SAVE_STRING_TO_DEBUG_FILE("		tRoom = \"")SAVE_STRING_TO_DEBUG_FILE(sPassedScene.sScene.tCreateRoom)
		SAVE_STRING_TO_DEBUG_FILE("\"	RETURN TRUE BREAK")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		//
		IF vCreateRot.x <> 0
		OR vCreateRot.y <> 0
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("vCreateRot = <<")SAVE_FLOAT_TO_DEBUG_FILE(vCreateRot.x)SAVE_STRING_TO_DEBUG_FILE(", ")SAVE_FLOAT_TO_DEBUG_FILE(vCreateRot.y)SAVE_STRING_TO_DEBUG_FILE(", fCreateHead>>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
		
		//
		IF DOES_ENTITY_EXIST(g_pScene_buddy)
			
			IF NOT IS_ENTITY_DEAD(g_pScene_buddy)
				sPassedScene.vSceneBuddyCoordOffset = GET_ENTITY_COORDS(g_pScene_buddy) - vCreateCoords
				sPassedScene.fSceneBuddyHeadOffset = GET_ENTITY_HEADING(g_pScene_buddy) - fCreateHead
				
				IF sPassedScene.fSceneBuddyHeadOffset > 180.0
					sPassedScene.fSceneBuddyHeadOffset -= 360.0
				ENDIF
				IF sPassedScene.fSceneBuddyHeadOffset < -180.0
					sPassedScene.fSceneBuddyHeadOffset += 360.0
				ENDIF
				
			ENDIF
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("InitialiseBuddy(")SAVE_VECTOR_TO_DEBUG_FILE(sPassedScene.vSceneBuddyCoordOffset)
			SAVE_STRING_TO_DEBUG_FILE(", ")SAVE_FLOAT_TO_DEBUG_FILE(sPassedScene.fSceneBuddyHeadOffset)
			SAVE_STRING_TO_DEBUG_FILE(")")SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
		IF NOT IS_ENTITY_DEAD(scene_veh)
			
			IF DOES_ENTITY_EXIST(scene_veh)
				vVehCoordsOffset = GET_ENTITY_COORDS(scene_veh) - vCreateCoords
				fVehHeadOffset = GET_ENTITY_HEADING(scene_veh) - fCreateHead
				
				IF fVehHeadOffset > 180.0
					fVehHeadOffset -= 360.0
				ENDIF
				IF fVehHeadOffset < -180.0
					fVehHeadOffset += 360.0
				ENDIF
				
			ENDIF
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("vVehCoordsOffset = ")SAVE_VECTOR_TO_DEBUG_FILE(vVehCoordsOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("fVehHeadOffset = ")SAVE_FLOAT_TO_DEBUG_FILE(fVehHeadOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
		IF NOT IS_ENTITY_DEAD(playerProp)
		
		
				
			IF PropRotation.z > 180.0
				PropRotation.z -= 360.0
			ENDIF
			IF PropRotation.z < -180.0
				PropRotation.z += 360.0
			ENDIF
		
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("PropOffset = ")SAVE_VECTOR_TO_DEBUG_FILE(PropOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("PropRotation = ")SAVE_VECTOR_TO_DEBUG_FILE(PropRotation)SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
		IF NOT IS_ENTITY_DEAD(playerExtraProp)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("ExtraPropOffset = ")SAVE_VECTOR_TO_DEBUG_FILE(ExtraPropOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("ExtraPropRotation = ")SAVE_VECTOR_TO_DEBUG_FILE(ExtraPropRotation)SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
		
		SAVE_STRING_TO_DEBUG_FILE("gpCamHead = ")SAVE_FLOAT_TO_DEBUG_FILE(gpCamHead)
		SAVE_STRING_TO_DEBUG_FILE("		gpCamPitch = ")SAVE_FLOAT_TO_DEBUG_FILE(gpCamPitch)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
	CLOSE_DEBUG_FILE()
	
	
	RETURN TRUE
ENDFUNC


FUNC BOOL Perform_Switch_Demo(PED_REQUEST_SCENE_ENUM &sceneArray[], INT &iSceneWait[],
		BOOL &bPerformDemo, INT &iTempRecentlySelectedScene)
	PED_SCENE_STRUCT sTempSceneData
	INT iSceneCount
	
	/*
//	PED_REQUEST_SCENE_ENUM sceneArray[iMAX_SCENE_COUNT]
	sceneArray[0] = PR_SCENE_M2_SAVEHOUSE0			// In bed with wife. Wife still asleep.
	sceneArray[1] = PR_SCENE_F_AUNT_TRASH			// Taking out his aunts trash
	sceneArray[2] = PR_SCENE_T_CR_PARK				// Drunk in park – puking on fountain
	sceneArray[3] = PR_SCENE_M_TRAFFIC				// Stuck in traffic
	sceneArray[4] = PR_SCENE_F_REC_CENTRE			// Playing basketball at the Rec Centre
	sceneArray[5] = PR_SCENE_T_FIGHTBBUILD			// Trying to start a fight with a body builder 
	sceneArray[6] = PR_SCENE_M_SHOPPING				// Dropping off wife at Beverly Hills shops.
	sceneArray[7] = PR_SCENE_T_STRIPCLUB_in			// Getting ready to leave the stripclub
	sceneArray[8] = PR_SCENE_F_GYM					// In the gym
	sceneArray[9] = PR_SCENE_M2_PHARMACY			// Picking up pills (for his wife/him)
	sceneArray[10] = PR_SCENE_T_CR_POLICE			// Being chased by the police
	//sceneArray[11] = PR_SCENE_F_DRIVETHRU			// Approaching a drivethru
	sceneArray[12-1] = PR_SCENE_M_CYCLING			// Cycling around the city
	sceneArray[13-1] = PR_SCENE_T_CR_CHATEAU		// Chucked out of Chateau. Another hotel or a swanky restaurant. Beverly Hills Hotel?
	//sceneArray[14-1] = PR_SCENE_F_DRAGRACE_A		// Found approaching the four dragrace locations
	sceneArray[15-1-1] = PR_SCENE_T_CR_WAKETRASH	// Waking up in trash
	sceneArray[16-1-1] = PR_SCENE_M2_LUNCH			// Wife will complain and argue as he gets up to leave.
	sceneArray[17-1-1] = PR_SCENE_T_CR_CHASECAR		// Chasing a car along river.
	sceneArray[18-1-1] = PR_SCENE_M_POOLSIDE		// Sunbathing by the pool.
	sceneArray[19-1-1] = PR_SCENE_T_CR_INSULTWOMAN	// Insulting women at the beach
	sceneArray[20-1-1] = PR_SCENE_M_KIDS_TV			// Watching TV with his kids
	
//	INT iSceneWait[iMAX_SCENE_COUNT]
	iSceneWait[0] = 4500-1000
	iSceneWait[1] = 4500-1000	//trash
	iSceneWait[2] = 4500+2000	//park
	iSceneWait[3] = 4500
	iSceneWait[4] = 4500
	iSceneWait[5] = 4500
	iSceneWait[6] = 4500
	iSceneWait[7] = 4500+1500	//strip club
	iSceneWait[8] = 4500
	iSceneWait[9] = 4500
	iSceneWait[10] = 4500
//	iSceneWait[11] = 4500+5000	//drive thru
	iSceneWait[12-1] = 4500
	iSceneWait[13-1] = 4500
//	iSceneWait[14-1] = 4500+2000	//drag race
	iSceneWait[15-1-1] = 4500
	iSceneWait[16-1-1] = 4500
	iSceneWait[17-1-1] = 4500
	iSceneWait[18-1-1] = 4500
	iSceneWait[19-1-1] = 4500
	iSceneWait[20-1-1] = 4500
	*/
	
	INT iScene
	REPEAT COUNT_OF(sceneArray) iScene
		sceneArray[iScene] = INT_TO_ENUM(PED_REQUEST_SCENE_ENUM, iScene)
		iSceneWait[iScene] = GET_RANDOM_INT_IN_RANGE(5000-1500, 5000+1500)
	ENDREPEAT
	
	// To shuffle an array a of n elements (indexes 0..n-1):
	iScene = (COUNT_OF(sceneArray)-1)
	WHILE iScene > 0
		INT j = GET_RANDOM_INT_IN_RANGE(0, iScene+1)	//j ← random integer with 0 ≤ j ≤ i
		
		PED_REQUEST_SCENE_ENUM tempSceneJ = sceneArray[j]
		sceneArray[j] = sceneArray[iScene]				//exchange a[j] and a[i]
		sceneArray[iScene] = tempSceneJ					//exchange a[j] and a[i]
		
		iScene--
	ENDWHILE
	
	
	REPEAT COUNT_OF(sceneArray) iScene
		SAVE_STRING_TO_DEBUG_FILE("sceneArray[")
		SAVE_INT_TO_DEBUG_FILE(iScene)
		SAVE_STRING_TO_DEBUG_FILE("] = ")
		SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(sceneArray[iScene]))
		SAVE_NEWLINE_TO_DEBUG_FILE()
	ENDREPEAT
	
	
	SET_CLOCK_TIME(11, 0, 0)
	
	WHILE iSceneCount < COUNT_OF(sceneArray)
	AND bPerformDemo
		
		IF (sceneArray[iSceneCount] <> PR_SCENE_DEAD)
		AND (sceneArray[iSceneCount] <> PR_SCENE_HOSPITAL)
		AND (sceneArray[iSceneCount] <> PR_SCENE_M_OVERRIDE)
		AND (sceneArray[iSceneCount] <> PR_SCENE_F_OVERRIDE)
		AND (sceneArray[iSceneCount] <> PR_SCENE_T_OVERRIDE)
		AND (sceneArray[iSceneCount] <> PR_SCENE_M_DEFAULT)
		AND (sceneArray[iSceneCount] <> PR_SCENE_F_DEFAULT)
		AND (sceneArray[iSceneCount] <> PR_SCENE_T_DEFAULT)
		
			sTempSceneData.iStage = 0
			sTempSceneData.eScene = sceneArray[iSceneCount]
			sTempSceneData.sSelectorCam.bSplineCreated = FALSE
			
			GET_PLAYER_CHAR_FOR_PED_REQUEST_SCENE(sTempSceneData.eScene, sTempSceneData.ePed)
			
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(sTempSceneData.eScene)
			
			IF IS_PED_THE_CURRENT_PLAYER_PED(sTempSceneData.ePed)
				SELECTOR_SLOTS_ENUM newSelectorPed
				SWITCH sTempSceneData.ePed
					CASE CHAR_MICHAEL
						IF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
							newSelectorPed = SELECTOR_PED_FRANKLIN
						ELSE
							newSelectorPed = SELECTOR_PED_TREVOR
						ENDIF
					BREAK
					CASE CHAR_FRANKLIN
						IF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
							newSelectorPed = SELECTOR_PED_MICHAEL
						ELSE
							newSelectorPed = SELECTOR_PED_TREVOR
						ENDIF
					BREAK
					CASE CHAR_TREVOR
						IF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
							newSelectorPed = SELECTOR_PED_MICHAEL
						ELSE
							newSelectorPed = SELECTOR_PED_FRANKLIN
						ENDIF
					BREAK
				ENDSWITCH
				
				WHILE NOT SET_CURRENT_SELECTOR_PED(newSelectorPed)
					WAIT(0)
				ENDWHILE
			ENDIF
			
			BOOL bPerformingGenericPedScene = TRUE
			WHILE IS_PLAYER_PLAYING(PLAYER_ID())
			AND bPerformingGenericPedScene
				IF NOT GENERIC_PED_SCENE(sTempSceneData,
					&SCENE_CUSTOM_SCRIPT_REQUEST,
					&SCENE_CUSTOM_SCRIPT_SETUP)
					iTempRecentlySelectedScene = ENUM_TO_INT(g_eRecentlySelectedScene)
					PRINTSTRING("bPerformDemo sTempSceneData[")PRINTINT(iSceneCount)
					PRINTSTRING("].iStage: ")PRINTINT(sTempSceneData.iStage)PRINTNL()
				ELSE
					bPerformingGenericPedScene = FALSE
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			WAIT(iSceneWait[iSceneCount])
			
			SAVE_STRING_TO_DEBUG_FILE("scene ")
			SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(sceneArray[iSceneCount]))
			SAVE_STRING_TO_DEBUG_FILE(": ")

			IF IS_PLAYER_PLAYING(PLAYER_ID())
			VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
			FLOAT fPlayerCoord_ground_z = 0.0
			IF GET_GROUND_Z_FOR_3D_COORD(vPlayerCoord, fPlayerCoord_ground_z)
				SAVE_STRING_TO_DEBUG_FILE("diff = ")
				SAVE_FLOAT_TO_DEBUG_FILE(fPlayerCoord_ground_z-vPlayerCoord.z)
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("NO GROUND!!!")
			ENDIF
			ELSE
			SAVE_STRING_TO_DEBUG_FILE("dead????")
			ENDIF

			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
		
		iSceneCount++
		
		WAIT(0)
	ENDWHILE
	
	iSceneCount = 0
	bPerformDemo = FALSE
	
	RETURN FALSE
ENDFUNC	


#IF NOT USE_SP_DLC

FUNC BOOL Get_Average_Of_Mission_Array(WIDGET_GROUP_ID sceneWidget, BOOL &bGetAverageOfMissionArray)
	
	INT iCoord
	
	CONST_INT iCONST_EXILE_MISSIONS	5
	STATIC_BLIP_NAME_ENUM eExileMissions[iCONST_EXILE_MISSIONS]
	eExileMissions[0] = (STATIC_BLIP_MISSION_EXILE_1)		//"exile1"
	eExileMissions[1] = (STATIC_BLIP_MISSION_EXILE_2)		//"exile2"
	eExileMissions[2] = (STATIC_BLIP_MISSION_EXILE_3)		//"exile3"
	
	eExileMissions[3] = (STATIC_BLIP_MISSION_RURAL_BANK)	//"rural_bank_setup"
	eExileMissions[4] = (STATIC_BLIP_MISSION_RURAL_BANK)	//"rural_bank_heist"
	
	VECTOR vExileMissions[iCONST_EXILE_MISSIONS]
	
	VECTOR vExileMissionCentre
	FLOAT fExileMissionMaxDist
	FLOAT fExileMissionMinDist
	
	BLIP_INDEX exileMissionCentreBlip, ExileMissionBlips[iCONST_EXILE_MISSIONS]
	
	SET_CURRENT_WIDGET_GROUP(sceneWidget)
	WIDGET_GROUP_ID missionArrayAverage_widget = START_WIDGET_GROUP("missionArrayAverage_widget")
		ADD_WIDGET_BOOL("bGetAverageOfMissionArray", bGetAverageOfMissionArray)
		
		ADD_WIDGET_VECTOR_SLIDER("vExileMissionCentre", vExileMissionCentre, -4000.0, 4000.0, 0.0)
		ADD_WIDGET_FLOAT_SLIDER("fExileMissionMaxDist", fExileMissionMaxDist, 0.0, 4000.0, 0.0)
		ADD_WIDGET_FLOAT_SLIDER("fExileMissionMinDist", fExileMissionMinDist, 0.0, 4000.0, 0.0)
	STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(sceneWidget)
	
	WHILE bGetAverageOfMissionArray
		
		
		
		vExileMissionCentre = <<0,0,0>>
		REPEAT iCONST_EXILE_MISSIONS iCoord
			vExileMissions[iCoord] = GET_STATIC_BLIP_POSITION(eExileMissions[iCoord])
			vExileMissionCentre += vExileMissions[iCoord]
			
			IF NOT DOES_BLIP_EXIST(ExileMissionBlips[iCoord])
				ExileMissionBlips[iCoord] = ADD_BLIP_FOR_COORD(vExileMissions[iCoord])
				
				SET_BLIP_SPRITE(ExileMissionBlips[iCoord], RADAR_TRACE_HEIST)
				SET_BLIP_NAME_FROM_ASCII(ExileMissionBlips[iCoord], DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(eExileMissions[iCoord])))
			ELSE
				SET_BLIP_COORDS(ExileMissionBlips[iCoord], vExileMissions[iCoord])
			ENDIF
			
		ENDREPEAT
		vExileMissionCentre /= TO_FLOAT(iCONST_EXILE_MISSIONS)
		IF NOT DOES_BLIP_EXIST(exileMissionCentreBlip)
			exileMissionCentreBlip = ADD_BLIP_FOR_COORD(vExileMissionCentre)
			SET_BLIP_COLOUR(exileMissionCentreBlip, BLIP_COLOUR_RED)
		ELSE
			SET_BLIP_COORDS(exileMissionCentreBlip, vExileMissionCentre)
		ENDIF
			
		fExileMissionMaxDist = 0.0
		fExileMissionMinDist = 4000.0
		REPEAT iCONST_EXILE_MISSIONS iCoord
			FLOAT fDistance = VDIST(vExileMissionCentre, vExileMissions[iCoord])
			
			IF (fExileMissionMaxDist < fDistance)
				fExileMissionMaxDist = fDistance
			ENDIF
			IF (fExileMissionMinDist > fDistance)
				fExileMissionMinDist = fDistance
			ENDIF
			
		ENDREPEAT
		
		WAIT(0)
	ENDWHILE
	IF DOES_BLIP_EXIST(exileMissionCentreBlip)
		REMOVE_BLIP(exileMissionCentreBlip)
	ENDIF
	REPEAT iCONST_EXILE_MISSIONS iCoord
		IF DOES_BLIP_EXIST(ExileMissionBlips[iCoord])
			REMOVE_BLIP(ExileMissionBlips[iCoord])
		ENDIF
	ENDREPEAT
	
	IF DOES_WIDGET_GROUP_EXIST(missionArrayAverage_widget)
		DELETE_WIDGET_GROUP(missionArrayAverage_widget)
	ENDIF

	bGetAverageOfMissionArray = FALSE
	RETURN FALSE
ENDFUNC

#ENDIF

FUNC BOOL DEBUG_GetPlayerSwitchEstablishingShot(STRING tCreateRoom, STRING tPlayerSceneEstabShotSuffix,
		VECTOR &Position, VECTOR &Orientation, FLOAT &Fov)
	
	TEXT_LABEL_63 tEstabShot
	tEstabShot = tCreateRoom
	tEstabShot += tPlayerSceneEstabShotSuffix
	
	IF IS_STRING_NULL_OR_EMPTY(tEstabShot)
		RETURN FALSE
	ENDIF
	
	STRING sFileName = "playerswitchestablishingshots.xml"	//X:\gta5\build\dev\common\data\playerswitchestablishingshots.meta
	IF LOAD_XML_FILE(sFileName)
		
		BOOL bFoundShot = FALSE
		
		INT eachNode
		REPEAT GET_NUMBER_OF_XML_NODES() eachNode
			
			SWITCH GET_HASH_KEY(GET_XML_NODE_NAME())
				CASE HASH("Name")
					bFoundShot = FALSE
					
					STRING Name
					Name = GET_STRING_FROM_XML_NODE()
					
					IF ARE_STRINGS_EQUAL(tEstabShot, Name)
						bFoundShot = TRUE
					ENDIF
				BREAK
				CASE HASH("Position")
					IF bFoundShot
						INT posAttribute
						REPEAT GET_NUMBER_OF_XML_NODE_ATTRIBUTES() posAttribute
	                        SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(posAttribute))
	                            CASE HASH("x")		Position.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(posAttribute) BREAK
	                            CASE HASH("y")		Position.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(posAttribute) BREAK
	                            CASE HASH("z")		Position.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(posAttribute) BREAK
	                        ENDSWITCH
	                    ENDREPEAT
					ENDIF
					
				BREAK
				CASE HASH("Orientation")
					IF bFoundShot
						INT oriAttribute
						REPEAT GET_NUMBER_OF_XML_NODE_ATTRIBUTES() oriAttribute
	                        SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(oriAttribute))
	                            CASE HASH("x")		Orientation.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(oriAttribute) BREAK
	                            CASE HASH("y")		Orientation.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(oriAttribute) BREAK
	                            CASE HASH("z")		Orientation.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(oriAttribute) BREAK
	                        ENDSWITCH
	                    ENDREPEAT
					ENDIF
					
				BREAK
				CASE HASH("Fov")
					IF bFoundShot
						INT fovAttribute
						REPEAT GET_NUMBER_OF_XML_NODE_ATTRIBUTES() fovAttribute
	                        SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(fovAttribute))
	                            CASE HASH("value")	Fov = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(fovAttribute) BREAK
	                        ENDSWITCH
	                    ENDREPEAT
						
						DELETE_XML_FILE()
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
			
			GET_NEXT_XML_NODE()
			
		ENDREPEAT
		
		Position	= <<0,0,0>>
		Orientation	= <<0,0,0>>
		Fov			= 0.0
		
		DELETE_XML_FILE()
		RETURN FALSE
	ENDIF
	
	Position	= <<0,0,0>>
	Orientation	= <<0,0,0>>
	Fov			= 0.0
	
	DELETE_XML_FILE()
	RETURN FALSE
ENDFUNC

#ENDIF
