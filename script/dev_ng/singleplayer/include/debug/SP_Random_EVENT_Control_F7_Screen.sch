USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"

USING "random_events_public.sch"

USING "player_ped_debug.sch"
#if USE_CLF_DLC
USING "player_scene_scheduleCLF.sch"	
#endif
#if  USE_NRM_DLC
USING "player_scene_scheduleNRM.sch"
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
USING "player_scene_schedule.sch"
#endif
#endif


// *********************************************************************************************************************
// *********************************************************************************************************************
// *********************************************************************************************************************
//
//		MISSION NAME	:	SP_Random_EVENT_Control_F11_Screen.sch
//		AUTHOR			:	Paul (from Alwyn's SP_Family_Control_F8_Screen, from Keith's SP_Mission_Flow_F9_Screen)
//		DESCRIPTION		:	Updates the F11 Screen display.
//
// *********************************************************************************************************************
// *********************************************************************************************************************
// *********************************************************************************************************************

// Standard F11 Screen Text Scaling and Positioning Values

CONST_FLOAT	F11_TEXT_SCALE_X				0.25
CONST_FLOAT	F11_TEXT_SCALE_Y				0.3
CONST_FLOAT	F11_TEXT_WRAP_XPOS_START		0.0
CONST_FLOAT	F11_TEXT_WRAP_XPOS_END			1.0

CONST_FLOAT	F11_START_YPOS					0.075
CONST_FLOAT	F11_TITLE_HEIGHT				0.04
CONST_FLOAT	F11_TOP_LINE_HEIGHT 			0.04

CONST_FLOAT	F11_ADD_Y						0.02


CONST_FLOAT F11_TITLE_CONTROLLER_XPOS		0.135

CONST_FLOAT	F11_EVENT_NAME_XPOS				0.05
CONST_FLOAT	F11_LOCK_STATUS_XPOS			0.2
CONST_FLOAT	F11_EVENT_COMPLETE_XPOS			0.26
CONST_FLOAT	F11_PLAYER_PED_XPOS				0.36
CONST_FLOAT	F11_EVENT_COOLDOWN_XPOS			0.53

CONST_FLOAT F11_VAR_NAME_XPOS				0.075
CONST_FLOAT F11_VAR_COMPLETE_XPOS			0.225

// *******************************************************************************************

BOOL		bExpanded[MAX_RANDOM_EVENTS]
INT 		iCurrentSelection

// *******************************************************************************************
//		Standard F11 Screen Display Controls
// *******************************************************************************************

// *******************************************************************************************

// PURPOSE: Display the re-sizeable background for the general F11 screen
// 
PROC Display_F11_Background()

	// Calculate the background rectangle height
	FLOAT	textBoxYSize	= F11_ADD_Y * (ENUM_TO_INT(MAX_RANDOM_EVENTS))
	SP_RANDOM_EVENTS iLoop
	REPEAT MAX_RANDOM_EVENTS iLoop
		IF GET_RANDOM_EVENT_NUMBER_OF_VARIATIONS(iLoop) > 1
			IF bExpanded[ENUM_TO_INT(iLoop)]
				textBoxYSize += F11_ADD_Y * GET_RANDOM_EVENT_NUMBER_OF_VARIATIONS(iLoop)
			ENDIF
		ENDIF
	ENDREPEAT
	
	textBoxYSize += F11_TOP_LINE_HEIGHT
	
	FLOAT	boxYBorder		= F11_START_YPOS + (F11_ADD_Y * 0.5)
	FLOAT	totalBoxYSize	= textBoxYSize + boxYBorder
	
	FLOAT	boxYCentre		= totalBoxYSize * 0.5
	
	DRAW_RECT(0.5, boxYCentre, 1.0, totalBoxYSize, 0, 0, 0, 175)
	
ENDPROC

// *******************************************************************************************
//		Interface With Native Commands
// *******************************************************************************************

// PURPOSE:	Set up the standard text display used for the F11 screen
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B

PROC Setup_F11_Text_Display(HUD_COLOURS paramColour)

	INT theR, theG, theB, theA
	GET_HUD_COLOUR(paramColour, theR, theG, theB, theA)
	SET_TEXT_COLOUR(theR, theG, theB, theA)
	
	SET_TEXT_SCALE(F11_TEXT_SCALE_X, F11_TEXT_SCALE_Y)
	SET_TEXT_WRAP(F11_TEXT_WRAP_XPOS_START, F11_TEXT_WRAP_XPOS_END)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)

ENDPROC

// *******************************************************************************************
//		Generic F11 Screen Display Commands
// *******************************************************************************************

// PURPOSE:	Set up the standard literal text display used for the F11 screen and display text with it
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B
//					paramXPos			Text X pos
//					paramYPos			Text Y pos
//					paramText			The literal string to be output

PROC Display_F11_Literal_Text(HUD_COLOURS paramColour, FLOAT paramXPos, FLOAT paramYPos, STRING paramText)

	Setup_F11_Text_Display(paramColour)
	DISPLAY_TEXT_WITH_LITERAL_STRING(paramXPos, paramYPos, "STRING", paramText)

ENDPROC

// PURPOSE:	Set up the standard textLabel text display
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B
//					paramXPos			Text X pos
//					paramYPos			Text Y pos
//					paramTextLabel		The textLabel to be output

PROC Display_F11_TextLabel(HUD_COLOURS paramColour, FLOAT paramXPos, FLOAT paramYPos, STRING paramTextLabel)

	Setup_F11_Text_Display(paramColour)
	DISPLAY_TEXT(paramXPos, paramYPos, paramTextLabel)

ENDPROC

// PURPOSE:	Set up the standard textLabel text display with 1 number used for the F11 screen
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B
//					paramXPos			Text X pos
//					paramYPos			Text Y pos
//					paramTextLabel		The textLabel to be output
//					paramInt			The first number

PROC Display_F11_TextLabel_With_One_Number(HUD_COLOURS paramColour, FLOAT paramXPos, FLOAT paramYPos, STRING paramTextLabel, INT paramInt)

	Setup_F11_Text_Display(paramColour)
	DISPLAY_TEXT_WITH_NUMBER(paramXPos, paramYPos, paramTextLabel, paramInt)

ENDPROC

// PURPOSE:	Set up the standard textLabel text display with 2 numbers used for the F11 screen
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B
//					paramXPos			Text X pos
//					paramYPos			Text Y pos
//					paramTextLabel		The textLabel to be output
//					paramInt1			The first number
//					paramInt2			The second number

PROC Display_F11_TextLabel_With_Two_Numbers(HUD_COLOURS paramColour, FLOAT paramXPos, FLOAT paramYPos, STRING paramTextLabel, INT paramInt1, INT paramInt2)

	Setup_F11_Text_Display(paramColour)
	DISPLAY_TEXT_WITH_2_NUMBERS(paramXPos, paramYPos, paramTextLabel, paramInt1, paramInt2)

ENDPROC


// *******************************************************************************************
//	Specific F11 Family Screen Command Display Messages
// *******************************************************************************************

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_F11_Random_event_name(SP_RANDOM_EVENTS eRandomEvent, FLOAT paramYPos, HUD_COLOURS paramColour)

	STRING pReName
	
	IF GET_RANDOM_EVENT_NUMBER_OF_VARIATIONS(eRandomEvent) = 1
		pReName = GET_RANDOM_EVENT_NAME_LABEL(eRandomEvent)
		Display_F11_TextLabel(paramColour, F11_EVENT_NAME_XPOS, paramYPos, pReName)
	ELSE
		pReName = GET_RANDOM_EVENT_GROUP_NAME(eRandomEvent)
		Display_F11_Literal_Text(paramColour, F11_EVENT_NAME_XPOS, paramYPos, pReName)
	ENDIF
	
ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_F11_Random_Event_Lock_status(SP_RANDOM_EVENTS eRandomEvent, FLOAT paramYPos, HUD_COLOURS paramColour)

	STRING pLockState
	IF IS_RANDOM_EVENT_AVAILABLE(eRandomEvent)
		pLockState = "F11_REUNLOCKED"
	ELSE
		pLockState = "F11_RELOCKED"
	ENDIF
	
	Display_F11_TextLabel(paramColour, F11_LOCK_STATUS_XPOS, paramYPos, pLockState)
	
ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_F11_Random_Event_Availability_for_player_peds(SP_RANDOM_EVENTS eRandomEvent, FLOAT paramYPos, HUD_COLOURS paramColour)

	TEXT_LABEL_63 pAvailable = "Available to "
	
	IF SHOULD_RANDOM_EVENT_START_FOR_PLAYER_PED(eRandomEvent, -1, CHAR_MICHAEL)
	AND SHOULD_RANDOM_EVENT_START_FOR_PLAYER_PED(eRandomEvent, -1, CHAR_FRANKLIN)
	AND SHOULD_RANDOM_EVENT_START_FOR_PLAYER_PED(eRandomEvent, -1, CHAR_TREVOR)
		pAvailable += "All"
	ELSE
		IF SHOULD_RANDOM_EVENT_START_FOR_PLAYER_PED(eRandomEvent, -1, CHAR_MICHAEL)
			pAvailable += "Michael "
		ENDIF
		IF SHOULD_RANDOM_EVENT_START_FOR_PLAYER_PED(eRandomEvent, -1, CHAR_FRANKLIN)
			pAvailable += "Franklin "
		ENDIF
		IF SHOULD_RANDOM_EVENT_START_FOR_PLAYER_PED(eRandomEvent, -1, CHAR_TREVOR)
			pAvailable += "Trevor "
		ENDIF
	ENDIF
	
	Display_F11_Literal_Text(paramColour, F11_PLAYER_PED_XPOS, paramYPos, pAvailable)
	
ENDPROC

PROC Display_F11_Random_Event_Ready_Status(SP_RANDOM_EVENTS eRandomEvent, FLOAT paramYPos, HUD_COLOURS paramColour)

	TEXT_LABEL_63 pAvailable
	
	IF IS_NOW_AFTER_TIMEOFDAY(g_savedGlobals.sRandomEventData.eTimeBlockUntil[eRandomEvent])
		pAvailable = "Available to launch"
	ELSE
		pAvailable = "Will be available in "
		INT iMinutes
		INT iHours
		INT iDays
		INT iRest
		GET_DIFFERENCE_BETWEEN_NOW_AND_TIMEOFDAY(g_savedGlobals.sRandomEventData.eTimeBlockUntil[eRandomEvent], iRest, iMinutes, iHours, iDays, iRest, iRest)
		IF iDays > 0
			pAvailable += iDays
			pAvailable += " Days "
		ENDIF
		IF iHours > 0
			pAvailable += iHours
			pAvailable += " Hours "
		ENDIF
		IF iMinutes > 0
			pAvailable += iMinutes
			pAvailable += " Minutes"
		ENDIF
	ENDIF
	
	Display_F11_Literal_Text(paramColour, F11_EVENT_COOLDOWN_XPOS, paramYPos, pAvailable)
	
ENDPROC

PROC Display_F11_Random_Event_Completion(SP_RANDOM_EVENTS eRandomEvent, FLOAT paramYPos, HUD_COLOURS paramColour)

	TEXT_LABEL_63 pComplete
	
	IF IS_RANDOM_EVENT_COMPLETE(eRandomEvent)
		pComplete = "F11_RECOMP"
	ELSE
		pComplete = "F11_REINCOMP"
	ENDIF
	
	Display_F11_TextLabel(paramColour, F11_EVENT_COMPLETE_XPOS, paramYPos, pComplete)
	
ENDPROC

// *******************************************************************************************
//	F11 Screen Display
// *******************************************************************************************

// PURPOSE:	Display the titles for the different sections of the F11 screen
// 
PROC Display_F11_Titles()
	Display_F11_Literal_Text(HUD_COLOUR_GREEN, F11_EVENT_NAME_XPOS, F11_TITLE_HEIGHT - F11_ADD_Y, "RANDOM EVENTS DEBUG DISPLAY")
ENDPROC

PROC Display_F11_Single_Random_Event(SP_RANDOM_EVENTS eRandomEvent, FLOAT paramYPos, HUD_COLOURS paramColour)

	Display_F11_Random_event_name(eRandomEvent, paramYPos, paramColour)
	Display_F11_Random_Event_Lock_status(eRandomEvent, paramYPos, paramColour)
	Display_F11_Random_Event_Availability_for_player_peds(eRandomEvent, paramYPos, paramColour)
	Display_F11_Random_Event_Ready_Status(eRandomEvent, paramYPos, paramColour)
	
	IF GET_RANDOM_EVENT_NUMBER_OF_VARIATIONS(eRandomEvent) <= 1
		Display_F11_Random_Event_Completion(eRandomEvent, paramYPos, paramColour)
	ELSE 
		IF NOT bExpanded[eRandomEvent]
			Display_F11_Literal_Text(paramColour, F11_EVENT_NAME_XPOS - 0.01, paramYPos, "+")
		ELSE
			Display_F11_Literal_Text(paramColour, F11_EVENT_NAME_XPOS - 0.01, paramYPos, "-")
		ENDIF
	ENDIF
	
ENDPROC

PROC Display_F11_Random_Event_Variation_Name(SP_RANDOM_EVENTS eRandomEvent, INT iVariation, FLOAT paramYPos, HUD_COLOURS paramColour)

	STRING pReVarName
	pReVarName = GET_RANDOM_EVENT_NAME_LABEL(eRandomEvent, iVariation)
	Display_F11_TextLabel(paramColour, F11_VAR_NAME_XPOS, paramYPos, pReVarName)
	
ENDPROC

PROC Display_F11_Random_Event_Variation_Complete(SP_RANDOM_EVENTS eRandomEvent, INT iVariation, FLOAT paramYPos, HUD_COLOURS paramColour)
	STRING pReVarComplete
	IF IS_RANDOM_EVENT_COMPLETE(eRandomEvent, iVariation)
		pReVarComplete = "F11_VCOMP"
	ELSE
		pReVarComplete = "F11_VINCOMP"
	ENDIF
	Display_F11_TextLabel(paramColour, F11_VAR_COMPLETE_XPOS, paramYPos, pReVarComplete)
ENDPROC

PROC Display_F11_Random_Event_Variation(SP_RANDOM_EVENTS eRandomEvent, INT iVariation, FLOAT paramYPos, HUD_COLOURS paramColour)
	Display_F11_Random_Event_Variation_Name(eRandomEvent, iVariation, paramYPos, paramColour)
	Display_F11_Random_Event_Variation_Complete(eRandomEvent, iVariation, paramYPos, paramColour)
ENDPROC

PROC Display_F11_Random_Event_Top_Line()

	IF (GET_GAME_TIMER() - g_iLastRandomEventLaunch) < iMinTimeBetweenLaunches
	
		INT miliseconds =  iMinTimeBetweenLaunches - (GET_GAME_TIMER() - g_iLastRandomEventLaunch)
		
		INT seconds = (miliseconds/1000) % 60
		INT minutes = (miliseconds/60000) % 60
		INT hours = (miliseconds/3600000) % 24
		
		TEXT_LABEL_63 sTime = "Delay set - "
		
		IF hours < 10
			sTime += "0"
		ENDIF
		sTime += hours 
		sTime += ":"
		
		IF minutes < 10
			sTime += "0"
		ENDIF
		sTime += minutes
		sTime += ":"
		
		IF seconds < 10
			sTime += "0"
		ENDIF
		sTime += seconds
		
		sTime += "  Hit 1 on num pad to clear"
		
		Display_F11_Literal_Text(HUD_COLOUR_GREENLIGHT, F11_EVENT_NAME_XPOS, F11_TITLE_HEIGHT + F11_TOP_LINE_HEIGHT - 0.01, sTime)
	ELSE
		Display_F11_Literal_Text(HUD_COLOUR_GREEN, F11_EVENT_NAME_XPOS, F11_TITLE_HEIGHT + F11_TOP_LINE_HEIGHT - 0.01, "Ready to Launch")
	ENDIF
	
	TEXT_LABEL_63 sTemp
	
	IF g_eCurrentRandomEvent <> RE_NONE
		sTemp = GET_RANDOM_EVENT_DISPLAY_STRING_FROM_ID(g_eCurrentRandomEvent)
		IF GET_RANDOM_EVENT_FLAG()
			sTemp += " has launched and is activated"
		ELSE
			sTemp += " has launched, awaiting activation"
		ENDIF
	ELSE
		sTemp = "No Random Event launched."
	ENDIF
	
	Display_F11_Literal_Text(HUD_COLOUR_GREEN, F11_EVENT_COMPLETE_XPOS + 0.02, F11_TITLE_HEIGHT + F11_TOP_LINE_HEIGHT - 0.01, sTemp)
	
ENDPROC

// PURPOSE:	Display the details for all random events
// 
PROC Display_F11_All_Random_Events()

	FLOAT theY = F11_START_YPOS + F11_TOP_LINE_HEIGHT
	SP_RANDOM_EVENTS loopAsStrandID
	REPEAT MAX_RANDOM_EVENTS loopAsStrandID
		HUD_COLOURS reHudColour
		IF IS_RANDOM_EVENT_AVAILABLE(loopAsStrandID)
			IF GET_ACTIVE_RANDOM_EVENT() = loopAsStrandId
				reHudColour = HUD_COLOUR_GREEN
			ELSE
				reHudColour = HUD_COLOUR_GREY
			ENDIF
		ELSE
			reHudColour = HUD_COLOUR_REDDARK
		ENDIF
		IF iCurrentSelection = ENUM_TO_INT(loopAsStrandID)
			IF IS_RANDOM_EVENT_AVAILABLE(loopAsStrandID)
				IF GET_ACTIVE_RANDOM_EVENT() = loopAsStrandId
					reHudColour = HUD_COLOUR_GREENLIGHT
				ELSE
					reHudColour = HUD_COLOUR_WHITE
				ENDIF
			ELSE
				reHudColour = HUD_COLOUR_REDLIGHT
			ENDIF
		ENDIF
		Display_F11_Single_Random_Event(loopAsStrandID, theY, reHudColour)
		theY += F11_ADD_Y
		IF bExpanded[loopAsStrandId]
			IF GET_RANDOM_EVENT_NUMBER_OF_VARIATIONS(loopAsStrandID) > 1
				INT iCOuntVar
				FOR iCountVar = 1 TO GET_RANDOM_EVENT_NUMBER_OF_VARIATIONS(loopAsStrandID)
					Display_F11_Random_Event_Variation(loopAsStrandID, iCountVar, theY, reHudColour)
					theY += F11_ADD_Y
				ENDFOR
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//// *******************************************************************************************
////	F11 Select Debug Display
//// *******************************************************************************************
///    


PROC Run_Random_Event_Selection_Control()
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
		IF iCurrentSelection < ENUM_TO_INT(MAX_RANDOM_EVENTS) -1
			iCurrentSelection++
		ENDIF
	ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
		IF iCurrentSelection > 0
			iCurrentSelection--
		ELSE
			iCurrentSelection = ENUM_TO_INT(MAX_RANDOM_EVENTS) -1
		ENDIF
	ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN)
		IF GET_RANDOM_EVENT_NUMBER_OF_VARIATIONS(INT_TO_ENUM(SP_RANDOM_EVENTS, iCurrentSelection)) > 1
			bExpanded[iCurrentSelection] = ! bExpanded[iCurrentSelection]
			INT iTemp
			REPEAT MAX_RANDOM_EVENTS iTemp
				IF iTemp != iCurrentSelection
					bExpanded[iTemp] = FALSE
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

// *******************************************************************************************
//	F11 Select Debug Display
// *******************************************************************************************

// PURPOSE:	Displays the Random Event F11 screen when allowed by other systems
// 
PROC Display_Random_Event_F11_Screen()

	// Check if some other system has hidden this screen
	IF NOT g_flowUnsaved.bShowMissionFlowDebugScreen
		g_iDebugSelectedFriendConnDisplay = 0
		g_bDrawLiteralSceneString = FALSE
		EXIT
	ENDIF
	
	Display_F11_Background()
	Display_F11_Titles()
	Display_F11_Random_Event_Top_Line()
	Display_F11_All_Random_Events()
	Run_Random_Event_Selection_Control()
	
	g_bDrawLiteralSceneString = TRUE
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
		g_iLastRandomEventLaunch = GET_GAME_TIMER() - iMinTimeBetweenLaunches
		g_iLastSuccessfulAmbientLaunchTime = GET_GAME_TIMER() - iMinTimeBetweenLaunches
	ENDIF
	
ENDPROC
