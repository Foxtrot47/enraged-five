USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"

USING "family_public.sch"

USING "player_ped_debug.sch"

#if USE_CLF_DLC
USING "player_scene_scheduleCLF.sch"	
#endif
#if  USE_NRM_DLC
USING "player_scene_scheduleNRM.sch"
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
USING "player_scene_schedule.sch"
#endif
#endif


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	SP_Family_Control_F8_Screen.sch
//		AUTHOR			:	Alwyn (from Keiths SP_Mission_Flow_F9_Screen)
//		DESCRIPTION		:	Updates the F8 Screen display.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// Standard F8 Screen Text Scaling and Positioning Values

CONST_FLOAT	F8_TEXT_SCALE_X					0.25
CONST_FLOAT	F8_TEXT_SCALE_Y					0.3
CONST_FLOAT	F8_TEXT_WRAP_XPOS_START			0.0
CONST_FLOAT	F8_TEXT_WRAP_XPOS_END			1.0
CONST_FLOAT	F8_START_YPOS					0.075
CONST_FLOAT	F8_ADD_Y						0.02

CONST_INT	F8_SEPERATOR_COUNT				4				//3

CONST_FLOAT F8_TITLE_CONTROLLER_XPOS		0.135
CONST_FLOAT F8_TITLE_SWITCH_XPOS			0.635			//0.5+F8_TITLE_CONTROLLER_XPOS

CONST_FLOAT	F8_FAM_NAME_XPOS				0.055
CONST_FLOAT	F8_FAM_CURRENT_EVENT			0.205			//0.185	//0.135	//F8_DESCRIPTION_XPOS(0.235)
CONST_FLOAT	F8_FAM_PREVIOUS_EVENT			0.365			//F8_PARAM1_XPOS

CONST_FLOAT	F8_PLA_NAME_XPOS				0.555			//0.5+F8_FAM_NAME_XPOS
CONST_FLOAT	F8_PLA_CURRENT_SCENE_XPOS		0.655			//0.705			//0.5+F8_FAM_CURRENT_EVENT
CONST_FLOAT	F8_PLA_PREVIOUS_SCENE_XPOS		0.815			//0.865			//0.5+F8_FAM_PREVIOUS_EVENT

CONST_FLOAT	F8_PLA_INDENT_XPOS				0.580			//0.5+F8_PLA_NAME_XPOS


// *******************************************************************************************

BOOL		bChangeDebugF8SelectedEvent

BLIP_INDEX blipPlayerChars[NUM_OF_PLAYABLE_PEDS]
enumCharacterList eCurrentPlayerChar = INT_TO_ENUM(enumCharacterList, 0)
PED_REQUEST_SCENE_ENUM eCurrentPlayerEvents[NUM_OF_PLAYABLE_PEDS]
FLOAT fCurrentPlayerScenePercent[NUM_OF_PLAYABLE_PEDS]


// *******************************************************************************************
//		Standard F8 Screen Display Controls
// *******************************************************************************************


// *******************************************************************************************

// PURPOSE: Display the re-sizeable background for the general F8 screen
// 
PROC Display_F8_Background()

	// Calculate the background rectangle height
	FLOAT	textBoxYSize	= F8_ADD_Y * (ENUM_TO_INT(MAX_FAMILY_MEMBER)+F8_SEPERATOR_COUNT)
	FLOAT	boxYBorder		= F8_START_YPOS + (F8_ADD_Y * 0.5)
	FLOAT	totalBoxYSize	= textBoxYSize + boxYBorder
	
	FLOAT	boxYCentre		= totalBoxYSize * 0.5
	
	DRAW_RECT(0.5, boxYCentre, 1.0, totalBoxYSize, 0, 0, 0, 175)

ENDPROC


// *******************************************************************************************
//		Interface With Native Commands
// *******************************************************************************************

// PURPOSE:	Set up the standard text display used for the F8 screen
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B

PROC Setup_F8_Text_Display(HUD_COLOURS paramColour)

	INT theR, theG, theB, theA
	GET_HUD_COLOUR(paramColour, theR, theG, theB, theA)
	SET_TEXT_COLOUR(theR, theG, theB, theA)
	
	SET_TEXT_SCALE(F8_TEXT_SCALE_X, F8_TEXT_SCALE_Y)
	SET_TEXT_WRAP(F8_TEXT_WRAP_XPOS_START, F8_TEXT_WRAP_XPOS_END)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)

ENDPROC




// *******************************************************************************************
//		Generic F8 Screen Display Commands
// *******************************************************************************************

// PURPOSE:	Set up the standard literal text display used for the F8 screen and display text with it
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B
//					paramXPos			Text X pos
//					paramYPos			Text Y pos
//					paramText			The literal string to be output

PROC Display_F8_Literal_Text(HUD_COLOURS paramColour, FLOAT paramXPos, FLOAT paramYPos, STRING paramText)

	Setup_F8_Text_Display(paramColour)
	DISPLAY_TEXT_WITH_LITERAL_STRING(paramXPos, paramYPos, "STRING", paramText)

ENDPROC

// PURPOSE:	Set up the standard textLabel text display
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B
//					paramXPos			Text X pos
//					paramYPos			Text Y pos
//					paramTextLabel		The textLabel to be output

PROC Display_F8_TextLabel(HUD_COLOURS paramColour, FLOAT paramXPos, FLOAT paramYPos, STRING paramTextLabel)

	Setup_F8_Text_Display(paramColour)
	DISPLAY_TEXT(paramXPos, paramYPos, paramTextLabel)

ENDPROC

// PURPOSE:	Set up the standard textLabel text display with 1 number used for the F8 screen
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B
//					paramXPos			Text X pos
//					paramYPos			Text Y pos
//					paramTextLabel		The textLabel to be output
//					paramInt			The first number

PROC Display_F8_TextLabel_With_One_Number(HUD_COLOURS paramColour, FLOAT paramXPos, FLOAT paramYPos, STRING paramTextLabel, INT paramInt)

	Setup_F8_Text_Display(paramColour)
	DISPLAY_TEXT_WITH_NUMBER(paramXPos, paramYPos, paramTextLabel, paramInt)

ENDPROC

// PURPOSE:	Set up the standard textLabel text display with 2 numbers used for the F8 screen
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B
//					paramXPos			Text X pos
//					paramYPos			Text Y pos
//					paramTextLabel		The textLabel to be output
//					paramInt1			The first number
//					paramInt2			The second number

PROC Display_F8_TextLabel_With_Two_Numbers(HUD_COLOURS paramColour, FLOAT paramXPos, FLOAT paramYPos, STRING paramTextLabel, INT paramInt1, INT paramInt2)

	Setup_F8_Text_Display(paramColour)
	DISPLAY_TEXT_WITH_2_NUMBERS(paramXPos, paramYPos, paramTextLabel, paramInt1, paramInt2)

ENDPROC


// *******************************************************************************************
//	Specific F8 Family Screen Command Display Messages
// *******************************************************************************************

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Family_Member_Name(enumFamilyMember paramFamMemID, FLOAT paramYPos, HUD_COLOURS paramColour)
	
	STRING theName = Get_String_From_FamilyMember(paramFamMemID)
	Display_F8_Literal_Text(paramColour, F8_FAM_NAME_XPOS, paramYPos, theName)

ENDPROC


// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Family_Member_Current_Event(enumFamilyMember paramFamMemID, FLOAT paramYPos, HUD_COLOURS paramColour)
	
	enumFamilyEvents thisFamilyEvent = g_eCurrentFamilyEvent[paramFamMemID]
	
	TEXT_LABEL_63 theName = Get_String_From_FamilyEvent(thisFamilyEvent)
	
	//display additional info for placeholder anims etc...
	TEXT_LABEL_63 tFamilyAnimDict, tFamilyAnimClip
	ANIMATION_FLAGS eFamilyAnimFlag
	enumFamilyAnimProgress eFamilyAnimProgress
	IF PRIVATE_Get_FamilyMember_Anim(paramFamMemID, thisFamilyEvent,
			tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimFlag,
			eFamilyAnimProgress)
		IF (eFamilyAnimProgress = FAP_0_default)
//			theName += " [d]"
		ELIF (eFamilyAnimProgress = FAP_1_placeholder)
			theName += " [p]"
		ELIF (eFamilyAnimProgress = FAP_2_dialogue)
			theName += " [d]"
		ELIF (eFamilyAnimProgress = FAP_3_array)
			theName += " [a]"
		ELIF (eFamilyAnimProgress = FAP_4_scenario)
			theName += " [s]"
		ELSE
			theName += " [?]"
		ENDIF
	ELSE
//		theName += " [x]"
	ENDIF
	
	Display_F8_Literal_Text(paramColour, F8_FAM_CURRENT_EVENT, paramYPos, theName)

ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Family_Member_Previous_Event(enumFamilyMember paramFamMemID, FLOAT paramYPos, HUD_COLOURS paramColour)
	
	enumFamilyEvents thisFamilyEvent = Get_Previous_Event_For_FamilyMember(paramFamMemID)
	
	STRING theName = Get_String_From_FamilyEvent(thisFamilyEvent)
	Display_F8_Literal_Text(paramColour, F8_FAM_PREVIOUS_EVENT, paramYPos, theName)

ENDPROC


// *******************************************************************************************
//	Specific F8 Player Screen Command Display Messages
// *******************************************************************************************

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Player_Time(FLOAT paramYPos, HUD_COLOURS paramColour)
	TIMEOFDAY	sLastTimeActive		= GET_CURRENT_TIMEOFDAY()
	
	STRING theName = Get_String_From_TIMEOFDAY(sLastTimeActive)
	Display_F8_Literal_Text(paramColour, F8_PLA_NAME_XPOS, paramYPos, theName)

ENDPROC
// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Player_Char_Name(enumCharacterList paramPlayerCharID, FLOAT paramYPos, HUD_COLOURS paramColour)
	
	STRING theName = GET_PLAYER_PED_STRING(paramPlayerCharID)
	Display_F8_Literal_Text(paramColour, F8_PLA_NAME_XPOS, paramYPos, theName)

ENDPROC


// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Player_Char_Current_Scene(enumCharacterList paramPlayerCharID, FLOAT paramYPos, HUD_COLOURS paramColour)
	
	PED_REQUEST_SCENE_ENUM currentPlayerEvent = eCurrentPlayerEvents[paramPlayerCharID]
	FLOAT fScenePercent = fCurrentPlayerScenePercent[paramPlayerCharID]
			
//	GET_PLAYER_PED_SCENE_FOR_CURRENT_TIME(paramPlayerCharID, currentPlayerEvent)
	
	IF currentPlayerEvent = PR_SCENE_DEAD
		paramColour = HUD_COLOUR_RED
	ENDIF
	
	TEXT_LABEL_63 theName = Get_String_From_Ped_Request_Scene_Enum(currentPlayerEvent)
	
	IF (currentPlayerEvent = PR_SCENE_M_DEFAULT)
	OR (currentPlayerEvent = PR_SCENE_F_DEFAULT)
	OR (currentPlayerEvent = PR_SCENE_T_DEFAULT)
		IF DOES_ENTITY_EXIST(g_sPlayerPedRequest.sSelectorPeds.pedID[paramPlayerCharID])
			theName += "[exists]"
		ELSE
			theName += "["
			
			TIMEOFDAY sLastTimeActive = g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[paramPlayerCharID]
			FLOAT fCreateDistMultiplier
			SWITCH PRIVATE_GetRoamingLastKnownSwitchStage(paramPlayerCharID, sLastTimeActive, fCreateDistMultiplier)
				CASE 0
					theName += "as was "
					theName += GET_STRING_FROM_FLOAT(fCreateDistMultiplier)
				BREAK
				CASE 1
					theName += "A "
					theName += GET_STRING_FROM_FLOAT(fCreateDistMultiplier)
				BREAK
				CASE 2
					theName += "B "
					theName += GET_STRING_FROM_FLOAT(fCreateDistMultiplier)
				BREAK
				CASE 3
					theName += "C "
					theName += GET_STRING_FROM_FLOAT(fCreateDistMultiplier)
				BREAK
				DEFAULT
					theName += "?????"
				BREAK
			ENDSWITCH
			
			theName += "]"
		ENDIF
	ELSE
		TIME_SAMPLE eTimeSample = PRIVATE_GetPlayerTriggerPrioritySwitch(paramPlayerCharID)
		IF g_SavedGlobals.sPlayerSceneData.g_bPlayerTriggeredPrioritySwitch[paramPlayerCharID][eTimeSample]
			theName += "["
			theName += GET_STRING_FROM_FLOAT(fScenePercent)
			theName += "!]"
			
			IF (paramColour = HUD_COLOUR_BLUEDARK)
				paramColour = HUD_COLOUR_ORANGEDARK
			ELIF (paramColour = HUD_COLOUR_BLUELIGHT)
				paramColour = HUD_COLOUR_ORANGELIGHT
			ENDIF
		ELSE
			theName += "["
			IF (fScenePercent >= 0)
				theName += GET_STRING_FROM_FLOAT(fScenePercent, 1)
			ELSE
				theName += "inv"
			ENDIF
			theName += "?]"
		ENDIF
	ENDIF
	
	Display_F8_Literal_Text(paramColour, F8_PLA_CURRENT_SCENE_XPOS, paramYPos, theName)

ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Player_Char_Last_Scene(enumCharacterList paramPlayerCharID, FLOAT paramYPos, HUD_COLOURS paramColour)
	
	PED_REQUEST_SCENE_ENUM lastPlayerEvent = g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[paramPlayerCharID]
	
	IF lastPlayerEvent = PR_SCENE_DEAD
		paramColour = HUD_COLOUR_RED
	ENDIF
	
	STRING theName = Get_String_From_Ped_Request_Scene_Enum(lastPlayerEvent)
	Display_F8_Literal_Text(paramColour, F8_PLA_PREVIOUS_SCENE_XPOS, paramYPos, theName)

ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Player_Char_Last_Time_Active(enumCharacterList paramPlayerCharID, FLOAT &paramYPos, HUD_COLOURS paramColour)
	
	TIMEOFDAY sLastTimeActive = g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[paramPlayerCharID]
	
	IF NOT Is_TimeOfDay_Valid(sLastTimeActive)
		//
	ELSE
		//
		STRING theName = Get_String_From_TIMEOFDAY(sLastTimeActive)
		Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, theName)
		
		
		//
		INT iSeconds, iMinutes, iHours, iDays, iMonths, iYears
		GET_DIFFERENCE_BETWEEN_NOW_AND_TIMEOFDAY(sLastTimeActive, iSeconds, iMinutes, iHours, iDays, iMonths, iYears)
		
		TEXT_LABEL_63 theDist
		theDist = "diff: "
		theDist += GET_STRING_FROM_TIMEOFDAY_DIFFERENCE(iSeconds, iMinutes, iHours, iDays, iMonths, iYears)
		Display_F8_Literal_Text(paramColour, F8_PLA_PREVIOUS_SCENE_XPOS, paramYPos, theDist)
		
		paramYPos += F8_ADD_Y
	ENDIF
	
ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Player_Char_Last_Known_Coords(enumCharacterList paramPlayerCharID, FLOAT &paramYPos, HUD_COLOURS paramColour)
	
	VECTOR vecLastKnownCoords = g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[paramPlayerCharID]
	
	IF ARE_VECTORS_EQUAL(vecLastKnownCoords, <<0,0,0>>)
		//
	ELSE
		//
		TEXT_LABEL_63 theCoord
		theCoord = "<<"
		theCoord += GET_STRING_FROM_FLOAT(vecLastKnownCoords.x)
		theCoord += ", "
		theCoord += GET_STRING_FROM_FLOAT(vecLastKnownCoords.y)
		theCoord += ", "
		theCoord += GET_STRING_FROM_FLOAT(vecLastKnownCoords.z)
		theCoord += ">> //"
		theCoord += GET_NAME_OF_ZONE(vecLastKnownCoords)
		Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, theCoord)
		
		//
		TEXT_LABEL_63 theDist
		theDist = "dist: "
		theDist += GET_STRING_FROM_FLOAT(VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vecLastKnownCoords))
		Display_F8_Literal_Text(paramColour, F8_PLA_PREVIOUS_SCENE_XPOS, paramYPos, theDist)
		
		paramYPos += F8_ADD_Y
	ENDIF
	
ENDPROC



// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Player_Char_Last_Known_Vehicle(enumCharacterList paramPlayerCharID, FLOAT &paramYPos, HUD_COLOURS paramColour)
	
	BOOL bF8_ADD_Y = FALSE
	
	PED_VEH_DATA_STRUCT playerLastKnownVeh = g_sPlayerLastVeh[paramPlayerCharID]
	IF (playerLastKnownVeh.model = DUMMY_MODEL_FOR_SCRIPT)
		//
	ELSE
		//
		TEXT_LABEL_63 theVehicle
		theVehicle = "lastVeh: "
		theVehicle += GET_MODEL_NAME_FOR_DEBUG(playerLastKnownVeh.model)
		
		IF (playerLastKnownVeh.modelTrailer <> DUMMY_MODEL_FOR_SCRIPT)
			theVehicle += "/"
			theVehicle += GET_MODEL_NAME_FOR_DEBUG(playerLastKnownVeh.modelTrailer)
		ENDIF
		
		IF (playerLastKnownVeh.bConvertible)
			theVehicle += "^C^"
		ENDIF
		
		theVehicle += "["
		theVehicle += playerLastKnownVeh.fHealth
		theVehicle += "]"
		
		SWITCH g_ePlayerLastVehState[paramPlayerCharID]
			CASE PTVS_0_noVehicle
				theVehicle += " noVehicle"
			BREAK
			CASE PTVS_1_playerWithVehicle
				theVehicle += " wVeh<"
				theVehicle += GET_STRING_FROM_FLOAT(g_vPlayerLastVehCoord[paramPlayerCharID].x - g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[paramPlayerCharID].x)
				theVehicle += ","
				theVehicle += GET_STRING_FROM_FLOAT(g_vPlayerLastVehCoord[paramPlayerCharID].y - g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[paramPlayerCharID].y)
				theVehicle += ","
				theVehicle += GET_STRING_FROM_FLOAT(g_vPlayerLastVehCoord[paramPlayerCharID].z - g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[paramPlayerCharID].z)
				theVehicle += ">"
				
			BREAK
			CASE PTVS_2_playerInVehicle
				theVehicle += " inVehicle"
			BREAK
			DEFAULT
				theVehicle += " [XXX]"
			BREAK
		ENDSWITCH
		
		IF g_ePlayerLastVehGen[paramPlayerCharID] != VEHGEN_NONE
			theVehicle += " vehGen"
			theVehicle += ENUM_TO_INT(g_ePlayerLastVehGen[paramPlayerCharID])
		ENDIF
		
		Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, theVehicle)
		
		bF8_ADD_Y = TRUE
	ENDIF
	
	VEHICLE_INDEX vPlayerVeh = g_vPlayerVeh[paramPlayerCharID]
	IF NOT DOES_ENTITY_EXIST(vPlayerVeh)
		//
	ELSE
		//
		TEXT_LABEL_63 theVehicle
		theVehicle = "vPlayerVeh: "
		theVehicle += GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vPlayerVeh))
		Display_F8_Literal_Text(paramColour, F8_PLA_PREVIOUS_SCENE_XPOS, paramYPos, theVehicle)
		
		bF8_ADD_Y = TRUE
	ENDIF
	
	IF bF8_ADD_Y
		paramYPos += F8_ADD_Y
	ENDIF
	
ENDPROC



// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Player_Char_Last_Known_PedID(enumCharacterList paramPlayerCharID, FLOAT &paramYPos, HUD_COLOURS paramColour)
	
	IF (paramPlayerCharID = GET_CURRENT_PLAYER_PED_ENUM())
		EXIT
	ENDIF
	
	SELECTOR_SLOTS_ENUM playerSelectorID = GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(paramPlayerCharID)
	PED_INDEX playerLastKnownPedID = g_sPlayerPedRequest.sSelectorPeds.pedID[playerSelectorID]
	
	IF NOT DOES_ENTITY_EXIST(playerLastKnownPedID)
		//
		
		enumCreateState eCreateState
		IF SafeToCreatePlayerAmbientPedAtLastKnownLocation(paramPlayerCharID, eCreateState)
			
			TEXT_LABEL_63 thePedID
			thePedID = "creating!"
			Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, thePedID)
			
			paramYPos += F8_ADD_Y
			
		ELSE
			TEXT_LABEL_63 thePedID = "", theDist = ""
			
			SWITCH eCreateState
				CASE eCS_3_SWITCH_IN_PROGRESS
					thePedID = "DON'T create - eCS_3_SWITCH_IN_PROGRESS"
					Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, thePedID)
				BREAK
				CASE eCS_5_TOO_MANY_HOURS_PASSED
					thePedID = "DON'T create - eCS_5_TOO_MANY_HOURS_PASSED"
					Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, thePedID)
				BREAK
				CASE eCS_7_TOO_FAR_TO_CREATE
					thePedID = "DON'T create - eCS_7_TOO_FAR_TO_CREATE"
					Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, thePedID)
					DrawDebugSceneSphere(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[paramPlayerCharID], fCONST_FRIEND_TOO_FAR_TO_CREATE, HUD_COLOUR_BLUELIGHT, 0.15)
					
					theDist  = "["
					theDist += GET_STRING_FROM_FLOAT(fCONST_FRIEND_TOO_FAR_TO_CREATE)
					theDist += "m]"
					Display_F8_Literal_Text(paramColour, F8_PLA_PREVIOUS_SCENE_XPOS, paramYPos, theDist)
				BREAK
				CASE eCS_8_TOO_CLOSE_TO_CREATE
					thePedID = "DON'T create - eCS_8_TOO_CLOSE_TO_CREATE"
					Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, thePedID)
					DrawDebugSceneSphere(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[paramPlayerCharID], fCONST_FRIEND_TOO_CLOSE_TO_CREATE, HUD_COLOUR_BLUELIGHT, 0.15)
					
					theDist  = "["
					theDist += GET_STRING_FROM_FLOAT(fCONST_FRIEND_TOO_CLOSE_TO_CREATE)
					theDist += "m]"
					Display_F8_Literal_Text(paramColour, F8_PLA_PREVIOUS_SCENE_XPOS, paramYPos, theDist)
				BREAK
				CASE eCS_9_SPHERE_VISIBLE
					thePedID = "DON'T create - eCS_9_SPHERE_VISIBLE"
					Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, thePedID)
				BREAK
				
				CASE eCS_4_INVALID_LAST_TIME_ACTIVE		FALLTHRU
				CASE eCS_6_INVALID_LAST_KNOWN_COORDS
					EXIT
				BREAK
				
				CASE eCS_10_POST_MISSION_SWITCH
					thePedID = "DON'T create - eCS_10_POST_MISSION_SWITCH"
					Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, thePedID)
				BREAK
				CASE eCS_11_SUPRESSED_FOR_FRIEND_ACTIVITY
					thePedID = "DON'T create - eCS_11_SUPRESSED_FOR_FRIEND_ACTIVITY"
					Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, thePedID)
				BREAK
				CASE eCS_12_NEAR_MISSION_TRIGGER
					thePedID = "DON'T create - eCS_12_NEAR_MISSION_TRIGGER"
					Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, thePedID)
				BREAK
				CASE eCS_13_LEADIN_TRIGGER
					thePedID = "DON'T create - eCS_13_LEADIN_TRIGGER"
					Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, thePedID)
				BREAK
				CASE eCS_14_DIRECTOR_MODE
					thePedID = "DON'T create - eCS_14_DIRECTOR_MODE"
					Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, thePedID)
				BREAK
				DEFAULT
					thePedID = "DON'T create "
					thePedID += ENUM_TO_INT(eCreateState)
					Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, thePedID)
				BREAK
			ENDSWITCH
			
			paramYPos += F8_ADD_Y
			
		ENDIF
		
	ELSE
		// 
		TEXT_LABEL_63 thePedID
		thePedID = "pedID: "
		thePedID += GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(playerLastKnownPedID))
		
		
		// // #1240234 // // // // // // // // // // // // // // //
		SP_MISSIONS eTriggerMission = TRIGGER_SCENE_GET_SP_MISSION()
		IF (eTriggerMission != SP_MISSION_NONE)
			IF IS_BIT_SET(g_sPlayerPedRequest.iBitsetPedInLeadIn, ENUM_TO_INT(playerSelectorID))
				thePedID += " [trig-leadin]"
			ELSE
				INT InstanceId = 0
				STRING InstanceScript = GET_ENTITY_SCRIPT(playerLastKnownPedID, InstanceId)
				thePedID += " [t:"
				thePedID += InstanceScript
				thePedID += "]"
			ENDIF
		ELSE
			INT InstanceId = 0
			STRING InstanceScript = GET_ENTITY_SCRIPT(playerLastKnownPedID, InstanceId)
			thePedID += " [a:"
			thePedID += InstanceScript
			thePedID += "]"
		ENDIF
		
		// // // // // // // // // // // // // // // // // // // //
		
		
		Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, thePedID)
		
		paramYPos += F8_ADD_Y
	ENDIF
	
ENDPROC



// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Player_Char_Last_Known_Wanted(enumCharacterList paramPlayerCharID, FLOAT &paramYPos, HUD_COLOURS paramColour)
	
	INT intLastKnownWanted = g_savedGlobals.sPlayerData.sInfo.iLastKnownWantedLevel[paramPlayerCharID]
	
	IF NOT (intLastKnownWanted = 0)
		//
		TEXT_LABEL_63 theWanted
		theWanted = "wanted: "
		theWanted += intLastKnownWanted
		Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, theWanted)
		
		paramYPos += F8_ADD_Y
	ENDIF
	
ENDPROC



// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Player_Char_Changed_On_Mission(enumCharacterList paramPlayerCharID, FLOAT &paramYPos, HUD_COLOURS paramColour)
	
	BOOL boolLastKnownchaned = g_savedGlobals.sPlayerData.sInfo.bChangedClothesOnMission[paramPlayerCharID]
	
	IF (boolLastKnownchaned)
		//
		TEXT_LABEL_63 thechaned
		thechaned = "changed outfit on mission"
		Display_F8_Literal_Text(paramColour, F8_PLA_INDENT_XPOS, paramYPos, thechaned)
		
		paramYPos += F8_ADD_Y
	ENDIF
	
ENDPROC


// *******************************************************************************************
//	F8 Family Screen Strand Display
// *******************************************************************************************

// PURPOSE: Display the details for one strand
//
// INPUT PARAMS:	paramFamMemID		The Strand ID
//					paramYPos			The screen Y Pos for these details

PROC Update_Selected_Family_Member_Event(enumFamilyEvents newFamilyEvent)
	
	IF g_bWarpPlayerOnFamilyDebug
		enumFamilyEvents oldFamilyEvent = g_eCurrentFamilyEvent[g_eDebugSelectedMember]
		enumFamilyMember eFamilyMember
		REPEAT MAX_FAMILY_MEMBER eFamilyMember
			IF (eFamilyMember <> g_eDebugSelectedMember)
				IF (g_eCurrentFamilyEvent[eFamilyMember] = oldFamilyEvent)
					g_eCurrentFamilyEvent[eFamilyMember] = NO_FAMILY_EVENTS
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	g_eCurrentFamilyEvent[g_eDebugSelectedMember] = newFamilyEvent
	
ENDPROC

FUNC BOOL Control_Update_Family_Member_Event()
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		bChangeDebugF8SelectedEvent = NOT bChangeDebugF8SelectedEvent
	ENDIF
	
	CONST_FLOAT	totalBoxXOffset		0.04
	CONST_FLOAT	totalBoxXSize		0.24
	CONST_FLOAT	totalYextXOffset	0.00
	
	// Calculate the selection rectangle height
	FLOAT	totalTextBoxYSize_a		=  F8_ADD_Y * 4
	FLOAT	totalTextBoxYSize_b		=  F8_ADD_Y * 1
	FLOAT	boxYBorder				= (F8_ADD_Y * 0.5)
	FLOAT	totalBoxYSize_a			= totalTextBoxYSize_a + boxYBorder
	FLOAT	totalBoxYSize_b			= totalTextBoxYSize_b + boxYBorder
	
	FLOAT	boxYCentre_a			= totalBoxYSize_a * 0.5
	FLOAT	boxYCentre_b			= totalBoxYSize_b * 0.5
	
	// Calculate the background rectangle height
	FLOAT	detailsTextBoxYSize		= F8_ADD_Y * (ENUM_TO_INT(MAX_FAMILY_MEMBER)+F8_SEPERATOR_COUNT)
	FLOAT	detailsBoxYBorder		= F8_START_YPOS + (F8_ADD_Y * 0.5)
	FLOAT	detailsBoxXSize			= detailsTextBoxYSize + detailsBoxYBorder
	
	FLOAT	testParamXPos			= totalBoxXSize + totalYextXOffset - (F8_TITLE_CONTROLLER_XPOS+totalBoxXOffset)
	FLOAT	testParamYPos_a			= (boxYCentre_a+detailsBoxXSize)-(totalBoxYSize_a*0.5)
	FLOAT	testParamYPos_b			= (boxYCentre_b+detailsBoxXSize)-(totalBoxYSize_b*0.5)
	
	IF bChangeDebugF8SelectedEvent
		
		DRAW_RECT(F8_TITLE_CONTROLLER_XPOS+totalBoxXOffset, boxYCentre_a+detailsBoxXSize,
				totalBoxXSize, totalBoxYSize_a,
				0, 0, 0, 175)
		Display_F8_Literal_Text(HUD_COLOUR_PURE_WHITE, testParamXPos, testParamYPos_a + (F8_ADD_Y*0), "Press <- and -> to change, 'J' to return")
		
		TEXT_LABEL_63 str
		enumFamilyEvents leftFamilyEvents = g_eCurrentFamilyEvent[g_eDebugSelectedMember]
		BOOL bFoundValidNextEvent = FALSE
		WHILE NOT bFoundValidNextEvent
			
			INT iCurrentFamilyMemberEvent = ENUM_TO_INT(leftFamilyEvents)
			iCurrentFamilyMemberEvent--
			IF iCurrentFamilyMemberEvent < 0
				iCurrentFamilyMemberEvent = count_of(enumFamilyEvents)-1
			ELIF iCurrentFamilyMemberEvent >= count_of(enumFamilyEvents)
				iCurrentFamilyMemberEvent = 0
			ENDIF
			
			leftFamilyEvents = INT_TO_ENUM(enumFamilyEvents, iCurrentFamilyMemberEvent)
			IF IS_EVENT_VALID_FOR_FAMILY_MEMBER(g_eDebugSelectedMember, leftFamilyEvents)
				bFoundValidNextEvent = TRUE
			ENDIF
		ENDWHILE
		
		str = "    <- : "
		str += Get_String_From_FamilyEvent(leftFamilyEvents)
		Display_F8_Literal_Text(HUD_COLOUR_REDLIGHT, testParamXPos, testParamYPos_a + (F8_ADD_Y*1),
				str)

		enumFamilyEvents rightFamilyEvents = g_eCurrentFamilyEvent[g_eDebugSelectedMember]
		bFoundValidNextEvent = FALSE
		WHILE NOT bFoundValidNextEvent
			
			INT iCurrentFamilyMemberEvent = ENUM_TO_INT(rightFamilyEvents)
			iCurrentFamilyMemberEvent++
			IF iCurrentFamilyMemberEvent < 0
				iCurrentFamilyMemberEvent = count_of(enumFamilyEvents)-1
			ELIF iCurrentFamilyMemberEvent >= count_of(enumFamilyEvents)
				iCurrentFamilyMemberEvent = 0
			ENDIF
			
			rightFamilyEvents = INT_TO_ENUM(enumFamilyEvents, iCurrentFamilyMemberEvent)
			IF IS_EVENT_VALID_FOR_FAMILY_MEMBER(g_eDebugSelectedMember, rightFamilyEvents)
				bFoundValidNextEvent = TRUE
			ENDIF
		ENDWHILE
		
		str = "    -> : "
		str += Get_String_From_FamilyEvent(rightFamilyEvents)
		Display_F8_Literal_Text(HUD_COLOUR_REDLIGHT, testParamXPos, testParamYPos_a + (F8_ADD_Y*2),
				str)
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)
			
			PRINTSTRING(" * ")
			PRINTSTRING(Get_String_From_FamilyMember(g_eDebugSelectedMember))
			PRINTSTRING(" current = ")
			PRINTSTRING(Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[g_eDebugSelectedMember]))
			
			Update_Selected_Family_Member_Event(rightFamilyEvents)
			g_bUpdatedFamilyEvents = TRUE
			
			PRINTSTRING(", next = ")
			PRINTSTRING(Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[g_eDebugSelectedMember]))
			
			PRINTNL()
			
		ENDIF
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_LEFT)
			
			PRINTSTRING(" * ")
			PRINTSTRING(Get_String_From_FamilyMember(g_eDebugSelectedMember))
			PRINTSTRING(" current = ")
			PRINTSTRING(Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[g_eDebugSelectedMember]))
			
			Update_Selected_Family_Member_Event(leftFamilyEvents)
			g_bUpdatedFamilyEvents = TRUE
			
			PRINTSTRING(", previous = ")
			PRINTSTRING(Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[g_eDebugSelectedMember]))
			
			PRINTNL()
			
			
		ENDIF
		
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_H)
			g_bWarpPlayerOnFamilyDebug = NOT g_bWarpPlayerOnFamilyDebug
		ENDIF
		IF g_bWarpPlayerOnFamilyDebug
			Display_F8_Literal_Text(HUD_COLOUR_PURE_WHITE, testParamXPos, testParamYPos_a + (F8_ADD_Y*3),
				"Press 'H' to ~r~disable~s~ player warp")
		ELSE
			Display_F8_Literal_Text(HUD_COLOUR_PURE_WHITE, testParamXPos, testParamYPos_a + (F8_ADD_Y*3),
				"Press 'H' to ~b~enable~s~ player warp")
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	
	DRAW_RECT(F8_TITLE_CONTROLLER_XPOS+totalBoxXOffset, boxYCentre_b+detailsBoxXSize,
			totalBoxXSize, totalBoxYSize_b,
			0, 0, 0, 175)
	Display_F8_Literal_Text(HUD_COLOUR_GREYLIGHT, testParamXPos, testParamYPos_b, "Press key 'J' to alter")
	
	RETURN FALSE
ENDFUNC

// PURPOSE: Display the details for one strand
//
// INPUT PARAMS:	paramFamMemID		The Strand ID
//					paramYPos			The screen Y Pos for these details

PROC Display_Family_Member(enumFamilyMember paramFamMemID, FLOAT paramYPos)
	HUD_COLOURS lineColour
	IF g_eCurrentFamilyEvent[paramFamMemID] >= MAX_FAMILY_EVENTS
		IF paramFamMemID = g_eDebugSelectedMember
			IF bChangeDebugF8SelectedEvent
				lineColour = HUD_COLOUR_RED
			ELSE
				lineColour = HUD_COLOUR_PURE_WHITE
			ENDIF
		ELSE
			lineColour = HUD_COLOUR_GREY
		ENDIF
	ELSE
		IF paramFamMemID = g_eDebugSelectedMember
			
			IF bChangeDebugF8SelectedEvent
				lineColour = HUD_COLOUR_REDLIGHT
			ELSE
				lineColour = HUD_COLOUR_BLUELIGHT
			ENDIF
		ELSE
			lineColour = HUD_COLOUR_BLUEDARK
		ENDIF
	ENDIF
	
	// Display Strand Name in appropriate colour
	Display_Family_Member_Name(paramFamMemID, paramYPos, lineColour)
	
	Display_Family_Member_Current_Event(paramFamMemID, paramYPos, lineColour)
	Display_Family_Member_Previous_Event(paramFamMemID, paramYPos, lineColour)

ENDPROC


// *******************************************************************************************
//	F8 Player Screen Strand Display
// *******************************************************************************************

// PURPOSE: Display the details for one strand
//
// INPUT PARAMS:	paramFamMemID		The Strand ID
//					paramYPos			The screen Y Pos for these details

PROC Display_Player_Char(enumCharacterList paramPlayerCharID, FLOAT &paramYPos, BOOL bSwitchSplineCamInProgress)
	HUD_COLOURS lineColour = HUD_COLOUR_PURE_WHITE
	
	IF NOT bSwitchSplineCamInProgress
		IF (paramPlayerCharID = GET_CURRENT_PLAYER_PED_ENUM())
			lineColour = HUD_COLOUR_BLUEDARK
		ELSE
			lineColour = HUD_COLOUR_BLUELIGHT
		ENDIF
	ELSE
		IF (paramPlayerCharID = GET_CURRENT_PLAYER_PED_ENUM())
			lineColour = HUD_COLOUR_GREENDARK
		ELSE
			lineColour = HUD_COLOUR_GREENLIGHT
		ENDIF
	ENDIF
	
	// Display Strand Name in appropriate colour
	Display_Player_Char_Name(paramPlayerCharID, paramYPos, lineColour)
	
	Display_Player_Char_Current_Scene(paramPlayerCharID, paramYPos, lineColour)
	Display_Player_Char_Last_Scene(paramPlayerCharID, paramYPos, lineColour)
	paramYPos += F8_ADD_Y
	
	Display_Player_Char_Last_Time_Active(paramPlayerCharID, paramYPos, lineColour)
	Display_Player_Char_Last_Known_Coords(paramPlayerCharID, paramYPos, lineColour)
	Display_Player_Char_Last_Known_Vehicle(paramPlayerCharID, paramYPos, lineColour)
	Display_Player_Char_Last_Known_PedID(paramPlayerCharID, paramYPos, lineColour)
	Display_Player_Char_Last_Known_Wanted(paramPlayerCharID, paramYPos, lineColour)
	Display_Player_Char_Changed_On_Mission(paramPlayerCharID, paramYPos, lineColour)
	
ENDPROC

// PURPOSE: Display the details for one strand
//
// INPUT PARAMS:	paramFamMemID		The Strand ID

PROC Display_Player_Blip(enumCharacterList paramPlayerCharID)

	SELECTOR_SLOTS_ENUM selectorID = GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(paramPlayerCharID)

	IF NOT DOES_BLIP_EXIST(blipPlayerChars[paramPlayerCharID])
		IF NOT DOES_ENTITY_EXIST(g_sPlayerPedRequest.sSelectorPeds.pedID[selectorID])
			PED_REQUEST_SCENE_ENUM eReqScene = eCurrentPlayerEvents[paramPlayerCharID]
			
			IF (eReqScene <> PR_SCENE_INVALID)
				IF eReqScene = PR_SCENE_M_DEFAULT
				OR eReqScene = PR_SCENE_F_DEFAULT
				OR eReqScene = PR_SCENE_T_DEFAULT
					VECTOR vLastKnownCoords = g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[paramPlayerCharID]
					IF NOT ARE_VECTORS_EQUAL(vLastKnownCoords, <<0,0,0>>)
						blipPlayerChars[paramPlayerCharID] = ADD_BLIP_FOR_COORD(vLastKnownCoords)
						SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
						
						IF IS_PED_THE_CURRENT_PLAYER_PED(paramPlayerCharID)
							SWITCH selectorID
								CASE SELECTOR_PED_MICHAEL
									SET_BLIP_COLOUR(blipPlayerChars[paramPlayerCharID], BLIP_COLOUR_YELLOW)
									SET_BLIP_NAME_FROM_ASCII(blipPlayerChars[paramPlayerCharID], "MICHAEL (me)")
								BREAK
								CASE SELECTOR_PED_TREVOR
									SET_BLIP_COLOUR(blipPlayerChars[paramPlayerCharID], BLIP_COLOUR_RED)
									SET_BLIP_NAME_FROM_ASCII(blipPlayerChars[paramPlayerCharID], "TREVOR (me)")
								BREAK
								CASE SELECTOR_PED_FRANKLIN
									SET_BLIP_COLOUR(blipPlayerChars[paramPlayerCharID], BLIP_COLOUR_BLUE)
									SET_BLIP_NAME_FROM_ASCII(blipPlayerChars[paramPlayerCharID], "FRANKLIN (me)")
								BREAK
								
								CASE SELECTOR_PED_MULTIPLAYER
									SET_BLIP_COLOUR(blipPlayerChars[paramPlayerCharID], BLIP_COLOUR_GREEN)
									SET_BLIP_NAME_FROM_ASCII(blipPlayerChars[paramPlayerCharID], "MULTIPLAYER (me)")
								BREAK
								DEFAULT
									SCRIPT_ASSERT("invalid INT_TO_ENUM(SELECTOR_SLOTS_ENUM, paramPlayerCharID)")
								BREAK
							ENDSWITCH
						ELSE
							SWITCH selectorID
								CASE SELECTOR_PED_MICHAEL
									SET_BLIP_COLOUR(blipPlayerChars[paramPlayerCharID], 13)
									SET_BLIP_NAME_FROM_ASCII(blipPlayerChars[paramPlayerCharID], "MICHAEL (not)")
								BREAK
								CASE SELECTOR_PED_TREVOR
									SET_BLIP_COLOUR(blipPlayerChars[paramPlayerCharID], 12)
									SET_BLIP_NAME_FROM_ASCII(blipPlayerChars[paramPlayerCharID], "TREVOR (not)")
								BREAK
								CASE SELECTOR_PED_FRANKLIN
									SET_BLIP_COLOUR(blipPlayerChars[paramPlayerCharID], 15)
									SET_BLIP_NAME_FROM_ASCII(blipPlayerChars[paramPlayerCharID], "FRANKLIN (not)")
								BREAK
								
								CASE SELECTOR_PED_MULTIPLAYER
									SET_BLIP_COLOUR(blipPlayerChars[paramPlayerCharID], 14)
									SET_BLIP_NAME_FROM_ASCII(blipPlayerChars[paramPlayerCharID], "MULTIPLAYER (not)")
								BREAK
								DEFAULT
									SCRIPT_ASSERT("invalid INT_TO_ENUM(SELECTOR_SLOTS_ENUM, paramPlayerCharID)")
								BREAK
							ENDSWITCH
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			blipPlayerChars[paramPlayerCharID] = ADD_BLIP_FOR_ENTITY(g_sPlayerPedRequest.sSelectorPeds.pedID[selectorID])
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			
			SWITCH selectorID
				CASE SELECTOR_PED_MICHAEL
					SET_BLIP_COLOUR(blipPlayerChars[paramPlayerCharID], BLIP_COLOUR_YELLOW)
					SET_BLIP_NAME_FROM_ASCII(blipPlayerChars[paramPlayerCharID], "MICHAEL (is)")
				BREAK
				CASE SELECTOR_PED_TREVOR
					SET_BLIP_COLOUR(blipPlayerChars[paramPlayerCharID], BLIP_COLOUR_RED)
					SET_BLIP_NAME_FROM_ASCII(blipPlayerChars[paramPlayerCharID], "TREVOR (is)")
				BREAK
				CASE SELECTOR_PED_FRANKLIN
					SET_BLIP_COLOUR(blipPlayerChars[paramPlayerCharID], BLIP_COLOUR_BLUE)
					SET_BLIP_NAME_FROM_ASCII(blipPlayerChars[paramPlayerCharID], "FRANKLIN (is)")
				BREAK
				
				CASE SELECTOR_PED_MULTIPLAYER
					SET_BLIP_COLOUR(blipPlayerChars[paramPlayerCharID], BLIP_COLOUR_GREEN)
					SET_BLIP_NAME_FROM_ASCII(blipPlayerChars[paramPlayerCharID], "MULTIPLAYER (is)")
				BREAK
				DEFAULT
					SCRIPT_ASSERT("invalid INT_TO_ENUM(SELECTOR_SLOTS_ENUM, paramPlayerCharID)")
				BREAK
			ENDSWITCH
		ENDIF
	ELSE
		VECTOR vLastKnownCoords = g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[paramPlayerCharID]
		
		IF (GET_BLIP_INFO_ID_TYPE(blipPlayerChars[paramPlayerCharID]) = BLIPTYPE_COORDS)
		OR (GET_BLIP_INFO_ID_TYPE(blipPlayerChars[paramPlayerCharID]) = BLIPTYPE_CONTACT)
			
			IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_BLIP_COORDS(blipPlayerChars[paramPlayerCharID]), vLastKnownCoords)
				PRINTSTRING("blip ")PRINTINT(ENUM_TO_INT(selectorID))PRINTSTRING(": move coord")PRINTNL()
				SET_BLIP_COORDS(blipPlayerChars[paramPlayerCharID], vLastKnownCoords)
			ENDIF
			IF DOES_ENTITY_EXIST(g_sPlayerPedRequest.sSelectorPeds.pedID[selectorID])
				PRINTSTRING("blip ")PRINTINT(ENUM_TO_INT(selectorID))PRINTSTRING(": BLIPTYPE_CHAR")PRINTNL()
				REMOVE_BLIP(blipPlayerChars[paramPlayerCharID])
			ENDIF
		ELIF (GET_BLIP_INFO_ID_TYPE(blipPlayerChars[paramPlayerCharID]) = BLIPTYPE_CHAR)
			IF NOT DOES_ENTITY_EXIST(g_sPlayerPedRequest.sSelectorPeds.pedID[selectorID])
				PRINTSTRING("blip ")PRINTINT(ENUM_TO_INT(selectorID))PRINTSTRING(": BLIPTYPE_COORDS")PRINTNL()
				REMOVE_BLIP(blipPlayerChars[paramPlayerCharID])
			ENDIF
		ELSE
			PRINTSTRING("blip ")PRINTINT(ENUM_TO_INT(selectorID))
			PRINTSTRING(": BLIPTYPE_OTHER [")PRINTINT(ENUM_TO_INT(GET_BLIP_INFO_ID_TYPE(blipPlayerChars[paramPlayerCharID])))
			PRINTSTRING("]???")PRINTNL()
			
			SCRIPT_ASSERT("blipPlayerChars: BLIPTYPE_OTHER???")
		ENDIF
		
		// //
		IF NOT DOES_ENTITY_EXIST(g_sPlayerPedRequest.sSelectorPeds.pedID[selectorID])
			IF NOT IS_PED_THE_CURRENT_PLAYER_PED(paramPlayerCharID)
				DEFAULT_PLAYER_SWITCH_STATE_STRUCT sDefaultPlayerSwitchState = g_sDefaultPlayerSwitchState[paramPlayerCharID]
				
				//
				DrawDefaultPlayerSwitchState(paramPlayerCharID, "vVehicleCoord_a", sDefaultPlayerSwitchState.vVehicleCoord_a,	vLastKnownCoords,							sDefaultPlayerSwitchState.fVehicleHead_a, sDefaultPlayerSwitchState.fVehicleSpeed_a,	0.5, HUD_COLOUR_BLUE, FALSE)
				DrawDefaultPlayerSwitchState(paramPlayerCharID, "vVehicleCoord_b", sDefaultPlayerSwitchState.vVehicleCoord_b,	sDefaultPlayerSwitchState.vVehicleCoord_a,	sDefaultPlayerSwitchState.fVehicleHead_b, sDefaultPlayerSwitchState.fVehicleSpeed_b,	0.5, HUD_COLOUR_BLUE, FALSE)
				DrawDefaultPlayerSwitchState(paramPlayerCharID, "vVehicleCoord_c", sDefaultPlayerSwitchState.vVehicleCoord_c,	sDefaultPlayerSwitchState.vVehicleCoord_b,	sDefaultPlayerSwitchState.fVehicleHead_c, sDefaultPlayerSwitchState.fVehicleSpeed_c,	0.5, HUD_COLOUR_BLUE, FALSE)
				
				//
				DrawDefaultPlayerSwitchState(paramPlayerCharID, "vWalkCoord_a", sDefaultPlayerSwitchState.vWalkCoord_a,	vLastKnownCoords,						sDefaultPlayerSwitchState.fWalkHead_a, -1,				0.1, HUD_COLOUR_GREEN, TRUE)
				DrawDefaultPlayerSwitchState(paramPlayerCharID, "vWalkCoord_b", sDefaultPlayerSwitchState.vWalkCoord_b,	sDefaultPlayerSwitchState.vWalkCoord_a,	sDefaultPlayerSwitchState.fWalkHead_b, -1,				0.1, HUD_COLOUR_GREEN, TRUE)
				DrawDefaultPlayerSwitchState(paramPlayerCharID, "vWalkCoord_c", sDefaultPlayerSwitchState.vWalkCoord_c,	sDefaultPlayerSwitchState.vWalkCoord_b,	sDefaultPlayerSwitchState.fWalkHead_c, -1,				0.1, HUD_COLOUR_GREEN, TRUE)
				
			ENDIF
		ENDIF
		// //
		 
		 
	ENDIF	
ENDPROC

PROC Display_F8_Last_Switch_Queue_Details(FLOAT &paramYPos)
	
	enumCharacterList iPeds
	REPEAT NUM_OF_PLAYABLE_PEDS iPeds
		
		TEXT_LABEL		thePlayerPedString = GET_PLAYER_PED_STRING(iPeds)
		TEXT_LABEL_63	theTitle = "    "
		theTitle += GET_STRING_FROM_STRING(thePlayerPedString,
				GET_LENGTH_OF_LITERAL_STRING("CHAR_"),
				GET_LENGTH_OF_LITERAL_STRING(thePlayerPedString))
		theTitle += " sceneQueue"
		
		Display_F8_Literal_Text(HUD_COLOUR_GREYDARK, F8_PLA_PREVIOUS_SCENE_XPOS, paramYPos, theTitle)
		paramYPos += F8_ADD_Y
		
		INT iQueue
		REPEAT g_iCOUNT_OF_LAST_SCENE_QUEUE iQueue
			PED_REQUEST_SCENE_ENUM eScene = g_SavedGlobals.sPlayerSceneData.g_eLastSceneQueue[iPeds][iQueue]
			STRING theName = Get_String_From_Ped_Request_Scene_Enum(eScene)
			
			HUD_COLOURS lineColour = HUD_COLOUR_PURE_WHITE
			IF (eScene = PR_SCENE_INVALID)
				lineColour = HUD_COLOUR_REDLIGHT
			ENDIF
			
			Display_F8_Literal_Text(lineColour, F8_PLA_PREVIOUS_SCENE_XPOS, paramYPos, theName)
			paramYPos += F8_ADD_Y
			
		ENDREPEAT
		
	ENDREPEAT
	
ENDPROC
 

// *******************************************************************************************
//	F8 Screen Display
// *******************************************************************************************

// PURPOSE:	Display the titles for the different sections of the F8 screen
// 
PROC Display_F8_Titles()
	Display_F8_Literal_Text(HUD_COLOUR_GREEN, F8_TITLE_CONTROLLER_XPOS, 0.04, "FAMILY_CONTROLLER STATE")
	Display_F8_Literal_Text(HUD_COLOUR_GREEN, F8_TITLE_SWITCH_XPOS, 0.04, "PLAYER_SWITCH STATE")
	
ENDPROC

// PURPOSE:	Display the details for all family members
// 
PROC Display_F8_Family_Details()
		
	FLOAT theY = F8_START_YPOS
	
	enumFamilyMember loopAsStrandID
	REPEAT MAX_FAMILY_MEMBER loopAsStrandID
		Display_Family_Member(loopAsStrandID, theY)
		
		theY += F8_ADD_Y
		
		IF loopAsStrandID = FM_MICHAEL_GARDENER
		OR loopAsStrandID = FM_FRANKLIN_STRETCH
		OR loopAsStrandID = FM_TREVOR_0_WIFE
		OR loopAsStrandID = FM_TREVOR_1_WADE
			
			BOOL bFamilyScene = FALSE
			
			IF loopAsStrandID = FM_MICHAEL_GARDENER
				IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("family_scene_m")) > 0)
					Display_F8_Literal_Text(HUD_COLOUR_BLUE, F8_FAM_NAME_XPOS, theY, " - - family_scene_m running- - - - -")
					theY += F8_ADD_Y
					
					bFamilyScene = TRUE
				ENDIF
			ELIF loopAsStrandID = FM_FRANKLIN_STRETCH
				IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("family_scene_f0")) > 0)
					Display_F8_Literal_Text(HUD_COLOUR_BLUE, F8_FAM_NAME_XPOS, theY, " - - family_scene_f0 running- - - - -")
					theY += F8_ADD_Y
					
					bFamilyScene = TRUE
				ENDIF
				IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("family_scene_f1")) > 0)
					Display_F8_Literal_Text(HUD_COLOUR_BLUE, F8_FAM_NAME_XPOS, theY, " - - family_scene_f1 running- - - - -")
					theY += F8_ADD_Y
					
					bFamilyScene = TRUE
				ENDIF
			ELIF loopAsStrandID = FM_TREVOR_0_WIFE
				IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("family_scene_t0")) > 0)
					Display_F8_Literal_Text(HUD_COLOUR_BLUE, F8_FAM_NAME_XPOS, theY, " - - family_scene_t0 running - - - -")
					theY += F8_ADD_Y
					
					bFamilyScene = TRUE
				ENDIF
			ELIF loopAsStrandID = FM_TREVOR_1_WADE
				IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("family_scene_t1")) > 0)
					Display_F8_Literal_Text(HUD_COLOUR_BLUE, F8_FAM_NAME_XPOS, theY, " - - family_scene_t1 running - - - -")
					theY += F8_ADD_Y
					
					bFamilyScene = TRUE
				ENDIF
			ENDIF
			
			IF NOT bFamilyScene
				Display_F8_Literal_Text(HUD_COLOUR_GREYDARK, F8_FAM_NAME_XPOS, theY, " - - - - - - - - - - - - - - - - - -")
				theY += F8_ADD_Y
			ENDIF
		ENDIF

	ENDREPEAT
	
ENDPROC

// PURPOSE:	Display the details for all player switching
// 
PROC Display_F8_Switch_Details(PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene)
		
	FLOAT theY = F8_START_YPOS
	Display_Player_Time(theY, HUD_COLOUR_GREY)
	theY += F8_ADD_Y
	
	BOOL bGottenPlayerPedSceneForCurrentTime = FALSE
	
	enumCharacterList loopAsStrandID
	REPEAT NUM_OF_PLAYABLE_PEDS loopAsStrandID
		
		IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			BOOL bUpdateScene = FALSE
			
			IF NOT bGottenPlayerPedSceneForCurrentTime
				IF ((loopAsStrandID = eCurrentPlayerChar) AND (TIMERA() > 0250))
					bUpdateScene = TRUE
				ENDIF
			ENDIF
			
			IF NOT bGottenPlayerPedSceneForCurrentTime
				IF ((eCurrentPlayerEvents[loopAsStrandID] = PR_SCENE_INVALID) AND (loopAsStrandID <> GET_CURRENT_PLAYER_PED_ENUM()))
					bUpdateScene = TRUE
				ENDIF
			ENDIF
			
			IF NOT bGottenPlayerPedSceneForCurrentTime
				IF ((loopAsStrandID = eCurrentPlayerChar) AND (NOT IS_PLAYER_PED_AVAILABLE(loopAsStrandID)))
					bUpdateScene = TRUE
				ENDIF
			ENDIF
			
			IF bUpdateScene
				GET_PLAYER_PED_SCENE_FOR_CURRENT_TIME(loopAsStrandID, eCurrentPlayerEvents[loopAsStrandID], fCurrentPlayerScenePercent[loopAsStrandID], sPedScene, sPassedScene)
				
				UNUSED_PARAMETER(sPedScene)
				UNUSED_PARAMETER(sPassedScene)
				
				bGottenPlayerPedSceneForCurrentTime = TRUE
				SETTIMERA(0)
			ENDIF
			
		ELSE
			eCurrentPlayerEvents[loopAsStrandID] = g_eRecentlySelectedScene
			fCurrentPlayerScenePercent[loopAsStrandID] = 0.0
		ENDIF
		
		Display_Player_Char(loopAsStrandID, theY, IS_PLAYER_SWITCH_IN_PROGRESS())
		Display_Player_Blip(loopAsStrandID)
		
	ENDREPEAT
	
	IF bGottenPlayerPedSceneForCurrentTime
		INT iCurrentPlayerChar = ENUM_TO_INT(eCurrentPlayerChar)
		iCurrentPlayerChar++
		IF iCurrentPlayerChar >= NUM_OF_PLAYABLE_PEDS
			iCurrentPlayerChar = 0
		ENDIF
		eCurrentPlayerChar = INT_TO_ENUM(enumCharacterList, iCurrentPlayerChar)
	ENDIF
	
	Display_F8_Last_Switch_Queue_Details(theY)
	
ENDPROC
PROC Cleanup_F8_Switch_Details()

	enumCharacterList loopAsStrandID
	REPEAT NUM_OF_PLAYABLE_PEDS loopAsStrandID
		IF DOES_BLIP_EXIST(blipPlayerChars[loopAsStrandID])
			REMOVE_BLIP(blipPlayerChars[loopAsStrandID])
		ENDIF
	ENDREPEAT
	
	g_iDebugSelectedFriendConnDisplay = 0
	
ENDPROC

// *******************************************************************************************
//	F8 Select Debug Display
// *******************************************************************************************

// PURPOSE:	
// 
PROC Control_F8_Debug_Selection()
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
		INT iDebugSelectedMember = ENUM_TO_INT(g_eDebugSelectedMember)
		iDebugSelectedMember--
		IF iDebugSelectedMember < 0
			iDebugSelectedMember = ENUM_TO_INT(MAX_FAMILY_MEMBER)-1
		ENDIF
		g_eDebugSelectedMember = INT_TO_ENUM(enumFamilyMember, iDebugSelectedMember)
	ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
		INT iDebugSelectedMember = ENUM_TO_INT(g_eDebugSelectedMember)
		iDebugSelectedMember++
		IF iDebugSelectedMember >= ENUM_TO_INT(MAX_FAMILY_MEMBER)
			iDebugSelectedMember = 0
		ENDIF
		g_eDebugSelectedMember = INT_TO_ENUM(enumFamilyMember, iDebugSelectedMember)
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_L)
		g_bDrawDebugFamilyStuff = NOT g_bDrawDebugFamilyStuff
	ENDIF
	
ENDPROC

// *******************************************************************************************
//	F8 Select Debug Display
// *******************************************************************************************

// PURPOSE:	Displays the Family Controller F8 screen when allowed by other systems
// 
PROC Display_Family_Controller_F8_Screen(PED_SCENE_STRUCT &sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene)
	
	// Check if some other system has hidden this screen
	IF NOT g_flowUnsaved.bShowMissionFlowDebugScreen
		g_iDebugSelectedFriendConnDisplay = 0
		g_bDrawLiteralSceneString = FALSE
		EXIT
	ENDIF
	
	Display_F8_Background()
	Display_F8_Titles()
	
	Display_F8_Family_Details()
	Control_Update_Family_Member_Event()
	
	Display_F8_Switch_Details(sPedScene, sPassedScene)
	
	Control_F8_Debug_Selection()
	
	g_iDebugSelectedFriendConnDisplay = 1
	
	g_bDrawLiteralSceneString = TRUE
	
ENDPROC
