USING "commands_script.sch"
USING "commands_misc.sch"
USING "globals.sch"

#IF IS_DEBUG_BUILD
	CONST_INT CAM_EDITOR_FLAGS_FORCE_PLAYBACK	0
	
	PROC START_CAM_EDITOR_PLAYBACK_THIS_FRAME()
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("scripted_cam_editor")) > 0
			IF NOT IS_BIT_SET(g_iScriptedCamEditorFlags, CAM_EDITOR_FLAGS_FORCE_PLAYBACK)
				SET_BIT(g_iScriptedCamEditorFlags, CAM_EDITOR_FLAGS_FORCE_PLAYBACK)
			ENDIF
		ENDIF
	ENDPROC
	
	PROC SAVE_CAM_EDITOR_NODE_DATA_TO_NAMED_DEBUG_FILE(INT j, 
			VECTOR v_pos,
			VECTOR v_rot,
			FLOAT f_fov,
			INT i_duration, //Used for both spline and interp duration if relevant.
			
			//Interp-only variables
			INT i_pos_graph_type,
			INT i_rot_graph_type,
			
			//Spline-only variables
			INT i_spline_node_flags,
			STRING str_path, STRING str_file)
	
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<node node_id = \"", str_path, str_file)
		SAVE_INT_TO_NAMED_DEBUG_FILE(j, str_path, str_file)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE(" pos_x = \"", str_path, str_file)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(v_pos.x, str_path, str_file)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE(" pos_y = \"", str_path, str_file)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(v_pos.y, str_path, str_file)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE(" pos_z = \"", str_path, str_file)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(v_pos.z, str_path, str_file)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE(" rot_x = \"", str_path, str_file)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(v_rot.x, str_path, str_file)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE(" rot_y = \"", str_path, str_file)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(v_rot.y, str_path, str_file)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE(" rot_z = \"", str_path, str_file)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(v_rot.z, str_path, str_file)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE(" f_fov = \"", str_path, str_file)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(f_fov, str_path, str_file)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE(" i_node_duration = \"", str_path, str_file)
		SAVE_INT_TO_NAMED_DEBUG_FILE(i_duration, str_path, str_file)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE(" i_pos_graph = \"", str_path, str_file)
		SAVE_INT_TO_NAMED_DEBUG_FILE(i_pos_graph_type, str_path, str_file)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE(" i_rot_graph = \"", str_path, str_file)
		SAVE_INT_TO_NAMED_DEBUG_FILE(i_rot_graph_type, str_path, str_file)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE(" i_node_smooth = \"", str_path, str_file)
		SAVE_INT_TO_NAMED_DEBUG_FILE(i_spline_node_flags, str_path, str_file)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\"/>", str_path, str_file)
	
	ENDPROC
	
#ENDIF
