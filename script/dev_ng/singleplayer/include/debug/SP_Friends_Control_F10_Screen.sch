USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"

USING "friends_debug.sch"

#if USE_CLF_DLC
	USING "static_mission_data_helpCLF.sch"
#endif
#if USE_NRM_DLC
	USING "static_mission_data_helpNRM.sch"
#endif
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	SP_Friends_Control_F10_Screen.sch
//		AUTHOR			:	Alwyn (from Keiths SP_Mission_Flow_F9_Screen)
//		DESCRIPTION		:	Updates the F10 Screen display.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// Standard F10 Screen Text Scaling and Positioning Values

CONST_FLOAT	F10_TEXT_SCALE_X				0.25
CONST_FLOAT	F10_TEXT_SCALE_Y				0.3
CONST_FLOAT	F10_TEXT_WRAP_XPOS_START		0.0
CONST_FLOAT	F10_TEXT_WRAP_XPOS_END			1.0
CONST_FLOAT	F10_START_YPOS					0.075
CONST_FLOAT	F10_ADD_Y						0.02

CONST_FLOAT F10_TITLE_CONTROLLER_XPOS		0.135
CONST_FLOAT F10_TITLE_TIMER_XPOS			0.7795

CONST_FLOAT F10_NAME_XSPACE					0.11//0.15
CONST_FLOAT F10_STRING_XSPACE				0.065
CONST_FLOAT F10_INT_XSPACE					0.040
CONST_FLOAT F10_GAP_XSPACE					0.08//0.1

FLOAT		F10_FRI_NAME_XPOS				= 0.05												//	0.055
FLOAT		F10_FRI_STATE_XPOS				= F10_FRI_NAME_XPOS			+ F10_NAME_XSPACE		//	0.205	//	0.205
FLOAT		F10_FRI_MODE_XPOS				= F10_FRI_STATE_XPOS		+ F10_STRING_XSPACE		//	0.205	//	0.205
FLOAT		F10_FRI_BLOCK_XPOS				= F10_FRI_MODE_XPOS			+ F10_STRING_XSPACE		//	0.205	//	0.205
FLOAT		F10_FRI_MISSID_XPOS				= F10_FRI_BLOCK_XPOS		+ F10_INT_XSPACE		//	0.455	//	0.455
FLOAT		F10_FRI_COMMID_XPOS				= F10_FRI_MISSID_XPOS		+ F10_STRING_XSPACE		//	0.288333333	//	0.305

FLOAT		F10_FRI_LIKES_XPOS				= F10_FRI_COMMID_XPOS		+ F10_INT_XSPACE		//	0.371666667	//	0.405
FLOAT		F10_FRI_CONVCOUNT_XPOS			= F10_FRI_LIKES_XPOS		+ F10_INT_XSPACE		//	0.621666667	//	0.605
FLOAT		F10_FRI_CONTTIME_XPOS			= F10_FRI_CONVCOUNT_XPOS	+ F10_INT_XSPACE		//	0.705	//	0.705
FLOAT		F10_FRI_CONTTYPE_XPOS			= F10_FRI_CONTTIME_XPOS		+ F10_STRING_XSPACE		//	0.788333333		
FLOAT		F10_FRI_WANTED_XPOS				= F10_FRI_CONTTYPE_XPOS		+ F10_STRING_XSPACE		//	0.788333333
FLOAT		F10_FRI_COMPLETE_XPOS			= F10_FRI_WANTED_XPOS		+ F10_INT_XSPACE		//	0.538333333	//	0.505
FLOAT		F10_FRI_FAIL_XPOS				= F10_FRI_COMPLETE_XPOS		+ F10_INT_XSPACE

CONST_FLOAT	F10_FRI_ACTSCRIPT_XPOS			F10_TITLE_TIMER_XPOS
//CONST_FLOAT	F10_FRI_TIMER_XPOS			F10_TITLE_TIMER_XPOS

// *******************************************************************************************
BOOL		bChangeDebugF10SelectedConnection	= FALSE


ENUM enumF10Connection
	f10Connection_0_state = 0,
	f10Connection_mode,
	f10Connection_1_block,
	f10Connection_2_missID,
	f10Connection_3_commID,
	f10Connection_4_likes,
	f10Connection_5_conv,
	f10Connection_6_contTime,
	f10Connection_7_contType,
	f10Connection_8_wanted,
	f10Connection_9_complete,
	f10Connection_9b_fail,
	
//	f10Connection_10_g_nextContact,
	f10Connection_10_actScript,
	
	NUMBER_OF_F10CONNECTIONS,
	f10Connection_NULL = -1
ENDENUM
enumF10Connection eChangeDebugF10SelectedConnection	= f10Connection_NULL

// *******************************************************************************************
//		Standard F10 Screen Display Controls
// *******************************************************************************************


// *******************************************************************************************

// PURPOSE: Display the re-sizeable background for the general F10 screen
// 
PROC Display_F10_Background()

	// Calculate the background rectangle height
	FLOAT	textBoxYSize	= F10_ADD_Y * (ENUM_TO_INT(MAX_FRIEND_CONNECTIONS)+ENUM_TO_INT(MAX_FRIEND_GROUPS)+1)
	FLOAT	boxYBorder		= F10_START_YPOS + (F10_ADD_Y * 0.5)
	FLOAT	totalBoxYSize	= textBoxYSize + boxYBorder
	
	FLOAT	boxYCentre		= totalBoxYSize * 0.5
	
	DRAW_RECT(0.5, boxYCentre, 1.0, totalBoxYSize, 0, 0, 0, 175)

ENDPROC


// *******************************************************************************************
//		Interface With Native Commands
// *******************************************************************************************

// PURPOSE:	Set up the standard text display used for the F10 screen
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B

PROC Setup_F10_Text_Display(HUD_COLOURS paramColour)

	INT theR, theG, theB, theA
	GET_HUD_COLOUR(paramColour, theR, theG, theB, theA)
	SET_TEXT_COLOUR(theR, theG, theB, theA)
	
	SET_TEXT_SCALE(F10_TEXT_SCALE_X, F10_TEXT_SCALE_Y)
	SET_TEXT_WRAP(F10_TEXT_WRAP_XPOS_START, F10_TEXT_WRAP_XPOS_END)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)

ENDPROC




// *******************************************************************************************
//		Generic F10 Screen Display Commands
// *******************************************************************************************

// PURPOSE:	Set up the standard literal text display used for the F10 screen and display text with it
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B
//					paramXPos			Text X pos
//					paramYPos			Text Y pos
//					paramText			The literal string to be output

PROC Display_F10_Literal_Text(HUD_COLOURS paramColour, FLOAT paramXPos, FLOAT paramYPos, STRING paramText)

	Setup_F10_Text_Display(paramColour)
	DISPLAY_TEXT_WITH_LITERAL_STRING(paramXPos, paramYPos, "STRING", paramText)

ENDPROC

//// PURPOSE:	Set up the standard textLabel text display
////
//// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B
////					paramXPos			Text X pos
////					paramYPos			Text Y pos
////					paramTextLabel		The textLabel to be output
//
//PROC Display_F10_TextLabel(HUD_COLOURS paramColour, FLOAT paramXPos, FLOAT paramYPos, STRING paramTextLabel)
//
//	Setup_F10_Text_Display(paramColour)
//	DISPLAY_TEXT(paramXPos, paramYPos, paramTextLabel)
//
//ENDPROC
//
//// PURPOSE:	Set up the standard textLabel text display with 1 number used for the F10 screen
////
//// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B
////					paramXPos			Text X pos
////					paramYPos			Text Y pos
////					paramTextLabel		The textLabel to be output
////					paramInt			The first number
//
//PROC Display_F10_TextLabel_With_One_Number(HUD_COLOURS paramColour, FLOAT paramXPos, FLOAT paramYPos, STRING paramTextLabel, INT paramInt)
//
//	Setup_F10_Text_Display(paramColour)
//	DISPLAY_TEXT_WITH_NUMBER(paramXPos, paramYPos, paramTextLabel, paramInt)
//
//ENDPROC
//
//// PURPOSE:	Set up the standard textLabel text display with 2 numbers used for the F10 screen
////
//// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B
////					paramXPos			Text X pos
////					paramYPos			Text Y pos
////					paramTextLabel		The textLabel to be output
////					paramInt1			The first number
////					paramInt2			The second number
//
//PROC Display_F10_TextLabel_With_Two_Numbers(HUD_COLOURS paramColour, FLOAT paramXPos, FLOAT paramYPos, STRING paramTextLabel, INT paramInt1, INT paramInt2)
//
//	Setup_F10_Text_Display(paramColour)
//	DISPLAY_TEXT_WITH_2_NUMBERS(paramXPos, paramYPos, paramTextLabel, paramInt1, paramInt2)
//
//ENDPROC
//
//// PURPOSE: Display the timer progress bar that should display behind each communication timer line.
//// 
//PROC Display_F10_Timer_Background_Bar(HUD_COLOURS paramColour, INT paramTimerCount, INT paramTimerMax, FLOAT paramYPos)
//	
//	FLOAT fTopX = F10_FRI_TIMER_XPOS - 0.0025
//	FLOAT fWidth = ((F10_FRI_TIMER_XPOS-fTopX)+0.125) * (TO_FLOAT(paramTimerCount)/TO_FLOAT(paramTimerMax))
//	
//	FLOAT fTopY = paramYPos + 0.0025
//	FLOAT fHeight = F10_ADD_Y
//
//	INT theR, theG, theB, theA
//	GET_HUD_COLOUR(paramColour, theR, theG, theB, theA)
//	theR = ROUND(TO_FLOAT(theR)*0.5)
//	theG = ROUND(TO_FLOAT(theG)*0.5)
//	theB = ROUND(TO_FLOAT(theB)*0.5)
//	theA = ROUND(TO_FLOAT(theA)*0.5)
//	
//	DRAW_RECT_FROM_CORNER(fTopX, fTopY, fWidth, fHeight, theR, theG, theB, theA)
//ENDPROC



// *******************************************************************************************
//	Specific F10 Friends Screen Command Display Messages
// *******************************************************************************************

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Friends_Connection_Name(enumFriendConnection paramFriendConID, FLOAT paramYPos, HUD_COLOURS paramColour)
	
	// If friend A is NPC
	IF g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].friendA >= INT_TO_ENUM(enumFriend, NUM_OF_PLAYABLE_PEDS)
		TEXT_LABEL_63 theName = GetLabel_enumFriend(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].friendA)
		theName += " ("
		theName += GetLabel_enumFriend(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].friendB)
		theName += ")"
		Display_F10_Literal_Text(paramColour, F10_FRI_NAME_XPOS, paramYPos, theName)
	
	// If friend B is NPC
	ELIF g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].friendB >= INT_TO_ENUM(enumFriend, NUM_OF_PLAYABLE_PEDS)
		TEXT_LABEL_63 theName = GetLabel_enumFriend(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].friendB)
		theName += " ("
		theName += GetLabel_enumFriend(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].friendA)
		theName += ")"
		Display_F10_Literal_Text(paramColour, F10_FRI_NAME_XPOS, paramYPos, theName)
	
	ELSE
		// Both are playable, one must be player
//		enumFriend eFriend
//		IF GET_OTHER_FRIEND_FROM_CONNECTION(paramFriendConID, eFriend)
//			TEXT_LABEL_63 theName = GetLabel_enumFriend(eFriend)
//			theName += " ("
//			theName += GetLabel_enumFriend(GET_FRIEND_FROM_CHAR(g_eDefaultPlayerChar))
//			theName += ")"
//			
//			Display_F10_Literal_Text(paramColour, F10_FRI_NAME_XPOS, paramYPos, theName)
//		
//		// Neither are player, Shouldn't be happening but just in case
//		ELSE
			STRING theName = GetLabel_enumFriendConnection(paramFriendConID)
			Display_F10_Literal_Text(paramColour, F10_FRI_NAME_XPOS, paramYPos, theName)
//		ENDIF
	ENDIF
	
ENDPROC


// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Friends_Connection_State(enumFriendConnection paramFriendConID, FLOAT paramYPos, HUD_COLOURS paramColour, BOOL bEditing)
	IF bEditing
		paramColour = HUD_COLOUR_YELLOW
	ENDIF
	
	enumFriendConnectionState thisFriendsState = GET_CONNECTION_STATE(paramFriendConID)
	
	STRING theName = GetLabel_enumFriendConnectionState(thisFriendsState)
	Display_F10_Literal_Text(paramColour, F10_FRI_STATE_XPOS, paramYPos, theName)

ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Friends_Connection_Mode(enumFriendConnection paramFriendConID, FLOAT paramYPos, HUD_COLOURS paramColour, BOOL bEditing)
	IF bEditing
		paramColour = HUD_COLOUR_YELLOW
	ENDIF
	
	enumFriendConnectionMode thisFriendsMode = g_FriendConnectState[paramFriendConID].mode
	
	STRING theName = GetLabel_enumFriendConnectionMode(thisFriendsMode)
	Display_F10_Literal_Text(paramColour, F10_FRI_MODE_XPOS, paramYPos, theName)

ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Friends_Connection_Block(enumFriendConnection paramFriendConID, FLOAT paramYPos, HUD_COLOURS paramColour, BOOL bEditing)
	IF bEditing
		paramColour = HUD_COLOUR_YELLOW
	ENDIF
	
	TEXT_LABEL_63 theName = GetLabel_FriendBlockBits(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].blockBits)
	
	Display_F10_Literal_Text(paramColour, F10_FRI_BLOCK_XPOS, paramYPos, theName)
	
ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Friends_Connection_MissID(enumFriendConnection paramFriendConID, FLOAT paramYPos, HUD_COLOURS paramColour, BOOL bEditing)
	IF bEditing
		paramColour = HUD_COLOUR_YELLOW
	ENDIF
	
	SP_MISSIONS eMissionID = g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].blockMissionID
	
	TEXT_LABEL_63 theName = ""
	IF eMissionID != SP_MISSION_NONE
	
		IF eMissionID < SP_MISSION_MAX
			#if USE_CLF_DLC
				theName += GET_SCRIPT_FROM_HASH(g_sMissionStaticData[eMissionID].scriptHash)
			#endif
			#if USE_NRM_DLC
				theName += GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[eMissionID].scriptHash)
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				theName += g_sMissionStaticData[eMissionID].scriptName	
			#endif
			#endif
		ELIF eMissionID = SP_MISSION_MAX
			theName += "<max>"
			CPRINTLN(DEBUG_FRIENDS, "Display_Friends_Connection_MissID(", GetLabel_enumFriendConnection(paramFriendConID), ") - blockMissionID is SP_MISSION_MAX")
			SCRIPT_ASSERT("Display_Friends_Connection_MissID() - blockMissionID is SP_MISSION_MAX")
		ELSE
			theName += "error"
			CPRINTLN(DEBUG_FRIENDS, "Display_Friends_Connection_MissID(", GetLabel_enumFriendConnection(paramFriendConID), ") - blockMissionID is not a known value")
			SCRIPT_ASSERT("Display_Friends_Connection_MissID() - blockMissionID is not a known value")
		ENDIF

		Display_F10_Literal_Text(paramColour, F10_FRI_MISSID_XPOS, paramYPos, theName)
	
	ENDIF
	
ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Friends_Connection_CommID(enumFriendConnection paramFriendConID, FLOAT paramYPos, HUD_COLOURS paramColour, BOOL bEditing)
	IF bEditing
		paramColour = HUD_COLOUR_YELLOW
	ENDIF
	
	CC_CommID thisFriendsCommID = g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].commID
	thisFriendsCommID = TEXT_FRIEND
	
	STRING theName
	IF (thisFriendsCommID <> COMM_NONE)
		theName = GET_COMM_ID_DEBUG_STRING(thisFriendsCommID)
	ELSE
		theName = "invalid"
	ENDIF
	Display_F10_Literal_Text(paramColour, F10_FRI_COMMID_XPOS, paramYPos, theName)
	
ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Friends_Connection_Likes(enumFriendConnection paramFriendConID, FLOAT paramYPos, HUD_COLOURS paramColour, BOOL bEditing)
	IF bEditing
		paramColour = HUD_COLOUR_YELLOW
	ENDIF

	TEXT_LABEL_63 theName = ""
	theName += g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].likes

	Display_F10_Literal_Text(paramColour, F10_FRI_LIKES_XPOS, paramYPos, theName)
		
ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Friends_Connection_convCount(enumFriendConnection paramFriendConID, FLOAT paramYPos, HUD_COLOURS paramColour, BOOL bEditing)
	IF bEditing
		paramColour = HUD_COLOUR_YELLOW
	ENDIF
	
	TEXT_LABEL_63 theName = ""
	enumFriendChatType eType
	INT iBank, iIndex
	
	// Get bank and index for each chat type
	REPEAT MAX_FCHAT_TYPES eType
		IF paramFriendConID = FC_FRANKLIN_LAMAR 
		AND eType = FCHAT_TypeMini 
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
		AND GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_RESPAWNED_AFTER_FINALE)
		#endif
		#endif
			// Special Franklin/Lamar end-of-game mini chat banks
			IF Private_GetFriendChatIndex(g_SavedGlobals.sFriendsData.g_FranklinLamarEndChat, eType, iBank, iIndex, Private_GetFriendChatConditions())
				IF iBank = 0	theName += "G"
				ELIF iBank = 1	theName += "H"
				ELIF iBank = 2	theName += "I"
				ELIF iBank = 3	theName += "J"
				ELIF iBank = 4	theName += "K"
				ELIF iBank = 5	theName += "L"
				ELSE			theName += "Y"
				ENDIF
				
				theName += iIndex
			ELSE
				theName += "na"
			ENDIF
		ELSE
			// Normal bank
			IF Private_GetFriendChatIndex(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].chatData, eType, iBank, iIndex, Private_GetFriendChatConditions())
				IF iBank = 0	theName += "A"
				ELIF iBank = 1	theName += "B"
				ELIF iBank = 2	theName += "C"
				ELIF iBank = 3	theName += "D"
				ELIF iBank = 4	theName += "E"
				ELIF iBank = 5	theName += "F"
				ELSE			theName += "X"
				ENDIF
				
				theName += iIndex
			ELSE
				theName += "na"
			ENDIF
		ENDIF
	ENDREPEAT
	
	Display_F10_Literal_Text(paramColour, F10_FRI_CONVCOUNT_XPOS, paramYPos, theName)
	
ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Friends_Connection_LastContactTime(enumFriendConnection paramFriendConID, FLOAT paramYPos, HUD_COLOURS paramColour, BOOL bEditing)
	IF bEditing
		paramColour = HUD_COLOUR_YELLOW
	ENDIF
	
	structTimer theTimer = g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].lastContactTimer
	TEXT_LABEL_63 theName
	
	IF NOT IS_TIMER_STARTED(theTimer)
		theName = "STOPPED"
	ELSE
		theName = GET_STRING_FROM_MILISECONDS(ROUND(GET_TIMER_IN_SECONDS(theTimer) * 1000.0))
		IF IS_TIMER_PAUSED(theTimer)
			theName += "(p)"
		ENDIF
	ENDIF
	
	Display_F10_Literal_Text(paramColour, F10_FRI_CONTTIME_XPOS, paramYPos, theName)

ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Friends_Connection_LastContactType(enumFriendConnection paramFriendConID, FLOAT paramYPos, HUD_COLOURS paramColour, BOOL bEditing)
	IF bEditing
		paramColour = HUD_COLOUR_YELLOW
	ENDIF
	
	TEXT_LABEL_63 theName
	SWITCH g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].lastContactType
	
		CASE FRIEND_CONTACT_PHONE
			theName = "phone"
		BREAK
	
		CASE FRIEND_CONTACT_FACE
			theName = "face"
		BREAK
	
		CASE FRIEND_CONTACT_DISMISSED
			theName = "dismiss"
		BREAK
		
		DEFAULT
			theName = "error"
		BREAK

	ENDSWITCH
	
	Display_F10_Literal_Text(paramColour, F10_FRI_CONTTYPE_XPOS, paramYPos, theName)

ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Friends_Connection_Wanted(enumFriendConnection paramFriendConID, FLOAT paramYPos, HUD_COLOURS paramColour, BOOL bEditing)
	IF bEditing
		paramColour = HUD_COLOUR_YELLOW
	ENDIF
	
	enumFriend eWanted = g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].wanted
	
	TEXT_LABEL_63 theName = ""
	IF eWanted <> NO_FRIEND
		theName += GetLabel_enumFriend(eWanted)
		Display_F10_Literal_Text(paramColour, F10_FRI_WANTED_XPOS, paramYPos, theName)
	ENDIF
	
ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Friends_Connection_Complete(enumFriendConnection paramFriendConID, FLOAT paramYPos, HUD_COLOURS paramColour, BOOL bEditing)
	IF bEditing
		paramColour = HUD_COLOUR_YELLOW
	ENDIF
	
	TEXT_LABEL_63 theName = ""
	IF HAS_CONNECTION_BEEN_PLAYED(paramFriendConID)
		theName = "yes"
		Display_F10_Literal_Text(paramColour, F10_FRI_COMPLETE_XPOS, paramYPos, theName)
	ENDIF
	
ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC Display_Friends_Connection_Fail(enumFriendConnection paramFriendConID, FLOAT paramYPos, HUD_COLOURS paramColour, BOOL bEditing)
	IF bEditing
		paramColour = HUD_COLOUR_YELLOW
	ENDIF
	
	TEXT_LABEL_63 theName = ""
	enumFriend eFriend
	
	IF NOT GET_OTHER_FRIEND_FROM_CONNECTION(paramFriendConID, eFriend)
		theName += "/"
	
	ELSE
		IF NOT IS_TIMER_STARTED(g_SavedGlobals.sFriendsData.g_FriendFailTimers[eFriend])
			theName += "none"

		ELSE
			theName += GET_STRING_FROM_MILISECONDS(ROUND(GET_TIMER_IN_SECONDS(g_SavedGlobals.sFriendsData.g_FriendFailTimers[eFriend]) * 1000.0))
				
			IF IS_TIMER_PAUSED(g_SavedGlobals.sFriendsData.g_FriendFailTimers[eFriend])
				theName += "(p)"
			ENDIF
		ENDIF
	ENDIF
	
	Display_F10_Literal_Text(paramColour, F10_FRI_FAIL_XPOS, paramYPos, theName)
	
ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text
PROC Display_Friends_Group_Name(enumFriendGroup paramFriendGroupID, FLOAT paramYPos, HUD_COLOURS paramColour)
	
	STRING theName = GetLabel_enumFriendGroup(paramFriendGroupID)
	Display_F10_Literal_Text(paramColour, F10_FRI_NAME_XPOS, paramYPos, theName)
	
ENDPROC

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text
PROC Display_Friends_Group_convCount(enumFriendGroup paramFriendGroupID, FLOAT paramYPos, HUD_COLOURS paramColour, BOOL bEditing)
	IF bEditing
		paramColour = HUD_COLOUR_YELLOW
	ENDIF
	
	TEXT_LABEL_63 theName = ""
	enumFriendChatType eType
	INT iBank, iIndex
	
	// Get bank and index for each chat type
	REPEAT MAX_FCHAT_TYPES eType
		IF Private_GetFriendChatIndex(g_SavedGlobals.sFriendsData.g_FriendGroupData[paramFriendGroupID].chatData, eType, iBank, iIndex, Private_GetFriendChatConditions())
			IF iBank = 0		theName += "A"		theName += iIndex
			ELIF iBank = 1		theName += "B"		theName += iIndex
			ELIF iBank = 2		theName += "C"		theName += iIndex
			ELIF iBank = 3		theName += "D"		theName += iIndex
			ELIF iBank = 4		theName += "E"		theName += iIndex
			ELIF iBank = 5		theName += "F"		theName += iIndex
			ELSE				theName += "xx"
			ENDIF
		ELSE
			theName += "na"
		ENDIF
	ENDREPEAT
	
	Display_F10_Literal_Text(paramColour, F10_FRI_CONVCOUNT_XPOS, paramYPos, theName)
	
ENDPROC

// *******************************************************************************************
//	F10 Friends Screen Strand Display
// *******************************************************************************************

// PURPOSE: Display the details for one strand
//
// INPUT PARAMS:	paramFriendConID		The Strand ID
//					paramYPos			The screen Y Pos for these details
FUNC BOOL Control_Update_Friends_Member_Connection(enumFriendConnection paramFriendConID)
	
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		bChangeDebugF10SelectedConnection = NOT bChangeDebugF10SelectedConnection
		
		IF NOT bChangeDebugF10SelectedConnection
			IF eChangeDebugF10SelectedConnection > f10Connection_NULL
				eChangeDebugF10SelectedConnection = f10Connection_NULL
			ENDIF
		ENDIF
	ENDIF
	
	CONST_FLOAT	totalBoxXOffset		0.04
	CONST_FLOAT	totalBoxXSize		0.24
	CONST_FLOAT	totalYextXOffset	0.00
	
	// Calculate the selection rectangle height
	FLOAT	totalTextBoxYSize_a		=  F10_ADD_Y * 3
	FLOAT	totalTextBoxYSize_b		=  F10_ADD_Y * 1
	FLOAT	boxYBorder				= (F10_ADD_Y * 0.5)
	FLOAT	totalBoxYSize_a			= totalTextBoxYSize_a + boxYBorder
	FLOAT	totalBoxYSize_b			= totalTextBoxYSize_b + boxYBorder
	
	FLOAT	boxYCentre_a			= totalBoxYSize_a * 0.5
	FLOAT	boxYCentre_b			= totalBoxYSize_b * 0.5
	
	// Calculate the background rectangle height
	FLOAT	detailsTextBoxYSize		= F10_ADD_Y * (ENUM_TO_INT(MAX_FRIEND_CONNECTIONS)+ENUM_TO_INT(MAX_FRIEND_GROUPS)+1)
	FLOAT	detailsBoxYBorder		= F10_START_YPOS + (F10_ADD_Y * 0.5)
	FLOAT	detailsBoxXSize			= detailsTextBoxYSize + detailsBoxYBorder
	
	FLOAT	testParamXPos			= totalBoxXSize + totalYextXOffset - (F10_TITLE_CONTROLLER_XPOS+totalBoxXOffset)
	FLOAT	testParamYPos_a			= (boxYCentre_a+detailsBoxXSize)-(totalBoxYSize_a*0.5)
	FLOAT	testParamYPos_b			= (boxYCentre_b+detailsBoxXSize)-(totalBoxYSize_b*0.5)
	
	IF bChangeDebugF10SelectedConnection
		IF eChangeDebugF10SelectedConnection < f10Connection_0_state
			eChangeDebugF10SelectedConnection = f10Connection_0_state
		ENDIF
		
		DRAW_RECT(F10_TITLE_CONTROLLER_XPOS+totalBoxXOffset, boxYCentre_a+detailsBoxXSize,
				totalBoxXSize, totalBoxYSize_a,
				0, 0, 0, 175)
		Display_F10_Literal_Text(HUD_COLOUR_PURE_WHITE, testParamXPos, testParamYPos_a + (F10_ADD_Y*0), "Press <- and -> to change, 'J' to return")
		
		TEXT_LABEL_63 strKeyK
		TEXT_LABEL_63 strKeyH
		
		strKeyK = "    K: "
		strKeyH = "    H: "
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)
			INT iChangeDebugF10SelectedConnection = ENUM_TO_INT(eChangeDebugF10SelectedConnection)
			iChangeDebugF10SelectedConnection++
			
			IF iChangeDebugF10SelectedConnection >= ENUM_TO_INT(NUMBER_OF_F10CONNECTIONS)
				iChangeDebugF10SelectedConnection = 0
			ENDIF
			
			eChangeDebugF10SelectedConnection = INT_TO_ENUM(enumF10Connection, iChangeDebugF10SelectedConnection)
		ENDIF
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_LEFT)
			INT iChangeDebugF10SelectedConnection = ENUM_TO_INT(eChangeDebugF10SelectedConnection)
			iChangeDebugF10SelectedConnection--
			
			IF iChangeDebugF10SelectedConnection < 0
				iChangeDebugF10SelectedConnection = ENUM_TO_INT(NUMBER_OF_F10CONNECTIONS)-1
			ENDIF
			
			eChangeDebugF10SelectedConnection = INT_TO_ENUM(enumF10Connection, iChangeDebugF10SelectedConnection)
		ENDIF
		
		SWITCH eChangeDebugF10SelectedConnection
			CASE f10Connection_0_state		//Display_Friends_Connection_State()
				SWITCH GET_CONNECTION_STATE(paramFriendConID)
					CASE FC_STATE_Invalid
						strKeyK += "add char as friend"
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
							enumFriend eFriend
							IF GET_OTHER_FRIEND_FROM_CONNECTION(paramFriendConID, eFriend)
								Add_Char_As_DefaultPlayer_Friend(GET_CHAR_FROM_FRIEND(eFriend))
							ENDIF
						ENDIF
					BREAK
					CASE FC_STATE_ContactWait
						enumFriend eFriend
						IF GET_OTHER_FRIEND_FROM_CONNECTION(paramFriendConID, eFriend)
							IF Private_GET_FRIEND_LAST_CONTACT_TIME(g_eDefaultPlayerChar, GET_CHAR_FROM_FRIEND(eFriend)) < 5.0*60.0
								strKeyH += "add 5 hours"
								IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_H)
									Private_ALTER_FRIEND_LAST_CONTACT_TIME(g_eDefaultPlayerChar, GET_CHAR_FROM_FRIEND(eFriend), 5.0*60.0)
								ENDIF
							ENDIF
							
							IF CAN_CHARS_ARRANGE_FRIEND_ACTIVITY(g_eDefaultPlayerChar, GET_CHAR_FROM_FRIEND(eFriend))
								IF PRIVATE_FriendAcceptsPlayer_group(GET_FRIEND_FROM_CHAR(g_eDefaultPlayerChar), eFriend)
									strKeyK += "activity with friend"
									IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
										CPRINTLN(DEBUG_FRIENDS, "Debug forcing friend activity")
										
										// If trying to force adhoc mode, try and get local adhoc location
										IF GET_CONNECTION_MODE(paramFriendConID) = FC_MODE_Adhoc
											IF IS_PED_INJURED(PLAYER_PED_ID()) OR NOT Private_GetAvailableAdhocFriendLoc(GET_ENTITY_COORDS(PLAYER_PED_ID()))
												SET_CONNECTION_MODE(paramFriendConID, FC_MODE_Friend)
											ENDIF
										ENDIF
										
										SET_CONNECTION_FLAG(paramFriendConID, FC_FLAG_HasCallConnected, TRUE)
										SET_CONNECTION_STATE(paramFriendConID, FC_STATE_PhoneAccept)
									ENDIF
								ELSE
									strKeyK += "BLOCKED BY ACTIVE CONNECTION(S)"
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE FC_STATE_Init		FALLTHRU
					CASE FC_STATE_Active
						//
					BREAK
					
					DEFAULT
						TEXT_LABEL_63 tConnection
						TEXT_LABEL_63 tState
						TEXT_LABEL_63 tWillRun
						tConnection	= GetLabel_enumFriendConnection(paramFriendConID)
						tState		= GetLabel_enumFriendConnectionState(GET_CONNECTION_STATE(paramFriendConID))
						IF GET_OTHER_FRIEND_FROM_CONNECTION(paramFriendConID, eFriend)
							tWillRun = "<will run this frame>"
						ELSE
							tWillRun = "<won't run this frame>"
						ENDIF
						CPRINTLN(DEBUG_FRIENDS, "GET_CONNECTION_STATE(", tConnection, ") = ", tState, " ", tWillRun)
					BREAK
				ENDSWITCH
				
			BREAK
			CASE f10Connection_mode		//Display_Friends_Connection_Mode()
				IF GET_CONNECTION_STATE(paramFriendConID) = FC_STATE_ContactWait
					strKeyK += "cycle init mode"
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
						INT iNewMode
						iNewMode = ENUM_TO_INT(g_FriendConnectState[paramFriendConID].mode) + 1
						IF iNewMode < ENUM_TO_INT(FC_MODE_Ambient)//ENUM_TO_INT(MAX_FRIEND_INIT_MODES)
							g_FriendConnectState[paramFriendConID].mode = INT_TO_ENUM(enumFriendConnectionMode, iNewMode)
						ELSE
							g_FriendConnectState[paramFriendConID].mode = FC_MODE_Friend
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE f10Connection_1_block		//Display_Friends_Connection_Block()
				IF GET_CONNECTION_STATE(paramFriendConID) <> FC_STATE_Invalid
					enumFriend eFriend
					IF GET_OTHER_FRIEND_FROM_CONNECTION(paramFriendConID, eFriend)
						IF NOT IS_FRIEND_BLOCK_FLAG_SET(g_eDefaultPlayerChar, GET_CHAR_FROM_FRIEND(eFriend), FRIEND_BLOCK_FLAG_HIATUS)
							strKeyK += "set 'hiatus' block flag"
							IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
								SET_FRIEND_BLOCK_FLAG(g_eDefaultPlayerChar, GET_CHAR_FROM_FRIEND(eFriend), FRIEND_BLOCK_FLAG_HIATUS)
							ENDIF
						ELSE
							strKeyK += "clear 'hiatus' block flag"
							IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
								CLEAR_FRIEND_BLOCK_FLAG(g_eDefaultPlayerChar, GET_CHAR_FROM_FRIEND(eFriend), FRIEND_BLOCK_FLAG_HIATUS)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE f10Connection_2_missID		//Display_Friends_Connection_MissID()
			BREAK
			CASE f10Connection_3_commID		//Display_Friends_Connection_CommID()
			BREAK
			CASE f10Connection_4_likes		//Display_Friends_Connection_Likes()
				IF GET_CONNECTION_STATE(paramFriendConID) <> FC_STATE_Invalid
					strKeyK += "likes +5"
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
						enumFriend eFriend
						IF GET_OTHER_FRIEND_FROM_CONNECTION(paramFriendConID, eFriend)
							ADD_TO_FRIEND_LIKE(g_eDefaultPlayerChar, GET_CHAR_FROM_FRIEND(eFriend), 5)
						ENDIF
					ENDIF
					strKeyH += "likes -5"
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_H)
						enumFriend eFriend
						IF GET_OTHER_FRIEND_FROM_CONNECTION(paramFriendConID, eFriend)
							ADD_TO_FRIEND_LIKE(g_eDefaultPlayerChar, GET_CHAR_FROM_FRIEND(eFriend), -5)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE f10Connection_5_conv		//Display_Friends_Connection_convCount()
				IF GET_CONNECTION_STATE(paramFriendConID) <> FC_STATE_Invalid
					TEXT_LABEL tBlockDummy, tRootDummy
					strKeyH += "increment chats for this connection"
					strKeyK += "increment chats for multi-friend connections"
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_H)
						
						Private_GetFriendChat(	GET_CHAR_FROM_FRIEND(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].friendA),
												GET_CHAR_FROM_FRIEND(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].friendB),
												NO_CHARACTER, FCHAT_TypeFull, Private_GetFriendChatConditions(), tBlockDummy, tRootDummy, TRUE)
						Private_GetFriendChat(	GET_CHAR_FROM_FRIEND(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].friendA),
												GET_CHAR_FROM_FRIEND(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].friendB),
												NO_CHARACTER, FCHAT_TypeMini, Private_GetFriendChatConditions(), tBlockDummy, tRootDummy, TRUE)
						Private_GetFriendChat(	GET_CHAR_FROM_FRIEND(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].friendA),
												GET_CHAR_FROM_FRIEND(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].friendB),
												NO_CHARACTER, FCHAT_TypeDrunk, Private_GetFriendChatConditions(), tBlockDummy, tRootDummy, TRUE)
					
					ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
						
						Private_GetFriendChat(CHAR_MICHAEL, CHAR_FRANKLIN, CHAR_TREVOR,
												FCHAT_TypeFull, Private_GetFriendChatConditions(), tBlockDummy, tRootDummy, TRUE)
						Private_GetFriendChat(CHAR_MICHAEL, CHAR_FRANKLIN, CHAR_TREVOR,
												FCHAT_TypeMini, Private_GetFriendChatConditions(), tBlockDummy, tRootDummy, TRUE)
						Private_GetFriendChat(CHAR_MICHAEL, CHAR_FRANKLIN, CHAR_TREVOR,
												FCHAT_TypeDrunk, Private_GetFriendChatConditions(), tBlockDummy, tRootDummy, TRUE)

						Private_GetFriendChat(CHAR_FRANKLIN, CHAR_TREVOR, CHAR_LAMAR,
												FCHAT_TypeFull, Private_GetFriendChatConditions(), tBlockDummy, tRootDummy, TRUE)
						Private_GetFriendChat(CHAR_FRANKLIN, CHAR_TREVOR, CHAR_LAMAR,
												FCHAT_TypeMini, Private_GetFriendChatConditions(), tBlockDummy, tRootDummy, TRUE)
						Private_GetFriendChat(CHAR_FRANKLIN, CHAR_TREVOR, CHAR_LAMAR,
												FCHAT_TypeDrunk, Private_GetFriendChatConditions(), tBlockDummy, tRootDummy, TRUE)
					ENDIF
				ENDIF
			BREAK
			CASE f10Connection_6_contTime		//Display_Friends_Connection_LastContactTime()
				IF IS_TIMER_STARTED(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].lastContactTimer)
				AND NOT IS_TIMER_PAUSED(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].lastContactTimer)
					enumFriend eFriend
					IF GET_OTHER_FRIEND_FROM_CONNECTION(paramFriendConID, eFriend)
						strKeyK += "last contact +30s"
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
							Private_ALTER_FRIEND_LAST_CONTACT_TIME(g_eDefaultPlayerChar, GET_CHAR_FROM_FRIEND(eFriend), 30.0)
						ENDIF
						strKeyH += "last contact -30s"
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_H)
							Private_ALTER_FRIEND_LAST_CONTACT_TIME(g_eDefaultPlayerChar, GET_CHAR_FROM_FRIEND(eFriend), -30.0)
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			CASE f10Connection_7_contType		//Display_Friends_Connection_LastContactType()
				IF IS_TIMER_STARTED(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].lastContactTimer)
				AND NOT IS_TIMER_PAUSED(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].lastContactTimer)
					enumFriend eFriend
					IF GET_OTHER_FRIEND_FROM_CONNECTION(paramFriendConID, eFriend)
						enumFriendContactType eContact
						eContact = Private_GET_FRIEND_LAST_CONTACT_TYPE(g_eDefaultPlayerChar, GET_CHAR_FROM_FRIEND(eFriend))
						
						IF eContact = FRIEND_CONTACT_PHONE
							strKeyK += "set last contact type to face"
							IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
								Private_ALTER_FRIEND_LAST_CONTACT_TYPE(g_eDefaultPlayerChar, GET_CHAR_FROM_FRIEND(eFriend), FRIEND_CONTACT_FACE)
							ENDIF

						ELIF eContact = FRIEND_CONTACT_FACE
							strKeyK += "set last contact type to dismissed"
							IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
								Private_ALTER_FRIEND_LAST_CONTACT_TYPE(g_eDefaultPlayerChar, GET_CHAR_FROM_FRIEND(eFriend), FRIEND_CONTACT_DISMISSED)
							ENDIF

						ELIF eContact = FRIEND_CONTACT_DISMISSED
							strKeyK += "set last contact type to phone"
							IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
								Private_ALTER_FRIEND_LAST_CONTACT_TYPE(g_eDefaultPlayerChar, GET_CHAR_FROM_FRIEND(eFriend), FRIEND_CONTACT_PHONE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			CASE f10Connection_8_wanted			//Display_Friends_Connection_Wanted()
				//
				
			BREAK
			CASE f10Connection_9_complete		//Display_Friends_Connection_Complete()
				IF GET_CONNECTION_STATE(paramFriendConID) <> FC_STATE_Invalid
					IF NOT HAS_CONNECTION_BEEN_PLAYED(paramFriendConID)
						strKeyK += "mark as complete"
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
							SET_CONNECTION_PLAYED(paramFriendConID)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE f10Connection_9b_fail			//Display_Friends_Connection_Fail()
				//
				
			BREAK
			CASE f10Connection_10_actScript
				
				IF NOT IS_THREAD_ACTIVE(INT_TO_NATIVE(THREADID, g_SavedGlobals.sFriendsData.g_FriendScriptThread))
					IF NOT g_bAllowAmbientFriendStalking
						strKeyK += "enable ambient friend stalking"
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
							g_bAllowAmbientFriendStalking = TRUE
						ENDIF
					ELSE
						strKeyK += "disable ambient friend decline"
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
							g_bAllowAmbientFriendStalking = FALSE
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
			DEFAULT
			BREAK
		ENDSWITCH
		
		Display_F10_Literal_Text(HUD_COLOUR_PURE_WHITE, testParamXPos, testParamYPos_a + (F10_ADD_Y*1),
				strKeyH)
		Display_F10_Literal_Text(HUD_COLOUR_PURE_WHITE, testParamXPos, testParamYPos_a + (F10_ADD_Y*2),
				strKeyK)
		
		RETURN TRUE
	ENDIF
	
	
	DRAW_RECT(F10_TITLE_CONTROLLER_XPOS+totalBoxXOffset, boxYCentre_b+detailsBoxXSize,
			totalBoxXSize, totalBoxYSize_b,
			0, 0, 0, 175)
	Display_F10_Literal_Text(HUD_COLOUR_GREYLIGHT, testParamXPos, testParamYPos_b, "Press key 'J' to alter")
	
	RETURN FALSE
ENDFUNC


// PURPOSE: Display the details for one strand
//
// INPUT PARAMS:	paramFriendConID		The Strand ID
//					paramYPos			The screen Y Pos for these details

PROC Display_Friends_Connection(enumFriendConnection paramFriendConID, FLOAT paramYPos)
	HUD_COLOURS lineColour
	
	enumFriend eFriend
	IF GET_OTHER_FRIEND_FROM_CONNECTION(paramFriendConID, eFriend)
		IF GET_CONNECTION_STATE(paramFriendConID) <> FC_STATE_Invalid
			IF paramFriendConID = g_eDebugSelectedFriendConn
				lineColour = HUD_COLOUR_BLUELIGHT
			ELSE
				lineColour = HUD_COLOUR_BLUEDARK
			ENDIF
		ELSE
			IF paramFriendConID = g_eDebugSelectedFriendConn
				lineColour = HUD_COLOUR_REDLIGHT
			ELSE
				lineColour = HUD_COLOUR_REDDARK
			ENDIF
		ENDIF
		
		IF paramFriendConID = g_eDebugSelectedFriendConn
			Control_Update_Friends_Member_Connection(paramFriendConID)
		ENDIF
	ELSE
		IF paramFriendConID = g_eDebugSelectedFriendConn
			lineColour = HUD_COLOUR_GREYLIGHT
			
			IF eChangeDebugF10SelectedConnection >= f10Connection_0_state
				eChangeDebugF10SelectedConnection = f10Connection_NULL
			ENDIF
		ELSE
			lineColour = HUD_COLOUR_GREYDARK
		ENDIF
	ENDIF
	
	// Display Strand Name in appropriate colour
	Display_Friends_Connection_Name(paramFriendConID, paramYPos, lineColour)
	
	Display_Friends_Connection_State(paramFriendConID, paramYPos, lineColour,			(paramFriendConID = g_eDebugSelectedFriendConn) AND (eChangeDebugF10SelectedConnection = f10Connection_0_state))
	Display_Friends_Connection_Mode(paramFriendConID, paramYPos, lineColour,			(paramFriendConID = g_eDebugSelectedFriendConn) AND (eChangeDebugF10SelectedConnection = f10Connection_mode))
	Display_Friends_Connection_MissID(paramFriendConID, paramYPos, lineColour,			(paramFriendConID = g_eDebugSelectedFriendConn) AND (eChangeDebugF10SelectedConnection = f10Connection_2_missID))
	Display_Friends_Connection_Block(paramFriendConID, paramYPos, lineColour,			(paramFriendConID = g_eDebugSelectedFriendConn) AND (eChangeDebugF10SelectedConnection = f10Connection_1_block))
	Display_Friends_Connection_CommID(paramFriendConID, paramYPos, lineColour,			(paramFriendConID = g_eDebugSelectedFriendConn) AND (eChangeDebugF10SelectedConnection = f10Connection_3_commID))
	Display_Friends_Connection_Likes(paramFriendConID, paramYPos, lineColour,			(paramFriendConID = g_eDebugSelectedFriendConn) AND (eChangeDebugF10SelectedConnection = f10Connection_4_likes))
	Display_Friends_Connection_convCount(paramFriendConID, paramYPos, lineColour,		(paramFriendConID = g_eDebugSelectedFriendConn) AND (eChangeDebugF10SelectedConnection = f10Connection_5_conv))
	Display_Friends_Connection_LastContactTime(paramFriendConID, paramYPos, lineColour,	(paramFriendConID = g_eDebugSelectedFriendConn) AND (eChangeDebugF10SelectedConnection = f10Connection_6_contTime))
	Display_Friends_Connection_LastContactType(paramFriendConID, paramYPos, lineColour,	(paramFriendConID = g_eDebugSelectedFriendConn) AND (eChangeDebugF10SelectedConnection = f10Connection_7_contType))
	Display_Friends_Connection_Wanted(paramFriendConID, paramYPos, lineColour,			(paramFriendConID = g_eDebugSelectedFriendConn) AND (eChangeDebugF10SelectedConnection = f10Connection_8_wanted))
	Display_Friends_Connection_Complete(paramFriendConID, paramYPos, lineColour,		(paramFriendConID = g_eDebugSelectedFriendConn) AND (eChangeDebugF10SelectedConnection = f10Connection_9_complete))
	Display_Friends_Connection_Fail(paramFriendConID, paramYPos, lineColour,			(paramFriendConID = g_eDebugSelectedFriendConn) AND (eChangeDebugF10SelectedConnection = f10Connection_9b_fail))
	
ENDPROC

PROC Display_Friends_Group(enumFriendGroup paramFriendGroupID, FLOAT paramYPos)
	HUD_COLOURS lineColour
	
//	IF paramFriendConID = g_eDebugSelectedFriendConn
//		lineColour = HUD_COLOUR_GREYLIGHT
//		
//		IF eChangeDebugF10SelectedConnection >= f10Connection_0_state
//			eChangeDebugF10SelectedConnection = f10Connection_NULL
//		ENDIF
//	ELSE
		lineColour = HUD_COLOUR_GREY//DARK
//	ENDIF
	
	// Display Strand Name in appropriate colour
	Display_Friends_Group_Name(paramFriendGroupID, paramYPos, lineColour)
	Display_Friends_Group_convCount(paramFriendGroupID, paramYPos, lineColour, FALSE)
	
ENDPROC


// *******************************************************************************************
//	F10 Screen Display
// *******************************************************************************************

// PURPOSE:	Display the titles for the different sections of the F10 screen
// 
PROC Display_F10_Titles()
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("friends_controller")) > 0
		Display_F10_Literal_Text(HUD_COLOUR_GREEN, F10_TITLE_CONTROLLER_XPOS, 0.04, "FRIEND_CONTROLLER STATE: Running")
	ELSE
		Display_F10_Literal_Text(HUD_COLOUR_GREEN, F10_TITLE_CONTROLLER_XPOS, 0.04, "FRIEND_CONTROLLER STATE: NOT Running")
	ENDIF
ENDPROC


// PURPOSE:	Display the title for all friends members
// 
PROC Display_F10_Friends_Titles()
		
	FLOAT theY = F10_START_YPOS
	
	Display_F10_Literal_Text(HUD_COLOUR_GREEN, F10_FRI_STATE_XPOS,		theY, "State")
	Display_F10_Literal_Text(HUD_COLOUR_GREEN, F10_FRI_MODE_XPOS,		theY, "Mode")
	Display_F10_Literal_Text(HUD_COLOUR_GREEN, F10_FRI_BLOCK_XPOS,		theY, "Blocks")
	Display_F10_Literal_Text(HUD_COLOUR_GREEN, F10_FRI_MISSID_XPOS,		theY, "BlockMissID")//"missID")
	Display_F10_Literal_Text(HUD_COLOUR_GREEN, F10_FRI_COMMID_XPOS,		theY, "CommID")
	Display_F10_Literal_Text(HUD_COLOUR_GREEN, F10_FRI_LIKES_XPOS,		theY, "Likes")
	Display_F10_Literal_Text(HUD_COLOUR_GREEN, F10_FRI_CONVCOUNT_XPOS,	theY, "ConvBanks")
	Display_F10_Literal_Text(HUD_COLOUR_GREEN, F10_FRI_CONTTIME_XPOS,	theY, "LastContact")
	Display_F10_Literal_Text(HUD_COLOUR_GREEN, F10_FRI_CONTTYPE_XPOS,	theY, "ContactType")
	Display_F10_Literal_Text(HUD_COLOUR_GREEN, F10_FRI_WANTED_XPOS,		theY, "Wanted")
	Display_F10_Literal_Text(HUD_COLOUR_GREEN, F10_FRI_COMPLETE_XPOS,	theY, "Done")
	Display_F10_Literal_Text(HUD_COLOUR_GREEN, F10_FRI_FAIL_XPOS,		theY, "FailTimer")
	
ENDPROC


// PURPOSE:	Display the details for all friends members
// 
PROC Display_F10_Friends_Details()
		
	FLOAT theY = F10_START_YPOS + F10_ADD_Y
	
	enumFriendConnection eConnection
	REPEAT MAX_FRIEND_CONNECTIONS eConnection
		Display_Friends_Connection(eConnection, theY)
		theY += F10_ADD_Y
	ENDREPEAT
	
	enumFriendGroup eGroup
	REPEAT MAX_FRIEND_GROUPS eGroup
		Display_Friends_Group(eGroup, theY)
		theY += F10_ADD_Y
	ENDREPEAT
	
ENDPROC

PROC Display_F10_Literal_Text_test(STRING sLiteral, INT &iRow, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE)
	CONST_FLOAT fStringRow	5.0//3.5
	
	FLOAT paramXPos = F10_FRI_NAME_XPOS
	FLOAT paramYPos = (F10_START_YPOS*fStringRow)
	paramYPos += (F10_ADD_Y*TO_FLOAT(iRow))
	
	Display_F10_Literal_Text(eColour, paramXPos, paramYPos, sLiteral)
	iRow++
ENDPROC

PROC Display_F10_Literal_Text_testFloat(STRING sLiteral, FLOAT sFloat, INT &iRow, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE)
	TEXT_LABEL_63 sNewLiteral = sLiteral
	sNewLiteral += GET_STRING_FROM_FLOAT(sFloat)
	
	Display_F10_Literal_Text_test(sNewLiteral, iRow, eColour)
ENDPROC

PROC Display_F10_Friend_Call_Status_0_contactWait(enumFriendConnection paramFriendConID, enumFriend eFriend)
	INT iRow = 0
	enumFriend ePlayer = GET_FRIEND_FROM_CHAR(g_eDefaultPlayerChar)
	
	TEXT_LABEL_63 theName = GetLabel_enumFriend(eFriend)
	theName += " ("
	theName += GetLabel_enumFriend(ePlayer)
	theName += ")"
	Display_F10_Literal_Text_test(theName,								iRow, HUD_COLOUR_PURE_WHITE)
	
	enumFriendContactType eLastContactType
	FLOAT fLastContactTime
	INT iAwakeHour, iSleepHour, iCurrentHour
	
	IF PRIVATE_FriendAcceptsPlayer_exists(eFriend)
		Display_F10_Literal_Text_test("  exists: NONE",					iRow, HUD_COLOUR_GREEN)
	ELSE
		TEXT_LABEL_63 theExists = "  exists: "
		theExists += GetLabel_enumFriend(eFriend)
		Display_F10_Literal_Text_test(theExists,						iRow, HUD_COLOUR_RED)
	ENDIF
	
	IF PRIVATE_FriendAcceptsPlayer_fail(eFriend)
		Display_F10_Literal_Text_test("  failTimer: OK",				iRow, HUD_COLOUR_GREENLIGHT)
	ELSE
		Display_F10_Literal_Text_test("  failTimer: BLOCKED",			iRow, HUD_COLOUR_REDLIGHT)
	ENDIF
	
	IF PRIVATE_FriendAcceptsPlayer_hiatusBlock(ePlayer, eFriend)
		Display_F10_Literal_Text_test("  hiatusBlock: OFF",				iRow, HUD_COLOUR_GREENLIGHT)
	ELSE
		Display_F10_Literal_Text_test("  hiatusBlock: ON",				iRow, HUD_COLOUR_REDLIGHT)
	ENDIF
	
	IF PRIVATE_FriendAcceptsPlayer_clashBlock(ePlayer, eFriend)
		Display_F10_Literal_Text_test("  clashBlock: OFF",				iRow, HUD_COLOUR_GREENLIGHT)
	ELSE
		Display_F10_Literal_Text_test("  clashBlock: ON",				iRow, HUD_COLOUR_REDLIGHT)
	ENDIF
	
	IF PRIVATE_FriendAcceptsPlayer_missionBlock(ePlayer, eFriend)
		Display_F10_Literal_Text_test("  missionBlock: OFF",			iRow, HUD_COLOUR_GREENLIGHT)
	ELSE
		TEXT_LABEL_63 tMissionBlock = "  missionBlock: "
		tMissionBlock += GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].blockMissionID)
		Display_F10_Literal_Text_test(tMissionBlock,					iRow, HUD_COLOUR_REDLIGHT)
	ENDIF
	
	IF PRIVATE_FriendAcceptsPlayer_wanted(ePlayer, eFriend)
		Display_F10_Literal_Text_test("  wanted: SOMEONE",				iRow, HUD_COLOUR_GREENLIGHT)
	ELSE
		Display_F10_Literal_Text_test("  wanted: NONE",					iRow, HUD_COLOUR_REDLIGHT)
	ENDIF
	
	IF PRIVATE_FriendAcceptsPlayer_group(ePlayer, eFriend)
		Display_F10_Literal_Text_test("  group: ALLOWED",				iRow, HUD_COLOUR_GREEN)
	ELSE
		Display_F10_Literal_Text_test("  group: NOT ALLOWED", 			iRow, HUD_COLOUR_RED)
	ENDIF
	
	IF PRIVATE_FriendAcceptsPlayer_common(ePlayer, eFriend)
		Display_F10_Literal_Text_test("  common: CLEAR/NONE",			iRow, HUD_COLOUR_GREEN)
	ELSE
		Display_F10_Literal_Text_test("  common: BLOCKED", 				iRow, HUD_COLOUR_RED)
	ENDIF
	
	TEXT_LABEL_63 theTime
	IF NOT PRIVATE_FriendAcceptsPlayer_time(ePlayer, eFriend, fLastContactTime, eLastContactType)
		IF eLastContactType = FRIEND_CONTACT_PHONE
			theTime = "  phone: "
		ELIF eLastContactType = FRIEND_CONTACT_FACE
			theTime = "  face: "
		ELIF eLastContactType = FRIEND_CONTACT_DISMISSED
			theTime = "  dismissed: "
		ELSE
			theTime = "  contact: "
		ENDIF
		
		theTime += GET_STRING_FROM_MILISECONDS(ROUND((fLastContactTime) * 1000.0))
		Display_F10_Literal_Text_test(theTime,							iRow, HUD_COLOUR_RED)
	ELSE
		theTime = "  time: "
		theTime += GET_STRING_FROM_MILISECONDS(ROUND((fLastContactTime) * 1000.0))
		
		HUD_COLOURS eColour = HUD_COLOUR_GREEN
		IF TIMER_DO_WHEN_VERYIRATE(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].lastContactTimer)
			eColour = HUD_COLOUR_GREENDARK
		ELIF TIMER_DO_WHEN_IRATE(g_SavedGlobals.sFriendsData.g_FriendConnectData[paramFriendConID].lastContactTimer)
			eColour = HUD_COLOUR_BLACK
		ENDIF
		
		Display_F10_Literal_Text_test(theTime,							iRow, eColour)
	ENDIF
	
	IF PRIVATE_FriendAcceptsPlayer_awake(eFriend, iAwakeHour, iSleepHour, iCurrentHour)
		TEXT_LABEL_63 tAwake = "  awake: "
		tAwake += iAwakeHour
		tAwake += " > "
		tAwake += iCurrentHour
		tAwake += " > "
		tAwake += iSleepHour
		Display_F10_Literal_Text_test(tAwake,							iRow, HUD_COLOUR_GREEN)
	ELSE
		TEXT_LABEL_63 tAsleep = "  asleep: "
		tAsleep += iAwakeHour
		tAsleep += " > "
		tAsleep += iCurrentHour
		tAsleep += " > "
		tAsleep += iSleepHour
		Display_F10_Literal_Text_test(tAsleep,							iRow, HUD_COLOUR_RED)
	ENDIF
	
	IF PRIVATE_FriendAcceptsPlayer_laylow(eFriend)
		Display_F10_Literal_Text_test("  laylow: NONE",					iRow, HUD_COLOUR_GREEN)
	ELSE
		Display_F10_Literal_Text_test("  laylow: BLOCKED",				iRow, HUD_COLOUR_RED)
	ENDIF

	//
	enumFriendPhonePhrase greetConv_a, greetConv_b
	enumFriendPhonePhrase yesConv_a, yesConv_b
	enumFriendPhonePhrase noConv_A, noConv_b
	PRIVATE_Friend_GetPhoneConv(ePlayer, eFriend,
								greetConv_a, greetConv_b,
								yesConv_a, yesConv_b,
								noConv_A, noConv_b)
	
	Display_F10_Literal_Text_test(" - - - - - - -",								iRow, HUD_COLOUR_GREY)
	
	Display_F10_Literal_Text_test(GetLabel_enumFriendPhonePhrase(greetConv_a),	iRow, HUD_COLOUR_GREY)
	Display_F10_Literal_Text_test(GetLabel_enumFriendPhonePhrase(greetConv_b),	iRow, HUD_COLOUR_GREY)
	Display_F10_Literal_Text_test(GetLabel_enumFriendPhonePhrase(yesConv_a),	iRow, HUD_COLOUR_GREY)
	Display_F10_Literal_Text_test(GetLabel_enumFriendPhonePhrase(yesConv_b),	iRow, HUD_COLOUR_GREY)
	Display_F10_Literal_Text_test(GetLabel_enumFriendPhonePhrase(noConv_a),		iRow, HUD_COLOUR_GREY)
	Display_F10_Literal_Text_test(GetLabel_enumFriendPhonePhrase(noConv_b),		iRow, HUD_COLOUR_GREY)

ENDPROC
PROC Display_F10_Friend_Call_Status_1_activityActive(enumFriend eFriend)
	INT iRow = 0
	
	enumFriend			ePlayer			= GET_FRIEND_FROM_CHAR(g_eDefaultPlayerChar)
	enumCharacterList	friendCharID	= GET_CHAR_FROM_FRIEND(eFriend)
	TEXT_LABEL_63 theName = GetLabel_enumFriend(eFriend)
	theName += " ("
	theName += GetLabel_enumFriend(ePlayer)
	theName += ")"
	Display_F10_Literal_Text_test(theName,											iRow, HUD_COLOUR_PURE_WHITE)
	
	//
	enumFriendPhonePhrase cancelConv_a, cancelConv_b
	PRIVATE_Friend_GetCancelConvReply(GET_FRIEND_LIKE(g_eDefaultPlayerChar, friendCharID), cancelConv_a, cancelConv_b)
	
	Display_F10_Literal_Text_test(" - - - - - - -",									iRow, HUD_COLOUR_GREY)
	
	Display_F10_Literal_Text_test(GetLabel_enumFriendPhonePhrase(cancelConv_b),	iRow, HUD_COLOUR_GREY)

ENDPROC
PROC Display_F10_Friend_Call_Status(enumFriendConnection paramFriendConID)
	
	IF paramFriendConID >= MAX_FRIEND_CONNECTIONS
		EXIT
	ENDIF
	
	enumFriend eFriend
	IF GET_OTHER_FRIEND_FROM_CONNECTION(paramFriendConID, eFriend)
		IF GET_CONNECTION_STATE(paramFriendConID) = FC_STATE_ContactWait
			Display_F10_Friend_Call_Status_0_contactWait(paramFriendConID, eFriend)
		ELIF GET_CONNECTION_STATE(paramFriendConID) = FC_STATE_Active
			Display_F10_Friend_Call_Status_1_activityActive(eFriend)
		ELSE
			//
		ENDIF
	ENDIF
	
ENDPROC

PROC Display_F10_Friend_ActivityScriptStatus()
	
	FLOAT paramYPos = F10_START_YPOS
	
	HUD_COLOURS paramColour = HUD_COLOUR_GREENLIGHT
	IF eChangeDebugF10SelectedConnection = f10Connection_10_actScript
		paramColour = HUD_COLOUR_YELLOW
	ENDIF

	//draw next contact timer
	TEXT_LABEL_63 tActivityScriptStatus
	
	IF IS_THREAD_ACTIVE(INT_TO_NATIVE(THREADID, g_SavedGlobals.sFriendsData.g_FriendScriptThread))
		tActivityScriptStatus = "FriendScript active"
	ELSE
		IF g_bAllowAmbientFriendStalking
			tActivityScriptStatus = "AmbientFriendStalking: <enabled>"
		ELSE
			tActivityScriptStatus = "AmbientFriendStalking: <disabled>"
		ENDIF
	ENDIF
	
	Display_F10_Literal_Text(paramColour, F10_FRI_ACTSCRIPT_XPOS, paramYPos, tActivityScriptStatus)
	
ENDPROC

PROC Cleanup_F10_Switch_Details()
	g_iDebugSelectedFriendConnDisplay = 0
	
ENDPROC

// *******************************************************************************************
//	F10 Select Debug Display
// *******************************************************************************************

// PURPOSE:	
// 
PROC Control_F10_Debug_Selection()
	
	IF g_eDebugSelectedFriendConn	>= MAX_FRIEND_CONNECTIONS
		g_eDebugSelectedFriendConn	= FC_MICHAEL_FRANKLIN
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
		INT iDebugSelectedMember = ENUM_TO_INT(g_eDebugSelectedFriendConn)
		iDebugSelectedMember--
		IF iDebugSelectedMember < 0
			iDebugSelectedMember = ENUM_TO_INT(MAX_FRIEND_CONNECTIONS)-1
		ENDIF
		g_eDebugSelectedFriendConn = INT_TO_ENUM(enumFriendConnection, iDebugSelectedMember)
	ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
		INT iDebugSelectedMember = ENUM_TO_INT(g_eDebugSelectedFriendConn)
		iDebugSelectedMember++
		IF iDebugSelectedMember >= ENUM_TO_INT(MAX_FRIEND_CONNECTIONS)
			iDebugSelectedMember = 0
		ENDIF
		g_eDebugSelectedFriendConn = INT_TO_ENUM(enumFriendConnection, iDebugSelectedMember)
	ENDIF
	
ENDPROC

// *******************************************************************************************
//	F10 Select Debug Display
// *******************************************************************************************

// PURPOSE:	Displays the Friends Controller F10 screen when allowed by other systems
// 
PROC Display_Friends_Controller_F10_Screen()
	
	// Check if some other system has hidden this screen
	IF NOT g_flowUnsaved.bShowMissionFlowDebugScreen
		g_iDebugSelectedFriendConnDisplay = 0
		EXIT
	ENDIF
	
	Display_F10_Background()
	Display_F10_Titles()
	
	Display_F10_Friends_Titles()
	Display_F10_Friends_Details()
	Display_F10_Friend_Call_Status(g_eDebugSelectedFriendConn)
	Display_F10_Friend_ActivityScriptStatus()
	
	Control_F10_Debug_Selection()
	
	g_iDebugSelectedFriendConnDisplay = 2
	
ENDPROC
