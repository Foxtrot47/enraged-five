USING "rage_builtins.sch"
USING "globals.sch"
USING "Replay_Public.sch"
#IF IS_DEBUG_BUILD
	USING "debug_channels_structs.sch"
#ENDIF

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	replay_debug.sch
//		AUTHOR			:	Keith McLeman
//		MAINTAINED		:	Ben Rollinson
//		DESCRIPTION		:	Widgets and debug routines to aid 'replay' testing.
//		NOTES			:	Included within replay_controller.sc only.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


// These variables will be included only within replay_controller.sc
WIDGET_GROUP_ID		replayWidgetGroupID

// INTs for read-only ENUM to INT and TIMEOFDAY display
INT		m_iReplayStage						= ENUM_TO_INT(g_replay.replayStageID)

INT		m_iReplayHoldingStartChar			= ENUM_TO_INT(g_startSnapshot.eCharacter)
INT		m_iReplayHoldingStageChar			= ENUM_TO_INT(g_startSnapshot.eCharacter)
//INT		m_iReplayHoldingStartWeapons		[NUM_PLAYER_PED_WEAPON_SLOTS]
//INT		m_iReplayHoldingStageWeapons		[NUM_PLAYER_PED_WEAPON_SLOTS]

INT		m_iReplayHoldingStartTimeHour		= GET_TIMEOFDAY_HOUR(g_startSnapshot.tTime)
INT		m_iReplayHoldingStartTimeMinute		= GET_TIMEOFDAY_MINUTE(g_startSnapshot.tTime)
INT		m_iReplayHoldingStartTimeSecond		= GET_TIMEOFDAY_SECOND(g_startSnapshot.tTime)
INT		m_iReplayHoldingStartTimeDay		= GET_TIMEOFDAY_DAY(g_startSnapshot.tTime)
INT		m_iReplayHoldingStartTimeMonth		= ENUM_TO_INT(GET_TIMEOFDAY_MONTH(g_startSnapshot.tTime))
INT		m_iReplayHoldingStartTimeYear		= GET_TIMEOFDAY_YEAR(g_startSnapshot.tTime)

INT		m_iReplayHoldingStageTimeHour		= GET_TIMEOFDAY_HOUR(g_stageSnapshot.tTime)
INT		m_iReplayHoldingStageTimeMinute		= GET_TIMEOFDAY_MINUTE(g_stageSnapshot.tTime)
INT		m_iReplayHoldingStageTimeSecond		= GET_TIMEOFDAY_SECOND(g_stageSnapshot.tTime)
INT		m_iReplayHoldingStageTimeDay		= GET_TIMEOFDAY_DAY(g_stageSnapshot.tTime)
INT		m_iReplayHoldingStageTimeMonth		= ENUM_TO_INT(GET_TIMEOFDAY_MONTH(g_stageSnapshot.tTime))
INT		m_iReplayHoldingStageTimeYear		= GET_TIMEOFDAY_YEAR(g_stageSnapshot.tTime)

// BOOLs for external control tickboxes
BOOL	m_bForceCleanupReplay				= FALSE


// PURPOSE: Set up the debug widgets for Replay testing
PROC Initialise_Replay_Debug_Widgets()
//	INT index

	replayWidgetGroupID = START_WIDGET_GROUP("Replays")
		ADD_WIDGET_BOOL("Disable Fullscreen Replay Screen",				g_bDebugDisableFailScreen)	
		ADD_WIDGET_BOOL("Force Cleanup Replay",							m_bForceCleanupReplay)
	
		START_WIDGET_GROUP("Core Variables")
			ADD_WIDGET_INT_READ_ONLY("Replay State",					m_iReplayStage)
			ADD_WIDGET_INT_READ_ONLY("Script Variables Index",			g_replay.replayCoreVarsIndex)
			ADD_WIDGET_INT_READ_ONLY("Copy of Mid-Mission Stage",		g_replay.replayStageReadOnly)
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP("Mission Start Holding Variables")
			START_WIDGET_GROUP("Restart Time")
				ADD_WIDGET_INT_READ_ONLY("Hour",						m_iReplayHoldingStartTimeHour)	
				ADD_WIDGET_INT_READ_ONLY("Min",							m_iReplayHoldingStartTimeMinute)	
				ADD_WIDGET_INT_READ_ONLY("Sec",							m_iReplayHoldingStartTimeSecond)	
				ADD_WIDGET_INT_READ_ONLY("Day",							m_iReplayHoldingStartTimeDay)	
				ADD_WIDGET_INT_READ_ONLY("Month",						m_iReplayHoldingStartTimeMonth)	
				ADD_WIDGET_INT_READ_ONLY("Year",						m_iReplayHoldingStartTimeYear)	
			STOP_WIDGET_GROUP()
			ADD_WIDGET_INT_READ_ONLY("Character", 						m_iReplayHoldingStartChar)
//			START_WIDGET_GROUP("Weapons State")
//				REPEAT NUM_PLAYER_PED_WEAPON_SLOTS index
//					ADD_WIDGET_INT_READ_ONLY("Weapon Type",				m_iReplayHoldingStartWeapons[index])
//					ADD_WIDGET_INT_READ_ONLY("Weapon Ammo",				g_startSnapshot.ammo[index])
//					ADD_WIDGET_INT_READ_ONLY("Weapon Mods",				g_startSnapshot.mods[index])
//				ENDREPEAT
//			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
			
		START_WIDGET_GROUP("Last Stage Holding Variables")
			START_WIDGET_GROUP("Restart Time")
				ADD_WIDGET_INT_READ_ONLY("Hour",						m_iReplayHoldingStageTimeHour)	
				ADD_WIDGET_INT_READ_ONLY("Min",							m_iReplayHoldingStageTimeMinute)	
				ADD_WIDGET_INT_READ_ONLY("Sec",							m_iReplayHoldingStageTimeSecond)	
				ADD_WIDGET_INT_READ_ONLY("Day",							m_iReplayHoldingStageTimeDay)	
				ADD_WIDGET_INT_READ_ONLY("Month",						m_iReplayHoldingStageTimeMonth)	
				ADD_WIDGET_INT_READ_ONLY("Year",						m_iReplayHoldingStageTimeYear)	
			STOP_WIDGET_GROUP()
			ADD_WIDGET_INT_READ_ONLY("Character",						m_iReplayHoldingStageChar)
//			START_WIDGET_GROUP("Weapons State")
//				REPEAT NUM_PLAYER_PED_WEAPON_SLOTS index
//					ADD_WIDGET_INT_READ_ONLY("Weapon Type",				m_iReplayHoldingStageWeapons[index])
//					ADD_WIDGET_INT_READ_ONLY("Weapon Ammo",				g_stageSnapshot.ammo[index])
//				ENDREPEAT
//			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Other Replay Variables")
			ADD_WIDGET_INT_SLIDER("Current Mid-Mission Stage",								g_replayMissionStage, -1, 20, 1)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()

ENDPROC


// PURPOSE: Delete the widgets
PROC Delete_Replay_Debug_Widgets()
	DELETE_WIDGET_GROUP(replayWidgetGroupID)
ENDPROC


// PURPOSE: Maintain the Replay debug widgets.
PROC Maintain_Replay_Debug_Widgets()
	//Check if the replay is being cleaned up
	IF (m_bForceCleanupReplay)
		CPRINTLN(DEBUG_REPLAY, "Replay cleanup triggered by debug widget.")
		m_bForceCleanupReplay = FALSE
		Reset_All_Replay_Variables()
	ENDIF
	
	//Update debug ENUM to INT values.
	m_iReplayStage					= ENUM_TO_INT(g_replay.replayStageID)
	m_iReplayHoldingStartChar		= ENUM_TO_INT(g_startSnapshot.eCharacter)
	m_iReplayHoldingStageChar		= ENUM_TO_INT(g_startSnapshot.eCharacter)

	//Update debug TIMEOFDAY values.
	m_iReplayHoldingStartTimeHour		= GET_TIMEOFDAY_HOUR(g_startSnapshot.tTime)
	m_iReplayHoldingStartTimeMinute		= GET_TIMEOFDAY_MINUTE(g_startSnapshot.tTime)
	m_iReplayHoldingStartTimeSecond		= GET_TIMEOFDAY_SECOND(g_startSnapshot.tTime)
	m_iReplayHoldingStartTimeDay		= GET_TIMEOFDAY_DAY(g_startSnapshot.tTime)
	m_iReplayHoldingStartTimeMonth		= ENUM_TO_INT(GET_TIMEOFDAY_MONTH(g_startSnapshot.tTime))
	m_iReplayHoldingStartTimeYear		= GET_TIMEOFDAY_YEAR(g_startSnapshot.tTime)

	m_iReplayHoldingStageTimeHour		= GET_TIMEOFDAY_HOUR(g_stageSnapshot.tTime)
	m_iReplayHoldingStageTimeMinute		= GET_TIMEOFDAY_MINUTE(g_stageSnapshot.tTime)
	m_iReplayHoldingStageTimeSecond		= GET_TIMEOFDAY_SECOND(g_stageSnapshot.tTime)
	m_iReplayHoldingStageTimeDay		= GET_TIMEOFDAY_DAY(g_stageSnapshot.tTime)
	m_iReplayHoldingStageTimeMonth		= ENUM_TO_INT(GET_TIMEOFDAY_MONTH(g_stageSnapshot.tTime))
	m_iReplayHoldingStageTimeYear		= GET_TIMEOFDAY_YEAR(g_stageSnapshot.tTime)
ENDPROC

