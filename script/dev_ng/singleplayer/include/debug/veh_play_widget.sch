USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_debug.sch"
USING "commands_vehicle.sch"
USING "commands_streaming.sch"
USING "commands_script.sch"
USING "script_player.sch"




INT iWdCarRecPlayProg 	= 	0
INT iWdCarRecId			=	1

FLOAT fWdCarRecTime	=	0.0

BOOL bWdCarRecPutPlayerInCar	=	TRUE
BOOL bWdCarRecStartPlayback		=	FALSE
BOOL bWdCarRecBlipCar			=	FALSE
BOOL bWdCarRecRemoveWidget		=	FALSE
BOOL bWdCarRecSHowPlayRoute		=	FALSE

TEXT_WIDGET_ID wdCarModel, wdCarRecName
WIDGET_GROUP_ID wdVehicleRecordingPlayback

VEHICLE_INDEX vehWdCarRecPlayback

BLIP_INDEX blipWdCarRecPlayback

//PARAM NOTES: Specifiy whether or not the script should get cleaned up when the widget gets deleted. Needs to be FALSE if called from a mission script.
//PURPOSE: Wdiget for playing back vehicle recordings
PROC DO_VEHICLE_REC_PLAYBACK_WIDGET(BOOL bKillThreadOnCleanup = FALSE)
	MODEL_NAMES mVehicle
	VECTOR vTemp
	STRING sRecName
	SWITCH iWdCarRecPlayProg
		CASE 0
			wdVehicleRecordingPlayback = START_WIDGET_GROUP("Vehicle Recording Playback")
				wdCarModel = ADD_TEXT_WIDGET("Vehicle Model")
				wdCarRecName = ADD_TEXT_WIDGET("Recording Filename")
				ADD_WIDGET_INT_SLIDER("Recording number", iWdCarRecId, 1, 2000, 1)
				ADD_WIDGET_BOOL("Start playback", bWdCarRecStartPlayback)
				ADD_WIDGET_FLOAT_SLIDER("Playback time", fWdCarRecTime, 0.0, 2000000.0, 0.1)
				ADD_WIDGET_BOOL("Put player in car?", bWdCarRecPutPlayerInCar)
				ADD_WIDGET_BOOL("Blip playback car?", bWdCarRecBlipCar)
				ADD_WIDGET_BOOL("Show playback route?", bWdCarRecSHowPlayRoute)
				ADD_WIDGET_BOOL("Delete widget", bWdCarRecRemoveWidget)
			STOP_WIDGET_GROUP()
			
			SET_CONTENTS_OF_TEXT_WIDGET(wdCarModel, "POLICE")
			SET_CONTENTS_OF_TEXT_WIDGET(wdCarRecName, "jetBlast")
			iWdCarRecPlayProg++
		BREAK
		
		CASE 1
			IF bWdCarRecStartPlayback
				bWdCarRecStartPlayback = FALSE
				
				sRecName = GET_CONTENTS_OF_TEXT_WIDGET(wdCarRecName)
				
				
				IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iWdCarRecId, sRecName)
					REQUEST_VEHICLE_RECORDING(iWdCarRecId, sRecName)
					WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iWdCarRecId, sRecName)
						WAIT(0)
					ENDWHILE
				ENDIF
				
				//-- Place / Create the car at the beginning the of the recording
				vTemp =  GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iWdCarRecId, 0, sRecName)
				CLEAR_AREA(vTemp, 15.0, TRUE)
				
				IF NOT DOES_ENTITY_EXIST(vehWdCarRecPlayback)
					mVehicle = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(wdCarModel)))
					IF IS_MODEL_IN_CDIMAGE(mVehicle)
						IF NOT HAS_MODEL_LOADED(mVehicle)
							REQUEST_MODEL(mVehicle)
							WHILE NOT HAS_MODEL_LOADED(mVehicle)
								WAIT(0)
							ENDWHILE
						ENDIF
						 
						LOAD_SCENE(vTemp)
						vehWdCarRecPlayback = CREATE_VEHICLE(mVehicle, vTemp, 0.0)
						START_PLAYBACK_RECORDED_VEHICLE(vehWdCarRecPlayback, iWdCarRecId,sRecName )

					ELSE
						SCRIPT_ASSERT("Vehicle recording playback: Not a valid vehicle model!")
						EXIT
					ENDIF
				ELSE
					IF NOT IS_ENTITY_DEAD(vehWdCarRecPlayback)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehWdCarRecPlayback)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehWdCarRecPlayback)
						ENDIF
						START_PLAYBACK_RECORDED_VEHICLE(vehWdCarRecPlayback, iWdCarRecId,sRecName)

					ENDIF
				ENDIF
				
				//-- Puts the player in the playback car.
				IF bWdCarRecPutPlayerInCar
					bWdCarRecPutPlayerInCar = FALSE
					IF NOT IS_ENTITY_DEAD(vehWdCarRecPlayback)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehWdCarRecPlayback)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehWdCarRecPlayback)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			IF NOT IS_ENTITY_DEAD(vehWdCarRecPlayback)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehWdCarRecPlayback)
					//-- Display the current playback time
					fWdCarRecTime = GET_TIME_POSITION_IN_RECORDING(vehWdCarRecPlayback)
					
					//-- Display the route the playback takes
					IF bWdCarRecSHowPlayRoute
						DISPLAY_PLAYBACK_RECORDED_VEHICLE(vehWdCarRecPlayback, RDM_WHOLELINE)	
					ELSE
						DISPLAY_PLAYBACK_RECORDED_VEHICLE(vehWdCarRecPlayback, RDM_NONE)
					ENDIF
				ENDIF
				
				//--Blip the playback car
				IF bWdCarRecBlipCar
					bWdCarRecBlipCar = FALSE
					IF NOT DOES_BLIP_EXIST(blipWdCarRecPlayback)
						blipWdCarRecPlayback = ADD_BLIP_FOR_ENTITY(vehWdCarRecPlayback)
						SET_BLIP_AS_FRIENDLY(blipWdCarRecPlayback, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF bWdCarRecRemoveWidget
				//-- Remove the widget and kill the script
				bWdCarRecRemoveWidget = FALSE
				IF NOT IS_ENTITY_DEAD(vehWdCarRecPlayback)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehWdCarRecPlayback)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehWdCarRecPlayback)
					ENDIF
				ENDIF
				IF DOES_BLIP_EXIST(blipWdCarRecPlayback)
					REMOVE_BLIP(blipWdCarRecPlayback)
				ENDIF
				DELETE_WIDGET_GROUP(wdVehicleRecordingPlayback)
				IF bKillThreadOnCleanup
					TERMINATE_THIS_THREAD()
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC	

