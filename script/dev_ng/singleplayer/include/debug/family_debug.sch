USING "rage_builtins.sch"
USING "globals.sch"

#IF IS_DEBUG_BUILD
	USING "script_debug.sch"
	USING "shared_debug.sch"
	USING "building_control_public.sch"

///debug interface for family scripts
///    alwyn.roberts@rockstarnorth.com
///    

// *******************************************************************************************
//	DRAW LITERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL DrawFamilyLiteralString(STRING sLiteral, INT iColumn, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE)
	IF g_bDrawDebugFamilyStuff
		INT red, green, blue, iAlpha
		GET_HUD_COLOUR(eColour, red, green, blue, iAlpha)
		
		CONST_FLOAT	FAMILY_TEXT_SCALE_X					0.25	//0.45
		CONST_FLOAT	FAMILY_TEXT_SCALE_Y					0.3		//0.45
		
		CONST_FLOAT	FAMILY_TEXT_COORD_X					0.7795
		CONST_FLOAT	FAMILY_TEXT_START_Y					0.0305
		CONST_FLOAT	FAMILY_TEXT_ADD_Y					0.0255	//0.0305
		
		SET_TEXT_SCALE(FAMILY_TEXT_SCALE_X, FAMILY_TEXT_SCALE_Y)
		SET_TEXT_COLOUR(red, green, blue, iAlpha)
		DISPLAY_TEXT_WITH_LITERAL_STRING(FAMILY_TEXT_COORD_X, FAMILY_TEXT_START_Y+(FAMILY_TEXT_ADD_Y*TO_FLOAT(iColumn)),
				"STRING", sLiteral)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DrawFamilyLiteralStringInt(STRING sLiteral, INT sInt, INT iColumn, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE)
	TEXT_LABEL_63 sNewLiteral = sLiteral
	sNewLiteral += sInt
	
	RETURN DrawFamilyLiteralString(sNewLiteral, iColumn, eColour)
ENDFUNC
FUNC BOOL DrawFamilyLiteralStringFloat(STRING sLiteral, FLOAT sFloat, INT iColumn, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE)
	TEXT_LABEL_63 sNewLiteral = sLiteral
	sNewLiteral += GET_STRING_FROM_FLOAT(sFloat)
	
	RETURN DrawFamilyLiteralString(sNewLiteral, iColumn, eColour)
ENDFUNC

FUNC BOOL DrawDebugFamilyLine(VECTOR VecCoorsFirst, VECTOR VecCoorsSecond, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE, FLOAT fAlphaMult = 1.0)
	IF g_bDrawDebugFamilyStuff
		
		INT red, green, blue, iAlpha
		GET_HUD_COLOUR(eColour, red, green, blue, iAlpha)
		
		DRAW_DEBUG_LINE(VecCoorsFirst, VecCoorsSecond, red, green, blue, ROUND(TO_FLOAT(iAlpha) * fAlphaMult))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL DrawDebugFamilySphere(VECTOR VecCoors, FLOAT Range, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE, FLOAT fAlphaMult = 1.0)
	IF g_bDrawDebugFamilyStuff
		
		INT red, green, blue, iAlpha
		GET_HUD_COLOUR(eColour, red, green, blue, iAlpha)
		
		DRAW_DEBUG_SPHERE(VecCoors, Range, red, green, blue, ROUND(TO_FLOAT(iAlpha) * fAlphaMult))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL DrawDebugFamilyTextWithOffset(STRING text, VECTOR VecCoors, FLOAT Offset_y, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE, FLOAT fAlphaMult = 1.0)
	IF g_bDrawDebugFamilyStuff
		
		CONST_INT iTestXY 10
		INT iOffset_y = ROUND(Offset_y * TO_FLOAT(iTestXY))
		
		INT red, green, blue, iAlpha
		GET_HUD_COLOUR(eColour, red, green, blue, iAlpha)
		
		IF ARE_VECTORS_EQUAL(VecCoors, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
		AND (GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON)
			DRAW_DEBUG_TEXT_2D(text, 
					<<0.2,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iOffset_y))+(<<0.2,0.0,0>>*TO_FLOAT(0)),
					red, green, blue, ROUND(TO_FLOAT(iAlpha) * fAlphaMult))
			RETURN TRUE
		ENDIF
		
		DRAW_DEBUG_TEXT_WITH_OFFSET(text, VecCoors, 0, iOffset_y, red, green, blue, ROUND(TO_FLOAT(iAlpha) * fAlphaMult))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	GENERAL DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC STRING Get_String_From_FamilyMember(enumFamilyMember eFamilyMember)
	SWITCH eFamilyMember
		CASE FM_MICHAEL_SON				RETURN "FM_MICHAEL_SON"			BREAK
		CASE FM_MICHAEL_DAUGHTER		RETURN "FM_MICHAEL_DAUGHTER"	BREAK
		CASE FM_MICHAEL_WIFE			RETURN "FM_MICHAEL_WIFE"		BREAK
		CASE FM_MICHAEL_MEXMAID			RETURN "FM_MICHAEL_MEXMAID"		BREAK
		CASE FM_MICHAEL_GARDENER		RETURN "FM_MICHAEL_GARDENER"	BREAK
		
		CASE FM_FRANKLIN_AUNT			RETURN "FM_FRANKLIN_AUNT"		BREAK
		CASE FM_FRANKLIN_LAMAR			RETURN "FM_FRANKLIN_LAMAR"		BREAK
		CASE FM_FRANKLIN_STRETCH		RETURN "FM_FRANKLIN_STRETCH"	BREAK
//		CASE FM_FRANKLIN_DOG			RETURN "FM_FRANKLIN_DOG"		BREAK
		
		CASE FM_TREVOR_0_RON			RETURN "FM_TREVOR_0_RON"		BREAK
		CASE FM_TREVOR_0_MICHAEL		RETURN "FM_TREVOR_0_MICHAEL"	BREAK
		CASE FM_TREVOR_0_TREVOR			RETURN "FM_TREVOR_0_TREVOR"		BREAK
		CASE FM_TREVOR_0_WIFE			RETURN "FM_TREVOR_0_WIFE"		BREAK
		CASE FM_TREVOR_0_MOTHER			RETURN "FM_TREVOR_0_MOTHER"		BREAK
		
		CASE FM_TREVOR_1_FLOYD			RETURN "FM_TREVOR_1_FLOYD"		BREAK
		CASE FM_TREVOR_1_WADE			RETURN "FM_TREVOR_1_WADE"		BREAK
		
		
		CASE MAX_FAMILY_MEMBER
			RETURN "MAX_FAMILY_MEMBER"
		BREAK
		CASE NO_FAMILY_MEMBER
			RETURN "NO_FAMILY_MEMBER"
		BREAK
	ENDSWITCH
	
	TEXT_LABEL_63 str = "unknown eFamilyMember "
	str += ENUM_TO_INT(eFamilyMember)
	
	RETURN GET_STRING_FROM_STRING(str, 0, GET_LENGTH_OF_LITERAL_STRING(str))
ENDFUNC

FUNC STRING Get_String_From_FamilyEvent(enumFamilyEvents eFamilyEvent)
	SWITCH eFamilyEvent
		/* MICHAEL'S HOUSE */
		CASE FE_M_FAMILY_on_laptops						RETURN "FAMILY_on_laptops" BREAK
		CASE FE_M_FAMILY_MIC4_locked_in_room			RETURN "FAMILY_MIC4_locked_in_room" BREAK
		CASE FE_M7_FAMILY_finished_breakfast			RETURN "FAMILY_finished_breakfast" BREAK
		CASE FE_M7_FAMILY_finished_pizza				RETURN "FAMILY_finished_pizza" BREAK
		CASE FE_M7_FAMILY_watching_TV					RETURN "FAMILY_watching_TV" BREAK
		
		CASE FE_M_SON_sleeping							RETURN "M_SON_sleeping" BREAK
		CASE FE_M2_SON_gaming_loop						RETURN "M2_SON_gaming_loop" BREAK
//		CASE FE_M2_SON_gaming_exit						RETURN "M2_SON_gaming_exit" BREAK
//		CASE FE_M7_SON_gaming_exit						RETURN "M7_SON_gaming_exit" BREAK
		CASE FE_M_SON_rapping_in_the_shower				RETURN "M_SON_rapping_in_the_shower" BREAK
		CASE FE_M_SON_Borrows_sisters_car				RETURN "M_SON_Borrows_sisters_car" BREAK
		CASE FE_M_SON_watching_porn						RETURN "M_SON_watching_porn" BREAK
		CASE FE_M_SON_in_room_asks_for_munchies			RETURN "M_SON_in_room_asks_for_munchies" BREAK
		CASE FE_M_SON_phone_calls_in_room				RETURN "M_SON_phone_calls_in_room" BREAK
		CASE FE_M_SON_on_ecstasy_AND_friendly			RETURN "M_SON_on_ecstasy_AND_friendly" BREAK
		CASE FE_M_SON_Fighting_with_sister_A			RETURN "M_SON_Fighting_with_sister_A" BREAK
		CASE FE_M_SON_Fighting_with_sister_B			RETURN "M_SON_Fighting_with_sister_B" BREAK
		CASE FE_M_SON_Fighting_with_sister_C			RETURN "M_SON_Fighting_with_sister_C" BREAK
		CASE FE_M_SON_Fighting_with_sister_D			RETURN "M_SON_Fighting_with_sister_D" BREAK
		CASE FE_M_SON_smoking_weed_in_a_bong			RETURN "M_SON_smoking_weed_in_a_bong" BREAK
		CASE FE_M_SON_raids_fridge_for_food				RETURN "M_SON_raids_fridge_for_food" BREAK
		CASE FE_M7_SON_jumping_jacks					RETURN "M7_SON_jumping_jacks" BREAK
		CASE FE_M7_SON_gaming							RETURN "M7_SON_gaming" BREAK
		CASE FE_M7_SON_going_for_a_bike_ride			RETURN "M7_SON_going_for_a_bike_ride" BREAK
		CASE FE_M7_SON_coming_back_from_a_bike_ride		RETURN "M7_SON_coming_back_from_a_bike_ride" BREAK
		CASE FE_M7_SON_on_laptop_looking_for_jobs		RETURN "M7_SON_on_laptop_looking_for_jobs" BREAK
		CASE FE_M2_SON_watching_TV						RETURN "M2_SON_watching_TV" BREAK
		CASE FE_M7_SON_watching_TV_with_tracey			RETURN "M7_SON_watching_TV_with_tracey" BREAK
		
		CASE FE_M2_DAUGHTER_sunbathing					RETURN "M2_DAUGHTER_sunbathing" BREAK
		CASE FE_M_DAUGHTER_workout_with_mp3				RETURN "DAUGHTER_workout_with_mp3" BREAK
		CASE FE_M_DAUGHTER_Going_out_in_her_car			RETURN "DAUGHTER_Going_out_in_her_car" BREAK
		CASE FE_M_DAUGHTER_walks_to_room_music			RETURN "DAUGHTER_walks_to_room_music" BREAK
		CASE FE_M_DAUGHTER_dancing_practice				RETURN "DAUGHTER_dancing_practice" BREAK
		CASE FE_M_DAUGHTER_purges_in_the_bathroom		RETURN "DAUGHTER_purges_in_the_bathroom" BREAK
		CASE FE_M_DAUGHTER_on_phone_to_friends			RETURN "DAUGHTER_on_phone_to_friends" BREAK
		CASE FE_M_DAUGHTER_on_phone_LOCKED				RETURN "DAUGHTER_on_phone_LOCKED" BREAK
		CASE FE_M_DAUGHTER_shower						RETURN "DAUGHTER_shower" BREAK
		CASE FE_M_DAUGHTER_watching_TV_sober			RETURN "DAUGHTER_watching_TV_sober" BREAK
		CASE FE_M_DAUGHTER_watching_TV_drunk			RETURN "DAUGHTER_watching_TV_drunk" BREAK
		CASE FE_M_DAUGHTER_screaming_at_dad				RETURN "DAUGHTER_screaming_at_dad" BREAK
		CASE FE_M_DAUGHTER_sniffs_drugs_in_toilet		RETURN "DAUGHTER_sniffs_drugs_in_toilet" BREAK
		CASE FE_M_DAUGHTER_sex_sounds_from_room			RETURN "DAUGHTER_sex_sounds_from_room" BREAK
		CASE FE_M_DAUGHTER_crying_over_a_guy			RETURN "DAUGHTER_crying_over_a_guy" BREAK
		CASE FE_M_DAUGHTER_Coming_home_drunk			RETURN "DAUGHTER_Coming_home_drunk" BREAK
		CASE FE_M_DAUGHTER_sleeping						RETURN "DAUGHTER_sleeping" BREAK
		CASE FE_M_DAUGHTER_couchsleep					RETURN "DAUGHTER_couchsleep" BREAK
		CASE FE_M7_DAUGHTER_studying_on_phone			RETURN "M7_DAUGHTER_studying_on_phone" BREAK
		CASE FE_M7_DAUGHTER_studying_does_nails			RETURN "M7_DAUGHTER_studying_does_nails" BREAK
		CASE FE_M7_DAUGHTER_sunbathing					RETURN "M7_DAUGHTER_sunbathing" BREAK
		
		CASE FE_M_WIFE_screams_at_mexmaid				RETURN "M_WIFE_screams_at_mexmaid" BREAK
		CASE FE_M2_WIFE_in_face_mask					RETURN "M2_WIFE_in_face_mask" BREAK
		CASE FE_M7_WIFE_in_face_mask					RETURN "M7_WIFE_in_face_mask" BREAK
		CASE FE_M_WIFE_playing_tennis					RETURN "M_WIFE_playing_tennis" BREAK
		CASE FE_M2_WIFE_doing_yoga						RETURN "M2_WIFE_doing_yoga" BREAK
		CASE FE_M7_WIFE_doing_yoga						RETURN "M7_WIFE_doing_yoga" BREAK
//		CASE FE_M_WIFE_getting_nails_done				RETURN "M_WIFE_getting_nails_done" BREAK
//		CASE FE_M_WIFE_leaving_in_car_v2				RETURN "M_WIFE_leaving_in_car_v2" BREAK
//		CASE FE_M_WIFE_MD_leaving_in_car_v3				RETURN "M_WIFE_MD_leaving_in_car_v3" BREAK
		CASE FE_M_WIFE_leaving_in_car					RETURN "M_WIFE_leaving_in_car" BREAK
		CASE FE_M2_WIFE_with_shopping_bags_enter		RETURN "M2_WIFE_with_shopping_bags_enter" BREAK
		CASE FE_M7_WIFE_with_shopping_bags_enter		RETURN "M7_WIFE_with_shopping_bags_enter" BREAK
//		CASE FE_M2_WIFE_with_shopping_bags_idle			RETURN "M2_WIFE_with_shopping_bags_idle" BREAK
//		CASE FE_M7_WIFE_with_shopping_bags_idle			RETURN "M7_WIFE_with_shopping_bags_idle" BREAK
//		CASE FE_M2_WIFE_with_shopping_bags_exit			RETURN "M2_WIFE_with_shopping_bags_exit" BREAK
//		CASE FE_M7_WIFE_with_shopping_bags_exit			RETURN "M7_WIFE_with_shopping_bags_exit" BREAK
		CASE FE_M_WIFE_gets_drink_in_kitchen			RETURN "M_WIFE_gets_drink_in_kitchen" BREAK
		CASE FE_M2_WIFE_sunbathing						RETURN "M2_WIFE_sunbathing" BREAK
		CASE FE_M7_WIFE_sunbathing						RETURN "M7_WIFE_sunbathing" BREAK
//		CASE FE_M_WIFE_getting_botox_done				RETURN "M_WIFE_getting_botox_done" BREAK
		CASE FE_M2_WIFE_passed_out_SOFA  				RETURN "M2_WIFE_passed_out_SOFA  " BREAK
		CASE FE_M7_WIFE_passed_out_SOFA  				RETURN "M7_WIFE_passed_out_SOFA  " BREAK
//		CASE FE_M_WIFE_screaming_at_son_P1				RETURN "M_WIFE_screaming_at_son_P1" BREAK
		CASE FE_M_WIFE_screaming_at_son_P2				RETURN "M_WIFE_screaming_at_son_P2" BREAK
		CASE FE_M_WIFE_screaming_at_son_P3				RETURN "M_WIFE_screaming_at_son_P3" BREAK
		CASE FE_M_WIFE_screaming_at_daughter			RETURN "M_WIFE_screaming_at_daughter" BREAK
		CASE FE_M2_WIFE_phones_man_OR_therapist			RETURN "M2_WIFE_phones_man_OR_therapist" BREAK
		CASE FE_M7_WIFE_phones_man_OR_therapist			RETURN "M7_WIFE_phones_man_OR_therapist" BREAK
		CASE FE_M_WIFE_hangs_up_and_wanders				RETURN "M_WIFE_hangs_up_and_wanders" BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_M2_WIFE_using_vibrator					RETURN "M2_WIFE_using_vibrator" BREAK
		CASE FE_M_WIFE_using_vibrator_END				RETURN "M_WIFE_using_vibrator_END" BREAK
		CASE FE_M7_WIFE_using_vibrator					RETURN "M7_WIFE_using_vibrator" BREAK
		#ENDIF
		CASE FE_M_WIFE_passed_out_BED					RETURN "M_WIFE_passed_out_BED" BREAK
		CASE FE_M2_WIFE_sleeping						RETURN "M2_WIFE_sleeping" BREAK
		CASE FE_M7_WIFE_sleeping						RETURN "M7_WIFE_sleeping" BREAK
		CASE FE_M7_WIFE_Making_juice					RETURN "M7_WIFE_Making_juice" BREAK
		CASE FE_M7_WIFE_shopping_with_daughter			RETURN "M7_WIFE_shopping_with_daughter" BREAK
//		CASE FE_M7_WIFE_shopping_with_son				RETURN "M7_WIFE_shopping_with_son" BREAK
//		CASE FE_M7_WIFE_on_phone						RETURN "M7_WIFE_on_phone" BREAK
		
//		CASE FE_M_MEXMAID_cooking_for_son				RETURN "M_MEXMAID_cooking_for_son" BREAK
		CASE FE_M2_MEXMAID_cleans_booze_pot_other		RETURN "M2_MEXMAID_cleans_booze_pot_other" BREAK
		CASE FE_M7_MEXMAID_cleans_booze_pot_other		RETURN "M7_MEXMAID_cleans_booze_pot_other" BREAK
		CASE FE_M2_MEXMAID_clean_surface_a				RETURN "M2_MEXMAID_clean_surface_a" BREAK
		CASE FE_M2_MEXMAID_clean_surface_b				RETURN "M2_MEXMAID_clean_surface_b" BREAK
		CASE FE_M2_MEXMAID_clean_surface_c				RETURN "M2_MEXMAID_clean_surface_c" BREAK
		CASE FE_M7_MEXMAID_clean_surface				RETURN "M7_MEXMAID_clean_surface" BREAK
		CASE FE_M2_MEXMAID_clean_window					RETURN "M2_MEXMAID_clean_window" BREAK
		CASE FE_M7_MEXMAID_clean_window					RETURN "M7_MEXMAID_clean_window" BREAK
//		CASE FE_M_MEXMAID_MIC4_clean_surface			RETURN "M_MEXMAID_MIC4_clean_surface" BREAK
		CASE FE_M_MEXMAID_MIC4_clean_window				RETURN "M_MEXMAID_MIC4_clean_window" BREAK
		CASE FE_M_MEXMAID_does_the_dishes				RETURN "M_MEXMAID_does_the_dishes" BREAK
//		CASE FE_M_MEXMAID_makes_calls					RETURN "M_MEXMAID_makes_calls" BREAK
//		CASE FE_M_MEXMAID_watching_TV					RETURN "M_MEXMAID_watching_TV" BREAK
		CASE FE_M_MEXMAID_stealing_stuff				RETURN "M_MEXMAID_stealing_stuff" BREAK
		CASE FE_M_MEXMAID_stealing_stuff_caught			RETURN "M_MEXMAID_stealing_stuff_caught" BREAK
		
		CASE FE_M_GARDENER_with_leaf_blower				RETURN "GARDENER_with_leaf_blower" BREAK
		CASE FE_M_GARDENER_planting_flowers				RETURN "GARDENER_planting_flowers" BREAK
//		CASE FE_M_GARDENER_trimming_hedges				RETURN "GARDENER_trimming_hedges" BREAK
		CASE FE_M_GARDENER_cleaning_pool				RETURN "GARDENER_cleaning_pool" BREAK
		CASE FE_M_GARDENER_mowing_lawn					RETURN "GARDENER_mowing_lawn" BREAK
		CASE FE_M_GARDENER_watering_flowers				RETURN "GARDENER_watering_flowers" BREAK
//		CASE FE_M_GARDENER_spraying_for_weeds			RETURN "GARDENER_spraying_for_weeds" BREAK
		CASE FE_M_GARDENER_on_phone						RETURN "GARDENER_on_phone" BREAK
		CASE FE_M_GARDENER_smoking_weed					RETURN "GARDENER_smoking_weed" BREAK
		
//		CASE FE_M_MICHAEL_MIC2_washing_face				RETURN "FE_M_MICHAEL_MIC2_washing_face" BREAK
		
		/* FRANKLIN'S HOUSE */
		CASE FE_F_AUNT_pelvic_floor_exercises			RETURN "AUNT_pelvic_floor_exercises" BREAK
		CASE FE_F_AUNT_in_face_mask						RETURN "AUNT_in_face_mask" BREAK
		CASE FE_F_AUNT_watching_TV						RETURN "AUNT_watching_TV" BREAK
		CASE FE_F_AUNT_listens_to_selfhelp_tapes_x		RETURN "AUNT_listens_to_selfhelp_tapes" BREAK
		
		CASE FE_F_AUNT_returned_to_aunts				RETURN "FE_F_AUNT_returned_to_aunts" BREAK
		
//		CASE FE_F_LAMAR_and_STRETCH_chill_outside		RETURN "LAMAR_and_STRETCH_chill_outside" BREAK
//		CASE FE_F_LAMAR_and_STRETCH_bbq_outside			RETURN "LAMAR_and_STRETCH_bbq_outside" BREAK
//		CASE FE_F_LAMAR_and_STRETCH_arguing				RETURN "LAMAR_and_STRETCH_arguing" BREAK
//		CASE FE_F_LAMAR_and_STRETCH_shout_at_cops		RETURN "LAMAR_and_STRETCH_shout_at_cops" BREAK
		CASE FE_F_LAMAR_and_STRETCH_wandering			RETURN "LAMAR_and_STRETCH_wandering" BREAK
		
		/* TREVOR'S TRAILER - 1 */
		CASE FE_T0_RON_monitoring_police_frequency		RETURN "RON_monitoring_police_frequency" BREAK
		CASE FE_T0_RON_listens_to_radio_broadcast		RETURN "RON_listens_to_radio_broadcast" BREAK
		CASE FE_T0_RON_ranting_about_government_LAYING	RETURN "RON_ranting_about_government_LAYING" BREAK
		CASE FE_T0_RON_ranting_about_government_SITTING	RETURN "RON_ranting_about_government_SITTING" BREAK
		CASE FE_T0_RON_smoking_crystal					RETURN "RON_smoking_crystal" BREAK
		CASE FE_T0_RON_drinks_moonshine_from_a_jar		RETURN "RON_drinks_moonshine_from_a_jar" BREAK
		CASE FE_T0_RON_stares_through_binoculars		RETURN "RON_stares_through_binoculars" BREAK
		
		/* TREVOR'S TRAILER - EXILE */
		CASE FE_T0_MICHAEL_depressed_head_in_hands		RETURN "MICHAEL_depressed_head_in_hands" BREAK
		CASE FE_T0_MICHAEL_sunbathing					RETURN "MICHAEL_sunbathing" BREAK
		CASE FE_T0_MICHAEL_drinking_beer				RETURN "MICHAEL_drinking_beer" BREAK
		CASE FE_T0_MICHAEL_on_phone_to_therapist		RETURN "MICHAEL_on_phone_to_therapist" BREAK
		CASE FE_T0_MICHAEL_hangs_up_and_wanders			RETURN "MICHAEL_hangs_up_and_wanders" BREAK
		
		CASE FE_T0_TREVOR_and_kidnapped_wife_walk		RETURN "TREVOR_and_kidnapped_wife_walk" BREAK
		CASE FE_T0_TREVOR_and_kidnapped_wife_stare		RETURN "TREVOR_and_kidnapped_wife_stare" BREAK
		CASE FE_T0_TREVOR_smoking_crystal				RETURN "TREVOR_smoking_crystal" BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_T0_TREVOR_doing_a_shit					RETURN "TREVOR_doing_a_shit" BREAK
		#ENDIF
		//CASE FE_T0_TREVOR_and_kidnapped_wife_laugh		RETURN "TREVOR_and_kidnapped_wife_laugh" BREAK
		CASE FE_T0_TREVOR_blowing_shit_up				RETURN "TREVOR_blowing_shit_up" BREAK
		CASE FE_T0_TREVOR_passed_out_naked_drunk		RETURN "TREVOR_passed_out_naked_drunk" BREAK
		
		CASE FE_T0_RONEX_outside_looking_lonely			RETURN "RONEX_outside_looking_lonely" BREAK
		CASE FE_T0_RONEX_trying_to_pick_up_signals		RETURN "RONEX_trying_to_pick_up_signals" BREAK
		CASE FE_T0_RONEX_working_a_moonshine_sill		RETURN "RONEX_working_a_moonshine_sill" BREAK
		CASE FE_T0_RONEX_doing_target_practice			RETURN "RONEX_doing_target_practice" BREAK
//		CASE FE_T0_RONEX_conspiracies_boring_Michael	RETURN "RONEX_conspiracies_boring_Michael" BREAK
		
		CASE FE_T0_KIDNAPPED_WIFE_cleaning				RETURN "KIDNAPPED_WIFE_cleaning" BREAK
		CASE FE_T0_KIDNAPPED_WIFE_does_garden_work		RETURN "KIDNAPPED_WIFE_does_garden_work" BREAK
		CASE FE_T0_KIDNAPPED_WIFE_talks_to_Michael		RETURN "KIDNAPPED_WIFE_talks_to_Michael" BREAK
//		CASE FE_T0_KIDNAPPED_WIFE_cooking_a_meal		RETURN "KIDNAPPED_WIFE_cooking_a_meal" BREAK
		
		CASE FE_T0_MOTHER_duringRandomChar					RETURN "FE_T0_MOTHER_duringRandomChar" BREAK
//		CASE FE_T0_MOTHER_something_b					RETURN "FE_T0_MOTHER_something_b" BREAK
//		CASE FE_T0_MOTHER_something_c					RETURN "FE_T0_MOTHER_something_c" BREAK
		
		/* TREVOR'S SAFEHOUSE */
		CASE FE_T1_FLOYD_cleaning						RETURN "FLOYD_cleaning" BREAK
		CASE FE_T1_FLOYD_cries_in_foetal_position		RETURN "FLOYD_cries_in_the_foetal_position" BREAK
		CASE FE_T1_FLOYD_cries_on_sofa					RETURN "FLOYD_cries_on_sofa" BREAK
		CASE FE_T1_FLOYD_pineapple						RETURN "FLOYD_pineapple" BREAK
		CASE FE_T1_FLOYD_on_phone_to_girlfriend			RETURN "FLOYD_on_phone_to_girlfriend" BREAK
		CASE FE_T1_FLOYD_hangs_up_and_wanders			RETURN "FLOYD_hangs_up_and_wanders" BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_a			RETURN "FLOYD_hiding_from_Trevor_a" BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_b			RETURN "FLOYD_hiding_from_Trevor_b" BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_c			RETURN "FLOYD_hiding_from_Trevor_c" BREAK
		CASE FE_T1_FLOYD_is_sleeping					RETURN "FLOYD_is_sleeping" BREAK
		
//		CASE FE_T1_FLOYD_with_wade_post_trevor3			RETURN "FLOYD_with_wade_post_trevor3" BREAK
		CASE FE_T1_FLOYD_with_wade_post_docks1			RETURN "FLOYD_with_wade_post_docks1" BREAK
		
		
		CASE FE_ANY_find_family_event					RETURN "find_family_event" BREAK
		CASE FE_ANY_wander_family_event					RETURN "wander_family_event" BREAK
		
		CASE MAX_FAMILY_EVENTS
			RETURN "MAX_FAMILY_EVENTS"
		BREAK
		CASE FAMILY_MEMBER_BUSY
			RETURN "FAMILY_MEMBER_BUSY"
		BREAK
		CASE NO_FAMILY_EVENTS
			RETURN "NO_FAMILY_EVENTS"
		BREAK
	ENDSWITCH
	
	TEXT_LABEL_63 str = "unknown eFamilyEvent "
	str += ENUM_TO_INT(eFamilyEvent)
	
	RETURN GET_STRING_FROM_STRING(str, 0, GET_LENGTH_OF_LITERAL_STRING(str))
ENDFUNC

// *******************************************************************************************
//	GENERAL DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************

FUNC BOOL Get_DebugJumpAngle_From_Vector(VECTOR vDebugJumpOffset, FLOAT &fDebugJumpAngle, FLOAT &fDebugJumpDist)
	
	fDebugJumpAngle	= ATAN(vDebugJumpOffset.x/vDebugJumpOffset.y)
	fDebugJumpDist	= SQRT((vDebugJumpOffset.x*vDebugJumpOffset.x)+(vDebugJumpOffset.y*vDebugJumpOffset.y))
	
	IF (fDebugJumpAngle < 0.0)
		fDebugJumpAngle += 360.0
		fDebugJumpDist *= -1.0
	ENDIF
	IF (vDebugJumpOffset.x < 0.0)
		fDebugJumpDist *= -1.0
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL Get_Vector_From_DebugJumpAngle(FLOAT fDebugJumpAngle, FLOAT fDebugJumpDist, VECTOR &vDebugJumpOffset)
	
	vDebugJumpOffset = <<SIN(fDebugJumpAngle), COS(fDebugJumpAngle), 0.0>> * fDebugJumpDist
	
	RETURN TRUE
ENDFUNC

FUNC BOOL Get_DebugJumpOffset_From_FamilyEvent(enumFamilyEvents eFamilyEvent, VECTOR &vDebugJumpOffset)
	SWITCH eFamilyEvent
		CASE FE_M_FAMILY_on_laptops						vDebugJumpOffset = <<-1.6926, -2.3301, 0.0>>	RETURN TRUE BREAK
		CASE FE_M_FAMILY_MIC4_locked_in_room			vDebugJumpOffset = <<-3.5244, 6.6203, 0.0>>		RETURN TRUE BREAK
		CASE FE_M7_FAMILY_finished_breakfast
		CASE FE_M7_FAMILY_finished_pizza
			vDebugJumpOffset = <<-6.6313, -3.5038, 0.0>>
			RETURN TRUE BREAK
		CASE FE_M7_FAMILY_watching_TV					vDebugJumpOffset = <<4.5310, 5.9766, 0.0>>		RETURN TRUE BREAK
		
		CASE FE_M_SON_sleeping							vDebugJumpOffset = <<3.0500, 5.2828, 0.0>>		RETURN TRUE BREAK
		CASE FE_M2_SON_gaming_loop						vDebugJumpOffset = <<2.6398, 6.0495, 0.0>>*1.15	RETURN TRUE BREAK
//		CASE FE_M2_SON_gaming_exit						RETURN Get_DebugJumpOffset_From_FamilyEvent(FE_M2_SON_gaming_loop, vDebugJumpOffset)	BREAK
//		CASE FE_M7_SON_gaming_exit						RETURN Get_DebugJumpOffset_From_FamilyEvent(FE_M2_SON_gaming_loop, vDebugJumpOffset)	BREAK
		CASE FE_M_SON_rapping_in_the_shower				vDebugJumpOffset = <<-2.4000, -4.1040, 0.0>>	RETURN TRUE BREAK
		CASE FE_M_SON_Borrows_sisters_car				vDebugJumpOffset = <<-7.4382, 6.6973, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_SON_watching_porn						vDebugJumpOffset = <<1.1008, 10.4738, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_SON_in_room_asks_for_munchies			vDebugJumpOffset = <<2.2401, 6.2082, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_SON_phone_calls_in_room				vDebugJumpOffset = <<3.5988, 6.7810, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_SON_on_ecstasy_AND_friendly			vDebugJumpOffset = <<-4.3107, -3.2524, 0.0>>	RETURN TRUE BREAK
		CASE FE_M_SON_Fighting_with_sister_A			vDebugJumpOffset = <<-3.4166, 7.6738, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_SON_Fighting_with_sister_B			vDebugJumpOffset = <<-3.5943, 8.0730, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_SON_Fighting_with_sister_C			vDebugJumpOffset = <<-3.5185, 6.0942, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_SON_Fighting_with_sister_D			vDebugJumpOffset = <<-3.7013, 8.3133, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_SON_smoking_weed_in_a_bong			vDebugJumpOffset = <<2.6181, 6.4983, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_SON_raids_fridge_for_food				vDebugJumpOffset = <<1.0, -2.5, 0.0>>			RETURN TRUE BREAK
		CASE FE_M7_SON_jumping_jacks					vDebugJumpOffset = <<2.4863, 4.9060, 0.0>>		RETURN TRUE BREAK
		CASE FE_M7_SON_gaming							vDebugJumpOffset = <<3.2654, 6.7518, 0.0>>		RETURN TRUE BREAK
		CASE FE_M7_SON_going_for_a_bike_ride			vDebugJumpOffset = <<-11.6058, 9.0220, 0.0>>	RETURN TRUE BREAK
		CASE FE_M7_SON_coming_back_from_a_bike_ride
			vDebugJumpOffset = <<3.5464, 3.9387, 0>>
			vDebugJumpOffset *= 10.0
			vDebugJumpOffset.z = 30
			RETURN TRUE BREAK
		CASE FE_M7_SON_on_laptop_looking_for_jobs		vDebugJumpOffset = <<-4.1145, 5.6631, 0.0>>		RETURN TRUE BREAK
		CASE FE_M2_SON_watching_TV							
			Get_Vector_From_DebugJumpAngle(get_random_float_in_range(-180,180), 7.5, vDebugJumpOffset)
			RETURN TRUE BREAK
		CASE FE_M7_SON_watching_TV_with_tracey			vDebugJumpOffset = <<2.6878, 7.0019, 0.0>>		RETURN TRUE BREAK

		CASE FE_M2_DAUGHTER_sunbathing					vDebugJumpOffset = <<-6.0676, -4.4084, 0.0>>	RETURN TRUE BREAK
		CASE FE_M_DAUGHTER_workout_with_mp3				vDebugJumpOffset = <<-1.4, 2.0, 0.0>>			RETURN TRUE BREAK
		CASE FE_M_DAUGHTER_Going_out_in_her_car			vDebugJumpOffset =  <<-9.7301, 7.0232, 0.0>>	RETURN TRUE BREAK
		CASE FE_M_DAUGHTER_walks_to_room_music			vDebugJumpOffset = <<-2.9955, 4.6127, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_DAUGHTER_dancing_practice				RETURN Get_DebugJumpOffset_From_FamilyEvent(FE_M_DAUGHTER_workout_with_mp3, vDebugJumpOffset)	BREAK
		CASE FE_M_DAUGHTER_purges_in_the_bathroom		vDebugJumpOffset = <<-2.0830, 6.6829, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_DAUGHTER_on_phone_to_friends			vDebugJumpOffset = <<-1.6739, 4.6360, 0.0>>*1.25		RETURN TRUE BREAK
		CASE FE_M_DAUGHTER_on_phone_LOCKED			vDebugJumpOffset = <<-1.6739, 4.6360, 0.0>>*1.25		RETURN TRUE BREAK
		CASE FE_M_DAUGHTER_shower						vDebugJumpOffset = <<-2.3800, -4.4800, 0.0>>	RETURN TRUE BREAK
		CASE FE_M_DAUGHTER_watching_TV_sober			vDebugJumpOffset = <<1.2474, 5.8689, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_DAUGHTER_watching_TV_drunk			vDebugJumpOffset = <<0.4915, 4.8554, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_DAUGHTER_screaming_at_dad
			vDebugJumpOffset = <<-2.8241, 6.4050, 0.0>> *1.25
			RETURN TRUE BREAK
		CASE FE_M_DAUGHTER_sniffs_drugs_in_toilet		vDebugJumpOffset = <<-3.0461, 8.4688, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_DAUGHTER_sex_sounds_from_room			vDebugJumpOffset = <<-4.2436, 4.9236, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_DAUGHTER_crying_over_a_guy			vDebugJumpOffset = <<0.2748, 5.2428, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_DAUGHTER_Coming_home_drunk			vDebugJumpOffset = <<-5.2508, -4.6292, 0.0>>	RETURN TRUE BREAK
		CASE FE_M_DAUGHTER_sleeping						vDebugJumpOffset = <<-2.5658, 5.9722, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_DAUGHTER_couchsleep					vDebugJumpOffset = <<3, 4, 0.0>>*1.75			RETURN TRUE BREAK
		CASE FE_M7_DAUGHTER_studying_on_phone			vDebugJumpOffset = <<-3.3518, 6.9322, 0.0>>		RETURN TRUE BREAK
		CASE FE_M7_DAUGHTER_studying_does_nails			vDebugJumpOffset = <<-4.2707, 9.5922, 0.0>>		RETURN TRUE BREAK
		CASE FE_M7_DAUGHTER_sunbathing					RETURN Get_DebugJumpOffset_From_FamilyEvent(FE_M2_DAUGHTER_sunbathing, vDebugJumpOffset) BREAK
		CASE FE_M_WIFE_screams_at_mexmaid				vDebugJumpOffset = <<-11.0323, -3.5846, -0.0>>	RETURN TRUE BREAK
		CASE FE_M2_WIFE_in_face_mask
		CASE FE_M7_WIFE_in_face_mask
			vDebugJumpOffset = <<1.4090, 4.9406, 0.0>>	RETURN TRUE BREAK
		CASE FE_M_WIFE_playing_tennis					RETURN Get_DebugJumpOffset_From_FamilyEvent(FAMILY_MEMBER_BUSY, vDebugJumpOffset)	BREAK
		CASE FE_M2_WIFE_doing_yoga
		CASE FE_M7_WIFE_doing_yoga
			vDebugJumpOffset = <<-0.4896,-6.4815,-0.0>>	RETURN TRUE BREAK
//		CASE FE_M_WIFE_getting_nails_done				vDebugJumpOffset = <<0.8178, 5.6595, 0.0>>		RETURN TRUE BREAK
//		CASE FE_M_WIFE_leaving_in_car_v2				vDebugJumpOffset = <<-0.6481, -6.1661, 0.0>>	RETURN TRUE BREAK
//		CASE FE_M_WIFE_MD_leaving_in_car_v3				vDebugJumpOffset = <<4.3264, -7.2002, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_WIFE_leaving_in_car					vDebugJumpOffset = <<4.3264, -7.2002, 0.0>>		RETURN TRUE BREAK
		CASE FE_M2_WIFE_with_shopping_bags_enter
		CASE FE_M7_WIFE_with_shopping_bags_enter
			vDebugJumpOffset = <<9.9999, 0.0495, 0.0>>	RETURN TRUE BREAK
//		CASE FE_M2_WIFE_with_shopping_bags_idle
//		CASE FE_M7_WIFE_with_shopping_bags_idle
//			vDebugJumpOffset = <<5.9045, -1.7557, 0.0>>	RETURN TRUE BREAK
//		CASE FE_M2_WIFE_with_shopping_bags_exit
//		CASE FE_M7_WIFE_with_shopping_bags_exit
//			vDebugJumpOffset = <<5.9045, -1.7557, 0.0>>	RETURN TRUE BREAK
		CASE FE_M_WIFE_gets_drink_in_kitchen			vDebugJumpOffset = <<-1.2046, -6.3874, 0.0>>	RETURN TRUE BREAK
		CASE FE_M2_WIFE_sunbathing
		CASE FE_M7_WIFE_sunbathing
			vDebugJumpOffset = <<-3.472, -2.300, 0.0>> * 2.0	RETURN TRUE BREAK
//		CASE FE_M_WIFE_getting_botox_done				vDebugJumpOffset = <<2.5227, 4.1238, 0.0>>		RETURN TRUE BREAK
		CASE FE_M2_WIFE_passed_out_SOFA
		CASE FE_M7_WIFE_passed_out_SOFA
			vDebugJumpOffset = <<0.9184, 5.6762, 0.0>>	RETURN TRUE BREAK
//		CASE FE_M_WIFE_screaming_at_son_P1				vDebugJumpOffset = <<-5.7254, -3.0773, 0.0>>	RETURN TRUE BREAK
		CASE FE_M_WIFE_screaming_at_son_P2				vDebugJumpOffset = <<-5.7254, -3.0773, 0.0>>	RETURN TRUE BREAK
		CASE FE_M_WIFE_screaming_at_son_P3				vDebugJumpOffset = <<-5.7254, -3.0773, 0.0>>	RETURN TRUE BREAK
		CASE FE_M_WIFE_screaming_at_daughter			vDebugJumpOffset = <<-1.6078, -7.7347, 0.0000>>	RETURN TRUE BREAK
		CASE FE_M2_WIFE_phones_man_OR_therapist
		CASE FE_M7_WIFE_phones_man_OR_therapist
			vDebugJumpOffset = <<-7.3856, -3.0746, 0.0>>	RETURN TRUE BREAK
		CASE FE_M_WIFE_hangs_up_and_wanders
			Get_DebugJumpOffset_From_FamilyEvent(FE_M2_WIFE_phones_man_OR_therapist, vDebugJumpOffset)
			vDebugJumpOffset *=0.75
			RETURN TRUE BREAK
			
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_M2_WIFE_using_vibrator
		CASE FE_M_WIFE_using_vibrator_END
		CASE FE_M7_WIFE_using_vibrator
			vDebugJumpOffset = <<6.0016, -4.0113, -0.0>>		RETURN TRUE BREAK
		#ENDIF
		
		CASE FE_M_WIFE_passed_out_BED					vDebugJumpOffset = <<4.8080, -2.3900, 0.0>>		RETURN TRUE BREAK
		CASE FE_M2_WIFE_sleeping						vDebugJumpOffset = <<0.2515, -5.8946, 0.0>>		RETURN TRUE BREAK
		CASE FE_M7_WIFE_sleeping						RETURN Get_DebugJumpOffset_From_FamilyEvent(FE_M2_WIFE_sleeping, vDebugJumpOffset)	BREAK
		CASE FE_M7_WIFE_Making_juice					vDebugJumpOffset = <<6.1818, 2.0086, 0.0>>		RETURN TRUE BREAK
		CASE FE_M7_WIFE_shopping_with_daughter
			vDebugJumpOffset = <<9.0505, 3.7378, 0.0>>	RETURN TRUE BREAK
//		CASE FE_M7_WIFE_shopping_with_son							
//			vDebugJumpOffset = <<9.0505, 3.7378, 0.0>>+<<get_random_float_in_range(-0.1,0.1),get_random_float_in_range(-0.1,0.1),0>>
//			RETURN TRUE BREAK
//		CASE FE_M7_WIFE_on_phone						vDebugJumpOffset = <<2.2135, -6.1115, 0.0>>	RETURN TRUE BREAK

//		CASE FE_M_MEXMAID_cooking_for_son				vDebugJumpOffset = <<2.7949, -2.2560, 0.0>>		RETURN TRUE BREAK
		CASE FE_M2_MEXMAID_cleans_booze_pot_other		vDebugJumpOffset = <<-0.3860, -3.3530, 0.0>>	RETURN TRUE BREAK
		CASE FE_M7_MEXMAID_cleans_booze_pot_other		vDebugJumpOffset = <<-0.3860, -3.3530, 0.0>>	RETURN TRUE BREAK
		CASE FE_M2_MEXMAID_clean_surface_a				RETURN Get_DebugJumpOffset_From_FamilyEvent(FE_M7_MEXMAID_clean_surface, vDebugJumpOffset)	BREAK
		CASE FE_M2_MEXMAID_clean_surface_b				vDebugJumpOffset = <<3.4613, -4.2743, 0.0>>		RETURN TRUE BREAK
		CASE FE_M2_MEXMAID_clean_surface_c				RETURN Get_DebugJumpOffset_From_FamilyEvent(FE_M7_MEXMAID_clean_surface, vDebugJumpOffset)	BREAK
		CASE FE_M7_MEXMAID_clean_surface				vDebugJumpOffset = <<1.0430, -3.2100, 0.0>>		RETURN TRUE BREAK
		CASE FE_M2_MEXMAID_clean_window					vDebugJumpOffset = <<-6.1680, 1.6112, 0.0>>		RETURN TRUE BREAK
		CASE FE_M7_MEXMAID_clean_window					vDebugJumpOffset = <<-2.4014, 2.3714, 0.0>>		RETURN TRUE BREAK
//		CASE FE_M_MEXMAID_MIC4_clean_surface			vDebugJumpOffset = <<5.4813, 2.4404, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_MEXMAID_MIC4_clean_window				vDebugJumpOffset = <<3.5879, -3.9348, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_MEXMAID_does_the_dishes				vDebugJumpOffset = <<-0.5251, -5.0729, 0.0>>	RETURN TRUE BREAK
//		CASE FE_M_MEXMAID_makes_calls					vDebugJumpOffset = <<-1.829, -2.684, 0.0>>		RETURN TRUE BREAK
//		CASE FE_M_MEXMAID_watching_TV					vDebugJumpOffset = <<0.7092, 4.8664, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_MEXMAID_stealing_stuff				vDebugJumpOffset = <<-6.4721, -4.7023, 0.0>>	RETURN TRUE BREAK
		CASE FE_M_MEXMAID_stealing_stuff_caught
			Get_DebugJumpOffset_From_FamilyEvent(FE_M_MEXMAID_stealing_stuff, vDebugJumpOffset)
			vDebugJumpOffset *=0.75
			RETURN TRUE BREAK
		
		CASE FE_M_GARDENER_with_leaf_blower				vDebugJumpOffset = <<-0.825, 1.772, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_GARDENER_planting_flowers				vDebugJumpOffset = <<-4.9718, 3.3588, 0.0>>		RETURN TRUE BREAK
//		CASE FE_M_GARDENER_trimming_hedges				vDebugJumpOffset = <<3.6970, -0.4671, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_GARDENER_cleaning_pool				vDebugJumpOffset = <<4.8849, 2.0560, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_GARDENER_mowing_lawn					vDebugJumpOffset = <<4.8304, 4.3493, 0.0>>		RETURN TRUE BREAK
		CASE FE_M_GARDENER_watering_flowers				vDebugJumpOffset = <<-5.5, -3.8, 0.0>>			RETURN TRUE BREAK
//		CASE FE_M_GARDENER_spraying_for_weeds
//			Get_DebugJumpOffset_From_FamilyEvent(FE_M_GARDENER_watering_flowers, vDebugJumpOffset)
//			vDebugJumpOffset +=<<get_random_float_in_range(-1,1), get_random_float_in_range(-1,1), 0.0>>
//			RETURN TRUE BREAK
		CASE FE_M_GARDENER_on_phone						vDebugJumpOffset = <<3.1, -3.1, 0.0>>			RETURN TRUE BREAK
		CASE FE_M_GARDENER_smoking_weed					vDebugJumpOffset = <<0.3925, -7.4897, 0.0>>		RETURN TRUE BREAK
		
//		CASE FE_M_MICHAEL_MIC2_washing_face				vDebugJumpOffset = <<-2.6374, 8.6049, 0.0>>		RETURN TRUE BREAK
		
		CASE FE_F_AUNT_pelvic_floor_exercises			vDebugJumpOffset = <<-0.3552, 7.0627, 0.0>>		RETURN TRUE BREAK
		CASE FE_F_AUNT_in_face_mask						vDebugJumpOffset = <<-0.2076, 5.2959, 0.0>>		RETURN TRUE BREAK
		CASE FE_F_AUNT_watching_TV						vDebugJumpOffset = <<1.3685, 3.6551, 0.0>>		RETURN TRUE BREAK
		CASE FE_F_AUNT_listens_to_selfhelp_tapes_x		vDebugJumpOffset = <<-1.8000, -6.1120, 0.0>>	RETURN TRUE BREAK
		CASE FE_F_AUNT_returned_to_aunts							
			Get_Vector_From_DebugJumpAngle(get_random_float_in_range(-180,180), 7.5, vDebugJumpOffset)
			RETURN TRUE BREAK
		
//		CASE FE_F_LAMAR_and_STRETCH_chill_outside		vDebugJumpOffset = <<-6.9138, 1.0950, 0.0>>	RETURN TRUE BREAK
//		CASE FE_F_LAMAR_and_STRETCH_bbq_outside			vDebugJumpOffset = <<5.9175, 0.9915, 0.0>>		RETURN TRUE BREAK
//		CASE FE_F_LAMAR_and_STRETCH_arguing				vDebugJumpOffset = <<1.9862, -6.6052, 0.0>>		RETURN TRUE BREAK
//		CASE FE_F_LAMAR_and_STRETCH_shout_at_cops		vDebugJumpOffset = <<13.8564, -8.0000, 0.0>>	RETURN TRUE BREAK
		CASE FE_F_LAMAR_and_STRETCH_wandering			vDebugJumpOffset = <<8.0553, 0.6678, 0.0>>		RETURN TRUE BREAK
		
		CASE FE_T0_RON_monitoring_police_frequency		vDebugJumpOffset = <<-5.3937, -2.6284, 0.0>>	RETURN TRUE BREAK
		CASE FE_T0_RON_listens_to_radio_broadcast		vDebugJumpOffset = <<-5.2586, -3.8206, 0.0>>	RETURN TRUE BREAK
		CASE FE_T0_RON_ranting_about_government_LAYING	vDebugJumpOffset = <<-4.0934, -4.1103, 0.0>>	RETURN TRUE BREAK
		CASE FE_T0_RON_ranting_about_government_SITTING	RETURN Get_DebugJumpOffset_From_FamilyEvent(FE_T0_RON_ranting_about_government_LAYING, vDebugJumpOffset) BREAK
		CASE FE_T0_RON_smoking_crystal					vDebugJumpOffset = <<6.1038, 0.9930, 0.0>>		RETURN TRUE BREAK
		CASE FE_T0_RON_drinks_moonshine_from_a_jar		vDebugJumpOffset = <<7.5175, 2.7362, 0.0>>		RETURN TRUE BREAK
		CASE FE_T0_RON_stares_through_binoculars		vDebugJumpOffset = <<5.5034, 2.3900, 0.0>>		RETURN TRUE BREAK
		
		CASE FE_T0_MICHAEL_depressed_head_in_hands		vDebugJumpOffset = <<5.0000, 1.0000, 0.0>>		RETURN TRUE BREAK
		CASE FE_T0_MICHAEL_sunbathing					vDebugJumpOffset = <<6.6470, -0.4660, 0.0>>		RETURN TRUE BREAK
		CASE FE_T0_MICHAEL_drinking_beer				vDebugJumpOffset = <<-3.7554, -3.7399, 0.0>>	RETURN TRUE BREAK
		CASE FE_T0_MICHAEL_on_phone_to_therapist		vDebugJumpOffset = <<8.1915, 5.7358, 0.0>>		RETURN TRUE BREAK
		CASE FE_T0_MICHAEL_hangs_up_and_wanders
			Get_DebugJumpOffset_From_FamilyEvent(FE_T0_MICHAEL_on_phone_to_therapist, vDebugJumpOffset)
			vDebugJumpOffset *=0.75
			RETURN TRUE BREAK
		
		CASE FE_T0_TREVOR_and_kidnapped_wife_walk		vDebugJumpOffset = <<-0.8190, -5.6661, 0.0>>	RETURN TRUE BREAK
		CASE FE_T0_TREVOR_and_kidnapped_wife_stare		vDebugJumpOffset = <<-6.4721, -4.7023, 0.0>>	RETURN TRUE BREAK
		CASE FE_T0_TREVOR_smoking_crystal				vDebugJumpOffset = <<-3.1110, -4.3790, 0.0>>	RETURN TRUE BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_T0_TREVOR_doing_a_shit					vDebugJumpOffset = <<5.0962, 1.3655, 0.0>>		RETURN TRUE BREAK
		#ENDIF
		//CASE FE_T0_TREVOR_and_kidnapped_wife_laugh		vDebugJumpOffset = <<-4.0148, -4.4589, 0.0>>	RETURN TRUE BREAK
		CASE FE_T0_TREVOR_blowing_shit_up				vDebugJumpOffset = <<-4.1163, -1.7638, 0.0>>	RETURN TRUE BREAK
		CASE FE_T0_TREVOR_passed_out_naked_drunk		vDebugJumpOffset = <<5.0777, 0.4760, 0.0>>		RETURN TRUE BREAK
		
		CASE FE_T0_RONEX_outside_looking_lonely			vDebugJumpOffset = <<-5.9180, -5.4226, 0.0>>	RETURN TRUE BREAK
		CASE FE_T0_RONEX_trying_to_pick_up_signals		vDebugJumpOffset = <<6.9782, 3.9122, 0.0>>		RETURN TRUE BREAK
		CASE FE_T0_RONEX_working_a_moonshine_sill		vDebugJumpOffset = <<0.7638, -7.2670, 0.0>>		RETURN TRUE BREAK
		CASE FE_T0_RONEX_doing_target_practice			vDebugJumpOffset = <<-1.4678, -5.8177, 0.0>>	RETURN TRUE BREAK
//		CASE FE_T0_RONEX_conspiracies_boring_Michael	vDebugJumpOffset = <<-5.5694, -2.2321, 0.0>>	RETURN TRUE BREAK
		
		CASE FE_T0_KIDNAPPED_WIFE_cleaning				vDebugJumpOffset = <<-5.3158, -2.3491, 0.0>>	RETURN TRUE BREAK
		CASE FE_T0_KIDNAPPED_WIFE_does_garden_work 		vDebugJumpOffset = <<-2.0297, -7.2201, 0.0>>	RETURN TRUE BREAK
		CASE FE_T0_KIDNAPPED_WIFE_talks_to_Michael		vDebugJumpOffset = <<-3.6661, -4.4944, 0.0>>	RETURN TRUE BREAK
//		CASE FE_T0_KIDNAPPED_WIFE_cooking_a_meal		vDebugJumpOffset = <<-5.1430, -2.1999, 0.0>>	RETURN TRUE BREAK
		
		CASE FE_T0_MOTHER_duringRandomChar
//		CASE FE_T0_MOTHER_something_b
//		CASE FE_T0_MOTHER_something_c							
			vDebugJumpOffset = <<6.0917, -4.3750, 0.0000>>	* 1.1
			RETURN TRUE BREAK
			
		CASE FE_T1_FLOYD_cleaning						vDebugJumpOffset = <<-4.9387, 3.4072, 0.0>>		RETURN TRUE BREAK
		CASE FE_T1_FLOYD_cries_in_foetal_position		vDebugJumpOffset = <<-5.0968, -0.1813, 0.0>>	RETURN TRUE BREAK
		CASE FE_T1_FLOYD_cries_on_sofa							
			Get_Vector_From_DebugJumpAngle(get_random_float_in_range(-180,180), 7.5, vDebugJumpOffset)
			RETURN TRUE BREAK
		CASE FE_T1_FLOYD_pineapple						vDebugJumpOffset = <<5.4914, 2.4174, 0.0000>>	RETURN TRUE BREAK
		CASE FE_T1_FLOYD_on_phone_to_girlfriend
			vDebugJumpOffset = <<6.9486, 0.9044, 0.0>> * 1.1
			RETURN TRUE BREAK
		CASE FE_T1_FLOYD_hangs_up_and_wanders
			Get_DebugJumpOffset_From_FamilyEvent(FE_T1_FLOYD_on_phone_to_girlfriend, vDebugJumpOffset)
			vDebugJumpOffset *=0.75
			RETURN TRUE BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_a			vDebugJumpOffset = <<8.6859, 2.0120, 0.0>>		RETURN TRUE BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_b			vDebugJumpOffset = <<-7.4589, 0.7840, 0.0>>		RETURN TRUE BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_c			vDebugJumpOffset = <<-4.7908, -5.7705, 0.0>>	RETURN TRUE BREAK
		CASE FE_T1_FLOYD_is_sleeping					vDebugJumpOffset = <<-5.1390, -0.7943, 0.0>>	RETURN TRUE BREAK
//		CASE FE_T1_FLOYD_with_wade_post_trevor3			vDebugJumpOffset = <<4.9241, 0.8678, 0.0>>		RETURN TRUE BREAK
		CASE FE_T1_FLOYD_with_wade_post_docks1			vDebugJumpOffset = <<-7.9562, 0.8362, 0.0>>		RETURN TRUE BREAK
		
		
		CASE FE_ANY_find_family_event
			vDebugJumpOffset = <<0,0,0>>
			RETURN FALSE BREAK
		CASE FE_ANY_wander_family_event
			vDebugJumpOffset = <<0,0,0>>
			RETURN FALSE BREAK
		
		CASE FAMILY_MEMBER_BUSY	FALLTHRU
		CASE NO_FAMILY_EVENTS
			vDebugJumpOffset = <<0,0,0>>
			RETURN FALSE BREAK
		
		DEFAULT
			PRINTSTRING("invalid eFamilyEvent ")
			PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
			PRINTSTRING(" in Get_DebugJumpOffset_From_FamilyEvent()")
			PRINTNL()
			
			SCRIPT_ASSERT("invalid eFamilyEvent in Get_DebugJumpOffset_From_FamilyEvent()")
			
			Get_Vector_From_DebugJumpAngle(get_random_float_in_range(-180,180), 7.5, vDebugJumpOffset)
			RETURN FALSE BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("invalid something Get_DebugJumpOffset_From_FamilyEvent()")
	vDebugJumpOffset = <<0,0,0>>
	RETURN FALSE
ENDFUNC




PROC Draw_Family_Member_Cleaning_Debug_Surface_Plane(PED_INDEX PedIndex,
		FLOAT fRad, FLOAT fSurfaceOffset, HUD_COLOURS eColour)
	
	VECTOR vPedIndexCoord = GET_ENTITY_COORDS(PedIndex)
	
	VECTOR VecCoorsFirst	= vPedIndexCoord+<< fRad,-fRad, fSurfaceOffset>>
	VECTOR VecCoorsSecond	= vPedIndexCoord+<< fRad, fRad, fSurfaceOffset>>
	VECTOR VecCoorsThird	= vPedIndexCoord+<<-fRad, fRad, fSurfaceOffset>>
	VECTOR VecCoorsForth	= vPedIndexCoord+<<-fRad,-fRad, fSurfaceOffset>>
	
	INT red, green, blue, iAlpha
	GET_HUD_COLOUR(eColour, red, green, blue, iAlpha)
	iAlpha = ROUND(TO_FLOAT(iAlpha) * 0.5)
	
	DRAW_DEBUG_POLY(VecCoorsFirst, VecCoorsSecond, VecCoorsThird, red, green, blue, iAlpha)
	DRAW_DEBUG_POLY(VecCoorsFirst, VecCoorsThird, VecCoorsForth, red, green, blue, iAlpha)
	
ENDPROC

PROC Draw_Family_Member_Cleaning_Debug_Window_Plane(PED_INDEX PedIndex,
		FLOAT fRad, FLOAT fSurfaceOffset, FLOAT fSurfaceDiff, HUD_COLOURS eColour)
	
	VECTOR VecCoorsFirst = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PedIndex, << fRad, fSurfaceOffset-fSurfaceDiff,-fRad>>)
	VECTOR VecCoorsSecond = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PedIndex, << fRad, fSurfaceOffset-fSurfaceDiff ,fRad>>)
	VECTOR VecCoorsThird = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PedIndex, <<-fRad, fSurfaceOffset+fSurfaceDiff, fRad>>)
	VECTOR VecCoorsForth = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PedIndex, <<-fRad, fSurfaceOffset+fSurfaceDiff,-fRad>>)
	
	INT red, green, blue, iAlpha
	GET_HUD_COLOUR(eColour, red, green, blue, iAlpha)
	iAlpha = ROUND(TO_FLOAT(iAlpha) * 0.5)
	
	DRAW_DEBUG_POLY(VecCoorsFirst, VecCoorsSecond, VecCoorsThird, red, green, blue, iAlpha)
	DRAW_DEBUG_POLY(VecCoorsFirst, VecCoorsThird, VecCoorsForth, red, green, blue, iAlpha)
	
	DRAW_DEBUG_POLY(VecCoorsFirst, VecCoorsThird, VecCoorsSecond, 255-red, 255-green, 255-blue, iAlpha)
	DRAW_DEBUG_POLY(VecCoorsFirst, VecCoorsForth, VecCoorsThird, 255-red, 255-green, 255-blue, iAlpha)
	
ENDPROC

FLOAT fDebugWindowPlaneOffset = 0.55
FLOAT fDebugWindowPlaneDiff = 0.0
BOOL b_DebugWindowPlaneWidget = FALSE

PROC Draw_Family_Member_Cleaning_Debug_Info(PED_INDEX PedIndex, enumFamilyEvents eFamilyEvent)
	
	IF (eFamilyEvent = FE_M2_MEXMAID_clean_surface_a) OR (eFamilyEvent = FE_M2_MEXMAID_clean_surface_b) OR (eFamilyEvent = FE_M2_MEXMAID_clean_surface_c)
	OR (eFamilyEvent = FE_M7_MEXMAID_clean_surface)
//	OR (eFamilyEvent = FE_M_MEXMAID_MIC4_clean_surface)
		Draw_Family_Member_Cleaning_Debug_Surface_Plane(PedIndex, 0.8, 0.15-0.02, HUD_COLOUR_BLUELIGHT)
	ELIF (eFamilyEvent = FE_M2_MEXMAID_clean_window)
	OR (eFamilyEvent = FE_M7_MEXMAID_clean_window)
	OR (eFamilyEvent = FE_M_MEXMAID_MIC4_clean_window)
		Draw_Family_Member_Cleaning_Debug_Window_Plane(PedIndex, 0.8,
				fDebugWindowPlaneOffset,
				fDebugWindowPlaneDiff,
				HUD_COLOUR_BLUELIGHT)
			
		IF NOT b_DebugWindowPlaneWidget
//			START_WIDGET_GROUP("wg_DebugWindowPlaneWidget")
//				ADD_WIDGET_FLOAT_SLIDER("fDebugWindowPlaneOffset", fDebugWindowPlaneOffset,
//					fDebugWindowPlaneOffset - 0.1,
//					fDebugWindowPlaneOffset + 0.1,
//					0.001)
//				ADD_WIDGET_FLOAT_SLIDER("fDebugWindowPlaneDiff", fDebugWindowPlaneDiff,
//					fDebugWindowPlaneDiff - 0.1,
//					fDebugWindowPlaneDiff + 0.1,
//					0.001)
//			STOP_WIDGET_GROUP()
			
			b_DebugWindowPlaneWidget = TRUE
		ENDIF
		
	ELSE
		
	ENDIF
	
ENDPROC

#ENDIF
