//---------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------
//-- SceneTool_debug - Functions and datatypes,		for creating a rag editor that sets up and exports
//													data for your cutscene
//   sam.hackett@rockstarleeds.com
//---------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------
//
//	This tool allows you to write an editor that creates data in the following formats:
//	
//		- Pans		- these contains two camera positions, a duration, shake value, etc
//		- Cuts		- these contain one camera position, shake value, etc
//		- Markers	- these are just a vector position
//		- Placers	- these are a vector position + a float heading
//		- AngAreas	- these are two vector + width + height
//
//	If you build your editor using these data types, you can export them to temp_debug.txt and
//	easily paste them into a script your cutscenes can read.
//
//
//	Your scene editing header should include this file
//	Your scene playback header should include SceneTool_public.sch
//
//---------------------------------------------------------------------------------------------------



USING "SceneTool_public.sch"
USING "script_debug.sch"
//USING "shared_debug.sch"


// Button ID system ---------------------------------------------------------------
CONST_INT SCENETOOL_0xFF		255
CONST_INT SCENETOOL_0xFFFFFF	16777215

ENUM enumSceneTool_SelectionType
	SCENETOOL_PAN_GRABSTART = 0,
	SCENETOOL_PAN_GRABEND,
	SCENETOOL_PAN_SHOWSTART,
	SCENETOOL_PAN_SHOWEND,

	SCENETOOL_CUT_GRABCAM,
	SCENETOOL_CUT_SHOWCAM,
	
	SCENETOOL_MARKER_GRABPOS,

	SCENETOOL_PLACER_GRABPOS,
	
	SCENETOOL_ANGAREA_GRABPOS0,
	SCENETOOL_ANGAREA_GRABPOS1,
	
	SCENETOOL_POINT_GRABPOS,

	SCENETOOL_NO_SELECTION_TYPE = SCENETOOL_0xFF
ENDENUM



// Widget structs -----------------------------------------------------------------



// Tool structs -------------------------------------------------------------------
ENUM enumSceneTool_Action
	SCENETOOL_ACTION_UpdateData = 0,
	SCENETOOL_ACTION_RunScene,
	SCENETOOL_ACTION_SaveScene
ENDENUM

STRUCT structSceneTool_Launcher
	WIDGET_GROUP_ID					hWidget
	BOOL							bEnableTool
	INT								iLaunchSceneID
ENDSTRUCT




//----------------------------------------------------------------------------------------------------------------
//- Selection handle ---------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------

FUNC INT SceneTool_GetSelectionHandle(enumSceneTool_SelectionType eSelectionType, INT hWidget)
	
	INT iSelectionType	= ENUM_TO_INT(eSelectionType)
	INT hSelection		= SHIFT_LEFT(iSelectionType&SCENETOOL_0xFF, 24) | (hWidget&SCENETOOL_0xFFFFFF)
	
	RETURN hSelection

ENDFUNC

FUNC BOOL SceneTool_IsButtonSelected(enumSceneTool_SelectionType eSelectionType, INT hWidget, INT hCurSelection)

	INT iSelectionType	= ENUM_TO_INT(eSelectionType)
	INT hTestSelection	= SHIFT_LEFT(iSelectionType&SCENETOOL_0xFF, 24) | (hWidget&SCENETOOL_0xFFFFFF)
	
	IF hTestSelection = hCurSelection
		RETURN TRUE
	ENDIF
	RETURN FALSE

ENDFUNC

FUNC enumSceneTool_SelectionType SceneTool_GetSelectionType(INT hSelection)
	RETURN INT_TO_ENUM(enumSceneTool_SelectionType, SHIFT_RIGHT(hSelection, 24) & SCENETOOL_0xFF)
ENDFUNC

FUNC INT SceneTool_GetSelectionWidget(INT hSelection)
	RETURN hSelection & SCENETOOL_0xFFFFFF
ENDFUNC


PROC SceneToolDbg_DisplaySelection(structSceneTool& tool)

	TEXT_LABEL_31 tCamSel = "hCamSelection = "
	tCamSel += ENUM_TO_INT(SceneTool_GetSelectionType(tool.hCamSelection))
	tCamSel += ":"
	tCamSel += SceneTool_GetSelectionWidget(tool.hCamSelection)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.1, "STRING", tCamSel)
	
	TEXT_LABEL_31 tPosSel = "hPosSelection = "
	tPosSel += ENUM_TO_INT(SceneTool_GetSelectionType(tool.hPosSelection))
	tPosSel += ":"
	tPosSel += SceneTool_GetSelectionWidget(tool.hPosSelection)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.2, "STRING", tPosSel)

ENDPROC


//----------------------------------------------------------------------------------------------------------------
//- Tool widgets -------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------

FUNC STRING SceneToolPrv_MakeName(STRING sName, STRING sLabel)
	TEXT_LABEL_63 tFullLabel = ""
	tFullLabel += sName
	tFullLabel += sLabel
	RETURN GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(tFullLabel, GET_LENGTH_OF_LITERAL_STRING(tFullLabel))
ENDFUNC

PROC ADD_WIDGET_PAN(STRING sName, structSceneTool& tool, structSceneTool_Pan& pans[], INT hPan,				BOOL bAllowShow = TRUE, BOOL bEditShake = TRUE, BOOL bEditFov = FALSE)
	
	IF hPan >= COUNT_OF(tool.mPanWidgets)
		SCRIPT_ASSERT("ADD_WIDGET_PAN() - Out of slots for pan widgets, increase SCENETOOL_PANWIDGET_MAX")
		EXIT
	ENDIF
	
	ADD_WIDGET_BOOL(				SceneToolPrv_MakeName(sName, " - grab start camera"),		tool.mPanWidgets[hPan].bGrabStart)
	ADD_WIDGET_BOOL(				SceneToolPrv_MakeName(sName, " - grab end camera"),			tool.mPanWidgets[hPan].bGrabEnd)
	IF bAllowShow
		ADD_WIDGET_BOOL(			SceneToolPrv_MakeName(sName, " - show start camera"),		tool.mPanWidgets[hPan].bShowStart)
		ADD_WIDGET_BOOL(			SceneToolPrv_MakeName(sName, " - show end camera"),			tool.mPanWidgets[hPan].bShowEnd)
	ENDIF
	ADD_WIDGET_FLOAT_SLIDER(		SceneToolPrv_MakeName(sName, " - pan duration"),			pans[hPan].fDuration, 0.0, 15.0, 0.1)
	IF bEditShake
		ADD_WIDGET_FLOAT_SLIDER(	SceneToolPrv_MakeName(sName, " - shake"),					pans[hPan].fShake, 0.0, 1.0, 0.1)
	ENDIF
	IF bEditFov
		ADD_WIDGET_FLOAT_SLIDER(	SceneToolPrv_MakeName(sName, " - FOV"),						pans[hPan].fFov, 0.0, 100.0, 1.0)
	ENDIF

ENDPROC

PROC ADD_WIDGET_CUT(STRING sName, structSceneTool& tool, structSceneTool_Cut& cuts[], INT hCut,				BOOL bAllowShow = TRUE, BOOL bEditShake = TRUE, BOOL bEditFov = FALSE)
	
	IF hCut >= COUNT_OF(tool.mCutWidgets)
		SCRIPT_ASSERT("ADD_WIDGET_CUT() - Out of slots for cut widgets, increase SCENETOOL_CUTWIDGET_MAX")
		EXIT
	ENDIF
	
	ADD_WIDGET_BOOL(				SceneToolPrv_MakeName(sName, " - grab camera"),				tool.mCutWidgets[hCut].bGrabCam)
	IF bAllowShow
		ADD_WIDGET_BOOL(			SceneToolPrv_MakeName(sName, " - show camera"),				tool.mCutWidgets[hCut].bShowCam)
	ENDIF
	IF bEditShake
		ADD_WIDGET_FLOAT_SLIDER(	SceneToolPrv_MakeName(sName, " - shake"),					cuts[hCut].fShake, 0.0, 1.0, 0.1)
	ENDIF
	IF bEditFov
		ADD_WIDGET_FLOAT_SLIDER(	SceneToolPrv_MakeName(sName, " - FOV"),						cuts[hCut].fFov, 0.0, 100.0, 1.0)
	ENDIF

ENDPROC

PROC ADD_WIDGET_MARKER(STRING sName, structSceneTool& tool, structSceneTool_Marker& markers[], INT hMarker)

	IF hMarker >= COUNT_OF(tool.mMarkerWidgets)
		SCRIPT_ASSERT("ADD_WIDGET_MARKER() - Out of slots for marker widgets, increase SCENETOOL_MARKERWIDGET_MAX")
		EXIT
	ENDIF
	
	ADD_WIDGET_BOOL(				sName,													tool.mMarkerWidgets[hMarker].bGrabPos)
	markers[hMarker].vPos.x = markers[hMarker].vPos.x // Just to make all functions have consistent param lists

ENDPROC

PROC ADD_WIDGET_PLACER(STRING sName, structSceneTool& tool, structSceneTool_Placer& placers[], INT hPlacer, FLOAT slider_step = 15.0)
	
	IF hPlacer >= COUNT_OF(tool.mPlacerWidgets)
		SCRIPT_ASSERT("ADD_WIDGET_PLACER() - Out of slots for placer widgets, increase SCENETOOL_PLACERWIDGET_MAX")
		EXIT
	ENDIF
	
	ADD_WIDGET_BOOL(				sName,													tool.mPlacerWidgets[hPlacer].bGrabPos)
	ADD_WIDGET_FLOAT_SLIDER(		SceneToolPrv_MakeName(sName, " heading"),				placers[hPlacer].fRot, 0.0, 360.0, slider_step)

ENDPROC

PROC ADD_WIDGET_POINT(STRING sName, structSceneTool& tool, structSceneTool_point& points[], INT hpoint, FLOAT slider_step = 15.0)
	
	IF hpoint >= COUNT_OF(tool.mPointWidgets)
		SCRIPT_ASSERT("ADD_WIDGET_POINT() - Out of slots for point widgets, increase SCENETOOL_POINTWIDGET_MAX")
		EXIT
	ENDIF
	
	ADD_WIDGET_BOOL(				sName,													tool.mPointWidgets[hpoint].bGrabPos)
	ADD_WIDGET_FLOAT_SLIDER(		SceneToolPrv_MakeName(sName, ".vRot.x"),					points[hpoint].vRot.x, -180.0, 180.0, slider_step)
	ADD_WIDGET_FLOAT_SLIDER(		SceneToolPrv_MakeName(sName, ".vRot.y"),					points[hpoint].vRot.y, -180.0, 180.0, slider_step)
	ADD_WIDGET_FLOAT_SLIDER(		SceneToolPrv_MakeName(sName, ".vRot.z"),					points[hpoint].vRot.z, -180.0, 180.0, slider_step)

ENDPROC

PROC ADD_WIDGET_ANGAREA(STRING sName, structSceneTool& tool, structSceneTool_AngArea& angAreas[], INT hAngArea)

	IF hAngArea >= COUNT_OF(tool.mAngAreaWidgets)
		SCRIPT_ASSERT("ADD_WIDGET_ANGAREA() - Out of slots for AngArea widgets, increase SCENETOOL_ANGAREAWIDGET_MAX")
		EXIT
	ENDIF

	ADD_WIDGET_BOOL(				SceneToolPrv_MakeName(sName, " - grab pos 0"),			tool.mAngAreaWidgets[hAngArea].bGrabPos0)
	ADD_WIDGET_BOOL(				SceneToolPrv_MakeName(sName, " - grab pos 1"),			tool.mAngAreaWidgets[hAngArea].bGrabPos1)
	ADD_WIDGET_FLOAT_SLIDER(		SceneToolPrv_MakeName(sName, " - width"),				angAreas[hAngArea].fWidth, 0.0, 300.0, 1.0)
	ADD_WIDGET_FLOAT_SLIDER(		SceneToolPrv_MakeName(sName, " - height"),				angAreas[hAngArea].fHeight, 0.0, 300.0, 1.0)

ENDPROC



//----------------------------------------------------------------------------------------------------------------
//- Selection ----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------

PROC SceneTool_SetCamSelection(structSceneTool& tool, INT hNewSelection)
	
	INT hWidget
	REPEAT COUNT_OF(tool.mPanWidgets) hWidget
		tool.mPanWidgets[hWidget].bGrabStart	= SceneTool_IsButtonSelected(SCENETOOL_PAN_GRABSTART,	hWidget, hNewSelection)
		tool.mPanWidgets[hWidget].bGrabEnd		= SceneTool_IsButtonSelected(SCENETOOL_PAN_GRABEND,		hWidget, hNewSelection)
		
		tool.mPanWidgets[hWidget].bShowStart	= SceneTool_IsButtonSelected(SCENETOOL_PAN_SHOWSTART,	hWidget, hNewSelection)
		tool.mPanWidgets[hWidget].bShowEnd		= SceneTool_IsButtonSelected(SCENETOOL_PAN_SHOWEND,		hWidget, hNewSelection)
	ENDREPEAT

	REPEAT COUNT_OF(tool.mCutWidgets) hWidget
		tool.mCutWidgets[hWidget].bGrabCam		= SceneTool_IsButtonSelected(SCENETOOL_CUT_GRABCAM,		hWidget, hNewSelection)
		tool.mCutWidgets[hWidget].bShowCam		= SceneTool_IsButtonSelected(SCENETOOL_CUT_SHOWCAM,		hWidget, hNewSelection)
	ENDREPEAT
	
	tool.hCamSelection = hNewSelection

ENDPROC

PROC SceneTool_SetPosSelection(structSceneTool& tool, INT hNewSelection)
	
	INT hWidget
	REPEAT COUNT_OF(tool.mMarkerWidgets) hWidget
		tool.mMarkerWidgets[hWidget].bGrabPos	= SceneTool_IsButtonSelected(SCENETOOL_MARKER_GRABPOS,		hWidget, hNewSelection)
	ENDREPEAT
	REPEAT COUNT_OF(tool.mPlacerWidgets) hWidget
		tool.mPlacerWidgets[hWidget].bGrabPos	= SceneTool_IsButtonSelected(SCENETOOL_PLACER_GRABPOS,		hWidget, hNewSelection)
	ENDREPEAT
	REPEAT COUNT_OF(tool.mAngAreaWidgets) hWidget
		tool.mAngAreaWidgets[hWidget].bGrabPos0	= SceneTool_IsButtonSelected(SCENETOOL_ANGAREA_GRABPOS0,	hWidget, hNewSelection)
		tool.mAngAreaWidgets[hWidget].bGrabPos1	= SceneTool_IsButtonSelected(SCENETOOL_ANGAREA_GRABPOS1,	hWidget, hNewSelection)
	ENDREPEAT
	REPEAT COUNT_OF(tool.mPointWidgets) hWidget
		tool.mPointWidgets[hWidget].bGrabPos	= SceneTool_IsButtonSelected(SCENETOOL_POINT_GRABPOS,		hWidget, hNewSelection)
	ENDREPEAT

	tool.hPosSelection = hNewSelection

ENDPROC


FUNC BOOL SceneToolPrv_IsCamSelectionOutOfDate(structSceneTool& tool, INT& hNewSelection)

	hNewSelection = -1
	
	INT hWidget
	REPEAT COUNT_OF(tool.mPanWidgets) hWidget

		// Have any pan buttons been selected
		IF tool.mPanWidgets[hWidget].bGrabStart
			hNewSelection = SceneTool_GetSelectionHandle(SCENETOOL_PAN_GRABSTART, hWidget)
			IF hNewSelection <> tool.hPosSelection
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF tool.mPanWidgets[hWidget].bGrabEnd
			hNewSelection = SceneTool_GetSelectionHandle(SCENETOOL_PAN_GRABEND, hWidget)
			IF hNewSelection <> tool.hPosSelection
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF tool.mPanWidgets[hWidget].bShowStart
			hNewSelection = SceneTool_GetSelectionHandle(SCENETOOL_PAN_SHOWSTART, hWidget)
			IF hNewSelection <> tool.hPosSelection
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF tool.mPanWidgets[hWidget].bShowEnd
			hNewSelection = SceneTool_GetSelectionHandle(SCENETOOL_PAN_SHOWEND, hWidget)
			IF hNewSelection <> tool.hPosSelection
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDREPEAT

	REPEAT COUNT_OF(tool.mCutWidgets) hWidget

		// Have any cut buttons been selected
		IF tool.mCutWidgets[hWidget].bGrabCam
			hNewSelection = SceneTool_GetSelectionHandle(SCENETOOL_CUT_GRABCAM, hWidget)
			IF hNewSelection <> tool.hPosSelection
				RETURN TRUE
			ENDIF
		ENDIF

		IF tool.mCutWidgets[hWidget].bShowCam
			hNewSelection = SceneTool_GetSelectionHandle(SCENETOOL_CUT_SHOWCAM, hWidget)
			IF hNewSelection <> tool.hPosSelection
				RETURN TRUE
			ENDIF
		ENDIF

	ENDREPEAT

	// Should only get to here if a previous selection has been deselected
	IF hNewSelection <> tool.hPosSelection
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SceneToolPrv_IsPosSelectionOutOfDate(structSceneTool& tool, INT& hNewSelection)

	hNewSelection = -1

	INT hWidget
	REPEAT COUNT_OF(tool.mMarkerWidgets) hWidget
		
		// Have any marker buttons been selected
		IF tool.mMarkerWidgets[hWidget].bGrabPos
			hNewSelection = SceneTool_GetSelectionHandle(SCENETOOL_MARKER_GRABPOS, hWidget)
			IF hNewSelection <> tool.hPosSelection
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDREPEAT

	REPEAT COUNT_OF(tool.mPlacerWidgets) hWidget
		
		// Have any placer buttons been selected
		IF tool.mPlacerWidgets[hWidget].bGrabPos
			hNewSelection = SceneTool_GetSelectionHandle(SCENETOOL_PLACER_GRABPOS, hWidget)
			IF hNewSelection <> tool.hPosSelection
				RETURN TRUE
			ENDIF
		ENDIF

	ENDREPEAT
	
	REPEAT COUNT_OF(tool.mAngAreaWidgets) hWidget
		
		// Have any AngArea buttons been selected
		IF tool.mAngAreaWidgets[hWidget].bGrabPos0
			hNewSelection = SceneTool_GetSelectionHandle(SCENETOOL_ANGAREA_GRABPOS0, hWidget)
			IF hNewSelection <> tool.hPosSelection
				RETURN TRUE
			ENDIF
		ENDIF

		IF tool.mAngAreaWidgets[hWidget].bGrabPos1
			hNewSelection = SceneTool_GetSelectionHandle(SCENETOOL_ANGAREA_GRABPOS1, hWidget)
			IF hNewSelection <> tool.hPosSelection
				RETURN TRUE
			ENDIF
		ENDIF

	ENDREPEAT

	REPEAT COUNT_OF(tool.mPointWidgets) hWidget
		
		// Have any points buttons been selected
		IF tool.mPointWidgets[hWidget].bGrabPos
			hNewSelection = SceneTool_GetSelectionHandle(SCENETOOL_POINT_GRABPOS, hWidget)
			IF hNewSelection <> tool.hPosSelection
				RETURN TRUE
			ENDIF
		ENDIF

	ENDREPEAT
	
	// Should only get to here if a previous selection has been deselected
	IF hNewSelection <> tool.hPosSelection
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


//----------------------------------------------------------------------------------------------------------------
//- Data ---------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------

PROC SceneTool_UpdatePans(structSceneTool& tool, structSceneTool_Pan& pans[])

	// Show any selected cam
	IF tool.hCamSelection <> -1
		enumSceneTool_SelectionType	eSelectionType	= SceneTool_GetSelectionType(tool.hCamSelection)
		INT							hWidget			= SceneTool_GetSelectionWidget(tool.hCamSelection)

		IF eSelectionType = SCENETOOL_PAN_GRABSTART
			IF DOES_CAM_EXIST(GET_DEBUG_CAM()) AND IS_CAM_RENDERING(GET_DEBUG_CAM())
				pans[hWidget].mStart.vPos	= GET_CAM_COORD(GET_DEBUG_CAM())
				pans[hWidget].mStart.vRot	= GET_CAM_ROT(GET_DEBUG_CAM())
				pans[hWidget].fFov			= GET_CAM_FOV(GET_DEBUG_CAM())
				
				SceneTool_SetCamSelection(tool, -1)
				tool.bSaveOnExit = TRUE
			ENDIF
		
		ELIF eSelectionType = SCENETOOL_PAN_GRABEND
			IF DOES_CAM_EXIST(GET_DEBUG_CAM()) AND IS_CAM_RENDERING(GET_DEBUG_CAM())
				pans[hWidget].mEnd.vPos		= GET_CAM_COORD(GET_DEBUG_CAM())
				pans[hWidget].mEnd.vRot		= GET_CAM_ROT(GET_DEBUG_CAM())
				pans[hWidget].fFov			= GET_CAM_FOV(GET_DEBUG_CAM())

				SceneTool_SetCamSelection(tool, -1)
				tool.bSaveOnExit = TRUE
			ENDIF
		
		ELIF eSelectionType = SCENETOOL_PAN_SHOWSTART
			SET_CAM_COORD(GET_DEBUG_CAM(),	pans[hWidget].mStart.vPos)
			SET_CAM_ROT(GET_DEBUG_CAM(),	pans[hWidget].mStart.vRot)
			SET_CAM_FOV(GET_DEBUG_CAM(),	pans[hWidget].fFov)
			
			SceneTool_SetCamSelection(tool, -1)
//			IF DOES_CAM_EXIST(GET_DEBUG_CAM()) AND NOT IS_CAM_RENDERING(GET_DEBUG_CAM())
//				SET_DEBUG_CAM_ACTIVE(TRUE)
//				SET_DEBUG_PAD_ACTIVE(TRUE)
//			ENDIF
		
		ELIF eSelectionType = SCENETOOL_PAN_SHOWEND
			SET_CAM_COORD(GET_DEBUG_CAM(),	pans[hWidget].mEnd.vPos)
			SET_CAM_ROT(GET_DEBUG_CAM(),	pans[hWidget].mEnd.vRot)
			SET_CAM_FOV(GET_DEBUG_CAM(),	pans[hWidget].fFov)

			SceneTool_SetCamSelection(tool, -1)
//			IF DOES_CAM_EXIST(GET_DEBUG_CAM()) AND NOT IS_CAM_RENDERING(GET_DEBUG_CAM())
//				SET_DEBUG_CAM_ACTIVE(TRUE)
//				SET_DEBUG_PAD_ACTIVE(TRUE)
//			ENDIF
		
		ENDIF

	ENDIF

ENDPROC

PROC SceneTool_UpdateCuts(structSceneTool& tool, structSceneTool_Cut& cuts[])

	// Grab camera positions
	IF DOES_CAM_EXIST(GET_DEBUG_CAM()) AND IS_CAM_RENDERING(GET_DEBUG_CAM())

		// Show any selected cam
		IF tool.hCamSelection <> -1
			enumSceneTool_SelectionType	eSelectionType	= SceneTool_GetSelectionType(tool.hCamSelection)
			INT							hWidget			= SceneTool_GetSelectionWidget(tool.hCamSelection)
			
			IF eSelectionType = SCENETOOL_CUT_GRABCAM
				cuts[hWidget].mCam.vPos		= GET_CAM_COORD(GET_DEBUG_CAM())
				cuts[hWidget].mCam.vRot		= GET_CAM_ROT(GET_DEBUG_CAM())
				cuts[hWidget].fFov			= GET_CAM_FOV(GET_DEBUG_CAM())
				
				SceneTool_SetCamSelection(tool, -1)
				tool.bSaveOnExit = TRUE

			ELIF eSelectionType = SCENETOOL_CUT_SHOWCAM
				SET_CAM_COORD(GET_DEBUG_CAM(),	cuts[hWidget].mCam.vPos)
				SET_CAM_ROT(GET_DEBUG_CAM(),	cuts[hWidget].mCam.vRot)
				SET_CAM_FOV(GET_DEBUG_CAM(),	cuts[hWidget].fFov)
				
				SceneTool_SetCamSelection(tool, -1)
			
			ENDIF
		ENDIF

	ENDIF

ENDPROC

PROC SceneTool_UpdateMarkers(structSceneTool& tool, structSceneTool_Marker& markers[])

	IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)

		enumSceneTool_SelectionType	eSelectionType	= SceneTool_GetSelectionType(tool.hPosSelection)
		INT							hWidget			= SceneTool_GetSelectionWidget(tool.hPosSelection)
		
		IF eSelectionType = SCENETOOL_MARKER_GRABPOS
			markers[hWidget].vPos = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
			tool.bSaveOnExit = TRUE
		ENDIF
	
	ENDIF

ENDPROC

PROC SceneTool_UpdatePlacers(structSceneTool& tool, structSceneTool_Placer& placers[])

	IF IS_MOUSE_BUTTON_PRESSED(MB_RIGHT_BTN) AND SceneTool_GetSelectionType(tool.hPosSelection) = SCENETOOL_PLACER_GRABPOS
	
		INT hWidget = SceneTool_GetSelectionWidget(tool.hPosSelection)
		FLOAT x, y
		GET_MOUSE_POSITION(x, y)

		IF tool.mMouseDrag.bIsDragging = FALSE
			tool.mMouseDrag.rot = placers[hWidget].fRot
			tool.mMouseDrag.x = x
			tool.mMouseDrag.y = y
			tool.mMouseDrag.bIsDragging = TRUE
		ELSE
			placers[hWidget].fRot = tool.mMouseDrag.rot + ((x - tool.mMouseDrag.x) * 850.0) + ((tool.mMouseDrag.y - y) * 300.0)
			IF placers[hWidget].fRot < 0.0
				placers[hWidget].fRot += 360.0
			ELIF placers[hWidget].fRot >= 360.0
				placers[hWidget].fRot -= 360.0
			ENDIF
			tool.bSaveOnExit = TRUE
		ENDIF
	
	ELSE
		tool.mMouseDrag.bIsDragging = FALSE
		
		IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)

			enumSceneTool_SelectionType	eSelectionType	= SceneTool_GetSelectionType(tool.hPosSelection)
			INT							hWidget			= SceneTool_GetSelectionWidget(tool.hPosSelection)
			
			IF eSelectionType = SCENETOOL_PLACER_GRABPOS
				placers[hWidget].vPos = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
				tool.bSaveOnExit = TRUE
			ENDIF
		
		ENDIF
	ENDIF

ENDPROC

PROC SceneTool_UpdateAngAreas(structSceneTool& tool, structSceneTool_AngArea& angAreas[])

	enumSceneTool_SelectionType	eSelectionType = SceneTool_GetSelectionType(tool.hPosSelection)
	
	// Check for dragging size
	IF IS_MOUSE_BUTTON_PRESSED(MB_RIGHT_BTN) AND (eSelectionType = SCENETOOL_ANGAREA_GRABPOS0 OR eSelectionType = SCENETOOL_ANGAREA_GRABPOS1)
	
		INT hWidget = SceneTool_GetSelectionWidget(tool.hPosSelection)
		FLOAT x, y
		GET_MOUSE_POSITION(x, y)
		
		IF tool.mMouseDrag.bIsDragging
		ELSE
		ENDIF

		IF tool.mMouseDrag.bIsDragging = FALSE
			tool.mMouseDrag.width = angAreas[hWidget].fWidth
			tool.mMouseDrag.height = angAreas[hWidget].fHeight
			
			tool.mMouseDrag.x = x
			tool.mMouseDrag.y = y
			tool.mMouseDrag.bIsDragging = TRUE
		ELSE
			angAreas[hWidget].fWidth = tool.mMouseDrag.width + ((x - tool.mMouseDrag.x) * 10.0)
			angAreas[hWidget].fHeight = tool.mMouseDrag.height + ((y - tool.mMouseDrag.y) * 10.0)
			tool.bSaveOnExit = TRUE
		ENDIF
	
	ELSE
		tool.mMouseDrag.bIsDragging = FALSE
		
		// Check for dragging position
		IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)

			INT hWidget = SceneTool_GetSelectionWidget(tool.hPosSelection)
			
			IF eSelectionType = SCENETOOL_ANGAREA_GRABPOS0
				angAreas[hWidget].vPos0 = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
				tool.bSaveOnExit = TRUE

			ELIF eSelectionType = SCENETOOL_ANGAREA_GRABPOS1
				angAreas[hWidget].vPos1 = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
				tool.bSaveOnExit = TRUE
			
			ENDIF
		
		ENDIF
	
	ENDIF

ENDPROC

PROC SceneTool_UpdatePoints(structSceneTool& tool, structSceneTool_Point& points[])

	IF IS_MOUSE_BUTTON_PRESSED(MB_RIGHT_BTN) AND SceneTool_GetSelectionType(tool.hPosSelection) = SCENETOOL_Point_GRABPOS
	
		INT hWidget = SceneTool_GetSelectionWidget(tool.hPosSelection)
		FLOAT x, y
		GET_MOUSE_POSITION(x, y)

		IF tool.mMouseDrag.bIsDragging = FALSE
			tool.mMouseDrag.rot = points[hWidget].vRot.z
			tool.mMouseDrag.x = x
			tool.mMouseDrag.y = y
			tool.mMouseDrag.bIsDragging = TRUE
		ELSE
			points[hWidget].vRot.z = tool.mMouseDrag.rot + ((x - tool.mMouseDrag.x) * 850.0) + ((tool.mMouseDrag.y - y) * 300.0)
			IF points[hWidget].vRot.z < 0.0
				points[hWidget].vRot.z += 360.0
			ELIF points[hWidget].vRot.z >= 360.0
				points[hWidget].vRot.z -= 360.0
			ENDIF
			tool.bSaveOnExit = TRUE
		ENDIF
	
	ELSE
		tool.mMouseDrag.bIsDragging = FALSE
		
		IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)

			enumSceneTool_SelectionType	eSelectionType	= SceneTool_GetSelectionType(tool.hPosSelection)
			INT							hWidget			= SceneTool_GetSelectionWidget(tool.hPosSelection)
			
			IF eSelectionType = SCENETOOL_Point_GRABPOS
				points[hWidget].vPos = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
				tool.bSaveOnExit = TRUE
			ENDIF
		
		ENDIF
	ENDIF

ENDPROC


//----------------------------------------------------------------------------------------------------------------
//- Draw markers/placers -----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------

FUNC INT SceneToolPrv_GetPanAlpha(INT hPosSelection, INT hPan)
	IF hPosSelection <> -1
		IF SceneTool_IsButtonSelected(SCENETOOL_CUT_GRABCAM, hPan, hPosSelection)
			RETURN 255	
		ELSE
			RETURN 40//32
		ENDIF
	ENDIF
	RETURN 128
ENDFUNC

FUNC INT SceneToolPrv_GetMarkerAlpha(INT hPosSelection, INT hMarker)
	IF hPosSelection <> -1
		IF SceneTool_IsButtonSelected(SCENETOOL_MARKER_GRABPOS, hMarker, hPosSelection)
			RETURN 255	
		ELSE
			RETURN 40//32
		ENDIF
	ENDIF
	RETURN 128
ENDFUNC
	
FUNC INT SceneToolPrv_GetPlacerAlpha(INT hPosSelection, INT hPlacer)
	IF hPosSelection <> -1
		IF SceneTool_IsButtonSelected(SCENETOOL_PLACER_GRABPOS, hPlacer, hPosSelection)
			RETURN 255	
		ELSE
			RETURN 40//32
		ENDIF
	ENDIF
	RETURN 128
ENDFUNC
	
FUNC INT SceneToolPrv_GetPointAlpha(INT hPosSelection, INT hPoint)
	IF hPosSelection <> -1
		IF SceneTool_IsButtonSelected(SCENETOOL_Point_GRABPOS, hPoint, hPosSelection)
			RETURN 255	
		ELSE
			RETURN 40//32
		ENDIF
	ENDIF
	RETURN 128
ENDFUNC

PROC SceneTool_DrawPan(structSceneTool& tool, structSceneTool_Pan& Pans[], INT hPan, INT r, INT g, INT b, STRING sName = NULL)
	
	INT iAlpha = SceneToolPrv_GetPanAlpha(tool.hPosSelection, hPan)
	VECTOR vBoxSize = <<0.2,0.2,0.2>>
	
	DRAW_DEBUG_BOX(Pans[hPan].mStart.vPos-vBoxSize, Pans[hPan].mStart.vPos+vBoxSize,		r,g,b, iAlpha)
	DRAW_DEBUG_BOX(Pans[hPan].mEnd.vPos-vBoxSize, Pans[hPan].mEnd.vPos+vBoxSize,			255-r,255-g,255-b, iAlpha)
	
	DRAW_DEBUG_LINE_WITH_TWO_COLOURS(Pans[hPan].mStart.vPos, Pans[hPan].mEnd.vPos,			r,g,b, iAlpha,			255-r,255-g,255-b, iAlpha)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sName)
		TEXT_LABEL_63 str = sName
		str += " (start)"
		DRAW_DEBUG_TEXT(str, Pans[hPan].mStart.vPos,										r,g,b, iAlpha)
		
		str  = sName
		str += " (end)"
		DRAW_DEBUG_TEXT(str, Pans[hPan].mEnd.vPos,											255-r,255-g,255-b, iAlpha)
	ENDIF
ENDPROC
	
PROC SceneTool_DrawMarker(structSceneTool& tool, structSceneTool_Marker& markers[], INT hMarker, INT r, INT g, INT b, STRING sName = NULL)
	
	INT iAlpha = SceneToolPrv_GetMarkerAlpha(tool.hPosSelection, hMarker)
	
	DRAW_DEBUG_LINE(markers[hMarker].vPos-<<0,0,10>>,	markers[hMarker].vPos+<<0,0,10>>,	r,g,b, iAlpha)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sName)
		DRAW_DEBUG_TEXT(sName, markers[hMarker].vPos,										r,g,b, iAlpha)
	ENDIF
ENDPROC

PROC SceneTool_DrawMarker2(structSceneTool& tool, structSceneTool_Marker& markers[], INT hMarker, INT r, INT g, INT b, VECTOR vOrigin, STRING sName = NULL)
	
	INT iAlpha = SceneToolPrv_GetMarkerAlpha(tool.hPosSelection, hMarker)
	
	DRAW_DEBUG_LINE(markers[hMarker].vPos-<<0,0,10>>,	markers[hMarker].vPos+<<0,0,10>>,	r,g,b, iAlpha)
	DRAW_DEBUG_LINE(markers[hMarker].vPos,				vOrigin,							r,g,b, iAlpha)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sName)
		DRAW_DEBUG_TEXT(sName, markers[hMarker].vPos,										r,g,b, iAlpha)
	ENDIF
ENDPROC

PROC SceneTool_DrawPlacer(structSceneTool& tool, structSceneTool_Placer& placers[], INT hPlacer, INT r, INT g, INT b, STRING sName = NULL)
	
	VECTOR vPivotIndicator	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(placers[hPlacer].vPos, placers[hPlacer].fRot, <<0,1.5,0.5>>)
	INT iAlpha				= SceneToolPrv_GetPlacerAlpha(tool.hPosSelection, hPlacer)
	
	DRAW_DEBUG_LINE(placers[hPlacer].vPos-<<0,0,10>>,	placers[hPlacer].vPos+<<0,0,10>>,	r,g,b, iAlpha)
	DRAW_DEBUG_LINE(placers[hPlacer].vPos+<<0,0,0.5>>,	vPivotIndicator,					r,g,b, iAlpha)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sName)
		DRAW_DEBUG_TEXT(sName, placers[hPlacer].vPos,										r,g,b, iAlpha)
	ENDIF
ENDPROC

PROC SceneTool_DrawPlacer2(structSceneTool& tool, structSceneTool_Placer& placers[], INT hPlacer, INT r, INT g, INT b, VECTOR vOrigin, STRING sName = NULL)
	
	VECTOR vPivotIndicator	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(placers[hPlacer].vPos, placers[hPlacer].fRot, <<0,1.5,0.5>>)
	INT iAlpha				= SceneToolPrv_GetPlacerAlpha(tool.hPosSelection, hPlacer)
	
	DRAW_DEBUG_LINE(placers[hPlacer].vPos-<<0,0,10>>,	placers[hPlacer].vPos+<<0,0,10>>,	r,g,b, iAlpha)
	DRAW_DEBUG_LINE(placers[hPlacer].vPos+<<0,0,0.5>>,	vPivotIndicator,					r,g,b, iAlpha)
	DRAW_DEBUG_LINE(placers[hPlacer].vPos,				vOrigin,							r,g,b, iAlpha)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sName)
		DRAW_DEBUG_TEXT(sName, placers[hPlacer].vPos,										r,g,b, iAlpha)
	ENDIF
ENDPROC

PROC SceneTool_DrawAngArea(structSceneTool& tool, structSceneTool_AngArea& angAreas[], INT hAngArea, INT r, INT g, INT b)
	
//	BOOL bIsSelected = FALSE
	
	// Draw frame
	INT iAlpha = 128
	IF tool.hPosSelection <> -1
		IF SceneTool_IsButtonSelected(SCENETOOL_ANGAREA_GRABPOS0, hAngArea, tool.hPosSelection)
		OR SceneTool_IsButtonSelected(SCENETOOL_ANGAREA_GRABPOS1, hAngArea, tool.hPosSelection)
			iAlpha = 80
		ELSE
			iAlpha = 40
		ENDIF
	ENDIF
	
	VECTOR vPos0 = angAreas[hAngArea].vPos0
	VECTOR vPos1 = angAreas[hAngArea].vPos1
	
	IF vPos1.z > vPos0.z
		vPos1.z = vPos0.z
	ELSE
		vPos0.z = vPos1.z
	ENDIF
	
	VECTOR vDiff = vPos1 - vPos0
	VECTOR vPerp = << vDiff.y, -vDiff.x, 0.0 >>
	VECTOR vHeight = << 0.0, 0.0, angAreas[hAngArea].fHeight >>

	IF vPerp.x <> 0.0
	OR vPerp.y <> 0.0
		
		vPerp = NORMALISE_VECTOR(vPerp) * (angAreas[hAngArea].fWidth * 0.5)
		
		VECTOR lower[4]
		VECTOR upper[4]

		lower[0] = vPos0 - vPerp
		lower[1] = vPos0 + vPerp
		
		lower[2] = vPos1 + vPerp
		lower[3] = vPos1 - vPerp
		
		upper[0] = lower[0] + vHeight
		upper[1] = lower[1] + vHeight
		upper[2] = lower[2] + vHeight
		upper[3] = lower[3] + vHeight
		
//		DRAW_DEBUG_LINE(vPos0, vPos1,					r,g,b, iAlpha)
//		DRAW_DEBUG_LINE(vPos0+vHeight, vPos1+vHeight,	r,g,b, iAlpha)

		DRAW_DEBUG_LINE(lower[0], lower[1],		r,g,b, iAlpha)
		DRAW_DEBUG_LINE(lower[1], lower[2],		r,g,b, iAlpha)
		DRAW_DEBUG_LINE(lower[2], lower[3],		r,g,b, iAlpha)
		DRAW_DEBUG_LINE(lower[3], lower[0],		r,g,b, iAlpha)
		
		DRAW_DEBUG_LINE(lower[0], upper[0],		r,g,b, iAlpha)
		DRAW_DEBUG_LINE(lower[1], upper[1],		r,g,b, iAlpha)
		DRAW_DEBUG_LINE(lower[2], upper[2],		r,g,b, iAlpha)
		DRAW_DEBUG_LINE(lower[3], upper[3],		r,g,b, iAlpha)
		
		DRAW_DEBUG_LINE(upper[0], upper[1],		r,g,b, iAlpha)
		DRAW_DEBUG_LINE(upper[1], upper[2],		r,g,b, iAlpha)
		DRAW_DEBUG_LINE(upper[2], upper[3],		r,g,b, iAlpha)
		DRAW_DEBUG_LINE(upper[3], upper[0],		r,g,b, iAlpha)
		
	ENDIF
	
	iAlpha = 40
	IF tool.hPosSelection <> -1 //AND NOT bIsSelected
		iAlpha = 30//20
	ENDIF
	DRAW_DEBUG_POLY_ANGLED_AREA(vPos0, vPos1 + vHeight, angAreas[hAngArea].fWidth, r,g,b, iAlpha, FALSE, TRUE)

	iAlpha = 128
	IF tool.hPosSelection <> -1
		IF SceneTool_IsButtonSelected(SCENETOOL_ANGAREA_GRABPOS0, hAngArea, tool.hPosSelection)
			iAlpha = 255
		ELSE
			iAlpha = 40
		ENDIF
	ENDIF
	DRAW_DEBUG_SPHERE(angAreas[hAngArea].vPos0,	0.2,	r,g,b, iAlpha)
	DRAW_DEBUG_LINE(vPos0-<<0,0,10>>, vPos0+<<0,0,10>>,	r,g,b, iAlpha)
		
	iAlpha = 128
	IF tool.hPosSelection <> -1
		IF SceneTool_IsButtonSelected(SCENETOOL_ANGAREA_GRABPOS1, hAngArea, tool.hPosSelection)
			iAlpha = 255
		ELSE
			iAlpha = 40
		ENDIF
	ENDIF
	DRAW_DEBUG_SPHERE(angAreas[hAngArea].vPos1,	0.2,	r,g,b, iAlpha)
	DRAW_DEBUG_LINE(vPos1-<<0,0,10>>, vPos1+<<0,0,10>>,	r,g,b, iAlpha)
	
ENDPROC

PROC SceneTool_DrawPoint(structSceneTool& tool, structSceneTool_Point& points[], INT hPoint, INT r, INT g, INT b, STRING sName = NULL)
	
	INT iAlpha		= SceneToolPrv_GetPointAlpha(tool.hPosSelection, hPoint)
	FLOAT fSphereRad	= 0.2
	
	DRAW_DEBUG_SPHERE(points[hPoint].vPos,		fSphereRad,									r,g,b, iAlpha)
	
	FLOAT fLineSize	= fSphereRad*2.0
//	VECTOR vLineOffsetX = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(points[hPoint].vPos, points[hPoint].vRot.z, <<fLineSize,0,0>>) - points[hPoint].vPos
//	VECTOR vLineOffsetY = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(points[hPoint].vPos, points[hPoint].vRot.z, <<0,fLineSize,0>>) - points[hPoint].vPos
//	VECTOR vLineOffsetZ = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(points[hPoint].vPos, points[hPoint].vRot.z, <<0,0,fLineSize>>) - points[hPoint].vPos
	
	/* */
//	VECTOR vRenderingCamPos = points[hPoint].vPos
	VECTOR vRenderingCamRot = points[hPoint].vRot
	VECTOR vModifiedRenderingCamRot, vRenderingCamDirection
	
	vModifiedRenderingCamRot = vRenderingCamRot+<<1,0,0>>
	vRenderingCamDirection = <<-SIN(vModifiedRenderingCamRot.z)*COS(vModifiedRenderingCamRot.x), COS(vModifiedRenderingCamRot.z)*COS(vModifiedRenderingCamRot.x), SIN(vModifiedRenderingCamRot.x)>> 
	VECTOR vLineOffsetX = vRenderingCamDirection*fLineSize
	
	vModifiedRenderingCamRot = vRenderingCamRot+<<0,1,0>>
	vRenderingCamDirection = <<-SIN(vModifiedRenderingCamRot.z)*COS(vModifiedRenderingCamRot.x), COS(vModifiedRenderingCamRot.z)*COS(vModifiedRenderingCamRot.x), SIN(vModifiedRenderingCamRot.x)>> 
	VECTOR vLineOffsetY = vRenderingCamDirection*fLineSize
	
	vModifiedRenderingCamRot = vRenderingCamRot+<<0,0,1>>
	vRenderingCamDirection = <<-SIN(vModifiedRenderingCamRot.z)*COS(vModifiedRenderingCamRot.x), COS(vModifiedRenderingCamRot.z)*COS(vModifiedRenderingCamRot.x), SIN(vModifiedRenderingCamRot.x)>> 
	VECTOR vLineOffsetZ = vRenderingCamDirection*fLineSize
	/*	*/
	
	DRAW_DEBUG_LINE(points[hPoint].vPos-vLineOffsetX, points[hPoint].vPos+vLineOffsetX,		255,0,0, iAlpha)
	DRAW_DEBUG_LINE(points[hPoint].vPos-vLineOffsetY, points[hPoint].vPos+vLineOffsetY,		0,255,0, iAlpha)
	DRAW_DEBUG_LINE(points[hPoint].vPos-vLineOffsetZ, points[hPoint].vPos+vLineOffsetZ,		0,0,255, iAlpha)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sName)
		DRAW_DEBUG_TEXT(sName, points[hPoint].vPos,											r,g,b, iAlpha)
	ENDIF
ENDPROC


//----------------------------------------------------------------------------------------------------------------
//- Export -------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------

FUNC STRING SceneToolPrv_MakeIndent(INT iTabs)
	TEXT_LABEL_63 tIndent = ""
	INT i
	REPEAT iTabs i
		tIndent += "	"
	ENDREPEAT
	RETURN GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(tIndent, GET_LENGTH_OF_LITERAL_STRING(tIndent))
ENDFUNC

PROC SceneTool_OpenDebugFile(STRING sFuncName, STRING sCaseName, INT iIndent = 2, BOOL bExportWrapper = TRUE)

	STRING sIndent = SceneToolPrv_MakeIndent(iIndent)
	OPEN_DEBUG_FILE()
		
	// Save Dropoff to file
	SAVE_NEWLINE_TO_DEBUG_FILE()
	IF bExportWrapper
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("//----------------------- ")SAVE_STRING_TO_DEBUG_FILE(sFuncName)SAVE_STRING_TO_DEBUG_FILE(" - ")SAVE_STRING_TO_DEBUG_FILE(sCaseName)SAVE_NEWLINE_TO_DEBUG_FILE()
	ENDIF
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("CASE ")SAVE_STRING_TO_DEBUG_FILE(sCaseName)SAVE_NEWLINE_TO_DEBUG_FILE()

ENDPROC

PROC SceneTool_ExportPan(STRING sPanName, structSceneTool_Pan& pan, INT iIndent = 3)

	STRING sIndent = SceneToolPrv_MakeIndent(iIndent)
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mPans[")SAVE_STRING_TO_DEBUG_FILE(sPanName)SAVE_STRING_TO_DEBUG_FILE("].mStart.vPos = ")	SAVE_VECTOR_TO_DEBUG_FILE(pan.mStart.vPos)	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mPans[")SAVE_STRING_TO_DEBUG_FILE(sPanName)SAVE_STRING_TO_DEBUG_FILE("].mStart.vRot = ")	SAVE_VECTOR_TO_DEBUG_FILE(pan.mStart.vRot)	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mPans[")SAVE_STRING_TO_DEBUG_FILE(sPanName)SAVE_STRING_TO_DEBUG_FILE("].mEnd.vPos = ")	SAVE_VECTOR_TO_DEBUG_FILE(pan.mEnd.vPos)	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mPans[")SAVE_STRING_TO_DEBUG_FILE(sPanName)SAVE_STRING_TO_DEBUG_FILE("].mEnd.vRot = ")	SAVE_VECTOR_TO_DEBUG_FILE(pan.mEnd.vRot)	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mPans[")SAVE_STRING_TO_DEBUG_FILE(sPanName)SAVE_STRING_TO_DEBUG_FILE("].fFov = ")		SAVE_FLOAT_TO_DEBUG_FILE(pan.fFov)			SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mPans[")SAVE_STRING_TO_DEBUG_FILE(sPanName)SAVE_STRING_TO_DEBUG_FILE("].fShake = ")		SAVE_FLOAT_TO_DEBUG_FILE(pan.fShake)		SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mPans[")SAVE_STRING_TO_DEBUG_FILE(sPanName)SAVE_STRING_TO_DEBUG_FILE("].fDuration = ")	SAVE_FLOAT_TO_DEBUG_FILE(pan.fDuration)		SAVE_NEWLINE_TO_DEBUG_FILE()

ENDPROC

PROC SceneTool_ExportCut(STRING	sCutName, structSceneTool_Cut& cut, INT iIndent = 3)

	STRING sIndent = SceneToolPrv_MakeIndent(iIndent)
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mCuts[")SAVE_STRING_TO_DEBUG_FILE(sCutName)SAVE_STRING_TO_DEBUG_FILE("].mCam.vPos = ")	SAVE_VECTOR_TO_DEBUG_FILE(cut.mCam.vPos)	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mCuts[")SAVE_STRING_TO_DEBUG_FILE(sCutName)SAVE_STRING_TO_DEBUG_FILE("].mCam.vRot = ")	SAVE_VECTOR_TO_DEBUG_FILE(cut.mCam.vRot)	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mCuts[")SAVE_STRING_TO_DEBUG_FILE(sCutName)SAVE_STRING_TO_DEBUG_FILE("].fFov = ")		SAVE_FLOAT_TO_DEBUG_FILE(cut.fFov)			SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mCuts[")SAVE_STRING_TO_DEBUG_FILE(sCutName)SAVE_STRING_TO_DEBUG_FILE("].fShake = ")		SAVE_FLOAT_TO_DEBUG_FILE(cut.fShake)		SAVE_NEWLINE_TO_DEBUG_FILE()

ENDPROC

PROC SceneTool_ExportMarker(STRING sMarkerName, structSceneTool_Marker& marker, INT iIndent = 3)

	STRING sIndent = SceneToolPrv_MakeIndent(iIndent)
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mMarkers[")SAVE_STRING_TO_DEBUG_FILE(sMarkerName)SAVE_STRING_TO_DEBUG_FILE("].vPos = ")	SAVE_VECTOR_TO_DEBUG_FILE(marker.vPos)		SAVE_NEWLINE_TO_DEBUG_FILE()

ENDPROC

PROC SceneTool_ExportPlacer(STRING sPlacerName, structSceneTool_Placer& placer, INT iIndent = 3)

	STRING sIndent = SceneToolPrv_MakeIndent(iIndent)
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mPlacers[")SAVE_STRING_TO_DEBUG_FILE(sPlacerName)SAVE_STRING_TO_DEBUG_FILE("].vPos = ")	SAVE_VECTOR_TO_DEBUG_FILE(placer.vPos)		SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mPlacers[")SAVE_STRING_TO_DEBUG_FILE(sPlacerName)SAVE_STRING_TO_DEBUG_FILE("].fRot = ")	SAVE_FLOAT_TO_DEBUG_FILE(placer.fRot)		SAVE_NEWLINE_TO_DEBUG_FILE()

ENDPROC

PROC SceneTool_ExportAngArea(STRING	sAngAreaName, structSceneTool_AngArea& angArea, INT iIndent = 3)

	STRING sIndent = SceneToolPrv_MakeIndent(iIndent)
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mAngAreas[")SAVE_STRING_TO_DEBUG_FILE(sAngAreaName)SAVE_STRING_TO_DEBUG_FILE("].vPos0 = ")		SAVE_VECTOR_TO_DEBUG_FILE(angArea.vPos0)	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mAngAreas[")SAVE_STRING_TO_DEBUG_FILE(sAngAreaName)SAVE_STRING_TO_DEBUG_FILE("].vPos1 = ")		SAVE_VECTOR_TO_DEBUG_FILE(angArea.vPos1)	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mAngAreas[")SAVE_STRING_TO_DEBUG_FILE(sAngAreaName)SAVE_STRING_TO_DEBUG_FILE("].fWidth = ")		SAVE_FLOAT_TO_DEBUG_FILE(angArea.fWidth)	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mAngAreas[")SAVE_STRING_TO_DEBUG_FILE(sAngAreaName)SAVE_STRING_TO_DEBUG_FILE("].fHeight = ")		SAVE_FLOAT_TO_DEBUG_FILE(angArea.fHeight)	SAVE_NEWLINE_TO_DEBUG_FILE()

ENDPROC

PROC SceneTool_ExportPoint(STRING sPointName, structSceneTool_Point& point, INT iIndent = 3)

	STRING sIndent = SceneToolPrv_MakeIndent(iIndent)
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mPoints[")SAVE_STRING_TO_DEBUG_FILE(sPointName)SAVE_STRING_TO_DEBUG_FILE("].vPos = ")	SAVE_VECTOR_TO_DEBUG_FILE(point.vPos)		SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.mPoints[")SAVE_STRING_TO_DEBUG_FILE(sPointName)SAVE_STRING_TO_DEBUG_FILE("].vRot = ")	SAVE_VECTOR_TO_DEBUG_FILE(point.vRot)		SAVE_NEWLINE_TO_DEBUG_FILE()

ENDPROC

PROC SceneTool_ExportBool(STRING sMemberName, BOOL bBool, INT iIndent = 3)

	STRING sIndent = SceneToolPrv_MakeIndent(iIndent)
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.")SAVE_STRING_TO_DEBUG_FILE(sMemberName)SAVE_STRING_TO_DEBUG_FILE(" = ")					SAVE_BOOL_TO_DEBUG_FILE(bBool)				SAVE_NEWLINE_TO_DEBUG_FILE()

ENDPROC

PROC SceneTool_ExportFloat(STRING sMemberName, FLOAT fFloat, INT iIndent = 3)

	STRING sIndent = SceneToolPrv_MakeIndent(iIndent)
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("scene.")SAVE_STRING_TO_DEBUG_FILE(sMemberName)SAVE_STRING_TO_DEBUG_FILE(" = ")					SAVE_FLOAT_TO_DEBUG_FILE(fFloat)			SAVE_NEWLINE_TO_DEBUG_FILE()

ENDPROC

PROC SceneTool_ExpNl()
	SAVE_NEWLINE_TO_DEBUG_FILE()
ENDPROC

PROC SceneTool_CloseDebugFile(STRING sFuncName, INT iIndent = 2, BOOL bExportWrapper = TRUE)

	STRING sIndent2 = SceneToolPrv_MakeIndent(iIndent + 1)
	SAVE_STRING_TO_DEBUG_FILE(sIndent2)SAVE_STRING_TO_DEBUG_FILE("RETURN TRUE")SAVE_NEWLINE_TO_DEBUG_FILE()
	STRING sIndent = SceneToolPrv_MakeIndent(iIndent)
	SAVE_STRING_TO_DEBUG_FILE(sIndent)SAVE_STRING_TO_DEBUG_FILE("BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
	IF bExportWrapper
	SAVE_STRING_TO_DEBUG_FILE("//----------------------- ")SAVE_STRING_TO_DEBUG_FILE(sFuncName)SAVE_STRING_TO_DEBUG_FILE(" - END")SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_NEWLINE_TO_DEBUG_FILE()
	ENDIF
	SAVE_NEWLINE_TO_DEBUG_FILE()

	CLOSE_DEBUG_FILE()

ENDPROC

//----------------------------------------------------------------------------------------------------------------
//- Launcher widgets ---------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------

PROC START_SCENETOOL_LAUNCHER(STRING sTitle, structSceneTool_Launcher& launcher)
	launcher.bEnableTool = FALSE
	launcher.iLaunchSceneID = 0
	launcher.hWidget = START_WIDGET_GROUP(sTitle)
		ADD_WIDGET_BOOL("Edit", launcher.bEnableTool)
		START_NEW_WIDGET_COMBO()
ENDPROC

PROC STOP_SCENETOOL_LAUNCHER(structSceneTool_Launcher& launcher)
	STOP_WIDGET_COMBO("Scene to edit", launcher.iLaunchSceneID)
	STOP_WIDGET_GROUP()
ENDPROC


//----------------------------------------------------------------------------------------------------------------
//- Tool widgets -------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------

PROC START_SCENETOOL_GROUP(structSceneTool_Launcher& launcher, structSceneTool& tool, STRING sTitle)
	
	// Init widgets
	SceneTool_SetCamSelection(tool, -1)
	SceneTool_SetPosSelection(tool, -1)
	tool.mMouseDrag.bIsDragging = FALSE
	
	tool.bRunScene			= FALSE
	tool.bSaveNow			= FALSE
	tool.bSaveOnExit		= FALSE
	tool.bForceDebugDraw	= FALSE

	// Start widget group
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	SET_CURRENT_WIDGET_GROUP(launcher.hWidget)

	tool.hWidget = START_WIDGET_GROUP(sTitle)
		ADD_WIDGET_BOOL("Run scene",		tool.bRunScene)
		ADD_WIDGET_BOOL("Save now",			tool.bSaveNow)
		ADD_WIDGET_BOOL("Save on exit",		tool.bSaveOnExit)
ENDPROC

PROC STOP_SCENETOOL_GROUP(structSceneTool_Launcher& launcher, structSceneTool& tool)
	START_WIDGET_GROUP("Prefs")
	ADD_WIDGET_BOOL("(Force debug draw on)",tool.bForceDebugDraw)
	STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(launcher.hWidget)
ENDPROC

FUNC enumSceneTool_Action SceneTool_UpdateTool(structSceneTool& tool)
	
	INT hNewSelection

	// Print "can grab" message to screen
	IF DOES_CAM_EXIST(GET_DEBUG_CAM()) AND IS_CAM_RENDERING(GET_DEBUG_CAM())
		SET_TEXT_COLOUR(100, 255, 255, 255)
		SET_TEXT_SCALE(0.7, 0.8)
		SET_TEXT_WRAP(0.0, 1.0)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.6, 0.85, "STRING", "CAN GRAB CAMERA")
	ENDIF
	
	// Update selection
	IF SceneToolPrv_IsCamSelectionOutOfDate(tool, hNewSelection)
		SceneTool_SetCamSelection(tool, hNewSelection)
	ENDIF

	IF SceneToolPrv_IsPosSelectionOutOfDate(tool, hNewSelection)
		SceneTool_SetPosSelection(tool, hNewSelection)
	ENDIF
	
//	SceneToolDbg_DisplaySelection(tool)

	// Check froce debug draw
	IF tool.bForceDebugDraw
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	ENDIF
	
	// Check run scene button
	IF tool.bRunScene
		tool.bRunScene = FALSE
		RETURN SCENETOOL_ACTION_RunScene
	ENDIF
	
	// Check save scene button
	IF tool.bSaveNow
		tool.bSaveNow = FALSE
		tool.bSaveOnExit = FALSE
		RETURN SCENETOOL_ACTION_SaveScene
	ENDIF
	
	RETURN SCENETOOL_ACTION_UpdateData
ENDFUNC


FUNC enumSceneTool_Action SceneTool_DestroyTool(structSceneTool& tool)
	
	DELETE_WIDGET_GROUP(tool.hWidget)
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	
	// Check save scene on exit flag
	IF tool.bSaveOnExit
		tool.bSaveOnExit = FALSE
		RETURN SCENETOOL_ACTION_SaveScene
	ENDIF
	
	RETURN SCENETOOL_ACTION_UpdateData

ENDFUNC

PROC KILL_BLOCKING_OBJECTS(OBJECT_INDEX &DoorBlocker, OBJECT_INDEX &garageBlocker)
	IF DOES_ENTITY_EXIST(DoorBlocker)
		PRINTLN("AM_MP_PROPERTY_EXT: A tool is enabled entry object exists")
		//IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(DoorBlocker)
		DELETE_OBJECT(DoorBlocker)	
		PRINTLN("AM_MP_PROPERTY_EXT: Deleting entry collsion object for tool")
		//ELSE
		//	NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(DoorBlocker)
		//ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(garageBlocker)
		PRINTLN("AM_MP_PROPERTY_EXT: A tool is enabled garage object exists")
		//IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(garageBlocker)
		DELETE_OBJECT(garageBlocker)	
		PRINTLN("AM_MP_PROPERTY_EXT: Deleting garage collsion object for tool")
		//ELSE
		//	NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(garageBlocker)
		//ENDIF
	ENDIF
ENDPROC


