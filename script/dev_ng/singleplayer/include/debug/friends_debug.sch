USING "rage_builtins.sch"
USING "globals.sch"

USING "friendController_private.sch"
USING "friendActivity_private.sch"
USING "friendActivity_dropoff_private.sch"
USING "friendActivity_bar_private.sch"

#IF IS_DEBUG_BUILD
	USING "script_debug.sch"
	USING "shared_debug.sch"
//	USING "hud_creator_tool.sch"

///debug interface for friends scripts
///    sam.hackett@rockstarleeds.com
///    

// *******************************************************************************************
//	DEBUG UTILS
// *******************************************************************************************

PROC SAVE_DIALOGUESTAR_LINE_TO_DEBUG_FILE(STRING sRoot, STRING sDescription,
		enumCharacterList eName, STRING sDialogue, BOOL bCellphone)
	
	STRING sGuid
	SWITCH eName
		CASE CHAR_MICHAEL	sGuid="74A35B39-171E-4CD8-A348-F119DD0C638A"	BREAK
		CASE CHAR_FRANKLIN	sGuid="D116FE4D-A753-40C5-B9B5-129A7DF2911D"	BREAK
		CASE CHAR_TREVOR	sGuid="88F3AD4C-C46D-462A-8049-B335741D94C4"	BREAK
		CASE CHAR_LAMAR		sGuid="C82DCD89-2093-421B-8AD8-177C151F7B46"	BREAK
		CASE CHAR_JIMMY		sGuid="5fa02f57-dacc-42cc-9d38-ee36683e68fe"	BREAK
		CASE CHAR_AMANDA	sGuid="63883d26-a2f5-4e61-bed7-526d89d63a15"	BREAK
		
		DEFAULT				sGuid="XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"	BREAK
		
	ENDSWITCH
	
	SAVE_STRING_TO_DEBUG_FILE("<Conversation placeholder=\"True\" root=\"")
	SAVE_STRING_TO_DEBUG_FILE(sRoot)
	SAVE_STRING_TO_DEBUG_FILE("\" description=\"")
	SAVE_STRING_TO_DEBUG_FILE(sDescription)
	SAVE_STRING_TO_DEBUG_FILE("\" random=\"False\" ")
	IF NOT bCellphone
		SAVE_STRING_TO_DEBUG_FILE("category=\"Default\"")
	ELSE
		SAVE_STRING_TO_DEBUG_FILE("category=\"Cellphone\"")
	ENDIF
	SAVE_STRING_TO_DEBUG_FILE("  >")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	SAVE_STRING_TO_DEBUG_FILE("	<Lines>")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("		<Line guid=\"")
	SAVE_STRING_TO_DEBUG_FILE(sGuid)
	SAVE_STRING_TO_DEBUG_FILE("\" dialogue=\"")
	SAVE_STRING_TO_DEBUG_FILE(sDialogue)
	SAVE_STRING_TO_DEBUG_FILE("\" speaker=\"0\" listener=\"1\" ")
	IF NOT bCellphone
		SAVE_STRING_TO_DEBUG_FILE("special=\"Face_to_face\"")
	ELSE
		SAVE_STRING_TO_DEBUG_FILE("special=\"Cellphone\"")
	ENDIF
	SAVE_STRING_TO_DEBUG_FILE("  />")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("	</Lines>")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("</Conversation>")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
ENDPROC

PROC DEBUG_RequestAndWaitAdditionalText(STRING pTextBlockName, TEXT_BLOCK_SLOTS SlotNumber)
	REQUEST_ADDITIONAL_TEXT(pTextBlockName, SlotNumber)
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(SlotNumber)
		REQUEST_ADDITIONAL_TEXT(pTextBlockName, SlotNumber)
		WAIT(0)
	ENDWHILE
ENDPROC
#if USE_CLF_DLC
FUNC STRING GetLabel_SAVEHOUSE_NAME_ENUMCLF(SAVEHOUSE_NAME_ENUM eName)
	SWITCH eName
		CASE SAVEHOUSEclf_MICHAEL_BH			RETURN "SAVEHOUSEclf_MICHAEL_BH"		BREAK
		CASE SAVEHOUSEclf_MICHAEL_CS			RETURN "SAVEHOUSEclf_MICHAEL_CS"		BREAK
		CASE SAVEHOUSEclf_MICHAEL_PRO			RETURN "SAVEHOUSEclf_MICHAEL_PRO"		BREAK
		CASE SAVEHOUSEclf_TREVOR_CS				RETURN "SAVEHOUSEclf_TREVOR_CS"			BREAK
		CASE SAVEHOUSEclf_TREVOR_VB				RETURN "SAVEHOUSEclf_TREVOR_VB"			BREAK
		CASE SAVEHOUSEclf_TREVOR_SC				RETURN "SAVEHOUSEclf_TREVOR_SC"			BREAK
		CASE SAVEHOUSEclf_TREVOR_PRO			RETURN "SAVEHOUSEclf_TREVOR_PRO"		BREAK
		CASE SAVEHOUSEclf_FRANKLIN_SC			RETURN "SAVEHOUSEclf_FRANKLIN_SC"		BREAK
		CASE SAVEHOUSEclf_FRANKLIN_VH			RETURN "SAVEHOUSEclf_FRANKLIN_VH"		BREAK
		CASE SAVEHOUSEclf_FRANKLIN_PRO			RETURN "SAVEHOUSEclf_FRANKLIN_PRO"		BREAK
	ENDSWITCH
	
	RETURN "Invalid savehouse name"
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC STRING GetLabel_SAVEHOUSE_NAME_ENUMNRM(SAVEHOUSE_NAME_ENUM eName)
	SWITCH eName
		CASE SAVEHOUSENRM_BH			RETURN "SAVEHOUSENRM_BH"			BREAK
		CASE SAVEHOUSENRM_CHATEAU		RETURN "SAVEHOUSENRM_CHATEAU"		BREAK		
	ENDSWITCH
	
	RETURN "Invalid savehouse name"
ENDFUNC
#endif
FUNC STRING GetLabel_SAVEHOUSE_NAME_ENUM(SAVEHOUSE_NAME_ENUM eName)
#if USE_CLF_DLC
	return GetLabel_SAVEHOUSE_NAME_ENUMCLF(eName)
#endif
#if USE_NRM_DLC
	return GetLabel_SAVEHOUSE_NAME_ENUMNRM(eName)
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	
	SWITCH eName
		CASE SAVEHOUSE_MICHAEL_BH			RETURN "SAVEHOUSE_MICHAEL_BH"		BREAK
		CASE SAVEHOUSE_MICHAEL_CS			RETURN "SAVEHOUSE_MICHAEL_CS"		BREAK
		CASE SAVEHOUSE_MICHAEL_PRO			RETURN "SAVEHOUSE_MICHAEL_PRO"		BREAK
		CASE SAVEHOUSE_TREVOR_CS			RETURN "SAVEHOUSE_TREVOR_CS"		BREAK
		CASE SAVEHOUSE_TREVOR_VB			RETURN "SAVEHOUSE_TREVOR_VB"		BREAK
		CASE SAVEHOUSE_TREVOR_SC			RETURN "SAVEHOUSE_TREVOR_SC"		BREAK
		CASE SAVEHOUSE_TREVOR_PRO			RETURN "SAVEHOUSE_TREVOR_PRO"		BREAK
		CASE SAVEHOUSE_FRANKLIN_SC			RETURN "SAVEHOUSE_FRANKLIN_SC"		BREAK
		CASE SAVEHOUSE_FRANKLIN_VH			RETURN "SAVEHOUSE_FRANKLIN_VH"		BREAK
		CASE SAVEHOUSE_FRANKLIN_PRO			RETURN "SAVEHOUSE_FRANKLIN_PRO"		BREAK
	ENDSWITCH
	
	RETURN "Invalid savehouse name"
#endif
#endif	
ENDFUNC




// *******************************************************************************************
//	RAG DEBUGGING SERVICES
// *******************************************************************************************



//---------------------------------------------------------------------------------------------------
//-- FRIEND LOCATIONS (debug)
//---------------------------------------------------------------------------------------------------

PROC PRIVATE_DEBUG_DisplayFriendLocations(WIDGET_GROUP_ID& hParentWidgetGroup, BOOL& bDisplayFriendLocations)

	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_DisplayFriendLocations()")

	// Rag accessors
	BOOL			bJumpToPickupCoords[MAX_FRIEND_LOCATIONS]
	BOOL			bJumpToSpawnPos[MAX_FRIEND_LOCATIONS]
	BOOL			bJumpToParkPos[MAX_FRIEND_LOCATIONS]
	BOOL			bGrabAnchorPos[MAX_FRIEND_LOCATIONS]
	BOOL			bGrabSpawnPos[MAX_FRIEND_LOCATIONS]
	BOOL			bGrabParkPos[MAX_FRIEND_LOCATIONS]

	// Vars for temp peds/blips at locations
	WIDGET_GROUP_ID	hWidget[MAX_FRIEND_LOCATIONS]
	BLIP_INDEX		hDebugBlip[MAX_FRIEND_LOCATIONS]
	PED_INDEX		hDebugPedA
	PED_INDEX		hDebugPedB

	enumFriendLocation eFriendLoc
	

	// Create widgets for each FriendLocation
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_DisplayFriendLocations() // Create widgets for each FriendLocation")

	SET_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
		REPEAT MAX_FRIEND_LOCATIONS eFriendLoc
			IF eFriendLoc <> FLOC_adhoc
				hWidget[eFriendLoc] = 
				START_WIDGET_GROUP(GetLabel_enumFriendLocation(eFriendLoc))
					ADD_WIDGET_BOOL("Warp to location", 	bJumpToPickupCoords[eFriendLoc])
					ADD_WIDGET_BOOL("Warp to spawn pos", 	bJumpToSpawnPos[eFriendLoc])
					ADD_WIDGET_BOOL("Warp to park pos", 	bJumpToParkPos[eFriendLoc])
					ADD_WIDGET_STRING(".")
					ADD_WIDGET_VECTOR_SLIDER("Anchor",		g_FriendLocations[eFriendLoc].vPickupCoord, -9999.0, 9999.0, 0.1)
					ADD_WIDGET_STRING(".")
					ADD_WIDGET_VECTOR_SLIDER("PedA offset",	g_FriendLocations[eFriendLoc].vPedOffsetA, -30.0, 30.0, 0.1)
					ADD_WIDGET_STRING(".")
					ADD_WIDGET_VECTOR_SLIDER("PedB offset",	g_FriendLocations[eFriendLoc].vPedOffsetB, -30.0, 30.0, 0.1)
					ADD_WIDGET_STRING(".")
					ADD_WIDGET_BOOL("Grab anchor pos",		bGrabAnchorPos[eFriendLoc])
					ADD_WIDGET_BOOL("Grab spawn pos",		bGrabSpawnPos[eFriendLoc])
					ADD_WIDGET_BOOL("Grab park pos",		bGrabParkPos[eFriendLoc])
				STOP_WIDGET_GROUP()
			ENDIF
		ENDREPEAT
	CLEAR_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
	

	// Create peds/blips at each FriendLocation
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_DisplayFriendLocations() // Load models")

	MODEL_NAMES ped_modelA = GET_FRIEND_MODEL(FR_MICHAEL)
	MODEL_NAMES ped_modelB = GET_FRIEND_MODEL(FR_LAMAR)
	
	// Load models + anims
	REQUEST_MODEL(ped_modelA)
	REQUEST_MODEL(ped_modelB)
	REQUEST_ANIM_DICT("friends@laf@ig_1@idle_a")
	REQUEST_ANIM_DICT("friends@")
	
	WHILE NOT HAS_MODEL_LOADED(ped_modelA)
	OR    NOT HAS_MODEL_LOADED(ped_modelB)
	OR    NOT HAS_ANIM_DICT_LOADED("friends@laf@ig_1@idle_a")
	OR    NOT HAS_ANIM_DICT_LOADED("friends@")

		REQUEST_MODEL(ped_modelA)
		REQUEST_MODEL(ped_modelB)
		REQUEST_ANIM_DICT("friends@laf@ig_1@idle_a")
		REQUEST_ANIM_DICT("friends@")
		WAIT(0)
		
	ENDWHILE
	
	// Create peds
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_DisplayFriendLocations() // Create peds/blips")
	enumFriendLocation eRecentLoc = FLOC_coffeeShop_RH
	
	VECTOR vDriveway = g_FriendLocations[eRecentLoc].vPickupCoord
	VECTOR vDoorstepA = g_FriendLocations[eRecentLoc].vPickupCoord + g_FriendLocations[eRecentLoc].vPedOffsetA
	VECTOR vDoorstepB = g_FriendLocations[eRecentLoc].vPickupCoord + g_FriendLocations[eRecentLoc].vPedOffsetB
	
	CLEAR_AREA(vDriveway, 25.0, TRUE)
	hDebugPedA = CREATE_PED(PEDTYPE_MISSION, ped_modelA, vDoorstepA, 0.0)
	hDebugPedB = CREATE_PED(PEDTYPE_MISSION, ped_modelB, vDoorstepB, 0.0)
	
	SET_ENTITY_PROOFS(hDebugPedA, TRUE, TRUE, TRUE, TRUE, TRUE)
	SET_ENTITY_PROOFS(hDebugPedB, TRUE, TRUE, TRUE, TRUE, TRUE)
	
	TASK_PLAY_ANIM(hDebugPedA, "friends@laf@ig_1@idle_a", "idle_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
	TASK_PLAY_ANIM(hDebugPedB, "friends@", "pickupwait", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(ped_modelA)
	SET_MODEL_AS_NO_LONGER_NEEDED(ped_modelB)
	
	// Create blips
	REPEAT MAX_FRIEND_LOCATIONS eFriendLoc
		IF eFriendLoc <> FLOC_adhoc
			hDebugBlip[eFriendLoc] = ADD_BLIP_FOR_COORD(vDriveway)
			
			IF FriendLoc_AreFlagsSet(eFriendLoc, FLF_IsHomeLoc)								// HomeLoc blips are blue, otherwise red
				SET_BLIP_COLOUR(hDebugBlip[eFriendLoc], BLIP_COLOUR_BLUE)
			ELSE
				SET_BLIP_COLOUR(hDebugBlip[eFriendLoc], BLIP_COLOUR_RED)
			ENDIF
		#if USE_CLF_DLC
			IF g_FriendLocations[eFriendLoc].hSavehouse <> NUMBER_OF_CLF_SAVEHOUSE	// If it's linked to a safehouse it's larger
				SET_BLIP_SCALE(hDebugBlip[eFriendLoc], 1.25)
			ENDIF
		#endif
		#if USE_NRM_DLC
			IF g_FriendLocations[eFriendLoc].hSavehouse <> NUMBER_OF_NRM_SAVEHOUSE	// If it's linked to a safehouse it's larger
				SET_BLIP_SCALE(hDebugBlip[eFriendLoc], 1.25)
			ENDIF
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			IF g_FriendLocations[eFriendLoc].hSavehouse <> NUMBER_OF_SAVEHOUSE_LOCATIONS	// If it's linked to a safehouse it's larger
				SET_BLIP_SCALE(hDebugBlip[eFriendLoc], 1.25)
			ENDIF
		#endif
		#endif			
						
			SET_BLIP_NAME_FROM_TEXT_FILE(hDebugBlip[eFriendLoc], GetLabel_enumFriendLocation(eFriendLoc))
		ENDIF
	ENDREPEAT
	

	// Maintain widgets/FriendLocations
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_DisplayFriendLocations() // Maintain widgets/FriendLocations")
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)

	WHILE bDisplayFriendLocations
		REPEAT MAX_FRIEND_LOCATIONS eFriendLoc
			IF eFriendLoc <> FLOC_adhoc
				vDriveway = g_FriendLocations[eFriendLoc].vPickupCoord
				vDoorstepA = g_FriendLocations[eFriendLoc].vPickupCoord + g_FriendLocations[eFriendLoc].vPedOffsetA
				vDoorstepB = g_FriendLocations[eFriendLoc].vPickupCoord + g_FriendLocations[eFriendLoc].vPedOffsetB

				// Warp player if requested
				IF bJumpToPickupCoords[eFriendLoc]
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vDriveway)
						SET_ENTITY_FACING(PLAYER_PED_ID(), vDoorstepA)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					ENDIF
					eRecentLoc = eFriendLoc
					bJumpToPickupCoords[eFriendLoc] = FALSE
				ENDIF
				
				IF bJumpToSpawnPos[eFriendLoc]
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), g_FriendLocations[eFriendLoc].vSpawnPos)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), g_FriendLocations[eFriendLoc].fSpawnRot)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					ENDIF
					bJumpToSpawnPos[eFriendLoc] = FALSE
				ENDIF
				
				IF bJumpToParkPos[eFriendLoc]
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), g_FriendLocations[eFriendLoc].vParkPos)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					ENDIF
					bJumpToParkPos[eFriendLoc] = FALSE
				ENDIF
				
				IF bGrabAnchorPos[eFriendLoc]
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						VECTOR vAnchorNewPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						GET_GROUND_Z_FOR_3D_COORD(vAnchorNewPos+<<0,0,1>>, vAnchorNewPos.z)
						
						VECTOR vAnchorChange = vAnchorNewPos - g_FriendLocations[eFriendLoc].vPickupCoord
						
						g_FriendLocations[eFriendLoc].vPickupCoord	= vAnchorNewPos
						g_FriendLocations[eFriendLoc].vPedOffsetA	-= vAnchorChange
						g_FriendLocations[eFriendLoc].vPedOffsetB	-= vAnchorChange
					ENDIF
					bGrabAnchorPos[eFriendLoc] = FALSE
				ENDIF

				IF bGrabSpawnPos[eFriendLoc]
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						g_FriendLocations[eFriendLoc].vSpawnPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						g_FriendLocations[eFriendLoc].fSpawnRot = GET_ENTITY_HEADING(PLAYER_PED_ID())
					ENDIF
					bGrabSpawnPos[eFriendLoc] = FALSE
				ENDIF
				
				IF bGrabParkPos[eFriendLoc]
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						g_FriendLocations[eFriendLoc].vParkPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
					ENDIF
					bGrabParkPos[eFriendLoc] = FALSE
				ENDIF
				
				// Update blip position
				IF DOES_BLIP_EXIST(hDebugBlip[eFriendLoc])
					SET_BLIP_COORDS(hDebugBlip[eFriendLoc], vDriveway)
				ENDIF
				
				// Update ped position
				IF eFriendLoc = eRecentLoc
					IF NOT IS_PED_INJURED(hDebugPedA)
						SET_ENTITY_COORDS(hDebugPedA, vDoorstepA)
						SET_ENTITY_FACING(hDebugPedA, vDriveway)
					ENDIF
					IF NOT IS_PED_INJURED(hDebugPedB)
						SET_ENTITY_COORDS(hDebugPedB, vDoorstepB)
						SET_ENTITY_FACING(hDebugPedB, vDriveway)
					ENDIF
				ENDIF
				
				// Update flags
//				INT iFlags = 0
//				IF bAllowMichael[eFriendLoc]		iFlags = iFlags | FLF_Michael			ENDIF
//				IF bAllowFranklin[eFriendLoc]		iFlags = iFlags | FLF_Franklin			ENDIF
//				IF bAllowTrevor[eFriendLoc]			iFlags = iFlags | FLF_Trevor			ENDIF	
//				IF bAllowLamar[eFriendLoc]			iFlags = iFlags | FLF_Lamar				ENDIF	
//				IF bAllowJimmy[eFriendLoc]			iFlags = iFlags | FLF_Jimmy				ENDIF	
//				IF bAllowAmanda[eFriendLoc]			iFlags = iFlags | FLF_Amanda			ENDIF
//				
//				IF bAllowPickup[eFriendLoc]			iFlags = iFlags | FLF_Pickup			ENDIF
//				IF bAllowDropoff[eFriendLoc]		iFlags = iFlags | FLF_Dropoff			ENDIF	
//				IF bIsHomeLoc[eFriendLoc]			iFlags = iFlags | FLF_IsHomeLoc			ENDIF
//				
//				g_FriendLocations[eFriendLoc].iFlags = iFlags

				// Draw loc position + ped offset
				DRAW_DEBUG_SPHERE(vDriveway, 0.1, 255,255,255, 128)
				DRAW_DEBUG_LINE(vDriveway, vDoorstepA, 255, 255, 255, 255)
				DRAW_DEBUG_LINE(vDoorstepA-<<0,0,10>>, vDoorstepA+<<0,0,10>>, 255, 255, 255, 255)
				
				// Draw spawn and park positions
				DRAW_DEBUG_SPHERE(g_FriendLocations[eFriendLoc].vSpawnPos, 0.1, 0,255,0, 128)
				DRAW_DEBUG_SPHERE(g_FriendLocations[eFriendLoc].vParkPos, 0.1, 0,255,255, 128)
				
				// Draw ped offset B
				VECTOR vParkPos = g_FriendLocations[eFriendLoc].vParkPos
				DRAW_DEBUG_LINE(vDriveway, vParkPos, 200, 200, 200, 255)
				DRAW_DEBUG_LINE(vParkPos-<<0,0,10>>, vParkPos+<<0,0,10>>, 200, 200, 200, 255)
				DRAW_DEBUG_LINE(vParkPos, vDoorstepB, 150, 150, 150, 255)
				DRAW_DEBUG_LINE(vDoorstepB-<<0,0,10>>, vDoorstepB+<<0,0,10>>, 150, 150, 150, 255)

			ENDIF		
		ENDREPEAT
		
		WAIT(0)
	ENDWHILE
	
	// Delete peds/blips at each FriendLocation
	REPEAT MAX_FRIEND_LOCATIONS eFriendLoc
		IF eFriendLoc <> FLOC_adhoc
			IF DOES_ENTITY_EXIST(hDebugPedA)
				DELETE_PED(hDebugPedA)
			ENDIF
			IF DOES_ENTITY_EXIST(hDebugPedB)
				DELETE_PED(hDebugPedB)
			ENDIF
			IF DOES_BLIP_EXIST(hDebugBlip[eFriendLoc])
				REMOVE_BLIP(hDebugBlip[eFriendLoc])
			ENDIF
		ENDIF
	ENDREPEAT

	REMOVE_ANIM_DICT("friends@laf@ig_1@idle_a")
	REMOVE_ANIM_DICT("friends@")

	//-- Save output to file
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_DisplayFriendLocations() // Save to file")

	OPEN_DEBUG_FILE()
		
		SAVE_NEWLINE_TO_DEBUG_FILE()	
		SAVE_NEWLINE_TO_DEBUG_FILE()	
		SAVE_STRING_TO_DEBUG_FILE("//----------------------- PRIVATE_InitialiseFriendLocations() // Save to file...")
		SAVE_NEWLINE_TO_DEBUG_FILE()	

		
		REPEAT MAX_FRIEND_LOCATIONS eFriendLoc
			IF eFriendLoc <> FLOC_adhoc
				// Save comment heading
								
				#if USE_CLF_DLC
					IF g_FriendLocations[eFriendLoc].hSavehouse <> NUMBER_OF_CLF_SAVEHOUSE
						SAVE_STRING_TO_DEBUG_FILE("	// ")
						SAVE_STRING_TO_DEBUG_FILE(Get_Savehouse_Respawn_Name(g_FriendLocations[eFriendLoc].hSavehouse))
						SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				#endif
				#if USE_NRM_DLC
					IF g_FriendLocations[eFriendLoc].hSavehouse <> NUMBER_OF_NRM_SAVEHOUSE
						SAVE_STRING_TO_DEBUG_FILE("	// ")
						SAVE_STRING_TO_DEBUG_FILE(Get_Savehouse_Respawn_Name(g_FriendLocations[eFriendLoc].hSavehouse))
						SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					IF g_FriendLocations[eFriendLoc].hSavehouse <> NUMBER_OF_SAVEHOUSE_LOCATIONS
						SAVE_STRING_TO_DEBUG_FILE("	// ")
						SAVE_STRING_TO_DEBUG_FILE(Get_Savehouse_Respawn_Name(g_FriendLocations[eFriendLoc].hSavehouse))
						SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				#endif
				#endif
				// Save main entry
				SAVE_STRING_TO_DEBUG_FILE("	PRIVATE_SetupFriendLoc(")
					SAVE_STRING_TO_DEBUG_FILE(GetLabel_enumFriendLocation(eFriendLoc))
					SAVE_STRING_TO_DEBUG_FILE(", ")
					SAVE_VECTOR_TO_DEBUG_FILE(g_FriendLocations[eFriendLoc].vPickupCoord)
					SAVE_STRING_TO_DEBUG_FILE(", ")
					SAVE_VECTOR_TO_DEBUG_FILE(g_FriendLocations[eFriendLoc].vPedOffsetA)
					SAVE_STRING_TO_DEBUG_FILE(", ")
					SAVE_VECTOR_TO_DEBUG_FILE(g_FriendLocations[eFriendLoc].vPedOffsetB)
					#if USE_CLF_DLC
						IF g_FriendLocations[eFriendLoc].hSavehouse <> NUMBER_OF_CLF_SAVEHOUSE
							SAVE_STRING_TO_DEBUG_FILE(", ")
							SAVE_STRING_TO_DEBUG_FILE(GetLabel_SAVEHOUSE_NAME_ENUM(g_FriendLocations[eFriendLoc].hSavehouse))
						ENDIF
					#endif
					#if USE_NRM_DLC
						IF g_FriendLocations[eFriendLoc].hSavehouse <> NUMBER_OF_NRM_SAVEHOUSE
							SAVE_STRING_TO_DEBUG_FILE(", ")
							SAVE_STRING_TO_DEBUG_FILE(GetLabel_SAVEHOUSE_NAME_ENUM(g_FriendLocations[eFriendLoc].hSavehouse))
						ENDIF
					#endif
					#if not USE_CLF_DLC
					#if not USE_NRM_DLC
						IF g_FriendLocations[eFriendLoc].hSavehouse <> NUMBER_OF_SAVEHOUSE_LOCATIONS
							SAVE_STRING_TO_DEBUG_FILE(", ")
							SAVE_STRING_TO_DEBUG_FILE(GetLabel_SAVEHOUSE_NAME_ENUM(g_FriendLocations[eFriendLoc].hSavehouse))
						ENDIF
					#endif
					#endif
					
					SAVE_STRING_TO_DEBUG_FILE(")")
				SAVE_NEWLINE_TO_DEBUG_FILE()
			
				// Save spawn points
				SAVE_STRING_TO_DEBUG_FILE("		PRIVATE_SetupFriendLoc_SpawnPoint(")
					SAVE_VECTOR_TO_DEBUG_FILE(g_FriendLocations[eFriendLoc].vSpawnPos)
					SAVE_STRING_TO_DEBUG_FILE(", ")
					SAVE_FLOAT_TO_DEBUG_FILE(g_FriendLocations[eFriendLoc].fSpawnRot)
					SAVE_STRING_TO_DEBUG_FILE(", ")
					SAVE_VECTOR_TO_DEBUG_FILE(g_FriendLocations[eFriendLoc].vParkPos)
					SAVE_STRING_TO_DEBUG_FILE(")")
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				// Save char flags
				BOOL bPunctuate = FALSE
				SAVE_STRING_TO_DEBUG_FILE("		PRIVATE_SetupFriendLoc_CharFlags(")
					IF FriendLoc_AreFlagsSet(eFriendLoc, FLF_AllChars)
						SAVE_STRING_TO_DEBUG_FILE("FLF_AllChars")
					ELSE
						IF FriendLoc_AreFlagsSet(eFriendLoc, FLF_Michael)
							SAVE_STRING_TO_DEBUG_FILE("FLF_Michael")
							bPunctuate = TRUE
						ENDIF
						IF FriendLoc_AreFlagsSet(eFriendLoc, FLF_Franklin)
							IF bPunctuate SAVE_STRING_TO_DEBUG_FILE("|") ENDIF
							SAVE_STRING_TO_DEBUG_FILE("FLF_Franklin")
							bPunctuate = TRUE
						ENDIF
						IF FriendLoc_AreFlagsSet(eFriendLoc, FLF_Trevor)
							IF bPunctuate SAVE_STRING_TO_DEBUG_FILE("|") ENDIF
							SAVE_STRING_TO_DEBUG_FILE("FLF_Trevor")
							bPunctuate = TRUE
						ENDIF
						IF FriendLoc_AreFlagsSet(eFriendLoc, FLF_Lamar)
							IF bPunctuate SAVE_STRING_TO_DEBUG_FILE("|") ENDIF
							SAVE_STRING_TO_DEBUG_FILE("FLF_Lamar")
							bPunctuate = TRUE
						ENDIF
						IF FriendLoc_AreFlagsSet(eFriendLoc, FLF_Jimmy)
							IF bPunctuate SAVE_STRING_TO_DEBUG_FILE("|") ENDIF
							SAVE_STRING_TO_DEBUG_FILE("FLF_Jimmy")
							bPunctuate = TRUE
						ENDIF
						IF FriendLoc_AreFlagsSet(eFriendLoc, FLF_Amanda)
							IF bPunctuate SAVE_STRING_TO_DEBUG_FILE("|") ENDIF
							SAVE_STRING_TO_DEBUG_FILE("FLF_Amanda")
							bPunctuate = TRUE
						ENDIF
					ENDIF
					SAVE_STRING_TO_DEBUG_FILE(")")
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				// Save usage flags
				bPunctuate = FALSE
				SAVE_STRING_TO_DEBUG_FILE("		PRIVATE_SetupFriendLoc_UsageFlags(")
					IF FriendLoc_AreFlagsSet(eFriendLoc, FLF_PickupDropoff)
						SAVE_STRING_TO_DEBUG_FILE("FLF_PickupDropoff")
						bPunctuate = TRUE
					ELIF FriendLoc_AreFlagsSet(eFriendLoc, FLF_Pickup)
						SAVE_STRING_TO_DEBUG_FILE("FLF_Pickup")
						bPunctuate = TRUE
					ELIF FriendLoc_AreFlagsSet(eFriendLoc, FLF_Dropoff)
						SAVE_STRING_TO_DEBUG_FILE("FLF_Dropoff")
						bPunctuate = TRUE
					ENDIF
					IF FriendLoc_AreFlagsSet(eFriendLoc, FLF_IsHomeLoc)
						IF bPunctuate SAVE_STRING_TO_DEBUG_FILE("|") ENDIF
						SAVE_STRING_TO_DEBUG_FILE("FLF_IsHomeLoc")
						bPunctuate = TRUE
					ENDIF
					SAVE_STRING_TO_DEBUG_FILE(")")
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
		ENDREPEAT
		
		// Save pickup to file
		SAVE_STRING_TO_DEBUG_FILE("//----------------------- PRIVATE_InitialiseFriendLocations() // END")
		SAVE_NEWLINE_TO_DEBUG_FILE()	
		SAVE_NEWLINE_TO_DEBUG_FILE()	
		SAVE_NEWLINE_TO_DEBUG_FILE()	
	
	CLOSE_DEBUG_FILE()

	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_DisplayFriendLocations() // End")

	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	REPEAT MAX_FRIEND_LOCATIONS eFriendLoc
		IF eFriendLoc <> FLOC_adhoc
			DELETE_WIDGET_GROUP(hWidget[eFriendLoc])
		ENDIF
	ENDREPEAT
ENDPROC

//---------------------------------------------------------------------------------------------------
//-- ACTIVITY LOCATIONS (debug)
//---------------------------------------------------------------------------------------------------

PROC PRIVATE_DEBUG_DisplayActivityLocations(WIDGET_GROUP_ID& hParentWidgetGroup, BOOL& bDisplayActivityLocations)

	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_DisplayActivityLocations()")

	// Rag accessors
	BOOL			bJumpToPickupCoords[MAX_ACTIVITY_LOCATIONS]
	BOOL			bRespotVehicle

	// Vars for temp peds/blips at locations
	WIDGET_GROUP_ID	hWidget
	BLIP_INDEX		hDebugBlip[MAX_ACTIVITY_LOCATIONS]

	enumActivityLocation eActivityLoc
	enumActivityLocation eDrawLoc = NO_ACTIVITY_LOCATION
	

	// Create widgets for each ActivityLocation
	CDEBUG2LN(DEBUG_FRIENDS, "PRIVATE_DEBUG_DisplayActivityLocations() // Create widgets for each ActivityLocation")

	SET_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
		hWidget = 
		START_WIDGET_GROUP("Locations")
			REPEAT MAX_ACTIVITY_LOCATIONS eActivityLoc
				TEXT_LABEL_63 tJumpLabel
				tJumpLabel = "Warp to "
				tJumpLabel += GetLabel_enumActivityLocation(eActivityLoc)
				ADD_WIDGET_BOOL(tJumpLabel, bJumpToPickupCoords[eActivityLoc])
			ENDREPEAT
			
			START_WIDGET_GROUP("Controls")
				ADD_WIDGET_BOOL("Respot vehicle", bRespotVehicle)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
	

	// Create blip at each ActivityLocation
	CDEBUG2LN(DEBUG_FRIENDS, "PRIVATE_DEBUG_EditActivityLocations() // Create peds/blips at each ActivityLocation")

	REPEAT MAX_ACTIVITY_LOCATIONS eActivityLoc
		hDebugBlip[eActivityLoc] = ADD_BLIP_FOR_COORD(GET_STATIC_BLIP_POSITION(g_ActivityLocations[eActivityLoc].sprite))
		SET_BLIP_NAME_FROM_TEXT_FILE(hDebugBlip[eActivityLoc], GetLabel_enumActivityLocation(eActivityLoc))
	ENDREPEAT
	

	// Maintain widgets/ActivityLocations
	CDEBUG2LN(DEBUG_FRIENDS, "PRIVATE_DEBUG_EditActivityLocations() // Maintain widgets/ActivityLocations")
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)

	structFRespotData respot
	
	WHILE bDisplayActivityLocations
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)

		REPEAT MAX_ACTIVITY_LOCATIONS eActivityLoc
			VECTOR vAnchor	= GET_STATIC_BLIP_POSITION(g_ActivityLocations[eActivityLoc].sprite)

			// Warp player if requested
			IF bJumpToPickupCoords[eActivityLoc]
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vAnchor)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				ENDIF
				bJumpToPickupCoords[eActivityLoc] = FALSE
				eDrawLoc = eActivityLoc
			ENDIF
			
//			// Update blip position
//			IF DOES_BLIP_EXIST(hDebugBlip[eActivityLoc])
//				SET_BLIP_COORDS(hDebugBlip[eActivityLoc], vAnchor)
//			ENDIF
		ENDREPEAT
		
		IF eDrawLoc < MAX_ACTIVITY_LOCATIONS
			
			VECTOR vAnchor	= GET_STATIC_BLIP_POSITION(g_ActivityLocations[eDrawLoc].sprite)
			
			// Draw loc position
			DRAW_DEBUG_SPHERE(vAnchor, 0.1,								255, 255, 255, 255)
			DRAW_DEBUG_LINE(vAnchor-<<0,0,10>>, vAnchor+<<0,0,10>>,		255, 255, 255, 255)
			
			// Draw respot position
			IF Private_ALOC_GetRespotData(eDrawLoc, respot)
				VECTOR vPos = respot.vCarPos
				VECTOR vLook = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(respot.vCarPos, respot.fCarRot, <<0,1,0>>)
				
				DRAW_DEBUG_SPHERE(vPos, 0.1,							255,   0,  75, 255)
				DRAW_DEBUG_LINE(vPos-<<0,0,10>>, vPos+<<0,0,10>>,		255,   0,  75, 255)
				DRAW_DEBUG_LINE(vPos+<<0,0,0.5>>, vLook+<<0,0,0.5>>,	255,   0,  75, 255)
			ENDIF
			
			IF bRespotVehicle
				VEHICLE_INDEX hVehicle = GET_PLAYERS_LAST_VEHICLE()
				IF DOES_ENTITY_EXIST(hVehicle) AND NOT IS_ENTITY_DEAD(hVehicle)
					Private_RespotVehicleForActivity(hVehicle, eDrawLoc)
				ENDIF
				bRespotVehicle = FALSE
			ENDIF

		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	// Delete blips at each ActivityLocation
	REPEAT MAX_ACTIVITY_LOCATIONS eActivityLoc
		IF DOES_BLIP_EXIST(hDebugBlip[eActivityLoc])
			REMOVE_BLIP(hDebugBlip[eActivityLoc])
		ENDIF
	ENDREPEAT

//	//-- Save output to file
//	CDEBUG2LN(DEBUG_FRIENDS, "PRIVATE_DEBUG_DisplayActivityLocations() // Save to file")
//
//	OPEN_DEBUG_FILE()
//		
//		SAVE_NEWLINE_TO_DEBUG_FILE()	
//		SAVE_NEWLINE_TO_DEBUG_FILE()	
//		SAVE_STRING_TO_DEBUG_FILE("//----------------------- PRIVATE_InitialiseActivityLocations() // Save to file...")
//		SAVE_NEWLINE_TO_DEBUG_FILE()	
//		
//		REPEAT MAX_ACTIVITY_LOCATIONS eActivityLoc
//			// Save main entry
//			SAVE_STRING_TO_DEBUG_FILE("	PRIVATE_SetupActivityLoc(")
//				SAVE_STRING_TO_DEBUG_FILE(GetLabel_enumActivityLocation(eActivityLoc))
//				SAVE_STRING_TO_DEBUG_FILE(", ")
//				SAVE_STRING_TO_DEBUG_FILE(GetLabel_enumActivityType(g_ActivityLocations[eActivityLoc].type))
//				SAVE_STRING_TO_DEBUG_FILE(", ")
//				SAVE_STRING_TO_DEBUG_FILE(DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(g_ActivityLocations[eActivityLoc].sprite)))
//				SAVE_STRING_TO_DEBUG_FILE(")")
//			SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_NEWLINE_TO_DEBUG_FILE()
//		ENDREPEAT
//		
//		// Save pickup to file
//		SAVE_STRING_TO_DEBUG_FILE("//----------------------- PRIVATE_InitialiseActivityLocations() // END")
//		SAVE_NEWLINE_TO_DEBUG_FILE()	
//		SAVE_NEWLINE_TO_DEBUG_FILE()	
//		SAVE_NEWLINE_TO_DEBUG_FILE()	
//	
//	CLOSE_DEBUG_FILE()

	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	DELETE_WIDGET_GROUP(hWidget)
ENDPROC



//---------------------------------------------------------------------------------------------------
//-- DROPOFF SCENE (debug)
//---------------------------------------------------------------------------------------------------

FUNC BOOL PRIVATE_DEBUG_CreateFriendPed(BOOL bUsePlayableChar = TRUE)
	
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_CreateFriendPed() - Adding friend ped...")
	
	// Check group size
	INT iLeaderCount, iFollowerCount
	GET_GROUP_SIZE(PLAYER_GROUP_ID(), iLeaderCount, iFollowerCount)
	
	IF iFollowerCount >= 2
		SCRIPT_ASSERT("PRIVATE_DEBUG_CreateFriendPed() - Group too big to add more friends")
		RETURN FALSE
	ENDIF

	PED_INDEX hFollower = NULL
	IF iFollowerCount > 0
		hFollower = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 0)
	ENDIF

	// Select unused char
	enumCharacterList eNewChar = NO_CHARACTER

	IF bUsePlayableChar
		IF   GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
		AND (IS_PED_INJURED(hFollower) OR Private_GetCharFromPed(hFollower) <> CHAR_MICHAEL)
			eNewChar = CHAR_MICHAEL
				
		ELIF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
		AND (IS_PED_INJURED(hFollower) OR Private_GetCharFromPed(hFollower) <> CHAR_FRANKLIN)
			eNewChar = CHAR_FRANKLIN
				
		ELIF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_TREVOR
		AND (IS_PED_INJURED(hFollower) OR Private_GetCharFromPed(hFollower) <> CHAR_TREVOR)
			eNewChar = CHAR_TREVOR
		
		ELSE
			SCRIPT_ASSERT("Couldn't find valid unused player character to create ped")
			RETURN FALSE
		ENDIF
	ELSE
		INT iNpcStart = ENUM_TO_INT(MAX_FRIENDS) - (1 + NUM_OF_NPC_FRIENDS)
		INT iNpcIndex = GET_RANDOM_INT_IN_RANGE(0, NUM_OF_NPC_FRIENDS)
		INT iCount = 0
		
		WHILE eNewChar = NO_CHARACTER
			enumCharacterList ePotentialNpcChar = GET_CHAR_FROM_FRIEND(INT_TO_ENUM(enumFriend, iNpcStart+iNpcIndex))

			IF (IS_PED_INJURED(hFollower) OR Private_GetCharFromPed(hFollower) <> ePotentialNpcChar)
				eNewChar = ePotentialNpcChar
			ELSE
				iNpcIndex = (iNpcIndex + 1) % ENUM_TO_INT(MAX_FRIENDS)
				iCount++
				IF iCount > NUM_OF_NPC_FRIENDS
					SCRIPT_ASSERT("Couldn't find valid unused npc character to create ped")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDWHILE
	ENDIF
	
	// Load model
	MODEL_NAMES model
	IF eNewChar = CHAR_MICHAEL
		model = GET_FRIEND_MODEL(FR_MICHAEL)
	ELIF eNewChar = CHAR_FRANKLIN
		model = GET_FRIEND_MODEL(FR_FRANKLIN)
	ELIF eNewChar = CHAR_TREVOR
		model = GET_FRIEND_MODEL(FR_TREVOR)
	ELIF eNewChar = CHAR_LAMAR
		model = GET_FRIEND_MODEL(FR_LAMAR)
	ELIF eNewChar = CHAR_JIMMY
		model = GET_FRIEND_MODEL(FR_JIMMY)
	ELIF eNewChar = CHAR_AMANDA
		model = GET_FRIEND_MODEL(FR_AMANDA)
	ENDIF
	
	REQUEST_MODEL(model)
	WHILE NOT HAS_MODEL_LOADED(model)
		REQUEST_MODEL(model)
		SET_TEXT_COLOUR(100, 255, 255, 255)
		SET_TEXT_SCALE(0.5, 0.6)
		SET_TEXT_WRAP(0.0, 1.0)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.78, 0.85, "STRING", "Creating friend ped...")
		WAIT(0)
	ENDWHILE
		
	// Create friend ped
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())

		VECTOR vSpawnPos
		IF iFollowerCount = 0
			vSpawnPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<-0.5, 2.0, 1.0>>)
		ELSE
			vSpawnPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.5, 2.0, 1.0>>)
		ENDIF
		GET_GROUND_Z_FOR_3D_COORD(vSpawnPos, vSpawnPos.z)
		PED_INDEX hFriendPed = CREATE_PED(PEDTYPE_MISSION, model, vSpawnPos, GET_ENTITY_HEADING(PLAYER_PED_ID()))
		
		IF NOT IS_PED_INJURED(hFriendPed)
			// TODO: Set friend ped attributes?
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX hPlayerVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF IS_ENTITY_ALIVE(hPlayerVehicle)
					SET_PED_INTO_VEHICLE(hFriendPed, hPlayerVehicle, VS_ANY_PASSENGER)
				ENDIF
			ENDIF
			
			SET_PED_AS_GROUP_MEMBER(hFriendPed, PLAYER_GROUP_ID())
			SET_MODEL_AS_NO_LONGER_NEEDED(model)
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PRIVATE_DEBUG_RemoveAllFriendPeds()

	INT iLeaderCount, iFollowerCount
	GET_GROUP_SIZE(PLAYER_GROUP_ID(), iLeaderCount, iFollowerCount)

	INT iIndex = 0
	WHILE iIndex < iFollowerCount
		PED_INDEX hGroupPed = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), iIndex)
		IF NOT IS_PED_INJURED(hGroupPed) AND NOT IS_PED_GROUP_LEADER(hGroupPed, PLAYER_GROUP_ID())
			REMOVE_PED_FROM_GROUP(hGroupPed)
			DELETE_PED(hGroupPed)
		ENDIF
		iIndex++
	ENDWHILE
	
ENDPROC

PROC PRIVATE_DEBUG_RunDropoffScene(structFDropoffScene& dropoff, structPedsForConversation& convPeds, INT iMinFriends = 1, VEHICLE_INDEX hFriendCarA = NULL, VEHICLE_INDEX hFriendCarB = NULL)
	
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_RunDropoffScene()...")
	
	PED_INDEX hFriendA, hFriendB
	
	// Grab friend peds to use
	INT iLeaderCount, iFollowerCount
	GET_GROUP_SIZE(PLAYER_GROUP_ID(), iLeaderCount, iFollowerCount)
	
	WHILE iFollowerCount < iMinFriends
		IF PRIVATE_DEBUG_CreateFriendPed()
			iFollowerCount++
		ELSE
			SCRIPT_ASSERT("PRIVATE_DEBUG_RunDropoffScene() - Couldn't create friend ped")
			EXIT
		ENDIF
	ENDWHILE
	hFriendA = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 0)
	hFriendB = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 1)
	
	// Reset gameplay cam
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	VEHICLE_INDEX hVehicle = NULL
	structFDropoff dropoffData

	// Run the scene
	INT iCutsceneStep = 0
	INT iDrunkTimer = 0
	
	WHILE NOT Dropoff_ProcessScene(dropoff, dropoffData, convPeds, iCutsceneStep, iDrunkTimer, hFriendA, hFriendB, hVehicle, hFriendCarA, hFriendCarB)
		WAIT(0)
	ENDWHILE
	
//	IF DOES_ENTITY_EXIST(hFriendA)
//		DELETE_PED(hFriendA)
//	ENDIF
//	IF DOES_ENTITY_EXIST(hFriendB)
//		DELETE_PED(hFriendB)
//	ENDIF
	
ENDPROC

/*
PROC PRIVATE_DEBUG_EditDropoffScene(WIDGET_GROUP_ID& hParentWidgetGroup, structPedsForConversation& convPeds, BOOL& bEnable, structFDropoffScene& dropoff, VECTOR vAnchor)

	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_EditDropoffScene()...")

	// Rag accessors
	BOOL	bDisplayPositions		= TRUE
	BOOL	bForceRunScene			= FALSE
	BOOL	bToggleFriendPeds		= FALSE
	
	BOOL	bGrabCarCamPan0			= FALSE
	BOOL	bGrabCarCamPan1			= FALSE

	BOOL	bGrabFootCamPan0		= FALSE
	BOOL	bGrabFootCamPan1		= FALSE

	VECTOR	vCarOffset				= dropoff.vCarPos - vAnchor

	BOOL	bFixPosHeights			= FALSE
	
	BOOL	bGrabPlayerPos			= FALSE
	BOOL	bGrabPed0Pos			= FALSE
	BOOL	bGrabPed1Pos			= FALSE

	BOOL	bGrabExitPos			= FALSE
	BOOL	bGrabSwitch0Pos			= FALSE
	BOOL	bGrabSwitch1Pos			= FALSE
	

	//-- Create widgets --
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_EditDropoffScene() // Create widgets")
	
	WIDGET_GROUP_ID hDropoffGroup

	SET_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
		hDropoffGroup =
		START_WIDGET_GROUP("Scene data")
			// Add widgets
			ADD_WIDGET_BOOL("Display positions",				bDisplayPositions)
			ADD_WIDGET_BOOL("Toggle friend peds",				bToggleFriendPeds)
			ADD_WIDGET_BOOL("Force run scene",					bForceRunScene)
			ADD_WIDGET_BOOL("Fix ped pos heights",				bFixPosHeights)

			ADD_WIDGET_STRING(".")
			ADD_WIDGET_BOOL("Grab switch0 pos from player",		bGrabSwitch0Pos)
			ADD_WIDGET_BOOL("Grab switch1 pos from player",		bGrabSwitch1Pos)

			ADD_WIDGET_STRING(".")
			ADD_WIDGET_BOOL("Grab car cam pan start",			bGrabCarCamPan0)
			ADD_WIDGET_BOOL("Grab car cam pan end",				bGrabCarCamPan1)

			ADD_WIDGET_STRING(".")
			ADD_WIDGET_BOOL("Grab on-foot cam pan start",		bGrabFootCamPan0)
			ADD_WIDGET_BOOL("Grab on-foot cam pan end",			bGrabFootCamPan1)

			ADD_WIDGET_STRING(".")
			ADD_WIDGET_VECTOR_SLIDER("Car offset",				vCarOffset, 					-30.0, 30.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Car heading",				dropoff.fCarRot,				0.0, 360.0, 0.1)
			
			ADD_WIDGET_STRING(".")
			ADD_WIDGET_BOOL("Grab player pos with mouse",		bGrabPlayerPos)
			ADD_WIDGET_BOOL("Grab friendA pos with mouse",		bGrabPed0Pos)
			ADD_WIDGET_BOOL("Grab friendB pos with mouse",		bGrabPed1Pos)
			ADD_WIDGET_BOOL("Grab exit pos with mouse",			bGrabExitPos)
		STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
	
	
	//-- Setup world --
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_EditDropoffScene() // Setup world")
	
	// Create pickup loc blip
	BLIP_INDEX hLocBlip = ADD_BLIP_FOR_COORD(vAnchor)
	SET_BLIP_SPRITE(hLocBlip, RADAR_TRACE_FRIEND)
	SET_BLIP_NAME_FROM_TEXT_FILE(hLocBlip, "FR_PKUPBLIP")
	
	// Warp player
	CLEAR_AREA(vAnchor, 25.0, TRUE)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vAnchor)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		//LOAD_SCENE(vPickupCoord)
	ENDIF

	// Load text
	REQUEST_ADDITIONAL_TEXT("FRIENDS", OBJECT_TEXT_SLOT)
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(OBJECT_TEXT_SLOT)
		WAIT(0)
	ENDWHILE
	

	//-- Monitor widgets --
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_EditDropoffScene() // Montior widgets")
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	

	WHILE bEnable
		// Update dropoff positions from offset widgets
		dropoff.vCarPos				= vAnchor + vCarOffset
		
		// Draw anchor pos
		IF bDisplayPositions
			DRAW_DEBUG_SPHERE(vAnchor, 0.1, 255,255,255, 128)
			
			VECTOR vPos
			FLOAT fRot
			INT iR
			INT iG
			INT iB
			
			// Draw player spot pos
			vPos = dropoff.vPlayerPos
			iR = 200
			iG = 0
			iB = 200
			DRAW_DEBUG_LINE(vAnchor, vPos,																					iR,iG,iB, 128)
			DRAW_DEBUG_LINE(vPos-<<0,0,10>>, vPos+<<0,0,10>>,																iR,iG,iB, 128)

			// Draw friend positions
			vPos = dropoff.vPedPos[0]
			iR = 200
			iG = 200
			iB = 200
			DRAW_DEBUG_LINE(vAnchor, vPos,																					iR,iG,iB, 128)
			DRAW_DEBUG_LINE(vPos-<<0,0,10>>, vPos+<<0,0,10>>,																iR,iG,iB, 128)

			vPos = dropoff.vPedPos[1]
			iR = 200
			iG = 200
			iB = 200
			DRAW_DEBUG_LINE(vAnchor, vPos,																					iR,iG,iB, 128)
			DRAW_DEBUG_LINE(vPos-<<0,0,10>>, vPos+<<0,0,10>>,																iR,iG,iB, 128)

			// Draw car spot pos
			vPos = dropoff.vCarPos
			fRot = dropoff.fCarRot
			iR = 255
			iG = 0
			iB = 75
			DRAW_DEBUG_LINE(vAnchor, vPos,																					iR,iG,iB, 128)
			DRAW_DEBUG_LINE(vPos-<<0,0,10>>, vPos+<<0,0,10>>,																iR,iG,iB, 128)
			DRAW_DEBUG_LINE(vPos+<<0,0,0.1>>, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fRot, <<0,1.5,0.1>>),	iR,iG,iB, 128)

			// Draw exit pos
			vPos = dropoff.vExitPos
			iR = 0
			iG = 255
			iB = 175
			DRAW_DEBUG_LINE(vAnchor, vPos,																					iR,iG,iB, 128)
			DRAW_DEBUG_LINE(vPos-<<0,0,10>>, vPos+<<0,0,10>>,																iR,iG,iB, 128)
		ENDIF
	
		// Grab camera positions
		IF DOES_CAM_EXIST(GET_DEBUG_CAM())
			IF IS_CAM_RENDERING(GET_DEBUG_CAM())
				// Grab car debug cam
				IF bGrabCarCamPan0
					dropoff.mCarCamPan[0].vPos = GET_CAM_COORD(GET_DEBUG_CAM())
					dropoff.mCarCamPan[0].vRot = GET_CAM_ROT(GET_DEBUG_CAM())
					dropoff.fCarCamPanFov		= GET_CAM_FOV(GET_DEBUG_CAM())
					bGrabCarCamPan0 = FALSE
				ENDIF
				IF bGrabCarCamPan1
					dropoff.mCarCamPan[1].vPos = GET_CAM_COORD(GET_DEBUG_CAM())
					dropoff.mCarCamPan[1].vRot = GET_CAM_ROT(GET_DEBUG_CAM())
					dropoff.fCarCamPanFov		= GET_CAM_FOV(GET_DEBUG_CAM())
					bGrabCarCamPan1 = FALSE
				ENDIF

				// Grab foot debug cam
				IF bGrabFootCamPan0
					dropoff.mFootCamPan[0].vPos = GET_CAM_COORD(GET_DEBUG_CAM())
					dropoff.mFootCamPan[0].vRot = GET_CAM_ROT(GET_DEBUG_CAM())
					dropoff.fFootCamPanFov		= GET_CAM_FOV(GET_DEBUG_CAM())
					bGrabFootCamPan0 = FALSE
				ENDIF
				IF bGrabFootCamPan1
					dropoff.mFootCamPan[1].vPos = GET_CAM_COORD(GET_DEBUG_CAM())
					dropoff.mFootCamPan[1].vRot = GET_CAM_ROT(GET_DEBUG_CAM())
					dropoff.fFootCamPanFov		= GET_CAM_FOV(GET_DEBUG_CAM())
					bGrabFootCamPan1 = FALSE
				ENDIF

				// Print FOV to screen
				TEXT_LABEL tFOVDebug = "FOV: "
				tFOVDebug += GET_STRING_FROM_FLOAT(GET_CAM_FOV(GET_DEBUG_CAM()))

				SET_TEXT_COLOUR(100, 255, 255, 255)
				SET_TEXT_SCALE(0.7, 0.8)
				SET_TEXT_WRAP(0.0, 1.0)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.78, 0.85, "STRING", tFOVDebug)
			ENDIF
		ENDIF
		
		// Set ped standing positions to ground level
		IF bFixPosHeights
			dropoff.vPlayerPos.z = vAnchor.z + 1.0
			GET_GROUND_Z_FOR_3D_COORD(dropoff.vPlayerPos, dropoff.vPlayerPos.z)

			dropoff.vPedPos[0].z = vAnchor.z + 1.0
			GET_GROUND_Z_FOR_3D_COORD(dropoff.vPedPos[0], dropoff.vPedPos[0].z)

			dropoff.vPedPos[1].z = vAnchor.z + 1.0
			GET_GROUND_Z_FOR_3D_COORD(dropoff.vPedPos[1], dropoff.vPedPos[1].z)
			bFixPosHeights = FALSE
		ENDIF

		// Grab positions from mouse
		IF IS_MOUSE_BUTTON_JUST_PRESSED(MB_LEFT_BTN)
			IF bGrabPlayerPos
				dropoff.vPlayerPos = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
			ENDIF
			IF bGrabPed0Pos
				dropoff.vPedPos[0] = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
			ENDIF
			IF bGrabPed1Pos
				dropoff.vPedPos[1] = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
			ENDIF
			IF bGrabExitPos
				dropoff.vExitPos = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
			ENDIF
		ENDIF
		
//		// Look away from exit dest
//		IF bGrabExitPos
//			FLOAT fExitHeading = GET_FACING_HEADING(GET_ENTITY_COORDS(PLAYER_PED_ID()), dropoff.vExitPos)
//			FLOAT fRelativeHeading = fExitHeading - GET_ENTITY_HEADING(PLAYER_PED_ID())
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING(fRelativeHeading + 165)
//		ENDIF
		
		// Grab switch positions
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF bGrabSwitch0Pos
				dropoff.vSwitchPos[0] = GET_ENTITY_COORDS(PLAYER_PED_ID())
				dropoff.fSwitchRot[0] = GET_ENTITY_HEADING(PLAYER_PED_ID())
//				SET_ENTITY_COORDS(PLAYER_PED_ID(), dropoff.vSwitchPos[0])
//				SET_ENTITY_HEADING(PLAYER_PED_ID(), dropoff.fSwitchRot[0])
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				bGrabSwitch0Pos = FALSE
			ENDIF
			IF bGrabSwitch1Pos
				dropoff.vSwitchPos[1] = GET_ENTITY_COORDS(PLAYER_PED_ID())
				dropoff.fSwitchRot[1] = GET_ENTITY_HEADING(PLAYER_PED_ID())
//				SET_ENTITY_COORDS(PLAYER_PED_ID(), dropoff.vSwitchPos[1])
//				SET_ENTITY_HEADING(PLAYER_PED_ID(), dropoff.fSwitchRot[1])
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				bGrabSwitch1Pos = FALSE
			ENDIF
		ENDIF

		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
			INT iLeaderCount = 0, iFollowerCount = 0
			GET_GROUP_SIZE(PLAYER_GROUP_ID(), iLeaderCount, iFollowerCount)
			
			// Toggle friends
			IF bToggleFriendPeds
				IF iFollowerCount < 2
					PRIVATE_DEBUG_CreateFriendPed()
				ELSE
					PRIVATE_DEBUG_RemoveAllFriendPeds()
				ENDIF
				bToggleFriendPeds = FALSE
			ENDIF

			// Run scene if triggered
			IF CAN_PLAYER_START_CUTSCENE()
				IF bForceRunScene
					PRIVATE_DEBUG_RunDropoffScene(dropoff, convPeds, 2)
					bForceRunScene = FALSE
					
				ELIF iFollowerCount > 0
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vAnchor+<<0,0,g_vAnyMeansLocate.z>>, g_vAnyMeansLocate, TRUE, TRUE, TM_IN_VEHICLE)
					OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vAnchor+<<0,0,g_vOnFootLocate.z>>, g_vOnFootLocate, TRUE, TRUE, TM_ON_FOOT)
						PRIVATE_DEBUG_RunDropoffScene(dropoff, convPeds, 2)
						bForceRunScene = FALSE
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
			
		WAIT(0)
	ENDWHILE
	
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_EditDropoffScene() // Cleanup")

	IF DOES_BLIP_EXIST(hLocBlip)
		REMOVE_BLIP(hLocBlip)
	ENDIF

	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)

	DELETE_WIDGET_GROUP(hDropoffGroup)
ENDPROC
*/
PROC PRIVATE_DEBUG_EditDropoffScene_Luke(WIDGET_GROUP_ID& hParentWidgetGroup, structPedsForConversation& convPeds, BOOL& bEnable, structFDropoffScene& dropoff, VECTOR vAnchor)

	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_EditDropoffScene_Luke()...")

	// Rag accessors
	BOOL		bRunScene2				= FALSE
	BOOL		bRunScene1				= FALSE
	
	BOOL		bGrabFootPanStart[6]
	BOOL		bGrabFootPanEnd[6]
	BOOL		bGrabFootCatchupCam
	BOOL		bGrabCarCatchupCam
	
	BOOL		bShowFootPanStart5
	
	BOOL		bWarpToAnchor
	BOOL		bWarpToSwitch[3]
	
	CONST_INT	c_iMaxGrabPositions		11
	INT			iGrabPosState			= -1
	BOOL		bGrabPosition[c_iMaxGrabPositions]

	BOOL		bRespotExpansion[4]
	
	BOOL			bGrabFriendCarA
	BOOL			bGrabFriendCarB
	VEHICLE_INDEX	hFriendCarA
	VEHICLE_INDEX	hFriendCarB
	BLIP_INDEX		hFriendCarABlip
	BLIP_INDEX		hFriendCarBBlip
	
	BOOL			bForceDebugDraw
	

	//-- Create widgets --
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_EditDropoffScene_Luke() // Create widgets")
	
	WIDGET_GROUP_ID hDropoffGroup

	SET_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
		hDropoffGroup =
		START_WIDGET_GROUP("Scene data")
			// Add widgets
			ADD_WIDGET_BOOL("Run scene with 2 friends",						bRunScene2)
			ADD_WIDGET_BOOL("Run scene with 1 friend",						bRunScene1)
			ADD_WIDGET_STRING(".")
			ADD_WIDGET_BOOL("Debug - set current car owned by friend A",	bGrabFriendCarA)
			ADD_WIDGET_BOOL("Debug - set current car owned by friend B",	bGrabFriendCarB)
			ADD_WIDGET_BOOL("Debug - force debug draw",						bForceDebugDraw)

			// On-foot cams
			START_WIDGET_GROUP("Cameras")
				ADD_WIDGET_BOOL("Friend shots are alternate versions",		dropoff.bFootCam2IsAlternative)
				ADD_WIDGET_BOOL("Taxi style scene",							dropoff.bIsTaxiStyle)
				ADD_WIDGET_FLOAT_SLIDER("Initial speech delay",				dropoff.fFootSpeechDelay, 0.0, 15.0, 0.1)
				ADD_WIDGET_STRING(".")

				START_WIDGET_GROUP("Establishing shot")
					ADD_WIDGET_BOOL("Establishing shot - enable",				dropoff.mFootPans[0].bEnabled)
					ADD_WIDGET_BOOL("Establishing shot - grab start camera",	bGrabFootPanStart[0])
					ADD_WIDGET_BOOL("Establishing shot - grab end camera",		bGrabFootPanEnd[0])
					ADD_WIDGET_FLOAT_SLIDER("Establishing shot - pan duration",	dropoff.mFootPans[0].fDuration, 0.0, 15.0, 0.1)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("Friend 1 shot")
					ADD_WIDGET_BOOL("Friend 1 shot - enable",					dropoff.mFootPans[1].bEnabled)
					ADD_WIDGET_BOOL("Friend 1 shot - grab start camera",		bGrabFootPanStart[1])
					ADD_WIDGET_BOOL("Friend 1 shot - grab end camera",			bGrabFootPanEnd[1])
					ADD_WIDGET_FLOAT_SLIDER("Friend 1 shot - pan duration",		dropoff.mFootPans[1].fDuration, 0.0, 15.0, 0.1)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("Friend 2 shot")
					ADD_WIDGET_BOOL("Friend 2 shot - enable",					dropoff.mFootPans[2].bEnabled)
					ADD_WIDGET_BOOL("Friend 2 shot - grab start camera",		bGrabFootPanStart[2])
					ADD_WIDGET_BOOL("Friend 2 shot - grab end camera",			bGrabFootPanEnd[2])
					ADD_WIDGET_FLOAT_SLIDER("Friend 2 shot - pan duration",		dropoff.mFootPans[2].fDuration, 0.0, 15.0, 0.1)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("Exit shot (normal)")
					ADD_WIDGET_BOOL("Exit shot - enable",						dropoff.mFootPans[3].bEnabled)
					ADD_WIDGET_BOOL("Exit shot - grab start camera",			bGrabFootPanStart[3])
					ADD_WIDGET_BOOL("Exit shot - grab end camera",				bGrabFootPanEnd[3])
					ADD_WIDGET_BOOL("Exit shot - grab catchup cam",				bGrabFootCatchupCam)
					ADD_WIDGET_FLOAT_SLIDER("Exit shot - pan duration",			dropoff.mFootPans[3].fDuration, 0.0, 15.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Exit shot - hold",					dropoff.fFootFinalDelay, 0.0, 15.0, 0.1)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("Exit shot (car 1)")
					ADD_WIDGET_BOOL("Exit shot - enable",						dropoff.mFootPans[4].bEnabled)
					ADD_WIDGET_BOOL("Exit shot - grab start camera",			bGrabFootPanStart[4])
					ADD_WIDGET_BOOL("Exit shot - grab end camera",				bGrabFootPanEnd[4])
					ADD_WIDGET_FLOAT_SLIDER("Exit shot - pan duration",			dropoff.mFootPans[4].fDuration, 0.0, 15.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Exit shot - hold",					dropoff.fCarFinalDelay0, 0.0, 15.0, 0.1)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("Exit shot (car 2)")
					ADD_WIDGET_BOOL("Exit shot - enable",						dropoff.mFootPans[5].bEnabled)
					ADD_WIDGET_BOOL("Exit shot - grab start camera",			bGrabFootPanStart[5])
					ADD_WIDGET_BOOL("Exit shot - grab end camera",				bGrabFootPanEnd[5])
					ADD_WIDGET_BOOL("Exit shot - grab catchup cam",				bGrabCarCatchupCam)
					ADD_WIDGET_FLOAT_SLIDER("Exit shot - pan duration",			dropoff.mFootPans[5].fDuration, 0.0, 15.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Exit shot - hold",					dropoff.fCarFinalDelay, 0.0, 15.0, 0.1)
					ADD_WIDGET_BOOL("Exit shot - show cam pan start",			bShowFootPanStart5)
				STOP_WIDGET_GROUP()

			STOP_WIDGET_GROUP()

			// Positions
			START_WIDGET_GROUP("Positions")
				ADD_WIDGET_BOOL("Set player position",					bGrabPosition[0])
				ADD_WIDGET_BOOL("Set friend A position",				bGrabPosition[1])
				ADD_WIDGET_BOOL("Set friend B position",				bGrabPosition[2])
				ADD_WIDGET_BOOL("Set exit position",					bGrabPosition[3])
				ADD_WIDGET_BOOL("Set car position",						bGrabPosition[4])
				ADD_WIDGET_FLOAT_SLIDER("Car heading",					dropoff.fCarRot, 0.0, 360.0, 5.0)

				ADD_WIDGET_BOOL("Set car fake walk position",			bGrabPosition[5])
				ADD_WIDGET_BOOL("Set car fake drive position",			bGrabPosition[6])
				START_WIDGET_GROUP("Extra respot")
					ADD_WIDGET_BOOL("Set position",							bGrabPosition[7])
					ADD_WIDGET_FLOAT_SLIDER("Car heading",					dropoff.mExtraRespot.fCarRot, 0.0, 360.0, 5.0)
					ADD_WIDGET_BOOL("Expand X left",						bRespotExpansion[0])
					ADD_WIDGET_BOOL("Expand X right",						bRespotExpansion[1])
					ADD_WIDGET_BOOL("Expand Y forward",						bRespotExpansion[2])
					ADD_WIDGET_BOOL("Expand Y back",						bRespotExpansion[3])
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			// Positions
			START_WIDGET_GROUP("Switch Positions")
				ADD_WIDGET_BOOL("Set friend A switch position",			bGrabPosition[8])
				ADD_WIDGET_BOOL("Set friend B switch position",			bGrabPosition[9])
				ADD_WIDGET_BOOL("Set car switch position",				bGrabPosition[10])
				ADD_WIDGET_FLOAT_SLIDER("friend A switch heading",		dropoff.fSwitchRot[0], 0.0, 360.0, 5.0)
				ADD_WIDGET_FLOAT_SLIDER("friend B switch heading",		dropoff.fSwitchRot[1], 0.0, 360.0, 5.0)
				ADD_WIDGET_FLOAT_SLIDER("car switch heading",			dropoff.fSwitchRot[2], 0.0, 360.0, 5.0)
				ADD_WIDGET_BOOL("Warp to anchor position",				bWarpToAnchor)
				ADD_WIDGET_BOOL("Warp to friend A switch position",		bWarpToSwitch[0])
				ADD_WIDGET_BOOL("Warp to friend B switch position",		bWarpToSwitch[1])
				ADD_WIDGET_BOOL("Warp to car switch position",			bWarpToSwitch[2])
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
	
	
	//-- Setup world --
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_EditDropoffScene_Luke() // Setup world")
	
	// Create pickup loc blip
	BLIP_INDEX hLocBlip = ADD_BLIP_FOR_COORD(vAnchor)
	SET_BLIP_SPRITE(hLocBlip, RADAR_TRACE_FRIEND)
	SET_BLIP_NAME_FROM_TEXT_FILE(hLocBlip, "FR_PKUPBLIP")
	
	// Warp player
	CLEAR_AREA(vAnchor, 25.0, TRUE)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vAnchor)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		//LOAD_SCENE(vPickupCoord)
	ENDIF

	// Load text
	REQUEST_ADDITIONAL_TEXT("FRIENDS", OBJECT_TEXT_SLOT)
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(OBJECT_TEXT_SLOT)
		WAIT(0)
	ENDWHILE

	// Setup expansion boxes
	INT iBitLoop
	REPEAT COUNT_OF(bRespotExpansion) iBitLoop
		bRespotExpansion[iBitLoop] = IS_BIT_SET(dropoff.mExtraRespot.expandDir, iBitLoop)
	ENDREPEAT

	
	//-- Monitor widgets --
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_EditDropoffScene_Luke() // Montior widgets")
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	

	WHILE bEnable

		VECTOR vPos
		FLOAT fRot
		INT iR
		INT iG
		INT iB
		
		// Draw anchor
		DRAW_DEBUG_SPHERE(vAnchor, 0.1, 255, 255, 255, 100)

		// Draw player spot pos
		vPos = dropoff.vPlayerPos
		iR = 200
		iG = 0
		iB = 200
		DRAW_DEBUG_LINE(vPos-<<0,0,10>>, vPos+<<0,0,10>>,																iR,iG,iB, 128)

		// Draw friend positions
		vPos = dropoff.vPedPos[0]
		iR = 0
		iG = 255
		iB = 175
		DRAW_DEBUG_LINE(dropoff.vPlayerPos, vPos,																		iR,iG,iB, 128)
		DRAW_DEBUG_LINE(vPos-<<0,0,10>>, vPos+<<0,0,10>>,																iR,iG,iB, 128)

		vPos = dropoff.vPedPos[1]
		iR = 0
		iG = 255
		iB = 175
		DRAW_DEBUG_LINE(dropoff.vPlayerPos, vPos,																		iR,iG,iB, 128)
		DRAW_DEBUG_LINE(vPos-<<0,0,10>>, vPos+<<0,0,10>>,																iR,iG,iB, 128)

		// Draw exit pos
		iR = 200
		iG = 200
		iB = 200
		vPos = dropoff.vExitPos
		DRAW_DEBUG_LINE(dropoff.vPedPos[0], vPos,																		iR,iG,iB, 128)
		DRAW_DEBUG_LINE(dropoff.vPedPos[1], vPos,																		iR,iG,iB, 128)
		DRAW_DEBUG_LINE(vPos-<<0,0,10>>, vPos+<<0,0,10>>,																iR,iG,iB, 128)
		
		// Draw car spot pos
		vPos = dropoff.vCarPos
		fRot = dropoff.fCarRot
		iR = 255
		iG = 0
		iB = 75
		DRAW_DEBUG_LINE(vPos-<<0,0,10>>, vPos+<<0,0,10>>,																iR,iG,iB, 128)
		DRAW_DEBUG_LINE(vPos+<<0,0,0.5>>, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fRot, <<0,1.5,0.5>>),	iR,iG,iB, 128)
	
		// Draw car fake walk pos
		iR = 255
		iG = 75
		iB = 0
		vPos = dropoff.vCarFakeWalkPos
		DRAW_DEBUG_LINE(vPos-<<0,0,10>>, vPos+<<0,0,10>>,																iR,iG,iB, 128)

		// Draw car fake drive pos
		iR = 255/2
		iG = 75/2
		iB = 0
		vPos = dropoff.vCarFakeDrivePos
		DRAW_DEBUG_LINE(vPos-<<0,0,10>>, vPos+<<0,0,10>>,																iR,iG,iB, 128)

		// Draw secondary car pos
		vPos = dropoff.mExtraRespot.vCarPos
		fRot = dropoff.mExtraRespot.fCarRot
		iR = 255
		iG = 255
		iB = 0
		DRAW_DEBUG_LINE(vPos-<<0,0,10>>, vPos+<<0,0,10>>,																iR,iG,iB, 128)
		DRAW_DEBUG_LINE(vPos+<<0,0,0.5>>, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fRot, <<0,1.5,0.5>>),	iR,iG,iB, 128)
	
		// Draw switch A pos
		vPos = dropoff.vSwitchPos[0]
		fRot = dropoff.fSwitchRot[0]
		iR = 175
		iG = 0
		iB = 255
		DRAW_DEBUG_LINE(vPos-<<0,0,10>>, vPos+<<0,0,10>>,																iR,iG,iB, 128)
		DRAW_DEBUG_LINE(vPos+<<0,0,0.5>>, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fRot, <<0,1.5,0.5>>),	iR,iG,iB, 128)
	
		// Draw switch B pos
		vPos = dropoff.vSwitchPos[1]
		fRot = dropoff.fSwitchRot[1]
		iR = 175
		iG = 0
		iB = 255
		DRAW_DEBUG_LINE(vPos-<<0,0,10>>, vPos+<<0,0,10>>,																iR,iG,iB, 128)
		DRAW_DEBUG_LINE(vPos+<<0,0,0.5>>, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fRot, <<0,1.5,0.5>>),	iR,iG,iB, 128)
	
		// Draw switch car pos
		vPos = dropoff.vSwitchPos[2]
		fRot = dropoff.fSwitchRot[2]
		iR = 175+75
		iG = 0+75
		iB = 255+75
		DRAW_DEBUG_LINE(vPos-<<0,0,10>>, vPos+<<0,0,10>>,																iR,iG,iB, 128)
		DRAW_DEBUG_LINE(vPos+<<0,0,0.5>>, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fRot, <<0,1.5,0.5>>),	iR,iG,iB, 128)
	
		// Grab camera positions
		IF DOES_CAM_EXIST(GET_DEBUG_CAM())
			IF IS_CAM_RENDERING(GET_DEBUG_CAM())
				// Force debug cam
				IF bShowFootPanStart5
					SET_CAM_COORD(GET_DEBUG_CAM(), dropoff.mFootPans[5].mStart.vPos)
					SET_CAM_ROT(GET_DEBUG_CAM(), dropoff.mFootPans[5].mStart.vRot)
					SET_CAM_FOV(GET_DEBUG_CAM(), dropoff.mFootPans[5].fFov)
					bShowFootPanStart5 = FALSE
				ENDIF

				// Grab debug cam
				INT i
				REPEAT COUNT_OF(dropoff.mFootPans) i
					// Grab foot cams from debug
					IF bGrabFootPanStart[i]
						dropoff.mFootPans[i].mStart.vPos = GET_CAM_COORD(GET_DEBUG_CAM())
						dropoff.mFootPans[i].mStart.vRot = GET_CAM_ROT(GET_DEBUG_CAM())
						dropoff.mFootPans[i].fFov		= GET_CAM_FOV(GET_DEBUG_CAM())
						dropoff.mFootPans[i].bEnabled	= TRUE
						bGrabFootPanStart[i] = FALSE
					ENDIF
					IF bGrabFootPanEnd[i]
						dropoff.mFootPans[i].mEnd.vPos	= GET_CAM_COORD(GET_DEBUG_CAM())
						dropoff.mFootPans[i].mEnd.vRot	= GET_CAM_ROT(GET_DEBUG_CAM())
						dropoff.mFootPans[i].fFov		= GET_CAM_FOV(GET_DEBUG_CAM())
						dropoff.mFootPans[i].bEnabled	= TRUE
						bGrabFootPanEnd[i] = FALSE
					ENDIF
				ENDREPEAT
				
				IF bGrabFootCatchupCam
					dropoff.mFootCatchupCam.vPos	= GET_CAM_COORD(GET_DEBUG_CAM())
					dropoff.mFootCatchupCam.vRot	= GET_CAM_ROT(GET_DEBUG_CAM())
					dropoff.fFootCatchupFov			= GET_CAM_FOV(GET_DEBUG_CAM())
					bGrabFootCatchupCam = FALSE
				ENDIF

				IF bGrabCarCatchupCam
					dropoff.mCarCatchupCam.vPos		= GET_CAM_COORD(GET_DEBUG_CAM())
					dropoff.mCarCatchupCam.vRot		= GET_CAM_ROT(GET_DEBUG_CAM())
					dropoff.fCarCatchupFov			= GET_CAM_FOV(GET_DEBUG_CAM())
					bGrabCarCatchupCam = FALSE
				ENDIF

				// Print FOV to screen
				TEXT_LABEL tFOVDebug = "CAN GRAB CAMERA"
//				tFOVDebug += GET_STRING_FROM_FLOAT(GET_CAM_FOV(GET_DEBUG_CAM()))

				SET_TEXT_COLOUR(100, 255, 255, 255)
				SET_TEXT_SCALE(0.7, 0.8)
				SET_TEXT_WRAP(0.0, 1.0)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.6, 0.85, "STRING", tFOVDebug)
			ENDIF
		ENDIF
		
		// Grab actor positions
		INT iGrabPosLoop, iSubLoop
		
		REPEAT c_iMaxGrabPositions iGrabPosLoop
			IF bGrabPosition[iGrabPosLoop]
				
				// If this is a new selection...
				IF iGrabPosState <> iGrabPosLoop
					
					// Set as current
					iGrabPosState = iGrabPosLoop
					
					// Deselect everything else
					REPEAT c_iMaxGrabPositions iSubLoop
						IF iSubLoop <> iGrabPosState
							bGrabPosition[iSubLoop] = FALSE
						ENDIF
					ENDREPEAT
				ENDIF

			ELSE
				
				// If has been deselected...
				IF iGrabPosState = iGrabPosLoop
				
					// Clear current
					iGrabPosState = -1

					// Deselect everything else
					REPEAT c_iMaxGrabPositions iSubLoop
						bGrabPosition[iSubLoop] = FALSE
					ENDREPEAT
				ENDIF

			ENDIF
		ENDREPEAT
		
		VECTOR vMousePos
		IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)
			vMousePos = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()

			IF iGrabPosState = 0		dropoff.vPlayerPos				= vMousePos
			ELIF iGrabPosState = 1		dropoff.vPedPos[0]				= vMousePos
			ELIF iGrabPosState = 2		dropoff.vPedPos[1]				= vMousePos
			ELIF iGrabPosState = 3		dropoff.vExitPos				= vMousePos
			ELIF iGrabPosState = 4		dropoff.vCarPos					= vMousePos
			ELIF iGrabPosState = 5		dropoff.vCarFakeWalkPos			= vMousePos
			ELIF iGrabPosState = 6		dropoff.vCarFakeDrivePos		= vMousePos
			ELIF iGrabPosState = 7		dropoff.mExtraRespot.vCarPos	= vMousePos
			ELIF iGrabPosState = 8		dropoff.vSwitchPos[0]			= vMousePos
			ELIF iGrabPosState = 9		dropoff.vSwitchPos[1]			= vMousePos
			ELIF iGrabPosState = 10		dropoff.vSwitchPos[2]			= vMousePos
			ENDIF
		ENDIF

		IF bRespotExpansion[0] AND bRespotExpansion[1]
			IF IS_BIT_SET(dropoff.mExtraRespot.expandDir, 0)
				bRespotExpansion[0] = FALSE
			ELSE
				bRespotExpansion[1] = FALSE
			ENDIF
		ENDIF
		IF bRespotExpansion[2] AND bRespotExpansion[3]
			IF IS_BIT_SET(dropoff.mExtraRespot.expandDir, 2)
				bRespotExpansion[2] = FALSE
			ELSE
				bRespotExpansion[3] = FALSE
			ENDIF
		ENDIF
		REPEAT COUNT_OF(bRespotExpansion) iBitLoop
			IF bRespotExpansion[iBitLoop]
				SET_BIT(dropoff.mExtraRespot.expandDir, iBitLoop)
			ELSE
				CLEAR_BIT(dropoff.mExtraRespot.expandDir, iBitLoop)
			ENDIF
		ENDREPEAT
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
			// Run scene if triggered
			IF CAN_PLAYER_START_CUTSCENE()
				IF bRunScene2
					PRIVATE_DEBUG_RunDropoffScene(dropoff, convPeds, 2, hFriendCarA, hFriendCarB)
					bRunScene2 = FALSE
				
				ELIF bRunScene1
					PRIVATE_DEBUG_RunDropoffScene(dropoff, convPeds, 1, hFriendCarA, hFriendCarB)
					bRunScene1 = FALSE
					
				ENDIF
			ENDIF
			
			IF bWarpToAnchor
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vAnchor)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				bWarpToAnchor = FALSE
			ENDIF
			INT iSwitch
			REPEAT 3 iSwitch
				IF bWarpToSwitch[iSwitch]
					SET_ENTITY_COORDS(PLAYER_PED_ID(), dropoff.vSwitchPos[iSwitch])
					SET_ENTITY_HEADING(PLAYER_PED_ID(), dropoff.fSwitchRot[iSwitch])
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					bWarpToSwitch[iSwitch] = FALSE
				ENDIF
			ENDREPEAT
			
			IF bGrabFriendCarA
				IF DOES_BLIP_EXIST(hFriendCarABlip)
					REMOVE_BLIP(hFriendCarABlip)
				ENDIF
//				IF DOES_ENTITY_EXIST(hFriendCarA)
//					SET_VEHICLE_AS_NO_LONGER_NEEDED(hFriendCarA)
//				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					hFriendCarA = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(hFriendCarA)
						hFriendCarABlip = CREATE_VEHICLE_BLIP(hFriendCarA)
//						IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hFriendCarA)
//							SET_ENTITY_AS_MISSION_ENTITY(hFriendCarA, TRUE, TRUE)
//						ENDIF
					ENDIF
				ENDIF
				bGrabFriendCarA = FALSE
			ENDIF
			IF bGrabFriendCarB
				IF DOES_BLIP_EXIST(hFriendCarBBlip)
					REMOVE_BLIP(hFriendCarBBlip)
				ENDIF
//				IF DOES_ENTITY_EXIST(hFriendCarB)
//					SET_VEHICLE_AS_NO_LONGER_NEEDED(hFriendCarB)
//				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					hFriendCarB = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(hFriendCarB)
						hFriendCarBBlip = CREATE_VEHICLE_BLIP(hFriendCarB)
//						IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hFriendCarB)
//							SET_ENTITY_AS_MISSION_ENTITY(hFriendCarB, TRUE, TRUE)
//						ENDIF
					ENDIF
				ENDIF
				bGrabFriendCarB = FALSE
			ENDIF

			IF bForceDebugDraw
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			ENDIF

		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_EditDropoffScene_Luke() // Cleanup")

	IF DOES_BLIP_EXIST(hLocBlip)
		REMOVE_BLIP(hLocBlip)
	ENDIF
	IF DOES_BLIP_EXIST(hFriendCarABlip)
		REMOVE_BLIP(hFriendCarABlip)
	ENDIF
	IF DOES_BLIP_EXIST(hFriendCarBBlip)
		REMOVE_BLIP(hFriendCarBBlip)
	ENDIF

	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)

	DELETE_WIDGET_GROUP(hDropoffGroup)
ENDPROC

PROC PRIVATE_DEBUG_SaveDropoffScene(TEXT_LABEL_63& tLocName, structFDropoffScene& scene, BOOL bIsFriendLoc, BOOL bExportWrapperComments = TRUE)

	TEXT_LABEL_63 tFuncName
	IF bIsFriendLoc
		tFuncName = "Private_FLOC_GetDropoffScene()"
	ELSE
		tFuncName = "PRIVATE_ActivityLoc_GetDropoffData()"
	ENDIF

	//-- Save output to file
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_SaveDropoffScene(", tLocName, ") // Save dropoff scene to file")

	OPEN_DEBUG_FILE()
		
		// Save Dropoff to file
		SAVE_NEWLINE_TO_DEBUG_FILE()
		IF bExportWrapperComments
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("//----------------------- ")SAVE_STRING_TO_DEBUG_FILE(tFuncName)SAVE_STRING_TO_DEBUG_FILE(" - ")SAVE_STRING_TO_DEBUG_FILE(tLocName) SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
		SAVE_STRING_TO_DEBUG_FILE("		CASE ")							SAVE_STRING_TO_DEBUG_FILE(tLocName)									SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[0].mStart.vPos	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[0].mStart.vPos)	SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[0].mStart.vRot	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[0].mStart.vRot)	SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[0].mEnd.vPos	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[0].mEnd.vPos)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[0].mEnd.vRot	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[0].mEnd.vRot)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[0].fFov			= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.mFootPans[0].fFov)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[0].fDuration	= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.mFootPans[0].fDuration)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[0].bEnabled		= ")SAVE_BOOL_TO_DEBUG_FILE(  scene.mFootPans[0].bEnabled)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[1].mStart.vPos	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[1].mStart.vPos)	SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[1].mStart.vRot	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[1].mStart.vRot)	SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[1].mEnd.vPos	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[1].mEnd.vPos)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[1].mEnd.vRot	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[1].mEnd.vRot)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[1].fFov			= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.mFootPans[1].fFov)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[1].fDuration	= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.mFootPans[1].fDuration)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[1].bEnabled		= ")SAVE_BOOL_TO_DEBUG_FILE(  scene.mFootPans[1].bEnabled)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[2].mStart.vPos	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[2].mStart.vPos)	SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[2].mStart.vRot	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[2].mStart.vRot)	SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[2].mEnd.vPos	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[2].mEnd.vPos)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[2].mEnd.vRot	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[2].mEnd.vRot)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[2].fFov			= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.mFootPans[2].fFov)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[2].fDuration	= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.mFootPans[2].fDuration)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[2].bEnabled		= ")SAVE_BOOL_TO_DEBUG_FILE(  scene.mFootPans[2].bEnabled)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[3].mStart.vPos	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[3].mStart.vPos)	SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[3].mStart.vRot	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[3].mStart.vRot)	SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[3].mEnd.vPos	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[3].mEnd.vPos)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[3].mEnd.vRot	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[3].mEnd.vRot)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[3].fFov			= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.mFootPans[3].fFov)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[3].fDuration	= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.mFootPans[3].fDuration)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[3].bEnabled		= ")SAVE_BOOL_TO_DEBUG_FILE(  scene.mFootPans[3].bEnabled)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[4].mStart.vPos	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[4].mStart.vPos)	SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[4].mStart.vRot	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[4].mStart.vRot)	SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[4].mEnd.vPos	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[4].mEnd.vPos)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[4].mEnd.vRot	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[4].mEnd.vRot)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[4].fFov			= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.mFootPans[4].fFov)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[4].fDuration	= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.mFootPans[4].fDuration)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[4].bEnabled		= ")SAVE_BOOL_TO_DEBUG_FILE(  scene.mFootPans[4].bEnabled)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[5].mStart.vPos	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[5].mStart.vPos)	SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[5].mStart.vRot	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[5].mStart.vRot)	SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[5].mEnd.vPos	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[5].mEnd.vPos)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[5].mEnd.vRot	= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootPans[5].mEnd.vRot)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[5].fFov			= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.mFootPans[5].fFov)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[5].fDuration	= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.mFootPans[5].fDuration)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootPans[5].bEnabled		= ")SAVE_BOOL_TO_DEBUG_FILE(  scene.mFootPans[5].bEnabled)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootCatchupCam.vPos		= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootCatchupCam.vPos)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mFootCatchupCam.vRot		= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mFootCatchupCam.vRot)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.fFootCatchupFov			= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.fFootCatchupFov)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mCarCatchupCam.vPos		= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mCarCatchupCam.vPos)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mCarCatchupCam.vRot		= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mCarCatchupCam.vRot)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.fCarCatchupFov			= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.fCarCatchupFov)				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vCarPos					= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vCarPos)					SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.fCarRot					= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.fCarRot)					SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vPlayerPos				= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vPlayerPos)					SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vPedPos[0]				= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vPedPos[0])					SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vPedPos[1]				= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vPedPos[1])					SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vExitPos					= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vExitPos)					SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.fFootSpeechDelay			= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.fFootSpeechDelay)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.fFootFinalDelay			= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.fFootFinalDelay)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.fCarFinalDelay0			= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.fCarFinalDelay0)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.fCarFinalDelay			= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.fCarFinalDelay)				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.bFootCam2IsAlternative	= ")SAVE_BOOL_TO_DEBUG_FILE(  scene.bFootCam2IsAlternative)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.bIsTaxiStyle				= ")SAVE_BOOL_TO_DEBUG_FILE(  scene.bIsTaxiStyle)				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vCarFakeWalkPos			= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vCarFakeWalkPos)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vCarFakeDrivePos			= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vCarFakeDrivePos)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mExtraRespot.vCarPos		= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mExtraRespot.vCarPos)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mExtraRespot.fCarRot		= ")SAVE_FLOAT_TO_DEBUG_FILE(scene.mExtraRespot.fCarRot)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mExtraRespot.expandDir	= ")SAVE_INT_TO_DEBUG_FILE(scene.mExtraRespot.expandDir)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vSwitchPos[0]				= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vSwitchPos[0])				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.fSwitchRot[0]				= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.fSwitchRot[0])				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vSwitchPos[1]				= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vSwitchPos[1])				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.fSwitchRot[1]				= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.fSwitchRot[1])				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vSwitchPos[2]				= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vSwitchPos[2])				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.fSwitchRot[2]				= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.fSwitchRot[2])				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			RETURN TRUE")																					SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("		BREAK")																								SAVE_NEWLINE_TO_DEBUG_FILE()
		IF bExportWrapperComments
		SAVE_STRING_TO_DEBUG_FILE("//----------------------- ")SAVE_STRING_TO_DEBUG_FILE(tFuncName)SAVE_STRING_TO_DEBUG_FILE(" - END")		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
		SAVE_NEWLINE_TO_DEBUG_FILE()

	CLOSE_DEBUG_FILE()

ENDPROC

PROC PRIVATE_DEBUG_ExportAllFLOCDropoffs()
	structFDropoffScene scene
	enumFriendLocation eLoc
	REPEAT MAX_FRIEND_LOCATIONS eLoc
		IF eLoc <> FLOC_adhoc
			Private_FLOC_GetDropoffScene(eLoc, scene)
			
			scene.mFootPans[5].mStart.vPos	= scene.mFootCatchupCam.vPos
			scene.mFootPans[5].mStart.vRot	= scene.mFootCatchupCam.vRot
			scene.mFootPans[5].mEnd.vPos	= scene.mFootCatchupCam.vPos
			scene.mFootPans[5].mEnd.vRot	= scene.mFootCatchupCam.vRot
			scene.mFootPans[5].fFov			= scene.fFootCatchupFov
			scene.mFootPans[5].fDuration	= 0
			scene.mFootPans[5].bEnabled		= TRUE

			TEXT_LABEL_63 tLocName = GetLabel_enumFriendLocation(eLoc)
			PRIVATE_DEBUG_SaveDropoffScene(tLocName, scene, TRUE, FALSE)
		ENDIF
	ENDREPEAT
ENDPROC

PROC PRIVATE_DEBUG_ExportAllALOCDropoffs()
	structFDropoffScene scene
	enumActivityLocation eLoc = ALOC_cinema_downtown
	WHILE eLoc <= ALOC_cinema_vinewood
		Private_ALOC_GetDropoffScene(eLoc, scene)

		scene.mFootPans[5].mStart.vPos	= scene.mFootCatchupCam.vPos
		scene.mFootPans[5].mStart.vRot	= scene.mFootCatchupCam.vRot
		scene.mFootPans[5].mEnd.vPos	= scene.mFootCatchupCam.vPos
		scene.mFootPans[5].mEnd.vRot	= scene.mFootCatchupCam.vRot
		scene.mFootPans[5].fFov			= scene.fFootCatchupFov
		scene.mFootPans[5].fDuration	= 0
		scene.mFootPans[5].bEnabled		= TRUE

		TEXT_LABEL_63 tLocName = GetLabel_enumActivityLocation(eLoc)
		PRIVATE_DEBUG_SaveDropoffScene(tLocName, scene, FALSE, FALSE)
		eLoc = INT_TO_ENUM(enumActivityLocation, ENUM_TO_INT(eLoc) + 1)
	ENDWHILE
ENDPROC

//---------------------------------------------------------------------------------------------------
//-- ACTIVITY SCENE (debug)
//---------------------------------------------------------------------------------------------------

PROC PRIVATE_DEBUG_RunActivityScene(structFActivityScene& scene, structPedsForConversation& convPeds, enumActivityLocation eLoc)
	
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_RunActivityScene()...")
	
	PED_INDEX hFriendA, hFriendB
	
	// Grab friend peds to use
	INT iLeaderCount, iFollowerCount
	GET_GROUP_SIZE(PLAYER_GROUP_ID(), iLeaderCount, iFollowerCount)
	
	IF iFollowerCount = 0
		IF NOT PRIVATE_DEBUG_CreateFriendPed()
			SCRIPT_ASSERT("PRIVATE_DEBUG_RunActivityScene() - Couldn't create friend ped")
			EXIT
		ENDIF
		hFriendA = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 0)
		hFriendB = NULL
	
	ELIF iFollowerCount = 1
		hFriendA = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 0)
		hFriendB = NULL
		
	ELSE
		hFriendA = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 0)
		hFriendB = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 1)
	ENDIF


	// Run the scene
	INT iCutsceneStep = 0
	
	WHILE NOT Private_ProcessActivityScene(eLoc, scene, iCutsceneStep, hFriendA, hFriendB, convPeds)
		WAIT(0)
	ENDWHILE
	
//	IF DOES_ENTITY_EXIST(hFriendA)
//		DELETE_PED(hFriendA)
//	ENDIF
//	IF DOES_ENTITY_EXIST(hFriendB)
//		DELETE_PED(hFriendB)
//	ENDIF

ENDPROC

PROC PRIVATE_DEBUG_MakeGroupSober()
	
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_MakeGroupSober()...")
	PED_INDEX hFriendA, hFriendB
	
	// Grab friend peds to use
	INT iLeaderCount, iFollowerCount
	GET_GROUP_SIZE(PLAYER_GROUP_ID(), iLeaderCount, iFollowerCount)
	
	MAKE_PED_SOBER(PLAYER_PED_ID())
	
	IF iFollowerCount > 0
		hFriendA = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 0)
		IF NOT IS_PED_INJURED(hFriendA)
			MAKE_PED_SOBER(hFriendA)
		ENDIF
	ENDIF
	IF iFollowerCount > 1
		hFriendB = GET_PED_AS_GROUP_MEMBER(PLAYER_GROUP_ID(), 1)
		IF NOT IS_PED_INJURED(hFriendB)
			MAKE_PED_SOBER(hFriendB)
		ENDIF
	ENDIF
	
	QUIT_DRUNK_CAMERA_IMMEDIATELY()

ENDPROC

PROC PRIVATE_DEBUG_DrawActivityScenePath(VECTOR& vNodes[], INT r, INT g, INT b, BOOL bIsCinema, INT iHighlight)

	// Draw path
	DRAW_DEBUG_LINE(vNodes[0], vNodes[1], r,g,b, 128)
	IF bIsCinema = FALSE
		DRAW_DEBUG_LINE(vNodes[1], vNodes[2], r,g,b, 128)
	ENDIF
	
	// Draw uprights
	INT iCount = COUNT_OF(vNodes)
	IF bIsCinema
		iCount--
	ENDIF
	
	INT i
	REPEAT iCount i
		DRAW_DEBUG_LINE(vNodes[i]-<<0,0,10>>, vNodes[i]+<<0,0,10>>, r,g,b, 128)
		r += 75
		g += 75
		b += 75
		
		IF i = iHighlight
			DRAW_DEBUG_SPHERE(vNodes[i]+<<0.0, 0.0, 0.25>>, 0.025, r,g,b, 128)
		ENDIF
	ENDREPEAT

ENDPROC


PROC PRIVATE_DEBUG_EditActivityScene(WIDGET_GROUP_ID& hParentWidgetGroup, structPedsForConversation& convPeds, BOOL& bEnable, structFActivityScene& scene, VECTOR vAnchor, enumActivityLocation eLoc)

	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_EditActivityScene(", GetLabel_enumActivityLocation(eLoc), ")...")

	// Rag accessors
	BOOL	bDisplayPositions		= TRUE
	BOOL	bToggleFriendPeds		= FALSE
	BOOL	bSetFriendsSober		= FALSE
	BOOL	bForceRunScene			= FALSE
	
	BOOL	bGrabCamPanA0			= FALSE
	BOOL	bGrabCamPanA1			= FALSE
	BOOL	bGrabCamTime			= FALSE
	BOOL	bGrabCamPanB0			= FALSE
	BOOL	bGrabCamPanB1			= FALSE
	BOOL	bIsCinema				= scene.bIsCinema

	INT		iCurGroup				= 0
	
	BOOL	bDisplayClear			= FALSE
	BOOL	bEditClear[2]
	INT		iCurClearEdit			= -1
	FLOAT	fClearHeight			= scene.vClearB.z - scene.vClearA.z
	
	BOOL	bQuickGrabPos[3]
	BOOL	bEditPos[9]
	INT		iCurPedPos				= 0
	VECTOR	vPedPosOffset			= scene.vPedPPos[0] - vAnchor
	
	bEditPos[0] = TRUE
	bEditPos[1] = FALSE
	bEditPos[2] = FALSE
	bEditPos[3] = FALSE
	bEditPos[4] = FALSE
	bEditPos[5] = FALSE
	bEditPos[6] = FALSE
	bEditPos[7] = FALSE
	bEditPos[8] = FALSE
	
	bQuickGrabPos[0] = FALSE
	bQuickGrabPos[1] = FALSE
	bQuickGrabPos[2] = FALSE


	//-- Create widgets --
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_EditActivityScene() // Create widgets")
	
	WIDGET_GROUP_ID hASceneGroup

	SET_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
		hASceneGroup =
		START_WIDGET_GROUP("Scene data")
			// Add widgets
			ADD_WIDGET_BOOL("Toggle friend peds",				bToggleFriendPeds)
			ADD_WIDGET_BOOL("Set friends sober",				bSetFriendsSober)
			ADD_WIDGET_BOOL("Run scene",						bForceRunScene)

			START_WIDGET_GROUP("Camera")
				ADD_WIDGET_BOOL("Grab cam pan A start",				bGrabCamPanA0)
				ADD_WIDGET_BOOL("Grab cam pan A end",				bGrabCamPanA1)
				ADD_WIDGET_BOOL("Grab cam time",					bGrabCamTime)
				ADD_WIDGET_BOOL("Grab cam pan B start",				bGrabCamPanB0)
				ADD_WIDGET_BOOL("Grab cam pan B end",				bGrabCamPanB1)
				ADD_WIDGET_FLOAT_SLIDER("Pan A duration (normal)",	scene.fPanADurationNormal, 0.0, 10.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Pan A duration (drunk)",	scene.fPanADurationDrunk, 0.0, 10.0, 1.0)
				ADD_WIDGET_BOOL("Is cinema scene",					bIsCinema)
			STOP_WIDGET_GROUP()

			START_WIDGET_GROUP("Ped positions")
				ADD_WIDGET_BOOL("Edit positions",				bDisplayPositions)
				ADD_WIDGET_VECTOR_SLIDER("Ped pos",				vPedPosOffset, -30.0, 30.0, 0.1)
				ADD_WIDGET_STRING(".")
				ADD_WIDGET_BOOL("Edit player pos 0",			bEditPos[0])
				ADD_WIDGET_BOOL("Edit player pos 1",			bEditPos[1])
				ADD_WIDGET_BOOL("Edit player pos 2",			bEditPos[2])
				ADD_WIDGET_STRING(".")
				ADD_WIDGET_BOOL("Edit friend A pos 0",			bEditPos[3])
				ADD_WIDGET_BOOL("Edit friend A pos 1",			bEditPos[4])
				ADD_WIDGET_BOOL("Edit friend A pos 2",			bEditPos[5])
				ADD_WIDGET_STRING(".")
				ADD_WIDGET_BOOL("Edit friend B pos 0",			bEditPos[6])
				ADD_WIDGET_BOOL("Edit friend B pos 1",			bEditPos[7])
				ADD_WIDGET_BOOL("Edit friend B pos 2",			bEditPos[8])
				ADD_WIDGET_STRING(".")
				ADD_WIDGET_BOOL("Quick grab all pos 0",			bQuickGrabPos[0])
				ADD_WIDGET_BOOL("Quick grab all pos 1",			bQuickGrabPos[1])
				ADD_WIDGET_BOOL("Quick grab all pos 2",			bQuickGrabPos[2])
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Clear box")
				ADD_WIDGET_BOOL("Edit vehicle clear box",		bDisplayClear)
				ADD_WIDGET_BOOL("Edit pos 0",					bEditClear[0])
				ADD_WIDGET_BOOL("Edit pos 1",					bEditClear[1])
				ADD_WIDGET_FLOAT_SLIDER("Width",				scene.fClearW, 0.0, 100.0, 10.0)
				ADD_WIDGET_FLOAT_SLIDER("Height",				fClearHeight, 0.0, 50.0, 10.0)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
	
	
	//-- Setup world --
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_EditActivityScene() // Setup world")
	
	// Create pickup loc blip
	BLIP_INDEX hLocBlip = ADD_BLIP_FOR_COORD(vAnchor)
	
	// Warp player
	CLEAR_AREA(vAnchor, 25.0, TRUE)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vAnchor)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		//LOAD_SCENE(vPickupCoord)
	ENDIF


	//-- Monitor widgets --
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_EditActivityScene() // Montior widgets")
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	

	WHILE bEnable
		
		// Grab camera positions
		IF DOES_CAM_EXIST(GET_DEBUG_CAM())
			IF IS_CAM_RENDERING(GET_DEBUG_CAM())
				// Grab debug cam
				IF bGrabCamPanA0
					scene.mCamPanA[0].vPos	= GET_CAM_COORD(GET_DEBUG_CAM())
					scene.mCamPanA[0].vRot	= GET_CAM_ROT(GET_DEBUG_CAM())
					scene.fCamPanAFov		= GET_CAM_FOV(GET_DEBUG_CAM())
					bGrabCamPanA0 = FALSE
				ENDIF
				IF bGrabCamPanA1
					scene.mCamPanA[1].vPos	= GET_CAM_COORD(GET_DEBUG_CAM())
					scene.mCamPanA[1].vRot	= GET_CAM_ROT(GET_DEBUG_CAM())
					scene.fCamPanAFov		= GET_CAM_FOV(GET_DEBUG_CAM())
					bGrabCamPanA1 = FALSE
				ENDIF
				IF bGrabCamTime
					scene.mCamTime.vPos		= GET_CAM_COORD(GET_DEBUG_CAM())
					scene.mCamTime.vRot		= GET_CAM_ROT(GET_DEBUG_CAM())
					scene.fCamTimeFov		= GET_CAM_FOV(GET_DEBUG_CAM())
					bGrabCamPanA1 = FALSE
				ENDIF
				IF bGrabCamPanB0
					scene.mCamPanB[0].vPos	= GET_CAM_COORD(GET_DEBUG_CAM())
					scene.mCamPanB[0].vRot	= GET_CAM_ROT(GET_DEBUG_CAM())
					scene.fCamPanBFov		= GET_CAM_FOV(GET_DEBUG_CAM())
					bGrabCamPanB0 = FALSE
				ENDIF
				IF bGrabCamPanB1
					scene.mCamPanB[1].vPos	= GET_CAM_COORD(GET_DEBUG_CAM())
					scene.mCamPanB[1].vRot	= GET_CAM_ROT(GET_DEBUG_CAM())
					scene.fCamPanBFov		= GET_CAM_FOV(GET_DEBUG_CAM())
					bGrabCamPanB1 = FALSE
				ENDIF

				// Print FOV to screen
				TEXT_LABEL tFOVDebug = "FOV: "
				tFOVDebug += GET_STRING_FROM_FLOAT(GET_CAM_FOV(GET_DEBUG_CAM()))

				SET_TEXT_COLOUR(100, 255, 255, 255)
				SET_TEXT_SCALE(0.7, 0.8)
				SET_TEXT_WRAP(0.0, 1.0)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.78, 0.85, "STRING", tFOVDebug)
			ENDIF
		ENDIF
		
		scene.bIsCinema = bIsCinema
		
		// Make position/clear box editing mutually exclusive
		IF bDisplayPositions AND iCurGroup <> 0
			bDisplayClear = FALSE
			iCurGroup = 0
		
		ELIF bDisplayClear AND iCurGroup <> 1
			bDisplayPositions = FALSE
			iCurGroup = 1
		ENDIF
		
		//-- Ped Positions
		IF bDisplayPositions
			
			DRAW_DEBUG_SPHERE(vAnchor, 0.1, 255,255,255, 128)
			
			PRIVATE_DEBUG_DrawActivityScenePath(scene.vPedPPos, 200, 0, 200, scene.bIsCinema, iCurPedPos)
			PRIVATE_DEBUG_DrawActivityScenePath(scene.vPedAPos, 255, 0,  75, scene.bIsCinema, iCurPedPos - 3)
			PRIVATE_DEBUG_DrawActivityScenePath(scene.vPedBPos, 0, 255, 175, scene.bIsCinema, iCurPedPos - 6)
		
			// If a box is ticked, make that the current edit pos
			BOOL bUpdatedEditPos = FALSE
			INT i
			REPEAT COUNT_OF(bEditPos) i
				IF bUpdatedEditPos = FALSE
					IF bEditPos[i]
						IF iCurPedPos <> i
							iCurPedPos = i
							
							IF iCurPedPos = 0			vPedPosOffset = scene.vPedPPos[0] - vAnchor
							ELIF iCurPedPos = 1			vPedPosOffset = scene.vPedPPos[1] - vAnchor
							ELIF iCurPedPos = 2			vPedPosOffset = scene.vPedPPos[2] - vAnchor
							ELIF iCurPedPos = 3			vPedPosOffset = scene.vPedAPos[0] - vAnchor
							ELIF iCurPedPos = 4			vPedPosOffset = scene.vPedAPos[1] - vAnchor
							ELIF iCurPedPos = 5			vPedPosOffset = scene.vPedAPos[2] - vAnchor
							ELIF iCurPedPos = 6			vPedPosOffset = scene.vPedBPos[0] - vAnchor
							ELIF iCurPedPos = 7			vPedPosOffset = scene.vPedBPos[1] - vAnchor
							ELIF iCurPedPos = 8			vPedPosOffset = scene.vPedBPos[2] - vAnchor
							ENDIF
							
							bUpdatedEditPos = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT

			// Untick all boxes that aren't the current edit pos
			REPEAT 9 i
				IF iCurPedPos = i
					bEditPos[i] = TRUE
				ELSE
					bEditPos[i] = FALSE
				ENDIF
			ENDREPEAT

			// Update the current edit pos
			IF IS_MOUSE_BUTTON_JUST_PRESSED(MB_LEFT_BTN)
				vPedPosOffset = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS() - vAnchor
			ENDIF
			VECTOR vPedPos = vAnchor + vPedPosOffset

			IF iCurPedPos = 0			scene.vPedPPos[0] = vPedPos
			ELIF iCurPedPos = 1			scene.vPedPPos[1] = vPedPos
			ELIF iCurPedPos = 2			scene.vPedPPos[2] = vPedPos
			ELIF iCurPedPos = 3			scene.vPedAPos[0] = vPedPos
			ELIF iCurPedPos = 4			scene.vPedAPos[1] = vPedPos
			ELIF iCurPedPos = 5			scene.vPedAPos[2] = vPedPos
			ELIF iCurPedPos = 6			scene.vPedBPos[0] = vPedPos
			ELIF iCurPedPos = 7			scene.vPedBPos[1] = vPedPos
			ELIF iCurPedPos = 8			scene.vPedBPos[2] = vPedPos
			ENDIF

			// Quick grab from player
			REPEAT COUNT_OF(bQuickGrabPos) i
				IF bQuickGrabPos[i]
					scene.vPedPPos[i] = GET_ENTITY_COORDS(PLAYER_PED_ID())
					scene.vPedAPos[i] = GET_ENTITY_COORDS(PLAYER_PED_ID())
					scene.vPedBPos[i] = GET_ENTITY_COORDS(PLAYER_PED_ID())
					bQuickGrabPos[i] = FALSE
				ENDIF
			ENDREPEAT


		//-- Clear Box
		ELIF bDisplayClear

			// Display ped clear box
			VECTOR vTempMin = <<0.0, 0.0, 0.0>>
			VECTOR vTempMax = <<0.0, 0.0, 0.0>>
			Private_GetActivityScenePedPosBoundingBox(scene, vTempMin, vTempMax)
			DRAW_DEBUG_BOX(vTempMin, vTempMax, 255, 0, 0, 100)

			// Display vehicle clear box
			scene.vClearB.z = scene.vClearA.z + fClearHeight
			DRAW_DEBUG_ANGLED_AREA(scene.vClearA, scene.vClearB, scene.fClearW, 0, 0, 0, 100)//255)
			
			// Update which point is being edited
			IF bEditClear[0] AND iCurClearEdit <> 0
				iCurClearEdit = 0
				bEditClear[1] = FALSE
			
			ELIF bEditClear[1] AND iCurClearEdit <> 1
				iCurClearEdit = 1
				bEditClear[0] = FALSE
			
			ELIF NOT bEditClear[0] AND NOT bEditClear[1]
				iCurClearEdit = -1
			ENDIF
			
			// Draw highlight on edit point
			IF iCurClearEdit = 0			DRAW_DEBUG_SPHERE(scene.vClearA, 0.01, 150, 150, 150, 255)
			ELIF iCurClearEdit = 1			DRAW_DEBUG_SPHERE(scene.vClearB, 0.01, 150, 150, 150, 255)
			ENDIF
			
			// Update edit point on click
			IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)
				IF iCurClearEdit = 0		scene.vClearA = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
				ELIF iCurClearEdit = 1		scene.vClearB = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
				ENDIF
			ENDIF
		
		ENDIF
	
		// Run scene + manage buddy peds
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
			INT iLeaderCount = 0, iFollowerCount = 0
			GET_GROUP_SIZE(PLAYER_GROUP_ID(), iLeaderCount, iFollowerCount)
			
			// Toggle friends
			IF bToggleFriendPeds
				IF iFollowerCount < 2
					PRIVATE_DEBUG_CreateFriendPed()
				ELSE
					PRIVATE_DEBUG_RemoveAllFriendPeds()
				ENDIF
				bToggleFriendPeds = FALSE
			ENDIF

			// Run scene if triggered
			IF CAN_PLAYER_START_CUTSCENE()
				IF bForceRunScene
//					PRIVATE_DEBUG_MakeGroupSober()
					PRIVATE_DEBUG_RunActivityScene(scene, convPeds, eLoc)
					bForceRunScene = FALSE
				ENDIF
			ENDIF
			
			IF bSetFriendsSober
				PRIVATE_DEBUG_MakeGroupSober()
				bSetFriendsSober = FALSE
			ENDIF
			
		ENDIF
			
		WAIT(0)
	ENDWHILE
	
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_EditActivityScene() // Cleanup")

	IF DOES_BLIP_EXIST(hLocBlip)
		REMOVE_BLIP(hLocBlip)
	ENDIF

	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)

	DELETE_WIDGET_GROUP(hASceneGroup)
ENDPROC

PROC PRIVATE_DEBUG_SaveActivityScene(TEXT_LABEL_63& tLocName, structFActivityScene& scene)

	TEXT_LABEL_63 tFuncName = "Private_ALOC_GetActivityScene()"

	//-- Save output to file
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_SaveActivityScene(", tLocName, ") // Save activity scene to file")

	OPEN_DEBUG_FILE()
		
		// Save activity scene to file
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("//----------------------- ")SAVE_STRING_TO_DEBUG_FILE(tFuncName)SAVE_STRING_TO_DEBUG_FILE(" - ")SAVE_STRING_TO_DEBUG_FILE(tLocName) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("		CASE ")							SAVE_STRING_TO_DEBUG_FILE(tLocName)								SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mCamPanA[0].vPos		= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mCamPanA[0].vPos)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mCamPanA[0].vRot		= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mCamPanA[0].vRot)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mCamPanA[1].vPos		= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mCamPanA[1].vPos)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mCamPanA[1].vRot		= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mCamPanA[1].vRot)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.fCamPanAFov			= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.fCamPanAFov)				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mCamTime.vPos			= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mCamTime.vPos)				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mCamTime.vRot			= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mCamTime.vRot)				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.fCamTimeFov			= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.fCamTimeFov)				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mCamPanB[0].vPos		= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mCamPanB[0].vPos)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mCamPanB[0].vRot		= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mCamPanB[0].vRot)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mCamPanB[1].vPos		= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mCamPanB[1].vPos)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.mCamPanB[1].vRot		= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.mCamPanB[1].vRot)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.fCamPanBFov			= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.fCamPanBFov)				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.fPanADurationNormal	= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.fPanADurationNormal)		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.fPanADurationDrunk	= ")SAVE_FLOAT_TO_DEBUG_FILE( scene.fPanADurationDrunk)			SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.bIsCinema				= ")SAVE_BOOL_TO_DEBUG_FILE(scene.bIsCinema)					SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vPedPPos[0]			= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vPedPPos[0])				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vPedPPos[1]			= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vPedPPos[1])				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vPedPPos[2]			= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vPedPPos[2])				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vPedAPos[0]			= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vPedAPos[0])				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vPedAPos[1]			= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vPedAPos[1])				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vPedAPos[2]			= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vPedAPos[2])				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vPedBPos[0]			= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vPedBPos[0])				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vPedBPos[1]			= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vPedBPos[1])				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vPedBPos[2]			= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vPedBPos[2])				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vClearA				= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vClearA)					SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.vClearB				= ")SAVE_VECTOR_TO_DEBUG_FILE(scene.vClearB)					SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			scene.fClearW				= ")SAVE_FLOAT_TO_DEBUG_FILE(scene.fClearW)						SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("			RETURN TRUE")																				SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("		BREAK")																							SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("//----------------------- ")SAVE_STRING_TO_DEBUG_FILE(tFuncName)SAVE_STRING_TO_DEBUG_FILE(" - END")	SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()

	CLOSE_DEBUG_FILE()

ENDPROC



//---------------------------------------------------------------------------------------------------
//-- DISPLAY (track player instances)
//---------------------------------------------------------------------------------------------------


PROC PRIVATE_DEBUG_TrackFriendPlayerInstances(BOOL bEnable, BLIP_INDEX &hBlips[])

	CONST_INT DEBUG_CONST_iExistsRow 10

	// For each playable character
	IF bEnable
		INT i
		REPEAT 3 i
			// Get character
			enumCharacterList char = INT_TO_ENUM(enumCharacterList, i)
			
			// Get traits
			HUD_COLOURS hudColourExists	= HUD_COLOUR_PURE_WHITE
			HUD_COLOURS hudColourKnown	= HUD_COLOUR_WHITE
			INT			iBlipColour		= BLIP_COLOUR_RED
			TEXT_LABEL	tName			= "PlayerChar"

			IF i = 0
				tName = "Michael"
				hudColourExists = HUD_COLOUR_REDLIGHT
				hudColourKnown = HUD_COLOUR_REDDARK
				iBlipColour = BLIP_COLOUR_RED
			ELIF i = 1
				tName = "Franklin"
				hudColourExists = HUD_COLOUR_BLUELIGHT
				hudColourKnown = HUD_COLOUR_BLUEDARK
				iBlipColour = BLIP_COLOUR_BLUE
			ELSE
				tName = "Trevor"
				hudColourExists = HUD_COLOUR_YELLOWLIGHT
				hudColourKnown = HUD_COLOUR_YELLOWDARK
				iBlipColour = BLIP_COLOUR_YELLOW
			ENDIF
			
			PED_INDEX hPed = g_sPlayerPedRequest.sSelectorPeds.pedID[GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(char)]
		
			// If ped is the player...
			IF g_eDefaultPlayerChar = char
				DrawFriendLiteralString(tName, DEBUG_CONST_iExistsRow+i, HUD_COLOUR_PURE_WHITE)
				IF DOES_BLIP_EXIST(hBlips[i])
					REMOVE_BLIP(hBlips[i])
				ENDIF
			
			// If ped exists in world...
			ELIF DOES_ENTITY_EXIST(hPed)
				DrawFriendLiteralString(tName, DEBUG_CONST_iExistsRow+i, hudColourExists)
				
				IF NOT DOES_BLIP_EXIST(hBlips[i])
				OR GET_BLIP_ALPHA(hBlips[i]) <> 255
					IF DOES_BLIP_EXIST(hBlips[i])
						REMOVE_BLIP(hBlips[i])
					ENDIF
					
					hBlips[i] = CREATE_BLIP_FOR_PED(g_sPlayerPedRequest.sSelectorPeds.pedID[GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(char)])
					
					IF DOES_BLIP_EXIST(hBlips[i])
						SET_BLIP_COLOUR(hBlips[i], iBlipColour)
						SET_BLIP_SCALE(hBlips[i], 2.0)
						SET_BLIP_ALPHA(hBlips[i], 255)
					ENDIF
				ENDIF

			// If is position known
			ELIF IS_TIMEOFDAY_VALID(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[char])
			AND NOT HasNumOfHoursPassedSincePedTimeStruct(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[char], 1)
			AND g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[char].x <> 0.0
			AND g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[char].y <> 0.0
			AND g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[char].z <> 0.0

				INT iSeconds=0, iMinutes=0, iHours=0, iDays=0, iMonths=0, iYears=0
				GET_DIFFERENCE_BETWEEN_NOW_AND_TIMEOFDAY(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[char], iSeconds, iMinutes, iHours, iDays, iMonths, iYears)
	
				iMinutes += iHours * 60
				iMinutes += iDays * 60 * 24
				TEXT_LABEL_63 tNameWithTime = tName
				tNameWithTime += "	"
				tNameWithTime += iMinutes
				tNameWithTime += " mins inactive"
				
				DrawFriendLiteralString(tNameWithTime, DEBUG_CONST_iExistsRow+i, hudColourKnown)
				
				IF DOES_BLIP_EXIST(hBlips[i])
					REMOVE_BLIP(hBlips[i])
				ENDIF

				hBlips[i] = CREATE_BLIP_FOR_COORD(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[char])
				
				IF DOES_BLIP_EXIST(hBlips[i])
					SET_BLIP_COLOUR(hBlips[i], iBlipColour)
					SET_BLIP_SCALE(hBlips[i], 1.5)
					SET_BLIP_ALPHA(hBlips[i], 100)
				ENDIF
			
			// If ped has no known position...
			ELSE
				DrawFriendLiteralString(tName, DEBUG_CONST_iExistsRow+i, HUD_COLOUR_GREY)
				IF DOES_BLIP_EXIST(hBlips[i])
					REMOVE_BLIP(hBlips[i])
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			DrawFriendLiteralString("IS_PLAYER_SWITCH_IN_PROGRESS()", DEBUG_CONST_iExistsRow+5, HUD_COLOUR_GREY)
		
		ELIF GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT
			DrawFriendLiteralString("IS_PLAYER_SWITCH_IN_PROGRESS(): Short", DEBUG_CONST_iExistsRow+5, HUD_COLOUR_PURE_WHITE)
		
		ELSE
			TEXT_LABEL_63 tJumpCut = "IS_PLAYER_SWITCH_IN_PROGRESS(): "
			
			IF GET_PLAYER_SWITCH_TYPE() <> SWITCH_TYPE_SHORT
				tJumpCut += GET_PLAYER_SWITCH_JUMP_CUT_INDEX()
			ELSE	
				tJumpCut += "short"
			ENDIF
			
			DrawFriendLiteralString(tJumpCut, DEBUG_CONST_iExistsRow+5, HUD_COLOUR_PURE_WHITE)
		ENDIF

		IF (g_sPlayerPedRequest.eState <> PR_STATE_PROCESSING)
			DrawFriendLiteralString("PR_STATE_PROCESSING", DEBUG_CONST_iExistsRow+6, HUD_COLOUR_GREY)
		ELSE
			DrawFriendLiteralString("PR_STATE_PROCESSING", DEBUG_CONST_iExistsRow+6, HUD_COLOUR_PURE_WHITE)
		ENDIF

	ELSE
		// Switched off -> remove blips
		INT i
		REPEAT 3 i
			IF DOES_BLIP_EXIST(hBlips[i])
				REMOVE_BLIP(hBlips[i])
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_VEHICLE_A_PLAYER_PERSONAL_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), CHAR_MICHAEL)
				DrawFriendLiteralString("Is player's personal vehicle: MICHAEL", DEBUG_CONST_iExistsRow+7, HUD_COLOUR_PURE_WHITE)
				
			ELIF IS_VEHICLE_A_PLAYER_PERSONAL_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), CHAR_FRANKLIN)
				DrawFriendLiteralString("Is player's personal vehicle: FRANKLIN", DEBUG_CONST_iExistsRow+7, HUD_COLOUR_PURE_WHITE)
				
			ELIF IS_VEHICLE_A_PLAYER_PERSONAL_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), CHAR_TREVOR)
				DrawFriendLiteralString("Is player's personal vehicle: TREVOR", DEBUG_CONST_iExistsRow+7, HUD_COLOUR_PURE_WHITE)
				
			ELSE
				DrawFriendLiteralString("Is player's personal vehicle: NONE", DEBUG_CONST_iExistsRow+7, HUD_COLOUR_GREY)
			ENDIF
		ENDIF
	ENDIF

ENDPROC


PROC PRIVATE_DEBUG_TestAdhocFriendLocation(BOOL& bTestAdhocFriendLoc)

	// Maintain adhoc FriendLocation
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_TestAdhocFriendLocation() // Display adhoc location every frame")
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	BLIP_INDEX hAdhocBlip = NULL

	
	WHILE bTestAdhocFriendLoc
			
		Private_ClearAdhocFriendLoc()
		IF DOES_BLIP_EXIST(hAdhocBlip)
			REMOVE_BLIP(hAdhocBlip)
		ENDIF

		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			IF Private_GetAvailableAdhocFriendLoc(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				VECTOR vDriveway = g_FriendLocations[FLOC_adhoc].vPickupCoord
				VECTOR vDoorstepA = g_FriendLocations[FLOC_adhoc].vPickupCoord + g_FriendLocations[FLOC_adhoc].vPedOffsetA
				VECTOR vDoorstepB = g_FriendLocations[FLOC_adhoc].vPickupCoord + g_FriendLocations[FLOC_adhoc].vPedOffsetB

				// Draw loc position + ped offset
				DRAW_DEBUG_SPHERE(vDriveway, 0.1, 255,255,255, 128)
				DRAW_DEBUG_LINE(vDriveway, vDoorstepA, 255,255,255, 128)
				DRAW_DEBUG_LINE(vDoorstepA-<<0,0,10>>, vDoorstepA+<<0,0,10>>, 255,255,255, 128)
				DRAW_DEBUG_LINE(vDriveway, vDoorstepB, 255,255,255, 128)
				DRAW_DEBUG_LINE(vDoorstepB-<<0,0,10>>, vDoorstepB+<<0,0,10>>, 255,255,255, 128)
				
				// Draw spawn and park positions
				DRAW_DEBUG_SPHERE(g_FriendLocations[FLOC_adhoc].vSpawnPos, 0.1, 0,255,0, 128)
				DRAW_DEBUG_SPHERE(g_FriendLocations[FLOC_adhoc].vParkPos, 0.1, 0,255,255, 128)
				
				// Draw ped offset B
				VECTOR vParkPos = g_FriendLocations[FLOC_adhoc].vParkPos
				DRAW_DEBUG_LINE(vDriveway, vParkPos, 0,255,255, 128)
				DRAW_DEBUG_LINE(vParkPos-<<0,0,10>>, vParkPos+<<0,0,10>>, 0,255,255, 128)

				// Add blip
				hAdhocBlip = ADD_BLIP_FOR_COORD(vDriveway)
				IF DOES_BLIP_EXIST(hAdhocBlip)
					SET_BLIP_AS_FRIENDLY(hAdhocBlip, TRUE)
					SET_BLIP_SPRITE(hAdhocBlip, RADAR_TRACE_FRIEND)
				ENDIF
			ELSE
				DISPLAY_TEXT(0.5, 0.1, "No adhoc loc")
				SCRIPT_ASSERT("PRIVATE_DEBUG_TestAdhocFriendLocation() - No adhoc location found")
			ENDIF
		ENDIF
		
		WAIT(0)

	ENDWHILE
				
	// Cleanup
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_TestAdhocFriendLocation() // Cleanup")
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)

	Private_ClearAdhocFriendLoc()
	IF DOES_BLIP_EXIST(hAdhocBlip)
		REMOVE_BLIP(hAdhocBlip)
	ENDIF
	
ENDPROC


//---------------------------------------------------------------------------------------------------
//-- TESTING (debug)
//---------------------------------------------------------------------------------------------------

PROC PRIVATE_DEBUG_TestKnockoutScene(enumCharacterList eChar, FLOAT fHitTime, VECTOR vBoxOffsetA, VECTOR vBoxSizeA, VECTOR vBoxOffsetB, VECTOR vBoxSizeB, VECTOR vBoxOffsetC, VECTOR vBoxSizeC)

	TEXT_LABEL_63	tAnimDict	= "friends@frt@ig_2"
	TEXT_LABEL_63	tPlayerAnim	= "knockout_player"
	TEXT_LABEL_63	tFriendAnim	= "knockout_trevor"
	TEXT_LABEL_63	tCameraAnim	= "knockout_cam"
	
	SWITCH eChar
		CASE CHAR_MICHAEL
			tAnimDict	= "friends@frm@ig_2"
			tPlayerAnim	= "knockout_player"
			tFriendAnim	= "knockout_mic"
			tCameraAnim	= "knockout_cam"
//			fHitTime	= 0.295
		BREAK
		CASE CHAR_FRANKLIN
			tAnimDict	= "friends@frf@ig_2"
			tPlayerAnim	= "knockout_plyr"
			tFriendAnim	= "knockout_fra"
			tCameraAnim	= "knockout_cam"
//			fHitTime	= 0.257
		BREAK
		CASE CHAR_TREVOR
			tAnimDict	= "friends@frt@ig_2"
			tPlayerAnim	= "knockout_player"
			tFriendAnim	= "knockout_trevor"
			tCameraAnim	= "knockout_cam"
//			fHitTime	= 0.391
		BREAK
	ENDSWITCH
	
	
	// Setup
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_TestKnockoutScene() // Setup")
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)//, SPC_LEAVE_CAMERA_CONTROL_ON)
	
	SET_ENTITY_HEADING(PLAYER_PED_ID(), 0)		// TEMP - for testing bbox


	// Load anim dict
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_TestKnockoutScene() // Load anim dict")
	REQUEST_ANIM_DICT(tAnimDict)
	WHILE NOT HAS_ANIM_DICT_LOADED(tAnimDict)
		REQUEST_ANIM_DICT(tAnimDict)
		WAIT(0)
	ENDWHILE

	
	// Create friend ped
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_TestKnockoutScene() // Create friend ped")
	MODEL_NAMES eModel = A_M_M_BevHills_01
	REQUEST_MODEL(eModel)
	WHILE NOT HAS_MODEL_LOADED(eModel)
		REQUEST_MODEL(eModel)
		WAIT(0)
	ENDWHILE
	PED_INDEX hFriend = CREATE_PED(PEDTYPE_CIVMALE, eModel, GET_ENTITY_COORDS(PLAYER_PED_ID()))
	SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
	
	
	// Create camera
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_TestKnockoutScene() // Create camera")
	CAMERA_INDEX hCamera = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)

	
	// Start sync scene
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_TestKnockoutScene() // Start sync scene")
	
	VECTOR vPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	VECTOR vAnchor = vPos
	GET_GROUND_Z_FOR_3D_COORD(vAnchor, vAnchor.z)
	
	FLOAT fHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
	VECTOR vRot = << 0.0, 0.0, fHeading >>
	
	INT hScene = CREATE_SYNCHRONIZED_SCENE(vAnchor, vRot)
	TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(),	hScene, tAnimDict, tPlayerAnim, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
	TASK_SYNCHRONIZED_SCENE(hFriend,			hScene, tAnimDict, tFriendAnim, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
	PLAY_SYNCHRONIZED_CAM_ANIM(hCamera,			hScene, tCameraAnim, tAnimDict)

	IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(hScene)
		SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(hScene, FALSE)
	ENDIF

	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	

	// Wait for sync scene
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_TestKnockoutScene() // Wait for sync scene")
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	BOOL bHasSwitchedToRagdoll = FALSE
	
	WHILE IS_SYNCHRONIZED_SCENE_RUNNING(hScene) AND NOT IS_SCREEN_FADED_OUT()
		
		WAIT(0)
		DRAW_DEBUG_SPHERE(vAnchor, 0.2, 255, 100, 100, 100)
		
		IF vBoxSizeA.x != 0.0 AND vBoxSizeA.y != 0.0 AND vBoxSizeA.z != 0.0
			DRAW_DEBUG_BOX(vPos + vBoxOffsetA - (vBoxSizeA*0.5), vPos + vBoxOffsetA + (vBoxSizeA*0.5), 255, 0, 64, 64)
		ENDIF
		IF vBoxSizeB.x != 0.0 AND vBoxSizeB.y != 0.0 AND vBoxSizeB.z != 0.0
			DRAW_DEBUG_BOX(vPos + vBoxOffsetB - (vBoxSizeB*0.5), vPos + vBoxOffsetB + (vBoxSizeB*0.5), 64, 0, 255, 64)
		ENDIF
		IF vBoxSizeC.x != 0.0 AND vBoxSizeC.y != 0.0 AND vBoxSizeC.z != 0.0
			DRAW_DEBUG_BOX(vPos + vBoxOffsetC - (vBoxSizeC*0.5), vPos + vBoxOffsetC + (vBoxSizeC*0.5), 64, 128, 64, 64)
		ENDIF
		
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
			DO_SCREEN_FADE_OUT(500)
		ENDIF
		
		IF bHasSwitchedToRagdoll = FALSE
			IF GET_SYNCHRONIZED_SCENE_PHASE(hScene) >= fHitTime
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//					SET_ENTITY_HEALTH(PLAYER_PED_ID(), 0)
					SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 30000, 30000, TASK_RELAX, FALSE, FALSE, FALSE)
					bHasSwitchedToRagdoll = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF bHasSwitchedToRagdoll
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.1, "STRING", "Ragdolling")
		ENDIF
		
	ENDWHILE

	// Clean up scene
	CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_TestKnockoutScene() // Cleanup")
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_TO_ANIMATED(PLAYER_PED_ID(), TRUE)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeading)
	ENDIF

	IF DOES_CAM_EXIST(hCamera)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(hCamera)
	ENDIF
	
	IF DOES_ENTITY_EXIST(hFriend)
		DELETE_PED(hFriend)
	ENDIF

	// Unload resources
	REMOVE_ANIM_DICT(tAnimDict)
	
	// Restore control
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	DO_SCREEN_FADE_IN(500)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)

ENDPROC


PROC PRIVATE_DEBUG_AddFriendsForPlayer()
			
	CLEAR_HELP()
	
	SWITCH g_eDefaultPlayerChar
		CASE CHAR_MICHAEL
			PRINT_HELP("FR_00a")		//Call Franklin or Trevor to organise an activity.
			Add_Char_As_DefaultPlayer_Friend(CHAR_FRANKLIN)
			Add_Char_As_DefaultPlayer_Friend(CHAR_TREVOR)
		BREAK
		
		CASE CHAR_FRANKLIN
			PRINT_HELP("FR_00b")		//Call Trevor or Michael to organise an activity.
			Add_Char_As_DefaultPlayer_Friend(CHAR_TREVOR)
			Add_Char_As_DefaultPlayer_Friend(CHAR_MICHAEL)
		BREAK
		
		CASE CHAR_TREVOR
			PRINT_HELP("FR_00c")		//Call Michael or Franklin to organise an activity.
			Add_Char_As_DefaultPlayer_Friend(CHAR_MICHAEL)
			Add_Char_As_DefaultPlayer_Friend(CHAR_FRANKLIN)
		BREAK
		
		DEFAULT
			//
		BREAK
	ENDSWITCH

ENDPROC


FUNC BOOL PRIVATE_DEBUG_ForceFriendActivityWithAnyone()
	// Get a random friend connection...
	enumFriendConnection eNewConn
	eNewConn = INT_TO_ENUM(enumFriendConnection, GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(MAX_FRIEND_CONNECTIONS)))
	
	// If player is one of the friends in the connection, get the other one...
	enumFriend eNewFriend
	IF GET_OTHER_FRIEND_FROM_CONNECTION(eNewConn, eNewFriend)
		
		enumCharacterList eNewFriendChar = GET_CHAR_FROM_FRIEND(eNewFriend)
		
		IF GET_CONNECTION_STATE(eNewConn) = FC_STATE_Invalid
			Add_Char_As_DefaultPlayer_Friend(eNewFriendChar)
		ENDIF
		
		IF GET_CONNECTION_STATE(eNewConn) = FC_STATE_ContactWait
			
			CPRINTLN(DEBUG_FRIENDS, "PRIVATE_DEBUG_ForceFriendActivityWithAnyone(): Force with ", GetLabel_enumFriend(eNewFriend))
			SET_CONNECTION_FLAG(eNewConn, FC_FLAG_HasCallConnected)
			SET_CONNECTION_STATE(eNewConn, FC_STATE_PhoneAccept)
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC PRIVATE_DEBUG_CheckForMissingFriendDialogue()

	BOOL bOpenedDebugFile = FALSE
	enumFriendAudioBlock eAudioBlock = FAB_phone
	TEXT_LABEL tAudioBlockLabel
	BOOL bSaveAudioBlock = FALSE
	
	// For each friend connection...
	enumFriendConnection thisConnID
	REPEAT MAX_FRIEND_CONNECTIONS thisConnID
		
		enumFriend playerID = g_SavedGlobals.sFriendsData.g_FriendConnectData[thisConnID].friendA
		enumFriend friendID = g_SavedGlobals.sFriendsData.g_FriendConnectData[thisConnID].friendB
		
		// Check FriendPhoneConvs...
		eAudioBlock = FAB_phone
		tAudioBlockLabel = PRIVATE_FriendDialogue_GetAudioCode(playerID, friendID, NO_FRIEND, eAudioBlock)
		tAudioBlockLabel += "AU"
		DEBUG_RequestAndWaitAdditionalText(tAudioBlockLabel, PHONE_TEXT_SLOT)
		
		// Check phone conv's friendA -> friendB
		enumFriendPhonePhrase eCellConv
		REPEAT MAX_FRIEND_PHONE_PHRASE eCellConv

			TEXT_LABEL str = ""
			Private_GetFriendPhonePhrase(GET_CHAR_FROM_FRIEND(playerID), GET_CHAR_FROM_FRIEND(friendID), eCellConv, tAudioBlockLabel, str)
			
			IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED(tAudioBlockLabel, PHONE_TEXT_SLOT)
				DEBUG_RequestAndWaitAdditionalText(tAudioBlockLabel, PHONE_TEXT_SLOT)
				bSaveAudioBlock = FALSE
			ENDIF
			
			TEXT_LABEL tDialogueFile = str
			tDialogueFile += "_01"
			IF NOT DOES_TEXT_LABEL_EXIST(tDialogueFile)
				TEXT_LABEL_63 tDescription, tDialogue
				tDescription 	= GetLabel_enumFriend(playerID)
				tDescription   += " "
				tDescription   += GetLabel_enumFriendPhonePhrase(eCellConv)
				tDescription   += " - "
				tDialogue		= "ENTER PHOLDER DIALOG FOR "
				tDialogue	   += GetLabel_enumFriendPhonePhrase(eCellConv)
				
				IF NOT bOpenedDebugFile
					OPEN_DEBUG_FILE()
					bOpenedDebugFile = TRUE
				ENDIF
				
				IF NOT bSaveAudioBlock
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("	block: \"")
					SAVE_STRING_TO_DEBUG_FILE(tAudioBlockLabel)
					SAVE_STRING_TO_DEBUG_FILE("\"")
					SAVE_NEWLINE_TO_DEBUG_FILE()
					
					bSaveAudioBlock = TRUE
				ENDIF
				
				SAVE_DIALOGUESTAR_LINE_TO_DEBUG_FILE(str,
						tDescription,
						GET_CHAR_FROM_FRIEND(playerID),
						tDialogue, TRUE)
			ENDIF
		ENDREPEAT
		// Check phone conv's friendB -> friendA
//		REPEAT MAX_FRIEND_PHONE_PHRASE eCellConv
//
//			TEXT_LABEL str = ""
//			Private_GetFriendPhonePhrase(playerID, friendID, eCellConv, tAudioBlockLabel, str)
//			
//			IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED(tAudioBlockLabel, PHONE_TEXT_SLOT)
//				DEBUG_RequestAndWaitAdditionalText(tAudioBlockLabel, PHONE_TEXT_SLOT)
//				bSaveAudioBlock = FALSE
//			ENDIF
//			
//			TEXT_LABEL tDialogueFile = str
//			tDialogueFile += "_01"
//			IF NOT DOES_TEXT_LABEL_EXIST(tDialogueFile)
//				TEXT_LABEL_63 tDescription, tDialogue
//				tDescription 	= GetLabel_enumFriend(friendID)
//				tDescription   += " "
//				tDescription   += GetLabel_enumFriendPhonePhrase(eCellConv)
//				tDescription   += " - "
//				tDialogue		= "ENTER PHOLDER DIALOG FOR "
//				tDialogue	   += GetLabel_enumFriendPhonePhrase(eCellConv)
//				
//				IF NOT bOpenedDebugFile
//					OPEN_DEBUG_FILE()
//					bOpenedDebugFile = TRUE
//				ENDIF
//				
//				SAVE_DIALOGUESTAR_LINE_TO_DEBUG_FILE(str,
//						tDescription,
//						GET_CHAR_FROM_FRIEND(friendID),
//						tDialogue, TRUE)
//			ENDIF
//		ENDREPEAT
		
		// Check FriendActivityConvs...
		eAudioBlock = FAB_activity
		tAudioBlockLabel = PRIVATE_FriendDialogue_GetAudioCode(playerID, friendID, NO_FRIEND, eAudioBlock)
		tAudioBlockLabel += "AU"
		DEBUG_RequestAndWaitAdditionalText(tAudioBlockLabel, PHONE_TEXT_SLOT)
		bSaveAudioBlock = FALSE
		
		// Check activity conv's friendA -> friendB
		enumFriendActivityPhrase eActivityConv
		REPEAT MAX_FRIEND_ACTIVITY_PHRASES eActivityConv
			
			TEXT_LABEL str = ""
			Private_GetFriendActivityPhrase(GET_CHAR_FROM_FRIEND(playerID), GET_CHAR_FROM_FRIEND(friendID), eActivityConv, tAudioBlockLabel, str)

			IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED(tAudioBlockLabel, PHONE_TEXT_SLOT)
				DEBUG_RequestAndWaitAdditionalText(tAudioBlockLabel, PHONE_TEXT_SLOT)
				bSaveAudioBlock = FALSE
			ENDIF
			
			TEXT_LABEL tDialogueFile = str
			tDialogueFile += "_01"
			IF NOT DOES_TEXT_LABEL_EXIST(tDialogueFile)
				TEXT_LABEL_63 tDescription, tDialogue
				tDescription 	= GetLabel_enumFriend(playerID)
				tDescription   += " "
				tDescription   += GetLabel_enumFriendActivityPhrase(eActivityConv)
				tDescription   += " - "
				tDialogue		= "ENTER PHOLDER DIALOG FOR "
				tDialogue	   += GetLabel_enumFriendActivityPhrase(eActivityConv)
				
				IF NOT bOpenedDebugFile
					OPEN_DEBUG_FILE()
					bOpenedDebugFile = TRUE
				ENDIF
				
				IF NOT bSaveAudioBlock
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("	block: \"")
					SAVE_STRING_TO_DEBUG_FILE(tAudioBlockLabel)
					SAVE_STRING_TO_DEBUG_FILE("\"")
					SAVE_NEWLINE_TO_DEBUG_FILE()
					
					bSaveAudioBlock = TRUE
				ENDIF
				
				SAVE_DIALOGUESTAR_LINE_TO_DEBUG_FILE(str,
						tDescription,
						GET_CHAR_FROM_FRIEND(playerID),
						tDialogue, FALSE)
			ENDIF
		ENDREPEAT
		// Check activity conv's friendB -> friendA
//		REPEAT MAX_FRIEND_ACTIVITY_PHRASES eActivityConv
//			
//			TEXT_LABEL str = ""
//			Private_GetFriendActivityPhrase(GET_CHAR_FROM_FRIEND(friendID), GET_CHAR_FROM_FRIEND(playerID), eActivityConv, tAudioBlockLabel, str)
//
//			IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED(tAudioBlockLabel, PHONE_TEXT_SLOT)
//				DEBUG_RequestAndWaitAdditionalText(tAudioBlockLabel, PHONE_TEXT_SLOT)
//				bSaveAudioBlock = FALSE
//			ENDIF
//			
//			TEXT_LABEL tDialogueFile = str
//			tDialogueFile += "_01"
//			IF NOT DOES_TEXT_LABEL_EXIST(tDialogueFile)
//				TEXT_LABEL_63 tDescription, tDialogue
//				tDescription 	= GetLabel_enumFriend(friendID)
//				tDescription   += " "
//				tDescription   += GetLabel_enumFriendActivityPhrase(eActivityConv)
//				tDescription   += " - "
//				tDialogue		= "ENTER PHOLDER DIALOG FOR "
//				tDialogue	   += GetLabel_enumFriendActivityPhrase(eActivityConv)
//				
//				IF NOT bOpenedDebugFile
//					OPEN_DEBUG_FILE()
//					bOpenedDebugFile = TRUE
//				ENDIF
//				
//				SAVE_DIALOGUESTAR_LINE_TO_DEBUG_FILE(str,
//						tDescription,
//						GET_CHAR_FROM_FRIEND(friendID),
//						tDialogue, FALSE)
//			ENDIF
//		ENDREPEAT

		// Check FriendTextMessages...
		tAudioBlockLabel	= "FRIENDS"
		DEBUG_RequestAndWaitAdditionalText(tAudioBlockLabel, PHONE_TEXT_SLOT)
		CONST_INT		iMAX_TEXT_MESSAGE_COUNT	2//3
		
		enumFriendTextMessage eTxtMsg
		REPEAT MAX_FRIEND_TEXT_MESSAGES eTxtMsg
			tAudioBlockLabel = "FRIENDS"
			INT iTxtMsg
			REPEAT iMAX_TEXT_MESSAGE_COUNT iTxtMsg
				TEXT_LABEL str = ""
				Private_GetFriendTextMessageLabel(playerID, eTxtMsg, iTxtMsg, str)
				
				IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED(tAudioBlockLabel, PHONE_TEXT_SLOT)
					DEBUG_RequestAndWaitAdditionalText(tAudioBlockLabel, PHONE_TEXT_SLOT)
					bSaveAudioBlock = FALSE
				ENDIF
				
				IF NOT DOES_TEXT_LABEL_EXIST(str)
					TEXT_LABEL_63 tMessage
					tMessage	= GetLabel_enumFriend(playerID)
					tMessage	+= ", it's "
					tMessage	+= GetLabel_enumFriend(friendID)
					tMessage	+= ". msg for \""
					tMessage	+= GetLabel_enumFriendTextMessage(eTxtMsg)
					tMessage	+= "\" - "
					tMessage	+= iTxtMsg
					
					IF NOT bOpenedDebugFile
						OPEN_DEBUG_FILE()
						bOpenedDebugFile = TRUE
					ENDIF
					
					SAVE_STRING_TO_DEBUG_FILE("[")SAVE_STRING_TO_DEBUG_FILE(str)SAVE_STRING_TO_DEBUG_FILE(":")SAVE_STRING_TO_DEBUG_FILE(tAudioBlockLabel)SAVE_STRING_TO_DEBUG_FILE("]")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE(tMessage)SAVE_NEWLINE_TO_DEBUG_FILE()
					
				ENDIF
			ENDREPEAT
		ENDREPEAT
		REPEAT MAX_FRIEND_TEXT_MESSAGES eTxtMsg
			tAudioBlockLabel = "FRIENDS"	//PRIVATE_Get_FriendConnection_textBlock_Label(thisConnID, btextDialogue)
			INT iTxtMsg
			REPEAT iMAX_TEXT_MESSAGE_COUNT iTxtMsg
				TEXT_LABEL str = ""
				Private_GetFriendTextMessageLabel(friendID, eTxtMsg, iTxtMsg, str)
				
				IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED(tAudioBlockLabel, PHONE_TEXT_SLOT)
					DEBUG_RequestAndWaitAdditionalText(tAudioBlockLabel, PHONE_TEXT_SLOT)
					bSaveAudioBlock = FALSE
				ENDIF
				
				IF NOT DOES_TEXT_LABEL_EXIST(str)
					TEXT_LABEL_63  tMessage
					tMessage	= GetLabel_enumFriend(friendID)
					tMessage	+= ", it's "
					tMessage	+= GetLabel_enumFriend(playerID)
					tMessage	+= ". msg for \""
					tMessage	+= GetLabel_enumFriendTextMessage(eTxtMsg)
					tMessage	+= "\" - "
					tMessage	+= iTxtMsg
					
					IF NOT bOpenedDebugFile
						OPEN_DEBUG_FILE()
						bOpenedDebugFile = TRUE
					ENDIF
					
					SAVE_STRING_TO_DEBUG_FILE("[")SAVE_STRING_TO_DEBUG_FILE(str)SAVE_STRING_TO_DEBUG_FILE(":")SAVE_STRING_TO_DEBUG_FILE(tAudioBlockLabel)SAVE_STRING_TO_DEBUG_FILE("]")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE(tMessage)SAVE_NEWLINE_TO_DEBUG_FILE()
					
				ENDIF
			ENDREPEAT
		ENDREPEAT
		
	ENDREPEAT
	
	IF bOpenedDebugFile
		CLOSE_DEBUG_FILE()
		bOpenedDebugFile = FALSE
	ELSE
		CPRINTLN(DEBUG_FRIENDS, "no missing friend dialogue :)")
	ENDIF
				
ENDPROC


PROC PRIVATE_DEBUG_TestFriendPhoneDialogue(WIDGET_GROUP_ID& hParentWidgetGroup, BOOL& bTestFriendPhonecalls)
	
	INT iTestSpeaker, iTestListener, iTestConn, iTestConv
	TEXT_WIDGET_ID label_textWidget, block_textWidget, speech_textWidget
	
	SET_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
	WIDGET_GROUP_ID testFriendPhonecalls_widget = START_WIDGET_GROUP("testFriendPhonecalls")
		ADD_WIDGET_BOOL("bTestFriendPhonecalls", bTestFriendPhonecalls)
		
		enumFriend eFri
		START_NEW_WIDGET_COMBO()
			REPEAT MAX_FRIENDS eFri
				ADD_TO_WIDGET_COMBO(GetLabel_enumFriend(eFri))
			ENDREPEAT
		STOP_WIDGET_COMBO("iTestSpeaker", iTestSpeaker)
		START_NEW_WIDGET_COMBO()
			REPEAT MAX_FRIENDS eFri
				ADD_TO_WIDGET_COMBO(GetLabel_enumFriend(eFri))
			ENDREPEAT
		STOP_WIDGET_COMBO("iTestListener", iTestListener)
		START_NEW_WIDGET_COMBO()
			enumFriendConnection eConn
			REPEAT MAX_FRIEND_CONNECTIONS eConn
				ADD_TO_WIDGET_COMBO(GetLabel_enumFriendConnection(eConn))
			ENDREPEAT
		STOP_WIDGET_COMBO("iTestConn", iTestConn)
		
		START_NEW_WIDGET_COMBO()
			enumFriendPhonePhrase eConv
			REPEAT MAX_FRIEND_PHONE_PHRASE eConv
				ADD_TO_WIDGET_COMBO(GetLabel_enumFriendPhonePhrase(eConv))
			ENDREPEAT
		STOP_WIDGET_COMBO("iTestConv", iTestConv)
		
		block_textWidget = ADD_TEXT_WIDGET("block")
		label_textWidget = ADD_TEXT_WIDGET("label")
		speech_textWidget = ADD_TEXT_WIDGET("speech")
	STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
	
	enumFriend eThisSpeaker, eThisListener
	eThisSpeaker = GET_FRIEND_FROM_CHAR(GET_CURRENT_PLAYER_PED_ENUM())
	eThisListener = INT_TO_ENUM(enumFriend, ENUM_TO_INT(eThisSpeaker)+1)
	
	enumFriendConnection eThisConn
	eThisConn = GET_CONNECTION_FROM_FRIENDS(eThisSpeaker, eThisListener)
	
	enumFriendPhonePhrase eThisConv
	
	iTestSpeaker	= ENUM_TO_INT(eThisSpeaker)
	iTestListener	= ENUM_TO_INT(eThisListener)
	iTestConn		= ENUM_TO_INT(eThisConn)
	iTestConv		= ENUM_TO_INT(eThisConv)
	
	WHILE bTestFriendPhonecalls
	
		IF (eThisSpeaker	<> INT_TO_ENUM(enumFriend, iTestSpeaker))
			eThisSpeaker	= INT_TO_ENUM(enumFriend, iTestSpeaker)
			
			IF (eThisListener = eThisSpeaker)
				eThisListener = INT_TO_ENUM(enumFriend, ENUM_TO_INT(eThisSpeaker)+1)
				iTestListener = ENUM_TO_INT(eThisListener)
			ENDIF
			
			IF eThisSpeaker = FR_LAMAR
				IF (eThisListener <> FR_FRANKLIN)
					eThisListener = FR_FRANKLIN
					iTestListener = ENUM_TO_INT(eThisListener)
				ENDIF
			ENDIF
		ENDIF
		IF (eThisListener	<> INT_TO_ENUM(enumFriend, iTestListener))
			eThisListener	= INT_TO_ENUM(enumFriend, iTestListener)
			
			IF (eThisSpeaker = eThisListener)
				eThisSpeaker = INT_TO_ENUM(enumFriend, ENUM_TO_INT(eThisListener)+1)
				iTestSpeaker = ENUM_TO_INT(eThisSpeaker)
			ENDIF
			
			IF eThisListener = FR_LAMAR
				IF (eThisSpeaker <> FR_FRANKLIN)
					eThisSpeaker = FR_FRANKLIN
					iTestSpeaker = ENUM_TO_INT(eThisSpeaker)
				ENDIF
			ENDIF
		ENDIF
		
		eThisConn			= GET_CONNECTION_FROM_FRIENDS(eThisSpeaker, eThisListener)
		iTestConn			= ENUM_TO_INT(eThisConn)
		
		IF (eThisConv		<> INT_TO_ENUM(enumFriendPhonePhrase, iTestConv))
			eThisConv		= INT_TO_ENUM(enumFriendPhonePhrase, iTestConv)
		ENDIF
		
		TEXT_LABEL tBlock = ""
		TEXT_LABEL tLabel = ""
		Private_GetFriendPhonePhrase(GET_CHAR_FROM_FRIEND(eThisSpeaker), GET_CHAR_FROM_FRIEND(eThisListener), eThisConv, tBlock, tLabel)
		
		SET_CONTENTS_OF_TEXT_WIDGET(block_textWidget, tBlock)
		SET_CONTENTS_OF_TEXT_WIDGET(label_textWidget, tLabel)
		
		IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED(tBlock, PHONE_TEXT_SLOT)
			DEBUG_RequestAndWaitAdditionalText(tBlock, PHONE_TEXT_SLOT)
		ENDIF
		
		TEXT_LABEL tDialogueFile = tLabel
		tDialogueFile += "_01"
		STRING sSpeech = GET_STRING_FROM_TEXT_FILE(tDialogueFile)
		SET_CONTENTS_OF_TEXT_WIDGET(speech_textWidget, sSpeech)
		
		WAIT(0)
	ENDWHILE
	
	IF DOES_WIDGET_GROUP_EXIST(testFriendPhonecalls_widget)
		DELETE_WIDGET_GROUP(testFriendPhonecalls_widget)
	ENDIF
ENDPROC


PROC PRIVATE_DEBUG_TestFriendActivityDialogue(WIDGET_GROUP_ID& hParentWidgetGroup, BOOL& bTestFriendContexts)
//				enumFriendAudioBlock bContext = FAB_a_cellp""hone
				
	INT iTestSpeaker, iTestListener, iTestConn, iTestConv
	TEXT_WIDGET_ID label_textWidget, block_textWidget, speech_textWidget
	
	SET_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
	WIDGET_GROUP_ID testFriendContexts_widget = START_WIDGET_GROUP("testFriendContexts")
		ADD_WIDGET_BOOL("bTestFriendContexts", bTestFriendContexts)
		
		enumFriend eFri
		START_NEW_WIDGET_COMBO()
			REPEAT MAX_FRIENDS eFri
				ADD_TO_WIDGET_COMBO(GetLabel_enumFriend(eFri))
			ENDREPEAT
		STOP_WIDGET_COMBO("iTestSpeaker", iTestSpeaker)
		START_NEW_WIDGET_COMBO()
			REPEAT MAX_FRIENDS eFri
				ADD_TO_WIDGET_COMBO(GetLabel_enumFriend(eFri))
			ENDREPEAT
		STOP_WIDGET_COMBO("iTestListener", iTestListener)
		START_NEW_WIDGET_COMBO()
			enumFriendConnection eConn
			REPEAT MAX_FRIEND_CONNECTIONS eConn
				ADD_TO_WIDGET_COMBO(GetLabel_enumFriendConnection(eConn))
			ENDREPEAT
		STOP_WIDGET_COMBO("iTestConn", iTestConn)
		
		START_NEW_WIDGET_COMBO()
			enumFriendActivityPhrase eConv
			REPEAT MAX_FRIEND_ACTIVITY_PHRASES eConv
				ADD_TO_WIDGET_COMBO(GetLabel_enumFriendActivityPhrase(eConv))
			ENDREPEAT
		STOP_WIDGET_COMBO("iTestConv", iTestConv)
		
		block_textWidget = ADD_TEXT_WIDGET("block")
		label_textWidget = ADD_TEXT_WIDGET("label")
		speech_textWidget = ADD_TEXT_WIDGET("speech")
	STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
	
	enumFriend eThisSpeaker, eThisListener
	eThisSpeaker = GET_FRIEND_FROM_CHAR(GET_CURRENT_PLAYER_PED_ENUM())
	eThisListener = INT_TO_ENUM(enumFriend, ENUM_TO_INT(eThisSpeaker)+1)
	
	enumFriendConnection eThisConn
	eThisConn = GET_CONNECTION_FROM_FRIENDS(eThisSpeaker, eThisListener)
	
	enumFriendActivityPhrase eThisConv
	
	iTestSpeaker	= ENUM_TO_INT(eThisSpeaker)
	iTestListener	= ENUM_TO_INT(eThisListener)
	iTestConn		= ENUM_TO_INT(eThisConn)
	iTestConv		= ENUM_TO_INT(eThisConv)
	
	WHILE bTestFriendContexts
	
		IF (eThisSpeaker	<> INT_TO_ENUM(enumFriend, iTestSpeaker))
			eThisSpeaker	= INT_TO_ENUM(enumFriend, iTestSpeaker)
			
			IF (eThisListener = eThisSpeaker)
				eThisListener = INT_TO_ENUM(enumFriend, ENUM_TO_INT(eThisSpeaker)+1)
				iTestListener = ENUM_TO_INT(eThisListener)
			ENDIF
			
			IF eThisSpeaker = FR_LAMAR
				IF (eThisListener <> FR_FRANKLIN)
					eThisListener = FR_FRANKLIN
					iTestListener = ENUM_TO_INT(eThisListener)
				ENDIF
			ENDIF
		ENDIF
		IF (eThisListener	<> INT_TO_ENUM(enumFriend, iTestListener))
			eThisListener	= INT_TO_ENUM(enumFriend, iTestListener)
			
			IF (eThisSpeaker = eThisListener)
				eThisSpeaker = INT_TO_ENUM(enumFriend, ENUM_TO_INT(eThisListener)+1)
				iTestSpeaker = ENUM_TO_INT(eThisSpeaker)
			ENDIF
			
			IF eThisListener = FR_LAMAR
				IF (eThisSpeaker <> FR_FRANKLIN)
					eThisSpeaker = FR_FRANKLIN
					iTestSpeaker = ENUM_TO_INT(eThisSpeaker)
				ENDIF
			ENDIF
		ENDIF
		
		eThisConn			= GET_CONNECTION_FROM_FRIENDS(eThisSpeaker, eThisListener)
		iTestConn			= ENUM_TO_INT(eThisConn)
		
		IF (eThisConv		<> INT_TO_ENUM(enumFriendActivityPhrase, iTestConv))
			eThisConv		= INT_TO_ENUM(enumFriendActivityPhrase, iTestConv)
		ENDIF
		
		TEXT_LABEL tBlock = ""
		TEXT_LABEL tLabel = ""
		Private_GetFriendActivityPhrase(GET_CHAR_FROM_FRIEND(eThisSpeaker), GET_CHAR_FROM_FRIEND(eThisListener), eThisConv, tBlock, tLabel)
		
		SET_CONTENTS_OF_TEXT_WIDGET(block_textWidget, tBlock)
		SET_CONTENTS_OF_TEXT_WIDGET(label_textWidget, tLabel)
		
		IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED(tBlock, PHONE_TEXT_SLOT)
			DEBUG_RequestAndWaitAdditionalText(tBlock, PHONE_TEXT_SLOT)
		ENDIF
		
		TEXT_LABEL tDialogueFile = tLabel
		tDialogueFile += "_01"
		STRING sSpeech = GET_STRING_FROM_TEXT_FILE(tDialogueFile)
		SET_CONTENTS_OF_TEXT_WIDGET(speech_textWidget, sSpeech)
		
		WAIT(0)
	ENDWHILE
	
	IF DOES_WIDGET_GROUP_EXIST(testFriendContexts_widget)
		DELETE_WIDGET_GROUP(testFriendContexts_widget)
	ENDIF

ENDPROC


PROC PRIVATE_DEBUG_TestFriendTextMessages(WIDGET_GROUP_ID& hParentWidgetGroup, BOOL& bTestFriendTextMessages)
//				enumFriendAudioBlock eAudioBlock = FAB_b
				
	INT iTestSpeaker, iTestListener, iTestConn, iTestConv, iTestMsgCount = -1
	TEXT_WIDGET_ID label_textWidget, block_textWidget, message_textWidget
	
	SET_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
	WIDGET_GROUP_ID testFriendtextmessages_widget = START_WIDGET_GROUP("testFriendtextmessages")
		ADD_WIDGET_BOOL("bTestFriendTextMessages", bTestFriendTextMessages)
		
		enumFriend eFri
		START_NEW_WIDGET_COMBO()
			REPEAT MAX_FRIENDS eFri
				ADD_TO_WIDGET_COMBO(GetLabel_enumFriend(eFri))
			ENDREPEAT
		STOP_WIDGET_COMBO("iTestSpeaker", iTestSpeaker)
		START_NEW_WIDGET_COMBO()
			REPEAT MAX_FRIENDS eFri
				ADD_TO_WIDGET_COMBO(GetLabel_enumFriend(eFri))
			ENDREPEAT
		STOP_WIDGET_COMBO("iTestListener", iTestListener)
		START_NEW_WIDGET_COMBO()
			enumFriendConnection eConn
			REPEAT MAX_FRIEND_CONNECTIONS eConn
				ADD_TO_WIDGET_COMBO(GetLabel_enumFriendConnection(eConn))
			ENDREPEAT
		STOP_WIDGET_COMBO("iTestConn", iTestConn)
		
		START_NEW_WIDGET_COMBO()
			enumFriendTextMessage eTxt
			REPEAT MAX_FRIEND_TEXT_MESSAGES eTxt
				ADD_TO_WIDGET_COMBO(GetLabel_enumFriendTextMessage(eTxt))
			ENDREPEAT
		STOP_WIDGET_COMBO("iTestConv", iTestConv)
		
		ADD_WIDGET_INT_SLIDER("iTestMsgCount", iTestMsgCount, -1, 10, 1)
		
		label_textWidget = ADD_TEXT_WIDGET("label")
		block_textWidget = ADD_TEXT_WIDGET("block")
		message_textWidget = ADD_TEXT_WIDGET("message")
	STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
	
	enumFriend eThisSpeaker, eThisListener
	eThisSpeaker = GET_FRIEND_FROM_CHAR(GET_CURRENT_PLAYER_PED_ENUM())
	eThisListener = INT_TO_ENUM(enumFriend, ENUM_TO_INT(eThisSpeaker)+1)
	
	enumFriendConnection eThisConn
	eThisConn = GET_CONNECTION_FROM_FRIENDS(eThisSpeaker, eThisListener)
	
	enumFriendTextMessage eThisTxt
	
	iTestSpeaker	= ENUM_TO_INT(eThisSpeaker)
	iTestListener	= ENUM_TO_INT(eThisListener)
	iTestConn		= ENUM_TO_INT(eThisConn)
	iTestConv		= ENUM_TO_INT(eThisTxt)
	
	BOOL bReloadBlock = TRUE
	WHILE bTestFriendTextMessages
		
		IF (eThisSpeaker	<> INT_TO_ENUM(enumFriend, iTestSpeaker))
			eThisSpeaker	= INT_TO_ENUM(enumFriend, iTestSpeaker)
			bReloadBlock = TRUE
			
			IF (eThisListener = eThisSpeaker)
				eThisListener = INT_TO_ENUM(enumFriend, ENUM_TO_INT(eThisSpeaker)+1)
				iTestListener = ENUM_TO_INT(eThisListener)
			ENDIF
			
			IF eThisSpeaker = FR_LAMAR
				IF (eThisListener <> FR_FRANKLIN)
					eThisListener = FR_FRANKLIN
					iTestListener = ENUM_TO_INT(eThisListener)
				ENDIF
			ENDIF
		ENDIF
		IF (eThisListener	<> INT_TO_ENUM(enumFriend, iTestListener))
			eThisListener	= INT_TO_ENUM(enumFriend, iTestListener)
			bReloadBlock = TRUE
			
			IF (eThisSpeaker = eThisListener)
				eThisSpeaker = INT_TO_ENUM(enumFriend, ENUM_TO_INT(eThisListener)+1)
				iTestSpeaker = ENUM_TO_INT(eThisSpeaker)
			ENDIF
			
			IF eThisListener = FR_LAMAR
				IF (eThisSpeaker <> FR_FRANKLIN)
					eThisSpeaker = FR_FRANKLIN
					iTestSpeaker = ENUM_TO_INT(eThisSpeaker)
				ENDIF
			ENDIF
		ENDIF
		
		eThisConn			= GET_CONNECTION_FROM_FRIENDS(eThisSpeaker, eThisListener)
		iTestConn			= ENUM_TO_INT(eThisConn)
		
		IF (eThisTxt		<> INT_TO_ENUM(enumFriendTextMessage, iTestConv))
			eThisTxt		= INT_TO_ENUM(enumFriendTextMessage, iTestConv)
			bReloadBlock	= TRUE
		ENDIF
		
		TEXT_LABEL tBlock = "FRIENDS"		//PRIVATE_Friend_GetAudioBlock(eThisConn, FAB_chat)
		TEXT_LABEL tLabel = ""
		Private_GetFriendTextMessageLabel(eThisSpeaker, eThisTxt, iTestMsgCount, tLabel)
		
		IF bReloadBlock
			DEBUG_RequestAndWaitAdditionalText(tBlock, PHONE_TEXT_SLOT)
			bReloadBlock = FALSE
		ENDIF
		
		STRING sMessage = GET_STRING_FROM_TEXT_FILE(tLabel)
		
		SET_CONTENTS_OF_TEXT_WIDGET(block_textWidget, tBlock)
		SET_CONTENTS_OF_TEXT_WIDGET(label_textWidget, tLabel)
		SET_CONTENTS_OF_TEXT_WIDGET(message_textWidget, sMessage)
		
		WAIT(0)
	ENDWHILE
	
	IF DOES_WIDGET_GROUP_EXIST(testFriendtextmessages_widget)
		DELETE_WIDGET_GROUP(testFriendtextmessages_widget)
	ENDIF

ENDPROC

/*
PROC PRIVATE_DEBUG_TestAllLamarDialogue(WIDGET_GROUP_ID& hParentWidgetGroup, BOOL& bTestAllLamarDialogue)

	structPedsForConversation inSpeechStruct
	
	BOOL bFriendConvBit_toggle[3], bTestThisLabel
	TEXT_WIDGET_ID label_textWidget, block_textWidget, speech_textWidget
	
	enumFriendConnection eConnect = FC_FRANKLIN_LAMAR
	TEXT_LABEL tBlock = "FRLFbAU"
	
//	UPDATE_FRIEND_CONVERSATION_STAGE(CHAR_FRANKLIN, CHAR_LAMAR, FGS_stageEarly)
	
	SET_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
	WIDGET_GROUP_ID testAllLamarDialogue_widget = START_WIDGET_GROUP("testAllLamarDialogue")
		
		START_WIDGET_GROUP("convStruct")
			ADD_WIDGET_INT_SLIDER("iMainConv", g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnect].convStruct.iMainConv,	0, 10, 1)
			ADD_WIDGET_INT_SLIDER("iMiniConv", g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnect].convStruct.iMiniConv,	0, 10, 1)
			ADD_WIDGET_INT_SLIDER("iDrunkConv", g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnect].convStruct.iDrunkConv,0, 10, 1)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("enumFriendConvBit")
			ADD_WIDGET_BOOL("FGS_stageEarly", bFriendConvBit_toggle[0])
			ADD_WIDGET_BOOL("FGS_stageMid", bFriendConvBit_toggle[1])
			ADD_WIDGET_BOOL("FGS_stageEnd", bFriendConvBit_toggle[2])
			
//						ADD_WIDGET_BOOL("FC_invalid", bFriendConvBit_toggle[5])
		STOP_WIDGET_GROUP()
		
		block_textWidget = ADD_TEXT_WIDGET("block")
		label_textWidget = ADD_TEXT_WIDGET("label")
		speech_textWidget = ADD_TEXT_WIDGET("speech")
		
		ADD_WIDGET_BOOL("bTestThisLabel", bTestThisLabel)
	STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(hParentWidgetGroup)
	
	WHILE bTestAllLamarDialogue
		//FC_stageEarly
		IF bFriendConvBit_toggle[0]
			g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnect].convStruct.eGameStage = FGS_stageEarly
			bFriendConvBit_toggle[0] = FALSE
		ENDIF
		
		//FC_stageMid
		IF bFriendConvBit_toggle[1]
			g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnect].convStruct.eGameStage = FGS_stageMid
			bFriendConvBit_toggle[1] = FALSE
		ENDIF
		
		//FC_stageLate
		IF bFriendConvBit_toggle[2]
			g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnect].convStruct.eGameStage = FGS_stageLate
			bFriendConvBit_toggle[2] = FALSE
		ENDIF
			
		TEXT_LABEL tLabel = "FR_LF_"
//		BOOL bFoundstateBit = FALSE
//		IF IS_BITMASK_AS_ENUM_SET(g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnect].convStruct.stateBit, FGS_stageEarly)
//			bFoundstateBit = TRUE
//			tLabel += "E"
//		ENDIF
//		IF IS_BITMASK_AS_ENUM_SET(g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnect].convStruct.stateBit, FGS_stageMid)
//			bFoundstateBit = TRUE
//			tLabel += "M"
//		ENDIF
//		IF IS_BITMASK_AS_ENUM_SET(g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnect].convStruct.stateBit, FGS_stageLate)
//			bFoundstateBit = TRUE
//			tLabel += "L"
//		ENDIF
//		IF NOT bFoundstateBit
//			tLabel += "X"
//		ENDIF
//		
//		BOOL bFoundType = FALSE
//		IF IS_BITMASK_AS_ENUM_SET(g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnect].convStruct.stateBit, FC_typeConv)
//			bFoundType = TRUE
//			tLabel += "C"
//		ENDIF
//		IF IS_BITMASK_AS_ENUM_SET(g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnect].convStruct.stateBit, FC_typeMini)
//			bFoundType = TRUE
//			tLabel += "M"
//		ENDIF
//		IF NOT bFoundType
//			tLabel += "X"
//		ENDIF
//		
//		tLabel += g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnect].convStruct.count
		
		
		IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED(tBlock, PHONE_TEXT_SLOT)
			DEBUG_RequestAndWaitAdditionalText(tBlock, PHONE_TEXT_SLOT)
		ENDIF
		
		SET_CONTENTS_OF_TEXT_WIDGET(block_textWidget, tBlock)
		SET_CONTENTS_OF_TEXT_WIDGET(label_textWidget, tLabel)
		
		TEXT_LABEL tDialogueFile_1 = tLabel
		tDialogueFile_1 += "_1"
		
		IF DOES_TEXT_LABEL_EXIST(tDialogueFile_1)
			STRING sSpeech_1 = GET_STRING_FROM_TEXT_FILE(tDialogueFile_1)
			SET_CONTENTS_OF_TEXT_WIDGET(speech_textWidget, sSpeech_1)
			
			IF bTestThisLabel
				ADD_PED_FOR_DIALOGUE(inSpeechStruct, 0, NULL, "LAMAR")
				ADD_PED_FOR_DIALOGUE(inSpeechStruct, 1, NULL, "FRANKLIN")
				
				KILL_ANY_CONVERSATION()
				IF CREATE_CONVERSATION(inSpeechStruct, tBlock, tLabel,
						CONV_PRIORITY_AMBIENT_MEDIUM)
					bTestThisLabel = FALSE
				ENDIF
			ENDIF
			
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(speech_textWidget, "NONE")
			bTestThisLabel = FALSE
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	IF DOES_WIDGET_GROUP_EXIST(testAllLamarDialogue_widget)
		DELETE_WIDGET_GROUP(testAllLamarDialogue_widget)
	ENDIF

ENDPROC
*/
PROC PRIVATE_DEBUG_PrintInvolvedFriendsList()

	CPRINTLN(DEBUG_FRIENDS, "Printing full mission \"involved friends\" list to temp_debug.txt")
	OPEN_DEBUG_FILE()

	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("Full mission \"Involved friends\" list")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	SP_MISSIONS eMissionID
	REPEAT SP_MISSION_MAX eMissionID
		IF eMissionID <> SP_MISSION_NONE
			TEXT_LABEL_63 tMission = GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID)
			TEXT_LABEL_63 tInvolved = GetLabel_InvolvedFriends(eMissionID)
		
			SAVE_STRING_TO_DEBUG_FILE(tMission)
			SAVE_STRING_TO_DEBUG_FILE(":		")
			SAVE_STRING_TO_DEBUG_FILE(tInvolved)
			SAVE_NEWLINE_TO_DEBUG_FILE()

		ENDIF

	ENDREPEAT

	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_NEWLINE_TO_DEBUG_FILE()

	CLOSE_DEBUG_FILE()

ENDPROC

PROC PRIVATE_DEBUG_PrintMissionsCloseToLocations()

	IF OPEN_DEBUG_FILE()

		CPRINTLN(DEBUG_FRIENDS, "\n\n\nPrinting missions close to locations to temp_debug.txt\n")
		SAVE_STRING_TO_DEBUG_FILE("\nMissions close to activity locations...\n")
		
		// For each story mission
		SP_MISSIONS eMission
		REPEAT SP_MISSION_MAX eMission
			// Get mission name
			TEXT_LABEL_63 tMission = GetLabel_SP_MISSIONS(eMission)
			
			// Get mission pos
			VECTOR vMissionPos
			STATIC_BLIP_NAME_ENUM eMissionBlip
			
			eMissionBlip = g_sMissionStaticData[eMission].blip
			IF eMissionBlip <> STATIC_BLIP_NAME_DUMMY_FINAL
				IF IS_STATIC_BLIP_MULTIMODE(eMissionBlip)
					vMissionPos = GET_STATIC_BLIP_POSITION(eMissionBlip, GET_CURRENT_PLAYER_PED_INT())
				ELSE
				 	vMissionPos = GET_STATIC_BLIP_POSITION(eMissionBlip)
				ENDIF
				
				// For each activity location
				enumActivityLocation eLoc
				REPEAT MAX_ACTIVITY_LOCATIONS eLoc
					// Get location name
					TEXT_LABEL_63 tLoc = GetLabel_enumActivityLocation(eLoc)
					
					// Get location pos
					VECTOR vLocPos = GET_STATIC_BLIP_POSITION(g_ActivityLocations[eLoc].sprite)
					
					// If too close, print out
					FLOAT fDist = VDIST(vMissionPos, vLocPos)
					IF fDist <= 100.0
						SAVE_STRING_TO_DEBUG_FILE("  Mission \"")
						SAVE_STRING_TO_DEBUG_FILE(tMission)
						SAVE_STRING_TO_DEBUG_FILE("\" is close to \"")
						SAVE_STRING_TO_DEBUG_FILE(tLoc)
						SAVE_STRING_TO_DEBUG_FILE("\",   dist = ")
						SAVE_FLOAT_TO_DEBUG_FILE(fDist)
						SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				ENDREPEAT
			ENDIF
		ENDREPEAT

		
		SAVE_STRING_TO_DEBUG_FILE("\nMissions close to friend locations...\n")

		REPEAT SP_MISSION_MAX eMission
			// Get mission name
			TEXT_LABEL_63 tMission = GetLabel_SP_MISSIONS(eMission)
			
			// Get mission pos
			VECTOR vMissionPos
			STATIC_BLIP_NAME_ENUM eMissionBlip
			
			eMissionBlip = g_sMissionStaticData[eMission].blip
			IF eMissionBlip <> STATIC_BLIP_NAME_DUMMY_FINAL
				IF IS_STATIC_BLIP_MULTIMODE(eMissionBlip)
					vMissionPos = GET_STATIC_BLIP_POSITION(eMissionBlip, GET_CURRENT_PLAYER_PED_INT())
				ELSE
				 	vMissionPos = GET_STATIC_BLIP_POSITION(eMissionBlip)
				ENDIF
				
				// For each friend location
				enumFriendLocation eLoc
				REPEAT MAX_FRIEND_LOCATIONS eLoc
					IF eLoc <> FLOC_adhoc
						// Get location name
						TEXT_LABEL_63 tLoc = GetLabel_enumFriendLocation(eLoc)
						
						// Get location pos
						VECTOR vLocPos = FriendLoc_GetCoord(eLoc)
						
						// If too close, print out
						FLOAT fDist = VDIST(vMissionPos, vLocPos)
						IF fDist <= 100.0
							SAVE_STRING_TO_DEBUG_FILE("  Mission \"")
							SAVE_STRING_TO_DEBUG_FILE(tMission)
							SAVE_STRING_TO_DEBUG_FILE("\" is close to \"")
							SAVE_STRING_TO_DEBUG_FILE(tLoc)
							SAVE_STRING_TO_DEBUG_FILE("\",   dist = ")
							SAVE_FLOAT_TO_DEBUG_FILE(fDist)
							SAVE_NEWLINE_TO_DEBUG_FILE()
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDREPEAT

		SAVE_STRING_TO_DEBUG_FILE("\nEnd\n\n\n")
		CLOSE_DEBUG_FILE()
		
	ENDIF

ENDPROC

#ENDIF
