/*
Add commands in this order: 


//Create your widgets
CREATE_SCRIPT_PROFILER_WIDGET() 								// ***** ESSENTIAL COMMAND ****** in order to active the display screen

//At the start of the main loop

WHILE (TRUE)

	WAIT(0)

	SCRIPT_PROFILER_START_OF_FRAME()			 				// ***** ESSENTIAL COMMAND ****** place it just after your wait(0)


	ADD_SCRIPT_PROFILE_MARKER("marker 1") 						//Add this where you want to add a profile point


	ADD_SCRIPT_PROFILE_MARKER("marker 2") 						//Add this where you want to add a profile point



	// ****************************************************
	// 	LOOP PROFILING
	//		You may wish to profile functions within
	//		a loop.
	// ****************************************************

	//Use this for any repeats 
	ADD_SCRIPT_PROFILE_PRE_LOOP_MARKER("Loop profile")			// to profile a loop, place this just before the loop starts
		
		REPEAT LOOP_SIZE i
			
			ADD_SCRIPT_PROFILE_MARKER("loop marker 1") 			//Add this where you want to add a profile point
			
			ADD_SCRIPT_PROFILE_MARKER("loop marker 2") 			//Add this where you want to add a profile point
			
		ENDREPEAT

	ADD_SCRIPT_PROFILE_POST_LOOP_MARKER()						// when profiling a loop remember to call this just after the loop



	// ****************************************************
	// 	NESTING MARKERS
	//		You can nest markers inside other markers 
	//		and expand and colapse them on the display 
	//		screen
	//      Note: the top level of the nest will be 
	//			  the FOLLOWING marker, see below:
	// ****************************************************
	
	
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("marker 3")					
	
		ADD_SCRIPT_PROFILE_MARKER("marker 3.1") 						

		ADD_SCRIPT_PROFILE_MARKER("marker 3.2") 

		OPEN_SCRIPT_PROFILE_MARKER_GROUP(TRUE)	
			ADD_SCRIPT_PROFILE_MARKER("marker 3.3.1")
			ADD_SCRIPT_PROFILE_MARKER("marker 3.3.2")
		CLOSE_SCRIPT_PROFILE_MARKER_GROUP()	

		ADD_SCRIPT_PROFILE_MARKER("marker 3.3")

	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()



	//Add this to the end of the main loop
	SCRIPT_PROFILER_END_OF_FRAME()					 			// ***** ESSENTIAL COMMAND ****** place just before your wait(0)

END WHILE 

*/

USING "rage_builtins.sch"
USING "globals.sch"
USING "profiler_globals.sch"

USING "commands_misc.sch"
USING "commands_debug.sch"
USING "script_network.sch"

#IF IS_DEBUG_BUILD 
#IF SCRIPT_PROFILER_ACTIVE

STRUCT SCRIPT_PROFILER_DATA
	BOOL bSPWidgetCreated
	BOOL bIsActive = FALSE
	BOOL bRenderSP //= TRUE
	BOOL bOutputToFile
	BOOL bClearData
ENDSTRUCT
SCRIPT_PROFILER_DATA SPData


TWEAK_FLOAT fSPTextX  	0.022
TWEAK_FLOAT fSPStartX	0.355
TWEAK_FLOAT fSPNumbersX  0.730
TWEAK_FLOAT fSPNumbersX2  0.845
TWEAK_FLOAT fSPStartY  0.037
TWEAK_FLOAT fSPHeight  0.007
TWEAK_FLOAT fSPWidthPerMS  0.140
TWEAK_FLOAT fSPWidthPer1000Commands 0.026
TWEAK_FLOAT fProfTextScale 0.242
TWEAK_FLOAT fYSpacer 0.022
TWEAK_FLOAT fTextAdjustmentY  -0.008
TWEAK_FLOAT fSPScrollValue 0.004
TWEAK_FLOAT fRowWidth 0.975
TWEAK_INT PROFILER_INTERNAL_COST	13
TWEAK_INT PROFILER_LOOP_INTERNAL_COST	21
TWEAK_INT PROFILER_INTERNAL_TIME_COST	6

STRING sp_PATHNAME = "X:/gta5/titleupdate/dev_ng/"
STRING sp_FILENAME = "SPLog.txt"

PROC CREATE_SCRIPT_PROFILER_WIDGET()
	INT i
	TEXT_LABEL_63 strLabel
	IF NOT (SPData.bSPWidgetCreated)		
		strLabel = "Script Profiler - "
		strLabel += GET_THIS_SCRIPT_NAME()
		START_WIDGET_GROUP(strLabel)	
		
			ADD_WIDGET_BOOL("Is Active", SPData.bIsActive)
			ADD_WIDGET_BOOL("Render Bars", SPData.bRenderSP)
			ADD_WIDGET_BOOL("Output to File", SPData.bOutputToFile)
			ADD_WIDGET_BOOL("Clear Data", SPData.bClearData)
			
			START_WIDGET_GROUP("debug")
			
				START_WIDGET_GROUP("rendering")
					ADD_WIDGET_FLOAT_SLIDER("fSPTextX", fSPTextX, -1.0, 1.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("fSPStartX", fSPStartX, -1.0, 1.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("fSPNumbersX", fSPNumbersX, -1.0, 1.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("fSPNumbersX2", fSPNumbersX2, -1.0, 1.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("fSPStartY", fSPStartY, -1.0, 1.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("fSPHeight", fSPHeight, -1.0, 1.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("fSPWidthPerMS", fSPWidthPerMS, -1.0, 1.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("fProfTextScale", fProfTextScale, 0.0, 1.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("fYSpacer", fYSpacer, -1.0, 1.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("fSPWidthPer1000Commands", fSPWidthPer1000Commands, -1.0, 1.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("fTextAdjustmentY", fTextAdjustmentY, -1.0, 1.0, 0.001)	
					ADD_WIDGET_FLOAT_SLIDER("fSPScrollValue", fSPScrollValue, -1.0, 1.0, 0.001)	
					ADD_WIDGET_FLOAT_SLIDER("fRowWidth", fRowWidth, -1.0, 1.0, 0.001)						
				STOP_WIDGET_GROUP()
				
				ADD_WIDGET_INT_SLIDER("iActiveThread", GlobalScriptProfileData.iActiveThread, -1, 999, 1)
				ADD_WIDGET_INT_SLIDER("iMarkerCount", GlobalScriptProfileData.iMarkerCount, -1, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("iFailedMarkerCount", GlobalScriptProfileData.iFailedMarkerCount, -1, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("iStartRowToRender", GlobalScriptProfileData.iStartRowToRender, -1, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("iNumberOfRowsToRender", GlobalScriptProfileData.iNumberOfRowsToRender, -1, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("iRowToRender", GlobalScriptProfileData.iRowToRender, -1, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("iTotalRowsRendered", GlobalScriptProfileData.iTotalRowsRendered, -1, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("iRowToHighlight", GlobalScriptProfileData.iRowToHighlight, -1, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("iHighlightedMarkerID", GlobalScriptProfileData.iHighlightedMarkerID, -1, HIGHEST_INT, 1)

				ADD_WIDGET_INT_SLIDER("iOpenNestGroups", GlobalScriptProfileData.iOpenNestGroups, -1, HIGHEST_INT, 1)

				ADD_WIDGET_INT_SLIDER("PROFILER_INTERNAL_COST", PROFILER_INTERNAL_COST, -999, 999, 1)
				ADD_WIDGET_INT_SLIDER("PROFILER_LOOP_INTERNAL_COST", PROFILER_LOOP_INTERNAL_COST, -999, 999, 1)
				ADD_WIDGET_INT_SLIDER("PROFILER_INTERNAL_TIME_COST", PROFILER_INTERNAL_TIME_COST, -100, 100, 1)
				
				ADD_WIDGET_INT_SLIDER("iLastMarkerTime", GlobalScriptProfileData.iLastMarkerTime, -1, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("iLastMarkerIns", GlobalScriptProfileData.iLastMarkerIns, -1, HIGHEST_INT, 1)
				
				ADD_WIDGET_INT_SLIDER("iSuspendTime", GlobalScriptProfileData.iSuspendTime, -1, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("iSuspendIns", GlobalScriptProfileData.iSuspendIns, -1, HIGHEST_INT, 1)
				
				START_WIDGET_GROUP("Markers")
					REPEAT MAX_SCRIPT_PROFILE_MARKERS i
						strLabel = "MarkerData["
						strLabel += i
						strLabel += "]"
						START_WIDGET_GROUP(strLabel)
							ADD_WIDGET_INT_SLIDER("iIndex", GlobalScriptProfileData.MarkerData[i].iIndex, -1, HIGHEST_INT, 1)	
							ADD_WIDGET_INT_SLIDER("iParentID", GlobalScriptProfileData.MarkerData[i].iParentID, -1, HIGHEST_INT, 1)
							ADD_WIDGET_INT_SLIDER("iHash", GlobalScriptProfileData.MarkerData[i].iHash, LOWEST_INT, HIGHEST_INT, 1)
							ADD_WIDGET_INT_SLIDER("iTime", GlobalScriptProfileData.MarkerData[i].iTime, -1, HIGHEST_INT, 1)
							ADD_WIDGET_INT_SLIDER("iIns", GlobalScriptProfileData.MarkerData[i].iIns, -1, HIGHEST_INT, 1)
							ADD_WIDGET_INT_SLIDER("iPeakTime", GlobalScriptProfileData.MarkerData[i].iPeakTime, -1, HIGHEST_INT, 1)
							ADD_WIDGET_INT_SLIDER("iPeakIns", GlobalScriptProfileData.MarkerData[i].iPeakIns, -1, HIGHEST_INT, 1)
							ADD_WIDGET_BOOL("bCalledThisFrame", GlobalScriptProfileData.MarkerData[i].bCalledThisFrame)
							ADD_WIDGET_STRING(GlobalScriptProfileData.MarkerData[i].strName)
							ADD_WIDGET_BOOL("bIsLoopParent", GlobalScriptProfileData.MarkerData[i].bIsLoopParent)
							ADD_WIDGET_BOOL("bIsLoopChild", GlobalScriptProfileData.MarkerData[i].bIsLoopChild)
							ADD_WIDGET_BOOL("bShowChildren", GlobalScriptProfileData.MarkerData[i].bShowChildren)
							ADD_WIDGET_BOOL("bHasChildren", GlobalScriptProfileData.MarkerData[i].bHasChildren)
							ADD_WIDGET_BOOL("bIsRendering", GlobalScriptProfileData.MarkerData[i].bIsRendering)							
							ADD_WIDGET_INT_SLIDER("iTimeTotal", GlobalScriptProfileData.MarkerData[i].iTimeTotal, -1, HIGHEST_INT, 1)
							ADD_WIDGET_INT_SLIDER("iInsTotal", GlobalScriptProfileData.MarkerData[i].iInsTotal, -1, HIGHEST_INT, 1)
							ADD_WIDGET_INT_SLIDER("iTimeAvg", GlobalScriptProfileData.MarkerData[i].iTimeAvg, -1, HIGHEST_INT, 1)
							ADD_WIDGET_INT_SLIDER("iInsAvg", GlobalScriptProfileData.MarkerData[i].iInsAvg, -1, HIGHEST_INT, 1)
							ADD_WIDGET_INT_SLIDER("iCalls", GlobalScriptProfileData.MarkerData[i].iCalls, -1, HIGHEST_INT, 1)				
						STOP_WIDGET_GROUP()
					ENDREPEAT
				STOP_WIDGET_GROUP()
				
			STOP_WIDGET_GROUP()
			
			
		STOP_WIDGET_GROUP()
		SPData.bSPWidgetCreated = TRUE
	ENDIF
ENDPROC


PROC ClearPeaks()
	INT i
	REPEAT MAX_SCRIPT_PROFILE_MARKERS i
		GlobalScriptProfileData.MarkerData[i].iPeakTime = 0
		GlobalScriptProfileData.MarkerData[i].iPeakIns = 0
		GlobalScriptProfileData.MarkerData[i].iCalls = 0
		GlobalScriptProfileData.MarkerData[i].iTimeTotal = 0
		GlobalScriptProfileData.MarkerData[i].iInsTotal = 0
		GlobalScriptProfileData.MarkerData[i].iTimeAvg = 0
		GlobalScriptProfileData.MarkerData[i].iInsAvg = 0
	ENDREPEAT
ENDPROC

PROC ClearProfilerData()

	SCRIPT_PROFILE_MARKER_DATA EmptyMarker
	INT iMarker
	REPEAT MAX_SCRIPT_PROFILE_MARKERS iMarker	
		GlobalScriptProfileData.iMarkerIDs[iMarker] = 0
		GlobalScriptProfileData.MarkerData[iMarker] = EmptyMarker
	ENDREPEAT

	GlobalScriptProfileData.iStartRowToRender = 0
	GlobalScriptProfileData.iNumberOfRowsToRender = NUMBER_OF_ROWS_TO_RENDER
	GlobalScriptProfileData.iRowToHighlight = 0
	GlobalScriptProfileData.iMarkerCount = 0
	GlobalScriptProfileData.iLastMarkerTime = 0
	GlobalScriptProfileData.iLastMarkerIns = 0
	
	INT i
	REPEAT MAX_NUMBER_OF_SCRIPT_PROFILER_NESTS i
		GlobalScriptProfileData.iPreviousParentID[i] = -1
	ENDREPEAT
	GlobalScriptProfileData.iCurrentParentID = -1
	GlobalScriptProfileData.iLastMarkerID = -1
	GlobalScriptProfileData.iSuspendTime = 0
	GlobalScriptProfileData.iSuspendIns = 0
	GlobalScriptProfileData.bCountingSuspended = FALSE
	GlobalScriptProfileData.iRowToRender = 0
	GlobalScriptProfileData.iTotalRowsRendered = 0
	GlobalScriptProfileData.iHighlightedMarkerID = 0
	GlobalScriptProfileData.iStartOfFrameCommands = 0

	GlobalScriptProfileData.iCommandsSinceStartOfFrame = 0
	GlobalScriptProfileData.iCommandsSinceStartOfFramePeak = 0
	
	GlobalScriptProfileData.bStartOfFrameCalled = FALSE
	GlobalScriptProfileData.bLoopProfilerStarted = FALSE
		
	GlobalScriptProfileData.iMarkerCount = 0
	GlobalScriptProfileData.iFailedMarkerCount = 0
	GlobalScriptProfileData.bGoodToOutput = FALSE

	GlobalScriptProfileData.iLastBackground = 0
	GlobalScriptProfileData.iActiveThread = -1
		
	GlobalScriptProfileData.iTotalAccumulatedTime = 0
	GlobalScriptProfileData.iTotalAccumulatedCommands = 0
	GlobalScriptProfileData.iTotalAccumulatedTimePeak = 0
		
	GlobalScriptProfileData.iOpenNestGroups	= 0

ENDPROC

PROC SP_PRINT_TAG()
	PRINTSTRING("[script_profiler][") PRINTSTRING(GET_THIS_SCRIPT_NAME()) PRINTSTRING("] ")
ENDPROC

	
FUNC BOOL IsThisScriptProfilerThreadActive()

	IF (SPData.bClearData)
		ClearProfilerData()	
		SPData.bClearData = FALSE
	ENDIF

	IF (SPData.bIsActive)	
		IF (GlobalScriptProfileData.iActiveThread = -1)
			ClearProfilerData()
			ClearPeaks()
			GlobalScriptProfileData.iActiveThread = NATIVE_TO_INT(GET_ID_OF_THIS_THREAD())
			CLEAR_NAMED_DEBUG_FILE(sp_PATHNAME, sp_FILENAME)
			OPEN_NAMED_DEBUG_FILE(sp_PATHNAME, sp_FILENAME)
			SP_PRINT_TAG() PRINTSTRING("] - made the active thread") PRINTNL()			
		ENDIF		
		IF (GlobalScriptProfileData.iActiveThread = NATIVE_TO_INT(GET_ID_OF_THIS_THREAD()))
			RETURN(TRUE)	
		ELSE
			//SP_PRINT_TAG()  SP_PRINTSTRING("Another script is currently active - this script is waiting")  	SP_PRINTNL()	
		ENDIF		
	ELSE	
		IF (GlobalScriptProfileData.iActiveThread = NATIVE_TO_INT(GET_ID_OF_THIS_THREAD()))	
		OR NOT IS_THREAD_ACTIVE(INT_TO_NATIVE(THREADID, GlobalScriptProfileData.iActiveThread))
			GlobalScriptProfileData.iActiveThread = -1	
			CLOSE_DEBUG_FILE()
		ENDIF		
	ENDIF	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsThisScriptGoodToOutputToFile()
	IF (SPData.bOutputToFile)		
		RETURN(GlobalScriptProfileData.bGoodToOutput)			
	ENDIF
	RETURN(FALSE)
ENDFUNC


PROC SET_SCRIPT_PROFILER_ACTIVE(BOOL bSet)
	SPData.bIsActive = bSet
ENDPROC

PROC SET_SCRIPT_PROFILER_RENDER_BARS(BOOL bSet)
	SPData.bRenderSP = bSet
ENDPROC

PROC OUTPUT_ALL_SCRIPT_PROFILER_DATA_TO_FILE()
	SPData.bOutputToFile = TRUE
ENDPROC

PROC SP_PRINTSTRING(sTRING str)
	PRINTSTRING(str)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(str, sp_PATHNAME, sp_FILENAME)
ENDPROC

PROC SP_PRINTINT(INT iInt)
	PRINTINT(iInt)
	SAVE_INT_TO_NAMED_DEBUG_FILE(iInt, sp_PATHNAME, sp_FILENAME)
ENDPROC

PROC SP_PRINTFLOAT(FLOAT fFloat)
	PRINTFLOAT(fFloat)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(fFloat, sp_PATHNAME, sp_FILENAME)
ENDPROC

PROC SP_PRINTNL()
	PRINTNL()
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sp_PATHNAME, sp_FILENAME)
ENDPROC




PROC FILL_STRING_TO_LENGTH(INT iLength, STRING str)
	INT i
	REPEAT (iLength - GET_LENGTH_OF_LITERAL_STRING(str)) i
		SP_PRINTSTRING(" ")	
	ENDREPEAT
ENDPROC



PROC PROFILER_TEST_NUMBER_OF_COMMMANDS(INT iNumber)
	INT i
	REPEAT iNumber i
		// do nothing
	ENDREPEAT
ENDPROC

// ************* new procs ********************
FUNC INT GetMarkerIndexFromString(STRING strName)
	
	INT iStringAsHash = GET_HASH_KEY(strName)
	INT i
	
	REPEAT MAX_SCRIPT_PROFILE_MARKERS i
		IF (GlobalScriptProfileData.iMarkerIDs[i] = iStringAsHash)
			RETURN i
		ENDIF
	ENDREPEAT
	
	// else need to add it
	REPEAT MAX_SCRIPT_PROFILE_MARKERS i
		IF (GlobalScriptProfileData.iMarkerIDs[i] = 0)
			GlobalScriptProfileData.iMarkerIDs[i] = iStringAsHash
			RETURN i
		ENDIF
	ENDREPEAT

	RETURN -1
	
ENDFUNC

FUNC INT GET_SCRIPT_TIME_IN_MICROSECONDS()
	RETURN FLOOR(GET_SCRIPT_TIME_WITHIN_FRAME_IN_MICROSECONDS())
ENDFUNC

PROC SUSPEND_COUNTING(BOOL bSet)
	IF IsThisScriptProfilerThreadActive()
		IF (bSet)			
			IF (GlobalScriptProfileData.bCountingSuspended)
				SCRIPT_ASSERT("SUSPEND_COUNTING - already called with TRUE")
			ELSE			
				GlobalScriptProfileData.iSuspendTime = GET_SCRIPT_TIME_IN_MICROSECONDS()
				GlobalScriptProfileData.iSuspendIns = GET_NUMBER_OF_INSTRUCTIONS_EXECUTED()
			ENDIF
		ELSE
			IF NOT (GlobalScriptProfileData.bCountingSuspended)
				SCRIPT_ASSERT("SUSPEND_COUNTING - already called with FALSE")
			ELSE
				INT iDiff
				iDiff = GET_SCRIPT_TIME_IN_MICROSECONDS() - GlobalScriptProfileData.iSuspendTime
				IF (iDiff > 0)
					GlobalScriptProfileData.iLastMarkerTime += iDiff 
				ENDIF
				iDiff = GET_NUMBER_OF_INSTRUCTIONS_EXECUTED() - GlobalScriptProfileData.iSuspendIns
				IF (iDiff > 0)
					GlobalScriptProfileData.iLastMarkerIns += iDiff
				ENDIF
			ENDIF
		ENDIF
		GlobalScriptProfileData.bCountingSuspended = bSet
	ENDIF
ENDPROC

FUNC INT GET_TOTAL_FOR_MARKER(INT iMarkerID, INT iType)
	INT iTotal = 0
	FLOAT fAverage
	IF (iMarkerID > -1)
		SWITCH iType
			CASE 0
				iTotal = GlobalScriptProfileData.MarkerData[iMarkerID].iTime
			BREAK
			CASE 1
				iTotal = GlobalScriptProfileData.MarkerData[iMarkerID].iPeakTime
			BREAK
			CASE 2
				iTotal = GlobalScriptProfileData.MarkerData[iMarkerID].iIns
			BREAK
			CASE 3
				iTotal = GlobalScriptProfileData.MarkerData[iMarkerID].iPeakIns
			BREAK
			CASE 4
				fAverage = TO_FLOAT(GlobalScriptProfileData.MarkerData[iMarkerID].iTimeTotal) / TO_FLOAT(GlobalScriptProfileData.MarkerData[iMarkerID].iCalls)
				iTotal = ROUND(fAverage)
				//iTotal = GlobalScriptProfileData.MarkerData[iMarkerID].iTimeAvg
			BREAK
			CASE 5
				fAverage = TO_FLOAT(GlobalScriptProfileData.MarkerData[iMarkerID].iInsTotal) / TO_FLOAT(GlobalScriptProfileData.MarkerData[iMarkerID].iCalls)
				iTotal = ROUND(fAverage)
				//iTotal += GlobalScriptProfileData.MarkerData[iMarkerID].iInsAvg
			BREAK
		ENDSWITCH
		IF (GlobalScriptProfileData.MarkerData[iMarkerID].bHasChildren)
			INT iChild
			REPEAT MAX_SCRIPT_PROFILE_MARKERS iChild	
				IF (GlobalScriptProfileData.MarkerData[iChild].iParentID = iMarkerID)
					iTotal += GET_TOTAL_FOR_MARKER(iChild, iType)
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	RETURN iTotal
ENDFUNC

PROC OUTPUT_MARKER_TO_FILE(INT iMarkerID, INT iIndent)

	IF IsThisScriptGoodToOutputToFile()
	
		INT iLength
		INT i
		TEXT_LABEL_63 str63

		INT iTotal[6]
		REPEAT 6 i
			iTotal[i] = GET_TOTAL_FOR_MARKER(iMarkerID, i)
			IF (iTotal[i] < 0)
				iTotal[i] = 0
			ENDIF
		ENDREPEAT

		iLength = 0	
		
		PRINTSTRING("[script_profiler] - ") PRINTSTRING(GET_THIS_SCRIPT_NAME()) PRINTSTRING("] ")
		REPEAT iIndent i
			SP_PRINTSTRING("   ")	
			iLength += GET_LENGTH_OF_LITERAL_STRING("   ")
		ENDREPEAT
		SP_PRINTSTRING(GlobalScriptProfileData.MarkerData[iMarkerID].strName)		
	
		FILL_STRING_TO_LENGTH(100 - iLength, GlobalScriptProfileData.MarkerData[iMarkerID].strName)

		str63 = " Av Time: " 
		str63 += iTotal[4]	
		SP_PRINTSTRING(str63)
		FILL_STRING_TO_LENGTH(25, str63)
		
		str63 = " Av Ins: " 
		str63 += iTotal[5]
		SP_PRINTSTRING(str63)
		FILL_STRING_TO_LENGTH(25, str63)	
			

		str63 = " Peak Time: " 
		str63 += iTotal[1]
		SP_PRINTSTRING(str63)
		FILL_STRING_TO_LENGTH(25, str63)
								
		
		str63 = " Peak Ins: " 
		str63 += iTotal[3]	
		SP_PRINTSTRING(str63)
		FILL_STRING_TO_LENGTH(25, str63)

		
		SP_PRINTNL()
	
	ENDIF

ENDPROC

PROC OUTPUT_MARKER(INT iMarkerID, INT iIndent)
	
	OUTPUT_MARKER_TO_FILE(iMarkerID, iIndent)
	
	INT iChild
	REPEAT MAX_SCRIPT_PROFILE_MARKERS iChild	
		IF (GlobalScriptProfileData.MarkerData[iChild].iParentID = iMarkerID)
			OUTPUT_MARKER(iChild, iIndent+1)
		ENDIF
	ENDREPEAT 	

ENDPROC

PROC OUTPUT_ALL_MARKERS_TO_FILE()
	INT iMarker
	REPEAT MAX_SCRIPT_PROFILE_MARKERS iMarker	
		IF (GlobalScriptProfileData.MarkerData[iMarker].iIndex > -1)
		AND (GlobalScriptProfileData.MarkerData[iMarker].iParentID = -1)
			OUTPUT_MARKER(iMarker, 0)
		ENDIF
	ENDREPEAT 	
ENDPROC


PROC RENDER_MARKER_BARS(INT iMarkerID, INT iIndent)
	
	IF (GlobalScriptProfileData.iRowToRender >= GlobalScriptProfileData.iStartRowToRender)
	AND (GlobalScriptProfileData.iRowToRender < (GlobalScriptProfileData.iStartRowToRender + GlobalScriptProfileData.iNumberOfRowsToRender))

		FLOAT fThisY
		FLOAT fThisWidth
		TEXT_LABEL_63 strLabel
		INT r,g,b,a
		INT i	
		
		INT iTotal[6]
		REPEAT 6 i
			iTotal[i] = GET_TOTAL_FOR_MARKER(iMarkerID, i)
			IF (iTotal[i] < 0)
				iTotal[i] = 0
			ENDIF
		ENDREPEAT
		
		fThisY = fSPStartY + (fYSpacer * TO_FLOAT((GlobalScriptProfileData.iRowToRender + 1) - GlobalScriptProfileData.iStartRowToRender))
		SET_TEXT_SCALE(fProfTextScale, fProfTextScale)
		
		IF NOT (GlobalScriptProfileData.MarkerData[iMarkerID].bCalledThisFrame)
			SET_TEXT_COLOUR(128, 128, 128, 255)	
		ELSE
			SET_TEXT_COLOUR(255, 255, 255, 255)	
		ENDIF
		
		strLabel = ""
		REPEAT iIndent i
			strLabel += "     "	
		ENDREPEAT
		IF (GlobalScriptProfileData.MarkerData[iMarkerID].bHasChildren)
			IF (GlobalScriptProfileData.MarkerData[iMarkerID].bShowChildren)
				strLabel += "[-]"
			ELSE
				strLabel += "[+]"	
			ENDIF
		ENDIF
		IF (GlobalScriptProfileData.MarkerData[iMarkerID].bIsLoopParent)
			strLabel += "[L]"	
		ENDIF
		
		strLabel += GlobalScriptProfileData.MarkerData[iMarkerID].strName
		strLabel += ":"		
		DISPLAY_TEXT_WITH_LITERAL_STRING(fSPTextX, fThisY + fTextAdjustmentY + fSPScrollValue, "STRING", strLabel)		

		SET_TEXT_SCALE(fProfTextScale, fProfTextScale)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		strLabel = "~HC_YELLOW~"
		strLabel += iTotal[0]
		strLabel += "~s~,~HC_ORANGE~"	
		strLabel += iTotal[4]
		strLabel += "~s~,~HC_RED~"	
		strLabel += iTotal[1]
		DISPLAY_TEXT_WITH_LITERAL_STRING(fSPNumbersX, fThisY + fTextAdjustmentY + fSPScrollValue, "STRING", strLabel)
		
		SET_TEXT_SCALE(fProfTextScale, fProfTextScale)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		strLabel = "~HC_BLUELIGHT~"
		strLabel += iTotal[2]
		strLabel += "~s~,~HC_BLUE~"
		strLabel += iTotal[5]
		strLabel += "~s~,~HC_PURPLE~"	
		strLabel += iTotal[3]
		DISPLAY_TEXT_WITH_LITERAL_STRING(fSPNumbersX2, fThisY + fTextAdjustmentY + fSPScrollValue, "STRING", strLabel)		
		
		// alterternate background row colours
		IF (GlobalScriptProfileData.iLastBackground = 0)
			GlobalScriptProfileData.iLastBackground = 1
		ELSE
			GlobalScriptProfileData.iLastBackground = 0
		ENDIF	
		
		// draw background
		IF (GlobalScriptProfileData.iRowToHighlight = GlobalScriptProfileData.iRowToRender)
			r = 255
			g = 0
			b = 0
			a = 128		
		ELSE
			SWITCH iIndent
				CASE 0
					r = 0
					g = 0
					b = 0	
				BREAK
				CASE 1
					r = 0
					g = 0
					b = 64
				BREAK
				CASE 2
					r = 64
					g = 0
					b = 0
				BREAK
				CASE 3
					r = 0
					g = 64
					b = 0
				BREAK
				CASE 4
					r = 0
					g = 64
					b = 64
				BREAK
				CASE 5
					r = 64
					g = 0
					b = 64
				BREAK
				CASE 6
					r = 64
					g = 64
					b = 0
				BREAK
				CASE 7
					r = 32
					g = 32
					b = 128
				BREAK
				CASE 8
					r = 32
					g = 128
					b = 32
				BREAK
				CASE 9
					r = 128
					g = 32
					b = 32
				BREAK
				CASE 10
					r = 128
					g = 128
					b = 32
				BREAK
			ENDSWITCH

			IF (GlobalScriptProfileData.iLastBackground = 0)
				a = 192
			ELSE
				a = 128
			ENDIF

		ENDIF
		DRAW_RECT_FROM_CORNER(fSPTextX, fThisY + fTextAdjustmentY + fSPScrollValue, fRowWidth, fYSpacer, r,g,b,a)
		
		// draw  bar for time
		fThisWidth = TO_FLOAT(iTotal[0]) * 0.001 * fSPWidthPerMS
		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, r,g,b,a)
		a = 192
		DRAW_RECT_FROM_CORNER(fSPStartX, fThisY - (fSPHeight * 0.5) + fSPScrollValue, fThisWidth, fSPHeight, r,g,b,a)
		
		// draw orrange average
		IF (iTotal[4] > 0)
			fThisWidth = TO_FLOAT(iTotal[4]) * 0.001 * fSPWidthPerMS
			GET_HUD_COLOUR(HUD_COLOUR_ORANGE, r,g,b,a)
			DRAW_RECT_FROM_CORNER(fSPStartX + fThisWidth, fThisY - (fSPHeight * 0.5) + fSPScrollValue, 0.001, fSPHeight, r,g,b,a)			
		ENDIF		
		
		// draw red peak
		IF (iTotal[1] > 0)
			fThisWidth = TO_FLOAT(iTotal[1]) * 0.001 * fSPWidthPerMS
			GET_HUD_COLOUR(HUD_COLOUR_RED, r,g,b,a)
			DRAW_RECT_FROM_CORNER(fSPStartX + fThisWidth, fThisY - (fSPHeight * 0.5) + fSPScrollValue, 0.001, fSPHeight, r,g,b,a)			
		ENDIF		
		
		// draw bar for commands
		fThisWidth = (TO_FLOAT(iTotal[2]) * 0.001) * fSPWidthPer1000Commands
		GET_HUD_COLOUR(HUD_COLOUR_BLUELIGHT, r,g,b,a)
		a = 192
		DRAW_RECT_FROM_CORNER(fSPStartX, fThisY + (fSPHeight * 0.5) + fSPScrollValue, fThisWidth, fSPHeight, r,g,b,a)
		
		// draw blue average
		IF (iTotal[5] > 0)
			fThisWidth = TO_FLOAT(iTotal[5]) * 0.001 * fSPWidthPer1000Commands
			GET_HUD_COLOUR(HUD_COLOUR_BLUE, r,g,b,a)
			DRAW_RECT_FROM_CORNER(fSPStartX + fThisWidth, fThisY + (fSPHeight * 0.5) + fSPScrollValue, 0.001, fSPHeight, r,g,b,a)			
		ENDIF		
		
		// draw purple peak
		IF (iTotal[3] > 0)
			fThisWidth = TO_FLOAT(iTotal[3]) * 0.001 * fSPWidthPer1000Commands
			GET_HUD_COLOUR(HUD_COLOUR_PURPLE, r,g,b,a)
			DRAW_RECT_FROM_CORNER(fSPStartX + fThisWidth, fThisY + (fSPHeight * 0.5) + fSPScrollValue, 0.001, fSPHeight, r,g,b,a)			
		ENDIF
		
		GlobalScriptProfileData.MarkerData[iMarkerID].bIsRendering = TRUE
	ENDIF
	
	IF (GlobalScriptProfileData.iRowToHighlight = GlobalScriptProfileData.iRowToRender)
		GlobalScriptProfileData.iHighlightedMarkerID = iMarkerID
	ENDIF

	GlobalScriptProfileData.iRowToRender++

ENDPROC


PROC RENDER_MARKER(INT iMarkerID, INT iIndent)
	
	IF NOT (GlobalScriptProfileData.MarkerData[iMarkerID].bIsHidden)
	
		RENDER_MARKER_BARS(iMarkerID, iIndent)

		IF (GlobalScriptProfileData.MarkerData[iMarkerID].bShowChildren)
			INT iChild
			REPEAT MAX_SCRIPT_PROFILE_MARKERS iChild	
				IF (GlobalScriptProfileData.MarkerData[iChild].iParentID = iMarkerID)
					RENDER_MARKER(iChild, iIndent+1)
				ENDIF
			ENDREPEAT 	
		ENDIF
		
	ENDIF

ENDPROC

PROC RENDER_TOP_PARENTS()
	INT iMarker

	REPEAT MAX_SCRIPT_PROFILE_MARKERS iMarker	
		GlobalScriptProfileData.MarkerData[iMarker].bIsRendering = FALSE
	ENDREPEAT
	
	REPEAT MAX_SCRIPT_PROFILE_MARKERS iMarker	
		IF (GlobalScriptProfileData.MarkerData[iMarker].iIndex > -1)
		AND (GlobalScriptProfileData.MarkerData[iMarker].iParentID = -1)
			RENDER_MARKER(iMarker, 0)
		ENDIF
	ENDREPEAT 			
ENDPROC

PROC RENDER_PROFILER_SCREEN()
	
	IF (SPData.bRenderSP)

		TEXT_LABEL_63 strLabel

		// display total commands and time
		DRAW_RECT_FROM_CORNER(fSPTextX, fSPStartY + fTextAdjustmentY, 0.8, fYSpacer, 0, 0, 0, 128)
		SET_TEXT_SCALE(fProfTextScale, fProfTextScale)
		SET_TEXT_COLOUR(255, 0, 0, 255)
		strLabel = "Total Time = "
		strLabel += GlobalScriptProfileData.iTotalAccumulatedTime
		strLabel += ", Total Commands = "
		strLabel += GlobalScriptProfileData.iCommandsSinceStartOfFrame
		strLabel += ","
		strLabel += GlobalScriptProfileData.iTotalAccumulatedCommands
		DISPLAY_TEXT_WITH_LITERAL_STRING(fSPTextX, fSPStartY + fTextAdjustmentY, "STRING", strLabel)		

		// draw peak total commands and time						
		SET_TEXT_SCALE(fProfTextScale, fProfTextScale)
		SET_TEXT_COLOUR(255, 0, 0, 255)
		strLabel = "Peak Total Time = "
		strLabel += GlobalScriptProfileData.iTotalAccumulatedTimePeak
		strLabel += ", Peak Total Commands = "
		strLabel += GlobalScriptProfileData.iCommandsSinceStartOfFramePeak
		DISPLAY_TEXT_WITH_LITERAL_STRING(fSPTextX + 0.25, fSPStartY + fTextAdjustmentY, "STRING", strLabel)				

		// how many markers are there?
		SET_TEXT_SCALE(fProfTextScale, fProfTextScale)
		SET_TEXT_COLOUR(255, 0, 0, 255)
		strLabel = "Number of markers = "
		strLabel += GlobalScriptProfileData.iMarkerCount
		DISPLAY_TEXT_WITH_LITERAL_STRING(fSPTextX + 0.65, fSPStartY + fTextAdjustmentY, "STRING", strLabel)

		RENDER_TOP_PARENTS()
		
	ENDIF

ENDPROC

PROC SCRIPT_PROFILER_END_OF_FRAME()
	
	SUSPEND_COUNTING(TRUE)

	IF IsThisScriptProfilerThreadActive()
		
		IF (GlobalScriptProfileData.bStartOfFrameCalled)
	
			GlobalScriptProfileData.iCommandsSinceStartOfFrame = GET_NUMBER_OF_INSTRUCTIONS_EXECUTED() - GlobalScriptProfileData.iStartOfFrameCommands
			
			IF IsThisScriptGoodToOutputToFile()
				
				PRINTSTRING("************************* Script Profiler - END - ") PRINTSTRING(GET_THIS_SCRIPT_NAME()) PRINTSTRING("**********************************") PRINTNL()
					
				//OUTPUT_PEAK_ARRAY_INFO()
				
				OUTPUT_ALL_MARKERS_TO_FILE()
					
				SPData.bOutputToFile = FALSE
				//SPData.bIsActive = FALSE // after we have out put then switch this thread off to allow others to be output
				//ClearPeaks()
				
			ENDIF
			GlobalScriptProfileData.bStartOfFrameCalled = FALSE
	
			RENDER_PROFILER_SCREEN()
			
		ENDIF
		
	ENDIF
	
	SUSPEND_COUNTING(FALSE)
	
ENDPROC


PROC SCRIPT_PROFILER_START_OF_FRAME()
	
	INT i
	
	//SUSPEND_COUNTING(TRUE)

	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_1, KEYBOARD_MODIFIER_SHIFT, "Output script profiler info")
		
		ClearPeaks()
		SPData.bIsActive = TRUE
		SPData.bOutputToFile = TRUE
		
		SP_PRINT_TAG() PRINTSTRING("- detected key 1 press") PRINTNL()
		
		// clear the SPLog.txt
		IF IsThisScriptProfilerThreadActive()
			CLOSE_DEBUG_FILE()
			CLEAR_NAMED_DEBUG_FILE(sp_PATHNAME, sp_FILENAME)
			OPEN_NAMED_DEBUG_FILE(sp_PATHNAME, sp_FILENAME)
		ENDIF
		
	ENDIF

	IF IsThisScriptProfilerThreadActive()	
	
		IF (GlobalScriptProfileData.bStartOfFrameCalled)
			SCRIPT_PROFILER_END_OF_FRAME()
		ENDIF
		
		IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), GlobalScriptProfileData.iClearPeakTime) > 10000)
			GlobalScriptProfileData.bGoodToOutput = TRUE
		ELSE
			GlobalScriptProfileData.bGoodToOutput = FALSE
		ENDIF

		IF NOT (SPData.bSPWidgetCreated)
			SCRIPT_ASSERT("Need to call CREATE_SCRIPT_PROFILER_WIDGET in your widget group")
		ENDIF

		// update peaks
		IF (GlobalScriptProfileData.iTotalAccumulatedTime > GlobalScriptProfileData.iTotalAccumulatedTimePeak)
			GlobalScriptProfileData.iTotalAccumulatedTimePeak = GlobalScriptProfileData.iTotalAccumulatedTime
		ENDIF
		IF (GlobalScriptProfileData.iCommandsSinceStartOfFrame > GlobalScriptProfileData.iCommandsSinceStartOfFramePeak)
			GlobalScriptProfileData.iCommandsSinceStartOfFramePeak = GlobalScriptProfileData.iCommandsSinceStartOfFrame
		ENDIF
		
		// get current number of rows being rendered
		GlobalScriptProfileData.iTotalRowsRendered = 0
		REPEAT MAX_SCRIPT_PROFILE_MARKERS i
			IF (GlobalScriptProfileData.MarkerData[i].bIsRendering)
				GlobalScriptProfileData.iTotalRowsRendered++
			ENDIF		
		ENDREPEAT

		IF (SPData.bRenderSP)

			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
				GlobalScriptProfileData.iRowToHighlight++				
				
				IF (GlobalScriptProfileData.iRowToHighlight >= GlobalScriptProfileData.iStartRowToRender + GlobalScriptProfileData.iTotalRowsRendered)					
					// loop round
					IF (GlobalScriptProfileData.iRowToHighlight >= GlobalScriptProfileData.iRowToRender)
						GlobalScriptProfileData.iRowToHighlight--	
					ELSE
						// scroll down
						GlobalScriptProfileData.iStartRowToRender++
						IF (GlobalScriptProfileData.iStartRowToRender > (GlobalScriptProfileData.iMarkerCount - (GlobalScriptProfileData.iNumberOfRowsToRender-1)))
							GlobalScriptProfileData.iStartRowToRender = (GlobalScriptProfileData.iMarkerCount - (GlobalScriptProfileData.iNumberOfRowsToRender-1)) 
						ENDIF
					ENDIF				
				ENDIF	

			
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD8)
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
				GlobalScriptProfileData.iRowToHighlight--
				
				IF (GlobalScriptProfileData.iRowToHighlight < GlobalScriptProfileData.iStartRowToRender)
					// loop round?
					IF (GlobalScriptProfileData.iRowToHighlight < 0 )
						GlobalScriptProfileData.iRowToHighlight = GlobalScriptProfileData.iStartRowToRender + (GlobalScriptProfileData.iTotalRowsRendered - 1)
					ELSE
					// scroll up
						GlobalScriptProfileData.iStartRowToRender--
						IF (GlobalScriptProfileData.iStartRowToRender < 0)
							GlobalScriptProfileData.iStartRowToRender = 0
						ENDIF	
					ENDIF
				ENDIF	

			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)			
				REPEAT MAX_SCRIPT_PROFILE_MARKERS i
					GlobalScriptProfileData.MarkerData[i].bShowChildren = FALSE
				ENDREPEAT
				GlobalScriptProfileData.iStartRowToRender = 0
				GlobalScriptProfileData.iRowToHighlight = 0
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD3)
				REPEAT MAX_SCRIPT_PROFILE_MARKERS i
					GlobalScriptProfileData.MarkerData[i].bShowChildren = TRUE
				ENDREPEAT
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
				ClearPeaks()
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD6)	
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)
				IF (GlobalScriptProfileData.iHighlightedMarkerID > -1)
					IF (GlobalScriptProfileData.MarkerData[GlobalScriptProfileData.iHighlightedMarkerID].bHasChildren)
					OR (GlobalScriptProfileData.MarkerData[GlobalScriptProfileData.iHighlightedMarkerID].bIsLoopParent)
						GlobalScriptProfileData.MarkerData[GlobalScriptProfileData.iHighlightedMarkerID].bShowChildren = TRUE
					ENDIF
				ENDIF
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_LEFT)
				IF (GlobalScriptProfileData.iHighlightedMarkerID > -1)
					IF (GlobalScriptProfileData.MarkerData[GlobalScriptProfileData.iHighlightedMarkerID].bShowChildren)
						GlobalScriptProfileData.MarkerData[GlobalScriptProfileData.iHighlightedMarkerID].bShowChildren = FALSE
					ENDIF
				ENDIF
			ENDIF		
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_B)
				IF (SPData.bRenderSP)
					SPData.bRenderSP = FALSE
				ELSE
					SPData.bRenderSP = TRUE
				ENDIF
			ENDIF		
			
	
			
			GlobalScriptProfileData.iLastBackground = 0

			
		ENDIF
		
		IF IsThisScriptGoodToOutputToFile()
			SP_PRINTNL()
			SP_PRINTSTRING("************************* Script Profiler - ") SP_PRINTSTRING(GET_THIS_SCRIPT_NAME()) SP_PRINTSTRING(" **********************************") SP_PRINTNL()
			SP_PRINTNL()
			
			// count number of players?
			INT iCount
			IF NETWORK_IS_GAME_IN_PROGRESS()
			
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT() 
					SP_PRINT_TAG()  SP_PRINTSTRING("Host:YES")	SP_PRINTNL()
				ELSE
					SP_PRINT_TAG()  SP_PRINTSTRING("Host:NO") 	SP_PRINTNL()
				ENDIF
			
				REPEAT NUM_NETWORK_PLAYERS i
					IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, i), FALSE, TRUE)
						iCount++
					ENDIF
				ENDREPEAT				
				SP_PRINT_TAG()  SP_PRINTSTRING("No of players:") SP_PRINTINT(iCount) 	SP_PRINTNL()
												
			ENDIF
			
			// total commands
			SP_PRINT_TAG()   SP_PRINTSTRING("Total Commands:") SP_PRINTINT(GlobalScriptProfileData.iCommandsSinceStartOfFrame) SP_PRINTNL()
			SP_PRINT_TAG()   SP_PRINTSTRING("Total Time:") SP_PRINTINT(GlobalScriptProfileData.iTotalAccumulatedTime) SP_PRINTNL() 
			
			// peak totals
			SP_PRINT_TAG()   SP_PRINTSTRING("Peak Total Commands:") SP_PRINTINT(GlobalScriptProfileData.iCommandsSinceStartOfFramePeak) SP_PRINTNL()
			SP_PRINT_TAG()   SP_PRINTSTRING("Peak Total Time:") SP_PRINTINT(GlobalScriptProfileData.iTotalAccumulatedTimePeak) SP_PRINTNL()
			
			SP_PRINT_TAG()   SP_PRINTSTRING("Time since peak clear:") SP_PRINTINT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), GlobalScriptProfileData.iClearPeakTime)) SP_PRINTNL()
			
			SP_PRINTNL()
		
		ENDIF
		
		GET_NUMBER_OF_MICROSECONDS_SINCE_LAST_CALL()
		GlobalScriptProfileData.iStartOfFrameCommands = GET_NUMBER_OF_INSTRUCTIONS_EXECUTED() 	
		GlobalScriptProfileData.iTotalAccumulatedTime = 0
		GlobalScriptProfileData.iTotalAccumulatedCommands = 0
		
		GlobalScriptProfileData.bStartOfFrameCalled = TRUE		
		GlobalScriptProfileData.iCurrentParentID = -1
		
		GlobalScriptProfileData.iRowToRender = 0
		GlobalScriptProfileData.iFailedMarkerCount = 0
		GlobalScriptProfileData.iOpenNestGroups = 0
		
		REPEAT MAX_NUMBER_OF_SCRIPT_PROFILER_NESTS i
			GlobalScriptProfileData.iPreviousParentID[i] = -1
		ENDREPEAT
		
		REPEAT MAX_SCRIPT_PROFILE_MARKERS i
			GlobalScriptProfileData.MarkerData[i].bHasChildren = FALSE
			GlobalScriptProfileData.MarkerData[i].bIsLoopParent = FALSE
			GlobalScriptProfileData.MarkerData[i].bCalledThisFrame = FALSE
			GlobalScriptProfileData.MarkerData[i].iTime = 0
			GlobalScriptProfileData.MarkerData[i].iIns = 0				
		ENDREPEAT
		
		
		GlobalScriptProfileData.iLastMarkerTime = GET_SCRIPT_TIME_IN_MICROSECONDS() 
		GlobalScriptProfileData.iLastMarkerIns =  GET_NUMBER_OF_INSTRUCTIONS_EXECUTED() 	
		
	ENDIF
	//SUSPEND_COUNTING(FALSE)
	
	
ENDPROC

PROC ADD_SCRIPT_PROFILE_MARKER( STRING strName, BOOL bIsHidden=FALSE, BOOL bForceAdd=FALSE)
	
	SUSPEND_COUNTING(TRUE)
	
	INT iThisMarkerID = -1

	INT iTime = GET_SCRIPT_TIME_IN_MICROSECONDS() - GlobalScriptProfileData.iLastMarkerTime
	INT iInsThis = GET_NUMBER_OF_INSTRUCTIONS_EXECUTED()
	INT iIns = iInsThis - GlobalScriptProfileData.iLastMarkerIns
	
	IF IsThisScriptProfilerThreadActive()	

		IF (iTime < 0)
			INT iTime1 = GET_SCRIPT_TIME_IN_MICROSECONDS()
			SP_PRINT_TAG() PRINTLN(" NEG TIME - GET_SCRIPT_TIME_IN_MICROSECONDS = ", iTime1, " iLastMarkerTime = ", GlobalScriptProfileData.iLastMarkerTime	)
		ENDIF
		IF (iIns < 0)
			SP_PRINT_TAG() PRINTLN(" NEG INS - GET_NUMBER_OF_INSTRUCTIONS_EXECUTED = ", GET_NUMBER_OF_INSTRUCTIONS_EXECUTED(), " iLastMarkerIns = ", GlobalScriptProfileData.iLastMarkerIns	)
		ENDIF

		iThisMarkerID = GetMarkerIndexFromString(strName)
		
		IF (iThisMarkerID > -1)
		
//			//IF ((GlobalScriptProfileData.iLastMarkerID > iThisMarkerID) AND (iThisMarkerID > 0))
//			IF (iThisMarkerID = 17)
//				PRINTLN("ADD_SCRIPT_PROFILE_MARKER - iLastMarkerID ", GlobalScriptProfileData.iLastMarkerID, " > iThisMarkerID ", iThisMarkerID)
//				PRINTLN("ADD_SCRIPT_PROFILE_MARKER - iInsThis ", iInsThis, " GlobalScriptProfileData.iLastMarkerIns ", GlobalScriptProfileData.iLastMarkerIns, " iIns = ", iIns)
//			ENDIF
		
			GlobalScriptProfileData.MarkerData[iThisMarkerID].bIsHidden = bIsHidden
		
			// update this marker's data
			IF GlobalScriptProfileData.MarkerData[iThisMarkerID].iIndex = -1
			OR GlobalScriptProfileData.MarkerData[iThisMarkerID].iIndex = iThisMarkerID
			OR (bForceAdd)
			
				IF ((iTime >= 0) OR (iIns >= 0))
				OR (bForceAdd)

					IF NOT (GlobalScriptProfileData.MarkerData[iThisMarkerID].bCalledThisFrame) // only update if not been called already this frame
					OR (bForceAdd)
					OR (GlobalScriptProfileData.bLoopProfilerStarted)
					
						// if the parent has changed then it must be getting called from somewhere else
						IF (GlobalScriptProfileData.MarkerData[iThisMarkerID].iIndex != -1)
						AND (GlobalScriptProfileData.MarkerData[iThisMarkerID].iParentID != -1)
						AND (GlobalScriptProfileData.iCurrentParentID != GlobalScriptProfileData.MarkerData[iThisMarkerID].iParentID)
						
							SP_PRINT_TAG() PRINTLN("ADD_SCRIPT_PROFILE_MARKER - already registered in another location!. ", strName, ", ID ", iThisMarkerID, " hash ", GlobalScriptProfileData.iMarkerIDs[iThisMarkerID])

						
						ELSE
						
					
							IF (GlobalScriptProfileData.MarkerData[iThisMarkerID].iIndex = -1)
								SP_PRINT_TAG() PRINTLN("ADD_SCRIPT_PROFILE_MARKER - new marker registerd. ", strName, ", ID ", iThisMarkerID, " hash ", GlobalScriptProfileData.iMarkerIDs[iThisMarkerID])
								DEBUG_PRINTCALLSTACK()
								GlobalScriptProfileData.iMarkerCount++
							ENDIF
						
							GlobalScriptProfileData.MarkerData[iThisMarkerID].iHash = GlobalScriptProfileData.iMarkerIDs[iThisMarkerID]
							GlobalScriptProfileData.MarkerData[iThisMarkerID].iIndex = iThisMarkerID
							GlobalScriptProfileData.MarkerData[iThisMarkerID].iParentID = GlobalScriptProfileData.iCurrentParentID
							
							IF (GlobalScriptProfileData.bLoopProfilerStarted)
								GlobalScriptProfileData.MarkerData[iThisMarkerID].iTime += iTime
								GlobalScriptProfileData.MarkerData[iThisMarkerID].iIns += iIns														
							ELSE
								GlobalScriptProfileData.MarkerData[iThisMarkerID].iTime = iTime
								GlobalScriptProfileData.MarkerData[iThisMarkerID].iIns = iIns
							ENDIF
							
							// get averages
							GlobalScriptProfileData.MarkerData[iThisMarkerID].iTimeTotal += iTime
							GlobalScriptProfileData.MarkerData[iThisMarkerID].iInsTotal += iIns
							
							IF NOT (GlobalScriptProfileData.MarkerData[iThisMarkerID].bCalledThisFrame)
								GlobalScriptProfileData.MarkerData[iThisMarkerID].iCalls++
							ENDIF
							
							//FLOAT fAverage
							//fAverage = TO_FLOAT(GlobalScriptProfileData.MarkerData[iThisMarkerID].iTimeTotal) / TO_FLOAT(GlobalScriptProfileData.MarkerData[iThisMarkerID].iCalls)
							//GlobalScriptProfileData.MarkerData[iThisMarkerID].iTimeAvg = FLOOR(fAverage)
							
							//fAverage = TO_FLOAT(GlobalScriptProfileData.MarkerData[iThisMarkerID].iInsTotal) / TO_FLOAT(GlobalScriptProfileData.MarkerData[iThisMarkerID].iCalls)
							//GlobalScriptProfileData.MarkerData[iThisMarkerID].iInsAvg = FLOOR(fAverage)
																			
							IF (GlobalScriptProfileData.MarkerData[iThisMarkerID].iTime > GlobalScriptProfileData.MarkerData[iThisMarkerID].iPeakTime)
								GlobalScriptProfileData.MarkerData[iThisMarkerID].iPeakTime = GlobalScriptProfileData.MarkerData[iThisMarkerID].iTime
							ENDIF
							IF (GlobalScriptProfileData.MarkerData[iThisMarkerID].iIns > GlobalScriptProfileData.MarkerData[iThisMarkerID].iPeakIns)
								GlobalScriptProfileData.MarkerData[iThisMarkerID].iPeakIns = GlobalScriptProfileData.MarkerData[iThisMarkerID].iIns
							ENDIF
							
							GlobalScriptProfileData.MarkerData[iThisMarkerID].bCalledThisFrame = TRUE
							GlobalScriptProfileData.MarkerData[iThisMarkerID].strName = strName
							GlobalScriptProfileData.MarkerData[iThisMarkerID].bIsLoopChild = GlobalScriptProfileData.bLoopProfilerStarted
							GlobalScriptProfileData.iLastMarkerID = iThisMarkerID
							
							GlobalScriptProfileData.iLastMarkerTime = GET_SCRIPT_TIME_IN_MICROSECONDS()	
							GlobalScriptProfileData.iLastMarkerIns = GET_NUMBER_OF_INSTRUCTIONS_EXECUTED()
							GlobalScriptProfileData.bCountingSuspended = FALSE
							
							GlobalScriptProfileData.iTotalAccumulatedTime += iTime
							GlobalScriptProfileData.iTotalAccumulatedCommands += iIns
							
							EXIT
							
						ENDIF
						
					ENDIF
					
				ENDIF
				
			ENDIF
		ELSE
			GlobalScriptProfileData.iFailedMarkerCount++
		ENDIF
	ENDIF
	
	SUSPEND_COUNTING(FALSE)
	
	
ENDPROC


PROC NEST_SCRIPT_PROFILE_MARKERS(BOOL bNest, BOOL bLoop=FALSE)

	SUSPEND_COUNTING(TRUE)

	IF IsThisScriptProfilerThreadActive()
		IF (bNest)	
			IF (GlobalScriptProfileData.iOpenNestGroups < MAX_NUMBER_OF_SCRIPT_PROFILER_NESTS)
				GlobalScriptProfileData.iPreviousParentID[GlobalScriptProfileData.iOpenNestGroups] = GlobalScriptProfileData.iCurrentParentID
				GlobalScriptProfileData.iCurrentParentID = GlobalScriptProfileData.iLastMarkerID	
				GlobalScriptProfileData.MarkerData[GlobalScriptProfileData.iCurrentParentID].bHasChildren = TRUE	
				GlobalScriptProfileData.MarkerData[GlobalScriptProfileData.iCurrentParentID].bIsLoopParent = bLoop
				GlobalScriptProfileData.iOpenNestGroups++
			ELSE
				SCRIPT_ASSERT("NEST_SCRIPT_PROFILE_MARKERS - hit MAX_NUMBER_OF_SCRIPT_PROFILER_NESTS ")	
			ENDIF
		ELSE
			IF (GlobalScriptProfileData.iOpenNestGroups > 0)
				GlobalScriptProfileData.iOpenNestGroups--
			ELSE
				SCRIPT_ASSERT("NEST_SCRIPT_PROFILE_MARKERS - open nest groups < 0 ")		
			ENDIF
			GlobalScriptProfileData.iCurrentParentID = GlobalScriptProfileData.iPreviousParentID[GlobalScriptProfileData.iOpenNestGroups]
		ENDIF
	ENDIF
	
	SUSPEND_COUNTING(FALSE)
	
ENDPROC	

PROC OPEN_SCRIPT_PROFILE_MARKER_GROUP(STRING strName)
	ADD_SCRIPT_PROFILE_MARKER(strName, FALSE, TRUE)
	NEST_SCRIPT_PROFILE_MARKERS(TRUE)
ENDPROC

PROC CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	IF IsThisScriptProfilerThreadActive()
		TEXT_LABEL_63 str = ""
		str += GlobalScriptProfileData.MarkerData[GlobalScriptProfileData.iCurrentParentID].iIndex
		str += " end group"
		ADD_SCRIPT_PROFILE_MARKER(str, TRUE, TRUE)
	ENDIF
	NEST_SCRIPT_PROFILE_MARKERS(FALSE)
ENDPROC



PROC ADD_SCRIPT_PROFILE_PRE_LOOP_MARKER(STRING strName)

	SUSPEND_COUNTING(TRUE)
	
	IF IsThisScriptProfilerThreadActive()		
	
		IF NOT (GlobalScriptProfileData.bLoopProfilerStarted)						
			ADD_SCRIPT_PROFILE_MARKER(strName, FALSE, TRUE)
			NEST_SCRIPT_PROFILE_MARKERS(TRUE, TRUE)
			GlobalScriptProfileData.bLoopProfilerStarted = TRUE
		ELSE
			SCRIPT_ASSERT("ADD_SCRIPT_PROFILE_PRE_LOOP_MARKER - called without an END")
		ENDIF			

	ENDIF
	
	SUSPEND_COUNTING(FALSE)
	
ENDPROC


PROC ADD_SCRIPT_PROFILE_POST_LOOP_MARKER()

	SUSPEND_COUNTING(TRUE)

	IF IsThisScriptProfilerThreadActive()

		IF (GlobalScriptProfileData.bLoopProfilerStarted)		
			CLOSE_SCRIPT_PROFILE_MARKER_GROUP()									
			GlobalScriptProfileData.bLoopProfilerStarted = FALSE	
		ELSE
			SCRIPT_ASSERT("END_SCRIPT_PROFILE_ACCUMULATOR - called without a START")
		ENDIF
				
	ENDIF
	
	SUSPEND_COUNTING(FALSE)
	
ENDPROC




#ENDIF
#ENDIF

