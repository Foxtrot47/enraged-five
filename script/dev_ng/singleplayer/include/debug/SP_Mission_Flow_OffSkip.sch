//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Authors:Ben Rollinson					Date: 20/10/11			│
//│						Kenneth Ross											│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					  Mission Flow Off-Mission J-Skip Header					│
//│																				│
//│			DESCRIPTION: All debug script used to manage off-mission			│
//│						 J-skipping.											│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"
USING "script_clock.sch"
USING "commands_pad.sch"
USING "player_ped_public.sch"
USING "flow_debug_game.sch"
USING "flow_public_core_override.sch"
#if not USE_CLF_DLC
#if not USE_NRM_DLC
USING "randomChar_Public.sch"
#endif
#endif


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ Constants  ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

CONST_FLOAT		DEBUG_OFFSKIP_MENU_ITEM_SPACING		0.04

CONST_FLOAT		DEBUG_OFFSKIP_SCREEN_TEXT_Y			0.06
CONST_FLOAT		DEBUG_OFFSKIP_TEXT_SIZE_X			0.48
CONST_FLOAT		DEBUG_OFFSKIP_TEXT_SIZE_Y			0.44
CONST_FLOAT		DEBUG_OFFSKIP_COMMS_TEXT_SIZE_X		0.34
CONST_FLOAT		DEBUG_OFFSKIP_COMMS_TEXT_SIZE_Y		0.29
CONST_INT		DEBUG_OFFSKIP_TEXT_TIME				4000
CONST_INT		DEBUG_OFFSKIP_MENU_INPUT_DELAY		125
CONST_FLOAT		DEBUG_OFFSKIP_HOLD_TIME				0.8
CONST_INT		DEBUG_OFFSKIP_MENU_ITEM_DEPTH		10
CONST_FLOAT		DEBUG_OFFSKIP_MENU_COLUMN_WIDTH		0.14
CONST_FLOAT		DEBUG_OFFSKIP_MENU_ROW_HEIGHT		0.035


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ Enums  ╞═══════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

ENUM OffSkipMenuColumns
	OFFSKIP_COLUMN_MISSIONS,
	OFFSKIP_COLUMN_RC_MISSIONS,
	OFFSKIP_COLUMN_STRAND_DELAYS,
	OFFSKIP_COLUMN_COMMS_DELAYS,
	
	//Should always be last.
	OFFSKIP_MAX_COLUMNS
ENDENUM

ENUM OffSkipFunction
	OFFSKIP_SKIP_FAIL_SCREEN,
	OFFSKIP_WARP_TO_MISSION,
	OFFSKIP_SKIP_STRAND_DELAY,
	OFFSKIP_SKIP_COMMS_DELAY,
	OFFSKIP_WARP_TO_RC_MISSION,
	OFFSKIP_SWITCH_CHARACTER,
	
	//Should always be last.
	OFFSKIP_MAX_FUNCTIONS
ENDENUM


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ Variables  ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//General.
INT iInputDelayTimer = 0
BOOL bOffSkipFunctionAvailable[OFFSKIP_MAX_FUNCTIONS]

//On-screen messages.
STRING sCurrentOffSkipMessage
INT iOffSkipDisplayMessageUntil

//Menu.
BOOL m_bOffSkipMenuActive = FALSE
BOOL m_bOffSkipMenuSelectionMade = FALSE
BOOL m_bOffSkipMenuAutoCompleteMade = FALSE
INT m_iTimeLastMenuInput
INT m_iCurrentColumnSelected = 0
INT m_iCurrentRowSelected = 0


STRUCT OffSkipMenuMission
	TEXT_LABEL_31 			name
	INT						index
	VECTOR					coords
ENDSTRUCT
OffSkipMenuMission m_sMenuMissions[DEBUG_OFFSKIP_MENU_ITEM_DEPTH]
OffSkipMenuMission m_sMenuRCs[DEBUG_OFFSKIP_MENU_ITEM_DEPTH]

STRUCT OffSkipStrandDelay
	INT				strandIndex 
	INT				commandPos
ENDSTRUCT
OffSkipStrandDelay m_sMenuStrandDelays[DEBUG_OFFSKIP_MENU_ITEM_DEPTH]

STRUCT OffSkipCommsDelay
	TEXT_LABEL_63 	name
	INT				queueIndex
	CC_CommID		commID
ENDSTRUCT
OffSkipCommsDelay	m_sMenuCommDelays[DEBUG_OFFSKIP_MENU_ITEM_DEPTH]

INT m_iMenuColumItemCount[OFFSKIP_MAX_COLUMNS]

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ Text Management ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC Configure_Menu_Text(INT r, INT g, INT b, INT a)
	SET_TEXT_SCALE (0.38, 0.34)
	SET_TEXT_WRAP(0.0, 1.0)
	SET_TEXT_DROPSHADOW (0,0,0,0,255)
	SET_TEXT_COLOUR(r,g,b,a)
	SET_TEXT_EDGE (0,0,0,0,255)     
	SET_TEXT_PROPORTIONAL (FALSE)                                                 
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC


PROC Configure_Menu_Comms_Text(INT r, INT g, INT b, INT a)
	SET_TEXT_SCALE (0.30, 0.24)
	SET_TEXT_WRAP(0.0, 1.0)
	SET_TEXT_DROPSHADOW (0,0,0,0,255)
	SET_TEXT_COLOUR(r,g,b,a)
	SET_TEXT_EDGE (0,0,0,0,255)     
	SET_TEXT_PROPORTIONAL (FALSE)                                                 
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC


PROC Configure_On_Screen_Text()
	SET_TEXT_SCALE (DEBUG_OFFSKIP_TEXT_SIZE_X, DEBUG_OFFSKIP_TEXT_SIZE_Y)
	SET_TEXT_WRAP(0.0, 1.0)
	SET_TEXT_DROPSHADOW (0,0,0,0,255)
	SET_TEXT_COLOUR(0,200,0,180)
	SET_TEXT_EDGE (0,0,0,0,255)     
	SET_TEXT_PROPORTIONAL (FALSE)                                                 
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC


PROC Update_OffSkip_Screen_Text()
	IF GET_GAME_TIMER() < iOffSkipDisplayMessageUntil
		Configure_On_Screen_Text()
		FLOAT fHalfStringWidth = GET_STRING_WIDTH_WITH_STRING("STRING", sCurrentOffSkipMessage) * 0.5
		DISPLAY_TEXT_WITH_LITERAL_STRING(	0.5 - fHalfStringWidth, 
											DEBUG_OFFSKIP_SCREEN_TEXT_Y, 
											"STRING", 
											sCurrentOffSkipMessage)
	ENDIF
ENDPROC


PROC Print_OffSkip_Info_Text(STRING sMessage)
	TEXT_LABEL_63 tMessage
	
	//Print message to console log.
	tMessage = "<SKIP> "
	tMessage += sMessage
	CPRINTLN(DEBUG_FLOW, tMessage)

	//Set up message to display on screen.
	sCurrentOffSkipMessage = sMessage
	iOffSkipDisplayMessageUntil = GET_GAME_TIMER() + DEBUG_OFFSKIP_TEXT_TIME
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ Menu Data Management ╞═══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC Populate_OffSkip_Menu_Missions()
	INT index

	//Reset count.
	m_iMenuColumItemCount[OFFSKIP_COLUMN_MISSIONS] = 0

	//Store the player's current character.
	enumCharacterList ePlayerChar
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		ePlayerChar = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
	ENDIF
	
	
#IF USE_CLF_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU index
		IF NOT (g_availableMissionsTU[index].index = ILLEGAL_ARRAY_POSITION)
		
			//Get the flow variable index.
			INT coreVarsIndex = g_availableMissionsTU[index].index
#ENDIF
#IF USE_NRM_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU index
		IF NOT (g_availableMissionsTU[index].index = ILLEGAL_ARRAY_POSITION)
		
			//Get the flow variable index.
			INT coreVarsIndex = g_availableMissionsTU[index].index
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	REPEAT MAX_MISSIONS_AVAILABLE index
		IF NOT (g_availableMissions[index].index = ILLEGAL_ARRAY_POSITION)
		
			//Get the flow variable index.
			INT coreVarsIndex = g_availableMissions[index].index
#ENDIF
#ENDIF
			//Get the mission data index.
			INT missionDataIndex = g_flowUnsaved.coreVars[coreVarsIndex].iValue1
			
			//Get the playable character bitset.
			INT triggerCharBitset = g_sMissionStaticData[missionDataIndex].triggerCharBitset

			//Check if we are the correct playable character.
			IF Is_Mission_Triggerable_By_Character(triggerCharBitset, ePlayerChar)
	
				//Get the Blip that this mission starts at and check it's valid.
				STATIC_BLIP_NAME_ENUM theBlip = g_sMissionStaticData[missionDataIndex].blip
				IF NOT (theBlip = STATIC_BLIP_NAME_DUMMY_FINAL)
				
					//Found mission. Store data in menu arrays.
					IF m_iMenuColumItemCount[OFFSKIP_COLUMN_MISSIONS] >= DEBUG_OFFSKIP_MENU_ITEM_DEPTH
						SCRIPT_ASSERT("Populate_OffSkip_Menu_Missions: Ran out of space in menu arrays. Does DEBUG_OFFSKIP_MENU_ITEM_DEPTH need increasing? Tell BenR.")
						EXIT
					ENDIF
					
					TEXT_LABEL txtMissionLabel = GET_SP_MISSION_NAME_LABEL(INT_TO_ENUM(SP_MISSIONS, missionDataIndex))
					TEXT_LABEL_31 txtMissionName = GET_STRING_FROM_TEXT_FILE(txtMissionLabel)
				
					m_sMenuMissions[m_iMenuColumItemCount[OFFSKIP_COLUMN_MISSIONS]].name = txtMissionName
					m_sMenuMissions[m_iMenuColumItemCount[OFFSKIP_COLUMN_MISSIONS]].index = missionDataIndex
					m_sMenuMissions[m_iMenuColumItemCount[OFFSKIP_COLUMN_MISSIONS]].coords = g_GameBlips[theBlip].vCoords[0]
					m_iMenuColumItemCount[OFFSKIP_COLUMN_MISSIONS]++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


PROC Populate_OffSkip_Menu_RC_Missions()
	//Deactivate RC Missions for certain DLC
	#IF NOT USE_CLF_DLC
	#if not USE_NRM_DLC
		
		//Reset count.
		m_iMenuColumItemCount[OFFSKIP_COLUMN_RC_MISSIONS] = 0
		
		g_structRCMissionsStatic sRCMissionDetails
		
		INT index
		REPEAT MAX_RC_MISSIONS index
			//Check if this RC mission is available.
			IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[index].rcFlags, ENUM_TO_INT(RC_FLAG_ACTIVATED))
			AND IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[index].rcFlags, ENUM_TO_INT(RC_FLAG_ACTIVATED_IN_FLOW))
			AND NOT IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[index].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
				
				//Get RC mission data.
				Retrieve_Random_Character_Static_Mission_Details(INT_TO_ENUM(g_eRC_MissionIDs, index), sRCMissionDetails)
				
				//Check if we are the correct playable character.
				IF IS_BIT_SET(sRCMissionDetails.rcPlayableChars, ENUM_TO_INT(GET_CURRENT_PLAYER_PED_ENUM()))
				
					//Found RC mission. Store data in menu arrays.
					IF m_iMenuColumItemCount[OFFSKIP_COLUMN_RC_MISSIONS] >= DEBUG_OFFSKIP_MENU_ITEM_DEPTH
						SCRIPT_ASSERT("Populate_OffSkip_Menu_RC_Missions: Ran out of space in menu arrays. Does DEBUG_OFFSKIP_MENU_ITEM_DEPTH need increasing? Tell BenR.")
						EXIT
					ENDIF

					m_sMenuRCs[m_iMenuColumItemCount[OFFSKIP_COLUMN_RC_MISSIONS]].name = sRCMissionDetails.rcScriptName
					m_sMenuRCs[m_iMenuColumItemCount[OFFSKIP_COLUMN_RC_MISSIONS]].index = index
					m_sMenuRCs[m_iMenuColumItemCount[OFFSKIP_COLUMN_RC_MISSIONS]].coords = sRCMissionDetails.rcCoords
						
					m_iMenuColumItemCount[OFFSKIP_COLUMN_RC_MISSIONS]++
				ENDIF
			ENDIF
		ENDREPEAT
	#ENDIF
	#ENDIF
ENDPROC

#if USE_CLF_DLC
PROC Populate_OffSkip_Menu_Strand_Delays_CLF()
	//Reset count.
	m_iMenuColumItemCount[OFFSKIP_COLUMN_STRAND_DELAYS] = 0
	
	INT index
	REPEAT MAX_STRANDS_CLF index
		//Check the current command this strand is on.
		FLOW_COMMANDS sCurrentCommand = g_flowUnsaved.flowCommands[g_savedGlobalsClifford.sFlow.strandSavedVars[index].thisCommandPos]
		
		//Is it a wait command?
		SWITCH(sCurrentCommand.command)
			CASE FLOW_WAIT_IN_REAL_TIME
			CASE FLOW_WAIT_IN_GAME_TIME
			CASE FLOW_WAIT_UNTIL_GAME_HOUR
			CASE FLOW_WAIT_UNTIL_GAME_TIME_AND_DATE
				
				//Found strand delay. Store data in menu arrays.
				IF m_iMenuColumItemCount[OFFSKIP_COLUMN_STRAND_DELAYS] >= DEBUG_OFFSKIP_MENU_ITEM_DEPTH
					SCRIPT_ASSERT("Populate_OffSkip_Menu_Strand_Delays: Ran out of space in menu arrays. Does DEBUG_OFFSKIP_MENU_ITEM_DEPTH need increasing? Tell BenR.")
					EXIT
				ENDIF

				m_sMenuStrandDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_STRAND_DELAYS]].strandIndex = index
				m_sMenuStrandDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_STRAND_DELAYS]].commandPos = g_savedGlobalsClifford.sFlow.strandSavedVars[index].thisCommandPos
				
				m_iMenuColumItemCount[OFFSKIP_COLUMN_STRAND_DELAYS]++
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC
#endif
#if USE_NRM_DLC
PROC Populate_OffSkip_Menu_Strand_Delays_NRM()
	//Reset count.
	m_iMenuColumItemCount[OFFSKIP_COLUMN_STRAND_DELAYS] = 0
	
	INT index
	REPEAT MAX_STRANDS_NRM index
		//Check the current command this strand is on.
		FLOW_COMMANDS sCurrentCommand = g_flowUnsaved.flowCommands[g_savedGlobalsnorman.sFlow.strandSavedVars[index].thisCommandPos]
		
		//Is it a wait command?
		SWITCH(sCurrentCommand.command)
			CASE FLOW_WAIT_IN_REAL_TIME
			CASE FLOW_WAIT_IN_GAME_TIME
			CASE FLOW_WAIT_UNTIL_GAME_HOUR
			CASE FLOW_WAIT_UNTIL_GAME_TIME_AND_DATE
				
				//Found strand delay. Store data in menu arrays.
				IF m_iMenuColumItemCount[OFFSKIP_COLUMN_STRAND_DELAYS] >= DEBUG_OFFSKIP_MENU_ITEM_DEPTH
					SCRIPT_ASSERT("Populate_OffSkip_Menu_Strand_Delays: Ran out of space in menu arrays. Does DEBUG_OFFSKIP_MENU_ITEM_DEPTH need increasing? Tell BenR.")
					EXIT
				ENDIF

				m_sMenuStrandDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_STRAND_DELAYS]].strandIndex = index
				m_sMenuStrandDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_STRAND_DELAYS]].commandPos = g_savedGlobalsnorman.sFlow.strandSavedVars[index].thisCommandPos
				
				m_iMenuColumItemCount[OFFSKIP_COLUMN_STRAND_DELAYS]++
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC
#endif
PROC Populate_OffSkip_Menu_Strand_Delays()
	#IF USE_CLF_DLC
		Populate_OffSkip_Menu_Strand_Delays_CLF()
		exit
	#ENDIF
	#IF USE_NRM_DLC
		Populate_OffSkip_Menu_Strand_Delays_NRM()
		exit
	#ENDIF
	
	#IF NOT USE_CLF_DLC
	#if not USE_NRM_DLC
	//Reset count.
	m_iMenuColumItemCount[OFFSKIP_COLUMN_STRAND_DELAYS] = 0
	
	INT index
	REPEAT MAX_STRANDS index
		//Check the current command this strand is on.
		FLOW_COMMANDS sCurrentCommand = g_flowUnsaved.flowCommands[g_savedGlobals.sFlow.strandSavedVars[index].thisCommandPos]
		
		//Is it a wait command?
		SWITCH(sCurrentCommand.command)
			CASE FLOW_WAIT_IN_REAL_TIME
			CASE FLOW_WAIT_IN_GAME_TIME
			CASE FLOW_WAIT_UNTIL_GAME_HOUR
			CASE FLOW_WAIT_UNTIL_GAME_TIME_AND_DATE
				
				//Found strand delay. Store data in menu arrays.
				IF m_iMenuColumItemCount[OFFSKIP_COLUMN_STRAND_DELAYS] >= DEBUG_OFFSKIP_MENU_ITEM_DEPTH
					SCRIPT_ASSERT("Populate_OffSkip_Menu_Strand_Delays: Ran out of space in menu arrays. Does DEBUG_OFFSKIP_MENU_ITEM_DEPTH need increasing? Tell BenR.")
					EXIT
				ENDIF

				m_sMenuStrandDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_STRAND_DELAYS]].strandIndex = index
				m_sMenuStrandDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_STRAND_DELAYS]].commandPos = g_savedGlobals.sFlow.strandSavedVars[index].thisCommandPos
				
				m_iMenuColumItemCount[OFFSKIP_COLUMN_STRAND_DELAYS]++
			BREAK
		ENDSWITCH
	ENDREPEAT
	#ENDIF
	#endif
ENDPROC

PROC Populate_OffSkip_Menu_Comm_Delays_CLF()
	//Reset count.
	m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS] = 0
	
	//Check call queue.
	INT index
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls index
		//Is this call for the current player character?
		IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Is this call high enough priority?
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.ePriority = g_savedGlobalsClifford.sCommsControlData.eCharacterPriorityLevel[GET_CURRENT_PLAYER_PED_ENUM()]
				
				//Found communication delay. Store data in menu arrays.
				IF m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS] >= DEBUG_OFFSKIP_MENU_ITEM_DEPTH
					SCRIPT_ASSERT("Populate_OffSkip_Menu_Comm_Delays: Ran out of space in menu arrays. Does DEBUG_OFFSKIP_MENU_ITEM_DEPTH need increasing? Tell BenR.")
					EXIT
				ENDIF
				
				TEXT_LABEL_63 tCommName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.eID)
				
				m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].name = tCommName
				m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].queueIndex = index
				m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].commID = g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.eID
				
				m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]++
			ENDIF
		ENDIF	
	ENDREPEAT
	
	//Check text message queue.
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts index
		//Is this text for the current player character?
		IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Found communication delay. Store data in menu arrays.
			IF m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS] >= DEBUG_OFFSKIP_MENU_ITEM_DEPTH
				SCRIPT_ASSERT("Populate_OffSkip_Menu_Comm_Delays: Ran out of space in menu arrays. Does DEBUG_OFFSKIP_MENU_ITEM_DEPTH need increasing? Tell BenR.")
				EXIT
			ENDIF
			TEXT_LABEL_63 tCommName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.eID)

			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].name = tCommName
			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].queueIndex = index
			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].commID = g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.eID
				
			m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]++
		ENDIF
	ENDREPEAT
	
	//Check email queue.
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedEmails index
		//Is this email for the current player character?
		IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Found communication delay. Store data in menu arrays.
			IF m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS] >= DEBUG_OFFSKIP_MENU_ITEM_DEPTH
				SCRIPT_ASSERT("Populate_OffSkip_Menu_Comm_Delays: Ran out of space in menu arrays. Does DEBUG_OFFSKIP_MENU_ITEM_DEPTH need increasing? Tell BenR.")
				EXIT
			ENDIF
			TEXT_LABEL_63 tCommName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.eID)

			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].name = tCommName
			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].queueIndex = index
			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].commID = g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.eID
				
			m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]++
		ENDIF
	ENDREPEAT
ENDPROC
PROC Populate_OffSkip_Menu_Comm_Delays_NRM()
	//Reset count.
	m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS] = 0
	
	//Check call queue.
	INT index
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls index
		//Is this call for the current player character?
		IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Is this call high enough priority?
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.ePriority = g_savedGlobalsnorman.sCommsControlData.eCharacterPriorityLevel[GET_CURRENT_PLAYER_PED_ENUM()]
				
				//Found communication delay. Store data in menu arrays.
				IF m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS] >= DEBUG_OFFSKIP_MENU_ITEM_DEPTH
					SCRIPT_ASSERT("Populate_OffSkip_Menu_Comm_Delays: Ran out of space in menu arrays. Does DEBUG_OFFSKIP_MENU_ITEM_DEPTH need increasing? Tell BenR.")
					EXIT
				ENDIF
				
				TEXT_LABEL_63 tCommName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.eID)
				
				m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].name = tCommName
				m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].queueIndex = index
				m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].commID = g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.eID
				
				m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]++
			ENDIF
		ENDIF	
	ENDREPEAT
	
	//Check text message queue.
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts index
		//Is this text for the current player character?
		IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Found communication delay. Store data in menu arrays.
			IF m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS] >= DEBUG_OFFSKIP_MENU_ITEM_DEPTH
				SCRIPT_ASSERT("Populate_OffSkip_Menu_Comm_Delays: Ran out of space in menu arrays. Does DEBUG_OFFSKIP_MENU_ITEM_DEPTH need increasing? Tell BenR.")
				EXIT
			ENDIF
			TEXT_LABEL_63 tCommName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.eID)

			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].name = tCommName
			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].queueIndex = index
			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].commID = g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.eID
				
			m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]++
		ENDIF
	ENDREPEAT
	
	//Check email queue.
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedEmails index
		//Is this email for the current player character?
		IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Found communication delay. Store data in menu arrays.
			IF m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS] >= DEBUG_OFFSKIP_MENU_ITEM_DEPTH
				SCRIPT_ASSERT("Populate_OffSkip_Menu_Comm_Delays: Ran out of space in menu arrays. Does DEBUG_OFFSKIP_MENU_ITEM_DEPTH need increasing? Tell BenR.")
				EXIT
			ENDIF
			TEXT_LABEL_63 tCommName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.eID)

			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].name = tCommName
			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].queueIndex = index
			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].commID = g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.eID
				
			m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]++
		ENDIF
	ENDREPEAT
ENDPROC

PROC Populate_OffSkip_Menu_Comm_Delays()
	#IF USE_CLF_DLC
		Populate_OffSkip_Menu_Comm_Delays_CLF()
		EXIT
	#ENDIF
	#IF USE_NRM_DLC
		Populate_OffSkip_Menu_Comm_Delays_NRM()
		EXIT
	#ENDIF
	#IF NOT USE_CLF_DLC
	#if not USE_NRM_DLC
	//Reset count.
	m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS] = 0
	
	//Check call queue.
	INT index
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls index
		//Is this call for the current player character?
		IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Is this call high enough priority?
			IF g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.ePriority = g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[GET_CURRENT_PLAYER_PED_ENUM()]
				
				//Found communication delay. Store data in menu arrays.
				IF m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS] >= DEBUG_OFFSKIP_MENU_ITEM_DEPTH
					SCRIPT_ASSERT("Populate_OffSkip_Menu_Comm_Delays: Ran out of space in menu arrays. Does DEBUG_OFFSKIP_MENU_ITEM_DEPTH need increasing? Tell BenR.")
					EXIT
				ENDIF
				
				TEXT_LABEL_63 tCommName = GET_COMM_ID_DEBUG_STRING(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eID)
				
				m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].name = tCommName
				m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].queueIndex = index
				m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].commID = g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eID
				
				m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]++
			ENDIF
		ENDIF	
	ENDREPEAT
	
	//Check text message queue.
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedTexts index
		//Is this text for the current player character?
		IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Found communication delay. Store data in menu arrays.
			IF m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS] >= DEBUG_OFFSKIP_MENU_ITEM_DEPTH
				SCRIPT_ASSERT("Populate_OffSkip_Menu_Comm_Delays: Ran out of space in menu arrays. Does DEBUG_OFFSKIP_MENU_ITEM_DEPTH need increasing? Tell BenR.")
				EXIT
			ENDIF
			TEXT_LABEL_63 tCommName = GET_COMM_ID_DEBUG_STRING(g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.eID)

			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].name = tCommName
			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].queueIndex = index
			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].commID = g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.eID
				
			m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]++
		ENDIF
	ENDREPEAT
	
	//Check email queue.
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedEmails index
		//Is this email for the current player character?
		IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Found communication delay. Store data in menu arrays.
			IF m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS] >= DEBUG_OFFSKIP_MENU_ITEM_DEPTH
				SCRIPT_ASSERT("Populate_OffSkip_Menu_Comm_Delays: Ran out of space in menu arrays. Does DEBUG_OFFSKIP_MENU_ITEM_DEPTH need increasing? Tell BenR.")
				EXIT
			ENDIF
			TEXT_LABEL_63 tCommName = GET_COMM_ID_DEBUG_STRING(g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.eID)

			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].name = tCommName
			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].queueIndex = index
			m_sMenuCommDelays[m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]].commID = g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.eID
				
			m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]++
		ENDIF
	ENDREPEAT
	#ENDIF
	#endif
ENDPROC


PROC Populate_OffSkip_Menu_Data()
	Populate_OffSkip_Menu_Missions()
	Populate_OffSkip_Menu_RC_Missions()
	Populate_OffSkip_Menu_Strand_Delays()
	Populate_OffSkip_Menu_Comm_Delays()
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════╡ Individual Off-Skip Functions ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC Perform_OffSkip_Skip_Fail_Screen()
	Print_OffSkip_Info_Text("Performing skip fail screen.")
	
	g_bDebugSkipReplayScreen = TRUE
ENDPROC


PROC Perform_OffSkip_Warp_To_Any_Mission()
	Print_OffSkip_Info_Text("Performing warp to mission.")
	
	//Store the player's current character.
	enumCharacterList ePlayerChar
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		ePlayerChar = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
	ENDIF
	
	INT tempLoop
#IF USE_CLF_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU tempLoop
		IF NOT (g_availableMissionsTU[tempLoop].index = ILLEGAL_ARRAY_POSITION)
			//Get the mission data array index.
			INT missionDataIndex = g_flowUnsaved.coreVars[g_availableMissionsTU[tempLoop].index].iValue1
#ENDIF
#IF USE_NRM_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU tempLoop
		IF NOT (g_availableMissionsTU[tempLoop].index = ILLEGAL_ARRAY_POSITION)
			//Get the mission data array index.
			INT missionDataIndex = g_flowUnsaved.coreVars[g_availableMissionsTU[tempLoop].index].iValue1
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
		REPEAT MAX_MISSIONS_AVAILABLE tempLoop
		IF NOT (g_availableMissions[tempLoop].index = ILLEGAL_ARRAY_POSITION)
			//Get the mission data array index.
			INT missionDataIndex = g_flowUnsaved.coreVars[g_availableMissions[tempLoop].index].iValue1
#ENDIF
#ENDIF
//Get the playable character bitset.
			INT triggerCharBitset = g_sMissionStaticData[missionDataIndex].triggerCharBitset
		
			//Check if we are the correct playable character.
			IF Is_Mission_Triggerable_By_Character(triggerCharBitset, ePlayerChar)
	
				IF ARE_VECTORS_EQUAL(g_sMissionDebugData[missionDataIndex].warpPosition, WARP_POS_MAIN_BLIP)
					// ...found an available mission
					// Get the Blip that this mission starts at
					STATIC_BLIP_NAME_ENUM theBlip = g_sMissionStaticData[missionDataIndex].blip
					
					// Tell any trigger scenes to auto trigger.
					g_bSceneAutoTrigger = TRUE
					
					// Is it a valid blip?
					IF NOT (theBlip = STATIC_BLIP_NAME_DUMMY_FINAL)
						//Multi char blip or not?
						IF IS_BIT_SET(g_GameBlips[theBlip].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)//g_GameBlips[theBlip].bMultiCoordAndSprite
							DO_PLAYER_MAP_WARP_WITH_LOAD(g_GameBlips[theBlip].vCoords[GET_CURRENT_PLAYER_PED_INT()])
						ELSE
							DO_PLAYER_MAP_WARP_WITH_LOAD(g_GameBlips[theBlip].vCoords[0])
						ENDIF
						EXIT
					ENDIF
				ELSE
					DO_PLAYER_MAP_WARP_WITH_LOAD(g_sMissionDebugData[missionDataIndex].warpPosition)
					EXIT
				ENDIF
				
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


PROC Perform_OffSkip_Skip_Any_Strand_Delay()
	Print_OffSkip_Info_Text("Performing strand delay skip.")
	
	FLOW_COMMANDS 	sCurrentCommand
	TIMEOFDAY				sTimeOfDay

	
	INT tempLoop
#IF USE_CLF_DLC
	REPEAT MAX_STRANDS_CLF tempLoop
		//Check the current command this strand is on.
		sCurrentCommand = g_flowUnsaved.flowCommands[g_savedGlobalsClifford.sFlow.strandSavedVars[tempLoop].thisCommandPos]
#ENDIF
#IF USE_NRM_DLC
	REPEAT MAX_STRANDS_NRM tempLoop
		//Check the current command this strand is on.
		sCurrentCommand = g_flowUnsaved.flowCommands[g_savedGlobalsnorman.sFlow.strandSavedVars[tempLoop].thisCommandPos]
#ENDIF

#IF NOT USE_CLF_DLC
#if not USE_NRM_DLC
	REPEAT MAX_STRANDS tempLoop
		//Check the current command this strand is on.
		sCurrentCommand = g_flowUnsaved.flowCommands[g_savedGlobals.sFlow.strandSavedVars[tempLoop].thisCommandPos]
#ENDIF	
#endif	
		//Is it a wait command?
		SWITCH(sCurrentCommand.command)
			CASE FLOW_WAIT_IN_REAL_TIME
				//Override the strand timer.
				g_flowUnsaved.iStrandTimer[tempLoop] = GET_GAME_TIMER()
				g_flowUnsaved.bStrandTimerSet[tempLoop] = TRUE

				EXIT
			BREAK
			
			CASE FLOW_WAIT_IN_GAME_TIME
				// Get current time
				sTimeOfDay = GET_CURRENT_TIMEOFDAY()
				//Add delay time.
				ADD_TIME_TO_TIMEOFDAY(sTimeOfDay,
					g_flowUnsaved.coreVars[sCurrentCommand.index].iValue3,
					g_flowUnsaved.coreVars[sCurrentCommand.index].iValue2,
					g_flowUnsaved.coreVars[sCurrentCommand.index].iValue1, 0, 0, 0)
				
				//Jump in-game time of day to the time of day saved for this strand.
				SET_CLOCK_TIME(	GET_TIMEOFDAY_HOUR(sTimeOfDay),
								GET_TIMEOFDAY_MINUTE(sTimeOfDay),
								GET_TIMEOFDAY_SECOND(sTimeOfDay))
				SET_CLOCK_DATE(	GET_TIMEOFDAY_DAY(sTimeOfDay),
								GET_TIMEOFDAY_MONTH(sTimeOfDay),
								GET_TIMEOFDAY_YEAR(sTimeOfDay))
				EXIT
			BREAK
			
			CASE FLOW_WAIT_UNTIL_GAME_HOUR
				//Store the current time of day into a struct.
				sTimeOfDay = GET_CURRENT_TIMEOFDAY()
				
				//If the current hour is after the required hour then we need to wait until the next day.
				IF GET_TIMEOFDAY_HOUR(sTimeOfDay) > g_flowUnsaved.miscellaneousVars[sCurrentCommand.index].anInt
					ADD_TIME_TO_TIMEOFDAY(sTimeOfDay, 0, 0, 0, 1, 0, 0)
				ENDIF
				//If the current time isn't already in the correct hour set the hour to wait till.
				IF GET_TIMEOFDAY_HOUR(sTimeOfDay) <> g_flowUnsaved.miscellaneousVars[sCurrentCommand.index].anInt
					SET_TIMEOFDAY_SECOND(sTimeOfDay, 0)
					SET_TIMEOFDAY_MINUTE(sTimeOfDay, 0)
					SET_TIMEOFDAY_HOUR(sTimeOfDay, g_flowUnsaved.miscellaneousVars[sCurrentCommand.index].anInt)
				ENDIF
				
				//Set the gameclock to the values in the time of day struct.
				SET_CLOCK_TIME(GET_TIMEOFDAY_HOUR(sTimeOfDay), GET_TIMEOFDAY_MINUTE(sTimeOfDay), GET_TIMEOFDAY_SECOND(sTimeOfDay))
				SET_CLOCK_DATE(GET_TIMEOFDAY_DAY(sTimeOfDay), INT_TO_ENUM(MONTH_OF_YEAR,GET_TIMEOFDAY_MONTH(sTimeOfDay)), GET_TIMEOFDAY_YEAR(sTimeOfDay))
				EXIT
			BREAK
			
			CASE FLOW_WAIT_UNTIL_GAME_TIME_AND_DATE
				//Jump in-game time of day to the time of day saved for this strand.
				sTimeOfDay = g_flowUnsaved.miscellaneousVars[sCurrentCommand.index].aTimeOfDay
				SET_CLOCK_TIME(	GET_TIMEOFDAY_HOUR(sTimeOfDay),
								GET_TIMEOFDAY_MINUTE(sTimeOfDay),
								GET_TIMEOFDAY_SECOND(sTimeOfDay))
				SET_CLOCK_DATE(	GET_TIMEOFDAY_DAY(sTimeOfDay),
								INT_TO_ENUM(MONTH_OF_YEAR, GET_TIMEOFDAY_MONTH(sTimeOfDay)),
								GET_TIMEOFDAY_YEAR(sTimeOfDay))
				EXIT
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC

PROC Perform_OffSkip_Skip_Any_Communication_Delay_CLF()
	Print_OffSkip_Info_Text("Performing communication delay skip CLF.")
	
	//Check call queue.
	INT tempLoop
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls tempLoop
		//Is this call for the current player character?
		IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[tempLoop].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Is this call high enough priority?
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[tempLoop].sCommData.ePriority = g_savedGlobalsClifford.sCommsControlData.eCharacterPriorityLevel[GET_CURRENT_PLAYER_PED_ENUM()]
				INT iGameTime = GET_GAME_TIMER()
				//Set global communication wait timer to game time.
				g_iGlobalWaitTime = iGameTime
				//Set character wait timer to game time.
				g_iCharWaitTime[g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[tempLoop].sCommData.eNPCCharacter] = iGameTime
				//Set call timer to 0.
				g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[tempLoop].sCommData.iQueueTime = iGameTime
				
				//Override any restricted area associated with this call if the player is in the blocking area.
				IF IS_PED_AT_VECTOR_ID(PLAYER_PED_ID(), g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[tempLoop].sCommData.eRestrictedAreaID)
					g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[tempLoop].sCommData.eRestrictedAreaID = VID_BLANK
				ENDIF
				
				EXIT
			ENDIF
		ENDIF	
	ENDREPEAT
	
	//Check text message queue.
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts tempLoop
		//Is this text for the current player character?
		IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[tempLoop].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Is this text message high enough priority?
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[tempLoop].sCommData.ePriority = g_savedGlobalsClifford.sCommsControlData.eCharacterPriorityLevel[GET_CURRENT_PLAYER_PED_ENUM()]
				INT iGameTime = GET_GAME_TIMER()
				
				//Set global communication wait timer to 0.
				g_iGlobalWaitTime = iGameTime
				//Set character wait timer to 0.
				g_iCharWaitTime[g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[tempLoop].sCommData.eNPCCharacter] = iGameTime
				//Set text message timer to 0.
				g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[tempLoop].sCommData.iQueueTime = iGameTime
				//Override any restricted area associated with this text.
				g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[tempLoop].sCommData.eRestrictedAreaID = VID_BLANK
				
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Check email queue.
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedEmails tempLoop
		//Is this email for the current player character?
		IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[tempLoop].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Is this email message high enough priority?
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[tempLoop].sCommData.ePriority = g_savedGlobalsClifford.sCommsControlData.eCharacterPriorityLevel[GET_CURRENT_PLAYER_PED_ENUM()]
				INT iGameTime = GET_GAME_TIMER()
				
				//Set global communication wait timer to 0.
				g_iGlobalWaitTime = iGameTime
				//Set character wait timer to 0.
				g_iCharWaitTime[g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[tempLoop].sCommData.eNPCCharacter] = iGameTime
				//Set email timer to 0.
				g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[tempLoop].sCommData.iQueueTime = iGameTime
				//Override any restricted area associated with this email.
				g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[tempLoop].sCommData.eRestrictedAreaID = VID_BLANK
				
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
PROC Perform_OffSkip_Skip_Any_Communication_Delay_NRM()
	Print_OffSkip_Info_Text("Performing communication delay skip NRM.")
	
	//Check call queue.
	INT tempLoop
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls tempLoop
		//Is this call for the current player character?
		IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[tempLoop].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Is this call high enough priority?
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[tempLoop].sCommData.ePriority = g_savedGlobalsnorman.sCommsControlData.eCharacterPriorityLevel[GET_CURRENT_PLAYER_PED_ENUM()]
				INT iGameTime = GET_GAME_TIMER()
				//Set global communication wait timer to game time.
				g_iGlobalWaitTime = iGameTime
				//Set character wait timer to game time.
				g_iCharWaitTime[g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[tempLoop].sCommData.eNPCCharacter] = iGameTime
				//Set call timer to 0.
				g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[tempLoop].sCommData.iQueueTime = iGameTime
				
				//Override any restricted area associated with this call if the player is in the blocking area.
				IF IS_PED_AT_VECTOR_ID(PLAYER_PED_ID(), g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[tempLoop].sCommData.eRestrictedAreaID)
					g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[tempLoop].sCommData.eRestrictedAreaID = VID_BLANK
				ENDIF
				
				EXIT
			ENDIF
		ENDIF	
	ENDREPEAT
	
	//Check text message queue.
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts tempLoop
		//Is this text for the current player character?
		IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[tempLoop].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Is this text message high enough priority?
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[tempLoop].sCommData.ePriority = g_savedGlobalsnorman.sCommsControlData.eCharacterPriorityLevel[GET_CURRENT_PLAYER_PED_ENUM()]
				INT iGameTime = GET_GAME_TIMER()
				
				//Set global communication wait timer to 0.
				g_iGlobalWaitTime = iGameTime
				//Set character wait timer to 0.
				g_iCharWaitTime[g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[tempLoop].sCommData.eNPCCharacter] = iGameTime
				//Set text message timer to 0.
				g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[tempLoop].sCommData.iQueueTime = iGameTime
				//Override any restricted area associated with this text.
				g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[tempLoop].sCommData.eRestrictedAreaID = VID_BLANK
				
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Check email queue.
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedEmails tempLoop
		//Is this email for the current player character?
		IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[tempLoop].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Is this email message high enough priority?
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[tempLoop].sCommData.ePriority = g_savedGlobalsnorman.sCommsControlData.eCharacterPriorityLevel[GET_CURRENT_PLAYER_PED_ENUM()]
				INT iGameTime = GET_GAME_TIMER()
				
				//Set global communication wait timer to 0.
				g_iGlobalWaitTime = iGameTime
				//Set character wait timer to 0.
				g_iCharWaitTime[g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[tempLoop].sCommData.eNPCCharacter] = iGameTime
				//Set email timer to 0.
				g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[tempLoop].sCommData.iQueueTime = iGameTime
				//Override any restricted area associated with this email.
				g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[tempLoop].sCommData.eRestrictedAreaID = VID_BLANK
				
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
PROC Perform_OffSkip_Skip_Any_Communication_Delay()
	#IF USE_CLF_DLC
		Perform_OffSkip_Skip_Any_Communication_Delay_CLF()
		EXIT
	#ENDIF
	#IF USE_NRM_DLC
		Perform_OffSkip_Skip_Any_Communication_Delay_NRM()
		EXIT
	#ENDIF
	Print_OffSkip_Info_Text("Performing communication delay skip.")
	//Check call queue.
	INT tempLoop
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls tempLoop
		//Is this call for the current player character?
		IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[tempLoop].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Is this call high enough priority?
			IF g_savedGlobals.sCommsControlData.sQueuedCalls[tempLoop].sCommData.ePriority = g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[GET_CURRENT_PLAYER_PED_ENUM()]
				INT iGameTime = GET_GAME_TIMER()
				//Set global communication wait timer to game time.
				g_iGlobalWaitTime = iGameTime
				//Set character wait timer to game time.
				g_iCharWaitTime[g_savedGlobals.sCommsControlData.sQueuedCalls[tempLoop].sCommData.eNPCCharacter] = iGameTime
				//Set call timer to 0.
				g_savedGlobals.sCommsControlData.sQueuedCalls[tempLoop].sCommData.iQueueTime = iGameTime
				
				//Override any restricted area associated with this call if the player is in the blocking area.
				IF IS_PED_AT_VECTOR_ID(PLAYER_PED_ID(), g_savedGlobals.sCommsControlData.sQueuedCalls[tempLoop].sCommData.eRestrictedAreaID)
					g_savedGlobals.sCommsControlData.sQueuedCalls[tempLoop].sCommData.eRestrictedAreaID = VID_BLANK
				ENDIF
				
				EXIT
			ENDIF
		ENDIF	
	ENDREPEAT
	
	//Check text message queue.
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedTexts tempLoop
		//Is this text for the current player character?
		IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedTexts[tempLoop].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Is this text message high enough priority?
			IF g_savedGlobals.sCommsControlData.sQueuedTexts[tempLoop].sCommData.ePriority = g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[GET_CURRENT_PLAYER_PED_ENUM()]
				INT iGameTime = GET_GAME_TIMER()
				
				//Set global communication wait timer to 0.
				g_iGlobalWaitTime = iGameTime
				//Set character wait timer to 0.
				g_iCharWaitTime[g_savedGlobals.sCommsControlData.sQueuedTexts[tempLoop].sCommData.eNPCCharacter] = iGameTime
				//Set text message timer to 0.
				g_savedGlobals.sCommsControlData.sQueuedTexts[tempLoop].sCommData.iQueueTime = iGameTime
				//Override any restricted area associated with this text.
				g_savedGlobals.sCommsControlData.sQueuedTexts[tempLoop].sCommData.eRestrictedAreaID = VID_BLANK
				
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Check email queue.
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedEmails tempLoop
		//Is this email for the current player character?
		IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedEmails[tempLoop].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Is this email message high enough priority?
			IF g_savedGlobals.sCommsControlData.sQueuedEmails[tempLoop].sCommData.ePriority = g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[GET_CURRENT_PLAYER_PED_ENUM()]
				INT iGameTime = GET_GAME_TIMER()
				
				//Set global communication wait timer to 0.
				g_iGlobalWaitTime = iGameTime
				//Set character wait timer to 0.
				g_iCharWaitTime[g_savedGlobals.sCommsControlData.sQueuedEmails[tempLoop].sCommData.eNPCCharacter] = iGameTime
				//Set email timer to 0.
				g_savedGlobals.sCommsControlData.sQueuedEmails[tempLoop].sCommData.iQueueTime = iGameTime
				//Override any restricted area associated with this email.
				g_savedGlobals.sCommsControlData.sQueuedEmails[tempLoop].sCommData.eRestrictedAreaID = VID_BLANK
				
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

// Forces the correct outoft on the character, if the upcoming mission 
// requires the player to wearing a specific outfit or clothing style.
PROC FORCE_CORRECT_RCM_OUTFIT(STRING sScriptName)
		
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		
		// Force robes outfit for Epsilon7
		IF ARE_STRINGS_EQUAL(sScriptName, "Epsilon7")
			IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON) // Michael
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON, FALSE)
			ENDIF
		// Force Michael's triathlon outfit for Fanatic1
		ELIF ARE_STRINGS_EQUAL(sScriptName, "Fanatic1")
			IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_TRIATHLON) // Michael
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_TRIATHLON, FALSE)
			ENDIF
		// Force Trevor's trathon outfit for Fanatic2
		ELIF ARE_STRINGS_EQUAL(sScriptName, "Fanatic2")
			IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_TRIATHLON) // Trevor 
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_TRIATHLON, FALSE)
			ENDIF
		// Force Franklin's triathalon outfit for Fanatic3
		ELIF ARE_STRINGS_EQUAL(sScriptName, "Fanatic3")
			IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_TRIATHLON) // Franklin
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_TRIATHLON, FALSE)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Attempts to Warp the player to a specified RC mission and set it to trigger
/// PARAMS:
///    iMission - which RC mission we want to warp to
/// RETURNS:
///    True if we can trigger this RC mission, false otherwise
FUNC BOOL PerformOffskipWarpToRCMission(INT iMission)
	iMission=iMission
	//Deactivate RC missions for certain DLC
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC

		// Check that the RC mission is one we haven't completed and is currently unlocked
		IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[iMission].rcFlags, ENUM_TO_INT(RC_FLAG_ACTIVATED))
		AND IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[iMission].rcFlags, ENUM_TO_INT(RC_FLAG_ACTIVATED_IN_FLOW))
		AND NOT IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[iMission].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
		
			g_structRCMissionsStatic sRCMissionDetails
		
			Retrieve_Random_Character_Static_Mission_Details(INT_TO_ENUM(g_eRC_MissionIDs, iMission), sRCMissionDetails)
			IF IS_BIT_SET(sRCMissionDetails.rcPlayableChars, ENUM_TO_INT(GET_CURRENT_PLAYER_PED_ENUM()))
			
				// Clear any time requirements
				g_savedGlobals.sRandomChars.savedRC[iMission].rcTimeReqSet = TRUE
				g_savedGlobals.sRandomChars.savedRC[iMission].rcTimeReq = GET_CURRENT_TIMEOFDAY()
				
				// Clear any flow flags
				IF sRCMissionDetails.rcFlowFlagReq <> FLOWFLAG_NONE
					Set_Mission_Flow_Flag_State(sRCMissionDetails.rcFlowFlagReq, TRUE)
				ENDIF
				
				//Tell the RC controller to auto trigger any scene.
				g_bSceneAutoTrigger = TRUE
				
				// Mark as ready to play
				SET_BIT(g_savedGlobals.sRandomChars.savedRC[iMission].rcFlags, ENUM_TO_INT(RC_FLAG_READY_TO_PLAY))
				
				// Set time of day if required. Note, debug menu will override this
				IF sRCMissionDetails.rcStartTime != 0000 OR sRCMissionDetails.rcEndTime != 2359
					INT iHour = (sRCMissionDetails.rcStartTime/100)
					INT iMinute = (sRCMissionDetails.rcStartTime%100) 
					SET_CLOCK_TIME(iHour, iMinute, 0)
				ENDIF
				
				// If an RCM mission required a specific outfit, force it on the player...
				FORCE_CORRECT_RCM_OUTFIT(sRCMissionDetails.rcScriptName)		
				
				// Perform warp 
				DO_PLAYER_MAP_WARP_WITH_LOAD(<<sRCMissionDetails.rcCoords.X+0.4, sRCMissionDetails.rcCoords.Y, sRCMissionDetails.rcCoords.Z>>)

				// Re-activate any world points
				g_bResetRandomCharDebugRange = TRUE
				REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
				
				//When mission is running turn off the auto trigger flag.
				WHILE NOT g_RandomChars[iMission].rcIsRunning
					WAIT(0)
				ENDWHILE
				g_bSceneAutoTrigger = FALSE
				Print_OffSkip_Info_Text("OFFSKIP:Performed warp to random character mission.")
				RETURN TRUE
			ENDIF
		ENDIF
	#endif
	#endif
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Warps the player to a Random RC mission
PROC Perform_OffSkip_Warp_To_Any_RC_Mission()
	Print_OffSkip_Info_Text("OFFSKIP: Performing warp to random RC mission.")

	INT tempLoop
	REPEAT MAX_RC_MISSIONS tempLoop
		IF PerformOffskipWarpToRCMission(tempLoop)
			EXIT // we've found a valid RC mission, exit
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Waits for the mission to trigger. If it takes too long and mission has a lead in, it gets forced to launch.
///    (Only used during autoPT)
/// RETURNS:
///    TRUE if mission launched in time (or was forced to trigger). FALSE otherwise
FUNC BOOL HANDLE_LEAD_IN(SP_MISSIONS eMission)

	INT iMaxTimer = GET_GAME_TIMER() + 1000
	
	WHILE MISSION_FLOW_GET_RUNNING_MISSION() != eMission
		WAIT(0)
		CPRINTLN(DEBUG_AUTOPLAY, "Waiting for mission to launch.")
		IF GET_GAME_TIMER() > iMaxTimer
			CPRINTLN(DEBUG_AUTOPLAY, "Waited too long.")
			IF IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_HAS_LEADIN)
			
				IF IS_CUTSCENE_PLAYING()
					STOP_CUTSCENE()
					CPRINTLN(DEBUG_AUTOPLAY, "Stopping cut-scene for lead in..")
				ENDIF
				CPRINTLN(DEBUG_AUTOPLAY, "Force mission with lead in to trigger..")
	      		MISSION_FLOW_FORCE_TRIGGER_MISSION(eMission)
				RETURN TRUE
	   		ENDIF
			RETURN FALSE //failed to launch
		ENDIF
	ENDWHILE
	
	RETURN TRUE
ENDFUNC


PROC Perform_OffSkip_Menu_Mission_Selection(INT iRowIndex)
	IF iRowIndex > m_iMenuColumItemCount[OFFSKIP_COLUMN_MISSIONS]
		SCRIPT_ASSERT("Perform_OffSkip_Menu_Mission_Selection: Tried to perform an off-skip for an invalid menu item.")
		EXIT
	ENDIF
	
	IF ARE_VECTORS_EQUAL(g_sMissionDebugData[m_sMenuMissions[iRowIndex].index].warpPosition, WARP_POS_MAIN_BLIP)
	
		STATIC_BLIP_NAME_ENUM theBlip = g_sMissionStaticData[m_sMenuMissions[iRowIndex].index].blip
		
		g_bSceneAutoTrigger = TRUE

		IF theBlip != STATIC_BLIP_NAME_DUMMY_FINAL
			IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(theBlip)
				IF IS_BIT_SET(g_GameBlips[theBlip].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)//g_GameBlips[theBlip].bMultiCoordAndSprite
					DO_PLAYER_MAP_WARP_WITH_LOAD(g_GameBlips[theBlip].vCoords[GET_CURRENT_PLAYER_PED_INT()])
				ELSE
					DO_PLAYER_MAP_WARP_WITH_LOAD(g_GameBlips[theBlip].vCoords[0])
				ENDIF
				Print_OffSkip_Info_Text("Performed warp to mission.")
				
				// autoPT skips over lead ins
				IF g_bFlowAutoplayInProgress = TRUE
					HANDLE_LEAD_IN(INT_TO_ENUM(SP_MISSIONS, m_sMenuMissions[iRowIndex].index))
				ENDIF
				EXIT
			ENDIF
		ENDIF
	
	ELSE
		DO_PLAYER_MAP_WARP_WITH_LOAD(g_sMissionDebugData[m_sMenuMissions[iRowIndex].index].warpPosition)
		
		// autoPT skips over lead ins
		IF g_bFlowAutoplayInProgress = TRUE
			HANDLE_LEAD_IN(INT_TO_ENUM(SP_MISSIONS, m_sMenuMissions[iRowIndex].index))
		ENDIF
		Print_OffSkip_Info_Text("Performed warp to mission.")
		EXIT
	ENDIF
		
	Print_OffSkip_Info_Text("Skip no longer available.")
ENDPROC

/// PURPOSE:
///    Warps the player to the RC mission he has selected from the menu
/// PARAMS:
///    iRowIndex - the row the player selected from the menu
PROC Perform_OffSkip_Menu_RC_Mission_Selection(INT iRowIndex)
	IF iRowIndex > m_iMenuColumItemCount[OFFSKIP_COLUMN_RC_MISSIONS]
		SCRIPT_ASSERT("Perform_OffSkip_Menu_RC_Mission_Selection: Tried to perform an off-skip for an invalid menu item.")
		EXIT
	ENDIF
	
	INT iRCIndex = m_sMenuRCs[iRowIndex].index

	IF NOT PerformOffskipWarpToRCMission(iRCIndex)
		Print_OffSkip_Info_Text("OFFSKIP: RC mission skip no longer available.") // skip not valid
	ENDIF
ENDPROC


PROC Perform_OffSkip_Menu_Strand_Delay_Selection(INT iRowIndex)
	IF iRowIndex > m_iMenuColumItemCount[OFFSKIP_COLUMN_STRAND_DELAYS]
		SCRIPT_ASSERT("Perform_OffSkip_Menu_Strand_Delay_Selection: Tried to perform a off-skip for an invalid menu item.")
		EXIT
	ENDIF
	
	INT iStrandIndex = m_sMenuStrandDelays[iRowIndex].strandIndex
	INT iCommandIndex = m_sMenuStrandDelays[iRowIndex].commandPos
#IF USE_CLF_DLC
	IF g_savedGlobalsClifford.sFlow.strandSavedVars[iStrandIndex].thisCommandPos = iCommandIndex
#ENDIF
#IF USE_NRM_DLC
	IF g_savedGlobalsnorman.sFlow.strandSavedVars[iStrandIndex].thisCommandPos = iCommandIndex
#ENDIF
#IF NOT USE_CLF_DLC
#if not USE_NRM_DLC
	IF g_savedGlobals.sFlow.strandSavedVars[iStrandIndex].thisCommandPos = iCommandIndex
#ENDIF
#endif
		FLOW_COMMANDS 	sCurrentCommand
		TIMEOFDAY		sTimeOfDay
		
		//Check the current command this strand is on.
		sCurrentCommand = g_flowUnsaved.flowCommands[iCommandIndex]
		
		//Is it a wait command?
		SWITCH(sCurrentCommand.command)
			CASE FLOW_WAIT_IN_REAL_TIME
				//Override the strand timer.
				g_flowUnsaved.iStrandTimer[iStrandIndex] = GET_GAME_TIMER()
				g_flowUnsaved.bStrandTimerSet[iStrandIndex] = TRUE
			BREAK
			
			CASE FLOW_WAIT_IN_GAME_TIME
				// Get current time
				sTimeOfDay = GET_CURRENT_TIMEOFDAY()
				//Add delay time.
				ADD_TIME_TO_TIMEOFDAY(sTimeOfDay,
					g_flowUnsaved.coreVars[sCurrentCommand.index].iValue3,
					g_flowUnsaved.coreVars[sCurrentCommand.index].iValue2,
					g_flowUnsaved.coreVars[sCurrentCommand.index].iValue1, 0, 0, 0)
				//Jump in-game time of day to the time of day saved for this strand.
				SET_CLOCK_TIME(	GET_TIMEOFDAY_HOUR(sTimeOfDay),
								GET_TIMEOFDAY_MINUTE(sTimeOfDay),
								GET_TIMEOFDAY_SECOND(sTimeOfDay))
				SET_CLOCK_DATE(	GET_TIMEOFDAY_DAY(sTimeOfDay),
								GET_TIMEOFDAY_MONTH(sTimeOfDay),
								GET_TIMEOFDAY_YEAR(sTimeOfDay))
			BREAK
			
			CASE FLOW_WAIT_UNTIL_GAME_HOUR
				//Store the current time of day into a struct.
				sTimeOfDay = GET_CURRENT_TIMEOFDAY()
				
				//If the current hour is after the required hour then we need to wait until the next day.
				IF GET_TIMEOFDAY_HOUR(sTimeOfDay) > g_flowUnsaved.miscellaneousVars[sCurrentCommand.index].anInt
					ADD_TIME_TO_TIMEOFDAY(sTimeOfDay, 0, 0, 0, 1, 0, 0)
				ENDIF
				//If the current time isn't already in the correct hour set the hour to wait till.
				IF GET_TIMEOFDAY_HOUR(sTimeOfDay) <> g_flowUnsaved.miscellaneousVars[sCurrentCommand.index].anInt
					SET_TIMEOFDAY_SECOND(sTimeOfDay, 0)
					SET_TIMEOFDAY_MINUTE(sTimeOfDay, 0)
					SET_TIMEOFDAY_HOUR(sTimeOfDay, g_flowUnsaved.miscellaneousVars[sCurrentCommand.index].anInt)
				ENDIF
				
				//Set the gameclock to the values in the time of day struct.
				SET_CLOCK_TIME(GET_TIMEOFDAY_HOUR(sTimeOfDay), GET_TIMEOFDAY_MINUTE(sTimeOfDay), GET_TIMEOFDAY_SECOND(sTimeOfDay))
				SET_CLOCK_DATE(GET_TIMEOFDAY_DAY(sTimeOfDay), INT_TO_ENUM(MONTH_OF_YEAR,GET_TIMEOFDAY_MONTH(sTimeOfDay)), GET_TIMEOFDAY_YEAR(sTimeOfDay))
			BREAK
			
			CASE FLOW_WAIT_UNTIL_GAME_TIME_AND_DATE
				//Jump in-game time of day to the time of day saved for this strand.
				sTimeOfDay = g_flowUnsaved.miscellaneousVars[sCurrentCommand.index].aTimeOfDay
				SET_CLOCK_TIME(	GET_TIMEOFDAY_HOUR(sTimeOfDay),
								GET_TIMEOFDAY_MINUTE(sTimeOfDay),
								GET_TIMEOFDAY_SECOND(sTimeOfDay))
				SET_CLOCK_DATE(	GET_TIMEOFDAY_DAY(sTimeOfDay),
								INT_TO_ENUM(MONTH_OF_YEAR, GET_TIMEOFDAY_MONTH(sTimeOfDay)),
								GET_TIMEOFDAY_YEAR(sTimeOfDay))
			BREAK
		ENDSWITCH
	
		Print_OffSkip_Info_Text("Performed strand delay skip.")
		EXIT
	ENDIF
	
	Print_OffSkip_Info_Text("Skip no longer available.")
ENDPROC

PROC Perform_OffSkip_Menu_Comms_Delay_Selection_CLF(INT iRowIndex)
	IF iRowIndex > m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]
		SCRIPT_ASSERT("Perform_OffSkip_Menu_Comms_Delay_Selection: Tried to perform an off-skip for an invalid menu item.")
		EXIT
	ENDIF
	
	INT iQueueIndex = m_sMenuCommDelays[iRowIndex].queueIndex
	
	IF g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[iQueueIndex].sCommData.eID = m_sMenuCommDelays[iRowIndex].commID
		INT iGameTime = GET_GAME_TIMER()
		//Set global communication wait timer to game time.
		g_iGlobalWaitTime = iGameTime
		//Set character wait timer to game time.
		g_iCharWaitTime[g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[iQueueIndex].sCommData.eNPCCharacter] = iGameTime
		//Set call timer to 0.
		g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[iQueueIndex].sCommData.iQueueTime = iGameTime
		//Override any restricted area associated with this call.
		g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[iQueueIndex].sCommData.eRestrictedAreaID = VID_BLANK
		
		Print_OffSkip_Info_Text("Performing communication delay skip for call.")
		EXIT
	ELIF g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[iQueueIndex].sCommData.eID = m_sMenuCommDelays[iRowIndex].commID
		INT iGameTime = GET_GAME_TIMER()
		//Set global communication wait timer to 0.
		g_iGlobalWaitTime = iGameTime
		//Set character wait timer to 0.
		g_iCharWaitTime[g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[iQueueIndex].sCommData.eNPCCharacter] = iGameTime
		//Set text message timer to 0.
		g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[iQueueIndex].sCommData.iQueueTime = iGameTime
		//Override any restricted area associated with this text.
		g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[iQueueIndex].sCommData.eRestrictedAreaID = VID_BLANK
	
		Print_OffSkip_Info_Text("Performed communication delay skip for text.")
		EXIT
	ELIF g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[iQueueIndex].sCommData.eID = m_sMenuCommDelays[iRowIndex].commID
		INT iGameTime = GET_GAME_TIMER()
		//Set global communication wait timer to 0.
		g_iGlobalWaitTime = iGameTime
		//Set character wait timer to 0.
		g_iCharWaitTime[g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[iQueueIndex].sCommData.eNPCCharacter] = iGameTime
		//Set email timer to 0.
		g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[iQueueIndex].sCommData.iQueueTime = iGameTime
		//Override any restricted area associated with this email.
		g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[iQueueIndex].sCommData.eRestrictedAreaID = VID_BLANK
	
		Print_OffSkip_Info_Text("Performed communication delay skip for email.")
		EXIT
	ENDIF
	
	Print_OffSkip_Info_Text("Communication skip no longer available.")
ENDPROC

PROC Perform_OffSkip_Menu_Comms_Delay_Selection_NRM(INT iRowIndex)
	IF iRowIndex > m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]
		SCRIPT_ASSERT("Perform_OffSkip_Menu_Comms_Delay_Selection: Tried to perform an off-skip for an invalid menu item.")
		EXIT
	ENDIF
	
	INT iQueueIndex = m_sMenuCommDelays[iRowIndex].queueIndex
	
	IF g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[iQueueIndex].sCommData.eID = m_sMenuCommDelays[iRowIndex].commID
		INT iGameTime = GET_GAME_TIMER()
		//Set global communication wait timer to game time.
		g_iGlobalWaitTime = iGameTime
		//Set character wait timer to game time.
		g_iCharWaitTime[g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[iQueueIndex].sCommData.eNPCCharacter] = iGameTime
		//Set call timer to 0.
		g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[iQueueIndex].sCommData.iQueueTime = iGameTime
		//Override any restricted area associated with this call.
		g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[iQueueIndex].sCommData.eRestrictedAreaID = VID_BLANK
		
		Print_OffSkip_Info_Text("Performing communication delay skip for call.")
		EXIT
	ELIF g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[iQueueIndex].sCommData.eID = m_sMenuCommDelays[iRowIndex].commID
		INT iGameTime = GET_GAME_TIMER()
		//Set global communication wait timer to 0.
		g_iGlobalWaitTime = iGameTime
		//Set character wait timer to 0.
		g_iCharWaitTime[g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[iQueueIndex].sCommData.eNPCCharacter] = iGameTime
		//Set text message timer to 0.
		g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[iQueueIndex].sCommData.iQueueTime = iGameTime
		//Override any restricted area associated with this text.
		g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[iQueueIndex].sCommData.eRestrictedAreaID = VID_BLANK
	
		Print_OffSkip_Info_Text("Performed communication delay skip for text.")
		EXIT
	ELIF g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[iQueueIndex].sCommData.eID = m_sMenuCommDelays[iRowIndex].commID
		INT iGameTime = GET_GAME_TIMER()
		//Set global communication wait timer to 0.
		g_iGlobalWaitTime = iGameTime
		//Set character wait timer to 0.
		g_iCharWaitTime[g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[iQueueIndex].sCommData.eNPCCharacter] = iGameTime
		//Set email timer to 0.
		g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[iQueueIndex].sCommData.iQueueTime = iGameTime
		//Override any restricted area associated with this email.
		g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[iQueueIndex].sCommData.eRestrictedAreaID = VID_BLANK
	
		Print_OffSkip_Info_Text("Performed communication delay skip for email.")
		EXIT
	ENDIF
	
	Print_OffSkip_Info_Text("Communication skip no longer available.")
ENDPROC

PROC Perform_OffSkip_Menu_Comms_Delay_Selection(INT iRowIndex)
	#IF USE_CLF_DLC
		Perform_OffSkip_Menu_Comms_Delay_Selection_CLF(iRowIndex)
		EXIT
	#ENDIF
	#IF USE_NRM_DLC
		Perform_OffSkip_Menu_Comms_Delay_Selection_NRM(iRowIndex)
		EXIT
	#ENDIF
	
	IF iRowIndex > m_iMenuColumItemCount[OFFSKIP_COLUMN_COMMS_DELAYS]
		SCRIPT_ASSERT("Perform_OffSkip_Menu_Comms_Delay_Selection: Tried to perform an off-skip for an invalid menu item.")
		EXIT
	ENDIF
	
	INT iQueueIndex = m_sMenuCommDelays[iRowIndex].queueIndex
	
	IF g_savedGlobals.sCommsControlData.sQueuedCalls[iQueueIndex].sCommData.eID = m_sMenuCommDelays[iRowIndex].commID
		INT iGameTime = GET_GAME_TIMER()
		//Set global communication wait timer to game time.
		g_iGlobalWaitTime = iGameTime
		//Set character wait timer to game time.
		g_iCharWaitTime[g_savedGlobals.sCommsControlData.sQueuedCalls[iQueueIndex].sCommData.eNPCCharacter] = iGameTime
		//Set call timer to 0.
		g_savedGlobals.sCommsControlData.sQueuedCalls[iQueueIndex].sCommData.iQueueTime = iGameTime
		//Override any restricted area associated with this call.
		g_savedGlobals.sCommsControlData.sQueuedCalls[iQueueIndex].sCommData.eRestrictedAreaID = VID_BLANK
		
		Print_OffSkip_Info_Text("Performing communication delay skip for call.")
		EXIT
	ELIF g_savedGlobals.sCommsControlData.sQueuedTexts[iQueueIndex].sCommData.eID = m_sMenuCommDelays[iRowIndex].commID
		INT iGameTime = GET_GAME_TIMER()
		//Set global communication wait timer to 0.
		g_iGlobalWaitTime = iGameTime
		//Set character wait timer to 0.
		g_iCharWaitTime[g_savedGlobals.sCommsControlData.sQueuedTexts[iQueueIndex].sCommData.eNPCCharacter] = iGameTime
		//Set text message timer to 0.
		g_savedGlobals.sCommsControlData.sQueuedTexts[iQueueIndex].sCommData.iQueueTime = iGameTime
		//Override any restricted area associated with this text.
		g_savedGlobals.sCommsControlData.sQueuedTexts[iQueueIndex].sCommData.eRestrictedAreaID = VID_BLANK
	
		Print_OffSkip_Info_Text("Performed communication delay skip for text.")
		EXIT
	ELIF g_savedGlobals.sCommsControlData.sQueuedEmails[iQueueIndex].sCommData.eID = m_sMenuCommDelays[iRowIndex].commID
		INT iGameTime = GET_GAME_TIMER()
		//Set global communication wait timer to 0.
		g_iGlobalWaitTime = iGameTime
		//Set character wait timer to 0.
		g_iCharWaitTime[g_savedGlobals.sCommsControlData.sQueuedEmails[iQueueIndex].sCommData.eNPCCharacter] = iGameTime
		//Set email timer to 0.
		g_savedGlobals.sCommsControlData.sQueuedEmails[iQueueIndex].sCommData.iQueueTime = iGameTime
		//Override any restricted area associated with this email.
		g_savedGlobals.sCommsControlData.sQueuedEmails[iQueueIndex].sCommData.eRestrictedAreaID = VID_BLANK
	
		Print_OffSkip_Info_Text("Performed communication delay skip for email.")
		EXIT
	ENDIF
	
	Print_OffSkip_Info_Text("Communication skip no longer available.")
ENDPROC


PROC Perform_OffSkip_Mission_Auto_Complete(INT iRowIndex)
	SP_MISSIONS eMission = INT_TO_ENUM(SP_MISSIONS,m_sMenuMissions[iRowIndex].index)
	
	CLEAR_ALL_FRIEND_BLOCKS_FOR_MISSION(eMission)
	
	SET_MISSION_COMPLETE_STATE(eMission, TRUE)
	
	WAIT(2000)
	
	IF g_eMissionSceneToCleanup != SP_MISSION_NONE
		MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(g_eMissionSceneToCleanup)
	ENDIF
	
	TEXT_LABEL_63 txtInfo = "Passed mission "
	TEXT_LABEL txtMissionName = GET_SP_MISSION_NAME_LABEL(eMission)
	txtInfo += GET_STRING_FROM_TEXT_FILE(txtMissionName)
	txtInfo += "."
	Print_OffSkip_Info_Text(txtInfo)
ENDPROC

#if not USE_CLF_DLC
#if not USE_NRM_DLC
PROC Perform_OffSkip_RC_Mission_Auto_Complete(INT iRowIndex)
	g_eRC_MissionIDs eRCMission = INT_TO_ENUM(g_eRC_MissionIDs, m_sMenuRCs[iRowIndex].index)

	Activate_RC_Mission(eRCMission, TRUE)
	
	WAIT(2000)
	
	IF g_eMissionSceneToCleanup != SP_MISSION_NONE
		MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(g_eMissionSceneToCleanup)
	ENDIF

	TEXT_LABEL_63 txtInfo = "Passed RC mission "
	TEXT_LABEL txtMissionName = GET_RC_MISSION_NAME_LABEL(eRCMission)
	txtInfo += GET_STRING_FROM_TEXT_FILE(txtMissionName)
	txtInfo += "."
	Print_OffSkip_Info_Text(txtInfo)
ENDPROC
#endif
#endif

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ Off-Skip System Control ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛
#if USE_CLF_DLC
PROC Update_OffSkip_Available_Functions_CLF()
	INT tempLoop
	
	//Initially flag all functions as unavailable.
	REPEAT OFFSKIP_MAX_FUNCTIONS tempLoop
		bOffSkipFunctionAvailable[tempLoop] = FALSE
	ENDREPEAT
	
	// Store the player's current character.
	enumCharacterList ePlayerChar
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		ePlayerChar = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
	ENDIF
	
	//Is a fail screen active?
	IF IsReplayWaitingForReplayScreen()
		bOffSkipFunctionAvailable[OFFSKIP_SKIP_FAIL_SCREEN] = TRUE
	ENDIF
	
	//Is a mission warp available?
#IF USE_CLF_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU tempLoop
		IF NOT (g_availableMissionsTU[tempLoop].index = ILLEGAL_ARRAY_POSITION)
			INT iMissionDataIndex = g_flowUnsaved.coreVars[g_availableMissionsTU[tempLoop].index].iValue1
#ENDIF
#IF USE_NRM_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU tempLoop
		IF NOT (g_availableMissionsTU[tempLoop].index = ILLEGAL_ARRAY_POSITION)
			INT iMissionDataIndex = g_flowUnsaved.coreVars[g_availableMissionsTU[tempLoop].index].iValue1
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	REPEAT MAX_MISSIONS_AVAILABLE tempLoop
		IF NOT (g_availableMissions[tempLoop].index = ILLEGAL_ARRAY_POSITION)
			INT iMissionDataIndex = g_flowUnsaved.coreVars[g_availableMissions[tempLoop].index].iValue1
#ENDIF
#ENDIF
			INT iTriggerCharBitset = g_sMissionStaticData[iMissionDataIndex].triggerCharBitset
			IF Is_Mission_Triggerable_By_Character(iTriggerCharBitset, ePlayerChar)
				IF g_sMissionStaticData[iMissionDataIndex].blip <> STATIC_BLIP_NAME_DUMMY_FINAL
					bOffSkipFunctionAvailable[OFFSKIP_WARP_TO_MISSION] = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Are any strands sitting on skippable delays?
	REPEAT MAX_STRANDS_CLF tempLoop
		FLOW_COMMAND_IDS eCurrentCommand = g_flowUnsaved.flowCommands[g_savedGlobalsClifford.sFlow.strandSavedVars[tempLoop].thisCommandPos].command
		SWITCH(eCurrentCommand)
			CASE FLOW_WAIT_IN_REAL_TIME
			CASE FLOW_WAIT_IN_GAME_TIME
			CASE FLOW_WAIT_UNTIL_GAME_HOUR
			CASE FLOW_WAIT_UNTIL_GAME_TIME_AND_DATE
				bOffSkipFunctionAvailable[OFFSKIP_SKIP_STRAND_DELAY] = TRUE
			BREAK
		ENDSWITCH
	ENDREPEAT
	
	//Are any communications queued with skippable delays?
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls tempLoop
		IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[tempLoop].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[tempLoop].sCommData.ePriority = g_savedGlobalsClifford.sCommsControlData.eCharacterPriorityLevel[ePlayerChar]
				bOffSkipFunctionAvailable[OFFSKIP_SKIP_COMMS_DELAY] = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts tempLoop
		IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[tempLoop].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[tempLoop].sCommData.ePriority = g_savedGlobalsClifford.sCommsControlData.eCharacterPriorityLevel[GET_CURRENT_PLAYER_PED_ENUM()]
				bOffSkipFunctionAvailable[OFFSKIP_SKIP_COMMS_DELAY] = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
		
	//Are there any players available to skip to.
	REPEAT 3 tempLoop
		IF GET_CURRENT_PLAYER_PED_INT() != tempLoop
			IF IS_PLAYER_PED_AVAILABLE(INT_TO_ENUM(enumCharacterList, tempLoop))
				bOffSkipFunctionAvailable[OFFSKIP_SWITCH_CHARACTER] = TRUE  
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
#endif
#if USE_NRM_DLC
PROC Update_OffSkip_Available_Functions_NRM()
	INT tempLoop
	
	//Initially flag all functions as unavailable.
	REPEAT OFFSKIP_MAX_FUNCTIONS tempLoop
		bOffSkipFunctionAvailable[tempLoop] = FALSE
	ENDREPEAT
	
	// Store the player's current character.
	enumCharacterList ePlayerChar
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		ePlayerChar = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
	ENDIF
	
	//Is a fail screen active?
	IF IsReplayWaitingForReplayScreen()
		bOffSkipFunctionAvailable[OFFSKIP_SKIP_FAIL_SCREEN] = TRUE
	ENDIF
	
	//Is a mission warp available?
#IF USE_CLF_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU tempLoop
		IF NOT (g_availableMissionsTU[tempLoop].index = ILLEGAL_ARRAY_POSITION)
			INT iMissionDataIndex = g_flowUnsaved.coreVars[g_availableMissionsTU[tempLoop].index].iValue1
#ENDIF
#IF USE_NRM_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU tempLoop
		IF NOT (g_availableMissionsTU[tempLoop].index = ILLEGAL_ARRAY_POSITION)
			INT iMissionDataIndex = g_flowUnsaved.coreVars[g_availableMissionsTU[tempLoop].index].iValue1
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	REPEAT MAX_MISSIONS_AVAILABLE tempLoop
		IF NOT (g_availableMissions[tempLoop].index = ILLEGAL_ARRAY_POSITION)
			INT iMissionDataIndex = g_flowUnsaved.coreVars[g_availableMissions[tempLoop].index].iValue1
#ENDIF
#ENDIF
			INT iTriggerCharBitset = g_sMissionStaticData[iMissionDataIndex].triggerCharBitset
			IF Is_Mission_Triggerable_By_Character(iTriggerCharBitset, ePlayerChar)
				IF g_sMissionStaticData[iMissionDataIndex].blip <> STATIC_BLIP_NAME_DUMMY_FINAL
					bOffSkipFunctionAvailable[OFFSKIP_WARP_TO_MISSION] = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Are any strands sitting on skippable delays?
	REPEAT MAX_STRANDS_NRM tempLoop
		FLOW_COMMAND_IDS eCurrentCommand = g_flowUnsaved.flowCommands[g_savedGlobalsnorman.sFlow.strandSavedVars[tempLoop].thisCommandPos].command
		SWITCH(eCurrentCommand)
			CASE FLOW_WAIT_IN_REAL_TIME
			CASE FLOW_WAIT_IN_GAME_TIME
			CASE FLOW_WAIT_UNTIL_GAME_HOUR
			CASE FLOW_WAIT_UNTIL_GAME_TIME_AND_DATE
				bOffSkipFunctionAvailable[OFFSKIP_SKIP_STRAND_DELAY] = TRUE
			BREAK
		ENDSWITCH
	ENDREPEAT
	
	//Are any communications queued with skippable delays?
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls tempLoop
		IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[tempLoop].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[tempLoop].sCommData.ePriority = g_savedGlobalsnorman.sCommsControlData.eCharacterPriorityLevel[ePlayerChar]
				bOffSkipFunctionAvailable[OFFSKIP_SKIP_COMMS_DELAY] = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts tempLoop
		IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[tempLoop].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[tempLoop].sCommData.ePriority = g_savedGlobalsnorman.sCommsControlData.eCharacterPriorityLevel[GET_CURRENT_PLAYER_PED_ENUM()]
				bOffSkipFunctionAvailable[OFFSKIP_SKIP_COMMS_DELAY] = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
		
	//Are there any players available to skip to.
	REPEAT 3 tempLoop
		IF GET_CURRENT_PLAYER_PED_INT() != tempLoop
			IF IS_PLAYER_PED_AVAILABLE(INT_TO_ENUM(enumCharacterList, tempLoop))
				bOffSkipFunctionAvailable[OFFSKIP_SWITCH_CHARACTER] = TRUE  
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
#endif
PROC Update_OffSkip_Available_Functions()
	#IF USE_CLF_DLC
		Update_OffSkip_Available_Functions_CLF()
		EXIT
	#ENDIF
	#IF USE_NRM_DLC
		Update_OffSkip_Available_Functions_NRM()
		EXIT
	#ENDIF
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	INT tempLoop
	
	//Initially flag all functions as unavailable.
	REPEAT OFFSKIP_MAX_FUNCTIONS tempLoop
		bOffSkipFunctionAvailable[tempLoop] = FALSE
	ENDREPEAT
	
	// Store the player's current character.
	enumCharacterList ePlayerChar
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		ePlayerChar = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
	ENDIF
	
	//Is a fail screen active?
	IF IsReplayWaitingForReplayScreen()
		bOffSkipFunctionAvailable[OFFSKIP_SKIP_FAIL_SCREEN] = TRUE
	ENDIF
	
	//Is a mission warp available?
#IF USE_CLF_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU tempLoop
		IF NOT (g_availableMissionsTU[tempLoop].index = ILLEGAL_ARRAY_POSITION)
			INT iMissionDataIndex = g_flowUnsaved.coreVars[g_availableMissionsTU[tempLoop].index].iValue1
#ENDIF
#IF USE_NRM_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU tempLoop
		IF NOT (g_availableMissionsTU[tempLoop].index = ILLEGAL_ARRAY_POSITION)
			INT iMissionDataIndex = g_flowUnsaved.coreVars[g_availableMissionsTU[tempLoop].index].iValue1
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	REPEAT MAX_MISSIONS_AVAILABLE tempLoop
		IF NOT (g_availableMissions[tempLoop].index = ILLEGAL_ARRAY_POSITION)
			INT iMissionDataIndex = g_flowUnsaved.coreVars[g_availableMissions[tempLoop].index].iValue1
#ENDIF
#ENDIF
			INT iTriggerCharBitset = g_sMissionStaticData[iMissionDataIndex].triggerCharBitset
			IF Is_Mission_Triggerable_By_Character(iTriggerCharBitset, ePlayerChar)
				IF g_sMissionStaticData[iMissionDataIndex].blip <> STATIC_BLIP_NAME_DUMMY_FINAL
					bOffSkipFunctionAvailable[OFFSKIP_WARP_TO_MISSION] = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Are any strands sitting on skippable delays?
	REPEAT MAX_STRANDS tempLoop
		FLOW_COMMAND_IDS eCurrentCommand = g_flowUnsaved.flowCommands[g_savedGlobals.sFlow.strandSavedVars[tempLoop].thisCommandPos].command
		SWITCH(eCurrentCommand)
			CASE FLOW_WAIT_IN_REAL_TIME
			CASE FLOW_WAIT_IN_GAME_TIME
			CASE FLOW_WAIT_UNTIL_GAME_HOUR
			CASE FLOW_WAIT_UNTIL_GAME_TIME_AND_DATE
				bOffSkipFunctionAvailable[OFFSKIP_SKIP_STRAND_DELAY] = TRUE
			BREAK
		ENDSWITCH
	ENDREPEAT
	
	//Are any communications queued with skippable delays?
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls tempLoop
		IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[tempLoop].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			IF g_savedGlobals.sCommsControlData.sQueuedCalls[tempLoop].sCommData.ePriority = g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[ePlayerChar]
				bOffSkipFunctionAvailable[OFFSKIP_SKIP_COMMS_DELAY] = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedTexts tempLoop
		IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedTexts[tempLoop].sCommData.iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			IF g_savedGlobals.sCommsControlData.sQueuedTexts[tempLoop].sCommData.ePriority = g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[GET_CURRENT_PLAYER_PED_ENUM()]
				bOffSkipFunctionAvailable[OFFSKIP_SKIP_COMMS_DELAY] = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Are any random character missions available?
	g_structRCMissionsStatic sRCMissionDetails
	REPEAT MAX_RC_MISSIONS tempLoop
		IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[tempLoop].rcFlags, ENUM_TO_INT(RC_FLAG_ACTIVATED))
		AND IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[tempLoop].rcFlags, ENUM_TO_INT(RC_FLAG_ACTIVATED_IN_FLOW))
		AND NOT IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[tempLoop].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
			Retrieve_Random_Character_Static_Mission_Details(INT_TO_ENUM(g_eRC_MissionIDs, tempLoop), sRCMissionDetails)
			IF IS_BIT_SET(sRCMissionDetails.rcPlayableChars, ENUM_TO_INT(GET_CURRENT_PLAYER_PED_ENUM()))
				bOffSkipFunctionAvailable[OFFSKIP_WARP_TO_RC_MISSION] = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Are there any players available to skip to.
	REPEAT 3 tempLoop
		IF GET_CURRENT_PLAYER_PED_INT() != tempLoop
			IF IS_PLAYER_PED_AVAILABLE(INT_TO_ENUM(enumCharacterList, tempLoop))
				bOffSkipFunctionAvailable[OFFSKIP_SWITCH_CHARACTER] = TRUE  
			ENDIF
		ENDIF
	ENDREPEAT
	#endif
	#endif
ENDPROC


PROC Perform_OffSkip_Menu_Selection()
	SWITCH INT_TO_ENUM(OffSkipMenuColumns, m_iCurrentColumnSelected)
		CASE OFFSKIP_COLUMN_MISSIONS
			Perform_OffSkip_Menu_Mission_Selection(m_iCurrentRowSelected)
		BREAK
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
		CASE OFFSKIP_COLUMN_RC_MISSIONS
			Perform_OffSkip_Menu_RC_Mission_Selection(m_iCurrentRowSelected)
		BREAK
		#endif
		#endif
		CASE OFFSKIP_COLUMN_STRAND_DELAYS
			Perform_OffSkip_Menu_Strand_Delay_Selection(m_iCurrentRowSelected)
		BREAK
		CASE OFFSKIP_COLUMN_COMMS_DELAYS
			Perform_OffSkip_Menu_Comms_Delay_Selection(m_iCurrentRowSelected)
		BREAK
	ENDSWITCH
ENDPROC


PROC Perform_OffSkip_Menu_Auto_Complete()
	SWITCH INT_TO_ENUM(OffSkipMenuColumns, m_iCurrentColumnSelected)
		CASE OFFSKIP_COLUMN_MISSIONS
			Perform_OffSkip_Mission_Auto_Complete(m_iCurrentRowSelected)
		BREAK
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
		CASE OFFSKIP_COLUMN_RC_MISSIONS
			Perform_OffSkip_RC_Mission_Auto_Complete(m_iCurrentRowSelected)
		BREAK
		#endif
		#endif
		DEFAULT
			SCRIPT_ASSERT("Perform_OffSkip_Menu_Auto_Complete: Triggered an auto complete event for an invalid menu column. Tell BenR.")
		BREAK
	ENDSWITCH
ENDPROC


PROC Perform_OffSkip_Switch_Character()
	INT iCurrentPlayerPed = GET_CURRENT_PLAYER_PED_INT()
	INT iNewPlayerPed = iCurrentPlayerPed
		
	WHILE TRUE
		//Step to the next playable ped index.
		iNewPlayerPed++
		IF iNewPlayerPed > 2
			iNewPlayerPed = 0
		ENDIF
		
		//Have we looped back to the current player ped and failed to find a new available ped to switch to?
		IF iNewPlayerPed = iCurrentPlayerPed
			EXIT
		ENDIF
		
		//Is this new playable ped index available to be swapped to?
		IF IS_PLAYER_PED_AVAILABLE(INT_TO_ENUM(enumCharacterList, iNewPlayerPed))
			Print_OffSkip_Info_Text("Performing swapping of player character.")
			WHILE NOT SET_CURRENT_SELECTOR_PED(GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(INT_TO_ENUM(enumCharacterList, iNewPlayerPed)))
				WAIT(0)
			ENDWHILE
			EXIT
		ENDIF
	ENDWHILE
ENDPROC



PROC Perform_Prioritised_OffSkip()
	PRINTSTRING("<OFFSKIP> J pressed. Running prioritised off-skip.\n")

	// Ignore if not in gameflow mode.
	#IF USE_CLF_DLC
		IF NOT g_savedGlobalsClifford.sFlow.isGameflowActive
			EXIT
		ENDIF
	#ENDIF
	#IF USE_NRM_DLC
		IF NOT g_savedGlobalsnorman.sFlow.isGameflowActive
			EXIT
		ENDIF
	#ENDIF
	#IF NOT USE_CLF_DLC
	#if not USE_NRM_DLC
	IF NOT g_savedGlobals.sFlow.isGameflowActive
		EXIT
	ENDIF
	#ENDIF
	#endif
	// Ignore if already on mission
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY)
		Print_OffSkip_Info_Text("Can't skip. On mission.")
		EXIT
	ENDIF
	
	// Ignore if cellphone on screen
	IF (IS_PHONE_ONSCREEN())
		Print_OffSkip_Info_Text("Can't skip. Cellphone on screen.")
		EXIT
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		Print_OffSkip_Info_Text("Can't skip. Cutscene is playing.")
		EXIT
	ENDIF
	
	Update_OffSkip_Available_Functions()
	
	IF bOffSkipFunctionAvailable[OFFSKIP_SKIP_FAIL_SCREEN]
		Perform_OffSkip_Skip_Fail_Screen()

	ELIF bOffSkipFunctionAvailable[OFFSKIP_WARP_TO_MISSION]
		Perform_OffSkip_Warp_To_Any_Mission()
		
	ELIF bOffSkipFunctionAvailable[OFFSKIP_SKIP_STRAND_DELAY]
		Perform_OffSkip_Skip_Any_Strand_Delay()
		
	ELIF bOffSkipFunctionAvailable[OFFSKIP_SKIP_COMMS_DELAY]
		Perform_OffSkip_Skip_Any_Communication_Delay()
	#IF NOT USE_CLF_DLC
	#if not USE_NRM_DLC	
	ELIF bOffSkipFunctionAvailable[OFFSKIP_WARP_TO_RC_MISSION]
		Perform_OffSkip_Warp_To_Any_RC_Mission()
	#endif
	#endif
	ELIF bOffSkipFunctionAvailable[OFFSKIP_SWITCH_CHARACTER]
		Perform_OffSkip_Switch_Character()

	ELSE
		Print_OffSkip_Info_Text("Nothing found to skip.")
	ENDIF

ENDPROC


PROC Display_OffSkip_Menu()
	INT iColumnIndex, iRowIndex
	FLOAT fHalfStringWidth

	//Get screen aspect ratio.
	INT iScreenX
	INT iScreenY
	GET_SCREEN_RESOLUTION(iScreenX,iScreenY)
	FLOAT fAspectRatio = TO_FLOAT(iScreenY) / TO_FLOAT(iScreenX)
	
	//Calculate base values.
	FLOAT fWidth 		= ENUM_TO_INT(OFFSKIP_MAX_COLUMNS)*DEBUG_OFFSKIP_MENU_COLUMN_WIDTH
	FLOAT fHeight 		= DEBUG_OFFSKIP_MENU_ROW_HEIGHT * (DEBUG_OFFSKIP_MENU_ITEM_DEPTH+1)
	FLOAT fFirstColumnX	= 0.5 - (fWidth*0.5) + (DEBUG_OFFSKIP_MENU_COLUMN_WIDTH*0.5)
	FLOAT fFirstRowY	= 0.5 - (fHeight*0.5) + (DEBUG_OFFSKIP_MENU_ROW_HEIGHT*0.5)
	
	//Draw background.
	DRAW_RECT(0.5, 0.5 - (DEBUG_OFFSKIP_MENU_ROW_HEIGHT*0.5), fWidth + (0.006*fAspectRatio), fHeight + 0.012 + DEBUG_OFFSKIP_MENU_ROW_HEIGHT, 0,0,0,255)
	
	//Draw title.
	Configure_Menu_Text(0, 200, 0, 150)
	fHalfStringWidth = GET_STRING_WIDTH("JSKP_TITLE") * 0.5
	DISPLAY_TEXT(	0.5 - fHalfStringWidth, 
					fFirstRowY - DEBUG_OFFSKIP_MENU_ROW_HEIGHT - 0.015, 
					"JSKP_TITLE")
												
	//Draw columns.
	FLOAT fColumnX = fFirstColumnX
	REPEAT OFFSKIP_MAX_COLUMNS iColumnIndex
		//Draw background.
		DRAW_RECT(fColumnX, 0.5, DEBUG_OFFSKIP_MENU_COLUMN_WIDTH - (0.006*fAspectRatio), fHeight, 10,10,10,255)
		
		//Draw column title.
		Configure_Menu_Text(255, 255, 255, 150)
		TEXT_LABEL_15 tColumnName = "JSKP_COL"
		tColumnName += iColumnIndex
		fHalfStringWidth = GET_STRING_WIDTH(tColumnName) * 0.5
		DISPLAY_TEXT(	fColumnX - fHalfStringWidth, 
						fFirstRowY - 0.01, 
						tColumnName)
						
		//Draw selection highlight.
		IF iColumnIndex = m_iCurrentColumnSelected
			DRAW_RECT(	fColumnX, 
						fFirstRowY + (( m_iCurrentRowSelected+1)*DEBUG_OFFSKIP_MENU_ROW_HEIGHT), 
						DEBUG_OFFSKIP_MENU_COLUMN_WIDTH - (0.006*fAspectRatio), 
						DEBUG_OFFSKIP_MENU_ROW_HEIGHT, 
						25,25,25,200)
		ENDIF
		
		//Draw column items.
		REPEAT m_iMenuColumItemCount[iColumnIndex] iRowIndex
			SWITCH INT_TO_ENUM(OffSkipMenuColumns, iColumnIndex)
				CASE OFFSKIP_COLUMN_MISSIONS
					Configure_Menu_Text(180, 180, 180, 150)
					DISPLAY_TEXT_WITH_LITERAL_STRING(	fColumnX - (DEBUG_OFFSKIP_MENU_COLUMN_WIDTH*0.35), 
														fFirstRowY + ((iRowIndex+1)*DEBUG_OFFSKIP_MENU_ROW_HEIGHT) - 0.015, 
														"STRING",
														m_sMenuMissions[iRowIndex].name)
				BREAK
				
				CASE OFFSKIP_COLUMN_RC_MISSIONS
					Configure_Menu_Text(180, 180, 180, 150)
					DISPLAY_TEXT_WITH_LITERAL_STRING(	fColumnX - (DEBUG_OFFSKIP_MENU_COLUMN_WIDTH*0.35), 
														fFirstRowY + ((iRowIndex+1)*DEBUG_OFFSKIP_MENU_ROW_HEIGHT) - 0.015, 
														"STRING",
														m_sMenuRCs[iRowIndex].name)
				BREAK
				
				CASE OFFSKIP_COLUMN_STRAND_DELAYS
					Configure_Menu_Text(180, 180, 180, 150)
					DISPLAY_TEXT_WITH_LITERAL_STRING(	fColumnX - (DEBUG_OFFSKIP_MENU_COLUMN_WIDTH*0.35), 
														fFirstRowY + ((iRowIndex+1)*DEBUG_OFFSKIP_MENU_ROW_HEIGHT) - 0.015, 
														"STRING",
														GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS,m_sMenuStrandDelays[iRowIndex].strandIndex)))
				BREAK
				
				CASE OFFSKIP_COLUMN_COMMS_DELAYS
					Configure_Menu_Comms_Text(180, 180, 180, 150)
					DISPLAY_TEXT_WITH_LITERAL_STRING(	fColumnX - (DEBUG_OFFSKIP_MENU_COLUMN_WIDTH*0.35), 
														fFirstRowY + ((iRowIndex+1)*DEBUG_OFFSKIP_MENU_ROW_HEIGHT) - 0.015, 
														"STRING",
														m_sMenuCommDelays[iRowIndex].name)
				BREAK
			ENDSWITCH
		ENDREPEAT
		
		fColumnX += DEBUG_OFFSKIP_MENU_COLUMN_WIDTH
	ENDREPEAT
	
ENDPROC


PROC Manage_OffSkip_Menu_Input()
	IF (GET_GAME_TIMER() - m_iTimeLastMenuInput) > DEBUG_OFFSKIP_MENU_INPUT_DELAY
	
		//Get analogue stick positons.
		INT iStickLeftY
		INT iStickLeftX
		INT temp
		GET_POSITION_OF_ANALOGUE_STICKS(PAD1,iStickLeftX,iStickLeftY,temp,temp)
	
		//Check for item selection input.
		IF IS_BUTTON_PRESSED(PAD1, CROSS)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN)
			m_bOffSkipMenuSelectionMade = TRUE
			m_bOffSkipMenuAutoCompleteMade = FALSE
			m_bOffSkipMenuActive = FALSE
		ENDIF
		
		//J skippable for autoPT
		IF g_bFlowAutoplayInProgress = TRUE
		AND IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			m_bOffSkipMenuSelectionMade = TRUE
			m_bOffSkipMenuAutoCompleteMade = FALSE
			m_bOffSkipMenuActive = FALSE
		ENDIF
		
		//Check for item auto complete selection input.
		IF IS_BUTTON_PRESSED(PAD1, SQUARE)
			OffSkipMenuColumns eColumn = INT_TO_ENUM(OffSkipMenuColumns, m_iCurrentColumnSelected)
			IF eColumn = OFFSKIP_COLUMN_MISSIONS
			OR eColumn = OFFSKIP_COLUMN_RC_MISSIONS
				m_bOffSkipMenuSelectionMade = FALSE
				m_bOffSkipMenuAutoCompleteMade = TRUE
				m_bOffSkipMenuActive = FALSE
			ENDIF
		ENDIF
		
		//Check for exit input.
		IF IS_BUTTON_PRESSED(PAD1, CIRCLE)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_ESCAPE)
			m_bOffSkipMenuSelectionMade = FALSE
			m_bOffSkipMenuAutoCompleteMade = FALSE
			m_bOffSkipMenuActive = FALSE
		ENDIF
		
		//Check for row selection change input.
		IF IS_BUTTON_PRESSED(PAD1, DPADUP)
		OR IS_KEYBOARD_KEY_PRESSED(KEY_UP)
		OR iStickLeftY < -30
			m_iCurrentRowSelected--
			IF m_iCurrentRowSelected < 0
				m_iCurrentRowSelected = (m_iMenuColumItemCount[m_iCurrentColumnSelected]-1)
			ENDIF
			m_iTimeLastMenuInput = GET_GAME_TIMER()
		ENDIF
		IF IS_BUTTON_PRESSED(PAD1, DPADDOWN)
		OR IS_KEYBOARD_KEY_PRESSED(KEY_DOWN)
		OR iStickLeftY > 30
			m_iCurrentRowSelected++
			IF m_iCurrentRowSelected >= m_iMenuColumItemCount[m_iCurrentColumnSelected]
				m_iCurrentRowSelected = 0
			ENDIF
			m_iTimeLastMenuInput = GET_GAME_TIMER()
		ENDIF
		
		//Check for column selection change input.
		IF IS_BUTTON_PRESSED(PAD1, DPADLEFT)
		OR IS_KEYBOARD_KEY_PRESSED(KEY_LEFT)
		OR iStickLeftX < -30
			m_iCurrentColumnSelected--
			IF m_iCurrentColumnSelected < 0
				m_iCurrentColumnSelected = (ENUM_TO_INT(OFFSKIP_MAX_COLUMNS)-1)
			ENDIF
			WHILE m_iMenuColumItemCount[m_iCurrentColumnSelected] = 0
				m_iCurrentColumnSelected--
				IF m_iCurrentColumnSelected < 0
					m_iCurrentColumnSelected = (ENUM_TO_INT(OFFSKIP_MAX_COLUMNS)-1)
				ENDIF
			ENDWHILE
			IF m_iCurrentRowSelected >= m_iMenuColumItemCount[m_iCurrentColumnSelected]
				m_iCurrentRowSelected = (m_iMenuColumItemCount[m_iCurrentColumnSelected]-1)
			ENDIF
			m_iTimeLastMenuInput = GET_GAME_TIMER()
		ENDIF
		IF IS_BUTTON_PRESSED(PAD1, DPADRIGHT)
		OR IS_KEYBOARD_KEY_PRESSED(KEY_RIGHT)
		OR iStickLeftX > 30
			m_iCurrentColumnSelected++
			IF m_iCurrentColumnSelected = ENUM_TO_INT(OFFSKIP_MAX_COLUMNS)
				m_iCurrentColumnSelected = 0
			ENDIF
			WHILE m_iMenuColumItemCount[m_iCurrentColumnSelected] = 0
				m_iCurrentColumnSelected++
				IF m_iCurrentColumnSelected = ENUM_TO_INT(OFFSKIP_MAX_COLUMNS)
					m_iCurrentColumnSelected = 0
				ENDIF
			ENDWHILE
			IF m_iCurrentRowSelected >= m_iMenuColumItemCount[m_iCurrentColumnSelected]
				m_iCurrentRowSelected = (m_iMenuColumItemCount[m_iCurrentColumnSelected]-1)
			ENDIF
			m_iTimeLastMenuInput = GET_GAME_TIMER()
		ENDIF
	ENDIF
ENDPROC


PROC Perform_OffSkip_Menu()
	PRINTSTRING("<OFFSKIP> J held. Running off-mission J-skip menu.\n")
	// Ignore if not in gameflow mode.
	#IF USE_CLF_DLC
		IF NOT g_savedGlobalsClifford.sFlow.isGameflowActive
			EXIT
		ENDIF
	#ENDIF
	#IF USE_NRM_DLC
		IF NOT g_savedGlobalsnorman.sFlow.isGameflowActive
			EXIT
		ENDIF
	#ENDIF
	
	#IF NOT USE_CLF_DLC
	#if not USE_NRM_DLC
	IF NOT g_savedGlobals.sFlow.isGameflowActive
		EXIT
	ENDIF
	#ENDIF
	#endif
	// Ignore if cellphone on screen
	IF (IS_PHONE_ONSCREEN())
		Print_OffSkip_Info_Text("Can't skip. Cellphone on screen.")
		EXIT
	ENDIF
	
	//Populate the menu's data.
	Populate_OffSkip_Menu_Data()
	
	//Were there any options available?
	BOOL bItemFound = FALSE
	INT index
	REPEAT OFFSKIP_MAX_COLUMNS index
		IF m_iMenuColumItemCount[index] > 0
			bItemFound = TRUE
		ENDIF
	ENDREPEAT
	IF NOT bItemFound
		Print_OffSkip_Info_Text("Nothing found to skip.")
		EXIT
	ENDIF
	
	//Disable player control.
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
	ENDIF
	
	//Start displaying help text.
	CLEAR_HELP()
	PRINT_HELP_FOREVER("OFFSKIP_H")
	
	//Find the first populated column to select in the menu.
	m_iCurrentColumnSelected = 0
	WHILE m_iMenuColumItemCount[m_iCurrentColumnSelected] = 0
		m_iCurrentColumnSelected++
	ENDWHILE
	m_iCurrentRowSelected = 0
	
	//Flag a result screen as displaying to safeguard against other systems interrupting our menu.
	SET_RESULT_SCREEN_DISPLAYING_STATE(TRUE)
	
	//Display the off-skip menu and wait for it to complete.
	m_bOffSkipMenuActive = TRUE
	WHILE m_bOffSkipMenuActive
		Display_OffSkip_Menu()
		Manage_OffSkip_Menu_Input()
		WAIT(0)
	ENDWHILE
	
	//Clean up result screen displaying flag.
	SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
	
	//Stop displaying help text.
	CLEAR_HELP(FALSE)
	
	//Re-enable player control.
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	
	//Perform any selected off-skip.
	IF m_bOffSkipMenuSelectionMade
		Perform_OffSkip_Menu_Selection()
	ELIF m_bOffSkipMenuAutoCompleteMade
		Perform_OffSkip_Menu_Auto_Complete()
	ENDIF
	
ENDPROC


PROC Maintain_KeyJ_Skips()

	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY)
	OR IS_PLAYER_PED_SWITCH_IN_PROGRESS()
	OR g_bIsOnRampage
	OR IS_TRANSITION_ACTIVE()
	OR GET_RANDOM_EVENT_FLAG()
	OR IS_MINIGAME_IN_PROGRESS()
	OR (g_iDebugSelectedFriendConnDisplay > 0)		//= 1 for F8 screen, =2 for F10 screen
	OR g_bMagDemoActive
	OR IS_RESULT_SCREEN_DISPLAYING()
		EXIT
	ENDIF
	
	Update_OffSkip_Screen_Text()

	IF iInputDelayTimer < GET_GAME_TIMER()
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			Perform_OffSkip_Menu()
			iInputDelayTimer = GET_GAME_TIMER() + 1500
		ENDIF
	ENDIF
ENDPROC

#ENDIF
