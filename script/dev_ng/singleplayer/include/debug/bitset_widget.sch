USING "rage_builtins.sch"
USING "globals.sch"

#IF IS_DEBUG_BUILD
BOOL BitsetWidget[32]
BOOL BitsetWidgetLastFrame[32]
WIDGET_GROUP_ID BitSetWidgetID
#endif

PROC CREATE_BITSET_WIDGET(INT BitSet = 0)
#IF IS_DEBUG_BUILD
	INT i
	BOOL bBitValue
	TEXT_LABEL str
	BitSetWidgetID = START_WIDGET_GROUP("Bitset Widget")
		REPEAT 32 i
			str = ""
			str += i
			ADD_WIDGET_BOOL(str, BitsetWidget[i])			
			// copy over initial values
			bBitValue = IS_BIT_SET(BitSet, i)
			BitsetWidget[i] = bBitValue
			BitsetWidgetLastFrame[i] = bBitValue			
		ENDREPEAT
	STOP_WIDGET_GROUP()
#endif
ENDPROC

PROC UPDATE_BITSET_WIDGET(INT &BitSet)
#IF IS_DEBUG_BUILD
	INT i
	BOOL bBitValue
	REPEAT 32 i	
		// has user changed a value in the widget since last frame?
		IF NOT (BitsetWidget[i] = BitsetWidgetLastFrame[i])			
			// update the bitset
			IF (BitsetWidget[i])
				SET_BIT(BitSet, i)
			ELSE
				CLEAR_BIT(BitSet, i)
			ENDIF		
		ENDIF	
		// has the bit set been changed by something else since last frame?
		bBitValue = IS_BIT_SET(BitSet, i)
		IF NOT (bBitValue = BitsetWidget[i])
			BitsetWidget[i] = bBitValue	
		ENDIF		
		// store values for next frame
		BitsetWidgetLastFrame[i] = BitsetWidget[i]
	ENDREPEAT
#endif
ENDPROC

PROC CLEANUP_BITSET_WIDGET()
#IF IS_DEBUG_BUILD
	IF DOES_WIDGET_GROUP_EXIST(BitSetWidgetID)
		DELETE_WIDGET_GROUP(BitSetWidgetID)
	ENDIF
#endif
ENDPROC

