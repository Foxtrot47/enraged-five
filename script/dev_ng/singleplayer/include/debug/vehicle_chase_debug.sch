// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	vehicle_chase_debug.sch
//		AUTHOR			:	Rob Schmitz
//		DESCRIPTION		:	Widgets and debug routines to aid 'vehicle chase' testing.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "commands_debug.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "script_player.sch"

CONST_INT VEHICLE_CHASE_DEBUG_MAX_CHASERS 5

STRUCT STRUCT_Vehicle_Chase_Debug_Controller
	BOOL bRunTest
	BOOL bTestStarted
ENDSTRUCT

STRUCT STRUCT_Vehicle_Chase_Debug_RunningData
	VEHICLE_INDEX iPlayerVehicle
	PED_INDEX aChaserPeds[VEHICLE_CHASE_DEBUG_MAX_CHASERS]
	VEHICLE_INDEX aChaserVehicles[VEHICLE_CHASE_DEBUG_MAX_CHASERS]
ENDSTRUCT

STRUCT STRUCT_Vehicle_Chase_Debug_PlayerParameters
	INT iVehicleModel
	INT iDrivingMode
	FLOAT fCruiseSpeed
	BOOL bUserControlled
ENDSTRUCT

STRUCT STRUCT_Vehicle_Chase_Debug_ChasersParameters
	INT iPedModel
	INT iVehicleModel
	INT iNumChasers
	BOOL bCantBlock
	BOOL bCantPursue
	BOOL bCantRam
	BOOL bCantSpinOut
	BOOL bCantMakeAggressiveMove
	FLOAT fIdealPursuitDistance
ENDSTRUCT

STRUCT STRUCT_Vehicle_Chase_Debug_EnvironmentParameters
	VECTOR vStart
	FLOAT fStartHeading
	VECTOR vEnd
ENDSTRUCT

STRUCT STRUCT_Vehicle_Chase_Debug_ChasersParameterSets
	BOOL bNone
	BOOL bPursue
	BOOL bChase
ENDSTRUCT

STRUCT STRUCT_Vehicle_Chase_Debug_EnvironmentParameterSets
	BOOL bHighway
	BOOL bCorner
ENDSTRUCT

STRUCT STRUCT_Vehicle_Chase_Debug_GameState
	INT iMaxWantedLevel
	BOOL bPlayerWillFlyThroughWindscreen
ENDSTRUCT

STRUCT STRUCT_Vehicle_Chase_Debug
	STRUCT_Vehicle_Chase_Debug_Controller Controller
	STRUCT_Vehicle_Chase_Debug_RunningData RunningData
	STRUCT_Vehicle_Chase_Debug_PlayerParameters PlayerParameters
	STRUCT_Vehicle_Chase_Debug_ChasersParameters ChasersParameters
	STRUCT_Vehicle_Chase_Debug_EnvironmentParameters EnvironmentParameters
	STRUCT_Vehicle_Chase_Debug_ChasersParameterSets ChasersParameterSets
	STRUCT_Vehicle_Chase_Debug_EnvironmentParameterSets EnvironmentParameterSets
	STRUCT_Vehicle_Chase_Debug_GameState GameState
ENDSTRUCT

PROC Create_Vehicle_Chase_Ped_Model_Widget(INT& iValue)
	START_NEW_WIDGET_COMBO()
		ADD_TO_WIDGET_COMBO("Chef")
		ADD_TO_WIDGET_COMBO("Cop")
	STOP_WIDGET_COMBO("Ped Model", iValue)
ENDPROC

FUNC MODEL_NAMES Vehicle_Chase_Debug_Int_To_Ped_ModelNames(INT iValue)

	SWITCH iValue
	
		CASE 0
			RETURN S_M_Y_CHEF_01

		CASE 1
			RETURN S_M_Y_COP_01
			
		DEFAULT
			RETURN S_M_Y_CHEF_01
	
	ENDSWITCH

ENDFUNC

PROC Create_Vehicle_Chase_Vehicle_Model_Widget(INT& iValue)
	START_NEW_WIDGET_COMBO()
		ADD_TO_WIDGET_COMBO("Tailgater")
		ADD_TO_WIDGET_COMBO("Police")
	STOP_WIDGET_COMBO("Vehicle Model", iValue)
ENDPROC

FUNC MODEL_NAMES Vehicle_Chase_Debug_Int_To_Vehicle_ModelNames(INT iValue)

	SWITCH iValue
	
		CASE 0
			RETURN TAILGATER
		
		CASE 1
			RETURN POLICE

		DEFAULT
			RETURN TAILGATER
	
	ENDSWITCH

ENDFUNC

PROC Create_Vehicle_Chase_Driving_Mode_Widget(INT& iValue)
	START_NEW_WIDGET_COMBO()
		ADD_TO_WIDGET_COMBO("Avoid Cars")
		ADD_TO_WIDGET_COMBO("Avoid Cars (Reckless)")
		ADD_TO_WIDGET_COMBO("Plough Through")
	STOP_WIDGET_COMBO("Driving Mode", iValue)
ENDPROC

FUNC DRIVINGMODE Vehicle_Chase_Debug_Int_To_DrivingMode(INT iValue)

	SWITCH iValue
	
		CASE 0
			RETURN DRIVINGMODE_AVOIDCARS
		CASE 1
			RETURN DRIVINGMODE_AVOIDCARS_RECKLESS
		CASE 2
			RETURN DRIVINGMODE_PLOUGHTHROUGH
		DEFAULT
			RETURN DRIVINGMODE_AVOIDCARS
	ENDSWITCH

ENDFUNC

PROC Initialise_Vehicle_Chase_Debug(STRUCT_Vehicle_Chase_Debug& rData)

	//Initialize the controller.
	rData.Controller.bRunTest = FALSE
	rData.Controller.bTestStarted = FALSE
	
	//Initialize the running data.
	rData.RunningData.iPlayerVehicle = NULL
	INT i
	FOR i = 0 TO VEHICLE_CHASE_DEBUG_MAX_CHASERS - 1
		rData.RunningData.aChaserPeds[i] = NULL
		rData.RunningData.aChaserVehicles[i] = NULL
	ENDFOR

	//Initialize the player parameters.
	rData.PlayerParameters.iVehicleModel = 0
	rData.PlayerParameters.iDrivingMode = 0
	rData.PlayerParameters.fCruiseSpeed = 50.0
	rData.PlayerParameters.bUserControlled = FALSE
	
	//Initialize the chasers parameters.
	rData.ChasersParameters.iPedModel = 1
	rData.ChasersParameters.iVehicleModel = 1
	rData.ChasersParameters.iNumChasers = 1
	rData.ChasersParameters.bCantBlock = FALSE
	rData.ChasersParameters.bCantPursue = FALSE
	rData.ChasersParameters.bCantRam = FALSE
	rData.ChasersParameters.bCantSpinOut = FALSE
	rData.ChasersParameters.bCantMakeAggressiveMove = FALSE
	rData.ChasersParameters.fIdealPursuitDistance = 15.0
	
	//Initialise the environment parameters.
	rData.EnvironmentParameters.vStart = <<0.0, 0.0, 0.0>>
	rData.EnvironmentParameters.fStartHeading = 0.0
	rData.EnvironmentParameters.vEnd = <<0.0, 0.0, 0.0>>
	
	//Initialize the chasers parameter sets.
	rData.ChasersParameterSets.bNone = FALSE
	rData.ChasersParameterSets.bPursue = FALSE
	rData.ChasersParameterSets.bChase = TRUE
	
	//Initialize the environment parameter sets.
	rData.EnvironmentParameterSets.bHighway = TRUE
	rData.EnvironmentParameterSets.bCorner = FALSE
	
	//Initialize the game state.
	rData.GameState.iMaxWantedLevel = GET_MAX_WANTED_LEVEL()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		rData.GameState.bPlayerWillFlyThroughWindscreen = GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen)
	ENDIF

ENDPROC

PROC Create_Vehicle_Chase_Debug_Widgets(STRUCT_Vehicle_Chase_Debug& rData)

	START_WIDGET_GROUP("Vehicle Chase Debug")
		START_WIDGET_GROUP("Player Parameters")
			Create_Vehicle_Chase_Vehicle_Model_Widget(rData.PlayerParameters.iVehicleModel)
			Create_Vehicle_Chase_Driving_Mode_Widget(rData.PlayerParameters.iDrivingMode)
			ADD_WIDGET_FLOAT_SLIDER("Cruise Speed", rData.PlayerParameters.fCruiseSpeed, 0.0, 100.0, 1.0)
			ADD_WIDGET_BOOL("User Controlled", rData.PlayerParameters.bUserControlled)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Chasers Parameters")
			Create_Vehicle_Chase_Ped_Model_Widget(rData.ChasersParameters.iPedModel)
			Create_Vehicle_Chase_Vehicle_Model_Widget(rData.ChasersParameters.iVehicleModel)
			ADD_WIDGET_INT_SLIDER("Num Chasers", rData.ChasersParameters.iNumChasers, 0, VEHICLE_CHASE_DEBUG_MAX_CHASERS, 1)
			ADD_WIDGET_BOOL("Can't Block", rData.ChasersParameters.bCantBlock)
			ADD_WIDGET_BOOL("Can't Pursue", rData.ChasersParameters.bCantPursue)
			ADD_WIDGET_BOOL("Can't Ram", rData.ChasersParameters.bCantRam)
			ADD_WIDGET_BOOL("Can't Spin Out", rData.ChasersParameters.bCantSpinOut)
			ADD_WIDGET_BOOL("Can't Make Aggressive Move", rData.ChasersParameters.bCantMakeAggressiveMove)
			ADD_WIDGET_FLOAT_SLIDER("Ideal Pursuit Distance", rData.ChasersParameters.fIdealPursuitDistance, 0.0, 50.0, 1.0)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Environment Parameters")
			ADD_WIDGET_FLOAT_SLIDER("Start X", rData.EnvironmentParameters.vStart.x, -10000.0, 10000.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Start Y", rData.EnvironmentParameters.vStart.y, -10000.0, 10000.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Start Z", rData.EnvironmentParameters.vStart.z, -10000.0, 10000.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Start Heading", rData.EnvironmentParameters.fStartHeading, 0.0, 360.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("End X", rData.EnvironmentParameters.vEnd.x, -10000.0, 10000.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("End Y", rData.EnvironmentParameters.vEnd.y, -10000.0, 10000.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("End Z", rData.EnvironmentParameters.vEnd.z, -10000.0, 10000.0, 1.0)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Chasers Parameter Sets")
			ADD_WIDGET_BOOL("None", rData.ChasersParameterSets.bNone)
			ADD_WIDGET_BOOL("Pursue", rData.ChasersParameterSets.bPursue)
			ADD_WIDGET_BOOL("Chase", rData.ChasersParameterSets.bChase)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Environment Parameter Sets")
			ADD_WIDGET_BOOL("Highway", rData.EnvironmentParameterSets.bHighway)
			ADD_WIDGET_BOOL("Corner", rData.EnvironmentParameterSets.bCorner)
		STOP_WIDGET_GROUP()
		ADD_WIDGET_BOOL("Run Test", rData.Controller.bRunTest)
	STOP_WIDGET_GROUP()

ENDPROC

FUNC VEHICLE_INDEX Vehicle_Chase_Debug_Create_Vehicle(MODEL_NAMES nModelName, VECTOR vPosition, FLOAT fHeading)

	//Create a vehicle.
	VEHICLE_INDEX iVehicleIndex = CREATE_VEHICLE(nModelName, vPosition, fHeading)
	
	//Set the vehicle on the ground.
	SET_VEHICLE_ON_GROUND_PROPERLY(iVehicleIndex)
	
	//Turn the engine on.
	SET_VEHICLE_ENGINE_ON(iVehicleIndex, TRUE, TRUE)
	
	RETURN iVehicleIndex

ENDFUNC

FUNC BOOL Vehicle_Chase_Debug_Stream(STRUCT_Vehicle_Chase_Debug& rData)

	//Stream the model for the player vehicle.
	MODEL_NAMES mVehicle = Vehicle_Chase_Debug_Int_To_Vehicle_ModelNames(rData.PlayerParameters.iVehicleModel)
	REQUEST_MODEL(mVehicle)
	IF NOT HAS_MODEL_LOADED(mVehicle)
		RETURN FALSE
	ENDIF

	//Stream the model for the chaser ped.
	MODEL_NAMES mPed = Vehicle_Chase_Debug_Int_To_Ped_ModelNames(rData.ChasersParameters.iPedModel)
	REQUEST_MODEL(mPed)
	IF NOT HAS_MODEL_LOADED(mPed)
		RETURN FALSE
	ENDIF
	
	//Stream the model for the chaser vehicles.
	mVehicle = Vehicle_Chase_Debug_Int_To_Vehicle_ModelNames(rData.ChasersParameters.iVehicleModel)
	REQUEST_MODEL(mVehicle)
	IF NOT HAS_MODEL_LOADED(mVehicle)
		RETURN FALSE
	ENDIF
	
	//Stream all path nodes.
	IF NOT LOAD_ALL_PATH_NODES(TRUE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

PROC Vehicle_Chase_Debug_Save_Game_State(STRUCT_Vehicle_Chase_Debug& rData)

	//Save the max wanted level.
	rData.GameState.iMaxWantedLevel = GET_MAX_WANTED_LEVEL()
	
	//Save whether the player will fly through the windscreen.
	rData.GameState.bPlayerWillFlyThroughWindscreen = GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen)

ENDPROC

PROC Vehicle_Chase_Debug_Restore_Game_State(STRUCT_Vehicle_Chase_Debug& rData)

	//Restore the max wanted level.
	SET_MAX_WANTED_LEVEL(rData.GameState.iMaxWantedLevel)
	
	//Restore whether the player will fly through the windscreen.
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, rData.GameState.bPlayerWillFlyThroughWindscreen)

ENDPROC

PROC Vehicle_Chase_Debug_Setup_Game_State()

	//Set the max wanted level.
	SET_MAX_WANTED_LEVEL(0)
	
	//Block the player from flying through the windscreen.
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)

ENDPROC

PROC Vehicle_Chase_Debug_Setup_Player(STRUCT_Vehicle_Chase_Debug& rData)

	//Grab the player index.
	PED_INDEX iPlayerIndex = PLAYER_PED_ID()

	//Clear the player tasks.
	CLEAR_PED_TASKS_IMMEDIATELY(iPlayerIndex)
	
	//Move the player to the start position.
	SET_PED_COORDS_KEEP_VEHICLE(iPlayerIndex, rData.EnvironmentParameters.vStart)
	
	//Create the vehicle.
	MODEL_NAMES mVehicle = Vehicle_Chase_Debug_Int_To_Vehicle_ModelNames(rData.PlayerParameters.iVehicleModel)
	VEHICLE_INDEX iVehicleIndex = Vehicle_Chase_Debug_Create_Vehicle(mVehicle, rData.EnvironmentParameters.vStart, rData.EnvironmentParameters.fStartHeading)
	
	//Set the player in the vehicle.
	SET_PED_INTO_VEHICLE(iPlayerIndex, iVehicleIndex)
	
	//Check if the player is not user controlled.
	IF NOT rData.PlayerParameters.bUserControlled
	
		//Cruise to the end position.
		DRIVINGMODE nDrivingMode = Vehicle_Chase_Debug_Int_To_DrivingMode(rData.PlayerParameters.iDrivingMode)
		TASK_VEHICLE_MISSION_COORS_TARGET(iPlayerIndex, iVehicleIndex, rData.EnvironmentParameters.vEnd, MISSION_GOTO, rData.PlayerParameters.fCruiseSpeed, nDrivingMode, 5.0, 20.0, FALSE)
	ENDIF
	
	//Update the running data.
	rData.RunningData.iPlayerVehicle = iVehicleIndex

ENDPROC

PROC Vehicle_Chase_Debug_Setup_Chaser(STRUCT_Vehicle_Chase_Debug& rData, INT iChaser)

	//Calculate the position.
	VECTOR vPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0, -15.0 * (iChaser + 1), 0.0>>)
	
	//Create the ped.
	MODEL_NAMES mPed = Vehicle_Chase_Debug_Int_To_Ped_ModelNames(rData.ChasersParameters.iPedModel)
	PED_INDEX iPedIndex = CREATE_PED(PEDTYPE_MISSION, mPed, vPosition, rData.EnvironmentParameters.fStartHeading)
	
	//Block the ped from flying through the windscreen.
	SET_PED_CONFIG_FLAG(iPedIndex, PCF_WillFlyThroughWindscreen, FALSE)
	
	//Create the vehicle.
	MODEL_NAMES mVehicle = Vehicle_Chase_Debug_Int_To_Vehicle_ModelNames(rData.ChasersParameters.iVehicleModel)
	VEHICLE_INDEX iVehicleIndex = Vehicle_Chase_Debug_Create_Vehicle(mVehicle, vPosition, rData.EnvironmentParameters.fStartHeading)
	
	//Set the ped in the vehicle.
	SET_PED_INTO_VEHICLE(iPedIndex, iVehicleIndex)
	
	//Chase the player.
	TASK_VEHICLE_CHASE(iPedIndex, PLAYER_PED_ID())
	
	//Update the running data.
	rData.RunningData.aChaserPeds[iChaser] = iPedIndex
	rData.RunningData.aChaserVehicles[iChaser] = iVehicleIndex

ENDPROC

PROC Vehicle_Chase_Debug_Setup_Chasers(STRUCT_Vehicle_Chase_Debug& rData)

	INT i
	FOR i = 0 TO rData.ChasersParameters.iNumChasers - 1
		Vehicle_Chase_Debug_Setup_Chaser(rData, i)
	ENDFOR

ENDPROC

PROC Vehicle_Chase_Debug_Start(STRUCT_Vehicle_Chase_Debug& rData)
	
	//Note that the test has started.
	rData.Controller.bTestStarted = TRUE
	
	//Save the game state.
	Vehicle_Chase_Debug_Save_Game_State(rData)
	
	//Set up the game state.
	Vehicle_Chase_Debug_Setup_Game_State()
	
	//Set up the player.
	Vehicle_Chase_Debug_Setup_Player(rData)
	
	//Set up the chasers.
	Vehicle_Chase_Debug_Setup_Chasers(rData)
	
ENDPROC

PROC Vehicle_Chase_Debug_Maintain(STRUCT_Vehicle_Chase_Debug& rData)

	//Maintain the chasers.
	INT i
	FOR i = 0 TO VEHICLE_CHASE_DEBUG_MAX_CHASERS - 1
		
		//Check if the chaser ped is valid.
		PED_INDEX iPedIndex = rData.RunningData.aChaserPeds[i]
		IF iPedIndex != NULL
		
			//Check if the ped is injured.
			IF NOT IS_PED_INJURED(iPedIndex)
			
				//Check if the ped is running the chase task.
				IF GET_SCRIPT_TASK_STATUS(iPedIndex, SCRIPT_TASK_VEHICLE_CHASE) = PERFORMING_TASK
					
					//Set the behavior flags.
					SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG(iPedIndex, VEHICLE_CHASE_CANT_BLOCK,					rData.ChasersParameters.bCantBlock)
					SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG(iPedIndex, VEHICLE_CHASE_CANT_PURSUE,					rData.ChasersParameters.bCantPursue)
					SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG(iPedIndex, VEHICLE_CHASE_CANT_RAM,						rData.ChasersParameters.bCantRam)
					SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG(iPedIndex, VEHICLE_CHASE_CANT_SPIN_OUT,				rData.ChasersParameters.bCantSpinOut)
					SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG(iPedIndex, VEHICLE_CHASE_CANT_MAKE_AGGRESSIVE_MOVE,	rData.ChasersParameters.bCantMakeAggressiveMove)
				
					//Set the ideal pursuit distance.
					SET_TASK_VEHICLE_CHASE_IDEAL_PURSUIT_DISTANCE(iPedIndex, rData.ChasersParameters.fIdealPursuitDistance)
					
				ENDIF
			
			ENDIF
		
		ENDIF
		
	ENDFOR

ENDPROC

PROC Vehicle_Chase_Debug_Clean_Up(STRUCT_Vehicle_Chase_Debug& rData)

	//Unload all path nodes.
	LOAD_ALL_PATH_NODES(FALSE)
	
	//Grab the player index.
	PED_INDEX iPlayerIndex = PLAYER_PED_ID()

	//Clear the ped tasks.
	CLEAR_PED_TASKS_IMMEDIATELY(iPlayerIndex)
	
	//Clear the player vehicle.
	SET_VEHICLE_AS_NO_LONGER_NEEDED(rData.RunningData.iPlayerVehicle)
	rData.RunningData.iPlayerVehicle = NULL
	
	//Clear the chasers.
	INT i
	FOR i = 0 TO VEHICLE_CHASE_DEBUG_MAX_CHASERS - 1
	
		//Release the ped.
		PED_INDEX iPedIndex = rData.RunningData.aChaserPeds[i]
		IF iPedIndex != NULL
			SET_PED_AS_NO_LONGER_NEEDED(iPedIndex)
		ENDIF
		
		//Release the vehicle.
		VEHICLE_INDEX iVehicleIndex = rData.RunningData.aChaserVehicles[i]
		IF iVehicleIndex != NULL
			SET_VEHICLE_AS_NO_LONGER_NEEDED(iVehicleIndex)
		ENDIF
		
		//Clear the data.
		rData.RunningData.aChaserPeds[i] = NULL
		rData.RunningData.aChaserVehicles[i] = NULL
		
	ENDFOR
	
ENDPROC

PROC Vehicle_Chase_Debug_Stop(STRUCT_Vehicle_Chase_Debug& rData)
	
	//Note that the test has stopped.
	rData.Controller.bTestStarted = FALSE
	
	//Restore the game state.
	Vehicle_Chase_Debug_Restore_Game_State(rData)
	
	//Clean up.
	Vehicle_Chase_Debug_Clean_Up(rData)
	
ENDPROC

PROC Vehicle_Chase_Debug_ProcessParameterSets(STRUCT_Vehicle_Chase_Debug& rData)

	//Process the chasers parameter sets.
	IF rData.ChasersParameterSets.bNone
	
		rData.ChasersParameterSets.bNone = FALSE
		
		rData.ChasersParameters.iNumChasers = 0
		
	ENDIF
	IF rData.ChasersParameterSets.bPursue
	
		rData.ChasersParameterSets.bPursue = FALSE
		
		rData.ChasersParameters.iNumChasers = 1
		rData.ChasersParameters.bCantBlock = TRUE
		rData.ChasersParameters.bCantPursue = FALSE
		rData.ChasersParameters.bCantRam = TRUE
		rData.ChasersParameters.bCantSpinOut = TRUE
		rData.ChasersParameters.bCantMakeAggressiveMove = TRUE
		
	ENDIF
	IF rData.ChasersParameterSets.bChase
	
		rData.ChasersParameterSets.bChase = FALSE
		
		rData.ChasersParameters.iNumChasers = 1
		rData.ChasersParameters.bCantBlock = FALSE
		rData.ChasersParameters.bCantPursue = FALSE
		rData.ChasersParameters.bCantRam = FALSE
		rData.ChasersParameters.bCantSpinOut = FALSE
		rData.ChasersParameters.bCantMakeAggressiveMove = FALSE
		
	ENDIF
		
	//Process the environment parameter sets.
	IF rData.EnvironmentParameterSets.bHighway
	
		rData.EnvironmentParameterSets.bHighway = FALSE
		
		rData.EnvironmentParameters.vStart = <<2519.77, 643.73, 106.45>>
		rData.EnvironmentParameters.fStartHeading = 180.0
		rData.EnvironmentParameters.vEnd = <<1392.89,-1074.37,54.31>>
	
	ENDIF
	IF rData.EnvironmentParameterSets.bCorner
	
		rData.EnvironmentParameterSets.bCorner = FALSE
		
		rData.EnvironmentParameters.vStart = <<-27.22, -948.10, 29.58>>
		rData.EnvironmentParameters.fStartHeading = 90.0
		rData.EnvironmentParameters.vEnd = <<-150.20, -798.10, 31.82>>
	
	ENDIF

ENDPROC

PROC Maintain_Vehicle_Chase_Debug(STRUCT_Vehicle_Chase_Debug& rData)

	//Process the parameter sets.
	Vehicle_Chase_Debug_ProcessParameterSets(rData)
	
	IF NOT rData.Controller.bTestStarted
	
		IF rData.Controller.bRunTest
		
			IF Vehicle_Chase_Debug_Stream(rData)
		
				Vehicle_Chase_Debug_Start(rData)
				
			ENDIF
		
		ENDIF
	
	ELSE
	
		IF NOT rData.Controller.bRunTest
		
			Vehicle_Chase_Debug_Stop(rData)
			
		ELSE
		
			Vehicle_Chase_Debug_Maintain(rData)
		
		ENDIF
	
	ENDIF
	
ENDPROC
