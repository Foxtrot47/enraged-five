USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_entity.sch"

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	SP_Respawn_Debug.sch
//		AUTHOR			:	Keith (copied from Kenneth's .sc file)
//		DESCRIPTION		:	Contains any standard Respawn debug functionality.
//		NOTES			:	Respawns are after being busted and wasted (police and hospital)
//							after a game launch or a reload (savehouse) or on return from
//							multiplayer.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


// NOTE: Only the respawn_controller will include this debug header file so it is okay for
//			these variables to be loose rather than wrapped up in a struct.

CONST_INT RESPAWN_FLAG_ACTUAL 0
CONST_INT RESPAWN_FLAG_DEBUG  1

INT iCount
INT iSavehouseSelection 	= 0
INT iHospitalSelection 		= 0
INT iPoliceStationSelection = 0
BOOL bWarpToSavehouse 		= FALSE
BOOL bWarpToHospital 		= FALSE
BOOL bWarpToPoliceStation 	= FALSE

#if USE_CLF_DLC
	BOOL bSavehouseAvailable[NUMBER_OF_CLF_SAVEHOUSE][2]
#endif
#if USE_NRM_DLC
	BOOL bSavehouseAvailable[NUMBER_OF_NRM_SAVEHOUSE][2]
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	BOOL bSavehouseAvailable[NUMBER_OF_SAVEHOUSE_LOCATIONS][2]
#endif
#endif
	
BOOL bHospitalAvailable[NUMBER_OF_HOSPITAL_LOCATIONS][2]
BOOL bPoliceStationAvailable[NUMBER_OF_POLICE_STATION_LOCATIONS][2]

TEXT_LABEL_63 sText
#if USE_CLF_DLC
FUNC WIDGET_GROUP_ID SETUP_RESPAWN_CONTROL_WIDGETSCLF()
	WIDGET_GROUP_ID respawn_widget_group = START_WIDGET_GROUP("Respawn Controller")
		ADD_WIDGET_BOOL("g_bDebug_KeepRespawnControllerRunning", g_bDebug_KeepRespawnControllerRunning)
		
		START_WIDGET_GROUP("Savehouses")
			START_NEW_WIDGET_COMBO()
				FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_CLF_SAVEHOUSE)-1)
					ADD_TO_WIDGET_COMBO(Get_Savehouse_Respawn_Name(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)))
				ENDFOR
			STOP_WIDGET_COMBO("Savehouses", iSavehouseSelection)
			ADD_WIDGET_BOOL("Warp to selected savehouse", bWarpToSavehouse)
			
			FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_CLF_SAVEHOUSE)-1)
				sText = Get_Savehouse_Respawn_Name(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount))
				sText += " Available"
				
				bSavehouseAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bSavehouseAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				ADD_WIDGET_BOOL(sText, bSavehouseAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDFOR
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Hospitals")
			START_NEW_WIDGET_COMBO()
				FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_HOSPITAL_LOCATIONS)-1)
					ADD_TO_WIDGET_COMBO(Get_Hospital_Respawn_Name(INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)))
				ENDFOR
			STOP_WIDGET_COMBO("Hospitals", iHospitalSelection)
			ADD_WIDGET_BOOL("Warp to selected hospital", bWarpToHospital)
			
			FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_HOSPITAL_LOCATIONS)-1)
				sText = Get_Hospital_Respawn_Name(INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount))
				sText += " Available"
				
				bHospitalAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bHospitalAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				ADD_WIDGET_BOOL(sText, bHospitalAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDFOR
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Police Stations")
			START_NEW_WIDGET_COMBO()
				FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_POLICE_STATION_LOCATIONS)-1)
					ADD_TO_WIDGET_COMBO(Get_Police_Respawn_Name(INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)))
				ENDFOR
			STOP_WIDGET_COMBO("Police Stations", iPoliceStationSelection)
			ADD_WIDGET_BOOL("Warp to selected police station", bWarpToPoliceStation)
			
			FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_POLICE_STATION_LOCATIONS)-1)
				sText = Get_Police_Respawn_Name(INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount))
				sText += " Available"
				
				bPoliceStationAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bPoliceStationAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				ADD_WIDGET_BOOL(sText, bPoliceStationAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDFOR
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Respawn Flags")
			ADD_WIDGET_BOOL("bSeenFirstTimeWasted", g_savedGlobalsClifford.sRespawnData.bSeenFirstTimeWasted)
			ADD_WIDGET_BOOL("bSeenFirstTimeDrowned", g_savedGlobalsClifford.sRespawnData.bSeenFirstTimeDrowned)
			ADD_WIDGET_BOOL("bSeenFirstTimeBusted", g_savedGlobalsClifford.sRespawnData.bSeenFirstTimeBusted)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	RETURN respawn_widget_group
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC WIDGET_GROUP_ID SETUP_RESPAWN_CONTROL_WIDGETSNRM()
	WIDGET_GROUP_ID respawn_widget_group = START_WIDGET_GROUP("Respawn Controller")
		ADD_WIDGET_BOOL("g_bDebug_KeepRespawnControllerRunning", g_bDebug_KeepRespawnControllerRunning)
		
		START_WIDGET_GROUP("Savehouses")
			START_NEW_WIDGET_COMBO()
				FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_NRM_SAVEHOUSE)-1)
					ADD_TO_WIDGET_COMBO(Get_Savehouse_Respawn_Name(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)))
				ENDFOR
			STOP_WIDGET_COMBO("Savehouses", iSavehouseSelection)
			ADD_WIDGET_BOOL("Warp to selected savehouse", bWarpToSavehouse)
			
			FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_NRM_SAVEHOUSE)-1)
				sText = Get_Savehouse_Respawn_Name(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount))
				sText += " Available"
				
				bSavehouseAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bSavehouseAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				ADD_WIDGET_BOOL(sText, bSavehouseAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDFOR
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Hospitals")
			START_NEW_WIDGET_COMBO()
				FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_HOSPITAL_LOCATIONS)-1)
					ADD_TO_WIDGET_COMBO(Get_Hospital_Respawn_Name(INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)))
				ENDFOR
			STOP_WIDGET_COMBO("Hospitals", iHospitalSelection)
			ADD_WIDGET_BOOL("Warp to selected hospital", bWarpToHospital)
			
			FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_HOSPITAL_LOCATIONS)-1)
				sText = Get_Hospital_Respawn_Name(INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount))
				sText += " Available"
				
				bHospitalAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bHospitalAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				ADD_WIDGET_BOOL(sText, bHospitalAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDFOR
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Police Stations")
			START_NEW_WIDGET_COMBO()
				FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_POLICE_STATION_LOCATIONS)-1)
					ADD_TO_WIDGET_COMBO(Get_Police_Respawn_Name(INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)))
				ENDFOR
			STOP_WIDGET_COMBO("Police Stations", iPoliceStationSelection)
			ADD_WIDGET_BOOL("Warp to selected police station", bWarpToPoliceStation)
			
			FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_POLICE_STATION_LOCATIONS)-1)
				sText = Get_Police_Respawn_Name(INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount))
				sText += " Available"
				
				bPoliceStationAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bPoliceStationAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				ADD_WIDGET_BOOL(sText, bPoliceStationAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDFOR
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Respawn Flags")
			ADD_WIDGET_BOOL("bSeenFirstTimeWasted", g_savedGlobalsnorman.sRespawnData.bSeenFirstTimeWasted)
			ADD_WIDGET_BOOL("bSeenFirstTimeDrowned", g_savedGlobalsnorman.sRespawnData.bSeenFirstTimeDrowned)
			ADD_WIDGET_BOOL("bSeenFirstTimeBusted", g_savedGlobalsnorman.sRespawnData.bSeenFirstTimeBusted)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	RETURN respawn_widget_group
ENDFUNC
#endif
FUNC WIDGET_GROUP_ID SETUP_RESPAWN_CONTROL_WIDGETS()
#if USE_CLF_DLC
	RETURN SETUP_RESPAWN_CONTROL_WIDGETSCLF()
#endif
#if USE_NRM_DLC
	RETURN SETUP_RESPAWN_CONTROL_WIDGETSNRM()
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC

	WIDGET_GROUP_ID respawn_widget_group = START_WIDGET_GROUP("Respawn Controller")
		ADD_WIDGET_BOOL("g_bDebug_KeepRespawnControllerRunning", g_bDebug_KeepRespawnControllerRunning)
		
		START_WIDGET_GROUP("Savehouses")
			START_NEW_WIDGET_COMBO()
				FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_SAVEHOUSE_LOCATIONS)-1)
					ADD_TO_WIDGET_COMBO(Get_Savehouse_Respawn_Name(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount)))
				ENDFOR
			STOP_WIDGET_COMBO("Savehouses", iSavehouseSelection)
			ADD_WIDGET_BOOL("Warp to selected savehouse", bWarpToSavehouse)
			
			FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_SAVEHOUSE_LOCATIONS)-1)
				sText = Get_Savehouse_Respawn_Name(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount))
				sText += " Available"
				
				bSavehouseAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bSavehouseAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				ADD_WIDGET_BOOL(sText, bSavehouseAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDFOR
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Hospitals")
			START_NEW_WIDGET_COMBO()
				FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_HOSPITAL_LOCATIONS)-1)
					ADD_TO_WIDGET_COMBO(Get_Hospital_Respawn_Name(INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount)))
				ENDFOR
			STOP_WIDGET_COMBO("Hospitals", iHospitalSelection)
			ADD_WIDGET_BOOL("Warp to selected hospital", bWarpToHospital)
			
			FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_HOSPITAL_LOCATIONS)-1)
				sText = Get_Hospital_Respawn_Name(INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount))
				sText += " Available"
				
				bHospitalAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobals.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bHospitalAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobals.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				ADD_WIDGET_BOOL(sText, bHospitalAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDFOR
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Police Stations")
			START_NEW_WIDGET_COMBO()
				FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_POLICE_STATION_LOCATIONS)-1)
					ADD_TO_WIDGET_COMBO(Get_Police_Respawn_Name(INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount)))
				ENDFOR
			STOP_WIDGET_COMBO("Police Stations", iPoliceStationSelection)
			ADD_WIDGET_BOOL("Warp to selected police station", bWarpToPoliceStation)
			
			FOR iCount = 0 TO (ENUM_TO_INT(NUMBER_OF_POLICE_STATION_LOCATIONS)-1)
				sText = Get_Police_Respawn_Name(INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount))
				sText += " Available"
				
				bPoliceStationAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobals.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bPoliceStationAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobals.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				ADD_WIDGET_BOOL(sText, bPoliceStationAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDFOR
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Respawn Flags")
			ADD_WIDGET_BOOL("bSeenFirstTimeWasted", g_savedGlobals.sRespawnData.bSeenFirstTimeWasted)
			ADD_WIDGET_BOOL("bSeenFirstTimeDrowned", g_savedGlobals.sRespawnData.bSeenFirstTimeDrowned)
			ADD_WIDGET_BOOL("bSeenFirstTimeBusted", g_savedGlobals.sRespawnData.bSeenFirstTimeBusted)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	RETURN respawn_widget_group
#endif
#endif
ENDFUNC

PROC MAINTAIN_RESPAWN_CONTROL_WIDGETS()
	////////////////////////////////////////////////////////////////////////
	///      SAVEHOUSES
	
	#if USE_CLF_DLC
		IF bWarpToSavehouse
			IF iSavehouseSelection < ENUM_TO_INT(NUMBER_OF_CLF_SAVEHOUSE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iSavehouseSelection)].vSpawnCoords)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iSavehouseSelection)].fSpawnHeading)
					bWarpToSavehouse = FALSE
				ENDIF
			ENDIF
		ENDIF
	
		FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_CLF_SAVEHOUSE)-1
			IF NOT (IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bSavehouseAvailable[iCount][RESPAWN_FLAG_ACTUAL])
				bSavehouseAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bSavehouseAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
			
			// Otherwise if we updated the debug flag then update the real flag
			ELIF NOT (IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bSavehouseAvailable[iCount][RESPAWN_FLAG_DEBUG])
				Set_Savehouse_Respawn_Available(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount), bSavehouseAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDIF
		endfor	
		
	#endif
	#if USE_NRM_DLC
		IF bWarpToSavehouse
			IF iSavehouseSelection < ENUM_TO_INT(NUMBER_OF_NRM_SAVEHOUSE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iSavehouseSelection)].vSpawnCoords)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iSavehouseSelection)].fSpawnHeading)
					bWarpToSavehouse = FALSE
				ENDIF
			ENDIF
		ENDIF
		FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_NRM_SAVEHOUSE)-1
			IF NOT (IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bSavehouseAvailable[iCount][RESPAWN_FLAG_ACTUAL])
				bSavehouseAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bSavehouseAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
			
			// Otherwise if we updated the debug flag then update the real flag
			ELIF NOT (IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bSavehouseAvailable[iCount][RESPAWN_FLAG_DEBUG])
				Set_Savehouse_Respawn_Available(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount), bSavehouseAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDIF
		endfor	
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF bWarpToSavehouse
			IF iSavehouseSelection < ENUM_TO_INT(NUMBER_OF_SAVEHOUSE_LOCATIONS)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iSavehouseSelection)].vSpawnCoords)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), g_sSavehouses[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iSavehouseSelection)].fSpawnHeading)
					bWarpToSavehouse = FALSE
				ENDIF
			ENDIF
		ENDIF
		FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_SAVEHOUSE_LOCATIONS)-1
			// If the real flag has been updated then update our debug flags	
			IF NOT (IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bSavehouseAvailable[iCount][RESPAWN_FLAG_ACTUAL])
				bSavehouseAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bSavehouseAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
			
			// Otherwise if we updated the debug flag then update the real flag
			ELIF NOT (IS_BIT_SET(g_savedGlobals.sRespawnData.iSavehouseProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bSavehouseAvailable[iCount][RESPAWN_FLAG_DEBUG])
				Set_Savehouse_Respawn_Available(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iCount), bSavehouseAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDIF
		ENDFOR
	#endif
	#endif
	
	
	////////////////////////////////////////////////////////////////////////
	///      HOSPITALS
	IF bWarpToHospital
		IF iHospitalSelection < ENUM_TO_INT(NUMBER_OF_HOSPITAL_LOCATIONS)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), g_sHospitals[INT_TO_ENUM(HOSPITAL_NAME_ENUM, iHospitalSelection)].vSpawnCoords)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), g_sHospitals[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iHospitalSelection)].fSpawnHeading)
				bWarpToHospital = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_HOSPITAL_LOCATIONS)-1
		#if USE_CLF_DLC
			// If the real flag has been updated then update our debug flags
			IF NOT (IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bHospitalAvailable[iCount][RESPAWN_FLAG_ACTUAL])
				bHospitalAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bHospitalAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
			
			// Otherwise if we updated the debug flag then update the real flag
			ELIF NOT (IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bHospitalAvailable[iCount][RESPAWN_FLAG_DEBUG])
				Set_Hospital_Respawn_Available(INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount), bHospitalAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDIF
		#endif	
		#if USE_NRM_DLC
			// If the real flag has been updated then update our debug flags
			IF NOT (IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bHospitalAvailable[iCount][RESPAWN_FLAG_ACTUAL])
				bHospitalAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bHospitalAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
			
			// Otherwise if we updated the debug flag then update the real flag
			ELIF NOT (IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bHospitalAvailable[iCount][RESPAWN_FLAG_DEBUG])
				Set_Hospital_Respawn_Available(INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount), bHospitalAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDIF
		#endif	
		#if not USE_CLF_DLC
		#if not use_NRM_DLC
			// If the real flag has been updated then update our debug flags
			IF NOT (IS_BIT_SET(g_savedGlobals.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bHospitalAvailable[iCount][RESPAWN_FLAG_ACTUAL])
				bHospitalAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobals.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bHospitalAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobals.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
			
			// Otherwise if we updated the debug flag then update the real flag
			ELIF NOT (IS_BIT_SET(g_savedGlobals.sRespawnData.iHospitalProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bHospitalAvailable[iCount][RESPAWN_FLAG_DEBUG])
				Set_Hospital_Respawn_Available(INT_TO_ENUM(HOSPITAL_NAME_ENUM, iCount), bHospitalAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDIF
		#endif
		#endif
	ENDFOR
	
	////////////////////////////////////////////////////////////////////////
	///      POLICE STATIONS
	IF bWarpToPoliceStation
		IF iPoliceStationSelection < ENUM_TO_INT(NUMBER_OF_POLICE_STATION_LOCATIONS)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), g_sPoliceStations[INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iPoliceStationSelection)].vSpawnCoords)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), g_sPoliceStations[INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iPoliceStationSelection)].fSpawnHeading)
				bWarpToPoliceStation = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_POLICE_STATION_LOCATIONS)-1
		#if USE_CLF_DLC
			// If the real flag has been updated then update our debug flags
			IF NOT (IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bPoliceStationAvailable[iCount][RESPAWN_FLAG_ACTUAL])
				bPoliceStationAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bPoliceStationAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
			
			// Otherwise if we updated the debug flag then update the real flag
			ELIF NOT (IS_BIT_SET(g_savedGlobalsClifford.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bPoliceStationAvailable[iCount][RESPAWN_FLAG_DEBUG])
				Set_Police_Respawn_Available(INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount), bPoliceStationAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDIF
		#endif
		#if USE_NRM_DLC
			// If the real flag has been updated then update our debug flags
			IF NOT (IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bPoliceStationAvailable[iCount][RESPAWN_FLAG_ACTUAL])
				bPoliceStationAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bPoliceStationAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
			
			// Otherwise if we updated the debug flag then update the real flag
			ELIF NOT (IS_BIT_SET(g_savedGlobalsnorman.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bPoliceStationAvailable[iCount][RESPAWN_FLAG_DEBUG])
				Set_Police_Respawn_Available(INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount), bPoliceStationAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDIF
		#endif
		
		#if not USE_CLF_DLC
		#if not use_NRM_DLC
			// If the real flag has been updated then update our debug flags
			IF NOT (IS_BIT_SET(g_savedGlobals.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bPoliceStationAvailable[iCount][RESPAWN_FLAG_ACTUAL])
				bPoliceStationAvailable[iCount][RESPAWN_FLAG_ACTUAL] = IS_BIT_SET(g_savedGlobals.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
				bPoliceStationAvailable[iCount][RESPAWN_FLAG_DEBUG] = IS_BIT_SET(g_savedGlobals.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT)
			
			// Otherwise if we updated the debug flag then update the real flag
			ELIF NOT (IS_BIT_SET(g_savedGlobals.sRespawnData.iPoliceStationProperties[iCount], REPAWN_FLAG_AVAILABLE_BIT) = bPoliceStationAvailable[iCount][RESPAWN_FLAG_DEBUG])
				Set_Police_Respawn_Available(INT_TO_ENUM(POLICE_STATION_NAME_ENUM, iCount), bPoliceStationAvailable[iCount][RESPAWN_FLAG_DEBUG])
			ENDIF
		#endif
		#endif
	ENDFOR

ENDPROC

#ENDIF
