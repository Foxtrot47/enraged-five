USING "SceneTool_debug.sch"

ENUM enumVEHGEN_GARAGE_SCENE_Type
	VEHGEN_GARAGE_SCENE_TYPE_enterPed = 0,
	VEHGEN_GARAGE_SCENE_TYPE_enterVeh,
	VEHGEN_GARAGE_SCENE_TYPE_exitPedOpen,
	VEHGEN_GARAGE_SCENE_TYPE_exitVeh,
	VEHGEN_GARAGE_SCENE_TYPE_MAX
ENDENUM

PROC Private_SaveVehgen_Garage_Scene(STRUCT_VEHGEN_GARAGE_SCENE& scene, VEHICLE_GEN_NAME_ENUM eName)

	TEXT_LABEL_63 str
	IF eName = VEHGEN_WEB_CAR_MICHAEL		str = "VEHGEN_WEB_CAR_MICHAEL"
	ELIF eName = VEHGEN_WEB_CAR_FRANKLIN	str = "VEHGEN_WEB_CAR_FRANKLIN"
	ELIF eName = VEHGEN_WEB_CAR_TREVOR		str = "VEHGEN_WEB_CAR_TREVOR"
	ELSE
		str = ENUM_TO_INT(eName)
		str += "	//"
		str += ENUM_TO_INT(eName)
	ENDIF
	
	OPEN_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(SceneToolPrv_MakeIndent(1))SAVE_STRING_TO_DEBUG_FILE("//VEHGEN_GARAGE_SCENE_TYPE_exitPedOpen")SAVE_NEWLINE_TO_DEBUG_FILE()
	SceneTool_ExportPlacer("VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen", scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen], 1)
	SAVE_STRING_TO_DEBUG_FILE(SceneToolPrv_MakeIndent(1))SAVE_NEWLINE_TO_DEBUG_FILE()
	SceneTool_ExportPan("VEHGEN_GARAGE_SCENE_PAN_exitPedOpen", scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen], 1)
	SAVE_STRING_TO_DEBUG_FILE(SceneToolPrv_MakeIndent(1))SAVE_STRING_TO_DEBUG_FILE("scene.fHold[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen] = ")SAVE_FLOAT_TO_DEBUG_FILE(			scene.fHold[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen])SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE(SceneToolPrv_MakeIndent(1))SAVE_NEWLINE_TO_DEBUG_FILE()
	CLOSE_DEBUG_FILE()
	
	SceneTool_OpenDebugFile("Private_GET_GARAGE_VEHICLE_WARP_DATA()", str)
		SAVE_STRING_TO_DEBUG_FILE(SceneToolPrv_MakeIndent(3))SAVE_STRING_TO_DEBUG_FILE("//VEHGEN_GARAGE_SCENE_TYPE_enterPed")SAVE_NEWLINE_TO_DEBUG_FILE()
		SceneTool_ExportPan("VEHGEN_GARAGE_SCENE_PAN_enterPed", scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed])
		SAVE_STRING_TO_DEBUG_FILE(SceneToolPrv_MakeIndent(3))SAVE_STRING_TO_DEBUG_FILE("scene.fHold[VEHGEN_GARAGE_SCENE_PAN_enterPed] = ")SAVE_FLOAT_TO_DEBUG_FILE(			scene.fHold[VEHGEN_GARAGE_SCENE_PAN_enterPed])SAVE_NEWLINE_TO_DEBUG_FILE()
		SceneTool_ExportPlacer("VEHGEN_GARAGE_SCENE_PLACER_enterPed", scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed])
		SAVE_STRING_TO_DEBUG_FILE(SceneToolPrv_MakeIndent(3))SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE(SceneToolPrv_MakeIndent(3))SAVE_STRING_TO_DEBUG_FILE("//VEHGEN_GARAGE_SCENE_TYPE_enterVeh")SAVE_NEWLINE_TO_DEBUG_FILE()
		SceneTool_ExportPan("VEHGEN_GARAGE_SCENE_PAN_enterVeh", scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh])
		SAVE_STRING_TO_DEBUG_FILE(SceneToolPrv_MakeIndent(3))SAVE_STRING_TO_DEBUG_FILE("scene.fHold[VEHGEN_GARAGE_SCENE_PAN_enterVeh] = ")SAVE_FLOAT_TO_DEBUG_FILE(			scene.fHold[VEHGEN_GARAGE_SCENE_PAN_enterVeh])SAVE_NEWLINE_TO_DEBUG_FILE()
		SceneTool_ExportMarker("VEHGEN_GARAGE_SCENE_MARKER_enterVeh", scene.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_enterVeh])
		SceneTool_ExportPoint("VEHGEN_GARAGE_SCENE_POINT_enterVeh", scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh])
		SAVE_STRING_TO_DEBUG_FILE(SceneToolPrv_MakeIndent(3))SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_STRING_TO_DEBUG_FILE(SceneToolPrv_MakeIndent(3))SAVE_STRING_TO_DEBUG_FILE("//VEHGEN_GARAGE_SCENE_TYPE_exitVeh")SAVE_NEWLINE_TO_DEBUG_FILE()
		SceneTool_ExportPan("VEHGEN_GARAGE_SCENE_PAN_exitVeh", scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh])
		SAVE_STRING_TO_DEBUG_FILE(SceneToolPrv_MakeIndent(3))SAVE_STRING_TO_DEBUG_FILE("scene.fHold[VEHGEN_GARAGE_SCENE_PAN_exitVeh] = ")SAVE_FLOAT_TO_DEBUG_FILE(			scene.fHold[VEHGEN_GARAGE_SCENE_PAN_exitVeh])SAVE_NEWLINE_TO_DEBUG_FILE()
		SceneTool_ExportPoint("VEHGEN_GARAGE_SCENE_POINT_exitVeh", scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh])
		SceneTool_ExportMarker("VEHGEN_GARAGE_SCENE_MARKER_exitVeh", scene.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_exitVeh])
		SceneTool_ExportPlacer("VEHGEN_GARAGE_SCENE_PLACER_exitVeh", scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitVeh])
		SAVE_STRING_TO_DEBUG_FILE(SceneToolPrv_MakeIndent(3))SAVE_NEWLINE_TO_DEBUG_FILE()
		
//		SAVE_STRING_TO_DEBUG_FILE(SceneToolPrv_MakeIndent(3))SAVE_STRING_TO_DEBUG_FILE("//VEHGEN_GARAGE_SCENE_garageDoor")SAVE_NEWLINE_TO_DEBUG_FILE()
//		SceneTool_ExportPoint("VEHGEN_GARAGE_SCENE_POINT_garageDoor", scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor])
//		SAVE_STRING_TO_DEBUG_FILE(SceneToolPrv_MakeIndent(3))SAVE_NEWLINE_TO_DEBUG_FILE()
	SceneTool_CloseDebugFile("Private_GET_GARAGE_VEHICLE_WARP_DATA()")
	
ENDPROC



TYPEDEF FUNC BOOL SceneGarageFunc(STRUCT_VEHGEN_GARAGE_SCENE &scene, enumVEHGEN_GARAGE_SCENE_Type eType)

PROC Private_DebugPlayVehgen_Garage_Scene(STRUCT_VEHGEN_GARAGE_SCENE& scene, SceneGarageFunc customVehgenGarageScene, enumVEHGEN_GARAGE_SCENE_Type eType)
	
	WHILE NOT CALL customVehgenGarageScene(scene, eType)
		WAIT(0)
	ENDWHILE

ENDPROC

PROC Private_EDIT_Vehgen_Garage_Scene(structSceneTool_Launcher& launcher, VEHICLE_GEN_NAME_ENUM eName, SceneGarageFunc customVehgenGarageScene, INT &iVehgen_garage_scene_type, BOOL &bSnapMarkerToPoint)	//, OBJECT_INDEX &objGarageDoor)

	// Get scene data
	STRUCT_VEHGEN_GARAGE_SCENE scene
	//structSceneTool tool
	
	// bOutputMissingNetRealtyScenes
	
	Private_GET_GARAGE_VEHICLE_WARP_DATA(eName, scene)
	
	INT iLaunchSceneID = launcher.iLaunchSceneID
	
	// Add widgets
	TEXT_LABEL_63 str
	IF eName = VEHGEN_WEB_CAR_MICHAEL		str = "VEHGEN_WEB_CAR_MICHAEL"
	ELIF eName = VEHGEN_WEB_CAR_FRANKLIN	str = "VEHGEN_WEB_CAR_FRANKLIN"
	ELIF eName = VEHGEN_WEB_CAR_TREVOR		str = "VEHGEN_WEB_CAR_TREVOR"
	ELSE
		str = ENUM_TO_INT(eName)
		str += "	//"
		str += ENUM_TO_INT(eName)
	ENDIF
	START_SCENETOOL_GROUP(launcher, g_sAmMpPropertyExtSceneTool, str)
		
		START_NEW_WIDGET_COMBO()
			ADD_TO_WIDGET_COMBO("TYPE_enterPed")
			ADD_TO_WIDGET_COMBO("TYPE_enterVeh")
			ADD_TO_WIDGET_COMBO("TYPE_exitPedOpen")
			ADD_TO_WIDGET_COMBO("TYPE_exitVeh")
		STOP_WIDGET_COMBO("Scene type to edit", iVehgen_garage_scene_type)
		
		START_WIDGET_GROUP("VEHGEN_GARAGE_SCENE_TYPE_enterPed")
			ADD_WIDGET_STRING("Pans")
			ADD_WIDGET_PAN("PAN_enterPed", g_sAmMpPropertyExtSceneTool,scene.mPans, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_PAN_enterPed))
			ADD_WIDGET_FLOAT_SLIDER("PAN_HOLD", scene.fHold[VEHGEN_GARAGE_SCENE_PAN_enterPed], 0.0, 5.0, 0.1)
			
			ADD_WIDGET_STRING("Placer")
			ADD_WIDGET_PLACER("PLACER_enterPed", g_sAmMpPropertyExtSceneTool,scene.mPlacers, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_PLACER_enterPed), 5.0)

			ADD_WIDGET_STRING("Marker")
			
			ADD_WIDGET_STRING("Point")
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("VEHGEN_GARAGE_SCENE_TYPE_enterVeh")
			ADD_WIDGET_STRING("Pans")
			ADD_WIDGET_PAN("PAN_enterVeh", g_sAmMpPropertyExtSceneTool,scene.mPans, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_PAN_enterVeh))
			ADD_WIDGET_FLOAT_SLIDER("PAN_HOLD", scene.fHold[VEHGEN_GARAGE_SCENE_PAN_enterVeh], 0.0, 5.0, 0.1)
			
			ADD_WIDGET_STRING("Placer")
			
			ADD_WIDGET_STRING("Marker")
			ADD_WIDGET_MARKER("MARKER_enterVeh", g_sAmMpPropertyExtSceneTool,scene.mMarkers, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_MARKER_enterVeh))
			ADD_WIDGET_BOOL("Snap Marker To Point", bSnapMarkerToPoint)
			
			ADD_WIDGET_STRING("Point")
			ADD_WIDGET_POINT("POINT_enterVeh", g_sAmMpPropertyExtSceneTool,scene.mPoints, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_POINT_enterVeh), 5.0)
//			ADD_WIDGET_POINT("POINT_garageDoor", g_sAmMpPropertyExtSceneTool,scene.mPoints, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_POINT_garageDoor), 5.0)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("VEHGEN_GARAGE_SCENE_TYPE_exitPedOpen")
			ADD_WIDGET_STRING("Pans")
			ADD_WIDGET_PAN("PAN_exitPedOpen", g_sAmMpPropertyExtSceneTool,scene.mPans, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_PAN_exitPedOpen))
			ADD_WIDGET_FLOAT_SLIDER("PAN_HOLD", scene.fHold[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen], 0.0, 5.0, 0.1)
			
			ADD_WIDGET_STRING("Placer")
			ADD_WIDGET_PLACER("PLACER_exitPedOpen", g_sAmMpPropertyExtSceneTool,scene.mPlacers, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen), 5.0)
			
			ADD_WIDGET_STRING("Marker")
			
			ADD_WIDGET_STRING("Point")
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("VEHGEN_GARAGE_SCENE_TYPE_exitVeh")
			ADD_WIDGET_STRING("Pans")
			ADD_WIDGET_PAN("PAN_exitVeh", g_sAmMpPropertyExtSceneTool,scene.mPans, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_PAN_exitVeh))
			ADD_WIDGET_FLOAT_SLIDER("PAN_HOLD", scene.fHold[VEHGEN_GARAGE_SCENE_PAN_exitVeh], 0.0, 5.0, 0.1)
			
			ADD_WIDGET_STRING("Marker")
			ADD_WIDGET_MARKER("MARKER_exitVeh", g_sAmMpPropertyExtSceneTool,scene.mMarkers, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_MARKER_exitVeh))
			ADD_WIDGET_BOOL("Snap Marker To Point", bSnapMarkerToPoint)
			
			ADD_WIDGET_STRING("Point")
			ADD_WIDGET_POINT("POINT_exitVeh", g_sAmMpPropertyExtSceneTool,scene.mPoints, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_POINT_exitVeh), 5.0)
//			ADD_WIDGET_POINT("POINT_garageDoor", g_sAmMpPropertyExtSceneTool,scene.mPoints, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_POINT_garageDoor), 5.0)
			
			ADD_WIDGET_STRING("Placer")
			ADD_WIDGET_PLACER("PLACER_exitVeh", g_sAmMpPropertyExtSceneTool,scene.mPlacers, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_PLACER_exitVeh), 5.0)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("*sliders*")
			START_WIDGET_GROUP("Placer")
				START_WIDGET_GROUP("PLACER_enterPed")
					ADD_WIDGET_VECTOR_SLIDER("PLACER_enterPed", scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].vPos, -6000, 6000, 0.01)
					ADD_WIDGET_FLOAT_READ_ONLY("*heading*", scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].fRot)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("PLACER_exitPedOpen")
					ADD_WIDGET_VECTOR_SLIDER("PLACER_exitPedOpen", scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen].vPos, -6000, 6000, 0.01)
					ADD_WIDGET_FLOAT_READ_ONLY("*heading*", scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen].fRot)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("PLACER_exitVeh")
					ADD_WIDGET_VECTOR_SLIDER("PLACER_exitVeh", scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitVeh].vPos, -6000, 6000, 0.01)
					ADD_WIDGET_FLOAT_READ_ONLY("*heading*", scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitVeh].fRot)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Marker")
				START_WIDGET_GROUP("MARKER_enterVeh")
					ADD_WIDGET_VECTOR_SLIDER("MARKER_enterVeh", scene.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_enterVeh].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("MARKER_exitVeh")
					ADD_WIDGET_VECTOR_SLIDER("MARKER_exitVeh", scene.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_exitVeh].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Point")
				START_WIDGET_GROUP("POINT_enterVeh")
					ADD_WIDGET_VECTOR_SLIDER("POINT_enterVeh", scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vPos, -6000, 6000, 0.01)
					ADD_WIDGET_FLOAT_READ_ONLY("*rot x*", scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vRot.x)
					ADD_WIDGET_FLOAT_READ_ONLY("*rot y*", scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vRot.y)
					ADD_WIDGET_FLOAT_READ_ONLY("*rot z*", scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vRot.z)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("POINT_exitVeh")
					ADD_WIDGET_VECTOR_SLIDER("POINT_exitVeh", scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vPos, -6000, 6000, 0.01)
					ADD_WIDGET_FLOAT_READ_ONLY("*rot x*", scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vRot.x)
					ADD_WIDGET_FLOAT_READ_ONLY("*rot y*", scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vRot.y)
					ADD_WIDGET_FLOAT_READ_ONLY("*rot z*", scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vRot.z)
				STOP_WIDGET_GROUP()
//				START_WIDGET_GROUP("POINT_garageDoor")
//					ADD_WIDGET_VECTOR_SLIDER("POINT_garageDoor", scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vPos, -6000, 6000, 0.01)
//					ADD_WIDGET_FLOAT_READ_ONLY("*rot x*", scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vRot.x)
//					ADD_WIDGET_FLOAT_READ_ONLY("*rot y*", scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vRot.y)
//					ADD_WIDGET_FLOAT_READ_ONLY("*rot z*", scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vRot.z)
//				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
	STOP_SCENETOOL_GROUP(launcher, g_sAmMpPropertyExtSceneTool)
	
	// Warp to player pos marker
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(vehIndex)
				SET_ENTITY_COORDS(vehIndex, scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vPos)
				SET_ENTITY_HEADING(vehIndex, scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vRot.z)
				iVehgen_garage_scene_type = ENUM_TO_INT(VEHGEN_GARAGE_SCENE_TYPE_enterVeh)
			ENDIF
		ELSE
			SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].vPos)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].fRot)
			iVehgen_garage_scene_type = ENUM_TO_INT(VEHGEN_GARAGE_SCENE_TYPE_enterPed)
		ENDIF
	ENDIF
	
	// Update widgets
	WHILE launcher.bEnableTool
	
		// Update system
		SWITCH SceneTool_UpdateTool(g_sAmMpPropertyExtSceneTool)
			
			CASE SCENETOOL_ACTION_RunScene
				Private_DebugPlayVehgen_Garage_Scene(scene, customVehgenGarageScene, INT_TO_ENUM(enumVEHGEN_GARAGE_SCENE_Type, iVehgen_garage_scene_type))
				bSnapMarkerToPoint = FALSE
			BREAK
			
			CASE SCENETOOL_ACTION_SaveScene
				Private_SaveVehgen_Garage_Scene(scene, eName)
				bSnapMarkerToPoint = FALSE
			BREAK
			
			CASE SCENETOOL_ACTION_UpdateData
				SceneTool_UpdatePans(g_sAmMpPropertyExtSceneTool, scene.mPans)
//				SceneTool_UpdateCuts(g_sAmMpPropertyExtSceneTool, scene.mCuts)
				SceneTool_UpdateMarkers(g_sAmMpPropertyExtSceneTool, scene.mMarkers)
				SceneTool_UpdatePlacers(g_sAmMpPropertyExtSceneTool, scene.mPlacers)
				SceneTool_UpdatePoints(g_sAmMpPropertyExtSceneTool, scene.mPoints)
				
				IF bSnapMarkerToPoint
					bSnapMarkerToPoint = FALSE
					enumVEHGEN_GARAGE_SCENE_Markers eMarker
					REPEAT VEHGEN_GARAGE_SCENE_MARKER_MAX eMarker
						IF g_sAmMpPropertyExtSceneTool.mMarkerWidgets[eMarker].bGrabPos
							
							enumVEHGEN_GARAGE_SCENE_Points ePoint
							ePoint = VEHGEN_GARAGE_SCENE_POINT_MAX
							IF eMarker = VEHGEN_GARAGE_SCENE_MARKER_enterVeh
								ePoint = VEHGEN_GARAGE_SCENE_POINT_enterVeh
							ELIF eMarker = VEHGEN_GARAGE_SCENE_MARKER_exitVeh
								ePoint = VEHGEN_GARAGE_SCENE_POINT_exitVeh
							ELSE
								bSnapMarkerToPoint = FALSE
								BREAK
							ENDIF
							
							VECTOR vOldMarkerPos
							vOldMarkerPos = scene.mMarkers[eMarker].vPos
							scene.mMarkers[eMarker].vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS( scene.mPoints[ePoint].vPos, scene.mPoints[ePoint].vRot.z, <<0,7.5,0>>)
							bSnapMarkerToPoint = TRUE
							
							IF VDIST2(vOldMarkerPos, scene.mMarkers[eMarker].vPos) > (1.0*1.0)
								g_sAmMpPropertyExtSceneTool.bSaveOnExit = TRUE
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			BREAK
		
		ENDSWITCH
		
		SWITCH INT_TO_ENUM(enumVEHGEN_GARAGE_SCENE_Type, iVehgen_garage_scene_type)
			CASE VEHGEN_GARAGE_SCENE_TYPE_enterPed
				// Draw markers
			//	SceneTool_DrawMarker(g_sAmMpPropertyExtSceneTool, scene.mMarkers, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_MARKER_enterPed),	0, 255, 175,	"MARKER_enterPed")
				
				// Draw placers
				SceneTool_DrawPlacer(g_sAmMpPropertyExtSceneTool, scene.mPlacers, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_PLACER_enterPed),	255, 0, 75,		"PLACER_enterPed")
				
				// Draw points
			//	SceneTool_DrawPoint(g_sAmMpPropertyExtSceneTool, scene.mPoints, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_POINT_garageDoor),		128, 75, 75,	"POINT_garageDoor")
//				IF DOES_ENTITY_EXIST(objGarageDoor)
//					DELETE_OBJECT(objGarageDoor)
//				ENDIF
				
				// Draw pans
				SceneTool_DrawPan(g_sAmMpPropertyExtSceneTool, scene.mPans, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_PAN_enterPed),			255, 0, 75,		"PAN_enterPed")
			BREAK
			CASE VEHGEN_GARAGE_SCENE_TYPE_enterVeh
				// Draw markers
				SceneTool_DrawMarker(g_sAmMpPropertyExtSceneTool, scene.mMarkers, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_MARKER_enterVeh),	0, 255, 175,	"MARKER_enterVeh")
				
				// Draw placers
				SceneTool_DrawPoint(g_sAmMpPropertyExtSceneTool, scene.mPoints, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_POINT_enterVeh),	255, 0, 75,		"POINT_enterVeh")
				
				// Draw points
//				SceneTool_DrawPoint(g_sAmMpPropertyExtSceneTool, scene.mPoints, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_POINT_garageDoor),		128, 75, 75,	"POINT_garageDoor")
//				IF NOT DOES_ENTITY_EXIST(objGarageDoor)
//					MODEL_NAMES fakeGarageDoorModel
//					fakeGarageDoorModel = V_ILEV_CSR_GARAGEDOOR
//					REQUEST_MODEL(fakeGarageDoorModel)
//					WHILE NOT HAS_MODEL_LOADED(fakeGarageDoorModel)
//						WAIT(0)
//					ENDWHILE
//					objGarageDoor = CREATE_OBJECT_NO_OFFSET(fakeGarageDoorModel, scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vPos)
//					SET_ENTITY_ROTATION(objGarageDoor, scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vRot)
//					SET_ENTITY_ALPHA(objGarageDoor, 50, TRUE)
//				ELSE
//					SET_ENTITY_COORDS(objGarageDoor, scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vPos)
//					SET_ENTITY_ROTATION(objGarageDoor, scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vRot)
//				ENDIF
				
				// Draw pans
				SceneTool_DrawPan(g_sAmMpPropertyExtSceneTool, scene.mPans, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_PAN_enterVeh),			255, 0, 75,		"PAN_enterVeh")
			BREAK
			CASE VEHGEN_GARAGE_SCENE_TYPE_exitPedOpen
				// Draw markers
			//	SceneTool_DrawMarker(g_sAmMpPropertyExtSceneTool, scene.mMarkers, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_MARKER_exitPedOpen),	0, 255, 175,	"MARKER_exitPedOpen")
				
				// Draw placers
				SceneTool_DrawPlacer(g_sAmMpPropertyExtSceneTool, scene.mPlacers, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen),	255, 0, 75,		"PLACER_exitPedOpen")
				
				// Draw points
			//	SceneTool_DrawPoint(g_sAmMpPropertyExtSceneTool, scene.mPoints, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_POINT_garageDoor),		128, 75, 75,	"POINT_garageDoor")
//				IF DOES_ENTITY_EXIST(objGarageDoor)
//					DELETE_OBJECT(objGarageDoor)
//				ENDIF
				
				// Draw pans
				SceneTool_DrawPan(g_sAmMpPropertyExtSceneTool, scene.mPans, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_PAN_exitPedOpen),			255, 0, 75,		"PAN_exitPedOpen")
			BREAK
//			CASE VEHGEN_GARAGE_SCENE_TYPE_exitPedClose
//				// Draw markers
//				SceneTool_DrawMarker(g_sAmMpPropertyExtSceneTool, scene.mMarkers, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_MARKER_exitPedClose),	0, 255, 175,	"MARKER_exitPedClose")
//				
//				// Draw placers
//				SceneTool_DrawPlacer(g_sAmMpPropertyExtSceneTool, scene.mPlacers, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_PLACER_exitPedClose),	255, 0, 75,		"PLACER_exitPedClose")
//				
//				// Draw points
//			//	SceneTool_DrawPoint(g_sAmMpPropertyExtSceneTool, scene.mPoints, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_POINT_garageDoor),		128, 75, 75,	"POINT_garageDoor")
//				IF DOES_ENTITY_EXIST(objGarageDoor)
//					DELETE_OBJECT(objGarageDoor)
//				ENDIF
//				
//				// Draw pans
//				SceneTool_DrawPan(g_sAmMpPropertyExtSceneTool, scene.mPans, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_PAN_exitPedClose),			255, 0, 75,		"PAN_exitPedClose")
//			BREAK
			CASE VEHGEN_GARAGE_SCENE_TYPE_exitVeh
				// Draw markers
				SceneTool_DrawMarker(g_sAmMpPropertyExtSceneTool, scene.mMarkers, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_MARKER_exitVeh),	0, 255, 175,	"MARKER_exitVeh")
				
				// Draw placers
				SceneTool_DrawPlacer(g_sAmMpPropertyExtSceneTool, scene.mPlacers, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_PLACER_exitVeh),	255, 0, 75,		"PLACER_exitVeh")
				
				// Draw points
				SceneTool_DrawPoint(g_sAmMpPropertyExtSceneTool, scene.mPoints, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_POINT_exitVeh),		255, 0, 75,		"POINT_exitVeh")
//				SceneTool_DrawPoint(g_sAmMpPropertyExtSceneTool, scene.mPoints, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_POINT_garageDoor),		128, 75, 75,	"POINT_garageDoor")
//				IF NOT DOES_ENTITY_EXIST(objGarageDoor)
//					MODEL_NAMES fakeGarageDoorModel
//					fakeGarageDoorModel = V_ILEV_CSR_GARAGEDOOR
//					REQUEST_MODEL(fakeGarageDoorModel)
//					WHILE NOT HAS_MODEL_LOADED(fakeGarageDoorModel)
//						WAIT(0)
//					ENDWHILE
//					objGarageDoor = CREATE_OBJECT_NO_OFFSET(fakeGarageDoorModel, scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vPos)
//					SET_ENTITY_ROTATION(objGarageDoor, scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vRot)
//					SET_ENTITY_ALPHA(objGarageDoor, 50, TRUE)
//				ELSE
//					SET_ENTITY_COORDS(objGarageDoor, scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vPos)
//					SET_ENTITY_ROTATION(objGarageDoor, scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vRot)
//				ENDIF
				
				// Draw pans
				SceneTool_DrawPan(g_sAmMpPropertyExtSceneTool, scene.mPans, ENUM_TO_INT(VEHGEN_GARAGE_SCENE_PAN_exitVeh),			255, 0, 75,		"PAN_exitVeh")
			BREAK
		ENDSWITCH
		launcher.iLaunchSceneID = iLaunchSceneID
		
		
		WAIT(0)
	ENDWHILE
	
//	IF DOES_ENTITY_EXIST(objGarageDoor)
//		DELETE_OBJECT(objGarageDoor)
//	ENDIF
	
	// Cleanup system
	SWITCH SceneTool_DestroyTool(g_sAmMpPropertyExtSceneTool)
		CASE SCENETOOL_ACTION_SaveScene
			Private_SaveVehgen_Garage_Scene(scene, eName)
		BREAK
	ENDSWITCH
	
ENDPROC

