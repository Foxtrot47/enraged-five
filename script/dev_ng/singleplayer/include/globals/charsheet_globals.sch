// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      SCRIPT NAME     :   charsheet_globals.sch
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Contains global variable instances for filling out character sheet.
//                          This is filled on start up by charsheet_initial.sc
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "charsheet_global_definitions.sch"


structCharacterSheetNonSaved g_CharacterSheetNonSaved[MAX_CHARACTERS_PLUS_DUMMY]
structCharacterSheet g_sCharacterSheetAll[MAX_CHARACTERS_PLUS_DUMMY] 


//The following are used to specify which characters from the charsheet should appear on the phone during a call or conference call. Moved here to refer more easily to enumCharacterList.

enumCharacterList g_TheContactInvolvedinCall  //this tells the phone system which character name to display on the calling screen. The enums are defined in charsheet_globals.sch.
                                              //if a call is successfully placed, this takes on the value of the holder global below. The holder is required to prevent any ongoing
                                              //call contact label being overwritten.

enumCharacterList g_TheContactInvolvedinCallHolder //used to store whichever contact is actively trying to call the player in the interim.



enumCharacterList g_AdditionalContactInvolvedinCall  //This is the name of any second caller involved in a conference call.
                                            
enumCharacterList g_AdditionalContactInvolvedinCallHolder //used to store whichever second contact is involved in the conference call in the interim.




//_______________________________________________________________________________________________________________________________________________________________________________________
//
//                                              Gameworld Phone Numbers - Numbers that will play an answerphone message when dialled from the cellphone.
//_______________________________________________________________________________________________________________________________________________________________________________________

struct_GameworldNumbers g_GameworldNumber[MAX_GAMEWORLD_NUMBERS]




//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                    Special character and secondary functions for SP / MP Contacts.
//
//________________________________________________________________________________________________________________________________________________________________________________


//These are filled in Fill_Special_SP_Characters in charsheet_public.sch

structSpecialSPCharacter g_SpecialSPCharacters[MAX_SPECIAL_SP_CHARACTERS]



//These are filled in Fill_Special_MP_Characters in charsheet_public.sch

structSpecialMPCharacter g_SpecialMPCharacters[MAX_SPECIAL_MP_CHARACTERS]

