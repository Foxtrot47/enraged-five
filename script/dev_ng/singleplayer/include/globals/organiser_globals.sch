

ENUM ORGANISER_EVENT_ENUM
	ORG_EVT_NOTHING,
	ORG_EVT_HOUSE_VIEWING
ENDENUM



//BOOL g_bDebugOrganiserDisplay

BOOL g_bFirstOrganiserUpdateDone = FALSE

//used to detect changes to the organiser when the UI is updated
INT g_iOrganiserChangeValue = 0
INT g_iOrganiserChangeValueLastUpdate = 0

INT g_iOrganiserEventsCurrentlyLogged = 0

STRUCT ORGANISER_EVENT_STRUCT
	ORGANISER_EVENT_ENUM type
ENDSTRUCT

//rollover ints - all times past the current until the current is reached (including looping) are considered in the future
INT g_iFocusHour
INT g_iFocusDay
ORGANISER_EVENT_STRUCT g_OrganiserUpcomingWeek[7][24]


//store the current date and time so timeskips can be handled and resolved on update

INT g_iOrgCurrentHour
INT g_iOrgCurrentDay
INT g_iOrgCurrentMonth

























