// Assassination_globals.sch
USING "script_maths.sch"
USING "clock_globals.sch"

// These are to be bits. Not the bit index!
ENUM ASSASSIN_SAVED_BOOLS
	ASS_BOOL_FIRSTTIMEPRINT = BIT0,
	ASS_BOOL_FIRSTTIMEGETINFO = BIT1,
	ASS_BOOL_TODPRINTHELP = BIT2,
	ASS_BOOL_MISSION_CAND_RUNNING = BIT3,
	
	ASS_BOOL_DEBUG_LAUNCH = BIT29,
	ASS_BOOL_FORCE_PKG = BIT30
ENDENUM

STRUCT AssassinDataSaved
	INT 		iCurrentAssassinRank
	INT 		iGenericData
	INT			iBools
	INT			iAttemptsOnCur = 0
	TIMEOFDAY 	TODForNextMission
	FLOAT		fHotelMissionTime
	FLOAT		fMultiMissionTime
	FLOAT		fViceMissionTime
	FLOAT		fBusMissionTime
	FLOAT		fConstructionMissionTime
ENDSTRUCT
