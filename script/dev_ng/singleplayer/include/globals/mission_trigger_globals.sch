USING "candidate_enums.sch"
USING "blip_enums.sch"
USING "trigger_scene_globals.sch"
USING "charsheet_global_definitions.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	mission_trigger_globals.sch
//		AUTHOR			:	Ak, BenR
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

CONST_INT MAX_MISSION_TRIGGERS 8

ENUM MISSION_TRIGGER_TYPE
	MISSION_TRIGGER_TYPE_SCENE,	// Stores a trigger scene to operate on.
	MISSION_TRIGGER_TYPE_SWITCH,// Stores a character that will trip the trigger when switching to.
	MISSION_TRIGGER_TYPE_CALL,	// Stores an off-mission communication to monitor. UNIMPLEMENTED.
	
	MISSION_TRIGGER_TYPE_NULL = -1
ENDENUM

STRUCT MISSION_TRIGGER_STRUCT
		BOOL bUsed			// For reusing of indices.
		BOOL bLaunchMe 		// If true, then mission launch has been negotiated for this trigger.
		BOOL bBlocked		// The trigger scene has flagged that this trigger is currently in a blocked state.
		
		MISSION_TRIGGER_TYPE eType
		m_enumMissionCandidateTypeID eCandidateType	// The type of candidate request this trigger should make.
		SP_MISSIONS eMissionID 						// The SP mission this trigger will start.
		STRANDS eStrand 							// The strand the mission to trigger is in.
		
		INT iCandidateIndex 			// Used to store the candidate ID reserved for the trigger.
		INT iTriggerableCharBitset		// Bitset flagging which players can activate this trigger.
		STATIC_BLIP_NAME_ENUM eBlip		// Used by category MISSION_TRIGGER_TYPE_BLIP
		TRIGGER_SCENE sScene			// Used by category MISSION_TRIGGER_TYPE_SCENE
ENDSTRUCT

TYPEDEF PROC BindTriggerScene(SP_MISSIONS paramMissionID, TRIGGER_SCENE &paramTrigScene, TRIGGER_SCENE_POINTERS &paramScenePointers)
	
STRUCT MISSION_TRIGGERER_LOCAL_VARS
	BindTriggerScene fpBindTriggerScene
	TRIGGER_SCENE_POINTERS sScenePointers[MAX_MISSION_TRIGGERS]
	
	BOOL bCallbackSetup = FALSE
	BOOL bReleaseFunctionBound = FALSE
	ReleaseTriggerSceneAssets fpReleaseAssetsForLoadedScene
	
	INT iSwitchTriggerCooldown = 0
	INT iNoFramesMissionRunning = 0
	INT iLockInTimer = 0
	BOOL bSkipNextSwitchCheck = FALSE
	BOOL bDisplayedTriggerRejectHelp = FALSE
	
	BLIP_INDEX blipTODTrigger
	structTimelapse sTimeOfDay
ENDSTRUCT

SP_MISSIONS 				g_eMissionTriggerProcessing		= SP_MISSION_NONE
SP_MISSIONS 				g_eMissionSceneToCleanup 		= SP_MISSION_NONE
SP_MISSIONS 				g_eMissionSceneToPreLoad		= SP_MISSION_NONE
SP_MISSIONS 				g_eMissionForceTrigger			= SP_MISSION_NONE
SP_MISSIONS 				g_eMissionTriggerActive			= SP_MISSION_NONE
BOOL 						g_bCleanupTriggerScene 			= FALSE
BOOL						g_bMissionTriggerLoading		= FALSE	//Allows other systems to query if a trigger is attempting to stream in.
BOOL						g_bTriggerSceneActive			= FALSE //Allows other systems to query if a trigger scene is active.
BOOL						g_bTriggerSceneBlockHelp		= FALSE //Allows other systems to query if the player is close to trigger blip (10m)

BOOL g_bMissionTriggerFired
INT g_iMissionTriggerableToTrigger = -1

INT g_iRegisteredMissionTriggers
MISSION_TRIGGER_STRUCT g_TriggerableMissions[MAX_MISSION_TRIGGERS]


#IF IS_DEBUG_BUILD
	ENUM TriggerDebugMode
		TDM_OFF = 0,
		TDM_TRIGGER,
		TDM_TRIGGER_TEXT,
		TDM_LOAD,
		TDM_LOAD_TEXT,
		TDM_FRIEND_REJECT,
		TDM_FRIEND_REJECT_TEXT,
		TDM_BBUDDY_CALL,
		TDM_BBUDDY_CALL_TEXT,
		TDM_AUTODOOR,
		TDM_AUTODOOR_TEXT,
		TDM_ALL,
		TDM_ALL_TEXT
	ENDENUM
	TriggerDebugMode g_eTriggerDebugMode = TDM_OFF
	BOOL g_bTriggerDebugOn = FALSE
#ENDIF




//////////////////////////////////
/// Mission analytic nanny globals

TEXT_LABEL_63 g_sNanMissionString = 0








