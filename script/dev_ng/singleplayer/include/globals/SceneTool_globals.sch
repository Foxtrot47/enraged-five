//---------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------
//-- SceneTool_glonals
//					
//   sam.hackett@rockstarleeds.com
//---------------------------------------------------------------------------------------------------

STRUCT structSceneTool_Cam
	VECTOR					vPos
	VECTOR					vRot
ENDSTRUCT

STRUCT structSceneTool_Pan
	structSceneTool_Cam		mStart
	structSceneTool_Cam		mEnd
	FLOAT					fFov
	FLOAT					fShake
	FLOAT					fDuration
ENDSTRUCT

STRUCT structSceneTool_Cut
	structSceneTool_Cam		mCam
	FLOAT					fFov
	FLOAT					fShake
ENDSTRUCT

STRUCT structSceneTool_Marker
	VECTOR					vPos
ENDSTRUCT

STRUCT structSceneTool_Placer
	VECTOR					vPos
	FLOAT					fRot
ENDSTRUCT

STRUCT structSceneTool_AngArea
	VECTOR					vPos0
	VECTOR					vPos1
	FLOAT					fWidth
	FLOAT					fHeight
ENDSTRUCT

//STRUCT structSceneTool_BoxArea
//	VECTOR					vPos0
//	VECTOR					vPos1
//	FLOAT					fHeight
//ENDSTRUCT
//
//STRUCT structSceneTool_SphArea
//	VECTOR					vPos
//	FLOAT					fRadius
//ENDSTRUCT

STRUCT structSceneTool_Point
	VECTOR					vPos
	VECTOR					vRot
ENDSTRUCT




