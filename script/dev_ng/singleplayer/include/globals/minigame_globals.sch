//╒═════════════════════════════════════════════════════════════════════════════╕
//│					 Communication Controller Global Header						│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│		AUTHOR:			Ben Rollinson											│
//│		DATE:			06/07/13												│
//│		DESCRIPTION: 	All global definitions required by all minigames		│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"

//World point activation ranges.
CONST_INT 	MG_BRAIN_ACTIVATION_RANGE_NONE		-1
CONST_INT 	MG_BRAIN_ACTIVATION_RANGE_MICRO		18
CONST_INT 	MG_BRAIN_ACTIVATION_RANGE_SMALL		20
CONST_INT 	MG_BRAIN_ACTIVATION_RANGE_MEDIUM	100
CONST_INT 	MG_BRAIN_ACTIVATION_RANGE_LARGE		209

//Using this flag to make sure we only perform the checks when necessary
BOOL g_HasPerformedMiniGameRangeChecks = FALSE

/// PURPOSE:
///		Square the const_int and convert to float for checking against FLAT_VDIST2
///    
FUNC FLOAT TO_SQUARED_FLOAT(INT thisInt)
	RETURN TO_FLOAT(thisInt*thisInt)
ENDFUNC

/// PURPOSE:
///		Flattens the coords out before getting FLAT_VDIST2
///    
FUNC FLOAT FLAT_VDIST2(VECTOR A, VECTOR B)
	CPRINTLN(DEBUG_SCRIPT_LAUNCH, "checking player distance from ", B)
	RETURN VDIST2(<<A.x, A.y, 0>>, <<B.x, B.y, 0>>)
ENDFUNC

FUNC BOOL IS_PLAYER_WITHIN_RANGE_OF_BJ_HELI_LAUNCHER(VECTOR myPlayerCoords, INT iRange)
//	"launcher_BasejumpHeli", 		MG_BRAIN_ACTIVATION_RANGE_LARGE
	FLOAT fRange = TO_SQUARED_FLOAT(iRange)
	IF FLAT_VDIST2(myPlayerCoords, <<-798.8392, -1289.817, 196.9543>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-742.5047, 4493.466, 75.2206>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<2463.904, 1509.964, 35.0372>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<1208.197, 174.5777, 81.0003>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<1054.534, -179.6562, 70.3162>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<2517.962, 4971.619, 44.7057>>) < fRange
		RETURN TRUE	
	ELIF FLAT_VDIST2(myPlayerCoords, <<-1367.687, 4381.998, 41.4017>>) < fRange
		RETURN TRUE	
	ELIF FLAT_VDIST2(myPlayerCoords, <<-274.6559, 6633.891, 7.5426>>) < fRange
		RETURN TRUE	
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_WITHIN_RANGE_OF_BJ_PACK_LAUNCHER(VECTOR myPlayerCoords, INT iRange)
//	"launcher_BasejumpPack", 		MG_BRAIN_ACTIVATION_RANGE_MEDIUM
	FLOAT fRange = TO_SQUARED_FLOAT(iRange)
	IF FLAT_VDIST2(myPlayerCoords, <<-1237.2, 4540.75, 184.75>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-92.35, -854.3, 39.571>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-801.3582, 298.8532, 84.949>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-120.92, -976.05, 295.49>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-767.209, 4331.832, 147.5061>>) < fRange
		RETURN TRUE	
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_WITHIN_RANGE_OF_GOLF_LAUNCHER(VECTOR myPlayerCoords, INT iRange)
//	"launcher_golf", 				MG_BRAIN_ACTIVATION_RANGE_LARGE
	FLOAT fRange = TO_SQUARED_FLOAT(iRange)
	IF FLAT_VDIST2(myPlayerCoords, <<-1370.625, 56.1227, 52.7033>>) < fRange
		RETURN TRUE
	ENDIF
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_PLAYER_WITHIN_RANGE_OF_HUNTING_LAUNCHER(VECTOR myPlayerCoords, INT iRange)
//	"launcher_Hunting_Ambient",	MG_BRAIN_ACTIVATION_RANGE_LARGE
	FLOAT fRange = TO_SQUARED_FLOAT(iRange)
	IF FLAT_VDIST2(myPlayerCoords, <<-1703.17, 4664.09, 22.31>>) < fRange
		RETURN TRUE
	ENDIF
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_PLAYER_WITHIN_RANGE_OF_OFFROAD_LAUNCHER(VECTOR myPlayerCoords, INT iRange)
//	"launcher_OffroadRacing", 	MG_BRAIN_ACTIVATION_RANGE_LARGE
	FLOAT fRange = TO_SQUARED_FLOAT(iRange)
	IF FLAT_VDIST2(myPlayerCoords, <<-1939.483, 4443.953, 37.3474>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-223.6753, 4224.644, 43.7304>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<2037.664, 2137.386, 92.7095>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-518.3414, 2005.449, 204.1878>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<1606.578, 3841.188, 33.2931>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<2996.776, 2774.085, 43.26>>) < fRange
		RETURN TRUE	
	ENDIF
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_PLAYER_WITHIN_RANGE_OF_RACING_LAUNCHER(VECTOR myPlayerCoords, INT iRange)
//	"launcher_Racing", 			MG_BRAIN_ACTIVATION_RANGE_LARGE
	FLOAT fRange = TO_SQUARED_FLOAT(iRange)
	IF FLAT_VDIST2(myPlayerCoords, <<-813.0546, -2546.738, 12.7888>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<369.29, 312.41, 103.24>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<791.5949, -1160.856, 27.8921>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-1062.986, -1153.365, 1.129525>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-155.8476, -1566.98, 34.00011>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<3063.114, 639.855, 0>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<3447.747, 5192.996, 0>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<152.8052, 3460.92, 28>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<621.749, -2136.798, 0>>) < fRange
		RETURN TRUE
	ENDIF
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_PLAYER_WITHIN_RANGE_OF_RAMPAGE_LAUNCHER(VECTOR myPlayerCoords, INT iRange)
//	"launcher_rampage", 			RC_BRAIN_ACTIVATION_RANGE_EXTRA
	FLOAT fRange = TO_SQUARED_FLOAT(iRange)
	IF FLAT_VDIST2(myPlayerCoords, <<908, 3643.7, 32.2>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<1181.5, -400.1, 67.5>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<465.1, -1849.3, 27.8>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-161, -1669.7, 33>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<1298.2, 2504.14, 21.09>>) < fRange
		RETURN TRUE
	ENDIF
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_PLAYER_WITHIN_RANGE_OF_PILOT_SCHOOL_LAUNCHER(VECTOR myPlayerCoords, INT iRange)
//	"launcher_pilotschool", 		MG_BRAIN_ACTIVATION_RANGE_MEDIUM
	FLOAT fRange = TO_SQUARED_FLOAT(iRange)
	IF FLAT_VDIST2(myPlayerCoords, <<-1154.11, -2715.203, 22>>) < fRange
		RETURN TRUE
	ENDIF
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_PLAYER_WITHIN_RANGE_OF_RANGE_LAUNCHER(VECTOR myPlayerCoords, INT iRange)
//	"launcher_range", 			MG_BRAIN_ACTIVATION_RANGE_SMALL
	FLOAT fRange = TO_SQUARED_FLOAT(iRange)
	IF FLAT_VDIST2(myPlayerCoords, <<5.7734, -1099.782, 29.8447>>) < fRange
		RETURN TRUE
	ENDIF
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_PLAYER_WITHIN_RANGE_OF_STUNTS_LAUNCHER(VECTOR myPlayerCoords, INT iRange)
//	"launcher_stunts", 			MG_BRAIN_ACTIVATION_RANGE_LARGE
	FLOAT fRange = TO_SQUARED_FLOAT(iRange)
	IF FLAT_VDIST2(myPlayerCoords, <<1694.74, 3276.502, 41.2796>>) < fRange
		RETURN TRUE
	ENDIF
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_PLAYER_WITHIN_RANGE_OF_TENNIS_LAUNCHER(VECTOR myPlayerCoords, INT iRange)
//	"launcher_tennis",			MG_BRAIN_ACTIVATION_RANGE_MEDIUM
	FLOAT fRange = TO_SQUARED_FLOAT(iRange)
	IF FLAT_VDIST2(myPlayerCoords, <<-1171.28, -1599.59, 3.34>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-780.4614, 156.5187, 66.4744>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<487.5186, -217.7695, 52.7864>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-1223.908, 338.3682, 78.9859>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-1233.088, 372.8105, 78.9812>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-1618.487, 266.4707, 58.5552>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-1372.016, -101.2861, 49.7046>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-2869.991, 9.229736, 10.6083>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-939.617, -1255.732, 6.9773>>) < fRange
		RETURN TRUE
	ENDIF
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_PLAYER_WITHIN_RANGE_OF_TRIATHLON_LAUNCHER(VECTOR myPlayerCoords, INT iRange)
//	"launcher_Triathlon", 		MG_BRAIN_ACTIVATION_RANGE_MEDIUM
	FLOAT fRange = TO_SQUARED_FLOAT(iRange)
	IF FLAT_VDIST2(myPlayerCoords, <<2434.415, 4284.25, 35.5059>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<-1230.622, -2049.97, 12.8882>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<1591.686, 3813.401, 33.3371>>) < fRange
		RETURN TRUE
	ENDIF
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_PLAYER_WITHIN_RANGE_OF_YOGA_LAUNCHER(VECTOR myPlayerCoords, INT iRange)
//	"launcher_Yoga", 				MG_BRAIN_ACTIVATION_RANGE_SMALL
	FLOAT fRange = TO_SQUARED_FLOAT(iRange)
	IF FLAT_VDIST2(myPlayerCoords, <<-790.9058, 186.293, 71.8351>>) < fRange
		RETURN TRUE
	ELIF FLAT_VDIST2(myPlayerCoords, <<2862.152, 5945.49, 357.1186>>) < fRange
		RETURN TRUE
	ENDIF
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_PLAYER_WITHIN_RANGE_OF_DARTS_LAUNCHER(VECTOR myPlayerCoords, INT iRange)
//	"launcher_Darts", PROP_DART_BD_CAB_01,		MG_BRAIN_ACTIVATION_RANGE_MEDIUM
	FLOAT fRange = TO_SQUARED_FLOAT(iRange)
	IF FLAT_VDIST2(myPlayerCoords, <<1992.27, 3050.60, 47.89>>) < fRange
		RETURN TRUE
	ENDIF
	RETURN FALSE	
ENDFUNC
