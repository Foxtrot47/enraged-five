// towing_globals.sch
USING "types.sch"
USING "script_maths.sch"
USING "cellphone_globals.sch"


ENUM TOWING_GLOBAL_BOOLS
	TOW_GLOBAL_LaunchedViaDebug = BIT0,
	TOW_GLOBAL_Debug_BrokenDown = BIT1,
	TOW_GLOBAL_Debug_Train 		= BIT2,
	TOW_GLOBAL_Debug_Handicap 	= BIT3,
	TOW_GLOBAL_Debug_Abandoned 	= BIT4,
	TOW_GLOBAL_Debug_Accident 	= BIT5,
	TOW_GLOBAL_Debug_Selection 	= BIT6,

	TOW_GLOBAL_MissedJob = BIT10,
	
	TOW_GLOBAL_NO_DELAY = BIT11,
	TOW_GLOBAL_10_DELAY = BIT12,
	TOW_GLOBAL_60_DELAY = BIT13,
	TOW_GLOBAL_MAX_DELAY = BIT14,
	
	TOW_GLOBAL_TONYA1 = BIT15,
	TOW_GLOBAL_TONYA2 = BIT16,
	TOW_GLOBAL_TONYA3 = BIT17,
	TOW_GLOBAL_TONYA4 = BIT18,
	TOW_GLOBAL_TONYA5 = BIT19,
	TOW_GLOBAL_PROC = BIT20,
	TOW_GLOBAL_SWITCH = BIT21,
	
	TOW_GLOBAL_USED_RESET = BIT22,
	
	TOW_DEBUG_LAUNCH	= BIT30
ENDENUM

ENUM NODE_TYPE
	NODE_TYPE_NONE = -1,
	NODE_TYPE_HANDI,
	NODE_TYPE_TRAIN,
	NODE_TYPE_ABANDON,
	NODE_TYPE_DYNAMIC,
	NODE_TYPE_ACCIDENT,
	NODE_TYPE_COUNT
ENDENUM

ENUM TOWING_MODE
	TOWING_MODE_NONE = -9999,
	
	TOWING_MODE_PATROL = 0,
	TOWING_MODE_TRADITIONAL,

	TOWING_MODES_NUM_DYN_TYPES
ENDENUM

ENUM TOWING_RC_FLAGS
	TOWING_FLAG_FAILED = 0,
	TOWING_FLAG_PASSED
ENDENUM

STRUCT TOWING_LAUNCH_DATA
	PED_INDEX		tutorialPed
	VEHICLE_INDEX	tutorialTruck
	TOWING_MODE 	launchMode
	NODE_TYPE 		nodeType
	BOOL 			bDebug
	BOOL 			bSpecifyNode
	BOOL			bInit 
	BOOL			bRcVersion
	INT				iTowFlags
ENDSTRUCT


// If you add to this, also make the mirroring changes to 
// X:\gta5\script\dev\shared\include\globals\registration\towing_globals_reg.sch
STRUCT TowingDataSaved
	INT 		iCurTowingRank
	INT			iBools
	NODE_TYPE 	lastNodeType
	INT			iTowingJobsCompleted
	//structPedsForConversation conversation
	INT			iAbandonCompleted
	INT			iTrainCompleted
	INT			iHandiCompleted
	INT			iBreakDownCompleted
	INT			iAccidentCompleted
	INT			iLastNodeIndex
ENDSTRUCT
