USING "candidate_enums.sch"

//Candidate monitoring globals/////////////////////////////////////////////////////


INT g_iCurrentCandidatesInList = 0
INT g_iCurrentlyRunningCandidateID = -2
THREADID g_CurrentlyRunningCandidateThread = NULL
INT g_nextMissionCandidateID = 0//used to assign IDs to requests
BOOL g_bExileMidTriggering = FALSE
BOOL g_bCandidateSystemMidProcessing = FALSE

m_structMissionCandidate g_listCandidates[MAX_MISSION_CANDIDATES]

MISSION_CANDIDATE_MISSION_TYPE_ENUM g_OnMissionState = MISSION_TYPE_OFF_MISSION


