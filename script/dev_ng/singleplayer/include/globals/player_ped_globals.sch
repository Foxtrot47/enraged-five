USING "tattoo_globals.sch"
USING "commands_task.sch"
USING "commands_hud.sch"

//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	player_ped_globals.sch										//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Enums and structs for use with current player ped.			//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

CONST_INT NUMBER_OF_DLC_WEAPONS 				51 // PS3/360 is capped at 16
CONST_INT NUMBER_OF_DLC_WEAPONS_MP				66
CONST_INT NUM_PLAYER_PED_WEAPON_SLOTS 			NUM_WEAPONSLOTS
CONST_INT NUM_PLAYER_PED_WEAPON_GROUPS 			17	// total of WEAPON_GROUP - hard coded for now at Kenneth's request, since weapon_enums,sch gets generated from code changes
CONST_INT NUM_PLAYER_PED_PROPS 					9
CONST_INT NUM_PLAYER_PED_MOD_WEAPONS 			23
CONST_INT NUM_PLAYER_PED_WEAPON_BIT_SETS		2
CONST_INT NUM_PLAYER_PED_WEAPON_MOD_BIT_SETS 	4
CONST_INT NUM_PLAYER_VEHICLE_IDS		 		9 // 3 for each player character
CONST_INT NUM_PLAYER_PED_IDS		 			9 // 
CONST_INT NUM_NPC_VEHICLE_IDS		 			3 // 3 for each npc character

CONST_INT SAVED_VEHICLE_SLOT_CAR				0 // Used to represent array index for sPlayerVehicle
CONST_INT SAVED_VEHICLE_SLOT_BIKE				1 // 
CONST_INT SAVED_VEHICLE_SLOT_GARAGE				2 // 
CONST_INT SAVED_VEHICLE_SLOT_MODDED				3 // 


CONST_INT NUMBER_OF_PLAYER_STATS 8

CONST_INT NUMBER_OF_VEHICLE_EXTRAS 12

// We set this flag whenever we start a new game or switch character so we can set melee modifier
BOOL g_bPerformCustomStatUpdate
BOOL g_bPerformCustomStatUpdateWhenMissionEnds
BOOL g_bDisableStatUpdatesThisFrame

BOOL g_bHandleEndOfMission_Martin1 = FALSE

// this bool is set to true when loading a quick save / repeat play save
// so that the player's variations don't get set up twice
BOOL g_bPlayerSetupByQuickSave = FALSE

/// PURPOSE: Enum list for use with GET_PLAYER_PED_STAT_ENUM() to get the correct stat for current character.
ENUM PLAYER_STATS_ENUM
	PS_SPECIAL_ABILITY = 0,
	PS_STAMINA,
	PS_STRENGTH,
	PS_LUNG_CAPACITY,
	PS_DRIVING_ABILITY,
	PS_FLYING_ABILITY,
	PS_SHOOTING_ABILITY,
	PS_STEALTH_ABILITY
ENDENUM

// COMPONENT BLOCKS FOR CUTSCENE PROCS
ENUM COMPONENT_BLOCK_FLAGS
	CBF_NONE 		= 0,
	CBF_DEFAULT	 	= 1, // Parachutes, helmets, headsets
	CBF_ALL		 	= 2, 
	CBF_MASKS 		= 4,
	CBF_HELMETS 	= 8,
	CBF_HEADSETS 	= 16,
	CBF_PARACHUTES 	= 32,
	CBF_SCUBA	 	= 64
ENDENUM


// WARDROBE DATA
ENUM PLAYER_WARDROBE_ENUM
	PW_MICHAEL_MANSION = 0,
	PW_MICHAEL_COUNTRYSIDE,
	
	PW_TREVOR_COUNTRYSIDE,
	PW_TREVOR_CITY,
	PW_TREVOR_STRIPCLUB,
	
	PW_FRANKLIN_AUNTS,
	PW_FRANKLIN_HILLS,
	
	PW_FREEMODE,
	
	DUMMY_WARDROBE
ENDENUM
CONST_INT NUM_PLAYER_PED_WARDROBES (DUMMY_WARDROBE)

ENUM LIMITED_PLAYER_WARDROBE_ENUM
	LPW_MICHAEL_COUNTRYSIDE = 0,
	LPW_TREVOR_CITY,
	LPW_TREVOR_STRIPCLUB
ENDENUM
CONST_INT NUM_PLAYER_PED_LIMITED_WARDROBES 3

ENUM eWeaponWheelSlot
	WEAPON_WHEEL_SLOT_PISTOL = 0,
	WEAPON_WHEEL_SLOT_SMG,
	WEAPON_WHEEL_SLOT_RIFLE,
	WEAPON_WHEEL_SLOT_SNIPER,
	WEAPON_WHEEL_SLOT_UNARMED_MELEE,
	WEAPON_WHEEL_SLOT_SHOTGUN,
	WEAPON_WHEEL_SLOT_HEAVY,
	WEAPON_WHEEL_SLOT_THROWABLE_SPECIAL,

	MAX_WHEEL_SLOTS
ENDENUM

PLAYER_WARDROBE_ENUM g_eCurrentWardrobe
BOOL g_bWardrobeScriptRunning[NUM_PLAYER_PED_WARDROBES]
BOOL g_bWardrobeScriptLaunched[NUM_PLAYER_PED_WARDROBES]
STRUCT WARDROBE_LAUNCHER_STRUCT
	PLAYER_WARDROBE_ENUM eWardrobe
	VECTOR vCoords
	FLOAT fHeading
ENDSTRUCT
STRUCT LIMITED_WARDROBE_ITEMS
	INT iItemBitset[NUMBER_OF_PED_COMP_TYPES][PED_COMPONENT_BITSETS]
ENDSTRUCT





// A flag to indicate if the player is allowed to change their clothes using the wardrobe or shops whilst on a misson
BOOL g_bPlayerCanChangeClothesOnMission = TRUE

// A flag to indicate that we should not set the players health/armour/ammo etc from the stored data - let code handle it
//BOOL g_bSetupPlayerFromLaunch = FALSE

/*
// A flag to indicate when the player selector has finished and player_timetable_scene.sc can release the player ped
BOOL g_bSwitchSplineCamInProgress = FALSE
*/

// A flag to indicate if the player variations should be updated during cutscenes
BOOL g_bSetPlayerClothesInCutscenes = TRUE
BOOL g_bSetPlayerWeaponsInCutscenes = TRUE


// A flag to indicate that the player is using the shop or wardrobe to change clothes
BOOL g_bPlayerIsChangingClothes = FALSE


// A flag to indicate that ambient player peds will not be created (due to friend activity starting)
BOOL g_bSupressAmbientPlayersForFriendActivity = FALSE

// 
BOOL g_b_NOT_PlayerHasStartedNewScene = FALSE

// A flag to indicate if we should use per character timecycles
BOOL g_bUseCharacterFilters = FALSE

// A counter to indicate the amount of available missions a character has
INT g_iAvailablePlayerMissions[NUM_OF_PLAYABLE_PEDS]
SP_MISSIONS g_eFirstAvailablePlayerMission[NUM_OF_PLAYABLE_PEDS]	//stored the first found missionID available for that player character
INT g_iQueuedCalls[NUM_OF_PLAYABLE_PEDS]
INT g_iQueuedTxts[NUM_OF_PLAYABLE_PEDS]

// A global int for storing the synched scene ID for a switch so it can be shared amongst scripts
INT g_iPlayer_Timetable_Loop_SynchSceneID = -1	//for the looping stage of a switch
INT g_iPlayer_Timetable_Exit_SynchSceneID = -1	//for the exit stage of a switch

// Global vehicle index to track created player vehicles.
VEHICLE_INDEX g_viCreatedPlayerVehicleIDs[NUM_PLAYER_VEHICLE_IDS]
enumCharacterList g_eCreatedPlayerVehiclePed[NUM_PLAYER_VEHICLE_IDS]
MODEL_NAMES g_eCreatedPlayerVehicleModel[NUM_PLAYER_VEHICLE_IDS]
// Global vehicle index to track created npc vehicles.
VEHICLE_INDEX g_viCreatedNPCVehicleIDs[NUM_NPC_VEHICLE_IDS]
// Global vehicle index to track last vehicle sent to cloud
VEHICLE_INDEX g_viLastVehicleSentToCloud[NUM_OF_PLAYABLE_PEDS]
// Global vehicle index to track created player peds.
PED_INDEX g_piCreatedPlayerPedIDs[NUM_PLAYER_PED_IDS]

INT g_iCreatedPlayerVehicleCleanupTimer[NUM_OF_PLAYABLE_PEDS][2]

BOOL g_bSPInitialisedThroughSwitch = FALSE

//////////////////////////////////////////////////////////////////////////////////////////
///    Enums for the activities that alter special ability meter
ENUM PLAYER_ACTIVITY_ENUM
	// Remove this default debug case once all activities have been created
	ACTIVITY_DEFAULT_DEBUG = 0,
	
	ACTIVITY_DEATH,
	ACTIVITY_ARREST,
	ACTIVITY_MISSION_FAILED,
	ACTIVITY_MISSION_PASSED,
	ACTIVITY_EAT,
	ACTIVITY_SLEEP,
	ACTIVITY_YOGA,
	ACTIVITY_CYCLE,
	ACTIVITY_JOG,
	ACTIVITY_TENNIS,
	ACTIVITY_SHOOTING_RANGE,
	ACTIVITY_RANDOM_EVENT,
	ACTIVITY_BUY_WEAPON,
	ACTIVITY_BUY_CLOTHES,
	ACTIVITY_BUY_HAIRCUT,
	ACTIVITY_BUY_TATTOO,
	ACTIVITY_BUY_VEHICLE,
	ACTIVITY_MOD_VEHICLE
	/*
	IMPORTANT !!!
	If you add a new enum, be sure to setup the speical ability charging/depleting command
	in player_ped_public.sch - UPDATE_SPECIAL_ABILITY_FROM_ACTIVITY()
	*/
ENDENUM

//////////////////////////////////////////////////////////////////////////////////////////
///    Flags for checking players vehicle type
ENUM PLAYER_VEHICLE_FLAGS
	
	PV_CAR = 1,
	PV_BIKE = 2,
	PV_HELI = 4,
	PV_BOAT = 8,
	
	PV_2_DOOR = 16,
	PV_4_DOOR = 32,
	
	PV_2_SEATER = 64,
	PV_4_SEATER = 128,
	
	PV_DEFAULT_PLAYER_VEHICLE = 256,
	
	PV_SAFE_FOR_REPOSITION = 512
	
ENDENUM

//////////////////////////////////////////////////////////////////////////////////////////
///    Struct for storing last known stuff on the player for switching back...
STRUCT DEFAULT_PLAYER_SWITCH_STATE_STRUCT
	VECTOR	vVehicleCoord_a, vVehicleCoord_b, vVehicleCoord_c
	FLOAT	fVehicleHead_a, fVehicleHead_b, fVehicleHead_c
	FLOAT	fVehicleSpeed_a, fVehicleSpeed_b, fVehicleSpeed_c
	BOOL	bVehicleCoordForced, bPlayerInScuba
	
	VECTOR	vWalkCoord_a, vWalkCoord_b, vWalkCoord_c
	FLOAT	fWalkHead_a, fWalkHead_b, fWalkHead_c
ENDSTRUCT
DEFAULT_PLAYER_SWITCH_STATE_STRUCT	g_sDefaultPlayerSwitchState[NUM_OF_PLAYABLE_PEDS]

//////////////////////////////////////////////////////////////////////////////////////////
///    Enums for the vehicle creation procs
CONST_INT NUM_OF_PLAYER_PED_VEHICLE_TYPES 4
ENUM VEHICLE_CREATE_TYPE_ENUM
	VEHICLE_TYPE_DEFAULT = 0,
	VEHICLE_TYPE_CAR,
	VEHICLE_TYPE_BIKE,
	VEHICLE_TYPE_SECONDARY_CAR
ENDENUM

//////////////////////////////////////////////////////////////////////////////////////////
///    Structs/Enums for the general ped data
ENUM PED_REQUEST_STATE_ENUM
	PR_STATE_WAITING = 0,	// Waiting for the request to be made
	PR_STATE_PROCESSING,	// Request is currently being processed
	PR_STATE_COMPLETE		// Request has just finished
ENDENUM

STRUCT PED_REQUEST_STRUCT
	SELECTOR_PED_STRUCT		sSelectorPeds
	PED_REQUEST_STATE_ENUM  eState
	enumCharacterList		ePed
	INT						iStage
	INT						iBitsetPedInLeadIn	// A bitset to track which of the selector pedIDs are created by a lead-in.					
ENDSTRUCT
PED_REQUEST_STRUCT g_sPlayerPedRequest // Maybe change this to a list later to handle multiple requests

STRUCT PED_VARIATION_STRUCT
	// Components
	INT iTextureVariation[NUM_PED_COMPONENTS]
	INT iDrawableVariation[NUM_PED_COMPONENTS]
	INT iPaletteVariation[NUM_PED_COMPONENTS]
	
	// Props
	INT iPropIndex[NUM_PLAYER_PED_PROPS]
	INT iPropTexture[NUM_PLAYER_PED_PROPS]
	
	// These are needed so we can store / restore hair / beards when player equips / removes masks + hats
	PED_COMP_NAME_ENUM eStoredHairstyle
	PED_COMP_NAME_ENUM eItemThatForcedHairChange
	PED_COMP_TYPE_ENUM eTypeThatForcedHairChange
	PED_COMP_NAME_ENUM eStoredBeard
	PED_COMP_NAME_ENUM eItemThatForcedBeardChange
	PED_COMP_TYPE_ENUM eTypeThatForcedBeardChange
ENDSTRUCT

STRUCT PED_WEAPONS_STRUCT
	WEAPON_INFO sWeaponInfo[NUM_PLAYER_PED_WEAPON_SLOTS]
	WEAPON_INFO sDLCWeaponInfo[NUMBER_OF_DLC_WEAPONS]
ENDSTRUCT

STRUCT PED_WEAPONS_MP_STRUCT
	WEAPON_INFO sWeaponInfo[NUM_PLAYER_PED_WEAPON_SLOTS]
	WEAPON_INFO sDLCWeaponInfo[NUMBER_OF_DLC_WEAPONS_MP]
ENDSTRUCT
PED_WEAPONS_MP_STRUCT g_sTempPedWeaponsMPData

STRUCT WEAPONS_PURCHASE_DATA
	INT iWeaponBitset[4]
	INT iWeaponAddonBitset[10]
	INT iWeaponTintBitset[15]
ENDSTRUCT
WEAPONS_PURCHASE_DATA sPurchasedWeaponData

STRUCT PED_VEH_DATA_STRUCT
	MODEL_NAMES model, modelTrailer
	FLOAT fDirtLevel
	INT fHealth
	INT iColourCombo
	INT iColour1, iColour2
	INT iColourExtra1, iColourExtra2
	BOOL bColourCombo, bColourExtra	//	going to remove these soon
	BOOL bExtraOn[12] // [0+1] to [8+1]		/* [1] to [9] */
	BOOL bConvertible
	INT iRadioIndex
	INT iPlateBack
	TEXT_LABEL_15 tlNumberPlate
	INT iModIndex[MAX_VEHICLE_MOD_SLOTS]
	INT iModVariation[MAX_VEHICLE_MOD_VAR_SLOTS]
	INT iTyreR, iTyreG, iTyreB
	INT iWindowTintColour
	BOOL bTyresCanBurst
	INT iLivery
	INT iWheelType
	VEHICLE_CREATE_TYPE_ENUM eType
	
	INT iFlags
	INT iNeonR, iNeonG, iNeonB
	
	FLOAT fEnvEff
	
	BOOL bIsPlayerVehicle
ENDSTRUCT

STRUCT PED_INFO_STRUCT

	//	ANY NEW VARIABLES MUST BE RESET IN THE SETUP_DEFAULT_PLAYER_INFO() PROC
	//	WITHIN THE player_ped_private.sch HEADER FILE!!!
	
	PED_VARIATION_STRUCT sVariations[NUM_OF_PLAYABLE_PEDS]
	
	PED_COMP_NAME_ENUM eGolfTop[NUM_OF_PLAYABLE_PEDS]
	PED_COMP_NAME_ENUM eGolfPants[NUM_OF_PLAYABLE_PEDS]
	
	PED_COMP_NAME_ENUM eTrackedPedComp[NUM_PED_COMPONENTS][NUM_OF_PLAYABLE_PEDS]
	PED_COMP_NAME_ENUM eTrackedPedProps[NUM_PLAYER_PED_PROPS][NUM_OF_PLAYABLE_PEDS]
	
	FLOAT			fHealthPerc[NUM_OF_PLAYABLE_PEDS]
	INT				iArmour[NUM_OF_PLAYABLE_PEDS]
	
	PED_WEAPONS_STRUCT sWeapons[NUM_OF_PLAYABLE_PEDS]
	WEAPON_TYPE eWeaponToShow[MAX_WHEEL_SLOTS][NUM_OF_PLAYABLE_PEDS]
	INT equippedWeaponSlot = -1
	
	PED_WEAPONS_STRUCT sMissionSnapshotWeapons
	
	INT 			iUnlockedCarMods[5] // NOTE: We only use 107 bits for carmods the rest is up for grabs for other use - see script_misc.sch
	
	INT 			iPlayerStatScript[NUMBER_OF_PLAYER_STATS][NUM_OF_PLAYABLE_PEDS]
	INT				iWheelieDayTimer[NUM_OF_PLAYABLE_PEDS]
	INT 			iWheelieUpdatesThisDay[NUM_OF_PLAYABLE_PEDS]
	INT 			iWheelieTimeAtStartOfDay[NUM_OF_PLAYABLE_PEDS]
	BOOL 			bGrabCurrentWheelieTime[NUM_OF_PLAYABLE_PEDS]
	
	
	TIMEOFDAY		sLastTimeActive[NUM_OF_PLAYABLE_PEDS]
	VECTOR 			vLastKnownCoords[NUM_OF_PLAYABLE_PEDS]
	FLOAT			fLastKnownHead[NUM_OF_PLAYABLE_PEDS]
	INT 			iLastKnownRoomKey[NUM_OF_PLAYABLE_PEDS]
	VECTOR 			vLastKnownVelocity[NUM_OF_PLAYABLE_PEDS]
	INT 			iLastKnownWantedLevel[NUM_OF_PLAYABLE_PEDS]
	
	BOOL 			bChangedClothesOnMission[NUM_OF_PLAYABLE_PEDS]
	
	TIMEOFDAY		sLastTimeWeChangedClothes[NUM_OF_PLAYABLE_PEDS]
	INT				iLastTimeWeChangedHairdo[NUM_OF_PLAYABLE_PEDS]
	INT				iLastTimeWeGotTattoo[NUM_OF_PLAYABLE_PEDS]
	
	BOOL			bPedAvailable[NUM_OF_PLAYABLE_PEDS]
	BOOL 			bDefaultInfoSet
	BOOL			bDefaultClothesInfoSet
	BOOL 			bDefaultStatsSet
	
	FLOAT 			fStatOffset_DistRunning[NUM_OF_PLAYABLE_PEDS]
	INT				iStatOffset_HitsUnarmed[NUM_OF_PLAYABLE_PEDS]
	INT				iStatOffset_NumberNearMisses[NUM_OF_PLAYABLE_PEDS]
	FLOAT			fStatOffset_DistStealth[NUM_OF_PLAYABLE_PEDS]
	INT				iStatOffset_StealtKills[NUM_OF_PLAYABLE_PEDS]
	INT				iStatOffset_HitsMission[NUM_OF_PLAYABLE_PEDS]
	INT				iStatOffset_HitsOffMission[NUM_OF_PLAYABLE_PEDS]
	
	BOOL 			bSpecialAbilityUnlocked[NUM_OF_PLAYABLE_PEDS]
	INT 			iTrackedSpecialAbilityUnlock[NUM_OF_PLAYABLE_PEDS]
	
	
	INT				iTrackedPedComps[NUM_OF_PLAYABLE_PEDS]
	
	PED_COMP_NAME_ENUM 	eFBI4MaskName[NUM_OF_PLAYABLE_PEDS]
	PED_COMP_TYPE_ENUM	eFBI4MaskType[NUM_OF_PLAYABLE_PEDS]
	
	PED_COMP_NAME_ENUM eFBI4BoilerSuit[NUM_OF_PLAYABLE_PEDS]
	
	PED_VEH_DATA_STRUCT sPlayerVehicle[4][NUM_OF_PLAYABLE_PEDS] // [4] : 0 = Car, 1 = Bike, 2 = Garage, 3 = Modded
	
	VECTOR vPlayerVehicleCoords[2][NUM_OF_PLAYABLE_PEDS]
	FLOAT fPlayerVehicleHeading[2][NUM_OF_PLAYABLE_PEDS]
	
	PED_VEH_DATA_STRUCT sNPCVehicle[1][2] // [Car], [Amanda][Tracey]
	
	LIMITED_WARDROBE_ITEMS sLimitedWardrobeItems[NUM_PLAYER_PED_LIMITED_WARDROBES]
	
	BOOL bPlayerVariationsStoredToStats
	
	BOOL bJimmyModsSetOnMichaelsCar
	
	SAVEHOUSE_NAME_ENUM eLastVehicleSentToCloudSavehouse[NUM_OF_PLAYABLE_PEDS]
	
	// Enums to keep track of the current and last known player ped
	enumCharacterList 	eCurrentPed
	enumCharacterList 	ePreviousPed
	enumCharacterList 	eLastKnownPed
ENDSTRUCT


//////////////////////////////////////////////////////////////////////////////////////////
///    Structs/Enums for the player ped scenes (uses PED_VEH_DATA_STRUCT)
USING "player_scene_globals.sch"			//kept in seperate header to keep player_ped_globals.sch clean


//////////////////////////////////////////////////////////////////////////////////////////
///    Structs/Enums for debug purpouses
#IF IS_DEBUG_BUILD
BOOL g_bDrawPlayerControlDebugDisplay
BOOL g_bWarpDebugPlayerCharScene
BOOL g_bKeepAmmoWhenResettingPlayerData, g_bResetAmmoNewGame
INT g_iDebugPlayerStatOffset[NUMBER_OF_PLAYER_STATS]
#ENDIF


//////////////////////////////////////////////////////////////////////////////////////////
///    Struct to store temp variations during missions
PED_VARIATION_STRUCT g_sLastStoredPlayerPedClothes[NUM_OF_PLAYABLE_PEDS]

BOOL g_bDLCClothesCheckedOnRestore[NUM_OF_PLAYABLE_PEDS]

PED_INDEX g_ambientSelectorPedDeleteQueue[NUMBER_OF_SELECTOR_PEDS]

//////////////////////////////////////////////////////////////////////////////////////////
///    Struct for the global saved data
STRUCT PlayerDataSaved

	// A list of all the component items that are available/acquired
	PED_COMPONENTS_STRUCT sComponents[NUM_OF_PLAYABLE_PEDS]
	
	// A list of all the player peds current tattoos.
	PED_TATTOO_STRUCT sTattoos[NUM_OF_PLAYABLE_PEDS]
	
	// Various pieces of data to keep track of the peds.
	PED_INFO_STRUCT sInfo
	
	// A global ID used to save the core outift that Franklin is wearing when Lamar asks him to change his clothes.
	INT iFranklinOriginalOutfitID = -1
	
	// force the player to load as this character
	enumCharacterList eOverridePed = NO_CHARACTER
	
	// Patch flags
	INT iDLCPatchBitset
ENDSTRUCT

PED_VARIATION_STRUCT g_Peyote_sPedVariations

