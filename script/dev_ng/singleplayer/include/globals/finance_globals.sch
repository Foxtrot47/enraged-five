USING "finance_enums.sch"
USING "charsheet_global_definitions.sch"
USING "finance_generated_header.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	finance_globals.sch
//		AUTHOR			:	Ak
//		DESCRIPTION		:	The global data for the player's accounts and the stock market
//							
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


INT 	g_iForceStockLogUpdate = 0
BOOL 	g_bBrowserStockPriceSynchInProgress
INT 	g_iBrowserStockTimeLastSynch = -1


// *****************************************************************************************
// ********************************Account management data**********************************
// *****************************************************************************************
//Account balances/logs

ENUM BANK_ACCOUNT_ACTION_TYPE
	BAA_DEBIT,
	BAA_CREDIT
ENDENUM

//anything that can credit or debit an account should be here
ENUM BANK_ACCOUNT_ACTION_SOURCE_BAAC// TODO, move out to finance enums
	
	// Default/basic actions.
	BAAC_DEFAULT_DEBUG,
	BAAC_UNLOGGED_SMALL_ACTION,
	BAAC_BROKERAGE_PAYMENT,
	
	// Characters that can pay others
	BAAC_MICHAEL,
	
	// Company actions.
	BAAC_CLUCKING_BELL,
	BAAC_WHIZZ_PHONE,
	BAAC_MADAM_CHONGS,
	BAAC_DS_CLOTHING,
	BAAC_LOS_SANTOS_HOSPITAL,
	BAAC_CRAPKIA,
	BAAC_VINE_CLEAN,
	BAAC_CANDYSUXX,
	BAAC_VINEWOOD_BEAUTY,
	BAAC_AMMUNATION,
	BAAC_BAHAMAMAMAS,
	BAAC_BAY_BAR,
	BAAC_BIKER_BAR,
	BAAC_HIMEN_BAR,
	BAAC_MOJITOS_BAR,
	BAAC_SHENANIGANS_BAR,
	BAAC_SINGLETONS_BAR,

	// Service actions.
	BAAC_TAXI,
	BAAC_DRUG_TRAFFICKING,
	BAAC_CAR_REPO,
	BAAC_SHRINK,
	BAAC_STRIP_CLUB,
	BAAC_HUNTING,
	BAAC_SHOOTING_RANGE,
	BAAC_RACES,
	
	// Other/misc actions.
	BAAC_EPSILON_SITE_DONATION,
	BAAC_EPSILON_ROBES_DONATION,

	// Mission actions.
	BAAC_SIMEON,
	BAAC_LESTER,
	BAAC_AMANDA,
	BAAC_JIMMY,
	BAAC_TRACEY,
	BAAC_OSCAR,
	BAAC_ABIGAIL,
	BAAC_RE_BURIAL,
	BAAC_MRSPOKE,  					// Family 2 - Bike rental
	BAAC_LOS_SANTOS_GOLF_CLUB,

	// Hairdo shops
	BAAC_HAIRDO_SHOP_01_BH, 		// Salon - Rockford Hills
	BAAC_HAIRDO_SHOP_02_SC, 		// Barbers - South Los Santos
	BAAC_HAIRDO_SHOP_03_V, 			// Barbers - Vespucci
	BAAC_HAIRDO_SHOP_04_SS, 		// Barbers - Sandy Shores
	BAAC_HAIRDO_SHOP_05_MP, 		// Barbers - Mirror Park
	BAAC_HAIRDO_SHOP_06_HW, 		// Barbers - Vinewood
	BAAC_HAIRDO_SHOP_07_PB, 		// Barbers - Paleto Bay
	
	// Clothes shops
	BAAC_CLOTHES_SHOP_L_01_SC,	 	// Clothes Low - South Los Santos
	BAAC_CLOTHES_SHOP_L_02_GS,	 	// Clothes Low - Grapeseed
	BAAC_CLOTHES_SHOP_L_03_DT,	 	// Clothes Low - Downtown
	BAAC_CLOTHES_SHOP_L_04_CS,	 	// Clothes Low - Countryside
	BAAC_CLOTHES_SHOP_L_05_GSD,	 	// Clothes Low - Grande Senora Desert
	BAAC_CLOTHES_SHOP_L_06_VC,	 	// Clothes Low - Vespucci Canals
	BAAC_CLOTHES_SHOP_L_07_PB,	 	// Clothes Low - Paleto Bay
	
	BAAC_CLOTHES_SHOP_M_01_SM, 		// Clothes Mid - Del Perro
	BAAC_CLOTHES_SHOP_M_03_H, 		// Clothes Mid - Harmony
	BAAC_CLOTHES_SHOP_M_04_HW, 		// Clothes Mid - Vinewood
	BAAC_CLOTHES_SHOP_M_05_GOH, 	// Clothes Mid - Great Ocean Highway
	
	BAAC_CLOTHES_SHOP_H_01_BH, 		// Clothes High - Rockford Hills
	BAAC_CLOTHES_SHOP_H_02_B, 		// Clothes High - Burton
	BAAC_CLOTHES_SHOP_H_03_MW, 		// Clothes High - Morningwood
	
	BAAC_CLOTHES_SHOP_A_01_VB, 		// Clothes Ambient - Vespucci Movie Masks
	
	// Tattoos shops
	BAAC_TATTOO_PARLOUR_01_HW, 		// Tattoo - Vinewood
	BAAC_TATTOO_PARLOUR_02_SS, 		// Tattoo - Sandy Shores
	BAAC_TATTOO_PARLOUR_03_PB, 		// Tattoo - Paleto Bay
	BAAC_TATTOO_PARLOUR_04_VC, 		// Tattoo - Vespucci Canals
	BAAC_TATTOO_PARLOUR_05_ELS, 	// Tattoo - East Los Santos
	BAAC_TATTOO_PARLOUR_06_GOH, 	// Tattoo - Great Ocean Highway
	
	// Gun shops
	BAAC_GUN_SHOP_01_DT,	 		// Weapons - Downtown
	BAAC_GUN_SHOP_02_SS,	 		// Weapons - Sandy Shores
	BAAC_GUN_SHOP_03_HW,	 		// Weapons - Vinewood
	BAAC_GUN_SHOP_04_ELS, 			// Weapons - East Los Santos
	BAAC_GUN_SHOP_05_PB,	 		// Weapons - Paleto Bay
	BAAC_GUN_SHOP_06_LS,	 		// Weapons - Little Seoul
	BAAC_GUN_SHOP_07_MW,	 		// Weapons - Morningwood
	BAAC_GUN_SHOP_08_CS,	 		// Weapons - Countryside
	BAAC_GUN_SHOP_09_GOH, 			// Weapons - Great Ocean Highway
	BAAC_GUN_SHOP_10_VWH, 			// Weapons - Vinewood Hills
	BAAC_GUN_SHOP_11_ID1, 			// Weapons - Cypress Flats
								   
	// Car mod shops
	BAAC_CARMOD_SHOP_01_AP, 		// Car Mod - AMB1
	BAAC_CARMOD_SHOP_05_ID2,		// Car Mod - AMB2
	BAAC_CARMOD_SHOP_06_BT1,		// Car Mod - AMB3
	BAAC_CARMOD_SHOP_07_CS1,		// Car Mod - AMB4
	BAAC_CARMOD_SHOP_08_CS6,		// Car Mod - AMB5
	
	// Vehicle websites
	BAAC_MOTORSPORT_SITE, 
	BAAC_ARMY_SITE, 
	BAAC_PLANE_SITE,
	BAAC_BOAT_SITE,
	BAAC_BIKE_SITE,
	BAAC_SUPERAUTO_SITE,
	BAAC_LOWRIDER_SITE,
	BAAC_ARENA_SITE,
	
	// Other websites
	BAAC_CONSITE,					// www.iwillsurviveitall.com
	BAAC_REALITY_MILL,				// www.therealitymill.com

	// Misc
	BAAC_BAIL_BONDS,
	BAAC_CASH_DEPOSIT,
	BAAC_HEIST_OFFSHORE_ACCOUNT,
	BAAC_SNACKS,
	
	// Properties
	BAAC_PROP_TOWING,				// Towing Impound (Franklin)
	BAAC_PROP_TAXI,					// Taxi Lot (all)
	BAAC_PROP_ARMS,					// Arms Trafficking (Trevor)
	BAAC_PROP_SONAR,				// Sonar Collections (All)
	BAAC_PROP_CARMOD,				// Car Mod Shop (Franklin)
	BAAC_PROP_VCINEMA,				// Vinewood Cinema (Michael)
	BAAC_PROP_DCINEMA,				// Downtown Cinema (Michael)
	BAAC_PROP_MCINEMA,				// Morningwood Cinema (Michael)
	BAAC_PROP_GOLF,					// Golf club (All)
	BAAC_PROP_CSCRAP,				// Car Scrap Yard (Franklin)
	BAAC_PROP_SMOKE,				// Smoke on the Water Weed Shop (Franklin)
	BAAC_PROP_BAR_TEQUILA,			// Tequi-la-la Bar (All)
	BAAC_PROP_BAR_PITCHERS,			// Pitchers (All)
	BAAC_PROP_BAR_HEN,				// The Hen House (all)
	BAAC_PROP_BAR_HOOKIES,			// Hookies Bar (all)
	BAAC_PROP_MARINA,
	BAAC_PROP_HANGAR,
	BAAC_PROP_HELIPAD,
	BAAC_PROP_GARAGE,
	
	// Police stations
	BAAC_POLICE_STATION_VB, 		// Vespucci Beach
	BAAC_POLICE_STATION_SC,			// South Central
	BAAC_POLICE_STATION_DT,			// Downtown
	BAAC_POLICE_STATION_RH,			// Rockford Hills
	BAAC_POLICE_STATION_SS,			// Sandy Shores
	BAAC_POLICE_STATION_PB,			// Paleto Bay
	BAAC_POLICE_STATION_HW,			// Vinewood (#1289759)
	
	// Hospitals
	BAAC_HOSPITAL_RH, 				// Rockford Hills
	BAAC_HOSPITAL_SC, 				// South Central
	BAAC_HOSPITAL_DT, 				// Downtown
	BAAC_HOSPITAL_SS, 				// Sandy Shores
	BAAC_HOSPITAL_PB,				// Paleto Bay
	
	//MP
	
	BAAC_DYNASTY_PROPERTY
	
	,BAAC_CARMOD_SHOP_SUPERMOD		// Car Mod - AMB6
	
ENDENUM

/// PURPOSE: used to store the current state of news to be displayed on Eye Find
ENUM EYEFIND_NEWS_STORY_STATE_ENUM
	// SP mission unlocks
	EYEFIND_NEWS_STORY_STATE_SP_PRO = 0,	// Newsbreak1
	EYEFIND_NEWS_STORY_STATE_SP_ARM2,		// Newsbreak2
	EYEFIND_NEWS_STORY_STATE_SP_ARM3,		// Newsbreak3
	EYEFIND_NEWS_STORY_STATE_SP_FAM1,		// Newsbreak4
	EYEFIND_NEWS_STORY_STATE_SP_FAM3,		// Newsbreak5
	EYEFIND_NEWS_STORY_STATE_SP_LAM1,		// Newsbreak6
	EYEFIND_NEWS_STORY_STATE_SP_LST1,		// Newsbreak7
	
	EYEFIND_NEWS_STORY_STATE_SP_H_JWL,		// Newsbreak8
	
	EYEFIND_NEWS_STORY_STATE_SP_TRE2,		// Newsbreak9
	EYEFIND_NEWS_STORY_STATE_SP_CHI1,		// Newsbreak10
	EYEFIND_NEWS_STORY_STATE_SP_CHI2,		// Newsbreak11
	EYEFIND_NEWS_STORY_STATE_SP_TRE3,		// Newsbreak12
	EYEFIND_NEWS_STORY_STATE_SP_FAM4,		// Newsbreak13
	EYEFIND_NEWS_STORY_STATE_SP_FIB2,		// Newsbreak14
	EYEFIND_NEWS_STORY_STATE_SP_FIB3,		// Newsbreak15
	EYEFIND_NEWS_STORY_STATE_SP_FRA1,		// Newsbreak16
	EYEFIND_NEWS_STORY_STATE_SP_FIB4,		// Newsbreak17
	EYEFIND_NEWS_STORY_STATE_SP_H_DOCK,		// Newsbreak18
	EYEFIND_NEWS_STORY_STATE_SP_CARS2,		// Newsbreak19
	EYEFIND_NEWS_STORY_STATE_SP_SOL1,		// Newsbreak20
	EYEFIND_NEWS_STORY_STATE_SP_MTN1,		// Newsbreak21
	EYEFIND_NEWS_STORY_STATE_SP_CARS3,		// Newsbreak22
	EYEFIND_NEWS_STORY_STATE_SP_EXL1,		// Newsbreak23
	EYEFIND_NEWS_STORY_STATE_SP_H_RB2A,		// Newsbreak24
	EYEFIND_NEWS_STORY_STATE_SP_EXL3,		// Newsbreak25
	EYEFIND_NEWS_STORY_STATE_SP_FIB5,		// Newsbreak26
	EYEFIND_NEWS_STORY_STATE_SP_MIC1,		// Newsbreak27
	EYEFIND_NEWS_STORY_STATE_SP_SOL2,		// Newsbreak28
	EYEFIND_NEWS_STORY_STATE_SP_FAM6,		// Newsbreak29
	EYEFIND_NEWS_STORY_STATE_SP_H_AG3A,		// Newsbreak30
	EYEFIND_NEWS_STORY_STATE_SP_MIC3,		// Newsbreak31
	EYEFIND_NEWS_STORY_STATE_SP_SOL3,		// Newsbreak32
	EYEFIND_NEWS_STORY_STATE_SP_MIC4,		// Newsbreak33
	EYEFIND_NEWS_STORY_STATE_SP_H_BS2A,		// Newsbreak34
	EYEFIND_NEWS_STORY_STATE_SP_FINALE,		// Newsbreak35	
	
	// RC mission unlocks
	EYEFIND_NEWS_STORY_STATE_RC_PAP1,		// Newsbreak36
	//EYEFIND_NEWS_STORY_STATE_RC_PAP2,		// Newsbreak37	- no story
	EYEFIND_NEWS_STORY_STATE_RC_PAP3A,		// Newsbreak38
	EYEFIND_NEWS_STORY_STATE_RC_PAP3B,		// Newsbreak39
	EYEFIND_NEWS_STORY_STATE_RC_DRF1,		// Newsbreak40
	EYEFIND_NEWS_STORY_STATE_RC_EPN8,		// Newsbreak41
	EYEFIND_NEWS_STORY_STATE_RC_NIG1A,		// Newsbreak42
	EYEFIND_NEWS_STORY_STATE_RC_NIG1B,		// Newsbreak43
	EYEFIND_NEWS_STORY_STATE_RC_NIG1C,		// Newsbreak44
	EYEFIND_NEWS_STORY_STATE_RC_NIG1D,		// Newsbreak45
	EYEFIND_NEWS_STORY_STATE_RC_NIG2,		// Newsbreak46
	EYEFIND_NEWS_STORY_STATE_RC_NIG3,		// Newsbreak47
	EYEFIND_NEWS_STORY_STATE_RC_EXT4,		// Newsbreak48
	
	// Assassination (now SP_)
	EYEFIND_NEWS_STORY_STATE_SP_ASS1,		// Newsbreak49
	EYEFIND_NEWS_STORY_STATE_SP_ASS2,		// Newsbreak50
	EYEFIND_NEWS_STORY_STATE_SP_ASS3,		// Newsbreak51
	EYEFIND_NEWS_STORY_STATE_SP_ASS4,		// Newsbreak52
	EYEFIND_NEWS_STORY_STATE_SP_ASS5,		// Newsbreak53
	
	// Oddjobs - Air trafficking
	EYEFIND_NEWS_STORY_STATE_O_ATA3,		// Newsbreak54
	
	//Shrink (now SP_
	EYEFIND_NEWS_STORY_STATE_SP_SHK5,		// Newsbreak55
	
	EYEFIND_NEWS_STORY_STATE_O_CULT,		// Newsbreak56
	
	EYEFIND_NEWS_STORY_STATE_INVALID
ENDENUM

CONST_INT MAX_BANK_ACCOUNT_LOG_ENTRIES 11 // cut down to 11 last entries for each char to save memory

STRUCT BANK_ACCOUNT_LOG_ENTRY
	BANK_ACCOUNT_ACTION_TYPE eType
	BANK_ACCOUNT_ACTION_SOURCE_BAAC eBaacSource
	INT iDegree
	
	//checkpoint log for 1523081
	BANK_ACCOUNT_ACTION_TYPE eTypeCheckpoint
	BANK_ACCOUNT_ACTION_SOURCE_BAAC eBaacSourceCheckpoint
	INT iDegreeCheckpoint
	
ENDSTRUCT



//each account must store a log of events too, not sure how to structure it yet...
STRUCT BANK_ACCOUNT_DATA
	INT iBalance//current balance
ENDSTRUCT


STRUCT BANK_ACCOUNT_SAVED_LOG
	INT iLogActions//the total number of actions processed
	INT iLogIndexPoint//a rolling point showing the last index
	BANK_ACCOUNT_LOG_ENTRY LogEntries[MAX_BANK_ACCOUNT_LOG_ENTRIES]
ENDSTRUCT


CONST_INT MAX_BANK_ACCOUNTS MAX_ACCOUNTS

//enumBankAccountName g_BankFocusAccount

BANK_ACCOUNT_DATA g_BankAccounts[MAX_BANK_ACCOUNTS]

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// *******************************Stock market management data******************************
// *****************************************************************************************











 // rest of values are in the finance generated header
CONST_INT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER 10
/* //now direct from savegame
STRUCT BAWSAQ_PORTFOLIO_ENTRY_STRUCT
	BAWSAQ_COMPANIES company 
	FLOAT fMoneyInvested
	INT iSharesOwned
ENDSTRUCT
*/
//these all need to be saved
//BAWSAQ_PORTFOLIO_ENTRY_STRUCT g_BS_Portfolios[BS_TR_TOTAL][MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]

BOOL g_bStockMarketBrowserUpdateTick = FALSE

//car mod
BOOL g_bStockMarketCarModified = FALSE
MODEL_NAMES g_eStockMarketCarModified

BOOL g_bInATM = FALSE

#IF IS_DEBUG_BUILD

	TEXT_LABEL_63 g_m_strFile 							= "stockSystem.log"
	TEXT_LABEL_63 g_m_strPath 							= "X:/gta5/build/dev/"
 
	
	
	BOOL g_b_financeLogFirstTick = TRUE
	
	BOOL g_bFake_fire_and_forget_new_atm = FALSE
	
	PROC DEBUG_STOCK_DUMP_HEADER_CHECK()

		IF g_b_financeLogFirstTick 
			g_b_financeLogFirstTick = FALSE
			
			IF g_b_financeLogFirstTick
				g_b_financeLogFirstTick = FALSE
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(g_m_strPath, g_m_strFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(g_m_strPath, g_m_strFile)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("---------------------------------------------------",g_m_strPath, g_m_strFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(g_m_strPath, g_m_strFile)
				STRING m_txtLogString = "[NEW STOCK LOG]\n"
				SAVE_STRING_TO_NAMED_DEBUG_FILE(m_txtLogString, g_m_strPath, g_m_strFile)
			ENDIF
			
			
		ENDIF
	ENDPROC

#ENDIF


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// Finance system saved data

CONST_INT MAX_SP_FINANCE_VALUE_STORAGE 42
CONST_INT MAX_SP_FINANCE_FILTERS 8


STRUCT FinancePortfolioBackup

	INT MIKE_SHARES_COMPANY_INDEX[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER][2]
	FLOAT MIKE_SHARES_INVESTED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER][2]
	INT MIKE_SHARES_OWNED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER][2]

	INT FRANKLIN_SHARES_COMPANY_INDEX[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER][2]
	FLOAT FRANKLIN_SHARES_INVESTED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER][2]
	INT FRANKLIN_SHARES_OWNED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER][2]
	
	INT TREVOR_SHARES_COMPANY_INDEX[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER][2]
	FLOAT TREVOR_SHARES_INVESTED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER][2]
	INT TREVOR_SHARES_OWNED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER][2]

ENDSTRUCT

FinancePortfolioBackup g_sPortfolioSnapshot // used for maintaining consistant share values on retries

STRUCT FinanceDataSaved
//BAWSAQ_PORTFOLIO_ENTRY_STRUCT g_BS_Portfolios[BS_TR_TOTAL][MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
	//BAWSAQ_COMPANIES company 
	//FLOAT fMoneyInvested
	//INT iSharesOwned
	
	INT MIKE_SHARES_COMPANY_INDEX[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
	FLOAT MIKE_SHARES_INVESTED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
	INT MIKE_SHARES_OWNED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
	
	INT FRANKLIN_SHARES_COMPANY_INDEX[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
	FLOAT FRANKLIN_SHARES_INVESTED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
	INT FRANKLIN_SHARES_OWNED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
	
	INT TREVOR_SHARES_COMPANY_INDEX[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
	FLOAT TREVOR_SHARES_INVESTED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
	INT TREVOR_SHARES_OWNED[MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER]
	
	BOOL bFirstTimeTutorialSeen
	BOOL bAtmFirstTimeFlowHelpShown
	BOOL bInitialPriceGenerationPerformed
	BOOL bShitSkipBool
	
	//Stored single player prices
	FLOAT SINGLE_PLAYER_SHARE_PRICE_DUMP[MAX_SP_FINANCE_VALUE_STORAGE]
	FLOAT SINGLE_PLAYER_SHARE_MAX_DUMP[MAX_SP_FINANCE_VALUE_STORAGE]
	FLOAT SINGLE_PLAYER_SHARE_MIN_DUMP[MAX_SP_FINANCE_VALUE_STORAGE]

	//bank data
	BOOL bFirstTimeBankInit
	BANK_ACCOUNT_SAVED_LOG PLAYER_ACCOUNT_LOGS[MAX_BANK_ACCOUNTS]
	
	INT iProfitLoss
	
	
	INT iFiltersRegistered
	BAWSAQ_COMPANIES FilteredBind[MAX_SP_FINANCE_FILTERS]
	INT iFilterDurationRemaining[MAX_SP_FINANCE_FILTERS]
	INT iFilterFlags[MAX_SP_FINANCE_FILTERS]
	
	BOOL bBrowserTutorialSeen
	
	INT iSaveCoupons
	
	EYEFIND_NEWS_STORY_STATE_ENUM eCurrentEyeFindNewsStoryState
ENDSTRUCT









// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// *************************************OFFLINE Stock News Ticker****************************
//Requirements:
//maintain a list of headlines - with some synonyms and variants?
//maintain a list of criteria for activation of those stories 
		//- price,modifiers,online (how to generalise? don't?)
//track fired stories and provide a "cool down" time during which they cannot trigger again
/*
CONST_INT MAX_BAWSAQ_NEWS_STORIES 8 //will be way more than this likely

CONST_INT MAX_BAWSAQ_NEWS_PRICE_TRIGGERS 4

CONST_INT MAX_BAWSAQ_NEWS_MODIFIER_TRIGGERS 4

//criteria for triggering a news event based on price change of a listing
//tracks the price and triggers the news story if the crieria specified by it's type is triggered
//types: - TODO make the enum table for these

ENUM BAWSAQ_NEWS_PRICE_TRIGGER_TYPE

	BS_NM_PRICE_CRASH,//a stock halves it's value twice in a given timeframe
	BS_NM_PRICE_BOOM,//a stock doubles it's value twice in a given timeframe 
	BS_NM_PRICE_RALLY//a stock halves it's value then rises to double it's value in a given timeframe
ENDENUM

ENUM BAWSAQ_NEWS_MODIFIER_TRIGGER_TYPE
	//panics: triggered when a modifier increases for a number of times in a row
	
	BS_NM_MODIFIER_RISE_PANIC,//eg. for kills "spate of lethal violence plagues city, etc etc"
	BS_NM_MODIFIER_FALL_PANIC//eg. for gym visits falling "study shows population becoming lazier!"
ENDENUM


//TODO presentation delay! especially for kills/damage

STRUCT BAWSAQ_NEWS_PRICE_TRIGGER
	//type - todo, enums
	//listing price watched
	INT iPriceIndexToWatch
	INT iNewsStoryToTrigger
	
	BAWSAQ_NEWS_PRICE_TRIGGER_TYPE myType
	//stored pinned settings (used for watching price over a the duration of fTimeFrame)
	FLOAT fTimeFrame
	
	FLOAT fPinValue
	FLOAT fPinnedAgo
	
	FLOAT fHighPinValue
	FLOAT fHighPinnedAgo
	
	FLOAT fLowPinValue
	FLOAT fLowPinnedAgo
ENDSTRUCT

//criteria for triggering a news event based on modifier change
//for both drastic sudden events, and slow trickles over time
//TODO, list these out
STRUCT BAWSAQ_NEWS_MODIFIER_TRIGGER
	INT iModifierIndexToWatch
	INT iNewsStoryToTrigger
	//
	BAWSAQ_NEWS_MODIFIER_TRIGGER_TYPE myType
	//
	INT iMovementAccumulated//used to count the number of consequtive increases or decreases

ENDSTRUCT

/// PURPOSE: 
STRUCT BAWSAQ_NEWS_STORY
	TEXT_LABEL sScrollingLable
	
	//has it been triggered and is it ready to fire next query?
	BOOL bPrimed
		
	//is it on cooldown?
	BOOL bOnCooldown
	FLOAT fBeenOnCooldownFor
	
ENDSTRUCT


//triggers
BAWSAQ_NEWS_PRICE_TRIGGER g_BS_NewsPriceTrigger[MAX_BAWSAQ_NEWS_PRICE_TRIGGERS]
BAWSAQ_NEWS_MODIFIER_TRIGGER g_BS_NewsModifierTrigger[MAX_BAWSAQ_NEWS_MODIFIER_TRIGGERS]

//Stories
BAWSAQ_NEWS_STORY g_BS_NewsStories[MAX_BAWSAQ_NEWS_STORIES]

*/


///////////////////////////////////Redesign
///    
///    
///    Story queue or buffer sorted by recentness and relevance?
///    
///    Story triggering score weights?
///    
///    3 varieties of story trigger
///    		//price
///    			//sustained rise
///    			//sustained fall
///    			//HF volitility
///
///    		//modifier
///    			//spike (killing spree -etc etc)
///    			//dormancy (no activity in this modifier for a threshold time?)
///    			
///    		//special
///    			//plot related stories
///    			
///    
///    
///    ALSO
///    
///    //market state summary string 
/// 		   	//
///    
///    //Stock tips string // do with another stack
///    			//
///   
///    
///    
///    
///    
//   

//CONST_INT BS_NEWS_STORY_BUFFER_PER_CATERGORY 3

ENUM eSTOCK_STORY_CATEGORY
	eSS_main,
	eSS_city,
	eSS_money,
	eSS_tech,
	eSS_Max_Categories
ENDENUM

STRUCT STOCK_MARKET_NEWS_STORY
	eSTOCK_STORY_CATEGORY eCat
	TEXT_LABEL tHeader
	TEXT_LABEL tSummary
	
	INT storyID //assigned automatically on creation
	FLOAT freshness //a cooldown for stories that prevents them firing repeadedly
ENDSTRUCT


//triggers
ENUM eSTOCK_MARKET_NEWS_STORY_PRICE_TRIGGER_TYPE
	PRICE_STORY_SUSTAINED_RISE,//sustained price increase over 10 ticks with end price greater than 50%+ of starting
	PRICE_STORY_SUSTAINED_FALL,//sustained price fall over 10 ticks with end price lower than 50%- of starting
	PRICE_STORY_RAPID_FLUCTUATION,//at least 5 price reversals in the last 10 ticks combined with at least a 20% price change on average, with finishing price within 20%+/- of start
	
	TOTAL_MARKET_NEWS_STORY_PRICE_TRIGGER_TYPES
ENDENUM
	
ENUM eSTOCK_MARKET_NEWS_STORY_MODIFIER_TRIGGER_TYPE
	
	MODIFIER_STORY_RATE_SPIKE_UP,//increase of 200% over less than 3 ticks followed by a drop of 100% or more over 3-5 ticks
	MODIFIER_STORY_RATE_SPIKE_DOWN,//reverse of spike up

	
	TOTAL_MARKET_NEWS_STORY_MODIFIER_TRIGGER_TYPES
ENDENUM


	


STRUCT STOCK_STORY_PRICE_TRIGGER // 1 : 1 with prices
	STOCK_MARKET_NEWS_STORY s	
	eSTOCK_MARKET_NEWS_STORY_PRICE_TRIGGER_TYPE t
	BAWSAQ_COMPANIES c
	INT checkCooldown
ENDSTRUCT



STRUCT STOCK_STORY_MODIFIER_PRICE_TRIGGER // 1 : 1 with modifier
	STOCK_MARKET_NEWS_STORY s	
	eSTOCK_MARKET_NEWS_STORY_MODIFIER_TRIGGER_TYPE t
	BSMF_TYPES m
	INT checkCooldown
ENDSTRUCT

CONST_INT PRICE_TRIGGER_STACK_TOTAL 150
CONST_INT MODIFIER_TRIGGER_STACK_TOTAL 20
INT g_iPRICE_CARET = 0
INT g_iMODIFIER_CARET = 0
INT g_iSTORY_ID_CARET = 1

STOCK_STORY_PRICE_TRIGGER g_BS_PriceTriggers[PRICE_TRIGGER_STACK_TOTAL]
STOCK_STORY_MODIFIER_PRICE_TRIGGER g_BS_ModifierTrigger[MODIFIER_TRIGGER_STACK_TOTAL]


//story stacks

CONST_INT BAWSAQ_STORIES_PER_CATEGORY 3
STOCK_MARKET_NEWS_STORY g_BS_NewsStack[eSS_Max_Categories][BAWSAQ_STORIES_PER_CATEGORY]

INT g_iStockSellAllQuote = 0

// Used to prevent the market from being seen until it is in a valid state.
BOOL g_bStockMarketInitialisationInProgress = TRUE	// Still reading stats from server.
BOOL g_bInvalidOnlinePricesRead = TRUE 				// The server returned bad data that we can't work with.
BOOL g_bTickerPriceChange = TRUE

BOOL g_StockControllerRestoreValues = FALSE

BOOL g_bBSWebsiteNoSpaceTrigger = FALSE

BOOL g_bBSWebsiteTerminated = FALSE
//#IF IS_DEBUG_BUILD
	//BOOL g_bStockMarketOnlineUploadDisabled = TRUE //disabled unless command line arg doUploadStockModifiers
	//BOOL g_DEBUG_bStockStatDebugMode = FALSE // enabled by adding the command line 
//#ENDIF

//when opening the browser to the online stock market
//it will redirect to LCN if the online market is not available 1015820
//BOOL g_bSingleShotLCNRedirect = FALSE// disabled again for    1339130


/*
•	Hair on Hawick (or other hair/barber shop)-First cut free
•	Ink Inc (or other in-game tattoo parlor)-First tattoo free
•	Warstock-Cache-And-Carry.com -X% off first purchase
•	LegendaryMotorSport.net - X% off first purchase
•	ElitasTravel.com (private jets)-X% off
•	Redwood Cigarettes – we’ll help you with your medical expenses
•	Sprunk-Free can of Sprunk from vending machine
•	Los Santos Customs-Free spray paint job 
*/
ENUM COUPON_TYPE //related to the owned coupon bitset in the save game
	COUPON_HAIRCUT,
	COUPON_TATOO,
	COUPON_MIL_SITE, //%
	COUPON_CAR_SITE,
	COUPON_PLANE_SITE,
	COUPON_MEDICAL,
	COUPON_SPRUNK,
	COUPON_SPRAY,
	COUPON_CAR_XMAS2017,
	COUPON_CAR_XMAS2018,
	COUPON_HELI_XMAS2018,
	COUPON_CAR2_XMAS2018,
	COUPON_CASINO_PLANE_SITE,	//Elitas Travel
	COUPON_CASINO_BOAT_SITE,	//Dock Tease
	COUPON_CASINO_CAR_SITE,		//Legendary Motorsports
	COUPON_CASINO_CAR_SITE2,	//Southern San Andreas Super Autos
	COUPON_CASINO_MIL_SITE,		//Warstock Cache and Carry
	COUPON_CASINO_BIKE_SITE,	//Pedal and Metal
	#IF FEATURE_GEN9_EXCLUSIVE
	// HSW mod vouchers
	COUPON_HSW_MOD1,	
	COUPON_HSW_MOD2,	
	COUPON_HSW_MOD3,	
	COUPON_HSW_MOD4,	
	COUPON_HSW_MOD5,
	COUPON_HSW_MOD6,	
	COUPON_HSW_MOD7,
	COUPON_HSW_MOD8,
	COUPON_HSW_MOD9,
	COUPON_HSW_MOD10,
	COUPON_HSW_MOD11,
	COUPON_HSW_MOD12,
	COUPON_HSW_MOD13,
	COUPON_HSW_MOD14,
	COUPON_HSW_MOD15,
	COUPON_HSW_MOD16,
	COUPON_HSW_MOD17,
	COUPON_HSW_MOD18,
	COUPON_HSW_MOD19,
	COUPON_HSW_MOD20,
	// If you add more HSW MOD coupon update MAX_NUM_HSW_MOD_COUPON & g_sMPTunables.iHSW_MOD_COUPON_LIMIT
	
	COUPON_HSW_UPGRADE,	// HSW upgrade vouchers
	COUPON_CAR_GEN9_MIGRATION,
	#ENDIF
	
	MAX_COUPONS
ENDENUM


#IF FEATURE_GEN9_EXCLUSIVE
CONST_INT MAX_NUM_HSW_MOD_COUPON 			20
CONST_INT MAX_NUM_HSW_UPGRADE_COUPON 		5
#ENDIF



































