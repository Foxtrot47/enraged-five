
//////////////////////////////////////////////////////////////////////
/* basejump_globals.sch												*/
/* Authors: DJ Jones & Ryan Paradis									*/
/* Structures and global information for the basejump activity.		*/
//////////////////////////////////////////////////////////////////////


STRUCT BasejumpSavedData
	INT iLaunchRank	
	INT iCompletedFlags // This is a BIT FIELD of completed jumps.
	FLOAT fLongestSkydive
ENDSTRUCT

#IF IS_DEBUG_BUILD
	INT g_bjDebugLaunchTimeStamp = 0	
#ENDIF
