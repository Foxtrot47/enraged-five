USING "Clock_Globals.sch"
USING "flow_commands_enums_GAME.sch"
USING "charsheet_global_definitions.sch"
USING "player_ped_globals.sch"

// *****************************************************************************************
//
//		MISSION NAME	:	replay_globals.sch
//		AUTHOR			:	Andy Minghella
//		DESCRIPTION		:	Replay globals.
//		NOTES			:	Replay related non-saved globals
//
// *****************************************************************************************

CONST_INT MAX_RESTRICTED_VEHICLES 7
CONST_FLOAT REPLAY_LOAD_SCENE_SIZE 4500.0
CONST_FLOAT REPLAY_LOAD_SCENE_SIZE_INTERIOR 500.0
CONST_INT REPLAY_LOAD_SCENE_TIMEOUT 20000
CONST_INT REPOP_EARLY_OUT	1000
CONST_INT REPOP_MIN_WAIT	10

ENUM REPLAY_WARP_STAGE
	RWS_WAIT,
	RWS_START,
	RWS_START_LOAD,
	RWS_DO_WARP,
	RWS_POST_WARP,
	RWS_REPOPULATE,
	RWS_DONE
ENDENUM
REPLAY_WARP_STAGE g_eReplayWarpStage 
VECTOR g_eReplayWarpPos
FLOAT g_eReplayWarpHeading


// Replay Setup Progression Stages
ENUM g_eReplayStages
	RS_DO_BLUR,							// Show fail reason and fade the screen out

	RS_SCREEN_DEFAULT,					// Show default replay screen + wait for input.
	RS_SCREEN_FORCE_RESTART,			// Show force restart replay screen + wait for input.
	RS_SCREEN_RESTART_CONFIRMATION,		// Show restart confirmation screen + wait for input.
	RS_SCREEN_REJECT_CONFIRMATION,		// Show rejection confirmation screen + wait for input
	RS_SCREEN_SKIP_CONFIRMATION,		// Show skip confirmation screen + wait for input
	RS_SCREEN_FAIL_ONLY,				// Show fail only screen + wait for input.
	// if any new wait for screen stages are added, add them to IsReplayWaitingForReplayScreen()
	
	RS_ACCEPTING,						// the player has accepted replay: this state cleans up the mission
	RS_REJECTING,						// the player has rejected replay: this state cleans up the mission
	
	RS_ACTIVATE,						// Activation is in progress for a replay
	RS_ACTIVE,							// A replay is in progress
	RS_REJECTED,						// Player has rejected replay to get mission flow running properly again
	
	// debug only states
	#IF IS_DEBUG_BUILD
		RS_WAITING_FOR_FLOW,			// This state is entered when debug menu force cleanup is triggered
										// to prevent the mission currently being played from offering a replay just before flow setup begins
	#ENDIF
	
	RS_NOT_RUNNING,						// Replay controller has shutdown
	
	// Leave this at the bottom
	RS_NOT_REQUIRED	
	
	// If stages are added that are non-processing stages, add them to IS_REPLAY_BEING_PROCESSED()
ENDENUM

ENUM g_eReplayTypes
	// These replay types use the story mission launchers 
	// (add to DoesThisReplayTypeUseStoryMissionLauncher)
	REPLAY_TYPE_MISSION,
	REPLAY_TYPE_MISSION_FORCE_RESTART,
	REPLAY_TYPE_FAIL_SCREEN_ONLY,
	REPLAY_TYPE_PHONECALL_TRIGGER,		// story mission that triggers via phonecall the player can receive from anywhere 
	REPLAY_TYPE_MICHAEL_EVENT, 			// (like missions but no shitskips + warp player back to start pos if retry from beginning)
	
	// other replay types
	REPLAY_TYPE_MINIGAME,
	REPLAY_TYPE_RANDOM_CHARACTER
	
	// If you add a new replay type and you want it to use shitskips- add it to IsShitskipUsedInThisReplayType()
ENDENUM

ENUM REPLAY_BITS
	RB_FAIL_EFFECT_TRIGGERED,		// Has the replay controller triggered the fail effect
	RB_FAIL_OUT_EFFECT_TRIGGERED,	// Has the replay controller triggered the fail out effect
	RB_PAUSED_GAME,					// Has the replay controller paused the game?
	RB_PAUSED_FEED,					// Has the replay controller paused the feed?
	RB_DISABLED_SELECTOR,			// Has the replay controller disabled the character select wheel?
	RB_DISABLED_VEH_CONT,			// this gets set if the player fails a mission in an upside down vehicle
	RB_AUDIO_LOADED,				// has the fail screen audio loaded?
	RB_QUESTION_SETUP,				// used to only setup the question in the scaleform once (or when it changes)
	RB_BUTTONS_SETUP,				// used to only setup the button prompts in scaleform once (or when they change)
	RB_TEXT_SCALE_LOADED,			// used to reset the scaleform movie the once it has loaded (to prevent other text appearing)
	RB_WEAPON_SELECT_DISABLED,		// Has replay controller disabled weapon select
	RB_BLOCKED_DEATH_JINGLE,		// Has the replay controller blocked the death jingle?
	RB_REPLAY_SCENE_STARTED,		// Has the replay controller started the replay screen audio scene?
	RB_REPLAY_SETUP_STARTED,		// Is a replay currently being set up? (Used by the new warp system)
	RB_DO_VEHICLE_REPOP,			// Used in replay setup- do we want to wait for the vehicle repopulation to refill?
	RB_FROZEN_PLAYER,				// Has the replay controller frozen the player?
	RB_TRIGGERED_SLO_MO,			// Has the replay controller triggerd the slow motion effect?
	RB_DID_WE_EVER_SHITSKIP,		// Has the player ever shitskipped while playing this mission 
	RB_BLOCK_DAMAGE_OVERLAY,		// Has the replay controller blocked the damage overlay?
	RB_SET_INVINCIBLE,				// Has the replay controller set the player as invincible?
	RB_STARTED_TRANSITION,			// Have we triggered the message transition (moving failed up the screen)
	RB_DONE_TRANSITION,				// Have we completed the message transition (moving failed up the screen)
	RB_2ND_TEXT_SETUP,				// Have we updated the secondary fail text?
	RB_BLOCKED_LOAD_SCREEN,			// Have we blocked the loading screen
	RB_FULLY_INTERIOR,				// Is the checkpoint we are setting up in an interior with no view of outside? (used to stream smaller area)
	RB_BLOCK_RESPAWN				// Stops the respawn controller from taking over the 
ENDENUM

STRUCT g_structReplayVariables
	g_eReplayStages			replayStageID				// Replay Setup stage
	INT						replayCoreVarsIndex			// Index into the core vars array for this mission details (also used by RC missions to store the RC mission ID as an int)
	INT						replayStageReadOnly			// This is a copy of the g_replayMissionStage that gets read during the mission but not written to (writes go to the main variable)
	TEXT_LABEL_31			replayScriptName			// The name of the script this replay needs to fire off. Only used for oddjob or minigame replays.
	g_eReplayTypes			replayType					// An enum to determine whether the replay is configured for a mission, minigame, or RC mission.
	INT						iReplayInt[3]				// Array of integers that can be used by missions to ensure consistency when replaying the mission								
	SCALEFORM_INDEX			mFailTextScaleform			// the index to the scaleform movie used to draw the fail screen text
	SCALEFORM_INDEX			mFailButtonsScaleform		// the index to the scaleform movie used to draw the fail screen button prompts			
	INT 					iFailSoundID				// sound ID used to play / stop fail audio						
	INT 					iFailBedSoundID				// sound ID used to play / stop fail bed audio						
	INT						iReplayBits					// bitset used to store all replay controller bools
	enumCharacterList		eReplayRejectCharacter		// used if this mission must force the player to be a certain character when rejected
	VEHICLE_INDEX			mRestrictedVehicles[MAX_RESTRICTED_VEHICLES]	// we store vehicle indexes for restricted vehicles so we can make sure snapshot system doesnt recreate them when replay rejected
	FLOAT 					fTextTransitionTime			// used for timing start and end of text transition
	VECTOR					vVehicleVelocity			// used to restore the velocity when rejecting a replay and not warping
ENDSTRUCT

// Replay control globals
g_structReplayVariables g_replay	//The main struct

BOOL g_bLaunchMinigameReplay		// Informs all minigame controllers that a minigame replay is ready to launch. 
									// Launchers can check the value of g_replay.replayScriptName to see which launcher needs to run the replay.
									// Also used to get rampage launcher to behave correctly

BOOL g_bReplayDoRejectWarp 			// Used to set player position after a replay is rejected

BOOL g_bSkipFailScreen				// If true we skip over the fail screen completely (Used in agency heist missions if player rejects them via phonecall)

#IF IS_DEBUG_BUILD
	INT g_iSkipFailScreenTime		// Added to avoid instantly skipping fail screen when F-failing
	BOOL g_bDebugSkipReplayScreen	//Flag used by F/J skip system to skip mission fail screens.
	BOOL g_bDebugDisableFailScreen
	INT g_iMetricsTrackingStage		// added so we can have more metrics stages than replay stages in missions
#ENDIF

// Mid Mission Stage - mission specific
// Not part of the replay struct because this variable shouldn't get reset when 
// the replay control variables get reset otherwise valid data will get lost.
// Whether replay or not, mission should update this when the player passes a checkpoint
INT			g_replayMissionStage		= 0
