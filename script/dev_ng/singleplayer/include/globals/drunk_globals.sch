
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	drunk_globals.sch
//		AUTHOR			:	Keith / Alwyn
//		DESCRIPTION		:	Globals required to control drunk peds.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

CONST_INT	MAX_NUMBER_OF_DRUNK_REQUESTS	5
CONST_INT	MAX_NUMBER_OF_DRUNK_PEDS		16
CONST_INT	MAX_NUMBER_OF_DRUNK_NOTICES		16

CONST_INT	UNKNOWN_DRUNK_ARRAY_INDEX		-1
CONST_INT	NO_DRUNK_NOTICES				-1
CONST_INT	INACTIVE_DRUNK_TIMER			-2

CONST_INT	DRUNK_DEFAULT_TIMEOUT_msec		30000

CONST_INT	DRUNK_LEVEL_NOT_DRUNK			0
CONST_INT	DRUNK_LEVEL_CONSTANT			-1

// Unique Drunk Ped IDs
CONST_INT	NO_UNIQUE_DRUNK_PED_ID			-1
CONST_INT	UNIQUE_ID_CONTROL_SCRIPT		0	// For generic notices
CONST_INT	UNIQUE_ID_PUBLIC_FUNCTIONS		1	// Command from public functions - the control script converts these to UNIQUE_ID_CONTROL_SCRIPT so they get cleaned up next frame after the relevant 'drunk' scripts have had a chance to read them
CONST_INT	UNIQUE_ID_ALL_FOLLOWERS			2	// So a leader can inform all followers of his actions
CONST_INT	UNIQUE_ID_FIRST_UNIQUE_ID		100	// All new drunk peds will get a unique ID from this value upwards

INT			g_nextUniqueDrunkPedID			= UNIQUE_ID_FIRST_UNIQUE_ID
INT			g_drunkRequestsPending			= 0


BOOL		g_bHANGOVER_WAKEUP_BROWSER_OPEN_KILLSWITCH = FALSE

ENUM g_eDrunkStatus
	// Script Control
	DS_REQUEST_SCRIPT,		// Used when the 'drunk' script has been requested but is not yet loaded
	
	// Long-Term Target
	DSLT_PLAYER_CONTROLLED,	// Used when the ped is being controlled by player input
	DSLT_GET_INTO_VEHICLE,	// Used when an AI's leader is sitting in a vehicle
	DSLT_HANGOVER_WAKEUP,	// Used when the player ped has had too much to drink
	
	// Short-Term Task
	DSST_SWITCH_ON_RAGDOLL,	// Used when the ped may need to re-activate ragdoll
	DSST_WALK,				// Used when the ped needs to walk somewhere
	DSST_ENTER_VEHICLE,		// Used when the ped is trying to get in position to enter a vehicle
	DSST_ENTERING_VEHICLE,	// Used when the ped has been given a task to enter vehicle
	DSST_WARP_INTO_TAXI,	// Used when the ped failed to enter taxi normally (taxi doors are locked when player chooses destination)
	DSST_SHUFFLE_CARSEATS,	// Used when teh ped has been told to shuffle from teh passenger seat to the driver seat
	DSST_IN_VEHICLE,		// Used when the ped is sitting in a vehicle
	DSST_LEAVE_VEHICLE,		// Used when the ped has been given a task to leave a vehicle
	
	// Generic
	DS_IDLE,				// Used when the ped is just stumbling aimlessly
	
	// Leave this at the bottom
	DS_NO_STATUS
ENDENUM

ENUM g_eDrunkLevel
	DL_verydrunk,			//"move_m@drunk@verydrunk"
	DL_slightlydrunk,		//"move_m@drunk@slightlydrunk"
	DL_moderatedrunk,		//"move_m@drunk@moderatedrunk"
	
	// Leave this at the bottom
	DL_NO_LEVEL = -1
ENDENUM

// -----------------------------------------------------------------------------------------
//	Drunken Requests globals
// -----------------------------------------------------------------------------------------

STRUCT g_sDrunkRequests
	g_eDrunkStatus	status			// Current status of the drunk request
	PED_INDEX		ped				// The ped getting drunk
	INT				ragdoll_msec	// How long should the ped ragdoll for
	INT				overall_msec	// How long other drunk effects should remain active for
	VEHICLE_INDEX	vehicle			// The vehicle the ped either owns or is trying to get into
ENDSTRUCT

// The number of peds requesting to become drunk at the same time
g_sDrunkRequests	g_drunkRequests[MAX_NUMBER_OF_DRUNK_REQUESTS]



// -----------------------------------------------------------------------------------------
//	Drunken Ped globals
// -----------------------------------------------------------------------------------------

STRUCT g_sDrunkPeds
	INT				uniqueID	// Unique ID for this drunk ped (for posting and receiving notices)
	PED_INDEX		myPedIndex	// PED_INDEX for this drunk ped
	
	g_eDrunkLevel	eDrunkLevel	//drunken level (#1341523)
	INT				iAlcoholHit, iWeedHit
ENDSTRUCT

// Control Details for all drunk peds
g_sDrunkPeds		g_drunkPeds[MAX_NUMBER_OF_DRUNK_PEDS]


// -----------------------------------------------------------------------------------------
//	Drunken Noticeboard globals
// -----------------------------------------------------------------------------------------

ENUM g_eDrunkNoticeIDs
	DNID_STILL_DRUNK,		// Sent by 'drunk' each frame. Informs 'control' that drunk script for ped is still active.
	DNID_BECOME_SOBER,		// Sent by 'control' script. Informs 'drunk' script that it should terminate.
	DNID_EXTENDED_TIME,		// Sent by 'control' script. Informs 'drunk' script that effects other than ragdolling should last a bit longer.
	DNID_HIT_ALCOHOL,		// 
	DNID_HIT_WEED,			// 
	DNID_IN_VEHICLE,		// Sent by 'drunk' script. Informs 'followers' that this drunk ped in now in a vehicle.
	
	// Leave this at the bottom
	DNID_NO_DRUNK_NOTICE
ENDENUM


STRUCT g_sDrunkNoticeboard
	INT					poster		// notice poster ID
	INT					reader		// target ID for notice
	g_eDrunkNoticeIDs	notice		// message ID for notice
	PED_INDEX			pedIndex	// ped parameter for notice
	INT					intval		// int parameter for notice
	INT					inthit		// int parameter for number of hits
ENDSTRUCT

g_sDrunkNoticeboard		g_drunkNotices[MAX_NUMBER_OF_DRUNK_NOTICES]


// -----------------------------------------------------------------------------------------
//	Drunken Camera globals
// -----------------------------------------------------------------------------------------

FLOAT		g_drunkenCinematicCamMultiplier	= 1.0

BOOL			g_drunkCameraActive					= FALSE
CAMERA_INDEX	g_drunkCameraIndex					= NULL

INT				g_drunkCameraTimeout				= DRUNK_LEVEL_NOT_DRUNK			// Roughly when the effects should wear off
INT				g_drunkCameraTimeoutDuration		= DRUNK_DEFAULT_TIMEOUT_msec	// Amount of time to spend gradually timing out the drunk gameplay cam
FLOAT			g_drunkCameraMotionBlur				= 0.0							// Amount of blur to apply to the drunk gameplay cam
FLOAT			g_drunkCameraActualAmplitudeScalar	= 0.0							// Amount of shake amplitude to apply to the drunk gameplay cam
FLOAT			g_drunkCameraDesiredAmplitudeScalar	= 0.0							// Amount of shake amplitude to apply to the drunk gameplay cam
FLOAT			g_drunkCameraEffectStrength			= 1.0

FLOAT			g_drunkCameraTimeCycleModifier		= 0.0
TEXT_LABEL		g_drunkCameraTimeCycleName			= ""

TEXT_LABEL_63	g_drunkAudioSceneName				= ""
TEXT_LABEL		g_drunkAudioSceneScript				= ""

// -----------------------------------------------------------------------------------------
//	Drunken Player globals
// -----------------------------------------------------------------------------------------

BOOL		g_playerRequestedToBeDrunk	= FALSE			// TRUE when the request to make the player drunk appears, FALSE once the player IS drunk, or can't be made drunk
BOOL		g_playerIsDrunk				= FALSE			// TRUE when the player has become drunk, FALSE at all other times - including when the player has been requested to be drunk
BOOL		g_isPlayerDrunk				= FALSE			// TRUE when the player has become drunk, FALSE at all other times - including when the player has been requested to be drunk

INT			g_iPlayerDrunkCount			= 0

// -----------------------------------------------------------------------------------------
//	Drunken Player wakeup
// -----------------------------------------------------------------------------------------

ENUM g_eDrunkWakeupCutscene
	DWC_MICHAEL_mansion,
	DWC_MICHAEL_trailer,
	DWC_FRANKLIN_city,
	DWC_FRANKLIN_hills,
	DWC_TREVOR_trailer,
	DWC_TREVOR_beach,
	DWC_TREVOR_stripclub,
	
	// Leave this at the bottom
	DWC_NO_DRUNK_WAKEUP_CUTSCENE
ENDENUM



#IF IS_DEBUG_BUILD
// -----------------------------------------------------------------------------------------
//	Debug globals
// -----------------------------------------------------------------------------------------

WIDGET_GROUP_ID		g_wDrawDebugDrunkInfo

// Requests
INT		g_widgetDrunkRequestStatusAsInt[MAX_NUMBER_OF_DRUNK_REQUESTS]

// Peds
INT		g_widgetUniqueID[MAX_NUMBER_OF_DRUNK_PEDS]
INT		g_widgetLongTermStatusAsInt[MAX_NUMBER_OF_DRUNK_PEDS]
INT		g_widgetShortTermStatusAsInt[MAX_NUMBER_OF_DRUNK_PEDS]
INT		g_widgetDrunkLevelAsInt[MAX_NUMBER_OF_DRUNK_PEDS]
INT		g_widgetTimerA[MAX_NUMBER_OF_DRUNK_PEDS]
INT		g_widgetRagdolling[MAX_NUMBER_OF_DRUNK_PEDS]
INT		g_widgetOverall[MAX_NUMBER_OF_DRUNK_PEDS]
INT		g_widgetActivity[MAX_NUMBER_OF_DRUNK_PEDS]
INT		g_widgetFailsafe[MAX_NUMBER_OF_DRUNK_PEDS]
INT		g_widgetSteering[MAX_NUMBER_OF_DRUNK_PEDS]
INT		g_widgetAlcoholHitCount[MAX_NUMBER_OF_DRUNK_PEDS]
INT		g_widgetWeedHitCount[MAX_NUMBER_OF_DRUNK_PEDS]

BOOL	g_widgetMakePlayerDrunk_60s			= FALSE
BOOL	g_widgetMakePlayerDrunk_constant	= FALSE
BOOL	g_widgetExtendPlayerDrunkTime_30s	= FALSE
BOOL	g_widgetMakePlayerSober				= FALSE

BOOL	g_widgetPlayerTakesAlcoholHit		= FALSE
BOOL	g_widgetPlayerTakesWeedHit			= FALSE
BOOL	g_widgetPlayerHangoverWakeup		= FALSE

BOOL	g_widgetCreateDrunkPedOne			= FALSE
BOOL	g_widgetCreateDrunkPedTwo			= FALSE

PED_INDEX	g_widgetDrunkPedOne				= NULL
PED_INDEX	g_widgetDrunkPedTwo				= NULL

BOOL	g_widgetActivateDrunkCamera_60s		= FALSE
BOOL	g_widgetActivateDrunkCameraConstant	= FALSE
BOOL	g_widgetQuitDrunkCameraNow			= FALSE
BOOL	g_widgetQuitDrunkCameraGradual		= FALSE

BOOL	g_bDrawDebugDrunkInfo				= FALSE

BOOL	g_bIncrementDrunkModifier[5]

BOOL	g_isDrunkCameraActive				= FALSE

#ENDIF

#IF FEATURE_CASINO_HEIST
CONST_INT ARCADE_FORTUNE_GLOBAL_BS_SPECIAL_FORTUNE_ACTIVE		0
CONST_INT ARCADE_FORTUNE_GLOBAL_BS_WEATHER						1
CONST_INT ARCADE_FORTUNE_GLOBAL_BS_FEDS							2
CONST_INT ARCADE_FORTUNE_GLOBAL_BS_TRUE_LOVE					3
CONST_INT ARCADE_FORTUNE_GLOBAL_BS_BOUNTY						4
CONST_INT ARCADE_FORTUNE_GLOBAL_BS_MERRY_WEATHER				5
CONST_INT ARCADE_FORTUNE_GLOBAL_BS_MERRY_WEATHER_SENT			6
CONST_INT ARCADE_FORTUNE_GLOBAL_BS_MUGGER						7
CONST_INT ARCADE_FORTUNE_GLOBAL_BS_PEYOTE						8
CONST_INT ARCADE_FORTUNE_GLOBAL_BS_DRANK_PEYOTE					9
CONST_INT ARCADE_FORTUNE_GLOBAL_BS_AIRSTRIKE					10

INCIDENT_INDEX g_iFortuneIncident

INT g_iArcadeFortuneBS = 0

SCRIPT_TIMER g_stArcadeFortuneTimer
SCRIPT_TIMER g_stArcadeFortuneCooldownTimer
#ENDIF
