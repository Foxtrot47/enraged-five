// sea_race_globals.sch
// Enums
ENUM SEA_RACE_INDEX
	SEA_RACE_NONE = -1,
	
	SEA_RACE_EAST= 0,
	SEA_RACE_NORTH,
	SEA_RACE_CANYON,
	SEA_RACE_CITY,
	
	NUM_SEA_RACES
ENDENUM

// Constants
CONST_INT SEA_RACE_TIMEOUT 60000 // 1 minute timeout after interfering with initial scene

// Unsaved
BOOL g_bSeaRaceTimeout[NUM_SEA_RACES]
INT g_iSeaRaceCooldown[NUM_SEA_RACES]

// Saved
STRUCT SeaRaceSaved
	BOOL bFirstBlip	    // Used to determine whether to flash the initial sea race blip
	INT  iRaceWon       // Bitset used to mark the blip as gold on completion
	INT	 iRaceLeaveArea // Bitset to check whether we need to leave the race area before it's available again
ENDSTRUCT
