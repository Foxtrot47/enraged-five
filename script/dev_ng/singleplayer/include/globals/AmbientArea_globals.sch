// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	AmbientArea_globals.sch
//		AUTHOR			:	Ryan Paradis
//		DESCRIPTION		:	This is the globals file containing all single player globals 
//								for ambient areas, such as the prison airport, military 
//								base, golf, etc.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
USING "script_maths.sch"

/// PURPOSE: Enums for all ambient areas this controller will run in the game.
ENUM AMBIENT_AREAS
	eAMBAREA_ARMYBASE = 0,
	eAMBAREA_GOLF,
	eAMBAREA_PUTTING, 
	eAMBAREA_PRISON,
	eAMBAREA_STRIPCLUB,
	
	NUMBER_OF_AMB_AREAS
ENDENUM

/// PURPOSE: Flags that define how each amb area will behave.
ENUM AMBIENT_AREA_FLAGS
	eAmbientArea_NoFlags				= 0,
	
	eAmbientArea_AllowDuringMission		= BIT0,
	
	eAmbientArea_Running				= BIT1,
	eAmbientArea_ForceDeactivate		= BIT2
	
ENDENUM

/// PURPOSE: All of the data that each amb area contains.
STRUCT AMBIENT_AREA_DATA
	AMBIENT_AREA_FLAGS	ambAreaFlags		
	FLOAT				fRadius2
	VECTOR				vAmbAreaCenter
	THREADID			thisThread
	BOOL				bSuppressGateGuards
ENDSTRUCT



// ========================================= DEFINED GLOBALS
AMBIENT_AREA_DATA g_AmbientAreaData[NUMBER_OF_AMB_AREAS]
