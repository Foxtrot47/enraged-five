// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	timer_globals.sch
//		AUTHOR			:	Alwyn
//		DESCRIPTION		:	Globals required to control timer headers.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

/// PURPOSE:
///    A TIMER variable - Keeps track of whether it has been started/cancelled, 
///    or paused/unpaused, and has all time values in SECONDS.
STRUCT structTimer
	INT	  TimerBits
	FLOAT StartTime
	FLOAT PauseTime
ENDSTRUCT


CONST_INT TODS_SUNSET			21
CONST_INT TODS_SUNRISE			6
CONST_INT TODS_MORNING			8
CONST_INT TODS_EVENING			20
CONST_INT TODS_EARLY_EVENING	5

STRUCT structTimelapse
	TIMEOFDAY sStartTimeOfDay, currentTimeOfDay
	INT iTimelapseCut			//DO_TIMELAPSE()
	INT iTimeSkipStage			//SKIP_TO_TIME_DURING_SPLINE_CAMERA()
	CAMERA_INDEX splineCamera
	
	BOOL bSkipToNightTime = FALSE
	BOOL bCutsceneSkipped = FALSE

	INT iTimeWindowStart = TODS_SUNSET
	INT iTimeWindowEnd	 = TODS_SUNRISE
	
	INT iGameTimeHold
	
	INT iSplineStageSound
	INT iSecondsToSkipTotal
ENDSTRUCT


STRUCT structTimelapseSettings
	INT iLapseStartHour
	INT iLapseEndHour
	
	VECTOR vCamPos1
	VECTOR vCamRot1
	VECTOR vCamPos2
	VECTOR vCamRot2
	FLOAT fCamFOV
	INT iCamTime
	
	BOOL bSkipToNight
	INT iCustomBehaviour
	
	TEXT_LABEL tWeatherType
	TEXT_LABEL tCloudHat
ENDSTRUCT

