//╔═════════════════════════════════════════════════════════════════════════════╗
//║																				║
//║				Author: Ben Rollinson				Date: 05/07/2012		 	║
//║																				║
//╠═════════════════════════════════════════════════════════════════════════════╣
//║																				║
//║ 						Trigger Scene Globals								║
//║																				║
//║		All globals required to handle trigger scenes in the triggering system.	║
//║																				║
//╚═════════════════════════════════════════════════════════════════════════════╝

USING "rage_builtins.sch"
USING "charsheet_global_definitions.sch"
USING "load_queue_private.sch"

CONST_INT	MAX_TRIG_SCENE_VEHICLES	8
CONST_INT	MAX_TRIG_SCENE_PEDS		18
CONST_INT	MAX_TRIG_SCENE_OBJECTS	8
CONST_INT	MAX_TRIG_SCENE_ROPES	4

CONST_FLOAT	TS_DEFAULT_FRIEND_REJECT_DISTANCE	50.0
CONST_INT	TS_LOCK_IN_TIMEOUT_TIME				12000 //The time after the player locks in after which the mission will automatically trigger.


STRUCT TRIGGER_SCENE_ASSETS 
	VEHICLE_INDEX veh[MAX_TRIG_SCENE_VEHICLES]
	PED_INDEX ped[MAX_TRIG_SCENE_PEDS]
	OBJECT_INDEX object[MAX_TRIG_SCENE_OBJECTS]
	ROPE_INDEX rope[MAX_TRIG_SCENE_ROPES]
	
	REL_GROUP_HASH relGroup
	SCENARIO_BLOCKING_INDEX scenarioBlocking
	
	structPedsForConversation conversation // this doesn't need to be passed between scripts but is the best place for it to minimise globals
	
	LoadQueueMedium loadQueue
	
	BOOL 	flag			//Multi-purpose flag.
	INT		id				//Multi-purpose id.
	INT 	timer			//Multi-purpose timer.
	
	BLIP_INDEX blip
ENDSTRUCT


// A single assets struct to be used by all trigger scenes.
// Only possible assuming there are never two trigger scenes active at the same time with
// overlapping scene areas.
TRIGGER_SCENE_ASSETS 	g_sTriggerSceneAssets

//Trigger scene specific persistent variables.
BOOL					g_bLester1DoorKnocked
BOOL 					g_bAgencyP1HelpDisplayed = FALSE
BOOL 					g_bAgencyP1OnlyMissionLeft	= FALSE
TIMEOFDAY 				g_eAgencyP1TimeNextHelp = INVALID_TIMEOFDAY

//Jewel Prep 2A
TEXT_LABEL_63			g_JHP2A_string_WayPoint				= ""					// Waypoint recording that is used as the route
TIMEOFDAY				g_JHP2A_tod_NextTriggerTime			= INVALID_TIMEOFDAY		// Time of day used to enable/disable the blip
INT						g_JHP2A_iTriggerRunCount			= 0

//Jewel Prep 1b
TEXT_LABEL_63			g_JP1b_string_WayPoint				= ""					// Waypoint recording that is used as the route
TIMEOFDAY				g_JP1b_tod_NextTriggerTime			= INVALID_TIMEOFDAY		// Time of day used to enable/disable the blip
INT						g_JP1b_iTriggerRunCount				= 0

//Rural prep 1
TEXT_LABEL_63			g_RHP_string_WayPoint				= ""					// Waypoint recording that is used as the route
TIMEOFDAY				g_RHP_tod_NextTriggerTime			= INVALID_TIMEOFDAY		// Time of day used to enable/disable the blip
INT						g_RHP_iTriggerRunCount				= 0

//Finale Heist 2A/2B
BOOL					g_FH2_bTimelapseTriggeredInside		= FALSE
BOOL					g_FHP3_FirstGauntWarn				= TRUE

TIMEOFDAY				g_FHPC_tod_NextRemindTime			= INVALID_TIMEOFDAY		// Time of day used to remind the player of Gaunlet task


// Function pointer types for trigger scenes.
TYPEDEF PROC ResetTriggerScene()
TYPEDEF PROC RequestTriggerSceneAssets()
TYPEDEF PROC ReleaseTriggerSceneAssets()
TYPEDEF	FUNC BOOL HaveTriggerSceneAssetsLoaded()
TYPEDEF PROC CreateTriggerScene()
TYPEDEF PROC ReleaseTriggerScene()
TYPEDEF PROC DeleteTriggerScene()
TYPEDEF PROC UpdateTriggerScene()
TYPEDEF PROC AmbientUpdateTriggerScene()
TYPEDEF FUNC BOOL HasTriggerSceneBeenTriggered()
TYPEDEF FUNC BOOL HasTriggerSceneBeenDisrupted()
TYPEDEF FUNC BOOL IsTriggerSceneBlocked()


// Struct representing a single trigger scene instance.
STRUCT TRIGGER_SCENE
	SP_MISSIONS eMission			// Singleplay mission this scene is bound to.
	
	INT iStateBitset				// A bitfield containing all state and setting flags for this trigger scene. See TriggerSceneSettings for indexes.
	FLOAT fLoadDistance				// The distance from the scene's blip at which the scene streams in.
	FLOAT fUnloadDistance			// The distance from the scene's blip at which the scene streams out.
	FLOAT fFriendRejectDistance		// The distance from the scene's blip at which friends who can't trigger the mission get rejected.
	FLOAT fBattleBuddyCallDistance	// The distance from the scene's blip at which the player can call in battle buddies.
	INT iFriendsToAcceptBitset		// Bitset containing which friends should be allowed to trigger this mission with the player.
ENDSTRUCT


//Trigger scene control functions/procedure pointers.
STRUCT TRIGGER_SCENE_POINTERS
	BOOL							bSceneBound
	ResetTriggerScene				RESET_TRIGGER_SCENE
	RequestTriggerSceneAssets		REQUEST_TRIGGER_SCENE_ASSETS 		
	ReleaseTriggerSceneAssets		RELEASE_TRIGGER_SCENE_ASSETS 		
	HaveTriggerSceneAssetsLoaded	HAVE_TRIGGER_SCENE_ASSETS_LOADED 	
	CreateTriggerScene				CREATE_TRIGGER_SCENE 	
	ReleaseTriggerScene				RELEASE_TRIGGER_SCENE
	DeleteTriggerScene				DELETE_TRIGGER_SCENE 	
	UpdateTriggerScene				UPDATE_TRIGGER_SCENE
	AmbientUpdateTriggerScene		AMBIENT_UPDATE_TRIGGER_SCENE
	HasTriggerSceneBeenTriggered	HAS_TRIGGER_SCENE_BEEN_TRIGGERED 	
	HasTriggerSceneBeenDisrupted	HAS_TRIGGER_SCENE_BEEN_DISRUPTED
	IsTriggerSceneBlocked			IS_TRIGGER_SCENE_BLOCKED
ENDSTRUCT


BOOL g_bBlockTriggerSceneLoadDuringSwitch = FALSE
BOOL g_bPlayerLockedInToTrigger = FALSE
