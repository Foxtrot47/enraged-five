// specialPed_globals.sch
USING "script_maths.sch"

ENUM SPECIAL_PEDS
	SP_NONE			= 0,
	
	SP_ANDYMOON 	= BIT0,
	SP_BAYGOR 		= BIT1,
	SP_BILLBINDER	= BIT2,
	SP_CLINTON		= BIT3,
	SP_GRIFF 		= BIT4,
	SP_IMPOTENTRAGE	= BIT5,
	SP_JANE			= BIT6,
	SP_JEROME		= BIT7,
	SP_JESSE		= BIT8,
	SP_MANI			= BIT9,
	SP_MIME			= BIT10,	//removed from specials
	SP_PAMELADRAKE	= BIT11,
	SP_SUPERHERO	= BIT12,
	SP_ZOMBIE		= BIT13,
	SP_DANCER		= BIT14,
	SP_TONYA		= BIT15
ENDENUM

STRUCT SpecialPedDataSaved
	SPECIAL_PEDS	ePedsKilled
ENDSTRUCT

ENUM specialPedList
	ANDYMOON,
	BAYGOR,
	BILL,
	CLINTON,
	GRIFF,
	JANE, 
	JEROME,
	JESSE,
	MANI,
	MIME,						//removed from specials
	PAMELA,
	SUPERHERO,
	ZOMBIE,
	TONYA,
	PED_COUNT
ENDENUM

INT	conversation_offset[PED_COUNT]
BOOL andyToldYouToSeeNakedOldPeople = FALSE
