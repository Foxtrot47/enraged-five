//╒═════════════════════════════════════════════════════════════════════════════╕
//│					 Communication Controller Global Header						│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│		AUTHOR:			Ben Rollinson											│
//│		DATE:			13/10/10												│
//│		DESCRIPTION: 	All global definitions required by the communication	│
//│						controller system.										│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "cellphone_globals.sch"
USING "vector_id_globals.sch"
USING "email_generated_globals.sch"
USING "flow_commands_enums_game.sch"
USING "script_maths.sch"
USING "code_control_globals.sch"
USING "candidate_enums.sch"



//═══════════════════════════════════════════════════════════════════════════════
//════════════════════════════╡ Global Constants ╞═══════════════════════════════
//═══════════════════════════════════════════════════════════════════════════════

CONST_INT CC_MAX_QUEUED_CALLS	9
CONST_INT CC_MAX_QUEUED_TEXTS	8
CONST_INT CC_MAX_QUEUED_EMAILS	10
CONST_INT CC_MAX_MISSED_CALLS	4
CONST_INT CC_MAX_CHAT_CALLS		30
CONST_INT CC_MAX_SENT_TEXTS		3

CONST_INT	CC_GLOBAL_DELAY_BETWEEN_COMMS		20000	//Time to wait between communications going to the player.
CONST_INT	CC_GLOBAL_DELAY_POST_MISSION		30000	//Time to wait after a mission has been played before allowing communications.
CONST_INT	CC_CHARACTER_DELAY_BETWEEN_COMMS	20000	//Time to wait between communications going to the player from the same character.
CONST_INT	CC_DELAY_POST_AMBIENT_SWITCH		8000	//Time to wait after an off-mission switch before allowing communications.
CONST_INT	CC_END_OF_MISSION_QUEUE_TIME		6000	//Standard delay time for an end of mission call after a mission passes.

//═══════════════════════════════════════════════════════════════════════════════
//══════════════════════════════╡ Global Enums ╞═════════════════════════════════
//═══════════════════════════════════════════════════════════════════════════════

HASH_ENUM CC_CommID
	//Normal calls.
	CALL_AGENCY_2_TRIGGER,
	CALL_AGENCY_P_M_DONE,
	CALL_AGENCY_P_F_DONE,
	CALL_B_AGENCY_P_M_READY,
	CALL_B_AGENCY_P_M_WAIT,
	CALL_B_AGENCY_P_F_READY,
	CALL_B_AGENCY_P_F_WAIT,
//	CALL_AGENCY_3_M_UNLOCK,
//	CALL_AGENCY_3_F_UNLOCK,
	CALL_AGENCY_3A_DONE,
	CALL_AGENCY_3B_DONE,
	CALL_ARM2_UNLOCK,
	CALL_CARDMG_ARM2_UNLOCK,
	CALL_ARM3_DONE,
	CALL_ASS1_UNLOCK,
	CALL_ASS1_DONE,
	CALL_CARS1_UNLOCK,
	CALL_CARS4_F_DONE,
	CALL_CHIN1_UNLOCK,
	CALL_DOCKS_POST_PLAN,
	CALL_DOCKS_POST_PLANALT,
	CALL_B_DOCKS_POST_PLAN_FLYING,
	CALL_DOCKS_PREP1_DONE_SOME,
	CALL_DOCKS_PREP1_DONE_ALL,
	CALL_DOCKS_2_T_UNLOCK,
	CALL_EXILE_1_END,
	CALL_EXILE_CALL1,
	CALL_EXILE_CALL2,
	CALL_EXILE_AMANDA,
	CALL_EXILE_STEVE,
	CALL_EXILE_M_MARTIN1,
	CALL_EXILE_M_MARTIN2,
	CALL_EXILE_M_MARTIN3,
	CALL_EXILE_M_MARTIN4,
	CALL_EXILE_T_MARTIN1,
	CALL_EXILE_T_MARTIN2,
	CALL_EXILE_T_MARTIN3,
	CALL_EXILE_T_MARTIN4,
	CALL_EPSILON_DESERT_NEAR,
	CALL_EPSILON_DESERT_DONE,
	CALL_EPSILON8_STOLE,
	CALL_EPSILON8_DONE,
	CALL_FAM4_DONE,
	CALL_FAM5_DONE,
	CALL_FIB3_M_TRIGGER,
	CALL_FIB3_T_TRIGGER,
	CALL_FIB4I_DONE,
	CALL_FIB4_P1_M_DONE,
	CALL_FIB4_P1_F_DONE,
	CALL_FIB4_P1_T_DONE,
	CALL_FIB4_P2_M_DONE,
	CALL_FIB4_P2_F_DONE,
	CALL_FIB4_P2_T_DONE,
	CALL_FIB4_P4_M_DONE,
	CALL_FIB4_P4_F_DONE,
	CALL_FIB4_P4_T_DONE,
	CALL_FIB4_P5_M_DONE,
	CALL_FIB4_P5_F_DONE,
	CALL_FIB4_P5_T_DONE,
	CALL_FIB4_PREPS_DONE,
	CALL_FIB4_PREPS_DONE_WAIT,
	CALL_FIB4_M_UNLOCK,
	CALL_FIB4_F_UNLOCK,
	CALL_FIB4_T_UNLOCK,
	CALL_FIB4_GO_TIME,
	CALL_FIN_CHOICE_1A,
	CALL_FIN_CHOICE_1B,
	CALL_FIN_CHOICE_2A,
	CALL_FIN_CHOICE_2B,
	CALL_FIN_CHOICE_3,
	CALL_FIN_TANISHA,
	CALL_FIN_AMANDA,
	CALL_FIN_DAVE,
	CALL_FIN_DEVIN,
	CALL_FIN_LAMAR,
	CALL_FIN_MDEAD1,
	CALL_FIN_MDEAD2,
	CALL_FIN_MDEAD3,
	CALL_FIN_STEVE,
	CALL_FIN_TDEAD1,
	CALL_FIN_TDEAD2,	
	CALL_FINH_PA_M_DONE,
	CALL_FINH_PA_F_DONE,
	CALL_FINH_PA_T_DONE,
	CALL_FINH_PB_M_DONE,
	CALL_FINH_PB_F_DONE,
	CALL_FINH_PB_T_DONE,
	CALL_FINH_PC1_M_DONE,	
	CALL_FINH_PC1_F_DONE,
	CALL_FINH_PC1_T_DONE,
	CALL_FINH_PC2_M_DONE,	
	CALL_FINH_PC2_F_DONE,
	CALL_FINH_PC2_T_DONE,
	CALL_FINH_PC_M_DONE,
	CALL_FINH_PC_F_DONE,
	CALL_FINH_PC_T_DONE,
	CALL_FINH_PD_M_DONE,
	CALL_FINH_PD_F_DONE,
	CALL_FINH_PD_T_DONE,
	CALL_FINH_PE_M_DONE,
	CALL_FINH_PE_F_DONE,
	CALL_FINH_PE_T_DONE,
	CALL_B_FINH_P_READY,
	CALL_B_FINH_P_PREPS,
	CALL_B_FINH_P_STING,
	CALL_B_FINH_P_GETAW,
	CALL_B_FINH_P_DRILL,
	CALL_B_FINH_P_TRAIN,
	CALL_B_FINH_P_GAUNT,
	CALL_FRAN_CHOP_UNLOCK,
	CALL_JEWEL_SETUP_UNLOCK,
	CALL_JEWEL_POST_PLAN,
	CALL_JEWEL_PREP1A_DONE_UNLOCK_P2A,
	CALL_JEWEL_PREP1A_DONE_NEED_P2A,
	CALL_JEWEL_PREP2A_DONE,
	CALL_M_JHEIST_PAY,
	CALL_F_JHEIST_PAY,
	CALL_HACKER_1,
	CALL_HACKER_2,
	CALL_HACKER_3,
	CALL_HACKER_4,
	CALL_MIKE_1,
	CALL_MIKE_3,
	CALL_MICHAEL4_M_UNLOCK,
	CALL_MIKE_4,
	CALL_PAP3A_DONE,
	CALL_PATRICIA_1,
	CALL_PATRICIA_2,
	CALL_PATRICIA_3,
	CALL_RURAL_PREP_DONE1,
	CALL_B_RURAL_PREP_DONE1_WAIT,
	CALL_RURAL_PREP_DONE2,
	CALL_B_RURAL_PREP_DONE2_WAIT,
	CALL_RURAL_HEIST_UNLOCK,
	CALL_SHRINK1_UNLOCK,
	CALL_SOLOMON1_DONE,
	CALL_SOLOMON3_DONE,
	CALL_DIVING_DONE,
	CALL_SONAR_COLLECT_DONE,
	CALL_TRV5_LESCALL,
	CALL_RE_BURIAL_M_DONE,
	CALL_RE_BURIAL_F_DONE,
	CALL_RE_BURIAL_T_DONE,
	CALL_RACES_DONE,
	CALL_PROP_TOWING,
	CALL_PROP_GOLF_T,
	CALL_PROP_GOLF_M,
	CALL_PROP_GOLF_F,
	CALL_PROP_CINEMA_M,
	CALL_PROP_TAXI_F,
	
	//Chat calls.
	CHAT_AMA01,
	CHAT_AMA02,
	CHAT_AMA03,
	CHAT_AMA04,
	CHAT_CLE1_1,
	CHAT_CLE1_2,
	CHAT_CLE2_1,
	CHAT_CLE2_2,
	CHAT_CLE3_1,
	CHAT_DAVE01,
	CHAT_DEV03,
	CHAT_DEV04,
	CHAT_DEV05,
	CHAT_DEV06,
	CHAT_DEV07,
	CHAT_DEV08,
	CHAT_JIM05,
	CHAT_JIM06,
	CHAT_JIM07,
	CHAT_JIM08,
	CHAT_JIM09,
	CHAT_JIM10,
	CHAT_JIM11,
	CHAT_JIM12,
	CHAT_JIM13,
	CHAT_JIM14,
	CHAT_JIM15,
	CHAT_JIM16,
	CHAT_JIM17,
	CHAT_JIM18,
	CHAT_JIM19,
	CHAT_JIM20,
	CHAT_JIM21,
	CHAT_JIM22,
	CHAT_JIM23,
	CHAT_JIM24,
	CHAT_LAM06,
	CHAT_LAM08,
	CHAT_LAM09,
	CHAT_LAM10,
	CHAT_LAM11,
	CHAT_LAM12,
	CHAT_LAM13,
	CHAT_LAM14,
	CHAT_LAM15,
	CHAT_LAM16,
	CHAT_LAM17,
	CHAT_LAM18,
	CHAT_LAM19,
	CHAT_LAM20,
	CHAT_LAM21,
	CHAT_RON01,
	CHAT_RON02,
	CHAT_RON03,
	CHAT_SOL01,
	CHAT_SOL02,
	CHAT_SOL03,
	CHAT_SOL04,
	CHAT_STR01,
	CHAT_STR02,
	CHAT_STR03,
	CHAT_STR04,
	CHAT_STR05,
	CHAT_TEXL1,
	CHAT_TEXL2,
	CHAT_TEXL3,
	CHAT_TEXL4,
	CHAT_TEXL5,
	CHAT_TEXL6,
	CHAT_TEXL7,
	CHAT_TNOT,
	CHAT_TRA01,
	CHAT_TRA02,
	CHAT_TRA03,
	CHAT_WADE01,
	CHAT_WADE02,
	CHAT_WADE03,
	CHAT_WADE04,
	CHAT_MAR01,
	CHAT_MAR02,
	CHAT_MAR03,
	CHAT_CHG1,
	CHAT_CHG2,
	CHAT_CHG3,
	CHAT_HOSP,

	//Calls with questions.
	QCALL_ME_AMANDA,
	QCALL_ME_JIMMY,
	QCALL_ME_TRACEY,
	QCALL_PROP_TAXI_TAKE_IT_EASY,	
	QCALL_PROP_TAXI_NEED_EXCITEMENT,
	QCALL_PROP_TAXI_DEADLINE,		
	QCALL_PROP_TAXI_FOLLOW_THAT_CAR,	
	QCALL_PROP_TAXI_GOT_YOUR_BACK,		
	QCALL_PROP_TAXI_TAKE_TO_BEST,		
	QCALL_PROP_TAXI_CUT_YOU_IN,		
	QCALL_PROP_TAXI_GOT_YOU_NOW,		
 	QCALL_PROP_TAXI_CLOWN_CAR,			
	
	//Family griefing calls.
	CALL_FAM_M_GRIEF_GENERAL,
	CALL_FAM_M_GRIEF_FIRING_ROCKETS,
	CALL_FAM_M_GRIEF_STOLE_CAR,
	CALL_FAM_M_GRIEF_SHOOTING_HOUSE,

	CALL_FAM_F_GRIEF_GENERAL,
	CALL_FAM_F_GRIEF_FIRING_ROCKETS,
	CALL_FAM_F_GRIEF_STOLE_CAR_c,
	CALL_FAM_F_GRIEF_STOLE_CAR_b,
	CALL_FAM_F_GRIEF_SHOOTING_HOUSE,

	CALL_FAM_T_GRIEF_GENERAL,
	CALL_FAM_T_GRIEF_FIRING_ROCKETS,
	CALL_FAM_T_GRIEF_STOLE_CAR,
	CALL_FAM_T_GRIEF_SHOOTING_HOUSE,

	//Text messages.
	TEXT_AGENCY_1_UNLOCK,
	TEXT_AGENCY_2_UNLOCK,
	TEXT_AGENCY_P_UNLOCK,
	TEXT_AGENCY_GETA_REMINDER,
	TEXT_AGENCY_3B_UNLOCK,
	TEXT_ASS1_UNLOCK_MISSED,
	TEXT_ASS_FIN_A,
	TEXT_ASS_FIN_B,
	TEXT_ARM2_END,
	TEXT_ARM3_BAGGER_UNLOCK,
	TEXT_ARM3_DONE_MISSED,
	TEXT_CAR1_UNLOCK,
	TEXT_CARS2_DONE,
	TEXT_CAR3_F_REM,
	TEXT_CAR3_MT_REM,
	TEXT_CARS4_T_DONE_MISSED,
	TEXT_CHOP_UNLOCK,
	TEXT_CITY_RON_CHINESE,
	TEXT_CREW_CHEF_UNLOCK,
	TEXT_DOCKS_F_SHOOT,
	TEXT_DOCKS_P1_UNLOCK,
	TEXT_DOCKS_P2B_UNLOCK,
	TEXT_DOCKS_P2B_REMINDER,
	TEXT_DOCKS_MIKE_FLY,
	TEXT_DOCKS_2_MF_UNLOCK,
	TEXT_EXILE2_UNLOCK,
	TEXT_EXILE2_OSCAR,
	TEXT_EXILE_WADE_CHINESE,
	TEXT_EXILE_HUNTER_PIE,
	TEXT_EXILE_PAT_LOVE,
	TEXT_JIM_CAR_BACK,
	TEXT_FAMILY1_END,
	TEXT_FAMILY_DADDYS_GIRL,
	TEXT_FAMILY5_END,
	TEXT_FIB4I_UNLOCK,
	TEXT_FIB4_P3_M_REMINDER,
	TEXT_FIB4_P3_FT_REMINDER,
	TEXT_FIB4_M_UNLOCK,
	TEXT_FIB4_FT_UNLOCK,
	TEXT_FIB4_F_ASS1,
	TEXT_FIB4_MT_ASS1,
	TEXT_FINH_PA_UNLOCK,
	TEXT_FINH_PB_UNLOCK,
	TEXT_FINH_PD_UNLOCK,
	TEXT_FINH_GETA_REMINDER,
	TEXT_FINH_KILLM,
	TEXT_FINH_KILLT,
	TEXT_FORSALESIGNS_DONE,
	TEXT_FRAN0_END,
	TEXT_FRAN1_END,	
	TEXT_JEWEL_P1A_UNLOCK,
	TEXT_JEWEL_FRANK_PREP,
	TEXT_JEWEL_P2A_UNLOCK,
	TEXT_JEWEL_P1B_UNLOCK,
	TEXT_JEWEL_P1B_HOSTAGE_END,
	TEXT_JEWEL_2_DONE,
	TEXT_JOSH_1,
	TEXT_MARTIN_1_UNLOCK_MISSED,
	TEXT_MARTIN1_TREV_SILENT,
	TEXT_ME_AMANDA_FAIL,
	TEXT_ME_MANADA_FAIL2,
	TEXT_ME_JIMMY_FAIL,
	TEXT_ME_TRACEY_FAIL,
	TEXT_MICHAEL4_M_UNLOCK,
	TEXT_MICHAEL4_F_UNLOCK,
	TEXT_PILOTS_UNLOCK,
	TEXT_RC_BARRY3_UNLOCK,
	TEXT_RC_BARRY4_UNLOCK,
	TEXT_RC_EPSILON6_UNLOCK,
	TEXT_RC_EXTR2_UNLOCK,
	TEXT_RC_EXTR3_UNLOCK,
	TEXT_RC_HUNT1_UNLOCK,
	TEXT_RC_PAP_3_UNLOCK,
	TEXT_RC_TONYA_3_UNLOCK,
	TEXT_RC_TONYA_4_UNLOCK,
	TEXT_RE_BURIAL_END,
	TEXT_RBH_UNLOCK,
	TEXT_RBH_PREP_UNLOCK,
	TEXT_SOL2_END,
	TEXT_SRANGE_UNLOCK,
	TEXT_SHRINK3_UNLOCK,
	TEXT_SHRINK4_UNLOCK,
	TEXT_STREET_RACE_AIRPORT,
	TEXT_STREET_RACE_FREEWAY,
	TEXT_STREET_RACE_CANALS,
	TEXT_TENNIS_UNLOCK,
	TEXT_TENNIS_UNLOCK_POST_FAM3,
	TEXT_TREV_LOST_HANGER,
	TEXT_UFOPARTS_DONE,
	TEXT_PROP_UNAV_TOWING,
	TEXT_MARTIN_THREAT,
	TEXT_MARTIN_CASH_RECIEVED,
	TEXT_TREVOR_BLAZER3_UNLOCK,
	TEXT_ABIGAIL_MISSED,
	TEXT_HAO1,
	TEXT_PAP1_WILDLIFE_UNLOCK,
	TEXT_MONKEY_CAR_UNLOCK,
	TEXT_MONKEY_MOSAIC_LAMAR,
	TEXT_COUNTRY_RACE_UNLOCK,
	
	
	//Composite bases.
	TEXT_SEXT,
	TEXT_FRIEND,
	TEXT_FRIEND_GRIEF_MICHAEL,
	TEXT_FRIEND_GRIEF_FRANKLIN,
	TEXT_FRIEND_GRIEF_TREVOR,
	TEXT_FRIEND_GRIEF_LAMAR,
	TEXT_FRIEND_GRIEF_JIMMY,
	TEXT_FRIEND_GRIEF_AMANDA,
	TEXT_PROPERTY,
	TEXT_BS_PREPC,
	
	//Family griefing texts.
	TEXT_FAM_GRIEF_MICHAEL,
	TEXT_FAM_GRIEF_FRANKLIN,
	TEXT_FAM_GRIEF_TREVOR,
	
	TEXT_FAM_HOSPITALBILL,

	//Emails.
	EMAIL_CARMOD_UNLOCK,
	EMAIL_HEIST_PAY,
	EMAIL_OFFROAD_UNLOCK,
	EMAIL_SEARACE_M_UNLOCK,
	EMAIL_SEARACE_F_UNLOCK,
	EMAIL_SEARACE_T_UNLOCK,
	EMAIL_STOCKMARKET,
	EMAIL_WEAPON_STOCK_1,
	EMAIL_WEAPON_STOCK_2,
	EMAIL_WEAPON_STOCK_3,
	EMAIL_WEAPON_STOCK_4,
	EMAIL_WEAPON_STOCK_5,
	EMAIL_WEAPON_STOCK_6,
	EMAIL_WEAPON_STOCK_7,
	EMAIL_WEAPON_STOCK_8,
	EMAIL_WEAPON_STOCK_9,
	EMAIL_FINH_GOOD_HACKER,
	EMAIL_FINH_MED_HACKER,
	EMAIL_FINH_BAD_HACKER,
	EMAIL_RECARTHEFT2_T_PASS,
	EMAIL_RECARTHEFT2_M_PASS,
	EMAIL_RECARTHEFT2_F_PASS,
	EMAIL_EPSILON1_DONATE,
	EMAIL_EPSILON2_DONATE,
	EMAIL_EPSILON4_DONATE,
	EMAIL_DRFRIEDLANDER_EMAIL_A1,
	EMAIL_DRFRIEDLANDER_EMAIL_END,
	EMAIL_TRACEY_EMAIL_EXILE,
	EMAIL_DAVE_EMAIL_FBI1,
	EMAIL_DAVE_EMAIL_EG,
	EMAIL_DAVE_EMAIL_EGBB,
	EMAIL_AMANDA_EMAIL_FAM6,
	EMAIL_AMANDA_EMAIL_EGKM,
	EMAIL_MERRYWETHER_EMAIL_EG,
	EMAIL_RON_EMAIL_EG,
	EMAIL_RON_EMAIL_FAM4,
	EMAIL_TANISHA_EMAIL_NG,
	EMAIL_DENISE_EMAIL_FNH,
	EMAIL_LAMAR_EMAIL_FNK0,
	EMAIL_LAMAR_EMAIL_EGKT,
	EMAIL_LAMAR_EMAIL_EGKM,
	EMAIL_LAMAR_EMAIL_EGSB,
	EMAIL_TREVOR_EMAIL_EGKM,
	EMAIL_MICHAEL_EMAIL_EGKT,
	EMAIL_MICHAEL_EMAIL_EGSB,
	EMAIL_JIMMY_EMAIL_FAM4,
	EMAIL_PATRICIA_EMAIL_MIC4,
	EMAIL_REHO_BMS_EMAIL,
	EMAIL_BITH_AA_EMAIL_M,
	EMAIL_BITH_AA_EMAIL_F,
	EMAIL_BITH_AA_EMAIL_T,
	EMAIL_RC_NIG_1_UNLOCK,
	EMAIL_BRAD_EMAIL_TRV1,
	EMAIL_BRAD_EMAIL_FIB1,
	EMAIL_BRAD_EMAIL_MIC1,
	EMAIL_RON_CULT_TREV,
	EMAIL_PRO_TOWING_IMPOUND_F,
	EMAIL_PRO_ARMS_TRAFFICKING_T,
	EMAIL_PRO_SONAR_COLLECTIONS_M,
	EMAIL_PRO_SONAR_COLLECTIONS_T,
	EMAIL_PRO_SONAR_COLLECTIONS_F,
	EMAIL_PRO_CAR_MOD_SHOP_F,
	EMAIL_PRO_CINEMA_VINEWOOD_M,
	EMAIL_PRO_CINEMA_DOWNTOWN_M,
	EMAIL_PRO_CAR_SCRAP_YARD_M,
	EMAIL_PRO_CAR_SCRAP_YARD_T,
	EMAIL_PRO_CAR_SCRAP_YARD_F,
	EMAIL_PRO_WEED_SHOP_F,
	EMAIL_PRO_BAR_TEQUILALA_M,
	EMAIL_PRO_BAR_TEQUILALA_T,
	EMAIL_PRO_BAR_TEQUILALA_F,
	EMAIL_PRO_BAR_PITCHERS_M,
	EMAIL_PRO_BAR_PITCHERS_T,
	EMAIL_PRO_BAR_PITCHERS_F,
	EMAIL_PRO_BAR_HEN_HOUSE_M,
	EMAIL_PRO_BAR_HEN_HOUSE_T,
	EMAIL_PRO_BAR_HEN_HOUSE_F,
	EMAIL_PRO_BAR_HOOKIES_M,
	EMAIL_PRO_BAR_HOOKIES_F,
	EMAIL_COTM_ADRIAN_1,
	EMAIL_COTM_ADRIAN_2,
	EMAIL_COTM_ADRIAN_3,
	EMAIL_COTM_ADRIAN_4,
	EMAIL_WILDLIFE_PHOTOGRAPHY,
	
	//Christmas 2 DLC weapons.
	EMAIL_WEAPON_STOCK_10,	//Proximity mine
	EMAIL_WEAPON_STOCK_11,	//Homing launcher

	//Luxe DLC weapons.
	EMAIL_WEAPON_STOCK_12,	//Knuckle duster
	EMAIL_WEAPON_STOCK_13,	//Combat PDW
	EMAIL_WEAPON_STOCK_14,	//Marksman pistol
	
	//Lowrider DLC weapons.
	EMAIL_WEAPON_STOCK_15,	//Machete
	EMAIL_WEAPON_STOCK_16,	//Machine Pistol
	
	//==== Agent T comms ===
	
	//calls
	CALL_CLF_TEST01,
	//calls with Q
	QCALL_CLF_TEST01,	
	//texts
	TEXT_CLF_TEST01,
	//chat
	CHAT_CLF_TEST01,
	//email
	EMAIL_CLF_TEST01,
	
	//==== Agent T comms ===
	
	//calls
	CALL_NRM_TEST01,
	//calls with Q
	QCALL_NRM_TEST01,	
	//texts
	TEXT_NRM_TEST01,
	//chat
	CHAT_NRM_TEST01,
	//email
	EMAIL_NRM_TEST01,
	
	COMM_NONE = -1
	
ENDENUM

ENUM CC_QuestionCallResponse
	RESP_ME_AMANDA_YES,
	RESP_ME_AMANDA_NO,
	RESP_ME_JIMMY_YES,
	RESP_ME_JIMMY_NO,
	RESP_ME_TRACEY_YES,
	RESP_ME_TRACEY_NO,
	RESP_PROP_TAXI_M_YES,
	RESP_PROP_TAXI_M_NO,
	RESP_PROP_TAXI_F_YES,
	RESP_PROP_TAXI_F_NO,
	RESP_PROP_TAXI_T_YES,
	RESP_PROP_TAXI_T_NO,
	RESP_PROP_TAXI_TAKE_IT_EASY_YES,	
	RESP_PROP_TAXI_NEED_EXCITEMENT_YES,	
	RESP_PROP_TAXI_DEADLINE_YES,			
	RESP_PROP_TAXI_FOLLOW_THAT_CAR_YES,	
	RESP_PROP_TAXI_GOT_YOUR_BACK_YES,			
	RESP_PROP_TAXI_TAKE_TO_BEST_YES,			
	RESP_PROP_TAXI_CUT_YOU_IN_YES,			
	RESP_PROP_TAXI_GOT_YOU_NOW_YES,			
 	RESP_PROP_TAXI_CLOWN_CAR_YES,				
	
	
	RESP_MAX,
	RESP_NONE
ENDENUM

ENUM CC_TextPart
	//Sext character composition parts.
	TPART_SEXT_JULIET,
	TPART_SEXT_NIKKI,
	TPART_SEXT_SAPPHIRE,
	TPART_SEXT_INFERNUS,
	TPART_SEXT_LIZ,
	TPART_SEXT_HITCHER,
	
	//Sext type composition parts.
	TPART_SEXT_STUP,
	TPART_SEXT_RUDE,
	TPART_SEXT_1,
	TPART_SEXT_2,
	TPART_SEXT_NEED,
	
	//Grief type parts.
	TPART_GRIEF_FIRING_ROCKETS,
	TPART_GRIEF_CREEP,
	TPART_GRIEF_STOLE_CAR_P,
	TPART_GRIEF_STOLE_CAR_W,
	TPART_GRIEF_STOLE_CAR_D,
	TPART_GRIEF_SET_FIRE,
	TPART_GRIEF_SHOOTING_HOUSE,
	TPART_GRIEF_GRENADE_HOUSE,
	
	//Grief random parts.
	TPART_GRIEF_ZERO,
	TPART_GRIEF_ONE,
	TPART_GRIEF_TWO,
	TPART_GRIEF_THREE,
	TPART_GRIEF_FOUR,
	
	//Grief family hospital.
	TPART_FHOS_M_SON,
	TPART_FHOS_M_DAU,
	TPART_FHOS_M_WIF,
	TPART_FHOS_M_MAI,
	TPART_FHOS_M_GAR,
	
	TPART_FHOS_F_AUN,
	
	TPART_FHOS_T_RON,
	TPART_FHOS_T_WIF,
	TPART_FHOS_T_FLO,
	
	//Property type parts.
	TPART_PROP_NOTI1,
	TPART_PROP_NOTI2,
	TPART_PROP_NOTI3,
	TPART_PROP_NOTI4,
	TPART_PROP_NOTI5,
	TPART_PROP_NOTI6,
	TPART_PROP_PASS1,
	TPART_PROP_PASS2,
	TPART_PROP_PASS3,
	TPART_PROP_PASS4,
	TPART_PROP_PASS5,
	TPART_PROP_PASS6,
	TPART_PROP_FAIL1,
	TPART_PROP_FAIL2,
	TPART_PROP_FAIL3,
	TPART_PROP_FAIL4,
	TPART_PROP_FAIL5,
	TPART_PROP_FAIL6,
	TPART_PROP_FAIL7,
	TPART_PROP_FAIL8,
	
	//Property mission parts.
	TPART_PROP_WEED_DEL,
	TPART_PROP_BAR_DEL,
	TPART_PROP_BAR_GANG,
	TPART_PROP_BAR_STLN,
	TPART_PROP_BAR_PAP,
	TPART_PROP_SCRAP_DEF,
	TPART_PROP_TAXI,
	TPART_PROP_CIN_PLANE,
	
	//friends
	TPART_FRND_MIKE,			
	TPART_FRND_FRANK,			
	TPART_FRND_TREVOR,			
	TPART_FRND_LAMAR,			
	TPART_FRND_JIMMY,		
	TPART_FRND_AMANDA,	
	
	//situation for Friends
	TPART_FRND_SIT_LOSTA,	
	TPART_FRND_SIT_LOSTB,		 
	TPART_FRND_SIT_LATEA,		 
	TPART_FRND_SIT_LATEB,		 
	TPART_FRND_SIT_HOSPA,		 
	TPART_FRND_SIT_HOSPB,		
	TPART_FRND_SIT_DIEDA,		 
	TPART_FRND_SIT_DIEDB,		
	TPART_FRND_SIT_BUSTA,		 
	TPART_FRND_SIT_BUSTB,	
	
	//Big Score Prep C
	//Hackers
	TPART_BS_GH,
	TPART_BS_AH,
	TPART_BS_BH,
	
	//Mission variations.
	TPART_BS_C1,
	TPART_BS_C2,
	TPART_BS_C3,
	
	TPART_MAX,
	TPART_NONE = -1
ENDENUM

ENUM CC_CommunicationForm
	CF_CALL,
	CF_QUESTION_CALL,
	CF_TEXT,
	CF_EMAIL,
	
	CF_NO_COMM_FORMS,
	CF_ERROR = -1
ENDENUM

ENUM CC_CommunicationPriority
	CPR_VERY_LOW = 0,
	CPR_LOW,
	CPR_MEDIUM,
	CPR_MEDIUM_HIGH,
	CPR_HIGH,
	CPR_VERY_HIGH,
	
	CPR_NO_PRIORITY_LEVELS,
	CPR_ERROR
ENDENUM

ENUM CC_CommunicationType
	CT_END_OF_MISSION = 0,
	CT_FLOW,
	CT_FLOW_URGENT,
	CT_FRIEND,
	CT_FRIEND_urgent,
	CT_MINIGAME,
	CT_AMBIENT,
	CT_ON_MISSION,
	
	CT_NONE,
	CT_ERROR
ENDENUM

ENUM CC_CommunicationStatus
	CS_IN_PROGRESS = 0,
	CS_INACTIVE,
	CS_WAITING_FOR_GLOBAL_DELAY,
	CS_WAITING_FOR_CHARACTER_DELAY,
	CS_WAITING_FOR_QUEUE_TIMER,
	CS_WAITING_FOR_HIGHER_PRIORITY,
	
	CS_ERROR
ENDENUM

ENUM CC_CallMissedResponse
	CMR_REQUEUE = 0,
	CMR_SEND_TEXT,
	CMR_REMOVE
ENDENUM

// Call setting bit indexes.
CONST_INT 	COMM_FLAG_FROM_CHAR_IS_PLAYER		BIT0
CONST_INT 	COMM_FLAG_HAS_QUESTION				BIT1
CONST_INT 	COMM_FLAG_UNLOCKS_MISSION			BIT2
CONST_INT 	COMM_FLAG_TRIGGERS_MISSION			BIT3
CONST_INT 	COMM_FLAG_TODS_SKIP_TIMER			BIT4
CONST_INT 	COMM_FLAG_ADD_CONTACT				BIT5
CONST_INT 	COMM_FLAG_DONT_SAVE					BIT6
CONST_INT 	COMM_FLAG_CALL_IS_QUICK				BIT7
CONST_INT 	COMM_FLAG_CALL_IS_MISSED			BIT8
CONST_INT 	COMM_FLAG_CALL_IS_CONFERENCE		BIT9
CONST_INT 	COMM_FLAG_CALL_REQUEUE_ON_MISS		BIT10
CONST_INT 	COMM_FLAG_CALL_TEXT_ON_MISS			BIT11
CONST_INT 	COMM_FLAG_CALL_BLOCK_HANGUP			BIT12
CONST_INT 	COMM_FLAG_CALL_BRANCHED				BIT13
CONST_INT 	COMM_FLAG_CALL_ADD_TEMP_CONTACT		BIT14
//CONST_INT COMM_FLAG_CALL_REMOVE_TEMP_CONTACT	BIT15 //Private system use only.
CONST_INT 	COMM_FLAG_TXTMSG_NOT_CRITICAL		BIT16
CONST_INT 	COMM_FLAG_TXTMSG_LOCKED				BIT17
CONST_INT 	COMM_FLAG_TXTMSG_NO_SILENT			BIT18
CONST_INT 	COMM_FLAG_TXTMSG_FORCE_SILENT		BIT19
CONST_INT 	COMM_FLAG_TXTMSG_AWAITING_RESP		BIT20
CONST_INT 	COMM_FLAG_INGORE_GLOBAL_DELAY		BIT21


CONST_INT 	COMM_BIT_FROM_CHAR_IS_PLAYER		0
CONST_INT 	COMM_BIT_HAS_QUESTION				1
CONST_INT 	COMM_BIT_UNLOCKS_MISSION			2
CONST_INT 	COMM_BIT_TRIGGERS_MISSION			3
CONST_INT 	COMM_BIT_TODS_SKIP_TIMER			4
CONST_INT 	COMM_BIT_ADD_CONTACT				5
CONST_INT 	COMM_BIT_DONT_SAVE					6
CONST_INT 	COMM_BIT_CALL_IS_QUICK				7
CONST_INT 	COMM_BIT_CALL_IS_MISSED				8
CONST_INT 	COMM_BIT_CALL_IS_CONFERENCE			9
CONST_INT 	COMM_BIT_CALL_REQUEUE_ON_MISS		10
CONST_INT 	COMM_BIT_CALL_TEXT_ON_MISS			11
CONST_INT 	COMM_BIT_CALL_BLOCK_HANGUP			12
CONST_INT 	COMM_BIT_CALL_BRANCHED				13
CONST_INT 	COMM_BIT_CALL_ADD_TEMP_CONTACT		14
CONST_INT 	COMM_BIT_CALL_REMOVE_TEMP_CONTACT	15
CONST_INT 	COMM_BIT_TXTMSG_NOT_CRITICAL		16
CONST_INT 	COMM_BIT_TXTMSG_LOCKED				17
CONST_INT 	COMM_BIT_TXTMSG_NO_SILENT			18
CONST_INT 	COMM_BIT_TXTMSG_FORCE_SILENT		19
CONST_INT 	COMM_BIT_TXTMSG_AWAITING_RESP		20
CONST_INT 	COMM_BIT_INGORE_GLOBAL_DELAY		21



//═══════════════════════════════════════════════════════════════════════════════
//═════════════════════════════╡ Global Structs ╞════════════════════════════════
//═══════════════════════════════════════════════════════════════════════════════

//Shared data struct for phonecalls and text messages.
STRUCT CC_CommData
	CC_CommID					eID						// A unique ID that can be used to reference this communication.
	INT							iSettings				// A bitset contain setting flags for this communication.
	INT							iPlayerCharBitset		// A bitset containing which player characters this communication is for.
	CC_CommunicationPriority	ePriority				// An ENUM that denotes the priority of this communication.
	INT							iQueueTime				// The time at which this communication is allowed to trigger.
	INT							iRequeueTime			// The base time to add when requeuing this communication.
	enumCharacterList 			eNPCCharacter			// An ENUM pointing to the NPC character who is involved in this communication.
	VectorID					eRestrictedAreaID		// A vector ID that defines an area in the world that the player must not be in for the communication to trigger.
	CC_CodeID					eExecuteOnCompleteID	// A code ID that defines a block of script that should be executed when the communication is completed sucessfully.
	FLOW_CHECK_IDS				eSendCheck				// A custom check that must pass before this communication is sent.
ENDSTRUCT

//Phonecall specific data struct.
STRUCT CC_CallData
	CC_CommData				sCommData				// The generic communication data for this phonecall. 
	CC_CommID				eCommExtra				// The ID of first part of additional data for this call.
	CC_CommID				eCommExtra2				// The ID of second part of additional data for this call.
	CC_QuestionCallResponse	eYesResponse
	CC_QuestionCallResponse eNoResponse
	INT						iSpeakerID				// The speaker ID for the character in the conversation that is not a main character.
ENDSTRUCT

//Text message specific data struct.
STRUCT CC_TextMessageData
	CC_CommData					sCommData					// The generic communication data for this text message. 
	CC_TextPart					ePart1						// An ID for the first composite part that should make up the text label for this message.
	CC_TextPart					ePart2						// An ID for the second composite part that should make up the text label for this message.
	INT							iFailCount					// How many times has this text failed to send?
	enumTxtMsgCanCallSender 	WhichCanCallSenderStatus 	// Can player call back the char texting them
ENDSTRUCT

//Email specific data struct.
STRUCT CC_EmailData
	CC_CommData					sCommData				// The generic communication data for this text message. 
ENDSTRUCT

//Global control struct for tracking the state of peds involved in any communcation controller phonecall conversations.
structPedsForConversation g_sPedsForConversation


//═══════════════════════════════════════════════════════════════════════════════
//════════════════════════════╡ Global Variables ╞═══════════════════════════════
//═══════════════════════════════════════════════════════════════════════════════

INT g_iCallInProgress = -1							//The queue index of any call that is currently in progress. -1 if no calls are in progress.
INT g_iCommsCandidateID = NO_CANDIDATE_ID			//CandidateID that the comms controller can use to secure the mission flag for a mission
													//that triggers off a phonecall.

//Communication timers.
BOOL g_bPauseCommsQueues = FALSE			//Should the communication controller queues be halted at the moment?
BOOL g_bPauseCommsQueuesThisFrame = FALSE	//Should the communication controller queues be halted for this frame only?
INT g_iGlobalWaitTime 						//The time at which next communication is allowed to be sent to the player.
INT g_iCharWaitTime[MAX_CHARACTERS_MP] 		//The time at which the next communication from a specific character can be sent to the player.


//═══════════════════════════════════════════════════════════════════════════════
//═══════════════════════════╡ Globals to be saved ╞═════════════════════════════
//═══════════════════════════════════════════════════════════════════════════════

STRUCT CommsControlSaved

	//The queued calls list and counter.
	CC_CallData	sQueuedCalls[CC_MAX_QUEUED_CALLS]
	INT iNoQueuedCalls
	
	//The queued missed calls list and counter.
	CC_CallData	sMissedCalls[CC_MAX_MISSED_CALLS]
	INT iNoMissedCalls
	
	//The registered chat calls list and counter.
	CC_CallData	sChatCalls[CC_MAX_CHAT_CALLS]
	INT iNoChatCalls

	//The queued text messages list and counter.
	CC_TextMessageData sQueuedTexts[CC_MAX_QUEUED_TEXTS]
	INT iNoQueuedTexts
	
	//The queued email list and counter.
	CC_EmailData sQueuedEmails[CC_MAX_QUEUED_EMAILS]
	INT iNoQueuedEmails
	
	//Sent text queue.
	CC_TextMessageData sSentTexts[CC_MAX_SENT_TEXTS]
	INT iNoSentTexts

	//Last call data.
	CC_CommID eLastCompletedCall = COMM_NONE
	BOOL bLastCallAnswered
	BOOL bLastCallHadResponse
	BOOL bLastCallResponse
	
	//Last text data.
	CC_CommID eLastCompletedText = COMM_NONE
	BOOL bLastTextHadResponse
	BOOL bLastTextResponse
	
	//Last email data.
	CC_CommID eLastCompletedEmail = COMM_NONE
	
	//Playable Character communication priority levels.
	CC_CommunicationPriority eCharacterPriorityLevel[3] //One priority level for each playable character.
	
	INT iExileWarningBitset
	
	INT iCommsGameTime
	
ENDSTRUCT
