//////////////////////////////////////////////////////////////////////////////////////////////
//																							//
//		MISSION NAME	:	flightschool_globals.sch										//
//		AUTHOR			:	Alwyn Roberts												    //
//		DESCRIPTION		:	Global data saved between instances of the Pilot School club	//
//																							//
//////////////////////////////////////////////////////////////////////////////////////////////

#IF IS_DEBUG_BUILD
	CONST_INT iDEBUG_USE_XML	11	//comment out when XML stuff is no longer needed
	#IF NOT DEFINED iDEBUG_USE_XML
		CONST_INT iDEBUG_USE_XML	0
	#ENDIF

#ENDIF

ENUM PILOT_SCHOOL_STATUS_ENUM
	PSS_LOCKED = 0,
	PSS_UNLOCKED,
	PSS_NEW,	
	
	NUMBER_OF_PILOT_SCHOOL_STATUSES
ENDENUM

ENUM PILOT_SCHOOL_GOAL_BITS
	FSG_0_timeTaken 			= 0,		//BIT0,
	FSG_1_damageTaken 			= 1,		//BIT1,
	FSG_2_distanceFromTarget	= 2,		//BIT2,
	FSG_3_checkpointsPassed		= 3,		//BIT3,
	FSG_4_formationCompletion	= 4,		//BIT4,
	FSG_5_avgCheckpointHeight	= 5,		//BIT5,
	FSG_6_targetsDestroyed		= 6,		//BIT6,
	NUMBER_OF_PILOT_SCHOOL_GOALS	=  7	//BIT7
ENDENUM

ENUM PILOT_SCHOOL_CLASSES_ENUM
	PSC_Takeoff=0,
	PSC_Landing,
	PSC_Inverted,
	PSC_Knifing,
	PSC_FlyLow,
	PSC_DaringLanding,
	PSC_loopTheLoop,
	PSC_heliCourse,
	PSC_heliSpeedRun,
	PSC_parachuteOntoTarget,
	PSC_chuteOntoMovingTarg,
	PSC_planeCourse,

	NUMBER_OF_PILOT_SCHOOL_CLASSES
ENDENUM


ENUM PILOT_SCHOOL_MEDAL
	PS_NONE,
	PS_BRONZE,
	PS_SILVER,
	PS_GOLD
ENDENUM

PILOT_SCHOOL_CLASSES_ENUM	g_current_selected_PilotSchool_class					= PSC_Takeoff // deprecate this for dlc

//===============================================================================
//============================| Data to be saved |===============================
//===============================================================================

STRUCT PS_PLAYER_DATA_STRUCT
	FLOAT						ElapsedTime = -1.0
	FLOAT						LastElapsedTime = -1.0
	FLOAT						LastLandingDistance = -1.0
	FLOAT						LandingDistance = -1.0
	FLOAT						Multiplier = 1.0
	INT							CheckpointCount = -1
	FLOAT						FormationTimer = -1.0
	BOOL						HasPassedLesson = FALSE
	PILOT_SCHOOL_MEDAL			eMedal = PS_NONE
	PILOT_SCHOOL_MEDAL			eLastMedal = PS_NONE
ENDSTRUCT

STRUCT FlightSchoolSaved
	PS_PLAYER_DATA_STRUCT		PlayerData[NUMBER_OF_PILOT_SCHOOL_CLASSES]
ENDSTRUCT



