USING "email_generated_globals.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	email_globals.sc
//		AUTHOR			:	Ak
//		DESCRIPTION		:	Datatypes/data for the email system
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

CONST_INT MAX_DYNAMIC_EMAIL_THREAD_PARTICIPANTS 4
CONST_INT MAX_DYNAMIC_EMAIL_SUBSTRINGS 10	// ADDED BY CC

BOOL g_bDebugEmailDisplay

BOOL g_bEmailSystemPaused

BOOL g_bEmailSystemUpdated //used by system to tell ui when an email comes in

INT g_iUnreadEmailsSP0
INT g_iUnreadEmailsSP1
INT g_iUnreadEmailsSP2

//consts
CONST_INT MAX_INBOX_LOGGED_MAILS_THREADS 16 // sizes for rolling trackers

CONST_INT MAX_THREAD_LOG_LENGTH 9  


CONST_INT MAX_EMAIL_FEED_BUFFER_ENTRIES 3


INT g_iEmailFeedBuffer[MAX_EMAIL_FEED_BUFFER_ENTRIES]
INT g_iEmailFeedBufferCaret = 0



STRUCT EMAIL_RESPONSE
	INT iResponseNameTag//the short version tag for picking this response
	INT iResponseMail//the index of the email response sent when this is picked
	BOOL bResponseEndsThread//does this response being picked cause the thread to finish?
	INT iResponseFiresThread//-1 means triggers nothing, otherwise attempt to start EMAIL_THREAD in threadlist
ENDSTRUCT

STRUCT EMAIL
	//title
	INT iTitleTag
	//content
	INT iContentTag
	
	EMAILER_ENUMS eFrom
	EMAILER_ENUMS eTo
	
	//response details
	
	INT iResponses//0 no pickable responses
	EMAIL_RESPONSE Responses[MAX_RESPONSES_IN_EMAIL]

	BOOL bEmailBlocksThread//does this email cause thread block until current email has been responded to?
	INT iDelay
ENDSTRUCT

EMAIL g_AllEmails[TOTAL_EMAILS_IN_GAME] //all the stored possible emails in the game

STRUCT EMAIL_THREAD
	BOOL bActive
	
	BOOL bEnded
	
	INT iParticipants
	EMAILER_ENUMS Participants[MAX_EMAIL_THREAD_PARTICIPANTS] //the people being mailed as part of this thread, if the current message is not TO or FROM them (within the email) they are marked as CC

	//set in initial
	INT ThreadMails[MAX_THREAD_LENGTH]
	INT iTotalThreadMails//how many mails are in the log
	INT iCurrentlyAt//how far through the thread path the mail is
	
	//updated as progress
	INT ThreadEmailLog[MAX_THREAD_LOG_LENGTH]
	INT iMailsInLog//how much of the thread log has been used
	
	INT iDelay
	INT iDelayPassed
	BOOL bBLockedUntilResponse//this thread is stopped, waiting for a player response to iCurrentlyAt
ENDSTRUCT

EMAIL_THREAD g_AllEmailThreads[TOTAL_EMAIL_THREADS_IN_GAME] // all the threads in the game

STRUCT EMAIL_INBOX
	INT iTotalMails
	
	INT EmailsLogIndex[MAX_INBOX_LOGGED_MAILS_THREADS]//Thread path log indices for the last LOGLENGTH emails recieved // used to figure out which part of the thread was sent
	INT ThreadIndex[MAX_INBOX_LOGGED_MAILS_THREADS]//The threads that initiated the last LOGLENGTH emails recieved
	//if dynamic then thread index is the buffer ID of the thread that its in
	
	//interal settings
	BOOL HasFiredResponse[MAX_INBOX_LOGGED_MAILS_THREADS]
	INT PickedResponseIndex[MAX_INBOX_LOGGED_MAILS_THREADS]
	BOOL HasBeenViewed[MAX_INBOX_LOGGED_MAILS_THREADS]
	BOOL IsDynamic[MAX_INBOX_LOGGED_MAILS_THREADS]
	
	BOOL bDelete[MAX_INBOX_LOGGED_MAILS_THREADS]
	
ENDSTRUCT

EMAIL_INBOX g_Inboxes[TOTAL_INBOXES]




////////////////////////////////////////////////////////////////////////////
///    Dynamic email system data
///    
///    
///////////////////////////////////////////////////////////////////////////

ENUM DYNAMIC_EMAIL_THREAD_NAMES
	DYNAMIC_THREAD_UNSET,
	TEST_DYNAMIC_THREAD,
	DYNAMIC_THREAD_EPSILON3,
	DYNAMIC_THREAD_EPSILON_DONATE,
	DYNAMIC_THREAD_EPSILON_ROBES,
	DYNAMIC_THREAD_CAR_BUY,
	DYNAMIC_THREAD_MILITARY_BUY,
	DYNAMIC_THREAD_PLANE_BUY,
	DYNAMIC_THREAD_BOAT_BUY,
	DYNAMIC_THREAD_BIKE_BUY,
	DYNAMIC_THREAD_AUTO_BUY,
	DYNAMIC_THREAD_BOUGHT_HELIPAD_MIKE,
	DYNAMIC_THREAD_BOUGHT_MARINA_MIKE,
	DYNAMIC_THREAD_BOUGHT_HANGAR_MIKE,
	DYNAMIC_THREAD_BOUGHT_HELIPAD_FRANKLIN,
	DYNAMIC_THREAD_BOUGHT_MARINA_FRANKLIN,
	DYNAMIC_THREAD_BOUGHT_HANGAR_FRANKLIN,
	DYNAMIC_THREAD_BOUGHT_HELIPAD_TREVOR,
	DYNAMIC_THREAD_BOUGHT_MARINA_TREVOR,
	DYNAMIC_THREAD_BOUGHT_HANGAR_TREVOR,
	DYNAMIC_THREAD_BOUGHT_GARAGE_MIKE,
	DYNAMIC_THREAD_BOUGHT_GARAGE_FRANKLIN,
	DYNAMIC_THREAD_BOUGHT_GARAGE_TREVOR,
	//DYNAMIC_THREAD_PROPERTIES_FINANCE_REPORT,
	DYNAMIC_THREAD_PROPERTIES_FINANCE_REPORT_MIKE,
	DYNAMIC_THREAD_PROPERTIES_FINANCE_REPORT_TREVOR,
	DYNAMIC_THREAD_PROPERTIES_FINANCE_REPORT_FRANKLIN,
	DYNAMIC_THREAD_BAIL_BONDS,
	DYNAMIC_THREAD_EPSILON_TRACT,
	DYNAMIC_THREAD_WILDLIFE_PHOTOGRAPHY
ENDENUM

//TYPEDEF FUNC BOOL DynamicEmailContentOverrideCallback(INT threadRegistrationID, INT logIndex, EMAIL_MESSAGE_ENUMS mailEnum)
/*
FUNC BOOL TestDefaultDynamicEmailContentOverrideCallbackFunction(INT threadRegistrationID, EMAIL_MESSAGE_ENUMS mailEnum)

	
    BEGIN_TEXT_COMMAND_SCALEFORM_STRING(body)
	INT rep = 0
	REPEAT g_DynamicEmailThreadBuffers[tind].emails[emaillogindex].iOverrideAdditional rep
    	ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_DynamicEmailThreadBuffers[tind].emails[emaillogindex].additional[rep])
	ENDREPEAT				
    END_TEXT_COMMAND_SCALEFORM_STRING()
	

	RETURN TRUE // return false if you want the system to update the content itself
ENDFUNC
*/
//BOOL g_bEmailSystemContentCallback = FALSE
//DynamicEmailContentOverrideCallback g_EmailContentCallback

//dynamic email structure
STRUCT DYNAMIC_EMAIL_INSTANCE//this struct is saved so keep data and changes to a minimum
	//content
	//uses instance of email?
	EMAIL_MESSAGE_ENUMS sourceEmail//take content and responses from this email
	
	
	//BOOL bHasContentCallback
	//DynamicEmailContentOverrideCallback contentCallback
	//
	BOOL bOverrideContent
	TEXT_LABEL_15 content
	INT iOverrideAdditional
	TEXT_LABEL_15 additional[MAX_DYNAMIC_EMAIL_SUBSTRINGS]	// 5 - CC
	
ENDSTRUCT




CONST_INT DYNAMIC_EMAIL_THREAD_BUFFERS 7
CONST_INT DYNAMIC_EMAIL_THREAD_MAX_LENGTH 4


INT g_iEmailWithLoadedTextureIndex = -1

//BOOL g_bDyanmicEmailContentOverridePending = FALSE
//DynamicEmailContentOverrideCallback g_nextDynamicCallbackHandle





//dynamic thread buffers - copy is saved
STRUCT DYNAMIC_THREAD_BUFFER
	BOOL bBufferCritical
	INT registrationID
	
	//thread status
	DYNAMIC_EMAIL_THREAD_NAMES belongsTo
	
	
	
	INT iParticipants
	EMAILER_ENUMS Participants[MAX_DYNAMIC_EMAIL_THREAD_PARTICIPANTS] //the people being mailed as part of this thread, if the current message is not TO or FROM them (within the email) they are marked as CC

	INT iProgress
	DYNAMIC_EMAIL_INSTANCE emails[DYNAMIC_EMAIL_THREAD_MAX_LENGTH]
	
ENDSTRUCT

DYNAMIC_THREAD_BUFFER g_DynamicEmailThreadBuffers[DYNAMIC_EMAIL_THREAD_BUFFERS]



//dynamic email queue - this is saved.

STRUCT DYNAMIC_THREAD_EMAIL_PENDING
	DYNAMIC_EMAIL_THREAD_NAMES eTargetThread
	EMAIL_MESSAGE_ENUMS eEmailID
	INT iInGameHoursBeforeTrigger
	INT iPendingIndex
	
	BOOL bNotSent
	
	BOOL bOverrideContent
	TEXT_LABEL_15 content
	INT iOverrideAdditional
	TEXT_LABEL_15 additional[10]	// 5 - CC 
	
	//tracking
	INT RegIDOfTarget//0 = unused // released when parent thread goes out of buffer or when successfully queried complete
ENDSTRUCT

CONST_INT MAX_DYNAMIC_EMAILS_PENDING 8
INT g_iDynamicEmailsPending = 0
//INT g_savedGlobals.sEmailData.g_PendingEmailIDGenerator = 0
DYNAMIC_THREAD_EMAIL_PENDING g_DynamicEmailsPending[MAX_DYNAMIC_EMAILS_PENDING]


/*
Error: [script] Verifyf(m_NumberOfSubStringTextLabels < MaxNumberOfSubStringsInPrintCommand) 
FAILED: SCRIPT: Script Name = appemail : Program Counter = 7238 :
CScriptTextConstruction::AddSubStringTextLabel - Too many substrings.
The maximum is 4 (from Updating script appemail)

*/

///////////////////////////////////////////////////////////////////////////////////
///    
///    Email system saved data past this point
///    
///    
///    
///
///////////////////////////////////////////////////////////////////////////////////


//note, look at SHOP_ROBBERIES_LAUNCH_DATA

//Saved email system data


STRUCT EmailSavedInbox
			//INT iTotalMails
			//INT EmailsLogIndex[MAX_INBOX_LOGGED_MAILS_THREADS]//Thread path log indices for the last LOGLENGTH emails recieved // used to figure out which part of the thread was sent
			//INT ThreadIndex[MAX_INBOX_LOGGED_MAILS_THREADS]//The threads that initiated the last LOGLENGTH emails recieved
			//BOOL HasFiredResponse[MAX_INBOX_LOGGED_MAILS_THREADS]
			//INT PickedResponseIndex[MAX_INBOX_LOGGED_MAILS_THREADS]
			//BOOL HasBeenViewed[MAX_INBOX_LOGGED_MAILS_THREADS]
			//BOOL IsDynamic[MAX_INBOX_LOGGED_MAILS_THREADS]
			
		INT InboxTotalMailsIn
		
		INT InboxEmailLogIndices[MAX_INBOX_LOGGED_MAILS_THREADS]
		INT InboxThreadIndex[MAX_INBOX_LOGGED_MAILS_THREADS]
		BOOL InboxHasFiredResponses[MAX_INBOX_LOGGED_MAILS_THREADS]
		INT InboxPickedResponseIndices[MAX_INBOX_LOGGED_MAILS_THREADS]
		BOOL InboxHasBeenViewed[MAX_INBOX_LOGGED_MAILS_THREADS]
		BOOL InboxIsDynamic[MAX_INBOX_LOGGED_MAILS_THREADS]
ENDSTRUCT



STRUCT EmailSavedDynamicThread

		//DYNAMIC_EMAIL_THREAD_NAMES belongsTo //used to recover the ID
		//bool iscritical
		//participant ints MAX_DYNAMIC_EMAIL_THREAD_PARTICIPANTS
		
		//emails DYNAMIC_EMAIL_THREAD_MAX_LENGTH

				
				
		DYNAMIC_EMAIL_THREAD_NAMES BelongsTo
		INT AssignedID
		
		BOOL isCritical
		
		INT iParticipants
		EMAILER_ENUMS participantIDs[MAX_DYNAMIC_EMAIL_THREAD_PARTICIPANTS]
		
		
		
		INT iProgress
		DYNAMIC_EMAIL_INSTANCE DynamicEmailData[DYNAMIC_EMAIL_THREAD_MAX_LENGTH]
		
				
ENDSTRUCT


STRUCT EmailSavedStaticThread
		//currently at/progress
		//bool active
		//bool ended
		//BOOL bBLockedUntilResponse//this thread is stopped, waiting for a player response to iCurrentlyAt
		BOOL active
		BOOL ended
		BOOL blockedForResponse
		INT iCurrentlyAt
		//INT iDelayPassed
		
		//INT ThreadMails[MAX_THREAD_LENGTH]
		INT ThreadEmailLog[MAX_THREAD_LOG_LENGTH]
ENDSTRUCT




STRUCT EmailDataSaved
	//inbox contents dump
	//most recent index also
	EmailSavedInbox InboxSnapshots[TOTAL_INBOXES]		
			
	
	//active dynamic thread states
	INT g_PendingEmailIDGenerator
	EmailSavedDynamicThread DynamicThreadBufferSnapshots[DYNAMIC_EMAIL_THREAD_BUFFERS]
				
		
				
	//static thread progress states TOTAL_EMAIL_THREADS_IN_GAME
	EmailSavedStaticThread StaticThreadSnapshots[TOTAL_EMAIL_THREADS_IN_GAME]
		
	
		
	//TODO, store the responses to static threads

	//Dynamic pending buffers
	DYNAMIC_THREAD_EMAIL_PENDING DynamicPendingMails[MAX_DYNAMIC_EMAILS_PENDING]
	
	
ENDSTRUCT



