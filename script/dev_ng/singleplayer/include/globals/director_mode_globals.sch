//╒═════════════════════════════════════════════════════════════════════════════╕
//│						 Director Mode Global Header							│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│		DATE:			25/11/14												│
//│		DESCRIPTION: 	All global definitions required by the director mode	│
//│						system.													│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "globals_sp_dlc_feature_flags.sch"

#IF FEATURE_SP_DLC_DIRECTOR_MODE

BOOL g_bLaunchDirectorMode = FALSE				// Used by any script that wants to launch Director Mode.
BOOL g_bDirectorModeRunning = FALSE				// Used to check whether there is a DirectorMode script running
BOOL g_bDirectorSwitchToLastPlayer = FALSE		// Used by the selector to inform the DM script to return to the last player saved position.
BOOL g_bDirectorSwitchToCastingTrailer = FALSE  // Used by the selector to inform the DM script to return to the Trailer.
BOOL g_bDirectorSwitchToCastingTrailerFromCode = FALSE  // Used to switch to Director Mode from the Pause menu, bypassing the warning screen
//BOOL g_bDirectorInteriorsCached = FALSE			// Used for 1-time set-up of interiors for use with block conditions
BOOL g_bDirectorPIMenuUp = FALSE				// Used to check whether the PI menu is up before blocking
BOOL g_bDirectorForceSaveData = FALSE			// Used to indicate a forced auto save before the general script cleanup occurs
BOOL g_bDirectorTerminateOnSpCleanup = FALSE	// Used to signal Director Mode to terminate when SP cleans up and other force cleanups have not been hit.

#ENDIF

CONST_INT 		cTotalLocations 15					// Max number of travel locations.
CONST_FLOAT 	cf_REVEAL_LOC_DIST 	150.0
INT 			iMaxLocationsBitValue = -1

#IF FEATURE_SP_DLC_DIRECTOR_MODE
CONST_INT ci_MAXPROPSCENES	4
CONST_INT ci_MAXPropObjectsPerScene 50
#ENDIF

//Interior instances
INTERIOR_INSTANCE_INDEX g_intSolomonOffice, g_intPlayerGarage, g_intFoundryqq
INTERIOR_INSTANCE_INDEX g_intChickenFactory2, g_intChickenFactory1,  g_intChickenFactory3, g_intChickenFactory4
INTERIOR_INSTANCE_INDEX g_intShootRange,  g_intStripClub, g_intCinema1, g_intCinema2, g_intCinema3, g_intChopShop, g_intEpsilon2
INTERIOR_INSTANCE_INDEX g_intMine, g_intOmega, g_intCarDealer ,g_intSweatshop, g_intSweatshop2, g_intShopRob[20]

#IF FEATURE_SP_DLC_DIRECTOR_MODE
ENUM DirectorUnlockStory
	DU_STORY_INVALID = -1,

	DU_STORY_MICHAEL,
	DU_STORY_FRANKLIN,
	DU_STORY_TREVOR,
	DU_STORY_AMANDA_T,
	DU_STORY_BEVERLY,
	DU_STORY_BRAD,
	DU_STORY_CHRIS_F,
	DU_STORY_DAVE_N,
	DU_STORY_DEVIN,
	DU_STORY_DR_F,
	DU_STORY_FABIEN,
	DU_STORY_FLOYD,
	DU_STORY_JIMMY,
	DU_STORY_LAMAR,
	DU_STORY_LAZLOW,
	DU_STORY_LESTER,
	DU_STORY_MAUDE,
	DU_STORY_MRS_T,
	DU_STORY_RON,
	DU_STORY_PATRICIA,
	DU_STORY_SIMEON,
	DU_STORY_SOLOMON,
	DU_STORY_STEVE_HAINS,
	DU_STORY_STRETCH,
	DU_STORY_TANISHA,
	DU_STORY_TAO_C,
	DU_STORY_TRACEY,
	DU_STORY_WADE,
	
	MAX_DU_STORY
ENDENUM

ENUM DirectorUnlockHeist
	DU_HEIST_INVALID = -1,

	DU_HEIST_CHEF,
	DU_HEIST_CHRISTIAN,
	DU_HEIST_DARYL,
	DU_HEIST_EDDIE,
	DU_HEIST_GUSTAV,
	DU_HEIST_HUGH,
	DU_HEIST_KARIM,
	DU_HEIST_KARL,
	DU_HEIST_NORM,
	DU_HEIST_PACKIE,
	DU_HEIST_PAIGE,
	DU_HEIST_RICKIE,
	DU_HEIST_TALINA,
	
	MAX_DU_HEIST
ENDENUM

ENUM DirectorUnlockSpecial
	DU_SPECIAL_INVALID = -1,

	DU_SPECIAL_ANDYMOON,	
	DU_SPECIAL_BAYGOR,
	DU_SPECIAL_BILLBINDER,
	DU_SPECIAL_CLINTON,
	DU_SPECIAL_GRIFF,
	DU_SPECIAL_JANE,
	DU_SPECIAL_JEROME,
	DU_SPECIAL_JESSE,
	DU_SPECIAL_MANI,
	DU_SPECIAL_MIME,
	DU_SPECIAL_PAMELADRAKE,
	DU_SPECIAL_SUPERHERO,
	DU_SPECIAL_ZOMBIE,
	
	MAX_DU_SPECIAL
ENDENUM
	
ENUM DirectorUnlockAnimal
	DU_ANIMAL_INVALID = -1,

	DU_ANIMAL_BOAR,			          
	DU_ANIMAL_CAT,
	DU_ANIMAL_COW,
	DU_ANIMAL_COYOTE,
	DU_ANIMAL_DEER,
	DU_ANIMAL_HUSKY,
	DU_ANIMAL_MTLION,
	DU_ANIMAL_PIG,
	DU_ANIMAL_POODLE,
	DU_ANIMAL_PUG,
	DU_ANIMAL_RABBIT,
	DU_ANIMAL_RETRIEVER,
	DU_ANIMAL_ROTTWEILER,
	DU_ANIMAL_SHEPHERD,
	DU_ANIMAL_WESTY,
	DU_ANIMAL_CHICKENHAWK,
	DU_ANIMAL_CORMORANT,
	DU_ANIMAL_CROW,
	DU_ANIMAL_HEN,
	DU_ANIMAL_PIGEON,
	DU_ANIMAL_SEAGULL,
	DU_ANIMAL_SASQUATCH,
	
	MAX_DU_ANIMAL
ENDENUM

ENUM DirectorTravelLocation
	DTL_INVALID = -1

	,DTL_USER_1
	,DTL_USER_2

	,DTL_LS_INTERNATIONAL
	,DTL_DOCKS
	,DTL_LS_DOWNTOWN
	,DTL_DEL_PERRO_PIER
	,DTL_VINEWOOD_HILLS
	,DTL_WIND_FARM
	,DTL_SANDY_SHORES
	,DTL_STAB_CITY
	,DTL_FORT_ZANCUDO
	,DTL_CANYON
	,DTL_CABLE_CAR
	,DTL_PALETO
	,DTL_LIGHTHOUSE
	,DTL_VINEYARD
	,DTL_STUDIO
	,DTL_GOLFCLUB
	,DTL_ALTRUIST
	,DTL_POWERSTATION
	,DTL_SAWMILL
	,DTL_QUARRY
	,DTL_DAM
	,DTL_STORMDRAIN
	,DTL_OBSERVATORY
	,DTL_RADIOARRAY
	,DTL_LSU
	,DTL_COVE
	,DTL_MTGORDO	
	#IF FEATURE_SP_DLC_DM_WAYPOINT_WARP ,DTL_WAYPOINT	#ENDIF

	,MAX_DTL
ENDENUM


// Saved globals for this system. Remember to update 
// x:\gta5\script\<BRANCH>\shared\include\globals\registration\director_mode_globals_reg.sch
// if you change this.
STRUCT DirectorModeSaved
	INT iBitsetCharacterStoryUnlock
	INT iBitsetCharacterHeistUnlock
	INT iBitsetCharacterSpecialUnlock
	INT iBitsetCharacterAnimalUnlock

	INT iBitsetTravelLocationRevealed

	INT iExitHelpShownCount
	
	INT iShortlistData[100]
	INT shortlistCategories[20]
	INT shortlistModels[20]
	INT shortlistSex
	INT shortlistVoice[20]
	
	//last location
	VECTOR vSavedMapCoord
	FLOAT fSavedPlayerHeading
	
	//saved user coords
	VECTOR vSavedUserDefinedLocation[2]
	FLOAT fSavedUserDefinedHeading[2]
	BOOL bSavedUserDefinedInterior[2]	

	#IF FEATURE_SP_DLC_DM_PROP_EDITOR
	//Prop Editor Scenes
	VECTOR			vSavedPropScenesPos[ci_MAXPROPSCENES*ci_MAXPropObjectsPerScene]
	VECTOR			vSavedPropScenesRot[ci_MAXPROPSCENES*ci_MAXPropObjectsPerScene]
	INT 			vSavedPropScenesModel[ci_MAXPROPSCENES*ci_MAXPropObjectsPerScene]
	TEXT_LABEL_23	vSavedPropScenesNames[ci_MAXPROPSCENES]
	#ENDIF
ENDSTRUCT


#ENDIF
