
//╒═════════════════════════════════════════════════════════════════════════════╕
//│					 		Flow Global Variable  Header						│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│		AUTHOR:			Ben Rollinson											│
//│		DATE:			19/11/13												│
//│		DESCRIPTION: 	All global variables required by the singleplayer		│
//│						gameflow.												│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "flow_global_definitions.sch"


// ===========================================================================================================
//      Control Variables - Unsaved
// ===========================================================================================================

// TRUE when main_persistent knows the game has gone into multiplayer, FALSE when back in singleplayer.
BOOL    g_bInMultiplayer = FALSE

// Used to identify when we are loading to SP from a fresh load and when we're returning from a MP session.
BOOL    g_bHaveBeenInMultiplayer = FALSE

//Used to only run SET_GLOBAL_INSTANCE_PRIORITY in main_persistant if not the same frame as startup
BOOL    g_bIsStartUp = FALSE


//Flow cutscene preloading variables.
BOOL            g_bFlowLoadIntroCutscene            = FALSE
BOOL            g_bFlowIntroVariationRequestsMade   = FALSE
SP_MISSIONS     g_eMissionIDToLoadCutscene          = SP_MISSION_NONE
TEXT_LABEL_23   g_txtIntroMocapToLoad               = "NONE"
INT             g_iCharIntroSectionsToLoad[3]       
INT             g_iFlowIntroCutsceneRequestID       = NULL_OFFMISSION_CUTSCENE_REQUEST
BOOL            g_bFlowCleanupIntroCutscene         = FALSE
BOOL            g_bFlowLoadRequestStarted           = FALSE
BOOL            g_bFlowLoadingIntroCutscene         = FALSE


//Flow cutscene variation setting variables.
INT g_iFlowBitsetBlockPlayerCutsceneVariations[3]

INT g_iFlowCurrentCutsceneHandleCount = 0
TEXT_LABEL_23 g_txtFlowCutsceneHandles[MAX_QUEUED_CUTSCENE_HANDLES]

INT g_iFlowCurrentCutsceneVariationCount = 0
FlowCutsceneVariation g_sFlowCutsceneVariations[MAX_QUEUED_CUTSCENE_VARIATIONS]

INT g_iFlowCurrentCutsceneOutfitCount = 0
FlowCutsceneOutfit g_sFlowCutsceneOutfit[MAX_QUEUED_OUTFIT_VARIATIONS]

INT g_iFlowCurrentPedVariationCount = 0
FlowPedVariation g_sFlowPedVariation[MAX_QUEUED_PED_VARIATIONS]

INT g_iFlowCurrentCutscenPropCount = 0
FlowCutsceneProp g_sFlowCutsceneProp[MAX_QUEUED_CUTSCENE_PROPS]


// mission fail globals
TEXT_LABEL_15   g_txtMissionFailReason
TEXT_LABEL_15   g_txtMissionFailAddText
VECTOR          g_vMissionFailWarp
FLOAT           g_fMissionFailHeading
INT             g_iTimeStartedExitingVehicle            = -1

// A flag to tell launchers and triggers to automatically trigger as they are creating trigger scenes.
BOOL g_bSceneAutoTrigger = FALSE

#IF IS_DEBUG_BUILD
    BOOL g_bBlockMissionTrigger = FALSE
#ENDIF


// ===========================================================================================================
//      Repeat Play Variables.
// ===========================================================================================================
RepeatPlayData g_RepeatPlayData         // Struct used for storing which mission the player is currently repeat playing

// bitset used to store repeat play global bools
INT g_iRepeatPlayBits = 0   
enumGrouping g_eMenuMissionType  // Needs a separate variable so using the pause menu during repeat play doesn't break stuff
INT g_iNoRepeatableMissions		// The number of repeatable mission in the pause menu mission list
VECTOR g_vRPVelocity			// used for unfreezing the player's vehicle realistically- is needed as the game is paused when we store the snapshot	


// --------Mission titles--------------------------------------------
MISSION_TITLE_STATE g_eMissionTitleState                    = MTS_DONE
INT                 g_iFlowDisplayMissionTitle              = -1
INT                 g_iFlowTimeBeganDisplayMissionTitle     = -1
enumGrouping        g_eMissionTitleType                     = CP_GROUP_NO_GROUP
BOOL				g_bMissionTitleBlocked					= FALSE
BOOL 				g_bMissionTitleMovedSubtitles			= FALSE

// Mission passed.m
BOOL            g_bResultScreenDisplaying               = FALSE
BOOL			g_bResultScreenPrepared					= FALSE
SP_MISSIONS     g_eRunningMission                       = SP_MISSION_NONE
SP_MISSIONS     g_eMissionRunningOnMPSwitchStart		= SP_MISSION_NONE
SP_MISSIONS     g_eLastMissionPassed                    = SP_MISSION_NONE
INT             g_iLastMissionPassedGameTime            = -1
INT             g_iLastSelectedPauseMenuMission         = -1

//Scrap collection shard active
BOOL 			g_bCollectedScreenDisplaying 			= FALSE


// ===========================================================================================================
//      Mission Flow Command Storage Arrays
// ===========================================================================================================
BOOL g_bEndScreenSuppressFadeIn = FALSE
FLOW_UNSAVED_VARS g_flowUnsaved


#IF IS_NEXTGEN_BUILD
TWEAK_FLOAT	END_SCREEN_CURSIVE_TITLE_SCALE		 0.6250
TWEAK_FLOAT	END_SCREEN_CURSIVE_TITLE_Y_OFFSET	-0.0015

TWEAK_FLOAT	END_SCREEN_CURSIVE_COMP_SCALE		 0.5000
TWEAK_FLOAT	END_SCREEN_CURSIVE_COMP_X_OFFSET	 0.119 //0.0350

TWEAK_FLOAT	END_SCREEN_MEDAL_SCALE				 0.6500
TWEAK_FLOAT	END_SCREEN_MEDAL_Y_OFFSET			 10.0		//9.0

BOOL		g_bEnd_Screen_Display_Finish		= FALSE
#ENDIF

// ===========================================================================================================
//      Available Mission Tracking
// ===========================================================================================================

// Available Missions
// Contains the index within the mission variables array for all missions currently available to the player


// -----------------------------------------------------------------------------------------------------------
// Array of Available Missions.
// This array gets filled and emptied as new missions open up to the player as the Mission Flow progresses.

AVAILABLE_MISSION_STORAGE   g_availableMissions[MAX_MISSIONS_AVAILABLE]

// ===========================================================================================================
//      Individual Mission's Data
// ===========================================================================================================

// Non-saved static data for each story mission.
g_structMissionStaticData   g_sMissionStaticData[SP_MISSION_MAX]

// Non-saved active data for each story mission.
g_structMissionActiveData   g_sMissionActiveData[SP_MISSION_MAX]

// Non-saved static debug data for each story mission.
#IF IS_DEBUG_BUILD
    g_structMissionDebugData    g_sMissionDebugData[SP_MISSION_MAX]
	VECTOR WARP_POS_MAIN_BLIP = <<0,0,0>>
#ENDIF


// ===========================================================================================================
//      Mission Triggering
// ===========================================================================================================

//storage of suppressed model by random events
MODEL_NAMES g_mnFlowRandomEventSuppressedModel = DUMMY_MODEL_FOR_SCRIPT


// ===========================================================================================================
//      Shitskip Control Variables
// ===========================================================================================================

//The number of fails without progress before a shitship is offered.
INT g_iLastObservedRetryStatusOnFail = -1 // set to -1 so 1st fail on 1st stage isn't counted towards the 2 total fails.
BOOL g_bShitskipOffered = FALSE     // does this replay have a shitskip?
BOOL g_bShitskipAccepted = FALSE    // has the player accepted the shitskip?
BOOL g_bShitskipToEnd = FALSE       // would shitskipping from here end the mission? (not reset at same time as above variables. handled when setting checkpoints)


// ===========================================================================================================
//      Relaunch Variables for Flow Dependent Scripts.
// ===========================================================================================================

// Specifies the size of the Restore_Launch array, struct and the enum which is publically accessible.
// When adding an entry to the enum, please ensure that a matching script name and stack size is included
// in initial.sc within the Define_Restore_Launched_ScriptNames() procedure.
 
structRestore_Launched_Scripts g_Restore_Launched_Script[MAXIMUM_LAUNCH_BITS]

// ===========================================================================================================
//      Checklist Variables.
// ===========================================================================================================

TEXT_LABEL_15   g_tChecklistItems[MAX_CHECKLIST_LENGTH]
INT             g_iChecklistItemCount = 0


// ===========================================================================================================
//      Custom Flow Check Globals.
// ===========================================================================================================

INT 	g_iTimeStartedMissionUnlockCheck = -1


// ===========================================================================================================
//		Flow command hash ID checking variables
// ===========================================================================================================

#IF IS_DEBUG_BUILD
#IF ENABLE_FLOW_COMMAND_HASH_ID_CHECKING
	INT	g_iCommandHashes[MAX_MISSION_FLOW_COMMANDS]
#ENDIF
#ENDIF
