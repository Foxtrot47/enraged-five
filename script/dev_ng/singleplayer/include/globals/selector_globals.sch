//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	selector_globals.sch										//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Globals used for the hotswap.								//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "commands_graphics.sch"
USING "Net_Script_Timers.sch"

CONST_INT USE_REPLAY_RECORDING_TRIGGERS 1

CONST_INT NUM_SELECTOR_INTERP_CAMS 		8

CONST_INT SELECTOR_UI_QUICK_SWITCH_TIME_msec 300
CONST_INT SELECTOR_UI_AMBIENT_MP_TIME_msec 300
CONST_INT SELECTOR_UI_AMBIENT_SP_TIME_msec 200

CONST_INT SELECTOR_UI_FADE_TIME_msec 	1500
CONST_FLOAT SELECTOR_UI_FADE_TIME_sec 	1.5

CONST_FLOAT SELECTOR_CAM_DEFAULT_FOV 	50.0

bool turn_on_sniper_hud_for_exile_2 = false

// Enums used to define the peds
ENUM SELECTOR_SLOTS_ENUM
	SELECTOR_PED_MICHAEL = 0,
	SELECTOR_PED_FRANKLIN,
	SELECTOR_PED_TREVOR,
	SELECTOR_PED_MULTIPLAYER,
		
	NUMBER_OF_SELECTOR_PEDS
ENDENUM

// Enums used to define the ui icons
ENUM SELECTOR_ICON_ENUM
	SELECTOR_ICON_DEFAULT = 0,
	SELECTOR_ICON_CHOP,
	SELECTOR_ICON_PRO_MICHAEL_MASKED,		// Only works when Prologue strand is active
	SELECTOR_ICON_PRO_MICHAEL_UNMASKED,		// Only works when Prologue strand is active
	SELECTOR_ICON_PRO_TREVOR_MASKED,		// Only works when Prologue strand is active
	SELECTOR_ICON_PRO_TREVOR_UNMASKED		// Only works when Prologue strand is active
ENDENUM

// Enums used to define the ui activity icon
ENUM SELECTOR_ACTIVITY_ENUM
	
	SELECTOR_ACTIVITY_DEFAULT = 0,
	
	SELECTOR_ACTIVITY_BIKE = 1,
	SELECTOR_ACTIVITY_PLANE = 2,
	SELECTOR_ACTIVITY_SNIPER = 3,
	SELECTOR_ACTIVITY_HELI = 4,
	SELECTOR_ACTIVITY_BOAT = 5,
	SELECTOR_ACTIVITY_CAR = 6,
	SELECTOR_ACTIVITY_STANDING = 7,
	SELECTOR_ACTIVITY_GUN = 8,
	
	// These ones are no longer used...
	SELECTOR_ACTIVITY_ATTACHED_TO_HELI,
	SELECTOR_ACTIVITY_MICHAEL_UPSIDE_DOWN,
	SELECTOR_ACTIVITY_TORTURE,
	SELECTOR_ACTIVITY_THERMAL_VISION
ENDENUM

// Enums used to define ped state
ENUM SELECTOR_STATE_ENUM
	SELECTOR_STATE_DEFAULT = 0,
	SELECTOR_STATE_UNAVAILABLE,
	SELECTOR_STATE_AVAILABLE,
	SELECTOR_STATE_CURRENT
ENDENUM

// Types of camera used in hot-swap
ENUM SELECTOR_CAM_INTERP_TYPE
	SELECTOR_CAM_DEFAULT = 0,
	SELECTOR_CAM_STRAIGHT_INTERP,
	SELECTOR_CAM_SHORT_SPLINE,
	SELECTOR_CAM_LONG_SPLINE
ENDENUM

ENUM SELECTOR_UI_STATE_ENUM
	SELECTOR_UI_HIDDEN = 0,
	SELECTOR_UI_HIDDEN_TO_FULL,
	SELECTOR_UI_HINT,
	SELECTOR_UI_HINT_TO_FULL,
	SELECTOR_UI_FULL,
	SELECTOR_UI_AUTOSWITCH_HINT
ENDENUM


ENUM TAKE_CONTROL_OF_PED_FLAGS
	TCF_NONE = 0,
	TCF_CLEAR_TASK_INTERRUPT_CHECKS = 1 // Clear the PCF_WaitingForPlayerControlInterrupt conflig flag to ensure the players task remain
	//TCF_ = 2,
	//TCF_ = 4,
	//TCF_ = 8,
	//TCF_ = 16
ENDENUM


// The camera struct used for hot-swap
STRUCT SELECTOR_CAM_STRUCT
	CAMERA_INDEX	camInterpIDs[NUM_SELECTOR_INTERP_CAMS]
	CAMERA_INDEX	camID						// The camera used in the hot-swap
	CAMERA_INDEX	camTo						// The destination camera if we chose not to use a ped
	CAMERA_INDEX	camSky						// The camera we use when interping to and from the sky
	PED_INDEX		pedTo						// The ped that the player is swapping to
	PED_INDEX		pedFrom						// Run cam spline sets this to the player id
	SELECTOR_CAM_INTERP_TYPE camType			// The type of camera used in the hot-swap
	BOOL			bSplineCreated				// Control check to see if we need to generate a camera for hotswap
	BOOL			bSplineActive				// Control check to see if the camera used in the hot-swap has been activated
	BOOL			bSplineComplete				// Control check that tells us when the hot-swap camera has finished interpolating
	BOOL			bOKToSwitchPed				// Control check that enabled the calling script to know when to switch the peds
	BOOL			bPedSwitched				// Control check that the calling script can use
	BOOL			bRun						// Control check that the calling script can use
	BOOL			bIsPhoneDisabled			// Lets us know if we should set the phone to away when were done
	BOOL			bTakenContolOfTransition	// #1323307
	BOOL			bSetShadowSampleBox3x3		// #2006772
	INT				iInterpCams					// Amount of additional cams we are using
	INT				iActivateTimer				// The timer that gets set when the hot-swap camera becomes active
	TIME_DATATYPE	iActivateTimerNetwork		// Added by Neilf - required for new network times
	INT				iSplineDuration				// The overall duration of the camera spline
	FLOAT 			fMaxHeight					// The max height that the camera reaches during the spline
//	SCRIPT_TIMER	iMPActivateTimer			// The timer that gets set when the hot-swap camera becomes active
	
	STREAMVOL_ID SteamingVolume
	SCRIPT_TIMER iStreamingTimer
	INTERIOR_INSTANCE_INDEX aStreamedInterior
	
	BOOL DoneLoadingScene
	
	#IF IS_DEBUG_BUILD
		CAMERA_INDEX camCodeTransition
		BOOL bUsingCodeCam
	#ENDIF
	
ENDSTRUCT
SELECTOR_CAM_STRUCT g_SkyCamData

// The main struct that calling script will use to define the peds used in hot-swap
STRUCT SELECTOR_PED_STRUCT	
	PED_INDEX			pedID[NUMBER_OF_SELECTOR_PEDS]				// All the peds used in hot-swap
	SELECTOR_SLOTS_ENUM	ePreviousSelectorPed						// The hot-swap ped that the player was previously in control of
	SELECTOR_SLOTS_ENUM	eCurrentSelectorPed							// The hot-swap ped that the player is currently in control of
	SELECTOR_SLOTS_ENUM	eNewSelectorPed								// The hot-swap ped that the player wants to be in control o
	BOOL				bDisplayHint[NUMBER_OF_SELECTOR_PEDS] 		// If we should display the player select hint
	BOOL				bDisplayDamage[NUMBER_OF_SELECTOR_PEDS] 	// If we should display the player damaged hint
	BOOL				bBlockSelectorPed[NUMBER_OF_SELECTOR_PEDS] 	// If we should block the ped from being selection
	BOOL				bAmbient									// If the struct is part of the ambient or mission hotswap
	SELECTOR_ICON_ENUM  eIcons[NUMBER_OF_SELECTOR_PEDS]
	SELECTOR_ACTIVITY_ENUM eActivity[NUMBER_OF_SELECTOR_PEDS]
	SELECTOR_STATE_ENUM eFakeState[NUMBER_OF_SELECTOR_PEDS]
	BOOL 				bInitialised
	
	SELECTOR_SLOTS_ENUM eAutoSelectOption1
	SELECTOR_SLOTS_ENUM eAutoSelectOption2
	SELECTOR_SLOTS_ENUM eAutoSelectOption3
	BOOL bUseCustomAutoSelectOption
ENDSTRUCT
SELECTOR_PED_STRUCT g_sTempSelectorData // For use with SET_CURRENT_SELECTOR_PED() so memory not added to script.

BOOL g_bSelectorCamActive = FALSE
INT	g_iPlayerFlowHintActive = 0

// Scaleform UI
STRUCT SELECTOR_UI_STRUCT
	// Scaleform movie state
	BOOL bLoaded
	BOOL bSetup
	BOOL bFakeStateUpdated
	BOOL bForceUpdate
	BOOL bOnScreen
	BOOL bDisabled
	BOOL bDisabledThisFrame
	BOOL bForceLoadThisFrame
	BOOL bDisableMapOverrideThisFrame
	BOOL bHiddenThisFrame
	BOOL bAllowSelectionWhenHidden
	BOOL bSelection
	BOOL bMissionUpdate
	BOOL bManualSelection
	BOOL bFirstSelectionMade
	BOOL bResetSelection
	BOOL bBlockQuickSwitchToDamagedPed
	
	BOOL bDisableForEndOfMission
	INT iEndOfMissionTimer
	
	// Scaleform data
	INT iState[4]
	INT iEnum[4]
	INT iCurrent[4]
	INT iMissions[4]
	INT iCalls[4]
	INT iTxts[4]
	BOOL bHinted[4]
	BOOL bDamaged[4]
	BOOL bFlashDamage[4]
	INT iHintEnum[4]
	INT iSelectedSlot
	BOOL bCharAvailable
	INT iHealth[4]
	INT iArmour[4]
	VECTOR vCoords[4]
	INT iSpecial[4]
	INT iSpecialMax[4]
	
	SELECTOR_UI_STATE_ENUM eUIStateCurrent
	
	// Audio data
	INT iSFX_Display, iSFX_Display_ScriptHash
	INT iSFX_HeadDown, iSFX_HeadDown_ScriptHash
	INT iSFX_Sky, iSFX_Sky_ScriptHash
	BOOL bSFX_BlockAudioCalls
	
	// Visuals
	BOOL bFX_Fade
	BOOL bFX_SkipFadeOut
	
	INT iDisplayTimer
	TIME_DATATYPE tdDisplayTimer
	
	BOOL bTimeScaleReduced
	
	TEXT_LABEL_15 tlStoredCountdown
	
	#IF IS_DEBUG_BUILD
		BOOL debug_bUseTransitionCamera
		FLOAT debug_fSwitchPedAfterPhase
		BOOL debug_bDisplay
		PED_INDEX debug_pedID[4]
		BLIP_INDEX debug_blipID[4]
		BOOL debug_bBlipFlashing[4]
		BOOL debug_bBlockState[4]
		BOOL debug_bHintState[4]
		BOOL debug_bDamageState[4]
		INT debug_iState[4]
		
		TEXT_LABEL_63 tlDisableScript
		TEXT_LABEL_63 tlHideScript
	#ENDIF
	
	
	
	BOOL bCheckPlayerControl
	BOOL bCheckActivationButton
	BOOL bAllowWhenDead
	BOOL bDisplay
	
	BOOL bHideUiForSwitch
	
	BOOL bShowAutoSwitch
	INT iAutoSwitchSlot
	INT iAutoSwitchEnum
	INT iAutoSwitchTimer
	
	BOOL bMustReleaseSelectorUIButton
	BOOL bMustReleaseCancelButton
	BOOL bMustReleaseAcceptButton
	BOOL bMustReleaseSquareButton
	
	#IF USE_REPLAY_RECORDING_TRIGGERS
		#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 tlDisableReplayFeedScript
		#ENDIF
		BOOL bReplayFeedDisabledThisFrame
		BOOL bFeedAddedForRecording
	#ENDIF
	
	
	
ENDSTRUCT
SELECTOR_UI_STRUCT g_sSelectorUI

