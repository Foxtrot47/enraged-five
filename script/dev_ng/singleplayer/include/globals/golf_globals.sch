//Bit field of available buddies
ENUM GOLF_BUDDIES

	GB_NONE     = 0,
	GB_MALE1   = 1,
	//GB_CAR_THEFT_VICTIM  = 1
	GB_MALE3    = 2,
	//GB_FEMALE2  = 16,
	
	GB_MALE4    = 4,
	GB_MALE5    = 8,
	GB_DOMESTIC    = 16,
	//GB_FEMALE3  = 128,
	//GB_FEMALE4  = 256,
	//GB_FEMALE5  = 512,
	
	GB_ALL_UNLOCKED = 31, //all flags set to true
	GB_MAX_BUDDIES = 32 //max buddies is one bit higher then the buddy wit the highest values bit

ENDENUM

ENUM GOLF_SAVED_FLAGS
	GSAVE_NONE = 0,
	GSAVE_MICHAEL_UNLOCKED_DOMESTIC = BIT0,
	GSAVE_TREVOR_UNLOCKED_DOMESTIC = BIT1,
	GSAVE_FRANKLIN_UNLOCKED_DOMESTIC = BIT2
ENDENUM

STRUCT GolfHoleDataSaved
	FLOAT	fLongestDriveHole
	FLOAT	fLongestPuttHole
ENDSTRUCT

STRUCT GolfCourseDataSaved
	GolfHoleDataSaved		sHole[9]
//	INT		iLowestScoreFront	// Removing since we only have front 9
//	INT 	iLowestScoreBack	// Removing since we only have front 9
//	INT 	iLowestScoreTotal	Moving to stat list
//	FLOAT	fAveragePutts		Moving to stat list
//	FLOAT	fFairwayDrives	// Moving to stat list
ENDSTRUCT

STRUCT GolfDataSaved
	GolfCourseDataSaved		sCourse[1]		// This will need increasing if we get more than 1 course
	INT 	iNumPuttsTotal
	INT 	iNumFairwayDrivesTotal
	INT 	iNumHolesPlayed
	GOLF_BUDDIES unlockedBuddies
	GOLF_SAVED_FLAGS golfSavedFlags
	
	INT iNumRoundsPlayedSP, iNumRoundsPlayedMP, iNumGolfWins
	INT iBestRound = 82
	INT iNumRoundsWithCharacter[3]
	FLOAT fLongestDrive
	FLOAT fLongestPutt
	FLOAT fAveragePutts
	FLOAT fAverageFairwayDrives
ENDSTRUCT

ENUM GLOBAL_GOLF_CONTROL_FLAGS
	GGCF_NO_FLAGS				= 0,
	GGCF_PLAYER_SKIP			= BIT0,
	GGCF_PLAYER_DONE_WITH_HOLE  = BIT1,
	GGCF_START_GOLF_GAME		= BIT2,
	GGCF_AMBIENT_GOLFERS_STREAMED_IN = BIT3,
	GGCF_PLAYER_ADDRESSING_BALL = BIT4,
	GGCF_PLAYER_QUIT_MP_GOLF = BIT5,
	GGCF_PLAYER_HOLE_IN_ONE = BIT6
ENDENUM


STRUCT GolfGlobals
	INT CurrrentPlayerHole = -1
	INT GlobalGolfControlFlag
ENDSTRUCT

GolfGlobals g_sGolfGlobals

PROC  UNLOCK_GOLF_BUDDY(GolfDataSaved &savedData, GOLF_BUDDIES unlockBuddy)
	savedData.unlockedBuddies = savedData.unlockedBuddies | unlockBuddy
ENDPROC

PROC LOCK_GOLF_BUDDIES(GolfDataSaved &savedData, GOLF_BUDDIES lockBuddy)
	savedData.unlockedBuddies -= (savedData.unlockedBuddies & lockBuddy) 
ENDPROC

FUNC BOOL IS_GOLF_BUDDY_UNLOCKED(GolfDataSaved &savedData, GOLF_BUDDIES Buddy)
	RETURN (savedData.unlockedBuddies & Buddy) !=  GB_NONE
ENDFUNC

PROC SET_GLOBAL_GOLF_CONTROL_FLAG(GLOBAL_GOLF_CONTROL_FLAGS golfControlFlags)
	g_sGolfGlobals.GlobalGolfControlFlag = g_sGolfGlobals.GlobalGolfControlFlag | ENUM_TO_INT(golfControlFlags)
ENDPROC
PROC CLEAR_GLOBAL_GOLF_CONTROL_FLAG(GLOBAL_GOLF_CONTROL_FLAGS golfControlFlags)
	g_sGolfGlobals.GlobalGolfControlFlag -= g_sGolfGlobals.GlobalGolfControlFlag &  ENUM_TO_INT(golfControlFlags)
ENDPROC

FUNC BOOL IS_GLOBAL_GOLF_CONTROL_FLAG_SET(GLOBAL_GOLF_CONTROL_FLAGS golfControlFlags)
	RETURN (g_sGolfGlobals.GlobalGolfControlFlag &  ENUM_TO_INT(golfControlFlags)) != ENUM_TO_INT(GGCF_NO_FLAGS)
ENDFUNC

FUNC INT GET_PRICE_OF_GOLF_GAME(BOOL bFriendActivity)
	IF bFriendActivity
		RETURN 200
	ELSE
		RETURN 100
	ENDIF
ENDFUNC

FUNC BOOL IS_GOLF_CLUB_OPEN_AT_TIME_OF_DAY(INT iHour)
	RETURN iHour> 6 AND iHour < 18
ENDFUNC
