// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      SCRIPT NAME     :   CompletionPercentage_globals.sch
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   sets up global enums to track percentage completion of the game.
//      DATE            :   25.07.11
//                          
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************




ENUM enumCompletionPercentageEntries

    //Make sure to update the grouping divisor in CompletionPercentage_public.sch

    CP_ARM1,            //Armenian 1 - Franklin and Lamar
    CP_ARM2,            //Armenian 2 - Repossession 
    CP_ARM3,            //Armenian 3 - Complications



    CP_CAR1,            //Car Steal 1 - I fought the law..

    CP_CAR2,            //Car Steal 2 - Eye in the Sky
    CP_CAR3,            //Car Steal 3 - Agent
    CP_CAR4,            //Car Steak 4 - Pack Man.


    CP_CHN1,            //Chinese 1 - Trevor Philips Industries
    CP_CHN2,            //Chinese 2 - Crystal Maze   



    CP_EXL1,            //Exile 1 - Minor Turbulence
    CP_EXL2,            //Exile 2 - Predator
    CP_EXL3,            //Exile 3 - Derailed

    
    CP_FAM1,            //Family 1 - Father / Son
    CP_FAM2,            //Family 2 - Daddy's Little Girl
    CP_FAM3,            //Family 3 - Marriage Counselling
    CP_FAM4,            //Family 4 - Fame or Shame
    CP_FAM5,            //Family 5 - Did Somebody say Yoga?


    CP_FAM6,            //Family 6 - Reuniting the Family merged.

  
  
    CP_FBI1,            //FBI 1 - Dead Man Walking
    CP_FBI2,            //FBI 2 - Three's Company
    CP_FBI3,            //FBI 3 - By the Book
    

  

    CP_FBI4_P1,         //Steal Garbage Truck
    CP_FBI4_P2,         //Steal Tow Truck
    CP_FBI4_P3,         //Prepare Getaway,
    CP_FBI4_P4,         //Obtain outfits - Masks
    CP_FBI4_P5,         //Obtain outfits - Boiler Suits.


    CP_FBI4,            //FBI 4 - Heat


    CP_FBI5,            //FBI 5  - new almagamated Monkey Business.


    



    CP_FRA0,            //Franklin 0  - Chop 
    CP_FRA1,            //Franklin 1 - Ghetto Safari
    CP_FRA2,            //Franklin 2 - Lamar Down

    CP_LM1,             //Lamar 1 - Terminador



    CP_LS1,             //Lester 1A - Friend Request
    CP_LS1A,

    CP_MIC1,            //Michael 1 - Bury the Hatchet
    CP_MIC2,            //Michael 2 - Fresh Meat
    CP_MIC3,            //Michael 3 - The Wrap Up
    CP_MIC4,            //Michael 4 - Movie Premier.





    CP_PRO1,            //Prologue


    CP_MAR1,            //Martin 1 - Vinewood Babylon


    CP_SOL1,            //Solomon 1 - Placeholder
    CP_SOL2,            //Solomon 2 - Placeholder
    CP_SOL3,            //Solomon 3 - BlowBack


    CP_TRV1,            //Trevor 1 - Mr. Philips                
    CP_TRV2,            //Trevor 2 - Nervous Ron
    CP_TRV3,            //Trevor 3 - Friends Reunited

    //Up for removal. See bug 896600
    CP_TRV4,            //Trevor 4 - Blow Back
    
    
   

    

    CP_FH1,             //Agency Heist 1 - Setup - Follow Janitor

    
    CP_FH2,             //Agency Heist 2

    CP_FH3_CHOICE,      //Agency Heist 3 between 3a, 3b





    CP_BHB,             //Big Score Planning Board
    CP_BH1,             //Big Score Heist 1 - Setup

    CP_BH3_CHOICE,      //Big Score 3 - between 3A, 3B - FIND OUT WHAT IS HAPPENING WITH THIS NUMBERING!


    CP_JH1,             //The Jewel Heist 1 - Setup
    
    
  
    CP_JH_PREP_CHOICE,  //The Jewel Heist prep choice.
   
    CP_JH2,      //The Jewel Heist 2 between 2A, 2B - Very similar. Ben says no longer a choice.


    CP_RBHP1,           //The Paleto Prep / Steal Military Hardware
    CP_RH1,             //The Paleto Score 1 Setup   / Rural Bank
    CP_RH2,             //The Paleto Score 2 - No longer a choice mission. See 956281


    CP_DHP1,            //The Port of Los Santos Heist Prep mission.
    CP_DH1,             //The Port of Los Santos Heist 1
    CP_DH2_CHOICE,      //The Port of Los Santos 2 between 2A and 2B



    
    //Up for possible removal. See bug 956282
    CP_FINALE_PREP_CHOICE,  //Finale Prep 1 choice mission.

    CP_FINALE_PREP_CHOICE2, //Finale Prep 2 choice mission.   


    CP_FINALE,          //Now only one Finale mission and no longer a choice. See bug 891989

    //Up for possible removal.
    CP_FINALE_CHOICE,   //Game Finale Choice missions. 1, 2 and 3a-3b.






    //Random Characters
    CP_RAND_C_OMG1,     //Omega 1
    CP_RAND_C_OMG2,     //Omega 2


    CP_RAND_C_DREY1,    //Dreyfuss 1

    CP_RAND_C_BAR1,     //Barry 1
    CP_RAND_C_BAR2,     //Barry 2
    CP_RAND_C_BAR3,     //Barry 3
    CP_RAND_C_BAR4,     //Barry 4

    CP_RAND_C_EPS1,     //Epsilonism 1
    CP_RAND_C_EPS2,     //Epsilonism 2
    CP_RAND_C_EPS3,     //Epsilonism 3
    CP_RAND_C_EPS4,     //Epsilonism 4
    CP_RAND_C_EPS5,     //Epsilonism 5
    CP_RAND_C_EPS6,     //Epsilonism 6
    CP_RAND_C_EPS7,     //Epsilonism 7
    CP_RAND_C_EPS8,     //Epsilonism 8

    CP_RAND_C_EXT1,     //Extreme 1
    CP_RAND_C_EXT2,     //Extreme 1
    CP_RAND_C_EXT3,     //Extreme 1
    CP_RAND_C_EXT4,     //Extreme 1

    CP_RAND_C_FAN1,     //Fanatic 1
    CP_RAND_C_FAN2,     //Fanatic 2
    CP_RAND_C_FAN3,     //Fanatic 3

    CP_RAND_C_JOS1,     //Josh 1
    CP_RAND_C_JOS2,     //Josh 2
    CP_RAND_C_JOS3,     //Josh 3
    CP_RAND_C_JOS4,     //Josh 4

    CP_RAND_C_MIN1,     //Minute Man 1
    CP_RAND_C_MIN2,     //Minute Man 2
    CP_RAND_C_MIN3,     //Minute Man 3

    CP_RAND_C_NIG1,     //Nige1 1
    CP_RAND_C_NIG1A,    //Nigel 1A
    CP_RAND_C_NIG1B,    //Nigel 1B
    CP_RAND_C_NIG1C,    //Nigel 1C
    CP_RAND_C_NIG1D,    //Nigel 1D
    CP_RAND_C_NIG2,     //Nigel 2
    CP_RAND_C_NIG3,     //Nigel 3

    CP_RAND_C_PAP1,     //Paparazzo 1
    CP_RAND_C_PAP2,     //Paparazzo 2
    CP_RAND_C_PAP3,     //Paparazzo 3
    CP_RAND_C_PAP4,     //Paparazzo 4

    CP_RAND_C_HAO1,     //Hao 1

    CP_RAND_C_MAU1,     //Maude 1

    CP_RAND_C_SAS1,     //Sasquatch / The Last One.

    CP_RAND_C_ABI1,     //Abigail 1

    CP_RAND_C_MRS1,     //Mrs Philips 1
    CP_RAND_C_MRS2,     //Mrs Philips 2







    
    //Minigames

    CP_MG_OFF1,         //Offroad Race 1 - Canyon Spiral
    CP_MG_OFF2,         //Offroad Race 2 - Ridge Run
    CP_MG_OFF3,         //Offroad Race 3 - Valley Trail
    CP_MG_OFF4,         //Offroad Race 1 - Lakeside Splash
    CP_MG_OFF5,         //Offroad Race 1 - Eco Friendly
    CP_MG_OFF6,         //Offroad Race 1 - Mineward Spiral


    //Up for removal - class-based enums incoming.
    CP_MG_SHOOT,        //Shooting Range.


    CP_MG_SHHAN,        //Shooting Range - Handguns
    CP_MG_SHSUB,        //Shooting Range - Submachine guns
    CP_MG_SHRIF,        //Shooting Range - Assault Rifles
    CP_MG_SHLMG,        //Shooting Range - Light Machine Guns
    CP_MG_SHHVY,        //Shooting Range - Heavy
    CP_MG_SHSHO,        //Shooting Range - Shotguns



    CP_MG_STUNT_TT,     //Stunt Plane Time Trials. Special Edition only

    CP_MG_TENNIS,       //Tennis


    CP_MG_GOLF,         //Golf

    CP_MG_BASEJ,        //Basejumping

    CP_MG_SKYD,         //Skydiving



    CP_MG_DARTS,        //Darts

    CP_MG_STRIP,        //Strip Club Visit


    CP_MG_SNCA,         //Sea Race North Coast 
    CP_MG_SSC,          //Sea Race South Coast
    CP_MG_SCAN,         //Sea Race Canyon 
    CP_MG_SLOS,         //Sea Race Los Santos


    CP_MG_SRCC,         //Street Race City Circuit
    CP_MG_SRAP,         //Street Race Airport
    CP_MG_SRFW,         //Street Race Freeway
    CP_MG_SRVC,         //Street Race Vespucci Canals
    CP_MG_SRLS,         //Street Race South Los Santos

    
    CP_MG_TRI1,         //Triathlon Trailer Park
    CP_MG_TRI2,         //Triathlon Muscle Beach
    CP_MG_TRI3,         //Triathlon Mountain Road









    //Oddjobs
    CP_OJ_ASS1,         //Assassinations 1 - Vice
    CP_OJ_ASS2,         //Assassinations 2 - Hotel
    CP_OJ_ASS3,         //Assassinations 3 - Construction
    CP_OJ_ASS4,         //Assassinations 4 - Bus
    CP_OJ_ASS5,         //Assassinations 5 - MultiTarget




    //Bail Bond oddjobs
    CP_OJ_BB3,          //Bail Bond 3 - Quarry
    CP_OJ_BB5,          //Bail Bond 5 - Abandoned Farm
    CP_OJ_BB6,          //Bail Bond 6 - Hobo Camp
    CP_OJ_BB7,          //Bail Bond 7 - Mountains







    CP_OJ_DTA1,         //Drug Arms Trafficking Air 1
    CP_OJ_DTA2,         //Drug Arms Trafficking Air 2
    CP_OJ_DTA3,         //Drug Arms Trafficking Air 3
    CP_OJ_DTA4,         //Drug Arms Trafficking Air 4
    CP_OJ_DTA5,         //Drug Arms Trafficking Air 5


    CP_OJ_DTG1,         //Drug Arms Trafficking Ground 1
    CP_OJ_DTG2,         //Drug Arms Trafficking Ground 2
    CP_OJ_DTG3,         //Drug Arms Trafficking Ground 3
    CP_OJ_DTG4,         //Drug Arms Trafficking Ground 4
    CP_OJ_DTG5,         //Drug Arms Trafficking Ground 5
    CP_OJ_DTG6,         //Drug Arms Trafficking Ground 6
    CP_OJ_DTG7,         //Drug Arms Trafficking Ground 7
    CP_OJ_DTG8,         //Drug Arms Trafficking Ground 8
    CP_OJ_DTG9,         //Drug Arms Trafficking Ground 9
    CP_OJ_DTG10,        //Drug Arms Trafficking Ground 10
    CP_OJ_DTG11,        //Drug Arms Trafficking Ground 11
    CP_OJ_DTG12,        //Drug Arms Trafficking Ground 12
    CP_OJ_DTG13,        //Drug Arms Trafficking Ground 13
    CP_OJ_DTG14,        //Drug Arms Trafficking Ground 14
    CP_OJ_DTG15,        //Drug Arms Trafficking Ground 15
    CP_OJ_DTG16,        //Drug Arms Trafficking Ground 16
    CP_OJ_DTG17,        //Drug Arms Trafficking Ground 17
    CP_OJ_DTG18,        //Drug Arms Trafficking Ground 18
    CP_OJ_DTG19,        //Drug Arms Trafficking Ground 19
    CP_OJ_DTG20,        //Drug Arms Trafficking Ground 20





    CP_OJ_HUN1,         //Hunting 1 (This is now included in the random character group.)
    CP_OJ_HUN2,         //Hunting 2 (This is now included in the random character group.)



    CP_OJ_HUNTCHALL,    //This is the medal-based hunting oddjob.


    CP_OJ_PS0T,         //Pilot School 0a - Takeoff
    CP_OJ_PS0L,         //Pilot School 0b - Landing
    CP_OJ_PS1,          //Pilot School 1 Inverted Flight
    CP_OJ_PS2,          //Pilot School 2 Knife Flight
    CP_OJ_PS3,          //Pilot School 3 Loop the Loop
    CP_OJ_PS4,          //Pilot School 4 Fly Low
    CP_OJ_PS5,          //Pilot School 5 Daring Landing

    CP_OJ_PS6,          //Pilot School 6 Plane Obstacle Course
    CP_OJ_PS7,          //Pilot School 7 Heli Obstacle Course
    CP_OJ_PS8,          //Pilot School 8 Heli Speed Run
    CP_OJ_PS9,          //Pilot School 9 Parachute on to Target
    CP_OJ_PS10,         //Pilot School 10 Parachute on to moving target

    CP_OJ_PS11,         //Pilot School 11 Formation Display

    CP_OJ_RAM1,         //Rampage 1
    CP_OJ_RAM2,         //Rampage 2
    CP_OJ_RAM3,         //Rampage 3
    CP_OJ_RAM4,         //Rampage 4
    CP_OJ_RAM5,         //Rampage 5


    //Taxi - Modified order
    CP_OJ_TAX1,
    CP_OJ_TAX2,
    CP_OJ_TAX3,
    CP_OJ_TAX4,
    CP_OJ_TAX5,

    CP_OJ_TAX7,
    CP_OJ_TAX8,
    CP_OJ_TAX9,
    CP_OJ_TAX10,








    //Tonya Towing
    CP_OJ_TOW1,
    CP_OJ_TOW2,
    CP_OJ_TOW3,
    CP_OJ_TOW4,
    CP_OJ_TOW5,






    //Random Events
    //Newly added Random Events due to change in game finale that means no playable characters die.

    CP_RE_ABCAR1,   //Abandoned vehicle 1
    CP_RE_ABCAR2,   //Abandoned vehicle 2

    CP_RE_CGANGF,   //Countryside Gang Fight

    CP_RE_GANGINT,  //Gang Intimidation.
    
    CP_RE_HOMESEC,  //Homeland Security


    CP_RE_LURED,    //Luring Girl into Alley

    CP_RE_ESCPAP,   //Escape Paparazzi

    CP_RE_PICPOC,   //Pickpocket

    CP_RE_PLIFT1,   //PrisonerLift 1
    CP_RE_PLIFT2,   //PrisonerLift 2

    CP_RE_UNGHO,    //Ungrateful Whore.
    


    //Arrests 1 and 2.
    CP_RE_ARR1, 
    CP_RE_ARR2,


    //ATM Robbery.
    CP_RE_ATM,



    //Bike Thief City 1 and 2
    CP_RE_BTC1,
    CP_RE_BTC2,


    //Border Patrol 1 - 3
    CP_RE_BORP1,
    CP_RE_BORP2,
    CP_RE_BORP3,


    //Broken Down
    CP_RE_BKD1,
    CP_RE_BKD2,
    CP_RE_BKD3,
    CP_RE_BKD4,
    CP_RE_BKD5,

    CP_RE_BKD6,
    CP_RE_BKD7,
    CP_RE_BKD8,
    CP_RE_BKD9,
    CP_RE_BKD10,


    CP_RE_BUR1, //Burial
    CP_RE_BUS1, //Bus Tour


    //Car Thief 1, 2
    CP_RE_CTHF1,
    CP_RE_CTHF2, 


    //Chase Thieves City 1, 2
    CP_RE_CHTCIT1,
    CP_RE_CHTCIT2,



    //Chase Thieves Country 1, 2
    CP_RE_CHTCO1,
    CP_RE_CHTCO2,


    //Construction Accident
    CP_RE_CONA1,

    
    //Countryside Robbery
    CP_RE_COROB1,

    //Crash Rescue
    CP_RE_CRAR1,
    


    //Cult Shootout
    CP_RE_CULT1, 


    //Deal Gone Wrong
    CP_RE_DEAL1,



    //Dirt Bike Thief (Country)
    CP_RE_DIRT1,


    //Domestic
    CP_RE_DOM1,


    //Drunk Driver 1 and 2
    CP_RE_DRUD1,
    CP_RE_DRUD2,


    //Re Getaway ( now included - see bug 427950
    CP_RE_GET1,

    //Hitch Lift 1 to 5
    CP_RE_HITL1,
    CP_RE_HITL2,
    CP_RE_HITL3,
    CP_RE_HITL4,
    CP_RE_HITL5,

    
    //Kidnap Girl
    CP_RE_KIDN1,

    
    //Mountain Dancer
    CP_RE_MOUN1,

    
    //Mugging 1 to 5
    CP_RE_MUG1,
    CP_RE_MUG2,
    CP_RE_MUG3,
    CP_RE_MUG4,
    //CP_RE_MUG5, 782814 requested removal




    //Security Van 1,2,4, 5 to 7.
    CP_RE_SECV1,
    CP_RE_SECV2,
    CP_RE_SECV4,
    CP_RE_SECV5,
    CP_RE_SECV6,
    CP_RE_SECV7,
    CP_RE_SECV8,
    CP_RE_SECV9,
    CP_RE_SECV10,
    CP_RE_SECV11,



    //Shop Robbery 1,2
    CP_RE_SHOP1,
    CP_RE_SHOP2,


    //Stag Do Running Man
    CP_RE_STAG1,

    //Wanderer
    CP_RE_WAND1,


    //Simeon Yetarian,
    CP_RE_YETAR,




    //Omega Spaceship Part Collection
    CP_UFOP1,

    //Dreyfuss Letter Scraps Collection
    CP_DLS1,


    CP_CINEMA,

    CP_STNJMP,  //Stunt Jumps

    CP_UNDBRG,  //Under the Bridge

    CP_KNFFLT,  //Knife Flight



    CP_PRPTY,   //Property Purchase
    CP_BUYVEH,  //Buy vehicle from website
    CP_CHOP,    //Walk, play fetch with Chop the dog.
    CP_BOOTY,   //Organise a booty call.
    CP_HOOKER,  //Meet up with a hooker
    CP_HOLDUP,  //Shop Holdup
    


    CP_DIVCOLL, //DIving Pickups
    CP_SONCOLL, //Sonar Collections

    CP_YOGA,    //Yoga
    CP_CABLE,   //Cable Car Ride

    CP_CARWSH,  //Car Wash
    CP_FAIRG,   //Fairground
    
    CP_CLOTH,   //Clothes Purchase
    CP_CARMOD,  //Car Mod Purchase
    CP_BARBER,  //Get A Haircut
    CP_TATTOO,  //Get A Tattoo
    CP_BUYGUN,  //Weapon Purchase

    CP_WTCHTV,  //Watch TV

    CP_STOCKS,  //Deal in the Stock Market


    //Up for removal, should be replaced by 6 individual location activities
    CP_FRIENDS,

    CP_FR_BAR,  //Friends Bar
    CP_FR_CIN,  //Friends Cinema
    CP_FR_DAR,  //Friends Darts
    CP_FR_GOL,  //Friends Golf
    CP_FR_STR,  //Friends Strip Club
    CP_FR_TEN,  //Friends Tennis
	
	
	// New NG only REs
    CP_RE_DUEL,
    CP_RE_SEAPL,
	CP_RE_MONKEYPHOTO,



    MAX_COMP_PERCENTAGE_ENTRIES,

    UNUSED_DEFAULT
	
	//CLF mission complete enums
	,CP_CLFT = 0
	,CP_CLFF
	
	,CP_CLFTS1
	,CP_CLFTS2
	,CP_CLFTSF
	,CP_CLFCT
	,CP_CLFCL
	,CP_CLFCJ
	,CP_CLFCH
	,CP_CLFCD
	,CP_CLFCR
	
	,CP_CLFKP
	,CP_CLFKR
	,CP_CLFKS
	,CP_CLFK4
	,CP_CLFK5
	
	,CP_CLFRP
	,CP_CLFRC
	,CP_CLFRV
	,CP_CLFRS
	,CP_CLFRJ
	,CP_CLFRCLK
	
	,CP_CLFA1
	,CP_CLFAD
	,CP_CLFAT
	,CP_CLFAF
	
	,CP_CLFCSS
	,CP_CLFCSP1
	,CP_CLFCSP2
	,CP_CLFCSP3
	,CP_CLFCSH 
	
	,CP_CLFMD
	,CP_CLFMP
	,CP_CLFMR
	
	,CP_CLFASSP
	,CP_CLFASSR
	,CP_CLFASSC
	,CP_CLFASSG
	,CP_CLFASSS
	,CP_CLFASSH
	,CP_CLFASSV
	,CP_CLFASSN
	,CP_CLFASSY	
	
	,CP_CLFJET1
	,CP_CLFJET2
	,CP_CLFJET3
	,CP_CLFJETREP
	
	,CP_CLFRCAL1
	,CP_CLFRCAL2
	,CP_CLFRCAL3
	,CP_CLFRCAL4
	
	,CP_CLFRCME1
	,CP_CLFRCME2
	,CP_CLFRCME3
	,CP_CLFRCME4
	
	,CP_CLFRCAG1
	,CP_CLFRCAG2
	,CP_CLFRCAG3
	
	,CP_CLFRCCL1
	,CP_CLFRCCL2
	,CP_CLFRCCL3
		
	,MAX_COMP_PERCENTAGE_ENTRIES_CLF
	
	//norman
	,CP_ZSTRT = 0
	,CP_ZAMA 
	,CP_ZTRA 
	,CP_ZMIKE	
	,CP_ZHOME
	,CP_ZJIM 
	,CP_ZPAR 
	,CP_ZCURE
	,CP_ZRENG
	,CP_ZRMED
	,CP_ZRGUN
	,CP_ZFUEL
	,CP_ZAMMO
	,CP_ZMEDS
	,CP_ZFOOD
	,CP_ZRA  
	,CP_ZRB  
	,CP_ZRC  
	
	
	,MAX_COMP_PERCENTAGE_ENTRIES_NRM
	
    //Could put any extra "choice" enums required for phone checklist under here, so they don't get counted in 100 percent completion routines.
    //But would you be able to replay any choice missions? Mission names would need more unique keys because of save game stuff.
    //How would you know which choice was made? Ben registers the choice somewhere for me to interrogate when common "CHOICE" enum is set and I mark
    //the specific choice as included in phone replay list?
    //Do this when registration takes place? Use a separate included in replay list field.
    //
    /*
    CP_FH3A,
    CP_FH3B,
    CP_FH3C,


    CP_FH5A,
    CP_FH5B,

    CP_BH3A,
    CP_BH3B,


    MAX_ALL_MISSIONS_FOR_PHONE_LIST
    */

ENDENUM


//SET_PACKED_STAT_BOOL(PACKED_CHECKLIST_START+CP_FR_STR, TRUE)



//Uses 4 bytes...
ENUM enumChoiceMiss

    STANDARD_MISSION,
    CHOICE_MISSION

ENDENUM



ENUM enumSCCluster

    NO_CLUSTER,
    TAXI_CLUSTER,
    STUNTJUMP_CLUSTER,
    UNDERBRIDGE_CLUSTER

ENDENUM





//Should these be saved? Should be calculated pre-restoration.
INT g_Group_Num_of_Missions, g_Group_Num_of_Minigames, g_Group_Num_of_Oddjobs, g_Group_Num_of_RandomChars
INT g_Group_Num_of_RandomEvents, g_Group_Num_of_Miscellaneous, g_Group_Num_of_Friends
INT g_Quarter_Threshold_Group_Num_of_RandomEvents


//These are initialised in CompletionPercentage_Public.sch, as they need to be updated on each new addition or subtraction as a control value.
FLOAT g_f_Expected_Number_in_Missions_Group, g_f_Expected_Number_in_Minigames_Group, g_f_Expected_Number_in_Oddjobs_Group
FLOAT g_f_Expected_Number_in_RandomChars_Group, g_f_Expected_Number_in_RandomEvents_Group
FLOAT g_f_Expected_Number_in_Miscellaneous_Group, g_f_Expected_Number_in_Friends_Group



BOOL g_Trigger_CloudUpload_of_CP_Elements
BOOL g_OutputCompletedandUncompletedElements = FALSE





//These 7 constants should total no more or no less than 100.0!  You will get an assert if it doesn't.

CONST_FLOAT g_c_Missions_CombinedWeighting          55.0//50.0

CONST_FLOAT g_c_Minigames_CombinedWeighting         10.0

CONST_FLOAT g_c_Oddjobs_CombinedWeighting           0.0//5.0       //Number of Oddjobs included in 100 percent is now zero. Moving weighting to missions group. See 1534381 

CONST_FLOAT g_c_RandomChars_CombinedWeighting       10.0

CONST_FLOAT g_c_RandomEvents_CombinedWeighting      5.0

CONST_FLOAT g_c_Miscellaneous_CombinedWeighting     15.0

CONST_FLOAT g_c_Friends_CombinedWeighting            5.0








//A temporary store for holding the number of missions completed out of a certain grouping so far.
INT temp_Group_Num_of_Missions = 0, temp_Group_Num_of_Minigames = 0, temp_Group_Num_of_Oddjobs = 0, temp_Group_Num_of_RandomChars = 0
INT temp_Group_Num_of_RandomEvents = 0, temp_Group_Num_of_Miscellaneous = 0, temp_Group_Num_of_Friends = 0




//100 per cent checklist sub-percentages. These will probably move to non-globals once stat environment is available.

INT g_i_Story_Missions_Percentage           //Percentage of Story Missions Complete from total Story Missions available
INT g_i_Ambient_Missions_Percentage         //Percentage of Random Event + Random Character missions complete from combined total available
INT g_i_Oddjobs_Percentage                  //Percentage of Oddjobs + Minigames complete from combined total available
INT g_i_Misc_Percentage                     //Percentage of Miscellaneous + Friends complete from combined total available.
//g_i_Heists_Percentage



STRUCT structCompletionPercentageList
    



    TEXT_LABEL      CP_Label    //use these labels for rich presence crossover?
    FLOAT           CP_Weighting

    BOOL            CP_Marked_As_Complete
    //BOOL            CP_Assigned_as_included_by_Script


    enumGrouping    CP_Grouping

    enumChoiceMiss  CP_ChoiceMission

    INT             CP_EnumAndVariationData // Store both the enum value (SP_MISSIONS, SP_MINIGAME, etc) and the variation index into one int. 
                                            // Bits 0-6 stores the enum, bits 7-13 stores the variation. Necessary for reverse lookup from
                                            // completion enum back to other game data. -BenR
    
    enumSCCluster   CP_SocialClubClusterDisplay         //If these gameplay elements should only be displayed as a single item in the social club checklist
                                                        //then we need to specify this and the type they are.


    FLOAT           CP_DefinedLocationX     //Can be set by any script upon registration that may not have a defined starting location.
    FLOAT           CP_DefinedLocationY     //Required by social club checklist.

    //To round out the groupings do this...
    //Go through the g_CompletionPercentageList for the range of the enum and find out how many of that grouping exist by interrogating that element and keeping it as a 
    //global - do this once at the launch of CompletionPercentage_controller.sc
    //When a public function is called to specify that this mission, oddjob should be added then we go through an update procedure and check if all of that grouping is done.
    //if it is then we do a rounding and mark a bool as saying it has been done. The percentage total would work out via a branch whether to add the group weighting or 
    //individual weighting for each thing in the group if the entire group has not been complete.
    

    //In calculation procedure, we will have separate running totals for each grouping which are added together to give an overall percentage total.
    //If all of a grouping have been completed then we set that running total to the maximum combined group weighting so that the overall total cannot exceed 100 percent.

ENDSTRUCT






// Instance of CompletionPercentage struct
// The following are saved globals from here on.
STRUCT g_CompletionPercentageSavedData
    structCompletionPercentageList g_CompletionPercentageList[MAX_COMP_PERCENTAGE_ENTRIES] 

    //Need to store total percentage... needs saved.
    FLOAT g_Resultant_CompletionPercentage
    BOOL b_g_OneHundredPercentReached

    INT g_BitSet_SP_EventFeedEntryTracker

    INT g_BitSet_SP_EventFeedEntryTracker_2

ENDSTRUCT


CONST_INT MAX_ITEMS_IN_SP_EVENT_QUEUE 5

INT i_SP_QueueWindow_ShutTime = 300000//60000 //Test with one minute.

//Holds a queue of event updates for SP's portion of the social feed controller. 
STRUCT_GAME_PRESENCE_EVENT_STAT_UPDATE  SP_Queue_UpdateEventStructs[MAX_ITEMS_IN_SP_EVENT_QUEUE]
SP_MISSIONS                             SP_Queue_MissionTypes[MAX_ITEMS_IN_SP_EVENT_QUEUE]
BOOL                                    SP_Queue_IsFull[MAX_ITEMS_IN_SP_EVENT_QUEUE]

INT i_SP_Queue_Window_CurrentTime, i_SP_Queue_Window_StartTime

INT i_NumberOfEventsInSP_Queue = 0




//To be used in conjunction with g_BitSet_SP_EventFeedEntryTracker

CONST_INT   g_BS_SP_Event_DIST_DRIVING_ROADVEH_500              0
CONST_INT   g_BS_SP_Event_DIST_DRIVING_ROADVEH_5000             1
CONST_INT   g_BS_SP_Event_DIST_DRIVING_ROADVEH_50000            2

CONST_INT   g_BS_SP_Event_TIMES_COMBINED_BUSTED_10              3
CONST_INT   g_BS_SP_Event_TIMES_COMBINED_BUSTED_25              4
CONST_INT   g_BS_SP_Event_TIMES_COMBINED_BUSTED_50              5
CONST_INT   g_BS_SP_Event_TIMES_COMBINED_BUSTED_100             6
CONST_INT   g_BS_SP_Event_TIMES_COMBINED_BUSTED_250             7

CONST_INT   g_BS_SP_Event_TIMES_COMBINED_WASTED_10              8
CONST_INT   g_BS_SP_Event_TIMES_COMBINED_WASTED_25              9
CONST_INT   g_BS_SP_Event_TIMES_COMBINED_WASTED_50              10
CONST_INT   g_BS_SP_Event_TIMES_COMBINED_WASTED_100             11
CONST_INT   g_BS_SP_Event_TIMES_COMBINED_WASTED_250             12

CONST_INT   g_BS_SP_Event_COMBINED_SHOTS_MILLION_1              13
CONST_INT   g_BS_SP_Event_COMBINED_SHOTS_MILLION_2              14
CONST_INT   g_BS_SP_Event_COMBINED_SHOTS_MILLION_3              15
CONST_INT   g_BS_SP_Event_COMBINED_SHOTS_MILLION_4              16
CONST_INT   g_BS_SP_Event_COMBINED_SHOTS_MILLION_5              17

CONST_INT   g_BS_SP_Event_EVADED_5STAR_WANTED_LEVEL             18


CONST_INT   g_BS_SP_Event_DIST_DRIVING_FLYINGVEHICLE_500        19
CONST_INT   g_BS_SP_Event_DIST_DRIVING_FLYINGVEHICLE_5000       20
CONST_INT   g_BS_SP_Event_DIST_DRIVING_FLYINGVEHICLE_50000      21


CONST_INT   g_BS_SP_Event_DIST_RAN_50                           22
CONST_INT   g_BS_SP_Event_DIST_RAN_100                          23
CONST_INT   g_BS_SP_Event_DIST_RAN_1000                         24

CONST_INT   g_BS_SP_Event_DIST_SWAM_50                          25
CONST_INT   g_BS_SP_Event_DIST_SWAM_100                         26
CONST_INT   g_BS_SP_Event_DIST_SWAM_1000                        27

//Fill spare to 31 with a few one shots...






//To be used with in conjunction withg_BitSet_SP_EventFeedEntryTracker_2

CONST_INT   g_BS2_SP_Event_WEB_BOUGHT_CAR                       0
CONST_INT   g_BS2_SP_Event_WEB_BOUGHT_RHINO                     1
CONST_INT   g_BS2_SP_Event_WEB_BOUGHT_BUZZARD                   2


CONST_INT   g_BS2_SP_Event_STOCK_BIG_INVESTMENT                 3
CONST_INT   g_BS2_SP_Event_STOCK_BIG_WIN                        4
CONST_INT   g_BS2_SP_Event_STOCK_BIG_LOSS                       5


CONST_INT   g_BS2_SP_Event_UNDERTHEBRIDGE_25PC                  6
CONST_INT   g_BS2_SP_Event_UNDERTHEBRIDGE_50PC                  7
CONST_INT   g_BS2_SP_Event_UNDERTHEBRIDGE_75PC                  8
CONST_INT   g_BS2_SP_Event_UNDERTHEBRIDGE_100PC                 9


CONST_INT   g_BS2_SP_Event_STUNTJUMPS_25PC                      10
CONST_INT   g_BS2_SP_Event_STUNTJUMPS_50PC                      11
CONST_INT   g_BS2_SP_Event_STUNTJUMPS_75PC                      12
CONST_INT   g_BS2_SP_Event_STUNTJUMPS_100PC                     13


CONST_INT   g_BS2_SP_Event_FOUND_ALL_RANDOM_EV                  14

CONST_INT   g_BS2_SP_Event_DRIVEN_ALL_CARS                      15







/*
//Up for removal! See Bug Change: 955537 
HASH_ENUM  MINIGAME_PRESENCE_HASH_ENUM

    //Offroad races.
    MGP_OFFROAD1,
    MGP_OFFROAD2,
    MGP_OFFROAD3,
    MGP_OFFROAD4,
    MGP_OFFROAD5,
    MGP_OFFROAD6,
    
    //Shooting range - We'll probably need more / less variations for each individual weapon type in the shooting range classes and perhaps cater for dlc expansion.
    MGP_SHOOTING_RANGE_HANDGUNS_1,
    MGP_SHOOTING_RANGE_HANDGUNS_2,
    MGP_SHOOTING_RANGE_HANDGUNS_3,
    MGP_SHOOTING_RANGE_HANDGUNS_4,

    MGP_SHOOTING_RANGE_SUBMACHINE_1,
    MGP_SHOOTING_RANGE_SUBMACHINE_2,
    MGP_SHOOTING_RANGE_SUBMACHINE_3,
    MGP_SHOOTING_RANGE_SUBMACHINE_4,

    MGP_SHOOTING_RANGE_ASSAULT_1,
    MGP_SHOOTING_RANGE_ASSAULT_2,
    MGP_SHOOTING_RANGE_ASSAULT_3,
    MGP_SHOOTING_RANGE_ASSAULT_4,

    MGP_SHOOTING_RANGE_LIGHTMACHINE_1,
    MGP_SHOOTING_RANGE_LIGHTMACHINE_2,
    MGP_SHOOTING_RANGE_LIGHTMACHINE_3,
    MGP_SHOOTING_RANGE_LIGHTMACHINE_4,

    MGP_SHOOTING_RANGE_HEAVYGUN_1,
    MGP_SHOOTING_RANGE_HEAVYGUN_2,
    MGP_SHOOTING_RANGE_HEAVYGUN_3,
    MGP_SHOOTING_RANGE_HEAVYGUN_4,

    MGP_SHOOTING_RANGE_SHOTGUN_1,
    MGP_SHOOTING_RANGE_SHOTGUN_2,
    MGP_SHOOTING_RANGE_SHOTGUN_3,
    MGP_SHOOTING_RANGE_SHOTGUN_4,

    

    MGP_TENNIS,
    MGP_DARTS,
    MGP_GOLF,


    //Basejumping and Skydiving - these are both known as parachuting externally but were originally set up slightly differently. We'll need an entry for each jump, so probably need expanded.
    MGP_PARACHUTING_1,
    MGP_PARACHUTING_2,
    MGP_PARACHUTING_3,
    MGP_PARACHUTING_4,
    MGP_PARACHUTING_5,
    MGP_PARACHUTING_6,
    MGP_PARACHUTING_7,
    MGP_PARACHUTING_8,
    MGP_PARACHUTING_9,
    MGP_PARACHUTING_10,


    //Sea Races.
    MGP_SEARACE_1,
    MGP_SEARACE_2,
    MGP_SEARACE_3,
    MGP_SEARACE_4,


    //Street races
    MGP_STREETRACE_1,
    MGP_STREETRACE_2,
    MGP_STREETRACE_3,
    MGP_STREETRACE_4,
    MGP_STREETRACE_5,

    //Triathlon
    MGP_TRIATHLON1,
    MGP_TRIATHLON2,
    MGP_TRIATHLON3,

    
    //Flight / Pilot School.
    MGP_FLIGHTSCHOOL_1,
    MGP_FLIGHTSCHOOL_2,
    MGP_FLIGHTSCHOOL_3,
    MGP_FLIGHTSCHOOL_4,
    MGP_FLIGHTSCHOOL_5,
    MGP_FLIGHTSCHOOL_6,
    MGP_FLIGHTSCHOOL_7,
    MGP_FLIGHTSCHOOL_8,
    MGP_FLIGHTSCHOOL_9,
    MGP_FLIGHTSCHOOL_10,
    MGP_FLIGHTSCHOOL_11,
    MGP_FLIGHTSCHOOL_12

ENDENUM
*/

