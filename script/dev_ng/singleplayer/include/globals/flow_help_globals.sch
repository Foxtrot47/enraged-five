//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 09/02/12			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					 		Flow Help Text Globals								│
//│																				│
//│				Globals used by the flow help text queuing system.				│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "code_control_globals.sch"
USING "script_maths.sch"

CONST_INT 	FLOW_HELP_QUEUE_SIZE	9
CONST_INT	FLOW_HELP_BITSET_COUNT	3

BOOL 				g_bFlowHelpPaused 			= FALSE
BOOL 				g_bFlowHelpDisplaying 		= FALSE
INT					g_iFlowHelpDisplayStartTime	= 0
INT					g_iFlowHelpDelayTime		= 0
TEXT_LABEL_15 		g_txtFlowHelpLastDisplayed 	= ""

INT					g_iFlowHelpFirstPersonTimer	= -1

ENUM FlowHelpPriority
	FHP_LOW,
	FHP_MEDIUM,
	FHP_HIGH,
	FHP_VERY_HIGH
ENDENUM

STRUCT FlowHelp
	TEXT_LABEL_15 		tHelpText
	TEXT_LABEL_15		tAddText
	INT 				iStartTime
	INT 				iDuration
	INT					iExpirationTime
	INT					iCharBitset
	FlowHelpPriority	ePriority
	CC_CodeID			eCodeIDStart
	CC_CodeID			eCodeIDDisplayed
	INT					iSettingsFlags 			
ENDSTRUCT

STRUCT FlowHelpSaved
	FlowHelp 			sHelpQueue[FLOW_HELP_QUEUE_SIZE]
	INT 				iFlowHelpCount
	FlowHelpPriority	eFlowHelpPriority[3]
	
	//Bitset to remember which "one time only" help messages have been displayed.
	//Indexed by the FlowHelpMessage enum.
	INT					iHelpDisplayedBitset[FLOW_HELP_BITSET_COUNT]
ENDSTRUCT


//Flow Help Settings bit flags.
CONST_INT	FHF_NONE						0
CONST_INT	FHF_SAVED						BIT0
CONST_INT	FHF_ALLOW_IN_TRIGGER			BIT1


//Flow Help Settings bit flag indexes.
CONST_INT	FHF_INDEX_SAVED					0
CONST_INT	FHF_INDEX_ALLOW_IN_TRIGGER		1

