//HASH("IG_ORLEANS")
CONST_INT	SASQUATCH_MODEL_BASE			729479320
CONST_INT	SASQUATCH_MODEL_OFFSET			911855321

//HASH("MP_M_FREEMODE_01")
CONST_INT	BEAST_MODEL_BASE				1293251340
CONST_INT	BEAST_MODEL_OFFSET				591982310

//HASH("A_C_ROTTWEILER")
CONST_INT	ROTTWEILER_MODEL_BASE			-1201509860
CONST_INT	ROTTWEILER_MODEL_OFFSET			-587155455

//HASH("A_C_RABBIT_01")
CONST_INT	RABBIT_MODEL_BASE				-577131543
CONST_INT	RABBIT_MODEL_OFFSET				35369112

//HASH("A_C_BOAR")
CONST_INT	BOAR_MODEL_BASE					-657378037
CONST_INT	BOAR_MODEL_OFFSET				-175195287

//HASH("A_C_DEER")
CONST_INT	DEER_MODEL_BASE					-574319886
CONST_INT	DEER_MODEL_OFFSET				-89733213

//HASH("EMPEROR2")
CONST_INT	CAR_WRECK_MODEL_BASE			-658480725
CONST_INT	CAR_WRECK_MODEL_OFFSET			-1224521423
	
//HASH("SCORCHER")
CONST_INT	MOUNTAIN_BIKE_MODEL_BASE		-186736009
CONST_INT	MOUNTAIN_BIKE_MODEL_OFFSET		198558
	
//HASH("U_M_Y_CYCLIST_01")
CONST_INT	MOUNTAIN_BIKER_MODEL_BASE		822941422			
CONST_INT	MOUNTAIN_BIKER_MODEL_OFFSET		-66984451			
	
//HASH("A_C_MTLION")
CONST_INT	LION_MODEL_BASE					241519279
CONST_INT	LION_MODEL_OFFSET				65768715
	
//HASH("DUNE")
CONST_INT	BUGGY_MODEL_BASE				-1203932058
CONST_INT	BUGGY_MODEL_OFFSET				-457922135

//HASH("IG_HUNTER")
CONST_INT	HUNTER_MODEL_BASE				1471434108
CONST_INT	HUNTER_MODEL_OFFSET				59784112

//HASH("A_M_Y_HIKER_01")
CONST_INT	HIKER_MODEL_BASE				468638912
CONST_INT	HIKER_MODEL_OFFSET				889741132

//HASH("A_F_Y_HIPPIE_01")
CONST_INT	HIPPIE_MODEL_BASE				57462762
CONST_INT	HIPPIE_MODEL_OFFSET				285796413

//HASH("A_M_M_HILLBILLY_01")
CONST_INT	REDNECK_MODEL_BASE				1027449508
CONST_INT	REDNECK_MODEL_OFFSET			794658213
	

//================================ Beast Peyote ==================================
VECTOR g_vPeyoteDaySfxPosition[7]
VECTOR g_vPeyoteDayHintPosition[7]


//================================ Beast Hunt ==================================
CONST_INT		BH_Bit_Peyote_x			1
CONST_INT		BH_Bit_Peyote_y			2
CONST_INT		BH_Bit_Peyote_z			4
CONST_INT		BH_Bit_Hunt_x			8
CONST_INT		BH_Bit_Hunt_y			16
CONST_INT		BH_Bit_Hunt_z			32
CONST_INT		BH_Bit_Fight_x			64
CONST_INT		BH_Bit_Fight_y			128
CONST_INT		BH_Bit_Fight_z			256
CONST_INT		BH_Bit_Peyote_Init		512
CONST_INT		BH_Bit_Hunt_Init		1024
CONST_INT		BH_Bit_Fight_Init		2048


CONST_INT		BH_Bit_Data_Finished	511	//All x/y/z data sets finished
INT 			g_iBeastSetupInt = 0



CONST_INT 		BH_CHECKPOINT_COUNT					11
CONST_INT 		BH_MAX_PATH_COUNT					64
CONST_INT 		BH_NODES_PER_PATH					4

STRUCT StructBeastHuntPath
	INT iLength
	VECTOR vNode[BH_NODES_PER_PATH]
ENDSTRUCT



STRUCT BeastWhiteFade
	FLOAT 	fDelta
	FLOAT 	fTargetAlpha
	FLOAT 	fCurrentAlpha
	INT 	iLastFrameUpdate
ENDSTRUCT
BeastWhiteFade g_sWhiteFade

VECTOR g_vBHCheckpoints[BH_CHECKPOINT_COUNT]						// Base positions of checkpoints that paths lead betwen.
VECTOR g_vBHNodes[BH_MAX_PATH_COUNT/2][BH_NODES_PER_PATH]			// Number of nodes for all paths. This likely needs increasing
INT g_iBHCheckpointType[BH_CHECKPOINT_COUNT]						// What kind of hint spawns at each checkpoint.
INT g_iBHPathIndexes[BH_CHECKPOINT_COUNT][BH_CHECKPOINT_COUNT]		// Array index for paths between pairs of checkpoints.
BOOL g_bBHPathReversed[BH_CHECKPOINT_COUNT][BH_CHECKPOINT_COUNT]	// Should paths between pairs of checkpoints be traversed in reverse.
StructBeastHuntPath g_sBHPath[BH_MAX_PATH_COUNT]					// Data for each path between checkpoints.



//================================ Beast Fight ==================================
VECTOR g_vBeastFinaleRootPosition, g_vBeastFinaleCheckPos
VECTOR g_vBeastFinaleArenaPos, g_vBeastFinaleArenaSize

VECTOR g_vBeastFinaleAnimalSpawnVec[7]
FLOAT g_vBeastFinaleAnimalSpawnHead[7]

VECTOR g_vBeastFinaleStartPoint, g_vBeastFinaleJumpOffPoint


TIMEOFDAY  g_todCurrent

CONST_INT		BF_MAX_ANIMALS_TO_SPAWN				15
CONST_INT		BF_ANIMAL_SPAWN_POINT_COUNT			7
CONST_INT		BF_NUM_TAUNT_ANIMS					3

CONST_INT		BF_DEFAULT_WANTED_LVL				5

ENUM BeastFightState
	BFS_Intro,
	BFS_Approach_Player,
	BFS_Attack_Player,
	BFS_Let_Player_Get_Up,
	BFS_Call_Backup,
	BFS_Let_Dogs_Attack,
	BFS_Run_Away,
	BFS_Death	
ENDENUM


STRUCT BeastFinaleVars
	INT iState
	INT iSoundID
	INT iSyncScene
	INT	iTimer, iTimer2, iTimerStage, iTimerHit
	INT iStartWantedLevel = BF_DEFAULT_WANTED_LVL
	INT iDeadFrames, iUnderCarTime, iHitCount
	FLOAT fPushForce = 50.0
	
	PED_INDEX pedBeast
	REL_GROUP_HASH relGroupHashBeast
	BeastFightState eBeastFightingState
	INT iProg		//Progress in the current state
	
	PED_INDEX pedAnimals[BF_MAX_ANIMALS_TO_SPAWN]
	SEQUENCE_INDEX seqAnimalAttack[BF_MAX_ANIMALS_TO_SPAWN]
	TEXT_LABEL_63 sTauntAnimDicts[BF_NUM_TAUNT_ANIMS+1]
	TEXT_LABEL_23 sTauntAnims[BF_NUM_TAUNT_ANIMS+1]
	TEXT_LABEL_31 sTransformAnimDict, sTransformAnim
	TEXT_LABEL_31 sBeastSfx, sKneelingAnim, sRelGroup
	TEXT_LABEL_31 sFightMusic, sFightMusicStop, sBeastCalls, sFMEvents
	
	
	#IF IS_DEBUG_BUILD
		BOOL bDebugTerminateThread
		BOOL bDebugWarpToLocation
		BOOL bDebugCompleteHunt
		BOOL bDebugDrawTriggers
		BOOL bDebugDrawingEnabled
		BOOL bDebugBeastDamaged
		BOOL bDebugDrawAnimalSpawns
		BOOL bDebugBeastKilledAndUnlocked
		BOOL bDebugToggleBeastKilled
		BOOL bDebugSetBeastLowHealth
	#ENDIF
	
ENDSTRUCT

//From main_persistent.sc 		- Purposefully "hidden" in amongst this script. We want it to be hard
//							 	- for hackers to spot these scripts in decompiled output. -BenR
#IF FEATURE_SP_DLC_BEAST_SECRET
#IF NOT FEATURE_SP_DLC_BEAST_SECRET_DEBUG
	BeastFinaleVars sBeastFinaleVars
#ENDIF
#ENDIF

#IF IS_DEBUG_BUILD

INT g_iBeastWidget = 0

STRUCT DecalPlacementData
	BOOL bUseMousePosition
	
	INT iType
	VECTOR vPosition
	FLOAT fHeading
	FLOAT fRoll
	BOOL bNormaliseSide
	FLOAT fWidth
	FLOAT fHeight
	FLOAT fColorR
	FLOAT fColorG
	FLOAT fColorB
	FLOAT fColorA
	BOOL bLongRange
	BOOL bDynamic
	BOOL bComplexCollision
	
	DECAL_ID id
	BOOL bCreate
	BOOL bDelete
	BOOL bWriteToFile
	
	BOOL bCreated
ENDSTRUCT

#ENDIF
