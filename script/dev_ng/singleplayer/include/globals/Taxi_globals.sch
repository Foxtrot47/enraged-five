// Taxi_globals.sch
//USING "shop_public.sch"
USING "controller_Taxi.sch"
USING "generic.sch"
USING "Taxi_Stats.sch"
USING "Taxi_Dialogue.sch"
USING "Taxi_Radio_Control.sch"
USING "Taxi_locations.sch"
USING "Script_maths.sch"

// We're only really using bits 1-5 to store the rank (value 1-31).
// The rest I'm using to pass data. Really, 1024 is the 10th bit, which is far too much to store rank, but we'll still
// never need all of the 21 bits afterward.
ENUM TAXI_BOOL_BITS
	TAXI_BOOL_TAXIHEALTHHELP			= BIT0,
	TAXI_BOOL_CANCELHELP_SHOWN			= BIT1,
	TAXI_BOOL_SUPPRESSING_TAXI 			= BIT2,
	TAXI_BOOL_MISSIONSAVAILABLE 		= BIT3,
	TAXI_BOOL_FIRSTTIMEINTAXI 			= BIT4,
	TAXI_BOOL_MISSEDJOB					= BIT5,
	TAXI_BOOL_CONTROLLER_RUNNING 		= BIT6,
	TAXI_BOOL_MISSION_RUNNING 			= BIT7,
	
	TAXI_BOOL_NEED_CLEAR_MISSION_CAND 	= BIT8,
	
	TAXI_BOOL_ALL_MISSIONS_DONE 		= BIT9,
	
	TAXI_BOOL_PASSED					= BIT10,
	TAXI_BOOL_INIT_MISSIONS				= BIT11,
	TAXI_BOOL_SCORECARD_IS_DRAWING		= BIT12,
	TAXI_BOOL_PLAYER_IN_TAXI			= BIT13,
	TAXI_BOOL_ANOTHER_JOB	 			= BIT14,
	TAXI_BOOL_WANTED_HELP	 			= BIT15,
	
	//<!-- 1. Need Excitement -->		//<!-- 2. Take It Easy-->		//<!-- 3. Deadline -->
	//<!-- 4. Got Your Back -->			//<!-- 5. Take To Best -->		//<!-- 6. I Know the Way -->  // I Know the Way is now out
	//<!-- 7. Cut You In -->			//<!-- 8. Got You Now -->		//<!-- 9. Clown Car -->	//<!-- 10. Follow That Car -->
	TAXI_BOOL_Procedural				= BIT19,
	TAXI_BOOL_NeedExcitement			= BIT20,
	TAXI_BOOL_TakeItEasy				= BIT21,
	TAXI_BOOL_Deadline					= BIT22,
	TAXI_BOOL_GotYourBack				= BIT23,
	TAXI_BOOL_TakeToBest				= BIT24,
	//TAXI_BOOL_IKnowWay					= BIT25,
	TAXI_BOOL_CutYouIn					= BIT26,
	TAXI_BOOL_GotYouNow					= BIT27,
	TAXI_BOOL_ClownCar					= BIT28,
	TAXI_BOOL_FollowThatCar				= BIT29,
	TAXI_BOOL_LAUNCHED_VIA_DEBUG		= BIT30
ENDENUM

STRUCT TAXI_MISSION_DATA
	BOOL		bIsComplete
	BOOL		bMissionFailed
ENDSTRUCT

STRUCT TaxiMissionSaved
	INT iGenericTaxiData

	// Stores the missions we should play, in order. Also has a flag that dictates if that mission can be selected
	// via our proximity check. Missions are removed as they are successfully completed.
	TAXI_MISSION_DATA	missions[TXM_NUM_TYPES]		
	
	INT	iTaxiStats[TAXI_STATS_NUM_STATS]				//Keep track of all stats
	INT iTaxiOJ_NumRuns[TXM_NUM_TYPES]	//Keep track of the number of taxi oj attempts
ENDSTRUCT

ENUM TAXI_RIDE_STATE
	TRS_INIT_STREAM = 0,
	TRS_STREAMING,
	TRS_FINDING_LOCATION,
	TRS_SPAWNING,
	TRS_SPAWN_CAR,
	TRS_MANAGE_PICKUP,
	TRS_PASSENGER_ENTER,
	TRS_WAIT_FOR_TIME,
	TRS_WAIT_FOR_TAIL,
	TRS_DRIVING_PASSENGER,
	TRS_WAIT_FOR_FULL_STOP,
	TRS_SWITCH_JOB,
	TRS_SAVE_DAMSEL,
	TRS_CHASE_DRIVER,
	TRS_REVEAL_DESTINATION,
	TRS_WAIT_PARK,
	TRS_SEND_TO_STORE,
	TRS_WAIT_1ST_STOP,
	TRS_PRE_CUTSCENE,
	TRS_CUTSCENE,
	TRS_CUTSCENE_02,
	TRS_WAIT_FOR_PASSENGER,
	TRS_DROPPING_OFF,
	TRS_ELIMATE_ALL_ENEMIES,
	TRS_TIE_UP_LOSE_ENDS,
	TRS_ESCAPE_POLICE,
	TRS_POLICE_ESCAPED,
	
	TRS_REGULAR_PAYMENT,
	TRS_SPECIAL_ENDING,

	TRS_SCORECARD_GRADE,
	TRS_CLEANUP,
	TRS_END
ENDENUM

ENUM TAXI_FARE_TYPE
	TFT_INVALID = -1,
	TFT_TAKEITEASY,
	TFT_EXCITEMENT,
	TFT_DEADLINE,
	TFT_KNOWTHEWAY
ENDENUM

ENUM TAXI_FOLLOW_POSITION
	TAXI_FOLLOW_POS_NONE = 0,
	TAXI_LOSING_CHASE,
	TAXI_TOO_CLOSE_TARGET,
	TAXI_TOO_CLOSE_WARNING,
	TAXI_TOO_CLOSE_FAIL,
	TAXI_TOO_FAR_FAIL,
	TAXI_TOO_FAR_WARNING,
	TAXI_ARRIVE_WALK,
	TAXI_SWEET_SPOT,
	TAXI_SWEET_SPOT_BANTER,
	TAXI_HIT_CAR,
	TAXI_LEFT_CAR,
	TAXI_RETURN_OBJ
ENDENUM

ENUM TAXI_OJ_WANTED_STATES
	TWS_CHECK_IF_WANTED = 0,
	TWS_PRINT_OBJ_TO_LOSE_POLICE,
	TWS_CONFIRM_OBJ_LOSE_POLICE_PRINTED,
	TWS_CHECK_IF_PLAYER_LOST_WANTED,
	TWS_CONGRATULATE_PLAYER_ON_LOSING_WANTED,
	TWS_CLEANUP,
	TWS_NUM_STATES
ENDENUM

/*
STRUCT NearVehicleStruct
	VEHICLE_INDEX theVehicle
	INT iTime
ENDSTRUCT
*/
ENUM TAXI_TIP_VALUE
	TAXI_ZONE_GREAT = 1,
	TAXI_ZONE_GOOD,
	TAXI_ZONE_AVG,
	TAXI_ZONE_LOW,
	TAXI_ZONE_NONE
ENDENUM

CONST_INT	BONUS_STATUS_INITIALIZED		1
CONST_INT	BONUS_STATUS_AWARDED			2

STRUCT BONUS_FIELD
	INT		status
	INT		cash
	STRING	description
ENDSTRUCT

CONST_INT	MAX_BONUS_INFO			5

ENUM TAXI_FAIL_STRING

	TFS_TAXI_DISABLED,
	TFS_TAXI_ENGINE_FIRE,
	TFS_LOST_CAR,
	TFS_SPOTTED,
	TFS_WANTED,
	TFS_SPOOKED,
	TFS_HIT2,
	TFS_JACKED,
	TFS_ABANDONED_PASSENGER,
	TFS_TIME_EXPIRED,
	TFS_AGGROD,
	TFS_UNDER_WATER,
	TFS_CHASEE_SHOT,
	TFS_CHASEE_KILLED,
	TFS_PASSENGER_SHOT,
	TFS_PASSENGER_DIED,
	TFS_PASSENGER_SICK,
	TFS_PASSENGER_BORED,
	TFS_TAXI_GARBAGE_COLLECTED,
	TFS_TAXI_CANCELLED,
	TFS_TAXI_STOPPED,
	
	TFS_DEBUG_FAIL
ENDENUM

ENUM TAXI_TIMERS
	TT_GENERIC = 0,
	TT_STOP_STUCK,
	TT_STOPPED,
	TT_FAILDELAY,
	TT_IKW_DIRECTIONS,
	TT_PASSENGER_ACTION,
	
	TT_DROPOFF,
	TT_RIDETODEST,
	TT_FOLLOW,
	TT_POLICE,
	TT_BORING,
	TT_BANTER,
	TT_PENALTY,
	TT_COLLIDE,
	TT_NOTINCAB,
	TT_DIFFERENTCAR,
	TT_DIALOGUE,
	TT_INTERUPT,
	TT_DQUEUE,
	TT_CUTSCENE,
	TT_DELAY,
	TT_PUKING,

	TT_RDSTADDTL,
	TT_TIP_CHECK,
	TT_FOLLOW_CAR_AI,
	TT_UNDER_WATER,
	TT_CONTROL_CHECK,
	TT_AGGRO,
	
	TT_REGION_FIND_POINT,
	TT_REGION_TIME_POINT,
	TT_REGION_FIND_FAIL,
	
	TT_CLEANUP,
	TT_NUM_TIMERS
ENDENUM



//This is a structure designed to be declared by any script that wishes to 
//run a standard taxi mission. Just pass the struct into one of the functions
//in taxi support lib and watch the magic happen.
STRUCT TaxiStruct
	CAMERA_INDEX 		camTaxi					//a camera ref that we use for pickup/dropoff etc.
	CAMERA_INDEX		camInterp
	
	PED_INDEX 			piTaxiPlayer					//a ref to the player
	PED_INDEX 			piTaxiPassenger				//a ref to the passenger
	
	VEHICLE_INDEX 		viTaxi					//a ref to the current taxi vehicle the player is driving
	VEHICLE_INDEX 		viNearMiss				//a ref to near miss vehicles
	
	FLOAT				fTaxiEnterSpeed = PEDMOVE_WALK
	VEHICLE_SEAT		vsEnterSeat
	
	BLIP_INDEX 			blipTaxiPassenger			//blip for the passenger
	BLIP_INDEX 			blipTaxiDropOff				//blip for the destination
	BLIP_INDEX 			blipTaxiVehicle				//blip for your taxi in case you leave it during a mission
	
	VECTOR 				vTaxiOJSpawn					//vector coord of the passenger's spawn location
	VECTOR 				vTaxiOJPickup					//vector coord of the passenger's pickup location
	VECTOR 				vTaxiOJDropoff					//vector coord for the destination
	VECTOR				vTaxiOJ_WarpPt
	VECTOR 				vTaxiOJ_WarpPtPickup				// Used for J Skips--------------------
	VECTOR 				vTaxiOJ_WarpPtDropoff
	VECTOR 				vTaxiOJ_PassengerGoToPt			//Point of interest for passenger to go look at / standby after they are dropped off.
	
	FLOAT				fTaxiOJ_WarpPtHeading
	FLOAT 				fTaxiOJ_WarpPtHeadingPickup
	FLOAT 				fTaxiOJ_WarpPtHeadingDropoff	
	FLOAT 				fTaxiOJ_TimeDeadline					//amount of time given to the player to complete the fare
	
	//FLOAT fTaxiOJ_TimeETA						//estimated time of arrival to pickup the passenger
	FLOAT 				fTaxiOJ_BanterDelay					//how long to wait before triggering banter
	FLOAT 				fTaxiOJ_SpeedCurr
	FLOAT 				fTaxiOJ_SpeedPrev
	FLOAT				fTaxiOJ_TimeGood					//This keeps track of your current time goal and prints it to the tip debug screen
	FLOAT				fTaxiOJ_DistanceGood				//This keeps track of your current distance goal and prints it to the tip debug screen
	FLOAT				fTaxiOJ_CurrentMileage	=	0.0
	FLOAT				fTaxiOJ_InitialMileage	=	0.0
	FLOAT				fTaxiOJ_TempFloat
	
	INT					iTaxiJobBits						//Set various checks/on/off in the mission
	INT 				iStoredTaxiRank
	INT 				iOldVehicleHealth
	INT 				iTaxiOJ_HornHonkCount				//tracks number of times player honked horn
	INT 				iTaxiOJ_StatesPickup	=	0				//a switch iterator used when picking up a passenger, should change this into an enum but it only has 2 states
	INT 				iTaxiOJ_StatesGenIndex						//scrap switch iterator I use when needed
	INT 				iTaxiOJ_CashFare					//how much the base fare is for this dropoff			
	
	INT					iTaxiOJ_SpeedReducVols[3]
	//Tips
	INT					iTaxiOJ_TipsBitMask
	INT 				iTaxiOJ_CashTip
	INT					iTaxiOJ_CashTipToAdd
	INT					iTaxiOJ_CashTipAvg
	INT					iTaxiOJ_CashTipAmazing
	BONUS_FIELD 		iTaxiOJ_CashBonusInfo[MAX_BONUS_INFO]
	INT 				iTaxiOJ_PassengerExcitement			//Only used in Needs Excitement & Take It Easy
	INT 				iTaxiOJ_PassengerSatisfaction	//How satisfied is the passenger with the current ride----USED to handle passenger reactions and run out scenarios
	INT 				iTaxiOJ_DebugThrottle
	INT 				iTaxiOJ_CheatSel
	INT					iTaxiOJ_BanterCount					//Used by follow car, probably will be used for other things.  This is so we can play multiple banters in one mission
	//Dialogue Bits------------------------
	INT 				iTaxiOJ_BitsDialogue				//used as a bitmask to check which variations of dialogue have played
	INT 				iTaxiOJ_ExtraBitsDialogue			//used as a another bitmask to check which variations of dialogue have played
	INT					iTaxiOJ_ReactBitsDialogue			//new dialogue has been written for some reactions, so adding another bitfield
	INT 				iTaxiOJ_ObjGatesBits				//Used to make sure objs only display once
	INT 				iTaxiOJ_DXBitsPolice				//used to keep track of the police barks
	INT 				iTaxiOJ_DXBitsStopped				//used to keep track of bitching at the player when he's stopped
	INT 				iTaxiOJ_DXBitsSpeed					//used to keep track of bitching at the player when he's speeding
	INT 				iTaxiOJ_DXBitsOffroad				//used to keep track of remarking at the player when he's offroad
	INT 				iTaxiOJ_DXBitsGTime					//used to keep track of bitching at the player when he's on a deadline and making good time
	INT 				iTaxiOJ_DXBitsBTime					//used to keep track of bitching at the player when he's on a deadline and making bad time
	INT 				iTaxiOJ_DXBitsEnemyBanter
	INT 				iTaxiOJ_DXBitsEnemyBanter2
	INT					iTaxiOJ_DXBitsGenericMission
	INT					iTaxiOJ_DXBitsFC_SS_Banter
	INT					iTaxiOJ_DXBitsCollision
	INT					iTaxiOJ_DXBitsExcitedToBored
	INT					iTaxiOJ_DXBitsBoredToExcited
	INT 				iTaxiOJ_Bits_Gates
	INT					iTaxiOJ_DXBitsHail
	//-------------------------------------
	INT 				iTaxiOJ_PassengerReactBits
	INT 				iTaxiOJ_NumDXVariations				//Use this for num of different banter variations
	INT 				iTaxiOJ_GetRunModCount				//Stores the number of mission runs modded by the dialogue banter variations
	INT 				iTaxiOJ_NumCompliments				//Track num of times players has been complimented
	INT 				iTaxiOJ_NumDisses					//Track num of times player has been dissed
	INT 				iTaxiOJ_SpecialCash = 0
	INT 				iTaxiOJ_WantedLevel = 0
	
	INT					iTaxiOJ_AggroCount
	BOOL 				bTaxiOJ_DoSayNoThanks
	BOOL 				bTaxiOJ_Failed
	BOOL 				bTaxiOJ_CanSpeak
	BOOL				bBlipPassenger
	BOOL				bExcitedFlag 	= FALSE			// flags to be used for playing medium dialogue in NEX and TIE
	BOOL				bBoredFlag 		= FALSE
	BOOL				bIsTaxiOJInCombat = FALSE
	BOOL				bSecondPassenger
	BOOL				bSecondLeg
	
	//Emergency fail case
	BOOL				bEmergencyDlgStop
	TAXI_FAIL_STRING	tfsEmergencyFailString
	
	//Taxi Cheats - will convert to a bit flag if needed, but for now this bool will suffice
	BOOL 				bTaxiOJ_CheatDrawDists	= TRUE
	BOOL				bTaxiOJ_CheckRadioStation =FALSE
	STRING 				sTaxiOJ_Reason4Fail
	STRING 				sTaxiOJ_IDPrefix
	STRING 				sTaxiOJ_RideState
	TEXT_LABEL_23		sTaxiOJ_TipLine
	TEXT_LABEL_23		sTaxiOJ_FailGodLabel
	TEXT_LABEL_7		sFastTime
	
	BOOL				bPedEntering							//Bool to track if the passenger has been tasked to enter the taxi
	BOOL				bObjPrinted
	BOOL				bIsCurrentlyWanted
	
	BOOL				bPassengerObjPrinted
	BOOL				bIsTaxiDebugSkipping
	STRING 				sTaxiOJ_DXMissionID						//Mission ID from Dialogue Star 	
	STRING 				sTaxiOJ_DXSubtitleGroupID				//Subtitle Group ID from Dialogue Star
	
	STRING				sProceduralPassengerVoice
	
	structTimer 		tTimers[TT_NUM_TIMERS]
	
	SEQUENCE_INDEX 		siSeqIndex
	
	structPedsForConversation tTaxiOJ_Convo			//Conversation data
	//
	SHAPETEST_INDEX			tShapeTest
	
	TAXI_RIDE_STATE 		tTaxiOJ_RideState 
	
	TAXI_MISSION_TYPES 		tTaxiOJ_MissionType
	TAXI_OJ_WANTED_STATES 	tWantedStateIndex
	REL_GROUP_HASH 			relPassenger
	
	TAXI_FOLLOW_POSITION	tTaxiOJ_FollowPosition			//TODO : remove this and just track los & distance
	TAXI_FAIL_STRING 		tTaxiOJ_FailType
	
	TAXI_DIALOGUE_INDEX 	tTaxiOJ_DXIndex	=	TAXI_DISPATCH_DIRECTIONS
	TAXI_DIALOGUE_INDEX		tTaxiOJ_ObjectiveCurrent
	
	TAXI_RADIO_STRUCT 		tTaxiOJ_Radio						//Taxi Radio Station Data
	
	SHOP_TYPE_ENUM 			targetShopType

	STATSENUM 				sDrivingStat
	
	TIMEOFDAY 				eTODToPickUpPassenger
ENDSTRUCT

// === Taxi Globals. Taxi for player use.
VEHICLE_INDEX g_WaitingTaxi
PED_INDEX g_WaitingTaxiDriver
BLIP_INDEX g_BlipToIgnore
BLIP_INDEX g_OnlyMissionBlipToDisplayOnTaxiMeter
BLIP_INDEX g_FirstBlipInTaxiMeter
BLIP_INDEX g_CustomDropOffBlip
BLIP_SPRITE g_FirstBlipSpriteInTaxiMeter
VECTOR g_vTaxiDropOffPosition
VECTOR g_vCustomDropPosition	
FLOAT g_fTaxiDropOffHeading
FLOAT g_fCustomDropHeading
BOOL g_bTaxiIsWaiting							// potential remove - only seems to get set, never unset and not tested in the project
BOOL g_bPlayerIsInTaxi							// scripts can use this to test if the player is taking a ride as a passenger in a taxi
BOOL g_bTaxiShouldForcePhoneDislayOnHud = FALSE	// B*2053172 - set when phone needs to be forced to display on the hud if active
BOOL g_bTaxiHailingIsUnlocked = TRUE			// never gets disabled currently
BOOL g_bTaxiHailingIsDisabled = FALSE			// set by scripts to temporarily block player being allowed to hail or enter taxis as a passenger
BOOL g_bRequestTaxiServiceCleanup = FALSE		// set by the replay system to request the taxiService.sc script cleans up

BOOL g_bPlayerCannotAffordToSkip				// MP only appears to be used to trigger different dialogue
BOOL g_bTaxiCamIs1stPerson						// MP only (but looks like it should be removed since it's only tested in cellphone_flashhand.sc using a SP check)

BOOL g_bTaxiFirstProcedural						// Taxi procedural only
BOOL g_bTaxiProceduralRunning					// Taxi procedural only	

TEXT_LABEL_23 g_sTaxiHailingDisabledByThisScript = "NULL"
TEXT_LABEL_23 g_sTaxiEntitiesOwnedByThisScript
INT g_iTaxiHailedTime

CONST_INT NUMBER_OF_LAST_DESTINATIONS 4

INT g_iLastTaxiPickupLocation[NUMBER_OF_LAST_DESTINATIONS]
INT g_iLastTaxiPickupRegion[NUMBER_OF_LAST_DESTINATIONS]

TAXI_PROCEDURAL_LOCATIONS g_LastFewDestinations[NUMBER_OF_LAST_DESTINATIONS] // 
TAXI_PROCEDURAL_DISTANCE g_LastProceduralDistance

