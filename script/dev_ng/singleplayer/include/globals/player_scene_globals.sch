USING "commands_task.sch"

//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	player_scene_globals.sch									//
//		AUTHOR			:	Alwyn Roberts												//
//		DESCRIPTION		:	Enums and structs for use with current player ped.			//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////
///    Structs/Enums for the player ped scenes
CONST_INT NUM_OF_PLAYER_PED_STORED_COORDS 19
ENUM PED_REQUEST_SCENE_ENUM
	/* player char is dead, thats his previous scene! */
	PR_SCENE_DEAD	= 0,
	PR_SCENE_HOSPITAL,
	
	/* friend scene - for when switching to a friend activity */
	PR_SCENE_M_OVERRIDE,
	PR_SCENE_F_OVERRIDE,
	PR_SCENE_T_OVERRIDE,
	
	/* default scenes - for when switching back within 1hr */
	PR_SCENE_M_DEFAULT,
	PR_SCENE_F_DEFAULT,
	PR_SCENE_T_DEFAULT,
	
	/* after-mission scenes - to trigger for 1hr AFTER that mission has passed */
	PR_SCENE_Fa_PHONECALL_ARM3,
	PR_SCENE_Fa_PHONECALL_FAM1,
	PR_SCENE_Fa_PHONECALL_FAM3,

	PR_SCENE_Fa_STRIPCLUB_ARM3,
//	PR_SCENE_Ma_ARM3,				// Fired one game-hour if PR_SCENE_Fa_STRIPCLUB_ARM3 was triggered
//	PR_SCENE_Fa_STRIPCLUB_FAM1,
//	PR_SCENE_Fa_STRIPCLUB_FAM3,
	
	PR_SCENE_Ma_FAMILY1,			// url:bugstar:963245
	PR_SCENE_Fa_FAMILY3,			// franklin driving down road 200 + meters from michaels house
	PR_SCENE_Fa_FBI1,				// limit switch scenes to one of the following - F_GARBAGE, F_TACO_FARMER, F_BAR, F0_SH_READING, F0_SH_PUSHUP, F_GARBAGE, F_TRAFFIC, F0_WALKCHOP
	PR_SCENE_Ta_FBI2,				// trevor driving near by drop off location for fbi2 custom switch
	PR_SCENE_Fa_FBI4intro,			// Seems that Franklin is too far away considering how much time has passed (about a minute in real time) since the fbi4_intro cutscene at the warehouse
	PR_SCENE_Ma_FBI4intro,			// Seems that Trevor is too far away considering how much time has passed (about a minute in real time) since the fbi4_intro cutscene at the warehouse
	PR_SCENE_Ma_FBI3,				// FIB 3 - Michael in car near FIB building as if dropped off Dave.
	PR_SCENE_Fa_FBI4,				// FIB 4 - Franklin on way home from burning vehicle location.
	PR_SCENE_Fa_FBI5,				// franklin driving towards city
	PR_SCENE_Ma_FBI5,				// michael driving from airport towards city
//	PR_SCENE_Ma_FAMILY4_a,			// if you switch immediatley lets have just out the storm drain if you wait 30 seconds or so have him at the car
//	PR_SCENE_Ma_FAMILY4_b,			// michael getting into him car at the stadium, say something like Lazlow is a dick, no custom anims needed
	PR_SCENE_Ta_FAMILY4,			// Give me a shout about the switch from Trevor after Family 4
	PR_SCENE_Fa_AGENCY1,			// can we have franklin saying got to find this guy - keep current scene of Franklin in traffic
	PR_SCENE_Fa_AGENCYprep1,			// hey Alwyn could we add a custom switch for Franklin after Agency Heist Planning intro to 3A. If possible have Franklin (in the vehicle he arrived at mission blip in) driving at 745, -464, 36, or on foot at 790, -744, 27? 
	PR_SCENE_Fa_CARSTEAL1,			// After this mission the switch will need some unique ones for Franklin as they drive off in a car
	PR_SCENE_Ta_CARSTEAL1,			// After this mission the switch will need some unique ones for Trevor as they drive off in a car
	PR_SCENE_Fa_AGENCY2,			// Franklin after Agency 2 - walking along street away from sweatshop at 763, -906, 25
	PR_SCENE_Ta_CARSTEAL2,			// can we have trevor in the police helicopter flying nearish to the garage from end cutscene
//	PR_SCENE_Ta_CARSTEAL4,			// limit switch scenes to one of the following - T_CR_CHILI, T_CR_TACO, T_KONEIGHBOUR, T_SCARETRAMP, T_CR_CHASEBIKE, T_CR_RUDEATCAFE, T_FIGHTBAR_a, T_FIGHTBAR_b, T_YELLATDOORMAN, T_FLOYD_DOLL, T_FLOYDHARASS_C, T_FLOYDKNEE
	PR_SCENE_Fa_FBI2,				// FIB 2 - Franklin in area or at home
	PR_SCENE_Ta_FBI4,				// FIB 4 - Trevor anywhere in city, but switch that does not imply too much time has passed
	PR_SCENE_Fa_DOCKS2B,			// Port of LS 2B - Franklin at home.
	PR_SCENE_Ta_FAMILY6,			// can we force the Trevor switch scene after Family 6. If player switched to Trevor can we have either of two following switches... (PR_SCENE_T_SC_BAR, PR_SCENE_T_NAKED_GARDEN)
	PR_SCENE_Ta_FINALEprepD,		// Switched from Michael to Trevor after delivering the train and Trevor was on a mountain in a dress after just switching the track over and taking out the rail workers.
	PR_SCENE_Ma_FAMILY6,			// where are the kids?
	PR_SCENE_Fa_MARTIN1,			// after martin1 - need to force switch to franklin see imran
	PR_SCENE_Ma_TREVOR3,			// Specific switch to Michael and the end of Trevor 3 and before Michael 4
	PR_SCENE_Fa_TREVOR3,			// Pick a switch for after Trevor 3 on going back to Franklin, once Trevor has came into the city
	PR_SCENE_Fa_RURAL2A,			// franklin is driving towards town on bike
	PR_SCENE_Ta_RURAL2A,			// trevor is at his trailer
	PR_SCENE_Ta_RC_MRSP2,			// trevor is at his trailer (post-Mrs Philips 2)
	PR_SCENE_Ma_RURAL1,				// michael droping lester at motel
	PR_SCENE_FTa_FRANKLIN1a,		// url:bugstar:839434 & url:bugstar:1015706
//	PR_SCENE_FTa_FRANKLIN1b,		// 
	PR_SCENE_FTa_FRANKLIN1c,		// 
	PR_SCENE_FTa_FRANKLIN1d,		// 
	PR_SCENE_FTa_FRANKLIN1e,		// 
	PR_SCENE_Ma_EXILE2,				// have him back at trailer chilling
	PR_SCENE_Fa_EXILE2,				// have him by the chopper
	PR_SCENE_Ma_EXILE3,				// michael getting into car at motel - imply dropped something off - 'i ll leave that to lester he can sort that out'
	PR_SCENE_Fa_EXILE3,				// have him by the chopper
	PR_SCENE_Fa_MICHAEL3,			// Franklin driving away from end location to his safehouse		//between <<-1457.3401, -377.2540, 37.8909>> and SH
	PR_SCENE_Ta_MICHAEL3,			// Trevor driving away from end location to his safehouse			//between <<-1457.3401, -377.2540, 37.8909>> and SH
	PR_SCENE_Ma_DOCKS2A,			// Michael driving back to safehouse - changed out of wetsuit
//	PR_SCENE_Fa_DOCKS2A,			// Franklin back at safehouse (use a F1 scene)
	PR_SCENE_Fa_FINALE1,			// Franklin back at safehouse (use a F1 scene)
	PR_SCENE_Ta_FINALE1,			// Trevor back at the airstrip
	PR_SCENE_Ta_CARSTEAL4,			// Car Steal 4 - Trevor in Paleto Bay
	PR_SCENE_Fa_FINALE2intro,		// The Big Score 2 planning - Franklin within vicinity of strip club
	PR_SCENE_Ta_FINALE2intro,		// The Big Score 2 planning - Trevor within vicinity of strip club
//	PR_SCENE_Ma_DOCKS2B,			// Michael leaving airstrip, heading back to the city		//between <<1737.7271, 3290.5232, 40.1505>> and SH
//	PR_SCENE_Ma_MARTIN1,			// Michael driving from barn to trailer?	//<<2309.6731, 4885.9873, 40.8082>>
//	PR_SCENE_Ta_MARTIN1,			// Trevor in barn with Patricia?			//<<2309.6731, 4885.9873, 40.8082>>
//	PR_SCENE_Fa_AGENCY3B,			// Franklin inside of his house, with Lester on the laptop discussing communicating with Trevor.
	PR_SCENE_Ma_FRANKLIN2,			// Could have the switch scene for M in the countryside still. Though it is long enough that they could have made it back to the city.
	PR_SCENE_Ta_FRANKLIN2,			// Could have the switch scene for T in the countryside still. Though it is long enough that they could have made it back to the city.
	PR_SCENE_Ma_FBI1end,			// Michael in vehicle driving from oil fields
	PR_SCENE_Ma_DOCKS2B,			// Port of LS 2B - Michael driving to safehouse.
	PR_SCENE_Ma_FINALE1,			// The Big Score 1 - Michael in car driving away from Franklin’s hills house.
	PR_SCENE_Fa_AGENCY3A,			// Agency Heist 3A - Franklin driving away from Lester’s house
	PR_SCENE_Ma_FINALE2B,			// The Big Score 2B - Michael leaving city airport area in car
	PR_SCENE_Ta_FINALE2B,			// The Big Score 2B - Trevor leaving city airport area in car. Or still in heli?
	PR_SCENE_Fa_DOCKS2A,			// Port of LS 2A - Franklin at home.
	PR_SCENE_Fa_AGENCY3B,			// Agency Heist 3B - Franklin in his new house with Lester is there on computer.
	PR_SCENE_Ma_FINALE2A,			// The Big Score 2A - Michael Michael in safehouse PRM_4 watchingtv.
	PR_SCENE_Ta_FINALE2A,			// The Big Score 2A -  Trevor walking down street away from Michaels at -852, 111, 54
	
	PR_SCENE_Fa_FINALEA,			// Custom Switch after End Credits - if player chose Finale A Kill Trevor. Franklin first switch use switch PRF1_poolside_b
	PR_SCENE_Fa_FINALEB,			// Custom Switch after End Credits - if player chose Finale B Kill Michael. Franklin first switch use switch PRF1_poolside_b
	PR_SCENE_Ma_FINALEC,			// Custom Switch after End Credits - if player chose Finale C Save Michael and Trevor. Michael first switch – use PRM7_WifeTennis
	PR_SCENE_Fa_FINALEC,			// Custom Switch after End Credits - if player chose Finale C Save Michael and Trevor. Franklin first switch use PRF1_byetaxi
	PR_SCENE_Ta_FINALEC,			// Custom Switch after End Credits - if player chose Finale C Save Michael and Trevor. Trevor is driving back to trailer (2049, 3839, 34)
	
/*	PR_SCENE_FTa_FRANKLIN1,			// Franklin 1 - Need to discuss with Lukasz
	PR_SCENE_Xa_FRANKLIN2,			// Franklin 2  - Need to check if both on mission as optional.
	*/
	
	/* Magdemo */
	PR_SCENE_M_MD_FBI2,				// 
	PR_SCENE_F_MD_FRANKLIN2,		// 
	
	/* michael */
	PR_SCENE_M2_BEDROOM,			// Amanda and Michael are laying in bed and then Michael sits up on the side of the bed.
	PR_SCENE_M2_SAVEHOUSE0_b,		// Amanda and Michael are in bed together and Michael sits up on the side and then gets up.
	PR_SCENE_M2_SAVEHOUSE1_a,		//  
	PR_SCENE_M2_SAVEHOUSE1_b,		// Sat at table in garden - smoking/drinking if possible?
	PR_SCENE_M2_SMOKINGGOLF,		// Sat at table at golf club - smoking/drinking if possible?
	PR_SCENE_M4_WAKEUPSCREAM,		// In bed alone. Should get up when we Hotswap to him. Wife asleep in spare room.
	PR_SCENE_M4_WAKESUPSCARED,		// Michael wakes up from a dream, scared, he screams, grabs his gun and stands up at the side of the bed aiming his weapon
//	PR_SCENE_M4_HOUSEBED_b,			// Sleeping on couch, gets up.
	PR_SCENE_M6_HOUSEBED,			// Sleeping in Trevor’s trailer (during exile)
	PR_SCENE_M4_WATCHINGTV,			// Watching TV in the middle of the night
	PR_SCENE_M2_KIDS_TV,			// Watching TV with his kids
	PR_SCENE_M_POOLSIDE_a,			// Sunbathing by the pool.
	PR_SCENE_M_POOLSIDE_b,			// 
	PR_SCENE_M2_CARSLEEP_a,			// Michael asleep in car
	PR_SCENE_M2_CARSLEEP_b,			// location for new Michael switch scene where he asleep in his car. Alleyway near Micheal's house.
	PR_SCENE_M_CANAL_a,				// Bench by the canal in Venice - Looking aimless?
	PR_SCENE_M_CANAL_b,				// Park bench in hills
	PR_SCENE_M_CANAL_c,				// Bench on shopping street
	PR_SCENE_M2_LUNCH_a,			// Wife will complain and argue as he gets up to leave.
//	PR_SCENE_M7_LUNCH_b,			// Eating lunch at café with wife after reconciled
	PR_SCENE_M4_EXITRESTAURANT,		// Eating alone coming out of restaurant
	PR_SCENE_M4_LUNCH_b,			// Coming out of golf club, shop
	PR_SCENE_M4_CINEMA,				// Leaving cinema
	PR_SCENE_M2_WIFEEXITSCAR,		// Dropping off wife at Beverly Hills shops.
	PR_SCENE_M2_DROPOFFDAU_a,		// Dropping Tracey at shops
	PR_SCENE_M2_DROPOFFDAU_b,		// Dropping Tracey at club
	PR_SCENE_M2_DROPOFFSON_a,		// Dropping Jimmy off at high time
	PR_SCENE_M2_DROPOFFSON_b,		// Dropping Jimmy off at Pawn shop
	PR_SCENE_M_PIER_a,				// Smoking standing on pier looking out at sea
	PR_SCENE_M_PIER_b,				// On boardwalk near shrink
	PR_SCENE_M_TRAFFIC_a,			// Stuck in traffic jam in Los Peurta
	PR_SCENE_M_TRAFFIC_b,			// Traffic - various locations
	PR_SCENE_M_TRAFFIC_c,			// Traffic - various locations
	PR_SCENE_M_VWOODPARK_a,			// Sitting at bench in Vinewood park talking to guy	
	PR_SCENE_M_VWOODPARK_b,			// Sitting at bench in beach talking to guy
	PR_SCENE_M_BENCHCALL_a,			// Receiving phone call while sitting on park bench (about son)
	PR_SCENE_M_BENCHCALL_b,			// Receiving phone call while sitting on different bench
	PR_SCENE_M_PARKEDHILLS_a,		// Sitting on car up at Vinewood sign.
	PR_SCENE_M_PARKEDHILLS_b,		// In park, at observatory, viewpoint in hills
	PR_SCENE_M2_PHARMACY,			// Picking up pills (for his wife/him)
//	PR_SCENE_M4_DOORSTUMBLE,		// Drinking alone in the afternoon?
	PR_SCENE_M_COFFEE_a,			// Drinking coffee outside coffee shop in Burton
	PR_SCENE_M_COFFEE_b,			// Drinking coffee outside coffee shop in Eclipse
	PR_SCENE_M_COFFEE_c,			// Drinking coffee outside coffee shop in Rockford Hills
	PR_SCENE_M2_CYCLING_a,			// Riding bike in Puerto Del Sol
	PR_SCENE_M2_CYCLING_b,			// Riding bike in Vinewood Hills
	PR_SCENE_M2_CYCLING_c,			// Riding bike in Rockford Hills
//	PR_SCENE_M_BAR_a,				// Bar (drunk for 10sec.) 20%
//	PR_SCENE_M_BAR_b,				// can we use this location for a variation of the Michael leaving a bar switch scene?
	PR_SCENE_M2_MARINA,				// Marina 20%
	PR_SCENE_M2_ARGUEWITHWIFE,		// Fight with wife, she storms off 20%
	PR_SCENE_M4_PARKEDBEACH,		// Parked at the beach 20%
	PR_SCENE_M4_WASHFACE,			// have michael doing something at his house. no family as not reunited yet
	PR_SCENE_M_HOOKERMOTEL,			// leaving cheap motel with hooker getting out?
//	PR_SCENE_M_HOOKERCAR,			// cheaper still in car with hooker getting out?
	PR_SCENE_M6_MORNING_a,			// FE_T0_MICHAEL_depressed_head_in_hands
//	PR_SCENE_M6_MORNING_b,			// Sleeping on sofa in trailer
	PR_SCENE_M6_CARSLEEP,			// Asleep in car outside trailer
	PR_SCENE_M6_HOUSETV_a,			// Watching TV in Trevors trailer in night
//	PR_SCENE_M6_HOUSETV_b,			// Watching TV in trevors trailer during exile
	PR_SCENE_M6_SUNBATHING,			// FE_T0_MICHAEL_sunbathing
	PR_SCENE_M6_DRINKINGBEER,		// FE_T0_MICHAEL_drinking_beer
	PR_SCENE_M6_ONPHONE,			// FE_T0_MICHAEL_on_phone_to_therapist
	PR_SCENE_M6_DEPRESSED,			// FE_T0_MICHAEL_depressed_head_in_hands
	PR_SCENE_M6_BOATING,			// 
	PR_SCENE_M6_LIQUORSTORE,		//
//	PR_SCENE_M6_PILOTSCHOOL,		//
//	PR_SCENE_M6_TRIATHLON,			//
	PR_SCENE_M6_PARKEDHILLS_a,		// Countryside layby (during exile)
	PR_SCENE_M6_PARKEDHILLS_b,		// Countryside pier (during exile)
	PR_SCENE_M6_PARKEDHILLS_c,		// Countryside river (during exile)
	PR_SCENE_M6_PARKEDHILLS_d,		// Countryside fastfood layby (during exile)
	PR_SCENE_M6_PARKEDHILLS_e,		// Outside roadside fruit store (during exile)
	PR_SCENE_M2_DRIVING_a,			// near Michaels family
	PR_SCENE_M2_DRIVING_b,			// near Lester1B
	PR_SCENE_M6_DRIVING_a,			// 1: 2367.1, 3908.8, 35.2		(nighttime)
	PR_SCENE_M6_DRIVING_b,			// 2:-479.5, 5874.1, 33.0		(nighttime)
	PR_SCENE_M6_DRIVING_c,			// 3: -184.7, 6464.6, 30.2		(daytime)
	PR_SCENE_M6_DRIVING_d,			// 4: 1665.5, 4871.0, 41.6		(daytime)
	PR_SCENE_M6_DRIVING_e,			// 5: 2159.3, 3256.1, 46.9		(daytime)
	PR_SCENE_M6_DRIVING_f,			// 6: 2783.4, 3476.6, 54.8		(daytime)
	PR_SCENE_M6_DRIVING_g,			// 7: 2547.9, 2616.8, 37.5		(daytime)
	PR_SCENE_M6_DRIVING_h,			// 8: 1295.0, 1529.9, 96.6		(daytime)
	PR_SCENE_M6_RONBORING,			// Ron is sitting while Michael stands in front of him as they talk
	PR_SCENE_M7_RESTAURANT,			// Out to lunch with wife - Michael makes excuses (business) and leaves
	PR_SCENE_M7_LOUNGECHAIRS,		// At Beverly hills hotel pool with wife
	PR_SCENE_M7_BYESOLOMON_a,		// Leaving lunch with Solomon
	PR_SCENE_M7_BYESOLOMON_b,		// Leaving lunch with Solomon
	PR_SCENE_M7_WIFETENNIS,			// Finishing game of tennis with amanda
	PR_SCENE_M7_ROUNDTABLE,			// Michael and Amanda/Jimmy/Tracey about to have a blazing row when he takes a deep breath and calms down.
	PR_SCENE_M7_REJECTENTRY,		// Michael trying to get into fancy restaurants.  Being turned away. He drops some names, tells them they’ll know who he is soon.
	PR_SCENE_M7_HOOKERS,			// Michael telling hookers that he’s just window shopping
	PR_SCENE_M7_EXITBARBER,			// Leaving hair salon
	PR_SCENE_M7_EXITFANCYSHOP,		// Leaving high end clothes store
	PR_SCENE_M7_FAKEYOGA,			// Smoking outside the house, pretending he’s doing yoga.  “Of course I’m not smoking, I’m doing Sun Salutations! Why are you so paranoid.”  Stubbs it out
	PR_SCENE_M7_COFFEE,				// Grabbing coffee early morning - happy
	PR_SCENE_M7_GETSREADY,			// Waking up in bed with wife
//	PR_SCENE_M7_PARKEDHILLS,		// Overlooking city from car - but happy
	PR_SCENE_M7_READSCRIPT,			// On lounger by his pool - reading a script
	PR_SCENE_M7_EMPLOYEECONVO,		// Working at the movie lot
	PR_SCENE_M7_TALKTOGUARD,		// Leaving the movie set - Michael telling security guy at gate that he really should know who the fuck he is by now.
	PR_SCENE_M7_LOT_JIMMY,			// On the movie lot (letting Jimmy look around?)
	PR_SCENE_M7_KIDS_TV,			// Watching old movies with Tracey and jimmy
	PR_SCENE_M7_KIDS_GAMING,		// Michael and Jimmy gaming together.  Michael shouting obscenities at the screen.
	PR_SCENE_M7_OPENDOORFORAMA,		// Dropping wife at shops - opening car door for her - being nice
	PR_SCENE_M7_DROPPINGOFFJMY,		// Dropping Jimmy off somewhere- gives him cash. Club or mall
	PR_SCENE_M7_TRACEYEXITSCAR,		// Dropping Tracey off somewhere- her them cash. Club or mall
	PR_SCENE_M7_BIKINGJIMMY,		// arriving home after cycling - with jimmy?
	
	
//	/* * MICHAEL VARIATIONS * */
//PR_SCENE__M4_PARKEDBEACH_b,	//	Standing next to car smoking cigarette at beach	At edge of city boundary - overlooking los santos during exile		Need to confirm boundary line to avoid wanted level
//PR_SCENE__M6_DEPRESSED,		//	Standing outside Trevor's trailer	Standing by lake	1970.5, 3961.1, 32.0
	
	PR_SCENE_M_S_FAMILY4,			// Michael is at family, launch mission Family4 (bug 91894)
	
	/* franklin */
//	PR_SCENE_F0_SAVEHOUSE,
//	PR_SCENE_F1_SAVEHOUSE,
	PR_SCENE_F0_SH_ASLEEP,			// VERSION 1 - Franklin is passed out on his bed.  He wakes up with a start, looks at the clock, and decides to get up.
	PR_SCENE_F1_SH_ASLEEP,			//	Asleep in bed in house	New safehouse
	PR_SCENE_F1_NAPPING,			// on bed, putting on shoes
	PR_SCENE_F1_GETTINGREADY,		// putting on shirt
	PR_SCENE_F0_SH_READING,			// VERSION 2 - Franklin is awake in his bedroom.  He’s reading a business book. He puts it down and gets off his bed and leaves. 
	PR_SCENE_F1_SH_READING,			//	Sitting reading book in bed	New safehouse - on couch
	PR_SCENE_F0_SH_PUSHUP_a,		// VERSION 3 - Franklin has been doing push ups in his room.  He stands up, wipes his brow, and gets ready to go.  
	PR_SCENE_F0_SH_PUSHUP_b,		// Doing press ups in back garden at city safe houses
	PR_SCENE_F1_SH_PUSHUP,			// Doing press ups in back garden at hills safe houses
	PR_SCENE_F1_POOLSIDE_a,			// Variations with Franklin at his new safehouse later in game?
	PR_SCENE_F1_POOLSIDE_b,			// Variations with Franklin at his new safehouse later in game?
	PR_SCENE_F1_CLEANINGAPT,		// Being houseproud – cleaning, rearranging stuff.
	PR_SCENE_F1_ONCELL,				// On the phone – “you got to come around and see the place.  Okay, another time”
	PR_SCENE_F1_SNACKING,			// Eating alone
	PR_SCENE_F1_ONLAPTOP,			// On the computer wearing glasses, takes them off.
	PR_SCENE_F1_IRONING,			// Ironing
//	PR_SCENE_F0_WATCHINGTV,			// Watching TV (Old house variation)
	PR_SCENE_F1_WATCHINGTV,			// watching TV (New house variation)
	PR_SCENE_F_MD_KUSH_DOC,			// MAGDEMO Coming out of weed shop near beach
	PR_SCENE_F_KUSH_DOC_a,			// Coming out of weed shop near beach
	PR_SCENE_F_KUSH_DOC_b,			// Coming out of another weed shop
	PR_SCENE_F_KUSH_DOC_c,			// Coming out of another weed shop
	PR_SCENE_F0_TANISHAFIGHT,		// FRANKLIN is in his aunt’s house with TANISHA who is leaving.  
	PR_SCENE_F1_NEWHOUSE,		// Franklin walking around house on phone
	PR_SCENE_F0_GARBAGE,			// Taking out trash at aunts house	
	PR_SCENE_F1_GARBAGE,			// Taking out trash at new house
	PR_SCENE_F_THROW_CUP,			// VERSION 1 - "THROW_CUP"			(formally PR_SCENE_F_TACO_FARMER_A
	PR_SCENE_F_HIT_CUP_HAND,		// VERSION 1 - "HIT_CUP_HAND"		(formally PR_SCENE_F_HIT_CUP_HAND)
	PR_SCENE_F_GYM,
	PR_SCENE_F0_WALKCHOP,			// Walking Chop the Dog – arriving home? Old house variation
	PR_SCENE_F0_PLAYCHOP,			// playing with Chop the Dog in garden. (Old house variation)
//	PR_SCENE_F1_WALKCHOP,			// Walking Chop the Dog – arriving home? new house
	PR_SCENE_F1_PLAYCHOP,			// playing with Chop the Dog in garden. (New  house variation)
	PR_SCENE_F_WALKCHOP_a,			// Walking Chop the Dog – 
	PR_SCENE_F_WALKCHOP_b,			// Walking Chop the Dog – 
	PR_SCENE_F_TRAFFIC_a,			// Franklin stuck in traffic in Los Peurta
	PR_SCENE_F_TRAFFIC_b,			// Franklin stuck in traffic in Olympic Freeway
	PR_SCENE_F_TRAFFIC_c,			// Franklin stuck in traffic in Vinewood Blvd
	PR_SCENE_F0_BIKE,				// Franklin standing next to bike in driveway
	PR_SCENE_F0_CLEANCAR,			// Just cleaned his car (Old house variation)
	PR_SCENE_F1_BIKE,				// Franklin standing next to bike in new house
	PR_SCENE_F1_CLEANCAR,			// Just cleaned his car (New house variation)
	PR_SCENE_F1_BYETAXI,			// Putting a one night stand in a cab
	PR_SCENE_F_BIKE_c,				// Franklin standing next to bike overlooking city
	PR_SCENE_F_BIKE_d,				// Franklin standing next to bike by beach
//	PR_SCENE_F_TAUNT,				// Franklin is deciding whether to fight or not
	PR_SCENE_F_LAMTAUNT_P1,
//	PR_SCENE_F_LAMTAUNT_P2,
	PR_SCENE_F_LAMTAUNT_P3,
	PR_SCENE_F_LAMTAUNT_P5,
//	PR_SCENE_F_LAMGRAFF,
	PR_SCENE_F_CLUB,
	PR_SCENE_F_CS_CHECKSHOE,
	PR_SCENE_F_CS_WIPEHANDS,
	PR_SCENE_F_CS_WIPERIGHT,
	PR_SCENE_F_LAMTAUNT_NIGHT,
	PR_SCENE_F_BAR_a_01,			// Coming out of supermarket, rubbing hands
	PR_SCENE_F_BAR_b_01,			// Coming out of stripclub "hornbills", waves back as he leaves
	PR_SCENE_F_BAR_c_02,			// Coming out of stripclub "vanilla unicorn", waves back as he leaves
	PR_SCENE_F_BAR_d_02,			// Coming out of diner "Casey's", waves back as he leaves
	PR_SCENE_F_BAR_e_01,			// Coming out of corner shop "Long Pig", rubbing hands
	
	PR_SCENE_F_S_EXILE2,			// Franklin is in a car chase, launch mission Exile2 (bug 574569)
	PR_SCENE_F_S_AGENCY_2A_a,
	PR_SCENE_F_S_AGENCY_2A_b,
	
	/* trevor */
	#IF NOT IS_JAPANESE_BUILD
	PR_SCENE_T_SHIT,				// As camera zooms in on Trevor, we see him at his Country Safehouse. Trevor would be in default t-shirt and jeans. Time would be late morning. Trevor could be "Do the toilet one here on his own"
	#ENDIF
//	PR_SCENE_T_STRIPCLUB_a,			// Trevor is sat in the strip club. He is sat next to another man  . While they watch a girl dancing on stage Trevor is rubbing the man’s back.  After a moment, he gets up to go.	Location:	(110.1095, -1291.8535, 27.2609)
	PR_SCENE_T_SC_MOCKLAPDANCE,		// Trevor is sat in the strip club. Next to him a man is getting a lap dance  . Trevor watches the man in a creepy way until he gets bored and turns around	Location:	(116.4469, -1286.5035, 27.2647)
	PR_SCENE_T_SC_BAR,				// Trevor is sitting with a drink, staring at his reflection. He downs his drink and gets up	Location:	(126.7111, -1286.2056, 28.2742)
	PR_SCENE_T_SC_CHASE,			// Trevor is sitting with a drink, staring at his reflection. He downs his drink and gets up	Location:	(126.7111, -1286.2056, 28.2742)
	PR_SCENE_T_STRIPCLUB_out,		// 80%
	PR_SCENE_T_ESCORTED_OUT,		// Chucked out of Chateau. Another hotel or a swanky restaurant. Beverly Hills Hotel?
	PR_SCENE_T_CN_CHATEAU_b,		// Trevor getting kicked out of the random location
	PR_SCENE_T_CN_CHATEAU_c,		// Trevor getting kicked out of the motel swimming pool
	PR_SCENE_T_CR_CHATEAU_d,		// Trevor getting kicked out of the graveyard
	PR_SCENE_T_GARBAGE_FOOD,
	PR_SCENE_T_THROW_FOOD,
	PR_SCENE_T_SMOKEMETH,			// %
	PR_SCENE_T_FIGHTBBUILD,			// Trying to start a fight with a body builder 
	PR_SCENE_T_ANNOYSUNBATHERS,		// Insulting women at the beach
	PR_SCENE_T_CR_BLOCK_CAMERA,		// Outside theatre in a funny outfit
	PR_SCENE_T_GUITARBEATDOWN,		// Outside theatre starting a fight with busker
	PR_SCENE_T_DOCKS_a,				// 50% Hanging around - planning heist. Should have his car.(Do not use after Docks Heist)
	PR_SCENE_T_DOCKS_b,				// Smoking a cigarette - Michael house
	PR_SCENE_T_DOCKS_c,				// Smoking a cigarette - franklins house
	PR_SCENE_T_DOCKS_d,				// Smoking a cigarette - jail
	PR_SCENE_T_CR_LINGERIE,			// Being thrown out lingerie shop
	PR_SCENE_T_CR_FUNERAL,			// At a funeral home - crashing a funeral or being chucked out?
	PR_SCENE_T_FIGHTBAR_a,			// Just got kicked out of a bar for fighting, shouting back in
	PR_SCENE_T_FIGHTBAR_b,			// Just got kicked out of a city bar for fighting, shouting back in
	PR_SCENE_T_FIGHTBAR_c,			// Just got kicked out of a country bar for fighting, shouting back in
	PR_SCENE_T_YELLATDOORMAN,		// Trevor being kicked out of a yacht club for fighting, shouting back in
	PR_SCENE_T_FIGHTYAUCLUB_b,		// Trevor being kicked out of a vinewood hills hotel for fighting, shouting back in
	PR_SCENE_T_FIGHTCASINO,			// Trevor being kicked out of a casino for fighting, shouting back in
	PR_SCENE_T_KONEIGHBOUR,			// screaming at a neighbour
	PR_SCENE_T_SCARETRAMP,			// insulting/starting a fight with a tramp
	PR_SCENE_T_CR_DUMPSTER,			// Waking up in trash
	PR_SCENE_T_CN_WAKETRASH_b,		// Waking up on top of mountain hill
	PR_SCENE_T_CR_WAKEBEACH,		// Waking up on beach
	PR_SCENE_T_CN_WAKEBARN,			// Waking up in cow barn
	PR_SCENE_T_CN_WAKETRAIN,		// Waking up on a train line
	PR_SCENE_T_CR_WAKEROOFTOP,		// Waking up on a rooftop
	PR_SCENE_T_CN_WAKEMOUNTAIN,		// Waking up on a mountain top
	PR_SCENE_T_CR_ALLEYDRUNK,		// In an alley, drinking
	PR_SCENE_T_SC_ALLEYDRUNK,		// In the stripclub, drinking
	PR_SCENE_T_PUKEINTOFOUNT,		// Trevor throwing up in a park.
	PR_SCENE_T_CN_PARK_b,			// Trevor throwing up in water feature at winery
//	PR_SCENE_T_CN_PARK_c,			// Trevor throwing up in winery car park
	PR_SCENE_T_CR_POLICE_a,			// Trevor driving away from the police on the freeway???
	PR_SCENE_T_CN_POLICE_b,			// Trevor driving away from the police in countryside x 2
	PR_SCENE_T_CN_POLICE_c,			// Trevor driving away from the police in countryside x 2
	PR_SCENE_T_NAKED_BRIDGE,		// Trevor wakes up naked (soon) on a bridge
	PR_SCENE_T_NAKED_GARDEN,		// Trevor wakes up naked (soon) in someones garden - house in the hills
	PR_SCENE_T_NAKED_ISLAND,		// Trevor wakes up naked (soon) on an island with a boat quite nearby. have dead bodies around him - what happened here - He should be looking across as in screenshot attached. have bodies on beach at x and boad parked up for player to get away
	PR_SCENE_T_CR_CHASECAR_a,		// Chasing a car along river.
	PR_SCENE_T_CN_CHASECAR_b,		// Trevor chasing car in countryside
	PR_SCENE_T_CR_CHASEBIKE,		// Trevor running over 3 cyclist in the road - cycle rage
	PR_SCENE_T_CR_CHASESCOOTER,		// Trevor chasing a guy on a scooter and screaming 'Scooter Brother!'	http://youtu.be/GxKj_CmtHRI
	PR_SCENE_T_CR_BRIDGEDROP,		// Trevor drops someone from a bridge.
	PR_SCENE_T_CR_RUDEATCAFE,		// Insulting/ranting at vegetarians outside restaurant.
	PR_SCENE_T_CR_RAND_TEMPLE,		// tying someone up in Temple
	PR_SCENE_T_UNDERPIER,			// tying someone up under the pier.
	PR_SCENE_T_DRUNKHOWLING,		// In a hotel / apartment garden
	PR_SCENE_T_SC_DRUNKHOWLING,		// In the strip club
	PR_SCENE_T_FLOYDSAVEHOUSE,		// Trevor sitting on sofa in city savehouse
	PR_SCENE_T_FLOYDSPOON_A,		// Trevor spooning Floyd on the bed - "Hush now."
	PR_SCENE_T_FLOYDSPOON_B,		// Trevor spooning Floyd on the bed - "Hush now."
	PR_SCENE_T_FLOYDSPOON_B2,		// Trevor spooning Floyd on the bed - "You look so peaceful sleeping."
	PR_SCENE_T_FLOYDSPOON_A2,		// Trevor spooning Floyd on the bed - "You're spooning me next time."
//	PR_SCENE_T_FLOYDSPOON_E,		// removed for url:bugstar:1097027
	PR_SCENE_T_FLOYD_BEAR,			// Trevor harasses Floyd with various objects - Mr Raspberry Jam
	PR_SCENE_T_FLOYD_DOLL,			// Trevor harasses Floyd with various objects - ragdoll
	PR_SCENE_T_FLOYDPINEAPPLE,		// Trevor harasses Floyd with various objects - pineapple
	PR_SCENE_T_FLOYDCRYING_A,		// Trevor giving Floyd a 'pony ride' on his knee Console_Wasnt_Fun
	PR_SCENE_T_FLOYDCRYING_E0,		// Trevor giving Floyd a 'pony ride' on his knee Console_Enjoyed_That
	PR_SCENE_T_FLOYDCRYING_E1,		// Trevor giving Floyd a 'pony ride' on his knee Console_Lucky_Lady
	PR_SCENE_T_FLOYDCRYING_E2,		// Trevor giving Floyd a 'pony ride' on his knee Console_My_Turn
	PR_SCENE_T_FLOYDCRYING_E3,		// Trevor giving Floyd a 'pony ride' on his knee Console_Get_Along
	PR_SCENE_T6_SMOKECRYSTAL,		// FE_T0_TREVOR_smoking_crystal
//	PR_SCENE_T6_BLOWSHITUP,			// FE_T0_TREVOR_blowing_shit_up
	PR_SCENE_T6_METHLAB,			// 
	PR_SCENE_T6_HUNTING1,			// 
	PR_SCENE_T6_HUNTING2,			// 
	PR_SCENE_T6_HUNTING3,			// 
	PR_SCENE_T6_TRAF_AIR,			//
//	PR_SCENE_T6_DISPOSEBODY_A,		// Trevor is at the top of a bridge with a large suitcase. He throws it into the water and casually walks away.
	PR_SCENE_T6_DIGGING,			// Trevor is stood in the middle of the desert. He uses a shovel to pile on the last bit of dirt onto an obvious grave he just filled.
	PR_SCENE_T6_FLUSHESFOOT,		// Trevor is stood in his bathroom. He is trying to flush a severed foot down the toilet. The toilet gets stuck and water starts to flood the bathroom, Trevor walks away.
	PR_SCENE_T_CN_PIER,				// trevor threatening someone at end of a pier in country - holiding gun using ingame animation so does not need custom anim.
	PR_SCENE_T6_LAKE,				//	lake: (hey, can we call the underwater switch a new scene name - separate from the other three waking up variation scenes)
	PR_SCENE_T_FLYING_PLANE,		// trevor flying a plane - general over map
	PR_SCENE_T_HEADINSINK,			// Trevor has his head in a sink and quickly raises it out of the water and shakes it off. 
	#IF NOT IS_JAPANESE_BUILD
	PR_SCENE_T_JERKOFF,				// Trevor masturbating with his penis into the toilet and smears the left over debris onto the wall. 
	#ENDIF

//	/* * TREVOR VARIATIONS * */
//PR_SCENE__T_ANNOYSUNBATHERS_b,		//	Trevor shouting cheesy lines at woman on beach	Women shopping, walking in street	-1243, -742, 20				
//PR_SCENE__T6_AFTERNOON_b,			//	Smoking near the chair at Trevor's trailer	Smoking outside bar/diner	791, -736, 27				


	NUM_OF_PED_REQUEST_SCENES,
	PR_SCENE_INVALID
ENDENUM
PED_REQUEST_SCENE_ENUM g_eSelectedPlayerCharScene = PR_SCENE_INVALID



STRUCT PED_SCENE_STRUCT
	INT iStage								// INT to control the process of the scene
	INT iMissionID = NO_CANDIDATE_ID		// for Request_Mission_Launch()
	PED_REQUEST_SCENE_ENUM eScene			// The enum used to identify the scene
	enumCharacterList ePed					// The character enum for the ped that we are swapping to
	VECTOR vCreateCoords					// Where we create the ped that we hotswap to
	FLOAT fCreateHead						// The heading we give the ped that we hotswap to
	TEXT_LABEL_31 tCreateRoom				// The model name of the interior we wish to hotswap to, if one exists
	INTERIOR_INSTANCE_INDEX roomInterior	// Room interior so we can pin the interior into memory
	SELECTOR_PED_STRUCT sSelectorPeds		// The hotswap enum for the ped that we are swapping to
	SELECTOR_SLOTS_ENUM eSelectorPed		// Hotswap peds
	SELECTOR_CAM_STRUCT sSelectorCam		// Hotswap camera
	structTimelapse sTimelapse				// Track the progress of a spline cam transition.
	SCENARIO_BLOCKING_INDEX scenarioBlockID	// For adding a scenario blocking area around a switch camera location
ENDSTRUCT

PED_SCENE_STRUCT g_sOverrideScene[NUM_OF_PLAYABLE_PEDS]

STRUCT PED_SCENE_DATA_STRUCT
	VECTOR vCreateCoords
//	FLOAT fCreateHead
//	TEXT_LABEL_31 tRoom
ENDSTRUCT
PED_SCENE_DATA_STRUCT g_sPedSceneData[NUM_OF_PED_REQUEST_SCENES]


ENUM enumTimetableVehState
	PTVS_0_noVehicle = 0,
	PTVS_1_playerWithVehicle,
	PTVS_2_playerInVehicle
	
ENDENUM
STRUCT PLAYER_TIMETABLE_SCENE_STRUCT
	PED_SCENE_STRUCT		sScene
	
	SCRIPT_TASK_NAME		eLoopTask, eOutTask
	
	enumCharacterList		eSceneBuddy
	VECTOR					vSceneBuddyCoordOffset
	FLOAT					fSceneBuddyHeadOffset
	SCRIPT_TASK_NAME		eBuddyLoopTask, eBuddyOutTask
	
	enumTimetableVehState	eVehState
	BOOL					bDescentOnly
ENDSTRUCT

ENUM POST_FRANKLIN1_SWITCH_ENUM
	POST_FRANKLIN1_0_escapePoliceAlone,
	POST_FRANKLIN1_1_TrevorStripclub,
	POST_FRANKLIN1_2_Frank,
	
	POST_FRANKLIN1_NULL = -1
ENDENUM
POST_FRANKLIN1_SWITCH_ENUM g_ePostFranklin1Switch = POST_FRANKLIN1_NULL

// for mag demo - struct to pass over entities into FBI2
STRUCT MAG_DEMO_FBI2_ENTITIES_STRUCT
	BLIP_INDEX blip
	BOOL bBlipActive
	
	VEHICLE_INDEX heliVehicle
	VEHICLE_INDEX bikeVehicle
	
	VEHICLE_INDEX truck01Vehicle
	VEHICLE_INDEX truck02Vehicle
	VEHICLE_INDEX fibVehicle
	
	PED_INDEX trevorPed
	PED_INDEX franklinPed
	PED_INDEX fibPed
	BOOL bCreated
ENDSTRUCT
MAG_DEMO_FBI2_ENTITIES_STRUCT g_sMagDemoFBI2Entities

PED_REQUEST_SCENE_ENUM		g_eRecentlySelectedScene = PR_SCENE_INVALID

PED_VEH_DATA_STRUCT 		g_sPlayerLastVeh[NUM_OF_PLAYABLE_PEDS]			// Used to store the previous vehicle, potentially used for DEFAULT scene
enumTimetableVehState 		g_ePlayerLastVehState[NUM_OF_PLAYABLE_PEDS]		// Used to store the previous vehicle, potentially used for DEFAULT scene
VEHICLE_INDEX		 		g_vPlayerVeh[NUM_OF_PLAYABLE_PEDS]				// Used to store the previous vehicle, potentially used for DEFAULT scene
VECTOR				 		g_vPlayerLastVehCoord[NUM_OF_PLAYABLE_PEDS]		// Used to store the previous vehicle, potentially used for DEFAULT scene
FLOAT				 		g_fPlayerLastVehHead[NUM_OF_PLAYABLE_PEDS]		// Used to store the previous vehicle, potentially used for DEFAULT scene

PED_INDEX					g_pScene_buddy
PED_INDEX					g_pScene_extra_buddy							// for use in RELEASE_PED_TO_FAMILY_SCENE()

//FLOAT						g_fCamToPhase = -1

INT							g_iPauseOnOutro = 0
INT							g_iPausedOnOutroGametime = -1

CONST_INT g_iCOUNT_OF_LAST_SCENE_QUEUE		5

CONST_INT	g_iCONST_GAME_HOURS_FOR_POST_MISSION_SWITCH			1
CONST_INT	g_iCONST_GAME_HOURS_FOR_DEFAULT_SWITCH				3	//2	//1
CONST_INT	g_iCONST_GAME_HOURS_FOR_ASLEEP_SWITCH				6


// *******************************************************************************************
//	SAVED VARIABLES
// *******************************************************************************************
STRUCT g_PlayerSceneSavedData
    PED_REQUEST_SCENE_ENUM	g_ePlayerLastScene[NUM_OF_PLAYABLE_PEDS]	// Used to store the previous switch location of each of the peds
    PED_REQUEST_SCENE_ENUM	g_eLastSceneQueue[NUM_OF_PLAYABLE_PEDS][g_iCOUNT_OF_LAST_SCENE_QUEUE]	// Used to store the last 5 seen switch location
	INT g_iSeenOneOffSceneBit											// some scenes are one-offs and can only be seen once
    BOOL g_bPlayerTriggeredPrioritySwitch[NUM_OF_PLAYABLE_PEDS][4]		// Player has triggered a priority switch scene for a timezone
	BOOL g_bSeenTrevorsPrettyDress
ENDSTRUCT


//////////////////////////////////////////////////////////////////////////////////////////
///    Structs/Enums for debug purpouses
#IF IS_DEBUG_BUILD
USING "commands_misc.sch"
INT g_iSelectedDebugPlayerCharScene = GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(NUM_OF_PED_REQUEST_SCENES))
BOOL g_bDebugPrint_SceneScheduleInfo = FALSE
BOOL g_bDrawLiteralSceneString = FALSE
#ENDIF
