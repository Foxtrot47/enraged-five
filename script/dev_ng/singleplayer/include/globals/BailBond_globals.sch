
/// PURPOSE: list the bail bond mission IDs used by the launcher and mission scripts
ENUM BAILBOND_ID
	BBI_QUARRY	 = 0,
	BBI_FARM	 = 1,
	BBI_MOUNTAIN = 2,
	BBI_HOBO	 = 3,
	
	MAX_BAILBOND_IDS
ENDENUM

/// PURPOSE: set by the mission when it ends to inform the launcher how the mission ended
ENUM BAIL_BOND_MISSION_OVER_STATE
	BBMOS_CLEANUP,				// mission cleaned up without the player approaching the bail jumper to start the scenario
	BBMOS_PASSED_KILLED,		// player killed the bail jumper
	BBMOS_PASSED_DELIVERED,		// player handed the bail jumper over to Maude
	BBMOS_FAILED
ENDENUM
BAIL_BOND_MISSION_OVER_STATE eBailBondMissionOverState

/// PURPOSE: enum for flags which BailBondSaveData.iLauncherBitFlags stores
ENUM BAILBOND_LAUNCHER_BIT_FLAGS_ENUM
	// mission completed flags
	BBL_BIT_FLAG_COMPLETED_QUARRY,
	BBL_BIT_FLAG_COMPLETED_FARM,
	BBL_BIT_FLAG_COMPLETED_MOUNTAIN,
	BBL_BIT_FLAG_COMPLETED_HOBO,
	// mission passed flags
	BBL_BIT_FLAG_PASSED_TYPE_KILLED_QUARRY,
	BBL_BIT_FLAG_PASSED_TYPE_KILLED_FARM,
	BBL_BIT_FLAG_PASSED_TYPE_KILLED_MOUNTAIN,
	BBL_BIT_FLAG_PASSED_TYPE_KILLED_HOBO,
	// common launcher flags
	BBL_BIT_FLAG_DONE_INITIAL_LEAVE_AREA_CHECK,
	BBL_BIT_FLAG_SENT_MISSION_REMINDER_EMAIL,
	//debug only flag to make sure the mission can be launched
	BBL_BIT_FLAG_DEBUG_LAUNCHED
ENDENUM

/// PURPOSE: data passed to the bail bond mission script from the launcher
STRUCT BAIL_BOND_LAUNCH_DATA
	VECTOR vStartPoint						// this and...
	FLOAT fStartRange						// ... this must be set for all enums on launch	
	BAILBOND_ID eBailBondID 				// used to pick the correct save slot the data for the mission is stored in the cloud
	BOOL bTrevorKilledPreviousBailJumper	// player recieves a trigger and completion email - email threads are pre generated
	INT iMissionCandidateID = -1			// the bail bond mission's candidate ID
	BOOL bDoneTrevorReadsEmailDialogue		// used to trigger comment from Trevor when he has viewed the email from Maude		
ENDSTRUCT

/// PURPOSE: data passed to the post killed bail jumper ambient script from the bail bond mission
STRUCT BAIL_BOND_POST_KILLED_JUMPER_DATA
	PED_INDEX pedEnemies[4]
	VEHICLE_INDEX vehEnemies[3]
ENDSTRUCT

/// PURPOSE: the data we need to store for the bail bonds
STRUCT BailBondSaveData
	INT iLauncherBitFlags					// bit flags to store if a bail bond has completed, how it was passed
	INT iCurrentMission = 0					// store the current mission
	INT iLauncherState = -1					// store the state of the launcher
	TIMEOFDAY timeMissionStamp				// store time for mission checks e.g. delaying email, mission trigger delay, reminder message
	INT iFailsNoProgress[4]					// how many times each bailbond has been failed without progress (used in shitskips)
ENDSTRUCT
