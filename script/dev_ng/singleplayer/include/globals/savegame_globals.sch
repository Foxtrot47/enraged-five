//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	savegame_globals.sch										//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Struct used to store the autosave data.						//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

#IF IS_DEBUG_BUILD
	BOOL g_bDebug_KeepPlayerControllerBRunning = FALSE
	BOOL g_bDebug_KeepDrunkControllerRunning = FALSE
	BOOL g_bDebug_KeepAutoSaveControllerRunning = FALSE
	BOOL g_bDebug_KeepRespawnControllerRunning = FALSE
	BOOL g_bDebug_IsRespawnControllerRunning = FALSE
	BOOL g_bDebug_KeepFriendsControllerRunning = FALSE
	BOOL g_bDebug_KeepFriendsDebugControllerRunning = FALSE
	
	HOSPITAL_NAME_ENUM g_eDebug_ForceHospitalRespawn = NUMBER_OF_HOSPITAL_LOCATIONS
	POLICE_STATION_NAME_ENUM g_eDebug_ForcePoliceStationRespawn = NUMBER_OF_POLICE_STATION_LOCATIONS
#ENDIF

/// PURPOSE: The struct we use to store all the autosave request data
STRUCT AUTOSAVE_DATA_STRUCT
	BOOL bFlushAutosaves
	BOOL bRequest				// Has an autosave request been made
	BOOL bPerforming			// Has the DO_AUTO_SAVE command been called
	BOOL bInProgress			// Is the autosave in progress
	BOOL bComplete				// Is the autosave process complete
	BOOL bBeenOffMission		// Have we been off mission during the request
	BOOL bFirstAutosaveComplete	// Have we completed the first autosave
	BOOL bIgnoreOnMissionFlag 	// Should the save process ignore on mission flags
	BOOL bIgnoreScreenFade 		// Should we skip the screen fade check? Used at end of prologue and for triggering a switch when loading a savegame in a safehouse
	INT iTriggerTime			// The game time in which the autosave request triggered
	
	INT iQueuedRequests
	
ENDSTRUCT
AUTOSAVE_DATA_STRUCT g_sAutosaveData

// used to check if the game has been saved through the savebed
BOOL g_bSavebedCompleteSuccessfully = FALSE
