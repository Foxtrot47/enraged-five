// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	rampage_globals.sch
//		AUTHOR			:	Aaron Gandaa
//		DESCRIPTION		:	Globals required to control rampages.
//
// *****************************************************************************************
// *****************************************************************************************

//----------------------
//	CONST
//----------------------
CONST_INT   NUM_OF_RAMPAGES             	5  // Temp: Constant for number of rampages

//----------------------
//	ENUMS
//----------------------
ENUM RAMPAGE_MEDAL
	RAMPAGE_NOMEDAL,
	RAMPAGE_BRONZE,
	RAMPAGE_SILVER,
	RAMPAGE_GOLD
ENDENUM

//----------------------
//	STRUCTS
//----------------------
STRUCT RAMPAGE_STRUCT
	INT iMissionCandidateID
	TEXT_LABEL_31 sRampageScript
	INT iBlipIndex
ENDSTRUCT

STRUCT RAMPAGE_SCRIPT_ARGS
	STRING	sIntroCutscene			// Name of intro cutscene to preload.
	PED_INDEX 		pedID[3]		// Initial scene peds 
	VEHICLE_INDEX	vehID[2]		// Initial scene vehicles
	OBJECT_INDEX	objID[3]		// Initial objects - chairs and stuff
	INT	iPedsUsed =	0				// Number Of Peds In scene
	INT	iVehiclesUsed =	0			// Number Of Vehicles In scene	
	INT	iObjectsUsed =	0			// Number Of Vehicles In scene	
ENDSTRUCT

STRUCT RAMPAGE_PLAYER_DATA
	INT iMedalIndex	= 0				// 0 - no medal, 1 - bronze, 2 - silver, 3 - gold
	INT iHighScore = 0
ENDSTRUCT

STRUCT RampageDataSaved
	RAMPAGE_PLAYER_DATA playerData[NUM_OF_RAMPAGES]
ENDSTRUCT

//----------------------
//	STRUCTS
//----------------------
//RAMPAGE_STRUCT g_Rampage
//BOOL bRampagesCompleted[NUM_OF_RAMPAGES]
//INT iRampagesCompleted = 0
BOOL bDoRampageCityLockout = TRUE


// this is used to the launcher once the rampage script has ownership
//BOOL bTerminateRampLauncher = FALSE 
BOOL bIsRampLauncherRunning = FALSE

