// street_race_globals.sch
// Enums
ENUM STREET_RACE_INDEX
	STREET_RACE_NONE = -1,
	
	STREET_RACE_LOS_SANTOS = 0,
	STREET_RACE_CITY_CIRCUIT,
	STREET_RACE_AIRPORT,
	STREET_RACE_FREEWAY,
	STREET_RACE_VESPUCCI_CANALS,
	
	NUM_STREET_RACES
ENDENUM

// Constants
CONST_INT MIN_STREET_RACE_HOUR 20	 // Street races active from 8pm
CONST_INT MAX_STREET_RACE_HOUR 5	 // Finish at 5am
CONST_INT STREET_RACE_TIMEOUT  60000 // 1 minute timeout after interfering with initial scene

// Unsaved
BOOL g_bStreetRaceTimeout[NUM_STREET_RACES]
INT g_iStreetRaceCooldown[NUM_STREET_RACES]

// Saved
STRUCT StreetRaceSaved
	STREET_RACE_INDEX eRaceToUnlock	 // Stores which race is next to be unlocked via text message
	INT 		   	  iRaceUnlocked	 // Bitset checked to see whether a race is available
	INT 		      iRaceWon       // Bitset used to mark the blip as gold on completion
	INT				  iRaceLeaveArea // Bitset to check whether we need to leave the race area before it's available again
	BOOL			  bPhonecallDone // Bool to check if the phonecall for winning all races has been triggered
	INT				  iSlipstreamHelpCount // Stores how many times the slipstream help text has been displayed
	BOOL			  bSpecialAbilityHelp  // Stores if the special ability help text has been displayed
ENDSTRUCT
