USING "timer_globals.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	family_globals.sch
//		AUTHOR			:	Alwyn
//		DESCRIPTION		:	Globals required to control family activities.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************
CONST_FLOAT	fCONST_DISTANCE_TO_START_FAMILY_SCENE_SCRIPT	100.0

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
REL_GROUP_HASH				RELGROUPHASH_FAMILY_M		//Jimmy, Tracey, Amanda, Maid, Gardener
REL_GROUP_HASH 				RELGROUPHASH_FAMILY_F		//Denise, Lamar Stretch
REL_GROUP_HASH				RELGROUPHASH_FAMILY_T		//Ron, Patricia, Floyd

ENUM enumFamilyEvents

	/* MICHAEL'S HOUSE */
	FE_M_FAMILY_on_laptops = 0,							// All of family online on laptops at a table ignoring each other and you. Both kids wearing headphones.
	FE_M_FAMILY_MIC4_locked_in_room,					//
	FE_M7_FAMILY_finished_breakfast,					// Just finished eating a breakfast w Jimmy and Tracey, talking
	FE_M7_FAMILY_finished_pizza,						// Just finished eating a pizza w Jimmy and Tracey, talking
	FE_M7_FAMILY_watching_TV,							// watching TV together
	
	FE_M2_SON_gaming_loop,								// Son gaming (loop)
//	FE_M2_SON_gaming_exit,								// Son gaming (exit)
	FE_M_SON_rapping_in_the_shower,						// Son rapping in the shower
	FE_M_SON_Borrows_sisters_car,						// Son borrowing the car when you arrive
	FE_M_SON_watching_porn,								// Son watching porn
	FE_M_SON_in_room_asks_for_munchies,					// Son asking you to bring him munchies in his room
	FE_M_SON_phone_calls_in_room,						// Lot's of phone conversations behind locked, closed doors
	FE_M_SON_on_ecstasy_AND_friendly,					// Son on ecstasy – friendly
	FE_M_SON_Fighting_with_sister_A,					// Son fighting with sister. Arguing and then slamming doors upstairs. Door locked
	FE_M_SON_Fighting_with_sister_B,					// Son fighting with sister. Arguing and then slamming doors upstairs. Door locked
	FE_M_SON_Fighting_with_sister_C,					// Son fighting with sister. Arguing and then slamming doors upstairs. Door locked
	FE_M_SON_Fighting_with_sister_D,					// Son fighting with sister. Arguing and then slamming doors upstairs. Door locked
	FE_M_SON_smoking_weed_in_a_bong,					// Son smoking weed in a bong
	FE_M_SON_raids_fridge_for_food,						// Son with the munchies raiding the fridge
	FE_M_SON_sleeping,									// Son sleeping
	FE_M2_SON_watching_TV,								// Son watching TV after PRM2_KIDS_TV
	
	FE_M7_SON_jumping_jacks,							// Bad pushups (FAMR_IG_2)
	FE_M7_SON_gaming,									// Gaming (less aggressive) – could do with an exit anim of him calmly turning off the console?
//	FE_M7_SON_gaming_exit,								// Son gaming (exit)
	FE_M7_SON_going_for_a_bike_ride,					// Son going for a bike ride/coming back from ma bike ride
	FE_M7_SON_coming_back_from_a_bike_ride,				// Son going for a bike ride/coming back from ma bike ride
	FE_M7_SON_on_laptop_looking_for_jobs,				// Son on laptop looking for jobs
	FE_M7_SON_watching_TV_with_tracey,					// Son watching TV with sister. after PRM7_KIDS_TV
	
	FE_M2_DAUGHTER_sunbathing,							// Daughter sunbathing
	FE_M_DAUGHTER_Going_out_in_her_car,					// Daughter going out in her car 
	FE_M_DAUGHTER_Coming_home_drunk,					// Daughter coming home drunk
	FE_M_DAUGHTER_on_phone_to_friends,					// Daughter on phone to friends / boyfriend – door locked
	FE_M_DAUGHTER_on_phone_LOCKED,						// Daughter on phone to friends / boyfriend – door locked
	FE_M_DAUGHTER_shower,								// Daughter shower
	FE_M_DAUGHTER_workout_with_mp3,						// Daughter doing work out with MP3 player on
	FE_M_DAUGHTER_dancing_practice,						// Daughter dancing practice
	FE_M_DAUGHTER_watching_TV_sober,					// Daughter watching TV (sober)
	FE_M_DAUGHTER_watching_TV_drunk,					// Daughter watching TV (drunk)
	FE_M_DAUGHTER_crying_over_a_guy,					// Daughter crying over a guy
	FE_M_DAUGHTER_screaming_at_dad,						// Daughter screaming to dad to ‘get out'
	FE_M_DAUGHTER_purges_in_the_bathroom,				// Daughter purging in the bathroom
	FE_M_DAUGHTER_sniffs_drugs_in_toilet,				// Daughter sniffing drugs in toilet door locked
	FE_M_DAUGHTER_sex_sounds_from_room,					// Sex sounds from daughter's room – door locked
	FE_M_DAUGHTER_walks_to_room_music,					// Daughter listening to music in her room – door locked
	FE_M_DAUGHTER_sleeping,								// Daughter sleeping
	FE_M_DAUGHTER_couchsleep,							// Daughter asleep on couch (formally FE_M_MEXMAID_asleep)
	
	FE_M7_DAUGHTER_studying_on_phone,					// Daughter Trying to study, on her phone
	FE_M7_DAUGHTER_studying_does_nails,					// Daughter Trying to study, doing her nails
	FE_M7_DAUGHTER_sunbathing,							// Daughter sunbathing (less aggressive)
	
	

	FE_M_WIFE_screams_at_mexmaid,						// Wife screaming at Mexican maid 
	FE_M2_WIFE_in_face_mask,							// Wife in face mask
	FE_M_WIFE_playing_tennis,							// Wife playing Tennis until Family3
	FE_M2_WIFE_doing_yoga,								// Wife doing yoga
//	FE_M_WIFE_getting_nails_done,						// Wife getting nails done
	FE_M_WIFE_leaving_in_car,							// Wife leaving in car - synched scene
//	FE_M_WIFE_leaving_in_car_v2,						// Wife leaving in car - top of stairs
//	FE_M_WIFE_MD_leaving_in_car_v3,						// MAGDEMO Wife leaving in car - bottom of stairs
	FE_M2_WIFE_with_shopping_bags_enter,				// Wife with expensive shopping bags. Drops them
//	FE_M2_WIFE_with_shopping_bags_idle,					// Wife with expensive shopping bags. Drops them
//	FE_M2_WIFE_with_shopping_bags_exit,					// Wife with expensive shopping bags. Drops them
	FE_M_WIFE_gets_drink_in_kitchen,					// Wife gets drink in kitchen
	FE_M2_WIFE_sunbathing,								// Wife sunbathing
//	FE_M_WIFE_getting_botox_done,						// Wife getting botox done
	FE_M2_WIFE_passed_out_SOFA,  						// Wife passed out drunk / on meds (sofa)  
//	FE_M_WIFE_screaming_at_son_P1,						// Wife screaming at son "You Know What"
	FE_M_WIFE_screaming_at_son_P2,						// Wife screaming at son "Its Not Wasting Time"
	FE_M_WIFE_screaming_at_son_P3,						// Wife screaming at son "Its Not A Big Deal"
	FE_M_WIFE_screaming_at_daughter,					// Wife screaming at daughter
	FE_M2_WIFE_phones_man_OR_therapist,					// Wife on phone to man / therapist
 	FE_M_WIFE_hangs_up_and_wanders,						// 
 	
	#IF NOT IS_JAPANESE_BUILD
	FE_M2_WIFE_using_vibrator,							// Wife using rampant rabbit
 	FE_M_WIFE_using_vibrator_END,						//
	FE_M7_WIFE_using_vibrator,							// {Michael disturbs Amanda in bedroom  - post reunion}
	#ENDIF
	
	FE_M_WIFE_passed_out_BED,							// Wife passed out drunk / on meds (bed)
	FE_M2_WIFE_sleeping,								// Wife sleeping
	FE_M7_WIFE_sleeping,								// Wife sleeping
	
	FE_M7_WIFE_with_shopping_bags_enter,				// {Amanda comes home with shopping  - post reunion}
//	FE_M7_WIFE_with_shopping_bags_idle,					// {Amanda comes home with shopping  - post reunion}
//	FE_M7_WIFE_with_shopping_bags_exit,					// {Amanda comes home with shopping  - post reunion}
	FE_M7_WIFE_in_face_mask,							// {Amanda sits in her facemask - post reunion}
	FE_M7_WIFE_doing_yoga,								// {Amanda does yoga - post reunion}
	FE_M7_WIFE_sunbathing,								// {Amanda sunbathes  - post reunion}
	FE_M7_WIFE_passed_out_SOFA,  						// {Amanda sleeps on the sofa  - post reunion} 
	FE_M7_WIFE_phones_man_OR_therapist,					// {Amanda is on the phone  - post reunion}
 	FE_M7_WIFE_Making_juice,							// Wife Making juice
	FE_M7_WIFE_shopping_with_daughter,					// Wife Coming back from shopping w Tracey
//	FE_M7_WIFE_shopping_with_son,						// Wife Coming back from shopping w Jimmy
//	FE_M7_WIFE_on_phone,								// Wife on phone, hangs up, walks away
	
//	FE_M_MEXMAID_cooking_for_son,						// Mex Maid cooking for son
	FE_M2_MEXMAID_cleans_booze_pot_other,				// Mex Maid cleaning up booze / weed / shopping
	FE_M2_MEXMAID_clean_surface_a,						// Mex Maid cleaning up flat surface
 	FE_M2_MEXMAID_clean_surface_b,						// Mex Maid cleaning up flat surface
 	FE_M2_MEXMAID_clean_surface_c,						// Mex Maid cleaning up flat surface
 	FE_M2_MEXMAID_clean_window,							// MAGDEMO Mex Maid cleaning up window
// 	FE_M_MEXMAID_MIC4_clean_surface,					// Mex Maid cleaning up flat surface (post-Solomon5)
 	FE_M_MEXMAID_MIC4_clean_window,						// Mex Maid cleaning up window (post-Solomon5)
 	FE_M_MEXMAID_does_the_dishes,						// Mex Maid does the dishes
// 	FE_M_MEXMAID_watching_TV,	//cut (dress doesnt work)	// Mex Maid watching TV
	FE_M_MEXMAID_stealing_stuff,						// Mex Maid stealing stuff
	FE_M_MEXMAID_stealing_stuff_caught,					// Mex Maid stealing stuff
	
	FE_M7_MEXMAID_cleans_booze_pot_other,				// Mex Maid cleaning up booze / weed / shopping
	FE_M7_MEXMAID_clean_surface,						// Mex Maid cleaning up flat surface
 	FE_M7_MEXMAID_clean_window,							// MAGDEMO Mex Maid cleaning up window
 	
	FE_M_GARDENER_with_leaf_blower,						// Mex gardener with leaf blower
	FE_M_GARDENER_planting_flowers,						// WORLD_HUMAN_GARDENER_PLANT
//	FE_M_GARDENER_trimming_hedges,						// Mex Gardener trimming hedges
	FE_M_GARDENER_cleaning_pool,						// Mex Gardener cleaning pool
	FE_M_GARDENER_mowing_lawn,							// Mex Gardener mowing lawn
	FE_M_GARDENER_watering_flowers,						// Mex Gardener watering flowers
//	FE_M_GARDENER_spraying_for_weeds,					// Mex Gardener spraying for weeds
	FE_M_GARDENER_on_phone,								// Mex gardener on phone
	FE_M_GARDENER_smoking_weed,							// Mex gardener smoking weed
	
	
//	FE_M_MICHAEL_MIC2_washing_face,						// #1141457 after this mission can we have michael go into a timetable event in the house
	
	
	/* FRANKLIN'S HOUSE */
	FE_F_AUNT_pelvic_floor_exercises,					// Aunt doing pelvic floor exercises
	FE_F_AUNT_in_face_mask,								// Aunt in face mask
	FE_F_AUNT_watching_TV,								// Aunt watching TV
	FE_F_AUNT_listens_to_selfhelp_tapes_x,				// Aunt listening to self-improvement tapes
	
	FE_F_AUNT_returned_to_aunts,
	
//	FE_F_LAMAR_and_STRETCH_chill_outside,				// Lamar and Stretch chilling outside house
//	FE_F_LAMAR_and_STRETCH_bbq_outside,					// Lamar and Stretch having a bbq outside house
//	FE_F_LAMAR_and_STRETCH_arguing,						// Lamar and Stretch arguing
//	FE_F_LAMAR_and_STRETCH_shout_at_cops,				// Lamar and Stretch shouting at cops
	FE_F_LAMAR_and_STRETCH_wandering,					// Lamar and Stretch wandering (post-mission)
	
	/* TREVOR'S TRAILER - 1 */
	FE_T0_RON_monitoring_police_frequency,				// Ron monitoring police frequency
	FE_T0_RON_listens_to_radio_broadcast,				// Ron listening to Alex Jones style radio broadcast
	FE_T0_RON_ranting_about_government_LAYING,			// Ron ranting about the Federal government
	FE_T0_RON_ranting_about_government_SITTING,			// Ron ranting about the Federal government
	FE_T0_RON_smoking_crystal,							// Ron smoking crystal
	FE_T0_RON_drinks_moonshine_from_a_jar,				// Ron drinking moonshine from a jar
	FE_T0_RON_stares_through_binoculars,				// Ron staring at the sky through binoculars, looking for aliens
	
	/* TREVOR'S TRAILER - EXILE */
	FE_T0_MICHAEL_depressed_head_in_hands,				// Michael, head in hands, sunk in depression
	FE_T0_MICHAEL_sunbathing,							// Michael sunbathing
	FE_T0_MICHAEL_drinking_beer,						// Michael drinking beer
	FE_T0_MICHAEL_on_phone_to_therapist,				// Michael on phone to his therapist
	FE_T0_MICHAEL_hangs_up_and_wanders,					// Michael hangs up the phone and wanders around

	FE_T0_TREVOR_smoking_crystal,						// Trevor smoking crystal, offers it to Michael when he arrives
	#IF NOT IS_JAPANESE_BUILD
	FE_T0_TREVOR_doing_a_shit,							// Trevor doing a shit
	#ENDIF
	FE_T0_TREVOR_blowing_shit_up,						// Trevor blowing shit up
	FE_T0_TREVOR_passed_out_naked_drunk,				// Trevor passed out drunk and naked
	FE_T0_TREVOR_and_kidnapped_wife_walk,				// Trevor and kidnapped wife walking hand in and (if poss) and gazing into each others eyes
	FE_T0_TREVOR_and_kidnapped_wife_stare,				// Trevor and kidnapped wife stare at each other 
	//FE_T0_TREVOR_and_kidnapped_wife_laugh,				// Trevor and kidnapped wife laughing at some joke

	FE_T0_RONEX_outside_looking_lonely,					// Ron looking lonely outside trailer
	FE_T0_RONEX_trying_to_pick_up_signals,				// Ron trying to pick up signals from outer space
	FE_T0_RONEX_working_a_moonshine_sill,				// Ron working a moonshine sill
	FE_T0_RONEX_doing_target_practice,					// Ron doing target practice
//	FE_T0_RONEX_conspiracies_boring_Michael,			// Ron boring Michael with paranoid conspiracies

	FE_T0_KIDNAPPED_WIFE_cleaning,						// Kidnapped wife cleaning.
	FE_T0_KIDNAPPED_WIFE_does_garden_work,				// Kidnapped wife doing some garden work
	FE_T0_KIDNAPPED_WIFE_talks_to_Michael,				// Kidnapped wife talking to Michael about Trevor
//	FE_T0_KIDNAPPED_WIFE_cooking_a_meal,				// Kidnapped wife cooking a meal for them
    
	FE_T0_MOTHER_duringRandomChar,						// Trevors mother during RC MRS PHILLIPS.
//	FE_T0_MOTHER_something_b,							// Kidnapped wife cleaning.
//	FE_T0_MOTHER_something_c,							// Kidnapped wife cleaning.
	
	/* TREVOR'S SAFEHOUSE */
	FE_T1_FLOYD_cleaning,								// Floyd does some cleaning
	FE_T1_FLOYD_cries_in_foetal_position,				// Floyd cries in the foetal position
	FE_T1_FLOYD_cries_on_sofa,							// following PR_SCENE_T_FLOYDCRYING_A/E
	FE_T1_FLOYD_pineapple,								// following PR_SCENE_T_FLOYDPINEAPPLE
	FE_T1_FLOYD_on_phone_to_girlfriend,					// Floyd chats to Debra on the phone
	FE_T1_FLOYD_hangs_up_and_wanders,					// 
	FE_T1_FLOYD_hiding_from_Trevor_a,					// Floyd is hiding behind something
	FE_T1_FLOYD_hiding_from_Trevor_b,					// Floyd is hiding behind something
	FE_T1_FLOYD_hiding_from_Trevor_c,					// Floyd is hiding behind something
	FE_T1_FLOYD_is_sleeping,							// Floyd is sleeping
	
//	FE_T1_FLOYD_with_wade_post_trevor3,					// #414665 (removed for #788771)
	FE_T1_FLOYD_with_wade_post_docks1,					// #414673
	
	FE_ANY_find_family_event,							// #784658
	FE_ANY_wander_family_event,
	
	MAX_FAMILY_EVENTS,
	FAMILY_MEMBER_BUSY,
	NO_FAMILY_EVENTS
ENDENUM
enumFamilyEvents	g_eCurrentFamilyEvent[MAX_FAMILY_MEMBER]


enumFamilyEvents	g_eSceneBuddyEvents = NO_FAMILY_EVENTS


/*
Is that you firing rockets at my house?
Is that you creeping around my house?
Is that you who stole my car from in front of my house?
Is that you who set fire to my house?
Stop shooting up my house
*/

ENUM MONITER_PLAYER_GRIEFING_ENUM
	MPG_Firing_Rockets,
	MPG_Creep,
	MPG_Set_Fire,
	MPG_Shooting_House,
	MPG_Stole_Car_player,
	MPG_Grenade_House,			//keep at bottom for the debug draw rows
	MPG_Stole_Car_wife,			//keep at bottom for the debug draw rows
	MPG_Stole_Car_daughter,		//keep at bottom for the debug draw rows
	
	NUM_MONITER_PLAYER_GRIEFING
ENDENUM
MONITER_PLAYER_GRIEFING_ENUM	g_ePreviousFamilyGriefing		= NUM_MONITER_PLAYER_GRIEFING
INT								g_iFamilyGriefingTimer			= -1


ENUM ARMENIAN1_PRESTREAM_DENISE_ENUM
	ARM1_PD_0_null = 0,
	ARM1_PD_1_mission_requested_prestream,
	ARM1_PD_2_request,
	ARM1_PD_3_release
ENDENUM
ARMENIAN1_PRESTREAM_DENISE_ENUM	g_eArm1PrestreamDenise				= ARM1_PD_0_null
ARMENIAN1_PRESTREAM_DENISE_ENUM	g_eLam1PrestreamLamStretch			= ARM1_PD_0_null

STRUCT structFamilyGriefing
	INT iFiring_Rockets
	INT iCreep
	INT iStole_Car
	INT iSet_Fire
	INT iShooting_House
ENDSTRUCT

// *******************************************************************************************
//	STRUCTS
// *******************************************************************************************
STRUCT structFamilyScene
	VECTOR				sceneCoord
	FLOAT				sceneHead
ENDSTRUCT

// *******************************************************************************************
//	DEBUG VARIABLES
// *******************************************************************************************
#IF IS_DEBUG_BUILD
BOOL				g_bDrawDebugFamilyStuff				//draw debug lines, text and whatnot

BOOL				g_bUpdatedFamilyEvents				// scene needs updating after change in familyScreen_debug.sch
BOOL				g_bWarpPlayerOnFamilyDebug	= TRUE	// should changing the family event via F8 warp the player?
BOOL				g_bDebugForceCreateFamily	= FALSE	// if event is debugged through the menu, always create the ped as soon as poss
enumFamilyMember	g_eDebugSelectedMember		= INT_TO_ENUM(enumFamilyMember, 0)
#ENDIF


// *******************************************************************************************
//	SAVED VARIABLES
// *******************************************************************************************
STRUCT g_FamilySavedData
    enumFamilyEvents	ePreviousFamilyEvent[MAX_FAMILY_MEMBER]
	BOOL				bInitialisedPreviousEvents
	BOOL				bSeenFamWeaponDisplay
	BOOL				bHeardTrevorCountry
ENDSTRUCT

