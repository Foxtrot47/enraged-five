USING "buildtype.sch"

CONST_INT CLIFFORD_MG_EXIT_BIT_PokerExitTypeWin			0
CONST_INT CLIFFORD_MG_EXIT_BIT_PokerExitTypeBust		1
CONST_INT CLIFFORD_MG_EXIT_BIT_PokerExitTypeQuit		2

ENUM CLIFFORD_CASINO_CONFIG
	CLIFFORD_CASINO_CONFIG_NONE = 0,
	CLIFFORD_CASINO_CONFIG_ALL_SCENES,
	CLIFFORD_CASINO_CONFIG_NO_SCENES,
	
	CLIFFORD_CASINO_CONFIG_MAX
ENDENUM

ENUM CLIFFORD_CASINO_STATE
	CLIFFORD_CASINO_STATE_NONE = 0,
	CLIFFORD_CASINO_STATE_INIT,
	CLIFFORD_CASINO_STATE_IDLE
ENDENUM

STRUCT S_JP_DATA
	BOOL bJPControllerEnabled = TRUE
	INT iContextBtnIntent = -1
	INT iTakeOffTime = 3000
	INT iTakeoffTimer = 0
	FLOAT fTakeoffForce = 1000.0
	BOOL bShowJPHUD = FALSE
	BOOL bJPEnabledOnMission = FALSE
ENDSTRUCT
