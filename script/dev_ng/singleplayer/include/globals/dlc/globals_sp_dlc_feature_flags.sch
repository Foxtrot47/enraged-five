
//╒═════════════════════════════════════════════════════════════════════════════╕
//│			Precompiler flags for enabling/disabling SP featues from being		│
//│			compiled into the script project.									│
//╘═════════════════════════════════════════════════════════════════════════════╛

CONST_INT	FEATURE_SP_DLC_SELECTION			0		//Compile in the DLC selection routines in the startup script.
CONST_INT	FEATURE_SP_DLC_DIRECTOR_MODE		1
CONST_INT	FEATURE_SP_DLC_DM_PROP_EDITOR		1
CONST_INT 	FEATURE_SP_DLC_DM_WAYPOINT_WARP		1

#IF IS_DEBUG_BUILD
CONST_INT 	FEATURE_SP_DLC_BEAST_SECRET			1	//Must be 1 for the Beast Secret to be compiled 
													//anywhere into the game.
CONST_INT 	FEATURE_SP_DLC_BEAST_SECRET_DEBUG	0	//When 1 causes the Beast Secret to be compiled and run from test threads instead of main controllers.

CONST_INT	FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT	1	//Only compile new Stunt props in debug only. To prevent any bugs coming in, I'm not checking this in as 1 for just now.

CONST_INT	FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT_SPECIAL_RACES_ADDITIONS	1	
#ENDIF												



#IF NOT IS_DEBUG_BUILD
CONST_INT 	FEATURE_SP_DLC_BEAST_SECRET			1
CONST_INT 	FEATURE_SP_DLC_BEAST_SECRET_DEBUG	0
CONST_INT	FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT	1
CONST_INT	FEATURE_SP_DLC_DM_PROP_EDITOR_STUNT_SPECIAL_RACES_ADDITIONS	1	
#ENDIF
