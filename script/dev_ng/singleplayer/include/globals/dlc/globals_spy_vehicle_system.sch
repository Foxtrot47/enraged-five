USING "buildtype.sch"


//----------------------------------------------- ENUMS -----------------------------------------------------------

enum ambient_spy_vehicle_system_enum
	initialise_system = 0,
	switch_on_system,
	main_system_processing,
	player_dead_fail, 
	selector_ped_trevor_dead_fail,
	vehicle_destroyed_on_mission_cleanup,
	vehicle_destroyed_off_mission_cleanup,
	vehicle_set_for_soft_deletion,
	ambient_spy_vehicle_do_nothing
endenum

//---------------------------------------SPY VEHICLE SYSTEM VARIABLES ------------------------------------------

vehicle_index global_spy_vehicle
	

//**********variables not initialised within spy_vehicle_initialise_spy_vehicle_system_variables()**********

int spy_vehicle_camera_obscured_help_text_system_status = 0

bool spy_vehicle_switch_hud_and_app_available = false 
bool spy_vehicle_set_for_soft_deletion = false //only initialise variable once the spy vehicle is deleted via spy_vehicle_reset_global_ambient_spy_vehicle_system_launch_variables() 
bool spy_vehicle_handed_over_to_ambient_spy_vehicle_system = false
bool global_spy_vehicle_created = false


//----------------------------------AMBIENT SPY VEHICLE SYSTEM VARIABLES -----------------------------------------------

ambient_spy_vehicle_system_enum ambient_spy_vehicle_system_flow = initialise_system

bool ambient_spy_vehicle_system_launched = false
bool ambient_spy_vehicle_system_cleanup_triggered = false

//**********variables not initialised within ambient_spy_vehicle_initialise_system_variables()**********
bool ambient_spy_vehicle_allow_system_cleanup_checks_to_run = false
bool ambient_spy_vehicle_release_requested_assets = false

int ambient_spy_vehicle_activation_help_text_system_status = 0
int ambient_spy_vehicle_camera_help_text_system_status = 0
int ambient_spy_vehicle_weapon_help_text_system_status = 0
int ambient_spy_vehicle_switch_off_system_help_text_system_status = 0
	

	
//-----------------------------------------WIDGETS------------------------------------------------------------
	
#if is_debug_build
	bool widget_cleanup_and_terminate_spy_vehicle_system = false 
	bool widget_launch_spy_vehicle_system_sc = false 
	bool widget_allow_spy_vehicle_system_sc_to_run = false
#endif 




