
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   SP_globals_saved.sch
//      AUTHOR          :   Keith
//      DESCRIPTION     :   This script is essentially a giant struct containing other
//                          structs defined by other globals files. All variables contained
//                          within this struct will be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "flightschool_globals.sch"

// All structs containing variables that should be saved should be contained within this struct
STRUCT g_structSavedGlobalsPilotSchool
    
	//New singleplayer DLC data that needs to live outwith the SP savegame block.
	INT iDummySavedVar 

ENDSTRUCT

// *** Place no more variables after this ENDSTRUCT - they will not get saved

// This is the struct containing all saved variables.
// startup.sc sets this up as the PersistentGlobals struct.
g_structSavedGlobalsPilotSchool g_savedGlobalsPilotSchool
