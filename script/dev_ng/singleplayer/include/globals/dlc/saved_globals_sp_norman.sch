
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   SP_globals_sp_norman.sch
//      AUTHOR          :   Craig
//      DESCRIPTION     :   This script is essentially a giant struct containing other
//                          structs defined by other globals files. All variables contained
//                          within this struct will be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "respawn_globals.sch"
USING "player_ped_globals.sch"
USING "flow_globals.sch"
USING "charsheet_global_definitions.sch"
USING "comms_control_globals.sch"
USING "building_globals.sch"
USING "vehicle_gen_globals.sch"
USING "flow_help_globals.sch"
USING "CompletionPercentage_globals.sch"
USING "replay_globals.sch"
USING "email_globals.sch"
USING "snapshot_globals.sch"
USING "finance_globals.sch"
USING "ambient_globals.sch"	

// All structs containing variables that should be saved should be contained within this struct
STRUCT g_structSavedGlobalsnorman
		
	// All core mission flow saved data
    FLOW_SAVED_VARS 		sFlow
	
	// Custom flow saved data
	FlowCustomSaved			sFlowCustom
	
	// All saved data for player
    PlayerDataSaved 		sPlayerData
	
	// All saved data for respawn
    RespawnDataSaved 		sRespawnData
	
	// All the saved data for buildings
    BuildingDataSaved 		sBuildingData
    
    // All saved data for the communication controller
    CommsControlSaved 		sCommsControlData
	
	// All saved data for the script CodeID controller.
	CodeControlSaved 		sCodeControlData
	
    // All saved data for Completion Percentage tracking.
    g_CompletionPercentageSavedData sCompletionPercentageData

    // All saved data for Cellphone Setting save data.
    g_CellphoneSettingsSavedData sCellphoneSettingsData

    // All saved data for Cellphone Text Message save data.
    g_TextMessageSavedData 	sTextMessageSavedData

    // All saved data for Gallery Image saved data
    g_GalleryImageSavedData sGalleryImageSavedData
	
	 // Vehicle generation saved data
    VehicleGenDataSaved 	sVehicleGenData
    
	// All saved data for ambient missions
	g_ambientSavedData		sAmbient
	
    // Flow Help Queue saved data
    FlowHelpSaved 			sFlowHelp 
    
    // Finance System Stored Data
    FinanceDataSaved 		sFinanceData
	
	// Email system stored data
	EmailDataSaved 			sEmailData
	
	// Repeat Play Data (used in repeat play and save anywhere / quick save)
	REPEAT_PLAY_STRUCT 		sRepeatPlayData // Player pos and vehicle data 
	
	//sp buyable vehicle save data
	BuyableVehicleSaved  sBuyableVehicleSavedData
	
    // All saved data for character sheets
	// THIS MUST BE THE LAST STRUCT UNTIL 516508 IS FIXED
    g_CharSheetSavedData 	sCharSheetData
	
	// All Player Switch Scene saved data
    g_PlayerSceneSavedData 	sPlayerSceneData
	
ENDSTRUCT

// *** Place no more variables after this ENDSTRUCT - they will not get saved

// This is the struct containing all saved variables.
// startup.sc sets this up as the PersistentGlobals struct.
g_structSavedGlobalsnorman g_savedGlobalsnorman
