USING "buildtype.sch"
USING "global_block_defines.sch"
USING "mission_trigger_globals.sch"

GLOBALS GLOBALS_BLOCK_SP_DLC

	USING "globals_sp_dlc_feature_flags.sch"

	//DLC Mission flow config bit indexes.
	//NB. Keep in sync with the bit flags in x:\gta5\script\dev_ng\singleplayer\include\private\mission_flow\flow_mission_data_common.sch
	CONST_INT MF_EXTRA_INDEX_FORCE_TRIGGER_CORONA	0
	CONST_INT MF_EXTRA_INDEX_TEMP_ZERO_LEAVE_AREA	1
	CONST_INT MF_EXTRA_INDEX_PRESTREAM_ON			2

	//Moved to here so that DLC flow controllers don't have to reference the TU block - BenR
	SP_MISSIONS g_MissionLeftSPIn
	
	//New bools for the Dialogue Handler system to know when to force a DLC mission to request legacy dialogue rather than REQUEST_ADDITIONAL_TEXT_FOR_DLC
	BOOL g_Holder_ForceDLC_DialogueRequest = FALSE
	BOOL g_ForceDLC_DialogueRequest = FALSE
	
	//Moved here so that these globals get frozen when the singleplayer DLC packs are released. This is to stop the DLC packs
	//referencing globals in the TU blocks that will need to change after the packs are shipped.
	INT g_iCurrentLocateForDLC = 11
	INT g_iLastDLCItemNameHash
	INT g_iLastDLCItemLockHash
	BOOL g_bLastDLCItemNew
	
	INT g_iExtraMissionFlags[MAX_MISSION_DATA_SLOTS]
	
	// KEITH 25/3/14: Struct with one variable to register as a saved game for SP DLC
	// NOTE: This is to fix bug 1795872.
	//		The SP DLC block contained a saved game block for the SP Assassination pack that was released with Business1.
	//		This saved game block was then ripped out for the CTF_Creator pack so that Assassination globals wouldn't
	//		be released to the public - but they were already out there. This meant that an existing saved game with
	//		SP Assassination globals in it was getting loaded, but now there was no saved block getting registered to
	//		deal with them so the block was hanging around in memory (the same memory used by the network heap) causing the
	//		network game to bust it's memory boundaries. To fix this we've re-created an SP DLC saved block containing
	//		one variable. This will get registered allowing the loaded save variable block to match with this block
	//		(which is based on the script name, not the variables within it) and thereby allowing the saved block memory
	//		to get freed up afterwards.
	STRUCT structFakeSpDLC
		INT aFakeSavedInt = 0
	ENDSTRUCT

	structFakeSpDLC	g_fakeSpDLCSave

	//DLC Selection flags.
	BOOL g_bLoadedNorman 		= false	
	BOOL g_bLoadedClifford 		= false
	
	CONST_INT MAX_MISSION_TRIGGERS_TU 25
	
	STRUCT MISSION_TRIGGERER_LOCAL_VARS_TU
	BindTriggerScene fpBindTriggerScene
	TRIGGER_SCENE_POINTERS sScenePointers[MAX_MISSION_TRIGGERS_TU]
	
	BOOL bCallbackSetup = FALSE
	BOOL bReleaseFunctionBound = FALSE
	ReleaseTriggerSceneAssets fpReleaseAssetsForLoadedScene
	
	INT iSwitchTriggerCooldown = 0
	INT iNoFramesMissionRunning = 0
	INT iLockInTimer = 0
	BOOL bSkipNextSwitchCheck = FALSE
	BOOL bDisplayedTriggerRejectHelp = FALSE
	
	BLIP_INDEX blipTODTrigger
	structTimelapse sTimeOfDay
ENDSTRUCT

MISSION_TRIGGER_STRUCT g_TriggerableMissionsTU[MAX_MISSION_TRIGGERS_TU]

ENDGLOBALS
