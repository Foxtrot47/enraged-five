USING "buildtype.sch"
USING "global_block_defines.sch"
USING "globals_sp_clifford_definitions.sch"


GLOBALS GLOBALS_BLOCK_SP_CLF_DLC 

	USING "saved_globals_sp_clifford.sch"
	
	using "globals_spy_vehicle_system.sch"
		
	
	// disable casino minigame launcher
	BOOL g_bDisableCliffordMgLauncher = false
	
	// Casino configuration request + Casino state
	CLIFFORD_CASINO_CONFIG		g_eCliffordCasinoReset_write	= CLIFFORD_CASINO_CONFIG_None		// Write to this var to reset the casino (set it to the config you want to reset to)
	CLIFFORD_CASINO_STATE		g_eCliffordCasinoState_read		= CLIFFORD_CASINO_STATE_None		// Read this var to find the current casino state
	CLIFFORD_CASINO_CONFIG		g_eCliffordCasinoConfig_read	= CLIFFORD_CASINO_CONFIG_None		// Read this var to find the current casino config

	
	// exit state of casino minigame
	INT g_iCliffordMgExitBits = 0
	
	// Used in DLC_Hunter 
	BOOL g_bHasKilledAmbientGuard = false
	
	// last Miles mission completed
	INT g_iLastMilesMissionCompleted
	
	//--------------Assassination Hunting Global----------------
	BOOL g_bAssHuntPlayed = FALSE
	
	//--------------Keep giving the player a parachute every frame global
	
	BOOL g_bGiveFreeParachute = TRUE
	
	//--------------Jetpack Controller Data Global--------------
	S_JP_DATA g_sJPData
	
ENDGLOBALS
