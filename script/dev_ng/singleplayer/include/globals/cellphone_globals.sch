// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      SCRIPT NAME     :   cellphone_globals.sch
//      AUTHOR          :   Steven Taylor ( main user ).
//      DESCRIPTION     :   Enums and structs for use with the cellphone and accessible from
//                          other scripts. Also includes dialogue handling vars and consts.
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************



USING "types.sch"                           // Keith 26/9/12: To get PEDHEADSHOT_ID
USING "charsheet_global_definitions.sch"    //Needs to be here so we can reference the character list enum.
USING "commands_pad.sch"


//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Cellphone Phone Bit Set 1.
//
//________________________________________________________________________________________________________________________________________________________________________________



//Add more to this if need be... 0, 1, 2... to 31. Linked to BitSet_CellphoneDisplay

CONST_INT   g_BS_DISPLAY_NEW_CONTACT_SIGNIFIER              0
CONST_INT   g_BS_DISPLAY_NEW_TXTMSG_SIGNIFIER               1

CONST_INT   g_BS_DISPLAY_NEW_SIDETASK_SIGNIFIER             2
CONST_INT   g_BS_DISPLAY_NEW_EMAIL_SIGNIFIER                3

CONST_INT   g_BS_DISPLAY_NEW_APPOINTMENT_SIGNIFIER          4
CONST_INT   g_BS_DISPLAY_NEW_JOBLIST_SIGNIFIER              5

CONST_INT   g_BS_HAS_SIGNIFIER_MOVIE_LOADED                 6
CONST_INT   g_BS_CELLPHONE_TEXTFEED_NEEDS_UPDATED           7

CONST_INT   g_BS_LAUNCH_PHONE_TO_HOMESCREEN                 8

CONST_INT   g_BS_SENDING_CELLPHONE_CAM_PIC                  9

CONST_INT   g_BS_SENDING_TXTMSG_TO_ALL_PLAYERS              10

CONST_INT   g_BS_PHONE_UP_TO_EAR                            11

CONST_INT   g_BS_REMOVE_ALL_PENDING_TXTMSG_DATA             12

CONST_INT   g_BS_DISABLE_SIGNIFIERS_FOR_CUTSCENE            13

CONST_INT   g_BS_HAS_CELLPHONE_FULLY_MOVED_UP               14

CONST_INT   g_BS_DOCKS_SETUP_TUTORIAL_ONGOING               15

CONST_INT   g_BS_AUTO_SCROLL_CONTACT_ENABLED                16

CONST_INT   g_BS_OTHER_OPTION_IS_DISPLAYED                  17

CONST_INT   g_BS_CAMERA_READY_TO_TAKE_PHOTO                 18

CONST_INT   g_BS_MP_REQUESTS_RESET_OF_TXT_SIG               19

CONST_INT   g_BS_DISABLE_INCOMING_OR_OUTGOING_CALL_HANGUP   20

CONST_INT   g_BS_PICTURE_TAKE_BUTTON_PRESSED                21

CONST_INT   g_BS_FORCE_CELLPHONE_CAM_AUTO_CONTINUE          22

CONST_INT   g_BS_WAITING_ON_APP_TO_LOAD                     23

CONST_INT   g_BS_FORCE_CALL_WITH_REPLIES_NEGATIVE           24

CONST_INT   g_BS_DISPLAY_SLEEP_SIGNIFIER                    25

CONST_INT   g_BS_SNIPER_APP_SELECTED                        26

CONST_INT   g_BS_PROMPT_FLASHHAND_TO_PLAY_REMOTE_RING       27

CONST_INT   g_BS_DISPLAY_SEND_IN_CAMERA_HELP_TEXT           28

CONST_INT   g_BS_SENDPIC_CONTACT_SELECTED                   29

CONST_INT   g_BS_MOVE_PHONE_DOWN_INSTANTLY                  30

CONST_INT   g_BS_DO_TEXT_MESSAGE_PREVIEW                    31





INT BitSet_CellphoneDisplay = 0 //clear all bits.













//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Cellphone Phone Bit Set 2. Be careful when setting or clearing these! All BSCs should use Continued.
//
//________________________________________________________________________________________________________________________________________________________________________________





CONST_INT g_BSC_LAUNCH_FINALE_CONTACTS                          0
CONST_INT g_BSC_UPDATE_NEG_BUTTON_PRESENCE                      1
CONST_INT g_BSC_REQUESTED_DISABLE_CELLPHONE_THIS_FRAME          2
CONST_INT g_BSC_SHOULD_DISABLE_CAMERA_APP_THIS_FRAME            3
CONST_INT g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME         4
CONST_INT g_BSC_FINISHED_CALL_RETURNS_TO_HOMESCREEN             5

CONST_INT g_BSC_SHOULD_DISABLE_INTERNET_APP_THIS_FRAME          6
CONST_INT g_BSC_REQUESTED_DISABLE_INTERNET_APP_THIS_FRAME       7
CONST_INT g_BSC_CELLPHONE_THREEQUARTERS_UP                      8     

CONST_INT g_BSC_UNIVERSAL_REPLIES_FOR_N_LINES_HAS_BEEN_SET      9 
CONST_INT g_BSC_LAUNCH_APPINTERNET_ON_RETURN_TO_PDS_MAXIMUM     10
CONST_INT g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED              11

CONST_INT g_BSC_AUTO_BACK_TEXTS_SINGLEMESSAGE_VIEW              12

CONST_INT g_BSC_LOW_QUALITY_CREATE_FAILED                       14
CONST_INT g_BSC_LOAD_FROM_MEDIA_FAILED                          15


CONST_INT g_BSC_LAST_CALL_ABORTED_DUE_TO_SCRIPT_HANGUP          16


CONST_INT g_BSC_PREVENT_CELLPHONE_LINESKIP                      17

CONST_INT g_BSC_TERMINATE_APPCONTACTS_FOR_CHAT_CALL             18

CONST_INT g_BSC_PREVENT_CALL_FOR_CHAR_DETONATEPHONE             19

CONST_INT g_BSC_SET_CHAT_CALL_AS_ENGAGED                        20

CONST_INT g_BSC_PLAY_INTERFERENCE_AUDIO                         21

CONST_INT g_BSC_SKIP_TRACKIFY_LOADING_SCREEN                    22

CONST_INT g_BSC_CELLPHONE_WITH_REPLIES_WAITING_ON_USER_INPUT    23


CONST_INT g_BSC_CALL_IN_PROGRESS_VANISH_PHONE                   24
CONST_INT g_BSC_CALL_IN_PROGRESS_REINSTATE_PHONE                25

CONST_INT g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED           26
CONST_INT g_BSC_CALL_IN_PROGRESS_PHONE_ALREADY_SIGNALLED_TO_VANISH_BY_DH  27

CONST_INT g_BSC_BYPASS_CAMERA_APP_SAVE_ROUTINE                  28

CONST_INT g_BSC_HIDE_TRACKIFY_TARGET_ARROW                      29
CONST_INT g_BSC_REMOVE_TRACKIFY_TARGET                          30

CONST_INT g_BSC_THIS_IS_AN_ANSWERPHONE_CALL                     31




INT BitSet_CellphoneDisplay_Continued = 0 //clear all bits.



//______________________________________________________________________________________________________________________________________________________________________________________
//                                                                                                                                                                              
//                                                                      Cellphone - Third and final bitset. Be careful when setting or clearing these! Make sure to designate as third
//
//______________________________________________________________________________________________________________________________________________________________________________________




CONST_INT g_BSTHIRD_KEEP_MP_CALL_ACTIVE_ON_PAUSE_MENU           0 
CONST_INT g_BSTHIRD_DISPLAY_TRACKIFY_RELATIVE_HEIGHT            1

CONST_INT g_BSTHIRD_DO_NOT_MOVE_PHONE_UP_ON_LAUNCH              2
CONST_INT g_BSTHIRD_IS_CAMERA_IN_SELFIE_MODE                    3

CONST_INT g_BSTHIRD_REPOSITION_SUBS_FOR_LANDSCAPE_PHONE         4


CONST_INT g_BSTHIRD_PREVENT_PRELOADED_PHONECALL_START           5


CONST_INT g_BSTHIRD_HUMAN_CONTACT_HIGHLIGHT_AVAILABLE           6


CONST_INT g_BSTHIRD_HUMAN_HIDE_FOR_HANDS_FREE_HELP_DONE         7

CONST_INT g_BSTHIRD_HUMAN_TAKEOUT_FOR_HANDS_FREE_HELP_DONE      8

//6417795 - April 2020
//Adding new bit to specify when the MP Cellphone homescreen has been switched and is operating in secondary mode.
CONST_INT g_BSTHIRD_MP_CELLPHONE_IN_SECONDARY_MODE			    9


//CONST_INT g_CELLPHONE_MOVIE_STREAMING_IN                      10 //Comment in for resolution of 1135663  




INT BitSet_CellphoneDisplay_Third = 0















//Used to throttle the cellphone's clock display to prevent scaleform method repetition every frame. 
//Needs to be a global as this is used by appContacts and cellphone flashhand.sc
INT ClockThrottleMins 




//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Cellphone Quicksave
//
//________________________________________________________________________________________________________________________________________________________________________________



BOOL g_HasCellphoneRequestedSave
BOOL g_QuickSaveDisabledByScript

INT g_HomescreenIcon_AlphaValue = 255

CONST_INT i_HomescreenIcon_FullAlpha  255
CONST_INT i_HomescreenIcon_GreyedOut  42







//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Phone Application Enums
//
//________________________________________________________________________________________________________________________________________________________________________________


//Cellphone Application List
ENUM enumApplicationList

    AppCONTACTS,
    AppTEXTS,
    AppINTERNET,
    AppCAMERA,
    AppCHECKLIST,
    AppSIDETASK,
    AppREPEATPLAY,
    AppEMAIL,
    AppORGANISER,
    AppBAWSAQ,
    AppSETTINGS,
    AppPAGE2APPS,
    AppDEDICATEDSLEEP,
    AppMPCOPBACKUP,     // KGM 6/9/11: 'Cop Backup' app for cops only
    AppMPJOBLIST,       // KGM 5/9/11: Replaced with new app
    AppPAUSEMAP,
    AppSNIPER,
    AppTRACKIFY,
    AppZIT,
    AppMEDIA,
    AppQUICKSAVE,
    AppJIPMP,
    AppBroadcast,
   // AppMPPLAYERLIST,

    //Necessary dummies to fill out empty slots. Add more for every tributary homescreen that is added.
    AppDummyApp0,
    AppDummyApp1,
    AppDummyApp2,
    AppDummyApp3,
    AppDummyApp4,
    AppDummyApp5,
    AppDummyApp6,
    AppDummyApp7,
    AppDummyApp8,
    AppDummyApp9,
    AppDummyApp10,

    //Don't remove
    MAX_APPLICATIONS,
    MAX_APPLICATIONS_PLUS_DUMMY

ENDENUM



ENUM enumAppAvailabilityOWNER1

    App_O1_LOCKED,
    App_O1_UNLOCKED

ENDENUM



ENUM enumAppAvailabilityOWNER2

    App_O2_LOCKED,
    App_O2_UNLOCKED

ENDENUM



ENUM enumAppAvailabilityOWNER3

    App_O3_LOCKED,
    App_O3_UNLOCKED

ENDENUM




                         





//Will need to be a saved global so we can keep tack of each apps availability for each owner.
STRUCT structAppList
    

    TEXT_LABEL AppName              //the name of the app for displaying on the phone
    INT ThisAppPosition         //the position this app should occupy in the flash master position list.
    TEXT_LABEL AppScriptName    //the name of the script to be launched when this app is selected.
    INT AppScriptNameHash       //stores the hash key for this selected app.

    INT SF_IconEnum     //specifies which icon should be displayed with this app on the home menu.
    INT Homescreen_Page_Precedence    //defines which homescreen this app should appear on. 1 is the first, 2 would mean it would appear in the first screen of "more apps",
                                //3 would be "even more apps" if need be. This is necessary so I can make sure that I don't try to populate the 3 x 3 grid or whatever 
                                //with two or more apps in the same slot ( they can share the same slot number ) as I work through the slot placement routine.
                                //This has the added advantage of "0" not appearing in the list.

    //Icon name here?
   
    enumAppAvailabilityOWNER1  O1_Availability
    enumAppAvailabilityOWNER2  O2_Availability
    enumAppAvailabilityOWNER3  O3_Availability

    //Perhaps have the ringtones and wallpaper stored here for each OWNER also?


ENDSTRUCT




//The main list of cellphone apps
//Will need to be saved globals...
structAppList g_AppList[MAX_APPLICATIONS_PLUS_DUMMY]


//Temp: This would be the flash array of empty positions.
//Need to fill application list.
INT AppSlot[MAX_APPLICATIONS_PLUS_DUMMY]
BOOL Has_this_AppSlot_Been_Assigned[MAX_APPLICATIONS_PLUS_DUMMY]

INT g_LastUsed_Homescreen_Page_Precedence //Store the last homescreen precedence stipulated so that we can return to it from an application exit. 


BOOL g_TerminateApp //bool flag used to let Check_Application_Exit() public proc know that it should terminate.

INT g_App_SafetyBuffer_StartTime
INT g_App_Elapsed_SafetyBuffer = 0 //stores how long the safety buffer has been in operation for comparison with the required duration. Should be reset to zero at app launch.
INT g_App_Required_SafetyBuffer_Duration = 300 //determines the length of the safety buffer. During the safety buffer timezone, apps won't check for the exit button - see bug 25815











//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Phone Signifiers
//
//________________________________________________________________________________________________________________________________________________________________________________









ENUM eSignifierType

    SIG_EMPTY,
    SIG_CONTACT1,
    SIG_CONTACT2, //will be used for adding more than contact at once.
    SIG_TEXT,
    SIG_EMAIL,
    SIG_JOB,
    SIG_APPOINT,
    SIG_SIDETASK,

    
    MAX_SIGNIFIERS

ENDENUM



ENUM eSignifierStatus

    SIG_OFFSCREEN,
    SIG_ONSCREEN,
    SIG_MULTIPLE
    
ENDENUM



STRUCT structSignifierSlot


    //INT                 SignifierIcon
    TEXT_LABEL          SignifierPrimaryText

    //enumCharacterList   SignifierCharacter
    //TEXT_LABEL          SignifierSecondaryText
    eSignifierStatus    SignifierStatus
    //INT                 SignifierDuration                                
    enumPhoneBookPresence SignifierPhoneBook

ENDSTRUCT


structSignifierSlot SignifierData[MAX_SIGNIFIERS]



TEXT_LABEL BufferedContactSignifierText[4]  //Used to hold a copy of any new contact label that should be buffered for a player character.
BOOL BufferedContactAvailable[4]            //Set when necessary by cellphone_public's add_contact routine and checked by cellphone controller on a player swap.
enumCharacterlist BufferedHeadshotRef[4]

enumCharacterList g_Temp_ContactHeadshotRef





enumCharacterList g_Temp_MP_PassedCharacterListRef                                             
                   
TEXT_LABEL_63 g_Temp_MP_SenderString






INT g_Current_Number_Of_Signifiers = 0  //We use this to keep track of when we need to request or remove the scaleform signifier movie loaded in cellphone_controller.sc
                                        //If it's greater than zero we request and load the movie, and once it returns to zero, we remove the movie.


BOOL g_UseContactSignifierNewType = FALSE


ENUM eLastTextSentTo

    WAS_SENT_TO_MICHAEL,
    WAS_SENT_TO_FRANKLIN,
    WAS_SENT_TO_TREVOR,
    WAS_SENT_TO_MULTIPLAYER,
    WAS_SENT_TO_EVERY_SP_CHARACTER

ENDENUM



eLastTextSentTo g_LastTextSentTo


//When a text message is sent, the global g_TextMessageIconStyle is used within CREATE_ALERT to display the desired signifier...
ENUM eTextMessageIconStyle

    ICON_STANDARD_TEXTMSG,
    ICON_REPLAY_TEXTMSG,
    ICON_SHITSKIP_TEXTMSG

ENDENUM



//

eTextMessageIconStyle g_TextMessageIconStyle



ENUM enumRecipientList

    SILENT_MICHAEL,
    SILENT_FRANKLIN,
    SILENT_TREVOR,
    NO_SILENT_RECIPIENT

ENDENUM





//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Phone Numpad
//
//________________________________________________________________________________________________________________________________________________________________________________



ENUM enumNumpadList

    NUM_0,
    NUM_1,
    NUM_2,
    NUM_3,
    NUM_4,
    NUM_5,
    NUM_6,
    NUM_7,
    NUM_8,
    NUM_9,
    NUM_10_ASTERISK,
    NUM_11_HASH,


    //Don't remove
    MAX_NUMPAD_ENTRIES,
    MAX_NUMPAD_ENTRIES_PLUS_DUMMY

ENDENUM







STRUCT structNumpadList
    

    TEXT_LABEL  NumpadLabel     //reference to a text label containing the number or special character to be displayed. We might not need this when literals can be displayed.
    INT ThisNumpadPosition      //the position this num should occupy in the flash master position list.


ENDSTRUCT



structNumpadList NumpadList[MAX_NUMPAD_ENTRIES_PLUS_DUMMY]
  

TEXT_LABEL g_Redial_NumpadEntryString[4] //used to store any entered number for redial purposes. Now needs one for each character. See bug

INT NumpadSlot[MAX_NUMPAD_ENTRIES]














//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Phone Settings Sheet
//
//________________________________________________________________________________________________________________________________________________________________________________




ENUM enumSettingList

    //
    SETTING_PROFILE,   //0
    SETTING_RINGTONE,  //1
    SETTING_THEME,     //2
    SETTING_VIBRATE,
    SETTING_WALLPAPER,
 


    SETTING_MISC_A, //Now used for Invite Sound in MP.

    SETTING_MISC_B, //Extra settings for any DLC use. Free.
    SETTING_MISC_C, //Free
    SETTING_MISC_D, //Free
    

    MAX_SETTINGS,
    MAX_SETTINGS_PLUS_DUMMY,
    NO_SETTING

ENDENUM





ENUM enumPhoneGuiPresence 

    UNAVAILABLE_IN_GUI,

    AVAILABLE_IN_GUI 

ENDENUM



// Setting List structure
CONST_INT Max_Settings_Secondary_Options 19


STRUCT structSettingList
    
    INT                 Setting_orderint            //int used for alphabetical order. This can be altered on the fly, so that a priority setting can be stipulated and sent to the top of the settings list.
    INT                 Setting_original_orderint   //int used to keep a copy of the original alpha int for all settings so they can be reordered after a priority setting routine.
    TEXT_LABEL          Setting_Primary_label       //text label of setting name as it would be displayed on the phone's setting list  
    INT                 Setting_Icon_Int            //icon_int
    TEXT_LABEL          Setting_Secondary_Option_labels[Max_Settings_Secondary_Options]
    INT                 Setting_Secondary_Order[Max_Settings_Secondary_Options]
    INT                 Setting_Secondary_Icon_Int[Max_Settings_Secondary_Options]
    BOOL                Setting_Secondary_Option_available[Max_Settings_Secondary_Options]
    TEXT_LABEL_23       Setting_Secondary_Option_filename_label[Max_Settings_Secondary_Options] //needs to be a pointer to a label so it can be easily changed by someone else.
    INT                 Setting_Currently_Selected_Option //Stores which secondary option has been stored for this particular setting, defaults to zero.
    //INT                 Setting_Linked_Wallpaper[Max_Settings_Secondary_Options]  //Removed to save globals 15.03.13  
    INT                 Setting_Linked_SF_Reference[Max_Settings_Secondary_Options]

    enumPhoneGuiPresence phoneGuiPresence //allows us to specify if this particular setting should be available in the settings menu.
    
    
ENDSTRUCT








//Needs to be saved globals...

STRUCT structThis_Cellphone_Owner_Settings

    //Moved this into a separate structure to prevent savegame size mismatch.
    //structSettingList   g_SettingList[MAX_SETTINGS_PLUS_DUMMY] 

    TEXT_LABEL_23       ScaleformOS_Movie_Name    //the filename of the cellphone scaleform movie that this owner should load.

    INT                 ThemeForThisPlayer                                                              
    
    INT                 OSTypeForThisPlayer 

    INT                 ProviderForThisPlayer

    INT                 WallpaperForThisPlayer

    //This is unlikely to be a saved global...
    //INT                 ProfileForThisPlayer 
    
    INT                 VibrateForThisPlayer

    TEXT_LABEL_23       RingtoneForThisPlayer

    BOOL                g_LastMessageSentMustBeRead

    BOOL                g_LaunchToTextMessageScreen

    BOOL                Is_This_Player_On_Scheduled_Activity


    

ENDSTRUCT 


BOOL g_IsMonochromeCheatActive = FALSE



CONST_INT Max_Setting_Profiles 4



STRUCT g_CellphoneSettingsSavedData
    structThis_Cellphone_Owner_Settings This_Cellphone_Owner_Settings[Max_Setting_Profiles]

    BOOL b_HasSettingsHelpBeenDisplayed //Now used for Focus Lock help.
    BOOL b_HasSleepWarningBeenDisplayed
    BOOL b_HasSleepIconHelpBeenDisplayed
    BOOL b_HasSleepReminderBeenDisplayed
    BOOL b_HasQuickSaveHelpBeenDisplayed
    BOOL b_HasTranslucentIconHelpBeenDisplayed
    BOOL b_MP_HasTranslucentIconHelpBeenDisplayed



    BOOL b_IsSniperAppAvailable
    BOOL b_IsTrackifyAppAvailable

ENDSTRUCT





STRUCT structThis_Cellphone_Owner_Settings_ListContents

    structSettingList g_SettingList[MAX_SETTINGS_PLUS_DUMMY] 

ENDSTRUCT


structThis_Cellphone_Owner_Settings_ListContents This_Cellphone_Owner_Settings_ListContents[Max_Setting_Profiles]







//Profile Enum for improved clarity of functionality when used in other scripts.


ENUM PhoneProfileEnum


    PROFILE_NORMAL_MODE,    //0
    PROFILE_QUIET_MODE,     //1
    PROFILE_SLEEP_MODE      //2


ENDENUM














 










//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Physical Phone Display, Navigation and Status
//
//________________________________________________________________________________________________________________________________________________________________________________


//Primary Scaleform drawing screen position and size.

//Notes:
//Prescribed phone width - height ratio from Gareth 0.316 x 0.755

FLOAT g_SF_PhonePosX = 0.1
FLOAT g_SF_PhonePosY = 0.179

FLOAT g_SF_PhoneSizeX = 0.2
FLOAT g_SF_PhoneSizeY = 0.356


FLOAT g_SF_ButtonsPosX = 0.1
FLOAT g_SF_ButtonsPosY = 0.325//Proper 0.325 - 0.350 is a placeholder until we get transparency for the button bar.

FLOAT g_SF_ButtonsSizeX = 0.19//0.2
FLOAT g_SF_ButtonsSizeY = 0.060




VECTOR g_3dPhonePosVec = <<-30.0, 3.0, -90.0>>
//Pocket work.
VECTOR g_3dPhoneRotVec = <<0.0, 0.0, 0.0>>
VECTOR g_3dPhoneDebugRotVec = <<-89.8, -14.7, 0.0>>//Second angle <<-93.3, -23.4, 0.0>> // Old nice jaunty angle for testing <<-90.0, 0.0, 0.0>>


VECTOR g_TempCamPosvec

BOOL g_3dPhoneNeedsMovedUp, g_3dPhoneNeedsMovedDown


BOOL g_Cellphone_FullScreen_Mode = FALSE
BOOL g_Use_Prologue_Cellphone = FALSE


BOOL g_Cellphone_FH_Needs_To_Exit = FALSE



//Use these to determine where the 3d phone model should initially be positioned before moving it up onto the screen and where it should finish drawing.
//The declarations and screen type is ascertained in cellphone_controller.sc

ENUM Screen_Display_Type
    DISPLAY_16_9,
    DISPLAY_4_3
ENDENUM

Screen_Display_Type g_Chosen_Ratio

//These are initialised at runtime in cellphone controller when the screen ratio is determined.
VECTOR g_This_Screen_3dPhoneStartVec[2] 
VECTOR g_This_Screen_3dPhoneEndVec[2]




//Rotation at start... This is the default starting rotation of the phone.
VECTOR g_3dPhoneStartRotVec =  <<-90.0, 0.0, 0.0>>//<<-90.00, 2.7, -0.0>> //Old RHS<<-89.8, -14.7, 0.0>>
VECTOR g_3dPhonePocketRotVec =  <<-90.0, -100.4, 0.0>>//<<-90.00, 2.7, -0.0>> //Old RHS<<-89.8, -14.7, 0.0>>



//When the phone is moving down screen, it is temporarily blocked...
BOOL g_Phone_Blocked_While_Moving_Down = FALSE





//Temporary macro for the action to take the phone from a pocket into min mode. A subsequent press will maximise the phone.
//Important! DPAD / CIRCLE / SQUARE should be replaced by whatever INPUT enum entry is eventually provided for this from commands_pad.sch

//Update 15.02.12 Instructed to use frontend controls by Adam F. May cause issues with any Japanese build due to frontend switchover, so need to consider this.
//CONTROL_ACTION

INT PHONE_TAKEOUT_INPUT = ENUM_TO_INT (INPUT_PHONE)//Changed at request of Stephen R.   //(INPUT_CELLPHONE_UP)   //(INPUT_FRONTEND_UP)       //(DPADUP)  //
INT PHONE_GO_BACK_INPUT = ENUM_TO_INT (INPUT_CELLPHONE_CANCEL)                          //(INPUT_FRONTEND_CANCEL)   //(CIRCLE)

INT PHONE_POSITIVE_INPUT = ENUM_TO_INT (INPUT_CELLPHONE_SELECT)                         //(INPUT_FRONTEND_ACCEPT)  //(CROSS)
INT PHONE_NEGATIVE_INPUT = ENUM_TO_INT (INPUT_CELLPHONE_CANCEL)                         //(INPUT_FRONTEND_CANCEL)  //(CIRCLE)


//May need to change these frontend control assignments.
INT PHONE_DELETE_OPTION_INPUT =   ENUM_TO_INT (INPUT_CELLPHONE_EXTRA_OPTION)            //(INPUT_CELLPHONE_OPTION)      //ENUM_TO_INT //(INPUT_FRONTEND_Y) //(TRIANGLE)
INT PHONE_SPECIAL_OPTION_INPUT =  ENUM_TO_INT (INPUT_CELLPHONE_EXTRA_OPTION)            //(INPUT_CELLPHONE_OPTION)      //ENUM_TO_INT //(INPUT_FRONTEND_Y) //(TRIANGLE)

INT PHONE_EXTRA_SPECIAL_OPTION_INPUT =  ENUM_TO_INT (INPUT_CELLPHONE_OPTION)            //(INPUT_CELLPHONE_EXTRA_OPTION)      //(SQUARE) 


INT PHONE_NAV_LEFT_INPUT = ENUM_TO_INT (INPUT_CELLPHONE_LEFT)                           //(INPUT_FRONTEND_LEFT)   //(DPADLEFT)
INT PHONE_NAV_RIGHT_INPUT = ENUM_TO_INT (INPUT_CELLPHONE_RIGHT)                         //(INPUT_FRONTEND_RIGHT)  //(DPADRIGHT)
INT PHONE_NAV_UP_INPUT = ENUM_TO_INT (INPUT_CELLPHONE_UP)                               //(INPUT_FRONTEND_UP)     //(DPADUP)
INT PHONE_NAV_DOWN_INPUT = ENUM_TO_INT (INPUT_CELLPHONE_DOWN)                           //(INPUT_FRONTEND_DOWN)   //(DPADDOWN)



/*
    INPUT_CELLPHONE_UP,
    INPUT_CELLPHONE_DOWN,
    INPUT_CELLPHONE_LEFT,
    INPUT_CELLPHONE_RIGHT,
    INPUT_CELLPHONE_SELECT,
    INPUT_CELLPHONE_CANCEL,
    INPUT_CELLPHONE_OPTION,
    INPUT_CELLPHONE_SCROLL_FORWARD,
    INPUT_CELLPHONE_SCROLL_BACKWARD,
*/





//Usage example:
// IF IS_BUTTON_PRESSED (PAD1, INT_TO_ENUM(PAD_BUTTON_NUMBER, PHONE_TAKEOUT_INPUT)) etc.



BOOL g_InputButtonJustPressed = FALSE //used to prevent button presses following through screen modes - can't use wait with scaleform at certain points.

//Temp - will be handled by flash navigation...
BOOL dpad_pause_cued = FALSE  //might not be!

INT AppCursorIndex = 0




/*See bug 605548 for removal reasons.
ENUM e_DelayCinematicCamReinstatement

    NO_DELAY_REQUIRED,
    INITIALISE_DELAY,
    ONGOING_DELAY
     
ENDENUM



e_DelayCinematicCamReinstatement g_DelayCinematicCamReinstatement = NO_DELAY_REQUIRED
*/







//Scaleform index global for storing Scaleform Movie ID

SCALEFORM_INDEX SF_MovieIndex //SF_MovieButtonsIndex

BOOL g_B_Scaleform_Movies_Loaded = FALSE //boolean to prevent double loading of scaleform movies assets.
BOOL g_B_External_Interrupt_Autoplay_MovieFail_Flag = FALSE
BOOL g_Attempted_Call_Aborted = FALSE
BOOL g_B_ForcedAway = FALSE



BOOL g_PhoneGuiRender_Enabled = TRUE
BOOL g_MainGui_RendertargetNeedsReset = FALSE     


//Button constants. Old order.
/*
CONST_INT BADGER_POS    1
CONST_INT BADGER_OTHER  2
CONST_INT BADGER_NEG    3
*/


//Test order
CONST_INT BADGER_OTHER  1
CONST_INT BADGER_POS    2
CONST_INT BADGER_NEG    3


BOOL g_b_ToggleButtonLabels



//Audio Sound IDS for playing keytones / ringtones

//INT g_Phone_Ringtone_SoundID

TEXT_LABEL_23 g_Owner_Soundset




//Sorting method for Repeat Play list.
INT g_RepeatPlaySortingMethod = 0
INT g_CheckListSortingMethod = 0








//Cellphone ThreadID.

THREADID Cellphone_FlashResponseThread, Application_Thread









// Enum to signify whether the phone should be absent from the screen, drawn minimal so that only common functions
// are present or fully drawn on screen in a number of various states.

// DO NOT ALTER THIS! CRITICAL PHONE STUFF!

ENUM enumPhoneDisplayStatus

    PDS_DISABLED_THIS_FRAME_ONLY, //0
    PDS_DISABLED,   //1
    PDS_SLEEPING,   //2
    PDS_AWAY,       //3
    PDS_TAKEOUT,    //4
    PDS_MINIMAL,    //5
    PDS_MAXIMUM,    //6
    PDS_RUNNINGAPP, //7
    PDS_COMPLEXAPP, //8
    PDS_ONGOING_CALL,   //9
    PDS_ATTEMPTING_TO_CALL_CONTACT  //10

ENDENUM


               

ENUM enumInboundCallStatus

    CALL_NONE_WAITING,
    CALL_WAITING_TO_BE_ANSWERED,
    CALL_ACCEPTED,
    CALL_REJECTED

ENDENUM

enumInboundCallStatus g_InboundCallWaitAccRej











STRUCT structCellphone
    
    enumCharacterList PhoneOwner
    enumPhoneDisplayStatus PhoneDS 
    //Perhaps have call status in here...

ENDSTRUCT





//The main cellphone struct.
StructCellPhone g_Cellphone






//Phone Operating System and Provider Constants

//They have to be in this order so they match up with a scaleform method for selecting the appropriate web browser skin.
CONST_INT OS_BADGER 0

CONST_INT OS_BITTERSWEET 0
CONST_INT OS_POLICE 1
CONST_INT OS_FACADE 2
CONST_INT OS_IFRUIT 3


CONST_INT PROVIDER_DEFAULT  0  //Will default to badger in scaleform.
CONST_INT PROVIDER_BADGER   1
CONST_INT PROVIDER_WHIZ     2
CONST_INT PROVIDER_TINKLE   3


CONST_INT WALLPAPER_DEFAULT     0
CONST_INT WALLPAPER_BADGER      1
CONST_INT WALLPAPER_WHIZ        2
CONST_INT WALLPAPER_TINKLE      3















//Inner workings.


enumPhoneDisplayStatus BeforeCallPhoneDS //used to store whatever the phone display state was before a call was made. If a call is accepted, then finished or hung up, the phone will 
                                         //return to whatever state it was in before...



//Stores selected index values after player navigates phone menus which are array based.
INT SelectedAppIndex 

INT Previous_SelectedAppCursorPos_PageOne, Previous_SelectedAppCursorPos_PageTwo //stores a copy of the last selectedAppIndex cursor position so that it can be restored on a return to the homescreen.

//INT g_SelectedContactIndex



//Moved here so that it can be interrogated by public functions used in phone tutorial.
//CONST_INT MaxNumberOfContactSlots 40

INT ContactListSlot[MAX_CHARACTERS_PLUS_DUMMY]  //up for deletion. appContacts can now be cutdown post scaleform. Will check debug overlays.



enumCharacterList g_ForcedContactSelection = NO_CHARACTER



//Global to hold whether or not the phone was brought up by the player or launched from a script command.
BOOL g_Cellphone_Launched_by_Button_Press = FALSE





//Misc cellphone functionality used by cellphone_public.sch

//Alert signifiers - these specify which alerts should display on the hud. If TRUE they will be shown.
BOOL g_DisplayNewSideTaskSignifier
//BOOL g_DisplayNewEmailSignifier,  g_DisplayNewAppointmentSignifier, g_DisplayJobListMsgSignifier



BOOL g_Phone_Active_but_Hidden = FALSE

BOOL g_Homescreen_In_Secondary_Mode, g_b_Secondary_Screen_Available

BOOL g_b_QuickSaveGreyedOut = FALSE

BOOL g_b_CameraGreyedOut = FALSE


BOOL Movement_Exception = FALSE //lets the 1st person animations and scaleform movement homescreen routines know if they should swap. Set from cellphone_flashhand's navigation processes.


BOOL g_b_ReplaceThisWithQSavailable = TRUE  //This can be removed at a later date when a function is used to check for quick save availability. This is merely a test var to
                                            //allow a test run of dynamic homescreen icon replacement.
                                            //Other references in cellphone_private.sch, cellphone_flashhand.sc and for testing, cellphone_controller.









//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Conversation Buffer
//
//________________________________________________________________________________________________________________________________________________________________________________



CONST_INT   g_BS_STANDARD_CONVERSATION_BUFFER_ACTIVE            0 
CONST_INT   g_BS_STANDARD_CONVERSATION_BUFFER_TIMER_BEGUN       1
 

INT BitSet_DialogueHandler = 0 //clear all bits.




//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Dialogue Handling Globals.
//
//________________________________________________________________________________________________________________________________________________________________________________


CONST_INT constConversationLabels 70 //There are conversations of approximately 70 lines long already - including SFX pauses.

CONST_INT constMaxNum_Conversers 16 //This could be increased up to 36, 0-9 plus A to Z which populate array indices 10 to 36.
                                    //If voice IDs were still passed by script at that point, we would need to swap over to an enum based look up table to avoid stack limits.
                                    //e.g Voice ID of VOICE_MICHAEL would then be cross referenced with an array of strings in dialogue handler or elsewhere to grab and
                                    //pass into the native commands.
                                                                                          
BOOL g_b_PreloadOngoing = FALSE //Critical preload bool which has to be hacked in at two minutes to midnight. Risky to use bit here.



TEXT_LABEL_23 g_ConversationLabels[constConversationLabels]  //the maximum amount of labels we can have in any one conversation.



//Structs for peds in conversation and the associated textblock. I'm deliberately keeping these two structs separate rather than combining the peds within the conversation data
//in an effort to minimize bugs by keeping the text block fairly isolated from the ped array.

STRUCT StructIndividualPedInfo

    PED_INDEX       Index
    TEXT_LABEL_23   VoiceID //decreased from 31 as MaxNum_Conversers has just been upped to 14. If this becomes an actual issue, then enum system comes into play.
    BOOL            ActiveInConversation
    BOOL            PlayAmbientAnims
    BOOL            CanUseAutoLookAt //Specifies whether the ped will use the automatic "look at" code system during scripted conversations.

ENDSTRUCT



STRUCT structPedsForConversation

    structIndividualPedInfo PedInfo[constMaxNum_Conversers]

    INT     NullPed_Number_for_VoicePlacement
    VECTOR  NullPed_Vector_for_VoicePlacement 

ENDSTRUCT




CONST_INT constMaxMultiparts 30 //Upped from 10 to facilitate "Shrink" conversations.

STRUCT structConversationData

    TEXT_LABEL_23 ConversationSegmentToGrab  
    TEXT_LABEL_23 MultipartSegmentToGrab[constMaxMultiparts] 
    TEXT_LABEL_23 MultipartSpecificLabel[constMaxMultiparts] 

    BOOL g_DisplaySubtitles
    BOOL g_AddToBriefScreen
    BOOL g_CloneConversation

ENDSTRUCT



//Global instance of array of conversation ped structure and holder.
structPedsForConversation g_ConversationPedsStruct, g_ConversationPedsHolderStruct




//Global instance of conversation structure
structConversationData g_ConversationData


TEXT_LABEL_23 g_BufferSegmentToGrab


//Global ints to hold number of parts in multipart conversation
INT g_TotalMultiparts = 0
INT g_TotalMultipartsHolder = 0

INT g_TotalReplyMultiparts = 0
INT g_TotalReplyMultipartsHolder = 0








ENUM enumConversationStatus


    CONV_STATE_FREE,
    CONV_STATE_BUILDING,
    CONV_STATE_WAITING_ON_PLAYER_ACCREJ,
    CONV_STATE_PAUSE_ANSWERING_TIME, 
    CONV_STATE_PLAYING,
    CONV_STATE_HANGUPAWAY, //special case for player hanging up phone.
    CONV_STATE_FINISHED


ENDENUM

enumConversationStatus g_ConversationStatus












ENUM enumConversationPriority

    // no priority - nothing is playing
    CONV_PRIORITY_NONE,

    CONV_PRIORITY_AMBIENT_LOW,
    CONV_PRIORITY_NON_CRITICAL_CALL,
    CONV_PRIORITY_AMBIENT_MEDIUM,
    CONV_PRIORITY_AMBIENT_HIGH,

    CONV_PRIORITY_VERY_LOW,
    CONV_PRIORITY_LOW,
    CONV_PRIORITY_MEDIUM,
    CONV_PRIORITY_HIGH,
    CONV_PRIORITY_VERY_HIGH,

    CONV_PRIORITY_MP_SPECIAL,

    //Put cellphone calls at top priority so that any other scripted conversations cannot interrupt.
    CONV_PRIORITY_CELLPHONE,

    CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT,   //Special use by flow phonecalls only. This will force this priority to load into the ambient dialogue slot
                                                //in dialogue handler even though the conversation is very high priority and will not be overwritten
                                                //by face-to-face or other ambient conversations. This addresses pre-loaded cutscenes hogging / overwriting 
                                                //the mission dialogue text slot. See #1242086


    CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT_BYPASS_SLEEPMODE, //As above but these are even more important in that they can bypass sleep mode. FLOW USE ONLY!


    CONV_MISSION_CALL  //Highest priority for end of mission calls?


ENDENUM




BOOL g_Non_Essential_Call_Aborted_and_CS_Already_Free = FALSE




enumConversationPriority g_CurrentlyPlayingConvPriority //stores the priority of the current conversation so that it can be interrogated by any other conversation attempting
                                                        //to cue which may be of a higher priority and can therefore cancel the currently playing conversation.

enumConversationPriority g_BufferConvPriority






BOOL g_IsConferenceCall, g_IsConferenceCallHolder //lets cellphone_flashhand.sc know if the call is a conference call and as such, whether to display multiple names on the "calling" screen.







BOOL g_IsThisConversationForPhone, g_IsThisConversationForPhoneHolder //lets my systems know whether this conversation is intended for the phone or a face to face conversation.

BOOL g_IsThisAnMPJobOffer, g_IsThisAnMpJobOfferHolder




BOOL g_UpcomingConversationInboundForCellphone //holds whether the call is inbound to the player's cellphone from another char as opposed to the player calling out proactively.

BOOL g_ShouldPhoneBeForcedOnScreen, g_ShouldPhoneBeForcedOnScreenHolder //should the phone be automatically brought up on screen when this call is cued?

BOOL g_IsThisAnMPChatCall, g_IsThisAnMPChatCallHolder //this is a special call state that will go through the phonecall graphic system and dialogue handler but not initiate any dialogue as such.

TEXT_LABEL_63 g_ChatCallerString, g_ChatCallerStringHolder



BOOL g_ForcePlayerAnswer = FALSE  //should the player be forced to answer the phone? Any forced answer call means the player is unable to reject any incoming call.
BOOL g_ForcePlayerAnswerHolder = FALSE


BOOL g_LastCellphoneCallInterrupted = FALSE //if the last phonecall did not conclude naturally at the end of the final line of the conversation, this will be set to true.

BOOL g_LastCallHungupDuringCallViaJoypad = FALSE //if the last phonecall was concluded after answering by the user actively hanging up via the joypad during
                                                //PlayCellphoneConversation then this will also be set to true.




BOOL g_HasStateBeenPreserved = FALSE //has the phone's display state been stored so that it can be reverted back to after any incoming call has concluded. 



//Globals to handle yes / no responses and phonecall segment labels to conversations that offer branching choices. Need holders to avoid public functions overwriting data.
BOOL g_CallRequiringPlayerResponseHolder, g_CallRequiringPlayerResponse
BOOL g_IsEmergencyServicesCallHolder,  g_IsEmergencyServicesCall


BOOL g_AllowUnderwaterDialogue  //Under normal circumstances, conversations are terminated when the player enters water. 
                                //There are some occasions when this restriction needs to be circumvented. FBI5a for example.



//Dynamic phone calls
BOOL g_PrepareCallForDynamicBranch //This lets dialogue handler know that the next time a phone call terminates or is interrupted via script, it should not auto-hang up or change view state.


TEXT_LABEL_23 g_DynamicBranch_Root, g_DynamicBlockToLoad //The new conversation root and block




//Conversation and phonecall labels

TEXT_LABEL_23 g_BlockToLoadHolder, g_BlockToLoad


//Conceivably, these segment labels could just be replaced by an array of a single holder but that makes it less transparent as to what's going on in some functions.
//This may however be adopted if we have more special case question response calls outwith YES / NO and AMBULANCE/FIRE/POLICE
TEXT_LABEL_23 g_YesSegmentLabelHolder, g_NoSegmentLabelHolder

TEXT_LABEL_23 g_YesSegmentLabel, g_NoSegmentLabel

TEXT_LABEL_23 g_AmbulanceSegmentLabelHolder, g_FireSegmentLabelHolder, g_PoliceSegmentLabelHolder
TEXT_LABEL_23 g_AmbulanceSegmentLabel, g_FireSegmentLabel, g_PoliceSegmentLabel


TEXT_LABEL_23 g_QuestionGodLabel, g_QuestionGodLabelHolder






//Special case for Alwyn's friend activity calls which only require a maximum of a two-line multipart reply. May optimise this to use upper limits of standard multipart array.
//Will consolidate into universal processing below when convenient.
TEXT_LABEL_23 g_Multipart_YesSegmentLabel[2]
TEXT_LABEL_23 g_Multipart_NoSegmentLabel[2]

TEXT_LABEL_23 g_Multipart_YesSegmentLabelHolder[2]
TEXT_LABEL_23 g_Multipart_NoSegmentLabelHolder[2]


TEXT_LABEL_23 g_Multipart_YesSpecificLabel[2]
TEXT_LABEL_23 g_Multipart_NoSpecificLabel[2]

TEXT_LABEL_23 g_Multipart_YesSpecificLabelHolder[2]
TEXT_LABEL_23 g_Multipart_NoSpecificLabelHolder[2]

BOOL g_b_Is_MultipartRepliesInProgress = FALSE







//New methodology to negate holder to save globals. Used in conjunction with a bit and proper checking by polling scripts, i.e checking for a return of TRUE,
//this should be safe enough as the system uses a locking bit.
TEXT_LABEL_23 g_Multipart_Universal_YesRoots[constMaxMultiparts]
TEXT_LABEL_23 g_Multipart_Universal_NoRoots[constMaxMultiparts]

TEXT_LABEL_23 g_Multipart_Universal_YesSpecifics[constMaxMultiparts]
TEXT_LABEL_23 g_Multipart_Universal_NoSpecifics[constMaxMultiparts]

INT g_i_Total_UniversalNoReplies = 0
INT g_i_Total_UniversalYesReplies = 0                                      






















//Miscellaneous


ENUM enumSubtitlesState

    DISPLAY_SUBTITLES,
    DO_NOT_DISPLAY_SUBTITLES

ENDENUM


ENUM enumBriefScreenState

    DO_ADD_TO_BRIEF_SCREEN,
    DO_NOT_ADD_TO_BRIEF_SCREEN

ENDENUM



enumSubtitlesState g_AssignDisplaySubtitlesBuffer

enumBriefScreenState g_AssignAddtoBriefScreenBuffer

BOOL g_DisplaySubtitlesHolder, g_AddToBriefScreenHolder, g_CloneConversationHolder




BOOL g_PlayingSingleLine, g_PlayingSingleLineHolder

BOOL g_IsThisFaceToFacePreloaded, g_IsThisFaceToFacePreloadedHolder

INT g_i_PreloadAutoPlayTime = 0 
INT g_i_PreloadAutoPlayTimeHolder = 0


BOOL g_PlayingFromLine, g_PlayingFromLineHolder

TEXT_LABEL_23   g_SpecificLabel, g_SpecificLabelHolder
INT g_DialogueAdjustment //used for adjusting the value returned by getting the currently playing conversation line.




BOOL g_ConversationPaused = FALSE

BOOL g_PauseRestartHasComeFromDedicatedCellphoneFunction = FALSE


BOOL g_TellFlashHandToDisplayYN, g_b_RadarWasAlreadyHidden 

BOOL g_TellFlashHandToDisplayEmergencyServices

BOOL g_LastInboundCallRejected = FALSE




//Used in cellphone when a phonecall requires a response.

ENUM enumCellphonePromptResponse

    RESPONSE_STORE_EMPTY,
    RESPONDED_YES,
    RESPONDED_NO,
    RESPONDED_AMBULANCE,
    RESPONDED_FIRE,
    RESPONDED_POLICE,
    RESPONSE_DYNAMIC_BRANCH

ENDENUM

enumCellphonePromptResponse g_CellphonePromptResponse, g_CellphoneJobOfferResponse




//Answerphone and engaged tone handling.
INT Connection_EngagedPauseLength = 1521  //specifies the length of time an attempted call to a contact without an answerphone msg will have to be grabbed by the flow
                                          //before returning an engaged tone. Keep this as a relatively unique number so to avoid false comparison results.

INT Connection_AnsMsgPauseLength = 9000   //specifies the length of time an attempted call to a contact with an answerphone msg will have to be grabbed by the flow
                                          //before playing the answerphone message.


CONST_INT g_Const_Outgoing_Call_Pause_Answering_Time 5000 //Standard of 5 seconds pause when player makes outgoing call and the cellphone conversation begins.
                                                          //This simulates the other character's answering time.

INT g_Outgoing_Call_Pause_Answering_Time = g_Const_Outgoing_Call_Pause_Answering_Time



INT g_Timestamp_of_last_ConvStateFree = 0


TEXT_LABEL_31 answerPhoneBlock = "ANAUD" //Any answerphone messages for any contact should be stored in this block in dialoguestar,



 enumCharacterList g_AutoScrollContact //Hold the character enum of a contact which we desire to scroll to when the contacts list is selected.


BOOL g_b_AllowHidePhoneDuringCallTrial = FALSE      






                                        



//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Cellphone_Camera operations.
//
//________________________________________________________________________________________________________________________________________________________________________________


//Rotation globals
BOOL g_b_Rotate3dPhonePortrait = FALSE
BOOL g_b_Rotate3dPhoneLandscape = FALSE


ENUM eCellphonePicProcessStage

    PIC_STAGE_IDLE,
    PIC_SHUTTER_DELAY,
    PIC_STAGE_HQ_BEGIN,
    PIC_STAGE_HQ_ONGOING,
    //PIC_STAGE_HQ_SUCCEEDED,
    PIC_STAGE_LQ_BEGIN,
    PIC_STAGE_LQ_ONGOING,
    //PIC_STAGE_LQ_SUCCEEDED,
    PIC_STAGE_HOLDING_LQ_COPY,
    PIC_STAGE_HQ_SAVE,
    PIC_CHECK_STORAGE,
    PIC_STAGE_STORE_TO_MEDIA,
    PIC_STAGE_STORE_TO_MEDIA_ONGOING,
    PIC_STAGE_POST_SAVE_PAUSE,
    PIC_STAGE_LOAD_FROM_MEDIA,
    PIC_STAGE_LOAD_FROM_MEDIA_ONGOING,
    PIC_STAGE_TIDY_UP

ENDENUM 


eCellphonePicProcessStage g_Cell_Pic_Stage 


BOOL Cellphone_Pic_Just_Taken

BOOL g_b_appHasRequestedTidyUp = FALSE
BOOL g_b_appHasRequestedSave = FALSE

INT g_LoadFromSlot, g_i_PhotoSlot












//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Text Messaging
//
//________________________________________________________________________________________________________________________________________________________________________________



ENUM enumTxtMsgLockedStatus //will specify whether or not this message can be freely deleted from the list if the text message array nears capacity.

    TXTMSG_EMPTY,
    TXTMSG_UNLOCKED,
    TXTMSG_LOCKED

ENDENUM



ENUM enumTxtMsgMissionCritical  //additional specifier for mission critical text message that could be used to display a message at the top of the display list.
                                
    TXTMSG_NOT_CRITICAL,
    TXTMSG_CRITICAL

ENDENUM


ENUM enumTxtMsgAutoUnlockAfterRead //If a message has been locked on sending, i.e it can't be deleted by the user via the phone interface, this specifies if it will auto unlock on reading.
                                
    TXTMSG_AUTO_UNLOCK_AFTER_READ,
    TXTMSG_DO_NOT_AUTO_UNLOCK

ENDENUM





ENUM enumTxtMsgIsReplyRequired //specifies whether this text message will allow the player to reply with yes / no... a "replay mission" txt for example.

    NO_REPLY_REQUIRED,
    REPLY_IS_REQUIRED, 
    REPLIED_BARTER,
    REPLIED_YES,
    REPLIED_NO

ENDENUM


ENUM enumTxtMsgCanCallSender

    CANNOT_CALL_SENDER,
    CAN_CALL_SENDER

ENDENUM


ENUM enumTxtMsgIsBarterRequired

    NO_BARTER_REQUIRED,
    BARTER_IS_REQUIRED

ENDENUM




ENUM enumTxtMsgDeletionMode 

    DELETE_FROM_THIS_CHAR,
    DELETE_FROM_ALL_CHARACTERS

ENDENUM



ENUM enumTxtMsgReadStatus

    UNREAD_TXTMSG,
    READ_TXTMSG

ENDENUM



ENUM enumTxtMsgSpecialComponents
    
    NO_SPECIAL_COMPONENTS,
    STRING_COMPONENT,
    NUMBER_COMPONENT,
    STRING_AND_NUMBER_COMPONENT,
    CAR_LIST_COMPONENT,
    SUPERAUTOS_LIST_COMPONENT,
    LEGENDARY_LIST_COMPONENT,
    PEDALMETAL_LIST_COMPONENT,
    WARSTOCK_LIST_COMPONENT,
    ELITAS_LIST_COMPONENT,
    DOCKTEASE_LIST_COMPONENT,
    DAILYOBJ_LIST_COMPONENT,  //Added to support Martin's g_DailyObjectivesList[3] #2018068
	DAILY_CHALKBOARD_VEHICLE_LIST_COMPONENT // url:bugstar:7188269

ENDENUM


STRUCT structTxtMsgTimeSent


    INT TxtMsgSecs
    INT TxtMsgMins
    INT TxtMsgHours

    INT TxtMsgDay
    INT TxtMsgMonth // The native command uses an enum to hold the return value from the current month - will enum-to-int for uniformity's sake.
    
    INT TxtMsgYear


ENDSTRUCT





STRUCT structTextMessage


    //Removed on trial basis. Text messages are using global text rather than a text block in the interim.
    //TEXT_LABEL                    TxtBlockToLoad
    
    TEXT_LABEL_63                   TxtMsgLabel 

    INT                             TxtMsgFeedEntryId


    enumCharacterList               TxtMsgSender 

    structTxtMsgTimeSent            TxtMsgTimeSent


    enumTxtMsgLockedStatus          TxtMsgLockStatus 
    enumTxtMsgMissionCritical       TxtMsgCritical
    enumTxtMsgAutoUnlockAfterRead   TxtMsgAutoUnlockStatus


    enumTxtMsgDeletionMode          TxtMsgDeletionMode


    enumTxtMsgReadStatus            TxtMsgReadStatus
    enumTxtMsgIsReplyRequired       TxtMsgReplyStatus
    enumTxtMsgCanCallSender         TxtMsgCanCallSenderStatus
    enumTxtMsgIsBarterRequired      TxtMsgBarterStatus

    enumTxtMsgSpecialComponents     TxtMsgSpecialComponents

    TEXT_LABEL_63                   TxtMsgStringComponent
    INT                             TxtMsgNumberComponent

    
    //Upped this to a TL_63 for 2024559
    TEXT_LABEL_63                   TxtMsgSenderStringComponent


    INT                             TxtMsgNumberOfAdditionalStrings
    TEXT_LABEL_63                   TxtMsgSecondStringComponent
    TEXT_LABEL_63                   TxtMsgThirdStringComponent


    BOOL                            PhonePresence[4]  //specifies whether this message should be included in each player character filter.





ENDSTRUCT



//When moving to single message view, if the chosen text message is found to have an associated appInternet url, then it is stored in this buffer.
//It is possible to extract the string from the text_label using GET_FILENAME_FOR_AUDIO_CONVERSATION. Andrew can then use ARE_STRINGS_EQUAL to check if
//it is "NO_HYPERLINK_EMBEDDED". If it isn't then he uses the actual contents as the starting webpage.

TEXT_LABEL_63 g_HyperLink_Buffered_Label = "NO_HYPERLINK"





//Last activatable feed type and ID that should be set whenever an Invite, text or new email feed item has been created.


ENUM ActivatableFeedType
    
    ACT_APP_NONE,
    ACT_APP_EMAIL,
    ACT_APP_INVITE,
    ACT_APP_TEXT,
    ACT_APP_BOSSINV

ENDENUM


INT g_Last_App_ActivatableFeedID //Written to whenever an EMAIL, INVITE or TEXT feed entry is created.

ActivatableFeedType g_Last_App_ActivatableType  //Written to whenever an EMAIL, INVITE or TEXT feed entry is created.

ActivatableFeedType CellphoneAppAutoLaunchType = ACT_APP_NONE   //Hold the last feed type when processing.


BOOL g_ShouldForceSelectionOfLatestAppItem = FALSE






// Keith 26/9/12: Contains the pre-generated headshotID when the text message is sent from an MP player.
//          If not NULL it should be valid - you can be sure by calling: IS_PEDHEADSHOT_VALID(PEDHEADSHOT_ID) and IS_PEDHEADSHOT_READY(PEDHEADSHOT_ID),
//              If either of these return FALSE then there is a problem somewhere so just use a default headshot TXD or something.
//          You can get the txdString associated with this headshot ID using: GET_PEDHEADSHOT_TXD_STRING(PEDHEADSHOT_ID),
//              sounds like you pass the returned string to the Feed function as both the TXD and the ImageName
PEDHEADSHOT_ID  g_playerTxtmsgHeadshotMP            = NULL

// Keith 3/12/12: FLAG: Auto-Launch Joblist App? If TRUE auto-launch joblist app when cellphone taken out.
BOOL            g_autolaunchJoblistApp              = FALSE

// Keith 16/7/13: global to count the number of unread joblist invites (to display the indicator on the joblist icon)
INT             g_numNewJoblistInvites              = 0

// Keith 6/11/14 [BUG 2060056]: FLAG: Only show signifier beside radar if there are unread 'player' invites only (not NPC invites)
BOOL            g_displayPlayerInvitesIndicator         = FALSE

// Keith 3/12/14 [BUG 2135305]: FLAG: We now want the player invites indicator to appear if any new player invites are received while the player is on the corona menus.
// This flag should become TRUE only if a player invite is received while on the corona menus, and should get automatically cleared whenthe player is no longer on the corona menus.
// If TRUE, the invites indicator on the left hand side of the screen (usually beside the radar) should be displayed as normal.
BOOL            g_forceDisplayPlayerInvitesIndicator    = FALSE




//structTxtMsgTimeSent TxtMsgComparisonTime

BOOL g_DoOptionsForSingleMessage = FALSE




//Text message array. For simplicity, MP and SP share the same array. A specific portion of this array is sectioned off for use by MP messages only.
//We need a couple of dummy positions, so SP is free to use 0 to 20, whilst MP can access 21 to 30.

CONST_INT MAX_TEXT_MESSAGES 35  //The number of text messages that we can store and display is equal to the MAX_TEXT_MESSAGES - 2. The chronological sort needs a dummy, one 
                                //is taken up for "while" loop comparison ease. So if MAX_TEXT_MESSAGES = 8, we can have 0,1,2,3,4 and 5 displayed.

CONST_INT SP_TEXT_PORTION_END 20    //0 - 19

CONST_INT SP_MICHAEL_BUFFER_INDEX   21  //Reserved buffers for text messages sent to a player character who is not currently being used.
CONST_INT SP_FRANKLIN_BUFFER_INDEX  22
CONST_INT SP_TREVOR_BUFFER_INDEX    23

CONST_INT MP_TEXT_PORTION_START 24  //24 onwards










INT TxtMsgIdentifiedFreeArrayIndex 


INT g_CurrentNumberOfUnreadTexts 
//INT g_CurrentNumberOfUnreadEmails, g_CurrentNumberOfUnreadSidetasks



//2d arrays work!
//structTextMessage g_TextMessage[3][MAX_TEXT_MESSAGES]




STRUCT g_TextMessageSavedData

    structTextMessage g_TextMessage[MAX_TEXT_MESSAGES]

ENDSTRUCT


//HeadshotStrings do not require to be saved.
PEDHEADSHOT_ID g_TxtMsgHeadShotID[MAX_TEXT_MESSAGES]


//Car List text decision enum


ENUM enumCarListMessageDecision
    
    NEW_CAR_LIST,
    UPDATED_CAR_LIST

ENDENUM


enumCarListMessageDecision g_CarListMessageDecision = NEW_CAR_LIST




//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Gallery and Image Resources.
//
//________________________________________________________________________________________________________________________________________________________________________________






CONST_INT MAX_GALLERY_IMAGES    10  

STRUCT structGalleryImage

    
    TEXT_LABEL_63 GalleryImage_ThumbLabel
    TEXT_LABEL_63 GalleryImage_PhotoLabel


ENDSTRUCT




STRUCT g_GalleryImageSavedData

    structGalleryImage g_GalleryImage[MAX_GALLERY_IMAGES]

ENDSTRUCT








//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Trackify Application
//
//________________________________________________________________________________________________________________________________________________________________________________


//Can be specified by another script using SET_TRACKIFY_TARGET_VECTOR

VECTOR g_v_TrackifyTarget



           




//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Friend Activity Display.
//
//________________________________________________________________________________________________________________________________________________________________________________




//BOOL Is_This_Player_On_Scheduled_Activity[3] 
//Needs to be a saved global so moved to STRUCT structThis_Cellphone_Owner_Settings definition.


BOOL This_Player_Request_Cancellation[3]

BOOL g_b_Is_This_An_Activity_Cancellation_Call = FALSE

BOOL g_b_Is_This_An_Activity_Call = FALSE

BOOL g_b_Is_This_A_Secondary_Function_Call = FALSE


//__________________________________________________________________________________________________________________
//
//                                              Camera Selfie 
//
//__________________________________________________________________________________________________________________


INT selfie_camera_help_text_system = 0

INT i_MP_selfie_camera_help_text_system = 0



//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      SideTasks - up for removal!
//
//________________________________________________________________________________________________________________________________________________________________________________


CONST_INT MAX_NUM_SIDETASKS 4   //The number of live sidetasks  that we can store and display is equal to the MAX_NUM_SIDETASKS - 2. The chronological sort needs a dummy, one 
                                //is taken up for "while" loop comparison ease. So if MAX_NUM_SIDETASKS = 8, we can have 0,1,2,3,4 and 5 displayed.



CONST_INT MAX_NUM_SIDETASK_PORTIONS 4

INT SideTaskIdentifiedFreeArrayIndex




ENUM enumSideTaskLockedStatus //will specify whether or not this message can be freely deleted from the list if the text message array nears capacity.

    SIDETASK_EMPTY,
    SIDETASK_UNLOCKED,
    SIDETASK_LOCKED

ENDENUM









//For use by ambient guys...


ENUM enumSideTaskList

    DUMMYSidetask_0,
    Sidetask_1,
    Sidetask_2,
    Sidetask_3

ENDENUM








STRUCT structSideTask


    TEXT_LABEL                  SideTaskTitle           //Display the task name.
    TEXT_LABEL                  SideTaskSynopsis        //Displays an overview of the task as whole.
    BOOL                        SideTaskFullyComplete   //Once all sidetask portions are complete, this becomes true.

    INT                         NumberOfSideTaskPortions        //Needed, so it can be easily determined how many sidetasks portions to display. 

    TEXT_LABEL SideTaskPortionLabel[MAX_NUM_SIDETASK_PORTIONS] //labels for the subtasks of the sidetask
    
    INT SideTaskPortionCurrentInt[MAX_NUM_SIDETASK_PORTIONS]
    INT SideTaskPortionTargetInt[MAX_NUM_SIDETASK_PORTIONS]                                                                        
    
    
    BOOL HasThisSideTaskPortionBeenCompleted[MAX_NUM_SIDETASK_PORTIONS]     //bools to signify whether each subtask has been completed.


ENDSTRUCT



structSideTask g_AmbientSideTask[MAX_NUM_SIDETASKS]

//For ambient guys, you would set up your sidetask and portion labels like so...


//g_AmbientSideTask[Sidetask_1].SideTaskSynopsis = "SYNOPSIS_LABEL1"
//g_AmbientSideTask[Sidetask_1].SideTaskFullyComplete = FALSE

//g_AmbientSideTask[Sidetask_1].NumberOfSideTaskPortions = 5
//g_AmbientSideTask[Sidetask_1].SideTaskPortionLabel[1] = "PORTION_LABEL1"
//g_AmbientSideTask[Sidetask_1].SideTaskPortionCurrentInt[1] = 1
//g_AmbientSideTask[Sidetask_1].HasThisSideTaskPortionBeenCompleted[1] = FALSE

//g_AmbientSideTask[Sidetask_1].SideTaskPortionLabel[2] = "PORTION_LABEL2"
//g_AmbientSideTask[Sidetask_1].HasThisSideTaskPortionBeenCompleted[2] = FALSE







//For use by me. Ambient guys will "create a new sidetask" and I will pass in these detail into a live sidetask array - like the text message system.

STRUCT structLiveSideTasks


    enumCharacterList           SideTaskGiver           //Optional -  we can pass in NO CHARACTER if there is no character from the char sheet involved.

    enumSideTaskList            SideTaskIdentifier      //Refers to the enum which will identify the various sidetasks.

    structTxtMsgTimeSent        TxtMsgTimeSent          //The time that the sidetask was delivered to the player - we may need this for sorting purposes. Can reuse txtmsg struct.

    enumSideTaskLockedStatus    SideTaskLockStatus    //used by my internal mechanisms. 
   

ENDSTRUCT



structLiveSideTasks g_LiveSideTask[MAX_NUM_SIDETASKS]



















//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Multiplayer Job List
//
//________________________________________________________________________________________________________________________________________________________________________________


/*Removed 14.10.11


ENUM enumJobListStatus

    JOB_EMPTY,
    JOB_AVAILABLE,
    JOB_CURRENT,
    JOB_TRIPSKIP,
    JOB_CANCELLED

ENDENUM





//Remove this...

STRUCT structJobList


    //Removed on trial basis. Text messages are using global text rather than a text block in the interim.
    //TEXT_LABEL                  TxtBlockToLoad
    
    TEXT_LABEL                  JobListLabel 


    enumCharacterList           JobListSender 

    structTxtMsgTimeSent        TxtMsgTimeSent


    enumJobListStatus           JobListStatus 
    
    //Details of the job
    INT                         iMission //Mission Enum converted to an int
    
    INT                         iInstance 


ENDSTRUCT






//BOOL g_DoOptionsForSingleJob = FALSE //Now within dedicated bitset.



CONST_INT MAX_JOB_STORAGE 8 //The number of text messages that we can store and display is equal to MAX_JOB_STORAGE - 2. The chronological sort needs a dummy, one 
                            //is taken up for "while" loop comparison ease. So if MAX_JOB_STORAGE = 8, we can have 0,1,2,3,4 and 5 displayed.

//INT g_CurrentNumberOfJobsStored, JobListIdentifiedFreeArrayIndex






structJobList g_JobListEntry[MAX_JOB_STORAGE]






//MP Bit Set.
//Add more to this if need be... 0, 1, 2... to 31
CONST_INT g_BS_MAINTAIN_NEW_JOB_REQUEST     0   
CONST_INT g_BS_BEEN_IN_COMPLEXAPP           1

//NEED TO REMOVE THIS ONE
CONST_INT g_BS_DO_CANCEL_MISSION_STUFF      5


//INT BitSet_CellphoneJobList = 0 //clear all bits.


INT JobListIdentifiedFreeArrayIndex


  

Removed 14.10.11*/



























//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Debug Variables
//
//________________________________________________________________________________________________________________________________________________________________________________



//Debug globals
#if IS_DEBUG_BUILD

    
    


    BOOL g_Cellphone_Disable_WidgetToggle
    BOOL g_Cellphone_Disable_ThisFrameOnly_WidgetToggle

    BOOL g_Cellphone_Onscreen_State_Debug
    BOOL g_b_DebugPhoneTimerDisplay

    BOOL g_b_CellDialDebugTextToggle = FALSE

    BOOL g_b_DisplayConversationToggle = FALSE
    BOOL g_b_DisplayProceduralConversationToggle = FALSE

  
    //BOOL g_Cellphone_FullScreen_Mode = FALSE

    BOOL g_Cellphone_Ratio_4_3_WidgetToggle, g_Cellphone_Ratio_16_9_WidgetToggle


    FLOAT debugrect_posX = 0.5, debugrect_posY = 0.5
    FLOAT debugrect_sizeX = 0.1, debugrect_sizeY = 0.1

    BOOL g_DoDebugTempDraw_Phone = FALSE

    BOOL g_b_DisplayDrawingMovieDump = FALSE

    BOOL b_DebugCellphoneGuiInfo = FALSE

    BOOL g_Debug_Signifiers_Hidden_by_Script = FALSE

    BOOL g_DumpDisableEveryFrameCaller = FALSE


    BOOL g_DisplayTestPhotoSprite = FALSE
    BOOL g_HighqualityPhotoSprite = FALSE

    INT  DebugCellphoneGui = 1 //Default startup gui.
    TEXT_LABEL_23 ScaleformDebugMovie[6]




    INT TestingNavigationInt = 0

    



#endif


