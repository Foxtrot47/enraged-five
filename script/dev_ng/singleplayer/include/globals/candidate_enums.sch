
// Mission candidate types.
ENUM m_enumMissionCandidateTypeID//Order of this list dictates the priority in case of activation at the same time
	MCTID_MUST_LAUNCH,				// Added By Keith 26/5/10: top priority to allow two missions to launch in sequence like one continuous mission
	MCTID_MEET_CHARACTER,			// To Be Used when assets are already created in the world prior to mission start
	MCTID_SELECTED_BY_PLAYER,		// To Be Used when the activity was selected by player through a button press, or an internet or email yes/no, etc.
	MCTID_REPLAY,					// Added By Keith 12/2/11: used when the player has accepted the option to replay
	MCTID_PLAYER_MADE_PHONECALL,	// To Be Used when the activity is started when the player chooses to phone a contact
	MCTID_PHONECALL_TO_PLAYER,		// To Be Used when the activity is started by a contact calling the player - this should be registered prior to the call and the call should only go ahead if permission is given)
	MCTID_CONTACT_POINT,				// To Be Used when the activity is started by the player walking into a contact point
	MCTID_RETRACTED//USED INTERNALLY, DO NOT PASS THIS INTO ANY CALL, ALL IT WILL DO IS ASSERT
ENDENUM

// Mission types
ENUM MISSION_CANDIDATE_MISSION_TYPE_ENUM
	MISSION_TYPE_STORY,
	MISSION_TYPE_SPMC,						// Mission creator singleplayer mission
	MISSION_TYPE_STORY_PREP, 				// Friends convert to battle buddies right away
	MISSION_TYPE_STORY_FRIENDS,				// Friends with cutscene transition
	MISSION_TYPE_RANDOM_CHAR,
	MISSION_TYPE_RANDOM_EVENT,
	MISSION_TYPE_FRIEND_ACTIVITY,			// On a friend activity with no minigame running yet.
	MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG,	// On a friend activity with a minigame running on top of it.
	MISSION_TYPE_FRIEND_SQUAD, 				// Like friend activity, but you can't start minigames or activities.
	MISSION_TYPE_MINIGAME,
	MISSION_TYPE_MINIGAME_FRIENDS,			// A minigame that can be triggered during a friend activity.
	MISSION_TYPE_RAMPAGE,
	MISSION_TYPE_EXILE,						// Overrides friend activities and REs but nothing else.
	MISSION_TYPE_ANIMAL,					// New next-gen mode where the player can play as an animal.
	MISSION_TYPE_DIRECTOR,					// New next-gen mode where the player is free to customise their character to aid recording videos.
	MISSION_TYPE_OFF_MISSION,
	
	//Blip specific theoretical launch levels used to perform comparisons. 
	//Should never actually be launched
	MISSION_TYPE_SHOP_BLIPS_AVAILABILITY,//can launch over anything but minigames
	
	//Types that are not actually missions but block missions
	MISSION_TYPE_SWITCH,
	MISSION_TYPE_GRIEFING
	
	#IF USE_CLF_DLC
	,MISSION_TYPE_CLF_CAR
	#ENDIF

	
ENDENUM

///request proc that registers and should be called repeatedly until the candidate system responds with acceptance or not
// Returns ACCEPTED/PROCESSING/DENIED///returns a candidate ID if on mission is available reference
ENUM m_enumMissionCandidateReturnValue
	MCRET_DENIED,
	MCRET_ACCEPTED,
	MCRET_PROCESSING//	MCRET_LAUNCHED
ENDENUM

CONST_INT NO_CANDIDATE_ID -1

CONST_INT MAX_MISSION_CANDIDATES 8

STRUCT m_structMissionCandidate
	INT mcid
	m_enumMissionCandidateTypeID type
	MISSION_CANDIDATE_MISSION_TYPE_ENUM missionType
	THREADID terminationThread
ENDSTRUCT
