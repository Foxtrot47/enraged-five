//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	social_globals.sch											//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Globals used to store social data. 							//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "model_enums.sch"

CONST_INT USE_MULTI_NUMBER_PLATE_SETUP_IN_MP 	1
CONST_INT MAX_NUMBER_OF_SC_LICENSE_PLATES 		30

ENUM CHOP_BEHAVIOUR_ENUM
	CHOP_BEHAVIOUR_MEDIUM = 0,
	CHOP_BEHAVIOUR_GOOD,
	CHOP_BEHAVIOUR_BAD
ENDENUM

ENUM FACEBOOK_POST_ENUM
	FBPOST_ACCOUNT_LINKED = 0,
	FBPOST_100_PERCENT,
	FBPOST_STORY_COMPLETE,
	FBPOST_ALL_VEHICLES,
	FBPOST_ALL_PROPERTIES,
	FBPOST_PSYCH_REPORT,
	FBPOST_FIRST_HEIST,
	FBPOST_FINAL_HEIST,
	FBPOST_MAP_REVEALED
ENDENUM
	
STRUCT SOCIAL_CAR_APP_DATA

	// Vehicle setup
	MODEL_NAMES eModel
	INT iColourID1
	INT iColourID2
	INT iWindowTint
	INT iTyreSmokeR, iTyreSmokeG, iTyreSmokeB
	INT iEngine
	INt iBrakes
	INT iWheels
	INT iWheelType
	INT iExhaust
	INT iSuspension
	INT iArmour
	INT iHorn
	INT iLights
	BOOL bBulletProofTyres
	INT iTurbo
	INT iTyreSmoke
	TEXT_LABEL_15 tlPlateText
	INT iPlateBack
	
	INT iModCountEngine
	INT iModCountBrakes
	INT iModCountExhaust
	INT iModCountWheels
	INT iModCountHorn
	INT iModCountArmour
	INT iModCountSuspension
	FLOAT fModPriceModifier
	INT iModColoursThatCanBeSet
	INT iHornHash[5]
	MOD_KIT_TYPE eModKitType
	
	// Game data
	BOOL bSendDataToCloud
	BOOL bDeleteData
	BOOL bUpdateMods
ENDSTRUCT

STRUCT SOCIAL_CAR_APP_ORDER_DATA

	// Vehicle setup
	MODEL_NAMES eModel
	INT iColourID1
	INT iColourID2
	INT iColour1Group
	INT iColour2Group
	INT iWindowTint
	INT iTyreSmokeR, iTyreSmokeG, iTyreSmokeB
	INT iEngine
	INt iBrakes
	INT iWheels
	INT iWheelType
	INT iExhaust
	INT iSuspension
	INT iArmour
	INT iHorn
	INT iLights
	BOOL bBulletProofTyres
	INT iTurbo
	INT iTyreSmoke
	TEXT_LABEL_15 tlPlateText
	INT iPlateBack
	
	// Cloud data
	INT iUID
	INT iCost
	TEXT_LABEL_15 tlPlateText_pending
	INT iPlateBack_pending
	
	// Game data
	BOOL bProcessOrder
	BOOL bOrderForPlayerVehicle
	BOOL bOrderReceivedOnBoot
	BOOL bOrderPending
	BOOL bCheckPlateProfanity
	BOOL bSCProfanityFailed
	BOOL bOrderFailedFunds
	BOOL bOrderPaidFor
ENDSTRUCT

STRUCT SOCIAL_DOG_APP_DATA
	FLOAT fHappiness
	FLOAT fCleanliness
	FLOAT fHunger
	INT iTrainingLevel
	
	INT iCollar
	
	BOOL bAppDataReceived
ENDSTRUCT


//////////////////////////////////////////////////////////////////////////////////////////
///    Struct for the global saved data
STRUCT SocialDataSaved
	SOCIAL_CAR_APP_DATA sCarAppData[NUM_OF_PLAYABLE_PEDS]
	SOCIAL_CAR_APP_ORDER_DATA sCarAppOrder[NUM_OF_PLAYABLE_PEDS]
	
	SOCIAL_DOG_APP_DATA sDogAppData
	
	BOOL bSingleplayeDataWiped
	BOOL bCarAppPlateSet
	BOOL bCarAppUsed
	BOOL bDogAppUsed
	BOOL bUpdateDogLocation
	BOOL bDeleteCarData
	INT iOrderToDelete
	
	TEXT_LABEL_15 tlCarAppPlateText
	INT iCarAppPlateBack
	
	INT iGameUID
	
	BOOL bPlayerUnlockedInApp[NUM_OF_PLAYABLE_PEDS]
	BOOL bFirstVehicleSentToCloud[NUM_OF_PLAYABLE_PEDS]
	BOOL bFirstOrderProcessed[NUM_OF_PLAYABLE_PEDS]
	
	INT iOrderCount[NUM_OF_PLAYABLE_PEDS]
	
	BOOL bCarAppHelpTextTriggered
	
	INT iFacebookPostsMadeBitset
	INT iActivityFeedPostsMadeBitset
ENDSTRUCT

BOOL g_bResetSocialController

#IF USE_MULTI_NUMBER_PLATE_SETUP_IN_MP
	TEXT_LABEL_15 g_tlPlateTextFromSCAccount[MAX_NUMBER_OF_SC_LICENSE_PLATES]
	BOOL g_bPlateTextFromSCAccountInMPList[MAX_NUMBER_OF_SC_LICENSE_PLATES]
	TEXT_LABEL_15 g_tlPlateTextForSCAccount[2]
	BOOL g_bAddSCPlateToMPList[2]
	BOOL g_bAddPlateTextToSC
	BOOL g_bRebuildSCPlateList
	BOOL g_bPlateCountOnSCAccountFull
	BOOL g_bInitialSCPlateListGrabbedMP
#ENDIF

BOOL g_bPopulateEmptyAppSlots = FALSE

BOOL g_bKickPlayerForInvalidTransaction = FALSE

