USING "flow_commands_enums_GAME.sch"
USING "commands_debug.sch"
USING "charsheet_global_definitions.sch"

#IF IS_DEBUG_BUILD

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	flow_debug_globals.sch
//		AUTHOR			:	Keith
//		DESCRIPTION		:	Globals used for mission flow debug widgets and screens.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// ---------------------------- AUTOPLAY SYSTEM --------------------------------
// Flag to inform other systems that the Autoplay system is active.
BOOL g_bFlowAutoplayInProgress 					= FALSE
BOOL g_bFlowAutoplayJustPassed					= FALSE

//A debug value that tracks which mission is running by pointing to an index in 
//the mission data array. Used by the autoplay system to output a meaningful log of 
//flow progress.
TEXT_LABEL_31 g_txtFlowAutoplayRunningMission 	= "NO MISSION"


// -------------------------- METRICS ZONE SYSTEM ------------------------------
BOOL 			g_bFlowMetricZonesEnabled = FALSE
BOOL			g_bFlowMetricZoneInProgress = FALSE
BOOL 			g_bDisplayingMetricsZoneResults = FALSE
INT				g_iTimeDisplayedMetricsReport = -1
TIME_DATATYPE	g_iTimeDisplayedMetricsReport_NETWORK
TEXT_LABEL_31	g_txtCurrentMetricZoneName		= "NOT SET"

// -------------------------- SMOKE TEST SYSTEM ------------------------------
BOOL			g_bFlowSmoketestEnabled = FALSE
BOOL			g_bFlowSmoketestSectionActive = FALSE

// -------------------------- FLOW INFO SCREEN -------------------------------
FLOAT			g_fFlowInfoBackgroundHeight


STRUCT g_structDebugLanchAndForgetScripts
	TEXT_LABEL_31	filename
	THREADID		theThreadID
ENDSTRUCT

g_structDebugLanchAndForgetScripts	g_debugLaunchAndForgetScripts[DEBUG_MAX_LAUNCH_AND_FORGET_SCRIPTS]

BOOL			g_showFlowLaunchAndForgetScripts		= TRUE
//BOOL			g_bDebugDrawGameTimeDate				= FALSE

//Player starting character override.
enumCharacterList		g_eDebugForceStartingCharacter = NO_CHARACTER

//Debug system silencing.
BOOL			g_bDebugFlowSilencePhonecalls			= FALSE
BOOL			g_bDebugBlockAutosaves					= FALSE
BOOL			g_bDebugBlockAmbientScripts				= FALSE

//Debug app overrides used by debug_app_select_screen.sc
BOOL g_bDebugAppScreenOverride = FALSE
BOOL g_bDebugChopPlayedStatusOverride = FALSE
BOOL g_bDebugChopGoodStatusOverride = FALSE
BOOL g_bDebugLosSantosCustomsPlayedStatusOverride = FALSE

//Used for warping smoketest to go to end of game flow
BOOL g_end_of_game_flow = FALSE
BOOL g_disable_for_smoketest = FALSE

//Used to keep track of which iteration of a metrics test we're on.
INT g_iMetricsIteration = 0


// Bitset for blocking communications from queueing during or just after a debug launch.
INT g_iDebugLaunchBlockCommunication = 0

CONST_INT BIT_DBG_LNCH_COMM_BLOCK_WILDLIFE_PHOTO	0
CONST_INT BIT_DBG_LNCH_COMM_SEND_TONYA_3_TXT		1
CONST_INT BIT_DBG_LNCH_COMM_SEND_TONYA_4_TXT		2	


#ENDIF	// IS_DEBUG_BUILD

