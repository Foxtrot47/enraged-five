//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		MISSION NAME	:	stripclub_globals.sch										//
//		AUTHOR			:	Rob Bray												    //
//		DESCRIPTION		:	Global data saved between instances of visiting strip club	//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

ENUM STRIP_CLUB_ENUM
	STRIP_CLUB_LOW = 0
	/* unused
	STRIP_CLUB_MED,
	STRIP_CLUB_HIGH
	*/
ENDENUM

ENUM BOOTY_CALL_CONTACT_ENUM
	BC_STRIPPER_JULIET = 0,
	BC_STRIPPER_NIKKI,
	BC_STRIPPER_CHASTITY,
	BC_STRIPPER_CHEETAH,
	BC_STRIPPER_SAPPHIRE,
	BC_STRIPPER_INFERNUS,
	BC_STRIPPER_FUFU,
	BC_STRIPPER_PEACH,
	BC_TAXI_LIZ,
	BC_HITCHER_GIRL
	
ENDENUM

ENUM STRIP_CLUB_PLAYER_CHAR_ENUM
	STRIP_CLUB_PLAYER_CHAR_MICHAEL,
	STRIP_CLUB_PLAYER_CHAR_FRANKLIN,
	STRIP_CLUB_PLAYER_CHAR_TREVOR,
	STRIP_CLUB_PLAYER_CHAR_MULTI_0,
	STRIP_CLUB_PLAYER_CHAR_MULTI_1,
	STRIP_CLUB_PLAYER_CHAR_MULTI_2,
	STRIP_CLUB_PLAYER_CHAR_MULTI_3,
	STRIP_CLUB_PLAYER_CHAR_MULTI_4
ENDENUM

ENUM BOOTY_CALL_STATE_ENUM
	BOOTY_CALL_INACTIVE = 0,
	BOOTY_CALL_PLAYER_CALLING,
	BOOTY_CALL_STRIPPER_CALLING,
	BOOTY_CALL_SCRIPT_RUNNING
ENDENUM


// debug bools
#IF IS_DEBUG_BUILD
	//BOOL g_bDebugShowDropPts
	BOOL g_bDebugMaxOutLike
	BOOL g_bDebugRefreshOnce
	BOOL g_bDebugDisplayStripClubAndBootyCall
	BOOL g_bDebugAdvanceBootyCallTimes
	BOOL g_bDebugDisplayStripperPoints
	BOOL g_bDebugSetAsBootyCall[COUNT_OF(BOOTY_CALL_CONTACT_ENUM)][COUNT_OF(STRIP_CLUB_PLAYER_CHAR_ENUM)]
	BOOL g_bDebugForceSext
	BOOL g_bDebugForceBootyCall
	BOOL g_bDebugForceBootyCallDecline
	BOOL g_bDebugAddContact[COUNT_OF(BOOTY_CALL_CONTACT_ENUM)]
	BOOL g_bTurnOnLukeHCameraDebug
#ENDIF

//═══════════════════════════════════════════════════════════════════════════════
//═════════════════════════════╡ Unsaved data ╞══════════════════════════════════
//═══════════════════════════════════════════════════════════════════════════════

STRUCT BOOTY_CALL_DATA
	BOOTY_CALL_STATE_ENUM bootyCallState
	INT iStripperCalling = -1
	INT iBootyCallID = -1
	INT iNextCallPlayerTime
	INT iNextAvailableTime[COUNT_OF(BOOTY_CALL_CONTACT_ENUM)]
	INT iAddNumberTime[COUNT_OF(BOOTY_CALL_CONTACT_ENUM)]
	BOOL bStripperWillAccept
	BOOL bPreventingBootyCall
ENDSTRUCT
BOOTY_CALL_DATA g_bootyCallData

BOOL g_bPreventStripperApproach
BOOL g_bStripClubRunning
BOOL g_bDisableStripClubs


//═══════════════════════════════════════════════════════════════════════════════
//════════════════════════════╡ Data to be saved ╞═══════════════════════════════
//═══════════════════════════════════════════════════════════════════════════════

STRUCT STRIP_CLUB
	INT iReputation
ENDSTRUCT

STRUCT BOOTY_CALL_CONTACT
	INT iLike
	INT iNumPlayerDenials //# of times that the stripper has called but player didn't accept booty call
	INT iSextsSent
	INT iBCCBits
	BOOL bActivated	
ENDSTRUCT

STRUCT StripClubSaved
	STRIP_CLUB stripClub[COUNT_OF(STRIP_CLUB_ENUM)]
	BOOTY_CALL_CONTACT bootyCall[COUNT_OF(BOOTY_CALL_CONTACT_ENUM)]
ENDSTRUCT

ENUM GLOBAL_STRIPCLUB_FLAGS
	GSF_USING_BAR = BIT1,
	GSF_SENT_CLUB_HOSTILE = BIT2,
	GSF_FRONT_BOUNCER_DEAD = BIT3,
	GSF_THROWN_OUT_OF_CLUB = BIT4,
	GSF_IN_CLUB = BIT5,
	GSF_FRONT_BOUNCER_EXISTS = BIT6,
	GSF_FORCE_CREATE_FRONT_BOUNCER = BIT7,
	GSF_GETTING_LAP_DANCE = BIT8,
	GSF_POST_HEIST_SCENE_IS_PLAYING = BIT9
ENDENUM

ENUM GLOBAL_BOOTYCALL_FLAGS
	GBF_INJURED_ON_CALL = BIT1,
	GBF_CALLED_AFTER_INJURED = BIT2,
	GBF_MEET_PLAYER = BIT3
ENDENUM

STRUCT StripclubGlobals
	INT iStripclubFlags
	BOOTY_CALL_CONTACT_ENUM eCurrentBootyCall
ENDSTRUCT

StripclubGlobals g_StripclubGlobals
