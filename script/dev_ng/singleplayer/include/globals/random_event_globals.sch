
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	random_event_globals.sch
//		AUTHOR			:	Paul
//		DESCRIPTION		:	Random Event Globals.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

STRUCT RandomEventSaved
	INT iBitsetReUnlockA
	INT iBitsetReUnlockB
	INT iHelpCount
	INT iBlipFlashHelpCount
	INT iItemReturnHelpCount
	INT iNumberOfDeliveredPeds
	INT iSecurityVanHelps
	BOOL bHasPlayerAttackedDrugFarm
	INT iREVariationComplete[MAX_RANDOM_EVENTS]
	TIMEOFDAY eTimeBlockUntil[MAX_RANDOM_EVENTS]
ENDSTRUCT

ENUM RANDOM_EVENT_HELP_ENUM
	REHLP_EVENT_ACTIVE = 0,
	REHLP_BLIP_FLASH,
	REHLP_RETURN_ITEM
ENDENUM

ENUM RANDOM_EVENT_PASS_TYPE
	RE_FAIL_OR_NO_ATTEMPT = 0,
	RE_PASS_NORMAL,
	RE_PASS_KEEP_ITEM,
	RE_PASS_RETURN_ITEM,
	RE_PASS_ALTRUIST_DROP_OFF,
	RE_PASS_HELP_COPS,
	RE_PASS_HELP_CRIMINALS,
	RE_PASS_KEEP_VEHICLE,
	RE_PASS_RETURN_VEHICLE
ENDENUM

INT g_iREPassType
INT g_iREStartTime
INT g_iREVariation

//stores the current random event that has been succesfully launched
SP_RANDOM_EVENTS g_eCurrentRandomEvent = RE_NONE

//flag for the current random event being locked in
BOOL g_bRandomEventActive

//the time that the last active random event finished
INT g_iLastRandomEventLaunch

// ATM Robbery globals
VECTOR g_vLastCompletedATMRobbery

// A flag to make sure that lured runs straight after the jewl heist setup
BOOL g_bForceNextLuredEvent

INT g_iREHelpShown

#IF IS_DEBUG_BUILD

	TEXT_LABEL_63 g_sRandomEventDebugScriptRequested = ""
	INT g_iRandomEventDebugVarRequested = 0

#ENDIF

