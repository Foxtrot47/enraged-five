// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      SCRIPT NAME     :   charsheet_globals.sch
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Contains constant, enum, and struct definitions for filling out 
//                          character sheet. NB Should not contain instantiated variables.
//                          This is filled on start up by charsheet_initial.sc
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "mp_globals_new_features_tu.sch"

CONST_INT NUM_OF_PLAYABLE_PEDS   3  // Michael, Trevor, and Franklin


ENUM enumCharacterList

  
    //DO NOT PUT ANY CHARACTERS BEFORE CHAR_MICHAEL, FRANKLIN OR TREVOR
    //Other enums depend on this order.
	
	// ANY NEW MULTIPLAYER/ONLINE CONTACTS MUST BE PLACED AFTER MAX_CHARACTERS_SP_SAVE TO AVOID BLOATING SP SAVES

    //Player characters - DO NOT REMOVE, DO NOT ALTER.
    CHAR_MICHAEL,  //0
    CHAR_FRANKLIN, //1
    CHAR_TREVOR,   //2
    

    CHAR_MULTIPLAYER, //3

    CHAR_ALL_PLAYERS_CONF, //Special ID for calls that may involve different subsets of the player characters.
    CHAR_FRANK_TREV_CONF,
    CHAR_LEST_FRANK_CONF,
    CHAR_LEST_MIKE_CONF,
    CHAR_MIKE_FRANK_CONF,
    CHAR_MIKE_TREV_CONF,
    CHAR_STEVE_MIKE_CONF,
    CHAR_STEVE_TREV_CONF,

    // Story characters
    CHAR_LESTER,
    CHAR_LESTER_DEATHWISH,
    CHAR_JIMMY,
    CHAR_TRACEY,
    CHAR_ABIGAIL,
    CHAR_AMANDA,
    CHAR_SIMEON,
    CHAR_LAMAR,
    CHAR_RON,
    CHAR_CHENG,
    CHAR_CEOASSIST,	//See bug 2853732. This was CHAR_SAEEDA but I'm reusing this for the phonecall contact CEO Assistant.
    CHAR_STEVE,
    CHAR_WADE,
    CHAR_TENNIS_COACH,
    CHAR_SOLOMON,
    CHAR_LAZLOW,
    CHAR_ESTATE_AGENT,
    CHAR_DEVIN,     // CarSteal Financier
    CHAR_DAVE,
    CHAR_MARTIN,
    CHAR_FLOYD,
    CHAR_GAYMILITARY,
    CHAR_OSCAR,
    CHAR_CHENGSR,
    CHAR_DR_FRIEDLANDER,
    CHAR_STRETCH,
    CHAR_ORTEGA,
    CHAR_ONEIL,
    CHAR_PATRICIA,
    CHAR_PEGASUS_DELIVERY,
    CHAR_LIFEINVADER,
    CHAR_TANISHA,
    CHAR_DENISE,
    CHAR_MOLLY,
    
    // Unlockable heist crew members.
    CHAR_RICKIE,    //Lifehack hacker crew memeber.
    CHAR_CHEF,      //Chinese 1 meth lab cook crew member.


    CHAR_BLIMP,
    
    // Random character mission characters.
    CHAR_BARRY,
    CHAR_BEVERLY,
    CHAR_CRIS,
    CHAR_DOM,
    CHAR_HAO,
    CHAR_HUNTER,
    CHAR_JIMMY_BOSTON,
    CHAR_JOE,
    CHAR_JOSEF,
    CHAR_JOSH,
    CHAR_MANUEL,
    CHAR_MARNIE,
    CHAR_MARY_ANN,
    CHAR_MAUDE,
    CHAR_MRS_THORNHILL,
    CHAR_NIGEL,
    CHAR_SASQUATCH,
    CHAR_ASHLEY,
    CHAR_ANDREAS,
    CHAR_DREYFUSS,
    CHAR_OMEGA,

    // Multiplayer Characters
    CHAR_MP_BIKER_BOSS,     // Biker Gang Boss 
    CHAR_MP_FAM_BOSS,       // Families Gang Boss
    CHAR_MP_MEX_BOSS,       // Mexican Gang Boss
    CHAR_MP_PROF_BOSS,      // Professionals Gang Boss
    
	//Was CHAR_MP_MEX_LT.  Now reusing for the female Personal Assistant, CHAR_MP_P_ASSISTF, for executive pack. See 2769151.
    CHAR_MP_P_ASSISTF,         // Mexican Gang Lieutenant -		 
    
    CHAR_MP_P_ASSIST, 		// Was CHAR_MP_BIKER_MECHANIC - Steve T  01.02.16 Now used as a contact for 2686174
   
    
    CHAR_MP_AGENT_14,      // Mexican Docks Contact: Jim Rivera - Definitely used.
    CHAR_MP_STRETCH,        // Families Car Dropoff -				MAYBE FREE  - Steve T  01.02.16 - A few hash extraction commands in fmmc_menus and a net comms debug but nothing else.
    
    CHAR_MP_SNITCH,         // Generic MP Snitch: C. Parkerr - 	POTENTIALLY FREE - Steve T  01.02.16 - Lots of CnC commented out work and a net comms debug but nothing else.
    

      
    CHAR_YACHT_CAPTAIN,     // This CHAR_ENUM was originally CHAR_MP_FIB_CONTACT but have renamed for 2568241. Now definitely used.
    CHAR_MP_ARMY_CONTACT,   // ARMY Contact - 				POTENTIALLY FREE - Steve T  01.02.16 - Couple of vague CnC references and a net comms debug but nothing else. Have mailed out.
    
    CHAR_BENNYS_OMW,        // This CHAR_ENUM was originally unused CHAR_MP_ROBERTO but have renamed for 2519183. Now Bennys Original Motorworks.
    CHAR_MP_RAY_LAVOY,      // Ray Lavoy - Reusing this character as the Agency Boss for TODO 2511934. CELL_AGBOSS is the name of the character that appears on the cellphone.
    
    CHAR_MP_FM_CONTACT,     // Freemode - Used during the intro to invite players to Freemode, and perhaps available for other uses too
    CHAR_MP_BRUCIE,         // Freemode - Brucie - a contact for one of the Freemode special abilities
    CHAR_MP_MERRYWEATHER,   // Freemode - Merryweather - a contact for one of the Freemode special abilities
    CHAR_MP_GERALD,         // Freemode - Gerald - a Contact Mission contact
    CHAR_MP_STRIPCLUB_PR,   // Freemode - The Strip Club PR contact, for promotional text messages, etc
    
    CHAR_MP_DETONATEPHONE,  // Used by MAINTAIN_BOMB_VEHICLES, see bug #968485 
    
    CHAR_MP_MECHANIC,   //Freemode - General mechanic used to delivery vehicles once player owns a garage
    CHAR_MP_JULIO,	//															POTENTIALLY FREE - Steve T 01.02.16 - One reference in fm_mission_info IF (paramContactFromCloud > (ENUM_TO_INT(CHAR_MP_JULIO)))
    																						//That would not be impacted by a renaming as it's an enum-to-int greater than comparison.
    CHAR_MP_MORS_MUTUAL,


    //Random Event characters
    CHAR_DOMESTIC_GIRL,
    CHAR_HITCHER_GIRL,
    CHAR_BROKEN_DOWN_GIRL,
    CHAR_ANTONIA,

    //Los Santos Customs
    CHAR_LS_CUSTOMS,
    
    //Ammunation
    CHAR_AMMUNATION,
    
    // Social Club
    CHAR_SOCIAL_CLUB,
    
    //LS Tourist Board
    CHAR_LS_TOURIST_BOARD,
    
    // Mechanic
    CHAR_MECHANIC,

    //Dial_A_Sub
    CHAR_DIAL_A_SUB,
    
    // Chop
    CHAR_CHOP,
    
    // Tonya, for Towing
    CHAR_TOW_TONYA,
    
    // Stripper booty calls
    CHAR_STRIPPER_JULIET,
    CHAR_STRIPPER_NIKKI,
    CHAR_STRIPPER_CHASTITY,
    CHAR_STRIPPER_CHEETAH,
    CHAR_STRIPPER_SAPPHIRE,
    CHAR_STRIPPER_INFERNUS,
    CHAR_STRIPPER_FUFU,
    CHAR_STRIPPER_PEACH,
    
    // Non stripper booty calls
    CHAR_TAXI_LIZ,

    // Property Management calls
    CHAR_PROPERTY_TAXI_LOT,
    CHAR_PROPERTY_CINEMA_VINEWOOD,
    CHAR_PROPERTY_CINEMA_DOWNTOWN,
    CHAR_PROPERTY_CINEMA_MORNINGWOOD,   
    CHAR_PROPERTY_CAR_SCRAP_YARD,
    CHAR_PROPERTY_WEED_SHOP,
    CHAR_PROPERTY_BAR_TEQUILALA,
    CHAR_PROPERTY_BAR_PITCHERS,
    CHAR_PROPERTY_BAR_HEN_HOUSE,
    CHAR_PROPERTY_BAR_HOOKIES,
    CHAR_PROPERTY_GOLF_CLUB,
    CHAR_PROPERTY_CAR_MOD_SHOP,
    CHAR_PROPERTY_TOWING_IMPOUND,
    CHAR_PROPERTY_ARMS_TRAFFICKING,
    CHAR_PROPERTY_SONAR_COLLECTIONS,
    
    //Special Contact
    CHAR_DETONATEBOMB,
    CHAR_DETONATEPHONE,
    CHAR_CALL911,
    CHAR_TAXI,
    CHAR_CHAT_CALL,
    CHAR_MONKEY_MAN,
    CHAR_ISAAC, // used for murder mystery
 

    //Dummy temps used for testing. We might be able to use this in the near future 21/10/2015
    CHAR_CASTRO,
    CHAR_ARTHUR,
    
    CHAR_BLOCKED,

    
    //Website sources for text messages in MP
    CHAR_CARSITE,           // Legendary Motorsport
    CHAR_BOATSITE,
    CHAR_MILSITE,
    CHAR_PLANESITE,
    CHAR_BIKESITE_PAMC,     // Pedal and Metal Cycles
    CHAR_CARSITE_SSASA,     // Southern SA Super Autos

    //Important! If you add any characters to this list, make sure you fill out an initial character sheet for the new addition in charsheet_initial.sc.
    CHAR_BLANK_ENTRY, 
    NO_CHARACTER,   //Note we can't move this now. It has to be the same as when we shipped the game. New characters should be added under this.
    
    CHAR_DIRECTOR,
	
	//Two new biker clubhouse contacts requested by TODO 2945259.
	CHAR_BIKER_CH1,
	CHAR_BIKER_CH2,
	
	//Maze Bank MP email contact 3021274 - not added to phone.
	CHAR_MAZE_MPEMAIL,
	
	//Treasure Map MP email contact - not added to phone.
	CHAR_MAPSENDER,
	
	//Nightclub Characters - See 4601562 for pic details.
	CHAR_NCLUBT,
	CHAR_NCLUBL,
	CHAR_NCLUBE,
	
	CHAR_BBPAIGE,
	
	CHAR_ARENA,
	
	CHAR_BRYONY,
	
	CHAR_CASINO_MANAGER,
	CHAR_CASINO_TOMCONNORS,
		
	CHAR_COMIC_STORE,
	
	//Casino MP email contact 5690249 - not added to phone.
	CHAR_CASINO_MPEMAIL,
	
	// Only characters before this are saved to sp
	MAX_CHARACTERS,	
	MAX_CHARACTERS_SP_SAVE,
	
	//========= MP ONLY ============ 
	
	CHAR_CASINO_TAO_TRANSLATOR,
	CHAR_ARCADE_WENDY,
	CHAR_ARCADE_CELEB,
	CHAR_ARCADE_MN, // Madame Nazar Easter Egg contact
	
	#IF FEATURE_HEIST_ISLAND
	CHAR_MIGUEL_MADRAZO,
	CHAR_KEINEMUSIK,
	CHAR_PAVEL,
	CHAR_STILL_SLIPPIN_RADIO,
	CHAR_MOODYMANN,
	#ENDIF
	
	#IF FEATURE_COPS_N_CROOKS
	
	CHAR_POLICE_DISPATCH,
	
	#ENDIF
	
	#IF FEATURE_TUNER
	CHAR_SESSANTA,
	CHAR_KDJ,
	//for email
	CHAR_LS_CAR_MEET_MPEMAIL, //Ls Car MEet MP email contact- not added to phone.
	#ENDIF
	
	#IF FEATURE_FIXER
	CHAR_FIXER_FRANKLIN,
	CHAR_FIXER_IMANI,
	CHAR_FIXER_FRANKLIN_IMANI_CONF,
	CHAR_FIXER_PRODUCER,
	#ENDIF
	
	#IF FEATURE_DLC_1_2022
	CHAR_JUNK_ENERGY,
	CHAR_MARCEL,
	CHAR_YOHAN,
	CHAR_ULP,
	CHAR_SINDY,
	CHAR_LUPE,
	CHAR_LUXURY_AUTOS_MPEMAIL,
	CHAR_PREMIUM_DELUXE_MPEMAIL,
	#ENDIF
	
	#IF FEATURE_DLC_2_2022
	CHAR_VAGOS,
	CHAR_DAX,
	CHAR_COOK,
	#ENDIF
    
	MAX_CHARACTERS_MP,	
    MAX_CHARACTERS_PLUS_DUMMY
	
	
	
//=========AGENT T============  
//  CHAR_MICHAEL,  //0
//  CHAR_FRANKLIN, //1
//  CHAR_TREVOR,   //2
//  CHAR_MULTIPLAYER, //3

    ,CHAR_CLF_RON =4
    ,CHAR_CLF_PAPERMAN
    ,CHAR_CLF_14
    ,CHAR_CLF_SIMON_FENCE
    ,CHAR_CLF_KAREN_DANIELS
    ,CHAR_CLF_WADE
    ,CHAR_CLF_MILES_GOLDSTEIN
    ,CHAR_CLF_VASILY_SILVERSTEIN
    ,CHAR_CLF_AFIF
    ,CHAR_CLF_ADIL
    ,CHAR_CLF_BASIL
    ,CHAR_CLF_DR_MINTON
    ,CHAR_CLF_JUNG_HO_SONG
    ,CHAR_CLF_CASINO_OWNER
    ,CHAR_CLF_LILLY
    ,CHAR_CLF_CLAIRE
    ,CHAR_CLF_AGNES
    ,CHAR_CLF_MELODY
    ,CHAR_CLF_ALEX
    ,CHAR_CLF_PAYPHONE
    ,CHAR_CLF_LS_CUSTOMS
    ,CHAR_CLF_AMMUNATION
    ,CHAR_CLF_CALL911
    ,CHAR_CLF_TAXI
    ,CHAR_CLF_BLOCKED
    ,CHAR_CLF_CHAT_CALL
    ,CHAR_CLF_BLANK_ENTRY
    
    ,MAX_CLF_CHARACTERS
    ,MAX_CLF_CHARACTERS_PLUS_DUMMY
//============================  
    
//==========ZOMBIE============  
//  CHAR_MICHAEL,       //0
//  CHAR_FRANKLIN,      //1
//  CHAR_TREVOR,        //2
//  CHAR_MULTIPLAYER,   //3
    ,CHAR_NRM_MICHAEL   = CHAR_MICHAEL
    ,CHAR_NRM_JIMMY     = CHAR_FRANKLIN
    ,CHAR_NRM_TRACEY    = CHAR_TREVOR
    ,CHAR_NRM_MULT      = CHAR_MULTIPLAYER
    
    ,CHAR_NRM_AMANDA
    ,CHAR_NRM_AMMUNATION
    ,CHAR_NRM_CALL911
    ,CHAR_NRM_TAXI
    ,CHAR_NRM_BLOCKED
    ,CHAR_NRM_CHAT_CALL
    ,CHAR_NRM_BLANK_ENTRY
    
    ,MAX_NRM_CHARACTERS
    ,MAX_NRM_CHARACTERS_PLUS_DUMMY
//============================

ENDENUM

FUNC STRING enumCharacterList_TO_STRING(enumCharacterList thisChar)

	SWITCH thisChar
		CASE CHAR_MICHAEL											RETURN "CHAR_MICHAEL"
		CASE CHAR_FRANKLIN                                          RETURN "CHAR_FRANKLIN"
		CASE CHAR_TREVOR                                            RETURN "CHAR_TREVOR"
		CASE CHAR_MULTIPLAYER                                       RETURN "CHAR_MULTIPLAYER"
		CASE CHAR_ALL_PLAYERS_CONF                                  RETURN "CHAR_ALL_PLAYERS_CONF"
		CASE CHAR_FRANK_TREV_CONF                                   RETURN "CHAR_FRANK_TREV_CONF"
		CASE CHAR_LEST_FRANK_CONF                                   RETURN "CHAR_LEST_FRANK_CONF"
		CASE CHAR_LEST_MIKE_CONF                                    RETURN "CHAR_LEST_MIKE_CONF"
		CASE CHAR_MIKE_FRANK_CONF                                   RETURN "CHAR_MIKE_FRANK_CONF"
		CASE CHAR_MIKE_TREV_CONF                                    RETURN "CHAR_MIKE_TREV_CONF"
		CASE CHAR_STEVE_MIKE_CONF                                   RETURN "CHAR_STEVE_MIKE_CONF"
		CASE CHAR_STEVE_TREV_CONF                                   RETURN "CHAR_STEVE_TREV_CONF"
		CASE CHAR_LESTER                                            RETURN "CHAR_LESTER"
		CASE CHAR_LESTER_DEATHWISH                                  RETURN "CHAR_LESTER_DEATHWISH"
		CASE CHAR_JIMMY                                             RETURN "CHAR_JIMMY"
		CASE CHAR_TRACEY                                            RETURN "CHAR_TRACEY"
		CASE CHAR_ABIGAIL                                           RETURN "CHAR_ABIGAIL"
		CASE CHAR_AMANDA                                            RETURN "CHAR_AMANDA"
		CASE CHAR_SIMEON                                            RETURN "CHAR_SIMEON"
		CASE CHAR_LAMAR                                             RETURN "CHAR_LAMAR"
		CASE CHAR_RON                                               RETURN "CHAR_RON"
		CASE CHAR_CHENG                                             RETURN "CHAR_CHENG"
		CASE CHAR_CEOASSIST                                         RETURN "CHAR_CEOASSIST"
		CASE CHAR_STEVE                                             RETURN "CHAR_STEVE"
		CASE CHAR_WADE                                              RETURN "CHAR_WADE"
		CASE CHAR_TENNIS_COACH                                      RETURN "CHAR_TENNIS_COACH"
		CASE CHAR_SOLOMON                                           RETURN "CHAR_SOLOMON"
		CASE CHAR_LAZLOW                                            RETURN "CHAR_LAZLOW"
		CASE CHAR_ESTATE_AGENT                                      RETURN "CHAR_ESTATE_AGENT"
		CASE CHAR_DEVIN                                             RETURN "CHAR_DEVIN"
		CASE CHAR_DAVE                                              RETURN "CHAR_DAVE"
		CASE CHAR_MARTIN                                            RETURN "CHAR_MARTIN"
		CASE CHAR_FLOYD                                             RETURN "CHAR_FLOYD"
		CASE CHAR_GAYMILITARY                                       RETURN "CHAR_GAYMILITARY"
		CASE CHAR_OSCAR                                             RETURN "CHAR_OSCAR"
		CASE CHAR_CHENGSR                                           RETURN "CHAR_CHENGSR"
		CASE CHAR_DR_FRIEDLANDER                                    RETURN "CHAR_DR_FRIEDLANDER"
		CASE CHAR_STRETCH                                           RETURN "CHAR_STRETCH"
		CASE CHAR_ORTEGA                                            RETURN "CHAR_ORTEGA"
		CASE CHAR_ONEIL                                             RETURN "CHAR_ONEIL"
		CASE CHAR_PATRICIA                                          RETURN "CHAR_PATRICIA"
		CASE CHAR_PEGASUS_DELIVERY                                  RETURN "CHAR_PEGASUS_DELIVERY"
		CASE CHAR_LIFEINVADER                                       RETURN "CHAR_LIFEINVADER"
		CASE CHAR_TANISHA                                           RETURN "CHAR_TANISHA"
		CASE CHAR_DENISE                                            RETURN "CHAR_DENISE"
		CASE CHAR_MOLLY                                             RETURN "CHAR_MOLLY"
		CASE CHAR_RICKIE                                            RETURN "CHAR_RICKIE"
		CASE CHAR_CHEF                                              RETURN "CHAR_CHEF"
		CASE CHAR_BLIMP                                             RETURN "CHAR_BLIMP"
		CASE CHAR_BARRY                                             RETURN "CHAR_BARRY"
		CASE CHAR_BEVERLY                                           RETURN "CHAR_BEVERLY"
		CASE CHAR_CRIS                                              RETURN "CHAR_CRIS"
		CASE CHAR_DOM                                               RETURN "CHAR_DOM"
		CASE CHAR_HAO                                               RETURN "CHAR_HAO"
		CASE CHAR_HUNTER                                            RETURN "CHAR_HUNTER"
		CASE CHAR_JIMMY_BOSTON                                      RETURN "CHAR_JIMMY_BOSTON"
		CASE CHAR_JOE                                               RETURN "CHAR_JOE"
		CASE CHAR_JOSEF                                             RETURN "CHAR_JOSEF"
		CASE CHAR_JOSH                                              RETURN "CHAR_JOSH"
		CASE CHAR_MANUEL                                            RETURN "CHAR_MANUEL"
		CASE CHAR_MARNIE                                            RETURN "CHAR_MARNIE"
		CASE CHAR_MARY_ANN                                          RETURN "CHAR_MARY_ANN"
		CASE CHAR_MAUDE                                             RETURN "CHAR_MAUDE"
		CASE CHAR_MRS_THORNHILL                                     RETURN "CHAR_MRS_THORNHILL"
		CASE CHAR_NIGEL                                             RETURN "CHAR_NIGEL"
		CASE CHAR_SASQUATCH                                         RETURN "CHAR_SASQUATCH"
		CASE CHAR_ASHLEY                                            RETURN "CHAR_ASHLEY"
		CASE CHAR_ANDREAS                                           RETURN "CHAR_ANDREAS"
		CASE CHAR_DREYFUSS                                          RETURN "CHAR_DREYFUSS"
		CASE CHAR_OMEGA                                             RETURN "CHAR_OMEGA"
		CASE CHAR_MP_BIKER_BOSS                                     RETURN "CHAR_MP_BIKER_BOSS"
		CASE CHAR_MP_FAM_BOSS                                       RETURN "CHAR_MP_FAM_BOSS"
		CASE CHAR_MP_MEX_BOSS                                       RETURN "CHAR_MP_MEX_BOSS"
		CASE CHAR_MP_PROF_BOSS                                      RETURN "CHAR_MP_PROF_BOSS"
		CASE CHAR_MP_P_ASSISTF                                      RETURN "CHAR_MP_P_ASSISTF"
		CASE CHAR_MP_P_ASSIST                                       RETURN "CHAR_MP_P_ASSIST"
		CASE CHAR_MP_AGENT_14                                       RETURN "CHAR_MP_AGENT_14"
		CASE CHAR_MP_STRETCH                                        RETURN "CHAR_MP_STRETCH"
		CASE CHAR_MP_SNITCH                                         RETURN "CHAR_MP_SNITCH"
		CASE CHAR_YACHT_CAPTAIN                                     RETURN "CHAR_YACHT_CAPTAIN"
		CASE CHAR_MP_ARMY_CONTACT                                   RETURN "CHAR_MP_ARMY_CONTACT"
		CASE CHAR_BENNYS_OMW                                        RETURN "CHAR_BENNYS_OMW"
		CASE CHAR_MP_RAY_LAVOY                                      RETURN "CHAR_MP_RAY_LAVOY"
		CASE CHAR_MP_FM_CONTACT                                     RETURN "CHAR_MP_FM_CONTACT"
		CASE CHAR_MP_BRUCIE                                         RETURN "CHAR_MP_BRUCIE"
		CASE CHAR_MP_MERRYWEATHER                                   RETURN "CHAR_MP_MERRYWEATHER"
		CASE CHAR_MP_GERALD                                         RETURN "CHAR_MP_GERALD"
		CASE CHAR_MP_STRIPCLUB_PR                                   RETURN "CHAR_MP_STRIPCLUB_PR"
		CASE CHAR_MP_DETONATEPHONE                                  RETURN "CHAR_MP_DETONATEPHONE"
		CASE CHAR_MP_MECHANIC                                       RETURN "CHAR_MP_MECHANIC"
		CASE CHAR_MP_JULIO                                          RETURN "CHAR_MP_JULIO"
		CASE CHAR_MP_MORS_MUTUAL                                    RETURN "CHAR_MP_MORS_MUTUAL"
	ENDSWITCH
	
	SWITCH thisChar
		CASE CHAR_DOMESTIC_GIRL                                     RETURN "CHAR_DOMESTIC_GIRL"
		CASE CHAR_HITCHER_GIRL                                      RETURN "CHAR_HITCHER_GIRL"
		CASE CHAR_BROKEN_DOWN_GIRL                                  RETURN "CHAR_BROKEN_DOWN_GIRL"
		CASE CHAR_ANTONIA                                           RETURN "CHAR_ANTONIA"
		CASE CHAR_LS_CUSTOMS                                        RETURN "CHAR_LS_CUSTOMS"
		CASE CHAR_AMMUNATION                                        RETURN "CHAR_AMMUNATION"
		CASE CHAR_SOCIAL_CLUB                                       RETURN "CHAR_SOCIAL_CLUB"
		CASE CHAR_LS_TOURIST_BOARD                                  RETURN "CHAR_LS_TOURIST_BOARD"
		CASE CHAR_MECHANIC                                          RETURN "CHAR_MECHANIC"
		CASE CHAR_DIAL_A_SUB                                        RETURN "CHAR_DIAL_A_SUB"
		CASE CHAR_CHOP                                              RETURN "CHAR_CHOP"
		CASE CHAR_TOW_TONYA                                         RETURN "CHAR_TOW_TONYA"
		CASE CHAR_STRIPPER_JULIET                                   RETURN "CHAR_STRIPPER_JULIET"
		CASE CHAR_STRIPPER_NIKKI                                    RETURN "CHAR_STRIPPER_NIKKI"
		CASE CHAR_STRIPPER_CHASTITY                                 RETURN "CHAR_STRIPPER_CHASTITY"
		CASE CHAR_STRIPPER_CHEETAH                                  RETURN "CHAR_STRIPPER_CHEETAH"
		CASE CHAR_STRIPPER_SAPPHIRE                                 RETURN "CHAR_STRIPPER_SAPPHIRE"
		CASE CHAR_STRIPPER_INFERNUS                                 RETURN "CHAR_STRIPPER_INFERNUS"
		CASE CHAR_STRIPPER_FUFU                                     RETURN "CHAR_STRIPPER_FUFU"
		CASE CHAR_STRIPPER_PEACH                                    RETURN "CHAR_STRIPPER_PEACH"
		CASE CHAR_TAXI_LIZ                                          RETURN "CHAR_TAXI_LIZ"
		CASE CHAR_PROPERTY_TAXI_LOT                                 RETURN "CHAR_PROPERTY_TAXI_LOT"
		CASE CHAR_PROPERTY_CINEMA_VINEWOOD                          RETURN "CHAR_PROPERTY_CINEMA_VINEWOOD"
		CASE CHAR_PROPERTY_CINEMA_DOWNTOWN                          RETURN "CHAR_PROPERTY_CINEMA_DOWNTOWN"
		CASE CHAR_PROPERTY_CINEMA_MORNINGWOOD                       RETURN "CHAR_PROPERTY_CINEMA_MORNINGWOOD"
		CASE CHAR_PROPERTY_CAR_SCRAP_YARD                           RETURN "CHAR_PROPERTY_CAR_SCRAP_YARD"
		CASE CHAR_PROPERTY_WEED_SHOP                                RETURN "CHAR_PROPERTY_WEED_SHOP"
		CASE CHAR_PROPERTY_BAR_TEQUILALA                            RETURN "CHAR_PROPERTY_BAR_TEQUILALA"
		CASE CHAR_PROPERTY_BAR_PITCHERS                             RETURN "CHAR_PROPERTY_BAR_PITCHERS"
		CASE CHAR_PROPERTY_BAR_HEN_HOUSE                            RETURN "CHAR_PROPERTY_BAR_HEN_HOUSE"
		CASE CHAR_PROPERTY_BAR_HOOKIES                              RETURN "CHAR_PROPERTY_BAR_HOOKIES"
		CASE CHAR_PROPERTY_GOLF_CLUB                                RETURN "CHAR_PROPERTY_GOLF_CLUB"
		CASE CHAR_PROPERTY_CAR_MOD_SHOP                             RETURN "CHAR_PROPERTY_CAR_MOD_SHOP"
		CASE CHAR_PROPERTY_TOWING_IMPOUND                           RETURN "CHAR_PROPERTY_TOWING_IMPOUND"
		CASE CHAR_PROPERTY_ARMS_TRAFFICKING                         RETURN "CHAR_PROPERTY_ARMS_TRAFFICKING"
		CASE CHAR_PROPERTY_SONAR_COLLECTIONS                        RETURN "CHAR_PROPERTY_SONAR_COLLECTIONS"
		CASE CHAR_DETONATEBOMB                                      RETURN "CHAR_DETONATEBOMB"
		CASE CHAR_DETONATEPHONE                                     RETURN "CHAR_DETONATEPHONE"
		CASE CHAR_CALL911                                           RETURN "CHAR_CALL911"
		CASE CHAR_TAXI                                              RETURN "CHAR_TAXI"
		CASE CHAR_CHAT_CALL                                         RETURN "CHAR_CHAT_CALL"
		CASE CHAR_MONKEY_MAN                                        RETURN "CHAR_MONKEY_MAN"
		CASE CHAR_ISAAC                                             RETURN "CHAR_ISAAC"
		CASE CHAR_CASTRO                                            RETURN "CHAR_CASTRO"
		CASE CHAR_ARTHUR                                            RETURN "CHAR_ARTHUR"
		CASE CHAR_BLOCKED                                           RETURN "CHAR_BLOCKED"
		CASE CHAR_CARSITE                                           RETURN "CHAR_CARSITE"
		CASE CHAR_BOATSITE                                          RETURN "CHAR_BOATSITE"
		CASE CHAR_MILSITE                                           RETURN "CHAR_MILSITE"
		CASE CHAR_PLANESITE                                         RETURN "CHAR_PLANESITE"
		CASE CHAR_BIKESITE_PAMC                                     RETURN "CHAR_BIKESITE_PAMC"
		CASE CHAR_CARSITE_SSASA                                     RETURN "CHAR_CARSITE_SSASA"
		CASE CHAR_BLANK_ENTRY                                       RETURN "CHAR_BLANK_ENTRY"
		CASE NO_CHARACTER                                           RETURN "NO_CHARACTER"
		CASE CHAR_DIRECTOR                                          RETURN "CHAR_DIRECTOR"
		CASE CHAR_BIKER_CH1                                         RETURN "CHAR_BIKER_CH1"
		CASE CHAR_BIKER_CH2                                         RETURN "CHAR_BIKER_CH2"
		CASE CHAR_MAZE_MPEMAIL                                      RETURN "CHAR_MAZE_MPEMAIL"
		CASE CHAR_MAPSENDER                                         RETURN "CHAR_MAPSENDER"
		CASE CHAR_NCLUBT                                            RETURN "CHAR_NCLUBT"
		CASE CHAR_NCLUBL                                            RETURN "CHAR_NCLUBL"
		CASE CHAR_NCLUBE                                            RETURN "CHAR_NCLUBE"
		CASE CHAR_BBPAIGE                                           RETURN "CHAR_BBPAIGE"
		CASE CHAR_ARENA                                             RETURN "CHAR_ARENA"
		CASE CHAR_BRYONY                                            RETURN "CHAR_BRYONY"
		CASE CHAR_CASINO_MANAGER                                    RETURN "CHAR_CASINO_MANAGER"
		CASE CHAR_CASINO_TOMCONNORS                                 RETURN "CHAR_CASINO_TOMCONNORS"
		CASE CHAR_COMIC_STORE                                       RETURN "CHAR_COMIC_STORE"
		CASE CHAR_CASINO_MPEMAIL                                    RETURN "CHAR_CASINO_MPEMAIL"
		CASE MAX_CHARACTERS	                                        RETURN "MAX_CHARACTERS"
		CASE MAX_CHARACTERS_SP_SAVE                                 RETURN "MAX_CHARACTERS_SP_SAVE"
		CASE CHAR_CASINO_TAO_TRANSLATOR                             RETURN "CHAR_CASINO_TAO_TRANSLATOR"
		CASE CHAR_ARCADE_WENDY                                      RETURN "CHAR_ARCADE_WENDY"
		CASE CHAR_ARCADE_CELEB                                      RETURN "CHAR_ARCADE_CELEB"
		CASE CHAR_ARCADE_MN                                         RETURN "CHAR_ARCADE_MN"
		CASE CHAR_MIGUEL_MADRAZO                                    RETURN "CHAR_MIGUEL_MADRAZO"
		CASE CHAR_KEINEMUSIK                                        RETURN "CHAR_KEINEMUSIK"
		CASE CHAR_PAVEL                                             RETURN "CHAR_PAVEL"
		CASE CHAR_STILL_SLIPPIN_RADIO                               RETURN "CHAR_STILL_SLIPPIN_RADIO"
		CASE CHAR_MOODYMANN                                         RETURN "CHAR_MOODYMANN"
		
	ENDSWITCH
	
	#IF FEATURE_COPS_N_CROOKS
	SWITCH thisChar
		CASE CHAR_POLICE_DISPATCH                                   RETURN "CHAR_POLICE_DISPATCH"
	ENDSWITCH
	#ENDIF
	
	SWITCH thisChar
		CASE CHAR_SESSANTA                                          RETURN "CHAR_SESSANTA"
		CASE CHAR_KDJ                                               RETURN "CHAR_KDJ"
		CASE CHAR_LS_CAR_MEET_MPEMAIL                               RETURN "CHAR_LS_CAR_MEET_MPEMAIL"
		CASE CHAR_FIXER_FRANKLIN                                    RETURN "CHAR_FIXER_FRANKLIN"
		CASE CHAR_FIXER_IMANI                                       RETURN "CHAR_FIXER_IMANI"
		CASE CHAR_FIXER_FRANKLIN_IMANI_CONF                         RETURN "CHAR_FIXER_FRANKLIN_IMANI_CONF"
		CASE CHAR_FIXER_PRODUCER                                    RETURN "CHAR_FIXER_PRODUCER"
		CASE CHAR_JUNK_ENERGY                                       RETURN "CHAR_JUNK_ENERGY"
		CASE CHAR_MARCEL                                            RETURN "CHAR_MARCEL"
		CASE CHAR_YOHAN                                             RETURN "CHAR_YOHAN"
		CASE CHAR_ULP                                               RETURN "CHAR_ULP"
		CASE CHAR_SINDY                                             RETURN "CHAR_SINDY"
		CASE CHAR_LUPE                                              RETURN "CHAR_LUPE"
		CASE CHAR_LUXURY_AUTOS_MPEMAIL                              RETURN "CHAR_LUXURY_AUTOS_MPEMAIL"
		CASE CHAR_PREMIUM_DELUXE_MPEMAIL                            RETURN "CHAR_PREMIUM_DELUXE_MPEMAIL"
		
		#IF FEATURE_DLC_2_2022
		CASE CHAR_VAGOS												RETURN "CHAR_VAGOS"
		CASE CHAR_DAX												RETURN "CHAR_DAX"
		CASE CHAR_COOK												RETURN "CHAR_COOK"
		#ENDIF
		
		CASE MAX_CHARACTERS_MP	                                    RETURN "MAX_CHARACTERS_MP"
		CASE MAX_CHARACTERS_PLUS_DUMMY                              RETURN "MAX_CHARACTERS_PLUS_DUMMY"
	ENDSWITCH

	RETURN ""
ENDFUNC

ENUM enumPhoneContact

    PC_MICHAEL,
    PC_FRANKLIN,
    PC_TREVOR,
   
    PC_ALL_PLAYERS,
    PC_FRANK_TREV_CONF,

    PC_LEST_FRANK_CONF,
    PC_LEST_MIKE_CONF,

    PC_MIKE_FRANK_CONF,
    PC_MIKE_TREV_CONF,
    PC_STEVE_MIKE_CONF,
    PC_STEVE_TREV_CONF,

    //Story
    PC_LESTER,
    PC_JIMMY,
    PC_TRACEY,
    PC_ABIGAIL,
    PC_AMANDA,
    PC_SIMEON,
    PC_LAMAR,
    PC_RON,
    PC_CHENG,
    PC_SAEEDA,
    PC_STEVE,
    PC_WADE,
    PC_TENNIS_COACH,
    PC_SOLOMON,
    PC_LAZLOW,
    PC_ESTATE_AGENT,
    PC_DEVIN,       // CarSteal Financier
    PC_DAVE,
    PC_MARTIN,
    PC_FLOYD,
    PC_GAYMILITARY,
    PC_OSCAR,
    PC_CHENGSR,
    PC_DR_FRIEDLANDER,
    PC_STRETCH,
    PC_ORTEGA,
    PC_ONEIL,
    PC_PATRICIA,
    PC_PEGASUS_DELIVERY,
    PC_LIFEINVADER,
    PC_TANISHA,
    PC_DENISE,
    PC_MOLLY,
    PC_RICKIE,
    PC_CHEF,
    
    PC_BLIMP,

    //Random Characters
    PC_BARRY,
    PC_BEVERLY,
    PC_CRIS,
    PC_DOM,
    PC_HAO,
    PC_HUNTER,
    PC_JIMMY_BOSTON,
    PC_JOE,
    PC_JOSEF,
    PC_JOSH,
    PC_MANUEL,
    PC_MARNIE,
    PC_MARY_ANN,
    PC_MAUDE,
    PC_MRS_THORNHILL,
    PC_NIGEL,
    PC_SASQUATCH,
    PC_ASHLEY,
    PC_OMEGA,

    //Multiplayer
    PC_MP_BIKERBOSS,
    PC_MP_COPBOSS,
    PC_MP_FAMBOSS,
    PC_MP_MEXBOSS,
    PC_MP_PROFBOSS,
    PC_MP_MEXLT,
    PC_MP_STRETCH,
    PC_MP_BIKERMECHANIC,
    PC_MP_AGENT_14,
    PC_MP_SNITCH,
    PC_MP_FIB_CONTACT,
    PC_MP_ARMY_CONTACT,
    PC_MP_ROBERTO,
    PC_MP_RAY_LAVOY,
    PC_MP_FM_CONTACT,
    PC_MP_BRUCIE,
    PC_MP_MERRYWEATHER,
    PC_MP_PROSTITUTE,
    PC_MP_STRIPCLUB_PR,
    PC_MP_MORS_MUTUAL,


    PC_GUN1,
    PC_GUN2,
    PC_GUN3,

    PC_IT1,
    PC_IT2,

    //Random Event characters
    PC_DOMESTIC_GIRL,
    PC_HITCHER_GIRL,
    PC_BROKEN_DOWN_GIRL,
    PC_BURIAL_ANTONIA,

    //Los Santos Customs
    PC_LS_CUSTOMS,
    
    //Ammunation
    PC_AMMUNATION,

    //Dial_A_Sub
    PC_DIAL_A_SUB,

    //Chop
    PC_CHOP,

    // Strip club booty calls
    PC_STRIPPER1,
    PC_STRIPPER2,
    PC_STRIPPER3,
    PC_STRIPPER4,
    PC_STRIPPER5,
    PC_STRIPPER6,
    PC_STRIPPER7,
    PC_STRIPPER8,
    
    // Non stripper booty calls
    PC_TAXI_LIZ,
    
    // Property Management
    PC_PROPERTY_TAXI_LOT,
    PC_PROPERTY_CINEMA_VINEWOOD,
    PC_PROPERTY_CINEMA_DOWNTOWN,
    PC_PROPERTY_CINEMA_MORNINGWOOD, 
    PC_PROPERTY_CAR_SCRAP_YARD,
    PC_PROPERTY_PLANE_SCRAP_YARD,
    PC_PROPERTY_WEED_SHOP,
    PC_PROPERTY_BAR_TEQUILALA,
    PC_PROPERTY_BAR_PITCHERS,
    PC_PROPERTY_BAR_HEN_HOUSE,
    PC_PROPERTY_BAR_HOOKIES,
    PC_PROPERTY_GOLF_CLUB,
    PC_PROPERTY_CAR_MOD_SHOP,
    PC_PROPERTY_TOWING_IMPOUND,
    PC_PROPERTY_ARMS_TRAFFICKING,
    PC_PROPERTY_SONAR_COLLECTIONS,
    
    // Towing ped
    PC_TOW_TONYA,


    PC_DETONATEBOMB,
    PC_DETONATEPHONE,
    PC_CALL911,
    PC_TAXI,
  
    PC_CASTRO,
    PC_ARTHUR,
    
    PC_BLOCKED,

    PC_CHAT_CALL,
    
    PC_LS_TOURIST,
    

    PC_CHAR_BLANK_ENTRY,
    
    PC_NOIR,
    
    //Director Mode LS Talent
    PC_DIRECTOR,
	
	//Two new biker clubhouse contacts requested by TODO 2945259.
	PC_BIKER_CH1,
	PC_BIKER_CH2,
	
	
	//Nightclub Characters - See 4601562 for details.
	PC_NCLUBT,
	PC_NCLUBL,
	PC_NCLUBE,
	
	PC_BBPAIGE,
	
	PC_BRYONY,
	
	PC_CASINO_MANAGER,
	
	PC_HARDCORE_COMIC_STORE,
	
	// Casino heist
	PC_ARCADE_WENDY,
	PC_ARCADE_TW,
	PC_ARCADE_MN,
	
	PC_ISLAND_MIGUEL,
	PC_KEINEMUSIK,
	PC_STILL_SLIPPIN_RADIO,
	PC_MOODYMANN,
	#IF FEATURE_TUNER
	PC_SESSANTA,
	PC_KDJ,
	#ENDIF
	#IF FEATURE_FIXER
	PC_FIXER_FRANKLIN,
	PC_FIXER_IMANI,
	PC_FIXER_DRE,
	PC_FIXER_FRAKLIN_IMANI_CONF,
	#ENDIF
	#IF FEATURE_DLC_1_2022
	PC_JUNK_ENERGY,
	PC_MARCEL,
	PC_YOHAN,
	PC_ULP,
	PC_SINDY,
	PC_LUPE,
	#ENDIF
	#IF FEATURE_DLC_2_2022
	PC_VAGOS,
	PC_DAX,
	PC_COOK,
	#ENDIF
	MAX_PHONE_CONTACTS,
    NO_PHONE_CONTACT
    
    //=========AGENT T============  
//  CHAR_MICHAEL,  //0
//  CHAR_FRANKLIN, //1
//  CHAR_TREVOR,   //2
//  CHAR_MULTIPLAYER, //3

    ,PC_CLF_RON =4
    ,PC_CLF_PAPERMAN
    ,PC_CLF_14
    ,PC_CLF_SIMON_FENCE
    ,PC_CLF_KAREN_DANIELS
    ,PC_CLF_WADE
    ,PC_CLF_MILES_GOLDSTEIN
    ,PC_CLF_VASILY_SILVERSTEIN
    ,PC_CLF_AFIF
    ,PC_CLF_ADIL
    ,PC_CLF_BASIL
    ,PC_CLF_DR_MINTON
    ,PC_CLF_JUNG_HO_SONG
    ,PC_CLF_CASINO_OWNER
    ,PC_CLF_LILLY
    ,PC_CLF_CLAIRE
    ,PC_CLF_AGNES
    ,PC_CLF_MELODY
    ,PC_CLF_ALEX
    ,PC_CLF_PAYPHONE
    
    ,PC_CLF_LS_CUSTOMS
    ,PC_CLF_AMMUNATION
    ,PC_CLF_CALL911
    ,PC_CLF_TAXI
    ,PC_CLF_BLOCKED
    ,PC_CLF_CHAT_CALL
    ,PC_CLF_BLANK_ENTRY
    
    ,MAX_CLF_PHONE_CONTACTS
//============================  
    
//==========ZOMBIE============  
//  CHAR_MICHAEL,       //0
//  CHAR_FRANKLIN,      //1
//  CHAR_TREVOR,        //2
//  CHAR_MULTIPLAYER,   //3

    ,PC_NRM_JIMMY  = PC_FRANKLIN
    ,PC_NRM_TRACEY = PC_TREVOR
    ,PC_NRM_AMANDA
    ,PC_NRM_AMMUNATION
    ,PC_NRM_CALL911
    ,PC_NRM_TAXI
    ,PC_NRM_BLOCKED
    ,PC_NRM_CHAT_CALL
    ,PC_NRM_BLANK_ENTRY
    
    ,MAX_NRM_PHONE_CONTACTS
//============================

ENDENUM



ENUM enumEmailContact

    EMAIL_MICHAEL,
    EMAIL_FRANKLIN,
    EMAIL_TREVOR,
    
    MAX_EMAIL_CONTACTS,
    NO_EMAIL_CONTACT

ENDENUM



ENUM enumFriend

    FR_MICHAEL,
    FR_FRANKLIN,
    FR_TREVOR,
  
    FR_LAMAR,
    FR_JIMMY,
    FR_AMANDA,
    
    MAX_FRIENDS,
    NO_FRIEND

ENDENUM
CONST_INT NUM_OF_NPC_FRIENDS   3  // MAX_FRIENDS - NUM_OF_PLAYABLE_PEDS: Lamar, Jimmy, Amanda



ENUM enumFamilyMember

    FM_MICHAEL_SON,
    FM_MICHAEL_DAUGHTER,
    FM_MICHAEL_WIFE,
    FM_MICHAEL_MEXMAID,
    FM_MICHAEL_GARDENER,
    
    FM_FRANKLIN_AUNT,
    FM_FRANKLIN_LAMAR,
    FM_FRANKLIN_STRETCH,
//  FM_FRANKLIN_DOG,
    
    FM_TREVOR_0_MICHAEL,
    FM_TREVOR_0_TREVOR,
    FM_TREVOR_0_RON,
    FM_TREVOR_0_WIFE,
    FM_TREVOR_0_MOTHER,
    
    FM_TREVOR_1_FLOYD,
    FM_TREVOR_1_WADE,
    
    
    MAX_FAMILY_MEMBER,
    NO_FAMILY_MEMBER

ENDENUM



ENUM enumPhoneBookPresence

   
    MICHAEL_BOOK,  //0
    FRANKLIN_BOOK, //1
    TREVOR_BOOK,   //2

    MULTIPLAYER_BOOK, //3

  
    ALL_OWNERS_BOOKS

 
ENDENUM







ENUM enumIndividualPhoneBookState

    NOT_LISTED,
    LISTED,
    SPECIAL_NUMBERS //e.g 911. We want it to be a recognised number when called/dialled but not appear in the contacts list.

ENDENUM






ENUM enumBankAccountName

    BANK_ACCOUNT_MICHAEL,
    BANK_ACCOUNT_FRANKLIN,
    BANK_ACCOUNT_TREVOR,
    
    MAX_ACCOUNTS,
    
    NO_ACCOUNT

ENDENUM







ENUM enumPicMsgStatus
    
    NO_PICMSG_STORED,
    HAS_PICMSG_STORED

ENDENUM




ENUM enumMissedCallStatus
    
    NO_MISSED_CALL,
    MISSED_CALL

ENDENUM



ENUM enumStatusAsCaller //If the caller is unknown, then their details will display as their designated phone number rather than their contact name.
    
    UNKNOWN_CALLER, //0
    KNOWN_CALLER

ENDENUM



CONST_INT CHARACTER_SHEET_MAX_PLAYER_ID 4

// Character Sheet structure
// Includes alphabetical order int here if better way can't be sussed out via text labels...
// This may need extra fields for presence of char in multiple phone books. Should be saved so ADD_CONTACT works fine?
STRUCT structCharacterSheet
    
    MODEL_NAMES         game_model          //actual model
    INT                 alphaint            //int used for alphabetical order. This can be altered on the fly, so that a priority contact can be stipulated.
    INT                 original_alphaint   //int used to keep a copy of the original alpha int for all characters so they can be reordered after a priority contact routine.
    TEXT_LABEL          label               //text label of character name as it would be displayed on the phone.
    
    //Removed by Steve T. 14/11/12 in a effort to reduce global usage.
    //TEXT_LABEL          role                //text label that provides additional info on this character's role in relation to the player. Friend, girlfriend, hired gun etc.
    
    
    
    TEXT_LABEL          picture             //text label reference to the string which refers to the character's pic for display in the contact and calling list.


    enumFriend          friend              //is this char a friend?
    
    //Removed by Steve T. 14/11/12 in a effort to reduce global usage.
    //enumFamilyMember    familyMember        //is this char a family member of a player char?
    


    
    enumIndividualPhoneBookState PhoneBookState[CHARACTER_SHEET_MAX_PLAYER_ID] //in which characters phonebook should this contact appear?  0 - Michael, 1 - Franklin, 2 - Trevor, 3 - Multiplayer
    
    enumBankAccountName bank_account

    enumPicMsgStatus    picmsgStatus

    enumMissedCallStatus missedCallStatus[CHARACTER_SHEET_MAX_PLAYER_ID] //one each for Michael, Franklin, Trevor and Multiplayer

    enumStatusAsCaller  statusAsCaller[CHARACTER_SHEET_MAX_PLAYER_ID] //again, we need one for each relation to Michael, Franklin or Trevor and Multiplayer

    
    //Removed by Steve T. 12/11/12 in a effort to reduce global usage.
    //enumActivityStatus  ActivityStatus[4]
    
    
ENDSTRUCT


// Instance of character sheet
// This will need to be saved globals.

//structCharacterSheet g_CharacterSheet[MAX_CHARACTERS_PLUS_DUMMY] 

STRUCT g_CharSheetSavedData
    structCharacterSheet g_CharacterSheet[MAX_CHARACTERS_SP_SAVE] 
ENDSTRUCT

STRUCT structCharacterSheetNonSaved

    
        TEXT_LABEL          ansphone_labelroot  //the text label root for the contact's answerphone. Pass in dummy NO_ANSWERPHONE value for any contact that should not have 
                                                //an answerphone message. Contact's with no answerphone message should default to engaged tone after a specified duration.

        TEXT_LABEL          phonebookNumberLabel

        enumPhoneContact    phone               //is this char a phone contact?
    

        enumEmailContact    email               //is this char an email contact?


ENDSTRUCT



//_______________________________________________________________________________________________________________________________________________________________________________________
//
//                                              Gameworld Phone Numbers - Numbers that will play an answerphone message when dialled from the cellphone.
//_______________________________________________________________________________________________________________________________________________________________________________________



ENUM enumGameWorldNumbers

    GW_Test0,
    GW_CHEAT1,
    GW_CHEAT2,
    GW_CHEAT3,
    GW_CHEAT4,
    GW_CHEAT5,
    GW_CHEAT6,
    GW_CHEAT7,
    GW_CHEAT8,
    GW_CHEAT9,
    GW_CHEAT10,
    GW_CHEAT11,
    GW_CHEAT12,
    GW_CHEAT13,
    GW_CHEAT14,
    GW_CHEAT15,
    GW_CHEAT16,
    GW_CHEAT17,
    GW_CHEAT18,
    GW_CHEAT19,
    GW_CHEAT20,
    GW_CHEAT21,
    GW_CHEAT22,
    GW_CHEAT23,
    GW_CHEAT24,
    GW_CHEAT25,
    GW_CHEAT26,
    GW_CHEAT27,
    GW_CHEAT28,
    GW_CHEAT29,
    GW_CHEAT30,
    GW_CHEAT31,
    GW_CHEAT32,
    GW_CHEAT33,
    GW_CHEAT34,
    GW_CHEAT35,
    GW_CHEAT36,

    MAX_GAMEWORLD_NUMBERS

ENDENUM




STRUCT struct_GameworldNumbers

    TEXT_LABEL      Gameworld_DialledNumberLabel
    TEXT_LABEL      Gameworld_AnsphoneLabelroot
    TEXT_LABEL_23   Gameworld_VoiceID

ENDSTRUCT





//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                    Special character and secondary functions for SP / MP Contacts.
//
//________________________________________________________________________________________________________________________________________________________________________________


CONST_INT MAX_SPECIAL_SP_CHARACTERS 4 //Required for Michael, Franklin, Trevor and Lester.

STRUCT  structSpecialSPCharacter

    TEXT_LABEL_31 Name_TextLabel
    TEXT_LABEL_31 SecondaryFunctionLabel_1
 
ENDSTRUCT


CONST_INT MAX_SPECIAL_MP_CHARACTERS 4


STRUCT  structSpecialMPCharacter

    TEXT_LABEL_31 Name_TextLabel
    TEXT_LABEL_31 SecondaryFunctionLabel_1
    TEXT_LABEL_31 SecondaryFunctionLabel_2

ENDSTRUCT
