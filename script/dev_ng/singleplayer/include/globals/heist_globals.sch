//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 27/01/10			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						     Heist Global Variables								│
//│																				│
//│			Contains all variable data that needs to be accessible to			│
//│			multiple heist scripts. Includes the saved data struct for 			│
//│ 		heists.																│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "types.sch"
USING "model_enums.sch"
USING "weapon_enums.sch"


//═══════════════════════════════════════════════════════════════════════════════
//══════════════════════════╡ Data not to be saved ╞═════════════════════════════
//═══════════════════════════════════════════════════════════════════════════════


//════════════════════════════╡ Heist Indexes ╞═══════════════════════════════
CONST_INT	NO_HEISTS			5

CONST_INT	HEIST_JEWEL			0
CONST_INT	HEIST_DOCKS			1
CONST_INT	HEIST_RURAL_BANK	2
CONST_INT	HEIST_AGENCY		3
CONST_INT	HEIST_FINALE		4


//═════════════════════╡ Heist Mission Choice Globals ╞═══════════════════════

//General choice values.
CONST_INT	HEIST_CHOICE_EMPTY				0

//Jewelry Heist choices.
CONST_INT	HEIST_CHOICE_JEWEL_STEALTH		1
CONST_INT	HEIST_CHOICE_JEWEL_HIGH_IMPACT	2

//Docks Heist choices.
CONST_INT	HEIST_CHOICE_DOCKS_BLOW_UP_BOAT	3
CONST_INT	HEIST_CHOICE_DOCKS_DEEP_SEA		4

//Rural Bank Heist choices.
CONST_INT	HEIST_CHOICE_RURAL_NO_TANK		5

//Agency Heist choices.
CONST_INT	HEIST_CHOICE_AGENCY_FIRETRUCK	6
CONST_INT	HEIST_CHOICE_AGENCY_HELICOPTER	7

//Finale Heist choices.
CONST_INT	HEIST_CHOICE_FINALE_TRAFFCONT	8
CONST_INT	HEIST_CHOICE_FINALE_HELI		9

CONST_INT	MAX_HEIST_CHOICES				10


//════════════════════════╡ Planning Board Enums ╞═══════════════════════════

//Interactions.
ENUM g_eBoardInteractions
	INTERACT_POI_OVERVIEW,
	INTERACT_CREW,
	INTERACT_GAMEPLAY_CHOICE,
	
	//These two should always be last.
	INTERACT_MAX,
	INTERACT_NONE
ENDENUM

//Modes.
ENUM g_eBoardModes
	PBM_VIEW = 0,
	PBM_POI_OVERVIEW,
	PBM_CREW_SELECT,
	PBM_CHOICE_SELECT,
	PBM_CONFIRM,
	
	PBM_INVALID
ENDENUM

//Display groups.
ENUM g_eBoardDisplayGroups
	PBDG_0,
	PBDG_1,
	PBDG_2,
	PBDG_3,
	PBDG_4,
	PBDG_5,
	PBDG_6,
	PBDG_7,
	PBDG_8,
	PBDG_9,
	PBDG_10,
	PBDG_11,
	
	//These two should always be last.
	PBDG_MAX,
	PBDG_INVALID
ENDENUM


//═════════════════════════╡ Crew Memeber Globals ╞═══════════════════════════

CONST_INT MAX_CREW_SIZE	5

//Crew memeber selection pool.
ENUM CrewMember
	CM_EMPTY = 0,
	
	//Permanent.
	CM_GUNMAN_G_GUSTAV,
	CM_GUNMAN_G_KARL,
	CM_GUNMAN_M_HUGH,
	CM_GUNMAN_B_NORM,
	CM_GUNMAN_B_DARYL,
	CM_HACKER_G_PAIGE,
	CM_HACKER_M_CHRIS,
	CM_DRIVER_G_EDDIE,
	CM_DRIVER_B_KARIM,
	
	//Unlockable.
	CM_GUNMAN_G_PACKIE_UNLOCK,
	CM_GUNMAN_G_CHEF_UNLOCK,
	CM_HACKER_B_RICKIE_UNLOCK,
	CM_DRIVER_G_TALINA_UNLOCK,
	
	CM_MAX_CREW_MEMBERS,
	CM_ERROR
ENDENUM


//Crew member types.
ENUM CrewMemberType
	CMT_EMPTY,
	CMT_GUNMAN,
	CMT_HACKER,
	CMT_DRIVER,
	
	CMT_MAX_CREW_TYPES,
	CMT_ERROR
ENDENUM


//Crew member specialist skill levels.
ENUM CrewMemberSkill
	CMSK_BAD = 0,
	CMSK_MEDIUM,
	CMSK_GOOD
ENDENUM


//Crew member statuses.
ENUM CrewMemberStatus
	CMST_NOT_SET,	//Default state when no status has been set.
	CMST_FINE,		//Crew member was untouched.
	CMST_INJURED,	//Crew member was badly injured.
	CMST_KILLED		//Crew member was killed in action.
ENDENUM


//Crew member type specific stats.
//Gunmen.
ENUM CrewGunmanStat
	CGS_MAX_HEALTH = 0,	
	CGS_ACCURACY,	
	CGS_SHOOT_RATE,	
	CGS_WEAPON_CHOICE		
ENDENUM
//Hackers.
ENUM CrewHackerStat
	CHS_SYS_KNOWLEDGE = 0,	
	CHS_DECRYPT_SKILL,	
	CHS_ACCESS_SPEED		
ENDENUM
//Drivers.
ENUM CrewDriverStat
	CDS_DRIVING_SKILL = 0,	
	CDS_COMPOSURE,	
	CDS_VEHICLE_CHOICE			
ENDENUM


//Component variation indexes.
//Components.
CONST_INT	CM_COMP_HEAD	0
CONST_INT	CM_COMP_HAIR	1
CONST_INT	CM_COMP_UPPER	2
CONST_INT	CM_COMP_LOWER	3
CONST_INT	CM_COMP_FEET	4
CONST_INT	CM_COMP_ACCS	5
CONST_INT	CM_COMP_MAX		6

//Component elements.
CONST_INT	CM_COMP_ELEMENT_DRAWABLE	0
CONST_INT	CM_COMP_ELEMENT_TEXTURE		1
CONST_INT	CM_COMP_ELEMENT_MAX			2


STRUCT CrewMemberStaticData
	CrewMemberType		type
	INT					jobCut
	INT					headVariation
	INT					defaultOutfitVariation
	MODEL_NAMES			model
ENDSTRUCT
CrewMemberStaticData g_sCrewMemberStaticData[CM_MAX_CREW_MEMBERS]


STRUCT CrewMemberActiveData
	CrewMemberSkill		skill
	INT 				statsA
	INT					statsB
ENDSTRUCT


//══════════════════════════╡ Planning Location Globals ╞═════════════════════════════

ENUM PlanningLocationName
	PLN_STRIPCLUB,
	PLN_SWEATSHOP,
	PLN_METH_LAB,
	PLN_TREV_CITY,
	
	PLN_MAX_PLANNING_LOCATIONS,
	PLN_ERROR
ENDENUM

STRUCT PlanningLocationData
	VECTOR vPlanningLocation
	VECTOR vPlanningLocationBoard
	FLOAT fPlanningLocationBoardHeading
	TEXT_LABEL_31 tPlanningLocationRoomName
ENDSTRUCT
PlanningLocationData g_sPlanningLocationData[PLN_MAX_PLANNING_LOCATIONS]


//═══════════════════════════╡ Heist Data Storage ╞═══════════════════════════════

CONST_INT	MAX_BOARD_CONVERSATIONS	4
CONST_INT	MAX_BOARD_STATIC_ITEMS	22
CONST_INT	MAX_BOARD_TODO_ITEMS	9
CONST_INT	MAX_BOARD_PINS			20
CONST_INT	MAX_BOARD_POIS			7

//Crew used bitflags.
CONST_INT	CREW_USED_JEWEL_GUSTAV		0
CONST_INT	CREW_USED_JEWEL_PACKIE		1
CONST_INT	CREW_USED_JEWEL_CHRIS		2
CONST_INT	CREW_USED_JEWEL_RICKIE		3
CONST_INT	CREW_USED_JEWEL_PAIGE		4
CONST_INT	CREW_USED_JEWEL_EDDIE		5
CONST_INT	CREW_USED_JEWEL_KARIM		6
CONST_INT	CREW_USED_PALETO_GUSTAV		7
CONST_INT	CREW_USED_PALETO_PACKIE		8
CONST_INT	CREW_USED_PALETO_CHEF		9
CONST_INT	CREW_USED_AGENCY_GUSTAV		10
CONST_INT	CREW_USED_AGENCY_PACKIE		11
CONST_INT	CREW_USED_AGENCY_DARYL		12
CONST_INT	CREW_USED_AGENCY_HUGH		13
CONST_INT	CREW_USED_AGENCY_CHRIS		14
CONST_INT	CREW_USED_AGENCY_RICKIE		15
CONST_INT	CREW_USED_AGENCY_EDDIE		16
CONST_INT	CREW_USED_AGENCY_NORM		17
CONST_INT	CREW_USED_AGENCY_KARIM		18
CONST_INT	CREW_USED_AGENCY_PAIGE		19
CONST_INT	CREW_USED_AGENCY_TALINA		20
CONST_INT	CREW_USED_MAX				21


ENUM BoardDialogueSlot
	BDS_CONFIRM = 0,
	BDS_CHOICE_SELECT,
	BDS_CREW_SELECT,
	BDS_TOO_SLOW,
	
	BDS_POI_1,
	BDS_POI_2,
	BDS_POI_3,
	BDS_POI_4,
	BDS_POI_5,
	BDS_POI_6,
	BDS_POI_7,
	
	BDS_MAX_SLOTS
ENDENUM

ENUM CrewDialogueSlot
	CRDS_PICK_GUSTAV = 1,
	CRDS_PICK_KARL,
	CRDS_PICK_HUGH,
	CRDS_PICK_NORM,
	CRDS_PICK_DARYL,
	CRDS_PICK_PAIGE,
	CRDS_PICK_CHRIS,
	CRDS_PICK_EDDIE,
	CRDS_PICK_KARIM,
	CRDS_PICK_PACKIE,
	CRDS_PICK_CHEF,
	CRDS_PICK_RICKIE,
	CRDS_PICK_TALINA,

	CRDS_MET_PACKIE,
	CRDS_MET_RICKIE,
	CRDS_MET_TALINA,
	CRDS_MET_CHEF,
	
	CRDS_USED_JEWEL,
	CRDS_USED_PALETO,
	CRDS_USED_AGENCY,
	CRDS_USED_JEWEL_PALETO,
	CRDS_USED_JEWEL_AGENCY,
	CRDS_USED_ALL,
	CRDS_USED_RICKIE_LAST_ONE,
	CRDS_USED_RICKIE_LAST_TWO,
	CRDS_USED_KARIM_LAST_ONE,
	CRDS_USED_KARIM_LAST_TWO,
	CRDS_USED_CHRIS_JEWEL,
	CRDS_USED_CHRIS_AGENCY,
	CRDS_USED_CHRIS_JEWEL_AGENCY,
	CRDS_USED_BADG_AGENCY,

	CRDS_MAX_SLOTS
ENDENUM

ENUM HeistChoiceDialogueSlot
	HCDS_CHOICE,
	HCDS_GUNMAN,
	HCDS_DRIVER,
	HCDS_HACKER,
	HCDS_EXIT,

	CDS_MAX_SLOTS
ENDENUM

STRUCT BoardPixel
	INT x
	INT y
ENDSTRUCT


STRUCT HeistBoardData
	STATIC_BLIP_NAME_ENUM	blipHeist
	PlanningLocationName	ePlanningLocation
	
	INT 					iScaleformPixelWidth
	INT 					iScaleformPixelHeight
	FLOAT 					fScaleformWidth
	FLOAT 					fScaleformHeight
	FLOAT 					fScaleformXScale
	FLOAT 					fScaleformYScale
	FLOAT					fCameraDistance
	FLOAT 					fMaxZoom
	FLOAT 					fChoiceZoom
	FLOAT 					fPOIZoom
	
	BoardPixel				sDefaultFocusPos
	BoardPixel				sListBoardPos

	BoardPixel				sChoiceBoardPos
	BoardPixel				sChoiceCameraOffset
	TEXT_LABEL_15			tChoiceHelp[2]
	g_eBoardDisplayGroups	eChoiceGroup
	g_eBoardDisplayGroups	eChoiceSelectedGroup[2]
	
	INT						iNoBoardPins
	BoardPixel				sPinPos[MAX_BOARD_PINS]
	g_eBoardDisplayGroups	ePinGroup[MAX_BOARD_PINS]
	
	INT						iMaxCrewSize
	BoardPixel				sCrewBoardPos[MAX_CREW_SIZE]
	TEXT_LABEL_15			tCrewHelp[2]
	
	INT						iNoStaticItems
	INT						iStaticItemAsset[MAX_BOARD_STATIC_ITEMS]
	BoardPixel				sStaticItemPos[MAX_BOARD_STATIC_ITEMS]
	g_eBoardDisplayGroups	eStaticItemGroup[MAX_BOARD_STATIC_ITEMS]
	
	INT						iNoTodoItems
	g_eBoardDisplayGroups	eTodoListGroup
	TEXT_LABEL_15			tTodoItemText[MAX_BOARD_TODO_ITEMS]
	g_eBoardDisplayGroups	eTodoItemAppearGroup[MAX_BOARD_TODO_ITEMS]
	g_eBoardDisplayGroups	eTodoItemTickGroup[MAX_BOARD_TODO_ITEMS]
	
	TEXT_LABEL_15			tBoardScaleform
	TEXT_LABEL_15			tBoardText
	TEXT_LABEL_7			tBoardDialogueSlot
	TEXT_LABEL_7			tCrewDialogueSlot
	TEXT_LABEL_7			tBoardDialogueLabels[BDS_MAX_SLOTS]
	INT						iBitsetBoardDialoguePlayed
	TEXT_LABEL_7			tCrewDialogueLabels[CRDS_MAX_SLOTS]
	
	INT						iNoPointsOfInterest
	INT						iPOIDelayBeforeNextBitset
	BoardPixel				sPOIPos[MAX_BOARD_POIS]
	g_eBoardDisplayGroups	ePOIGroup[MAX_BOARD_POIS]
	
	VECTOR					vAreaBlockCenter
	VECTOR					vAreaBlockDimensions
	
	#IF IS_DEBUG_BUILD
		BOOL bDebugDrawCenter = FALSE
		BOOL bDebugDrawDefaultFocus = FALSE
		BOOL bDebugDrawStickFocus = FALSE
		BOOL bDebugDrawGrid = FALSE
		BOOL bDebugDrawPins = FALSE
		BOOL bDebugDrawPOIs = FALSE
		BOOL bDebugDrawStaticItems = FALSE
		
		BOOL bDebugDrawingEnabled = FALSE
		BOOL bBoardWidgetCreated = FALSE
		BOOL bRefreshBoard = FALSE
		BOOL bRefreshViews = FALSE
		INT iModeWidget
	#ENDIF
ENDSTRUCT

BOOL g_bHeistBoardViewActive = FALSE
INT g_iBitsetHeistBoardUpdated 			//Flags if a change has been made to the board that the heist controller needs to deal with.
INT g_iBitsetHeistBoardPinned 			//Flags if a heist planning board should be forced to load, create and remain created until unpinned.
INT g_iBitsetHeistBoardLoaded			//Flags if a heist planning board is currently loaded.
INT g_iBitsetHeistBoardAllowCreation	//Allows a heist planning board that is pinned in memory to create. Used to control the exact frame the board creates on.

//Temporary crew member stat values to be held during a heist
//to be used to track stat increases that will be made permanent
//if the heist is passed.
STRUCT HeistTempCrewStats
	BOOL statsIncreased
	INT statsA
	INT statsB
ENDSTRUCT
HeistTempCrewStats g_sHeistTempStats[MAX_CREW_SIZE]
BOOL g_bHeistTempCrewStatsPrimed = FALSE

STRUCT HeistChoiceData
	INT				iCrewSize
	CrewMemberType	eCrewType[MAX_CREW_SIZE]
	TEXT_LABEL_7	tChoiceDialogueLabels[CDS_MAX_SLOTS]
	INT 			iBitsetChoiceDialoguePlayed
ENDSTRUCT
HeistChoiceData g_sHeistChoiceData[MAX_HEIST_CHOICES]

//═══════════════════════════╡ End Screen Globals ╞═══════════════════════════════


CONST_INT	MAX_END_SCREEN_EXPENSES		10


STRUCT HeistEndScreen
	FLOAT fXOffset = 0.8
	INT iAlpha = 0
	
	INT iTimeDisplayed = 0
	BOOL bSformeUploadDone
	SCALEFORM_INDEX pieChart
	SCALEFORM_INDEX continueButton

	FLOAT fLesterPercentage //-1 means not present 
	INT iLesterTotal
	
	INT iHeistTimer
	
	INT iExpenses
ENDSTRUCT
HeistEndScreen sEndScreen


ENUM HEIST_TIME_WINDOW_ENUM
	UNSET_HEIST_TIME_WINDOW,
	
	HEIST_TIME_WINDOW_JC_FRANKLIN_TO_ROOF,
	HEIST_TIME_WINDOW_JC_DRIVE_TO_JOB_TIME,
	HEIST_TIME_WINDOW_JC_LOOTING_TIME,
	HEIST_TIME_WINDOW_JC_MOTORBIKE_CHASE_TIME,
	
	TOTAL_HEIST_TIME_WINDOWS
ENDENUM

CONST_INT MAX_HEIST_TIME_WINDOW_ENUM_ENTRIES 5


ENUM HEIST_ENDSCREEN_GEAR_ENUM
	//UNSET_HEIST_ENDSCREEN_GEAR_ENUM = -1,
	
	HEIST_GEAR_GAS_GRENADE,
	HEIST_GEAR_REMOTE_RIFLE,
		
	TOTAL_HEIST_ENDSCREEN_GEAR_ENTRIES
ENDENUM

CONST_INT MAX_HEIST_GEAR_ENUM_ENTRIES 5


ENUM HEIST_ENDSCREEN_PENALTY_ENUM
	
	HEIST_PENALTY_MONEY_DROPPED,
	
	HEIST_PENALTY_JCVAN,//JC heist van damage
	HEIST_PENALTY_JCHOUSE,//Madrazzo house damage
	
	HEIST_PENALTY_AGENTCUT,
	
	HEIST_PENALTY_UNSELLABLE_WEAPON,
	
	HEIST_PENALTY_BIGS_SLOW_LOADING,
	HEIST_PENALTY_HOSTAGE_GIFT,
	
	TOTAL_HEIST_ENDSCREEN_PENALTY_ENTRIES
ENDENUM

CONST_INT MAX_HEIST_PENALTY_ENUM_ENTRIES 5


STRUCT HeistEndScreenData
	INT iPotentialTake 				//total they could have taken
	INT iActualTake 				//total they did take
	INT iPlayerTake[3] 				//used to distribute rewards to player accounts seperate from display
	FLOAT fPlayerPercentage[3] 		// -1 means not present

	INT iTimeTaken	// how long it took the player to complete the heist (used in repeat play menu stats)
	CrewMemberStatus eCrewStatus[MAX_CREW_SIZE]
	INT iCrewMemberTake[MAX_CREW_SIZE]
ENDSTRUCT


STRUCT HEIST_NONE_SAVED_DATA

	//Time windows
	BOOL bTimeWindowOpen[MAX_HEIST_TIME_WINDOW_ENUM_ENTRIES]
	INT iTimeWindowMSValue[MAX_HEIST_TIME_WINDOW_ENUM_ENTRIES]
	HEIST_TIME_WINDOW_ENUM eTimeWindowSource[MAX_HEIST_TIME_WINDOW_ENUM_ENTRIES]

	//Heist gear buffer
	HEIST_ENDSCREEN_GEAR_ENUM GearUsed[MAX_HEIST_GEAR_ENUM_ENTRIES]
	INT iGearLogged
	
	//Penalty buffer
	HEIST_ENDSCREEN_PENALTY_ENUM Penalties[MAX_HEIST_PENALTY_ENUM_ENTRIES]
	INT iPenaltiesValues[MAX_HEIST_PENALTY_ENUM_ENTRIES]
	INT iPenaltiesLogged

ENDSTRUCT

HEIST_NONE_SAVED_DATA g_HeistNoneSavedDatas[NO_HEISTS]


#IF IS_DEBUG_BUILD
	INT g_iDebugHeistDoCrewSelection = 0
	INT g_iDebugChoiceSelection[NO_HEISTS]
#ENDIF

//═══════════════════════╡ Misc Heist Mission stuff ╞════════════════════════════

//Jewel Heist
//INT g_iJewelHeistPlayedCounter
BOOL g_bJewelStoreJobSeeCarCrashOnce
BOOL g_bLateLeaveJewelStore
BOOL g_JH2AGarbageTruckSeen
BOOL g_bBirdsJHeist
INT g_iJewelPrep1AVanHealth				= 1000

//Paleto Score
INT g_iPaletoScoreTake					= 0

//FBI Agency
BOOL g_bCalled911BeforeText = FALSE

//Heist take displays
CONST_INT HEIST_TAKE_DISPLAY_FLASH_TIME		5000

//═══════════════════════════════════════════════════════════════════════════════
//════════════════════════════╡ End screen display settings ╞════════════════════
//═══════════════════════════════════════════════════════════════════════════════

CONST_INT HEIST_SCREEN_MAX_CREW 9


ENUM HEIST_END_SCREEN_CREW_ENTRY_TYPE
	HENDSCR_PLAYER,
	HENDSCR_EXTRA,//lester etc.
	HENDSCR_GUNMAN,
	HENDSCR_HACKER,
	HENDSCR_DRIVER

ENDENUM





//used to set the values
STRUCT HEIST_END_SCREEN_RAW_DATA
	
	//crewlist, left col
	INT iCrewTotal
	INT iCrewDead

	INT iCrewDollars[HEIST_SCREEN_MAX_CREW]
	HEIST_END_SCREEN_CREW_ENTRY_TYPE eCrewTypes[HEIST_SCREEN_MAX_CREW]
	FLOAT fCrewPercentages[HEIST_SCREEN_MAX_CREW]
	TEXT_LABEL iCrewNames[HEIST_SCREEN_MAX_CREW]
	TEXT_LABEL iCrewImages[HEIST_SCREEN_MAX_CREW]
	BOOL bKIA[HEIST_SCREEN_MAX_CREW]
	//crew stat dump - this is a subset of the crewlist, find which maps to which with indices
	INT iCrewStatTotal
	INT iCrewStatIndices[HEIST_SCREEN_MAX_CREW] //
	
	INT iCrewStatsForThisMember[HEIST_SCREEN_MAX_CREW] 
	//INT iCrewStatTypeEnum[HEIST_SCREEN_MAX_CREW][5] //redundant
	FLOAT fCrewStatCurrentPerc[HEIST_SCREEN_MAX_CREW][5]
	FLOAT fCrewStatAdditionalPerc[HEIST_SCREEN_MAX_CREW][5]
	
	
	FLOAT fStatPercBlend
	
	//overview top
	INT iActual
	INT iTotal
	
	INT iHeistIndex
ENDSTRUCT

//used to position the boxes, 
//text within elements takes it's position relative to these
STRUCT HEIST_END_SCREEN_ELEMENT_POSITIONS
	
	BOOL bSet//if this is set to false then the end screen will set these values to default
	//then set this to true
	
	//Title text anchor
	FLOAT fAnchorCrewNameX
	FLOAT fAnchorCrewNameY
	
	FLOAT fSeperationX
	FLOAT fSeperationY

	
	FLOAT fHeadersX
	FLOAT fHeadersY
	FLOAT fHeadersW
	FLOAT fHeadersH
	
	FLOAT fHeadersToContentY
	
	FLOAT fContentCrewAdvanceY
	FLOAT fContentStatAdvanceY
	
	FLOAT fContentOverviewY
	FLOAT fContentOverviewLineY

	//subsection values
	FLOAT fTopLinerY
	
	FLOAT fTopCrewListColourBarX

	FLOAT fCrewStatTitleY
	
	FLOAT fCrewStatBarW
	FLOAT fCrewStatBarH
	
	FLOAT fCrewStatScrollBarH
	
	
	INT bargb[HEIST_SCREEN_MAX_CREW][3]
	
	INT topcol[3]
	INT titlecol[3]

	INT lightAlpha
	INT darkAlpha

	
	//0.03888
	FLOAT fontSizeHeading
	FLOAT fontSizeLarge
	FLOAT fontSizeMedium
	FLOAT fontSizeTiny
	
	
ENDSTRUCT

//HEIST_END_SCREEN_RAW_DATA g_HeistEndScreenRawData
//HEIST_END_SCREEN_ELEMENT_POSITIONS g_HeistEndScreenRawPositions

//═══════════════════════════════════════════════════════════════════════════════
//════════════════════════════╡ Data to be saved ╞═══════════════════════════════
//═══════════════════════════════════════════════════════════════════════════════

STRUCT HeistSaved
	BOOL					bChoiceHelpDisplayed[NO_HEISTS]
	BOOL					bCrewHelpDisplayed[NO_HEISTS]
	CrewMember 				eSelectedCrew[MAX_HEIST_CHOICES][MAX_CREW_SIZE]
	CrewMemberActiveData 	sCrewActiveData[CM_MAX_CREW_MEMBERS]
	INT						iCrewUnlockedBitset
	INT						iCrewUsedBitset
	INT						iCrewDeadBitset
	INT						iCrewDialogueBitset
	INT 					iDisplayGroupVisibleBitset[NO_HEISTS]
	HeistEndScreenData 		sEndScreenData[NO_HEISTS]
ENDSTRUCT



