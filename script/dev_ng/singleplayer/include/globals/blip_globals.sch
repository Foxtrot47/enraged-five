USING "blip_enums.sch"
USING "commands_hud.sch"
USING "candidate_enums.sch"
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	blip_globals.sch
//		AUTHOR			:	Ak
//		DESCRIPTION		:	The global data for the none mission blip system, 
//							should not be accessed directly. Methods for working with the 
//							static blips are in blip_control_public.sch.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


//------------------------------------------------------------------------------------------
//---------------------------------- Global Blip Scales ------------------------------------
//------------------------------------------------------------------------------------------

CONST_FLOAT		BLIP_SIZE_PED						0.7		//Default scale of pedestrian blips.
CONST_FLOAT		BLIP_SIZE_OBJECT 					0.7 	//Default scale of object blips.
CONST_FLOAT		BLIP_SIZE_VEHICLE					1.0 	//Default scale of vehicle blips.
CONST_FLOAT		BLIP_SIZE_PICKUP					0.7 	//Default scale of pickup blips.
CONST_FLOAT		BLIP_SIZE_COORD						1.0 	//Default scale of coord blips.
CONST_FLOAT		BLIP_SIZE_CHECKPOINT				1.2		//Default scale of checkpoints.
CONST_FLOAT		BLIP_SIZE_CHECKPOINT_HIT			0.5		//Default scale of checkpoints that have already been hit.
CONST_FLOAT		BLIP_SIZE_OUT_OF_TIME_SLOT  		0.6		

CONST_FLOAT		BLIP_SIZE_NETWORK_PLAYER 			0.8							//	Default scale of a network player blip.
CONST_FLOAT		BLIP_SIZE_NETWORK_PED				BLIP_SIZE_PED 				//	0.85	Default scale of pedestrian blips.
CONST_FLOAT		BLIP_SIZE_NETWORK_OBJECT 			BLIP_SIZE_OBJECT 			//	0.85 	Default scale of object blips.
CONST_FLOAT		BLIP_SIZE_NETWORK_VEHICLE			BLIP_SIZE_VEHICLE 			//	1.0 	Default scale of vehicle blips.
CONST_FLOAT		BLIP_SIZE_NETWORK_VEHICLE_LARGE		1.2							//	Default scale of large highlighted pickups.
CONST_FLOAT		BLIP_SIZE_NETWORK_PICKUP			BLIP_SIZE_PICKUP			//	0.85 	Default scale of pickup blips.
CONST_FLOAT		BLIP_SIZE_NETWORK_PICKUP_LARGE		1.2							//	Default scale of large highlighted pickups.
CONST_FLOAT		BLIP_SIZE_NETWORK_COORD				BLIP_SIZE_COORD				//	1.0 	Default scale of coord blips.
CONST_FLOAT		BLIP_SIZE_NETWORK_CHECKPOINT		BLIP_SIZE_CHECKPOINT		//	1.2		Default scale of checkpoints.
CONST_FLOAT		BLIP_SIZE_NETWORK_CHECKPOINT_HIT	BLIP_SIZE_CHECKPOINT_HIT	//	0.5		Default scale of checkpoints that have already been hit.


//------------------------------------------------------------------------------------------

CONST_INT STATIC_BLIP_SETTING_ACTIVE 						0
CONST_INT STATIC_BLIP_SETTING_HIDE_INSIDE 					1
CONST_INT STATIC_BLIP_SETTING_HIDE_OUTSIDE 					2
CONST_INT STATIC_BLIP_SETTING_VISIBILITY 					3 	//used to suppress visibility of a blip but not activation status
CONST_INT STATIC_BLIP_SETTING_APPEAR_FRONT 					4
CONST_INT STATIC_BLIP_SETTING_RADAR_SHORT 					5
CONST_INT STATIC_BLIP_SETTING_RADAR_LONG 					6
CONST_INT STATIC_BLIP_SETTING_HIDE_ON_MISSION 				7
CONST_INT STATIC_BLIP_SETTING_CHARACTER_SPECIFIC 			8
CONST_INT STATIC_BLIP_SETTING_WILL_FLASH_ON_NEXT_ACTIVE		9 
CONST_INT STATIC_BLIP_SETTING_OTHER_CHARACTER_SPECIFIC 		10
CONST_INT STATIC_BLIP_SETTING_NO_LONG_RANGE_ON_MISSION 		11

CONST_INT STATIC_BLIP_SETTING_ON_WANTED_HIDE 				13
CONST_INT STATIC_BLIP_SETTING_HIDE_ON_RANDOM_EVENT 			14
CONST_INT STATIC_BLIP_SETTING_ACTIVATED_EXTERNALLY 			15	// dictates if it should come back after suppression
CONST_INT STATIC_BLIP_SETTING_MISSION_LEVEL_EXCLUSIVE 		16 	// dictates that it may only display when this type of mission could launch
CONST_INT STATIC_BLIP_SETTING_SHOW_MAP_HOVER_INFO 			17	// dictates if this blip should display info when hovered over on the map.
CONST_INT STATIC_BLIP_SETTING_STATUS_CHANGED 				18 	// replacement for the bool in the struct
CONST_INT STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE 		19 	// replacement for the bool in the struct
CONST_INT STATIC_BLIP_SETTING_TICKED 						20	// replacement for the bool in the struct

CONST_INT STATIC_BLIP_COLOUR_BITS 							5	// Colour setting
CONST_INT STATIC_BLIP_SETTING_COLOUR_CODE_START 			21	// to bit 26

CONST_INT STATIC_BLIP_BLIP_SETTING_HIDE_IN_EXILE 			27 	// hides this blip if the flow is in the exile stage
CONST_INT STATIC_BLIP_SETTING_AREA_BLIP			 			28	// Used for the Barry RC weed stash missions
CONST_INT STATIC_BLIP_DISPLAY_AS_INACTIVE	 				29	// Used for greying out specific blips
CONST_INT STATIC_BLIP_MINIGAME_LEAVE_AREA_FLAG	 			30


//------------------------------------------------------------------------------------------

TWEAK_FLOAT WEAPON_RANGE_MULTIPLIER_FOR_BLIPS 2.5

TWEAK_FLOAT NOTICABLE_RADIUS  	 100.0
TWEAK_FLOAT HEARD_RADIUS		 100.0
TWEAK_FLOAT SHOTAT_RADIUS		 1.5
TWEAK_INT 	NOTICABLE_TIME  	 3500					// Reduced from 5000 for TODO B*820865 & 820908
TWEAK_INT 	NOTICABLE_CHECK_TIME 500	
TWEAK_INT	BLIP_FLASH_TIME		 1000

CONST_INT 	BLIP_FADE_PERIOD	 1400					// Reduced from 2000 for TODO B*820865 & 820908

CONST_INT	BLIP_HIDE_FADE_PERIOD	750

//enumCharacterList g_BlipSystemCharChangeDetector
BOOL 			  g_bBlipSystemRefreshDetector = TRUE
BOOL 			  g_bBlipChangeDuringUpdate = FALSE
BOOL			  g_bBlipSystemStartupLeash = TRUE //blip system is unleashed by startup positioning
//type is implicit in array position


STRUCT BlipData
	VECTOR 						vCoords[3]
	FLOAT						fRadius					// used for area blips
	INT 						iSetting
	
	BLIP_SPRITE 				eSprite[3]
	STATIC_BLIP_CATEGORIES_ENUM eCategory
	
	enumCharacterList 			eVisibleOnlyToChar
	enumCharacterList 			eVisibleOnlyToOtherChar
	
	BLIP_INDEX 					biBlip 
	
	TEXT_LABEL_7				txtBlipName

	MISSION_CANDIDATE_MISSION_TYPE_ENUM launchLevel 	// STATIC_BLIP_SETTING_MISSION_LEVEL_EXCLUSIVE
ENDSTRUCT


BlipData g_GameBlips[g_iTotalStaticBlips]
BOOL 	 g_RebuildDiscoverableBlipList					// Used to request the blip discoverer to rebuild the list it is watching
BOOL 	 g_BlipSystemPrologueMode = FALSE
BOOL 	 g_bBlipSystemMissionStateChange = FALSE 		// Used to check for candidate level changes
BOOL	 g_bMinigameBlipsRestored = FALSE

// KENNETH
// These vars used to be in the blip_controller script and was causing 
// a stack overflow when a bunch of new blips were added for the shops.
// Moved them to this globals header to lift the weight from the script.
// Debug only.

#IF IS_DEBUG_BUILD
	
	//widget values
	BOOL d_BlipCtrl_bWidgetBools[g_iTotalStaticBlips]
	
	//CONST_INT STATIC_BLIP_SETTING_ACTIVE 0
	BOOL d_BlipCtrl_bWidgetBoolActive[g_iTotalStaticBlips]
	
	//CONST_INT STATIC_BLIP_SETTING_HIDE_INSIDE 1
	BOOL d_BlipCtrl_bWidgetBoolHiddenInside[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_SETTING_HIDE_OUTSIDE 2
	BOOL d_BlipCtrl_bWidgetBoolHiddenOutside[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_SETTING_FLASH_ON_ACTIVE 3
	BOOL d_BlipCtrl_bWidgetBoolFlashOnActive[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_SETTING_APPEAR_FRONT 4
	BOOL d_BlipCtrl_bWidgetBoolAppearFront[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_SETTING_RADAR_SHORT 5
	BOOL d_BlipCtrl_bWidgetBoolRadarShort[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_SETTING_RADAR_LONG 6
	BOOL d_BlipCtrl_bWidgetBoolRadarLong[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_SETTING_HIDE_ON_MISSION 7
	BOOL d_BlipCtrl_bWidgetBoolHideOnMission[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_SETTING_ZONE_SPECIFIC 9	
	BOOL d_BlipCtrl_bWidgetBoolZoneSpecific[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_SETTING_DISCOVERABLE 10
	BOOL d_BlipCtrl_bWidgetBoolDiscoverable[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_SETTING_DISCOVERED 11
	BOOL d_BlipCtrl_bWidgetBoolDiscovered[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_ACTIVATION_STATE_PREPOST_MISSION 12
	BOOL d_BlipCtrl_bWidgetBoolSupressedDueToMission[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_SETTING_CHARACTER_SPECIFIC 13
	BOOL d_BlipCtrl_bWidgetBoolCharSpecific[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_SETTING_OTHER_CHARACTER_SPECIFIC 16
	BOOL d_BlipCtrl_bWidgetBoolSecondaryCharSpecific[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_SETTING_HAS_FLASHED 14
	BOOL d_BlipCtrl_bWidgetBoolHasFlashed[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_IGNORES_CHAR_SPECIFIC_MODE_BOOL 15 
	BOOL d_BlipCtrl_bWidgetBoolUniversal[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_SETTING_NO_LONG_RANGE_ON_MISSION 17
	BOOL d_BlipCtrl_bWidgetBoolNotLongRangeDuringMission[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_SETTING_TIME_SPECIFIC 18
	BOOL d_BlipCtrl_bWidgetBoolTimeSpecific[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_SETTING_ON_WANTED_HIDE 19
	BOOL d_BlipCtrl_bWidgetBoolOnWantedHide[g_iTotalStaticBlips]
	//CONST_INT STATIC_BLIP_SETTING_HIDE_ON_RANDOM_EVENT 20
	BOOL d_BlipCtrl_bWidgetBoolHideOnRandomEvent[g_iTotalStaticBlips]
#ENDIF

