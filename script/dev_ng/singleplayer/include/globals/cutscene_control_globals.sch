//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 04/04/12			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│				 	Off-mission Cutscene Controller	GTA5 Globals				│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

CONST_INT	MAX_OFFMISSION_CUTSCENE_REQUESTS	5	//Maxmimum number of requests the cutscene controller can manage at a time.
CONST_INT	NULL_OFFMISSION_CUTSCENE_REQUEST	-1	//Default cutscene request ID representing no request.

//Enums representing the actions a script may be requested to take regarding their cutscene load requests.
ENUM OFFMISSION_CUTSCENE_ACTION
	OCA_WAIT,
	OCA_LOAD,
	OCA_UNLOAD
ENDENUM

//Enums representing the various types of cutscene loading requests.
ENUM OFFMISSION_CUTSCENE_TYPE
	OCT_NULL,

	OCT_RC_MISSION,
	OCT_STORY_MISSION,
	OCT_MUST_LOAD
ENDENUM

//Struct representing a single cutscene load request.
STRUCT OFFMISSION_CUTSCENE_REQUEST
	INT 						iID
	OFFMISSION_CUTSCENE_TYPE	eType
ENDSTRUCT

//The list of current cutscene requests.
OFFMISSION_CUTSCENE_REQUEST	g_sOffMissionCutsceneRequests[MAX_OFFMISSION_CUTSCENE_REQUESTS]	

//The number of cutscene request IDs currently stored in the list.
INT	g_iOffMissionCutsceneRequestCount

//Flag to tell the controller that a change has been made to the request list.
BOOL g_bOffMissionCutsceneChange = FALSE

//Keep track of the frame we unloaded a cutscene on to ensure we don't start loading
//on the same frame.
INT g_iOffMissionCutsceneUnloadedFrame = 0

//The request ID that has been given the OK to load a cutscene.
INT	g_iOffMissionCutsceneRequestAllowed	= NULL_OFFMISSION_CUTSCENE_REQUEST

//The request ID that has a cutscene loaded.
INT	g_iOffMissionCutsceneRequestActive = NULL_OFFMISSION_CUTSCENE_REQUEST

// Global flag for tracking when scripts are set to be safe for cutscenes.
BOOL g_bScriptsSetSafeForCutscene = FALSE

// Global flag for tracking when scripts have determined we must fade out to encourage
// cutscene streaming.
BOOL g_bCutsceneFailsafeActive = FALSE

#IF IS_DEBUG_BUILD
	INT g_iUnauthCutTimeDisplayed = -1
	TEXT_LABEL_31 g_txtUnauthCutName
#ENDIF
