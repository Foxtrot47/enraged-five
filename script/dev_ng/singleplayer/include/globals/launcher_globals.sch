// launcher_globals.sch

USING "minigame_globals.sch"
USING "commands_brains.sch"

BOOL g_MG_Bypass_Respawn_Cutscene = FALSE
BOOL g_MG_Slash_Showing = FALSE

ENUM LAUNCHED_SCRIPT_FLAGS
	LAUNCHED_SCRIPT_MinigameFailed = 0,
	LAUNCHED_SCRIPT_AirTraffickingFailed,
	LAUNCHED_SCRIPT_GroundTraffickingFailed,
	LAUNCHED_SCRIPT_TowingFailed
ENDENUM

STRUCT LauncherDataSaved
	INT 			iLauncherFlags
ENDSTRUCT

// Launcher data. Does not need to be saved.
LauncherDataSaved sLauncherData

PROC Store_Launcher_Flag_Data(LAUNCHED_SCRIPT_FLAGS flagToSet)
	sLauncherData.iLauncherFlags = ENUM_TO_INT(flagToSet)
ENDPROC

PROC SET_CURRENT_MINIGAME_TO_BYPASS_RESPAWN_CUTSCENE(BOOL thisBool)
	IF g_OnMissionState = MISSION_TYPE_MINIGAME OR g_OnMissionState = MISSION_TYPE_MINIGAME_FRIENDS OR g_OnMissionState = MISSION_TYPE_RANDOM_EVENT
		g_MG_Bypass_Respawn_Cutscene = thisBool
		IF thisBool
			PRINTLN("SUCESSFUL CALLING SET_CURRENT_MINIGAME_TO_BYPASS_RESPAWN_CUTSCENE(TRUE)")
		ELSE
			PRINTLN("SUCESSFUL CALLING SET_CURRENT_MINIGAME_TO_BYPASS_RESPAWN_CUTSCENE(FALSE)")
		ENDIF
	ELSE
		IF thisBool
			PRINTLN("UNSUCCESSFUL CALLING SET_CURRENT_MINIGAME_TO_BYPASS_RESPAWN_CUTSCENE(TRUE)")
		ELSE
			PRINTLN("SUCESSFUL CALLING SET_CURRENT_MINIGAME_TO_BYPASS_RESPAWN_CUTSCENE(FALSE)")
		ENDIF
		g_MG_Bypass_Respawn_Cutscene = FALSE
	ENDIF
	PRINTLN("g_MG_Bypass_Respawn_Cutscene is (1 for true)", g_MG_Bypass_Respawn_Cutscene)
	
ENDPROC

FUNC BOOL IS_CURRENT_MINIGAME_SET_TO_BYPASS_RESPAWN_CUTSCENE()
	IF g_OnMissionState = MISSION_TYPE_MINIGAME OR g_OnMissionState = MISSION_TYPE_MINIGAME_FRIENDS OR g_OnMissionState = MISSION_TYPE_RANDOM_EVENT
		RETURN g_MG_Bypass_Respawn_Cutscene
	ELSE
		g_MG_Bypass_Respawn_Cutscene = FALSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL IS_MINIGAME_SPLASH_SHOWING()
	IF g_OnMissionState = MISSION_TYPE_MINIGAME OR g_OnMissionState = MISSION_TYPE_MINIGAME_FRIENDS
		RETURN g_MG_Slash_Showing
	ELSE
		//PRINTLN("We're not in a MINIGAME")
		g_MG_Slash_Showing = FALSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC SET_MINIGAME_SPLASH_SHOWING(BOOL thisBool)
	IF g_OnMissionState = MISSION_TYPE_MINIGAME OR g_OnMissionState = MISSION_TYPE_MINIGAME_FRIENDS
		g_MG_Slash_Showing = thisBool
	ELSE
		g_MG_Slash_Showing = FALSE
	ENDIF
ENDPROC

PROC RESET_MINI_GAME_TENNIS_LAUNCHERS()
	VECTOR vMyCurrentPlayerCoords
	STRING launcherScriptName = "launcher_tennis"
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		vMyCurrentPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ELSE
		EXIT //leave if the ped is injured.
	ENDIF
			
	CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Checking promximity to ", launcherScriptName)
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("launcher_tennis")) = 0
		//check if im in range of one of the points
		IF IS_PLAYER_WITHIN_RANGE_OF_TENNIS_LAUNCHER(vMyCurrentPlayerCoords, MG_BRAIN_ACTIVATION_RANGE_MEDIUM)		
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "Reactivating script brains for", launcherScriptName)
			REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE(launcherScriptName)			
		ELSE
			CPRINTLN(DEBUG_SCRIPT_LAUNCH, "player isn't within range of any ", launcherScriptName, " points")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_SCRIPT_LAUNCH, launcherScriptName, " currently has at least one script already running")
	ENDIF
ENDPROC
