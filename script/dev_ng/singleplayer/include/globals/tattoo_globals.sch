//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	tattoo_globals.sch											//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Contains the saved data struct and a list of enums that we	//
//							can use to reference tattoos.								//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "commands_ped.sch"
USING "commands_extrametadata.sch"
USING "script_maths.sch"


CONST_INT MAX_NUMBER_OF_TATTOOS 127 // MAX_TATTOOS_IN_SHOP + MP male torso decals + tshirt transfers + female hair overlays - cant go any higher than 127!!!
CONST_INT MAX_TATTOOS_IN_SHOP	71
CONST_INT MAX_NUMBER_OF_TATTOO_BITSETS ((MAX_NUMBER_OF_TATTOOS/32) + 1)
CONST_INT MAX_NUMBER_OF_DLC_TATTOO_BITSETS	50
CONST_INT MAX_DLC_TATTOOS_IN_SHOP			((MAX_NUMBER_OF_DLC_TATTOO_BITSETS-1)*32)

CONST_INT PED_TATTOO_UNLOCKED_SLOT 0
CONST_INT PED_TATTOO_CURRENT_SLOT 1

CONST_INT MAX_NUMBER_OF_TATTOO_FACING 8
//////////////////////////////////////////////////////////////////////////////////////////
///    Struct for the non saved global data
STRUCT TATTOO_DATA_STRUCT
	TEXT_LABEL_15 sLabel
	INT iCollection
	INT iPreset
	TATTOO_FACTION_ENUM eFaction
	INT iCost
	INT iUpgradeGroup
	INT iBitset
	INT iBitIndex
	TATTOO_NAME_ENUM eEnum
	TATTOO_FACING eFace
ENDSTRUCT

//////////////////////////////////////////////////////////////////////////////////////////
///     Struct for the non-saved makeup/facepaint data
STRUCT MAKEUP_DATA_STRUCT
	TEXT_LABEL_15 sLabel
	INT iCost
ENDSTRUCT


//////////////////////////////////////////////////////////////////////////////////////////
///    Struct for the global saved data
STRUCT PED_TATTOO_STRUCT
	// Each bit in the INTs represent a tattoo item
	INT iUnlockedTattoos[MAX_NUMBER_OF_TATTOO_BITSETS]
	INT iViewedTattoos[MAX_NUMBER_OF_TATTOO_BITSETS]
	INT iCurrentTattoos[MAX_NUMBER_OF_TATTOO_BITSETS]
ENDSTRUCT

FLOAT fCameraPointHeading[MAX_NUMBER_OF_TATTOO_FACING]
