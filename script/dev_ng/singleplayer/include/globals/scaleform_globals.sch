//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	scaleform_globals.sch										//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Globals used for the scaleform helper.						//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

ENUM SCALEFORM_CONTROL_ENUM
	SF_REQUEST = 0,
	SF_WAIT,
	SF_LOADED
ENDENUM


/// PURPOSE: Scripts should create an instance of this struct and set
///    		 the appropriate scaleform filename and if we should block loading
///    		 when the movie is requested
STRUCT SCALEFORM_DATA_STRUCT
	SCALEFORM_INDEX movieID
	TEXT_LABEL_23 filename
	BOOL blockLoad
	INT loadTimer
	SCALEFORM_CONTROL_ENUM stage
ENDSTRUCT

