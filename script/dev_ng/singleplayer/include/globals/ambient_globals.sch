USING "globals_sp_dlc_feature_flags.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ambient_globals.sch
//		AUTHOR			:	Keith
//		DESCRIPTION		:	This is the globals file containing all single player ambient globals.
//							It will be automatically included when Globals.sch is used
//							within any single player scripts.
//
//							Any global scripts included within here should split it's data
//							into saved and unsaved data. The saved data should be packaged
//							up in one struct so that it can be easily added to the Saved
//							Globals struct contained within SP_Globals_Saved.sch.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// Global BOOL to indicate that the player is currently on a rampage mission 
// IS_CURRENTLY_ON_MISSION_TO_TYPE() will be TRUE
BOOL g_bIsOnRampage = FALSE

// Global BOOL used to force shutdown the cable car script externally
BOOL g_bForceNoCableCar = FALSE

// Global BOOL used to force shutdown the ambient ufo script externally
BOOL g_bForceNoAmbientUFO = FALSE

// Global BOOL used by the Chop ambient script to detect when the player is about to ride the cable car
BOOL g_bIsOnCableCar = FALSE

// Global BOOL to allow a debug HOLD state for Random Event worldpoint scripts
BOOL g_bHoldRandomEventForSelection = FALSE

// Global BOOL to disable the military base script
BOOL g_bDisableArmyBase = FALSE

// Global BOOL to disable the Prison script
BOOL g_bDisablePrison = FALSE

// Global BOOL used to check the medals. This has to be used or i'll have to include loads of stuff from achievement controller into mission_stat_public.sch
// This gets set back to false once the achievement_controller has checked it - this is not saved
BOOL g_bCheckSolidGoldBabyAchievement = FALSE

// Disabled calls to REGISTER_NOIR_LENS_EFFECT in noirEffects.sch
BOOL g_bDisableNoirLensEffect = FALSE

// Debug global bools
#IF IS_DEBUG_BUILD
	BOOL g_bDontResetCompletionOnDebugLaunch = FALSE
	BOOL can_cleanup_script_for_debug 					// Used so random scipts can instantly clean up assets for the debug menu
#ENDIF


MODEL_NAMES g_eAmbientModelSuppress[10]
BOOL g_bAmbientModelSuppressVehicle[10]
INT g_iAmbientModelSuppressTimer[10]
BOOL g_bAmbientModelSuppressActive

// Stores the current active safehouse activity
ENUM SAFEHOUSE_ACTIVITY_STATE
	SA_NONE,
	SA_FRANKLIN_SOFA,
	SA_MICHAEL_BEER,
	SA_MICHAEL_SOFA,
	SA_TREVOR_BEER,
	SA_MICHAEL_SHOTS,
	SA_TREVOR_SHOTS
ENDENUM
SAFEHOUSE_ACTIVITY_STATE g_eCurrentSafehouseActivity = SA_NONE
CONST_FLOAT SOFA_TRIGGER_DIST 3.0

// Random event globals
CONST_INT TEMP_DELAY -1
FLOAT     RE_BLIP_SCALE = 0.6

//Time delay for update
CONST_INT CONST_AmbEventUpdate 600 

// Timestamp to limit number of ambient events running
INT g_iLastSuccessfulAmbientLaunchTime

// for stepping through each variation when launched through the debug menu
//INT g_iDrunkDriverEventVariation = 0
//INT g_iGangVictimEventVariation = 0
//INT g_iShopRobberyEventVariation = 0
//INT g_iHitchLiftsEventVariation = 0
//INT g_single_player_basejump = 0
//INT g_iPrisonerLiftEventVariation = 0
//INT g_iSecurityVanEventVariation = 0

#IF IS_DEBUG_BUILD
	TEXT_LABEL_23 g_sLastRandomEventScript = "NULL"
#ENDIF

//BOOL g_bKillAllAmbience = TRUE

BOOL g_bMilitaryWantedLevelSuppressedByMenu

// Activity globals
BOOL g_bActivityLaunchedFromDebugMenu

// Global INT for re_hitch_lift mission
//INT g_iHitchLiftStage = 0

// Tasklist mission globals
//BOOL CONTACT_1_ACTIVATED = FALSE, TREVOR_GOALS_ACTIVATED = FALSE
BOOL CHECKED_OFF_KILLS[3]
BOOL CHECKED_OFF_GANG_PHOTOS[3]
//BOOL SHOOT_PEDS_FROM_BIKE[3]

// Global to force emrgencyCall onto answer phone/engaged
BOOL g_bForceEmergencyCallAnswerphone

INT g_iRECandidateID = -1

// Global enum to check current TV channel
ENUM TV_CHANNEL
	TV_CHANNEL_NONE,
	
	TV_CHANNEL_CNT,
	TV_CHANNEL_WEAZEL,

	TV_CHANNEL_END
ENDENUM

ENUM TV_LOCATION
	TV_LOC_NONE = -1,
	
	TV_LOC_FRANKLIN_LIVING = 0,
	TV_LOC_FRANKLIN_VINEWOOD = 1,
	
	TV_LOC_TREVOR_TRAILER = 2,
	TV_LOC_TREVOR_VENICE = 3,
	
	TV_LOC_MICHAEL_PROJECTOR = 4,
	TV_LOC_JIMMY_BEDROOM = 5,
	
	TV_LOC_GUN_CLUB = 6,
	
	TV_LOC_END = 7
ENDENUM

/// PURPOSE:  Holds global variables to control and query TV functionality. STEVE R. LDS
STRUCT TV_STRUCT

	INT			iTVChannelType				// Stored TV channel type for this TV
	INT			iTVChannelPlaylist			// Stored TV channel playlist for this TV
	BOOL		bIsTVOn						// TRUE if this TV is on and displaying programmes.
	BOOL		bForceStartTV				// Set this to TRUE to force a TV on (May be redundant - see bForceStartAmbientTV)
	BOOL		bForceStopTV				// Set this to TRUE to force a TV off.
	BOOL		bForceStartAmbientTV		// Set this to TRUE to force a TV on and playing TV ambiently.
	BOOL		bAvailableForUse			// TRUE if the TV script is initialised, and ready for use.
	BOOL		bIsPlayerControlled			// TRUE if the player turned on the TV.
	BOOL		bTVControlsDisabled			// Set this to TRUE to disable the TV's controls, so the player can't manipulate it.
	BOOL		bStartPlaylistFromBeginning	// Set this to TRUE to start a playlist from the beginning.
	BOOL		bIsIndestructible			// Set this to TRUE to make a TV indestructible.
	
ENDSTRUCT

// Array of TV structures. One for each TV location.
TV_STRUCT g_TVStruct[TV_LOC_END]
	
// New TV globals
TEXT_LABEL_23 g_sTVPlaybackStartedByThisScript = "NULL"

//BOOL ambient_sprite_blip_active = FALSE

// Forces the current minigame stat tracker to kill itself...can probably go away if/when we move it to use the mission tracker one
BOOL g_bForceKillMGStatTracker 

// B*1186007 - Used to reset Chop's happiness if the player kills him
BOOL g_bPlayerHasKilledChop

// B*1502616 - Used by scripts other than chop.sc to check if Franklin is currently interacting with Chop
BOOL g_bPlayerInteractingWithChop

// B*1334659 - Used to tell the Chop script when a safehouse tutorial is active
BOOL g_bSafehouseTutorialIsActive = FALSE

// B*2019260 - Used to tell the Chop script when the player is in a cinema
BOOL g_bPlayerIsInCinema = FALSE

// ------------------------------------------------------------
//
//		GLOBALS THAT WILL BE SAVED
//
// ------------------------------------------------------------

STRUCT STUNT_JUMPS
	INT iStuntJumpsCompleted
ENDSTRUCT

/*
STRUCT RE_ARREST
	BOOL g_cop_killer
ENDSTRUCT
*/

//Kevin Bolt's Garage Door Variables.
INT 	g_propGarDoorBit                // A bit to store locked state of various garage doors for script control.

//Prostitute globals
//═══════════════════════════════════════════════════════════════════════════════
//═════════════════════════════╡ Unsaved data ╞══════════════════════════════════
//═══════════════════════════════════════════════════════════════════════════════

BOOL     g_bPlayerHasActiveProstitute
BOOL	 g_bPlayerCanCallProstitute
BOOL	 g_bIsProstituteAICustomerActive
//BOOL	 g_bIsProstitutePimpActive      //In a group of prostitutes, keep track if any of them are talking to a pimp
FLOAT    g_fDistToClosestProstitute     // When near more then one prostitute this will keep track of the closest one
THREADID g_threadIDOfClosestProstitute

ENUM PROSTITUTE_UNIQUE_ENUM
	PUN_UNDEFINED = -1,
	PUN_BABS	=	0,		
	PUN_DANA,				
	PUN_LIZZIE,				
	PUN_AMANDA,				
	PUN_ASHLEY,				
	PUN_KRISTEN,			
	PUN_SASHA,				
	PUN_JACQUELINE,
	
	MAX_UNIQUE_PROSTITUTES		//8
ENDENUM

ENUM UNIQUE_PROSTITUTE_DRAWABLE_COMPONENTS
	PRO_COMP_HEAD = 0,		//only care about the head texture
	PRO_COMP_HAIR,			//hair texture
//	PRO_COMP_TORSO,
//	PRO_COMP_LEG,
	
	NUM_PRO_COMPONENTS
	
ENDENUM

ENUM UNIQUE_PROSTITUTE_TEXTURE_VARIATIONS
	PRO_TEXT_VAR_HEAD = 0,		//only care about the head texture
	PRO_TEXT_VAR_HAIR,			//hair texture

	NUM_PRO_TEXTURE_VARIATIONS
	
ENDENUM

CONST_INT	CONST_iMaxNumStoryLines	6		//Num of lines to trigger in D*

//Peyote Animal Pickups
ENUM AnimalPickup
	AP_INVALID = -1,

	AP_LOWLAND_BOAR,			
	AP_LOWLAND_CAT,
	AP_LOWLAND_COW,			
	AP_LOWLAND_COYOTE,
	AP_LOWLAND_DEER,
	AP_LOWLAND_HUSKY,
	AP_LOWLAND_MTLION,		
	AP_LOWLAND_PIG,
	AP_LOWLAND_POODLE, 		
	AP_LOWLAND_PUG, 			
	AP_LOWLAND_RABBIT, 		
	AP_LOWLAND_RETRIEVER,	
	AP_LOWLAND_ROTTWEILER,
	AP_LOWLAND_SHEPHERD,
	AP_LOWLAND_WESTY,
	
	AP_HIGHLAND_CHICKENHAWK,						
	AP_HIGHLAND_CORMORANT,	
	AP_HIGHLAND_CROW,
	AP_HIGHLAND_HEN,
	AP_HIGHLAND_PIGEON,		
	AP_HIGHLAND_SEAGULL,
				
	AP_UNDERWATER_DOLPHIN,		
	AP_UNDERWATER_FISH,			
	AP_UNDERWATER_KILLERWHALE,	  	
	AP_UNDERWATER_SHARKHAMMER, 	
	AP_UNDERWATER_SHARKTIGER,	
	AP_UNDERWATER_STINGRAY, 	
	
	AP_SECRET_SASQUATCH,		
	AP_SECRET_CHOP,	      			// 28
	AP_UNDERWATER_HUMPBACKWHALE, 	
	AP_COUNT
ENDENUM

ENUM AnimalPickupType
	APT_INVALID = -1,

	APT_LOWLAND = 0,
	APT_HIGHLAND,
	APT_UNDERWATER,
	APT_SECRET,
	
	APT_COUNT
ENDENUM

INT g_iAnimalShockingEvent = 0
INT g_iAnimalWaterTimer = 0
BOOL g_bAnimalControllerFakeDeath = FALSE

#IF IS_DEBUG_BUILD
	BOOL g_bDebugPeyoteResetProgression = FALSE
	BOOL g_bDebugPeyotePickupLabels[APT_COUNT]
	BOOL g_bDebugPeyotePickupBlips[APT_COUNT]
	AnimalPickup g_eDebugPeyoteAnimalLowland = AP_INVALID
	AnimalPickup g_eDebugPeyoteAnimalHighland = AP_INVALID
	AnimalPickup g_eDebugPeyoteAnimalUnderwater = AP_INVALID
	AnimalPickup g_eDebugPeyoteAnimalSecret = AP_INVALID
	AnimalPickupType g_eDebugPeyoteType = APT_INVALID
	INT g_iDebugPeyoteIndex = -1
	INT g_iDebugPeyoteBlockControllerUntil = 0
#ENDIF

//═══════════════════════════════════════════════════════════════════════════════
//════════════════════════════╡ Data to be saved ╞═══════════════════════════════
//═══════════════════════════════════════════════════════════════════════════════



STRUCT ProstituteSaved

	INT 		iNumTimesEncounteredByPlayer[NUM_OF_PLAYABLE_PEDS]
	INT			iComponentDrawableVariation[NUM_PRO_COMPONENTS]
	INT			iComponentTextureVariation[NUM_PRO_TEXTURE_VARIATIONS]
	INT			iModelType
ENDSTRUCT
//End Prostitute Data

CONST_INT MAX_HUNT_CHALLENGE		3
CONST_INT NUMBER_OF_LETTER_SCRAPS   50  // Hidden packages
CONST_INT NUMBER_OF_SPACESHIP_PARTS 50

// Hidden packages globals
STRUCT GLOBAL_SCRAP_DATA
	INT  iScrap0to31
	INT  iScrap32to63
	BOOL bMissionActive
ENDSTRUCT

ENUM NOIR_EFFECTS_STATUS
	NOIR_OFF,
	NOIR_FILTER_0,
	NOIR_FILTER_1
ENDENUM

STRUCT g_ambientSavedData

//	RE_ARREST 	ReArrest
	STUNT_JUMPS	stuntJumps
	
	ProstituteSaved	sProstituteData[MAX_UNIQUE_PROSTITUTES]			//These are the 8 UNIQUE BACKSTORY Prostitues
	
	INT			iNumTimesPlayerPaidForProstituteServices[3]			//B*1416861 - This is all time prostitutes
	INT 		iChopHintsDisplayed
	
	INT 		iWardrobeHintsDisplayed
	
	INT			iBridgesFlownUnderFlags
	INT			iBridgesFlownUnderFlags2
	INT			iBridgesFlownUnderFlags3
	
	INT			iWildlifePhotographsFlags
	
	INT			iChaseHelpTextDisplayed_mission
	INT			iChaseHelpTextDisplayed_RC
	INT			iChaseHelpTextDisplayed_RE

	INT 		iEpsilonCarStage						// Epsilon 3
	INT			iEpsilonLastEmailSentDay				// Epsilon 3
	INT 		iEpsilonRobesHours						// Epsilon 5
	INT			iEpsilonRobesDeliveryTime				// Epsilon 5 (Expected Robes Delivery Time)
	FLOAT 		fDistRunInDesert						// Epsilon 7
	INT 		iForSaleSignDestroyedFlags				// Josh 1
	INT 		iHighScoreFreeMode						// Ambient Hunting
	INT			iLowestGoldTime							// Ambient Hunting
	INT			iChallengeRankFreeMode[MAX_HUNT_CHALLENGE]
	BOOL		bChallengeHelpDisplayed					// Ambient Hunting
	TIMEOFDAY	todHuntedWeekExp						// Ambient Hunting
	
	INT			iVehiclesRepairedByFranklin
	INT			iVehiclesRepairedByMichael
	INT			iVehiclesRepairedByTrevor
	
	BOOL		bTaxiHailingHelpDisplayed
	INT 		iTaxiEnterPromptDisplayed
	
	GLOBAL_SCRAP_DATA sLetterScrapData					// Hidden packages (used by Chop)
	GLOBAL_SCRAP_DATA sSpaceshipPartData
	
	INT			iGetawayState
	
	BOOL 		bWebsiteSubscription[3]					// Website subscription for Reality Mill
	
	INT			iMurderMysteryFlags						// Murder mystery easter egg
	NOIR_EFFECTS_STATUS		eNoirEffectState			// Saves the type of filter to be used by the noir effects header
	INT			iPhotographyMonkeyFlags[2]				// Bit flags for photographyMonkey
	
	INT			iPeyotePickupOfTypeFound[APT_COUNT]
	INT			iPeyoteAnimalSeen
	BOOL		bPeyoteProgressionComplete
ENDSTRUCT

// Restricted Area Globals
CONST_INT 	  TOTAL_AC_AREAS  9 //ENUM_TO_INT(AC_END)
INT 		  g_iRestrictedAreaBitSet
TEXT_LABEL_23 g_sAreaSuppressedByThisScript[TOTAL_AC_AREAS]
BOOL          g_bPlayerIsActiveInRestrictedArea[TOTAL_AC_AREAS]

// Emergency Calls Globals
BOOL          g_bEmergencyCallsSuppressed = FALSE
TEXT_LABEL_31 g_sEmergencyCallsSuppressedByThisScript = "NULL"

BOOL          g_bEmergencyCallPriorityIsHigh = FALSE

// Fly Under Bridges
INT g_bCompletedBridgesCount

// Wildlife Photography
INT g_bCompletedAnimalPhotosCount

// Monkey Photography None Saved
#IF IS_DEBUG_BUILD
	INT g_iDebugMosaicToJumpTo = -1
	BOOL g_bDebugJumpToMosaic = FALSE
	BOOL g_bDebugCompleteAllMosaics = FALSE
#ENDIF

// Gate Controller Globals
BOOL g_bDocksBackGatesIgnoreTrevorsUniform = FALSE

CONST_INT ciSINGLE_PLAYER_SJS_GROUP	  0
CONST_INT ciFREEMODE_PLAYER_SJS_GROUP 1
CONST_INT ciUGC_PLAYER_SJS_GROUP 2
CONST_INT ciRC_TIME_TRIALS_SJS_GROUP 3

// Altrusit Cult Globals
BOOL g_bDeliverCurrentGroupMembersToCult = FALSE
BOOL g_bCurrentGroupMembersHaveBeenDeliveredToCult = FALSE
BOOL g_bAltruistCultHelpHasDisplayed = FALSE
BOOL g_bAltruistCultDialogueHasPlayed = FALSE
BOOL g_bAltruistCultFinalDialogueHasPlayed = FALSE
BOOL g_bAltruistCultExitMusicPlayed = FALSE
//BOOL g_bThisPedHasBeenDeliveredToCult[7]

// Pickup controller query system
ENUM PICKUP_QUERY
	PC_QUERY_INACTIVE,
	PC_QUERY_REQUEST,
	PC_QUERY_RESULT,
	PC_QUERY_RESET
ENDENUM

ENUM QUERY_TYPE
	PC_QUERY_TYPE_ARMOUR,
	PC_QUERY_TYPE_HEALTH,
	PC_QUERY_TYPE_WEAPON,
	PC_QUERY_TYPE_INVALID
ENDENUM

PICKUP_QUERY g_ePickupQuery = PC_QUERY_INACTIVE
QUERY_TYPE	 g_eQueryType = PC_QUERY_TYPE_INVALID
INT			 g_iPickupIndex
BOOL		 g_bPickupCollected

// Cheat controller
INT			 g_iBitsetCheatsCurrentlyDisabled			// Bitset which tells the cheat controller which cheats are disabled this frame.
INT			 g_iBitsetCheatsCurrentlyActive				// Bitset which keeps track of which cheats are currently active.
INT			 g_iBitsetCheatsUsedThisSession				// Bitset which keeps track of which cheats were used this session.
INT			 g_iCheatTriggerBitset						// Bitset for triggering cheats, e.g. from phone.
MODEL_NAMES	 g_eCheatVehicleModelName					// Model name of phone cheat vehicle.
BOOL		 g_bHasAnyCheatBeenUsed						// Bool for the stats guys to check if a cheat has been used. Must be reset to FALSE when starting checking.

STRUCT PHYSICS_PERF_STRUCT
	VECTOR vInput
	FLOAT fInput
ENDSTRUCT

//Are the vending machines active. 
BOOL g_bVendingMachinesActive = TRUE
BOOL g_bCurrentlyUsingVendingMachine = FALSE // B*1567131 Prevent Chop running off when using a vending machine

BOOL g_bCurrentlyUsingTelescope = FALSE // Prevent Chop running off when using a telescope

BOOL g_bCurrentlyBuyingProperty = FALSE // Prevent Chop running off when showing the buying property cutscene

//Peyote conversation bitsets
INT			 g_iBitsetPeyoteFranklin			
INT			 g_iBitsetPeyoteMichael				
INT			 g_iBitsetPeyoteTrevor				
INT			 g_iBitsetPeyoteFranklinSecond			
INT			 g_iBitsetPeyoteMichaelSecond					
INT			 g_iBitsetPeyoteTrevorSecond	

//Ped brain timer.
CONST_INT	PED_BRAIN_LOAD_COOLDOWN		10000
INT			g_iPedBrainLastLoaded		= -PED_BRAIN_LOAD_COOLDOWN


// Grass cull spheres
CONST_INT	NUMBER_AVAILABLE_CULLSPHERES		8	

// Used to stop ambient assets loading while the player is moving at speed.
// Helps relieve pressure on the streaming engine.
CONST_INT	AMBIENT_LOAD_PLAYER_SPEED_CUTOFF	1369 // 37^2 m/s

//Previous number of completed stunt jumps
INT g_iPreviousNumberOfStuntJumps = -1

INT g_iLuxeLatestScriptInstanceID = -1
INT g_iLuxeBootVerifyBS
BOOL g_bInLuxeVehicle = FALSE // True when the local player is inside a luxe vehicle.

BOOL g_BInYachtJacuzzi = FALSE // True when player is using the Jacuzzi
BOOL g_bInRadioLocate = FALSE
BOOL g_bRadioActTriggered = FALSE 
BOOL g_bInTVLocate = FALSE 
BOOL g_bCanPlayerUseSafehouseActivity = FALSE
BOOL g_bUsingBarmaidActivity = FALSE

ENUM OFFICE_SEAT_ACT_STATE
OFFICE_SEAT_INIT = 0,
OFFICE_SEAT_LOCATECHECK,
OFFICE_SEAT_PROMPT,
OFFICE_SEAT_REQUEST,
OFFICE_SEAT_APPROACH,
OFFICE_SEAT_RUN_ENTER_ANIM,
OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM,
OFFICE_SEAT_CHECK_BASE_ANIM,
OFFICE_SEAT_RUNANIM,
OFFICE_SEAT_EXIT_TURN,
OFFICE_SEAT_ENTER_TURN,
OFFICE_SEAT_EXIT,
OFFICE_SEAT_CLEANUP,
OFFICE_SEAT_EXIT_INTERRUPT,
OFFICE_SEAT_TV_EXIT_SEAT, 
OFFICE_SEAT_PUT_ON_CLOTHES,
OFFICE_SEAT_PLAY_COMPUTER_ENTER,
OFFICE_SEAT_PLAY_COMPUTER_IDLE,
OFFICE_SEAT_PLAY_COMPUTER_EXIT,
OFFICE_SEAT_SWITCH_TO_GAME_ANIM,
OFFICE_SEAT_SWITCH_FROM_GAME_ANIM_TO_SEAT,
OFFICE_SEAT_SWITCH_FROM_GAME_ANIM_TO_STAND,
OFFICE_SEAT_PLAY_LAPTOP_INTERNET_ENTER,
OFFICE_SEAT_PLAY_GAME_STANDING_ENTER,
OFFICE_SEAT_SPAWN_ACTIVITY,
OFFICE_SEAT_CLEANUP_WAIT
,OFFICE_SEAT_RESET_AFTER_QUITTING_SMV
ENDENUM

OFFICE_SEAT_ACT_STATE g_OfficeArmChairState = OFFICE_SEAT_INIT

INT g_iDayOfWeek

BOOL g_b_ScriptMPBlock = FALSE

/*														*\
*********** Global cache for specific events *************
				See event_listener.sc
\*														*/
STRUCT EVENT_CACHE_ITEM
	INT iEventName
	INT iFrame
	INT iScriptNameHash
	
#IF IS_DEBUG_BUILD
	INT iDebugCount
#ENDIF //IS_DEBUG_BUILD
ENDSTRUCT

CONST_INT EVENT_CACHE_SIZE 20

STRUCT EVENT_CACHE
	EVENT_CACHE_ITEM events[EVENT_CACHE_SIZE]
	INT iLength
ENDSTRUCT
EVENT_CACHE g_eventCache

#IF IS_DEBUG_BUILD
STRUCT SCRIPT_EVENT_FATAL_ERROR_DATA
	EVENT_NAMES eEvent
	INT iFrame
	TEXT_LABEL_31 txtScriptName 
	TEXT_LABEL_23 txt23FatalPlayer
ENDSTRUCT
STRUCT GLOBAL_NET_FATAL_ERROR_DATA
	PLAYER_INDEX iPlayerIndex
	SCRIPT_EVENT_FATAL_ERROR_DATA data
	INT iFrameReceived
ENDSTRUCT
GLOBAL_NET_FATAL_ERROR_DATA g_netFatalError
BOOL g_bReceivedNetFatalError
BOOL g_bBroadcastNetFatalError
#ENDIF //IS_DEBUG_BUILD
/********************************************************/
