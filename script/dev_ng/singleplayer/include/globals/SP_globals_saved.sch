
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   SP_globals_saved.sch
//      AUTHOR          :   Keith
//      DESCRIPTION     :   This script is essentially a giant struct containing other
//                          structs defined by other globals files. All variables contained
//                          within this struct will be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "respawn_globals.sch"
USING "player_ped_globals.sch"
USING "heist_globals.sch"
USING "stripclub_globals.sch"
USING "flightschool_globals.sch"
USING "mission_globals.sch"
USING "flow_globals.sch"
USING "social_globals.sch"
USING "shop_globals.sch"
USING "charsheet_global_definitions.sch"
USING "comms_control_globals.sch"
USING "building_globals.sch"
USING "golf_globals.sch"
USING "sptt_globals.sch"
USING "Taxi_globals.sch"
USING "towing_globals.sch"
USING "Assassination_globals.sch"
USING "ShootRange_globals.sch"
USING "tennis_globals.sch"
USING "trafficking_globals.sch"
//USING "website_globals.sch" // included within vehicle_gen_globals.sch
USING "vehicle_gen_globals.sch"
USING "randomChar_globals.sch"
USING "Offroad_globals.sch"
USING "triathlon_globals.sch"
USING "flow_help_globals.sch"
USING "CompletionPercentage_globals.sch"
USING "replay_globals.sch"
USING "basejump_globals.sch"
USING "shrink_globals.sch"
USING "shoprobberies_globals.sch"
USING "specialPed_globals.sch"
USING "email_globals.sch"
USING "feed_globals.sch"
USING "darts_globals.sch"
USING "sea_race_globals.sch"
USING "street_race_globals.sch"
USING "rampage_globals.sch"
USING "snapshot_globals.sch"
USING "BailBond_globals.sch"
USING "finance_globals.sch"
USING "ambient_globals.sch"
USING "properties_globals.sch"
USING "random_event_globals.sch"
USING "country_race_globals.sch"
USING "director_mode_globals.sch"

// All structs containing variables that should be saved should be contained within this struct
STRUCT g_structSavedGlobals
	// The version number of this save.
	FLOAT 					fSaveVersion

    // All saved data relating to the heist missions.
    HeistSaved 				sHeistData
    
    // All saved data for strip clubs
    StripClubSaved 			sStripClubData[COUNT_OF(STRIP_CLUB_PLAYER_CHAR_ENUM)]
    
    // All saved data for the shops
    ShopDataSaved 			sShopData
    
    // All saved data for flight school
    FlightSchoolSaved 		sFlightSchoolData[NUM_OF_PLAYABLE_PEDS]
    
	// All saved data for rampages
	RampageDataSaved 		sRampageData
	
    // All saved data for player
    PlayerDataSaved 		sPlayerData
    
    // All saved data for respawn
    RespawnDataSaved 		sRespawnData
    
    // All the saved data for buildings
    BuildingDataSaved 		sBuildingData
    
    // All saved data for the communication controller
    CommsControlSaved 		sCommsControlData
	
	// All saved data for the script CodeID controller.
	CodeControlSaved 		sCodeControlData
    
    // All core mission flow saved data
    FLOW_SAVED_VARS 		sFlow
	
	// Custom flow saved data
	FlowCustomSaved			sFlowCustom

    // All saved data for ambient missions
    g_ambientSavedData  	sAmbient

    // All saved data for Completion Percentage tracking.
    g_CompletionPercentageSavedData sCompletionPercentageData

    // All saved data for Cellphone Setting save data.
    g_CellphoneSettingsSavedData sCellphoneSettingsData

    // All saved data for Cellphone Text Message save data.
    g_TextMessageSavedData 	sTextMessageSavedData

    // All saved data for Gallery Image saved data
    g_GalleryImageSavedData sGalleryImageSavedData

    // All saved data for friend sheets
    g_FriendsSavedData 		sFriendsData

    // All Family Character saved data
    g_FamilySavedData 		sFamilyData

    // All Player Switch Scene saved data
    g_PlayerSceneSavedData 	sPlayerSceneData
    
    // All Random Character saved data
    g_structSavedVarsRC 	sRandomChars
	
	// all saved and global data for basejumps
	BasejumpSavedData		sBasejumpData
    
	// All saved data for Darts
	DartsDataSaved			sDartsData
    
	//  All saved data for Golf
    GolfDataSaved           sGolfData
    
    // All saved data for offroad racing
    OffroadSaved            sOffroadData
        
    // All saved data for Stunt Plane Races.
//  SPRDataSaved            sSPRData
	SPTTDataSaved           sSPTTData
    
    // All the saved data for taxi missions
    TaxiMissionSaved        sTaxiData

    // All saved data for modern shooting range
    ShootRangeDataSaved     sRangeData[3]
    
	// All saved data for shrink
    ShrinkDataSaved     	sShrinkData
	
    // All the saved data for taxi missions
    AssassinDataSaved       sAssassinData
    
    // All saved data for towing missions
    TowingDataSaved         sTowingData
    
	// Gun trafficking minigame saved data
    TraffickingDataSaved    sTraffickingData
    
    // Triathlon saved data
    TriathlonDataSaved      sTriathlonData
    
    // Tennis save data
    TennisDataSaved         sTennisData
	
	// Shop Robberies save data
	ShopRobberiesDataSaved	sShopRobberiesData
    
	// Special ped save data.
	SpecialPedDataSaved		sSpecialPedData
    
    // Social integration saved data
    SocialDataSaved 		sSocialData
    
    // Flow Help Queue saved data
    FlowHelpSaved 			sFlowHelp 
    
    // Finance System Stored Data
    FinanceDataSaved 		sFinanceData
	
	// Email system stored data
	EmailDataSaved 			sEmailData
	
	// Properties system stored data
	PropertySaved			sPropertyData
	
	// Sea Race stored data
	SeaRaceSaved 			sSeaRaceData
	
	// Street Race stored data
	StreetRaceSaved 		sStreetRaceData
	
	// Bail Bond stored data
	BailBondSaveData 		sBailBondData
	
	// Random events save data
	RandomEventSaved 		sRandomEventData
	
	// Country race stored data
	CountryRaceDataSaved	sCountryRaceData
	
	// Feed saved data
//	FeedDataSaved 			sFeedData
	
	// Repeat Play Data (used in repeat play and save anywhere / quick save)
	REPEAT_PLAY_STRUCT 		sRepeatPlayData // Player pos and vehicle data 
	
	//sp buyable vehicle save data
	BuyableVehicleSaved  	sBuyableVehicleSavedData

	#IF FEATURE_SP_DLC_DIRECTOR_MODE
		// All saved data for next-gen only Director Mode
		DirectorModeSaved		sDirectorModeData
	#ENDIF
	
    // All saved data for character sheets.
	// It used to be that this was the largest struct and it was 
	// breaking code's tracking of variable offsets within structures (see 2172009).
    g_CharSheetSavedData 	sCharSheetData

    // Vehicle generation saved data
	// This struct is now the largest one. Keep it at the end.
	//	This helps us to avoid the problem of the struct's starting offset being greater than code's psoSchemaMemberData::MAX_OFFSET (262144)
    VehicleGenDataSaved 	sVehicleGenData
ENDSTRUCT

// *** Place no more variables after this ENDSTRUCT - they will not get saved

// This is the struct containing all saved variables.
// startup.sc sets this up as the PersistentGlobals struct.
g_structSavedGlobals g_savedGlobals

