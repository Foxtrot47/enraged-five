//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 30/05/12			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						    Respawn Cleanup Header								│
//│																				│
//│			Contains definitions for custom cleanup blocks that can be			│
//│			set to run on respawn. Useful for missions that need to queue		│
//│			up cleanup events for after the screen has faded on death/arrest.	│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"

ENUM RESPAWN_CLEANUP_ID
	RCI_NONE = -1,
	
	//Add new cleanup IDs to the list below.
	RCI_ARMENIAN2,
	RCI_BIGSCORE2B,
	RCI_EXILE2,
	RCI_FAMILY2,
	RCI_FBI3,
	RCI_LAMAR1,
	RCI_MICHAEL1,
	RCI_PROLOGUE,
	RCI_TREVOR2
ENDENUM

RESPAWN_CLEANUP_ID g_eTriggerRCI = RCI_NONE
