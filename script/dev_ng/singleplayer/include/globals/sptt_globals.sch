// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	SPTT_Globals.sch
//		AUTHOR			:	Nicholas Zippmann, Troy Schram
//		DESCRIPTION		:	Stunt Plane Time Trials - Global data file
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// SPR Max Races.
CONST_INT SPTT_RACE_MAX 8

ENUM SPTT_BITFLAGS
	SPTT_BITFLAG_LaunchedViaDebug = 1
ENDENUM

ENUM SPTT_STATUS_ENUM
	SPTT_LOCKED = 0,
	SPTT_UNLOCKED,
	SPTT_NEW_RECORD,
	
	NUMBER_OF_SPR_Race_STATUSES
ENDENUM

ENUM SPTT_GOAL_BITS
	SPTTG_0_timeTaken 			= 0,		//BIT0,
	SPTTG_1_damageTaken 			= 1,		//BIT1,
	SPTTG_2_distanceFromTarget	= 2,		//BIT2,
	SPTTG_3_checkpointsPassed		= 3,		//BIT3,
	SPTTG_4_racePosition			= 4,		//BIT4,
	
	NUMBER_OF_SPTT_GOALS	=  5	//BIT5
ENDENUM

ENUM SPTT_Races_ENUM	
	SPTT_BridgeBinge = 0,
	SPTT_Vinewood_Flyby,
	SPTT_Advanced_Race01,
	SPTT_AirportFlyby,
	SPTT_Altitude,
	NUMBER_OF_SPTT_COURSES
ENDENUM

SPTT_Races_ENUM		g_current_selected_SPTT_Race					= SPTT_BridgeBinge
//===============================================================================
//============================| Data to be saved |===============================
//===============================================================================

STRUCT SPTT_STRUCT
	TEXT_LABEL					name
	SPTT_STATUS_ENUM			status
	INT							goals
	INT							preReq
	INT							medalscore	
ENDSTRUCT

// SPTT Race Data.
STRUCT SPTTDataRace
	INT 			iBestRank[SPTT_RACE_MAX]
	FLOAT 			fBestTime[SPTT_RACE_MAX]
	
	SPTT_STRUCT		structSPTT[NUMBER_OF_SPTT_COURSES]	
ENDSTRUCT

// SPR Saved Data.
STRUCT SPTTDataSaved
//	SPTTDataRace 	PlaData	// Stunt Plane Races.
	INT 			iBestRank[NUMBER_OF_SPTT_COURSES]
	FLOAT 			fBestTime[NUMBER_OF_SPTT_COURSES]
	
	SPTT_STRUCT		CourseData[NUMBER_OF_SPTT_COURSES]
	INT iFlags
ENDSTRUCT

// END OF FILE! DO NOT ADD ANYTHING BELOW THIS LINE!
