///Automatically generated header.

USING "stats_enums.sch"

/// List of company enums.

	ENUM BAWSAQ_COMPANIES
		BS_CO_AMU,// Ammu-Nation// Online
		BS_CO_BDG,// Badger// Online
		BS_CO_BOL,// BankOfLiberty// Offline
		BS_CO_BFA,// BF// Online
		BS_CO_BIN,// Binco// Online
		BS_CO_BTR,// BitterSweet// Online
		BS_CO_BLE,// Bleeter// Online
		BS_CO_BRU,// Brute// Online
		BS_CO_CNT,// CNT// Online
		BS_CO_CRE,// Crevis// Online
		BS_CO_DGP,// DailyGlobe// Online
		BS_CO_WAP,// Dept.WaterandPower// Online
		BS_CO_FAC,// Facade// Online
		BS_CO_FLC,// Fleeca// Offline
		BS_CO_HJK,// Hijak// Offline
		BS_CO_FRT,// Fruit// Online
		BS_CO_AUG,// AuguryInsurance// Offline
		BS_CO_BUL,// BULLHEAD// Offline
		BS_CO_HAF,// Hammerstein&Faust// Offline
		BS_CO_SSS,// Slaughter,Slaughter&Slaughter// Offline
		BS_CO_LSC,// LosSantosCustoms// Online
		BS_CO_LST,// LSTransport// Online
		BS_CO_LTD,// LTDOil// Online
		BS_CO_MAI,// Maibatsu// Online
		BS_CO_MAZ,// MazeBank// Offline
		BS_CO_PKW,// Peckerwood// Online
		BS_CO_ARK,// AnimalArk// Offline
		BS_CO_PIS,// Pisswasser// Online
		BS_CO_PON,// Ponsonbys// Online
		BS_CO_RAI,// Raine// Offline
		BS_CO_RON,// RonOil// Online
		BS_CO_SHT,// Schyster// Online
		BS_CO_SPU,// Sprunk// Online
		BS_CO_TNK,// Tinkle// Online
		BS_CO_WIW,// WIWANG// Online
		BS_CO_UMA,// Ubermacht// Online
		BS_CO_VAP,// Vapid// Online
		BS_CO_VOM,// VomFeuer// Online
		BS_CO_WZL,// Weazel// Online
		BS_CO_WIZ,// Whiz// Online
		BS_CO_DEB,// Debonaire// Offline
		BS_CO_ZIT,// Zit// Online
		BS_CO_SHK,// Shark// Online
		BS_CO_MOL,// BettaPharmaceuticals// Online
		BS_CO_PMP,// PumpnRun// Online
		BS_CO_GOT,// GrainOfTruth// Online
		BS_CO_EYE,// Eyefind// Online
		BS_CO_EMU,// AirEmu// Offline
		BS_CO_BEN,// BeanMachine// Offline
		BS_CO_GOL,// GoldCoast// Offline
		BS_CO_BOM,// BobMulet// Offline
		BS_CO_BGR,// BurgerShot// Offline
		BS_CO_CLK,// CluckinBell// Offline
		BS_CO_BAN,// CoolBeans// Offline
		BS_CO_DOP,// DollarPills// Offline
		BS_CO_ECL,// eCola// Offline
		BS_CO_FUS,// FlyUS// Offline
		BS_CO_GAS,// GastroBand// Offline
		BS_CO_GOP,// GoPostal// Offline
		BS_CO_GRU,// GruppeSechs// Offline
		BS_CO_KRP,// Krapea// Offline
		BS_CO_LFI,// LifeInvader// Offline
		BS_CO_MAX,// MaxRenda// Offline
		BS_CO_POP,// PostOP// Offline
		BS_CO_PRO,// ProLaps// Offline
		BS_CO_RWC,// Redwood// Offline
		BS_CO_RIM,// RichardsMajestic// Offline
		BS_CO_TBO,// TacoBomb// Offline
		BS_CO_UPA,// UpandAtom// Offline
		BS_CO_VAG,// Vangelico// Offline
		BS_CO_UNI,// VanillaUnicorn// Offline
		BS_CO_HVY,// HVYIndustries// Online
		BS_CO_LOG,// Logger// Offline
		BS_CO_MER,// Merryweather// Offline
		BS_CO_RA1,// WorldwideFM// Offline
		BS_CO_RA2,// RadioLosSantos// Offline
		BS_CO_SHR,// Shrewsbury// Online
		BS_CO_HAL,// HawkAndLittle// Online
		BS_CO_MOR,// MorsMutualInsurance// Offline
		BS_CO_BSS_PRI,// Bilkinton// Offline
		BS_CO_TOTAL_LISTINGS
	ENDENUM

///List of modifier enums.

	ENUM BSMF_TYPES
		BSMF_SM_BRVECDES_FOR_BFA,// BrandedVehiclesDestroyed : used by BF
		BSMF_SM_BRVECDES_FOR_BRU,// BrandedVehiclesDestroyed : used by Brute
		BSMF_SM_BRVECDES_FOR_HJK,// BrandedVehiclesDestroyed : used by Hijak
		BSMF_SM_BRVECDES_FOR_LSC,// BrandedVehiclesDestroyed : used by LosSantosCustoms
		BSMF_SM_BRVECDES_FOR_LST,// BrandedVehiclesDestroyed : used by LSTransport
		BSMF_SM_BRVECDES_FOR_LTD,// BrandedVehiclesDestroyed : used by LTDOil
		BSMF_SM_BRVECDES_FOR_MAI,// BrandedVehiclesDestroyed : used by Maibatsu
		BSMF_SM_BRVECDES_FOR_RON,// BrandedVehiclesDestroyed : used by RonOil
		BSMF_SM_BRVECDES_FOR_SHT,// BrandedVehiclesDestroyed : used by Schyster
		BSMF_SM_BRVECDES_FOR_UMA,// BrandedVehiclesDestroyed : used by Ubermacht
		BSMF_SM_BRVECDES_FOR_VAP,// BrandedVehiclesDestroyed : used by Vapid
		BSMF_SM_BRVECDES_FOR_HVY,// BrandedVehiclesDestroyed : used by HVYIndustries
		BSMF_SM_VECBUY_FOR_BFA,// VehiclesOfTypeBought : used by BF
		BSMF_SM_VECBUY_FOR_BRU,// VehiclesOfTypeBought : used by Brute
		BSMF_SM_VECBUY_FOR_HJK,// VehiclesOfTypeBought : used by Hijak
		BSMF_SM_VECBUY_FOR_MAI,// VehiclesOfTypeBought : used by Maibatsu
		BSMF_SM_VECBUY_FOR_SHT,// VehiclesOfTypeBought : used by Schyster
		BSMF_SM_VECBUY_FOR_UMA,// VehiclesOfTypeBought : used by Ubermacht
		BSMF_SM_VECBUY_FOR_VAP,// VehiclesOfTypeBought : used by Vapid
		BSMF_SM_VECBUY_FOR_HVY,// VehiclesOfTypeBought : used by HVYIndustries
		BSMF_SM_DISDRIV_FOR_BFA,// DistanceDrivenInType : used by BF
		BSMF_SM_DISDRIV_FOR_BRU,// DistanceDrivenInType : used by Brute
		BSMF_SM_DISDRIV_FOR_HJK,// DistanceDrivenInType : used by Hijak
		BSMF_SM_DISDRIV_FOR_LST,// DistanceDrivenInType : used by LSTransport
		BSMF_SM_DISDRIV_FOR_MAI,// DistanceDrivenInType : used by Maibatsu
		BSMF_SM_DISDRIV_FOR_SHT,// DistanceDrivenInType : used by Schyster
		BSMF_SM_DISDRIV_FOR_UMA,// DistanceDrivenInType : used by Ubermacht
		BSMF_SM_DISDRIV_FOR_VAP,// DistanceDrivenInType : used by Vapid
		BSMF_SM_DISDRIV_FOR_HVY,// DistanceDrivenInType : used by HVYIndustries
		BSMF_SM_VECMOD_FOR_AUG,// VehicleModification : used by AuguryInsurance
		BSMF_SM_VECMOD_FOR_LSC,// VehicleModification : used by LosSantosCustoms
		BSMF_SM_VECSTOL_FOR_BFA,// VehicleOfTypeStolen : used by BF
		BSMF_SM_VECSTOL_FOR_BRU,// VehicleOfTypeStolen : used by Brute
		BSMF_SM_VECSTOL_FOR_HJK,// VehicleOfTypeStolen : used by Hijak
		BSMF_SM_VECSTOL_FOR_LST,// VehicleOfTypeStolen : used by LSTransport
		BSMF_SM_VECSTOL_FOR_MAI,// VehicleOfTypeStolen : used by Maibatsu
		BSMF_SM_VECSTOL_FOR_SHT,// VehicleOfTypeStolen : used by Schyster
		BSMF_SM_VECSTOL_FOR_UMA,// VehicleOfTypeStolen : used by Ubermacht
		BSMF_SM_VECSTOL_FOR_VAP,// VehicleOfTypeStolen : used by Vapid
		BSMF_SM_VECSTOL_FOR_HVY,// VehicleOfTypeStolen : used by HVYIndustries
		BSMF_SM_VECDMG_FOR_BFA,// DamageDoneToVehicleType : used by BF
		BSMF_SM_VECDMG_FOR_BRU,// DamageDoneToVehicleType : used by Brute
		BSMF_SM_VECDMG_FOR_HJK,// DamageDoneToVehicleType : used by Hijak
		BSMF_SM_VECDMG_FOR_MAI,// DamageDoneToVehicleType : used by Maibatsu
		BSMF_SM_VECDMG_FOR_SHT,// DamageDoneToVehicleType : used by Schyster
		BSMF_SM_VECDMG_FOR_UMA,// DamageDoneToVehicleType : used by Ubermacht
		BSMF_SM_VECDMG_FOR_VAP,// DamageDoneToVehicleType : used by Vapid
		BSMF_SM_VECDMG_FOR_HVY,// DamageDoneToVehicleType : used by HVYIndustries
		BSMF_SM_VECPEDKIL, //PeopleKilledWithVehicle
		BSMF_SM_WEPBUY_FOR_SHR,// WeaponTypeBought : used by Shrewsbury
		BSMF_SM_WEPBUY_FOR_HAL,// WeaponTypeBought : used by HawkAndLittle
		BSMF_SM_WEPTAKE_FOR_VOM,// WeaponTypeTaken : used by VomFeuer
		BSMF_SM_WEPTAKE_FOR_SHR,// WeaponTypeTaken : used by Shrewsbury
		BSMF_SM_WEPTAKE_FOR_HAL,// WeaponTypeTaken : used by HawkAndLittle
		BSMF_SM_KILCOP_FOR_VOM,// CopsKilledWithWeaponType : used by VomFeuer
		BSMF_SM_KILCOP_FOR_SHR,// CopsKilledWithWeaponType : used by Shrewsbury
		BSMF_SM_KILCOP_FOR_HAL,// CopsKilledWithWeaponType : used by HawkAndLittle
		BSMF_SM_KILCRIM_FOR_VOM,// CriminalsKilledWithWeaponType : used by VomFeuer
		BSMF_SM_KILCRIM_FOR_SHR,// CriminalsKilledWithWeaponType : used by Shrewsbury
		BSMF_SM_KILCRIM_FOR_HAL,// CriminalsKilledWithWeaponType : used by HawkAndLittle
		BSMF_SM_KILCIV_FOR_MAI,// CiviliansKilledWithWeaponType : used by Maibatsu
		BSMF_SM_KILCIV_FOR_SHT,// CiviliansKilledWithWeaponType : used by Schyster
		BSMF_SM_KILCIV_FOR_UMA,// CiviliansKilledWithWeaponType : used by Ubermacht
		BSMF_SM_KILCIV_FOR_VOM,// CiviliansKilledWithWeaponType : used by VomFeuer
		BSMF_SM_KILCIV_FOR_SHR,// CiviliansKilledWithWeaponType : used by Shrewsbury
		BSMF_SM_KILCIV_FOR_HAL,// CiviliansKilledWithWeaponType : used by HawkAndLittle
		BSMF_SM_VENUSE_FOR_RAI,// VendingMachineOftypeUsed : used by Raine
		BSMF_SM_VENUSE_FOR_SPU,// VendingMachineOftypeUsed : used by Sprunk
		BSMF_SM_NEWDAM, //DamageToNewsagentStalls
		BSMF_SM_HPKIL, //HealthyPeopleKilled
		BSMF_SM_PUBCLUB, //PubandClubvisits
		BSMF_SM_TDRNK, //TimesDrunk
		BSMF_SM_FRNPUB, //FriendsTakenToPubsclubs
		BSMF_SM_DRNKCRM, //DrunkenCrimes
		BSMF_SM_RAMCOM, //RampagesComplete
		BSMF_SM_TKNWA, //TicksWithNoWantedRating
		BSMF_SM_RAD_FOR_CNT,// RadioStationsListenedTo : used by CNT
		BSMF_SM_RAD_FOR_WZL,// RadioStationsListenedTo : used by Weazel
		BSMF_SM_RAD_FOR_ZIT,// RadioStationsListenedTo : used by Zit
		BSMF_SM_RAD_FOR_RA1,// RadioStationsListenedTo : used by WorldwideFM
		BSMF_SM_RAD_FOR_RA2,// RadioStationsListenedTo : used by RadioLosSantos
		BSMF_SM_ZITIT_FOR_CNT,// SongsIDedWithZIT : used by CNT
		BSMF_SM_ZITIT_FOR_WZL,// SongsIDedWithZIT : used by Weazel
		BSMF_SM_ZITIT_FOR_ZIT,// SongsIDedWithZIT : used by Zit
		BSMF_SM_RADCHA_FOR_CNT,// TimesChangedAwayFromRadiostation : used by CNT
		BSMF_SM_RADCHA_FOR_WZL,// TimesChangedAwayFromRadiostation : used by Weazel
		BSMF_SM_RADCHA_FOR_RA1,// TimesChangedAwayFromRadiostation : used by WorldwideFM
		BSMF_SM_RADCHA_FOR_RA2,// TimesChangedAwayFromRadiostation : used by RadioLosSantos
		BSMF_SM_PARA, //ParaJumps
		BSMF_SM_TKFIRE, //BeingOnFireTicks
		BSMF_SM_FIBAI, //BailsFromVehicleWhenOnFire
		BSMF_SM_TANDES, //TankersDestroyed
		BSMF_SM_GAREP, //GarageRepairsMade
		BSMF_SM_GAMONSP, //MoneySpentAtGarage
		BSMF_SM_MONB, //CarsModifiedThenNotBought
		BSMF_SM_MONUP_FOR_BOL,// MoneyGainedTotal : used by BankOfLiberty
		BSMF_SM_MONUP_FOR_FLC,// MoneyGainedTotal : used by Fleeca
		BSMF_SM_MONUP_FOR_MAZ,// MoneyGainedTotal : used by MazeBank
		BSMF_SM_MONUP_FOR_SHK,// MoneyGainedTotal : used by Shark
		BSMF_SM_MONSP_FOR_BOL,// AmountOfMoneySpent : used by BankOfLiberty
		BSMF_SM_TAXDEST, //TaxisDestroyed
		BSMF_SM_KILW_FOR_BFA,// PeopleKilledWith : used by BF
		BSMF_SM_KILW_FOR_BRU,// PeopleKilledWith : used by Brute
		BSMF_SM_KILW_FOR_HJK,// PeopleKilledWith : used by Hijak
		BSMF_SM_KILW_FOR_MAI,// PeopleKilledWith : used by Maibatsu
		BSMF_SM_KILW_FOR_SHT,// PeopleKilledWith : used by Schyster
		BSMF_SM_KILW_FOR_UMA,// PeopleKilledWith : used by Ubermacht
		BSMF_SM_KILW_FOR_VAP,// PeopleKilledWith : used by Vapid
		BSMF_SM_KILW_FOR_VOM,// PeopleKilledWith : used by VomFeuer
		BSMF_SM_KILW_FOR_HVY,// PeopleKilledWith : used by HVYIndustries
		BSMF_SM_KILW_FOR_MER,// PeopleKilledWith : used by Merryweather
		BSMF_SM_CRIMCOM, //CrimesCommited
		BSMF_SM_CLOBOF_FOR_BIN,// ClothesBoughtFrom : used by Binco
		BSMF_SM_CLOBOF_FOR_PKW,// ClothesBoughtFrom : used by Peckerwood
		BSMF_SM_CLOBOF_FOR_PON,// ClothesBoughtFrom : used by Ponsonbys
		BSMF_SM_HDOG, //HappyDogTime
		BSMF_SM_UHDOG, //UnhappyDogTime
		BSMF_SM_YOGA, //TimesCompletedYoga
		BSMF_SM_TRI, //TimesCompletedTriathlon
		BSMF_SM_GYM, //TimeSpentAtGym
		BSMF_SM_STRIP, //TicksAtStripClub
		BSMF_SM_UGHOK, //UglyHookersKilled
		BSMF_SM_STRTRO, //TroubleCauseAtStripClub
		BSMF_SM_PISCO, //PilotSchoolUse
		BSMF_SM_TOTINJ, //PeopleInjured
		BSMF_SM_DRUGKIL, //DrugDealersKilled
		BSMF_SM_HANGOVR, //NumberOfHangOvers
		BSMF_SM_KILLSPR, //KillingSprees
		BSMF_SM_MAILDEST, //MailBoxesDestroyed
		BSMF_SM_PEDFIREKILL, //PedsKilledByFire
		BSMF_SM_PEDFIRETICK, //PedsOnFireTimes
		BSMF_SM_TVTICK_FOR_WAP,// TVTypeUsage : used by Dept.WaterandPower
		BSMF_SM_TVTICK_FOR_WIW,// TVTypeUsage : used by WIWANG
		BSMF_SM_TVTICK_FOR_RIM,// TVTypeUsage : used by RichardsMajestic
		BSMF_SM_ZITPOP_FOR_ZIT,// ZitPopularitySongGroup : used by Zit
		BSMF_SM_CARAPP, //TimesUsedCarApp
		BSMF_SM_HAIRCUTS, //TimesHairCut
		BSMF_SM_STOROB, //StoreRobberiesProfits
		BSMF_SM_SP_RUNDIST_M, //BASE_SP_RUNDIST_MIKE //this is an external enum used by offline
		BSMF_SM_SP_RUNDIST_F, //BASE_SP_RUNDIST_FRANK //this is an external enum used by offline
		BSMF_SM_SP_RUNDIST_T, //BASE_SP_RUNDIST_TREV //this is an external enum used by offline
		BSMF_SM_SP_BIKEDIST_M, //BASE_SP_BIKED_DIST_MIKE //this is an external enum used by offline
		BSMF_SM_SP_BIKEDIST_F, //BASE_SP_BIKED_DIST_FRANK //this is an external enum used by offline
		BSMF_SM_SP_BIKEDIST_T, //BASE_SP_BIKED_DIST_TREV //this is an external enum used by offline
		BSMF_SM_SP_CARBAILS_M, //BASE_SP_CARBAILS_MIKE //this is an external enum used by offline
		BSMF_SM_SP_CARBAILS_F, //BASE_SP_CARBAILS_FRANK //this is an external enum used by offline
		BSMF_SM_SP_CARBAILS_T, //BASE_SP_CARBAILS_TREV //this is an external enum used by offline
		BSMF_SM_SP_FIRESTART_M, //BASE_SP_FIRESTART_MIKE
		BSMF_SM_SP_FIRESTART_F, //BASE_SP_FIRESTART_FRANK
		BSMF_SM_SP_FIRESTART_T, //BASE_SP_FIRESTART_TREV
		BSMF_SM_SP_LCRASH_M, //BASE_SP_LARGE_CRASH_MIKE //this is an external enum used by offline
		BSMF_SM_SP_LCRASH_F, //BASE_SP_LARGE_CRASH_FRANK //this is an external enum used by offline
		BSMF_SM_SP_LCRASH_T, //BASE_SP_LARGE_CRASH_TREV //this is an external enum used by offline
		BSMF_SM_SP_LKILLS_M, //BASE_SP_LEGIT_KILLS_MIKE //this is an external enum used by offline
		BSMF_SM_SP_LKILLS_F, //BASE_SP_LEGIT_KILLS_FRANK //this is an external enum used by offline
		BSMF_SM_SP_LKILLS_T, //BASE_SP_LEGIT_KILLS_TREV //this is an external enum used by offline
		BSMF_SM_MONTAX_M, //BASE_SP_MONEY_TAXI_MIKE //this is an external enum used by offline
		BSMF_SM_MONTAX_F, //BASE_SP_MONEY_TAXI_FRANK //this is an external enum used by offline
		BSMF_SM_MONTAX_T, //BASE_SP_MONEY_TAXI_TREV //this is an external enum used by offline
		BSMF_SM_HECAR_M, //BASE_SP_MONEY_HEALTHCARE_MIKE //this is an external enum used by offline
		BSMF_SM_HECAR_F, //BASE_SP_MONEY_HEALTHCARE_FRANK //this is an external enum used by offline
		BSMF_SM_HECAR_T, //BASE_SP_MONEY_HEALTHCARE_TREV //this is an external enum used by offline
		BSMF_SM_PHONCAL_FOR_BDG,// SPLIT_PHONE_CALLS : used by Badger
		BSMF_SM_PHONCAL_FOR_TNK,// SPLIT_PHONE_CALLS : used by Tinkle
		BSMF_SM_PHONCAL_FOR_WIZ,// SPLIT_PHONE_CALLS : used by Whiz
		BSMF_SM_PHONTXT_FOR_BDG,// SPLIT_PHONE_TEXTS : used by Badger
		BSMF_SM_PHONTXT_FOR_TNK,// SPLIT_PHONE_TEXTS : used by Tinkle
		BSMF_SM_PHONTXT_FOR_WIZ,// SPLIT_PHONE_TEXTS : used by Whiz
		BSMF_SM_CHTICK_FOR_BDG,// SPLIT_TICKS_ON_CHAR : used by Badger
		BSMF_SM_CHTICK_FOR_TNK,// SPLIT_TICKS_ON_CHAR : used by Tinkle
		BSMF_SM_CHTICK_FOR_WIZ,// SPLIT_TICKS_ON_CHAR : used by Whiz
		BSMF_SM_CALCAN_FOR_BDG,// SPLIT_CALLS_CANCELED : used by Badger
		BSMF_SM_CALCAN_FOR_TNK,// SPLIT_CALLS_CANCELED : used by Tinkle
		BSMF_SM_CALCAN_FOR_WIZ,// SPLIT_CALLS_CANCELED : used by Whiz
		BSMF_SM_MONBRI_M, //BASE_SP_MONEY_BRIBES_MIKE //this is an external enum used by offline
		BSMF_SM_MONBRI_F, //BASE_SP_MONEY_BRIBES_FRANK //this is an external enum used by offline
		BSMF_SM_MONBRI_T, //BASE_SP_MONEY_BRIBES_TREV //this is an external enum used by offline
		BSMF_SM_BUST_M, //BASE_SP_BUSTED_MIKE //this is an external enum used by offline
		BSMF_SM_BUST_F, //BASE_SP_BUSTED_FRANK //this is an external enum used by offline
		BSMF_SM_BUST_T, //BASE_SP_BUSTED_TREV //this is an external enum used by offline
		BSMF_SM_FALDE_M, //BASE_SP_FALLDEATH_MIKE //this is an external enum used by offline
		BSMF_SM_FALDE_F, //BASE_SP_FALLDEATH_FRANK //this is an external enum used by offline
		BSMF_SM_FALDE_T, //BASE_SP_FALLDEATH_TREV //this is an external enum used by offline
		BSMF_SM_STATAI_M, //BASE_SP_STARS_ATTAINED_MIKE //this is an external enum used by offline
		BSMF_SM_STATAI_F, //BASE_SP_STARS_ATTAINED_FRANK //this is an external enum used by offline
		BSMF_SM_STATAI_T, //BASE_SP_STARS_ATTAINED_TREV //this is an external enum used by offline
		BSMF_SM_STALOS_M, //BASE_SP_STARS_LOST_MIKE //this is an external enum used by offline
		BSMF_SM_STALOS_F, //BASE_SP_STARS_LOST_FRANK //this is an external enum used by offline
		BSMF_SM_STALOS_T, //BASE_SP_STARS_LOST_TREV //this is an external enum used by offline
		BSMF_SM_SAVE_M, //BASE_SP_SAVED_MIKE //this is an external enum used by offline
		BSMF_SM_SAVE_F, //BASE_SP_SAVED_FRANK //this is an external enum used by offline
		BSMF_SM_SAVE_T, //BASE_SP_SAVED_TREV //this is an external enum used by offline
		BSMF_SM_COPKIL_M, //BASE_SP_COPKILL_MIKE //this is an external enum used by offline
		BSMF_SM_COPKIL_F, //BASE_SP_COPKILL_FRANK //this is an external enum used by offline
		BSMF_SM_COPKIL_T, //BASE_SP_COPKILL_TREV //this is an external enum used by offline
		BSMF_SM_SWAKIL_M, //BASE_SP_SWATKILL_MIKE //this is an external enum used by offline
		BSMF_SM_SWAKIL_F, //BASE_SP_SWATKILL_FRANK //this is an external enum used by offline
		BSMF_SM_SWAKIL_T, //BASE_SP_SWATKILL_TREV //this is an external enum used by offline
		BSMF_SP_COTI_M, //BASE_SP_COVERTIME_MIKE //this is an external enum used by offline
		BSMF_SP_COTI_F, //BASE_SP_COVERTIME_FRANK //this is an external enum used by offline
		BSMF_SP_COTI_T, //BASE_SP_COVERTIME_TREV //this is an external enum used by offline
		BSMF_SM_FURDEST, //FurnitureDestroyed
		BSMF_BSM_SMOKED_FOR_DEB,// TimesSmoked : used by Debonaire
		BSMF_BSM_SMOKED_FOR_RWC,// TimesSmoked : used by Redwood
		BSMF_TOTAL_INDICES
	ENDENUM

/// Initialisation and modifier update functionality.

// Three sections of graph a day over 5 days. That's 15 sections requiring 16 data points.
CONST_INT MAX_STOCK_PRICE_LOG_ENTRIES 16

STRUCT BAWSAQ_LISTING_STRUCT
		TEXT_LABEL sLongName
		TEXT_LABEL sShortName
		BOOL bOnlineStock

		FLOAT fCurrentPrice
		FLOAT fUniversalPriceModifier
		FLOAT fUPMAppliedLastUpdate
		INT iVolumeTraded
		INT iLogIndexCaret

		FLOAT LoggedPrices[MAX_STOCK_PRICE_LOG_ENTRIES]
		
		FLOAT fLoggedHighValue
		FLOAT fLoggedLowValue
		FLOAT fLoggedPriceChangeFromWeeklyAverage
		FLOAT fLoggedPriceChangeInPercent
		
		INT StatIndex//for online index is in g_BS_OIndexData, for offline g_BS_OfAttribData
ENDSTRUCT

BAWSAQ_LISTING_STRUCT g_BS_Listings[BS_CO_TOTAL_LISTINGS]


//Online stat data
STRUCT BAWSAQ_ONLINE_STAT_STUCT
		STATSENUM OnlinePriceStat
		STATSENUM OnlineVolumeUploadStat
		STATSENUM OnlineVolumeDownloadStat
		STATSENUM OnlineCharOwned[3]//mike//frank//trev
ENDSTRUCT

BAWSAQ_ONLINE_STAT_STUCT g_BS_OIndexData[39]


STRUCT BAWSAQ_OFFLINE_ATTRIB_STUCT // contains the calculation settings needed for new price generation // only offline companies have this
		FLOAT fCurrentInclination//This is the only thing that would need to be saved from this list

		FLOAT fBearish
		FLOAT fBullish
		FLOAT fCeiling
		FLOAT fFloor
		FLOAT fStepCap
		FLOAT fNoise
ENDSTRUCT

BAWSAQ_OFFLINE_ATTRIB_STUCT g_BS_OfAttribData[41]


STRUCT BAWSAQ_MODIFIER_STRUCT
	bool bOnline
	STATSENUM uploadTo
	bool bReadFromProfile//does this modifier read it's value from a profile stat? 
	STATSENUM ProfStatToRead
	BOOL bProfIsFloatType
	INT iPrev
	INT iDelta
ENDSTRUCT

BAWSAQ_MODIFIER_STRUCT g_BS_Modifiers[BSMF_TOTAL_INDICES]

	

PROC BAWSAQ_INTIALISE_LISTS()

	INT i = 0
	REPEAT BS_CO_TOTAL_LISTINGS i
		g_BS_Listings[i].fLoggedPriceChangeFromWeeklyAverage = 0.0
		g_BS_Listings[i].iLogIndexCaret = 0
		g_BS_Listings[i].iVolumeTraded = 0
		g_BS_Listings[i].fLoggedHighValue = 0
		g_BS_Listings[i].fLoggedLowValue = 3.402823E+38
		int j = 0
		REPEAT MAX_STOCK_PRICE_LOG_ENTRIES j
			g_BS_Listings[i].LoggedPrices[j] = 0.0
		ENDREPEAT
	ENDREPEAT

	i = 0
	REPEAT BSMF_TOTAL_INDICES i
		g_BS_Modifiers[i].bReadFromProfile = FALSE
		//g_BS_Modifiers[i].ProfStatToRead = FALSE //unset
	ENDREPEAT

	g_BS_Listings[BS_CO_AMU].sLongName =  "BSS_BSTR_0"
	g_BS_Listings[BS_CO_AMU].sShortName =  "BSS_BSTR_1"
	g_BS_Listings[BS_CO_AMU].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_AMU].StatIndex = 0 
	g_BS_Listings[BS_CO_BDG].sLongName =  "BSS_BSTR_2"
	g_BS_Listings[BS_CO_BDG].sShortName =  "BSS_BSTR_3"
	g_BS_Listings[BS_CO_BDG].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_BDG].StatIndex = 1 
	g_BS_Listings[BS_CO_BOL].sLongName =  "BSS_BSTR_4"
	g_BS_Listings[BS_CO_BOL].sShortName =  "BSS_BSTR_5"
	g_BS_Listings[BS_CO_BOL].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_BOL].StatIndex = 0 
	g_BS_Listings[BS_CO_BFA].sLongName =  "BSS_BSTR_6"
	g_BS_Listings[BS_CO_BFA].sShortName =  "BSS_BSTR_7"
	g_BS_Listings[BS_CO_BFA].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_BFA].StatIndex = 2 
	g_BS_Listings[BS_CO_BIN].sLongName =  "BSS_BSTR_8"
	g_BS_Listings[BS_CO_BIN].sShortName =  "BSS_BSTR_9"
	g_BS_Listings[BS_CO_BIN].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_BIN].StatIndex = 3 
	g_BS_Listings[BS_CO_BTR].sLongName =  "BSS_BSTR_10"
	g_BS_Listings[BS_CO_BTR].sShortName =  "BSS_BSTR_11"
	g_BS_Listings[BS_CO_BTR].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_BTR].StatIndex = 4 
	g_BS_Listings[BS_CO_BLE].sLongName =  "BSS_BSTR_12"
	g_BS_Listings[BS_CO_BLE].sShortName =  "BSS_BSTR_13"
	g_BS_Listings[BS_CO_BLE].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_BLE].StatIndex = 5 
	g_BS_Listings[BS_CO_BRU].sLongName =  "BSS_BSTR_14"
	g_BS_Listings[BS_CO_BRU].sShortName =  "BSS_BSTR_15"
	g_BS_Listings[BS_CO_BRU].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_BRU].StatIndex = 6 
	g_BS_Listings[BS_CO_CNT].sLongName =  "BSS_BSTR_16"
	g_BS_Listings[BS_CO_CNT].sShortName =  "BSS_BSTR_16"
	g_BS_Listings[BS_CO_CNT].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_CNT].StatIndex = 7 
	g_BS_Listings[BS_CO_CRE].sLongName =  "BSS_BSTR_17"
	g_BS_Listings[BS_CO_CRE].sShortName =  "BSS_BSTR_18"
	g_BS_Listings[BS_CO_CRE].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_CRE].StatIndex = 8 
	g_BS_Listings[BS_CO_DGP].sLongName =  "BSS_BSTR_19"
	g_BS_Listings[BS_CO_DGP].sShortName =  "BSS_BSTR_20"
	g_BS_Listings[BS_CO_DGP].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_DGP].StatIndex = 9 
	g_BS_Listings[BS_CO_WAP].sLongName =  "BSS_BSTR_21"
	g_BS_Listings[BS_CO_WAP].sShortName =  "BSS_BSTR_22"
	g_BS_Listings[BS_CO_WAP].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_WAP].StatIndex = 10 
	g_BS_Listings[BS_CO_FAC].sLongName =  "BSS_BSTR_23"
	g_BS_Listings[BS_CO_FAC].sShortName =  "BSS_BSTR_24"
	g_BS_Listings[BS_CO_FAC].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_FAC].StatIndex = 11 
	g_BS_Listings[BS_CO_FLC].sLongName =  "BSS_BSTR_25"
	g_BS_Listings[BS_CO_FLC].sShortName =  "BSS_BSTR_26"
	g_BS_Listings[BS_CO_FLC].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_FLC].StatIndex = 1 
	g_BS_Listings[BS_CO_HJK].sLongName =  "BSS_BSTR_27"
	g_BS_Listings[BS_CO_HJK].sShortName =  "BSS_BSTR_28"
	g_BS_Listings[BS_CO_HJK].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_HJK].StatIndex = 2 
	g_BS_Listings[BS_CO_FRT].sLongName =  "BSS_BSTR_29"
	g_BS_Listings[BS_CO_FRT].sShortName =  "BSS_BSTR_30"
	g_BS_Listings[BS_CO_FRT].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_FRT].StatIndex = 12 
	g_BS_Listings[BS_CO_AUG].sLongName =  "BSS_BSTR_31"
	g_BS_Listings[BS_CO_AUG].sShortName =  "BSS_BSTR_32"
	g_BS_Listings[BS_CO_AUG].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_AUG].StatIndex = 3 
	g_BS_Listings[BS_CO_BUL].sLongName =  "BSS_BSTR_33"
	g_BS_Listings[BS_CO_BUL].sShortName =  "BSS_BSTR_34"
	g_BS_Listings[BS_CO_BUL].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_BUL].StatIndex = 4 
	g_BS_Listings[BS_CO_HAF].sLongName =  "BSS_BSTR_35"
	g_BS_Listings[BS_CO_HAF].sShortName =  "BSS_BSTR_36"
	g_BS_Listings[BS_CO_HAF].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_HAF].StatIndex = 5 
	g_BS_Listings[BS_CO_SSS].sLongName =  "BSS_BSTR_37"
	g_BS_Listings[BS_CO_SSS].sShortName =  "BSS_BSTR_38"
	g_BS_Listings[BS_CO_SSS].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_SSS].StatIndex = 6 
	g_BS_Listings[BS_CO_LSC].sLongName =  "BSS_BSTR_39"
	g_BS_Listings[BS_CO_LSC].sShortName =  "BSS_BSTR_40"
	g_BS_Listings[BS_CO_LSC].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_LSC].StatIndex = 13 
	g_BS_Listings[BS_CO_LST].sLongName =  "BSS_BSTR_41"
	g_BS_Listings[BS_CO_LST].sShortName =  "BSS_BSTR_42"
	g_BS_Listings[BS_CO_LST].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_LST].StatIndex = 14 
	g_BS_Listings[BS_CO_LTD].sLongName =  "BSS_BSTR_43"
	g_BS_Listings[BS_CO_LTD].sShortName =  "BSS_BSTR_44"
	g_BS_Listings[BS_CO_LTD].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_LTD].StatIndex = 15 
	g_BS_Listings[BS_CO_MAI].sLongName =  "BSS_BSTR_45"
	g_BS_Listings[BS_CO_MAI].sShortName =  "BSS_BSTR_46"
	g_BS_Listings[BS_CO_MAI].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_MAI].StatIndex = 16 
	g_BS_Listings[BS_CO_MAZ].sLongName =  "BSS_BSTR_47"
	g_BS_Listings[BS_CO_MAZ].sShortName =  "BSS_BSTR_48"
	g_BS_Listings[BS_CO_MAZ].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_MAZ].StatIndex = 7 
	g_BS_Listings[BS_CO_PKW].sLongName =  "BSS_BSTR_49"
	g_BS_Listings[BS_CO_PKW].sShortName =  "BSS_BSTR_50"
	g_BS_Listings[BS_CO_PKW].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_PKW].StatIndex = 17 
	g_BS_Listings[BS_CO_ARK].sLongName =  "BSS_BSTR_51"
	g_BS_Listings[BS_CO_ARK].sShortName =  "BSS_BSTR_52"
	g_BS_Listings[BS_CO_ARK].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_ARK].StatIndex = 8 
	g_BS_Listings[BS_CO_PIS].sLongName =  "BSS_BSTR_53"
	g_BS_Listings[BS_CO_PIS].sShortName =  "BSS_BSTR_54"
	g_BS_Listings[BS_CO_PIS].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_PIS].StatIndex = 18 
	g_BS_Listings[BS_CO_PON].sLongName =  "BSS_BSTR_55"
	g_BS_Listings[BS_CO_PON].sShortName =  "BSS_BSTR_56"
	g_BS_Listings[BS_CO_PON].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_PON].StatIndex = 19 
	g_BS_Listings[BS_CO_RAI].sLongName =  "BSS_BSTR_57"
	g_BS_Listings[BS_CO_RAI].sShortName =  "BSS_BSTR_58"
	g_BS_Listings[BS_CO_RAI].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_RAI].StatIndex = 9 
	g_BS_Listings[BS_CO_RON].sLongName =  "BSS_BSTR_59"
	g_BS_Listings[BS_CO_RON].sShortName =  "BSS_BSTR_60"
	g_BS_Listings[BS_CO_RON].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_RON].StatIndex = 20 
	g_BS_Listings[BS_CO_SHT].sLongName =  "BSS_BSTR_61"
	g_BS_Listings[BS_CO_SHT].sShortName =  "BSS_BSTR_62"
	g_BS_Listings[BS_CO_SHT].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_SHT].StatIndex = 21 
	g_BS_Listings[BS_CO_SPU].sLongName =  "BSS_BSTR_63"
	g_BS_Listings[BS_CO_SPU].sShortName =  "BSS_BSTR_64"
	g_BS_Listings[BS_CO_SPU].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_SPU].StatIndex = 22 
	g_BS_Listings[BS_CO_TNK].sLongName =  "BSS_BSTR_65"
	g_BS_Listings[BS_CO_TNK].sShortName =  "BSS_BSTR_66"
	g_BS_Listings[BS_CO_TNK].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_TNK].StatIndex = 23 
	g_BS_Listings[BS_CO_WIW].sLongName =  "BSS_BSTR_67"
	g_BS_Listings[BS_CO_WIW].sShortName =  "BSS_BSTR_68"
	g_BS_Listings[BS_CO_WIW].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_WIW].StatIndex = 24 
	g_BS_Listings[BS_CO_UMA].sLongName =  "BSS_BSTR_69"
	g_BS_Listings[BS_CO_UMA].sShortName =  "BSS_BSTR_70"
	g_BS_Listings[BS_CO_UMA].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_UMA].StatIndex = 25 
	g_BS_Listings[BS_CO_VAP].sLongName =  "BSS_BSTR_71"
	g_BS_Listings[BS_CO_VAP].sShortName =  "BSS_BSTR_72"
	g_BS_Listings[BS_CO_VAP].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_VAP].StatIndex = 26 
	g_BS_Listings[BS_CO_VOM].sLongName =  "BSS_BSTR_73"
	g_BS_Listings[BS_CO_VOM].sShortName =  "BSS_BSTR_74"
	g_BS_Listings[BS_CO_VOM].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_VOM].StatIndex = 27 
	g_BS_Listings[BS_CO_WZL].sLongName =  "BSS_BSTR_75"
	g_BS_Listings[BS_CO_WZL].sShortName =  "BSS_BSTR_76"
	g_BS_Listings[BS_CO_WZL].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_WZL].StatIndex = 28 
	g_BS_Listings[BS_CO_WIZ].sLongName =  "BSS_BSTR_77"
	g_BS_Listings[BS_CO_WIZ].sShortName =  "BSS_BSTR_78"
	g_BS_Listings[BS_CO_WIZ].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_WIZ].StatIndex = 29 
	g_BS_Listings[BS_CO_DEB].sLongName =  "BSS_BSTR_79"
	g_BS_Listings[BS_CO_DEB].sShortName =  "BSS_BSTR_80"
	g_BS_Listings[BS_CO_DEB].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_DEB].StatIndex = 10 
	g_BS_Listings[BS_CO_ZIT].sLongName =  "BSS_BSTR_81"
	g_BS_Listings[BS_CO_ZIT].sShortName =  "BSS_BSTR_82"
	g_BS_Listings[BS_CO_ZIT].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_ZIT].StatIndex = 30 
	g_BS_Listings[BS_CO_SHK].sLongName =  "BSS_BSTR_83"
	g_BS_Listings[BS_CO_SHK].sShortName =  "BSS_BSTR_84"
	g_BS_Listings[BS_CO_SHK].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_SHK].StatIndex = 31 
	g_BS_Listings[BS_CO_MOL].sLongName =  "BSS_BSTR_85"
	g_BS_Listings[BS_CO_MOL].sShortName =  "BSS_BSTR_86"
	g_BS_Listings[BS_CO_MOL].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_MOL].StatIndex = 32 
	g_BS_Listings[BS_CO_PMP].sLongName =  "BSS_BSTR_87"
	g_BS_Listings[BS_CO_PMP].sShortName =  "BSS_BSTR_88"
	g_BS_Listings[BS_CO_PMP].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_PMP].StatIndex = 33 
	g_BS_Listings[BS_CO_GOT].sLongName =  "BSS_BSTR_89"
	g_BS_Listings[BS_CO_GOT].sShortName =  "BSS_BSTR_90"
	g_BS_Listings[BS_CO_GOT].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_GOT].StatIndex = 34 
	g_BS_Listings[BS_CO_EYE].sLongName =  "BSS_BSTR_91"
	g_BS_Listings[BS_CO_EYE].sShortName =  "BSS_BSTR_92"
	g_BS_Listings[BS_CO_EYE].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_EYE].StatIndex = 35 
	g_BS_Listings[BS_CO_EMU].sLongName =  "BSS_BSTR_93"
	g_BS_Listings[BS_CO_EMU].sShortName =  "BSS_BSTR_94"
	g_BS_Listings[BS_CO_EMU].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_EMU].StatIndex = 11 
	g_BS_Listings[BS_CO_BEN].sLongName =  "BSS_BSTR_95"
	g_BS_Listings[BS_CO_BEN].sShortName =  "BSS_BSTR_96"
	g_BS_Listings[BS_CO_BEN].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_BEN].StatIndex = 12 
	g_BS_Listings[BS_CO_GOL].sLongName =  "BSS_BSTR_97"
	g_BS_Listings[BS_CO_GOL].sShortName =  "BSS_BSTR_98"
	g_BS_Listings[BS_CO_GOL].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_GOL].StatIndex = 13 
	g_BS_Listings[BS_CO_BOM].sLongName =  "BSS_BSTR_99"
	g_BS_Listings[BS_CO_BOM].sShortName =  "BSS_BSTR_100"
	g_BS_Listings[BS_CO_BOM].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_BOM].StatIndex = 14 
	g_BS_Listings[BS_CO_BGR].sLongName =  "BSS_BSTR_101"
	g_BS_Listings[BS_CO_BGR].sShortName =  "BSS_BSTR_102"
	g_BS_Listings[BS_CO_BGR].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_BGR].StatIndex = 15 
	g_BS_Listings[BS_CO_CLK].sLongName =  "BSS_BSTR_103"
	g_BS_Listings[BS_CO_CLK].sShortName =  "BSS_BSTR_104"
	g_BS_Listings[BS_CO_CLK].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_CLK].StatIndex = 16 
	g_BS_Listings[BS_CO_BAN].sLongName =  "BSS_BSTR_105"
	g_BS_Listings[BS_CO_BAN].sShortName =  "BSS_BSTR_106"
	g_BS_Listings[BS_CO_BAN].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_BAN].StatIndex = 17 
	g_BS_Listings[BS_CO_DOP].sLongName =  "BSS_BSTR_107"
	g_BS_Listings[BS_CO_DOP].sShortName =  "BSS_BSTR_108"
	g_BS_Listings[BS_CO_DOP].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_DOP].StatIndex = 18 
	g_BS_Listings[BS_CO_ECL].sLongName =  "BSS_BSTR_109"
	g_BS_Listings[BS_CO_ECL].sShortName =  "BSS_BSTR_110"
	g_BS_Listings[BS_CO_ECL].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_ECL].StatIndex = 19 
	g_BS_Listings[BS_CO_FUS].sLongName =  "BSS_BSTR_111"
	g_BS_Listings[BS_CO_FUS].sShortName =  "BSS_BSTR_112"
	g_BS_Listings[BS_CO_FUS].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_FUS].StatIndex = 20 
	g_BS_Listings[BS_CO_GAS].sLongName =  "BSS_BSTR_113"
	g_BS_Listings[BS_CO_GAS].sShortName =  "BSS_BSTR_114"
	g_BS_Listings[BS_CO_GAS].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_GAS].StatIndex = 21 
	g_BS_Listings[BS_CO_GOP].sLongName =  "BSS_BSTR_115"
	g_BS_Listings[BS_CO_GOP].sShortName =  "BSS_BSTR_116"
	g_BS_Listings[BS_CO_GOP].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_GOP].StatIndex = 22 
	g_BS_Listings[BS_CO_GRU].sLongName =  "BSS_BSTR_117"
	g_BS_Listings[BS_CO_GRU].sShortName =  "BSS_BSTR_118"
	g_BS_Listings[BS_CO_GRU].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_GRU].StatIndex = 23 
	g_BS_Listings[BS_CO_KRP].sLongName =  "BSS_BSTR_119"
	g_BS_Listings[BS_CO_KRP].sShortName =  "BSS_BSTR_120"
	g_BS_Listings[BS_CO_KRP].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_KRP].StatIndex = 24 
	g_BS_Listings[BS_CO_LFI].sLongName =  "BSS_BSTR_121"
	g_BS_Listings[BS_CO_LFI].sShortName =  "BSS_BSTR_122"
	g_BS_Listings[BS_CO_LFI].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_LFI].StatIndex = 25 
	g_BS_Listings[BS_CO_MAX].sLongName =  "BSS_BSTR_123"
	g_BS_Listings[BS_CO_MAX].sShortName =  "BSS_BSTR_124"
	g_BS_Listings[BS_CO_MAX].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_MAX].StatIndex = 26 
	g_BS_Listings[BS_CO_POP].sLongName =  "BSS_BSTR_125"
	g_BS_Listings[BS_CO_POP].sShortName =  "BSS_BSTR_126"
	g_BS_Listings[BS_CO_POP].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_POP].StatIndex = 27 
	g_BS_Listings[BS_CO_PRO].sLongName =  "BSS_BSTR_127"
	g_BS_Listings[BS_CO_PRO].sShortName =  "BSS_BSTR_128"
	g_BS_Listings[BS_CO_PRO].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_PRO].StatIndex = 28 
	g_BS_Listings[BS_CO_RWC].sLongName =  "BSS_BSTR_129"
	g_BS_Listings[BS_CO_RWC].sShortName =  "BSS_BSTR_130"
	g_BS_Listings[BS_CO_RWC].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_RWC].StatIndex = 29 
	g_BS_Listings[BS_CO_RIM].sLongName =  "BSS_BSTR_131"
	g_BS_Listings[BS_CO_RIM].sShortName =  "BSS_BSTR_132"
	g_BS_Listings[BS_CO_RIM].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_RIM].StatIndex = 30 
	g_BS_Listings[BS_CO_TBO].sLongName =  "BSS_BSTR_133"
	g_BS_Listings[BS_CO_TBO].sShortName =  "BSS_BSTR_134"
	g_BS_Listings[BS_CO_TBO].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_TBO].StatIndex = 31 
	g_BS_Listings[BS_CO_UPA].sLongName =  "BSS_BSTR_135"
	g_BS_Listings[BS_CO_UPA].sShortName =  "BSS_BSTR_136"
	g_BS_Listings[BS_CO_UPA].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_UPA].StatIndex = 32 
	g_BS_Listings[BS_CO_VAG].sLongName =  "BSS_BSTR_137"
	g_BS_Listings[BS_CO_VAG].sShortName =  "BSS_BSTR_138"
	g_BS_Listings[BS_CO_VAG].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_VAG].StatIndex = 33 
	g_BS_Listings[BS_CO_UNI].sLongName =  "BSS_BSTR_139"
	g_BS_Listings[BS_CO_UNI].sShortName =  "BSS_BSTR_140"
	g_BS_Listings[BS_CO_UNI].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_UNI].StatIndex = 34 
	g_BS_Listings[BS_CO_HVY].sLongName =  "BSS_BSTR_141"
	g_BS_Listings[BS_CO_HVY].sShortName =  "BSS_BSTR_142"
	g_BS_Listings[BS_CO_HVY].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_HVY].StatIndex = 36 
	g_BS_Listings[BS_CO_LOG].sLongName =  "BSS_BSTR_143"
	g_BS_Listings[BS_CO_LOG].sShortName =  "BSS_BSTR_144"
	g_BS_Listings[BS_CO_LOG].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_LOG].StatIndex = 35 
	g_BS_Listings[BS_CO_MER].sLongName =  "BSS_BSTR_145"
	g_BS_Listings[BS_CO_MER].sShortName =  "BSS_BSTR_146"
	g_BS_Listings[BS_CO_MER].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_MER].StatIndex = 36 
	g_BS_Listings[BS_CO_RA1].sLongName =  "BSS_BSTR_147"
	g_BS_Listings[BS_CO_RA1].sShortName =  "BSS_BSTR_148"
	g_BS_Listings[BS_CO_RA1].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_RA1].StatIndex = 37 
	g_BS_Listings[BS_CO_RA2].sLongName =  "BSS_BSTR_149"
	g_BS_Listings[BS_CO_RA2].sShortName =  "BSS_BSTR_150"
	g_BS_Listings[BS_CO_RA2].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_RA2].StatIndex = 38 
	g_BS_Listings[BS_CO_SHR].sLongName =  "BSS_BSTR_151"
	g_BS_Listings[BS_CO_SHR].sShortName =  "BSS_BSTR_152"
	g_BS_Listings[BS_CO_SHR].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_SHR].StatIndex = 37 
	g_BS_Listings[BS_CO_HAL].sLongName =  "BSS_BSTR_153"
	g_BS_Listings[BS_CO_HAL].sShortName =  "BSS_BSTR_154"
	g_BS_Listings[BS_CO_HAL].bOnlineStock = TRUE 
	g_BS_Listings[BS_CO_HAL].StatIndex = 38 
	g_BS_Listings[BS_CO_MOR].sLongName =  "BSS_BSTR_155"
	g_BS_Listings[BS_CO_MOR].sShortName =  "BSS_BSTR_156"
	g_BS_Listings[BS_CO_MOR].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_MOR].StatIndex = 39 
	g_BS_Listings[BS_CO_BSS_PRI].sLongName =  "BSS_BSTR_157"
	g_BS_Listings[BS_CO_BSS_PRI].sShortName =  "BSS_BSTR_158"
	g_BS_Listings[BS_CO_BSS_PRI].bOnlineStock = FALSE 
	g_BS_Listings[BS_CO_BSS_PRI].StatIndex = 40 
	//Offline price calculation attribs for BankOfLiberty
	g_BS_OfAttribData[0].fBullish 	=  1	
	g_BS_OfAttribData[0].fBearish 	=  1	
	g_BS_OfAttribData[0].fCeiling 	=  1000	
		g_BS_OfAttribData[0].fFloor = 3.5  
		g_BS_OfAttribData[0].fStepCap = 3.5
		g_BS_OfAttribData[0].fNoise = 0.1 
	//Offline price calculation attribs for Fleeca
	g_BS_OfAttribData[1].fBullish 	=  1	
	g_BS_OfAttribData[1].fBearish 	=  1	
	g_BS_OfAttribData[1].fCeiling 	=  1000	
		g_BS_OfAttribData[1].fFloor = 3.5  
		g_BS_OfAttribData[1].fStepCap = 3.5
		g_BS_OfAttribData[1].fNoise = 0.1 
	//Offline price calculation attribs for Hijak
	g_BS_OfAttribData[2].fBullish 	=  1	
	g_BS_OfAttribData[2].fBearish 	=  1	
	g_BS_OfAttribData[2].fCeiling 	=  1000	
		g_BS_OfAttribData[2].fFloor = 3.5  
		g_BS_OfAttribData[2].fStepCap = 3.5
		g_BS_OfAttribData[2].fNoise = 0.1 
	//Offline price calculation attribs for AuguryInsurance
	g_BS_OfAttribData[3].fBullish 	=  1	
	g_BS_OfAttribData[3].fBearish 	=  1	
	g_BS_OfAttribData[3].fCeiling 	=  1000	
		g_BS_OfAttribData[3].fFloor = 3.5  
		g_BS_OfAttribData[3].fStepCap = 3.5
		g_BS_OfAttribData[3].fNoise = 0.1 
	//Offline price calculation attribs for BULLHEAD
	g_BS_OfAttribData[4].fBullish 	=  1	
	g_BS_OfAttribData[4].fBearish 	=  1	
	g_BS_OfAttribData[4].fCeiling 	=  1000	
		g_BS_OfAttribData[4].fFloor = 3.5  
		g_BS_OfAttribData[4].fStepCap = 3.5
		g_BS_OfAttribData[4].fNoise = 0.1 
	//Offline price calculation attribs for Hammerstein&Faust
	g_BS_OfAttribData[5].fBullish 	=  1	
	g_BS_OfAttribData[5].fBearish 	=  1	
	g_BS_OfAttribData[5].fCeiling 	=  1000	
		g_BS_OfAttribData[5].fFloor = 3.5  
		g_BS_OfAttribData[5].fStepCap = 3.5
		g_BS_OfAttribData[5].fNoise = 0.1 
	//Offline price calculation attribs for Slaughter,Slaughter&Slaughter
	g_BS_OfAttribData[6].fBullish 	=  1	
	g_BS_OfAttribData[6].fBearish 	=  1	
	g_BS_OfAttribData[6].fCeiling 	=  1000	
		g_BS_OfAttribData[6].fFloor = 3.5  
		g_BS_OfAttribData[6].fStepCap = 3.5
		g_BS_OfAttribData[6].fNoise = 0.1 
	//Offline price calculation attribs for MazeBank
	g_BS_OfAttribData[7].fBullish 	=  1	
	g_BS_OfAttribData[7].fBearish 	=  1	
	g_BS_OfAttribData[7].fCeiling 	=  1000	
		g_BS_OfAttribData[7].fFloor = 3.5  
		g_BS_OfAttribData[7].fStepCap = 3.5
		g_BS_OfAttribData[7].fNoise = 0.1 
	//Offline price calculation attribs for AnimalArk
	g_BS_OfAttribData[8].fBullish 	=  1	
	g_BS_OfAttribData[8].fBearish 	=  1	
	g_BS_OfAttribData[8].fCeiling 	=  1000	
		g_BS_OfAttribData[8].fFloor = 3.5  
		g_BS_OfAttribData[8].fStepCap = 3.5
		g_BS_OfAttribData[8].fNoise = 0.1 
	//Offline price calculation attribs for Raine
	g_BS_OfAttribData[9].fBullish 	=  1	
	g_BS_OfAttribData[9].fBearish 	=  1	
	g_BS_OfAttribData[9].fCeiling 	=  1000	
		g_BS_OfAttribData[9].fFloor = 3.5  
		g_BS_OfAttribData[9].fStepCap = 3.5
		g_BS_OfAttribData[9].fNoise = 0.1 
	//Offline price calculation attribs for Debonaire
	g_BS_OfAttribData[10].fBullish 	=  1	
	g_BS_OfAttribData[10].fBearish 	=  1	
	g_BS_OfAttribData[10].fCeiling 	=  1000	
		g_BS_OfAttribData[10].fFloor = 3.5  
		g_BS_OfAttribData[10].fStepCap = 3.5
		g_BS_OfAttribData[10].fNoise = 0.1 
	//Offline price calculation attribs for AirEmu
	g_BS_OfAttribData[11].fBullish 	=  1	
	g_BS_OfAttribData[11].fBearish 	=  1	
	g_BS_OfAttribData[11].fCeiling 	=  1000	
		g_BS_OfAttribData[11].fFloor = 3.5  
		g_BS_OfAttribData[11].fStepCap = 3.5
		g_BS_OfAttribData[11].fNoise = 0.1 
	//Offline price calculation attribs for BeanMachine
	g_BS_OfAttribData[12].fBullish 	=  1	
	g_BS_OfAttribData[12].fBearish 	=  1	
	g_BS_OfAttribData[12].fCeiling 	=  1000	
		g_BS_OfAttribData[12].fFloor = 3.5  
		g_BS_OfAttribData[12].fStepCap = 3.5
		g_BS_OfAttribData[12].fNoise = 0.1 
	//Offline price calculation attribs for GoldCoast
	g_BS_OfAttribData[13].fBullish 	=  1	
	g_BS_OfAttribData[13].fBearish 	=  1	
	g_BS_OfAttribData[13].fCeiling 	=  1000	
		g_BS_OfAttribData[13].fFloor = 3.5  
		g_BS_OfAttribData[13].fStepCap = 3.5
		g_BS_OfAttribData[13].fNoise = 0.1 
	//Offline price calculation attribs for BobMulet
	g_BS_OfAttribData[14].fBullish 	=  1	
	g_BS_OfAttribData[14].fBearish 	=  1	
	g_BS_OfAttribData[14].fCeiling 	=  1000	
		g_BS_OfAttribData[14].fFloor = 3.5  
		g_BS_OfAttribData[14].fStepCap = 3.5
		g_BS_OfAttribData[14].fNoise = 0.1 
	//Offline price calculation attribs for BurgerShot
	g_BS_OfAttribData[15].fBullish 	=  1	
	g_BS_OfAttribData[15].fBearish 	=  1	
	g_BS_OfAttribData[15].fCeiling 	=  1000	
		g_BS_OfAttribData[15].fFloor = 3.5  
		g_BS_OfAttribData[15].fStepCap = 3.5
		g_BS_OfAttribData[15].fNoise = 0.1 
	//Offline price calculation attribs for CluckinBell
	g_BS_OfAttribData[16].fBullish 	=  1	
	g_BS_OfAttribData[16].fBearish 	=  1	
	g_BS_OfAttribData[16].fCeiling 	=  1000	
		g_BS_OfAttribData[16].fFloor = 3.5  
		g_BS_OfAttribData[16].fStepCap = 3.5
		g_BS_OfAttribData[16].fNoise = 0.1 
	//Offline price calculation attribs for CoolBeans
	g_BS_OfAttribData[17].fBullish 	=  1	
	g_BS_OfAttribData[17].fBearish 	=  1	
	g_BS_OfAttribData[17].fCeiling 	=  1000	
		g_BS_OfAttribData[17].fFloor = 3.5  
		g_BS_OfAttribData[17].fStepCap = 3.5
		g_BS_OfAttribData[17].fNoise = 0.1 
	//Offline price calculation attribs for DollarPills
	g_BS_OfAttribData[18].fBullish 	=  1	
	g_BS_OfAttribData[18].fBearish 	=  1	
	g_BS_OfAttribData[18].fCeiling 	=  1000	
		g_BS_OfAttribData[18].fFloor = 3.5  
		g_BS_OfAttribData[18].fStepCap = 3.5
		g_BS_OfAttribData[18].fNoise = 0.1 
	//Offline price calculation attribs for eCola
	g_BS_OfAttribData[19].fBullish 	=  1	
	g_BS_OfAttribData[19].fBearish 	=  1	
	g_BS_OfAttribData[19].fCeiling 	=  1000	
		g_BS_OfAttribData[19].fFloor = 3.5  
		g_BS_OfAttribData[19].fStepCap = 3.5
		g_BS_OfAttribData[19].fNoise = 0.1 
	//Offline price calculation attribs for FlyUS
	g_BS_OfAttribData[20].fBullish 	=  1	
	g_BS_OfAttribData[20].fBearish 	=  1	
	g_BS_OfAttribData[20].fCeiling 	=  1000	
		g_BS_OfAttribData[20].fFloor = 3.5  
		g_BS_OfAttribData[20].fStepCap = 3.5
		g_BS_OfAttribData[20].fNoise = 0.1 
	//Offline price calculation attribs for GastroBand
	g_BS_OfAttribData[21].fBullish 	=  1	
	g_BS_OfAttribData[21].fBearish 	=  1	
	g_BS_OfAttribData[21].fCeiling 	=  1000	
		g_BS_OfAttribData[21].fFloor = 3.5  
		g_BS_OfAttribData[21].fStepCap = 3.5
		g_BS_OfAttribData[21].fNoise = 0.1 
	//Offline price calculation attribs for GoPostal
	g_BS_OfAttribData[22].fBullish 	=  1	
	g_BS_OfAttribData[22].fBearish 	=  1	
	g_BS_OfAttribData[22].fCeiling 	=  1000	
		g_BS_OfAttribData[22].fFloor = 3.5  
		g_BS_OfAttribData[22].fStepCap = 3.5
		g_BS_OfAttribData[22].fNoise = 0.1 
	//Offline price calculation attribs for GruppeSechs
	g_BS_OfAttribData[23].fBullish 	=  1	
	g_BS_OfAttribData[23].fBearish 	=  1	
	g_BS_OfAttribData[23].fCeiling 	=  1000	
		g_BS_OfAttribData[23].fFloor = 3.5  
		g_BS_OfAttribData[23].fStepCap = 3.5
		g_BS_OfAttribData[23].fNoise = 0.1 
	//Offline price calculation attribs for Krapea
	g_BS_OfAttribData[24].fBullish 	=  1	
	g_BS_OfAttribData[24].fBearish 	=  1	
	g_BS_OfAttribData[24].fCeiling 	=  1000	
		g_BS_OfAttribData[24].fFloor = 3.5  
		g_BS_OfAttribData[24].fStepCap = 3.5
		g_BS_OfAttribData[24].fNoise = 0.1 
	//Offline price calculation attribs for LifeInvader
	g_BS_OfAttribData[25].fBullish 	=  1	
	g_BS_OfAttribData[25].fBearish 	=  1	
	g_BS_OfAttribData[25].fCeiling 	=  1000	
		g_BS_OfAttribData[25].fFloor = 3.5  
		g_BS_OfAttribData[25].fStepCap = 3.5
		g_BS_OfAttribData[25].fNoise = 0.1 
	//Offline price calculation attribs for MaxRenda
	g_BS_OfAttribData[26].fBullish 	=  1	
	g_BS_OfAttribData[26].fBearish 	=  1	
	g_BS_OfAttribData[26].fCeiling 	=  1000	
		g_BS_OfAttribData[26].fFloor = 3.5  
		g_BS_OfAttribData[26].fStepCap = 3.5
		g_BS_OfAttribData[26].fNoise = 0.1 
	//Offline price calculation attribs for PostOP
	g_BS_OfAttribData[27].fBullish 	=  1	
	g_BS_OfAttribData[27].fBearish 	=  1	
	g_BS_OfAttribData[27].fCeiling 	=  1000	
		g_BS_OfAttribData[27].fFloor = 3.5  
		g_BS_OfAttribData[27].fStepCap = 3.5
		g_BS_OfAttribData[27].fNoise = 0.1 
	//Offline price calculation attribs for ProLaps
	g_BS_OfAttribData[28].fBullish 	=  1	
	g_BS_OfAttribData[28].fBearish 	=  1	
	g_BS_OfAttribData[28].fCeiling 	=  1000	
		g_BS_OfAttribData[28].fFloor = 3.5  
		g_BS_OfAttribData[28].fStepCap = 3.5
		g_BS_OfAttribData[28].fNoise = 0.1 
	//Offline price calculation attribs for Redwood
	g_BS_OfAttribData[29].fBullish 	=  1	
	g_BS_OfAttribData[29].fBearish 	=  1	
	g_BS_OfAttribData[29].fCeiling 	=  1000	
		g_BS_OfAttribData[29].fFloor = 3.5  
		g_BS_OfAttribData[29].fStepCap = 3.5
		g_BS_OfAttribData[29].fNoise = 0.1 
	//Offline price calculation attribs for RichardsMajestic
	g_BS_OfAttribData[30].fBullish 	=  1	
	g_BS_OfAttribData[30].fBearish 	=  1	
	g_BS_OfAttribData[30].fCeiling 	=  1000	
		g_BS_OfAttribData[30].fFloor = 3.5  
		g_BS_OfAttribData[30].fStepCap = 3.5
		g_BS_OfAttribData[30].fNoise = 0.1 
	//Offline price calculation attribs for TacoBomb
	g_BS_OfAttribData[31].fBullish 	=  1	
	g_BS_OfAttribData[31].fBearish 	=  1	
	g_BS_OfAttribData[31].fCeiling 	=  1000	
		g_BS_OfAttribData[31].fFloor = 3.5  
		g_BS_OfAttribData[31].fStepCap = 3.5
		g_BS_OfAttribData[31].fNoise = 0.1 
	//Offline price calculation attribs for UpandAtom
	g_BS_OfAttribData[32].fBullish 	=  1	
	g_BS_OfAttribData[32].fBearish 	=  1	
	g_BS_OfAttribData[32].fCeiling 	=  1000	
		g_BS_OfAttribData[32].fFloor = 3.5  
		g_BS_OfAttribData[32].fStepCap = 3.5
		g_BS_OfAttribData[32].fNoise = 0.1 
	//Offline price calculation attribs for Vangelico
	g_BS_OfAttribData[33].fBullish 	=  1	
	g_BS_OfAttribData[33].fBearish 	=  1	
	g_BS_OfAttribData[33].fCeiling 	=  1000	
		g_BS_OfAttribData[33].fFloor = 3.5  
		g_BS_OfAttribData[33].fStepCap = 3.5
		g_BS_OfAttribData[33].fNoise = 0.1 
	//Offline price calculation attribs for VanillaUnicorn
	g_BS_OfAttribData[34].fBullish 	=  1	
	g_BS_OfAttribData[34].fBearish 	=  1	
	g_BS_OfAttribData[34].fCeiling 	=  1000	
		g_BS_OfAttribData[34].fFloor = 3.5  
		g_BS_OfAttribData[34].fStepCap = 3.5
		g_BS_OfAttribData[34].fNoise = 0.1 
	//Offline price calculation attribs for Logger
	g_BS_OfAttribData[35].fBullish 	=  1	
	g_BS_OfAttribData[35].fBearish 	=  1	
	g_BS_OfAttribData[35].fCeiling 	=  1000	
		g_BS_OfAttribData[35].fFloor = 3.5  
		g_BS_OfAttribData[35].fStepCap = 3.5
		g_BS_OfAttribData[35].fNoise = 0.1 
	//Offline price calculation attribs for Merryweather
	g_BS_OfAttribData[36].fBullish 	=  1	
	g_BS_OfAttribData[36].fBearish 	=  1	
	g_BS_OfAttribData[36].fCeiling 	=  1000	
		g_BS_OfAttribData[36].fFloor = 3.5  
		g_BS_OfAttribData[36].fStepCap = 3.5
		g_BS_OfAttribData[36].fNoise = 0.1 
	//Offline price calculation attribs for WorldwideFM
	g_BS_OfAttribData[37].fBullish 	=  1	
	g_BS_OfAttribData[37].fBearish 	=  1	
	g_BS_OfAttribData[37].fCeiling 	=  1000	
		g_BS_OfAttribData[37].fFloor = 3.5  
		g_BS_OfAttribData[37].fStepCap = 3.5
		g_BS_OfAttribData[37].fNoise = 0.1 
	//Offline price calculation attribs for RadioLosSantos
	g_BS_OfAttribData[38].fBullish 	=  1	
	g_BS_OfAttribData[38].fBearish 	=  1	
	g_BS_OfAttribData[38].fCeiling 	=  1000	
		g_BS_OfAttribData[38].fFloor = 3.5  
		g_BS_OfAttribData[38].fStepCap = 3.5
		g_BS_OfAttribData[38].fNoise = 0.1 
	//Offline price calculation attribs for MorsMutualInsurance
	g_BS_OfAttribData[39].fBullish 	=  1	
	g_BS_OfAttribData[39].fBearish 	=  1	
	g_BS_OfAttribData[39].fCeiling 	=  1000	
		g_BS_OfAttribData[39].fFloor = 3.5  
		g_BS_OfAttribData[39].fStepCap = 3.5
		g_BS_OfAttribData[39].fNoise = 0.1 
	//Offline price calculation attribs for Bilkinton
	g_BS_OfAttribData[40].fBullish 	=  1	
	g_BS_OfAttribData[40].fBearish 	=  1	
	g_BS_OfAttribData[40].fCeiling 	=  1000	
		g_BS_OfAttribData[40].fFloor = 3.5  
		g_BS_OfAttribData[40].fStepCap = 3.5
		g_BS_OfAttribData[40].fNoise = 0.1 

	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_BFA].uploadTo 	= 	SM_BRVECDESBFA //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_BFA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_BRU].uploadTo 	= 	SM_BRVECDESBRU //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_BRU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_LSC].uploadTo 	= 	SM_BRVECDESLSC //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_LSC].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_LST].uploadTo 	= 	SM_BRVECDESLST //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_LST].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_LTD].uploadTo 	= 	SM_BRVECDESLTD //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_LTD].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_MAI].uploadTo 	= 	SM_BRVECDESMAI //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_MAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_RON].uploadTo 	= 	SM_BRVECDESRON //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_RON].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_SHT].uploadTo 	= 	SM_BRVECDESSHT //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_SHT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_UMA].uploadTo 	= 	SM_BRVECDESUMA //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_UMA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_VAP].uploadTo 	= 	SM_BRVECDESVAP //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_VAP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_HVY].uploadTo 	= 	SM_BRVECDESHVY //BrandedVehiclesDestroyed
	g_BS_Modifiers[BSMF_SM_BRVECDES_FOR_HVY].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECBUY_FOR_BFA].uploadTo 	= 	SM_VECBUYBFA //VehiclesOfTypeBought
	g_BS_Modifiers[BSMF_SM_VECBUY_FOR_BFA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECBUY_FOR_BRU].uploadTo 	= 	SM_VECBUYBRU //VehiclesOfTypeBought
	g_BS_Modifiers[BSMF_SM_VECBUY_FOR_BRU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECBUY_FOR_MAI].uploadTo 	= 	SM_VECBUYMAI //VehiclesOfTypeBought
	g_BS_Modifiers[BSMF_SM_VECBUY_FOR_MAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECBUY_FOR_SHT].uploadTo 	= 	SM_VECBUYSHT //VehiclesOfTypeBought
	g_BS_Modifiers[BSMF_SM_VECBUY_FOR_SHT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECBUY_FOR_UMA].uploadTo 	= 	SM_VECBUYUMA //VehiclesOfTypeBought
	g_BS_Modifiers[BSMF_SM_VECBUY_FOR_UMA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECBUY_FOR_VAP].uploadTo 	= 	SM_VECBUYVAP //VehiclesOfTypeBought
	g_BS_Modifiers[BSMF_SM_VECBUY_FOR_VAP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECBUY_FOR_HVY].uploadTo 	= 	SM_VECBUYHVY //VehiclesOfTypeBought
	g_BS_Modifiers[BSMF_SM_VECBUY_FOR_HVY].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_DISDRIV_FOR_BFA].uploadTo 	= 	SM_DISDRIVBFA //DistanceDrivenInType
	g_BS_Modifiers[BSMF_SM_DISDRIV_FOR_BFA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_DISDRIV_FOR_BRU].uploadTo 	= 	SM_DISDRIVBRU //DistanceDrivenInType
	g_BS_Modifiers[BSMF_SM_DISDRIV_FOR_BRU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_DISDRIV_FOR_LST].uploadTo 	= 	SM_DISDRIVLST //DistanceDrivenInType
	g_BS_Modifiers[BSMF_SM_DISDRIV_FOR_LST].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_DISDRIV_FOR_MAI].uploadTo 	= 	SM_DISDRIVMAI //DistanceDrivenInType
	g_BS_Modifiers[BSMF_SM_DISDRIV_FOR_MAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_DISDRIV_FOR_SHT].uploadTo 	= 	SM_DISDRIVSHT //DistanceDrivenInType
	g_BS_Modifiers[BSMF_SM_DISDRIV_FOR_SHT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_DISDRIV_FOR_UMA].uploadTo 	= 	SM_DISDRIVUMA //DistanceDrivenInType
	g_BS_Modifiers[BSMF_SM_DISDRIV_FOR_UMA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_DISDRIV_FOR_VAP].uploadTo 	= 	SM_DISDRIVVAP //DistanceDrivenInType
	g_BS_Modifiers[BSMF_SM_DISDRIV_FOR_VAP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_DISDRIV_FOR_HVY].uploadTo 	= 	SM_DISDRIVHVY //DistanceDrivenInType
	g_BS_Modifiers[BSMF_SM_DISDRIV_FOR_HVY].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECMOD_FOR_LSC].uploadTo 	= 	SM_VECMODLSC //VehicleModification
	g_BS_Modifiers[BSMF_SM_VECMOD_FOR_LSC].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECSTOL_FOR_BFA].uploadTo 	= 	SM_VECSTOLBFA //VehicleOfTypeStolen
	g_BS_Modifiers[BSMF_SM_VECSTOL_FOR_BFA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECSTOL_FOR_BRU].uploadTo 	= 	SM_VECSTOLBRU //VehicleOfTypeStolen
	g_BS_Modifiers[BSMF_SM_VECSTOL_FOR_BRU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECSTOL_FOR_LST].uploadTo 	= 	SM_VECSTOLLST //VehicleOfTypeStolen
	g_BS_Modifiers[BSMF_SM_VECSTOL_FOR_LST].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECSTOL_FOR_MAI].uploadTo 	= 	SM_VECSTOLMAI //VehicleOfTypeStolen
	g_BS_Modifiers[BSMF_SM_VECSTOL_FOR_MAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECSTOL_FOR_SHT].uploadTo 	= 	SM_VECSTOLSHT //VehicleOfTypeStolen
	g_BS_Modifiers[BSMF_SM_VECSTOL_FOR_SHT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECSTOL_FOR_UMA].uploadTo 	= 	SM_VECSTOLUMA //VehicleOfTypeStolen
	g_BS_Modifiers[BSMF_SM_VECSTOL_FOR_UMA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECSTOL_FOR_VAP].uploadTo 	= 	SM_VECSTOLVAP //VehicleOfTypeStolen
	g_BS_Modifiers[BSMF_SM_VECSTOL_FOR_VAP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECSTOL_FOR_HVY].uploadTo 	= 	SM_VECSTOLHVY //VehicleOfTypeStolen
	g_BS_Modifiers[BSMF_SM_VECSTOL_FOR_HVY].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECDMG_FOR_BFA].uploadTo 	= 	SM_VECDMGBFA //DamageDoneToVehicleType
	g_BS_Modifiers[BSMF_SM_VECDMG_FOR_BFA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECDMG_FOR_BRU].uploadTo 	= 	SM_VECDMGBRU //DamageDoneToVehicleType
	g_BS_Modifiers[BSMF_SM_VECDMG_FOR_BRU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECDMG_FOR_MAI].uploadTo 	= 	SM_VECDMGMAI //DamageDoneToVehicleType
	g_BS_Modifiers[BSMF_SM_VECDMG_FOR_MAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECDMG_FOR_SHT].uploadTo 	= 	SM_VECDMGSHT //DamageDoneToVehicleType
	g_BS_Modifiers[BSMF_SM_VECDMG_FOR_SHT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECDMG_FOR_UMA].uploadTo 	= 	SM_VECDMGUMA //DamageDoneToVehicleType
	g_BS_Modifiers[BSMF_SM_VECDMG_FOR_UMA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECDMG_FOR_VAP].uploadTo 	= 	SM_VECDMGVAP //DamageDoneToVehicleType
	g_BS_Modifiers[BSMF_SM_VECDMG_FOR_VAP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECDMG_FOR_HVY].uploadTo 	= 	SM_VECDMGHVY //DamageDoneToVehicleType
	g_BS_Modifiers[BSMF_SM_VECDMG_FOR_HVY].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VECPEDKIL].uploadTo 	= 	SM_VECPEDKIL //PeopleKilledWithVehicle
	g_BS_Modifiers[BSMF_SM_VECPEDKIL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_WEPBUY_FOR_SHR].uploadTo 	= 	SM_WEPBUYSHR //WeaponTypeBought
	g_BS_Modifiers[BSMF_SM_WEPBUY_FOR_SHR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_WEPBUY_FOR_HAL].uploadTo 	= 	SM_WEPBUYHAL //WeaponTypeBought
	g_BS_Modifiers[BSMF_SM_WEPBUY_FOR_HAL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_WEPTAKE_FOR_VOM].uploadTo 	= 	SM_WEPTAKEVOM //WeaponTypeTaken
	g_BS_Modifiers[BSMF_SM_WEPTAKE_FOR_VOM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_WEPTAKE_FOR_SHR].uploadTo 	= 	SM_WEPTAKESHR //WeaponTypeTaken
	g_BS_Modifiers[BSMF_SM_WEPTAKE_FOR_SHR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_WEPTAKE_FOR_HAL].uploadTo 	= 	SM_WEPTAKEHAL //WeaponTypeTaken
	g_BS_Modifiers[BSMF_SM_WEPTAKE_FOR_HAL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILCOP_FOR_VOM].uploadTo 	= 	SM_KILCOPVOM //CopsKilledWithWeaponType
	g_BS_Modifiers[BSMF_SM_KILCOP_FOR_VOM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILCOP_FOR_SHR].uploadTo 	= 	SM_KILCOPSHR //CopsKilledWithWeaponType
	g_BS_Modifiers[BSMF_SM_KILCOP_FOR_SHR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILCOP_FOR_HAL].uploadTo 	= 	SM_KILCOPHAL //CopsKilledWithWeaponType
	g_BS_Modifiers[BSMF_SM_KILCOP_FOR_HAL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILCRIM_FOR_VOM].uploadTo 	= 	SM_KILCRIMVOM //CriminalsKilledWithWeaponType
	g_BS_Modifiers[BSMF_SM_KILCRIM_FOR_VOM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILCRIM_FOR_SHR].uploadTo 	= 	SM_KILCRIMSHR //CriminalsKilledWithWeaponType
	g_BS_Modifiers[BSMF_SM_KILCRIM_FOR_SHR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILCRIM_FOR_HAL].uploadTo 	= 	SM_KILCRIMHAL //CriminalsKilledWithWeaponType
	g_BS_Modifiers[BSMF_SM_KILCRIM_FOR_HAL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILCIV_FOR_MAI].uploadTo 	= 	SM_KILCIVMAI //CiviliansKilledWithWeaponType
	g_BS_Modifiers[BSMF_SM_KILCIV_FOR_MAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILCIV_FOR_SHT].uploadTo 	= 	SM_KILCIVSHT //CiviliansKilledWithWeaponType
	g_BS_Modifiers[BSMF_SM_KILCIV_FOR_SHT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILCIV_FOR_UMA].uploadTo 	= 	SM_KILCIVUMA //CiviliansKilledWithWeaponType
	g_BS_Modifiers[BSMF_SM_KILCIV_FOR_UMA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILCIV_FOR_VOM].uploadTo 	= 	SM_KILCIVVOM //CiviliansKilledWithWeaponType
	g_BS_Modifiers[BSMF_SM_KILCIV_FOR_VOM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILCIV_FOR_SHR].uploadTo 	= 	SM_KILCIVSHR //CiviliansKilledWithWeaponType
	g_BS_Modifiers[BSMF_SM_KILCIV_FOR_SHR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILCIV_FOR_HAL].uploadTo 	= 	SM_KILCIVHAL //CiviliansKilledWithWeaponType
	g_BS_Modifiers[BSMF_SM_KILCIV_FOR_HAL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_VENUSE_FOR_SPU].uploadTo 	= 	SM_VENUSESPU //VendingMachineOftypeUsed
	g_BS_Modifiers[BSMF_SM_VENUSE_FOR_SPU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_NEWDAM].uploadTo 	= 	SM_NEWDAM //DamageToNewsagentStalls
	g_BS_Modifiers[BSMF_SM_NEWDAM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_HPKIL].uploadTo 	= 	SM_HPKIL //HealthyPeopleKilled
	g_BS_Modifiers[BSMF_SM_HPKIL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_PUBCLUB].uploadTo 	= 	SM_PUBCLUB //PubandClubvisits
	g_BS_Modifiers[BSMF_SM_PUBCLUB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_TDRNK].uploadTo 	= 	SM_TDRNK //TimesDrunk
	g_BS_Modifiers[BSMF_SM_TDRNK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_FRNPUB].uploadTo 	= 	SM_FRNPUB //FriendsTakenToPubsclubs
	g_BS_Modifiers[BSMF_SM_FRNPUB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_DRNKCRM].uploadTo 	= 	SM_DRNKCRM //DrunkenCrimes
	g_BS_Modifiers[BSMF_SM_DRNKCRM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_RAMCOM].uploadTo 	= 	SM_RAMCOM //RampagesComplete
	g_BS_Modifiers[BSMF_SM_RAMCOM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_RAD_FOR_CNT].uploadTo 	= 	SM_RADCNT //RadioStationsListenedTo
	g_BS_Modifiers[BSMF_SM_RAD_FOR_CNT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_RAD_FOR_WZL].uploadTo 	= 	SM_RADWZL //RadioStationsListenedTo
	g_BS_Modifiers[BSMF_SM_RAD_FOR_WZL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_RAD_FOR_ZIT].uploadTo 	= 	SM_RADZIT //RadioStationsListenedTo
	g_BS_Modifiers[BSMF_SM_RAD_FOR_ZIT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_ZITIT_FOR_CNT].uploadTo 	= 	SM_ZITITCNT //SongsIDedWithZIT
	g_BS_Modifiers[BSMF_SM_ZITIT_FOR_CNT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_ZITIT_FOR_WZL].uploadTo 	= 	SM_ZITITWZL //SongsIDedWithZIT
	g_BS_Modifiers[BSMF_SM_ZITIT_FOR_WZL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_ZITIT_FOR_ZIT].uploadTo 	= 	SM_ZITITZIT //SongsIDedWithZIT
	g_BS_Modifiers[BSMF_SM_ZITIT_FOR_ZIT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_RADCHA_FOR_CNT].uploadTo 	= 	SM_RADCHACNT //TimesChangedAwayFromRadiostation
	g_BS_Modifiers[BSMF_SM_RADCHA_FOR_CNT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_RADCHA_FOR_WZL].uploadTo 	= 	SM_RADCHAWZL //TimesChangedAwayFromRadiostation
	g_BS_Modifiers[BSMF_SM_RADCHA_FOR_WZL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_PARA].uploadTo 	= 	SM_PARA //ParaJumps
	g_BS_Modifiers[BSMF_SM_PARA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_TKFIRE].uploadTo 	= 	SM_TKFIRE //BeingOnFireTicks
	g_BS_Modifiers[BSMF_SM_TKFIRE].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_FIBAI].uploadTo 	= 	SM_FIBAI //BailsFromVehicleWhenOnFire
	g_BS_Modifiers[BSMF_SM_FIBAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_TANDES].uploadTo 	= 	SM_TANDES //TankersDestroyed
	g_BS_Modifiers[BSMF_SM_TANDES].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_GAREP].uploadTo 	= 	SM_GAREP //GarageRepairsMade
	g_BS_Modifiers[BSMF_SM_GAREP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_GAMONSP].uploadTo 	= 	SM_GAMONSP //MoneySpentAtGarage
	g_BS_Modifiers[BSMF_SM_GAMONSP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_MONB].uploadTo 	= 	SM_MONB //CarsModifiedThenNotBought
	g_BS_Modifiers[BSMF_SM_MONB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_MONUP_FOR_SHK].uploadTo 	= 	SM_MONUPSHK //MoneyGainedTotal
	g_BS_Modifiers[BSMF_SM_MONUP_FOR_SHK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_TAXDEST].uploadTo 	= 	SM_TAXDEST //TaxisDestroyed
	g_BS_Modifiers[BSMF_SM_TAXDEST].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILW_FOR_BFA].uploadTo 	= 	SM_KILWBFA //PeopleKilledWith
	g_BS_Modifiers[BSMF_SM_KILW_FOR_BFA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILW_FOR_BRU].uploadTo 	= 	SM_KILWBRU //PeopleKilledWith
	g_BS_Modifiers[BSMF_SM_KILW_FOR_BRU].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILW_FOR_MAI].uploadTo 	= 	SM_KILWMAI //PeopleKilledWith
	g_BS_Modifiers[BSMF_SM_KILW_FOR_MAI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILW_FOR_SHT].uploadTo 	= 	SM_KILWSHT //PeopleKilledWith
	g_BS_Modifiers[BSMF_SM_KILW_FOR_SHT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILW_FOR_UMA].uploadTo 	= 	SM_KILWUMA //PeopleKilledWith
	g_BS_Modifiers[BSMF_SM_KILW_FOR_UMA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILW_FOR_VAP].uploadTo 	= 	SM_KILWVAP //PeopleKilledWith
	g_BS_Modifiers[BSMF_SM_KILW_FOR_VAP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILW_FOR_VOM].uploadTo 	= 	SM_KILWVOM //PeopleKilledWith
	g_BS_Modifiers[BSMF_SM_KILW_FOR_VOM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILW_FOR_HVY].uploadTo 	= 	SM_KILWHVY //PeopleKilledWith
	g_BS_Modifiers[BSMF_SM_KILW_FOR_HVY].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_CLOBOF_FOR_BIN].uploadTo 	= 	SM_CLOBOFBIN //ClothesBoughtFrom
	g_BS_Modifiers[BSMF_SM_CLOBOF_FOR_BIN].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_CLOBOF_FOR_PKW].uploadTo 	= 	SM_CLOBOFPKW //ClothesBoughtFrom
	g_BS_Modifiers[BSMF_SM_CLOBOF_FOR_PKW].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_CLOBOF_FOR_PON].uploadTo 	= 	SM_CLOBOFPON //ClothesBoughtFrom
	g_BS_Modifiers[BSMF_SM_CLOBOF_FOR_PON].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_YOGA].uploadTo 	= 	SM_YOGA //TimesCompletedYoga
	g_BS_Modifiers[BSMF_SM_YOGA].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_TRI].uploadTo 	= 	SM_TRI //TimesCompletedTriathlon
	g_BS_Modifiers[BSMF_SM_TRI].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_GYM].uploadTo 	= 	SM_GYM //TimeSpentAtGym
	g_BS_Modifiers[BSMF_SM_GYM].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_STRIP].uploadTo 	= 	SM_STRIP //TicksAtStripClub
	g_BS_Modifiers[BSMF_SM_STRIP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_UGHOK].uploadTo 	= 	SM_UGHOK //UglyHookersKilled
	g_BS_Modifiers[BSMF_SM_UGHOK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_STRTRO].uploadTo 	= 	SM_STRTRO //TroubleCauseAtStripClub
	g_BS_Modifiers[BSMF_SM_STRTRO].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_PISCO].uploadTo 	= 	SM_PISCO //PilotSchoolUse
	g_BS_Modifiers[BSMF_SM_PISCO].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_TOTINJ].uploadTo 	= 	SM_TOTINJ //PeopleInjured
	g_BS_Modifiers[BSMF_SM_TOTINJ].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_DRUGKIL].uploadTo 	= 	SM_DRUGKIL //DrugDealersKilled
	g_BS_Modifiers[BSMF_SM_DRUGKIL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_HANGOVR].uploadTo 	= 	SM_HANGOVR //NumberOfHangOvers
	g_BS_Modifiers[BSMF_SM_HANGOVR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_KILLSPR].uploadTo 	= 	SM_KILLSPR //KillingSprees
	g_BS_Modifiers[BSMF_SM_KILLSPR].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_PEDFIREKILL].uploadTo 	= 	SM_PEDFIREKILL //PedsKilledByFire
	g_BS_Modifiers[BSMF_SM_PEDFIREKILL].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_PEDFIRETICK].uploadTo 	= 	SM_PEDFIRETICK //PedsOnFireTimes
	g_BS_Modifiers[BSMF_SM_PEDFIRETICK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_TVTICK_FOR_WAP].uploadTo 	= 	SM_TVTICKWAP //TVTypeUsage
	g_BS_Modifiers[BSMF_SM_TVTICK_FOR_WAP].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_TVTICK_FOR_WIW].uploadTo 	= 	SM_TVTICKWIW //TVTypeUsage
	g_BS_Modifiers[BSMF_SM_TVTICK_FOR_WIW].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_ZITPOP_FOR_ZIT].uploadTo 	= 	SM_ZITPOPZIT //ZitPopularitySongGroup
	g_BS_Modifiers[BSMF_SM_ZITPOP_FOR_ZIT].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_CARAPP].uploadTo 	= 	SM_CARAPP //TimesUsedCarApp
	g_BS_Modifiers[BSMF_SM_CARAPP].bOnline 	= TRUE	
	//g_BS_Modifiers[BSMF_SM_HAIRCUTS].uploadTo 	= 	SM_HAIRCUTS //TimesHairCut
	//g_BS_Modifiers[BSMF_SM_HAIRCUTS].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_STOROB].uploadTo 	= 	SM_STOROB //StoreRobberiesProfits
	g_BS_Modifiers[BSMF_SM_STOROB].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_PHONCAL_FOR_BDG].uploadTo 	= 	SM_PHONCALBDG //SPLIT_PHONE_CALLS
	g_BS_Modifiers[BSMF_SM_PHONCAL_FOR_BDG].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_PHONCAL_FOR_TNK].uploadTo 	= 	SM_PHONCALTNK //SPLIT_PHONE_CALLS
	g_BS_Modifiers[BSMF_SM_PHONCAL_FOR_TNK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_PHONCAL_FOR_WIZ].uploadTo 	= 	SM_PHONCALWIZ //SPLIT_PHONE_CALLS
	g_BS_Modifiers[BSMF_SM_PHONCAL_FOR_WIZ].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_PHONTXT_FOR_BDG].uploadTo 	= 	SM_PHONTXTBDG //SPLIT_PHONE_TEXTS
	g_BS_Modifiers[BSMF_SM_PHONTXT_FOR_BDG].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_PHONTXT_FOR_TNK].uploadTo 	= 	SM_PHONTXTTNK //SPLIT_PHONE_TEXTS
	g_BS_Modifiers[BSMF_SM_PHONTXT_FOR_TNK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_PHONTXT_FOR_WIZ].uploadTo 	= 	SM_PHONTXTWIZ //SPLIT_PHONE_TEXTS
	g_BS_Modifiers[BSMF_SM_PHONTXT_FOR_WIZ].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_CHTICK_FOR_BDG].uploadTo 	= 	SM_CHTICKBDG //SPLIT_TICKS_ON_CHAR
	g_BS_Modifiers[BSMF_SM_CHTICK_FOR_BDG].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_CHTICK_FOR_TNK].uploadTo 	= 	SM_CHTICKTNK //SPLIT_TICKS_ON_CHAR
	g_BS_Modifiers[BSMF_SM_CHTICK_FOR_TNK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_CHTICK_FOR_WIZ].uploadTo 	= 	SM_CHTICKWIZ //SPLIT_TICKS_ON_CHAR
	g_BS_Modifiers[BSMF_SM_CHTICK_FOR_WIZ].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_CALCAN_FOR_BDG].uploadTo 	= 	SM_CALCANBDG //SPLIT_CALLS_CANCELED
	g_BS_Modifiers[BSMF_SM_CALCAN_FOR_BDG].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_CALCAN_FOR_TNK].uploadTo 	= 	SM_CALCANTNK //SPLIT_CALLS_CANCELED
	g_BS_Modifiers[BSMF_SM_CALCAN_FOR_TNK].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_CALCAN_FOR_WIZ].uploadTo 	= 	SM_CALCANWIZ //SPLIT_CALLS_CANCELED
	g_BS_Modifiers[BSMF_SM_CALCAN_FOR_WIZ].bOnline 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_RUNDIST_M].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_RUNDIST_M].ProfStatToRead 	= SP0_DIST_WALKING	
	g_BS_Modifiers[BSMF_SM_SP_RUNDIST_M].bProfIsFloatType 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_RUNDIST_F].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_RUNDIST_F].ProfStatToRead 	= SP1_DIST_WALKING	
	g_BS_Modifiers[BSMF_SM_SP_RUNDIST_F].bProfIsFloatType 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_RUNDIST_T].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_RUNDIST_T].ProfStatToRead 	= SP2_DIST_WALKING	
	g_BS_Modifiers[BSMF_SM_SP_RUNDIST_T].bProfIsFloatType 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_BIKEDIST_M].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_BIKEDIST_M].ProfStatToRead 	= SP0_DIST_DRIVING_BICYCLE	
	g_BS_Modifiers[BSMF_SM_SP_BIKEDIST_M].bProfIsFloatType 	= True	
	g_BS_Modifiers[BSMF_SM_SP_BIKEDIST_F].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_BIKEDIST_F].ProfStatToRead 	= SP1_DIST_DRIVING_BICYCLE	
	g_BS_Modifiers[BSMF_SM_SP_BIKEDIST_F].bProfIsFloatType 	= True	
	g_BS_Modifiers[BSMF_SM_SP_BIKEDIST_T].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_BIKEDIST_T].ProfStatToRead 	= SP2_DIST_DRIVING_BICYCLE	
	g_BS_Modifiers[BSMF_SM_SP_BIKEDIST_T].bProfIsFloatType 	= True	
	g_BS_Modifiers[BSMF_SM_SP_CARBAILS_M].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_CARBAILS_M].ProfStatToRead 	= SP0_BAILED_FROM_VEHICLE	
	g_BS_Modifiers[BSMF_SM_SP_CARBAILS_M].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_SP_CARBAILS_F].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_CARBAILS_F].ProfStatToRead 	= SP1_BAILED_FROM_VEHICLE	
	g_BS_Modifiers[BSMF_SM_SP_CARBAILS_F].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_SP_CARBAILS_T].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_CARBAILS_T].ProfStatToRead 	= SP2_BAILED_FROM_VEHICLE	
	g_BS_Modifiers[BSMF_SM_SP_CARBAILS_T].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_SP_LCRASH_M].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_LCRASH_M].ProfStatToRead 	= SP0_LARGE_ACCIDENTS	
	g_BS_Modifiers[BSMF_SM_SP_LCRASH_M].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_SP_LCRASH_F].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_LCRASH_F].ProfStatToRead 	= SP1_LARGE_ACCIDENTS	
	g_BS_Modifiers[BSMF_SM_SP_LCRASH_F].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_SP_LCRASH_T].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_LCRASH_T].ProfStatToRead 	= SP2_LARGE_ACCIDENTS	
	g_BS_Modifiers[BSMF_SM_SP_LCRASH_T].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_SP_LKILLS_M].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_LKILLS_M].ProfStatToRead 	= SP0_TOTAL_LEGITIMATE_KILLS	
	g_BS_Modifiers[BSMF_SM_SP_LKILLS_M].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_SP_LKILLS_F].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_LKILLS_F].ProfStatToRead 	= SP1_TOTAL_LEGITIMATE_KILLS	
	g_BS_Modifiers[BSMF_SM_SP_LKILLS_F].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_SP_LKILLS_T].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SP_LKILLS_T].ProfStatToRead 	= SP2_TOTAL_LEGITIMATE_KILLS	
	g_BS_Modifiers[BSMF_SM_SP_LKILLS_T].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_MONTAX_M].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_MONTAX_M].ProfStatToRead 	= SP0_MONEY_SPENT_ON_TAXIS	
	g_BS_Modifiers[BSMF_SM_MONTAX_M].bProfIsFloatType 	= FALSE	
	g_BS_Modifiers[BSMF_SM_MONTAX_F].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_MONTAX_F].ProfStatToRead 	= SP1_MONEY_SPENT_ON_TAXIS	
	g_BS_Modifiers[BSMF_SM_MONTAX_F].bProfIsFloatType 	= FALSE	
	g_BS_Modifiers[BSMF_SM_MONTAX_T].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_MONTAX_T].ProfStatToRead 	= SP2_MONEY_SPENT_ON_TAXIS	
	g_BS_Modifiers[BSMF_SM_MONTAX_T].bProfIsFloatType 	= FALSE	
	g_BS_Modifiers[BSMF_SM_HECAR_M].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_HECAR_M].ProfStatToRead 	= SP0_MONEY_SPENT_ON_HEALTHCARE	
	g_BS_Modifiers[BSMF_SM_HECAR_M].bProfIsFloatType 	= FALSE	
	g_BS_Modifiers[BSMF_SM_HECAR_F].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_HECAR_F].ProfStatToRead 	= SP1_MONEY_SPENT_ON_HEALTHCARE	
	g_BS_Modifiers[BSMF_SM_HECAR_F].bProfIsFloatType 	= FALSE	
	g_BS_Modifiers[BSMF_SM_HECAR_T].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_HECAR_T].ProfStatToRead 	= SP2_MONEY_SPENT_ON_HEALTHCARE	
	g_BS_Modifiers[BSMF_SM_HECAR_T].bProfIsFloatType 	= FALSE	
	g_BS_Modifiers[BSMF_SM_MONBRI_M].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_MONBRI_M].ProfStatToRead 	= SP0_MONEY_SPENT_IN_COP_BRIBES	
	g_BS_Modifiers[BSMF_SM_MONBRI_M].bProfIsFloatType 	= FALSE	
	g_BS_Modifiers[BSMF_SM_MONBRI_F].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_MONBRI_F].ProfStatToRead 	= SP1_MONEY_SPENT_IN_COP_BRIBES	
	g_BS_Modifiers[BSMF_SM_MONBRI_F].bProfIsFloatType 	= FALSE	
	g_BS_Modifiers[BSMF_SM_MONBRI_T].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_MONBRI_T].ProfStatToRead 	= SP2_MONEY_SPENT_IN_COP_BRIBES	
	g_BS_Modifiers[BSMF_SM_MONBRI_T].bProfIsFloatType 	= FALSE	
	g_BS_Modifiers[BSMF_SM_BUST_M].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_BUST_M].ProfStatToRead 	= SP0_BUSTED	
	g_BS_Modifiers[BSMF_SM_BUST_M].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_BUST_F].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_BUST_F].ProfStatToRead 	= SP1_BUSTED	
	g_BS_Modifiers[BSMF_SM_BUST_F].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_BUST_T].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_BUST_T].ProfStatToRead 	= SP2_BUSTED	
	g_BS_Modifiers[BSMF_SM_BUST_T].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_FALDE_M].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_FALDE_M].ProfStatToRead 	= SP0_DIED_IN_FALL	
	g_BS_Modifiers[BSMF_SM_FALDE_M].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_FALDE_F].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_FALDE_F].ProfStatToRead 	= SP1_DIED_IN_FALL	
	g_BS_Modifiers[BSMF_SM_FALDE_F].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_FALDE_T].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_FALDE_T].ProfStatToRead 	= SP2_DIED_IN_FALL	
	g_BS_Modifiers[BSMF_SM_FALDE_T].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_STATAI_M].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_STATAI_M].ProfStatToRead 	= SP0_STARS_ATTAINED	
	g_BS_Modifiers[BSMF_SM_STATAI_M].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_STATAI_F].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_STATAI_F].ProfStatToRead 	= SP1_STARS_ATTAINED	
	g_BS_Modifiers[BSMF_SM_STATAI_F].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_STATAI_T].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_STATAI_T].ProfStatToRead 	= SP2_STARS_ATTAINED	
	g_BS_Modifiers[BSMF_SM_STATAI_T].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_STALOS_M].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_STALOS_M].ProfStatToRead 	= SP0_STARS_EVADED	
	g_BS_Modifiers[BSMF_SM_STALOS_M].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_STALOS_F].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_STALOS_F].ProfStatToRead 	= SP1_STARS_EVADED	
	g_BS_Modifiers[BSMF_SM_STALOS_F].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_STALOS_T].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_STALOS_T].ProfStatToRead 	= SP2_STARS_EVADED	
	g_BS_Modifiers[BSMF_SM_STALOS_T].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_SAVE_M].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SAVE_M].ProfStatToRead 	= SP0_MANUAL_SAVED	
	g_BS_Modifiers[BSMF_SM_SAVE_M].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_SAVE_F].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SAVE_F].ProfStatToRead 	= SP1_MANUAL_SAVED	
	g_BS_Modifiers[BSMF_SM_SAVE_F].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_SAVE_T].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SAVE_T].ProfStatToRead 	= SP2_MANUAL_SAVED	
	g_BS_Modifiers[BSMF_SM_SAVE_T].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_COPKIL_M].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_COPKIL_M].ProfStatToRead 	= SP0_KILLS_COP	
	g_BS_Modifiers[BSMF_SM_COPKIL_M].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_COPKIL_F].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_COPKIL_F].ProfStatToRead 	= SP1_KILLS_COP	
	g_BS_Modifiers[BSMF_SM_COPKIL_F].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_COPKIL_T].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_COPKIL_T].ProfStatToRead 	= SP2_KILLS_COP	
	g_BS_Modifiers[BSMF_SM_COPKIL_T].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_SWAKIL_M].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SWAKIL_M].ProfStatToRead 	= SP0_KILLS_SWAT	
	g_BS_Modifiers[BSMF_SM_SWAKIL_M].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_SWAKIL_F].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SWAKIL_F].ProfStatToRead 	= SP1_KILLS_SWAT	
	g_BS_Modifiers[BSMF_SM_SWAKIL_F].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SM_SWAKIL_T].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SM_SWAKIL_T].ProfStatToRead 	= SP2_KILLS_SWAT	
	g_BS_Modifiers[BSMF_SM_SWAKIL_T].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SP_COTI_M].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SP_COTI_M].ProfStatToRead 	= SP0_TIME_IN_COVER	
	g_BS_Modifiers[BSMF_SP_COTI_M].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SP_COTI_F].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SP_COTI_F].ProfStatToRead 	= SP1_TIME_IN_COVER	
	g_BS_Modifiers[BSMF_SP_COTI_F].bProfIsFloatType 	= False	
	g_BS_Modifiers[BSMF_SP_COTI_T].bReadFromProfile 	= TRUE	
	g_BS_Modifiers[BSMF_SP_COTI_T].ProfStatToRead 	= SP2_TIME_IN_COVER	
	g_BS_Modifiers[BSMF_SP_COTI_T].bProfIsFloatType 	= False	

	g_BS_OIndexData[0].OnlinePriceStat 	=  SM_PRICE_AMU	
		g_BS_OIndexData[0].OnlineCharOwned[0] 	= AMU_OW0  //mike
		g_BS_OIndexData[0].OnlineCharOwned[1] 	= AMU_OW1 //frank
		g_BS_OIndexData[0].OnlineCharOwned[2] 	= AMU_OW2  //trev
	g_BS_OIndexData[1].OnlinePriceStat 	=  SM_PRICE_BDG	
		g_BS_OIndexData[1].OnlineCharOwned[0] 	= BDG_OW0  //mike
		g_BS_OIndexData[1].OnlineCharOwned[1] 	= BDG_OW1 //frank
		g_BS_OIndexData[1].OnlineCharOwned[2] 	= BDG_OW2  //trev
	g_BS_OIndexData[2].OnlinePriceStat 	=  SM_PRICE_BFA	
		g_BS_OIndexData[2].OnlineCharOwned[0] 	= BFA_OW0  //mike
		g_BS_OIndexData[2].OnlineCharOwned[1] 	= BFA_OW1 //frank
		g_BS_OIndexData[2].OnlineCharOwned[2] 	= BFA_OW2  //trev
	g_BS_OIndexData[3].OnlinePriceStat 	=  SM_PRICE_BIN	
		g_BS_OIndexData[3].OnlineCharOwned[0] 	= BIN_OW0  //mike
		g_BS_OIndexData[3].OnlineCharOwned[1] 	= BIN_OW1 //frank
		g_BS_OIndexData[3].OnlineCharOwned[2] 	= BIN_OW2  //trev
	g_BS_OIndexData[4].OnlinePriceStat 	=  SM_PRICE_BTR	
		g_BS_OIndexData[4].OnlineCharOwned[0] 	= BTR_OW0  //mike
		g_BS_OIndexData[4].OnlineCharOwned[1] 	= BTR_OW1 //frank
		g_BS_OIndexData[4].OnlineCharOwned[2] 	= BTR_OW2  //trev
	g_BS_OIndexData[5].OnlinePriceStat 	=  SM_PRICE_BLE	
		g_BS_OIndexData[5].OnlineCharOwned[0] 	= BLE_OW0  //mike
		g_BS_OIndexData[5].OnlineCharOwned[1] 	= BLE_OW1 //frank
		g_BS_OIndexData[5].OnlineCharOwned[2] 	= BLE_OW2  //trev
	g_BS_OIndexData[6].OnlinePriceStat 	=  SM_PRICE_BRU	
		g_BS_OIndexData[6].OnlineCharOwned[0] 	= BRU_OW0  //mike
		g_BS_OIndexData[6].OnlineCharOwned[1] 	= BRU_OW1 //frank
		g_BS_OIndexData[6].OnlineCharOwned[2] 	= BRU_OW2  //trev
	g_BS_OIndexData[7].OnlinePriceStat 	=  SM_PRICE_CNT	
		g_BS_OIndexData[7].OnlineCharOwned[0] 	= CNT_OW0  //mike
		g_BS_OIndexData[7].OnlineCharOwned[1] 	= CNT_OW1 //frank
		g_BS_OIndexData[7].OnlineCharOwned[2] 	= CNT_OW2  //trev
	g_BS_OIndexData[8].OnlinePriceStat 	=  SM_PRICE_CRE	
		g_BS_OIndexData[8].OnlineCharOwned[0] 	= CRE_OW0  //mike
		g_BS_OIndexData[8].OnlineCharOwned[1] 	= CRE_OW1 //frank
		g_BS_OIndexData[8].OnlineCharOwned[2] 	= CRE_OW2  //trev
	g_BS_OIndexData[9].OnlinePriceStat 	=  SM_PRICE_DGP	
		g_BS_OIndexData[9].OnlineCharOwned[0] 	= DGP_OW0  //mike
		g_BS_OIndexData[9].OnlineCharOwned[1] 	= DGP_OW1 //frank
		g_BS_OIndexData[9].OnlineCharOwned[2] 	= DGP_OW2  //trev
	g_BS_OIndexData[10].OnlinePriceStat 	=  SM_PRICE_WAP	
		g_BS_OIndexData[10].OnlineCharOwned[0] 	= WAP_OW0  //mike
		g_BS_OIndexData[10].OnlineCharOwned[1] 	= WAP_OW1 //frank
		g_BS_OIndexData[10].OnlineCharOwned[2] 	= WAP_OW2  //trev
	g_BS_OIndexData[11].OnlinePriceStat 	=  SM_PRICE_FAC	
		g_BS_OIndexData[11].OnlineCharOwned[0] 	= FAC_OW0  //mike
		g_BS_OIndexData[11].OnlineCharOwned[1] 	= FAC_OW1 //frank
		g_BS_OIndexData[11].OnlineCharOwned[2] 	= FAC_OW2  //trev
	g_BS_OIndexData[12].OnlinePriceStat 	=  SM_PRICE_FRT	
		g_BS_OIndexData[12].OnlineCharOwned[0] 	= FRT_OW0  //mike
		g_BS_OIndexData[12].OnlineCharOwned[1] 	= FRT_OW1 //frank
		g_BS_OIndexData[12].OnlineCharOwned[2] 	= FRT_OW2  //trev
	g_BS_OIndexData[13].OnlinePriceStat 	=  SM_PRICE_LSC	
		g_BS_OIndexData[13].OnlineCharOwned[0] 	= LSC_OW0  //mike
		g_BS_OIndexData[13].OnlineCharOwned[1] 	= LSC_OW1 //frank
		g_BS_OIndexData[13].OnlineCharOwned[2] 	= LSC_OW2  //trev
	g_BS_OIndexData[14].OnlinePriceStat 	=  SM_PRICE_LST	
		g_BS_OIndexData[14].OnlineCharOwned[0] 	= LST_OW0  //mike
		g_BS_OIndexData[14].OnlineCharOwned[1] 	= LST_OW1 //frank
		g_BS_OIndexData[14].OnlineCharOwned[2] 	= LST_OW2  //trev
	g_BS_OIndexData[15].OnlinePriceStat 	=  SM_PRICE_LTD	
		g_BS_OIndexData[15].OnlineCharOwned[0] 	= LTD_OW0  //mike
		g_BS_OIndexData[15].OnlineCharOwned[1] 	= LTD_OW1 //frank
		g_BS_OIndexData[15].OnlineCharOwned[2] 	= LTD_OW2  //trev
	g_BS_OIndexData[16].OnlinePriceStat 	=  SM_PRICE_MAI	
		g_BS_OIndexData[16].OnlineCharOwned[0] 	= MAI_OW0  //mike
		g_BS_OIndexData[16].OnlineCharOwned[1] 	= MAI_OW1 //frank
		g_BS_OIndexData[16].OnlineCharOwned[2] 	= MAI_OW2  //trev
	g_BS_OIndexData[17].OnlinePriceStat 	=  SM_PRICE_PKW	
		g_BS_OIndexData[17].OnlineCharOwned[0] 	= PKW_OW0  //mike
		g_BS_OIndexData[17].OnlineCharOwned[1] 	= PKW_OW1 //frank
		g_BS_OIndexData[17].OnlineCharOwned[2] 	= PKW_OW2  //trev
	g_BS_OIndexData[18].OnlinePriceStat 	=  SM_PRICE_PIS	
		g_BS_OIndexData[18].OnlineCharOwned[0] 	= PIS_OW0  //mike
		g_BS_OIndexData[18].OnlineCharOwned[1] 	= PIS_OW1 //frank
		g_BS_OIndexData[18].OnlineCharOwned[2] 	= PIS_OW2  //trev
	g_BS_OIndexData[19].OnlinePriceStat 	=  SM_PRICE_PON	
		g_BS_OIndexData[19].OnlineCharOwned[0] 	= PON_OW0  //mike
		g_BS_OIndexData[19].OnlineCharOwned[1] 	= PON_OW1 //frank
		g_BS_OIndexData[19].OnlineCharOwned[2] 	= PON_OW2  //trev
	g_BS_OIndexData[20].OnlinePriceStat 	=  SM_PRICE_RON	
		g_BS_OIndexData[20].OnlineCharOwned[0] 	= RON_OW0  //mike
		g_BS_OIndexData[20].OnlineCharOwned[1] 	= RON_OW1 //frank
		g_BS_OIndexData[20].OnlineCharOwned[2] 	= RON_OW2  //trev
	g_BS_OIndexData[21].OnlinePriceStat 	=  SM_PRICE_SHT	
		g_BS_OIndexData[21].OnlineCharOwned[0] 	= SHT_OW0  //mike
		g_BS_OIndexData[21].OnlineCharOwned[1] 	= SHT_OW1 //frank
		g_BS_OIndexData[21].OnlineCharOwned[2] 	= SHT_OW2  //trev
	g_BS_OIndexData[22].OnlinePriceStat 	=  SM_PRICE_SPU	
		g_BS_OIndexData[22].OnlineCharOwned[0] 	= SPU_OW0  //mike
		g_BS_OIndexData[22].OnlineCharOwned[1] 	= SPU_OW1 //frank
		g_BS_OIndexData[22].OnlineCharOwned[2] 	= SPU_OW2  //trev
	g_BS_OIndexData[23].OnlinePriceStat 	=  SM_PRICE_TNK	
		g_BS_OIndexData[23].OnlineCharOwned[0] 	= TNK_OW0  //mike
		g_BS_OIndexData[23].OnlineCharOwned[1] 	= TNK_OW1 //frank
		g_BS_OIndexData[23].OnlineCharOwned[2] 	= TNK_OW2  //trev
	g_BS_OIndexData[24].OnlinePriceStat 	=  SM_PRICE_WIW	
		g_BS_OIndexData[24].OnlineCharOwned[0] 	= WIW_OW0  //mike
		g_BS_OIndexData[24].OnlineCharOwned[1] 	= WIW_OW1 //frank
		g_BS_OIndexData[24].OnlineCharOwned[2] 	= WIW_OW2  //trev
	g_BS_OIndexData[25].OnlinePriceStat 	=  SM_PRICE_UMA	
		g_BS_OIndexData[25].OnlineCharOwned[0] 	= UMA_OW0  //mike
		g_BS_OIndexData[25].OnlineCharOwned[1] 	= UMA_OW1 //frank
		g_BS_OIndexData[25].OnlineCharOwned[2] 	= UMA_OW2  //trev
	g_BS_OIndexData[26].OnlinePriceStat 	=  SM_PRICE_VAP	
		g_BS_OIndexData[26].OnlineCharOwned[0] 	= VAP_OW0  //mike
		g_BS_OIndexData[26].OnlineCharOwned[1] 	= VAP_OW1 //frank
		g_BS_OIndexData[26].OnlineCharOwned[2] 	= VAP_OW2  //trev
	g_BS_OIndexData[27].OnlinePriceStat 	=  SM_PRICE_VOM	
		g_BS_OIndexData[27].OnlineCharOwned[0] 	= VOM_OW0  //mike
		g_BS_OIndexData[27].OnlineCharOwned[1] 	= VOM_OW1 //frank
		g_BS_OIndexData[27].OnlineCharOwned[2] 	= VOM_OW2  //trev
	g_BS_OIndexData[28].OnlinePriceStat 	=  SM_PRICE_WZL	
		g_BS_OIndexData[28].OnlineCharOwned[0] 	= WZL_OW0  //mike
		g_BS_OIndexData[28].OnlineCharOwned[1] 	= WZL_OW1 //frank
		g_BS_OIndexData[28].OnlineCharOwned[2] 	= WZL_OW2  //trev
	g_BS_OIndexData[29].OnlinePriceStat 	=  SM_PRICE_WIZ	
		g_BS_OIndexData[29].OnlineCharOwned[0] 	= WIZ_OW0  //mike
		g_BS_OIndexData[29].OnlineCharOwned[1] 	= WIZ_OW1 //frank
		g_BS_OIndexData[29].OnlineCharOwned[2] 	= WIZ_OW2  //trev
	g_BS_OIndexData[30].OnlinePriceStat 	=  SM_PRICE_ZIT	
		g_BS_OIndexData[30].OnlineCharOwned[0] 	= ZIT_OW0  //mike
		g_BS_OIndexData[30].OnlineCharOwned[1] 	= ZIT_OW1 //frank
		g_BS_OIndexData[30].OnlineCharOwned[2] 	= ZIT_OW2  //trev
	g_BS_OIndexData[31].OnlinePriceStat 	=  SM_PRICE_SHK	
		g_BS_OIndexData[31].OnlineCharOwned[0] 	= SHK_OW0  //mike
		g_BS_OIndexData[31].OnlineCharOwned[1] 	= SHK_OW1 //frank
		g_BS_OIndexData[31].OnlineCharOwned[2] 	= SHK_OW2  //trev
	g_BS_OIndexData[32].OnlinePriceStat 	=  SM_PRICE_MOL	
		g_BS_OIndexData[32].OnlineCharOwned[0] 	= MOL_OW0  //mike
		g_BS_OIndexData[32].OnlineCharOwned[1] 	= MOL_OW1 //frank
		g_BS_OIndexData[32].OnlineCharOwned[2] 	= MOL_OW2  //trev
	g_BS_OIndexData[33].OnlinePriceStat 	=  SM_PRICE_PMP	
		g_BS_OIndexData[33].OnlineCharOwned[0] 	= PMP_OW0  //mike
		g_BS_OIndexData[33].OnlineCharOwned[1] 	= PMP_OW1 //frank
		g_BS_OIndexData[33].OnlineCharOwned[2] 	= PMP_OW2  //trev
	g_BS_OIndexData[34].OnlinePriceStat 	=  SM_PRICE_GOT	
		g_BS_OIndexData[34].OnlineCharOwned[0] 	= GOT_OW0  //mike
		g_BS_OIndexData[34].OnlineCharOwned[1] 	= GOT_OW1 //frank
		g_BS_OIndexData[34].OnlineCharOwned[2] 	= GOT_OW2  //trev
	g_BS_OIndexData[35].OnlinePriceStat 	=  SM_PRICE_EYE	
		g_BS_OIndexData[35].OnlineCharOwned[0] 	= EYE_OW0  //mike
		g_BS_OIndexData[35].OnlineCharOwned[1] 	= EYE_OW1 //frank
		g_BS_OIndexData[35].OnlineCharOwned[2] 	= EYE_OW2  //trev
	g_BS_OIndexData[36].OnlinePriceStat 	=  SM_PRICE_HVY	
		g_BS_OIndexData[36].OnlineCharOwned[0] 	= HVY_OW0  //mike
		g_BS_OIndexData[36].OnlineCharOwned[1] 	= HVY_OW1 //frank
		g_BS_OIndexData[36].OnlineCharOwned[2] 	= HVY_OW2  //trev
	g_BS_OIndexData[37].OnlinePriceStat 	=  SM_PRICE_SHR	
		g_BS_OIndexData[37].OnlineCharOwned[0] 	= SHR_OW0  //mike
		g_BS_OIndexData[37].OnlineCharOwned[1] 	= SHR_OW1 //frank
		g_BS_OIndexData[37].OnlineCharOwned[2] 	= SHR_OW2  //trev
	g_BS_OIndexData[38].OnlinePriceStat 	=  SM_PRICE_HAL	
		g_BS_OIndexData[38].OnlineCharOwned[0] 	= HAL_OW0  //mike
		g_BS_OIndexData[38].OnlineCharOwned[1] 	= HAL_OW1 //frank
		g_BS_OIndexData[38].OnlineCharOwned[2] 	= HAL_OW2  //trev

ENDPROC

/// Auto generated calculation of new UPM for each company

PROC BAWSAQ_GENERATED_APPLY_MODIFIERS_TO_PRICES()
ENDPROC
