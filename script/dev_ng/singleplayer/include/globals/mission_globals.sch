
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	mission_globals.sch
//		AUTHOR			:	Keith
//		DESCRIPTION		:	This file is split into two sections.
//
//		The majority of mission globals will be in this section
//		On a reload, all these globals will become 0, FALSE, NULL, etc
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


// -------------------------------------------------------
//		None saved mission globals.
// -------------------------------------------------------

//Car steal strand: default car colours for each of the stolen vehicles
CONST_INT CARSTEAL_COLOURS_MONROE 		126
CONST_INT CARSTEAL_COLOURS_CHEETAH 		68
CONST_INT CARSTEAL_COLOURS_STINGER 		33
CONST_INT CARSTEAL_COLOURS_JB700 		3
CONST_INT CARSTEAL_COLOURS_ENTITYXF 	44
CONST_INT CARSTEAL_COLOURS_ZTYPE 		0

CONST_INT	SHRINK_SEX_ACTS				2 //time stamps for michael getting late
CONST_INT	SHRINK_LAP_DANCES			1 //time stamps for michael getting lap dances


CONST_INT	iSHRINK_4STAR_WANTED		1
CONST_INT	iSHRINK_RANDOM_KILLS		3
CONST_INT	iSHRINK_2STAR_WANTED		1
CONST_INT	iSHRINK_VEHICLE_KILLS		1
CONST_INT	iSHRINK_KILLS				1
CONST_INT	iSHRINK_SHOP_ROBBERY		1
CONST_INT	iSHRINK_PED_ROBBERY			1
CONST_INT	iSHRINK_STOLEN_CARS			2
CONST_INT	iSHRINK_WANTED_IN_CAR		1
CONST_INT	iSHRINK_DANGEROUS_DRIVING	1


INT g_TVStockMarketIntro_CellphoneTimer = 7000

BOOL g_family1GarbageTruckSeen 			= FALSE
BOOL g_Family1BallerSpawnAfter1stTry 	= FALSE

BOOL g_bOpenAbattoirRollerDoor			= FALSE	// Added by Dave W, for abattoir exit in Michael2
BOOL g_bAbattoirRollerDoorHasOpened		= FALSE
BOOL g_bSetPieceCrossoverHasBeenSeen	= FALSE // Added by Ben B, for a setpiece being witnessed to stop it happening twice if player dies in Solomon 1.
//BOOL g_bSetPieceTruckExplodeSeen		= FALSE // Added by Ben B, for a setpiece being witnessed to stop it happening twice if player is on a replay during Trevor 4.
//BOOL g_bFinaleCompletedFranklinStage
//BOOL g_bFinaleCompletedTrevorStage

//Armenian 1 - Matt B
INT g_iArmenian1VehicleChoice			= 0 //Used for remembering which car the player chose if they fail the mission and retry.

//Armenian 2 - Kev B
BOOL g_bGunPickedUp						= FALSE

//Armenian 3 - Alan L
INT g_iArmenian3HelpText				= 0 //Used for remembering if help text has been displayed already, in the case of replays and progressing quickly through the previous stage.

//Car Steal 1 - Alan L
//BOOL g_bCarSteal3LauncherRunning		= FALSE	//Used by the Taxi controller to check if the launcher is running.
//BOOL g_bCarSteal3LauncherLoading		= FALSE	//Used for taking a Taxi to the launcher and skipping the journey.

//Trevor 3 - Kev B
int g_numEnemyAlive						= 0 //stores the number of biker peds alive when the replay stage is stored after all the trailers are blown up.

// Exile 1 - Rob B
BOOL g_bExile1DestroyedEscapeVehicle	= FALSE

//Finale 3B - Rob B
BOOL g_bFinale3BHitSuccessful[3]		// Used to track which stages have been completed for replays, as can be tackled in any order
BOOL g_bFinaleCreditsPlaylistStarted
INT  g_iFinaleCreditsToPlay // 0=Finale A kill Trevor 		1= Finale B kill Michael 		2 = Finale C Save Both

//Solomon 1 - Craig v
Bool g_bUnderTheBridgeTriggered			= FALSE	//Used to check if the player has flow under a bridge 

//Franklin 1 - Lukasz B
INT  g_iFranklin1VehicleChoice			= 0		//stores information about speedophiles assigned to each peds when mission retry is in progress
BOOL g_bFranklin1GirlFled				= FALSE	//stores information if female ped from photo shoot was fleeing before player jacked her speedophile
BOOL g_bFranklin1BikersSpawned			= FALSE	//stores information if bmx bikers in Grove Street were spawned last time drive stage was played

//Franklin 2 - Matt B
BOOL g_bFranklin2RequestedBackup		= FALSE //Stores if the player called for backup from Michael/Trevor at the start of the mission.

//Car Steal 3 - Lukasz B
INT  g_iCarSteal3AgentBitSet			= 0		//stores various information about player actions in the mission

//family 4 - Lawrence Kerr
bool family4_trailer_detached				= false

//Fbi4 - Lawrence Ker
int fbi4_shootout_wanted_level				= 4
bool fbi4_special_ability_help_activated	= false

//Fbi 5A - Lawrence Kerr
bool fbi5a_scientist_4_alive 				= true
bool fbi5a_scientist_5_alive				= true
bool fbi5a_special_ability_help_activated	= false
bool fbi5a_perform_tods   					= false

//AH3A - Lawrence Kerr
bool ah3a_crew_1_alive						= true

//Exile 2 - Lawrence Kerr
bool big_foot_global_created				= false


//Big Score 2A - Matt B
BOOL g_bBigScore2ARescuedBadDriver			= FALSE

INT g_iShrinkBrokeConvTimestamp

INT g_iShrinkSexTimestamps[SHRINK_SEX_ACTS]
INT g_iShrinkLapDanceTimestamps[SHRINK_LAP_DANCES]
INT g_iShrinkStripClubTimestamp //Used for tracking strip club visit only.


//SHRINK OPEN WORLD VIOLENCE ARRAYS
INT g_iShrink4StarWantedTimestamps[iSHRINK_4STAR_WANTED]
INT g_iShrinkRandomKillsTimestamps[iSHRINK_RANDOM_KILLS]
INT g_iShrink2StarWantedTimestamps[iSHRINK_2STAR_WANTED]
INT g_iShrinkVehKillsTimestamps[iSHRINK_VEHICLE_KILLS]
INT g_iShrinkKillsTimestamps[iSHRINK_KILLS]
INT g_iShrinkShopRobTimestamps[iSHRINK_SHOP_ROBBERY]
INT g_iShrinkPedRobTimestamps[iSHRINK_PED_ROBBERY]
INT g_iShrinkStolenCarTimestamps[iSHRINK_STOLEN_CARS]
INT g_iShrinkWantedInCarTimestamps[iSHRINK_WANTED_IN_CAR]
INT g_iShrinkDangerousDriveTimestamps[iSHRINK_DANGEROUS_DRIVING]

//Single player mission creation triggers
INT g_iSPMCIndexTrigger = -1
INT g_iSPMCInstanceTrigger = -1
BOOL g_bSPMCLastFailed = FALSE

//Chinese 2 farmhouse fire
ENUM enum_farmhouse_fire_state
	FARMHOUSE_FIRE_PENDING,
	FARMHOUSE_IGNITE_FIRE,
	FARMHOUSE_FIRE_BURNING
ENDENUM

enum_farmhouse_fire_state g_eFarmhouseFireState
 
bool g_bDontLetBuddiesReactToNextPlayerDeath = FALSE

//Moving prep missions - Force route.
INT g_iForcePrepMissionRoute = -1

//Post Heist Lester management - Ben R / Paul D
STRUCT StructLesterHandover
	PED_INDEX pedHandover
ENDSTRUCT

//Post Docks Heist 2A sub management - Ryan P
STRUCT StructDocksSubHandover
	VEHICLE_INDEX subHandover
	ENTITY_INDEX hookHandover
ENDSTRUCT

