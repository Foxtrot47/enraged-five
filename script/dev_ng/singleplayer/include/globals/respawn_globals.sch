//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	respawn_globals.sch											//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Structs and enums used to store the savehouse, hospital, 	//
//							and police station data.									//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

CONST_INT REPAWN_FLAG_AVAILABLE_BIT				0
CONST_INT REPAWN_FLAG_LONG_RANGE_BLIP_BIT		1

CONST_INT MAX_WATCHED_SAVEHOUSES 7//This has to be the total safehouses for instances where there are
									//super fast 3 way switches

/// PURPOSE: A list of enums to represent each savehouse

ENUM SAVEHOUSE_NAME_ENUM

#if not USE_NRM_DLC	
#if not USE_CLF_DLC
	// Michaels
	SAVEHOUSE_MICHAEL_BH = 0,
	SAVEHOUSE_MICHAEL_CS,
	
	// Trevors
	SAVEHOUSE_TREVOR_CS,
	SAVEHOUSE_TREVOR_VB,
	SAVEHOUSE_TREVOR_SC,
	
	// Franklins
	SAVEHOUSE_FRANKLIN_SC,
	SAVEHOUSE_FRANKLIN_VH,
	
	NUMBER_OF_ACTIVE_SAVEHOUSE_LOCATIONS,
	
	// Temp so we can make an autosave after prologue and before arm1
	SAVEHOUSE_MICHAEL_PRO = NUMBER_OF_ACTIVE_SAVEHOUSE_LOCATIONS,
	SAVEHOUSE_FRANKLIN_PRO,
	SAVEHOUSE_TREVOR_PRO,
	
	NUMBER_OF_SAVEHOUSE_LOCATIONS // CHANGE THE DLC VERSIONS TOO!!!
#endif	
#endif


#if USE_CLF_DLC
	// Michaels
	SAVEHOUSEclf_MICHAEL_BH = 0
	,SAVEHOUSEclf_MICHAEL_CS
	
	// Trevors
	,SAVEHOUSEclf_TREVOR_CS
	,SAVEHOUSEclf_TREVOR_VB
	,SAVEHOUSEclf_TREVOR_SC
	
	// Franklins
	,SAVEHOUSEclf_FRANKLIN_SC
	,SAVEHOUSEclf_FRANKLIN_VH
	
	// Temp so we can make an autosave after prologue and before arm1
	,SAVEHOUSEclf_MICHAEL_PRO
	,SAVEHOUSEclf_FRANKLIN_PRO
	,SAVEHOUSEclf_TREVOR_PRO
	
	,NUMBER_OF_CLF_SAVEHOUSE
	,NUMBER_OF_SAVEHOUSE_LOCATIONS = 10 // must be equal to the orginal
#endif

#if use_NRM_DLC
	SAVEHOUSENRM_BH = 0
	,SAVEHOUSENRM_CHATEAU	
	
	,NUMBER_OF_NRM_SAVEHOUSE
	,NUMBER_OF_SAVEHOUSE_LOCATIONS = 10 // must be equal to the orginal
#endif	

ENDENUM

/// PURPOSE: A list of enums to represent each police station
ENUM POLICE_STATION_NAME_ENUM
	
	POLICE_STATION_VB = 0,	// Vespucci Beach
	POLICE_STATION_SC,		// South Central
	POLICE_STATION_DT,		// Downtown
	POLICE_STATION_RH,		// Rockford Hills
	POLICE_STATION_SS,		// Sandy Shores
	POLICE_STATION_PB,		// Paleto Bay
	POLICE_STATION_HW,		// Vinewood (#1289759)
	
	NUMBER_OF_POLICE_STATION_LOCATIONS
ENDENUM

/// PURPOSE: A list of enums to represent each hospital
ENUM HOSPITAL_NAME_ENUM
	
	HOSPITAL_RH = 0, 	// Rockford Hills
	HOSPITAL_SC, 		// South Central
	HOSPITAL_DT, 		// Downtown
	HOSPITAL_SS, 		// Sandy Shores
	HOSPITAL_PB, 		// Paleto Bay
	
	NUMBER_OF_HOSPITAL_LOCATIONS
ENDENUM

/// PURPOSE: The struct we use to store all the hospital respawn data
STRUCT HOSPITAL_RESPAWN_DATA_STRUCT
	VECTOR vBlipCoords
	VECTOR vSpawnCoords
	FLOAT  fSpawnHeading
	STATIC_BLIP_NAME_ENUM eBlip
	HOSPITAL_RESTART_INDEX index
ENDSTRUCT

/// PURPOSE: The struct we use to store all the police respawn data
STRUCT POLICE_RESPAWN_DATA_STRUCT
	VECTOR vBlipCoords
	VECTOR vSpawnCoords
	FLOAT  fSpawnHeading
	STATIC_BLIP_NAME_ENUM eBlip
	POLICE_RESTART_INDEX index
ENDSTRUCT

/// PURPOSE: The struct we use to store all the savehouse respawn data
STRUCT SAVEHOUSE_RESPAWN_DATA_STRUCT
	VECTOR vBlipCoords
	VECTOR vSpawnCoords
	FLOAT  fSpawnHeading
	STATIC_BLIP_NAME_ENUM eBlip
	SAVE_HOUSE_INDEX index
	INT iChar
ENDSTRUCT


INT 							g_iSavehouseOpenForPedsBitset[NUMBER_OF_SAVEHOUSE_LOCATIONS]
SAVEHOUSE_RESPAWN_DATA_STRUCT 	g_sSavehouses[NUMBER_OF_SAVEHOUSE_LOCATIONS]
POLICE_RESPAWN_DATA_STRUCT  	g_sPoliceStations[NUMBER_OF_POLICE_STATION_LOCATIONS]
HOSPITAL_RESPAWN_DATA_STRUCT  	g_sHospitals[NUMBER_OF_HOSPITAL_LOCATIONS]

INT g_iNumberOfTimesDieDrowning
INT g_iNumberOfTimesDieExplosion
INT g_iNumberOfTimesDieFall
INT g_iNumberOfTimesDieFire
INT g_iNumberOfTimesDieRoad
INT g_iNumberOfTimesDieMission

BOOL g_RestoreStoredWeaponsOnDeath		// use this in occasions where you do want RESTORE_PLAYER_PED_WEAPONS() after you die like on rampages / barry 1 / barry 2
BOOL g_RestoreSnapshotWeaponsOnDeath	// use this in occasions where we take weapon snapshot and remove all weapons
BOOL g_bBlockDeathFadeout				// use this to prevent the screen from fading. Should ONLY be for special cases (like Hunting minigame where the results screen needs to be drawn during death)
BOOL g_bBlockWastedTitle				// use this to prevent the WASTED title from being forced on screen (like in hunting again, where you can transition to leaderboard)
BOOL g_bBlockUltraSloMoOnDeath			// use this to prevent stage 2 slo mo from happening while the player's dead.


//////////////////////////////////////////////////////////////////////////////////////////
///    Struct for the global saved data
STRUCT RespawnDataSaved


	INT iSavehouseProperties[NUMBER_OF_SAVEHOUSE_LOCATIONS]
	INT iHospitalProperties[NUMBER_OF_HOSPITAL_LOCATIONS]
	INT iPoliceStationProperties[NUMBER_OF_POLICE_STATION_LOCATIONS]
	
	BOOL bDefaultSavehouseDataSet
	BOOL bDefaultHospitalDataSet
	BOOL bDefaultPoliceStationDataSet
	
	BOOL bSeenFirstTimeWasted
	BOOL bSeenFirstTimeDrowned
	BOOL bSeenFirstTimeBusted
	
	BOOL bNewGameStarted // This is a simple flag to let us know if the startup script is launching for a new game or a save game.
ENDSTRUCT

////////////////////////////////////////////////////////
///    Respawn location pickups
CONST_INT NUM_OF_SAVEHOUSE_HEALTH_PICKUPS 		3


CONST_INT NUM_OF_SAVEHOUSES_USING_HEALTH_PCKUPS (NUMBER_OF_SAVEHOUSE_LOCATIONS)


// So we can share pickup data across the savegame bed scripts
PICKUP_INDEX g_piSavehouseHealthPickups[NUM_OF_SAVEHOUSES_USING_HEALTH_PCKUPS][NUM_OF_SAVEHOUSE_HEALTH_PICKUPS]

