// shrink_globals.sch
//Location of each piece of data within its byte
CONST_INT CONST_ShrinkBrokePos				5
CONST_INT CONST_ShrinkSexPos				11

//Based on number of bits used for each piece of data
CONST_INT CONST_ShrinkBrokeMask			3
CONST_INT CONST_ShrinkNarrativeMask			31
CONST_INT CONST_ShrinkOpenWorldMask			2047
CONST_INT CONST_ShrinkSexMask				4095


// ***MAP OF SAVED DATA***
// 
// BYTE0
// Bits 0-4: Last played narrative conversation
// Bits 5-6: Last played "out of cash" conversation
// 
// BYTE1
// Bits 0-10: Used open world conversations
// Bits 11-22: Used sex conversations
//
// Design changes have reduced the need for save data.
// Could be optimized to save a byte, but we're pretty close to ship so leaving as-is. -DJ 24 May 2013


//                         RL    Game
//aproximate in game time 2sec = 1 minute
//                        2min = 1 hour
//                        48min = 1 day
//                        96min = 2 day

CONST_INT	SHRINK_SEX_MEMORY_TIME		96*60*1000	//Time to check to discard old time stamps.
CONST_INT	SHRINK_VIOLENCE_MEMORY_TIME		96*60*1000 	//Time to check to discard old time stamps.
CONST_INT	SHRINK_VIOLENCE_VEH_KILLS_MEMORY_TIME		300000 	//Time to check to discard old vehicle kills time stamps.


// The enumerated list of unique sessions.
ENUM SHRINK_SESSION
	//SHRINKSESSION_INVALID,
	SHRINKSESSION_OFFICE_CHAOS,
	SHRINKSESSION_OFFICE_EVIL,
	SHRINKSESSION_PHONE_NEGATIVITY,
	SHRINKSESSION_PHONE_FUCKED_UP,
	SHRINKSESSION_OFFICE_ABANDONMENT,
	//SHRINKSESSION_OFFICE_COUPLES,
	SHRINKSESSIONS
ENDENUM


STRUCT ShrinkDataSaved
	INT iByte0
	INT iByte1
ENDSTRUCT


FUNC INT SHRINK_GET_LAST_NARRATIVE(ShrinkDataSaved &thisShrinkData)
	RETURN thisShrinkData.iByte0 & CONST_ShrinkNarrativeMask
ENDFUNC

PROC SHRINK_SET_LAST_NARRATIVE(ShrinkDataSaved &thisShrinkData, INT iNarrative)
	IF iNarrative < 0 OR iNarrative > 12
		EXIT 
	ENDIF		
	thisShrinkData.iByte0 -= thisShrinkData.iByte0 & CONST_ShrinkNarrativeMask
	thisShrinkData.iByte0 = thisShrinkData.iByte0 | iNarrative
ENDPROC

FUNC INT SHRINK_GET_LAST_BROKE_CONV(ShrinkDataSaved &thisShrinkData)
	RETURN SHIFT_RIGHT(thisShrinkData.iByte0, CONST_ShrinkBrokePos) & CONST_ShrinkBrokeMask
ENDFUNC

PROC SHRINK_SET_LAST_BROKE_CONV(ShrinkDataSaved &thisShrinkData, INT iBroke)
	IF iBroke < 0 OR iBroke > 3
		EXIT
	ENDIF
	thisShrinkData.iByte0 -= thisShrinkData.iByte0 & SHIFT_LEFT(CONST_ShrinkBrokeMask, CONST_ShrinkBrokePos)
	thisShrinkData.iByte0 = thisShrinkData.iByte0 | SHIFT_LEFT(iBroke, CONST_ShrinkBrokePos)
ENDPROC

FUNC BOOL SHRINK_IS_OPEN_WORLD_RESPONSE_USED(ShrinkDataSaved &thisShrinkData, INT iResponse)
	IF iResponse < 0 OR iResponse > 11
		RETURN FALSE
	ENDIF
	RETURN IS_BIT_SET(thisShrinkData.iByte1, iResponse)
ENDFUNC

PROC SHRINK_SET_OPEN_WORLD_RESPONSE_USED(ShrinkDataSaved &thisShrinkData, INT iResponse, BOOL bUsed)
	IF iResponse < 0 OR iResponse > 11
		EXIT 
	ENDIF
	IF bUsed
		SET_BIT(thisShrinkData.iByte1, iResponse)
	ELSE
		CLEAR_BIT(thisShrinkData.iByte1, iResponse)
	ENDIF
ENDPROC

PROC SHRINK_CLEAR_OPEN_WORLD_RESPONSES(ShrinkDataSaved &thisShrinkData)
	thisShrinkData.iByte1 -= thisShrinkData.iByte1 & CONST_ShrinkOpenWorldMask
ENDPROC

FUNC BOOL SHRINK_IS_SEX_RESPONSE_USED(ShrinkDataSaved &thisShrinkData, INT iResponse)
	IF iResponse < 0 OR iResponse > 12
		RETURN FALSE
	ENDIF
	iResponse += CONST_ShrinkSexPos
	RETURN IS_BIT_SET(thisShrinkData.iByte1, iResponse)
ENDFUNC

PROC SHRINK_SET_SEX_RESPONSE_USED(ShrinkDataSaved &thisShrinkData, INT iResponse, BOOL bUsed)
	IF iResponse < 0 OR iResponse > 12
		EXIT 
	ENDIF
	iResponse += CONST_ShrinkSexPos
	IF bUsed
		SET_BIT(thisShrinkData.iByte1, iResponse)
	ELSE
		CLEAR_BIT(thisShrinkData.iByte1, iResponse)
	ENDIF
ENDPROC

PROC SHRINK_CLEAR_SEX_RESPONSES(ShrinkDataSaved &thisShrinkData)
	thisShrinkData.iByte1 -= thisShrinkData.iByte1 & SHIFT_LEFT(CONST_ShrinkSexMask, CONST_ShrinkSexPos)
ENDPROC
