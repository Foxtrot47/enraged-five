USING "types.sch"
USING "model_enums.sch"
USING "stats_enums.sch"
USING "leaderboards_enum.sch"
USING "mission_stat_generated_globals.sch"

BOOL g_bMissionNoStatsNeedsSplashAndSting = FALSE
BOOL g_bHeistEndscreenDisplaying = FALSE
BOOL g_bMissionStatSystemBuildingReplayStats = FALSE

//System
BOOL g_bMissionStatSystemPrimed = FALSE
BOOL g_bMissionOverStatTrigger = FALSE
BOOL g_bStatsInWatchingLoop = FALSE
BOOL g_bMissionStatSystemMissionStarted = FALSE
BOOL g_bMissionStatSystemResponseToReplayNeeded = FALSE
BOOL g_bMissionStatSystemResponseToShitskipNeeded = FALSE
BOOL g_bMissionStatTimeDeltaReset = FALSE
BOOL g_bMissionStatSystemBlocker = FALSE//used to delay display of the mission complete UI by the flow
BOOL g_bMissionStatSystemResetFlag = FALSE
BOOL g_MissionStatSystemSuppressVisual = FALSE
BOOL g_bMissionStatSystemHeistMode = FALSE
INT g_iMissionStatSystemHeistIndex = -1
BOOL g_bMissionStatSystemUploadPending = FALSE
//Stat information
BOOL g_bMissionStatSystemSequenceStatus = FALSE
BOOL g_bMissionStatSystemActionCamWatchStatus = FALSE


//Time window switch
BOOL g_bMissionStatTimeWindowGate = FALSE
BOOL g_bMissionStatTimeWindowClosedForGood = FALSE // used to inform system popup for less than time window stats is available
ENUM_MISSION_STATS g_eCurrentTimeWindowTarget

//Tracked entity
ENTITY_INDEX g_MissionStatSingleSpeedWatchEntity = NULL
ENUM_MISSION_STATS g_MissionStatSpeedWatchEntitySpecific = UNSET_MISSION_STAT_ENUM

ENTITY_INDEX g_MissionStatSingleDamageWatchEntity = NULL
ENUM_MISSION_STATS g_MissionStatSingleDamageWatchStat = UNSET_MISSION_STAT_ENUM//if unset then target first in list

TEXT_LABEL_15 g_sMissionStatsName
//accuracy tracking data
BOOL g_bMissionStatAccuracyTallied

BOOL g_MissionStatUIIgnoreFade = FALSE
BOOL g_bSuppressNextStatTrigger = FALSE

INT g_iMissionStatsSP0Hits = 0
INT g_iMissionStatsSP0Shots = 0
INT g_iMissionStatsSP1Hits = 0
INT g_iMissionStatsSP1Shots = 0
INT g_iMissionStatsSP2Hits = 0
INT g_iMissionStatsSP2Shots = 0

INT g_iMissionStatsStartTime = 0

ENUM ENUM_MISSION_STAT_TYPES
	MISSION_STAT_TYPE_UNSET,//default
	
	MISSION_STAT_TYPE_TOTALTIME, 
	MISSION_STAT_TYPE_ACTION_CAM_USE,
	MISSION_STAT_TYPE_UNIQUE_BOOL,
	MISSION_STAT_TYPE_WINDOWED_TIMER,//Time window
	MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD,//speed watch
	MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD,//single entity damage watch
	MISSION_STAT_TYPE_HEADSHOTS,//head shot entity list watch
	MISSION_STAT_TYPE_FRACTION,//script pushed fraction
	MISSION_STAT_TYPE_ACCURACY,
	MISSION_STAT_TYPE_PURE_COUNT,//special cases
	MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE,
	MISSION_STAT_TYPE_FINANCE_DIRECT,
	MISSION_STAT_TYPE_FINANCE_TABLE,
	MISSION_STAT_TYPE_PURE_COUNT_DISTANCE, //In pending new upload type
	MISSION_STAT_TYPE_BULLETS_FIRED,//accuracy but shots only
	MISSION_STAT_TYPE_INNOCENTS_KILLED,
	MISSION_STAT_TYPE_SPECIAL_ABILITY_USE,

	MAX_MISSION_STAT_TYPES
ENDENUM


STRUCT ENTITY_WATCH_LIST_ENTRY
	ENTITY_INDEX index
	ENUM_MISSION_STAT_TYPES type
ENDSTRUCT


CONST_INT MAX_ENTITY_WATCH_LIST_ENTRIES 64
INT g_iEntityWatchListLoggedCount = 0
ENTITY_WATCH_LIST_ENTRY g_EntityWatchList[MAX_ENTITY_WATCH_LIST_ENTRIES]




STRUCT FINANCE_LOOKUP_TABLE_ENTRY
	 MODEL_NAMES target
	 INT value
ENDSTRUCT

CONST_INT MAX_FINANCE_LOOKUP_ENTRIES 32
INT g_iFinanceLookupLogged = 0
FINANCE_LOOKUP_TABLE_ENTRY g_FinanaceLookup[MAX_FINANCE_LOOKUP_ENTRIES]


//TODO completion percentages

/// PURPOSE: Mission Stat Base data
STRUCT MissionStatInfo
	ENUM_MISSION_STAT_TYPES type
	INT currentvalue//mirror of the values stored in int stats
	INT success_threshold//for check
	BOOL less_than_threshold
	
	STATSENUM statname
	
	//percentage calculation values for score contribution
	INT MinRange
	INT MaxRange
	
	BOOL bHidden
	
	//leaderboard stat data
	
	// diff: [REQUIRED] Is this stat used as a differentiator //(do we add on the score from this stat to allow for differentiation between 2 people who both met the objective)? For example if 2 people got > -10 car damage, we can add on the actual damage they took which may place one player above another. (bool)
	BOOL lb_differentiator
	// weighting: [REQUIRED] The amount of points this value is worth. For example each 1% of car damage is worth 1 point, but we don't want each millisecond of action cam use to be worth 1 point as it isn't fairly weighted. Therefore each 1 millisecond of action cam use is worth 0.001 points - this is variable and TBD per mission (float)
	FLOAT lb_weight_PPC
	// min: [REQUIRED] The lowest/worst score the player can legally get, or the cut off point where we don't care anymore if they're lower - may be negated if required (int)
	INT lb_min_legal
	// max: [REQUIRED] The highest/best score the player can legally get, or the cut off point where we don't care anymore if they#re higher - may be negated if required. Cut off point is particularly useful for unbounded values like time, bullets fired, damage etc. We can record scores up to 30 minutes, and if they go above this, we only add 30 mins worth of bonus score to their time etc. (int)
	INT lb_max_legal
	// precedence: [REQUIRED for mission objectives] The order which the objectives take precedence. 1 is worth the most. Objectives can have the same precedence level if there is no way to rank one higher than another. (int)
	INT lb_precedence
ENDSTRUCT
MissionStatInfo g_MissionStatTrackingPrototypes[MAX_MISSION_STATS]

ENUM MISSION_STAT_SYSTEM_INVALIDATION_REASON
	MSSIR_VALID,
	MSSIR_CHECKPOINT,
	MSSIR_SKIP,
	MSSIR_TAXI_USED,
	MSSIR_CHEAT_ACTIVE,
	MSSIR_NOT_SET
ENDENUM
/// PURPOSE: Mission stat tracking data
STRUCT MissionStatTrackerEntry
	ENUM_MISSION_STATS target
	
	//value
	INT ivalue
	FLOAT fvalue//used for some metrics to avoid precision loss before calculation
	
	//invalidation bool (skip/replay/etc)
	//BOOL invalidated
	MISSION_STAT_SYSTEM_INVALIDATION_REASON invalidationReason 
	
	BOOL successshown //this is overridden and doesn't happen if less_than_threshold = true
	
	BOOL windowDeltaTracking//used to auto increment 
	
	//New tracking entity
	ENTITY_INDEX TrackingEntity
	BOOL bTrackingEntityChanged
	INT iTrackingDelta
	
	
	
	#IF IS_DEBUG_BUILD
	TEXT_WIDGET_ID target_textWidget
	INT invalidationReason_int
	#ENDIF
ENDSTRUCT


INT g_iMissionStatsBeingTracked = 0

MissionStatTrackerEntry g_MissionStatTrackingArray[MAX_TRACKED_MISSION_STATS]


#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID g_missionstatwidgetgroup
	
	
	BOOL g_DEBUG_show_hidden_stats_on_mission_complete = FALSE
#ENDIF


//per mission leaderboard score entries
/*
BOOL g_bMissionScoreLeadboardPrimed = FALSE
LEADERBOARDS_ENUM g_eTargetMissionScoreLeaderboardBestRun = LEADERBOARD_MINI_GAMES_TENNIS //Using tennis to signify
LEADERBOARDS_ENUM g_eTargetMissionScoreLeaderboardIndivRec = LEADERBOARD_MINI_GAMES_TENNIS //an invalid value for now
*/


//leaderboard  per mission upload globals
INT g_LeaderboardLastMissionScore = 0
INT g_LeaderboardLastMissionScore_Derivs = 0
//INT g_LeaderboardLastMissionReplays = 0
//INT g_LeaderboardLastMissionFails = 0
//INT g_LeaderboardLastMissionPasses = 0
INT g_LeaderboardLastMissionBestTime = 0

INT g_LeaderboardLastObjectivesPassed = 0//1369112

BOOL g_LeaderboardLastMissionRCType = FALSE
INT g_LeaderboardLastMissionID = -1



BOOL g_bTrackingInnocentsLogged = FALSE

//Tracking structs that can be used to automate incrementation of pure count stats


//kills incrementor watch list - removed on dead
STRUCT STAT_ENTITY_DEADPOOL_ENTRY
	ENTITY_INDEX entar
	ENUM_MISSION_STATS statar
ENDSTRUCT
CONST_INT STAT_DEADPOOL_DEPTH 16
STAT_ENTITY_DEADPOOL_ENTRY g_StatSystemDeadpool[STAT_DEADPOOL_DEPTH]
INT g_StatSystemDeadpoolEntries = 0



//kills with weapon auto incrementor - persists
STRUCT STAT_WEAPON_INCPOOL_ENTRY
	WEAPON_TYPE weptar
	ENUM_MISSION_STATS statar
ENDSTRUCT
CONST_INT STAT_WEPPOOL_DEPTH 4
STAT_WEAPON_INCPOOL_ENTRY g_StatSystemWeaponpool[STAT_WEPPOOL_DEPTH]
INT g_StatSystemWeponpoolEntries = 0










