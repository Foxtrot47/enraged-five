// trafficking_globals.sch
FORWARD ENUM TIMEOFDAY

CONST_INT		NUM_WEAPON_CARGO_PKGS		5

ENUM GANG_TYPE
	GANG_MEXICANS = 0,
	GANG_MARABUNTA,
	GANG_HILLBILLIES
ENDENUM

STRUCT TraffickingDataSaved
	//INT 		iCurrentRank
	INT			iBools
	INT 		iGroundRank
	INT 		iAirRank
	INT 		iLastPairing
	INT 		iNumRepeatComplete
	
	INT			iTimePackageCollected[NUM_WEAPON_CARGO_PKGS] // One for each weapon cargo package
	
	TIMEOFDAY	todNextPlayable_Air
	TIMEOFDAY	todNextPlayable_Gnd
	
	BOOL		bGndBlipActive
	BOOL		bAirBlipActive
	BOOL 		bRanIntroduction
	
	//saved data need to track the last random variation of repeat mode you did so that replay works
	
	//bomb or package
	BOOL 	bBomb
	//train or cluster
	BOOL 	bTrain
	BOOL 	bConvoy
	//timed
	BOOL 	bTimed
	//low alt
	BOOL 	bLowAlt
	INT 	iDropLocations[4]
	INT		iNumDropLocations
	
	GANG_TYPE gangTypes  	
ENDSTRUCT

ENUM TRAFFICKING_GLOBAL_BOOLS
	TRAF_BOOL_LaunchedGndViaDebug 	= 1,
	TRAF_BOOL_LaunchedAirViaDebug 	= 2,
	TRAF_BOOL_Type1				 	= 4,
	
	TRAF_BOOL_GroundDone 			= 8,
	TRAF_BOOL_AirDone 				= 16, 
	TRAF_BOOL_GaveHelpPrint 		= 32,
	TRAF_BOOL_Type2				 	= 64,
	TRAF_BOOL_PlaneCreated			= 128
ENDENUM
