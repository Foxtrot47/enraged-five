///  Global settings for the website system
//

USING "mp_globals_new_features_TU.sch"

MODEL_NAMES eDLC_BIG_YACHT_MODEL = INT_TO_ENUM(MODEL_NAMES, HASH("apa_mp_apa_yacht"))
MODEL_NAMES eDLC_BIG_TRUCK_MODEL			= INT_TO_ENUM(MODEL_NAMES, HASH("gr_mp_gr_truck"))
MODEL_NAMES eDLC_BALLISTIC_EQUIPMENT_MODEL	= INT_TO_ENUM(MODEL_NAMES, HASH("gr_mp_gr_BALLISTIC_EQUIPMENT"))
MODEL_NAMES eDLC_BIG_PLANE_MODEL			= INT_TO_ENUM(MODEL_NAMES, HASH("gr_mp_go_plane"))
MODEL_NAMES eDLC_HACKER_TRUCK_MODEL			= INT_TO_ENUM(MODEL_NAMES, HASH("gr_mp_bb_hackertruck"))

ENUM SPECIAL_BROWSER_START_STATES
	 SBSS_Default							// Normal start
	,SBSS_EyeFind							// Search page
	,SBSS_CashMachineFinance
	,SBSS_LifeInvader
	,SBSS_LifeInvader_BrucieResponse
	,SBSS_Bawsaq
	,SBSS_SHOP_RESTORE
	,SBSS_Launch_Link
	,SBSS_ArenaWar
	,SBSS_Casino
ENDENUM

ENUM START_BROWSER_TYPE_ENUM
	 eAppInternet
	,eSecuroServ
	,eBikerApp
	,eIEApp
ENDENUM

STRUCT LAUNCH_BROWSER_ON_START_FREEMODE_DATA
	BOOL bLaunch = FALSE
	TEXT_LABEL_63 tlLinkToLaunch
ENDSTRUCT

LAUNCH_BROWSER_ON_START_FREEMODE_DATA g_LaunchBrowserOnStartFreemode

TEXT_LABEL_63 g_BrowserLinkToLaunch //SBSS_Launch_Link tries to load site in this label

BOOL g_bBlockInternetApp = FALSE

BOOL g_bBrowserVisible = FALSE
BOOL g_bSPForceKillInternet = FALSE

BOOL g_bBrowserUserMode = TRUE
BOOL g_bBrowserQuitMessage = FALSE

BOOL g_bSecuroDelayOfficeChairExit = FALSE
BOOL g_bSecuroQuickExitOfficeChair = FALSE
BOOL g_bSVMThumbnailFallback = FALSE
TEXT_LABEL_23 g_txtSVMThumbnails[8]
TEXT_LABEL_23 g_txtWVMThumbnails[8]

BOOL g_bBrowserRequestHangarContrabandRemoval = FALSE
BOOL g_bSmugglerAppBlockDeliverableCheck = FALSE
BOOL g_bIEAppFailedRemoveVehicles = FALSE

//lifeinvader 
BOOL g_bLifeInvaderMissionMessageToBruciePosted = FALSE
BOOL g_bLifeInvaderMissionResponseToBruciePosted = FALSE

BOOL g_bLaptopMissionSuppressed = FALSE

//used to display a help message on various stages of the lifeinvader site
ENUM ENUM_LIFEINVADER_HELP_SEQUENCE_STATE
	LIFEINVADER_NO_PROGRESS_0,//pre photo upload
	LIFEINVADER_PHOTO_UPLOADED_1,//photo uploaded, search for brucie
	LIFEINVADER_BRUCIE_FOUND_2,//brucie found, post comment
	//in the next mission
	LIFEINVADER_RESPONSE_PENDING_3,//on the page with brucies comment, but not posted response
	LIFEINVADER_COMPLETE_4//comment response to brucie posted, help chain complete
ENDENUM

ENUM_LIFEINVADER_HELP_SEQUENCE_STATE g_eLifeInvaderMissionPageState = LIFEINVADER_NO_PROGRESS_0
INT g_iLifeInvaderTutorialLastVisitedPage = -1

//end lifeinvader

SPECIAL_BROWSER_START_STATES g_BrowserStartState = SBSS_Default
INT g_iDelayATMDisplay = 0
//BOOL g_bBrowserBAWSAQCrashSequence = FALSE

CONST_INT WEB_BROWSER_PAGE_REFRESH_RATE		120	//ms


	// Webpage indices
ENUM WEBSITE_INDEX_ENUM
	 WWW_NOT_EXIST							= 0
	,WWW_BANK_COM							= 1
	,WWW_EYEFIND_INFO						= 2
	//3 WWW_DDWINGS_COM (removed)
	,WWW_BAWSAQ_COM							= 4
	,WWW_EPSILONPROGRAM_COM					= 5
	,WWW_LIFEINVADER_COM					= 6
	,WWW_LCN_D_EXCHANGE_COM					= 7
	//8 WWW_WINDSOR_D_REALESTATE_COM (removed) 
	,WWW_LENNYAVERY_D_REALTY_COM			= 9
	,WWW_LEGENDARYMOTORSPORT_NET			= 10
	,WWW_ELITASTRAVEL_COM					= 11
	,WWW_WARSTOCK_D_CACHE_D_AND_D_CARRY_COM	= 12
	,WWW_DOCKTEASE_COM						= 13
	,WWW_BLEETER_COM						= 14
	,WWW_PANDMCYCLES_COM					= 15
	,WWW_SOUTHERNSANANDREASSUPERAUTOS_COM	= 16
	,WWW_IWILLSURVIVEITALL_COM				= 17
	,WWW_DYNASTY8REALESTATE_COM				= 18
	,WWW_THEREALITYMILL_COM					= 19
	,WWW_THECHILDRENOFTHEMOUNTAIN_COM		= 20
	,WWW_MAZE_D_BANK_COM					= 21
	,WWW_FLEECA_COM							= 22
	,WWW_THEBANKOFLIBERTY_COM				= 23
	,WWW_MYDIVINEWITHIN_COM					= 24
	
	#IF NOT IS_JAPANESE_BUILD
	,WWW_HUSHSMUSH_COM						= 25
	#ENDIF
	,WWW_BENNYSORIGINALMOTORWORKS_COM		= 26
	,WWW_DYNASTY8EXECUTIVEREALTY_COM		= 27
	,FORECLOSURES_MAZE_D_BANK_COM			= 28
	,WWW_ARENAWAR_TV						= 29
	,WWW_THEDIAMONDCASINOANDRESORT_COM		= 30
	
	,WWW_EYEFIND_INFO_S_SEARCH				= 997
	,WWW_EYEFIND_INFO_S_ERROR				= 998
	
	,INVALID_WEBSITE_INDEX					= -1
ENDENUM

INT g_iTimeLastBrowserRefresh = -1
BOOL g_bForceBrowserRefresh = FALSE
INT g_iWebPageIndexFeedback = -1 /// use this to grab and register the page index so page specific form so
WEBSITE_INDEX_ENUM g_eWebSiteIndexFeedback = INVALID_WEBSITE_INDEX	///button presses can be handled.

INT g_sLastLastSiteHash = -1//arrgh bad naming
INT g_sLastSiteHash = -1

INT g_iLastSelectedWebIndex = -1
BOOL g_bLastSelectedWebIndexInvalid = TRUE

//This varying offset is because eddie added an extra column for the portfolio screen and effed up all the offsets
INT g_iLastSetFinanceSiteListOffset = 11

//Stock market interaction values
BOOL g_bBuySell 					= FALSE
BOOL g_bStockListMarketMode 		= TRUE///TODO Set this to true when markets are picked, false when portfolio is picked
INT g_iStockSelectedPortfolioIndex 	= -1
INT g_iStockSelectionFocus 			= -1
INT g_iSharesToBuySell 				= 0
INT g_iPrevSharesToBuySell 			= 0		//Used to check if the value has looped around MAXINT
//BOOL g_bShareBuySellButtonInvalidator = FALSE
INT g_iSharesBuySellCap				= 0
FLOAT g_fShareTotal 				= 0.0
FLOAT g_fOriginalProfit 			= 0.0
FLOAT g_fCurrentProfit 				= 0.0

BOOL g_bCultSiteVisitedSinceReset 	= FALSE
BOOL g_bMikeVisitedCultRobePage 	= FALSE // Being used to track B*1531470

FLOAT g_fWebCursorSensitivity 		= 10.0

BOOL g_bBAWSAQWebTutorialInProgress = FALSE

ENUM BROWSER_HELP_MESSAGE_TYPE_ENUM
	BHMT_STRING,
	BHMT_STRING_WITH_NUMBER,
	BHMT_STRING_WITH_TWO_NUMBERS,
	BHMT_STRING_WITH_STRING,
	BHMT_STRING_WITH_TWO_STRINGS,
	BHMT_STRING_WITH_NUMBER_AND_STRING,
	BHMT_STRING_WITH_PLAYER_NAME,
	BHMT_STRING_WITH_COLOURED_PLAYER_NAME,
	BHMT_STRING_WITH_TEAM_NAME,
	BHMT_STRING_WITH_TEAM_COLOURED_TEAM_NAME,
	BHMT_STRING_WITH_USER_CREATED_STRING,
	BHMT_STRING_WITH_TWO_USER_CREATED_STRINGS,
	BHMT_STRING_AS_USER_CREATED_STRING,
	BHMT_STRING_WITH_PLAYER_NAME_AND_STRING,
	BHMT_STRING_WITH_TWO_STRINGS_COLOURED,
	BHMT_STRING_WITH_COLOURED_NAME_AND_STRING,
	BHMT_STRING_WITH_PLAYER_NAME_AND_STRING_COLOURED,
	BHMT_STRING_WITH_COLOURED_STRING,
	BHMT_STRING_WITH_TWO_PLAYER_NAMES_AND_STRING,
	BHMT_STRING_WITH_TWO_PLAYER_NAMES_AND_STRING_COLOURED,
	BHMT_STRING_WITH_COLOURED_TEXT_LABEL,			//Actually does what WITH_COLOURED_STRING should do
	
	// Leave this at the bottom
	BHMT_NONE							// No active Objective Text
ENDENUM
STRUCT BROWSER_HELP_MESSAGE_STRUCT
	BOOL bShow = FALSE
	TEXT_LABEL_15 tl15Tag
	
	BROWSER_HELP_MESSAGE_TYPE_ENUM eTextType
	BOOL bForever = FALSE
	INT iNumToDisplay1
	INT iNumToDisplay2
	TEXT_LABEL_15 tl15StringToDisplay
ENDSTRUCT
BROWSER_HELP_MESSAGE_STRUCT g_sBrowserHelpMessage


BOOL g_bDisable_toggle_player_proofs_when_processing_internet_shopping_basket = FALSE

BOOL g_bBrowser_help_bunkmod_seen = FALSE

BOOL g_bOnlineMarket = FALSE

//Sell all results

FLOAT g_fLastStockSellAll_Totval = 0.0
FLOAT g_fLastStockSellAll_Profval = 0.0
INT g_iLastStockSellAll_Totshares = 0
INT g_iLastStockSellAll_Cotot = 0
FLOAT g_fLastStockSellAll_Valover = 0.0
FLOAT g_fLastStockSellAll_Valunder = 0.0
FLOAT g_fLastStockSellAll_NewWealthPerc = 0.0

BOOL g_bLastStaticSiteInvalid = FALSE

//browser handle, now has to be a global for preloading
SCALEFORM_INDEX g_BrosMov = NULL
SCALEFORM_INDEX g_BrosButtons = NULL
SCALEFORM_INDEX g_BrosNagScreen = NULL
BOOL g_bBrowserNagScreenState = FALSE
BOOL g_bBrowserVoucherNagScreenState = FALSE
BOOL g_bBrowserGoToStoreTrigger = FALSE
INT g_iBrowserGoToStoreItemHash = -1
INT g_iBrowserGoToStoreItemPrice = -1
BOOL g_bPropertyRefreshTrigger = FALSE
INT  g_iBrowserHealthDelta = 0


VEHICLE_SETUP_STRUCT_MP g_sConfVssMP


INT g_iChildrenOfTheMountainStatus = 0

CONST_INT c_MAX_EMAIL_CONTACTS 70  //(10 contacts is roughly 200 increase in stack size)
STRUCT m_structPlayerListEmail
	PLAYER_INDEX	plPlayers[NUM_NETWORK_PLAYERS]  // Array of PlayerIDs currently active NUM_NETWORK_PLAYERS
	INT	   plNumPlayers				 // Number of Player details listed
  
	//Old working regime...
	//GAMER_INFO	  plFriends[c_MAX_EMAIL_CONTACTS] // Need to up stack size dramatically. Testing with 25.
	
	//New trial. Don't think we really need to have a separate list of tl23 player names as they only get referenced when adding into scaleform. Primarily we use the handle.
	GAMER_HANDLE	plFriendsHandle[c_MAX_EMAIL_CONTACTS] // Need to up stack size dramatically. Testing with 25
	TEXT_LABEL_23   selectedPlayerName




	INT	   plTotalNumContacts
	INT	   m_selectedPlayerSlot
	//TEXT_LABEL_63   M_selectedPlayerClanName

	GAMER_HANDLE	selectedPlayerHandle
ENDSTRUCT
m_structPlayerListEmail  g_sPlayers

BOOL bg_bBypassValidGamerCheck = FALSE

TEXT_LABEL_63 g_sPendingBrowserPage
INT 		  g_iPendingBrowserTimer = -1
INT 		  g_iBrowserNoInputTimer = -1
INT 		  g_iBrowserTimer = 0

#IF IS_DEBUG_BUILD
BOOL g_bForceBawsaqNews = FALSE
BOOL g_bForceVehicleWebsite_sale = FALSE
BOOL g_bForcePropertyWebsite_sale = FALSE

STRUCT DEBUG_CLOTHES_REWARD_STRUCT
	BOOL bForceReward = FALSE
	
	TEXT_WIDGET_ID twHashGenerator, twGeneratedLabel
	INT iHashkey
	BOOL bGenerateHashkey
ENDSTRUCT
DEBUG_CLOTHES_REWARD_STRUCT g_sDebug_Vehicle_Clothes_Reward
#ENDIF

STRUCT YACHT_DATA_STRUCT
	INT iMod = -1
	INT iFixture = -1
	INT iLighting = -1
	INT iColour = -1
	INT iFlag = -1
	TEXT_LABEL_63 tl63Name //= ""
	TEXT_LABEL_63 tl63NewName //= ""
	INT iYachtNameID
ENDSTRUCT
YACHT_DATA_STRUCT g_sYachtDataStruct

STRUCT OFFICE_DATA_STRUCT
	INT 			iOfficeID		= -1
	INT 			iStyle			= -1
	INT 			iPersonnel		= -1
	INT 			iFont			= -1
	INT 			iColour			= -1
	TEXT_LABEL_63 	tl63OldOrgName
	TEXT_LABEL_63 	tl63NewOrgName
	INT 			iGunLocker		= -1
	INT 			iVault			= -1
	INT 			iAccommodation	= -1
	INT				iOfficeNameID
	
	INT 			iGarage1Style		= -1
	INT 			iGarage1Lighting	= -1
	INT 			iGarage1Number		= -1
	INT 			iGarage2Style		= -1
	INT 			iGarage2Lighting	= -1
	INT 			iGarage2Number		= -1
	INT 			iGarage3Style		= -1
	INT 			iGarage3Lighting	= -1
	INT 			iGarage3Number		= -1
	INT 			iModshop			= -1
ENDSTRUCT
OFFICE_DATA_STRUCT g_sOfficeDataStruct

STRUCT FIXER_HQ_DATA_STRUCT
	INT iAgencyID			= -1
	INT iArt				= -1
	INT iWallpaper			= -1
	INT iTint				= -1
	INT iVehWorkshop		= -1
	INT iArmory				= -1
	INT iPersonalQuarters	= -1
ENDSTRUCT
FIXER_HQ_DATA_STRUCT g_sFixerHQDataStruct

#IF FEATURE_DLC_2_2022
STRUCT MULTISTOREY_GARAGE_DATA_STRUCT
	INT iGarageID			= -1
	INT iStyle				= -1
	INT iTint				= -1
ENDSTRUCT
MULTISTOREY_GARAGE_DATA_STRUCT g_sMultistoreyGarageDataStruct
#ENDIF

INT g_iLastPendingGarageTransactionValue = 0

CONST_INT iCONST_DEFAULT_FREE_CLUBHOUSE_EMBLEM	1	//0

STRUCT CLUBHOUSE_DATA_STRUCT
	INT 			iClubhouseID	= -1
	INT 			iWall			= -1
	INT 			iHanging		= -1
	INT 			iFurniture		= -1
	INT 			iColourScheme	= -1
	INT 			iFont			= -1
	INT 			iFontColour		= -1
	INT 			iEmblem			= -1
	INT 			iHideSinage		= -1
	TEXT_LABEL_63 	tl63OldSignage
	TEXT_LABEL_63 	tl63NewSignage
	INT 			iGunLocker		= -1
	INT 			iGarage			= -1
	TEXT_LABEL_63 	tl63CrewEmblem
	INT				iClubhouseNameID
ENDSTRUCT
CLUBHOUSE_DATA_STRUCT g_sClubhouseDataStruct

STRUCT BUNKER_DATA_STRUCT
	INT 			iBunkerID		= -1
	INT 			iDecor			= -1
	INT 			iAccommodation	= -1
	INT 			iFiringRange	= -1
	INT 			iGunLocker		= -1
	INT 			iTransportation	= -1
ENDSTRUCT
BUNKER_DATA_STRUCT g_sBunkerDataStruct

STRUCT TRUCK_DATA_STRUCT
	INT iCab = -1
	INT iSection1 = -1
	INT iSection2 = -1
	INT iSection3 = -1
	INT iColour = -1
ENDSTRUCT
TRUCK_DATA_STRUCT g_sTruckDataStruct

STRUCT HANGAR_DATA_STRUCT
	INT 			iHangarID			= -1
	INT 			iStyle				= -1		//	iColour		
	INT 			iLighting			= -1
	INT 			iFlooring			= -1		//  iFloorDecal	
	INT 			iFurniture			= -1
	INT 			iSleepingQuarters	= -1		//  iAccommodation
	INT 			iWorkshop			= -1		//  iModshop
ENDSTRUCT
HANGAR_DATA_STRUCT g_sHangarDataStruct

STRUCT DEFUNCT_BASE_DATA_STRUCT
	INT 			iBaseID				= -1
	INT 			iStyle				= -1
	INT 			iGraphics			= -1
	INT 			iOrbitalWeapon		= -1
	INT 			iSecurityRoom		= -1
	INT 			iLounge				= -1
	INT 			iPersonalQuarters	= -1
	INT 			iPrivicyGlass		= -1
ENDSTRUCT
DEFUNCT_BASE_DATA_STRUCT g_sDefunctBaseDataStruct

STRUCT PLANE_DATA_STRUCT
	INT iAvenger = -1
	INT iInterior = -1
	INT iGunTurret = -1
	INT iVehShop = -1
	INT iWeaShop = -1
ENDSTRUCT
PLANE_DATA_STRUCT g_sPlaneDataStruct

STRUCT BUSINESS_HUB_DATA_STRUCT
	INT 			iHubID				= -1
	INT 			iStyle				= -1
	INT 			iGraphics			= -1
	INT 			iOrbitalWeapon		= -1
	INT 			iSecurityRoom		= -1
	INT 			iLounge				= -1
	INT 			iPersonalQuarters	= -1
	INT 			iPrivicyGlass		= -1
	
	TEXT_LABEL_63 	tl63OldSignage
	TEXT_LABEL_63 	tl63NewSignage
	INT				iNameID
ENDSTRUCT
BUSINESS_HUB_DATA_STRUCT g_sBusinessHubDataStruct
STRUCT NIGHTCLUB_DATA_STRUCT	
	INT 			iNightclubID		= -1
	INT 			iStyle				= -1
	INT 			iLighting			= -1
	INT 			iDancers			= -1
	INT 			iDryIce				= -1
	INT 			iStorage			= -1
	INT 			iGarage				= -1
	INT				iNameID				= -1
ENDSTRUCT
NIGHTCLUB_DATA_STRUCT g_sNightclubDataStruct

STRUCT HACKER_TRUCK_DATA_STRUCT
	INT iCab				= -1
	INT iDecal				= -1
	INT iTint				= -1
	INT iMissileLauncher	= -1
	INT iDroneStation		= -1
	INT iWeaShop			= -1
	INT iVehShop			= -1
ENDSTRUCT
HACKER_TRUCK_DATA_STRUCT g_sHackerTruckDataStruct

#IF FEATURE_HEIST_ISLAND
STRUCT KOSATKA_DATA_STRUCT
	INT iOwned				= -1
	INT iColour				= -1
	INT iFlag				= -1
	INT iSonar				= -1
	INT iMissiles			= -1
	INT iWeaShop			= -1
	INT iSeaSparrow2		= -1
	INT iAvisa				= -1
ENDSTRUCT
KOSATKA_DATA_STRUCT g_sKosatkaDataStruct
#ENDIF

STRUCT ARENA_GARAGE_DATA_STRUCT	
	INT 			iArenaID			= -1
	INT 			iStyle				= -1
	INT 			iGraphics			= -1
	INT 			iColour				= -1
	INT 			iExpansionFloorB1	= -1
	INT 			iExpansionFloorB2	= -1
	INT 			iBennyMechanic		= -1
	INT 			iWeaponMechanic		= -1
	INT				iPersonalQuarters	= -1
ENDSTRUCT
ARENA_GARAGE_DATA_STRUCT g_sArenaDataStruct

STRUCT CASINO_APT_DATA_STRUCT	
	INT 			iCasinoID			= -1
	INT 			iColourOption		= -1
	INT 			iStyleOption		= -1
	INT 			iLounge				= -1
	INT 			iBar				= -1
	INT 			iDealer				= -1
	INT 			iBedroom			= -1
	INT 			iMediaroom			= -1
	INT				iSpa				= -1
	INT				iOffice				= -1
	INT				iGarage				= -1
//	INT				iHelipad			= -1
	INT				iCasinoMembership	= -1
	TEXT_LABEL_63	tl63Gamertag
	TEXT_LABEL_23 	tl23MugshotTexture
ENDSTRUCT
CASINO_APT_DATA_STRUCT g_sCasinoDataStruct

BOOL g_bPurchasedCasinoAptInsideCasino = FALSE

#IF FEATURE_CASINO_HEIST
STRUCT ARCADE_DATA_STRUCT
	INT iArcadeID = -1
	INT iFloor = -1
	INT iCeiling = -1
	INT iWall = -1
	INT iPersonalQuarters = -1
	INT iGarage = -1
	INT iNeonLights = -1
	INT iScreens = -1
ENDSTRUCT
ARCADE_DATA_STRUCT g_sArcadeDataStruct
#ENDIF

//Vehicle shop data
ENUM VEHICLE_SITE_IDENTIFIER
	VEHSITE_CARS,
	VEHSITE_PLANES,
	VEHSITE_ARMY,
	VEHSITE_BOATS,
	TOTAL_VEHICLE_SITES
ENDENUM

ENUM SITE_BUYABLE_VEHICLE
	UNSET_BUYABLE_VEHICLE = -1,
	
	// WWW_LEGENDARYMOTORSPORT_NET
	BV_C_ZTYPE = 0,
	BV_C_STINGER,
	BV_C_JB700,
	BV_C_CHEETAH,
	BV_C_ENTITYXF,
	BV_C_VENEM,
	BV_C_MONROE,
	BV_C_COGCABRIO,
	BV_C_DUMMY_ITEM_DO_NOT_REMOVE, // Fix for 2271888
//	BV_C_VELOCE,
	
	// WWW_ELITASTRAVEL_COM	
	BV_P_DODO,
	BV_P_SHAMAL,
	BV_P_STUNT,
	BV_P_CUBAN,
	BV_P_DUSTER,
	BV_P_LUXOR,
	BV_P_FROGGER,
	BV_P_MAVERICK,
	
	// WWW_WARSTOCK_D_CACHE_D_AND_D_CARRY_COM
	BV_A_RHINO,
	BV_A_TITAN,
	BV_A_CARGOBOB,
	BV_A_BUZZARD,
	BV_A_CRUSADER,
	BV_A_BARRACKS,
	
	// WWW_DOCKTEASE_COM
	BV_B_MARINA,
	BV_B_MARQUIS,
	BV_B_JETMAX,
	BV_B_SPEEDER,
	BV_B_SQUALO,
	BV_B_SUNDECK,
	BV_B_TROPIC,
	BV_B_SEASHARK,
	BV_B_SUBMERSIBLE,
	BV_B_SUNTRAP,

	// WWW_PANDMCYCLES_COM
	BV_C_BMX,//3
	BV_C_SCORCHER,
	BV_C_WHIPPET,
	BV_C_ENDUREX,
	BV_C_TRICYCLE,
	BV_C_CRUISER,//8
	
	// WWW_SOUTHERNSANANDREASSUPERAUTOS_COM
	BV_C_SCHWARZER,
	BV_C_ZION,
	BV_C_GAUNTLET,
	BV_C_VIGERO,
	BV_C_ISSI2,
	BV_C_INFERNUS,
	BV_C_SURANO,
	BV_C_VACCA,
	BV_C_NINEF,
	BV_C_COMET2,
	BV_C_BANSHEE,
	BV_C_FELTZER2,
	BV_C_BFINJECTION,
	BV_C_SANDKING,
	BV_C_FUGITIVE,
	BV_C_DILETTANTE,
	BV_C_SUPERD,
	BV_C_EXEMPLAR,
	BV_C_BALLER2,
	BV_C_CAVALCADE,
	BV_C_ROCOTO,
	BV_C_FELON,
	BV_C_ORACLE2,
	BV_C_BATI,
	BV_C_AKUMA,	
	BV_C_RUFFIAN,
	BV_C_VADER,
	BV_C_BLAZER,	
	BV_C_PCJ,
	BV_C_SANCHEZ,
	BV_C_FAGGIO2,
	
	// Additional MP buyable vehicles
	// WWW_LEGENDARYMOTORSPORT_NET
	BV_MP_BULLET,
	BV_MP_CARBONIZZARE,
	BV_MP_COQUETTE,
	BV_MP_NINEF2,
	BV_MP_RAPIDGT,
	BV_MP_RAPIDGT2,
	BV_MP_STINGERGT,
	BV_MP_VOLTIC,
	// WWW_ELITASTRAVEL_COM
	BV_MP_ANNIHILATOR,
	BV_MP_MAMMATUS,
	BV_MP_VELUM,
	// WWW_WARSTOCK_D_CACHE_D_AND_D_CARRY_COM
	BV_MP_DUMP,
	BV_MP_AIRBUS,
	BV_MP_BUS,
	BV_MP_COACH,
	BV_MP_JOURNEY,
	BV_MP_MULE,
	BV_MP_RENTALBUS,
	BV_MP_STRETCH,
	// WWW_SOUTHERNSANANDREASSUPERAUTOS_COM
	BV_MP_BISON,
	BV_MP_DOUBLE,
	BV_MP_FELON2,
	BV_MP_HEXER,
	BV_MP_ZION2,
	BV_MP_BATI2,
	// Social Club sign-up vehicle
	BV_SC_ELEGY2,
	// Collector edition MP only vehicle
	BV_CE_KHAMELION,
	// Collector edition SP/MP vehicles
	BV_CE_HOTKNIFE,
	BV_CE_CARBONRS,

	// Beach Bum pack
	BV_DLC_BIFTA,
	BV_DLC_KALAHARI,
	BV_DLC_PARADISE,
	BV_DLC_SPEEDER,
	// Beach Bum additional site vehicles for MP
	BV_MP_BODHI2,
	BV_MP_DUNE,
	BV_MP_REBEL,
	BV_MP_SADLER,
	BV_MP_SANCHEZ2,
	BV_MP_SANDKINGSWB,
	
	// Valentines pack
	BV_DLC_ROOSEVELT,
	
	// Business pack 1
	BV_DLC_ALPHA,
	BV_DLC_JESTER,
	BV_DLC_TURISMO,
	BV_DLC_VESTRA,
	
	// Business pack 2
	BV_DLC_MASSACRO,
	BV_DLC_ZENTORNO,
	BV_DLC_HUNTLEY,
	BV_DLC_COQUETTE_TOPLESS,
	BV_C_BANSHEE_TOPLESS,
	BV_DLC_STINGER_TOPLESS,
	BV_DLC_VOLTIC_HARDTOP,
	BV_DLC_THRUST,
	BV_MP_DOMINATOR,
	BV_MP_F620,
	BV_MP_FUSILADE,
	BV_MP_PENUMBRA,
	BV_MP_SENTINEL,
	BV_MP_SENTINEL2,
	
	// Business pack additional site vehicles for MP
	BV_MP_ASEA,
	BV_MP_ASTEROPE,
	BV_MP_BOBCATXL,
	BV_MP_CAVALCADE2,
	BV_MP_GRANGER,
	BV_MP_INGOT,
	BV_MP_INTRUDER,
	BV_MP_MINIVAN,
	BV_MP_PREMIER,
	BV_MP_RADIUS,
	BV_MP_RANCHERXL,
	BV_MP_RATLOADER,
	BV_MP_STANIER,
	BV_MP_STRATUM,
	BV_MP_WASHINGTON,
	
	// Hipster pack
	BV_DLC_BLADE,
	BV_DLC_WARRENER,
	BV_DLC_GLENDALE,
	BV_DLC_RHAPSODY,
	BV_DLC_PANTO,
	BV_DLC_DUBSTA3,
	BV_DLC_PIGALLE,
	
	// Hipster pack additional site vehicles for MP
	BV_MP_PICADOR,
	BV_MP_REGINA,
	BV_MP_SURFER,
	BV_MP_YOUGA,
	BV_MP_BLAZER3,
	BV_MP_REBEL2,
	BV_MP_PRIMO,
	BV_MP_BUFFALO,
	BV_MP_BUFFALO2,
	BV_MP_TAILGATER,
	
	//Independance pack
	BV_MP_LIBERATOR,
	BV_MP_SOVEREIGN,
	
	// Pilot School pack
	BV_DLC_MILJET,
	BV_DLC_BESRA,
	BV_DLC_SWIFT,
	BV_MP_COQUETTE2,
	BV_MP_COQUETTE2_TOPLESS,
	
	//LTS pack
	BV_DLC_INNOVATION,
	BV_DLC_HAKUCHOU,
	BV_DLC_FUROREGT,
	BV_DLC_KALAHARI_TOPLESS,
	
	//Heist pack
	BV_DLC_BOXVILLE4,
	BV_DLC_CASCO,
	BV_DLC_DINGHY3,
	BV_DLC_ENDURO,
	BV_DLC_GBURRITO2,
	BV_DLC_GUARDIAN,
	BV_DLC_HYDRA,
	BV_DLC_INSURGENT,
	BV_DLC_INSURGENT2,
	BV_DLC_KURUMA,
	BV_DLC_KURUMA2,
	BV_DLC_LECTRO,
	BV_DLC_MULE3,
	BV_DLC_PBUS,
	BV_DLC_SAVAGE,
	BV_DLC_TECHNICAL,
	BV_DLC_VALKYRIE,
	BV_DLC_VELUM2,
	
	//Heist pack additional site vehicles for MP
	BV_MP_GRESLEY,
	BV_MP_JACKAL,
	BV_MP_LANDSTALKER,
	BV_MP_MESA3,
	BV_MP_NEMESIS,
	BV_MP_ORACLE,
	BV_MP_RUMPO,
	BV_MP_SCHAFTER2,
	BV_MP_SEMINOLE,
	BV_MP_SURGE,

	#IF IS_NEXTGEN_BUILD
	//CG-NG pack
	BV_NG_DODO,
	BV_NG_MARSHALL,
	BV_NG_SUBMERSIBLE2,
	BV_NG_BLISTA2,
	BV_NG_STALION,
	BV_NG_DUKES,
	BV_NG_DUKES2,
	BV_NG_STALION2,
	BV_NG_DOMINATOR2,
	BV_NG_GAUNTLET2,
	BV_NG_BUFFALO3,
	#ENDIF
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		CAREFUL ADDING NEW VEHICLES BELOW HERE FOR NG
		savegame size relies on NUMBER_OF_BUYABLE_VEHICLES_SP being a constant
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	//Christmas2 pack
	BV_DLC_SLAMVAN,
	BV_DLC_RATLOADER2,
	BV_DLC_JESTER2,
	BV_DLC_MASSACRO2,
	
	//Luxe pack						//BV_LX
	BV_LX_FELTZER3,
	BV_LX_LUXOR2,
	BV_LX_OSIRIS,
	BV_LX_SWIFT2,
	BV_LX_VIRGO,
	BV_LX_WINDSOR,

	//Luxe2 pack					//BV_L2
	BV_L2_BRAWLER,
	BV_L2_CHINO,
	BV_L2_COQUETTE3,
	BV_L2_T20,
	BV_L2_TORO,
	BV_L2_VINDICATOR,

		//BV_S1
	BV_S1_BUCCANEER,
	BV_S1_FACTION,
	BV_S1_MOONBEAM,
	BV_S1_PRIMO,
	BV_S1_VOODOO2,

		//BV_HA
	BV_HA_BTYPE2,
	BV_HA_LURCHER,
	
		//BV_AP
	BV_AP_BALLER3,
	BV_AP_BALLER4,
	BV_AP_COG55,
	BV_AP_COGNOSCENTI,
	BV_AP_LIMO2,
	BV_AP_MAMBA,
	BV_AP_NIGHTSHADE,
	BV_AP_SCHAFTER3,
	BV_AP_SCHAFTER4,
	BV_AP_VERLIERER2,
	BV_AP_SUPERVOLITO1,
	BV_AP_SUPERVOLITO2,
	BV_DLC_BIG_YACHT,
	
				//BV_X3
	BV_X3_TAMPA,
	
			//BV_JA
	BV_JA_SULTAN,
//	BV_DLC_BANSHEE,		//BV_C_BANSHEE/BV_C_BANSHEE_TOPLESS??
	
			//BV_V2
	BV_V2_ROOSEVELT2,
	
		//BV_S2
//	BV_DLC_MINIVAN,		//BV_MP_MINIVAN?
	BV_S2_SABREGT,
	BV_S2_TORNADO,
	BV_S2_VIRGO3,
	BV_S2_FACTION_DONK,
	
				//BV_EX
	BV_EX_VOLATUS,
	BV_EX_CARGOBOB2,
	BV_EX_RUMPO3,
	BV_EX_BRICKADE,
	BV_EX_NIMBUS,
	BV_EX_TUG,
	BV_EX_WINDSOR2,
	BV_EX_PROTOTIPO,
	BV_EX_FMJ,
	BV_EX_BESTIAGTS,
	BV_EX_XLS,
	BV_EX_SEVEN70,
	BV_EX_PFISTER811,
	BV_EX_REAPER,
	
					//BV_ST
	BV_ST_LE7B,
	BV_ST_OMNIS,
	BV_ST_TROPOS,
	BV_ST_BRIOSO,
	BV_ST_TROPHYTRUCK,
	BV_ST_TROPHYTRUCK2,
	BV_ST_CONTENDER,
	BV_ST_CLIFFHANGER,
	BV_ST_BF400,
	BV_ST_RALLYTRUCK,
	BV_ST_TAMPA2,
	BV_ST_GARGOYLE,
	BV_ST_TYRUS,
	BV_ST_LYNX,
	BV_ST_SHEAVA,
	
					//BV_BI
	BV_BI_BAGGER,
	BV_BI_ESSKEY,
	BV_BI_NIGHTBLADE,
	BV_BI_DEFILER,
	BV_BI_AVARUS,
	BV_BI_ZOMBIEA,
	BV_BI_ZOMBIEB,
	BV_BI_CHIMERA,
	BV_BI_DAEMON2,
	BV_BI_RATBIKE,
	BV_BI_SHOTARO,
	BV_BI_RAPTOR,
	BV_BI_HAKUCHOU2,
	BV_BI_VORTEX,
	BV_BI_BLAZER4,
	BV_BI_SANCTUS,
	BV_BI_MANCHEZ,
	BV_BI_TORNADO6,
	BV_BI_YOUGA2,
	BV_BI_WOLFSBANE,
	BV_BI_FAGGIO3,
	BV_BI_FAGGIO,
	
			//BV_IE
	BV_IE_DUNE5,
	BV_IE_PHANTOM2,
	BV_IE_TECHNICAL2,
	BV_IE_BLAZER5,
	BV_IE_BOXVILLE5,
	BV_IE_WASTELANDER,
	BV_IE_RUINER2,
	BV_IE_VOLTIC2,
	BV_IE_PENETRATOR,
	BV_IE_TEMPESTA,
	BV_IE_ITALIGTB,
	BV_IE_NERO,
	BV_IE_DIABLOUS,
	BV_IE_FCR,
//	BV_IE_ELEGY2,		//BV_SC_ELEGY2?
//	BV_IE_COMET2,		//BV_C_COMET2?
	BV_IE_SPECTER,
	
	BV_SR_GP1,
	BV_SR_INFERNUS2,
	BV_SR_RUSTON,
	BV_SR_TURISMO2,
	
	BV_DLC_BIG_TRUCK,
	BV_DLC_BALLISTIC_EQUIPMENT,
	BV_GR_CHEETAH2,
	BV_GR_TORERO,
	BV_GR_VAGNER,
	BV_GR_XA21,
	BV_GR_APC,
	BV_GR_DUNE3,
	BV_GR_HALFTRACK,
	BV_GR_OPPRESSOR,
	BV_GR_TAMPA3,
	BV_GR_TRAILERSMALL2,
	BV_GR_ARDENT,
	BV_GR_NIGHTSHARK,
	
	BV_SM_LAZER,
	BV_SM_MICROLIGHT,
	BV_SM_MOGUL,
	BV_SM_ROGUE,
	BV_SM_STARLING,
	BV_SM_SEABREEZE,
	BV_SM_TULA,
	BV_SM_PYRO,
	BV_SM_MOLOTOK,
	BV_SM_NOKOTA,
	BV_SM_BOMBUSHKA,
	BV_SM_HUNTER,
	BV_SM_HAVOK,
	BV_SM_HOWARD,
	BV_SM_ALPHAZ1,
	BV_SM_CYCLONE,
	BV_SM_VISIONE,
	BV_SM_RETINUE,
	BV_SM_RAPIDGT3,
	BV_SM_VIGILANTE,
	
	BV_DLC_BIG_PLANE,
	BV_GO_DELUXO,
	BV_GO_STROMBERG,
	BV_GO_RIOT2,
	BV_GO_CHERNOBOG,
	BV_GO_KHANJALI,
	BV_GO_AKULA,
	BV_GO_THRUSTER,
	BV_GO_BARRAGE,
	BV_GO_VOLATOL,
	BV_GO_COMET4,
	BV_GO_NEON,
	BV_GO_STREITER,
	BV_GO_SENTINEL3,
	BV_GO_YOSEMITE,
	BV_GO_SC1,
	BV_GO_AUTARCH,
	BV_GO_GT500,
	BV_GO_HUSTLER,
	BV_GO_REVOLTER,
	BV_GO_PARIAH,
	BV_GO_RAIDEN,
	BV_GO_SAVESTRA,
	BV_GO_RIATA,
	BV_GO_HERMES,
	BV_GO_COMET5,
	BV_GO_Z190,
	BV_GO_VISERIS,
	BV_GO_KAMACHO,
	
	BV_AR_GB200,
	BV_AR_FAGALOA,
	BV_AR_ELLIE,
	BV_AR_ISSI3,
	BV_AR_MICHELLI,
	BV_AR_FLASHGT,
	BV_AR_HOTRING,
	BV_AR_TEZERACT,
	BV_AR_TYRANT,
	BV_AR_DOMINATOR3,
	BV_AR_TAIPAN,
	BV_AR_ENTITY2,
	BV_AR_JESTER3,
	BV_AR_CHEBUREK,
	BV_AR_CARACARA,
	BV_AR_SEASPARROW,
	
	BV_DLC_HACKER_TRUCK,
	BV_BB_MULE4,
	BV_BB_POUNDER2,
	BV_BB_SWINGER,
	BV_BB_MENACER,
	BV_BB_SCRAMJET,
	BV_BB_STRIKEFORCE,
	BV_BB_OPPRESSOR2,
	BV_BB_PATRIOT2,
	BV_BB_STAFFORD,
	BV_BB_FREECRAWLER,
	BV_BB_BLIMP3,
	BV_BB_PBUS2,
	BV_MP_FUTO,
	BV_MP_RUINER,
	BV_MP_ROMERO,
	BV_MP_PRAIRIE,
	BV_MP_BALLER,
	BV_MP_SERRANO,
	BV_MP_BJXL,
	BV_MP_HABANERO,
	BV_MP_FQ2,
	BV_MP_PATRIOT,
	
	BV_AW_CERBERUS,		// Arena ready (apocalypse)
	BV_AW_CERBERUS2,	// Arena ready (sci fi)
	BV_AW_CERBERUS3,	// Arena ready (consumer wasteland)
	BV_AW_BRUTUS,		// Arena ready (apocalypse)
	BV_AW_BRUTUS2,		// Arena ready (sci fi)
	BV_AW_BRUTUS3,		// Arena ready (consumer wasteland)
	BV_AW_SCARAB,		// Arena ready (apocalypse)
	BV_AW_SCARAB2,		// Arena ready (sci fi)
	BV_AW_SCARAB3,		// Arena ready (consumer wasteland)
	BV_AW_IMPERATOR,	// Arena ready (apocalypse)
	BV_AW_IMPERATOR2,	// Arena ready (sci fi)
	BV_AW_IMPERATOR3,	// Arena ready (consumer wasteland)
	BV_AW_ZR380,		// Arena ready (apocalypse)
	BV_AW_ZR3802,		// Arena ready (sci fi)
	BV_AW_ZR3803,		// Arena ready (consumer wasteland)
	BV_AW_IMPALER,		// Upgradeable
	BV_AW_VAMOS,
	BV_AW_TULIP,
	BV_AW_DEVESTE,
	BV_AW_SCHLAGEN,
	BV_AW_TOROS,
	BV_AW_DEVIANT,
	BV_AW_CLIQUE,
	BV_AW_ITALIGTO,
	BV_AW_RCBANDITO,
	BV_MP_TAXI,
	BV_MP_BULLDOZER,
	BV_MP_SPEEDO2,
	BV_MP_TRASH2,
	BV_MP_BARRACKS2,
	BV_MP_MIXER,
	BV_MP_DUNE2,
	BV_MP_TRACTOR,
	BV_MP_BLISTA3,
	
	BV_CA_THRAX,
	BV_CA_DRAFTER,
	BV_CA_LOCUST,
	BV_CA_NOVAK,
	BV_CA_ZORRUSSO,
	BV_CA_GAUNTLET3,
	BV_CA_ISSI7,
	BV_CA_ZION3,
	BV_CA_NEBULA,
	BV_CA_HELLION,
	BV_CA_DYNASTY,
	BV_CA_RROCKET,
	BV_CA_PEYOTE2,
	BV_CA_GAUNTLET4,
	BV_CA_CARACARA2,
	BV_CA_JUGULAR,
	BV_CA_S80,
	BV_CA_KRIEGER,
	BV_CA_EMERUS,
	BV_CA_NEO,
	BV_CA_PARAGON,
	
	// Casino Heist
	BV_CH_FIRETRUK,
	BV_CH_BURRITO2,
	BV_CH_BOXVILLE,
	BV_CH_STOCKADE,
	BV_CH_LGUARD,
	BV_CH_BLAZER2,
	BV_CH_ZHABA,
	BV_CH_MINITANK,
	BV_CH_JB7002,
	BV_CH_STRYDER,
	BV_CH_VSTR,
	BV_CH_FORMULA,
	BV_CH_IMORGON,
	BV_CH_FORMULA2,
	BV_CH_FURIA,
	BV_CH_REBLA,
	BV_CH_VAGRANT,
	BV_CH_RETINUE2,
	BV_CH_YOSEMITE2,
	BV_CH_KOMODA,
	BV_CH_SULTAN2,
	BV_CH_SUGOI,
	BV_CH_EVERON,
	BV_CH_ASBO,
	BV_CH_KANJO,
	BV_CH_OUTLAW,
	
//	#IF FEATURE_COPS_N_CROOKS
//	BV_CNC_SURFER3,	
//	BV_CNC_ZOKU,
//	BV_CNC_POLBATI,
//	BV_CNC_POLBUFFALO,
//	BV_CNC_POLGAUNTLET,
//	BV_CNC_POLGRANGER,
//	BV_CNC_POLICE5,
//	BV_CNC_POLICE6,
//	BV_CNC_POLICEB2,
//	BV_CNC_POLICET2,
//	BV_CNC_POLPANTO,
//	BV_CNC_POLCARACARA,
//	BV_CNC_POLRIATA,
//	BV_CNC_POLGREENWOOD,
//	#ENDIF
	
	// Summer Pack 2020
	BV_SUM_CLUB,
	BV_SUM_DUKES3,
	BV_SUM_YOSEMITE3,
	BV_SUM_PENUMBRA2,
	BV_SUM_LANDSTALKER2,
	BV_SUM_SEMINOLE2,	
	BV_SUM_TIGON,	
	BV_SUM_OPENWHEEL1,	
	BV_SUM_OPENWHEEL2,
	BV_SUM_COQUETTE4,
	BV_SUM_MANANA,
	BV_SUM_PEYOTE,
	
	// Island Heist 2020
	#IF FEATURE_HEIST_ISLAND
	BV_IH_KOSATKA,
	BV_IH_ITALIRSX,
	BV_IH_SLAMTRUCK,
	BV_IH_BRIOSO2,
	BV_IH_WEEVIL,
	BV_IH_ALKONOST,
	BV_IH_ANNIHILATOR2,
//	BV_IH_AVISA,
	BV_IH_DINGHY5,
	BV_IH_MANCHEZ2,
	BV_IH_PATROLBOAT,
//	BV_IH_SEASPARROW2,
	BV_IH_SQUADDIE,
	BV_IH_TOREADOR,
	BV_IH_VERUS,
	BV_IH_VETIR,
	BV_IH_WINKY,
	BV_IH_LONGFIN,
	BV_IH_VETO,
	BV_IH_VETO2,
	#ENDIF
	#IF FEATURE_TUNER
	BV_TU_CALICO,
	BV_TU_COMET6,
	BV_TU_CYPHER,
	BV_TU_DOMINATOR7,
	BV_TU_JESTER4,
	BV_TU_REMUS,
	BV_TU_VECTRE,
	BV_TU_ZR350,
	BV_TU_WARRENER2,
	BV_TU_RT3000,
	BV_TU_FUTO2,
	BV_TU_SULTAN3,
	BV_TU_TAILGATER2,
	BV_TU_DOMINATOR8,
	BV_TU_EUROS,
	BV_TU_GROWLER,
	BV_TU_PREVION,
	#ENDIF
	#IF FEATURE_GEN9_EXCLUSIVE
	BV_G9E_S95,
	BV_G9E_ARBITERGT,
	BV_G9E_ASTRON2,
	BV_G9E_CYCLONE2,
	BV_G9E_IGNUS2,
	#ENDIF
	BV_AGENCY_HELI,
	BV_AGENCY_COMET7,
	BV_AGENCY_SHINOBI,
	BV_AGENCY_REEVER,
	BV_AGENCY_BALLER7,
	BV_AGENCY_CINQUEMILA,
	BV_AGENCY_JUBILEE,
	BV_AGENCY_ASTRON,
	BV_AGENCY_DEITY,
	BV_AGENCY_ZENO,
	BV_AGENCY_CHAMPION,
	BV_AGENCY_IGNUS,
	BV_AGENCY_BUFFALO4,
	BV_AGENCY_GRANGER2,
	BV_AGENCY_IWAGEN,
	BV_AGENCY_PATRIOT3,
	#IF FEATURE_DLC_1_2022
	BV_SUM2_TENF,
	BV_SUM2_SM722,
	BV_SUM2_TORERO2,
	BV_SUM2_CORSITA,
	BV_SUM2_LM87,
	BV_SUM2_OMNISEGT,
	BV_SUM2_RHINEHART,
	BV_SUM2_POSTLUDE,
	BV_SUM2_KANJOSJ,
	BV_SUM2_VIGERO2,
	BV_SUM2_RUINER4,
	BV_SUM2_DRAUGUR,
	BV_SUM2_GREENWOOD,
	BV_SUM2_CONADA,
	#ENDIF
	
	TOTAL_BUYABLE_VEHICLES,
	
	//100 should be enough extra spaces for SP save globals hopefully
	//based on //depot/gta5/script/NG_Submission_5_0/singleplayer/include/globals/website_globals.sch
	NUMBER_OF_BUYABLE_VEHICLES_SP = 312
ENDENUM

CONST_INT TOTAL_BUYABLE_VEHICLES_CONST TOTAL_BUYABLE_VEHICLES

ENUM SITE_BUYABLE_VEHICLE_COLOURS
	BCV_NO_COLOUR,
	BCV_RED_COLOUR,		//USA
	BCV_PINK_COLOUR,	//AUS
	BCV_YELLOW_COLOUR,	//BRA
	BCV_ORANGE_COLOUR,	//SPA
	BCV_GREEN_COLOUR,	//CAN
	BCV_BLUE_COLOUR,	//FRA
	BCV_BLACK_COLOUR,	//GER
	BCV_WHITE_COLOUR,	//JAP
	
	BCV_COLOUR_1,		//SCO	//BROWN
	BCV_COLOUR_2,		//UK	//CYAN
	BCV_COLOUR_3,		//ITA	//GREY
	BCV_COLOUR_4,		//SWI	//MINT
	BCV_COLOUR_5,		//JAM	//PINK
	BCV_COLOUR_6,		//COL	//PALE YELLOW
	BCV_COLOUR_7,		//NOR	//BURGUNDY
	BCV_COLOUR_8,		//SWE	//LILAC
	BCV_COLOUR_9,		//BEL
	BCV_COLOUR_10,		//MEX
	BCV_COLOUR_11,		//AUT
	BCV_COLOUR_12,		//RUS
	BCV_COLOUR_13,		//ARG
	BCV_COLOUR_14,		//TUR
	BCV_COLOUR_15,		//IRE
	BCV_COLOUR_16,		//WAL
	BCV_COLOUR_17,		//ENG
	
	BCV_COLOUR_18,		//EU
	BCV_COLOUR_19,		//FIN
	BCV_COLOUR_20,		//NED
	BCV_COLOUR_21,		//POR
	BCV_COLOUR_22,		//KOR
	BCV_COLOUR_23,		//CHI
	BCV_COLOUR_24,		//HUN
	BCV_COLOUR_25,		//NZ
	BCV_COLOUR_26,		//PUR
	BCV_COLOUR_27,		//CRO
	BCV_COLOUR_28,		//ISR
	BCV_COLOUR_29,		//NIG
	BCV_COLOUR_30,		//SVK
	BCV_COLOUR_31,		//CZE
	BCV_COLOUR_32,		//LIC
	BCV_COLOUR_33,		//PAL
	BCV_COLOUR_34,		//SOU
	BCV_COLOUR_35,		//DEN
	BCV_COLOUR_36,		//MAL
	BCV_COLOUR_37,		//POL
	BCV_COLOUR_38		//SLO
	
	#IF FEATURE_GEN9_EXCLUSIVE
	,BCV_COLOUR_39 		// DARKPURPLEPEARL  
	#ENDIF
ENDENUM

SITE_BUYABLE_VEHICLE_COLOURS g_eLastBuyableVehicleColourSelected = BCV_NO_COLOUR
BOOL g_bLastBuyableVehicleArmourSelected = FALSE

INT g_iClothingForVehBits[(TOTAL_BUYABLE_VEHICLES_CONST/32) +1]
INT g_iClothingForWebBits[1]
INT g_iChipsForVehBits[(TOTAL_BUYABLE_VEHICLES_CONST/32) +1]
INT g_iChipsForPropBits[(TOTAL_BUYABLE_VEHICLES_CONST/32) +1]

//Vehicle purchased state// these need to be saved
STRUCT BuyableVehicleSaved
	INT g_bOwnedVehicleStates[NUMBER_OF_BUYABLE_VEHICLES_SP]
	SITE_BUYABLE_VEHICLE_COLOURS g_eOwnedVehicleColoursM[NUMBER_OF_BUYABLE_VEHICLES_SP]
	SITE_BUYABLE_VEHICLE_COLOURS g_eOwnedVehicleColoursF[NUMBER_OF_BUYABLE_VEHICLES_SP]
	SITE_BUYABLE_VEHICLE_COLOURS g_eOwnedVehicleColoursT[NUMBER_OF_BUYABLE_VEHICLES_SP]
ENDSTRUCT

INT g_iEpsilonTutorialStage = -1

ENUM EPSILON_FAIL_PAGE_REASON
	EPSILON_SUPPRESSIVE,
	EPSILON_OUT_OF_STOCK,
	EPSILON_NO_MONEY,
	EPSILON_IP_INFRINGE
ENDENUM

EPSILON_FAIL_PAGE_REASON g_eEpsilonFailReason = EPSILON_SUPPRESSIVE

BOOL g_bIEAppVisible = FALSE
BOOL g_bFHQAppOpen = FALSE
BOOL g_bUpdateLeakStrandOnRollover = TRUE
BOOL g_bForceCloseBrowserForOfficeGarageGPS = TRUE

BOOL g_bForceActivitySeatExit = FALSE

BOOL g_bBrowser_MBANKHELP_seen = FALSE		// [MBANKHELP]	Go to foreclosures.maze-bank.com to purchase a Facility. This property gives you access to The Doomsday Heist planning room as a VIP, CEO or MC President.

#IF FEATURE_CASINO_HEIST
BOOL g_bBrowser_ARCADEHELP_seen = FALSE		// [ARCADEHELP]	Purchase an Arcade ~BLIP_LAPTOP~ to gain access to The Casino Heist.
#ENDIF

INT g_iPotentialFixForMatchingPropertySellingId = 0			//HASH("FACTORY_INDEX_28_t0_v0")
INT g_iPotentialFixForMatchingPropertySellingId_CESP = 0	//HASH("FACTORY_INDEX_28_t0_v0_CESP")

#IF FEATURE_FIXER
BOOL g_bGlobalAmbientInternetDisabled
#ENDIF


#IF FEATURE_TUNER
ENUM VEHICLE_WEBSITES
	VEHICLEWEBSITE_INVALID = -1,
	
	VEHICLEWEBSITE_LEGENDARY_MOTORSPORT,
	VEHICLEWEBSITE_SOUTHERN_SAN_ANDREAS_SUPER_AUTOS,
	VEHICLEWEBSITE_BENNYS_ORIGINAL_MOTORWORKS,
	VEHICLEWEBSITE_ARENA_WAR,
	VEHICLEWEBSITE_ELITAS_TRAVEL,
	VEHICLEWEBSITE_WARSTOCK_CACHE_AND_CARRY,
	VEHICLEWEBSITE_DOCKTEASE,
	VEHICLEWEBSITE_PEDAL_AND_METAL_CYCLES,
	
	VEHICLEWEBSITE_MAX
ENDENUM

STRUCT AUTO_SHOP_DATA_STRUCT
	INT iAutoShopID = -1
	INT iWall = -1
	INT iTint = -1
	INT iEmblem = -1
	INT iCrewName = -1
	INT iStaff1 = -1
	INT iStaff2 = -1
	INT iCarLift = -1
	INT iPersonalQuarters = -1
	INT iCarClubMembership = -1
ENDSTRUCT
AUTO_SHOP_DATA_STRUCT g_sAutoShopDataStruct
#ENDIF
