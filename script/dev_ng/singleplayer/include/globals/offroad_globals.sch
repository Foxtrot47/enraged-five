// offroad_globals.sch
USING "script_maths.sch"

ENUM OFFROADRACE_BIT_FLAGS
	OFFROAD_F_WonCurrentRace = BIT0,	// This is temporarily set when the race setup needs to tell the controller that we won the race. (BIT0)
	
	OFFROAD_F_Race5Won		= BIT1,
	OFFROAD_F_Race8Won		= BIT2,
	OFFROAD_F_Race9Won		= BIT3,
	OFFROAD_F_Race10Won		= BIT4,
	OFFROAD_F_Race11Won		= BIT5,
	OFFROAD_F_Race12Won		= BIT6,
	OFFROAD_F_Race13Won		= BIT7

ENDENUM

#IF IS_DEBUG_BUILD
	INT g_ORR_DebugLaunchWarpTime = 0 
#ENDIF

/// PURPOSE: Indices to our races.
///   			EACH TIME YOU DEFINE A NEW ONE, ADD IT TO THE FUNC BELOW
ENUM OFFROAD_RACE_INDEX
	OFFROAD_RACE_NONE = -1,
	
	OFFROAD_RACE_CANYON_CLIFFS = 0,
	OFFROAD_RACE_RIDGE_RUN,
	OFFROAD_RACE_MINEWARD_SPIRAL,
	OFFROAD_RACE_VALLEY_TRAIL,
	OFFROAD_RACE_LAKESIDE_SPLASH,
	OFFROAD_RACE_ECO_FRIENDLY,
	OFFROAD_RACE13, 
	
	NUM_OFFROAD_RACES
ENDENUM

STRUCT RACE_LAUNCH_DATA
	OFFROAD_RACE_INDEX 	raceToLaunch
	VECTOR 				raceStartLoc
	PED_INDEX 			Racer[5]
	VEHICLE_INDEX		RaceVehicle[5]
	PED_INDEX			Loaner
	VEHICLE_INDEX		LoanerVehicle
ENDSTRUCT

STRUCT OffroadSaved
	INT 				iBitFlags
	OFFROAD_RACE_INDEX 	eCurrentRace
	INT					iBestRank[NUM_OFFROAD_RACES]
	FLOAT 				fBestTime[NUM_OFFROAD_RACES]
	INT					iRacePlaceEarned
	INT					iRaceBestPlaces[NUM_OFFROAD_RACES]
ENDSTRUCT

// We don't have any save data yet... I kind just need this function.
FUNC VECTOR OFFROAD_RACE_GET_LOC_FOR_INDEX(OFFROAD_RACE_INDEX raceIndex)
	// Make sure it's valid.
	IF (raceIndex >= NUM_OFFROAD_RACES) OR (raceIndex <= OFFROAD_RACE_NONE)
		//DEBUG_MESSAGE("You're asking for a race location out of range. Returning <<0,0,0>>!!")
		RETURN <<0,0,0>>
	ENDIF
	
	// Get the race loc for that index.
	SWITCH (raceIndex)
		CASE OFFROAD_RACE_CANYON_CLIFFS
			RETURN <<-1939.4829, 4443.9526, 37.3474>>		// Canyon Cliffs - 1 in menu.
			
		CASE OFFROAD_RACE_RIDGE_RUN
			RETURN <<-516.9256, 2008.0140, 204.0998>>		// Ridge Run - 2 in menu.
	
		CASE OFFROAD_RACE_VALLEY_TRAIL	
			RETURN <<-223.6755, 4224.6436, 43.7304>>	// Valley Trail - 3 in menu.		
	
		CASE OFFROAD_RACE_LAKESIDE_SPLASH
			RETURN <<1606.5785, 3841.1882, 33.2931>>	// Lakeside Splash - 4 in menu.		
			
		CASE OFFROAD_RACE_ECO_FRIENDLY
			RETURN <<2037.6644, 2137.3862, 92.7095>>	// Eco Friendly - 5 in menu.

		CASE OFFROAD_RACE_MINEWARD_SPIRAL
			RETURN <<2996.7759, 2774.0850, 43.2600>>	// Mineward Spiral - 6 in menu.
		
//		CASE OFFROAD_RACE13
//			RETURN << 1222.9738, 2373.2100, 64.7099 >>	// Construction Course - 7 in menu.
//		BREAK
	ENDSWITCH
	
	//DEBUG_MESSAGE("You're asking for a race location and the index hasn't been assigned a point. Returning <<0,0,0>>!!")
	RETURN <<0, 0, 0>>
ENDFUNC


/// PURPOSE:
///    Returns the OFFROAD_RACE_INDEX that the player is nearest to.
/// PARAMS:
///    vCurLoc - Player's current location
/// RETURNS:
///    OFFROAD_RACE_INDEX that we're closest to.
FUNC OFFROAD_RACE_INDEX GET_RACE_INDEX_BY_POSITION(VECTOR vCurLoc, VECTOR & vRaceStartLoc)
	// We'll return this.
	OFFROAD_RACE_INDEX eRetInd = OFFROAD_RACE_CANYON_CLIFFS
	
	// STORE ALL RACE LOCATIONS HERE:
	VECTOR raceLocs[NUM_OFFROAD_RACES]
	raceLocs[OFFROAD_RACE_CANYON_CLIFFS] = OFFROAD_RACE_GET_LOC_FOR_INDEX(OFFROAD_RACE_CANYON_CLIFFS)
	raceLocs[OFFROAD_RACE_RIDGE_RUN] = OFFROAD_RACE_GET_LOC_FOR_INDEX(OFFROAD_RACE_RIDGE_RUN)
	raceLocs[OFFROAD_RACE_VALLEY_TRAIL] = OFFROAD_RACE_GET_LOC_FOR_INDEX(OFFROAD_RACE_VALLEY_TRAIL)
	raceLocs[OFFROAD_RACE_LAKESIDE_SPLASH] = OFFROAD_RACE_GET_LOC_FOR_INDEX(OFFROAD_RACE_LAKESIDE_SPLASH)
	raceLocs[OFFROAD_RACE_ECO_FRIENDLY] = OFFROAD_RACE_GET_LOC_FOR_INDEX(OFFROAD_RACE_ECO_FRIENDLY)
	raceLocs[OFFROAD_RACE_MINEWARD_SPIRAL] = OFFROAD_RACE_GET_LOC_FOR_INDEX(OFFROAD_RACE_MINEWARD_SPIRAL)
	raceLocs[OFFROAD_RACE13] = OFFROAD_RACE_GET_LOC_FOR_INDEX(OFFROAD_RACE13)
	
	// We assume first = closest. We'll figure it out later.
	FLOAT fClosestDist = VDIST2(vCurLoc, raceLocs[0])
	
	INT index = 0
	FLOAT fDist = 0.0
	FOR index = 1 TO (ENUM_TO_INT(NUM_OFFROAD_RACES) - 1)
		fDist = VDIST2(vCurLoc, raceLocs[index])
		IF (fDist < fClosestDist)
			// Found a closer one!
			eRetInd = INT_TO_ENUM(OFFROAD_RACE_INDEX, index)
			fClosestDist = fDist
		ENDIF
	ENDFOR
	
	// Return the closest one we found.
	vRaceStartLoc = raceLocs[eRetInd]
	RETURN eRetInd
ENDFUNC

/// PURPOSE:
///    Given the race index, returns the bit flag for "won race" and the static blip for this race.
PROC GET_RACE_BITFLAG_AND_STATIC_BLIP(OFFROAD_RACE_INDEX eRaceIndex, OFFROADRACE_BIT_FLAGS & eWonFlag, STATIC_BLIP_NAME_ENUM & eStaticBlipEnum)
	IF (eRaceIndex = OFFROAD_RACE_CANYON_CLIFFS)
		eWonFlag = OFFROAD_F_Race5Won
		eStaticBlipEnum = STATIC_BLIP_MINIGAME_OFFROAD_RACE5
		
	ELIF (eRaceIndex = OFFROAD_RACE_RIDGE_RUN)
		eWonFlag = OFFROAD_F_Race8Won
		eStaticBlipEnum = STATIC_BLIP_MINIGAME_OFFROAD_RACE8
	
	ELIF (eRaceIndex = OFFROAD_RACE_VALLEY_TRAIL)
		eWonFlag = OFFROAD_F_Race9Won
		eStaticBlipEnum = STATIC_BLIP_MINIGAME_OFFROAD_RACE9
		
	ELIF (eRaceIndex = OFFROAD_RACE_LAKESIDE_SPLASH)
		eWonFlag = OFFROAD_F_Race10Won
		eStaticBlipEnum = STATIC_BLIP_MINIGAME_OFFROAD_RACE10
			
	ELIF (eRaceIndex = OFFROAD_RACE_ECO_FRIENDLY)
		eWonFlag = OFFROAD_F_Race11Won
		eStaticBlipEnum = STATIC_BLIP_MINIGAME_OFFROAD_RACE11
		
	ELIF (eRaceIndex = OFFROAD_RACE_MINEWARD_SPIRAL)
		eWonFlag = OFFROAD_F_Race12Won
		eStaticBlipEnum = STATIC_BLIP_MINIGAME_OFFROAD_RACE12
	ENDIF
ENDPROC
