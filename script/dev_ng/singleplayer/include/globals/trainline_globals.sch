//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   trainline_globals.sch                                  		//
//      AUTHOR          :   Rob Bray                                                    //
//      DESCRIPTION     :   Globals to turn on/off trainlines							//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

BOOL g_bTrainLinesChangedFlag
INT g_iTrainLineActiveBitset
