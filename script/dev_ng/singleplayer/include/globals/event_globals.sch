//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	event_globals.sch											//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Globals used to store temp event info.						//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

CONST_INT MAX_EVENTS_STORED_PER_TYPE 10

//////////////////////////////////////////////////////////////////////////////////////////
///    Struct for the global non-saved data
STRUCT EventDataStruct
	
	// Vehicle events
	INT iVehicleDamagedEvents
	ENTITY_INDEX eidVehicleDamaged[MAX_EVENTS_STORED_PER_TYPE]
	INT iVehicleDestroyedEvents
	ENTITY_INDEX eidVehicleDestroyed[MAX_EVENTS_STORED_PER_TYPE]
	
	// Ped events
	INT iPedInjuredEvents
	ENTITY_INDEX eidPedInjured[MAX_EVENTS_STORED_PER_TYPE]
	INT iPedKilledEvents
	ENTITY_INDEX eidPedKilled[MAX_EVENTS_STORED_PER_TYPE]
ENDSTRUCT
EventDataStruct g_sEventData

