//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	shop_globals.sch											//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Globals used to store shop states. 							//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

// Global saved flags
CONST_INT SHOP_S_FLAG_SHOP_AVAILABLE 						0
CONST_INT SHOP_S_FLAG_SHOP_AVAILABLE_WHEN_OUT_OF_RANGE		1
CONST_INT SHOP_S_FLAG_PLAYER_VISITED_SHOP 					2
CONST_INT SHOP_S_FLAG_PLAYER_PURCHASED_ITEM_IN_SHOP 		3
CONST_INT SHOP_S_FLAG_SHOP_RUN_ENTRY_INTRO 					4
CONST_INT SHOP_S_FLAG_MICHAEL_STOPPED_SHOP_ROBBERY 			5 // Used for ambient event
CONST_INT SHOP_S_FLAG_TREVOR_STOPPED_SHOP_ROBBERY 			6 // Used for ambient event
CONST_INT SHOP_S_FLAG_FRANKLIN_STOPPED_SHOP_ROBBERY 		7 // Used for ambient event
CONST_INT SHOP_S_FLAG_SHOP_BLIP_LONG_RANGE 					8
CONST_INT SHOP_S_FLAG_PLAYER_BROWSED_ITEMS_IN_SHOP			9
CONST_INT SHOP_S_FLAG_FLASH_BLIP_WHEN_ACTIVATED				10
CONST_INT SHOP_S_FLAG_PLAYER_SEEN_OUTFIT_HELP				11
CONST_INT SHOP_S_FLAG_PLAYER_SEEN_OPTED_BEAST_HELP			12


// Global runtime flags
CONST_INT SHOP_NS_FLAG_SHOP_OPEN 	 					0
CONST_INT SHOP_NS_FLAG_PLAYER_KICKING_OFF 				1
CONST_INT SHOP_NS_FLAG_SHOP_TEMPORARILY_UNAVAILABLE 	2
CONST_INT SHOP_NS_FLAG_PLAYER_ALLOWED_TO_SHOOT_IN_SHOP 	3
CONST_INT SHOP_NS_FLAG_SHOP_BEING_ROBBED 				4
CONST_INT SHOP_NS_FLAG_PLAYER_IN_SHOP 					5
CONST_INT SHOP_NS_FLAG_PLAYER_BROWSING_ITEMS_IN_SHOP 	6
CONST_INT SHOP_NS_FLAG_CAN_ROBBERY_TAKE_PLACE_IN_SHOP 	7
CONST_INT SHOP_NS_FLAG_SHOP_LOCATES_BLOCKED				8
CONST_INT SHOP_NS_FLAG_FORCE_CLEANUP					9
CONST_INT SHOP_NS_FLAG_FORCE_RESET						10
CONST_INT SHOP_NS_FLAG_FORCE_RESET_PROCESSING			11
CONST_INT SHOP_NS_FLAG_CLOSED_FOR_MISSION				12
CONST_INT SHOP_NS_FLAG_CLOSED_FOR_MINIGAME				13
CONST_INT SHOP_NS_FLAG_SHOP_DIALOGUE_BLOCKED			14
CONST_INT SHOP_NS_FLAG_PLAYER_ATTACKED_IN_SHOP			15
CONST_INT SHOP_NS_FLAG_SHOP_SCRIPT_RUNNING 				16
CONST_INT SHOP_NS_FLAG_CLEAR_SHOP_LAUNCHED_FLAG			17
CONST_INT SHOP_NS_FLAG_IGNORE_KICK_OFF_CHECKS			18
CONST_INT SHOP_NS_FLAG_FORCE_RESET_LINKED				19
CONST_INT SHOP_NS_FLAG_SHOP_INTRO_RUNNING				20
CONST_INT SHOP_NS_FLAG_PROCESSING_BROWSE_TRANSITION		21
CONST_INT SHOP_NS_FLAG_PLAYER_TRYING_TO_ENTER_MOD_SHOP	22
CONST_INT SHOP_NS_FLAG_PLAYER_FADED_OUT					23
CONST_INT SHOP_NS_FLAG_SHOP_OUTRO_RUNNING				24
CONST_INT SHOP_NS_FLAG_PLAYER_REMOTE_BROWSING			25
CONST_INT SHOP_NS_FLAG_SHOULD_LOCK_DOORS				26
CONST_INT SHOP_NS_FLAG_DOORS_LOCKED						27
CONST_INT SHOP_NS_FLAG_DOORS_UNLOCKED					28
CONST_INT SHOP_NS_FLAG_VIEWING_BENNY_CUTSCENE			29
CONST_INT SHOP_NS_FLAG_DISABLE_INPUT_BLOCKS				30
CONST_INT SHOP_NS_FLAG_FLASH_BLIP						31

CONST_INT SHOP_NS_FLAG_ARMORY_DIALOGUE_ARMORY_GREET		0
CONST_INT SHOP_NS_FLAG_BUNKER_DIALOGUE_MOD_SHOP_GREET	1
CONST_INT SHOP_NS_FLAG_TRUCK_DIALOGUE_MOD_SHOP_GREET	2
CONST_INT SHOP_NS_FLAG_TRUCK_DIALOGUE_GREET				3
CONST_INT SHOP_NS_FLAG_TRUCK_DIALOGUE_BYE				4
CONST_INT SHOP_NS_FLAG_TRUCK_DIALOGUE_NEW_MOD			5
CONST_INT SHOP_NS_FLAG_ARMORY_DIALOGUE_ARMORY_QUEST		6
CONST_INT SHOP_NS_FLAG_BUNKER_DIALOGUE_GREET			7
CONST_INT SHOP_NS_FLAG_BUNKER_DIALOGUE_BYE				8
CONST_INT SHOP_NS_FLAG_BUNKER_DIALOGUE_NEW_MOD			9
CONST_INT SHOP_NS_FLAG_TRUCK_DIALOGUE_CARMOD			10
CONST_INT SHOP_NS_FLAG_TRUCK_DIALOGUE_WEPMOD			11
CONST_INT SHOP_NS_FLAG_BUNKER_DIALOGUE_MOD_SHOP_QUEST	12

enumSubtitlesState g_eShopSubtitleState = DO_NOT_DISPLAY_SUBTITLES

CONST_INT SHOP_ACTIVATION_RANGE 120
CONST_INT SHOP_ACTIVATION_RANGE_LONG 140

CONST_INT VEHICLE_COUNT_FOR_STAR_ICON 306

#IF IS_DEBUG_BUILD
	BOOL g_bDisplayShopDebug
	BOOL g_bUseDebugCarmodCamScale
	VECTOR vCarmodCamScale
	BOOL g_bForceBailMissionCarMod
	BOOL g_bForceStartMissionCarMod
	BOOL g_bForceReadyCarModForMissionLaunch
#ENDIF

CONST_INT NUMBER_OF_SHOPS 58


ENUM SHOP_NAME_ENUM

	// Invalid shop
	EMPTY_SHOP = -1,				// Used to represent an invalid shop
	
	// Hairdo shops - 7
	HAIRDO_SHOP_01_BH = 0,			// Salon - Rockford Hills
	HAIRDO_SHOP_02_SC, 				// Barbers - South Los Santos
	HAIRDO_SHOP_03_V, 				// Barbers - Vespucci
	HAIRDO_SHOP_04_SS, 				// Barbers - Sandy Shores
	HAIRDO_SHOP_05_MP, 				// Barbers - Mirror Park
	HAIRDO_SHOP_06_HW, 				// Barbers - Vinewood
	HAIRDO_SHOP_07_PB, 				// Barbers - Paleto Bay
	
	// Clothes shops - 14
	CLOTHES_SHOP_L_01_SC,		 	// Clothes Low - South Los Santos
	CLOTHES_SHOP_L_02_GS,		 	// Clothes Low - Grapeseed
	CLOTHES_SHOP_L_03_DT,		 	// Clothes Low - Downtown
	CLOTHES_SHOP_L_04_CS,		 	// Clothes Low - Countryside
	CLOTHES_SHOP_L_05_GSD,		 	// Clothes Low - Grande Senora Desert
	CLOTHES_SHOP_L_06_VC,		 	// Clothes Low - Vespucci Canals
	CLOTHES_SHOP_L_07_PB,		 	// Clothes Low - Paleto Bay
	
	CLOTHES_SHOP_M_01_SM, 			// Clothes Mid - Del Perro
	CLOTHES_SHOP_M_03_H, 			// Clothes Mid - Harmony
	CLOTHES_SHOP_M_04_HW, 			// Clothes Mid - Vinewood
	CLOTHES_SHOP_M_05_GOH, 			// Clothes Mid - Great Ocean Highway
	
	CLOTHES_SHOP_H_01_BH, 			// Clothes High - Rockford Hills
	CLOTHES_SHOP_H_02_B, 			// Clothes High - Burton
	CLOTHES_SHOP_H_03_MW, 			// Clothes High - Morningwood
	
	CLOTHES_SHOP_A_01_VB, 			// Clothes Ambient - Vespucci Movie Masks
	
	// Tattoos shops - 6
	TATTOO_PARLOUR_01_HW, 			// Tattoo - Vinewood
	TATTOO_PARLOUR_02_SS, 			// Tattoo - Sandy Shores
	TATTOO_PARLOUR_03_PB, 			// Tattoo - Paleto Bay
	TATTOO_PARLOUR_04_VC, 			// Tattoo - Vespucci Canals
	TATTOO_PARLOUR_05_ELS, 			// Tattoo - East Los Santos
	TATTOO_PARLOUR_06_GOH, 			// Tattoo - Great Ocean Highway
	
	// Gun shops - 10
	GUN_SHOP_01_DT,	 				// Weapons - Downtown
	GUN_SHOP_02_SS,	 				// Weapons - Sandy Shores
	GUN_SHOP_03_HW,	 				// Weapons - Vinewood
	GUN_SHOP_04_ELS, 				// Weapons - East Los Santos
	GUN_SHOP_05_PB,	 				// Weapons - Paleto Bay
	GUN_SHOP_06_LS,	 				// Weapons - Little Seoul
	GUN_SHOP_07_MW,	 				// Weapons - Morningwood
	GUN_SHOP_08_CS,	 				// Weapons - Countryside
	GUN_SHOP_09_GOH, 				// Weapons - Great Ocean Highway
	GUN_SHOP_10_VWH, 				// Weapons - Vinewood Hills
	GUN_SHOP_11_ID1, 				// Weapons - Cypress Flats
									   
	// Car mod shops - 5
	CARMOD_SHOP_01_AP, 				// Car Mod - AMB1 (v_carmod)
	CARMOD_SHOP_05_ID2,				// Car Mod - AMB2 (v_lockup)
	CARMOD_SHOP_06_BT1,				// Car Mod - AMB3 (v_carmod)
	CARMOD_SHOP_07_CS1,				// Car Mod - AMB4 (v_carmod3)
	CARMOD_SHOP_08_CS6				// Car Mod - AMB5 (v_carmod3)
	
	,CARMOD_SHOP_SUPERMOD			// Car Mod - AMB6 (lr_supermod_int)
	,CARMOD_SHOP_PERSONALMOD		// property carmod shops
	,GUN_SHOP_ARMORY				// Weapons - Gunrunning DLC Armory
	,GUN_SHOP_ARMORY_AVENGER  		// Weapons - Gang Ops Avenger DLC Armory
	,GUN_SHOP_ARMORY_HACKER_TRUCK  	// Weapons - Business Battle Hacker Truck DLC Armory
	,GUN_SHOP_ARMORY_ARENA  		// Weapons - Arena Garage DLC Armory
	,HAIRDO_SHOP_CASINO_APT 		// Barbers - Casino Penthouse DLC personal hair and makeup station
	,CLOTHES_SHOP_CASINO 			// Clothes - Casino
	,GUN_SHOP_ARMORY_ARCADE 		// Weapons - Arcade DLC Armory
	,GUN_SHOP_ARMORY_SUB			// Weapons - Sub DLC Armory
	
	#IF FEATURE_TUNER
	,TATTOO_PARLOUR_07_CCT			// Tattoo - Car Club
	,CLOTHES_SHOP_CAR_MEET 			// Clothes - Car meet
	#ENDIF
	
	#IF FEATURE_FIXER
	,GUN_SHOP_ARMORY_FIXER			// Weapons - Fixer DLC Armory
	,CLOTHES_SHOP_MUSIC_STUDIO 		// Clothes - Music Studio
	#ENDIF
	
ENDENUM

ENUM SHOP_TYPE_ENUM
	SHOP_TYPE_HAIRDO = 0,
	SHOP_TYPE_CLOTHES,
	SHOP_TYPE_TATTOO,
	SHOP_TYPE_GUN,
	SHOP_TYPE_CARMOD,
	SHOP_TYPE_PERSONAL_CARMOD,
	SHOP_TYPE_EMPTY
ENDENUM

CONST_INT NUMBER_OF_SHOP_EVENTS 4
CONST_INT NUMBER_OF_SHOPS_WITH_EVENTS 7
ENUM SHOP_EVENT_ENUM
	SHOP_EVENT_INVALID = 0,
	
	SHOP_EVENT_MECH_IDLE,						// Mechanic carries out idle duties
	SHOP_EVENT_MECH_GREET_FRANKLIN,				// Mechanic welcomes the player (SP)
	SHOP_EVENT_MECH_GOLD_PREP_HEAVY				// Mechanic says heavy load line for gold prep (SP)
ENDENUM

CONST_INT NUMBER_OF_SHOP_EVENT_STATES 5
ENUM SHOP_EVENT_STATE_ENUM
	SHOP_EVENT_STATE_INVALID = 0,
	
	SHOP_EVENT_STATE_WAITING_TO_START,
	SHOP_EVENT_STATE_PERFORMING,
	SHOP_EVENT_STATE_FINISHED,
	SHOP_EVENT_STATE_FAILED
ENDENUM

CONST_INT NUMBER_OF_CAR_MODS 104
ENUM CARMOD_MENU_ENUM
	
/*0*/	CMM_MAIN, 			// Select mod or buy

/*1*/	CMM_BUY,			// Select buy options

/*2*/	CMM_MOD,			// Select mod options
/*3*/		CMM_MAINTAIN,
/*4*/		CMM_ARMOUR,
/*5*/		CMM_BRAKES,
/*6*/		CMM_BODYWORK,
/*7*/		CMM_BULLBARS,
/*8*/		CMM_BUMPERS,
/*9*/		CMM_CHASSIS,
/*10*/		CMM_ENGINE,
/*11*/		CMM_ENGINEBAY,
/*12*/		CMM_EXHAUST,
/*13*/		CMM_EXPLOSIVES,
/*14*/		CMM_FAIRING,
/*15*/		CMM_FENDERS,
/*16*/		CMM_FRAME,
/*17*/		CMM_FRONTFORKS,
/*18*/		CMM_FRONTMUDGUARD,
/*19*/		CMM_FRONTSEAT,
/*20*/		CMM_FUELTANK,
/*21*/		CMM_GOLD_PREP,
/*22*/		CMM_GOLD_PREP2,
/*23*/		CMM_GRILL,
/*24*/		CMM_HANDLEBARS,
/*25*/		CMM_HEADLIGHTS,
/*26*/		CMM_HOOD,
/*27*/		CMM_HORN,
/*28*/		CMM_LIGHTS,
/*29*/		CMM_LIGHTS_HEAD,
/*30*/		CMM_LIGHTS_NEON,
/*31*/		CMM_INSURANCE, // Now called LOSS/THEFT PREVENTION
/*32*/		CMM_MIRRORS,
/*33*/		CMM_PLATES,
/*34*/		CMM_PLATEHOLDER,
/*35*/		CMM_PUSHBAR,
/*36*/		CMM_REARMUDGUARD,
/*37*/		CMM_REARSEAT,
/*38*/		CMM_PAINT_JOB, // Now called RESPRAY
/*39*/		CMM_ROLLCAGE,
/*40*/		CMM_ROOF,
/*41*/		CMM_SADDLEBAGS,
/*42*/		CMM_SELL,
/*43*/		CMM_SIDESTEP,					
/*44*/		CMM_SKIRTS,
/*45*/		CMM_SPAREWHEEL,
/*46*/		CMM_SPOILER,
/*47*/		CMM_SUSPENSION,
/*48*/		CMM_TAILGATE,
/*49*/		CMM_TRACKER,
/*50*/		CMM_TRANSMISSION,
/*51*/		CMM_TRUCKBED,
/*52*/		CMM_TRUNK,
/*53*/		CMM_TURBO,
/*54*/		CMM_VIN,
/*55*/		CMM_WHEELS_MAIN,
/*56*/		CMM_WHEELS,
/*57*/		CMM_WHEEL_COL,
/*58*/		CMM_WHEEL_ACCS,
/*59*/		CMM_WHEELIEBAR,
/*60*/		CMM_WINDOWS

/*61*/		,CMM_SUPERMOD
		
/*62*/		,CMM_SUPERMOD_PLTHOLDER
/*63*/		,CMM_SUPERMOD_PLTVANITY
/*64*/		,CMM_SUPERMOD_INTERIOR1
/*65*/		,CMM_SUPERMOD_INTERIOR2
/*66*/		,CMM_SUPERMOD_INTERIOR3
/*67*/		,CMM_SUPERMOD_INTERIOR4
/*68*/		,CMM_SUPERMOD_INTERIOR5
/*69*/		,CMM_SUPERMOD_SEATS
/*70*/		,CMM_SUPERMOD_STEERING
/*71*/		,CMM_SUPERMOD_KNOB
/*72*/		,CMM_SUPERMOD_PLAQUE
/*73*/		,CMM_SUPERMOD_ICE
/*74*/		,CMM_SUPERMOD_TRUNK
/*75*/		,CMM_SUPERMOD_HYDRO
/*76*/		,CMM_SUPERMOD_ENGINEBAY1
/*77*/		,CMM_SUPERMOD_ENGINEBAY2
/*78*/		,CMM_SUPERMOD_ENGINEBAY3
/*79*/		,CMM_SUPERMOD_CHASSIS2
/*80*/		,CMM_SUPERMOD_CHASSIS3
/*81*/		,CMM_SUPERMOD_CHASSIS4
/*82*/		,CMM_SUPERMOD_CHASSIS5
/*83*/		,CMM_SUPERMOD_DOOR_L
/*84*/		,CMM_SUPERMOD_DOOR_R
/*85*/		,CMM_SUPERMOD_LIVERY
	
/*86*/		,CMM_CHASSIS_GROUP
/*87*/		,CMM_INTERIOR_GROUP
/*88*/		,CMM_PLATE_GROUP
/*89*/		,CMM_ENGINE_GROUP

/*90*/		,CMM_DIALS
/*91*/		,CMM_TRIM
/*92*/		,CMM_LIGHT_COLOUR
/*93*/		,CMM_DEFLECTOR
/*94*/		,CMM_ORNAMENTS
		
/*95*/		,CMM_FENDERSTWO
/*96*/		,CMM_WAITING_ON_PLAYERS
/*97*/		,CMM_PRIMARY_COLOUR 
/*98*/		,CMM_SECONDARY_COLOUR
/*99*/		,CMM_BUMPER_GROUP
/*100*/		,CMM_BUMPER_SUBMENU_GROUP
/*101*/		,CMM_HOOD_GROUP
/*102*/		,CMM_ROOF_GROUP
/*103*/		,CMM_SUPERMOD_TWO
		
ENDENUM

ENUM CARMOD_HORN_ENUM
	HORN_STOCK = 0,
	
	HORN_INDEP_1,
	HORN_INDEP_2,
	HORN_INDEP_3,
	HORN_INDEP_4,
	
	HORN_HIPSTER_1,
	HORN_HIPSTER_2,
	HORN_HIPSTER_3,
	HORN_HIPSTER_4,
	
	HORN_BUSINESS2_SCALE_C0,
	HORN_BUSINESS2_SCALE_D0,
	HORN_BUSINESS2_SCALE_E0,
	HORN_BUSINESS2_SCALE_F0,
	HORN_BUSINESS2_SCALE_G0,
	HORN_BUSINESS2_SCALE_A0,
	HORN_BUSINESS2_SCALE_B0,
	HORN_BUSINESS2_SCALE_C1,
	
	HORN_BUSINESS1_1,
	HORN_BUSINESS1_2,
	HORN_BUSINESS1_3,
	HORN_BUSINESS1_4,
	HORN_BUSINESS1_5,
	HORN_BUSINESS1_6,
	HORN_BUSINESS1_7,
	
	HORN_LUXE_1_FULL,
	HORN_LUXE_2_FULL,
	HORN_LUXE_3_FULL,
	HORN_LUXE_1_PREVIEW,
	HORN_LUXE_2_PREVIEW,
	HORN_LUXE_3_PREVIEW,
	
	HORN_LOWRIDER_1_FULL,
	HORN_LOWRIDER_2_FULL,
	HORN_LOWRIDER_1_PREVIEW,
	HORN_LOWRIDER_2_PREVIEW,
	
	HORN_HALLOWEEN_1_FULL,
	HORN_HALLOWEEN_2_FULL,
	HORN_HALLOWEEN_1_PREVIEW,
	HORN_HALLOWEEN_2_PREVIEW,
	
	HORN_XM15_HORN_1_FULL,
	HORN_XM15_HORN_2_FULL,
	HORN_XM15_HORN_3_FULL,
	HORN_XM15_HORN_1_PREVIEW,
	HORN_XM15_HORN_2_PREVIEW,
	HORN_XM15_HORN_3_PREVIEW,
	
	HORN_TRUCK,
	HORN_COP,
	HORN_CLOWN,
	HORN_MUSICAL_1,
	HORN_MUSICAL_2,
	HORN_MUSICAL_3,
	HORN_MUSICAL_4,
	HORN_MUSICAL_5,
	HORN_SAD_TROMBONE,
	
	HORN_AIRHORN_01,
	HORN_AIRHORN_02,
	HORN_AIRHORN_03,
	HORN_AIRHORN_01_PREVIEW,
	HORN_AIRHORN_02_PREVIEW,
	HORN_AIRHORN_03_PREVIEW
	
ENDENUM

ENUM CARMOD_MOD_TYPE_ENUM
/*0*/	CMT_REPAIR = 0,
/*1*/	CMT_ARMOUR,
/*2*/	CMT_BRAKES,
/*3*/	CMT_BUMPERS,
/*4*/	CMT_CHASSIS,
/*5*/	CMT_ENGINE,
/*6*/	CMT_EXHAUST,
/*7*/	CMT_FENDERS,
/*8*/	CMT_GRILL,
/*9*/	CMT_HOOD,
/*10*/	CMT_HORN,
/*11*/	CMT_HEADLIGHTS,
/*12*/	CMT_ROOF,
/*13*/	CMT_SKIRTS,
/*14*/	CMT_SPOILER,
/*15*/	CMT_SUSPENSION,
/*16*/	CMT_TRANSMISSION,
/*17*/	CMT_TURBO,
/*18*/	CMT_TYRE_SMOKE,
/*19*/	CMT_WHEELS,

		// Non standard mods
/*20*/	CMT_EXPLOSIVES,
/*21*/	CMT_NEONLIGHTS,
/*22*/	CMT_INSURANCE,
/*23*/	CMT_PLATES,
/*24*/	CMT_RESPRAY_PRIMARY,
/*25*/	CMT_RESPRAY_SECONDARY,
/*26*/	CMT_RESPRAY_LIVERY,
/*27*/	CMT_CREW_EMBLEM,
/*28*/	CMT_TRACKER,
/*29*/	CMT_WHEEL_COLOUR,
/*30*/	CMT_TYRES_BULLETPROOF,
/*31*/	CMT_TYRES_STOCK,
/*32*/	CMT_TYRES_CUSTOM,
/*33*/	CMT_WINDOW_TINTS,

		// Splits
/*34*/	CMT_BUMPERS_F,
/*35*/	CMT_BUMPERS_R,
	
/*36*/	CMT_FENDERS_L,
/*37*/	CMT_FENDERS_R,
	
/*38*/	CMT_RESPRAY_TERTIARY,
/*39*/	CMT_RESPRAY_LIVERY2,
/*40*/	CMT_UPGRADE,
	
/*41*/	CMT_SUPERMOD_0,			//MOD_PLTHOLDER		
/*42*/	CMT_SUPERMOD_1,			//MOD_PLTVANITY		
/*43*/	CMT_SUPERMOD_2,			//MOD_INTERIOR1		
/*44*/	CMT_SUPERMOD_3,			//MOD_INTERIOR2		
/*45*/	CMT_SUPERMOD_4,			//MOD_INTERIOR3		
/*46*/	CMT_SUPERMOD_5,			//MOD_INTERIOR4		
/*47*/	CMT_SUPERMOD_6,			//MOD_INTERIOR5		
/*48*/	CMT_SUPERMOD_7,			//MOD_SEATS			
/*49*/	CMT_SUPERMOD_8,			//MOD_STEERING		
/*50*/	CMT_SUPERMOD_9,			//MOD_KNOB			
/*51*/	CMT_SUPERMOD_10,		//MOD_PLAQUE			
/*52*/	CMT_SUPERMOD_11,		//MOD_ICE			
/*53*/	CMT_SUPERMOD_12,		//MOD_TRUNK			
/*54*/	CMT_SUPERMOD_13,		//MOD_HYDRO			
/*55*/	CMT_SUPERMOD_14,		//MOD_ENGINEBAY1		
/*56*/	CMT_SUPERMOD_15,		//MOD_ENGINEBAY2		
/*57*/	CMT_SUPERMOD_16,		//MOD_ENGINEBAY3		
/*58*/	CMT_SUPERMOD_17,		//MOD_CHASSIS2		
/*59*/	CMT_SUPERMOD_18,		//MOD_CHASSIS3		
/*60*/	CMT_SUPERMOD_19,		//MOD_CHASSIS4		
/*61*/	CMT_SUPERMOD_20,		//MOD_CHASSIS5		
/*62*/	CMT_SUPERMOD_21,		//MOD_DOOR_L			
/*63*/	CMT_SUPERMOD_22,		//MOD_DOOR_R			
/*64*/	CMT_SUPERMOD_23,		//MOD_LIVERY			
	
/*65*/	CMT_RESPRAY_DIALS,

/*66*/	CMT_TYRES_CUSTOM_MOD,
	
	CMT_DUMMY
ENDENUM

ENUM SUPERMOD_TYPE 
	SUPERMOD_TYPE_BENNY,
	SUPERMOD_TYPE_WEAPONIZED,
	SUPERMOD_TYPE_ARENA_WAR,
	SUPERMOD_TYPE_TOTAL
ENDENUM

ENUM SHOP_HAIR_CONVERSATION_STATE
	TALKED_TO_LOW_END,
	HAIR_CUT_BY_LOW_END,
	TALKED_TO_BY_HIGH_END,
	HAIR_CUT_BY_HIGH_END
ENDENUM

//////////////////////////////////////////////////////////////////////////////////////////
///    Struct for the vehicle repair
STRUCT VehicleService
	BOOL bProcessing
	BOOL bReadyForCollection
	BOOL bMessageSent
	INT iHoursToComplete
	INT iType
ENDSTRUCT

//////////////////////////////////////////////////////////////////////////////////////////
///    Struct for the runtime shop settings
STRUCT ShopDataRuntime
	WEAPON_TYPE eDefaultGunshopWeapon
	WEAPON_TYPE eSelectedGunshopWeapon
	INT iBlockedWeapons[2]
	
	BOOL bDisplayingShopHelp
	INT iShopDisplayingHelp
	INT iShopHelpFrameDelay
	
	BOOL bHideAllShopBlips
	BOOL bHideShopBlipsByType[6]
	
	BOOL bControllerRunning
	BOOL bFirstPassComplete
	
	BOOL bShopScriptLaunched[NUMBER_OF_SHOPS]
	BOOL bShopScriptLaunchedInMP[NUMBER_OF_SHOPS]
	BOOL bShopScriptRequested[NUMBER_OF_SHOPS]
	
	BOOL bUpdateShopBlips
	BOOL bUpdateShopBlip[NUMBER_OF_SHOPS]
	BLIP_INDEX blipID[NUMBER_OF_SHOPS]
	
	FLOAT fDistToShop[NUMBER_OF_SHOPS]
	
	INTERIOR_INSTANCE_INDEX playerInterior
	INT playerRoom
	
	INT iCount_PlayerInShop
	INT iCount_BrowsingInShop
	
	SHOP_EVENT_STATE_ENUM eEventState[NUMBER_OF_SHOPS_WITH_EVENTS][NUMBER_OF_SHOP_EVENTS]
	SHOP_EVENT_ENUM eCurrentEvent[NUMBER_OF_SHOPS]
	
	SHOP_NAME_ENUM eLastShopToVisit
	
	PED_COMP_NAME_ENUM eLastPreviewItem[NUM_OF_PLAYABLE_PEDS]
	PED_COMP_TYPE_ENUM eLastPreviewType[NUM_OF_PLAYABLE_PEDS]
	
	CARMOD_MENU_ENUM eCurrentCarmodShopMenu
	BOOL bTriggerCarmodIntroNow
	
	BOOL bDevTest
	
	TEXT_LABEL_15 tlMenuItem_label[MAX_MENU_ROWS]
	INT iMenuItem_cost[MAX_MENU_ROWS]
	INT iMenuItem_catalogurKey[MAX_MENU_ROWS]
	BOOL bMenuItem_freeForTutorial[MAX_MENU_ROWS]
	
	#IF IS_DEBUG_BUILD
	INT iMenuItem_priceVariation[MAX_MENU_ROWS]
	#ENDIF
	
	TEXT_LABEL_15 tlCurrentItem_label
	INT iCurrentItem_cost
	INT iCurrentItem_hash
	INT iCurrentItem_count
	INT iCurrentItem_colour
	BOOL bCurrentItem_onsale
	
	INT iProperties[NUMBER_OF_SHOPS] // Set bits: Open[0], Kicking Off[1], Temp Unavailable[2] etc.
	
	SHOP_HAIR_CONVERSATION_STATE eHairConvState //Used to determine certain conversations. 
	
	//Store the info about the players helmet. 
	BOOL forceHelmet
	INT iStoredPropIndex
	INT iStoredPropTexture
	
	TIME_DATATYPE tdTimeSinceWeSoldVehicle
	BOOL bVehicleSold
	
	BOOL bProcessStoreAlert
	STORE_PURCHASE_LOCATION ePurchaseLocation
	INT iLastViewedItemHash
	INT iLastViewedItemCost
	INT iLastViewedItemShop
	
	// MP Tutorial
	BOOL bTutorialTrackerAddedToBasket
	BOOL bTutorialInsuranceAddedToBasket
	
	INT iLockDoorBitset
	INT iBunkerDialogueBitset
	
	// Mod shop cleanip
	BOOL bAssignPVBackOnCleanup
	
	INT iPurchasedMods[60]
	INT iPurchasedWheels[20]
	
	BOOL bForceRebuy
	
	BOOL bDoorsOpen_CachedForSupermod
	
	INT iModShopUpgradeBlockBitset
	
	BOOl bBlockFreePVSale
	
	BOOL bPreviewTattooMode
	
	BOOL bBlockCommerceStore
	
	BOOL bSetStoredModForResearchModPreview = TRUE
	
	TEXT_LABEL_15	tlTelemetryLabel[MAX_MENU_ROWS]
	BOOL bRunControllerInCreator = FALSE
	
	INT iGunShopLocationDiscountKey
	FLOAT fGunShopLocationDiscountValue
	
	BOOL bForceCleanupShopScriptsWhenMainUpdateDisabled = TRUE
	BOOL bDisableMainUpdatesForAllAdversaryModes = TRUE
ENDSTRUCT
ShopDataRuntime g_sShopSettings


INT g_iPersonalCarModVariation = 0

ENUM PERSONAL_CAR_MOD_VARIATION
	PERSONAL_CAR_MOD_NONE								= 0
	,PERSONAL_CAR_MOD_VARIATION_IE_SMALL 				=	1
	,PERSONAL_CAR_MOD_VARIATION_IE_MEDIUM 				=	2
	,PERSONAL_CAR_MOD_VARIATION_IE_LARGE				=	3
	,PERSONAL_CAR_MOD_VARIATION_BIKER_ONE 				=	4
	,PERSONAL_CAR_MOD_VARIATION_BIKER_TWO 				=	5
	,PERSONAL_CAR_MOD_VARIATION_OFFICE_ONE 				=	6
	,PERSONAL_CAR_MOD_VARIATION_OFFICE_TWO 				=	7
	,PERSONAL_CAR_MOD_VARIATION_OFFICE_THREE		 	=	8
	,PERSONAL_CAR_MOD_VARIATION_OFFICE_FOUR 			=	9
	,PERSONAL_CAR_MOD_VARIATION_TRUCK 					=	10
	,PERSONAL_CAR_MOD_VARIATION_BUNKER					=	11	
	,PERSONAL_CAR_MOD_VARIATION_HANGAR					=	12	
	,PERSONAL_CAR_MOD_VARIATION_AOC						=	13	
	,PERSONAL_CAR_MOD_VARIATION_BASE 					=	14	
	,PERSONAL_CAR_MOD_VARIATION_BUSINESS_HUB			=	15
	,PERSONAL_CAR_MOD_VARIATION_HACKER_TRUCK			=	16
	,PERSONAL_CAR_MOD_VARIATION_ARENA_WARS				=	17
	,PERSONAL_CAR_MOD_VARIATION_CAR_MEET				=	18
	,PERSONAL_CAR_MOD_VARIATION_TUNER_AUTO_SHOP			=	19
	,PERSONAL_CAR_MOD_VARIATION_FIXER_HQ				=	20
	#IF FEATURE_DLC_2_2022
	,PERSONAL_CAR_MOD_VARIATION_JUGGALO_HIDEOUT			=	21
	#ENDIF
ENDENUM

//////////////////////////////////////////////////////////////////////////////////////////
///    Struct that gets passed in to shop scripts when launched
STRUCT SHOP_LAUNCHER_STRUCT
	SHOP_NAME_ENUM eShop
	BOOL bLinkedShop
	PERSONAL_CAR_MOD_VARIATION ePersonalCarModVariation
	INT iNetInstanceID
ENDSTRUCT

//////////////////////////////////////////////////////////////////////////////////////////
///    Struct for the global saved data
STRUCT ShopDataSaved
	INT iProperties[NUMBER_OF_SHOPS] // Set bits: Available[0], Visited[1], Purchased Item[2] etc.
	BOOL bDefaultDataSet
	
	INT iShopTypesVisited // Set bit for each type
	INT iHairdoShopVisits
	INT iClothesShopVisits
	INT iTattooShopVisits
	INT iCarmodShopVisits
	INT iGunShopVisits
	
	INT iGunShopPostTrev2Dialogue
	INT iGunShopPostLamar1Dialogue
	INT iClothesShopPostLester1Dialogue
	
	INT iGunShopHelpCount
	
	BOOL bFirstModShopRepairComplete
	
	VehicleService sVehicleService[NUM_OF_PLAYABLE_PEDS]
	
	// Star icon for modshop.
	INT iCarmodsViewedBitset[(VEHICLE_COUNT_FOR_STAR_ICON)*((NUMBER_OF_CARMOD_UNLOCKS/32)+1)]
	
	// New content checks
	INT iContentChecks_Vehicles
	INT iContentChecks_Weapons
	INT iContentChecks_Clothes
	INT iContentChecks_Hairdos
	INT iContentChecks_Tattoos
	INT iContentChecks_Game
	
	//Help text shown flags.
	BOOL bBarberBlipHelpShown
	BOOL bArmourHelpShown
ENDSTRUCT

//For the boss events.
CONST_INT 	ciTRIGGER_EVENT_INVALID_EVENT				 -1
CONST_INT 	ciTRIGGER_EVENT_PACKAGE_DELIVERED			  1
INT 		g_iStartGangBossFlowEvent				   = -1

FLOAT g_fFORCED_CarModCamera_MaxHRadius				= -1.0
FLOAT g_fFORCED_CarModCamera_MinGndHeight			= -1.0
FLOAT g_fFORCED_CarModCamera_MaxGndHeight			= -1.0

FLOAT g_fFORCED_CarModCamera_ClampRotationAngle_Min	= -360.0
FLOAT g_fFORCED_CarModCamera_ClampRotationAngle_Max	=  360.0

BOOL g_bShopCutsceneActive = FALSE

BOOL g_bForceCleaupShopsForRecordingPlayback

CONST_INT MAX_MENU_OPTIONS 123
	
INT g_iBGMenuOptionsBlock[(MAX_MENU_OPTIONS/32) + 1]

CONST_INT PERSONAL_CAR_MOD_CLUBHOUSE_SCRIPT_INSTANCE_OFFSET 	5
CONST_INT PERSONAL_CAR_MOD_OFFICE_SCRIPT_INSTANCE_OFFSET		37
CONST_INT PERSONAL_CAR_MOD_IE_WAREHOUSE_SCRIPT_INSTANCE_OFFSET	69
CONST_INT PERSONAL_CAR_MOD_TRUCK_SCRIPT_INSTANCE_OFFSET			79

VECTOR g_vOfficeGargeSlotCoords
FLOAT g_fOfficeGarageSlotHeading
INT g_iOfficeGargeSlotBeforeMod

CARMOD_UNLOCK_ITEMS eItemUnlocks[MAX_MENU_OPTIONS]
MOD_TYPE eModSlotToUse[MAX_MENU_OPTIONS] // Increase if we get more mod slots.
INT iModColours[MAX_MENU_OPTIONS]

BOOL g_bValidatePlayersTorsoComponent = TRUE
BOOL g_bValidatePlayersHairStats = TRUE
BOOL g_bValidatedPlayerPurchasedHair = FALSE
BOOL g_bValidatePlayersHelmetBerd = TRUE
BOOL g_bValidatePlayersScubaGear = TRUE
BOOL g_bForcePlayersHelmetOn = FALSE
BOOL g_bUnoptimiseValidatePlayersHairStats = FALSE
BOOL g_bBlockScubaGear = FALSE

TEXT_LABEL_31 g_tlCustomCrewLogoTXDTexture[NUM_NETWORK_PLAYERS]
INT g_iCustomCrewLogoUsePresetBitset

	NETWORK_INDEX g_superModVeh
	VEHICLE_INDEX g_personalCarModVeh
	VEHICLE_INDEX g_viSuperModVeh
	INT g_iOfficePersonalCarModOutroFailTimer = 10000
	INT g_iOfficePerosnalCarModRemoteBrowseStage = -2
VEHICLE_INDEX g_truckPersonalCarModVeh
VEHICLE_INDEX g_personalAACarModVeh
VEHICLE_INDEX g_personalBunkerTruck
VEHICLE_INDEX g_personalBunkerTrailer

PED_COMP_NAME_ENUM g_eLastStoredCompName = DUMMY_PED_COMP
PED_COMP_TYPE_ENUM g_eLastStoredCompType
INT g_iLastStoredCompTexture
INT g_iLastStoredDecalHash
PED_VARIATION_STRUCT g_sLastStoredClothesInWardrobe

ENUM SPEC_VEH_MOD_SHOP_NAME_ENUM
	EMPTY_SPEC_VEH_MOD_SHOP = -1,	// Used to represent an invalid special vehicle mod shop
	SPEC_VEH_MOD_ARMORY_TRUCK
ENDENUM

//////////////////////////////////////////////////////////////////////////////////////////
///    Struct that gets passed in to shop scripts when launched
STRUCT SPEC_VEH_MOD_LAUNCHER_STRUCT
	SPEC_VEH_MOD_SHOP_NAME_ENUM eSpecVehModShop
ENDSTRUCT

INT g_iResearchTypeUnlocked = 0
BOOL g_bTriggerResearchUnlockTicker
TEXT_LABEL_15 g_tlResearchUnlockItem
BOOL g_bResearchTriggerHelp
BOOL g_bResearchTriggerTxt
BOOL g_bResearchTriggerFeed
TEXT_LABEL_15 g_tlResearchUnlockTxt

CONST_INT SHOP_PERSONAL_CAR_MOD_IMPOUNDED_PV_WARNING							0

CONST_INT SHOP_PERSONAL_CAR_MOD_DELIVER_AUTO_SHOP_CLIENT_VEHICLE				1
CONST_INT SHOP_PERSONAL_CAR_MOD_DELIVER_AUTO_SHOP_CLIENT_VEHICLE_BY_STAFF		2

INT g_iAutoShopDeliveryByStaffSlot = -1

INT g_iPersonalCarMod_BS

INT g_iRDRRewardStage
INT g_iRDRRewardTransactionID

STRUCT SHOP_AWARD_DATA
	MODEL_NAMES awardVehModel[27]
	BOOL bDisplayAwardHelp
	INT iLiveryIndex
	INT iLiveryBit[11]
	INT iLiveryType[3], iLiveryTypeHelp[3]
ENDSTRUCT
SHOP_AWARD_DATA g_sShopAwardData

INT g_iRDRHatchetRewardStage
INT g_iRDRHatchetRewardTransactionID

INT g_iNightclubDancingRewardType
INT g_iNightclubDancingRewardStage
INT g_iNightclubDancingRewardTransactionID

INT g_iRayPistolRewardStage
INT g_iRayPistolXmasLiveryRewardStage

CONST_INT MAX_NUM_CORONA_VEH_MOD		4

ENUM CORONA_VEH_MOD_TYPE
	CVT_LIVERY = 0,
	CVT_ARMOUR,
	CVT_SCOOP_RAMBAR,
	CVT_MINE
ENDENUM

STRUCT CORONA_VEHICLE_MOD_STRUCT
	INT			iDefaultModIndex[MAX_NUM_CORONA_VEH_MOD]
	INT 		iModIndex[MAX_NUM_CORONA_VEH_MOD]
	INT 		iModPrice[MAX_NUM_CORONA_VEH_MOD]
ENDSTRUCT
CORONA_VEHICLE_MOD_STRUCT g_sCoronaVehMod

TEXT_LABEL_63 g_tlArenaVehicleName[NUM_NETWORK_PLAYERS]
INT g_iPersonalCarmodBS
INT g_iArenaVehNameBS[NUM_NETWORK_PLAYERS]

// g_iArenaVehNameBS
CONST_INT ARENA_VEH_NAME_GLOBAL_BS_VEHICLE_NAME_READY				0
CONST_INT ARENA_VEH_NAME_GLOBAL_BS_VEHICLE_NAME_REQUESTED			1

// g_iPersonalCarmodBS
CONST_INT PERSONAL_CARMOD_GLOBAL_BS_MODDING_RC_BANDITO							0
CONST_INT PERSONAL_CARMOD_GLOBAL_BS_HAVE_DONAR_VEHICLE_IN_GARAGE				1
CONST_INT PERSONAL_CARMOD_GLOBAL_BS_HAVE_CONTENDER_VEHICLE_IN_GARAGE			2
CONST_INT PERSONAL_CARMOD_GLOBAL_BS_ARENA_MOD_UNLOCKED							3
CONST_INT PERSONAL_CARMOD_GLOBAL_BS_ARENA_SUPERMOD_ON_GROUND					4
CONST_INT PERSONAL_CARMOD_GLOBAL_BS_ALLOW_WEAPONS_IN_SHOP						5
CONST_INT PERSONAL_CARMOD_GLOBAL_BS_ALL_MODBAYS_OCCUPIED						6
CONST_INT PERSONAL_CARMOD_GLOBAL_BS_MODDING_TUNER_CLIENT_VEHICLE				7
CONST_INT VEHICLE_UNMODDABLE_IN_CAR_MEET										8
CONST_INT PERSONAL_CARMOD_GLOBAL_BS_ALLOW_MODSHOP_DURING_CONTENT_MISSION		9
#IF FEATURE_DLC_1_2022
CONST_INT PERSONAL_CARMOD_GLOBAL_BS_MODDING_BIKER_CLIENT_VEHICLE				10
#ENDIF

CONST_INT PURCHASED_CAR_MEET_MEM_STAGE_NONE 	-1
CONST_INT PURCHASED_CAR_MEET_MEM_STAGE_HELP_1 	0
CONST_INT PURCHASED_CAR_MEET_MEM_STAGE_HELP_2 	1


INT g_iJustBoughtCMMembership = PURCHASED_CAR_MEET_MEM_STAGE_NONE
BOOL g_bPurchasedCarMeetPrivateTicketThisBoot = FALSE
MODEL_NAMES g_WeakHandBrakeVeh = DUMMY_MODEL_FOR_SCRIPT
