USING "timer_globals.sch"
USING "charsheet_global_definitions.sch"
USING "respawn_globals.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	friends_globals.sch
//		AUTHOR			:	Sam Hackett
//		DESCRIPTION		:	Globals required to control friends activities.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// -------------------------------------------------------------------------------------------
//	FRIEND LOCATION
// -------------------------------------------------------------------------------------------

// Friend locations
ENUM enumFriendLocation
	FLOC_adhoc = 0,				// Sometimes friend says they will come to meet the player, this location is setup near the player's road node

	FLOC_michael_RH,			// Michael safehouse (rockford hills)
	FLOC_michael_CS,			// Michael safehouse (countryside)
	FLOC_franklin_SC,			// Franklin safehouse (South central)
	FLOC_franklin_VH,			// Franklin safehouse (Vinewood hills)
	FLOC_trevor_CS,				// Trevor safehouse (Countryside)
	FLOC_trevor_VB,				// Trevor safehouse (Venice beach)
	FLOC_trevor_SC,				// Trevor safehouse (Strip club) - dropoff only
	FLOC_trevor_SCp,			// Trevor safehouse (Strip club) - pickup only
	FLOC_lamar_SC,				// Lamar home (South central)


	FLOC_coffeeShop_RH,			// City - Rockford Hills locations
	FLOC_shoppingPlaza_RH,
	FLOC_shoppingMall_RH,
	FLOC_minimartCarpark_RH,
	
	FLOC_coffeeShop_DT,			// City - Downtown locations
//	FLOC_gasStation_DT,
	FLOC_minimartCarpark_DT,
	FLOC_artPlaza_DT,
	FLOC_bar_DT,
	FLOC_recCentre_SC,
	
	FLOC_shoppingPlaza_VB,		// City - Western locations
//	FLOC_beachFront_VB,
	FLOC_bar_VB,
	FLOC_minimartCarpark_MW,
	
	FLOC_paletoMainSt_PA,		// Rural locations
	FLOC_minimartCarpark_SS,
	
	MAX_FRIEND_LOCATIONS,
	NO_FRIEND_LOCATION
ENDENUM

// Friend location flags
CONST_INT FLF_Michael			BIT0
CONST_INT FLF_Franklin			BIT1
CONST_INT FLF_Trevor			BIT2
CONST_INT FLF_Lamar				BIT3
CONST_INT FLF_Jimmy				BIT4
CONST_INT FLF_Amanda			BIT5
CONST_INT FLF_AllChars			FLF_Michael|FLF_Franklin|FLF_Trevor|FLF_Lamar|FLF_Jimmy|FLF_Amanda

CONST_INT FLF_Pickup			BIT7
CONST_INT FLF_Dropoff			BIT8
CONST_INT FLF_IsHomeLoc			BIT9
CONST_INT FLF_PickupDropoff		FLF_Pickup|FLF_Dropoff
CONST_INT FLF_AllUsage			FLF_Pickup|FLF_Dropoff|FLF_IsHomeLoc

CONST_INT FLF_AdhocReadyForUse	BIT10

// Friend location data
STRUCT structFriendLocation

	VECTOR					vPickupCoord
	VECTOR					vPedOffsetA
	VECTOR					vPedOffsetB
	
	VECTOR					vSpawnPos
	FLOAT					fSpawnRot
	VECTOR					vParkPos
	
	SAVEHOUSE_NAME_ENUM		hSavehouse
	INT						iFlags
	
ENDSTRUCT


// -------------------------------------------------------------------------------------------
//	FRIEND CHAT
// -------------------------------------------------------------------------------------------

ENUM enumFriendChatType
	FCHAT_TypeFull = 0,
	FCHAT_TypeMini,
	FCHAT_TypeDrunk,
	
	MAX_FCHAT_TYPES
ENDENUM

ENUM enumFriendChatCondition		// I think the maximum conditions that can be added is 255, (needs checking though)
	FCHAT_ConditionNever = 0,
	FCHAT_ConditionAlways,
	FCHAT_ConditionSingleFriend,
	FCHAT_ConditionPreBreak,			// Pre family 5
	FCHAT_ConditionPostBreak,			// Post family 5
	FCHAT_ConditionPreExile,			// Pre exile 1
	FCHAT_ConditionPostExile,			// Post exile 1
	FCHAT_ConditionEarly,
	FCHAT_ConditionMid,
	FCHAT_ConditionEnd,
	FCHAT_ConditionEndAllAlive,
	FCHAT_ConditionEndMDead,
	FCHAT_ConditionEndTDead,
	FCHAT_ConditionEndMAlive,
	FCHAT_ConditionEndTAlive,
	FCHAT_ConditionBeforeTrevor,
	FCHAT_ConditionBeforeBetrayal,
	FCHAT_ConditionMichaelBetrayal,
	FCHAT_ConditionFL0,
	FCHAT_ConditionFL1,
	FCHAT_ConditionInLosSantos,
	FCHAT_ConditionPostBreakTrevorAlive,// After family 5, before finale_a
	
	FCHAT_MAX_CONDITIONS
ENDENUM

CONST_INT CONST_iFriendChatMaxBanks 6//4

STRUCT structFriendChatData
	INT banks[CONST_iFriendChatMaxBanks]		// FullCount/MiniCount/DrunkCount / FullTotal/MiniTotal/DrunkTotal / Condition
ENDSTRUCT


// -------------------------------------------------------------------------------------------
//	FRIEND CONNECTION
// -------------------------------------------------------------------------------------------

ENUM enumFriendConnection

    // Main players
	FC_MICHAEL_FRANKLIN,
    FC_FRANKLIN_TREVOR,
    FC_TREVOR_MICHAEL,
	
	// Lamar
	FC_FRANKLIN_LAMAR,
	FC_TREVOR_LAMAR,
	
	// Jimmy
	FC_MICHAEL_JIMMY,
	FC_FRANKLIN_JIMMY,
	FC_TREVOR_JIMMY,
	
	// Amanda
	FC_MICHAEL_AMANDA,
	
	MAX_FRIEND_CONNECTIONS,
    NO_FRIEND_CONNECTION
	
ENDENUM

ENUM enumFriendGroup

	FG_MICHAEL_FRANKLIN_TREVOR,
    FG_FRANKLIN_TREVOR_LAMAR,
	
	MAX_FRIEND_GROUPS,
    NO_FRIEND_GROUP
	
ENDENUM

ENUM enumFriendConnectionState
	FC_STATE_ContactWait = 0,
	
	FC_STATE_PhoneAccept,
	FC_STATE_PhoneDecline,
	FC_STATE_Init,
	FC_STATE_Active,
	
	MAX_FRIEND_STATE,
	FC_STATE_Invalid = -1
ENDENUM

ENUM enumFriendConnectionMode
	FC_MODE_Friend = 0,
	FC_MODE_Adhoc,
	FC_MODE_ReplayGroup,

	FC_MODE_Ambient,
	FC_MODE_Squad,
	
	MAX_FRIEND_INIT_MODES
ENDENUM

ENUM enumFriendConnectionFlag
	FC_FLAG_DoneCompletionPercent = 0,
	FC_FLAG_HasInitiated,
	FC_FLAG_HasCallConnected,
	FC_FLAG_IsCallAnswerphone
ENDENUM


ENUM enumFriendContactType
	FRIEND_CONTACT_PHONE = 0,
	FRIEND_CONTACT_FACE,
	FRIEND_CONTACT_DISMISSED
ENDENUM

ENUM enumFriendBlockFlag
	FRIEND_BLOCK_FLAG_MISSION = 0,
	FRIEND_BLOCK_FLAG_HIATUS,
	FRIEND_BLOCK_FLAG_CLASH
ENDENUM


STRUCT structFriendConnection
	enumFriendConnectionState	state
	enumFriendConnectionMode	mode
ENDSTRUCT

STRUCT structFriendConnectData
	enumFriend					friendA
	enumFriend					friendB
	
	INT							blockBits
	SP_MISSIONS					blockMissionID
	CC_CommID					commID
	
	structTimer					lastContactTimer
	enumFriendContactType		lastContactType
	structFriendChatData		chatData

	enumFriend					wanted
	INT							likes
	INT							flags
	
ENDSTRUCT

STRUCT structFriendGroupData
	structFriendChatData		chatData
ENDSTRUCT


STRUCT structFriendData
	structCharacterSheet		charSheet
//	enumFriendLocation			defaultPickupLoc
ENDSTRUCT


// -------------------------------------------------------------------------------------------
//	ACTIVITY LOCATION
// -------------------------------------------------------------------------------------------

ENUM enumActivityLocation	// Make sure you let me know if you add anything to this enum, thanks - (Samuel Hackett)
	ALOC_bar_bahamas,
	ALOC_bar_baybar,
	ALOC_bar_biker,
	ALOC_bar_downtown,
	ALOC_bar_himen,
	ALOC_bar_mojitos,
	ALOC_bar_singletons,
	
	ALOC_cinema_downtown,
	ALOC_cinema_morningwood,
	ALOC_cinema_vinewood,

	ALOC_darts_hickBar,
	
	ALOC_golf_countryClub,
	
	ALOC_stripclub_southCentral,
	
	ALOC_tennis_beachCourt,
	ALOC_tennis_chumashHotel,
	ALOC_tennis_LSUCourt1,
	ALOC_tennis_michaelHouse,
	ALOC_tennis_richmanHotel1,
	ALOC_tennis_vespucciHotel,
	ALOC_tennis_vinewoodhotel1,
	ALOC_tennis_weazelCourt1,
	
	ALOC_suspendFriends,
	
	MAX_ACTIVITY_LOCATIONS,
    NO_ACTIVITY_LOCATION
ENDENUM

ENUM enumActivityType
	ATYPE_golf,
	ATYPE_tennis,
	ATYPE_stripclub,
	ATYPE_darts,
	
	ATYPE_cinema,
	ATYPE_bar,
	
	ATYPE_suspend,
	
	MAX_ACTIVITY_TYPES,
	NO_ACTIVITY_TYPE
ENDENUM

ENUM enumActivityResult
	AR_playerWon,					// the player won the activity against friend/s
	AR_playerDraw,					// the player drew the activity with friend/s
	AR_playerQuit,					// the player quit the activity
	AR_buddyA_won,					// the player lost the activity to friend A
	AR_buddyB_won,					// the player lost the activity to friend B
	
	AR_buddyA_attacked,
	AR_buddyB_attacked,
	AR_buddyAll_attacked,
	
	AR_buddy_injured,				// buddy was injured while on the friend activity
	AR_deatharrest,					// player was killed/arrested while on the friend activity
	
	NO_ACTIVITY_RESULT				// used as a default
ENDENUM


STRUCT structActivityLocation
	STATIC_BLIP_NAME_ENUM	sprite
	enumActivityType		type
ENDSTRUCT

STRUCT structActivityLocationBackup
	INT						iSettings
	INT						iColour
ENDSTRUCT


// -------------------------------------------------------------------------------------------
//	BATTLE BUDDIES
// -------------------------------------------------------------------------------------------

STRUCT structBattleBuddySnapshot
	SP_MISSIONS		eMission
	INT				iAllowedChars
	INT				iReplayChars
	INT				iFailedChars
ENDSTRUCT


// -------------------------------------------------------------------------------------------
//	MISSION ZONE
// -------------------------------------------------------------------------------------------

ENUM enumFriendMissionZoneState
	FRIEND_MISSION_ZONE_OFF = 0,
	FRIEND_MISSION_ZONE_CALL,
	FRIEND_MISSION_ZONE_ON,
	FRIEND_MISSION_ZONE_REJECT,
	FRIEND_MISSION_ZONE_LAUNCHING,
	FRIEND_MISSION_ZONE_LAUNCHED
ENDENUM


// -------------------------------------------------------------------------------------------
//	AMBIENT FRIENDS
// -------------------------------------------------------------------------------------------

ENUM enumAmbChatBitIndex
	AMBCHATBIT_MFSHOWROOM = 0,
	AMBCHATBIT_MTFALLOUT,
	AMBCHATBIT_KILLEDM,
	AMBCHATBIT_KILLEDT,
	
	FRCHATBIT_TJ_DRINKING,
	FRCHATBIT_MJ_DRINKING
ENDENUM


ENUM enumAmbGrabMode
	AMBGRAB_NORMAL = 0,
	AMBGRAB_SAFEHOUSE_TRAILER,
	AMBGRAB_SAFEHOUSE_DRUNK,
	AMBGRAB_SAFEHOUSE_OUTSIDE,
	AMBGRAB_TRANSFER_REJECTED,
	AMBGRAB_TRANSFER_WANDER,
	AMBGRAB_TRANSFER_FLEE
ENDENUM

ENUM enumAmbDismissMode
	AMBMODE_WANDER = 0,
	AMBMODE_FLEE,
	AMBMODE_REJECTED,
	AMBMODE_JACKED,
	AMBMODE_FIGHT
ENDENUM


// -------------------------------------------------------------------------------------------
//	FRIEND FLAGS
// -------------------------------------------------------------------------------------------

ENUM enumFriendFlags
	FRIENDFLAG_MICHAEL_AVAILABLE = 0,
	FRIENDFLAG_FRANKLIN_AVAILABLE,
	FRIENDFLAG_TREVOR_AVAILABLE,

	FRIENDFLAG_IS_ROBBERY_UNDERWAY
ENDENUM


// -------------------------------------------------------------------------------------------
//	DEBUG VARIABLES
// -------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD

	BOOL					g_bForceFriendActivityWithAnyone

	enumFriendConnection	g_eDebugSelectedFriendConn				= NO_FRIEND_CONNECTION
	INT						g_iDebugSelectedFriendConnDisplay		= -1
	INT						g_iDebugSelectedFriendConnSelection		= -1

#ENDIF


// -------------------------------------------------------------------------------------------
//	UNSAVED CONTROLLER VARIABLES
// -------------------------------------------------------------------------------------------

structFriendLocation		g_FriendLocations[MAX_FRIEND_LOCATIONS]

enumCharacterList			g_eDefaultPlayerChar		= CHAR_MICHAEL
INT							g_iNumberOfActiveFriends	= 0


// -------------------------------------------------------------------------------------------
//	UNSAVED ACTIVITY VARIABLES
// -------------------------------------------------------------------------------------------

structActivityLocation 		g_ActivityLocations[MAX_ACTIVITY_LOCATIONS]
structActivityLocationBackup g_ActivityLocationBackups[MAX_ACTIVITY_LOCATIONS]

enumActivityLocation		g_eCurrentActivityLoc				= NO_ACTIVITY_LOCATION
enumActivityLocation		g_ePreviousActivityLoc				= g_eCurrentActivityLoc
enumActivityResult			g_ePreviousActivityResult			= NO_ACTIVITY_RESULT

BOOL						g_bAllowAmbientFriendLaunching		= TRUE
BOOL						g_bAllowAmbientFriendStalking		= TRUE
enumCharacterList			g_eRecentFriendChar					= NO_CHARACTER

PED_INDEX					g_pActivityFriendA
PED_INDEX					g_pActivityFriendB
PED_INDEX					g_pGlobalFriends[NUM_OF_NPC_FRIENDS]					// Lamar, Jimmy, Amanda
PED_INDEX					g_pDismissPeds[NUM_OF_PLAYABLE_PEDS]
enumAmbGrabMode				g_pDismissMode[NUM_OF_PLAYABLE_PEDS]

enumFriendMissionZoneState	g_eFriendMissionZoneState			= FRIEND_MISSION_ZONE_OFF
SP_MISSIONS					g_eFriendMissionZoneID				= SP_MISSION_NONE
INT							g_iFriendMissionZoneAcceptBitset	= 0

SP_MISSIONS					g_BattleBuddyMission				= SP_MISSION_NONE
INT							g_BattleBuddyAllowedChars			= 0

INT							g_bitfieldFriendFlags				= 0

INT							g_bitfieldBattleBuddyPhoneContact	= 0
INT							g_bitfieldBattleBuddyAvailable		= 0
INT							g_bitfieldBattleBuddyOverridden		= 0

INT							g_bitfieldBattleBuddyBehaviour		= 0

structFriendConnection		g_FriendConnectState[MAX_FRIEND_CONNECTIONS]

#IF IS_DEBUG_BUILD
enumFriendLocation			g_ForceFriendLocation				= NO_FRIEND_LOCATION
#ENDIF

// -------------------------------------------------------------------------------------------
//	SAVED VARIABLES
// -------------------------------------------------------------------------------------------

STRUCT g_FriendsSavedData
    structFriendData		g_FriendData[MAX_FRIENDS]
    structFriendConnectData	g_FriendConnectData[MAX_FRIEND_CONNECTIONS]
	structFriendGroupData	g_FriendGroupData[MAX_FRIEND_GROUPS]
	
	structTimer				g_FriendFailTimers[MAX_FRIENDS]
	CC_CommID				g_FriendFailMessages[MAX_FRIENDS]
	structFriendChatData	g_FranklinLamarEndChat
	
	INT						g_FriendScriptThread
	
	BOOL					g_bHelpDoneCanPhoneFriend		= FALSE		// has player received help text about being able to call friends?
	BOOL					g_bHelpDoneCanPhoneBBuddy		= FALSE		// has player received help text about being able to call battle buddies?
	BOOL					g_bHelpDoneCanPhoneDecline		= FALSE		// has player received help text about friend declining?
	BOOL					g_bHelpDonePickupDest			= FALSE		// has player received help text about picking friend up at blip?
	BOOL					g_bHelpDonePickupWait			= FALSE		// has player received help text about waiting for friend at blip?
	BOOL					g_bHelpDoneActivityBlips		= FALSE		// has player received the help text about activity blips?
	BOOL					g_bHelpDoneOpenMap				= FALSE		// has player received the help text about more activities being available on the map?
	BOOL					g_bHelpDoneDropoff				= FALSE		// has player received the help text about activity blips?
	BOOL					g_bHelpDoneCanCancel			= FALSE		// has player received a warning about cancelling an activity?
	BOOL					g_bCalledToCancelOnce			= FALSE		// has player called friend to cancel an activity?
	BOOL					g_bHasPlayerBeenTurnedDown		= FALSE		// has a friend declined when being called by player to arrange activity
	BOOL					g_bExplainedDeadFriend			= FALSE		// has player received a warning about killing friend before activity?
	BOOL					g_bHelpDoneBBuddyArrival		= FALSE		// has player received help text about battle buddy arriving
	BOOL					g_bHelpDoneBBuddySwitch			= FALSE		// has player received help text about battle buddy arriving
	INT						g_iAmbChatBitfield				= 0			// has each ambient chat played out
ENDSTRUCT

