USING "Clock_Globals.sch"
USING "flow_commands_enums_GAME.sch"
USING "charsheet_global_definitions.sch"
USING "player_ped_globals.sch"
USING "friends_globals.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	snapshot_globals.sch
//		AUTHOR			:	Andrew Minghella
//		DESCRIPTION		:	Globals for controlling the stage snapshots used 
//							in replay controller and repeat play.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

CONST_INT MAX_VEHICLE_SNAPSHOT_DIST 100 // how far away a vehicle is allowed to be when taking the vehicle snapshot

// Used in replays / repeat play + save anywhere
STRUCT PLAYER_SNAPSHOT
	VECTOR					vPos								// Where the player is.
	FLOAT					fHeading							// Player's heading.
	INT 					iWantedLevel						// Player's current wanted level
	PED_PARACHUTE_STATE 	eParachuteState						// Used to restore parachute where needed
ENDSTRUCT

// Used in replays / repeat play + save anywhere
STRUCT VEHICLE_SNAPSHOT
	VECTOR					vVehiclePos							// Position of player's current vehicle.
	VECTOR					vVehicleVelocity					// Vehicle's current velocity: used for quick save
	FLOAT					fVehicleHeading						// Player's vehicle's heading.
	BOOL					bPersonalVehicle					// Is snapshot vehicle the player's personal vehicle?
	BOOL					bInVehicle							// Was the player in a vehicle when this snapshot was stored?
	BOOL					bTrackedForImpound					// Is this vehicle being trakced for the impound
	BOOL					bVehGen								// Was this vehicle created by a VehGen
	enumCharacterList		ePlayerCharacter
	VEHICLE_SETUP_STRUCT	mVehicle							// Player's current vehicle.
ENDSTRUCT

// This struct is used in repeat play and save anywhere
// it just stores the info needed to respot the player and his vehicle after loading the save
STRUCT REPEAT_PLAY_STRUCT
	PLAYER_SNAPSHOT 		mPlayerStruct						// Player position info
	VEHICLE_SNAPSHOT		mVehicleStruct						// Player's vehicle info
	SP_MISSIONS				eMission							// Mission ID of any mission lead-in that was active on quicksave.
	g_eRC_MissionIDs		eRCMissionToBlock					// RC mission ID of nearest valid RC that has its leave area flag set
																// Used to stop the RC launching when we load a save made in the trigger area
ENDSTRUCT

// This is the main snapshot struct
// It is used by the replay controller to store / restore the game state when replaying missions
// NOTE if anything gets added to this struct or any structs contained in it
// you will need to update Reset_Snapshot + CopyStartSnapshotToStageSnapshot in snapshot_private.sch
STRUCT SNAPSHOT_STRUCT
	enumCharacterList		eCharacter							// Which character the player is.
	TIMEOFDAY				tTime								// Game time.
	WEAPON_TYPE				mCurrentWeapon						// Player's current weapon.
	BOOL					bRestrictedVehicle					// Is this a restricted vehicle (not to be recreated for the player when they reject a replay)
	VEHICLE_INDEX			mVehicleIndex						// Vehicle index of the player's last vehicle
	enumCharacterList		eVehicleOwner						// Which player character the associated vehicle snapshot was stored for
	INT						iPreviousWeather					// Previous weather type
	INT						iNextWeather						// Next weather type
	FLOAT					fWeatherInterp						// Interpolation value of weather

	// all player characters
	INT						iBankLogIndex[NUM_OF_PLAYABLE_PEDS]	// Holds the index of the last bank transaction for each of the player characters.
	INT						iCash[NUM_OF_PLAYABLE_PEDS]			// Holds the amount of cash each character has
	INT						iArmour[NUM_OF_PLAYABLE_PEDS]		// All player characters' armour.
	WEAPON_TYPE				mFailWeapon[NUM_OF_PLAYABLE_PEDS]	// Weapon the player was using when he failed the mission.
	PED_VEH_DATA_STRUCT 	mPlayerVehicle[2][NUM_OF_PLAYABLE_PEDS] // Stores mods for the player's default vehicles (slot 0 = car. Slot 1 = bike)
	PED_VARIATION_STRUCT	mVariations[NUM_OF_PLAYABLE_PEDS]	// All player characters' variations.
	PED_WEAPONS_STRUCT 		mWeapons[NUM_OF_PLAYABLE_PEDS]		// All player characters' weapons / mods.
	WEAPONS_PURCHASE_DATA	mPurchasedWeapons[NUM_OF_PLAYABLE_PEDS]	// all player characters' weapon purchase states.
	INT						iSpecialAbility[NUM_OF_PLAYABLE_PEDS]	// All player characters' special ability amount.
	PED_COMPONENTS_STRUCT 	sComponents[NUM_OF_PLAYABLE_PEDS]	// Info on each clothing item (acquired, available, used)
	PED_TATTOO_STRUCT		sTattoos[NUM_OF_PLAYABLE_PEDS]		// info on all tattoos (unlocked, acquired, viewed)
	
	// other structs
	PLAYER_SNAPSHOT 			mPlayerStruct					// Player position info
	VEHICLE_SNAPSHOT			mVehicleStruct					// Player's vehicle info
	structBattleBuddySnapshot	mBattleBuddyStruct
ENDSTRUCT

// Unsaved data
TEXT_LABEL_31	g_snapshotScriptName	// The script that stored the snapshot
SNAPSHOT_STRUCT	g_startSnapshot 		// Stores info from start of mission
SNAPSHOT_STRUCT	g_stageSnapshot			// Stores info from last checkpoint reached

// Saved data
// declared in sp_globals_saved.sch (g_structRepeatPlay sRepeatPlayData) 
