STRUCT CountryRaceDataSaved
	INT 	iCurrentRace
	INT		iBestTime
	INT		iSlipStreamHelpCount
	BOOL	bStallionUnlocked
	BOOL	bGauntletUnlocked
	BOOL	bDominatorUnlocked
	BOOL	bBuffaloUnlocked
	BOOL	bMarshallUnlocked
	BOOL	bRaceJustCompleted
	BOOL	bDisrupted
	BOOL	bTextRegistered
ENDSTRUCT

BOOL g_bCountryRacePassed

INT g_iRaceMissionCandidateID
INT g_iDebugRaceWarp = -1
