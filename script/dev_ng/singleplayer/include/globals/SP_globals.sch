USING "ambient_globals.sch"
USING "AmbientArea_globals.sch"
USING "blip_globals.sch"
USING "building_globals.sch"
USING "candidate_globals.sch"
USING "code_control_globals.sch"
USING "comms_control_globals.sch"
USING "context_control_globals.sch"
USING "drunk_globals.sch"
USING "email_globals.sch"
USING "event_globals.sch"
USING "finance_globals.sch"
USING "flow_globals.sch"
USING "family_globals.sch"
USING "friends_globals.sch"
USING "heist_globals.sch"
USING "mission_globals.sch"
USING "mission_trigger_globals.sch"
USING "organiser_globals.sch"
USING "ped_component_globals.sch"
USING "rampage_globals.sch"
USING "replay_globals.sch"
USING "respawn_globals.sch"
USING "respawn_cleanup_globals.sch"
USING "savegame_globals.sch"
USING "shop_globals.sch"
USING "snapshot_globals.sch"
USING "social_globals.sch"
USING "beast_secret_globals.sch"
USING "tattoo_globals.sch"
USING "trainline_globals.sch"
//USING "website_globals.sch" // included within vehicle_gen_globals.sch
USING "vehicle_gen_globals.sch"
USING "flow_help_globals.sch"
USING "launcher_globals.sch"
//USING "mission_stat_globals.sch"//included through flow globals
USING "floating_help_globals.sch"
USING "cutscene_control_globals.sch"
USING "BailBond_globals.sch"
USING "properties_globals.sch"
USING "minigame_globals.sch"
USING "director_mode_globals.sch"

// Must be last - it has to include any saved globals structs defined in scripts above here
USING "SP_globals_saved.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	SP_globals.sch
//		AUTHOR			:	Keith
//		DESCRIPTION		:	This is the globals file containing all single player globals.
//							It will be automatically included when Globals.sch is used
//							within any single player scripts.
//
//							Any global scripts included within here should split it's data
//							into saved and unsaved data. The saved data should be packaged
//							up in one struct so that it can be easily added to the Saved
//							Globals struct contained within SP_Globals_Saved.sch.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// SP Main Loop has started
BOOL g_isSPMainInitialised = FALSE
BOOL g_bCharacterModelCheckDone = TRUE		// Set FALSE if SP boots and the main.sc thread starts. 
											// Set back TRUE when the main thread validates and repairs the 
											// SP character model the player has saved.

// ammo values when giving peds weapons
CONST_INT INFINITE_AMMO -1

// ****************************************************************************************************************************************************
// XML MENUS - Simon Lashley - Steve T has moved these here temporarily as I suss out which missions are using segments of this in release mode. 
// ****************************************************************************************************************************************************

// ENUM FOR THE DIFFERENT TYPES OF OPTIONS AN XML MENU CAN HAVE
ENUM XML_MENU_OPTION_TYPE
    XML_MENU_OPTION_NULL,
    XML_MENU_OPTION_WARP,
    XML_MENU_OPTION_SCRIPT,
    XML_MENU_OPTION_VALUE
ENDENUM


// ENUM FOR THE DIFFERENT TYPES OF MENU LOADED IN GAME
ENUM XML_MENU_LIST
    XML_MENU_LIST_PERSONAL_WARPS,
    XML_MENU_LIST_SHARED_WARPS,
    XML_MENU_LIST_CODE_ACTIONS,
    XML_MENU_LIST_VEHICLES,
    XML_MENU_LIST_PEDS, 
    XML_MENU_LIST_OBJECTS,
    XML_MENU_LIST_MISSION,
	XML_MENU_LIST_MOVE,
    TOTAL_XML_MENUS,
    XML_MENU_LIST_NULL
ENDENUM


// ENUM FOR DIFFERENT MOVEMENTS ON THE MENU
ENUM XML_MENU_INPUT_TYPE
    XML_MENU_INPUT_NULL,
    XML_MENU_INPUT_UP,
    XML_MENU_INPUT_DOWN,
	XML_MENU_INPUT_LEFT,
    XML_MENU_INPUT_RIGHT,
    XML_MENU_INPUT_ACCEPT,
	XML_MENU_INPUT_ACCEPT2,
	XML_MENU_INPUT_ACCEPT3,
    XML_MENU_INPUT_CANCEL,
    XML_MENU_INPUT_NEXT_MENU,
    XML_MENU_INPUT_PREVIOUS_MENU,
    XML_MENU_INPUT_NEW_MENU,
	XML_MENU_INPUT_SEARCH,
	XML_MENU_INPUT_KEYPRESS
ENDENUM

// MAX ALLOWED ITEMS ON THE SCREEN
CONST_INT TOTAL_XML_ITEMS_ON_SCREEN 21


// STRUCT FOR THE DIFFERENT XML MENUS AND THE BUTTONS THAT LOAD THEM
STRUCT XML_MENUS

    // xml filename
    TEXT_LABEL_31 xml
    // button to load the menu, converted to enum and vice versa
    INT button
    // bool saying if the menu should be active
    BOOL active
    
ENDSTRUCT

// ACTUAL STRUCTURE STORING MENUS 
XML_MENUS xmlMenuInfo[TOTAL_XML_MENUS]


// STRUCT FOR STORING DATA FROM THE XML FILES
STRUCT XML_MENU_ITEM

    // The type of the menu option, 
    XML_MENU_OPTION_TYPE type
    // text that appears on the menu
    TEXT_LABEL_63 name
	
	TEXT_LABEL_15 modelName
    // location of warp
    VECTOR warp
    // float
    FLOAT heading
    // name of script to load
    TEXT_LABEL_31 scriptName
    // value to pass back
    INT value

ENDSTRUCT


// STRUCT FOR STORING INFO THAT APPEARS IN THE WIDGET WHEN 
// CREATING A VEHICLE, PED or OBJECT
/*STRUCT XML_MENU_WIDGET
    VECTOR coords
ENDSTRUCT*/

#IF IS_DEBUG_BUILD

// STRUCT FOR STORING DATA ABOUT THE MENU AND VALUES IT HOLDS
STRUCT XML_MENU_DATA

    // DRAWING
    // background rectangle colour
    INT colourBackground
    // active text colour
    INT colourActiveText
    // active text backshadow colour
    INT colourActiveTextBackshadow
    // deactive text colour
    INT colourDeactiveText
    // info text colour
    INT colourInfoText
    // bool saying if the menu is being drawn or not
    BOOL menuIsBeingDrawn

    // NAVIGATION
    // current menu item
    INT current 
    // total items on the menu
    INT total
    // item at the top of the screen used for scrolling
    INT top
	// key press used for item jump
	KEY_NUMBER key
    // value passed back to mission menus
    INT missionValue
    // scroll speed for menu
    INT menuScrollSpeed
    // choosen menu
    XML_MENU_LIST choosenMenu
    
    // DEBUG
    FLOAT size
    
    // TITLE
    TEXT_LABEL_63 menuTitle
    
    // ACTUAL STRUCTURE ARRAY THAT WILL HOLD ALL THE MENU ITEMS
    XML_MENU_ITEM items[TOTAL_XML_ITEMS_ON_SCREEN+1]

    // widget 
    //XML_MENU_WIDGET widget

ENDSTRUCT

// ACTUAL STRUCTURE STORING ALL THE MENU DATA
XML_MENU_DATA xmlMenuData

#ENDIF	//	IS_DEBUG_BUILD

// ********************************************************
// ********************************************************
// ********************************************************
// END OF XML MENUS 
// ********************************************************
// ********************************************************
// ********************************************************
