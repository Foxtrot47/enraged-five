//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		MISSION NAME	:	ped_component_globals.sch									//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Contains the saved data struct and a list of enums that we	//
//							can use to reference component items and types.				//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "commands_ped.sch"
USING "script_maths.sch"

// Const ints for states that can be changed
CONST_INT PED_COMPONENT_AVAILABLE_SLOT		0
CONST_INT PED_COMPONENT_ACQUIRED_SLOT		1
CONST_INT PED_COMPONENT_USED_SLOT			2

// Const ints for states that remain static
CONST_INT PED_COMPONENT_CAN_HAVE_BIT 		0
CONST_INT PED_COMPONENT_AVAILABLE_BIT 		1
CONST_INT PED_COMPONENT_ACQUIRED_BIT 		2
CONST_INT PED_COMPONENT_OUTFIT_ONLY_BIT 	3
CONST_INT PED_COMPONENT_IS_NEW_BIT 			4
CONST_INT PED_COMPONENT_USE_SP_DATA_BIT 	5
CONST_INT PED_COMPONENT_IS_DLC_BIT			6
CONST_INT PED_COMPONENT_IS_REBREATHER_BIT	7

CONST_INT PED_COMPONENT_BITSETS				10
CONST_INT PED_COMPONENTS_PER_TYPE			(PED_COMPONENT_BITSETS*32)
CONST_INT PED_COMPONENTS_OUTFITS			54
CONST_INT PED_COMPONENTS_PROPGROUPS			20

// DLC Ped comp restriction hash
CONST_INT DLC_RESTRICTION_OUTFIT_ONLY		HASH("OUTFIT_ONLY")		// Item should only be used as part of a full outfit
CONST_INT DLC_RESTRICTION_TAG_HAT			HASH("HAT")				// Item is a hat
CONST_INT DLC_RESTRICTION_TAG_FITTED_CAP	HASH("FITTED_CAP")		// Item is a hat
CONST_INT DLC_RESTRICTION_TAG_BOWLER_HAT	HASH("BOWLER_HAT")		// Item is a hat
CONST_INT DLC_RESTRICTION_TAG_TOP_HAT		HASH("TOP_HAT")			// Item is a hat
CONST_INT DLC_RESTRICTION_TAG_GLASSES		HASH("GLASSES")			// Item is a pair of glasses
CONST_INT DLC_RESTRICTION_TAG_BIG_GLASSES	HASH("BIG_GLASSES")		// Item is a pair of glasses that should now be worn with helmets
CONST_INT DLC_RESTRICTION_TAG_WATCH			HASH("WATCH")			// Item is a watch
CONST_INT DLC_RESTRICTION_TAG_VEST_TIE		HASH("VEST_TIE")		// Item is a vest tie
CONST_INT DLC_RESTRICTION_TAG_LONG_SLEEVE	HASH("LONG_SLEEVE")		// Item has long sleeves
CONST_INT DLC_RESTRICTION_TAG_JACKET		HASH("JACKET")			// Item is a jacket
CONST_INT DLC_RESTRICTION_TAG_TAILS_JACKET	HASH("TAILS_JACKET")	// Item is a tails jacket
CONST_INT DLC_RESTRICTION_TAG_SCRUFFY_JACKET	HASH("SCRUFFY_JACKET")	// Item is a scruffy jacket
CONST_INT DLC_RESTRICTION_TAG_UNDER_JACKET	HASH("UNDER_JACKET")	// Item to be worn under a jacket (For use with JBIB->SPECIAL variants)
CONST_INT DLC_RESTRICTION_TAG_OVER_JACKET	HASH("OVER_JACKET")		// Item to be worn on top if a top
CONST_INT DLC_RESTRICTION_TAG_ALT_SPECIAL	HASH("ALT_SPECIAL")		// Item should be used as the alternaitve version (For use with the special jacket)
CONST_INT DLC_RESTRICTION_TAG_ALT_SPECIAL_2	HASH("ALT_SPECIAL_2")	// Item should be used as the alternaitve version (For use with the special jacket)
CONST_INT DLC_RESTRICTION_TAG_ALT_SPECIAL_3	HASH("ALT_SPECIAL_3")	// Item should be used as the alternaitve version (For use with the special jacket)
CONST_INT DLC_RESTRICTION_TAG_ALT_SPECIAL_4	HASH("ALT_SPECIAL_4")	// Item should be used as the alternaitve version (For use with the special jacket)
CONST_INT DLC_RESTRICTION_TAG_ALT_SPECIAL_5	HASH("ALT_SPECIAL_5")	// Item should be used as the alternaitve version (For use with the special jacket)
CONST_INT DLC_RESTRICTION_TAG_ALT_SPECIAL_6	HASH("ALT_SPECIAL_6")	// Item should be used as the alternaitve version (For use with the special jacket)
CONST_INT DLC_RESTRICTION_TAG_NO_ALT		HASH("NO_ALT")			// Item doesn't have an alt version - for use with some scarfs.
CONST_INT DLC_RESTRICTION_TAG_TAILS_VERSION HASH("TAILS_VERSION")	// Item should be used as the alternaitve version (For use with the untucked shirts and tails jacket)
CONST_INT DLC_RESTRICTION_TAG_HAS_TAILS_VERSION HASH("HAS_TAILS_VERSION")	// Item should be used as the alternaitve version (For use with the untucked shirts and tails jacket)
CONST_INT DLC_RESTRICTION_TAG_ALT_JBIB		HASH("ALT_JBIB")		// Item should be used as the alternaitve version
CONST_INT DLC_RESTRICTION_TAG_TAT_DECL		HASH("TAT_DECL")		// Item uses the DECL component to force a tatoo overlay
CONST_INT DLC_RESTRICTION_TAG_HIGH_HEELS	HASH("HIGH_HEELS")		// Item is a pair of high heels
CONST_INT DLC_RESTRICTION_TAG_SKIRT			HASH("SKIRT")			// Item is a skirt
CONST_INT DLC_RESTRICTION_TAG_MULTI_DECAL	HASH("MULTI_DECAL")		// Item can be mixed with multiple decals
CONST_INT DLC_RESTRICTION_TAG_STOCKINGS		HASH("STOCKINGS")		// Item is for use with stockings
CONST_INT DLC_RESTRICTION_TAG_DRESS			HASH("DRESS")			// Item is a dress
CONST_INT DLC_RESTRICTION_TAG_HIPSTER_DRESS	HASH("HIPSTER_DRESS")	// Item is a dress from hipster pack
CONST_INT DLC_RESTRICTION_TAG_ALLOW_TIES	HASH("ALLOW_TIES")		// Item is suitable with ties
CONST_INT DLC_RESTRICTION_TAG_VEST_SHIRT	HASH("VEST_SHIRT")		// Item is a vest shirt
CONST_INT DLC_RESTRICTION_TAG_DRESS_LEGS	HASH("DRESS_LEGS")		// Item is legs for use with a dress
CONST_INT DLC_RESTRICTION_TAG_LONG_NECKLACE	HASH("LONG_NECKLACE")	// Item is a long necklace
CONST_INT DLC_RESTRICTION_TAG_VEST			HASH("VEST")			// Item can mix with the vest (waistcoat)
CONST_INT DLC_RESTRICTION_TAG_BASIC_VEST	HASH("BASIC_VEST")		// Item can mix with the vest (waistcoat)
CONST_INT DLC_RESTRICTION_TAG_CUFFED_SHIRT 	HASH("CUFFED_SHIRT")	// Item is a cuffed shirt
CONST_INT DLC_RESTRICTION_TAG_SHIRT_BRACES 	HASH("SHIRT_BRACES")	// Item is a shirt with braces
CONST_INT DLC_RESTRICTION_TAG_ANIM_OVERRIDE HASH("ANIM_OVERRIDE")	// Item uses female animation override

CONST_INT DLC_RESTRICTION_TAG_SCARF 		HASH("SCARF")			// Item is a scarf
CONST_INT DLC_RESTRICTION_TAG_JACKET_SCARF 	HASH("JACKET_SCARF")	// Item is a scarf that should be used for jackets

CONST_INT DLC_RESTRICTION_TAG_TUX_JACKET 	HASH("TUX_JACKET")		// Item is a tux jacket
CONST_INT DLC_RESTRICTION_TAG_TUX_VEST		HASH("TUX_VEST")		// Item is a tux vest
CONST_INT DLC_RESTRICTION_TAG_TUX_SHIRT 	HASH("TUX_SHIRT")		// Item is a tux shirt
CONST_INT DLC_RESTRICTION_TAG_TUX_TIE 		HASH("TUX_TIE")			// Item is a tux tie
CONST_INT DLC_RESTRICTION_TAG_TUX_TIE_ALT	HASH("TUX_TIE_ALT")		// Item is a stubby tux tie
CONST_INT DLC_RESTRICTION_TAG_TUX_PANTS 	HASH("TUX_PANTS")		// Item is a pair of tux pants

CONST_INT DLC_RESTRICTION_TAG_ALT_VEST	 	HASH("ALT_VEST")		// Item should be used as the alternaitve version (For use with the closed tux jacket)

CONST_INT DLC_RESTRICTION_TAG_OPEN_JACKET 	HASH("OPEN_JACKET")		// Item is an open jacket
CONST_INT DLC_RESTRICTION_TAG_CLOSED_JACKET HASH("CLOSED_JACKET")	// Item is a closed jacket

CONST_INT DLC_RESTRICTION_TAG_OPEN_COLLAR 	HASH("OPEN_COLLAR")		// Item is an open collar shirt
CONST_INT DLC_RESTRICTION_TAG_CLOSED_COLLAR HASH("CLOSED_COLLAR")	// Item is a closed collar shirt

CONST_INT DLC_RESTRICTION_TAG_LOOSE_BOWTIE	HASH("LOOSE_BOWTIE")	// Item is a loose bowtie
CONST_INT DLC_RESTRICTION_TAG_LOOSE_TIE		HASH("LOOSE_TIE")		// Item is a loose tie
CONST_INT DLC_RESTRICTION_TAG_SLACK_TIE		HASH("SLACK_TIE")		// Item is a slack tie
CONST_INT DLC_RESTRICTION_TAG_TIE			HASH("TIE")				// Item is a normal tie
CONST_INT DLC_RESTRICTION_TAG_BOWTIE		HASH("BOWTIE")			// Item is a bowtie
CONST_INT DLC_RESTRICTION_TAG_ALT_TIE		HASH("ALT_TIE")			// Item is alt version
CONST_INT DLC_RESTRICTION_TAG_FULL_BOWTIE	HASH("FULL_BOWTIE")		// Item is a full bowtie

CONST_INT DLC_RESTRICTION_TAG_OFF_SHOULDER_ACCS 	HASH("OFF_SHOULDER_ACCS")	// Item is an off shoulder top accs

CONST_INT DLC_RESTRICTION_TAG_GUN_ACCS		HASH("GUN_ACCS")		// Item is for gun accs menu
CONST_INT DLC_RESTRICTION_TAG_EAR_PIECE		HASH("EAR_PIECE")		// Item is earpiece
CONST_INT DLC_RESTRICTION_TAG_NIGHT_VISION	HASH("NIGHT_VISION")	// Item is night vision goggles
CONST_INT DLC_RESTRICTION_TAG_REBREATHER	HASH("REBREATHER")		// Item is rebreather

CONST_INT DLC_RESTRICTION_TAG_PILOT_SUIT	HASH("PILOT_SUIT")		// Item is the pilot suit
CONST_INT DLC_RESTRICTION_TAG_COMBAT_GEAR	HASH("COMBAT_GEAR")		// Item is from the combat group
CONST_INT DLC_RESTRICTION_TAG_COMBAT_TOP	HASH("COMBAT_TOP")		// Item is the combat top
CONST_INT DLC_RESTRICTION_TAG_COMBAT_SWEAT	HASH("COMBAT_SWEAT")	// Item is the combat sweater
CONST_INT DLC_RESTRICTION_TAG_FORCE_BALD	HASH("FORCE_BALD")		// Item forces player to use bald hair
CONST_INT DLC_RESTRICTION_TAG_HELMET		HASH("HELMET")			// Item is a helmet
CONST_INT DLC_RESTRICTION_TAG_BULLETPROOF 	HASH("BULLETPROOF")		// Item is bulletproof
CONST_INT DLC_RESTRICTION_TAG_FULL_FACE 	HASH("FULL_FACE")		// Item is full face helmet
CONST_INT DLC_RESTRICTION_TAG_HEIST_GEAR	HASH("HEIST_GEAR")		// Item is from the heist group
CONST_INT DLC_RESTRICTION_TAG_GAS_MASK		HASH("GAS_MASK")		// Item is gas mask
CONST_INT DLC_RESTRICTION_TAG_HAZ_MASK		HASH("HAZ_MASK")		// Item is hazmat mask
CONST_INT DLC_RESTRICTION_TAG_HAZ_HOOD		HASH("HAZ_HOOD")		// Item is hazmat hood
CONST_INT DLC_RESTRICTION_TAG_HOOD_DOWN		HASH("HOOD_DOWN")		// Item is checm suit hood (down)
CONST_INT DLC_RESTRICTION_TAG_HOOD_UP		HASH("HOOD_UP")			// Item is checm suit hood (up)
CONST_INT DLC_RESTRICTION_TAG_HOOD_TUCKED	HASH("HOOD_TUCKED")		// Item is checm suit hood (tucked)

CONST_INT DLC_RESTRICTION_TAG_HAT_MENU		HASH("HAT_MENU")		// Item should appear in the hat menu

CONST_INT DLC_RESTRICTION_TAG_DRAW_0		HASH("DRAW_0")			// Item used to be drawable 0 (For use with the TEETH/JBIB checks)
CONST_INT DLC_RESTRICTION_TAG_DRAW_1		HASH("DRAW_1")			// Item used to be drawable 1 (For use with the TEETH/JBIB checks)
CONST_INT DLC_RESTRICTION_TAG_DRAW_2		HASH("DRAW_2")			// Item used to be drawable 2 (For use with the TEETH/JBIB checks)
CONST_INT DLC_RESTRICTION_TAG_DRAW_3		HASH("DRAW_3")			// Item used to be drawable 3 (For use with the TEETH/JBIB checks)
CONST_INT DLC_RESTRICTION_TAG_DRAW_4		HASH("DRAW_4")			// Item used to be drawable 4 (For use with the TEETH/JBIB checks)
CONST_INT DLC_RESTRICTION_TAG_DRAW_5		HASH("DRAW_5")			// Item used to be drawable 5 (For use with the TEETH/JBIB checks)
CONST_INT DLC_RESTRICTION_TAG_DRAW_6		HASH("DRAW_6")			// Item used to be drawable 6 (For use with the TEETH/JBIB checks)
CONST_INT DLC_RESTRICTION_TAG_DRAW_7		HASH("DRAW_7")			// Item used to be drawable 7 (For use with the TEETH/JBIB checks)
CONST_INT DLC_RESTRICTION_TAG_DRAW_8		HASH("DRAW_8")			// Item used to be drawable 8 (For use with the TEETH/JBIB checks)
CONST_INT DLC_RESTRICTION_TAG_DRAW_9		HASH("DRAW_9")			// Item used to be drawable 9 (For use with the TEETH/JBIB checks)
CONST_INT DLC_RESTRICTION_TAG_DRAW_10		HASH("DRAW_10")			// Item used to be drawable 10 (For use with the TEETH/JBIB checks)
CONST_INT DLC_RESTRICTION_TAG_DRAW_11		HASH("DRAW_11")			// Item used to be drawable 11 (For use with the TEETH/JBIB checks)
CONST_INT DLC_RESTRICTION_TAG_DRAW_12		HASH("DRAW_12")			// Item used to be drawable 12 (For use with the TEETH/JBIB checks)
CONST_INT DLC_RESTRICTION_TAG_DRAW_13		HASH("DRAW_13")			// Item used to be drawable 13 (For use with the TEETH/JBIB checks)
CONST_INT DLC_RESTRICTION_TAG_DRAW_14		HASH("DRAW_14")			// Item used to be drawable 14 (For use with the TEETH/JBIB checks)
CONST_INT DLC_RESTRICTION_TAG_DRAW_15		HASH("DRAW_15")			// Item used to be drawable 15 (For use with the TEETH/JBIB checks)

CONST_INT DLC_RESTRICTION_TAG_BERD_HELMET	HASH("BERD_HELMET")		// Item is a Helmet but uses the BERD type.
CONST_INT DLC_RESTRICTION_TAG_DOME_HELMET	HASH("DOME_HELMET")		// Item is a dome helmet that works with the half masks.
CONST_INT DLC_RESTRICTION_TAG_BIKER_MASK	HASH("BIKER_MASK")		// Item is a half face mask that should be restricted for some items
CONST_INT DLC_RESTRICTION_TAG_BIG_MASK		HASH("BIG_MASK")		// Item is a big mask that should be restricted for some items
CONST_INT DLC_RESTRICTION_TAG_PARACHUTE		HASH("PARACHUTE")		// Item is a parachute
CONST_INT DLC_RESTRICTION_TAG_DUFFEL_BAG 	HASH("DUFFEL_BAG")		// Item is a duffel bag
CONST_INT DLC_RESTRICTION_TAG_GLOVES		HASH("GLOVES")			// Item is a pair of gloves

CONST_INT DLC_RESTRICTION_TAG_MOTOX_GLOVES	HASH("MOTOX_GLOVES")			// Item is a pair of motoX gloves
CONST_INT DLC_RESTRICTION_TAG_RACE_GLOVES	HASH("RACE_GLOVES")				// Item is a pair of racesuit gloves

CONST_INT DLC_RESTRICTION_TAG_SHRINK_HEAD	HASH("SHRINK_HEAD")		// Item should shrink players head when applied
CONST_INT DLC_RESTRICTION_TAG_OUTFIT_CHECKS	HASH("OUTFIT_CHECKS")	// Item should use new retriction checks when processing force_valid_combo.
CONST_INT DLC_RESTRICTION_TAG_FORCE_PROP	HASH("FORCE_PROP")		// Item should force the shades and baseball hat props

CONST_INT DLC_RESTRICTION_TAG_HOOD_COMPAT	HASH("HOOD_COMPAT")		// Item is compatible with the hooded jacket
CONST_INT DLC_RESTRICTION_TAG_HOODED_JACKET	HASH("HOODED_JACKET")		// Item is hooded jacket

CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_0	HASH("HEIST_DRAW_0")	// Item item is part of the drawable 0 group for heist data
CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_1	HASH("HEIST_DRAW_1")	// Item item is part of the drawable 1 group for heist data
CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_2	HASH("HEIST_DRAW_2")	// Item item is part of the drawable 2 group for heist data
CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_3	HASH("HEIST_DRAW_3")	// Item item is part of the drawable 3 group for heist data
CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_4	HASH("HEIST_DRAW_4")	// Item item is part of the drawable 4 group for heist data
CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_5	HASH("HEIST_DRAW_5")	// Item item is part of the drawable 5 group for heist data
CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_6	HASH("HEIST_DRAW_6")	// Item item is part of the drawable 6 group for heist data
CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_7	HASH("HEIST_DRAW_7")	// Item item is part of the drawable 7 group for heist data
CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_8	HASH("HEIST_DRAW_8")	// Item item is part of the drawable 8 group for heist data
CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_9	HASH("HEIST_DRAW_9")	// Item item is part of the drawable 9 group for heist data
CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_10	HASH("HEIST_DRAW_10")	// Item item is part of the drawable 10 group for heist data
CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_11	HASH("HEIST_DRAW_11")	// Item item is part of the drawable 11 group for heist data
CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_12	HASH("HEIST_DRAW_12")	// Item item is part of the drawable 12 group for heist data
CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_13	HASH("HEIST_DRAW_13")	// Item item is part of the drawable 13 group for heist data
CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_14	HASH("HEIST_DRAW_14")	// Item item is part of the drawable 14 group for heist data
CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_15	HASH("HEIST_DRAW_15")	// Item item is part of the drawable 15 group for heist data
CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_16	HASH("HEIST_DRAW_16")	// Item item is part of the drawable 16 group for heist data
CONST_INT DLC_RESTRICTION_TAG_HEIST_DRAW_17	HASH("HEIST_DRAW_17")	// Item item is part of the drawable 17 group for heist data

CONST_INT DLC_RESTRICTION_TAG_XMAS2_DRAW_0	HASH("XMAS2_DRAW_0")	// Item item is part of the drawable 0 group for christmas2 data
CONST_INT DLC_RESTRICTION_TAG_XMAS2_DRAW_1	HASH("XMAS2_DRAW_1")	// Item item is part of the drawable 1 group for christmas2 data

CONST_INT DLC_RESTRICTION_TAG_LUXE_DRAW_0	HASH("LUXE_DRAW_0")		// Item is part of the drawable 0 group for luxe data
CONST_INT DLC_RESTRICTION_TAG_LUXE_DRAW_1	HASH("LUXE_DRAW_1")		// Item is part of the drawable 1 group for luxe data
CONST_INT DLC_RESTRICTION_TAG_LUXE_DRAW_2	HASH("LUXE_DRAW_2")		// Item is part of the drawable 2 group for luxe data
CONST_INT DLC_RESTRICTION_TAG_LUXE_DRAW_3	HASH("LUXE_DRAW_3")		// Item is part of the drawable 3 group for luxe data
CONST_INT DLC_RESTRICTION_TAG_LUXE_DRAW_4	HASH("LUXE_DRAW_4")		// Item is part of the drawable 4 group for luxe data
CONST_INT DLC_RESTRICTION_TAG_LUXE_DRAW_5	HASH("LUXE_DRAW_5")		// Item is part of the drawable 5 group for luxe data
CONST_INT DLC_RESTRICTION_TAG_LUXE_DRAW_6	HASH("LUXE_DRAW_6")		// Item is part of the drawable 6 group for luxe data
CONST_INT DLC_RESTRICTION_TAG_LUXE_DRAW_7	HASH("LUXE_DRAW_7")		// Item is part of the drawable 7 group for luxe data
CONST_INT DLC_RESTRICTION_TAG_LUXE_DRAW_8	HASH("LUXE_DRAW_8")		// Item is part of the drawable 8 group for luxe data
CONST_INT DLC_RESTRICTION_TAG_LUXE_DRAW_9	HASH("LUXE_DRAW_9")		// Item is part of the drawable 9 group for luxe data
CONST_INT DLC_RESTRICTION_TAG_LUXE_DRAW_10	HASH("LUXE_DRAW_10")	// Item is part of the drawable 10 group for luxe data
CONST_INT DLC_RESTRICTION_TAG_LUXE_DRAW_11	HASH("LUXE_DRAW_11")	// Item is part of the drawable 10 group for luxe data
CONST_INT DLC_RESTRICTION_TAG_LUXE_DRAW_12	HASH("LUXE_DRAW_12")	// Item is part of the drawable 10 group for luxe data
CONST_INT DLC_RESTRICTION_TAG_LUXE_DRAW_13	HASH("LUXE_DRAW_13")	// Item is part of the drawable 10 group for luxe data

CONST_INT DLC_RESTRICTION_TAG_LUXE_CLOTH	HASH("LUXE_CLOTH")		// Item is part of the luxe clothing for outfit checks

CONST_INT DLC_RESTRICTION_TAG_LUXE_BOMB		HASH("LUXE_BOMB")		// Item is luxe bomber
CONST_INT DLC_RESTRICTION_TAG_LUXE_COAT		HASH("LUXE_COAT")		// Item is luxe coat
CONST_INT DLC_RESTRICTION_TAG_LUXE_SCARF	HASH("LUXE_SCARF")		// Item is luxe scarf
CONST_INT DLC_RESTRICTION_TAG_LUXE_SWEAT	HASH("LUXE_SWEAT")		// Item is luxe sweater

CONST_INT DLC_RESTRICTION_TAG_CHAIN			HASH("CHAIN")			// Item is part a chain
CONST_INT DLC_RESTRICTION_TAG_ALT_CHAIN		HASH("ALT_CHAIN")			// Item is part a chain
CONST_INT DLC_RESTRICTION_TAG_EARRING		HASH("EARRING")			// Item is an earring
CONST_INT DLC_RESTRICTION_TAG_CUFF			HASH("CUFF")			// Item is a luxe cuff
CONST_INT DLC_RESTRICTION_TAG_TAT_DECL_NO_SAVE	HASH("TAT_DECL_NO_SAVE")	// Item uses the DECL component to force a tatoo overlay but doesn't use tattoo save data


CONST_INT DLC_RESTRICTION_TAG_LUXE2_DRAW_0	HASH("LUXE2_DRAW_0")		// Item is part of the drawable 0 group for luxe2 data
CONST_INT DLC_RESTRICTION_TAG_LUXE2_DRAW_1	HASH("LUXE2_DRAW_1")		// Item is part of the drawable 1 group for luxe2 data
CONST_INT DLC_RESTRICTION_TAG_LUXE2_DRAW_2	HASH("LUXE2_DRAW_2")		// Item is part of the drawable 2 group for luxe2 data
CONST_INT DLC_RESTRICTION_TAG_LUXE2_DRAW_3	HASH("LUXE2_DRAW_3")		// Item is part of the drawable 3 group for luxe2 data
CONST_INT DLC_RESTRICTION_TAG_LUXE2_DRAW_4	HASH("LUXE2_DRAW_4")		// Item is part of the drawable 4 group for luxe2 data
CONST_INT DLC_RESTRICTION_TAG_LUXE2_DRAW_5	HASH("LUXE2_DRAW_5")		// Item is part of the drawable 5 group for luxe2 data
CONST_INT DLC_RESTRICTION_TAG_LUXE2_DRAW_6	HASH("LUXE2_DRAW_6")		// Item is part of the drawable 6 group for luxe2 data
CONST_INT DLC_RESTRICTION_TAG_LUXE2_DRAW_7	HASH("LUXE2_DRAW_7")		// Item is part of the drawable 7 group for luxe2 data
CONST_INT DLC_RESTRICTION_TAG_LUXE2_DRAW_8	HASH("LUXE2_DRAW_8")		// Item is part of the drawable 8 group for luxe2 data
CONST_INT DLC_RESTRICTION_TAG_LUXE2_DRAW_9	HASH("LUXE2_DRAW_9")		// Item is part of the drawable 9 group for luxe2 data
CONST_INT DLC_RESTRICTION_TAG_LUXE2_DRAW_10	HASH("LUXE2_DRAW_10")		// Item is part of the drawable 10 group for luxe2 data
CONST_INT DLC_RESTRICTION_TAG_LUXE2_DRAW_11	HASH("LUXE2_DRAW_11")		// Item is part of the drawable 10 group for luxe2 data
CONST_INT DLC_RESTRICTION_TAG_LUXE2_DRAW_12	HASH("LUXE2_DRAW_12")		// Item is part of the drawable 10 group for luxe2 data
CONST_INT DLC_RESTRICTION_TAG_LUXE2_DRAW_13	HASH("LUXE2_DRAW_13")		// Item is part of the drawable 10 group for luxe2 data

CONST_INT DLC_RESTRICTION_TAG_HTB_MASK		HASH("HTB_MASK")				// Item is the beast mask

CONST_INT DLC_RESTRICTION_TAG_LOW_DRAW_0	HASH("LOW_DRAW_0")		// Item is part of the drawable 0 group for lowrider data
CONST_INT DLC_RESTRICTION_TAG_LOW_DRAW_1	HASH("LOW_DRAW_1")		// Item is part of the drawable 1 group for lowrider data
CONST_INT DLC_RESTRICTION_TAG_LOW_DRAW_2	HASH("LOW_DRAW_2")		// Item is part of the drawable 2 group for lowrider data
CONST_INT DLC_RESTRICTION_TAG_LOW_DRAW_3	HASH("LOW_DRAW_3")		// Item is part of the drawable 3 group for lowrider data
CONST_INT DLC_RESTRICTION_TAG_LOW_DRAW_4	HASH("LOW_DRAW_4")		// Item is part of the drawable 4 group for lowrider data
CONST_INT DLC_RESTRICTION_TAG_LOW_DRAW_5	HASH("LOW_DRAW_5")		// Item is part of the drawable 5 group for lowrider data
CONST_INT DLC_RESTRICTION_TAG_LOW_DRAW_6	HASH("LOW_DRAW_6")		// Item is part of the drawable 6 group for lowrider data
CONST_INT DLC_RESTRICTION_TAG_LOW_DRAW_7	HASH("LOW_DRAW_7")		// Item is part of the drawable 7 group for lowrider data
CONST_INT DLC_RESTRICTION_TAG_LOW_DRAW_8	HASH("LOW_DRAW_8")		// Item is part of the drawable 8 group for lowrider data
CONST_INT DLC_RESTRICTION_TAG_LOW_DRAW_9	HASH("LOW_DRAW_9")		// Item is part of the drawable 9 group for lowrider data
CONST_INT DLC_RESTRICTION_TAG_LOW_DRAW_10	HASH("LOW_DRAW_10")		// Item is part of the drawable 10 group for lowrider data
CONST_INT DLC_RESTRICTION_TAG_LOW_DRAW_11	HASH("LOW_DRAW_11")		// Item is part of the drawable 10 group for lowrider data
CONST_INT DLC_RESTRICTION_TAG_LOW_DRAW_12	HASH("LOW_DRAW_12")		// Item is part of the drawable 10 group for lowrider data
CONST_INT DLC_RESTRICTION_TAG_LOW_DRAW_13	HASH("LOW_DRAW_13")		// Item is part of the drawable 10 group for lowrider data

CONST_INT DLC_RESTRICTION_TAG_LOW_BOMB		HASH("LOW_BOMB")			// Item is lowrider bomber
CONST_INT DLC_RESTRICTION_TAG_LOW_SWEAT		HASH("LOW_SWEAT")			// Item is lowrider sweater

CONST_INT DLC_RESTRICTION_TAG_CREW_LOCK		HASH("CREW_LOCK")			// Item is a crew locked item
CONST_INT DLC_RESTRICTION_TAG_CREW_COL		HASH("CREW_COL")			// Item is sets the crew colour
CONST_INT DLC_RESTRICTION_TAG_CREW_JACKET	HASH("CREW_JACKET")			// Item is a crew jacket
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO		HASH("CREW_LOGO")			// Item is a crew logo on the decl component

CONST_INT DLC_RESTRICTION_TAG_SILK_PYJAMAS		HASH("SILK_PYJAMAS")		// 
CONST_INT DLC_RESTRICTION_TAG_SILK_ROBE			HASH("SILK_ROBE")			// 
CONST_INT DLC_RESTRICTION_TAG_SMOKING_JACKET 	HASH("SMOKING_JACKET")		// 
CONST_INT DLC_RESTRICTION_TAG_BARE_FEET		 	HASH("BARE_FEET")		// 
CONST_INT DLC_RESTRICTION_TAG_SANTA_SUIT		HASH("SANTA_SUIT")			// 


CONST_INT DLC_RESTRICTION_TAG_JAN_DRAW_1	HASH("JAN_DRAW_1")		// Item is part of the drawable 1 group for january2016 data
CONST_INT DLC_RESTRICTION_TAG_JAN_DRAW_2	HASH("JAN_DRAW_2")		// Item is part of the drawable 2 group for january2016 data
CONST_INT DLC_RESTRICTION_TAG_JAN_BOMB		HASH("JAN_BOMB")		// Item is january2016 bomber


CONST_INT DLC_RESTRICTION_TAG_LOW2_DRAW_0	HASH("LOW2_DRAW_0")		// Item is part of the drawable 0 group for lowrider2 data
CONST_INT DLC_RESTRICTION_TAG_LOW2_DRAW_1	HASH("LOW2_DRAW_1")		// Item is part of the drawable 1 group for lowrider2 data
CONST_INT DLC_RESTRICTION_TAG_LOW2_DRAW_2	HASH("LOW2_DRAW_2")		// Item is part of the drawable 2 group for lowrider2 data
CONST_INT DLC_RESTRICTION_TAG_LOW2_DRAW_3	HASH("LOW2_DRAW_3")		// Item is part of the drawable 3 group for lowrider2 data
CONST_INT DLC_RESTRICTION_TAG_LOW2_DRAW_4	HASH("LOW2_DRAW_4")		// Item is part of the drawable 4 group for lowrider2 data
CONST_INT DLC_RESTRICTION_TAG_LOW2_DRAW_5	HASH("LOW2_DRAW_5")		// Item is part of the drawable 5 group for lowrider2 data
CONST_INT DLC_RESTRICTION_TAG_LOW2_DRAW_6	HASH("LOW2_DRAW_6")		// Item is part of the drawable 6 group for lowrider2 data
CONST_INT DLC_RESTRICTION_TAG_LOW2_DRAW_7	HASH("LOW2_DRAW_7")		// Item is part of the drawable 7 group for lowrider2 data
CONST_INT DLC_RESTRICTION_TAG_LOW2_DRAW_8	HASH("LOW2_DRAW_8")		// Item is part of the drawable 8 group for lowrider2 data
CONST_INT DLC_RESTRICTION_TAG_LOW2_DRAW_9	HASH("LOW2_DRAW_9")		// Item is part of the drawable 9 group for lowrider2 data

CONST_INT DLC_RESTRICTION_TAG_LOW2_BOMB			HASH("LOW2_BOMB")		// Item is lowrider2 bomber
CONST_INT DLC_RESTRICTION_TAG_LOW2_OPEN_CHECK 	HASH("LOW2_OPEN_CHECK")	// Item is lowrider2 open check shirt
CONST_INT DLC_RESTRICTION_TAG_HIGH_WAIST 		HASH("HIGH_WAIST")		// Item is lowrider2 high waist trousers

CONST_INT DLC_RESTRICTION_TAG_NO_CREW_EMBLEM	HASH("NO_CREW_EMBLEM")	// Item can't be used with crew emblem
CONST_INT DLC_RESTRICTION_TAG_BRACES	 		HASH("BRACES")			// Item is lowrider2 braces
CONST_INT DLC_RESTRICTION_TAG_BIG_CHAIN	 		HASH("BIG_CHAIN")			// Item is a big chain that clips with normal jackets
CONST_INT DLC_RESTRICTION_TAG_JACKET_ONLY		HASH("JACKET_ONLY") 	//item is a jacket but doesnt work with tees

CONST_INT DLC_RESTRICTION_TAG_BLOCK_SCARFS		HASH("BLOCK_SCARFS") 	// Item can't be worn with a scarf
CONST_INT DLC_RESTRICTION_TAG_BLOCK_CHAINS		HASH("BLOCK_CHAINS") 	// Item can't be worn with a chain

CONST_INT DLC_RESTRICTION_TAG_APART_CLOTH	HASH("APART_CLOTH")	// Item is part of the apartment/executive clothing for outfit checks

CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_0	HASH("APART_DRAW_0")	// Item is part of the drawable 0 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_1	HASH("APART_DRAW_1")	// Item is part of the drawable 1 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_2	HASH("APART_DRAW_2")	// Item is part of the drawable 2 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_3	HASH("APART_DRAW_3")	// Item is part of the drawable 3 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_4	HASH("APART_DRAW_4")	// Item is part of the drawable 4 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_5	HASH("APART_DRAW_5")	// Item is part of the drawable 5 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_6	HASH("APART_DRAW_6")	// Item is part of the drawable 6 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_7	HASH("APART_DRAW_7")	// Item is part of the drawable 7 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_8	HASH("APART_DRAW_8")	// Item is part of the drawable 8 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_9	HASH("APART_DRAW_9")	// Item is part of the drawable 9 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_10	HASH("APART_DRAW_10")	// Item is part of the drawable 10 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_11	HASH("APART_DRAW_11")	// Item is part of the drawable 11 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_12	HASH("APART_DRAW_12")	// Item is part of the drawable 12 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_13	HASH("APART_DRAW_13")	// Item is part of the drawable 13 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_14	HASH("APART_DRAW_14")	// Item is part of the drawable 14 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_15	HASH("APART_DRAW_15")	// Item is part of the drawable 15 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_16	HASH("APART_DRAW_16")	// Item is part of the drawable 16 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_17	HASH("APART_DRAW_17")	// Item is part of the drawable 17 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_18	HASH("APART_DRAW_18")	// Item is part of the drawable 18 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_19	HASH("APART_DRAW_19")	// Item is part of the drawable 19 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_20	HASH("APART_DRAW_20")	// Item is part of the drawable 20 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_21	HASH("APART_DRAW_21")	// Item is part of the drawable 21 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_22	HASH("APART_DRAW_22")	// Item is part of the drawable 22 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_23	HASH("APART_DRAW_23")	// Item is part of the drawable 23 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_24	HASH("APART_DRAW_24")	// Item is part of the drawable 24 group for apartment data
CONST_INT DLC_RESTRICTION_TAG_APART_DRAW_25	HASH("APART_DRAW_25")	// Item is part of the drawable 25 group for apartment data

CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_0	HASH("EXEC_DRAW_0")	// Item is part of the drawable 0 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_1	HASH("EXEC_DRAW_1")	// Item is part of the drawable 1 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_2	HASH("EXEC_DRAW_2")	// Item is part of the drawable 2 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_3	HASH("EXEC_DRAW_3")	// Item is part of the drawable 3 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_4	HASH("EXEC_DRAW_4")	// Item is part of the drawable 4 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_5	HASH("EXEC_DRAW_5")	// Item is part of the drawable 5 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_6	HASH("EXEC_DRAW_6")	// Item is part of the drawable 6 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_7	HASH("EXEC_DRAW_7")	// Item is part of the drawable 7 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_8	HASH("EXEC_DRAW_8")	// Item is part of the drawable 8 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_9	HASH("EXEC_DRAW_9")	// Item is part of the drawable 9 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_10	HASH("EXEC_DRAW_10")	// Item is part of the drawable 10 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_11	HASH("EXEC_DRAW_11")	// Item is part of the drawable 11 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_12	HASH("EXEC_DRAW_12")	// Item is part of the drawable 12 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_13	HASH("EXEC_DRAW_13")	// Item is part of the drawable 13 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_14	HASH("EXEC_DRAW_14")	// Item is part of the drawable 14 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_15	HASH("EXEC_DRAW_15")	// Item is part of the drawable 15 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_16	HASH("EXEC_DRAW_16")	// Item is part of the drawable 16 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_17	HASH("EXEC_DRAW_17")	// Item is part of the drawable 17 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_18	HASH("EXEC_DRAW_18")	// Item is part of the drawable 18 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_19	HASH("EXEC_DRAW_19")	// Item is part of the drawable 19 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_20	HASH("EXEC_DRAW_20")	// Item is part of the drawable 20 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_21	HASH("EXEC_DRAW_21")	// Item is part of the drawable 21 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_22	HASH("EXEC_DRAW_22")	// Item is part of the drawable 22 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_23	HASH("EXEC_DRAW_23")	// Item is part of the drawable 23 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_24	HASH("EXEC_DRAW_24")	// Item is part of the drawable 24 group for executive data
CONST_INT DLC_RESTRICTION_TAG_EXEC_DRAW_25	HASH("EXEC_DRAW_25")	// Item is part of the drawable 25 group for executive data

CONST_INT DLC_RESTRICTION_TAG_COWBOY_BOOTS	HASH("COWBOY_BOOTS")		// Item is a pair of cowboy boots
CONST_INT DLC_RESTRICTION_TAG_ALT_FEET		HASH("ALT_FEET")			// Item is the alt feet variant
CONST_INT DLC_RESTRICTION_TAG_SWEAT_VEST	HASH("SWEAT_VEST")			// Item is used for the sweater vest and shirt combo
CONST_INT DLC_RESTRICTION_TAG_BEAD_NECKLACE	HASH("BEAD_NECKLACE")		// Item is a large bead necklace for use with the sweater vests
CONST_INT DLC_RESTRICTION_TAG_DESERT_SCARF	HASH("DESERT_SCARF")		// Item is a desert scarf from apartment pack (male)
CONST_INT DLC_RESTRICTION_TAG_SKINNY		HASH("SKINNY")				// Item is skinny variant
CONST_INT DLC_RESTRICTION_TAG_ALT_HELMET	HASH("ALT_HELMET")			// Item is an alternate of the helmet (visor down?)



CONST_INT DLC_RESTRICTION_TAG_STUNT_CLOTH	HASH("STUNT_CLOTH")	// Item is part of the stunt clothing for outfit checks

CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_0	HASH("STUNT_DRAW_0")	// Item is part of the drawable 0 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_1	HASH("STUNT_DRAW_1")	// Item is part of the drawable 1 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_2	HASH("STUNT_DRAW_2")	// Item is part of the drawable 2 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_3	HASH("STUNT_DRAW_3")	// Item is part of the drawable 3 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_4	HASH("STUNT_DRAW_4")	// Item is part of the drawable 4 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_5	HASH("STUNT_DRAW_5")	// Item is part of the drawable 5 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_6	HASH("STUNT_DRAW_6")	// Item is part of the drawable 6 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_7	HASH("STUNT_DRAW_7")	// Item is part of the drawable 7 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_8	HASH("STUNT_DRAW_8")	// Item is part of the drawable 8 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_9	HASH("STUNT_DRAW_9")	// Item is part of the drawable 9 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_10	HASH("STUNT_DRAW_10")	// Item is part of the drawable 10 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_11	HASH("STUNT_DRAW_11")	// Item is part of the drawable 11 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_12	HASH("STUNT_DRAW_12")	// Item is part of the drawable 12 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_13	HASH("STUNT_DRAW_13")	// Item is part of the drawable 13 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_14	HASH("STUNT_DRAW_14")	// Item is part of the drawable 14 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_15	HASH("STUNT_DRAW_15")	// Item is part of the drawable 15 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_16	HASH("STUNT_DRAW_16")	// Item is part of the drawable 16 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_17	HASH("STUNT_DRAW_17")	// Item is part of the drawable 17 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_18	HASH("STUNT_DRAW_18")	// Item is part of the drawable 18 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_19	HASH("STUNT_DRAW_19")	// Item is part of the drawable 19 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_20	HASH("STUNT_DRAW_20")	// Item is part of the drawable 20 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_21	HASH("STUNT_DRAW_21")	// Item is part of the drawable 21 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_22	HASH("STUNT_DRAW_22")	// Item is part of the drawable 22 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_23	HASH("STUNT_DRAW_23")	// Item is part of the drawable 23 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_24	HASH("STUNT_DRAW_24")	// Item is part of the drawable 24 group for stunt data
CONST_INT DLC_RESTRICTION_TAG_STUNT_DRAW_25	HASH("STUNT_DRAW_25")	// Item is part of the drawable 25 group for stunt data




CONST_INT DLC_RESTRICTION_TAG_BIKER_CLOTH	HASH("BIKER_CLOTH")		// Item is part of the biker clothing for outfit checks

CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_0	HASH("BIKER_DRAW_0")	// Item is part of the drawable 0 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_1	HASH("BIKER_DRAW_1")	// Item is part of the drawable 1 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_2	HASH("BIKER_DRAW_2")	// Item is part of the drawable 2 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_3	HASH("BIKER_DRAW_3")	// Item is part of the drawable 3 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_4	HASH("BIKER_DRAW_4")	// Item is part of the drawable 4 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_5	HASH("BIKER_DRAW_5")	// Item is part of the drawable 5 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_6	HASH("BIKER_DRAW_6")	// Item is part of the drawable 6 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_7	HASH("BIKER_DRAW_7")	// Item is part of the drawable 7 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_8	HASH("BIKER_DRAW_8")	// Item is part of the drawable 8 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_9	HASH("BIKER_DRAW_9")	// Item is part of the drawable 9 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_10	HASH("BIKER_DRAW_10")	// Item is part of the drawable 10 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_11	HASH("BIKER_DRAW_11")	// Item is part of the drawable 11 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_12	HASH("BIKER_DRAW_12")	// Item is part of the drawable 12 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_13	HASH("BIKER_DRAW_13")	// Item is part of the drawable 13 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_14	HASH("BIKER_DRAW_14")	// Item is part of the drawable 14 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_15	HASH("BIKER_DRAW_15")	// Item is part of the drawable 15 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_16	HASH("BIKER_DRAW_16")	// Item is part of the drawable 16 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_17	HASH("BIKER_DRAW_17")	// Item is part of the drawable 17 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_18	HASH("BIKER_DRAW_18")	// Item is part of the drawable 18 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_19	HASH("BIKER_DRAW_19")	// Item is part of the drawable 19 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_20	HASH("BIKER_DRAW_20")	// Item is part of the drawable 20 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_21	HASH("BIKER_DRAW_21")	// Item is part of the drawable 21 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_22	HASH("BIKER_DRAW_22")	// Item is part of the drawable 22 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_23	HASH("BIKER_DRAW_23")	// Item is part of the drawable 23 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_24	HASH("BIKER_DRAW_24")	// Item is part of the drawable 24 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_25	HASH("BIKER_DRAW_25")	// Item is part of the drawable 25 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_26	HASH("BIKER_DRAW_26")	// Item is part of the drawable 26 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_27	HASH("BIKER_DRAW_27")	// Item is part of the drawable 27 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_28	HASH("BIKER_DRAW_28")	// Item is part of the drawable 28 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_29	HASH("BIKER_DRAW_29")	// Item is part of the drawable 29 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_30	HASH("BIKER_DRAW_30")	// Item is part of the drawable 30 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_31	HASH("BIKER_DRAW_31")	// Item is part of the drawable 31 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_32	HASH("BIKER_DRAW_32")	// Item is part of the drawable 32 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_33	HASH("BIKER_DRAW_33")	// Item is part of the drawable 33 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_34	HASH("BIKER_DRAW_34")	// Item is part of the drawable 34 group for biker data
CONST_INT DLC_RESTRICTION_TAG_BIKER_DRAW_35	HASH("BIKER_DRAW_35")	// Item is part of the drawable 35 group for biker data

CONST_INT DLC_RESTRICTION_TAG_BIKER_SKULL	HASH("BIKER_SKULL")		// Item is biker skull cap helmet

CONST_INT DLC_RESTRICTION_TAG_BIKER_VEST	HASH("BIKER_VEST")			// Item is biker vest (waistocat)
CONST_INT DLC_RESTRICTION_TAG_ALT_LEGS		HASH("ALT_LEGS")			// Item is the alt legs variant
CONST_INT DLC_RESTRICTION_TAG_HAS_ALT_VERSION 	HASH("HAS_ALT_VERSION")	// Item has an alt version
CONST_INT DLC_RESTRICTION_TAG_BACK_CREW_LOGO	HASH("BACK_CREW_LOGO")	// Item is a crew logo on the players back

CONST_INT DLC_RESTRICTION_TAG_DEADLINE_OUTFIT	HASH("DEADLINE_OUTFIT")		// Item is part of the deadline outfit
CONST_INT DLC_RESTRICTION_TAG_OVERCOAT_ACCS		HASH("OVERCOAT_ACCS")		// Item is a jacket that goes under an overcoat
CONST_INT DLC_RESTRICTION_TAG_OVERCOAT_JBIB		HASH("OVERCOAT_JBIB")		// Item can have under jackets

CONST_INT DLC_RESTRICTION_TAG_USE_JACKET_SCARF 	HASH("USE_JACKET_SCARF")	// Item works with the jacket version of scarfs
CONST_INT DLC_RESTRICTION_TAG_JUGG_SUIT 		HASH("JUGG_SUIT")			// Item is part of the juggernaut suit
CONST_INT DLC_RESTRICTION_TAG_FITTED_HOOD 		HASH("FITTED_HOOD")			// Item has a tight fitting hood
CONST_INT DLC_RESTRICTION_TAG_HOOD_HAT 			HASH("HOOD_HAT")			// Item has a hat for hoodies
CONST_INT DLC_RESTRICTION_TAG_BACKWARDS_HAT 	HASH("BACKWARDS_HAT")		// Item has a backwards hat
CONST_INT DLC_RESTRICTION_TAG_FAKE_HAT 			HASH("FAKE_HAT")			// Item has a hat for hoodies
CONST_INT DLC_RESTRICTION_TAG_MORPH_SUIT		HASH("MORPH_SUIT")			// 
CONST_INT DLC_RESTRICTION_TAG_LIGHT_UP			HASH("LIGHT_UP")			// 
CONST_INT DLC_RESTRICTION_TAG_SKI_MASK			HASH("SKI_MASK")			// Item is a half face mask that should be restricted for some items

CONST_INT DLC_RESTRICTION_TAG_IE_CLOTH			HASH("IE_CLOTH")		// Item is part of the ie clothing for outfit checks

CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_0			HASH("IE_DRAW_0")	// Item is part of the drawable 0 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_1			HASH("IE_DRAW_1")	// Item is part of the drawable 1 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_2			HASH("IE_DRAW_2")	// Item is part of the drawable 2 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_3			HASH("IE_DRAW_3")	// Item is part of the drawable 3 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_4			HASH("IE_DRAW_4")	// Item is part of the drawable 4 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_5			HASH("IE_DRAW_5")	// Item is part of the drawable 5 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_6			HASH("IE_DRAW_6")	// Item is part of the drawable 6 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_7			HASH("IE_DRAW_7")	// Item is part of the drawable 7 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_8			HASH("IE_DRAW_8")	// Item is part of the drawable 8 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_9			HASH("IE_DRAW_9")	// Item is part of the drawable 9 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_10		HASH("IE_DRAW_10")	// Item is part of the drawable 10 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_11		HASH("IE_DRAW_11")	// Item is part of the drawable 11 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_12		HASH("IE_DRAW_12")	// Item is part of the drawable 12 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_13		HASH("IE_DRAW_13")	// Item is part of the drawable 13 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_14		HASH("IE_DRAW_14")	// Item is part of the drawable 14 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_15		HASH("IE_DRAW_15")	// Item is part of the drawable 15 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_16		HASH("IE_DRAW_16")	// Item is part of the drawable 16 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_17		HASH("IE_DRAW_17")	// Item is part of the drawable 17 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_18		HASH("IE_DRAW_18")	// Item is part of the drawable 18 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_19		HASH("IE_DRAW_19")	// Item is part of the drawable 19 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_20		HASH("IE_DRAW_20")	// Item is part of the drawable 20 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_21		HASH("IE_DRAW_21")	// Item is part of the drawable 21 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_22		HASH("IE_DRAW_22")	// Item is part of the drawable 22 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_23		HASH("IE_DRAW_23")	// Item is part of the drawable 23 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_24		HASH("IE_DRAW_24")	// Item is part of the drawable 24 group for ie data
CONST_INT DLC_RESTRICTION_TAG_IE_DRAW_25		HASH("IE_DRAW_25")	// Item is part of the drawable 25 group for ie data


CONST_INT DLC_RESTRICTION_TAG_GUN_CLOTH		HASH("GUN_CLOTH")	// Item is part of the gunrunning clothing for outfit checks

CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_0	HASH("GUN_DRAW_0")	// Item is part of the drawable 0 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_1	HASH("GUN_DRAW_1")	// Item is part of the drawable 1 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_2	HASH("GUN_DRAW_2")	// Item is part of the drawable 2 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_3	HASH("GUN_DRAW_3")	// Item is part of the drawable 3 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_4	HASH("GUN_DRAW_4")	// Item is part of the drawable 4 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_5	HASH("GUN_DRAW_5")	// Item is part of the drawable 5 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_6	HASH("GUN_DRAW_6")	// Item is part of the drawable 6 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_7	HASH("GUN_DRAW_7")	// Item is part of the drawable 7 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_8	HASH("GUN_DRAW_8")	// Item is part of the drawable 8 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_9	HASH("GUN_DRAW_9")	// Item is part of the drawable 9 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_10	HASH("GUN_DRAW_10")	// Item is part of the drawable 10 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_11	HASH("GUN_DRAW_11")	// Item is part of the drawable 11 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_12	HASH("GUN_DRAW_12")	// Item is part of the drawable 12 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_13	HASH("GUN_DRAW_13")	// Item is part of the drawable 13 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_14	HASH("GUN_DRAW_14")	// Item is part of the drawable 14 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_15	HASH("GUN_DRAW_15")	// Item is part of the drawable 15 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_16	HASH("GUN_DRAW_16")	// Item is part of the drawable 16 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_17	HASH("GUN_DRAW_17")	// Item is part of the drawable 17 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_18	HASH("GUN_DRAW_18")	// Item is part of the drawable 18 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_19	HASH("GUN_DRAW_19")	// Item is part of the drawable 19 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_20	HASH("GUN_DRAW_20")	// Item is part of the drawable 20 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_21	HASH("GUN_DRAW_21")	// Item is part of the drawable 21 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_22	HASH("GUN_DRAW_22")	// Item is part of the drawable 22 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_23	HASH("GUN_DRAW_23")	// Item is part of the drawable 23 group for gunrunning data
CONST_INT DLC_RESTRICTION_TAG_GUN_DRAW_24	HASH("GUN_DRAW_24")	// Item is part of the drawable 24 group for gunrunning data

CONST_INT DLC_RESTRICTION_TAG_DUNGAREES		HASH("DUNGAREES")	// Item is pair of dungarees



CONST_INT DLC_RESTRICTION_TAG_AIR_CLOTH	HASH("AIR_CLOTH")	// Item is part of the air race clothing for outfit checks

CONST_INT DLC_RESTRICTION_TAG_AIR_DRAW_0	HASH("AIR_DRAW_0")	// Item is part of the drawable 0 group for air race data
CONST_INT DLC_RESTRICTION_TAG_AIR_DRAW_1	HASH("AIR_DRAW_1")	// Item is part of the drawable 1 group for air race data
CONST_INT DLC_RESTRICTION_TAG_AIR_DRAW_2	HASH("AIR_DRAW_2")	// Item is part of the drawable 2 group for air race data
CONST_INT DLC_RESTRICTION_TAG_AIR_DRAW_3	HASH("AIR_DRAW_3")	// Item is part of the drawable 3 group for air race data



CONST_INT DLC_RESTRICTION_TAG_SMUG_CLOTH		HASH("SMUG_CLOTH")	// Item is part of the smuggler clothing for outfit checks

CONST_INT DLC_RESTRICTION_TAG_SMUG_DRAW_0		HASH("SMUG_DRAW_0")	// Item is part of the drawable 0 group for smuggler data
CONST_INT DLC_RESTRICTION_TAG_SMUG_DRAW_1		HASH("SMUG_DRAW_1")	// Item is part of the drawable 1 group for smuggler data
CONST_INT DLC_RESTRICTION_TAG_SMUG_DRAW_2		HASH("SMUG_DRAW_2")	// Item is part of the drawable 2 group for smuggler data
CONST_INT DLC_RESTRICTION_TAG_SMUG_DRAW_3		HASH("SMUG_DRAW_3")	// Item is part of the drawable 3 group for smuggler data
CONST_INT DLC_RESTRICTION_TAG_SMUG_DRAW_4		HASH("SMUG_DRAW_4")	// Item is part of the drawable 4 group for smuggler data
CONST_INT DLC_RESTRICTION_TAG_SMUG_DRAW_5		HASH("SMUG_DRAW_5")	// Item is part of the drawable 5 group for smuggler data
CONST_INT DLC_RESTRICTION_TAG_SMUG_DRAW_6		HASH("SMUG_DRAW_6")	// Item is part of the drawable 6 group for smuggler data

CONST_INT DLC_RESTRICTION_TAG_BLOCK_EARRINGS 	HASH("BLOCK_EARRINGS")
CONST_INT DLC_RESTRICTION_TAG_HEADSCARF 		HASH("HEADSCARF")
CONST_INT DLC_RESTRICTION_TAG_BLOCK_GLASSES		HASH("BLOCK_GLASSES")
CONST_INT DLC_RESTRICTION_TAG_HAIR_SHRINK 		HASH("HAIR_SHRINK")
CONST_INT DLC_RESTRICTION_TAG_SHRINK_HAIR 		HASH("SHRINK_HAIR")
CONST_INT DLC_RESTRICTION_TAG_THERMAL_VISION	HASH("THERMAL_VISION")	// Item is thermal vision goggles

CONST_INT DLC_RESTRICTION_TAG_SCRUFFY_BALACLAVA 	HASH("SCRUFFY_BALACLAVA")

CONST_INT DLC_RESTRICTION_TAG_CAT_SUIT			HASH("CAT_SUIT")
CONST_INT DLC_RESTRICTION_TAG_GORKA_SUIT		HASH("GORKA_SUIT")
CONST_INT DLC_RESTRICTION_TAG_SCUBA_GEAR		HASH("SCUBA_GEAR")
CONST_INT DLC_RESTRICTION_TAG_SCUBA_SOCKS		HASH("SCUBA_SOCKS")
CONST_INT DLC_RESTRICTION_TAG_SCUBA_FLIPPERS	HASH("SCUBA_FLIPPERS")
CONST_INT DLC_RESTRICTION_TAG_ACCS_DECL			HASH("ACCS_DECL")

CONST_INT DLC_RESTRICTION_TAG_X17_CLOTH		HASH("X17_CLOTH")	// Item is part of the x17 clothing for outfit checks

CONST_INT DLC_RESTRICTION_TAG_X17_DRAW_0		HASH("X17_DRAW_0")	// Item is part of the drawable 0 group for x17 data
CONST_INT DLC_RESTRICTION_TAG_X17_DRAW_1		HASH("X17_DRAW_1")	// Item is part of the drawable 1 group for x17 data
CONST_INT DLC_RESTRICTION_TAG_X17_DRAW_2		HASH("X17_DRAW_2")	// Item is part of the drawable 2 group for x17 data
CONST_INT DLC_RESTRICTION_TAG_X17_DRAW_3		HASH("X17_DRAW_3")	// Item is part of the drawable 3 group for x17 data
CONST_INT DLC_RESTRICTION_TAG_X17_DRAW_4		HASH("X17_DRAW_4")	// Item is part of the drawable 4 group for x17 data
CONST_INT DLC_RESTRICTION_TAG_X17_DRAW_5		HASH("X17_DRAW_5")	// Item is part of the drawable 5 group for x17 data
CONST_INT DLC_RESTRICTION_TAG_X17_DRAW_6		HASH("X17_DRAW_6")	// Item is part of the drawable 6 group for x17 data
CONST_INT DLC_RESTRICTION_TAG_X17_DRAW_7		HASH("X17_DRAW_7")	// Item is part of the drawable 7 group for x17 data
CONST_INT DLC_RESTRICTION_TAG_X17_DRAW_8		HASH("X17_DRAW_8")	// Item is part of the drawable 8 group for x17 data
CONST_INT DLC_RESTRICTION_TAG_X17_DRAW_9		HASH("X17_DRAW_9")	// Item is part of the drawable 9 group for x17 data
CONST_INT DLC_RESTRICTION_TAG_X17_DRAW_10		HASH("X17_DRAW_10")	// Item is part of the drawable 10 group for x17 data
CONST_INT DLC_RESTRICTION_TAG_X17_DRAW_11		HASH("X17_DRAW_11")	// Item is part of the drawable 11 group for x17 data
CONST_INT DLC_RESTRICTION_TAG_X17_DRAW_12		HASH("X17_DRAW_12")	// Item is part of the drawable 11 group for x17 data
CONST_INT DLC_RESTRICTION_TAG_X17_DRAW_13		HASH("X17_DRAW_13")	// Item is part of the drawable 11 group for x17 data


CONST_INT DLC_RESTRICTION_TAG_ASSAULT_CLOTH		HASH("ASSAULT_CLOTH")	// Item is part of the Assault Course clothing for outfit checks
CONST_INT DLC_RESTRICTION_TAG_ASSAULT_DRAW_0	HASH("ASSAULT_DRAW_0")	// Item is part of the drawable 0 group for ASsault Course data

CONST_INT DLC_RESTRICTION_TAG_BATTLE_DRAW_0		HASH("BATTLE_DRAW_0")		// Item is part of the drawable 0 group for Business Battle data
CONST_INT DLC_RESTRICTION_TAG_BATTLE_DRAW_1		HASH("BATTLE_DRAW_1")		// Item is part of the drawable 1 group for Business Battle data
CONST_INT DLC_RESTRICTION_TAG_BATTLE_DRAW_2		HASH("BATTLE_DRAW_2")		// Item is part of the drawable 2 group for Business Battle data


CONST_INT DLC_RESTRICTION_TAG_ARENA_CLOTH		HASH("ARENA_CLOTH")	// Item is part of the Arena War clothing for outfit checks

CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_0		HASH("ARENA_DRAW_0")	// Item is part of the drawable 0 group for Arena War data
CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_1		HASH("ARENA_DRAW_1")	// Item is part of the drawable 1 group for Arena War data
CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_2		HASH("ARENA_DRAW_2")	// Item is part of the drawable 2 group for Arena War data
CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_3		HASH("ARENA_DRAW_3")	// Item is part of the drawable 3 group for Arena War data
CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_4		HASH("ARENA_DRAW_4")	// Item is part of the drawable 4 group for Arena War data
CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_5		HASH("ARENA_DRAW_5")	// Item is part of the drawable 5 group for Arena War data
CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_6		HASH("ARENA_DRAW_6")	// Item is part of the drawable 6 group for Arena War data
CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_7		HASH("ARENA_DRAW_7")	// Item is part of the drawable 7 group for Arena War data
CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_8		HASH("ARENA_DRAW_8")	// Item is part of the drawable 8 group for Arena War data
CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_9		HASH("ARENA_DRAW_9")	// Item is part of the drawable 9 group for Arena War data
CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_10		HASH("ARENA_DRAW_10")	// Item is part of the drawable 10 group for Arena War data
CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_11		HASH("ARENA_DRAW_11")	// Item is part of the drawable 11 group for Arena War data
CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_12		HASH("ARENA_DRAW_12")	// Item is part of the drawable 12 group for Arena War data
CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_13		HASH("ARENA_DRAW_13")	// Item is part of the drawable 13 group for Arena War data
CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_14		HASH("ARENA_DRAW_14")	// Item is part of the drawable 14 group for Arena War data
CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_15		HASH("ARENA_DRAW_15")	// Item is part of the drawable 15 group for Arena War data
CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_16		HASH("ARENA_DRAW_16")	// Item is part of the drawable 16 group for Arena War data
CONST_INT DLC_RESTRICTION_TAG_ARENA_DRAW_17		HASH("ARENA_DRAW_17")	// Item is part of the drawable 17 group for Arena War data

CONST_INT DLC_RESTRICTION_TAG_BLOCK_PARACHUTE	HASH("BLOCK_PARACHUTE")	// Item blocks the selection of chutes in the PI Menu

CONST_INT DLC_RESTRICTION_TAG_EPSILON_CHAIN		HASH("EPSILON_CHAIN")		// Item is the epsilon easter egg chain
CONST_INT DLC_RESTRICTION_TAG_EPSILON_CHAIN_ALT	HASH("EPSILON_CHAIN_ALT")	// Item is the epsilon easter egg chain


CONST_INT DLC_RESTRICTION_TAG_HIGH_ROLLER_TIE	HASH("HIGH_ROLLER_TIE")		// Item is a high roller tie
CONST_INT DLC_RESTRICTION_TAG_HIGH_ROLLER_SHIRT	HASH("HIGH_ROLLER_SHIRT")	// Item is a high roller shirt

CONST_INT DLC_RESTRICTION_TAG_CASINO_ITEM		HASH("CASINO_ITEM")	// Item is part of the Vinewood Casino clothing shop

CONST_INT DLC_RESTRICTION_TAG_VWD_CLOTH			HASH("VWD_CLOTH")	// Item is part of the Vinewood Casino clothing for outfit checks

CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_0		HASH("VWD_DRAW_0")	// Item is part of the drawable 0 group for Vinewood Casino data
CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_1		HASH("VWD_DRAW_1")	// Item is part of the drawable 1 group for Vinewood Casino data
CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_2		HASH("VWD_DRAW_2")	// Item is part of the drawable 2 group for Vinewood Casino data
CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_3		HASH("VWD_DRAW_3")	// Item is part of the drawable 3 group for Vinewood Casino data
CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_4		HASH("VWD_DRAW_4")	// Item is part of the drawable 4 group for Vinewood Casino data
CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_5		HASH("VWD_DRAW_5")	// Item is part of the drawable 5 group for Vinewood Casino data
CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_6		HASH("VWD_DRAW_6")	// Item is part of the drawable 6 group for Vinewood Casino data
CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_7		HASH("VWD_DRAW_7")	// Item is part of the drawable 7 group for Vinewood Casino data
CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_8		HASH("VWD_DRAW_8")	// Item is part of the drawable 8 group for Vinewood Casino data
CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_9		HASH("VWD_DRAW_9")	// Item is part of the drawable 9 group for Vinewood Casino data
CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_10		HASH("VWD_DRAW_10")	// Item is part of the drawable 10 group for Vinewood Casino data
CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_11		HASH("VWD_DRAW_11")	// Item is part of the drawable 11 group for Vinewood Casino data
CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_12		HASH("VWD_DRAW_12")	// Item is part of the drawable 12 group for Vinewood Casino data
CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_13		HASH("VWD_DRAW_13")	// Item is part of the drawable 13 group for Vinewood Casino data
CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_14		HASH("VWD_DRAW_14")	// Item is part of the drawable 14 group for Vinewood Casino data
CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_15		HASH("VWD_DRAW_15")	// Item is part of the drawable 15 group for Vinewood Casino data
CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_16		HASH("VWD_DRAW_16")	// Item is part of the drawable 16 group for Vinewood Casino data
CONST_INT DLC_RESTRICTION_TAG_VWD_DRAW_17		HASH("VWD_DRAW_17")	// Item is part of the drawable 17 group for Vinewood Casino data


CONST_INT DLC_RESTRICTION_TAG_H3_CLOTH			HASH("H3_CLOTH")	// Item is part of the Casino Heist clothing for outfit checks

CONST_INT DLC_RESTRICTION_TAG_H3_DRAW_0		HASH("H3_DRAW_0")	// Item is part of the drawable 0 group for Casino Heist data
CONST_INT DLC_RESTRICTION_TAG_H3_DRAW_1		HASH("H3_DRAW_1")	// Item is part of the drawable 1 group for Casino Heist data
CONST_INT DLC_RESTRICTION_TAG_H3_DRAW_2		HASH("H3_DRAW_2")	// Item is part of the drawable 2 group for Casino Heist data
CONST_INT DLC_RESTRICTION_TAG_H3_DRAW_3		HASH("H3_DRAW_3")	// Item is part of the drawable 3 group for Casino Heist data
CONST_INT DLC_RESTRICTION_TAG_H3_DRAW_4		HASH("H3_DRAW_4")	// Item is part of the drawable 4 group for Casino Heist data
CONST_INT DLC_RESTRICTION_TAG_H3_DRAW_5		HASH("H3_DRAW_5")	// Item is part of the drawable 5 group for Casino Heist data
CONST_INT DLC_RESTRICTION_TAG_H3_DRAW_6		HASH("H3_DRAW_6")	// Item is part of the drawable 6 group for Casino Heist data
CONST_INT DLC_RESTRICTION_TAG_H3_DRAW_7		HASH("H3_DRAW_7")	// Item is part of the drawable 7 group for Casino Heist data
CONST_INT DLC_RESTRICTION_TAG_H3_DRAW_8		HASH("H3_DRAW_8")	// Item is part of the drawable 8 group for Casino Heist data
CONST_INT DLC_RESTRICTION_TAG_H3_DRAW_9		HASH("H3_DRAW_9")	// Item is part of the drawable 9 group for Casino Heist data

CONST_INT DLC_RESTRICTION_TAG_OPEN_SHORT		HASH("OPEN_SHORT")	// Item is open shirt with short sleeves

CONST_INT DLC_RESTRICTION_TAG_KNEE_HIGH			HASH("KNEE_HIGH")			// Item is a pair of knee high boots
CONST_INT DLC_RESTRICTION_TAG_KNEE_HIGH_COMPAT	HASH("KNEE_HIGH_COMPAT")	// Item is compatible with the knee high boots

CONST_INT DLC_RESTRICTION_TAG_REMOVE_WITH_JBIB		HASH("REMOVE_WITH_JBIB")	// Item should be removed when jbib changes
CONST_INT DLC_RESTRICTION_TAG_REMOVE_WITH_SPECIAL	HASH("REMOVE_WITH_SPECIAL")	// Item should be removed when special changes
CONST_INT DLC_RESTRICTION_TAG_REMOVE_WITH_LEGS		HASH("REMOVE_WITH_LEGS")	// Item should be removed when legs changes

CONST_INT DLC_RESTRICTION_TAG_TUCKED				HASH("TUCKED")	// Item is a tucked top not compaitble with some pants

CONST_INT DLC_RESTRICTION_TAG_H4_CLOTH			HASH("H4_CLOTH")	// Item is part of the Island Heist clothing for outfit checks

CONST_INT DLC_RESTRICTION_TAG_H4_DRAW_0		HASH("H4_DRAW_0")	// Item is part of the drawable 0 group for Island Heist data
CONST_INT DLC_RESTRICTION_TAG_H4_DRAW_1		HASH("H4_DRAW_1")	// Item is part of the drawable 1 group for Island Heist data
CONST_INT DLC_RESTRICTION_TAG_H4_DRAW_2		HASH("H4_DRAW_2")	// Item is part of the drawable 2 group for Island Heist data
CONST_INT DLC_RESTRICTION_TAG_H4_DRAW_3		HASH("H4_DRAW_3")	// Item is part of the drawable 3 group for Island Heist data
CONST_INT DLC_RESTRICTION_TAG_H4_DRAW_4		HASH("H4_DRAW_4")	// Item is part of the drawable 4 group for Island Heist data
CONST_INT DLC_RESTRICTION_TAG_H4_DRAW_5		HASH("H4_DRAW_5")	// Item is part of the drawable 5 group for Island Heist data
CONST_INT DLC_RESTRICTION_TAG_H4_DRAW_6		HASH("H4_DRAW_6")	// Item is part of the drawable 6 group for Island Heist data
CONST_INT DLC_RESTRICTION_TAG_H4_DRAW_7		HASH("H4_DRAW_7")	// Item is part of the drawable 7 group for Island Heist data
CONST_INT DLC_RESTRICTION_TAG_H4_DRAW_8		HASH("H4_DRAW_8")	// Item is part of the drawable 8 group for Island Heist data
CONST_INT DLC_RESTRICTION_TAG_H4_DRAW_9		HASH("H4_DRAW_9")	// Item is part of the drawable 9 group for Island Heist data

CONST_INT DLC_RESTRICTION_TAG_HOOP_NECKLACE		HASH("HOOP_NECKLACE")	// Item is a large hooped glow necklace

CONST_INT DLC_RESTRICTION_TAG_BACK_LOGO	HASH("BACK_LOGO")	// Item is a logo on the players back

#IF FEATURE_TUNER
CONST_INT DLC_RESTRICTION_TAG_TR_CLOTH			HASH("TR_CLOTH")	// Item is part of the Tuner clothing for outfit checks

CONST_INT DLC_RESTRICTION_TAG_TR_DRAW_0		HASH("TR_DRAW_0")	// Item is part of the drawable 0 group for Tuner data
CONST_INT DLC_RESTRICTION_TAG_TR_DRAW_1		HASH("TR_DRAW_1")	// Item is part of the drawable 1 group for Tuner data
CONST_INT DLC_RESTRICTION_TAG_TR_DRAW_2		HASH("TR_DRAW_2")	// Item is part of the drawable 2 group for Tuner data
CONST_INT DLC_RESTRICTION_TAG_TR_DRAW_3		HASH("TR_DRAW_3")	// Item is part of the drawable 3 group for Tuner data
CONST_INT DLC_RESTRICTION_TAG_TR_DRAW_4		HASH("TR_DRAW_4")	// Item is part of the drawable 4 group for Tuner data
CONST_INT DLC_RESTRICTION_TAG_TR_DRAW_5		HASH("TR_DRAW_5")	// Item is part of the drawable 5 group for Tuner data
CONST_INT DLC_RESTRICTION_TAG_TR_DRAW_6		HASH("TR_DRAW_6")	// Item is part of the drawable 6 group for Tuner data
CONST_INT DLC_RESTRICTION_TAG_TR_DRAW_7		HASH("TR_DRAW_7")	// Item is part of the drawable 7 group for Tuner data
CONST_INT DLC_RESTRICTION_TAG_TR_DRAW_8		HASH("TR_DRAW_8")	// Item is part of the drawable 8 group for Tuner data
CONST_INT DLC_RESTRICTION_TAG_TR_DRAW_9		HASH("TR_DRAW_9")	// Item is part of the drawable 9 group for Tuner data

CONST_INT DLC_RESTRICTION_TAG_CAR_MEET_ITEM		HASH("CAR_MEET_ITEM")	// Item is part of the Car Meet clothing shop


CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_TUNER_0		HASH("CREW_LOGO_TUNER_0")	// Item is compatible with crew logo 0 from the Tuner data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_TUNER_1		HASH("CREW_LOGO_TUNER_1")	// Item is compatible with crew logo 1 from the Tuner data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_TUNER_2		HASH("CREW_LOGO_TUNER_2")	// Item is compatible with crew logo 2 from the Tuner data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_TUNER_3		HASH("CREW_LOGO_TUNER_3")	// Item is compatible with crew logo 3 from the Tuner data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_TUNER_4		HASH("CREW_LOGO_TUNER_4")	// Item is compatible with crew logo 4 from the Tuner data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_TUNER_5		HASH("CREW_LOGO_TUNER_5")	// Item is compatible with crew logo 5 from the Tuner data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_TUNER_6		HASH("CREW_LOGO_TUNER_6")	// Item is compatible with crew logo 6 from the Tuner data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_TUNER_7		HASH("CREW_LOGO_TUNER_7")	// Item is compatible with crew logo 7 from the Tuner data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_TUNER_36	HASH("CREW_LOGO_TUNER_36")	// Item is compatible with crew logo 36 from the Tuner data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_TUNER_37	HASH("CREW_LOGO_TUNER_37")	// Item is compatible with crew logo 37 from the Tuner data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_TUNER_38	HASH("CREW_LOGO_TUNER_38")	// Item is compatible with crew logo 38 from the Tuner data

CONST_INT DLC_RESTRICTION_TAG_DOCK_GLOVES	HASH("DOCK_GLOVES")	// Item is a pair of dock worker gloves

#ENDIF

#IF FEATURE_FIXER
CONST_INT DLC_RESTRICTION_TAG_FX_CLOTH			HASH("FX_CLOTH")	// Item is part of the Fixer clothing for outfit checks

CONST_INT DLC_RESTRICTION_TAG_FX_DRAW_0		HASH("FX_DRAW_0")	// Item is part of the drawable 0 group for Fixer data
CONST_INT DLC_RESTRICTION_TAG_FX_DRAW_1		HASH("FX_DRAW_1")	// Item is part of the drawable 1 group for Fixer data
CONST_INT DLC_RESTRICTION_TAG_FX_DRAW_2		HASH("FX_DRAW_2")	// Item is part of the drawable 2 group for Fixer data
CONST_INT DLC_RESTRICTION_TAG_FX_DRAW_3		HASH("FX_DRAW_3")	// Item is part of the drawable 3 group for Fixer data
CONST_INT DLC_RESTRICTION_TAG_FX_DRAW_4		HASH("FX_DRAW_4")	// Item is part of the drawable 4 group for Fixer data
CONST_INT DLC_RESTRICTION_TAG_FX_DRAW_5		HASH("FX_DRAW_5")	// Item is part of the drawable 5 group for Fixer data
CONST_INT DLC_RESTRICTION_TAG_FX_DRAW_6		HASH("FX_DRAW_6")	// Item is part of the drawable 6 group for Fixer data
CONST_INT DLC_RESTRICTION_TAG_FX_DRAW_7		HASH("FX_DRAW_7")	// Item is part of the drawable 7 group for Fixer data
CONST_INT DLC_RESTRICTION_TAG_FX_DRAW_8		HASH("FX_DRAW_8")	// Item is part of the drawable 8 group for Fixer data

CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_FIXER_0		HASH("CREW_LOGO_FIXER_0")	// Item is compatible with crew logo 0 from the Fixer data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_FIXER_1		HASH("CREW_LOGO_FIXER_1")	// Item is compatible with crew logo 1 from the Fixer data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_FIXER_2		HASH("CREW_LOGO_FIXER_2")	// Item is compatible with crew logo 2 from the Fixer data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_FIXER_3		HASH("CREW_LOGO_FIXER_3")	// Item is compatible with crew logo 3 from the Fixer data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_FIXER_4		HASH("CREW_LOGO_FIXER_4")	// Item is compatible with crew logo 4 from the Fixer data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_FIXER_5		HASH("CREW_LOGO_FIXER_5")	// Item is compatible with crew logo 5 from the Fixer data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_FIXER_6		HASH("CREW_LOGO_FIXER_6")	// Item is compatible with crew logo 6 from the Fixer data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_FIXER_7		HASH("CREW_LOGO_FIXER_7")	// Item is compatible with crew logo 7 from the Fixer data

CONST_INT DLC_RESTRICTION_TAG_GOLF_GLOVE			HASH("GOLF_GLOVE")	// Item is single golf glove

CONST_INT DLC_RESTRICTION_TAG_MUSIC_STUDIO_ITEM		HASH("MUSIC_STUDIO_ITEM")	// Item is part of the Music Studio Merch shop

CONST_INT DLC_RESTRICTION_TAG_VNECK_SWEAT			HASH("VNECK_SWEAT") // Item is a v-neck sweater

CONST_INT DLC_RESTRICTION_TAG_PARA_ALT01			HASH("PARA_ALT01")	// Item is uses parachute alt01 (GET_PARACHUTE_ALT_DRAWABLE_FOR_MP)
CONST_INT DLC_RESTRICTION_TAG_PARA_ALT02			HASH("PARA_ALT02")	// Item is uses parachute alt02 (GET_PARACHUTE_ALT_DRAWABLE_FOR_MP)
CONST_INT DLC_RESTRICTION_TAG_PARA_ALT03			HASH("PARA_ALT03")	// Item is uses parachute alt03 (GET_PARACHUTE_ALT_DRAWABLE_FOR_MP)
CONST_INT DLC_RESTRICTION_TAG_PARA_ALT04			HASH("PARA_ALT04")	// Item is uses parachute alt04 (GET_PARACHUTE_ALT_DRAWABLE_FOR_MP)
CONST_INT DLC_RESTRICTION_TAG_PARA_ALT05			HASH("PARA_ALT05")	// Item is uses parachute alt05 (GET_PARACHUTE_ALT_DRAWABLE_FOR_MP)
CONST_INT DLC_RESTRICTION_TAG_PARA_ALT06			HASH("PARA_ALT06")	// Item is uses parachute alt06 (GET_PARACHUTE_ALT_DRAWABLE_FOR_MP)
CONST_INT DLC_RESTRICTION_TAG_PARA_ALT07			HASH("PARA_ALT07")	// Item is uses parachute alt07 (GET_PARACHUTE_ALT_DRAWABLE_FOR_MP)
CONST_INT DLC_RESTRICTION_TAG_PARA_ALT08			HASH("PARA_ALT08")	// Item is uses parachute alt08 (GET_PARACHUTE_ALT_DRAWABLE_FOR_MP)
CONST_INT DLC_RESTRICTION_TAG_PARA_ALT09			HASH("PARA_ALT09")	// Item is uses parachute alt09 (GET_PARACHUTE_ALT_DRAWABLE_FOR_MP)
CONST_INT DLC_RESTRICTION_TAG_PARA_ALT10			HASH("PARA_ALT10")	// Item is uses parachute alt10 (GET_PARACHUTE_ALT_DRAWABLE_FOR_MP)
CONST_INT DLC_RESTRICTION_TAG_PARA_ALT11			HASH("PARA_ALT11")	// Item is uses parachute alt11 (GET_PARACHUTE_ALT_DRAWABLE_FOR_MP)

#ENDIF

#IF FEATURE_DLC_1_2022
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_SUM2_0		HASH("CREW_LOGO_SUM2_0")	// Item is compatible with crew logo 0 from the Summer 2022 data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_SUM2_1		HASH("CREW_LOGO_SUM2_1")	// Item is compatible with crew logo 1 from the Summer 2022 data
CONST_INT DLC_RESTRICTION_TAG_CREW_LOGO_SUM2_2		HASH("CREW_LOGO_SUM2_2")	// Item is compatible with crew logo 2 from the Summer 2022 data

CONST_INT DLC_RESTRICTION_TAG_SHORTS				HASH("SHORTS")	// Item is a pair of shorts
CONST_INT DLC_RESTRICTION_TAG_MINI_DRESS			HASH("MINI_DRESS") // Item is a mini dress
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////
///    Enums used to give ped components a more meaningful name
///    
ENUM PED_COMP_NAME_ENUM

	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///     																										///
	///    The ped component saved globals use an INT for each component type (torso, legs, outfit, props etc).		///
	///    This way we can have up to 31 component items per type as we just set the relevant BIT for each item.	///
	///    A double array is required, one for available items, the other for acquired items.						///
	///     																										///
	///    In order to minmimise issues with savegame data, text labels, loops etc. the component item enum must 	///
	///     be set up with a unique value and should not be changed.												///
	///     																										///
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	////////////////////////////////////////////////////////
	/// DUMMY
	DUMMY_PED_COMP 					= -99,
	
	OUTFIT_DEFAULT					= 0,
	
	PROPS_HEAD_NONE 				= 0,
	PROPS_EYES_NONE 				= 1,
	PROPS_EARS_NONE 				= 2,
	PROPS_MOUTH_NONE 				= 3,
	PROPS_LHAND_NONE 				= 4,
	PROPS_RHAND_NONE 				= 5,
	PROPS_LWRIST_NONE 				= 6,
	PROPS_RWRIST_NONE 				= 7,
	PROPS_HIP_NONE 					= 8,
	
	PROPGROUP_NONE 					= 31,
	
	
	////////////////////////////////////////////////////////
	/// MICHAEL
	HEAD_P0_0_0						=0,
	HEAD_P0_0_1						,
	HEAD_P0_0_2						,
	HEAD_P0_0_3						,
	HEAD_P0_0_4						,
	HEAD_P0_0_5						,
	HEAD_P0_0_6						,
	HEAD_P0_DLC						, // leave this at the bottom

	HAIR_P0_0_0						= 0,
	HAIR_P0_1_0						,
	HAIR_P0_2_0 					,
	HAIR_P0_3_0 					,
	HAIR_P0_4_0 					,
	HAIR_P0_5_0 					,
	HAIR_P0_DLC						,
	
	TORSO_P0_GREY_SUIT 				= 0,
	TORSO_P0_GREY_SUIT_01			,
	TORSO_P0_GREY_SUIT_02			,
	TORSO_P0_GREY_SUIT_03 			,
	TORSO_P0_GREY_SUIT_04			,
	TORSO_P0_GREY_SUIT_05 			,
	TORSO_P0_GREY_SUIT_06			,
	TORSO_P0_GREY_SUIT_07 			,
	TORSO_P0_GREY_SUIT_08 			,
	TORSO_P0_GREY_SUIT_09 			,
	TORSO_P0_GREY_SUIT_10 			,
	TORSO_P0_GREY_SUIT_11 			,
	TORSO_P0_GREY_SUIT_12 			,
	TORSO_P0_GREY_SUIT_13 			,
	TORSO_P0_GREY_SUIT_14 			,
	TORSO_P0_GREY_SUIT_15 			,
	TORSO_P0_FIREMAN 				,
	TORSO_P0_V_NECK_0				,
	TORSO_P0_V_NECK_1				,
	TORSO_P0_V_NECK_2				,
	TORSO_P0_V_NECK_3				,
	TORSO_P0_V_NECK_4				,
	TORSO_P0_V_NECK_5				,
	TORSO_P0_V_NECK_6				,
	TORSO_P0_V_NECK_7				,
	TORSO_P0_V_NECK_8				,
	TORSO_P0_V_NECK_9				,
	TORSO_P0_V_NECK_10				,
	TORSO_P0_V_NECK_11				,
	TORSO_P0_V_NECK_12				,
	TORSO_P0_V_NECK_13				,
	TORSO_P0_V_NECK_14				,
	TORSO_P0_V_NECK_15				,
	TORSO_P0_SCUBA					,
	TORSO_P0_SCUBA_BRIGHT			,
	TORSO_P0_STEALTH				,
	TORSO_P0_BALLISTICS 			,
	TORSO_P0_NAVY_COP				,
	TORSO_P0_SHIRT_AND_GILET_0		,
	TORSO_P0_SHIRT_AND_GILET_1		,
	TORSO_P0_SHIRT_AND_GILET_2		,
	TORSO_P0_SHIRT_AND_GILET_3		,
	TORSO_P0_SHIRT_AND_GILET_4		,
	TORSO_P0_SHIRT_AND_GILET_5		,
	TORSO_P0_OPEN_SHIRT				,
	TORSO_P0_OPEN_SHIRT_1			,
	TORSO_P0_OPEN_SHIRT_2			,
	TORSO_P0_OPEN_SHIRT_3			,
	TORSO_P0_OPEN_SHIRT_4			,
	TORSO_P0_OPEN_SHIRT_5			,
	TORSO_P0_OPEN_SHIRT_6			,
	TORSO_P0_OPEN_SHIRT_7			,
	TORSO_P0_OPEN_SHIRT_8			,
	TORSO_P0_OPEN_SHIRT_9			,
	TORSO_P0_OPEN_SHIRT_10			,
	TORSO_P0_OPEN_SHIRT_11			,
	TORSO_P0_OPEN_SHIRT_12			,
	TORSO_P0_OPEN_SHIRT_13			,
	TORSO_P0_OPEN_SHIRT_14			,
	TORSO_P0_OPEN_SHIRT_15			,
	TORSO_P0_NAVY_PARAMEDIC			,
	TORSO_P0_HIGHWAY_PATROL			,
	TORSO_P0_SECURITY				,
	TORSO_P0_EXTERMINATOR			,
	TORSO_P0_BOILER_SUIT_1			,
	TORSO_P0_BOILER_SUIT_2			,
	TORSO_P0_BOILER_SUIT_3			,
	TORSO_P0_BOILER_SUIT_4			,
	TORSO_P0_BOILER_SUIT_5			,
	TORSO_P0_TRIATHLON				,
	TORSO_P0_GILET_0				,
	TORSO_P0_GILET_1				,
	TORSO_P0_GILET_2				,
	TORSO_P0_GILET_3				,
	TORSO_P0_GILET_4				,
	TORSO_P0_GILET_5				,
	TORSO_P0_LUDENDORFF				,
	TORSO_P0_GOLF					,
	TORSO_P0_POLO_SHIRT_1			,
	TORSO_P0_POLO_SHIRT_2			,
	TORSO_P0_POLO_SHIRT_3			,
	TORSO_P0_POLO_SHIRT_4			,
	TORSO_P0_POLO_SHIRT_5			,
	TORSO_P0_POLO_SHIRT_6			,
	TORSO_P0_POLO_SHIRT_7			,
	TORSO_P0_YOGA_0					,
	TORSO_P0_YOGA_1					,
	TORSO_P0_YOGA_2					,
	TORSO_P0_YOGA_3					,
	TORSO_P0_YOGA_4					,
	TORSO_P0_YOGA_5					,
	TORSO_P0_MOTO_X					,
	TORSO_P0_BED					,
	TORSO_P0_VEST_1					,
	TORSO_P0_VEST_2					,
	TORSO_P0_VEST_3					,
	TORSO_P0_VEST_4					,
	TORSO_P0_DENIM_SHIRT_0			,
	TORSO_P0_DENIM_SHIRT_1			,
	TORSO_P0_DENIM_SHIRT_2			,
	TORSO_P0_DENIM_SHIRT_3			,
	TORSO_P0_DENIM_SHIRT_4			,
	TORSO_P0_DENIM_SHIRT_5			,
	TORSO_P0_DENIM_SHIRT_6			,
	TORSO_P0_DENIM_SHIRT_7			,
	TORSO_P0_DENIM_SHIRT_8			,
	TORSO_P0_DENIM_SHIRT_9			,
	TORSO_P0_DENIM_SHIRT_10			,
	TORSO_P0_DENIM_SHIRT_11			,
	TORSO_P0_DENIM_SHIRT_12			,
	TORSO_P0_DENIM_SHIRT_13			,
	TORSO_P0_DENIM_SHIRT_14			,
	TORSO_P0_DENIM_SHIRT_15			,
	TORSO_P0_HOODIE_0				,
	TORSO_P0_HOODIE_1				,
	TORSO_P0_HOODIE_2				,
	TORSO_P0_HOODIE_3				,
	TORSO_P0_HOODIE_4				,
	TORSO_P0_HOODIE_5				,
	TORSO_P0_HOODIE_6				,
	TORSO_P0_HOODIE_7				,
	TORSO_P0_HOODIE_8				,
	TORSO_P0_HOODIE_9				,
	TORSO_P0_HOODIE_10				,
	TORSO_P0_HOODIE_11				,
	TORSO_P0_HOODIE_12				,
	TORSO_P0_HOODIE_13				,
	TORSO_P0_HOODIE_14				,
	TORSO_P0_HOODIE_15				,
	TORSO_P0_LEATHER_JACKET_0		,
	TORSO_P0_LEATHER_JACKET_1		,
	TORSO_P0_LEATHER_JACKET_2		,
	TORSO_P0_LEATHER_JACKET_3		,
	TORSO_P0_LEATHER_JACKET_4		,
	TORSO_P0_LEATHER_JACKET_5		,
	TORSO_P0_SUIT_JACKET			,
	TORSO_P0_SUIT_JACKET_1			,
	TORSO_P0_SUIT_JACKET_2			,
	TORSO_P0_SUIT_JACKET_3			,
	TORSO_P0_SUIT_JACKET_4			,
	TORSO_P0_SUIT_JACKET_5			,
	TORSO_P0_SUIT_JACKET_6			,
	TORSO_P0_SUIT_JACKET_7			,
	TORSO_P0_SUIT_JACKET_8			,
	TORSO_P0_SUIT_JACKET_9			,
	TORSO_P0_SUIT_JACKET_10			,
	TORSO_P0_SUIT_JACKET_11			,
	TORSO_P0_SUIT_JACKET_12			,
	TORSO_P0_SUIT_JACKET_13			,
	TORSO_P0_SUIT_JACKET_14			,
	TORSO_P0_SUIT_JACKET_15			,
	TORSO_P0_VNECK					,
	TORSO_P0_VNECK_1				,
	TORSO_P0_VNECK_2				,
	TORSO_P0_VNECK_3				,
	TORSO_P0_VNECK_4				,
	TORSO_P0_VNECK_5				,
	TORSO_P0_VNECK_6				,
	TORSO_P0_VNECK_7				,
	TORSO_P0_VNECK_8				,
	TORSO_P0_VNECK_9				,
	TORSO_P0_DRESS_SHIRT			,
	TORSO_P0_DRESS_SHIRT_1			,
	TORSO_P0_DRESS_SHIRT_2			,
	TORSO_P0_DRESS_SHIRT_3			,
	TORSO_P0_DRESS_SHIRT_4			,
	TORSO_P0_DRESS_SHIRT_5			,
	TORSO_P0_DRESS_SHIRT_6			,
	TORSO_P0_DRESS_SHIRT_7			,
	TORSO_P0_BARE_CHEST				,
	TORSO_P0_EPSILON				,
	TORSO_P0_TENNIS					,
	TORSO_P0_TENNIS_1				,
	TORSO_P0_TENNIS_2				,
	TORSO_P0_MOVIE_TUXEDO			,
	TORSO_P0_HEAVY_JACKET			,
	TORSO_P0_HEAVY_JACKET_1			,
	TORSO_P0_CHECK_SHIRT_0			,
	TORSO_P0_CHECK_SHIRT_1			,
	TORSO_P0_CHECK_SHIRT_2			,
	TORSO_P0_CHECK_SHIRT_3			,
	TORSO_P0_DLC					,
	// If you add a new torso that has a high waist update DOES_TORSO_HAVE_HIGH_WAIST
	
	LEGS_P0_GREY_SUIT 				= 0,
	LEGS_P0_GREY_SUIT_1 			,
	LEGS_P0_GREY_SUIT_2 			,
	LEGS_P0_GREY_SUIT_3 			,
	LEGS_P0_GREY_SUIT_4 			,
	LEGS_P0_GREY_SUIT_5 			,
	LEGS_P0_GREY_SUIT_6 			,
	LEGS_P0_GREY_SUIT_7 			,
	LEGS_P0_GREY_SUIT_8 			,
	LEGS_P0_GREY_SUIT_9 			,
	LEGS_P0_GREY_SUIT_10 			,
	LEGS_P0_GREY_SUIT_11 			,
	LEGS_P0_GREY_SUIT_12 			,
	LEGS_P0_GREY_SUIT_13 			,
	LEGS_P0_GREY_SUIT_14 			,
	LEGS_P0_GREY_SUIT_15			,
	LEGS_P0_FIREMAN 				,
	LEGS_P0_NAVY_JANITOR  			,
	LEGS_P0_SCUBA			 		,
	LEGS_P0_SCUBA_BRIGHT		 	,
	LEGS_P0_STEALTH					,
	LEGS_P0_BALLISTICS 				,
	LEGS_P0_NAVY_COP 				,
	LEGS_P0_CHINOS_0 				,
	LEGS_P0_CHINOS_1 				,
	LEGS_P0_CHINOS_2 				,
	LEGS_P0_CHINOS_3 				,
	LEGS_P0_CHINOS_4 				,
	LEGS_P0_CHINOS_5 				,
	LEGS_P0_CHINOS_6 				,
	LEGS_P0_CHINOS_7 				,
	LEGS_P0_NAVY_PARAMEDIC			,
	LEGS_P0_HIGHWAY_PATROL			,
	LEGS_P0_SECURITY				,
	LEGS_P0_EXTERMINATOR			,
	LEGS_P0_BOILER_SUIT_1			,
	LEGS_P0_BOILER_SUIT_2			,
	LEGS_P0_BOILER_SUIT_3			,
	LEGS_P0_BOILER_SUIT_4			,
	LEGS_P0_BOILER_SUIT_5			,
	LEGS_P0_TRIATHLON				,
	LEGS_P0_CARGO_SHORTS_0			,
	LEGS_P0_CARGO_SHORTS_1			,
	LEGS_P0_CARGO_SHORTS_2			,		
	LEGS_P0_CARGO_SHORTS_3			,		
	LEGS_P0_CARGO_SHORTS_4			,
	LEGS_P0_LUDENDORFF				,
	LEGS_P0_GOLF					,
	LEGS_P0_GOLF_1					,
	LEGS_P0_GOLF_2					,
	LEGS_P0_GOLF_3					,
	LEGS_P0_GOLF_4					,
	LEGS_P0_GOLF_5					,
	LEGS_P0_GOLF_6					,
	LEGS_P0_GOLF_7					,
	LEGS_P0_YOGA_0					,
	LEGS_P0_YOGA_1					,
	LEGS_P0_YOGA_2					,
	LEGS_P0_YOGA_3					,
	LEGS_P0_YOGA_4					,
	LEGS_P0_YOGA_5					,
	LEGS_P0_YOGA_6					,
	LEGS_P0_YOGA_7					,
	LEGS_P0_YOGA_8					,
	LEGS_P0_YOGA_9					,
	LEGS_P0_YOGA_10					,
	LEGS_P0_YOGA_11					,
	LEGS_P0_YOGA_12					,
	LEGS_P0_YOGA_13					,
	LEGS_P0_YOGA_14					,
	LEGS_P0_YOGA_15					,
	LEGS_P0_MOTO_X					,			
	LEGS_P0_BED						,
	LEGS_P0_BOXERS_1				,
	LEGS_P0_BOXERS_2				,
	LEGS_P0_BOXERS_3				,
	LEGS_P0_BOXERS_4				,
	LEGS_P0_BOXERS_5				,
	LEGS_P0_BOXERS_6				,
	LEGS_P0_BOXERS_7				,
	LEGS_P0_EPSILON					,
	LEGS_P0_CASUAL_JEANS			,
	LEGS_P0_CASUAL_JEANS_1			,
	LEGS_P0_CASUAL_JEANS_2			,
	LEGS_P0_REGULAR_PANTS			,
	LEGS_P0_REGULAR_PANTS_1			,
	LEGS_P0_REGULAR_PANTS_2			,
	LEGS_P0_REGULAR_PANTS_3			,
	LEGS_P0_SMART_JEANS				,
	LEGS_P0_TENNIS					,
	LEGS_P0_TENNIS_1				,
	LEGS_P0_TENNIS_2				,
	LEGS_P0_NAKED					,
	LEGS_P0_BLACK_TROUSERS			,
	LEGS_P0_JEANS					,
	LEGS_P0_YOGA_BAREFOOT			,
	LEGS_P0_LONG_SHORTS_0			,
	LEGS_P0_LONG_SHORTS_1			,
	LEGS_P0_LONG_SHORTS_2			,
	LEGS_P0_LONG_SHORTS_3			,
	LEGS_P0_LONG_SHORTS_4			,
	LEGS_P0_LONG_SHORTS_5			,
	LEGS_P0_LONG_SHORTS_6			,
	LEGS_P0_LONG_SHORTS_7			,
	LEGS_P0_LONG_SHORTS_8			,
	LEGS_P0_LONG_SHORTS_9			,
	LEGS_P0_LONG_SHORTS_10			,
	LEGS_P0_LONG_SHORTS_11			,
	LEGS_P0_LONG_SHORTS_12			,
	LEGS_P0_LONG_SHORTS_13			,
	LEGS_P0_LONG_SHORTS_14			,
	LEGS_P0_LONG_SHORTS_15			,
	LEGS_P0_JEANS_BAREFEET			,
	LEGS_P0_DLC						,
	
	// If you add new legs which have feet attached update DOES_LEGS_COMPONENT_HAVE_FEET_ATTACHED
	// If you add new legs that have a low waist update DOES_LEGS_HAVE_LOW_WAIST
	
	FEET_P0_BLACK_SHOES				= 0,
	FEET_P0_BLACK_SHOES_1			,
	FEET_P0_BLACK_SHOES_2			,
	FEET_P0_BLACK_SHOES_3			,
	FEET_P0_BLACK_SHOES_4			,
	FEET_P0_BLACK_SHOES_5			,
	FEET_P0_1						,
	FEET_P0_BED			 			,
	FEET_P0_SCUBA_WATER	 			,
	FEET_P0_BLACK_TRACKSUIT 		,
	FEET_P0_BALLISTICS				,
	FEET_P0_PROGRAMMER 				,
	FEET_P0_7		 				,
	FEET_P0_TRIATHLON				,
	FEET_P0_MOTO_X		 			,
	FEET_P0_ARM_GUARDS				,
	FEET_P0_MIME		 			,
	FEET_P0_SCUBA_LAND	 			,
	FEET_P0_DRESS_LOAFERS			,		
	FEET_P0_BOOTS					,			
	FEET_P0_SKATE_SHOES				,			
	FEET_P0_SKATE_SHOES_1			,			
	FEET_P0_SKATE_SHOES_2			,			
	FEET_P0_SKATE_SHOES_3			,			
	FEET_P0_SKATE_SHOES_4			,			
	FEET_P0_SKATE_SHOES_5			,			
	FEET_P0_SKATE_SHOES_6			,			
	FEET_P0_SKATE_SHOES_7			,			
	FEET_P0_SKATE_SHOES_8			,			
	FEET_P0_SKATE_SHOES_9			,	
	FEET_P0_COMFY_SHOES_0			,		
	FEET_P0_COMFY_SHOES_1			,		
	FEET_P0_COMFY_SHOES_2			,		
	FEET_P0_COMFY_SHOES_3			,		
	FEET_P0_COMFY_SHOES_4			,		
	FEET_P0_COMFY_SHOES_5			,		
	FEET_P0_COMFY_SHOES_6			,		
	FEET_P0_COMFY_SHOES_7			,
	FEET_P0_SECURITY_BOOTS			,
	FEET_P0_OXFORDS_0				,
	FEET_P0_OXFORDS_1				,
	FEET_P0_OXFORDS_2				,
	FEET_P0_OXFORDS_3				,
	FEET_P0_OXFORDS_4				,
	FEET_P0_OXFORDS_5				,
	FEET_P0_OXFORDS_6				,
	FEET_P0_OXFORDS_7				,
	FEET_P0_OXFORDS_8				,
	FEET_P0_OXFORDS_9				,
	FEET_P0_OXFORDS_10				,
	FEET_P0_OXFORDS_11				,
	FEET_P0_OXFORDS_12				,
	FEET_P0_OXFORDS_13				,
	FEET_P0_OXFORDS_14				,
	FEET_P0_OXFORDS_15				,
	FEET_P0_SQ_LOAFERS_0			,
	FEET_P0_SQ_LOAFERS_1			,
	FEET_P0_SQ_LOAFERS_2			,
	FEET_P0_SQ_LOAFERS_3			,
	FEET_P0_SQ_LOAFERS_4			,
	FEET_P0_SQ_LOAFERS_5			,
	FEET_P0_SQ_LOAFERS_6			,
	FEET_P0_SQ_LOAFERS_7			,
	FEET_P0_SQ_LOAFERS_8			,
	FEET_P0_SQ_LOAFERS_9			,
	FEET_P0_SQ_LOAFERS_10			,
	FEET_P0_SQ_LOAFERS_11			,
	FEET_P0_SQ_LOAFERS_12			,
	FEET_P0_SQ_LOAFERS_13			,
	FEET_P0_SQ_LOAFERS_14			,
	FEET_P0_SQ_LOAFERS_15			,	
	FEET_P0_WINGTIPS_0				,
	FEET_P0_WINGTIPS_1				,
	FEET_P0_WINGTIPS_2				,
	FEET_P0_WINGTIPS_3				,
	FEET_P0_WINGTIPS_4				,
	FEET_P0_WINGTIPS_5				,
	FEET_P0_WINGTIPS_6				,
	FEET_P0_WINGTIPS_7				,
	FEET_P0_WINGTIPS_8				,
	FEET_P0_WINGTIPS_9				,
	FEET_P0_WINGTIPS_10				,
	FEET_P0_WINGTIPS_11				,
	FEET_P0_WINGTIPS_12				,
	FEET_P0_WINGTIPS_13				,
	FEET_P0_WINGTIPS_14				,
	FEET_P0_WINGTIPS_15				,
	FEET_P0_LOAFERS_0				,
	FEET_P0_LOAFERS_1				,
	FEET_P0_LOAFERS_2				,
	FEET_P0_LOAFERS_3				,
	FEET_P0_LOAFERS_4				,
	FEET_P0_LOAFERS_5				,
	FEET_P0_LOAFERS_6				,
	FEET_P0_LOAFERS_7				,
	FEET_P0_LOAFERS_8				,
	FEET_P0_LOAFERS_9				,
	FEET_P0_LOAFERS_10				,
	FEET_P0_LOAFERS_11				,
	FEET_P0_DLC						,
	// if you add new dummy feet, update IS_PED_COMPONENT_ITEM_RESTRICTED_FOR_ITEM_SP
	
	HAND_P0_NONE 					= 0,
	HAND_P0_FIREMAN 				,
	HAND_P0_NAVY_PARAMEDIC			,
	HAND_P0_GOLF					,
	HAND_P0_GOLF_1					,
	HAND_P0_GOLF_2					,
	HAND_P0_GOLF_3					,
	HAND_P0_GOLF_4					,
	HAND_P0_WHITE_GLOVES			,
	HAND_P0_BROWN_LEATHER			,
	HAND_P0_PADDED_GLOVES			,
	HAND_P0_BLACK_GLOVES			,
	HAND_P0_BLACK_LEATHER_GLOVES	,
	HAND_P0_MOTO_X					,
	HAND_P0_DLC						, // leave this at the bottom
	
	SPECIAL_P0_NONE 				= 0,
	SPECIAL_P0_FIREMAN_ACCS			,
	SPECIAL_P0_JANITOR_ACCS 		,
	SPECIAL_P0_SCUBA_ACCS			,
	SPECIAL_P0_POUCHES				,
	SPECIAL_P0_BALLISTICS 			,
	SPECIAL_P0_TIE 					,
	SPECIAL_P0_BALACLAVA 			,
	SPECIAL_P0_SECURITY_ACCS		,
	SPECIAL_P0_GAS_MASK				,
	SPECIAL_P0_WATCH_AND_BRACELET	,
	SPECIAL_P0_STAR_TATTOOS			,
	SPECIAL_P0_BACKPACK				,
	SPECIAL_P0_EPSILON_MEDAL		,
	SPECIAL_P0_CLOWN_ACCS			,
	SPECIAL_P0_PARACHUTE			,
	SPECIAL_P0_WATCH_LEATHER_STRAP	,
	SPECIAL_P0_CUP					,
	SPECIAL_P0_TIE_BLACK			,
	SPECIAL_P0_SCARF				,
	SPECIAL_P0_LEATHER_BAG			,
	SPECIAL_P0_FIREMAN_ACCS_1		,
	SPECIAL_P0_SCUBA_ACCS_1			,
	SPECIAL_P0_BALACLAVA_B			,
	SPECIAL_P0_DLC					, // keep this at the bottom
	
	SPECIAL2_P0_NONE 				= 0,
	SPECIAL2_P0_BAG_1 				,
	SPECIAL2_P0_BAG_1_B				,
	SPECIAL2_P0_BAG_2 				,
	SPECIAL2_P0_HARNESS 			,
	SPECIAL2_P0_COP_BELT_1 			,
	SPECIAL2_P0_PARACHUTE_2 		,
	SPECIAL2_P0_COP_BELT_2 			,
	SPECIAL2_P0_SCUBA_HEADGEAR		,
	SPECIAL2_P0_WRESTLER_MASK_0		,
	SPECIAL2_P0_WRESTLER_MASK_1		,
	SPECIAL2_P0_WRESTLER_MASK_2		,
	SPECIAL2_P0_WRESTLER_MASK_3		,
	SPECIAL2_P0_WRESTLER_MASK_4		,
	SPECIAL2_P0_WRESTLER_MASK_5		,
	SPECIAL2_P0_RUCKSACK			,
	SPECIAL2_P0_BAG_3				,
	SPECIAL2_P0_BAG_3_B				,
	SPECIAL2_P0_BAG_4				,
	SPECIAL2_P0_BAG_5				,
	SPECIAL2_P0_DLC					, // keep this at the bottom
	
	DECL_P0_NONE 					= 0,
	DECL_P0_FIREMAN 				,
	DECL_P0_NAVY_PARAMEDIC			,
	DECL_P0_HIGHWAY_PATROL 			,
	DECL_P0_EXTERMINATOR			,
	DECL_P0_COP						,
	DECL_P0_CROWN_LOGO				,
	DECL_P0_T_SHIRT_LOGO_0			,
	DECL_P0_T_SHIRT_LOGO_1			,
	DECL_P0_T_SHIRT_LOGO_2			,
	DECL_P0_T_SHIRT_LOGO_3			,
	DECL_P0_T_SHIRT_LOGO_4			,
	DECL_P0_T_SHIRT_LOGO_5			,
	DECL_P0_T_SHIRT_LOGO_6			,
	DECL_P0_T_SHIRT_LOGO_7			,
	DECL_P0_AMMUNATION_0			,
	DECL_P0_AMMUNATION_1			,
	DECL_P0_AMMUNATION_2			,
	DECL_P0_AMMUNATION_3			,
	DECL_P0_AMMUNATION_4			,
	DECL_P0_AMMUNATION_5			,
	DECL_P0_SPORT_0					,
	DECL_P0_SPORT_1					,
	DECL_P0_SPORT_2					,
	DECL_P0_SPORT_3					,
	DECL_P0_SPORT_4					,
	DECL_P0_SPORT_5					,
	DECL_P0_SPORT_6					,
	DECL_P0_TSHIRT_0				,
	DECL_P0_TSHIRT_1				,
	DECL_P0_TSHIRT_2				,
	DECL_P0_TSHIRT_3				,
	DECL_P0_TSHIRT_4				,
	DECL_P0_TSHIRT_5				,
	DECL_P0_TSHIRT_6				,
	DECL_P0_TSHIRT_7				,
	DECL_P0_TSHIRT_8				,
	DECL_P0_TSHIRT_9				,
	DECL_P0_TSHIRT_10				,
	DECL_P0_TSHIRT_11				,
	DECL_P0_TSHIRT_12				,
	DECL_P0_TSHIRT_13				,
	DECL_P0_TSHIRT_14				,
	DECL_P0_TSHIRT_15				,
	DECL_P0_MASK					,
	DECL_P0_MASK_1					,
	DECL_P0_MASK_2					,
	DECL_P0_MASK_3					,
	DECL_P0_DLC						,
	
	BERD_P0_NONE 					= 0,
	BERD_P0_1_0						,
	BERD_P0_2_0 					,
	BERD_P0_3_0 					,
	BERD_P0_4_0 					,
	BERD_P0_DLC						,
	
	TEETH_P0_NONE 					= 0,
	TEETH_P0_DLC					,
	
	JBIB_P0_NONE 					= 0,
	JBIB_P0_TIE 					,
	JBIB_P0_VEST 					,
	JBIB_P0_VEST_1 					,
	JBIB_P0_VEST_2 					,
	JBIB_P0_VEST_3 					,
	JBIB_P0_VEST_4 					,
	JBIB_P0_VEST_5 					,
	JBIB_P0_3_0 					,
	JBIB_P0_3_1 					,
	JBIB_P0_3_2 					,
	JBIB_P0_3_3 					,
	JBIB_P0_3_4 					,
	JBIB_P0_3_5 					,
	JBIB_P0_3_6 					,
	JBIB_P0_3_7 					,
	JBIB_P0_3_8 					,
	JBIB_P0_3_9 					,
	JBIB_P0_4_0 					,
	JBIB_P0_4_1 					,
	JBIB_P0_4_2 					,
	JBIB_P0_4_3 					,
	JBIB_P0_4_4 					,
	JBIB_P0_4_5 					,
	JBIB_P0_4_6 					,
	JBIB_P0_4_7 					,
	JBIB_P0_4_8 					,
	JBIB_P0_4_9 					,
	JBIB_P0_5_0 					,
	JBIB_P0_5_1						,
	JBIB_P0_5_2 					,
	JBIB_P0_5_3 					,
	JBIB_P0_5_4 					,
	JBIB_P0_5_5 					,
	JBIB_P0_5_6 					,
	JBIB_P0_5_7 					,
	JBIB_P0_5_8 					,
	JBIB_P0_5_9 					,
	JBIB_P0_5_10 					,
	JBIB_P0_5_11					,
	JBIB_P0_5_12 					,
	JBIB_P0_5_13					,
	JBIB_P0_5_14 					,
	JBIB_P0_5_15 					,
	JBIB_P0_BARE_CHEST				,
	JBIB_P0_DLC						, // leave this at the bottom
	
	OUTFIT_P0_DEFAULT 				= OUTFIT_DEFAULT,
	OUTFIT_P0_FIREMAN 				,
	OUTFIT_P0_BALLISTICS 			,
	OUTFIT_P0_NAVY_JANITOR  		,
	OUTFIT_P0_HIGHWAY_PATROL 		,
	OUTFIT_P0_GOLF			 		,
	OUTFIT_P0_BED			 		,
	OUTFIT_P0_YOGA			 		,
	OUTFIT_P0_EPSILON		 		,
	OUTFIT_P0_TENNIS		 		,
	OUTFIT_P0_SCUBA_WATER	 		,
	OUTFIT_P0_SCUBA_LAND	 		,
	OUTFIT_P0_STEALTH		 		,
	OUTFIT_P0_TRIATHLON				,
	OUTFIT_P0_SECURITY				,
	OUTFIT_P0_EXTERMINATOR			,
	OUTFIT_P0_MOVIE_TUXEDO			,
	OUTFIT_P0_LUDENDORFF			,
	OUTFIT_P0_RAPPEL				,
	OUTFIT_P0_BLUE_BOILER_SUIT		,
	OUTFIT_P0_PREP_BOILER_SUIT_1	,
	OUTFIT_P0_PREP_BOILER_SUIT_2	,
	OUTFIT_P0_PREP_BOILER_SUIT_3	,
	OUTFIT_P0_PROLOGUE				,
	OUTFIT_P0_COMMANDO				,
	OUTFIT_P0_DENIM					,
	OUTFIT_P0_BLACK_SUIT			,
	OUTFIT_P0_LEATHER_AND_JEANS		,
	OUTFIT_P0_EPSILON_WITH_MEDAL	,
	OUTFIT_P0_DARK_GRAY_SUIT		,
	OUTFIT_P0_SHIRT_AND_PANTS		,
	OUTFIT_P0_JEWEL_HEIST			,
	OUTFIT_P0_YOGA_FLIP_FLOPS		,
	OUTFIT_P0_POLOSHIRT_PANTS		,
	OUTFIT_P0_SHIRT_SHORTS_1		,
	OUTFIT_P0_POLOSHIRT_JEANS_1		, 
	OUTFIT_P0_NAVY_SUIT				, 
	OUTFIT_P0_SUIT_5				,
	OUTFIT_P0_SUIT_6				,
	OUTFIT_P0_SUIT_7				,
	OUTFIT_P0_SUIT_8				,
	OUTFIT_P0_SUIT_9				,
	OUTFIT_P0_SUIT_10				,
	OUTFIT_P0_SUIT_11				,
	OUTFIT_P0_SUIT_12				,
	OUTFIT_P0_SUIT_13				,
	OUTFIT_P0_SUIT_14				,
	OUTFIT_P0_SUIT_15				, 
	OUTFIT_P0_MOTO_X				, 
	OUTFIT_P0_STEALTH_NO_MASK		,
	OUTFIT_P0_SCUBA_WATER_NO_MASK	, 
	OUTFIT_P0_BARECHEST_SHORTS		, 
	OUTFIT_P0_BARECHEST_BOXERS		, // 53
	OUTFIT_P0_DLC					, // leave this at the bottom
	// If adding outfits, you may need to increase PED_COMPONENTS_OUTFITS
	
	PROPGROUP_P0_FIREMAN 				= 0,
	PROPGROUP_P0_COP 					,
	PROPGROUP_P0_HIGHWAY_PATROL			,
	PROPGROUP_P0_GOLF					,
	PROPGROUP_P0_SCUBA_WATER			,
	PROPGROUP_P0_SCUBA_LAND				,
	PROPGROUP_P0_SECURITY				,
	PROPGROUP_P0_RAPPEL					,
	PROPGROUP_P0_BALLISTICS				,
	PROPGROUP_P0_SCUBA_WATER_NO_MASK	,
	PROPGROUP_P0_DLC					, // leave this at the bottom
	
	// props need to be ordered by anchor and then drawable / variation
	// if the 1st prop of a type changes, update GET_FIRST_PROP_OF_TYPE
	// ANCHOR_HEAD, 
	PROPS_P0_FIREMAN_HAT 			= 10, // Start props at 10.
	PROPS_P0_COP_CAP 				,
	PROPS_P0_HOCKEY_MASK			,
	PROPS_P0_EAR_DEFENDERS			,
	PROPS_P0_COP_HELMET				,
	PROPS_P0_SECURITY_HELMET		,
	PROPS_P0_BUGSTAR_CAP			,
	PROPS_P0_CYCLING_HELMET			,
	PROPS_P0_GOLF_VISOR				,
	PROPS_P0_SECURITY_CAP			,
	PROPS_P0_CLOWN_HAT				,
	PROPS_P0_CRASH_HELMET			,
	PROPS_P0_CRASH_HELMET_1			,
	PROPS_P0_CRASH_HELMET_2			,
	PROPS_P0_CRASH_HELMET_3			,
	PROPS_P0_CRASH_HELMET_4			,
	PROPS_P0_CRASH_HELMET_5			,
	PROPS_P0_CRASH_HELMET_6			,
	PROPS_P0_CRASH_HELMET_7			,
	PROPS_P0_HAT					,
	PROPS_P0_HEADPHONES				,
	PROPS_P0_MASK_MONSTER_GREEN		,
	PROPS_P0_MASK_MONSTER_RED		, 
	PROPS_P0_MASK_PIG				,
	PROPS_P0_MASK_PIG_DARK			, 
	PROPS_P0_MASK_SKULL_GREY		,
	PROPS_P0_MASK_SKULL_YELLOW		,
	PROPS_P0_MASK_MONKEY			,
	PROPS_P0_MASK_HOCKEY_WHITE		,
	PROPS_P0_MASK_HOCKEY_RED		,
	PROPS_P0_MASK_APE				,
	PROPS_P0_MASK_APE_DARK			,
	PROPS_P0_MASK_TRIBAL_1			,
	PROPS_P0_MASK_TRIBAL_2			,
	PROPS_P0_MASK_TRIBAL_3			,
	PROPS_P0_PILOT_HEADSET			, 
	PROPS_P0_SWIM_CAP				,
	PROPS_P0_MOTO_X_HELMET_0		,
	PROPS_P0_MOTO_X_HELMET_1		,
	PROPS_P0_MOTO_X_HELMET_2		,
	PROPS_P0_MOTO_X_HELMET_3		,
	PROPS_P0_MOTO_X_HELMET_4		,
	PROPS_P0_MOTO_X_HELMET_5		,
	PROPS_P0_BLANK					,
	PROPS_P0_BLACK_HELMET			,
	PROPS_P0_ARMOURED_HELMET		,
	PROPS_P0_FLIGHT_CAP				, // 56
	PROPS_P0_FIGHTER_JET_HELMET		,
	
	//ANCHOR_EYES, 
	PROPS_P0_GLASSES 				,
	PROPS_P0_SCUBA_MASK				,
	PROPS_P0_EXTERMINATOR_MASK		,
	PROPS_P0_SHADES_REFLECTIVE		,
	PROPS_P0_PROGRAMMER_GLASSES		,
	PROPS_P0_PROGRAMMER_GLASSES_1	,
	PROPS_P0_PROGRAMMER_GLASSES_2	,
	PROPS_P0_PROGRAMMER_GLASSES_3	,
	PROPS_P0_PROGRAMMER_GLASSES_4	,
	PROPS_P0_PROGRAMMER_GLASSES_5	,
	PROPS_P0_PROGRAMMER_GLASSES_6	,
	PROPS_P0_PROGRAMMER_GLASSES_7	,
	PROPS_P0_GLASSES_DARK			,
	PROPS_P0_GLASSES_DARK_1			,
	PROPS_P0_GLASSES_DARK_2			,
	PROPS_P0_GLASSES_DARK_3			,
	PROPS_P0_GLASSES_DARK_4			,
	PROPS_P0_GLASSES_DARK_5			,
	PROPS_P0_GLASSES_DARK_6			,
	PROPS_P0_GLASSES_DARK_7			,
	PROPS_P0_GLASSES_DARK_8			,
	PROPS_P0_GLASSES_DARK_9			,
	PROPS_P0_SHADES					,
	PROPS_P0_SHADES_1				,
	PROPS_P0_SHADES_2				,
	PROPS_P0_SHADES_3				,
	PROPS_P0_SHADES_4				,
	PROPS_P0_SHADES_5				,
	PROPS_P0_SHADES_6				,
	PROPS_P0_SHADES_7				,
	PROPS_P0_SHADES_8				,
	PROPS_P0_SHADES_9				,
	PROPS_P0_SWIMMING_GOGGLES		,
	PROPS_P0_AVIATORS_0				,
	PROPS_P0_AVIATORS_1				,
	PROPS_P0_AVIATORS_2				,
	PROPS_P0_AVIATORS_3				,
	PROPS_P0_AVIATORS_4				,
	PROPS_P0_AVIATORS_5				,
	PROPS_P0_AVIATORS_6				,
	PROPS_P0_AVIATORS_7				,
	PROPS_P0_AVIATORS_8				,
	PROPS_P0_AVIATORS_9				,
	PROPS_P0_AVIATORS_10			,
	PROPS_P0_AVIATORS_11			,
	PROPS_P0_GLASSES_THICK_RIM_0	,
	PROPS_P0_GLASSES_THICK_RIM_1	,//Hawaiian snow charcoal shades
	PROPS_P0_GLASSES_THICK_RIM_2	,
	PROPS_P0_GLASSES_THICK_RIM_3	,
	PROPS_P0_GLASSES_THICK_RIM_4	,
	PROPS_P0_GLASSES_THICK_RIM_5	,//Hawaiian snow Tortoiseshell shades
	PROPS_P0_GLASSES_THICK_RIM_6	,
	PROPS_P0_GLASSES_THICK_RIM_7	,//Hawaiian snow marbled shades
	PROPS_P0_GLASSES_RANGE_0,		//New shooting range glasses
	
	//ANCHOR_EARS
	PROPS_P0_HEADSET				,
	
    //ANCHOR_MOUTH,
   	//ANCHOR_LEFT_HAND,
    //ANCHOR_RIGHT_HAND,
   	//ANCHOR_LEFT_WRIST,
    //ANCHOR_RIGHT_WRIST,
    //ANCHOR_HIP

	PROPS_P0_DLC					, // leave this at the bottom
	
	////////////////////////////////////////////////////////
	/// TREVOR
	HEAD_P2_NONE 					= 0,
	HEAD_P2_NONE_1					,
	HEAD_P2_NONE_2					,
	HEAD_P2_NONE_3					,
	HEAD_P2_NONE_4					,
	HEAD_P2_NONE_5					,
	HEAD_P2_NONE_6					,
	HEAD_P2_DLC						, // leave this at the bottom
	
	HAIR_P2_MESSY 					= 0,
	HAIR_P2_FROZEN					,
	HAIR_P2_SHAVED 					,
	HAIR_P2_CURLS 					,
	HAIR_P2_SIDE_SHED 				,
	HAIR_P2_MESSY2 					,
	HAIR_P2_MESSY3 					,
	HAIR_P2_SHAVED2 				,
	HAIR_P2_PROLOGUE 				,
	HAIR_P2_DLC						,
	
	TORSO_P2_WHITE_TSHIRT			= 0,
	TORSO_P2_VNECK_1				,
	TORSO_P2_VNECK_2				,
	TORSO_P2_VNECK_3				,
	TORSO_P2_VNECK_4				,
	TORSO_P2_VNECK_5				,
	TORSO_P2_VNECK_6				,
	TORSO_P2_VNECK_7				,
	TORSO_P2_VNECK_8				,
	TORSO_P2_VNECK_9				,
	TORSO_P2_VNECK_10				,
	TORSO_P2_VNECK_11				,
	TORSO_P2_VNECK_12				,
	TORSO_P2_VNECK_13				,
	TORSO_P2_VNECK_14				,
	TORSO_P2_VNECK_15				,
	TORSO_P2_FIREMAN 				,
	TORSO_P2_BALLISTICS 			,
	TORSO_P2_TANK_TOP_0 			,
	TORSO_P2_TANK_TOP_1 			,
	TORSO_P2_TANK_TOP_2 			,
	TORSO_P2_TANK_TOP_3 			,
	TORSO_P2_TANK_TOP_4 			,
	TORSO_P2_TANK_TOP_5 			,
	TORSO_P2_TANK_TOP_6 			,
	TORSO_P2_TANK_TOP_7 			,
	TORSO_P2_TANK_TOP_8 			,
	TORSO_P2_LUDENDORFF  			,
	TORSO_P2_DOWN_JACKET_1			,  			
	TORSO_P2_DOWN_JACKET_2  		,
	TORSO_P2_DOWN_JACKET_3  		,
	TORSO_P2_DOWN_JACKET_4  		,
	TORSO_P2_DOWN_JACKET_5  		,
	TORSO_P2_DOWN_JACKET_6  		,
	TORSO_P2_DOWN_JACKET_7  		,
	TORSO_P2_DOWN_JACKET_8  		,
	TORSO_P2_DOWN_JACKET_9  		,
	TORSO_P2_DOWN_JACKET_10  		,
	TORSO_P2_DOWN_JACKET_11 		,
	TORSO_P2_DOWN_JACKET_12  		,
	TORSO_P2_DOWN_JACKET_13 		,
	TORSO_P2_DOWN_JACKET_14  		,
	TORSO_P2_DOWN_JACKET_15  		,
	TORSO_P2_DOCK_WORKER  			,
	TORSO_P2_BOILER_SUIT_BLUE		,
	TORSO_P2_PREP_BOILER_SUIT_1		,
	TORSO_P2_PREP_BOILER_SUIT_2		,
	TORSO_P2_PREP_BOILER_SUIT_3		,
	TORSO_P2_DOCK_WORKER_B			,
	TORSO_P2_SCUBA					,
	TORSO_P2_HIGHWAY_PATROL 		,
	TORSO_P2_SECURITY				,
	TORSO_P2_PROLOGUE				,
	TORSO_P2_TRIATHLON				,
	TORSO_P2_GOLF					,
	TORSO_P2_TSHIRT_1				,
	TORSO_P2_TSHIRT_2				,
	TORSO_P2_TSHIRT_3				,
	TORSO_P2_TSHIRT_4				,
	TORSO_P2_TSHIRT_5				,
	TORSO_P2_TSHIRT_6				,
	TORSO_P2_TSHIRT_7				,
	TORSO_P2_TSHIRT_8				,
	TORSO_P2_TSHIRT_9				,
	TORSO_P2_TSHIRT_10				,
	TORSO_P2_TSHIRT_11				,
	TORSO_P2_TSHIRT_12				,
	TORSO_P2_TSHIRT_13				,
	TORSO_P2_TSHIRT_14				,
	TORSO_P2_TSHIRT_15				,
	TORSO_P2_MOTO_X					,
	TORSO_P2_DRESS					,
	TORSO_P2_DRESS_1				,
	TORSO_P2_DRESS_2				,
	TORSO_P2_DRESS_3				,
	TORSO_P2_DRESS_4				,
	TORSO_P2_DRESS_5				,
	TORSO_P2_DRESS_6				,
	TORSO_P2_DRESS_7				,
	TORSO_P2_STEALTH				,
	TORSO_P2_HOODIE_1				,
	TORSO_P2_HOODIE_2				,
	TORSO_P2_HOODIE_3				,
	TORSO_P2_HOODIE_4				,
	TORSO_P2_HOODIE_5				,
	TORSO_P2_HOODIE_6				,
	TORSO_P2_HOODIE_7				,
	TORSO_P2_HOODIE_8				,
	TORSO_P2_HOODIE_9				,
	TORSO_P2_HOODIE_10				,
	TORSO_P2_HOODIE_11				,
	TORSO_P2_HOODIE_12				,
	TORSO_P2_HOODIE_13				,
	TORSO_P2_HOODIE_14				,
	TORSO_P2_HOODIE_15				,
	TORSO_P2_TENNIS					,
	TORSO_P2_NONE					,
	TORSO_P2_LADIES					,
	TORSO_P2_DENIM_JACKET			,
	TORSO_P2_JACKET_1				,
	TORSO_P2_JACKET_2				,
	TORSO_P2_JACKET_3				,
	TORSO_P2_JACKET_4				,
	TORSO_P2_JACKET_5				,
	TORSO_P2_DENIM_SHIRT			,
	TORSO_P2_SHIRT_1				,
	TORSO_P2_SHIRT_2				,
	TORSO_P2_SHIRT_3				,
	TORSO_P2_SHIRT_4				,
	TORSO_P2_SHIRT_5				,
	TORSO_P2_SHIRT_6				,
	TORSO_P2_SHIRT_7				,
	TORSO_P2_SHIRT_8				,
	TORSO_P2_SHIRT_9				,
	TORSO_P2_SHIRT_10				,
	TORSO_P2_SHIRT_11				,
	TORSO_P2_SHIRT_12				,
	TORSO_P2_SHIRT_13				,
	TORSO_P2_SHIRT_14				,
	TORSO_P2_SHIRT_15				,
	TORSO_P2_BOWLING_SHIRT			,
	TORSO_P2_BOWLING_SHIRT_1		,
	TORSO_P2_BOWLING_SHIRT_2		,
	TORSO_P2_BOWLING_SHIRT_3		,
	TORSO_P2_BOWLING_SHIRT_4		,
	TORSO_P2_BOWLING_SHIRT_5		,
	TORSO_P2_BOWLING_SHIRT_6		,
	TORSO_P2_BOWLING_SHIRT_7		,
	TORSO_P2_BOWLING_SHIRT_8		,
	TORSO_P2_BOWLING_SHIRT_9		,
	TORSO_P2_BOWLING_SHIRT_10		,
	TORSO_P2_BOWLING_SHIRT_11		,
	TORSO_P2_BOWLING_SHIRT_12		,
	TORSO_P2_BOWLING_SHIRT_13		,
	TORSO_P2_BOWLING_SHIRT_14		,
	TORSO_P2_BOWLING_SHIRT_15		,
	TORSO_P2_BLOUSON				,
	TORSO_P2_BLOUSON_1				,
	TORSO_P2_BLOUSON_2				,
	TORSO_P2_BLOUSON_3				,
	TORSO_P2_BLOUSON_4				,
	TORSO_P2_BLOUSON_5				,
	TORSO_P2_BLOUSON_6				,
	TORSO_P2_BLOUSON_7				,
	TORSO_P2_LEATHER_JACKET			,
	TORSO_P2_LEATHER_JACKET_1		,
	TORSO_P2_LEATHER_JACKET_2		,
	TORSO_P2_LEATHER_JACKET_3		,
	TORSO_P2_LEATHER_JACKET_4		,
	TORSO_P2_LEATHER_JACKET_5		,
	TORSO_P2_LEATHER_JACKET_6		,
	TORSO_P2_LEATHER_JACKET_7		,
	TORSO_P2_LEATHER_JACKET_8		,
	TORSO_P2_LEATHER_JACKET_9		,
	TORSO_P2_LEATHER_JACKET_10		,
	TORSO_P2_LEATHER_JACKET_11		,
	TORSO_P2_GILET					,
	TORSO_P2_GILET_1				,
	TORSO_P2_GILET_2				,
	TORSO_P2_GILET_3				,
	TORSO_P2_GILET_4				,
	TORSO_P2_GILET_5				,
	TORSO_P2_YELLOW_VEST			,
	TORSO_P2_YELLOW_VEST_1			,
	TORSO_P2_YELLOW_VEST_2			,
	TORSO_P2_YELLOW_VEST_3			,
	TORSO_P2_YELLOW_VEST_4			,
	TORSO_P2_YELLOW_VEST_5			,
	TORSO_P2_YELLOW_VEST_6			,
	TORSO_P2_YELLOW_VEST_7			,
	TORSO_P2_YELLOW_VEST_8			,
	TORSO_P2_YELLOW_VEST_9			,
	TORSO_P2_YELLOW_VEST_10			,
	TORSO_P2_YELLOW_VEST_11			,
	TORSO_P2_YELLOW_VEST_12			,
	TORSO_P2_YELLOW_VEST_13			,
	TORSO_P2_YELLOW_VEST_14			,
	TORSO_P2_YELLOW_VEST_15			,
	TORSO_P2_LONG_SLEEVE			,
	TORSO_P2_LONG_SLEEVE_1			,
	TORSO_P2_LONG_SLEEVE_2			,
	TORSO_P2_LONG_SLEEVE_3			,
	TORSO_P2_LONG_SLEEVE_4			,
	TORSO_P2_LONG_SLEEVE_5			,
	TORSO_P2_LONG_SLEEVE_6			,
	TORSO_P2_LONG_SLEEVE_7			,
	TORSO_P2_LONG_SLEEVE_8			,
	TORSO_P2_LONG_SLEEVE_9			,
	TORSO_P2_LONG_SLEEVE_10			,
	TORSO_P2_LONG_SLEEVE_11			,
	TORSO_P2_CHEAPSUIT_0			,
	TORSO_P2_CHEAPSUIT_1			,
	TORSO_P2_CHEAPSUIT_2			,
	TORSO_P2_CHEAPSUIT_3			,
	TORSO_P2_CHEAPSUIT_4			,
	TORSO_P2_CHEAPSUIT_5			,
	TORSO_P2_CHEAPSUIT_6			,
	TORSO_P2_CHEAPSUIT_7			,
	TORSO_P2_CHEAPSUIT_8			,
	TORSO_P2_CHEAPSUIT_9			,
	TORSO_P2_STYLESUIT_JACKET		,
	TORSO_P2_STYLESUIT_JACKET_1 	,
	TORSO_P2_STYLESUIT_JACKET_2 	,
	TORSO_P2_STYLESUIT_JACKET_3 	,
	TORSO_P2_STYLESUIT_JACKET_4		,
	TORSO_P2_STYLESUIT_JACKET_5 	,
	TORSO_P2_STYLESUIT_JACKET_6 	,
	TORSO_P2_WOOL_SWEATER			,
	TORSO_P2_WOOL_SWEATER_1			,
	TORSO_P2_WOOL_SWEATER_2			,
	TORSO_P2_WOOL_SWEATER_3			,
	TORSO_P2_WOOL_SWEATER_4			,
	TORSO_P2_WOOL_SWEATER_5			,
	TORSO_P2_WOOL_SWEATER_6			,
	TORSO_P2_WOOL_SWEATER_7			,
	TORSO_P2_WOOL_SWEATER_8			,
	TORSO_P2_WOOL_SWEATER_9			,
	TORSO_P2_WOOL_SWEATER_10		,
	TORSO_P2_WOOL_SWEATER_11		,
	TORSO_P2_WOOL_SWEATER_12		,
	TORSO_P2_WOOL_SWEATER_13		,
	TORSO_P2_WOOL_SWEATER_14		,
	TORSO_P2_WOOL_SWEATER_15		,
	TORSO_P2_STYLESUIT_TEE_0		,
	TORSO_P2_STYLESUIT_TEE_1		,
	TORSO_P2_STYLESUIT_TEE_2		,
	TORSO_P2_STYLESUIT_TEE_3		,
	TORSO_P2_STYLESUIT_TEE_4		,
	TORSO_P2_STYLESUIT_TEE_5		,
	TORSO_P2_STYLESUIT_TEE_6		,
	TORSO_P2_STYLESUIT_TEE_7		,
	TORSO_P2_STYLESUIT_TEE_8		,
	TORSO_P2_STYLESUIT_TEE_9		,
	TORSO_P2_STYLESUIT_VNECK		,
	TORSO_P2_STYLESUIT_VNECK_1		,
	TORSO_P2_STYLESUIT_VNECK_2		,
	TORSO_P2_STYLESUIT_VNECK_3		,
	TORSO_P2_STYLESUIT_VNECK_4		,
	TORSO_P2_STYLESUIT_VNECK_5		,
	TORSO_P2_STYLESUIT_VNECK_6		,
	TORSO_P2_STYLESUIT_VNECK_7		,
	TORSO_P2_TUXEDO					,
	TORSO_P2_DLC					,
	// If you add a new torso that has a high waist update DOES_TORSO_HAVE_HIGH_WAIST
	
	LEGS_P2_BLUE_JEANS 				= 0,
	LEGS_P2_JEANS_1 				,
	LEGS_P2_JEANS_2 				,
	LEGS_P2_JEANS_3 				,
	LEGS_P2_FIREMAN 				,
	LEGS_P2_BALLISTICS 				,
	LEGS_P2_NAVY_COP 				,
	LEGS_P2_PARAMEDIC 				,
	LEGS_P2_DOCK_WORKER 			,
	LEGS_P2_BOILER_SUIT_BLUE		,
	LEGS_P2_PREP_BOILER_SUIT_1		,
	LEGS_P2_PREP_BOILER_SUIT_2		,
	LEGS_P2_PREP_BOILER_SUIT_3		,
	LEGS_P2_SCUBA					,
	LEGS_P2_HIGHWAY_PATROL 			,
	LEGS_P2_SECURITY				,
	LEGS_P2_PROLOGUE				,
	LEGS_P2_TRIATHLON				,
	LEGS_P2_GOLF					,
	LEGS_P2_GOLF_1					,
	LEGS_P2_GOLF_2					,
	LEGS_P2_GOLF_3					,
	LEGS_P2_GOLF_4					,
	LEGS_P2_GOLF_5					,
	LEGS_P2_GOLF_6					,
	LEGS_P2_GOLF_7					,
	LEGS_P2_GOLF_8					,
	LEGS_P2_GOLF_9					,
	LEGS_P2_GOLF_10					,
	LEGS_P2_GOLF_11					,
	LEGS_P2_MOTO_X					,
	LEGS_P2_MIME					,
	LEGS_P2_STEALTH					,
	LEGS_P2_TENNIS					,
	LEGS_P2_LADIES_SWEAT			,
	LEGS_P2_WORKPANTS				,
	LEGS_P2_WORKPANTS_1				,
	LEGS_P2_WORKPANTS_2				,
	LEGS_P2_WORKPANTS_3				,
	LEGS_P2_WORKPANTS_4				,
	LEGS_P2_WORKPANTS_5				,
	LEGS_P2_WORKPANTS_6				,
	LEGS_P2_WORKPANTS_7				,
	LEGS_P2_CARGOPANTS				,
	LEGS_P2_CARGOPANTS_1			,
	LEGS_P2_CARGOPANTS_2			,
	LEGS_P2_CARGOPANTS_3			,
	LEGS_P2_CARGOPANTS_4			,
	LEGS_P2_CARGOPANTS_5			,
	LEGS_P2_CARGOPANTS_6			,
	LEGS_P2_CARGOPANTS_7			,
	LEGS_P2_CARGOPANTS_8			,
	LEGS_P2_CARGOPANTS_9			,
	LEGS_P2_CARGOPANTS_10			,
	LEGS_P2_BEACH					,
	LEGS_P2_BEACH_1					,
	LEGS_P2_BEACH_2					,
	LEGS_P2_BEACH_3					,
	LEGS_P2_BEACH_4					,
	LEGS_P2_BEACH_5					,
	LEGS_P2_BEACH_6					,
	LEGS_P2_BEACH_7					,
	LEGS_P2_SUIT_PANTS				,
	LEGS_P2_SUIT_PANTS_1			,
	LEGS_P2_SUIT_PANTS_2			,
	LEGS_P2_SUIT_PANTS_3			,
	LEGS_P2_SUIT_PANTS_4			,
	LEGS_P2_SUIT_PANTS_5			,
	LEGS_P2_SUIT_PANTS_6			,
	LEGS_P2_SUIT_PANTS_7			,
	LEGS_P2_SUIT_PANTS_8			,
	LEGS_P2_CHEAP_SUIT_PANTS		,
	LEGS_P2_CHEAP_SUIT_PANTS_1		,
	LEGS_P2_CHEAP_SUIT_PANTS_2		,
	LEGS_P2_CHEAP_SUIT_PANTS_3		,
	LEGS_P2_CHEAP_SUIT_PANTS_4		,
	LEGS_P2_CHEAP_SUIT_PANTS_5		,
	LEGS_P2_CHEAP_SUIT_PANTS_6		,
	LEGS_P2_CHEAP_SUIT_PANTS_7		,
	LEGS_P2_CHEAP_SUIT_PANTS_8		,
	LEGS_P2_CHEAP_SUIT_PANTS_9		,
	LEGS_P2_UNDERWEAR				,
	LEGS_P2_UNDERWEAR_1				,
	LEGS_P2_UNDERWEAR_2				,
	LEGS_P2_UNDERWEAR_3				,
	LEGS_P2_UNDERWEAR_4				,
	LEGS_P2_UNDERWEAR_5				,
	LEGS_P2_UNDERWEAR_6				,
	LEGS_P2_UNDERWEAR_7				,
	LEGS_P2_UNDERWEAR_8				,
	LEGS_P2_UNDERWEAR_9				,
	LEGS_P2_SWEAT_PANTS				,
	LEGS_P2_CHEAP_TUXEDO_PANTS		,
	LEGS_P2_TOILET					,
	LEGS_P2_UNDERWEAR_BF_0			,
	LEGS_P2_UNDERWEAR_BF_1			,
	LEGS_P2_UNDERWEAR_BF_2			,
	LEGS_P2_UNDERWEAR_BF_3			,
	LEGS_P2_UNDERWEAR_BF_4			,
	LEGS_P2_UNDERWEAR_BF_5			,
	LEGS_P2_UNDERWEAR_BF_6			,
	LEGS_P2_UNDERWEAR_BF_7			,
	LEGS_P2_UNDERWEAR_BF_8			,
	LEGS_P2_UNDERWEAR_BF_9			,
	LEGS_P2_DLC						,
	// If you add new legs which have feet attached update DO_LEGS_CONTAIN_FEET
	
	FEET_P2_BLACK_BOOTS  			= 0,
	FEET_P2_LEATHER_BOOTS_1			,
	FEET_P2_LEATHER_BOOTS_2 		,
	FEET_P2_LEATHER_BOOTS_3			,
	FEET_P2_LEATHER_BOOTS_4			,
	FEET_P2_LEATHER_BOOTS_5			,
	FEET_P2_LEATHER_BOOTS_6			,
	FEET_P2_LEATHER_BOOTS_7			,
	FEET_P2_DUMMY					,
	FEET_P2_BALLISTICS				,
	FEET_P2_SCUBA_WATER				,
	FEET_P2_TRIATHLON				,
	FEET_P2_DOCK_WORKER				,
	FEET_P2_ARM_GUARDS				,
	FEET_P2_SCUBA_LAND				,
	FEET_P2_DRESSY_SHOES			,
	FEET_P2_LOAFERS					,
	FEET_P2_LOAFERS_1				,
	FEET_P2_LOAFERS_2				,
	FEET_P2_LOAFERS_3				,
	FEET_P2_LOAFERS_4				,
	FEET_P2_LOAFERS_5				,
	FEET_P2_LOAFERS_6				,
	FEET_P2_LOAFERS_7				,
	FEET_P2_LOAFERS_8				,
	FEET_P2_LOAFERS_9				,
	FEET_P2_LOAFERS_10				,
	FEET_P2_LOAFERS_11				,
	FEET_P2_REDWINGS				,
	FEET_P2_TOILET					,
	FEET_P2_WINTER_BOOTS			,
	FEET_P2_LUDENDORFF				,
	FEET_P2_MOTO_X					,
	FEET_P2_SHOES_0					,
	FEET_P2_DUMMY_2					,
	FEET_P2_BARE_FEET				,
	FEET_P2_OXFORDS_0				,
	FEET_P2_OXFORDS_1				,
	FEET_P2_OXFORDS_2				,
	FEET_P2_OXFORDS_3				,
	FEET_P2_OXFORDS_4				,
	FEET_P2_OXFORDS_5				,
	FEET_P2_OXFORDS_6				,
	FEET_P2_OXFORDS_7				,
	FEET_P2_OXFORDS_8				,
	FEET_P2_OXFORDS_9				,
	FEET_P2_OXFORDS_10				,
	FEET_P2_OXFORDS_11				,
	FEET_P2_OXFORDS_12				,
	FEET_P2_OXFORDS_13				,
	FEET_P2_OXFORDS_14				,
	FEET_P2_OXFORDS_15				,
	FEET_P2_SQ_LOAFERS_0			,
	FEET_P2_SQ_LOAFERS_1			,
	FEET_P2_SQ_LOAFERS_2			,
	FEET_P2_SQ_LOAFERS_3			,
	FEET_P2_SQ_LOAFERS_4			,
	FEET_P2_SQ_LOAFERS_5			,
	FEET_P2_SQ_LOAFERS_6			,
	FEET_P2_SQ_LOAFERS_7			,
	FEET_P2_SQ_LOAFERS_8			,
	FEET_P2_SQ_LOAFERS_9			,
	FEET_P2_SQ_LOAFERS_10			,
	FEET_P2_SQ_LOAFERS_11			,
	FEET_P2_SQ_LOAFERS_12			,
	FEET_P2_SQ_LOAFERS_13			,
	FEET_P2_SQ_LOAFERS_14			,
	FEET_P2_SQ_LOAFERS_15			,
	FEET_P2_WINGTIPS_0				,
	FEET_P2_WINGTIPS_1				,
	FEET_P2_WINGTIPS_2				,
	FEET_P2_WINGTIPS_3				,
	FEET_P2_WINGTIPS_4				,
	FEET_P2_WINGTIPS_5				,
	FEET_P2_WINGTIPS_6				,
	FEET_P2_WINGTIPS_7				,
	FEET_P2_WINGTIPS_8				,
	FEET_P2_WINGTIPS_9				,
	FEET_P2_WINGTIPS_10				,
	FEET_P2_WINGTIPS_11				,
	FEET_P2_WINGTIPS_12				,
	FEET_P2_WINGTIPS_13				,
	FEET_P2_WINGTIPS_14				,
	FEET_P2_WINGTIPS_15				,
	FEET_P2_DLC						, // leave this at the bottom
	// if you add new dummy feet, update IS_PED_COMPONENT_ITEM_RESTRICTED_FOR_ITEM_SP
	
	HAND_P2_NONE 					= 0,
	HAND_P2_FROZEN 					,
	HAND_P2_FIREMAN					,
	HAND_P2_DOCK_WORKER 			,
	HAND_P2_SCUBA					,
	HAND_P2_FINGERLESS_GLOVES		,
	HAND_P2_MOTO_X					,
	HAND_P2_DLC						,
	
	SPECIAL_P2_WATCH 				= 0,
	SPECIAL_P2_WATCH_1	 			,
	SPECIAL_P2_FIREMAN_ACCS	 		,
	SPECIAL_P2_BALLISTICS 			,
	SPECIAL_P2_PARACHUTE 			,
	SPECIAL_P2_DOCK_WORKER 			,
	SPECIAL_P2_SCUBA				,
	SPECIAL_P2_BALACLAVA			,
	SPECIAL_P2_SECURITY				,
	SPECIAL_P2_CLOWN	 			,	
	SPECIAL_P2_STAR_TATTOOS			,	
	SPECIAL_P2_NCHEIFMASK	 		,	
	SPECIAL_P2_BOWTIE		 		, 
	SPECIAL_P2_TOILET				,	
	SPECIAL_P2_MASK					,	
	SPECIAL_P2_DUMMY 				,	
	SPECIAL_P2_FIREMAN_ACCS_1	 	,
	SPECIAL_P2_SCUBA_1				,
	SPECIAL_P2_DLC					, // leave this at the bottom
	
	SPECIAL2_P2_NONE 				= 0,
	SPECIAL2_P2_BAG 				,		
	SPECIAL2_P2_BAG_1 				,		
	SPECIAL2_P2_BAG_2 				,		
	SPECIAL2_P2_BAG_3 				,	
	SPECIAL2_P2_COP_ACCS 			,
	SPECIAL2_P2_HIGHWAY_PATROL 		,			
	SPECIAL2_P2_DUMMY				,		
	SPECIAL2_P2_MASK 				,	
	SPECIAL2_P2_WRESTLER_MASK_0		,
	SPECIAL2_P2_WRESTLER_MASK_1		,
	SPECIAL2_P2_WRESTLER_MASK_2		,
	SPECIAL2_P2_WRESTLER_MASK_3		,
	SPECIAL2_P2_WRESTLER_MASK_4		,
	SPECIAL2_P2_WRESTLER_MASK_5		,
	SPECIAL2_P2_MASK_MONSTER_GREEN	,
	SPECIAL2_P2_MASK_MONSTER_RED	,
	SPECIAL2_P2_DLC						,
	
	DECL_P2_NONE 					= 0,
	DECL_P2_FIREMAN 				,
	DECL_P2_PARAMEDIC				,
	DECL_P2_HIGHWAY_PATROL			,
	DECL_P2_COP						,
	DECL_P2_AMMUNATION_0			,
	DECL_P2_AMMUNATION_1			,
	DECL_P2_AMMUNATION_2			,
	DECL_P2_AMMUNATION_3			,
	DECL_P2_AMMUNATION_4			,
	DECL_P2_AMMUNATION_5			,
	DECL_P2_BOWLING_0				,
	DECL_P2_BOWLING_1				,
	DECL_P2_BOWLING_2				,
	DECL_P2_BOWLING_3				,
	DECL_P2_BOWLING_4				,
	DECL_P2_BOWLING_5				,
	DECL_P2_BOWLING_6				,
	DECL_P2_BOWLING_7				,
	DECL_P2_BOWLING_8				,
	DECL_P2_BOWLING_9				,
	DECL_P2_LOGO_0					,
	DECL_P2_SPORTS_0				,
	DECL_P2_SPORTS_1				,
	DECL_P2_SPORTS_2				,
	DECL_P2_SPORTS_3				,
	DECL_P2_SPORTS_4				,
	DECL_P2_SPORTS_5				,
	DECL_P2_SPORTS_6				,
	DECL_P2_MASK_0					,
	DECL_P2_MASK_1					,
	DECL_P2_MASK_2					,
	DECL_P2_MASK_3					,
	DECL_P2_DLC						, // leave this at the bottom

	BERD_P2_NONE 					= 0,
	BERD_P2_PROLOGUE				,
	BERD_P2_2_0						,
	BERD_P2_3_0						,
	BERD_P2_4_0						,
	BERD_P2_5_0						,
	BERD_P2_DLC						,// leave this at the bottom
	
	TEETH_P2_NONE 					= 0,
	TEETH_P2_DLC						,
	
	JBIB_P2_NONE 					= 0,
	JBIB_P2_DLC						,
	
	OUTFIT_P2_DEFAULT 				= OUTFIT_DEFAULT,
	OUTFIT_P2_BALLISTICS 			,
	OUTFIT_P2_DOCK_WORKER			,
	OUTFIT_P2_HIGHWAY_PATROL 		,
	OUTFIT_P2_GOLF			 		,
	OUTFIT_P2_TENNIS		 		,
	OUTFIT_P2_SCUBA_WATER	 		,
	OUTFIT_P2_SCUBA_LAND	 		,
	OUTFIT_P2_STEALTH		 		,
	OUTFIT_P2_TRIATHLON				,
	OUTFIT_P2_UNDERWEAR				,
	OUTFIT_P2_SECURITY				,
	OUTFIT_P2_TOILET				,
	OUTFIT_P2_PROLOGUE				,
	OUTFIT_P2_TUXEDO				,
	OUTFIT_P2_LADIES				,
	OUTFIT_P2_BLUE_BOILER_SUIT		,
	OUTFIT_P2_PREP_BOILER_SUIT_1	,
	OUTFIT_P2_PREP_BOILER_SUIT_2	,
	OUTFIT_P2_PREP_BOILER_SUIT_3	,
	OUTFIT_P2_HUNTING				,
	OUTFIT_P2_TSHIRT_CARGOPANTS_1	,
	OUTFIT_P2_DENIM					,
	OUTFIT_P2_LUDENDORFF			,
	OUTFIT_P2_CHEAPSUIT_0			,
	OUTFIT_P2_CHEAPSUIT_1			,
	OUTFIT_P2_CHEAPSUIT_2			,
	OUTFIT_P2_CHEAPSUIT_3			,
	OUTFIT_P2_CHEAPSUIT_4			,
	OUTFIT_P2_CHEAPSUIT_5			,
	OUTFIT_P2_CHEAPSUIT_6			,
	OUTFIT_P2_CHEAPSUIT_7			,
	OUTFIT_P2_CHEAPSUIT_8			,
	OUTFIT_P2_CHEAPSUIT_9			,
	OUTFIT_P2_STYLESUIT_0			,
	OUTFIT_P2_STYLESUIT_1			,
	OUTFIT_P2_STYLESUIT_2			,
	OUTFIT_P2_STYLESUIT_3			,
	OUTFIT_P2_STYLESUIT_4			,
	OUTFIT_P2_STYLESUIT_5			,
	OUTFIT_P2_STYLESUIT_6			,
	OUTFIT_P2_TSHIRT_CARGOPANTS_2	,
	OUTFIT_P2_TSHIRT_JEANS_1		,
	OUTFIT_P2_TSHIRT_CARGOPANTS_3	,
	OUTFIT_P2_MOTO_X				, 
	OUTFIT_P2_TANKTOP_SWEATPANTS_1	, 
	OUTFIT_P2_TSHIRT_JEANS_2		,
	OUTFIT_P2_STEALTH_NO_MASK		, //47
	OUTFIT_P2_DLC					, // leave this at the bottom
	// If adding outfits, you may need to increase PED_COMPONENTS_OUTFITS

	PROPGROUP_P2_COP 				= 0,
	PROPGROUP_P2_DOCK_WORKER		,
	PROPGROUP_P2_HIGHWAY_PATROL		,
	PROPGROUP_P2_SCUBA_WATER		,
	PROPGROUP_P2_SCUBA_LAND			,
	PROPGROUP_P2_SECURITY			,
	PROPGROUP_P2_TRIATHLON			,
	PROPGROUP_P2_SUNGLASSES			,
	PROPGROUP_P2_BALLISTICS			,
	PROPGROUP_P2_DLC				, // leave this at the bottom
	
	// props need to be ordered by anchor and then drawable / variation
	// if the 1st prop of a type changes, update GET_FIRST_PROP_OF_TYPE
	// ANCHOR_HEAD, 
	PROPS_P2_FIREMAN_HAT 			= 10, // Start props at 10.
	PROPS_P2_COP_HAT 				,
	PROPS_P2_HOCKEY_MASK 			,
	PROPS_P2_EAR_DEFENDERS			,
	PROPS_P2_HARD_HAT 				,
	PROPS_P2_COP_HELMET				,
	PROPS_P2_SECURITY_HELMET		,
	PROPS_P2_BEANIE_HAT				,
	PROPS_P2_BEANIE_HAT_1			,
	PROPS_P2_CLOWN_HAT				,
	PROPS_P2_CRASH_HELMET			,
	PROPS_P2_CRASH_HELMET_1			,
	PROPS_P2_CRASH_HELMET_2			,
	PROPS_P2_CRASH_HELMET_3			,
	PROPS_P2_CRASH_HELMET_4			,
	PROPS_P2_CRASH_HELMET_5			,
	PROPS_P2_CRASH_HELMET_6			,
	PROPS_P2_CRASH_HELMET_7			,
	PROPS_P2_CRASH_HELMET_8			,
	PROPS_P2_CRASH_HELMET_9			,
	PROPS_P2_CRASH_HELMET_10		,
	PROPS_P2_CRASH_HELMET_11		,
	PROPS_P2_CRASH_HELMET_12		,
	PROPS_P2_CRASH_HELMET_13		,
	PROPS_P2_CRASH_HELMET_14		,
	PROPS_P2_CRASH_HELMET_15		,
	PROPS_P2_HEADBAND				,
	PROPS_P2_ARMY_HELMET			,
	PROPS_P2_ARMY_HELMET_1			,
	PROPS_P2_ARMY_HELMET_2			,
	PROPS_P2_ARMY_HELMET_3			,
	PROPS_P2_ARMY_HELMET_4			,
	PROPS_P2_ARMY_HELMET_5			,
	PROPS_P2_ARMY_HELMET_6			,
	PROPS_P2_ARMY_HELMET_7			,
	PROPS_P2_PILOT_HEADPHONES		,
	PROPS_P2_HEADPHONES				,
	PROPS_P2_MASK_MONSTER_GREEN		,
	PROPS_P2_MASK_MONSTER_RED		,
	PROPS_P2_MASK_PIG				,
	PROPS_P2_MASK_PIG_DARK			,
	PROPS_P2_MASK_SKULL_GREY		,
	PROPS_P2_MASK_SKULL_YELLOW		,
	PROPS_P2_MASK_MONKEY			,
	PROPS_P2_MASK_HOCKEY_WHITE		,
	PROPS_P2_MASK_HOCKEY_RED		,
	PROPS_P2_MASK_APE				,
	PROPS_P2_MASK_APE_DARK			,
	PROPS_P2_MASK_TRIBAL_1			,
	PROPS_P2_MASK_TRIBAL_2			,
	PROPS_P2_MASK_TRIBAL_3			,
	PROPS_P2_FLIGHT_CAP				,
	PROPS_P2_SWIM_CAP				,
	PROPS_P2_MOTO_X_0				,
	PROPS_P2_MOTO_X_1				,
	PROPS_P2_MOTO_X_2				,
	PROPS_P2_MOTO_X_3				,
	PROPS_P2_MOTO_X_4				,
	PROPS_P2_MOTO_X_5				,
	PROPS_P2_ARMOURED_HELMET		,
	PROPS_P2_BLACK_HELMET			,
	PROPS_P2_CAP_0					,
	PROPS_P2_CAP_1					,
	PROPS_P2_CAP_2					,
	PROPS_P2_CAP_3					,
	PROPS_P2_CAP_4					,
	PROPS_P2_CAP_5					,
	PROPS_P2_CAP_6					,
	PROPS_P2_CAP_7					,
	PROPS_P2_CAP_8					,
	PROPS_P2_CAP_9					,
	PROPS_P2_CAP_10					,
	PROPS_P2_CAP_11					,
	PROPS_P2_CAP_12					,
	PROPS_P2_CAP_13					,
	PROPS_P2_CAP_14					,
	PROPS_P2_CAP_15					,
	PROPS_P2_FIGHTER_JET_HELMET		,
	
	//ANCHOR_EYES, 
	PROPS_P2_SCUBA_MASK				,
	PROPS_P2_SUNGLASSES				,
	PROPS_P2_GLASSES				,
	PROPS_P2_GLASSES_1				,
	PROPS_P2_GLASSES_2				,
	PROPS_P2_GLASSES_3				,
	PROPS_P2_GLASSES_4				,
	PROPS_P2_GLASSES_5				,
	PROPS_P2_GLASSES_6				,
	PROPS_P2_GLASSES_7				,
	PROPS_P2_GLASSES_8				,
	PROPS_P2_GLASSES_9				,
	PROPS_P2_SHADES_A_0				,
	PROPS_P2_SHADES_A_1				,
	PROPS_P2_SHADES_A_2				,
	PROPS_P2_SHADES_A_3				,
	PROPS_P2_SHADES_A_4				,
	PROPS_P2_SHADES_A_5				,
	PROPS_P2_SHADES_A_6				,
	PROPS_P2_SHADES_A_7				,
	PROPS_P2_SHADES_A_8				,
	PROPS_P2_SHADES_A_9				,
	PROPS_P2_WORK_MASK				,
	PROPS_P2_BIKE_GOGGLES			,
	PROPS_P2_GOGGLES				,
	PROPS_P2_SHADES_B_0				,
	PROPS_P2_SHADES_B_1				,
	PROPS_P2_SHADES_B_2				,
	PROPS_P2_SHADES_B_3				,
	PROPS_P2_SHADES_B_4				,
	PROPS_P2_SHADES_B_5				,
	PROPS_P2_SHADES_B_6				,
	PROPS_P2_SHADES_B_7				,
	PROPS_P2_SHADES_B_8				,
	PROPS_P2_SHADES_B_9				,
	PROPS_P2_SUNGLASSES_B_0			,
	PROPS_P2_SUNGLASSES_B_1			,
	PROPS_P2_SUNGLASSES_B_2			,
	PROPS_P2_SUNGLASSES_B_3			,
	PROPS_P2_SUNGLASSES_B_4			,
	PROPS_P2_SUNGLASSES_B_5			,
	PROPS_P2_SUNGLASSES_B_6			,
	PROPS_P2_SUNGLASSES_B_7			,
	PROPS_P2_SUNGLASSES_B_8			,
	PROPS_P2_SUNGLASSES_B_9			,
	PROPS_P2_SUNGLASSES_C_0			,
	PROPS_P2_SUNGLASSES_C_1			,
	PROPS_P2_SUNGLASSES_C_2			,
	PROPS_P2_SUNGLASSES_C_3			,
	PROPS_P2_SUNGLASSES_C_4			,
	PROPS_P2_SUNGLASSES_C_5			,
	PROPS_P2_SUNGLASSES_C_6			,
	PROPS_P2_SUNGLASSES_C_7			,
	PROPS_P2_SUNGLASSES_C_8			,
	PROPS_P2_SUNGLASSES_C_9			,
	PROPS_P2_SQUARE_GLASSES_0		,
	PROPS_P2_SQUARE_GLASSES_1		,
	PROPS_P2_SQUARE_GLASSES_2		,
	PROPS_P2_SQUARE_GLASSES_3		,
	PROPS_P2_SQUARE_GLASSES_4		,
	PROPS_P2_SQUARE_GLASSES_5		,
	PROPS_P2_SQUARE_GLASSES_6		,
	PROPS_P2_SQUARE_GLASSES_7		,
	PROPS_P2_SQUARE_GLASSES_8		,
	PROPS_P2_SQUARE_GLASSES_9		,
	PROPS_P2_GLASSES_RANGE_0,		//New shooting range glasses
	
	//ANCHOR_EARS
	PROPS_P2_HEADSET				,
	
	//ANCHOR_MOUTH,
   	//ANCHOR_LEFT_HAND,
    //ANCHOR_RIGHT_HAND,
   	//ANCHOR_LEFT_WRIST,
    //ANCHOR_RIGHT_WRIST,
    //ANCHOR_HIP

	PROPS_P2_DLC					, // leave this at the bottom
	
	////////////////////////////////////////////////////////
	/// FRANKLIN
	HEAD_P1_0_0						= 0,
	HEAD_P1_0_1						,
	HEAD_P1_0_2						,
	HEAD_P1_0_3						,
	HEAD_P1_0_4						, // bald
	HEAD_P1_0_5						,
	HEAD_P1_0_6						,
	HEAD_P1_0_7						,
	HEAD_P1_0_8						,
	HEAD_P1_0_9						,
	HEAD_P1_DLC						,// leave this at the bottom
	
	HAIR_P1_SHORT_1 				= 0,	// 0_0
	HAIR_P1_SHORT_2 				,
	HAIR_P1_SHORT_3 				,
	HAIR_P1_SHORT_4 				,
	HAIR_P1_SHORT_5 				,
	HAIR_P1_SHORT_6 				,
	HAIR_P1_SHORT_7 				,
	HAIR_P1_SHORT_8 				,
	HAIR_P1_SHORT_9 				,
	HAIR_P1_SHORT_10 				,
	HAIR_P1_SHORT_11 				,
	HAIR_P1_SHORT_12 				,
	HAIR_P1_SHORT_13 				,
	HAIR_P1_SHORT_14 				,
	HAIR_P1_SHORT_15 				,
	HAIR_P1_SHORT_16 				,	// 0_15
	HAIR_P1_1_0 					,	// 1_0
	HAIR_P1_2_0 					,	// 2_0
	HAIR_P1_3_0 					,	// 3_0
	HAIR_P1_VERY_SHORT 				,	// 4_0
	HAIR_P1_MASK 					,	// 5_0
	HAIR_P1_DLC						,// leave this at the bottom
	
	TORSO_P1_WHITE_VEST				= 0,	// 0_0
	TORSO_P1_BLACK_VEST				,		// 0_1
	TORSO_P1_GRAY_VEST				,		// 0_2
	TORSO_P1_FEUD_GREEN_VEST		,		// 0_3
	TORSO_P1_FRUNTALOT_BLUE_VEST	,		// 0_4
	TORSO_P1_YELLOWBROWN_VEST		,		// 0_5
	TORSO_P1_ORANGESTRIPE_VEST		,		// 0_6
	TORSO_P1_GREEN_WHITE_VEST		,		// 0_7
	TORSO_P1_YELLOW_VEST	 		,		// 0_8
	TORSO_P1_PURPLE_VEST			,		// 0_9
	TORSO_P1_RED_VEST				,		// 0_10
	TORSO_P1_WHITESTRIPE_VEST		,		// 0_11
	TORSO_P1_CAMO_VEST				,		// 0_12
	TORSO_P1_FEUD3_WHITE_VEST	 	,		// 0_13
	TORSO_P1_GRAYSTRIPE_VEST		,		// 0_14
	TORSO_P1_FEUD_WHITE_VEST		,		// 0_15	
	TORSO_P1_BLACK_BOILER			,		// 1_0 
	TORSO_P1_RED_BOILER				,		// 1_1
	TORSO_P1_BLUE_BOILER			,		// 1_2
	TORSO_P1_NAVY_BOILER			,		// 1_3
	TORSO_P1_GRAY_BOILER			,		// 1_4
	TORSO_P1_GREEN_BOILER			,		// 1_5
	TORSO_P1_WHITE_TUXEDO			,		// 2_0
	TORSO_P1_SCUBA					,		// 3_0
	TORSO_P1_BAGGY_TEE_0			,		// 4_0
	TORSO_P1_BAGGY_TEE_1			,		// 4_1
	TORSO_P1_BAGGY_TEE_2			,		// 4_2
	TORSO_P1_BAGGY_TEE_3			,		// 4_3
	TORSO_P1_BAGGY_TEE_4			,		// 4_4
	TORSO_P1_BAGGY_TEE_5			,		// 4_5
	TORSO_P1_BAGGY_TEE_6			,		// 4_6
	TORSO_P1_BAGGY_TEE_7			,		// 4_7
	TORSO_P1_BAGGY_TEE_8			,		// 4_8
	TORSO_P1_BAGGY_TEE_9			,		// 4_9
	TORSO_P1_BAGGY_TEE_10			,		// 4_10
	TORSO_P1_BAGGY_TEE_11			,		// 4_11
	TORSO_P1_BAGGY_TEE_12			,		// 4_12
	TORSO_P1_BAGGY_TEE_13			,		// 4_13
	TORSO_P1_BAGGY_TEE_14			,		// 4_14
	TORSO_P1_BAGGY_TEE_15			,		// 4_15
	TORSO_P1_TRIATHLON				,		// 5_0
	TORSO_P1_GOLF					,		// 6_0
	TORSO_P1_SWEATER_1				,		// 6_1
	TORSO_P1_SWEATER_2				,		// 6_2
	TORSO_P1_SWEATER_3				,		// 6_3
	TORSO_P1_SWEATER_4				,		// 6_4
	TORSO_P1_SWEATER_5				,		// 6_5
	TORSO_P1_SWEATER_6				,		// 6_6
	TORSO_P1_SWEATER_7				,		// 6_7
	TORSO_P1_SWEATER_8				,		// 6_8
	TORSO_P1_SWEATER_9				,		// 6_9
	TORSO_P1_SWEATER_10				,		// 6_10
	TORSO_P1_SWEATER_11				,		// 6_11
	TORSO_P1_SWEATER_12				,		// 6_12
	TORSO_P1_SWEATER_13				,		// 6_13
	TORSO_P1_SWEATER_14				,		// 6_14
	TORSO_P1_SWEATER_15				,		// 6_15
	TORSO_P1_SHIRT_0				,		// 7_0
	TORSO_P1_SHIRT_1				,		// 7_1
	TORSO_P1_SHIRT_2				,		// 7_2
	TORSO_P1_SHIRT_3				,		// 7_3
	TORSO_P1_SHIRT_4				,		// 7_4
	TORSO_P1_SHIRT_5				,		// 7_5
	TORSO_P1_SHIRT_6				,		// 7_6
	TORSO_P1_SHIRT_7				,		// 7_7
	TORSO_P1_SHIRT_8				,		// 7_8
	TORSO_P1_SHIRT_9				,		// 7_9
	TORSO_P1_SHIRT_10				,		// 7_10
	TORSO_P1_SHIRT_11				,		// 7_11
	TORSO_P1_SHIRT_12				,		// 7_12
	TORSO_P1_SHIRT_13				,		// 7_13
	TORSO_P1_SHIRT_14				,		// 7_14
	TORSO_P1_SHIRT_15				,		// 7_15
	TORSO_P1_BLUE_SHIRT				,		// 8_0
	TORSO_P1_YELLOW_SHIRT			,		// 8_1
	TORSO_P1_OFF_WHITE_SHIRT		,		// 8_2
	TORSO_P1_DARK_GRAY_SHIRT		,		// 8_3
	TORSO_P1_SALMON_CHECK_SHIRT		,		// 8_4
	TORSO_P1_BLACK_SHIRT			,		// 8_5
	TORSO_P1_BLUE_CHECK_SHIRT		,		// 8_6
	TORSO_P1_CHECK_SHIRT			,		// 8_7
	TORSO_P1_BLUEGRN_CHECK_SHIRT	,		// 8_8
	TORSO_P1_ORANGE_SHIRT			,		// 8_9
	TORSO_P1_BROWN_CHECK_SHIRT		,		// 8_10
	TORSO_P1_GRAY_CHECK_SHIRT		,		// 8_11
	TORSO_P1_PURPLE_CHECK_SHIRT		,		// 8_12
	TORSO_P1_GREEN_SHIRT		 	,		// 8_13
	TORSO_P1_WHITE_SHIRT			,		// 8_14
	TORSO_P1_GREEN_CHECK_SHIRT		,		// 8_15	
	TORSO_P1_MOTO_X					,		// 9_0
	TORSO_P1_FIREMAN				,		// 10_0
	TORSO_P1_WHITE_LNGSLEEVE		,		// 11_0
	TORSO_P1_GRAY_LNGSLEEVE			,		// 11_1
	TORSO_P1_BLACK_LNGSLEEVE		,		// 11_2
	TORSO_P1_TEN_LNGSLEEVE			,		// 11_3
	TORSO_P1_KING_LNGSLEEVE			,		// 11_4
	TORSO_P1_BLACK_YETI_LNGSLEEVE	,		// 11_5
	TORSO_P1_FRUNTALOT_LNGSLEEVE	,		// 11_6
	TORSO_P1_BLUE_LNGSLEEVE			,		// 11_7
	TORSO_P1_STRIPED_LNGSLEEVE		,		// 11_8
	TORSO_P1_GREEN_LNGSLEEVE		,		// 11_9
	TORSO_P1_SWEATBOX_LNGSLEEVE		,		// 11_10
	TORSO_P1_KHAKI_LNGSLEEVE		,		// 11_11
	TORSO_P1_CAMO_YETI_LNGSLEEVE	,		// 11_12
	TORSO_P1_FEUD_LNGSLEEVE			,		// 11_13
	TORSO_P1_BROKER_LNGSLEEVE		,		// 11_14
	TORSO_P1_UPTOWN_LNGSLEEVE		,		// 11_15
	TORSO_P1_VARSITY				,		// 12_0
	TORSO_P1_VARSITY_1				,		// 12_1
	TORSO_P1_VARSITY_2				,		// 12_2
	TORSO_P1_VARSITY_3				,		// 12_3
	TORSO_P1_VARSITY_4				,		// 12_4
	TORSO_P1_VARSITY_5				,		// 12_5
	TORSO_P1_VARSITY_6				,		// 12_6
	TORSO_P1_VARSITY_7				,		// 12_7
	TORSO_P1_VARSITY_8				,		// 12_8
	TORSO_P1_VARSITY_9				,		// 12_9
	TORSO_P1_VARSITY_10				,		// 12_10
	TORSO_P1_VARSITY_11				,		// 12_11
	TORSO_P1_VARSITY_12				,		// 12_12
	TORSO_P1_VARSITY_13				,		// 12_13
	TORSO_P1_VARSITY_14				,		// 12_14
	TORSO_P1_VARSITY_15				,		// 12_15
	TORSO_P1_HOODIE					,		// 13_0
	TORSO_P1_HOODIE_1				,		// 13_1
	TORSO_P1_HOODIE_2				,		// 13_2
	TORSO_P1_HOODIE_3				,		// 13_3
	TORSO_P1_HOODIE_4				,		// 13_4
	TORSO_P1_HOODIE_5				,		// 13_5
	TORSO_P1_HOODIE_6				,		// 13_6
	TORSO_P1_HOODIE_7				,		// 13_7
	TORSO_P1_HOODIE_8				,		// 13_8
	TORSO_P1_HOODIE_9				,		// 13_9
	TORSO_P1_HOODIE_10				,		// 13_10
	TORSO_P1_HOODIE_11				,		// 13_11
	TORSO_P1_HOODIE_12				,		// 13_12
	TORSO_P1_HOODIE_13				,		// 13_13
	TORSO_P1_HOODIE_14				,		// 13_14
	TORSO_P1_HOODIE_15				,		// 13_15
	TORSO_P1_ARMY_JACKET			,		// 14_0
	TORSO_P1_ARMY_JACKET_1			,		// 14_1
	TORSO_P1_ARMY_JACKET_2			,		// 14_2
	TORSO_P1_ARMY_JACKET_3			,		// 14_3
	TORSO_P1_ARMY_JACKET_4			,		// 14_4
	TORSO_P1_ARMY_JACKET_5			,		// 14_5
	TORSO_P1_ARMY_JACKET_6			,		// 14_6
	TORSO_P1_ARMY_JACKET_7			,		// 14_7
	TORSO_P1_SHORT_SLEEVE			,		// 15_0
	TORSO_P1_SHORT_SLEEVE_1			,		// 15_1
	TORSO_P1_SHORT_SLEEVE_2			,		// 15_2
	TORSO_P1_SHORT_SLEEVE_3			,		// 15_3
	TORSO_P1_100_PERCENT_TSHIRT		,		// 15_4
	TORSO_P1_SHORT_SLEEVE_5			,		// 15_5
	TORSO_P1_SHORT_SLEEVE_6			,		// 15_6
	TORSO_P1_SHORT_SLEEVE_7			,		// 15_7
	TORSO_P1_SHORT_SLEEVE_8			,		// 15_8
	TORSO_P1_SHORT_SLEEVE_9			,		// 15_9
	TORSO_P1_SHORT_SLEEVE_10		,		// 15_10
	TORSO_P1_SHORT_SLEEVE_11		,		// 15_11
	TORSO_P1_STEALTH				,		// 16_0
	TORSO_P1_TRACKSUIT_0			,		// 17_0
	TORSO_P1_TRACKSUIT_1			,		// 17_1
	TORSO_P1_TRACKSUIT_2			,		// 17_2
	TORSO_P1_TRACKSUIT_3			,		// 17_3
	TORSO_P1_TRACKSUIT_4			,		// 17_4
	TORSO_P1_TRACKSUIT_5			,		// 17_5
	TORSO_P1_TRACKSUIT_6			,		// 17_6
	TORSO_P1_TRACKSUIT_7			,		// 17_7
	TORSO_P1_TRACKSUIT_8			,		// 17_8
	TORSO_P1_TRACKSUIT_9			,		// 17_9
	TORSO_P1_TRACKSUIT_10			,		// 17_10
	TORSO_P1_TRACKSUIT_11			,		// 17_11
	TORSO_P1_TRACKSUIT_12			,		// 17_12
	TORSO_P1_TRACKSUIT_13			,		// 17_13
	TORSO_P1_TRACKSUIT_14			,		// 17_14
	TORSO_P1_TRACKSUIT_15			,		// 17_15
	TORSO_P1_SUIT					,		// 18_0
	TORSO_P1_SUIT_1					,		// 18_1
	TORSO_P1_SUIT_2					,		// 18_2
	TORSO_P1_SUIT_3					,		// 18_3
	TORSO_P1_SUIT_4					,		// 18_4
	TORSO_P1_SUIT_5					,		// 18_5
	TORSO_P1_SUIT_6					,		// 18_6
	TORSO_P1_SUIT_7					,		// 18_7
	TORSO_P1_SUIT_8					,		// 18_8
	TORSO_P1_SUIT_9					,		// 18_9
	TORSO_P1_SUIT_10				,		// 18_10
	TORSO_P1_SUIT_11				,		// 18_11
	TORSO_P1_SUIT_12				,		// 18_12
	TORSO_P1_SUIT_13				,		// 18_13
	TORSO_P1_SUIT_14				,		// 18_14
	TORSO_P1_SUIT_15				,		// 18_15
	TORSO_P1_DRESS_SHIRT			,		// 19_0
	TORSO_P1_SHIRT_UP_SLEEVES_1		,		// 19_1
	TORSO_P1_SHIRT_UP_SLEEVES_2		,		// 19_2
	TORSO_P1_SHIRT_UP_SLEEVES_3		,		// 19_3
	TORSO_P1_SHIRT_UP_SLEEVES_4		,		// 19_4
	TORSO_P1_SHIRT_UP_SLEEVES_5		,		// 19_5
	TORSO_P1_SHIRT_UP_SLEEVES_6		,		// 19_6
	TORSO_P1_SHIRT_UP_SLEEVES_7		,		// 19_7
	TORSO_P1_SHIRT_UP_SLEEVES_8		,		// 19_8
	TORSO_P1_SHIRT_UP_SLEEVES_9		,		// 19_9
	TORSO_P1_SHIRT_UP_SLEEVES_10	,		// 19_10
	TORSO_P1_SHIRT_UP_SLEEVES_11	,		// 19_11
	TORSO_P1_SHIRT_UP_SLEEVES_12	,		// 19_12
	TORSO_P1_SHIRT_UP_SLEEVES_13	,		// 19_13
	TORSO_P1_SHIRT_UP_SLEEVES_14	,		// 19_14
	TORSO_P1_SHIRT_UP_SLEEVES_15	,		// 19_15
	TORSO_P1_SKYDIVING				,		// 20_0
	TORSO_P1_JACKET_0				,		// 21_0
	TORSO_P1_JACKET_1				,		// 21_0
	TORSO_P1_JACKET_2				,		// 21_0
	TORSO_P1_JACKET_3				,		// 21_0
	TORSO_P1_JACKET_4				,		// 21_0
	TORSO_P1_JACKET_5				,		// 21_0
	TORSO_P1_JACKET_6				,		// 21_0
	TORSO_P1_JACKET_7				,		// 21_0
	TORSO_P1_JACKET_8				,		// 21_8
	TORSO_P1_JACKET_9				,		// 21_9
	TORSO_P1_JACKET_10				,		// 21_10
	TORSO_P1_JACKET_11				,		// 21_11
	TORSO_P1_JACKET_12				,		// 21_12
	TORSO_P1_JACKET_13				,		// 21_13
	TORSO_P1_WOOL_PEACOAT			,		// 22_0
	TORSO_P1_WOOL_PEACOAT_1			,		// 22_1
	TORSO_P1_WOOL_PEACOAT_2			,		// 22_2
	TORSO_P1_WOOL_PEACOAT_3			,		// 22_3
	TORSO_P1_3PCSUIT				,		// 23_1
	TORSO_P1_3PCSUIT_1				,		// 23_1
	TORSO_P1_3PCSUIT_2				,		// 23_2
	TORSO_P1_3PCSUIT_3				,		// 23_3
	TORSO_P1_3PCSUIT_4				,		// 23_4
	TORSO_P1_3PCSUIT_5				,		// 23_5
	TORSO_P1_3PCSUIT_6				,		// 23_6
	TORSO_P1_3PCSUIT_7				,		// 23_7
	TORSO_P1_3PCSUIT_8				,		// 23_8
	TORSO_P1_3PCSUIT_9				,		// 23_9
	TORSO_P1_3PCSUIT_10				,		// 23_10
	TORSO_P1_3PCSUIT_11				,		// 23_11
	TORSO_P1_3PCSUIT_12				,		// 23_12
	TORSO_P1_3PCSUIT_13				,		// 23_13
	TORSO_P1_3PCSUIT_14				,		// 23_14
	TORSO_P1_3PCSUIT_15				,		// 23_15
	TORSO_P1_WAISTCOAT				,		// 24_1
	TORSO_P1_WAISTCOAT_1			,		// 24_1
	TORSO_P1_WAISTCOAT_2			,		// 24_2
	TORSO_P1_WAISTCOAT_3			,		// 24_3
	TORSO_P1_WAISTCOAT_4			,		// 24_4
	TORSO_P1_WAISTCOAT_5			,		// 24_5
	TORSO_P1_WAISTCOAT_6			,		// 24_6
	TORSO_P1_WAISTCOAT_7			,		// 24_7
	TORSO_P1_WAISTCOAT_8			,		// 24_8
	TORSO_P1_WAISTCOAT_9			,		// 24_9
	TORSO_P1_WAISTCOAT_10			,		// 24_10
	TORSO_P1_WAISTCOAT_11			,		// 24_11
	TORSO_P1_WAISTCOAT_12			,		// 24_12
	TORSO_P1_WAISTCOAT_13			,		// 24_13
	TORSO_P1_WAISTCOAT_14			,		// 24_14
	TORSO_P1_WAISTCOAT_15			,		// 24_15
	TORSO_P1_TUXEDO_AND_SHIRT		,		// 25_0
	TORSO_P1_BARE_CHEST				,		// 26_0
	TORSO_P1_SHOOTING_VEST_0		,		// 27_0
	TORSO_P1_SHOOTING_VEST_1		,		// 27_1
	TORSO_P1_SHOOTING_VEST_2		,		// 27_2
	TORSO_P1_SHOOTING_VEST_3		,		// 27_3
	TORSO_P1_SHOOTING_VEST_4		,		// 27_4
	TORSO_P1_SHOOTING_VEST_5		,		// 27_5
	TORSO_P1_GRAY_HOODIE			,		// 28_0
	TORSO_P1_AMMUN_HOODIE_1			,		// 28_1
	TORSO_P1_AMMUN_HOODIE_2			,		// 28_2
	TORSO_P1_AMMUN_HOODIE_3			,		// 28_3
	TORSO_P1_AMMUN_HOODIE_4			,		// 28_4
	TORSO_P1_AMMUN_HOODIE_5			,		// 28_5
	TORSO_P1_AMMUN_HOODIE_6			,		// 28_6
	TORSO_P1_AMMUN_HOODIE_7			,		// 28_7
	TORSO_P1_AMMUN_HOODIE_8			,		// 28_8
	TORSO_P1_AMMUN_HOODIE_9			,		// 28_9
	TORSO_P1_AMMUN_HOODIE_10		,		// 28_10
	TORSO_P1_AMMUN_HOODIE_11		,		// 28_11
	TORSO_P1_AMMUN_HOODIE_12		,		// 28_12
	TORSO_P1_AMMUN_HOODIE_13		,		// 28_13
	TORSO_P1_AMMUN_HOODIE_14		,		// 28_14
	TORSO_P1_AMMUN_HOODIE_15		,		// 28_15
	TORSO_P1_T_SHIRT_0				,		// 29_0
	TORSO_P1_T_SHIRT_1				,		// 29_1
	TORSO_P1_T_SHIRT_2				,		// 29_2
	TORSO_P1_T_SHIRT_3				,		// 29_3
	TORSO_P1_T_SHIRT_4				,		// 29_4
	TORSO_P1_T_SHIRT_5				,		// 29_5
	TORSO_P1_T_SHIRT_6				,		// 29_6
	TORSO_P1_T_SHIRT_7				,		// 29_7
	TORSO_P1_T_SHIRT_8				,		// 29_8
	TORSO_P1_T_SHIRT_9				,		// 29_9
	TORSO_P1_T_SHIRT_10				,		// 29_10
	TORSO_P1_T_SHIRT_11				,		// 29_11
	TORSO_P1_T_SHIRT_12				,		// 29_12
	TORSO_P1_CARDIGAN_0				,		// 30_0	
	TORSO_P1_CARDIGAN_1				,		// 30_1	
	TORSO_P1_CARDIGAN_2				,		// 30_2	
	TORSO_P1_CARDIGAN_3				,		// 30_3	
	TORSO_P1_CARDIGAN_4				,		// 30_4	
	TORSO_P1_CARDIGAN_5				,		// 30_5	
	TORSO_P1_CARDIGAN_6				,		// 30_6	
	TORSO_P1_CARDIGAN_7				,		// 30_7	
	TORSO_P1_CARDIGAN_8				,		// 30_8	
	TORSO_P1_CARDIGAN_9				,		// 30_9	
	TORSO_P1_CARDIGAN_10			,		// 30_10	
	TORSO_P1_CARDIGAN_11			,		// 30_11	
	TORSO_P1_CARDIGAN_12			,		// 30_12	
	TORSO_P1_BASKETBALL_0			,		// 31_0	
	TORSO_P1_BASKETBALL_1			,		// 31_1	
	TORSO_P1_BASKETBALL_2			,		// 31_2	
	TORSO_P1_BASKETBALL_3			,		// 31_3	
	TORSO_P1_BASKETBALL_4			,		// 31_4	
	TORSO_P1_BASKETBALL_5			,		// 31_5	
	TORSO_P1_BASKETBALL_6			,		// 31_6	
	TORSO_P1_BASKETBALL_7			,		// 31_7
	TORSO_P1_BASKETBALL_8			,		// 31_8
	TORSO_P1_DLC					,// leave this at the bottom
	
	LEGS_P1_BEIGE_SHORTS			= 0,	// 0_0
	LEGS_P1_CARGO_SHORTS_1			,		// 0_1
	LEGS_P1_CARGO_SHORTS_2			,		// 0_2
	LEGS_P1_CARGO_SHORTS_3			,		// 0_3
	LEGS_P1_BLACK_BOILER			,		// 1_0
	LEGS_P1_RED_BOILER				,		// 1_1
	LEGS_P1_BLUE_BOILER				,		// 1_2
	LEGS_P1_NAVY_BOILER				,		// 1_3
	LEGS_P1_GRAY_BOILER				,		// 1_4
	LEGS_P1_GREEN_BOILER			,		// 1_5
	LEGS_P1_WHITE_TUXEDO			,		// 2_0
	LEGS_P1_SCUBA					,		// 3_0
	LEGS_P1_BALLISTICS				,		// 4_0
	LEGS_P1_TRIATHLON				,		// 5_0
	LEGS_P1_GOLF_0					,		// 6_0
	LEGS_P1_GOLF_1					,		// 6_1
	LEGS_P1_GOLF_2					,		// 6_2
	LEGS_P1_GOLF_3					,		// 6_3
	LEGS_P1_GOLF_4					,		// 6_4
	LEGS_P1_GOLF_5					,		// 6_5
	LEGS_P1_GOLF_6					,		// 6_6
	LEGS_P1_GOLF_7					,		// 6_7
	LEGS_P1_MOTO_X					,		// 7_0
	LEGS_P1_MOTO_X_1				,		// 7_1
	LEGS_P1_BLACK_JEANS				,		// 8_0
	LEGS_P1_BLUE_JEANS				,		// 8_1
	LEGS_P1_BLUE_JEANS_2			,		// 8_2
	LEGS_P1_BLUE_JEANS_3			,		// 8_3
	LEGS_P1_BLUE_JEANS_4			,		// 8_4
	LEGS_P1_BLUE_JEANS_5			,		// 8_5
	LEGS_P1_BLUE_JEANS_6			,		// 8_6
	LEGS_P1_MIME					,		// 9_0
	LEGS_P1_FIREMAN					,		// 10_0
	LEGS_P1_CHINOS					,		// 11_0
	LEGS_P1_CHINOS_1				,		// 11_1
	LEGS_P1_CHINOS_2				,		// 11_2
	LEGS_P1_CHINOS_3				,		// 11_3
	LEGS_P1_CHINOS_4				,		// 11_4
	LEGS_P1_CHINOS_5				,		// 11_5
	LEGS_P1_DUMMY					,		// 12_0
	LEGS_P1_SWEATPANTS				,		// 13_0
	LEGS_P1_SWEATPANTS_1			,		// 13_1
	LEGS_P1_SWEATPANTS_2			,		// 13_2
	LEGS_P1_SWEATPANTS_3			,		// 13_3
	LEGS_P1_SWEATPANTS_4			,		// 13_4
	LEGS_P1_SWEATPANTS_5			,		// 13_5
	LEGS_P1_SWEATPANTS_6			,		// 13_6
	LEGS_P1_SWEATPANTS_7			,		// 13_7
	LEGS_P1_SWEATPANTS_8			,		// 13_8
	LEGS_P1_SWEATPANTS_9			,		// 13_9
	LEGS_P1_SWEATPANTS_10			,		// 13_10
	LEGS_P1_SWEATPANTS_11			,		// 13_11
	LEGS_P1_TENNIS					,		// 14_0
	LEGS_P1_SUIT					,		// 15_0
	LEGS_P1_SUIT_1					,		// 15_1	
	LEGS_P1_SUIT_2					,		// 15_2	
	LEGS_P1_SUIT_3					,		// 15_3	
	LEGS_P1_SUIT_4					,		// 15_4	
	LEGS_P1_SUIT_5					,		// 15_5	
	LEGS_P1_SUIT_6					,		// 15_6	
	LEGS_P1_SUIT_7					,		// 15_7
	LEGS_P1_SUIT_8					,		// 15_8	
	LEGS_P1_SUIT_9					,		// 15_9	
	LEGS_P1_SUIT_10					,		// 15_10	
	LEGS_P1_SUIT_11					,		// 15_11	
	LEGS_P1_SUIT_12					,		// 15_12	
	LEGS_P1_SUIT_13					,		// 15_13	
	LEGS_P1_SUIT_14					,		// 15_14	
	LEGS_P1_SUIT_15					,		// 15_15	
	LEGS_P1_STEALTH					,		// 16_0
	LEGS_P1_STEALTH_1				,		// 16_1
	LEGS_P1_SKYDIVING				,		// 17_0
	LEGS_P1_BOXERS					,		// 18_0
	LEGS_P1_BOXERS_1				,		// 18_1
	LEGS_P1_BOXERS_2				,		// 18_2
	LEGS_P1_BOXERS_3				,		// 18_3
	LEGS_P1_BOXERS_4				,		// 18_4
	LEGS_P1_BOXERS_5				,		// 18_5
	LEGS_P1_TUXEDO_NO_SHOES			,		// 19_0
	LEGS_P1_JEANS_0					,		// 20_0
	LEGS_P1_JEANS_1					,		// 20_1
	LEGS_P1_JEANS_2					,		// 20_2
	LEGS_P1_JEANS_3					,		// 20_3
	LEGS_P1_JEANS_4					,		// 20_4
	LEGS_P1_JEANS_5					,		// 20_5
	LEGS_P1_JEANS_6					,		// 20_6
	LEGS_P1_JEANS_7					,		// 20_7
	LEGS_P1_JEANS_8					,		// 20_8
	LEGS_P1_JEANS_B_0				,		// 21_0
	LEGS_P1_JEANS_B_1				,		// 21_1
	LEGS_P1_JEANS_B_2				,		// 21_2
	LEGS_P1_JEANS_B_3				,		// 21_3
	LEGS_P1_JEANS_B_4				,		// 21_4
	LEGS_P1_JEANS_B_5				,		// 21_5
	LEGS_P1_JEANS_B_6				,		// 21_6
	LEGS_P1_JEANS_B_7				,		// 21_7
	LEGS_P1_JEANS_B_8				,		// 21_8
	LEGS_P1_BASKETBALL_SHORTS_0		,		// 22_0
	LEGS_P1_BASKETBALL_SHORTS_1		,		// 22_1
	LEGS_P1_BASKETBALL_SHORTS_2		,		// 22_2
	LEGS_P1_BASKETBALL_SHORTS_3		,		// 22_3
	LEGS_P1_BASKETBALL_SHORTS_4		,		// 22_4
	LEGS_P1_BASKETBALL_SHORTS_5		,		// 22_5
	LEGS_P1_BASKETBALL_SHORTS_6		,		// 22_6
	LEGS_P1_BASKETBALL_SHORTS_7		,		// 22_7
	LEGS_P1_BASKETBALL_SHORTS_8		,		// 22_8
	LEGS_P1_BASKETBALL_SHORTS_9		,		// 22_9
	LEGS_P1_BASKETBALL_SHORTS_10	,		// 22_10
	LEGS_P1_BASKETBALL_SHORTS_11	,		// 22_11
	LEGS_P1_BASKETBALL_SHORTS_12	,		// 22_12
	LEGS_P1_BASKETBALL_SHORTS_13	,		// 22_13
	LEGS_P1_SHORTS_0				,
	LEGS_P1_SHORTS_1				,
	LEGS_P1_SHORTS_2				,
	LEGS_P1_SHORTS_3				,
	LEGS_P1_SHORTS_4				,
	LEGS_P1_SHORTS_5				,
	LEGS_P1_DLC						,// leave this at the bottom
	// If you add new legs which have feet attached update DOES_LEGS_COMPONENT_HAVE_FEET_ATTACHED
	
	FEET_P1_TRAINERS				= 0,
	FEET_P1_TRAINERS_1				,
	FEET_P1_TRAINERS_2				,
	FEET_P1_TRAINERS_3				,
	FEET_P1_TRAINERS_4				,
	FEET_P1_TRAINERS_5				,
	FEET_P1_TRAINERS_6				,
	FEET_P1_TRAINERS_7				,
	FEET_P1_TRAINERS_8				,
	FEET_P1_TRAINERS_9				,
	FEET_P1_TRAINERS_10				,
	FEET_P1_TRAINERS_11				,
	FEET_P1_BLACK_BOILER			,
	FEET_P1_SCUBA_WATER				,
	FEET_P1_TRIATHLON				,
	FEET_P1_MOTO_X					,
	FEET_P1_MOTO_X_1				,
	FEET_P1_DUMMY	 				,
	FEET_P1_NUBUCK_BOOTS			,
	FEET_P1_NUBUCK_BOOTS_1			,
	FEET_P1_NUBUCK_BOOTS_2			,
	FEET_P1_NUBUCK_BOOTS_3			,
	FEET_P1_NUBUCK_BOOTS_4			,
	FEET_P1_NUBUCK_BOOTS_5			,
	FEET_P1_MIME					,
	FEET_P1_SUIT					,
	FEET_P1_SUIT_1					,
	FEET_P1_SUIT_2					,
	FEET_P1_SUIT_3					,
	FEET_P1_SUIT_4					,
	FEET_P1_SUIT_5					,
	FEET_P1_SUIT_6					,
	FEET_P1_SUIT_7					,
	FEET_P1_SUIT_8					,
	FEET_P1_SCUBA_LAND				,
	FEET_P1_SMART_SHOES				,
	FEET_P1_SNEAKERS_A_0			,
	FEET_P1_SNEAKERS_A_1			,
	FEET_P1_SNEAKERS_A_2			,
	FEET_P1_SNEAKERS_A_3			,
	FEET_P1_SNEAKERS_A_4			,
	FEET_P1_SNEAKERS_A_5			,
	FEET_P1_SNEAKERS_A_6			,
	FEET_P1_SNEAKERS_A_7			,
	FEET_P1_SNEAKERS_A_8			,
	FEET_P1_SNEAKERS_A_9			,
	FEET_P1_SNEAKERS_A_10			,
	FEET_P1_SNEAKERS_A_11			,
	FEET_P1_SNEAKERS_A_12			,
	FEET_P1_SNEAKERS_A_13			,
	FEET_P1_SNEAKERS_A_14			,
	FEET_P1_SNEAKERS_A_15			,
	FEET_P1_SNEAKERS_B_0			,
	FEET_P1_SNEAKERS_B_1			,
	FEET_P1_SNEAKERS_B_2			,
	FEET_P1_SNEAKERS_B_3			,
	FEET_P1_SNEAKERS_B_4			,
	FEET_P1_SNEAKERS_B_5			,
	FEET_P1_SNEAKERS_B_6			,
	FEET_P1_SNEAKERS_B_7			,
	FEET_P1_SNEAKERS_B_8			,
	FEET_P1_SNEAKERS_B_9			,
	FEET_P1_SNEAKERS_B_10			,
	FEET_P1_SNEAKERS_B_11			,
	FEET_P1_BROGUES_0				,
	FEET_P1_SKATE_SHOES_0			,
	FEET_P1_SKATE_SHOES_1			,
	FEET_P1_SKATE_SHOES_2			,
	FEET_P1_SKATE_SHOES_3			,
	FEET_P1_SKATE_SHOES_4			,
	FEET_P1_SKATE_SHOES_5			,
	FEET_P1_SKATE_SHOES_6			,
	FEET_P1_SKATE_SHOES_7			,
	FEET_P1_BARE_FEET				,
	FEET_P1_LOAFERS_0				,
	FEET_P1_LOAFERS_1				,
	FEET_P1_LOAFERS_2				,
	FEET_P1_LOAFERS_3				,
	FEET_P1_LOAFERS_4				,
	FEET_P1_LOAFERS_5				,
	FEET_P1_LOAFERS_6				,
	FEET_P1_LOAFERS_7				,
	FEET_P1_LOAFERS_8				,
	FEET_P1_LOAFERS_9				,
	FEET_P1_LOAFERS_10				,
	FEET_P1_LOAFERS_11				,
	FEET_P1_OXFORDS_0				,
	FEET_P1_OXFORDS_1				,
	FEET_P1_OXFORDS_2				,
	FEET_P1_OXFORDS_3				,
	FEET_P1_OXFORDS_4				,
	FEET_P1_OXFORDS_5				,
	FEET_P1_OXFORDS_6				,
	FEET_P1_OXFORDS_7				,
	FEET_P1_OXFORDS_8				,
	FEET_P1_OXFORDS_9				,
	FEET_P1_OXFORDS_10				,
	FEET_P1_OXFORDS_11				,
	FEET_P1_OXFORDS_12				,
	FEET_P1_OXFORDS_13				,
	FEET_P1_OXFORDS_14				,
	FEET_P1_OXFORDS_15				,
	FEET_P1_SQ_LOAFERS_0			,
	FEET_P1_SQ_LOAFERS_1			,
	FEET_P1_SQ_LOAFERS_2			,
	FEET_P1_SQ_LOAFERS_3			,
	FEET_P1_SQ_LOAFERS_4			,
	FEET_P1_SQ_LOAFERS_5			,
	FEET_P1_SQ_LOAFERS_6			,
	FEET_P1_SQ_LOAFERS_7			,
	FEET_P1_SQ_LOAFERS_8			,
	FEET_P1_SQ_LOAFERS_9			,
	FEET_P1_SQ_LOAFERS_10			,
	FEET_P1_SQ_LOAFERS_11			,
	FEET_P1_SQ_LOAFERS_12			,
	FEET_P1_SQ_LOAFERS_13			,
	FEET_P1_SQ_LOAFERS_14			,
	FEET_P1_SQ_LOAFERS_15			,
	FEET_P1_WINGTIPS_0				,
	FEET_P1_WINGTIPS_1				,
	FEET_P1_WINGTIPS_2				,
	FEET_P1_WINGTIPS_3				,
	FEET_P1_WINGTIPS_4				,
	FEET_P1_WINGTIPS_5				,
	FEET_P1_WINGTIPS_6				,
	FEET_P1_WINGTIPS_7				,
	FEET_P1_WINGTIPS_8				,
	FEET_P1_WINGTIPS_9				,
	FEET_P1_WINGTIPS_10				,
	FEET_P1_WINGTIPS_11				,
	FEET_P1_WINGTIPS_12				,
	FEET_P1_WINGTIPS_13				,
	FEET_P1_WINGTIPS_14				,
	FEET_P1_WINGTIPS_15				,
	FEET_P1_DLC						,// leave this at the bottom
	// if you add new dummy feet, update IS_PED_COMPONENT_ITEM_RESTRICTED_FOR_ITEM_SP
	
	HAND_P1_NONE 					= 0,
	HAND_P1_FIREMAN					,
	HAND_P1_GOLF	 				,
	HAND_P1_GOLF_1	 				,
	HAND_P1_WHITE_GLOVES			,
	HAND_P1_BLACK_GLOVES			,
	HAND_P1_MOTO_X					,
	HAND_P1_DLC						,// leave this at the bottom
	
	
	SPECIAL_P1_WATCH_AND_BRACELET	= 0,
	SPECIAL_P1_PARACHUTE			,
	SPECIAL_P1_SCUBA	 			,
	SPECIAL_P1_BALLISTICS 			,
	SPECIAL_P1_BALACLAVA 			,
	SPECIAL_P1_EXTERMINATOR			,
	SPECIAL_P1_CLOWN	 			,
	SPECIAL_P1_SCARF	 			,
	SPECIAL_P1_FIREMAN				,
	SPECIAL_P1_MASK					,
	SPECIAL_P1_PARACHUTE_1			,
	SPECIAL_P1_TIE					,
	SPECIAL_P1_TIE_1				,
	SPECIAL_P1_TIE_2				,
	SPECIAL_P1_TIE_3				,
	SPECIAL_P1_TIE_4				,
	SPECIAL_P1_TIE_5				,
	SPECIAL_P1_TIE_6				,
	SPECIAL_P1_TIE_7				,
	SPECIAL_P1_TIE_8				,
	SPECIAL_P1_TIE_9				,
	SPECIAL_P1_TIE_10				,
	SPECIAL_P1_TIE_11				,
	SPECIAL_P1_TIE_12				,
	SPECIAL_P1_BOWTIE				,
	SPECIAL_P1_POUCHES				,
	SPECIAL_P1_DUMMY				,
	SPECIAL_P1_TIE_SHORT			,
	SPECIAL_P1_TIE_SHORT_1			,
	SPECIAL_P1_TIE_SHORT_2			,
	SPECIAL_P1_TIE_SHORT_3			,
	SPECIAL_P1_TIE_SHORT_4			,
	SPECIAL_P1_TIE_SHORT_5			,
	SPECIAL_P1_TIE_SHORT_6			,
	SPECIAL_P1_TIE_SHORT_7			,
	SPECIAL_P1_TIE_SHORT_8			,
	SPECIAL_P1_TIE_SHORT_9			,
	SPECIAL_P1_TIE_SHORT_10			,
	SPECIAL_P1_TIE_SHORT_11			,
	SPECIAL_P1_TIE_SHORT_12			,
	SPECIAL_P1_TIE_SHORT_13			,
	SPECIAL_P1_TIE_SHORT_14			,
	SPECIAL_P1_TIE_SHORT_15			,
	SPECIAL_P1_TIE_MEDIUM			,
	SPECIAL_P1_TIE_MEDIUM_1			,
	SPECIAL_P1_TIE_MEDIUM_2			,
	SPECIAL_P1_TIE_MEDIUM_3			,
	SPECIAL_P1_TIE_MEDIUM_4			,
	SPECIAL_P1_TIE_MEDIUM_5			,
	SPECIAL_P1_TIE_MEDIUM_6			,
	SPECIAL_P1_TIE_MEDIUM_7			,
	SPECIAL_P1_TIE_MEDIUM_8			,
	SPECIAL_P1_TIE_MEDIUM_9			,
	SPECIAL_P1_TIE_MEDIUM_10		,
	SPECIAL_P1_TIE_MEDIUM_11		,
	SPECIAL_P1_TIE_MEDIUM_12		,
	SPECIAL_P1_TIE_MEDIUM_13		,
	SPECIAL_P1_TIE_MEDIUM_14		,
	SPECIAL_P1_TIE_MEDIUM_15		,
	SPECIAL_P1_TIE_LONG				,
	SPECIAL_P1_TIE_LONG_1			,
	SPECIAL_P1_TIE_LONG_2			,
	SPECIAL_P1_TIE_LONG_3			,
	SPECIAL_P1_TIE_LONG_4			,
	SPECIAL_P1_TIE_LONG_5			,
	SPECIAL_P1_TIE_LONG_6			,
	SPECIAL_P1_TIE_LONG_7			,
	SPECIAL_P1_TIE_LONG_8			,
	SPECIAL_P1_TIE_LONG_9			,
	SPECIAL_P1_TIE_LONG_10			,
	SPECIAL_P1_TIE_LONG_11			,
	SPECIAL_P1_TIE_LONG_12			,
	SPECIAL_P1_TIE_LONG_13			,
	SPECIAL_P1_TIE_LONG_14			,
	SPECIAL_P1_TIE_LONG_15			,
	SPECIAL_P1_FIREMAN_1			,
	SPECIAL_P1_SCUBA_1				,
	SPECIAL_P1_DLC					,// leave this at the bottom
	
	SPECIAL2_P1_NONE				= 0,
	SPECIAL2_P1_BAG					,
	SPECIAL2_P1_HARNESS				,
	SPECIAL2_P1_BALLISTICS 			,
	SPECIAL2_P1_SCUBA_HEADGEAR 		,
	SPECIAL2_P1_WRESTLER_MASK_0		,
	SPECIAL2_P1_WRESTLER_MASK_1		,
	SPECIAL2_P1_WRESTLER_MASK_2		,
	SPECIAL2_P1_WRESTLER_MASK_3		,
	SPECIAL2_P1_WRESTLER_MASK_4		,
	SPECIAL2_P1_WRESTLER_MASK_5		,
	SPECIAL2_P1_BAG_A				,
	SPECIAL2_P1_DLC					,// leave this at the bottom
	
	DECL_P1_NONE 					= 0,
	DECL_P1_FIREMAN					,
	DECL_P1_EXTERMINATOR			,
	DECL_P1_AMMUNATION_0			,
	DECL_P1_AMMUNATION_1			,
	DECL_P1_AMMUNATION_2			,
	DECL_P1_AMMUNATION_3			,
	DECL_P1_AMMUNATION_4			,
	DECL_P1_AMMUNATION_5			,
	DECL_P1_SPORTS_0				,
	DECL_P1_SPORTS_1				,
	DECL_P1_SPORTS_2				,
	DECL_P1_SPORTS_3				,
	DECL_P1_SPORTS_4				,
	DECL_P1_SPORTS_5				,
	DECL_P1_SPORTS_6				,
	DECL_P1_VARSITY_0				,
	DECL_P1_VARSITY_1				,
	DECL_P1_VARSITY_2				,
	DECL_P1_VARSITY_3				,
	DECL_P1_VARSITY_4				,
	DECL_P1_VARSITY_5				,
	DECL_P1_VARSITY_6				,
	DECL_P1_VARSITY_7				,
	DECL_P1_VARSITY_8				,
	DECL_P1_VARSITY_9				,
	DECL_P1_VARSITY_10				,
	DECL_P1_VARSITY_11				,
	DECL_P1_VARSITY_12				,
	DECL_P1_VARSITY_13				,
	DECL_P1_VARSITY_14				,
	DECL_P1_VARSITY_15				,
	DECL_P1_CARDIGAN_0				,
	DECL_P1_CARDIGAN_1				,
	DECL_P1_CARDIGAN_2				,
	DECL_P1_CARDIGAN_3				,
	DECL_P1_CARDIGAN_4				,
	DECL_P1_CARDIGAN_5				,
	DECL_P1_CARDIGAN_6				,
	DECL_P1_BASKETBALL_0			,
	DECL_P1_BASKETBALL_1			,
	DECL_P1_BASKETBALL_2			,
	DECL_P1_BASKETBALL_3			,
	DECL_P1_BASKETBALL_4			,
	DECL_P1_BASKETBALL_5			,
	DECL_P1_BASKETBALL_6			,
	DECL_P1_BASKETBALL_7			,
	DECL_P1_MASK_0					,
	DECL_P1_MASK_1					,
	DECL_P1_MASK_2					,
	DECL_P1_MASK_3					,
	DECL_P1_UPTOWN_A				,
	DECL_P1_UPTOWN_B				,
	DECL_P1_DLC						,// leave this at the bottom
	
	BERD_P1_NONE 					= 0,
	BERD_P1_1_0						,
	BERD_P1_2_0						,
	BERD_P1_3_0						,
	BERD_P1_4_0						,
	BERD_P1_DLC						,// leave this at the bottom
	
	TEETH_P1_NONE 					= 0,
	TEETH_P1_DLC					,// leave this at the bottom
	
	JBIB_P1_NONE 					= 0,
	JBIB_P1_COLLAR_A 				,
	JBIB_P1_COLLAR_A_1 				,
	JBIB_P1_COLLAR_A_2 				,
	JBIB_P1_COLLAR_A_3 				,
	JBIB_P1_COLLAR_B				,
	JBIB_P1_COLLAR_B_1				,
	JBIB_P1_COLLAR_B_2				,
	JBIB_P1_COLLAR_B_3				,
	JBIB_P1_WAISTCOAT_A 			,
	JBIB_P1_WAISTCOAT_A_1 			,// 10
	JBIB_P1_WAISTCOAT_A_2 			,
	JBIB_P1_WAISTCOAT_A_3 			,
	JBIB_P1_WAISTCOAT_A_4 			,
	JBIB_P1_WAISTCOAT_A_5 			,
	JBIB_P1_WAISTCOAT_A_6 			,
	JBIB_P1_WAISTCOAT_A_7 			,
	JBIB_P1_WAISTCOAT_A_8 			,
	JBIB_P1_WAISTCOAT_A_9 			,
	JBIB_P1_WAISTCOAT_A_10 			,
	JBIB_P1_WAISTCOAT_A_11 			,// 20
	JBIB_P1_WAISTCOAT_A_12 			,
	JBIB_P1_WAISTCOAT_A_13 			,
	JBIB_P1_WAISTCOAT_A_14 			,
	JBIB_P1_WAISTCOAT_A_15 			,
	JBIB_P1_WAISTCOAT_B				,
	JBIB_P1_WAISTCOAT_B_1			,
	JBIB_P1_WAISTCOAT_B_2			,
	JBIB_P1_WAISTCOAT_B_3			,
	JBIB_P1_WAISTCOAT_B_4			,
	JBIB_P1_WAISTCOAT_B_5			,// 30
	JBIB_P1_WAISTCOAT_B_6			,
	JBIB_P1_WAISTCOAT_B_7			,
	JBIB_P1_WAISTCOAT_B_8			,
	JBIB_P1_WAISTCOAT_B_9			,
	JBIB_P1_WAISTCOAT_B_10			,
	JBIB_P1_WAISTCOAT_B_11			,
	JBIB_P1_WAISTCOAT_B_12			,
	JBIB_P1_WAISTCOAT_B_13			,
	JBIB_P1_WAISTCOAT_B_14			,
	JBIB_P1_WAISTCOAT_B_15			,// 40
	JBIB_P1_SHIRT_A 				,
	JBIB_P1_SHIRT_B					,
	JBIB_P1_SHIRT_B_1				,
	JBIB_P1_HOODIE_1				,
	JBIB_P1_POLO_SHIRT_1			,
	JBIB_P1_COLLAR_C				,
	JBIB_P1_TSHIRT_0				,
	JBIB_P1_TSHIRT_1				,
	JBIB_P1_TSHIRT_2				,
	JBIB_P1_TSHIRT_3				,
	JBIB_P1_TSHIRT_4				,
	JBIB_P1_TSHIRT_5				,
	JBIB_P1_TSHIRT_6				,
	JBIB_P1_TSHIRT_7				,
	JBIB_P1_TSHIRT_8				,
	JBIB_P1_TSHIRT_9				,
	JBIB_P1_TSHIRT_10				,
	JBIB_P1_TSHIRT_11				,
	JBIB_P1_TSHIRT_12				,
	JBIB_P1_TSHIRT_13				,
	JBIB_P1_TSHIRT_14				,
	JBIB_P1_TSHIRT_15				,
	JBIB_P1_DLC						,// leave this at the bottom
	
	OUTFIT_P1_DEFAULT 				= OUTFIT_DEFAULT,
	OUTFIT_P1_WHITE_TUXEDO 			,
	OUTFIT_P1_GOLF		 			,
	OUTFIT_P1_SCUBA_WATER 			,
	OUTFIT_P1_SCUBA_LAND 			,
	OUTFIT_P1_STEALTH	 			,
	OUTFIT_P1_TRIATHLON	 			,
	OUTFIT_P1_FIREMAN				,
	OUTFIT_P1_EXTERMINATOR			,
	OUTFIT_P1_BLACK_BOILER			,
	OUTFIT_P1_SKYDIVING				,
	OUTFIT_P1_TUXEDO				,
	OUTFIT_P1_BLUE_BOILER_SUIT		,
	OUTFIT_P1_PREP_BOILER_SUIT_1	,
	OUTFIT_P1_PREP_BOILER_SUIT_2	,
	OUTFIT_P1_PREP_BOILER_SUIT_3	,
	OUTFIT_P1_HOODIE_AND_SWEATS		,
	OUTFIT_P1_GREEN_SHIRT_JEANS		,
	OUTFIT_P1_SUIT_1				,
	OUTFIT_P1_HOODIE_AND_JEANS_1	,
	OUTFIT_P1_TRACKSUIT_JEANS		,
	OUTFIT_P1_WHITE_SHIRT_JEANS		,
	OUTFIT_P1_3PC_SUIT_0			,
	OUTFIT_P1_3PC_SUIT_1			,
	OUTFIT_P1_3PC_SUIT_2			,
	OUTFIT_P1_3PC_SUIT_3			,
	OUTFIT_P1_3PC_SUIT_4			,
	OUTFIT_P1_3PC_SUIT_5			,
	OUTFIT_P1_3PC_SUIT_6			,
	OUTFIT_P1_3PC_SUIT_7			,
	OUTFIT_P1_3PC_SUIT_8			,
	OUTFIT_P1_3PC_SUIT_9			,
	OUTFIT_P1_3PC_SUIT_10			,
	OUTFIT_P1_3PC_SUIT_11			,
	OUTFIT_P1_3PC_SUIT_12			,
	OUTFIT_P1_3PC_SUIT_13			,
	OUTFIT_P1_3PC_SUIT_14			,
	OUTFIT_P1_3PC_SUIT_15			,
	OUTFIT_P1_UNDERWEAR				,
	OUTFIT_P1_HOODIE_AND_JEANS_2	,
	OUTFIT_P1_HOODIE_AND_JEANS_3	, 
	OUTFIT_P1_STEALTH_NO_MASK		, 
	OUTFIT_P1_MOTO_X				, 
	OUTFIT_P1_UPTOWN_1				,
	OUTFIT_P1_UPTOWN_2				,
	OUTFIT_P1_UPTOWN_3				,
	OUTFIT_P1_UPTOWN_4				, // 47
	OUTFIT_P1_DLC					,// leave this at the bottom
	// If adding outfits, you may need to increase PED_COMPONENTS_OUTFITS
	
	PROPGROUP_P1_HOCKEY_MASK		= 0,
	PROPGROUP_P1_SCUBA_WATER		,
	PROPGROUP_P1_SCUBA_LAND			,
	PROPGROUP_P1_FIREMAN			,
	PROPGROUP_P1_GREEN_CAP			,
	PROPGROUP_P1_BUGSTARS_CAP		,
	PROPGROUP_P1_UPTOWN_1			,
	PROPGROUP_P1_BLACK_SHADES		,
	PROPGROUP_P1_BLACK_CAP_SHADES	,
	PROPGROUP_P1_DLC				, // leave this at the bottom
	
	// props need to be ordered by anchor and then drawable / variation
	// if the 1st prop of a type changes, update GET_FIRST_PROP_OF_TYPE
	// ANCHOR_HEAD, 
	PROPS_P1_HOCKEY_MASK 			= 10, // Start props at 10.
	PROPS_P1_EAR_DEFENDERS			,
	PROPS_P1_BIKE_HELMET			,
	PROPS_P1_MIME_HAT				,
	PROPS_P1_HELI_HEADSET			,
	PROPS_P1_FIREMAN_HAT			,
	PROPS_P1_CRASH_HELMET			,
	PROPS_P1_CRASH_HELMET_1			,
	PROPS_P1_CRASH_HELMET_2			,
	PROPS_P1_CRASH_HELMET_3			,
	PROPS_P1_CRASH_HELMET_4			,
	PROPS_P1_CRASH_HELMET_5			,
	PROPS_P1_CRASH_HELMET_6			,
	PROPS_P1_CRASH_HELMET_7			,
	PROPS_P1_CRASH_HELMET_8			,
	PROPS_P1_HARD_HAT				,
	PROPS_P1_MASK_MONSTER_GREEN		,
	PROPS_P1_MASK_MONSTER_RED		,
	PROPS_P1_MASK_PIG				,
	PROPS_P1_MASK_PIG_DARK			,
	PROPS_P1_MASK_SKULL_GREY		,
	PROPS_P1_MASK_SKULL_YELLOW		,
	PROPS_P1_MASK_MONKEY			,
	PROPS_P1_MASK_HOCKEY_WHITE		,
	PROPS_P1_MASK_HOCKEY_RED		,
	PROPS_P1_MASK_APE				,
	PROPS_P1_MASK_APE_DARK			,
	PROPS_P1_MASK_TRIBAL_1			,
	PROPS_P1_MASK_TRIBAL_2			,
	PROPS_P1_MASK_TRIBAL_3			,
	PROPS_P1_BUGSTAR_CAP			,
	PROPS_P1_GREEN_CAP				,
	PROPS_P1_CAP_BACK_1				,
	PROPS_P1_CAP_BACK_2				,
	PROPS_P1_CAP_BACK_3				,
	PROPS_P1_CAP_BACK_4				,
	PROPS_P1_CAP_BACK_5				,
	PROPS_P1_CAP_BACK_6				,
	PROPS_P1_CAP_BACK_7				,
	PROPS_P1_CAP_BACK_8				,
	PROPS_P1_CAP_BACK_9				,
	PROPS_P1_CAP_BACK_10			,
	PROPS_P1_CAP_BACK_11			,
	PROPS_P1_CAP_BACK_12			,
	PROPS_P1_CAP_BACK_13			,
	PROPS_P1_CAP_BACK_14			,
	PROPS_P1_CAP_BACK_15			,
	PROPS_P1_MOTO_X_HELMET_0		,	
	PROPS_P1_MOTO_X_HELMET_1		,	
	PROPS_P1_MOTO_X_HELMET_2		,	
	PROPS_P1_MOTO_X_HELMET_3		,	
	PROPS_P1_MOTO_X_HELMET_4		,	
	PROPS_P1_MOTO_X_HELMET_5		,		
	PROPS_P1_HEADPHONES				,
	PROPS_P1_CAP_FRONT_0			,
	PROPS_P1_CAP_FRONT_1			,
	PROPS_P1_CAP_FRONT_2			,
	PROPS_P1_CAP_FRONT_3			,
	PROPS_P1_CAP_FRONT_4			,
	PROPS_P1_CAP_FRONT_5			,
	PROPS_P1_CAP_FRONT_6			,
	PROPS_P1_CAP_FRONT_7			,
	PROPS_P1_CAP_FRONT_8			,
	PROPS_P1_CAP_FRONT_9			,
	PROPS_P1_CAP_FRONT_10			,
	PROPS_P1_CAP_FRONT_11			,
	PROPS_P1_CAP_FRONT_12			,
	PROPS_P1_CAP_FRONT_13			,
	PROPS_P1_CAP_FRONT_14			,
	PROPS_P1_CAP_FRONT_15			,	
	PROPS_P1_FLIGHT_CAP				,
	PROPS_P1_FIGHTER_JET_HELMET		,
	
	//ANCHOR_EYES, 
	PROPS_P1_SCUBA_MASK				,
	PROPS_P1_SUNGLASSES_A_0			,
	PROPS_P1_SUNGLASSES_A_1			,
	PROPS_P1_SUNGLASSES_A_2			,
	PROPS_P1_SUNGLASSES_A_3			,
	PROPS_P1_SUNGLASSES_A_4			,
	PROPS_P1_SUNGLASSES_A_5			,
	PROPS_P1_SUNGLASSES_A_6			,
	PROPS_P1_SUNGLASSES_A_7			,
	PROPS_P1_SUNGLASSES_A_8			,
	PROPS_P1_SUNGLASSES_A_9			,
	PROPS_P1_GOGGLES				,
	PROPS_P1_SUNGLASSES_B_0			,//race
	PROPS_P1_SUNGLASSES_C_0			,//
	PROPS_P1_SUNGLASSES_C_1			,
	PROPS_P1_SUNGLASSES_C_2			,
	PROPS_P1_SUNGLASSES_C_3			,
	PROPS_P1_SUNGLASSES_C_4			,
	PROPS_P1_SUNGLASSES_C_5			,
	PROPS_P1_SUNGLASSES_C_6			,
	PROPS_P1_SUNGLASSES_D_0			,//aviator1
	PROPS_P1_SUNGLASSES_D_1			,
	PROPS_P1_SUNGLASSES_D_2			,
	PROPS_P1_SUNGLASSES_D_3			,
	PROPS_P1_SUNGLASSES_D_4			,
	PROPS_P1_SUNGLASSES_D_5			,
	PROPS_P1_SUNGLASSES_D_6			,
	PROPS_P1_SUNGLASSES_D_7			,
	PROPS_P1_SUNGLASSES_D_8			,
	PROPS_P1_SUNGLASSES_D_9			,
	PROPS_P1_SUNGLASSES_E_0			,//aviator2
	PROPS_P1_SUNGLASSES_E_1			,
	PROPS_P1_SUNGLASSES_E_2			,
	PROPS_P1_SUNGLASSES_E_3			,
	PROPS_P1_SUNGLASSES_E_4			,
	PROPS_P1_SUNGLASSES_E_5			,
	PROPS_P1_SUNGLASSES_E_6			,
	PROPS_P1_SUNGLASSES_E_7			,
	PROPS_P1_SUNGLASSES_E_8			,
	PROPS_P1_SUNGLASSES_E_9			,
	PROPS_P1_SUNGLASSES_F_0			,//Suburban
	PROPS_P1_SUNGLASSES_F_1			,
	PROPS_P1_SUNGLASSES_F_2			,
	PROPS_P1_SUNGLASSES_F_3			,
	PROPS_P1_SUNGLASSES_F_4			,
	PROPS_P1_SUNGLASSES_F_5			,
	PROPS_P1_SUNGLASSES_F_6			,
	PROPS_P1_SUNGLASSES_F_7			,
	PROPS_P1_SUNGLASSES_F_8			,
	PROPS_P1_SUNGLASSES_F_9			,
	PROPS_P1_SUNGLASSES_G_0			,
	PROPS_P1_SUNGLASSES_G_1			,
	PROPS_P1_SUNGLASSES_G_2			,
	PROPS_P1_SUNGLASSES_G_3			,
	PROPS_P1_SUNGLASSES_G_4			,
	PROPS_P1_SUNGLASSES_G_5			,
	PROPS_P1_SUNGLASSES_G_6			,
	PROPS_P1_SUNGLASSES_G_7			,
	PROPS_P1_SUNGLASSES_H_0			,
	PROPS_P1_SUNGLASSES_H_1			,
	PROPS_P1_SUNGLASSES_H_2			,
	PROPS_P1_SUNGLASSES_H_3			,
	PROPS_P1_SUNGLASSES_H_4			,
	PROPS_P1_SUNGLASSES_H_5			,
	PROPS_P1_SUNGLASSES_H_6			,
	PROPS_P1_SUNGLASSES_H_7			,
	PROPS_P1_SUNGLASSES_H_8			,
	PROPS_P1_SUNGLASSES_H_9			,
	PROPS_P1_SUNGLASSES_I_0			,
	PROPS_P1_SUNGLASSES_I_1			,
	PROPS_P1_SUNGLASSES_I_2			,
	PROPS_P1_SUNGLASSES_I_3			,
	PROPS_P1_SUNGLASSES_I_4			,
	PROPS_P1_SUNGLASSES_I_5			,
	PROPS_P1_SUNGLASSES_I_6			,
	PROPS_P1_GLASSES_RANGE_0,		//New shooting range glasses
	
	//ANCHOR_EARS
	PROPS_P1_HEADSET				,
	PROPS_P1_EARRING_ROUND_0		,
	PROPS_P1_EARRING_ROUND_1		,
	PROPS_P1_EARRING_ROUND_2		,
	PROPS_P1_EARRING_ROUND_3		,
	PROPS_P1_EARRING_ROUND_4		,
	PROPS_P1_EARRING_ROUND_5		,
	PROPS_P1_EARRING_ROUND_6		,
	PROPS_P1_EARRING_ROUND_7		,
	PROPS_P1_EARRING_SQUARE_0		,
	PROPS_P1_EARRING_SQUARE_1		,
	PROPS_P1_EARRING_SQUARE_2		,
	PROPS_P1_EARRING_SQUARE_3		,
	PROPS_P1_EARRING_SQUARE_4		,
	PROPS_P1_EARRING_SQUARE_5		,
	PROPS_P1_EARRING_SQUARE_6		,
	PROPS_P1_EARRING_SQUARE_7		,
	
	//ANCHOR_MOUTH,
   	//ANCHOR_LEFT_HAND,
    //ANCHOR_RIGHT_HAND,
   	//ANCHOR_LEFT_WRIST,
    //ANCHOR_RIGHT_WRIST,
    //ANCHOR_HIP
	
	PROPS_P1_DLC					, // leave this at the bottom
	
	////////////////////////////////////////////////////////
	/// FM_M - MP_M_FREEMODE_01
	HAIR_FMM_0_0			= 0,
	HAIR_FMM_1_0			,
	HAIR_FMM_1_1			,
	HAIR_FMM_1_2			,
	HAIR_FMM_1_3			,
	HAIR_FMM_1_4			,
	HAIR_FMM_1_5			,
	HAIR_FMM_2_0			,
	HAIR_FMM_2_1			,
	HAIR_FMM_2_2			,
	HAIR_FMM_2_3			,
	HAIR_FMM_2_4			,
	HAIR_FMM_2_5			,
	HAIR_FMM_3_0			,
	HAIR_FMM_3_1			,
	HAIR_FMM_3_2			,
	HAIR_FMM_3_3			,
	HAIR_FMM_3_4			,
	HAIR_FMM_3_5			,
	HAIR_FMM_4_0			,
	HAIR_FMM_4_1			,
	HAIR_FMM_4_2			,
	HAIR_FMM_4_3			,
	HAIR_FMM_4_4			,
	HAIR_FMM_4_5			,
	HAIR_FMM_4_6			,
	HAIR_FMM_5_0			,
	HAIR_FMM_5_1			,
	HAIR_FMM_5_2			,
	HAIR_FMM_5_3			,
	HAIR_FMM_5_4			,
	HAIR_FMM_5_5			,
	HAIR_FMM_6_0			,
	HAIR_FMM_6_1			,
	HAIR_FMM_6_2			,
	HAIR_FMM_6_3			,
	HAIR_FMM_6_4			,
	HAIR_FMM_6_5			,
	HAIR_FMM_7_0			,
	HAIR_FMM_7_1			,
	HAIR_FMM_7_2			,
	HAIR_FMM_7_3			,
	HAIR_FMM_7_4			,
	HAIR_FMM_7_5			,
	HAIR_FMM_7_6			,
	HAIR_FMM_8_0			,
	HAIR_FMM_8_1			,
	HAIR_FMM_8_2			,
	HAIR_FMM_8_3			,
	HAIR_FMM_8_4			,
	HAIR_FMM_9_0			,
	HAIR_FMM_9_1			,
	HAIR_FMM_9_2			,
	HAIR_FMM_9_3			,
	HAIR_FMM_9_4			,
	HAIR_FMM_9_5			,
	HAIR_FMM_9_6			,
	HAIR_FMM_10_0   		,
	HAIR_FMM_10_1   		,
	HAIR_FMM_10_2   		,
	HAIR_FMM_10_3   		,
	HAIR_FMM_10_4   		,
	HAIR_FMM_10_5   		,
	HAIR_FMM_11_0   		,
	HAIR_FMM_11_1   		,
	HAIR_FMM_11_2   		,
	HAIR_FMM_11_3   		,
	HAIR_FMM_11_4   		,
	HAIR_FMM_11_5   		,
	HAIR_FMM_12_0   		,
	HAIR_FMM_12_1   		,
	HAIR_FMM_12_2   		,
	HAIR_FMM_12_3   		,
	HAIR_FMM_12_4   		,
	HAIR_FMM_13_0   		,
	HAIR_FMM_13_1   		,
	HAIR_FMM_13_2   		,
	HAIR_FMM_13_3   		,
	HAIR_FMM_13_4   		,
	HAIR_FMM_13_5   		,
	HAIR_FMM_14_0   		,
	HAIR_FMM_14_1   		,
	HAIR_FMM_14_2			,
	HAIR_FMM_14_3			,
	HAIR_FMM_14_4			,
	HAIR_FMM_15_0			,
	HAIR_FMM_15_1			,
	HAIR_FMM_15_2			,
	HAIR_FMM_15_3			,
	HAIR_FMM_15_4			,
	HAIR_FMM_15_5			,
	HAIR_FMM_DLC			,
	

	TORSO_FMM_0_0			=0,
	TORSO_FMM_1_0			,
	TORSO_FMM_2_0			,
	TORSO_FMM_3_0			,
	TORSO_FMM_4_0			,
	TORSO_FMM_5_0			,
	TORSO_FMM_6_0			,
	TORSO_FMM_7_0			,
	TORSO_FMM_8_0			,
	TORSO_FMM_9_0			,
	TORSO_FMM_10_0			,
	TORSO_FMM_11_0			,
	TORSO_FMM_12_0			,
	TORSO_FMM_13_0			,
	TORSO_FMM_14_0			,
	TORSO_FMM_15_0			,
	TORSO_FMM_DLC			,

	LEGS_FMM_0_0			=0,
	LEGS_FMM_0_1			,
	LEGS_FMM_0_2			,
	LEGS_FMM_0_3			,
	LEGS_FMM_0_4			,
	LEGS_FMM_0_5			,
	LEGS_FMM_0_6			,
	LEGS_FMM_0_7			,
	LEGS_FMM_0_8			,
	LEGS_FMM_0_9			,
	LEGS_FMM_0_10			,
	LEGS_FMM_0_11			,
	LEGS_FMM_0_12			,
	LEGS_FMM_0_13			,
	LEGS_FMM_0_14			,
	LEGS_FMM_0_15			,
	LEGS_FMM_1_0			,
	LEGS_FMM_1_1			,
	LEGS_FMM_1_2			,
	LEGS_FMM_1_3			,
	LEGS_FMM_1_4			,
	LEGS_FMM_1_5			,
	LEGS_FMM_1_6			,
	LEGS_FMM_1_7			,
	LEGS_FMM_1_8			,
	LEGS_FMM_1_9			,
	LEGS_FMM_1_10			,
	LEGS_FMM_1_11			,
	LEGS_FMM_1_12			,
	LEGS_FMM_1_13			,
	LEGS_FMM_1_14			,
	LEGS_FMM_1_15			,
	LEGS_FMM_2_0			, // Moved to DLC
	LEGS_FMM_2_1			, // Moved to DLC
	LEGS_FMM_2_2			, // Moved to DLC
	LEGS_FMM_2_3			, // Moved to DLC
	LEGS_FMM_2_4			, // Moved to DLC
	LEGS_FMM_2_5			, // Moved to DLC
	LEGS_FMM_2_6			, // Moved to DLC
	LEGS_FMM_2_7			, // Moved to DLC
	LEGS_FMM_2_8			, // Moved to DLC
	LEGS_FMM_2_9			, // Moved to DLC
	LEGS_FMM_2_10			, // Moved to DLC
	LEGS_FMM_2_11			,
	LEGS_FMM_2_12			, // Moved to DLC
	LEGS_FMM_2_13			, // Moved to DLC
	LEGS_FMM_2_14			, // Moved to DLC
	LEGS_FMM_2_15			, // Moved to DLC
	LEGS_FMM_3_0			,
	LEGS_FMM_3_1			,
	LEGS_FMM_3_2			,
	LEGS_FMM_3_3			,
	LEGS_FMM_3_4			,
	LEGS_FMM_3_5			,
	LEGS_FMM_3_6			,
	LEGS_FMM_3_7			,
	LEGS_FMM_3_8			,
	LEGS_FMM_3_9			,
	LEGS_FMM_3_10			,
	LEGS_FMM_3_11			,
	LEGS_FMM_3_12			,
	LEGS_FMM_3_13			,
	LEGS_FMM_3_14			,
	LEGS_FMM_3_15			,
	LEGS_FMM_4_0			,
	LEGS_FMM_4_1			,
	LEGS_FMM_4_2			,
	LEGS_FMM_4_3			, // Moved to DLC
	LEGS_FMM_4_4			,
	LEGS_FMM_4_5			, // Moved to DLC
	LEGS_FMM_4_6			, // Moved to DLC
	LEGS_FMM_4_7			, // Moved to DLC
	LEGS_FMM_4_8			, // Moved to DLC
	LEGS_FMM_4_9			, // Moved to DLC
	LEGS_FMM_4_10			, // Moved to DLC
	LEGS_FMM_4_11			, // Moved to DLC
	LEGS_FMM_4_12			, // Moved to DLC
	LEGS_FMM_4_13			, // Moved to DLC
	LEGS_FMM_4_14			, // Moved to DLC
	LEGS_FMM_4_15			, // Moved to DLC
	LEGS_FMM_5_0			,
	LEGS_FMM_5_1			,
	LEGS_FMM_5_2			,
	LEGS_FMM_5_3			,
	LEGS_FMM_5_4			,
	LEGS_FMM_5_5			,
	LEGS_FMM_5_6			,
	LEGS_FMM_5_7			,
	LEGS_FMM_5_8			,
	LEGS_FMM_5_9			,
	LEGS_FMM_5_10			,
	LEGS_FMM_5_11			,
	LEGS_FMM_5_12			,
	LEGS_FMM_5_13			,
	LEGS_FMM_5_14			,
	LEGS_FMM_5_15			,
	LEGS_FMM_6_0			,
	LEGS_FMM_6_1			,
	LEGS_FMM_6_2			,
	LEGS_FMM_6_3			, // Moved to DLC
	LEGS_FMM_6_4			, // Moved to DLC
	LEGS_FMM_6_5			, // Moved to DLC
	LEGS_FMM_6_6			, // Moved to DLC
	LEGS_FMM_6_7			, // Moved to DLC
	LEGS_FMM_6_8			, // Moved to DLC
	LEGS_FMM_6_9			, // Moved to DLC
	LEGS_FMM_6_10			,
	LEGS_FMM_6_11			, // Moved to DLC
	LEGS_FMM_6_12			, // Moved to DLC
	LEGS_FMM_6_13			, // Moved to DLC
	LEGS_FMM_6_14			, // Moved to DLC
	LEGS_FMM_6_15			, // Moved to DLC
	LEGS_FMM_7_0			,
	LEGS_FMM_7_1			,
	LEGS_FMM_7_2			,
	LEGS_FMM_7_3			,
	LEGS_FMM_7_4			,
	LEGS_FMM_7_5			,
	LEGS_FMM_7_6			,
	LEGS_FMM_7_7			,
	LEGS_FMM_7_8			,
	LEGS_FMM_7_9			,
	LEGS_FMM_7_10			,
	LEGS_FMM_7_11			,
	LEGS_FMM_7_12			,
	LEGS_FMM_7_13			,
	LEGS_FMM_7_14			,
	LEGS_FMM_7_15			,
	LEGS_FMM_8_0			,
	LEGS_FMM_8_1			, // Moved to DLC
	LEGS_FMM_8_2			, // Moved to DLC
	LEGS_FMM_8_3			,
	LEGS_FMM_8_4			,
	LEGS_FMM_8_5			, // Moved to DLC
	LEGS_FMM_8_6			, // Moved to DLC
	LEGS_FMM_8_7			, // Moved to DLC
	LEGS_FMM_8_8			, // Moved to DLC
	LEGS_FMM_8_9			, // Moved to DLC
	LEGS_FMM_8_10			, // Moved to DLC
	LEGS_FMM_8_11			, // Moved to DLC
	LEGS_FMM_8_12			, // Moved to DLC
	LEGS_FMM_8_13			, // Moved to DLC
	LEGS_FMM_8_14			,
	LEGS_FMM_8_15			, // Moved to DLC
	LEGS_FMM_9_0			,
	LEGS_FMM_9_1			,
	LEGS_FMM_9_2			,
	LEGS_FMM_9_3			,
	LEGS_FMM_9_4			,
	LEGS_FMM_9_5			,
	LEGS_FMM_9_6			,
	LEGS_FMM_9_7			,
	LEGS_FMM_9_8			,
	LEGS_FMM_9_9			,
	LEGS_FMM_9_10			,
	LEGS_FMM_9_11			,
	LEGS_FMM_9_12			,
	LEGS_FMM_9_13			,
	LEGS_FMM_9_14			,
	LEGS_FMM_9_15			,
	LEGS_FMM_10_0			,
	LEGS_FMM_10_1			,
	LEGS_FMM_10_2			,
	LEGS_FMM_10_3			, // Moved to DLC
	LEGS_FMM_10_4			, // Moved to DLC
	LEGS_FMM_10_5			, // Moved to DLC
	LEGS_FMM_10_6			, // Moved to DLC
	LEGS_FMM_10_7			, // Moved to DLC
	LEGS_FMM_10_8			, // Moved to DLC
	LEGS_FMM_10_9			, // Moved to DLC
	LEGS_FMM_10_10			, // Moved to DLC
	LEGS_FMM_10_11			, // Moved to DLC
	LEGS_FMM_10_12			, // Moved to DLC
	LEGS_FMM_10_13			, // Moved to DLC
	LEGS_FMM_10_14			, // Moved to DLC
	LEGS_FMM_10_15			, // Moved to DLC
	LEGS_FMM_11_0			, // Moved to DLC
	LEGS_FMM_11_1			, // Moved to DLC
	LEGS_FMM_11_2			, // Moved to DLC
	LEGS_FMM_11_3			, // Moved to DLC
	LEGS_FMM_11_4			, // Moved to DLC
	LEGS_FMM_11_5			, // Moved to DLC
	LEGS_FMM_11_6			, // Moved to DLC
	LEGS_FMM_11_7			, // Moved to DLC
	LEGS_FMM_11_8			, // Moved to DLC
	LEGS_FMM_11_9			, // Moved to DLC
	LEGS_FMM_11_10			, // Moved to DLC
	LEGS_FMM_11_11			, // Moved to DLC
	LEGS_FMM_11_12			, // Moved to DLC
	LEGS_FMM_11_13			, // Moved to DLC
	LEGS_FMM_11_14			, // Moved to DLC
	LEGS_FMM_11_15			, // Moved to DLC
	LEGS_FMM_12_0			,
	LEGS_FMM_12_1			, // Moved to DLC
	LEGS_FMM_12_2			, // Moved to DLC
	LEGS_FMM_12_3			, // Moved to DLC
	LEGS_FMM_12_4			,
	LEGS_FMM_12_5			,
	LEGS_FMM_12_6			, // Moved to DLC
	LEGS_FMM_12_7			,
	LEGS_FMM_12_8			, // Moved to DLC
	LEGS_FMM_12_9			, // Moved to DLC
	LEGS_FMM_12_10			, // Moved to DLC
	LEGS_FMM_12_11			, // Moved to DLC
	LEGS_FMM_12_12			,
	LEGS_FMM_12_13			, // Moved to DLC
	LEGS_FMM_12_14			, // Moved to DLC
	LEGS_FMM_12_15			, // Moved to DLC
	LEGS_FMM_13_0			,
	LEGS_FMM_13_1			,
	LEGS_FMM_13_2			,
	LEGS_FMM_13_3			, // Moved to DLC
	LEGS_FMM_13_4			, // Moved to DLC
	LEGS_FMM_13_5			, // Moved to DLC
	LEGS_FMM_13_6			, // Moved to DLC
	LEGS_FMM_13_7			, // Moved to DLC
	LEGS_FMM_13_8			, // Moved to DLC
	LEGS_FMM_13_9			, // Moved to DLC
	LEGS_FMM_13_10			, // Moved to DLC
	LEGS_FMM_13_11			, // Moved to DLC
	LEGS_FMM_13_12			, // Moved to DLC
	LEGS_FMM_13_13			, // Moved to DLC
	LEGS_FMM_13_14			, // Moved to DLC
	LEGS_FMM_13_15			, // Moved to DLC
	LEGS_FMM_14_0			,
	LEGS_FMM_14_1			,
	LEGS_FMM_14_2			, // Moved to DLC
	LEGS_FMM_14_3			,
	LEGS_FMM_14_4			, // Moved to DLC
	LEGS_FMM_14_5			, // Moved to DLC
	LEGS_FMM_14_6			, // Moved to DLC
	LEGS_FMM_14_7			, // Moved to DLC
	LEGS_FMM_14_8			, // Moved to DLC
	LEGS_FMM_14_9			, // Moved to DLC
	LEGS_FMM_14_10			, // Moved to DLC
	LEGS_FMM_14_11			, // Moved to DLC
	LEGS_FMM_14_12			,
	LEGS_FMM_14_13			, // Moved to DLC
	LEGS_FMM_14_14			, // Moved to DLC
	LEGS_FMM_14_15			, // Moved to DLC
	LEGS_FMM_15_0			,
	LEGS_FMM_15_1			,
	LEGS_FMM_15_2			,
	LEGS_FMM_15_3			,
	LEGS_FMM_15_4			,
	LEGS_FMM_15_5			,
	LEGS_FMM_15_6			,
	LEGS_FMM_15_7			,
	LEGS_FMM_15_8			,
	LEGS_FMM_15_9			,
	LEGS_FMM_15_10			,
	LEGS_FMM_15_11			,
	LEGS_FMM_15_12			,
	LEGS_FMM_15_13			,
	LEGS_FMM_15_14			,
	LEGS_FMM_15_15			,
	LEGS_FMM_DLC			,

	FEET_FMM_0_0			=0, // Moved to DLC
	FEET_FMM_0_1			, // Moved to DLC
	FEET_FMM_0_2			, // Moved to DLC
	FEET_FMM_0_3			, // Moved to DLC
	FEET_FMM_0_4			, // Moved to DLC
	FEET_FMM_0_5			, // Moved to DLC
	FEET_FMM_0_6			, // Moved to DLC
	FEET_FMM_0_7			, // Moved to DLC
	FEET_FMM_0_8			, // Moved to DLC
	FEET_FMM_0_9			, // Moved to DLC
	FEET_FMM_0_10			,
	FEET_FMM_0_11			, // Moved to DLC
	FEET_FMM_0_12			, // Moved to DLC
	FEET_FMM_0_13			, // Moved to DLC
	FEET_FMM_0_14			, // Moved to DLC
	FEET_FMM_0_15			, // Moved to DLC
	FEET_FMM_1_0			,
	FEET_FMM_1_1			,
	FEET_FMM_1_2			,
	FEET_FMM_1_3			,
	FEET_FMM_1_4			,
	FEET_FMM_1_5			,
	FEET_FMM_1_6			,
	FEET_FMM_1_7			,
	FEET_FMM_1_8			,
	FEET_FMM_1_9			,
	FEET_FMM_1_10			,
	FEET_FMM_1_11			,
	FEET_FMM_1_12			,
	FEET_FMM_1_13			,
	FEET_FMM_1_14			,
	FEET_FMM_1_15			,
	FEET_FMM_2_0			, // Moved to DLC
	FEET_FMM_2_1			, // Moved to DLC
	FEET_FMM_2_2			, // Moved to DLC
	FEET_FMM_2_3			, // Moved to DLC
	FEET_FMM_2_4			, // Moved to DLC
	FEET_FMM_2_5			, // Moved to DLC
	FEET_FMM_2_6			,
	FEET_FMM_2_7			, // Moved to DLC
	FEET_FMM_2_8			, // Moved to DLC
	FEET_FMM_2_9			, // Moved to DLC
	FEET_FMM_2_10			, // Moved to DLC
	FEET_FMM_2_11			, // Moved to DLC
	FEET_FMM_2_12			, // Moved to DLC
	FEET_FMM_2_13			,
	FEET_FMM_2_14			, // Moved to DLC
	FEET_FMM_2_15			, // Moved to DLC
	FEET_FMM_3_0			,
	FEET_FMM_3_1			,
	FEET_FMM_3_2			,
	FEET_FMM_3_3			,
	FEET_FMM_3_4			,
	FEET_FMM_3_5			,
	FEET_FMM_3_6			,
	FEET_FMM_3_7			,
	FEET_FMM_3_8			,
	FEET_FMM_3_9			,
	FEET_FMM_3_10			,
	FEET_FMM_3_11			,
	FEET_FMM_3_12			,
	FEET_FMM_3_13			,
	FEET_FMM_3_14			,
	FEET_FMM_3_15			,
	FEET_FMM_4_0			,
	FEET_FMM_4_1			,
	FEET_FMM_4_2			,
	FEET_FMM_4_3			, // Moved to DLC
	FEET_FMM_4_4			,
	FEET_FMM_4_5			, // Moved to DLC
	FEET_FMM_4_6			, // Moved to DLC
	FEET_FMM_4_7			, // Moved to DLC
	FEET_FMM_4_8			, // Moved to DLC
	FEET_FMM_4_9			, // Moved to DLC
	FEET_FMM_4_10			, // Moved to DLC
	FEET_FMM_4_11			, // Moved to DLC
	FEET_FMM_4_12			, // Moved to DLC
	FEET_FMM_4_13			, // Moved to DLC
	FEET_FMM_4_14			, // Moved to DLC
	FEET_FMM_4_15			, // Moved to DLC
	FEET_FMM_5_0			,
	FEET_FMM_5_1			,
	FEET_FMM_5_2			,
	FEET_FMM_5_3			,
	FEET_FMM_5_4			, // Moved to DLC
	FEET_FMM_5_5			, // Moved to DLC
	FEET_FMM_5_6			, // Moved to DLC
	FEET_FMM_5_7			, // Moved to DLC
	FEET_FMM_5_8			, // Moved to DLC
	FEET_FMM_5_9			, // Moved to DLC
	FEET_FMM_5_10			, // Moved to DLC
	FEET_FMM_5_11			, // Moved to DLC
	FEET_FMM_5_12			, // Moved to DLC
	FEET_FMM_5_13			, // Moved to DLC
	FEET_FMM_5_14			, // Moved to DLC
	FEET_FMM_5_15			, // Moved to DLC
	FEET_FMM_6_0			,
	FEET_FMM_6_1			,
	FEET_FMM_6_2			, // Moved to DLC
	FEET_FMM_6_3			, // Moved to DLC
	FEET_FMM_6_4			, // Moved to DLC
	FEET_FMM_6_5			, // Moved to DLC
	FEET_FMM_6_6			, // Moved to DLC
	FEET_FMM_6_7			, // Moved to DLC
	FEET_FMM_6_8			, // Moved to DLC
	FEET_FMM_6_9			, // Moved to DLC
	FEET_FMM_6_10			, // Moved to DLC
	FEET_FMM_6_11			, // Moved to DLC
	FEET_FMM_6_12			, // Moved to DLC
	FEET_FMM_6_13			, // Moved to DLC
	FEET_FMM_6_14			, // Moved to DLC
	FEET_FMM_6_15			, // Moved to DLC
	FEET_FMM_7_0			,
	FEET_FMM_7_1			,
	FEET_FMM_7_2			,
	FEET_FMM_7_3			,
	FEET_FMM_7_4			,
	FEET_FMM_7_5			,
	FEET_FMM_7_6			,
	FEET_FMM_7_7			,
	FEET_FMM_7_8			,
	FEET_FMM_7_9			,
	FEET_FMM_7_10			,
	FEET_FMM_7_11			,
	FEET_FMM_7_12			,
	FEET_FMM_7_13			,
	FEET_FMM_7_14			,
	FEET_FMM_7_15			,
	FEET_FMM_8_0			,
	FEET_FMM_8_1			,
	FEET_FMM_8_2			,
	FEET_FMM_8_3			,
	FEET_FMM_8_4			,
	FEET_FMM_8_5			,
	FEET_FMM_8_6			,
	FEET_FMM_8_7			,
	FEET_FMM_8_8			,
	FEET_FMM_8_9			,
	FEET_FMM_8_10			,
	FEET_FMM_8_11			,
	FEET_FMM_8_12			,
	FEET_FMM_8_13			,
	FEET_FMM_8_14			,
	FEET_FMM_8_15			,
	FEET_FMM_9_0			,
	FEET_FMM_9_1			,
	FEET_FMM_9_2			,
	FEET_FMM_9_3			,
	FEET_FMM_9_4			,
	FEET_FMM_9_5			,
	FEET_FMM_9_6			,
	FEET_FMM_9_7			,
	FEET_FMM_9_8			,
	FEET_FMM_9_9			,
	FEET_FMM_9_10			,
	FEET_FMM_9_11			,
	FEET_FMM_9_12			,
	FEET_FMM_9_13			,
	FEET_FMM_9_14			,
	FEET_FMM_9_15			,
	FEET_FMM_10_0			,
	FEET_FMM_10_1			, // Moved to DLC
	FEET_FMM_10_2			, // Moved to DLC
	FEET_FMM_10_3			, // Moved to DLC
	FEET_FMM_10_4			, // Moved to DLC
	FEET_FMM_10_5			, // Moved to DLC
	FEET_FMM_10_6			, // Moved to DLC
	FEET_FMM_10_7			,
	FEET_FMM_10_8			, // Moved to DLC
	FEET_FMM_10_9			, // Moved to DLC
	FEET_FMM_10_10			, // Moved to DLC
	FEET_FMM_10_11			, // Moved to DLC
	FEET_FMM_10_12			,
	FEET_FMM_10_13			, // Moved to DLC
	FEET_FMM_10_14			,
	FEET_FMM_10_15			, // Moved to DLC
	FEET_FMM_11_0			,
	FEET_FMM_11_1			,
	FEET_FMM_11_2			,
	FEET_FMM_11_3			,
	FEET_FMM_11_4			,
	FEET_FMM_11_5			,
	FEET_FMM_11_6			,
	FEET_FMM_11_7			,
	FEET_FMM_11_8			,
	FEET_FMM_11_9			,
	FEET_FMM_11_10			,
	FEET_FMM_11_11			,
	FEET_FMM_11_12			,
	FEET_FMM_11_13			,
	FEET_FMM_11_14			,
	FEET_FMM_11_15			,
	FEET_FMM_12_0			,
	FEET_FMM_12_1			,
	FEET_FMM_12_2			,
	FEET_FMM_12_3			,
	FEET_FMM_12_4			,
	FEET_FMM_12_5			,
	FEET_FMM_12_6			,
	FEET_FMM_12_7			,
	FEET_FMM_12_8			,
	FEET_FMM_12_9			,
	FEET_FMM_12_10			,
	FEET_FMM_12_11			,
	FEET_FMM_12_12			,
	FEET_FMM_12_13			,
	FEET_FMM_12_14			,
	FEET_FMM_12_15			,
	FEET_FMM_13_0			, // Moved to DLC
	FEET_FMM_13_1			, // Moved to DLC
	FEET_FMM_13_2			, // Moved to DLC
	FEET_FMM_13_3			, // Moved to DLC
	FEET_FMM_13_4			, // Moved to DLC
	FEET_FMM_13_5			, // Moved to DLC
	FEET_FMM_13_6			, // Moved to DLC
	FEET_FMM_13_7			, // Moved to DLC
	FEET_FMM_13_8			, // Moved to DLC
	FEET_FMM_13_9			, // Moved to DLC
	FEET_FMM_13_10			, // Moved to DLC 
	FEET_FMM_13_11			, // Moved to DLC
	FEET_FMM_13_12			, // Moved to DLC
	FEET_FMM_13_13			, // Moved to DLC
	FEET_FMM_13_14			, // Moved to DLC 
	FEET_FMM_13_15			, // Moved to DLC
	FEET_FMM_14_0			,
	FEET_FMM_14_1			,
	FEET_FMM_14_2			,
	FEET_FMM_14_3			,
	FEET_FMM_14_4			,
	FEET_FMM_14_5			,
	FEET_FMM_14_6			,
	FEET_FMM_14_7			,
	FEET_FMM_14_8			,
	FEET_FMM_14_9			,
	FEET_FMM_14_10			,
	FEET_FMM_14_11			,
	FEET_FMM_14_12			,
	FEET_FMM_14_13			,
	FEET_FMM_14_14			,
	FEET_FMM_14_15			,
	FEET_FMM_15_0			,
	FEET_FMM_15_1			,
	FEET_FMM_15_2			,
	FEET_FMM_15_3			,
	FEET_FMM_15_4			,
	FEET_FMM_15_5			,
	FEET_FMM_15_6			,
	FEET_FMM_15_7			,
	FEET_FMM_15_8			,
	FEET_FMM_15_9			,
	FEET_FMM_15_10			,
	FEET_FMM_15_11			,
	FEET_FMM_15_12			,
	FEET_FMM_15_13			,
	FEET_FMM_15_14			,
	FEET_FMM_15_15			,
	FEET_FMM_DLC			,

	SPECIAL_FMM_0_0			=0,
	SPECIAL_FMM_0_1			,
	SPECIAL_FMM_0_2			,
	SPECIAL_FMM_0_3			,
	SPECIAL_FMM_0_4			,
	SPECIAL_FMM_0_5			,
	SPECIAL_FMM_0_6			, // moved to DLC
	SPECIAL_FMM_0_7			,
	SPECIAL_FMM_0_8			,
	SPECIAL_FMM_0_9			, // moved to DLC
	SPECIAL_FMM_0_10		, // moved to DLC
	SPECIAL_FMM_0_11		,
	SPECIAL_FMM_0_12		, // moved to DLC
	SPECIAL_FMM_0_13		, // moved to DLC
	SPECIAL_FMM_0_14		, // moved to DLC
	SPECIAL_FMM_0_15		, // moved to DLC
	SPECIAL_FMM_1_0			,
	SPECIAL_FMM_1_1			,
	SPECIAL_FMM_1_2			, // moved to DLC
	SPECIAL_FMM_1_3			,
	SPECIAL_FMM_1_4			,
	SPECIAL_FMM_1_5			,
	SPECIAL_FMM_1_6			,
	SPECIAL_FMM_1_7			,
	SPECIAL_FMM_1_8			,
	SPECIAL_FMM_1_9			, // moved to DLC
	SPECIAL_FMM_1_10		, // moved to DLC
	SPECIAL_FMM_1_11		,
	SPECIAL_FMM_1_12		,
	SPECIAL_FMM_1_13		, // moved to DLC
	SPECIAL_FMM_1_14		, 
	SPECIAL_FMM_1_15		, // moved to DLC
	SPECIAL_FMM_2_0			,
	SPECIAL_FMM_2_1			,
	SPECIAL_FMM_2_2			,
	SPECIAL_FMM_2_3			,
	SPECIAL_FMM_2_4			,
	SPECIAL_FMM_2_5			,
	SPECIAL_FMM_2_6			,
	SPECIAL_FMM_2_7			,
	SPECIAL_FMM_2_8			,
	SPECIAL_FMM_2_9			,
	SPECIAL_FMM_2_10		,
	SPECIAL_FMM_2_11		,
	SPECIAL_FMM_2_12		,
	SPECIAL_FMM_2_13		,
	SPECIAL_FMM_2_14		,
	SPECIAL_FMM_2_15		,
	SPECIAL_FMM_3_0			,
	SPECIAL_FMM_3_1			,
	SPECIAL_FMM_3_2			,
	SPECIAL_FMM_3_3			,// moved to DLC
	SPECIAL_FMM_3_4			,// moved to DLC
	SPECIAL_FMM_3_5			,// moved to DLC
	SPECIAL_FMM_3_6			,// moved to DLC
	SPECIAL_FMM_3_7			,// moved to DLC
	SPECIAL_FMM_3_8			,// moved to DLC
	SPECIAL_FMM_3_9			,// moved to DLC
	SPECIAL_FMM_3_10		,// moved to DLC
	SPECIAL_FMM_3_11		,// moved to DLC
	SPECIAL_FMM_3_12		,// moved to DLC
	SPECIAL_FMM_3_13		,// moved to DLC
	SPECIAL_FMM_3_14		,// moved to DLC
	SPECIAL_FMM_3_15		,// moved to DLC
	SPECIAL_FMM_4_0			,
	SPECIAL_FMM_4_1			,
	SPECIAL_FMM_4_2			,
	SPECIAL_FMM_4_3			,// moved to DLC
	SPECIAL_FMM_4_4			,// moved to DLC
	SPECIAL_FMM_4_5			,// moved to DLC
	SPECIAL_FMM_4_6			,// moved to DLC
	SPECIAL_FMM_4_7			,// moved to DLC
	SPECIAL_FMM_4_8			,// moved to DLC
	SPECIAL_FMM_4_9			,// moved to DLC
	SPECIAL_FMM_4_10		,// moved to DLC
	SPECIAL_FMM_4_11		,// moved to DLC
	SPECIAL_FMM_4_12		,// moved to DLC
	SPECIAL_FMM_4_13		,// moved to DLC
	SPECIAL_FMM_4_14		,// moved to DLC
	SPECIAL_FMM_4_15		,// moved to DLC
	SPECIAL_FMM_5_0			,
	SPECIAL_FMM_5_1			,
	SPECIAL_FMM_5_2			,
	SPECIAL_FMM_5_3			, // moved to DLC
	SPECIAL_FMM_5_4			, // moved to DLC
	SPECIAL_FMM_5_5			, // moved to DLC
	SPECIAL_FMM_5_6			, // moved to DLC
	SPECIAL_FMM_5_7			,
	SPECIAL_FMM_5_8			, // moved to DLC
	SPECIAL_FMM_5_9			, // moved to DLC
	SPECIAL_FMM_5_10		, // moved to DLC
	SPECIAL_FMM_5_11		, // moved to DLC
	SPECIAL_FMM_5_12		, // moved to DLC
	SPECIAL_FMM_5_13		, // moved to DLC
	SPECIAL_FMM_5_14		, // moved to DLC
	SPECIAL_FMM_5_15		, // moved to DLC
	SPECIAL_FMM_6_0			,
	SPECIAL_FMM_6_1			,
	SPECIAL_FMM_6_2			,
	SPECIAL_FMM_6_3			,
	SPECIAL_FMM_6_4			,
	SPECIAL_FMM_6_5			,
	SPECIAL_FMM_6_6			,
	SPECIAL_FMM_6_7			,
	SPECIAL_FMM_6_8			,
	SPECIAL_FMM_6_9			,
	SPECIAL_FMM_6_10		,
	SPECIAL_FMM_6_11		,
	SPECIAL_FMM_6_12		,
	SPECIAL_FMM_6_13		,
	SPECIAL_FMM_6_14		,
	SPECIAL_FMM_6_15		,
	SPECIAL_FMM_7_0			,
	SPECIAL_FMM_7_1			,
	SPECIAL_FMM_7_2			,
	SPECIAL_FMM_7_3			,
	SPECIAL_FMM_7_4			,
	SPECIAL_FMM_7_5			,
	SPECIAL_FMM_7_6			,
	SPECIAL_FMM_7_7			,
	SPECIAL_FMM_7_8			,
	SPECIAL_FMM_7_9			,
	SPECIAL_FMM_7_10		,
	SPECIAL_FMM_7_11		,
	SPECIAL_FMM_7_12		,
	SPECIAL_FMM_7_13		,
	SPECIAL_FMM_7_14		,
	SPECIAL_FMM_7_15		,
	SPECIAL_FMM_8_0			,
	SPECIAL_FMM_8_1			, // moved to DLC
	SPECIAL_FMM_8_2			, // moved to DLC
	SPECIAL_FMM_8_3			, // moved to DLC
	SPECIAL_FMM_8_4			, // moved to DLC
	SPECIAL_FMM_8_5			, // moved to DLC
	SPECIAL_FMM_8_6			, // moved to DLC
	SPECIAL_FMM_8_7			, // moved to DLC
	SPECIAL_FMM_8_8			, // moved to DLC
	SPECIAL_FMM_8_9			, // moved to DLC
	SPECIAL_FMM_8_10		,
	SPECIAL_FMM_8_11		, // moved to DLC
	SPECIAL_FMM_8_12		, // moved to DLC
	SPECIAL_FMM_8_13		,
	SPECIAL_FMM_8_14		,
	SPECIAL_FMM_8_15		, // moved to DLC
	SPECIAL_FMM_9_0			,
	SPECIAL_FMM_9_1			,
	SPECIAL_FMM_9_2			,
	SPECIAL_FMM_9_3			,
	SPECIAL_FMM_9_4			,
	SPECIAL_FMM_9_5			,
	SPECIAL_FMM_9_6			,
	SPECIAL_FMM_9_7			,
	SPECIAL_FMM_9_8			, // moved to DLC
	SPECIAL_FMM_9_9			, // moved to DLC
	SPECIAL_FMM_9_10		,
	SPECIAL_FMM_9_11		,
	SPECIAL_FMM_9_12		,
	SPECIAL_FMM_9_13		,
	SPECIAL_FMM_9_14		,
	SPECIAL_FMM_9_15		,
	SPECIAL_FMM_10_0		,
	SPECIAL_FMM_10_1		,
	SPECIAL_FMM_10_2		,
	SPECIAL_FMM_10_3		,
	SPECIAL_FMM_10_4		,
	SPECIAL_FMM_10_5		,
	SPECIAL_FMM_10_6		,
	SPECIAL_FMM_10_7		,
	SPECIAL_FMM_10_8		,
	SPECIAL_FMM_10_9		,
	SPECIAL_FMM_10_10		,
	SPECIAL_FMM_10_11		,
	SPECIAL_FMM_10_12		,
	SPECIAL_FMM_10_13		,
	SPECIAL_FMM_10_14		,
	SPECIAL_FMM_10_15		,
	SPECIAL_FMM_11_0		,
	SPECIAL_FMM_11_1		,
	SPECIAL_FMM_11_2		,
	SPECIAL_FMM_11_3		,
	SPECIAL_FMM_11_4		,
	SPECIAL_FMM_11_5		,
	SPECIAL_FMM_11_6		,
	SPECIAL_FMM_11_7		,
	SPECIAL_FMM_11_8		,
	SPECIAL_FMM_11_9		,
	SPECIAL_FMM_11_10		,
	SPECIAL_FMM_11_11		,
	SPECIAL_FMM_11_12		,
	SPECIAL_FMM_11_13		,
	SPECIAL_FMM_11_14		,
	SPECIAL_FMM_11_15		,
	SPECIAL_FMM_12_0		,
	SPECIAL_FMM_12_1		,
	SPECIAL_FMM_12_2		,
	SPECIAL_FMM_12_3		,
	SPECIAL_FMM_12_4		,
	SPECIAL_FMM_12_5		,
	SPECIAL_FMM_12_6		,
	SPECIAL_FMM_12_7		,
	SPECIAL_FMM_12_8		,
	SPECIAL_FMM_12_9		,
	SPECIAL_FMM_12_10		,
	SPECIAL_FMM_12_11		,
	SPECIAL_FMM_12_12		, // moved to DLC
	SPECIAL_FMM_12_13		, // moved to DLC
	SPECIAL_FMM_12_14		, // moved to DLC
	SPECIAL_FMM_12_15		, // moved to DLC
	SPECIAL_FMM_13_0		,
	SPECIAL_FMM_13_1		,
	SPECIAL_FMM_13_2		,
	SPECIAL_FMM_13_3		,
	SPECIAL_FMM_13_4		, // moved to DLC
	SPECIAL_FMM_13_5		,
	SPECIAL_FMM_13_6		, // moved to DLC
	SPECIAL_FMM_13_7		, // moved to DLC
	SPECIAL_FMM_13_8		, // moved to DLC
	SPECIAL_FMM_13_9		, // moved to DLC
	SPECIAL_FMM_13_10		, // moved to DLC
	SPECIAL_FMM_13_11		, // moved to DLC
	SPECIAL_FMM_13_12		, // moved to DLC
	SPECIAL_FMM_13_13		,
	SPECIAL_FMM_13_14		, // moved to DLC
	SPECIAL_FMM_13_15		, // moved to DLC
	SPECIAL_FMM_14_0		,
	SPECIAL_FMM_14_1		,
	SPECIAL_FMM_14_2		,
	SPECIAL_FMM_14_3		,
	SPECIAL_FMM_14_4		,
	SPECIAL_FMM_14_5		,
	SPECIAL_FMM_14_6		,
	SPECIAL_FMM_14_7		,
	SPECIAL_FMM_14_8		,
	SPECIAL_FMM_14_9		,
	SPECIAL_FMM_14_10		,
	SPECIAL_FMM_14_11		,
	SPECIAL_FMM_14_12		,
	SPECIAL_FMM_14_13		,
	SPECIAL_FMM_14_14		,
	SPECIAL_FMM_14_15		,
	SPECIAL_FMM_15_0		,
	SPECIAL_FMM_DLC			,
	
	SPECIAL2_FMM_0_0			=0,
	SPECIAL2_FMM_1_0			,
	SPECIAL2_FMM_1_1			,
	SPECIAL2_FMM_1_2			,
	SPECIAL2_FMM_1_3			,
	SPECIAL2_FMM_1_4			,
	SPECIAL2_FMM_2_0			,
	SPECIAL2_FMM_2_1			,
	SPECIAL2_FMM_2_2			,
	SPECIAL2_FMM_2_3			,
	SPECIAL2_FMM_2_4			,
	SPECIAL2_FMM_3_0			,
	SPECIAL2_FMM_3_1			,
	SPECIAL2_FMM_3_2			,
	SPECIAL2_FMM_3_3			,
	SPECIAL2_FMM_3_4			,
	SPECIAL2_FMM_4_0			,
	SPECIAL2_FMM_4_1			,
	SPECIAL2_FMM_4_2			,
	SPECIAL2_FMM_4_3			,
	SPECIAL2_FMM_4_4			,
	SPECIAL2_FMM_5_0			,
	SPECIAL2_FMM_5_1			,
	SPECIAL2_FMM_5_2			,
	SPECIAL2_FMM_5_3			,
	SPECIAL2_FMM_5_4			,
	SPECIAL2_FMM_6_0			,
	SPECIAL2_FMM_6_1			,
	SPECIAL2_FMM_6_2			,
	SPECIAL2_FMM_6_3			,
	SPECIAL2_FMM_6_4			,
	SPECIAL2_FMM_7_0			,
	SPECIAL2_FMM_7_1			,
	SPECIAL2_FMM_7_2			,
	SPECIAL2_FMM_7_3			,
	SPECIAL2_FMM_7_4			,
	SPECIAL2_FMM_8_0			,
	SPECIAL2_FMM_8_1			,
	SPECIAL2_FMM_8_2			,
	SPECIAL2_FMM_8_3			,
	SPECIAL2_FMM_8_4			,
	SPECIAL2_FMM_9_0			,
	SPECIAL2_FMM_9_1			,
	SPECIAL2_FMM_9_2			,
	SPECIAL2_FMM_9_3			,
	SPECIAL2_FMM_9_4			,
	SPECIAL2_FMM_DLC			,
	
	DECL_FMM_0_0			=0,
	DECL_FMM_1_0			,
	DECL_FMM_2_0			,
	DECL_FMM_3_0			,
	DECL_FMM_4_0			,
	DECL_FMM_5_0			,
	DECL_FMM_6_0			,
	DECL_FMM_DLC			,

	BERD_FMM_0_0			=0,
	BERD_FMM_1_0			,
	BERD_FMM_1_1			,
	BERD_FMM_1_2			,
	BERD_FMM_1_3			,
	BERD_FMM_2_0			,
	BERD_FMM_2_1			,
	BERD_FMM_2_2			,
	BERD_FMM_2_3			,
	BERD_FMM_3_0			,
	BERD_FMM_4_0			,
	BERD_FMM_4_1			,
	BERD_FMM_4_2			,
	BERD_FMM_4_3			,
	BERD_FMM_5_0			,
	BERD_FMM_5_1			,
	BERD_FMM_5_2			,
	BERD_FMM_5_3			,
	BERD_FMM_6_0			,
	BERD_FMM_6_1			,
	BERD_FMM_6_2			,
	BERD_FMM_6_3			,
	BERD_FMM_7_0			,
	BERD_FMM_7_1			,
	BERD_FMM_7_2			,
	BERD_FMM_7_3			,
	BERD_FMM_DLC			,

	TEETH_FMM_0_0			= 0, 
	TEETH_FMM_1_0			, // moved to DLC
	TEETH_FMM_1_1			, // moved to DLC
	TEETH_FMM_1_2			, // moved to DLC
	TEETH_FMM_2_0			, // moved to DLC
	TEETH_FMM_2_1			, // moved to DLC
	TEETH_FMM_2_2			, // moved to DLC
	TEETH_FMM_3_0			, // moved to DLC
	TEETH_FMM_3_1			, // moved to DLC
	TEETH_FMM_3_2			, // moved to DLC
	TEETH_FMM_4_0			, // moved to DLC
	TEETH_FMM_4_1			, // moved to DLC
	TEETH_FMM_4_2			, // moved to DLC
	TEETH_FMM_4_3			, // moved to DLC
	TEETH_FMM_4_4			, // moved to DLC
	TEETH_FMM_4_5			, // moved to DLC
	TEETH_FMM_4_6			, // moved to DLC
	TEETH_FMM_4_7			, // moved to DLC
	TEETH_FMM_4_8			, // moved to DLC
	TEETH_FMM_4_9			, // moved to DLC
	TEETH_FMM_4_10			, // moved to DLC
	TEETH_FMM_4_11			, // moved to DLC
	TEETH_FMM_4_12			, // moved to DLC
	TEETH_FMM_4_13			, // moved to DLC
	TEETH_FMM_4_14			, // moved to DLC
	TEETH_FMM_4_15			, // moved to DLC
	TEETH_FMM_5_0			,  // moved to DLC
	TEETH_FMM_5_1			, // moved to DLC
	TEETH_FMM_5_2			, // moved to DLC
	TEETH_FMM_5_3			, // moved to DLC
	TEETH_FMM_5_4			, // moved to DLC
	TEETH_FMM_5_5			, // moved to DLC
	TEETH_FMM_6_0			, // moved to DLC
	TEETH_FMM_6_1			, // moved to DLC
	TEETH_FMM_6_2			, // moved to DLC
	TEETH_FMM_6_3			, // moved to DLC
	TEETH_FMM_6_4			, // moved to DLC
	TEETH_FMM_6_5			, // moved to DLC
	TEETH_FMM_7_0			,  
	TEETH_FMM_8_0			, 
	TEETH_FMM_9_0			, 
	TEETH_FMM_10_0			, 
	TEETH_FMM_10_1			,
	TEETH_FMM_10_2			,
	TEETH_FMM_10_3			, // moved to DLC
	TEETH_FMM_10_4			, // moved to DLC
	TEETH_FMM_10_5			, // moved to DLC
	TEETH_FMM_10_6			, // moved to DLC
	TEETH_FMM_10_7			, // moved to DLC
	TEETH_FMM_10_8			, // moved to DLC
	TEETH_FMM_10_9			, // moved to DLC
	TEETH_FMM_10_10			, // moved to DLC
	TEETH_FMM_10_11			, // moved to DLC
	TEETH_FMM_10_12			, // moved to DLC
	TEETH_FMM_10_13			, // moved to DLC
	TEETH_FMM_10_14			, // moved to DLC
	TEETH_FMM_10_15			, // moved to DLC
	TEETH_FMM_11_0			, // moved to DLC
	TEETH_FMM_11_1			, // moved to DLC
	TEETH_FMM_11_2			,
	TEETH_FMM_11_3			, // moved to DLC
	TEETH_FMM_11_4			, // moved to DLC
	TEETH_FMM_11_5			, // moved to DLC
	TEETH_FMM_11_6			, // moved to DLC
	TEETH_FMM_11_7			, // moved to DLC
	TEETH_FMM_11_8			, // moved to DLC
	TEETH_FMM_11_9			, // moved to DLC
	TEETH_FMM_11_10			, // moved to DLC
	TEETH_FMM_11_11			, // moved to DLC
	TEETH_FMM_11_12			, // moved to DLC
	TEETH_FMM_11_13			, // moved to DLC
	TEETH_FMM_11_14			, // moved to DLC
	TEETH_FMM_11_15			, // moved to DLC
	TEETH_FMM_12_0			, 
	TEETH_FMM_12_1			,
	TEETH_FMM_12_2			,
	TEETH_FMM_12_3			, // moved to DLC
	TEETH_FMM_12_4			, // moved to DLC
	TEETH_FMM_12_5			, // moved to DLC
	TEETH_FMM_12_6			, // moved to DLC
	TEETH_FMM_12_7			, // moved to DLC
	TEETH_FMM_12_8			, // moved to DLC
	TEETH_FMM_12_9			, // moved to DLC
	TEETH_FMM_12_10			, // moved to DLC
	TEETH_FMM_12_11			, // moved to DLC
	TEETH_FMM_12_12			, // moved to DLC
	TEETH_FMM_12_13			, // moved to DLC
	TEETH_FMM_12_14			, // moved to DLC
	TEETH_FMM_12_15			, // moved to DLC
	TEETH_FMM_13_0			, 
	TEETH_FMM_14_0			,
	TEETH_FMM_15_0			, 
	TEETH_FMM_DLC			, 
	
	JBIB_FMM_0_0			=0,
	JBIB_FMM_0_1			,
	JBIB_FMM_0_2			,
	JBIB_FMM_0_3			,
	JBIB_FMM_0_4			,
	JBIB_FMM_0_5			,
	JBIB_FMM_0_6			, // Moved to DLC
	JBIB_FMM_0_7			,
	JBIB_FMM_0_8			,
	JBIB_FMM_0_9			, // Moved to DLC
	JBIB_FMM_0_10			, // Moved to DLC
	JBIB_FMM_0_11			,
	JBIB_FMM_0_12			, // Moved to DLC
	JBIB_FMM_0_13			, // Moved to DLC
	JBIB_FMM_0_14			, // Moved to DLC
	JBIB_FMM_0_15			, // Moved to DLC
	JBIB_FMM_1_0			,
	JBIB_FMM_1_1			,
	JBIB_FMM_1_2			, // Moved to DLC
	JBIB_FMM_1_3			,
	JBIB_FMM_1_4			,
	JBIB_FMM_1_5			,
	JBIB_FMM_1_6			,
	JBIB_FMM_1_7			,
	JBIB_FMM_1_8			,
	JBIB_FMM_1_9			, // Moved to DLC
	JBIB_FMM_1_10			, // Moved to DLC
	JBIB_FMM_1_11			,
	JBIB_FMM_1_12			,
	JBIB_FMM_1_13			, // Moved to DLC
	JBIB_FMM_1_14			, 
	JBIB_FMM_1_15			, // Moved to DLC
	JBIB_FMM_2_0			, // Moved to DLC
	JBIB_FMM_2_1			, // Moved to DLC
	JBIB_FMM_2_2			, // Moved to DLC
	JBIB_FMM_2_3			, // Moved to DLC
	JBIB_FMM_2_4			, // Moved to DLC
	JBIB_FMM_2_5			, // Moved to DLC
	JBIB_FMM_2_6			, // Moved to DLC
	JBIB_FMM_2_7			, // Moved to DLC
	JBIB_FMM_2_8			, // Moved to DLC
	JBIB_FMM_2_9			,
	JBIB_FMM_2_10			, // Moved to DLC
	JBIB_FMM_2_11			, // Moved to DLC
	JBIB_FMM_2_12			, // Moved to DLC
	JBIB_FMM_2_13			, // Moved to DLC
	JBIB_FMM_2_14			, // Moved to DLC
	JBIB_FMM_2_15			, // Moved to DLC
	JBIB_FMM_3_0			,
	JBIB_FMM_3_1			,
	JBIB_FMM_3_2			,
	JBIB_FMM_3_3			,
	JBIB_FMM_3_4			,
	JBIB_FMM_3_5			,
	JBIB_FMM_3_6			,
	JBIB_FMM_3_7			,
	JBIB_FMM_3_8			,
	JBIB_FMM_3_9			,
	JBIB_FMM_3_10			,
	JBIB_FMM_3_11			,
	JBIB_FMM_3_12			,
	JBIB_FMM_3_13			,
	JBIB_FMM_3_14			,
	JBIB_FMM_3_15			,
	JBIB_FMM_4_0			,
	JBIB_FMM_4_1			, // Moved to DLC
	JBIB_FMM_4_2			,
	JBIB_FMM_4_3			,
	JBIB_FMM_4_4			, // Moved to DLC
	JBIB_FMM_4_5			, // Moved to DLC
	JBIB_FMM_4_6			, // Moved to DLC
	JBIB_FMM_4_7			, // Moved to DLC
	JBIB_FMM_4_8			, // Moved to DLC
	JBIB_FMM_4_9			, // Moved to DLC
	JBIB_FMM_4_10			, // Moved to DLC
	JBIB_FMM_4_11			,
	JBIB_FMM_4_12			, // Moved to DLC
	JBIB_FMM_4_13			, // Moved to DLC
	JBIB_FMM_4_14			,
	JBIB_FMM_4_15			, // Moved to DLC
	JBIB_FMM_5_0			,
	JBIB_FMM_5_1			,
	JBIB_FMM_5_2			,
	JBIB_FMM_5_3			, // Moved to DLC
	JBIB_FMM_5_4			, // Moved to DLC
	JBIB_FMM_5_5			, // Moved to DLC
	JBIB_FMM_5_6			, // Moved to DLC
	JBIB_FMM_5_7			,
	JBIB_FMM_5_8			, // Moved to DLC
	JBIB_FMM_5_9			, // Moved to DLC
	JBIB_FMM_5_10			, // Moved to DLC
	JBIB_FMM_5_11			, // Moved to DLC
	JBIB_FMM_5_12			, // Moved to DLC
	JBIB_FMM_5_13			, // Moved to DLC
	JBIB_FMM_5_14			, // Moved to DLC
	JBIB_FMM_5_15			, // Moved to DLC
	JBIB_FMM_6_0			,
	JBIB_FMM_6_1			,
	JBIB_FMM_6_2			, // Moved to DLC
	JBIB_FMM_6_3			,
	JBIB_FMM_6_4			,
	JBIB_FMM_6_5			,
	JBIB_FMM_6_6			,
	JBIB_FMM_6_7			, // Moved to DLC
	JBIB_FMM_6_8			,
	JBIB_FMM_6_9			,
	JBIB_FMM_6_10			, // Moved to DLC
	JBIB_FMM_6_11			,
	JBIB_FMM_7_0			,
	JBIB_FMM_7_1			,
	JBIB_FMM_7_2			,
	JBIB_FMM_7_3			,
	JBIB_FMM_7_4			,
	JBIB_FMM_7_5			,
	JBIB_FMM_7_6			,
	JBIB_FMM_7_7			,
	JBIB_FMM_7_8			,
	JBIB_FMM_7_9			,
	JBIB_FMM_7_10			,
	JBIB_FMM_7_11			,
	JBIB_FMM_7_12			,
	JBIB_FMM_7_13			,
	JBIB_FMM_7_14			,
	JBIB_FMM_7_15			,
	JBIB_FMM_8_0			,
	JBIB_FMM_8_1			, // Moved to DLC
	JBIB_FMM_8_2			, // Moved to DLC
	JBIB_FMM_8_3			, // Moved to DLC
	JBIB_FMM_8_4			, // Moved to DLC
	JBIB_FMM_8_5			, // Moved to DLC
	JBIB_FMM_8_6			, // Moved to DLC
	JBIB_FMM_8_7			, // Moved to DLC
	JBIB_FMM_8_8			, // Moved to DLC
	JBIB_FMM_8_9			, // Moved to DLC
	JBIB_FMM_8_10			,
	JBIB_FMM_8_11			, // Moved to DLC
	JBIB_FMM_8_12			, // Moved to DLC
	JBIB_FMM_8_13			,
	JBIB_FMM_8_14			,
	JBIB_FMM_8_15			, // Moved to DLC
	JBIB_FMM_9_0			,
	JBIB_FMM_9_1			,
	JBIB_FMM_9_2			,
	JBIB_FMM_9_3			,
	JBIB_FMM_9_4			,
	JBIB_FMM_9_5			,
	JBIB_FMM_9_6			,
	JBIB_FMM_9_7			,
	JBIB_FMM_9_8			, // Moved to DLC
	JBIB_FMM_9_9			, // Moved to DLC
	JBIB_FMM_9_10			,
	JBIB_FMM_9_11			,
	JBIB_FMM_9_12			,
	JBIB_FMM_9_13			,
	JBIB_FMM_9_14			,
	JBIB_FMM_9_15			,
	JBIB_FMM_10_0			,
	JBIB_FMM_10_1			,
	JBIB_FMM_10_2			,
	JBIB_FMM_10_3			, // Moved to DLC
	JBIB_FMM_10_4			, // Moved to DLC
	JBIB_FMM_10_5			, // Moved to DLC
	JBIB_FMM_10_6			, // Moved to DLC
	JBIB_FMM_10_7			, // Moved to DLC
	JBIB_FMM_10_8			, // Moved to DLC
	JBIB_FMM_10_9			, // Moved to DLC
	JBIB_FMM_10_10			, // Moved to DLC
	JBIB_FMM_10_11			, // Moved to DLC
	JBIB_FMM_10_12			, // Moved to DLC
	JBIB_FMM_10_13			, // Moved to DLC
	JBIB_FMM_10_14			, // Moved to DLC
	JBIB_FMM_10_15			, // Moved to DLC
	JBIB_FMM_11_0			,
	JBIB_FMM_11_1			,
	JBIB_FMM_11_2			, // Moved to DLC
	JBIB_FMM_11_3			, // Moved to DLC
	JBIB_FMM_11_4			, // Moved to DLC
	JBIB_FMM_11_5			, // Moved to DLC
	JBIB_FMM_11_6			, // Moved to DLC
	JBIB_FMM_11_7			,
	JBIB_FMM_11_8			, // Moved to DLC
	JBIB_FMM_11_9			, // Moved to DLC
	JBIB_FMM_11_10			, // Moved to DLC
	JBIB_FMM_11_11			, // Moved to DLC
	JBIB_FMM_11_12			, // Moved to DLC
	JBIB_FMM_11_13			, // Moved to DLC
	JBIB_FMM_11_14			,
	JBIB_FMM_11_15			, // Moved to DLC
	JBIB_FMM_12_0			,
	JBIB_FMM_12_1			,
	JBIB_FMM_12_2			,
	JBIB_FMM_12_3			,
	JBIB_FMM_12_4			,
	JBIB_FMM_12_5			,
	JBIB_FMM_12_6			,
	JBIB_FMM_12_7			,
	JBIB_FMM_12_8			,
	JBIB_FMM_12_9			,
	JBIB_FMM_12_10			,
	JBIB_FMM_12_11			,
	JBIB_FMM_12_12			, // Moved to DLC
	JBIB_FMM_12_13			, // Moved to DLC
	JBIB_FMM_12_14			, // Moved to DLC
	JBIB_FMM_12_15			, // Moved to DLC
	JBIB_FMM_13_0			,
	JBIB_FMM_13_1			,
	JBIB_FMM_13_2			,
	JBIB_FMM_13_3			,
	JBIB_FMM_13_4			, // Moved to DLC
	JBIB_FMM_13_5			,
	JBIB_FMM_13_6			, // Moved to DLC
	JBIB_FMM_13_7			, // Moved to DLC
	JBIB_FMM_13_8			, // Moved to DLC
	JBIB_FMM_13_9			, // Moved to DLC
	JBIB_FMM_13_10			, // Moved to DLC
	JBIB_FMM_13_11			, // Moved to DLC
	JBIB_FMM_13_12			, // Moved to DLC
	JBIB_FMM_13_13			,
	JBIB_FMM_13_14			, // Moved to DLC
	JBIB_FMM_13_15			, // Moved to DLC
	JBIB_FMM_14_0			,
	JBIB_FMM_14_1			,
	JBIB_FMM_14_2			,
	JBIB_FMM_14_3			,
	JBIB_FMM_14_4			,
	JBIB_FMM_14_5			,
	JBIB_FMM_14_6			,
	JBIB_FMM_14_7			,
	JBIB_FMM_14_8			,
	JBIB_FMM_14_9			,
	JBIB_FMM_14_10			,
	JBIB_FMM_14_11			,
	JBIB_FMM_14_12			,
	JBIB_FMM_14_13			,
	JBIB_FMM_14_14			,
	JBIB_FMM_14_15			,
	JBIB_FMM_15_0			,
	JBIB_FMM_DLC			,
	
	// props need to be ordered by anchor and then drawable / variation
	// if the 1st prop of a type changes, update GET_FIRST_PROP_OF_TYPE
	// ANCHOR_HEAD, 
	PROPS_FMM_EARDEFENDERS_0_0		= 10, // Start props at 10.
	PROPS_FMM_EARDEFENDERS_0_1		,
	PROPS_FMM_EARDEFENDERS_0_2		,
	PROPS_FMM_EARDEFENDERS_0_3		,
	PROPS_FMM_EARDEFENDERS_0_4		,
	PROPS_FMM_EARDEFENDERS_0_5		,
	PROPS_FMM_EARDEFENDERS_0_6		,
	PROPS_FMM_EARDEFENDERS_0_7		,
	
	PROPS_FMM_DUNCE_HAT_1_0			,
	
	PROPS_FMM_HAT_2_0		,
	PROPS_FMM_HAT_2_1		,
	PROPS_FMM_HAT_2_2		,
	PROPS_FMM_HAT_2_3		,
	PROPS_FMM_HAT_2_4		,
	PROPS_FMM_HAT_2_5		,
	PROPS_FMM_HAT_2_6		,
	PROPS_FMM_HAT_2_7		,
	
	PROPS_FMM_HAT_3_0		, // moved to DLC
	PROPS_FMM_HAT_3_1		,
	PROPS_FMM_HAT_3_2		,
	PROPS_FMM_HAT_3_3		, // moved to DLC
	PROPS_FMM_HAT_3_4		, // moved to DLC
	PROPS_FMM_HAT_3_5		, // moved to DLC
	PROPS_FMM_HAT_3_6		, // moved to DLC
	PROPS_FMM_HAT_3_7		, // moved to DLC
	
	PROPS_FMM_HAT_4_0		,
	PROPS_FMM_HAT_4_1		,
	PROPS_FMM_HAT_4_2		, // moved to DLC
	PROPS_FMM_HAT_4_3		, // moved to DLC
	PROPS_FMM_HAT_4_4		, // moved to DLC
	PROPS_FMM_HAT_4_5		, // moved to DLC
	PROPS_FMM_HAT_4_6		, // moved to DLC
	PROPS_FMM_HAT_4_7		, // moved to DLC
	
	PROPS_FMM_HAT_5_0		,
	PROPS_FMM_HAT_5_1		,
	PROPS_FMM_HAT_5_2		, // moved to DLC
	PROPS_FMM_HAT_5_3		, // moved to DLC
	PROPS_FMM_HAT_5_4		, // moved to DLC
	PROPS_FMM_HAT_5_5		, // moved to DLC
	PROPS_FMM_HAT_5_6		, // moved to DLC
	PROPS_FMM_HAT_5_7		, // moved to DLC
	
	PROPS_FMM_HAT_6_0		,
	PROPS_FMM_HAT_6_1		,
	PROPS_FMM_HAT_6_2		,
	PROPS_FMM_HAT_6_3		,
	PROPS_FMM_HAT_6_4		,
	PROPS_FMM_HAT_6_5		,
	PROPS_FMM_HAT_6_6		,
	PROPS_FMM_HAT_6_7		,
	
	PROPS_FMM_HAT_7_0		,
	PROPS_FMM_HAT_7_1		,
	PROPS_FMM_HAT_7_2		,
	PROPS_FMM_HAT_7_3		,
	PROPS_FMM_HAT_7_4		,
	PROPS_FMM_HAT_7_5		,
	PROPS_FMM_HAT_7_6		,
	PROPS_FMM_HAT_7_7		,
	
	PROPS_FMM_HAT_8_0		, // moved to DLC
	PROPS_FMM_HAT_8_1		, // moved to DLC
	PROPS_FMM_HAT_8_2		, // moved to DLC
	PROPS_FMM_HAT_8_3		, // moved to DLC
	PROPS_FMM_HAT_8_4		, // moved to DLC
	PROPS_FMM_HAT_8_5		, // moved to DLC
	PROPS_FMM_HAT_8_6		, // moved to DLC
	PROPS_FMM_HAT_8_7		, // moved to DLC
	
	PROPS_FMM_HAT_9_0		, // moved to DLC
	PROPS_FMM_HAT_9_1		, // moved to DLC
	PROPS_FMM_HAT_9_2		, // moved to DLC
	PROPS_FMM_HAT_9_3		, // moved to DLC
	PROPS_FMM_HAT_9_4		, // moved to DLC
	PROPS_FMM_HAT_9_5		,
	PROPS_FMM_HAT_9_6		, // moved to DLC
	PROPS_FMM_HAT_9_7		,
	
	PROPS_FMM_HAT_10_0		, // moved to DLC
	PROPS_FMM_HAT_10_1		, // moved to DLC
	PROPS_FMM_HAT_10_2		, // moved to DLC
	PROPS_FMM_HAT_10_3		, // moved to DLC
	PROPS_FMM_HAT_10_4		, // moved to DLC
	PROPS_FMM_HAT_10_5		,
	PROPS_FMM_HAT_10_6		, // moved to DLC
	PROPS_FMM_HAT_10_7		,
	
	PROPS_FMM_HAT_11_0		, // moved to DLC
	PROPS_FMM_HAT_11_1		, // moved to DLC
	PROPS_FMM_HAT_11_2		, // moved to DLC
	PROPS_FMM_HAT_11_3		, // moved to DLC
	PROPS_FMM_HAT_11_4		, // moved to DLC
	PROPS_FMM_HAT_11_5		, // moved to DLC
	PROPS_FMM_HAT_11_6		, // moved to DLC
	PROPS_FMM_HAT_11_7		, // moved to DLC
	
	PROPS_FMM_HAT_12_0		,
	PROPS_FMM_HAT_12_1		,
	PROPS_FMM_HAT_12_2		,
	PROPS_FMM_HAT_12_3		, // moved to DLC
	PROPS_FMM_HAT_12_4		,
	PROPS_FMM_HAT_12_5		, // moved to DLC
	PROPS_FMM_HAT_12_6		,
	PROPS_FMM_HAT_12_7		,
	
	PROPS_FMM_HAT_13_0		,
	PROPS_FMM_HAT_13_1		,
	PROPS_FMM_HAT_13_2		,
	PROPS_FMM_HAT_13_3		,
	PROPS_FMM_HAT_13_4		,
	PROPS_FMM_HAT_13_5		,
	PROPS_FMM_HAT_13_6		,
	PROPS_FMM_HAT_13_7		,
	
	PROPS_FMM_HAT_14_0		,
	PROPS_FMM_HAT_14_1		,
	PROPS_FMM_HAT_14_2		,
	PROPS_FMM_HAT_14_3		,
	PROPS_FMM_HAT_14_4		,
	PROPS_FMM_HAT_14_5		,
	PROPS_FMM_HAT_14_6		,
	PROPS_FMM_HAT_14_7		,
	
	PROPS_FMM_HAT_15_0		,
	PROPS_FMM_HAT_15_1		,
	PROPS_FMM_HAT_15_2		,
	PROPS_FMM_HAT_15_3		,
	PROPS_FMM_HAT_15_4		,
	PROPS_FMM_HAT_15_5		,
	PROPS_FMM_HAT_15_6		,
	PROPS_FMM_HAT_15_7		,
	
	PROPS_FMM_HELMET_16_0	,
	PROPS_FMM_HELMET_16_1	,
	PROPS_FMM_HELMET_16_2	,
	PROPS_FMM_HELMET_16_3	,
	PROPS_FMM_HELMET_16_4	,
	PROPS_FMM_HELMET_16_5	,
	PROPS_FMM_HELMET_16_6	,
	PROPS_FMM_HELMET_16_7	,
					 
	PROPS_FMM_HELMET_17_0	,
	PROPS_FMM_HELMET_17_1	,
	PROPS_FMM_HELMET_17_2	,
	PROPS_FMM_HELMET_17_3	,
	PROPS_FMM_HELMET_17_4	,
	PROPS_FMM_HELMET_17_5	,
	PROPS_FMM_HELMET_17_6	,
	PROPS_FMM_HELMET_17_7	,
					 
	PROPS_FMM_HELMET_18_0	,
	PROPS_FMM_HELMET_18_1	,
	PROPS_FMM_HELMET_18_2	,
	PROPS_FMM_HELMET_18_3	,
	PROPS_FMM_HELMET_18_4	,
	PROPS_FMM_HELMET_18_5	,
	PROPS_FMM_HELMET_18_6	,
	PROPS_FMM_HELMET_18_7	,
	
	//ANCHOR_EYES, 
	PROPS_FMM_GLASSES_0_0		, // Moved to DLC
	PROPS_FMM_GLASSES_0_1		, // Moved to DLC
	PROPS_FMM_GLASSES_0_2		, // Moved to DLC
	PROPS_FMM_GLASSES_0_3		, // Moved to DLC
	PROPS_FMM_GLASSES_0_4		, // Moved to DLC
	PROPS_FMM_GLASSES_0_5		, // Moved to DLC
	PROPS_FMM_GLASSES_0_6		, // Moved to DLC
	PROPS_FMM_GLASSES_0_7		, // Moved to DLC
	PROPS_FMM_GLASSES_0_8		,
	PROPS_FMM_GLASSES_0_9		,
	PROPS_FMM_GLASSES_0_10		,
	
	PROPS_FMM_GLASSES_1_0		, // Moved to DLC
	PROPS_FMM_GLASSES_1_1		,
	PROPS_FMM_GLASSES_1_2		, // Moved to DLC
	PROPS_FMM_GLASSES_1_3		, // Moved to DLC
	PROPS_FMM_GLASSES_1_4		, // Moved to DLC
	PROPS_FMM_GLASSES_1_5		, // Moved to DLC
	PROPS_FMM_GLASSES_1_6		, // Moved to DLC
	PROPS_FMM_GLASSES_1_7		, // Moved to DLC
	
	PROPS_FMM_GLASSES_2_0		,
	PROPS_FMM_GLASSES_2_1		,
	PROPS_FMM_GLASSES_2_2		,
	PROPS_FMM_GLASSES_2_3		,
	PROPS_FMM_GLASSES_2_4		,
	PROPS_FMM_GLASSES_2_5		,
	PROPS_FMM_GLASSES_2_6		,
	PROPS_FMM_GLASSES_2_7		,
	PROPS_FMM_GLASSES_2_8		,
	PROPS_FMM_GLASSES_2_9		,
	PROPS_FMM_GLASSES_2_10		,
	
	PROPS_FMM_GLASSES_3_0		,
	PROPS_FMM_GLASSES_3_1		,
	PROPS_FMM_GLASSES_3_2		,
	PROPS_FMM_GLASSES_3_3		,
	PROPS_FMM_GLASSES_3_4		,
	PROPS_FMM_GLASSES_3_5		,
	PROPS_FMM_GLASSES_3_6		,
	PROPS_FMM_GLASSES_3_7		,
	PROPS_FMM_GLASSES_3_8		,
	PROPS_FMM_GLASSES_3_9		,
	PROPS_FMM_GLASSES_3_10		,
	
	PROPS_FMM_GLASSES_4_0		,
	PROPS_FMM_GLASSES_4_1		,
	PROPS_FMM_GLASSES_4_2		,
	PROPS_FMM_GLASSES_4_3		,
	PROPS_FMM_GLASSES_4_4		,
	PROPS_FMM_GLASSES_4_5		,
	PROPS_FMM_GLASSES_4_6		,
	PROPS_FMM_GLASSES_4_7		,
	PROPS_FMM_GLASSES_4_8		,
	PROPS_FMM_GLASSES_4_9		,
	PROPS_FMM_GLASSES_4_10		,
	
	PROPS_FMM_GLASSES_5_0		,
	PROPS_FMM_GLASSES_5_1		,
	PROPS_FMM_GLASSES_5_2		,
	PROPS_FMM_GLASSES_5_3		,
	PROPS_FMM_GLASSES_5_4		,
	PROPS_FMM_GLASSES_5_5		,
	PROPS_FMM_GLASSES_5_6		,
	PROPS_FMM_GLASSES_5_7		,
	PROPS_FMM_GLASSES_5_8		,
	PROPS_FMM_GLASSES_5_9		,
	PROPS_FMM_GLASSES_5_10		,
	
	PROPS_FMM_GLASSES_6_0		, // Moved to DLC
	PROPS_FMM_GLASSES_6_1		, // Moved to DLC
	PROPS_FMM_GLASSES_6_2		, // Moved to DLC
	PROPS_FMM_GLASSES_6_3		, // Moved to DLC
	PROPS_FMM_GLASSES_6_4		, // Moved to DLC
	PROPS_FMM_GLASSES_6_5		, // Moved to DLC
	PROPS_FMM_GLASSES_6_6		, // Moved to DLC
	PROPS_FMM_GLASSES_6_7		, // Moved to DLC
	
	PROPS_FMM_GLASSES_7_0		,
	PROPS_FMM_GLASSES_7_1		,
	PROPS_FMM_GLASSES_7_2		,
	PROPS_FMM_GLASSES_7_3		,
	PROPS_FMM_GLASSES_7_4		,
	PROPS_FMM_GLASSES_7_5		,
	PROPS_FMM_GLASSES_7_6		,
	PROPS_FMM_GLASSES_7_7		,
	PROPS_FMM_GLASSES_7_8		,
	PROPS_FMM_GLASSES_7_9		,
	PROPS_FMM_GLASSES_7_10		,
	
	PROPS_FMM_GLASSES_8_0		,
	PROPS_FMM_GLASSES_8_1		,
	PROPS_FMM_GLASSES_8_2		,
	PROPS_FMM_GLASSES_8_3		,
	PROPS_FMM_GLASSES_8_4		,
	PROPS_FMM_GLASSES_8_5		,
	PROPS_FMM_GLASSES_8_6		,
	PROPS_FMM_GLASSES_8_7		,
	PROPS_FMM_GLASSES_8_8		,
	PROPS_FMM_GLASSES_8_9		,
	PROPS_FMM_GLASSES_8_10		,
	
	PROPS_FMM_GLASSES_9_0		,
	PROPS_FMM_GLASSES_9_1		,
	PROPS_FMM_GLASSES_9_2		,
	PROPS_FMM_GLASSES_9_3		,
	PROPS_FMM_GLASSES_9_4		,
	PROPS_FMM_GLASSES_9_5		,
	PROPS_FMM_GLASSES_9_6		,
	PROPS_FMM_GLASSES_9_7		,
	PROPS_FMM_GLASSES_9_8		,
	PROPS_FMM_GLASSES_9_9		,
	PROPS_FMM_GLASSES_9_10		,
	
	PROPS_FMM_GLASSES_10_0		,
	PROPS_FMM_GLASSES_10_1		,
	PROPS_FMM_GLASSES_10_2		,
	PROPS_FMM_GLASSES_10_3		,
	PROPS_FMM_GLASSES_10_4		,
	PROPS_FMM_GLASSES_10_5		,
	PROPS_FMM_GLASSES_10_6		,
	PROPS_FMM_GLASSES_10_7		,
	PROPS_FMM_GLASSES_10_8		,
	PROPS_FMM_GLASSES_10_9		,
	PROPS_FMM_GLASSES_10_10		,
	
	PROPS_FMM_GLASSES_11_0		, // Moved to DLC
	PROPS_FMM_GLASSES_11_1		, // Moved to DLC
	PROPS_FMM_GLASSES_11_2		, // Moved to DLC
	PROPS_FMM_GLASSES_11_3		, // Moved to DLC
	PROPS_FMM_GLASSES_11_4		, // Moved to DLC
	PROPS_FMM_GLASSES_11_5		, // Moved to DLC
	PROPS_FMM_GLASSES_11_6		, // Moved to DLC
	PROPS_FMM_GLASSES_11_7		, // Moved to DLC
	
	PROPS_FMM_GLASSES_12_0		,
	PROPS_FMM_GLASSES_12_1		,
	PROPS_FMM_GLASSES_12_2		,
	PROPS_FMM_GLASSES_12_3		,
	PROPS_FMM_GLASSES_12_4		,
	PROPS_FMM_GLASSES_12_5		,
	PROPS_FMM_GLASSES_12_6		,
	PROPS_FMM_GLASSES_12_7		,
	PROPS_FMM_GLASSES_12_8		,
	PROPS_FMM_GLASSES_12_9		,
	PROPS_FMM_GLASSES_12_10		,
	
	PROPS_FMM_GLASSES_13_0		,
	PROPS_FMM_GLASSES_13_1		,
	PROPS_FMM_GLASSES_13_2		,
	PROPS_FMM_GLASSES_13_3		,
	PROPS_FMM_GLASSES_13_4		,
	PROPS_FMM_GLASSES_13_5		,
	PROPS_FMM_GLASSES_13_6		,
	PROPS_FMM_GLASSES_13_7		,
	PROPS_FMM_GLASSES_13_8		,
	PROPS_FMM_GLASSES_13_9		,
	PROPS_FMM_GLASSES_13_10		,
	
	PROPS_FMM_GLASSES_14_0		, // Moved to DLC
	PROPS_FMM_GLASSES_14_1		, // Moved to DLC
	PROPS_FMM_GLASSES_14_2		, // Moved to DLC
	PROPS_FMM_GLASSES_14_3		, // Moved to DLC
	PROPS_FMM_GLASSES_14_4		, // Moved to DLC
	PROPS_FMM_GLASSES_14_5		, // Moved to DLC
	PROPS_FMM_GLASSES_14_6		, // Moved to DLC
	PROPS_FMM_GLASSES_14_7		, // Moved to DLC
	
	PROPS_FMM_GLASSES_15_0		,
	PROPS_FMM_GLASSES_15_1		,
	PROPS_FMM_GLASSES_15_2		,
	PROPS_FMM_GLASSES_15_3		,
	PROPS_FMM_GLASSES_15_4		,
	PROPS_FMM_GLASSES_15_5		,
	PROPS_FMM_GLASSES_15_6		,
	PROPS_FMM_GLASSES_15_7		,
	PROPS_FMM_GLASSES_15_8		,
	PROPS_FMM_GLASSES_15_9		,
	PROPS_FMM_GLASSES_15_10		,
	
	//ANCHOR_EARS
    //ANCHOR_MOUTH,
   	//ANCHOR_LEFT_HAND,
    //ANCHOR_RIGHT_HAND,
	
   	//ANCHOR_LEFT_WRIST,
	PROPS_FMM_WATCH_0_0		,
	PROPS_FMM_WATCH_0_1		,  // Moved to DLC
	PROPS_FMM_WATCH_0_2		, // Moved to DLC
	PROPS_FMM_WATCH_0_3		, // Moved to DLC
	PROPS_FMM_WATCH_0_4		, // Moved to DLC
	
	PROPS_FMM_WATCH_1_0		,
	PROPS_FMM_WATCH_1_1		, // Moved to DLC
	PROPS_FMM_WATCH_1_2		, // Moved to DLC
	
	PROPS_FMM_DLC			,
		
	//ANCHOR_RIGHT_WRIST,
    //ANCHOR_HIP
	
	HEAD_FMM_DLC			=0,
	
	HAND_FMM_0_0			=0,
	HAND_FMM_1_0			,
	HAND_FMM_2_0			,
	HAND_FMM_3_0			,
	HAND_FMM_4_0			,
	HAND_FMM_5_0			,
	HAND_FMM_6_0			,
	HAND_FMM_7_0			,
	HAND_FMM_8_0			,
	HAND_FMM_DLC			,
	
	OUTFIT_FMM_DEFAULT		= 0,
	OUTFIT_FMM_SUB_1		, // suburban outfits
	OUTFIT_FMM_SUB_2		,
	OUTFIT_FMM_SUB_3		,
	OUTFIT_FMM_SUB_4		,
	OUTFIT_FMM_SUB_5		,
	OUTFIT_FMM_SUB_6		,
	OUTFIT_FMM_SUB_7		,
	OUTFIT_FMM_SUB_8		,
	OUTFIT_FMM_SUB_9		,
	OUTFIT_FMM_PONS_1		,// ponsonbys outfits
	OUTFIT_FMM_PONS_2		,
	OUTFIT_FMM_PONS_3		,
	OUTFIT_FMM_PONS_4		,
	OUTFIT_FMM_PONS_5		,
	OUTFIT_FMM_PONS_6		,
	OUTFIT_FMM_PONS_7		,
	OUTFIT_FMM_PONS_8		,
	OUTFIT_FMM_BIN_1		,// binco outfits
	OUTFIT_FMM_BIN_2		,
	OUTFIT_FMM_BIN_3		,
	OUTFIT_FMM_BIN_4		,
	OUTFIT_FMM_BIN_5		,
	OUTFIT_FMM_BIN_6		,
	OUTFIT_FMM_BIN_7		,
	OUTFIT_FMM_BIN_8		,
	OUTFIT_FMM_DLC,
	
	PROPGROUP_FMM_SUB_1		=0,
	PROPGROUP_FMM_SUB_2		,
	PROPGROUP_FMM_SUB_3		,
	PROPGROUP_FMM_SUB_4		,
	PROPGROUP_FMM_SUB_5		,
	PROPGROUP_FMM_SUB_6		,
	PROPGROUP_FMM_SUB_7		,
	PROPGROUP_FMM_SUB_8		,
	PROPGROUP_FMM_SUB_9		,
	PROPGROUP_FMM_PONS_1	,
	PROPGROUP_FMM_PONS_2	,
	PROPGROUP_FMM_PONS_3	,
	PROPGROUP_FMM_PONS_4	,
	PROPGROUP_FMM_PONS_5	,
	PROPGROUP_FMM_PONS_6	,
	PROPGROUP_FMM_PONS_7	,
	PROPGROUP_FMM_PONS_8	,
	PROPGROUP_FMM_BIN_1		,
	PROPGROUP_FMM_BIN_2		,
	PROPGROUP_FMM_BIN_3		,
	PROPGROUP_FMM_BIN_4		,
	PROPGROUP_FMM_BIN_5		,
	PROPGROUP_FMM_BIN_6		,
	PROPGROUP_FMM_BIN_7		,
	PROPGROUP_FMM_BIN_8		,
	#IF USE_TU_CHANGES
	PROPGROUP_FMM_DLC		,
	#ENDIF
	
	////////////////////////////////////////////////////////
	/// FM_F - MP_F_FREEMODE_01
	HAIR_FMF_0_0			=0,
	HAIR_FMF_1_0			,
	HAIR_FMF_1_1			,
	HAIR_FMF_1_2			,
	HAIR_FMF_1_3			,
	HAIR_FMF_1_4			,
	HAIR_FMF_1_5			,
	HAIR_FMF_2_0			,
	HAIR_FMF_2_1			,
	HAIR_FMF_2_2			,
	HAIR_FMF_2_3			,
	HAIR_FMF_2_4			,
	HAIR_FMF_2_5			,
	HAIR_FMF_3_0			,
	HAIR_FMF_3_1			,
	HAIR_FMF_3_2			,
	HAIR_FMF_3_3			,
	HAIR_FMF_3_4			,
	HAIR_FMF_4_0			,
	HAIR_FMF_4_1			,
	HAIR_FMF_4_2			,
	HAIR_FMF_4_3			,
	HAIR_FMF_4_4			,
	HAIR_FMF_4_5			,
	HAIR_FMF_5_0			,
	HAIR_FMF_5_1			,
	HAIR_FMF_5_2			,
	HAIR_FMF_5_3			,
	HAIR_FMF_5_4			,
	HAIR_FMF_5_5			,
	HAIR_FMF_6_0			,
	HAIR_FMF_6_1			,
	HAIR_FMF_6_2			,
	HAIR_FMF_6_3			,
	HAIR_FMF_6_4			,
	HAIR_FMF_7_0			,
	HAIR_FMF_7_1			,
	HAIR_FMF_7_2			,
	HAIR_FMF_7_3			,
	HAIR_FMF_7_4			,
	HAIR_FMF_7_5			,
	HAIR_FMF_8_0			,
	HAIR_FMF_8_1			,
	HAIR_FMF_8_2			,
	HAIR_FMF_8_3			,
	HAIR_FMF_8_4			,
	HAIR_FMF_8_5			,
	HAIR_FMF_9_0			,
	HAIR_FMF_9_1			,
	HAIR_FMF_9_2			,
	HAIR_FMF_9_3			,
	HAIR_FMF_9_4			,
	HAIR_FMF_9_5			,
	HAIR_FMF_10_0			,
	HAIR_FMF_10_1			,
	HAIR_FMF_10_2			,
	HAIR_FMF_10_3			,
	HAIR_FMF_10_4			,
	HAIR_FMF_10_5			,
	HAIR_FMF_10_6			,
	HAIR_FMF_11_0			,
	HAIR_FMF_11_1			,
	HAIR_FMF_11_2			,
	HAIR_FMF_11_3			,
	HAIR_FMF_11_4			,
	HAIR_FMF_11_5			,
	HAIR_FMF_11_6			,
	HAIR_FMF_12_0			,
	HAIR_FMF_12_1			,
	HAIR_FMF_12_2			,
	HAIR_FMF_12_3			,
	HAIR_FMF_12_4			,
	HAIR_FMF_12_5			,
	HAIR_FMF_13_0			,
	HAIR_FMF_13_1			,
	HAIR_FMF_13_2			,
	HAIR_FMF_13_3			,
	HAIR_FMF_13_4			,
	HAIR_FMF_13_5			,
	HAIR_FMF_14_0			,
	HAIR_FMF_14_1			,
	HAIR_FMF_14_2			,
	HAIR_FMF_14_3			,
	HAIR_FMF_14_4			,
	HAIR_FMF_14_5			,
	HAIR_FMF_15_0			,
	HAIR_FMF_15_1			,
	HAIR_FMF_15_2			,
	HAIR_FMF_15_3			,
	HAIR_FMF_15_4			,
	HAIR_FMF_15_5			,
	HAIR_FMF_15_6			,
	HAIR_FMF_DLC			,
	
	// MAIN UPPER
	JBIB_FMF_0_0			=0,
	JBIB_FMF_0_1			,
	JBIB_FMF_0_2			,
	JBIB_FMF_0_3			,
	JBIB_FMF_0_4			,
	JBIB_FMF_0_5			,
	JBIB_FMF_0_6			,
	JBIB_FMF_0_7			,
	JBIB_FMF_0_8			,
	JBIB_FMF_0_9			,
	JBIB_FMF_0_10			,
	JBIB_FMF_0_11			,
	JBIB_FMF_0_12			,
	JBIB_FMF_0_13			,
	JBIB_FMF_0_14			,
	JBIB_FMF_0_15			,
	JBIB_FMF_1_0			,
	JBIB_FMF_1_1			,
	JBIB_FMF_1_2			,
	JBIB_FMF_1_3			, // moved to DLC
	JBIB_FMF_1_4			,
	JBIB_FMF_1_5			,
	JBIB_FMF_1_6			,
	JBIB_FMF_1_7			, // moved to DLC
	JBIB_FMF_1_8			, // moved to DLC
	JBIB_FMF_1_9			,
	JBIB_FMF_1_10			, // moved to DLC
	JBIB_FMF_1_11			,
	JBIB_FMF_1_12			, // moved to DLC
	JBIB_FMF_1_13			, // moved to DLC
	JBIB_FMF_1_14			,
	JBIB_FMF_1_15			, // moved to DLC
	JBIB_FMF_2_0			,
	JBIB_FMF_2_1			,
	JBIB_FMF_2_2			,
	JBIB_FMF_2_3			,
	JBIB_FMF_2_4			,
	JBIB_FMF_2_5			,
	JBIB_FMF_2_6			,
	JBIB_FMF_2_7			,
	JBIB_FMF_2_8			,
	JBIB_FMF_2_9			,
	JBIB_FMF_2_10			,
	JBIB_FMF_2_11			,
	JBIB_FMF_2_12			,
	JBIB_FMF_2_13			,
	JBIB_FMF_2_14			,
	JBIB_FMF_2_15			,
	JBIB_FMF_3_0			,
	JBIB_FMF_3_1			,
	JBIB_FMF_3_2			,
	JBIB_FMF_3_3			,
	JBIB_FMF_3_4			,
	JBIB_FMF_3_5			, // moved to DLC
	JBIB_FMF_3_6			, // moved to DLC
	JBIB_FMF_3_7			, // moved to DLC
	JBIB_FMF_3_8			, // moved to DLC
	JBIB_FMF_3_9			, // moved to DLC
	JBIB_FMF_3_10			,
	JBIB_FMF_3_11			,
	JBIB_FMF_3_12			,
	JBIB_FMF_3_13			,
	JBIB_FMF_3_14			,
	JBIB_FMF_3_15			, // moved to DLC
	JBIB_FMF_4_0			, // moved to DLC
	JBIB_FMF_4_1			, // moved to DLC
	JBIB_FMF_4_2			, // moved to DLC
	JBIB_FMF_4_3			, // moved to DLC
	JBIB_FMF_4_4			, // moved to DLC
	JBIB_FMF_4_5			, // moved to DLC
	JBIB_FMF_4_6			, // moved to DLC
	JBIB_FMF_4_7			, // moved to DLC
	JBIB_FMF_4_8			, // moved to DLC
	JBIB_FMF_4_9			, // moved to DLC
	JBIB_FMF_4_10			, // moved to DLC
	JBIB_FMF_4_11			, // moved to DLC
	JBIB_FMF_4_12			, // moved to DLC
	JBIB_FMF_4_13			,
	JBIB_FMF_4_14			,
	JBIB_FMF_4_15			, // moved to DLC
	JBIB_FMF_5_0			,
	JBIB_FMF_5_1			,
	JBIB_FMF_5_2			, // moved to DLC
	JBIB_FMF_5_3			, // moved to DLC
	JBIB_FMF_5_4			, // moved to DLC
	JBIB_FMF_5_5			, // moved to DLC
	JBIB_FMF_5_6			, // moved to DLC
	JBIB_FMF_5_7			,
	JBIB_FMF_5_8			, // moved to DLC
	JBIB_FMF_5_9			,
	JBIB_FMF_5_10			, // moved to DLC
	JBIB_FMF_5_11			, // moved to DLC
	JBIB_FMF_5_12			, // moved to DLC
	JBIB_FMF_5_13			, // moved to DLC
	JBIB_FMF_5_14			, // moved to DLC
	JBIB_FMF_5_15			, // moved to DLC
	JBIB_FMF_6_0			,
	JBIB_FMF_6_1			,
	JBIB_FMF_6_2			,
	JBIB_FMF_6_3			, // moved to DLC
	JBIB_FMF_6_4			,
	JBIB_FMF_6_5			, // moved to DLC
	JBIB_FMF_6_6			, // moved to DLC
	JBIB_FMF_6_7			, // moved to DLC
	JBIB_FMF_6_8			, // moved to DLC
	JBIB_FMF_6_9			, // moved to DLC
	JBIB_FMF_6_10			, // moved to DLC
	JBIB_FMF_6_11			, // moved to DLC
	JBIB_FMF_6_12			, // moved to DLC
	JBIB_FMF_6_13			, // moved to DLC
	JBIB_FMF_6_14			, // moved to DLC
	JBIB_FMF_6_15			, // moved to DLC
	JBIB_FMF_7_0			,
	JBIB_FMF_7_1			,
	JBIB_FMF_7_2			,
	JBIB_FMF_7_3			, // moved to DLC
	JBIB_FMF_7_4			, // moved to DLC
	JBIB_FMF_7_5			, // moved to DLC
	JBIB_FMF_7_6			, // moved to DLC
	JBIB_FMF_7_7			, // moved to DLC
	JBIB_FMF_7_8			,
	JBIB_FMF_7_9			, // moved to DLC
	JBIB_FMF_7_10			, // moved to DLC
	JBIB_FMF_7_11			, // moved to DLC
	JBIB_FMF_7_12			, // moved to DLC
	JBIB_FMF_7_13			, // moved to DLC
	JBIB_FMF_7_14			, // moved to DLC
	JBIB_FMF_7_15			, // moved to DLC
	JBIB_FMF_8_0			,
	JBIB_FMF_8_1			,
	JBIB_FMF_8_2			,
	JBIB_FMF_8_3			, // moved to DLC
	JBIB_FMF_8_4			, // moved to DLC
	JBIB_FMF_8_5			, // moved to DLC
	JBIB_FMF_8_6			, // moved to DLC
	JBIB_FMF_8_7			, // moved to DLC
	JBIB_FMF_8_8			, // moved to DLC
	JBIB_FMF_8_9			, // moved to DLC
	JBIB_FMF_8_10			, // moved to DLC
	JBIB_FMF_8_11			, // moved to DLC
	JBIB_FMF_8_12			,
	JBIB_FMF_8_13			, // moved to DLC
	JBIB_FMF_8_14			, // moved to DLC
	JBIB_FMF_8_15			, // moved to DLC
	JBIB_FMF_9_0			,
	JBIB_FMF_9_1			,
	JBIB_FMF_9_2			,
	JBIB_FMF_9_3			,
	JBIB_FMF_9_4			,
	JBIB_FMF_9_5			,
	JBIB_FMF_9_6			,
	JBIB_FMF_9_7			,
	JBIB_FMF_9_8			,
	JBIB_FMF_9_9			,
	JBIB_FMF_9_10			,
	JBIB_FMF_9_11			,
	JBIB_FMF_9_12			,
	JBIB_FMF_9_13			,
	JBIB_FMF_9_14			,
	JBIB_FMF_9_15			, // moved to DLC
	JBIB_FMF_10_0			,
	JBIB_FMF_10_1			,
	JBIB_FMF_10_2			,
	JBIB_FMF_10_3			, // moved to DLC
	JBIB_FMF_10_4			, // moved to DLC
	JBIB_FMF_10_5			, // moved to DLC
	JBIB_FMF_10_6			, // moved to DLC
	JBIB_FMF_10_7			,
	JBIB_FMF_10_8			, // moved to DLC
	JBIB_FMF_10_9			, // moved to DLC
	JBIB_FMF_10_10			,
	JBIB_FMF_10_11			,
	JBIB_FMF_10_12			, // moved to DLC
	JBIB_FMF_10_13			,
	JBIB_FMF_10_14			, // moved to DLC
	JBIB_FMF_10_15			,
	JBIB_FMF_11_0			,
	JBIB_FMF_11_1			,
	JBIB_FMF_11_2			,
	JBIB_FMF_11_3			, // moved to DLC
	JBIB_FMF_11_4			, // moved to DLC
	JBIB_FMF_11_5			, // moved to DLC
	JBIB_FMF_11_6			, // moved to DLC
	JBIB_FMF_11_7			, // moved to DLC
	JBIB_FMF_11_8			, // moved to DLC
	JBIB_FMF_11_9			, // moved to DLC
	JBIB_FMF_11_10			,
	JBIB_FMF_11_11			,
	JBIB_FMF_11_12			, // moved to DLC
	JBIB_FMF_11_13			, // moved to DLC
	JBIB_FMF_11_14			, // moved to DLC
	JBIB_FMF_11_15			,
	JBIB_FMF_12_0			, // moved to DLC
	JBIB_FMF_12_1			, // moved to DLC
	JBIB_FMF_12_2			, // moved to DLC
	JBIB_FMF_12_3			, // moved to DLC
	JBIB_FMF_12_4			, // moved to DLC
	JBIB_FMF_12_5			, // moved to DLC
	JBIB_FMF_12_6			, // moved to DLC
	JBIB_FMF_12_7			,
	JBIB_FMF_12_8			,
	JBIB_FMF_12_9			,
	JBIB_FMF_12_10			, // moved to DLC
	JBIB_FMF_12_11			, // moved to DLC
	JBIB_FMF_12_12			, // moved to DLC
	JBIB_FMF_12_13			, // moved to DLC
	JBIB_FMF_12_14			, // moved to DLC
	JBIB_FMF_12_15			, // moved to DLC
	JBIB_FMF_13_0			,
	JBIB_FMF_13_1			,
	JBIB_FMF_13_2			,
	JBIB_FMF_13_3			,
	JBIB_FMF_13_4			,
	JBIB_FMF_13_5			,
	JBIB_FMF_13_6			,
	JBIB_FMF_13_7			,
	JBIB_FMF_13_8			,
	JBIB_FMF_13_9			,
	JBIB_FMF_13_10			,
	JBIB_FMF_13_11			,
	JBIB_FMF_13_12			,
	JBIB_FMF_13_13			,
	JBIB_FMF_13_14			,
	JBIB_FMF_13_15			,
	JBIB_FMF_14_0			,
	JBIB_FMF_14_1			,
	JBIB_FMF_14_2			,
	JBIB_FMF_14_3			,
	JBIB_FMF_14_4			,
	JBIB_FMF_14_5			,
	JBIB_FMF_14_6			,
	JBIB_FMF_14_7			,
	JBIB_FMF_14_8			,
	JBIB_FMF_14_9			,
	JBIB_FMF_14_10			,
	JBIB_FMF_14_11			,
	JBIB_FMF_14_12			,
	JBIB_FMF_14_13			,
	JBIB_FMF_14_14			,
	JBIB_FMF_14_15			,
	JBIB_FMF_15_0			,
	JBIB_FMF_15_1			, // moved to DLC
	JBIB_FMF_15_2			, // moved to DLC
	JBIB_FMF_15_3			,
	JBIB_FMF_15_4			, // moved to DLC
	JBIB_FMF_15_5			, // moved to DLC
	JBIB_FMF_15_6			, // moved to DLC
	JBIB_FMF_15_7			, // moved to DLC
	JBIB_FMF_15_8			, // moved to DLC
	JBIB_FMF_15_9			, // moved to DLC
	JBIB_FMF_15_10			,
	JBIB_FMF_15_11			,
	JBIB_FMF_15_12			, // moved to DLC
	JBIB_FMF_15_13			, // moved to DLC
	JBIB_FMF_15_14			, // moved to DLC
	JBIB_FMF_15_15			, // moved to DLC
	JBIB_FMF_DLC			,
	
	// ARMS
	TORSO_FMF_0_0			=0,
	TORSO_FMF_1_0			,
	TORSO_FMF_2_0			,
	TORSO_FMF_3_0			,
	TORSO_FMF_4_0			,
	TORSO_FMF_5_0			,
	TORSO_FMF_6_0			,
	TORSO_FMF_7_0			,
	TORSO_FMF_8_0			,
	TORSO_FMF_9_0			,
	TORSO_FMF_10_0			,
	TORSO_FMF_11_0			,
	TORSO_FMF_12_0			,
	TORSO_FMF_13_0			,
	TORSO_FMF_14_0			,
	TORSO_FMF_15_0			,
	TORSO_FMF_DLC			,

	// SECONDARY UPPER
	SPECIAL_FMF_0_0			=0,
	SPECIAL_FMF_0_1			,
	SPECIAL_FMF_0_2			,
	SPECIAL_FMF_0_3			,
	SPECIAL_FMF_0_4			,
	SPECIAL_FMF_0_5			,
	SPECIAL_FMF_0_6			,
	SPECIAL_FMF_0_7			,
	SPECIAL_FMF_0_8			,
	SPECIAL_FMF_0_9			,
	SPECIAL_FMF_0_10		,
	SPECIAL_FMF_0_11		,
	SPECIAL_FMF_0_12		,
	SPECIAL_FMF_0_13		,
	SPECIAL_FMF_0_14		,
	SPECIAL_FMF_0_15		,
	SPECIAL_FMF_1_0			,
	SPECIAL_FMF_1_1			,
	SPECIAL_FMF_1_2			,
	SPECIAL_FMF_1_3			,
	SPECIAL_FMF_1_4			,
	SPECIAL_FMF_1_5			,
	SPECIAL_FMF_1_6			,
	SPECIAL_FMF_1_7			,
	SPECIAL_FMF_1_8			,
	SPECIAL_FMF_1_9			,
	SPECIAL_FMF_1_10		,
	SPECIAL_FMF_1_11		,
	SPECIAL_FMF_1_12		,
	SPECIAL_FMF_1_13		,
	SPECIAL_FMF_1_14		,
	SPECIAL_FMF_1_15		,
	SPECIAL_FMF_2_0			,
	SPECIAL_FMF_3_0			,
	SPECIAL_FMF_4_0			, // moved to DLC
	SPECIAL_FMF_4_1			, // moved to DLC
	SPECIAL_FMF_4_2			, // moved to DLC
	SPECIAL_FMF_4_3			, // moved to DLC
	SPECIAL_FMF_4_4			, // moved to DLC
	SPECIAL_FMF_4_5			, // moved to DLC
	SPECIAL_FMF_4_6			, // moved to DLC
	SPECIAL_FMF_4_7			, // moved to DLC
	SPECIAL_FMF_4_8			, // moved to DLC
	SPECIAL_FMF_4_9			, // moved to DLC
	SPECIAL_FMF_4_10		, // moved to DLC
	SPECIAL_FMF_4_11		, // moved to DLC
	SPECIAL_FMF_4_12		, // moved to DLC
	SPECIAL_FMF_4_13		,
	SPECIAL_FMF_4_14		,
	SPECIAL_FMF_4_15		, // moved to DLC
	SPECIAL_FMF_5_0			,
	SPECIAL_FMF_5_1			,
	SPECIAL_FMF_5_2			, // moved to DLC
	SPECIAL_FMF_5_3			, // moved to DLC
	SPECIAL_FMF_5_4			, // moved to DLC
	SPECIAL_FMF_5_5			, // moved to DLC
	SPECIAL_FMF_5_6			, // moved to DLC
	SPECIAL_FMF_5_7			,
	SPECIAL_FMF_5_8			, // moved to DLC
	SPECIAL_FMF_5_9			,
	SPECIAL_FMF_5_10		, // moved to DLC
	SPECIAL_FMF_5_11		, // moved to DLC
	SPECIAL_FMF_5_12		, // moved to DLC
	SPECIAL_FMF_5_13		, // moved to DLC
	SPECIAL_FMF_5_14		, // moved to DLC
	SPECIAL_FMF_5_15		, // moved to DLC
	SPECIAL_FMF_6_0			,
	SPECIAL_FMF_7_0			,
	SPECIAL_FMF_8_0			,
	SPECIAL_FMF_9_0			,
	SPECIAL_FMF_10_0		,
	SPECIAL_FMF_11_0		,
	SPECIAL_FMF_11_1		,
	SPECIAL_FMF_11_2		,
	SPECIAL_FMF_11_3		, // moved to DLC
	SPECIAL_FMF_11_4		, // moved to DLC
	SPECIAL_FMF_11_5		, // moved to DLC
	SPECIAL_FMF_11_6		, // moved to DLC
	SPECIAL_FMF_11_7		, // moved to DLC
	SPECIAL_FMF_11_8		, // moved to DLC
	SPECIAL_FMF_11_9		, // moved to DLC
	SPECIAL_FMF_11_10		,
	SPECIAL_FMF_11_11		,
	SPECIAL_FMF_11_12		, // moved to DLC
	SPECIAL_FMF_11_13		, // moved to DLC
	SPECIAL_FMF_11_14		, // moved to DLC
	SPECIAL_FMF_11_15		,
	SPECIAL_FMF_12_0		, // moved to DLC
	SPECIAL_FMF_12_1		, // moved to DLC
	SPECIAL_FMF_12_2		, // moved to DLC
	SPECIAL_FMF_12_3		, // moved to DLC
	SPECIAL_FMF_12_4		, // moved to DLC
	SPECIAL_FMF_12_5		, // moved to DLC
	SPECIAL_FMF_12_6		, // moved to DLC
	SPECIAL_FMF_12_7		,
	SPECIAL_FMF_12_8		,
	SPECIAL_FMF_12_9		,
	SPECIAL_FMF_12_10		, // moved to DLC
	SPECIAL_FMF_12_11		, // moved to DLC
	SPECIAL_FMF_12_12		, // moved to DLC
	SPECIAL_FMF_12_13		, // moved to DLC
	SPECIAL_FMF_12_14		, // moved to DLC
	SPECIAL_FMF_12_15		, // moved to DLC
	SPECIAL_FMF_13_0		,
	SPECIAL_FMF_13_1		,
	SPECIAL_FMF_13_2		,
	SPECIAL_FMF_13_3		,
	SPECIAL_FMF_13_4		,
	SPECIAL_FMF_13_5		,
	SPECIAL_FMF_13_6		,
	SPECIAL_FMF_13_7		,
	SPECIAL_FMF_13_8		,
	SPECIAL_FMF_13_9		,
	SPECIAL_FMF_13_10		,
	SPECIAL_FMF_13_11		,
	SPECIAL_FMF_13_12		,
	SPECIAL_FMF_13_13		,
	SPECIAL_FMF_13_14		,
	SPECIAL_FMF_13_15		,
	SPECIAL_FMF_14_0		,
	SPECIAL_FMF_15_0		,
	SPECIAL_FMF_15_1		, // moved to DLC
	SPECIAL_FMF_15_2		, // moved to DLC
	SPECIAL_FMF_15_3		,
	SPECIAL_FMF_15_4		, // moved to DLC
	SPECIAL_FMF_15_5		, // moved to DLC
	SPECIAL_FMF_15_6		, // moved to DLC
	SPECIAL_FMF_15_7		, // moved to DLC
	SPECIAL_FMF_15_8		, // moved to DLC
	SPECIAL_FMF_15_9		, // moved to DLC
	SPECIAL_FMF_15_10		,
	SPECIAL_FMF_15_11		,
	SPECIAL_FMF_15_12		, // moved to DLC
	SPECIAL_FMF_15_13		, // moved to DLC
	SPECIAL_FMF_15_14		, // moved to DLC
	SPECIAL_FMF_15_15		, // moved to DLC
	SPECIAL_FMF_DLC			,
	
	SPECIAL2_FMF_0_0			=0,
	SPECIAL2_FMF_1_0			,
	SPECIAL2_FMF_1_1			,
	SPECIAL2_FMF_1_2			,
	SPECIAL2_FMF_1_3			,
	SPECIAL2_FMF_1_4			,
	SPECIAL2_FMF_2_0			,
	SPECIAL2_FMF_2_1			,
	SPECIAL2_FMF_2_2			,
	SPECIAL2_FMF_2_3			,
	SPECIAL2_FMF_2_4			,
	SPECIAL2_FMF_3_0			,
	SPECIAL2_FMF_3_1			,
	SPECIAL2_FMF_3_2			,
	SPECIAL2_FMF_3_3			,
	SPECIAL2_FMF_3_4			,
	SPECIAL2_FMF_4_0			,
	SPECIAL2_FMF_4_1			,
	SPECIAL2_FMF_4_2			,
	SPECIAL2_FMF_4_3			,
	SPECIAL2_FMF_4_4			,
	SPECIAL2_FMF_5_0			,
	SPECIAL2_FMF_5_1			,
	SPECIAL2_FMF_5_2			,
	SPECIAL2_FMF_5_3			,
	SPECIAL2_FMF_5_4			,
	SPECIAL2_FMF_6_0			,
	SPECIAL2_FMF_6_1			,
	SPECIAL2_FMF_6_2			,
	SPECIAL2_FMF_6_3			,
	SPECIAL2_FMF_6_4			,
	SPECIAL2_FMF_7_0			,
	SPECIAL2_FMF_7_1			,
	SPECIAL2_FMF_7_2			,
	SPECIAL2_FMF_7_3			,
	SPECIAL2_FMF_7_4			,
	SPECIAL2_FMF_DLC			,
	
	DECL_FMF_0_0				=0,
	DECL_FMF_1_0				,
	DECL_FMF_2_0				,
	DECL_FMF_3_0				,
	DECL_FMF_4_0				,
	DECL_FMF_5_0				,
	DECL_FMF_DLC				,

	LEGS_FMF_0_0			=0,
	LEGS_FMF_0_1			,
	LEGS_FMF_0_2			,
	LEGS_FMF_0_3			,
	LEGS_FMF_0_4			,
	LEGS_FMF_0_5			,
	LEGS_FMF_0_6			,
	LEGS_FMF_0_7			,
	LEGS_FMF_0_8			,
	LEGS_FMF_0_9			,
	LEGS_FMF_0_10			,
	LEGS_FMF_0_11			,
	LEGS_FMF_0_12			,
	LEGS_FMF_0_13			,
	LEGS_FMF_0_14			,
	LEGS_FMF_0_15			,
	LEGS_FMF_1_0			,
	LEGS_FMF_1_1			,
	LEGS_FMF_1_2			,
	LEGS_FMF_1_3			,
	LEGS_FMF_1_4			,
	LEGS_FMF_1_5			,
	LEGS_FMF_1_6			,
	LEGS_FMF_1_7			,
	LEGS_FMF_1_8			,
	LEGS_FMF_1_9			,
	LEGS_FMF_1_10			,
	LEGS_FMF_1_11			,
	LEGS_FMF_1_12			,
	LEGS_FMF_1_13			,
	LEGS_FMF_1_14			,
	LEGS_FMF_1_15			,
	LEGS_FMF_2_0			,
	LEGS_FMF_2_1			,
	LEGS_FMF_2_2			,
	LEGS_FMF_2_3			, // moved to DLC
	LEGS_FMF_2_4			, // moved to DLC
	LEGS_FMF_2_5			, // moved to DLC
	LEGS_FMF_2_6			, // moved to DLC
	LEGS_FMF_2_7			, // moved to DLC
	LEGS_FMF_2_8			, // moved to DLC
	LEGS_FMF_2_9			, // moved to DLC
	LEGS_FMF_2_10			, // moved to DLC
	LEGS_FMF_2_11			, // moved to DLC
	LEGS_FMF_2_12			, // moved to DLC
	LEGS_FMF_2_13			, // moved to DLC
	LEGS_FMF_2_14			, // moved to DLC
	LEGS_FMF_2_15			, // moved to DLC
	LEGS_FMF_3_0			,
	LEGS_FMF_3_1			,
	LEGS_FMF_3_2			,
	LEGS_FMF_3_3			,
	LEGS_FMF_3_4			,
	LEGS_FMF_3_5			,
	LEGS_FMF_3_6			,
	LEGS_FMF_3_7			,
	LEGS_FMF_3_8			,
	LEGS_FMF_3_9			,
	LEGS_FMF_3_10			,
	LEGS_FMF_3_11			,
	LEGS_FMF_3_12			,
	LEGS_FMF_3_13			,
	LEGS_FMF_3_14			,
	LEGS_FMF_3_15			,
	LEGS_FMF_4_0			,
	LEGS_FMF_4_1			,
	LEGS_FMF_4_2			,
	LEGS_FMF_4_3			,
	LEGS_FMF_4_4			,
	LEGS_FMF_4_5			,
	LEGS_FMF_4_6			,
	LEGS_FMF_4_7			,
	LEGS_FMF_4_8			,
	LEGS_FMF_4_9			,
	LEGS_FMF_4_10			,
	LEGS_FMF_4_11			,
	LEGS_FMF_4_12			,
	LEGS_FMF_4_13			,
	LEGS_FMF_4_14			,
	LEGS_FMF_4_15			,
	LEGS_FMF_5_0			, // moved to DLC
	LEGS_FMF_5_1			, // moved to DLC
	LEGS_FMF_5_2			, // moved to DLC
	LEGS_FMF_5_3			, // moved to DLC
	LEGS_FMF_5_4			, // moved to DLC
	LEGS_FMF_5_5			, // moved to DLC
	LEGS_FMF_5_6			, // moved to DLC
	LEGS_FMF_5_7			, // moved to DLC
	LEGS_FMF_5_8			,
	LEGS_FMF_5_9			, // moved to DLC
	LEGS_FMF_5_10			, // moved to DLC
	LEGS_FMF_5_11			, // moved to DLC
	LEGS_FMF_5_12			, // moved to DLC
	LEGS_FMF_5_13			, // moved to DLC
	LEGS_FMF_5_14			,
	LEGS_FMF_5_15			,
	LEGS_FMF_6_0			,
	LEGS_FMF_6_1			,
	LEGS_FMF_6_2			,
	LEGS_FMF_6_3			, // moved to DLC
	LEGS_FMF_6_4			, // moved to DLC
	LEGS_FMF_6_5			, // moved to DLC
	LEGS_FMF_6_6			, // moved to DLC
	LEGS_FMF_6_7			, // moved to DLC
	LEGS_FMF_6_8			, // moved to DLC
	LEGS_FMF_6_9			, // moved to DLC
	LEGS_FMF_6_10			, // moved to DLC
	LEGS_FMF_6_11			, // moved to DLC
	LEGS_FMF_6_12			, // moved to DLC
	LEGS_FMF_6_13			, // moved to DLC
	LEGS_FMF_6_14			, // moved to DLC
	LEGS_FMF_6_15			, // moved to DLC
	LEGS_FMF_7_0			,
	LEGS_FMF_7_1			,
	LEGS_FMF_7_2			,
	LEGS_FMF_7_3			, // moved to DLC
	LEGS_FMF_7_4			, // moved to DLC
	LEGS_FMF_7_5			, // moved to DLC
	LEGS_FMF_7_6			, // moved to DLC
	LEGS_FMF_7_7			, // moved to DLC
	LEGS_FMF_7_8			, // moved to DLC
	LEGS_FMF_7_9			, // moved to DLC
	LEGS_FMF_7_10			, // moved to DLC
	LEGS_FMF_7_11			, // moved to DLC
	LEGS_FMF_7_12			, // moved to DLC
	LEGS_FMF_7_13			, // moved to DLC
	LEGS_FMF_7_14			, // moved to DLC
	LEGS_FMF_7_15			, // moved to DLC
	LEGS_FMF_8_0			,
	LEGS_FMF_8_1			,
	LEGS_FMF_8_2			,
	LEGS_FMF_8_3			,
	LEGS_FMF_8_4			,
	LEGS_FMF_8_5			,
	LEGS_FMF_8_6			,
	LEGS_FMF_8_7			,
	LEGS_FMF_8_8			,
	LEGS_FMF_8_9			,
	LEGS_FMF_8_10			,
	LEGS_FMF_8_11			,
	LEGS_FMF_8_12			,
	LEGS_FMF_8_13			, // moved to DLC
	LEGS_FMF_8_14			,
	LEGS_FMF_8_15			,
	LEGS_FMF_9_0			,
	LEGS_FMF_9_1			,
	LEGS_FMF_9_2			,
	LEGS_FMF_9_3			,
	LEGS_FMF_9_4			,
	LEGS_FMF_9_5			,
	LEGS_FMF_9_6			,
	LEGS_FMF_9_7			,
	LEGS_FMF_9_8			,
	LEGS_FMF_9_9			,
	LEGS_FMF_9_10			,
	LEGS_FMF_9_11			,
	LEGS_FMF_9_12			,
	LEGS_FMF_9_13			,
	LEGS_FMF_9_14			,
	LEGS_FMF_9_15			,
	LEGS_FMF_10_0			,
	LEGS_FMF_10_1			,
	LEGS_FMF_10_2			,
	LEGS_FMF_10_3			, // moved to DLC
	LEGS_FMF_10_4			, // moved to DLC
	LEGS_FMF_10_5			, // moved to DLC
	LEGS_FMF_10_6			, // moved to DLC
	LEGS_FMF_10_7			, // moved to DLC
	LEGS_FMF_10_8			, // moved to DLC
	LEGS_FMF_10_9			, // moved to DLC
	LEGS_FMF_10_10			, // moved to DLC
	LEGS_FMF_10_11			, // moved to DLC
	LEGS_FMF_10_12			, // moved to DLC
	LEGS_FMF_10_13			, // moved to DLC
	LEGS_FMF_10_14			, // moved to DLC
	LEGS_FMF_10_15			, // moved to DLC
	LEGS_FMF_11_0			,
	LEGS_FMF_11_1			,
	LEGS_FMF_11_2			,
	LEGS_FMF_11_3			,
	LEGS_FMF_11_4			,
	LEGS_FMF_11_5			,
	LEGS_FMF_11_6			,
	LEGS_FMF_11_7			,
	LEGS_FMF_11_8			,
	LEGS_FMF_11_9			,
	LEGS_FMF_11_10			,
	LEGS_FMF_11_11			,
	LEGS_FMF_11_12			,
	LEGS_FMF_11_13			,
	LEGS_FMF_11_14			,
	LEGS_FMF_11_15			,
	LEGS_FMF_12_0			,
	LEGS_FMF_12_1			,
	LEGS_FMF_12_2			,
	LEGS_FMF_12_3			,
	LEGS_FMF_12_4			,
	LEGS_FMF_12_5			,
	LEGS_FMF_12_6			,
	LEGS_FMF_12_7			,
	LEGS_FMF_12_8			,
	LEGS_FMF_12_9			,
	LEGS_FMF_12_10			,
	LEGS_FMF_12_11			,
	LEGS_FMF_12_12			,
	LEGS_FMF_12_13			,
	LEGS_FMF_12_14			,
	LEGS_FMF_12_15			,
	LEGS_FMF_13_0			, // moved to DLC
	LEGS_FMF_13_1			, // moved to DLC
	LEGS_FMF_13_2			, // moved to DLC
	LEGS_FMF_13_3			, // moved to DLC
	LEGS_FMF_13_4			, // moved to DLC
	LEGS_FMF_13_5			, // moved to DLC
	LEGS_FMF_13_6			, // moved to DLC
	LEGS_FMF_13_7			, // moved to DLC
	LEGS_FMF_13_8			, // moved to DLC
	LEGS_FMF_13_9			, // moved to DLC
	LEGS_FMF_13_10			, // moved to DLC
	LEGS_FMF_13_11			, // moved to DLC
	LEGS_FMF_13_12			, // moved to DLC
	LEGS_FMF_13_13			, // moved to DLC
	LEGS_FMF_13_14			, // moved to DLC
	LEGS_FMF_13_15			, // moved to DLC
	LEGS_FMF_14_0			,
	LEGS_FMF_14_1			,
	LEGS_FMF_14_2			, // moved to DLC
	LEGS_FMF_14_3			, // moved to DLC
	LEGS_FMF_14_4			, // moved to DLC
	LEGS_FMF_14_5			, // moved to DLC
	LEGS_FMF_14_6			, // moved to DLC
	LEGS_FMF_14_7			, // moved to DLC
	LEGS_FMF_14_8			,
	LEGS_FMF_14_9			,
	LEGS_FMF_14_10			, // moved to DLC
	LEGS_FMF_14_11			, // moved to DLC
	LEGS_FMF_14_12			, // moved to DLC
	LEGS_FMF_14_13			, // moved to DLC
	LEGS_FMF_14_14			, // moved to DLC
	LEGS_FMF_14_15			, // moved to DLC
	LEGS_FMF_15_0			,
	LEGS_FMF_15_1			, // moved to DLC
	LEGS_FMF_15_2			, // moved to DLC
	LEGS_FMF_15_3			,
	LEGS_FMF_15_4			, // moved to DLC
	LEGS_FMF_15_5			, // moved to DLC
	LEGS_FMF_15_6			, // moved to DLC
	LEGS_FMF_15_7			, // moved to DLC
	LEGS_FMF_15_8			, // moved to DLC
	LEGS_FMF_15_9			, // moved to DLC
	LEGS_FMF_15_10			,
	LEGS_FMF_15_11			,
	LEGS_FMF_15_12			, // moved to DLC
	LEGS_FMF_15_13			, // moved to DLC
	LEGS_FMF_15_14			, // moved to DLC
	LEGS_FMF_15_15			, // moved to DLC
	LEGS_FMF_DLC			,

	FEET_FMF_0_0			=0,
	FEET_FMF_0_1			,
	FEET_FMF_0_2			,
	FEET_FMF_0_3			,
	FEET_FMF_0_4			, // moved to DLC
	FEET_FMF_0_5			, // moved to DLC
	FEET_FMF_0_6			, // moved to DLC
	FEET_FMF_0_7			, // moved to DLC
	FEET_FMF_0_8			, // moved to DLC
	FEET_FMF_0_9			, // moved to DLC
	FEET_FMF_0_10			, // moved to DLC
	FEET_FMF_0_11			, // moved to DLC
	FEET_FMF_0_12			, // moved to DLC
	FEET_FMF_0_13			, // moved to DLC
	FEET_FMF_0_14			, // moved to DLC
	FEET_FMF_0_15			, // moved to DLC
	FEET_FMF_1_0			,
	FEET_FMF_1_1			,
	FEET_FMF_1_2			,
	FEET_FMF_1_3			,
	FEET_FMF_1_4			,
	FEET_FMF_1_5			,
	FEET_FMF_1_6			,
	FEET_FMF_1_7			,
	FEET_FMF_1_8			,
	FEET_FMF_1_9			,
	FEET_FMF_1_10			,
	FEET_FMF_1_11			,
	FEET_FMF_1_12			,
	FEET_FMF_1_13			,
	FEET_FMF_1_14			,
	FEET_FMF_1_15			,
	FEET_FMF_2_0			,
	FEET_FMF_2_1			,
	FEET_FMF_2_2			,
	FEET_FMF_2_3			,
	FEET_FMF_2_4			,
	FEET_FMF_2_5			,
	FEET_FMF_2_6			,
	FEET_FMF_2_7			,
	FEET_FMF_2_8			,
	FEET_FMF_2_9			,
	FEET_FMF_2_10			,
	FEET_FMF_2_11			,
	FEET_FMF_2_12			,
	FEET_FMF_2_13			,
	FEET_FMF_2_14			,
	FEET_FMF_2_15			,
	FEET_FMF_3_0			,
	FEET_FMF_3_1			,
	FEET_FMF_3_2			,
	FEET_FMF_3_3			,
	FEET_FMF_3_4			,
	FEET_FMF_3_5			,
	FEET_FMF_3_6			,
	FEET_FMF_3_7			,
	FEET_FMF_3_8			,
	FEET_FMF_3_9			,
	FEET_FMF_3_10			,
	FEET_FMF_3_11			,
	FEET_FMF_3_12			,
	FEET_FMF_3_13			,
	FEET_FMF_3_14			,
	FEET_FMF_3_15			,
	FEET_FMF_4_0			,
	FEET_FMF_4_1			,
	FEET_FMF_4_2			,
	FEET_FMF_4_3			,
	FEET_FMF_4_4			, // moved to DLC
	FEET_FMF_4_5			, // moved to DLC
	FEET_FMF_4_6			, // moved to DLC
	FEET_FMF_4_7			, // moved to DLC
	FEET_FMF_4_8			, // moved to DLC
	FEET_FMF_4_9			, // moved to DLC
	FEET_FMF_4_10			, // moved to DLC
	FEET_FMF_4_11			, // moved to DLC
	FEET_FMF_4_12			, // moved to DLC
	FEET_FMF_4_13			, // moved to DLC
	FEET_FMF_4_14			, // moved to DLC
	FEET_FMF_4_15			, // moved to DLC
	FEET_FMF_5_0			,
	FEET_FMF_5_1			,
	FEET_FMF_5_2			, // moved to DLC
	FEET_FMF_5_3			, // moved to DLC
	FEET_FMF_5_4			, // moved to DLC
	FEET_FMF_5_5			, // moved to DLC
	FEET_FMF_5_6			, // moved to DLC
	FEET_FMF_5_7			, // moved to DLC
	FEET_FMF_5_8			, // moved to DLC
	FEET_FMF_5_9			, // moved to DLC
	FEET_FMF_5_10			,
	FEET_FMF_5_11			, // moved to DLC
	FEET_FMF_5_12			, // moved to DLC
	FEET_FMF_5_13			,
	FEET_FMF_5_14			, // moved to DLC
	FEET_FMF_5_15			, // moved to DLC
	FEET_FMF_6_0			,
	FEET_FMF_6_1			,
	FEET_FMF_6_2			,
	FEET_FMF_6_3			,
	FEET_FMF_6_4			, // moved to DLC
	FEET_FMF_6_5			, // moved to DLC
	FEET_FMF_6_6			, // moved to DLC
	FEET_FMF_6_7			, // moved to DLC
	FEET_FMF_6_8			, // moved to DLC
	FEET_FMF_6_9			, // moved to DLC
	FEET_FMF_6_10			, // moved to DLC
	FEET_FMF_6_11			, // moved to DLC
	FEET_FMF_6_12			, // moved to DLC
	FEET_FMF_6_13			, // moved to DLC
	FEET_FMF_6_14			, // moved to DLC
	FEET_FMF_6_15			, // moved to DLC
	FEET_FMF_7_0			,
	FEET_FMF_7_1			,
	FEET_FMF_7_2			,
	FEET_FMF_7_3			,
	FEET_FMF_7_4			,
	FEET_FMF_7_5			,
	FEET_FMF_7_6			,
	FEET_FMF_7_7			,
	FEET_FMF_7_8			,
	FEET_FMF_7_9			,
	FEET_FMF_7_10			,
	FEET_FMF_7_11			,
	FEET_FMF_7_12			,
	FEET_FMF_7_13			,
	FEET_FMF_7_14			,
	FEET_FMF_7_15			,
	FEET_FMF_8_0			,
	FEET_FMF_8_1			,
	FEET_FMF_8_2			,
	FEET_FMF_8_3			,
	FEET_FMF_8_4			,
	FEET_FMF_8_5			,
	FEET_FMF_8_6			,
	FEET_FMF_8_7			,
	FEET_FMF_8_8			,
	FEET_FMF_8_9			,
	FEET_FMF_8_10			,
	FEET_FMF_8_11			,
	FEET_FMF_8_12			,
	FEET_FMF_8_13			,
	FEET_FMF_8_14			,
	FEET_FMF_8_15			,
	FEET_FMF_9_0			,
	FEET_FMF_9_1			,
	FEET_FMF_9_2			,
	FEET_FMF_9_3			,
	FEET_FMF_9_4			, // moved to DLC
	FEET_FMF_9_5			, // moved to DLC
	FEET_FMF_9_6			, // moved to DLC
	FEET_FMF_9_7			, // moved to DLC
	FEET_FMF_9_8			, // moved to DLC
	FEET_FMF_9_9			, // moved to DLC
	FEET_FMF_9_10			, // moved to DLC
	FEET_FMF_9_11			,
	FEET_FMF_9_12			,
	FEET_FMF_9_13			, // moved to DLC
	FEET_FMF_9_14			, // moved to DLC
	FEET_FMF_9_15			, // moved to DLC
	FEET_FMF_10_0			,
	FEET_FMF_10_1			,
	FEET_FMF_10_2			,
	FEET_FMF_10_3			,
	FEET_FMF_10_4			, // moved to DLC
	FEET_FMF_10_5			, // moved to DLC
	FEET_FMF_10_6			, // moved to DLC
	FEET_FMF_10_7			, // moved to DLC
	FEET_FMF_10_8			, // moved to DLC
	FEET_FMF_10_9			, // moved to DLC
	FEET_FMF_10_10			, // moved to DLC
	FEET_FMF_10_11			, // moved to DLC
	FEET_FMF_10_12			, // moved to DLC
	FEET_FMF_10_13			, // moved to DLC
	FEET_FMF_10_14			, // moved to DLC
	FEET_FMF_10_15			, // moved to DLC
	FEET_FMF_11_0			,
	FEET_FMF_11_1			,
	FEET_FMF_11_2			,
	FEET_FMF_11_3			,
	FEET_FMF_11_4			, // moved to DLC
	FEET_FMF_11_5			, // moved to DLC
	FEET_FMF_11_6			, // moved to DLC
	FEET_FMF_11_7			, // moved to DLC
	FEET_FMF_11_8			, // moved to DLC
	FEET_FMF_11_9			, // moved to DLC
	FEET_FMF_11_10			, // moved to DLC
	FEET_FMF_11_11			, // moved to DLC
	FEET_FMF_11_12			, // moved to DLC
	FEET_FMF_11_13			, // moved to DLC
	FEET_FMF_11_14			, // moved to DLC
	FEET_FMF_11_15			, // moved to DLC
	FEET_FMF_12_0			, // moved to DLC
	FEET_FMF_12_1			, // moved to DLC
	FEET_FMF_12_2			, // moved to DLC
	FEET_FMF_12_3			, // moved to DLC
	FEET_FMF_12_4			, // moved to DLC
	FEET_FMF_12_5			, // moved to DLC
	FEET_FMF_12_6			, // moved to DLC
	FEET_FMF_12_7			, // moved to DLC
	FEET_FMF_12_8			, // moved to DLC
	FEET_FMF_12_9			, // moved to DLC
	FEET_FMF_12_10			, // moved to DLC
	FEET_FMF_12_11			, // moved to DLC
	FEET_FMF_12_12			, // moved to DLC
	FEET_FMF_12_13			, // moved to DLC
	FEET_FMF_12_14			, // moved to DLC
	FEET_FMF_12_15			, // moved to DLC
	FEET_FMF_13_0			,
	FEET_FMF_13_1			,
	FEET_FMF_13_2			,
	FEET_FMF_13_3			,
	FEET_FMF_13_4			,
	FEET_FMF_13_5			,
	FEET_FMF_13_6			,
	FEET_FMF_13_7			,
	FEET_FMF_13_8			,
	FEET_FMF_13_9			,
	FEET_FMF_13_10			,
	FEET_FMF_13_11			,
	FEET_FMF_13_12			,
	FEET_FMF_13_13			,
	FEET_FMF_13_14			,
	FEET_FMF_13_15			,
	FEET_FMF_14_0			,
	FEET_FMF_14_1			,
	FEET_FMF_14_2			,
	FEET_FMF_14_3			,
	FEET_FMF_14_4			,
	FEET_FMF_14_5			,
	FEET_FMF_14_6			,
	FEET_FMF_14_7			,
	FEET_FMF_14_8			,
	FEET_FMF_14_9			,
	FEET_FMF_14_10			,
	FEET_FMF_14_11			,
	FEET_FMF_14_12			,
	FEET_FMF_14_13			,
	FEET_FMF_14_14			,
	FEET_FMF_14_15			,
	FEET_FMF_15_0			,
	FEET_FMF_15_1			,
	FEET_FMF_15_2			,
	FEET_FMF_15_3			,
	FEET_FMF_15_4			,
	FEET_FMF_15_5			,
	FEET_FMF_15_6			,
	FEET_FMF_15_7			,
	FEET_FMF_15_8			,
	FEET_FMF_15_9			,
	FEET_FMF_15_10			,
	FEET_FMF_15_11			,
	FEET_FMF_15_12			,
	FEET_FMF_15_13			,
	FEET_FMF_15_14			,
	FEET_FMF_15_15			,
	FEET_FMF_DLC			,
	
	BERD_FMF_0_0			=0,
	BERD_FMF_1_0			,
	BERD_FMF_1_1			,
	BERD_FMF_1_2			,
	BERD_FMF_1_3			,
	BERD_FMF_2_0			,
	BERD_FMF_2_1			,
	BERD_FMF_2_2			,
	BERD_FMF_2_3			,
	BERD_FMF_3_0			,
	BERD_FMF_4_0			,
	BERD_FMF_4_1			,
	BERD_FMF_4_2			,
	BERD_FMF_4_3			,
	BERD_FMF_5_0			,
	BERD_FMF_5_1			,
	BERD_FMF_5_2			,
	BERD_FMF_5_3			,
	BERD_FMF_6_0			,
	BERD_FMF_6_1			,
	BERD_FMF_6_2			,
	BERD_FMF_6_3			,
	BERD_FMF_7_0			,
	BERD_FMF_7_1			,
	BERD_FMF_7_2			,
	BERD_FMF_7_3			,
	BERD_FMF_DLC			,
	
	TEETH_FMF_0_0			=0,
	TEETH_FMF_1_0			, 
	TEETH_FMF_1_1			,
	TEETH_FMF_1_2			,
	TEETH_FMF_1_3			,
	TEETH_FMF_1_4			,
	TEETH_FMF_1_5			,
	TEETH_FMF_2_0			, 
	TEETH_FMF_2_1			,
	TEETH_FMF_2_2			,
	TEETH_FMF_2_3			,
	TEETH_FMF_2_4			,
	TEETH_FMF_2_5			,
	TEETH_FMF_3_0			, 
	TEETH_FMF_3_1			,
	TEETH_FMF_3_2			,
	TEETH_FMF_3_3			,
	TEETH_FMF_3_4			,
	TEETH_FMF_3_5			,
	TEETH_FMF_4_0			, // moved to DLC
	TEETH_FMF_4_1			, // moved to DLC
	TEETH_FMF_4_2			,
	TEETH_FMF_4_3			,
	TEETH_FMF_4_4			, // moved to DLC
	TEETH_FMF_4_5			, // moved to DLC
	TEETH_FMF_5_0			, // moved to DLC
	TEETH_FMF_5_1			, // moved to DLC
	TEETH_FMF_5_2			, // moved to DLC
	TEETH_FMF_5_3			, // moved to DLC
	TEETH_FMF_5_4			,
	TEETH_FMF_5_5			,
	TEETH_FMF_6_0			, 
	TEETH_FMF_6_1			, 
	TEETH_FMF_6_2			, 
	TEETH_FMF_6_3			, 
	TEETH_FMF_6_4			, 
	TEETH_FMF_6_5			, 
	TEETH_FMF_7_0			, 
	TEETH_FMF_7_1			, 
	TEETH_FMF_7_2			,  // moved to DLC
	TEETH_FMF_7_3			,  // moved to DLC
	TEETH_FMF_7_4			,  // moved to DLC
	TEETH_FMF_7_5			,  // moved to DLC
	TEETH_FMF_8_0			,  // moved to DLC
	TEETH_FMF_8_1			,  // moved to DLC
	TEETH_FMF_8_2			,  // moved to DLC
	TEETH_FMF_8_3			,  // moved to DLC
	TEETH_FMF_8_4			,  // moved to DLC
	TEETH_FMF_8_5			,  // moved to DLC
	TEETH_FMF_9_0			, 
	TEETH_FMF_9_1			,  // moved to DLC
	TEETH_FMF_9_2			,  // moved to DLC
	TEETH_FMF_9_3			,  // moved to DLC
	TEETH_FMF_9_4			,  // moved to DLC
	TEETH_FMF_9_5			,  // moved to DLC
	TEETH_FMF_DLC			,
	
	// props need to be ordered by anchor and then drawable / variation
	// if the 1st prop of a type changes, update GET_FIRST_PROP_OF_TYPE
	// ANCHOR_HEAD, 
	PROPS_FMF_EARDEFENDERS_0_0		= 10, // Start props at 10.
	PROPS_FMF_EARDEFENDERS_0_1		,
	PROPS_FMF_EARDEFENDERS_0_2		,
	PROPS_FMF_EARDEFENDERS_0_3		,
	PROPS_FMF_EARDEFENDERS_0_4		,
	PROPS_FMF_EARDEFENDERS_0_5		,
	PROPS_FMF_EARDEFENDERS_0_6		,
	PROPS_FMF_EARDEFENDERS_0_7		,
	
	PROPS_FMF_DUNCE_HAT_1_0			,
	
	PROPS_FMF_HAT_2_0		, // moved to DLC
	PROPS_FMF_HAT_2_1		,
	PROPS_FMF_HAT_2_2		, // moved to DLC
	PROPS_FMF_HAT_2_3		, // moved to DLC
	PROPS_FMF_HAT_2_4		, // moved to DLC
	PROPS_FMF_HAT_2_5		, // moved to DLC
	PROPS_FMF_HAT_2_6		, // moved to DLC
	PROPS_FMF_HAT_2_7		, // moved to DLC
	
	PROPS_FMF_HAT_3_0		, // moved to DLC
	PROPS_FMF_HAT_3_1		, // moved to DLC
	PROPS_FMF_HAT_3_2		, // moved to DLC
	PROPS_FMF_HAT_3_3		, // moved to DLC
	PROPS_FMF_HAT_3_4		, // moved to DLC
	PROPS_FMF_HAT_3_5		, // moved to DLC
	PROPS_FMF_HAT_3_6		, // moved to DLC
	PROPS_FMF_HAT_3_7		,
	
	PROPS_FMF_HAT_4_0		,
	PROPS_FMF_HAT_4_1		,
	PROPS_FMF_HAT_4_2		,
	PROPS_FMF_HAT_4_3		,
	PROPS_FMF_HAT_4_4		,
	PROPS_FMF_HAT_4_5		,
	PROPS_FMF_HAT_4_6		,
	PROPS_FMF_HAT_4_7		,
	
	PROPS_FMF_HAT_5_0		,
	PROPS_FMF_HAT_5_1		,
	PROPS_FMF_HAT_5_2		,
	PROPS_FMF_HAT_5_3		,
	PROPS_FMF_HAT_5_4		,
	PROPS_FMF_HAT_5_5		,
	PROPS_FMF_HAT_5_6		,
	PROPS_FMF_HAT_5_7		,
	
	PROPS_FMF_HAT_6_0		,
	PROPS_FMF_HAT_6_1		,
	PROPS_FMF_HAT_6_2		,
	PROPS_FMF_HAT_6_3		,
	PROPS_FMF_HAT_6_4		,
	PROPS_FMF_HAT_6_5		,
	PROPS_FMF_HAT_6_6		,
	PROPS_FMF_HAT_6_7		,
	
	PROPS_FMF_HAT_7_0		,
	PROPS_FMF_HAT_7_1		,
	PROPS_FMF_HAT_7_2		,
	PROPS_FMF_HAT_7_3		,
	PROPS_FMF_HAT_7_4		,
	PROPS_FMF_HAT_7_5		,
	PROPS_FMF_HAT_7_6		,
	PROPS_FMF_HAT_7_7		,
	
	PROPS_FMF_HAT_8_0		, // moved to DLC
	PROPS_FMF_HAT_8_1		, // moved to DLC
	PROPS_FMF_HAT_8_2		, // moved to DLC
	PROPS_FMF_HAT_8_3		, // moved to DLC
	PROPS_FMF_HAT_8_4		, // moved to DLC
	PROPS_FMF_HAT_8_5		, // moved to DLC
	PROPS_FMF_HAT_8_6		, // moved to DLC
	PROPS_FMF_HAT_8_7		, // moved to DLC
	
	PROPS_FMF_HAT_9_0		,
	PROPS_FMF_HAT_9_1		,
	PROPS_FMF_HAT_9_2		,
	PROPS_FMF_HAT_9_3		,
	PROPS_FMF_HAT_9_4		,
	PROPS_FMF_HAT_9_5		,
	PROPS_FMF_HAT_9_6		,
	PROPS_FMF_HAT_9_7		,
	
	PROPS_FMF_HAT_10_0		, // moved to DLC
	PROPS_FMF_HAT_10_1		, // moved to DLC
	PROPS_FMF_HAT_10_2		, // moved to DLC
	PROPS_FMF_HAT_10_3		, // moved to DLC
	PROPS_FMF_HAT_10_4		, // moved to DLC
	PROPS_FMF_HAT_10_5		, // moved to DLC
	PROPS_FMF_HAT_10_6		, // moved to DLC
	PROPS_FMF_HAT_10_7		,
	
	PROPS_FMF_HAT_11_0		, // moved to DLC
	PROPS_FMF_HAT_11_1		,
	PROPS_FMF_HAT_11_2		, // moved to DLC
	PROPS_FMF_HAT_11_3		, // moved to DLC
	PROPS_FMF_HAT_11_4		, // moved to DLC
	PROPS_FMF_HAT_11_5		, // moved to DLC
	PROPS_FMF_HAT_11_6		, // moved to DLC
	PROPS_FMF_HAT_11_7		, // moved to DLC
	
	PROPS_FMF_HAT_12_0		,
	PROPS_FMF_HAT_12_1		, // moved to DLC
	PROPS_FMF_HAT_12_2		, // moved to DLC
	PROPS_FMF_HAT_12_3		, // moved to DLC
	PROPS_FMF_HAT_12_4		, // moved to DLC
	PROPS_FMF_HAT_12_5		, // moved to DLC
	PROPS_FMF_HAT_12_6		,
	PROPS_FMF_HAT_12_7		,
	
	PROPS_FMF_HAT_13_0		,
	PROPS_FMF_HAT_13_1		,
	PROPS_FMF_HAT_13_2		,
	PROPS_FMF_HAT_13_3		,
	PROPS_FMF_HAT_13_4		,
	PROPS_FMF_HAT_13_5		,
	PROPS_FMF_HAT_13_6		,
	PROPS_FMF_HAT_13_7		,
	
	PROPS_FMF_HAT_14_0		,
	PROPS_FMF_HAT_14_1		,
	PROPS_FMF_HAT_14_2		,
	PROPS_FMF_HAT_14_3		,
	PROPS_FMF_HAT_14_4		,
	PROPS_FMF_HAT_14_5		,
	PROPS_FMF_HAT_14_6		,
	PROPS_FMF_HAT_14_7		,
	
	PROPS_FMF_HAT_15_0		,
	PROPS_FMF_HAT_15_1		,
	PROPS_FMF_HAT_15_2		,
	PROPS_FMF_HAT_15_3		,
	PROPS_FMF_HAT_15_4		,
	PROPS_FMF_HAT_15_5		,
	PROPS_FMF_HAT_15_6		,
	PROPS_FMF_HAT_15_7		,
	
	PROPS_FMF_HELMET_16_0	,
	PROPS_FMF_HELMET_16_1	,
	PROPS_FMF_HELMET_16_2	,
	PROPS_FMF_HELMET_16_3	,
	PROPS_FMF_HELMET_16_4	,
	PROPS_FMF_HELMET_16_5	,
	PROPS_FMF_HELMET_16_6	,
	PROPS_FMF_HELMET_16_7	,
					 
	PROPS_FMF_HELMET_17_0	,
	PROPS_FMF_HELMET_17_1	,
	PROPS_FMF_HELMET_17_2	,
	PROPS_FMF_HELMET_17_3	,
	PROPS_FMF_HELMET_17_4	,
	PROPS_FMF_HELMET_17_5	,
	PROPS_FMF_HELMET_17_6	,
	PROPS_FMF_HELMET_17_7	,
					 
	PROPS_FMF_HELMET_18_0	,
	PROPS_FMF_HELMET_18_1	,
	PROPS_FMF_HELMET_18_2	,
	PROPS_FMF_HELMET_18_3	,
	PROPS_FMF_HELMET_18_4	,
	PROPS_FMF_HELMET_18_5	,
	PROPS_FMF_HELMET_18_6	,
	PROPS_FMF_HELMET_18_7	,
	
	//ANCHOR_EYES, 
	PROPS_FMF_GLASSES_0_0		,
	PROPS_FMF_GLASSES_0_1		,
	PROPS_FMF_GLASSES_0_2		,
	PROPS_FMF_GLASSES_0_3		,
	PROPS_FMF_GLASSES_0_4		,
	PROPS_FMF_GLASSES_0_5		,
	PROPS_FMF_GLASSES_0_6		,
	PROPS_FMF_GLASSES_0_7		,
	PROPS_FMF_GLASSES_0_8		,
	PROPS_FMF_GLASSES_0_9		,
	PROPS_FMF_GLASSES_0_10		,
	
	PROPS_FMF_GLASSES_1_0		,
	PROPS_FMF_GLASSES_1_1		,
	PROPS_FMF_GLASSES_1_2		,
	PROPS_FMF_GLASSES_1_3		,
	PROPS_FMF_GLASSES_1_4		,
	PROPS_FMF_GLASSES_1_5		,
	PROPS_FMF_GLASSES_1_6		,
	PROPS_FMF_GLASSES_1_7		,
	PROPS_FMF_GLASSES_1_8		,
	PROPS_FMF_GLASSES_1_9		,
	PROPS_FMF_GLASSES_1_10		,
	
	PROPS_FMF_GLASSES_2_0		,
	PROPS_FMF_GLASSES_2_1		,
	PROPS_FMF_GLASSES_2_2		,
	PROPS_FMF_GLASSES_2_3		,
	PROPS_FMF_GLASSES_2_4		,
	PROPS_FMF_GLASSES_2_5		,
	PROPS_FMF_GLASSES_2_6		,
	PROPS_FMF_GLASSES_2_7		,
	PROPS_FMF_GLASSES_2_8		,
	PROPS_FMF_GLASSES_2_9		,
	PROPS_FMF_GLASSES_2_10		,
	
	PROPS_FMF_GLASSES_3_0		,
	PROPS_FMF_GLASSES_3_1		,
	PROPS_FMF_GLASSES_3_2		,
	PROPS_FMF_GLASSES_3_3		,
	PROPS_FMF_GLASSES_3_4		,
	PROPS_FMF_GLASSES_3_5		,
	PROPS_FMF_GLASSES_3_6		,
	PROPS_FMF_GLASSES_3_7		,
	PROPS_FMF_GLASSES_3_8		,
	PROPS_FMF_GLASSES_3_9		,
	PROPS_FMF_GLASSES_3_10		,
	
	PROPS_FMF_GLASSES_4_0		,
	PROPS_FMF_GLASSES_4_1		,
	PROPS_FMF_GLASSES_4_2		,
	PROPS_FMF_GLASSES_4_3		,
	PROPS_FMF_GLASSES_4_4		,
	PROPS_FMF_GLASSES_4_5		,
	PROPS_FMF_GLASSES_4_6		,
	PROPS_FMF_GLASSES_4_7		,
	PROPS_FMF_GLASSES_4_8		,
	PROPS_FMF_GLASSES_4_9		,
	PROPS_FMF_GLASSES_4_10		,
	
	PROPS_FMF_GLASSES_5_0		, // moved to DLC
	PROPS_FMF_GLASSES_5_1		, // moved to DLC
	PROPS_FMF_GLASSES_5_2		, // moved to DLC
	PROPS_FMF_GLASSES_5_3		, // moved to DLC
	PROPS_FMF_GLASSES_5_4		, // moved to DLC
	PROPS_FMF_GLASSES_5_5		, // moved to DLC
	PROPS_FMF_GLASSES_5_6		, // moved to DLC
	PROPS_FMF_GLASSES_5_7		, // moved to DLC
	PROPS_FMF_GLASSES_5_8		,
	PROPS_FMF_GLASSES_5_9		,
	PROPS_FMF_GLASSES_5_10		,
	
	PROPS_FMF_GLASSES_6_0		,
	PROPS_FMF_GLASSES_6_1		, // moved to DLC
	PROPS_FMF_GLASSES_6_2		, // moved to DLC
	PROPS_FMF_GLASSES_6_3		, // moved to DLC
	PROPS_FMF_GLASSES_6_4		, // moved to DLC
	PROPS_FMF_GLASSES_6_5		, // moved to DLC
	PROPS_FMF_GLASSES_6_6		, // moved to DLC
	PROPS_FMF_GLASSES_6_7		, // moved to DLC
	PROPS_FMF_GLASSES_6_8		,
	PROPS_FMF_GLASSES_6_9		,
	PROPS_FMF_GLASSES_6_10		,
	
	PROPS_FMF_GLASSES_7_0		,
	PROPS_FMF_GLASSES_7_1		,
	PROPS_FMF_GLASSES_7_2		,
	PROPS_FMF_GLASSES_7_3		,
	PROPS_FMF_GLASSES_7_4		,
	PROPS_FMF_GLASSES_7_5		,
	PROPS_FMF_GLASSES_7_6		,
	PROPS_FMF_GLASSES_7_7		,
	PROPS_FMF_GLASSES_7_8		,
	PROPS_FMF_GLASSES_7_9		,
	PROPS_FMF_GLASSES_7_10		,
	
	PROPS_FMF_GLASSES_8_0		,
	PROPS_FMF_GLASSES_8_1		, // moved to DLC
	PROPS_FMF_GLASSES_8_2		, // moved to DLC
	PROPS_FMF_GLASSES_8_3		, // moved to DLC
	PROPS_FMF_GLASSES_8_4		, // moved to DLC
	PROPS_FMF_GLASSES_8_5		, // moved to DLC
	PROPS_FMF_GLASSES_8_6		, // moved to DLC
	PROPS_FMF_GLASSES_8_7		, // moved to DLC
	PROPS_FMF_GLASSES_8_8		,
	PROPS_FMF_GLASSES_8_9		,
	PROPS_FMF_GLASSES_8_10		,
	
	PROPS_FMF_GLASSES_9_0		,
	PROPS_FMF_GLASSES_9_1		,
	PROPS_FMF_GLASSES_9_2		,
	PROPS_FMF_GLASSES_9_3		,
	PROPS_FMF_GLASSES_9_4		,
	PROPS_FMF_GLASSES_9_5		,
	PROPS_FMF_GLASSES_9_6		,
	PROPS_FMF_GLASSES_9_7		,
	PROPS_FMF_GLASSES_9_8		,
	PROPS_FMF_GLASSES_9_9		,
	PROPS_FMF_GLASSES_9_10		,
	
	PROPS_FMF_GLASSES_10_0		,
	PROPS_FMF_GLASSES_10_1		,
	PROPS_FMF_GLASSES_10_2		,
	PROPS_FMF_GLASSES_10_3		,
	PROPS_FMF_GLASSES_10_4		,
	PROPS_FMF_GLASSES_10_5		,
	PROPS_FMF_GLASSES_10_6		,
	PROPS_FMF_GLASSES_10_7		,
	PROPS_FMF_GLASSES_10_8		,
	PROPS_FMF_GLASSES_10_9		,
	PROPS_FMF_GLASSES_10_10		,
	
	PROPS_FMF_GLASSES_11_0		,
	PROPS_FMF_GLASSES_11_1		,
	PROPS_FMF_GLASSES_11_2		,
	PROPS_FMF_GLASSES_11_3		,
	PROPS_FMF_GLASSES_11_4		,
	PROPS_FMF_GLASSES_11_5		,
	PROPS_FMF_GLASSES_11_6		,
	PROPS_FMF_GLASSES_11_7		,
	
	PROPS_FMF_GLASSES_12_0		, // moved to DLC
	PROPS_FMF_GLASSES_12_1		, // moved to DLC
	PROPS_FMF_GLASSES_12_2		, // moved to DLC
	PROPS_FMF_GLASSES_12_3		, // moved to DLC
	PROPS_FMF_GLASSES_12_4		, // moved to DLC
	PROPS_FMF_GLASSES_12_5		, // moved to DLC
	PROPS_FMF_GLASSES_12_6		, // moved to DLC
	PROPS_FMF_GLASSES_12_7		, // moved to DLC
	
	PROPS_FMF_GLASSES_13_0		, // moved to DLC
	PROPS_FMF_GLASSES_13_1		, // moved to DLC
	PROPS_FMF_GLASSES_13_2		, // moved to DLC
	PROPS_FMF_GLASSES_13_3		, // moved to DLC
	PROPS_FMF_GLASSES_13_4		, // moved to DLC
	PROPS_FMF_GLASSES_13_5		, // moved to DLC
	PROPS_FMF_GLASSES_13_6		, // moved to DLC
	PROPS_FMF_GLASSES_13_7		, // moved to DLC
	
	PROPS_FMF_GLASSES_14_0		,
	PROPS_FMF_GLASSES_14_1		,
	PROPS_FMF_GLASSES_14_2		,
	PROPS_FMF_GLASSES_14_3		,
	PROPS_FMF_GLASSES_14_4		,
	PROPS_FMF_GLASSES_14_5		,
	PROPS_FMF_GLASSES_14_6		,
	PROPS_FMF_GLASSES_14_7		,
	PROPS_FMF_GLASSES_14_8		,
	PROPS_FMF_GLASSES_14_9		,
	PROPS_FMF_GLASSES_14_10		,
	
	PROPS_FMF_GLASSES_15_0		, // moved to DLC
	PROPS_FMF_GLASSES_15_1		, // moved to DLC
	PROPS_FMF_GLASSES_15_2		, // moved to DLC
	PROPS_FMF_GLASSES_15_3		, // moved to DLC
	PROPS_FMF_GLASSES_15_4		, // moved to DLC
	PROPS_FMF_GLASSES_15_5		, // moved to DLC
	PROPS_FMF_GLASSES_15_6		, // moved to DLC
	PROPS_FMF_GLASSES_15_7		, // moved to DLC
	
	//ANCHOR_EARS
    //ANCHOR_MOUTH,
   	//ANCHOR_LEFT_HAND,
    //ANCHOR_RIGHT_HAND,
	
   	//ANCHOR_LEFT_WRIST,
	PROPS_FMF_WATCH_0_0		, // moved to DLC
	PROPS_FMF_WATCH_0_1		, // moved to DLC
	PROPS_FMF_WATCH_0_2		, // moved to DLC
	PROPS_FMF_WATCH_0_3		, // moved to DLC
	PROPS_FMF_WATCH_0_4		,
	
	PROPS_FMF_WATCH_1_0		, // moved to DLC
	PROPS_FMF_WATCH_1_1		, // moved to DLC
	PROPS_FMF_WATCH_1_2		, // moved to DLC
	
	PROPS_FMF_DLC			,
	
	HEAD_FMF_DLC			=0,
	
	HAND_FMF_0_0			=0,
	HAND_FMF_1_0			,
	HAND_FMF_2_0			,
	HAND_FMF_3_0			,
	HAND_FMF_4_0			,
	HAND_FMF_5_0			,
	HAND_FMF_6_0			,
	HAND_FMF_7_0			,
	HAND_FMF_8_0			,
	HAND_FMF_DLC			,
	
	//ANCHOR_RIGHT_WRIST,
    //ANCHOR_HIP
	
	OUTFIT_FMF_DEFAULT		= OUTFIT_DEFAULT,
	OUTFIT_FMF_SUB_1		,
	OUTFIT_FMF_SUB_2		,
	OUTFIT_FMF_SUB_3		,
	OUTFIT_FMF_SUB_4		,
	OUTFIT_FMF_SUB_5		,
	OUTFIT_FMF_SUB_6		,
	OUTFIT_FMF_SUB_7		,
	OUTFIT_FMF_SUB_8		,
	OUTFIT_FMF_PONS_1		,
	OUTFIT_FMF_PONS_2		,
	OUTFIT_FMF_PONS_3		,
	OUTFIT_FMF_PONS_4		,
	OUTFIT_FMF_PONS_5		,
	OUTFIT_FMF_PONS_6		,
	OUTFIT_FMF_PONS_7		,
	OUTFIT_FMF_PONS_8		,
	OUTFIT_FMF_PONS_9		,
	OUTFIT_FMF_PONS_10		,
	OUTFIT_FMF_BIN_1		,
	OUTFIT_FMF_BIN_2		,
	OUTFIT_FMF_BIN_3		,
	OUTFIT_FMF_BIN_4		,
	OUTFIT_FMF_BIN_5		,
	OUTFIT_FMF_BIN_6		,
	OUTFIT_FMF_BIN_7		,
	OUTFIT_FMF_BIN_8		,
	OUTFIT_FMF_BIN_9		,
	OUTFIT_FMF_DLC,
	
	PROPGROUP_FMF_SUB_2		=0,
	PROPGROUP_FMF_SUB_3		,
	PROPGROUP_FMF_SUB_4		,
	PROPGROUP_FMF_SUB_5		,
	PROPGROUP_FMF_SUB_6		,
	PROPGROUP_FMF_SUB_7		,
	PROPGROUP_FMF_SUB_8		,
	PROPGROUP_FMF_PONS_1	,
	PROPGROUP_FMF_PONS_2	,
	PROPGROUP_FMF_PONS_3	,
	PROPGROUP_FMF_PONS_4	,
	PROPGROUP_FMF_PONS_6	,
	PROPGROUP_FMF_PONS_7	,
	PROPGROUP_FMF_PONS_8	,
	PROPGROUP_FMF_PONS_9	,
	PROPGROUP_FMF_PONS_10	,
	PROPGROUP_FMF_BIN_1		,
	PROPGROUP_FMF_BIN_2		,
	PROPGROUP_FMF_BIN_4		,
	PROPGROUP_FMF_BIN_5		,
	PROPGROUP_FMF_BIN_6		,
	PROPGROUP_FMF_BIN_7		,
	PROPGROUP_FMF_BIN_8		,
	PROPGROUP_FMF_BIN_9	
	#IF USE_TU_CHANGES
	,PROPGROUP_FMF_DLC
	#ENDIF
ENDENUM

//////////////////////////////////////////////////////////////////////////////////////////
///    Custom enum list to represent component items as well as outfits
///     
CONST_INT NUMBER_OF_PED_PROP_TYPES 9
CONST_INT NUMBER_OF_PED_COMP_TYPES 15
ENUM PED_COMP_TYPE_ENUM
	COMP_TYPE_HEAD = 0,
	COMP_TYPE_BERD,
	COMP_TYPE_HAIR,
	COMP_TYPE_TORSO,
	COMP_TYPE_LEGS,
	COMP_TYPE_HAND,
	COMP_TYPE_FEET,
	COMP_TYPE_TEETH,
	COMP_TYPE_SPECIAL,
	COMP_TYPE_SPECIAL2,
	COMP_TYPE_DECL,
	COMP_TYPE_JBIB,
	
	COMP_TYPE_OUTFIT,
	COMP_TYPE_PROPGROUP,
	COMP_TYPE_PROPS
ENDENUM

/// PURPOSE: Used to specify the prop type so we can group them in the wardrobe menu
ENUM PED_PROP_TYPE_ENUM
	PROP_TYPE_DUMMY = -1,
	
	// Defaults
	PROP_TYPE_HEAD,
	PROP_TYPE_EYES,
	PROP_TYPE_EARS,
	PROP_TYPE_MOUTH,
	PROP_TYPE_L_HAND,
	PROP_TYPE_R_HAND,
	PROP_TYPE_L_WRIST,
	PROP_TYPE_R_WRIST,
	PROP_TYPE_HIP,
	
	// Custom
	PROP_TYPE_HAT,
	PROP_TYPE_GLASSES,
	PROP_TYPE_MASK,
	PROP_TYPE_HEADPHONES,
	PROP_TYPE_BANDANA
ENDENUM

/// PURPOSE: Used to flag the look of a ped component. Used so that characters can comment appropriately.
ENUM PED_COMP_STYLE_ENUM
	COMP_STYLE_EMPTY = 0, 	//This component slot is empty.
	
	COMP_STYLE_CRAZY,
	COMP_STYLE_SENSIBLE,
	COMP_STYLE_STYLISH,
	COMP_STYLE_UGLY
ENDENUM

/// PURPOSE: Flags used when searching bitfields for ped component items
ENUM PED_COMPONENT_SEARCH_FLAGS

	PED_COMP_FLAG_INVALID 				= 0,
	
	PED_COMP_FLAG_OUTFIT_SPECIFIC		= 1,
	PED_COMP_FLAG_AVAILABLE				= 2,
	PED_COMP_FLAG_ACQUIRED				= 4,
	PED_COMP_FLAG_APPEAR_IN_WARDROBE	= 8,
	PED_COMP_FLAG_LOOP					= 16,
	PED_COMP_FLAG_PROP_HAT				= 32,
	PED_COMP_FLAG_PROP_MASK				= 64,
	PED_COMP_FLAG_PROP_BANDANA			= 128,
	PED_COMP_FLAG_PROP_GLASSES			= 256,
	PED_COMP_FLAG_PROP_HEADPHONES		= 512
ENDENUM

/// PURPOSE: Used to specify which type of spawn outfit a MP character should use
ENUM MP_SPAWN_OUTFIT_TYPE
	MPSO_CASUAL,
	MPSO_COUCH_POTATO,
	MPSO_ILLEGAL_WORK,
	MPSO_LEGAL_WORK,
	MPSO_PARTYING,
	MPSO_SPORT
ENDENUM

/// PURPOSE: MP beards (clean shaven is -1)
ENUM MP_BEARDS
	MP_BEARD_LIGHT_STUBBLE = 0,
	MP_BEARD_BALBO,
	MP_BEARD_CIRCLE,
	MP_BEARD_GOATEE,
	MP_BEARD_CHIN,
	MP_BEARD_SOUL_PATCH,
	MP_BEARD_PENCIL_CHIN_STRAP,
	MP_BEARD_LIGHT,
	MP_BEARD_MUSKETEER,
	MP_BEARD_MUSTACHE,
	MP_BEARD_HEAVY,
	MP_BEARD_STUBBLE,
	MP_BEARD_THIN_CIRCLE,
	MP_BEARD_HS_SIDEBURNS,
	MP_BEARD_PM_CHOPS,
	MP_BEARD_PM_CHINSTRAP,
	MP_BEARD_BALBO_SIDEBURNS,
	MP_BEARD_CHOPS,
	MP_BEARD_FULL,  
	MP_BEARDS_MAX  //20
ENDENUM

CONST_INT DLC_MP_BEARDS_MAX  36

ENUM DLC_AWARD_SHIRT_ENUM
	DLC_SHIRT_AWARD_DEATH_DEFYING = 0,
	DLC_SHIRT_AWARD_FOR_HIRE,
	DLC_SHIRT_AWARD_LIVE_A_LITTLE,
	DLC_SHIRT_AWARD_ASSHOLE,
	DLC_SHIRT_AWARD_CANT_TOUCH_THIS,
	DLC_SHIRT_AWARD_DECORATED,
	DLC_SHIRT_AWARD_PSYCHO_KILLER,
	DLC_SHIRT_AWARD_ONE_MAN_ARMY,
	DLC_SHIRT_AWARD_SHOT_CALLER,
	DLC_SHIRT_AWARD_SHOWROOM,
	
	DLC_SHIRT_RETRO_BASE_5,
	DLC_SHIRT_RETRO_BITCHN,
	DLC_SHIRT_RETRO_BOBO,
	DLC_SHIRT_RETRO_CROCS,
	DLC_SHIRT_RETRO_EMOTION,
	DLC_SHIRT_RETRO_FEVER,
	DLC_SHIRT_RETRO_FLASH,
	DLC_SHIRT_RETRO_VINYL,
	DLC_SHIRT_RETRO_HOMIES,
	DLC_SHIRT_RETRO_KDST,
	DLC_SHIRT_RETRO_VIVISECTION,
	DLC_SHIRT_RETRO_KJAH,
	DLC_SHIRT_RETRO_BOUNCE,
	DLC_SHIRT_RETRO_KROSE,
	
	DLC_SHIRT_RSTAR_DEV_WHITE,
	DLC_SHIRT_RSTAR_DEV_BLACK,
	DLC_SHIRT_RSTAR_DEV_GRAY,
	
	DLC_SHIRT_INDI_HAPPINESS,
	
	DLC_SHIRT_PILOT_ELITAS,
	
	DLC_SHIRT_LTS_FIST,
	
	#IF IS_NEXTGEN_BUILD
	DLC_SHIRT_CG_TO_NG_1,
	DLC_SHIRT_CG_TO_NG_2,
	DLC_SHIRT_CG_TO_NG_3,
	DLC_SHIRT_CG_TO_NG_4,
	#ENDIF
	
	DLC_SHIRT_HEIST_ELITE,
	DLC_SHIRT_HEIST_ELITE_1
	
ENDENUM

//////////////////////////////////////////////////////////////////////////////////////////
///    Structs used to get ped component information
///    
STRUCT PED_COMP_ITEM_DATA_STRUCT
	//PED_COMP_TYPE_ENUM 	eType			// Component type (PED_COMP_TORSO, PED_COMP_FEET...)
	//PED_COMP_NAME_ENUM	eItem			// Component enum (TORSO_P0_BLACK_SHIRT)
	PED_COMP_STYLE_ENUM	eStyle			// Component style. Used to track the look of a player so that other characters can comment appropriately.
	INT 				iItemBit		// Used to identify bit for saved data
	INT 				iBitset			// Used to identify the bitset for saved data
	INT 				iDrawable		// Drawable variation
	INT 				iTexture		// Texture variation
	INT 				iPedID			// Used to identify array index for saved data
	INT 				iProperties		// Sets bits - CanHave [0], Available [1], Acquired [2], Outfit Only [3], Can Toggle [4], Wardrobe [5]
	INT 				iCost			// The price we have to pay to look good
	TEXT_LABEL_15 		sLabel 			// The label used to get item name
	PED_PROP_POSITION 	ePropPos 		// The anchor used if the item is a prop
	PED_PROP_TYPE_ENUM	ePropType		// The prop type (hat/headphones/glasses/mask)
ENDSTRUCT

STRUCT PED_COMP_OUTFIT_DATA_STRUCT
	PED_COMP_NAME_ENUM eItems[NUMBER_OF_PED_COMP_TYPES]
	BOOL bUsesTattooDecals
ENDSTRUCT

STRUCT PED_COMP_PROPS_DATA_STRUCT
	PED_COMP_NAME_ENUM eItems[NUMBER_OF_PED_PROP_TYPES]
ENDSTRUCT

INT g_iPedComponentSlot
INT g_iPedComponentNest
INT g_iPedComponentDressLegsCheck

PED_COMP_ITEM_DATA_STRUCT 	g_sTempCompData[3]
PED_COMP_OUTFIT_DATA_STRUCT g_sTempOutfitData
PED_COMP_PROPS_DATA_STRUCT 	g_sTempPropGroupData
MP_OUTFITS_DATA 			g_sOutfitsData

INT g_iPedComponentTemp_DaysPlayed
INT g_iPedComponentTemp_Rank

STRUCT PED_COMP_HASH_OPTIMISATION_DATA
	PED_COMP_NAME_ENUM eCachedPedComp[12]
	INT iCachedCompHash[12]
	MODEL_NAMES eCachedPedModel[12]
ENDSTRUCT
PED_COMP_HASH_OPTIMISATION_DATA g_sPedCompHashOptimisationData

BOOL g_bCachedArmourForParachuteDrawable = FALSE
INT g_iCachedBagIndexForParachuteDrawable = -1
INT g_iCachedSpecHashForParachuteDrawable = -1
INT g_iCachedJbibHashForParachuteDrawable = -1
INT g_iCachedLegsHashForParachuteDrawable = -1
INT g_iCachedSpecDrawForParachuteDrawable = -1
INT g_iCachedJbibDrawForParachuteDrawable = -1
INT g_iCachedLegsDrawForParachuteDrawable = -1

INT g_iCachedParachuteDrawableForMP = -1

BOOL g_bUseDLCHashOverrideForPedData = TRUE


#IF IS_DEBUG_BUILD
BOOL g_bAllClothesAcquired
BOOL g_bAllClothesAvailable

BOOL g_bAllTattoosAcquired
BOOL g_bAllWeaponsAcquired
#ENDIF


//////////////////////////////////////////////////////////////////////////////////////////
///    Struct for the global saved data
STRUCT PED_COMPONENTS_STRUCT
	// Each bit in the INTs represent a component item - this way we can have 32 items per component type

	// optimised so we only have the number of bitsets we need for each comp type
	// (previously had 10 for each, when most comp types don't need that many)
	// the 3 slots are:
	// PED_COMPONENT_AVAILABLE_SLOT		0
	// PED_COMPONENT_ACQUIRED_SLOT		1
	// PED_COMPONENT_USED_SLOT			2
	
	INT iHeadBitset0[3]
	
	INT iBeardBitset0[3]
	
	INT iHairBitset0[3]
	
	INT iTorsoBitset0[3]
	INT iTorsoBitset1[3]
	INT iTorsoBitset2[3]
	INT iTorsoBitset3[3]
	INT iTorsoBitset4[3]
	INT iTorsoBitset5[3]
	INT iTorsoBitset6[3]
	INT iTorsoBitset7[3]
	INT iTorsoBitset8[3]
	INT iTorsoBitset9[3]
	
	INT iLegsBitset0[3]
	INT iLegsBitset1[3]
	INT iLegsBitset2[3]
	INT iLegsBitset3[3]
	
	INT iHandBitset0[3]
	
	INT iFeetBitset0[3]
	INT iFeetBitset1[3]
	INT iFeetBitset2[3]
	INT iFeetBitset3[3]
	INT iFeetBitset4[3]
	
	INT iTeethBitset0[3]
	
	INT iSpecialBitset0[3]
	INT iSpecialBitset1[3]
	INT iSpecialBitset2[3]
	
	INT iSpecial2Bitset0[3]
	
	INT iDeclBitset0[3]
	INT iDeclBitset1[3]
	
	INT iJbibBitset0[3]
	INT iJbibBitset1[3]
	
	INT iOutfitBitset0[3]
	INT iOutfitBitset1[3]
	
	INT iPropGroupBitset0[3]
	
	INT iPropsBitset0[3]
	INT iPropsBitset1[3]
	INT iPropsBitset2[3]
	INT iPropsBitset3[3]
	INT iPropsBitset4[3]
	INT iPropsBitset5[3]
ENDSTRUCT

ENUM PED_COMP_PALETTE_COLOUR_ENUM
	// Standard 9 that all items get
	PED_COMP_COL_BLACK,
	PED_COMP_COL_WHITE,
	PED_COMP_COL_RED,
	PED_COMP_COL_YELLOW,
	PED_COMP_COL_BLUE,
	PED_COMP_COL_GREEN,
	PED_COMP_COL_PURPLE,
	PED_COMP_COL_ORANGE,
	PED_COMP_COL_CREW
ENDENUM

