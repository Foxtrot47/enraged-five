// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	triathlon_globals.sch
//		AUTHOR			:	Carlos Mijares (CM)
//		DESCRIPTION		:	Global functionality for Triathlon.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

CONST_INT TRI_RACE_MAX 8

/// PURPOSE: 
///    Indeces for each Tri race.
ENUM TRIATHLON_RACE_INDEX
	TRIATHLON_RACE_NONE = -1,
	TRIATHLON_RACE_VESPUCCI	= 0,	// Muscle beach. Vespucci Beach
	TRIATHLON_RACE_ALAMO_SEA,		// Trailer park. Alamo Sea
	TRIATHLON_RACE_IRONMAN,			// Mountain. Los Santos Iron Man
	NUM_TRIATHLON_RACES
ENDENUM

ENUM TRIATHLON_BITFLAGS
	TRIATHLON_WonRace = BIT0
ENDENUM

/// PURPOSE: 
///    Encapsulates launch data for Tri races.
STRUCT TRIATHLON_LAUNCH_DATA
	TRIATHLON_RACE_INDEX 	raceToLaunch
	VECTOR					raceStartLocation
	OBJECT_INDEX			oTable
	PED_INDEX				pedStartPed
	OBJECT_INDEX			oClipboard
	OBJECT_INDEX			oPencil
ENDSTRUCT

STRUCT TriathlonDataSaved
	INT 					iBestRank[NUM_TRIATHLON_RACES]
	FLOAT 					fBestTime[NUM_TRIATHLON_RACES]
	TRIATHLON_RACE_INDEX 	eCurrentRaceUnlocked
	INT						iBitFlags
ENDSTRUCT


/// PURPOSE: 
///    Get the position of a Triathlon race by its index.
FUNC VECTOR GET_TRIATHLON_RACE_POSITION_BY_RACE_INDEX(TRIATHLON_RACE_INDEX eRaceIndex)
	PRINTLN("[triathlon_globals.sch->GET_TRIATHLON_RACE_POSITION_BY_RACE_INDEX] Function started.")
	
	// Ensure the race index is valid.
	IF (eRaceIndex >= NUM_TRIATHLON_RACES)  OR  (eRaceIndex <= TRIATHLON_RACE_NONE)
		PRINTLN("[triathlon_globals.sch->GET_TRIATHLON_RACE_POSITION_BY_RACE_INDEX] Race index is outside of range.  Invalid.")
		RETURN << 0.0, 0.0, 0.0 >>
	ENDIF

	// Get the position of the race by its index.
	SWITCH (eRaceIndex)
		CASE TRIATHLON_RACE_ALAMO_SEA
			PRINTLN("[triathlon_globals.sch->GET_TRIATHLON_RACE_POSITION_BY_RACE_INDEX] Race index is 1!.")
			RETURN <<2434.4146, 4284.2495, 35.5059>>
			
		CASE TRIATHLON_RACE_VESPUCCI
			PRINTLN("[triathlon_globals.sch->GET_TRIATHLON_RACE_POSITION_BY_RACE_INDEX] Race index is 2!.")
			RETURN <<-1230.6222, -2049.9700, 12.8882>>
			
		CASE TRIATHLON_RACE_IRONMAN
			PRINTLN("[triathlon_globals.sch->GET_TRIATHLON_RACE_POSITION_BY_RACE_INDEX] Race index is 3!.")
			RETURN <<1591.6860, 3813.4014, 33.3371>>
		BREAK
	ENDSWITCH
	
	PRINTLN("[triathlon_globals.sch->GET_TRIATHLON_RACE_POSITION_BY_RACE_INDEX] lol, how did we get here?")
	RETURN << 0.0, 0.0, 0.0 >>
	
ENDFUNC


/// PURPOSE:
///    Calculate the TRIATHLON_RACE_INDEX closest to a given position vector.
///    
/// PARAMS:
///    vCoord 			- Position to check against each race index.
///    vRaceStartLoc	- Reference to the closest race position, to be set here.
///    
/// RETURNS:
///    TRIATHLON_RACE_INDEX closest to the position given.
FUNC TRIATHLON_RACE_INDEX GET_TRIATHLON_RACE_INDEX_BY_COORDS(VECTOR vCoord, VECTOR & vRaceStartLoc)
	PRINTLN("[triathlon_globals.sch->GET_TRIATHLON_RACE_INDEX_BY_COORDS] Function started.")

	// Initialize the race we'll be returning, for protection.
	TRIATHLON_RACE_INDEX eClosestRaceToPlayer = TRIATHLON_RACE_VESPUCCI
	
	// Temporarily store all race locations here to check against.
	VECTOR vTriRaceLocations[NUM_TRIATHLON_RACES]
	vTriRaceLocations[TRIATHLON_RACE_ALAMO_SEA] = GET_TRIATHLON_RACE_POSITION_BY_RACE_INDEX(TRIATHLON_RACE_ALAMO_SEA)
	vTriRaceLocations[TRIATHLON_RACE_VESPUCCI] = GET_TRIATHLON_RACE_POSITION_BY_RACE_INDEX(TRIATHLON_RACE_VESPUCCI)
	vTriRaceLocations[TRIATHLON_RACE_IRONMAN] = GET_TRIATHLON_RACE_POSITION_BY_RACE_INDEX(TRIATHLON_RACE_IRONMAN)
	
	// Let's initialize the closest distance, for protection.
	FLOAT fClosestDistanceFromCoordToRacePosition = VDIST2(vTriRaceLocations[TRIATHLON_RACE_VESPUCCI], vCoord)
	
	INT iLoopCounter
	FLOAT fDistanceToCheck = 0.0
	
	FOR iLoopCounter = 1  TO  (ENUM_TO_INT(NUM_TRIATHLON_RACES) - 1)
		fDistanceToCheck = VDIST2(vCoord, vTriRaceLocations[iLoopCounter])
		IF (fDistanceToCheck < fClosestDistanceFromCoordToRacePosition)
			eClosestRaceToPlayer = INT_TO_ENUM(TRIATHLON_RACE_INDEX, iLoopCounter)
			fClosestDistanceFromCoordToRacePosition = fDistanceToCheck
		ENDIF
	ENDFOR
	
	PRINTLN("[triathlon_globals.sch->GET_TRIATHLON_RACE_INDEX_BY_COORDS] Closest race is ", ENUM_TO_INT(eClosestRaceToPlayer))
	
	// Return the closest race to the coord by index.
	vRaceStartLoc = vTriRaceLocations[eClosestRaceToPlayer]
	RETURN eClosestRaceToPlayer
ENDFUNC


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
// 		END OF FILE - DO NOT ADD ANYTHING BELOW THIS BLOCK!
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************









