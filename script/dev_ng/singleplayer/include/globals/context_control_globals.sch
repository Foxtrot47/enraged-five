
ENUM CONTEXT_PRIORITY_ENUM
	CP_TAXI_PRIORITY,
	CP_MINIMUM_PRIORITY,
	CP_LOW_PRIORITY,
	CP_MEDIUM_PRIORITY,
	CP_HIGH_PRIORITY,
	CP_MAXIMUM_PRIORITY
ENDENUM

ENUM CONTEXT_STYLE_ENUM
	CS_DEFAULT
	,CS_EVENTS
	,CS_GANG_BOSS
ENDENUM

STRUCT CONTEXT_INTENTION
	BOOL bUsed
	INT iID
	INT iPriority
	INT iStyle
	
	BOOL bActive
	BOOL bAccepted
	BOOL bNoString
	
	BOOL bToBeDeleted
	
	TEXT_LABEL helpLabel
	
	BOOL bAdditionalHelp 
	TEXT_LABEL_63 additionalHelpLabel 
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 DEBUG_sname
	#ENDIF
	
	BOOL bPickedUp //used for "just" mode
	
	BOOL subStringIsPlayerName = FALSE
	
	THREADID RegisteredBy
ENDSTRUCT

CONST_INT MAX_CONTEXT_INTENTION 6

CONTEXT_INTENTION g_IntentionList[MAX_CONTEXT_INTENTION]

BOOL g_bSuppressContextHelpDuringHandling = FALSE
BOOL g_ContextResetForced
TEXT_LABEL g_strContextButton

BOOL g_bSuppressContextActionsThisFrame = FALSE
BOOL b_SuppressContextHelpNextUpdate = FALSE

INT g_iContextIntentionIDGenerator = 1

INT g_iCurrentlyDisplayingContextINDEX = -1

















