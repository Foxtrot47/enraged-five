
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	randomChar_globals.sch
//		AUTHOR			:	Keith
//		DESCRIPTION		:	Random Character Globals.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
CONST_FLOAT RC_BRAIN_COMMS_OVERRIDE_RANGE		50.0
CONST_FLOAT RC_BRAIN_ACTIVATION_RANGE_NORMAL	100.0
CONST_FLOAT RC_BRAIN_ACTIVATION_RANGE_EXTRA		209.0	// must be less than 210.0 //increased from 150.0 to fix bug 904887
CONST_FLOAT RANDOMCHAR_ACTIVATE_MISSION_RANGE	12.0
CONST_FLOAT RC_BECKON_ANIM_DISTANCE 			20.0

CONST_INT   RC_MAX_SCENE_PEDS(6)
CONST_INT   RC_MAX_SCENE_VEHS(5)
CONST_INT   RC_MAX_SCENE_OBJS(6)
CONST_INT	RC_MAX_FOW_VISIBLE_BITSETS(2)

// ENUM List of Random Character mission strands for GTA5
ENUM g_eRC_MissionStrands
	RCS_ABIGAIL,
	RCS_BARRY,
	RCS_DREYFUSS,
	RCS_EPSILON,
	RCS_EXTREME,
	RCS_FANATIC,
	RCS_HAO,
	RCS_HUNTING,
	RCS_JOSH,
	RCS_MAUDE,
	RCS_MINUTE,
	RCS_MRS_PHILIPS,
	RCS_NIGEL,
	RCS_OMEGA,
	RCS_PAPARAZZO,
	RCS_RAMPAGE,
	RCS_THELASTONE,
	RCS_TONYA,
	NO_RC_STRAND
ENDENUM

// ENUM List of Random Character sub mission groups for GTA5
ENUM g_eRC_MissionGroupIDs

	RC_GROUP_BARRY_3,		// Barry 3A-3E
	RC_GROUP_NIGEL_1,		// Nigel 1A-1E
	RC_GROUP_PAPARAZZO_3,	// Paparazzo 3A + 3B
	
	// Leave these at the bottom in this order
	MAX_RC_MISSION_GROUPS,
	NO_RC_MISSION_GROUP
ENDENUM

// ENUM List of Random Character flags for GTA5
ENUM g_eRC_FlagIDs
	RC_FLAG_ACTIVATED, 			// Allows the mission to start checking the trigger conditions
	RC_FLAG_ACTIVATED_IN_FLOW, 	// Flagged as activated in the flow
	RC_FLAG_READY_TO_PLAY, 		// Mission can start when trigger conditions are met (eg. initial delay expired, mission flow flag set, etc.)
	RC_FLAG_COMPLETED, 			// Player has passed the random character mission
	RC_FLAG_BLIP_HELP_DISPLAYED,	// Help text has been displayed for rc blip
	RC_FLAG_STATWATCHER_RUNNING //used to ensure stat watcher launches happen in the right order for RC missions
ENDENUM

ENUM g_eRC_BlipState
	RC_BLIP_TURNED_OFF,
	RC_BLIP_READY_TO_PLAY,
	RC_BLIP_READY_TO_TRIGGER
ENDENUM

ENUM g_eRC_TriggerEntity
	
	RC_TRIG_CHAR,				// Player is near a specified character
	RC_TRIG_NEAR_VEHICLE,       // Player is near a specified vehicle
	RC_TRIG_ENTER_VEHICLE,		// Player enters a specific vehicle
	
	RC_TRIG_LOCATE_POINT,       // Player gets within activation range of specified coords
	RC_TRIG_LOCATE_AXIS,		// Player enters axis aligned locate
	RC_TRIG_LOCATE_NONAXIS,     // Player enters non-axis aligned locate
		
	RC_TRIG_NOT_SET
ENDENUM

/// PURPOSE: used the the Eyefind News Story feed to check which stories are relevant
ENUM g_eRC_IfindNewsStoryBits
	RC_NWS_DREYFUSS_KILLED_DREY = 0,			// set if Dreyfuss is killed in Dreyfuss1.sc
	RC_NWS_EPS_STOLEN_CASH,						// set if Michael steals the money in Epsilon8.sc
	RC_NWS_NGLA_KILLED_WILLIE,					// set if Willie was killed in Nigel1A.sc
	RC_NWS_NGLB_KILLED_TYLER,					// set if Tyler was killed in Nigel1B.sc
	RC_NWS_NGLD_KILLED_MARK,					// set if Mark was killed in Nigel1D.sc
	RC_NWS_NGL3_DINAPOLI_NOT_KILLED_BY_TRAIN,	// set if Di Napoli doesn't get killed by the train
	RC_WEB_VISITED_COM_SITE
ENDENUM

// -----------------------------------------------------------------
//	UNSAVED DATA
// -----------------------------------------------------------------
// State the RC idle animations can be in before the mission is triggered
ENUM RC_LAUNCHER_ANIM_STATE
	LAS_INACTIVE,										// Animations not set up or they have all played
	LAS_DONE_SETUP,										// Check animations have been setup
	LAS_PLAYING_IDLE,									// RC ped is playing main idle anim
	LAS_WAITING_FOR_BECKON,								// Trigger beckon animation
	LAS_PLAYING_PRE_LEAD_IN,							// RC ped is turning to player
	LAS_TURNING_TO_PLAYER								
ENDENUM

// STRUCT containing the information used for RC characters to play animations before the mission is launched
STRUCT RC_LAUNCHER_ANIMS
	STRING 					sDictionary
	STRING 					sIdleAnim
	STRING 					sBeckonAnim
	RC_LAUNCHER_ANIM_STATE 	eState
	BOOL					bBeckonPerformed
	VECTOR					vAnimPos
	VECTOR					vAnimRot
ENDSTRUCT

// ENUM containing states for handling stopping & restarting conversation to avoid subtitles overwriting god text
ENUM RC_CONV_RESTORE_STATE
	RC_CONV_IDLE,
	RC_CONV_STOP_CURRENT_CONV,
	RC_CONV_RESTART_WITHOUT_SUBS,
	RC_CONV_RESTORE_WAIT_FOR_NO_TEXT,
	RC_CONV_RESTART_WITH_SUBS
ENDENUM

// STRUCT containing the initial RC scene vars. This gets passed into an RC script when launched.
// The RC Character / Object will always be in Index 0 of the arrays
// If changing this, please modify RC_TakeEntityOwnership() and RC_CleanupSceneEntities() to accommodate them
STRUCT g_structRCScriptArgs
	
	g_eRC_MissionIDs		eMissionID				// ID of the mission
	TEXT_LABEL_31 			sScriptName				// Actual name of the script to launch
	TEXT_LABEL_23			sIntroCutscene			// Name of intro cutscene to preload.
	FLOAT 					activationRange			// The range at which the mission triggers
	
	g_eRC_TriggerEntity		triggerType				// What does the player have to get close to, so that the mission triggers
	VECTOR					triggerLocate[2]		// Used to store trigger positions and locates
	FLOAT					triggerWidth			// Used for non-axis aligned locates
	
	BOOL					bPedsCritical			// Deactivate mission when scene peds are attacked
	BOOL					bVehsCritical			// Deactivate mission when scene vehicles are attacked
	BOOL					bAllowVehicleActivation	// Allows the mission to be activated when already in a non-scene vehicle
	
	PED_INDEX 				pedID[RC_MAX_SCENE_PEDS]// Initial scene peds - if RC is a char trigger, it will always be index 0
	VEHICLE_INDEX			vehID[RC_MAX_SCENE_VEHS]// Initial scene vehicles - if the RC is a vehicle trigger, it will always be index 0
	OBJECT_INDEX			objID[RC_MAX_SCENE_OBJS]// Initial scene props
	
	RC_LAUNCHER_ANIMS		sAnims					// Animations required by peds setup in the RC launcher
	INT						iSyncSceneIndex			// Index to the synced scene if one is necessary
	
	INT						iConversationTimer // Delay before the next ambient pre-launch conversation
ENDSTRUCT

// STRUCT containing Random Character static mission details
STRUCT g_structRCMissionsStatic
	
	// These vars have been setup in a static struct as they will not change
	// throughout the game. Scripts should use the look-up func to obtain the 
	// static mission details.
	
	STRING					rcScriptName			// Random Character Mission Filename
	TEXT_LABEL_7 			tRCNameLabel			// Text label for looking up the name of this RC mission
	INT						rcStatVariation			// The variation of the stat ID for bugstar and playstat tracking.
	g_eRC_MissionStrands	rcStrandID				// Random Character Mission Strand
	g_eRC_MissionGroupIDs 	rcMissionGroup 			// The Random Character mission group that this mission belongs to
	VECTOR					rcCoords				// Random Character Blip Coords
	BLIP_SPRITE				rcBlipSprite			// Which blip sprite to use - needed more than male / female
	TEXT_LABEL_15			rcBlipHelp				// Which help text we should display when the blip first becomes active
	FLOW_FLAG_IDS 			rcFlowFlagReq 			// A flow flag requirement for the Random Character to become ready
	INT 					rcHoursToWaitReq 		// A time delay from when the Random Character mission gets Activated to when it can become Ready To Play
	TEXT_LABEL_23 			rcAmbientPassScript		// An ambient script to launch when the Random Character mission passes
	CC_CodeID				rcPassCodeID			// A code ID referencing a custom block of script to be executed when the RC mission is passed.
	BOOL 					rcMustBeActivatedInFlow // A flow activation requirement for the Random Character to become ready
	g_eRC_MissionIDs 		rcNextMission			// A Random Character mission that should be Activated when the mission passes
	g_eRC_MissionGroupIDs	rcNextMissionGroup		// A group of Random Character missions that should be Activated when the mission passes
	INT 					rcPlayableChars 		// Bitset used to store which characters can trigger the mission - BIT_MICHAEL, BIT_TREVOR, BIT_FRANKLIN.
	INT 					rcStartTime 			// The time of day that the random char becomes available
	INT 					rcEndTime		 		// The time of day that the random char is no longer available
	BOOL					bRCRepeatable			// Can this RC mission be repeat played?
	BOOL					bBlockedInExile			// Can this RC mission launch when Michael and Trevor are Exile?
	BOOL					bUsesShitSkips			// Does this RC mission offer a shitskip if a section is failed 3 times?
ENDSTRUCT

// STRUCT containing Random Character dynamic mission details
STRUCT g_structRCMissionsDynamic
	BOOL					rcTerminateLauncher		// TRUE if the Random Character Launcher that started this mission should terminate
	BOOL					rcIsAwaitingTrigger		// TRUE if the Random Character mission is awaiting player trigger - RESET FLAG
	BOOL					rcForceStateUpdate		// TRUE if the Random Character Controller should update the RC missions state - RESET FLAG
	
	BOOL 					rcLeaveAreaCheck 		// TRUE if the Random Character mission should check for the player leaving the area
	
	BOOL					rcIsRunning				// TRUE if the Random Character mission is currently running
	BOOL					rcPassed				// TRUE if the Random Character mission has passed whilst the script is running
	BOOL					rcFailed				// TRUE if the Random Character mission has failed whilst the script is running
	g_eRC_BlipState			rcBlipState 			// The current state of the blip - off, ready to play, ready to trigger
	INT 					rcBlipIndex 			// The index of the random character blip array
	INT 					rcMissionCandidateID	// Candidate ID required for requesting mission launch
ENDSTRUCT

// An array of Random Character Mission data - unsaved
g_structRCMissionsDynamic g_RandomChars[MAX_RC_MISSIONS]

#IF IS_DEBUG_BUILD
	g_eRC_MissionIDs g_eDebugLaunchingToRCMission = NO_RC_MISSION
#ENDIF

// -----------------------------------------------------------------
//	SAVED DATA
// -----------------------------------------------------------------
// STRUCT containing Random Character mission details that need to be saved
STRUCT g_structSavedMissionsRC
	INT       	rcFlags				// Bitset used to contain all the random character mission flags (see g_eRC_FlagIDs enum)
	BOOL      	rcTimeReqSet 		// If the time delay has been set
	TIMEOFDAY 	rcTimeReq 			// The game time that the Random Character can become Ready To Play
	INT       	iCompletionOrder	// The order that missions were completed. Used by repeat play to set gameflow
	INT			iFailsNoProgress	// Number of times mission has been failed without progressing to next checkpoint (used for shitskips)
	FLOAT 		fStatCompletion		// awarded percentage from stat system, is equivalent to medal value
ENDSTRUCT

STRUCT g_structSavedVarsRC
	g_structSavedMissionsRC	savedRC[MAX_RC_MISSIONS]
	INT  savedRCEvents
	INT  iRCMissionsCompleted		// The number of RC missions the player has completed. Used for repeat play
	INT  g_iCurrentEpsilonPayment	// The amount of money that the player has donated to Epsilonism (reset on each request)
	INT  g_iWebsiteQueryBit			// Used to determine whether an Ifind story should be displayed on the Ifind home page
	INT	 g_iREDomesticCompOrder		// Used to determine when RE Domestic was completed to know when to unlock a message on Lifeinvader
	BOOL g_bFanaticHelp				// First time stamina help message on playing Fanatic mission
	BOOL g_bFanaticStamina			// Second time stamina help message on playing Fanatic mission
	BOOL g_bFanaticCheated 			// Used in Fanatic missions to have dialogue about player previously cheating
	BOOL g_bFinalEpsilonPayment		// Whether the player has paid $ to start the final Epsilon mission
	BOOL g_bStoleEpsilonCash		// Whether the player stole the Epsilonists money
	BOOL g_bTriggeredHao1			// Whether the player has ever triggered Hao 1
	
	INT	g_iVisibleInFOWBitset[RC_MAX_FOW_VISIBLE_BITSETS] // A bit for each RC mission tracking if their location has been revealed in the FOW.
ENDSTRUCT

INT g_iNoRCLeadinsActive = 0
