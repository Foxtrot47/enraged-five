// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	shoprobberies_globals.sch
//		AUTHOR			:	Carlos Mijares (CM)
//		DESCRIPTION		:	Global functionality for single-player Shop Robberies.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************



USING "model_enums.sch"
USING "commands_object.sch"
//USING "commands_misc.sch"



/// PURPOSE: 
///    Indeces for each shop.
ENUM SHOP_ROBBERIES_SHOP_INDEX
	SHOP_ROBBERIES_SHOP_CONV_10	= -2,
	SHOP_ROBBERIES_SHOP_NONE 	= -1,
	
	// Gas station stores.
	SHOP_ROBBERIES_SHOP_GAS_1 	= 0,
	SHOP_ROBBERIES_SHOP_GAS_2	= 1,
	SHOP_ROBBERIES_SHOP_GAS_3	= 2,
	SHOP_ROBBERIES_SHOP_GAS_4	= 3,
	SHOP_ROBBERIES_SHOP_GAS_5	= 4,
	
	
	// Liquor stores.
	SHOP_ROBBERIES_SHOP_LIQ_1	= 5,
	SHOP_ROBBERIES_SHOP_LIQ_2	= 6,
	SHOP_ROBBERIES_SHOP_LIQ_3	= 7,
	SHOP_ROBBERIES_SHOP_LIQ_4	= 8,
	SHOP_ROBBERIES_SHOP_LIQ_5	= 9,
	
	
	// Convenience stores.
	SHOP_ROBBERIES_SHOP_CONV_1	= 10,
	SHOP_ROBBERIES_SHOP_CONV_2	= 11,
	SHOP_ROBBERIES_SHOP_CONV_3	= 12,
	SHOP_ROBBERIES_SHOP_CONV_4	= 13,
	SHOP_ROBBERIES_SHOP_CONV_5	= 14,
	SHOP_ROBBERIES_SHOP_CONV_6	= 15,
	SHOP_ROBBERIES_SHOP_CONV_7	= 16,
	SHOP_ROBBERIES_SHOP_CONV_8	= 17,
	SHOP_ROBBERIES_SHOP_CONV_9	= 18,
	
	NUM_SHOP_ROBBERIES_SHOPS
ENDENUM

/// PURPOSE: 
///    Hashes for each shop for telemetry.
FUNC STRING GET_SHOP_NAME_FOR_TELEMETRY(SHOP_ROBBERIES_SHOP_INDEX eShop)
 	SWITCH eShop
		CASE SHOP_ROBBERIES_SHOP_CONV_10		RETURN "SHOP_ROBBERIES_SHOP_CONV_10"		//	1147977341,
	//	CASE SHOP_ROBBERIES_SHOP_NONE 			RETURN "SHOP_ROBBERIES_SHOP_NONE"			//	646502408,
		
		// Gas station stores.
		CASE SHOP_ROBBERIES_SHOP_GAS_1 			RETURN "SHOP_ROBBERIES_SHOP_GAS_1"			//	316586133,
		CASE SHOP_ROBBERIES_SHOP_GAS_2			RETURN "SHOP_ROBBERIES_SHOP_GAS_2"			//	1308339918,
		CASE SHOP_ROBBERIES_SHOP_GAS_3			RETURN "SHOP_ROBBERIES_SHOP_GAS_3"			//	812348334,
		CASE SHOP_ROBBERIES_SHOP_GAS_4			RETURN "SHOP_ROBBERIES_SHOP_GAS_4"			//	1804528116,
		CASE SHOP_ROBBERIES_SHOP_GAS_5			RETURN "SHOP_ROBBERIES_SHOP_GAS_5"			//	1028132199,
		
		
		// Liquor stores.
		CASE SHOP_ROBBERIES_SHOP_LIQ_1			RETURN "SHOP_ROBBERIES_SHOP_LIQ_1"			//	1078420967,
		CASE SHOP_ROBBERIES_SHOP_LIQ_2			RETURN "SHOP_ROBBERIES_SHOP_LIQ_2"			//	984832703,
		CASE SHOP_ROBBERIES_SHOP_LIQ_3			RETURN "SHOP_ROBBERIES_SHOP_LIQ_3"			//	-561012151,
		CASE SHOP_ROBBERIES_SHOP_LIQ_4			RETURN "SHOP_ROBBERIES_SHOP_LIQ_4"			//	-597320203,
		CASE SHOP_ROBBERIES_SHOP_LIQ_5			RETURN "SHOP_ROBBERIES_SHOP_LIQ_5"			//	183728912,
		
		
		// Convenience stores.
		CASE SHOP_ROBBERIES_SHOP_CONV_1			RETURN "SHOP_ROBBERIES_SHOP_CONV_1"			//	-348803600,
		CASE SHOP_ROBBERIES_SHOP_CONV_2			RETURN "SHOP_ROBBERIES_SHOP_CONV_2"			//	-109098365,
		CASE SHOP_ROBBERIES_SHOP_CONV_3			RETURN "SHOP_ROBBERIES_SHOP_CONV_3"			//	264533773,
		CASE SHOP_ROBBERIES_SHOP_CONV_4			RETURN "SHOP_ROBBERIES_SHOP_CONV_4"			//	-1545429173,
		CASE SHOP_ROBBERIES_SHOP_CONV_5			RETURN "SHOP_ROBBERIES_SHOP_CONV_5"			//	-1306510394,
		CASE SHOP_ROBBERIES_SHOP_CONV_6			RETURN "SHOP_ROBBERIES_SHOP_CONV_6"			//	-1051764188,
		CASE SHOP_ROBBERIES_SHOP_CONV_7			RETURN "SHOP_ROBBERIES_SHOP_CONV_7"			//	1500875366,
		CASE SHOP_ROBBERIES_SHOP_CONV_8			RETURN "SHOP_ROBBERIES_SHOP_CONV_8"			//	1769417321,
		CASE SHOP_ROBBERIES_SHOP_CONV_9			RETURN "SHOP_ROBBERIES_SHOP_CONV_9"			//	2076233468,
	ENDSWITCH
	
	RETURN "SHOP_ROBBERIES_SHOP_NONE"
ENDFUNC

// eventually convert all global bools to this
ENUM SHOP_ROB_BOOL_BITS
	SHOPROB_BOOL_FIRST_SHOP_ROB		= BIT0,
	SHOPROB_BOOL_FIRST_SHOP_SNACK	= BIT1
ENDENUM

/// PURPOSE: 
///    Encapsulates launch data for Shop Robberies shops.
STRUCT SHOP_ROBBERIES_LAUNCH_DATA
	SHOP_ROBBERIES_SHOP_INDEX 	eStoreToLaunch
	VECTOR						vShopStartLocation
ENDSTRUCT

BOOL g_bBlockShopRoberies

/// PURPOSE: 
///    Encapsulates Shop Robberies data to be saved globally.
STRUCT ShopRobberiesDataSaved
	// Total number of times each shop has been robbed.
	INT iNumberOfTimesShopsRobbed[NUM_SHOP_ROBBERIES_SHOPS]
	
	INT iGenericShopRobData
	
	// How long has it been since the last time each shop was robbed.  Used for determining when to reactive shop, at which point this value is reset.
	INT iMilliSecsWhenShopsWereRobbed[NUM_SHOP_ROBBERIES_SHOPS]
	
	// How many total shops have been robbed since the last time the script cops showed up during a robbery.  Reset when cops are allowed to spawn again.
	INT iNumberOfShopRobsSinceLastCopAmbush
	
	// Track how long it has been since each shop was last robbed by a particular character (Michael, Franklin, Trevor).  Reset after X time has passed.
	INT iMilliSecsWhenShopsWereRobbedByLastPlayer[NUM_SHOP_ROBBERIES_SHOPS]
	
	// Track which player character last robbed each shop.  Michael = 1, Franklin = 2, Trevor = 3.
	INT iLastPlayerToRobShops[NUM_SHOP_ROBBERIES_SHOPS]
ENDSTRUCT


/// PURPOSE: 
///    Ensure the shop index is valid.
FUNC BOOL IS_SHOP_ROBBERY_INDEX_VALID(SHOP_ROBBERIES_SHOP_INDEX eShopIndex)
	IF (eShopIndex >= NUM_SHOP_ROBBERIES_SHOPS)
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->IS_SHOP_ROBBERY_INDEX_VALID] INVALID SHOP INDEX: Shop index ", ENUM_TO_INT(eShopIndex), " is above maximum range")
		RETURN FALSE
	ELIF (eShopIndex <= SHOP_ROBBERIES_SHOP_NONE)
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->IS_SHOP_ROBBERY_INDEX_VALID] INVALID SHOP INDEX: Shop index ", ENUM_TO_INT(eShopIndex), " is below minimum range")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


/// PURPOSE: 
///    Get the position of a Shop Robbery by its index.
FUNC VECTOR GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX(SHOP_ROBBERIES_SHOP_INDEX eShopIndex)
	//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] Function started.")
	
	IF NOT IS_SHOP_ROBBERY_INDEX_VALID(eShopIndex)
		RETURN << 0.0, 0.0, 0.0 >>
	ENDIF

	// Get the position of the shop by its index.
	SWITCH (eShopIndex)
		
		// Gas station stores.
		CASE SHOP_ROBBERIES_SHOP_GAS_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_1: Shop index is 1.")
			RETURN << -711.8212, -915.9057, 18.2377 >>
			
		CASE SHOP_ROBBERIES_SHOP_GAS_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_2: Shop index is 2.")
			RETURN << -52.7185, -1756.1747, 28.4432 >>
			
		CASE SHOP_ROBBERIES_SHOP_GAS_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_3: Shop index is 3.")
			RETURN << 1159.4408, -325.6666, 68.2272 >>
			
		CASE SHOP_ROBBERIES_SHOP_GAS_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_4: Shop index is 4.")
			RETURN << 1699.4293, 4928.6416, 41.0858 >>
			
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_5: Shop index is 5.")
			RETURN << -1822.9261, 788.9531, 137.2120 >>
			
			
		// Liquor stores.
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_1: Shop index is 5.")
			RETURN << 1166.4265, 2703.5283, 37.1574 >>
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_2: Shop index is 6.")
			RETURN << -2973.4138, 390.6885, 14.0433 >>
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_3: Shop index is 7.")
			RETURN << -1225.8604, -903.5782, 11.3263 >>
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_4: Shop index is 8.")
			RETURN << 1140.6591, -981.0806, 45.4158 >>
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_5: Shop index is 9.")
			RETURN << -1490.2753, -382.8514, 39.1634 >>
			

		// Convenience stores.
		CASE SHOP_ROBBERIES_SHOP_CONV_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_1: Shop index is 10.")
			RETURN << -3240.7188, 1004.5081, 11.8468 >>
			
		CASE SHOP_ROBBERIES_SHOP_CONV_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_2: Shop index is 11.")
			RETURN << -3039.2488, 589.3831, 6.9251 >>
			
		CASE SHOP_ROBBERIES_SHOP_CONV_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_3: Shop index is 12.")
			RETURN << 544.4275, 2672.0610, 41.1726 >>
			
		CASE SHOP_ROBBERIES_SHOP_CONV_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_4: Shop index is 13.")
			RETURN << 2558.7542, 385.5990, 107.6391 >>
			
		CASE SHOP_ROBBERIES_SHOP_CONV_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_5: Shop index is 14.")
			RETURN << 2681.5112, 3282.7627, 54.2573 >>
			
		CASE SHOP_ROBBERIES_SHOP_CONV_6
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_6: Shop index is 15.")
			RETURN << 1731.1532, 6411.6333, 34.0373 >>
			
		CASE SHOP_ROBBERIES_SHOP_CONV_7
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_7: Shop index is 16.")
			RETURN << 1964.9305, 3741.2070, 31.3599 >>

		CASE SHOP_ROBBERIES_SHOP_CONV_8
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_8: Shop index is 17.")
			RETURN << 29.0707, -1348.7728, 28.5101 >>
			
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_9: Shop index is 18.")
			RETURN << 376.8503, 323.9777, 102.5825 >>
			
		//BREAK
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX] INVALID SHOP INDEX: Shop index is EVIL.  It bypassed the validation check.")
	RETURN << 0.0, 0.0, 0.0 >>
ENDFUNC


/// PURPOSE:
///    Calculate the SHOP_ROBBERIES_SHOP_INDEX closest to a given position vector.
///    
/// PARAMS:
///    vCoord 			- Position to check against each shop index.
///    vShopStartLoc	- Reference to the closest shop position, to be set here.
///    
/// RETURNS:
///    SHOP_ROBBERIES_SHOP_INDEX closest to the position given.
PROC GET_SHOP_ROBBERIES_SHOP_INDEX_BY_COORDS(VECTOR & vCoord, VECTOR & vShopStartLoc, SHOP_ROBBERIES_SHOP_INDEX & eClosestShopToPlayer)
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_INDEX_BY_COORDS] Function started.")

	// Initialize the race we'll be returning, for protection.
	eClosestShopToPlayer = SHOP_ROBBERIES_SHOP_GAS_1
	
	// Let's initialize the closest distance, for protection.
	FLOAT fClosestDistanceFromCoordToShopPosition = VDIST2(GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX(SHOP_ROBBERIES_SHOP_GAS_1), vCoord)
	
	INT iShopCounter
	FLOAT fDistanceToCheck = 0.0
	
	FOR iShopCounter = 1  TO  (ENUM_TO_INT(NUM_SHOP_ROBBERIES_SHOPS) - 1)
		//fDistanceToCheck = VDIST2(vCoord,  vShopRobberyLocations[iShopCounter])
		fDistanceToCheck = VDIST2(vCoord,  GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX(INT_TO_ENUM(SHOP_ROBBERIES_SHOP_INDEX, iShopCounter)))
		IF (fDistanceToCheck < fClosestDistanceFromCoordToShopPosition)
			eClosestShopToPlayer = INT_TO_ENUM(SHOP_ROBBERIES_SHOP_INDEX, iShopCounter)
			fClosestDistanceFromCoordToShopPosition = fDistanceToCheck
		ENDIF
	ENDFOR
	
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_INDEX_BY_COORDS] Closest shop is at index ", ENUM_TO_INT(eClosestShopToPlayer))
	
	// Return the closest race to the coord by index.
	vShopStartLoc = GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX(eClosestShopToPlayer)
ENDPROC


/// PURPOSE:
///    Returns shop data based on shop index.
///    
/// PARAMS:
///    eShopIndex 				- Index of a Shop Robberies shop.
///    vInteriorMainArea_Min 	- Minimum vector of angled area of main interior.
///    vInteriorMainArea_Max	- Maximum vector of angled area of main interior.
///    fInteriorMainArea_Width	- Width of angled area of main interior.
///    
/// RETURNS:
///    A 0-vector for now.
FUNC VECTOR GET_SHOP_ROBBERIES_SHOP_LOCATE_DATA_BY_INDEX(SHOP_ROBBERIES_SHOP_INDEX eShopIndex, 
															VECTOR &vInteriorMainArea_Min, VECTOR &vInteriorMainArea_Max, FLOAT &fInteriorMainArea_Width, 
															VECTOR &vInteriorHallArea_Min, VECTOR &vInteriorHallArea_Max, FLOAT &fInteriorHallArea_Width, 
															VECTOR &vInteriorBackArea_Min, VECTOR &vInteriorBackArea_Max, FLOAT &fInteriorBackArea_Width,
															VECTOR &vInteriorBackDoorArea_Min, VECTOR &vInteriorBackDoorArea_Max, FLOAT &fInteriorBackDoorArea_Width,
															VECTOR &vInteriorCounterArea_Min, VECTOR &vInteriorCounterArea_Max, FLOAT &fInteriorCounterArea_Width,
															VECTOR &vInteriorSnackPoint, FLOAT &fInteriorSnackRadius)
	// Get the position of the clerk by the shop index.
	SWITCH (eShopIndex)

		// Gas station stores.
		CASE SHOP_ROBBERIES_SHOP_GAS_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_1: Getting main interior angled area at shop index 1.")
			vInteriorMainArea_Min 		= << -711.81, -916.36, 18.22 >>
			vInteriorMainArea_Max		= << -711.74, -908.75, 21.72 >>
			fInteriorMainArea_Width 	=  13.92
			
			vInteriorHallArea_Min		= << -705.76, -908.76, 18.22 >>
			vInteriorHallArea_Max		= << -705.80, -903.16, 21.24 >>
			fInteriorHallArea_Width 	=  2.32
			
			vInteriorBackArea_Min		= << -709.02, -907.72, 18.22 >>
			vInteriorBackArea_Max		= << -708.96, -903.15, 21.25 >>
			fInteriorBackArea_Width 	=  3.66
			
			vInteriorBackDoorArea_Min	= << -707.05, -907.18, 18.23 >>
			vInteriorBackDoorArea_Max	= << -707.04, -905.90, 20.51 >>
			fInteriorBackDoorArea_Width =  0.75
			
			vInteriorCounterArea_Min	=  << -704.528, -913.948, 18.00 >>
			vInteriorCounterArea_Max	=  << -706.528, -913.948, 22.221 >>
			fInteriorCounterArea_Width  =  4.00
			
			vInteriorSnackPoint			= <<-711.290527,-913.791931,19.115599>>
			fInteriorSnackRadius		= 1.05
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_GAS_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_2: Getting main interior angled area at shop index 2.")
			vInteriorMainArea_Min 		= << -52.86, -1756.43, 28.42 >>
			vInteriorMainArea_Max		= << -47.94, -1750.60, 31.93 >>
			fInteriorMainArea_Width 	=  13.92
			
			vInteriorHallArea_Min		= << -43.36, -1754.47, 28.44 >>
			vInteriorHallArea_Max		= << -39.78, -1750.15, 31.46 >>
			fInteriorHallArea_Width 	=  2.32
			
			vInteriorBackArea_Min		= << -45.14, -1751.51, 28.43 >>
			vInteriorBackArea_Max		= << -42.23, -1748.08, 31.48 >>
			fInteriorBackArea_Width 	=  3.66
			
			vInteriorBackDoorArea_Min	= << -43.30, -1752.40, 28.48 >>
			vInteriorBackDoorArea_Max	= << -42.50, -1751.44, 30.71 >>
			fInteriorBackDoorArea_Width =  0.75
			
			vInteriorCounterArea_Min	= <<-45.231945,-1756.995972,28.421005>> //<< -45.264, -1757.029, 28.00 >>
			vInteriorCounterArea_Max	= <<-47.684238,-1759.942505,31.921005>> //<< -47.885, -1760.051, 32.426 >>
			fInteriorCounterArea_Width  = 1.80 //4.00
			
			vInteriorSnackPoint			= <<-50.833340,-1754.826904,29.321005>>
			fInteriorSnackRadius		= 1.05
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_GAS_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_3: Getting main interior angled area at shop index 3.")
			vInteriorMainArea_Min 		= << 1159.64, -326.47, 68.21 >>
			vInteriorMainArea_Max		= << 1158.33, -318.91, 71.71 >>
			fInteriorMainArea_Width 	=  13.92
			
			vInteriorHallArea_Min		= << 1164.22, -317.89, 68.21 >>
			vInteriorHallArea_Max		= << 1163.26, -312.51, 71.20 >>
			fInteriorHallArea_Width 	=  2.32
			
			vInteriorBackArea_Min		= << 1160.91, -317.34, 68.21 >>
			vInteriorBackArea_Max		= << 1160.11, -313.19, 71.13 >>
			fInteriorBackArea_Width 	=  3.66
			
			vInteriorBackDoorArea_Min	= << 1162.68, -316.51, 68.23 >>
			vInteriorBackDoorArea_Max	= << 1162.46, -315.28, 70.49 >>
			fInteriorBackDoorArea_Width =  0.75
			
			vInteriorCounterArea_Min	=  << 1164.222, -323.349, 68.210 >>
			vInteriorCounterArea_Max	=  << 1166.191, -323.001, 72.210 >>
			fInteriorCounterArea_Width  =  4.00
			
			vInteriorSnackPoint			= <<1159.639038,-323.768646,69.105103>>
			fInteriorSnackRadius		= 1.05
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_GAS_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_4: Getting main interior angled area at shop index 4.")
			vInteriorMainArea_Min 		= << 1699.09, 4929.01, 41.06 >>
			vInteriorMainArea_Max		= << 1705.30, 4924.67, 44.58 >>
			fInteriorMainArea_Width 	=  13.92
			
			vInteriorHallArea_Min		= << 1701.85, 4919.78, 41.06 >>
			vInteriorHallArea_Max		= << 1706.43, 4916.54, 44.10 >>
			fInteriorHallArea_Width 	=  2.32
			
			vInteriorBackArea_Min		= << 1704.57, 4921.80, 41.06 >>
			vInteriorBackArea_Max		= << 1708.29, 4919.18, 44.10 >>
			fInteriorBackArea_Width 	=  3.66
			
			vInteriorBackDoorArea_Min	= << 1703.90, 4919.88, 41.09 >>
			vInteriorBackDoorArea_Max	= << 1704.92, 4919.15, 43.36 >>
			fInteriorBackDoorArea_Width = 0.75
			
			vInteriorCounterArea_Min	=  << 1698.032, 4923.538, 41.069 >>
			vInteriorCounterArea_Max	=  << 1696.990, 4921.831, 45.069 >>
			fInteriorCounterArea_Width  =  4.00
			
			vInteriorSnackPoint			= <<1700.855713,4927.149902,41.963657>>
			fInteriorSnackRadius		= 1.05
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_5: Getting main interior angled area at shop index 5.")
			vInteriorMainArea_Min 		= << -1822.46, 788.35, 137.19 >>
			vInteriorMainArea_Max		= << -1827.54, 794.02, 140.72 >>
			fInteriorMainArea_Width 	=  13.92
			
			vInteriorHallArea_Min		= << -1823.17, 798.02, 137.10 >>
			vInteriorHallArea_Max		= << -1826.81, 802.05, 140.12 >>
			fInteriorHallArea_Width 	=  2.32
			
			vInteriorBackArea_Min		= << -1826.25, 796.63, 137.16 >>
			vInteriorBackArea_Max		= << -1829.17, 799.88, 140.16 >>
			fInteriorBackArea_Width 	=  3.66
			
			vInteriorBackDoorArea_Min	= << -1825.16, 798.33, 137.13 >>
			vInteriorBackDoorArea_Max	= << -1826.00, 799.31, 139.43 >>
			fInteriorBackDoorArea_Width =  0.75
			
			vInteriorCounterArea_Min	=  << -1820.332, 793.679, 137.084 >>
			vInteriorCounterArea_Max	=  << -1818.891, 795.067, 141.084 >>
			fInteriorCounterArea_Width  =  4.00
			
			vInteriorSnackPoint			= <<-1823.841064,790.731079,138.086411>>
			fInteriorSnackRadius		= 1.05
		BREAK
			
			
		// Liquor stores.
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_1: Getting main interior angled area at shop index 5.")
			vInteriorMainArea_Min 		= << 1170.182, 2708.049, 37.600 >>
			vInteriorMainArea_Max		= << 1162.385, 2708.255, 40.600 >>
			fInteriorMainArea_Width 	=  8.80
			
			vInteriorHallArea_Min		= << -1.0, -1.0, -1.0 >>
			vInteriorHallArea_Max		= << -1.0, -1.0, -1.0 >>
			fInteriorHallArea_Width 	=  -1.0
			
			vInteriorBackArea_Min		= << 1170.399, 2716.422, 37.000 >>
			vInteriorBackArea_Max		= << 1162.601, 2716.628, 41.19 >>
			fInteriorBackArea_Width 	=  9.00
			
			vInteriorBackDoorArea_Min	= << 1170.700, 2712.368, 37.700 >>
			vInteriorBackDoorArea_Max	= << 1168.300, 2712.431, 40.73 >>
			fInteriorBackDoorArea_Width =  1.50
			
			vInteriorCounterArea_Min	=  << 1165.149, 2712.433, 37.138 >>
			vInteriorCounterArea_Max	=  << 1165.132, 2710.033, 41.138 >>
			fInteriorCounterArea_Width  =  5.00
			
			vInteriorSnackPoint			= <<1165.945801,2709.136475,37.963161>>
			fInteriorSnackRadius		= 1.0
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_2: Getting main interior angled area at shop index 6.")
			vInteriorMainArea_Min 		= << -2973.31, 390.75, 14.04 >>
			vInteriorMainArea_Max		= << -2964.67, 390.23, 17.65 >>
			fInteriorMainArea_Width 	=  7.50
			
			vInteriorHallArea_Min		= << -1.0, -1.0, -1.0 >>
			vInteriorHallArea_Max		= << -1.0, -1.0, -1.0 >>
			fInteriorHallArea_Width 	=  -1.0
			
			vInteriorBackArea_Min		= << -2956.98, 389.73, 13.21 >>
			vInteriorBackArea_Max		= << -2964.11, 390.19, 17.65 >>
			fInteriorBackArea_Width 	=  7.50
			
			vInteriorBackDoorArea_Min	= << -2964.86, 388.43, 14.05 >>
			vInteriorBackDoorArea_Max	= << -2964.98, 386.49, 17.63 >>
			fInteriorBackDoorArea_Width =  1.72
			
			vInteriorCounterArea_Min	=  << -2964.645, 391.391, 14.048 >>
			vInteriorCounterArea_Max	=  << -2966.636, 391.577, 18.048 >>
			fInteriorCounterArea_Width  =  4.50
			
			vInteriorSnackPoint			= <<-2967.906250,391.042358,14.943308>>
			fInteriorSnackRadius		= 1.0
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_3: Getting main interior angled area at shop index 7.")
			vInteriorMainArea_Min 		= << -1226.29, -902.84, 11.33 >>
			vInteriorMainArea_Max		= << -1221.40, -910.16, 14.93 >>
			fInteriorMainArea_Width 	=  7.50
			
			vInteriorHallArea_Min		= << -1.0, -1.0, -1.0 >>
			vInteriorHallArea_Max		= << -1.0, -1.0, -1.0 >>
			fInteriorHallArea_Width 	=  -1.0
			
			vInteriorBackArea_Min		= << -1217.01, -916.41, 10.47 >>
			vInteriorBackArea_Max		= << -1221.13, -910.58, 14.89 >>
			fInteriorBackArea_Width 	=  7.50
			
			vInteriorBackDoorArea_Min	= << -1222.99, -911.03, 11.33 >>
			vInteriorBackDoorArea_Max	= << -1224.59, -912.13, 14.93 >>
			fInteriorBackDoorArea_Width =  1.72
			
			vInteriorCounterArea_Min	=  << -1220.512, -909.343, 11.331 >>
			vInteriorCounterArea_Max	=  << -1221.766, -907.785, 15.331 >>
			fInteriorCounterArea_Width  =  4.50
			
			vInteriorSnackPoint			= <<-1222.686523,-907.000122,12.226351>>
			fInteriorSnackRadius		= 1.15
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_4: Getting main interior angled area at shop index 8.")
			vInteriorMainArea_Min 		= << 1141.00, -980.98, 45.42 >>
			vInteriorMainArea_Max		= << 1132.30, -982.16, 48.99 >>
			fInteriorMainArea_Width 	= 7.50
			
			vInteriorHallArea_Min		= << -1.0, -1.0, -1.0 >>
			vInteriorHallArea_Max		= << -1.0, -1.0, -1.0 >>
			fInteriorHallArea_Width 	=  -1.0
			
			vInteriorBackArea_Min		= << 1124.73, -983.22, 44.57 >>
			vInteriorBackArea_Max		= << 1131.81, -982.24, 48.99 >>
			fInteriorBackArea_Width 	=  7.50
			
			vInteriorBackDoorArea_Min	= << 1132.21, -980.35, 45.42 >>
			vInteriorBackDoorArea_Max	= << 1131.91, -978.44, 49.02 >>
			fInteriorBackDoorArea_Width =  1.72
			
			vInteriorCounterArea_Min	=  << 1132.764, -983.742, 45.420 >>
			vInteriorCounterArea_Max	=  << 1134.692, -983.210, 49.420 >>
			fInteriorCounterArea_Width  =  4.50
			
			vInteriorSnackPoint			= <<1135.650757,-982.411316,46.315825>>
			fInteriorSnackRadius		= 1.05
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_LIQ_5: Getting main interior angled area at shop index 9.")
			vInteriorMainArea_Min 		= << -1490.78, -383.33, 39.16 >>
			vInteriorMainArea_Max		= << -1484.56, -377.10, 42.74 >>
			fInteriorMainArea_Width 	=  7.50
			
			vInteriorHallArea_Min		= << -1.0, -1.0, -1.0 >>
			vInteriorHallArea_Max		= << -1.0, -1.0, -1.0 >>
			fInteriorHallArea_Width 	=  -1.0
			
			vInteriorBackArea_Min		= << -1479.17, -371.69, 38.33 >>
			vInteriorBackArea_Max		= << -1484.20, -376.75, 42.73 >>
			fInteriorBackArea_Width 	=  7.50
			
			vInteriorBackDoorArea_Min	= << -1483.39, -378.48, 39.17 >>
			vInteriorBackDoorArea_Max	= << -1482.01, -379.86, 42.77 >>
			fInteriorBackDoorArea_Width =  1.72
			
			vInteriorCounterArea_Min	=  << -1485.500, -376.545, 39.167 >>
			vInteriorCounterArea_Max	=  << -1486.889, -377.985, 43.167 >>
			fInteriorCounterArea_Width  =  4.50
			
			vInteriorSnackPoint			= <<-1487.306274,-378.921967,39.813408>>
			fInteriorSnackRadius		= 1.05
		BREAK


		// Convenience stores.
		CASE SHOP_ROBBERIES_SHOP_CONV_1
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_1: Getting main interior angled area at shop index 10.")
			vInteriorMainArea_Min 		= << -3240.12, 1004.46, 11.84 >>
			vInteriorMainArea_Max		= << -3247.19, 1005.06, 15.36 >>
			fInteriorMainArea_Width 	=  11.21			
			vInteriorHallArea_Min		= << -1.0, -1.0, -1.0 >>
			vInteriorHallArea_Max		= << -1.0, -1.0, -1.0 >>
			fInteriorHallArea_Width 	=  -1.0
			
			vInteriorBackArea_Min		= << -3249.75, 999.74, 11.83 >>
			vInteriorBackArea_Max		= << -3249.05, 1007.41, 15.20 >>
			fInteriorBackArea_Width 	=  3.66		
			
			vInteriorBackDoorArea_Min	= << -3247.71, 1000.33, 11.83 >>
			vInteriorBackDoorArea_Max	= << -3247.59, 1001.62, 14.13 >>
			fInteriorBackDoorArea_Width =  0.75
			
			vInteriorCounterArea_Min	=  << -3242.573, 999.168, 11.835 >>
			vInteriorCounterArea_Max	=  << -3242.482, 1000.365, 15.835 >>
			fInteriorCounterArea_Width  =  4.200
			
			vInteriorSnackPoint			= <<-3241.871582,1006.539673,12.730711>>
			fInteriorSnackRadius		= 1.0
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_2
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_2: Getting main interior angled area at shop index 11.")
			vInteriorMainArea_Min 		= << -3038.70, 589.51, 6.92 >>
			vInteriorMainArea_Max		= << -3045.47, 587.31, 10.45 >>
			fInteriorMainArea_Width 	=  11.21
			
			vInteriorHallArea_Min		= << -1.0, -1.0, -1.0 >>
			vInteriorHallArea_Max		= << -1.0, -1.0, -1.0 >>
			fInteriorHallArea_Width 	=  -1.0
			
			vInteriorBackArea_Min		= << -3045.77, 581.47, 6.91 >>
			vInteriorBackArea_Max		= << -3048.23, 589.10, 10.43 >>
			fInteriorBackArea_Width 	=  3.66
			
			vInteriorBackDoorArea_Min	= << -3044.12, 582.75, 6.9 >>
			vInteriorBackDoorArea_Max	= << -3044.51, 583.99, 9.21 >>
			fInteriorBackDoorArea_Width =  0.75
			
			vInteriorCounterArea_Min	=  << -3039.040, 583.639, 6.914 >>
			vInteriorCounterArea_Max	=  << -3039.453, 584.766, 10.914 >>
			fInteriorCounterArea_Width  =  4.200
			
			vInteriorSnackPoint			= <<-3041.180420,590.771790,7.808933>>
			fInteriorSnackRadius		= 1.0
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_3
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_3: Getting main interior angled area at shop index 12.")
			vInteriorMainArea_Min 		= << 544.21, 2672.50, 41.16 >>
			vInteriorMainArea_Max		= << 545.16, 2665.44, 44.68 >>
			fInteriorMainArea_Width 	=  11.21
			
			vInteriorHallArea_Min		= << -1.0, -1.0, -1.0 >>
			vInteriorHallArea_Max		= << -1.0, -1.0, -1.0 >>
			fInteriorHallArea_Width 	=  -1.0
			
			vInteriorBackArea_Min		= << 550.96, 2664.13, 41.16 >>
			vInteriorBackArea_Max		= << 542.86, 2663.05, 44.69 >>
			fInteriorBackArea_Width 	=  3.66
			
			vInteriorBackDoorArea_Min	= << 549.89, 2665.96, 41.17 >>
			vInteriorBackDoorArea_Max	= << 548.61, 2665.78, 43.46 >>
			fInteriorBackDoorArea_Width =  0.75
			
			vInteriorCounterArea_Min	=  << 549.913, 2671.202, 41.161 >>
			vInteriorCounterArea_Max	=  << 548.726, 2671.024, 45.161 >>
			fInteriorCounterArea_Width  =  4.200
			
			vInteriorSnackPoint			= <<542.583130,2670.375977,42.056515>>
			fInteriorSnackRadius		= 1.0
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_4
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_4: Getting main interior angled area at shop index 13.")
			vInteriorMainArea_Min 		= << 2559.12, 385.39, 107.62 >>
			vInteriorMainArea_Max		= << 2552.06, 385.68, 111.17 >>
			fInteriorMainArea_Width 	=  11.21
			
			vInteriorHallArea_Min		= << -1.0, -1.0, -1.0 >>
			vInteriorHallArea_Max		= << -1.0, -1.0, -1.0 >>
			fInteriorHallArea_Width 	=  -1.0
			
			vInteriorBackDoorArea_Min	= << 2551.76, 380.95, 107.65 >>
			vInteriorBackDoorArea_Max	= << 2551.82, 382.21, 109.92 >>
			fInteriorBackDoorArea_Width =  0.75
			
			vInteriorCounterArea_Min	=  << 2556.854, 379.973, 107.627 >>
			vInteriorCounterArea_Max	=  << 2556.845, 381.173, 111.627 >>
			fInteriorCounterArea_Width  =  4.200
			
			vInteriorSnackPoint			= <<2557.248047,387.317719,108.522987>>
			fInteriorSnackRadius		= 1.0
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_5
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_5: Getting main interior angled area at shop index 14.")
			vInteriorMainArea_Min 		= << 2681.84, 3282.62, 54.24 >>
			vInteriorMainArea_Max		= << 2675.63, 3286.07, 57.79 >>
			fInteriorMainArea_Width 	=  11.21
			
			vInteriorHallArea_Min		= << -1.0, -1.0, -1.0 >>
			vInteriorHallArea_Max		= << -1.0, -1.0, -1.0 >>
			fInteriorHallArea_Width 	=  -1.0
			
			vInteriorBackArea_Min		= << 2671.06, 3282.22, 54.48 >>
			vInteriorBackArea_Max		= << 2675.10, 3289.37, 57.77 >>
			fInteriorBackArea_Width 	=  3.66
			
			vInteriorBackDoorArea_Min	= << 2673.23, 3281.96, 54.25 >>
			vInteriorBackDoorArea_Max	= << 2673.85, 3283.09, 56.53 >>
			fInteriorBackDoorArea_Width =  0.75
		
			vInteriorCounterArea_Min	=  << 2677.411, 3278.796, 54.246 >>
			vInteriorCounterArea_Max	=  << 2677.978, 3279.854, 58.246 >>
			fInteriorCounterArea_Width  =  4.200
			
			vInteriorSnackPoint			= <<2680.963867,3285.271484,55.141151>>
			fInteriorSnackRadius		= 1.0
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_6
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_6: Getting main interior angled area at shop index 15.")
			vInteriorMainArea_Min 		= << 1731.25, 6411.58, 34.04 >>
			vInteriorMainArea_Max		= << 1734.35, 6417.97, 37.58 >>
			fInteriorMainArea_Width 	=  11.21
			
			vInteriorHallArea_Min		= << -1.0, -1.0, -1.0 >>
			vInteriorHallArea_Max		= << -1.0, -1.0, -1.0 >>
			fInteriorHallArea_Width 	=  -1.0
			
			vInteriorBackArea_Min		= << 1730.29, 6422.24, 34.23 >>
			vInteriorBackArea_Max		= << 1737.68, 6418.66, 37.59 >>
			fInteriorBackArea_Width 	=  3.66
			
			vInteriorBackDoorArea_Min	= << 1730.17, 6420.17, 34.04 >>
			vInteriorBackDoorArea_Max	= << 1731.33, 6419.58, 36.34 >>
			fInteriorBackDoorArea_Width =  0.75
			
			vInteriorCounterArea_Min	=  << 1727.219, 6415.816, 34.042 >>
			vInteriorCounterArea_Max	=  << 1728.296, 6415.289, 38.042 >>
			fInteriorCounterArea_Width  =  4.200
			
			vInteriorSnackPoint			= <<1733.864624,6412.566406,34.937244>>
			fInteriorSnackRadius		= 1.0
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_7
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_7: Getting main interior angled area at shop index 16.")
			vInteriorMainArea_Min 		= << 1964.96, 3740.86, 31.38 >>
			vInteriorMainArea_Max		= << 1961.43, 3746.95, 34.89 >>
			fInteriorMainArea_Width 	=  11.21
			
			vInteriorHallArea_Min		= << -1.0, -1.0, -1.0 >>
			vInteriorHallArea_Max		= << -1.0, -1.0, -1.0 >>
			fInteriorHallArea_Width 	=  -1.0
			
			vInteriorBackArea_Min		= << 1955.68, 3746.02, 31.40 >>
			vInteriorBackArea_Max		= << 1962.38, 3749.91, 34.72 >>
			fInteriorBackArea_Width 	=  3.66
			
			vInteriorBackDoorArea_Min	= << 1957.30, 3744.68, 31.35 >>
			vInteriorBackDoorArea_Max	= << 1958.38, 3745.33, 33.63 >>
			fInteriorBackDoorArea_Width =  0.75
			
			vInteriorCounterArea_Min	=  << 1959.255, 3739.774, 31.349  >>
			vInteriorCounterArea_Max	=  << 1960.274, 3740.408, 35.349  >>
			fInteriorCounterArea_Width  =  4.200
			
			vInteriorSnackPoint			= <<1965.634277,3743.549805,32.243763>>
			fInteriorSnackRadius		= 1.0
		BREAK

		CASE SHOP_ROBBERIES_SHOP_CONV_8
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_8: Getting main interior angled area at shop index 17.")
			vInteriorMainArea_Min 		= << 34.84, -1345.60, 27.00 >>
			vInteriorMainArea_Max		= << 23.34, -1345.56, 32.00 >>
			fInteriorMainArea_Width 	=  7.2

			vInteriorHallArea_Min		= << -1.0, -1.0, -1.0 >>
			vInteriorHallArea_Max		= << -1.0, -1.0, -1.0 >>
			fInteriorHallArea_Width 	=  -1.0
			
			vInteriorBackArea_Min		= << 27.440, -1341.815, 27.01 >>
			vInteriorBackArea_Max		= << 27.460, -1337.815, 32.01 >>
			fInteriorBackArea_Width 	=  9.00
			
			vInteriorBackDoorArea_Min	= << 24.05, -1341.89, 27.00 >>
			vInteriorBackDoorArea_Max	= << 26.05, -1341.87, 32.00 >>
			fInteriorBackDoorArea_Width =  1.00
			
			vInteriorCounterArea_Min	=  << 23.566, -1346.956, 27.00 >>
			vInteriorCounterArea_Max	=  << 25.065, -1346.901, 32.00 >>
			fInteriorCounterArea_Width  =  4.00
			
			vInteriorSnackPoint			= <<30.973484,-1347.114624,29.393927>>
			fInteriorSnackRadius		= 1.0
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			//CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_CONV_9: Getting main interior angled area at shop index 18.")
			vInteriorMainArea_Min 		= << 376.65, 323.60, 102.57 >>
			vInteriorMainArea_Max		= << 378.35, 330.47, 106.12 >>
			fInteriorMainArea_Width 	=  11.21
			
			vInteriorHallArea_Min		= << -1.0, -1.0, -1.0 >>
			vInteriorHallArea_Max		= << -1.0, -1.0, -1.0 >>
			fInteriorHallArea_Width 	=  -1.0
			
			vInteriorBackArea_Min		= << 373.45, 333.81, 102.58 >>
			vInteriorBackArea_Max		= << 381.42, 331.81, 106.11 >>
			fInteriorBackArea_Width 	=  3.66
			
			vInteriorBackDoorArea_Min	= << 373.79, 331.72, 102.58 >>
			vInteriorBackDoorArea_Max	= << 375.03, 331.41, 104.86 >>
			fInteriorBackDoorArea_Width =  0.75
			
			vInteriorCounterArea_Min	=  << 371.789, 326.823, 102.571 >>
			vInteriorCounterArea_Max	=  << 372.957, 326.548, 106.571 >>
			fInteriorCounterArea_Width  =  4.200
			
			vInteriorSnackPoint			= <<378.993591,325.034302,103.466423>>
			fInteriorSnackRadius		= 1.0
		BREAK
	ENDSWITCH
	
	RETURN << 0.0, 0.0, 0.0 >>
ENDFUNC


/// PURPOSE:
///    Returns and sets shop synched scene data based on shop index.
///    
/// PARAMS:
///    eShopIndex 			- Index of a Shop Robberies shop.
///    fShopClerkHeading	- Returns the shop clerk's heading.
///    clerkModelName		- Returns the shop clerk's character model.
///    vScenePos			- Scene position where clerk and cash bag offset from.
///    vSceneRotation		- Scene rotation where clerk and cash bag offset from.
PROC GET_SHOP_ROBBERIES_SHOP_SCENE_DATA_BY_INDEX(SHOP_ROBBERIES_SHOP_INDEX eShopIndex, VECTOR& vScenePos, VECTOR& vSceneRotation, 
															VECTOR& vClerkPosition, 	FLOAT& fClerkHeading, 		MODEL_NAMES& clerkModelName,
															VECTOR& vCashBagPosition, 	VECTOR& vCashBagRot, 	MODEL_NAMES& cashbagModelName,
															INT& iCashBagMinCash, INT& iCashBagMaxCash, INT & iNumCounters)
	
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[shoprobberies_globals.sch->GET_SHOP_ROBBERIES_SHOP_SCENE_DATA_BY_INDEX] CASE SHOP_ROBBERIES_SHOP_GAS_1: Setting shop scene data at index", eShopIndex)
	
	// Get the position of the clerk by the shop index.
	SWITCH (eShopIndex)
		// Gas station stores.
		CASE SHOP_ROBBERIES_SHOP_GAS_1
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<-706.638184,-913.688660,19.210>> //<< -706.660, -913.740, 19.230 >>
			vSceneRotation 	= << 0, 0, -89.999 >>//-87 >>
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_GAS_2
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<-47.198711,-1757.670044,29.420>> //<< -47.300, -1757.750, 29.440 >>
			vSceneRotation 	= << 0, 0, -130 >>//-125 >>
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_GAS_3
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<1164.21, -322.89, 69.20>> //<< 1164.200, -323.000, 69.220 >>
			vSceneRotation 	= <<0.00, 0.00, -80.00>> //<< 0, 0, -74 >>
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_GAS_4
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<1698.306519,4923.370605,42.06>> //<< 1696.750, 4924.420, 42.070 >>
			vSceneRotation 	= <<0.00, 0.00, 145.00>> //<< 0, 0, 151 >>
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<-1820.465332,793.816589,138.09>> //<< -1820.400, 793.715, 138.130 >>
			vSceneRotation 	= << 0, 0, -47.53 >> // -41 >>
		BREAK
			
			
		// Liquor stores.
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<1165.958130,2710.200928,38.14286>> //05>> //<< 1166.012, 2710.219, 38.082 >>
			vSceneRotation 	= << 0, 0, -1.15 >>//-6 >>
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<-2967.026855,390.903809,15.020>> //<< -2967.015, 390.900, 15.040 >>
			vSceneRotation 	= << 0, 0, -94.76 >> // -92 >>
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<-1222.330566,-907.823364,12.31>> //<< -1222.400, -907.870, 12.320 >>
			vSceneRotation 	= << 0, 0, -147.297 >> // -145 >>
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<1134.811401,-982.361450,46.40>> //<< 1134.780, -982.350, 46.410 >>
			vSceneRotation 	= << 0, 0, 96.685623 >>//103.314 >> // 103 >>
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<-1486.672852,-378.463806,40.15>> //<< -1486.650, -378.450, 40.160 >>
			vSceneRotation 	= << 0, 0, -46.229 >> // -41 >>
		BREAK


		// Convenience stores.
		CASE SHOP_ROBBERIES_SHOP_CONV_1
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<-3244.573486,1000.657776,12.83>> //<< -3244.589, 1000.726, 12.840 >>
			vSceneRotation 	= << 0, 0, 175.074 >> // 180.0 >>
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_2
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<-3041.356689,584.266541,7.90>> //<< -3041.699, 584.222, 7.952 >>
			vSceneRotation 	= << 0, 0, -162.241 >> // -160 >>
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_3
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<548.901489,2668.941406,42.15>> //<< 548.830, 2669.050, 42.150 >>
			vSceneRotation 	= << 0, 0, -82.5 >> // -99 >>
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_4
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<2554.875000,381.385742,108.62>> //<< 2554.750, 381.460, 108.620 >>
			vSceneRotation 	= << 0, 0, 177.716 >> //180 >>
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_5
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<2676.212402,3280.969482,55.24>> //<< 2676.190, 3281.050, 55.250 >>
			vSceneRotation 	= << 0, 0, 150.87 >> // 155 >>
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_6
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<1729.329468,6417.123047,35.03>> //<< 1729.450, 6417.100, 35.040 >>
			vSceneRotation 	= << 0.0, 0.0, 63.641 >> //57.0 >>
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_7
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<1959.322998,3742.289551,32.34>> //<< 1959.360, 3742.350, 32.350 >>
			vSceneRotation 	= << 0.0, 0.0, 120 >> //117.0 >>
		BREAK

		CASE SHOP_ROBBERIES_SHOP_CONV_8
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<24.945620,-1344.954468,29.49>> //<< 25.000, -1344.856, 29.500 >>
			vSceneRotation 	= << 0.0, 0.0, 90 >> //92.0 >>
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			clerkModelName		= MP_M_SHOPKEEP_01
			cashBagModelName	= p_poly_bag_01_s
			
			vScenePos 		= <<373.595398,328.589203,103.56>> //<< 373.660, 328.550, 103.570 >>
			vSceneRotation 	= << 0, 0, 75.885 >> //75 >>
		BREAK
	ENDSWITCH
	
	// Set the cash bounds in the cash bag.
	iCashBagMinCash = 220
	iCashBagMaxCash = 787
	
	//Get the Staff Position based on the Scene
	
	vClerkPosition 		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vScenePos, vSceneRotation.z, << -0.668213, 0.85762, 0.0181999 >>)//<< -0.738572, 0.915935, -1.01 >>)
	fClerkHeading 		= (180.0 + vSceneRotation.z)
	
	SWITCH (eShopIndex)
		CASE SHOP_ROBBERIES_SHOP_GAS_1
		CASE SHOP_ROBBERIES_SHOP_GAS_2
		CASE SHOP_ROBBERIES_SHOP_GAS_3
		CASE SHOP_ROBBERIES_SHOP_GAS_4
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			vCashBagPosition 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vScenePos, vSceneRotation.z, << -0.040857, 0.366089 - 0.088, -0.428174 >>)
			iNumCounters		= 3
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			vCashBagPosition 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vScenePos, vSceneRotation.z, << -0.040857, 0.366089 - 0.033, -0.398174 >>)
			iNumCounters		= 2
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_1
		CASE SHOP_ROBBERIES_SHOP_CONV_2
		CASE SHOP_ROBBERIES_SHOP_CONV_3
		CASE SHOP_ROBBERIES_SHOP_CONV_4
		CASE SHOP_ROBBERIES_SHOP_CONV_5
		CASE SHOP_ROBBERIES_SHOP_CONV_6
		CASE SHOP_ROBBERIES_SHOP_CONV_7
		CASE SHOP_ROBBERIES_SHOP_CONV_8
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			vCashBagPosition 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vScenePos, vSceneRotation.z, << -0.040857, 0.366089 - 0.077, -0.378174 >>)
			iNumCounters		= 3
		BREAK
		
	ENDSWITCH
	
	//vCashBagPosition 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vScenePos, vSceneRotation.z, << -0.040857, 0.366089 - 0.066, -0.428174 >>)
	//vCashBagPosition 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vScenePos, vSceneRotation.z, << -0.040857, 0.366089 - 0.099, -0.428174 >>)
	//fCashBagHeading 	= vSceneRotation.z
	vCashBagRot			= << 5.0, 0.0, vSceneRotation.z >>
ENDPROC

FUNC BOOL GET_SHOP_SNACK_LOCATE_DATA(SHOP_ROBBERIES_SHOP_INDEX eShopIndex, INT iLocate, VECTOR & vOrigin, FLOAT fOHeading, VECTOR& vPos1, VECTOR & vPos2, VECTOR & vLocateCenter, FLOAT & fSize)
	
	//fOHeading = fOHeading
	
	SWITCH eShopIndex
		CASE SHOP_ROBBERIES_SHOP_GAS_1
		CASE SHOP_ROBBERIES_SHOP_GAS_2
		CASE SHOP_ROBBERIES_SHOP_GAS_3
		CASE SHOP_ROBBERIES_SHOP_GAS_4
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			SWITCH iLocate
				CASE 0
//					GAS 0 ref		= <<-706.638184,-913.688660,19.210>>
//					vPos1 			= <<-714.799988,-911.011108,18.215599>>
//					vPos2 			= <<-717.125122,-913.371399,20.065599>> 
//					vLocateCenter 	= <<-715.68, -911.86, 19.65>>
					
					vPos1 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -2.67755, -8.1618, -0.9944 >>)
					vPos2 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -0.317261, -10.4869, 0.8556 >>)
					vLocateCenter 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -1.82867, -9.04181, 0.440001 >>)
					
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "GAS 0 Initial")
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos1 = ", vPos1)
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos2 = ", vPos2)
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vLocateCenter = ", vLocateCenter)
					
					fSize 			= 1.82
				BREAK
				CASE 1
//					Gas 0 Reference
//					vPos1 			= <<-714.622192,-913.932861,18.215599>>
//					vPos2 			= <<-711.256714,-910.582397,20.065599>>
//					vLocateCenter 	= <<-713.14, -912.53, 19.64>>
					
					vPos1 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << 0.244202, -7.98401, -0.9944  >>)
					vPos2 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -3.10626, -4.61853, 0.8556 >>)
					vLocateCenter 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -1.15863, -6.50183, 0.43 >>)
					
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "GAS 1 Initial")
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos1 = ", vPos1)
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos2 = ", vPos2)
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vLocateCenter = ", vLocateCenter)
					
					fSize 			= 1.82
				BREAK
				CASE 2
//					Gas 0 Reference
//					vPos1 			= <<-708.677002,-911.204956,18.215599>>
//					vPos2 			= <<-711.284912,-913.846802,20.065599>>
//					vLocateCenter 	= <<-709.92, -912.40, 19.65>>
					
					vPos1 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -2.4837, -2.03882, -0.9944 >>)
					vPos2 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << 0.158142, -4.64673, 0.8556 >>)
					vLocateCenter 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -1.28864, -3.2818, 0.440001 >>)
					
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "GAS 2 Initial")
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos1 = ", vPos1)
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos2 = ", vPos2)
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vLocateCenter = ", vLocateCenter)
					
					fSize 			= 1.82
				BREAK
			ENDSWITCH
		BREAK
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			SWITCH iLocate
				CASE 0
//					LIQ 0 ref		=  << 1166.012, 2710.219, 38.082 >>
//					vPos1 			= <<1163.424561,2709.329834,37.063164>>
//					vPos2 			= <<1168.217529,2709.325684,38.963165>> 
//					vLocateCenter 	= <<1165.62, 2709.87, 38.04>>
					
					vPos1 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -2.55054, -0.82007, -0.998837 >>)
					vPos2 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << 2.24138, -0.920414, 0.901165 >>)
					vLocateCenter 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -0.344706, -0.323958, -0.0219994 >>)
					
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "LIQ 0 Initial")
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos1 = ", vPos1)
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos2 = ", vPos2)
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vLocateCenter = ", vLocateCenter)
					
					fSize 			= 0.65
				BREAK
				CASE 1
//					LIQ 0 Reference
//					vPos1 			= <<1165.155884,2705.749512,37.063164>>
//					vPos2 			= <<1165.130127,2708.716797,38.963165>>
//					vLocateCenter 	= <<1165.14, 2707.15, 38.50>>
					
					vPos1 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -0.891424, -4.43442, -0.998837 >>)
					vPos2 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -0.857623, -1.46721, 0.901165 >>)
					vLocateCenter 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -0.879184, -3.03399, 0.438 >>)
					
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "LIQ 1 Initial")
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos1 = ", vPos1)
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos2 = ", vPos2)
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vLocateCenter = ", vLocateCenter)
					
					fSize 			= 1.82
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_1
		CASE SHOP_ROBBERIES_SHOP_CONV_2
		CASE SHOP_ROBBERIES_SHOP_CONV_3
		CASE SHOP_ROBBERIES_SHOP_CONV_4
		CASE SHOP_ROBBERIES_SHOP_CONV_5
		CASE SHOP_ROBBERIES_SHOP_CONV_6
		CASE SHOP_ROBBERIES_SHOP_CONV_7
		CASE SHOP_ROBBERIES_SHOP_CONV_8
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			SWITCH iLocate
				CASE 0
//					CONV 0 ref		= <<-3244.573486,1000.657776,12.83>>
//					vPos1 			= <<-3245.111572,1006.802856,11.830711>>
//					vPos2 			= <<-3241.702393,1006.524902,13.650711>> 
//					vLocateCenter 	= << -3243.58, 1006.62, 12.90 >>
					
					vPos1 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << 1.06377, -6.07618, -0.999289 >>)
					vPos2 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -2.35669, -6.09199, 0.820711 >>)
					vLocateCenter 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -0.477769, -6.0255, 0.0699997 >>)
					
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "CONV 0 Initial")
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos1 = ", vPos1)
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos2 = ", vPos2)
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vLocateCenter = ", vLocateCenter)
					
					fSize 			= 1.83
				BREAK
				CASE 1
//					Gas 0 Reference
//					vPos1 			= <<-3245.442871,1003.958191,11.830711>>
//					vPos2 			= <<-3242.558105,1003.682922,13.650711>> 
//					vLocateCenter 	= << -3243.95, 1003.88, 12.88 >>
					
					vPos1 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << 1.14958, -3.21357, -0.999289 >>)
					vPos2 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -1.74817, -3.18703, 0.820711 >>)
					vLocateCenter 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -0.344543, -3.26387, 0.0500002 >>)
					
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "CONV 1 Initial")
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos1 = ", vPos1)
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos2 = ", vPos2)
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vLocateCenter = ", vLocateCenter)
					
					fSize 			= 1.83
				BREAK
				CASE 2
//					Gas 0 Reference
//					vPos1 			= <<-3246.530029,1005.944763,11.830711>>
//					vPos2 			= <<-3246.730713,1003.896790,13.650711>> 
//					vLocateCenter 	= << -3247.14, 1005.05, 12.94 >>
					
					vPos1 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << 2.4033, -5.09945, -0.999289 >>)
					vPos2 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << 2.42739, -3.04181, 0.820711 >>)
					vLocateCenter 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << 2.93408, -4.15561, 0.11 >>)
					
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "CONV 2 Initial")
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos1 = ", vPos1)
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos2 = ", vPos2)
//					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vLocateCenter = ", vLocateCenter)
					
					fSize 			= 0.83
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_SHOP_BUY_LOCATE_DATA(SHOP_ROBBERIES_SHOP_INDEX eShopIndex, VECTOR & vOrigin, FLOAT fOHeading, VECTOR& vPos1, VECTOR & vPos2, VECTOR & vLocateCenter, FLOAT & fSize)
	
	//fOHeading = fOHeading
	
	SWITCH eShopIndex
		CASE SHOP_ROBBERIES_SHOP_GAS_1
		CASE SHOP_ROBBERIES_SHOP_GAS_2
		CASE SHOP_ROBBERIES_SHOP_GAS_3
		CASE SHOP_ROBBERIES_SHOP_GAS_4
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			
			vPos1 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << 1.46991, -0.814453, -0.97226 >>)
			vPos2 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -1.5828, -0.814453, 0.84774 >>)
			vLocateCenter 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << 0.0586548, 0.178162, 0 >>)
			
//			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "GAS 0 Initial")
//			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos1 = ", vPos1)
//			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos2 = ", vPos2)
//			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vLocateCenter = ", vLocateCenter)
			
			fSize 			= 0.75
		BREAK
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			
			vPos1 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -1.55054, -0.920414, -0.998837 >>)
			vPos2 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << 1.54138, -0.920414, 0.901165 >>)
			vLocateCenter 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << 0.0586548, 0.178162, 0 >>)
			
//			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "LIQ 0 Initial")
//			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos1 = ", vPos1)
//			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos2 = ", vPos2)
//			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vLocateCenter = ", vLocateCenter)
			
			fSize 			= 0.75
		BREAK
		CASE SHOP_ROBBERIES_SHOP_CONV_1
		CASE SHOP_ROBBERIES_SHOP_CONV_2
		CASE SHOP_ROBBERIES_SHOP_CONV_3
		CASE SHOP_ROBBERIES_SHOP_CONV_4
		CASE SHOP_ROBBERIES_SHOP_CONV_5
		CASE SHOP_ROBBERIES_SHOP_CONV_6
		CASE SHOP_ROBBERIES_SHOP_CONV_7
		CASE SHOP_ROBBERIES_SHOP_CONV_8
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			
			vPos1 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -2.60296, -0.851523, -0.999289 >>)
			vPos2 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << 0.443411, -0.851523, 0.820711 >>)
			vLocateCenter 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fOHeading, << -1.17421, 0.39005, 0 >>)
			
			fSize 			= 0.75
			
//			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "CONV 0 Initial")
//			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos1 = ", vPos1)
//			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vPos2 = ", vPos2)
//			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "vLocateCenter = ", vLocateCenter)
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_10
			
			vPos1 			= <<1390.954712,3604.379395,33.980911>>
			vPos2 			= <<1394.475220,3605.663574,35.980911>> 
			vLocateCenter 	= <<1392.55054, 3605.44897, 35.30949>>
			
			fSize 			= 0.75
			
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Gets snack cam data, it actually uses an offset, but one that's stored on the global side
/// PARAMS:
///    eShopIndex - which store
///    vPos - return position of cam
///    vRot - return rotation of cam
///    fov - return fov of cam
/// RETURNS: 
///    TRUE all the time
FUNC BOOL GET_SHOP_SNACK_CAM_DATA_NO_OFFSET(SHOP_ROBBERIES_SHOP_INDEX eShopIndex, VECTOR & vPos, VECTOR& vRot, FLOAT & fov)
	
	VECTOR vOrigin
	FLOAT fHeading
	
	SWITCH eShopIndex
		CASE SHOP_ROBBERIES_SHOP_GAS_1
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<-706.638184,-913.688660,19.210>>, -89.999, << 2.40015, -7.75244, 1.0825 >>)
			vRot.x 			= -4.2069
			vRot.y 			= -0.0270
			vRot.z 			= -89.999 + 22.8175
			fov				= 32.8283
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_GAS_2
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<-47.198711,-1757.670044,29.420>>, -130, << 2.40015, -7.75244, 1.0825 >>)
			vRot.x 			= -4.2069
			vRot.y 			= -0.0270
			vRot.z 			= -130 + 22.8175
			fov				= 32.8283
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_GAS_3
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<1164.21, -322.89, 69.20>>, -80.00, << 2.40015, -7.75244, 1.0825 >>)
			vRot.x 			= -4.2069
			vRot.y 			= -0.0270
			vRot.z 			= -80.00 + 22.8175
			fov				= 32.8283
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_GAS_4
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<1698.306519,4923.370605,42.06>>, 145.00, << 2.40015, -7.75244, 1.0825 >>)
			vRot.x 			= -4.2069
			vRot.y 			= -0.0270
			vRot.z 			= 145.00 + 22.8175
			fov				= 32.8283
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<-1820.465332,793.816589,138.09>>, -47.53, << 2.40015, -7.75244, 1.0825 >>)
			vRot.x 			= -4.2069
			vRot.y 			= -0.0270
			vRot.z 			= -47.53 + 22.8175
			fov				= 32.8283
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
			
			vOrigin 		= <<1165.958130,2710.200928,38.14286>>
			fHeading	 	= -1.15
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, << 3.80296, -5.391, 1.213 >>)
			vRot.x 			= -9.3866
			vRot.y 			= -0.0011
			vRot.z 			= fHeading + 46.9866
			fov				= 35.00
			
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
			
			vOrigin 		= <<-2967.026855,390.903809,15.020>>
			fHeading 		= -94.76
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, << 3.80296, -5.391, 1.213 >>)
			vRot.x 			= -9.3866
			vRot.y 			= -0.0011
			vRot.z 			= fHeading + 46.9866
			fov				= 35.00
			
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
			
			vOrigin 		= <<-1222.330566,-907.823364,12.31>> //<< -1222.400, -907.870, 12.320 >>
			fHeading 		= -147.297
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, << 3.80296, -5.391, 1.213 >>)
			vRot.x 			= -9.3866
			vRot.y 			= -0.0011
			vRot.z 			= fHeading + 46.9866
			fov				= 35.00
			
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
			
			vOrigin 		= <<1134.811401,-982.361450,46.40>>
			fHeading 		= 96.685623
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, << 3.80296, -5.391, 1.213 >>)
			vRot.x 			= -9.3866
			vRot.y 			= -0.0011
			vRot.z 			= fHeading + 46.9866
			fov				= 35.00
			
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			
			vOrigin 		= <<-1486.672852,-378.463806,40.15>>
			fHeading 		= -46.229
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, << 3.80296, -5.391, 1.213 >>)
			vRot.x 			= -9.3866
			vRot.y 			= -0.0011
			vRot.z 			= fHeading + 46.9866
			fov				= 35.00
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_1
			
			vOrigin 		= <<-3244.573486,1000.657776,12.83>>
			fHeading 		= 175.074
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, << 2.97963, -3.39452, 0.934498 >>)
			vRot.x 			= -8.4830
			vRot.y 			= 0.0149
			vRot.z 			= fHeading + 53.5519
			fov				= 35.00
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_2
			
			vOrigin 		= <<-3041.356689,584.266541,7.90>>
			fHeading 		= -162.241
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, << 2.97963, -3.39452, 0.934498 >>)
			vRot.x 			= -8.4830
			vRot.y 			= 0.0149
			vRot.z 			= fHeading + 53.5519
			fov				= 35.00
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_3
			
			vOrigin 		= <<548.901489,2668.941406,42.15>>
			fHeading 		= -82.5
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, << 2.97963, -3.39452, 0.934498 >>)
			vRot.x 			= -8.4830
			vRot.y 			= 0.0149
			vRot.z 			= fHeading + 53.5519
			fov				= 35.00
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_4
			
			vOrigin 		= <<2554.875000,381.385742,108.62>>
			fHeading 		= 177.716
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, << 2.97963, -3.39452, 0.934498 >>)
			vRot.x 			= -8.4830
			vRot.y 			= 0.0149
			vRot.z 			= fHeading + 53.5519
			fov				= 35.00
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_5
			
			vOrigin 		= <<2676.212402,3280.969482,55.24>>
			fHeading 		= 150.87
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, << 2.97963, -3.39452, 0.934498 >>)
			vRot.x 			= -8.4830
			vRot.y 			= 0.0149
			vRot.z 			= fHeading + 53.5519
			fov				= 35.00
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_6
			
			vOrigin 		= <<1729.329468,6417.123047,35.03>>
			fHeading 		= 63.641
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, << 2.97963, -3.39452, 0.934498 >>)
			vRot.x 			= -8.4830
			vRot.y 			= 0.0149
			vRot.z 			= fHeading + 53.5519
			fov				= 35.00
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_7
			
			vOrigin 		= <<1959.322998,3742.289551,32.34>>
			fHeading 		= 120
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, << 2.97963, -3.39452, 0.934498 >>)
			vRot.x 			= -8.4830
			vRot.y 			= 0.0149
			vRot.z 			= fHeading + 53.5519
			fov				= 35.00
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_8
			
			vOrigin 		= <<24.945620,-1344.954468,29.49>>
			fHeading 		= 90
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, << 2.97963, -3.39452, 0.934498 >>)
			vRot.x 			= -8.4830
			vRot.y 			= 0.0149
			vRot.z 			= fHeading + 53.5519
			fov				= 35.00
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			
			vOrigin 		= <<373.595398,328.589203,103.56>>
			fHeading	 	= 75.885
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, << 2.97963, -3.39452, 0.934498 >>)
			vRot.x 			= -8.4830
			vRot.y 			= 0.0149
			vRot.z 			= fHeading + 53.5519
			fov				= 35.00
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_10
			
			vPos			= <<1390.6526, 3598.9656, 35.6115>>
			vRot			= <<-3.6091, 0.0220, -8.6461>>
			fov				= 35.0
			
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_SHOP_SNACK_CAM_DATA(SHOP_ROBBERIES_SHOP_INDEX eShopIndex, VECTOR vOrigin, FLOAT fHeading, VECTOR & vPos, VECTOR& vRot, FLOAT & fov)
	
	SWITCH eShopIndex
		CASE SHOP_ROBBERIES_SHOP_GAS_1
		CASE SHOP_ROBBERIES_SHOP_GAS_2
		CASE SHOP_ROBBERIES_SHOP_GAS_3
		CASE SHOP_ROBBERIES_SHOP_GAS_4
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, << 2.40015, -7.75244, 1.0825 >>)//<< -2.32674, -4.65969, 1.8199 >>)
			vRot.x 			= -4.2069 //-17.7519
			vRot.y 			= -0.0270 //0
			vRot.z 			= fHeading + 22.8175 //(-33.521)
			fov				= 32.8283
			
		BREAK
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, << 3.80296, -5.391, 1.213 >>) //<< 0.644096, -6.1575, 1.12384 >>)//<< 2.23982, -4.25083, 2.046 >>)
			vRot.x 			= -9.3866	//-7.8608 	//-23.1038
			vRot.y 			= -0.0011	//0.0254 	//0
			vRot.z 			= fHeading + 46.9866 	//+ 13.2733 //36.4399
			fov				= 35.00 	//30.4289
			
		BREAK
		CASE SHOP_ROBBERIES_SHOP_CONV_1
		CASE SHOP_ROBBERIES_SHOP_CONV_2
		CASE SHOP_ROBBERIES_SHOP_CONV_3
		CASE SHOP_ROBBERIES_SHOP_CONV_4
		CASE SHOP_ROBBERIES_SHOP_CONV_5
		CASE SHOP_ROBBERIES_SHOP_CONV_6
		CASE SHOP_ROBBERIES_SHOP_CONV_7
		CASE SHOP_ROBBERIES_SHOP_CONV_8
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			
			vPos 			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOrigin, fHeading, << 2.97963, -3.39452, 0.934498 >>) //<< -3.70911, -8.82975, 1.3661 >>) //<< 2.15405, -4.29492, 2.046 >>)
			vRot.x 			= -8.4830 	//-7.2438 	//-13.1347
			vRot.y 			= 0.0149	//0.0264 	//0
			vRot.z 			= fHeading + 53.5519	//- 17.0258 //+ 33.8741
			fov				= 35.00 	//29.7866
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_10
			
			vPos			= <<1390.6526, 3598.9656, 35.6115>> //<<1391.7242, 3599.5488, 37.2394>>
			vRot			= <<-3.6091, 0.0220, -8.6461>> //<<-18.3771, 0.0, -6.0173>>
			fov				= 35.0
			
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC STRING GET_SHOP_ROB_NAME(SHOP_ROBBERIES_SHOP_INDEX eShopIndex)
	
	
	
	SWITCH eShopIndex
		CASE SHOP_ROBBERIES_SHOP_GAS_1
		CASE SHOP_ROBBERIES_SHOP_GAS_2
		CASE SHOP_ROBBERIES_SHOP_GAS_3
		CASE SHOP_ROBBERIES_SHOP_GAS_4
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			
			RETURN "SHR_GAS"
		
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
			
			RETURN "SHR_LIQ1"
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			
			RETURN "SHR_LIQ2"
			
		CASE SHOP_ROBBERIES_SHOP_CONV_1
		CASE SHOP_ROBBERIES_SHOP_CONV_2
		CASE SHOP_ROBBERIES_SHOP_CONV_3
		CASE SHOP_ROBBERIES_SHOP_CONV_4
		CASE SHOP_ROBBERIES_SHOP_CONV_5
		CASE SHOP_ROBBERIES_SHOP_CONV_6
		CASE SHOP_ROBBERIES_SHOP_CONV_7
		CASE SHOP_ROBBERIES_SHOP_CONV_8
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			
			RETURN "SHR_CONV"
		
		CASE SHOP_ROBBERIES_SHOP_CONV_10
		
			RETURN "SHR_ACE"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL GET_SHOP_SCENARIO_BLOCKING_INFO(SHOP_ROBBERIES_SHOP_INDEX eShopIndex, FLOAT fHeading, VECTOR & vMin, VECTOR & vMax)
	
	SWITCH eShopIndex
		CASE SHOP_ROBBERIES_SHOP_GAS_1
		CASE SHOP_ROBBERIES_SHOP_GAS_2
		CASE SHOP_ROBBERIES_SHOP_GAS_3
		CASE SHOP_ROBBERIES_SHOP_GAS_4
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			
			vMin = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX(eShopIndex), fHeading, << 5.95155, -8.251, -0.2377 >>)
			vMax = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX(eShopIndex), fHeading, << -12.7268, 7.21174, 2.93854 >>)
			
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			
			vMin = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX(eShopIndex), fHeading, << -6.52428, -8.8002, -0.657398 >>)
			vMax = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX(eShopIndex), fHeading, << 3.54253, 16.8382, 3.20474 >>)
			
		BREAK
		CASE SHOP_ROBBERIES_SHOP_CONV_1
		CASE SHOP_ROBBERIES_SHOP_CONV_2
		CASE SHOP_ROBBERIES_SHOP_CONV_3
		CASE SHOP_ROBBERIES_SHOP_CONV_4
		CASE SHOP_ROBBERIES_SHOP_CONV_5
		CASE SHOP_ROBBERIES_SHOP_CONV_6
		CASE SHOP_ROBBERIES_SHOP_CONV_7
		CASE SHOP_ROBBERIES_SHOP_CONV_8
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			
			vMin = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX(eShopIndex), fHeading, << -4.55363, -7.52366, -0.75 >>)
			vMax = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX(eShopIndex), fHeading, << 11.8868, 7.00122, 3.9688 >>)
			
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC SHOP_ROBBERIES_SHOP_INDEX GET_SHOP_INDEX_FROM_HOLD_UP_POINT(INT iHoldUpPoint)
	
	SWITCH iHoldUpPoint
		CASE 16		RETURN SHOP_ROBBERIES_SHOP_GAS_1	// eCRIM_HUP_GAS_2
		CASE 17		RETURN SHOP_ROBBERIES_SHOP_GAS_2	// eCRIM_HUP_GAS_3
		CASE 18		RETURN SHOP_ROBBERIES_SHOP_GAS_3	// eCRIM_HUP_GAS_4
		CASE 15		RETURN SHOP_ROBBERIES_SHOP_GAS_4	// eCRIM_HUP_GAS_1
		CASE 19		RETURN SHOP_ROBBERIES_SHOP_GAS_5	// eCRIM_HUP_GAS_5
		
		CASE 10		RETURN SHOP_ROBBERIES_SHOP_LIQ_1	// eCRIM_HUP_LIQUOR_1
		CASE 11		RETURN SHOP_ROBBERIES_SHOP_LIQ_2	// eCRIM_HUP_LIQUOR_2
		CASE 12		RETURN SHOP_ROBBERIES_SHOP_LIQ_3	// eCRIM_HUP_LIQUOR_3
		CASE 13		RETURN SHOP_ROBBERIES_SHOP_LIQ_4	// eCRIM_HUP_LIQUOR_4
		CASE 14		RETURN SHOP_ROBBERIES_SHOP_LIQ_5	// eCRIM_HUP_LIQUOR_5
		
		CASE 2		RETURN SHOP_ROBBERIES_SHOP_CONV_1	// eCRIM_HUP_SHOP247_3
		CASE 1		RETURN SHOP_ROBBERIES_SHOP_CONV_2	// eCRIM_HUP_SHOP247_2
		CASE 3		RETURN SHOP_ROBBERIES_SHOP_CONV_3	// eCRIM_HUP_SHOP247_4
		CASE 4		RETURN SHOP_ROBBERIES_SHOP_CONV_4	// eCRIM_HUP_SHOP247_5
		CASE 5		RETURN SHOP_ROBBERIES_SHOP_CONV_5	// eCRIM_HUP_SHOP247_6
		CASE 6		RETURN SHOP_ROBBERIES_SHOP_CONV_6	// eCRIM_HUP_SHOP247_7
		CASE 7		RETURN SHOP_ROBBERIES_SHOP_CONV_7	// eCRIM_HUP_SHOP247_8
		CASE 8		RETURN SHOP_ROBBERIES_SHOP_CONV_8	// eCRIM_HUP_SHOP247_9
		CASE 9		RETURN SHOP_ROBBERIES_SHOP_CONV_9	// eCRIM_HUP_SHOP247_10
		
		CASE 0		RETURN SHOP_ROBBERIES_SHOP_CONV_10	// eCRIM_HUP_SHOP247_1
		
	ENDSWITCH
	
	RETURN SHOP_ROBBERIES_SHOP_NONE
	
ENDFUNC

FUNC STRING GET_SHOP_HEADER_TEXTURE_STRING(SHOP_ROBBERIES_SHOP_INDEX eShopIndex)
	
	SWITCH eShopIndex
		CASE SHOP_ROBBERIES_SHOP_GAS_1	
		CASE SHOP_ROBBERIES_SHOP_GAS_2	
		CASE SHOP_ROBBERIES_SHOP_GAS_3	
		CASE SHOP_ROBBERIES_SHOP_GAS_4	
		CASE SHOP_ROBBERIES_SHOP_GAS_5		RETURN 	"ShopUI_Title_GasStation"
		
		CASE SHOP_ROBBERIES_SHOP_LIQ_1		RETURN 	"ShopUI_Title_LiquorStore3"
		
		CASE SHOP_ROBBERIES_SHOP_LIQ_2		
		CASE SHOP_ROBBERIES_SHOP_LIQ_3		
		CASE SHOP_ROBBERIES_SHOP_LIQ_4		
		CASE SHOP_ROBBERIES_SHOP_LIQ_5		RETURN 	"ShopUI_Title_LiquorStore2"
		
		CASE SHOP_ROBBERIES_SHOP_CONV_1		
		CASE SHOP_ROBBERIES_SHOP_CONV_2		
		CASE SHOP_ROBBERIES_SHOP_CONV_3		
		CASE SHOP_ROBBERIES_SHOP_CONV_4		
		CASE SHOP_ROBBERIES_SHOP_CONV_5		
		CASE SHOP_ROBBERIES_SHOP_CONV_6		
		CASE SHOP_ROBBERIES_SHOP_CONV_7		
		CASE SHOP_ROBBERIES_SHOP_CONV_8		
		CASE SHOP_ROBBERIES_SHOP_CONV_9		RETURN 	"ShopUI_Title_ConvenienceStore"
		
		CASE SHOP_ROBBERIES_SHOP_CONV_10	RETURN "ShopUI_Title_LiquorStore"
		
	ENDSWITCH
	
	RETURN "NULL"
	
ENDFUNC

// shops will either be run by a chinese, pakistani, or latino man.  in order for the player to be recognised, each shop will have one of the specific heads.
// if time permits, get specific ped variations for each shop
PROC GET_SHOP_CLERK_HEAD(SHOP_ROBBERIES_SHOP_INDEX eShopIndex, INT& iDrawable, INT& iTexture)
	
	SWITCH eShopIndex
		CASE SHOP_ROBBERIES_SHOP_GAS_1
		CASE SHOP_ROBBERIES_SHOP_GAS_2
		CASE SHOP_ROBBERIES_SHOP_GAS_3
		CASE SHOP_ROBBERIES_SHOP_GAS_4
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			
			iDrawable = 0
			iTexture = GET_RANDOM_INT_IN_RANGE(0, 3)
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			
			iDrawable = 1
			iTexture = 0
			
		BREAK
		CASE SHOP_ROBBERIES_SHOP_CONV_1
		CASE SHOP_ROBBERIES_SHOP_CONV_2
		CASE SHOP_ROBBERIES_SHOP_CONV_3
		CASE SHOP_ROBBERIES_SHOP_CONV_4
		CASE SHOP_ROBBERIES_SHOP_CONV_5
		CASE SHOP_ROBBERIES_SHOP_CONV_6
		CASE SHOP_ROBBERIES_SHOP_CONV_7
		CASE SHOP_ROBBERIES_SHOP_CONV_8
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			
			iDrawable = 2
			iTexture = GET_RANDOM_INT_IN_RANGE( 0, 3 )
			
		BREAK
	ENDSWITCH
ENDPROC

// shops will either be run by a chinese, pakistani, or latino man..
// if time permits, get specific ped variations for each shop
PROC GET_SHOP_CLERK_HAIR(INT & iDrawable, INT & iTexture)

	iDrawable = GET_RANDOM_INT_IN_RANGE(0, 3)
	
	IF iDrawable > 0
		iTexture = GET_RANDOM_INT_IN_RANGE(0, 2)
	ELSE
		iTexture = 0
	ENDIF
	
ENDPROC

// shops will either be run by a chinese, pakistani, or latino man.
// if time permits, get specific ped variations for each shop
PROC GET_SHOP_CLERK_TORSO(INT & iDrawable, INT & iTexture)

	iDrawable = GET_RANDOM_INT_IN_RANGE(0, 2)
	
	iTexture = GET_RANDOM_INT_IN_RANGE(0, 3)
	
ENDPROC

// shops will either be run by a chinese, pakistani, or latino man..
// if time permits, get specific ped variations for each shop
PROC GET_SHOP_CLERK_LEG(INT & iDrawable, INT & iTexture)

	iDrawable = 0
	
	iTexture = GET_RANDOM_INT_IN_RANGE(0, 2)
	
ENDPROC

// shops will either be run by a chinese, pakistani, or latino man..
// if time permits, get specific ped variations for each shop
PROC GET_SHOP_CLERK_SPECIAL(INT & iDrawable, INT & iTexture)

	iDrawable = GET_RANDOM_INT_IN_RANGE(0, 2)
	
	IF iDrawable = 0
		iTexture = GET_RANDOM_INT_IN_RANGE(0, 2)
	ELSE
		iTexture = 0
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Get the destination and heading of where the clerk will go to cower when threatened.
PROC GET_SHOP_CLERK_BACKROOM_COWER_DATA(SHOP_ROBBERIES_SHOP_INDEX eShopIndex, VECTOR& vCowerPos, FLOAT& fCowerHead)
	SWITCH eShopIndex
		CASE SHOP_ROBBERIES_SHOP_GAS_1
			vCowerPos	=	<< -709.7998, -907.1352, 18.2156 >>
			fCowerHead	=	291.6504
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_GAS_2
			vCowerPos	=	<< -41.8128, -1749.6851, 28.4210 >>
			fCowerHead	=	214.6526
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_GAS_3
			vCowerPos	=	<< 1159.6815, -314.2540, 68.2051 >>
			fCowerHead	=	232.6337
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_GAS_4
			vCowerPos	=	<< 1707.3031, 4918.3101, 41.0636 >>
			fCowerHead	=	24.9178
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			vCowerPos	=	<< -1828.9071, 799.6096, 137.1776 >>
			fCowerHead	=	247.1234
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
			vCowerPos	=	<< 1168.9711, 2719.1179, 36.0632 >>
			fCowerHead	=	136.5945
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
			vCowerPos	=	<< -2962.9829, 391.9788, 14.0433 >>
			fCowerHead	=	176.1174
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
			vCowerPos	=	<< -1218.2826, -915.7103, 10.3264 >>
			fCowerHead	=	43.8031
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
			vCowerPos	=	<< 1130.1548, -979.2816, 45.4158 >>
			fCowerHead	=	269.2587
		BREAK
			
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			vCowerPos	=	<< -1479.1631, -375.0302, 38.1633 >>
			fCowerHead	=	36.5415
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_1
			vCowerPos	=	<< -3249.1143, 1006.5576, 11.8307 >>
			fCowerHead	=	191.5940
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_2
			vCowerPos	=	<<-3047.5117, 588.9807, 6.9089>>
			fCowerHead	=	178.8753
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_3
			vCowerPos	=	<< 543.0796, 2663.9673, 41.1565 >>
			fCowerHead	=	228.4295
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_4
			vCowerPos	=	<< 2549.8501, 387.1622, 107.6230 >>
			fCowerHead	=	197.2994
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_5
			vCowerPos	=	<< 2671.3508, 3283.1360, 54.2411 >>
			fCowerHead	=	296.5427
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_6
			vCowerPos	=	<< 1733.9670, 6421.4951, 34.0372 >>
			fCowerHead	=	130.9518
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_7
			vCowerPos	=	<< 1958.9199, 3746.2673, 31.3438 >>
			fCowerHead	=	73.6245
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_8
			vCowerPos	=	<< 30.5721, -1339.7816, 28.4939 >>
			fCowerHead	=	110.7699
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			vCowerPos	=	<< 376.2976, 331.8158, 102.5664 >>
			fCowerHead	=	52.0064
		BREAK
		
		CASE SHOP_ROBBERIES_SHOP_CONV_10
			vCowerPos	=	<< 1958.9199, 3746.2673, 31.3438 >>
			fCowerHead	=	73.6245
		BREAK
	ENDSWITCH
ENDPROC



//PURPOSE: Gets the Hold Up Door Details
PROC SHOP_ROB_DOOR_DETAILS(SHOP_ROBBERIES_SHOP_INDEX eShopIndex, INT &iHash[], MODEL_NAMES &Model[], VECTOR &vCoords[])
	//Crim_HOLD_UP_POINT WhichHoldUp = INT_TO_ENUM(Crim_HOLD_UP_POINT, iHoldUp)
	
	SWITCH eShopIndex
		CASE SHOP_ROBBERIES_SHOP_GAS_1
			//ELIF WhichHoldUp = eCRIM_HUP_GAS_2
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_GAS_2A")
			Model[0]	= v_ilev_gasdoor
			vCoords[0]	= <<-713.07, -916.54, 19.37>>
			iHash[1]	= GET_HASH_KEY("eCRIM_HUP_GAS_2B")
			Model[1]	= v_ilev_gasdoor_r
			vCoords[1]	= <<-710.47, -916.54, 19.37>>
		BREAK
		CASE SHOP_ROBBERIES_SHOP_GAS_2
			//ELIF WhichHoldUp = eCRIM_HUP_GAS_3
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_GAS_3A")
			Model[0]	= v_ilev_gasdoor
			vCoords[0]	= <<-53.96, -1755.72, 29.57>>
			iHash[1]	= GET_HASH_KEY("eCRIM_HUP_GAS_3B")
			Model[1]	= v_ilev_gasdoor_r
			vCoords[1]	= <<-51.97, -1757.39, 29.57>>
		BREAK
		CASE SHOP_ROBBERIES_SHOP_GAS_3
			//ELIF WhichHoldUp = eCRIM_HUP_GAS_4
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_GAS_4A")
			Model[0]	= v_ilev_gasdoor
			vCoords[0]	= <<1158.36, -326.82, 69.36>>
			iHash[1]	= GET_HASH_KEY("eCRIM_HUP_GAS_4B")
			Model[1]	= v_ilev_gasdoor_r
			vCoords[1]	= <<1160.93, -326.36, 69.36>>
		BREAK
		CASE SHOP_ROBBERIES_SHOP_GAS_4
			//ELIF WhichHoldUp = eCRIM_HUP_GAS_1
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_GAS_1A")
			Model[0]	= v_ilev_gasdoor
			vCoords[0]	= <<1699.66, 4930.28, 42.21>>
			iHash[1]	= GET_HASH_KEY("eCRIM_HUP_GAS_1B")
			Model[1]	= v_ilev_gasdoor_r
			vCoords[1]	= <<1698.17, 4928.15, 42.21>>
		BREAK
		CASE SHOP_ROBBERIES_SHOP_GAS_5
			//ELIF WhichHoldUp = eCRIM_HUP_GAS_5
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_GAS_5A")
			Model[0]	= v_ilev_gasdoor
			vCoords[0]	= <<-1823.28, 787.37, 138.36>>
			iHash[1]	= GET_HASH_KEY("eCRIM_HUP_GAS_5B")
			Model[1]	= v_ilev_gasdoor_r
			vCoords[1]	= <<-1821.37, 789.13, 138.31>>
		BREAK
		CASE SHOP_ROBBERIES_SHOP_LIQ_1
			//ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_1
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_LIQUOR_1A")
			Model[0]	= v_ilev_ml_door1
			vCoords[0]	= <<1167.13, 2703.75, 38.30>>
			iHash[1]	= -1
		BREAK
		CASE SHOP_ROBBERIES_SHOP_LIQ_2
			//ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_2
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_LIQUOR_2A")
			Model[0]	= v_ilev_ml_door1
			vCoords[0]	= <<-2973.53, 390.14, 15.19>>
			iHash[1]	= -1
		BREAK
		CASE SHOP_ROBBERIES_SHOP_LIQ_3
			//ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_3
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_LIQUOR_3A")
			Model[0]	= v_ilev_ml_door1
			vCoords[0]	= <<-1226.89, -903.12, 12.47>>
			iHash[1]	= -1
		BREAK
		CASE SHOP_ROBBERIES_SHOP_LIQ_4
			//ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_4
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_LIQUOR_4A")
			Model[0]	= v_ilev_ml_door1
			vCoords[0]	= <<1141.04, -980.32, 46.56>>
			iHash[1]	= -1
		BREAK
		CASE SHOP_ROBBERIES_SHOP_LIQ_5
			//ELIF WhichHoldUp = eCRIM_HUP_LIQUOR_5
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_LIQUOR_5A")
			Model[0]	= v_ilev_ml_door1
			vCoords[0]	= <<-1490.41, -383.85, 40.31>>
			iHash[1]	= -1
		BREAK
		CASE SHOP_ROBBERIES_SHOP_CONV_1
			//ELIF WhichHoldUp = eCRIM_HUP_SHOP247_3
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_3A")
			Model[0]	= v_ilev_247door
			vCoords[0]	= <<-3240.13, 1003.16, 12.98>>
			iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_3B")
			Model[1]	= v_ilev_247door_r
			vCoords[1]	= <<-3239.90, 1005.75, 12.98>>
		BREAK
		CASE SHOP_ROBBERIES_SHOP_CONV_2
			//ELIF WhichHoldUp = eCRIM_HUP_SHOP247_2
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_2A")
			Model[0]	= v_ilev_247door
			vCoords[0]	= <<-3038.22, 588.29, 8.06>>
			iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_2B")
			Model[1]	= v_ilev_247door_r
			vCoords[1]	= <<-3039.01, 590.76, 8.06>>
		BREAK
		CASE SHOP_ROBBERIES_SHOP_CONV_3
			//ELIF WhichHoldUp = eCRIM_HUP_SHOP247_4
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_4A")
			Model[0]	= v_ilev_247door
			vCoords[0]	= <<545.50, 2672.75, 42.31>>
			iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_4B")
			Model[1]	= v_ilev_247door_r
			vCoords[1]	= <<542.93, 2672.41, 42.31>>
		BREAK
		CASE SHOP_ROBBERIES_SHOP_CONV_4
			//ELIF WhichHoldUp = eCRIM_HUP_SHOP247_5
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_5A")
			Model[0]	= v_ilev_247door
			vCoords[0]	= <<2559.20, 384.09, 108.77>>
			iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_5B")
			Model[1]	= v_ilev_247door_r
			vCoords[1]	= <<2559.30, 386.69, 108.77>>
		BREAK
		CASE SHOP_ROBBERIES_SHOP_CONV_5
			//ELIF WhichHoldUp = eCRIM_HUP_SHOP247_6
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_6A")
			Model[0]	= v_ilev_247door
			vCoords[0]	= <<2681.29, 3281.43, 55.39>>
			iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_6B")
			Model[1]	= v_ilev_247door_r
			vCoords[1]	= <<2682.56, 3283.70, 55.39>>
		BREAK
		CASE SHOP_ROBBERIES_SHOP_CONV_6
			//ELIF WhichHoldUp = eCRIM_HUP_SHOP247_7
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_7A")
			Model[0]	= v_ilev_247door
			vCoords[0]	= <<1730.03, 6412.07, 35.19>>
			iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_7B")
			Model[1]	= v_ilev_247door_r
			vCoords[1]	= <<1732.36, 6410.92, 35.19>>
		BREAK
		CASE SHOP_ROBBERIES_SHOP_CONV_7
			//ELIF WhichHoldUp = eCRIM_HUP_SHOP247_8
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_8A")
			Model[0]	= v_ilev_247door
			vCoords[0]	= <<1963.92, 3740.08, 32.49>>
			iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_8B")
			Model[1]	= v_ilev_247door_r
			vCoords[1]	= <<1966.17, 3741.38, 32.49>>
		BREAK
		CASE SHOP_ROBBERIES_SHOP_CONV_8
			//ELIF WhichHoldUp = eCRIM_HUP_SHOP247_9
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_9A")
			Model[0]	= v_ilev_247door
			vCoords[0]	= <<27.82, -1349.17, 29.65>>
			iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_9B")
			Model[1]	= v_ilev_247door_r
			vCoords[1]	= <<30.42, -1349.17, 29.65>>
		BREAK
		CASE SHOP_ROBBERIES_SHOP_CONV_9
			//ELIF WhichHoldUp = eCRIM_HUP_SHOP247_10
			iHash[0]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_10A")
			Model[0]	= v_ilev_247door
			vCoords[0]	= <<375.35, 323.80, 103.72>>
			iHash[1]	= GET_HASH_KEY("eCRIM_HUP_SHOP247_10B")
			Model[1]	= v_ilev_247door_r
			vCoords[1]	= <<377.88, 323.17, 103.72>>
		BREAK
	ENDSWITCH
ENDPROC

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
// 		END OF FILE - DO NOT ADD ANYTHING BELOW THIS BLOCK!
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************









